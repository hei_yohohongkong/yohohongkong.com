<?php

/***
 * order_print.php
 * rewritten by howang 2015-05-04
 *
 * Output order in HTML or PDF format for printing
 ***/

define('IN_ECS', true);

define('FORCE_HTTPS', true);

require(dirname(__FILE__) . '/includes/init.php');
require_once(ROOT_PATH . 'includes/lib_order.php');
require_once(ROOT_PATH . 'includes/lib_erp.php');

// Load additional language file
require_once(ROOT_PATH . 'languages/zh_tw/order_print.php');
// Re-assign the language array to template engine
$smarty->assign('lang', $_LANG);

// Check admin login
$admin = check_admin_login();
if (!$admin)
{
    show_message($_LANG['not_login'], $_LANG['back_home'], $ecs->url(), 'error');
    exit;
}
$smarty->assign('admin', $admin);

// Input parameters
$order_id = (isset($_REQUEST['order_id'])) ? intval($_REQUEST['order_id']) : 0;
$order_sn = (!empty($_REQUEST['order_sn'])) ? trim($_REQUEST['order_sn']) : '';
$delivery_id = (isset($_REQUEST['delivery_id'])) ? intval($_REQUEST['delivery_id']) : 0;

// moved code to lib_order.php so we can call it from multiple places
assign_order_print($order_id, $order_sn, $delivery_id);
$order = $smarty->get_template_vars('order');

/* 如果订单不存在，退出 */
if (empty($order))
{
    die('order does not exist');
}

if (isset($_GET['print']))
{
    $smarty->template_dir = DATA_DIR;
    $template = ($order['only_market_invoice']) ? 'order_print3.html' : 'order_print2.html';
    $smarty->assign('date', local_date("m/d"));
    $smarty->assign('time', local_date("H:i"));
    $smarty->assign('month', date('m'));
    $smarty->assign('day', local_date('d'));
    $order_invoice_id = $order['order_sn'];
    
    if ($_GET['print'] == 'market_price')
    {
        $template = 'order_print3.html';
    }
    elseif ($_GET['print'] == 'shipping_tqb')
    {
        $address = array(
            // 'HKI' => false,
            // 'NT' => false,
            // 'KLN' => false,
            'formatted' => ''
        );
        // if ((stripos($order['address'], 'Hong Kong Island') !== false) ||
        //     (strpos($order['address'], '香港島') !== false))
        // {
        //     $address['HKI'] = true;
        // }
        // elseif ((stripos($order['address'], 'N.T.') !== false) ||
        //     (stripos($order['address'], 'new territories') !== false) ||
        //     (strpos($order['address'], '新界') !== false))
        // {
        //     $address['NT'] = true;
        // }
        // elseif ((stripos($order['address'], 'KLN') !== false) ||
        //     (stripos($order['address'], 'K.L.N.') !== false) ||
        //     (stripos($order['address'], 'Kowloon') !== false) ||
        //     (strpos($order['address'], '九龍') !== false))
        // {
        //     $address['KLN'] = true;
        // }
        $address['formatted'] = chinese_wrap($order['address'], 34, '<br>');
        $line_breaks = substr_count($address['formatted'], '<br>');
        if ($line_breaks < 5) $address['formatted'] .= str_repeat('<br>', (5 - $line_breaks));
        
        $smarty->assign('address', $address);
        $next_day = local_strtotime("+1 day");
        $smarty->assign('day', local_date('d', $next_day));
        $smarty->assign('month', local_date('m',$next_day));
        $template = 'order_shipping_tqb.html';
    }
    elseif ($_GET['print'] == 'shipping_sf')
    {
        $address = array(
            'formatted' => ''
        );
        $address['formatted'] = chinese_wrap($order['address'], 34, '<br>');
        $line_breaks = substr_count($address['formatted'], '<br>');
        if ($line_breaks < 5) $address['formatted'] .= str_repeat('<br>', (5 - $line_breaks));

        $smarty->assign('address', $address);
        
        $template = 'order_shipping_sf.html';
    }
    elseif (in_array($_GET['print'], array('receipt', 'receipt_print', 'receipt_market', 'receipt_market_print')))
    {
        // Accepted Modes:
        // receipt              - Normal receipt PDF
        // receipt_print        - Receipt PDF, without background, for printing
        // receipt_market       - Market price receipt PDF
        // receipt_market_print - Market price receipt PDF, without background, for printing
        $receipt_mode = $_GET['print'];
        
        $pdfname = $receipt_mode . '_' . $order['order_sn'] . '.pdf';
        $receiptdir = ROOT_PATH . DATA_DIR . '/receiptpdf/';
        $receiptfile = $receiptdir . $pdfname;
        
        if ((empty($_GET['force_generate'])) && (file_exists($receiptfile)))
        {
            $pdf = file_get_contents($receiptfile);
        }
        else
        {
            $is_print = (substr($receipt_mode, -6) == '_print');
            
            if ($is_print)
            {
                $receipt_mode = substr($receipt_mode, 0, -6);
            }
            
            if ($receipt_mode == 'receipt_market' || $order['only_market_invoice'])
            {
                $template = 'order_receipt_market.html';
            }
            else // if ($receipt_mode == 'receipt')
            {
                $template = 'order_receipt.html';
            }
            $receipt_background = $is_print ? false : true;
            $smarty->assign('receipt_background', $receipt_background);
            $html = $smarty->fetch($template);
            $pdf = generate_receipt_pdf($html);
            
            file_put_contents($receiptfile, $pdf);
        }
        
        // 2015-02-11: output only specific page of the pdf
        $page = '';
        if (!empty($_GET['page']))
        {
            $page = preg_replace('/[^0-9-]/', '', $_GET['page']);
        }
        if (!empty($page))
        {
            $pdfname = substr($pdfname, 0, -4) . '_p' . $page . '.pdf';
        }
        
        // Output PDF to browser
        header('Content-Type: application/pdf');
        header('Content-Disposition:inline; filename=' . $pdfname);
        if (empty($page))
        {
            echo $pdf;
        }
        else
        {
            // output only specific page of the pdf with pdftk
            passthru('/usr/bin/pdftk ' . escapeshellarg($receiptfile) . ' cat ' . escapeshellarg($page) . ' output -');
        }
        exit;
    }
    $smarty->display($template);
    exit;
}
elseif (isset($_GET['debug']))
{
    header('Content-Type: text/plain;charset=utf8');
    print_r($order);
    print_r($goods_list);
    exit;
}

function chinese_wrap($str, $wrap_at = 40, $wrap_with = "\n")
{
	$output = '';
	$line_len = 0;
	$l = mb_strlen($str, 'UTF-8');
	for ($i = 0; $i < $l; $i++)
	{
		$c = mb_substr($str, $i, 1, 'UTF-8');
		$cl = (strlen($c) > 1) ? 2 : 1; // if not ASCII, strlen($c) = 3
		if ($line_len + $cl > $wrap_at)
		{
			$output .= $wrap_with;
			$line_len = 0;
		}
		$output .= $c;
		$line_len += $cl;
	}
	return $output;
}

?>