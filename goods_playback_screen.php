<?php

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
include_once(ROOT_PATH . ADMIN_PATH . '/includes/lib_main.php');

$goodsPlaybackScreenController = new Yoho\cms\Controller\GoodsPlaybackScreenController();

$validMode = ['top-100', 'new', 'five-star-review', 'seven-day-hot'];
$mode = $_REQUEST['mode'] ?: 'top-100';
$action = $_REQUEST['act'] ?: 'display';

if (!in_array($mode, $validMode, true)) {
    exit;
}

if ($action === 'display') {
    $smarty->assign('mode', $mode);
    $smarty->display('product_playback_screen.dwt');
}

if ($action === 'query') {
    if ($mode === 'top-100') {
        $data = $goodsPlaybackScreenController->getTopHundredBestseller();
    }

    if ($mode === 'five-star-review') {
        $data = $goodsPlaybackScreenController->getFiveStarReviewItems();
    }

    if ($mode === 'new') {
        $data = $goodsPlaybackScreenController->getNewItem();
    }

    if ($mode === 'seven-day-hot') {
        $data = $goodsPlaybackScreenController->getSevenDayHotItem();
    }

    exit (json_encode([
        'content' => $data,
        'error' => '0',
        'message' => ''
        ], JSON_UNESCAPED_UNICODE));
}
