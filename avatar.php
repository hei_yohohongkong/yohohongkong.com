<?php

/***
 * avatar.php
 * by howang 2014-08-04
 *
 * Avatar proxying script
 ***/

ini_set('display_errors', 0);

define('ROOT_PATH', dirname(__FILE__) . '/');

$avatar_id = intval($_REQUEST['id']);
$size = empty($_REQUEST['size']) ? 'm' : strtolower($_REQUEST['size']);
$size = in_array($size, array('s','m','l')) ? $size : 'm';

$size_suffixes = array(
	's' => '_60x60',
	'm' => '_100x100',
	'l' => '',
);

$size_suffix = $size_suffixes[$size];

$path = 'uploads/avatars/' . $avatar_id . $size_suffix . '.png';

$lastmod = @filemtime(ROOT_PATH . $path);

if ($lastmod !== false)
{
	$url = '/' . $path . '?' . $lastmod;
}
else
{
	$url = '/uploads/avatars/default' . $size_suffix . '.png';
}

header('Location: ' . $url);
exit;

?>