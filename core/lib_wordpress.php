<?php

/***
* lib_wordpress.php
* by howang 2015-03-02
*
* Library for ECShop <--> WordPress communications
* 
***/

if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

require_once ROOT_PATH . 'core/lib_payload.php';

define('HOWANG_ECSHOP_WORDPRESS_KEY', 'ero/vnTR4QEzn/9HkWk5tK10Ydgwmbb3izxeqpaMwgA=');

function wordpress_call($obj)
{
	$start = time() + microtime();
	
	$payload = base64_encode(encode_payload($obj, HOWANG_ECSHOP_WORDPRESS_KEY));
	
	// $api_url = 'http://blog.yohohongkong.com/ecshop_api.php';
	// $skip_dns = '--resolve blog.yohohongkong.com:80:127.0.0.1'; // As DNS lookup is really slow
	// exec('curl ' . $skip_dns . ' -d "payload=' . rawurlencode($payload) . '" ' . $api_url, $output);
	// return json_decode(implode('', $output), true);
	
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_HEADER, 0);
	curl_setopt($curl, CURLOPT_VERBOSE, 0);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_URL, 'http://127.0.0.1/ecshop_api.php'); // As DNS lookup is really slow
	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Host: blog.yohohongkong.com')); // We set host header manually
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, 'payload=' . rawurlencode($payload));
	$response = curl_exec($curl);
	curl_close($curl);
	
	$end = time() + microtime();
	
	error_log('wordpress_call time used: ' . ($end - $start) . 's');
	
	return json_decode($response, true);
}

function wordpress_recent_posts($num = 10)
{
	$cache_name = 'wordpress_recent_posts_' . $num;
	$data = read_static_cache($cache_name);
	if ($data !== false)
	{
		return $data;
	}
	
	$result = wordpress_call(array(
		'act' => 'recent_posts',
		'num' => $num
	));
	
	if ($result['status'] != 1)
	{
		return false;
	}
	
	write_static_cache($cache_name, $result['data']);
	return $result['data'];
}

?>