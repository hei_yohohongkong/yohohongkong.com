<?php

if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

$usingSSL = (isset($_SERVER['HTTPS']) && (strtolower($_SERVER['HTTPS']) != 'off')) || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && (strtolower($_SERVER['HTTP_X_FORWARDED_PROTO']) == 'https'));
if (defined('FORCE_HTTPS'))
{
	if (!$usingSSL)
	{
		redirect_to_https();
	}
}

function redirect_to_https()
{
	header('Location: https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
	exit;
}

?>