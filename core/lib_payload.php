<?php

/***
* lib_payload.php
* by howang 2014-09-01
*
* Library encrypting / decrypting API payloads to be transferred over the Internet
* 
***/

if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

define('HOWANG_ECSHOP_INTERNAL_KEY', 'Ul2xYjj2aPw+Hwh3DOqv+femVtXD4OBnyHrjQSr0kPc=');

function generate_iv()
{
	$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CFB);
	$iv = mcrypt_create_iv($iv_size, MCRYPT_DEV_URANDOM); // 16 bytes output
	return $iv;
}

function generate_key()
{
	$key_size = mcrypt_get_key_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CFB);
	$encryption_key = openssl_random_pseudo_bytes($key_size, $strong);
	return ($strong) ? $encryption_key : null;
}

function encode_payload($obj, $key = HOWANG_ECSHOP_INTERNAL_KEY)
{
	$key = base64_decode($key);
	$iv = generate_iv();
	$encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, json_encode($obj), MCRYPT_MODE_CFB, $iv);
	return $iv . $encrypted;
}

function decode_payload($data, $key = HOWANG_ECSHOP_INTERNAL_KEY)
{
	$key = base64_decode($key);
	$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CFB);
	$iv = substr($data, 0, $iv_size);
	$decrypted = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, substr($data, $iv_size), MCRYPT_MODE_CFB, $iv);
	return json_decode($decrypted, true);
}

// Example usage:
//base64_encode(encode_payload($obj));
//decode_payload(base64_decode($data));

?>