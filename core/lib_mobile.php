<?php

if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

// Get domain from SERVER_NAME
define('SERVER_DOMAIN', implode('.', array_slice(explode('.', $_SERVER['SERVER_NAME']), -2)));

if (strpos($_SERVER['SERVER_NAME'], 'beta.') !== false)
{
	define('DESKTOP_HOST', 'beta.' . SERVER_DOMAIN);
	define('MOBILE_HOST', 'm.beta.' . SERVER_DOMAIN);
}
else
{
	define('DESKTOP_HOST', 'www.' . SERVER_DOMAIN);
	define('MOBILE_HOST', 'm.' . SERVER_DOMAIN);
}

define('PROTOCOL_PREFIX', (isset($_SERVER['HTTPS']) && (strtolower($_SERVER['HTTPS']) != 'off')) ? 'https://' : 'http://');

$handleMobileViewToggling = true;
// Don't handle mobile view redirect in ajax scripts, they are common to both website and mobile version
if (strpos($_SERVER['REQUEST_URI'],'ajax/') !== false) $handleMobileViewToggling = false;
// Don't handle mobile view redirect in api folder
if (strpos($_SERVER['REQUEST_URI'],'api/') !== false) $handleMobileViewToggling = false;
// Don't handle mobile view redirect if the webpage only available on one version
if ((defined('DESKTOP_ONLY')) || (defined('MOBILE_ONLY'))) $handleMobileViewToggling = false;
// Don't handle mobile view redirect if cookie is set (for development use)
if (isset($_COOKIE['disable_mobile_redirct']) && $_COOKIE['disable_mobile_redirct'])
{
	// If not previously disabled handling, we should remove the toggleMobileView query string
	if ($handleMobileViewToggling && isset($_GET['toggleMobileView']))
	{
		$url = parse_url($_SERVER["REQUEST_URI"]);
		parse_str($url['query'], $get);
		unset($get['toggleMobileView']);
		$query = empty($get) ? '' : '?' . http_build_query($get, '', '&');
		header('Location: ' . $url['path'] . $query);
		exit;
	}
	$handleMobileViewToggling = false;
}

if ($handleMobileViewToggling)
{
	if (isset($_GET['toggleMobileView']))
	{
		$use_mobile = (boolean)$_GET['toggleMobileView'];
		setcookie('use_mobile_view', $use_mobile ? '1' : '0', strtotime('+1 month'), '/', '.' . SERVER_DOMAIN);
		
		$url = parse_url($_SERVER["REQUEST_URI"]);
		parse_str($url['query'], $get);
		unset($get['toggleMobileView']);
		$query = empty($get) ? '' : '?' . http_build_query($get, '', '&');
		header('Location: ' . PROTOCOL_PREFIX . ($use_mobile ? MOBILE_HOST : DESKTOP_HOST) . $url['path'] . $query);
		exit;
	}
	else
	{
		if (!isset($_COOKIE['use_mobile_view']))
		{
			$is_mobile = detectmobilebrowsers();
		}
		else
		{
			$is_mobile = ($_COOKIE['use_mobile_view'] == '1');
		}
		
		if ($is_mobile && !defined('YOHO_MOBILE'))
		{
			header('Location: ' . PROTOCOL_PREFIX . MOBILE_HOST . $_SERVER['REQUEST_URI']);
			exit;
		}
		elseif (!$is_mobile && defined('YOHO_MOBILE'))
		{
			header('Location: ' . PROTOCOL_PREFIX . DESKTOP_HOST . $_SERVER['REQUEST_URI']);
			exit;
		}
	}
}

// Insert functions to use in template files
// Use {insert name=desktop_link} to link to desktop version
function insert_desktop_link($arr)
{
	return PROTOCOL_PREFIX . DESKTOP_HOST . $_SERVER['REQUEST_URI'] . (strpos($_SERVER['REQUEST_URI'],'?') !== false ? '&' : '?') . 'toggleMobileView=0';
}
// Use {insert name=mobile_link} to link to mobile version
function insert_mobile_link($arr)
{
	return PROTOCOL_PREFIX . MOBILE_HOST . $_SERVER['REQUEST_URI'] . (strpos($_SERVER['REQUEST_URI'],'?') !== false ? '&' : '?') . 'toggleMobileView=1';
}
// Use {insert name=current_link} to link to current version
function insert_current_link($arr)
{
	$current_host = empty($_SERVER['HTTP_HOST']) ? $_SERVER['SERVER_NAME'] : $_SERVER['HTTP_HOST'];
	return PROTOCOL_PREFIX . $current_host . $_SERVER['REQUEST_URI'];
}

// Helper functions for linking to specific version
function desktop_url($uri)
{
	return PROTOCOL_PREFIX . DESKTOP_HOST . (substr($uri, 0, 1) == '/' ? '' : '/') . $uri;
}
function mobile_url($uri)
{
	return PROTOCOL_PREFIX . MOBILE_HOST . (substr($uri, 0, 1) == '/' ? '' : '/') . $uri;
}
function absolute_url($uri)
{
	return defined('YOHO_MOBILE') ? mobile_url($uri) : desktop_url($uri);
}

function detectmobilebrowsers()
{
	$useragent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
	return !!(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|ad)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)));
}

?>