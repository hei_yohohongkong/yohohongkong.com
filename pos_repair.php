<?php

/***
 * pos_repair.php
 * by howang 2015-05-05
 *
 * POS System for YOHO Hong Kong
 ***/

define('IN_ECS', true);

// This page don't have mobile version
define('DESKTOP_ONLY', true);

define('FORCE_HTTPS', true);

require(dirname(__FILE__) . '/includes/init.php');

require(ROOT_PATH . 'includes/lib_order.php');
$erpController = new Yoho\cms\Controller\ErpController();
$rmaController = new Yoho\cms\Controller\RmaController();

if ((DEBUG_MODE & 2) != 2)
{
	$smarty->caching = true;
}

// Check admin login
$admin = check_admin_login();
if (!$admin)
{
	show_message($_LANG['not_login'], $_LANG['back_home'], $ecs->url(), 'error');
	exit;
}
$smarty->assign('admin', $admin);

$action = empty($_REQUEST['act']) ? '' : $_REQUEST['act'];


// Save repair data to database
if ($action == 'confirm_repair')
{
	$order_id = empty($_REQUEST['order_id']) ? 0 : intval($_REQUEST['order_id']);
	$order = order_info($order_id);
	$goods_id = empty($_REQUEST['goods_id']) ? 0 : intval($_REQUEST['goods_id']);
	$repair_fee = empty($_REQUEST['repair_fee']) ? 0 : doubleval($_REQUEST['repair_fee']);
	$contact_name = empty($_REQUEST['contact_name']) ? '' : trim($_REQUEST['contact_name']);
	$contact_phone = empty($_REQUEST['mobile']) ? '' : trim($_REQUEST['mobile']);
	$contact_email = empty($_REQUEST['email']) ? '' : trim($_REQUEST['email']);
	$operator = empty($_REQUEST['operator']) ? '' : trim($_REQUEST['operator']);
	$remark = empty($_REQUEST['remark']) ? '' : trim($_REQUEST['remark']);
	
	$repair_order = compact('order_id','goods_id','repair_fee','contact_name','contact_phone','contact_email','operator','remark');
	$repair_order['add_time'] = gmtime();
	
	$error_no = 0;
	do
	{
		if($order_id > 0){
			$dedup = $db->getAll("SELECT * FROM ".$ecs->table('repair_orders')." WHERE goods_id=".$goods_id." AND order_id=".$order_id." AND is_deleted=0 AND repair_order_status<>'".Yoho\cms\Controller\OrderController::DB_REPAIR_ORDER_STATUS_RESOLVED."'");
		} else {// Remark by Anthony: If do not have order id, check by user phone number (maybe is a ws/sa order item)
			$dedup = $db->getAll("SELECT * FROM ".$ecs->table('repair_orders')." WHERE goods_id=".$goods_id." AND order_id=".$order_id." AND contact_phone=".$contact_phone." AND is_deleted=0 AND repair_order_status<>'".Yoho\cms\Controller\OrderController::DB_REPAIR_ORDER_STATUS_RESOLVED."'");
		}

		if(count($dedup)>0){
			show_message('此產品已登記維修單'.$dedup[0]['repair_sn'], '返回 POS 系統', '/pos_repair.php', 'warning');
			exit;
		}
		$repair_order['repair_sn'] = get_repair_sn();
		$repair_sn = $repair_order['repair_sn'];
		$repair_order['repair_order_status'] = Yoho\cms\Controller\OrderController::DB_REPAIR_ORDER_STATUS_PENDING;
		
		$db->autoExecute($ecs->table('repair_orders'), $repair_order, 'INSERT', '', 'SILENT');
		$error_no = $db->errno();

		if ($error_no > 0 && $error_no != 1062){
			die($db->errorMsg());
		}
	}
	
	while ($error_no == 1062); //如果是订单号重复则重新提交数据

	$repair_id = $db->insert_id();
	$repair_order['repair_id'] = $repair_id;

	// insert to rma table
	$new_rma_id = $rmaController->createRma($rmaController::CHANNEL_REPAIR,$repair_id,$order_id,$goods_id,1,$operator,$remark);

	if($order_id>0)
		order_action($order['order_sn'], $order['order_status'], $order['shipping_status'], $order['pay_status'],'新增維修單-<b>'.$repair_order['repair_sn'].'</b>' , $operator);

	$erpController->stockBackFromPOS($repair_order,['warehouse_id'=>2]);

	// update rma id
	$sql = 'update '. $ecs->table('erp_stock') .' set rma_id = "'.$new_rma_id.'" where order_sn = "'.$repair_sn.'" and w_d_style_id = 6 and warehouse_id = 2 ';
	$db->query($sql);

	$smarty->assign('repair_order', $repair_order);
	
	assign_template();
	$position = assign_ur_here(0, '維修 － POS 系統');
	$smarty->assign('page_title',  $position['title']);	// 页面标题
	$smarty->assign('ur_here',     $position['ur_here']);  // 当前位置
	/* meta information */
	$smarty->assign('keywords',    htmlspecialchars($_CFG['shop_keywords']));
	$smarty->assign('description', htmlspecialchars($_CFG['shop_desc']));
	$smarty->display('pos_repair_confirm.dwt');
	exit;
}
elseif ($action == 'print')
{
	$repair_id = empty($_REQUEST['repair_id']) ? 0 : $_REQUEST['repair_id'];
	
	$repair_order = $db->getRow("SELECT * FROM " . $ecs->table('repair_orders') . " WHERE repair_id = '" . $repair_id . "'");
	
	if (empty($repair_order))
	{
		show_message('找不到該維修單', '', '', 'error');
	}
	
	// Get goods info
	$sql = "SELECT * FROM " . $ecs->table('goods') . " WHERE `goods_id` = '" . $repair_order['goods_id'] . "'";
	$goods_list = $db->getAll($sql);
	$smarty->assign('goods_list', $goods_list);
	
	// howang: update salesperson to display name
    $sql = "SELECT `display_name` FROM " . $ecs->table('salesperson') . " WHERE `sales_name` = '" . $repair_order['salesperson'] . "'";
	$repair_order['salesperson'] = $db->getOne($sql);
	
	// Format date and price
	$repair_order['add_time_formatted'] = local_date($_CFG['time_format'], $repair_order['add_time']);
	$repair_order['repair_fee_formatted'] = price_format($repair_order['repair_fee']);
	
	$smarty->assign('repair_order', $repair_order);
	
	$smarty->template_dir = DATA_DIR;
	$smarty->display('repair_print.html');
	exit;
}

/*------------------------------------------------------ */
//-- 判断是否存在缓存，如果存在则调用缓存，反之读取相应内容
/*------------------------------------------------------ */
/* 缓存编号 */
$cache_id = sprintf('%X', crc32($_SESSION['user_rank'] . '-' . $_CFG['lang']));
if (!$smarty->is_cached('pos_repair.dwt', $cache_id))
{
	assign_template();
	//$cat = get_cat_info($cat_id);   // 获得分类的相关信息
	$position = assign_ur_here(0, '維修 － POS 系統');
	$smarty->assign('page_title',  $position['title']);	// 页面标题
	$smarty->assign('ur_here',     $position['ur_here']);  // 当前位置
	/* meta information */
	$smarty->assign('keywords',    htmlspecialchars($_CFG['shop_keywords']));
	$smarty->assign('description', htmlspecialchars($_CFG['shop_desc']));
	
	// List of salesperson available in POS
	$smarty->assign('salespeople', $db->getAll('SELECT sales_id, sales_name FROM ' . $GLOBALS['ecs']->table('salesperson') . ' AS sp WHERE available = 1 AND allow_pos = 1'));
}
$smarty->display('pos_repair.dwt', $cache_id);

exit;

function output_json($obj)
{
	header('Content-Type: application/json');
	echo json_encode($obj);
	exit;
}

?>