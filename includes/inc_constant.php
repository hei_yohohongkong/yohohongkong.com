<?php

/**
 * ECSHOP 常量
 * ============================================================================
 * * 版权所有 2005-2012 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liubo $
 * $Id: inc_constant.php 17217 2011-01-19 06:29:08Z liubo $
*/

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

/* 图片处理相关常数 */
define('ERR_INVALID_IMAGE',         1);
define('ERR_NO_GD',                 2);
define('ERR_IMAGE_NOT_EXISTS',      3);
define('ERR_DIRECTORY_READONLY',    4);
define('ERR_UPLOAD_FAILURE',        5);
define('ERR_INVALID_PARAM',         6);
define('ERR_INVALID_IMAGE_TYPE',    7);

/* 插件相关常数 */
define('ERR_COPYFILE_FAILED',       1);
define('ERR_CREATETABLE_FAILED',    2);
define('ERR_DELETEFILE_FAILED',     3);

/* 商品属性类型常数 */
define('ATTR_TEXT',                 0);
define('ATTR_OPTIONAL',             1);
define('ATTR_TEXTAREA',             2);
define('ATTR_URL',                  3);

/* 会员整合相关常数 */
define('ERR_USERNAME_EXISTS',       1); // 用户名已经存在
define('ERR_EMAIL_EXISTS',          2); // Email已经存在
define('ERR_INVALID_USERID',        3); // 无效的user_id
define('ERR_INVALID_USERNAME',      4); // 无效的用户名
define('ERR_INVALID_PASSWORD',      5); // 密码错误
define('ERR_INVALID_EMAIL',         6); // email错误
define('ERR_USERNAME_NOT_ALLOW',    7); // 用户名不允许注册
define('ERR_EMAIL_NOT_ALLOW',       8); // EMAIL不允许注册

/* 加入购物车失败的错误代码 */
define('ERR_NOT_EXISTS',            1); // 商品不存在
define('ERR_OUT_OF_STOCK',          2); // 商品缺货
define('ERR_NOT_ON_SALE',           3); // 商品已下架
define('ERR_CANNT_ALONE_SALE',      4); // 商品不能单独销售
define('ERR_NO_BASIC_GOODS',        5); // 没有基本件
define('ERR_NEED_SELECT_ATTR',      6); // 需要用户选择属性
define('ERR_INS_EXISTS',            1); // 有相同類型保險服務

/* 购物车商品类型 */
define('CART_GENERAL_GOODS',        0); // 普通商品
define('CART_GROUP_BUY_GOODS',      1); // 团购商品
define('CART_AUCTION_GOODS',        2); // 拍卖商品
define('CART_SNATCH_GOODS',         3); // 夺宝奇兵
define('CART_EXCHANGE_GOODS',       4); // 积分商城

/* 订单状态 */
define('OS_UNCONFIRMED',            0); // 未确认
define('OS_CONFIRMED',              1); // 已确认
define('OS_CANCELED',               2); // 已取消
define('OS_INVALID',                3); // 无效
define('OS_RETURNED',               4); // 退货
define('OS_SPLITED',                5); // 已分单
define('OS_SPLITING_PART',          6); // 部分分单
define('OS_WAITING_REFUND',         7); // 等待全單退款

/* 支付类型 */
define('PAY_ORDER',                 0); // 订单支付
define('PAY_SURPLUS',               1); // 会员预付款

/* 配送状态 */
define('SS_UNSHIPPED',              0); // 未发货
define('SS_SHIPPED',                1); // 已发货
define('SS_RECEIVED',               2); // 已收货
define('SS_PREPARING',              3); // 备货中
define('SS_SHIPPED_PART',           4); // 已发货(部分商品)
define('SS_SHIPPED_ING',            5); // 发货中(处理分单)
define('OS_SHIPPED_PART',           6); // 已发货(部分商品)

/* 支付状态 */
define('PS_UNPAYED',                0); // 未付款
define('PS_PAYING',                 1); // 付款中
define('PS_PAYED',                  2); // 已付款

/* 综合状态 */
define('CS_AWAIT_PAY',              100); // 待付款：货到付款且已发货且未付款，非货到付款且未付款
define('CS_AWAIT_SHIP',             101); // 待发货：货到付款且未发货，非货到付款且已付款且未发货
define('CS_FINISHED',               102); // 已完成：已确认、已付款、已发货
define('CS_AWAIT_SHIP_HK',          103); // 待发货：齊貨，但未发货，且為香港速遞
define('CS_AWAIT_SHIP_LOCKER',      108); // 待发货：齊貨，但未发货，且為智能櫃
define('CS_AWAIT_PICK_HK',          104); // 待发货：齊貨，但未发货，且為門市自取
define('CS_AWAIT_SHIP_NON_HK',      105); // 待发货：齊貨，但未发货，且為外地速遞
define('CS_AWAIT_SHIP_HK_YOHO',     106); // 待发货：齊貨，但未发货，且為本店下單的香港速遞
define('CS_FINISHED_PICK',          107); // 已完成：已确认、已付款、已发货，且為門市自取
define('CS_PAYED_CASH',             110); // 已确认、已付款、未发货、部份出貨，且為現金支付
define('CS_SHIPPED',                201); // 已发货：已发货或收貨確認
define('CS_AWAIT_HK_FLS',           109); // 待出貨(本地速遞) Fls
define('CS_AWAIT_HK_WILSON',        111); // 待出貨(本地速遞) Wilson
define('CS_CANCELED_STAFF',         301); // 由管理員取消
define('CS_CANCELED_SELF',          302); // 由用户取消

/* 發貨單狀態 */
define('DO_DELIVERED',              0); // 已發貨
define('DO_RETURNED',               1); // 退貨
define('DO_NORMAL',                 2); // 正常
define('DO_RECEIVED',               3); // 己收貨
define('DO_PACKED',                 4); // 己pack 貨
define('DO_COLLECTED',              5); // 速遞已收件

/* 缺货处理 */
define('OOS_WAIT',                  0); // 等待货物备齐后再发
define('OOS_CANCEL',                1); // 取消订单
define('OOS_CONSULT',               2); // 与店主协商

/* 帐户明细类型 */
define('SURPLUS_SAVE',              0); // 为帐户冲值
define('SURPLUS_RETURN',            1); // 从帐户提款

/* 评论状态 */
define('COMMENT_UNCHECKED',         0); // 未审核
define('COMMENT_CHECKED',           1); // 已审核或已回复(允许显示)
define('COMMENT_REPLYED',           2); // 该评论的内容属于回复

/* 红包发放的方式 */
define('SEND_BY_USER',              0); // 按用户发放
define('SEND_BY_GOODS',             1); // 按商品发放
define('SEND_BY_ORDER',             2); // 按订单发放
define('SEND_BY_PRINT',             3); // 线下发放

/* 广告的类型 */
define('IMG_AD',                    0); // 图片广告
define('FALSH_AD',                  1); // flash广告
define('CODE_AD',                   2); // 代码广告
define('TEXT_AD',                   3); // 文字广告

/* 是否需要用户选择属性 */
define('ATTR_NOT_NEED_SELECT',      0); // 不需要选择
define('ATTR_NEED_SELECT',          1); // 需要选择

/* 用户中心留言类型 */
define('M_MESSAGE',                 0); // 留言
define('M_COMPLAINT',               1); // 投诉
define('M_ENQUIRY',                 2); // 询问
define('M_CUSTOME',                 3); // 售后
define('M_BUY',                     4); // 求购
define('M_BUSINESS',                5); // 商家
define('M_COMMENT',                 6); // 评论

/* 团购活动状态 */
define('GBS_PRE_START',             0); // 未开始
define('GBS_UNDER_WAY',             1); // 进行中
define('GBS_FINISHED',              2); // 已结束
define('GBS_SUCCEED',               3); // 团购成功（可以发货了）
define('GBS_FAIL',                  4); // 团购失败

/* 红包是否发送邮件 */
define('BONUS_NOT_MAIL',            0);
define('BONUS_MAIL_SUCCEED',        1);
define('BONUS_MAIL_FAIL',           2);

/* 商品活动类型 */
define('GAT_SNATCH',                0);
define('GAT_GROUP_BUY',             1);
define('GAT_AUCTION',               2);
define('GAT_POINT_BUY',             3);
define('GAT_PACKAGE',               4); // 超值礼包

/* 帐号变动类型 */
define('ACT_SAVING',                0);     // 帐户冲值
define('ACT_DRAWING',               1);     // 帐户提款
define('ACT_ADJUSTING',             2);     // 调节帐户
define('ACT_OTHER',                99);     // 其他类型

/* 密码加密方法 */
define('PWD_MD5',                   1);  //md5加密方式
define('PWD_PRE_SALT',              2);  //前置验证串的加密方式
define('PWD_SUF_SALT',              3);  //后置验证串的加密方式

/* 文章分类类型 */
define('COMMON_CAT',                1); //Post category and post parent category type
define('SYSTEM_CAT',                2); //系统默认分类
define('INFO_CAT',                  3); //网店信息分类
define('UPHELP_CAT',                4); //Static parent category type
define('HELP_CAT',                  5); //Static category type
define('SUPPORT_CAT',               6); //Q&A category and Q&A parent category type

/* 活动状态 */
define('PRE_START',                 0); // 未开始
define('UNDER_WAY',                 1); // 进行中
define('FINISHED',                  2); // 已结束
define('SETTLED',                   3); // 已处理

/* 验证码 */
define('CAPTCHA_REGISTER',          1); //注册时使用验证码
define('CAPTCHA_LOGIN',             2); //登录时使用验证码
define('CAPTCHA_COMMENT',           4); //评论时使用验证码
define('CAPTCHA_ADMIN',             8); //后台登录时使用验证码
define('CAPTCHA_LOGIN_FAIL',       16); //登录失败后显示验证码
define('CAPTCHA_MESSAGE',          32); //留言时使用验证码

/* 优惠活动的优惠范围 */
define('FAR_ALL',                   0); // 全部商品
define('FAR_CATEGORY',              1); // 按分类选择
define('FAR_BRAND',                 2); // 按品牌选择
define('FAR_GOODS',                 3); // 按商品选择

/* 优惠活动的优惠方式 */
define('FAT_GOODS',                 0); // 送赠品或优惠购买
define('FAT_PRICE',                 1); // 现金减免
define('FAT_DISCOUNT',              2); // 价格打折优惠

/* 评论条件 */
define('COMMENT_ALL',               0); //所有用户可以评论
define('COMMENT_LOGIN',             1); //只有登录用户可以评论
define('COMMENT_CUSTOM',            2); //只有有过一次以上购买行为的用户可以评论
define('COMMENT_BOUGHT',            3); //只有购买够该商品的人可以评论

/* 减库存时机 */
define('SDT_SHIP',                  0); // 发货时
define('SDT_PLACE',                 1); // 下订单时

/* 加密方式 */
define('ENCRYPT_ZC',                1); //zc加密方式
define('ENCRYPT_UC',                2); //uc加密方式

/* 商品类别 */
define('G_REAL',                    1); //实体商品
define('G_CARD',                    0); //虚拟卡

/* 积分兑换 */
define('TO_P',                      0); //兑换到商城消费积分
define('FROM_P',                    1); //用商城消费积分兑换
define('TO_R',                      2); //兑换到商城等级积分
define('FROM_R',                    3); //用商城等级积分兑换

/* 支付宝商家账户 */
define('ALIPAY_AUTH', 'gh0bis45h89m5mwcoe85us4qrwispes0');
define('ALIPAY_ID', '2088002052150939');

/* 添加feed事件到UC的TYPE*/
define('BUY_GOODS',                 1); //购买商品
define('COMMENT_GOODS',             2); //添加商品评论

/* 邮件发送用户 */
define('SEND_LIST', 0);
define('SEND_USER', 1);
define('SEND_RANK', 2);

/* license接口 */
define('LICENSE_VERSION', '1.0');

/* Mass Key 狀態 */
define('MK_CANCEL',     0);
define('MK_UNDELIVERY', 1);
define('MK_DELIVERY',   2);
define('MK_EXPORT',     3);
define('MK_IMPORT',     4);
define('MK_MAIL',       5);
define('MK_FINISH',     6);

/* 配送方式 */
define('SHIP_LIST', 'cac|city_express|ems|flat|fpd|post_express|post_mail|presswork|sf_express|sto_express|yto|zto');

/* ERP log status */
define('ERP_CREATE',        1); //Create order
define('ERP_UPDATE',        2); //Modified order
define('ERP_REMOVE',        3); //Delete order
define('ERP_APPROVE',       4); //Approve/reject order
define('ERP_UNAPPROVE',     5); //Unapprove order
define('ERP_PAID',          6); //Paid order
define('ERP_COMPLETE',      7); //Complete order
define('ERP_CRON_REMOVE',   8); //Cron delete order

/* Refund status */
define('REFUND_PENDING',        1); //Refund pending
define('REFUND_PROCESS',        2); //Refund processing
define('REFUND_FINISH',         3); //Refund finished
define('REFUND_REJECT',         4); //Refund rejected
define('REFUND_PROCESSED',      5); //Refund processed

/* Refund type */
define('REFUND_NORMAL',         0); //Normal refund
define('REFUND_COMPENSATE',     1); //Compensate refund
define('REFUND_WHOLE_ORDER',    2); //Whole order refund
define('REFUND_CUSTOM',         3); //Custom refund

/* Order fraud risk status */
define('FRAUD_CLEAR',       0); //No risk
define('FRAUD_CONFIRM',     1); //Is fraud
define('FRAUD_HIGH',        2); //High risk
define('FRAUD_MEDIUM',      3); //Medium risk
define('FRAUD_LOW',         4); //Low risk

/* CRM platform */
define('CRM_PLATFORM_GMAIL',       1); //Gmail
define('CRM_PLATFORM_FACEBOOK',    2); //Facebook
define('CRM_PLATFORM_WECHAT',      3); //WeChat
define('CRM_PLATFORM_CHATRA',      4); //Chatra
define('CRM_PLATFORM_AIRCALL',     5); //Aircall
define('CRM_PLATFORM_TWILIO',      6); //Twilio

/* CRM request type */
define('CRM_REQUEST_UNCATEGORIZED',     0); //Uncategorized
define('CRM_REQUEST_ORDER_QUERY',       1); //Order query
define('CRM_REQUEST_ORDER_PAYMENT',     2); //Order payment
define('CRM_REQUEST_ORDER_SHIPPING',    3); //Order shipping
define('CRM_REQUEST_ORDER_AFTER_SALE',  4); //Order after sale, eg refund
define('CRM_REQUEST_SHIP_WRONG',        5); //Order shipped worng product
define('CRM_REQUEST_COMPLAIN',          6); //Complains
define('CRM_REQUEST_QUOTATION',         7); //Quotation
define('CRM_REQUEST_COOPERATION',       8); //Cooperation
define('CRM_REQUEST_QUERY',             9); //Other query

/* CRM request status */
define('CRM_STATUS_PENDING',        0); //Request pending
define('CRM_STATUS_PROCESSING',     1); //Request processing
define('CRM_STATUS_REPLIED',        2); //Request replied
define('CRM_STATUS_FINISHED',       3); //Request finished
define('CRM_STATUS_ARCHIVED',       4); //Request archived

/* CRM request status */
define('CRM_PRIORITY_NOT_SET',      0); //Not set priority
define('CRM_PRIORITY_LOW',          1); //Low priority
define('CRM_PRIORITY_MEDIUM',       2); //Medium priority
define('CRM_PRIORITY_HIGH',         3); //High priority

/* CRM log type */
define('CRM_LOG_TYPE_INSERT',   0); //Insert record
define('CRM_LOG_TYPE_UPDATE',   1); //Update record
define('CRM_LOG_TYPE_REPLY',    2); //Reply message

/* CRM Aircall event type */
define('CRM_AIRCALL_ANSWER',   1); //Call answered
define('CRM_AIRCALL_MISSED',   2); //Call missed

/* Auto price suggestion type */
define('AUTO_PRICE_TYPE_NORMAL',    0); //Normal price
define('AUTO_PRICE_TYPE_DEAD',      1); //On sale
define('AUTO_PRICE_TYPE_VIP',       2); //VIP price

/* Auto price suggestion status */
define('AUTO_PRICE_STATUS_SUGGESTED',   0); //Suggestion proposed
define('AUTO_PRICE_STATUS_APPLIED',     1); //Suggestion applied
define('AUTO_PRICE_STATUS_EXPIRED',     2); //Suggestion ended
define('AUTO_PRICE_STATUS_REJECTED',    3); //Suggestion rejected

/* Special user rank program */
define('RANK_PROGRAM_HIDE',   0); //Program not shown
define('RANK_PROGRAM_SHOW',   1); //Program running
define('RANK_PROGRAM_END',    2); //Program ended

/* Rank program application status */
define('RANK_APPLICATION_WAIT',         0); //Waiting approve
define('RANK_APPLICATION_PREPARE',      1); //Prepare approved
define('RANK_APPLICATION_APPROVED',     2); //Approved
define('RANK_APPLICATION_DISAPPROVE',   3); //Disapproved
define('RANK_APPLICATION_FURTHER',      4); //Require further validation


/* verify code status */
define('VERIFY_CODE_UNVERIFY',        0); //Unverify
define('VERIFY_CODE_REERIFY',         1); //Reverify
define('VERIFY_CODE_FINISH',          2); //Finish

/* Offline payment auto process status */
define('OFFLINE_PAYTMENT_PENDING',      0); //Order not pay
define('OFFLINE_PAYTMENT_UPLOADED',     1); //Receipt uploaded
define('OFFLINE_PAYTMENT_EXTRA_INFO',   2); //Require proof supplement
define('OFFLINE_PAYTMENT_FINISHED',     3); //Order marked paid
define('OFFLINE_PAYTMENT_CLOSED',       4); //Not process

/* Accounting posting status */
define('ACTG_POST_PENDING',     0); //Pending posting
define('ACTG_POST_APPROVED',    1); //Posting approved
define('ACTG_POST_REJECTED',    -1); //Posting rejected

/* ERP order revise status */
define('PO_REVISE_CREATED',     0); //Revise created
define('PO_REVISE_SUBMITTED',   1); //Revise waiting appove
define('PO_REVISE_APPROVED',    2); //Revise appoved
define('PO_REVISE_DISAPPROVED', 3); //Revise disappoved

?>