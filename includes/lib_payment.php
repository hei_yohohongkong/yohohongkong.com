<?php

/**
 * ECSHOP 支付接口函数库
 * ============================================================================
 * 版权所有 2005-2010 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: yehuaixiao $
 * $Id: lib_payment.php 17218 2011-01-24 04:10:41Z yehuaixiao $
 */

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

require_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_accounting.php');

/**
 * 取得返回信息地址
 * @param   string  $code   支付方式代码
 */
function return_url($code)
{
    return get_secure_ecs_url() . 'respond.php?code=' . $code;
}

function get_secure_ecs_url()
{
	$old_https = isset($_SERVER['HTTPS']) ? $_SERVER['HTTPS'] : null;
	$_SERVER['HTTPS'] = 'on';
	$secure_ecs_url = $GLOBALS['ecs']->url();
	if (is_null($old_https))
	{
		unset($_SERVER['HTTPS']);
	}
	else
	{
		$_SERVER['HTTPS'] = $old_https;
	}
	return $secure_ecs_url;
}

/**
 *  取得某支付方式信息
 *  @param  string  $code   支付方式代码
 */
function get_payment($code)
{
    $sql = 'SELECT * FROM ' . $GLOBALS['ecs']->table('payment').
           " WHERE pay_code = '$code' AND enabled = '1'";
    $payment = $GLOBALS['db']->getRow($sql);

    if ($payment)
    {
        $config_list = unserialize($payment['pay_config']);

        foreach ($config_list AS $config)
        {
            $payment[$config['name']] = $config['value'];
        }
    }

    return $payment;
}

/**
 *  通过订单sn取得订单ID
 *  @param  string  $order_sn   订单sn
 *  @param  blob    $voucher    是否为会员充值
 */
function get_order_id_by_sn($order_sn, $voucher = 'false')
{
    if ($voucher == 'true')
    {
        if(is_numeric($order_sn))
        {
            return $GLOBALS['db']->getOne("SELECT log_id FROM " . $GLOBALS['ecs']->table('pay_log') . " WHERE order_id=" . $order_sn . ' AND order_type=1');
        }
        else
        {
            return "";
        }
    }
    else
    {
        if(is_numeric($order_sn))
        {
            $sql = 'SELECT order_id FROM ' . $GLOBALS['ecs']->table('order_info'). " WHERE order_sn = '$order_sn'";
            $order_id = $GLOBALS['db']->getOne($sql);
        }
        if (!empty($order_id))
        {
            $pay_log_id = $GLOBALS['db']->getOne("SELECT log_id FROM " . $GLOBALS['ecs']->table('pay_log') . " WHERE order_id='" . $order_id . "'");
            return $pay_log_id;
        }
        else
        {
            return "";
        }
    }
}

/**
 *  通过订单ID取得订单商品名称
 *  @param  string  $order_id   订单ID
 */
function get_goods_name_by_id($order_id)
{
    $sql = 'SELECT goods_name FROM ' . $GLOBALS['ecs']->table('order_goods'). " WHERE order_id = '$order_id'";
    $goods_name = $GLOBALS['db']->getCol($sql);
    return implode(',', $goods_name);
}

/**
 * 检查支付的金额是否与订单相符
 *
 * @access  public
 * @param   string   $log_id      支付编号
 * @param   float    $money       支付接口返回的金额
 * @return  true
 */
function check_money($log_id, $money)
{
    $sql = 'SELECT order_amount FROM ' . $GLOBALS['ecs']->table('pay_log') .
              " WHERE log_id = '$log_id'";
    $amount = $GLOBALS['db']->getOne($sql);

    if ($money == $amount)
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
 * 修改订单的支付状态
 *
 * @access  public
 * @param   string  $log_id     支付编号
 * @param   integer $pay_status 状态
 * @param   string  $note       备注
 * @return  void
 */
function order_paid($log_id, $pay_status = PS_PAYED, $note = '')
{
    /* 取得支付编号 */
    $log_id = intval($log_id);
    if ($log_id > 0)
    {
        /* 取得要修改的支付记录信息 */
        $sql = "SELECT * FROM " . $GLOBALS['ecs']->table('pay_log') .
                " WHERE log_id = '$log_id'";
        $pay_log = $GLOBALS['db']->getRow($sql);
        if ($pay_log && $pay_log['is_paid'] == 0)
        {
            /* 修改此次支付操作的状态为已付款 */
            $sql = 'UPDATE ' . $GLOBALS['ecs']->table('pay_log') .
                    " SET is_paid = '1' WHERE log_id = '$log_id'";
            $GLOBALS['db']->query($sql);

            /* 根据记录类型做相应处理 */
            if ($pay_log['order_type'] == PAY_ORDER)
            {
                /* 取得订单信息 */
                $sql = 'SELECT order_id, user_id, order_type, order_sn, consignee, address, tel, email, shipping_id, extension_code, extension_id, goods_amount, order_amount ' .
                        'FROM ' . $GLOBALS['ecs']->table('order_info') .
                       " WHERE order_id = '$pay_log[order_id]'";
                $order    = $GLOBALS['db']->getRow($sql);
                $order_id = $order['order_id'];
                $order_sn = $order['order_sn'];

                // Assign to global variable so we can access it in respond.php
                $GLOBALS['paid_order'] = $order;

                /* 修改订单状态为已付款 */
                $sql = 'UPDATE ' . $GLOBALS['ecs']->table('order_info') .
                            " SET order_status = '" . OS_CONFIRMED . "', " .
                                " confirm_time = '" . gmtime() . "', " .
                                " pay_status = '$pay_status', " .
                                " pay_time = '".gmtime()."', " .
                                " money_paid = money_paid + order_amount," .
                                " order_amount = 0 ".
                       "WHERE order_id = '$order_id'";
                $GLOBALS['db']->query($sql);

                // Accounting System, order paid
                $GLOBALS['actgHooks']->orderPaid($order_id, $order['order_amount']);

                /* 记录订单操作记录 */
                order_action($order_sn, OS_CONFIRMED, SS_UNSHIPPED, $pay_status, $note, $GLOBALS['_LANG']['buyer']);

                /* 如果需要，发短信 */
                if ($GLOBALS['_CFG']['sms_order_payed'] == '1' && $GLOBALS['_CFG']['sms_shop_mobile'] != '')
                {
                    include_once(ROOT_PATH.'includes/cls_sms.php');
                    $sms = new sms();
                    $sms->send($GLOBALS['_CFG']['sms_shop_mobile'],
                        sprintf($GLOBALS['_LANG']['order_payed_sms'], $order_sn, $order['consignee'], $order['tel']), 0);
                }

                // YOHO: Send email
                if (
                    can_send_mail($order['email'], true) 
                    && (
                            (!isset($order['order_type']))
                            || (isset($order['order_type']) 
                            && !in_array($order['order_type'], [Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE, Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT])
                            )
                        )
                    )
                {
                    $tpl = get_mail_template('payment_received');
                    $GLOBALS['smarty']->assign('order', $order);
                    $GLOBALS['smarty']->assign('shop_name', $GLOBALS['_CFG']['shop_name']);
                    $GLOBALS['smarty']->assign('send_date', local_date($GLOBALS['_CFG']['date_format']));
                    $GLOBALS['smarty']->assign('sent_date', local_date($GLOBALS['_CFG']['date_format']));
                    $content = $GLOBALS['smarty']->fetch('str:' . $tpl['template_content']);
                    send_mail($order['consignee'], $order['email'], $tpl['template_subject'] . ' - ' . $order['order_sn'], $content, $tpl['is_html']);


                    $sql = "SELECT goods_id, goods_name ".
                            " FROM " . $GLOBALS['ecs']->table('order_goods')." ".
                            " WHERE order_id = '$pay_log[order_id]' ".
                            " AND is_insurance = '1' ".
                            " AND is_package = '0' ".
                            " AND flashdeal_id = '0' ".
                            " AND insurance_of != '0' ";
                    $order_ins_goods = $GLOBALS['db']->getAll($sql);

                    if (!empty($order_ins_goods)){
                        $arr_order_ins_goods_name = array();
                        foreach ($order_ins_goods as $ins_goods){
                            array_push($arr_order_ins_goods_name, $ins_goods['goods_name']);
                        }
                        if (!empty($arr_order_ins_goods_name)){
                            $str_order_ins_goods_name = implode('<br>', $arr_order_ins_goods_name);
                            $order['ins_goods_name'] = $str_order_ins_goods_name;
                            $tpl = get_mail_template('insurance_notice');
                            $GLOBALS['smarty']->assign('order', $order);
                            $GLOBALS['smarty']->assign('shop_name', $GLOBALS['_CFG']['shop_name']);
                            $GLOBALS['smarty']->assign('send_date', local_date($GLOBALS['_CFG']['date_format']));
                            $GLOBALS['smarty']->assign('sent_date', local_date($GLOBALS['_CFG']['date_format']));
                            $content = $GLOBALS['smarty']->fetch('str:' . $tpl['template_content']);
                            send_mail($order['consignee'], $order['email'], $tpl['template_subject'] . ' - ' . $order['order_sn'], $content, $tpl['is_html']);
                        }
                    }
                }

                /* 对虚拟商品的支持 */
                $virtual_goods = get_virtual_goods($order_id);
                if (!empty($virtual_goods))
                {
                    $msg = '';
                    if (!virtual_goods_ship($virtual_goods, $msg, $order_sn, true))
                    {
                        $GLOBALS['_LANG']['pay_success'] .= '<div style="color:red;">'.$msg.'</div>'.$GLOBALS['_LANG']['virtual_goods_ship_fail'];
                    }

                    /* 如果订单没有配送方式，自动完成发货操作 */
                    if ($order['shipping_id'] == -1)
                    {
                        /* 将订单标识为已发货状态，并记录发货记录 */
                        $sql = 'UPDATE ' . $GLOBALS['ecs']->table('order_info') .
                               " SET shipping_status = '" . SS_SHIPPED . "', shipping_time = '" . gmtime() . "'" .
                               " WHERE order_id = '$order_id'";
                        $GLOBALS['db']->query($sql);

                         /* 记录订单操作记录 */
                        order_action($order_sn, OS_CONFIRMED, SS_SHIPPED, $pay_status, $note, $GLOBALS['_LANG']['buyer']);
                        $integral = integral_to_give($order);
                        log_account_change($order['user_id'], 0, 0, intval($integral['rank_points']), intval($integral['custom_points']), sprintf($GLOBALS['_LANG']['order_gift_integral'], $order['order_sn']), ACT_OTHER, 0, ['sales_order_id' => $order_id]);
                    }
                }

                /* 清除缓存 */
//                clear_cache_files();
                //clear_cache_files('flashdeal');
                //clear_cache_files('goods');

            }
            elseif ($pay_log['order_type'] == PAY_SURPLUS)
            {
                /* 更新会员预付款的到款状态 */
                $sql = 'UPDATE ' . $GLOBALS['ecs']->table('user_account') .
                       " SET paid_time = '" .gmtime(). "', is_paid = 1" .
                       " WHERE id = '$pay_log[order_id]' LIMIT 1";
                $GLOBALS['db']->query($sql);

                /* 取得添加预付款的用户以及金额 */
                $sql = "SELECT user_id, amount FROM " . $GLOBALS['ecs']->table('user_account') .
                        " WHERE id = '$pay_log[order_id]'";
                $arr = $GLOBALS['db']->getRow($sql);

                /* 修改会员帐户金额 */
                $_LANG = array();
                include_once(ROOT_PATH . 'languages/' . $GLOBALS['_CFG']['lang'] . '/user.php');
                log_account_change($arr['user_id'], $arr['amount'], 0, 0, 0, $_LANG['surplus_type_0'], ACT_SAVING);
            }
        }
        else
        {
            /* 取得已发货的虚拟商品信息 */
            $post_virtual_goods = get_virtual_goods($pay_log['order_id'], true);

            /* 有已发货的虚拟商品 */
            if (!empty($post_virtual_goods))
            {
                $msg = '';
                /* 检查两次刷新时间有无超过12小时 */
                $sql = 'SELECT pay_time, order_sn FROM ' . $GLOBALS['ecs']->table('order_info') . " WHERE order_id = '$pay_log[order_id]'";
                $row = $GLOBALS['db']->getRow($sql);
                $intval_time = gmtime() - $row['pay_time'];
                if ($intval_time >= 0 && $intval_time < 3600 * 12)
                {
                    $virtual_card = array();
                    foreach ($post_virtual_goods as $code => $goods_list)
                    {
                        /* 只处理虚拟卡 */
                        if ($code == 'virtual_card')
                        {
                            foreach ($goods_list as $goods)
                            {
                                if ($info = virtual_card_result($row['order_sn'], $goods))
                                {
                                    $virtual_card[] = array('goods_id'=>$goods['goods_id'], 'goods_name'=>$goods['goods_name'], 'info'=>$info);
                                }
                            }

                            $GLOBALS['smarty']->assign('virtual_card',      $virtual_card);
                        }
                    }
                }
                else
                {
                    $msg = '<div>' .  $GLOBALS['_LANG']['please_view_order_detail'] . '</div>';
                }

                $GLOBALS['_LANG']['pay_success'] .= $msg;
            }

           /* 取得未发货虚拟商品 */
           $virtual_goods = get_virtual_goods($pay_log['order_id'], false);
           if (!empty($virtual_goods))
           {
               $GLOBALS['_LANG']['pay_success'] .= '<br />' . $GLOBALS['_LANG']['virtual_goods_ship_fail'];
           }
        }
    }
}

/**
 * Check online payment
 * 
 * @param int $order_id order id
 * @param int $pay_id   payment record id
 * 
 * @return int 0 or 1
 */
function is_online_payment($order_id = 0, $pay_id = 0) {
    if (!empty($order_id)) {
        $sql = "SELECT 1 FROM " . $GLOBALS['ecs']->table('order_info') . " oi " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('payment') . " p ON oi.pay_id = p.pay_id " .
            "WHERE oi.order_id = $order_id AND p.is_online_pay = 1";
    } elseif (!empty($pay_id)) {
        $sql = "SELECT 1 FROM " . $GLOBALS['ecs']->table('payment') . " WHERE pay_id = $pay_id AND is_online_pay = 1";
    } else {
        return 0;
    }
    return $GLOBALS['db']->getOne($sql);
}

/**
 * Log transaction risk level
 * 
 * @param string $txn      txn id
 * @param int    $order_id order id
 * @param int    $pay_id   payment type
 * @param int    $fraud    risk level
 * 
 * @return void
 */
function payment_risk_level_log($txn, $order_id, $pay_id, $fraud = 0, $success = 0)
{
    if (!empty($txn) && !empty($order_id) && !empty($pay_id)) {
        $fsql = "INSERT INTO " . $GLOBALS['ecs']->table('order_fraud') . " (txn, order_id, status, pay_id, success) VALUES ('" . $txn . "', $order_id, $fraud, $pay_id, $success)";
        $GLOBALS['db']->query($fsql);
    }
}
?>
