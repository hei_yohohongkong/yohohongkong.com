<?php return array (
  'sans-serif' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'Helvetica',
    'bold' => DOMPDF_FONT_DIR . 'Helvetica-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Helvetica-Oblique',
    'bold_italic' => DOMPDF_FONT_DIR . 'Helvetica-BoldOblique',
  ),
  'times' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'Times-Roman',
    'bold' => DOMPDF_FONT_DIR . 'Times-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Times-Italic',
    'bold_italic' => DOMPDF_FONT_DIR . 'Times-BoldItalic',
  ),
  'times-roman' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'Times-Roman',
    'bold' => DOMPDF_FONT_DIR . 'Times-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Times-Italic',
    'bold_italic' => DOMPDF_FONT_DIR . 'Times-BoldItalic',
  ),
  'courier' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'Courier',
    'bold' => DOMPDF_FONT_DIR . 'Courier-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Courier-Oblique',
    'bold_italic' => DOMPDF_FONT_DIR . 'Courier-BoldOblique',
  ),
  'helvetica' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'Helvetica',
    'bold' => DOMPDF_FONT_DIR . 'Helvetica-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Helvetica-Oblique',
    'bold_italic' => DOMPDF_FONT_DIR . 'Helvetica-BoldOblique',
  ),
  'zapfdingbats' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'ZapfDingbats',
    'bold' => DOMPDF_FONT_DIR . 'ZapfDingbats',
    'italic' => DOMPDF_FONT_DIR . 'ZapfDingbats',
    'bold_italic' => DOMPDF_FONT_DIR . 'ZapfDingbats',
  ),
  'symbol' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'Symbol',
    'bold' => DOMPDF_FONT_DIR . 'Symbol',
    'italic' => DOMPDF_FONT_DIR . 'Symbol',
    'bold_italic' => DOMPDF_FONT_DIR . 'Symbol',
  ),
  'serif' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'Times-Roman',
    'bold' => DOMPDF_FONT_DIR . 'Times-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Times-Italic',
    'bold_italic' => DOMPDF_FONT_DIR . 'Times-BoldItalic',
  ),
  'monospace' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'Courier',
    'bold' => DOMPDF_FONT_DIR . 'Courier-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Courier-Oblique',
    'bold_italic' => DOMPDF_FONT_DIR . 'Courier-BoldOblique',
  ),
  'fixed' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'Courier',
    'bold' => DOMPDF_FONT_DIR . 'Courier-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Courier-Oblique',
    'bold_italic' => DOMPDF_FONT_DIR . 'Courier-BoldOblique',
  ),
  'dejavu sans' => 
  array (
    'bold' => DOMPDF_FONT_DIR . 'DejaVuSans-Bold',
    'bold_italic' => DOMPDF_FONT_DIR . 'DejaVuSans-BoldOblique',
    'italic' => DOMPDF_FONT_DIR . 'DejaVuSans-Oblique',
    'normal' => DOMPDF_FONT_DIR . 'DejaVuSans',
  ),
  'dejavu sans light' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'DejaVuSans-ExtraLight',
  ),
  'dejavu sans condensed' => 
  array (
    'bold' => DOMPDF_FONT_DIR . 'DejaVuSansCondensed-Bold',
    'bold_italic' => DOMPDF_FONT_DIR . 'DejaVuSansCondensed-BoldOblique',
    'italic' => DOMPDF_FONT_DIR . 'DejaVuSansCondensed-Oblique',
    'normal' => DOMPDF_FONT_DIR . 'DejaVuSansCondensed',
  ),
  'dejavu sans mono' => 
  array (
    'bold' => DOMPDF_FONT_DIR . 'DejaVuSansMono-Bold',
    'bold_italic' => DOMPDF_FONT_DIR . 'DejaVuSansMono-BoldOblique',
    'italic' => DOMPDF_FONT_DIR . 'DejaVuSansMono-Oblique',
    'normal' => DOMPDF_FONT_DIR . 'DejaVuSansMono',
  ),
  'dejavu serif' => 
  array (
    'bold' => DOMPDF_FONT_DIR . 'DejaVuSerif-Bold',
    'bold_italic' => DOMPDF_FONT_DIR . 'DejaVuSerif-BoldItalic',
    'italic' => DOMPDF_FONT_DIR . 'DejaVuSerif-Italic',
    'normal' => DOMPDF_FONT_DIR . 'DejaVuSerif',
  ),
  'dejavu serif condensed' => 
  array (
    'bold' => DOMPDF_FONT_DIR . 'DejaVuSerifCondensed-Bold',
    'bold_italic' => DOMPDF_FONT_DIR . 'DejaVuSerifCondensed-BoldItalic',
    'italic' => DOMPDF_FONT_DIR . 'DejaVuSerifCondensed-Italic',
    'normal' => DOMPDF_FONT_DIR . 'DejaVuSerifCondensed',
  ),
  'microsoft jhenghei' => 
  array (
    'normal' => DOMPDF_FONT_DIR . '0b925ad17754f8b7c3b3219fd3bc7c33',
    'bold' => DOMPDF_FONT_DIR . 'b5ad079583242fc62e485176a15783f8',
  ),
  'arial' => 
  array (
    'normal' => DOMPDF_FONT_DIR . '0a1ebe004bda4400a9a96962294d893e',
    'bold' => DOMPDF_FONT_DIR . '419543e92f0661da2e91c9124132ac0c',
    'bold_italic' => DOMPDF_FONT_DIR . '15e59c7ce4ac9711fdad713e081cdf51',
    'italic' => DOMPDF_FONT_DIR . 'b28873e5a5e653f87bc905da42a2b4ee',
  ),
  'century gothic' => 
  array (
    'normal' => DOMPDF_FONT_DIR . '276738fea9fc9e15c69d7c18cb2c9964',
    'bold' => DOMPDF_FONT_DIR . '34c78d09fe4605bf827f432e376d11cb',
    'bold_italic' => DOMPDF_FONT_DIR . 'f1057a480c7ad5b4ec07999b8e0b4159',
    'italic' => DOMPDF_FONT_DIR . '3942c3a9e120fed7d84f89964eedccdb',
  ),
) ?>