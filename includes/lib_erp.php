<?php

function get_goods_stock($goods_id,$goods_attr_id_list='',$agency_id=0)
{
	$attr_id=get_attr_id($goods_id,$goods_attr_id_list);
	//remark: temporary do not apply supplierController here, to be refactored
	$sub_sql="select warehouse_id from ".$GLOBALS['ecs']->table('erp_warehouse')." where is_on_sale='1' and is_valid='1'";
	if($agency_id>0)
	{
		$sub_sql.=" and agency_id='".$agency_id."'";
	}
	$sql="select sum(goods_qty) from ".$GLOBALS['ecs']->table('erp_goods_attr_stock')." where attr_id='".$attr_id."' and warehouse_id in (".$sub_sql.") ";
	return $GLOBALS['db']->getOne($sql);
}

if(!function_exists('get_attr_id'))
{
	function get_attr_id($goods_id,$goods_attr_id_list='')
	{
		$attr_id=0;
		if(empty($goods_attr_id_list))
		{
			$sql="select attr_id from ".$GLOBALS['ecs']->table('erp_goods_attr')." where goods_id='".$goods_id."' and goods_attr_id_list='$$'";
			return $GLOBALS['db']->getOne($sql);
		}
		else
		{
			if(!is_array($goods_attr_id_list))
			{
				$goods_attr_id_list=explode(',',$goods_attr_id_list);
			}
			$sql="select attr_id,goods_attr_id_list from ".$GLOBALS['ecs']->table('erp_goods_attr')." where goods_id='".$goods_id."'";
			$attrs=$GLOBALS['db']->getAll($sql);
			if(!empty($attrs))
			{
				foreach($attrs as $key =>$item)
				{
					$attr_id=$item['attr_id'];
					$attr_id_list=explode(',',$item['goods_attr_id_list']);
					$tmp1=array_diff($goods_attr_id_list,$attr_id_list);
					$tmp2=array_diff($attr_id_list,$goods_attr_id_list);
					if(empty($tmp1) &&empty($tmp2))
					{
						return $attr_id;
					}
				}
			}
		}
	}
}
?>
