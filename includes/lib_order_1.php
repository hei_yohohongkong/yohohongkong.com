<?php

/**
 * 取得购物车商品
 * @param   int      $type   类型：默认普通商品
 * @param  array   $custom_prices   類型：POS custom price
 * @return  array   购物车商品数组
 */

function cart_goods_1($type = CART_GENERAL_GOODS, $custom_prices = array(), $user_name = null, $user_rank = 0, $sa=-1, $vip_package = true)
{
    $packageController = new Yoho\cms\Controller\PackageController();
    $orderController = new Yoho\cms\Controller\OrderController();

    if(isset($user_name))
    {
        $sql = "SELECT ur.rank_id FROM ".$GLOBALS['ecs']->table('user_rank')." AS ur ".
                " LEFT JOIN ".$GLOBALS['ecs']->table('users')." AS u ON u.user_name = '$user_name' ".
                " WHERE special_rank = '0' AND min_points <= u.rank_points AND max_points > u.rank_points LIMIT 1";
        $user_rank = $GLOBALS['db']->getOne($sql);
    }

    if($vip_package) {
        $price_sql = "LEAST(IFNULL(mp.user_price, c.goods_price),c.goods_price) as goods_price";
    } else {
        $price_sql = "IF(c.is_package = 0, LEAST(IFNULL(mp.user_price, c.goods_price),c.goods_price), c.goods_price) as goods_price";
    }

    $sql = "SELECT c.rec_id, c.user_id, g.cat_id, g.brand_id, c.goods_id, c.goods_name, c.goods_sn, c.goods_number, " .
            " (g.integral * c.goods_number) AS max_integral_amount, c.market_price,".
            $price_sql .
            ", c.goods_attr, c.is_real, c.extension_code, c.parent_id, c.is_gift, c.is_package, c.is_shipping, " .
            "LEAST(IFNULL(mp.user_price, c.goods_price),c.goods_price) * c.goods_number AS subtotal " .
            "FROM " . $GLOBALS['ecs']->table('cart1') ." as c ".
            "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON g.goods_id = c.goods_id " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('member_price') . " AS mp ON mp.goods_id = c.goods_id AND mp.user_rank = '$user_rank' ".
            " WHERE c.session_id = '" . SESS_ID . "' " .
            "AND c.rec_type = '$type'";
    $arr = $GLOBALS['db']->getAll($sql);

    /* 格式化价格及礼包商品 */
    foreach ($arr as $key => $value)
    {
		$arr[$key]['formated_market_price'] = price_format($value['market_price'], false);
		$arr[$key]['formated_goods_price'] = price_format($value['goods_price'], false);
		$arr[$key]['formated_subtotal'] = price_format($value['subtotal'], false);
        $arr[$key]['max_integral']      = integral_of_value($value['max_integral_amount']);
		if ($value['extension_code'] == 'package_buy')
		{
			$arr[$key]['package_goods_list'] = $packageController->get_package_goods($value['goods_id']);
		}
        
        if ($sa > 0){
            $arr[$key]['custom_price'] = $orderController->getGoodGaPrice($sa, $value['goods_id']);
            $arr[$key]['custom_subtotal'] = $arr[$key]['custom_price'] * $value['goods_number'];
            $arr[$key]['formated_custom_price'] = price_format($arr[$key]['custom_price'], false);
            $arr[$key]['formated_custom_subtotal'] = price_format($arr[$key]['custom_subtotal'], false);
        } else {
            //  If have custom price
            if(!empty($custom_prices))
            {
                if(isset( $custom_prices[$value['goods_id']] ) && $custom_prices[$value['goods_id']] !="" ) {
                    $arr[$key]['custom_price'] = $custom_prices[$value['goods_id']];
                    $arr[$key]['custom_subtotal'] = $custom_prices[$value['goods_id']] * $value['goods_number'];
                    $arr[$key]['formated_custom_price'] = price_format($custom_prices[$value['goods_id']], false);
                    $arr[$key]['formated_custom_subtotal'] = price_format($custom_prices[$value['goods_id']] * $value['goods_number'], false);
                }
            }
        }
	}

    return $arr;
}

/**
 * 获得购物车中的商品
 *
 * @access  public
 * @return  array
 */
function get_cart_goods_1($user_rank = 0, $sa=-1, $vip_package = true)
{
    $packageController = new Yoho\cms\Controller\PackageController();

    /* 初始化 */
    $goods_list = array();
    $total = array(
        'goods_price'  => 0, // 本店售价合计（有格式）
        'market_price' => 0, // 市场售价合计（有格式）
        'saving'       => 0, // 节省金额（有格式）
        'save_rate'    => 0, // 节省百分比
        'goods_amount' => 0, // 本店售价合计（无格式）
    );
    $auto_add_fav = [];
    /* 循环、统计 */
    if ($sa > 0){
        $sql = "SELECT c.*, IFNULL(sag.sa_price, 0) as goods_price, IF(c.parent_id, c.parent_id, c.goods_id) AS pid, '0' as is_vip, g.require_goods_id " .
                " FROM " . $GLOBALS['ecs']->table('cart1') . " as c " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('sales_agent_goods') . " AS sag ON sag.goods_id = c.goods_id AND sag.sa_id = '$sa' ".
                "LEFT JOIN (SELECT goods_id, require_goods_id FROM " . $GLOBALS['ecs']->table('goods') . ") AS g ON g.goods_id = c.goods_id ".
                " WHERE c.session_id = '" . SESS_ID . "' AND c.rec_type = '" . CART_GENERAL_GOODS . "'" .
                " ORDER BY pid, parent_id";
    } else {
        if($vip_package) {
            $price_sql = "LEAST(IFNULL(mp.user_price, c.goods_price),c.goods_price) as goods_price";
            $is_vip_sql = "IF(mp.user_price = LEAST(IFNULL(mp.user_price, c.goods_price),c.goods_price), 1, 0) as is_vip";
        } else {
            $price_sql = "IF(c.is_package = 0, LEAST(IFNULL(mp.user_price, c.goods_price),c.goods_price), c.goods_price) as goods_price";
            $is_vip_sql = "IF(c.is_package = 0, IF(mp.user_price = LEAST(IFNULL(mp.user_price, c.goods_price),c.goods_price), 1, 0), 0) as is_vip";
        }

        $sql = "SELECT c.*,".$price_sql.", IF(c.parent_id, c.parent_id, c.goods_id) AS pid, ".$is_vip_sql.", g.require_goods_id, g.goods_sn " .
                " FROM " . $GLOBALS['ecs']->table('cart1') . " as c " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('member_price') . " AS mp ON mp.goods_id = c.goods_id AND mp.user_rank = '$user_rank' ".
                "LEFT JOIN (SELECT goods_id, require_goods_id, goods_sn FROM " . $GLOBALS['ecs']->table('goods') . ") AS g ON g.goods_id = c.goods_id ".
                " WHERE c.session_id = '" . SESS_ID . "' AND c.rec_type = '" . CART_GENERAL_GOODS . "'" .
                " ORDER BY pid, parent_id";
    }
    $res = $GLOBALS['db']->query($sql);

    /* 用于统计购物车中实体商品和虚拟商品的个数 */
    $virtual_goods_count = 0;
    $real_goods_count    = 0;

    while ($row = $GLOBALS['db']->fetchRow($res))
    {
        $total['goods_price']  += $row['goods_price'] * $row['goods_number'];
        $total['market_price'] += $row['market_price'] * $row['goods_number'];

        $row['goods_price_normal'] = $row['goods_price'];
        $row['subtotal']     = price_format($row['goods_price'] * $row['goods_number'], false);
        $row['goods_price']  = price_format($row['goods_price'], false);
        $row['market_price'] = price_format($row['market_price'], false);

        /* 统计实体商品和虚拟商品的个数 */
        if ($row['is_real'])
        {
            $real_goods_count++;
        }
        else
        {
            $virtual_goods_count++;
        }

        /* 查询规格 */
        if (trim($row['goods_attr']) != '')
        {
            $sql = "SELECT attr_value FROM " . $GLOBALS['ecs']->table('goods_attr') . " WHERE goods_attr_id " .
            db_create_in($row['goods_attr']);
            $attr_list = $GLOBALS['db']->getCol($sql);
            foreach ($attr_list AS $attr)
            {
                $row['goods_name'] .= ' [' . $attr . '] ';
            }
        }
        /* 增加是否在购物车里显示商品图 */
        if (($GLOBALS['_CFG']['show_goods_in_cart'] == "2" || $GLOBALS['_CFG']['show_goods_in_cart'] == "3") && $row['extension_code'] != 'package_buy')
        {
            $goods_thumb = $GLOBALS['db']->getOne("SELECT `goods_thumb` FROM " . $GLOBALS['ecs']->table('goods') . " WHERE `goods_id`='{$row['goods_id']}'");
            $row['goods_thumb'] = get_image_path($row['goods_id'], $goods_thumb, true);
        }
        if ($row['extension_code'] == 'package_buy')
        {
            $row['package_goods_list'] = $packageController->get_package_goods($row['goods_id']);
        }
        // POS : check stock
        $goods = get_goods_info($row['goods_id']);
        if (intval($row['is_gift']) > 0 && $goods['goods_number'] > 0) {

            if (in_array(intval($row['is_gift']), $auto_add_fav)) {
                $row['is_auto_add'] = true;
            } else {
                $sql = "select act_type_ext, act_type, gift FROM ". $GLOBALS['ecs']->table('favourable_activity') ." WHERE act_id = ".$row['is_gift'];
                $favourable = $GLOBALS['db']->getRow($sql);
                $count_gift = count(unserialize($favourable['gift']));
                $total_gift_price = array_sum(array_column(unserialize($favourable['gift']), 'price'));
                if (
                    ( $favourable['act_type'] == FAT_GOODS && intval($favourable['act_type_ext']) > 0 && $count_gift > 0 && $count_gift == intval($favourable['act_type_ext']) && $total_gift_price == 0 ) ||
                    ( $favourable['act_type'] == FAT_GOODS && intval($favourable['act_type_ext']) == 0 ) 
                ){
                    $auto_add_fav[] = intval($row['is_gift']);
                    $row['is_auto_add'] = true;
                }
            }
        }
        $goods_list[] = $row;
    }
    $total['goods_amount'] = $total['goods_price'];
    $total['saving']       = price_format($total['market_price'] - $total['goods_price'], false);
    if ($total['market_price'] > 0)
    {
        $total['save_rate'] = $total['market_price'] ? round(($total['market_price'] - $total['goods_price']) *
        100 / $total['market_price']).'%' : 0;
    }
    $total['goods_price']  = price_format($total['goods_price'], false);
    $total['market_price'] = price_format($total['market_price'], false);
    $total['real_goods_count']    = $real_goods_count;
    $total['virtual_goods_count'] = $virtual_goods_count;

    return array('goods_list' => $goods_list, 'total' => $total);
}



/**  This function is unused. Please use Yoho\cms\Controller\OrderController->order_fee_flow($order, $goods, $consignee, $platform)
 * 获得订单中的费用信息
 *
 * @access  public
 * @param   array   $order
 * @param   array   $goods
 * @param   array   $consignee
 * @param   bool    $is_gb_deposit  是否团购保证金（如果是，应付款金额只计算商品总额和支付费用，可以获得的积分取 $gift_integral）
 * @return  array
 */
function order_fee1($order, $goods, $consignee, $discount_plus='1')
{
    /* Step 1 : Initialize extension_code, group_buy (Unused in YOHO). */
    if (!isset($order['extension_code']))
    {
        $order['extension_code'] = '';
    }

    if ($order['extension_code'] == 'group_buy')
    {
        $group_buy = group_buy_info($order['extension_id']);
    }

    /* Step 2 : Initialize order. */
    $total  = array('real_goods_count' => 0,
                    'gift_amount'      => 0,
                    'goods_price'      => 0,
                    'market_price'     => 0,
                    'discount'         => 0,
                    'pack_fee'         => 0,
                    'card_fee'         => 0,
                    'shipping_fee'     => 0,
                    'shipping_insure'  => 0,
                    'integral_money'   => 0,
                    'bonus'            => 0,
                    'coupon'           => 0,
                    'surplus'          => 0,
                    'cp_price'         => 0,
                    'cod_fee'          => 0,
                    'pay_fee'          => 0,
                    'tax'              => 0);
    $weight = 0;

    /* Step 3 : Calculate goods total price. */
    foreach ($goods AS $val)
    {
        // Count real goods
        if ($val['is_real'])
        {
            $total['real_goods_count']++;
        }
	   if(isset($val['custom_price'])){
            $total['goods_price']  += $val['custom_price'] * $val['goods_number'];
	   } else {
            $total['goods_price']  += $val['goods_price'] * $val['goods_number'];
	   }
        $total['market_price'] += $val['market_price'] * $val['goods_number'];

        // Calculate the item cost
        $sql = "SELECT cost FROM " . $GLOBALS['ecs']->table('goods') . " WHERE goods_id = '" . $val['goods_id'] . "'";
        $cp_price = $GLOBALS['db']->getOne($sql);
        $total['cp_price'] += round($cp_price * $val['goods_number'], 2);
    }

    $total['saving']    = $total['market_price'] - $total['goods_price'];
    $total['save_rate'] = $total['market_price'] ? round($total['saving'] * 100 / $total['market_price']) . '%' : 0;

    $total['goods_price_formated']  = price_format($total['goods_price'], false);
    $total['market_price_formated'] = price_format($total['market_price'], false);
    $total['saving_formated']       = price_format($total['saving'], false);
	
    /* Step 4 : Calculate order discount. */
    if ($order['extension_code'] != 'group_buy')
    {
        $discount = compute_discount_1();
        $total['discount'] = $discount['discount'];
        if ($total['discount'] > $total['goods_price'])
        {
            $total['discount'] = $total['goods_price'];
        }
    }
    
    /* Step 5 : Calculate YOHO POS discount. */
    if($discount_plus < 1 && $discount_plus > 0)
    {
        $total['goods_price_discounted'] = $total['goods_price'] * $discount_plus;
    }
    elseif($discount_plus > 1 || $discount_plus < 0)
    {
        $total['goods_price_discounted'] = $total['goods_price'] + $discount_plus;
    }
    else
    {
        $total['goods_price_discounted'] = $total['goods_price'];
    }
    $total['discount'] += ($total['goods_price'] - $total['goods_price_discounted']);
    
    $total['discount_formated'] = price_format($total['discount'], false);

    /* Step 6 : Calculate order tax (Unused in YOHO). */
    if (!empty($order['need_inv']) && $order['inv_type'] != '')
    {
        /* 查税率 */
        $rate = 0;
        foreach ($GLOBALS['_CFG']['invoice_type']['type'] as $key => $type)
        {
            if ($type == $order['inv_type'])
            {
                $rate = floatval($GLOBALS['_CFG']['invoice_type']['rate'][$key]) / 100;
                break;
            }
        }
        if ($rate > 0)
        {
            $total['tax'] = $rate * $total['goods_price'];
        }
    }
    $total['tax_formated'] = price_format($total['tax'], false);

    /* Step 7 : Calculate order pack fee(包裝) (Unused in YOHO). */
    if (!empty($order['pack_id']))
    {
        $total['pack_fee']      = pack_fee($order['pack_id'], $total['goods_price']);
    }
    $total['pack_fee_formated'] = price_format($total['pack_fee'], false);
   
   

    /* Step 8 : Calculate order card fee(賀卡) (Unused in YOHO). */
    if (!empty($order['card_id']))
    {
        $total['card_fee']      = card_fee($order['card_id'], $total['goods_price']);
    }
    $total['card_fee_formated'] = price_format($total['card_fee'], false);

    /* Step 9 : Calculate order bonus(紅包) (Unused in YOHO). */
    if (!empty($order['bonus_id']))
    {
        $bonus          = bonus_info($order['bonus_id']);
        $total['bonus'] = $bonus['type_money'];
    }
    $total['bonus_formated'] = price_format($total['bonus'], false);
    
    /* Step 10 : Calculate order bonus kill(線下紅包) (Unused in YOHO). */
     if (!empty($order['bonus_kill']))
    {
        $bonus          = bonus_info(0,$order['bonus_kill']);
        $total['bonus_kill'] = $order['bonus_kill'];
        $total['bonus_kill_formated'] = price_format($total['bonus_kill'], false);
    }
    
    /* Step 11: Calculate Coupon. */
    $total['coupon'] = 0;
    if (!empty($order['coupon_id']))
    {
        // initial coupon value
        $coupons_flat = array();
        $coupons_percent = array();
        
        // Loop order coupon id
        foreach ($order['coupon_id'] as $coupon_id)
        {
            // Get coupon info
            $coupon = coupon_info($coupon_id);
            if ($coupon)
            {
                // If coupon is deduct amount.
                if ($coupon['coupon_amount'] > 0)
                {
                    $coupons_flat[] = $coupon;
                }
                // If coupon is a percent coupon.
                elseif ($coupon['coupon_discount'] > 0)
                {
                    $coupons_percent[] = $coupon;
                }
                // If coupon is free shipping coupon.
                if ($coupon['free_shipping'])
                {
                    $coupons_free_shipping[] = $coupon;
                }
            }
        }
        // Process flat discount first, then percentage discount
        foreach ($coupons_flat as $coupon)
        {
            $total['coupon'] += $coupon['coupon_amount'];
        }
        foreach ($coupons_percent as $coupon)
        {
            if ($coupon['cartwide']) // percentage apply to all products in cart
            {
                $total['coupon'] += floor(($coupon['coupon_discount'] / 100) * $total['goods_price']);
            }
            else
            {
                $coupon_amount = 0;
                $coupon_goods = array_reduce($coupon['goods_list'], 'array_merge', array());
                foreach ($goods AS $g)
                {
                    if (in_array($g['goods_id'], $coupon_goods))
                    {
                        $coupon_amount += floor(($coupon['coupon_discount'] / 100) * $g['goods_price'] * $g['goods_number']);
                    }
                }
                $total['coupon'] += $coupon_amount;
            }
        }
    }
    $total['coupon_formated'] = price_format($total['coupon'], false);
    
    /* Step 12: Calculate goods can use the total amount of bonus paid. */
    $bonus_amount = compute_discount_amount_1();
    /* Step 13: Calculate the maximum amount of bonus and integral that can be paid is the total amount of goods. */
    $max_amount = $total['goods_price'] == 0 ? $total['goods_price'] : $total['goods_price'] - $bonus_amount;

    /* Step 14: Calculate the initial order amount. */
    // If it's a group buy order, order amount = total goods price.
    if ($order['extension_code'] == 'group_buy' && $group_buy['deposit'] > 0)
    {
        $total['amount'] = $total['goods_price'];
    }
    else // else, calculate the initial order amount.
    {
        // Calculate the initial order amount.(Without shipping in this step.) Edited:Anthony@YOHO 2017-06-09
        // $total['amount'] = $total['goods_price'] - $total['discount'] + $total['tax'] + $total['pack_fee'] + $total['card_fee'] +
        // $total['shipping_fee'] + $total['shipping_insure'] + $total['cod_fee'];
        //** YOHO: $total['amount'] = goods_price - discount
        $total['amount']  = $total['goods_price'] - $total['discount'] + $total['tax'] + $total['pack_fee'] + $total['card_fee'];

        // Deduct bonus amount
        $use_bonus        = min($total['bonus'], $max_amount);
        if(isset($total['bonus_kill']))
        {
            $use_bonus_kill   = min($total['bonus_kill'], $max_amount);
            $total['amount'] -=  $price = number_format($total['bonus_kill'], 2, '.', '');
        }
        
        // Deduct bonus kill amount
        $total['bonus']   = $use_bonus;
        $total['bonus_formated'] = price_format($total['bonus'], false);
        $total['amount'] -= $use_bonus;
        $max_amount      -= $use_bonus;
        
        // Deduct coupon amount
        $use_coupon       = min($total['coupon'], $max_amount); // Actual coupon amount used
        $total['coupon']  = $use_coupon;
        $total['coupon_formated'] = price_format($total['coupon'], false);
        $total['amount'] -= $use_coupon;
        $max_amount      -= $use_coupon;
    }

    /* Step 15: Calculate surplus(餘額) (Unused in YOHO). */
    $order['surplus'] = $order['surplus'] > 0 ? $order['surplus'] : 0;
    if ($total['amount'] > 0)
    {
        if (isset($order['surplus']) && $order['surplus'] > $total['amount'])
        {
            $order['surplus'] = $total['amount'];
            $total['amount']  = 0;
        }
        else
        {
            $total['amount'] -= floatval($order['surplus']);
        }
    }
    else
    {
        $order['surplus'] = 0;
        $total['amount']  = 0;
    }
    $total['surplus'] = $order['surplus'];
    $total['surplus_formated'] = price_format($order['surplus'], false);

    /* Step 16: Calculate integral(積分/user pay_point). */
    $order['integral'] = $order['integral'] > 0 ? $order['integral'] : 0;
    //** $total['amount'] = goods_price - discount - tax + pack_fee + card_fee - bonus_kill(if have) - bonus - coupon - surplus
    //** YOHO: $total['amount'] = goods_price - discount - coupon
    if ($total['amount'] > 0 && $max_amount > 0 && $order['integral'] > 0)
    {
        $integral_money = value_of_integral($order['integral']);
        // Use integral pay
        $use_integral            = min($total['amount'], $max_amount, $integral_money);
        $total['amount']        -= $use_integral;
        $total['integral_money'] = $use_integral;
        $order['integral']       = integral_of_value($use_integral);
    }
    else
    {
        $total['integral_money'] = 0;
        $order['integral']       = 0;
    }
    $total['integral'] = $order['integral'];
    $total['integral_formated'] = price_format($total['integral_money'], false);

    /* Step 17: Save order flow into session. */
    $_SESSION['flow_order'] = $order;
    $se_flow_type = isset($_SESSION['flow_type']) ? $_SESSION['flow_type'] : '';
    
    /* Step 18: Calculate pay fee(支付折扣). */
    // If have order pay_id, have real goods, calculate pay fee.
    if (!empty($order['pay_id']) && ($total['real_goods_count'] > 0 || $se_flow_type != CART_EXCHANGE_GOODS))
    {
        //** $total['amount'] = goods_price - discount - tax + pack_fee + card_fee - bonus_kill(if have) - bonus - coupon - surplus - integral_money
        //** YOHO: $total['amount'] = goods_price - discount - coupon - integral_money
        $total['pay_fee']      = pay_fee($order['pay_id'], $total['amount'], $shipping_cod_fee);
    }

    $total['amount']           += $total['pay_fee'];
    $total['pay_fee_formated'] = price_format($total['pay_fee'], false);
    $total['pay_discount_formated'] = price_format(-$total['pay_fee'], false);
    
    /* Step 19: Calculate shipping fee(運費). */
    $shipping_cod_fee = NULL;

    // If order have shipping id, have real goods, and order type is not sale agent, calculate shipping fee.
    if ($order['shipping_id'] > 0 && $total['real_goods_count'] > 0 && !in_array($order['order_type'], [Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT]))
    {
        $region['country']  = $consignee['country'];
        $region['province'] = $consignee['province'];
        $region['city']     = $consignee['city'];
        $region['district'] = $consignee['district'];
        $shipping_info = shipping_area_info($order['shipping_id'], $region);

        if (!empty($shipping_info))
        {
            // Process free shipping coupons
            global $coupon_free_shipping_goods;
            $coupon_free_shipping_goods = array();
            if (!empty($order['coupon_id']))
            {
                foreach ($coupons_free_shipping as $coupon)
                {
                    if ($coupon['cartwide']) // percentage apply to all products in cart
                    {
                        foreach ($goods AS $g)
                        {
                            $coupon_free_shipping_goods[] = $g['goods_id'];
                        }
                    }
                    else
                    {
                        $coupon_goods = array_reduce($coupon['goods_list'], 'array_merge', array());
                        foreach ($goods AS $g)
                        {
                            if (in_array($g['goods_id'], $coupon_goods))
                            {
                                $coupon_free_shipping_goods[] = $g['goods_id'];
                            }
                        }
                    }
                }
                $coupon_free_shipping_goods = array_unique($coupon_free_shipping_goods);
            }
            
            if ($order['extension_code'] == 'group_buy')
            {
                $weight_price = cart_weight_price_1(CART_GROUP_BUY_GOODS);
            }
            else
            {
                $weight_price = cart_weight_price_1();
            }

            // 查看购物车中是否全为免运费商品，若是则把运费赋为零
            $sql = 'SELECT count(*) FROM ' . $GLOBALS['ecs']->table('cart1') . " WHERE  `session_id` = '" . SESS_ID. "' AND `extension_code` != 'package_buy' AND `is_shipping` = 0";
            $shipping_count = $GLOBALS['db']->getOne($sql);

            if ($shipping_count == 0 && $weight_price['free_shipping'] == 1)
            {
                $total['shipping_fee'] = 0;
            }
            else
            {
                $total['shipping_fee'] = shipping_fee($shipping_info['shipping_code'], $shipping_info['configure'], $weight_price['weight'], $weight_price['amount'], $weight_price['number'], $weight_price['price']);
            }

            if (!empty($order['need_insure']) && $shipping_info['insure'] > 0)
            {
                $total['shipping_insure'] = shipping_insure_fee($shipping_info['shipping_code'],
                    $total['goods_price'], $shipping_info['insure']);
            }
            else
            {
                $total['shipping_insure'] = 0;
            }

            if ($shipping_info['support_cod'])
            {
                $shipping_cod_fee = $shipping_info['pay_fee'];
                $total['cod_fee'] = $shipping_cod_fee;
            }
            
            // If user wants to pay fee on delivery, set shipping_fee to 0
            // but if the user selected some product with its own shipping charge, we have to include them back
            if ($order['shipping_fod'])
            {
                $total['shipping_fee'] = 0 + $weight_price['price'];
            }
        }
    }

    $total['shipping_fee_formated']    = price_format($total['shipping_fee'], false);
    $total['shipping_insure_formated'] = price_format($total['shipping_insure'], false);
    // Order amount calculate by shipping.
    //** $total['amount'] = goods_price - discount - tax + pack_fee + card_fee - bonus_kill(if have) - bonus - coupon - surplus - integral_money + pay_fee
    //** YOHO: $total['amount'] = goods_price - discount - coupon - integral_money + pay_fee
    $total['amount'] += $total['shipping_fee'] + $total['shipping_insure'] + $total['cod_fee'];
    // Formated amount.
    $total['amount_formated']  = price_format($total['amount'], false);
    $total['weight']           = $weight_price['weight'];
    
    /* Step 20: Calculate user points and bouns (bouns is unused in YOHO). */
    if ($order['extension_code'] == 'group_buy')
    {
        $total['will_get_integral'] = $group_buy['gift_integral'];
    }
    elseif ($order['extension_code'] == 'exchange_goods')
    {
        $total['will_get_integral'] = 0;
    }
    else
    {
        $total['will_get_integral'] = get_give_integral_1($goods);
    }
    $total['will_get_bonus']        = $order['extension_code'] == 'exchange_goods' ? 0 : price_format(get_total_bonus_1(), false);
    $total['formated_goods_price']  = price_format($total['goods_price'], false);
    $total['formated_market_price'] = price_format($total['market_price'], false);
    $total['formated_saving']       = price_format($total['saving'], false);

    if ($order['extension_code'] == 'exchange_goods')
    {
        $sql = 'SELECT SUM(eg.exchange_integral) '.
               'FROM ' . $GLOBALS['ecs']->table('cart1') . ' AS c,' . $GLOBALS['ecs']->table('exchange_goods') . 'AS eg '.
               "WHERE c.goods_id = eg.goods_id AND c.session_id= '" . SESS_ID . "' " .
               "  AND c.rec_type = '" . CART_EXCHANGE_GOODS . "' " .
               '  AND c.is_gift = 0 AND c.goods_id > 0 ' .
               'GROUP BY eg.goods_id';
        $exchange_integral = $GLOBALS['db']->getOne($sql);
        $total['exchange_integral'] = $exchange_integral;
    }

    /* Step 21: Return order fee. */
    return $total;
}


/**
 * 计算折扣：根据购物车和优惠活动
 * @return  float   折扣
 */
function compute_discount_1()
{
    /* 查询优惠活动 */
    $now = gmtime();
    $user_rank = ',' . $_SESSION['user_rank'] . ',';
    $sql = "SELECT *" .
            "FROM " . $GLOBALS['ecs']->table('favourable_activity') .
            " WHERE start_time <= '$now'" .
            " AND end_time >= '$now'" .
            " AND status = 1" .
            " AND CONCAT(',', user_rank, ',') LIKE '%" . $user_rank . "%'" .
            " AND act_type " . db_create_in(array(FAT_DISCOUNT, FAT_PRICE));
    $favourable_list = $GLOBALS['db']->getAll($sql);
    if (!$favourable_list)
    {
        return 0;
    }

    /* 查询购物车商品 */
    $sql = "SELECT c.goods_id, c.goods_price * c.goods_number AS subtotal, g.cat_id, g.brand_id " .
            "FROM " . $GLOBALS['ecs']->table('cart1') . " AS c, " . $GLOBALS['ecs']->table('goods') . " AS g " .
            "WHERE c.goods_id = g.goods_id " .
            "AND c.session_id = '" . SESS_ID . "' " .
            "AND c.parent_id = 0 " .
            "AND c.is_gift = 0 " .
            "AND rec_type = '" . CART_GENERAL_GOODS . "'";
    $goods_list = $GLOBALS['db']->getAll($sql);
    if (!$goods_list)
    {
        return 0;
    }

    /* 初始化折扣 */
    $discount = 0;
    $favourable_name = array();

    /* 循环计算每个优惠活动的折扣 */
    foreach ($favourable_list as $favourable)
    {
        $total_amount = 0;
        if ($favourable['act_range'] == FAR_ALL)
        {
            foreach ($goods_list as $goods)
            {
                $total_amount += $goods['subtotal'];
            }
        }
        elseif ($favourable['act_range'] == FAR_CATEGORY)
        {
            /* 找出分类id的子分类id */
            $id_list = array();
            $raw_id_list = explode(',', $favourable['act_range_ext']);
            foreach ($raw_id_list as $id)
            {
                $id_list = array_merge($id_list, array_keys(cat_list($id, 0, false)));
            }
            $ids = join(',', array_unique($id_list));

            foreach ($goods_list as $goods)
            {
                if (strpos(',' . $ids . ',', ',' . $goods['cat_id'] . ',') !== false)
                {
                    $total_amount += $goods['subtotal'];
                }
            }
        }
        elseif ($favourable['act_range'] == FAR_BRAND)
        {
            foreach ($goods_list as $goods)
            {
                if (strpos(',' . $favourable['act_range_ext'] . ',', ',' . $goods['brand_id'] . ',') !== false)
                {
                    $total_amount += $goods['subtotal'];
                }
            }
        }
        elseif ($favourable['act_range'] == FAR_GOODS)
        {
            foreach ($goods_list as $goods)
            {
                if (strpos(',' . $favourable['act_range_ext'] . ',', ',' . $goods['goods_id'] . ',') !== false)
                {
                    $total_amount += $goods['subtotal'];
                }
            }
        }
        else
        {
            continue;
        }

        /* 如果金额满足条件，累计折扣 */
        if ($total_amount > 0 && $total_amount >= $favourable['min_amount'] && ($total_amount <= $favourable['max_amount'] || $favourable['max_amount'] == 0))
        {
            if ($favourable['act_type'] == FAT_DISCOUNT)
            {
                $discount += $total_amount * (1 - $favourable['act_type_ext'] / 100);

                $favourable_name[] = $favourable['act_name'];
            }
            elseif ($favourable['act_type'] == FAT_PRICE)
            {
                $discount += $favourable['act_type_ext'];

                $favourable_name[] = $favourable['act_name'];
            }
        }
    }

    return array('discount' => $discount, 'name' => $favourable_name);
}

/**
 * 计算购物车中的商品能享受红包支付的总额
 * @return  float   享受红包支付的总额
 */
function compute_discount_amount_1()
{
    /* 查询优惠活动 */
    $now = gmtime();
    $user_rank = ',' . $_SESSION['user_rank'] . ',';
    $sql = "SELECT *" .
            "FROM " . $GLOBALS['ecs']->table('favourable_activity') .
            " WHERE start_time <= '$now'" .
            " AND end_time >= '$now'" .
            " AND status = 1" .
            " AND CONCAT(',', user_rank, ',') LIKE '%" . $user_rank . "%'" .
            " AND act_type " . db_create_in(array(FAT_DISCOUNT, FAT_PRICE));
    $favourable_list = $GLOBALS['db']->getAll($sql);
    if (!$favourable_list)
    {
        return 0;
    }

    /* 查询购物车商品 */
    $sql = "SELECT c.goods_id, c.goods_price * c.goods_number AS subtotal, g.cat_id, g.brand_id " .
            "FROM " . $GLOBALS['ecs']->table('cart1') . " AS c, " . $GLOBALS['ecs']->table('goods') . " AS g " .
            "WHERE c.goods_id = g.goods_id " .
            "AND c.session_id = '" . SESS_ID . "' " .
            "AND c.parent_id = 0 " .
            "AND c.is_gift = 0 " .
            "AND rec_type = '" . CART_GENERAL_GOODS . "'";
    $goods_list = $GLOBALS['db']->getAll($sql);
    if (!$goods_list)
    {
        return 0;
    }

    /* 初始化折扣 */
    $discount = 0;
    $favourable_name = array();

    /* 循环计算每个优惠活动的折扣 */
    foreach ($favourable_list as $favourable)
    {
        $total_amount = 0;
        if ($favourable['act_range'] == FAR_ALL)
        {
            foreach ($goods_list as $goods)
            {
                $total_amount += $goods['subtotal'];
            }
        }
        elseif ($favourable['act_range'] == FAR_CATEGORY)
        {
            /* 找出分类id的子分类id */
            $id_list = array();
            $raw_id_list = explode(',', $favourable['act_range_ext']);
            foreach ($raw_id_list as $id)
            {
                $id_list = array_merge($id_list, array_keys(cat_list($id, 0, false)));
            }
            $ids = join(',', array_unique($id_list));

            foreach ($goods_list as $goods)
            {
                if (strpos(',' . $ids . ',', ',' . $goods['cat_id'] . ',') !== false)
                {
                    $total_amount += $goods['subtotal'];
                }
            }
        }
        elseif ($favourable['act_range'] == FAR_BRAND)
        {
            foreach ($goods_list as $goods)
            {
                if (strpos(',' . $favourable['act_range_ext'] . ',', ',' . $goods['brand_id'] . ',') !== false)
                {
                    $total_amount += $goods['subtotal'];
                }
            }
        }
        elseif ($favourable['act_range'] == FAR_GOODS)
        {
            foreach ($goods_list as $goods)
            {
                if (strpos(',' . $favourable['act_range_ext'] . ',', ',' . $goods['goods_id'] . ',') !== false)
                {
                    $total_amount += $goods['subtotal'];
                }
            }
        }
        else
        {
            continue;
        }
        if ($total_amount > 0 && $total_amount >= $favourable['min_amount'] && ($total_amount <= $favourable['max_amount'] || $favourable['max_amount'] == 0))
        {
            if ($favourable['act_type'] == FAT_DISCOUNT)
            {
                $discount += $total_amount * (1 - $favourable['act_type_ext'] / 100);
            }
            elseif ($favourable['act_type'] == FAT_PRICE)
            {
                $discount += $favourable['act_type_ext'];
            }
        }
    }


    return $discount;
}

/**
 * 获得购物车中商品的总重量、总价格、总数量
 *
 * @access  public
 * @param   int     $type   类型：默认普通商品
 * @return  array
 */
function cart_weight_price_1($type = CART_GENERAL_GOODS, $cal_free_shipping_goods = false)
{
    // Handle goods marked as free_shipping by coupon
    global $coupon_free_shipping_goods;
    if (!$coupon_free_shipping_goods)
    {
        $coupon_free_shipping_goods = array();
    }
    
    $package_row['weight'] = 0;
    $package_row['amount'] = 0;
    $package_row['number'] = 0;
    $package_row['price'] = 0;

    $packages_row['free_shipping'] = 1;

    /* 计算超值礼包内商品的相关配送参数 */
    $sql = 'SELECT goods_id, goods_number, goods_price FROM ' . $GLOBALS['ecs']->table('cart1') . " WHERE extension_code = 'package_buy' AND session_id = '" . SESS_ID . "'";
    $row = $GLOBALS['db']->getAll($sql);

    if ($row)
    {
        $packages_row['free_shipping'] = 0;
        $free_shipping_count = 0;

        foreach ($row as $val)
        {
            // 如果商品全为免运费商品，设置一个标识变量
            $sql = 'SELECT count(*) FROM ' .
                    $GLOBALS['ecs']->table('package_goods') . ' AS pg, ' .
                    $GLOBALS['ecs']->table('goods') . ' AS g ' .
                    "WHERE g.goods_id = pg.goods_id AND g.is_shipping = 0 AND pg.package_id = '"  . $val['goods_id'] . "' " .
                    "AND g.goods_id NOT " . db_create_in($coupon_free_shipping_goods);
            $shipping_count = $GLOBALS['db']->getOne($sql);

            if ($shipping_count > 0)
            {
                // 循环计算每个超值礼包商品的重量和数量，注意一个礼包中可能包换若干个同一商品
                // Only include those without shipping price
                $sql = "SELECT sum(g.`goods_weight` * pg.`goods_number`) as weight, " .
                            "sum(pg.`goods_number`) as number " .
                        "FROM " . $GLOBALS['ecs']->table('package_goods') . " as pg " .
                        "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON g.goods_id = pg.goods_id " .
                        "WHERE ".($cal_free_shipping_goods ? "g.is_shipping = 0" : " 1 " ) ." AND g.shipping_price = 0 AND pg.package_id = '"  . $val['goods_id'] . "' " .
                        "AND g.goods_id NOT " . db_create_in($coupon_free_shipping_goods);
                $goods_row = $GLOBALS['db']->getRow($sql);
                $package_row['weight'] += floatval($goods_row['weight']) * $val['goods_number'];
                $package_row['amount'] += floatval($val['goods_price']) * $val['goods_number'];
                $package_row['number'] += intval($goods_row['number']) * $val['goods_number'];
                
                // Add shipping price
                $sql = "SELECT sum(g.`shipping_price` * pg.`goods_number`) as price " .
                        "FROM " . $GLOBALS['ecs']->table('package_goods') . " as pg " .
                        "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON g.goods_id = pg.goods_id " .
                        "WHERE ".($cal_free_shipping_goods ? "g.is_shipping = 0" : " 1 " ) ." AND g.shipping_price > 0 AND pg.package_id = '"  . $val['goods_id'] . "' " .
                        "AND g.goods_id NOT " . db_create_in($coupon_free_shipping_goods);
                $shipping_price = $GLOBALS['db']->getOne($sql);
                $package_row['price'] += floatval($shipping_price) * $val['goods_number'];
            }
            else
            {
                $free_shipping_count++;
            }
        }

        $packages_row['free_shipping'] = $free_shipping_count == count($row) ? 1 : 0;
        $packages_row['free_shipping'] = ($cal_free_shipping_goods) ? $packages_row['free_shipping'] : 0;
    }

    /* 获得购物车中非超值礼包商品的总重量 */
    // Only include those without shipping price
    $sql = "SELECT   sum(g.`goods_weight` * c.`goods_number`) as weight, " .
                    "sum(c.`goods_price` * c.`goods_number`) as amount, " .
                    "sum(c.`goods_number`) as number " .
            "FROM " . $GLOBALS['ecs']->table('cart1') . " as c " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON g.goods_id = c.goods_id " .
            "WHERE c.session_id = '" . SESS_ID . "' " .
            "AND rec_type = '$type' AND ".($cal_free_shipping_goods ? "g.is_shipping = 0" : " 1 " ) ." AND g.shipping_price = 0 AND c.extension_code != 'package_buy' " .
            "AND g.goods_id NOT " . db_create_in($coupon_free_shipping_goods);
    $row = $GLOBALS['db']->getRow($sql);
    $packages_row['weight'] += floatval($row['weight']);
    $packages_row['amount'] += floatval($row['amount']);
    $packages_row['number'] += intval($row['number']);
    
    // Add shipping price
    $sql = "SELECT sum(g.`shipping_price` * c.`goods_number`) as price " .
            "FROM " . $GLOBALS['ecs']->table('cart1') . " as c " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON g.goods_id = c.goods_id " .
            "WHERE c.session_id = '" . SESS_ID . "' " .
            "AND rec_type = '$type' AND ".($cal_free_shipping_goods ? "g.is_shipping = 0" : " 1 " ) ." AND g.shipping_price > 0 AND c.extension_code != 'package_buy' " .
            "AND g.goods_id NOT " . db_create_in($coupon_free_shipping_goods);
    $shipping_price = $GLOBALS['db']->getOne($sql);
    $packages_row['price'] += floatval($shipping_price);
    // If we cal free shipping goods, include price( If not cal free shipping goods, price is also in amount )
    if ($cal_free_shipping_goods) {
        // Update 2016-08-28: Also include the price for free shipping products into calculation
        $sql = "SELECT   sum(c.`goods_price` * c.`goods_number`) as amount " .
                "FROM " . $GLOBALS['ecs']->table('cart1') . " as c " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON g.goods_id = c.goods_id " .
                "WHERE c.session_id = '" . SESS_ID . "' " .
                "AND rec_type = '$type' AND g.shipping_price = 0 AND c.extension_code != 'package_buy' " .
                "AND (g.is_shipping = 1 OR g.goods_id " . db_create_in($coupon_free_shipping_goods) . ") ";
        $row = $GLOBALS['db']->getRow($sql);
        $packages_row['amount'] += floatval($row['amount']);
    }
    
    /* 格式化重量 */
    $packages_row['formated_weight'] = formated_weight($packages_row['weight']);

    return $packages_row;
}

/**
 * 获得用户的可用积分
 *
 * @access  private
 * @return  integral
 */
function flow_available_points_1()
{
    $sql = "SELECT SUM(g.integral * c.goods_number) ".
            "FROM " . $GLOBALS['ecs']->table('cart1') . " AS c, " . $GLOBALS['ecs']->table('goods') . " AS g " .
            "WHERE c.session_id = '" . SESS_ID . "' AND c.goods_id = g.goods_id AND c.is_gift = 0 AND g.integral > 0 " .
            "AND c.rec_type = '" . CART_GENERAL_GOODS . "'";

    $val = intval($GLOBALS['db']->getOne($sql));

    return integral_of_value($val);
}

/**
 * 取得购物车该赠送的积分数
 * @return  int     积分数
 */
function get_give_integral_1()
{
        $sql = "SELECT SUM(c.goods_number * IF(g.give_integral > -1, g.give_integral, c.goods_price))" .
                "FROM " . $GLOBALS['ecs']->table('cart1') . " AS c, " .
                          $GLOBALS['ecs']->table('goods') . " AS g " .
                "WHERE c.goods_id = g.goods_id " .
                "AND c.session_id = '" . SESS_ID . "' " .
                "AND c.goods_id > 0 " .
                "AND c.parent_id = 0 " .
                "AND c.rec_type = 0 " .
                "AND c.is_gift = 0";

        return intval($GLOBALS['db']->getOne($sql));
}

/**
 * 取得购物车总金额
 * @params  boolean $include_gift   是否包括赠品
 * @param   int     $type           类型：默认普通商品
 * @return  float   购物车总金额
 */
function cart_amount_1($include_gift = true, $type = CART_GENERAL_GOODS)
{
    $sql = "SELECT SUM(goods_price * goods_number) " .
            " FROM " . $GLOBALS['ecs']->table('cart1') .
            " WHERE session_id = '" . SESS_ID . "' " .
            "AND rec_type = '$type' ";

    if (!$include_gift)
    {
        $sql .= ' AND is_gift = 0 AND goods_id > 0';
    }

    return floatval($GLOBALS['db']->getOne($sql));
}

/**
 * 取得当前用户应该得到的红包总额
 */
function get_total_bonus_1()
{
    $day    = getdate();
    $today  = local_mktime(23, 59, 59, $day['mon'], $day['mday'], $day['year']);

    /* 按商品发的红包 */
    $sql = "SELECT SUM(c.goods_number * t.type_money)" .
            "FROM " . $GLOBALS['ecs']->table('cart1') . " AS c, "
                    . $GLOBALS['ecs']->table('bonus_type') . " AS t, "
                    . $GLOBALS['ecs']->table('goods') . " AS g " .
            "WHERE c.session_id = '" . SESS_ID . "' " .
            "AND c.is_gift = 0 " .
            "AND c.goods_id = g.goods_id " .
            "AND g.bonus_type_id = t.type_id " .
            "AND t.send_type = '" . SEND_BY_GOODS . "' " .
            "AND t.send_start_date <= '$today' " .
            "AND t.send_end_date >= '$today' " .
            "AND c.rec_type = '" . CART_GENERAL_GOODS . "'";
    $goods_total = floatval($GLOBALS['db']->getOne($sql));

    /* 取得购物车中非赠品总金额 */
    $sql = "SELECT SUM(goods_price * goods_number) " .
            "FROM " . $GLOBALS['ecs']->table('cart1') .
            " WHERE session_id = '" . SESS_ID . "' " .
            " AND is_gift = 0 " .
            " AND rec_type = '" . CART_GENERAL_GOODS . "'";
    $amount = floatval($GLOBALS['db']->getOne($sql));

    /* 按订单发的红包 */
    $sql = "SELECT FLOOR('$amount' / min_amount) * type_money " .
            "FROM " . $GLOBALS['ecs']->table('bonus_type') .
            " WHERE send_type = '" . SEND_BY_ORDER . "' " .
            " AND send_start_date <= '$today' " .
            "AND send_end_date >= '$today' " .
            "AND min_amount > 0 ";
    $order_total = floatval($GLOBALS['db']->getOne($sql));

    return $goods_total + $order_total;
}
/**
 * 检查订单中商品库存
 *
 * @access  public
 * @param   array   $arr
 *
 * @return  void
 */
function flow_cart_stock_1($arr)
{
    foreach ($arr AS $key => $val)
    {
        $val = intval(make_semiangle($val));
        if ($val <= 0)
        {
            continue;
        }

        $sql = "SELECT `goods_id`, `goods_attr_id`, `extension_code` FROM" .$GLOBALS['ecs']->table('cart1').
               " WHERE rec_id='$key' AND session_id='" . SESS_ID . "'";
        $goods = $GLOBALS['db']->getRow($sql);

        $sql = "SELECT g.goods_name, g.goods_number, c.product_id ".
                "FROM " .$GLOBALS['ecs']->table('goods'). " AS g, ".
                    $GLOBALS['ecs']->table('cart1'). " AS c ".
                "WHERE g.goods_id = c.goods_id AND c.rec_id = '$key'";
        $row = $GLOBALS['db']->getRow($sql);

        //系统启用了库存，检查输入的商品数量是否有效
        if (intval($GLOBALS['_CFG']['use_storage']) > 0 && $goods['extension_code'] != 'package_buy')
        {
            if ($row['goods_number'] < $val)
            {
                show_message(sprintf($GLOBALS['_LANG']['stock_insufficiency'], $row['goods_name'],
                $row['goods_number'], $row['goods_number']));
                exit;
            }

            /* 是货品 */
            $row['product_id'] = trim($row['product_id']);
            if (!empty($row['product_id']))
            {
                $sql = "SELECT product_number FROM " .$GLOBALS['ecs']->table('products'). " WHERE goods_id = '" . $goods['goods_id'] . "' AND product_id = '" . $row['product_id'] . "'";
                $product_number = $GLOBALS['db']->getOne($sql);
                if ($product_number < $val)
                {
                    show_message(sprintf($GLOBALS['_LANG']['stock_insufficiency'], $row['goods_name'],
                    $row['goods_number'], $row['goods_number']));
                    exit;
                }
            }
        }
        elseif (intval($GLOBALS['_CFG']['use_storage']) > 0 && $goods['extension_code'] == 'package_buy')
        {
            if (judge_package_stock($goods['goods_id'], $val))
            {
                show_message($GLOBALS['_LANG']['package_stock_insufficiency']);
                exit;
            }
        }
    }

}

/**
 * 清空购物车
 * @param   int     $type   类型：默认普通商品
 */
function clear_cart_1($type = CART_GENERAL_GOODS)
{
    $sql = "DELETE FROM " . $GLOBALS['ecs']->table('cart1') .
            " WHERE session_id = '" . SESS_ID . "' AND rec_type = '$type'";
    $GLOBALS['db']->query($sql);
}
?>
