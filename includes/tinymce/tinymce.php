<?php


define('IN_ECS', true);

require(dirname(dirname(dirname(__FILE__))) . '/includes/init.php');
require_once(ROOT_PATH . 'includes/cls_image.php');

/* Tinymce file upload */
if($_REQUEST['act']=='tinymce_upload'){

	$return = array();

	if (!make_dir(ROOT_PATH . "/images/upload/Image/"))
	{
		/* 创建目录失败 */
		return false;
	}

	//Upload file
	$file = $_FILES['file'];
	$filename = cls_image::random_filename() . substr($file['name'], strpos($file['name'], '.'));
	$path     = ROOT_PATH . "/images/upload/Image/" . $filename;

	if (move_upload_file($file['tmp_name'], $path))
	{
		$img_path = $_SERVER['REQUEST_SCHEME']."://".$_SERVER['SERVER_NAME']."/images/upload/Image/" . $filename;
		$return=['status'=>'success','location'=>$img_path];
	}
	else
	{
		$return=['status'=>'error','msg'=>'cannot upload file'];
	}

	echo json_encode($return);

}