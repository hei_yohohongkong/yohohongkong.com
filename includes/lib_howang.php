<?php

/***
 * lib_howang.php
 * by howang 2014-07-11
 *
 * Useful ecshop functions with improvements by howang
 ***/

if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

function hw_is_empty_html($html)
{
	return ((trim(str_replace('&nbsp;',' ',strip_tags($html))) == '') // Is empty after stripping all HTML and "&nbsp;"
		&& (stripos($html, '<img') === false)); // And do not contains HTML image
}

function hw_goods_in_stock_sql($prefix = '', $stock_column = 'goods_number')
{
	// The in_stock condition is, 2 = the goods is really in stock, 1 = available for 訂貨
	return "IF({$stock_column} - reserved_number > 0, 2, " .
		   "IF({$prefix}is_pre_sale = 0 AND ({$prefix}pre_sale_days > 0 OR {$prefix}pre_sale_date > " . gmtime() . "), 1, 0)" .
		   ") as in_stock";
}

function hw_goods_number_sql($prefix = '', $stock_column = 'goods_number')
{
	// If 預售: goods_number = 0
	// If 有貨 + 可訂貨: goods_number = GREATEST(goods_number - reserved_number, 20)
	// If 有貨 + 不可訂貨: goods_number = goods_number - reserved_number
	// If 訂貨: goods_number = 20
	// Else 缺貨: goods_number = 0
	return "IF({$prefix}is_pre_sale = 1, 0, " .
		   "IF({$stock_column} - reserved_number > 0 AND ({$prefix}pre_sale_days > 0 OR {$prefix}pre_sale_date > " . gmtime() . "), GREATEST({$stock_column} - reserved_number, 20), " .
		   "IF({$stock_column} - reserved_number > 0, {$stock_column} - reserved_number, " .
		   "IF({$prefix}pre_sale_days > 0 OR {$prefix}pre_sale_date > " . gmtime() . ", 20, " .
		   "0)))) as goods_number";
}

function hw_goods_can_overorder_sql($prefix = '')
{
	// can_overorder = can buy more than stock
	// If 預售: can't buy, of coz cannot overorder
	// If 訂貨: can overorder
	// Else: cannot overorder
	return "IF({$prefix}is_pre_sale = 1, 0, " .
		   "IF({$prefix}pre_sale_days > 0 OR {$prefix}pre_sale_date > " . gmtime() . ", 1, 0)" .
		   ") as can_overorder";
}

function hw_goods_number_subquery($prefix = '', $column_name = 'goods_number', $ega_alias = 'ega', $house = false)
{
	if ($ega_alias == ''){$ega_alias = 'ega';}
	$erpController = new Yoho\cms\Controller\ErpController();
	$sql = "(SELECT SUM({$ega_alias}.goods_qty) FROM " . $GLOBALS['ecs']->table('erp_goods_attr_stock') . " AS {$ega_alias} WHERE {$prefix}goods_id = {$ega_alias}.goods_id ";
			if ($house != false && $house !="" && (int)$house > 0){
				if ($house == 1){
					$sql .= "AND ({$ega_alias}.warehouse_id = 1 OR {$ega_alias}.warehouse_id IS NULL)) ";
				} else {
					$sql .= "AND {$ega_alias}.warehouse_id = {$house}) ";
				}
			} else {
				$erpController = new Yoho\cms\Controller\ErpController();
				$sql .= "AND {$ega_alias}.warehouse_id IN (".$erpController->getSellableWarehouseIds(true).")) ";
			}
			$sql .= "as {$column_name}";
	return $sql;
}

function hw_reserved_number_subquery($prefix = '', $column_name = 'reserved_number', $oi_alias = 'oi', $og_alias = 'og')
{
	return "IFNULL((" .
				"SELECT SUM(IF({$og_alias}.send_number > {$og_alias}.goods_number, 0, {$og_alias}.goods_number - {$og_alias}.send_number)) " .
				"FROM " . $GLOBALS['ecs']->table('order_goods') . " as {$og_alias} " .
				"LEFT JOIN " . $GLOBALS['ecs']->table('order_info') . " as {$oi_alias} ON {$og_alias}.order_id = {$oi_alias}.order_id " .
				"WHERE {$prefix}goods_id = {$og_alias}.goods_id " . order_query_sql('await_ship', $oi_alias . '.') .
			"),0) as {$column_name}";
}

function hw_goods_list_sql($where, $sort = 'goods_id', $order = 'DESC', $num = 0, $start = 0, $need_real_stock = true)
{
	require_once ROOT_PATH . 'includes/lib_order.php';
	global $_CFG, $is_testing;
	$erpController = new \Yoho\cms\Controller\ErpController();
	// Parameter 1 can be an associated array
	$in_stock_first = true;
	$include_hidden = false;

	if ($is_testing) {
		$include_hidden = true;
	}

	$extra_select = '';
	$extra_join = '';
	if (is_array($where))
	{
		$opt = $where;
		$where = '';

		if (isset($opt['select'])) $extra_select = $opt['select'];
		if (isset($opt['join'])) $extra_join = $opt['join'];
		if (isset($opt['where'])) $where = $opt['where'];
		if (isset($opt['sort'])) $sort = $opt['sort'];
		if (isset($opt['order'])) $order = $opt['order'];
		if (isset($opt['num'])) $num = $opt['num'];
		if (isset($opt['start'])) $start = $opt['start'];
		if (isset($opt['in_stock_first'])) $in_stock_first = $opt['in_stock_first'];
		if (isset($opt['include_hidden'])) $include_hidden = $opt['include_hidden'];
	}
	$commentController = new Yoho\cms\Controller\CommentController();
	$comment_type = $commentController::COMMENT_TYPE_GOODS;
	// howang: Added virtual field "in_stock" and sort it so in stock goods always comes first
	// goods_number will be set to 20 if available for 訂貨
	if($need_real_stock) {
		$sql =  "SELECT *, " . hw_goods_in_stock_sql() . ", " . hw_goods_number_sql() . " FROM (".
			"SELECT g.`sales`+g.`sales_adjust` as shownum, ".
			"g.`goods_id`, g.`goods_sn`, g.`goods_name`, g.`goods_name_style`, g.`is_new`, g.`is_best`, g.`is_hot`, g.`country_of_origin`, g.sort_order as sort_order, ".
			"gperma, bperma, mp.user_rank, " .
			hw_goods_number_subquery('g.') . ", " .
			hw_reserved_number_subquery('g.') . ", " .
			"g.market_price, g.shop_price AS org_price, ".
			"IFNULL(mp.user_price, g.shop_price * '$_SESSION[discount]') AS shop_price, mp2.user_price as vip_price, ".
			"(CASE WHEN (fde.status = 1) THEN (IFNULL(fd2.goods_id, 0)) ELSE 0 END) AS is_flashdeal, MAX(fd2.deal_price) as today_deal_price, fd2.sort_order as fd_sort_order, ".
			"g.promote_price, g.promote_start_date, g.promote_end_date, ".
			"g.goods_thumb, g.goods_img, g.goods_brief, g.keywords, g.goods_type, g.last_update, g.display_discount, ".
			"g.is_pre_sale, g.pre_sale_date, g.pre_sale_days, AVG(cm.comment_rank) as rating, COUNT(cm.comment_rank) as rating_total " . $extra_select.
			"FROM " . $GLOBALS['ecs']->table('goods') . " AS g ".
			"LEFT JOIN  " . $GLOBALS['ecs']->table('goods_lang') . " AS gl ON g.goods_id = gl.goods_id AND gl.lang = '" . $_CFG['lang'] . "' " .
			"LEFT JOIN (SELECT id, table_name, lang, perma_link as bperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") bpl ON bpl.id = g.brand_id  AND bpl.table_name = 'brand' AND bpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
			"LEFT JOIN (SELECT id, table_name, lang, perma_link as gperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") gpl ON gpl.id = g.goods_id  AND gpl.table_name = 'goods' AND gpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
			"LEFT JOIN " . $GLOBALS['ecs']->table('member_price') . " AS mp2 ON mp2.goods_id = g.goods_id AND mp2.user_rank = 2 ". // TODO:VIP
			"LEFT JOIN " . $GLOBALS['ecs']->table('flashdeals') . " AS fd2 ON fd2.goods_id = g.goods_id AND fd2.deal_date BETWEEN '" . local_strtotime(local_date('Y-m-d')) . "' AND '" . gmtime() . "' ". // For flashdeal_watermark
			"LEFT JOIN " . $GLOBALS['ecs']->table('flashdeal_event') . " AS fde ON fde.fd_event_id = fd2.fd_event_id ".
			"LEFT JOIN " . $GLOBALS['ecs']->table('comment') . " AS cm ON cm.id_value = g.goods_id AND cm.comment_type = $comment_type AND cm.status = 1 ". // comment
			"LEFT JOIN " . $GLOBALS['ecs']->table('member_price') . " AS mp ON mp.goods_id = g.goods_id AND mp.user_rank = '$_SESSION[user_rank]' " . $extra_join.
			"WHERE $where " . ($include_hidden ? '' : " AND g.is_hidden = 0 ") .
			"GROUP BY g.goods_id " .
			") as tmp ";
	} else {
		$sql = "SELECT g.`sales`+g.`sales_adjust` as shownum, ".
			"g.`goods_id`, g.`goods_desc`, g.`goods_sn`, g.`goods_name`, g.`goods_name_style`, g.`is_new`, g.`is_best`, g.`is_hot`, g.`country_of_origin`, g.sort_order as sort_order, ".
			"gperma, bperma, mp.user_rank, " .
			"g.market_price, g.shop_price AS org_price, ".
			"IFNULL(mp.user_price, g.shop_price * '$_SESSION[discount]') AS shop_price, mp2.user_price as vip_price, ".
			"(CASE WHEN (fde.status = 1) THEN (IFNULL(fd2.goods_id, 0)) ELSE 0 END) AS is_flashdeal, MAX(fd2.deal_price) as today_deal_price, fd2.sort_order as fd_sort_order, ".
			" IF((SELECT sum(ega.goods_qty) FROM " . $GLOBALS['ecs']->table('erp_goods_attr_stock') . " as ega WHERE ega.goods_id = g.goods_id AND ega.warehouse_id IN (".$erpController->getSellableWarehouseIds(true).")) > 0 , 1, 0) as in_stock, ".
			"g.promote_price, g.promote_start_date, g.promote_end_date, ".
			"g.goods_thumb, g.goods_img, g.goods_brief, g.keywords, g.goods_type, g.last_update, g.display_discount, ".
			"g.is_pre_sale, g.pre_sale_date, g.pre_sale_days, AVG(cm.comment_rank) as rating, COUNT(cm.comment_rank) as rating_total " . $extra_select.
			"FROM " . $GLOBALS['ecs']->table('goods') . " AS g ".
			"LEFT JOIN  " . $GLOBALS['ecs']->table('goods_lang') . " AS gl ON g.goods_id = gl.goods_id AND gl.lang = '" . $_CFG['lang'] . "' " .
			"LEFT JOIN (SELECT id, table_name, lang, perma_link as bperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") bpl ON bpl.id = g.brand_id  AND bpl.table_name = 'brand' AND bpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
			"LEFT JOIN (SELECT id, table_name, lang, perma_link as gperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") gpl ON gpl.id = g.goods_id  AND gpl.table_name = 'goods' AND gpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
			"LEFT JOIN " . $GLOBALS['ecs']->table('member_price') . " AS mp2 ON mp2.goods_id = g.goods_id AND mp2.user_rank = 2 ". // TODO:VIP
			"LEFT JOIN " . $GLOBALS['ecs']->table('flashdeals') . " AS fd2 ON fd2.goods_id = g.goods_id AND fd2.deal_date BETWEEN '" . local_strtotime(local_date('Y-m-d')) . "' AND '" . gmtime() . "' ". // For flashdeal_watermark
			"LEFT JOIN " . $GLOBALS['ecs']->table('flashdeal_event') . " AS fde ON fde.fd_event_id = fd2.fd_event_id ".
			"LEFT JOIN " . $GLOBALS['ecs']->table('comment') . " AS cm ON cm.id_value = g.goods_id AND cm.comment_type = $comment_type AND cm.status = 1 ". // comment
			"LEFT JOIN " . $GLOBALS['ecs']->table('member_price') . " AS mp ON mp.goods_id = g.goods_id AND mp.user_rank = '$_SESSION[user_rank]' " . $extra_join.
			"WHERE $where " . ($include_hidden ? '' : " AND g.is_hidden = 0 ") .
			"GROUP BY g.goods_id ";
	}
    /* 2017-7-5 Anthony: Update goods list sort filter */
    /* 1. Default(using last_update): in_stock -> goods sort_order -> price -> last_update */
    /* 2. Shownum: shownum -> in_stock -> goods sort_order  */
    /* 3. goods_id: in_stock -> goods_id */
    /* 4. shop_price: price -> in_stock -> goods sort_order  */
    /* Other: in_stork -> sort */
    switch($sort){
        case 'last_update':
            $sql .=	"ORDER BY ". ' is_flashdeal DESC ,today_deal_price DESC , fd_sort_order ASC, user_rank DESC ,' . ($in_stock_first ? "in_stock DESC, " : '') . " sort_order DESC, shop_price DESC, "."$sort $order ";
            break;
        case 'shownum':
            $sql .=	"ORDER BY " ."$sort $order ". ($in_stock_first ? ", in_stock DESC " : '') . " ,sort_order $order , shop_price $order ";
            break;
        case 'goods_id':
            $sql .=	"ORDER BY " . ($in_stock_first ? "in_stock DESC, " : '') . "$sort $order ";
            break;
        case 'shop_price':
            $sql .=	"ORDER BY " ."$sort $order ". ($in_stock_first ? ", in_stock DESC " : '') . " ,sort_order $order ";
			break;
		case 'sort_order':
            $sql .=	"ORDER BY sort_order $order " . ", goods_id DESC";
            break;
        default:
			$sql .=	"ORDER BY " . ($in_stock_first ? "in_stock DESC, " : '') . "$sort $order ";
            break;
    }
	$sql .=	($num <= 0 ? '' : "LIMIT " . ($start > 0 ? $start . ',' : '') . $num);
	return $sql;
}

function hw_category_get_goods($children, $brand, $min, $max, $ext, $size, $page, $sort, $order, $is_promote = 0, $display = 'grid',$tags_goods = array(), $need_real_stock = true)
{
	$where = "g.is_on_sale = 1 AND g.is_alone_sale = 1 AND g.is_delete = 0 AND ($children OR " . get_extension_goods($children) . ')';

	if ($brand > 0)
	{
		$where .=  "AND g.brand_id=$brand ";
	}

	if ($min > 0)
	{
		$where .= " AND IFNULL(mp.user_price, g.shop_price * '$_SESSION[discount]') >= $min ";
	}

	if ($max > 0)
	{
		$where .= " AND IFNULL(mp.user_price, g.shop_price * '$_SESSION[discount]') <= $max ";
	}

	if($is_promote > 0)
	{
		$where.= " AND g.is_promote = 1 ";
	}

    if(!empty($tags_goods))
    {
        $where.= " AND g.goods_id ".db_create_in($tags_goods)." ";
    }

	$sql = hw_goods_list_sql(" $where $ext ", $sort, $order, $size, ($page - 1) * $size, $need_real_stock);
	
	$res = $GLOBALS['db']->getAll($sql);

	$arr = hw_process_goods_rows($res);

	return $arr;
}

function hw_process_goods_rows($rows, $localize = true)
{
	$userRankController = new Yoho\cms\Controller\UserRankController();
	$controller = new Yoho\cms\Controller\YohoBaseController();
	if ($localize)
	{
		$rows = localize_db_result('goods', $rows);
	}
	$arr = array();
	$goods_ids = array_column($rows, 'goods_id');
	$min_rank_list = $userRankController->getUserMinRank($goods_ids, $_SESSION['user_id']);
	foreach($rows as $row)
	{
		$min_rank = isset($min_rank_list[$row['goods_id']]) ? $min_rank_list[$row['goods_id']] : ['user_price' => 0];

		if($min_rank['user_price'] > 0) {
            $row['shop_price']  = min($min_rank['user_price'], $row['shop_price']);
            $row['sbuser_name'] = $min_rank['program_name'];
			$row['is_sp_price'] = true;
			$row['can_buy']     = $min_rank['can_buy'];
			$row['price_theme_color'] = $min_rank['theme_color'];
        }
		if ($row['promote_price'] > 0)
		{
			$promote_price = bargain_price($row['promote_price'], $row['promote_start_date'], $row['promote_end_date']);

			if($promote_price > $row['shop_price']){ // If promote price > shop_price , don't show promote_price
                $promote_price = 0;
            } else if ($promote_price > 0) {
                $row['is_sp_price'] = false;
			}
		}
		else
		{
			$promote_price = 0;
		}

        // If have vip price
        if(!empty($row['vip_price']))
        {
            $arr[$row['goods_id']]['vip_price']    = $row['vip_price'];
            $arr[$row['goods_id']]['is_vip_price'] = $row['shop_price'] == $row['vip_price'] ? true : false;
			$arr[$row['goods_id']]['vip_price_formated'] = price_format($row['vip_price']);
			if($arr[$row['goods_id']]['is_vip_price'])
            {
				$row['is_sp_price'] = false;
            }
        }
		// Get final price to display, and calculate savings
		if($promote_price > 0){ // If have promote_price, check price.

            $row['display_price'] = $promote_price;
            if($arr[$row['goods_id']]['is_vip_price'])
            {
				$row['display_price'] = min($promote_price, $row['vip_price']);
            }

        } else {
            $row['display_price'] = $row['shop_price'];
		}
		
		// 原產地
        if ($row['country_of_origin'] == '日本')
        {
            $row['country_of_origin_code'] = 'jp';
        }
        elseif ($row['country_of_origin'] == '美國')
        {
            $row['country_of_origin_code'] = 'us';
        }
        elseif ($row['country_of_origin'] == '韓國')
        {
            $row['country_of_origin_code'] = 'ko';
        }
        elseif ($row['country_of_origin'] == '歐洲')
        {
            $row['country_of_origin_code'] = 'eu';
        }
        elseif ($row['country_of_origin'] == '德國')
        {
            $row['country_of_origin_code'] = 'de';
        }
        else // Country of origin not set or unknown
        {
            $row['country_of_origin'] = '';
        }
        if (!empty($row['country_of_origin']))
        {
            $row['country_of_origin_name'] = _L('country_of_origin_' . $row['country_of_origin_code'], $row['country_of_origin']);
        }

		$row['saved_price'] = $row['market_price'] - $row['display_price'];
		$row['saved_percentage'] = (empty($row['market_price']) ? '100' : round($row['saved_price'] / $row['market_price'] * 100.0)) . '%';

		// YOHO: lowest price = 3% discount, have to pay with cash / ATM / ebanking
		$row['lowest_price'] = ceil($row['display_price'] * 0.97);
		$row['lowest_saved_price'] = $row['market_price'] - $row['lowest_price'];
		$row['lowest_saved_percentage'] = (empty($row['market_price']) ? '100' : round($row['lowest_saved_price'] / $row['market_price'] * 100.0)) . '%';
		if($row['rating_total'] > 0) {
			if($row['rating_total'] < 3 && $row['rating'] <= 3) {
				$row['rating'] = 0;
			}
            if($row['rating'] > 0) {
				$arr[$row['goods_id']]['rating'] = $row['rating'];
				$arr[$row['goods_id']]['rating_code'] = $controller->genRatingSvg($row['rating']);
            }
		} else {
//            $arr[$row['goods_id']]['rating'] = 0;
			if(strpos($_SERVER['SERVER_NAME'], 'beta.yohohongkong.com') !== false) {
				$arr[$row['goods_id']]['rating'] = rand(1, 5);
				$arr[$row['goods_id']]['rating_code'] = $controller->genRatingSvg($arr[$row['goods_id']]['rating']);
			}
		}
		// 限時特價 > 友和推廣 > 人氣熱賣 > 最新產品
		$watermark_img = '';
		if ($promote_price != 0)
		{
			$watermark_img = 'watermark-promote';
		}
		elseif ($row['is_best'])
		{
			$watermark_img = 'watermark-best';
		}
		elseif ($row['is_hot'])
		{
			$watermark_img = 'watermark-hot';
		}
		elseif ($row['is_new'])
		{
			$watermark_img = 'watermark-new';
		}
		$arr[$row['goods_id']]['watermark_img']    = $watermark_img;
		// If have falshdeal >> watermark_img
		if(!empty($row['is_flashdeal'])){
			$arr[$row['goods_id']]['is_flashdeal'] = true;
			$arr[$row['goods_id']]['watermark_img']    = "watermark-flashdeal";

			$row['display_price'] = $row['today_deal_price'];

			$row['saved_price'] = $row['market_price'] - $row['display_price'];
			$row['saved_percentage'] = (empty($row['market_price']) ? '100' : round($row['saved_price'] / $row['market_price'] * 100.0)) . '%';

			$row['lowest_price'] = ceil($row['display_price']);
			$row['lowest_saved_price'] = $row['market_price'] - $row['lowest_price'];
			$row['lowest_saved_percentage'] = (empty($row['market_price']) ? '100' : round($row['lowest_saved_price'] / $row['market_price'] * 100.0)) . '%';
		}
		$arr[$row['goods_id']]['goods_id']         = $row['goods_id'];
		$arr[$row['goods_id']]['goods_sn']         = $row['goods_sn'];
		$arr[$row['goods_id']]['name']             = $row['goods_name'];
		$arr[$row['goods_id']]['goods_name']       = $row['goods_name'];
		$arr[$row['goods_id']]['goods_style_name'] = add_style($row['goods_name'],$row['goods_name_style']);
		$arr[$row['goods_id']]['goods_brief']      = $row['goods_brief'];
		$arr[$row['goods_id']]['keywords']         = $row['keywords'];
		$arr[$row['goods_id']]['type']             = $row['goods_type'];
		$arr[$row['goods_id']]['market_price']     = price_format($row['market_price']);
		$arr[$row['goods_id']]['shop_price']       = price_format($row['shop_price']);
		$arr[$row['goods_id']]['promote_price']    = ($promote_price > 0) ? price_format($promote_price) : '';
		$arr[$row['goods_id']]['goods_thumb']      = get_image_path($row['goods_id'], $row['goods_thumb'], true);
		$arr[$row['goods_id']]['goods_img']        = get_image_path($row['goods_id'], $row['goods_img']);
		$arr[$row['goods_id']]['url']              = build_uri('goods', array('gid' => $row['goods_id'], 'gname' => $row['goods_name'], 'gperma' => $row['gperma']), $row['goods_name']);
		$arr[$row['goods_id']]['number']           = empty($row['goods_number']) ? '0' : $row['goods_number'];
		$arr[$row['goods_id']]['goods_number']     = empty($row['goods_number']) ? '0' : $row['goods_number'];
		$arr[$row['goods_id']]['shownum']          = empty($row['shownum']) ? '0' : $row['shownum']; // 銷量
		$arr[$row['goods_id']]['in_stock']         = $row['in_stock']; //是否有貨 (有現貨或可訂貨)

		$arr[$row['goods_id']]['display_price']           = price_format($row['display_price']);
		$arr[$row['goods_id']]['display_price_number']    = $row['display_price'];
		$arr[$row['goods_id']]['saved_price']             = price_format($row['saved_price']);
		$arr[$row['goods_id']]['saved_percentage']        = $row['saved_percentage'];
		$arr[$row['goods_id']]['lowest_price']            = price_format($row['lowest_price']);
		$arr[$row['goods_id']]['lowest_saved_price']      = price_format($row['lowest_saved_price']);
		$arr[$row['goods_id']]['lowest_saved_percentage'] = $row['lowest_saved_percentage'];
		$arr[$row['goods_id']]['is_pre_sale'] = $row['is_pre_sale'];
		$arr[$row['goods_id']]['display_discount'] = $row['display_discount'];
		$arr[$row['goods_id']]['is_sp_price'] = $row['is_sp_price'];
		$arr[$row['goods_id']]['can_buy'] = $row['can_buy'];
		$arr[$row['goods_id']]['price_theme_color'] = $row['price_theme_color'];
		$arr[$row['goods_id']]['country_of_origin'] = $row['country_of_origin'];
		$arr[$row['goods_id']]['country_of_origin_code'] = $row['country_of_origin_code'];
		$arr[$row['goods_id']]['country_of_origin_name'] = $row['country_of_origin_name'];
		//特殊會員計劃名稱
		$arr[$row['goods_id']]['program_name'] = $row['sbuser_name'];
		$arr[$row['goods_id']]['program_theme_color'] = $row['price_theme_color'];
	}
	return $arr;
}

function hw_get_cagtegory_goods_count($children, $brand = 0, $min = 0, $max = 0, $ext='', $is_promote = 0)
{
	$where  = "g.is_on_sale = 1 AND g.is_alone_sale = 1 AND g.is_delete = 0 AND ($children OR " . get_extension_goods($children) . ')';

	if ($brand > 0)
	{
		$where .=  " AND g.brand_id = $brand ";
	}

	if ($min > 0)
	{
		$where .= " AND IFNULL(mp.user_price, g.shop_price * '$_SESSION[discount]') >= $min ";
	}

	if ($max > 0)
	{
		$where .= " AND IFNULL(mp.user_price, g.shop_price * '$_SESSION[discount]') <= $max ";
	}

	if ($is_promote > 0)
	{
		$where.=" AND g.is_promote = 1 ";
	}

	/* 返回商品总数 */
	return $GLOBALS['db']->getOne('SELECT COUNT(*) FROM ' . $GLOBALS['ecs']->table('goods') . " AS g  LEFT JOIN  " . $GLOBALS['ecs']->table('member_price'). " AS mp ON g.goods_id = mp.goods_id AND mp.user_rank = $_SESSION[user_rank] WHERE $where $ext");
}

function hw_get_all_categories($cat_id = 0)
{
	if ($cat_id > 0)
	{
		$parent_id = $cat_id;
	}
	else
	{
		$parent_id = 0;
	}

	/*
	 判断当前分类中全是是否是底级分类，
	 如果是取出底级分类上级分类，
	 如果不是取当前分类及其下的子分类
	*/
	$sql = 'SELECT count(*) FROM ' . $GLOBALS['ecs']->table('category') . " WHERE parent_id = '$cat_id' AND is_show = 1 ";
	if ($parent_id == 0) // Listing from root category
	{
		/* 获取当前分类的子分类及其孫分类 */
		$sql = 'SELECT a.cat_id, a.cat_name, a.sort_order AS parent_order, a.cat_id, a.is_show,' .
					'b.cat_id AS child_id, b.cat_name AS child_name, b.sort_order AS child_order, ' .
					"-1 as num " .//'(SELECT count(*) FROM ' . $GLOBALS['ecs']->table('goods') . ' AS g WHERE g.cat_id = b.cat_id AND g.is_delete = 0 AND g.is_on_sale = 1) as num ' .
				'FROM ' . $GLOBALS['ecs']->table('category') . ' AS a ' .
				'LEFT JOIN ' . $GLOBALS['ecs']->table('category') . ' AS b ON b.parent_id = a.cat_id AND b.is_show = 1 ' .
				"WHERE a.parent_id IN (SELECT c.cat_id FROM " . $GLOBALS['ecs']->table('category') . " AS c WHERE c.parent_id = '$parent_id') ORDER BY parent_order ASC, a.cat_id ASC, child_order ASC";
	}
	else if ($GLOBALS['db']->getOne($sql) || $parent_id == 0)
	{
		/* 获取当前分类及其子分类 */
		$sql = 'SELECT a.cat_id, a.cat_name, a.sort_order AS parent_order, a.cat_id, a.is_show,' .
					'b.cat_id AS child_id, b.cat_name AS child_name, b.sort_order AS child_order, ' .
					"-1 as num " .//'(SELECT count(*) FROM ' . $GLOBALS['ecs']->table('goods') . ' AS g WHERE g.cat_id = b.cat_id AND g.is_delete = 0 AND g.is_on_sale = 1) as num ' .
				'FROM ' . $GLOBALS['ecs']->table('category') . ' AS a ' .
				'LEFT JOIN ' . $GLOBALS['ecs']->table('category') . ' AS b ON b.parent_id = a.cat_id AND b.is_show = 1 ' .
				"WHERE a.parent_id = '$parent_id' ORDER BY parent_order ASC, a.cat_id ASC, child_order ASC";
	}
	else
	{
		/* 获取当前分类及其父分类 */
		$sql = 'SELECT a.cat_id, a.cat_name, b.cat_id AS child_id, b.cat_name AS child_name, b.sort_order, b.is_show, ' .
					"-1 as num " .//'(SELECT count(*) FROM ' . $GLOBALS['ecs']->table('goods') . ' AS g WHERE g.cat_id = b.cat_id AND g.is_delete = 0 AND g.is_on_sale = 1) as num ' .
				'FROM ' . $GLOBALS['ecs']->table('category') . ' AS a ' .
				'LEFT JOIN ' . $GLOBALS['ecs']->table('category') . ' AS b ON b.parent_id = a.cat_id AND b.is_show = 1 ' .
				"WHERE b.parent_id = '$parent_id' ORDER BY sort_order ASC";
	}
	$res = $GLOBALS['db']->getAll($sql);

	$cat_arr = array();
	foreach ($res AS $row)
	{
		if ($row['is_show'])
		{
			$cat_arr[$row['cat_id']]['id']   = $row['cat_id'];
			$cat_arr[$row['cat_id']]['name'] = $row['cat_name'];
			$cat_arr[$row['cat_id']]['url']  = build_uri('category', array('cid' => $row['cat_id'], 'cname' => $row['cat_name']), $row['cat_name']);
			$cat_arr[$row['cat_id']]['num']  = (isset($cat_arr[$row['cat_id']]['num']) ? $cat_arr[$row['cat_id']]['num'] : 0) + $row['num'];

			if ($row['child_id'] != NULL)
			{
				$cat_arr[$row['cat_id']]['children'][$row['child_id']]['id']   = $row['child_id'];
				$cat_arr[$row['cat_id']]['children'][$row['child_id']]['name'] = $row['child_name'];
				$cat_arr[$row['cat_id']]['children'][$row['child_id']]['url']  = build_uri('category', array('cid' => $row['child_id'], 'cname' => $row['child_name']), $row['child_name']);
				$cat_arr[$row['cat_id']]['children'][$row['child_id']]['num']  = $row['num'];
			}
		}
	}

	return $cat_arr;
}

function hw_get_categories_menu($cat_ids)
{
	if (!is_array($cat_ids))
	{
		$cat_ids = array(0);
	}
	return array_reduce(array_map('hw_get_all_categories', array_unique($cat_ids)), 'array_merge', array());
}

function hw_get_cat_name($id)
{
	if (!$id)
	{
		return _L('productlist_category_root', '全部產品');
	}
	global $_CFG, $db, $ecs;
	$sql = "SELECT IFNULL(cl.`cat_name`, c.`cat_name`) " .
			"FROM " . $ecs->table('category')  . " as c " .
			"LEFT JOIN " . $ecs->table('category_lang')  . " as cl ON c.cat_id = cl.cat_id AND cl.lang = '" . $_CFG['lang'] . "' " .
			"WHERE c.`cat_id` = '" . $id . "' ";
	$cat_name = $db->getOne($sql);
	return $cat_name;
}

function hw_get_cat_parent($value, $id = 0)
{
	if($value != 0)
	{
		$sql = 'SELECT parent_id FROM ' . $GLOBALS['ecs']->table('category') . " WHERE cat_id = '$value'";
		$res = $GLOBALS['db']->getOne($sql);
		return hw_get_cat_parent($res, $value);
	}
	else
	{
		return $id;
	}
}

function hw_get_subcategories($cat_id)
{
	global $_CFG, $db, $ecs;

	$sql = "SELECT parent_id FROM " . $ecs->table('category') . " WHERE cat_id = '$cat_id'";
	if ($db->getOne($sql) == 0) // Parent = 0, $cat_id is a 1st-level category
	{
		$sql = "SELECT cat_id FROM " . $ecs->table('category') . " WHERE parent_id = '$cat_id' AND is_show = 1";
		$q = $db->query($sql);
		if ($db->num_rows($q) == 1) // Only one subcategory, that means we can traverse one level down to 2nd-level
		{
			$cat_id = array_pop($db->fetchRow($q));
		}
	}

	$sql = "SELECT c.cat_id as `id`, IFNULL(cl.cat_name, c.cat_name) as `name`, IFNULL(pl.cat_name, p.cat_name)  as `parent_name`, cperma " .
			"FROM " . $ecs->table('category') . " as p " .
			"LEFT JOIN " . $ecs->table('category') . " as c ON c.parent_id = p.cat_id AND c.is_show = 1 " .
			"LEFT JOIN " . $ecs->table('category_lang') . " as cl ON c.cat_id = cl.cat_id AND cl.lang = '" . $_CFG['lang']  . "' " .
			"LEFT JOIN " . $ecs->table('category_lang') . " as pl ON p.cat_id = pl.cat_id AND pl.lang = '" . $_CFG['lang']  . "' " .
			'LEFT JOIN (SELECT id, table_name, lang, perma_link as cperma FROM ' . $GLOBALS['ecs']->table('perma_link') . ') cpl ON cpl.id = c.cat_id  AND cpl.table_name = "category" AND cpl.lang = "' . $GLOBALS['_CFG']['lang'] . '"' .
			"WHERE c.cat_id = '$cat_id' AND p.is_show = 1 " .
			"ORDER BY c.sort_order ASC";
	$res = $db->getAll($sql);
	// Use build_uri to obtain SEO friendly URLs for the category
	foreach ($res as $key => $row)
	{
		$res[$key]['url'] = build_uri('category', array('cid' => $row['id'], 'cname' => $row['name'], 'cperma' => $row['cperma']), $row['name']);
	}
	return $res;
}

function hw_assign_price_range($where, $vip = 0)
{
    global $db, $ecs, $smarty;
    /*
        算法思路：
            1、当分级大于1时，进行价格分级
            2、取出该类下商品价格的最大值、最小值
            3、根据商品价格的最大值来计算商品价格的分级数量级：
                    价格范围(不含最大值)    分级数量级
                    0-0.1                   0.001
                    0.1-1                   0.01
                    1-10                    0.1
                    10-100                  1
                    100-1000                10
                    1000-10000              100
            4、计算价格跨度：
                    取整((最大值-最小值) / (价格分级数) / 数量级) * 数量级
            5、根据价格跨度计算价格范围区间
            6、查询数据库

        可能存在问题：
            1、
            由于价格跨度是由最大值、最小值计算出来的
            然后再通过价格跨度来确定显示时的价格范围区间
            所以可能会存在价格分级数量不正确的问题
            该问题没有证明
            2、
            当价格=最大值时，分级会多出来，已被证明存在
    */
    if($vip > 0){ // Have vip number
        $sql = "SELECT IFNULL(min(IFNULL(mp.user_price, g.shop_price * '$_SESSION[discount]')),'0') AS min, IFNULL(max(IFNULL(mp.user_price, g.shop_price * '$_SESSION[discount]')),'0') as max ".
           " FROM " . $ecs->table('goods'). " AS g ".
           " LEFT JOIN  " . $ecs->table('member_price'). " AS mp ON g.goods_id = mp.goods_id AND mp.user_rank = $vip ".
           " WHERE " . $where;
    } else {
        $sql = "SELECT IFNULL(min(g.shop_price),'0') AS min, IFNULL(max(g.shop_price),'0') as max ".
           " FROM " . $ecs->table('goods'). " AS g ".
           " WHERE " . $where;
    }
           //获得当前搜尋條件下商品价格的最大值、最小值
    $row = $db->getRow($sql);

    // If max = min...
    if ($row['max'] - $row['min'] == 0)
    {
        // $smarty->assign('price_range',      array($row['min'], $row['max']));
        // $smarty->assign('price_range_step', 1);
        // $smarty->assign('price_range_min',  $row['min']);
        // $smarty->assign('price_range_max',  $row['max']);
		$smarty->assign('price_range',      array(0, $row['max']));
		$smarty->assign('price_range_step', $row['max']);
		$smarty->assign('price_range_min',  0);
		$smarty->assign('price_range_max',  $row['max']);
        return;
    }

    // 取得价格分级最小单位级数，比如，千元商品最小以100为级数
    $price_grade = 0.0001;
    for($i=-2; $i<= log10($row['max']); $i++)
    {
        $price_grade *= 10;
    }

    $price_grade_num = 3;

    //跨度
    $dx = ceil(($row['max'] - $row['min']) / ($price_grade_num) / $price_grade) * $price_grade;
    if($dx == 0)
    {
        $dx = $price_grade;
    }

    for($i = 1; $row['min'] > $dx * $i; $i ++);

    for($j = 1; $row['min'] > $dx * ($i-1) + $price_grade * $j; $j++);
    $row['min'] = $dx * ($i-1) + $price_grade * ($j - 1);

    for(; $row['max'] >= $dx * $i; $i ++);
    $row['max'] = $dx * ($i) + $price_grade * ($j - 1);

    $step = $price_grade;

    if($vip > 0 ){
        $sql = "SELECT (FLOOR((IFNULL(mp.user_price, g.shop_price * '$_SESSION[discount]') - $row[min]) / $dx)) AS sn, COUNT(*) AS goods_num  ".
           " FROM " . $ecs->table('goods') . " AS g ".
           " LEFT JOIN  " . $ecs->table('member_price'). " AS mp ON g.goods_id = mp.goods_id AND mp.user_rank = $vip ".
           "WHERE " . $where .
           " GROUP BY sn ";
    } else {
        $sql = "SELECT (FLOOR((g.shop_price - $row[min]) / $dx)) AS sn, COUNT(*) AS goods_num  ".
           " FROM " . $ecs->table('goods') . " AS g ".
           "WHERE " . $where .
           " GROUP BY sn ";
    }
    $price_grade = $db->getAll($sql);

    $price_range = array();
    $price_range[] = 0;
    foreach ($price_grade as $val)
    {
        $this_grade = $row['min'] + round($dx * $val['sn']);
        if ($this_grade > 0)
        {
            $price_range[] = $this_grade;
        }
    }
    $price_range[] = $row['min'] + round($dx * ($price_grade[count($price_grade) - 1]['sn'] + 1));
    $smarty->assign('price_range',      $price_range);
    $smarty->assign('price_range_step', $step);
    $smarty->assign('price_range_min',  $price_range[0]);
    $smarty->assign('price_range_max',  $price_range[count($price_range) - 1]);
}

function hw_get_brand_name($brand_id = 0)
{
	if (!$brand_id)
	{
		return _L('productlist_brand_filter_all', '全部');
	}
    global $_CFG, $db, $ecs;
    $sql = "SELECT IFNULL(bl.`brand_name`, b.`brand_name`) " .
        "FROM " . $ecs->table('brand') . " as b " .
        "LEFT JOIN " . $ecs->table('brand_lang') . " as bl ON b.brand_id = bl.brand_id AND bl.lang = '" . $_CFG['lang'] . "' " .
        "WHERE b.brand_id = '" . $brand_id . "' ";
    $brand_name = $db->getOne($sql);
    return $brand_name;
}

?>
