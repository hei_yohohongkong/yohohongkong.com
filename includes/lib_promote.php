<?php

/**
 * ECSHOP 购物流程函数库
 * ============================================================================
 * * 版权所有 2005-2012 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liubo $
 * $Id: lib_order.php 17217 2011-01-19 06:29:08Z liubo $
 */

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

/**
 * 处理序列化的支付、配送的配置参数
 * 返回一个以name为索引的数组
 *
 * @access  public
 * @param   string       $cfg
 * @return  void
 */

function get_promotion_info_by_cat($cat, $promote_kind="featured")
{
    $promote_kind = in_array($promote_kind, array("featured", "gift", "redeem", "package", "info")) ? $promote_kind : "featured";

    $category = isset($cat)  ? intval($cat) : (isset($cat) ? intval($cat) : 0);
    
    $categories = [];
    $categories_list_regexp_str = '';
    
    if ($category != 0) {
        $categories = (array_unique(array_merge(array($category), array_keys(cat_list($category, 0, false)))));
        foreach($categories as $category){
            $categories_list[] = ",".$category.",";
        }
        $categories_list_regexp_str = implode("|", $categories_list);    
    }

    $promotion_info = array();
    switch ($promote_kind){
        case "gift":
        case "redeem":
            $act_kind = $promote_kind=="gift"?"1":"2";
            $promotion_info = get_favourable_info($act_kind, $categories, $categories_list_regexp_str);
            break;
        case "package":
            $promotion_info = get_packages_info($categories, $categories_list_regexp_str);
            break;
        case "info":
            $promotion_info = get_topics_info($categories, $categories_list_regexp_str);
            break;
        case "featured":
            $gifts_arr = get_favourable_info(1, $categories, $categories_list_regexp_str);
            $redeems_arr = get_favourable_info(2, $categories, $categories_list_regexp_str);
            $packages_arr = get_packages_info($categories, $categories_list_regexp_str);
            $topics_arr = get_topics_info($categories, $categories_list_regexp_str);

            $promotion_info = merge_all_promotion(array($gifts_arr, $redeems_arr, $packages_arr, $topics_arr));
            break;
    }

    usort($promotion_info, function($a, $b) {
        return $b['sort_order'] - $a['sort_order'];
    });
    return $promotion_info;

}

function get_product_information($goods_ids, $categories=null, $limit=1, $topic){
    $where = "";
    if($categories){
        $where = " AND cat_id " . db_create_in($categories);
    }

    $order_by = " ORDER BY sort_order DESC , goods_id DESC";
    /*
    if($topic){
        $order_by = " ORDER BY goods_id DESC ";
    }
    */

    $sql = "SELECT goods_id, goods_img, goods_name, sort_order FROM " . $GLOBALS['ecs']->table('goods') .
    " WHERE is_delete = 0 AND is_hidden = 0 AND is_on_sale = 1 AND is_alone_sale = 1 " .
    " AND goods_id " . db_create_in($goods_ids) .
    $where .
    $order_by .
    " LIMIT " . $limit;

    
    $res = $GLOBALS['db']->getAll($sql);

    $data = array();
    foreach ($res as $row)
    {
        if($row['goods_img'] == ""){
            $row['goods_img'] = $GLOBALS['_CFG']['no_picture'];
        }
        $data[] = $row;
    }

    return $data;
}

function _find_goods_sort_order($targer_goods,$source_goods,$categories,$limit = 0)
{
    $sorted = [];

    $source_goods_temp = [];
    foreach ($source_goods as $source_goods_item) {
        $source_goods_temp[$source_goods_item['goods_id']] = $source_goods_item;
    }

    $source_goods = $source_goods_temp;

    foreach ($targer_goods as $item_goods_id) {
        if (!empty($categories)) {
            if (!in_array($source_goods[$item_goods_id]['sort_order']['cat_id'],$categories)) {
                continue;
            }
        }
        
        $sorted[$item_goods_id] = $source_goods[$item_goods_id]['sort_order']; 
    }
 
    arsort($sorted);
    $sorted_key = array_keys($sorted);

    $count = 0;
    $result = [];
    foreach ($sorted as $sorted_key => $sorted_item) {
        if ($count >= $limit) {
            break;
        }
       
        $result[] = $source_goods[$sorted_key];
        $count++;
    }
    


    return $result;
}

function get_favourable_info($search_act_kind, $search_categories, $search_categories_list){
    $now = gmtime();

    $sql = "SELECT fa.act_id, fa.act_range_ext, fa.act_type, fa.act_kind, fa.gift, fa.act_name, fal.act_name as act_name_i18n, fa.act_tag, fal.act_tag as act_tag_i18n " .
            "FROM " . $GLOBALS['ecs']->table('favourable_activity') . " fa 
            left join " . $GLOBALS['ecs']->table('favourable_activity_lang') . "fal on (fa.act_id = fal.act_id) and lang = '".$GLOBALS['_CFG']['lang']."' " .
            " WHERE CONCAT(',', user_rank, ',') LIKE '%" . $_SESSION['user_rank'] . "%'" .
            " AND start_time <= '$now' AND end_time >= '$now'" .
            " AND status = 1" .
            " AND act_type = '" . FAT_GOODS . "'" .
            " AND act_kind = '" . $search_act_kind . "'";

    $sql .= ($search_categories_list != ''? " AND CONCAT(',', fa.related_category, ',') REGEXP '" . $search_categories_list . "'" : '');
    $sql .= " GROUP BY fa.act_id ORDER BY sort_order";
    $res = $GLOBALS['db']->getAll($sql);

    $all_gifts_ids = array();
    $all_goods_ids = array();
    foreach ($res as $row)
    {
        $row['gift'] = unserialize( $row['gift']);
        $gifts_ids = array_map(function ($gift) { return $gift['id']; }, $row['gift']);
        
        foreach($gifts_ids as $gifts_id){
            $all_gifts_ids[] = $gifts_id;
            $all_goods_ids[] = $gifts_id;
        }

        $act_range_ext_ar = explode(',',$row["act_range_ext"]);
        $all_goods_ids = array_merge($all_goods_ids,$act_range_ext_ar);
    }

    $all_goods_info = get_product_information($all_goods_ids, null,10000);

    foreach ($res as $row)
    {
        $promotion = array();
        $row['gift'] = unserialize( $row['gift']);
        $gifts_ids = array_map(function ($gift) { return $gift['id']; }, $row['gift']);
        
        $promotion['main_product'] =  _find_goods_sort_order(explode(',',$row["act_range_ext"]),$all_goods_info,$search_categories,1)[0];
        //$promotion['main_product'] = get_product_information($row["act_range_ext"], $search_categories, 1)[0];
        $promotion["url"] = build_uri('goods', array('gid' => $promotion['main_product']['goods_id']), $promotion['main_product']['goods_name']);
        $promotion['sub_product'] = get_product_information($gifts_ids, null, 2);
        //$promotion['sub_product'] =  _find_goods_sort_order($gifts_ids,$all_goods_info,$search_categories,2);
        $promotion['promote_kind'] = $search_act_kind==1?"gift":"redeem";
        $promotion['sort_order'] = $promotion['main_product']['sort_order'];
        $promotion['gifts'] = $row['gift'];
        if (!empty($row['act_name_i18n'])) {
            $row['act_name'] = $row['act_name_i18n'];
        }
        if (!empty($favourable['act_tag_i18n'])) {
            $row['act_tag'] = $row['act_tag_i18n'];
        }
        $promotion["promote_desc"] = $row['act_tag'];

        $promotion_info[] = $promotion;
    }

    if($search_act_kind==1){
        $erpController = new \Yoho\cms\Controller\ErpController();
        $gifts_stock = $erpController->getSumOfSellableProductsQtyByWarehouses($all_gifts_ids,null,true,null,false);

        $sql = "SELECT g.goods_id, g.goods_name, g.goods_thumb, g.goods_img, g.shop_price, g.is_on_sale, g.goods_sn, gperma " ." FROM " . $GLOBALS['ecs']->table('goods') . "as g " .
            "LEFT JOIN (SELECT id, table_name, lang, perma_link as gperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") gpl ON gpl.id = g.goods_id  AND gpl.table_name = 'goods' AND gpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
            "WHERE g.goods_id " .  db_create_in($all_gifts_ids);
        $gifts_goods_list = $GLOBALS['db']->getAll($sql);
        $gifts_goods_list = localize_db_result('goods', $gifts_goods_list);
        foreach($promotion_info as $promotion_key => $promotion){
            foreach ($promotion['gifts'] as $key => $value){
                $goods = null;
                foreach ($gifts_goods_list as $g)
                {
                    if ($g['goods_id'] == $value['id'])
                    {
                        $goods = $g;
                        break;
                    }
                }
                if(!$goods || !$goods['is_on_sale'])
                {
                    unset($promotion['gifts'][$key]);
                    continue;
                }

                $stock = $gifts_stock[$value['id']];
                $stock = $stock > 0 ? $stock : 0;
                if($stock == 0){
                    unset($promotion['gifts'][$key]);
                    continue;
                }
            }

            if(empty($promotion['gifts'])){
                unset($promotion_info[$promotion_key]);
            }
        }
    }
    

    return $promotion_info;
}

function get_packages_info($search_categories, $search_categories_list){
    $now = gmtime();

    $sql = "SELECT ga.act_id, ga.act_name, gal.act_name as act_name_i18n, ga.act_tag, gal.act_tag as act_tag_i18n 
        FROM " . $GLOBALS['ecs']->table('goods_activity') . " AS ga left join ".$GLOBALS['ecs']->table('goods_activity_lang')." gal on (ga.act_id = gal.act_id) and lang = '".$GLOBALS['_CFG']['lang']."' 
        WHERE ga.start_time <= '" . $now . "'
        AND ga.end_time >= '" . $now . "'
        AND status = 1"; 
    //  AND CONCAT(',', ga.related_category, ',') REGEXP '" . $search_categories_list . "'";
    //  $res = $GLOBALS['db']->getAll($sql);

    $sql .= ($search_categories_list != ''? " AND CONCAT(',', ga.related_category, ',') REGEXP '" . $search_categories_list . "'" : '');
    //$sql .= " GROUP BY fa.act_id ORDER BY sort_order";
    $res = $GLOBALS['db']->getAll($sql);

    //get product for each package
    $package_ids = array_map(function ($package) { return $package['act_id']; }, $res);

    $sql = "SELECT package_id, goods_id
        FROM " . $GLOBALS['ecs']->table('package_goods') . " 
        WHERE package_id " . db_create_in($package_ids);
    $pg_res = $GLOBALS['db']->getAll($sql);

    $package_goods = array();
    $all_goods_ids = array();
    foreach($pg_res as $pg){
        $package_goods[$pg["package_id"]][] = $pg["goods_id"];
        $all_goods_ids[] = $pg["goods_id"];
    }

    $all_goods_info = get_product_information($all_goods_ids, null,10000);

    foreach ($res as $row)
    {
        $promotion = array();
        $package_good = $package_goods[$row['act_id']];

        //get main product
        $promotion['main_product'] =  _find_goods_sort_order($package_good,$all_goods_info,$search_categories,1)[0];
        //$promotion['main_product'] = get_product_information($package_good, $search_categories, 1)[0];
        $promotion["url"] = build_uri('goods', array('gid' => $promotion['main_product']['goods_id']), $promotion['main_product']['goods_name']);

        unset($package_good[array_search($promotion['main_product']['goods_id'], $package_good)]);
        //$promotion['sub_product'] = get_product_information($package_good, null, 2);
        $promotion['sub_product'] = _find_goods_sort_order($package_good,$all_goods_info,null,2);
      
        $promotion['promote_kind'] = "package";
        $promotion['sort_order'] = $promotion['main_product']['sort_order'];
        if (!empty($row['act_name_i18n'])) {
            $row['act_name'] = $row['act_name_i18n'];
        }
        if (!empty($favourable['act_tag_i18n'])) {
            $row['act_tag'] = $row['act_tag_i18n'];
        }
        $promotion["promote_desc"] = $row['act_tag'];
    
        $promotion_info[] = $promotion;
    }

    return $promotion_info;
}

function get_topics_info($search_categories, $search_categories_list){
    $sql = "SELECT t.*, tperma FROM " . $GLOBALS['ecs']->table('topic') .  
        " t LEFT JOIN (SELECT id, table_name, lang, perma_link as tperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") acpl ON acpl.id = t.topic_id  AND acpl.table_name = 'topic' AND acpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
        "WHERE (" . gmtime() . " BETWEEN start_time AND end_time + 86399) " ;
    //    "AND CONCAT(',', related_category, ',') REGEXP '" . $search_categories_list . "'".
    //    "ORDER BY `sort_order` ASC,`topic_id` ASC";
    //$res = $GLOBALS['db']->getAll($sql);

    $sql .= ($search_categories_list != ''? " AND CONCAT(',', related_category, ',') REGEXP '" . $search_categories_list . "'" : '');
    $sql .= "ORDER BY `sort_order` ASC,`topic_id` ASC";
    $res = $GLOBALS['db']->getAll($sql);

    $all_goods_ids = [];
    foreach ($res as $row)
    {
        if(file_exists(ROOT_PATH . DATA_DIR . '/afficheimg/topic/' . $row['topic_id'] . '_icon.png')){
            $promotion = array();

            $topic_data_temp = @unserialize($row['data']);
            if (!$topic_data) // Load old malformed data
            {
                $row['data'] = addcslashes($row['data'], "'");
                $topic_data_temp = @unserialize($row['data']);
            }
            $info_good = array();
            foreach($topic_data_temp as $brand){
                foreach($brand as $product){
                    $all_goods_ids[] = explode("|", $product)[1];
                }
            }
        }
    }

    $all_goods_info = get_product_information($all_goods_ids, null,10000);

    foreach ($res as $row)
    {
        if(file_exists(ROOT_PATH . DATA_DIR . '/afficheimg/topic/' . $row['topic_id'] . '_icon.png')){
            $promotion = array();

            $topic_data_temp = @unserialize($row['data']);
            if (!$topic_data) // Load old malformed data
            {
                $row['data'] = addcslashes($row['data'], "'");
                $topic_data_temp = @unserialize($row['data']);
            }
            $info_good = array();
            foreach($topic_data_temp as $brand){
                foreach($brand as $product){
                    $info_good[] = explode("|", $product)[1];
                }
            }

            //get main product
            // $promotion['main_product'] = get_product_information($info_good, $search_categories, 1, true)[0];
            $promotion['main_product'] =  _find_goods_sort_order($info_good,$all_goods_info,$search_categories,1,true)[0];
            $promotion["url"] = build_uri('topic', array('tid' => $row['topic_id'],'tperma'=>$row['tperma']));
            unset($info_good[array_search($promotion['main_product']['goods_id'], $info_good)]);
            //$promotion['sub_product'] = get_product_information($info_good, null, 2, true);
            $promotion['sub_product'] = _find_goods_sort_order($info_good,$all_goods_info,null,2,true);
            $promotion['promote_kind'] = "info";
            $promotion['sort_order'] = $promotion['main_product']['sort_order'];

            if (!empty($row['title_i18n'])) {
                $row['title'] = $row['title_i18n'];
            }
            if (!empty($row['tag_i18n'])) {
                $row['tag'] = $row['tag_i18n'];
            }
            $promotion["promote_desc"] = $row['tag'];
        
            $promotion_info[] = $promotion;
        }
    }

    return $promotion_info;
}

function merge_all_promotion($promotions){
    $return = array();
    if($promotions){
        foreach($promotions as $promotion){
            foreach($promotion as $detail){
                $return[] = $detail;
            }
        }
    }

    return $return;
}

?>
