<?php

/**
 * ECSHOP 前台公用文件
 * ============================================================================
 * 版权所有 2005-2010 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liuhui $
 * $Id: init.php 17153 2010-05-05 09:39:12Z liuhui $
*/
if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

error_reporting(E_ALL);

if (__FILE__ == '')
{
    die('Fatal error code: 0');
}

/* 取得当前ecshop所在的根目录 */
define('ROOT_PATH', str_replace('includes/init.php', '', str_replace('\\', '/', __FILE__)));

if (!file_exists(ROOT_PATH . 'data/install.lock') && !file_exists(ROOT_PATH . 'includes/install.lock')
    && !defined('NO_CHECK_INSTALL'))
{
    header("Location: ./install/index.php\n");

    exit;
}

/* 初始化设置 */
@ini_set('memory_limit',          '90M');
@ini_set('session.cache_expire',  180);
@ini_set('session.use_trans_sid', 0);
@ini_set('session.use_cookies',   1);
@ini_set('session.auto_start',    0);
@ini_set('display_errors',        1);

if (DIRECTORY_SEPARATOR == '\\')
{
    @ini_set('include_path', '.;' . ROOT_PATH);
}
else
{
    @ini_set('include_path', '.:' . ROOT_PATH);
}

require(ROOT_PATH . 'data/config.php');

if (defined('DEBUG_MODE') == false)
{
    define('DEBUG_MODE', 0);
}

if (PHP_VERSION >= '5.1' && !empty($timezone))
{
    date_default_timezone_set($timezone);
}

$php_self = isset($_SERVER['PHP_SELF']) ? $_SERVER['PHP_SELF'] : $_SERVER['SCRIPT_NAME'];
if ('/' == substr($php_self, -1))
{
    $php_self .= 'index.php';
}
define('PHP_SELF', $php_self);

/* PSR-4 class autoloading */
spl_autoload_register(function ($class) {
    $prefix = 'Yoho\\cms\\';
    $base_dir = ROOT_PATH . 'x/includes/';
    $len = strlen($prefix);
    if (strncmp($prefix, $class, $len) !== 0) {
        return;
    }

    $relative_class = substr($class, $len);
    $file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';
    if (file_exists($file)) {
        require $file;
    }
});

require(ROOT_PATH . 'core/lib_mobile.php');
require(ROOT_PATH . 'core/lib_https.php');
require(ROOT_PATH . 'includes/inc_constant.php');
require(ROOT_PATH . 'includes/cls_ecshop.php');
require(ROOT_PATH . 'includes/cls_error.php');
require(ROOT_PATH . 'includes/lib_time.php');
require(ROOT_PATH . 'includes/lib_base.php');
require(ROOT_PATH . 'includes/lib_common.php');
require(ROOT_PATH . 'includes/lib_main.php');
require(ROOT_PATH . 'includes/lib_insert.php');
require(ROOT_PATH . 'includes/lib_goods.php');
require(ROOT_PATH . 'includes/lib_howang.php');
require(ROOT_PATH . 'includes/lib_i18n.php');
require(ROOT_PATH . 'includes/ctrl_base.php');

$dir = new DirectoryIterator(dirname(__FILE__));
foreach ($dir as $fileinfo) {
    if (!$fileinfo->isDot()) {
        if(substr($fileinfo->getFilename(),0,5)=='ctrl_')
            require_once(ROOT_PATH.'includes/'.$fileinfo->getFilename());
    }
}



/* 对用户传入的变量进行转义操作。*/
if (!get_magic_quotes_gpc())
{
    if (!empty($_GET))
    {
        $_GET  = addslashes_deep($_GET);
    }
    if (!empty($_POST))
    {
        $_POST = addslashes_deep($_POST);
    }

    $_COOKIE   = addslashes_deep($_COOKIE);
    $_REQUEST  = addslashes_deep($_REQUEST);
}

/* 创建 ECSHOP 对象 */
$ecs = new ECS($db_name, $prefix);
define('DATA_DIR', $ecs->data_dir());
define('IMAGE_DIR', $ecs->image_dir());


/* 初始化数据库类 */
require(ROOT_PATH . 'includes/cls_mysql.php');
$db = new cls_mysql($db_host, $db_user, $db_pass, $db_name);
$db->set_disable_cache_tables(array($ecs->table('sessions'), $ecs->table('sessions_data'), $ecs->table('cart'), $ecs->table('connections')));
$db_host = $db_user = $db_pass = $db_name = NULL;

/* 创建错误处理对象 */
$err = new ecs_error('message.dwt');

/* 载入系统参数 */
$_CFG = load_config();

$memcache = new \Memcached();
$memcache->addServer('localhost',11211);
/* 载入语言文件 */
require(ROOT_PATH . 'languages/' . $_CFG['lang'] . '/common.php');
require(ROOT_PATH . 'languages/' . $_CFG['lang'] . '/yoho.php');

if ($_CFG['shop_closed'] == 1)
{
    /* 商店关闭了，输出关闭的消息 */
    header('Content-type: text/html; charset='.EC_CHARSET);

    die('<div style="margin: 150px; text-align: center; font-size: 14px"><p>' . $_LANG['shop_closed'] . '</p><p>' . $_CFG['close_comment'] . '</p></div>');
}
$is_spider = is_spider();
if ($is_spider)
{
    /* 如果是蜘蛛的访问，那么默认为访客方式，并且不记录到日志中 */
    if (!defined('INIT_NO_USERS'))
    {
        define('INIT_NO_USERS', true);
        /* 整合UC后，如果是蜘蛛访问，初始化UC需要的常量 */
        if($_CFG['integrate_code'] == 'ucenter')
        {
             $user = & init_users();
        }
    }
    $_SESSION = array();
    $_SESSION['user_id']     = 0;
    $_SESSION['user_name']   = '';
    $_SESSION['email']       = '';
    $_SESSION['user_rank']   = 0;
    $_SESSION['discount']    = 1.00;
    /* 设置cookie */
    $integrate_config = unserialize($GLOBALS['_CFG']['integrate_config']);
	$cookie_domain = empty($integrate_config['cookie_domain']) ? $GLOBALS['ecs']->get_host() : $integrate_config['cookie_domain'];
	$cookie_path = empty($integrate_config['cookie_path']) ? '/' : $integrate_config['cookie_path'];
	$expire = time() + 86400; 
    setcookie('is_spider', "1", $expire, $cookie_path, $cookie_domain);
}
$gotoerror = false;
if (!defined('INIT_NO_USERS'))
{
    /* 初始化session */
    include(ROOT_PATH . 'includes/cls_session.php');
    $controller    = new Yoho\cms\Controller\YohoBaseController();
    $gotoerror = $controller->onJail();
    if(!$gotoerror) {
        $sess = new cls_session($db, $ecs->table('sessions'), $ecs->table('sessions_data'));
        $controller->ipConnection();
        $session_id = $sess->get_session_id();
        if($session_id == false) {
            $gotoerror = true;
        } else {
            define('SESS_ID', $session_id);
        }
    }
}
if(isset($_SERVER['PHP_SELF']))
{
    $_SERVER['PHP_SELF']=htmlspecialchars($_SERVER['PHP_SELF']);
}

if($gotoerror && !$is_spider) {
    if (!empty($_CFG['queue_redirect_link'])) {
        header('HTTP/1.1 302');
        header('Location: '.$_CFG['queue_redirect_link']);
    } else {
        header("HTTP/1.1 503 Service Temporarily Unavailable");
        include(ROOT_PATH . 'error/503.html');
        die();
    }
    exit;
}

if (!defined('INIT_NO_SMARTY'))
{
    header('Cache-control: private');
    header('Content-type: text/html; charset='.EC_CHARSET);

    /* 创建 Smarty 对象。*/
    require(ROOT_PATH . 'core/cls_template.php');
    $smarty = new cls_template;

    $smarty->cache_lifetime = $_CFG['cache_time'];
    $smarty->template_dir   = ROOT_PATH . 'themes/' . $_CFG['template'];
    $smarty->cache_dir      = ROOT_PATH . 'temp/caches';
    $smarty->compile_dir    = ROOT_PATH . 'temp/compiled';

    if ((DEBUG_MODE & 2) == 2)
    {
        $smarty->direct_output = true;
        $smarty->force_compile = true;
    }
    else
    {
        $smarty->direct_output = false;
        $smarty->force_compile = false;
    }

    $smarty->assign('lang', $_LANG);
    $smarty->assign('ecs_charset', EC_CHARSET);
    $smarty->assign('lang_code', $_CFG['lang']);
    $alertController    = new Yoho\cms\Controller\AlertController();
    $alert = $alertController->getAlert();
    $smarty->assign('page_alert', $alert);

    if (!empty($_CFG['stylename']))
    {
        $smarty->assign('ecs_css_path', 'themes/' . $_CFG['template'] . '/style_' . $_CFG['stylename'] . '.css');
    }
    else
    {
        $smarty->assign('ecs_css_path', 'themes/' . $_CFG['template'] . '/style.css');
    }
}

if (!defined('INIT_NO_USERS'))
{
    /* 会员信息 */
    $user =& init_users();

    if (!isset($_SESSION['user_id']))
    {
        /* 获取投放站点的名称 */
        $site_name = isset($_GET['from'])   ? htmlspecialchars($_GET['from']) : addslashes($_LANG['self_site']);
        $from_ad   = !empty($_GET['ad_id']) ? intval($_GET['ad_id']) : 0;

        $_SESSION['from_ad'] = $from_ad; // 用户点击的广告ID
        $_SESSION['referer'] = stripslashes($site_name); // 用户来源

        unset($site_name);

        if (!defined('INGORE_VISIT_STATS'))
        {
            visit_stats();
        }
    }
    
    if (empty($_SESSION['user_id']))
    {
        // Session does not exists, or user not logged in
        if ($user->get_cookie()) // For "ecshop" integrate mode, this always return false
        {
            /* 如果会员已经登录并且还没有获得会员的帐户余额、积分以及优惠券 */
            if ($_SESSION['user_id'] > 0)
            {
                update_user_info();
            }
        }
        else
        {
            $_SESSION['user_id']     = 0;
            $_SESSION['user_name']   = '';
            $_SESSION['email']       = '';
            $_SESSION['user_rank']   = 0;
            $_SESSION['discount']    = 1.00;
            if (!isset($_SESSION['login_fail']))
            {
                $_SESSION['login_fail'] = 0;
            }
            $flashdealController = new \Yoho\cms\Controller\FlashdealController();
            $flashdealController->updateFlashdealCartByECSCart();
        }
    }
    else
    {
        // Session already exists, and user is logged in
        if (!defined('INIT_NO_UPDATE_USER_INFO')) // Added by howang
        {
            // Originally, there's a bug in the if condition for "session 不存在，检查cookie" part, which cause the
            // cookie is always checked, and have update_user_info() called every times. Now the if condition is
            // fixed, and we make it skippable with a constant "INIT_NO_UPDATE_USER_INFO" so we can avoid those
            // expensive SQL queries in update_user_info() in some page which don't need it, such as AJAX pages
            update_user_info();
        }
    }
    
    /* 设置推荐会员 */
    if (isset($_GET['u']))
    {
        set_affiliate();
    }

    /* Set promote code */
    if (isset($_GET['promote'])) {
        set_promote();
    }
    
    /* session 不存在，检查cookie */
    if (empty($_SESSION['user_id']) && !empty($_COOKIE['ECS']['user_id']) && !empty($_COOKIE['ECS']['password']))
    {
        // 找到了cookie, 验证cookie信息
        $sql = 'SELECT user_id, user_name, password ' .
                ' FROM ' .$ecs->table('users') .
                " WHERE user_id = '" . intval($_COOKIE['ECS']['user_id']) . "' AND password = '" .$_COOKIE['ECS']['password']. "'";

        $row = $db->GetRow($sql);

        if (!$row)
        {
            // 没有找到这个记录
           $time = time() - 3600;
           setcookie("ECS[user_id]",  '', $time, '/');
           setcookie("ECS[password]", '', $time, '/');
        }
        else
        {
            $_SESSION['user_id'] = $row['user_id'];
            $_SESSION['user_name'] = $row['user_name'];
            update_user_info();
        }
    }

    if (isset($smarty))
    {
        $smarty->assign('ecs_session', $_SESSION);
        // Affiliate
        $is_affiliate = empty($_SESSION['user_id']) ? '' : $db->getOne("SELECT is_affiliate FROM " . $ecs->table('users') . " WHERE user_id = '" . $_SESSION['user_id'] . "'");
        $smarty->assign('is_affiliate', $is_affiliate);
        $affiliate = unserialize($GLOBALS['_CFG']['affiliate']);
        $smarty->assign('affiliate', $affiliate);
        $area = user_area();
        $smarty->assign('user_area', $area);
        $is_on_sale = $memcache->get('is_on_sale_total');
        if($is_on_sale === false) {
            $sql = 'SELECT count(*) ' .
                ' FROM ' .$ecs->table('goods') .
                " WHERE is_on_sale = 1 ";

            $is_on_sale = $db->GetOne($sql);
            $memcache->set('is_on_sale_total', $is_on_sale);
        }

        $is_on_sale = number_format(intval($is_on_sale / 1000) * 1000, 0);
        $smarty->assign('is_on_sale', $is_on_sale);

        // check if the testing user
        $baseController = new Yoho\cms\Controller\YohoBaseController();
        $is_testing = $baseController->is_testing();
        $smarty->assign('is_testing', $is_testing);
    }
}

/* Add black list */
$flashdealController = new Yoho\cms\Controller\FlashdealController();
$flashdealController->blacklist();
// if (isset($smarty))
// {
//     $luckyDrawController = new Yoho\cms\Controller\LuckyDrawController();
//     $is_lucky_draw_active = $luckyDrawController->isActiveEventShowOnMenu();
//     $smarty->assign('is_lucky_draw_active', $is_lucky_draw_active);
// }

//handle redirect_to 
if (!empty($_REQUEST['redirect_to'])) {
    $_SESSION['redirect_to'] = $_REQUEST['redirect_to'];
}

if ((DEBUG_MODE & 1) == 1)
{
    error_reporting(E_ALL);
}
else
{
    error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING & ~E_STRICT & ~E_DEPRECATED);
}
if ((DEBUG_MODE & 4) == 4)
{
    include(ROOT_PATH . 'includes/lib.debug.php');
}

/* 判断是否支持 Gzip 模式 */
if (!defined('INIT_NO_SMARTY') && gzip_enabled())
{
    ob_start('ob_gzhandler');
}
else
{
    ob_start();
}

if (!defined('INIT_NO_SMARTY'))
{
    /* Ecodemall */
    @include(ROOT_PATH.'themes/'.$_CFG['template'].'/functions.php');
}
?>