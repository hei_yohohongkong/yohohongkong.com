<?php

/**
 * ECSHOP 动态内容函数库
 * ============================================================================
 * * 版权所有 2005-2012 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liubo $
 * $Id: lib_insert.php 17217 2011-01-19 06:29:08Z liubo $
*/

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

/**
 * 获得查询次数以及查询时间
 *
 * @access  public
 * @return  string
 */
function insert_query_info()
{
    if ($GLOBALS['db']->queryTime == '')
    {
        $query_time = 0;
    }
    else
    {
        if (PHP_VERSION >= '5.0.0')
        {
            $query_time = number_format(microtime(true) - $GLOBALS['db']->queryTime, 6);
        }
        else
        {
            list($now_usec, $now_sec)     = explode(' ', microtime());
            list($start_usec, $start_sec) = explode(' ', $GLOBALS['db']->queryTime);
            $query_time = number_format(($now_sec - $start_sec) + ($now_usec - $start_usec), 6);
        }
    }

    /* 内存占用情况 */
    if ($GLOBALS['_LANG']['memory_info'] && function_exists('memory_get_peak_usage'))
    {
        $memory_usage = sprintf($GLOBALS['_LANG']['memory_info'], memory_get_peak_usage() / 1048576);
    }
    else
    {
        $memory_usage = '';
    }

    /* 是否启用了 gzip */
    $gzip_enabled = gzip_enabled() ? $GLOBALS['_LANG']['gzip_enabled'] : $GLOBALS['_LANG']['gzip_disabled'];

    $online_count = $GLOBALS['db']->getOne("SELECT COUNT(*) FROM " . $GLOBALS['ecs']->table('sessions'));

    /* 加入触发cron代码 */
    $cron_method = empty($GLOBALS['_CFG']['cron_method']) ? '<img src="api/cron.php?t=' . gmtime() . '" alt="" style="width:0px;height:0px;" />' : '';

    return sprintf($GLOBALS['_LANG']['query_info'], $GLOBALS['db']->queryCount, $query_time, $online_count) . $gzip_enabled . $memory_usage . $cron_method;
}

/**
 * 调用浏览历史  //ly sbsn 0302 修改输出样式
 *
 * @access  public
 * @return  string
 */
function insert_history()
{
    $str = '';

	$stra=' <div class="panel_box"> <div class="h_panel"> ';
	$strb='  </div> </div> ';
    if (!empty($_COOKIE['ECS']['history']))
    {
        $where = db_create_in($_COOKIE['ECS']['history'], 'goods_id');
        $sql   = 'SELECT goods_id, goods_name, goods_thumb, shop_price FROM ' . $GLOBALS['ecs']->table('goods') .
                " WHERE $where AND is_on_sale = 1 AND is_alone_sale = 1 AND is_delete = 0 order by INSTR('".$_COOKIE['ECS']['history']."',goods_id)";
        $query = $GLOBALS['db']->query($sql);
        $res = array();
		$i=0;
        while ($row = $GLOBALS['db']->fetch_array($query))
        {
			$i++;
            $goods['goods_id'] = $row['goods_id'];
            $goods['goods_name'] = $row['goods_name'];
            $goods['short_name'] = $GLOBALS['_CFG']['goods_name_length'] > 0 ? sub_str($row['goods_name'], $GLOBALS['_CFG']['goods_name_length']) : $row['goods_name'];
            $goods['goods_thumb'] = get_image_path($row['goods_id'], $row['goods_thumb'], true);
            $goods['shop_price'] = price_format($row['shop_price']);
            $goods['url'] = build_uri('goods', array('gid'=>$row['goods_id']), $row['goods_name']);
            $str.=' <dl><dt><a href="'.$goods['url'].'" target="_blank"><img src="/'.$goods['goods_thumb'].'" alt="'.$goods['goods_name'].'" /></a>
</dt> <dd><p><span class="pink">'.$goods['shop_price'].'</span></p><p><a href="'.$goods['url'].'" target="_blank" title="'.$goods['goods_name'].'">'.$goods['short_name'].'</a></p></dd></dl>';
			if($i%6==0) {$str.="</div> <div class='h_panel'>"    ; }
        }
    }
    else
    {
	   $stra="";
	   $strb="";
	   $str="   <div class=empty_mod> <div class=empty_img></div><p>您還沒有瀏覽過產品哦</p></div>";
    }
    $str=$stra.$str.$strb;

    return $str;
}

/**
 * 購物車中的產品數量
 * @access  public
 * @return  string
 */
function insert_cart_info()
{
    //static $number = null;

    //if ($number === null)
    //{
        $sql = 'SELECT SUM(goods_number) AS number' .
               ' FROM ' . $GLOBALS['ecs']->table('cart') .
               " WHERE session_id = '" . SESS_ID . "' AND rec_type = '" . CART_GENERAL_GOODS . "'";
        $number = intval($GLOBALS['db']->GetOne($sql));
    //}
    return ($number > 0) ? $number : 0;
}
function insert_header_cart_count($arr)
{
    $count = '<em id="hd-cartnum'.$arr['prefix'].'" class="red hd-cartnum">' . insert_cart_info() . '</em>';
    return sprintf(_L('global_header_menu_cart_count', '%s件'), $count);
}

/**
* 購物車中的產品總值
* @access  public
* @return  string
*/
function insert_cart_infoamount()
{
    $sql = 'SELECT SUM(goods_price * goods_number) AS amount' .
           ' FROM ' . $GLOBALS['ecs']->table('cart') .
           " WHERE session_id = '" . SESS_ID . "' AND rec_type = '" . CART_GENERAL_GOODS . "'";
    $amount = floatval($GLOBALS['db']->GetOne($sql));
    return ($amount > 0) ? $amount : 0;
}

/**
 * 增加购物车產品列表输出
 *    //ly sbsn 0302 修改输出样式
 * @access  public
 * @return  string
 */
function insert_cart_info2()
{
    $packageController = new Yoho\cms\Controller\PackageController();
    $a='<div class="panel_tit"> ' . sprintf(_L('global_cart_popup_title_total_item', '共<span class="popup-cart-num">%s</span>件產品'), insert_cart_info()) . ' </div> <div class="panel_con">';

    $b='<div class="cart-bottom-amount-gocart">
        <div class="cart-amount"><p>' . sprintf(_L('global_cart_popup_total_amount', '合計：<span id="panel-cartnum">%s</span>'), 'HK$<span id="panel-price">'.insert_cart_infoamount().'</span>') . '</p></div>
        <div class="cart-gocart"><div class="gocart"><a href="/cart" class="btn">' . _L('global_cart_popup_go_cart_btn', '去購物車結算') . '</a></div></div>
        </div>
        </div>';

    $str = '';

    $sql = "SELECT c.goods_id, c.goods_name, c.goods_price, c.rec_id, c.goods_number as num, c.is_gift, c.is_package, g.goods_thumb, gperma " .
            "FROM " . $GLOBALS['ecs']->table('cart') . " as c " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON c.goods_id = g.goods_id " .
            "LEFT JOIN (SELECT id, table_name, lang, perma_link as gperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") gpl ON gpl.id = g.goods_id  AND gpl.table_name = 'goods' AND gpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
            "WHERE c.session_id = '" . SESS_ID . "' " .
            "AND c.rec_type = '" . CART_GENERAL_GOODS . "' " .
            "ORDER BY rec_id DESC";
    $query = $GLOBALS['db']->query($sql);
    $i = 0;
    $packages = [];
    while ($row = $GLOBALS['db']->fetch_array($query))
    {
        $i++;
        if ($row['is_package']) {
            if (in_array($row['is_package'], array_keys($packages))) continue;
            $packages[$row['is_package']] = $packageController->get_basic_package_info($row['is_package']);
            $packages[$row['is_package']]['qty'] = $row['num'] / $packages[$row['is_package']]['items'][$row['goods_id']]['goods_number'];
            $packages[$row['is_package']]['total_price'] = bcmul($packages[$row['is_package']]['qty'], $packages[$row['is_package']]['base_price'], 2);
            $packages[$row['is_package']]['formated_total_price'] = price_format($packages[$row['is_package']]['total_price'], false);
            $packages[$row['is_package']]['rec_id'] = $row['rec_id'];
            continue;
        }
        $goods['rec_id'] = $row['rec_id'];
        $goods['goods_id'] = $row['goods_id'];
        $goods['goods_name'] = $row['goods_name'];
        $goods['goods_thumb'] = get_image_path($row['goods_id'], $row['goods_thumb'], true);
        $goods['shop_price'] = price_format($row['goods_price']);
        $goods['num']=$row['num'];
        $goods['is_gift']=$row['is_gift'];
        $goods['is_package']=$row['is_package'];
        $goods['url'] = build_uri('goods', array('gid'=>$row['goods_id'], 'gperma' => $row['gperma']), $row['goods_name']);
        
        $goods['limit_number'] = 0;
        $str.= '
        <div class="good-row">
            <div class="good-img"><a href="'.$goods['url'].'" target="_blank"><img src="/'.$goods['goods_thumb'].'" alt="'.$goods['goods_name'].'" /></a></div>
            <div class="good-text">
                <div class="good-title"><a href="'.$goods['url'].'" target="_blank" title="'.$goods['goods_name'].'">'.$goods['goods_name'].'</a></div>
                <div class="good-price"><span class="good-unit-price">'.$goods['shop_price'].'</span>&nbsp;X&nbsp;<span class="good-unit-count">'.$goods['num'].'</span></div>
            </div>
            <div class="good-action">
                <div class="adjust-item">
        ';

        if(!$goods['is_gift'] && !$goods['is_package']){
            $str .= '<div class="nums" data-id="'.$goods['rec_id'].'">
                        <span class="minus" title="' . _L('cart_quantity_minus_one', '減少1個2') . '">-</span>
                        <input type="text" data-limit="20" value="'.$goods['num'].'">
                        <span class="add" title="' . _L('cart_quantity_add_one', '增加1個') . '">+</span>
                    </div>
            ';
        }
        $str.= '</div>';
        if(!$goods['is_gift'] && !$goods['is_package']){
            $str.= '<div class="del-item"><a class="del_btn" href="javascript:void(0);" data-id="'.$goods['rec_id'].'">' . _L('global_cart_popup_remove_btn', '刪除') . '</a></div>';
        }
        $str.= '</div>
        </div>';

        if($i == 5)
        {
            break;
        }
    }
    if (!empty($packages)) {
        foreach ($packages as $package) {
            $str .= '
            <div class="good-row">
                <div class="good-img"><a href="'.$package['link'].'" target="_blank"><img src="/'.$package['images'][0].'" alt="'.$package['package_name'].'" /></a></div>
                <div class="good-text">
                    <div class="good-title"><a href="'.$package['link'].'" target="_blank" title="'.$package['package_name'].'">'.$package['package_name'].'</a></div>
                    <div class="good-price"><span class="good-unit-price">'.$package['formated_base_price'].'</span>&nbsp;X&nbsp;<span class="good-unit-count">'.$package['qty'].'</span></div>
                </div>
                <div class="good-action">' .
                    // '<div class="adjust-item">
                    //     <div class="nums" data-id="'.$package['rec_id'].'">
                    //         // <span class="minus" title="' . _L('cart_quantity_minus_one', '減少1個') . '">-</span>
                    //         <input type="text" data-limit="20" value="'.$package['qty'].'">
                    //         <span class="add" title="' . _L('cart_quantity_add_one', '增加1個') . '">+</span>
                    //     </div>
                    // </div>' .
                    '<div class="del-item"><a class="del_btn" href="javascript:void(0);" data-id="'.$package['rec_id'].'">' . _L('global_cart_popup_remove_btn', '刪除') . '</a></div>
                </div>
            </div>';
        }
    }
    if ($i == 0)
    {
        $a='<div class="panel_tit panel_tit_empty"> ' . _L('global_cart_popup_title', '購物車') . ' </div>';
        $b='<div class="empty_mod">
            <div class="empty_img">
            <svg class="empty-cart-icon" alt="' . _L('global_cart_popup_nothing', '暫時沒有任何項目') . '" title="' . _L('global_cart_popup_nothing', '暫時沒有任何項目') . '">
                <use xlink:href="/yohohk/img/yoho_svg.svg#empty_cart"></use>
            </svg>
            </div>
            <div class="empty-text">' . _L('global_cart_popup_nothing', '暫時沒有任何項目') . '</div>
            <a href="/search.php" class="btn empty-btn">' . _L('global_cart_popup_buy_something', '不如買些東西吧！') . '</a>
        </div>';
    }
    $str = $a.$str.$b;
    return $str;
}

/**
 * 调用指定的广告位的广告
 *
 * @access  public
 * @param   integer $id     广告位ID
 * @param   integer $num    广告数量
 * @return  string
 */
function insert_ads($arr)
{
    static $static_res = NULL;
    // Fix ECShop全系列版本的远程代码执行漏洞: https://paper.seebug.org/695/
    $arr['num'] = intval($arr['num']);
    $arr['id']  = intval($arr['id']);
    $time = gmtime();
    $area = user_area();
    $language = $GLOBALS['_CFG']['lang'];

    $catlist_search = "";
    if($arr['catlist']){
        $catlist = array();
        foreach($arr['catlist'] as $cat){
            $catlist[] = ",".$cat.",";
        }
        $catlist = implode("|", $catlist);
        $catlist_search = "AND (a.cat = '' OR a.cat is null OR CONCAT(',', a.cat, ',') REGEXP '".$catlist."') ";
    }
    else{
        $catlist_search = "AND (a.cat = '' OR a.cat is null) ";
    }

    $cookie_list = array_keys($_COOKIE);
    $ad_show_once_list = preg_grep('/ad_show_once_(\w+)/', $cookie_list);
    if(!empty($ad_show_once_list)){
        $ad_show = array();
        foreach($ad_show_once_list as $ad_show_once){
            $ad_id = substr($ad_show_once, strrpos($ad_show_once, '_') + 1);
            $ad_show[] = $ad_id;
        }
        $ad_not_show_search = "AND (a.ad_id NOT IN (".implode(",", $ad_show).")) ";
        //echo $ad_not_show_search;
    }


    if (!empty($arr['num']) && $arr['num'] != 1)
    {
        $sql  = "SELECT a.ad_id, a.position_id, a.media_type, a.ad_link, a.ad_code, a.ad_name, a.ad_title, p.ad_width, p.ad_height, p.position_style, p.display_once, " .
                "(-LOG(RAND()) / (CASE WHEN a.area = '' THEN 1 ELSE 3 END)) AS rnd " . // area-specific ads got higher chance to display
                "FROM " . $GLOBALS['ecs']->table('ad') . " AS a ".
                //"LEFT JOIN " . $GLOBALS['ecs']->table('ad_position') . " AS p ON a.position_id = p.position_id " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('ad_position') . " AS p ON '".$arr['id']."' = p.position_id " .
                //"LEFT JOIN " . $GLOBALS['ecs']->table('ad_position') . " AS p ON CONCAT(',', a.position_id, ',') LIKE '%,p.position_id,%' " .
                "WHERE enabled = 1 AND start_time <= '" . $time . "' AND end_time >= '" . $time . "' ".
                    //"AND a.position_id = '" . $arr['id'] . "' " .
                    "AND CONCAT(',', a.position_id, ',') LIKE '%," . mysql_like_quote($arr['id']) . ",%' " .
                    "AND (a.area = '' OR a.area LIKE '%" . mysql_like_quote($area) . "%') " .
                    "AND (a.area_exclude = '' OR a.area_exclude NOT LIKE '%" . mysql_like_quote($area) . "%') " .
                    "AND (a.language = '' OR a.language LIKE '%" . mysql_like_quote($language) . "%') " .
                    "AND (a.language_exclude = '' OR a.language_exclude NOT LIKE '%" . mysql_like_quote($language) . "%') " .
                    $catlist_search .
                    $ad_not_show_search .
                "ORDER BY a.cat desc, rnd LIMIT " . $arr['num'];
        $res = $GLOBALS['db']->GetAll($sql);
    }
    else
    {
        if ($static_res[$arr['id']] === NULL)
        {
            $sql  = "SELECT a.ad_id, a.position_id, a.media_type, a.ad_link, a.ad_code, a.ad_name, a.ad_title, p.ad_width, p.ad_height, p.position_style, p.display_once, " .
                    "(-LOG(RAND()) / (CASE WHEN a.area = '' THEN 1 ELSE 3 END)) AS rnd " . // area-specific ads got higher chance to display
                    "FROM " . $GLOBALS['ecs']->table('ad') . " AS a ".
                    //"LEFT JOIN " . $GLOBALS['ecs']->table('ad_position') . " AS p ON a.position_id = p.position_id " .
                    "LEFT JOIN " . $GLOBALS['ecs']->table('ad_position') . " AS p ON '".$arr['id']."' = p.position_id " .
                    //"LEFT JOIN " . $GLOBALS['ecs']->table('ad_position') . " AS p ON CONCAT(',', a.position_id, ',') LIKE '%,p.position_id,%' " .
                    "WHERE enabled = 1 AND start_time <= '" . $time . "' AND end_time >= '" . $time . "' ".
                         //"AND a.position_id = '" . $arr['id'] . "' " .
                        "AND CONCAT(',', a.position_id, ',') LIKE '%," . mysql_like_quote($arr['id']) . ",%' " .
                        "AND (a.area = '' OR a.area LIKE '%" . mysql_like_quote($area) . "%') " .
                        "AND (a.area_exclude = '' OR a.area_exclude NOT LIKE '%" . mysql_like_quote($area) . "%') " .
                        "AND (a.language = '' OR a.language LIKE '%" . mysql_like_quote($language) . "%') " .
                        "AND (a.language_exclude = '' OR a.language_exclude NOT LIKE '%" . mysql_like_quote($language) . "%') " .
                        $catlist_search .
                        $ad_not_show_search .
                    "ORDER BY a.cat desc, rnd LIMIT 1";
            $static_res[$arr['id']] = $GLOBALS['db']->GetAll($sql);
        }
        $res = $static_res[$arr['id']];
    }
    //echo $sql."<br><br>";
    $ads = array();
    $position_style = '';

    foreach ($res AS $ad_key => $row)
    {
        //if ($row['position_id'] != $arr['id'])
        $position_id_list = explode(",", $row['position_id']);
        if(!in_array($arr['id'], $position_id_list))
        {
            continue;
        }
        $position_style = $row['position_style'];
        $display = " style='display:block;' ";
        /* Do if this position is only display once */
        if($row['display_once'])
        {   
            //$key = 'ad_show_once_'.$row['position_id'].'_'.$row['ad_id'];
            $key = 'ad_show_once_'.$arr['id'].'_'.$row['ad_id'];
            if (!isset($_COOKIE[$key]))
            {
                setcookie($key, true,  time()+86400); // 1 day
                $position_style = $row['position_style'];
                //echo "set cookie " . $key."<br>";
            } else {
                $display = " style='display:none;' ";
                $position_style = '';
                continue;
            }
            
        }
        switch ($row['media_type'])
        {
            case 0: // 图片广告
                $src = (strpos($row['ad_code'], 'http://') === false && strpos($row['ad_code'], 'https://') === false) ?
                        "/" . DATA_DIR . "/afficheimg/$row[ad_code]" : $row['ad_code'];
                if(!empty($row["ad_link"]))
                {
                    $ads[] = '<a href="/affiche.php?ad_id='.$row[ad_id].'&amp;uri=' .urlencode($row["ad_link"]). '" target="_blank"'.$display.'><img src="'.$src.'" '.(empty($row["ad_title"])? '': 'title="'.$row["ad_title"].'"')." ".(empty($row["ad_title"])? '': 'alt="'.$row["ad_title"].'"').' width="' .$row['ad_width']. '" style="width:'.$row['ad_width'].'px;" border="0"  /></a>';
                } else {
                    $ads[] = '<img src="'.$src.'"'.(empty($row["ad_title"])? '': 'title="'.$row["ad_title"].'"').' '.(empty($row["ad_title"])? '': 'alt="'.$row["ad_title"].'"').' width="' . $row["ad_width"]. '" style="width:'. $row["ad_width"].'px;" border="0"  />';
                }
                break;
            case 1: // Flash
                $src = (strpos($row['ad_code'], 'http://') === false && strpos($row['ad_code'], 'https://') === false) ?
                        DATA_DIR . "/afficheimg/$row[ad_code]" : $row['ad_code'];
                $ads[] = "<object classid=\"clsid:d27cdb6e-ae6d-11cf-96b8-444553540000\" " .
                         "codebase=\"http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0\"  " .
                           "width='$row[ad_width]' height='$row[ad_height]' .$display.>
                           <param name='movie' value='$src'>
                           <param name='quality' value='high'>
                           <embed src='$src' quality='high'
                           pluginspage='http://www.macromedia.com/go/getflashplayer'
                           type='application/x-shockwave-flash' width='$row[ad_width]'
                           height='$row[ad_height]'></embed>
                         </object>";
                break;
            case 2: // CODE
                $ads[] = $row['ad_code'];
                break;
            case 3: // TEXT
                if(!empty($row["ad_link"]))
                {
                    $ads[] = "<a href='/affiche.php?ad_id=$row[ad_id]&amp;uri=" .urlencode($row["ad_link"]). "'
                    target='_blank' .$display.>" .htmlspecialchars($row['ad_code']). '</a>';
                } else {
                    $ads[] = "" .htmlspecialchars($row['ad_code']). "";
                }
                break;
        }
    }

    $position_style = 'str:' . $position_style;

    $need_cache = $GLOBALS['smarty']->caching;
    $GLOBALS['smarty']->caching = false;

    $GLOBALS['smarty']->assign('ads', $ads);
    $val = $GLOBALS['smarty']->fetch($position_style);

    $GLOBALS['smarty']->caching = $need_cache;

    return $val;
}

/**
* 调用会员信息
*
* @access  public
* @return  string
*/
function insert_member_info()
{
    $need_cache = $GLOBALS['smarty']->caching;
    $GLOBALS['smarty']->caching = false;

    if ($_SESSION['user_id'] > 0)
    {
        $GLOBALS['smarty']->assign('user_info', get_user_info());
    }
    else
    {
        if (!empty($_COOKIE['ECS']['username']))
        {
            $GLOBALS['smarty']->assign('ecs_username', stripslashes($_COOKIE['ECS']['username']));
        }
        $captcha = intval($GLOBALS['_CFG']['captcha']);
        if (($captcha & CAPTCHA_LOGIN) && (!($captcha & CAPTCHA_LOGIN_FAIL) || (($captcha & CAPTCHA_LOGIN_FAIL) && $_SESSION['login_fail'] > 2)) && gd_version() > 0)
        {
            $GLOBALS['smarty']->assign('enabled_captcha', 1);
            $GLOBALS['smarty']->assign('rand', mt_rand());
        }
    }
    $output = $GLOBALS['smarty']->fetch('library/member_info.lbi');

    $GLOBALS['smarty']->caching = $need_cache;

    return $output;
}

/**
* 调用会员信息 JSON by howang
*
* @access  public
* @return  string
*/
function insert_member_info3()
{
    $info = array();

    $user_info = ($_SESSION['user_id'] > 0) ? get_user_info() : false;

    if ($user_info)
    {
        $info['login'] = 1;
        $info['uid'] = $user_info['user_id'];
        $info['name'] = $user_info['username'];
        $info['username'] = insert_user_name();
        $info['level'] = intval($_SESSION['user_rank']);
        $info['jifen'] = intval($user_info['pay_points']);
        $info['money'] = $user_info['user_money'];
        $info['trade'] = intval($user_info['ordernumber']);
    }
    else
    {
        $info['login'] = 0;
        $info['trade'] = 0;
        $info['countryList'] = available_login_countries();
    }

    $info['coupon'] = 0;
    $info['message'] = 0;
    $info['cart'] = insert_cart_info();
    $info['area'] = user_area();

    return json_encode($info);
}

function insert_user_name()
{
    global $db, $ecs;

    static $user_name = null;
    if ($user_name === null)
    {
        if ($_SESSION['user_id'])
        {
            $sql = "SELECT `content` as `display_name` " .
                    "FROM " . $ecs->table('reg_extend_info') .
                    " WHERE `reg_field_id` = 10 AND `user_id` = '" . $_SESSION['user_id'] ."'";
            $display_name = $db->getOne($sql);
            $user_name = empty($display_name) ? $_SESSION['user_name'] : $display_name;
        }
        else
        {
            $user_name = '';
        }
    }

    return $user_name;
}

function insert_page_head_login_btn($arr)
{
    $affiliate_on = $arr['aff_on'];
    $type = intval($arr['type']);
    $img = '<svg class="top-bar-icon">
                <use xlink:href="/yohohk/img/yoho_svg.svg#login"></use>
			</svg>' ;
    if ($_SESSION['user_id'])
    {
        return '
            <a href="/user.php" rel="nofollow" class="menu-link">
                '.$img.'<span class="user-name logged-in">' . _L('global_header_menu_user_name_message', '歡迎，') . insert_user_name() . '</span><div class="arrow-left"></div>
            </a>';
    }
    else
    {   
        if($affiliate_on == 1){
            return '          
            <a href="javascript:YOHO.dialog.showLogin(1);" rel="nofollow" class="menu-link">  
            '.$img.'<span class="user-name">' . _L('global_header_menu_user_login_register', '登入/註冊') . '</span>
            </a>';
        }else{
            return '          
            <a href="javascript:YOHO.dialog.showLogin(0);" rel="nofollow" class="menu-link">  
            '.$img.'<span class="user-name">' . _L('global_header_menu_user_login_register', '登入/註冊') . '</span>
            </a>';
        }
  
        
    }
}

function insert_page_head_nav_test_link($arr)
{
    global $is_testing;

    if ($is_testing)
    {
        if ($arr['mode'] == 'm') {
            return '<a class="list-group-item list-icon list-icon-promotions" href="/cat/405-payme">PayMe 測試</a>';
        } else if ($arr['mode'] == 'd') {
            return '<li  class=""><a href="/cat/405-payme">PayMe 測試</a></li>';
        } else {
            return '<li  class=""><a href="/cat/405-payme">PayMe 測試</a></li>';
        }
    }
}

/**
 * 调用评论信息
 *
 * @access  public
 * @return  string
 */
function insert_comments($arr)
{
    global $_CFG;
    // Fix ECShop全系列版本的远程代码执行漏洞: https://paper.seebug.org/695/
    $arr['num'] = intval($arr['num']);
    $arr['id']  = intval($arr['id']);
    $arr['type']  = intval($arr['type']);
    $commentController = new Yoho\cms\Controller\CommentController();
    $need_cache = $GLOBALS['smarty']->caching;
    $need_compile = $GLOBALS['smarty']->force_compile;

    $GLOBALS['smarty']->caching = false;
    $GLOBALS['smarty']->force_compile = true;
    $can_comment = $commentController->user_can_comment($commentController::COMMENT_TYPE_GOODS, $arr['id']);
    /* 验证码相关设置 */
    if ((intval($GLOBALS['_CFG']['captcha']) & CAPTCHA_COMMENT) && gd_version() > 0)
    {
        $GLOBALS['smarty']->assign('enabled_captcha', 1);
        $GLOBALS['smarty']->assign('rand', mt_rand());
    }
    $GLOBALS['smarty']->assign('username',     stripslashes($_SESSION['user_name']));
    $GLOBALS['smarty']->assign('email',        $_SESSION['email']);
    $GLOBALS['smarty']->assign('comment_type', $arr['type']);
    $GLOBALS['smarty']->assign('id',           $arr['id']);
    $cmt = $commentController->assign_comment($arr['id'],$arr['type'],'1',$arr['lei']);
    // comment
    if($_CFG['comment_open'] == 0) {
        $GLOBALS['smarty']->assign('can_comment', 0);
        $GLOBALS['smarty']->assign('comments',    0);
    } else {
        $GLOBALS['smarty']->assign('can_comment', $can_comment['can_comment']);
        $GLOBALS['smarty']->assign('comments',     $cmt['comments']);
    }
    $GLOBALS['smarty']->assign('count',     $cmt['total']);
    $GLOBALS['smarty']->assign('pager',        $cmt['pager']);
    $GLOBALS['smarty']->assign('lei',        $arr['lei']);
    $val = $GLOBALS['smarty']->fetch('library/comments_list.lbi');

    $GLOBALS['smarty']->caching = $need_cache;
    $GLOBALS['smarty']->force_compile = $need_compile;

    return $val;
}


/**
 * 调用產品购买记录
 *
 * @access  public
 * @return  string
 */
function insert_bought_notes($arr)
{
    // Fix ECShop全系列版本的远程代码执行漏洞: https://paper.seebug.org/695/
    $arr['num'] = intval($arr['num']);
    $arr['id']  = intval($arr['id']);
    $arr['goods_id']  = intval($arr['goods_id']);
    $need_cache = $GLOBALS['smarty']->caching;
    $need_compile = $GLOBALS['smarty']->force_compile;

    $GLOBALS['smarty']->caching = false;
    $GLOBALS['smarty']->force_compile = true;

    /* 產品购买记录 */
    $sql = 'SELECT u.user_name,u.user_rank, og.goods_number, oi.add_time, og.goods_name ,og.goods_price ,IF(oi.order_status IN (2, 3, 4), 0, 1) AS order_status ' .
           'FROM ' . $GLOBALS['ecs']->table('order_info') . ' AS oi LEFT JOIN ' . $GLOBALS['ecs']->table('users') . ' AS u ON oi.user_id = u.user_id, ' . $GLOBALS['ecs']->table('order_goods') . ' AS og ' .
           'WHERE oi.order_id = og.order_id AND og.goods_id = ' . $arr['id'] . ' ORDER BY oi.add_time DESC LIMIT 5';
    $bought_notes = $GLOBALS['db']->getAll($sql);

    foreach ($bought_notes as $key => $val)
    {
        $bought_notes[$key]['add_time'] = local_date("Y-m-d G:i:s", $val['add_time']);


		$bought_notes[$key]['consignee1'] =getstr($val['user_name'],4);  //增加收货人真实名
	//	$bought_notes[$key]['consignee2'] =getstr($val['user_name'],-3); //增加收货人真实名称
    }

    $sql = 'SELECT count(*) ' .
           'FROM ' . $GLOBALS['ecs']->table('order_info') . ' AS oi LEFT JOIN ' . $GLOBALS['ecs']->table('users') . ' AS u ON oi.user_id = u.user_id, ' . $GLOBALS['ecs']->table('order_goods') . ' AS og ' .
           'WHERE oi.order_id = og.order_id  AND og.goods_id = ' . $arr['id'];
    $count = $GLOBALS['db']->getOne($sql);
	$size = 5;
$page_count = ($count > 0) ? intval(ceil($count / $size)) : 1;

    /* 產品购买记录分页样式 */
	 /* 分页样式 */
	 $_pagenum = 10;     // 显示的页码
        $_offset = 4;       // 当前页偏移值
        $_from = $_to = 0;  // 开始页, 结束页
        if($_pagenum > $page_count)
        {
            $_from = 1;
            $_to = $page_count;
        }
        else
        {
            $_from = $page - $_offset;
            $_to = $_from + $_pagenum - 1;
            if($_from < 1)
            {
                $_to = $page + 1 - $_from;
                $_from = 1;
                if($_to - $_from < $_pagenum)
                {
                    $_to = $_pagenum;
                }
            }
            elseif($_to > $page_count)
            {
                $_from = $page_count - $_pagenum + 1;
                $_to = $page_count;
            }
		}

    $pager = array();
	    $pager['page']         = $page = 1;
    $pager['size']         = $size ;
    $pager['record_count'] = $count;
    $pager['page_count']   = $page_count ;
    $pager['page_first']   = "1";
    $pager['page_prev']    = $page > 1 ? $page-1 : '1';
    $pager['page_next']    = $page < $page_count  ? $page+1 : '1';
    $pager['page_last']    = $page < $page_count ? $page_count : '1';

$pager['page_number'] = array();
      	    for ($i=$_from;$i<=$_to;++$i)
            {
                $pager['page_number'][$i] = $url_format . $i;
            }

    $GLOBALS['smarty']->assign('notes', $bought_notes);
    $GLOBALS['smarty']->assign('pager', $pager);


    $val= $GLOBALS['smarty']->fetch('library/bought_notes.lbi');

    $GLOBALS['smarty']->caching = $need_cache;
    $GLOBALS['smarty']->force_compile = $need_compile;

    return $val;
}


/**
 * 调用在线调查信息
 *
 * @access  public
 * @return  string
 */
function insert_vote()
{
    $vote = get_vote();
    if (!empty($vote))
    {
        $GLOBALS['smarty']->assign('vote_id',     $vote['id']);
        $GLOBALS['smarty']->assign('vote',        $vote['content']);
    }
    $val = $GLOBALS['smarty']->fetch('library/vote.lbi');

    return $val;
}



/**
* 调用某產品的累积已售出  ly snsn 130304
*/
function insert_goods_sells($arr)
{
    // Fix ECShop全系列版本的远程代码执行漏洞: https://paper.seebug.org/695/
    $arr['goods_id'] = intval($arr['goods_id']);

	$sql = 'SELECT g.`sales`+g.`sales_adjust` AS number ' .
	    ' FROM ' . $GLOBALS['ecs']->table('goods') . " as g " .
		" WHERE g.goods_id = " . $arr['goods_id'];
	$row = $GLOBALS['db']->GetRow($sql);
	$num = $row ? intval($row['number']) : 0;
	return insert_goods_sells_text(array('num'=>$num));
}

function insert_goods_sells_text($arr)
{
	$num = $arr['num'];
	$class = empty($arr['class']) ? '' : $arr['class'];

	// if 0-10, then 已售出近10件
	// if 11-20, then 已售出超過10件
	// if 21-50, then 已售出超過20件
	// if 51-100, then 已售出超過50件
	// if 101-200, then 已售出超過100件
	// if 201-500, then 已售出超過200件
	// if 501-1000, then 已售出超過500件

	if ($num <= 10) // 0 - 10
	{
        $num = '10';
		if (!empty($class))
		{
            $num = '<span class="' . $class . '">' . $num . '</span>';
		}
        return sprintf(_L('goods_sells_text_near', '<%s 件'), $num);
	}
	else
	{
		$zeros = floor(log10($num));
		if (ceil(log10($num)) == $zeros) $zeros -= 1;
		$factor = pow(10, $zeros);
		$first = ceil($num / $factor);
		$lower_bound = ($first <= 2) ? $factor : (($first <= 5) ? $factor * 2 : $factor * 5);
		if (!empty($class))
		{
			$lower_bound = '<span class="' . $class . '">' . $lower_bound . '</span>';
		}
		return sprintf(_L('goods_sells_text_over','>%s 件'), $lower_bound);
    }
}


/**
* 调用某產品的评论数  ly snsn 130304
* arr  lei=1 判断为评论  lei=2判断为问答
*/

function insert_comment_num($arr)  {
     $sql= "select count(*) as number from ".$GLOBALS['ecs']->table('comment')." where id_value='".intval($arr['goods_id'])."'  AND status = 1 and lei='".intval($arr['lei'])."'";
       $row = $GLOBALS['db']->GetRow($sql);
    if ($row)
    {
        $number = intval($row['number']);
    }
    else
    {
        $number = 0;
    }
    return $number;
}




function insert_comment_rank($arr)  {
    // Fix ECShop全系列版本的远程代码执行漏洞: https://paper.seebug.org/695/
    $arr['goods_id']  = intval($arr['goods_id']);
    global $_CFG;
    $sql= "select sum(comment_rank) as tnumber , count(*) as num from ".$GLOBALS['ecs']->table('comment')." where id_value='".$arr['goods_id']."'  AND status = 1";
    $row = $GLOBALS['db']->GetRow($sql);
    if ($row['num']>0)
    {
        $number = intval($row['tnumber'])/$row['num'];
        $number =sprintf("%01.1F", $number);
        
        if($row['num'] < 3 && $number <= 3) {
            $number = 0;
        }
    }
    else
    {
        $number = 0;
    }
    if($_CFG['comment_open'] == 0) {
        $GLOBALS['smarty']->assign('can_comment', 0);
        return '';
    } else {
        $commentController = new Yoho\cms\Controller\CommentController();
        $can_comment = $commentController->user_can_comment($commentController::COMMENT_TYPE_GOODS, $arr['goods_id']);
        $GLOBALS['smarty']->assign('can_comment', $can_comment['can_comment']);
    }

    $GLOBALS['smarty']->assign('rating',        $number);
    $GLOBALS['smarty']->assign('total_comment', $row['num']);

    $val = $GLOBALS['smarty']->fetch('library/comment_rank.lbi');

    return $val;
}

function insert_comment_rankf($arr)  {
     $sql= "select sum(comment_rank) as tnumber , count(*) as num from ".$GLOBALS['ecs']->table('comment')." where id_value='".$arr['goods_id']."'  AND status = 1";
       $row = $GLOBALS['db']->GetRow($sql);
    if ($row['num']>0)
    {
        $number = intval($row['tnumber'])/$row['num']/5*100;
		$number =sprintf("%01.1F", $number);

    }
    else
    {
        $number = "100";
    }
    return $number;
}


function insert_t1($arr)  {
     $sql= "select promote_end_date from ".$GLOBALS['ecs']->table('goods')." where goods_id='".$arr['goods_id']."'  AND is_promote = 1";
      $row = $GLOBALS['db']->GetRow($sql);
  $t1= ($row['promote_end_date']-time())*1000;
    return $t1;
}

function insert_t2($arr)  {
   $t2=time()*1000;
    return $t2;
}



    function getstr($string, $length, $encoding  = 'utf-8') {
        $string = trim($string);

        if($length && strlen($string) > $length) {
            //截断字符
            $wordscut = '';
            if(strtolower($encoding) == 'utf-8') {
                //utf8编码
                $n = 0;
                $tn = 0;
                $noc = 0;
                while ($n < strlen($string)) {
                    $t = ord($string[$n]);
                    if($t == 9 || $t == 10 || (32 <= $t && $t <= 126)) {
                        $tn = 1;
                        $n++;
                        $noc++;
                    } elseif(194 <= $t && $t <= 223) {
                        $tn = 2;
                        $n += 2;
                        $noc += 2;
                    } elseif(224 <= $t && $t < 239) {
                        $tn = 3;
                        $n += 3;
                        $noc += 2;
                    } elseif(240 <= $t && $t <= 247) {
                        $tn = 4;
                        $n += 4;
                        $noc += 2;
                    } elseif(248 <= $t && $t <= 251) {
                        $tn = 5;
                        $n += 5;
                        $noc += 2;
                    } elseif($t == 252 || $t == 253) {
                        $tn = 6;
                        $n += 6;
                        $noc += 2;
                    } else {
                        $n++;
                    }
                    if ($noc >= $length) {
                        break;
                    }
                }
                if ($noc > $length) {
                    $n -= $tn;
                }
                $wordscut = substr($string, 0, $n);
            } else {
                for($i = 0; $i < $length - 1; $i++) {
                    if(ord($string[$i]) > 127) {
                        $wordscut .= $string[$i].$string[$i + 1];
                        $i++;
                    } else {
                        $wordscut .= $string[$i];
                    }
                }
            }
            $string = $wordscut;
        }
        return trim($string);
    }

// By howang, return cached ucdata if available
function insert_cached_ucdata()
{
    if (!empty($_SESSION['cached_ucdata']))
    {
        $ucdata = $_SESSION['cached_ucdata'];

        // Clear the ucdata
        $_SESSION['cached_ucdata'] = '';
        unset($_SESSION['cached_ucdata']);

        return '<div style="display: none;">' . $ucdata . '</div>';
    }

    return '';
}

// By howang, prompt the user to review his/her orders
function insert_reviewable_orders()
{
    if (empty($_SESSION['user_id']))
    {
        return '';
    }

    if (!empty($_COOKIE['hide_review_order_prompt']))
    {
        return '';
    }

    global $ecs, $db;

    $sql = "SELECT order_id, order_sn FROM " . $ecs->table('order_info') .
            " WHERE user_id = '" . $_SESSION['user_id'] . "'".
            " AND reviewed = 0 ".
            " AND order_status NOT " . db_create_in(array(OS_UNCONFIRMED, OS_CANCELED, OS_INVALID, OS_RETURNED, OS_WAITING_REFUND)).
            " AND shipping_status " . db_create_in(array(SS_SHIPPED, SS_RECEIVED)).
            " AND pay_status = '" . PS_PAYED . "'".
            " AND salesperson != ''";
    $orders = $db->getAll($sql);

    if (empty($orders))
    {
        return '';
    }

    // if (count($orders) == 1)
    // {
        $order = $orders[0];
        $msg = _L('reviewable_orders_message', '你有未評價的訂單。 ');
        $link = '/review.php?order_id=' . $order['order_id'];
        $link_text = _L('reviewable_orders_btn', '立即評價');
    // }
    // else
    // {
    //     $msg = '你有' . count($orders) . '個未評價的訂單。 ';
    //     $link = '/user.php?act=order_list';
    //     $link_text = '按此查看';
    // }

    if (defined('YOHO_MOBILE'))
    {
        return '<div class="review-order-prompt alert alert-warning alert-dismissible" role="alert">'.
               '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.
               $msg . '<a href="' . $link . '" class="alert-link">' . $link_text . '</a>'.
               '</div>';
    }
    else
    {
        return '<div class="review-order-prompt">'.
               '<button type="button" class="close">&times;</button>'.
               $msg.'<a href="' . $link . '"><b>' . $link_text . '</b></a>'.
               '</div>';
    }
}

function insert_flashdealPayload($arr)
{
    $goods_id = $arr['goods_id'];
    $_REQUEST['fid'] = ($arr['fid']) ? $arr['fid'] : $_REQUEST['fid'];
    if (!isset($_REQUEST['fid'])){
        $_REQUEST['fid'] = '';
    }
    $flashdealController = new \Yoho\cms\Controller\FlashdealController();
    $fid = intval($_REQUEST['fid']);
    $deal = $flashdealController->getFlashdealInfo($goods_id, $fid);

    return $deal['payload'];
}

function insert_goodspromote_remaining_qty_html($arr)
{
    global $smarty, $ecs, $db;
    
    $goods_id = $arr['goods_id'];
    $fid = $arr['f_id'];
    $flashdealController = new \Yoho\cms\Controller\FlashdealController();

    //$fid = intval($_REQUEST['fid']);
    $remaining_qty = $flashdealController->getFlashdealRemainingQty($goods_id, $fid);
    
    return $qty_html = '<div class="flashdeal-qty' . ($remaining_qty == 0 ? 'sold-out' : '' ) . '">'.sprintf(_L('flashdeal_remaining_quantity', '剩餘 %s 個'),$remaining_qty).'</div>';
}

function insert_goodspromote_flashdeal_payload_html($arr)
{
    global $smarty, $ecs, $db;
    
    $goods_id = $arr['goods_id'];
    $fid = $arr['f_id'];
    
    $flashdealController = new \Yoho\cms\Controller\FlashdealController();
    $remaining_qty = $flashdealController->getFlashdealRemainingQty($goods_id, $fid);
    $html = '';
    if ($remaining_qty > 0) {
        
        $_REQUEST['fid'] = ($arr['f_id']) ? $arr['f_id'] : $_REQUEST['fid'];
        
        if (!isset($_REQUEST['fid'])){
            $_REQUEST['fid'] = '';
        }
     
        $flashdealController = new \Yoho\cms\Controller\FlashdealController();
        $fid = intval($_REQUEST['fid']);
        $deal = $flashdealController->getFlashdealInfo($goods_id, $fid);
        return '<div style="display:none">'.$deal['payload'].'</div>';
    } else {
        return '';
    }
}

function insert_goodspromote_flashdeal_add_to_cart_html($arr)
{
    global $smarty, $ecs, $db;
    
    $goods_id = $arr['goods_id'];
    $fid = $arr['f_id'];
    $flashdealController = new \Yoho\cms\Controller\FlashdealController();

    //$fid = intval($_REQUEST['fid']);
    $remaining_qty = $flashdealController->getFlashdealRemainingQty($goods_id, $fid);
    
    $html = '';
    if ($remaining_qty <= 0) {
        $html .= '<div class="sold-out-text"> '. _L('flashdeal_in_the_cart', '此閃購所有剩餘數量都被人選購中, 請稍後再試') .' </div>';
        $html .= '<button disabled class="action-button sold-out" type="submit" value=""><i class="fa fa-fw fa-shopping-cart"></i> '.  _L('goods_add_to_cart', '加入購物車') .'</button>';
    } else {
        $html .= '<button id="goods_buy_btn" class="action-button" type="submit" value="'. $fid .'"><i class="fa fa-fw fa-shopping-cart"></i> '.  _L('goods_add_to_cart', '加入購物車') .'</button>';
    }

    return $html;
}

function insert_goodspromote_flashdeal_add_to_cart_m_html($arr)
{
    global $smarty, $ecs, $db;
    
    $goods_id = $arr['goods_id'];
    $fid = $arr['f_id'];
    $flashdealController = new \Yoho\cms\Controller\FlashdealController();
    //$fid = intval($_REQUEST['fid']);
    $deal = $flashdealController->getFlashdealInfo($goods_id, $fid);
    if (!isset($deal['random_left'])) {
        $deal['random_left'] = '';
    }
    $html = '';
    $html .= '<li class="double-btn buy flashdeal '.($deal['remaining_qty']<=0? 'disable' : '').' "><a onclick="javascript:void(0);" data-rl="'.$deal['random_left'].'" data-key="'.$deal['cart_key'].''.$deal['random'].'" data-id="'.$goods_id.'" data-fid="'.$deal['flashdeal_id'].'"><i class="fa fa-shopping-cart" aria-hidden="true"></i> '._L('goods_buy_now', '立即購買') .'</a></li>';
    
    return $html;
}

// By howang, used to replace ajaxPromotion.php
function insert_goodspromote($arr)
{
    global $smarty, $ecs, $db,$is_testing;
    $flashdealController = new \Yoho\cms\Controller\FlashdealController();
    $goods_id = $arr['goods_id'];
    if (!isset($_REQUEST['fid'])){
        $_REQUEST['fid'] = '';
    }

    $fid = intval($_REQUEST['fid']);
    $deal = $flashdealController->getFlashdealInfo($goods_id, $fid);

    $arr['return_output'] = !isset($arr['return_output'])? true : $arr['return_output'];
    // If goods already assigned to smarty, use it
    $goods = $smarty->get_template_vars('goods');

    if (!$goods)
    {
        //$start_t = microtime(true);  
        // Otherwise, get goods info

        if ($is_testing){
            $goods = get_goods_info($goods_id,true);
        } else {
            $goods = get_goods_info($goods_id);
        }

        //$goods = get_goods_info($goods_id);
        
        //$end_t = microtime(true);
        //$time_t = number_format(($end_t - $start_t), 2);
        //echo 'This page loaded in ', $time_t, ' seconds';

        if (!$goods) return '';
    }
    //brand-logo
    if (isset($goods['brand_id']) && $goods['brand_id'] > 0 && !$smarty->get_template_vars('brand_logo'))
    {
        $sql = "SELECT brand_logo, brand_id, brand_name, bperma FROM " . $ecs->table('brand') . ' b ' .
            "LEFT JOIN (SELECT id, table_name, lang, perma_link as bperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") bpl ON bpl.id = b.brand_id  AND bpl.table_name = 'brand' AND bpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
            " WHERE brand_id = '" . $goods['brand_id'] . "' " .
            "LIMIT 1";
        $brand = $db->getRow($sql);
        if ($brand['brand_logo'])
        {
            $brand_logo = '/' . DATA_DIR . '/brandlogo/' . $brand['brand_logo'];
            $brand_url = build_uri('brand', array('cid' => 0, 'bid' => $brand['brand_id'], 'bperma' => $brand['bperma']), $brand['brand_name']);
            $smarty->assign('brand_logo', $brand_logo);
            $smarty->assign('brand_url', $brand_url);
        }
    }
    // YOHO: Don't show accurate stocks
    $goods['goods_number'] = $goods['goods_number'] < 20 ? $goods['goods_number'] : 20;
    if($deal) {
        $userRankController = new \Yoho\cms\Controller\UserRankController();
        $arr_user_rank_list = $userRankController->getUserRankProgramName();
        $user_rank = [];
        foreach ($arr_user_rank_list as $index => $rank){
            $user_rank[$rank['rank_id']] = $rank['rank_name'];
        }
        $deal['rank_filter'] = 0;
        if (empty($deal['user_rank'])){
            $deal['user_rank'] = 0;

        } else {
            $deal['user_rank'] = explode("^", $deal['user_rank']);
            if (!in_array("1", $deal['user_rank'])){
                $deal['rank_filter'] = 1;
            }
            foreach ($deal['user_rank'] as $index => $rank){
                $deal['user_rank'][$index] = array("id"=>$rank, "name"=>$user_rank[$rank]);
            }
        }

        $goods['remaining_qty'] = $deal['remaining_qty'];
        $goods['deal_price_formated'] = price_format($deal['deal_price']);

        // YOHO: lowest price = 3% discount, have to pay with cash / ATM / ebanking
        $goods['lowest_pirce'] = ceil(doubleval($deal['deal_price']) * 0.97);
        $goods['lowest_price_formated'] = price_format($goods['lowest_pirce']);

        // Saved price
        $goods['saved_price'] = $goods['market_price2'] - $goods['lowest_pirce'];
        $goods['saved_price_normal'] = $goods['saved_price'];
        $goods['saved_price_formated'] = price_format($goods['saved_price']);
        $goods['saved_price_normal_formated'] = price_format($goods['saved_price']);
        $goods['is_sp_price'] = false;
        $goods['is_vip_price'] = false;
        $goods['is_promote'] = false;
        $goods['is_pre_sale'] = false;
        //$goods['goods_number'] = 1;
        $goods['saved_percentage'] = round($goods['saved_price'] / $goods['market_price2'] * 100.0, 2) . '%';
        $goods['saved_percentage_rounded'] = round($goods['saved_price'] / $goods['market_price2'] * 100.0) . '%';

        $smarty->assign('flashdeal', $deal);
    }
    // and assign it to smarty
    $smarty->assign('goods',  $goods);

    // Assign specification to smarty if needed
    if (!$smarty->get_template_vars('specification'))
    {
        $properties = get_goods_properties($goods_id);  // 获得商品的规格和属
        $smarty->assign('specification', $properties['spe']); // 商品规格
    }

    // Save current configuration
    $need_cache = $smarty->caching;
    $need_compile = $smarty->force_compile;

    // Disable caching and force compilation
    $smarty->caching = false;
    $smarty->force_compile = true;

    // Assign other values to smarty
    $smarty->assign('today', get_today_info());
    $smarty->assign('user_area', user_area());

    //Obtain smarty output

    $output = (strpos($_SERVER['SERVER_NAME'], 'm.') !== false) ? $smarty->fetch('library/goodspromote.lbi') : $smarty->fetch('ajax_goodspromote_putong.dwt');

    $output = $smarty->process_inserts($output);

    // Restore configuration values
    $smarty->caching = $need_cache;
    $smarty->force_compile = $need_compile;
    if ($arr['return_output']) {
        return $output;  
    }
}

$lib_insert_user_wish_list = null;

// By howang, add to / remove from wish list button in goods list
function insert_wish_list_btn($arr)
{
    $goods_id = $arr['goods_id'];

    if (empty($_SESSION['user_id']))
    {
        return '';
    }

    global $lib_insert_user_wish_list;
    if ($lib_insert_user_wish_list === null)
    {
        $lib_insert_user_wish_list = array();
        $sql = "SELECT goods_id FROM " . $GLOBALS['ecs']->table('wish_list') . " WHERE `user_id` = '" . intval($_SESSION['user_id']) . "'";
        $lib_insert_user_wish_list = $GLOBALS['db']->getCol($sql);
    }

    if (in_array($goods_id, $lib_insert_user_wish_list))
    {
        return '<a href="javascript:void(0)" class="removewishlist" rel="nofollow" title="' . _L('wishlist_remove', '從願望清單中移除') . '" data-goods-id="' . $goods_id . '"></a>';
    }
    else
    {
        return '<a href="javascript:void(0)" class="addwishlist" rel="nofollow" title="' . _L('wishlist_add', '加入願望清單') . '" data-goods-id="' . $goods_id . '"></a>';
    }
}

// By howang, add to / remove from wish list button in goods list
function insert_product_wish_list_btn($arr)
{
    $goods_id = $arr['goods_id'];

    if (empty($_SESSION['user_id']))
    {
        return '';
    }

    global $lib_insert_user_wish_list;
    if ($lib_insert_user_wish_list === null)
    {
        $lib_insert_user_wish_list = array();
        $sql = "SELECT goods_id FROM " . $GLOBALS['ecs']->table('wish_list') . " WHERE `user_id` = '" . intval($_SESSION['user_id']) . "'";
        $lib_insert_user_wish_list = $GLOBALS['db']->getCol($sql);
    }

    if (in_array($goods_id, $lib_insert_user_wish_list))
    {
        return '<a href="javascript:void(0);" class="product-wishlist on" rel="nofollow" title="' . _L('wishlist_remove', '從願望清單中移除') . '" data-goods-id="' . $goods_id . '"><div class="product-wishlist-tooltip">' . _L('productlist_wishlist_tooltip', '好想買') . '</div></a>';
    }
    else
    {
        return '<a href="javascript:void(0);" class="product-wishlist" rel="nofollow" title="' . _L('wishlist_add', '加入願望清單') . '" data-goods-id="' . $goods_id . '"><div class="product-wishlist-tooltip">' . _L('productlist_wishlist_tooltip', '好想買') . '</div></a>';
    }
}

// By howang, add to / remove from wish list button in goods page
function insert_wish_list_btn_big($arr)
{
    $goods_id = $arr['goods_id'];

    global $lib_insert_user_wish_list;
    if ($lib_insert_user_wish_list === null)
    {
        $lib_insert_user_wish_list = array();
        $sql = "SELECT goods_id FROM " . $GLOBALS['ecs']->table('wish_list') . " WHERE `user_id` = '" . intval($_SESSION['user_id']) . "'";
        $lib_insert_user_wish_list = $GLOBALS['db']->getCol($sql);
    }

    if (in_array($goods_id, $lib_insert_user_wish_list))
    {
        return '<a href="javascript:void(0);" class="action-button action-button-gray wishlist-btn on" title="' . _L('wishlist_remove', '從願望清單中移除') . '" data-goods-id="' . $goods_id . '"><i class="fa fa-fw fa-heart"></i> <span>' . _L('wishlist_on_wishlist', '在願望清單上') . '</span></a>';
    }
    else
    {
        return '<a href="javascript:void(0);" class="action-button action-button-gray wishlist-btn" title="' . _L('wishlist_add', '加入願望清單') . '" data-goods-id="' . $goods_id . '"><i class="fa fa-fw fa-plus"></i> <span>' . _L('wishlist_add', '加入願望清單') . '</span></a>';
    }
}

// By howang, add to / remove from wish list button in mobile goods page
function insert_wish_list_btn_mobile($arr)
{
    $goods_id = $arr['goods_id'];

    $sql = "SELECT '1' FROM " . $GLOBALS['ecs']->table('wish_list') . " WHERE `user_id` = '" . intval($_SESSION['user_id']) . "' AND `goods_id` = '" . intval($goods_id) . "' LIMIT 1";
    $in_wish_list = $GLOBALS['db']->getOne($sql);

    if ($in_wish_list)
    {
        $text = _L('wishlist_remove', '從願望清單中移除');
    }
    else
    {
        $text = _L('wishlist_add', '加入願望清單');
    }

    return
        '<a href="javascript:void(0)" class="btn btn-gray btn-lg btn-block left-icon-btn goods-wishlist-btn' . ($in_wish_list ? ' on' : '') . '" title="' . $text . '" data-goods-id="' . $goods_id . '">' .
            '<span class="glyphicon glyphicon-heart" aria-hidden="true"></span><span>'. $text . '</span>' .
        '</a>';
}

function insert_affiliateyoho_custom($arr){
    if($affiliate['config']['affiliate_text']) {
        $text = $affiliate['config']['affiliate_text'];
        $text = str_replace("{%url}", $url, $text);
        $text = str_replace("{%code}", intval($_SESSION['user_id']), $text);
    }
    $title = _L('user_affiliate_type');
    $img = substr($_SERVER['HTTP_HOST'], 0, 2) === "m." ? "../yohohk/img/affiliate.png" : $_CFG['affiliate_btn_img'];
    $tmp_img = dirname(__FILE__).'/'.$img;
    $banner_img = str_replace('../', '/', $_CFG['affiliate_banner_img']);
    if(!empty($img) && file_exists($tmp_img)) {
        $img = substr($_SERVER['HTTP_HOST'], 0, 2) === "m." ? "/yohohk/img/affiliate.png" :'/'.$_CFG['affiliate_btn_img'];
        if ($mode == 'link') {
            return '<a href="' . $_CFG['affiliate_link'] . '" id="" data-text="'.$text.'" data-title="'.$title.'" data-banner = "'.$banner_img.'" data-is_login="'.(($_SESSION['user_id']) ? "y":"n").'" data-share="' . $url . '"' . (!empty($arr['link_class']) ? ' class="' . $arr['link_class'] . '_img"' : '') . '>' . '<img src="'. $img . '" alt="'.$title.'"></a>';
        }
        return '<a id="affiliatelink" data-text="'.$text.'" data-title="'.$title.'" data-banner = "'.$banner_img.'" data-is_login="'.(($_SESSION['user_id']) ? "y":"n").'" data-share="' . $url . '"' . (!empty($arr['link_class']) ? ' class="' . $arr['link_class'] . '_img"' : '') . '>' . '<img src="'. $img . '" alt="'.$title.'"></a>';
    } else {
        return '<a id="affiliatelink" data-text="'.$text.'" data-title="'.$title.'"  data-banner = "'.$banner_img.'" data-is_login="'.(($_SESSION['user_id']) ? "y":"n").'" data-share="' . $url . '"' . (!empty($arr['link_class']) ? ' class="' . $arr['link_class'] . '"' : '') . '>' . (!empty($arr['link_text']) ? $arr['link_text'] : '<i class="fa fa-link"></i> ' . _L('user_affiliate_link', '友福同享專用鏈結')) . '</a>';
    }

}

// By howang, display affiliate link if user have affiliate access to the product
function insert_affiliateyoho_link($arr)
{
    global $_CFG, $_LANG;
    $mode = isset($arr['mode']) ? $arr['mode'] : 'popup';

    $is_hide = isset($arr['is_hide']) ? $arr['is_hide'] : false;

    

    $goods_id = $arr['goods_id'];
    if(!empty($goods_id)) {
        $cat_id = $arr['cat_id'];
        $url = (!empty($arr['goods_url'])) ? $arr['goods_uri'] : build_uri('goods', array('gid' => $goods_id));
        $url = ($url{0} == '/') ? $GLOBALS['ecs']->url() . substr($url, 1) : $url;
    } else {
        $url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    }
    
    if($_SESSION['user_id'])$url = uri_append_param($url, 'u', intval($_SESSION['user_id']));

    $affiliate = $affiliate = unserialize($_CFG['affiliate']);
   
    if(isset($affiliate['on']) && $affiliate['on'] == 1) { //友福同享
        if($affiliate['config']['affiliate_text']) {
            $text = $affiliate['config']['affiliate_text'];
            $text = str_replace("{%url}", $url, $text);
            $text = str_replace("{%code}", intval($_SESSION['user_id']), $text);
        }
        $title = _L('user_affiliate_type');
        //$img = substr($_SERVER['HTTP_HOST'], 0, 2) === "m." ? "../yohohk/img/affiliate.png" : $_CFG['affiliate_btn_img'];
        $img = $_CFG['affiliate_btn_img'];
        $tmp_img = dirname(__FILE__).'/'.$img;
        $banner_img = str_replace('../', '/', $_CFG['affiliate_banner_img']);

        $img = $GLOBALS['ecs']->url() .$img;

        if(!empty($img) && file_exists($tmp_img)) {
            if ($mode == 'link') {
                return '<a href="' . $_CFG['affiliate_link'] . '" id="" data-text="'.$text.'" data-title="'.$title.'" data-banner = "'.$banner_img.'" data-is_login="'.(($_SESSION['user_id']) ? "y":"n").'" data-share="' . $url . '"' . (!empty($arr['link_class']) ? ' class="' . $arr['link_class'] . '_img"' : '') . '>' . '<img src="'. $img . '" alt="'.$title.'"></a>';
            }
            return '<a id="affiliatelink" '. ($is_hide ? 'style="display:none;"':'').' data-text="'.$text.'" data-title="'.$title.'" data-banner = "'.$banner_img.'" data-is_login="'.(($_SESSION['user_id']) ? "y":"n").'" data-share="' . $url . '"' . (!empty($arr['link_class']) ? ' class="' . $arr['link_class'] . '_img"' : '') . '>' . '<img src="'. $img . '" alt="'.$title.'"></a>';
        } else {
            return '<a id="affiliatelink" '. ($is_hide ? 'style="display:none;"':'').' data-text="'.$text.'" data-title="'.$title.'"  data-banner = "'.$banner_img.'" data-is_login="'.(($_SESSION['user_id']) ? "y":"n").'" data-share="' . $url . '"' . (!empty($arr['link_class']) ? ' class="' . $arr['link_class'] . '"' : '') . '>' . (!empty($arr['link_text']) ? $arr['link_text'] : '<i class="fa fa-link"></i> ' . _L('user_affiliate_link', '友福同享專用鏈結')) . '</a>';
        }

    } elseif(isset($affiliate['affiliate_yoho_on']) && $affiliate['affiliate_yoho_on'] == 1) { //推廣大使專用連結
        $title = _L('user_affiliate');
        // Check user login
        if (!$_SESSION['user_id'] || substr($_SERVER['HTTP_HOST'], 0, 2) === "m.")
        {
            return '';
        }
        // Check if user have affiliate access to the specific goods
        $sql = "SELECT '1' " .
        "FROM " . $GLOBALS['ecs']->table('affiliate_rewards_detail') . " as ard " .
        "LEFT JOIN" . $GLOBALS['ecs']->table('affiliate_rewards') . " as ar ON ar.reward_id = ard.reward_id " .
        "WHERE ar.user_id = '" . intval($_SESSION['user_id']) . "' ".
        "AND (" . gmtime() . " BETWEEN ar.start_time AND ar.end_time) " .
        "AND ((" .
            "ard.type = 'all'" .
        ") OR (" .
            "ard.type = 'goods' AND ard.related_id = '" . intval($goods_id) . "'" .
        ") OR (" .
            "ard.type = 'category' AND ard.related_id = '" . intval($cat_id) . "'" .
        "))" .
        "LIMIT 1";
        $is_affiliate = $GLOBALS['db']->getOne($sql);

        if (!$is_affiliate)
        {
            return '';
        }

        $url = (!empty($arr['goods_url'])) ? $arr['goods_uri'] : build_uri('goods', array('gid' => $goods_id));
        $url = ($url{0} == '/') ? $GLOBALS['ecs']->url() . substr($url, 1) : $url;
        $url = uri_append_param($url, 'u', intval($_SESSION['user_id']));

        if ($mode == 'url')
        {
            return $url;
        }
        else//if ($mode == 'link')
        {
            return '<a id="affiliatelink" data-title="'.$title.'" data-banner = "'.$banner_img.'" data-is_login="'.(($_SESSION['user_id']) ? "y":"n").'" data-share="' . $url . '"' . (!empty($arr['link_class']) ? ' class="' . $arr['link_class'] . '"' : '') . '>' . (!empty($arr['link_text']) ? $arr['link_text'] : '<i class="fa fa-link"></i> ' . _L('goods_affiliate_link', '推廣大使專用鏈結')) . '</a>';
        }
    } else {
        return '';
    }
}

// By howang, get avatar URL with query string suffix to prevent caching issues
function insert_avatar_url($arr)
{
    global $ecs, $db;
    $avatar_id = (!empty($arr['id'])) ? intval($arr['id']) : ((!empty($arr['user_id'])) ? intval($arr['user_id']) : 0);
    $sql = "SELECT sex  FROM " .$ecs->table('users'). " WHERE user_id='$avatar_id'";
    $sex = $db->getOne($sql);
    $size = empty($arr['size']) ? 'm' : strtolower($arr['size']);
    $size = in_array($size, array('s','m','l')) ? $size : 'm';

    $size_suffixes = array(
    	's' => '_60x60',
    	'm' => '_100x100',
    	'l' => '',
    );

    $size_suffix = $size_suffixes[$size];

    $path = 'uploads/avatars/' . $avatar_id . $size_suffix . '.png';

    $lastmod = @filemtime(ROOT_PATH . $path);

    if ($lastmod !== false)
    {
    	$url = '/' . $path . '?' . $lastmod;
    }
    else
    {
        // add rand icon code
        $rand_icon = '';
        if($sex > 0) {
            $sex = ($sex == 1) ? 'male' : 'female';
            $num = $avatar_id % 2;
            $num += 1;
            $rand_icon = '_'.$sex.'_'.$num;
        }
    	$url = '/uploads/avatars/default'. $rand_icon . $size_suffix . '.png';
    }

    return $url;
}

// By howang, for generating cachable URL for javascript language pack
function insert_language_js($arr)
{
    $lang = $GLOBALS['_CFG']['lang'];
    $pack = empty($arr['pack']) ? 'common' : $arr['pack'];
    // $language_file = ROOT_PATH . 'languages/' . $lang . '/yoho.php';
    // $hash = dechex(filemtime($language_file));

    // return '<script>YOHO.lang = ' . json_encode($lang) . ';</script>' .
    // '<script src="/ajax/language-' . urlencode($pack) . '-' . urlencode($lang) . '-' . urlencode($hash) . '.js"></script>' . "\n";

    $languages = array();
    $langKeys = language_keys_for_pack($pack);
	foreach ($langKeys as $key)
	{
		if (isset($GLOBALS['_LANG'][$key]))
		{
			$languages[$key] = $GLOBALS['_LANG'][$key];
		}
	}
    return '<script type="text/javascript">' .
        'var YOHO=YOHO||{};YOHO.lang=' . json_encode($lang) . ';YOHO.languages=YOHO.languages||{};' .
        '$.extend(YOHO.languages, ' . json_encode($languages) . ');' .
    '</script>' . "\n";
}

// By howang, for debugging
function insert_var_dump($arr)
{
    ob_start();
    var_dump($arr['var']);
    return '<pre>' . ob_get_clean() . '</pre>';
}

function insert_index_review()
{
    global $smarty, $ecs, $db;

    $user_area = user_area();

    if ($user_area != 'CN') {
        $google_review = [
            // [
            //     'img_src'  => 'https://lh4.googleusercontent.com/-2c9p9fl1rzw/AAAAAAAAAAI/AAAAAAAAAAA/YzoNpr7qUZs/w60-h60-p-rp-mo-br100/photo.jpg',
            //     'name'     => 'annaannakwok Kwok',
            //     'review'   => '職員好好人服務態度一流，回覆快又有禮貌，令我可是以放心購物～門市同網上購物時電器選擇多元化，夠新夠齊全，同一種產品有齊多種牌子俾我哋選擇，輕鬆搜尋到心水產品省是時快捷，而且價錢夠實惠，更會不時推出優惠，合符經濟原則，購物有信心保證令人滿意，友和Yoho係一間好有心嘅鋪，為我帶嚟網上購物新體驗，一定會向朋友大力推介！',
            //     'rating'   => '5'
            // ],
            [
                'img_src' => 'https://lh3.googleusercontent.com/-DyU_AgJY5-U/AAAAAAAAAAI/AAAAAAAAAAA/QQ1NoM35nsI/w60-h60-p-rp-mo-ba3-br100/photo.jpg',
                'name'    => 'chu tony',
                'review'  => '最新電子潮流產品，家用電器亦幾多。',
                'rating'  => '5'
            ],
            [
                'img_src' => 'https://lh6.googleusercontent.com/-5DLDIebX3PA/AAAAAAAAAAI/AAAAAAAAAAA/CAfzunVUlI4/w60-h60-p-rp-mo-br100/photo.jpg',
                'name'    => 'Cathy Ng',
                'review'  => '職員很有禮貌，送貨速度超快！',
                'rating'  => '5'
            ],
            [
                'img_src' => 'https://lh3.googleusercontent.com/-Z9jjH7A2nSU/AAAAAAAAAAI/AAAAAAAAAAA/TaEvDLF3DOA/w60-h60-p-rp-mo-br100/photo.jpg',
                'name'    => 'tang mamching',
                'review'  => '真心推薦🙏🏻服務態度良好！電器又平又抵！良心企業！',
                'rating'  => '5'
            ],
            [
                'img_src' => 'https://lh6.googleusercontent.com/-ysPD4RMC20w/AAAAAAAAAAI/AAAAAAAAAAA/1JZ7t8iT97M/w60-h60-p-rp-mo-br100/photo.jpg',
                'name'    => 'Eve Ng',
                'review'  => '朋友介紹，買左3件貨品，其中正負零吸塵機價格平過香港電視商場同格價網站好多；客服回應查詢、改單、發貨都很迅速，讚！',
                'rating'  => '5'
            ],
            [
                'img_src' => 'https://lh5.googleusercontent.com/-riyT2aJW3RY/AAAAAAAAAAI/AAAAAAAAAAA/YU10g9hiEas/w60-h60-p-rp-mo-br100/photo.jpg',
                'name'    => 'Chris Yip',
                'review'  => 'Good services, just try their best to help you. Goods arrive and same as the picture what I had ordered from Websites. Trustable online seller.',
                'rating'  => '5'
            ],
            [
                'img_src' => 'https://lh3.googleusercontent.com/-qgEIkVsKSEA/AAAAAAAAAAI/AAAAAAAAAAA/H8xUc5ToFYY/w60-h60-p-rp-mo-br100/photo.jpg',
                'name'    => 'Toby Lau',
                'review'  => '呢度購物平過出面，兼係行貨有保養，貨物種類同大型百記有得比:P 仲買到日本24 beauty bar 😀 多得朋友介紹 😌 服務態度良好，讚！',
                'rating'  => '5'
            ],
            [
                'img_src' => 'https://lh3.googleusercontent.com/-QDymuFXILl0/AAAAAAAAAAI/AAAAAAAAAAA/mcKT-BUR8jE/w60-h60-p-rp-mo-br100/photo.jpg',
                'name'    => 'Kay Lee',
                'review'  => '店員耐心解答客人問題,售後服務能即時回覆以及當日解決客人疑難.第一次購買友和,會再其次光顧.',
                'rating'  => '5'
            ],
            // [
            //     'img_src' => 'https://lh4.googleusercontent.com/-iXCaPqddsvM/AAAAAAAAAAI/AAAAAAAAAAA/o5x5_tnEX2E/w60-h60-p-rp-mo-br100/photo.jpg',
            //     'name'    => 'Alan Tse',
            //     'review'  => 'A very thorough and comprehensive range of merchandises selling by YOHO Hong Kong online. It shows a good business model of O2O that enable me to spend during festival time.',
            //     'rating'  => '5'
            // ],
            // [
            //     'img_src' => 'https://lh4.googleusercontent.com/-aa9z53na7Nc/AAAAAAAAAAI/AAAAAAAAAAA/wn89NKlr1rk/w60-h60-p-rp-mo-ba3-br100/photo.jpg',
            //     'name'    => 'Janet Lam',
            //     'review'  => 'Very good shop with lots of new technology stuff on discounted price.
            //     Staff is quick and responds within a couple of minutes if you send them a message on the phone. Worth making the trouble to pay at the ATM before going to pick up the product as you can save a bit of money by doing so.',
            //     'rating'  => '5'
            // ],
            [
                'img_src'   => 'https://lh3.googleusercontent.com/-tUYOLuCf9OM/AAAAAAAAAAI/AAAAAAAAAAA/8Xj3pG7xMU0/w60-h60-p-rp-mo-br100/photo.jpg',
                'name'      => 'Frankie Pang',
                'review'    => '價錢實惠 ，員工態度十分友善， 讚！',
                'rating'    => '5',
            ],
            [
                'img_src'   => 'https://lh3.googleusercontent.com/-81CqpobK-hA/AAAAAAAAAAI/AAAAAAAAAAA/MGpaK-dLZcA/w60-h60-p-rp-mo-br100/photo.jpg',
                'name'      => 'Yp Chan',
                'review'    => '服務態度超級好,貨品又靚質素又好',
                'rating'    => '5',
            ],
            [
                'img_src'   => 'https://lh6.googleusercontent.com/-yre4FjV4nmU/AAAAAAAAAAI/AAAAAAAAAAA/R0s-RnITZhM/w60-h60-p-rp-mo-br100/photo.jpg',
                'name'      => 'Vivian Chan',
                'review'    => '職員服務態度有禮 ! 主動與客人溝通 !',
                'rating'    => '5',
            ],
            [
                'img_src'   => 'https://lh5.googleusercontent.com/-kiW1hqYv3XA/AAAAAAAAAAI/AAAAAAAAAAA/2NZD--JnkcY/w60-h60-p-rp-mo-br100/photo.jpg',
                'name'      => 'Dove Wong Ka Ho',
                'review'    => '最新最多野賣',
                'rating'    => '5',
            ],
            [
                'img_src'   => 'https://lh4.googleusercontent.com/-Jd5iH4G0LHg/AAAAAAAAAAI/AAAAAAAAAAA/OwrycmUElmk/w60-h60-p-rp-mo-br100/photo.jpg',
                'name'      => 'Dave Chau',
                'review'    => '店長殷勤親切，貨品也很新很齊，價錢如Apple也比其他鋪頭平，非其他一般電子店可比。',
                'rating'    => '5',
            ],
            [
                'img_src'   => 'https://lh6.googleusercontent.com/-ysPD4RMC20w/AAAAAAAAAAI/AAAAAAAAAAA/1JZ7t8iT97M/w60-h60-p-rp-mo-br100/photo.jpg',
                'name'      => 'Eve Ng',
                'review'    => '朋友介紹，買左3件貨品，其中正負零吸塵機價格平過香港電視商場同格價網站好多；客服回應查詢、改單、發貨都很迅速，讚！',
                'rating'    => '5',
            ],
        ];
    
        $facebook_review = [
            [
                'img_src'   => 'https://graph.facebook.com/1510570167/picture?width=50&height=50',
                'name'      => 'Katrina Wong',
                'review'    => '服務態度好，職員能解決我對產品的疑惑，是一次很愉快的購物經歷！very good！會再來',
                'rating'    => '5',
            ],
            [
                'img_src'   => 'https://graph.facebook.com/529587559/picture?width=50&height=50',
                'name'      => 'Cody Kam',
                'review'    => '服務好！員工態度好！種類多！價錢平！值得再回購！',
                'rating'    => '5',
            ],
            [
                'img_src'   => 'https://graph.facebook.com/100013158673345/picture?width=50&height=50',
                'name'      => '&nbsp;周文欣&nbsp;',
                'review'    => '購物很方便也很有保障！',
                'rating'    => '5',
            ],
            [
                'img_src'   => 'https://graph.facebook.com/1373067427/picture?width=50&height=50',
                'name'      => 'Sau Mei Ip',
                'review'    => '很滿意服務，價錢非常合理',
                'rating'    => '5',
            ],
            [
                'img_src'   => 'https://graph.facebook.com/573202068/picture?width=50&height=50',
                'name'      => 'Tina Tso',
                'review'    => '產品齊全，員工又友善',
                'rating'    => '5',
            ],
            [
                'img_src'   => 'https://graph.facebook.com/1517220786/picture?width=50&height=50',
                'name'      => 'Kwan Kwan Tam',
                'review'    => '尋日落單，今早就送到，昨天還在擔心，客服有問有答，非常專業，很滿意的一次購物',
                'rating'    => '5',
            ],
            [
                'img_src'   => 'https://graph.facebook.com/711824547/picture?width=50&height=50',
                'name'      => 'Candy Loi',
                'review'    => '產品多，齊全，經濟實惠',
                'rating'    => '5',
            ],
            [
                'img_src'   => 'https://graph.facebook.com/672215959/picture?width=50&height=50',
                'name'      => 'Fai Wong',
                'review'    => '真係要讚下，尋日先登記做用戶，三點幾落ORDER落銀行過數，四點幾已經出咗貨，今朝食晏前就收貨，又快又方便。',
                'rating'    => '5',
            ],
            [
                'img_src'   => 'https://graph.facebook.com/1731987383/picture?width=50&height=50',
                'name'      => 'Tong Wang',
                'review'    => 'Staff always reacts fast  Shopped online several times already; perfect service!',
                'rating'    => '5',
            ],
            /* [
                 'img_src'   => 'https://graph.facebook.com/1336182160/picture?width=50&height=50',
                 'name'      => 'Tracy Wan',
                 'review'    => 'Fast reply and delivery, good product with nice price. I have already bought 2 dehumidifiers, 2 vacuum cleaners and a shower head from yoho. I highly recommend yoho to my frds.',
                 'rating'    => '5',
             ],*/
            [
                'img_src'   => 'https://graph.facebook.com/533516694/picture?width=50&height=50',
                'name'      => 'Rainbow HK',
                'review'    => 'Fast delivery, excellent product with good price!',
                'rating'    => '5',
            ],
            [
                'img_src'   => 'https://graph.facebook.com/705702598/picture?width=50&height=50',
                'name'      => 'Christy Li',
                'review'    => 'Very good customer service. Nice shopping experience!',
                'rating'    => '5',
            ],
        ];
    } else {
        $google_review = [
            // [
            //     'img_src'  => 'https://lh4.googleusercontent.com/-2c9p9fl1rzw/AAAAAAAAAAI/AAAAAAAAAAA/YzoNpr7qUZs/w60-h60-p-rp-mo-br100/photo.jpg',
            //     'name'     => 'annaannakwok Kwok',
            //     'review'   => '職員好好人服務態度一流，回覆快又有禮貌，令我可是以放心購物～門市同網上購物時電器選擇多元化，夠新夠齊全，同一種產品有齊多種牌子俾我哋選擇，輕鬆搜尋到心水產品省是時快捷，而且價錢夠實惠，更會不時推出優惠，合符經濟原則，購物有信心保證令人滿意，友和Yoho係一間好有心嘅鋪，為我帶嚟網上購物新體驗，一定會向朋友大力推介！',
            //     'rating'   => '5'
            // ],
            [
                'img_src' => '/data/review_img/g01.jpg',
                'name'    => 'chu tony',
                'review'  => '最新電子潮流產品，家用電器亦幾多。',
                'rating'  => '5'
            ],
            [
                'img_src' => '/data/review_img/g02.jpg',
                'name'    => 'Cathy Ng',
                'review'  => '職員很有禮貌，送貨速度超快！',
                'rating'  => '5'
            ],
            [
                'img_src' => '/data/review_img/g03.jpg',
                'name'    => 'tang mamching',
                'review'  => '真心推薦🙏🏻服務態度良好！電器又平又抵！良心企業！',
                'rating'  => '5'
            ],
            [
               'img_src' => '/data/review_img/g04.jpg',
                'name'    => 'Eve Ng',
                'review'  => '朋友介紹，買左3件貨品，其中正負零吸塵機價格平過香港電視商場同格價網站好多；客服回應查詢、改單、發貨都很迅速，讚！',
                'rating'  => '5'
            ],
            [
                'img_src' => '/data/review_img/g05.jpg',
                'name'    => 'Chris Yip',
                'review'  => 'Good services, just try their best to help you. Goods arrive and same as the picture what I had ordered from Websites. Trustable online seller.',
                'rating'  => '5'
            ],
            [
                'img_src' => '/data/review_img/g06.jpg',
                'name'    => 'Toby Lau',
                'review'  => '呢度購物平過出面，兼係行貨有保養，貨物種類同大型百記有得比:P 仲買到日本24 beauty bar 😀 多得朋友介紹 😌 服務態度良好，讚！',
                'rating'  => '5'
            ],
            [
                'img_src' => '/data/review_img/g07.jpg',
                'name'    => 'Kay Lee',
                'review'  => '店員耐心解答客人問題,售後服務能即時回覆以及當日解決客人疑難.第一次購買友和,會再其次光顧.',
                'rating'  => '5'
            ],
            // [
            //     'img_src' => 'https://lh4.googleusercontent.com/-iXCaPqddsvM/AAAAAAAAAAI/AAAAAAAAAAA/o5x5_tnEX2E/w60-h60-p-rp-mo-br100/photo.jpg',
            //     'name'    => 'Alan Tse',
            //     'review'  => 'A very thorough and comprehensive range of merchandises selling by YOHO Hong Kong online. It shows a good business model of O2O that enable me to spend during festival time.',
            //     'rating'  => '5'
            // ],
            // [
            //     'img_src' => 'https://lh4.googleusercontent.com/-aa9z53na7Nc/AAAAAAAAAAI/AAAAAAAAAAA/wn89NKlr1rk/w60-h60-p-rp-mo-ba3-br100/photo.jpg',
            //     'name'    => 'Janet Lam',
            //     'review'  => 'Very good shop with lots of new technology stuff on discounted price.
            //     Staff is quick and responds within a couple of minutes if you send them a message on the phone. Worth making the trouble to pay at the ATM before going to pick up the product as you can save a bit of money by doing so.',
            //     'rating'  => '5'
            // ],
            [
                'img_src' => '/data/review_img/g08.jpg',
                'name'      => 'Frankie Pang',
                'review'    => '價錢實惠 ，員工態度十分友善， 讚！',
                'rating'    => '5',
            ],
            [
                'img_src' => '/data/review_img/g09.jpg',
                'name'      => 'Yp Chan',
                'review'    => '服務態度超級好,貨品又靚質素又好',
                'rating'    => '5',
            ],
            [
                'img_src' => '/data/review_img/g10.jpg',
                'name'      => 'Vivian Chan',
                'review'    => '職員服務態度有禮 ! 主動與客人溝通 !',
                'rating'    => '5',
            ],
            [
                'img_src' => '/data/review_img/g11.jpg',
                'name'      => 'Dove Wong Ka Ho',
                'review'    => '最新最多野賣',
                'rating'    => '5',
            ],
            [
                'img_src' => '/data/review_img/g12.jpg',
                'name'      => 'Dave Chau',
                'review'    => '店長殷勤親切，貨品也很新很齊，價錢如Apple也比其他鋪頭平，非其他一般電子店可比。',
                'rating'    => '5',
            ],
            [
                'img_src' => '/data/review_img/g13.jpg',
                'name'      => 'Eve Ng',
                'review'    => '朋友介紹，買左3件貨品，其中正負零吸塵機價格平過香港電視商場同格價網站好多；客服回應查詢、改單、發貨都很迅速，讚！',
                'rating'    => '5',
            ],
        ];
    
        $facebook_review = [
            [
                'img_src' => '/data/review_img/f01.jpg',
                'name'      => 'Katrina Wong',
                'review'    => '服務態度好，職員能解決我對產品的疑惑，是一次很愉快的購物經歷！very good！會再來',
                'rating'    => '5',
            ],
            [
                'img_src' => '/data/review_img/f02.jpg',
                'name'      => 'Cody Kam',
                'review'    => '服務好！員工態度好！種類多！價錢平！值得再回購！',
                'rating'    => '5',
            ],
            [
                'img_src' => '/data/review_img/f03.jpg',
                'name'      => '&nbsp;周文欣&nbsp;',
                'review'    => '購物很方便也很有保障！',
                'rating'    => '5',
            ],
            [
                'img_src' => '/data/review_img/f04.jpg',
                'name'      => 'Sau Mei Ip',
                'review'    => '很滿意服務，價錢非常合理',
                'rating'    => '5',
            ],
            [
                'img_src' => '/data/review_img/f05.jpg',
                'name'      => 'Tina Tso',
                'review'    => '產品齊全，員工又友善',
                'rating'    => '5',
            ],
            [
                'img_src' => '/data/review_img/f06.jpg',
                'name'      => 'Kwan Kwan Tam',
                'review'    => '尋日落單，今早就送到，昨天還在擔心，客服有問有答，非常專業，很滿意的一次購物',
                'rating'    => '5',
            ],
            [
                'img_src' => '/data/review_img/f07.jpg',
                'name'      => 'Candy Loi',
                'review'    => '產品多，齊全，經濟實惠',
                'rating'    => '5',
            ],
            [
                'img_src' => '/data/review_img/f08.jpg',
                'name'      => 'Fai Wong',
                'review'    => '真係要讚下，尋日先登記做用戶，三點幾落ORDER落銀行過數，四點幾已經出咗貨，今朝食晏前就收貨，又快又方便。',
                'rating'    => '5',
            ],
            [
                'img_src' => '/data/review_img/f09.jpg',
                'name'      => 'Tong Wang',
                'review'    => 'Staff always reacts fast  Shopped online several times already; perfect service!',
                'rating'    => '5',
            ],
            /* [
                 'img_src'   => 'https://graph.facebook.com/1336182160/picture?width=50&height=50',
                 'name'      => 'Tracy Wan',
                 'review'    => 'Fast reply and delivery, good product with nice price. I have already bought 2 dehumidifiers, 2 vacuum cleaners and a shower head from yoho. I highly recommend yoho to my frds.',
                 'rating'    => '5',
             ],*/
            [
                'img_src' => '/data/review_img/f10.jpg',
                'name'      => 'Rainbow HK',
                'review'    => 'Fast delivery, excellent product with good price!',
                'rating'    => '5',
            ],
            [
                'img_src' => '/data/review_img/f11.jpg',
                'name'      => 'Christy Li',
                'review'    => 'Very good customer service. Nice shopping experience!',
                'rating'    => '5',
            ],
        ];
    }

    $g_random_keys = array_rand($google_review, 5);
    $fb_random_keys = array_rand($facebook_review, 10);
    $rand_id = array_merge($g_random_keys, $fb_random_keys);
    $display_review = [];

    foreach($g_random_keys as $g_k) {
        $google_review[$g_k]['platform'] = 'google';
        $display_review[] = $google_review[$g_k];
    }

    foreach($fb_random_keys as $f_k) {
        $facebook_review[$f_k]['platform'] = 'facebook';
        $display_review[] = $facebook_review[$f_k];
    }
    shuffle($display_review);
    $smarty->assign('index_review', $display_review);

    // Obtain smarty output
    $output = $smarty->fetch('index_review.dwt');
    $output = $smarty->process_inserts($output);

    return $output;
}

function insert_flashdeal_timer()
{
    global $smarty, $ecs, $db;

    $flashdealController = new \Yoho\cms\Controller\FlashdealController();
    $time = $flashdealController->getCartFisrtExpired();
    if($time['expired'] != '') $smarty->assign('flashdeal_timer', $time);
    $output = $smarty->fetch('library/flashdealCartTimer.lbi');
    $output = $smarty->process_inserts($output);

    return $output;
}
function insert_flashdeal_widget($arr)
{
    global $smarty, $_LANG, $ecs, $db;
    $type = empty($arr['type']) ? 'index' : trim($arr['type']);
    $id   = intval($arr['id']);
    $fleashdealController = new Yoho\cms\Controller\FlashdealController();
    $arr['return_output'] = !isset($arr['return_output'])? true : $arr['return_output'];
    
    // Index page
    if($type == 'index') {
        $flashdeals = $fleashdealController->get_flashdeals_list(6, true);
        $sub_title = $flashdeals['current_time_session']['time_format'];
    } else if ($type == 'goods') { // Goods page
        // If goods already assigned to smarty, use it
        $goods = $smarty->get_template_vars('goods');
      
        $goods = ($goods) ? $goods : $db->getRow('SELECT goods_id, cat_id FROM '.$ecs->table('goods').' WHERE goods_id = '.$id);
        $flashdeals = $fleashdealController->get_flashdeals_list(5, true, 1, '', '', $goods);
        $sub_title = _L('flashdeal_widget_relate_goods', '相關產品');
    }
    if(empty($flashdeals)) return '';
    $smarty->assign('flashdeals', $flashdeals);
    $smarty->assign('title', _L('flashdeal_widget_todays_deal', '限時閃購'));
    $smarty->assign('sub_title', $sub_title);
    $output = $smarty->fetch('library/flashdeal_widget.lbi');
    $output = $smarty->process_inserts($output);

    if ($arr['return_output']) {
        return $output;  
    }
}

function insert_now_gmtime()
{
    return gmtime();
}
function insert_treasure_code($arr)
{
    global $_CFG, $db, $ecs;
    // if (!in_array($_SESSION['user_id'], [155600, 9481, 6796, 39, 160113]) && strpos($_SERVER['SERVER_NAME'], 'beta.') === false) {
    //     return "";
    // }
    $real_ip = real_ip();
    $platform = empty($arr['mobile']) ? "common" : "mobile";

    $now_time = gmtime();
    if (empty($_SESSION['max_end_time'])) {
        $_SESSION['max_end_time'] = $db->getOne("SELECT MAX(end_time) FROM " . $ecs->table("treasure") . " WHERE start_time <= $now_time AND end_time > $now_time AND coupon_id NOT IN ($_CFG[treasure_promise_id])");
    }

    if ($real_ip == "0.0.0.0" || empty($real_ip) || !defined("SESS_ID") || SESS_ID == "SESS_ID" || is_spider() || empty($_SESSION['max_end_time']) || $_SESSION['max_end_time'] < $now_time) {
        unset($_SESSION['treasure']);
        return "";
    }

    if (empty($_CFG['treasure_start']) || $_CFG['treasure_start'] == 2) {
        if (empty($_SESSION['treasure'])) {
            return $_CFG['treasure_start'] == 2 ? "<script>$(function(){YOHO.$platform.treasureOverallNotify('" . insert_treasure_notice() . "');})</script>" : "";
        }
    }

    insert_user_treasure();
    return "<script>$(function(){YOHO.$platform.treasureNotify(true);YOHO.$platform.treasureOverallNotify('" . insert_treasure_notice() . "');})</script>";
}

function insert_user_treasure()
{
    global $ecs, $db, $_CFG;
    // if (!in_array($_SESSION['user_id'], [155600, 9481, 6796, 39, 160113])) {
    //     return 0;
    // }
    usleep(rand(500, 1000));
    $real_ip = real_ip();
    if ($real_ip == "0.0.0.0" || empty($real_ip)) {
        return 0;
    }
    if (empty($_CFG['treasure_start']) || $_CFG['treasure_start'] == 2) {
        if (empty($_SESSION['treasure'])) {
            return 0;
        }
    }
    $blacklist_id = [154956, 221924, 234329, 247287];
    $blacklist_ip = ['112.118.71.46', '1.64.113.183', '58.177.83.238'];
    if (!defined("SESS_ID") || SESS_ID == "SESS_ID" || is_spider() || in_array($_SESSION['user_id'], $blacklist_id) || in_array($real_ip, $blacklist_ip)) {
        if (!is_spider()) {
            $msg = "$real_ip: " . local_date("Y-m-d H:i:s");
            $db->query("INSERT INTO `dem_test_abnormal` (`log`) VALUES ('$msg')");
        }
        if (!empty($_SESSION['treasure'])) {
            $db->query("UPDATE " . $ecs->table("treasure") . " SET ip = null, session_id = null, open_time = 0, confirm = 0 WHERE treasure_id = " . $_SESSION['treasure']['treasure_id']);
            unset($_SESSION['treasure']);
        }
        return 0;
    }
    $now_time = gmtime();
    $wait_unlock_time = strpos($_SERVER['SERVER_NAME'], 'beta.') !== false ? 5 : 60;
    $wait_expire_time = strpos($_SERVER['SERVER_NAME'], 'beta.') !== false ? 960 : 15 * 60;

    if (rand(1, 100) == 1) {
        $e_time = $now_time - $wait_expire_time;
        if (!empty($_CFG['treasure_promise_id'])) {
            $db->query("DELETE FROM " . $ecs->table("treasure") . " WHERE start_time <= $now_time AND end_time > $now_time AND confirm = 0 AND open_time < $e_time AND coupon_id IN ($_CFG[treasure_promise_id])");
        }
        $db->query("UPDATE " . $ecs->table("treasure") . " SET ip = null, session_id = null, open_time = 0, confirm = 0 WHERE start_time <= $now_time AND end_time > $now_time AND confirm = 0 AND open_time < $e_time");
    }

    // get coupon if views > (10 - 15)
    if (empty($_SESSION['max_end_time'])) {
        return 0;
    }
    if (empty($_SESSION['treasure_promise']) && !empty($_CFG['treasure_promise_id']) && $now_time < $_SESSION['max_end_time'] && !empty($_SESSION['max_end_time'])) {
        $treasure_ids = $db->getCol("SELECT treasure_id FROM " . $ecs->table("treasure") . " WHERE start_time <= $now_time AND end_time > $now_time AND coupon_id IN ($_CFG[treasure_promise_id]) AND ip = '$real_ip' ORDER BY open_time DESC");
        $_SESSION['treasure_promise'] = [
            "count"         => strpos($_SERVER['SERVER_NAME'], 'beta.') !== false ? [5, 10, 15] : [rand(10, 15), rand(30, 35), rand(50, 55)],
            "urls"          => [],
            "claim"         => count($treasure_ids),
            "treasure_id"   => $treasure_ids[0],
            "is_login"      => empty($_SESSION['user_id']) ? false : true,
        ];
        treasure_has_free_gift($now_time);
    }
    if (!empty($_SESSION['user_id']) && $_SESSION['treasure_promise']['is_login'] == false && $_SESSION['treasure_promise']['free_count'] != 0) {
        $_SESSION['treasure_promise']['is_login'] = true;
        treasure_has_free_gift($now_time);
    }

    if (!empty($_SESSION['treasure'])) {
        if ($now_time > $_SESSION['treasure']['expire_time']) {
            $db->query("UPDATE " . $ecs->table("treasure") . " SET ip = null, session_id = null, open_time = 0, confirm = 0 WHERE treasure_id = " . $_SESSION['treasure']['treasure_id']);
            unset($_SESSION['treasure']);
        }
    }

    if (!empty($_SESSION['treasure'])) {
        $_SESSION['treasure']['close_time'] = $_SESSION['treasure']['expire_time'] - $now_time;
        if ($_SESSION['treasure']['close_time'] < 0 ) {
            $_SESSION['treasure']['close_time'] = 0;
        }
        if ($_SESSION['treasure']['unlock_time'] == -1) {
            $_SESSION['treasure']['step'] = "found";
            // return 1; // never click unlock
        } elseif ($now_time < $_SESSION['treasure']['unlock_time']) {
            $_SESSION['treasure']['step'] = "unlock";
            $_SESSION['treasure']['remain_time'] = $_SESSION['treasure']['unlock_time'] - $now_time;
            // return 2; // clicked unlock
        } else {
            if ($_SESSION['treasure']['reward'] == 0) {
                $_SESSION['treasure']['step'] = "no_prize";
            } else {
                $_SESSION['treasure']['step'] = "prize";
            }
            // return 3; // can open
        }
        // return "'$now_time " . $_SESSION['treasure']['unlock_time'] . "'";
    } else {
        $start_second = 21 * 60 * 60;
        $duration = 86400;
        $start_time = local_strtotime("today") + $start_second;
        if ($start_time > $now_time) {
            $start_time -= $duration;
        }
        $end_time = $start_time + $duration - 1;

        $page_split = ["/product", "/promotion", "/post", "/cat", "/brand"];
        $page_group = ["/search.php", "/flashdeal", "/user.php", "/programs", "/article.php", "/vip"];
        $uri = trim($_SERVER['REQUEST_URI']);
        $uri_modified = false;

        foreach ($page_split as $page) {
            if (strpos($uri, $page) !== false) {
                $uri = explode("-", $uri)[0];
                $uri_modified = true;
            }
        }
        foreach ($page_group as $page) {
            if (strpos($uri, $page) !== false) {
                $uri = $page;
                $uri_modified = true;
            }
        }
        if (!$uri_modified && strpos($uri, "?") !== false) {
            $uri = explode("?", $uri)[0];
        }
        // Check if treasure not open for same session
        $treasures = $db->getRow("SELECT * FROM " . $ecs->table("treasure") . " WHERE start_time <= $now_time AND end_time > $now_time AND ip = '$real_ip' AND confirm = 0");
        // Check if this page has treasure
        if (empty($treasures)) {
            $treasures = $db->getRow("SELECT * FROM " . $ecs->table("treasure") . " WHERE start_time <= $now_time AND end_time > $now_time AND path = '$uri' AND open_time = 0 AND confirm = 0 AND NOT(coupon_id != 0 AND treasure_desc = '')");
        }
        // Check if unique page view
        if (empty($treasures) && !empty($_CFG['treasure_promise_id'])) {
            $treasure_promise_ids = explode(",", $_CFG['treasure_promise_id']);
            $treasure_promise_details = explode(",", $_CFG['treasure_promise_detail']);
            if ($now_time < $_SESSION['max_end_time'] && !empty($_SESSION['max_end_time'])) {
                $current_target_index = $_SESSION['treasure_promise']['claim'] % count($_SESSION['treasure_promise']['count']);
                if (count($_SESSION['treasure_promise']['urls']) >= $_SESSION['treasure_promise']['free_count'] && $_SESSION['treasure_promise']['free_count'] > 0) {
                    if (strpos($uri, "/product") !== false) {
                        $goods_id = preg_replace("/\/product\/(\d+)/", "$1", $uri);
                        $cat_id = $db->getOne("SELECT cat_id FROM " . $ecs->table("goods") . " WHERE goods_id = '$goods_id'");
                    }
                    if (!(empty($_SESSION['user_id']) || empty($_SESSION['rank_points']) || $_SESSION['rank_points'] < 3000) && !empty($cat_id) && in_array($cat_id, $_SESSION['treasure_promise']['free_cats'])) {
                        $treasures = $db->getRow("SELECT * FROM " . $ecs->table("treasure") . " WHERE start_time <= $now_time AND end_time > $now_time AND open_time = 0 AND confirm = 0 AND coupon_id != 0 AND treasure_desc = ''");
                    }
                    if (empty($treasures)) {
                        $_SESSION['treasure_promise']['free_count'] = 0;
                        $_SESSION['treasure_promise']['free_cats'] = [];
                        $_SESSION['treasure_promise']['urls'] = [];
                    }
                } elseif (count($_SESSION['treasure_promise']['urls']) >= $_SESSION['treasure_promise']['count'][$current_target_index] && ($_SESSION['treasure_promise']['claim'] < count($_SESSION['treasure_promise']['count']) || ($_SESSION['treasure_promise']['claim'] >= count($_SESSION['treasure_promise']['count']) && $_SESSION['treasure_promise']['free_count'] == 0))) {
                    if ($_SESSION['treasure_promise']['claim'] == 0 || rand(1, 100) + $_SESSION['treasure_promise']['claim'] < 60) {
                        $treasure_promise_index = $_SESSION['treasure_promise']['claim'] % count($treasure_promise_ids);
                        if (empty($treasure_promise_details[$treasure_promise_index])) {
                            $treasure_name = "$20優惠券";
                            $treasure_desc = "購物滿$500即可使用";
                        } else {
                            $detail = explode(":", $treasure_promise_details[$treasure_promise_index]);
                            $treasure_name = $detail[0];
                            $treasure_desc = $detail[1];
                        }
                        $db->query("INSERT INTO " . $ecs->table("treasure") . " (treasure_name, treasure_desc, treasure_tnc, path, coupon_id, start_time, end_time) VALUES ('$treasure_name', '$treasure_desc', '', '$uri', '" . $treasure_promise_ids[$treasure_promise_index] . "', $start_time, $end_time)");
                    } else {
                        $db->query("INSERT INTO " . $ecs->table("treasure") . " (treasure_name, treasure_desc, treasure_tnc, path, coupon_id, start_time, end_time) VALUES ('禮物名', '', '', '$uri', '0', $start_time, $end_time)");
                    }
                    $_SESSION['treasure_promise']['treasure_id'] = $db->insert_id();
                    $treasures = $db->getRow("SELECT * FROM " . $ecs->table("treasure") . " WHERE treasure_id = " . $_SESSION['treasure_promise']['treasure_id']);
                } elseif (!in_array($uri, $_SESSION['treasure_promise']['urls'])) {
                    $_SESSION['treasure_promise']['urls'][] = $uri;
                }
            }
        }

        if ($treasures) {
            $open_time = empty($treasures['open_time']) ? $now_time : $treasures['open_time'];
            $treasure_tnc = empty($treasures['treasure_tnc']) ? "" : ("<li>" . implode("</li><li>", explode("|", $treasures['treasure_tnc'])) . "</li>");
            $_SESSION['treasure'] = [
                'treasure_id'   => $treasures['treasure_id'],
                'treasure_name' => $treasures['treasure_name'],
                'treasure_desc' => $treasures['treasure_desc'],
                'treasure_tnc' => (empty($treasures['treasure_desc']) ? "" : "<li>$treasures[treasure_desc]</li>") . $treasure_tnc,
                'coupon_code'   => "",
                'unlock_time'   => -1,
                'reward'        => 0,
                'expire_time'   => $open_time + $wait_expire_time,
                'remain_time'   => $wait_expire_time,
                'close_time'    => $wait_expire_time,
                'step'          => 'found',
                'open_time'     => $open_time
            ];
            $db->query("UPDATE " . $ecs->table("treasure") . " SET ip = '" . real_ip() . "', session_id = '" . SESS_ID . "', open_time = $open_time WHERE treasure_id = " . $treasures['treasure_id']);
            if (!empty($treasures['coupon_id'])) {
                $coupon = $db->getRow("SELECT coupon_code, start_time, end_time FROM " . $ecs->table("coupons") . " WHERE coupon_id = $treasures[coupon_id]");
                if (!empty($coupon)) {
                    $_SESSION['treasure']['coupon_code'] = $coupon['coupon_code'];
                    $now_time = local_strtotime("now");
                    if ($now_time < intval($coupon['start_time'])) {
                        $_SESSION['treasure']['coupon_date'] = local_date("Y年m月d日", $coupon['start_time']);
                    } else {
                        $_SESSION['treasure']['coupon_date'] = "即日";
                    }
                    $_SESSION['treasure']['coupon_date'] .= " 至 " . local_date("Y年m月d日", $coupon['end_time'] - 86400);
                    $_SESSION['treasure']['reward'] = 1;
                    if (empty($treasures['treasure_desc'])) {
                        $_SESSION['treasure']['reward'] = 2;
                    }
                }
            }
        }
    }
    return 0;
}

function insert_treasure_notice()
{
    global $_CFG;
    // if (!in_array($_SESSION['user_id'], [155600, 9481, 6796, 39, 160113])) {
    //     return "";
    // }
    $now_time = gmtime();
    if (empty($_CFG['treasure_start']) || empty($_SESSION['max_end_time']) || $_SESSION['max_end_time'] < $now_time) {
        unset($_SESSION['max_end_time']);
        unset($_SESSION['treasure_notice']);
        return "";
    }
    $start_second = 21 * 60 * 60;
    $duration = 86400;
    $start_time = local_strtotime("today") + $start_second;
    if ($start_time > $now_time) {
        $start_time -= $duration;
    }
    if ($_SESSION['treasure_notice']['start_time'] < $start_time) {
        unset($_SESSION['max_end_time']);
        unset($_SESSION['treasure_notice']);
    }
    $custom_msg = [
        "再有 <b>神秘獎品</b> 於 " . rand(1, 5) . " 分鐘前被人捕捉！",
        "Hohoho！努力去捕捉<b>聖誕禮物</b>吧！",
        // "捕捉 <b>$50現金獎賞</b>！立即行動！",
        // "開始行動！萬勿錯過 <b>$100現金獎賞</b>！",
    ];
    if (!isset($_SESSION['treasure_notice'])) {
        $_SESSION['treasure_notice'] = [
            'count' => 0,
            'last_notice' => 0,
            'start_time' => $start_time,
        ];
    }
    if ($_SESSION['treasure_notice']['count'] < 5) {
        if ($now_time - $_SESSION['treasure_notice']['last_notice'] > 5 * 60) {
            $_SESSION['treasure_notice']['count']++;
            $_SESSION['treasure_notice']['last_notice'] = $now_time;
            if ($_CFG['treasure_start'] == 2) {
                return $_CFG['treasure_notice'];
            } else {
                return $custom_msg[array_rand($custom_msg, 1)];
            }
        }
    }
    return "";
}

function treasure_has_free_gift($now_time)
{
    global $db, $ecs;

    $goods_ids = [];
    $cat_ids = [];

    $claimed_today = $db->getOne("SELECT COUNT(*) FROM " . $ecs->table("treasure") . " WHERE start_time <= $now_time AND end_time > $now_time AND coupon_id != 0 AND treasure_desc = '' AND (ip = '" . real_ip() . "' OR session_id = '" . SESS_ID . "')");
    if (!$claimed_today) {
        $treasure_paths = $db->getCol("SELECT path FROM " . $ecs->table("treasure") . " WHERE start_time <= $now_time AND end_time > $now_time AND open_time = 0 AND confirm = 0 AND coupon_id != 0 AND treasure_desc = ''");
        $_SESSION['treasure_promise']['free_count'] = empty($treasure_paths) ? 0 : (strpos($_SERVER['SERVER_NAME'], 'beta.') !== false ? 20 : rand(60, 65));
        foreach ($treasure_paths as $path) {
            if (strpos($path, "/product") !== false) {
                $goods_ids[] = preg_replace("/\/product\/(\d+)/", "$1", $path);
            }
        }
    }
    if (!empty($goods_ids)) {
        $parent_cat_ids = $db->getCol("SELECT DISTINCT parent_id FROM " . $ecs->table("category") . " c LEFT JOIN " . $ecs->table("goods") . " g ON g.cat_id = c.cat_id WHERE goods_id " . db_create_in($goods_ids));
        $cat_ids = $db->getCol("SELECT DISTINCT cat_id FROM " . $ecs->table("category") . " WHERE parent_id " . db_create_in($parent_cat_ids));
    }
    $_SESSION['treasure_promise']['free_cats'] = $cat_ids;
}

function insert_daterange_group($arr)
{
    $start_date = empty($arr["start_date"]) ? "start_date" : $arr["start_date"];
    $end_date = empty($arr["end_date"]) ? "end_date" : $arr["end_date"];
    $submit = empty($arr["submit"]) && !$arr['input'] ? "submit" : $arr["submit"];
    $controller = new Yoho\cms\Controller\YohoBaseController();
    $last_month_start = local_date("Y-m-d", local_strtotime("first day of last month"));
    $last_month_end = local_date("Y-m-d", (local_strtotime("last day of last month")));
    $last_quarter = $controller->getQarter(local_date("Y"), 'Q' . (ceil(local_date("m") / 3)), 'month');
    $last_year = $controller->getQarter(local_date("Y"), 0, 'year');

    $GLOBALS['smarty']->assign('submit', $submit);
    $GLOBALS['smarty']->assign('start_date', $start_date);
    $GLOBALS['smarty']->assign('end_date', $end_date);
    $GLOBALS['smarty']->assign('last_month_start', $last_month_start);
    $GLOBALS['smarty']->assign('last_month_end', $last_month_end);
    $GLOBALS['smarty']->assign('last_quarter', $last_quarter);
    $GLOBALS['smarty']->assign('last_year', $last_year);
    $GLOBALS['smarty']->assign('arr', $arr);
    $output = $GLOBALS['smarty']->fetch('library/form_daterange_search.htm');
    return $output;
}

function insert_category_select_area($arr)
{
    $category = $arr['category'];

    $select = "";
    
    foreach ($category as $cat)
    {
        //print_r($cat);
        if ($cat['level'] == 0){
            $select .= "<select class='category_select' id='category_select_".$cat["cat_id"]."' data-id='".$cat["cat_id"]."'>";
        }
        
        $select .= '<option value="' . $cat['cat_id'] . '" ';
        $select .= ($cat['level'] == 0) ? "selected" : '';
        $select .= '>';
        if ($cat['level'] > 0){
            $select .= str_repeat('&nbsp;', $cat['level'] * 4);
        }
        $select .= htmlspecialchars(addslashes($cat['cat_name']), ENT_QUOTES) . '</option>';

        if($cat['children']){
            $select .= insert_category_select_area(array("category"=>$cat['children']));
        }

        if ($cat['level'] == 0){
            $select .= "</select>";
        }


    }
    
    return $select;
}

function insert_mobile_category_option($arr)
{
    $category = $arr['category'];

    $select = "";
    
    foreach ($category as $cat)
    {
        $option .= '<option value="' . $cat['cat_id'] . '">';
        if ($cat['level'] > 0){
            $option .= str_repeat('&nbsp;', $cat['level'] * 4);
        }
        $option .= htmlspecialchars(addslashes($cat['cat_name']), ENT_QUOTES) . '</option>';

        if($cat['children']){
            $option .= insert_mobile_category_option(array("category"=>$cat['children']));
        }


    }
    
    return $option;
}

function insert_category_info_area($arr)
{
    $category = $arr['category'];

    $output = "";
    
    foreach ($category as $cat)
    {
        $output .= "<div class='info_select' id='info_select_".$cat['cat_id']."'></div>";
    }
    
    return $output;
}

?>
