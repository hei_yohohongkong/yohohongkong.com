<?php

/**
* ECShop YohoHongkong Development
* Base Controller
 */
namespace Yoho\Controllers;

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

class YohoBaseController{
	protected $db,$ecs;
	public function __construct(){
		global $db,$ecs;
		$this->db=$db;
		$this->ecs=$ecs;
	}
}