<?php

/**
 * ECSHOP 商品相关函数库
 * ============================================================================
 * * 版权所有 2005-2012 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liubo $
 * $Id: lib_goods.php 17217 2011-01-19 06:29:08Z liubo $
*/

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

/**
 * 商品推荐usort用自定义排序行数
 */
function goods_sort($goods_a, $goods_b)
{
    if ($goods_a['sort_order'] == $goods_b['sort_order']) {
        return 0;
    }
    return ($goods_a['sort_order'] < $goods_b['sort_order']) ? -1 : 1;

}

/**
 * 获得指定分类同级的所有分类以及该分类下的子分类
 *
 * @access  public
 * @param   integer     $cat_id     分类编号
 * @return  array
 */
function get_categories_tree($cat_id = 0)
{
    if ($cat_id > 0)
    {
        $sql = 'SELECT parent_id FROM ' . $GLOBALS['ecs']->table('category') . " WHERE cat_id = '$cat_id'";
        $parent_id = $GLOBALS['db']->getOne($sql);
    }
    else
    {
        $parent_id = 0;
    }

    /*
     判断当前分类中全是是否是底级分类，
     如果是取出底级分类上级分类，
     如果不是取当前分类及其下的子分类
    */
    $sql = 'SELECT count(*) FROM ' . $GLOBALS['ecs']->table('category') . " WHERE parent_id = '$parent_id' AND is_show = 1 ";
    if ($GLOBALS['db']->getOne($sql) || $parent_id == 0)
    {
        /* 获取当前分类及其子分类 */
        $sql = 'SELECT cat_id,cat_name ,parent_id,is_show, cperma ' .
                'FROM ' . $GLOBALS['ecs']->table('category') . ' c ' .
                'LEFT JOIN (SELECT id, table_name, lang, perma_link as cperma FROM ' . $GLOBALS['ecs']->table('perma_link') . ') cpl ON cpl.id = c.cat_id  AND cpl.table_name = "category" AND cpl.lang = "' . $GLOBALS['_CFG']['lang'] . '"' .
                "WHERE parent_id = '$parent_id' AND is_show = 1 ORDER BY sort_order DESC, cat_id ASC";

        $res = $GLOBALS['db']->getAll($sql);

        foreach ($res AS $row)
        {
            if ($row['is_show'])
            {
                $cat_arr[$row['cat_id']]['id']   = $row['cat_id'];
                $cat_arr[$row['cat_id']]['name'] = $row['cat_name'];
                $cat_arr[$row['cat_id']]['url']  = build_uri('category', array('cid' => $row['cat_id'], 'cperma' => $row['cperma']), $row['cat_name']);

                if (isset($row['cat_id']) != NULL)
                {
                    $cat_arr[$row['cat_id']]['cat_id'] = get_child_tree($row['cat_id']);
                }
            }
        }
    }
    if(isset($cat_arr))
    {
        return $cat_arr;
    }
}

function get_child_tree($tree_id = 0)
{
    $three_arr = array();
    $sql = 'SELECT count(*) FROM ' . $GLOBALS['ecs']->table('category') . " WHERE parent_id = '$tree_id' AND is_show = 1 ";
    if ($GLOBALS['db']->getOne($sql) || $tree_id == 0)
    {
        $child_sql = 'SELECT cat_id, cat_name, parent_id, is_show ' .
                'FROM ' . $GLOBALS['ecs']->table('category') .
                "WHERE parent_id = '$tree_id' AND is_show = 1 ORDER BY sort_order DESC, cat_id ASC";
        $res = $GLOBALS['db']->getAll($child_sql);
        foreach ($res AS $row)
        {
            if ($row['is_show'])

               $three_arr[$row['cat_id']]['id']   = $row['cat_id'];
               $three_arr[$row['cat_id']]['name'] = $row['cat_name'];
               $three_arr[$row['cat_id']]['url']  = build_uri('category', array('cid' => $row['cat_id']), $row['cat_name']);

               if (isset($row['cat_id']) != NULL)
                   {
                       $three_arr[$row['cat_id']]['cat_id'] = get_child_tree($row['cat_id']);

            }
        }
    }
    return $three_arr;
}

/**
 * 调用当前分类的销售排行榜
 *
 * @access  public
 * @param   string  $cats   查询的分类
 * @return  array
 */
function get_top10($cats = '')
{
    $cats = get_children($cats);
    $where = !empty($cats) ? "AND ($cats OR " . get_extension_goods($cats) . ") " : '';

    /* 排行统计的时间 */
    switch ($GLOBALS['_CFG']['top10_time'])
    {
        case 1: // 一年
            $top10_time = "AND o.order_sn >= '" . date('Ymd', gmtime() - 365 * 86400) . "'";
        break;
        case 2: // 半年
            $top10_time = "AND o.order_sn >= '" . date('Ymd', gmtime() - 180 * 86400) . "'";
        break;
        case 3: // 三个月
            $top10_time = "AND o.order_sn >= '" . date('Ymd', gmtime() - 90 * 86400) . "'";
        break;
        case 4: // 一个月
            $top10_time = "AND o.order_sn >= '" . date('Ymd', gmtime() - 30 * 86400) . "'";
        break;
        default:
            $top10_time = '';
    }

    $sql = 'SELECT g.goods_id, g.goods_name, g.shop_price, g.goods_thumb, SUM(og.goods_number) as goods_number ' .
           'FROM ' . $GLOBALS['ecs']->table('goods') . ' AS g, ' .
                $GLOBALS['ecs']->table('order_info') . ' AS o, ' .
                $GLOBALS['ecs']->table('order_goods') . ' AS og ' .
           "WHERE g.is_on_sale = 1 AND g.is_alone_sale = 1 AND g.is_delete = 0 AND g.is_hidden = 0 $where $top10_time " ;
    //判断是否启用库存，库存数量是否大于0
    if ($GLOBALS['_CFG']['use_storage'] == 1)
    {
        $sql .= " AND g.goods_number > 0 ";
    }
    $sql .= ' AND og.order_id = o.order_id AND og.goods_id = g.goods_id ' .
           "AND (o.order_status = '" . OS_CONFIRMED .  "' OR o.order_status = '" . OS_SPLITED . "') " .
           "AND (o.pay_status = '" . PS_PAYED . "' OR o.pay_status = '" . PS_PAYING . "') " .
           "AND (o.shipping_status = '" . SS_SHIPPED . "' OR o.shipping_status = '" . SS_RECEIVED . "') " .
           'GROUP BY g.goods_id ORDER BY goods_number DESC, g.goods_id DESC LIMIT ' . $GLOBALS['_CFG']['top_number'];
           
    $arr = $GLOBALS['db']->getAll($sql);

    for ($i = 0, $count = count($arr); $i < $count; $i++)
    {
        $arr[$i]['short_name'] = $GLOBALS['_CFG']['goods_name_length'] > 0 ?
                                    sub_str($arr[$i]['goods_name'], $GLOBALS['_CFG']['goods_name_length']) : $arr[$i]['goods_name'];
        $arr[$i]['url']        = build_uri('goods', array('gid' => $arr[$i]['goods_id']), $arr[$i]['goods_name']);
        $arr[$i]['thumb'] = get_image_path($arr[$i]['goods_id'], $arr[$i]['goods_thumb'],true);
        $arr[$i]['price'] = price_format($arr[$i]['shop_price']);
    }

    return $arr;
}

/**
 * 获得推荐商品
 *
 * @access  public
 * @param   string      $type       推荐类型，可以是 best, new, hot
 * @return  array
 */
function get_recommend_goods($type = '', $cats = '')
{
    if (!in_array($type, array('best', 'new', 'hot')))
    {
        return array();
    }

    //取不同推荐对应的商品
    static $type_goods = array();
    if (empty($type_goods[$type]))
    {
        //初始化数据
        $type_goods['best'] = array();
        $type_goods['new'] = array();
        $type_goods['hot'] = array();
        $data = read_static_cache('recommend_goods');
        if ($data === false)
        {
            $sql = 'SELECT g.goods_id, g.is_best, g.is_new, g.is_hot, g.is_promote, b.brand_name,g.sort_order ' .
               ' FROM ' . $GLOBALS['ecs']->table('goods') . ' AS g ' .
               ' LEFT JOIN ' . $GLOBALS['ecs']->table('brand') . ' AS b ON b.brand_id = g.brand_id ' .
               ' WHERE g.is_on_sale = 1 AND g.is_alone_sale = 1 AND g.is_delete = 0 AND g.is_hidden = 0 ' .
               ' AND (g.is_best = 1 OR g.is_new =1 OR g.is_hot = 1)'.
               ' ORDER BY g.sort_order DESC, g.last_update DESC';
            $goods_res = $GLOBALS['db']->getAll($sql);
            //定义推荐,最新，热门，促销商品
            $goods_data['best'] = array();
            $goods_data['new'] = array();
            $goods_data['hot'] = array();
            $goods_data['brand'] = array();
            if (!empty($goods_res))
            {
                foreach($goods_res as $data)
                {
                    if ($data['is_best'] == 1)
                    {
                        $goods_data['best'][] = array('goods_id' => $data['goods_id'], 'sort_order' => $data['sort_order']);
                    }
                    if ($data['is_new'] == 1)
                    {
                        $goods_data['new'][] = array('goods_id' => $data['goods_id'], 'sort_order' => $data['sort_order']);
                    }
                    if ($data['is_hot'] == 1)
                    {
                        $goods_data['hot'][] = array('goods_id' => $data['goods_id'], 'sort_order' => $data['sort_order']);
                    }
                    if ($data['brand_name'] != '')
                    {
                        $goods_data['brand'][$data['goods_id']] = $data['brand_name'];
                    }
                }
            }
            write_static_cache('recommend_goods', $goods_data);
        }
        else
        {
            $goods_data = $data;
        }

        $time = gmtime();
        $order_type = $GLOBALS['_CFG']['recommend_order'];

        //按推荐数量及排序取每一项推荐显示的商品 order_type可以根据后台设定进行各种条件显示
        static $type_array = array();
        $type2lib = array('best'=>'recommend_best', 'new'=>'recommend_new', 'hot'=>'recommend_hot');
        if (empty($type_array))
        {
            foreach($type2lib as $key => $data)
            {
                if (!empty($goods_data[$key]))
                {
                    $num = get_library_number($data);
                    $data_count = count($goods_data[$key]);
                    $num = $data_count > $num  ? $num : $data_count;
                    if ($order_type == 0)
                    {
                        //usort($goods_data[$key], 'goods_sort');
                        $rand_key = array_slice($goods_data[$key], 0, $num);
                        foreach($rand_key as $key_data)
                        {
                            $type_array[$key][] = $key_data['goods_id'];
                        }
                    }
                    else
                    {
                        $rand_key = array_rand($goods_data[$key], $num);
                        if ($num == 1)
                        {
                            $type_array[$key][] = $goods_data[$key][$rand_key]['goods_id'];
                        }
                        else
                        {
                            foreach($rand_key as $key_data)
                            {
                                $type_array[$key][] = $goods_data[$key][$key_data]['goods_id'];
                            }
                        }
                    }
                }
                else
                {
                    $type_array[$key] = array();
                }
            }
        }

        //取出所有符合条件的商品数据，并将结果存入对应的推荐类型数组中
        $sql = 'SELECT g.goods_id, g.goods_name, g.goods_name_style, g.market_price, g.shop_price AS org_price, g.promote_price, ' .
                "IFNULL(mp.user_price, g.shop_price * '$_SESSION[discount]') AS shop_price, ".
                "promote_start_date, promote_end_date, g.goods_brief, g.goods_thumb, g.goods_img, RAND() AS rnd " .
                'FROM ' . $GLOBALS['ecs']->table('goods') . ' AS g ' .
                "LEFT JOIN " . $GLOBALS['ecs']->table('member_price') . " AS mp ".
                "ON mp.goods_id = g.goods_id AND mp.user_rank = '$_SESSION[user_rank]' ";
        $type_merge = array_merge($type_array['new'], $type_array['best'], $type_array['hot']);
        $type_merge = array_unique($type_merge);
        $sql .= ' WHERE g.goods_id ' . db_create_in($type_merge);
        $sql .= ' ORDER BY g.sort_order DESC, g.last_update DESC';

        $result = $GLOBALS['db']->getAll($sql);
        foreach ($result AS $idx => $row)
        {
            if ($row['promote_price'] > 0)
            {
                $promote_price = bargain_price($row['promote_price'], $row['promote_start_date'], $row['promote_end_date']);
                $goods[$idx]['promote_price'] = $promote_price > 0 ? price_format($promote_price) : '';
            }
            else
            {
                $goods[$idx]['promote_price'] = '';
            }

            $goods[$idx]['id']           = $row['goods_id'];
            $goods[$idx]['name']         = $row['goods_name'];
            $goods[$idx]['brief']        = $row['goods_brief'];
            $goods[$idx]['brand_name']   = isset($goods_data['brand'][$row['goods_id']]) ? $goods_data['brand'][$row['goods_id']] : '';
            $goods[$idx]['goods_style_name']   = add_style($row['goods_name'],$row['goods_name_style']);

            $goods[$idx]['short_name']   = $GLOBALS['_CFG']['goods_name_length'] > 0 ?
                                               sub_str($row['goods_name'], $GLOBALS['_CFG']['goods_name_length']) : $row['goods_name'];
            $goods[$idx]['short_style_name']   = add_style($goods[$idx]['short_name'],$row['goods_name_style']);
            $goods[$idx]['market_price'] = price_format($row['market_price']);
            $goods[$idx]['shop_price']   = price_format($row['shop_price']);
            $goods[$idx]['thumb']        = get_image_path($row['goods_id'], $row['goods_thumb'], true);
            $goods[$idx]['goods_img']    = get_image_path($row['goods_id'], $row['goods_img']);
            $goods[$idx]['url']          = build_uri('goods', array('gid' => $row['goods_id']), $row['goods_name']);
            if (in_array($row['goods_id'], $type_array['best']))
            {
                $type_goods['best'][] = $goods[$idx];
            }
            if (in_array($row['goods_id'], $type_array['new']))
            {
                $type_goods['new'][] = $goods[$idx];
            }
            if (in_array($row['goods_id'], $type_array['hot']))
            {
                $type_goods['hot'][] = $goods[$idx];
            }
        }
    }
    return $type_goods[$type];
}

/**
 * 获得促销商品
 *
 * @access  public
 * @return  array
 */
function get_promote_goods($cats = '')
{
    $time = gmtime();
    $order_type = $GLOBALS['_CFG']['recommend_order'];

    /* 取得促销lbi的数量限制 */
    $num = get_library_number("recommend_promotion");
    $sql = 'SELECT g.goods_id, g.goods_name, g.goods_name_style, g.market_price, g.shop_price AS org_price, g.promote_price, ' .
                "IFNULL(mp.user_price, g.shop_price * '$_SESSION[discount]') AS shop_price, g.display_discount, ".
                "promote_start_date, promote_end_date, g.goods_brief, g.goods_thumb, goods_img, b.brand_name, " .
                "g.is_best, g.is_new, g.is_hot, g.is_promote, RAND() AS rnd " .
            'FROM ' . $GLOBALS['ecs']->table('goods') . ' AS g ' .
            'LEFT JOIN ' . $GLOBALS['ecs']->table('brand') . ' AS b ON b.brand_id = g.brand_id ' .
            "LEFT JOIN " . $GLOBALS['ecs']->table('member_price') . " AS mp ".
                "ON mp.goods_id = g.goods_id AND mp.user_rank = '$_SESSION[user_rank]' ".
            'WHERE g.is_on_sale = 1 AND g.is_alone_sale = 1 AND g.is_delete = 0 AND g.is_hidden = 0 ' .
            " AND g.is_promote = 1 AND promote_start_date <= '$time' AND promote_end_date >= '$time' ";
    $sql .= $order_type == 0 ? ' ORDER BY g.sort_order DESC, g.last_update DESC' : ' ORDER BY rnd';
    $sql .= " LIMIT $num ";
    $result = $GLOBALS['db']->getAll($sql);

    $goods = hw_process_goods_rows($result);

    return $goods;
}

/**
 * 获得指定分类下的推荐商品
 *
 * @access  public
 * @param   string      $type       推荐类型，可以是 best, new, hot, promote
 * @param   string      $cats       分类的ID
 * @param   integer     $brand      品牌的ID
 * @param   integer     $min        商品价格下限
 * @param   integer     $max        商品价格上限
 * @param   string      $ext        商品扩展查询
 * @return  array
 */
function get_category_recommend_goods($type = '', $cats = '', $brand = 0, $min =0,  $max = 0, $ext='')
{
    $brand_where = ($brand > 0) ? " AND g.brand_id = '$brand'" : '';

    $price_where = ($min > 0) ? " AND g.shop_price >= $min " : '';
    $price_where .= ($max > 0) ? " AND g.shop_price <= $max " : '';
    $commentController = new Yoho\cms\Controller\CommentController();
	$comment_type = $commentController::COMMENT_TYPE_GOODS;
    $sql =  'SELECT g.goods_id, g.goods_name,g.keywords, g.goods_name_style, g.market_price, g.shop_price AS org_price, g.promote_price, ' .
                "gperma, " .
                "IFNULL(mp.user_price, g.shop_price * '$_SESSION[discount]') AS shop_price, g.display_discount, ".
                'mp2.user_price as vip_price, '.
                'promote_start_date, promote_end_date, g.goods_brief, g.goods_thumb, goods_img, b.brand_name, ' .
                'AVG(cm.comment_rank) as rating, COUNT(cm.comment_rank) as rating_total '.
            'FROM ' . $GLOBALS['ecs']->table('goods') . ' AS g ' .
            'LEFT JOIN ' . $GLOBALS['ecs']->table('brand') . ' AS b ON b.brand_id = g.brand_id ' .
            "LEFT JOIN " . $GLOBALS['ecs']->table('member_price') . " AS mp2 ON mp2.goods_id = g.goods_id AND mp2.user_rank = 2 ". // TODO:VIP = 2
            "LEFT JOIN " . $GLOBALS['ecs']->table('member_price') . " AS mp ".
                    "ON mp.goods_id = g.goods_id AND mp.user_rank = '$_SESSION[user_rank]' ".
            "LEFT JOIN " . $GLOBALS['ecs']->table('comment') . " AS cm ON cm.id_value = g.goods_id AND cm.comment_type = $comment_type AND cm.status = 1 ".
            " LEFT JOIN (SELECT id, table_name, lang, perma_link as gperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") AS gpl ON gpl.id = g.goods_id AND gpl.table_name = 'goods' AND gpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
            'WHERE g.is_on_sale = 1 AND g.is_alone_sale = 1 AND g.is_delete = 0 AND g.is_hidden = 0 ' . $brand_where . $price_where . $ext;
    $num = 0;
    $type2lib = array('best'=>'recommend_best', 'new'=>'recommend_new', 'hot'=>'recommend_hot', 'promote'=>'recommend_promotion', 'best_sellers'=>'recommend_hot');
    $num = get_library_number($type2lib[$type]);

    switch ($type)
    {
        case 'best':
            $sql .= ' AND is_best = 1';
            break;
        case 'new':
            $sql .= ' AND is_new = 1';
            break;
        case 'hot':
            $sql .= ' AND is_hot = 1';
            break;
        case 'best_sellers':
            $sql .= ' AND is_best_sellers = 1';
            break;
        case 'promote':
            $time = gmtime();
            $sql .= " AND is_promote = 1 AND promote_start_date <= '$time' AND promote_end_date >= '$time'";
            break;
    }

    if (!empty($cats))
    {
        $sql .= " AND (" . $cats . " OR " . get_extension_goods($cats) .")";
    }

    $order_type = $GLOBALS['_CFG']['recommend_order'];
    $sql .= " GROUP BY g.goods_id ";
    $sql .= ($order_type == 0) ? ' ORDER BY g.sort_order DESC , g.last_update DESC ' : ' ORDER BY RAND() ';
    $sql .= " LIMIT $num";
    $res = $GLOBALS['db']->getAll($sql);
    $goods = hw_process_goods_rows($res);

    return $goods;
}

/**
 * 获得商品的详细信息
 *
 * @access  public
 * @param   integer     $goods_id
 * @return  void
 */
function get_goods_info($goods_id, $ignore_hidden = false)
{
    require_once ROOT_PATH . 'includes/lib_order.php';
    $goodsController = new Yoho\cms\Controller\GoodsController();
    $userRankController = new Yoho\cms\Controller\UserRankController();
    $shippingController = new Yoho\cms\Controller\ShippingController();
    $time = gmtime();
    $sql = 'SELECT g.*, c.measure_unit, m.type_money AS bonus_money, ' .
                'IFNULL(AVG(r.comment_rank), 0) AS comment_rank, ' .
                "IFNULL(mp.user_price, g.shop_price * '$_SESSION[discount]') AS rank_price, " .
                'IFNULL(gl.goods_name, g.goods_name) as goods_name, ' .
                'IFNULL(gl.goods_name_style, g.goods_name_style) as goods_name_style, ' .
                'IFNULL(gl.keywords, g.keywords) as keywords, ' .
                'IFNULL(gl.goods_brief, g.goods_brief) as goods_brief, ' .
                'IFNULL(gl.goods_desc, g.goods_desc) as goods_desc, ' .
                'CASE WHEN g.warranty_template_id = 0 THEN IFNULL(gl.seller_note, g.seller_note) ELSE IFNULL(wtl.template_content, wt.template_content) END as seller_note, ' .
                'CASE WHEN g.shipping_template_id = 0 THEN IFNULL(gl.shipping_note, g.shipping_note) ELSE IFNULL(stl.template_content, st.template_content) END as shipping_note, ' .
                'IFNULL(gl.guarantee, g.guarantee) as guarantee, ' .
                'IFNULL(gl.inthebox, g.inthebox) as inthebox, ' .
                'IFNULL(bl.brand_name, b.brand_name) as goods_brand, ' .
                'gperma, bperma, ' .
                'mp2.user_price as vip_price,'.
                "c.default_length, c.default_width, c.default_height, ".
                hw_goods_number_subquery('g.', 'real_goods_number') . ", " .
    			hw_reserved_number_subquery('g.') . " " .
            'FROM ' . $GLOBALS['ecs']->table('goods') . ' AS g ' .
            "LEFT JOIN " . $GLOBALS['ecs']->table('member_price') . " AS mp2 ON mp2.goods_id = g.goods_id AND mp2.user_rank = 2 ". // TODO:VIP = 2
            'LEFT JOIN ' . $GLOBALS['ecs']->table('goods_lang') . ' AS gl ' .
                "ON gl.goods_id = g.goods_id AND gl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
            'LEFT JOIN ' . $GLOBALS['ecs']->table('category') . ' AS c ON g.cat_id = c.cat_id ' .
            'LEFT JOIN ' . $GLOBALS['ecs']->table('brand') . ' AS b ON g.brand_id = b.brand_id ' .
            'LEFT JOIN ' . $GLOBALS['ecs']->table('brand_lang') . ' AS bl ' .
                "ON b.brand_id = bl.brand_id AND bl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
            'LEFT JOIN ' . $GLOBALS['ecs']->table('comment') . ' AS r '.
                'ON r.id_value = g.goods_id AND comment_type = 0 AND r.parent_id = 0 AND r.status = 1 ' .
            'LEFT JOIN ' . $GLOBALS['ecs']->table('bonus_type') . ' AS m ' .
                "ON g.bonus_type_id = m.type_id AND m.send_start_date <= '$time' AND m.send_end_date >= '$time'" .
            " LEFT JOIN " . $GLOBALS['ecs']->table('member_price') . " AS mp " .
                "ON mp.goods_id = g.goods_id AND mp.user_rank = '$_SESSION[user_rank]' " .
            " LEFT JOIN " . $GLOBALS['ecs']->table('warranty_templates') . " AS wt " .
                "ON wt.template_id = g.warranty_template_id " .
            " LEFT JOIN " . $GLOBALS['ecs']->table('warranty_templates_lang') . " AS wtl " .
                "ON wt.template_id = wtl.template_id AND wtl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
            " LEFT JOIN " . $GLOBALS['ecs']->table('shipping_templates') . " AS st " .
                "ON st.template_id = g.shipping_template_id " .
            " LEFT JOIN " . $GLOBALS['ecs']->table('shipping_templates_lang') . " AS stl " .
                "ON st.template_id = stl.template_id AND stl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
            " LEFT JOIN (SELECT id, table_name, lang, perma_link as bperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") AS bpl " .
                "ON bpl.id = g.brand_id AND bpl.table_name = 'brand' AND bpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
            " LEFT JOIN (SELECT id, table_name, lang, perma_link as gperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") AS gpl " .
                "ON gpl.id = g.goods_id AND gpl.table_name = 'goods' AND gpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
                "WHERE g.goods_id = '$goods_id' AND g.is_delete = 0 " . ($ignore_hidden ? '' : " AND g.is_hidden = 0 ") .
            "GROUP BY g.goods_id";
    $row = $GLOBALS['db']->getRow($sql);
    if ($row !== false)
    {
        /* 用户评论级别取整 */
        $row['comment_rank']  = ceil($row['comment_rank']) == 0 ? 5 : ceil($row['comment_rank']);

        /* TODO: Hardcode update http to https with product image */
        $row['goods_desc']     = str_replace('http://www.yohohongkong.com/images/upload/Proddb/', 'https://www.yohohongkong.com/images/upload/Proddb/', $row['goods_desc']);
        $row['goods_desc']     = str_replace('http://blog.yohohongkong.com/wp-content/uploads/', 'https://blog.yohohongkong.com/wp-content/uploads/', $row['goods_desc']);
        /* 获得商品的销售价格 */
        $row['market_price2']       = $row['market_price'];
        $row['market_price']        = price_format($row['market_price']);
        //注释掉源码$row['shop_price_formated'] = price_format($row['shop_price']);//修改为获取当前会员价
        
        $row['sbuser_name'] = get_user_rankname($_SESSION['user_id']);
        $row['shop_price'] =  $_SESSION[user_rank] ?  $row['rank_price'] : $row['shop_price'];
        $min_rank = $userRankController->getUserMinRank($goods_id, $_SESSION['user_id']);
        $min_rank = $min_rank[$goods_id];
        if($min_rank['user_price'] > 0) {
            $row['shop_price'] = min($min_rank['user_price'], $row['shop_price']);
            $row['program_name'] = $row['sbuser_name'] = $min_rank['program_name'];
            $row['is_sp_price'] = true;
            $row['can_buy'] = $min_rank['can_buy'];
            $row['program_theme_color'] = $row['price_theme_color'] = $min_rank['theme_color'];

        }
        $row['shop_price_formated'] =  price_format($row['shop_price']);//修改为获取当前会员价
        $row['vip_price']    = empty($row['vip_price']) ? false : $row['vip_price'];
        $row['vip_price_formated'] = price_format($row['vip_price']);
        $row['is_vip_price'] = $row['shop_price'] == $row['vip_price'] ? true : false;
        /* 修正促销价格 */
        if ($row['promote_price'] > 0)
        {
            $promote_price = bargain_price($row['promote_price'], $row['promote_start_date'], $row['promote_end_date']);

            if($promote_price > $row['shop_price']){ // If promote price > shop_price , don't show promote_price
                $promote_price = 0;
            } else if ($promote_price > 0) {
                $row['is_sp_price'] = false;
            }
        }
        else
        {
            $promote_price = 0;
        }
        if($row['is_vip_price'] == true && $_SESSION['user_rank'] == 2) $row['is_sp_price'] = false;
        $row['promote_price_formated'] =  price_format($promote_price);
        
        /* 促销时间倒计时 */
        $time = gmtime();
        if ($time >= $row['promote_start_date'] && $time <= $row['promote_end_date'])
        {
            $row['gmt_end_time'] = $row['promote_end_date'];
            
            // Countdown for timed sales, don't display for those always on sale product
            // "always on sale" means on sale for more than 2678400 seconds = 1 month (31 days)
            if ($row['promote_end_date'] - $row['promote_start_date'] < 2678400)
            {
                $row['gmt_now_time'] = $time;
                
                $remaining_time = $row['promote_end_date'] - $time;
                $row['promote_countdown'] = '';
                if ($remaining_time > 86400)
                {
                    $row['promote_countdown'] .= floor($remaining_time / 86400) . _L('goods_promote_day', '天');
                    $remaining_time = $remaining_time % 86400;
                }
                if ($remaining_time > 3600)
                {
                    $row['promote_countdown'] .= floor($remaining_time / 3600) . _L('goods_promote_hour', '小時');
                    $remaining_time = $remaining_time % 3600;
                }
                if ($remaining_time > 60)
                {
                    $row['promote_countdown'] .= sprintf("%02d", floor($remaining_time / 60)) . _L('goods_promote_miniute', '分');
                    $remaining_time = $remaining_time % 60;
                }
                if ($remaining_time > 0)
                {
                    $row['promote_countdown'] .= sprintf("%02d", $remaining_time) . _L('goods_promote_second', '秒');
                }
            }

            $gmt_time_diff = intval($row['gmt_end_time'] - $time);
            if($gmt_time_diff < intval('259200‬')){ //in 3 days, start countdown
                $row['start_countdowm'] = 1;
            }
            else{
                $row['start_countdowm'] = 0;
            }
        }
        else
        {
            $row['gmt_end_time'] = 0;
        }
        

        // 另收運費
        $row['shipping_price_formated'] = price_format($row['shipping_price']);
        if($promote_price > 0 && $row['gmt_end_time'] > 0){ // If have promote_price, check price.

            $price = $promote_price;
            if($row['is_vip_price'])
            {
                $price = min($promote_price, $row['vip_price']);
            }

        } else {
            $price = $row['shop_price'];
        }
        // YOHO: lowest price = 3% discount, have to pay with cash / ATM / ebanking
        $row['lowest_pirce'] = ceil($price * 0.97);
        $row['lowest_price_formated'] = price_format($row['lowest_pirce']);
 
        // Normal Savings = market_price - (shop_price or promote_price)
        $row['saved_price_normal'] = ($row['market_price2'] < $price) ? 0 : $row['market_price2'] - $price;
        $row['saved_price_normal_formated'] = price_format($row['saved_price_normal']);
        $row['saved_percentage_normal'] = (($row['market_price2'] == 0) ? '0.00' :
            round($row['saved_price_normal'] / $row['market_price2'] * 100.0, 2)) . '%';
        $row['saved_percentage_normal_rounded'] = (($row['market_price2'] == 0) ? '0' :
            round($row['saved_price_normal'] / $row['market_price2'] * 100.0)) . '%';
        // Savings = market_price - lowest_price
        $row['saved_price'] = $row['market_price2'] - $row['lowest_pirce'];
        $row['saved_price_formated'] = price_format($row['saved_price']);
        $row['saved_percentage'] = (($row['market_price2'] == 0) ? '0.00' :
            round($row['saved_price'] / $row['market_price2'] * 100.0, 2)) . '%';
        $row['saved_percentage_rounded'] = (($row['market_price2'] == 0) ? '0' :
            round($row['saved_price'] / $row['market_price2'] * 100.0)) . '%';
        
        /* 处理商品水印图片 */
        $watermark_img = '';
        
        if ($promote_price != 0)
        {
            $watermark_img = "watermark_promote";
        }
        elseif ($row['is_new'] != 0)
        {
            $watermark_img = "watermark_new";
        }
        elseif ($row['is_best'] != 0)
        {
            $watermark_img = "watermark_best";
        }
        elseif ($row['is_hot'] != 0)
        {
            $watermark_img = 'watermark_hot';
        }

        if ($watermark_img != '')
        {
            $row['watermark_img'] =  $watermark_img;
        }
        
        /* 修正重量显示 */
        $row['goods_weight']  = (intval($row['goods_weight']) > 0) ?
            $row['goods_weight'] . $GLOBALS['_LANG']['kilogram'] :
            ($row['goods_weight'] * 1000) . $GLOBALS['_LANG']['gram'];
            
        /* 修正上架时间显示 */
        $row['add_time']      = local_date($GLOBALS['_CFG']['date_format'], $row['add_time']);
        
        /* 是否显示商品库存数量 */
        //$row['goods_number']  = ($GLOBALS['_CFG']['use_storage'] == 1) ? $row['goods_number'] : '';
        
        // get sellable stock of shop only (main warehouse), no FLS warehouse
        $erpController = new Yoho\cms\Controller\ErpController();
        $shop_warehouse_id = $erpController->getShopWarehouseId(FALSE); // FALSE = get more than one record
        $main_warehouse_id = $erpController->getMainWarehouseId();
        $fls_warehouse_id = $erpController->getFlsWarehouseId();

        $goods_id = $row['goods_id'];

        $sellableStockWarehouse = $erpController->getSellableProductsQtyByWarehouses($goods_id,null,true);

        $row['goods_number_fls'] = $sellableStockWarehouse[$goods_id][$fls_warehouse_id];
        $row['goods_number_main_warehouse'] = $sellableStockWarehouse[$goods_id][$main_warehouse_id];
        
        $if_goods_in_shops = FALSE;

        $row['goods_shops'] = array();

        foreach ($shop_warehouse_id as $shop){
            $shop_id = $shop["warehouse_id"];

            $shop_name = $erpController->getWarehouseNameById($shop_id);

            array_push($row['goods_shops'], array("id" => $shop_id, "name" => $shop_name));

            $sellableQtyShop = $sellableStockWarehouse[$goods_id][$shop_id];
        
            if (empty($sellableQtyShop)) {
                $sellableQtyShop = 0;
            }

            //$sellableQtyShop = $erpController->getSellableProductQtyWithoutFLS($row['goods_id']);
            $row['goods_number_shop'][$shop_id] = $sellableQtyShop;

            if ($row['goods_number_shop'][$shop_id] > 0){
                $if_goods_in_shops = TRUE;
            } else {
                $row['goods_number_shop'][$shop_id] = 0;
            }
            if ($row['goods_number_shop'][$shop_id]  >= 5)
            {
                //$row['goods_number_sn_shop'][$shop_id] = _L('goods_stock_enough', '充足存貨');
                $row['goods_number_sn_shop'][$shop_id] = _L('goods_buy_retail_available', '充足');
            }
            elseif ($row['goods_number_shop'][$shop_id]  < 5 && $row['goods_number_shop'][$shop_id]  > 0)
            {
                //$row['goods_number_sn_shop'][$shop_id] = _L('goods_stock_few', '少量存貨');
                $row['goods_number_sn_shop'][$shop_id] = sprintf(_L('goods_buy_retail_few', '少量 (%u件)'), $row['goods_number_shop'][$shop_id]);
            }
            else
            {
                //$row['goods_number_sn_shop'][$shop_id] = _L('goods_stock_none', '暫時缺貨');
                $row['goods_number_sn_shop'][$shop_id] = "shop_out_of_stock";
            }
        }
        $row['if_goods_in_shops'] = $if_goods_in_shops;

        $stock = intval($row['real_goods_number']) - intval($row['reserved_number']);
        $stock = $stock > 0 ? $stock : 0;
        if ($stock >= 5)
        {
            $row['goods_number_sn'] = _L('goods_stock_enough', '充足存貨');
        }
        elseif ($stock < 5 && $stock > 0)
        {
            $row['goods_number_sn'] = _L('goods_stock_few', '少量存貨');
        }
        else
        {
            $row['goods_number_sn'] = _L('goods_stock_none', '暫時缺貨');
        }
        
        $row['goods_number'] = $stock;
        
        // 沒有存貨時，自動判定訂貨模式
        $row = $goodsController->getGoodsPreOrderInfo($row, $stock, 1, 1);

        foreach ($shop_warehouse_id as $shop){
            $shop_id = $shop["warehouse_id"];
            if ($row['goods_number_sn_shop'][$shop_id] == "shop_out_of_stock"){
                $row['goods_number_sn_shop'][$shop_id] = sprintf(_L('goods_buy_retail_unavailable', "暫時缺貨﹕網上落單後，預計 %u 個工作日出貨。"), ($row['shop_total_day'][$shop_id]));
            }
        }

        if(!$row['is_on_sale']){
            $sql = "SELECT * FROM " . $GLOBALS['ecs']->table('goods') . " WHERE is_on_sale = 1 AND is_delete = 0 AND is_hidden = 0 AND " .
                    "brand_id = (SELECT brand_id FROM " . $GLOBALS['ecs']->table('goods') .
                        " WHERE goods_id = " . $row['goods_id'] . ") AND cat_id = (SELECT cat_id FROM " . $GLOBALS['ecs']->table('goods') .
                        " WHERE goods_id = " . $row['goods_id'] . ") ORDER BY goods_id DESC LIMIT 8";
            $suggest = $GLOBALS['db']->getAll($sql);
            if(count($suggest) == 0){
                $sql = "SELECT * FROM " . $GLOBALS['ecs']->table('goods') . " WHERE is_on_sale = 1 AND is_delete = 0 AND is_hidden = 0 AND " .
                        "brand_id = (SELECT brand_id FROM " . $GLOBALS['ecs']->table('goods') .
                            " WHERE goods_id = " . $row['goods_id'] . ") ORDER BY goods_id DESC LIMIT 8";
                $suggest = $GLOBALS['db']->getAll($sql);
            }
            if(count($suggest) == 0){
                $sql = "SELECT * FROM " . $GLOBALS['ecs']->table('goods') . " WHERE is_on_sale = 1 AND is_delete = 0 AND is_hidden = 0 AND " .
                        "cat_id = (SELECT cat_id FROM " . $GLOBALS['ecs']->table('goods') .
                            " WHERE goods_id = " . $row['goods_id'] . ") ORDER BY goods_id DESC LIMIT 8";
                $suggest = $GLOBALS['db']->getAll($sql);
            }
            $row['suggest'] = hw_process_goods_rows($suggest);
        }

        if($row['is_sp_price'] == true) {
            $min = ($row['pre_sale_days'] || $row['can_pre_sale']) ? 20 : $row['goods_number'];
            $row['can_buy'] = min($row['can_buy'], $min);
        }

        /* 修正积分：转换为可使用多少积分（原来是可以使用多少钱的积分） */
        $row['integral']      = $GLOBALS['_CFG']['integral_scale'] ? round($row['integral'] * 100 / $GLOBALS['_CFG']['integral_scale']) : 0;
        
        /* 修正穫得的積分 by howang */
        $final_price = $promote_price > 0 ? intval($promote_price) : intval($_SESSION[user_rank] ? $row['rank_price'] : $row['shop_price']);
        $row['give_integral'] = $row['give_integral'] > -1 ? $row['give_integral'] : $final_price;

        /* 修正优惠券 */
        $row['bonus_money']   = ($row['bonus_money'] == 0) ? 0 : price_format($row['bonus_money'], false);

        /* 修正商品图片 */
        $row['goods_img']   = get_image_path($goods_id, $row['goods_img']);
        $row['goods_thumb'] = get_image_path($goods_id, $row['goods_thumb'], true);

        // Absolute URLs
        $row['goods_uri'] = build_uri('goods', array('gid'=>$goods_id, 'gname'=>$row['goods_name'], 'gperma'=>$row['gperma']));
        $row['goods_desktop_url'] = desktop_url($row['goods_uri']);
        $row['goods_mobile_url'] = mobile_url($row['goods_uri']);
        $row['goods_img_url'] = desktop_url($row['goods_img']);
        
        // Language URLs
        $row['goods_lang_urls'] = array();
        foreach (available_languages() as $_lang)
        {
            $sql = "SELECT IFNULL(gl.goods_name, g.goods_name) as goods_name, gperma " .
                    "FROM " . $GLOBALS['ecs']->table('goods') . " as g " .
                    "LEFT JOIN " . $GLOBALS['ecs']->table('goods_lang') . " as gl " .
                        "ON gl.goods_id = g.goods_id AND gl.lang = '" . $_lang . "' " .
                    "LEFT JOIN (SELECT id, table_name, lang, perma_link as gperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") gpl ON gpl.id = g.goods_id  AND gpl.table_name = 'goods' AND gpl.lang = '" . $_lang . "' " .
                    "WHERE g.goods_id = '" . $goods_id . "'";
            $goods_localized = $GLOBALS['db']->getRow($sql);
            $row['goods_lang_urls'][$_lang] = absolute_url('/' . $_lang . build_uri('goods', array('gid'=>$goods_id, 'gname'=>$goods_localized['goods_name'], 'gperma'=>$goods_localized['gperma']), '', 0, '', 0, $_lang));
        }
        
        // 自訂欄位
        $sql = "SELECT * FROM " . $GLOBALS['ecs']->table('goods_extra_info') . " WHERE `goods_id` = '" . $goods_id . "' ORDER BY `info_order` ASC";
        $row['extra_info_list'] = $GLOBALS['db']->getAll($sql);
        
        // 原產地
        if ($row['country_of_origin'] == '日本')
        {
            $row['country_of_origin_code'] = 'jp';
        }
        elseif ($row['country_of_origin'] == '美國')
        {
            $row['country_of_origin_code'] = 'us';
        }
        elseif ($row['country_of_origin'] == '韓國')
        {
            $row['country_of_origin_code'] = 'ko';
        }
        elseif ($row['country_of_origin'] == '歐洲')
        {
            $row['country_of_origin_code'] = 'eu';
        }
        elseif ($row['country_of_origin'] == '德國')
        {
            $row['country_of_origin_code'] = 'de';
        }
        else // Country of origin not set or unknown
        {
            $row['country_of_origin'] = '';
        }
        if (!empty($row['country_of_origin']))
        {
            $row['country_of_origin_name'] = _L('country_of_origin_' . $row['country_of_origin_code'], $row['country_of_origin']);
        }
        // 推廣鏈結
        $sql = "SELECT * FROM " . $GLOBALS['ecs']->table('goods_promo_links') . " WHERE `goods_id` = '" . $goods_id . "' ORDER BY `link_order` ASC";
        $row['promo_links_list'] = $GLOBALS['db']->getAll($sql);
        
        // 著數優惠
        $sql = "SELECT t.*, tperma FROM " . $GLOBALS['ecs']->table('topic') .  
               " t LEFT JOIN (SELECT id, table_name, lang, perma_link as tperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") acpl ON acpl.id = t.topic_id  AND acpl.table_name = 'topic' AND acpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
                "WHERE (" . gmtime() . " BETWEEN start_time AND end_time + 86399) " .
                "AND `data` LIKE '%|" . $goods_id . "\"%' " .
                "ORDER BY `sort_order` ASC,`topic_id` ASC";
        $row['topic_list'] = $GLOBALS['db']->getAll($sql);

        foreach ($row['topic_list'] as $key => $topic)
        {
            if (!file_exists(ROOT_PATH . DATA_DIR . '/afficheimg/topic/' . $topic['topic_id'] . '_icon.png'))
            {
                unset($row['topic_list'][$key]);
                continue;
            }

            $row['topic_list'][$key]['title'] = nl2br(trim($topic['title']));
            $row['topic_list'][$key]['description'] = nl2br(trim($topic['description']));
            $row['topic_list'][$key]['url'] = build_uri('topic', array('tid' => $topic['topic_id'],'tperma'=>$topic['tperma']));
            $row['topic_list'][$key]['icon'] = DATA_DIR . '/afficheimg/topic/' . $topic['topic_id'] . '_icon.png';
        }
        // 銷量
        $row['sells'] = $row['sales'] + $row['sales_adjust'];
        
        // Google Structured Data
        $google_structured_data = array(
            '@context' => 'http://schema.org/',
            '@type' => 'Product',
            'name' => $row['goods_name'],
            'image' => $row['goods_img_url'],
            'description' => $row['goods_brief'],
            'brand' => array(
                '@type' => 'Thing',
                'name' => $row['goods_brand'],
            ),
            'offers' => array(
                '@type' => 'Offer',
                'priceCurrency' => 'HKD',
                'price' => $promote_price,
                'priceValidUntil' => local_date('Y-m-d', $row['promote_end_date']),
                'availability' => 'http://schema.org/' . (($row['availability'])? $row['availability'] : ($stock > 0 ? 'InStock' : 'OutOfStock')),
                'seller' => array(
                    '@type' => 'Organization',
                    'name' => '友和 YOHO'
                ),
                'url' => absolute_url($row['goods_uri'])
            ),
        );
        if (!$row['goods_brand'])
        {
            unset($google_structured_data['brand']);
        }
        if ($promote_price <= 0)
        {
            $google_structured_data['offers']['price'] = $row['shop_price'];
            unset($google_structured_data['offers']['priceValidUntil']);
        }
        $commentController = new Yoho\cms\Controller\CommentController();
        $rating_info = $commentController->getGoodsRatingInfo($row['goods_id']);
        if($rating_info['avg_rating'] > 0) {
            $google_structured_data['aggregateRating']['@type'] ='AggregateRating';
            $google_structured_data['aggregateRating']['ratingValue'] = $rating_info['avg_rating'];
            $google_structured_data['aggregateRating']['reviewCount'] = $rating_info['total'];
        }
        $row['google_structured_data_json'] = json_encode($google_structured_data);
        $row['width']  = (empty(floatval($row['goods_width'])))  ? $row['default_width']  : $row['goods_width'];
        $row['height'] = (empty(floatval($row['goods_height']))) ? $row['default_height'] : $row['goods_height'];
        $row['length'] = (empty(floatval($row['goods_length']))) ? $row['default_length'] : $row['goods_length'];
        $shipping_list = available_shipping_list(3409, 'pickuppoint');
        foreach ($shipping_list AS $key => $val)
        {
            $shipping_cfg = unserialize_config($val['configure']);
            /* If shipping have w x h x l, cal w x h x l */
            if($shipping_cfg['max_width'] && $shipping_cfg['max_height'] && $shipping_cfg['max_length']) {
                /* If any locker can put this product, we set can_locker is true and break the loop. */
                $row['can_locker'] = !$shippingController->cal_shipping_volumeric_oversize($row, $shipping_cfg);
                if($row['can_locker'] == true) break;
            }
        }
        return $row;
    }
    else
    {
        return false;
    }
}

/**
 * 获得商品的属性和规格
 *
 * @access  public
 * @param   integer $goods_id
 * @return  array
 */
function get_goods_properties($goods_id)
{
    /* 对属性进行重新排序和分组 */
    $sql = "SELECT attr_group ".
            "FROM " . $GLOBALS['ecs']->table('goods_type') . " AS gt, " . $GLOBALS['ecs']->table('goods') . " AS g ".
            "WHERE g.goods_id='$goods_id' AND gt.cat_id=g.goods_type";
    $grp = $GLOBALS['db']->getOne($sql);
    $i=0; //01判断循环次数  LY SBSN 130306
    if (!empty($grp))
    {
        $groups = explode("\n", strtr($grp, "\r", ''));
    }

    /* 获得商品的规格 */
    $sql = "SELECT a.attr_id, a.attr_name, a.attr_group, a.is_linked, a.attr_type, ".
                "g.goods_attr_id, g.attr_value, g.attr_price " .
            'FROM ' . $GLOBALS['ecs']->table('goods_attr') . ' AS g ' .
            'LEFT JOIN ' . $GLOBALS['ecs']->table('attribute') . ' AS a ON a.attr_id = g.attr_id ' .
            "WHERE g.goods_id = '$goods_id' " .
            'ORDER BY a.sort_order, g.attr_price, g.goods_attr_id';
    $res = $GLOBALS['db']->getAll($sql);

    $arr['pro'] = array();     // 属性
    $arr['spe'] = array();     // 规格
    $arr['lnk'] = array();     // 关联的属性

    foreach ($res AS $row)
    {
        $row['attr_value'] = str_replace("\n", '<br />', $row['attr_value']);

        if ($row['attr_type'] == 0)
        {
             $i++; //3-02判断循环次数  LY SBSN 130306
            $group = (isset($groups[$row['attr_group']])) ? $groups[$row['attr_group']] : $GLOBALS['_LANG']['goods_attr'];
            $arr['pro'][$group][$row['attr_id']]['i'] = $i+2; //3-03判断循环次数  LY SBSN 130306
            $arr['pro'][$group][$row['attr_id']]['name']  = $row['attr_name'];
            $arr['pro'][$group][$row['attr_id']]['value'] = $row['attr_value'];
        }
        else
        {
            $spec =array($row['goods_attr_id']);
            
            $anumber=get_products_info($goods_id,$spec);
            $z=$anumber['product_number']; //增加属性显示库存 sbsn 20130808
            //echo $z;//增加属性显示库存sbsn 20130808
            
            $arr['spe'][$row['attr_id']]['attr_id'] = $row['attr_id'];//判断显示的属性id
            $arr['spe'][$row['attr_id']]['attr_type'] = $row['attr_type'];
            $arr['spe'][$row['attr_id']]['name']     = $row['attr_name'];
            $arr['spe'][$row['attr_id']]['values'][] = array(
                                                        'label'        => $row['attr_value'],
                                                        'price'        => $row['attr_price'],
                                                        'format_price' => price_format(abs($row['attr_price']), false),
                                                        'id'           => $row['goods_attr_id'],
                                                        'anumber' =>$z//增加属性显示库存sbsn 20130808
                                                        );
        }

        if ($row['is_linked'] == 1)
        {
            /* 如果该属性需要关联，先保存下来 */
            $arr['lnk'][$row['attr_id']]['name']  = $row['attr_name'];
            $arr['lnk'][$row['attr_id']]['value'] = $row['attr_value'];
        }
    }


    return $arr;
}

/**
 * 获得属性相同的商品
 *
 * @access  public
 * @param   array   $attr   // 包含了属性名称,ID的数组
 * @return  array
 */
function get_same_attribute_goods($attr)
{
    $lnk = array();

    if (!empty($attr))
    {
        foreach ($attr['lnk'] AS $key => $val)
        {
            $lnk[$key]['title'] = sprintf($GLOBALS['_LANG']['same_attrbiute_goods'], $val['name'], $val['value']);

            /* 查找符合条件的商品 */
            $sql = 'SELECT g.goods_id, g.goods_name, g.goods_thumb, g.goods_img, g.shop_price AS org_price, ' .
                        "IFNULL(mp.user_price, g.shop_price * '$_SESSION[discount]') AS shop_price, ".
                        'g.market_price, g.promote_price, g.promote_start_date, g.promote_end_date ' .
                    'FROM ' . $GLOBALS['ecs']->table('goods') . ' AS g ' .
                    'LEFT JOIN ' . $GLOBALS['ecs']->table('goods_attr') . ' as a ON g.goods_id = a.goods_id ' .
                    "LEFT JOIN " . $GLOBALS['ecs']->table('member_price') . " AS mp ".
                        "ON mp.goods_id = g.goods_id AND mp.user_rank = '$_SESSION[user_rank]' ".
                    "WHERE a.attr_id = '$key' AND a.attr_value = '$val[value]' AND g.goods_id <> '$_REQUEST[id]' " .
                    "AND g.is_on_sale = 1 AND g.is_delete = 0 AND g.is_hidden = 0 ".
                    'LIMIT ' . $GLOBALS['_CFG']['attr_related_number'];
            $res = $GLOBALS['db']->getAll($sql);
            $res = localize_db_result('goods', $res);

            foreach ($res AS $row)
            {
                $lnk[$key]['goods'][$row['goods_id']]['goods_id']      = $row['goods_id'];
                $lnk[$key]['goods'][$row['goods_id']]['goods_name']    = $row['goods_name'];
                $lnk[$key]['goods'][$row['goods_id']]['short_name']    = $GLOBALS['_CFG']['goods_name_length'] > 0 ?
                    sub_str($row['goods_name'], $GLOBALS['_CFG']['goods_name_length']) : $row['goods_name'];
                $lnk[$key]['goods'][$row['goods_id']]['goods_thumb']     = (empty($row['goods_thumb'])) ? $GLOBALS['_CFG']['no_picture'] : $row['goods_thumb'];
                $lnk[$key]['goods'][$row['goods_id']]['market_price']  = price_format($row['market_price']);
                $lnk[$key]['goods'][$row['goods_id']]['shop_price']    = price_format($row['shop_price']);
                $lnk[$key]['goods'][$row['goods_id']]['promote_price'] = bargain_price($row['promote_price'],
                    $row['promote_start_date'], $row['promote_end_date']);
                $lnk[$key]['goods'][$row['goods_id']]['url']           = build_uri('goods', array('gid' => $row['goods_id']), $row['goods_name']);
            }
        }
    }

    return $lnk;
}

/**
 * 获得指定商品的相册
 *
 * @access  public
 * @param   integer     $goods_id
 * @return  array
 */
function get_goods_gallery($goods_id)
{
    $sql = 'SELECT img_id, img_url, thumb_url, img_desc' .
        ' FROM ' . $GLOBALS['ecs']->table('goods_gallery') .
        " WHERE goods_id = '$goods_id'" .
        " ORDER BY sort_order " .
        " LIMIT " . $GLOBALS['_CFG']['goods_gallery_number'];
    $row = $GLOBALS['db']->getAll($sql);
    /* 格式化相册图片路径 */
    foreach($row as $key => $gallery_img)
    {
        $row[$key]['img_url'] = get_image_path($goods_id, $gallery_img['img_url'], false, 'gallery');
        $row[$key]['thumb_url'] = get_image_path($goods_id, $gallery_img['thumb_url'], true, 'gallery');
    }
    return $row;
}

/**
 * 获得指定分类下的商品
 *
 * @access  public
 * @param   integer     $cat_id     分类ID
 * @param   integer     $num        数量
 * @param   string      $from       来自web/wap的调用
 * @param   string      $order_rule 指定商品排序规则
 * @return  array
 */
function assign_cat_goods($cat_id, $num = 0, $from = 'web', $order_rule = '')
{
    $children = get_children($cat_id);

    $sql = 'SELECT g.goods_id, g.goods_name, g.market_price, g.shop_price AS org_price, ' .
                "IFNULL(mp.user_price, g.shop_price * '$_SESSION[discount]') AS shop_price, ".
               'g.promote_price, promote_start_date, promote_end_date, g.goods_brief, g.goods_thumb, g.goods_img ' .
            "FROM " . $GLOBALS['ecs']->table('goods') . ' AS g '.
            "LEFT JOIN " . $GLOBALS['ecs']->table('member_price') . " AS mp ".
                    "ON mp.goods_id = g.goods_id AND mp.user_rank = '$_SESSION[user_rank]' ".
            'WHERE g.is_on_sale = 1 AND g.is_alone_sale = 1 AND '.
                'g.is_delete = 0 AND g.is_hidden = 0 AND (' . $children . 'OR ' . get_extension_goods($children) . ') ';

    $order_rule = empty($order_rule) ? 'ORDER BY g.sort_order DESC, g.goods_id DESC' : $order_rule;
    $sql .= $order_rule;
    if ($num > 0)
    {
        $sql .= ' LIMIT ' . $num;
    }
    $res = $GLOBALS['db']->getAll($sql);

    $goods = array();
    foreach ($res AS $idx => $row)
    {
        if ($row['promote_price'] > 0)
        {
            $promote_price = bargain_price($row['promote_price'], $row['promote_start_date'], $row['promote_end_date']);
            $goods[$idx]['promote_price'] = $promote_price > 0 ? price_format($promote_price) : '';
        }
        else
        {
            $goods[$idx]['promote_price'] = '';
        }

        $goods[$idx]['id']           = $row['goods_id'];
        $goods[$idx]['name']         = $row['goods_name'];
        $goods[$idx]['brief']        = $row['goods_brief'];
        $goods[$idx]['market_price'] = price_format($row['market_price']);
        $goods[$idx]['short_name']   = $GLOBALS['_CFG']['goods_name_length'] > 0 ?
                                        sub_str($row['goods_name'], $GLOBALS['_CFG']['goods_name_length']) : $row['goods_name'];
        $goods[$idx]['shop_price']   = price_format($row['shop_price']);
        $goods[$idx]['thumb']        = get_image_path($row['goods_id'], $row['goods_thumb'], true);
        $goods[$idx]['goods_img']    = get_image_path($row['goods_id'], $row['goods_img']);
        $goods[$idx]['url']          = build_uri('goods', array('gid' => $row['goods_id']), $row['goods_name']);
    }

    if ($from == 'web')
    {
        $GLOBALS['smarty']->assign('cat_goods_' . $cat_id, $goods);
    }
    elseif ($from == 'wap')
    {
        $cat['goods'] = $goods;
    }

    /* 分类信息 */
    $sql = 'SELECT cat_name FROM ' . $GLOBALS['ecs']->table('category') . " WHERE cat_id = '$cat_id'";
    $cat['name'] = $GLOBALS['db']->getOne($sql);
    $cat['url']  = build_uri('category', array('cid' => $cat_id), $cat['name']);
    $cat['id']   = $cat_id;

    return $cat;
}

/**
 * 获得指定的品牌下的商品
 *
 * @access  public
 * @param   integer     $brand_id       品牌的ID
 * @param   integer     $num            数量
 * @param   integer     $cat_id         分类编号
 * @param   string      $order_rule     指定商品排序规则
 * @return  void
 */
function assign_brand_goods($brand_id, $num = 0, $cat_id = 0,$order_rule = '')
{
    $sql =  'SELECT g.goods_id, g.goods_name, g.market_price, g.shop_price AS org_price, ' .
                "IFNULL(mp.user_price, g.shop_price * '$_SESSION[discount]') AS shop_price, ".
                'g.promote_price, g.promote_start_date, g.promote_end_date, g.goods_brief, g.goods_thumb, g.goods_img ' .
            'FROM ' . $GLOBALS['ecs']->table('goods') . ' AS g ' .
            "LEFT JOIN " . $GLOBALS['ecs']->table('member_price') . " AS mp ".
                    "ON mp.goods_id = g.goods_id AND mp.user_rank = '$_SESSION[user_rank]' ".
            "WHERE g.is_on_sale = 1 AND g.is_alone_sale = 1 AND g.is_delete = 0 AND g.is_hidden = 0 AND g.brand_id = '$brand_id'";

    if ($cat_id > 0)
    {
        $sql .= get_children($cat_id);
    }

    $order_rule = empty($order_rule) ? ' ORDER BY g.sort_order DESC, g.goods_id DESC' : $order_rule;
    $sql .= $order_rule;
    if ($num > 0)
    {
        $res = $GLOBALS['db']->selectLimit($sql, $num);
    }
    else
    {
        $res = $GLOBALS['db']->query($sql);
    }

    $idx = 0;
    $goods = array();
    while ($row = $GLOBALS['db']->fetchRow($res))
    {
        if ($row['promote_price'] > 0)
        {
            $promote_price = bargain_price($row['promote_price'], $row['promote_start_date'], $row['promote_end_date']);
        }
        else
        {
            $promote_price = 0;
        }

        $goods[$idx]['id']            = $row['goods_id'];
        $goods[$idx]['name']          = $row['goods_name'];
        $goods[$idx]['short_name']    = $GLOBALS['_CFG']['goods_name_length'] > 0 ?
            sub_str($row['goods_name'], $GLOBALS['_CFG']['goods_name_length']) : $row['goods_name'];
        $goods[$idx]['market_price']  = price_format($row['market_price']);
        $goods[$idx]['shop_price']    = price_format($row['shop_price']);
        $goods[$idx]['promote_price'] = $promote_price > 0 ? price_format($promote_price) : '';
        $goods[$idx]['brief']         = $row['goods_brief'];
        $goods[$idx]['thumb']         = get_image_path($row['goods_id'], $row['goods_thumb'], true);
        $goods[$idx]['goods_img']     = get_image_path($row['goods_id'], $row['goods_img']);
        $goods[$idx]['url']           = build_uri('goods', array('gid' => $row['goods_id']), $row['goods_name']);

        $idx++;
    }

    /* 分类信息 */
    $sql = 'SELECT brand_name FROM ' . $GLOBALS['ecs']->table('brand') . " WHERE brand_id = '$brand_id'";

    $brand['id']   = $brand_id;
    $brand['name'] = $GLOBALS['db']->getOne($sql);
    $brand['url']  = build_uri('brand', array('bid' => $brand_id), $brand['name']);

    $brand_goods = array('brand' => $brand, 'goods' => $goods);

    return $brand_goods;
}

/**
 * 获得所有扩展分类属于指定分类的所有商品ID
 *
 * @access  public
 * @param   string $cat_id     分类查询字符串
 * @return  string
 */
function get_extension_goods($cats)
{
    $extension_goods_array = '';
    $sql = 'SELECT goods_id FROM ' . $GLOBALS['ecs']->table('goods_cat') . " AS g WHERE $cats";
    $extension_goods_array = $GLOBALS['db']->getCol($sql);
    return db_create_in($extension_goods_array, 'g.goods_id');
}

/**
 * 判断某个商品是否正在特价促销期
 *
 * @access  public
 * @param   float   $price      促销价格
 * @param   string  $start      促销开始日期
 * @param   string  $end        促销结束日期
 * @return  float   如果还在促销期则返回促销价，否则返回0
 */
function bargain_price($price, $start, $end)
{
    if ($price == 0)
    {
        return 0;
    }
    else
    {
        $time = gmtime();
        if ($time >= $start && $time <= $end)
        {
            return $price;
        }
        else
        {
            return 0;
        }
    }
}

/**
 * 获得指定的规格的价格
 *
 * @access  public
 * @param   mix     $spec   规格ID的数组或者逗号分隔的字符串
 * @return  void
 */
function spec_price($spec)
{
    if (!empty($spec))
    {
    //更新11月11日 官方更新代码s
        if(is_array($spec))
        {
            foreach($spec as $key=>$val)
            {
                $spec[$key]=addslashes($val);
            }
        }
        else
        {
            $spec=addslashes($spec);
        }
	//更新11月11日 官方更新代码END

        $where = db_create_in($spec, 'goods_attr_id');

        $sql = 'SELECT SUM(attr_price) AS attr_price FROM ' . $GLOBALS['ecs']->table('goods_attr') . " WHERE $where";
        $price = floatval($GLOBALS['db']->getOne($sql));
    }
    else
    {
        $price = 0;
    }

    return $price;
}

/**
 * 取得团购活动信息
 * @param   int     $group_buy_id   团购活动id
 * @param   int     $current_num    本次购买数量（计算当前价时要加上的数量）
 * @return  array
 *                  status          状态：
 */
function group_buy_info($group_buy_id, $current_num = 0)
{
    /* 取得团购活动信息 */
    $group_buy_id = intval($group_buy_id);
    $sql = "SELECT *, act_id AS group_buy_id, act_desc AS group_buy_desc, start_time AS start_date, end_time AS end_date " .
            "FROM " . $GLOBALS['ecs']->table('goods_activity') .
            "WHERE act_id = '$group_buy_id' " .
            "AND act_type = '" . GAT_GROUP_BUY . "'";
    $group_buy = $GLOBALS['db']->getRow($sql);

    /* 如果为空，返回空数组 */
    if (empty($group_buy))
    {
        return array();
    }

    $ext_info = unserialize($group_buy['ext_info']);
    $group_buy = array_merge($group_buy, $ext_info);

    /* 格式化时间 */
    $group_buy['formated_start_date'] = local_date('Y-m-d H:i', $group_buy['start_time']);
    $group_buy['formated_end_date'] = local_date('Y-m-d H:i', $group_buy['end_time']);

    /* 格式化保证金 */
    $group_buy['formated_deposit'] = price_format($group_buy['deposit'], false);

    /* 处理价格阶梯 */
    $price_ladder = $group_buy['price_ladder'];
    if (!is_array($price_ladder) || empty($price_ladder))
    {
        $price_ladder = array(array('amount' => 0, 'price' => 0));
    }
    else
    {
        foreach ($price_ladder as $key => $amount_price)
        {
            $price_ladder[$key]['formated_price'] = price_format($amount_price['price'], false);
        }
    }
    $group_buy['price_ladder'] = $price_ladder;

    /* 统计信息 */
    $stat = group_buy_stat($group_buy_id, $group_buy['deposit']);
    $group_buy = array_merge($group_buy, $stat);

    /* 计算当前价 */
    $cur_price  = $price_ladder[0]['price']; // 初始化
    $cur_amount = $stat['valid_goods'] + $current_num; // 当前数量
    foreach ($price_ladder as $amount_price)
    {
        if ($cur_amount >= $amount_price['amount'])
        {
            $cur_price = $amount_price['price'];
        }
        else
        {
            break;
        }
    }
    $group_buy['cur_price'] = $cur_price;
    $group_buy['formated_cur_price'] = price_format($cur_price, false);

    /* 最终价 */
    $group_buy['trans_price'] = $group_buy['cur_price'];
    $group_buy['formated_trans_price'] = $group_buy['formated_cur_price'];
    $group_buy['trans_amount'] = $group_buy['valid_goods'];

    /* 状态 */
    $group_buy['status'] = group_buy_status($group_buy);
    if (isset($GLOBALS['_LANG']['gbs'][$group_buy['status']]))
    {
        $group_buy['status_desc'] = $GLOBALS['_LANG']['gbs'][$group_buy['status']];
    }

    $group_buy['start_time'] = $group_buy['formated_start_date'];
    $group_buy['end_time'] = $group_buy['formated_end_date'];

    return $group_buy;
}

/*
 * 取得某团购活动统计信息
 * @param   int     $group_buy_id   团购活动id
 * @param   float   $deposit        保证金
 * @return  array   统计信息
 *                  total_order     总订单数
 *                  total_goods     总商品数
 *                  valid_order     有效订单数
 *                  valid_goods     有效商品数
 */
function group_buy_stat($group_buy_id, $deposit)
{
    $group_buy_id = intval($group_buy_id);

    /* 取得团购活动商品ID */
    $sql = "SELECT goods_id " .
           "FROM " . $GLOBALS['ecs']->table('goods_activity') .
           "WHERE act_id = '$group_buy_id' " .
           "AND act_type = '" . GAT_GROUP_BUY . "'";
    $group_buy_goods_id = $GLOBALS['db']->getOne($sql);

    /* 取得总订单数和总商品数 */
    $sql = "SELECT COUNT(*) AS total_order, SUM(g.goods_number) AS total_goods " .
            "FROM " . $GLOBALS['ecs']->table('order_info') . " AS o, " .
                $GLOBALS['ecs']->table('order_goods') . " AS g " .
            " WHERE o.order_id = g.order_id " .
            "AND o.extension_code = 'group_buy' " .
            "AND o.extension_id = '$group_buy_id' " .
            "AND g.goods_id = '$group_buy_goods_id' " .
            "AND (order_status = '" . OS_CONFIRMED . "' OR order_status = '" . OS_UNCONFIRMED . "')";
    $stat = $GLOBALS['db']->getRow($sql);
    if ($stat['total_order'] == 0)
    {
        $stat['total_goods'] = 0;
    }

    /* 取得有效订单数和有效商品数 */
    $deposit = floatval($deposit);
    if ($deposit > 0 && $stat['total_order'] > 0)
    {
        $sql .= " AND (o.money_paid + o.surplus) >= '$deposit'";
        $row = $GLOBALS['db']->getRow($sql);
        $stat['valid_order'] = $row['total_order'];
        if ($stat['valid_order'] == 0)
        {
            $stat['valid_goods'] = 0;
        }
        else
        {
            $stat['valid_goods'] = $row['total_goods'];
        }
    }
    else
    {
        $stat['valid_order'] = $stat['total_order'];
        $stat['valid_goods'] = $stat['total_goods'];
    }

    return $stat;
}

/**
 * 获得团购的状态
 *
 * @access  public
 * @param   array
 * @return  integer
 */
function group_buy_status($group_buy)
{
    $now = gmtime();
    if ($group_buy['is_finished'] == 0)
    {
        /* 未处理 */
        if ($now < $group_buy['start_time'])
        {
            $status = GBS_PRE_START;
        }
        elseif ($now > $group_buy['end_time'])
        {
            $status = GBS_FINISHED;
        }
        else
        {
            if ($group_buy['restrict_amount'] == 0 || $group_buy['valid_goods'] < $group_buy['restrict_amount'])
            {
                $status = GBS_UNDER_WAY;
            }
            else
            {
                $status = GBS_FINISHED;
            }
        }
    }
    elseif ($group_buy['is_finished'] == GBS_SUCCEED)
    {
        /* 已处理，团购成功 */
        $status = GBS_SUCCEED;
    }
    elseif ($group_buy['is_finished'] == GBS_FAIL)
    {
        /* 已处理，团购失败 */
        $status = GBS_FAIL;
    }

    return $status;
}

/**
 * 取得拍卖活动信息
 * @param   int     $act_id     活动id
 * @return  array
 */
function auction_info($act_id, $config = false)
{
    $sql = "SELECT * FROM " . $GLOBALS['ecs']->table('goods_activity') . " WHERE act_id = '$act_id'";
    $auction = $GLOBALS['db']->getRow($sql);
    if ($auction['act_type'] != GAT_AUCTION)
    {
        return array();
    }
    $auction['status_no'] = auction_status($auction);
    if ($config == true)
    {

        $auction['start_time'] = local_date('Y-m-d H:i', $auction['start_time']);
        $auction['end_time'] = local_date('Y-m-d H:i', $auction['end_time']);
    }
    else
    {
        $auction['start_time'] = local_date($GLOBALS['_CFG']['time_format'], $auction['start_time']);
        $auction['end_time'] = local_date($GLOBALS['_CFG']['time_format'], $auction['end_time']);
    }
    $ext_info = unserialize($auction['ext_info']);
    $auction = array_merge($auction, $ext_info);
    $auction['formated_start_price'] = price_format($auction['start_price']);
    $auction['formated_end_price'] = price_format($auction['end_price']);
    $auction['formated_amplitude'] = price_format($auction['amplitude']);
    $auction['formated_deposit'] = price_format($auction['deposit']);

    /* 查询出价用户数和最后出价 */
    $sql = "SELECT COUNT(DISTINCT bid_user) FROM " . $GLOBALS['ecs']->table('auction_log') .
            " WHERE act_id = '$act_id'";
    $auction['bid_user_count'] = $GLOBALS['db']->getOne($sql);
    if ($auction['bid_user_count'] > 0)
    {
        $sql = "SELECT a.*, u.user_name " .
                "FROM " . $GLOBALS['ecs']->table('auction_log') . " AS a, " .
                        $GLOBALS['ecs']->table('users') . " AS u " .
                "WHERE a.bid_user = u.user_id " .
                "AND act_id = '$act_id' " .
                "ORDER BY a.log_id DESC";
        $row = $GLOBALS['db']->getRow($sql);
        $row['formated_bid_price'] = price_format($row['bid_price'], false);
        $row['bid_time'] = local_date($GLOBALS['_CFG']['time_format'], $row['bid_time']);
        $auction['last_bid'] = $row;
    }

    /* 查询已确认订单数 */
    if ($auction['status_no'] > 1)
    {
        $sql = "SELECT COUNT(*)" .
                " FROM " . $GLOBALS['ecs']->table('order_info') .
                " WHERE extension_code = 'auction'" .
                " AND extension_id = '$act_id'" .
                " AND order_status " . db_create_in(array(OS_CONFIRMED, OS_UNCONFIRMED));
        $auction['order_count'] = $GLOBALS['db']->getOne($sql);
    }
    else
    {
        $auction['order_count'] = 0;
    }

    /* 当前价 */
    $auction['current_price'] = isset($auction['last_bid']) ? $auction['last_bid']['bid_price'] : $auction['start_price'];
    $auction['formated_current_price'] = price_format($auction['current_price'], false);

    return $auction;
}

/**
 * 取得拍卖活动出价记录
 * @param   int     $act_id     活动id
 * @return  array
 */
function auction_log($act_id)
{
    $log = array();
    $sql = "SELECT a.*, u.user_name " .
            "FROM " . $GLOBALS['ecs']->table('auction_log') . " AS a," .
                      $GLOBALS['ecs']->table('users') . " AS u " .
            "WHERE a.bid_user = u.user_id " .
            "AND act_id = '$act_id' " .
            "ORDER BY a.log_id DESC";
    $res = $GLOBALS['db']->query($sql);
    while ($row = $GLOBALS['db']->fetchRow($res))
    {
        $row['bid_time'] = local_date($GLOBALS['_CFG']['time_format'], $row['bid_time']);
        $row['formated_bid_price'] = price_format($row['bid_price'], false);
        $log[] = $row;
    }

    return $log;
}

/**
 * 计算拍卖活动状态（注意参数一定是原始信息）
 * @param   array   $auction    拍卖活动原始信息
 * @return  int
 */
function auction_status($auction)
{
    $now = gmtime();
    if ($auction['is_finished'] == 0)
    {
        if ($now < $auction['start_time'])
        {
            return PRE_START; // 未开始
        }
        elseif ($now > $auction['end_time'])
        {
            return FINISHED; // 已结束，未处理
        }
        else
        {
            return UNDER_WAY; // 进行中
        }
    }
    elseif ($auction['is_finished'] == 1)
    {
        return FINISHED; // 已结束，未处理
    }
    else
    {
        return SETTLED; // 已结束，已处理
    }
}

/**
 * 取得商品信息
 * @param   int     $goods_id   商品id
 * @return  array
 */
function goods_info($goods_id)
{
    $sql = "SELECT g.*, b.brand_name " .
            "FROM " . $GLOBALS['ecs']->table('goods') . " AS g " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('brand') . " AS b ON g.brand_id = b.brand_id " .
            "WHERE g.goods_id = '$goods_id'";
    $row = $GLOBALS['db']->getRow($sql);
    if (!empty($row))
    {
        /* 修正重量显示 */
        $row['goods_weight'] = (intval($row['goods_weight']) > 0) ?
            $row['goods_weight'] . $GLOBALS['_LANG']['kilogram'] :
            ($row['goods_weight'] * 1000) . $GLOBALS['_LANG']['gram'];

        /* 修正图片 */
        $row['goods_img'] = get_image_path($goods_id, $row['goods_img']);
    }

    return $row;
}

/**
 * 取得优惠活动信息
 * @param   int     $act_id     活动id
 * @return  array
 */
function favourable_info($act_id)
{
    $sql = "SELECT * FROM " . $GLOBALS['ecs']->table('favourable_activity') .
            " WHERE act_id = '$act_id'";
    $row = $GLOBALS['db']->getRow($sql);
    if (!empty($row))
    {
        $row['start_time_unformated'] = $row['start_time'];
        $row['end_time_unformated'] = $row['end_time'];
        $row['start_time'] = local_date($GLOBALS['_CFG']['time_format'], $row['start_time']);
        $row['end_time'] = local_date($GLOBALS['_CFG']['time_format'], $row['end_time']);
        $row['formated_min_amount'] = price_format($row['min_amount']);
        $row['formated_max_amount'] = price_format($row['max_amount']);
        $row['gift'] = unserialize($row['gift']);

        foreach ($row['gift'] as &$gift_goods) {
            $goods_info = goods_info($gift_goods['id']);
            $gift_goods['name'] = $goods_info['goods_name'];
            $gift_goods['shop_price'] = $goods_info['shop_price'];
        }

        if ($row['act_type'] == FAT_GOODS)
        {
            $row['act_type_ext'] = round($row['act_type_ext']);
        }
    }

    return $row;
}

/**
 * 批发信息
 * @param   int     $act_id     活动id
 * @return  array
 */
function wholesale_info($act_id)
{
    $sql = "SELECT * FROM " . $GLOBALS['ecs']->table('wholesale') .
            " WHERE act_id = '$act_id'";
    $row = $GLOBALS['db']->getRow($sql);
    if (!empty($row))
    {
        $row['price_list'] = unserialize($row['prices']);
    }

    return $row;
}

/**
 * 添加商品名样式
 * @param   string     $goods_name     商品名称
 * @param   string     $style          样式参数
 * @return  string
 */
function add_style($goods_name, $style)
{
    $goods_style_name = $goods_name;

    $arr   = explode('+', $style);

    $font_color     = !empty($arr[0]) ? $arr[0] : '';
    $font_style = !empty($arr[1]) ? $arr[1] : '';

    if ($font_color!='')
    {
        $goods_style_name = '<font color=' . $font_color . '>' . $goods_style_name . '</font>';
    }
    if ($font_style != '')
    {
        $goods_style_name = '<' . $font_style .'>' . $goods_style_name . '</' . $font_style . '>';
    }
    return $goods_style_name;
}

/**
 * 取得商品属性
 * @param   int     $goods_id   商品id
 * @return  array
 */
function get_goods_attr($goods_id)
{
    $attr_list = array();
    $sql = "SELECT a.attr_id, a.attr_name " .
            "FROM " . $GLOBALS['ecs']->table('goods') . " AS g, " . $GLOBALS['ecs']->table('attribute') . " AS a " .
            "WHERE g.goods_id = '$goods_id' " .
            "AND g.goods_type = a.cat_id " .
            "AND a.attr_type = 1";
    $attr_id_list = $GLOBALS['db']->getCol($sql);
    $res = $GLOBALS['db']->query($sql);
    while ($attr = $GLOBALS['db']->fetchRow($res))
    {
        if (defined('ECS_ADMIN'))
        {
            $attr['goods_attr_list'] = array(0 => $GLOBALS['_LANG']['select_please']);
        }
        else
        {
            $attr['goods_attr_list'] = array();
        }
        $attr_list[$attr['attr_id']] = $attr;
    }

    $sql = "SELECT attr_id, goods_attr_id, attr_value " .
            "FROM " . $GLOBALS['ecs']->table('goods_attr') .
            " WHERE goods_id = '$goods_id' " .
            "AND attr_id " . db_create_in($attr_id_list);
    $res = $GLOBALS['db']->query($sql);
    while ($goods_attr = $GLOBALS['db']->fetchRow($res))
    {
        $attr_list[$goods_attr['attr_id']]['goods_attr_list'][$goods_attr['goods_attr_id']] = $goods_attr['attr_value'];
    }

    return $attr_list;
}

/**
 * 获得购物车中商品的配件
 *
 * @access  public
 * @param   array     $goods_list
 * @return  array
 */
function get_goods_fittings($goods_list = array())
{
    $temp_index = 0;
    $arr        = array();
    
    require_once ROOT_PATH . 'includes/lib_order.php';
    
    $sql = "SELECT *, " . hw_goods_in_stock_sql() . ", " . hw_goods_number_sql() . " FROM (".
           'SELECT gg.parent_id, ggg.goods_name AS parent_name, gg.goods_id, gg.goods_price, g.goods_name, g.goods_thumb, g.goods_img, g.shop_price AS org_price, g.market_price, ' .
                "IFNULL(mp.user_price, g.shop_price * '$_SESSION[discount]') AS shop_price, ".
                "g.is_pre_sale, g.pre_sale_date, g.pre_sale_days, g.display_discount, ".
                hw_goods_number_subquery('gg.') . ", " .
    			hw_reserved_number_subquery('gg.') . " " .
            'FROM ' . $GLOBALS['ecs']->table('group_goods') . ' AS gg ' .
            'LEFT JOIN ' . $GLOBALS['ecs']->table('goods') . 'AS g ON g.goods_id = gg.goods_id ' .
            "LEFT JOIN " . $GLOBALS['ecs']->table('member_price') . " AS mp ".
                    "ON mp.goods_id = gg.goods_id AND mp.user_rank = '$_SESSION[user_rank]' ".
            "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " AS ggg ON ggg.goods_id = gg.parent_id ".
            "WHERE gg.parent_id " . db_create_in($goods_list) . " AND g.is_delete = 0 AND g.is_hidden = 0 AND g.is_on_sale = 1 ".
            //"ORDER BY gg.parent_id, gg.goods_id DESC";
            ") as tmp where 1 HAVING in_stock != 0 ORDER BY in_stock DESC, parent_id ASC, goods_id DESC  ";
       
    $res = $GLOBALS['db']->getAll($sql);
    
    $arr = hw_process_goods_rows($res);
    foreach ($arr as $key => $row)
    {
        $arr[$key]['parent_id']         = $row['parent_id'];//配件的基本件ID
        $arr[$key]['parent_name']       = $row['parent_name'];//配件的基本件的名称
        $arr[$key]['parent_short_name'] = $GLOBALS['_CFG']['goods_name_length'] > 0 ?
            sub_str($row['parent_name'], $GLOBALS['_CFG']['goods_name_length']) : $row['parent_name'];//配件的基本件显示的名称
    }
    
    return $arr;
}

/**
 * 取指定规格的货品信息
 *
 * @access      public
 * @param       string      $goods_id
 * @param       array       $spec_goods_attr_id
 * @return      array
 */
function get_products_info($goods_id, $spec_goods_attr_id)
{
    $return_array = array();

    if (empty($spec_goods_attr_id) || !is_array($spec_goods_attr_id) || empty($goods_id))
    {
        return $return_array;
    }

    $goods_attr_array = sort_goods_attr_id_array($spec_goods_attr_id);

    if(isset($goods_attr_array['sort']))
    {
        $goods_attr = implode('|', $goods_attr_array['sort']);

        $sql = "SELECT * FROM " .$GLOBALS['ecs']->table('products'). " WHERE goods_id = '$goods_id' AND goods_attr = '$goods_attr' LIMIT 0, 1";
        $return_array = $GLOBALS['db']->getRow($sql);
    }
    return $return_array;
}

/**
 * 获得指定商品的关联商品
 *
 * @access  public
 * @param   integer     $goods_id
 * @return  array
 */
function get_linked_goods($goods_id)
{
    $sql = "SELECT *, " . hw_goods_in_stock_sql() . ", " . hw_goods_number_sql() . " FROM (".
    'SELECT g.goods_id, g.goods_name, g.goods_thumb, g.goods_img, g.shop_price AS org_price, ' .
        "IFNULL(mp.user_price, g.shop_price * '$_SESSION[discount]') AS shop_price, ".
        'g.market_price, g.promote_price, g.promote_start_date, g.display_discount, g.promote_end_date, ' .
        "g.is_pre_sale, g.pre_sale_date, g.pre_sale_days, ".
        hw_goods_number_subquery('g.') . ", " .
        hw_reserved_number_subquery('g.') . " " .
    'FROM ' . $GLOBALS['ecs']->table('link_goods') . ' lg ' .
    'LEFT JOIN ' . $GLOBALS['ecs']->table('goods') . ' AS g ON g.goods_id = lg.link_goods_id ' .
    "LEFT JOIN " . $GLOBALS['ecs']->table('member_price') . " AS mp ".
            "ON mp.goods_id = g.goods_id AND mp.user_rank = '$_SESSION[user_rank]' ".
    "WHERE lg.goods_id = '$goods_id' AND g.is_on_sale = 1 AND g.is_alone_sale = 1 AND g.is_delete = 0 AND g.is_hidden = 0 ".
    ") as tmp where 1 HAVING in_stock != 0 ORDER BY in_stock DESC, goods_id DESC ".   
    "LIMIT " . $GLOBALS['_CFG']['related_goods_number'];

    $res = $GLOBALS['db']->getAll($sql);    
    $arr = hw_process_goods_rows($res);

    return $arr;
}

/**
 * 获得指定商品的关联文章
 *
 * @access  public
 * @param   integer     $goods_id
 * @return  void
 */
//a.cat_id = 17 OR a.cat_id = 18 只展示選購指南和產品教學類的相關文章
function get_linked_articles($goods_id)
{
    $sql = 'SELECT a.article_id, a.title, a.file_url, a.open_type, a.add_time, aperma ' .
            'FROM ' . $GLOBALS['ecs']->table('goods_article') . ' AS g, ' .
                $GLOBALS['ecs']->table('article') . ' AS a ' .
            "LEFT JOIN (SELECT id, table_name, lang, perma_link as aperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") apl ON apl.id = a.article_id  AND apl.table_name = 'article' AND apl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
            "WHERE g.article_id = a.article_id AND g.goods_id = '$goods_id' AND a.is_open = 1 AND (a.cat_id = 17 OR a.cat_id = 18) " .
            'ORDER BY a.add_time DESC limit 5';
    $res = $GLOBALS['db']->query($sql);
    
    $arr = array();
    while ($row = $GLOBALS['db']->fetchRow($res))
    {
        $row['url']         = $row['open_type'] != 1 ?
            build_uri('article', array('aid'=>$row['article_id'], 'aperma' => $row['aperma']), $row['title']) : trim($row['file_url']);
        $row['add_time']    = local_date($GLOBALS['_CFG']['date_format'], $row['add_time']);
        $row['short_title'] = $GLOBALS['_CFG']['article_title_length'] > 0 ?
            sub_str($row['title'], $GLOBALS['_CFG']['article_title_length']) : $row['title'];

        $arr[] = $row;
    }

    return $arr;
}

/**
 * 获得购买过该商品的人还买过的商品
 *
 * @access  public
 * @param   integer     $goods_id
 * @return  array
 */
function get_also_bought($goods_id)
{
    require_once ROOT_PATH . 'includes/lib_order.php';
    
    $sql = "SELECT *, " . hw_goods_in_stock_sql() . ", " . hw_goods_number_sql() . " FROM (".
           'SELECT COUNT(b.goods_id) AS num, g.goods_id, g.market_price, g.goods_name, g.sales+g.sales_adjust as shownum, g.goods_thumb, g.keywords, g.goods_img, g.shop_price AS org_price, g.promote_price, g.promote_start_date, g.promote_end_date, ' .
            "IFNULL(mp.user_price, g.shop_price * '$_SESSION[discount]') AS shop_price, g.display_discount,  ".
            "g.is_pre_sale, g.pre_sale_date, g.pre_sale_days, ".
            hw_goods_number_subquery('g.') . ", " .
			hw_reserved_number_subquery('g.') . " " .
            'FROM ' . $GLOBALS['ecs']->table('order_goods') . ' AS a ' .
            'LEFT JOIN ' . $GLOBALS['ecs']->table('order_goods') . ' AS b ON b.order_id = a.order_id ' .
            "LEFT JOIN " . $GLOBALS['ecs']->table('member_price') . " AS mp ON mp.goods_id = b.goods_id AND mp.user_rank = '$_SESSION[user_rank]' ".
            'LEFT JOIN ' . $GLOBALS['ecs']->table('goods') . ' AS g ON g.goods_id = b.goods_id ' .
            "WHERE a.goods_id = '$goods_id' AND b.goods_id <> '$goods_id' " .
            'AND g.is_on_sale = 1 AND g.is_alone_sale = 1 AND g.is_delete = 0 AND g.is_hidden = 0 ' .
            'GROUP BY b.goods_id ' .
            'ORDER BY num DESC ' .
            'LIMIT ' . $GLOBALS['_CFG']['bought_goods'] .
            ") as tmp where 1 HAVING in_stock != 0 ORDER BY in_stock DESC, num DESC";
    $res = $GLOBALS['db']->getAll($sql);
    
    $arr = hw_process_goods_rows($res);

    return $arr;
}

/**
 * 获得购物车中商品的配件
 *
 * @access  public
 * @param   array     $goods_list
 * @return  array
 */
function get_goods_promote_cart($goods_list = array(), $num = 6)
{
    $arr = array();
    
    $sql =  "SELECT goods_id FROM (" .
            "(SELECT goods_id FROM " . $GLOBALS['ecs']->table('group_goods') . " WHERE parent_id " . db_create_in($goods_list) . ")" .
            "UNION DISTINCT" .
            "(SELECT goods_id FROM " . $GLOBALS['ecs']->table('goods') . " WHERE is_promote_cart = 1)" .
            ") as tmp WHERE goods_id NOT " . db_create_in($goods_list);
    $goods_list = $GLOBALS['db']->getCol($sql);
    
    require_once ROOT_PATH . 'includes/lib_order.php';
    
    $sql = "SELECT *, " . hw_goods_in_stock_sql() . ", " . hw_goods_number_sql() . " FROM (".
     'SELECT g.goods_id, g.market_price, g.goods_name, g.sales+g.sales_adjust as shownum, g.goods_thumb, g.keywords, g.goods_img, g.shop_price AS org_price, g.promote_price, g.promote_start_date, g.promote_end_date, ' .
     "IFNULL(mp.user_price, g.shop_price * '$_SESSION[discount]') AS shop_price, ".
     "g.is_pre_sale, g.pre_sale_date, g.pre_sale_days, gperma, ".
     hw_goods_number_subquery('g.') . ", " .
     hw_reserved_number_subquery('g.') . " " .
     'FROM ' . $GLOBALS['ecs']->table('goods') . ' AS g ' .
     "LEFT JOIN " . $GLOBALS['ecs']->table('member_price') . " AS mp ON mp.goods_id = g.goods_id AND mp.user_rank = '$_SESSION[user_rank]' ".
     "LEFT JOIN (SELECT id, table_name, lang, perma_link as gperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") gpl ON gpl.id = g.goods_id  AND gpl.table_name = 'goods' AND gpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
     "WHERE g.goods_id " . db_create_in($goods_list) . " AND g.is_on_sale = 1 AND g.is_alone_sale = 1 AND g.is_delete = 0 AND g.is_hidden = 0 " .
     'GROUP BY g.goods_id ' .
     ") as tmp ORDER BY in_stock DESC, RAND() LIMIT $num";
    
    $res = $GLOBALS['db']->getAll($sql);
    
    $arr = hw_process_goods_rows($res);

    return $arr;
}




function get_ly_newtree($cat_id = 0)
{
    global $_CFG, $db, $ecs;
    
    // Add brand list for our category menu
    $include_brands = ($cat_id == 0);
    
    // For now only cache when $cat_id = 0
    $use_static_cache = ($cat_id == 0);
    
    if ($use_static_cache)
    {
        $cache_name = 'ly_newtree_' . $cat_id . '_' . $_CFG['lang'];
        $data = read_static_cache($cache_name);
        if ($data !== false)
        {
            return $data;
        }
    }
    
	$cat_id = hw_get_cat_parent($cat_id);
	
    if ($cat_id > 0)
    {
		$parent_id = $cat_id;
    }
    else
    {
        $parent_id = 0;
    }
    
    /*
     判断当前分类中全是是否是底级分类，
     如果是取出底级分类上级分类，
     如果不是取当前分类及其下的子分类
    */
    $sql = 'SELECT count(*) FROM ' . $ecs->table('category') . " WHERE parent_id = '$cat_id' AND is_show = 1 ";
    if ($parent_id == 0) // Listing from root category
    {
        /* 获取当前分类及其子分类及其孫分类 */
        $sql = 'SELECT a.cat_id, a.cat_name, a.sort_order AS parent_order, a.cat_id, a.is_show,' .
                    'b.cat_id AS child_id, b.cat_name AS child_name, b.sort_order AS child_order, ' .
                    'c.cat_id AS grandson_id, c.cat_name AS grandson_name, c.sort_order AS grandson_order, ' .
                    'd.cat_id AS sub_grandson_id, d.cat_name AS sub_grandson_name, d.sort_order AS sub_grandson_order, ' .
                    '(SELECT count(*) FROM ' . $ecs->table('goods') . ' AS g WHERE g.cat_id = c.cat_id AND g.is_delete = 0 AND g.is_hidden = 0 AND g.is_on_sale = 1) as num ' .
                'FROM ' . $ecs->table('category') . ' AS a ' .
                'LEFT JOIN ' . $ecs->table('category') . ' AS b ON b.parent_id = a.cat_id AND b.is_show = 1 ' .
                'LEFT JOIN ' . $ecs->table('category') . ' AS c ON c.parent_id = b.cat_id AND c.is_show = 1 ' .
                'LEFT JOIN ' . $ecs->table('category') . ' AS d ON d.parent_id = c.cat_id AND d.is_show = 1 ' .
                "WHERE a.parent_id = '$parent_id' ORDER BY parent_order DESC, a.cat_id DESC, child_order DESC, grandson_order DESC, sub_grandson_order DESC ";
    }
    else
//    if ($parent_id == 0) // Listing from root category
//    {
//        /* 获取当前分类的子分类及其孫分类 */
//        $sql = 'SELECT a.cat_id, a.cat_name, a.sort_order AS parent_order, a.cat_id, a.is_show,' .
//                    'b.cat_id AS child_id, b.cat_name AS child_name, b.sort_order AS child_order, ' .
//                    "-1 as num " .//'(SELECT count(*) FROM ' . $ecs->table('goods') . ' AS g WHERE g.cat_id = b.cat_id AND g.is_delete = 0 AND g.is_hidden = 0 AND g.is_on_sale = 1) as num ' .
//                'FROM ' . $ecs->table('category') . ' AS a ' .
//                'LEFT JOIN ' . $ecs->table('category') . ' AS b ON b.parent_id = a.cat_id AND b.is_show = 1 ' .
//                "WHERE a.parent_id IN (SELECT c.cat_id FROM " . $ecs->table('category') . " AS c WHERE c.parent_id = '$parent_id') ORDER BY parent_order ASC, a.cat_id ASC, child_order ASC";
//    }
//    else
    if ($db->getOne($sql) || $parent_id == 0)
    {
        /* 获取当前分类及其子分类 */
        $sql = 'SELECT a.cat_id, a.cat_name, a.sort_order AS parent_order, a.cat_id, a.is_show,' .
                    'b.cat_id AS child_id, b.cat_name AS child_name, b.sort_order AS child_order, ' .
                    '(SELECT count(*) FROM ' . $ecs->table('goods') . ' AS g WHERE g.cat_id = b.cat_id AND g.is_delete = 0 AND g.is_hidden = 0 AND g.is_on_sale = 1) as num, cperma, child_cperma ' .
                'FROM ' . $ecs->table('category') . ' AS a ' .
                'LEFT JOIN ' . $ecs->table('category') . ' AS b ON b.parent_id = a.cat_id AND b.is_show = 1 ' .
                "LEFT JOIN (SELECT id, table_name, lang, perma_link as cperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") cpl ON cpl.id = a.cat_id  AND cpl.table_name = 'category' AND cpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
                "LEFT JOIN (SELECT id, table_name, lang, perma_link as child_cperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") ccpl ON ccpl.id = b.cat_id  AND ccpl.table_name = 'category' AND ccpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
                "WHERE a.parent_id = '$parent_id' ORDER BY parent_order DESC, a.cat_id ASC, child_order DESC";
    }
    else
    {
        /* 获取当前分类及其父分类 */
        $sql = 'SELECT a.cat_id, a.cat_name, b.cat_id AS child_id, b.cat_name AS child_name, b.sort_order, b.is_show, ' .
        			'(SELECT count(*) FROM ' . $ecs->table('goods') . ' AS g WHERE g.cat_id = b.cat_id AND g.is_delete = 0 AND g.is_hidden = 0 AND g.is_on_sale = 1) as num, cperma, child_cperma ' .
                'FROM ' . $ecs->table('category') . ' AS a ' .
                'LEFT JOIN ' . $ecs->table('category') . ' AS b ON b.parent_id = a.cat_id AND b.is_show = 1 ' .
                "LEFT JOIN (SELECT id, table_name, lang, perma_link as cperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") cpl ON cpl.id = a.cat_id  AND cpl.table_name = 'category' AND cpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
                "LEFT JOIN (SELECT id, table_name, lang, perma_link as child_cperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") ccpl ON ccpl.id = b.cat_id  AND ccpl.table_name = 'category' AND ccpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
                "WHERE b.parent_id = '$parent_id' ORDER BY sort_order DESC";
    }
    $res = $db->getAll($sql);

    $res = localize_db_result('category', $res);
    $res = localize_db_result('category', $res, array('child_id' => 'cat_id', 'child_name' => 'cat_name'));
    $res = localize_db_result('category', $res, array('grandson_id' => 'cat_id', 'grandson_name' => 'cat_name'));
    
    $cat_arr = array();
    foreach ($res as $row)
    {
        if (!$row['is_show']) continue;
        
        $cat_arr[$row['cat_id']]['id']   = $row['cat_id'];
        $cat_arr[$row['cat_id']]['name'] = $row['cat_name'];
        $cat_arr[$row['cat_id']]['url']  = build_uri('category', array('cid' => $row['cat_id'], 'cname' => $row['cat_name'], 'cperma' => $row['cperma']), $row['cat_name']);
        $cat_arr[$row['cat_id']]['num']  = (isset($cat_arr[$row['cat_id']]['num']) ? $cat_arr[$row['cat_id']]['num'] : 0) + $row['num'];
        
        if ($row['child_id'] != NULL)
        {
            $cat_arr[$row['cat_id']]['children'][$row['child_id']]['id']   = $row['child_id'];
            $cat_arr[$row['cat_id']]['children'][$row['child_id']]['name'] = $row['child_name'];
            $cat_arr[$row['cat_id']]['children'][$row['child_id']]['url']  = build_uri('category', array('cid' => $row['child_id'], 'cname' => $row['child_name'], 'cperma' => $row['child_cperma']), $row['child_name']);
            if ($row['grandson_id'] != NULL)
            {
                $cat_arr[$row['cat_id']]['children'][$row['child_id']]['num']  = (isset($cat_arr[$row['cat_id']]['children'][$row['child_id']]['num']) ? $cat_arr[$row['cat_id']]['children'][$row['child_id']]['num'] : 0) + $row['num'];
                
                $cat_arr[$row['cat_id']]['children'][$row['child_id']]['children'][$row['grandson_id']]['id']   = $row['grandson_id'];
                $cat_arr[$row['cat_id']]['children'][$row['child_id']]['children'][$row['grandson_id']]['name'] = $row['grandson_name'];
                $cat_arr[$row['cat_id']]['children'][$row['child_id']]['children'][$row['grandson_id']]['url']  = build_uri('category', array('cid' => $row['grandson_id'], 'cname' => $row['grandson_name']), $row['grandson_name']);
                $cat_arr[$row['cat_id']]['children'][$row['child_id']]['children'][$row['grandson_id']]['num']  = $row['num'];
            }
            else
            {
                $cat_arr[$row['cat_id']]['children'][$row['child_id']]['num']  = $row['num'];
            }
        }
    }
    
    if ($include_brands)
    {
        $sql = "SELECT cb.`cat_id`, b.*, bperma, cperma " .
                "FROM " . $ecs->table('category_brand') . " as cb " .
                "LEFT JOIN " . $ecs->table('brand') . " as b ON cb.brand_id = b.brand_id " .
                "LEFT JOIN (SELECT id, table_name, lang, perma_link as bperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") bpl ON bpl.id = cb.brand_id  AND bpl.table_name = 'brand' AND bpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
                "LEFT JOIN (SELECT id, table_name, lang, perma_link as cperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") cpl ON cpl.id = cb.cat_id  AND cpl.table_name = 'category' AND cpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
                "WHERE cb.cat_id " . db_create_in(array_keys($cat_arr)) . " " .
                "ORDER BY cb.`cat_id` ASC, cb.`sort_order` ASC";
        $res = $GLOBALS['db']->getAll($sql);
        
        $res = localize_db_result('brand', $res);
        
        foreach ($res AS $row)
        {
            $row['url'] = build_uri('category', array('cid' => $row['cat_id'], 'cname' => $cat_arr[$row['cat_id']]['name'], 'cperma' => $row['cperma'], 'bid' => $row['brand_id'], 'bname' => $row['brand_name'], 'bperma' => $row['bperma']), $row['brand_name']);
            
            $cat_arr[$row['cat_id']]['brands'] = isset($cat_arr[$row['cat_id']]['brands']) ? $cat_arr[$row['cat_id']]['brands'] : array();
            $cat_arr[$row['cat_id']]['brands'][] = $row;
        }
    }
    
    if ($use_static_cache)
    {
        write_static_cache($cache_name, $cat_arr);
    }
    return $cat_arr;
}

function get_today_info()
{
    $orderController = new Yoho\cms\Controller\OrderController();
    $yoho_holidays = $orderController->get_yoho_holidays();
    foreach ($yoho_holidays as $key => $value) {
        $holiday_list[] = $value['date'];
    }
    $now = time();
    $today = date('Y-m-d', $now);
    $today_dow = date('w', $now); // 0 = Sunday, 6 = Saturday
    $tomorrow = date('Y-m-d', $now+86400);
    $tomorrow_dow = date('w', $now+86400); // 0 = Sunday, 6 = Saturday
    $holiday = (in_array($today, $holiday_list));
    $before_holiday = (in_array($tomorrow, $holiday_list));
    $hour = date('H', $now);
    return compact('holiday','before_holiday','hour');
}

?>
