<?php

/**
 * ECSHOP 用户帐号相关函数库
 * ============================================================================
 * * 版权所有 2005-2012 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liubo $
 * $Id: lib_passport.php 17217 2011-01-19 06:29:08Z liubo $
*/

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

/**
 * 用户注册，登录函数
 *
 * @access  public
 * @param   string       $username          注册用户名
 * @param   string       $password          用户密码
 * @param   string       $email             注册email
 * @param   array        $other             注册的其他信息
 *
 * @return  bool         $bool
 */
function register($username, $password, $email, $other = array(), $is_pos = false, $affiliate_id = 0)
{
    /* 检查注册是否关闭 (POS 不受此限制) */
    if (!$is_pos && !empty($GLOBALS['_CFG']['shop_reg_closed']))
    {
        $GLOBALS['err']->add(_L('account_register_error_closed', '本店暫時不接受新會員註冊。'));
    }
    /* 检查username */
    if (empty($username))
    {
        $GLOBALS['err']->add(sprintf(_L('account_please_fill_in', '請輸入%s'), _L('account_username', '手提電話號碼')));
    }
    else
    {
        if (preg_match('/\'\/^\\s*$|^c:\\\\con\\\\con$|[%,\\*\\"\\s\\t\\<\\>\\&\'\\\\]/', $username))
        {
            $GLOBALS['err']->add(sprintf(
                _L('account_register_error_invalid', '您的%s (%s) 含有敏感字元'),
                _L('account_username', '手提電話號碼'),
                htmlspecialchars($username)
            ));
        }
    }

    /* 检查email */
    if (empty($email))
    {
        $GLOBALS['err']->add(sprintf(_L('account_please_fill_in', '請輸入%s'), _L('account_email', '電郵地址')));
    }
    else
    {
        if (!is_email($email))
        {
            $GLOBALS['err']->add(sprintf(
                _L('account_format_invalid', '%s格式有誤，再修改一下哦！'),
                _L('account_email', '電郵地址')
            ));
        }
    }

    if ($GLOBALS['err']->error_no > 0)
    {
        return false;
    }

    /* 检查是否和管理员重名 */
    if (admin_registered($username))
    {
        $GLOBALS['err']->add(sprintf(
            _L('account_register_error_already_taken', '您的%s（%s）已被使用'),
            _L('account_username', '手提電話號碼'),
            htmlspecialchars($username)
        ));
        return false;
    }

    if (!$GLOBALS['user']->add_user($username, $password, $email))
    {
        if ($GLOBALS['user']->error == ERR_INVALID_USERNAME)
        {
            $GLOBALS['err']->add(sprintf(
                _L('account_register_error_invalid', '您的%s (%s) 含有敏感字元'),
                _L('account_username', '手提電話號碼'),
                htmlspecialchars($username)
            ));
        }
        elseif ($GLOBALS['user']->error == ERR_USERNAME_NOT_ALLOW)
        {
            $GLOBALS['err']->add(sprintf(
                _L('account_register_error_disallow', '您的%s (%s) 不允許註冊'),
                _L('account_username', '手提電話號碼'),
                htmlspecialchars($username)
            ));
        }
        elseif ($GLOBALS['user']->error == ERR_USERNAME_EXISTS)
        {
            $GLOBALS['err']->add(sprintf(
                _L('account_register_error_already_taken', '您的%s（%s）已被使用'),
                _L('account_username', '手提電話號碼'),
                htmlspecialchars($username)
            ));
        }
        elseif ($GLOBALS['user']->error == ERR_INVALID_EMAIL)
        {
            $GLOBALS['err']->add(sprintf(
                _L('account_register_error_invalid', '您的%s (%s) 含有敏感字元'),
                _L('account_email', '電郵地址'),
                htmlspecialchars($email)
            ));
        }
        elseif ($GLOBALS['user']->error == ERR_EMAIL_NOT_ALLOW)
        {
            $GLOBALS['err']->add(sprintf(
                _L('account_register_error_disallow', '您的%s (%s) 不允許註冊'),
                _L('account_email', '電郵地址'),
                htmlspecialchars($email)
            ));
        }
        elseif ($GLOBALS['user']->error == ERR_EMAIL_EXISTS)
        {
            $GLOBALS['err']->add(sprintf(
                _L('account_register_error_already_taken', '您的%s（%s）已被使用'),
                _L('account_email', '電郵地址'),
                htmlspecialchars($email)
            ));
        }
        else
        {
            $GLOBALS['err']->add(_L('account_register_error_unknown', '註冊失敗，請稍後再試。'));
        }

        //注册失败
        return false;
    }
    else
    {
        //注册成功
        
        // POS 不登入新註冊的帳號
        if ($is_pos)
        {
            // 取得新註冊會員的 user_id
            $sql = "SELECT user_id FROM " . $GLOBALS['ecs']->table('users') . " WHERE user_name = '" . $username . "'";
            $user_id = $GLOBALS['db']->getOne($sql);
        }
        else
        {
            // 正常註冊程序 自動登入新註冊的帳號
            $GLOBALS['user']->set_session($username);
            $GLOBALS['user']->set_cookie($username);
            
            $user_id = $_SESSION['user_id'];
        }

        /* 注册送积分 */
        if (!empty($GLOBALS['_CFG']['register_points']))
        {
            log_account_change($user_id, 0, 0, $GLOBALS['_CFG']['register_points'], $GLOBALS['_CFG']['register_points'], $GLOBALS['_LANG']['register_points']);
        }

        /*推荐处理*/
        $affiliate  = unserialize($GLOBALS['_CFG']['affiliate']);
        if (isset($affiliate['on']) && $affiliate['on'] == 1)
        {
            // 推荐开关开启
            $up_uid     = get_affiliate(); //TODO: Update to using New affiliate
            empty($affiliate) && $affiliate = array();
            $affiliate['config']['level_register_all'] = intval($affiliate['config']['level_register_all']);
            $affiliate['config']['level_register_up'] = intval($affiliate['config']['level_register_up']);
            if ($up_uid && $up_uid != $user_id)
            {
                if (!empty($affiliate['config']['level_register_all']))
                {
                    if (!empty($affiliate['config']['level_register_up']))
                    {
                        $rank_points = $GLOBALS['db']->getOne("SELECT rank_points FROM " . $GLOBALS['ecs']->table('users') . " WHERE user_id = '$up_uid'");
                        if ($rank_points + $affiliate['config']['level_register_all'] <= $affiliate['config']['level_register_up'])
                        {
                            log_account_change($up_uid, 0, 0, $affiliate['config']['level_register_all'], 0, sprintf($GLOBALS['_LANG']['register_affiliate'], $_SESSION['user_id'], $username));
                        }
                    }
                    else
                    {
                        log_account_change($up_uid, 0, 0, $affiliate['config']['level_register_all'], 0, $GLOBALS['_LANG']['register_affiliate']);
                    }
                }

                //设置推荐人
                $sql = 'UPDATE '. $GLOBALS['ecs']->table('users') . ' SET parent_id = ' . $up_uid . ' WHERE user_id = ' . $user_id;

                $GLOBALS['db']->query($sql);
            }
        } elseif (isset($affiliate['affiliate_yoho_on']) && $affiliate['affiliate_yoho_on'] == 1) { // 沒有開普通推薦, 但開放了推廣大使

            $up_uid     = get_affiliate(true); //TODO: Update to using New affiliate
            if ($up_uid && $up_uid != $user_id) {
                //设置推荐人
                $sql = 'UPDATE '. $GLOBALS['ecs']->table('users') . ' SET parent_id = ' . $up_uid . ' WHERE user_id = ' . $user_id;
                $GLOBALS['db']->query($sql);
            }
        }

        // Unset affiliate cookie
        set_affiliate_cookie('', 1);

        //定义other合法的变量数组
        $other_key_array = array('msn', 'qq', 'office_phone', 'home_phone', 'mobile_phone','is_wholesale');
        $update_data['reg_time'] = local_strtotime(local_date('Y-m-d H:i:s'));
        $update_data['passchg_time'] = $update_data['reg_time']; // Added by howang: password change time
        if ($other)
        {
            foreach ($other as $key=>$val)
            {
                //删除非法key值
                if (!in_array($key, $other_key_array))
                {
                    unset($other[$key]);
                }
                else
                {
                    $other[$key] =  htmlspecialchars(trim($val)); //防止用户输入javascript代码
                }
            }
            $update_data = array_merge($update_data, $other);
        }
        $GLOBALS['db']->autoExecute($GLOBALS['ecs']->table('users'), $update_data, 'UPDATE', 'user_id = ' . $user_id);
        
        //update wholesale userids Cache
        if(array_key_exists('is_wholesale', $other)){
            if(boolval($other['is_wholesale'])==true){
                $userController = new Yoho\cms\Controller\UserController();
                $userController->flushWholesaleUserIds();
            }
        }

        // 登入後的處理
        if (!$is_pos)
        {
            update_user_info();      // 更新用户信息
            recalculate_price();     // 重新计算购物车中的商品价格
        }
        
        return true;
    }
}

/**
 *
 *
 * @access  public
 * @param
 *
 * @return void
 */
function logout()
{
/* todo */
}

/**
 *  将指定user_id的密码修改为new_password。可以通过旧密码和验证字串验证修改。
 *
 * @access  public
 * @param   int     $user_id        用户ID
 * @param   string  $new_password   用户新密码
 * @param   string  $old_password   用户旧密码
 * @param   string  $code           验证码（md5($user_id . md5($password))）
 *
 * @return  boolen  $bool
 */
function edit_password($user_id, $old_password, $new_password='', $code ='')
{
    if (empty($user_id)) $GLOBALS['err']->add($GLOBALS['_LANG']['not_login']);

    if ($GLOBALS['user']->edit_password($user_id, $old_password, $new_password, $code))
    {
        return true;
    }
    else
    {
        $GLOBALS['err']->add($GLOBALS['_LANG']['edit_password_failure']);

        return false;
    }
}

/**
 *  会员找回密码时，对输入的用户名和邮件地址匹配
 *
 * @access  public
 * @param   string  $user_name    用户帐号
 * @param   string  $email        用户Email
 *
 * @return  boolen
 */
function check_userinfo($user_name, $email)
{
    if (empty($user_name) || empty($email))
    {
        header('Location: /user.php?act=get_password');
        exit;
    }

    /* 检测用户名和邮件地址是否匹配 */
    $user_info = $GLOBALS['user']->check_pwd_info($user_name, $email);
    if (!empty($user_info))
    {
        return $user_info;
    }
    else
    {
        return false;
    }
}

/**
 *  用户进行密码找回操作时，发送一封确认邮件
 *
 * @access  public
 * @param   string  $uid          用户ID
 * @param   string  $user_name    用户帐号
 * @param   string  $email        用户Email
 * @param   string  $code         key
 *
 * @return  boolen  $result;
 */
function send_pwd_email($uid, $user_name, $email, $code)
{
    if (empty($uid) || empty($user_name) || empty($email) || empty($code))
    {
        header('Location: ' . $GLOBALS['ecs']->url() . 'user.php?act=get_password');
        exit;
    }

    /* 设置重置邮件模板所需要的内容信息 */
    $template    = get_mail_template('send_password');
    $reset_email = $GLOBALS['ecs']->url() . 'user.php?act=get_password&uid=' . $uid . '&code=' . $code;

    $GLOBALS['smarty']->assign('user_name',   $user_name);
    $GLOBALS['smarty']->assign('reset_email', $reset_email);
    $GLOBALS['smarty']->assign('shop_name',   $GLOBALS['_CFG']['shop_name']);
    $GLOBALS['smarty']->assign('send_date',   date('Y-m-d'));
    $GLOBALS['smarty']->assign('sent_date',   date('Y-m-d'));

    $content = $GLOBALS['smarty']->fetch('str:' . $template['template_content']);

    if (!can_send_mail($email, true))
    {
        return false;
    }

    /* 发送确认重置密码的确认邮件 */
    if (send_mail($user_name, $email, $template['template_subject'], $content, $template['is_html']))
    {
        return true;
    }
    else
    {
        return false;
    }
}

function generate_password_reset_token($user_id, $time = 0)
{
    global $_CFG, $ecs, $db;

    if ($time <= 0)
    {
        $time = gmtime();
    }

    // Use password change time in the hash to ensure the token is invalided after changing password
    $sql = "SELECT `passchg_time` FROM " . $ecs->table('users') . " WHERE user_id = '".$user_id."'";
    $passchg_time = $db->getOne($sql);

    $hash = strrev(sha1($time . $user_id . $passchg_time . $_CFG['hash_code']));

    $token = $time . ',' . $hash;

    return $token;
}

function verify_password_reset_token($user_id, $token)
{
    $tmp = explode(',', $token);
    if (count($tmp) != 2) return false;

    $time = $tmp[0];
    $hash = $tmp[1];

    // Only valid for 1 day (86400 seconds)
    if (gmtime() - $time > 86400) return false;

    $verify_token = generate_password_reset_token($user_id, $time);

    return ($verify_token === $token);
}

/**
 *  发送激活验证邮件
 *
 * @access  public
 * @param   int     $user_id        用户ID
 *
 * @return boolen
 */
function send_regiter_hash ($user_id)
{
    /* 设置验证邮件模板所需要的内容信息 */
    $template    = get_mail_template('register_validate');
    $hash = register_hash('encode', $user_id);
    $validate_email = $GLOBALS['ecs']->url() . 'user.php?act=validate_email&hash=' . $hash;

    $sql = "SELECT user_name, email FROM " . $GLOBALS['ecs']->table('users') . " WHERE user_id = '$user_id'";
    $row = $GLOBALS['db']->getRow($sql);

    $GLOBALS['smarty']->assign('user_name',         $row['user_name']);
    $GLOBALS['smarty']->assign('validate_email',    $validate_email);
    $GLOBALS['smarty']->assign('shop_name',         $GLOBALS['_CFG']['shop_name']);
    $GLOBALS['smarty']->assign('send_date',         date($GLOBALS['_CFG']['date_format']));

    $content = $GLOBALS['smarty']->fetch('str:' . $template['template_content']);

    if (!can_send_mail($row['email'], true))
    {
        return false;
    }

    /* 发送激活验证邮件 */
    if (send_mail($row['user_name'], $row['email'], $template['template_subject'], $content, $template['is_html']))
    {
        return true;
    }
    else
    {
        return false;
    }
}

function send_regiter_hash_async($user_id)
{
    require_once ROOT_PATH . 'core/lib_payload.php';
    
    $api_url = 'http://' . (empty($_SERVER['HTTP_HOST']) ? $_SERVER['SERVER_NAME'] : $_SERVER['HTTP_HOST']) . '/api/howang/email_verify.php';
    
    $obj = array('act' => 'send_verify_email', 'user_id' => $user_id);
    $payload = base64_encode(encode_payload($obj));
    
    exec('curl -d "payload=' . rawurlencode($payload) . '" ' . $api_url . ' > /dev/null 2>&1 &');
    
    return true;
}

/**
 *  生成邮件验证hash
 *
 * @access  public
 * @param
 *
 * @return void
 */
function register_hash ($operation, $key)
{
    if ($operation == 'encode')
    {
        $user_id = intval($key);
        $sql = "SELECT email, reg_time ".
               " FROM " . $GLOBALS['ecs'] ->table('users').
               " WHERE user_id = '$user_id' LIMIT 1";
        $row = $GLOBALS['db']->getRow($sql);
        $email = $row['email'];
        $reg_time = $row['reg_time'];

        $hash = substr(md5($user_id . $email . $GLOBALS['_CFG']['hash_code'] . $reg_time), 18, 4);

        return base64_encode($user_id . ',' . $hash);
    }
    else
    {
        $hash = base64_decode(trim($key));
        $row = explode(',', $hash);
        if (count($row) != 2)
        {
            return 0;
        }
        $user_id = intval($row[0]);
        $salt = trim($row[1]);

        if ($user_id <= 0 || strlen($salt) != 4)
        {
            return 0;
        }

        $sql = "SELECT email, reg_time ".
               " FROM " . $GLOBALS['ecs'] ->table('users').
               " WHERE user_id = '$user_id' LIMIT 1";
        $row = $GLOBALS['db']->getRow($sql);
        $email = $row['email'];
        $reg_time = $row['reg_time'];

        $pre_salt = substr(md5($user_id . $email . $GLOBALS['_CFG']['hash_code'] . $reg_time), 18, 4);

        if ($pre_salt == $salt)
        {
            return $user_id;
        }
        else
        {
            return 0;
        }
    }
}

/**
 * 判断超级管理员用户名是否存在
 * @param   string      $adminname 超级管理员用户名
 * @return  boolean
 */
function admin_registered( $adminname )
{
    $res = $GLOBALS['db']->getOne("SELECT COUNT(*) FROM " . $GLOBALS['ecs']->table('admin_user') .
                                  " WHERE user_name = '$adminname'");
    return $res;
}

?>