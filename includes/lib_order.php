<?php

/**
 * ECSHOP 购物流程函数库
 * ============================================================================
 * * 版权所有 2005-2012 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liubo $
 * $Id: lib_order.php 17217 2011-01-19 06:29:08Z liubo $
 */

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

/**
 * 处理序列化的支付、配送的配置参数
 * 返回一个以name为索引的数组
 *
 * @access  public
 * @param   string       $cfg
 * @return  void
 */
function unserialize_config($cfg)
{
    if (is_string($cfg) && ($arr = unserialize($cfg)) !== false)
    {
        $config = array();

        foreach ($arr AS $key => $val)
        {
            $config[$val['name']] = $val['value'];
        }


        return $config;
    }
    else
    {
        return false;
    }
}
/**
 * 取得已安装的配送方式
 * @return  array   已安装的配送方式
 */
function shipping_list($is_pos = false)
{
    $sql = 'SELECT s.shipping_id, s.shipping_name, st.shipping_type_code, st.shipping_type_name ' .
            'FROM ' . $GLOBALS['ecs']->table('shipping') .' as s '.
            'LEFT JOIN ' . $GLOBALS['ecs']->table('shipping_type') .' as st on st.shipping_type_id = s.shipping_type_id '.
            ' WHERE s.enabled = 1';
    if($is_pos)$sql .= " AND s.is_pos = 1 ";

    $res = $GLOBALS['db']->getAll($sql);
    $res = localize_db_result('shipping', $res);

    return $res;
}

/**
 * 取得配送方式信息
 * @param   int     $shipping_id    配送方式id
 * @return  array   配送方式信息
 */
function shipping_info($shipping_id)
{
    $sql = 'SELECT * FROM ' . $GLOBALS['ecs']->table('shipping') .
            " WHERE shipping_id = '$shipping_id' " .
            'AND enabled = 1';
    $row = $GLOBALS['db']->getRow($sql);
    $row = localize_db_row('shipping', $row);
    return $row;
}

/**
 * 取得可用的配送方式列表
 * @param   array $region_id_list 收货人地区id数组（包括国家、省、市、区）
 * @param null $shipping_type
 * @return  array   配送方式数组
 */
function available_shipping_list($region_id_list, $shipping_type = null)
{
    $where = " 1 ";
    if(!empty($region_id_list)) {
        $where .= "AND r.region_id " . db_create_in($region_id_list);
    }
	if(!empty($shipping_type)) {
		$where .= "AND st.shipping_type_code  = '" . $shipping_type."' ";
	}
    $sql = "SELECT a.shipping_area_id, r.region_id , s.shipping_id, s.shipping_code, s.insure, s.support_cod, s.support_fod,s.shipping_logo, a.configure, " .
                ((empty($region_id_list)) ? "IFNULL(al.shipping_area_name, a.shipping_area_name) as shipping_name, " : "IFNULL(sl.shipping_name, s.shipping_name) as shipping_name, " ).
                "IF(a.shipping_desc = '', " .
                    "IFNULL(sl.shipping_desc, s.shipping_desc), " .
                    "IFNULL(al.shipping_desc, a.shipping_desc) " .
                ") as shipping_desc " .
            "FROM " . $GLOBALS['ecs']->table('shipping') . " as s " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('shipping_lang') . " as sl ON sl.shipping_id = s.shipping_id AND sl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('shipping_area') . " as a ON a.shipping_id = s.shipping_id " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('shipping_type') . " as st ON st.shipping_type_id = s.shipping_type_id " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('shipping_area_lang') . " as al ON al.shipping_area_id = a.shipping_area_id AND al.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('area_region') . " as r ON r.shipping_area_id = a.shipping_area_id " .
            "WHERE ".$where .
            " AND s.enabled = 1 " .
            "ORDER BY s.shipping_order";
    $res =  $GLOBALS['db']->getAll($sql);
    return $res;
}

/**
 * 取得某配送方式对应于某收货地址的区域信息
 * @param   int $shipping_id 配送方式id
 * @param   array $region_id_list 收货人地区id数组
 * @param int $shipping_area_id
 * @return  array   配送区域信息（config 对应着反序列化的 configure）
 */
function shipping_area_info($shipping_id, $region_id_list, $shipping_area_id = 0)
{
    if(!empty($shipping_area_id)) $where = "AND a.shipping_area_id = $shipping_area_id ";
    else $where = "AND r.region_id " . db_create_in($region_id_list) ;
    $sql = "SELECT s.shipping_code, s.insure, s.support_cod, a.configure, " .
                "IFNULL(sl.shipping_name, s.shipping_name) as shipping_name, " .
                "IF(a.shipping_desc = '', " .
                    "IFNULL(sl.shipping_desc, s.shipping_desc), " .
                    "IFNULL(al.shipping_desc, a.shipping_desc) " .
                ") as shipping_desc, s.sort_order, GROUP_CONCAT(DISTINCT r.region_id) as region_list, a.shipping_area_id " .
                "FROM " . $GLOBALS['ecs']->table('shipping') . " as s " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('shipping_lang') . " as sl ON sl.shipping_id = s.shipping_id AND sl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('shipping_area') . " as a ON a.shipping_id = s.shipping_id " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('shipping_area_lang') . " as al ON al.shipping_area_id = a.shipping_area_id AND al.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('area_region') . " as r ON r.shipping_area_id = a.shipping_area_id " .
            "WHERE s.shipping_id = '" . $shipping_id . "' " .
            $where.
            " AND s.enabled = 1 ".
            " GROUP BY a.shipping_area_id ".
            " ORDER BY s.sort_order";
    $row = $GLOBALS['db']->getRow($sql);

    if (!empty($row))
    {
        $shipping_config = unserialize_config($row['configure']);
        if (isset($shipping_config['pay_fee']))
        {
            if (strpos($shipping_config['pay_fee'], '%') !== false)
            {
                $row['pay_fee'] = floatval($shipping_config['pay_fee']) . '%';
            }
            else
            {
                $row['pay_fee'] = floatval($shipping_config['pay_fee']);
            }
        }
        else
        {
            $row['pay_fee'] = 0.00;
        }
        $row['region_list'] = explode(',', $row['region_list']);
    }

    return $row;
}

/**
 * 计算运费
 * @param   string  $shipping_code      配送方式代码
 * @param   mix     $shipping_config    配送方式配置信息
 * @param   float   $goods_weight       商品重量
 * @param   float   $goods_amount       商品金额
 * @param   float   $goods_number       商品数量
 * @return  float   运费
 */
function shipping_fee($shipping_code, $shipping_config, $goods_weight, $goods_amount, $goods_number='', $shipping_price=0)
{
    if (!is_array($shipping_config))
    {
        $shipping_config = unserialize($shipping_config);
    }

    $filename = ROOT_PATH . 'includes/modules/shipping/' . $shipping_code . '.php';
    if (file_exists($filename))
    {
        include_once($filename);
        
        $obj = new $shipping_code($shipping_config);

        return $obj->calculate($goods_weight, $goods_amount, $goods_number, $shipping_price);
    }
    else
    {
        return 0;
    }
}

/**
 * 获取指定配送的保价费用
 *
 * @access  public
 * @param   string      $shipping_code  配送方式的code
 * @param   float       $goods_amount   保价金额
 * @param   mix         $insure         保价比例
 * @return  float
 */
function shipping_insure_fee($shipping_code, $goods_amount, $insure)
{
    if (strpos($insure, '%') === false)
    {
        /* 如果保价费用不是百分比则直接返回该数值 */
        return floatval($insure);
    }
    else
    {
        $path = ROOT_PATH . 'includes/modules/shipping/' . $shipping_code . '.php';

        if (file_exists($path))
        {
            include_once($path);

            $shipping = new $shipping_code;
            $insure   = floatval($insure) / 100;

            if (method_exists($shipping, 'calculate_insure'))
            {
                return $shipping->calculate_insure($goods_amount, $insure);
            }
            else
            {
                return ceil($goods_amount * $insure);
            }
        }
        else
        {
            return false;
        }
    }
}

/**
 * 取得已安装的支付方式列表
 * @return  array   已安装的配送方式列表
 */
function payment_list()
{
    $sql = 'SELECT pay_id, pay_name , IFNULL(NULLIF(display_name,""), pay_name) as display_name ' .
            'FROM ' . $GLOBALS['ecs']->table('payment') .
            ' WHERE enabled = 1';
    $res = $GLOBALS['db']->getAll($sql);
    $res = localize_db_result('payment', $res);
    return $res;
}

/**
 * 取得支付方式信息
 * @param   int     $pay_id     支付方式id
 * @return  array   支付方式信息
 */
function payment_info($pay_id)
{
    $paymentController = new \Yoho\cms\Controller\PaymentController();
    $sql = 'SELECT * FROM ' . $GLOBALS['ecs']->table('payment') .
            " WHERE pay_id = '$pay_id' AND enabled = 1";
    $row = $GLOBALS['db']->getRow($sql);

    $pay_config = unserialize_config($row['pay_config']);
    $row = localize_db_row('payment', $row);
    $lang_pay_config = unserialize_config($row['pay_config']);
    $output =[];
    foreach($pay_config as $key => $value){
        if($key == 'bank_user')
            $user = $value;
        elseif($key == 'banks')
            $banks = $value;
    }
    if(!empty($user) && !empty($banks)) {
        $output = array('bank_user'=>$user,'banks'=>$banks);
        //Multi-language checking
        foreach($lang_pay_config as $key => $value){
            if($key == 'banks'){
                foreach($value as $k => $v){
                    $output['banks'][$k]['name'] = $v['name'];
                }
            } elseif ($key == 'tutorial') {
                $output['tutorial'] = $value;
            }
        }
        $pay_config_list = array_merge($pay_config, $output);
    } else {
        $pay_config_list = array_merge($pay_config, $lang_pay_config);
    }


    $row['pay_config_list'] = $pay_config_list;
    if(empty($row['display_name'])){
        $row['display_name'] = $row['pay_name'];
    }
    if (!empty($row['payment_logo'])) {
        $path = $paymentController::LOGO_PATH . $row['payment_logo'];
        $extname = strtolower(substr($row['payment_logo'], strrpos($row['payment_logo'], '.') + 1));
        if (in_array($extname, ['svg', 'SVG'])) {
            $row['payment_logo_svg'] = $paymentController->svgIconContent($path, $row['pay_code']);
        }
        $row['payment_logo'] = $path;
    } else {
        $row['payment_logo'] = null;
    }
    return $row;
}

/**
 * 获得订单需要支付的支付费用
 *
 * @access  public
 * @param   integer $payment_id
 * @param   float   $order_amount
 * @param   mix     $cod_fee
 * @return  float
 */
function pay_fee($payment_id, $order_amount, $cod_fee=null)
{
    $pay_fee = 0;
    $payment = payment_info($payment_id);
    $rate    = ($payment['is_cod'] && !is_null($cod_fee)) ? $cod_fee : $payment['pay_fee'];

    if (strpos($rate, '%') !== false)
    {
        /* 支付费用是一个比例 */
        $val     = floatval($rate) / 100;
        //$pay_fee = $val > 0 ? $order_amount * $val /(1- $val) : 0;
        
        // howang: Modified to support negative value, if negative value, we just simply apply the discount onto it
        $pay_fee = $order_amount * ($val > 0 ? $val / (1 - $val) : $val);
    }
    else
    {
        $pay_fee = floatval($rate);
    }

    // YOHO: payment fee no decimal places
    //return round($pay_fee, 2);
    return ceil($pay_fee);
}

/**
 * 取得包装列表
 * @return  array   包装列表
 */
function pack_list()
{
    $sql = 'SELECT * FROM ' . $GLOBALS['ecs']->table('pack');
    $res = $GLOBALS['db']->query($sql);

    $list = array();
    while ($row = $GLOBALS['db']->fetchRow($res))
    {
        $row['format_pack_fee'] = price_format($row['pack_fee'], false);
        $row['format_free_money'] = price_format($row['free_money'], false);
        $list[] = $row;
    }

    return $list;
}

/**
 * 取得包装信息
 * @param   int     $pack_id    包装id
 * @return  array   包装信息
 */
function pack_info($pack_id)
{
    $sql = "SELECT * FROM " . $GLOBALS['ecs']->table('pack') .
            " WHERE pack_id = '$pack_id'";

    return $GLOBALS['db']->getRow($sql);
}

/**
 * 根据订单中的商品总额来获得包装的费用
 *
 * @access  public
 * @param   integer $pack_id
 * @param   float   $goods_amount
 * @return  float
 */
function pack_fee($pack_id, $goods_amount)
{
    $pack = pack_info($pack_id);

    $val = (floatval($pack['free_money']) <= $goods_amount && $pack['free_money'] > 0) ? 0 : floatval($pack['pack_fee']);

    return $val;
}

/**
 * 取得贺卡列表
 * @return  array   贺卡列表
 */
function card_list()
{
    $sql = "SELECT * FROM " . $GLOBALS['ecs']->table('card');
    $res = $GLOBALS['db']->query($sql);

    $list = array();
    while ($row = $GLOBALS['db']->fetchRow($res))
    {
        $row['format_card_fee'] = price_format($row['card_fee'], false);
        $row['format_free_money'] = price_format($row['free_money'], false);
        $list[] = $row;
    }

    return $list;
}

/**
 * 取得贺卡信息
 * @param   int     $card_id    贺卡id
 * @return  array   贺卡信息
 */
function card_info($card_id)
{
    $sql = "SELECT * FROM " . $GLOBALS['ecs']->table('card') .
            " WHERE card_id = '$card_id'";

    return $GLOBALS['db']->getRow($sql);
}

/**
 * 根据订单中商品总额获得需要支付的贺卡费用
 *
 * @access  public
 * @param   integer $card_id
 * @param   float   $goods_amount
 * @return  float
 */
function card_fee($card_id, $goods_amount)
{
    $card = card_info($card_id);

    return ($card['free_money'] <= $goods_amount && $card['free_money'] > 0) ? 0 : $card['card_fee'];
}

/**
 * 取得订单信息
 * @param   int     $order_id   订单id（如果order_id > 0 就按id查，否则按sn查）
 * @param   string  $order_sn   订单号
 * @return  array   订单信息（金额都有相应格式化的字段，前缀是formated_）
 */
function order_info($order_id, $order_sn = '')
{
    $userController =new Yoho\cms\Controller\UserController();
    /* 计算订单各种费用之和的语句 */
    $total_fee = " (o.goods_amount - o.discount + o.tax + o.pack_fee + o.card_fee - o.coupon - o.integral_money) AS total_fee "; //+ o.shipping_fee + o.insure_fee + o.pay_fee
    $total_amount = "(o.money_paid + o.order_amount) as total_amount "; // howang: money_paid(已付金額) + order_amount(應付金額) = 訂單總金額
    $order_id = intval($order_id);
    $order_sn = intval($order_sn);
    if ($order_id > 0)
    {
        $sql = "SELECT o.*, p.pay_code as origin_pay_code, p.pay_name as origin_pay_name, " . $total_fee . ", " . $total_amount . ", p.is_online_pay, oa.dest_type AS address_cat, oa.lift AS address_lift, oa.verify AS address_verify, oadt.address_dest_type_cat AS cat_type FROM " . $GLOBALS['ecs']->table('order_info') ." as o " .
                " LEFT JOIN " . $GLOBALS['ecs']->table('payment') ." as p ON p.pay_id = o.pay_id " .
                " LEFT JOIN " . $GLOBALS['ecs']->table('order_address') ." as oa ON oa.order_address_id = o.address_id " .
                " LEFT JOIN " . $GLOBALS['ecs']->table('order_address_dest_type') ." as oadt ON oadt.address_dest_type_id = oa.dest_type " .
                " WHERE o.order_id = '$order_id'";
    }
    else
    {
        $sql = "SELECT o.*, p.pay_code as origin_pay_code, p.pay_name as origin_pay_name, " . $total_fee . ", " . $total_amount . ", p.is_online_pay, oa.dest_type AS address_cat, oa.lift AS address_lift, oa.verify AS address_verify, oadt.address_dest_type_cat AS cat_type FROM " . $GLOBALS['ecs']->table('order_info') ." as o " .
        " LEFT JOIN " . $GLOBALS['ecs']->table('payment') ." as p ON p.pay_id = o.pay_id " .
        " LEFT JOIN " . $GLOBALS['ecs']->table('order_address') ." as oa ON oa.order_address_id = o.address_id " .
        " LEFT JOIN " . $GLOBALS['ecs']->table('order_address_dest_type') ." as oadt ON oadt.address_dest_type_id = oa.dest_type " .
                " WHERE o.order_sn = '$order_sn'";
    }
    $order = $GLOBALS['db']->getRow($sql);

    if ($order && $order['order_id'])
    {
        // Get order coupons list
        $sql = "SELECT coupon_id FROM ". $GLOBALS['ecs']->table('order_coupons').
        " WHERE order_id = " .$order['order_id'];
        $order['coupon_id'] = $GLOBALS['db']->getCol($sql);

        // Get order tracking list
        $sql = "SELECT invoice_no ".
        "FROM ". $GLOBALS['ecs']->table('delivery_order').
        // "LEFT JOIN ". $GLOBALS['ecs']->table('logistics')." as l ON do.logistics_id = l.logistics_id ".
        " WHERE order_id = " .$order['order_id'];
        $res = $GLOBALS['db']->getCol($sql);
        $order['invoice_no'] = implode(",", $res);
        $order['invoice_no'] = str_replace(" ","", $order['invoice_no']);
        /* 格式化金额字段 */

        $order['is_wholesale']            = ((in_array($order['user_id'],$userController->getWholesaleUserIds())||(isset($order['order_type']) && $order['order_type']==Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE))?true:false);
        $order['formated_goods_amount']   = price_format($order['goods_amount'], false);
        $order['formated_discount']       = price_format($order['discount'], false);
        $order['formated_tax']            = price_format($order['tax'], false);
        $order['formated_shipping_fee']   = price_format($order['shipping_fee'], false);
        $order['formated_insure_fee']     = price_format($order['insure_fee'], false);
        $order['formated_pay_fee']        = price_format($order['pay_fee'], false);
        $order['formated_pack_fee']       = price_format($order['pack_fee'], false);
        $order['formated_card_fee']       = price_format($order['card_fee'], false);
        $order['formated_total_fee']      = price_format($order['total_fee'], false);
        $order['formated_money_paid']     = price_format($order['money_paid'], false);
        $order['formated_bonus']          = price_format($order['bonus'], false);
        $order['formated_coupon']         = price_format($order['coupon'], false);
        $order['formated_integral_money'] = price_format($order['integral_money'], false);
        $order['formated_surplus']        = price_format($order['surplus'], false);
        $order['formated_cn_ship_tax']        = price_format($order['cn_ship_tax'], false);
        $order['formated_order_amount']   = price_format(abs($order['order_amount']), false);
        $order['formated_total_amount']   = price_format(abs($order['total_amount']), false);
        $order['formated_add_time']       = local_date($GLOBALS['_CFG']['time_format'], $order['add_time']);
        $order['formated_pay_time']       = local_date($GLOBALS['_CFG']['time_format'], $order['pay_time']);
        $order['formated_shipping_time']  = local_date($GLOBALS['_CFG']['time_format'], $order['shipping_time']);
        
        /* Get Address from order_address table */
        if(!empty($order['address_id'])){
            $address = new \Yoho\cms\Model\OrderAddress($order['address_id']);
            if(!empty($address->data)) $order = array_merge($order, $address->data);
        }
        $region_names = get_address_region_name($order['province'], $order['city'], $order['district']);
        $order['province_name'] = $region_names['province'];
        $order['city_name'] = $region_names['city'];
        $order['district_name'] = $region_names['district'];
        $order['ssq'] = $region_names['ssq'];
        $order['order_gnum'] = get_ly_dingdannum($order['order_id']);
        
        if ($order['pay_fee'] < 0)
        {
            $order['formated_pay_discount'] = price_format(-$order['pay_fee'], false);
        }

        if ($order['order_type'] == \Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT)
        {
            $salesagent = new \Yoho\cms\Model\Salesagent($order['type_id']);
            $order['is_sa'] = true;
            $order['sa_name'] = $salesagent->data['name'];
            $order['sa_sn'] = $order['type_sn'];
        }

        $orderController = new \Yoho\cms\Controller\OrderController();
        $order['data_postscript'] = $orderController->handle_ordering_remark($order['postscript'], $order['pay_status'], $order['pay_time'], $order['order_id']);
        $order['data_order_remarks'] = $orderController->handle_ordering_remark($order['order_remarks'], $order['pay_status'], $order['pay_time'], $order['order_id']);
        $order['shipping_name'] = $GLOBALS['db']->getOne("SELECT shipping_name FROM " . $GLOBALS['ecs']->table("shipping") . " WHERE shipping_id = $order[shipping_id]");
    }
    return $order;
}

/**
 * 判断订单是否已完成
 * @param   array   $order  订单信息
 * @return  bool
 */
function order_finished($order)
{
    return $order['order_status']  == OS_CONFIRMED &&
        ($order['shipping_status'] == SS_SHIPPED || $order['shipping_status'] == SS_RECEIVED) &&
        ($order['pay_status']      == PS_PAYED   || $order['pay_status'] == PS_PAYING);
}

/**
 * 取得订单商品
 * @param   int     $order_id   订单id
 * @return  array   订单商品数组
 */
function order_goods($order_id, $all_detail = false)
{
    $packageController = new Yoho\cms\Controller\PackageController();
    $packages = [];

    $sql = "SELECT og.rec_id, og.goods_id, og.goods_name, og.goods_sn, og.market_price, og.goods_number, og.send_number, og.delivery_qty, " .
            "og.goods_price, og.goods_attr, og.is_real, og.parent_id, og.is_gift, og.is_package, og.is_insurance, og.insurance_of, og.sa_rate_id, " .
            "(og.goods_price * og.goods_number) AS subtotal, og.extension_code, sar.sa_rate, " .
            "(g.integral * og.goods_number) AS max_integral_amount, og.flashdeal_id,g.brand_id,g.cat_id, gperma ".
            "FROM " . $GLOBALS['ecs']->table('order_goods') . "as og ".
            "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON g.goods_id = og.goods_id " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('sales_agent_rate') . " as sar ON og.sa_rate_id = sar.sa_rate_id ".
            "LEFT JOIN (SELECT id, table_name, lang, perma_link as gperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") gpl ON gpl.id = g.goods_id  AND gpl.table_name = 'goods' AND gpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
            " WHERE order_id = '$order_id'";

    $res = $GLOBALS['db']->query($sql);

    while ($row = $GLOBALS['db']->fetchRow($res))
    {
        if ($row['extension_code'] == 'package')
        {
            if (!in_array($row['is_package'], array_keys($packages))) {
                $packages[$row['is_package']] = $packageController->get_basic_package_info($row['is_package']);
                $packages[$row['is_package']]['qty'] = $row['goods_number'] / $packages[$row['is_package']]['items'][$row['goods_id']]['goods_number'];
                $packages[$row['is_package']]['total_price'] = bcmul($packages[$row['is_package']]['qty'], $packages[$row['is_package']]['base_price'], 2);
                $packages[$row['is_package']]['formated_total_price'] = price_format($packages[$row['is_package']]['total_price'], false);
            }
            $row['package_goods_list'] = $packages[$row['is_package']]['items'];
        }
        $row['max_integral'] = integral_of_value($row['max_integral_amount']);
        
        if ($row['is_insurance'] == 1 && $row['insurance_of'] > 0){
            $sql = "SELECT goods_name, goods_sn ".
                    "FROM ".$GLOBALS['ecs']->table('order_goods')." ".
                    "WHERE order_id = ".$order_id." ".
                    "AND goods_id = '".$row['insurance_of']."' ".
                    "AND extension_code != 'package' ".
                    "AND is_package = 0 ".
                    "AND is_insurance = 0 ".
                    "AND insurance_of = 0 ".
                    "AND is_gift = 0 ";
            $result = $GLOBALS['db']->getRow($sql);
            if (!empty($result)){
                $row['insurance_of_name'] = $result['goods_name'];
                $row['insurance_of_sn'] = $result['goods_sn'];
                
                $sql = "SELECT code ".
                "FROM ".$GLOBALS['ecs']->table('insurance')." ".
                "WHERE target_goods_id = '".$row['goods_id']."' ";
                $result = $GLOBALS['db']->getOne($sql);
                $row['tc_link'] = $GLOBALS['_LANG']['goods_buy_service_terms_conditions_link_'.$result];
            }
        }

        $goods_list[] = $row;
    }
    //return $GLOBALS['db']->getAll($sql);
    if ($all_detail) {
        return ["goods" => $goods_list, "packages" => $packages];
    }
    return $goods_list;
}

/**
 * 取得订单总金额
 * @param   int     $order_id   订单id
 * @param   bool    $include_gift   是否包括赠品
 * @return  float   订单总金额
 */
function order_amount($order_id, $include_gift = true)
{
    $sql = "SELECT SUM(goods_price * goods_number) " .
            "FROM " . $GLOBALS['ecs']->table('order_goods') .
            " WHERE order_id = '$order_id'";
    if (!$include_gift)
    {
        $sql .= " AND is_gift = 0";
    }

    return floatval($GLOBALS['db']->getOne($sql));
}

function cp_price($order_id)
{
    $sql = "SELECT SUM(cost * goods_number) " .
            "FROM " . $GLOBALS['ecs']->table('order_goods') .
            " WHERE order_id = '$order_id'";

    return floatval($GLOBALS['db']->getOne($sql));
}

/**
 * 取得某订单商品总重量和总金额（对应 cart_weight_price）
 * @param   int     $order_id   订单id
 * @return  array   ('weight' => **, 'amount' => **, 'formated_weight' => **)
 */
function order_weight_price($order_id, $cal_free_shipping_goods = false)
{
    // 
    // // Only include those without shipping price
    // $sql = "SELECT SUM(g.goods_weight * o.goods_number) AS weight, " .
    //             "SUM(o.goods_price * o.goods_number) AS amount, " .
    //             "SUM(o.goods_number) AS number " .
    //         "FROM " . $GLOBALS['ecs']->table('order_goods') . " AS o " .
    //         "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " AS g ON o.goods_id = g.goods_id " .
    //         "WHERE o.order_id = '$order_id' " .
    //         "AND g.shipping_price = 0";
    // $row = $GLOBALS['db']->getRow($sql);
    // $row['weight'] = floatval($row['weight']);
    // $row['amount'] = floatval($row['amount']);
    // $row['number'] = intval($row['number']);
    // 
    // // Add shipping price
    // $sql = "SELECT SUM(g.shipping_price * o.goods_number) AS price " .
    //         "FROM " . $GLOBALS['ecs']->table('order_goods') . " AS o " .
    //         "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " AS g ON o.goods_id = g.goods_id " .
    //         "WHERE o.order_id = '$order_id' " .
    //         "AND g.shipping_price > 0";
    // $row2 = $GLOBALS['db']->getRow($sql);
    // $row['price'] = floatval($row2['price']);
    // 
    // /* 格式化重量 */
    // $row['formated_weight'] = formated_weight($row['weight']);
    // 
    // return $row;
    // Handle goods marked as free_shipping by coupon
    global $coupon_free_shipping_goods;
    if (!$coupon_free_shipping_goods)
    {
        $coupon_free_shipping_goods = array();
    }
    
    $package_row['weight'] = 0;
    $package_row['amount'] = 0;
    $package_row['number'] = 0;
    $package_row['price'] = 0;

    $packages_row['free_shipping'] = 1;
    /* 计算超值礼包内商品的相关配送参数 */
    $sql = 'SELECT goods_id, goods_number, goods_price FROM ' . $GLOBALS['ecs']->table('order_goods') . " WHERE extension_code = 'package' AND order_id = '" . $order_id . "'";
    $row = $GLOBALS['db']->getAll($sql);
    if ($row)
    {
        $packages_row['free_shipping'] = 0;
        $free_shipping_count = 0;
        foreach ($row as $val)
        {
            // 如果商品全为免运费商品，设置一个标识变量
            $sql = 'SELECT count(*) FROM ' .
                    $GLOBALS['ecs']->table('package_goods') . ' AS pg, ' .
                    $GLOBALS['ecs']->table('goods') . ' AS g ' .
                    "WHERE g.goods_id = pg.goods_id AND g.is_shipping = 0 AND pg.package_id = '"  . $val['goods_id'] . "' " .
                    "AND g.goods_id NOT " . db_create_in($coupon_free_shipping_goods);
            $shipping_count = $GLOBALS['db']->getOne($sql);
            if ($shipping_count > 0)
            {
                // 循环计算每个超值礼包商品的重量和数量，注意一个礼包中可能包换若干个同一商品
                // Only include those without shipping price
                $sql = "SELECT sum(g.`goods_weight` * pg.`goods_number`) as weight, " .
                            "sum(pg.`goods_number`) as number " .
                        "FROM " . $GLOBALS['ecs']->table('package_goods') . " as pg " .
                        "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON g.goods_id = pg.goods_id " .
                        "WHERE ".($cal_free_shipping_goods ? "g.is_shipping = 0" : " 1 " ) ." AND g.shipping_price = 0 AND pg.package_id = '"  . $val['goods_id'] . "' " .
                        "AND g.goods_id NOT " . db_create_in($coupon_free_shipping_goods);
                $goods_row = $GLOBALS['db']->getRow($sql);
                $package_row['weight'] += floatval($goods_row['weight']) * $val['goods_number'];
                $package_row['amount'] += floatval($val['goods_price']) * $val['goods_number'];
                $package_row['number'] += intval($goods_row['number']) * $val['goods_number'];
                
                // Add shipping price
                $sql = "SELECT sum(g.`shipping_price` * pg.`goods_number`) as price " .
                        "FROM " . $GLOBALS['ecs']->table('package_goods') . " as pg " .
                        "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON g.goods_id = pg.goods_id " .
                        "WHERE ".($cal_free_shipping_goods ? "g.is_shipping = 0" : " 1 " ) ." AND g.shipping_price > 0 AND pg.package_id = '"  . $val['goods_id'] . "' " .
                        "AND g.goods_id NOT " . db_create_in($coupon_free_shipping_goods);
                $shipping_price = $GLOBALS['db']->getOne($sql);
                $package_row['price'] += floatval($shipping_price) * $val['goods_number'];
            }
            else
            {
                $free_shipping_count++;
            }
        }

        $packages_row['free_shipping'] = $free_shipping_count == count($row) ? 1 : 0;
        $packages_row['free_shipping'] = ($cal_free_shipping_goods) ? $packages_row['free_shipping'] : 0;
    }

    /* 获得购物车中非超值礼包商品的总重量 */
    // Only include those without shipping price
    $sql = "SELECT   sum(g.`goods_weight` * c.`goods_number`) as weight, " .
                    "sum(c.`goods_price` * c.`goods_number`) as amount, " .
                    "sum(c.`goods_number`) as number " .
            "FROM " . $GLOBALS['ecs']->table('order_goods') . " as c " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON g.goods_id = c.goods_id " .
            "WHERE c.order_id = '" . $order_id . "' " .
            "AND ".($cal_free_shipping_goods ? "g.is_shipping = 0" : " 1 " ) ." AND g.shipping_price = 0 AND c.extension_code != 'package' " .
            "AND g.goods_id NOT " . db_create_in($coupon_free_shipping_goods);
    $row = $GLOBALS['db']->getRow($sql);
    $packages_row['weight'] += floatval($row['weight']);
    $packages_row['amount'] += floatval($row['amount']);
    $packages_row['number'] += intval($row['number']);
    
    // Add shipping price
    $sql = "SELECT sum(g.`shipping_price` * c.`goods_number`) as price " .
            "FROM " . $GLOBALS['ecs']->table('order_goods') . " as c " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON g.goods_id = c.goods_id " .
            "WHERE c.order_id = '" . $order_id . "' " .
            "AND ".($cal_free_shipping_goods ? "g.is_shipping = 0" : " 1 " ) ." AND g.shipping_price > 0 AND c.extension_code != 'package' " .
            "AND g.goods_id NOT " . db_create_in($coupon_free_shipping_goods);
    $shipping_price = $GLOBALS['db']->getOne($sql);
    $packages_row['price'] += floatval($shipping_price);

    // If we cal free shipping goods, include price( If not cal free shipping goods, price is also in amount )
    if ($cal_free_shipping_goods) {
        // Update 2016-08-28: Also include the price for free shipping products into calculation
        $sql = "SELECT   sum(c.`goods_price` * c.`goods_number`) as amount " .
                "FROM " . $GLOBALS['ecs']->table('order_goods') . " as c " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON g.goods_id = c.goods_id " .
                "WHERE c.order_id = '" . $order_id . "' " .
                " AND g.shipping_price = 0 AND c.extension_code != 'package' " .
                "AND (g.is_shipping = 1 OR g.goods_id " . db_create_in($coupon_free_shipping_goods) . ") ";
        $row = $GLOBALS['db']->getRow($sql);
        $packages_row['amount'] += floatval($row['amount']);
        /* 格式化重量 */
        $packages_row['formated_weight'] = formated_weight($packages_row['weight']);
    }
    return $packages_row;
    
}

/** This function is unused. Please use Yoho\cms\Controller\OrderController->order_fee_flow($order, $goods, $consignee, $platform)
 * 获得订单中的费用信息
 *
 * @access  public
 * @param   array   $order
 * @param   array   $goods
 * @param   array   $consignee
 * @param   bool    $is_gb_deposit  是否团购保证金（如果是，应付款金额只计算商品总额和支付费用，可以获得的积分取 $gift_integral）
 * @return  array
 */
function order_fee($order, $goods, $consignee)
{
    /* Step 1 : Initialize extension_code, group_buy (Unused in YOHO). */
    if (!isset($order['extension_code']))
    {
        $order['extension_code'] = '';
    }

    if ($order['extension_code'] == 'group_buy')
    {
        $group_buy = group_buy_info($order['extension_id']);
    }

    /* Step 2 : Initialize order. */
    $total  = array('real_goods_count'   => 0,
                    'gift_amount'        => 0,
                    'goods_price'        => 0,
                    'market_price'       => 0,
                    'discount'           => 0,
                    'pack_fee'           => 0,
                    'card_fee'           => 0,
                    'shipping_fee'       => 0,
                    'shipping_insure'    => 0,
                    'integral_money'     => 0,
                    'bonus'              => 0,
                    'coupon'             => 0,
                    'surplus'            => 0,
                    'cp_price'           => 0,
                    'cod_fee'            => 0,
                    'pay_fee'            => 0,
                    'tax'                => 0,
                    'goods_max_integral' => 0);
    $weight = 0;

    /* Step 3 : Calculate goods total price. */
    foreach ($goods AS $val)
    {
        // Count real goods
        if ($val['is_real'])
        {
            $total['real_goods_count']++;
        }
        
        $total['goods_price']  += $val['goods_price'] * $val['goods_number'];
        $total['market_price'] += $val['market_price'] * $val['goods_number'];
		$total['goods_max_integral'] += $val['max_integral'];
		// Calculate the item cost
		$sql = "SELECT cost FROM " . $GLOBALS['ecs']->table('goods') . " WHERE goods_id = '" . $val['goods_id'] . "'";
		$cp_price = $GLOBALS['db']->getOne($sql);
		$total['cp_price'] += round($cp_price * $val['goods_number'], 2);
    }
    $total['goods_max_integral_amount'] = value_of_integral($total['goods_max_integral']);
    $total['saving']    = $total['market_price'] - $total['goods_price'];
    $total['save_rate'] = $total['market_price'] ? round($total['saving'] * 100 / $total['market_price']) . '%' : 0;

    $total['goods_price_formated']  = price_format($total['goods_price'], false);
    $total['market_price_formated'] = price_format($total['market_price'], false);
    $total['saving_formated']       = price_format($total['saving'], false);

    /* Step 4 : Calculate order discount. */
    if ($order['extension_code'] != 'group_buy')
    {
        $discount = compute_discount();
        $total['discount'] = $discount['discount'];
        if ($total['discount'] > $total['goods_price'])
        {
            $total['discount'] = $total['goods_price'];
        }
    }
    $total['discount_formated'] = price_format($total['discount'], false);

    /* Step 5 : Calculate order tax (Unused in YOHO). */
    if (!empty($order['need_inv']) && $order['inv_type'] != '')
    {
        /* 查税率 */
        $rate = 0;
        foreach ($GLOBALS['_CFG']['invoice_type']['type'] as $key => $type)
        {
            if ($type == $order['inv_type'])
            {
                $rate = floatval($GLOBALS['_CFG']['invoice_type']['rate'][$key]) / 100;
                break;
            }
        }
        if ($rate > 0)
        {
            $total['tax'] = $rate * $total['goods_price'];
        }
    }
    $total['tax_formated'] = price_format($total['tax'], false);

    /* Step 6 : Calculate order pack fee(包裝) (Unused in YOHO). */
    if (!empty($order['pack_id']))
    {
        $total['pack_fee']      = pack_fee($order['pack_id'], $total['goods_price']);
    }
    $total['pack_fee_formated'] = price_format($total['pack_fee'], false);

    /* Step 7 : Calculate order card fee(賀卡) (Unused in YOHO). */
    if (!empty($order['card_id']))
    {
        $total['card_fee']      = card_fee($order['card_id'], $total['goods_price']);
    }
    $total['card_fee_formated'] = price_format($total['card_fee'], false);

    /* Step 8 : Calculate order bonus(紅包) (Unused in YOHO). */
    if (!empty($order['bonus_id']))
    {
        $bonus          = bonus_info($order['bonus_id']);
        $total['bonus'] = $bonus['type_money'];
    }
    $total['bonus_formated'] = price_format($total['bonus'], false);

    /* Step 9 : Calculate order bonus kill(線下紅包) (Unused in YOHO). */
    if (!empty($order['bonus_kill']))
    {
        $bonus                        = bonus_info(0,$order['bonus_kill']);
        $total['bonus_kill']          = $order['bonus_kill'];
        $total['bonus_kill_formated'] = price_format($total['bonus_kill'], false);
    }
    
    /* Step 10: Calculate Coupon. */
    $total['coupon'] = 0;
    if (!empty($order['coupon_id']))
    {
        // initial coupon value
        $coupons_flat          = array();
        $coupons_percent       = array();
        $coupons_free_shipping = array();
        
        // Loop order coupon id
        foreach ($order['coupon_id'] as $coupon_id)
        {
            // Get coupon info
            $coupon = coupon_info($coupon_id);
            if ($coupon)
            {
                // If coupon is deduct amount.
                if ($coupon['coupon_amount'] > 0)
                {
                    $coupons_flat[] = $coupon;
                }
                // If coupon is a percent coupon.
                elseif ($coupon['coupon_discount'] > 0)
                {
                    $coupons_percent[] = $coupon;
                }
                // If coupon is free shipping coupon.
                if ($coupon['free_shipping'])
                {
                    $coupons_free_shipping[] = $coupon;
                }
            }
        }
        // Process flat discount first, then percentage discount
        foreach ($coupons_flat as $coupon)
        {
            $total['coupon'] += $coupon['coupon_amount'];
        }
        foreach ($coupons_percent as $coupon)
        {
            if ($coupon['cartwide']) // percentage apply to all products in cart
            {
                $total['coupon'] += floor(($coupon['coupon_discount'] / 100) * $total['goods_price']);
            }
            else
            {
                $coupon_amount = 0;
                $coupon_goods = array_reduce($coupon['goods_list'], 'array_merge', array());
                foreach ($goods AS $g)
                {
                    if (in_array($g['goods_id'], $coupon_goods))
                    {
                        $coupon_amount += floor(($coupon['coupon_discount'] / 100) * $g['goods_price'] * $g['goods_number']);
                    }
                }
                $total['coupon'] += $coupon_amount;
            }
        }
    }
    $total['coupon_formated'] = price_format($total['coupon'], false);
    
    /* Step 11: Calculate goods can use the total amount of bonus paid. */
    $bonus_amount = compute_discount_amount();
    /* Step 12: Calculate the maximum amount of bonus and integral that can be paid is the total amount of goods. */
    $max_amount = $total['goods_price'] == 0 ? $total['goods_price'] : $total['goods_price'] - $bonus_amount;
    
    /* Step 13: Calculate the initial order amount. */
    // If it's a group buy order, order amount = total goods price.
    if ($order['extension_code'] == 'group_buy' && $group_buy['deposit'] > 0)
    {
        $total['amount'] = $total['goods_price'];
    }
    else // else, calculate the initial order amount.
    {
        // Calculate the initial order amount.(Without shipping in this step.) Edited:Anthony@YOHO 2017-06-09
        // $total['amount'] = $total['goods_price'] - $total['discount'] + $total['tax'] + $total['pack_fee'] + $total['card_fee'] +
        // $total['shipping_fee'] + $total['shipping_insure'] + $total['cod_fee'];
        //** YOHO: $total['amount'] = goods_price - discount
        $total['amount']  = $total['goods_price'] - $total['discount'] + $total['tax'] + $total['pack_fee'] + $total['card_fee'];
        
        // Calculate the initial order amount without shipping.
        $total['amount3'] = $total['goods_price'] - $total['discount'] + $total['tax'] + $total['pack_fee'] + $total['card_fee'];
        // Deduct bonus amount
        $use_bonus = min($total['bonus'], $max_amount);
        
        // Deduct bonus kill amount
        if(isset($total['bonus_kill']))
        {
            $use_bonus_kill       = min($total['bonus_kill'], $max_amount);
            $total['bonus_kill']  = $use_bonus_kill;
            $total['amount']     -= number_format($total['bonus_kill'], 2, '.', '');
            $max_amount          -= $use_bonus_kill; // Calculate max amount bonus can use
        }
        $total['bonus']          = $use_bonus;
        $total['bonus_formated'] = price_format($total['bonus'], false);
        $total['amount']        -= $use_bonus; // Update new order amount
        $max_amount             -= $use_bonus; // Calculate max amount coupon can use
        
        // Deduct coupon amount
        $use_coupon               = min($total['coupon'], $max_amount); // Actual coupon amount used
        $total['coupon']          = $use_coupon;
        $total['coupon_formated'] = price_format($total['coupon'], false);
        $total['amount']         -= $use_coupon; // Update new order amount
        $max_amount              -= $use_coupon; // Calculate max amount integral can use
    }

    /* Step 14: Calculate surplus(餘額) (Unused in YOHO). */
    $order['surplus'] = $order['surplus'] > 0 ? $order['surplus'] : 0;
    if ($total['amount'] > 0)
    {
        if (isset($order['surplus']) && $order['surplus'] > $total['amount'])
        {
            $order['surplus'] = $total['amount'];
            $total['amount']  = 0;
        }
        else
        {
            $total['amount'] -= floatval($order['surplus']);
        }
    }
    else
    {
        $order['surplus'] = 0;
        $total['amount']  = 0;
    }
    $total['surplus'] = $order['surplus'];
    $total['surplus_formated'] = price_format($order['surplus'], false);
    
    /* Step 15: Calculate integral(積分/user pay_point). */
    $max_amount = min($total['goods_max_integral_amount'], $max_amount);
    $order['integral'] = $order['integral'] > 0 ? $order['integral'] : 0;
    //** $total['amount'] = goods_price - discount - tax + pack_fee + card_fee - bonus_kill(if have) - bonus - coupon - surplus
    //** YOHO: $total['amount'] = goods_price - discount - coupon
    if ($total['amount'] > 0 && $max_amount > 0 && $order['integral'] > 0)
    {
        $integral_money = value_of_integral($order['integral']);
        // Use integral pay
        $use_integral            = min($total['amount'], $max_amount, $integral_money);
        $total['amount']        -= $use_integral;
        $total['integral_money'] = $use_integral;
        $order['integral']       = integral_of_value($use_integral);
    }
    else
    {
        $total['integral_money'] = 0;
        $order['integral']       = 0;
    }
    $total['max_amount'] = $max_amount;
    $total['max_integral'] = integral_of_value($max_amount);
    $total['integral'] = $order['integral'];
    $total['integral_formated'] = price_format($total['integral_money'], false);

    /* Step 16: Save order flow into session. */
    $_SESSION['flow_order'] = $order;
    $se_flow_type = isset($_SESSION['flow_type']) ? $_SESSION['flow_type'] : '';
    
    /* Step 17: Calculate pay fee(支付折扣). */
    // If have order pay_id, have real goods, calculate pay fee.
    if (!empty($order['pay_id']) && ($total['real_goods_count'] > 0 || $se_flow_type != CART_EXCHANGE_GOODS))
    {
        //** $total['amount'] = goods_price - discount - tax + pack_fee + card_fee - bonus_kill(if have) - bonus - coupon - surplus - integral_money
        //** YOHO: $total['amount'] = goods_price - discount - coupon - integral_money
        $total['pay_fee']      = pay_fee($order['pay_id'], $total['amount']);
    }
    $total['pay_fee_formated']      = price_format($total['pay_fee'], false);
    $total['pay_discount_formated'] = price_format(-$total['pay_fee'], false);
    $total['amount']               += $total['pay_fee']; // Order amount + pay fee
    
    /* Step 18: Calculate shipping fee(運費). */
    $shipping_cod_fee = NULL;
    
    // If order have shipping id, have real goods, and order type is not sale agent, calculate shipping fee.
    if ($order['shipping_id'] > 0 && $total['real_goods_count'] > 0 && !in_array($order['order_type'], [Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT]))
    {
        $region['country']  = $consignee['country'];
        $region['province'] = $consignee['province'];
        $region['city']     = $consignee['city'];
        $region['district'] = $consignee['district'];
        $shipping_info      = shipping_area_info($order['shipping_id'], $region);
        
        if (!empty($shipping_info))
        {
            // Process free shipping coupons
            global $coupon_free_shipping_goods;
            $coupon_free_shipping_goods = array();
            if (!empty($order['coupon_id']))
            {
                foreach ($coupons_free_shipping as $coupon)
                {
                    if ($coupon['cartwide']) // percentage apply to all products in cart
                    {
                        foreach ($goods AS $g)
                        {
                            $coupon_free_shipping_goods[] = $g['goods_id'];
                        }
                    }
                    else
                    {
                        $coupon_goods = array_reduce($coupon['goods_list'], 'array_merge', array());
                        foreach ($goods AS $g)
                        {
                            if (in_array($g['goods_id'], $coupon_goods))
                            {
                                $coupon_free_shipping_goods[] = $g['goods_id'];
                            }
                        }
                    }
                }
                $coupon_free_shipping_goods = array_unique($coupon_free_shipping_goods);
            }
            
            if ($order['extension_code'] == 'group_buy')
            {
                $weight_price = cart_weight_price(CART_GROUP_BUY_GOODS);
            }
            else
            {
                $weight_price = cart_weight_price();
            }
            
            // 查看购物车中是否全为免运费商品，若是则把运费赋为零
            $sql = 'SELECT count(*) FROM ' . $GLOBALS['ecs']->table('cart') . " WHERE  `session_id` = '" . SESS_ID. "' AND `extension_code` != 'package' AND `is_shipping` = 0";
            $shipping_count = $GLOBALS['db']->getOne($sql);
            
            if ($shipping_count == 0 && $weight_price['free_shipping'] == 1)
            {
                $total['shipping_fee'] = 0;
            }
            else
            {
                $total['shipping_fee'] = shipping_fee($shipping_info['shipping_code'], $shipping_info['configure'], $weight_price['weight'], $weight_price['amount'], $weight_price['number'], $weight_price['price']);
            }
            
            if (!empty($order['need_insure']) && $shipping_info['insure'] > 0)
            {
                $total['shipping_insure'] = shipping_insure_fee($shipping_info['shipping_code'], $total['goods_price'], $shipping_info['insure']);
            }
            else
            {
                $total['shipping_insure'] = 0;
            }
            
            if ($shipping_info['support_cod'])
            {
                $shipping_cod_fee = $shipping_info['pay_fee'];
                $total['cod_fee'] = $shipping_cod_fee;
            }
            
            // If user wants to pay fee on delivery, set shipping_fee to 0
            // but if the user selected some product with its own shipping charge, we have to include them back
            if ($order['shipping_fod'])
            {
                $total['shipping_fee'] = 0 + $weight_price['price'];
            }
        }
    }
    
    $total['shipping_fee_formated']    = price_format($total['shipping_fee'], false);
    $total['shipping_insure_formated'] = price_format($total['shipping_insure'], false);
    // Order amount calculate by shipping.
    //** $total['amount'] = goods_price - discount - tax + pack_fee + card_fee - bonus_kill(if have) - bonus - coupon - surplus - integral_money + pay_fee
    //** YOHO: $total['amount'] = goods_price - discount + cod_fee - coupon - integral_money + pay_fee
    $total['amount'] += $total['shipping_fee'] + $total['shipping_insure'] + $total['cod_fee'];
    
    // Formated amount.
    $total['amount_formated']  = price_format($total['amount'], false);
    $total['amount_formated2'] = price_format($total['goods_price'], false); //sbsn ly 0323 更改显示总价的算法
    $total['amount_formated3'] = price_format($total['amount3'], false); //sbsn ly 0723 更改显示总价的算 不包括邮费
    $total['weight']           = $weight_price['weight']; //增加重量显示 sbsn ly 0323

    /* Step 19: Calculate user points and bouns (bouns is unused in YOHO). */
    if ($order['extension_code'] == 'group_buy')
    {
        $total['will_get_integral'] = $group_buy['gift_integral'];
    }
    elseif ($order['extension_code'] == 'exchange_goods')
    {
        $total['will_get_integral'] = 0;
    }
    else
    {
        $total['will_get_integral'] = get_give_integral($goods);
    }
    $total['will_get_bonus']        = $order['extension_code'] == 'exchange_goods' ? 0 : price_format(get_total_bonus(), false);
    $total['formated_goods_price']  = price_format($total['goods_price'], false);
    $total['formated_market_price'] = price_format($total['market_price'], false);
    $total['formated_saving']       = price_format($total['saving'], false);

    if ($order['extension_code'] == 'exchange_goods')
    {
        $sql = 'SELECT SUM(eg.exchange_integral) '.
               'FROM ' . $GLOBALS['ecs']->table('cart') . ' AS c,' . $GLOBALS['ecs']->table('exchange_goods') . 'AS eg '.
               "WHERE c.goods_id = eg.goods_id AND c.session_id= '" . SESS_ID . "' " .
               " AND c.rec_type = '" . CART_EXCHANGE_GOODS . "' " .
               ' AND c.is_gift = 0 AND c.goods_id > 0 ' .
               'GROUP BY eg.goods_id';
        $exchange_integral = $GLOBALS['db']->getOne($sql);
        $total['exchange_integral'] = $exchange_integral;
    }
    
    /* Step 20: Return order fee. */
    return $total;
}

/**
 * 修改订单
 * @param   int     $order_id   订单id
 * @param   array   $order      key => value
 * @return  bool
 */
function update_order($order_id, $order)
{
    // Add escape
    // case 1: "a\'s" -> "a\'s"
    // case 2: "a's"  -> "a\'s"
    foreach ($order as $key => $value)
    {
        if(is_string($value))
        {
            // If value have escape, remove first(magic_quotes_gpc is on, so input: ' change to \', but other cases just only: ' to ' ).
            $value       = stripslashes($value);
            // All value add escape if need.
            $value       = mysql_real_escape_string($value);
            $order[$key] = $value;
        }
    }
    return $GLOBALS['db']->autoExecute($GLOBALS['ecs']->table('order_info'),
        $order, 'UPDATE', "order_id = '$order_id'");
}

/**
 * 得到新订单号
 * @return  string
 */
function get_order_sn()
{
    /* 选择一个随机的方案 */
    mt_srand((double) microtime() * 1000000);

    return date('Ymd') . str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT);
}

/**
 * 取得购物车商品
 * @param   int     $type   类型：默认普通商品
 * @return  array   购物车商品数组
 */
function cart_goods($type = CART_GENERAL_GOODS, $all_detail = false)
{
    $packageController = new Yoho\cms\Controller\PackageController();

    $sql = "SELECT g.goods_thumb, c.rec_id, c.user_id, c.goods_id, c.goods_name, c.goods_sn, c.goods_number, c.parent_id,g.brand_id,g.cat_id, gperma, " .
                " (g.integral * c.goods_number) AS max_integral_amount, ".
                "c.market_price, c.goods_price, c.goods_attr, c.is_real, c.extension_code, c.is_gift, c.is_package, c.is_shipping, c.flashdeal_id, c.is_insurance, c.insurance_of, " .
                "c.goods_price * c.goods_number AS subtotal, " .
                "g.goods_length, g.goods_width, g.goods_height, ca.default_length, ca.default_width, ca.default_height, g.require_goods_id ".
            "FROM " . $GLOBALS['ecs']->table('cart') . " as c " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON g.goods_id = c.goods_id " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('category') . " as ca ON ca.cat_id = g.cat_id " .
            "LEFT JOIN (SELECT id, table_name, lang, perma_link as gperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") gpl ON gpl.id = g.goods_id  AND gpl.table_name = 'goods' AND gpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
            "WHERE session_id = '" . SESS_ID . "' " .
            "AND rec_type = '$type'";


    
    $arr = $GLOBALS['db']->getAll($sql);

    // handle products require goods
    $goods_ids = [];
    $require_goods_ids = [];
    $packages = [];
    foreach ($arr as $key => $value) {
        $goods_ids[] = $value['goods_id'];
        if (!empty($value['require_goods_id'])) {
            $require_goods_ids[] = $value['require_goods_id'];
        }
    }
    foreach ($require_goods_ids as $goods_id => $require_goods_id) {
        if (!in_array($require_goods_id, $goods_ids)) {
            $missing_require_goods[] = $goods_id;
        }
    }

    /* 格式化价格及礼包商品 */
    foreach ($arr as $key => $value)
    {
        $arr[$key]['price']                 = $value['goods_price'];
        $arr[$key]['formated_market_price'] = price_format($value['market_price'], false);
        $arr[$key]['formated_goods_price']  = price_format($value['goods_price'], false);
        $arr[$key]['formated_subtotal']     = price_format($value['subtotal'], false);
        $arr[$key]['goods_thumb']           = get_image_path($value['goods_id'], $value['goods_thumb'], true); //ly sbsn 增加订单中图片显示
        $arr[$key]['url']                   = build_uri('goods', array('gid' => $value['goods_id'], 'gname' => $value['goods_name'], 'gperma' => $value['gperma']), $value['goods_name']);
        $arr[$key]['max_integral']          = integral_of_value($value['max_integral_amount']);
        $arr[$key]['width']  = (empty(floatval($value['goods_width'])))  ? $value['default_width']  : $value['goods_width'];
        $arr[$key]['height'] = (empty(floatval($value['goods_height']))) ? $value['default_height'] : $value['goods_height'];
        $arr[$key]['length'] = (empty(floatval($value['goods_length']))) ? $value['default_length'] : $value['goods_length'];
        $arr[$key]['total_width']  = $arr[$key]['width'] * $value['goods_number'];
        $arr[$key]['total_height'] = $arr[$key]['height'] * $value['goods_number'];
        $arr[$key]['total_length'] = $arr[$key]['length'] * $value['goods_number'];
        if ($value['extension_code'] == 'package')
        {
            if (!in_array($arr[$key]['is_package'], array_keys($packages))) {
                $packages[$arr[$key]['is_package']] = $packageController->get_basic_package_info($arr[$key]['is_package']);
                $packages[$arr[$key]['is_package']]['qty'] = $arr[$key]['goods_number'] / $packages[$arr[$key]['is_package']]['items'][$arr[$key]['goods_id']]['goods_number'];
                $packages[$arr[$key]['is_package']]['total_price'] = bcmul($packages[$arr[$key]['is_package']]['qty'], $packages[$arr[$key]['is_package']]['base_price'], 2);
                $packages[$arr[$key]['is_package']]['formated_total_price'] = price_format($packages[$arr[$key]['is_package']]['total_price'], false);
                $packages[$arr[$key]['is_package']]['rec_id'] = $arr[$key]['rec_id'];
            }
            $arr[$key]['package_goods_list'] = $packages[$arr[$key]['is_package']]['items'];
        }

        if ($value['is_insurance'] == 1 && $value['insurance_of'] > 0){
            $sql = "SELECT goods_name ".
                    "FROM ".$GLOBALS['ecs']->table('cart')." ".
                    "WHERE session_id = '" . SESS_ID . "' ".
                    "AND goods_id = '".$value['insurance_of']."' ".
                    "AND extension_code != 'package' ".
                    "AND is_package = 0 ".
                    "AND is_gift = 0 ";
            $result = $GLOBALS['db']->getRow($sql);
            if (!empty($result)){
                $arr[$key]['insurance_of_name'] = $result['goods_name'];
            }
        }

        if ($value['goods_number'] > $GLOBALS['db']->getOne("SELECT goods_number - reserved_number FROM (SELECT " . hw_goods_number_subquery('g.') . ", " . hw_reserved_number_subquery('g.') . " FROM " . $GLOBALS['ecs']->table('goods') . " g WHERE g.goods_id = $value[goods_id]) as tmp") && $value['is_insurance'] == 0 && $value['insurance_of'] == 0)
        {
            $arr[$key]['need_pre_sale'] = 1;
            if ($value['extension_code'] == 'package') {
                $packages[$arr[$key]['is_package']]['need_pre_sale'] = 1;
            }
        }
    }

    if ($all_detail) {
        return ["goods" => $arr, "packages" => $packages];
    }
    return $arr;
}

/**
 * 取得购物车总金额
 * @params  boolean $include_gift   是否包括赠品
 * @param   int     $type           类型：默认普通商品
 * @return  float   购物车总金额
 */
function cart_amount($include_gift = true, $type = CART_GENERAL_GOODS)
{
    $sql = "SELECT SUM(goods_price * goods_number) " .
            " FROM " . $GLOBALS['ecs']->table('cart') .
            " WHERE session_id = '" . SESS_ID . "' " .
            "AND rec_type = '$type' ";

    if (!$include_gift)
    {
        $sql .= ' AND is_gift = 0 AND goods_id > 0';
    }

    return floatval($GLOBALS['db']->getOne($sql));
}


/**购物车商品总数 sbsn ly**/
function cart_ly_number($include_gift = true, $type = CART_GENERAL_GOODS)
{
    $sql = "SELECT SUM(goods_number) " .
            " FROM " . $GLOBALS['ecs']->table('cart') .
            " WHERE session_id = '" . SESS_ID . "' " .
            "AND rec_type = '$type' ";

    if (!$include_gift)
    {
        $sql .= ' AND is_gift = 0 AND goods_id > 0';
    }

    return floatval($GLOBALS['db']->getOne($sql));
}
/**
 * 检查某商品是否已经存在于购物车
 *
 * @access  public
 * @param   integer     $id
 * @param   array       $spec
 * @param   int         $type   类型：默认普通商品
 * @return  boolean
 */
function cart_goods_exists($id, $spec, $type = CART_GENERAL_GOODS)
{
    /* 检查该商品是否已经存在在购物车中 */
    $sql = "SELECT COUNT(*) FROM " .$GLOBALS['ecs']->table('cart').
            "WHERE session_id = '" .SESS_ID. "' AND goods_id = '$id' ".
            "AND parent_id = 0 AND goods_attr = '" .get_goods_attr_info($spec). "' " .
            "AND is_gift = 0 AND is_package = 0 " .
            "AND rec_type = '$type'";

    return ($GLOBALS['db']->getOne($sql) > 0);
}

/**
 * 获得购物车中商品的总重量、总价格、总数量
 *
 * @access  public
 * @param   int     $type   类型：默认普通商品
 * @return  array
 */
function cart_weight_price($type = CART_GENERAL_GOODS, $cal_free_shipping_goods = false)
{
    // Handle goods marked as free_shipping by coupon
    global $coupon_free_shipping_goods;
    if (!$coupon_free_shipping_goods)
    {
        $coupon_free_shipping_goods = array();
    }
    
    $package_row['weight'] = 0;
    $package_row['amount'] = 0;
    $package_row['number'] = 0;
    $package_row['price'] = 0;

    $packages_row['free_shipping'] = 1;

    /* 计算超值礼包内商品的相关配送参数 */
    $sql = 'SELECT goods_id, goods_number, goods_price FROM ' . $GLOBALS['ecs']->table('cart') . " WHERE extension_code = 'package' AND session_id = '" . SESS_ID . "'";
    $row = $GLOBALS['db']->getAll($sql);

    if ($row)
    {
        $packages_row['free_shipping'] = 0;
        $free_shipping_count = 0;

        foreach ($row as $val)
        {
            // 如果商品全为免运费商品，设置一个标识变量
            $sql = 'SELECT count(*) FROM ' .
                    $GLOBALS['ecs']->table('package_goods') . ' AS pg, ' .
                    $GLOBALS['ecs']->table('goods') . ' AS g ' .
                    "WHERE g.goods_id = pg.goods_id AND g.is_shipping = 0 AND pg.package_id = '"  . $val['goods_id'] . "' " .
                    "AND g.goods_id NOT " . db_create_in($coupon_free_shipping_goods);
            $shipping_count = $GLOBALS['db']->getOne($sql);
            
            if ($shipping_count > 0)
            {
                // 循环计算每个超值礼包商品的重量和数量，注意一个礼包中可能包换若干个同一商品
                $sql = "SELECT sum(g.`goods_weight` * pg.`goods_number`) as weight, " .
                            "sum(pg.`goods_number`) as number " .
                        "FROM " . $GLOBALS['ecs']->table('package_goods') . " as pg " .
                        "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON g.goods_id = pg.goods_id " .
                        "WHERE ".($cal_free_shipping_goods ? "g.is_shipping = 0" : " 1 " ) ." AND g.shipping_price = 0 AND pg.package_id = '"  . $val['goods_id'] . "' " .
                        "AND g.goods_id NOT " . db_create_in($coupon_free_shipping_goods);
                $goods_row = $GLOBALS['db']->getRow($sql);
                $package_row['weight'] += floatval($goods_row['weight']) * $val['goods_number'];
                $package_row['amount'] += floatval($val['goods_price']) * $val['goods_number'];
                $package_row['number'] += intval($goods_row['number']) * $val['goods_number'];
                
                // Add shipping price
                $sql = "SELECT sum(g.`shipping_price` * pg.`goods_number`) as price " .
                        "FROM " . $GLOBALS['ecs']->table('package_goods') . " as pg " .
                        "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON g.goods_id = pg.goods_id " .
                        "WHERE ".($cal_free_shipping_goods ? "g.is_shipping = 0" : " 1 " ) ." AND g.shipping_price > 0 AND pg.package_id = '"  . $val['goods_id'] . "' " .
                        "AND g.goods_id NOT " . db_create_in($coupon_free_shipping_goods);
                $shipping_price = $GLOBALS['db']->getOne($sql);
                $package_row['price'] += floatval($shipping_price) * $val['goods_number'];
            }
            else
            {
                $free_shipping_count++;
            }
        }

        $packages_row['free_shipping'] = $free_shipping_count == count($row) ? 1 : 0;
        $packages_row['free_shipping'] = ($cal_free_shipping_goods) ? $packages_row['free_shipping'] : 0;
    }

    /* 获得购物车中非超值礼包商品的总重量 */
    // Only include those without shipping price
    $sql = "SELECT   sum(g.`goods_weight` * c.`goods_number`) as weight, " .
                    "sum(c.`goods_price` * c.`goods_number`) as amount, " .
                    "sum(c.`goods_number`) as number " .
            "FROM " . $GLOBALS['ecs']->table('cart') . " as c " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON g.goods_id = c.goods_id " .
            "WHERE c.session_id = '" . SESS_ID . "' " .
            "AND rec_type = '$type' AND ".($cal_free_shipping_goods ? "g.is_shipping = 0" : " 1 " ) ." AND g.shipping_price = 0 " .
            "AND g.goods_id NOT " . db_create_in($coupon_free_shipping_goods);
    $row = $GLOBALS['db']->getRow($sql);
    $packages_row['weight'] += floatval($row['weight']);
    $packages_row['amount'] += floatval($row['amount']);
    $packages_row['number'] += intval($row['number']);
    
    // Add shipping price
    $sql = "SELECT sum(g.`shipping_price` * c.`goods_number`) as price " .
            "FROM " . $GLOBALS['ecs']->table('cart') . " as c " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON g.goods_id = c.goods_id " .
            "WHERE c.session_id = '" . SESS_ID . "' " .
            "AND rec_type = '$type' AND ".($cal_free_shipping_goods ? "g.is_shipping = 0" : " 1 " ) ." AND g.shipping_price > 0 " .
            "AND g.goods_id NOT " . db_create_in($coupon_free_shipping_goods);
    $shipping_price = $GLOBALS['db']->getOne($sql);
    $packages_row['price'] += floatval($shipping_price);

    // If we cal free shipping goods, include price( If not cal free shipping goods, price is also in amount )
    if($cal_free_shipping_goods) {
        // Update 2016-08-28: Also include the price for free shipping products into calculation
        $sql = "SELECT   sum(c.`goods_price` * c.`goods_number`) as amount " .
            "FROM " . $GLOBALS['ecs']->table('cart') . " as c " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON g.goods_id = c.goods_id " .
            "WHERE c.session_id = '" . SESS_ID . "' " .
            "AND rec_type = '$type' AND g.shipping_price = 0 " .
            "AND (g.is_shipping = 1 OR g.goods_id " . db_create_in($coupon_free_shipping_goods) . ") ";
        $row = $GLOBALS['db']->getRow($sql);
        $packages_row['amount'] += floatval($row['amount']);
    }

    /* 格式化重量 */
    $packages_row['formated_weight'] = formated_weight($packages_row['weight']);
    
    return $packages_row;
}

/**
 * 添加商品到购物车
 *
 * @access  public
 * @param   integer $goods_id   商品编号
 * @param   integer $num        商品数量
 * @param   array   $spec       规格值对应的id数组
 * @param   integer $parent     基本件
 * @return  boolean
 */
function addto_cart($goods_id, $num = 1, $spec = array(), $parent = 0, $insuranceOf = 0)
{
    $GLOBALS['err']->clean();
    $_parent_id = $parent;
    /* 取得商品信息 */
    $sql = "SELECT *, " . hw_goods_in_stock_sql() . ", " . hw_goods_number_sql() . " FROM (".
            "SELECT g.goods_id, g.goods_name, g.goods_sn, g.is_on_sale, g.is_real, ".
                "g.market_price, g.shop_price AS org_price, g.promote_price, g.promote_start_date, ".
                "g.promote_end_date, g.goods_weight, g.integral, g.extension_code, ".
                "g.is_alone_sale, g.is_shipping, g.is_pre_sale, g.pre_sale_date, g.pre_sale_days, ".
                hw_goods_number_subquery('g.') . ", " .
    			hw_reserved_number_subquery('g.') . ", " .
                "IFNULL(mp.user_price, g.shop_price * '$_SESSION[discount]') AS shop_price ".
            " FROM " .$GLOBALS['ecs']->table('goods'). " AS g ".
            " LEFT JOIN " . $GLOBALS['ecs']->table('member_price') . " AS mp ".
                    "ON mp.goods_id = g.goods_id AND mp.user_rank = '$_SESSION[user_rank]' ".
            " WHERE g.goods_id = '$goods_id'".
            " AND g.is_delete = 0".
            ") as tmp";
    $goods = $GLOBALS['db']->getRow($sql);

    if (empty($goods))
    {
        $GLOBALS['err']->add(_L('global_cart_goods_not_exists', '您選購的產品不存在'), ERR_NOT_EXISTS);

        return false;
    }
    
    $goods = localize_db_row('goods', $goods);
    
    /* 如果是作为配件添加到购物车的，需要先检查购物车里面是否已经有基本件 */
    if ($parent > 0)
    {
        $sql = "SELECT COUNT(*) FROM " . $GLOBALS['ecs']->table('cart') .
                " WHERE goods_id='$parent' AND session_id='" . SESS_ID . "' AND extension_code <> 'package'";
        if ($GLOBALS['db']->getOne($sql) == 0)
        {
            $GLOBALS['err']->add(_L('global_cart_goods_missing_parent', '您必須先選購產品後才能購買該產品的配件'), ERR_NO_BASIC_GOODS);

            return false;
        }
    }
    
    if ($insuranceOf > 0)
    {
        $sql = "SELECT 1 " . 
                "FROM " . $GLOBALS['ecs']->table('cart') . " AS c " . 
                "LEFT JOIN " . $GLOBALS['ecs']->table('insurance') . " AS i ON i.target_goods_id = c.goods_id " . 
                "WHERE i.insurance_id ='" . $GLOBALS['db']->getOne("SELECT insurance_id FROM ".$GLOBALS['ecs']->table('insurance')." WHERE target_goods_id = '".$goods_id."'") . "' " .
                "AND c.insurance_of ='" . $insuranceOf . "' ".
                "AND c.goods_id !='" . $goods_id . "' ".
                "AND c.session_id = '" . SESS_ID . "' ";

        if ($GLOBALS['db']->getOne($sql)){
            $GLOBALS['err']->add(_L('global_cart_insurance_type_exists', '購物車已有相同類型的保險服務'), ERR_INS_EXISTS);
            return false;
        } else {
            $sql = "SELECT COUNT(*) " . 
            "FROM " . $GLOBALS['ecs']->table('insurance') . " AS i " . 
            "LEFT JOIN " . $GLOBALS['ecs']->table('insurance_goods') . " AS ig ON (ig.insurance_id = i.insurance_id AND ig.duration = i.duration) " . 
            "WHERE i.target_goods_id ='" . $goods_id . "' AND ig.goods_id='" . $insuranceOf . "' ";

            if ($GLOBALS['db']->getOne($sql) > 0){

                $sql = "SELECT COUNT(*) FROM " . $GLOBALS['ecs']->table('cart') .
                        " WHERE goods_id='$insuranceOf' AND session_id='" . SESS_ID . "' ";
                if ($GLOBALS['db']->getOne($sql) == 0)
                {
                    $GLOBALS['err']->add(_L('global_cart_insurance_missing_good', '您必須先選購產品後才能購買該產品的保險服務'), ERR_NO_BASIC_GOODS);
                    return false;
                }
            } else {
                $GLOBALS['err']->add(_L('global_cart_insurance_not_exists', '您選購的保險服務不存在'), ERR_NOT_EXISTS);
                return false;
            }
        }
    }

    /* 是否正在销售 */
    if ($goods['is_on_sale'] == 0)
    {
        $GLOBALS['err']->add(_L('global_cart_goods_not_on_sale', '您選購的產品已下架'), ERR_NOT_ON_SALE);

        return false;
    }

    /* 不是配件/保險时检查是否允许单独销售 */
    if (empty($parent) && empty($insuranceOf) && $goods['is_alone_sale'] == 0)
    {
        $GLOBALS['err']->add(_L('global_cart_goods_not_alone', '您選購的產品不能單獨購買'), ERR_CANNT_ALONE_SALE);

        return false;
    }

    /* 如果商品有规格则取规格商品信息 配件除外 */
    $sql = "SELECT * FROM " .$GLOBALS['ecs']->table('products'). " WHERE goods_id = '$goods_id' LIMIT 0, 1";
    $prod = $GLOBALS['db']->getRow($sql);

    if (is_spec($spec) && !empty($prod))
    {
        $product_info = get_products_info($goods_id, $spec);
    }
    if (empty($product_info))
    {
        $product_info = array('product_number' => '', 'product_id' => 0);
    }

    /* 计算商品的促销价格 */
    $spec_price             = spec_price($spec);
    $final_price     = get_final_price($goods_id, $num, true, $spec);
    $goods_price     = $final_price['final_price'];
    $goods_user_rank =  $final_price['user_rank_id'];
    $goods['market_price'] += $spec_price;
    $goods_attr             = get_goods_attr_info($spec);
    $goods_attr_id          = join(',', $spec);
    // YOHO: limit quantity to 20 a time
    if($final_price['can_buy']) $goods['goods_number'] = min(intval($goods['goods_number']), 20, $final_price['can_buy']);
    else  $goods['goods_number'] = min(intval($goods['goods_number']), 20);

    if ($insuranceOf == 0){
        /* 检查：库存 */
        if ($GLOBALS['_CFG']['use_storage'] == 1)
        {
            //检查：商品购买数量是否大于总库存
            if ($num > $goods['goods_number'])
            {
                addto_cart_error_out_of_stock($num, $goods['goods_number']);

                return false;
            }

            //商品存在规格 是货品 检查该货品库存
            if (is_spec($spec) && !empty($prod))
            {
                if (!empty($spec))
                {
                    /* 取规格的货品库存 */
                    if ($num > $product_info['product_number'])
                    {
                        addto_cart_error_out_of_stock($num, $product_info['product_number']);

                        return false;
                    }
                }
            }
        }
    }

    /* 初始化要插入购物车的基本件数据 */
    $parent = array(
        'user_id'       => $_SESSION['user_id'],
        'session_id'    => SESS_ID,
        'goods_id'      => $goods_id,
        'goods_sn'      => addslashes($goods['goods_sn']),
        'product_id'    => $product_info['product_id'],
        'goods_name'    => addslashes($goods['goods_name']),
        'market_price'  => $goods['market_price'],
        'goods_attr'    => addslashes($goods_attr),
        'goods_attr_id' => $goods_attr_id,
        'is_real'       => $goods['is_real'],
        'extension_code'=> $goods['extension_code'],
        'is_gift'       => 0,
        'is_package'    => 0,
        'is_shipping'   => $goods['is_shipping'],
        'rec_type'      => CART_GENERAL_GOODS,
        'user_rank'     => $goods_user_rank
    );

    /* 如果该配件在添加为基本件的配件时，所设置的“配件价格”比原价低，即此配件在价格上提供了优惠， */
    /* 则按照该配件的优惠价格卖，但是每一个基本件只能购买一个优惠价格的“该配件”，多买的“该配件”不享 */
    /* 受此优惠 */
    $basic_list = array();
    $sql = "SELECT parent_id, goods_price " .
            "FROM " . $GLOBALS['ecs']->table('group_goods') .
            " WHERE goods_id = '$goods_id'" .
            " AND goods_price < '$goods_price'" .
            " AND parent_id = '$_parent_id'" .
            " ORDER BY goods_price";
    $res = $GLOBALS['db']->query($sql);
    while ($row = $GLOBALS['db']->fetchRow($res))
    {
        $basic_list[$row['parent_id']] = $row['goods_price'];
    }

    /* 取得购物车中该商品每个基本件的数量 */
    $basic_count_list = array();
    if ($basic_list)
    {
        $sql = "SELECT goods_id, SUM(goods_number) AS count " .
                "FROM " . $GLOBALS['ecs']->table('cart') .
                " WHERE session_id = '" . SESS_ID . "'" .
                " AND parent_id = 0" .
                " AND extension_code <> 'package' " .
                " AND goods_id " . db_create_in(array_keys($basic_list)) .
                " GROUP BY goods_id";
        $res = $GLOBALS['db']->query($sql);
        while ($row = $GLOBALS['db']->fetchRow($res))
        {
            $basic_count_list[$row['goods_id']] = $row['count'];
        }
    }

    /* 取得购物车中该商品每个基本件已有该商品配件数量，计算出每个基本件还能有几个该商品配件 */
    /* 一个基本件对应一个该商品配件 */
    if ($basic_count_list)
    {
        $sql = "SELECT parent_id, SUM(goods_number) AS count " .
                "FROM " . $GLOBALS['ecs']->table('cart') .
                " WHERE session_id = '" . SESS_ID . "'" .
                " AND goods_id = '$goods_id'" .
                " AND extension_code <> 'package' " .
                " AND parent_id " . db_create_in(array_keys($basic_count_list)) .
                " GROUP BY parent_id";
        $res = $GLOBALS['db']->query($sql);
        while ($row = $GLOBALS['db']->fetchRow($res))
        {
            $basic_count_list[$row['parent_id']] -= $row['count'];
        }
    }

    /* 循环插入配件 如果是配件则用其添加数量依次为购物车中所有属于其的基本件添加足够数量的该配件 */
    foreach ($basic_list as $parent_id => $fitting_price)
    {
        /* 如果已全部插入，退出 */
        if ($num <= 0)
        {
            break;
        }

        /* 如果该基本件不再购物车中，执行下一个 */
        if (!isset($basic_count_list[$parent_id]))
        {
            continue;
        }

        /* 如果该基本件的配件数量已满，执行下一个基本件 */
        if ($basic_count_list[$parent_id] <= 0)
        {
            continue;
        }

        /* 作为该基本件的配件插入 */
        $parent['goods_price']  = max($fitting_price, 0) + $spec_price; //允许该配件优惠价格为0
        $parent['goods_number'] = min($num, $basic_count_list[$parent_id]);
        $parent['parent_id']    = $parent_id;

        /* 添加 */
        $GLOBALS['db']->autoExecute($GLOBALS['ecs']->table('cart'), $parent, 'INSERT');

        /* 改变数量 */
        $num -= $parent['goods_number'];
    }

    if ($insuranceOf > 0){
        $ins_price = get_insurance_price($goods_id, $insuranceOf);
        if ($ins_price){
            $parent['goods_price']  = $ins_price;
            $parent['market_price']  = $ins_price;
            $parent['insurance_of'] = $insuranceOf;
        } else {
            $GLOBALS['err']->add(_L('global_cart_insurance_not_exists', '您選購的保險服務不存在'), ERR_NOT_EXISTS);
            return false;
        }
    }

    /* 如果数量不为0，作为基本件插入 */
    if ($num > 0)
    {
        /* 检查该商品是否已经存在在购物车中 */
        if ($insuranceOf == 0){
        $sql = "SELECT goods_number FROM " .$GLOBALS['ecs']->table('cart').
                " WHERE session_id = '" .SESS_ID. "' AND goods_id = '$goods_id' ".
                " AND parent_id = 0 AND goods_attr = '" .get_goods_attr_info($spec). "' " .
                " AND extension_code <> 'package' " .
                " AND rec_type = 'CART_GENERAL_GOODS' AND is_gift = 0";
        } else {
            $sql = "SELECT goods_number FROM " .$GLOBALS['ecs']->table('cart').
            " WHERE session_id = '" .SESS_ID. "' AND goods_id = '$goods_id' ".
            " AND parent_id = 0 " .
            " AND extension_code <> 'package' " .
            " AND is_gift = 0 " .
            " AND insurance_of = '" . $insuranceOf . "' ";
        }

        $row = $GLOBALS['db']->getRow($sql);
        
        if($row) //如果购物车已经有此物品，则更新
        {
            $num += $row['goods_number'];
            if(is_spec($spec) && !empty($prod) )
            {
                $goods_storage=$product_info['product_number'];
            }
            else
            {
                $goods_storage=$goods['goods_number'];
            }
            if ($insuranceOf > 0)
            {
                $ins_price = get_insurance_price($goods_id, $insuranceOf);
                if ($ins_price){
                    $sql = "UPDATE " . $GLOBALS['ecs']->table('cart') . " " .
                            "SET goods_number = '$num', goods_price = '$ins_price' " .
                            "WHERE session_id = '" .SESS_ID. "' " .
                            "AND goods_id = '$goods_id' " .
                            "AND insurance_of = '$insuranceOf' " .
                            "AND parent_id = 0 " .
                            "AND is_insurance = 1 ";
                    $GLOBALS['db']->query($sql);

                } else {
                    $GLOBALS['err']->add(_L('global_cart_insurance_not_exists', '您選購的保險服務不存在'), ERR_NOT_EXISTS);
                    return false;
                }

            } 
            elseif ($GLOBALS['_CFG']['use_storage'] == 0 || $num <= $goods_storage)
            {
                $final_price     = get_final_price($goods_id, $num, true, $spec);
                $goods_price     = $final_price['final_price'];
                $goods_user_rank =  $final_price['user_rank_id'];
                $sql = "UPDATE " . $GLOBALS['ecs']->table('cart') . " SET goods_number = '$num'" .
                       " , goods_price = '$goods_price', user_rank = '$goods_user_rank' ".
                       " WHERE session_id = '" .SESS_ID. "' AND goods_id = '$goods_id' ".
                       " AND parent_id = 0 AND goods_attr = '" .get_goods_attr_info($spec). "' " .
                       " AND extension_code <> 'package' " .
                       "AND rec_type = 'CART_GENERAL_GOODS'";
                $GLOBALS['db']->query($sql);
            }
            else
            {
                addto_cart_error_out_of_stock($num, $goods_storage);

                return false;
            }
        }
        else //购物车没有此物品，则插入
        {
            if ($insuranceOf > 0){
                $ins_price = get_insurance_price($goods_id, $insuranceOf);
                if ($ins_price){
                    $parent['insurance_of'] = $insuranceOf;
                    $parent['is_insurance'] = 1;
                    $goods_price = $ins_price;
                    $goods_user_rank = 0;
                } else {
                    $GLOBALS['err']->add(_L('global_cart_insurance_not_exists', '您選購的保險服務不存在'), ERR_NOT_EXISTS);
                    return false;
                }
            } else {
                $final_price = get_final_price($goods_id, $num, true, $spec);
                $goods_price     = $final_price['final_price'];
                $goods_user_rank =  $final_price['user_rank_id'];
            }
            //echo($goods_id);echo "|";echo $num;echo "|";print_r($spec);
            $parent['goods_price']  = max($goods_price, 0);
            $parent['goods_number'] = $num;
            $parent['parent_id']    = 0;
            $parent['user_rank']    = $goods_user_rank;
            $GLOBALS['db']->autoExecute($GLOBALS['ecs']->table('cart'), $parent, 'INSERT');
        }
    }

    /* 把赠品删除 */
    //$sql = "DELETE FROM " . $GLOBALS['ecs']->table('cart') . " WHERE session_id = '" . SESS_ID . "' AND is_gift <> 0";
    //$GLOBALS['db']->query($sql);

    return true;
}

/**
 * 添加商品及贈品到购物车
 *
 * @access  public
 * @param   integer $goods_id   商品编号
 * @param   integer $num        商品数量
 * @param   array   $spec       规格值对应的id数组
 * @param   integer $parent     基本件
 * @return  boolean
 */
function favourable_addto_cart($goods_id, $num = 1, $spec = array(), $parent = 0, $act_id = 0, $gift_id = 0)
{
    $orderController = new Yoho\cms\Controller\OrderController();

    $user_rank = $_SESSION['user_rank'];
    $user_id = $_SESSION['user_id'];
    $platform = $orderController::ORDER_FEE_PLATFORM_WEB;

    
    $favourable = favourable_info($act_id);
    $fav_goods_list = array();

    if(!$orderController->favourable_available($favourable, $user_rank, $user_id, $platform, $goods_id)){
        return false;
    }

    if($favourable["act_kind"]==1 && $favourable["act_type_ext"] == 0){
        $fav_goods_list = array_column($favourable['gift'], 'id');
        $gift_qun = 1;
        $ok = addto_cart($goods_id, $num, $spec, $parent);
    }
    elseif(($favourable["act_kind"]==1 && $favourable["act_type_ext"] > 0) || $favourable["act_kind"]==2){
        $fav_goods_list = array($gift_id);
        $gift_qun = 1;
        if(!cart_goods_exists($goods_id)){
            $ok = addto_cart($goods_id, $num, $spec, $parent);
        }
        else{
            $ok = true;
        }
    }
        
    if($ok){
        $ajax = true;
        
        $cart_favourable_quantity = $orderController->cart_favourable_quantity($favourable, $platform);
        $can_enjoy_favourable_count = floor($cart_favourable_quantity / $favourable['min_quantity']);
        $added = $orderController->add_favourable_goods(0, $can_enjoy_favourable_count, $favourable['act_id'], $user_rank, $user_id, $platform, $fav_goods_list, $ajax);
    }
    

    return ($ok && $added);
}


function addto_cart_error_out_of_stock($num, $stock)
{
    if ($stock > 0)
    {
        $msg = sprintf(_L('global_cart_quantity_exceeded', '您購買了%d個，超過購買上限<b>%d</b>個，加入購物車失敗。'), $num, $stock);
    }
    else
    {
        $msg = _L('global_cart_out_of_stock', '您選購的產品已經售罄。');
    }
    $GLOBALS['err']->add($msg, ERR_OUT_OF_STOCK);
}

/**
 * 清空购物车
 * @param   int     $type   类型：默认普通商品
 */
function clear_cart($type = CART_GENERAL_GOODS)
{
    $sql = "DELETE FROM " . $GLOBALS['ecs']->table('cart') .
            " WHERE session_id = '" . SESS_ID . "' AND rec_type = '$type'";
    $flashdealController = new \Yoho\cms\Controller\FlashdealController();
    $flashdealController->updateFlashdealCartByECSCart();
    $GLOBALS['db']->query($sql);
}

/**
 * 获得指定的商品属性
 *
 * @access      public
 * @param       array       $arr        规格、属性ID数组
 * @param       type        $type       设置返回结果类型：pice，显示价格，默认；no，不显示价格
 *
 * @return      string
 */
function get_goods_attr_info($arr, $type = 'pice')
{
    $attr   = '';

    if (!empty($arr))
    {
        $fmt = "%s:%s[%s] \n";

        $sql = "SELECT a.attr_name, ga.attr_value, ga.attr_price ".
                "FROM ".$GLOBALS['ecs']->table('goods_attr')." AS ga, ".
                    $GLOBALS['ecs']->table('attribute')." AS a ".
                "WHERE " .db_create_in($arr, 'ga.goods_attr_id')." AND a.attr_id = ga.attr_id";
        $res = $GLOBALS['db']->query($sql);

        while ($row = $GLOBALS['db']->fetchRow($res))
        {
            $attr_price = round(floatval($row['attr_price']), 2);
            $attr .= sprintf($fmt, $row['attr_name'], $row['attr_value'], $attr_price);
        }

        $attr = str_replace('[0]', '', $attr);
    }

    return $attr;
}

/**
 * 取得用户信息
 * @param   int     $user_id    用户id
 * @return  array   用户信息
 */
function user_info($user_id)
{
    $sql = "SELECT u.*, IFNULL(ur.rank_id, 0) as user_rank, ur.rank_name as rank_name  FROM " . $GLOBALS['ecs']->table('users') ." AS u ".
            " LEFT JOIN ".$GLOBALS['ecs']->table('user_rank')." AS ur ON ur.special_rank = '0' AND ur.min_points <= u.rank_points AND ur.max_points > u.rank_points ".
            " WHERE u.user_id = '$user_id'";
    $user = $GLOBALS['db']->getRow($sql);

    unset($user['question']);
    unset($user['answer']);

    /* 格式化帐户余额 */
    if ($user)
    {
//        if ($user['user_money'] < 0)
//        {
//            $user['user_money'] = 0;
//        }
        $user['formated_user_money'] = price_format($user['user_money'], false);
        $user['formated_frozen_money'] = price_format($user['frozen_money'], false);
    }

    return $user;
}

/**
 * 修改用户
 * @param   int     $user_id   订单id
 * @param   array   $user      key => value
 * @return  bool
 */
function update_user($user_id, $user)
{
    return $GLOBALS['db']->autoExecute($GLOBALS['ecs']->table('users'),
        $user, 'UPDATE', "user_id = '$user_id'");
}

/**
 * 取得用户地址列表
 * @param   int     $user_id    用户id
 * @return  array
 */
function address_list($user_id)
{
    $sql = "SELECT * FROM " . $GLOBALS['ecs']->table('user_address') .
            " WHERE user_id = '$user_id'";

    return $GLOBALS['db']->getAll($sql);
}

/**
 * 取得用户地址信息
 * @param   int     $address_id     地址id
 * @return  array
 */
function address_info($address_id)
{
    $sql = "SELECT * FROM " . $GLOBALS['ecs']->table('user_address') .
            " WHERE address_id = '$address_id'";

    return $GLOBALS['db']->getRow($sql);
}

/**
 * 取得用户当前可用红包
 * @param   int     $user_id        用户id
 * @param   float   $goods_amount   订单商品金额
 * @return  array   红包数组
 */
function user_bonus($user_id, $goods_amount = 0)
{
    $day    = getdate();
    $today  = local_mktime(23, 59, 59, $day['mon'], $day['mday'], $day['year']);

    $sql = "SELECT t.type_id, t.type_name, t.type_money, b.bonus_id " .
            "FROM " . $GLOBALS['ecs']->table('bonus_type') . " AS t," .
                $GLOBALS['ecs']->table('user_bonus') . " AS b " .
            "WHERE t.type_id = b.bonus_type_id " .
            "AND t.use_start_date <= '$today' " .
            "AND t.use_end_date >= '$today' " .
            "AND t.min_goods_amount <= '$goods_amount' " .
            "AND b.user_id<>0 " .
            "AND b.user_id = '$user_id' " .
            "AND b.order_id = 0";
    return $GLOBALS['db']->getAll($sql);
}

/**
 * 取得红包信息
 * @param   int     $bonus_id   红包id
 * @param   string  $bonus_sn   红包序列号
 * @param   array   红包信息
 */
function bonus_info($bonus_id, $bonus_sn = '')
{
    $sql = "SELECT t.*, b.* " .
            "FROM " . $GLOBALS['ecs']->table('bonus_type') . " AS t," .
                $GLOBALS['ecs']->table('user_bonus') . " AS b " .
            "WHERE t.type_id = b.bonus_type_id ";
    if ($bonus_id > 0)
    {
        $sql .= "AND b.bonus_id = '$bonus_id'";
    }
    else
    {
        $sql .= "AND b.bonus_sn = '$bonus_sn'";
    }

    return $GLOBALS['db']->getRow($sql);
}

/**
 * 检查红包是否已使用
 * @param   int $bonus_id   红包id
 * @return  bool
 */
function bonus_used($bonus_id)
{
    $sql = "SELECT order_id FROM " . $GLOBALS['ecs']->table('user_bonus') .
            " WHERE bonus_id = '$bonus_id'";

    return  $GLOBALS['db']->getOne($sql) > 0;
}

/**
 * 设置红包为已使用
 * @param   int     $bonus_id   红包id
 * @param   int     $order_id   订单id
 * @return  bool
 */
function use_bonus($bonus_id, $order_id)
{
    $sql = "UPDATE " . $GLOBALS['ecs']->table('user_bonus') .
            " SET order_id = '$order_id', used_time = '" . gmtime() . "' " .
            "WHERE bonus_id = '$bonus_id' LIMIT 1";

    return  $GLOBALS['db']->query($sql);
}

/**
 * 设置红包为未使用
 * @param   int     $bonus_id   红包id
 * @param   int     $order_id   订单id
 * @return  bool
 */
function unuse_bonus($bonus_id)
{
    $sql = "UPDATE " . $GLOBALS['ecs']->table('user_bonus') .
            " SET order_id = 0, used_time = 0 " .
            "WHERE bonus_id = '$bonus_id' LIMIT 1";

    return  $GLOBALS['db']->query($sql);
}

// Get Coupon Info
function coupon_info($coupon_id, $coupon_code = '')
{
    $sql = "SELECT * " .
           "FROM " . $GLOBALS['ecs']->table('coupons') . " " .
           "WHERE 1 ";
    if ($coupon_id > 0)
    {
        $sql .= "AND `coupon_id` = '$coupon_id'";
    }
    else
    {
        $sql .= "AND `coupon_code` = '$coupon_code'";
    }

    $coupon = $GLOBALS['db']->getRow($sql);
    
    if (!empty($coupon['goods_list']))
    {
        $coupon['goods_list'] = json_decode($coupon['goods_list'], true);
    }
    if (!empty($coupon['users_list']))
    {
        $coupon['users_list'] = json_decode($coupon['users_list'], true);
    }
    if (!empty($coupon['cat_list']))
    {
        $cat_list = explode(",", $coupon['cat_list']);
        $total_cat_list = [];
        foreach ($cat_list as $cat_id) {
            $total_cat_list[] = intval($cat_id);
            $total_cat_list = array_unique(array_merge($total_cat_list, array_keys(cat_list($cat_id, 0, false))));
        }
        $coupon['cat_list'] = $total_cat_list;
    }
    if (!empty($coupon['exclude_cat_list']))
    {
        $exclude_cat_list = explode(",", $coupon['exclude_cat_list']);
        $total_cat_list = [];
        foreach ($exclude_cat_list as $cat_id) {
            $total_cat_list[] = intval($cat_id);
            $total_cat_list = array_unique(array_merge($total_cat_list, array_keys(cat_list($cat_id, 0, false))));
        }
        $coupon['exclude_cat_list'] = $total_cat_list;
    }
    if (!empty($coupon['brand_list']))
    {
        $coupon['brand_list'] = explode(",", $coupon['brand_list']);
    }
    if (!empty($coupon['exclude_brand_list']))
    {
        $coupon['exclude_brand_list'] = explode(",", $coupon['exclude_brand_list']);
    }
    
    return $coupon;
}

// Coupon verification error codes
define('COUPON_ERROR_EMPTY_COUPON', 1);
define('COUPON_ERROR_MIN_GOODS_AMOUNT', 2);
define('COUPON_ERROR_BEFORE_START', 3);
define('COUPON_ERROR_AFTER_END', 4);
define('COUPON_ERROR_SPECIFIC_GOODS', 5);
define('COUPON_ERROR_SPECIFIC_USER', 6);
define('COUPON_ERROR_NOT_REUSABLE', 7);
define('COUPON_ERROR_EXCLUSIVE', 8);
define('COUPON_ERROR_EXISTS_EXCLUSIVE', 9);
define('COUPON_ERROR_REACHED_THE_LIMIT', 10);
define('COUPON_ERROR_SMS_VERIFED', 11);


// Verify the coupon can be used
function verify_coupon($coupon, &$order, $cart_amount, $cart_goods, $user_id, &$error = null)
{
    global $db, $ecs;
    
    if (empty($coupon))
    {
        $error = COUPON_ERROR_EMPTY_COUPON;
        return false;
    }
    $coupon_goods = array_reduce($coupon['goods_list'], 'array_merge', array());
    if ($coupon['min_goods_amount'] > 0)
    {
        // If cartwide flag is off, only count amount of specific products
        if (!$coupon['cartwide'] &&
            (!empty($coupon['goods_list']) || !empty($coupon['brand_list']) || !empty($coupon['cat_list'])
                || !empty($coupon['exclude_cat_list']) ||  !empty($coupon['exclude_brand_list'])))
        {
            $goods_amount = 0;
            $exclude_goods_list = [];
            foreach ($cart_goods AS $g)
            {
                if (empty($g['brand_id']) || empty($g['cat_id'])) {
                    $g_data = $db->getRow("SELECT brand_id, cat_id FROM ".$ecs->table('goods')." WHERE goods_id = ".$g['goods_id']);
                    $g['brand_id'] = $g_data['brand_id'];
                    $g['cat_id']   = $g_data['cat_id'];
                }
                if(empty($coupon['brand_list']) && in_array($g['cat_id'], $coupon['cat_list'])) { /* Only have cat list */
                    $coupon_goods[] = $g['goods_id'];
                }else if(empty($coupon['cat_list']) && in_array($g['brand_id'], $coupon['brand_list'])) {/* Only have brand list */
                    $coupon_goods[] = $g['goods_id'];
                }else if(in_array($g['cat_id'], $coupon['cat_list']) && in_array($g['brand_id'], $coupon['brand_list'])) {/* have brand list and cat list: must match 2 list  */
                    $coupon_goods[] = $g['goods_id'];
                }
                if(in_array($g['cat_id'], $coupon['exclude_cat_list']) && ($key = array_search($g['goods_id'], $coupon_goods)) !== false) {
                    unset($coupon_goods[$key]);
                    $exclude_goods_list[] = $g['goods_id'];
                }
                if(in_array($g['brand_id'], $coupon['exclude_brand_list']) && ($key = array_search($g['goods_id'], $coupon_goods)) !== false) {
                    unset($coupon_goods[$key]);
                    $exclude_goods_list[] = $g['goods_id'];
                }
                if (in_array($g['goods_id'], $coupon_goods))
                {
                    $goods_amount += $g['goods_price'] * $g['goods_number'];
                }
            }
            if ($coupon['min_goods_amount'] > $goods_amount)
            {
                $error = COUPON_ERROR_MIN_GOODS_AMOUNT;
                return false;
            }
        }
        if ($coupon['min_goods_amount'] > $cart_amount)
        {
            $error = COUPON_ERROR_MIN_GOODS_AMOUNT;
            return false;
        }
    }
    
    if ($coupon['start_time'] > gmtime())
    {
        $error = COUPON_ERROR_BEFORE_START;
        return false;
    }
    if ($coupon['end_time'] < gmtime())
    {
        $error = COUPON_ERROR_AFTER_END;
        return false;
    }
    
    $cart_goods_list = array_map(function ($goods) { return $goods['goods_id']; }, $cart_goods);
    $goods_cat_list = array_map(function ($goods) { return $goods['cat_id']; }, $cart_goods);
    if(empty($goods_cat_list) || $goods_cat_list[0] == null) $goods_cat_list = $db->getCol('SELECT cat_id FROM '.$ecs->table('goods')." WHERE goods_id ".db_create_in($cart_goods_list)." GROUP BY cat_id ");
    $goods_brand_list = array_map(function ($goods) { return $goods['brand_id']; }, $cart_goods);
    if(empty($goods_brand_list) || $goods_brand_list[0] == null)  $goods_brand_list = $db->getCol('SELECT brand_id FROM '.$ecs->table('goods')." WHERE goods_id ".db_create_in($cart_goods_list)." GROUP BY brand_id ");
    
    if (!empty($coupon['exclude_flashdeal']))
    {
        foreach ($cart_goods as $g)
        {
            if ($g['flashdeal_id'] > 0)
            {
                $error = COUPON_ERROR_SPECIFIC_GOODS;
                return false;
            }
        }
    }

    if (!empty($coupon['sms_verified']))
    {
        $userController = new Yoho\cms\Controller\UserController();
        $is_verifed = $userController->is_user_sms_verified($user_id);

        if (!$is_verifed) {
            $error = COUPON_ERROR_SMS_VERIFED;
            return false;
        }
    }

    /* If cart goods not in goods_list, cat_list, brand_list: Can't use this coupon. */
    if (!empty($coupon_goods))
    {
        foreach ($coupon['goods_list'] as $goods_group)
        {
            if (array_intersect($goods_group, $coupon_goods) && !array_intersect($goods_group, $cart_goods_list, $coupon_goods))
            {
                $error = COUPON_ERROR_SPECIFIC_GOODS;
                return false;
            }
        }
    }
    if (!empty($coupon['cat_list']))
    {
        $have_cat = false;
        foreach ($coupon['cat_list'] as $cat_id)
        {
            if (in_array($cat_id, $goods_cat_list))
            {
                $have_cat = true;
                break;
            }
        }
        if(!$have_cat) {
            $error = COUPON_ERROR_SPECIFIC_GOODS;
            return false;
        }
    }
    if (!empty($coupon['brand_list']))
    {
        $have_brand = false;
        foreach ($coupon['brand_list'] as $brand_id)
        {
            if (in_array($brand_id, $goods_brand_list))
            {
                $have_brand = true;
                break;
            }
        }
        if(!$have_brand) {
            $error = COUPON_ERROR_SPECIFIC_GOODS;
            return false;
        }
    }
    /*
    if (!$coupon['cartwide']) {
        if (!empty($coupon['exclude_cat_list']))
        {
            $have_exclude_cat = false;
            foreach ($coupon['exclude_cat_list'] as $cat_id)
            {
                if (in_array($cat_id, $goods_cat_list))
                {
                    $have_exclude_cat = true;
                    break;
                }
            }
            if($have_exclude_cat) {
                $error = COUPON_ERROR_SPECIFIC_GOODS;
                return false;
            }
        }
        if (!empty($coupon['exclude_brand_list']))
        {
            $have_exclude_brand = false;
            foreach ($coupon['exclude_brand_list'] as $brand_id)
            {
                if (in_array($brand_id, $goods_brand_list))
                {
                    $have_exclude_brand = true;
                    break;
                }
            }
            if($have_exclude_brand) {
                $error = COUPON_ERROR_SPECIFIC_GOODS;
                return false;
            }
        }
    }
    */
    if (!empty($coupon['users_list']))
    {
        if (!in_array($user_id, $coupon['users_list']))
        {
            $error = COUPON_ERROR_SPECIFIC_USER;
            return false;
        }
    }

    if (!empty($coupon['users_list'])) 
    {
        if (!empty($coupon['times_limit']) && $coupon['times_limit'] =="1")
        {
            // check how many time the user used the coupon
            $sql = "SELECT count(*) as user_used_time FROM " . $ecs->table('coupon_log') . " as cl ".
            "LEFT JOIN " . $ecs->table('order_info') . " as oi on oi.order_id = cl .order_id ".
            "WHERE  cl.user_id = '".$user_id."' and  cl.coupon_id = '" . $coupon['coupon_id'] . "' AND oi.order_status NOT IN(".OS_CANCELED.",".OS_INVALID.") ";
            $user_used_time = $db->getOne($sql);
            $users_list_count_ar = array_count_values($coupon['users_list']);
            if ($user_used_time >= $users_list_count_ar[$user_id]) 
            {
                $error = COUPON_ERROR_REACHED_THE_COUNT_LIMIT ;
                return false;
            }
        }
    } else {
        //限制優惠券使用人數
        if (!empty($coupon['user_limit']) && $coupon['user_limit'] =="1")
        {
            //人數限制
            $max_user_used = $coupon['max_user_used'];
            $number_sql = "SELECT COUNT(DISTINCT cl.user_id) FROM " . $ecs->table('coupon_log') . " AS cl " .
                    "LEFT JOIN " . $ecs->table('order_info') . " AS oi ON oi.order_id = cl.order_id ".
                    "WHERE cl.`coupon_id` = '" . $coupon['coupon_id'] . "' AND oi.order_status NOT IN(".OS_CANCELED.",".OS_INVALID.") ";
            //已使用人數
            $number_of_user_used = $db->getOne($number_sql);
            $user_sql = "SELECT DISTINCT cl.user_id FROM " . $ecs->table('coupon_log') . " AS cl " .
                    "LEFT JOIN " . $ecs->table('order_info') . " AS oi ON oi.order_id = cl.order_id ".
                    "WHERE cl.`coupon_id` = '" . $coupon['coupon_id'] . "' AND oi.order_status NOT IN(".OS_CANCELED.",".OS_INVALID.") ";
            //所有已使用優惠券的用戶
            $user_used = $db->getCol($user_sql);
            if ($number_of_user_used >= $max_user_used && !in_array($user_id, $user_used)) {
                $error = COUPON_ERROR_REACHED_THE_USER_LIMIT;
                return false;
            }
        }

        // 限制優惠券每人使用次數
        if (!empty($coupon['times_limit']) && $coupon['times_limit'] =="1")
        {
            //每人使用次數限制
            $max_times_used = $coupon['max_times_used'];
            // check how many time the user used the coupon
            $sql = "SELECT count(*) as user_used_time FROM " . $ecs->table('coupon_log') . " as cl ".
                "LEFT JOIN " . $ecs->table('order_info') . " as oi on oi.order_id = cl .order_id ".
                "WHERE  cl.user_id = '".$user_id."' and  cl.coupon_id = '" . $coupon['coupon_id'] . "' AND oi.order_status NOT IN(".OS_CANCELED.",".OS_INVALID.") ";
            $times_used = $db->getOne($sql);
            if ($times_used >= $max_times_used) 
            {
                $error = COUPON_ERROR_REACHED_THE_COUNT_LIMIT;
                return false;
            }
        }

    }
    if (!empty($order['exclusive_coupon']))
    {
        $error = COUPON_ERROR_EXISTS_EXCLUSIVE;
        return false;
    }
    
    if (!empty($coupon['exclusive']))
    {
        if (!empty($order['coupon_id']))
        {
            $error = COUPON_ERROR_EXCLUSIVE;
            return false;
        }
        
        // Store the exclusive coupon in $order
        $order['exclusive_coupon'] = $coupon;
    }
    
    return true;
}

// Coupon error code to message
function coupon_error_message($error)
{
    $messages = array(
        COUPON_ERROR_EMPTY_COUPON => '找不到該優惠券',
        COUPON_ERROR_MIN_GOODS_AMOUNT => '未達到指定的最低訂單金額',
        COUPON_ERROR_BEFORE_START => '優惠未開始',
        COUPON_ERROR_AFTER_END => '優惠券已過期',
        COUPON_ERROR_SPECIFIC_GOODS => '您並未選購指定的產品',
        COUPON_ERROR_SPECIFIC_USER => '優惠券只限指定會員使用',
        COUPON_ERROR_NOT_REUSABLE => '優惠券已被使用',
        COUPON_ERROR_REACHED_THE_USER_LIMIT => '優惠券已達到使用人數',
        COUPON_ERROR_REACHED_THE_COUNT_LIMIT => '優惠券已達到使用次數',
        COUPON_ERROR_EXCLUSIVE => '此優惠券不能與其他優惠券同時使用',
        COUPON_ERROR_EXISTS_EXCLUSIVE => '另一張優惠券不能與其他優惠券同時使用'
    );
    
    return isset($messages[$error]) ? $messages[$error] : '未知錯誤';
}

// Use a coupon
function use_coupon($coupon_id, $order_id, $user_id = null)
{
    if ($user_id === null)
    {
        $user_id = $_SESSION['user_id'];
    }
    
    $sql = "INSERT INTO " . $GLOBALS['ecs']->table('order_coupons') .
            "(`order_id`, `coupon_id`) " .
            "VALUES " .
            "('" . $order_id . "', '" . $coupon_id . "')";
    $GLOBALS['db']->query($sql);
    
    $sql = "SELECT `coupon_code` FROM " . $GLOBALS['ecs']->table('coupons') . " WHERE `coupon_id` = '" . $coupon_id . "'";
    $coupon_code = $GLOBALS['db']->getOne($sql);
    
    $sql = "INSERT INTO " . $GLOBALS['ecs']->table('coupon_log') .
            "(`coupon_id`, `coupon_code`, `user_id`, `use_time`, `order_id`) " .
            "VALUES " .
            "('" . $coupon_id . "', '" . $coupon_code . "', '" . $user_id . "', '" . gmtime() . "', '" . $order_id . "')";
    return  $GLOBALS['db']->query($sql);
}

// Get coupons associated with an order
function order_coupons($order_id)
{
    $sql = "SELECT c.* " .
            "FROM " . $GLOBALS['ecs']->table('order_coupons') . " as oc " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('coupons') . " as c ON oc.coupon_id = c.coupon_id " .
            "WHERE oc.order_id = '" . $order_id . "'";
    $coupons = $GLOBALS['db']->getAll($sql);
    
    foreach($coupons as $key => $coupon)
    {
        if (!empty($coupon['goods_list']))
        {
            $coupon['goods_list'] = json_decode($coupon['goods_list'], true);
        }
        if (!empty($coupon['users_list']))
        {
            $coupon['users_list'] = json_decode($coupon['users_list'], true);
        }
        $coupons[$key] = $coupon;
    }
    return $coupons;
}

// Get coupons can be used with an order
function available_coupons($order, $amount, $order_goods)
{
    $sql = "SELECT c.* FROM " . $GLOBALS['ecs']->table('coupons') . " as c " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('coupon_log') . " as cl ON c.coupon_id = cl.coupon_id " .
            "WHERE c.min_goods_amount < " . doubleval($amount) . " " .
            "AND (" . gmtime() . " BETWEEN c.start_time AND c.end_time) " .
            "AND (c.reusable = 1 OR cl.log_id IS NULL) " .
            "GROUP BY c.coupon_id";
    $coupons = $GLOBALS['db']->getAll($sql);
    
    $order_goods_list = array_map(function ($goods) { return $goods['goods_id']; }, $order_goods);
    
    $available_coupons = array();
    
    foreach ($coupons as $coupon)
    {
        if (!empty($coupon['goods_list']))
        {
            $coupon['goods_list'] = json_decode($coupon['goods_list'], true);
            $ok = true;
            foreach ($coupon['goods_list'] as $goods_group)
            {
                if (!array_intersect($goods_group, $order_goods_list))
                {
                    $ok = false;
                    break;
                }
            }
            if (!$ok) continue;
        }
        
        if (!empty($coupon['users_list']))
        {
            if ($order['user_id'] == 0)
            {
                continue;
            }
            else
            {
                $coupon['users_list'] = json_decode($coupon['users_list'], true);
                if (!in_array($order['user_id'], $coupon['users_list']))
                {
                    continue;
                }
            }
        }
        
        $available_coupons[] = $coupon;
    }
    return $available_coupons;
}

/**
 * 计算积分的价值（能抵多少钱）
 * @param   int     $integral   积分
 * @return  float   积分价值
 */
function value_of_integral($integral)
{
    $scale = floatval($GLOBALS['_CFG']['integral_scale']);

    return $scale > 0 ? round(($integral / 100) * $scale, 2) : 0;
}

/**
 * 计算指定的金额需要多少积分
 *
 * @access  public
 * @param   integer $value  金额
 * @return  void
 */
function integral_of_value($value)
{
    $scale = floatval($GLOBALS['_CFG']['integral_scale']);

    return $scale > 0 ? round($value / $scale * 100) : 0;
}

/**
 * 只取整數金額的積分
 *
 * @access  public
 * @param   integer $integral  積分
 * @return  void
 */
function imperfect_integral($integral)
{
    $scale = floatval($GLOBALS['_CFG']['integral_scale']);

    $imperfect =  $scale > 0 ?  $integral%(100 / $scale) : 0;
    $integral -= $imperfect;

    return $integral;
}

/**
 * 订单退款
 * @param   array   $order          订单
 * @param   int     $refund_type    退款方式 1 到帐户余额 2 到退款申请（先到余额，再申请提款） 3 不处理
 * @param   string  $refund_note    退款说明
 * @param   float   $refund_amount  退款金额（如果为0，取订单已付款金额）
 * @return  bool
 */
function order_refund($order, $refund_type, $refund_note, $refund_amount = 0)
{
    /* 检查参数 */
    $user_id = $order['user_id'];
    if ($user_id == 0 && $refund_type == 1)
    {
        die('anonymous, cannot return to account balance');
    }

    $amount = $refund_amount > 0 ? $refund_amount : $order['money_paid'];
    if ($amount <= 0)
    {
        return true;
    }

    if (!in_array($refund_type, array(1, 2, 3)))
    {
        die('invalid params');
    }

    /* 备注信息 */
    if ($refund_note)
    {
        $change_desc = $refund_note;
    }
    else
    {
        include_once(ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/admin/order.php');
        $change_desc = sprintf($GLOBALS['_LANG']['order_refund'], $order['order_sn']);
    }

    /* 处理退款 */
    if (1 == $refund_type)
    {
        log_account_change($user_id, $amount, 0, 0, 0, $change_desc);

        return true;
    }
    elseif (2 == $refund_type)
    {
        /* 如果非匿名，退回余额 */
        if ($user_id > 0)
        {
            log_account_change($user_id, $amount, 0, 0, 0, $change_desc);
        }

        /* user_account 表增加提款申请记录 */
        $account = array(
            'user_id'      => $user_id,
            'amount'       => (-1) * $amount,
            'add_time'     => gmtime(),
            'user_note'    => $refund_note,
            'process_type' => SURPLUS_RETURN,
            'admin_user'   => $_SESSION['admin_name'],
            'admin_note'   => sprintf($GLOBALS['_LANG']['order_refund'], $order['order_sn']),
            'is_paid'      => 0
        );
        $GLOBALS['db']->autoExecute($GLOBALS['ecs']->table('user_account'), $account, 'INSERT');

        return true;
    }
    else
    {
        return true;
    }
}

/**
 * 获得购物车中的商品
 *
 * @access  public
 * @return  array
 */
function get_cart_goods()
{

    /* 初始化 */
    $erpController = new Yoho\cms\Controller\ErpController();
    $packageController = new Yoho\cms\Controller\PackageController();
    
    $goods_list = array();
    $total = array(
        'goods_price'  => 0, // 本店售价合计（有格式）
        'market_price' => 0, // 市场售价合计（有格式）
        'saving'       => 0, // 节省金额（有格式）
        'save_rate'    => 0, // 节省百分比
        'goods_amount' => 0, // 本店售价合计（无格式）
		'gp2'          => 0, // 套餐优惠价
    );
    $auto_add_fav = [];

    /* 循环、统计 */
    $sql = "SELECT *, IF(parent_id, parent_id, goods_id) AS pid " .
            " FROM " . $GLOBALS['ecs']->table('cart') . " " .
            " WHERE session_id = '" . SESS_ID . "' AND rec_type = '" . CART_GENERAL_GOODS . "'" .
            " ORDER BY pid, parent_id";
    $res = $GLOBALS['db']->query($sql);

    /* 用于统计购物车中实体商品和虚拟商品的个数 */
    $virtual_goods_count = 0;
    $real_goods_count    = 0;
    $show_shipping = 0;
    $show_preorder = 0;
    $show_require_goods = 0;
    $total_goods = 0;
    $total_fls_item = 0;
    $total_wilson_item = 0;
    $packages = [];

    while ($row = $GLOBALS['db']->fetchRow($res))
    {
        $total['goods_price']  += $row['goods_price'] * $row['goods_number'];
        $total['market_price'] += $row['market_price'] * $row['goods_number'];

        $row['subtotal']     = price_format($row['goods_price'] * $row['goods_number'], false);
        $row['price']        = $row['goods_price'];
        $row['goods_price']  = price_format($row['goods_price'], false);
        $row['market_price'] = price_format($row['market_price'], false);
		$total_goods         = $total_goods + $row['goods_number'];

        /* 统计实体商品和虚拟商品的个数 */
        if ($row['is_real'])
        {
            $real_goods_count++;
        }
        else
        {
            $virtual_goods_count++;
        }

        /* show is free shipping */
        if($row['is_shipping'])
        {
            $show_shipping = 1;
        }

        /* 查询规格 */
        if (trim($row['goods_attr']) != '')
        {
            $row['goods_attr'] = addslashes($row['goods_attr']);//11-11 官方更新
            $sql = "SELECT attr_value FROM " . $GLOBALS['ecs']->table('goods_attr') . " WHERE goods_attr_id " .
            db_create_in($row['goods_attr']);
            $attr_list = $GLOBALS['db']->getCol($sql);
            foreach ($attr_list AS $attr)
            {
                $row['goods_name'] .= ' [' . $attr . '] ';
            }
        }
        
        if ($row['extension_code'] == 'package')
        {
            if (!in_array($row['is_package'], array_keys($packages))) {
                $packages[$row['is_package']] = $packageController->get_basic_package_info($row['is_package']);
                $packages[$row['is_package']]['qty'] = $row['goods_number'] / $packages[$row['is_package']]['items'][$row['goods_id']]['goods_number'];
                $packages[$row['is_package']]['total_price'] = bcmul($packages[$row['is_package']]['qty'], $packages[$row['is_package']]['base_price'], 2);
                $packages[$row['is_package']]['formated_total_price'] = price_format($packages[$row['is_package']]['total_price'], false);
                $packages[$row['is_package']]['rec_id'] = $row['rec_id'];
            }
            $row['package_goods_list'] = $packages[$row['is_package']]['items'];
        }

        $erpController = new Yoho\cms\Controller\ErpController();
        $orderController = new Yoho\cms\Controller\OrderController();
        $goodsController = new Yoho\cms\Controller\GoodsController();
        $flashdealController = new Yoho\cms\Controller\FlashdealController();

        if (!$flashdealController->checkFlashdealEventIsGoingToStart()) {
            if ($row['goods_number'] > $GLOBALS['db']->getOne("SELECT goods_number - reserved_number FROM (SELECT " . hw_goods_number_subquery('g.') . ", " . hw_reserved_number_subquery('g.') . " FROM " . $GLOBALS['ecs']->table('goods') . " g WHERE g.goods_id = $row[goods_id]) as tmp") && $row['is_insurance'] == 0 && $row['insurance_of'] == 0)
            {
                $row['need_pre_sale'] = 1;
                $show_preorder = 1;
            }
        }

        $require_goods_id = $GLOBALS['db']->getOne("SELECT require_goods_id FROM " . $GLOBALS['ecs']->table("goods") . " WHERE goods_id = $row[goods_id]");
        if (!empty($require_goods_id)) {
            if (!in_array($require_goods_id, $GLOBALS['db']->getCol("SELECT goods_id FROM " . $GLOBALS['ecs']->table("cart") . " WHERE session_id = '" . SESS_ID . "' AND rec_type = '" . CART_GENERAL_GOODS . "'"))) {
                $row['require_goods_id'] = $require_goods_id;
                $row['need_require_goods'] = 1;
                $row['need_require_goods_price'] = price_format(get_final_price($require_goods_id)['final_price'], false);
                $row['need_require_goods_name'] = $GLOBALS['db']->getOne("SELECT goods_name FROM " . $GLOBALS['ecs']->table("goods") . " WHERE goods_id = $require_goods_id");
                if ($GLOBALS['_CFG']['lang'] != "zh_tw") {
                    $require_goods_name_localized = $GLOBALS['db']->getOne("SELECT goods_name FROM " . $GLOBALS['ecs']->table("goods_lang") . " WHERE goods_id = $require_goods_id AND lang = '" . $GLOBALS['_CFG']['lang'] . "'");
                    if (!empty($require_goods_name_localized)) {
                        $row['need_require_goods_name'] = $require_goods_name_localized;
                    }
                }
                $show_require_goods = 1;
                if ($row['extension_code'] == 'package') {
                    $packages[$row['is_package']]['need_require_goods'][$row['goods_id']] = [
                        'goods_name'                => $packages[$row['is_package']]['package_name'] . " - " . $row['goods_name'],
                        'require_goods_id'          => $row['require_goods_id'],
                        'need_require_goods_price'  => $row['need_require_goods_price'],
                        'need_require_goods_name'   => $row['need_require_goods_name'],
                    ];
                }
            }
        }
        
        // howang: also get number of goods available for purchase
        $sql = "SELECT *, " . hw_goods_number_sql() . " FROM (" .
                "SELECT g.brand_id, IFNULL(bl.brand_name, b.brand_name) as brand_name, g.goods_thumb, " .
                hw_goods_number_subquery('g.') . ", " .
                hw_reserved_number_subquery('g.') . ", " .
                "g.is_pre_sale, g.pre_sale_date, g.pre_sale_days, g.pre_sale_time, g.pre_sale_string, gperma " .
                "FROM " . $GLOBALS['ecs']->table('goods') . " as g " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('brand') . " as b ON b.brand_id = g.brand_id " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('brand_lang'). " as bl ON bl.brand_id = b.brand_id AND bl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
                "LEFT JOIN (SELECT id, table_name, lang, perma_link as gperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") gpl ON gpl.id = g.goods_id  AND gpl.table_name = 'goods' AND gpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
                "WHERE g.goods_id = '" . $row['goods_id'] . "'" . 
                ") as tmp";
        $result = $GLOBALS['db']->getRow($sql);
        
		$row['bid'] = $result['brand_id'];
		$row['bname'] = $result['brand_name'];
        $row['goods_thumb'] = get_image_path($row['goods_id'], $result['goods_thumb'], true);
        $row['limit_number'] = $result['goods_number'];
        $userRankController = new Yoho\cms\Controller\UserRankController();
        $min_rank = $userRankController->getUserMinRank($row['goods_id'], $row['user_id']);
        $min_rank = $min_rank[$row['goods_id']];
        if($min_rank['user_price'] > 0 && $row['user_rank'] == $min_rank['rank_id']) {
            if (isset($min_rank['can_buy'])) {
                $row['limit_number'] = min($result['goods_number'],$min_rank['can_buy']);
            }
        }
        $row['pre_sale_days'] = $result['pre_sale_days'];
        $row['is_pre_sale'] = $result['is_pre_sale'];
        $row['pre_sale_date'] = $result['pre_sale_date'];
        $row['pre_sale_time'] = $result['pre_sale_time'];
        $row['pre_sale_string'] = $result['pre_sale_string'];
        $row['gperma'] = $result['gperma'];
        $time = gmtime();
        // if ($result['pre_sale_date'] > $time)
        // {
        //     $row['pre_sale_days'] = $orderController->count_yoho_work_day($result['pre_sale_date']);
        // }
        $row = $goodsController->getGoodsPreOrderInfo($row, 0, 0);
		$row['url'] = build_uri('goods', array('gid' => $row['goods_id'], 'gname' => $row['goods_name'], 'gperma' => $row['gperma']), $row['goods_name']);
		
        if ($row['extension_code'] == 'package')
        {
            if ($row['need_pre_sale']) {
                $packages[$row['is_package']]['need_pre_sale'][$row['goods_id']] = [
                    'goods_name'    => $packages[$row['is_package']]['package_name'] . " - " . $row['goods_name'],
                    'delivery_text' => $row['delivery_text'],
                ];
            }
            //$row['gp2'] = $packageController->get_package_goods_price($row['goods_id']) - $row['goods_price'];//套餐优惠价
            // $total['gp2']=price_format($row['gp2']-$row['goods_price']);//套餐优惠价
        }
        if (intval($row['is_gift']) > 0) {
            if (in_array(intval($row['is_gift']), $auto_add_fav)) {
                $row['is_auto_add'] = true;
            } else {
                $sql = "select act_type_ext, act_type, gift FROM ". $GLOBALS['ecs']->table('favourable_activity') ." WHERE act_id = ".$row['is_gift'];
                $favourable = $GLOBALS['db']->getRow($sql);
                $count_gift = count(unserialize($favourable['gift']));
                $total_gift_price = array_sum(array_column(unserialize($favourable['gift']), 'price'));
                if (( $favourable['act_type'] == FAT_GOODS && intval($favourable['act_type_ext']) > 0 && $count_gift > 0 && /*$count_gift == intval($favourable['act_type_ext']) &&*/ $total_gift_price == 0 )){
                    $auto_add_fav[] = intval($row['is_gift']);
                    $row['is_auto_add'] = true;
                }
            }
        }

        if ($row['is_insurance'] == 1 && $row['insurance_of'] > 0){
            $sql = "SELECT goods_number, goods_name ".
                    "FROM ".$GLOBALS['ecs']->table('cart')." ".
                    "WHERE session_id = '" . SESS_ID . "' ".
                    "AND goods_id = '".$row['insurance_of']."' ".
                    "AND extension_code != 'package' ".
                    "AND is_package = 0 ".
                    "AND is_gift = 0 ";
            $result = $GLOBALS['db']->getRow($sql);
            if (!empty($result)){
                $row['limit_number'] = $result['goods_number'];
                $row['insurance_of_name'] = $result['goods_name'];
            }
            $sql = "SELECT code ".
                    "FROM ".$GLOBALS['ecs']->table('insurance')." ".
                    "WHERE target_goods_id = '".$row['goods_id']."' ";
            $result = $GLOBALS['db']->getOne($sql);
            $row['tc_link'] = $GLOBALS['_LANG']['goods_buy_service_terms_conditions_link_'.$result];
        }
        
        $goods_list[] = $row;

        $warehouse_stock_result = $erpController->getSellableProductsQtyByWarehouses($row['goods_id']);
        $fls_warehouse_id = $erpController->getFlsWarehouseId();
        $wilson_warehouse_id = $erpController->getWilsonWarehouseId();

        // fls wilson
        $fls_warehouse_stock = $warehouse_stock_result[$row['goods_id']][$fls_warehouse_id];
        $wilson_warehouse_stock = $warehouse_stock_result[$row['goods_id']][$wilson_warehouse_id];

        if ($row['is_insurance'] == 0){
            // Count if delivered by fls
            if ($fls_warehouse_stock >= $row['goods_number']) {
                $total_fls_item += 1;
            } elseif ($fls_warehouse_stock > 0) {
                $total_fls_item += 0.5;
            }

            // Count if delivered by wilson
            if ($wilson_warehouse_stock >= $row['goods_number']) {
                $total_wilson_item += 1;
            } elseif ($wilson_warehouse_stock > 0) {
                $total_wilson_item += 0.5;
            }
        }
    }

    $total['goods_amount'] = $total['goods_price'];
    $total['saving']       = price_format($total['market_price'] - $total['goods_price'], false);
    if ($total['market_price'] > 0)
    {
        $total['save_rate'] = $total['market_price'] ? round(($total['market_price'] - $total['goods_price']) *
        100 / $total['market_price']).'%' : 0;
    }
    $total['goods_price']  = price_format($total['goods_price'], false);
    $total['market_price'] = price_format($total['market_price'], false);
    $total['real_goods_count']    = $real_goods_count;
    $total['virtual_goods_count'] = $virtual_goods_count;
    $total['show_shipping'] = $show_shipping;
    $total['show_preorder'] = $show_preorder;
    $total['show_require_goods'] = $show_require_goods;
    $total['total_goods'] = $total_goods;

    foreach ($packages as $key => $package) {
        $total_item = 0;
        foreach ($package['items'] as $item) {
            $total_item += $item['goods_number'];
        }
        $packages[$key]['total_item'] = $total_item;
    }

    if ($total_fls_item == count($goods_list)) {
        $ship_type = "fls";
    } elseif ($total_wilson_item == count($goods_list)) {
        $ship_type = "wilson";
    } elseif ($total_fls_item > 0 || $total_wilson_item > 0) {
        $ship_type = "mix";
    } else {
        $ship_type = "yoho";
    }
  
    return array('goods_list' => $goods_list, 'total' => $total, "ship_type" => $ship_type, 'packages' => $packages);
}

/**
 * 取得收货人信息
 * @param   int     $user_id    用户编号
 * @return  array
 */
function get_consignee($user_id, $verify = FALSE)
{
    if (isset($_SESSION['flow_consignee']) && $_SESSION['flow_consignee'] !== false)
    {
        /* 如果存在session，则直接返回session中的收货人信息 */

        $arr = $_SESSION['flow_consignee'];
    }
    else
    {
        /* 如果不存在，则取得用户的默认收货人信息 */
        $arr = array();

        if ($user_id > 0)
        {
            /* 取默认地址 */
            $sql = "SELECT ua.*".
                    " FROM " . $GLOBALS['ecs']->table('user_address') . "AS ua, ".$GLOBALS['ecs']->table('users').' AS u '.
                    " WHERE u.user_id='$user_id' AND ua.address_id = u.address_id";

            $arr = $GLOBALS['db']->getRow($sql);

        }
        //return $arr;
        
    }
    $arr['verify'] = 0;

    if (!empty($arr) && $verify){
        $addressController = new \Yoho\cms\Controller\AddressController();
        $correct = $addressController->check_user_address_correct($arr);
        if ($correct){
            $arr['verify'] = 1;
        }
    }
    
    return $arr;
}

/**
 * 查询购物车（订单id为0）或订单中是否有实体商品
 * @param   int     $order_id   订单id
 * @param   int     $flow_type  购物流程类型
 * @return  bool
 */
function exist_real_goods($order_id = 0, $flow_type = CART_GENERAL_GOODS)
{
    if ($order_id <= 0)
    {
        $sql = "SELECT COUNT(*) FROM " . $GLOBALS['ecs']->table('cart') .
                " WHERE session_id = '" . SESS_ID . "' AND is_real = 1 " .
                "AND rec_type = '$flow_type'";
    }
    else
    {
        $sql = "SELECT COUNT(*) FROM " . $GLOBALS['ecs']->table('order_goods') .
                " WHERE order_id = '$order_id' AND is_real = 1";
    }

    return $GLOBALS['db']->getOne($sql) > 0;
}

/**
 * 检查收货人信息是否完整
 * @param   array   $consignee  收货人信息
 * @param   int     $flow_type  购物流程类型
 * @return  bool    true 完整 false 不完整
 */
function check_consignee_info($consignee, $flow_type)
{
    if (exist_real_goods(0, $flow_type))
    {
        /* 如果存在实体商品 */
        $res = !empty($consignee['consignee']) &&
            !empty($consignee['country']) &&
            //!empty($consignee['email']) &&  //不判断邮箱
            !empty($consignee['tel']);

        if ($res)
        {
            if (empty($consignee['province']))
            {
                /* 没有设置省份，检查当前国家下面有没有设置省份 */
                $pro = get_regions(1, $consignee['country']);
                $res = empty($pro);
            }
            elseif (empty($consignee['city']))
            {
                /* 没有设置城市，检查当前省下面有没有城市 */
                $city = get_regions(2, $consignee['province']);
                $res = empty($city);
            }
            elseif (empty($consignee['district']))
            {
                $dist = get_regions(3, $consignee['city']);
                $res = empty($dist);
            }
        }

        return $res;
    }
    else
    {
        /* 如果不存在实体商品 */
        return !empty($consignee['consignee']) &&
            !empty($consignee['email']) &&
            !empty($consignee['tel']);
    }
}

/**
 * 获得上一次用户采用的支付和配送方式
 *
 * @access  public
 * @return  void
 */
function last_shipping_and_payment()
{
    $sql = "SELECT shipping_id, pay_id " .
            " FROM " . $GLOBALS['ecs']->table('order_info') .
            " WHERE user_id = '$_SESSION[user_id]' " .
            " ORDER BY order_id DESC LIMIT 1";
    $row = $GLOBALS['db']->getRow($sql);

    if (empty($row))
    {
        /* 如果获得是一个空数组，则返回默认值 */
        $row = array('shipping_id' => 0, 'pay_id' => 0);
    }

    return $row;
}

/**
 * 取得当前用户应该得到的红包总额
 */
function get_total_bonus()
{
    $day    = getdate();
    $today  = local_mktime(23, 59, 59, $day['mon'], $day['mday'], $day['year']);

    /* 按商品发的红包 */
    $sql = "SELECT SUM(c.goods_number * t.type_money)" .
            "FROM " . $GLOBALS['ecs']->table('cart') . " AS c, "
                    . $GLOBALS['ecs']->table('bonus_type') . " AS t, "
                    . $GLOBALS['ecs']->table('goods') . " AS g " .
            "WHERE c.session_id = '" . SESS_ID . "' " .
            "AND c.is_gift = 0 " .
            "AND c.goods_id = g.goods_id " .
            "AND g.bonus_type_id = t.type_id " .
            "AND t.send_type = '" . SEND_BY_GOODS . "' " .
            "AND t.send_start_date <= '$today' " .
            "AND t.send_end_date >= '$today' " .
            "AND c.rec_type = '" . CART_GENERAL_GOODS . "'";
    $goods_total = floatval($GLOBALS['db']->getOne($sql));

    /* 取得购物车中非赠品总金额 */
    $sql = "SELECT SUM(goods_price * goods_number) " .
            "FROM " . $GLOBALS['ecs']->table('cart') .
            " WHERE session_id = '" . SESS_ID . "' " .
            " AND is_gift = 0 " .
            " AND rec_type = '" . CART_GENERAL_GOODS . "'";
    $amount = floatval($GLOBALS['db']->getOne($sql));

    /* 按订单发的红包 */
    $sql = "SELECT FLOOR('$amount' / min_amount) * type_money " .
            "FROM " . $GLOBALS['ecs']->table('bonus_type') .
            " WHERE send_type = '" . SEND_BY_ORDER . "' " .
            " AND send_start_date <= '$today' " .
            "AND send_end_date >= '$today' " .
            "AND min_amount > 0 ";
    $order_total = floatval($GLOBALS['db']->getOne($sql));

    return $goods_total + $order_total;
}

/**
 * 处理红包（下订单时设为使用，取消（无效，退货）订单时设为未使用
 * @param   int     $bonus_id   红包编号
 * @param   int     $order_id   订单号
 * @param   int     $is_used    是否使用了
 */
function change_user_bonus($bonus_id, $order_id, $is_used = true)
{
    if ($is_used)
    {
        $sql = 'UPDATE ' . $GLOBALS['ecs']->table('user_bonus') . ' SET ' .
                'used_time = ' . gmtime() . ', ' .
                "order_id = '$order_id' " .
                "WHERE bonus_id = '$bonus_id'";
    }
    else
    {
        $sql = 'UPDATE ' . $GLOBALS['ecs']->table('user_bonus') . ' SET ' .
                'used_time = 0, ' .
                'order_id = 0 ' .
                "WHERE bonus_id = '$bonus_id'";
    }
    $GLOBALS['db']->query($sql);
}

/**
 * 获得订单信息
 *
 * @access  private
 * @return  array
 */
function flow_order_info()
{
    $order = isset($_SESSION['flow_order']) ? $_SESSION['flow_order'] : array();

    /* 初始化配送和支付方式 */
    if (!isset($order['shipping_id']) || !isset($order['pay_id']))
    {
        //如果还没有设置配送和支付
        // if ($_SESSION['user_id'] > 0)
        // {
        //     $arr = last_shipping_and_payment();  //sbsn 这条应该可以注释掉 避免错误选择 ly20130815
        //
        //     if (!isset($order['shipping_id']))
        //     {
        //         $order['shipping_id'] = $arr['shipping_id'];
        //     }
        //     if (!isset($order['pay_id']))
        //     {
        //         $order['pay_id'] = $arr['pay_id'];
        //     }
        // }
        // else
        {
            if (!isset($order['shipping_id']))
            {
                $order['shipping_id'] = 0;
            }
            if (!isset($order['pay_id']))
            {
                $order['pay_id'] = 0;
            }
        }
    }

    if (!isset($order['pack_id']))
    {
        $order['pack_id'] = 0;  // 初始化包装
    }
    if (!isset($order['card_id']))
    {
        $order['card_id'] = 0;  // 初始化贺卡
    }
    if (!isset($order['bonus']))
    {
        $order['bonus'] = 0;    // 初始化红包
    }
    if (!isset($order['coupon']))
    {
        $order['coupon'] = 0;    // Initialize coupon
    }
    if (!isset($order['integral']))
    {
        $order['integral'] = 0; // 初始化积分
    }
    if (!isset($order['surplus']))
    {
        $order['surplus'] = 0;  // 初始化余额
    }

    /* 扩展信息 */
    if (isset($_SESSION['flow_type']) && intval($_SESSION['flow_type']) != CART_GENERAL_GOODS)
    {
        $order['extension_code'] = $_SESSION['extension_code'];
        $order['extension_id'] = $_SESSION['extension_id'];
    }

    return $order;
}

/**
 * 合并订单
 * @param   string  $from_order_sn  从订单号
 * @param   string  $to_order_sn    主订单号
 * @return  成功返回true，失败返回错误信息
 */
function merge_order($from_order_sn, $to_order_sn)
{
    /* 订单号不能为空 */
    if (trim($from_order_sn) == '' || trim($to_order_sn) == '')
    {
        return $GLOBALS['_LANG']['order_sn_not_null'];
    }

    /* 订单号不能相同 */
    if ($from_order_sn == $to_order_sn)
    {
        return $GLOBALS['_LANG']['two_order_sn_same'];
    }

    /* 取得订单信息 */
    $from_order = order_info(0, $from_order_sn);
    $to_order   = order_info(0, $to_order_sn);

    /* 检查订单是否存在 */
    if (!$from_order)
    {
        return sprintf($GLOBALS['_LANG']['order_not_exist'], $from_order_sn);
    }
    elseif (!$to_order)
    {
        return sprintf($GLOBALS['_LANG']['order_not_exist'], $to_order_sn);
    }

    /* 检查合并的订单是否为普通订单，非普通订单不允许合并 */
    if ($from_order['extension_code'] != '' || $to_order['extension_code'] != 0)
    {
        return $GLOBALS['_LANG']['merge_invalid_order'];
    }

    /* 检查订单状态是否是已确认或未确认、未付款、未发货 */
    if ($from_order['order_status'] != OS_UNCONFIRMED && $from_order['order_status'] != OS_CONFIRMED)
    {
        return sprintf($GLOBALS['_LANG']['os_not_unconfirmed_or_confirmed'], $from_order_sn);
    }
    elseif ($from_order['pay_status'] != PS_UNPAYED)
    {
        return sprintf($GLOBALS['_LANG']['ps_not_unpayed'], $from_order_sn);
    }
    elseif ($from_order['shipping_status'] != SS_UNSHIPPED)
    {
        return sprintf($GLOBALS['_LANG']['ss_not_unshipped'], $from_order_sn);
    }

    if ($to_order['order_status'] != OS_UNCONFIRMED && $to_order['order_status'] != OS_CONFIRMED)
    {
        return sprintf($GLOBALS['_LANG']['os_not_unconfirmed_or_confirmed'], $to_order_sn);
    }
    elseif ($to_order['pay_status'] != PS_UNPAYED)
    {
        return sprintf($GLOBALS['_LANG']['ps_not_unpayed'], $to_order_sn);
    }
    elseif ($to_order['shipping_status'] != SS_UNSHIPPED)
    {
        return sprintf($GLOBALS['_LANG']['ss_not_unshipped'], $to_order_sn);
    }

    /* 检查订单用户是否相同 */
    if ($from_order['user_id'] != $to_order['user_id'])
    {
        return $GLOBALS['_LANG']['order_user_not_same'];
    }

    /* 合并订单 */
    $order = $to_order;
    $order['order_id']  = '';
    $order['add_time']  = gmtime();

    // 合并商品总额
    $order['goods_amount'] += $from_order['goods_amount'];

    // 合并折扣
    $order['discount'] += $from_order['discount'];

    if ($order['shipping_id'] > 0)
    {
        // 重新计算配送费用
        $weight_price       = order_weight_price($to_order['order_id']);
        $from_weight_price  = order_weight_price($from_order['order_id']);
        $weight_price['weight'] += $from_weight_price['weight'];
        $weight_price['amount'] += $from_weight_price['amount'];
        $weight_price['number'] += $from_weight_price['number'];
        $weight_price['price']  += $from_weight_price['price'];

        $region_id_list = array($order['country'], $order['province'], $order['city'], $order['district']);
        $shipping_area = shipping_area_info($order['shipping_id'], $region_id_list);

        $order['shipping_fee'] = shipping_fee($shipping_area['shipping_code'],
            unserialize($shipping_area['configure']), $weight_price['weight'], $weight_price['amount'], $weight_price['number'], $weight_price['price']);

        // 如果保价了，重新计算保价费
        if ($order['insure_fee'] > 0)
        {
            $order['insure_fee'] = shipping_insure_fee($shipping_area['shipping_code'], $order['goods_amount'], $shipping_area['insure']);
        }
    }

    // 重新计算包装费、贺卡费
    if ($order['pack_id'] > 0)
    {
        $pack = pack_info($order['pack_id']);
        $order['pack_fee'] = $pack['free_money'] > $order['goods_amount'] ? $pack['pack_fee'] : 0;
    }
    if ($order['card_id'] > 0)
    {
        $card = card_info($order['card_id']);
        $order['card_fee'] = $card['free_money'] > $order['goods_amount'] ? $card['card_fee'] : 0;
    }

    // 红包不变，合并积分、余额、已付款金额
    $order['integral']      += $from_order['integral'];
    $order['integral_money'] = value_of_integral($order['integral']);
    $order['surplus']       += $from_order['surplus'];
    $order['money_paid']    += $from_order['money_paid'];

    // 计算应付款金额（不包括支付费用）
    $order['order_amount'] = $order['goods_amount'] - $order['discount']
                           + $order['shipping_fee']
                           + $order['insure_fee']
                           + $order['pack_fee']
                           + $order['card_fee']
                           - $order['bonus']
                           - $order['coupon']
                           - $order['integral_money']
                           - $order['surplus']
                           - $order['money_paid'];

    if ($order['add_time'] > 1562700715 && ($order['platform'] == "web" || $order['platform'] == "mobile")){
        $order['order_amount'] += $order['cn_ship_tax'];
    }

    // 重新计算支付费
    if ($order['pay_id'] > 0)
    {
        // 货到付款手续费
        $cod_fee          = $shipping_area ? $shipping_area['pay_fee'] : 0;
        $order['pay_fee'] = pay_fee($order['pay_id'], $order['order_amount'], $cod_fee);

        // 应付款金额加上支付费
        $order['order_amount'] += $order['pay_fee'];
    }

    /* 插入订单表 */
    do
    {
        $order['order_sn'] = get_order_sn();
        if ($GLOBALS['db']->autoExecute($GLOBALS['ecs']->table('order_info'), addslashes_deep($order), 'INSERT'))
        {
            break;
        }
        else
        {
            if ($GLOBALS['db']->errno() != 1062)
            {
                die($GLOBALS['db']->errorMsg());
            }
        }
    }
    while (true); // 防止订单号重复

    /* 订单号 */
    $order_id = $GLOBALS['db']->insert_id();

    /* 更新订单商品 */
    $sql = 'UPDATE ' . $GLOBALS['ecs']->table('order_goods') .
            " SET order_id = '$order_id' " .
            "WHERE order_id " . db_create_in(array($from_order['order_id'], $to_order['order_id']));
    $GLOBALS['db']->query($sql);

    include_once(ROOT_PATH . 'includes/lib_clips.php');
    /* 插入支付日志 */
    insert_pay_log($order_id, $order['order_amount'], PAY_ORDER);

    /* 删除原订单 */
    $sql = 'DELETE FROM ' . $GLOBALS['ecs']->table('order_info') .
            " WHERE order_id " . db_create_in(array($from_order['order_id'], $to_order['order_id']));
    $GLOBALS['db']->query($sql);

    /* 删除原订单支付日志 */
    $sql = 'DELETE FROM ' . $GLOBALS['ecs']->table('pay_log') .
            " WHERE order_id " . db_create_in(array($from_order['order_id'], $to_order['order_id']));
    $GLOBALS['db']->query($sql);

    /* 返还 from_order 的红包，因为只使用 to_order 的红包 */
    if ($from_order['bonus_id'] > 0)
    {
        unuse_bonus($from_order['bonus_id']);
    }

    /* 返回成功 */
    return true;
}

/**
 * 查询配送区域属于哪个办事处管辖
 * @param   array   $regions    配送区域（1、2、3、4级按顺序）
 * @return  int     办事处id，可能为0
 */
function get_agency_by_regions($regions)
{
    if (!is_array($regions) || empty($regions))
    {
        return 0;
    }

    $arr = array();
    $sql = "SELECT region_id, agency_id " .
            "FROM " . $GLOBALS['ecs']->table('region') .
            " WHERE region_id " . db_create_in($regions) .
            " AND region_id > 0 AND agency_id > 0";
    $res = $GLOBALS['db']->query($sql);
    while ($row = $GLOBALS['db']->fetchRow($res))
    {
        $arr[$row['region_id']] = $row['agency_id'];
    }
    if (empty($arr))
    {
        return 0;
    }

    $agency_id = 0;
    for ($i = count($regions) - 1; $i >= 0; $i--)
    {
        if (isset($arr[$regions[$i]]))
        {
            return $arr[$regions[$i]];
        }
    }
}

/**
 * 获取配送插件的实例
 * @param   int   $shipping_id    配送插件ID
 * @return  object     配送插件对象实例
 */
function &get_shipping_object($shipping_id)
{
    $shipping  = shipping_info($shipping_id);
    if (!$shipping)
    {
        $object = new stdClass();
        return $object;
    }

    $file_path = ROOT_PATH.'includes/modules/shipping/' . $shipping['shipping_code'] . '.php';

    include_once($file_path);

    $object = new $shipping['shipping_code'];
    return $object;
}

/**
 * 改变订单中商品库存
 * @param   int     $order_id   订单号
 * @param   bool    $is_dec     是否减少库存
 * @param   bool    $storage     减库存的时机，1，下订单时；0，发货时；
 */
function change_order_goods_storage($order_id, $is_dec = true, $storage = 0)
{
    /* 查询订单商品信息 */
    switch ($storage)
    {
        case 0 :
            $sql = "SELECT goods_id, SUM(send_number) AS num, MAX(extension_code) AS extension_code, product_id FROM " . $GLOBALS['ecs']->table('order_goods') .
                    " WHERE order_id = '$order_id' AND is_real = 1 GROUP BY goods_id, product_id";
        break;

        case 1 :
            $sql = "SELECT goods_id, SUM(goods_number) AS num, MAX(extension_code) AS extension_code, product_id FROM " . $GLOBALS['ecs']->table('order_goods') .
                    " WHERE order_id = '$order_id' AND is_real = 1 GROUP BY goods_id, product_id";
        break;
    }

    $res = $GLOBALS['db']->query($sql);
    while ($row = $GLOBALS['db']->fetchRow($res))
    {
        if ($row['extension_code'] != "package")
        {
            if ($is_dec)
            {
                change_goods_storage($row['goods_id'], $row['product_id'], - $row['num']);
            }
            else
            {
                change_goods_storage($row['goods_id'], $row['product_id'], $row['num']);
            }
            $GLOBALS['db']->query($sql);
        }
        else
        {
            $sql = "SELECT goods_id, goods_number" .
                   " FROM " . $GLOBALS['ecs']->table('package_goods') .
                   " WHERE package_id = '" . $row['goods_id'] . "'";
            $res_goods = $GLOBALS['db']->query($sql);
            while ($row_goods = $GLOBALS['db']->fetchRow($res_goods))
            {
                $sql = "SELECT is_real" .
                   " FROM " . $GLOBALS['ecs']->table('goods') .
                   " WHERE goods_id = '" . $row_goods['goods_id'] . "'";
                $real_goods = $GLOBALS['db']->query($sql);
                $is_goods = $GLOBALS['db']->fetchRow($real_goods);

                if ($is_dec)
                {
                    change_goods_storage($row_goods['goods_id'], $row['product_id'], - ($row['num'] * $row_goods['goods_number']));
                }
                elseif ($is_goods['is_real'])
                {
                    change_goods_storage($row_goods['goods_id'], $row['product_id'], ($row['num'] * $row_goods['goods_number']));
                }
            }
        }
    }

}

/**
 * 商品库存增与减 货品库存增与减
 *
 * @param   int    $good_id         商品ID
 * @param   int    $product_id      货品ID
 * @param   int    $number          增减数量，默认0；
 *
 * @return  bool               true，成功；false，失败；
 */
function change_goods_storage($good_id, $product_id, $number = 0)
{
    if ($number == 0)
    {
        return true; // 值为0即不做、增减操作，返回true
    }

    if (empty($good_id) || empty($number))
    {
        return false;
    }

    $number = ($number > 0) ? '+ ' . $number : $number;

    /* 处理货品库存 */
    $products_query = true;
    if (!empty($product_id))
    {
        $sql = "UPDATE " . $GLOBALS['ecs']->table('products') ."
                SET product_number = product_number $number
                WHERE goods_id = '$good_id'
                AND product_id = '$product_id'
                LIMIT 1";
        $products_query = $GLOBALS['db']->query($sql);
    }

    /* 处理商品库存 */
    $sql = "UPDATE " . $GLOBALS['ecs']->table('goods') ."
            SET goods_number = goods_number $number
            WHERE goods_id = '$good_id'
            LIMIT 1";
    $query = $GLOBALS['db']->query($sql);

    if ($query && $products_query)
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
 * 取得支付方式id列表
 * @param   bool    $is_cod 是否货到付款
 * @return  array
 */
function payment_id_list($is_cod)
{
    $sql = "SELECT pay_id FROM " . $GLOBALS['ecs']->table('payment');
    if ($is_cod)
    {
        $sql .= " WHERE is_cod = 1";
    }
    else
    {
        $sql .= " WHERE is_cod = 0";
    }

    return $GLOBALS['db']->getCol($sql);
}

/**
 * 生成查询订单的sql
 * @param   string  $type   类型
 * @param   string  $alias  order表的别名（包括.例如 o.）
 * @return  string
 */
function order_query_sql($type = 'finished', $alias = '')
{
    /* 已完成订单 */
    if ($type == 'finished')
    {
        return " AND {$alias}order_status " . db_create_in(array(OS_CONFIRMED, OS_SPLITED)) .
               " AND {$alias}shipping_status " . db_create_in(array(SS_SHIPPED, SS_RECEIVED)) .
               " AND {$alias}pay_status " . db_create_in(array(PS_PAYED, PS_PAYING)) . " ";
    }
    /* 待发货订单 */
    elseif ($type == 'await_ship')
    {
        return " AND   {$alias}order_status " .
                 db_create_in(array(OS_CONFIRMED, OS_SPLITED, OS_SPLITING_PART)) .
               " AND   {$alias}shipping_status " .
                 db_create_in(array(SS_UNSHIPPED, SS_PREPARING, SS_SHIPPED_ING, SS_SHIPPED_PART)) .
               " AND ( {$alias}pay_status " . db_create_in(array(PS_PAYED, PS_PAYING)) . " OR {$alias}pay_id " . db_create_in(payment_id_list(true)) . ") ";
    }
    /* 待付款订单 */
    elseif ($type == 'await_pay')
    {
        return " AND   {$alias}order_status " . db_create_in(array(OS_CONFIRMED, OS_SPLITED)) .
               " AND   {$alias}pay_status = '" . PS_UNPAYED . "'" .
               " AND ( {$alias}shipping_status " . db_create_in(array(SS_SHIPPED, SS_RECEIVED)) . " OR {$alias}pay_id " . db_create_in(payment_id_list(false)) . ") ";
    }
    /* 未确认订单 */
    elseif ($type == 'unconfirmed')
    {
        return " AND {$alias}order_status = '" . OS_UNCONFIRMED . "' ";
    }
    /* 未处理订单：用户可操作 */
    elseif ($type == 'unprocessed')
    {
        return " AND {$alias}order_status " . db_create_in(array(OS_UNCONFIRMED, OS_CONFIRMED)) .
               " AND {$alias}shipping_status = '" . SS_UNSHIPPED . "'" .
               " AND {$alias}pay_status = '" . PS_UNPAYED . "' ";
    }
    /* 未付款未发货订单：管理员可操作 */
    elseif ($type == 'unpay_unship')
    {
        return " AND {$alias}order_status " . db_create_in(array(OS_UNCONFIRMED, OS_CONFIRMED)) .
               " AND {$alias}shipping_status " . db_create_in(array(SS_UNSHIPPED, SS_PREPARING)) .
               " AND {$alias}pay_status = '" . PS_UNPAYED . "' ";
    }
    /* 已发货订单：不论是否付款 */
    elseif ($type == 'shipped')
    {
        return //" AND {$alias}order_status = '" . OS_CONFIRMED . "'" .
               " AND {$alias}order_status " . db_create_in(array(OS_CONFIRMED,OS_SPLITED,OS_SPLITING_PART)) .
               " AND {$alias}shipping_status " . db_create_in(array(SS_SHIPPED, SS_RECEIVED)) . " ";
    }
    /* 已確認, 已付款, 不論是否發貨 */
    elseif ($type == 'payed')
    {
        return //" AND {$alias}order_status = '" . OS_CONFIRMED . "'" .
               " AND {$alias}order_status " . db_create_in(array(OS_CONFIRMED, OS_SPLITED, OS_SPLITING_PART)) .
               " AND {$alias}pay_status " . db_create_in(array(PS_PAYED)) . " ";
    }
    /* 已发货订单：未收貨確定 */
    elseif ($type == 'shipped_unconfirm')
    {
        return //" AND {$alias}order_status = '" . OS_CONFIRMED . "'" .
               " AND {$alias}order_status " . db_create_in(array(OS_CONFIRMED,OS_SPLITED,OS_SPLITING_PART)) .
               " AND {$alias}shipping_status " . db_create_in(array(SS_SHIPPED, SS_SHIPPED_PART)) . " ";
    }
    else
    {
        die('函数 order_query_sql 参数错误');
    }
}

/**
 * 生成查询订单总金额的字段
 * @param   string  $alias  order表的别名（包括.例如 o.）
 * @return  string
 */
function order_amount_field($alias = '')
{
    return "   {$alias}goods_amount + {$alias}tax + {$alias}shipping_fee" .
           " + {$alias}insure_fee + {$alias}pay_fee + {$alias}pack_fee" .
           " + {$alias}card_fee + {$alias}cn_ship_tax";
}

/**
 * 生成计算应付款金额的字段
 * @param   string  $alias  order表的别名（包括.例如 o.）
 * @return  string
 */
function order_due_field($alias = '')
{
    return order_amount_field($alias) .
            " - {$alias}money_paid - {$alias}surplus - {$alias}integral_money" .
            " - {$alias}bonus - {$alias}coupon - {$alias}discount ";
}
/**
 * 生成计算应付款金额的字段
 * @param   string  $alias  order表的别名（包括.例如 o.）
 * @param   bool    $sa     是否保留代理銷售訂單
 * @return  string
 */
function where_exclude($alias="",$sa = false){
    global $userController;
    $where = "";
    if($sa){
        $where .= " AND (".$alias."order_type NOT IN ('" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE . "') OR (".$alias."order_type IS NULL AND ".$alias."user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).") )  OR ".$alias."order_type IS NULL) ";
    } else {
        $where .= " AND (".$alias."order_type NOT IN ('" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT . "','" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE . "') OR (".$alias."order_type IS NULL AND ".$alias."user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).") ) ) " .
                " AND (".$alias."order_type <> '" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT . "' OR ".$alias."order_type IS NULL) ";                
    }
    return $where . " AND ".$alias."postscript NOT LIKE '%換貨，%' AND " . order_amount_field($alias) . " > 0 ";    
}
/**
 * 计算折扣：根据购物车和优惠活动
 * @return  float   折扣
 */
function compute_discount()
{
    global $ecs, $_CFG, $db;
    /* 查询优惠活动 */
    $now = gmtime();
    $user_rank = ',' . $_SESSION['user_rank'] . ',';
    $favourable_list = [];
    $sql = "SELECT *" .
            "FROM " . $GLOBALS['ecs']->table('favourable_activity') .
            " WHERE start_time <= '$now'" .
            " AND end_time >= '$now'" .
            " AND status = 1" .
            " AND CONCAT(',', user_rank, ',') LIKE '%" . $user_rank . "%'" .
            " AND act_type " . db_create_in(array(FAT_DISCOUNT, FAT_PRICE));
    $favourable_list = $GLOBALS['db']->getAll($sql);

    /* 查询购物车商品 */
    $sql = "SELECT c.goods_id, c.goods_price * c.goods_number AS subtotal, g.cat_id, g.brand_id " .
            "FROM " . $GLOBALS['ecs']->table('cart') . " AS c, " . $GLOBALS['ecs']->table('goods') . " AS g " .
            "WHERE c.goods_id = g.goods_id " .
            "AND c.session_id = '" . SESS_ID . "' " .
            "AND c.parent_id = 0 " .
            "AND c.is_gift = 0 " .
            "AND rec_type = '" . CART_GENERAL_GOODS . "'";
    $goods_list = $GLOBALS['db']->getAll($sql);
    if (!$goods_list)
    {
        return 0;
    }

    /* 初始化折扣 */
    $discount = 0;
    $favourable_name = array();

    /* 循环计算每个优惠活动的折扣 */
    foreach ($favourable_list as $favourable)
    {
        if(!empty($favourable['users_list'])){
            $users_list = json_decode($favourable['users_list']);
            $user_id = $_SESSION['user_id'];

            if(!in_array($user_id, $users_list)){
                continue;
            }
        }
        $total_amount = 0;
        if ($favourable['act_range'] == FAR_ALL)
        {
            foreach ($goods_list as $goods)
            {
                $total_amount += $goods['subtotal'];
            }
        }
        elseif ($favourable['act_range'] == FAR_CATEGORY)
        {
            /* 找出分类id的子分类id */
            $id_list = array();
            $raw_id_list = explode(',', $favourable['act_range_ext']);
            foreach ($raw_id_list as $id)
            {
                $id_list = array_merge($id_list, array_keys(cat_list($id, 0, false)));
            }
            $ids = join(',', array_unique($id_list));

            foreach ($goods_list as $goods)
            {
                if (strpos(',' . $ids . ',', ',' . $goods['cat_id'] . ',') !== false)
                {
                    $total_amount += $goods['subtotal'];
                }
            }
        }
        elseif ($favourable['act_range'] == FAR_BRAND)
        {
            foreach ($goods_list as $goods)
            {
                if (strpos(',' . $favourable['act_range_ext'] . ',', ',' . $goods['brand_id'] . ',') !== false)
                {
                    $total_amount += $goods['subtotal'];
                }
            }
        }
        elseif ($favourable['act_range'] == FAR_GOODS)
        {
            foreach ($goods_list as $goods)
            {
                if (strpos(',' . $favourable['act_range_ext'] . ',', ',' . $goods['goods_id'] . ',') !== false)
                {
                    $total_amount += $goods['subtotal'];
                }
            }
        }
        else
        {
            continue;
        }

        /* 如果金额满足条件，累计折扣 */
        if ($total_amount > 0 && $total_amount >= $favourable['min_amount'] && ($total_amount <= $favourable['max_amount'] || $favourable['max_amount'] == 0))
        {
            if ($favourable['act_type'] == FAT_DISCOUNT)
            {
                $discount += $total_amount * (1 - $favourable['act_type_ext'] / 100);

                $favourable_name[] = $favourable['act_name'];
            }
            elseif ($favourable['act_type'] == FAT_PRICE)
            {
                $discount += $favourable['act_type_ext'];

                $favourable_name[] = $favourable['act_name'];
            }
        }
    }
    // Flow : Handle order affiliate
    $affiliate = unserialize($_CFG['affiliate']);
    // Normal affiliate
    if(isset($affiliate['on']) && $affiliate['on'] == 1)
    {
        $user_id = $_SESSION['user_id'];
        $user    = user_info($user_id);
        $parent_id = $user['parent_id'];
        $total_amount = 0;
        foreach ($goods_list as $goods) {
            $total_amount += $goods['subtotal'];
        }
        $orderController = new Yoho\cms\Controller\OrderController();
        $can_affiliate   = $orderController->check_order_can_separate($user_id, $total_amount, null);;
        if($user_id == $parent_id) { // Can't user == affiliate
            $can_affiliate = false;
        }
        // We can cal this order affiliate discount:
        if($can_affiliate && !empty($affiliate['config']['instant_money_discount'])) {

            $instant_money_discount = $affiliate['config']['instant_money_discount'];
            if (strpos($instant_money_discount,'%') === false) {
                // 實際現金折扣
                $discount += $instant_money_discount;
            } else {
                $affiliate_discount = $total_amount * (floatval($instant_money_discount) / 100);
                $discount += $affiliate_discount;
            }
        }
    }
    return array('discount' => $discount, 'name' => $favourable_name);
}

/**
 * 取得购物车该赠送的积分数
 * @return  int     积分数
 */
function get_give_integral()
{
        $sql = "SELECT SUM(c.goods_number * IF(g.give_integral > -1, g.give_integral, c.goods_price))" .
                "FROM " . $GLOBALS['ecs']->table('cart') . " AS c, " .
                          $GLOBALS['ecs']->table('goods') . " AS g " .
                "WHERE c.goods_id = g.goods_id " .
                "AND c.session_id = '" . SESS_ID . "' " .
                "AND c.goods_id > 0 " .
                "AND c.parent_id = 0 " .
                "AND c.rec_type = 0 " .
                "AND c.is_gift = 0";

        return intval($GLOBALS['db']->getOne($sql));
}

/**
 * 取得某订单应该赠送的积分数
 * @param   array   $order  订单
 * @return  int     积分数
 */
function integral_to_give($order)
{
    /* 判断是否团购 */
    if ($order['extension_code'] == 'group_buy')
    {
        include_once(ROOT_PATH . 'includes/lib_goods.php');
        $group_buy = group_buy_info(intval($order['extension_id']));

        return array('custom_points' => $group_buy['gift_integral'], 'rank_points' => $order['goods_amount']);
    }
    elseif(isset($order['order_type']) && in_array($order['order_type'],[\Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE, Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT])){
        //Not giving out points to wholesale, sales agent
        return array('custom_points' => 0, 'rank_points' => 0);
    }
    else
    {
        $sql = "SELECT SUM(og.goods_number * IF(g.give_integral > -1, g.give_integral, og.goods_price)) AS custom_points, SUM(og.goods_number * IF(g.rank_integral > -1, g.rank_integral, og.goods_price)) AS rank_points " .
                "FROM " . $GLOBALS['ecs']->table('order_goods') . " AS og, " .
                          $GLOBALS['ecs']->table('goods') . " AS g " .
                "WHERE og.goods_id = g.goods_id " .
                "AND og.order_id = '$order[order_id]' " .
                "AND og.goods_id > 0 " .
                "AND og.parent_id = 0 " .
                "AND og.is_gift = 0";
        return $GLOBALS['db']->getRow($sql);
    }
}

/**
 * 发红包：发货时发红包
 * @param   int     $order_id   订单号
 * @return  bool
 */
function send_order_bonus($order_id)
{
    /* 取得订单应该发放的红包 */
    $bonus_list = order_bonus($order_id);

    /* 如果有红包，统计并发送 */
    if ($bonus_list)
    {
        /* 用户信息 */
        $sql = "SELECT u.user_id, u.user_name, u.email " .
                "FROM " . $GLOBALS['ecs']->table('order_info') . " AS o, " .
                          $GLOBALS['ecs']->table('users') . " AS u " .
                "WHERE o.order_id = '$order_id' " .
                "AND o.user_id = u.user_id ";
        $user = $GLOBALS['db']->getRow($sql);

        /* 统计 */
        $count = 0;
        $money = '';
        foreach ($bonus_list AS $bonus)
        {
            $count += $bonus['number'];
            $money .= price_format($bonus['type_money']) . ' [' . $bonus['number'] . '], ';

            /* 修改用户红包 */
            $sql = "INSERT INTO " . $GLOBALS['ecs']->table('user_bonus') . " (bonus_type_id, user_id) " .
                    "VALUES('$bonus[type_id]', '$user[user_id]')";
            for ($i = 0; $i < $bonus['number']; $i++)
            {
                if (!$GLOBALS['db']->query($sql))
                {
                    return $GLOBALS['db']->errorMsg();
                }
            }
        }

        /* 如果有红包，发送邮件 */
        if ($count > 0)
        {
            $tpl = get_mail_template('send_bonus');
            $GLOBALS['smarty']->assign('user_name', $user['user_name']);
            $GLOBALS['smarty']->assign('count', $count);
            $GLOBALS['smarty']->assign('money', $money);
            $GLOBALS['smarty']->assign('shop_name', $GLOBALS['_CFG']['shop_name']);
            $GLOBALS['smarty']->assign('send_date', local_date($GLOBALS['_CFG']['date_format']));
            $GLOBALS['smarty']->assign('sent_date', local_date($GLOBALS['_CFG']['date_format']));
            $content = $GLOBALS['smarty']->fetch('str:' . $tpl['template_content']);
            send_mail($user['user_name'], $user['email'], $tpl['template_subject'], $content, $tpl['is_html']);
        }
    }

    return true;
}

/**
 * 返回订单发放的红包
 * @param   int     $order_id   订单id
 */
function return_order_bonus($order_id)
{
    /* 取得订单应该发放的红包 */
    $bonus_list = order_bonus($order_id);

    /* 删除 */
    if ($bonus_list)
    {
        /* 取得订单信息 */
        $order = order_info($order_id);
        $user_id = $order['user_id'];

        foreach ($bonus_list AS $bonus)
        {
            $sql = "DELETE FROM " . $GLOBALS['ecs']->table('user_bonus') .
                    " WHERE bonus_type_id = '$bonus[type_id]' " .
                    "AND user_id = '$user_id' " .
                    "AND order_id = '0' LIMIT " . $bonus['number'];
            if(!$GLOBALS['db']->query($sql))
            {
            	return false;
            }
        }
    }
    return true;
}

/**
 * 取得订单应该发放的红包
 * @param   int     $order_id   订单id
 * @return  array
 */
function order_bonus($order_id)
{
    /* 查询按商品发的红包 */
    $day    = getdate();
    $today  = local_mktime(23, 59, 59, $day['mon'], $day['mday'], $day['year']);

    $sql = "SELECT b.type_id, b.type_money, SUM(o.goods_number) AS number " .
            "FROM " . $GLOBALS['ecs']->table('order_goods') . " AS o, " .
                      $GLOBALS['ecs']->table('goods') . " AS g, " .
                      $GLOBALS['ecs']->table('bonus_type') . " AS b " .
            " WHERE o.order_id = '$order_id' " .
            " AND o.is_gift = 0 " .
            " AND o.goods_id = g.goods_id " .
            " AND g.bonus_type_id = b.type_id " .
            " AND b.send_type = '" . SEND_BY_GOODS . "' " .
            " AND b.send_start_date <= '$today' " .
            " AND b.send_end_date >= '$today' " .
            " GROUP BY b.type_id ";
    $list = $GLOBALS['db']->getAll($sql);

    /* 查询定单中非赠品总金额 */
    $amount = order_amount($order_id, false);

    /* 查询订单日期 */
    $sql = "SELECT add_time " .
            " FROM " . $GLOBALS['ecs']->table('order_info') .
            " WHERE order_id = '$order_id' LIMIT 1";
    $order_time = $GLOBALS['db']->getOne($sql);

    /* 查询按订单发的红包 */
    $sql = "SELECT type_id, type_money, IFNULL(FLOOR('$amount' / min_amount), 1) AS number " .
            "FROM " . $GLOBALS['ecs']->table('bonus_type') .
            "WHERE send_type = '" . SEND_BY_ORDER . "' " .
            "AND send_start_date <= '$order_time' " .
            "AND send_end_date >= '$order_time' ";
    $list = array_merge($list, $GLOBALS['db']->getAll($sql));

    return $list;
}

/**
 * 计算购物车中的商品能享受红包支付的总额
 * @return  float   享受红包支付的总额
 */
function compute_discount_amount()
{
    /* 查询优惠活动 */
    $now = gmtime();
    $user_rank = ',' . $_SESSION['user_rank'] . ',';
    $sql = "SELECT *" .
            "FROM " . $GLOBALS['ecs']->table('favourable_activity') .
            " WHERE start_time <= '$now'" .
            " AND end_time >= '$now'" .
            " AND status = 1" .
            " AND CONCAT(',', user_rank, ',') LIKE '%" . $user_rank . "%'" .
            " AND act_type " . db_create_in(array(FAT_DISCOUNT, FAT_PRICE));
    $favourable_list = $GLOBALS['db']->getAll($sql);
    if (!$favourable_list)
    {
        return 0;
    }

    /* 查询购物车商品 */
    $sql = "SELECT c.goods_id, c.goods_price * c.goods_number AS subtotal, g.cat_id, g.brand_id " .
            "FROM " . $GLOBALS['ecs']->table('cart') . " AS c, " . $GLOBALS['ecs']->table('goods') . " AS g " .
            "WHERE c.goods_id = g.goods_id " .
            "AND c.session_id = '" . SESS_ID . "' " .
            "AND c.parent_id = 0 " .
            "AND c.is_gift = 0 " .
            "AND rec_type = '" . CART_GENERAL_GOODS . "'";
    $goods_list = $GLOBALS['db']->getAll($sql);
    if (!$goods_list)
    {
        return 0;
    }

    /* 初始化折扣 */
    $discount = 0;
    $favourable_name = array();

    /* 循环计算每个优惠活动的折扣 */
    foreach ($favourable_list as $favourable)
    {
        $total_amount = 0;
        if ($favourable['act_range'] == FAR_ALL)
        {
            foreach ($goods_list as $goods)
            {
                $total_amount += $goods['subtotal'];
            }
        }
        elseif ($favourable['act_range'] == FAR_CATEGORY)
        {
            /* 找出分类id的子分类id */
            $id_list = array();
            $raw_id_list = explode(',', $favourable['act_range_ext']);
            foreach ($raw_id_list as $id)
            {
                $id_list = array_merge($id_list, array_keys(cat_list($id, 0, false)));
            }
            $ids = join(',', array_unique($id_list));

            foreach ($goods_list as $goods)
            {
                if (strpos(',' . $ids . ',', ',' . $goods['cat_id'] . ',') !== false)
                {
                    $total_amount += $goods['subtotal'];
                }
            }
        }
        elseif ($favourable['act_range'] == FAR_BRAND)
        {
            foreach ($goods_list as $goods)
            {
                if (strpos(',' . $favourable['act_range_ext'] . ',', ',' . $goods['brand_id'] . ',') !== false)
                {
                    $total_amount += $goods['subtotal'];
                }
            }
        }
        elseif ($favourable['act_range'] == FAR_GOODS)
        {
            foreach ($goods_list as $goods)
            {
                if (strpos(',' . $favourable['act_range_ext'] . ',', ',' . $goods['goods_id'] . ',') !== false)
                {
                    $total_amount += $goods['subtotal'];
                }
            }
        }
        else
        {
            continue;
        }
        if ($total_amount > 0 && $total_amount >= $favourable['min_amount'] && ($total_amount <= $favourable['max_amount'] || $favourable['max_amount'] == 0))
        {
            if ($favourable['act_type'] == FAT_DISCOUNT)
            {
                $discount += $total_amount * (1 - $favourable['act_type_ext'] / 100);
            }
            elseif ($favourable['act_type'] == FAT_PRICE)
            {
                $discount += $favourable['act_type_ext'];
            }
        }
    }


    return $discount;
}

/**
 * 添加礼包到购物车
 *
 * @access  public
 * @param   integer $package_id   礼包编号
 * @param   integer $num          礼包数量
 * @return  boolean
 */
function add_package_to_cart($package_id, $spec = array(), $num = 1, $cart_table_name = 'cart')
{
    $packageController = new Yoho\cms\Controller\PackageController();
    $GLOBALS['err']->clean();

    /* 取得礼包信息 */
    $package = $packageController->get_package_info($package_id);
    if (empty($package))
    {
        $GLOBALS['err']->add(_L('global_cart_goods_not_exists', '您選購的產品不存在'), ERR_NOT_EXISTS);

        return false;
    }

    /* 是否正在销售 */
    if ($package['is_on_sale'] == 0)
    {
        $GLOBALS['err']->add(_L('global_cart_goods_not_on_sale', '您選購的產品已下架'), ERR_NOT_ON_SALE);

        return false;
    }


    /* wt's the total goods number in the packages */
    $package_goods_count = 0;
    foreach($package['goods_list'] as $goods){
        $package_goods_count += $goods["goods_number"];
    }

    /* How many same packages in the cart */
    $package_count = 0;

    $sql = "SELECT sum(goods_number) AS cart_package_goods_count FROM ".$GLOBALS['ecs']->table($cart_table_name)." WHERE is_package = '".$package_id."' AND session_id='".SESS_ID."'";
    $cart_package_goods_count = $GLOBALS['db']->getOne($sql);
    if ($cart_package_goods_count > 0)
    {
        $package_count = $cart_package_goods_count / $package_goods_count;
        $package_count = is_int($package_count)?$package_count:floor($package_count);
    }

    /* 现有库存是否还能凑齐一个礼包 */
    $package_count += $num;
    if ($GLOBALS['_CFG']['use_storage'] == '1' && judge_package_stock($package_id))
    {
        addto_cart_error_out_of_stock($package_count, 0);
        return false;
    }

    /* 初始化要插入购物车的基本件数据 */
    $sql = "DELETE FROM ".$GLOBALS['ecs']->table($cart_table_name)." WHERE is_package = '".$package_id."' AND session_id='".SESS_ID."'";
    $GLOBALS['db']->query($sql);

    foreach($package['goods_list'] as $goods){
        $goods_id = $goods["goods_id"];
        $goods_num = $goods["goods_number"] * $package_count;

        $sql = "SELECT *, " . hw_goods_in_stock_sql() . ", " . hw_goods_number_sql() . " FROM (".
            "SELECT g.goods_id, g.goods_name, g.goods_sn, g.is_on_sale, g.is_real, ".
                "g.market_price, g.shop_price AS org_price, g.promote_price, g.promote_start_date, ".
                "g.promote_end_date, g.goods_weight, g.integral, g.extension_code, ".
                "g.is_alone_sale, g.is_shipping, g.is_pre_sale, g.pre_sale_date, g.pre_sale_days, ".
                hw_goods_number_subquery('g.') . ", " .
    			hw_reserved_number_subquery('g.') . ", " .
                "IFNULL(mp.user_price, g.shop_price * '$_SESSION[discount]') AS shop_price ".
            " FROM " .$GLOBALS['ecs']->table('goods'). " AS g ".
            " LEFT JOIN " . $GLOBALS['ecs']->table('member_price') . " AS mp ".
                    "ON mp.goods_id = g.goods_id AND mp.user_rank = '$_SESSION[user_rank]' ".
            " WHERE g.goods_id = '$goods_id'".
            " AND g.is_delete = 0".
            ") as tmp";
        $goods_info = $GLOBALS['db']->getRow($sql);
        //print_r($goods);

        if (is_spec($spec) && !empty($prod))
        {
            $product_info = get_products_info($goods_id, $spec);
        }
        if (empty($product_info))
        {
            $product_info = array('product_number' => '', 'product_id' => 0);
        }

        /* 计算商品的促销价格 */
        $spec_price      = spec_price($spec);
        $final_price     = get_final_price($goods_id, $goods_num, true, $spec);
        $goods_price     = $final_price['final_price'];
        $goods_user_rank =  $final_price['user_rank_id'];
        $goods['market_price'] += $spec_price;
        $goods_attr             = get_goods_attr_info($spec);
        $goods_attr_id          = join(',', $spec);

        $parent = array(
            'user_id'       => $_SESSION['user_id'],
            'session_id'    => SESS_ID,
            'goods_id'      => $goods_id,
            'goods_sn'      => addslashes($goods_info['goods_sn']),
            'product_id'    => $product_info['product_id'],
            'goods_name'    => addslashes($goods_info['goods_name']),
            'market_price'  => $goods_info['market_price'],
            'goods_price'   => $goods['goods_price'],
            'goods_number'  => $goods_num,
            'goods_attr'    => addslashes($goods_attr),
            'goods_attr_id' => $goods_attr_id,
            'is_real'       => $goods_info['is_real'],
            'extension_code'=> 'package',
            'is_gift'       => 0,
            'is_package'    => $package['id'],
            'is_shipping'   => $goods_info['is_shipping'],
            'rec_type'      => CART_GENERAL_GOODS,
            'user_rank'     => $goods_user_rank
        );

        /* 添加 */
        $GLOBALS['db']->autoExecute($GLOBALS['ecs']->table($cart_table_name), $parent, 'INSERT');
    }

    return true;
}

/**
 * 得到新发货单号
 * @return  string
 */
function get_delivery_sn()
{
    /* 选择一个随机的方案 */
    mt_srand((double) microtime() * 1000000);

    return date('YmdHi') . str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT);
}

/**
 * 检查礼包内商品的库存
 * @return  boolen
 */
function judge_package_stock($package_id, $package_num = 1)
{
    $erpController = new Yoho\cms\Controller\ErpController();

    $sql = "SELECT goods_id, product_id, goods_number
            FROM " . $GLOBALS['ecs']->table('package_goods') . "
            WHERE package_id = '" . $package_id . "'";
    $row = $GLOBALS['db']->getAll($sql);

    if (empty($row))
    {
        return true;
    }

    foreach ($row as $value)
    {
        if($value['goods_id']){
            //echo $value['goods_id']."|".$erpController->getSellableProductQty($value['goods_id']) . "|" . ($value['goods_number'] * $package_num)."<br>";
            // $sql = "SELECT goods_number, is_pre_sale, pre_sale_days
            // FROM " . $GLOBALS['ecs']->table('goods') . "
            // WHERE goods_id = '" . $value['goods_id'] . "'";
            // $goods = $GLOBALS['db']->getRow($sql);

            $goods = get_goods_info($value['goods_id']);
            
            if(!(  $goods['goods_number'] >= ($value['goods_number'] * $package_num) || ($goods['is_pre_sale'] != '1' && $goods['pre_sale_days'] > 0) )  ){
                return true;
            }
            /*if($erpController->getSellableProductQty($value['goods_id']) < ($value['goods_number'] * $package_num)){
                return true;
            }*/
        }
    }
    /* 分离货品与商品
    $goods = array('product_ids' => '', 'goods_ids' => '');
    foreach ($row as $value)
    {
        if ($value['product_id'] > 0)
        {
            $goods['product_ids'] .= ',' . $value['product_id'];
            continue;
        }

        $goods['goods_ids'] .= ',' . $value['goods_id'];
    }
     */
    /* 检查货品库存
    if ($goods['product_ids'] != '')
    {
        $sql = "SELECT p.product_id
                FROM " . $GLOBALS['ecs']->table('products') . " AS p, " . $GLOBALS['ecs']->table('package_goods') . " AS pg
                WHERE pg.product_id = p.product_id
                AND pg.package_id = '$package_id'
                AND pg.goods_number * $package_num > p.product_number
                AND p.product_id IN (" . trim($goods['product_ids'], ',') . ")";
        $row = $GLOBALS['db']->getAll($sql);
        die($sql);
        if (!empty($row))
        {
            return true;
        }
    }
    */

    /* 检查商品库存
    if ($goods['goods_ids'] != '')
    {
        $sql = "SELECT g.goods_id
                FROM " . $GLOBALS['ecs']->table('goods') . "AS g, " . $GLOBALS['ecs']->table('package_goods') . " AS pg
                WHERE pg.goods_id = g.goods_id
                AND pg.goods_number * $package_num > g.goods_number
                AND pg.package_id = '" . $package_id . "'
                AND pg.goods_id IN (" . trim($goods['goods_ids'], ',') . ")";
        $row = $GLOBALS['db']->getAll($sql);
        die($sql);
        if (!empty($row))
        {
            return true;
        }
    }
    */

    return false;
}

// howang: Get region name for an order address
function get_address_region_name($province, $city, $district)
{
    $sql = "SELECT region_id, region_name FROM " . $GLOBALS['ecs']->table('region') .
           " WHERE region_id " . db_create_in(array($province, $city, $district));
    $res = $GLOBALS['db']->getAll($sql);
    $arr = array('province' => '', 'city' => '', 'district' => '');
    foreach ($arr as $key => $value)
    {
        foreach ($res as $row)
        {
            if ($row['region_id'] == $$key)
            {
                $arr[$key] = $row['region_name'];
            }
        }
    }
    $arr['ssq'] = $arr['province'].$arr['city'].$arr['district'];
    return $arr;
}

function get_ly_dingdannum($id)  //ly sbsn 订单商品总 数  $id 为订单id
{
    $sql = 'SELECT sum(goods_number)  '.
           'FROM '.$GLOBALS['ecs']->table('order_goods') .
           " WHERE order_id='" . $id . "'";
    $row = $GLOBALS['db']->getOne($sql);
    return $row;
}

function assign_order_print($order_id = 0, $order_sn = '', $delivery_id = 0, $delivery_temp = 0)
{
    global $db,$ecs,$smarty,$_CFG,$_LANG;
    $packageController = new Yoho\cms\Controller\PackageController();

    /* 根据订单id或订单号查询订单信息 */
    $order = order_info($order_id, $order_sn);
    
    /* 如果订单不存在，退出 */
    if (empty($order))
    {
        return;
    }
    
    /* 取得用户名 */
    if ($order['user_id'] > 0)
    {
        $user = user_info($order['user_id']);
        if (!empty($user))
        {
            $order['user_name'] = $user['user_name'];
        }
    }
    
    /* 取得所有办事处 */
    $sql = "SELECT agency_id, agency_name FROM " . $ecs->table('agency');
    $smarty->assign('agency_list', $db->getAll($sql));
    
    /* 取得区域名 */
    $sql = "SELECT concat(IFNULL(c.region_name, ''), '  ', IFNULL(p.region_name, ''), " .
                "'  ', IFNULL(t.region_name, ''), '  ', IFNULL(d.region_name, '')) AS region " .
            "FROM " . $ecs->table('order_info') . " AS o " .
                "LEFT JOIN " . $ecs->table('region') . " AS c ON o.country = c.region_id " .
                "LEFT JOIN " . $ecs->table('region') . " AS p ON o.province = p.region_id " .
                "LEFT JOIN " . $ecs->table('region') . " AS t ON o.city = t.region_id " .
                "LEFT JOIN " . $ecs->table('region') . " AS d ON o.district = d.region_id " .
            "WHERE o.order_id = '$order[order_id]'";
    $order['region'] = $db->getOne($sql);
    
    /* 格式化金额 */
    if ($order['order_amount'] < 0)
    {
        $order['money_refund']          = abs($order['order_amount']);
        $order['formated_money_refund'] = price_format(abs($order['order_amount']));
    }

    if(!empty($order['user_id']))//判断是否存在用户
    {
        $order['huodejifen'] = $db->getOne("SELECT pay_points FROM " .$ecs->table('account_log'). " WHERE change_desc like '%" . mysql_like_quote($order['order_sn']) . "%' and pay_points > 0");//訂單可獲得積分
        $order['xiaofeijifen'] = $db->getOne("SELECT pay_points FROM " .$ecs->table('users'). " WHERE user_id = '$order[user_id]'");//用戶消费積分餘額
        $order['zhanghu'] = $db->getOne("SELECT user_money FROM " .$ecs->table('users'). " WHERE user_id = '$order[user_id]'");//用戶消费積分餘額

        // howang: fix 積分 info from online orders
        if ($order['huodejifen'] === '')
        {
            $integral = integral_to_give($order);
            $order['huodejifen'] = intval($integral['custom_points']);
            $order['xiaofeijifen'] += $order['huodejifen'];
        }
    }
    /* 其他处理 */
    $order['order_time']    = local_date($_CFG['time_format'], $order['add_time']);
    $order['pay_time']      = $order['pay_time'] > 0 ?
        local_date($_CFG['time_format'], $order['pay_time']) : $_LANG['ps'][PS_UNPAYED];
    $order['shipping_time'] = $order['shipping_time'] > 0 ?
        local_date($_CFG['time_format'], $order['shipping_time']) : $_LANG['ss'][SS_UNSHIPPED];
    $order['status']        = $_LANG['os'][$order['order_status']] . ',' . $_LANG['ps'][$order['pay_status']] . ',' . $_LANG['ss'][$order['shipping_status']];
    $order['invoice_no']    = $order['shipping_status'] == SS_UNSHIPPED || $order['shipping_status'] == SS_PREPARING ? $_LANG['ss'][SS_UNSHIPPED] : $order['invoice_no'];
    
    /* 取得订单的来源 */
    if ($order['from_ad'] == 0)
    {
        $order['referer'] = empty($order['referer']) ? $_LANG['from_self_site'] : $order['referer'];
    }
    elseif ($order['from_ad'] == -1)
    {
        $order['referer'] = $_LANG['from_goods_js'] . ' ('.$_LANG['from'] . $order['referer'].')';
    }
    else
    {
        /* 查询广告的名称 */
         $ad_name = $db->getOne("SELECT ad_name FROM " .$ecs->table('ad'). " WHERE ad_id='$order[from_ad]'");
         $order['referer'] = $_LANG['from_ad_js'] . $ad_name . ' ('.$_LANG['from'] . $order['referer'].')';
    }
    
    /* 此订单的发货备注(此订单的最后一条操作记录) */
    $sql = "SELECT action_note FROM " . $ecs->table('order_action').
           " WHERE order_id = '$order[order_id]' AND shipping_status = 1 ORDER BY log_time DESC";
    $order['invoice_note'] = $db->getOne($sql);
    
    /* 取得订单商品总重量 */
    $weight_price = order_weight_price($order['order_id']);
    $order['total_weight'] = $weight_price['formated_weight'];
    
    /* 参数赋值：订单 */
    $smarty->assign('order', $order);
    
    /* 取得用户信息 */
    if ($order['user_id'] > 0)
    {
        /* 用户等级 */
        if ($user['user_rank'] > 0)
        {
            $where = " WHERE rank_id = '$user[user_rank]' ";
        }
        else
        {
            $where = " WHERE min_points <= " . intval($user['rank_points']) . " ORDER BY min_points DESC ";
        }
        $sql = "SELECT rank_name FROM " . $ecs->table('user_rank') . $where;
        $user['rank_name'] = $db->getOne($sql);
        
        // 用户红包数量
        $day    = getdate();
        $today  = local_mktime(23, 59, 59, $day['mon'], $day['mday'], $day['year']);
        $sql = "SELECT COUNT(*) " .
                "FROM " . $ecs->table('bonus_type') . " AS bt, " . $ecs->table('user_bonus') . " AS ub " .
                "WHERE bt.type_id = ub.bonus_type_id " .
                "AND ub.user_id = '$order[user_id]' " .
                "AND ub.order_id = 0 " .
                "AND bt.use_start_date <= '$today' " .
                "AND bt.use_end_date >= '$today'";
        $user['bonus_count'] = $db->getOne($sql);
        $smarty->assign('user', $user);
        
        // 地址信息
        $sql = "SELECT * FROM " . $ecs->table('user_address') . " WHERE user_id = '$order[user_id]'";
        $smarty->assign('address_list', $db->getAll($sql));
    }
    
    /* 取得订单商品及货品 */
    $goods_list = array();
    $goods_attr = array();
    $packages = array();
    
    // howang: calculate market price amount for goods
    $goods_market_amount = 0;
    if($delivery_id > 0 && $delivery_temp == 1) {
        $sql = "SELECT o.*,o.send_number as goods_number, o.goods_attr, g.suppliers_id, wt.template_content, g.seller_note, IFNULL(b.brand_name, '') AS brand_name, IFNULL(c.cat_name, '') AS cat_name, IFNULL(b.only_market_invoice, 0) AS b_omi , IFNULL(c.only_market_invoice, 0) AS c_omi " .
            "FROM " . $ecs->table('delivery_goods') . " AS o ".
                "LEFT JOIN " . $ecs->table('goods') . " AS g
                    ON o.goods_id = g.goods_id
                LEFT JOIN " . $ecs->table('brand') . " AS b
                    ON g.brand_id = b.brand_id
                LEFT JOIN " . $ecs->table('category') . " AS c
                    ON g.cat_id = c.cat_id
                LEFT JOIN " . $ecs->table('warranty_templates') . " AS wt
                    ON g.warranty_template_id = wt.template_id
            WHERE o.delivery_id = '$delivery_id'";
    } else {
        $sql = "SELECT o.*, o.rec_id as order_item_id, o.goods_attr, o.extension_code, o.is_package, g.suppliers_id, wt.template_content, g.seller_note, IFNULL(b.brand_name, '') AS brand_name, IFNULL(c.cat_name, '') AS cat_name, IFNULL(b.only_market_invoice, 0) AS b_omi , IFNULL(c.only_market_invoice, 0) AS c_omi " .
            "FROM " . $ecs->table('order_goods') . " AS o ".
                "LEFT JOIN " . $ecs->table('goods') . " AS g
                    ON o.goods_id = g.goods_id
                LEFT JOIN " . $ecs->table('brand') . " AS b
                    ON g.brand_id = b.brand_id
                LEFT JOIN " . $ecs->table('category') . " AS c
                    ON g.cat_id = c.cat_id
                LEFT JOIN " . $ecs->table('warranty_templates') . " AS wt
                    ON g.warranty_template_id = wt.template_id
            WHERE o.order_id = '$order[order_id]'";
        if($delivery_id > 0 && $delivery_temp == 2) {
            $dsql = "SELECT o.goods_id,o.send_number as goods_number, o.order_item_id as order_item_id " .
            "FROM " . $ecs->table('delivery_goods') . " AS o ".
                "LEFT JOIN " . $ecs->table('goods') . " AS g
                    ON o.goods_id = g.goods_id
            WHERE o.delivery_id = '$delivery_id'";

            $result = $db->getAll($dsql);
            $delivery_goods_list = [];
            foreach ($result as $k => $d_goods) {
                $delivery_goods_list[$d_goods['order_item_id']] = $d_goods;
            }
        }
    }

    $res = $db->query($sql);
    while ($row = $db->fetchRow($res))
    {
        /* 虚拟商品支持 */
        if ($row['is_real'] == 0)
        {
            /* 取得语言项 */
            $filename = ROOT_PATH . 'plugins/' . $row['extension_code'] . '/languages/common_' . $_CFG['lang'] . '.php';
            if (file_exists($filename))
            {
                include_once($filename);
                if (!empty($_LANG[$row['extension_code'].'_link']))
                {
                    $row['goods_name'] = $row['goods_name'] . sprintf($_LANG[$row['extension_code'].'_link'], $row['goods_id'], $order['order_sn']);
                }
            }
        }
        if($row['b_omi'] || $row['c_omi']){
            $order['only_market_invoice'] = true;
        }
        $row['formated_subtotal']        = price_format($row['goods_price'] * $row['goods_number']);
        $row['formated_goods_price']     = price_format($row['goods_price']);
        
        // howang: add market price
        $row['formated_market_subtotal'] = price_format($row['market_price'] * $row['goods_number']);
        $row['formated_market_price']    = price_format($row['market_price']);
        $goods_market_amount += $row['market_price'] * $row['goods_number'];
        
        $agency_id = $order['agency_id'];
        
        $row['storage'] = get_goods_stock($row['goods_id'], $row['goods_attr_id'], $agency_id);
        
        $goods_attr[] = explode(' ', trim($row['goods_attr'])); //将商品属性拆分为一个数组
        
        if ($row['extension_code'] == 'package')
        {
            if (!in_array($row['is_package'], array_keys($packages))) {
                $packages[$row['is_package']] = $packageController->get_basic_package_info($row['is_package']);
                $packages[$row['is_package']]['qty'] = $row['goods_number'] / $packages[$row['is_package']]['items'][$row['goods_id']]['goods_number'];
                $packages[$row['is_package']]['total_price'] = bcmul($packages[$row['is_package']]['qty'], $packages[$row['is_package']]['base_price'], 2);
                $packages[$row['is_package']]['formated_total_price'] = price_format($packages[$row['is_package']]['total_price'], false);
                $packages[$row['is_package']]['rec_id'] = $row['rec_id'];
            }
            $packages[$row['is_package']]['items'][$row['goods_id']]['qty'] = $row['goods_number'];
            $packages[$row['is_package']]['items'][$row['goods_id']]['seller_note'] = $row['seller_note'];
            $packages[$row['is_package']]['items'][$row['goods_id']]['template_content'] = $row['template_content'];
            $row['package_goods_list'] = $packages[$row['is_package']]['items'];
        }
        
        //欠货数量（订单的数量减去已发货的数量，就是欠客户的数量）
        $row['lack_qty'] = $row['goods_number'] > $row['send_number'] ? $row['goods_number'] - $row['send_number'] : 0;
        if(isset($delivery_goods_list[$row['order_item_id']])) {
            $row['is_this'] = '';
            $d_num = $delivery_goods_list[$row['order_item_id']]['goods_number'];
            $goods_num = $row['goods_number'];
            if($d_num != $goods_num) {
                $row['goods_number'] = "<s>$goods_num</s> $d_num";
                if ($row['extension_code'] == 'package') {
                    $packages[$row['is_package']]['items'][$row['goods_id']]['qty'] = $row['goods_number'];
                }
            }
        } else if ($delivery_temp == 2) {
            $row['goods_name'] = "<s>$row[goods_name]</s>";
            $row['goods_number'] = "<s>$row[goods_number]</s>";
            if ($row['extension_code'] == 'package') {
                $packages[$row['is_package']]['items'][$row['goods_id']]['qty'] = $row['goods_number'];
                $packages[$row['is_package']]['items'][$row['goods_id']]['goods_name'] = $row['goods_name'];
            }
        }

        if ($row['is_insurance'] == 1 && $row['insurance_of'] > 0){
            $insurance_of_sn = $GLOBALS['db']->getOne("SELECT goods_sn FROM ".$GLOBALS['ecs']->table('goods')." WHERE goods_id = '".$row['insurance_of']."' AND goods_id = '".$row['insurance_of']."' AND is_insurance = 0 ");
            if (!empty($insurance_of_sn)){
                $row['insurance_of_sn'] = $insurance_of_sn;
            }
        }

        // var_dump($row);
        $goods_list[] = $row;
    }
    if($delivery_temp == 1) {
        $order['is_purchase_order'] = true;
    } else if ($delivery_temp == 2) {
        $order['is_delivery_order'] = true;
    }

    // howang: add market price to order
    //$order_market_amount = $order['order_amount'] - $order['goods_amount'] + $goods_market_amount;
    $order_market_amount = $goods_market_amount;
    $order['order_market_amount'] = $order_market_amount;
    $order['formated_order_market_amount'] = price_format($order_market_amount, false);
    
    // howang: update salesperson to display name
    $sql = "SELECT `display_name` FROM " . $ecs->table('salesperson') . " WHERE `sales_name` = '" . $order['salesperson'] . "'";
    $order['salesperson'] = $db->getOne($sql);
    
    // // howang: as some payment method offer discount, here we apply it to the discount field for display
    // if ($order['pay_fee'] < 0)
    // {
    //     $order['discount'] -= $order['pay_fee'];
    //     $order['formated_discount'] = price_format($order['discount'], false);
    // }
    // howang: we changed the way we do this, now we have a new entry for payment discount
    if ($order['pay_fee'] < 0)
    {
        $order['payment_discount'] = -$order['pay_fee'];
        $order['formated_payment_discount'] = price_format($order['payment_discount'], false);
    }

    // get delivery order sn
    
   
    // add barcode invoice no
    require_once(ROOT_PATH . 'includes/php-barcode-generator/src/BarcodeGenerator.php');
    require_once(ROOT_PATH . 'includes/php-barcode-generator/src/BarcodeGeneratorJPG.php'); 
    $generator = new \Picqer\Barcode\BarcodeGeneratorJPG();
    $order['barcode_invoice_id'] = '<img  src="data:image/jpeg;base64,' . base64_encode($generator->getBarcode($order['order_sn'], $generator::TYPE_CODE_128,1,14)) . '">';

    if($delivery_id > 0 && $delivery_temp == 1) {
        $sql = "SELECT `delivery_sn` FROM " . $ecs->table('delivery_order') . " WHERE `delivery_id` = '" .$delivery_id . "'";
        $delivery_sn = $db->getOne($sql);

        $order['barcode_delivery_sn'] = '<img  src="data:image/jpeg;base64,' . base64_encode($generator->getBarcode($delivery_sn, $generator::TYPE_CODE_128,2,25)) . '">';
        $order['delivery_sn'] = $delivery_sn;
    }
    $tel = !empty($order['tel']) ? $order['tel'] : $order['mobile'];
    $tel = explode('-', $tel);
    $tel = preg_replace('/\s(?=)/', '', $tel[count($tel) - 1]);

    $order['dn_mobile'] = $tel;

    $order['is_reserve'] = false;
    if  ( 
            ((strpos($order['postscript'], '訂貨') !== false || strpos($order['order_remarks'], '訂貨') !== false) && $order['shipping_status'] == SS_UNSHIPPED) ||
            ($order['shipping_status'] == SS_UNSHIPPED && $order['platform'] == 'pos' && empty($order['order_type']))
        ) {
        $order['is_reserve'] = true;
    }

    // Re-assign order to smarty as we modified it
    $smarty->assign('order', $order);
    
    $attr = array();
    $arr  = array();
    foreach ($goods_attr AS $index => $array_val)
    {
        foreach ($array_val AS $value)
        {
            $arr = explode(':', $value);//以 : 号将属性拆开
            $attr[$index][] =  @array('name' => $arr[0], 'value' => $arr[1]);
        }
    }
    
    $smarty->assign('goods_attr', $attr);
    $smarty->assign('goods_list', $goods_list);
    $smarty->assign('packages', $packages);
    
    /* 取得是否存在实体商品 */
    $smarty->assign('exist_real_goods', exist_real_goods($order['order_id']));
    
    /* 打印订单赋值 */
    $smarty->assign('shop_name',    $_CFG['shop_name']);
    $smarty->assign('shop_url',     $ecs->url());
    $smarty->assign('shop_address', $_CFG['shop_address']);
    $smarty->assign('service_phone',$_CFG['service_phone']);
    $smarty->assign('print_time',   local_date($_CFG['time_format']));
    
    // Add testing message if invoking from beta website
    $smarty->assign('test_order',   substr($_SERVER['HTTP_HOST'],0,5) == 'beta.' ? '* 測試訂單 *' : '');
    
    // header('Content-Type: text/plain;charset=utf8');
    // print_r($order);
    // print_r($goods_list);
    // exit;
}

// ref: http://stackoverflow.com/a/535040/613541
function rutime($ru, $rus, $index)
{
    return ($ru["ru_$index.tv_sec"]*1000 + intval($ru["ru_$index.tv_usec"]/1000)) - ($rus["ru_$index.tv_sec"]*1000 + intval($rus["ru_$index.tv_usec"]/1000));
}

function generate_receipt_pdf($html)
{
    ini_set('display_errors', 0);
    ini_set('memory_limit', '512M');
    set_time_limit(0);
    
    // Record start time
    $rustart = getrusage();
    $tstart = time()+microtime();
    
    require_once ROOT_PATH . 'includes/dompdf/dompdf_config.inc.php';
    
    $dompdf = new DOMPDF();
    $dompdf->set_base_path(ROOT_PATH);
    $dompdf->set_option('dpi', 300);
    $dompdf->load_html($html);
    $dompdf->set_paper(array(0,0,2100,1480));
    
    $dompdf->render();
    
    //$dompdf->stream("receipt.pdf", array("Attachment" => false));
    $output = $dompdf->output();
    
    // Log time used
    $ru = getrusage();
    $t = time()+microtime();
    hw_error_log(__FUNCTION__ . ': time = ' . ($t-$tstart) . ',utime = ' . rutime($ru, $rustart, 'utime') . ', stime = ' . rutime($ru, $rustart, 'stime') . ', memory_get_peak_usage = ' . memory_get_peak_usage());
    
    return $output;
}

// howang: Get 訂貨 status for goods in cart
function check_goods_preorder($cart_goods)
{
    $result = array();
    foreach ($cart_goods as $goods)
    {
        $sql = "SELECT g.is_purchasing, g.is_pre_sale, g.pre_sale_date, g.pre_sale_days, g.pre_sale_time, ".
                hw_goods_number_subquery('g.', 'real_goods_number') . ", " .
    			hw_reserved_number_subquery('g.') . " " .
                "FROM " .$GLOBALS['ecs']->table('goods'). " AS g ".
                "WHERE g.goods_id = '" . $goods['goods_id'] . "'";
        $row = $GLOBALS['db']->getRow($sql);
        // Quantity available for purchasing
        $row['goods_number'] = intval($row['real_goods_number']) - intval($row['reserved_number']);
        
        // YOHO: limit quantity to 20 a time
        $row['goods_number'] = min(intval($row['goods_number']), 20);
        
        $is_preorder = false;
        $preorder_mode = '';
        $orderController = new Yoho\cms\Controller\OrderController();
        $today = $orderController->get_today_info();
        // 沒有存貨時，自動判定訂貨模式
        if ($row['goods_number'] < $goods['goods_number'])
        {
            //echo 555;
            if ($row['is_pre_sale'] && $row['goods_number'] <= 0) // 預售
            {
                // 預售不能購買
            }
            elseif ($row['is_purchasing'] && $row['pre_sale_days'] > 0 && $row['goods_number'] <= 0) // 外國代購
            {
                $is_preorder = true;
                $preorder_mode = '外國代購';
                $row['pre_sale_date'] = $orderController->get_yoho_next_work_day(gmtime(), $row['pre_sale_days']);
                $row['planned_delivery_date'] = $orderController->get_yoho_next_work_day(gmtime(), $row['pre_sale_days']);
            }
            elseif ($row['pre_sale_date'] > gmtime()) // 即將到貨
            {
                $is_preorder = true;
                $preorder_mode = '即將到貨';
                $row['planned_delivery_date'] = $orderController->get_yoho_next_work_day($row['pre_sale_date']);
                $row['pre_sale_days'] = $orderController->count_yoho_work_day($row['pre_sale_date']);
            }
            elseif ($row['pre_sale_days'] > 0) // 訂貨
            {
                $is_preorder = true;
                $preorder_mode = '訂貨';
                $row['pre_sale_date'] = local_strtotime('+' . $row['pre_sale_days'] . ' day');
                $row['planned_delivery_date'] = $orderController->get_yoho_next_work_day(gmtime(), $row['pre_sale_days']);
            }
            else // 缺貨
            {
                // 缺貨不能購買
            }
        }

        if ($is_preorder)
        {
            $package_name = "";
            if ($goods['is_package']) {
                $package_name = $GLOBALS['db']->getOne("SELECT act_name FROM " . $GLOBALS['ecs']->table('goods_activity') . " WHERE act_id = '$goods[is_package]' AND act_type = " . GAT_PACKAGE);;
            }
            $result[] = array(
                'goods_id' => $goods['goods_id'],
                'goods_sn' => $goods['goods_sn'],
                'goods_name' => (empty($package_name) ? "" : "$package_name - ") . $goods['goods_name'],
                'preorder_mode' => $preorder_mode,
                'pre_sale_days' => $row['pre_sale_days'],
                'pre_sale_date' => $row['pre_sale_date'],
                'pre_sale_date_formatted' => local_date('Y年m月d日', $row['pre_sale_date']),
                'planned_delivery_date' => $row['planned_delivery_date'],
                'planned_delivery_date_formatted' => local_date('Y年m月d日', $row['planned_delivery_date']),
            );
        }
    }
    return $result;
}

function get_repair_sn()
{
    static $cnt = 0;
    static $len = 2; // Default 2 random digits
    if (++$cnt > 3) // If we retry for more than 3 times, increase number of random digits
    {
        $len = $len + 1;
        $cnt = 0;
    }
    
    /* 选择一个随机的方案 */
    mt_srand((double) microtime() * 1000000);

    return 'R' . date('Ymd') . str_pad(mt_rand(1, pow(10, $len) - 1), $len, '0', STR_PAD_LEFT);
}

// howang: Check if affiliate is eligible for any goods in cart
function affiliate_eligible_for_order($affiliate_user_id, $order, $cart_goods)
{
    $time = empty($order['add_time']) ? 0 : intval($order['add_time']);
    $time = empty($time) ? gmtime() : $time;
    
    $goods_id_list = array_map(function ($goods) { return $goods['goods_id']; }, $cart_goods);
    
    $sql = "SELECT '1' " .
            "FROM " . $GLOBALS['ecs']->table('affiliate_rewards_detail') . " as ard " .
            "LEFT JOIN" . $GLOBALS['ecs']->table('affiliate_rewards') . " as ar ON ar.reward_id = ard.reward_id " .
            "WHERE ar.user_id = '" . intval($affiliate_user_id) . "' ".
            "AND (" . $time . " BETWEEN ar.start_time AND ar.end_time) " .
            "AND ((" .
                "ard.type = 'all'" .
            ") OR (" .
                "ard.type = 'goods' AND ard.related_id " . db_create_in($goods_id_list) .
            ") OR (" .
                "ard.type = 'category' AND ard.related_id IN (" .
                    "SELECT cat_id " .
                    "FROM " . $GLOBALS['ecs']->table('goods') .
                    "WHERE goods_id " . db_create_in($goods_id_list) .
                ")" .
            "))" .
            "LIMIT 1";
    return ($GLOBALS['db']->getOne($sql)) ? true : false;
}

// get  pos sales agent
function get_pos_sales_agent(){
	$salesagentController = new Yoho\cms\Controller\SalesagentController();
	$salesagents = $salesagentController->get_active_sales_agents();

	return $salesagents;
}

// remove  order goods rate
function remove_sa_rate_id($order_id){
	if(!isset($order_id)) return false;

	$sql = "UPDATE " . $GLOBALS['ecs']->table('order_goods') . " SET sa_rate_id = 0" .
                       " WHERE order_id = '".$order_id."' ";
     $GLOBALS['db']->query($sql);

}

?>
