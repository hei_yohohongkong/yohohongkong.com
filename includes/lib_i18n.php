<?php

/***
 * lib_i18n.php
 * by howang 2015-07-31
 *
 * Internationalization related functions
 ***/

if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

// Get user's location
function user_area()
{
    static $area = null;
        
    if ($area != null)
    {
        return $area;
    }
    
    $area = ecs_geoip(real_ip());
    
    return $area;
}

// Define list of countries allowed to login / register
function available_login_countries()
{
    return array('hk', 'mo', 'tw', 'cn', 'us', 'ca', 'gb', 'fr', 'au', 'nz', 'kr', 'jp', 'sg', 'my', 'th', 'id', 'in', 'de', 'br', 'ie', 'tr', 'ch', 'fi', 'nl', 'se', 'dk', 'no', 'be', 'at', 'it', 'lu', 'ru', 'pl', 'es');
}
function available_register_countries()
{
    return available_login_countries();
}

// Get Country short code from region id
function get_country_code_by_region($region_id)
{
	switch ($region_id)
	{
		case 3409: return 'hk';
		case 3410: return 'mo';
		case 3411: return 'tw';
		case 3436: return 'cn';
		case 3437: return 'us';
		case 3438: return 'ca';
		case 3439: return 'gb';
		case 3440: return 'fr';
		case 3441: return 'au';
		case 3442: return 'nz';
		case 3443: return 'kr';
		case 3444: return 'jp';
		case 3445: return 'sg';
		case 3446: return 'my';
		case 3447: return 'th';
		case 3448: return 'id';
		case 3449: return 'in';
		case 3450: return 'de';
		case 3451: return 'br';
		case 3452: return 'ie';
		case 3453: return 'tr';
		case 3454: return 'ch';
		case 3455: return 'fi';
		case 3456: return 'nl';
		case 3457: return 'se';
		case 3458: return 'dk';
		case 3459: return 'no';
		case 3460: return 'be';
		case 3461: return 'at';
		case 3462: return 'it';
		case 3463: return 'lu';
		case 3464: return 'ru';
		case 3465: return 'pl';
		case 3466: return 'es';
		case 3467: return 'ph';
		case 3468: return 'vn';
		default: return false;
	}
}
function get_region_by_country_code($country_code)
{
	switch ($country_code)
	{
		case 'hk': return 3409;
		case 'mo': return 3410;
		case 'tw': return 3411;
		case 'cn': return 3436;
		case 'us': return 3437;
		case 'ca': return 3438;
		case 'gb': return 3439;
		case 'fr': return 3440;
		case 'au': return 3441;
		case 'nz': return 3442;
		case 'kr': return 3443;
		case 'jp': return 3444;
		case 'sg': return 3445;
		case 'my': return 3446;
		case 'th': return 3447;
		case 'id': return 3448;
		case 'in': return 3449;
		case 'de': return 3450;
		case 'br': return 3451;
		case 'ie': return 3452;
		case 'tr': return 3453;
		case 'ch': return 3454;
		case 'fi': return 3455;
		case 'nl': return 3456;
		case 'se': return 3457;
		case 'dk': return 3458;
		case 'no': return 3459;
		case 'be': return 3460;
		case 'at': return 3461;
		case 'it': return 3462;
		case 'lu': return 3463;
		case 'ru': return 3464;
		case 'pl': return 3465;
		case 'es': return 3466;
		case 'ph': return 3467;
		case 'vn': return 3468;
		default: return 3409;
	}
}

// howang: Return hard-coded list of countries that we ship to
// TODO: Rewrite remove hard-code address in future
function available_countries()
{
    return array(
		array('id' => 3441, 'name' => _L('country_3441', '澳洲'), 'default' => false),
        array('id' => 3461, 'name' => _L('country_3461', '奧地利'), 'default' => false),
        array('id' => 3460, 'name' => _L('country_3460', '比利時'), 'default' => false),
        array('id' => 3451, 'name' => _L('country_3451', '巴西'), 'default' => false),
        array('id' => 3438, 'name' => _L('country_3438', '加拿大'), 'default' => false),
        array('id' => 3436, 'name' => _L('country_3436', '中國'), 'default' => false),
        array('id' => 3458, 'name' => _L('country_3458', '丹麥'), 'default' => false),
        array('id' => 3455, 'name' => _L('country_3455', '芬蘭'), 'default' => false),
        array('id' => 3440, 'name' => _L('country_3440', '法國'), 'default' => false),
        array('id' => 3450, 'name' => _L('country_3450', '德國'), 'default' => false),
        array('id' => 3409, 'name' => _L('country_3409', '香港'), 'default' => true),
        array('id' => 3449, 'name' => _L('country_3449', '印度'), 'default' => false),
        array('id' => 3448, 'name' => _L('country_3448', '印尼'), 'default' => false),
        array('id' => 3452, 'name' => _L('country_3452', '愛爾蘭'), 'default' => false),
        array('id' => 3462, 'name' => _L('country_3462', '意大利'), 'default' => false),
        array('id' => 3444, 'name' => _L('country_3444', '日本'), 'default' => false),
        array('id' => 3463, 'name' => _L('country_3463', '盧森堡'), 'default' => false),
        array('id' => 3410, 'name' => _L('country_3410', '澳門'), 'default' => false),
        array('id' => 3446, 'name' => _L('country_3446', '馬來西亞'), 'default' => false),
        array('id' => 3456, 'name' => _L('country_3456', '荷蘭'), 'default' => false),
        array('id' => 3442, 'name' => _L('country_3442', '紐西蘭'), 'default' => false),
        array('id' => 3459, 'name' => _L('country_3459', '挪威'), 'default' => false),
        array('id' => 3465, 'name' => _L('country_3465', '波蘭'), 'default' => false),
        array('id' => 3464, 'name' => _L('country_3464', '俄羅斯'), 'default' => false),
        array('id' => 3445, 'name' => _L('country_3445', '新加坡'), 'default' => false),
        array('id' => 3443, 'name' => _L('country_3443', '韓國'), 'default' => false),
        array('id' => 3466, 'name' => _L('country_3466', '西班牙'), 'default' => false),
        array('id' => 3457, 'name' => _L('country_3457', '瑞典'), 'default' => false),
        array('id' => 3454, 'name' => _L('country_3454', '瑞士'), 'default' => false),
        array('id' => 3411, 'name' => _L('country_3411', '台灣'), 'default' => false),
        array('id' => 3447, 'name' => _L('country_3447', '泰國'), 'default' => false),
        array('id' => 3453, 'name' => _L('country_3453', '土耳其'), 'default' => false),
        array('id' => 3439, 'name' => _L('country_3439', '英國'), 'default' => false),
        array('id' => 3437, 'name' => _L('country_3437', '美國'), 'default' => false),
	);
}

function available_countries_code()
{
	$list = available_countries();

	$res = [];
	foreach ($list as $key => $country) {
		$res[get_country_code_by_region($country['id'])] = $country['id'];
	}

	return $res;
}

function get_shipping_available_countries($shipping_id)
{
    $sql = "SELECT * FROM " . $GLOBALS['ecs']->table('shipping_area');
    if ($shipping_id > 0)
    {
        $sql .= " WHERE shipping_id = '$shipping_id'";
    }
    $res = $GLOBALS['db']->query($sql);
    $list = array();
    while ($row = $GLOBALS['db']->fetchRow($res))
    {
        $sql = "SELECT r.region_name, a.region_id " .
                "FROM " . $GLOBALS['ecs']->table('area_region'). " AS a, " .
                    $GLOBALS['ecs']->table('region') . " AS r ".
                "WHERE a.region_id = r.region_id ".
                "AND a.shipping_area_id = '$row[shipping_area_id]'";
        $regions = $GLOBALS['db']->getRow($sql);

        $list[] = $regions;
    }
    
    $arr = array();
    foreach ($list as $key => $value) {
		// Hard code : Hong Kong(3409) default true
        $arr[$key] = array('id' => $value['region_id'], 'name' => $value['region_name'], 'default' => $value['region_id'] == 3409 ? true : false);
    }
    return array_unique($arr, SORT_REGULAR);
}

// Get default language
function default_language()
{
	global $_CFG;
	return empty($_CFG['default_lang']) ? 'zh_tw' : $_CFG['default_lang'];
}

// Get list of available languages
function available_languages()
{
	return array('zh_tw', 'zh_cn', 'en_us');
}

// Get list of available language names
function available_language_names()
{
	$language_names = array(
		'zh_tw' => '繁體中文',
		'zh_cn' => '简体中文',
		'en_us' => 'English'
	);
	
	$available_langs = available_languages();
	
	foreach ($language_names as $language_code => $language_name)
	{
		if (!in_array($language_code, $available_langs))
		{
			unset($language_names[$language_code]);
		}
	}
	return $language_names;
}

function available_language_names_abbreviation()
{
	$language_names = array(
		'zh_tw' => '繁',
		'zh_cn' => '简',
		'en_us' => 'ENG'
	);
	
	$available_langs = available_languages();
	
	foreach ($language_names as $language_code => $language_name)
	{
		if (!in_array($language_code, $available_langs))
		{
			unset($language_names[$language_code]);
		}
	}
	return $language_names;
}

// Get user language base on Cookie or Accept-Language header
function user_language()
{
	$available_langs = available_languages();
	
	if (!empty($_GET['language']))
	{
		$get_lang = $_GET['language'];
		if (in_array($get_lang, $available_langs))
		{
			set_language_cookie($get_lang);
			return $get_lang;
		}
	}
	
	if (!empty($_COOKIE['yoho_language']))
	{
		$cookie_lang = $_COOKIE['yoho_language'];
		if (in_array($cookie_lang, $available_langs))
		{
			return $cookie_lang;
		}
	}
	
	$langs = array();
	if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']))
	{
		// break up string into pieces (languages and q factors)
		preg_match_all('/([a-z]{1,8}(-[a-z]{1,8})?)\s*(;\s*q\s*=\s*(1|0\.[0-9]+))?/i', $_SERVER['HTTP_ACCEPT_LANGUAGE'], $lang_parse);

		if (!empty($lang_parse[1]))
		{
			$langs = array_combine($lang_parse[1], $lang_parse[4]);
			
			foreach ($langs as $lang => $val)
			{
				if ($val === '')
				{
					$langs[$lang] = 1;
				}
				elseif ($val <= 0)
				{
					unset($langs[$lang]);
				}
			}
		}
	}
	
	$user_lang = default_language();
	$max_val = 0;
	foreach ($langs as $lang => $val)
	{
		if ($val > $max_val)
		{
			$lang = strtolower(str_replace('-', '_', $lang));
			if ((strpos($lang, 'zh_tw') !== false) ||
				(strpos($lang, 'zh_hant') !== false))
			{
				$lang = 'zh_tw';
			}
			elseif ((strpos($lang, 'zh_cn') !== false) ||
					(strpos($lang, 'zh_hans') !== false))
			{
				$lang = 'zh_cn';
				//$lang = 'zh_tw';
			}
			elseif (strpos($lang, 'zh') !== false)
			{
				$lang = 'zh_tw';
			}
			elseif (strpos($lang, 'en') !== false)
			{
				$lang = 'en_us';
			}
			
			if (in_array($lang, $available_langs))
			{
				$user_lang = $lang;
				$max_val = $val;
			}
		}
	}
	
	return $user_lang;
}

// Call after login and before update_user_info to restore user's language preference
function restore_user_saved_language()
{
	global $_CFG, $db, $ecs, $_LANG;
	$saved_lang = $db->getOne("SELECT `language` FROM " . $ecs->table('users') . " WHERE `user_id` = '" . $_SESSION['user_id'] . "'");
	if (($saved_lang != $_CFG['lang']) && (in_array($saved_lang, available_languages())))
	{
		// switch to user's saved language
		$_CFG['lang'] = $saved_lang;
		// load new language file
		include ROOT_PATH . 'languages/' . $_CFG['lang'] . '/yoho.php';
		// set language cookie for user
		set_language_cookie($saved_lang);
	}
}

function set_language_cookie($value)
{
	$integrate_config = unserialize($GLOBALS['_CFG']['integrate_config']);
	$cookie_domain = empty($integrate_config['cookie_domain']) ? $GLOBALS['ecs']->get_host() : $integrate_config['cookie_domain'];
	$cookie_path = empty($integrate_config['cookie_path']) ? '/' : $integrate_config['cookie_path'];
	$expire = time() + 2592000; // 30 days
	setcookie('yoho_language', $value, $expire, $cookie_path, $cookie_domain);
}

// Get localized string
function _L($key, $fallback = '')
{
	global $_LANG;
	return (isset($_LANG[$key])) ? $_LANG[$key] : $fallback;
}

// Localization table information
function localization_table_info($table)
{
	if ($table == 'brand')
	{
		$primary_key = 'brand_id';
		$lang_table = 'brand_lang';
	}
	elseif ($table == 'category')
	{
		$primary_key = 'cat_id';
		$lang_table = 'category_lang';
	}
	elseif ($table == 'goods')
	{
		$primary_key = 'goods_id';
		$lang_table = 'goods_lang';
	}
	elseif ($table == 'payment')
	{
		$primary_key = 'pay_id';
		$lang_table = 'payment_lang';
	}
	elseif ($table == 'shipping')
	{
		$primary_key = 'shipping_id';
		$lang_table = 'shipping_lang';
	}
	elseif ($table == 'shipping_area')
	{
		$primary_key = 'shipping_area_id';
		$lang_table = 'shipping_area_lang';
	}
	elseif ($table == 'topic')
	{
		$primary_key = 'topic_id';
		$lang_table = 'topic_lang';
	}
	elseif ($table == 'topic_category')
	{
		$primary_key = 'topic_cat_id';
		$lang_table = 'topic_category_lang';
	}
	elseif ($table == 'article')
	{
		$primary_key = 'article_id';
		$lang_table = 'article_lang';
	}
	elseif ($table == 'article_cat')
	{
		$primary_key = 'cat_id';
		$lang_table = 'article_cat_lang';
	}
	elseif ($table == 'nav')
	{
		$primary_key = 'id';
		$lang_table = 'nav_lang';
	}
	elseif ($table == 'mail_templates')
	{
		$primary_key = 'template_id';
		$lang_table = 'mail_templates_lang';
	}
	elseif ($table == 'warranty_templates')
	{
		$primary_key = 'template_id';
		$lang_table = 'warranty_templates_lang';
	}
    elseif ($table == 'attribute')
	{
		$primary_key = 'attr_id';
		$lang_table = 'attribute_lang';
	}
    elseif ($table == 'goods_attr')
	{
		$primary_key = 'goods_attr_id';
		$lang_table = 'goods_attr_lang';
	}
	elseif ($table == 'logistics')
	{
		$primary_key = 'logistics_id';
		$lang_table = 'logistics_lang';
	}
	elseif ($table == 'shipping_templates')
	{
		$primary_key = 'template_id';
		$lang_table = 'shipping_templates_lang';
	}
	elseif ($table == 'user_rank_program')
	{
		$primary_key = 'program_id';
		$lang_table = 'user_rank_program_lang';
	}
	elseif ($table == 'shipping_type')
	{
		$primary_key = 'shipping_type_id';
		$lang_table = 'shipping_type_lang';
	}
	elseif ($table == 'global_alert')
	{
		$primary_key = 'alert_id';
		$lang_table = 'global_alert_lang';
	}
	elseif ($table == 'tag')
	{
		$primary_key = 'tag_id';
		$lang_table = 'tag_lang';
	}
	elseif ($table == 'coupon_promote')
	{
		$primary_key = 'promote_id';
		$lang_table = 'coupon_promote_lang';
	}
	elseif ($table == 'favourable_activity')
	{
		$primary_key = 'act_id';
		$lang_table = 'favourable_activity_lang';
	}
	elseif ($table == 'goods_activity')
	{
		$primary_key = 'act_id';
		$lang_table = 'goods_activity_lang';
	}
	else
	{
		return false;
	}
	return compact('primary_key', 'lang_table');
}

// Get localized content from database
function localize_db_result($table, $data, $fields_mapping = array())
{
	global $db, $ecs, $_CFG;
	
	$table_info = localization_table_info($table);
	if ($table_info !== false)
	{
		extract($table_info);
	}
	else // Unknown table
	{
		return $data;
	}
	
	$data_primary_key = $primary_key;
	foreach ($fields_mapping as $src => $target)
	{
		if ($target == $primary_key)
		{
			$data_primary_key = $src;
		}
	}
	
	$ids = array();
	foreach ($data as $row)
	{
		$ids[] = $row[$data_primary_key];
	}
	$sql = "SELECT * " .
			"FROM " . $ecs->table($lang_table) . " " .
			"WHERE `" . $primary_key . "` " . db_create_in($ids) .
			"AND `lang` = '" . $_CFG['lang'] . "'";
	$res = $db->getAll($sql);
	foreach ($data as $data_key => $row)
	{
		foreach ($res as $r)
		{
			if ($r[$primary_key] == $row[$data_primary_key])
			{
				foreach ($r as $key => $value)
				{
					if ($key == $primary_key || $key == 'lang' || is_null($value)) continue;
					$row_key = array_search($key, $fields_mapping);
					if (!$row_key) $row_key = $key;
					if (isset($row[$row_key]))
					{
						$row[$row_key] = $r[$key];
					}
				}
				$data[$data_key] = $row;
				break;
			}
		}
	}
	return $data;
}

function localize_db_row($table, $data, $fields_mapping = array())
{
	$data = localize_db_result($table, array($data), $fields_mapping);
	return array_pop($data);
}

function localize_db_result_lang($lang, $table, $data, $fields_mapping = array())
{
	global $_CFG;
	$old_lang = $_CFG['lang'];
	$_CFG['lang'] = $lang;
	$data = localize_db_result($table, $data, $fields_mapping);
	$_CFG['lang'] = $old_lang;
	return $data;
}

function localizable_fields_for_table($table)
{
	global $db, $ecs;
	
	$table_info = localization_table_info($table);
	if ($table_info !== false)
	{
		extract($table_info);
	}
	else
	{
		return array();
	}
	
	$sql = "DESC " . $ecs->table($lang_table);
	$res = $db->getAll($sql);
	$fields = array();
	foreach ($res as $row)
	{
		if ($row['Field'] == $primary_key || $row['Field'] == 'lang') continue;
		$fields[] = $row['Field'];
	}
	return $fields;
}

function get_localized_versions($table, $primary_id, $localizable_fields = array())
{
	global $db, $ecs;
	
	$table_info = localization_table_info($table);
	if ($table_info !== false)
	{
		extract($table_info);
	}
	else
	{
		return array();
	}
	
	if (empty($localizable_fields))
	{
		$localizable_fields = localizable_fields_for_table($table);
	}
	array_unshift($localizable_fields, 'lang');
	
	$sql = "SELECT `" . implode('`, `', $localizable_fields) . "` " .
			"FROM " . $ecs->table($lang_table) . " " .
			"WHERE `" . $primary_key . "` = '" . $primary_id . "'";
	$res = $db->getAll($sql);
	$localized_versions = array();
	foreach ($res as $row)
	{
		$lang = $row['lang'];
		unset($row['lang']);
		$localized_versions[$lang] = $row;
	}
	return $localized_versions;
}

function save_localized_versions($table, $primary_id, $localized_versions = null, $localizable_fields = array())
{
	global $db, $ecs;
	
	$table_info = localization_table_info($table);
	if ($table_info !== false)
	{
		extract($table_info);
	}
	else
	{
		return false;
	}
	
	if (empty($primary_id))
	{
		return false;
	}
	
	if (empty($localizable_fields))
	{
		$localizable_fields = localizable_fields_for_table($table);
	}
	
	// If $localized_versions is not provided, get it from $_POST
	if (is_null($localized_versions))
	{
		$localized_versions = empty($_POST['localized_versions']) ? '' : stripslashes(trim($_POST['localized_versions']));
	}

	if (!empty($localized_versions))
	{
		$localized_versions = json_decode($localized_versions, true);
		if (is_array($localized_versions))
		{
			foreach ($localized_versions as $lang => $data)
			{
				foreach ($data as $field => $value)
				{
					// keep only localizable fields
					if (!in_array($field, $localizable_fields))
					{
						unset($data[$field]);
						continue;
					}
					// Set value to null if it's empty string
					if ($value === '')
					{
						if ($field != "perma_link") {
							$data[$field] = null;
						} else {
							unset($data[$field]);
						}
					}
				}
				// insert / update if $data is not empty
				if (!empty($data))
				{
					// create insert string
					$insert_data = array_merge(array('lang' => $lang), $data);
					$insert_data_str = implode(', ', array_map(function ($v) {
						return is_null($v) ? 'NULL' : "'" . mysql_escape_string($v) . "'";
					}, $insert_data));
					$primary_ids = is_array($primary_id) ? $primary_id : array($primary_id);
					$insert_str = "(`" . $primary_key . "`, `" . implode('`, `', array_keys($insert_data)) . "`) " .
									"VALUES " . implode(',', array_map(function ($prim_id) use ($insert_data_str) {
										return "('" . mysql_escape_string($prim_id) . "', " . $insert_data_str . ")";
									}, $primary_ids));
					
					// create update string
					$update_str = implode(', ', array_map(function ($k, $v) {
						return "`" . $k . "` = " . (is_null($v) ? 'NULL' : "'" . mysql_escape_string($v) . "'");
					}, array_keys($data), $data));
					
					// create the actual sql
					$sql = "INSERT INTO " . $ecs->table($lang_table) . " " . $insert_str . " ON DUPLICATE KEY UPDATE " . $update_str;
					$db->query($sql);
				}
			}
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

function localized_update($table, $primary_id, $data, $lang = '')
{
	global $db, $ecs;
	
	if (empty($table) || empty($primary_id) || empty($data))
	{
		return false;
	}
	
	$table_info = localization_table_info($table);
	if ($table_info !== false)
	{
		extract($table_info);
	}
	
	if ((!empty($lang_table)) && (!empty($lang)) && ($lang != default_language()))
	{
		// create insert string
		$insert_data = array_merge(array('lang' => $lang), $data);
		$insert_data_str = implode(', ', array_map(function ($v) {
			return is_null($v) ? 'NULL' : "'" . $v . "'";
		}, $insert_data));
		$primary_ids = is_array($primary_id) ? $primary_id : array($primary_id);
		$insert_str = "(`" . $primary_key . "`, `" . implode('`, `', array_keys($insert_data)) . "`) " .
						"VALUES " . implode(',', array_map(function ($prim_id) use ($insert_data_str) {
							return "('" . $prim_id . "', " . $insert_data_str . ")";
						}, $primary_ids));
		// create update string
		$update_str = implode(', ', array_map(function ($k, $v) {
			return "`" . $k . "` = " . (is_null($v) ? 'NULL' : "'" . $v . "'");
		}, array_keys($data), $data));
		// create the actual sql
		$sql = "INSERT INTO " . $ecs->table($lang_table) . " " . $insert_str . " ON DUPLICATE KEY UPDATE " . $update_str;
	}
	else
	{
		// make sure we have primary key info
		if (empty($primary_key))
		{
			$sql = "SHOW KEYS FROM " . $ecs->table($table) . " WHERE Key_name = 'PRIMARY'";
			$res = $db->getRow($sql);
			$primary_key = $res['Column_name'];
		}
		// support targetting multiple ids
		$primary_ids = is_array($primary_id) ? $primary_id : array($primary_id);
		// create update string
		$update_str = implode(', ', array_map(function ($k, $v) {
			return "`" . $k . "` = " . (is_null($v) ? 'NULL' : "'" . $v . "'");
		}, array_keys($data), $data));
		// create the actual sql
		$sql = "UPDATE " . $ecs->table($table) . " SET " . $update_str . " WHERE `" . $primary_key . "` " . db_create_in($primary_ids);
	}
	return $db->query($sql);
}

// Get language keys to return in a language pack
function language_keys_for_pack($languagePack = 'common')
{
	$langKeys = array();
	//if ($languagePack == 'common') // common pack is always included
	{
		$langKeys[] = 'global_OK_only';
		$langKeys[] = 'global_OK';
		$langKeys[] = 'global_Cancel';
		$langKeys[] = 'global_ajax_error';
		$langKeys[] = 'global_404_error';
		$langKeys[] = 'global_home';
		$langKeys[] = 'global_category';
		$langKeys[] = 'global_copied';
		
		$langKeys[] = 'global_search_placeholder';
		$langKeys[] = 'global_search_popular';
		$langKeys[] = 'global_search_alert';
		
		$langKeys[] = 'global_header_menu_user_login_register';
		$langKeys[] = 'global_header_menu_user_points';
		$langKeys[] = 'global_header_menu_user_orders';
		$langKeys[] = 'global_header_menu_user_profile';
		$langKeys[] = 'global_header_menu_user_affiliate';
		$langKeys[] = 'global_header_menu_user_logout';
		$langKeys[] = 'global_header_menu_user_name_message';
		
		$langKeys[] = 'global_cart_add_success';
		$langKeys[] = 'global_cart_quantity_exceeded';
		$langKeys[] = 'global_cart_invalid_quantity';
		$langKeys[] = 'global_cart_add_failed';
		$langKeys[] = 'global_cart_add_quantity_min';
		$langKeys[] = 'global_cart_add_quantity_max';
		$langKeys[] = 'global_cart_added_text';
		$langKeys[] = 'global_cart_added_popup_title';
		$langKeys[] = 'global_cart_added_popup_item_count';
		$langKeys[] = 'global_cart_added_popup_total_amount';
		$langKeys[] = 'global_cart_added_popup_go_cart_btn';
		
		$langKeys[] = 'global_login_popup_title';
		$langKeys[] = 'global_login_popup_username';
		$langKeys[] = 'account_email';
		$langKeys[] = 'account_password2';
		$langKeys[] = 'global_register';
		$langKeys[] = 'account_already_member_login_here';
		$langKeys[] = 'account_read_agree';	
		$langKeys[] = 'account_and';	
		$langKeys[] = 'account_i_have_read_tos';
		$langKeys[] = 'account_tos';
		$langKeys[] = 'account_privacy';
		$langKeys[] = 'account_tos_link';
		$langKeys[] = 'account_privacy_link';
		$langKeys[] = 'pls_agree_tos_and_privacy';	
		$langKeys[] = 'account_reference';
		$langKeys[] = 'global_login_popup_password';
		$langKeys[] = 'global_login_popup_login_btn';
		$langKeys[] = 'global_login_popup_register_btn';
		$langKeys[] = 'global_login_popup_no_account_register_btn';
		$langKeys[] = 'global_login_popup_forget_password_btn';
		$langKeys[] = 'global_login_popup_please_login';
		$langKeys[] = 'global_login_popup_please_fill_in';
		$langKeys[] = 'global_login_popup_logging_in';
		$langKeys[] = 'global_login_popup_login_failed';
		
		$langKeys[] = 'global_modal_title_info';
		$langKeys[] = 'global_modal_title_question';
		$langKeys[] = 'global_modal_title_success';
		$langKeys[] = 'global_modal_title_warning';
		$langKeys[] = 'global_modal_title_error';

		$langKeys[] = 'wishlist_title';
		$langKeys[] = 'wishlist_add';
		$langKeys[] = 'wishlist_remove';
		$langKeys[] = 'wishlist_add_success';
		$langKeys[] = 'wishlist_remove_success';
		$langKeys[] = 'wishlist_add_failed';
		$langKeys[] = 'wishlist_remove_failed';
		$langKeys[] = 'wishlist_on_wishlist';

		$langKeys[] = 'goods_discontinued';
		$langKeys[] = 'goods_show_related';
		$langKeys[] = 'goods_discontinued_redirect';

		$langKeys[] = 'user_send_verify';
		$langKeys[] = 'user_verify';
		$langKeys[] = 'user_sms_already';
		$langKeys[] = 'affiliate_copy';
		$langKeys[] = 'user_verify_code';
		$langKeys[] = 'user_cancel_sms_verify';
		$langKeys[] = 'comment_disclaimer';
		$langKeys[] = 'flashdeal_cart_timeout';
		$langKeys[] = 'global_flashdeal_bottombar_timeout';
		$langKeys[] = 'payment_failed_flashdeal';

        $langKeys[] = 'lucky_draw_already_redeemed';
        $langKeys[] = 'lucky_draw_invalid_user';

		$langKeys[] = 'user_rank_verification';
		$langKeys[] = 'user_rank_program_apply';
		$langKeys[] = 'user_rank_program_applying';
		$langKeys[] = 'user_rank_program_approved';
		$langKeys[] = 'user_rank_program_rejected';
		$langKeys[] = 'user_rank_program_supplement';
		$langKeys[] = 'user_rank_program_received';
		$langKeys[] = 'user_rank_program_email_domain_not_find';
		$langKeys[] = 'user_rank_program_not_found';
		$langKeys[] = 'user_rank_program_not_selected';
		$langKeys[] = 'user_rank_user_not_selected';
		$langKeys[] = 'user_rank_program_name';
		$langKeys[] = 'user_rank_program_username';
		$langKeys[] = 'user_rank_program_proof';
		$langKeys[] = 'user_rank_program_submit';
		$langKeys[] = 'user_rank_program_applied';
		$langKeys[] = 'user_rank_program_email_duplicate';

		$langKeys[] = 'treasure_tnc';
		$langKeys[] = 'treasure_coupon';
		$langKeys[] = 'treasure_prize';
		$langKeys[] = 'treasure_step_found';
		$langKeys[] = 'treasure_step_unlock';
		$langKeys[] = 'treasure_step_prize';
		$langKeys[] = 'treasure_step_prize_none';
		$langKeys[] = 'treasure_step_detail_prize';
		$langKeys[] = 'treasure_step_detail_validate';
		$langKeys[] = 'treasure_action_unlock';
		$langKeys[] = 'treasure_action_detail';
		$langKeys[] = 'treasure_action_open';
		$langKeys[] = 'treasure_action_fail_1';
		$langKeys[] = 'treasure_action_fail_2';
		$langKeys[] = 'treasure_action_fail_3';

		$langKeys[] = 'event_not_start';
		$langKeys[] = 'event_ended';
		$langKeys[] = 'event_not_login';
		$langKeys[] = 'event_no_gift';
		$langKeys[] = 'event_gift_confirm';
		$langKeys[] = 'event_gift_coupon';
		$langKeys[] = 'event_coupon_min_amount';
		$langKeys[] = 'event_coupon_code';
		$langKeys[] = 'event_record_now';
		$langKeys[] = 'event_gift_no_reissue';
		$langKeys[] = 'event_verify_phone_number';
		$langKeys[] = 'event_hk_phone_number_only';
		$langKeys[] = 'event_tnc_title';
		$langKeys[] = 'event_require_text';
		$langKeys[] = 'event_require_length';
		$langKeys[] = 'user_sms_already_verified';
		$langKeys[] = 'user_sms_incorrect_code';
		$langKeys[] = 'user_sms_no_record';

		$langKeys[] = 'order_receipt_proof_accept';
		$langKeys[] = 'order_receipt_message_pdf_preview';
		
		$langKeys[] = 'cart_remove_prompt';
	}
	if ($languagePack == 'index')
	{
		$langKeys[] = 'index_tags_title';
		$langKeys[] = 'index_blog_post_title';
		$langKeys[] = 'index_email_title';
		$langKeys[] = 'index_email_placeholder';
		$langKeys[] = 'index_email_subscribe';
		$langKeys[] = 'index_email_error_empty';
		$langKeys[] = 'index_email_error_invalid';
		$langKeys[] = 'index_email_result_registered';
		$langKeys[] = 'index_email_result_confirmed';
	}
	if (in_array($languagePack, array('category', 'search')))
	{
		$langKeys[] = 'productlist_brand_filter_more';
		$langKeys[] = 'productlist_brand_filter_less';
	}
	if ($languagePack == 'goods')
	{
		$langKeys[] = 'goods_promote_ended';
		$langKeys[] = 'goods_promote_ending_soon';
		$langKeys[] = 'goods_promote_day';
		$langKeys[] = 'goods_promote_hour';
		$langKeys[] = 'goods_promote_miniute';
		$langKeys[] = 'goods_promote_second';
		$langKeys[] = 'goods_stay_tuned_error_name_empty';
		$langKeys[] = 'goods_stay_tuned_error_value_empty';
		$langKeys[] = 'goods_stay_tuned_error_invalid_tel';
		$langKeys[] = 'goods_stay_tuned_error_invalid_email';
		$langKeys[] = 'goods_stay_tuned_error_invalid_whatsapp';
		$langKeys[] = 'goods_stay_tuned_submitting';
		$langKeys[] = 'goods_stay_tuned_submitted_prompt';
		$langKeys[] = 'goods_stay_tuned_submitted_text';
		$langKeys[] = 'goods_stay_tuned_submit';
		$langKeys[] = 'goods_feedback_submit';
		$langKeys[] = 'goods_feedback_cancel';
		$langKeys[] = 'goods_feedback_submiting';
		$langKeys[] = 'goods_feedback_submitted';
		$langKeys[] = 'goods_suggest_a_price';
		$langKeys[] = 'goods_feedback_product';
		$langKeys[] = 'goods_feedback_price';
		$langKeys[] = 'goods_feedback_our_price';
		$langKeys[] = 'goods_feedback_price_source';
		$langKeys[] = 'goods_feedback_price_source_placeholder';
		$langKeys[] = 'goods_feedback_other_comment';
		$langKeys[] = 'goods_report_a_problem';
		$langKeys[] = 'dropzone_upload_files';
		$langKeys[] = 'comment_submit';
		$langKeys[] = 'comment_upload_limit';
		$langKeys[] = 'comment_success';
		$langKeys[] = 'comment_error_frequently';
		$langKeys[] = 'comment_disclaimer';
		$langKeys[] = 'goods_add_to_cart';
		$langKeys[] = 'offer_price';
		$langKeys[] = 'original_price';
		$langKeys[] = 'added';
		$langKeys[] = 'add_goods_success';
		$langKeys[] = 'gift_desc';
		$langKeys[] = 'redeem_desc';
		$langKeys[] = 'offer_set_desc';
		$langKeys[] = 'vip_hint_tittle';
		$langKeys[] = 'redeem_add_to_cart_auto';
		$langKeys[] = 'gifts_add_to_cart_auto';
		$langKeys[] = 'gifts_add_to_cart_select';
		
	}
	if ($languagePack == 'flashdeal')
	{
		$langKeys[] = 'flashdeal_notifyme_instruction';
		$langKeys[] = 'flashdeal_notifyme_email';
		$langKeys[] = 'flashdeal_notifyme_error_missing_email';
		$langKeys[] = 'flashdeal_notifyme_error_invalid_email';
		$langKeys[] = 'flashdeal_notifyme_success';
		$langKeys[] = 'flashdeal_notifyme_failed';
		$langKeys[] = 'flashdeal_title';
		$langKeys[] = 'flashdeal_ended';
		$langKeys[] = 'flashdeal_started';
		$langKeys[] = 'goods_promote_ending_soon';
		$langKeys[] = 'goods_promote_day';
		$langKeys[] = 'goods_promote_hour';
		$langKeys[] = 'goods_promote_miniute';
		$langKeys[] = 'goods_promote_second';
	}
	if ($languagePack == 'user')
	{
		$langKeys[] = 'user_old_password_empty';
		$langKeys[] = 'user_new_password_empty';
		$langKeys[] = 'user_confirm_password_empty';
		$langKeys[] = 'user_confirm_password_mismatch';
		$langKeys[] = 'user_confirm_delivery_prompt';
		$langKeys[] = 'not_tracking_info';
		$langKeys[] = 'user_send_verify_mobile';
		$langKeys[] = 'user_sms_already';
		$langKeys[] = 'user_rank_verification';
		$langKeys[] = 'user_rank_program_apply';
		$langKeys[] = 'user_rank_program_applying';
		$langKeys[] = 'user_rank_program_approved';
		$langKeys[] = 'user_rank_program_rejected';
		$langKeys[] = 'user_rank_program_supplement';
		$langKeys[] = 'user_rank_program_received';
		$langKeys[] = 'user_rank_program_email_domain_not_find';
		$langKeys[] = 'user_rank_program_not_found';
		$langKeys[] = 'user_rank_program_not_selected';
		$langKeys[] = 'user_rank_user_not_selected';
		$langKeys[] = 'user_rank_program_name';
		$langKeys[] = 'user_rank_program_username';
		$langKeys[] = 'user_rank_program_proof';
		$langKeys[] = 'user_rank_program_submit';
		$langKeys[] = 'user_rank_program_applied';
		$langKeys[] = 'user_rank_program_email_duplicate';
	}
	if ($languagePack == 'account')
	{
		$langKeys[] = 'account_please_fill_in';
		$langKeys[] = 'account_username';
		$langKeys[] = 'account_password';
		$langKeys[] = 'account_logging_in';
		$langKeys[] = 'account_login_btn';
		$langKeys[] = 'account_login_failed';
		$langKeys[] = 'account_email';
		$langKeys[] = 'account_format_invalid';
		$langKeys[] = 'account_password_too_short';
		$langKeys[] = 'account_password_mismatch';
		$langKeys[] = 'account_submitting';
		$langKeys[] = 'account_agree_tos_and_register_btn';
		$langKeys[] = 'account_reset_password_use_default';
		$langKeys[] = 'account_reset_password_back_to_login';
		$langKeys[] = 'account_reset_password_requires_email';
		$langKeys[] = 'account_reset_password_email_sent_to';
		$langKeys[] = 'account_reset_password_check_mailbox';
		$langKeys[] = 'account_reset_password_back_to_edit';
		$langKeys[] = 'account_reset_password_success';
		$langKeys[] = 'account_reset_password_btn';
		$langKeys[] = 'account_reset_password_failed';
	}
	if ($languagePack == 'cart')
	{
		$langKeys[] = 'cart_clear_cart_prompt';
		$langKeys[] = 'cart_submitting';
		$langKeys[] = 'cart_remove_prompt';
		$langKeys[] = 'cart_update_limit';
		$langKeys[] = 'cart_summary';
		$langKeys[] = 'cart_checkout_now';
		$langKeys[] = 'cart_item_count';
		$langKeys[] = 'cart_update_failed';
		$langKeys[] = 'cart_remove_failed';
		$langKeys[] = 'goods_promote_day';
		$langKeys[] = 'goods_promote_hour';
		$langKeys[] = 'goods_promote_miniute';
		$langKeys[] = 'goods_promote_second';
		$langKeys[] = 'cart_goods_preorder';
		$langKeys[] = 'goods_delivery_timing_preorder';
		$langKeys[] = 'checkout_has_preorder_goods';
		$langKeys[] = 'productlist_add_to_cart';
		$langKeys[] = 'cart_checkout_notice';
		$langKeys[] = 'checkout_has_missing_require_goods';
		$langKeys[] = 'cart_require_goods_added';
		$langKeys[] = 'checkout_has_multiple_delivery_party';
	}
	if ($languagePack == 'checkout')
	{
		$langKeys[] = 'checkout_address_email';
		$langKeys[] = 'checkout_address_consignee';
		$langKeys[] = 'checkout_address_country';
		$langKeys[] = 'checkout_address_please_select';
		$langKeys[] = 'checkout_address_zipcode';
		$langKeys[] = 'checkout_address_street';
		$langKeys[] = 'checkout_address_phone';
		$langKeys[] = 'checkout_address_cn_phone_11';
		$langKeys[] = 'checkout_address_id_doc';
		$langKeys[] = 'checkout_address_cn_id_doc_18';
		$langKeys[] = 'checkout_address_id_doc_remark_1';
		$langKeys[] = 'checkout_address_id_doc_remark_2';
		$langKeys[] = 'checkout_address_id_doc_remark_3';
		$langKeys[] = 'checkout_fee_cn_ecom_shipping';
		$langKeys[] = 'checkout_address_head';
		$langKeys[] = 'checkout_contact_head';
		$langKeys[] = 'checkout_address_save_btn';
		$langKeys[] = 'checkout_address_next_btn';
		$langKeys[] = 'checkout_address_prev_btn';
		$langKeys[] = 'checkout_address_cancel_btn';
		$langKeys[] = 'checkout_back_to_cart';
		$langKeys[] = 'checkout_address_error_exceed';
		$langKeys[] = 'checkout_edit_address';
		$langKeys[] = 'checkout_add_address';
		$langKeys[] = 'checkout_address_error_invalid';
		$langKeys[] = 'checkout_address_error_not_selected';
		$langKeys[] = 'checkout_address_error_empty';
		$langKeys[] = 'checkout_address_error_empty_wo_exclamation';
		$langKeys[] = 'checkout_address_updated';
		$langKeys[] = 'checkout_address_error_too_long';
		$langKeys[] = 'checkout_address_error_invalid_format';
		$langKeys[] = 'checkout_contact_no_error_invalid_tel_format';
		$langKeys[] = 'checkout_address_error_register_failed';
		$langKeys[] = 'coupon_error_empty';
		$langKeys[] = 'checkout_use_points_error_not_numeric';
		$langKeys[] = 'checkout_use_points_error_out_of_range';
		$langKeys[] = 'checkout_use_points_over_order_amount';
		$langKeys[] = 'checkout_shipping_fee_included';
		$langKeys[] = 'checkout_shipping_over_size';
		$langKeys[] = 'checkout_pickuppoint_over_size';
		$langKeys[] = 'checkout_shipping_fee_excluded';
		$langKeys[] = 'checkout_submitting';
		$langKeys[] = 'checkout_error_missing_shipping';
		$langKeys[] = 'checkout_error_missing_payment';
		$langKeys[] = 'checkout_address_search_not_found';
        $langKeys[] = 'checkout_address_not_on_list';
        $langKeys[] = 'checkout_address_not_search';
		$langKeys[] = 'checkout_address_edit_address';
		$langKeys[] = 'checkout_locker_second_select';
		$langKeys[] = 'checkout_locker_is_hot';
		$langKeys[] = 'checkout_locker_is_black_list';
		$langKeys[] = 'checkout_submit_btn';
		$langKeys[] = 'checkout_locker_workday';
		$langKeys[] = 'checkout_locker_saturday';
		$langKeys[] = 'checkout_locker_sunday';
		$langKeys[] = 'checkout_locker_holiday';
		$langKeys[] = 'checkout_error_missing_locker';
		$langKeys[] = 'checkout_address_error_nr_info';
		$langKeys[] = 'checkout_shipping_restricted_to_hk';
		$langKeys[] = 'checkout_shipping_fee_on_delivery_notice';
		$langKeys[] = 'checkout_shipping_fee_on_delivery';
		$langKeys[] = 'checkout_insert_the_club_id';
		$langKeys[] = 'checkout_insert_correct_the_club_id';
		$langKeys[] = 'checkout_hint_the_club_id';
		$langKeys[] = 'checkout_terms_the_club_id';
		$langKeys[] = 'checkout_consignee';
		$langKeys[] = 'checkout_shipping';
		$langKeys[] = 'checkout_shipping_type';
		$langKeys[] = 'checkout_address_info';
		$langKeys[] = 'checkout_address_select_type';
		$langKeys[] = 'checkout_address_type';
		$langKeys[] = 'checkout_address_type_1';
		$langKeys[] = 'checkout_address_type_2';
		$langKeys[] = 'checkout_address_type_99';
		$langKeys[] = 'checkout_address_type_other';
		$langKeys[] = 'checkout_address_type_lift';
		$langKeys[] = 'checkout_address_type_lift_sel';
		$langKeys[] = 'checkout_address_type_lift_with';
		$langKeys[] = 'checkout_address_type_lift_with_no_ground';
		$langKeys[] = 'checkout_address_type_lift_wo';
		$langKeys[] = 'stripegooglepay_accept_payment_through';
		$langKeys[] = 'stripegooglepay_support_platform';
		$langKeys[] = 'stripegooglepay_credit_card_data_remain_confidential';
		$langKeys[] = 'stripegooglepay_exchange_rate';
		$langKeys[] = 'stripeapplepay_accept_payment_through';
		$langKeys[] = 'stripeapplepay_support_platform';
		$langKeys[] = 'stripeapplepay_credit_card_data_remain_confidential';
		$langKeys[] = 'stripeapplepay_exchange_rate';
		$langKeys[] = 'flashdeal_addtocart_error_only_single_payment_method_allowed';
	}
	if ($languagePack == 'payment')
	{
		$langKeys[] = 'payment_bank_account_name';
		$langKeys[] = 'payment_bank_hsbc';
		$langKeys[] = 'payment_bank_hs';
		$langKeys[] = 'payment_bank_boc';
		$langKeys[] = 'payment_bank_dbs';
		$langKeys[] = 'payment_bank_macau_boc';
		$langKeys[] = 'payment_bank_macau_icbc';
		$langKeys[] = 'payment_bank_transfer';
		$langKeys[] = 'payment_tell_yoho';
		$langKeys[] = 'payment_tell_yoho_whatsapp';
		$langKeys[] = 'payment_tell_yoho_email';
		$langKeys[] = 'payment_tell_yoho_line';
		$langKeys[] = 'payment_tell_yoho_weixin';
		$langKeys[] = 'payment_tell_yoho_facebook_messenger';
		$langKeys[] = 'payment_tell_yoho_facebook_messenger_link';
		$langKeys[] = 'payment_tell_yoho_confirm';
		$langKeys[] = 'payment_bank_transfer_title';
		$langKeys[] = 'payment_dialog_ok';
		$langKeys[] = 'payment_alipay';
		$langKeys[] = 'payment_alipay_account';
		$langKeys[] = 'payment_alipay_name';
		$langKeys[] = 'payment_alipay_title';
		$langKeys[] = 'payment_change_method';
	}
	if ($languagePack == 'mobile')
	{
		$langKeys[] = 'goods_stay_tuned_error_name_empty';
		$langKeys[] = 'goods_stay_tuned_error_value_empty';
		$langKeys[] = 'goods_stay_tuned_error_invalid_tel';
		$langKeys[] = 'goods_stay_tuned_error_invalid_email';
		$langKeys[] = 'goods_stay_tuned_error_invalid_whatsapp';
		$langKeys[] = 'goods_stay_tuned_submitting';
		$langKeys[] = 'goods_stay_tuned_submitted_prompt';
		$langKeys[] = 'goods_stay_tuned_submit';
		$langKeys[] = 'wishlist_title';
		$langKeys[] = 'wishlist_remove';
		$langKeys[] = 'wishlist_add';
		$langKeys[] = 'wishlist_add_success';
		$langKeys[] = 'wishlist_remove_success';
		$langKeys[] = 'wishlist_add_failed';
		$langKeys[] = 'wishlist_remove_failed';
		$langKeys[] = 'flashdeal_notifyme_title';
		$langKeys[] = 'flashdeal_notifyme_instruction';
		$langKeys[] = 'flashdeal_notifyme_email';
		$langKeys[] = 'flashdeal_notifyme_error_missing_email';
		$langKeys[] = 'flashdeal_notifyme_error_invalid_email';
		$langKeys[] = 'flashdeal_notifyme_success';
		$langKeys[] = 'flashdeal_notifyme_failed';
		$langKeys[] = 'flashdeal_title';
		$langKeys[] = 'flashdeal_ended';
		$langKeys[] = 'flashdeal_started';
		$langKeys[] = 'flashdeal_addtocart_error_only_single_payment_method_allowed';
		$langKeys[] = 'goods_promote_ending_soon';
		$langKeys[] = 'goods_promote_day';
		$langKeys[] = 'goods_promote_hour';
		$langKeys[] = 'goods_promote_miniute';
		$langKeys[] = 'goods_promote_second';
		$langKeys[] = 'account_username';
		$langKeys[] = 'account_email';
		$langKeys[] = 'account_password';
		$langKeys[] = 'account_please_fill_in';
		$langKeys[] = 'account_list_delimiter';
		$langKeys[] = 'account_format_invalid';
		$langKeys[] = 'account_password_too_short';
		$langKeys[] = 'account_password_mismatch';
		$langKeys[] = 'account_read_and_agree_tos';
		$langKeys[] = 'account_submitting';
		$langKeys[] = 'account_register_btn';
		$langKeys[] = 'account_register_error_unknown';
		$langKeys[] = 'account_reset_password_needs_input';
		$langKeys[] = 'account_reset_password_btn';
		$langKeys[] = 'account_reset_password_email_sent_to';
		$langKeys[] = 'account_reset_password_use_default';
		$langKeys[] = 'account_reset_password_requires_email';
		$langKeys[] = 'account_reset_password_title';
		$langKeys[] = 'account_reset_password_success';
		$langKeys[] = 'account_reset_password_failed';
		$langKeys[] = 'cart_update_failed';
		$langKeys[] = 'cart_remove_prompt';
		$langKeys[] = 'checkout_error_missing_shipping';
		$langKeys[] = 'checkout_error_missing_payment';
		$langKeys[] = 'checkout_submitting';
		$langKeys[] = 'checkout_address_email';
		$langKeys[] = 'checkout_address_consignee';
		$langKeys[] = 'checkout_address_country';
		$langKeys[] = 'checkout_address_please_select';
		$langKeys[] = 'checkout_address_zipcode';
		$langKeys[] = 'checkout_address_street';
		$langKeys[] = 'checkout_search_address';
		$langKeys[] = 'checkout_address_building';
		$langKeys[] = 'checkout_address_area';
		$langKeys[] = 'checkout_address_room';
		$langKeys[] = 'checkout_address_floor';
		$langKeys[] = 'checkout_address_street_name';
		$langKeys[] = 'checkout_address_street_num';
		$langKeys[] = 'checkout_address_locality';
		$langKeys[] = 'checkout_address_search_placeholder';
		$langKeys[] = 'checkout_address_edit';
		$langKeys[] = 'checkout_address_search_not_found';
		$langKeys[] = 'checkout_address_not_on_list';
        $langKeys[] = 'checkout_address_not_search';
		$langKeys[] = 'checkout_address_edit_address';
		$langKeys[] = 'checkout_address_phone';
		$langKeys[] = 'checkout_address_head';
		$langKeys[] = 'checkout_address_cn_phone_11';
		$langKeys[] = 'checkout_address_id_doc';
		$langKeys[] = 'checkout_address_cn_id_doc_18';
		$langKeys[] = 'checkout_address_receiver_info';
		$langKeys[] = 'checkout_address_id_doc_remark_1';
		$langKeys[] = 'checkout_address_id_doc_remark_2';
		$langKeys[] = 'checkout_address_id_doc_remark_3';
		$langKeys[] = 'checkout_fee_cn_ecom_shipping';
		$langKeys[] = 'checkout_contact_head';
		$langKeys[] = 'checkout_address_save_btn';
		$langKeys[] = 'checkout_address_next_btn';
		$langKeys[] = 'checkout_address_prev_btn';
		$langKeys[] = 'checkout_edit_address';
		$langKeys[] = 'checkout_add_address';
		$langKeys[] = 'checkout_address_error_empty';
		$langKeys[] = 'checkout_address_error_empty_wo_exclamation';
		$langKeys[] = 'checkout_address_error_invalid';
		$langKeys[] = 'checkout_address_error_too_long';
		$langKeys[] = 'checkout_address_error_exceed';
		$langKeys[] = 'checkout_address_error_invalid_format';
		$langKeys[] = 'checkout_address_error_register_failed';
		$langKeys[] = 'checkout_locker_second_select';
		$langKeys[] = 'checkout_locker_is_hot';
		$langKeys[] = 'checkout_locker_is_black_list';
		$langKeys[] = 'checkout_submit_btn';
		$langKeys[] = 'checkout_locker_workday';
		$langKeys[] = 'checkout_locker_saturday';
		$langKeys[] = 'checkout_locker_sunday';
		$langKeys[] = 'checkout_locker_holiday';
		$langKeys[] = 'checkout_error_missing_locker';
		$langKeys[] = 'checkout_shipping_restricted_to_hk';
		$langKeys[] = 'checkout_shipping_fee_on_delivery';
		$langKeys[] = 'checkout_shipping_fee_on_delivery_notice';
		$langKeys[] = 'payment_bank_account_name';
		$langKeys[] = 'payment_bank_hsbc';
		$langKeys[] = 'payment_bank_hs';
		$langKeys[] = 'payment_bank_boc';
		$langKeys[] = 'payment_bank_dbs';
		$langKeys[] = 'payment_bank_macau_boc';
		$langKeys[] = 'payment_bank_macau_icbc';
		$langKeys[] = 'payment_bank_transfer';
		$langKeys[] = 'payment_tell_yoho';
		$langKeys[] = 'payment_tell_yoho_whatsapp';
		$langKeys[] = 'payment_tell_yoho_email';
		$langKeys[] = 'payment_tell_yoho_line';
		$langKeys[] = 'payment_tell_yoho_weixin';
		$langKeys[] = 'payment_tell_yoho_facebook_messenger';
		$langKeys[] = 'payment_tell_yoho_facebook_messenger_link';
		$langKeys[] = 'payment_tell_yoho_confirm';
		$langKeys[] = 'payment_bank_transfer_title';
		$langKeys[] = 'payment_dialog_ok';
		$langKeys[] = 'payment_alipay';
		$langKeys[] = 'payment_alipay_account';
		$langKeys[] = 'payment_alipay_name';
		$langKeys[] = 'payment_alipay_title';
		$langKeys[] = 'payment_change_method_submit';
		$langKeys[] = 'payment_change_method';
		$langKeys[] = 'index_email_title';
		$langKeys[] = 'index_email_result_confirmed';
		$langKeys[] = 'index_email_result_registered';
		$langKeys[] = 'goods_delivery_timing_preorder';
		$langKeys[] = 'checkout_has_preorder_goods';
		$langKeys[] = 'checkout_preorder_goods';
		$langKeys[] = 'cart_submitting';
		$langKeys[] = 'user_send_verify_mobile';
		$langKeys[] = 'user_send_verify';
		$langKeys[] = 'checkout_error_missing_locker';
		$langKeys[] = 'checkout_address_error_nr_info';
		$langKeys[] = 'checkout_has_require_goods';
		$langKeys[] = 'cart_require_goods_buy_now';
		$langKeys[] = 'productlist_add_to_cart';
		$langKeys[] = 'cart_checkout_notice';
		$langKeys[] = 'checkout_has_missing_require_goods';
		$langKeys[] = 'cart_require_goods_added';
		$langKeys[] = 'productlist_add_to_cart_mobile';
        $langKeys[] = 'lucky_draw_already_redeemed';
        $langKeys[] = 'lucky_draw_invalid_user';
		$langKeys[] = 'checkout_has_multiple_delivery_party';
		$langKeys[] = 'checkout_insert_the_club_id';
		$langKeys[] = 'checkout_insert_correct_the_club_id';
		$langKeys[] = 'checkout_hint_the_club_id';
		$langKeys[] = 'checkout_terms_the_club_id';
		$langKeys[] = 'checkout_contact_no_error_invalid_tel_format';
		$langKeys[] = 'checkout_consignee';
		$langKeys[] = 'checkout_shipping';
		$langKeys[] = 'checkout_shipping_type';
		$langKeys[] = 'checkout_address_error_not_selected';
		$langKeys[] = 'checkout_address_info';
		$langKeys[] = 'checkout_address_select_type';
		$langKeys[] = 'checkout_address_type';
		$langKeys[] = 'checkout_address_type_1';
		$langKeys[] = 'checkout_address_type_2';
		$langKeys[] = 'checkout_address_type_99';
		$langKeys[] = 'checkout_address_type_other';
		$langKeys[] = 'checkout_address_type_lift';
		$langKeys[] = 'checkout_address_type_lift_sel';
		$langKeys[] = 'checkout_address_type_lift_with';
		$langKeys[] = 'checkout_address_type_lift_with_no_ground';
		$langKeys[] = 'checkout_address_type_lift_wo';
		$langKeys[] = 'gift_desc';
		$langKeys[] = 'redeem_desc';
		$langKeys[] = 'offer_set_desc';
		$langKeys[] = 'add_goods_success';
		$langKeys[] = 'cart_add_success_favourable';
		$langKeys[] = 'cart_add_success_package';
		$langKeys[] = 'gifts';
		$langKeys[] = 'redeem_goods';
		$langKeys[] = 'stripegooglepay_accept_payment_through';
		$langKeys[] = 'stripegooglepay_support_platform';
		$langKeys[] = 'stripegooglepay_credit_card_data_remain_confidential';
		$langKeys[] = 'stripegooglepay_exchange_rate';
		$langKeys[] = 'stripeapplepay_accept_payment_through';
		$langKeys[] = 'stripeapplepay_support_platform';
		$langKeys[] = 'stripeapplepay_credit_card_data_remain_confidential';
		$langKeys[] = 'stripeapplepay_exchange_rate';
	}
	if ($languagePack == 'subscription')
	{
		$langKeys[] = 'subscription_header_subscribe';
		$langKeys[] = 'subscription_header_unsubscribe';
		$langKeys[] = 'subscription_header_update';
		$langKeys[] = 'subscription_tips_subscribe';
		$langKeys[] = 'subscription_tips_unsubscribe';
		$langKeys[] = 'subscription_tips_update';
		$langKeys[] = 'subscription_button_submit';
		$langKeys[] = 'subscription_button_load';
		$langKeys[] = 'subscription_button_subscribe';
		$langKeys[] = 'subscription_button_unsubscribe';
		$langKeys[] = 'subscription_button_update';
		$langKeys[] = 'subscription_label_name';
		$langKeys[] = 'subscription_label_email';
		$langKeys[] = 'subscription_label_old_email';
		$langKeys[] = 'subscription_label_new_email';
		$langKeys[] = 'subscription_message_no_mail';
		$langKeys[] = 'subscription_message_invalid_mail';
		$langKeys[] = 'subscription_message_subscribe_success';
		$langKeys[] = 'subscription_message_subscribe_already';
		$langKeys[] = 'subscription_message_subscribe_verify';
		$langKeys[] = 'subscription_message_subscribe_cancel';
		$langKeys[] = 'subscription_message_subscribe_missing';
		$langKeys[] = 'subscription_message_subscribe_confirm';
		$langKeys[] = 'subscription_message_unsubscribe_success';
		$langKeys[] = 'subscription_message_unsubscribe_missing';
		$langKeys[] = 'subscription_message_update_success';
		$langKeys[] = 'subscription_message_update_cancel';
		$langKeys[] = 'subscription_message_update_missing';
		$langKeys[] = 'subscription_message_subscribe_fail';
		$langKeys[] = 'subscription_message_unsubscribe_fail';
		$langKeys[] = 'subscription_message_update_fail';
	}
	
	return $langKeys;
}

function to_simplified_chinese($fields = [], $localized_versions = null)
{
	if (is_null($localized_versions)) {
		$localized_versions = empty($_POST['localized_versions']) ? [] : json_decode(stripslashes(trim($_POST['localized_versions'])), true);
	}
	if (is_array($localized_versions)) {
		foreach ($fields as $key => $val) {
			$localized_versions['zh_cn'][$key] = translate_to_simplified_chinese($val);
			if ($key == "perma_link") {
				$localized_versions['zh_cn'][$key] = uri_encode($localized_versions['zh_cn'][$key]);
			}
		}
	}
	$_POST['localized_versions'] = addslashes(json_encode($localized_versions));
}

function to_diff_lang($fields = [], $localized_versions = null, $cur_lang='zh_cn',$goods_id)
{
	if (is_null($localized_versions)) {
		$localized_versions = empty($_POST['localized_versions']) ? [] : json_decode(stripslashes(trim($_POST['localized_versions'])), true);
	}
	if (is_array($localized_versions)) {
		foreach ($fields as $key => $val) {
			if ($cur_lang !='zh_tw') {
				$localized_versions[$cur_lang][$key] = $localized_versions[$cur_lang][$key];
				if ($key == "perma_link") {
					$localized_versions[$cur_lang][$key] = uri_encode($val);
					$sel_sql = "SELECT link_id FROM " . $GLOBALS['ecs']->table('perma_link') . " WHERE `id` = " . $goods_id . " AND `lang` LIKE '" . $cur_lang . "'";
					$sel_res = $GLOBALS['db']->getOne($sel_sql);
					if ($sel_res) {
						$upd_sql = "UPDATE " . $GLOBALS['ecs']->table("perma_link") . " SET perma_link = '" . $localized_versions[$cur_lang][$key] . "' WHERE `id` = " . $goods_id . " AND `lang` LIKE '" . $cur_lang . "'";
						$upd_res = $GLOBALS['db']->query($upd_sql);
					} else {
						$ins_sql ="INSERT INTO " . $GLOBALS['ecs']->table("perma_link") . " (`link_id`, `table_name`, `id`, `lang`, `perma_link`) VALUES (NULL, 'goods', '" . $goods_id . "', '" . $cur_lang . "', '" . $localized_versions[$cur_lang][$key] . "')";
						$GLOBALS['db']->query($ins_sql);
					}
				}
			}
		}
	}
	$_POST['localized_versions'] = addslashes(json_encode($localized_versions));
}

function translate_to_simplified_chinese($original_text, $external_lib = false)
{
	$text = stripslashes($original_text);
	$tmp = iconv('UTF-8', 'BIG5', $text);
	$result = "";

	// 20190516: Using external library to translate only
	// If unexpected charater exists
	if ($tmp === false || $external_lib) {
		try {
			include_once ROOT_PATH . "includes/translate/Chinese.php";
			$chinese = new \ChineseTranslate();
			$result = $chinese->to($chinese::CHS, $text);
		} catch (Exception $e) {
		}
	} elseif ($tmp) {
		$tmp = iconv('BIG5', 'GB2312', $tmp);
		if ($tmp === false) {
			return translate_to_simplified_chinese($original_text, true);
		} else {
			$result = iconv('GB2312', 'UTF-8', $tmp);
			if ($result === false) {
				return translate_to_simplified_chinese($original_text, true);
			}
		}
	}
	return $result;
}

?>
