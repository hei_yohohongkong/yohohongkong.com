<?php

/***
 * quotation.php
 * by Anthony 2017-03-29
 *
 * Yoho quotation library
***/

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

require(ROOT_PATH . 'includes/lib_order.php');

/* Count Quotation fee with count pay fee */
function quotation_fee($quotation, $cart_goods, $pay_id = 1, $consignee){

    $orderController = new Yoho\cms\Controller\OrderController();
    $order_fee_platform = $orderController::ORDER_FEE_PLATFORM_WEB;
	// Using order library to count total (without payment fee)
	$total = $orderController->order_fee_flow($quotation, $cart_goods, $consignee, $order_fee_platform);

	// Fix payment rate 3%
	$rate  = -3 / 100;
	
	// Check payment rate
	switch ($pay_id){
		case 1:
			break;
		case 2: // count payment rate
		/*
			$pay_fee                        = $total['amount'] * ($rate > 0 ? $rate / (1 - $rate) : $rate);
			$total['pay_fee']               = ceil($pay_fee);
			$total['pay_fee_formated']      = price_format($total['pay_fee'], false);
		    $total['pay_discount_formated'] = price_format(-$total['pay_fee'], false);
		    $total['amount']               += $total['pay_fee'];
			$total['amount_formated']       = price_format($total['amount'], false);
			*/
			break;
	}

	return $total;
}

function get_quotation_shipping_list($consignee, $cart_weight_price, $quotation, $cal_free_shipping = false){

	global $ecs, $db;

	$region            = array($consignee['country'], $consignee['province'], $consignee['city'], $consignee['district']);
	$shipping_list     = available_shipping_list($region);

	// 查看购物车中是否全为免运费商品，若是则把运费赋为零
	$sql = 'SELECT count(*) FROM ' . $ecs->table('cart') . " WHERE `session_id` = '" . SESS_ID. "' AND `extension_code` != 'package_buy' AND `is_shipping` = 0";
	$shipping_count = $db->getOne($sql);

	foreach ($shipping_list AS $key => $val)
	{
		if($val['shipping_type'] == 'locker') {
			unset($shipping_list[$key]);
			continue;
		}
		$shipping_cfg = unserialize_config($val['configure']);
		if ($shipping_count == 0 AND $cart_weight_price['free_shipping'] == 1 && $cal_free_shipping)
		{
			$shipping_fee = 0;
		}
		else
		{
			$shipping_fee = shipping_fee($val['shipping_code'], unserialize($val['configure']), $cart_weight_price['weight'], $cart_weight_price['amount'], $cart_weight_price['number'], $cart_weight_price['price']);
		}

		$shipping_list[$key]['format_shipping_fee'] = price_format($shipping_fee, false);
		$shipping_list[$key]['shipping_fee']        = $shipping_fee;
		$shipping_list[$key]['free_money']          = price_format($shipping_cfg['free_money'], false);
		$shipping_list[$key]['insure_formated']     = strpos($val['insure'], '%') === false ?
			price_format($val['insure'], false) : $val['insure'];

		/* 当前的配送方式是否支持保价 */
		if ($val['shipping_id'] == $quotation['shipping_id'])
		{
			$insure_disabled = ($val['insure'] == 0);
			$cod_disabled    = ($val['support_cod'] == 0);
		}

		/* 如果不是香港，不顯示運費到付選項 */
		if (($consignee['country'] != 3409) && ($val['support_fod']))
		{
			$shipping_list[$key]['support_fod'] = 0;
		}

		/* 如果訂單中有固定運費產品，不顯示運費到付選項 */
		if (($cart_weight_price['price'] > 0) && ($val['support_fod']))
		{
			$shipping_list[$key]['support_fod'] = 0;
		}
	}

	/* 如果訂單中有固定運費產品，只送香港 */
	if (($cart_weight_price['price'] > 0) && ($consignee['country'] != 3409))
	{
		// Clear shipping_list
		$shipping_list = array();

		$sql = "SELECT c.goods_id " .
				"FROM " . $ecs->table('cart') . " as c " .
				"LEFT JOIN " . $ecs->table('goods') . " as g ON g.goods_id = c.goods_id " .
				"WHERE c.session_id = '" . SESS_ID . "' " .
				"AND rec_type = '$flow_type' AND g.is_shipping = 0 AND g.shipping_price > 0 AND c.extension_code != 'package_buy'";
		$restricted_to_hk_goods_ids = $db->getCol($sql);
		$smarty->assign('restricted_to_hk_goods_ids', $restricted_to_hk_goods_ids);
	}

	/* 如果未指定送貨方式，或送貨方式不正確，自動指定第一個 */
	$available_shipping_ids = array_map(function ($val) { return $val['shipping_id']; }, $shipping_list);
	if (!empty($available_shipping_ids) && (empty($quotation['shipping_id']) || !in_array($quotation['shipping_id'], $available_shipping_ids)))
	{
		$quotation['shipping_id'] = $available_shipping_ids[0];
		$_SESSION['flow_order']   = $quotation;
	}

	return $shipping_list;
}

function add_quotation($contact_info, $shipping, $pay_id, $total){

	global $ecs, $db;

	$quotation_sn = get_quotation_sn();
	$param = array(
		'quotation_sn'  => $quotation_sn,
		'company_name'  => $contact_info['company_name'],
		'contact_name'  => $contact_info['contact_name'],
		'contact_email' => $contact_info['contact_email'],
		'contact_tel'   => $contact_info['contact_tel'],
		'shipping_id'   => $shipping,
		'pay_id'        => $pay_id,
		'pay_fee'       => $total['pay_fee'],
		'shipping_fee'  => $total['shipping_fee'],
		'quotation_fee' => $total['amount'],
		'created_at'    => gmtime()
	);

	return $db->autoExecute('ecs_quotation_info', $param, 'INSERT') ? $db->insert_id() : false;
}

function get_quotation($quotation_id){

	global $ecs, $db;

	if (empty($quotation_id)) return false;

	return $db->getRow("SELECT * " .
		"FROM `ecs_quotation_info` " .
		"WHERE quotation_id = " . $quotation_id . " LIMIT 1");
}

function get_quotation_sn()
{
    // Get a random number
    mt_srand((double) microtime() * 1000000);

    return date('Ymd') . str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT);
}

function add_quotation_goods($quotation_id, $goods){

	if (empty($quotation_id)) return false;

	global $ecs, $db;

	foreach ($goods as $key => $value) {
		$param = array(
			'goods_id'  => $value['goods_id'],
			'goods_name'  => $value['goods_name'],
			'goods_price'  => $value['goods_price'],
			'goods_number' => $value['goods_number'],
			'quotation_id'   => $quotation_id
		);
		$result = $db->autoExecute('ecs_quotation_goods', $param, 'INSERT');
		if(!$result){
			return false;
		}
	}

	return true;
}

function get_quotation_goods($quotation_id)
{
    $sql = "SELECT rec_id, goods_id, goods_name, goods_number, " .
            "goods_price, quotation_id, " .
            "goods_price * goods_number AS subtotal " .
            "FROM " . $GLOBALS['ecs']->table('quotation_goods') .
            " WHERE quotation_id = '$quotation_id'";

    $res = $GLOBALS['db']->query($sql);

    return $res;
}

function generate_pdf($html)
 {
    ini_set('display_errors', 0);
    ini_set('memory_limit', '512M');
    set_time_limit(0);
    // Record start time
    $rustart = getrusage();
    $tstart = time()+microtime();
    require_once ROOT_PATH . 'includes/dompdf/dompdf_config.inc.php';
    $dompdf = new DOMPDF();
    $dompdf->set_base_path(ROOT_PATH);
	// $dompdf->set_option('dpi', 300);
    $dompdf->load_html($html);
    $dompdf->set_paper('A4', 'Portrait');
    $dompdf->render();
    // $dompdf->stream("receipt.pdf", array("Attachment" => false));
    $output = $dompdf->output();
    // Log time used
    $ru = getrusage();
    $t = time()+microtime();
    hw_error_log(__FUNCTION__ . ': time = ' . ($t-$tstart) . ',utime = ' . rutime($ru, $rustart, 'utime') . ', stime = ' . rutime($ru, $rustart, 'stime') . ', memory_get_peak_usage = ' . memory_get_peak_usage());
    return $output;
}
?>
