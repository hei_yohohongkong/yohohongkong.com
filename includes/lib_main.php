<?php

/**
 * ECSHOP 前台公用函数库
 * ============================================================================
 * * 版权所有 2005-2012 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liubo $
 * $Id: lib_main.php 17217 2011-01-19 06:29:08Z liubo $
*/

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

/**
 * 更新用户SESSION,COOKIE及登录时间、登录次数。
 *
 * @access  public
 * @return  void
 */
function update_user_info()
{
    if (!$_SESSION['user_id'])
    {
        return false;
    }

    /* 查询会员信息 */
    $time = date('Y-m-d');
    $sql = 'SELECT u.user_money,u.email, u.pay_points, u.user_rank, u.rank_points, '.
            ' IFNULL(b.type_money, 0) AS user_bonus, u.last_login, u.last_ip'.
            ' FROM ' .$GLOBALS['ecs']->table('users'). ' AS u ' .
            ' LEFT JOIN ' .$GLOBALS['ecs']->table('user_bonus'). ' AS ub'.
            ' ON ub.user_id = u.user_id AND ub.used_time = 0 ' .
            ' LEFT JOIN ' .$GLOBALS['ecs']->table('bonus_type'). ' AS b'.
            " ON b.type_id = ub.bonus_type_id AND b.use_start_date <= '$time' AND b.use_end_date >= '$time' ".
            " WHERE u.user_id = '$_SESSION[user_id]'";
    if ($row = $GLOBALS['db']->getRow($sql))
    {
        /* 更新SESSION */
        $_SESSION['last_time']   = $row['last_login'];
        $_SESSION['last_ip']     = $row['last_ip'];
        $_SESSION['login_fail']  = 0;
        $_SESSION['email']       = $row['email'];

        // Added by howang: also put 積分 in session
        $_SESSION['user_money']  = $row['user_money'];
        $_SESSION['user_bonus']  = $row['user_bonus'];
        $_SESSION['pay_points']  = $row['pay_points'];
        $_SESSION['rank_points'] = $row['rank_points'];

        /*判断是否是特殊等级，可能后台把特殊会员组更改普通会员组*/
        if($row['user_rank'] >0)
        {
            $sql="SELECT special_rank from ".$GLOBALS['ecs']->table('user_rank')."where rank_id='$row[user_rank]'";
            if($GLOBALS['db']->getOne($sql)==='0' || $GLOBALS['db']->getOne($sql)===null)
            {
                $sql="update ".$GLOBALS['ecs']->table('users')."set user_rank='0' where user_id='$_SESSION[user_id]'";
                $GLOBALS['db']->query($sql);
                $row['user_rank']=0;
            }
        }

        /* 取得用户等级和折扣 */
        if ($row['user_rank'] == 0)
        {
            // 非特殊等级，根据等级积分计算用户等级（注意：不包括特殊等级）
            $sql = 'SELECT rank_id, discount FROM ' . $GLOBALS['ecs']->table('user_rank') . " WHERE special_rank = '0' AND min_points <= " . intval($row['rank_points']) . ' AND max_points > ' . intval($row['rank_points']);
            if ($row = $GLOBALS['db']->getRow($sql))
            {
                $_SESSION['user_rank'] = $row['rank_id'];
                $_SESSION['discount']  = $row['discount'] / 100.00;
            }
            else
            {
                $_SESSION['user_rank'] = 0;
                $_SESSION['discount']  = 1;
            }
        }
        else
        {
            // 特殊等级
            $sql = 'SELECT rank_id, discount FROM ' . $GLOBALS['ecs']->table('user_rank') . " WHERE rank_id = '$row[user_rank]'";
            if ($row = $GLOBALS['db']->getRow($sql))
            {
                $_SESSION['user_rank'] = $row['rank_id'];
                $_SESSION['discount']  = $row['discount'] / 100.00;
            }
            else
            {
                $_SESSION['user_rank'] = 0;
                $_SESSION['discount']  = 1;
            }
        }

        if (!isset($_SESSION["chatra_id"])) {
            $sql = "SELECT client_id FROM " . $GLOBALS['ecs']->table('crm_platform_user') . " WHERE user_id = $_SESSION[user_id] AND platform = 4";
            $_SESSION["chatra_id"] = $GLOBALS['db']->getOne($sql);
        }

        if (empty($_SESSION["chatra_id"])) {
            $_SESSION["chatra_id"] = substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(32/strlen($x)))), 1, 32);
            $sql = "INSERT INTO " . $GLOBALS['ecs']->table('crm_platform_user') . " (user_id, platform, client_id) VALUES ($_SESSION[user_id], 4, '$_SESSION[chatra_id]')";
            $GLOBALS['db']->query($sql);
        }

        // Get user rank program
        $userRankController = new Yoho\cms\Controller\UserRankController();
        $rank_list = $userRankController->getUserRankProgram($_SESSION['user_id']);
        if($rank_list && !defined('IS_POS')) {
            $_SESSION['user_rank_program'] = $rank_list;
        } else {
            unset($_SESSION['user_rank_program']);
        }
    }

    /* 更新登录时间，登录次数及登录ip */
    $sql = "UPDATE " .$GLOBALS['ecs']->table('users'). " SET".
           " visit_count = visit_count + 1, ".
           " last_ip = '" .real_ip(). "',".
           " last_login = '" .gmtime(). "',".
           " language = '" . $GLOBALS['_CFG']['lang'] . "'".
           " WHERE user_id = '" . $_SESSION['user_id'] . "'";
    $GLOBALS['db']->query($sql);


    /* 购物车功能改进 */
    // Merge cart contents from the same session / same user
    $sql = "UPDATE " . $GLOBALS['ecs']->table('cart') . " " .
            "SET user_id = '" . $_SESSION['user_id'] . "', " .
            "session_id = '" . SESS_ID . "' " .
            "WHERE user_id = '".$_SESSION['user_id']."' " .
            "OR session_id = '" . SESS_ID . "'";
    $GLOBALS['db']->query($sql);

    // Remove duplicated cart entries after merge
    $sql = "DELETE c " .
            "FROM " . $GLOBALS['ecs']->table('cart') . " as c " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('cart') . " as c1 " .
            "ON c.goods_id = c1.goods_id AND c.user_id = c1.user_id AND c1.rec_id > c.rec_id AND c.is_gift = c1.is_gift AND c.is_package = c1.is_package AND c.is_insurance = c1.is_insurance AND c.insurance_of = c1.insurance_of " . //handle gift goods
            "WHERE c1.rec_id IS NOT NULL AND c.user_id = '".$_SESSION['user_id']."'";
    $GLOBALS['db']->query($sql);

    // Remove 下架 and deleted goods from cart
    $sql = "DELETE c " .
            "FROM " . $GLOBALS['ecs']->table('cart') . " as c " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON c.goods_id = g.goods_id " .
            "WHERE (g.is_on_sale = 0 OR g.is_delete = 1) AND c.user_id = '".$_SESSION['user_id']."'";
    $GLOBALS['db']->query($sql);
	$flashdealController = new \Yoho\cms\Controller\FlashdealController();
    $flashdealController->updateFlashdealCartByECSCart();
}

/**
 *  获取用户信息数组
 *
 * @access  public
 * @param
 *
 * @return array        $user       用户信息数组
 */
function get_user_info($id=0)
{
    if ($id == 0)
    {
        $id = $_SESSION['user_id'];
    }
    // Try to load data from logged-in user's session directly
    if ($id == $_SESSION['user_id'] && $_SESSION['user_id'] > 0)
    {
        $user = array(
            'user_name'  => $_SESSION['user_name'],
            'user_rank'  => $_SESSION['user_rank'],
            'user_bonus' => $_SESSION['user_bonus'],
            'pay_points' => $_SESSION['pay_points'],
        );
        $bonus = array(
            'bonus_value' => $_SESSION['user_bonus']
        );
    }
    else
    {
        $sql  = 'SELECT  u.email, u.user_name, u.user_money, u.user_rank, u.pay_points'.//sbsn0702 user_rank
                ' FROM ' .$GLOBALS['ecs']->table('users'). ' AS u ' .
                " WHERE u.user_id = '$id'";
        $user = $GLOBALS['db']->getRow($sql);
        $bonus = get_user_bonus($id);
    }

    $user['ordernumber'] = get_user_order($id);//sbsn 0702 未付款订单数
    $user['user_rank']   = $user['user_rank'];//sbsn 0702 增加AJAX属性
    $user['user_id']     = $id;//sbsn 0702 增加AJAX属性
    $user['username']    = $user['user_name'];
    $user['user_points'] = $user['pay_points'] . $GLOBALS['_CFG']['integral_name'];
    $user['user_money']  = price_format($user['user_money'], false);
    $user['user_bonus']  = price_format($bonus['bonus_value'], false);

    return $user;
}








/**
 * 取得当前位置和页面标题
 *
 * @access  public
 * @param   integer     $cat    分类编号（只有商品及分类、文章及分类用到）
 * @param   string      $str    商品名、文章标题或其他附加的内容（无链接）
 * @return  array
 */
function assign_ur_here($cat = 0, $str = '')
{
    /* 判断是否重写，取得文件名 */

    $cur_url = basename(PHP_SELF);
    if (intval($GLOBALS['_CFG']['rewrite']))
    {
        $filename = strpos($cur_url,'-') ? substr($cur_url, 0, strpos($cur_url,'-')) : substr($cur_url, 0, -4);
    }
    else
    {
        $filename = substr($cur_url, 0, -4);
    }

    /* 初始化“页面标题”和“当前位置” */
    $cur_lang = $GLOBALS['_CFG']['lang']; 
    if($cur_lang == 'zh_tw'){
        $shop_title = $GLOBALS['_CFG']['shop_title'];
    } else {
        $sql = "SELECT `value` FROM " . $GLOBALS['ecs']->table('shop_config_lang') . " WHERE `code` LIKE 'shop_title' AND `lang` LIKE '" .$cur_lang. "'";
        $shop_title = $GLOBALS['db']->getOne($sql);
    }
    $page_title = $shop_title;
    $ur_here    = '<a href="/">' . _L('global_home', '首頁') . '</a>';

    // Google structured data json-ld
    $ur_here_list = array();

    /* 根据文件名分别处理中间的部分 */
    if ($filename != 'index')
    {
        /* 处理有分类的 */
        if (in_array($filename, array('category', 'goods', 'article_cat', 'article', 'brand', 'topic')))
        {
            /* 商品分类或商品 */
            if ('category' == $filename || 'goods' == $filename || 'brand' == $filename)
            {
                if ($cat > 0)
                {
                    $cat_arr = get_parent_cats($cat);

                    $key     = 'cid';
                    $namekey = 'cname';
                    $type    = 'category';
                    $permakey = 'cperma';
                }
                else
                {
                    $cat_arr = array();
                }
            }
            /* 文章分类或文章 */
            elseif ('article_cat' == $filename || 'article' == $filename)
            {
                if ($cat > 0)
                {
                    $cat_arr = get_article_parent_cats($cat);
                    $end = end($cat_arr);
                    //If the parent is UPHELP_CAT, don't show in nav bar
                    if($end['cat_type'] == UPHELP_CAT) array_pop($cat_arr);
                    $key  = 'acid';
                    $namekey = 'acname';
                    $type = 'article_cat';
                    $permakey = 'acperma';
                }
                else
                {
                    $cat_arr = array();
                }
            }
            /* 推廣優惠 */
            elseif ('topic' == $filename)
            {
                $key = 'tcid';
                $namekey = 'tcname';
                $type = 'topic_cat';
                $permakey = 'tcperma';

                $cat_arr = array(array('cat_id' => 0, 'cat_name' => _L('topic_root', '推廣優惠')));
                if ($cat > 0)
                {
                    $sql = "SELECT IFNULL(tcl.topic_cat_name, tc.topic_cat_name) as topic_cat_name, $permakey " .
                            "FROM " . $GLOBALS['ecs']->table('topic_category') . " as tc " .
                            "LEFT JOIN " . $GLOBALS['ecs']->table('topic_category_lang') . " as tcl " .
                                "ON tcl.topic_cat_id = tc.topic_cat_id AND tcl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
                            "LEFT JOIN (SELECT id, table_name, lang, perma_link as $permakey FROM " . $GLOBALS['ecs']->table('perma_link') . ") tcpl ON tcpl.id = tc.topic_cat_id  AND tcpl.table_name = 'topic_category' AND tcpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
                            "WHERE tc.topic_cat_id = '" . $cat . "'";
                    $localized_topic_cat = $GLOBALS['db']->getRow($sql);
                    array_unshift($cat_arr, array('cat_id' => $cat, 'cat_name' => $localized_topic_cat['topic_cat_name'], $permakey => $localized_topic_cat[$permakey]));
                }
            }

            /* 循环分类 */
            if (!empty($cat_arr))
            {
                krsort($cat_arr);
                $last_cat_name = '';
                foreach ($cat_arr AS $val)
                {
                    if ($last_cat_name == $val['cat_name']) continue; // Added by howang
                    $page_title = htmlspecialchars($val['cat_name']) . ' - ' . $page_title;
                    $args       = array($key => $val['cat_id'], $namekey => $val['cat_name'], $permakey => $val[$permakey]);
                    $uri        = build_uri($type, $args, $val['cat_name']);
                    $ur_here   .= ' <code>&gt;</code> <a href="'. $uri . '">' .
                                    htmlspecialchars($val['cat_name']) . '</a>';
                    $ur_here_list[] = array('name' => $val['cat_name'], 'url' => desktop_url($uri));
                    $last_cat_name = $val['cat_name']; // Added by howang
                }
            }
        }
        /* 处理无分类的 */
        else
        {
            /* 团购 */
            if ('group_buy' == $filename)
            {
                $page_title = $GLOBALS['_LANG']['group_buy_goods'] . ' - ' . $page_title;
                $args       = array('gbid' => '0');
                $ur_here   .= ' <code>&gt;</code> <a href="group_buy.php">' .
                                $GLOBALS['_LANG']['group_buy_goods'] . '</a>';
                $ur_here_list[] = array('name' => $GLOBALS['_LANG']['group_buy_goods'], 'url' => desktop_url('group_buy.php'));
            }
            /* 拍卖 */
            elseif ('auction' == $filename)
            {
                $page_title = $GLOBALS['_LANG']['auction'] . ' - ' . $page_title;
                $args       = array('auid' => '0');
                $ur_here   .= ' <code>&gt;</code> <a href="auction.php">' .
                                $GLOBALS['_LANG']['auction'] . '</a>';
                $ur_here_list[] = array('name' => $GLOBALS['_LANG']['auction'], 'url' => desktop_url('auction.php'));
            }
            /* 夺宝 */
            elseif ('snatch' == $filename)
            {
                $page_title = $GLOBALS['_LANG']['snatch'] . ' - ' . $page_title;
                $args       = array('id' => '0');
                $ur_here   .= ' <code> &gt; </code><a href="snatch.php">' .
                                $GLOBALS['_LANG']['snatch_list'] . '</a>';
                $ur_here_list[] = array('name' => $GLOBALS['_LANG']['snatch_list'], 'url' => desktop_url('snatch.php'));
            }
            /* 批发 */
            elseif ('wholesale' == $filename)
            {
                $page_title = $GLOBALS['_LANG']['wholesale'] . ' - ' . $page_title;
                $args       = array('wsid' => '0');
                $ur_here   .= ' <code>&gt;</code> <a href="wholesale.php">' .
                                $GLOBALS['_LANG']['wholesale'] . '</a>';
                $ur_here_list[] = array('name' => $GLOBALS['_LANG']['wholesale'], 'url' => desktop_url('wholesale.php'));
            }
            /* 积分兑换 */
            elseif ('exchange' == $filename)
            {
                $page_title = $GLOBALS['_LANG']['exchange'] . ' - ' . $page_title;
                $args       = array('wsid' => '0');
                $ur_here   .= ' <code>&gt;</code> <a href="exchange.php">' .
                                $GLOBALS['_LANG']['exchange'] . '</a>';
                $ur_here_list[] = array('name' => $GLOBALS['_LANG']['exchange'], 'url' => desktop_url('exchange.php'));
            }
            /* VIP */
            elseif ('vip' == $filename)
            {
                $page_title = $GLOBALS['_LANG']['vip_offer'] . ' - ' . $page_title;
                $args       = array();
                $ur_here   .= ' <code>&gt;</code> <a href="vip.php">' .
                                $GLOBALS['_LANG']['vip_offer'] . '</a>';
                $ur_here_list[] = array('name' => $GLOBALS['_LANG']['vip_offer'], 'url' => desktop_url('vip.php'));
            }
            /* 其他的在这里补充 */
        }
    }

    /* 处理最后一部分 */
    if (!empty($str))
    {
        $page_title  = $str . ' - ' . $page_title;
        $ur_here    .= ' <code>&gt;</code> ' . $str;
        $ur_here_list[] = array('name' => $str, 'url' => desktop_url($_SERVER['REQUEST_URI']));
    }

    if (is_object($GLOBALS['smarty']))
    {
        $GLOBALS['smarty']->assign('ur_here_list', $ur_here_list);
    }

    /* 返回值 */
    return array('title' => $page_title, 'ur_here' => $ur_here);
}

/**
 * 获得指定分类的所有上级分类
 *
 * @access  public
 * @param   integer $cat    分类编号
 * @return  array
 */
function get_parent_cats($cat)
{
    if ($cat == 0)
    {
        return array();
    }

    $arr = $GLOBALS['db']->GetAll(
        'SELECT cat_id, cat_name, parent_id, cperma FROM ' . $GLOBALS['ecs']->table('category') . ' c ' .
        'LEFT JOIN (SELECT id, table_name, lang, perma_link as cperma FROM ' . $GLOBALS['ecs']->table('perma_link') . ') cpl ON cpl.id = c.cat_id  AND cpl.table_name = "category" AND cpl.lang = "' . $GLOBALS['_CFG']['lang'] . '"'
    );

    if (empty($arr))
    {
        return array();
    }

    $arr = localize_db_result('category', $arr);

    $index = 0;
    $cats  = array();

    while (1)
    {
        foreach ($arr AS $row)
        {
            if ($cat == $row['cat_id'])
            {
                $cat = $row['parent_id'];

                $cats[$index]['cat_id']   = $row['cat_id'];
                $cats[$index]['cat_name'] = $row['cat_name'];
                $cats[$index]['cperma']   = $row['cperma'];

                $index++;
                break;
            }
        }

        if ($index == 0 || $cat == 0)
        {
            break;
        }
    }

    return $cats;
}

/**
 * 根据提供的数组编译成页面标题
 *
 * @access  public
 * @param   string  $type   类型
 * @param   array   $arr    分类数组
 * @return  string
 */
function build_pagetitle($arr, $type = 'category')
{
    $str = '';

    foreach ($arr AS $val)
    {
        $str .= htmlspecialchars($val['cat_name']) . '_';
    }

    return $str;
}

/**
 * 根据提供的数组编译成当前位置
 *
 * @access  public
 * @param   string  $type   类型
 * @param   array   $arr    分类数组
 * @return  void
 */
function build_urhere($arr, $type = 'category')
{
    krsort($arr);

    $str = '';
    foreach ($arr AS $val)
    {
        switch ($type)
        {
            case 'category':
            case 'brand':
                $args = array('cid' => $val['cat_id']);
                break;
            case 'article_cat':
                $args = array('acid' => $val['cat_id']);
                break;
        }

        $str .= ' <code>&gt;</code> <a href="/'. build_uri($type, $args). '">' . htmlspecialchars($val['cat_name']) . '</a>';
    }

    return $str;
}

/**
 * 获得指定页面的动态内容
 *
 * @access  public
 * @param   string  $tmp    模板名称
 * @return  void
 */
function assign_dynamic($tmp)
{
    $sql = 'SELECT id, number, type FROM ' . $GLOBALS['ecs']->table('template') .
        " WHERE filename = '$tmp' AND type > 0 AND remarks ='' AND theme='" . $GLOBALS['_CFG']['template'] . "'";
    $res = $GLOBALS['db']->getAll($sql);

   foreach ($res AS $row)
    {
        switch ($row['type'])
        {
            case 1:
                /* 分类下的商品 */
                $GLOBALS['smarty']->assign('goods_cat_' . $row['id'], assign_cat_goods($row['id'], $row['number']));
            break;
            case 2:
                /* 品牌的商品 */
                $brand_goods = assign_brand_goods($row['id'], $row['number']);

                $GLOBALS['smarty']->assign('brand_goods_' . $row['id'], $brand_goods['goods']);
                $GLOBALS['smarty']->assign('goods_brand_' . $row['id'], $brand_goods['brand']);
            break;
            case 3:
                /* 文章列表 */
                $cat_articles = assign_articles($row['id'], $row['number']);

                $GLOBALS['smarty']->assign('articles_cat_' . $row['id'], $cat_articles['cat']);
                $GLOBALS['smarty']->assign('articles_' . $row['id'], $cat_articles['arr']);
            break;
        }
    }
}

/**
 * 分配文章列表给smarty
 *
 * @access  public
 * @param   integer     $id     文章分类的编号
 * @param   integer     $num    文章数量
 * @return  array
 */
function assign_articles($id, $num)
{
    $sql = 'SELECT cat_name FROM ' . $GLOBALS['ecs']->table('article_cat') . " WHERE cat_id = '" . $id ."'";

    $cat['id']       = $id;
    $cat['name']     = $GLOBALS['db']->getOne($sql);
    $cat['url']      = build_uri('article_cat', array('acid' => $id), $cat['name']);

    $articles['cat'] = $cat;
    $articles['arr'] = get_cat_articles($id, 1, $num);

    return $articles;
}

/**
 * 分配帮助信息
 *
 * @access  public
 * @return  array
 */
function get_shop_help()
{
    $cache_name = 'shop_help_' . $GLOBALS['_CFG']['lang'];
    $data = read_static_cache($cache_name);
    if ($data !== false)
    {
        return $data;
    }

    $sql = 'SELECT c.cat_id, c.cat_name, c.cat_icon, c.sort_order, c.cat_type, a.article_id, a.title, a.file_url, a.open_type ' .
            'FROM ' .$GLOBALS['ecs']->table('article'). ' AS a ' .
            'LEFT JOIN ' .$GLOBALS['ecs']->table('article_cat'). ' AS c ' .
            'ON a.cat_id = c.cat_id WHERE c.show_in_footer = 1 AND a.is_open = 1 ' .
            'ORDER BY c.sort_order ASC, a.article_type DESC, a.add_time DESC';
    $res = $GLOBALS['db']->getAll($sql);
    $res = localize_db_result('article', $res);
    $res = localize_db_result('article_cat', $res);

    $arr = array();
    foreach ($res as $key => $row)
    {
        $arr[$row['cat_id']]['cat_id']                       = build_uri('article_cat', array('acid'=> $row['cat_id'], 'acname' => $row['cat_name']), $row['cat_name']);
        $arr[$row['cat_id']]['cat_name']                     = $row['cat_name'];
        $arr[$row['cat_id']]['cat_icon']  =  $row['cat_icon']  ?  '/data/afficheimg/cat_icon/'. $row['cat_icon'] : '/yohohk/img/article-default-menu-icon.png';
        $arr[$row['cat_id']]['article'][$key]['article_id']  = $row['article_id'];
        $arr[$row['cat_id']]['article'][$key]['title']       = $row['title'];
        $arr[$row['cat_id']]['article'][$key]['short_title'] = $GLOBALS['_CFG']['article_title_length'] > 0 ?
            sub_str($row['title'], $GLOBALS['_CFG']['article_title_length']) : $row['title'];
        $arr[$row['cat_id']]['article'][$key]['url']         = $row['open_type'] != 1 ?
            build_uri('article', array('aid' => $row['article_id'], 'aname' => $row['title']), $row['title']) : trim($row['file_url']);
        if($row['cat_type'] == SUPPORT_CAT) {
            $arr[$row['cat_id']]['article'][$key]['url']  = '/support/answer/'.$row['article_id'].'-'.$row['title'].'/';
        }
    }

    foreach ($arr as $key => $row){
        $arr[$key]['article'] = array_slice($row['article'], 0, 5);
    }

    write_static_cache($cache_name, $arr);

    return $arr;
}

/**
 * 创建分页信息
 *
 * @access  public
 * @param   string  $app            程序名称，如category
 * @param   string  $cat            分类ID
 * @param   string  $record_count   记录总数
 * @param   string  $size           每页记录数
 * @param   string  $sort           排序类型
 * @param   string  $order          排序顺序
 * @param   string  $page           当前页
 * @param   string  $keywords       查询关键字
 * @param   string  $brand          品牌
 * @param   string  $price_min      最小价格
 * @param   string  $price_max      最高价格
 * @return  void
 */
function assign_pager($app, $cat, $record_count, $size, $sort, $order, $page = 1,
                        $keywords = '', $brand = 0, $price_min = 0, $price_max = 0, $display_type = 'list', $filter_attr='', $url_format='', $sch_array='', $permas = [])
{
    $sch = array('keywords'  => $keywords,
                 'sort'      => $sort,
                 'order'     => $order,
                 'cat'       => $cat,
                 'brand'     => $brand,
                 'price_min' => $price_min,
                 'price_max' => $price_max,
                 'filter_attr'=>$filter_attr,
                 'display'   => $display_type
        );

    $page = intval($page);
    if ($page < 1)
    {
        $page = 1;
    }

    $page_count = $record_count > 0 ? intval(ceil($record_count / $size)) : 1;

    $pager['page']         = $page;
    $pager['size']         = $size;
    $pager['sort']         = $sort;
    $pager['order']        = $order;
    $pager['record_count'] = $record_count;
    $pager['page_count']   = $page_count;
    $pager['display']      = $display_type;

    switch ($app)
    {
        case 'category':
            $uri_args = array('cid' => $cat, 'cperma' => $permas['cperma'], 'bid' => $brand, 'bperma' => $permas['bperma'], 'price_min'=>$price_min, 'price_max'=>$price_max, 'filter_attr'=>$filter_attr, 'sort' => $sort, 'order' => $order, 'display' => $display_type, 'top' => 1);
            break;
        case 'article_cat':
            $uri_args = array('acid' => $cat, 'acperma' => $permas['acperma'], 'sort' => $sort, 'order' => $order);
            break;
        case 'brand':
            $uri_args = array('cid' => $cat, 'cperma' => $permas['cperma'], 'bid' => $brand, 'bperma' => $permas['bperma'], 'sort' => $sort, 'order' => $order, 'display' => $display_type);
            break;
        case 'search':
            $uri_args = array('cid' => $cat, 'cperma' => $permas['cperma'], 'bid' => $brand, 'bperma' => $permas['bperma'], 'sort' => $sort, 'order' => $order);
            break;
        case 'exchange':
            $uri_args = array('cid' => $cat, 'cperma' => $permas['cperma'], 'integral_min'=>$price_min, 'integral_max'=>$price_max, 'sort' => $sort, 'order' => $order, 'display' => $display_type);
        case 'flashdeal':
            $uri_args = array();
            break;
		case 'vip':
			$uri_args = array('cid' => $cat, 'cperma' => $permas['cperma'], 'bid' => $brand, 'bperma' => $permas['bperma'], 'price_min'=>$price_min, 'price_max'=>$price_max, 'sort' => $sort, 'order' => $order, 'display' => $display_type);
			break;
		case 'programs':
			$uri_args = array('cid' => $cat, 'cperma' => $permas['cperma'], 'bid' => $brand, 'bperma' => $permas['bperma'], 'price_min'=>$price_min, 'price_max'=>$price_max, 'sort' => $sort, 'order' => $order, 'display' => $display_type);
			break;
    }
    /* 分页样式 */
    $pager['styleid'] = isset($GLOBALS['_CFG']['page_style'])? intval($GLOBALS['_CFG']['page_style']) : 0;

    $page_prev  = ($page > 1) ? $page - 1 : 1;
    $page_next  = ($page < $page_count) ? $page + 1 : $page_count;
    if ($pager['styleid'] == 0)
    {
        if (!empty($url_format))
        {
            $pager['page_first'] = $url_format . 1;
            $pager['page_prev']  = $url_format . $page_prev;
            $pager['page_next']  = $url_format . $page_next;
            $pager['page_last']  = $url_format . $page_count;
        }
        else
        {
            $pager['page_first'] = build_uri($app, $uri_args, '', 1, $keywords);
            $pager['page_prev']  = build_uri($app, $uri_args, '', $page_prev, $keywords);
            $pager['page_next']  = build_uri($app, $uri_args, '', $page_next, $keywords);
            $pager['page_last']  = build_uri($app, $uri_args, '', $page_count, $keywords);
        }
        $pager['array']      = array();

        for ($i = 1; $i <= $page_count; $i++)
        {
            $pager['array'][$i] = $i;
        }
    }
    else
    {
        $_pagenum = 5; //defined('YOHO_MOBILE') ? 5 : 10;     // 显示的页码
        $_offset = 2;       // 当前页偏移值
        $_from = $_to = 0;  // 开始页, 结束页
        if($_pagenum > $page_count)
        {
            $_from = 1;
            $_to = $page_count;
        }
        else
        {
            $_from = $page - $_offset;
            $_to = $_from + $_pagenum - 1;
            if($_from < 1)
            {
                $_to = $page + 1 - $_from;
                $_from = 1;
                if($_to - $_from < $_pagenum)
                {
                    $_to = $_pagenum;
                }
            }
            elseif($_to > $page_count)
            {
                $_from = $page_count - $_pagenum + 1;
                $_to = $page_count;
            }
        }
        if (!empty($url_format))
        {
            $pager['page_first'] = ($page - $_offset > 1 && $_pagenum < $page_count) ? $url_format . 1 : '';
            $pager['page_prev']  = ($page > 1) ? $url_format . $page_prev : '';
            $pager['page_next']  = ($page < $page_count) ? $url_format . $page_next : '';
            $pager['page_last']  = ($_to < $page_count) ? $url_format . $page_count : '';
            $pager['page_kbd']  = ($_pagenum < $page_count) ? true : false;
            $pager['page_number'] = array();
            for ($i=$_from;$i<=$_to;++$i)
            {
                $pager['page_number'][$i] = $url_format . $i;
            }
        }
        else
        {
            $pager['page_first'] = ($page - $_offset > 1 && $_pagenum < $page_count) ? build_uri($app, $uri_args, '', 1, $keywords) : '';
            $pager['page_prev']  = ($page > 1) ? build_uri($app, $uri_args, '', $page_prev, $keywords) : '';
            $pager['page_next']  = ($page < $page_count) ? build_uri($app, $uri_args, '', $page_next, $keywords) : '';
            $pager['page_last']  = ($_to < $page_count) ? build_uri($app, $uri_args, '', $page_count, $keywords) : '';
            $pager['page_kbd']  = ($_pagenum < $page_count) ? true : false;
            $pager['page_number'] = array();
            for ($i=$_from;$i<=$_to;++$i)
            {
                $pager['page_number'][$i] = build_uri($app, $uri_args, '', $i, $keywords);
            }
        }
    }
    if (!empty($sch_array))
    {
        $pager['search'] = $sch_array;
    }
    else
    {
        $pager['search']['category'] = $cat;
        foreach ($sch AS $key => $row)
        {
            $pager['search'][$key] = $row;
        }
    }

    $GLOBALS['smarty']->assign('pager', $pager);
}

/**
 *  生成给pager.lbi赋值的数组
 *
 * @access  public
 * @param   string      $url        分页的链接地址(必须是带有参数的地址，若不是可以伪造一个无用参数)
 * @param   array       $param      链接参数 key为参数名，value为参数值
 * @param   int         $record     记录总数量
 * @param   int         $page       当前页数
 * @param   int         $size       每页大小
 *
 * @return  array       $pager
 */
function get_pager($url, $param, $record_count, $page = 1, $size = 10)
{
    $size = intval($size);
    if ($size < 1)
    {
        $size = 10;
    }

    $page = intval($page);
    if ($page < 1)
    {
        $page = 1;
    }

    $record_count = intval($record_count);

    $page_count = $record_count > 0 ? intval(ceil($record_count / $size)) : 1;
    if ($page > $page_count)
    {
        $page = $page_count;
    }
    /* 分页样式 */
    $pager['styleid'] = isset($GLOBALS['_CFG']['page_style'])? intval($GLOBALS['_CFG']['page_style']) : 0;

    $page_prev  = ($page > 1) ? $page - 1 : 1;
    $page_next  = ($page < $page_count) ? $page + 1 : $page_count;

    /* 将参数合成url字串 */
    $param_url = '?';
    foreach ($param AS $key => $value)
    {
        $param_url .= $key . '=' . $value . '&';
    }

    $pager['url']          = $url;
    $pager['start']        = ($page -1) * $size;
    $pager['page']         = $page;
    $pager['size']         = $size;
    $pager['record_count'] = $record_count;
    $pager['page_count']   = $page_count;

    if ($pager['styleid'] == 0)
    {
        $pager['page_first']   = $url . $param_url . 'page=1';
        $pager['page_prev']    = $url . $param_url . 'page=' . $page_prev;
        $pager['page_next']    = $url . $param_url . 'page=' . $page_next;
        $pager['page_last']    = $url . $param_url . 'page=' . $page_count;
        $pager['array']  = array();
        for ($i = 1; $i <= $page_count; $i++)
        {
            $pager['array'][$i] = $i;
        }
    }
    else
    {
        $_pagenum = 10;     // 显示的页码
        $_offset = 2;       // 当前页偏移值
        $_from = $_to = 0;  // 开始页, 结束页
        if($_pagenum > $page_count)
        {
            $_from = 1;
            $_to = $page_count;
        }
        else
        {
            $_from = $page - $_offset;
            $_to = $_from + $_pagenum - 1;
            if($_from < 1)
            {
                $_to = $page + 1 - $_from;
                $_from = 1;
                if($_to - $_from < $_pagenum)
                {
                    $_to = $_pagenum;
                }
            }
            elseif($_to > $page_count)
            {
                $_from = $page_count - $_pagenum + 1;
                $_to = $page_count;
            }
        }
        $url_format = $url . $param_url . 'page=';
        $pager['page_first'] = ($page - $_offset > 1 && $_pagenum < $page_count) ? $url_format . 1 : '';
        $pager['page_prev']  = ($page > 1) ? $url_format . $page_prev : '';
        $pager['page_next']  = ($page < $page_count) ? $url_format . $page_next : '';
        $pager['page_last']  = ($_to < $page_count) ? $url_format . $page_count : '';
        $pager['page_kbd']  = ($_pagenum < $page_count) ? true : false;
        $pager['page_number'] = array();
        for ($i=$_from;$i<=$_to;++$i)
        {
            $pager['page_number'][$i] = $url_format . $i;
        }
    }
    $pager['search'] = $param;

    return $pager;
}

/**
 * 调用调查内容
 *
 * @access  public
 * @param   integer $id   调查的编号
 * @return  array
 */
function get_vote($id = '')
{
    /* 随机取得一个调查的主题 */
    if (empty($id))
    {
        $time = gmtime();
        $sql = 'SELECT vote_id, vote_name, can_multi, vote_count, RAND() AS rnd' .
               ' FROM ' . $GLOBALS['ecs']->table('vote') .
               " WHERE start_time <= '$time' AND end_time >= '$time' ".
               ' ORDER BY rnd LIMIT 1';
    }
    else
    {
        $sql = 'SELECT vote_id, vote_name, can_multi, vote_count' .
               ' FROM ' . $GLOBALS['ecs']->table('vote').
               " WHERE vote_id = '$id'";
    }

    $vote_arr = $GLOBALS['db']->getRow($sql);

    if ($vote_arr !== false && !empty($vote_arr))
    {
        /* 通过调查的ID,查询调查选项 */
        $sql_option = 'SELECT v.*, o.option_id, o.vote_id, o.option_name, o.option_count ' .
                      'FROM ' . $GLOBALS['ecs']->table('vote') . ' AS v, ' .
                            $GLOBALS['ecs']->table('vote_option') . ' AS o ' .
                      "WHERE o.vote_id = v.vote_id AND o.vote_id = '$vote_arr[vote_id]' ORDER BY o.option_order ASC, o.option_id DESC";
        $res = $GLOBALS['db']->getAll($sql_option);

        /* 总票数 */
        $sql = 'SELECT SUM(option_count) AS all_option FROM ' . $GLOBALS['ecs']->table('vote_option') .
               " WHERE vote_id = '" . $vote_arr['vote_id'] . "' GROUP BY vote_id";
        $option_num = $GLOBALS['db']->getOne($sql);

        $arr = array();
        $count = 100;
        foreach ($res AS $idx => $row)
        {
            if ($option_num > 0 && $idx == count($res) - 1)
            {
                $percent = $count;
            }
            else
            {
                $percent = ($row['vote_count'] > 0 && $option_num > 0) ? round(($row['option_count'] / $option_num) * 100) : 0;

                $count -= $percent;
            }
            $arr[$row['vote_id']]['options'][$row['option_id']]['percent'] = $percent;

            $arr[$row['vote_id']]['vote_id']    = $row['vote_id'];
            $arr[$row['vote_id']]['vote_name']  = $row['vote_name'];
            $arr[$row['vote_id']]['can_multi']  = $row['can_multi'];
            $arr[$row['vote_id']]['vote_count'] = $row['vote_count'];

            $arr[$row['vote_id']]['options'][$row['option_id']]['option_id']    = $row['option_id'];
            $arr[$row['vote_id']]['options'][$row['option_id']]['option_name']  = $row['option_name'];
            $arr[$row['vote_id']]['options'][$row['option_id']]['option_count'] = $row['option_count'];
        }

        $vote_arr['vote_id'] = (!empty($vote_arr['vote_id'])) ? $vote_arr['vote_id'] : '';

        $vote = array('id' => $vote_arr['vote_id'], 'content' => $arr);

        return $vote;
    }
}

/**
 * 获得浏览器名称和版本
 *
 * @access  public
 * @return  string
 */
function get_user_browser()
{
    if (empty($_SERVER['HTTP_USER_AGENT']))
    {
        return '';
    }

    $agent       = $_SERVER['HTTP_USER_AGENT'];
    $browser     = '';
    $browser_ver = '';

    // Mozilla Firefox
    if (preg_match('/FireFox\/([^\s]+)/i', $agent, $regs))
    {
        $browser     = 'FireFox';
        $browser_ver = $regs[1];
    }
    // IE 12 pretent to be Chrome, but we can detect "Edge"
    elseif (preg_match('/Chrome\/[0-9.]+ Safari\/[0-9.]+ Edge\/([^\s]+)/i', $agent, $regs))
    {
        $browser     = 'Internet Explorer';
        $browser_ver = $regs[1];
    }
    // Opera 15 or above pretent to be Chrome, but we can detect "OPR"
    elseif (preg_match('/OPR\/([^\s]+)/i', $agent, $regs))
    {
        $browser     = 'Opera';
        $browser_ver = $regs[1];
    }
    // Google Chrome must be detected before Safari (as Chrome is a fork of Safari)
    elseif (preg_match('/Chrome\/([^\s]+)/i', $agent, $regs))
    {
        $browser     = 'Chrome';
        $browser_ver = $regs[1];
    }
    // Apple Safari
    elseif (preg_match('/Safari\/([^\s]+)/i', $agent, $regs))
    {
        $browser     = 'Safari';
        $browser_ver = $regs[1];
    }
    // WebKit-based browser (besides Safari and Chrome)
    elseif (preg_match('/AppleWebKit\/([^\s]+)/i', $agent, $regs))
    {
        $browser     = 'WebKit';
        $browser_ver = $regs[1];
    }
    // Android stock browser
    elseif (preg_match('/Dalvik\/([^\s]+)/i', $agent, $regs))
    {
        $browser     = 'Dalvik';
        $browser_ver = $regs[1];
    }
    // Opera 14 or below
    elseif (preg_match('/Opera[\s|\/]([^\s]+)/i', $agent, $regs))
    {
        $browser     = 'Opera';
        $browser_ver = $regs[1];
    }
    // China browsers
    elseif (preg_match('/UCBrowser\/([^\s]+)/i', $agent, $regs))
    {
        $browser     = 'UCBrowser';
        $browser_ver = $regs[1];
    }
    elseif (preg_match('/UCWEB\/([^\s]+)/i', $agent, $regs))
    {
        $browser     = 'UCWEB';
        $browser_ver = $regs[1];
    }
    // IE-based browser
    elseif (preg_match('/Maxthon/i', $agent, $regs))
    {
        $browser     = '(Internet Explorer ' .$browser_ver. ') Maxthon';
        $browser_ver = '';
    }
    elseif (preg_match('/NetCaptor\s([^\s|;]+)/i', $agent, $regs))
    {
        $browser     = '(Internet Explorer ' .$browser_ver. ') NetCaptor';
        $browser_ver = $regs[1];
    }
    // IE 10 or below
    elseif (preg_match('/MSIE\s([^\s|;]+)/i', $agent, $regs))
    {
        $browser     = 'Internet Explorer';
        $browser_ver = $regs[1];
    }
    // IE 11 don't have "MSIE" anymore, but we can detect "Trident"
    elseif (preg_match('/Trident\/.*rv:([0-9.]+)/i', $agent, $regs))
    {
        $browser     = 'Internet Explorer';
        $browser_ver = $regs[1];
    }
    // Legacy browsers
    elseif (preg_match('/OmniWeb\/(v*)([^\s|;]+)/i', $agent, $regs))
    {
        $browser     = 'OmniWeb';
        $browser_ver = $regs[2];
    }
    elseif (preg_match('/Netscape([\d]*)\/([^\s]+)/i', $agent, $regs))
    {
        $browser     = 'Netscape';
        $browser_ver = $regs[2];
    }
    // Command-line browsers
    elseif (preg_match('/Lynx\/([^\s]+)/i', $agent, $regs))
    {
        $browser     = 'Lynx';
        $browser_ver = $regs[1];
    }
    elseif (preg_match('/curl\/([^\s]+)/i', $agent, $regs))
    {
        $browser     = 'curl';
        $browser_ver = $regs[1];
    }
    elseif (preg_match('/wget\/([^\s]+)/i', $agent, $regs))
    {
        $browser     = 'wget';
        $browser_ver = $regs[1];
    }

    if (!empty($browser))
    {
       return addslashes($browser . ' ' . $browser_ver);
    }
    else
    {
        //return 'Unknow browser';
        return $agent;
    }
}

/**
 * 判断是否为搜索引擎蜘蛛
 *
 * @access  public
 * @return  string
 */
function is_spider($record = true)
{
    static $spider = NULL;

    if ($spider !== NULL)
    {
        return $spider;
    }

    if (empty($_SERVER['HTTP_USER_AGENT']))
    {
        $spider = '';

        return '';
    }

    $searchengine_bot = array(
        'googlebot',
        'mediapartners-google',
        'adsbot-google',
        'baiduspider+',
        'baiduspider',
        'msnbot',
        'bingbot',
        'yodaobot',
        'yahoo! slurp;',
        'yahoo! slurp china;',
        'iaskspider',
        'sogou web spider',
        'sogou push spider',
        'facebookexternalhit',
        'facebot',
        'ia_archiver',
        'yandexbot',
        'mj12bot',
        'blexbot',
        'linkdexbot',
        'dotbot'
    );

    $searchengine_name = array(
        'Google',
        'Google AdSense',
        'Google AdWords',
        'Baidu',
        'Baidu',
        'MSN',
        'Bing',
        'YoDao',
        'Yahoo',
        'Yahoo China',
        'iAsk',
        'SoGou',
        'SoGou',
        'Facebook',
        'Facebook',
        'Alexa',
        'Yandex',
        'Majestic-12',
        'WebMeUp',
        'Linkdex',
        'SEOmoz'
    );

    $spider = strtolower($_SERVER['HTTP_USER_AGENT']);

    foreach ($searchengine_bot AS $key => $value)
    {
        if (strpos($spider, $value) !== false)
        {
            $spider = $searchengine_name[$key];

            if ($record === true)
            {
                $GLOBALS['db']->autoReplace($GLOBALS['ecs']->table('searchengine'), array('date' => local_date('Y-m-d'), 'searchengine' => $spider, 'count' => 1), array('count' => 1));
            }

            return $spider;
        }
    }

    $spider = '';

    return '';
}

/**
 * 获得客户端的操作系统
 *
 * @access  private
 * @return  void
 */
function get_os()
{
    if (empty($_SERVER['HTTP_USER_AGENT']))
    {
        return 'Unknown';
    }

    $agent = strtolower($_SERVER['HTTP_USER_AGENT']);
    $os    = '';

    if (strpos($agent, 'win') !== false)
    {
        if (strpos($agent, 'nt 5.1') !== false)
        {
            $os = 'Windows XP';
        }
        elseif (strpos($agent, 'nt 5.2') !== false)
        {
            $os = 'Windows 2003';
        }
        elseif (strpos($agent, 'nt 5.0') !== false)
        {
            $os = 'Windows 2000';
        }
        elseif (strpos($agent, 'nt 6.0') !== false)
        {
            $os = 'Windows Vista';
        }
        elseif (strpos($agent, 'nt 6.1') !== false)
        {
            $os = 'Windows 7';
        }
        elseif (strpos($agent, 'nt 6.2') !== false)
        {
            $os = 'Windows 8';
        }
        elseif (strpos($agent, 'nt 6.3') !== false)
        {
            $os = 'Windows 8.1';
        }
        elseif (strpos($agent, 'nt 10.0') !== false)
        {
            $os = 'Windows 10';
        }
        elseif (strpos($agent, 'nt') !== false)
        {
            $os = 'Windows NT';
        }
        elseif (strpos($agent, 'win 9x') !== false && strpos($agent, '4.90') !== false)
        {
            $os = 'Windows ME';
        }
        elseif (strpos($agent, '98') !== false)
        {
            $os = 'Windows 98';
        }
        elseif (strpos($agent, '95') !== false)
        {
            $os = 'Windows 95';
        }
        elseif (strpos($agent, '32') !== false)
        {
            $os = 'Windows 32';
        }
        elseif (strpos($agent, 'ce') !== false)
        {
            $os = 'Windows CE';
        }
    }
    // iOS have the string "like Mac OS X", have to detect before Mac
    elseif (strpos($agent, 'iphone') !== false || strpos($agent, 'ipad') !== false || strpos($agent, 'ipod') !== false)
    {
        $os = 'iOS';
        if (preg_match('/OS ([0-9_]+) like Mac OS X/i', $agent, $matches))
        {
            $os .= ' ' . str_replace('_', '.', $matches[1]);
        }
    }
    elseif (strpos($agent, 'mac os x') !== false)
    {
        $os = 'Mac OS X';
        if (preg_match('/Mac OS X ([0-9_]+)/i', $agent, $matches))
        {
            $os .= ' ' . str_replace('_', '.', $matches[1]);
        }
    }
    elseif (strpos($agent, 'android') !== false)
    {
        $os = 'Android';
        if (preg_match('/Android ([0-9.]+)/i', $agent, $matches))
        {
            $os .= ' ' . $matches[1];
        }
    }
    elseif (strpos($agent, 'linux') !== false)
    {
        $os = 'Linux';
    }
    elseif (strpos($agent, 'unix') !== false)
    {
        $os = 'Unix';
    }
    elseif (strpos($agent, 'sun') !== false && strpos($agent, 'os') !== false)
    {
        $os = 'SunOS';
    }
    elseif (strpos($agent, 'ibm') !== false && strpos($agent, 'os') !== false)
    {
        $os = 'IBM OS/2';
    }
    elseif (strpos($agent, 'mac') !== false && strpos($agent, 'pc') !== false)
    {
        $os = 'Macintosh';
    }
    elseif (strpos($agent, 'powerpc') !== false)
    {
        $os = 'PowerPC';
    }
    elseif (strpos($agent, 'aix') !== false)
    {
        $os = 'AIX';
    }
    elseif (strpos($agent, 'hpux') !== false)
    {
        $os = 'HPUX';
    }
    elseif (strpos($agent, 'netbsd') !== false)
    {
        $os = 'NetBSD';
    }
    elseif (strpos($agent, 'bsd') !== false)
    {
        $os = 'BSD';
    }
    elseif (strpos($agent, 'osf1') !== false)
    {
        $os = 'OSF1';
    }
    elseif (strpos($agent, 'irix') !== false)
    {
        $os = 'IRIX';
    }
    elseif (strpos($agent, 'freebsd') !== false)
    {
        $os = 'FreeBSD';
    }
    elseif (strpos($agent, 'teleport') !== false)
    {
        $os = 'teleport';
    }
    elseif (strpos($agent, 'flashget') !== false)
    {
        $os = 'flashget';
    }
    elseif (strpos($agent, 'webzip') !== false)
    {
        $os = 'webzip';
    }
    elseif (strpos($agent, 'offline') !== false)
    {
        $os = 'offline';
    }
    else
    {
        $os = 'Unknown';
    }

    return $os;
}

/**
 * 统计访问信息
 *
 * @access  public
 * @return  void
 */
function visit_stats()
{
    if (isset($GLOBALS['_CFG']['visit_stats']) && $GLOBALS['_CFG']['visit_stats'] == 'off')
    {
        return;
    }
    $ip = real_ip();
    // Don't save stats if connecting from the server itself (most probably web cron)
    if ($ip == $_SERVER['SERVER_ADDR'] || $ip == '127.0.0.1')
    {
        return;
    }

    $time = gmtime();
    /* 检查客户端是否存在访问统计的cookie */
    $visit_times = (!empty($_COOKIE['ECS']['visit_times'])) ? intval($_COOKIE['ECS']['visit_times']) + 1 : 1;
    setcookie('ECS[visit_times]', $visit_times, $time + 86400 * 365, '/');

    $browser  = get_user_browser();
    $os       = get_os();
    //$ip       = real_ip();
    $area     = ecs_geoip($ip);

    /* 语言 */
    if (!empty($_SERVER['HTTP_ACCEPT_LANGUAGE']))
    {
        $pos  = strpos($_SERVER['HTTP_ACCEPT_LANGUAGE'], ';');
        $lang = addslashes(($pos !== false) ? substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, $pos) : $_SERVER['HTTP_ACCEPT_LANGUAGE']);
    }
    else
    {
        $lang = '';
    }

    /* 来源 */
    if (!empty($_SERVER['HTTP_REFERER']) && strlen($_SERVER['HTTP_REFERER']) > 9)
    {
        $pos = strpos($_SERVER['HTTP_REFERER'], '/', 9);
        if ($pos !== false)
        {
            $domain = substr($_SERVER['HTTP_REFERER'], 0, $pos);
            $path   = substr($_SERVER['HTTP_REFERER'], $pos);

            /* 来源关键字 */
            if (!empty($domain) && !empty($path))
            {
                save_searchengine_keyword($domain, $path);
            }
        }
        else
        {
            $domain = $path = '';
        }
    }
    else
    {
        $domain = $path = '';
    }

    // Don't save stats if referring from our own host
    if (strpos($domain, $GLOBALS['ecs']->get_host()) !== false)
    {
        return;
    }

    $sql = 'INSERT INTO ' . $GLOBALS['ecs']->table('stats') . ' ( ' .
                'ip_address, visit_times, browser, system, language, area, ' .
                'referer_domain, referer_path, access_url, access_time' .
            ') VALUES (' .
                "'$ip', '$visit_times', '" . addslashes($browser) . "', '" . addslashes($os) . "', '" . addslashes($lang) . "', '" . addslashes($area) . "', ".
                "'" . addslashes($domain) ."', '" . addslashes($path) ."', '" . addslashes(PHP_SELF) ."', '" . $time . "')";
    $GLOBALS['db']->query($sql);
}

/**
 * 保存搜索引擎关键字
 *
 * @access  public
 * @return  void
 */
function save_searchengine_keyword($domain, $path)
{
    if (strpos($domain, 'google.com.tw') !== false && preg_match('/q=([^&]*)/i', $path, $regs))
    {
        $searchengine = 'GOOGLE TAIWAN';
        $keywords = urldecode($regs[1]); // google taiwan
    }
    if (strpos($domain, 'google.cn') !== false && preg_match('/q=([^&]*)/i', $path, $regs))
    {
        $searchengine = 'GOOGLE CHINA';
        $keywords = urldecode($regs[1]); // google china
    }
    if (strpos($domain, 'google.com') !== false && preg_match('/q=([^&]*)/i', $path, $regs))
    {
        $searchengine = 'GOOGLE';
        $keywords = urldecode($regs[1]); // google
    }
    elseif (strpos($domain, 'baidu.') !== false && preg_match('/wd=([^&]*)/i', $path, $regs))
    {
        $searchengine = 'BAIDU';
        $keywords = urldecode($regs[1]); // baidu
    }
    elseif (strpos($domain, 'baidu.') !== false && preg_match('/word=([^&]*)/i', $path, $regs))
    {
        $searchengine = 'BAIDU';
        $keywords = urldecode($regs[1]); // baidu
    }
    elseif (strpos($domain, '114.vnet.cn') !== false && preg_match('/kw=([^&]*)/i', $path, $regs))
    {
        $searchengine = 'CT114';
        $keywords = urldecode($regs[1]); // ct114
    }
    elseif (strpos($domain, 'iask.com') !== false && preg_match('/k=([^&]*)/i', $path, $regs))
    {
        $searchengine = 'IASK';
        $keywords = urldecode($regs[1]); // iask
    }
    elseif (strpos($domain, 'soso.com') !== false && preg_match('/w=([^&]*)/i', $path, $regs))
    {
        $searchengine = 'SOSO';
        $keywords = urldecode($regs[1]); // soso
    }
    elseif (strpos($domain, 'sogou.com') !== false && preg_match('/query=([^&]*)/i', $path, $regs))
    {
        $searchengine = 'SOGOU';
        $keywords = urldecode($regs[1]); // sogou
    }
    elseif (strpos($domain, 'so.163.com') !== false && preg_match('/q=([^&]*)/i', $path, $regs))
    {
        $searchengine = 'NETEASE';
        $keywords = urldecode($regs[1]); // netease
    }
    elseif (strpos($domain, 'yodao.com') !== false && preg_match('/q=([^&]*)/i', $path, $regs))
    {
        $searchengine = 'YODAO';
        $keywords = urldecode($regs[1]); // yodao
    }
    elseif (strpos($domain, 'zhongsou.com') !== false && preg_match('/word=([^&]*)/i', $path, $regs))
    {
        $searchengine = 'ZHONGSOU';
        $keywords = urldecode($regs[1]); // zhongsou
    }
    elseif (strpos($domain, 'search.tom.com') !== false && preg_match('/w=([^&]*)/i', $path, $regs))
    {
        $searchengine = 'TOM';
        $keywords = urldecode($regs[1]); // tom
    }
    elseif (strpos($domain, 'live.com') !== false && preg_match('/q=([^&]*)/i', $path, $regs))
    {
        $searchengine = 'MSLIVE';
        $keywords = urldecode($regs[1]); // MSLIVE
    }
    elseif (strpos($domain, 'tw.search.yahoo.com') !== false && preg_match('/p=([^&]*)/i', $path, $regs))
    {
        $searchengine = 'YAHOO TAIWAN';
        $keywords = urldecode($regs[1]); // yahoo taiwan
    }
    elseif (strpos($domain, 'cn.yahoo.') !== false && preg_match('/p=([^&]*)/i', $path, $regs))
    {
        $searchengine = 'YAHOO CHINA';
        $keywords = urldecode($regs[1]); // yahoo china
    }
    elseif (strpos($domain, 'yahoo.') !== false && preg_match('/p=([^&]*)/i', $path, $regs))
    {
        $searchengine = 'YAHOO';
        $keywords = urldecode($regs[1]); // yahoo
    }
    elseif (strpos($domain, 'msn.com.tw') !== false && preg_match('/q=([^&]*)/i', $path, $regs))
    {
        $searchengine = 'MSN TAIWAN';
        $keywords = urldecode($regs[1]); // msn taiwan
    }
    elseif (strpos($domain, 'msn.com.cn') !== false && preg_match('/q=([^&]*)/i', $path, $regs))
    {
        $searchengine = 'MSN CHINA';
        $keywords = urldecode($regs[1]); // msn china
    }
    elseif (strpos($domain, 'msn.com') !== false && preg_match('/q=([^&]*)/i', $path, $regs))
    {
        $searchengine = 'MSN';
        $keywords = urldecode($regs[1]); // msn
    }

    if (!empty($keywords))
    {
        $gb_search = array('YAHOO CHINA', 'TOM', 'ZHONGSOU', 'NETEASE', 'SOGOU', 'SOSO', 'IASK', 'CT114', 'BAIDU');
        if (EC_CHARSET == 'utf-8' && in_array($searchengine, $gb_search))
        {
            $keywords = ecs_iconv('GBK', 'UTF8', $keywords);
        }
        if (EC_CHARSET == 'gbk' && !in_array($searchengine, $gb_search))
        {
            $keywords = ecs_iconv('UTF8', 'GBK', $keywords);
        }

        $GLOBALS['db']->autoReplace($GLOBALS['ecs']->table('keywords'), array('date' => local_date('Y-m-d'), 'searchengine' => $searchengine, 'keyword' => addslashes($keywords), 'count' => 1), array('count' => 1));
    }
}

/**
 * 获得指定用户、商品的所有标记
 *
 * @access  public
 * @param   integer $goods_id
 * @param   integer $user_id
 * @return  array
 */
function get_tags($goods_id = 0, $user_id = 0)
{
    $where = '';
    if ($goods_id > 0)
    {
        $where .= " goods_id = '$goods_id'";
    }

    if ($user_id > 0)
    {
        if ($goods_id > 0)
        {
            $where .= " AND";
        }
        $where .= " user_id = '$user_id'";
    }

    if ($where > '')
    {
        $where = ' WHERE' . $where;
    }

    $sql = 'SELECT tag_id, user_id, tag_words, COUNT(tag_id) AS tag_count' .
            ' FROM ' . $GLOBALS['ecs']->table('tag') .
            "$where GROUP BY tag_words";
    $arr = $GLOBALS['db']->getAll($sql);

    return $arr;
}

/**
 * 获取指定主题某个模板的主题的动态模块
 *
 * @access  public
 * @param   string       $theme    模板主题
 * @param   string       $tmp      模板名称
 *
 * @return array()
 */
function get_dyna_libs($theme, $tmp)
{
    $ext = end(explode('.', $tmp));
    $tmp = basename($tmp,".$ext");
    $sql = 'SELECT region, library, sort_order, id, number, type' .
            ' FROM ' . $GLOBALS['ecs']->table('template') .
            " WHERE theme = '$theme' AND filename = '" . $tmp . "' AND type > 0 AND remarks=''".
            ' ORDER BY region, library, sort_order';
    $res = $GLOBALS['db']->getAll($sql);

    $dyna_libs = array();
    foreach ($res AS $row)
    {
        $dyna_libs[$row['region']][$row['library']][] = array(
            'id'     => $row['id'],
            'number' => $row['number'],
            'type'   => $row['type']
        );
    }

    return $dyna_libs;
}

/**
 * 替换动态模块
 *
 * @access  public
 * @param   string       $matches    匹配内容
 *
 * @return string        结果
 */
function dyna_libs_replace($matches)
{
    $key = '/' . $matches[1];
    if ($row = array_shift($GLOBALS['libs'][$key]))
    {
        $str = '';
        switch($row['type'])
        {
            case 1:
                // 分类的商品
                $str = '{assign var="cat_goods" value=$cat_goods_' .$row['id']. '}{assign var="goods_cat" value=$goods_cat_' .$row['id']. '}';
                break;
            case 2:
                // 品牌的商品
                $str = '{assign var="brand_goods" value=$brand_goods_' .$row['id']. '}{assign var="goods_brand" value=$goods_brand_' .$row['id']. '}';
                break;
            case 3:
                // 文章列表
                $str = '{assign var="articles" value=$articles_' .$row['id']. '}{assign var="articles_cat" value=$articles_cat_' .$row['id']. '}';
                break;
            case 4:
                //广告位
                $str = '{assign var="ads_id" value=' . $row['id'] . '}{assign var="ads_num" value=' . $row['number'] . '}';
                break;
        }
        return $str . $matches[0];
    }
    else
    {
        return $matches[0];
    }
}

/**
 * 处理上传文件，并返回上传图片名(上传失败时返回图片名为空）
 *
 * @access  public
 * @param array     $upload     $_FILES 数组
 * @param array     $type       图片所属类别，即data目录下的文件夹名
 *
 * @return string               上传图片名
 */
function upload_file($upload, $type)
{
    if (!empty($upload['tmp_name']))
    {
        $ftype = check_file_type($upload['tmp_name'], $upload['name'], '|png|jpg|jpeg|gif|doc|xls|txt|zip|ppt|pdf|rar|docx|xlsx|pptx|');
        if (!empty($ftype))
        {
            $name = date('Ymd');
            for ($i = 0; $i < 6; $i++)
            {
                $name .= chr(mt_rand(97, 122));
            }

            $name = $_SESSION['user_id'] . '_' . $name . '.' . $ftype;

            $target = ROOT_PATH . DATA_DIR . '/' . $type . '/' . $name;
            if (!move_upload_file($upload['tmp_name'], $target))
            {
                $GLOBALS['err']->add($GLOBALS['_LANG']['upload_file_error'], 1);

                return false;
            }
            else
            {
                return $name;
            }
        }
        else
        {
            $GLOBALS['err']->add($GLOBALS['_LANG']['upload_file_type'], 1);

            return false;
        }
    }
    else
    {
        $GLOBALS['err']->add($GLOBALS['_LANG']['upload_file_error']);
        return false;
    }
}

/**
 * 显示一个提示信息
 *
 * @access  public
 * @param   string  $content
 * @param   string  $link
 * @param   string  $href
 * @param   string  $type               信息类型：warning, error, info
 * @param   string  $auto_redirect      是否自动跳转
 * @return  void
 */
function show_message($content, $links = '', $hrefs = '', $type = 'info', $auto_redirect = true)
{
    // If mobile version, use show_mobile_message() instead
    if ((defined('YOHO_MOBILE')) && (function_exists('show_mobile_message')))
    {
        return show_mobile_message($content, $links, $hrefs, $type, $auto_redirect);
    }
    assign_template();

    $msg['content'] = $content;
    if (is_array($links) && is_array($hrefs))
    {
        if (!empty($links) && count($links) == count($hrefs))
        {
            foreach($links as $key =>$val)
            {
                $msg['url_info'][$val] = $hrefs[$key];
            }
            $msg['back_url'] = $hrefs['0'];
        }
    }
    else
    {
        $link = empty($links) ? _L('global_go_back', '返回上一頁') : $links;
        $href = empty($hrefs) ? 'javascript:history.back()' : $hrefs;
        $msg['url_info'][$link] = $href;
        $msg['back_url'] = $href;
    }

    $msg['type'] = $type;
    $position = assign_ur_here(0, _L('global_system_message', '系統訊息'));
    $GLOBALS['smarty']->assign('page_title', $position['title']);   // 页面标题
    $GLOBALS['smarty']->assign('ur_here',    $position['ur_here']); // 当前位置

    if (is_null($GLOBALS['smarty']->get_template_vars('helps')))
    {
        $GLOBALS['smarty']->assign('helps', get_shop_help()); // 网店帮助
    }

    $GLOBALS['smarty']->assign('auto_redirect', $auto_redirect);
    $GLOBALS['smarty']->assign('message', $msg);
    $GLOBALS['smarty']->display('message.dwt');

    exit;
}

/**
 * 将一个形如+10, 10, -10, 10%的字串转换为相应数字，并返回操作符号
 *
 * @access  public
 * @param   string      str     要格式化的数据
 * @param   char        operate 操作符号，只能返回‘+’或‘*’;
 * @return  float       value   浮点数
 */
function parse_rate_value($str, &$operate)
{
    $operate = '+';
    $is_rate = false;

    $str = trim($str);
    if (empty($str))
    {
        return 0;
    }
    if ($str[strlen($str) - 1] == '%')
    {
        $value = floatval($str);
        if ($value > 0)
        {
            $operate = '*';

            return $value / 100;
        }
        else
        {
            return 0;
        }
    }
    else
    {
        return floatval($str);
    }
}

/**
 * 重新计算购物车中的商品价格：目的是当用户登录时享受会员价格，当用户退出登录时不享受会员价格
 * 如果商品有促销，价格不变
 *
 * @access  public
 * @return  void
 */
function recalculate_price()
{
    /* 取得有可能改变价格的商品：除配件和赠品之外的商品 */
    $sql = 'SELECT c.rec_id, c.goods_id, c.goods_attr_id, g.promote_price, g.promote_start_date, c.goods_number, c.is_insurance, c.insurance_of, '.
                "g.promote_end_date, IFNULL(mp.user_price, g.shop_price * '$_SESSION[discount]') AS member_price ".
            'FROM ' . $GLOBALS['ecs']->table('cart') . ' AS c '.
            'LEFT JOIN ' . $GLOBALS['ecs']->table('goods') . ' AS g ON g.goods_id = c.goods_id '.
            "LEFT JOIN " . $GLOBALS['ecs']->table('member_price') . " AS mp ".
                    "ON mp.goods_id = g.goods_id AND mp.user_rank = '" . $_SESSION['user_rank'] . "' ".
            "WHERE session_id = '" .SESS_ID. "' AND c.parent_id = 0 AND c.is_gift = 0 AND c.goods_id > 0 AND c.flashdeal_id = 0 " .
            "AND c.rec_type = '" . CART_GENERAL_GOODS . "' AND c.extension_code <> 'package'";

            $res = $GLOBALS['db']->getAll($sql);

    foreach ($res AS $row)
    {
        $attr_id    = empty($row['goods_attr_id']) ? array() : explode(',', $row['goods_attr_id']);
        if ($row['insurance_of'] > 0){
            $ins_price = get_insurance_price($row['goods_id'], $row['insurance_of']);
            if ($ins_price){
                $goods_price = $ins_price;
                $goods_user_rank = 0;
                $goods_number = min($GLOBALS['db']->getOne("SELECT goods_number FROM ".$GLOBALS['ecs']->table('cart')." WHERE session_id = '".SESS_ID."' AND goods_id = '".$row['insurance_of']."' AND extension_code <> 'package' AND is_package = 0 AND is_insurance = 0 AND is_gift = 0"), $row['goods_number']);
            } else {
                $GLOBALS['err']->add(_L('global_cart_insurance_not_exists', '您選購的保險服務不存在'), ERR_NOT_EXISTS);
                return false;
            }
        } else {
            $final_price = get_final_price($row['goods_id'], $row['goods_number'], true, $attr_id);
            $goods_price     = $final_price['final_price'];
            $goods_user_rank =  $final_price['user_rank_id'];
            $goods_number = ($final_price['can_buy']) ? min($final_price['can_buy'], $row['goods_number']) : $row['goods_number'];
        }
        if($goods_number == 0) {
            $goods_sql = "DELETE " .$GLOBALS['ecs']->table('cart'). " ";
            $goods_sql .= "WHERE goods_id = '" . $row['goods_id'] . "' AND session_id = '" . SESS_ID . "' AND rec_id = '" . $row['rec_id'] . "'";
        } else {
            $goods_sql = "UPDATE " .$GLOBALS['ecs']->table('cart'). " SET goods_price = '$goods_price', user_rank = '$goods_user_rank', goods_number = $goods_number ";
            $goods_sql .= "WHERE goods_id = '" . $row['goods_id'] . "' AND session_id = '" . SESS_ID . "' AND rec_id = '" . $row['rec_id'] . "'";
        }

        $GLOBALS['db']->query($goods_sql);
    }

    // 2016-11-03: disable auto remove gift as we don't limit favourable activity to member or non-member
    // /* 删除赠品，重新选择 */
    // $GLOBALS['db']->query('DELETE FROM ' . $GLOBALS['ecs']->table('cart') .
    //     " WHERE session_id = '" . SESS_ID . "' AND is_gift > 0");
}

function assign_template($ctype = '', $catlist = array(), $gtag = 'other')
{
    global $smarty;

    $smarty->assign('image_width',   $GLOBALS['_CFG']['image_width']);
    $smarty->assign('image_height',  $GLOBALS['_CFG']['image_height']);
    $smarty->assign('points_name',   $GLOBALS['_CFG']['integral_name']);
    // $smarty->assign('qq',            explode(',', $GLOBALS['_CFG']['qq']));
    // $smarty->assign('ww',            explode(',', $GLOBALS['_CFG']['ww']));
    // $smarty->assign('ym',            explode(',', $GLOBALS['_CFG']['ym']));
    // $smarty->assign('msn',           explode(',', $GLOBALS['_CFG']['msn']));
    // $smarty->assign('skype',         explode(',', $GLOBALS['_CFG']['skype']));
    $gtagString = "";
    preg_match("/gtag\('event','page_view',{(.*)}\);/",$GLOBALS['_CFG']['stats_code'],$gtagString);
    $gtagCode = $GLOBALS['_CFG']['stats_code'];
    if($gtagString){
        if(in_array($gtag,["home","category"])){
            $gtagCode = preg_replace("/(gtag\('event','page_view',{'ecomm_pagetype':')(.*)(','send_to':'AW-986254472'}\);)/","$1$gtag$3",$GLOBALS['_CFG']['stats_code']);
        }else if($gtag != "other"){
            $gtagCode = preg_replace("/(gtag\('event','page_view',{'ecomm_pagetype':')(.*)(','send_to':'AW-986254472'}\);)/","",$GLOBALS['_CFG']['stats_code']);
        }
    }
    $pvString="";
    preg_match("/ga(.*)\((.*)'send'(.*),(.*)'pageview'(.*)\);/",$gtagCode,$pvString);
    $pvString=@$pvString[0];
    $stats_code = substr($gtagCode,0,stripos($gtagCode,$pvString))."\n";
    $stats_code_pv = "\n".substr($gtagCode,stripos($gtagCode,$pvString));
    $smarty->assign('stats_code_pv', $stats_code_pv);
    $smarty->assign('stats_code', $stats_code);
    // $smarty->assign('copyright',     sprintf($GLOBALS['_LANG']['copyright'], date('Y'), $GLOBALS['_CFG']['shop_name']));
    $cur_lang = $GLOBALS['_CFG']['lang']; 
    if($cur_lang == 'zh_tw'){
        $shop_name = $GLOBALS['_CFG']['shop_name'];
    } else {
        $sql = "SELECT `value` FROM " . $GLOBALS['ecs']->table('shop_config_lang') . " WHERE `code` LIKE 'shop_name' AND `lang` LIKE '" .$cur_lang. "'";
        $shop_name = $GLOBALS['db']->getOne($sql);
    }
    $smarty->assign('shop_name',     $shop_name);
    $smarty->assign('service_email', $GLOBALS['_CFG']['service_email']);
    $smarty->assign('service_phone', $GLOBALS['_CFG']['service_phone']);
    $smarty->assign('shop_address',  $GLOBALS['_CFG']['shop_address']);
    // $smarty->assign('licensed',      license_info());
    // $smarty->assign('ecs_version',   VERSION);
    $smarty->assign('icp_number',    $GLOBALS['_CFG']['icp_number']);
    $smarty->assign('username',      !empty($_SESSION['user_name']) ? $_SESSION['user_name'] : '');
    // $smarty->assign('category_list', cat_list(0, 0, true,  2, false));
    // $smarty->assign('catalog_list',  cat_list(0, 0, false, 1, false));
    $smarty->assign('navigator_list',        get_navigator($ctype, $catlist));  //自定义导航栏

    // Multiple language support
    $smarty->assign('current_lang', $GLOBALS['_CFG']['lang']);
    $smarty->assign('default_lang', $GLOBALS['_CFG']['default_lang']);
    $available_languages = available_language_names();
    $smarty->assign('available_languages', $available_languages);
    $available_languages_abbreviation = available_language_names_abbreviation();
    $smarty->assign('available_languages_abbreviation', $available_languages_abbreviation);
    $smarty->assign('current_lang_name', $available_languages[$GLOBALS['_CFG']['lang']]);

    // popular search keywords
    $searchkeywords = (empty($GLOBALS['_CFG']['search_keywords'])) ? array() : explode(',', trim($GLOBALS['_CFG']['search_keywords']));
    $smarty->assign('searchkeywords', $searchkeywords);
}

/**
 * 将一个本地时间戳转成GMT时间戳
 *
 * @access  public
 * @param   int     $time
 *
 * @return int      $gmt_time;
 */
function time2gmt($time)
{
    return strtotime(gmdate('Y-m-d H:i:s', $time));
}

/**
 * 查询会员的红包金额
 *
 * @access  public
 * @param   integer     $user_id
 * @return  void
 */
function get_user_bonus($user_id = 0)
{
    if ($user_id == 0)
    {
        $user_id = $_SESSION['user_id'];
    }

    $sql = "SELECT SUM(bt.type_money) AS bonus_value, COUNT(*) AS bonus_count ".
            "FROM " .$GLOBALS['ecs']->table('user_bonus'). " AS ub, ".
                $GLOBALS['ecs']->table('bonus_type') . " AS bt ".
            "WHERE ub.user_id = '$user_id' AND ub.bonus_type_id = bt.type_id AND ub.order_id = 0";
    $row = $GLOBALS['db']->getRow($sql);

    return $row;
}

/**
 * 保存推荐uid
 *
 * @access  public
 * @param   void
 *
 * @return void
 * @author xuanyan
 **/
function set_affiliate()
{
    $config = unserialize($GLOBALS['_CFG']['affiliate']);
    if (!empty($_GET['u']) && ($config['on'] == 1 || $config['affiliate_yoho_on'] == 1)) // 只要普通推薦/推廣大使有開啟, 都會存在cookie
    {
        $uid = intval($_GET['u']);
        if ($GLOBALS['db']->getOne('SELECT user_id FROM ' . $GLOBALS['ecs']->table('users') . "WHERE user_id = '$uid'")) // remove AND is_affiliate = 1, because we will open all user to affiliate this cookie.
        {
            $duration = 3600 * 24; // 过期时间为 1 天
            if(!empty($config['config']['expire']))
            {
                if($config['config']['expire_unit'] == 'hour')
                {
                    $c = 1;
                }
                elseif($config['config']['expire_unit'] == 'day')
                {
                    $c = 24;
                }
                elseif($config['config']['expire_unit'] == 'week')
                {
                    $c = 24 * 7;
                }
                else
                {
                    $c = 1;
                }
                $duration = 3600 * $config['config']['expire'] * $c;
            }
            set_affiliate_cookie($uid, time() + $duration);
        }
    }
}

/**
 * 获取推荐uid
 *
 * @access  public
 * @param   void
 *
 * @return int
 * @author xuanyan
 **/
function get_affiliate($is_affiliate_only = false)
{
    if (!empty($_COOKIE['yoho_affiliate']))
    {
        $uid = intval($_COOKIE['yoho_affiliate']);
        if ($GLOBALS['db']->getOne('SELECT user_id FROM ' . $GLOBALS['ecs']->table('users') . "WHERE user_id = '$uid'".($is_affiliate_only ? " AND is_affiliate = 1": "")))
        {
            return $uid;
        }
        else
        {
            set_affiliate_cookie('', 1);
        }
    }

    return 0;
}

function set_affiliate_cookie($value, $expire = 0)
{
    $integrate_config = unserialize($GLOBALS['_CFG']['integrate_config']);
    $cookie_domain = empty($integrate_config['cookie_domain']) ? $GLOBALS['ecs']->get_host() : $integrate_config['cookie_domain'];
    $cookie_path = empty($integrate_config['cookie_path']) ? '/' : $integrate_config['cookie_path'];
    $_COOKIE['yoho_affiliate'] = $value; // Using for set cookie but not refresh browser (e.g. : ajaxuser_reg.php)
    setcookie('yoho_affiliate', $value, $expire, $cookie_path, $cookie_domain);
}

/**
 * 获得指定的文章的详细信息
 *
 * @access  private
 * @param   integer     $article_id
 * @return  array
 */
function get_article_info($article_id)
{
    /* 获得文章的信息 */
    $sql = "SELECT a.*, ac.cat_type, ac.cat_name, " .
        "IFNULL(AVG(r.comment_rank), 0) AS comment_rank, aperma " .
        "FROM " . $GLOBALS['ecs']->table('article') . " AS a " .
        "LEFT JOIN " . $GLOBALS['ecs']->table('comment') . " AS r ON r.id_value = a.article_id AND comment_type = 1 " .
        "LEFT JOIN " . $GLOBALS['ecs']->table('article_cat') . " AS ac ON a.cat_id = ac.cat_id " .
        "LEFT JOIN (SELECT id, table_name, lang, perma_link as aperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") apl ON apl.id = a.article_id  AND apl.table_name = 'article' AND apl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
        "WHERE a.is_open = 1 AND a.article_id = '$article_id' GROUP BY a.article_id";
    $row = $GLOBALS['db']->getRow($sql);

    if ($row !== false)
    {
        $row = localize_db_row('article_cat', $row);
        $row = localize_db_row('article', $row);

        $row['comment_rank'] = ceil($row['comment_rank']);                              // 用户评论级别取整
        $row['add_time']     = local_date($GLOBALS['_CFG']['date_format'], $row['add_time']); // 修正添加时间显示

        /* 作者信息如果为空，则用网站名称替换 */
        if (empty($row['author']) || $row['author'] == '_SHOPHELP')
        {
            $row['author'] = $GLOBALS['_CFG']['shop_name'];
        }
        if($row['cat_type'] != COMMON_CAT) {
            $row['article_type'] = 'page';
        } else {
            $row['article_type'] = 'post';
        }
        $row['url'] = build_uri('article', array('aid' => $row['article_id'], 'aname' => $row['title'], 'aperma' => $row['aperma']));
        $row['desktop_url'] = desktop_url($row['url']);
        $row['mobile_url'] = mobile_url($row['url']);
        if(isset($row['file_url']) && $row['file_url']!='' ){
            $row['file_url'] = desktop_url($row['file_url']);
        }
    }

    return $row;
}

/**
 * 获得文章关联的商品
 *
 * @access  public
 * @param   integer $id
 * @return  array
 */

function article_related_goods($id)
{
    $sql = "SELECT g.`goods_id`, g.`sales`+g.`sales_adjust` as shownum, g.`goods_name`, g.`goods_thumb`, g.`goods_img`, " .
                "IFNULL(mp.user_price, g.shop_price * '$_SESSION[discount]') AS shop_price, ".
                "g.`shop_price` as org_price, g.`market_price`, g.`promote_price`, g.`promote_start_date`, g.`promote_end_date` " .
           "FROM " . $GLOBALS['ecs']->table('goods_article') . " as ga " .
           "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON g.goods_id = ga.goods_id " .
           "LEFT JOIN " . $GLOBALS['ecs']->table('member_price') . " as mp ON mp.goods_id = g.goods_id AND mp.user_rank = '$_SESSION[user_rank]' " .
           "WHERE ga.article_id = '$id' AND g.is_on_sale = 1 AND g.is_alone_sale = 1 AND g.is_delete = 0 GROUP BY g.goods_id ";

    $res = $GLOBALS['db']->getAll($sql);
    $res = localize_db_result('goods', $res);

    $arr = array();
    foreach ($res as $row)
    {
        $arr[$row['goods_id']]['goods_id']      = $row['goods_id'];
        $arr[$row['goods_id']]['goods_name']    = $row['goods_name'];
        $arr[$row['goods_id']]['short_name']   = $GLOBALS['_CFG']['goods_name_length'] > 0 ?
            sub_str($row['goods_name'], $GLOBALS['_CFG']['goods_name_length']) : $row['goods_name'];
        $arr[$row['goods_id']]['goods_thumb']   = get_image_path($row['goods_id'], $row['goods_thumb'], true);
        $arr[$row['goods_id']]['goods_img']     = get_image_path($row['goods_id'], $row['goods_img']);
        $arr[$row['goods_id']]['market_price']  = price_format($row['market_price']);
        $arr[$row['goods_id']]['shop_price']    = price_format($row['shop_price']);
        $arr[$row['goods_id']]['url']           = build_uri('goods', array('gid' => $row['goods_id']), $row['goods_name']);
		$arr[$row['goods_id']]['shownum']             =  ($row['shownum']>0 && $row['shownum']!="")?$row['shownum']:'0';

        if ($row['promote_price'] > 0)
        {
            $arr[$row['goods_id']]['promote_price'] = bargain_price($row['promote_price'], $row['promote_start_date'], $row['promote_end_date']);
            $arr[$row['goods_id']]['formated_promote_price'] = price_format($arr[$row['goods_id']]['promote_price']);
        }
        else
        {
            $arr[$row['goods_id']]['promote_price'] = 0;
        }
    }

    return $arr;
}

/**
 * 获得文章分类下的文章列表
 *
 * @access  public
 * @param   integer     $cat_id
 * @param   integer     $page
 * @param   integer     $size
 * @param   string      $requirement
 *
 * @return  array
 */
function get_cat_articles($cat_id, $page = 1, $size = 20, $requirement='', $sort='')
{
    $where = " is_open = 1 ";

    //取出所有非0的文章
    if ($cat_id == '-1')
    {
        $where .= " AND a.cat_id > 0 ";
    }
    else
    {
        $where .= " AND a." . get_article_children($cat_id);
    }

    //增加搜索条件，如果有搜索内容就进行搜索
    if ($requirement != '')
    {
        $where .= " AND (a.title like '%" . $requirement . "%' OR a.description like '%" . $requirement . "%' OR a.content like '%" . $requirement . "%' )";
    }

    $order_by = "";
    if ($sort != '')
    {
        if ($sort == 'order'){
            $order_by = ", a.sort_order ASC";
        }
    }

    $order = " ORDER BY a.article_type DESC" . $order_by . ", a.add_time DESC";

    $sql = 'SELECT a.article_id, a.title, a.author, a.add_time, a.file_url, aperma, a.open_type, a.description, a.content, ac.cat_name ' .
            ' FROM ' .$GLOBALS['ecs']->table('article') ." as a ".
            ' LEFT JOIN ' .$GLOBALS['ecs']->table('article_cat') ." as ac on ac.cat_id = a.cat_id ".
            'LEFT JOIN (SELECT id, table_name, lang, perma_link as aperma FROM ' . $GLOBALS['ecs']->table('perma_link') . ') cpl ON cpl.id = a.article_id  AND cpl.table_name = "article" AND cpl.lang = "' . $GLOBALS['_CFG']['lang'] . '"' .
            ' WHERE ' . $where . ' ' .
            $order.
           ' LIMIT ' . (($page-1) * $size) . ',' . $size;
    $res = $GLOBALS['db']->getAll($sql);

    $res = localize_db_result('article', $res);
    $res = localize_db_result('article_cat', $res);

    $arr = array();
    foreach ($res as $row)
    {
        $article_id = $row['article_id'];

        $arr[$article_id]['id']          = $article_id;
        $arr[$article_id]['title']       = $row['title'];
        $arr[$article_id]['cat_name']       = $row['cat_name'];
        $arr[$article_id]['content']       = $row['content'];
        $arr[$article_id]['desc']       = $row['description'];
        $arr[$article_id]['short_title'] = $GLOBALS['_CFG']['article_title_length'] > 0 ? sub_str($row['title'], $GLOBALS['_CFG']['article_title_length']) : $row['title'];
        $arr[$article_id]['author']      = empty($row['author']) || $row['author'] == '_SHOPHELP' ? $GLOBALS['_CFG']['shop_name'] : $row['author'];
        $arr[$article_id]['url']         = $row['open_type'] != 1 ? build_uri('article', array('aid'=>$article_id,'aperma'=>$row['aperma']), $row['title']) : '';
        $arr[$article_id]['add_time']    = date($GLOBALS['_CFG']['date_format'], $row['add_time']);
        $arr[$article_id]['file_url']    = empty($row['file_url']) ? '/yohohk/img/article-default-feature.png' : $row['file_url'];
    }

    return $arr;
}

/**
 * 获得指定分类下的文章总数
 *
 * @param   integer     $cat_id
 * @param   string      $requirement
 *
 * @return  integer
 */
function get_article_count($cat_id, $requirement='')
{
    global $db, $ecs;

    $where = " is_open = 1 ";

    //取出所有非0的文章
    if ($cat_id == '-1')
    {
        $where .= " AND cat_id > 0 ";
    }
    else
    {
        $where .= " AND " . get_article_children($cat_id);
    }

    if ($requirement != '')
    {
        $where .= " AND title like '%" . $requirement . "%' ";
    }

    $count = $db->getOne("SELECT COUNT(*) FROM " . $ecs->table('article') . " WHERE " . $where);

    return intval($count);
}

/**
 * 获得指定分类同级的所有分类以及该分类下的子分类
 *
 * @access  public
 * @param   integer     $cat_id     分类编号
 * @return  array
 */
function article_categories_tree($cat_id = 0)
{
    if ($cat_id > 0)
    {
        $sql = 'SELECT parent_id FROM ' . $GLOBALS['ecs']->table('article_cat') . " WHERE cat_id = '$cat_id'";
        $parent_id = $GLOBALS['db']->getOne($sql);
    }
    else
    {
        $parent_id = 0;
    }

    /*
     判斷當前分類有否子分類，
     如果有取出子分類，
     如果沒有取上層分類
    */
    $sql = 'SELECT count(*) FROM ' . $GLOBALS['ecs']->table('article_cat') . " WHERE parent_id = '$cat_id'";
    $count = $GLOBALS['db']->getOne($sql);
    if ($count)
    {
        /* 获取当前分类及其子分类 */
        $sql = 'SELECT a.cat_id, a.cat_name, a.sort_order AS parent_order, a.cat_id, a.cat_icon, b.cat_icon as child_cat_icon, a.cat_banner, b.cat_banner as child_cat_banner, a.cat_type, acperma, ' .
                    'b.cat_id AS child_id, b.cat_name AS child_name, child_acperma, b.sort_order AS child_order, a.desktop_config, b.desktop_config as child_desktop_config ' .
                'FROM ' . $GLOBALS['ecs']->table('article_cat') . ' AS a ' .
                'LEFT JOIN ' . $GLOBALS['ecs']->table('article_cat') . ' AS b ON b.parent_id = a.cat_id ' .
                "LEFT JOIN (SELECT id, table_name, lang, perma_link as acperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") acpl ON acpl.id = a.cat_id  AND acpl.table_name = 'article_cat' AND acpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
                "LEFT JOIN (SELECT id, table_name, lang, perma_link as child_acperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") cacpl ON cacpl.id = b.cat_id  AND cacpl.table_name = 'article_cat' AND cacpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
                "WHERE b.parent_id = '$cat_id' AND a.cat_id = $cat_id ORDER BY parent_order ASC, a.cat_id ASC, child_order ASC";
    }
    else
    {
        /* 获取当前分类及其父分类 */
        $sql = 'SELECT a.cat_id, a.cat_name, acperma, b.cat_id AS child_id, b.cat_name AS child_name, child_acperma, b.sort_order, a.cat_icon as cat_icon, b.cat_icon as child_cat_icon, a.cat_banner, b.cat_banner as child_cat_banner, a.cat_type, a.desktop_config, b.desktop_config as child_desktop_config ' .
                'FROM ' . $GLOBALS['ecs']->table('article_cat') . ' AS a ' .
                'LEFT JOIN ' . $GLOBALS['ecs']->table('article_cat') . ' AS b ON b.parent_id = a.cat_id ' .
                "LEFT JOIN (SELECT id, table_name, lang, perma_link as acperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") acpl ON acpl.id = a.cat_id  AND acpl.table_name = 'article_cat' AND acpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
                "LEFT JOIN (SELECT id, table_name, lang, perma_link as child_acperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") cacpl ON cacpl.id = b.cat_id  AND cacpl.table_name = 'article_cat' AND cacpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
                "WHERE b.parent_id = '$parent_id'  ORDER BY sort_order ASC";
    }
    $res = $GLOBALS['db']->getAll($sql);

    $res = localize_db_result('article_cat', $res);
    $res = localize_db_result('article_cat', $res, array('child_id' => 'cat_id', 'child_name' => 'cat_name'));

    $cat_arr = array();
    foreach ($res AS $row)
    {
        $cat_arr[$row['cat_id']]['cat_id']   = $row['cat_id'];
        $cat_arr[$row['cat_id']]['cat_name'] = $row['cat_name'];
        $cat_arr[$row['cat_id']]['cat_icon'] = $row['cat_icon']  ?  '/data/afficheimg/cat_icon/'. $row['cat_icon'] : '/yohohk/img/article-default-menu-icon.png';
        $cat_arr[$row['cat_id']]['cat_banner'] = $row['cat_banner']  ?  '/data/afficheimg/cat_banner/'. $row['cat_banner'] : '/yohohk/img/article-default-menu-icon.png';
        if($row['cat_type'] != SUPPORT_CAT)$cat_arr[$row['cat_id']]['url']  = build_uri('article_cat', array('acid' => $row['cat_id'], 'acperma' => $row['acperma']), $row['cat_name']);
        else $cat_arr[$row['cat_id']]['url']  = '/support/'.$row['cat_id'].'-'.$row['cat_name'].'/';
        $row['desktop'] = unserialize($row['desktop_config']);
        $row['desktop']['grid'] = empty($row['desktop']['grid']) ? 0 : $row['desktop']['grid'];
        if(!empty($row['desktop']) && $row['desktop']['background_img'] != ''){
            $row['desktop']['old_background_img'] = $row['desktop']['background_img'];
            $src = '/' . DATA_DIR . '/afficheimg/cat_banner/'. $row['desktop']['background_img'];
            $row['desktop']['background_img'] = $src;
        }
        $cat_arr[$row['cat_id']]['desktop'] = $row['desktop'];

        if ($row['child_id'] != NULL)
        {
            $cat_arr[$row['cat_id']]['children'][$row['child_id']]['cat_id']   = $row['child_id'];
            $cat_arr[$row['cat_id']]['children'][$row['child_id']]['cat_name'] = $row['child_name'];
            $cat_arr[$row['cat_id']]['children'][$row['child_id']]['cat_icon'] = $row['child_cat_icon']  ?  '/data/afficheimg/cat_icon/'. $row['child_cat_icon'] : '/yohohk/img/article-default-menu-icon.png';
            $cat_arr[$row['cat_id']]['children'][$row['child_id']]['cat_banner'] = $row['child_cat_banner']  ?  '/data/afficheimg/cat_banner/'. $row['child_cat_banner'] : '';
            if($row['cat_type'] != SUPPORT_CAT)$cat_arr[$row['cat_id']]['children'][$row['child_id']]['url']  = build_uri('article_cat', array('acid' => $row['child_id'], 'acperma' => $row['child_acperma']), $row['child_name']);
            else $cat_arr[$row['cat_id']]['children'][$row['child_id']]['url']  = '/support/'.$row['child_id'].'-'.$row['child_name'].'/';
            $row['child_desktop'] = unserialize($row['child_desktop_config']);
            $row['child_desktop']['grid'] = empty($row['child_desktop']['grid']) ? 0 : $row['child_desktop']['grid'];
            if(!empty($row['child_desktop']) && $row['child_desktop']['background_img'] != ''){
                $row['child_desktop']['old_background_img'] = $row['child_desktop']['background_img'];
                $src = '/' . DATA_DIR . '/afficheimg/cat_banner/'. $row['child_desktop']['background_img'];
                $row['child_desktop']['background_img'] = $src;
            }
            $cat_arr[$row['cat_id']]['children'][$row['child_id']]['desktop'] = $row['child_desktop'];
        }
    }

    return $cat_arr;
}

/**
 * 获得指定文章分类的所有上级分类
 *
 * @access  public
 * @param   integer $cat    分类编号
 * @return  array
 */
function get_article_parent_cats($cat)
{
    if ($cat == 0)
    {
        return array();
    }

    $sql = "SELECT ac.cat_id, IFNULL(acl.cat_name, ac.cat_name) as cat_name, ac.parent_id, ac.cat_type, acperma " .
            "FROM " . $GLOBALS['ecs']->table('article_cat') . " as ac " .
            "LEFT JOIN (SELECT id, table_name, lang, perma_link as acperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") acpl ON acpl.id = ac.cat_id  AND acpl.table_name = 'article_cat' AND acpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
            "LEFT JOIN" . $GLOBALS['ecs']->table('article_cat_lang') . " as acl " .
                "ON acl.cat_id = ac.cat_id AND acl.lang = '" . $GLOBALS['_CFG']['lang'] . "' ";
    $arr = $GLOBALS['db']->getAll($sql);

    if (empty($arr))
    {
        return array();
    }

    $index = 0;
    $cats  = array();

    while (1)
    {
        foreach ($arr AS $row)
        {
            if ($cat == $row['cat_id'])
            {
                $cat = $row['parent_id'];

                $cats[$index]['cat_id']   = $row['cat_id'];
                $cats[$index]['cat_name'] = $row['cat_name'];
                $cats[$index]['cat_type'] = $row['cat_type'];
                $cats[$index]['acperma']  = $row['acperma'];

                $index++;
                break;
            }
        }

        if ($index == 0 || $cat == 0)
        {
            break;
        }
    }

    return $cats;
}

/**
 * 取得某模板某库设置的数量
 * @param   string      $template   模板名，如index
 * @param   string      $library    库名，如recommend_best
 * @param   int         $def_num    默认数量：如果没有设置模板，显示的数量
 * @return  int         数量
 */
function get_library_number($library, $template = null)
{
    global $page_libs;

    if (empty($template))
    {
        $template = basename(PHP_SELF);
        $template = substr($template, 0, strrpos($template, '.'));
    }
    $template = addslashes($template);

    static $lib_list = array();

    /* 如果没有该模板的信息，取得该模板的信息 */
    if (!isset($lib_list[$template]))
    {
        $lib_list[$template] = array();
        $sql = "SELECT library, number FROM " . $GLOBALS['ecs']->table('template') .
                " WHERE theme = '" . $GLOBALS['_CFG']['template'] . "'" .
                " AND filename = '$template' AND remarks='' ";
        $res = $GLOBALS['db']->query($sql);
        while ($row = $GLOBALS['db']->fetchRow($res))
        {
            $lib = basename(strtolower(substr($row['library'], 0, strpos($row['library'], '.'))));
            $lib_list[$template][$lib] = $row['number'];
        }
    }

    $num = 0;
    if (isset($lib_list[$template][$library]))
    {
        $num = intval($lib_list[$template][$library]);
    }
    else
    {
        /* 模板设置文件查找默认值 */
        include_once(ROOT_PATH . ADMIN_PATH . '/includes/lib_template.php');
        static $static_page_libs = null;
        if ($static_page_libs == null)
        {
            $static_page_libs = $page_libs;
        }
        $lib = '/library/' . $library . '.lbi';

        $num = isset($static_page_libs[$template][$lib]) ? $static_page_libs[$template][$lib] :  3;
    }

    return $num;
}

/**
 * 取得自定义导航栏列表
 * @param   string      $type    位置，如top、bottom、middle
 * @return  array         列表
 */
function get_navigator($ctype = '', $catlist = array())
{
    $sql = "SELECT *, " .
                "IFNULL(nl.name, n.name) as name, " .
                "IFNULL(nl.url, n.url) as url " .
            "FROM " . $GLOBALS['ecs']->table('nav') . " as n " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('nav_lang') . " as nl ON nl.id = n.id AND nl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
            "WHERE ifshow = '1' ORDER BY type, vieworder";
    $res = $GLOBALS['db']->query($sql);

    $cur_url = substr(strrchr($_SERVER['REQUEST_URI'],'/'),1);

    if (intval($GLOBALS['_CFG']['rewrite']))
    {
        if(strpos($cur_url, '-'))
        {
            preg_match('/([a-z]*)-([0-9]*)/',$cur_url,$matches);
            $cur_url = $matches[1].'.php?id='.$matches[2];
        }
    }
    else
    {
        $cur_url = substr(strrchr($_SERVER['REQUEST_URI'],'/'),1);
    }

    $noindex = false;
    $active = 0;
    $navlist = array(
        'top' => array(),
        'middle' => array(),
        'bottom' => array()
    );
    while ($row = $GLOBALS['db']->fetchRow($res))
    {
        $navlist[$row['type']][] = array(
            'name'      =>  $row['name'],
            'opennew'   =>  $row['opennew'],
            'url'       =>  $row['url'],
            'ctype'     =>  $row['ctype'],
            'cid'       =>  $row['cid'],
            'ifshow_mobile'      =>  $row['ifshow_mobile'],
            );
    }

    /*遍历自定义是否存在currentPage*/
    foreach($navlist['middle'] as $k=>$v)
    {
        $condition = empty($ctype) ? (strpos($cur_url, $v['url']) === 0) : (strpos($cur_url, $v['url']) === 0 && strlen($cur_url) == strlen($v['url']));
        if ($condition)
        {
            $navlist['middle'][$k]['active'] = 1;
            $noindex = true;
            $active += 1;
        }
    }

    if(!empty($ctype) && $active < 1)
    {
        foreach($catlist as $key => $val)
        {
            foreach($navlist['middle'] as $k=>$v)
            {
                if(!empty($v['ctype']) && $v['ctype'] == $ctype && $v['cid'] == $val && $active < 1)
                {
                    $navlist['middle'][$k]['active'] = 1;
                    $noindex = true;
                    $active += 1;
                }
            }
        }
    }

    if ($noindex == false) {
        $navlist['config']['index'] = 1;
    }

    return $navlist;
}

/**
 * 授权信息内容
 *
 * @return  str
 */
function license_info()
{
    if($GLOBALS['_CFG']['licensed'] > 0)
    {
        /* 获取HOST */
        if (isset($_SERVER['HTTP_X_FORWARDED_HOST']))
        {
            $host = $_SERVER['HTTP_X_FORWARDED_HOST'];
        }
        elseif (isset($_SERVER['HTTP_HOST']))
        {
            $host = $_SERVER['HTTP_HOST'];
        }
        $url_domain=url_domain();
        $host = 'http://' . $host .$url_domain ;
        $license = '<a href="http://www.ecshop.com/license.php?product=ecshop_b2c&url=' . urlencode($host) . '" target="_blank"
>&nbsp;&nbsp;Licensed</a>';
        return $license;
    }
    else
    {
        return '';
    }
}

function url_domain()
{
    $curr = strpos(PHP_SELF, ADMIN_PATH . '/') !== false ?
            preg_replace('/(.*)(' . ADMIN_PATH . ')(\/?)(.)*/i', '\1', dirname(PHP_SELF)) :
            dirname(PHP_SELF);

    $root = str_replace('\\', '/', $curr);

    if (substr($root, -1) != '/')
    {
        $root .= '/';
    }

    return $root;
}

function get_user_order($uid=0)  //sbsn 增加订单数量判断函数  20130702
{
    if ($uid == 0)
    {
        $uid = $_SESSION['user_id'];
    }
    $sql  = 'SELECT count(*)'.//sbsn0702 user_rank
            ' FROM ' .$GLOBALS['ecs']->table('order_info'). ' AS u ' .
            " WHERE user_id = '$uid' and shipping_status=0 and order_status=0 and pay_status=0";
    $number = $GLOBALS['db']->getOne($sql);

    return $number;
}

/**
 * sbsn 萬能調用商品函數
 * @param   int        $cat_id     分類ID
 * @param   int        $brand      品牌ID
 * @param   float      $min        最大金額
 * @param   float      $max        最小金額
 * @param   string     $sort       排序項
 * @param   string     $order      排序方式
 * @param   boolean    $is_promote 是否促銷
 * @param   int        $num        數量
 * @param   string     $biao       標籤 (unused)
 * @param   int        $tj         SQL WHERE
 * @return  array      $arr        產品列表
 **/
function category_get_goods_all($cat_id, $brand, $min, $max, $sort, $order, $is_promote=0, $num=6, $biao, $tj="")
{
    //category_get_goods_all(分類ID,品牌ID,最大金額,最小金額,排序項,排序方式,是否促銷,數量,標籤,WHERE)

    global $db, $ecs;

    //基礎判斷條件： 在售，實物，未刪除，沒隱藏
    $where = "g.is_on_sale = 1 AND g.is_alone_sale = 1 AND g.is_delete = 0 AND g.is_hidden = 0 ";

    if ($brand > 0)
    {
        $where .= " AND g.brand_id = " . intval($brand) . " ";
    }
    if ($cat_id > 0)
    {
        $where .= " AND " . get_children($cat_id);
    }
    if ($min > 0)
    {
        $where .= " AND g.shop_price >= " . floatval($min) . " ";
    }
    if ($max > 0)
    {
        $where .= " AND g.shop_price <= " . floatval($max) . " ";
    }
    if ($is_promote)
    {
        $where.= " AND g.is_promote = 1 ";
    }
    //if ($biao)
    //{
    //    $where .=  'AND g.gongxiao like \'%' . $biao . '%\'  ';
    //}
    if ($tj) // Arbitrary SQL to append after WHERE
    {
        $where .=  " $tj ";
    }

    // howang: support 7 day top sales
    $select_extra = '';
    $order_goods_extra = '';
    if ($sort == 'shownum7')
    {
        $within7days = gmtime() - 604800;
        $select_extra = ', (SELECT SUM(o7.goods_number) FROM ' . $ecs->table('order_goods') . ' as o7 WHERE o7.goods_id = g.goods_id AND o7.order_id IN (SELECT order_id FROM ' . $ecs->table('order_info') . ' WHERE add_time >= ' . $within7days . ' AND order_status IN (1,5) AND shipping_status IN (1,2) AND pay_status IN (2,1))) as shownum7 ';
    }

    if (is_array($num))
    {
        $limit_start = intval($num[0]);
        $limit_num = intval($num[1]);
    }
    else
    {
        $limit_start = 0;
        $limit_num = intval($num);
    }

    /* 获得商品列表 */
    $sql = "SELECT g.`goods_id`, g.`goods_sn`, g.`brand_id`, g.`goods_name`, g.`goods_name_style`, g.`goods_type`, " .
                "(g.`sales` + g.`sales_adjust`) as shownum, g.`goods_number`, g.`is_pre_sale`, " .
                'mp2.user_price as vip_price, '.
                "IFNULL(mp.user_price, g.shop_price * '$_SESSION[discount]') AS shop_price, " .
                " g.market_price, g.shop_price AS org_price, g.promote_price, g.promote_start_date, g.promote_end_date, " .
                " g.goods_brief, g.keywords, g.goods_thumb, g.goods_img, g.is_new, g.is_best,  g.display_discount, g.is_hot " . $select_extra .
            "FROM " . $ecs->table('goods') . " AS g " .
            "LEFT JOIN " . $ecs->table('member_price') . " AS mp ON mp.goods_id = g.goods_id AND mp.user_rank = '$_SESSION[user_rank]' " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('member_price') . " AS mp2 ON mp2.goods_id = g.goods_id AND mp2.user_rank = 2 ". // TODO:VIP = 2
            "WHERE $where ORDER BY $sort $order LIMIT $limit_start, $limit_num";
    $res = $db->getAll($sql);
    $arr = hw_process_goods_rows($res);

    return $arr;
}


//获取用户等级的函数  有时候会用到 比如在商品评论列表的用户名旁边显示 该用户的等级
function get_user_rankname($user_id)
{
    $infos = $GLOBALS['db']->getRow("select * from ".$GLOBALS['ecs']->table('users')." where user_id='$user_id'");

    if ($infos['user_rank'] > 0)
    {
        $sql = "SELECT rank_id, rank_name, discount FROM ".$GLOBALS['ecs']->table('user_rank') .
                " WHERE rank_id = '$infos[user_rank]'";
    }
    else
    {
        $sql = "SELECT rank_id, rank_name, discount, min_points".
            " FROM ".$GLOBALS['ecs']->table('user_rank') .
            " WHERE min_points <= " . intval($infos['rank_points']) . " ORDER BY min_points DESC";
    }

    if ($row = $GLOBALS['db']->getRow($sql))
    {
        $rank_name     = $row['rank_name'];
    }
    else
    {
        $rank_name = $GLOBALS['_LANG']['undifine_rank'];
    }

    return $rank_name;
}

// Get upcoming Flash Deals information
function get_upcoming_flashdeals($num = 0, $random = false)
{
    global $db, $ecs, $_CFG;

    // Get Upcoming deals
    $sql = hw_goods_list_sql(array(
        'select' => ', fd.flashdeal_id, fd.deal_date, fd.deal_price, fd.deal_quantity, fd.detail_visible ',
        'join' => "LEFT JOIN " . $ecs->table('flashdeals') . " as fd ON g.goods_id = fd.goods_id LEFT JOIN " . $ecs->table('flashdeal_event') . " as fde ON fde.fd_event_id = fd.fd_event_id ",
        'where' => "fd.deal_date >= " . (local_strtotime(local_date('Y-m-d')) + 86400) . " AND fde.status = 1 ",
        'sort' => $random ? 'RAND()' : 'deal_date',
        'order' => 'ASC',
        'in_stock_first' => false
    ));
    if ($num > 0)
    {
        $sql .= " LIMIT " . $num;
    }
    $upcoming_deals = $db->getAll($sql);
    $upcoming_deals = localize_db_result('goods', $upcoming_deals);

    foreach ($upcoming_deals as $key => $deal)
    {
        $deal['deal_date']   = local_date('d M Y', $deal['deal_date']);
        $deal['short_name']  = $_CFG['goods_name_length'] > 0 ? sub_str($deal['goods_name'], $_CFG['goods_name_length']) : $deal['goods_name'];
        $deal['goods_thumb'] = get_image_path($deal['goods_id'], $deal['goods_thumb'], true);
        $deal['goods_img']   = get_image_path($deal['goods_id'], $deal['goods_img']);
        $deal['url']         = build_uri('goods', array('gid'=>$deal['goods_id'],'gname'=>$deal['goods_name']), $deal['goods_name']);

        if ($deal['detail_visible'])
        {
            $deal['deal_price_formated'] = price_format($deal['deal_price']);

            // YOHO: lowest price = 3% discount, have to pay with cash / ATM / ebanking
            $deal['lowest_pirce'] = ceil(doubleval($deal['deal_price']) * 0.97);
            $deal['lowest_price_formated'] = price_format($deal['lowest_pirce']);

            // Saved price
            $deal['saved_price'] = $deal['market_price'] - $deal['lowest_pirce'];
            $deal['saved_price_formated'] = price_format($deal['saved_price']);
            $deal['saved_percentage'] = round($deal['saved_price'] / $deal['market_price'] * 100.0, 2) . '%';
            $deal['saved_percentage_rounded'] = round($deal['saved_price'] / $deal['market_price'] * 100.0) . '%';
        }

        $upcoming_deals[$key] = $deal;
    }

    return $upcoming_deals;
}

// Get past Flash Deals information
function get_past_flashdeals($mode = 'time', $param = '-1 month')
{
    global $db, $ecs, $_CFG;

    $today_time = local_strtotime(local_date('Y-m-d'));

    // Options to be passed to hw_goods_list_sql()
    $opts = array(
        'select' => ', fd.flashdeal_id, fd.deal_date, fd.deal_price ',
        'join' => "LEFT JOIN " . $ecs->table('flashdeals') . " as fd ON g.goods_id = fd.goods_id ",
        'where' => "fd.deal_date < " . $today_time . " ",
        'sort' => 'deal_date',
        'order' => 'DESC',
        'in_stock_first' => false
    );

    if ($mode == 'time')
    {
        $last_month_time = strtotime($param, $today_time);
        $opts['where'] .= " AND fd.deal_date >= " . $last_month_time . " ";
    }
    elseif ($mode == 'num')
    {
        $opts['num'] = intval($param);
    }
    elseif ($mode == 'custom')
    {
        $where = $opts['where'] . (empty($param['where']) ? '' : ' AND ' . $param['where']);
        $opts = array_merge($opts, $param);
        $opts['where'] = $where;
    }

    $sql = hw_goods_list_sql($opts);
    $past_deals = $db->getAll($sql);
    $past_deals = localize_db_result('goods', $past_deals);

    foreach ($past_deals as $key => $deal)
    {
        $deal['deal_date']   = local_date('d M Y', $deal['deal_date']);
        $deal['short_name']  = $_CFG['goods_name_length'] > 0 ? sub_str($deal['goods_name'], $_CFG['goods_name_length']) : $deal['goods_name'];
        $deal['goods_thumb'] = get_image_path($deal['goods_id'], $deal['goods_thumb'], true);
        $deal['goods_img']   = get_image_path($deal['goods_id'], $deal['goods_img']);
        $deal['url']         = build_uri('goods', array('gid'=>$deal['goods_id'],'gname'=>$deal['goods_name']), $deal['goods_name']);

        // YOHO: lowest price = 3% discount, have to pay with cash / ATM / ebanking
        $deal['lowest_price'] = ceil(doubleval($deal['deal_price']) * 0.97);
        $deal['lowest_price_formated'] = price_format($deal['lowest_pirce']);

        // Saved price
        $deal['saved_price'] = $deal['market_price'] - $deal['lowest_price'];
        $deal['saved_price_formated'] = price_format($deal['saved_price']);
        $deal['saved_percentage'] = round($deal['saved_price'] / $deal['market_price'] * 100.0, 2) . '%';
        $deal['saved_percentage_rounded'] = round($deal['saved_price'] / $deal['market_price'] * 100.0) . '%';

        $past_deals[$key] = $deal;
    }

    return $past_deals;
}

// Get carousel ads banner to display on homepage
function get_homepage_carousels()
{
    $area = user_area();
    $language = $GLOBALS['_CFG']['lang'];

    $sql = "SELECT * " .
            "FROM " . $GLOBALS['ecs']->table('homepage_carousel') .
            "WHERE (" . gmtime() . " BETWEEN `start_time` AND `end_time`) " .
            "AND (area = '' OR area LIKE '%" . mysql_like_quote($area) . "%') " .
            "AND (area_exclude = '' OR area_exclude NOT LIKE '%" . mysql_like_quote($area) . "%') " .
            "AND (language = '' OR language LIKE '%" . mysql_like_quote($language) . "%') " .
            "AND (language_exclude = '' OR language_exclude NOT LIKE '%" . mysql_like_quote($language) . "%') " .
            " ORDER BY `sort_order` ASC";
    $carousels = $GLOBALS['db']->getAll($sql);

    return $carousels;
}

// Get content of article with id = 1
function get_tos_article_content()
{
    global $_CFG, $db, $ecs;

    $sql = "SELECT IFNULL(al.`content`, a.`content`) as `content` " .
			"FROM " . $ecs->table('article') . " as a " .
			"LEFT JOIN " . $ecs->table('article_lang') . " as al " .
				"ON al.article_id = a.article_id AND al.lang = '" . $_CFG['lang'] . "' " .
			"WHERE a.article_id = '1'";
	return $db->getOne($sql);
}

// Get content of privacy with id = 2
function get_privacy_article_content()
{
    global $_CFG, $db, $ecs;

    $sql = "SELECT IFNULL(al.`content`, a.`content`) as `content` " .
			"FROM " . $ecs->table('article') . " as a " .
			"LEFT JOIN " . $ecs->table('article_lang') . " as al " .
				"ON al.article_id = a.article_id AND al.lang = '" . $_CFG['lang'] . "' " .
			"WHERE a.article_id = '2'";
	return $db->getOne($sql);
}

// Get hot articles list
function get_hot_article($cat_id, $art_id = 0, $support = 0)
{
    global  $db;
    $sql = 'SELECT a.article_id, a.title, a.file_url, a.open_type, ac.cat_type, aperma ' .
            'FROM ' .$GLOBALS['ecs']->table('article'). ' AS a ' .
            'LEFT JOIN ' .$GLOBALS['ecs']->table('article_cat'). ' AS ac ON a.cat_id = ac.cat_id ' .
            "LEFT JOIN (SELECT id, table_name, lang, perma_link as aperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") apl ON apl.id = a.article_id  AND apl.table_name = 'article' AND apl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
            'WHERE a.cat_id = '.$cat_id.' AND a.is_open = 1 ' .($art_id > 0 ? " AND a.article_id != $art_id " : " ").
            ($support ? "AND ac.cat_type = '".SUPPORT_CAT."' " : "").
            'ORDER BY a.click_count DESC, a.article_id LIMIT 5 ';

    $arr = $db->getAll($sql);

    if ($arr === false || empty($arr))
    {
        $sql = 'SELECT a.article_id, a.title, a.file_url, a.open_type, ac.cat_type, aperma ' .
            'FROM ' .$GLOBALS['ecs']->table('article'). ' AS a ' .
            'LEFT JOIN ' .$GLOBALS['ecs']->table('article_cat'). ' AS ac ON a.cat_id = ac.cat_id ' .
            "LEFT JOIN (SELECT id, table_name, lang, perma_link as aperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") apl ON apl.id = a.article_id  AND apl.table_name = 'article' AND apl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
            'WHERE a.is_open = 1 '.($support ? " AND ac.cat_type = '".SUPPORT_CAT."' " : " AND ac.cat_type = '" .COMMON_CAT."' ").($art_id > 0 ? " AND a.article_id != $art_id " : " ").
           'ORDER BY a.click_count DESC, a.article_id LIMIT 5 ';

        $arr = $db->getAll($sql);
    }
    $res = localize_db_result('article', $arr);

    $new_arr = array();
    foreach ($res as $row)
    {
        $article_id = $row['article_id'];

        $new_arr[$article_id]['id']          = $article_id;
        $new_arr[$article_id]['title']       = $row['title'];
        $new_arr[$article_id]['file_url']    = $row['file_url'];
        $new_arr[$article_id]['url']         = $row['open_type'] != 1 ? build_uri('article', array('aid'=>$article_id, 'aperma' => $row['aperma']), $row['title']) : trim($row['file_url']);
        if($row['cat_type'] == SUPPORT_CAT)$new_arr[$article_id]['url']         = "/support/answer/".$article_id."-".$row['title'];

    }

    return $new_arr;
}

function set_promote()
{
    if (!empty($_GET['promote'])) {
        $promote = trim(mysql_real_escape_string($_GET['promote']));
        if ($cid = $GLOBALS['db']->getOne('SELECT coupon_id FROM ' . $GLOBALS['ecs']->table('coupons') . "WHERE coupon_code = '$promote'")) {
            $duration = 3600 * 24 * 7; // expire after 7 days
            set_promote_cookie($cid, time() + $duration);
        }
    }
}

function get_promote()
{
    if (!empty($_COOKIE['yoho_promote'])) {
        $cid = intval($_COOKIE['yoho_promote']);
        if ($coupon_code = $GLOBALS['db']->getOne('SELECT coupon_code FROM ' . $GLOBALS['ecs']->table('coupons') . "WHERE coupon_id = '$cid'")) {
            return $coupon_code;
        } else {
            set_promote_cookie('', 1);
        }
    }

    return "";
}
function set_promote_cookie($value, $expire = 0)
{
    $integrate_config = unserialize($GLOBALS['_CFG']['integrate_config']);
    $cookie_domain = empty($integrate_config['cookie_domain']) ? $GLOBALS['ecs']->get_host() : $integrate_config['cookie_domain'];
    $cookie_path = empty($integrate_config['cookie_path']) ? '/' : $integrate_config['cookie_path'];
    $_COOKIE['yoho_promote'] = $value; // Using for set cookie but not refresh browser
    setcookie('yoho_promote', $value, $expire, $cookie_path, $cookie_domain);
}

?>
