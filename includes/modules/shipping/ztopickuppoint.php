<?php
/**
 * Created by Sing
 */


if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

$shipping_lang = ROOT_PATH.'languages/' .$GLOBALS['_CFG']['lang']. '/shipping/ztopickuppoint.php';
if (file_exists($shipping_lang))
{
    global $_LANG;
    include_once($shipping_lang);
}

include_once(ROOT_PATH . 'languages/' . $GLOBALS['_CFG']['lang'] . '/admin/shipping.php');

/* 模块的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
    $i = (isset($modules)) ? count($modules) : 0;

    /* 配送方式插件的代码必须和文件名保持一致 */
    $modules[$i]['code']    = basename(__FILE__, '.php');

    $modules[$i]['version'] = '1.0.0';

    /* 配送方式的描述 */
    $modules[$i]['desc']    = 'ztopickuppoint_desc';

    /* 配送方式是否支持货到付款 */
    $modules[$i]['cod']     = false;

    /* 插件的作者 */
    $modules[$i]['author']  = 'Sing';

    /* 插件作者的官方网站 */
    $modules[$i]['website'] = '';

    /* 配送接口需要的参数 */
    $modules[$i]['configure'] =
        array(
            array('name' => 'max_length', 'value' => 120),
            array('name' => 'max_width', 'value' => 120),
            array('name' => 'max_height', 'value' => 120),
            array('name' => 'base_fee', 'value' => 30)
        );

    /* 地址列表上傳 */
    $modules[$i]['address_list']  = true;

    /* 模式编辑器 */
    $modules[$i]['print_model'] = 2;

    /* 打印单背景 */
    $modules[$i]['print_bg'] = '';

    /* 打印快递单标签位置信息 */
    $modules[$i]['config_lable'] = '';
    $modules[$i]['group_list'] = 'pickupcenter';
    return;
}

class ztopickuppoint
{
    /*------------------------------------------------------ */
    //-- PUBLIC ATTRIBUTEs
    /*------------------------------------------------------ */

    /**
     * 配置信息
     */
    var $configure;

    /*------------------------------------------------------ */
    //-- PUBLIC METHODs
    /*------------------------------------------------------ */

    /**
     * 构造函数
     *
     * @param: $configure[array]    配送方式的参数的数组
     *
     * @return null
     */
    function ztopickuppoint($cfg=array())
    {
        foreach ($cfg AS $key=>$val)
        {
            $this->configure[$val['name']] = $val['value'];
        }
    }

    /**
     * 计算订单的配送费用的函数
     *
     * @param   float   $goods_weight   商品重量
     * @param   float   $goods_amount   商品金额
     * @param   float   $goods_number   商品件数
     * @param   float   $shipping_price 額外收費
     * @return  decimal
     */
    function calculate($goods_weight, $goods_amount, $goods_number, $shipping_price)
    {
        // 是否有設置免費額度，而且金額超過免費額度？
        if ($this->configure['free_money'] > 0 && $goods_amount >= $this->configure['free_money'])
        {
            $fee = 0;
        }
        else
        {
            /* 基本费用 */
            $fee = $this->configure['base_fee'];
        }
        if($shipping_price > $fee) $fee = $shipping_price;
        return $fee;
    }

    /**
     * 查询发货状态
     * 该配送方式不支持查询发货状态
     *
     * @access  public
     * @param   string  $invoice_sn     发货单号
     * @return  string
     */
    function query($invoice_sn)
    {
        return $invoice_sn;
    }

    /**
     * If shipping type is pickuppoint: we will call function getPickUpList to display list
     * @param bool $need_group
     * @param int $shipping_area_id
     * @param bool $clear clear memcache
     * @return array|mixed
     */
    function getPickUpList($need_group = true, $shipping_area_id = 0, $clear = false)
    {
        $code = get_class($this);
        global $_CFG;
        $memcache = new \Memcached();
        $memcache->addServer('localhost', 11211);
        $today = local_strtotime('today');
        $lang = $_CFG['lang'];
        if ($clear) {
            $memcache->delete('ztopickuppoint_'.$shipping_area_id.'_list_'.$today.'_zh_tw');
            $memcache->delete('ztopickuppoint_'.$shipping_area_id.'_list_'.$today.'_zh_cn');
            $memcache->delete('ztopickuppoint_'.$shipping_area_id.'_list_'.$today.'_en_us');
        }
        $excelData  = $memcache->get('ztopickuppoint_'.$shipping_area_id.'_list_'.$today.'_'.$lang);
        if (!$excelData) {
            /* Get sf xlsx */
            ini_set('memory_limit', '512M');
            define( 'PCLZIP_TEMPORARY_DIR', ROOT_PATH . 'temp/');

            // Get excel data
            require_once ROOT_PATH . 'includes/excel/PHPExcel.php';
            require_once ROOT_PATH . 'includes/excel/PHPExcel/IOFactory.php';
            \PHPExcel_Settings::setZipClass(\PHPExcel_Settings::PCLZIP);
            $fileName     = $path = ROOT_PATH.'data/shipping_area_file/' . $this->configure['file'];
            $fileType     = \PHPExcel_IOFactory::identify($fileName);
            $objReader    = \PHPExcel_IOFactory::createReader($fileType);
            $objPHPExcel  = $objReader->load($fileName);
            $lang_num = ($lang == 'zh_tw' || $lang == 'zh_cn') ? 0 : 2;
            $objWorksheet = $objPHPExcel->getSheet(0);
            $array_title = ['edCode', 'abbreviation', 'fullCode', 'pointType', 'nation', 'city', 'district', 'twThrowaddress', 'allday', 'workday', 'nonworkday'];
            foreach ($objWorksheet->getRowIterator() as $key => $row) {
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(false);
                $e_row = [];
                // This loops all cells,
                // even if it is not set.
                // By default, only cells
                // that are set will be
                // iterated.
                $i = 0;
                foreach ($cellIterator as $c_key => $cell) {
                    if($key == 1) {
                        if(!empty($cell->getValue()))$excelKey[$c_key] = trim($cell->getValue());
                    } else {
                        if(!empty($excelKey[$c_key]))$e_row[$array_title[$c_key]]  = trim($cell->getValue());
                    }
                }

                if ($e_row['pointType'] == '自提點'){
                    $e_row['twRegionName'] = $e_row['abbreviation'];
                    if($lang == 'zh_tw' || $lang == 'zh_cn') {
                        if($e_row['city'] == '香港') {
                            $e_row['city'] = '香港島';
                        }
                        if(strpos($e_row['district'], '區') === false) {
                            $e_row['district'] = $e_row['district'].'區';
                        }
                    } else {
                        if($e_row['city'] == '新界') {
                            $e_row['city'] = 'New Territories';
                        } else if($e_row['city'] == '香港' || $e_row['city'] == '香港島') {
                            $e_row['city'] = 'Hong Kong Island';
                        } else if($e_row['city'] == '九龍') {
                            $e_row['city'] = 'Kowloon';
                        }
                    }
                    //$e_row['shop_type']    = (strpos($e_row['twRegionName'], '') === false) ? 'ztopickuppoint' : '';
                    $path = '/yohohk/img/';
                    $e_row['shop_icon']    = $path.'icon_ztopickuppoint.png';
                    $e_row['isHot'] = 0;
                    $e_row['allotRouter'] = 0;

                    if($e_row['workday'] == '24小時') $e_row['workday'] = '00:00-23:59';
                    if($e_row['nonworkday'] == '24小時') $e_row['nonworkday'] = '00:00-23:59';

                    if ($e_row['pointType'] == '自提櫃'){
                        $e_row['sunday'] = $e_row['holiday'] = $e_row['saturday'] = $e_row['workday'];
                        if (strpos($e_row['abbreviation'], 'ShowEC')) {
                            $e_row['shop_icon']    = $path.'icon_showec.png';
                        } elseif (strpos($e_row['abbreviation'], 'LockerLife')){
                            $e_row['shop_icon']    = $path.'icon_lockerlife.png';
                        }
                    } else {

                        $nonworkday = preg_split('/\r\n|\r|\n/', $e_row['nonworkday']);
                        if(count($nonworkday) == 1 && (strpos($nonworkday[0], '(') !== false)) {
                            $tmp = [];
                            preg_match("/[0-9]+\:[0-9]+\-[0-9]+\:[0-9]+/", $nonworkday[0], $tmp);
                            $e_row['saturday'] = $tmp[0];
                            $e_row['holiday'] = '';
                            $e_row['sunday'] = '';
                        } elseif(count($nowwononworkdayrkday) == 1 && (strpos($nonworkday[0], '(') === false)) {
                            $match = [];
                            $nonworkday[0] = preg_replace("/\s+/", '', $nonworkday[0]);
                            preg_match("/[0-9]+\:[0-9]+\-[0-9]+\:[0-9]+/", $nonworkday[0], $match);
                            $number = $match[0];
                            $e_row['sunday'] = $e_row['holiday'] = $e_row['saturday'] = $number;
                        } else {
                            foreach($nonworkday as $dkey => $date) {
                                $date = preg_replace("/\s+/", '', $date);
                                $match = [];
                                preg_match("/[0-9]+\:[0-9]+\-[0-9]+\:[0-9]+/", $date, $match);
                                $number = $match[0];
                                if($dkey == 0)$e_row['saturday'] = $number;
                                elseif ($dkey == 1)$e_row['sunday'] = $e_row['holiday'] = $number;
                            }
                        }

                        if ($e_row['pointType'] == '郵局'){
                            $e_row['shop_icon'] = $path.'icon_hkpost.png';
                        }

                    }
                    if(array_filter($e_row) && !empty($e_row['city']))$excelData[$e_row['edCode']] = $e_row;
                }  

            }
            $memcache->set('ztopickuppoint_'.$shipping_area_id.'_list_'.$today.'_'.$lang, $excelData);
        }

        if($need_group) {
            $group_list = [];
            foreach($excelData as $key => $ef) {
                $group_list[$ef['city']][$ef['district']][$ef['allotRouter']][$ef['edCode']] = $ef;
            }
            return $group_list;
        } else {
            return $excelData;
        }
    }
}
