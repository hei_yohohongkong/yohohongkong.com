<?php

/***
* lockerlife.php
* by Anthony 2018-06-11
*
* 順便智能櫃 shipping module
***/

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

$shipping_lang = ROOT_PATH.'languages/' .$GLOBALS['_CFG']['lang']. '/shipping/lockerlife.php';
if (file_exists($shipping_lang))
{
    global $_LANG;
    include_once($shipping_lang);
}

include_once(ROOT_PATH . 'languages/' . $GLOBALS['_CFG']['lang'] . '/admin/shipping.php');

/* 模块的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
    $i = (isset($modules)) ? count($modules) : 0;

    /* 配送方式插件的代码必须和文件名保持一致 */
    $modules[$i]['code']    = basename(__FILE__, '.php');

    $modules[$i]['version'] = '1.0.0';

    /* 配送方式的描述 */
    $modules[$i]['desc']    = 'lockerlife_desc';

    /* 配送方式是否支持货到付款 */
    $modules[$i]['cod']     = false;

    /* 插件的作者 */
    $modules[$i]['author']  = 'Anthony';

    /* 插件作者的官方网站 */
    $modules[$i]['website'] = '';

    /* 配送接口需要的参数 */
    $modules[$i]['configure'] =
    array(
        array('name' => 'max_length', 'value' => 45),
		array('name' => 'max_width', 'value' => 35),
        array('name' => 'max_height', 'value' => 31),
        array('name' => 'base_fee', 'value' => 30),
	);

    /* 模式编辑器 */
    $modules[$i]['print_model'] = 2;

    /* 打印单背景 */
    $modules[$i]['print_bg'] = '';

    /* 打印快递单标签位置信息 */
    $modules[$i]['config_lable'] = '';

    $modules[$i]['group_list'] = 'locker';

    /* 地址列表上傳 */
    $modules[$i]['address_list'] = true;

    return;
}

class lockerlife
{
    /*------------------------------------------------------ */
    //-- PUBLIC ATTRIBUTEs
    /*------------------------------------------------------ */

    /**
     * 配置信息
     */
    var $configure;

    /*------------------------------------------------------ */
    //-- PUBLIC METHODs
    /*------------------------------------------------------ */

    /**
     * 构造函数
     *
     * @param: $configure[array]    配送方式的参数的数组
     *
     * @return null
     */
    function lockerlife($cfg=array())
    {
        foreach ($cfg AS $key=>$val)
        {
            $this->configure[$val['name']] = $val['value'];
        }
    }

    /**
     * 计算订单的配送费用的函数
     *
     * @param   float   $goods_weight   商品重量
     * @param   float   $goods_amount   商品金额
     * @param   float   $goods_number   商品件数
     * @param   float   $shipping_price 額外收費
     * @return  decimal
     */
    function calculate($goods_weight, $goods_amount, $goods_number, $shipping_price)
    {
        // 是否有設置免費額度，而且金額超過免費額度？
		if ($this->configure['free_money'] > 0 && $goods_amount >= $this->configure['free_money'])
		{
			$fee = 0;
		}
		else
		{
			/* 基本费用 */
			$fee = $this->configure['base_fee'];
        }
        if($shipping_price > $fee) $fee = $shipping_price;
        return $fee;
    }

    /**
     * 查询发货状态
     * 该配送方式不支持查询发货状态
     *
     * @access  public
     * @param   string  $invoice_sn     发货单号
     * @return  string
     */
    function query($invoice_sn)
    {
        return $invoice_sn;
    }

    /**
     * If shipping type is pickuppoint: we will call function getPickUpList to display list
     * @param bool $need_group
     * @param int $shipping_area_id
     * @param bool $clear clear memcache
     * @return array|mixed
     */
    function getPickUpList($need_group = true, $shipping_area_id = 0, $clear = false)
    {
        $code = get_class($this);
        global $_CFG;
        $memcache = new \Memcached();
        $memcache->addServer('localhost', 11211);
        $today = local_strtotime('today');
        $lang = $_CFG['lang'];
        if ($clear) {
            $memcache->delete('lockerlife_'.$shipping_area_id.'_list_'.$today.'_zh_tw');
            $memcache->delete('lockerlife_'.$shipping_area_id.'_list_'.$today.'_zh_cn');
            $memcache->delete('lockerlife_'.$shipping_area_id.'_list_'.$today.'_en_us');
        }
        $excelData  = $memcache->get('lockerlife_'.$shipping_area_id.'_list_'.$today.'_'.$lang);
        if (!$excelData) {
            /* Get sf xlsx */
            ini_set('memory_limit', '256M');
            define( 'PCLZIP_TEMPORARY_DIR', ROOT_PATH . 'temp/');

            // Get excel data
            require_once ROOT_PATH . 'includes/excel/PHPExcel.php';
            require_once ROOT_PATH . 'includes/excel/PHPExcel/IOFactory.php';
            \PHPExcel_Settings::setZipClass(\PHPExcel_Settings::PCLZIP);
            $fileName     = $path = ROOT_PATH.'data/shipping_area_file/' . $this->configure['file'];
            $fileType     = \PHPExcel_IOFactory::identify($fileName);
            $objReader    = \PHPExcel_IOFactory::createReader($fileType);
            $objPHPExcel  = $objReader->load($fileName);
            $lang_num = ($lang == 'zh_tw' || $lang == 'zh_cn') ? 0 : 1;
            $objWorksheet = $objPHPExcel->getSheet($lang_num);
            if($lang == 'zh_tw' || $lang == 'zh_cn') {
                $array_title = ['number','edCode', 'abbreviation', 'city', 'district', 'twThrowaddress', 'size', 'total', 'workday', 'remark'];
            } else {
                $array_title = ['number','edCode', 'abbreviation', 'twThrowaddress', 'district', 'city', 'size', 'total', 'workday', 'remark'];
            }
            $tmp = [];
            foreach ($objWorksheet->getRowIterator() as $key => $row) {
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(false);
                // This loops all cells,
                // even if it is not set.
                // By default, only cells
                // that are set will be
                // iterated.
                foreach ($cellIterator as $c_key => $cell) {

                    if ($key == 1 || $key == 2) {
                        continue;
                    } else if($key == 3) {
                        if(!empty($cell->getValue()))$excelKey[$c_key] = trim($cell->getValue());
                    } else {
                        if(!empty($excelKey[$c_key]))$e_row[$array_title[$c_key]]  = trim($cell->getValue());
                    }
                }
                $e_row['size'] = str_replace('cm', '', $e_row['size']);
                $tmp_size = [];
                $tmp_size = explode('*', $e_row['size']);
                if(count($tmp_size) == 3) {
                    $e_row['max_width'] = $tmp_size[0];
                    $e_row['max_length'] = $tmp_size[1];
                    $e_row['max_height'] = $tmp_size[2];
                }
                $e_row['twRegionName'] = $e_row['abbreviation'];
                $e_row['isHot'] = 0;
                $e_row['allotRouter'] = 0;
                $path = '/yohohk/img/';
                $e_row['shop_icon']    = $path.'icon_lockerlife.png';
                if($e_row['workday'] == '24') $e_row['workday'] = '00:00-23:59';
                $e_row['sunday'] = $e_row['holiday'] = $e_row['saturday'] = $e_row['workday'];
                if(array_filter($e_row) && !empty($e_row['city']))$excelData[$e_row['edCode']] = $e_row;
            }
            $memcache->set('lockerlife_'.$shipping_area_id.'_list_'.$today.'_'.$lang, $excelData);
        }

        if($need_group) {
            $group_list = [];
            foreach($excelData as $key => $ef) {
                $group_list[$ef['city']][$ef['district']][$ef['allotRouter']][$ef['edCode']] = $ef;
            }
            return $group_list;
        } else {
            return $excelData;
        }
    }
}
