<?php

/***
* eflocker.php
* by Anthony 2018-06-11
*
* 順便智能櫃 shipping module
***/

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

$shipping_lang = ROOT_PATH.'languages/' .$GLOBALS['_CFG']['lang']. '/shipping/eflocker.php';
if (file_exists($shipping_lang))
{
    global $_LANG;
    include_once($shipping_lang);
}

include_once(ROOT_PATH . 'languages/' . $GLOBALS['_CFG']['lang'] . '/admin/shipping.php');

/* 模块的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
    $i = (isset($modules)) ? count($modules) : 0;

    /* 配送方式插件的代码必须和文件名保持一致 */
    $modules[$i]['code']    = basename(__FILE__, '.php');

    $modules[$i]['version'] = '1.0.0';

    /* 配送方式的描述 */
    $modules[$i]['desc']    = 'eflocker_desc';

    /* 配送方式是否支持货到付款 */
    $modules[$i]['cod']     = false;

    /* 插件的作者 */
    $modules[$i]['author']  = 'Anthony';

    /* 插件作者的官方网站 */
    $modules[$i]['website'] = '';

    /* 配送接口需要的参数 */
    $modules[$i]['configure'] =
    array(
        array('name' => 'max_length', 'value' => 45),
		array('name' => 'max_width', 'value' => 35),
        array('name' => 'max_height', 'value' => 31),
        array('name' => 'base_fee', 'value' => 30),
	);

    /* 模式编辑器 */
    $modules[$i]['print_model'] = 2;

    /* 打印单背景 */
    $modules[$i]['print_bg'] = '';

    /* 打印快递单标签位置信息 */
    $modules[$i]['config_lable'] = '';

    $modules[$i]['group_list'] = 'locker';

    return;
}

class eflocker
{
    /*------------------------------------------------------ */
    //-- PUBLIC ATTRIBUTEs
    /*------------------------------------------------------ */

    /**
     * 配置信息
     */
    var $configure;

    /*------------------------------------------------------ */
    //-- PUBLIC METHODs
    /*------------------------------------------------------ */

    /**
     * 构造函数
     *
     * @param: $configure[array]    配送方式的参数的数组
     *
     * @return null
     */
    function eflocker($cfg=array())
    {
        foreach ($cfg AS $key=>$val)
        {
            $this->configure[$val['name']] = $val['value'];
        }
    }

    /**
     * 计算订单的配送费用的函数
     *
     * @param   float   $goods_weight   商品重量
     * @param   float   $goods_amount   商品金额
     * @param   float   $goods_number   商品件数
     * @param   float   $shipping_price 額外收費
     * @return  decimal
     */
    function calculate($goods_weight, $goods_amount, $goods_number, $shipping_price)
    {
        // 是否有設置免費額度，而且金額超過免費額度？
		if ($this->configure['free_money'] > 0 && $goods_amount >= $this->configure['free_money'])
		{
			$fee = 0;
		}
		else
		{
			/* 基本费用 */
			$fee = $this->configure['base_fee'];
        }
        if($shipping_price > $fee) $fee = $shipping_price;
        return $fee;
    }

    /**
     * 查询发货状态
     * 该配送方式不支持查询发货状态
     *
     * @access  public
     * @param   string  $invoice_sn     发货单号
     * @return  string
     */
    function query($invoice_sn)
    {
        return $invoice_sn;
    }

    /**
     * If shipping type is pickuppoint: we will call function getPickUpList to display list
     * @param bool $need_group
     * @param int $shipping_area_id
     * @param bool $clear clear memcache
     * @return array|mixed
     */
    function getPickUpList($need_group = true, $shipping_area_id = 0, $clear = false)
    {
        global $_CFG;
        $memcache = new \Memcached();
        $memcache->addServer('localhost', 11211);

        $today = local_strtotime('today');
        $lang = $_CFG['lang'];
        if ($clear) {
            $memcache->delete('ef_list_'.$today.'_zh_tw');
            $memcache->delete('ef_list_'.$today.'_zh_cn');
            $memcache->delete('ef_list_'.$today.'_en_us');
        }
        $ef_list  = $memcache->get('ef_list_'.$today.'_'.$lang);
        if (!$ef_list) {

            $beta = (strpos($_SERVER['SERVER_NAME'], 'beta.yohohongkong.com') !== false);
            $lang_num = ($lang == 'zh_tw' || $lang == 'zh_cn') ? 1 : 0;
           if($beta) {
                $url = 'http://ipl-hibox.sit.eflocker.com:4480/dropbox/hibox/expressCompany/queryEdmEdinfo';
                $content = json_encode(["appId"=>"sittest","transId"=>"IPL563490", "language"=>$lang_num]);
           } else {
                $url = "http://openapi.eflocker.com/hibox/expressCompany/queryEdmEdinfo";
                $content = json_encode(["appId"=>"yohohongkong","transId"=>"IPL281316", "language"=>$lang_num]);
           }
            // TODO: update $_CFG later.
            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt(
                $curl,
                CURLOPT_HTTPHEADER,
                array("Content-type: application/json")
            );
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

            $json_response = curl_exec($curl);

            $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            if ($status != 201) {
                // TODO: Error handle.
                // die("Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
            }
            curl_close($curl);

            $response = json_decode($json_response, true);
            $list     = $response['data'];
            $ef_list = [];
            foreach($list as $key => $ef) {
                if(empty($ef['city']) || strpos($ef['city'], '澳門') !== false || strpos($ef['city'], 'macau') !== false ) continue;
                $path = '/yohohk/img/';
                $ef['shop_icon']    = $path.'icon_EF.png';
                $ef_list[$ef['edCode']] = $ef;
            }
            $memcache->set('ef_list_'.$today.'_'.$lang, $ef_list);
        }

        if($need_group) {
            $group_list = [];
            foreach($ef_list as $key => $ef) {
                $group_list[$ef['city']][$ef['district']][$ef['allotRouter']][$ef['edCode']] = $ef;
            }
            return $group_list;
        } else {
            return $ef_list;
        }
    }
}
