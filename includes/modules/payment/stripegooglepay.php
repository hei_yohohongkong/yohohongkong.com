<?php
if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

$payment_lang = ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/payment/stripegooglepay.php';
$payment_lang_base = ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/payment/stripe.php';

if (file_exists($payment_lang))
{
	global $_LANG;

    include_once($payment_lang);
    include_once($payment_lang_base);
}

/* 模組的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
	$i = isset($modules) ? count($modules) : 0;

	/* 代碼 */
	$modules[$i]['code']    = basename(__FILE__, '.php');

	/* 描述对应的语言项 */
	$modules[$i]['desc']    = 'stripegooglepay_desc';

	/* 是否支持货到付款 */
	$modules[$i]['is_cod']  = '0';

	/* 是否支持在线支付 */
	$modules[$i]['is_online']  = '1';
	$modules[$i]['is_online_pay']  = '1';

	/* 作者 */
	$modules[$i]['author']  = 'Billy';

	/* 网	址 */
	$modules[$i]['website'] = '';

	/* 版本号 */
	$modules[$i]['version'] = '1.0.0';

	$modules[$i]['is_debug_mode'] = '0';

	/* 配置信息 */
	$modules[$i]['config'] = array(
		array('name' => 'stripe_secret_key', 'type' => 'text', 'value' => ''),
		array('name' => 'stripe_publishable_key', 'type' => 'text', 'value' => ''),
		array('name' => 'stripe_currency', 'type' => 'select', 'value' => 'HKD')
	);

	return;
}

/**
 * 类
 */
class stripegooglepay
{
	var $is_sandbox = false;
    var $stripeWebBrowserController;

    function __construct()
    {
        $this->stripeWebBrowserController = new Yoho\cms\Controller\StripeWebBrowserPaymentController();
    }
        
    /**
	 * 生成支付代码
	 * @param   array   $order  订单信息
	 * @param   array   $payment    支付方式信息
	 */
	function get_code($order, $payment)
	{
		
       return $this->stripeWebBrowserController->getPaymentButton($order, $payment, $this->is_sandbox, 'stripegooglepay');
    }

    function respond()
    {
        return false;
    }
}
?>
