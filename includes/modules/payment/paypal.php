<?php

/**
 * ECSHOP 贝宝插件
 * ============================================================================
 * * 版权所有 2005-2012 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liubo $
 * $Id: paypal.php 17217 2011-01-19 06:29:08Z liubo $
 */

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

$payment_lang = ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/payment/paypal.php';

if (file_exists($payment_lang))
{
    global $_LANG;

    include_once($payment_lang);
}

/* 模块的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
    $i = isset($modules) ? count($modules) : 0;

    /* 代码 */
    $modules[$i]['code']    = basename(__FILE__, '.php');

    /* 描述对应的语言项 */
    $modules[$i]['desc']    = 'paypal_desc';

    /* 是否支持货到付款 */
    $modules[$i]['is_cod']  = '0';

    /* 是否支持在线支付 */
    $modules[$i]['is_online']  = '1';

    /* 作者 */
    $modules[$i]['author']  = 'howang';

    /* 网址 */
    $modules[$i]['website'] = 'http://www.howang.hk/';

    /* 版本号 */
    $modules[$i]['version'] = '1.0.0';

    /* 配置信息 */
    $modules[$i]['config'] = array(
        array('name' => 'paypal_account', 'type' => 'text', 'value' => ''),
        array('name' => 'paypal_currency', 'type' => 'select', 'value' => 'USD')
    );

    return;
}

/**
 * 类
 */
class paypal
{
    var $is_sandbox = false;
    
    /**
     * 构造函数
     *
     * @access  public
     * @param
     *
     * @return void
     */
    function paypal()
    {
    }

    function __construct()
    {
        $this->paypal();
    }

    /**
     * 生成支付代码
     * @param   array   $order  订单信息
     * @param   array   $payment    支付方式信息
     */
    function get_code($order, $payment)
    {
        require_once ROOT_PATH . ADMIN_PATH . '/includes/lib_accounting.php';
        global $actgHooks;
        $pay_log = $actgHooks->paymentModuleGetPayLog(get_class($this), $order['log_id']);
        
        $data_order_id      = $order['log_id'];
        $data_amount        = $order['order_amount'];
        $data_return_url    = get_secure_ecs_url() . 'user.php?act=order_detail&order_id=' . $pay_log['order_id'] . '&payment_processing=1'; //return_url(basename(__FILE__, '.php'));
        $data_pay_account   = $this->is_sandbox ? 'hw_1332755085_biz@gmail.com' : $payment['paypal_account'];
        $currency_code      = !empty($pay_log['currency_code']) ? $pay_log['currency_code'] : $payment['paypal_currency'];
        $data_notify_url    = return_url(basename(__FILE__, '.php'));
        $cancel_return      = get_secure_ecs_url();//$GLOBALS['ecs']->url();
        $btn_class          = defined('YOHO_MOBILE') ? 'btn btn-primary btn-lg' : 'btn-css3';
        
        $address_data = $this->get_address_from_order($order);
        $pass_address = $this->should_pass_address($address_data);
        if ($pass_address)
        {
            $data_address1 = htmlspecialchars($address_data['address1']);
            $data_city = htmlspecialchars($address_data['city']);
            $data_country = htmlspecialchars($address_data['country']);
            $data_first_name = htmlspecialchars($address_data['first_name']);
            $data_last_name = htmlspecialchars($address_data['last_name']);
            $data_state = htmlspecialchars($address_data['state']);
            $data_zip = htmlspecialchars($address_data['zip']);
        }
        $data_email = htmlspecialchars($order['email']);
        $data_lc = $this->get_user_locale();
        
        $paypal_host = $this->is_sandbox ? 'www.sandbox.paypal.com' : 'www.paypal.com';
        
        $def_url  = '<br /><form style="text-align:center;" action="https://' . $paypal_host . '/cgi-bin/webscr" method="post">' .   // 不能省略
            "<input type='hidden' name='cmd' value='_xclick'>" .                             // 不能省略
            "<input type='hidden' name='business' value='$data_pay_account'>" .              // 贝宝帐号
            "<input type='hidden' name='item_name' value='友和 YOHO - $order[order_sn]'>" .   // 物品名稱
            "<input type='hidden' name='item_number' value='$order[order_sn]'>" .            // 订单号
            "<input type='hidden' name='amount' value='$data_amount'>" .                     // 订单金额
            "<input type='hidden' name='currency_code' value='$currency_code'>" .            // 货币
            "<input type='hidden' name='return' value='$data_return_url'>" .                 // 付款后页面
            "<input type='hidden' name='invoice' value='YOHO-$data_order_id'>" .             // log_id
            "<input type='hidden' name='charset' value='utf-8'>" .                           // 字符集
            ($pass_address ?
                "<input type='hidden' name='no_shipping' value='2'>" .                           // 2 – prompt for an address, and require one
                "<input type='hidden' name='address_override' value='1'>" .                      // Ask PayPal to use our address
                "<input type='hidden' name='address1' value='$data_address1'>" .                 // 詳細地址
                "<input type='hidden' name='city' value='$data_city'>" .                         // 城市
                "<input type='hidden' name='country' value='$data_country'>" .                   // 國家代碼
                "<input type='hidden' name='first_name' value='$data_first_name'>" .             // 名字
                "<input type='hidden' name='last_name' value='$data_last_name'>" .               // 姓氏
                (!empty($data_state) ? "<input type='hidden' name='state' value='$data_state'>" : '') . // U.S. state
                "<input type='hidden' name='zip' value='$data_zip'>"                             // Postal code
            :
                "<input type='hidden' name='no_shipping' value='1'>"                             // 1 – do not prompt for an address
            ) .
            "<input type='hidden' name='email' value='$data_email'>" .                       // 電郵地址
            "<input type='hidden' name='no_note' value=''>" .                                // 付款说明
            "<input type='hidden' name='notify_url' value='$data_notify_url'>" .             // IPN 通知頁面
            "<input type='hidden' name='rm' value='2'>" .                                    // Return method, 2 = POST with all variables
            "<input type='hidden' name='page_style' value='YOHO'>" .                         // PayPal帳戶中自訂的頁面風格
            "<input type='hidden' name='lc' value='$data_lc'>" .                             // Locale Code
            "<input type='hidden' name='cbt' value='返回友和 YOHO'>" .                        // 返回按鍵名顯示的文字
            "<input type='hidden' name='cancel_return' value='$cancel_return'>" .            // 返回頁面
            "<input type='submit' class='" . $btn_class . "' value='" . _L('payment_module_paypal_btn', '立即使用 PayPal 付款') . "'>" .                      // 按钮
            "</form><br />";
        
        return $def_url;
    }

    /**
     * 响应操作
     */
    function respond()
    {
        $payment        = get_payment('paypal');
        $merchant_id    = $this->is_sandbox ? 'hw_1332755085_biz@gmail.com' : $payment['paypal_account']; //获取商户编号
        
        $paypal_host = $this->is_sandbox ? 'www.sandbox.paypal.com' : 'www.paypal.com';

        // read the post from PayPal system and add 'cmd'
        $req = 'cmd=_notify-validate';
        foreach ($_POST as $key => $value)
        {
            $value = urlencode(stripslashes($value));
            $req .= "&$key=$value";
        }
        //hw_error_log('req = ' . $req);
        
        // assign posted variables to local variables
        $item_name = $_POST['item_name'];
        $order_sn = $_POST['item_number'];
        $payment_status = $_POST['payment_status'];
        $payment_amount = $_POST['mc_gross'];
        $payment_currency = $_POST['mc_currency'];
        $txn_id = $_POST['txn_id'];
        $receiver_email = $_POST['receiver_email'];
        $payer_email = $_POST['payer_email'];
        $pay_log_id = str_replace('YOHO-', '', $_POST['invoice']);
        $memo = !empty($_POST['memo']) ? $_POST['memo'] : '';
        $address = $this->get_address_from_ipn();
        $address = empty($address) ? '' : '[地址： ' . $address . '] ';
        $action_note = $GLOBALS['_LANG']['paypal_txn_id'] . ':' . $txn_id . '（' . $payment_currency . $payment_amount . '）' . $address . $memo;
        
        $ch = curl_init('https://' . $paypal_host . '/cgi-bin/webscr');
        if ($ch == false)
        {
        	return false;
        }
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
        $res = curl_exec($ch);
        if (curl_errno($ch) != 0) // cURL error
        {
            hw_error_log('Cannot connect to PayPal to validate IPN message: ' . curl_error($ch));
            curl_close($ch);
            return false;
        }
        else
        {
            //hw_error_log('HTTP response of validation request: ' . $res);
            curl_close($ch);
        }
        
        //ob_start();var_dump($res);hw_error_log('res = ' . ob_get_clean());
        
        if (strcmp($res, 'VERIFIED') == 0)
        {
            // check the payment_status is Completed
            if ($payment_status != 'Completed' && $payment_status != 'Pending')
            {
                return false;
            }

            // check that txn_id has not been previously processed
            /*$sql = "SELECT COUNT(*) FROM " . $GLOBALS['ecs']->table('order_action') . " WHERE action_note LIKE '" . mysql_like_quote($txn_id) . "%'";
            if ($GLOBALS['db']->getOne($sql) > 0)
            {
                return false;
            }*/

            // check that receiver_email is your Primary PayPal email
            if ($receiver_email != $merchant_id)
            {
                return false;
            }

            // check that payment_amount/payment_currency are correct
            // $sql = "SELECT order_amount FROM " . $GLOBALS['ecs']->table('pay_log') . " WHERE log_id = '$pay_log_id'";
            // if ($GLOBALS['db']->getOne($sql) != $payment_amount)
            // {
            //     return false;
            // }
            // if ($payment['paypal_currency'] != $payment_currency)
            // {
            //     return false;
            // }
            $sql = "SELECT * FROM " . $GLOBALS['ecs']->table('pay_log') . " WHERE log_id = '$pay_log_id'";
            $pay_log = $GLOBALS['db']->getRow($sql);
            if ($pay_log['order_amount'] != $payment_amount)
            {
                return false;
            }
            if ($pay_log['currency_code'] != $payment_currency)
            {
                return false;
            }

            // process payment
            order_paid($pay_log_id, PS_PAYED, $action_note);

            return true;
        }
        elseif (strcmp($res, 'INVALID') == 0)
        {
            // log for manual investigation
            return false;
        }
    }
    
    /**
     * Get address in PayPal format
     */
    function get_address_from_order($order)
    {
        $address = array();
        
        // First Name / Last Name
        $pos = strrpos($order['consignee'], ' ');
        if ((!empty($order['first_name'])) && (!empty($order['last_name'])))
        {
            $address['first_name'] = $order['first_name'];
            $address['last_name'] = $order['last_name'];
        }
        elseif (($pos > 0) && ($pos < strlen($order['consignee']) - 1)) // if the consignee name have a space in the middle
        {
            $address['first_name'] = substr($order['consignee'], 0, $pos);
            $address['last_name'] = substr($order['consignee'], $pos + 1);
        }
        else
        {
            $address['first_name'] = $order['consignee'];
            $address['last_name'] = '';
        }
        
        // Street Address
        $address['address1'] = ($order['address'] != 'NULL') ? $order['address'] : '';
        
        // Country code
        $address['country'] = strtoupper(get_country_code_by_region($order['country']));
        
        // City name (here we set it to country name)
        $sql = "SELECT `region_name` FROM " . $GLOBALS['ecs']->table('region') . " WHERE region_id = '" . $order['country'] . "'";
        $address['city'] = $GLOBALS['db']->getOne($sql);
        
        // Postal Code / US Zip Code
        $address['zip'] = (empty($order['zipcode'])) ? '00000' : (($order['zipcode'] != 'NULL') ? $order['zipcode'] : '00000');
        
        // Handle US city and state
        if ($address['country'] == 'US')
        {
            $sql = "SELECT city, state_code " .
                    "FROM " . $GLOBALS['ecs']->table('zipcodes') .
                    "WHERE country_code = '" . $address['country'] . "' " .
                    "AND zipcode = '" . $address['zip'] . "'";
            $res = $GLOBALS['db']->getRow($sql);
            if ($res)
            {
                $address['city'] = $res['city'];
                $address['state'] = $res['state_code'];
            }
        }
        // Handle Canada province
        if ($address['country'] == 'CA')
        {
            // Canada postal code is formatted like H0H 0H0
            // Our lookup table only support first 3 char
            $sql = "SELECT city, state_code " .
                    "FROM " . $GLOBALS['ecs']->table('zipcodes') .
                    "WHERE country_code = '" . $address['country'] . "' " .
                    "AND zipcode = '" . substr($address['zip'],0,3) . "'";
            $res = $GLOBALS['db']->getRow($sql);
            if ($res)
            {
                $address['state'] = $res['state_code'];
            }
        }
        // Handle China province
        if ($address['country'] == 'CN')
        {
            // Search for an exact match or the largest postal code smaller than it
            $sql = "SELECT city, state " .
                    "FROM " . $GLOBALS['ecs']->table('zipcodes') .
                    "WHERE country_code = '" . $address['country'] . "' " .
                    "AND zipcode <= '" . $address['zip'] . "' ORDER BY zipcode DESC LIMIT 1";
            $res = $GLOBALS['db']->getRow($sql);
            if ($res)
            {
                $address['city'] = $res['city'];
                $address['state'] = $res['state'];
            }
        }
        
        return $address;
    }
    
    /**
     * Determinate whether or not to pass address to PayPal
     */
    function should_pass_address($address)
    {
        // Check for address completeness
        if (empty($address['first_name']) ||
            empty($address['address1']) ||
            empty($address['country']))
        {
            return false;
        }
        if (in_array($address['country'], array('US', 'CA', 'GB', 'CN')))
        {
            if (empty($address['zip']) ||
                empty($address['city']) ||
                empty($address['state']))
            {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Return a PayPal locale code, based on user's country and language
     * ref: https://developer.paypal.com/docs/classic/api/locale_codes/
     */
    function get_user_locale()
    {
        $user_area = user_area();
        $user_lang = $GLOBALS['_CFG']['lang'];
        $user_lang2 = substr($user_lang, 0, 2);
        
        if ($user_area == 'HK')
        {
            return ($user_lang2 == 'zh') ? 'zh_HK' : 'en_GB';
        }
        else if ($user_area == 'TW')
        {
            return ($user_lang2 == 'zh') ? 'zh_TW' : 'en_US';
        }
        else if ($user_area == 'CN')
        {
            return ($user_lang2 == 'zh') ? 'zh_CN' : 'en_US';
        }
        else
        {
            return ($user_lang2 == 'zh') ? 'zh_TW' : 'en_US';
        }
    }
    
    /**
     * Get address from IPN POSTed variables
     */
    function get_address_from_ipn()
    {
        $address = '';
        if (!empty($_POST['first_name']))
        {
            $address .= $_POST['first_name'] . ' ';
        }
        if (!empty($_POST['last_name']))
        {
            $address .= $_POST['last_name'] . ' ';
        }
        if (!empty($_POST['address_name']))
        {
            $address .= '(' . $_POST['address_name'] . ') ';
        }
        if (!empty($_POST['contact_phone']))
        {
            $address .= $_POST['contact_phone'] . ', ';
        }
        if (!empty($_POST['address_street']))
        {
            $address .= $_POST['address_street'] . ', ';
        }
        if (!empty($_POST['address_city']))
        {
            $address .= $_POST['address_city'] . ', ';
        }
        if (!empty($_POST['address_state']))
        {
            $address .= $_POST['address_state'] . ' ';
        }
        if (!empty($_POST['address_zip']))
        {
            $address .= $_POST['address_zip'] . ' ';
        }
        if (!empty($_POST['address_country']))
        {
            $address .= $_POST['address_country'] . ' ';
        }
        if (!empty($_POST['address_country_code']))
        {
            $address .= '(' . $_POST['address_country_code'] . ') ';
        }
        if (!empty($_POSTS['address_status']))
        {
            $address .= '(Status: ' . $_POST['address_status'] . ')';
        }
        return $address;
    }
}

?>