<?php

/**
 * ECSHOP 支付宝插件
 * ============================================================================
 * * 版权所有 2005-2012 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liubo $
 * $Id: alipay.php 17217 2011-01-19 06:29:08Z liubo $
 */

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

$payment_lang = ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/payment/alipay.php';

if (file_exists($payment_lang))
{
    global $_LANG;

    include_once($payment_lang);
}

/* 模块的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
    $i = isset($modules) ? count($modules) : 0;

    /* 代码 */
    $modules[$i]['code']    = basename(__FILE__, '.php');

    /* 描述对应的语言项 */
    $modules[$i]['desc']    = 'alipay_desc';

    /* 是否支持货到付款 */
    $modules[$i]['is_cod']  = '0';

    /* 是否支持在线支付 */
    $modules[$i]['is_online']  = '1';

    /* 作者 */
    $modules[$i]['author']  = 'alipay2';

    /* 网址 */
    $modules[$i]['website'] = 'http://www.alipay.com';

    /* 版本号 */
    $modules[$i]['version'] = '1.0.2';

    /* 配置信息 */
    $modules[$i]['config']  = array(
        array('name' => 'alipay_account',           'type' => 'text',   'value' => ''),
        array('name' => 'alipay_key',               'type' => 'text',   'value' => ''),
        array('name' => 'alipay_partner',           'type' => 'text',   'value' => ''),
        array('name' => 'alipay_pay_method',        'type' => 'select', 'value' => '')
    );

    return;
}

/**
 * 类
 */
class alipayc2c
{

    /**
     * 构造函数
     *
     * @access  public
     * @param
     *
     * @return void
     */
    function alipayc2c()
    {
    }

    function __construct()
    {
        $this->alipayc2c();
    }

    /**
     * 生成支付代码
     * @param   array   $order      订单信息
     * @param   array   $payment    支付方式信息
     */
    function get_code($order, $payment)
    {
        require_once ROOT_PATH . ADMIN_PATH . '/includes/lib_accounting.php';
        global $actgHooks;
        $pay_log = $actgHooks->paymentModuleGetPayLog(get_class($this), $order['log_id']);
        
        // Early exit here...
        // $btn_class = defined('YOHO_MOBILE') ? 'btn btn-primary btn-lg' : 'btn-css3';
        // $btn_text = _L('payment_transfer_instructions_btn', '付款流程說明');
        // return '<div style="text-align:center;"><a href="javascript:void(0)" id="alipay-btn" class="'.$btn_class.'">'.$btn_text.'</a></div>';
        // 
        
        if (!defined('EC_CHARSET'))
        {
            $charset = 'utf-8';
        }
        else
        {
            $charset = EC_CHARSET;
        }

        $real_method = $payment['alipay_pay_method'];

        switch ($real_method){
            case '0':
                $service = 'trade_create_by_buyer';
                break;
            case '1':
                $service = 'create_partner_trade_by_buyer';
                break;
            case '2':
                $service = 'create_direct_pay_by_user';
                break;
            case '3':
                $service = defined('YOHO_MOBILE') ? 'create_forex_trade_wap' : 'create_forex_trade';
                break;
        }

        $extend_param = 'isv^sh22';

        if ($real_method == '3')
        {
            $parameter = array(
                'service'           => $service,
                'partner'           => $payment['alipay_partner'],                                               
                'return_url'        => return_url(basename(__FILE__, '.php')),
                'notify_url'        => return_url(basename(__FILE__, '.php')),
                '_input_charset'    => $charset,
                'subject'           => $order['order_sn'],
                'out_trade_no'      => $order['order_sn'] . $order['log_id'],
                'total_fee'         => $pay_log['order_amount'],
                'currency'          => $pay_log['currency_code']
            );
        }
        else
        {
            $parameter = array(
                'extend_param'      => $extend_param,
                'service'           => $service,
                'partner'           => $payment['alipay_partner'],
                //'partner'           => ALIPAY_ID,
                '_input_charset'    => $charset,
                'notify_url'        => return_url(basename(__FILE__, '.php')),
                'return_url'        => return_url(basename(__FILE__, '.php')),
                /* 业务参数 */
                'subject'           => $order['order_sn'],
                'out_trade_no'      => $order['order_sn'] . $order['log_id'],
                'price'             => $pay_log['order_amount'],
                'quantity'          => 1,
                'payment_type'      => 1,
                /* 物流参数 */
                'logistics_type'    => 'EXPRESS',
                'logistics_fee'     => 0,
                'logistics_payment' => 'BUYER_PAY_AFTER_RECEIVE',
                /* 买卖双方信息 */
                'seller_email'      => $payment['alipay_account']
            );
        }

        ksort($parameter);
        reset($parameter);

        $param = '';
        $sign  = '';

        foreach ($parameter AS $key => $val)
        {
            $param .= "$key=" .urlencode($val). "&";
            $sign  .= "$key=$val&";
        }

        $param = substr($param, 0, -1);
        $sign  = substr($sign, 0, -1). $payment['alipay_key'];
        //$sign  = substr($sign, 0, -1). ALIPAY_AUTH;

		if ($real_method == '3')
        {
            $endpoint = 'https://mapi.alipay.com/gateway.do';
        }
        else
        {
            $endpoint = 'https://www.alipay.com/cooperate/gateway.do';
        }
        $url = $endpoint.'?'.$param.'&sign='.md5($sign).'&sign_type=MD5';
		
		$btn_class = defined('YOHO_MOBILE') ? 'btn btn-primary btn-lg' : 'btn-css3';
        $btn_text = _L('payment_module_alipay_btn','立即使用支付寶付款');
		$button = '<div style="text-align:center;"><a href="'.$url.'" class="'.$btn_class.'">'.$btn_text.'</a></div>';
		
        return $button;
    }

    /**
     * 响应操作
     */
    function respond()
    {
        if (!empty($_POST))
        {
            foreach($_POST as $key => $data)
            {
                $_GET[$key] = $data;
            }
        }
        $payment  = get_payment($_GET['code']);
        $seller_email = rawurldecode($_GET['seller_email']);
        $log_id = str_replace($_GET['subject'], '', $_GET['out_trade_no']);
        $log_id = trim($log_id);
        $action_note = '支付宝交易号: ' . $_GET['trade_no'] . '（' . $_GET['total_fee'] . ')';


        /* 检查数字签名是否正确 */
        ksort($_GET);
        reset($_GET);

        $sign = '';
        foreach ($_GET AS $key=>$val)
        {
            if ($key != 'sign' && $key != 'sign_type' && $key != 'code')
            {
                $sign .= "$key=$val&";
            }
        }

        $sign = substr($sign, 0, -1) . $payment['alipay_key'];
        //$sign = substr($sign, 0, -1) . ALIPAY_AUTH;
        if (md5($sign) != $_GET['sign'])
        {
            return false;
        }

        /* 检查支付的金额是否相符 */
        // if (!check_money($order_sn, $_GET['total_fee']))
        // {
        //     return false;
        // }
        $sql = "SELECT * FROM " . $GLOBALS['ecs']->table('pay_log') . " WHERE log_id = '" . $log_id . "'";
        $pay_log = $GLOBALS['db']->getRow($sql);
        if ($pay_log['currency_code'] != 'CNY')
        {
            return false;
        }
        if ($pay_log['order_amount'] != $_GET['total_fee'])
        {
            return false;
        }

        if ($_GET['trade_status'] == 'WAIT_SELLER_SEND_GOODS')
        {
            /* 改变订单状态 */
            order_paid($log_id, PS_PAYED, $action_note);

            return true;
        }
        elseif ($_GET['trade_status'] == 'TRADE_FINISHED')
        {
            /* 改变订单状态 */
            order_paid($log_id, PS_PAYED, $action_note);

            return true;
        }
        elseif ($_GET['trade_status'] == 'TRADE_SUCCESS')
        {
            /* 改变订单状态 */
            order_paid($log_id, PS_PAYED, $action_note);

            return true;
        }
        else
        {
            return false;
        }
    }
}

?>