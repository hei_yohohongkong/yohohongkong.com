<?php
/**
 * Created by Anthony.
 * User: Anthony
 * Date: 2018/9/27
 * Time: 下午 03:53
 */

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

$payment_lang = ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/payment/fps.php';

if (file_exists($payment_lang))
{
    global $_LANG;

    include_once($payment_lang);
}

/* 模块的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
    $i = isset($modules) ? count($modules) : 0;

    /* 代码 */
    $modules[$i]['code']    = basename(__FILE__, '.php');

    /* 描述对应的语言项 */
    $modules[$i]['desc']    = 'fps_desc';

    /* 支付手續費 */
    $modules[$i]['pay_fee']    = '-3%';

    /* 是否支持货到付款 */
    $modules[$i]['is_cod']  = '0';

    /* 是否支持在线支付 */
    $modules[$i]['is_online']  = '1';

    /* 作者 */
    $modules[$i]['author']  = 'Anthony';

    /* 网址 */
    $modules[$i]['website'] = '';

    /* 版本号 */
    $modules[$i]['version'] = '1.0.0';

    /* 配置信息 */
    $modules[$i]['config']  = array(
        array('name'=>'phone', 'type'=>'text', 'value'=>'53356996'),
        array('name'=>'email', 'type'=>'text', 'value'=>'info@yohohongkong.com '),
        array('name'=>'fps_id', 'type'=>'text', 'value'=>'00000000'),
        // array('name'=>'desc', 'type'=>'textarea', 'value'=>'00000000'),
        // array('name'=>'desc_eng', 'type'=>'textarea', 'value'=>'00000000'),
        array('name'=>'tutorial', 'type'=>'hidden', 'value'=>'00000000'),

    );

    return;
}
class fps
{

    /**
     * 构造函数
     *
     * @access  public
     * @param
     *
     * @return void
     */
    function fps()
    {
    }

    function __construct()
    {
        $this->fps();
    }

    /**
     * 生成支付代码
     * @param   array   $order  订单信息
     * @param   array   $payment    支付方式信息
     */
    function get_code($order, $payment)
    {
        global $_CFG;
        $lang = $_CFG['lang'];
        $html =
            '<div class="huikuan-intro">
            <div id="fps" style="background: #FFFFFF; text-align: left;">'.
                '<p><span class="gray">'. _L('payment_tell_yoho_telephone', '電話: ').'</span>' . $payment['phone'] . '</p>'.
                '<p><span class="gray">'. _L('payment_tell_yoho_email', '電郵: ').'</span>' . $payment['email'] . '</p>'.
                '<p><span class="gray">'. _L('payment_fps_account_name', '帳戶名稱: ').'</span>' . $payment['fps_id'] . '</p>'.

            '<p style="border-top: 1px solid #00acee;margin: 15px 0 0 0;padding: 10px;line-height: 20px;">'. $payment['tutorial'] . '</p>'.
        $html .= '</div></div>';
        return $html;

    }

    /**
     * 处理函数
     */
    function response()
    {
        return false;
    }
}