<?php

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

$payment_lang = ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/payment/payme.php';

if (file_exists($payment_lang))
{
    global $_LANG;

    include_once($payment_lang);
}
// Skip update user info for faster loading
define('INIT_NO_UPDATE_USER_INFO', true);

/* 模块的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
    $i = isset($modules) ? count($modules) : 0;

    /* 代码 */
    $modules[$i]['code']    = basename(__FILE__, '.php');

    /* 描述对应的语言项 */
    $modules[$i]['desc']    = 'payme_desc';

    /* 是否支持货到付款 */
    $modules[$i]['is_cod']  = '0';

    /* 是否支持在线支付 */
    $modules[$i]['is_online']  = '1';
    $modules[$i]['is_online_pay']  = '1';

    /* 作者 */
    $modules[$i]['author']  = 'Billy';

    /* 网址 */
    $modules[$i]['website'] = '';

    /* 版本号 */
    $modules[$i]['version'] = '1.0.0';

    $modules[$i]['is_debug_mode'] = '0';

    /* 配置信息 */
    $modules[$i]['config']  = array(
        array('name' => 'payme_client_id',               'type' => 'text',   'value' => ''),
        array('name' => 'payme_client_secret',           'type' => 'text',   'value' => ''),
        array('name' => 'payme_signing_key_id',             'type' => 'text',   'value' => ''),
        array('name' => 'payme_signing_key',             'type' => 'text',   'value' => ''),
        array('name' => 'payme_api_version',             'type' => 'text',   'value' => '0.10'),
        array('name' => 'payme_api_base_url',            'type' => 'text',   'value' => "https://sandbox.api.payme.hsbc.com.hk")
    );

    return;
}

/**
 * 类
 */
class payme
{

    var $webhook = false; // Should be always be false since there is no method for webhook
    var $paymeController = null;
    var $orderController = null;
    var $errorMapping = [];
    var $api_refund = true;
    var $manual_api_refund_override = true;

    /**
     * 构造函数
     *
     * @access  public
     * @param
     *
     * @return void
     */
    function payme()
    {
    }

    function __construct()
    {
        global $_LANG;
        $this->paymeController = new Yoho\cms\Controller\PaymeController();
        $this->orderController = new Yoho\cms\Controller\OrderController();
        $this->errorMapping = [
          "EA000" => $_LANG['payme_error_unexpected_api_gateway_error'],
          "EA001" => $_LANG['payme_error_unexpected_not_authenticated'],
          "EA002" => $_LANG['payme_error_unexpected_not_validated'],
          "EA003" => $_LANG['payme_error_unexpected_execution_error'],
          "NA"    => $_LANG['payme_error_unexpected_timeout'],
          "EA008" => $_LANG['payme_error_unexpected_header_error'],
          "EA009" => $_LANG['payme_error_unexpected_too_many_request'],
          "EA014" => $_LANG['payme_error_unexpected_validation_error'],
          "EA017" => $_LANG['payme_error_unexpected_validation_error'],
          "EA018" => $_LANG['payme_error_unexpected_header_error'] ,
          "EB003" => $_LANG['payme_error_unexpected_payme_related_error'],
          "EB004" => $_LANG['payme_error_unexpected_internal_server_error'],
          "EB005" => $_LANG['payme_error_unexpected_internal_server_error'],
          "EB006" => $_LANG['payme_error_unexpected_internal_server_error'],
          "EB008" => $_LANG['payme_error_unexpected_paycode_not_found_error'],
          "EB099" => $_LANG['payme_error_unexpected_internal_server_error'],
          "payCodeExpired" => $_LANG['payme_error_unexpected_paycode_expired'],
          "payCodeExpiredNotConfirmed" => $_LANG['payme_error_paycode_expired_unconfirmed_payment']
        ];
    }

    /**
     * 生成支付代码
     * @param   array   $order      订单信息
     * @param   array   $payment    支付方式信息
     */
    function get_code($order, $payment)
    {
        global $_LANG;

        $token = $this->paymeController->getAccessToken();
        $token = $token->accessToken;

        $paymentRequest = $this->paymeController->createPaymentRequest($order, $token);
        $paymentRequest = json_decode($paymentRequest);

        $expiryDateTime = (isset($paymentRequest->createdTime) && !empty($paymentRequest->createdTime) && isset($paymentRequest->effectiveDuration) && !empty($paymentRequest->effectiveDuration))
                            ? local_date('Y-m-d H:i:s', gmstr2time($paymentRequest->createdTime) + $paymentRequest->effectiveDuration)
                            : null; 

        $paymentStatus = $paymentRequest->statusCode;

        $error_mapping = json_encode($this->errorMapping);

        if (empty($paymentRequest->errors)) {
          if ($paymentStatus == "PR005") {
            return $this->getPaymentSuccessBox();
          }
          else if (defined('YOHO_MOBILE')) {
            return $this->getMobilePaymentElement($paymentRequest, $expiryDateTime, $error_mapping);
          } 
          else {
            return $this->getDesktopPaymentElement($paymentRequest, $expiryDateTime, $error_mapping);
          }
        } else {
          return $this->getErrorBox($paymentRequest);
        }

    }

    function respond()
    {
        if (!$this->validateResponse()) {
          return false;
        }

        if ($this->validatePaymentStatus()) {
          return true;
        }

        return false;
    }

    private function validateResponse()
    {
      global $ecs, $db;
      
      $query_string = html_entity_decode($_SERVER['QUERY_STRING']);
      parse_str($query_string, $_REQUEST);

      // Check if the corresponding payme record exists
      $sql = "SELECT * FROM " . $ecs->table("payme_payment_request_record") 
      . " WHERE `order_id`='" . $_REQUEST['order_id'] . "' and `out_trade_no`='" . $_REQUEST['out_trade_no'] . "' and `gateway_total_amount`='" . $_REQUEST['order_amount'] . "' and `gateway_currency_code`='" . $_REQUEST['currency_code'] . "' and `user_id`='" . $_SESSION['user_id'] . "'";

      $result = $db->getAll($sql);
      if (!isset($result) || (empty($result))) {
        return false;
      }

      // Check if the order is belong to the current user
      $sql = "SELECT * FROM " . $ecs->table("order_info") 
      . " WHERE `order_id`='" . $_REQUEST['order_id'] . "' and `user_id`='" . $_SESSION['user_id'] . "'";

      $result = $db->getAll($sql);
      if (!isset($result) || (empty($result))) {
        return false;
      }

      // Check if the order sn matches the incoming order id
      $order_sn = substr(explode('_', trim($_REQUEST['out_trade_no']))[0], 0, 13);
      $sql = "SELECT order_id, order_sn FROM " . $ecs->table("order_info") 
      . " WHERE `order_sn`='" . $order_sn . "' and `user_id`='" . $_SESSION['user_id'] . "'";

      $result = $db->getRow($sql);
      if (!isset($result) || (empty($result))) {
        return false;
      }
      if ($result['order_id'] != $_REQUEST['order_id'] || $result['order_sn'] != $order_sn) {
        return false;
      } 

      // Check if the payment amount matches the incoming order amount
      $log_id = substr(explode('_', trim($_REQUEST['out_trade_no']))[0], 13);
      $sql = "SELECT * FROM " . $ecs->table("pay_log") 
      . " WHERE `log_id`='" . $log_id . "'";

      $result = $db->getRow($sql);
      if (!isset($result) || (empty($result))) {
        return false;
      }
      if ($result['order_id'] != $_REQUEST['order_id'] || $result['log_id'] != $log_id || $result['order_amount'] != $_REQUEST['order_amount'] || $result['currency_code'] != $_REQUEST['currency_code']) {
        return false;
      } 

      return true;
    }

    private function validatePaymentStatus()
    {
      global $ecs, $db;
      // Retrieve the corresponding payment request record
      $sql = "SELECT * FROM " . $ecs->table("payme_payment_request_record") 
      . " WHERE `order_id`='" . $_REQUEST['order_id'] . "' and `out_trade_no`='" . $_REQUEST['out_trade_no'] . "' and `gateway_total_amount`='" . $_REQUEST['order_amount'] . "' and `gateway_currency_code`='" . $_REQUEST['currency_code'] . "' and `user_id`='" . $_SESSION['user_id'] . "'";

      $result = $db->getRow($sql);

      $request_id = $result['gateway_payment_request_id'];

      $token = $this->paymeController->getAccessToken();
      $token = $token->accessToken;

      $result = $this->paymeController->getPaymentRequestInfo($request_id, $token);
      $result_data = json_decode($result);

      if ($result_data->statusCode == 'PR005') {
        $payment_info = get_payment('payme');
        $transaction_ids = isset($result_data->transactions) ? implode(",", $result_data->transactions) : $result_data->paymentRequestId;
        $action_note = 'payme' . ':' . $transaction_ids . '（' . $result_data->currencyCode . ($result_data->totalAmount) . '）';
        // Update the paymennt request for complete the payment request
        $log_id = $this->paymeController->getLogIdByPaymentRequestId($result_data->paymentRequestId);
        if (!empty($log_id)) {
          $this->orderController->order_payment_transaction_log($log_id, $payment_info['pay_id'], $transaction_ids, $result_data->currencyCode,($result_data->totalAmount),0);
          $this->paymeController->insertStatusIntoPaymentRequestRecord($result);
          order_paid($log_id,PS_PAYED,$action_note);
          return true;
        }
        return true;
      } elseif ($result_data->statusCode == 'PR007') {
        // Update the payment request for the expired payment request
        $this->paymeController->insertStatusIntoPaymentRequestRecord($result);
        return false;
      }
      return false;
    }

    private function getDesktopPaymentElement($paymentRequest, $expiryDateTime, $errorMapping)
    {
      global $_LANG;
      return "
        <link rel='stylesheet' type='text/css' href='/yohohk/css/detail.css'>
        <link rel='stylesheet' type='text/css' href='/yohohk/css/payment/payme.css'>
        <div style='text-align:center;margin:auto;padding-bottom:1rem;'>
            <div class='payme-logo-container' style='height:50px;'>
              <img style='height:inherit;' src='/yohohk/img/payment-icon/payme/PayMeFromHsbcLogo.png'/>
            </div>
            <div style='padding:1rem;'>
                <h1>" . $_LANG['payme_scan_paycode_with_payme'] . "</h1><br/>
                <canvas id='payCodeCanvas' width='200' height='200'></canvas>
                <p>" . $_LANG['payme_donot_close_this_page_until_payment_complete'] . "</p>
            </div>
            <div class='payme-error-container'>
              <p class='payme-error-message bg-danger'></p>
            </div>
            <hr>
            <div style='padding: 1rem;'>
                <h1 class='pull-left'>" . $_LANG['payme_payment_instruction_title'] . "</h1><br/>
                <div class='instruction-container' style='padding-top: 1rem;'>
                    <div class='item'><div><span class='step'>1</span></div><div> ". $_LANG['payme_payment_instruction_open_app'] . "</div></div>
                    <div class='item -center'><div><span class='step'>2</span></div><div style='padding-right:0.5rem;'> ". $_LANG['payme_payment_instruction_scan_to_authorize'] . "</div><div><span id='scanner-instruction-tooltip' title='' class='step -popover'>?</span></div></div>
                    <div class='item -right'><div><span class='step'>3</span></div><div> ". $_LANG['payme_payment_instruction_wait_for_confirm'] . "</div></div>
                </div>
            </div>
        </div>
        
        <script src='https://code.jquery.com/ui/1.12.1/jquery-ui.min.js'></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js'></script>
        <script src='/yohohk/js/payme/qrcode.js'></script>
        <script src='/yohohk/js/payme/paycode.js'></script>
        <script src='/yohohk/js/payme/paycodeGenerator.js'></script>
        <script>
            var webhook = true;
            var payme_request_id = '$paymentRequest->paymentRequestId';
            var payme_request_expiry_date_time = '$expiryDateTime';
            var payme_error_message = JSON.parse('$errorMapping');
            $(document).ready(function(){
                var qr = create_qrcode('$paymentRequest->webLink', 0, 'Q', 'Byte', 'UTF-8');
                var logo = new Image();
                logo.src = '/yohohk/img/article-default-menu-icon.png';
                var canvas = $('#payCodeCanvas')[0];
                var ctx = canvas.getContext('2d');
                ctx.setTransform(1, 0, 0, 1, 0, 0);
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                logo.onload = function() {
                    drawPayCode(qr, canvas, 7, logo, false);
                };
                
            });

            $('#scanner-instruction-tooltip').tooltip({
                position: {
                    my: 'center bottom-18',
                    at: 'center top',
                    using: function( position, feedback ) {
                    $( this ).css( position );
                    $( '<div>' )
                        .addClass( 'arrow' )
                        .addClass( feedback.vertical )
                        .addClass( feedback.horizontal )
                        .appendTo( this );
                    }
                },
                content: function(){
                    return \"<div style='display:flex;flex-direction:column;padding-bottom:0;'>\\
                                <div style='padding:1rem;text-align:center;line-height:2rem;'><p><h1>" . $_LANG['payme_payment_instruction_use_app_title'] . "</h1></p/><p><h2>" . $_LANG['payme_payment_instruction_use_app_instruction'] . "</h2></p></div>\\
                                <div style='padding:1rem 1rem 0 1rem;' class='split-layout'>\\
                                    <div class='split-layout__item'><p>" . $_LANG['payme_payment_instruction_use_app_tap_icon_and_select_scanner'] . "</p><img src='/yohohk/img/payment-icon/payme/UserPayCodeIcon.png'></img></div>\\
                                    <div class='split-layout__divider'><div class='split-layout__rule'></div><div class='split-layout__label'>" . $_LANG['payme_payment_instruction_use_app_or'] . "</div><div class='split-layout__rule -lower'></div></div>\\
                                    <div class='split-layout__item'><p>" . $_LANG['payme_payment_instruction_use_app_swipe_left'] . "</p><img src='/yohohk/img/payment-icon/payme/SwipePayMeAppIcon.png'></img></div>\\
                                </div>\\
                            </div>\";
                }
            });
        </script>";
    }

    private function getErrorBox($paymentRequest)
    {
      $errorBoxElement = (!defined('YOHO_MOBILE') ? "<link rel='stylesheet' type='text/css' href='/yohohk/css/payment/payme.css'>" : "");
      $errorBoxElement .= "
          <style>
          .payme-error-container {
            display: inherit !important;
          }
          </style>
          <div class='payme-error-container'>
            <p class='payme-error-message bg-danger' style='text-align:center;'>" . $this->errorMapping[$paymentRequest->errors[0]->errorCode] . "</p>
          </div>
      ";
      return $errorBoxElement;
    }

    private function getMobilePaymentElement($paymentRequest, $expiryDateTime, $errorMapping)
    {
      global $_LANG;
      return "
        <div style='text-align:center;margin:auto;padding-bottom:1rem;'>
          <div style='padding:1rem;text-align:justify;'>
            " . $_LANG['payme_payment_instruction_mobile'] . "
          </div>
          <div style='padding:1rem;'>
              <img src='/yohohk/img/payment-icon/payme/payme-mobile-payment-instruction.svg' style='width:100%'></img>
            </div>
          <div style='padding:1rem;' class='payme_mobile_payment_button_container'>
            <a href='$paymentRequest->webLink' class='btn payme-payment-btn'><img class='payme-payment-btn-image' src='/yohohk/img/payment-icon/payme/pay-pm-white.png'></img></a>
          </div>
          <div class='payme-error-container'>
            <p class='payme-error-message bg-danger'></p>
          </div>
        </div>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js'></script>
        <script>
          var payme_request_expiry_date_time = '$expiryDateTime';
          var payme_error_message = JSON.parse('$errorMapping');
          var webhook = true;
          var payme_request_id = '$paymentRequest->paymentRequestId';

          document.addEventListener('DOMContentLoaded', function(event) {
            checkPayStatus();
            checkPaymePaymentStatus();
          });
        </script>
        ";
    }

    private function getPaymentSuccessBox()
    {
      global $_LANG;
      $successMessageBox = (!defined('YOHO_MOBILE') ? "<link rel='stylesheet' type='text/css' href='/yohohk/css/payment/payme.css'>" : "");
      $successMessageBox .= "
          <style>
          .payme-error-container {
            display: inherit !important;
          }
          </style>
          <div class='payme-error-container'>
            <p class='payme-error-message bg-danger' style='text-align:center;'>" . $_LANG['payme_payment_successful_waiting_redirect'] . "</p>
          </div>
          ";

        if (defined('YOHO_MOBILE')) {
          $successMessageBox .= "
          <script>
            document.addEventListener('DOMContentLoaded', function(event) {
              checkPayStatus();
            });
          </script>
          ";
        }

        return $successMessageBox;
    }

    function do_refund($tnx_id,$refund_amount)
    {
        $paymeController = new Yoho\cms\Controller\PaymeController();
        return $paymeController->refund_by_transaction_id_and_amount($tnx_id, $refund_amount);
    }
}

?>