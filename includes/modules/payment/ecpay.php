<?php
/**
 * ECSHOP ECpay Creadit Card 插件
 * $Author: Anthony
 * $Id: braintree.php 15-03-2017
 */

require_once ROOT_PATH . 'includes/ecpay/ECPay.Payment.Integration.php';

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

$payment_lang = ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/payment/ecpay.php';

if (file_exists($payment_lang))
{
    global $_LANG;

    include_once($payment_lang);
}

/* 模組的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
    $i = isset($modules) ? count($modules) : 0;

    /* 代碼 */
    $modules[$i]['code']    = basename(__FILE__, '.php');

    /* 描述对应的语言项 */
    $modules[$i]['desc']    = 'ecpay_desc';

    /* 是否支持货到付款 */
    $modules[$i]['is_cod']  = '0';

    /* 是否支持在线支付 */
    $modules[$i]['is_online']  = '1';

    /* 作者 */
    $modules[$i]['author']  = 'Anthony';

    /* 网	址 */
    $modules[$i]['website'] = '';

    /* 版本号 */
    $modules[$i]['version'] = '1.0.0';

    /* 配置信息 */
    $modules[$i]['config'] = array(
        array('name' => 'ecpay_hashKey', 'type' => 'text', 'value' => ''),
        array('name' => 'ecpay_hashIv', 'type' => 'text', 'value' => ''),
    	array('name' => 'ecpay_merchantId', 'type' => 'text', 'value' => '')
    );

    return;
}

/**
 * 类
 */
class ecpay
{

	var $is_sandbox = false;

	/**
	 * 构造函数
	 *
	 * @access  public
	 * @param
	 *
	 * @return void
	 */
	function ecpay()
	{
	}

	function __construct()
	{
		$this->ecpay();
	}

	/**
	 * 生成支付代码
	 * @param   array   $order  订单信息
	 * @param   array   $payment    支付方式信息
	 */
	function get_code($order, $payment)
	{
		require_once ROOT_PATH . ADMIN_PATH . '/includes/lib_accounting.php';

		global $actgHooks;
		$pay_log = $actgHooks->paymentModuleGetPayLog(get_class($this), $order['log_id']);

		$ecpay     = new ECPay_AllInOne();
		$btn_class = defined('YOHO_MOBILE') ? 'btn btn-primary btn-lg' : 'btn-css3';
		$order_id  = $order['order_id'];

		if($this->is_sandbox){
			$ecpay->ServiceURL  = "https://payment-stage.ecpay.com.tw/Cashier/AioCheckOut/V4"; //測試用服務位置
			$ecpay->HashKey     = '5294y06JbISpM5x9' ;                                          //測試用Hashkey，請自行帶入ECPay提供的HashKey
			$ecpay->HashIV      = 'v77hoKGq4kWxNNIS' ;                                          //測試用HashIV，請自行帶入ECPay提供的HashIV
			$ecpay->MerchantID  = '2000132';

		} else {
			$ecpay->ServiceURL  = "https://payment.ecpay.com.tw/Cashier/AioCheckOut/V4"; //服務位置
			$ecpay->HashKey     = $payment['ecpay_hashKey'] ;
			$ecpay->HashIV      = $payment['ecpay_hashIv'] ;
			$ecpay->MerchantID  = $payment['ecpay_merchantId'];
		}

		//基本參數(請依系統規劃自行調整)
		$ecpay->Send['ReturnURL']         = return_url(basename(__FILE__, '.php'));    //付款完成通知回傳的網址
		$ecpay->Send['OrderResultURL']    = return_url(basename(__FILE__, '.php'));    //Client端回傳付款結果網址
		$ecpay->Send['ClientRedirectURL'] = return_url(basename(__FILE__, '.php'));	   //Client端回傳付款相關資訊
		$ecpay->Send['MerchantTradeNo']   = $order['log_id'].'e'.time();               //訂單編號
		$ecpay->Send['MerchantTradeDate'] = date('Y/m/d H:i:s');                       //交易時間
		$ecpay->Send['TotalAmount']       = (int)$order['order_amount'];               //交易金額 - 新台幣限定
		$ecpay->Send['TradeDesc']         = "YOHO" ;                                   //交易描述
		$ecpay->Send['ChoosePayment']     = ECPay_PaymentMethod::ALL ;                 //付款方式:全功能
		$ecpay->Send['IgnorePayment']     = "ATM#CVS#BARCODE";                         //隱藏ATM CVS BARCODE

		//訂單的商品資料
		$goods_list = order_goods($order_id);
		$ecpay->Send['Items'] = array();
		foreach ($goods_list AS $key => $value)
		{
			array_push($ecpay->Send['Items'],
			array(
				'Name'     => $value['goods_name'],
				'Price'    => (int)$value['goods_price'],
				'Currency' => $pay_log['currency_code'],
				'Quantity' => (int)$value['goods_number'],
				'url'      => "#"
			)
			);
		}
		$def_url = "<div style='text-align:center'>".$ecpay->CheckOutString(_L('sumbit', '提交'))."</div>";

		return $def_url;
	}

	/**
	 * 响应操作
	 */
	function respond()
	{

		$merchantId      = $_POST['MerchantID'];
		$merchantTradeNo = $_POST['MerchantTradeNo'];
		$paymentDate     = $_POST['PaymentDate'];
		$paymentType     = $_POST['PaymentType'];
		$rtnCode         = $_POST['RtnCode'];
		$rtnMsg          = $_POST['RtnMsg'];
		$checkMacValue   = $_POST['CheckMacValue'];
		$tradeNo         = $_POST['TradeNo'];
		$tradeDate       = $_POST['TradeDate'];
		$tradeAmt        = $_POST['TradeAmt'];

		$merchantTradeNo_str = explode('e' , $merchantTradeNo);
		$log_id = $merchantTradeNo_str[0];

		//Check paymentType
		if(preg_match("/^WebATM_/",$paymentType)){ //WebATM

			if($rtnCode == 1) { //若回傳值為 1 時，為付款成功 其餘代碼皆為交易失敗
				$action_note =
				'交易類型: ' . $paymentType . ', ' .
				'MerchantTradeNo: ' . $merchantTradeNo .', '.
				'歐付寶訂單編號: ' . $tradeNo .', '.
				'訂單日期: '. $tradeDate .', '.
				'價錢: ' .$tradeAmt. ' NTD';

				order_paid($log_id, PS_PAYED, $action_note);

				return true;

			} else {

				return false;
			}

		} elseif(preg_match("/^ATM_/",$paymentType)){ //ATM

			return false;

		} elseif(preg_match("/^CVS_/",$paymentType)){ //CVS

			return false;

		} elseif($paymentType == 'BARCODE_BARCODE'){ //BARCODE

			return false;

		} elseif($paymentType == 'Credit_CreditCard'){ //Credit card

			if($rtnCode == 1) { //若回傳值為 1 時，為付款成功 其餘代碼皆為交易失敗
				$action_note =
				'交易類型: ' . $paymentType . ', ' .
				'MerchantTradeNo: ' . $merchantTradeNo .', '.
				'歐付寶訂單編號: ' . $tradeNo .', '.
				'訂單日期: '. $tradeDate .', '.
				'價錢: ' .$tradeAmt. ' NTD';

				order_paid($log_id, PS_PAYED, $action_note);

				return true;

			} else {

				return false;
			}

		} else {

			return false;
		}
	}
}

?>
