<?php

/**
 * ECSHOP stripe WeChat 插件
* $Author: Anthony
* $Id: stripewechat.php 22-08-2017
*/

require_once(ROOT_PATH . '/includes/stripe/init.php');

if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

$payment_lang = ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/payment/stripe.php';

if (file_exists($payment_lang))
{
	global $_LANG;

	include_once($payment_lang);
}

/* 模組的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
	$i = isset($modules) ? count($modules) : 0;

	/* 代碼 */
	$modules[$i]['code']    = basename(__FILE__, '.php');

	/* 描述对应的语言项 */
	$modules[$i]['desc']    = 'stripe_wechat_desc';

	/* 是否支持货到付款 */
	$modules[$i]['is_cod']  = '0';

	/* 是否支持在线支付 */
	$modules[$i]['is_online']  = '1';

	/* 作者 */
	$modules[$i]['author']  = 'Anthony';

	/* 网	址 */
	$modules[$i]['website'] = '';

	/* 版本号 */
	$modules[$i]['version'] = '1.0.0';

	/* 配置信息 */
	$modules[$i]['config'] = array(
		array('name' => 'stripe_secret_key', 'type' => 'text', 'value' => ''),
		array('name' => 'stripe_publishable_key', 'type' => 'text', 'value' => ''),
		array('name' => 'stripe_currency', 'type' => 'select', 'value' => 'HKD')
	);

	return;
}

/**
 * 类
 */
class stripewechat
{

	var $is_sandbox = false;

	/**
	 * 构造函数
	 *
	 * @access  public
	 * @param
	 *
	 * @return void
	 */
	function stripewechat()
	{
	}

	function __construct()
	{
		$this->stripewechat();
	}

	/**
	 * 生成支付代码
	 * @param   array   $order  订单信息
	 * @param   array   $payment    支付方式信息
	 */
	function get_code($order, $payment)
	{
		require_once ROOT_PATH . ADMIN_PATH . '/includes/lib_accounting.php';

		global $actgHooks;

		if($this->is_sandbox){
			$publishable_key = 'pk_test_E0Stl002F54obSM0TUWwAgix';
			$secret_key      = 'sk_test_fB3Ene1AWkvnEc0IdfHLTgEH';
		} else {
			$publishable_key = $payment['stripe_publishable_key'];
			$secret_key      = $payment['stripe_secret_key'];
		}
		$stripe = array(
				'secret_key'      => $secret_key,
				'publishable_key' => $publishable_key
		);

		\Stripe\Stripe::setApiKey($stripe['secret_key']);

		$_SESSION['log_id'] 	 = $order['log_id'];
		$_SESSION['order_id'] 	 = $order['order_id'];
		$_SESSION['data_amount'] = $order['order_amount'];
		$pay_log                 = $actgHooks->paymentModuleGetPayLog(get_class($this), $order['log_id']);
		$btn_class				 = defined('YOHO_MOBILE') ? 'btn btn-default' : 'action-button';
		$currency 				 = !empty($pay_log['currency_code']) ? $pay_log['currency_code'] : $payment['stripe_currency'];
		$amount					 = $order['order_amount'] *100 ;

		$def_url = '
		<script src="https://js.stripe.com/v3/"></script>
		<form action='.return_url(basename(__FILE__, '.php')).' method="post" id="stripe-wc-payment-form" class="stripe-form" style="text-align: center;">'.
			'<input type="hidden" id="publishableKey" value="'.$publishable_key.'">
			<input type="hidden" name="amount" id="amount" value="'.$amount.'">
			<input type="hidden" name="order_id" id="order_id" value="'.$order['order_id'].'">
			<input type="hidden" name="log_id" id="log_id" value="'.$order['log_id'].'">
			<input type="hidden" name="pay_code" id="pay_code" value="'.basename(__FILE__, '.php').'">
			<input type="hidden" name="currency" id="currency" value="'.$currency.'">
			<input type="hidden" name="source" id="source">
			<a id="qrCode"><img src="/yohohk/img/imgloading.gif"/></a>
		</form>'.
		"<div id='processing-model' class='payment_processing hidden'>
			<div class='p-mask'></div>
			<div class='p-modal-frame'>
				<div class='p-modal-body' id = 'model-body'>
					"._L('payment_processing', '正在處理您的付款，這可能需要數分鐘時間…')."
				</div>
			</div>
		</div>"
				;
		if(!defined('YOHO_MOBILE')){
			$def_url .= "<script>
							$(function() {
								YOHO.payment.stripeWechat();
							});
						</script>";
		}

		return $def_url;
	}

	/**
	* 响应操作
	*/
	function respond()
	{

		if(isset($_SESSION['processing']['stripe'])) return false;

		$payment = get_payment('stripewechat');
		$log_id   = $_SESSION['log_id'];
		$order_id = $_SESSION['order_id'];

		// If QR code img is exists, delete it
		$img = "/images/tmpqrcode/".$order_id.".png";
		if (file_exists($img)) unlink($img);

		$_SESSION['processing']['stripe'] = $log_id;

		if (!isset($_REQUEST['source'])) {

			unset($_SESSION['processing']['stripe']);
			return false;
		}

		if($this->is_sandbox){
			$publishable_key = 'pk_test_E0Stl002F54obSM0TUWwAgix';
			$secret_key      = 'sk_test_fB3Ene1AWkvnEc0IdfHLTgEH';
		} else {
			$publishable_key = $payment['stripe_publishable_key'];
			$secret_key      = $payment['stripe_secret_key'];
		}

		$stripe = array(
				'secret_key'      => $secret_key,
				'publishable_key' => $publishable_key
		);

		\Stripe\Stripe::setApiKey($stripe['secret_key']);

		//Check amount
		$sql = "SELECT * FROM " . $GLOBALS['ecs']->table('pay_log') . " WHERE log_id = '" . $log_id . "'";
		$pay_log = $GLOBALS['db']->getRow($sql);

		// Get Order Info
		$oSql = "SELECT o.order_sn, o.consignee, o.email, o.tel, o.mobile, o.address, o.order_status, o.pay_status, o.shipping_status, r.region_name, u.reg_time, u.last_login, u.last_ip, u.user_name, u.new_user_name, u.rank_points, u.pay_points ".
				" FROM " . $GLOBALS['ecs']->table('order_info') ." as o ".
				" LEFT JOIN " . $GLOBALS['ecs']->table('region') ." as r ON r.region_id = o.country ".
				" LEFT JOIN " . $GLOBALS['ecs']->table('users') ." as u ON o.user_id = u.user_id ".
				" WHERE order_id = '" . $order_id . "'";

		$order_info = $GLOBALS['db']->getRow($oSql);
		$order_info['reg_time_formatted'] = local_date($GLOBALS['_CFG']['date_format'], $order_info['reg_time']);
		$order_info['last_login_formatted'] = local_date($GLOBALS['_CFG']['date_format'], $order_info['last_login']);
		if($order_info['rank_points'] > 1000 ){
			$order_info['rank_points_op'] = true;
		} else {
			$order_info['rank_points_op'] = false;
		}
		if ($pay_log['order_amount'] != $_SESSION['data_amount'])
		{
			unset($_SESSION['processing']['stripe']);
			return false;

		} else {

			try {

				$currency = !empty($pay_log['currency_code']) ? $pay_log['currency_code'] : $payment['stripe_currency'];
				$charge = \Stripe\Charge::create(array(
					'source'   => $_REQUEST['source'],
					'amount'   => $_SESSION['data_amount'] * 100,
					'metadata' => array(
						'order_id'  	 => $order_info['order_sn'],
						'consignee' 	 => $order_info['consignee'],
						'email'     	 => $order_info['email'],
						'tel'       	 => $order_info['tel'],
						'mobile'    	 => $order_info['mobile'],
						'log_id'    	 => $log_id,
						'address'   	 => $order_info['address'],
						'register_time'  => $order_info['reg_time_formatted'],
						'last_login'     => $order_info['last_login_formatted'],
						'country'        => $order_info['region_name'],
						'ip'			 => $order_info['last_ip'],
						'register_tel'   => $order_info['user_name'],
						'rank_points'    => $order_info['rank_points'],
						'rank_points_op' => $order_info['rank_points_op']

					),
					'currency' => $currency
				));

			} catch (Exception $e) {
				$error = $e->getMessage();
				/* 记录订单操作记录 */
                order_action($order_info['order_sn'], $order_info['order_status'], $order_info['shipping_status'], $order_info['pay_status'], '付款失敗(Stripe):'.$error, $GLOBALS['_LANG']['buyer']);
				unset($_SESSION['processing']['stripe']);
				return false;
			}
			if (!$error) {

				$txn_id = $charge->id;
				$action_note = $GLOBALS['_LANG']['stripe_txn_id'] . ':' . $txn_id . '（' . $pay_log['currency_code'] . $_SESSION['data_amount'] . '）';

				/* 改变订单状态 */
	    		order_paid($log_id, PS_PAYED, $action_note);

				unset($_SESSION['processing']['stripe']);
				unset($_SESSION['log_id']);
				unset($_SESSION['order_id']);
				unset($_SESSION['data_amount']);

				return true;

			} else {

				unset($_SESSION['processing']['stripe']);
				return false;
			}
		}

	}
}
?>
