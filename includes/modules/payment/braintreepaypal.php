<?php

/**
 * ECSHOP BrainTree Paypal 插件
 * $Author: Anthony
 * $Id: braintree.php 17-03-2017
 */

require_once ROOT_PATH . 'includes/braintree/lib/Braintree.php';

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

$payment_lang = ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/payment/braintree.php';

if (file_exists($payment_lang))
{
    global $_LANG;

    include_once($payment_lang);
}

/* 模組的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
    $i = isset($modules) ? count($modules) : 0;

    /* 代碼 */
    $modules[$i]['code']    = basename(__FILE__, '.php');

    /* 描述对应的语言项 */
    $modules[$i]['desc']    = 'braintree_paypal_desc';

    /* 是否支持货到付款 */
    $modules[$i]['is_cod']  = '0';

    /* 是否支持在线支付 */
    $modules[$i]['is_online']  = '1';

    /* 作者 */
    $modules[$i]['author']  = 'Anthony';

    /* 网	址 */
    $modules[$i]['website'] = '';

    /* 版本号 */
    $modules[$i]['version'] = '1.0.0';

    /* 配置信息 */
    $modules[$i]['config'] = array(
        array('name' => 'braintree_merchantId', 'type' => 'text', 'value' => ''),
        array('name' => 'braintree_publicKey', 'type' => 'text', 'value' => ''),
    	array('name' => 'braintree_privateKey', 'type' => 'text', 'value' => ''),
		array('name' => 'paypal_currency', 'type' => 'select', 'value' => 'HKD')
    );

    return;
}

/**
 * 类
 */
class braintreepaypal
{

    var $is_sandbox = false;

    /**
     * 构造函数
     *
     * @access  public
     * @param
     *
     * @return void
     */
    function braintreepaypal()
    {
    }

    function __construct()
    {
        $this->braintreepaypal();
    }

    /**
     * 生成支付代码
     * @param   array   $order  订单信息
     * @param   array   $payment    支付方式信息
     */
    function get_code($order, $payment)
    {
        require_once ROOT_PATH . ADMIN_PATH . '/includes/lib_accounting.php';

        global $actgHooks;
        $pay_log = $actgHooks->paymentModuleGetPayLog(get_class($this), $order['log_id']);
		$btn_class          = defined('YOHO_MOBILE') ? 'btn btn-primary btn-lg' : 'btn-css3';

        if($this->is_sandbox){
	        $environment = Braintree\Configuration::environment('sandbox');
	        $merchantId  = Braintree\Configuration::merchantId('jmpxzcdhxt43tm5r');
	        $publicKey   = Braintree\Configuration::publicKey('hv5mkc76ny2yxdqt');
	        $privateKey  = Braintree\Configuration::privateKey('52f760160a3b674f81d883d853ce7283');
        } else {
        	$environment = Braintree\Configuration::environment('production');
        	$merchantId  = Braintree\Configuration::merchantId($payment['braintree_merchantId']);
        	$publicKey   = Braintree\Configuration::publicKey($payment['braintree_publicKey']);
        	$privateKey  = Braintree\Configuration::privateKey($payment['braintree_privateKey']);
        }

        $_SESSION['log_id'] = $order['log_id'];
        $_SESSION['data_amount'] = $order['order_amount'];
        $_SESSION['order_id'] = $order['order_id'];

        $paypal_flow   = 'checkout';
        $currency_code = !empty($pay_log['currency_code']) ? $pay_log['currency_code'] : $payment['paypal_currency'];
        $data_lc = $this->get_user_locale();

        $address_data = $this->get_address_from_order($order);
        $pass_address = $this->should_pass_address($address_data);
        if ($pass_address)
        {
        	$data_address1 = htmlspecialchars($address_data['address1']);
        	$data_city = htmlspecialchars($address_data['city']);
        	$data_country = htmlspecialchars($address_data['country']);
        	$data_first_name = htmlspecialchars($address_data['first_name']);
        	$data_last_name = htmlspecialchars($address_data['last_name']);
        	$data_state = htmlspecialchars($address_data['state']);
        	$data_zip = htmlspecialchars($address_data['zip']);
        }

	    $pay_log = $actgHooks->paymentModuleGetPayLog(get_class($this), $order['log_id']);
        $clientToken = Braintree_ClientToken::generate();
        $def_url =
  			"<form action='".return_url(basename(__FILE__, '.php'))."' method='post' id='cardForm'>".
  			"<input type='hidden' name='payment_method_nonce'>".
  			"<input type='hidden' name='device_data'>".
  			"</form>".
        	"<div class='paypal-btn-frame' style='width: 100%;text-align: center;'>".
			"<input type='button' id='paypal-button' class='" . $btn_class . " graybtn' disabled='disabled' value='" . _L('payment_module_paypal_btn', '立即使用 PayPal 付款') . "' /></div>".
			"<script src='https://js.braintreegateway.com/web/3.20.0/js/client.js'></script>".
			"<script src='https://js.braintreegateway.com/web/3.20.0/js/paypal.min.js'></script>".
			"<script src='https://www.paypalobjects.com/api/checkout.js'></script>".
			"<script>
				// Fetch the button you are using to initiate the PayPal flow
				var paypalButton = document.getElementById('paypal-button');
				var form = document.querySelector('#cardForm');

				document.addEventListener('DOMContentLoaded', function(event) {
					paypalButton.removeAttribute('disabled', 'disabled');
					paypalButton.classList.remove('graybtn');
				});

				// Create a Client component
				braintree.client.create({
				  authorization: '".$clientToken."'
				}, function (clientErr, clientInstance) {
				  // Create PayPal component
				  braintree.paypal.create({
				    client: clientInstance
				  }, function (err, clientInstance){
				  		braintree.dataCollector.create({
					    client: clientInstance,
					    kount: true
					  }, function (err, dataCollectorInstance) {
					    if (err) {
					      // Handle error in creation of data collector
					      return;
					    }
					    var deviceData = dataCollectorInstance.deviceData;
				  		document.querySelector('input[name=\"device_data\"]').value = deviceData;
					  });
				  }, function (err, paypalInstance) {
				    paypalButton.addEventListener('click', function () {
					  paypalButton.setAttribute('disabled', 'disabled');
					  paypalButton.classList.add('graybtn');
					  paypalButton.setAttribute('value','" . _L('payment_processing', '正在處理您的付款，這可能需要數分鐘時間…') . "');
					  // Tokenize here!
				      paypalInstance.tokenize({
				        flow: '".$paypal_flow."', // Required
				        amount: ".$order['order_amount'].", // Required
				        currency: '".$currency_code."', // Required
				        locale: '".$data_lc."',".
				    ($pass_address ?
				       "enableShippingAddress: true,
				        shippingAddressEditable: false,
				        shippingAddressOverride: {
				          recipientName: '$data_first_name $data_last_name',
				          line1: '$data_address1',
				          line2: '',
				          city: '$data_city',
				          countryCode: '$data_country',
				          postalCode: '$data_zip',
				          state: '$data_state'
				        }"
		    		:
			    		"enableShippingAddress: false"
		    		).
				      "}, function (err, tokenizationPayload) {
				        	if (err) {
            					// console.error('Regular PayPal tokenization failed:', err);
								paypalButton.removeAttribute('disabled', 'disabled');
								paypalButton.classList.remove('graybtn');
								paypalButton.setAttribute('value','" ._L('payment_module_paypal_btn', '立即使用 PayPal 付款') . "');
          					} else {
				      			document.querySelector('input[name=\"payment_method_nonce\"]').value = tokenizationPayload.nonce;
				      			form.submit();
							}
				      });
				    });
				  });
				});
			</script>";

        return $def_url;
    }

    /**
     * 响应操作
     */
    function respond()
    {

    	$payment = get_payment('braintreepaypal');
    	$nonceFromTheClient = $_POST["payment_method_nonce"];
    	$log_id = $_SESSION['log_id'];
        $device_data = stripslashes($_POST["device_data"]);
		$order_id = $_SESSION['order_id'];

		// Check sandbox
    	if($this->is_sandbox){
    		$environment = Braintree\Configuration::environment('sandbox');
    		$merchantId  = Braintree\Configuration::merchantId('jmpxzcdhxt43tm5r');
    		$publicKey   = Braintree\Configuration::publicKey('hv5mkc76ny2yxdqt');
    		$privateKey  = Braintree\Configuration::privateKey('52f760160a3b674f81d883d853ce7283');
    	} else {
    		$environment = Braintree\Configuration::environment('production');
    		$merchantId  = Braintree\Configuration::merchantId($payment['braintree_merchantId']);
    		$publicKey   = Braintree\Configuration::publicKey($payment['braintree_publicKey']);
    		$privateKey  = Braintree\Configuration::privateKey($payment['braintree_privateKey']);
    	}

    	$sql = "SELECT * FROM " . $GLOBALS['ecs']->table('pay_log') . " WHERE log_id = '" . $log_id . "'";
    	$pay_log = $GLOBALS['db']->getRow($sql);
    	if ($pay_log['order_amount'] != $_SESSION['data_amount'])
    	{
    		return false;
    	} else {

			// Get Order Info
            $oSql = "SELECT o.order_sn, o.consignee, o.email, o.tel, o.mobile, o.address, o.pay_id, r.region_name, u.reg_time, u.last_login, u.last_ip, u.user_name, u.new_user_name, u.rank_points, u.pay_points ".
                " FROM " . $GLOBALS['ecs']->table('order_info') ." as o ".
                " LEFT JOIN " . $GLOBALS['ecs']->table('region') ." as r ON r.region_id = o.country ".
                " LEFT JOIN " . $GLOBALS['ecs']->table('users') ." as u ON o.user_id = u.user_id ".
                " WHERE order_id = '" . $order_id . "'";

			$order_info = $GLOBALS['db']->getRow($oSql);
            $order_info['reg_time_formatted'] = local_date($GLOBALS['_CFG']['date_format'], $order_info['reg_time']);
            $order_info['last_login_formatted'] = local_date($GLOBALS['_CFG']['date_format'], $order_info['last_login']);

    		$result = Braintree\Transaction::sale([
    				'amount' => $_SESSION['data_amount'],
    				'paymentMethodNonce' => $nonceFromTheClient,
    				'options' => [
						"paypal" => [
							"description" =>"友和 YOHO - $order_info[order_sn]",
						],
						'submitForSettlement' => true
					],
    				"deviceData" => $device_data,
					"customFields"      => [
                        'order_id'  	=> $order_info['order_sn'],
						'consignee' 	=> $order_info['consignee'],
						'email'     	=> $order_info['email'],
						'tel'       	=> $order_info['tel'],
						'mobile'    	=> $order_info['mobile'],
						'log_id'    	=> $log_id,
						'address'   	=> $order_info['address'],
						'register_time' => $order_info['reg_time_formatted'],
						'last_login'    => $order_info['last_login_formatted'],
						'country'       => $order_info['region_name'],
						'ip'			=> $order_info['last_ip'],
						'register_tel'  => $order_info['user_name'],
						'rank_points'   => $order_info['rank_points']
                    ]
    		]);

			$fraud = FRAUD_CLEAR;
			if ($result->transaction->gatewayRejectionReason == "fraud") {
				$fraud = FRAUD_CONFIRM;
			} elseif ($result->transaction->cvvResponseCode == "N") {
				$fraud = FRAUD_HIGH;
			}
			if (!empty($result->transaction->riskData) && $fraud == FRAUD_CLEAR) {
				if ($result->transaction->riskData->decision == "Decline") {
					$fraud = FRAUD_CONFIRM;
				} elseif ($result->transaction->riskData->decision != "Approve") {
					$fraud = FRAUD_MEDIUM;
				}
			}
			if ($result->transaction->cvvResponseCode != "M" && $fraud == FRAUD_CLEAR) {
				$fraud = FRAUD_LOW;
			}


    		if ($result->success) {

				$txn_id = $result->transaction->id;
				payment_risk_level_log($txn_id, $order_id, $order_info['pay_id'], $fraud, 1);
    			$action_note = $GLOBALS['_LANG']['braintree_paypal_txn_id'] . ':' . $txn_id . '（' . $pay_log['currency_code'] . $_SESSION['data_amount'] . '）';
    			/* 改变订单状态 */
    			order_paid($log_id, PS_PAYED, $action_note);
				$fraudController = new Yoho\cms\Controller\FraudController();
				$fraudController->fraudOrderControl($order_id, $fraudController::PAYMENT_CODE_PAYPAL, $result->transaction);
    			return true;

    		} else if ($result->transaction) {

    			//For test:

//     			 echo("Error processing transaction:");
//     			 echo("\n  code: " . $result->transaction->processorResponseCode);
//     			 echo("\n  text: " . $result->transaction->processorResponseText);

				$error = "(".$result->transaction->processorResponseCode.")".$result->transaction->processorResponseText;
				/* 记录订单操作记录 */
				payment_risk_level_log($result->transaction->id, $order_id, $order_info['pay_id'], $fraud);
				order_action($order_info['order_sn'], $order_info['order_status'], $order_info['shipping_status'], $order_info['pay_status'], '付款失敗(Paypal_transaction):'.$error, $GLOBALS['_LANG']['buyer']);

    			return false;

    		} else {

    			//For test:

//     			 echo("Validation errors: \n");
//     			 echo($result->errors->deepAll());
				$error = '';
    			foreach($result->errors->deepAll() AS $errorlog) {
					$error .= $errorlog->code . ": " . $errorlog->message . "\n";
				}
				order_action($order_info['order_sn'], $order_info['order_status'], $order_info['shipping_status'], $order_info['pay_status'], '付款失敗(Paypal_error):'.$error, $GLOBALS['_LANG']['buyer']);

    			return false;
    		}

    	}

    }

    /**
     * Get address in PayPal format
     */
    function get_address_from_order($order)
    {
    	$address = array();

    	// First Name / Last Name
    	$pos = strrpos($order['consignee'], ' ');
    	if ((!empty($order['first_name'])) && (!empty($order['last_name'])))
    	{
    		$address['first_name'] = $order['first_name'];
    		$address['last_name'] = $order['last_name'];
    	}
    	elseif (($pos > 0) && ($pos < strlen($order['consignee']) - 1)) // if the consignee name have a space in the middle
    	{
    		$address['first_name'] = substr($order['consignee'], 0, $pos);
    		$address['last_name'] = substr($order['consignee'], $pos + 1);
    	}
    	else
    	{
    		$address['first_name'] = $order['consignee'];
    		$address['last_name'] = '';
    	}

    	// Street Address
    	$address['address1'] = ($order['address'] != 'NULL') ? $order['address'] : '';

    	// Country code
    	$address['country'] = strtoupper(get_country_code_by_region($order['country']));

    	// City name (here we set it to country name)
    	$sql = "SELECT `region_name` FROM " . $GLOBALS['ecs']->table('region') . " WHERE region_id = '" . $order['country'] . "'";
    	$address['city'] = $GLOBALS['db']->getOne($sql);

    	// Postal Code / US Zip Code
    	$address['zip'] = (empty($order['zipcode'])) ? '00000' : (($order['zipcode'] != 'NULL') ? $order['zipcode'] : '00000');

    	// Handle US city and state
    	if ($address['country'] == 'US')
    	{
    		$sql = "SELECT city, state_code " .
    				"FROM " . $GLOBALS['ecs']->table('zipcodes') .
    				"WHERE country_code = '" . $address['country'] . "' " .
    				"AND zipcode = '" . $address['zip'] . "'";
    		$res = $GLOBALS['db']->getRow($sql);
    		if ($res)
    		{
    			$address['city'] = $res['city'];
    			$address['state'] = $res['state_code'];
    		}
    	}
    	// Handle Canada province
    	if ($address['country'] == 'CA')
    	{
    		// Canada postal code is formatted like H0H 0H0
    		// Our lookup table only support first 3 char
    		$sql = "SELECT city, state_code " .
    				"FROM " . $GLOBALS['ecs']->table('zipcodes') .
    				"WHERE country_code = '" . $address['country'] . "' " .
    				"AND zipcode = '" . substr($address['zip'],0,3) . "'";
    		$res = $GLOBALS['db']->getRow($sql);
    		if ($res)
    		{
    			$address['state'] = $res['state_code'];
    		}
    	}
    	// Handle China province
    	if ($address['country'] == 'CN')
    	{
    		// Search for an exact match or the largest postal code smaller than it
    		$sql = "SELECT city, state " .
    				"FROM " . $GLOBALS['ecs']->table('zipcodes') .
    				"WHERE country_code = '" . $address['country'] . "' " .
    				"AND zipcode <= '" . $address['zip'] . "' ORDER BY zipcode DESC LIMIT 1";
    		$res = $GLOBALS['db']->getRow($sql);
    		if ($res)
    		{
    			$address['city'] = $res['city'];
    			$address['state'] = $res['state'];
    		}
    	}

    	return $address;
    }

    /**
     * Determinate whether or not to pass address to PayPal
     */
    function should_pass_address($address)
    {
    	// Check for address completeness
    	if (empty($address['first_name']) ||
    			empty($address['address1']) ||
    			empty($address['country']))
    	{
    		return false;
    	}
    	if (in_array($address['country'], array('US', 'CA', 'GB', 'CN')))
    	{
    		if (empty($address['zip']) ||
    				empty($address['city']) ||
    				empty($address['state']))
    		{
    			return false;
    		}
    	}
    	return true;
    }

    /**
     * Return a PayPal locale code, based on user's country and language
     * ref: https://developer.paypal.com/docs/classic/api/locale_codes/
     */
    function get_user_locale()
    {
    	$user_area = user_area();
    	$user_lang = $GLOBALS['_CFG']['lang'];
    	$user_lang2 = substr($user_lang, 0, 2);

    	if ($user_area == 'HK')
    	{
    		return ($user_lang2 == 'zh') ? 'zh_HK' : 'en_GB';
    	}
    	else if ($user_area == 'TW')
    	{
    		return ($user_lang2 == 'zh') ? 'zh_TW' : 'en_US';
    	}
    	else if ($user_area == 'CN')
    	{
    		return ($user_lang2 == 'zh') ? 'zh_CN' : 'en_US';
    	}
    	else
    	{
    		return ($user_lang2 == 'zh') ? 'zh_TW' : 'en_US';
    	}
    }

    /**
     * Get address from IPN POSTed variables
     */
    function get_address_from_ipn()
    {
    	$address = '';
    	if (!empty($_POST['first_name']))
    	{
    		$address .= $_POST['first_name'] . ' ';
    	}
    	if (!empty($_POST['last_name']))
    	{
    		$address .= $_POST['last_name'] . ' ';
    	}
    	if (!empty($_POST['address_name']))
    	{
    		$address .= '(' . $_POST['address_name'] . ') ';
    	}
    	if (!empty($_POST['contact_phone']))
    	{
    		$address .= $_POST['contact_phone'] . ', ';
    	}
    	if (!empty($_POST['address_street']))
    	{
    		$address .= $_POST['address_street'] . ', ';
    	}
    	if (!empty($_POST['address_city']))
    	{
    		$address .= $_POST['address_city'] . ', ';
    	}
    	if (!empty($_POST['address_state']))
    	{
    		$address .= $_POST['address_state'] . ' ';
    	}
    	if (!empty($_POST['address_zip']))
    	{
    		$address .= $_POST['address_zip'] . ' ';
    	}
    	if (!empty($_POST['address_country']))
    	{
    		$address .= $_POST['address_country'] . ' ';
    	}
    	if (!empty($_POST['address_country_code']))
    	{
    		$address .= '(' . $_POST['address_country_code'] . ') ';
    	}
    	if (!empty($_POSTS['address_status']))
    	{
    		$address .= '(Status: ' . $_POST['address_status'] . ')';
    	}
    	return $address;
    }
}
?>
