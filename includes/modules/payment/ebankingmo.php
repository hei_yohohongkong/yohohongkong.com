<?php

/**
 * ECSHOP 網上銀行轉帳 插件
 * ============================================================================
 * * 版权所有 2005-2012 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liubo $
 * $Id: bank.php 17217 2011-01-19 06:29:08Z liubo $
 */

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

$payment_lang = ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/payment/ebankingmo.php';

if (file_exists($payment_lang))
{
    global $_LANG;

    include_once($payment_lang);
}

/* 模块的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
    $i = isset($modules) ? count($modules) : 0;

    /* 代码 */
    $modules[$i]['code']    = basename(__FILE__, '.php');

    /* 描述对应的语言项 */
    $modules[$i]['desc']    = 'ebankingmo_desc';

    /* 支付手續費 */
    $modules[$i]['pay_fee']    = '-3%';

    /* 是否支持货到付款 */
    $modules[$i]['is_cod']  = '0';

    /* 是否支持在线支付 */
    $modules[$i]['is_online']  = '1';

    /* 作者 */
    $modules[$i]['author']  = 'dem';

    /* 网址 */
    $modules[$i]['website'] = '';

    /* 版本号 */
    $modules[$i]['version'] = '1.0.0';

    /* 配置信息 */
    $modules[$i]['config']  = array(
        // array('name'=>'desc_zh_tw', 'type'=>'textarea', 'value'=>'轉帳後請上傳收據，以便核實您的付款。'),
        // array('name'=>'desc_zh_cn', 'type'=>'textarea', 'value'=>'转帐后请上传收据，以便核实您的付款。'),
        // array('name'=>'desc_en_us', 'type'=>'textarea', 'value'=>'To verify your payment, please upload your receipt after the transfer.'),
	    array('name'=>'bank_user', 'type'=>'text', 'value'=>'胡發枝 / Wu Faat Chi'),
	    array('name'=>'banks', 'type' => 'hidden','value'=>array(
		    array('name'=>'澳門中國銀行','account'=>'01-11-10-576151'),
		    array('name'=>'澳門工商銀行','account'=>'0119100100004889869')
        )),
        array('name'=>'tutorial', 'type'=>'hidden', 'value'=>'轉帳後請上傳收據，以便核實您的付款。'),
    );

    return;
}

/**
 * 类
 */
class ebankingmo
{
    /**
     * 构造函数
     *
     * @access  public
     * @param
     *
     * @return void
     */
    function ebankingmo()
    {
    }

    function __construct()
    {
        $this->ebankingmo();
    }

    /**
     * 生成支付代码
     * @param   array   $order  订单信息
     * @param   array   $payment    支付方式信息
     */
    function get_code($order, $payment)
    {
        $lang = $GLOBALS['_CFG']['lang'];
        $html =
            '<div class="huikuan-intro bank">
            <div class="step">' .
            (defined('YOHO_MOBILE') ? '' : ('<h4>'. _L('payment_bank_transfer', 'ATM轉帳 | 網上銀行轉帳').'</h4>')) .
            '<div id="banks">
                <p><span class="gray">'. _L('payment_bank_account_name', '帳戶名稱: ').'</span>' . $payment['bank_user'] . '</p>';
        foreach ($payment['banks'] as $bank) {
            $html .= '<p><span class="gray">' . $bank['name'] . ': </span>' . $bank['account'] . '</p>';
        }
        $html .= '</div></div><div class="step" style="padding-left: 15px; font-weight: 700; text-align: left">'. (empty($payment["tutorial"]) ? _L('payment_bank_after_transfer', '立即轉帳至以上一個銀行戶口，完成轉帳後點擊上傳證明') : nl2br($payment["tutorial"])) .'</div></div>';
        return $html;
    }

    /**
     * 处理函数
     */
    function response()
    {
        return false;
    }
}

?>
