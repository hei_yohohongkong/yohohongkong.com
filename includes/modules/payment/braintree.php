<?php

/**
 * ECSHOP BrainTree Creadit Card 插件
 * $Author: Anthony
 * $Id: braintree.php 15-03-2017
 */

require_once ROOT_PATH . 'includes/braintree/lib/Braintree.php';

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

$payment_lang = ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/payment/braintree.php';

if (file_exists($payment_lang))
{
    global $_LANG;

    include_once($payment_lang);
}

/* 模組的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
    $i = isset($modules) ? count($modules) : 0;

    /* 代碼 */
    $modules[$i]['code']    = basename(__FILE__, '.php');

    /* 描述对应的语言项 */
    $modules[$i]['desc']    = 'braintree_desc';

    /* 是否支持货到付款 */
    $modules[$i]['is_cod']  = '0';

    /* 是否支持在线支付 */
    $modules[$i]['is_online']  = '1';

    /* 作者 */
    $modules[$i]['author']  = 'Anthony';

    /* 网	址 */
    $modules[$i]['website'] = '';

    /* 版本号 */
    $modules[$i]['version'] = '1.0.0';

    /* 配置信息 */
    $modules[$i]['config'] = array(
        array('name' => 'braintree_merchantId', 'type' => 'text', 'value' => ''),
        array('name' => 'braintree_publicKey', 'type' => 'text', 'value' => ''),
    	array('name' => 'braintree_privateKey', 'type' => 'text', 'value' => '')
    );

    return;
}

/**
 * 类
 */
class braintree
{

    var $is_sandbox = false;

    /**
     * 构造函数
     *
     * @access  public
     * @param
     *
     * @return void
     */
    function braintree()
    {
    }

    function __construct()
    {
        $this->braintree();
    }

    /**
     * 生成支付代码
     * @param   array   $order  订单信息
     * @param   array   $payment    支付方式信息
     */
    function get_code($order, $payment)
    {
        require_once ROOT_PATH . ADMIN_PATH . '/includes/lib_accounting.php';

        global $actgHooks;

        if($this->is_sandbox){
	        $environment = Braintree\Configuration::environment('sandbox');
	        $merchantId  = Braintree\Configuration::merchantId('jmpxzcdhxt43tm5r');
	        $publicKey   = Braintree\Configuration::publicKey('hv5mkc76ny2yxdqt');
	        $privateKey  = Braintree\Configuration::privateKey('52f760160a3b674f81d883d853ce7283');
        } else {
        	$environment = Braintree\Configuration::environment('production');
        	$merchantId  = Braintree\Configuration::merchantId($payment['braintree_merchantId']);
        	$publicKey   = Braintree\Configuration::publicKey($payment['braintree_publicKey']);
        	$privateKey  = Braintree\Configuration::privateKey($payment['braintree_privateKey']);
        }

        $_SESSION['log_id'] 	 = $order['log_id'];
        $_SESSION['order_id'] 	 = $order['order_id'];
        $_SESSION['data_amount'] = $order['order_amount'];
        $pay_log                 = $actgHooks->paymentModuleGetPayLog(get_class($this), $order['log_id']);
        $clientToken             = Braintree_ClientToken::generate();
        $btn_class				 = defined('YOHO_MOBILE') ? 'btn btn-default pay-btn' : 'action-button pay-btn';

        $def_url = " <div class='braintree-frame' id='braintree-creadit-card'> ".
  			"<form action='".return_url(basename(__FILE__, '.php'))."' method='post' id='brain-cardForm' class='brain-form' >".
  			"<div class='cardinfo-card-number'>
		      	<div class='brain-label'><label class='hosted-fields--label' for='card-number'>"._L('card_number', '信用卡號碼')."</label></div>
		      	<div class='input-wrapper brain-input' id='card-number'></div>
		      	<div id='card-image'></div>
		    </div>".
    		"<div class='cardinfo-wrapper'>
		      	<div class='cardinfo-exp-date'>
		        	<div class='brain-label'><label class='cardinfo-label' for='expiration-date'>"._L('expiration_date', '有效日期')."</label></div>
		        	<div class='input-wrapper brain-input' id='expiration-date'></div>
		      	</div>
		      	<div class='cardinfo-cvv'>
		        	<div class='brain-label'><label class='cardinfo-label' for='cvv'>CVV</label></div>
		        	<div class='input-wrapper brain-input' id='cvv'></div>
		      	</div>
		    </div>".
    		"<div class='brain-error' id='brainError' style='display:none;'></div>".
			"<div class='brain-button-container'>".
			"<input type='hidden' name='payment_method_nonce'>".
			"<input type='hidden' name='device_data'>".
			"<input type='hidden' name='clientToken' id='clientToken' value='".$clientToken."'>".
			"<input type='hidden' name='amount' id='amount' value='".$order['order_amount']."'>".
    		"<input type='submit' id='button-pay' class='$btn_class' value='"._L('sumbit', '提交')."'/>".
    		"</div></form></div>".
    		"<div id='modal' class='three-d-pay hidden'>
			  	<div class='bt-mask'></div>
			  	<div class='bt-modal-frame'>
			    	<div class='bt-modal-header'>
			      		<div class='header-text'>Authentication</div>
			    	</div>
			    	<div class='bt-modal-body'></div>
			    	<div class='bt-modal-footer'><a id='text-close' href='#'>Cancel</a></div>
			  	</div>
			</div>".
			"<script src='//js.braintreegateway.com/web/3.11.0/js/client.js'></script>".
			"<script src='//js.braintreegateway.com/web/3.11.0/js/three-d-secure.min.js'></script>".
			"<script src='//js.braintreegateway.com/web/3.11.0/js/data-collector.min.js'></script>".
			"<script src='//js.braintreegateway.com/web/3.11.0/js/hosted-fields.js'></script>";
			if(!defined('YOHO_MOBILE')){
				$def_url .= "<script>
							$(function() {
								YOHO.payment.braintree();
							});
						</script>";
			}

        return $def_url;
    }

    /**
     * 响应操作
     */
    function respond()
    {
    	$payment = get_payment('braintree');
		$nonceFromTheClient = $_POST["payment_method_nonce"];

    	$log_id = $_SESSION['log_id'];
		$device_data = stripslashes($_POST["device_data"]);
        $order_id = $_SESSION['order_id'];

		// Get Order Info
		$oSql = "SELECT o.order_sn, o.order_status, o.shipping_status, o.pay_status, o.consignee, o.email, o.tel, o.mobile, o.address, o.pay_id, r.region_name, u.reg_time, u.last_login, u.last_ip, u.user_name, u.new_user_name, u.rank_points, u.pay_points ".
			" FROM " . $GLOBALS['ecs']->table('order_info') ." as o ".
			" LEFT JOIN " . $GLOBALS['ecs']->table('region') ." as r ON r.region_id = o.country ".
			" LEFT JOIN " . $GLOBALS['ecs']->table('users') ." as u ON o.user_id = u.user_id ".
			" WHERE order_id = '" . $order_id . "'";

		$order_info = $GLOBALS['db']->getRow($oSql);

		// Dem's credentials: [fd3vmpwh4cy7qcv3,38k9tgrwxz324dkd,69863db467746b406b5a7e951c35caf8]
    	if($this->is_sandbox){
    		$environment = Braintree\Configuration::environment('sandbox');
    		$merchantId  = Braintree\Configuration::merchantId('jmpxzcdhxt43tm5r');
    		$publicKey   = Braintree\Configuration::publicKey('hv5mkc76ny2yxdqt');
    		$privateKey  = Braintree\Configuration::privateKey('52f760160a3b674f81d883d853ce7283');
    	} else {
    		$environment = Braintree\Configuration::environment('production');
    		$merchantId  = Braintree\Configuration::merchantId($payment['braintree_merchantId']);
    		$publicKey   = Braintree\Configuration::publicKey($payment['braintree_publicKey']);
    		$privateKey  = Braintree\Configuration::privateKey($payment['braintree_privateKey']);
    	}

    	$sql = "SELECT * FROM " . $GLOBALS['ecs']->table('pay_log') . " WHERE log_id = '" . $log_id . "'";
    	$pay_log = $GLOBALS['db']->getRow($sql);
    	if ($pay_log['order_amount'] != $_POST["amount"])
    	{
			$error = "BrainTree amount not match.";
			order_action($order_info['order_sn'], $order_info['order_status'], $order_info['shipping_status'], $order_info['pay_status'], '付款失敗(BrainTree_error):'.$error, $GLOBALS['_LANG']['buyer']);
    		return false;

    	} else {
            $order_info['reg_time_formatted'] = local_date($GLOBALS['_CFG']['date_format'], $order_info['reg_time']);
            $order_info['last_login_formatted'] = local_date($GLOBALS['_CFG']['date_format'], $order_info['last_login']);

			// if (!empty($nonceFromTheClient)) {
			// 	$result = Braintree_PaymentMethodNonce::find($nonceFromTheClient);
			// 	if (isset($result->details['bin']) && !empty($result->details['bin'])) {
			// 		$orderController = new Yoho\cms\Controller\OrderController();
			// 		$bin_result = $orderController->checkCouponPromoteCreditCard($order_id,$result->details['bin']);
			// 		if ($bin_result['success'] == false) {
			// 			$error = $bin_result['error_msg_system'];	
			// 			order_action($order_info['order_sn'], $order_info['order_status'], $order_info['shipping_status'], $order_info['pay_status'], '付款失敗:'.$error, $GLOBALS['_LANG']['buyer']);
			// 			return $bin_result;
			// 		} else {
			// 			if ($bin_result['checked']) {
			// 				order_action($order_info['order_sn'], $order_info['order_status'], $order_info['shipping_status'], $order_info['pay_status'], '信用卡檢查成功:BIN('.$result->details['bin'].')', $GLOBALS['_LANG']['buyer']);
			// 			}
			// 		}
			// 	}
			// }
			
    		$result = Braintree_Transaction::sale([
    				'amount'             => $_POST["amount"],
    				'paymentMethodNonce' => $nonceFromTheClient,
    				'options'            => [
                        'submitForSettlement' => true,
                        'threeDSecure' => [
                            'required' => true
                        ]
    				],
    				"deviceData"        => $device_data,
                    "customFields"      => [
                        'order_id'  	=> $order_info['order_sn'],
						'consignee' 	=> $order_info['consignee'],
						'email'     	=> $order_info['email'],
						'tel'       	=> $order_info['tel'],
						'mobile'    	=> $order_info['mobile'],
						'log_id'    	=> $log_id,
						'address'   	=> $order_info['address'],
						'register_time' => $order_info['reg_time_formatted'],
						'last_login'    => $order_info['last_login_formatted'],
						'country'       => $order_info['region_name'],
						'ip'			=> $order_info['last_ip'],
						'register_tel'  => $order_info['user_name'],
						'rank_points'   => $order_info['rank_points']
                    ]
			]);
			
			$fraud = FRAUD_CLEAR;
			if ($result->transaction->gatewayRejectionReason == "fraud") {
				$fraud = FRAUD_CONFIRM;
			} elseif ($result->transaction->cvvResponseCode == "N") {
				$fraud = FRAUD_HIGH;
			}
			if (!empty($result->transaction->riskData) && $fraud == FRAUD_CLEAR) {
				if ($result->transaction->riskData->decision == "Decline") {
					$fraud = FRAUD_CONFIRM;
				} elseif ($result->transaction->riskData->decision != "Approve") {
					$fraud = FRAUD_MEDIUM;
				}
			}
			if ($result->transaction->cvvResponseCode != "M" && $fraud == FRAUD_CLEAR) {
				$fraud = FRAUD_LOW;
			}

    		if ($result->success) {

    			$txn_id = $result->transaction->id;
				payment_risk_level_log($txn_id, $order_id, $order_info['pay_id'], $fraud, 1);
    			$action_note = $GLOBALS['_LANG']['braintree_txn_id'] . ':' . $txn_id . '（' . $pay_log['currency_code'] . $_SESSION['data_amount'] . '）';
    			unset($_SESSION['order_id']);
                /* 改变订单状态 */
    			order_paid($log_id, PS_PAYED, $action_note);
				$fraudController = new Yoho\cms\Controller\FraudController();
				$fraudController->fraudOrderControl($order_id, $fraudController::PAYMENT_CODE_BRAINTREE, $result->transaction);
    			return true;

    		} else if ($result->transaction) {

    			//For test:
//     			 echo("Error processing transaction:");
//     			 echo("\n  code: " . $result->transaction->processorResponseCode);
//     			 echo("\n  text: " . $result->transaction->processorResponseText);
				$error = "(".$result->transaction->processorResponseCode.")".$result->transaction->processorResponseText;
				/* 记录订单操作记录 */
				payment_risk_level_log($result->transaction->id, $order_id, $order_info['pay_id'], $fraud);
                order_action($order_info['order_sn'], $order_info['order_status'], $order_info['shipping_status'], $order_info['pay_status'], '付款失敗(BrainTree_transaction):'.$error, $GLOBALS['_LANG']['buyer']);

    			return false;

    		} else {

    			//For test:
    			$error = '';
    			foreach($result->errors->deepAll() AS $errorlog) {
					$error .= $errorlog->code . ": " . $errorlog->message . "\n";
				}
				order_action($order_info['order_sn'], $order_info['order_status'], $order_info['shipping_status'], $order_info['pay_status'], '付款失敗(BrainTree_error):'.$error, $GLOBALS['_LANG']['buyer']);
    			return false;
    		}

    	}

    }
}
?>
