<?php

/**
 * ECSHOP 魂付款插件
 * ============================================================================
 * * 版权所有 2005-2012 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liubo $
 * $Id: paypal.php 17217 2011-01-19 06:29:08Z liubo $
 */

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

$payment_lang = ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/payment/howang.php';

if (file_exists($payment_lang))
{
    global $_LANG;

    include_once($payment_lang);
}

/* 模块的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
    $i = isset($modules) ? count($modules) : 0;

    /* 代码 */
    $modules[$i]['code']    = basename(__FILE__, '.php');

    /* 描述对应的语言项 */
    $modules[$i]['desc']    = 'howang_desc';

    /* 是否支持货到付款 */
    $modules[$i]['is_cod']  = '0';

    /* 是否支持在线支付 */
    $modules[$i]['is_online']  = '1';

    /* 作者 */
    $modules[$i]['author']  = 'howang';

    /* 网址 */
    $modules[$i]['website'] = 'http://www.howang.hk';

    /* 版本号 */
    $modules[$i]['version'] = '1.0.0';

    /* 配置信息 */
    $modules[$i]['config'] = array();

    return;
}

/**
 * 类
 */
class howang
{
    /**
     * 构造函数
     *
     * @access  public
     * @param
     *
     * @return void
     */
    function howang()
    {
    }

    function __construct()
    {
        $this->howang();
    }

    /**
     * 生成支付代码
     * @param   array   $order  订单信息
     * @param   array   $payment    支付方式信息
     */
    function get_code($order, $payment)
    {
        require_once ROOT_PATH . ADMIN_PATH . '/includes/lib_accounting.php';
        global $actgHooks;
        $pay_log = $actgHooks->paymentModuleGetPayLog(get_class($this), $order['log_id']);
        
        $data_order_sn      = $order['order_sn'];
        $data_order_id      = $order['log_id'];
        $data_amount        = $pay_log['order_amount'];
        $data_currency      = $pay_log['currency_code'];
        $data_return_url    = return_url(basename(__FILE__, '.php'));
        $data_notify_url    = return_url(basename(__FILE__, '.php'));
        $cancel_return      = $GLOBALS['ecs']->url();

        $str  = '<div style="text-align:center;">';
        
        $str .= '<pre style="text-align:left; margin: 0 auto;">';
        $str .= 'Order SN: ' . $data_order_sn . "\n";
        $str .= 'Order ID: ' . $data_order_id . "\n";
        $str .= 'Amount: ' . $data_amount . "\n";
        $str .= 'Currency: ' . $data_currency . "\n";
        $str .= 'Return URL: ' . $data_return_url . "\n";
        $str .= 'Cancel URL: ' . $cancel_return . "\n";
        $str .= '</pre>';
        
        $str .= '<form action="' . $data_return_url . '" method="post">';
        $str .= '<input type="hidden" name="log_id" value="' . $data_order_id . '">';
        $str .= '<input type="submit" class="btn-css3" value="' . _L('user_order_pay_btn', '立即付款') . '">';
        $str .= '</form>';
        
        $str .= '</div>';
        
        return $str;
    }

    /**
     * 响应操作
     */
    function respond()
    {
        // process payment
        $payment  = get_payment($_GET['code']);
        $log_id = empty($_REQUEST['log_id']) ? '' : $_REQUEST['log_id'];
        
        if (!empty($log_id))
        {
            order_paid($log_id, PS_PAYED);
            return true;
        }
        else
        {
            return false;
        }
    }
}

?>