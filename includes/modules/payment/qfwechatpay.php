<?php

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

$payment_lang = ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/payment/qfwechatpay.php';

if (file_exists($payment_lang))
{
    global $_LANG;

    include_once($payment_lang);
}
// Skip update user info for faster loading
define('INIT_NO_UPDATE_USER_INFO', true);

require_once(ROOT_PATH . '/includes/phpqrcode/qrlib.php');

/* 模块的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
    $i = isset($modules) ? count($modules) : 0;

    /* 代码 */
    $modules[$i]['code']    = basename(__FILE__, '.php');

    /* 描述对应的语言项 */
    $modules[$i]['desc']    = 'qfwechatpay_desc';

    /* 是否支持货到付款 */
    $modules[$i]['is_cod']  = '0';

    /* 是否支持在线支付 */
    $modules[$i]['is_online']  = '1';
    $modules[$i]['is_online_pay']  = '1';

    /* 作者 */
    $modules[$i]['author']  = 'billy';

    /* 网址 */
    $modules[$i]['website'] = '';

    /* 版本号 */
    $modules[$i]['version'] = '1.0.0';

    /* 配置信息 */
    $modules[$i]['config']  = array(
        array('name' => 'qfwechatpay_key',               'type' => 'text',   'value' => ''),
        array('name' => 'qfwechatpay_code',           'type' => 'text',   'value' => '')
    );

    return;
}

/**
 * 类
 */
class qfwechatpay
{

    var $is_sandbox = false;
    var $webhook = true;
    var $api_refund = true;
    var $manual_api_refund_override = false;
    /**
     * 构造函数
     *
     * @access  public
     * @param
     *
     * @return void
     */
    function qfwechatpay()
    {
        $this->is_sandbox = (strpos($_SERVER['SERVER_NAME'], 'beta.yohohongkong.com') === false) ? false : true;
    }

    function __construct()
    {
        $this->qfwechatpay();
    }

    /**
     * 生成支付代码
     * @param   array   $order      订单信息
     * @param   array   $payment    支付方式信息
     */
    function get_code($order, $payment)
    {
        require_once ROOT_PATH . ADMIN_PATH . '/includes/lib_accounting.php';
        global $actgHooks;
        $pay_log = $actgHooks->paymentModuleGetPayLog(get_class($this), $order['log_id']);

        if (!defined('EC_CHARSET'))
        {
            $charset = 'utf-8';
        }
        else
        {
            $charset = EC_CHARSET;
        }

        $order_payment_id = $order['order_sn'] . $order['log_id'] . "_" . local_gettime();

        $parameter = array(
            'out_trade_no'      => $order_payment_id,
            'txamt'             => $pay_log['order_amount'] * 100,
            'txcurrcd'          => $pay_log['currency_code'],
            'pay_type'          => 800201,
            'txdtm'             => local_date("Y-m-d H:i:s")
        );

        ksort($parameter);
        reset($parameter);

        $sign  = '';

        foreach ($parameter AS $key => $val)
        {
            $sign  .= ecs_iconv(EC_CHARSET, 'utf-8', $key) . "=" . ecs_iconv(EC_CHARSET, 'utf-8', $val). "&";
        }

        $sign  = substr($sign, 0, -1). ecs_iconv(EC_CHARSET, 'utf-8', $payment['qfwechatpay_key']);

        $qfpay_host = $this->is_sandbox ? "https://openapi-test.qfpay.com" : "https://openapi.qfpay.com";
        $ch = curl_init($qfpay_host . '/trade/v1/payment');
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($parameter));
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            "X-QF-APPCODE: " . $payment['qfwechatpay_code'],
            "X-QF-SIGN: " . strtoupper(md5($sign)),
            "Content-Type: application/x-www-form-urlencoded",
            "Content-Length: " . strlen(http_build_query($parameter)),
        ]);

        $wechat_pay_creation_result = curl_exec($ch);

        if (curl_errno($ch) != 0) // cURL error
        {
            hw_error_log('Cannot connect to QFPay: ' . curl_error($ch));
            curl_close($ch);
            return false;
        }
        else
        {
            // Serarate response headers and body
            $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            $wechat_pay_creation_result_header = substr($wechat_pay_creation_result, 0, $header_size);
            $wechat_pay_creation_result = substr($wechat_pay_creation_result, $header_size);

            // Parse and compare the signature
            if (!$this->compareResponseSignature($wechat_pay_creation_result_header, $wechat_pay_creation_result, $payment['qfwechatpay_key'])) {
                curl_close($ch);
                return false;
            }
            
            curl_close($ch);
        }

        $result = '';
        $logo_element = '';
        
        // TODO: Geneate / fetch QR code and begin poll for payment status update
        $wechat_pay_creation_result = json_decode($wechat_pay_creation_result);

        // If there is no qr code produced
        if (!file_exists(ROOT_PATH .'/images/tmpqrcode/')) {
            mkdir(ROOT_PATH .'/images/tmpqrcode/', 0777, true);
        }
        $path = ROOT_PATH ."/images/tmpqrcode/".$order_payment_id."_wechat_pay.png";
        try {
            QRcode::png($wechat_pay_creation_result->qrcode, $path , 'L', 4, 2);
        } catch (Exception $e) {        // 被忽略
            echo make_json_error('系統錯誤');
        }

        $logo_element = defined('YOHO_MOBILE') ? "" : "<img src='/yohohk/img/payment-icon/wechatpay_vertical.svg' style='margin:5px;height:100%;max-height:100px;'></img>";

        $result = "<div style='text-align:center;margin:auto;padding-bottom:1rem;'><img src='/images/tmpqrcode/". $order_payment_id."_wechat_pay.png' style='width:100%;max-width:200px;'></img><br/><div style='display:flex;flex-direction:row;justify-content:center;'>".$logo_element."<img src='/yohohk/img/payment-icon/scan_code_".$GLOBALS['_CFG']['lang'].".png' style='margin:5px;height:100%;max-height:200px;align-self:center;'></img></div></div><script>var qfwechatpay_order_payment_id = '".$order_payment_id."';var webhook = ".($this->webhook ? 'false' : 'true')."</script>";

        return ($result);
    }

    /**
     * 响应操作
     */
    function respond()
    {
        return false;
    }

    function do_refund($tnx_id,$refund_amount)
    {
        // echo 'transcation_id:'.$tnx_id.'<br>';
        // echo 'refund_amount:'.$refund_amount;
        // echo 'refunded<br>';
        $weChatPayController =  new Yoho\cms\Controller\WeChatPayLogController();
        return $weChatPayController->refund_by_transaction_id_and_amount($tnx_id, $refund_amount);
    }

    private function compareResponseSignature($response_header, $response_body, $key)
    {
        // Get headers for sign checking
        $headers = [];
        $data = explode("\n",$response_header);
        $headers['status'] = $data[0];
        array_shift($data);

        foreach($data as $part){
            $middle = explode(":",$part);
            $headers[trim($middle[0])] = trim($middle[1]);
        }

        $result_sign  = strtoupper(md5($response_body . ecs_iconv(EC_CHARSET, 'utf-8', $key)));
        
        return (!empty($headers['X-QF-SIGN']) && ($headers['X-QF-SIGN'] == $result_sign));
    }
}

?>