<?php

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

$payment_lang = ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/payment/alipayhk.php';

if (file_exists($payment_lang))
{
    global $_LANG;

    include_once($payment_lang);
}
// Skip update user info for faster loading
define('INIT_NO_UPDATE_USER_INFO', true);

/* 模块的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
    $i = isset($modules) ? count($modules) : 0;

    /* 代码 */
    $modules[$i]['code']    = basename(__FILE__, '.php');

    /* 描述对应的语言项 */
    $modules[$i]['desc']    = 'alipayhk_desc';

    /* 是否支持货到付款 */
    $modules[$i]['is_cod']  = '0';

    /* 是否支持在线支付 */
    $modules[$i]['is_online']  = '1';
    $modules[$i]['is_online_pay']  = '1';

    /* 作者 */
    $modules[$i]['author']  = 'Billy';

    /* 网址 */
    $modules[$i]['website'] = '';

    /* 版本号 */
    $modules[$i]['version'] = '1.0.0';

    /* 配置信息 */
    $modules[$i]['config']  = array(
        array('name' => 'alipayhk_merchant_pid',               'type' => 'text',   'value' => ''),
        array('name' => 'alipayhk_md5_signature_key',           'type' => 'text',   'value' => ''),
        array('name' => 'alipayhk_api_base_gateway',            'type' => 'text',   'value' => "https://mapi.alipaydev.com/gateway.do?")
    );

    return;
}

/**
 * 类
 */
class alipayhk
{
    // For async notification purpose, set to false for poll-based query
    var $webhook = true;
    var $orderController = null;
    // Allowing API refund with manual override
    var $api_refund = true;
    var $manual_api_refund_override = true;

    /**
     * 构造函数
     *
     * @access  public
     * @param
     *
     * @return void
     */
    function alipay()
    {
    }

    function __construct()
    {
        $this->orderController = new Yoho\cms\Controller\OrderController();
        $this->alipay();
    }

    /**
     * 生成支付代码
     * @param   array   $order      订单信息
     * @param   array   $payment    支付方式信息
     */
    function get_code($order, $payment)
    {
        require_once ROOT_PATH . ADMIN_PATH . '/includes/lib_accounting.php';
        global $actgHooks;
        global $_LANG;
        $pay_log = $actgHooks->paymentModuleGetPayLog(get_class($this), $order['log_id']);
        $order_goods = $this->orderController->get_order_goods($order);

        if (!defined('EC_CHARSET'))
        {
            $charset = 'utf-8';
        }
        else
        {
            $charset = EC_CHARSET;
        }

        $order_payment_id = $order['order_sn'] . $order['log_id'] . "_" . local_gettime();

        $goods_info = $this->getGoodsCategoryQuantityInfo($order_goods);
    
        $parameter = [
            '_input_charset'    =>      'UTF-8',
            'currency'          =>      $pay_log['currency_code'],
            'out_trade_no'      =>      $order_payment_id,
            'partner'           =>      $payment['alipayhk_merchant_pid'],
            'payment_inst'      =>      'ALIPAYHK',
            'product_code'      =>      'NEW_WAP_OVERSEAS_SELLER',
            'qr_pay_mode'       =>      4,
            'qrcode_width'      =>      200,
            'service'           =>      defined('YOHO_MOBILE') ? 'create_forex_trade_wap' : 'create_forex_trade',
            'subject'           =>      $order['order_sn'],
            'total_fee'         =>      $order['order_amount'],
            'return_url'        =>      return_url(basename(__FILE__, '.php')),
            'notify_url'        =>      'https://' . ((strpos($_SERVER['SERVER_NAME'], 'beta.yohohongkong.com') === false) ? 'www.yohohongkong.com' : 'beta.yohohongkong.com') . '/api/receiveAlipayPaymentNotification.php',
            'refer_url'         =>      'https://' .  $_SERVER['SERVER_NAME'],
            'trade_information' =>      json_encode([
                'business_type' => 4,
                'goods_info'    => $goods_info["goods_info"],
                'total_quantity' => $goods_info['total_quantity']
            ])
        ];

        ksort($parameter);
        reset($parameter);

        $param = '';
        $sign  = '';

        foreach ($parameter AS $key => $val)
        {
            if ($key != 'secondary_merchant_name') {
                $param .= "$key=" .urlencode($val). "&";
            } else {
                $param .= "$key=" .$val. "&";
            }
            $sign  .= "$key=$val&";
        }

        $param = substr($param, 0, -1);
        $sign  = substr($sign, 0, -1). ecs_iconv(EC_CHARSET, 'utf-8', $payment['alipayhk_md5_signature_key']);

        $btn_class = defined('YOHO_MOBILE') ? 'btn btn-primary btn-lg' : 'btn-css3';
        $btn_text = _L('payment_module_alipay_btn','立即使用支付寶付款');

        $desktop_payment_form = "
            <link rel='stylesheet' href='/yohohk/css/alipay_payment.css'>
            <div class='alipay-payment-form-container'>
                <div class='alipay-payment-form-logo-container'>
                    <img src='/yohohk/img/payment-icon/payment_icon_alipayhk_horizontal.png'/>
                </div>
                <div class='alipay-payment-form'>
                    <div class='qr-code'>
                        <iframe scrolling='no' src='".$payment['alipayhk_api_base_gateway'].$param.'&sign_type=MD5&sign='.strtolower(md5($sign))."'></iframe>
                    </div>
                    <div class='instruction'>
                        <div class='scan-code'>
                            <div class='icon'>
                            <img src='/yohohk/img/payment-icon/general/AlipayScanCode.svg'></img>
                            </div>
                            <div class='text'>
                                <p><strong>" . $_LANG['alipayhk_instruction_use_alipay_scan_qr_code_proceed_payment'] . "</strong><p>
                            </div>
                        </div>
                        <div class='download-app'>
                            <div class='icon'>
                                <img src='/yohohk/img/payment-icon/AlipayHK_AppIcon.svg'></img>
                            </div>
                            <div class='text'>
                                <p><strong>" . $_LANG['alipayhk_instruction_no_alipayhk_app'] . "</strong><p>
                                <p>" . $_LANG['alipayhk_scan_qr_code_to_download'] . "<p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                var webhook = " . ($this->webhook ? 'false' : 'true') . ";
                var alipay_order_payment_id = '" . $order_payment_id . "';
                var wallet='alipayhk';
            </script>
        ";

        // Create curl object
        $result = defined('YOHO_MOBILE') ?
        '<div style="text-align:center;"><a href="'.$payment['alipayhk_api_base_gateway'].$param.'&sign_type=MD5&sign='.strtolower(md5($sign)).'" class="'.$btn_class.'">'.$btn_text.'</a></div>'
        : $desktop_payment_form;
        return $result;
    }

    /**
     * 响应操作
     */
    function respond()
    {
        if (!empty($_POST))
        {
            foreach($_POST as $key => $data)
            {
                $_GET[$key] = $data;
            }
        }
        $payment  = get_payment($_GET['code']);
        $log_id = substr(explode("_", trim($_GET['out_trade_no']))[0], 13); //str_replace($_GET['subject'], '', $_GET['out_trade_no']);
        //$log_id = trim($log_id);
        $action_note = $_GET['code'] . ": " . $_GET['trade_no'] . '（' . $_GET['total_fee'] . ')';


        /* 检查数字签名是否正确 */
        ksort($_GET);
        reset($_GET);

        $sign = '';
        foreach ($_GET AS $key=>$val)
        {
            if ($key != 'sign' && $key != 'sign_type' && $key != 'code')
            {
                $sign .= "$key=$val&";
            }
        }

        $sign = substr($sign, 0, -1) . $payment['alipayhk_md5_signature_key'];
        //$sign = substr($sign, 0, -1) . ALIPAY_AUTH;
        if (md5($sign) != $_GET['sign'])
        {
            return false;
        }

        /* 检查支付的金额是否相符 */

        $sql = "SELECT * FROM " . $GLOBALS['ecs']->table('pay_log') . " WHERE log_id = '" . $log_id . "'";
        $pay_log = $GLOBALS['db']->getRow($sql);
        if ($pay_log['currency_code'] != $_GET['currency'])
        {
            return false;
        }
        if ($pay_log['order_amount'] != $_GET['total_fee'])
        {
            return false;
        }

        if ($_GET['trade_status'] == 'WAIT_SELLER_SEND_GOODS')
        {
            /* 改变订单状态 */
            if (!($this->webhook)) {
                $this->processPaidOrder($payment, $log_id, PS_PAYED, $action_note, $_GET);
            }

            return true;
        }
        elseif ($_GET['trade_status'] == 'TRADE_FINISHED')
        {
            /* 改变订单状态 */
            if (!($this->webhook)) {
                $this->processPaidOrder($payment, $log_id, PS_PAYED, $action_note, $_GET);
            }

            return true;
        }
        elseif ($_GET['trade_status'] == 'TRADE_SUCCESS')
        {
            /* 改变订单状态 */
            if (!($this->webhook)) {
                $this->processPaidOrder($payment, $log_id, PS_PAYED, $action_note, $_GET);
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    private function processPaidOrder($payment_info, $log_id, $pay_code, $action_note, $data)
    {
        $alipayController = new Yoho\cms\Controller\AlipayController();

        $this->orderController->order_payment_transaction_log($log_id, $payment_info['pay_id'], $data['trade_no'], $data['currency'], $data['total_fee'], 0);
        $alipayController->insertSyncNotification($data);

        order_paid($log_id, $pay_code, $action_note);
    }

    private function getGoodsCategoryQuantityInfo($order_goods)
    {
        $goods_quantity_list = array_column($order_goods['goods_list'], "goods_number", "goods_id");
        $goods_ids = array_keys($goods_quantity_list);

        $goodsController = new Yoho\cms\Controller\GoodsController();
        $goods_category_list = $goodsController->getGoodsCategoryList($goods_ids);

        if (empty($goods_category_list) || sizeof($goods_category_list) == 0)
        {
            return '';
        }

        $goods_category_list = array_column($goods_category_list, "cat_name", "goods_id");
        
        $goods_info_mapping = '';
        $iteration_idx = 0;
        $total_quantity = 0;

        foreach($goods_quantity_list as $goods_id => $goods_quantity)
        {
            if ($iteration_idx > 0) { // Not first element
                $goods_info_mapping .= "|";
            }

            $goods_info_mapping .= $goods_category_list[$goods_id] . "(" . $goods_id . ")^" . $goods_quantity;
            $total_quantity += intval($goods_quantity);

            $iteration_idx++;
        }

        return [
           "goods_info" => $goods_info_mapping,
           "total_quantity" => $total_quantity
        ];

    }

    public function do_refund($txn_id,$refund_amount)
    {
        $alipayController = new Yoho\cms\Controller\AlipayController();
        return $alipayController->refund_by_transaction_id_and_amount($txn_id, $refund_amount);
    }

}

?>
