<?php

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}
$cron_lang = ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/cron/generic_sorting_pro.php';
if (file_exists($cron_lang))
{
    global $_LANG;

    include_once($cron_lang);
}

/* 模块的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
    $i = isset($modules) ? count($modules) : 0;

    /* 代码 */
    $modules[$i]['code']	= basename(__FILE__, '.php');

    /* 描述对应的语言项 */
    $modules[$i]['desc']	= 'product_generic_desc';

    /* 作者 */
    $modules[$i]['author']  = 'zoe';

    /* 版本号 */
    $modules[$i]['version'] = '1.0.0';

    /* 配置信息 */
    $modules[$i]['config']  = array(
       
    );

    return;
}
error_reporting(E_ERROR );
ini_set('memory_limit', '1024M');
ini_set('max_execution_time', 1800);
require(ROOT_PATH . 'includes/lib_goods.php');
$statisticsController = new Yoho\cms\Controller\StatisticsController();
$autoPricingController = new Yoho\cms\Controller\AutoPricingController();
// Generic Sorting Part 2 --Product sorting
$max_score = 1000;
$goods_result = $statisticsController->getAllLastestGoodsStats();
$goods_perf_arr = $goods_result["data"];
$goods_quantity = $db->getOne("SELECT COUNT(*) FROM " . $ecs->table("goods"));
$proportion = ceil($goods_quantity / $max_score);
$goods_final_score = [];

//top 1/1000 of total listed quantity > 1000; 2/1000 > 999 .....
//7 day session sorting
$new_goods_perf_arr = [];
$s7_score_arr = [];
for ($i=0; $i<count($goods_perf_arr); $i++) {
    if ($i==0) $count = 0;
    $cur_cat_arr = $goods_perf_arr[$i];
    //some goods_id in ecs_google_statistics_temporary_view not exist in ecs_goods  =>  $goods_perf_arr[$i]["goods_name"] would be NULL
    if (!is_null($goods_perf_arr[$i]["goods_name"])) {
        if ($count>0 && $cur_cat_arr["session_7"]==$new_goods_perf_arr[$count-1]["session_7"]) {
            $score = $s7_score_arr[$new_goods_perf_arr[$count-1]["goods_id"]]["score"];
           
        } else {
            $sub = floor($count/$proportion);
            $score = ($max_score - $sub) * 0.8;
        }
        $s7_score_arr[$cur_cat_arr["goods_id"]] = array("score" => $score);
        array_push($new_goods_perf_arr,$goods_perf_arr[$i]);
        $count = $count + 1;
        $goods_final_score[$cur_cat_arr["goods_id"]] = array("score" => $score);
    }
}

$new_goods_perf_arr_length = count($new_goods_perf_arr);
//30 day session sorting
$s30_arr = array_column($new_goods_perf_arr,"session_30");
array_multisort($s30_arr, SORT_DESC, $new_goods_perf_arr);
$s30_score_arr = [];
for ($i=0; $i<$new_goods_perf_arr_length; $i++) {
    $cur_cat_arr = $new_goods_perf_arr[$i];
    if ($i>0 && $cur_cat_arr["session_30"]==$new_goods_perf_arr[$i-1]["session_30"]) {
        $score = $s30_score_arr[$new_goods_perf_arr[$i-1]["goods_id"]]["score"];
    } else {
        $sub = floor($i/$proportion);
        $score = ($max_score - $sub) * 0.6;     
    }
    $s30_score_arr[$cur_cat_arr["goods_id"]] = array("score" => $score);
    $goods_final_score[$cur_cat_arr["goods_id"]]["score"] += $score ;
}
//GA Lifetime session sorting
$lifetime_session_arr = array_column($new_goods_perf_arr,"session_lifetime");
array_multisort($lifetime_session_arr, SORT_DESC, $new_goods_perf_arr);
$lifetime_session_score_arr = [];
for ($i=0; $i<$new_goods_perf_arr_length; $i++) {
    $cur_cat_arr = $new_goods_perf_arr[$i];
    if($i>0 && $cur_cat_arr["session_lifetime"]==$new_goods_perf_arr[$i-1]["session_lifetime"]) {
        $score = $lifetime_session_score_arr[$new_goods_perf_arr[$i-1]["goods_id"]]["score"];
    } else {
        $sub = floor($i/$proportion);
        $score = ($max_score - $sub) * 0.6;  
    }
    $lifetime_session_score_arr[$cur_cat_arr["goods_id"]] = array("score" => $score);
    $goods_final_score[$cur_cat_arr["goods_id"]]["score"] += $score ;

}
//7day conversion sorting
$c7_arr = array_column($new_goods_perf_arr,"perf_sales_7to7_percentage");
array_multisort($c7_arr, SORT_DESC, $new_goods_perf_arr);
$c7_score_arr = [];
for ($i=0; $i<$new_goods_perf_arr_length; $i++) {
    $cur_cat_arr = $new_goods_perf_arr[$i];
    if ($i>0 && $cur_cat_arr["perf_sales_7to7_percentage"]==$new_goods_perf_arr[$i-1]["perf_sales_7to7_percentage"]) {
        $score = $c7_score_arr[$new_goods_perf_arr[$i-1]["goods_id"]]["score"];
    } else {
        $sub = floor($i/$proportion);
        $score = ($max_score - $sub) * 0.8;
    }
    $c7_score_arr[$cur_cat_arr["goods_id"]] = array("score" => $score);
    $goods_final_score[$cur_cat_arr["goods_id"]]["score"] += $score ;  
}

array_multisort($c7_arr, SORT_DESC, $new_goods_perf_arr);
//30day conversion sorting
$c30_arr = array_column($new_goods_perf_arr,"perf_sales_30to30_percentage");
array_multisort($c30_arr, SORT_DESC, $new_goods_perf_arr);
$c30_score_arr = [];
for ($i=0; $i<$new_goods_perf_arr_length; $i++) {
    $cur_cat_arr = $new_goods_perf_arr[$i];
    if ($i>0 && $cur_cat_arr["perf_sales_30to30_percentage"]==$new_goods_perf_arr[$i-1]["perf_sales_30to30_percentage"]) {
        $score = $c30_score_arr[$new_goods_perf_arr[$i-1]["goods_id"]]["score"];
    } else {
        $sub = floor($i/$proportion);
        $score = ($max_score - $sub) * 0.6;
    }
    $c30_score_arr[$cur_cat_arr["goods_id"]] = array("score" => $score);
    $goods_final_score[$cur_cat_arr["goods_id"]]["score"] += $score ;
}
//lifetime conversion sorting
$lifetime_conversion_arr = array_column($new_goods_perf_arr,"perf_sales_lifetime_percent");
array_multisort($lifetime_conversion_arr, SORT_DESC, $new_goods_perf_arr);
$lifetime_conversion_score_arr = [];
for ($i=0; $i<$new_goods_perf_arr_length; $i++) {
    $cur_cat_arr = $new_goods_perf_arr[$i];
    if ($i>0 && $cur_cat_arr["perf_sales_lifetime_percent"]==$new_goods_perf_arr[$i-1]["perf_sales_lifetime_percent"]) {
        $score = $lifetime_conversion_score_arr[$new_goods_perf_arr[$i-1]["goods_id"]]["score"];
    } else {
        $sub = floor($i/$proportion);
        $score = ($max_score - $sub) * 0.6;
    }
    $lifetime_conversion_score_arr[$cur_cat_arr["goods_id"]] = array("score" => $score);
    $goods_final_score[$cur_cat_arr["goods_id"]]["score"] += $score ;
}
//7day sold sorting
$sold7_arr = array_column($new_goods_perf_arr,"sales_7");
array_multisort($sold7_arr, SORT_DESC, $new_goods_perf_arr);
$sold7_score_arr = [];
for ($i=0; $i<$new_goods_perf_arr_length; $i++) {
    $cur_cat_arr = $new_goods_perf_arr[$i];
    if ($i>0 && $cur_cat_arr["sales_7"]==$new_goods_perf_arr[$i-1]["sales_7"]) {
        $score = $sold7_score_arr[$new_goods_perf_arr[$i-1]["goods_id"]]["score"];
    } else {
        $sub = floor($i/$proportion);
        $score = $max_score - $sub;
    }
    $sold7_score_arr[$cur_cat_arr["goods_id"]] = array("score" => $score);
    $goods_final_score[$cur_cat_arr["goods_id"]]["score"] += $score ;
}
//30day sold sorting
$sold30_arr = array_column($new_goods_perf_arr,"sales_30");
array_multisort($sold30_arr, SORT_DESC, $new_goods_perf_arr);
$sold30_score_arr = [];
for ($i=0; $i<$new_goods_perf_arr_length; $i++) {
    $cur_cat_arr = $new_goods_perf_arr[$i];
    if ($i>0 && $cur_cat_arr["sales_30"]==$new_goods_perf_arr[$i-1]["sales_30"]) {
        $score = $sold30_score_arr[$new_goods_perf_arr[$i-1]["goods_id"]]["score"];
    } else {
        $sub = floor($i/$proportion);
        $score = ($max_score - $sub) * 0.8;
    }
    $sold30_score_arr[$cur_cat_arr["goods_id"]] = array("score" => $score);
    $goods_final_score[$cur_cat_arr["goods_id"]]["score"] += $score ;
}
//lifetime sold sorting
$sold_lifetime_arr = array_column($new_goods_perf_arr,"sales_lifetime");
array_multisort($sold_lifetime_arr, SORT_DESC, $new_goods_perf_arr);
$sold_lifetime_score_arr = [];
for ($i=0; $i<$new_goods_perf_arr_length; $i++) {
    $cur_cat_arr = $new_goods_perf_arr[$i];
    if ($i>0 && $cur_cat_arr["sales_lifetime"]==$new_goods_perf_arr[$i-1]["sales_lifetime"]) {
        $score = $sold_lifetime_score_arr[$new_goods_perf_arr[$i-1]["goods_id"]]["score"];
    } else {
        $sub = floor($i/$proportion);
        $score = ($max_score - $sub) * 0.8;
    }
    $sold_lifetime_score_arr[$cur_cat_arr["goods_id"]] = array("score" => $score);
    $goods_final_score[$cur_cat_arr["goods_id"]]["score"] += $score ;
}

//每個分類下的產品表現
$AllLastesCats = $statisticsController->getAllLastestBrandsCatsStats("cat");
// 每個分類下7天轉化率前20%的產品ID
$cat_top20_goods = $AllLastesCats[0];
// get Brand 7 Day Conversion Rate Best 20% 
$AllLastesBrands = $statisticsController->getAllLastestBrandsCatsStats("brand");
$brand_top20_goods = $AllLastesBrands[0];
// get dead stock >=90
$dead_stock_list = $autoPricingController->getDeadStockList();
// If criteria meets, give 300 scores
$criteria_score = 300;
$count = $db->getOne("SELECT COUNT(*) FROM " . $ecs->table("goods"));
$size = 5000;
$times = ceil($count/$size);
$res = [];
$fitting_goods_id = [];
for ($i=0; $i<$times; $i++) {
    $start = $size * $i;
    $sql = "SELECT g.goods_id, g.shop_price FROM ". $ecs->table("goods") . " g LIMIT $start, $size";
    $result = $db->getAll($sql);
    foreach ($result as $key => $row) {  
        array_push($res, $row);
    }  
}  
foreach ($res as $key => $row) {    
    $goods_id = $row['goods_id'];     
    $shop_price = $row['shop_price'];
    if (is_null($goods_final_score[$goods_id])) {
        $goods_final_score[$goods_id]["score"] = 0.00;
    } 
    // Price > 400
    if ($shop_price>400) {       
        $goods_final_score[$goods_id]["score"] += $criteria_score;      
    }
    // Price > 1000
    if ($shop_price>1000) {
        $goods_final_score[$goods_id]["score"] += $criteria_score;
    }
    // Get review of goods
    $sql= "select sum(comment_rank) as tnumber , count(*) as num from ".$GLOBALS['ecs']->table('comment')." where id_value='".$goods_id."'  AND status = 1";
    $row = $GLOBALS['db']->GetRow($sql);
    if ($row['num']>0) {
        $number = intval($row['tnumber'])/$row['num'];
        $number =sprintf("%01.1F", $number);
        
        if ($row['num'] < 3 && $number <= 3) {
            $number = 0;
        }
    } else {
        $number = 0;
    }
    // Review > 4
    if ($number>4) {
        $goods_final_score[$goods_id]["score"] += $criteria_score;
    }
    // Review > 4.5
    if ($number>4.5) {
        $goods_final_score[$goods_id]["score"] += $criteria_score;
    }
    // goods belongs to Cat 7 Day Conversion Rate Best 20% 
    if (in_array($goods_id, $cat_top20_goods)) {
        $goods_final_score[$goods_id]["score"] += $criteria_score;
    }
    // goods belongs to Brand 7 Day Conversion Rate Best 20% 
    if (in_array($goods_id, $brand_top20_goods)) {
        $goods_final_score[$goods_id]["score"] += $criteria_score;
    }
    // Deadstock >= 90 Days
    if (in_array($goods_id, $dead_stock_list)) {
        $goods_final_score[$goods_id]["score"] += $criteria_score;      
    }
    // 找到所有fittings 配件
    $fittings = get_goods_fittings(array($goods_id));
    if ($fittings) {
        foreach($fittings as $fitting) {  
            if (!in_array($fitting["goods_id"], $fitting_goods_id)) {
                array_push($fitting_goods_id, $fitting["goods_id"]);
            }         
        }     
    }  
    $sql = "UPDATE" . $ecs->table('goods') .
            "SET sort_order = '" . $goods_final_score[$goods_id]["score"] . "'" .
            "WHERE goods_id = '" . $goods_id . "'";
    $db->query($sql);
}
//修正配件sort order 的值
foreach ($fitting_goods_id as $fitting_id) {  
    $fitting_score = $goods_final_score[$fitting_id]["score"] * 0.5;
    $sql = "UPDATE" . $ecs->table('goods') .
            "SET sort_order = '" . $fitting_score . "'" .
            "WHERE goods_id = '" . $fitting_id . "'";
    $db->query($sql);
}  
?>