<?php

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}
$cron_lang = ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/cron/auto_recommend.php';
if (file_exists($cron_lang))
{
    global $_LANG;

    include_once($cron_lang);
}
require_once(ROOT_PATH . 'includes/lib_goods.php');

/* 模块的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
    $i = isset($modules) ? count($modules) : 0;

    /* 代码 */
    $modules[$i]['code']	= basename(__FILE__, '.php');

    /* 描述对应的语言项 */
    $modules[$i]['desc']	= 'auto_recommend_desc';

    /* 作者 */
    $modules[$i]['author']  = 'zoe';

    /* 版本号 */
    $modules[$i]['version'] = '1.0.0';

    /* 配置信息 */
    $modules[$i]['config']  = array(
       
    );

    return;
}

error_reporting(E_ERROR );
ini_set('memory_limit', '1024M');
ini_set('max_execution_time', 1800);
$statisticsController = new Yoho\cms\Controller\StatisticsController();
$categoryController = new Yoho\cms\Controller\CategoryController(); 

//desktop首頁的熱門分類 —— 每個level1的level 3 排序值高的前5個類別的hot category設為1
//將category表中hot_category為1先設為0
$sql =  "UPDATE" . $ecs->table('category') . "as a INNER JOIN"  . $ecs->table('category') . 
        "AS b ON a.cat_id = b.cat_id  SET a.hot_category = '0' where b.hot_category = '1'";
$db->query($sql);
$level1_cat = $categoryController->getTopLevelCat();
foreach ($level1_cat as $level1_value) {
    $level1_cat_id = $level1_value["cat_id"];
    //某個level1的level 3 category按照排序值從高到低排列，取前5個類別的cat_id
    $level3_cat = $categoryController->getBottomCategories($level1_cat_id);
    foreach ($level3_cat as $level3_value) {
        $level3_cat_id = $level3_value["cat_id"];
        $sql = "UPDATE" . $ecs->table('category') .
           "SET hot_category = '1'" .
           "WHERE cat_id = '" . $level3_cat_id . "'";
        $db->query($sql);
    }
}
//每個分類下的產品表現
$AllLastesCats = $statisticsController->getAllLastestBrandsCatsStats("cat");
// 熱銷 —— 每個分類下(7天轉化率+同比上周轉化率)/2前5%的產品ID 
$is_hot_arr = $AllLastesCats[1];
// 推介 —— 每個分類下7天表現值從低到高排序，轉化率高於該分類的平均轉化率並且庫存不為0，前10%的產品ID 
$is_best_arr = $AllLastesCats[2];
$count = $db->getOne("SELECT COUNT(*) FROM " . $ecs->table("goods"));
$size = 5000;
$times = ceil($count/$size);
$res = [];
for ($i=0; $i<$times; $i++) {
    $start = $size * $i;
    $sql = "SELECT g.goods_id, g.add_time FROM ". $ecs->table("goods") . " g LIMIT $start, $size";
    $result = $db->getAll($sql);
    foreach ($result as $key => $row) {  
        array_push($res, $row);
    }  
}  
foreach ($res as $key => $row) {    
    $goods_id = $row['goods_id'];     
    //新品、推介、熱銷
    $is_new = 0;
    $is_best = 0;
    $is_hot = 0;
    //新品
    $time_90days_before = local_strtotime('-90 days midnight'); // 00:00:00 90 days ago
    $last_update = $row['add_time']; // 產品create time
    if ($last_update > $time_90days_before) {
        $is_new = 1;
    }
    // 熱銷 —— goods_id is in is_hot_arr 
    if (in_array($goods_id, $is_hot_arr)) {
        $is_hot = 1;
    } 
    // 推介 —— goods_id is in is_best_arr 
    if (in_array($goods_id, $is_best_arr)) {
        $is_best = 1;
    } 
    $sql = "UPDATE" . $ecs->table('goods') .
            "SET is_new = '" . $is_new . "', is_hot = '" . $is_hot . "', is_best = '" . $is_best . "' " .
            "WHERE goods_id = '" . $goods_id . "'";
    $db->query($sql);
}

?>