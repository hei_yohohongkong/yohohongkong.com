<?php

if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}
$cron_lang = ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/cron/keywords.php';
if (file_exists($cron_lang))
{
	global $_LANG;

	include_once($cron_lang);
}

/* 模块的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
	$i = isset($modules) ? count($modules) : 0;

	/* 代码 */
	$modules[$i]['code']	= basename(__FILE__, '.php');

	/* 描述对应的语言项 */
	$modules[$i]['desc']	= 'keywords_desc';

	/* 作者 */
	$modules[$i]['author']  = 'dem';

	/* 版本号 */
	$modules[$i]['version'] = '1.0.0';

	/* 配置信息 */
	$modules[$i]['config']  = array(
        array('name' => 'view_mode', 'type' => 'select', 'value' => '1'),
        array('name' => 'category_update', 'type' => 'select', 'value' => '0'),
        array('name' => 'category_sort', 'type' => 'select', 'value' => '0'),
        array('name' => 'cat_prefix_chi', 'type' => 'text', 'value' => ''),
        array('name' => 'cat_suffix_chi', 'type' => 'textarea', 'value' => '品牌 香港 購買 比較 推薦 代理 價錢 價格 團購 推介 專門店 網購 代購 經銷商 批發 銷售 行貨 水貨 特價 折扣 優惠 平 便宜 效果 用法 分別 功能 最好'),
        array('name' => 'cat_prefix_eng', 'type' => 'text', 'value' => ''),
        array('name' => 'cat_suffix_eng', 'type' => 'textarea', 'value' => 'brands hk price sale buy reseller distributor online shop promotion compare comparison best review'),
        array('name' => 'brand_update', 'type' => 'select', 'value' => '0'),
        array('name' => 'brand_sort', 'type' => 'select', 'value' => '0'),
        array('name' => 'brand_prefix_chi', 'type' => 'text', 'value' => '購買'),
        array('name' => 'brand_suffix_chi', 'type' => 'textarea', 'value' => '產品 - 香港代理零售經銷'),
        array('name' => 'brand_prefix_eng', 'type' => 'text', 'value' => 'Shop'),
        array('name' => 'brand_suffix_eng', 'type' => 'textarea', 'value' => 'now - hk distributor reseller'),
    );

	return;
}

empty($cron['category_update']) && $cron['category_update'] = 0;
empty($cron['category_sort']) && $cron['category_sort'] = 0;
empty($cron['brand_keywords']) && $cron['brand_keywords'] = 0;
empty($cron['brand_sort']) && $cron['brand_sort'] = 0;

if($cron['view_mode']){ echo '<script src="../js/jquery.1.3.2.js"></script><script>var page=1,tog=false;function nextPage(){if($(".page_"+(page+1)).length>0){$(".page").hide();$(".page_"+(page+1)).show();page++;if(tog){$(".success").hide();if($("tr:visible:not(.success,:first-child)").length==0)$("tr:not(.success)").show()}}}function prevPage(){if($(".page_"+(page-1)).length>0){$(".page").hide();$(".page_"+(page-1)).show();page--;if(tog){$(".success").hide();if($("tr:visible:not(.success,:first-child)").length==0)$("tr:not(.success)").show()}}}function toggleData(){$("tr:not(.page_"+page+",:first-child)").hide();$(".success.page_"+page).toggle();tog=!tog;if(tog&&$("tr:visible:not(.success,:first-child)").length==0)$("tr:not(.success)").show()}$(function(){$(".page").hide();$(".page_1").show();})</script><link href="styles/general.css" rel="stylesheet" type="text/css"><link href="styles/main.css" rel="stylesheet" type="text/css"><div class="list-div"><input type="button" class="button" style="float:right" onclick="nextPage()" value="下一頁"><input type="button" class="button" style="float:right" onclick="prevPage()" value="上一頁"><table><tr><th>名稱</th><th>描述<input type="button" class="button" style="float:right" onclick="toggleData()" value="隱藏/展開"></th></tr>';}
$count_page = 1;
$count_row = 1;
// 更新品牌描述
if($cron['brand_update']){
    $db->query('START TRANSACTION');
    $sql = "SELECT b.brand_name AS chi_name, IFNULL(bl.brand_name,'') AS eng_name,b.brand_id FROM " . $ecs->table('brand') . " b LEFT JOIN " . $ecs->table('brand_lang') . " bl ON b.brand_id = bl.brand_id WHERE is_update = 1";
    $brand_list = $db->getAll($sql);
    foreach($brand_list as $brand){
        $sql1 = "SELECT c.cat_name AS chi_name, IFNULL(cl.cat_name,'') AS eng_name, COUNT(*) AS goods_sum FROM " . $ecs->table('goods') . " g " .
                "LEFT JOIN " . $ecs->table('category') . " c ON c.cat_id = g.cat_id " .
                "LEFT JOIN " . $ecs->table('category_lang') . " cl ON c.cat_id = cl.cat_id " .
                "WHERE is_delete != 1 AND is_hidden != 1 AND c.cat_id IS NOT NULL AND brand_id = " . $brand['brand_id'] . " GROUP BY c.cat_id ORDER BY goods_sum DESC, c.cat_id DESC LIMIT 5";
        $sql2 = "SELECT c.cat_name AS chi_name, IFNULL(cl.cat_name,'') AS eng_name, SUM(sales) AS cat_sale FROM " . $ecs->table('goods') . " g " .
                "LEFT JOIN " . $ecs->table('category') . " c ON c.cat_id = g.cat_id " .
                "LEFT JOIN " . $ecs->table('category_lang') . " cl ON c.cat_id = cl.cat_id " .
                "WHERE is_delete != 1 AND is_hidden != 1 AND c.cat_id IS NOT NULL AND brand_id = " . $brand['brand_id'] . " GROUP BY c.cat_id ORDER BY cat_sale DESC, c.cat_id DESC LIMIT 5";
        $cat_list = $db->getAll($cron['brand_sort'] ? $sql1 : $sql2);

        $keywords = [];
        $keywords2 = [];
        !empty($cron['brand_prefix_chi']) ? $keywords[] = $cron['brand_prefix_chi'] : true;
        !empty($cron['brand_prefix_eng']) ? $keywords2[] = $cron['brand_prefix_eng'] : true;
        $keywords[] = $brand['chi_name'];
        $keywords2[] = !empty($brand['eng_name']) ? $brand['eng_name'] : $brand['chi_name'];
        foreach($cat_list as $cat){
            $keywords[] = $cat['chi_name'];
            $keywords2[] = !empty($cat['eng_name']) ? $cat['eng_name'] : $cat['chi_name'];
        }
        !empty($cron['brand_suffix_chi']) ? $keywords[] = $cron['brand_suffix_chi'] : true;
        !empty($cron['brand_suffix_eng']) ? $keywords2[] = $cron['brand_suffix_eng'] : true;
        $desc = implode(" ",$keywords);
        $desc2 = implode(" ",$keywords2);

        $sql = "SELECT 1 FROM " . $ecs->table('brand_lang') . " WHERE brand_id = " . $brand['brand_id'];
        $lang_exist = $db->getOne($sql);
        $alnum = preg_match('/^[a-z0-9 -\/!]+$/i', $brand['chi_name']);
        if(!$cron['view_mode']){
            $sql = "UPDATE " . $ecs->table('brand') . " SET brand_desc = '" . mysql_escape_string($desc) . "' WHERE brand_id = " . $brand['brand_id'];
            $db->query($sql);
            if($alnum){
                $sql = "INSERT INTO " . $ecs->table('brand_lang') . " (lang,brand_id,brand_name,brand_desc) VALUES ('en_us'," . $brand['brand_id'] . ",'" . mysql_escape_string($brand['chi_name']) . "','" . mysql_escape_string($desc2) . "') ON DUPLICATE KEY UPDATE brand_desc = '" . mysql_escape_string($desc2) . "'";
                $db->query($sql);
            }else if($lang_exist){
                $sql = "UPDATE " . $ecs->table('brand_lang') . " SET brand_desc = '" . mysql_escape_string($desc2) . "' WHERE brand_id = " . $brand['brand_id'];
                $db->query($sql);
            }
        }else{
            echo '<tr class="page page_' . $count_page . ($lang_exist ? ' success' : '') . '"><td rowspan="2" align="center"' . ($lang_exist ? '' : ($alnum ? ' style="color:green"' : ' style="color:red"')) . '>' . $brand['chi_name'] . '</td><td>' . mysql_escape_string($desc) . '</td></tr><tr class="page page_' . $count_page . ($lang_exist ? ' success' : '') . '"><td>' . mysql_escape_string($desc2) . '</td></tr>';
            $count_page = ceil($count_row / 30);
            $count_row++;
        }
    }
    $db->query('COMMIT');
}

// 更新分類描述
if($cron['category_update']){
    $db->query('START TRANSACTION');
    $sql = "SELECT c.cat_name AS chi_name, IFNULL(cl.cat_name,'') AS eng_name,c.cat_id FROM " . $ecs->table('category') . " c LEFT JOIN " . $ecs->table('category_lang') . " cl ON c.cat_id = cl.cat_id WHERE is_update = 1";
    $cat_list = $db->getAll($sql);
    foreach($cat_list as $cat){
        $sql1 = "SELECT b.brand_name AS chi_name, IFNULL(bl.brand_name,'') AS eng_name, COUNT(*) AS goods_sum FROM " . $ecs->table('goods') . " g " .
                "LEFT JOIN " . $ecs->table('brand') . " b ON b.brand_id = g.brand_id " .
                "LEFT JOIN " . $ecs->table('brand_lang') . " bl ON b.brand_id = bl.brand_id " .
                "WHERE is_delete != 1 AND is_hidden != 1 AND b.brand_id IS NOT NULL AND cat_id = " . $cat['cat_id'] . " GROUP BY b.brand_id ORDER BY goods_sum DESC, b.brand_id DESC LIMIT 5";
        $sql2 = "SELECT b.brand_name AS chi_name, IFNULL(bl.brand_name,'') AS eng_name, SUM(sales) AS brand_sale FROM " . $ecs->table('goods') . " g " .
                "LEFT JOIN " . $ecs->table('brand') . " b ON b.brand_id = g.brand_id " .
                "LEFT JOIN " . $ecs->table('brand_lang') . " bl ON b.brand_id = bl.brand_id " .
                "WHERE is_delete != 1 AND is_hidden != 1 AND b.brand_id IS NOT NULL AND cat_id = " . $cat['cat_id'] . " GROUP BY b.brand_id ORDER BY brand_sale DESC, b.brand_id DESC LIMIT 5";
        $brand_list = $db->getAll($cron['category_sort'] ? $sql1 : $sql2);

        $keywords = [];
        $keywords2 = [];
        !empty($cron['cat_prefix_chi']) ? $keywords[] = $cron['cat_prefix_chi'] : true;
        !empty($cron['cat_prefix_eng']) ? $keywords2[] = $cron['cat_prefix_eng'] : true;
        $keywords[] = $cat['chi_name'];
        $keywords2[] = !empty($cat['eng_name']) ? $cat['eng_name'] : $cat['chi_name'];
        foreach($brand_list as $brand){
            $keywords[] = $brand['chi_name'];
            $keywords2[] = !empty($brand['eng_name']) ? $brand['eng_name'] : $brand['chi_name'];
        }
        !empty($cron['cat_suffix_chi']) ? $keywords[] = $cron['cat_suffix_chi'] : true;
        !empty($cron['cat_suffix_eng']) ? $keywords2[] = $cron['cat_suffix_eng'] : true;
        $desc = implode(" ",$keywords);
        $desc2 = implode(" ",$keywords2);

        $sql = "SELECT 1 FROM " . $ecs->table('category_lang') . " WHERE cat_id = " . $cat['cat_id'];
        $lang_exist = $db->getOne($sql);
        $alnum = preg_match('/^[a-z0-9 -\/!]+$/i', $cat['chi_name']);
        if(!$cron['view_mode']){
            $sql = "UPDATE " . $ecs->table('category') . " SET cat_desc = '" . mysql_escape_string($desc) . "' WHERE cat_id = " . $cat['cat_id'];
            $db->query($sql);
            if($alnum){
                $sql = "INSERT INTO " . $ecs->table('category_lang') . " (lang,cat_id,cat_name,cat_desc) VALUES ('en_us'," . $cat['cat_id'] . ",'" . mysql_escape_string($cat['chi_name']) . "','" . mysql_escape_string($desc2) . "') ON DUPLICATE KEY UPDATE cat_desc = '" . mysql_escape_string($desc2) . "'";
                $db->query($sql);
            }else if($lang_exist){
                $sql = "UPDATE " . $ecs->table('category_lang') . " SET cat_desc = '" . mysql_escape_string($desc2) . "' WHERE cat_id = " . $cat['cat_id'];
                $db->query($sql);
            }
        }else{
            echo '<tr class="page page_' . $count_page . ($lang_exist ? ' success' : '') . '"><td rowspan="2" align="center"' . ($lang_exist ? '' : ($alnum ? ' style="color:green"' : ' style="color:red"')) . '>' . $cat['chi_name'] . '</td><td>' . mysql_escape_string($desc) . '</td></tr><tr class="page page_' . $count_page . ($lang_exist ? ' success' : '') . '"><td>' . mysql_escape_string($desc2) . '</td></tr>';
            $count_page = ceil($count_row / 30);
            $count_row++;
        }
    }
    $db->query('COMMIT');
}

if($cron['view_mode']){ echo '</table><input type="button" class="button" style="float:right" onclick="nextPage()" value="下一頁"><input type="button" class="button" style="float:right" onclick="prevPage()" value="上一頁"></div><input type="button" class="button" style="float:right" onclick="toggleData()" value="隱藏/展開">'; die();}

?>