<?php

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

$cron_lang = ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/cron/auto_task_rma.php';
$orderController = new Yoho\cms\Controller\OrderController();
$userController  = new Yoho\cms\Controller\UserController();
$rmaController  = new Yoho\cms\Controller\RmaController();

if (file_exists($cron_lang))
{
    global $_LANG;

    include_once($cron_lang);
}

/* 模块的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
    $i = isset($modules) ? count($modules) : 0;

    /* 代码 */
    $modules[$i]['code']    = basename(__FILE__, '.php');

    /* 描述对应的语言项 */
    $modules[$i]['desc']    = 'auto_task_rma_desc';

    /* 作者 */
    $modules[$i]['author']  = 'Eric';

    /* 网址 */
    $modules[$i]['website'] = '';

    /* 版本号 */
    $modules[$i]['version'] = '1.0.0';

    /* 配置信息 */
    $modules[$i]['config']  = array(
    );

    return;
}
// If today is holiday, don't run this task.
$now = gmtime();
$next_work_day = $orderController->get_yoho_next_work_day($now);
rma_reminder_cron($now, $next_work_day);

function task_day_diff($from,$to) {
   $dStart = new DateTime($from);
   $dEnd  = new DateTime($to);
   $dDiff = $dStart->diff($dEnd);

    if ($dDiff->y != '') {
        $diff_string = $dDiff->y . '年+';
    } else if ($dDiff->m != '') {
        $diff_string = $dDiff->m . '月+';
    } else if ($dDiff->d != '') {
        $diff_string = $dDiff->d . '日';
    }
    
    return $diff_string;
}

function getGoodsSupplierAdmin($goods_ids) {
    global $db, $ecs, $_LANG, $userController, $rmaController;

    $sql = "SELECT GROUP_CONCAT(distinct(au.user_id) ORDER BY au.user_id DESC) as user_ids, GROUP_CONCAT(distinct(au.user_name) ORDER BY au.user_id DESC) as user_names, GROUP_CONCAT(distinct(esa.supplier_id)) as supplier_ids, egs.goods_id ";
        $sql .= "FROM ". $ecs->table('erp_goods_supplier') ." as egs ";
        $sql .= "LEFT JOIN ". $ecs->table('erp_supplier_admin') ." as esa ON egs.supplier_id = esa.supplier_id ";
        $sql .= "LEFT JOIN ".$ecs->table('admin_user') ." as au ON esa.admin_id = au.user_id ";
        $sql .= "WHERE egs.goods_id". db_create_in($goods_ids) ." AND (au.user_id > 0 AND au.user_id != 1) GROUP BY egs.goods_id ORDER BY user_ids DESC";

    $res = $db->getAll($sql);
    foreach ($res as $key => $value) {
        $goods[$value['goods_id']] = $value;
    }

    $task_admin = [];
    foreach ($goods as $key => &$value) {
        if(empty($value['user_ids'])) continue;
        
        $admins = explode(",",$value['user_names']);
        $admins_ids = explode(",",$value['user_ids']);
        foreach ($admins as $k => $admin) {
            $value['admins'][$admins_ids[$k]] = $admin;
        }

        // We only create task on first admin (Max(admin_id))
        $first_admin_id = current(array_keys($value['admins']));
        $first_admin    = $value['admins'][$first_admin_id];
        $value['first_admin_id'] = $first_admin_id;
        $value['first_admin_name'] = $first_admin;
    }
    
    return $goods;
}

//RMA很久沒有進一步行動提示
function rma_reminder_cron($now, $next_work_day) {
    global $db, $ecs, $_LANG, $userController, $rmaController;

    $sql = "select er.*, g.goods_sn, g.goods_name from ". $ecs->table('erp_rma') ." er left join  ". $ecs->table('goods') ." g on (er.goods_id = g.goods_id) where er.status != ".$rmaController::FINISHED." ";
    $result = $db->getAll($sql);

    $pending_for_checking_status = $rmaController::PENDING_FOR_CHECKING;
    $notify_supplier_status = $rmaController::NOTIFY_SUPPLIER;
    $waiting_for_return_to_supplier_status = $rmaController::WAITING_FOR_RETURN_TO_SUPPLIER;
    $repairing_status = $rmaController::REPAIRING;
    $repaired_status = $rmaController::REPAIRED;

    $status_options = $rmaController->getStatusOptions();

    $task_reminder_condition = array (
                                $pending_for_checking_status => 7,
                                $notify_supplier_status => 7,
                                $waiting_for_return_to_supplier_status => 7,
                                $repairing_status => 14,
                                $repaired_status => 7,
                               ); 

    $goods_list = [];
    foreach ($result as $rma) {
        if (!in_array($rma['goods_id'],$goods_list)) {
            $goods_list[] = $rma['goods_id'];
        }
    }

    //$goods_admin = getGoodsSupplierAdmin($goods_list);

    $task_create_list = [];
    foreach ($result as $rma) {
        // check status
        switch ($rma['status']) {
        case $pending_for_checking_status :
            if (!empty($rma['last_action_time'])) {
                $last_action_date = local_date('Y-m-d',$rma['last_action_time']);
                $date = strtotime("-".$task_reminder_condition[$pending_for_checking_status]." day", gmtime());
                $check_date = local_date('Y-m-d',$date);
            }

            if (empty($rma['last_action_time']) || $last_action_date <= $check_date) {
                $today_date = local_date('Y-m-d',gmtime());
                $rma['no_further_act_day'] = task_day_diff($last_action_date,$today_date);
                $rma['task_send_to'] = $rma['person_in_charge'];
                $rma['status_name'] = $status_options[$rma['status']];
                $task_create_list[$rma['task_send_to']][] = $rma;
            }

            break;
        case $rmaController::NOTIFY_SUPPLIER :
            if (!empty($rma['last_action_time'])) {
                $last_action_date = local_date('Y-m-d',$rma['last_action_time']);
                $date = strtotime("-".$task_reminder_condition[$notify_supplier_status]." day", gmtime());
                $check_date = local_date('Y-m-d',$date);
            }

            if (empty($rma['last_action_time']) || $last_action_date <= $check_date) {
                $rma['no_further_act_day'] = task_day_diff($last_action_date,$today_date);
                $rma['task_send_to'] = $rma['person_in_charge'];
                $rma['status_name'] = $status_options[$rma['status']];
                $task_create_list[$rma['task_send_to']][] = $rma;
            }

            break;
        case $rmaController::WAITING_FOR_RETURN_TO_SUPPLIER :
            if (!empty($rma['last_action_time'])) {
                $last_action_date = local_date('Y-m-d',$rma['last_action_time']);
                $date = strtotime("-".$task_reminder_condition[$waiting_for_return_to_supplier_status]." day", gmtime());
                $check_date = local_date('Y-m-d',$date);
            }

            if (empty($rma['last_action_time']) || $last_action_date <= $check_date) {
                $rma['no_further_act_day'] = task_day_diff($last_action_date,$today_date);
                if (empty($rma['goods_admin'])) {
                   $rma['goods_admin'] = $rmaController->getGoodsUserAdmin($rma['goods_id']); 
                }
                $rma['task_send_to'] = $rma['goods_admin']; // send to product team
                $rma['status_name'] = $status_options[$rma['status']];
                $task_create_list[$rma['task_send_to']][] = $rma;
            }
            break;
        case $rmaController::REPAIRING :
            if (!empty($rma['last_action_time'])) {
                $last_action_date = local_date('Y-m-d',$rma['last_action_time']);
                $date = strtotime("-".$task_reminder_condition[$repairing_status]." day", gmtime());
                $check_date = local_date('Y-m-d',$date);
            }
            
            if (empty($rma['last_action_time']) || $last_action_date <= $check_date) {
                $rma['no_further_act_day'] = task_day_diff($last_action_date,$today_date);
                //$rma['task_send_to'] = $goods_admin[$rma['goods_id']]['first_admin_id']; // send to product team
                if (empty($rma['goods_admin'])) {
                    $rma['goods_admin'] = $rmaController->getGoodsUserAdmin($rma['goods_id']); 
                }
                $rma['task_send_to'] = $rma['goods_admin']; // send to product team
                $rma['status_name'] = $status_options[$rma['status']];
                $task_create_list[$rma['task_send_to']][] = $rma;
            }
            break;
        case $rmaController::REPAIRED :
            if (!empty($rma['last_action_time'])) {
                $last_action_date = local_date('Y-m-d',$rma['last_action_time']);
                $date = strtotime("-".$task_reminder_condition[$repaired_status]." day", gmtime());
                $check_date = local_date('Y-m-d',$date);
            }

            if (empty($rma['last_action_time']) || $last_action_date <= $check_date) {
                $rma['no_further_act_day'] = task_day_diff($last_action_date,$today_date);
                $rma['task_send_to'] =$rma['person_in_charge'];
                $rma['status_name'] = $status_options[$rma['status']];
                $task_create_list[$rma['task_send_to']][] = $rma;
            }
            break;
        }
    }


    $sender_id = 1;
    $sender_name = "admin";
    foreach ($task_create_list as $receiver_id => $task_create) {
        // get receiver name
        if (empty($receiver_id)) {
            continue;
        }

        $task_send_to_name =  $userController->get_user_name($receiver_id);

        // create task
        $task = array(
            'sender_id' => $sender_id,
            'receiver_id' => $receiver_id,
            'sent_time' => $now,
            'read_time' => 0,
            'status' => '0',
            'priority' => "High",
            'dead_time' => $next_work_day,
            'estimated_time' => '480',
            'sender_name' => $sender_name,
            'pic' => $task_send_to_name,
            'deleted' => 0,
            'title' => "RMA跟進提示 ".local_date('Y-m-d')
        );
        $message = "以下RMA需盡快處理: <br>";
        foreach ($task_create as $rma) {
            $message .= '<a target="_blank" href="/x/erp_rma_check.php?act=list&rma_id='.$rma['rma_id'].'">RMA#'.$rma['rma_id'].'</a> ['.$rma['status_name'].'][過了'.$rma['no_further_act_day'].'] '.$rma['goods_name'].'   <br>';
        }

        $task['message'] = mysql_escape_string($message);

        $db->autoExecute($ecs->table('admin_taskes'), $task, 'INSERT');
        $task_id = $db->insert_id();
    }
}
