<?php
ini_set('memory_limit','2048M');
if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}
$cron_lang = ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/cron/stock_cost_warehouse.php';
require_once(ROOT_PATH . 'includes/lib_order.php');
//require_once ROOT_PATH . 'includes/lib_howang.php';
if (file_exists($cron_lang))
{
	global $_LANG;

	include_once($cron_lang);
}

if (isset($set_modules) && $set_modules == TRUE)
{
	$i = isset($modules) ? count($modules) : 0;

	/* 代码 */
	$modules[$i]['code']	= basename(__FILE__, '.php');

	/* 描述对应的语言项 */
	$modules[$i]['desc']	= 'stock_cost_warehouse_desc';

	/* 作者 */
	$modules[$i]['author']  = 'sing';

	/* 版本号 */
	$modules[$i]['version'] = '1.0.0';

	/* 配置信息 */
	$modules[$i]['config']  = array(
        array('name' => 'stock_cost_warehouse', 'type' => 'select', 'value' => '0')
    );

	return;
}

empty($cron['stock_cost_warehouse']) && $cron['stock_cost_warehouse'] = 0;

$start_time = strtotime(date("Y/m/d")) - 86400;
$end_time = strtotime(date("Y/m/d"));
$log_time = time();
$sql = "SELECT GROUP_CONCAT(warehouse_id) FROM ". $ecs->table('erp_warehouse'). " WHERE is_valid = 1 AND is_on_sale = 1";
$warehouse_id = $db->getOne($sql);
$time_yesterday = local_strtotime('today'); // 23:59:59 yesterday
//$time_30days_before = local_strtotime('-30 days midnight'); // 00:00:00 30 days ago
//$time_60days_before = local_strtotime('-60 days midnight'); // 00:00:00 60 days ago
//$time_90days_before = local_strtotime('-90 days midnight'); // 00:00:00 90 days ago

$cost_arr = [];
$dead_arr = [];

$sql_limit = 5000;
$sql_offset = 0;
$sql_count = 5000;
$po_list = [];
while ($sql_count >= $sql_limit){
    $sql_warehouse_list = 
        "SELECT g.goods_id AS good_id, g.cost AS good_cost, ".
        "IF(g.goods_length > 0, g.goods_length, IF(c.default_length > 0, c.default_length,pc.default_length)) AS good_length, ".
        "IF(g.goods_width > 0, g.goods_width, IF(c.default_width > 0, c.default_width,pc.default_width)) AS good_width, ".
        "IF(g.goods_height > 0, g.goods_height, IF(c.default_height > 0, c.default_height,pc.default_height)) AS good_height, ".
        "gas.goods_qty AS good_qty, gas.warehouse_id AS warehouse_id ".
        "FROM ".$GLOBALS['ecs']->table('erp_goods_attr_stock')."AS gas ".
        "LEFT JOIN ".$GLOBALS['ecs']->table('goods')." AS g ON g.goods_id = gas.goods_id ".
        "LEFT JOIN ".$GLOBALS['ecs']->table('category')." AS c ON c.cat_id = g.cat_id ".
        "LEFT JOIN ".$GLOBALS['ecs']->table('category')." AS pc ON pc.cat_id = c.parent_id ".
        "WHERE gas.warehouse_id IN (".$warehouse_id.") AND g.is_delete = 0 AND g.is_real = 1 AND gas.goods_qty > 0 ".
        "ORDER BY g.goods_id DESC, gas.warehouse_id ASC ".
        "LIMIT ".$sql_limit." OFFSET ".$sql_offset;

        $list = $GLOBALS['db']->getAll($sql_warehouse_list);
        
        $list_size = sizeof($list);
        if ($list_size > 0){
            array_push($po_list, $list);  
        }
        $sql_count = $list_size;
        $sql_offset += $sql_count;
}

/*
$n_days = [
    '30' => $time_30days_before,
    '60' => $time_60days_before,
    '90' => $time_90days_before
];
foreach ($n_days as $key => $n_days_before) {
    $sql_n_day_sales = 'SELECT gn.goods_id, CASE WHEN td.ndayssales IS NULL OR gn.goods_number * 3 >= td.ndayssales * 5 THEN 1 ELSE 0 END AS dead ' .
    'FROM (SELECT ega.goods_id, sum(ega.goods_qty) as goods_number FROM ' . $ecs->table('erp_goods_attr_stock') . ' AS ega WHERE ega.warehouse_id IN ('. $warehouse_id .') GROUP BY ega.goods_id) as gn ' .
    'LEFT JOIN (SELECT og.goods_id, IFNULL(SUM(og.goods_number), 0) AS ndayssales FROM ' . $ecs->table('order_goods') . ' AS og ' .
    'LEFT JOIN ' . $ecs->table('order_info') . ' AS oi ON oi.order_id = og.order_id ' .
    'WHERE oi.order_status IN (1, 5) AND oi.shipping_status IN (1, 2) AND oi.pay_status IN (2, 1) AND ' .
    '(oi.shipping_time BETWEEN ' . $n_days_before . ' AND ' . $time_yesterday . ') GROUP BY og.goods_id' .
    ') AS td ON td.goods_id = gn.goods_id GROUP BY gn.goods_id HAVING dead = 1';
    $dead_list[$key] = $GLOBALS['db']->getAll($sql_n_day_sales);
}

foreach ($dead_list as $key => $goods) {
    foreach ($goods as $good){
        $dead_arr[$key][$good['goods_id']] = 1;
    }
}
*/
//$next_goods = true;
$goods_id   = 0;
foreach ($po_list as $round) {
    foreach ($round as $key => $po) {
        $temp_arr = [];
        if($next_goods && $po['goods_id'] == $goods_id) continue;

        //$next_goods = false;
        $qty = $po['good_qty'];
        $sum_cost = $po['good_cost'] * $qty;

        if (!empty($po['good_length']) && !empty($po['good_width']) && !empty($po['good_height'])){
            $volume = (($po['good_length']/100) * ($po['good_width']/100) * ($po['good_height']/100)) * $qty;
        } else {
            $volume = 0;
        }
        $cost_arr[$po['warehouse_id']][$po['good_id']]['c'] = number_format(isset($cost_arr[$po['warehouse_id']][$po['good_id']]['c']) ? $cost_arr[$po['warehouse_id']][$po['good_id']]['c']+= $sum_cost: $sum_cost, 2, '.', '');
        $cost_arr[$po['warehouse_id']][$po['good_id']]['v'] = isset($cost_arr[$po['warehouse_id']][$po['good_id']]['v']) ? $cost_arr[$po['warehouse_id']][$po['good_id']]['v']+= $volume: $volume;
        $cost_arr[$po['warehouse_id']][$po['good_id']]['q']  = (isset($cost_arr[$po['warehouse_id']][$po['good_id']]['q']) ? $cost_arr[$po['warehouse_id']][$po['good_id']]['q']+= $qty: $qty);
        //foreach (array_keys($dead_arr) as $day_key){
        //    if (!empty($dead_arr[$day_key][$goods_id])) {
        //        $cost_arr[$po['supplier_id']]['dead_cost_'.$day_key] = isset($cost_arr[$po['supplier_id']]['dead_cost_'.$day_key]) ? $cost_arr[$po['supplier_id']]['dead_cost_'.$day_key]+= $cost: $cost;
        //    }
        //}
    }
}

$serialized_cost_arr = serialize($cost_arr);

$sql = "INSERT INTO ".$ecs->table('stock_cost_warehouse_log')." (data_str, report_date, created_at) VALUES ('".$serialized_cost_arr."', ".$start_time.", ".$log_time.")";
$db->query($sql);

?>