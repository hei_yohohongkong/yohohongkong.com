<?php

/**
 * Auto price [2018-09-11]
 * Step 1. get all valid goods list
 * Step 2. get selected strategy
 * Step 3. suggest promotion if D180
 * Step 4. suggest shop_price if auto process enabled
 * Step 5. suggest vip_price if auto process enabled
 * Step 6. apply shop_price and vip_price suggestion
 */

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}
$cron_lang = ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/cron/products.php';
if (file_exists($cron_lang))
{
    global $_LANG;

    include_once($cron_lang);
}

/* 模块的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
    $i = isset($modules) ? count($modules) : 0;

    /* 代码 */
    $modules[$i]['code']	= basename(__FILE__, '.php');

    /* 描述对应的语言项 */
    $modules[$i]['desc']	= 'products_desc';

    /* 作者 */
    $modules[$i]['author']  = 'dem';

    /* 版本号 */
    $modules[$i]['version'] = '1.0.0';

    /* 配置信息 */
    $modules[$i]['config']  = array(
        array('name' => 'products_is_new_month', 'type' => 'select', 'value' => '3'),
        array('name' => 'auto_pricing', 'type' => 'select', 'value' => '0'),
        array('name' => 'max_on_sale_number', 'type' => 'text', 'value' => '500'),
    );

    return;
}

empty($cron['products_is_new_month']) && $cron['products_is_new_month'] = 3;

// 設定保留新品標籤的期限
$time = strtotime(gmdate("M d Y H:i:s",strtotime('-' . $cron['products_is_new_month'] . ' month')));

// 移除逾時的新品標籤
$db->query('START TRANSACTION');
$sql = "UPDATE " . $ecs->table('goods') . " SET is_new = 0 WHERE add_time < " . $time;
if(!$db->query($sql)){
    $db->query('ROLLBACK');
}
$db->query('COMMIT');

// Only run this on monday
if (local_date("w") == "1") {
    // Create task list for cost above other retail price
    $orderController = new Yoho\cms\Controller\OrderController();
    $autoPricingController = new Yoho\cms\Controller\AutoPricingController();
    $warn_cols = "";
    $warn_having = [];
    $task_admin = [];
    foreach ($autoPricingController::AUTO_PRICING_WEBS_COLUMNS as $key => $col) {
        $curr_table = "goods_$key";
        $warn_cols .= "IFNULL((SELECT $col FROM " . $GLOBALS['ecs']->table($curr_table) . " as $curr_table WHERE $curr_table.goods_id = g.goods_id AND $curr_table.$col > 0), -1) as " . $key . "_price, ";
        $warn_having[] = "(" . $key . "_price != -1 AND " . $key . "_price <= g.cost)";
    }
    $warn_sql = "SELECT " . $warn_cols . " g.goods_name, g.goods_id, g.cost FROM " . $ecs->table("goods") . " g WHERE g.is_delete = 0 AND g.is_on_sale = 1 AND g.is_real = 1 HAVING " . implode(" OR ", $warn_having);
    $res = $db->getAll($warn_sql);

    foreach ($res as $row) {
        foreach ($autoPricingController::AUTO_PRICING_WEBS_COLUMNS as $key => $col) {
            if ($row[$key . "_price"] <= $row['cost'] && $row[$key . "_price"] > 0) {
                $warn_goods[$row['goods_id']]['platform'][] = $autoPricingController::AUTO_PRICING_WEBS[$key];
            }
        }
        $warn_goods[$row['goods_id']]['goods_name'] = $row['goods_name'];
    }

    $sql2 = "SELECT GROUP_CONCAT(distinct(au.user_id) ORDER BY au.user_id DESC) as user_ids, GROUP_CONCAT(distinct(au.user_name) ORDER BY au.user_id DESC) as user_names, GROUP_CONCAT(distinct(esa.supplier_id)) as supplier_ids, egs.goods_id FROM " . $ecs->table('erp_goods_supplier') . " as egs
        LEFT JOIN " . $ecs->table('erp_supplier_admin') . " as esa ON egs.supplier_id = esa.supplier_id
        LEFT JOIN " . $ecs->table('admin_user') . " as au ON esa.admin_id = au.user_id
        WHERE egs.goods_id". db_create_in(array_keys($warn_goods)) ." AND (au.user_id > 0 AND au.user_id != 1) GROUP BY egs.goods_id ORDER BY user_ids DESC";
    foreach ($db->getAll($sql2) as $row) {
        if (empty($row['user_ids'])) continue;
        $admins = explode(",", $row['user_names']);
        $admins_ids = explode(",", $row['user_ids']);
        $curr_admin = [];
        foreach ($admins as $k => $admin) {
            $curr_admin[$admins_ids[$k]] = $admin;
        }

        $first_admin_id = current(array_keys($curr_admin));
        $first_admin    = $curr_admin[$first_admin_id];
        $task_admin[$first_admin_id]['admin_name'] = $first_admin;
        $task_admin[$first_admin_id]['goods'][$row['goods_id']] = $warn_goods[$row['goods_id']];
    }
    $sender_id = 1;
    $sender_name = "admin";
    $now = gmtime();
    $next_work_day = $orderController->get_yoho_next_work_day($now);

    foreach ($task_admin as $receiver_id => $receiver_admin) {
        ksort($receiver_admin['goods']);
        $title = "入貨成本高於其他平台售價 ".local_date('Y-m-d');
        if ($db->getOne("SELECT COUNT(*) FROM " . $ecs->table("admin_taskes") . " WHERE pic = '$receiver_admin[admin_name]' AND title LIKE '$title%'") > 0) {
            continue;
        }
        $task = array(
            'sender_id' => $sender_id,
            'receiver_id' => $receiver_id,
            'sent_time' => $now,
            'read_time' => 0,
            'status' => '0',
            'priority' => "High",
            'dead_time' => $next_work_day,
            'estimated_time' => '480',
            'sender_name' => $sender_name,
            'pic' => $receiver_admin['admin_name'],
            'deleted' => 0,
            'title' => $title
        );
        $admin_goods = array_chunk($receiver_admin['goods'], 200, true);
        foreach ($admin_goods as $msg_no => $goods_seg) {
            if (count($admin_goods) > 1) {
                $task['title'] = "$title (" . ($msg_no + 1) . ")";
            }
            $message = "請檢查以下產品之入貨成本 或 其他平台的售價: <br>";
            foreach ($goods_seg as $goods_id => $goods) {
                $message .= "<a href='/x/auto_pricing.php?act=list&goods_name=$goods[goods_name]'>$goods[goods_name]</a>: " . implode(", ", $goods["platform"]) . ' <br>';
            }

            $task['message'] = mysql_escape_string($message);

            $db->autoExecute($ecs->table('admin_taskes'), $task, 'INSERT');
        }
    }
}

// Auto Pricing
if ($cron['auto_pricing'] == 1) {
    $controller = new Yoho\cms\Controller\AutoPricingController();
    $erpController = new Yoho\cms\Controller\ErpController();
    $days_before = $controller::DEAD_STOCK_DAYS;
    $time_yesterday = local_strtotime("today");
    $time_days_before = local_strtotime("-$days_before days midnight");
    $default_strategy_id = $controller->getDefaultAutoPriceStrategy("default_auto_price");
    $web_sql = "";
    $exclude_sql = "";
    foreach ($controller::AUTO_PRICING_WEBS_COLUMNS as $key => $col) {
        $curr_table = "goods_$key";
        $web_sql .= "IFNULL((SELECT $col FROM " . $GLOBALS['ecs']->table($curr_table) . " as $curr_table WHERE $curr_table.goods_id = g.goods_id AND $curr_table.$col > 0), -1) as " . $key . "_price, ";
    }
    $exclude_rules = $controller->getAutoPricingRuleList(true);
    foreach ($exclude_rules as $rule) {
        $cats = empty($rule['config']['cats'][0]) ? [] : $rule['config']['cats'];
        $brands = empty($rule['config']['brands'][0]) ? [] : $rule['config']['brands'];
        $cond = [];
        if (!empty($cats)) {
            $cond[] = "(cat_id IN (SELECT cat_id FROM " . $ecs->table("category") . " WHERE cat_id " . db_create_in($cats) . " OR parent_id " . db_create_in($cats) . "))";
        }
        if (!empty($brands)) {
            $cond[] = "brand_id " . db_create_in($brands);
        }
        $exclude_sql[] = "(" . implode(" AND ", $cond) . ")";
    }
    $exclude_goods = empty($exclude_sql) ? [] : $db->getCol("SELECT goods_id FROM " . $ecs->table("goods") . (" WHERE " . implode(" OR ", $exclude_sql)));

    // Step 0. update suggestion status if already expired
    $expired_suggest = $db->getCol(
        "SELECT acl.suggest_id FROM ". $ecs->table("auto_pricing_log") . " acl " .
        "LEFT JOIN " . $ecs->table("auto_pricing_suggestion") . " aps ON aps.suggest_id = acl.suggest_id " .
        "WHERE acl.expire_date < CURRENT_TIMESTAMP AND aps.status = " . AUTO_PRICE_STATUS_APPLIED
    );
    if (!empty($expired_suggest)) {
        $db->query("UPDATE " . $ecs->table("auto_pricing_suggestion") . " SET status = " . AUTO_PRICE_STATUS_EXPIRED . " WHERE suggest_id" . db_create_in($expired_suggest));
    }

    // Step 1. get all valid goods list
    $count = $db->getOne(
        "SELECT COUNT(DISTINCT g.goods_id) FROM " . $ecs->table('goods'). " as g " .
        "WHERE g.is_delete = 0 AND g.is_on_sale = 1 AND g.is_real = 1 AND (g.is_promote = 0 OR g.promote_end_date < " . gmtime() . ") " . //"AND g.add_time < $time_days_before " .
        (empty($exclude_goods) ? "" : (" AND g.goods_id NOT " . db_create_in($exclude_goods)))
    );
    $stock_cost_log = false;
    $max_cost_log_time = $db->getOne("SELECT MAX(report_date) FROM " . $ecs->table('stock_cost_log') . " WHERE type = 4");
    if (!empty($max_cost_log_time))  {
        $stock_cost_log = true;
    }

    $loop = 0;
    $goods_list = [];
    while ($count > 0) {
        $sql = "SELECT g.goods_id, g.goods_name, g.shop_price, g.cost, g.market_price, g.price_strategy, g.promote_price, g.add_time, " .
                    "IFNULL(mp.user_price, -1) as vip_price, g.auto_pricing, " .
                    $web_sql .
                    ($stock_cost_log
                        ? "IFNULL((SELECT deadgroup_$days_before FROM " . $ecs->table('stock_cost_log') . " as scl WHERE scl.type = 4 AND scl.report_date = $max_cost_log_time AND scl.type_id = g.goods_id), 0) as deadgroup "
                        : (
                            "IFNULL((SELECT sum(ega.goods_qty) FROM " . $ecs->table('erp_goods_attr_stock') . " as ega WHERE ega.goods_id = g.goods_id AND ega.warehouse_id IN (".$erpController->getSellableWarehouseIds(true).")), 0) as goods_number, " .
                            "IFNULL((SELECT SUM(og.goods_number) FROM " . $ecs->table('order_goods') . " as og LEFT JOIN " . $ecs->table('order_info') . " as oi ON oi.order_id = og.order_id WHERE og.goods_id = g.goods_id " . order_query_sql('finished', 'oi.') . " AND (oi.shipping_time BETWEEN " . $time_days_before . " AND " . ($time_yesterday+86400) . ")), 0) as ndayssales "
                        )
                    ) .
                "FROM " . $ecs->table('goods'). " as g " .
                "LEFT JOIN (SELECT goods_id, user_price FROM " . $ecs->table('member_price'). " WHERE user_rank = 2) as mp ON mp.goods_id = g.goods_id " .
                "WHERE g.is_delete = 0 AND g.is_on_sale = 1 AND g.is_real = 1 AND (g.is_promote = 0 OR g.promote_end_date < " . gmtime() . ") " . //"AND g.add_time < $time_days_before " .
                // " AND g.goods_id = 2287" .
                (empty($exclude_goods) ? "" : (" AND g.goods_id NOT " . db_create_in($exclude_goods))) .
                " ORDER BY g.goods_id ASC LIMIT " . ($loop * 1000) . ", 1000";
        $goods_list = array_merge($goods_list, $db->getAll($sql));
        $count -= 1000;
        $loop++;
    }

    // no on sale suggestion made if any on sale suggestion is running
    $ignore_dead_stock = $db->getOne("SELECT COUNT(*) FROM " . $ecs->table("auto_pricing_log") . " WHERE expire_date > CURRENT_TIMESTAMP AND suggest_type = " . AUTO_PRICE_TYPE_DEAD);
    foreach ($goods_list as $good) {
        $dead = false;
        $dead_day = 0;
        if ($stock_cost_log) {
            if ($good['deadgroup'] == -1) {
                $dead_day = 180;
            } else {
                $dead_day = $good['deadgroup'];
            }
        } else {
            if ($good['goods_number'] > 0) {
                if ($good['ndayssales'] == 0) {
                    $dead_day = 180;
                } else {
                    $r_day = round($row['goods_number'] / $row['ndayssales'] * $days_before);
                    $d_group = ($r_day - $r_day % 30) / 30;
                    $dead_day = $r_day >= 180 ? 180 : $d_group * 30;
                }
            }
        }
        if ($dead_day > 0 && $row['add_time'] < $time_days_before) {
            $dead = true;
        }
        $strategy_id = 0;

        // Step 2. get selected strategy
        if ($cron['auto_pricing'] == 1) {
            if (empty($good['price_strategy']) && !empty($default_strategy_id)) {
                $strategy_id = $default_strategy_id;
            } elseif (!empty($good['price_strategy']) && $good['price_strategy'] != "-1") {
                $strategy_id = $good['price_strategy'];
            }
        }

        if (!empty($strategy_id)) {
            $strategy = $controller->getAutoPricingStrategy($strategy_id);
            $condition = $strategy['condition'];
            $select = $strategy['select'];
            $compare_price = 0;
            $select_price = 0;
            $price_limit = 0;
            $suggest_price = floatval($good['shop_price']);
            $sale_price = -1;
            $vip_price = -1;

            // get required price
            $prices = [];
            $prices_webs = [];
            foreach (array_keys($controller::AUTO_PRICING_WEBS) as $col) {
                $p = floatval($good[$col . "_price"]);
                if ($p > 0 && in_array($col, $strategy['reference'])) {
                    $prices[$col] = $p;
                    $prices_webs[] = $p;
                }
            }
            foreach (array_keys($controller::AUTO_PRICING_VALUES) as $col) {
                if ($col == 'max') {
                    $p = max($prices_webs);
                } elseif ($col == 'min') {
                    $p = min($prices_webs);
                } elseif ($col == 'avg') {
                    $p = array_sum($prices_webs) / count($prices_webs);
                }
                if ($p > 0) {
                    $prices[$col] = $p;
                }
            }

            // get condition price
            $compare_price = $prices[$condition];

            // get select price
            $select_price = $prices[$select];
            
            // get price limit
            if (!empty($strategy['price_limit'])) {
                if ($strategy['price_limit']['type'] == 1) {
                    $price_limit = $good['cost'] + intval($strategy['price_limit']['amount']);
                } elseif ($strategy['price_limit']['type'] == 2) {
                    $price_limit = $good['cost'] * (1 + intval($strategy['price_limit']['amount']) / 100);
                }
            }
            if ($price_limit < 0) {
                $price_limit = 0;
            }

            // process auto on sale
            if ($dead && !empty($strategy['on_sale']) && !$ignore_dead_stock) {
                $on_sale = $strategy['on_sale'];
                if ($dead_day > intval($on_sale['target'])) {
                    $sale_amount = 0;
                    if (!empty($on_sale['amount']['default'])) {
                        $sale_amount = intval($on_sale['amount']['default']);
                    } elseif (!empty($strategy['on_sale']['amount'][$dead_day])) {
                        $sale_amount = intval($on_sale['amount'][$dead_day]);
                    }
                    $gross_profit = floatval($good['shop_price']) - floatval($good['cost']);
                    if ($on_sale['type'] == 1) {
                        $sale_price = floatval($good['shop_price']) - intval($sale_amount);
                    } else {
                        $sale_price = round(floatval($good['shop_price']) - ($gross_profit * intval($sale_amount) / 100));
                    }
                    // Step 3. suggest promotion if D180
                    // 2018-09-11: On sale only if dead_day >= 180
                    if ($sale_price != -1 && $sale_price < $suggest_price && $sale_price > 0 && $dead_day >= 180) {
                        insertSuggestPrice($good['goods_id'], $strategy['strategy_id'], AUTO_PRICE_TYPE_DEAD, $sale_price, $good['promote_price'], $dead_day, $on_sale['duration']);
                    }
                }
            }

            // adjust normal price
            if (!empty($strategy['normal']) && $strategy_id != $default_strategy_id) {
                if ($compare_price > 0 && $select_price > 0) {
                    if (!$db->getOne("SELECT 1 FROM " . $ecs->table("auto_pricing_log") . " WHERE expire_date > CURRENT_TIMESTAMP AND goods_id = $good[goods_id] AND suggest_type = 0")) {
                        $compare_price_min = $compare_price * (1 - intval($strategy['condition_percent']) / 100);
                        $compare_price_max = $compare_price * (1 + intval($strategy['condition_percent']) / 100);
                        if ((floatval($good['shop_price']) < $compare_price_min || floatval($good['shop_price']) > $compare_price_max) && $good['shop_price'] > $price_limit) {
                            $suggest_price = round($select_price * intval($strategy['percent']) / 100);
                            if ($price_limit > 0 && $suggest_price < $price_limit) {
                                $suggest_price = $price_limit;
                            }
                            if ($suggest_price > $good['market_price']) {
                                $suggest_price = $good['market_price'];
                            }
                            if ($suggest_price < 0) {
                                $suggest_price = 0;
                            }
                            // Step 4. suggest shop_price if auto process enabled
                            if (round($suggest_price) != floatval($good['shop_price'])) {
                                insertSuggestPrice($good['goods_id'], $strategy['strategy_id'], AUTO_PRICE_TYPE_NORMAL, $suggest_price, $good['shop_price'], $dead_day);
                            }
                        }
                    }
                }
            }

            // update vip price
            if (!empty($strategy['vip'])) {
                if ($good['vip_price'] != -1) {
                    $vip_duration = 0;
                    if ($sale_price != -1) {
                        $vip_price = round($sale_price * intval($strategy["vip_percent"]) / 100);
                        $vip_duration = $on_sale['duration'];
                    } elseif ($suggest_price > 0) {
                        $vip_price = round($suggest_price * intval($strategy["vip_percent"]) / 100);
                    }
                    if ($vip_price != -1 && $vip_price < $good['vip_price']) {
                        // Step 5. suggest vip_price if auto process enabled
                        insertSuggestPrice($good['goods_id'], $strategy['strategy_id'], AUTO_PRICE_TYPE_VIP, $vip_price, $good['vip_price'], $dead_day, $vip_duration);
                    }
                }
            }
        }
    }

    // Step 6. apply shop_price and vip_price suggestion
    foreach ($goods_list as $good) {
        $auto_price_suggestions = $db->getCol("SELECT suggest_id FROM " . $ecs->table("auto_pricing_suggestion") . " WHERE goods_id = $good[goods_id] AND status = " . AUTO_PRICE_STATUS_SUGGESTED . " AND suggest_type " . db_create_in([AUTO_PRICE_TYPE_NORMAL, AUTO_PRICE_TYPE_VIP]));
        foreach ($auto_price_suggestions as $suggest_id) {
            $controller->updateAutoPricingSuggestion($suggest_id, AUTO_PRICE_STATUS_APPLIED, true);
        }
    }

    // Step 7. apply dead stock suggestion
    $on_sale_suggestion_ids = $db->getCol("SELECT suggest_id FROM " . $ecs->table("auto_pricing_suggestion") . " WHERE status = " . AUTO_PRICE_STATUS_SUGGESTED . " AND suggest_type = " . AUTO_PRICE_TYPE_DEAD);
    shuffle($on_sale_suggestion_ids);
    foreach ($on_sale_suggestion_ids as $key => $suggest_id) {
        // apply only first 500 suggestions, reject otherwise
        if ($key < intval($cron['max_on_sale_number'])) {
            $controller->updateAutoPricingSuggestion($suggest_id, AUTO_PRICE_STATUS_APPLIED, true);
        } else {
            $controller->updateAutoPricingSuggestion($suggest_id, AUTO_PRICE_STATUS_REJECTED, true);
        }
    }
}

function insertSuggestPrice($goods_id, $strategy_id, $suggest_type, $suggest_price, $origin_price, $dead_group = 0, $suggest_duration = 0)
{
    global $ecs, $db;

    $db->query('START TRANSACTION');
    $suggest_price = round($suggest_price);
    $suggest_price = $suggest_price < 0 ? 0 : $suggest_price;
    if (!$db->getOne("SELECT 1 FROM " . $ecs->table("auto_pricing_suggestion") . " WHERE strategy_id = $strategy_id AND suggest_type = $suggest_type AND suggest_price = '$suggest_price' AND goods_id = $goods_id AND suggest_duration = $suggest_duration AND status = " . AUTO_PRICE_STATUS_SUGGESTED)) {
        $db->query("UPDATE " . $ecs->table("auto_pricing_suggestion") . " SET status = " . AUTO_PRICE_STATUS_EXPIRED . " WHERE suggest_type = $suggest_type AND goods_id = $goods_id AND status != " . AUTO_PRICE_STATUS_REJECTED);
        $db->query("INSERT INTO " . $ecs->table("auto_pricing_suggestion") . " (goods_id, strategy_id, suggest_type, suggest_price, suggest_duration) VALUES ($goods_id, $strategy_id, $suggest_type, '$suggest_price', $suggest_duration)");
        $suggest_id = $db->insert_id();
        $db->query("UPDATE " . $ecs->table("auto_pricing_log") . " SET expire_date = CURRENT_TIMESTAMP WHERE suggest_type = $suggest_type AND goods_id = $goods_id AND expire_date > CURRENT_TIMESTAMP");
        $db->query("INSERT INTO " . $ecs->table("auto_pricing_log") . " (suggest_id, goods_id, strategy_id, suggest_type, log_type, old_price, new_price, dead_group) VALUES ($suggest_id, $goods_id, $strategy_id, $suggest_type, 0, $origin_price, $suggest_price, $dead_group)");
    }
    
    $db->query('COMMIT');
}
?>