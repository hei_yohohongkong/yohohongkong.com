<?php

if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}
$cron_lang          = ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/cron/products.php';
$categoryController = new Yoho\cms\Controller\CategoryController();
if (file_exists($cron_lang))
{
	global $_LANG;

	include_once($cron_lang);
}
if (file_exists($cron_lang))
{
	global $_LANG;

	include_once($cron_lang);
}
/* 模块的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
	$i = isset($modules) ? count($modules) : 0;

	/* 代码 */
	$modules[$i]['code']	= basename(__FILE__, '.php');

	/* 描述对应的语言项 */
	$modules[$i]['desc']	= 'auto_best_seller_desc';

	/* 作者 */
	$modules[$i]['author']  = 'Anthony';

	/* 版本号 */
	$modules[$i]['version'] = '1.0.0';

	/* 配置信息 */
	$modules[$i]['config']  = array(

    );

	return;
}

$cat_list = $categoryController->top_visit_sold_goods(false);
$update_goods = [];
foreach ($cat_list as $key => $cat) {
    foreach ($cat['goods'] as $key => $goods) {
        $update_goods[] = $goods['goods_id'];
    }
}

// Remove and add is_best_sellers
$db->query('START TRANSACTION');
$sql = "UPDATE " . $ecs->table('goods') . " SET is_best_sellers = 0 WHERE is_best_sellers = 1 ";

if(!$db->query($sql)){
	$db->query('ROLLBACK');
}
$sql = "UPDATE " . $ecs->table('goods') . " SET is_best_sellers = 1 WHERE goods_id ".db_create_in($update_goods);
if(!$db->query($sql)){
	$db->query('ROLLBACK');
}
$db->query('COMMIT');

?>