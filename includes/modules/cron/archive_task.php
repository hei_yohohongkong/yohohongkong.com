<?php
if (!defined('IN_ECS')) {
    die('Hacking attempt');
}

$cron_lang = ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/cron/archive_task.php';

if (file_exists($cron_lang)) {
    global $_LANG;

    include_once($cron_lang);
}

/* 模块的基本信息 */
if (isset($set_modules) && $set_modules == true) {
    $i = isset($modules) ? count($modules) : 0;

    /* 代码 */
    $modules[$i]['code']    = basename(__FILE__, '.php');

    /* 描述对应的语言项 */
    $modules[$i]['desc']    = 'archive_task_desc';

    /* 作者 */
    $modules[$i]['author']  = 'Eric';

    /* 网址 */
    $modules[$i]['website'] = '';

    /* 版本号 */
    $modules[$i]['version'] = '1.0.0';

    /* 配置信息 */
    $modules[$i]['config']  = array(
    );

    return;
}

archive_tasks();

function archive_tasks()
{
    global $db, $ecs, $_LANG, $userController;

    $date = strtotime("-0 day", gmtime());
    // echo '<br>';
    // echo local_date('Y-m-d',$date);
    // exit;

    // archive the tasks after task finished two days (4:完成並提交覆核)
    $sql = "select * from ".$ecs->table('admin_taskes')." where deleted = 0 and status = 4 and sender_id = 1 and finish_time <= ".$date."  " ;
    $res = $db->getAll($sql);

    foreach ($res as $task) {
        // set task to read
        $admin_id = 1;
        $task_id = $task['task_id'];
        $q1 = "UPDATE ".$ecs->table('admin_tasklog')." l JOIN ".$ecs->table('admin_taskes')." t ON t.task_id = l.task_id SET l.sender_read=1,l.receiver_read=1 WHERE l.task_id=".$task_id." AND (l.sender_read=0 AND t.sender_id=".$admin_id." OR l.receiver_read=0 AND t.receiver_id=".$admin_id.")";
        $db->query($q1);
        
        // log finish status
        $sender_read=1;
        $receiver_read=0;
        $content = '系統自動完成';
        $new_status = 5; // (5 : 已完成)
        $db->query('SET NAMES utf8');
        $q2="INSERT INTO ".$ecs->table('admin_tasklog')." (task_id,sender_read,receiver_read,content,new_status,created_by) VALUE (".$task_id.",".$sender_read.",".$receiver_read.",'".$content."','".$new_status."',".$admin_id.")";
        if ($db->query($q2)) {
            $tasklogId = $db->insert_id();
            // auto update to archive and change to finish status
            $q3 = "UPDATE ".$ecs->table('admin_taskes')." SET deleted = 1, status='".$new_status."' WHERE task_id=".$task_id;
            $db->query($q3);
        }
    }

    // archive the tasks after task finished one days (5:已完成)
    $sql = "select * from ".$ecs->table('admin_taskes')." where deleted = 0 and status = 5 and finish_time <= ".$date."  " ;
    $res = $db->getAll($sql);

    foreach ($res as $task) {
        // set task to read
        $admin_id = 1;
        $task_id = $task['task_id'];
       
        // auto update to archive and change to finish status
        $q3 = "UPDATE ".$ecs->table('admin_taskes')." SET deleted = 1 WHERE task_id=".$task_id;
        $db->query($q3);
    }

    return true;
}
