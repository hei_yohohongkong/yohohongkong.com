<?php
ini_set('memory_limit','2048M');
if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}
$cron_lang = ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/cron/sale_cash_report.php';
require_once(ROOT_PATH . 'includes/lib_order.php');

if (file_exists($cron_lang))
{
	global $_LANG;

	include_once($cron_lang);
}

if (isset($set_modules) && $set_modules == TRUE)
{
	$i = isset($modules) ? count($modules) : 0;

	/* 代码 */
	$modules[$i]['code']	= basename(__FILE__, '.php');

	/* 描述对应的语言项 */
	$modules[$i]['desc']	= 'sale_cash_report_desc';

	/* 作者 */
	$modules[$i]['author']  = 'ken';

	/* 版本号 */
	$modules[$i]['version'] = '1.0.0';

	/* 配置信息 */
	$modules[$i]['config']  = array(
        array('name' => 'sale_cash_report', 'type' => 'select', 'value' => '0')
    );

	return;
}

empty($cron['sale_cash_report']) && $cron['sale_cash_report'] = 0;

$userController = new Yoho\cms\Controller\UserController();
$orderController = new Yoho\cms\Controller\OrderController();
$erpController = new Yoho\cms\Controller\ErpController();

$today = date("Y-m-d");
//$shop_warehouse_ids = $erpController->getShopWarehouseId(FALSE); // FALSE = get more than one record
$shop_warehouse_ids = array(array("warehouse_id" => 1));
$shop_list = array();
foreach ($shop_warehouse_ids as $shop){
    $shop_id = $shop["warehouse_id"];

    $filter['shop'] = $shop_id;
    $filter['report_date'] = $today;
    $sql = "SELECT * " .
    "FROM " . $ecs->table('sale_cash_report') .
    "WHERE report_date = '".$filter['report_date']."' and shop_id = ".$filter['shop']." " .
    "ORDER BY reviewer_time DESC LIMIT 1";

    $cash_report = $db->getRow($sql);

    if(!$cash_report){
        balance_review($filter['shop']);
    }
}

function get_cash_report_data($filter){
    global $db, $ecs, $userController;

    $report_data = [];
    $start_time = mktime(0, 0, 0, date("m", strtotime($filter['report_date'])) , date("d", strtotime($filter['report_date'])), date("Y", strtotime($filter['report_date'])));
    $end_time = mktime(23, 59, 59, date("m", strtotime($filter['report_date'])) , date("d", strtotime($filter['report_date'])), date("Y", strtotime($filter['report_date'])));

    $sql = "SELECT IFNULL(sp.sales_name, au.user_name) as sales_name " .
            "FROM " . $ecs->table('admin_user') ." as au ".
            "JOIN " . $ecs->table('salesperson') ." as sp ON sp.admin_id = au.user_id AND sp.available = 1 And sp.allow_pos = 1 ".
            "WHERE is_disable = 0 " .
            "ORDER BY sales_name ASC ";
    $sales_name = $db->getCol($sql);

    /* 昨天門市關店現金結餘 */
    $filter['yesterday_report_date'] = local_date('Y-m-d', (local_strtotime($filter['report_date']) - 1));
    $sql = "SELECT * " .
    "FROM " . $ecs->table('sale_cash_report') .
    "WHERE report_date = '".$filter['yesterday_report_date']."' ".
    //"AND shop_id = ".$filter['shop']." " .
    "ORDER BY reviewer_time DESC LIMIT 1";
    $cash_report = $db->getRow($sql);
    if(!$cash_report){
        $report_data['shop_open_cash_balance_c1'] = 704559.2;
        $report_data['shop_open_cash_balance_c2'] = 32000;
        $report_data['shop_open_cash_balance_d'] = 52309;
    }
    else{
        $cash_report_data = unserialize($cash_report["data"]);
        $report_data['shop_open_cash_balance_c1'] = $cash_report_data['shop_close_cash_balance_c1'];
        $report_data['shop_open_cash_balance_c2'] = $cash_report_data['shop_close_cash_balance_c2'];
        $report_data['shop_open_cash_balance_d'] = $cash_report_data['shop_close_cash_balance_d'];
    }
    
    $report_data['shop_open_cash_total'] = $report_data['shop_open_cash_balance_c1']+$report_data['shop_open_cash_balance_c2']+$report_data['shop_open_cash_balance_d'];

    $shop_close_cash_balance_c1 = $report_data['shop_open_cash_balance_c1'];
    $shop_close_cash_balance_c2 = $report_data['shop_open_cash_balance_c2'];
    $shop_close_cash_balance_d = $report_data['shop_open_cash_balance_d'];

    /* 當天退款的現金 */
    $where_status = " AND rr.status " . db_create_in(array(REFUND_FINISH));
    $where_status .= " AND oi.pay_id in (2, 3) "; // eps or cash
    $where_status .= " AND (oi.platform = 'pos') "; // pos only
    $where_time = " AND rr.request_time BETWEEN " . $start_time . " AND " . $end_time;
    $where_time = " AND oi.add_time BETWEEN " . $start_time . " AND " . $end_time;

    $sql  = "SELECT SUM(CASE WHEN oi.shipping_status IN (1,2) THEN refund_amount ELSE 0 END) AS today_order_refund_amount_c1,";
    $sql .= " SUM(CASE WHEN oi.shipping_status NOT IN (1,2) THEN refund_amount ELSE 0 END) AS today_order_refund_amount_d";
    $sql .= " FROM " .$ecs->table('refund_requests') . " rr";
    $sql .= " LEFT JOIN (SELECT order_id, order_sn, shipping_status, pay_id, platform, add_time FROM " . $GLOBALS['ecs']->table('order_info') . ") oi ON rr.order_id = oi.order_id";
    $sql .= " WHERE 1 " . $where_status . $where_time;
    //die($sql);

    $today_order_refund_data = $db->getRow($sql);

    /* get sub total 當日現金(已出貨) C1 */
    $sql = '';
    // Exclude orders for the followiung user accounts: 11111111, 88888888, 99999999
    $where_exclude = " AND (order_type<>'".Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE."' OR (order_type IS NULL AND user_id NOT IN (".implode(',', $userController->getWholesaleUserIds())."))) ";
    // Exclude sales agent
    $where_exclude .= " AND (order_type <> '" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT . "' OR order_type IS NULL) ";
    // Exclude 展場POS
    $where_exclude .= " AND (platform <> 'pos_exhibition') ";

    $where_status = " AND order_status NOT IN (" . OS_CANCELED . ',' . OS_INVALID . ',' . OS_RETURNED . ") ";
    $where_status .= " AND shipping_status IN (" . SS_SHIPPED . ',' . SS_RECEIVED . ") ";
    $where_status .= " AND pay_id = 3 "; // cash
    //$where_status .= " AND shop_id = " .$filter['shop']; // shop

    $where_time = " AND shipping_time BETWEEN " . $start_time . " AND " . $end_time;
    $where_time .= " AND pay_time BETWEEN " . $start_time . " AND " . $end_time;

    $sql  = "SELECT SUM(`money_paid` + `order_amount`) as total_amount ";
    $sql .= " FROM " .$ecs->table('order_info');
    $sql .= " WHERE 1 " . $where_exclude . $where_status . $where_time;
    $sql .= " And `salesperson` " . db_create_in($sales_name) ;

    $report_data['cash_shipped_c1'] = $db->getOne($sql);
    $report_data['cash_shipped_c1'] += $today_order_refund_data['today_order_refund_amount_c1'];
    $report_data['cash_shipped_total'] = $report_data['cash_shipped_c1'];
    $shop_close_cash_balance_c1 += $report_data['cash_shipped_c1'];
    
    /* get sub total cash 當日現金(未出貨) D */
    $sql = '';
    // Exclude orders for the followiung user accounts: 11111111, 88888888, 99999999
    $where_exclude = " AND (order_type<>'".Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE."' OR (order_type IS NULL AND user_id NOT IN (".implode(',', $userController->getWholesaleUserIds())."))) ";
    // Exclude sales agent
    $where_exclude .= " AND (order_type <> '" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT . "' OR order_type IS NULL) ";
    // Exclude 展場POS
    $where_exclude .= " AND (platform <> 'pos_exhibition') ";

    $where_status = " AND order_status NOT IN (" . OS_CANCELED . ',' . OS_INVALID . ',' . OS_RETURNED . ") " ;
    $where_status .= " AND pay_status = " . PS_PAYED . " "; // paid
    $where_status .= " AND shipping_status NOT IN (" . SS_SHIPPED . ',' . SS_RECEIVED . ") "; // SS_UNSHIPPED,SS_PREPARING,SS_SHIPPED_PART,SS_SHIPPED_ING,OS_SHIPPED_PART
    $where_status .= " AND pay_id = 3 "; // cash
    //$where_status .= " AND shop_id = " .$filter['shop']; // shop

    $where_time = " AND pay_time BETWEEN " . $start_time . " AND " . $end_time;

    $sql  = "SELECT SUM(`money_paid` + `order_amount`) as total_amount ";
    $sql .= " FROM " .$ecs->table('order_info');
    $sql .= " WHERE 1 " . $where_exclude . $where_status . $where_time;
    $sql .= " And `salesperson` " . db_create_in($sales_name) ;

    $report_data['cash_unshipped_d'] = $db->getOne($sql);
    $report_data['cash_unshipped_d'] += $today_order_refund_data['today_order_refund_amount_d'];
    $report_data['cash_unshipped_total'] = $report_data['cash_unshipped_d'];
    $shop_close_cash_balance_d += $report_data['cash_unshipped_d'];

    /* get total refund by cash C1/D */

    $where_status = " AND rr.status " . db_create_in(array(REFUND_PENDING, REFUND_PROCESS, REFUND_FINISH));
    $where_status .= " AND oi.pay_id in (2, 3) "; // eps or cash
    $where_status .= " AND (oi.platform = 'pos') "; // pos only
    //$where_status .= " AND oi.shop_id = " .$filter['shop']; // shop
    //$where_status .= " AND oi.shipping_status IN (1,2) " ;
    $where_time = " AND rr.request_time BETWEEN " . $start_time . " AND " . $end_time;

    $sql  = "SELECT SUM(CASE WHEN oi.shipping_status IN (1,2) THEN refund_amount ELSE 0 END) AS refund_amount_c1,";
    $sql .= " SUM(CASE WHEN oi.shipping_status NOT IN (1,2) THEN refund_amount ELSE 0 END) AS refund_amount_d";
    $sql .= " FROM " .$ecs->table('refund_requests') . " rr";
    $sql .= " LEFT JOIN (SELECT order_id, order_sn, shipping_status, pay_id, platform FROM " . $GLOBALS['ecs']->table('order_info') . ") oi ON rr.order_id = oi.order_id";
    $sql .= " WHERE 1 " . $where_status . $where_time;

    $refund_data = $db->getRow($sql);
    $report_data['refund_amount_c1'] = $refund_data['refund_amount_c1']; //當日現金退款 C1
    $report_data['refund_amount_d'] = $refund_data['refund_amount_d']; //當日現金退款 D

    $shop_close_cash_balance_c1 -= $report_data['refund_amount_c1'];
    $shop_close_cash_balance_d -= $report_data['refund_amount_d'];
    
    /* get sub total cash 己付款,已出貸, 不是今日的訂單 C1加 D減*/
    // 銷售金額
    $sql = '';
    // Exclude orders for the followiung user accounts: 11111111, 88888888, 99999999
    $where_exclude = " AND (order_type<>'".Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE."' OR (order_type IS NULL AND user_id NOT IN (".implode(',', $userController->getWholesaleUserIds())."))) ";
    // Exclude sales agent
    $where_exclude .= " AND (order_type <> '" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT . "' OR order_type IS NULL) ";
    // Exclude 展場POS
    $where_exclude .= " AND (platform <> 'pos_exhibition') ";

    $where_status = " AND order_status IN (" . OS_CONFIRMED . ") " ;
    $where_status .= " AND pay_status = " . PS_PAYED . " "; // paid
    $where_status .= " AND shipping_status IN (" . SS_SHIPPED . ',' . SS_RECEIVED . ") "; // SS_UNSHIPPED,SS_PREPARING,SS_SHIPPED_PART,SS_SHIPPED_ING,OS_SHIPPED_PART
    $where_status .= " AND pay_id = 3 "; // cash
    //$where_status .= " AND shop_id = " .$filter['shop']; // shop
    $where_time  = " AND shipping_time BETWEEN " . $start_time . " AND " . $end_time;
    $where_time  .= " AND add_time NOT BETWEEN " . $start_time . " AND " . $end_time;

    $sql .= "select SUM(`money_paid`) as total_amount ";
    $sql .= " FROM " .$ecs->table('order_info');
    $sql .= " WHERE 1 " . $where_exclude . $where_status . $where_time;
    $report_data['today_ship_not_today_paid_c1'] = $db->getOne($sql);
    $report_data['today_ship_not_today_paid_d'] = $db->getOne($sql);
    $report_data['today_ship_not_today_paid_total'] = $report_data['today_ship_not_today_paid_c1'] - $report_data['today_ship_not_today_paid_d'];
    $shop_close_cash_balance_c1 += $report_data['today_ship_not_today_paid_c1'];
    $shop_close_cash_balance_d -= $report_data['today_ship_not_today_paid_d'];

    /* 收入/支出 */
    $operation_account_income = array();
    $operation_account_expenses = array();
    //$where_status  = " AND shop_id = '" . $filter['shop'] . "'";
    $where_status  = " ";
    $where_time  = " AND sale_date = '" . $filter['report_date'] . "'";

    $sql  = "SELECT *";
    $sql .= " FROM " .$ecs->table('sale_cash_operation');
    $sql .= " WHERE 1 " . $where_status . $where_time;
    $cash_operation = $db->getAll($sql);
    foreach($cash_operation as $operation){
        if($operation['operation_type'] == "income"){
            if($operation['operation_account'] == "C1"){
                $shop_close_cash_balance_c1 += $operation['operation_amount'];
            }
            else if($operation['operation_account'] == "C2"){
                $shop_close_cash_balance_c2 += $operation['operation_amount'];
            }
            else if($operation['operation_account'] == "D"){
                $shop_close_cash_balance_d += $operation['operation_amount'];
            }
            $operation_account_income[] = $operation;
        }
        else if($operation['operation_type'] == "expenses"){
            if($operation['operation_account'] == "C1"){
                $shop_close_cash_balance_c1 -= $operation['operation_amount'];
            }
            else if($operation['operation_account'] == "C2"){
                $shop_close_cash_balance_c2 -= $operation['operation_amount'];
            }
            else if($operation['operation_account'] == "D"){
                $shop_close_cash_balance_d -= $operation['operation_amount'];
            }
            $operation_account_expenses[] = $operation;
        }
    }
    
    $report_data['shop_close_cash_balance_c1'] = $shop_close_cash_balance_c1;
    $report_data['shop_close_cash_balance_c2'] = $shop_close_cash_balance_c2;
    $report_data['shop_close_cash_balance_d'] = $shop_close_cash_balance_d;
    $report_data['shop_close_cash_balance_total'] = $shop_close_cash_balance_c1+$shop_close_cash_balance_c2+$shop_close_cash_balance_d;

    $report_data = array_map("format_report_data", $report_data);

    $report_data['operation_account_income'] = $operation_account_income;
    $report_data['operation_account_expenses'] = $operation_account_expenses;

    return $report_data;
}

function format_report_data($num){
    if($num == null){
        $num = 0;
    }

    return number_format((float)$num, 2, '.', '');
}

function balance_review($shop_id){
    global $db, $ecs;

    $reviewer_id = $_REQUEST['reviewer_id'];
    $filter['shop'] = $shop_id;
    $filter['report_date'] = local_date('Y-m-d');

    $data = get_cash_report_data($filter);
    
    $cash_report = array(
        'report_date'   => local_date('Y-m-d'),
        'shop_id'       => $filter['shop'],
        'data'          => serialize($data),
        'reviewer_id'   => 1,
        'reviewer_time' => gmtime()
    );

    $db->autoExecute($ecs->table('sale_cash_report'), $cash_report, 'INSERT');
}

//$sql = "INSERT INTO ".$ecs->table('stock_cost_warehouse_log')." (data_str, report_date, created_at) VALUES ('".$serialized_cost_arr."', ".$start_time.", ".$log_time.")";
//$db->query($sql);

?>