<?php

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

$cron_lang = ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/cron/auto_task.php';
$orderController = new Yoho\cms\Controller\OrderController();
$userController  = new Yoho\cms\Controller\UserController();

if (file_exists($cron_lang))
{
    global $_LANG;

    include_once($cron_lang);
}

/* 模块的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
    $i = isset($modules) ? count($modules) : 0;

    /* 代码 */
    $modules[$i]['code']    = basename(__FILE__, '.php');

    /* 描述对应的语言项 */
    $modules[$i]['desc']    = 'auto_task_desc';

    /* 作者 */
    $modules[$i]['author']  = 'Anthony';

    /* 网址 */
    $modules[$i]['website'] = '';

    /* 版本号 */
    $modules[$i]['version'] = '1.0.0';

    /* 配置信息 */
    $modules[$i]['config']  = array(
    );

    return;
}
// If today is holiday, don't run this task.
if(!$orderController->is_holiday()){
    $now = gmtime();
    $next_work_day = $orderController->get_yoho_next_work_day($now);
    need_purchase_cron($now, $next_work_day);
    last_3_day_order($now, $next_work_day);
}

//需訂貨 但無remark
function need_purchase_cron($now, $next_work_day){

    global $db, $ecs, $_LANG, $userController;

    // Table name
    $t_ega = $ecs->table('erp_goods_attr_stock');
    $t_egs = $ecs->table('erp_goods_supplier');
    $t_esa = $ecs->table('erp_supplier_admin');
    $t_au  = $ecs->table('admin_user');
    $t_og  = $ecs->table('order_goods');
    $t_oa  = $ecs->table('order_action');
    $t_oi  = $ecs->table('order_info');
    $t_eo  = $ecs->table('erp_order');
    $t_p   = $ecs->table('payment');
    $t_u   = $ecs->table('users');
    $t_o   = $ecs->table('order_info');

    $sql = "SELECT eo.client_order_sn, o.order_id, o.order_sn, o.add_time, o.order_status, o.shipping_status,
    o.order_amount, o.money_paid, o.cp_price, o.pay_status, o.order_type, o.type_sn, o.type_id,
    o.actg_paid_amount,o.pay_id, o.extension_code, o.extension_id, o.postscript,
    o.serial_numbers, o.consignee, o.address, o.country, o.email, o.tel, o.shipping_name,
    o.shipping_fod, o.language, p.pay_name,
    ( o.goods_amount + o.tax + o.shipping_fee + o.insure_fee + o.pay_fee + o.pack_fee + o.card_fee ) AS total_fee,
    (o.money_paid + o.order_amount) AS order_total,
    IF((
        SELECT min(stock)
        FROM (
            SELECT og.order_id, (SUM(ega.goods_qty) - og.goods_number + og.send_number) as stock
            FROM $t_ega as ega
            LEFT JOIN $t_og as og ON ega.goods_id = og.goods_id AND ega.warehouse_id IN (1)
            GROUP BY og.order_id, og.goods_id
        ) as gs WHERE gs.order_id = o.order_id) >= 0, 'Y', 'N'
    ) AS all_in_stock,
    IFNULL((
        SELECT max(flashdeal_id) FROM $t_og as og1 WHERE og1.order_id = o.order_id GROUP BY og1.order_id), 0
    ) as flashdeal_id,
    IFNULL(u.user_name, '匿名用戶') AS buyer,
    o.user_id, o.salesperson, o.platform, o.fully_paid, o.actg_paid_amount,
    IFNULL((SELECT count(*) FROM $t_oa as oa WHERE oa.order_id = o.order_id), 0) as action_count,
    IFNULL((SELECT max(1) FROM $t_oa as oa WHERE oa.order_id = o.order_id AND oa.action_note LIKE '%已向供應商訂貨%'), 0) as ordered_from_supplier
    FROM $t_oi AS o
    LEFT JOIN $t_eo AS eo ON eo.client_order_sn=o.order_sn
    LEFT JOIN $t_p AS p ON p.pay_id=o.pay_id
    LEFT JOIN $t_u AS u ON u.user_id=o.user_id
    WHERE 1 ".
    " AND (o.order_type <> '".Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE."' OR o.order_type IS NULL) AND o.user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).")".
    " AND o.order_status IN ('1','5','6')
    AND o.shipping_status IN ('0','3','5','4')
    AND ( o.pay_status IN ('2','1') OR o.pay_id IN ('6') ) HAVING all_in_stock = 'N'
    AND ordered_from_supplier = 0;";

    $res = $db->getAll($sql);

    foreach ($res as $key => $value) {
        $order_ids[] = $value['order_id'];
    }
    if(empty($order_ids)) return true;
    // Group by goods.
    $sql = "SELECT gs.*, GROUP_CONCAT(gs.order_sn) as order_sns, GROUP_CONCAT(gs.order_id) as order_ids, gs.goods_name FROM (SELECT o.order_sn, og.order_id, og.goods_id, og.goods_name, (SUM(ega.goods_qty) - og.goods_number + og.send_number) as stock
    FROM $t_og as og
    LEFT JOIN $t_ega as ega ON ega.goods_id = og.goods_id AND ega.warehouse_id IN (1)
    LEFT JOIN $t_o as o ON og.order_id = o.order_id
    WHERE og.order_id ".db_create_in($order_ids)." AND og.send_number <> og.goods_number GROUP BY og.order_id, og.goods_id) as gs
    WHERE gs.stock <= 0 GROUP BY gs.goods_id DESC";

    $res = $db->getAll($sql);

    $goods = array();
    foreach ($res as $key => $value) {
        $goods_ids[] = $value['goods_id'];
        $goods[$value['goods_id']] = $value;
    }

    if(empty($goods_ids)) return true;
    $sql2 = "SELECT GROUP_CONCAT(distinct(au.user_id) ORDER BY au.user_id DESC) as user_ids, GROUP_CONCAT(distinct(au.user_name) ORDER BY au.user_id DESC) as user_names, GROUP_CONCAT(distinct(esa.supplier_id)) as supplier_ids, egs.goods_id FROM $t_egs as egs
    LEFT JOIN ((SELECT admin_id, supplier_id FROM ".$t_esa.
      ") union all".
      "(SELECT admin_id, supplier_id FROM ".$ecs->table('erp_supplier_secondary_admin').
      ")) as esa ON egs.supplier_id = esa.supplier_id ".
    " LEFT JOIN $t_au as au ON esa.admin_id = au.user_id
    WHERE egs.goods_id". db_create_in($goods_ids) ." AND (au.user_id > 0 AND au.user_id != 1) GROUP BY egs.goods_id ORDER BY user_ids DESC";// au.user_id != 1, without admin

    $res = $db->getAll($sql2);
    foreach ($res as $key => $value) {
        $goods[$value['goods_id']] += $value;
    }
    $task_admin = [];
    foreach ($goods as $key => $value) {

        if(empty($value['user_ids'])) continue;
        
        $admins = explode(",",$value['user_names']);
        $admins_ids = explode(",",$value['user_ids']);
        foreach ($admins as $k => $admin) {
            $value['admins'][$admins_ids[$k]] = $admin;
        }
        
        $res[$key] = $value;
        // We only create task on first admin (Max(admin_id))
        $first_admin_id = current(array_keys($value['admins']));
        $first_admin    = $value['admins'][$first_admin_id];
        $task_admin[$first_admin_id]['admin_name'] = $first_admin;

        // Group by order sn
        $order_list = array();
        $ordersns = explode(",", $value['order_sns']);
        $orders   = explode(",", $value['order_ids']);

        // Add order, goods into admin
        foreach ($ordersns as $o_k => $order_sn) {
            // Find all action_user in order action_note. If the user has made a comment for the order, it won't create the task. 
            $sql = "SELECT distinct(LOWER(action_user)) FROM ".$t_oa. " WHERE order_id = '".$orders[$o_k] ."' AND action_note LIKE '[儲存備註]%' ";
            $t = $db->getCol($sql);
            $value['action_note'] = $t;
            if (in_array(strtolower($first_admin),$value['action_note'])) {
                continue;
            }
            $task_admin[$first_admin_id]['order'][$order_sn]['order_id'] = $orders[$o_k];
            $task_admin[$first_admin_id]['order'][$order_sn]['goods'][$value['goods_id']] = $value['goods_name'];
        }
    }

    $sender_id = 1;
    $sender_name = "admin";
    // Handle task info and insert it.
    foreach ($task_admin as $receiver_id => $receiver_admin) {

        ksort($receiver_admin['order']);
        $task = array(
            'sender_id' => $sender_id,
            'receiver_id' => $receiver_id,
            'sent_time' => $now,
            'read_time' => 0,
            'status' => '0',
            'priority' => "High",
            'dead_time' => $next_work_day,
            'estimated_time' => '480',
            'sender_name' => $sender_name,
            'pic' => $receiver_admin['admin_name'],
            'deleted' => 0,
            'title' => "需訂貨 但未填寫備註".local_date('Y-m-d')
        );
        $message = "以下訂單需訂貨 但未填寫備註: <br>";
        foreach ($receiver_admin['order'] as $order_sn => $order) {
            $goods_list = implode(", ", $order['goods']);
            $message .= '<a href="/x/order.php?act=info&order_id='.$order['order_id'].'">#'.$order_sn.'</a> 的 '.$goods_list.' <br>';
        }

        $task['message'] = mysql_escape_string($message);

        $db->autoExecute($ecs->table('admin_taskes'), $task, 'INSERT');
        $task_id = $db->insert_id();
    }

    return true;
}

// 預定訂貨日期3天前但未出貨提醒
function last_3_day_order($now, $next_work_day){

    global $db, $ecs, $_LANG, $userController;

    $date = strtotime("+3 day", gmtime());

    // Find have planned_delivery_date and goods_number != send_number goods
    $sql = "SELECT GROUP_CONCAT(distinct(oi.order_sn)) as order_sns, GROUP_CONCAT(distinct(og.order_id)) as order_ids, og.goods_id, og.goods_name, og.planned_delivery_date FROM ".$ecs->table('order_goods')." as og ".
    " LEFT JOIN ".$ecs->table('order_info')." as oi ON og.order_id = oi.order_id ".
    " WHERE og.planned_delivery_date > 0 AND og.planned_delivery_date < $date AND og.send_number <> og.goods_number ".
    " AND (oi.order_type <> '".Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE."' OR oi.order_type IS NULL) AND oi.user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).")".
    " AND oi.order_status IN ('1','5','6') ".
    " AND oi.shipping_status IN ('0','3','5','4') ".
    " AND ( oi.pay_status IN ('2','1') OR oi.pay_id IN ('6') ) ".
    " GROUP BY og.goods_id";

    $res = $db->getAll($sql);
    $goods = array();
    foreach ($res as $key => $value) {
        $goods_ids[] = $value['goods_id'];
        $goods[$value['goods_id']] = $value;
    }
    
    if(empty($goods_ids)) return true;
    $sql2 = "SELECT GROUP_CONCAT(distinct(au.user_id) ORDER BY au.user_id DESC) as user_ids, GROUP_CONCAT(distinct(au.user_name) ORDER BY au.user_id DESC) as user_names, GROUP_CONCAT(distinct(esa.supplier_id)) as supplier_ids, egs.goods_id FROM ".$ecs->table('erp_goods_supplier')." as egs
    LEFT JOIN ((SELECT admin_id, supplier_id FROM ".$ecs->table('erp_supplier_admin').
    ") union all ".
    "(SELECT admin_id, supplier_id FROM ".$ecs->table('erp_supplier_secondary_admin').
    ")) as esa ON egs.supplier_id = esa.supplier_id ".
    " LEFT JOIN ".$ecs->table('admin_user')." as au ON esa.admin_id = au.user_id ".
    " WHERE egs.goods_id". db_create_in($goods_ids) ." AND (au.user_id > 0 AND au.user_id != 1) GROUP BY egs.goods_id ORDER BY user_ids DESC";// au.user_id != 1, without admin

    $res = $db->getAll($sql2);
    foreach ($res as $key => $value) {
        $goods[$value['goods_id']] += $value;
    }

    $task_admin = [];
    foreach ($goods as $key => $value) {

        if(empty($value['user_ids'])) continue;
        // Find all action_user in order action_note.
        $admins = explode(",",$value['user_names']);
        $admins_ids = explode(",",$value['user_ids']);
        foreach ($admins as $k => $admin) {
            $value['admins'][$admins_ids[$k]] = $admin;
        }
        // We only create task on first admin (Max(admin_id))
        $first_admin_id = current(array_keys($value['admins']));
        $first_admin    = $value['admins'][$first_admin_id];
        $task_admin[$first_admin_id]['admin_name'] = $first_admin;

        // Group by order sn
        $order_list = array();
        $ordersns = explode(",", $value['order_sns']);
        $orders   = explode(",", $value['order_ids']);
        // Add order, goods into admin
        foreach ($ordersns as $o_k => $order_sn) {
            $task_admin[$first_admin_id]['order'][$order_sn]['order_id'] = $orders[$o_k];
            $task_admin[$first_admin_id]['order'][$order_sn]['goods'][$value['goods_id']] = $value['goods_name'];
        }
    }

    $sender_id = 1;
    $sender_name = "admin";
    // Handle task info and insert it.
    foreach ($task_admin as $receiver_id => $receiver_admin) {
        ksort($receiver_admin['order']);
        $task = array(
            'sender_id' => $sender_id,
            'receiver_id' => $receiver_id,
            'sent_time' => $now,
            'read_time' => 0,
            'status' => '0',
            'priority' => "High",
            'dead_time' => $next_work_day,
            'estimated_time' => '480',
            'sender_name' => $sender_name,
            'pic' => $receiver_admin['admin_name'],
            'deleted' => 0,
            'title' => "訂貨快到期但未處理訂單".local_date('Y-m-d')
        );
        $message = "以下訂單內有訂貨3天後到期/已到期: <br>";
        foreach ($receiver_admin['order'] as $order_sn => $order) {
            $goods_list = implode(", ", $order['goods']);
            $message .= '<a href="/x/order.php?act=info&order_id='.$order['order_id'].'">#'.$order_sn.'</a> 的 '.$goods_list.' <br>';
        }

        $task['message'] = mysql_escape_string($message);

        $db->autoExecute($ecs->table('admin_taskes'), $task, 'INSERT');
        $task_id = $db->insert_id();
    }

    return true;
}
