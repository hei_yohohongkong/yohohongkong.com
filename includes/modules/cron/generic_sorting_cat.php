<?php
if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}
$cron_lang = ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/cron/generic_sorting_cat.php';
if (file_exists($cron_lang))
{
    global $_LANG;

    include_once($cron_lang);
}
/* 模块的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
    $i = isset($modules) ? count($modules) : 0;

    /* 代码 */
    $modules[$i]['code']	= basename(__FILE__, '.php');

    /* 描述对应的语言项 */
    $modules[$i]['desc']	= 'category_desc';

    /* 作者 */
    $modules[$i]['author']  = 'zoe';

    /* 版本号 */
    $modules[$i]['version'] = '1.0.0';

    /* 配置信息 */
    $modules[$i]['config']  = array(
       
    );

    return;
}
error_reporting(E_ERROR );
ini_set('memory_limit', '1024M');
ini_set('max_execution_time', 1800);

$statisticsController = new Yoho\cms\Controller\StatisticsController();
$categoryController = new Yoho\cms\Controller\CategoryController();
$all_cat_arr = $statisticsController->getLastestBrandsCatsStats("all_cat");
$max_score = 1000;
$week_session_arr = array_column($all_cat_arr,"total_session_7");
array_multisort($week_session_arr, SORT_DESC, $all_cat_arr);
$ws_score_arr = [];
for ($i=0; $i<count($all_cat_arr); $i++) {
    $cur_cat_arr = $all_cat_arr[$i];
    //7 day session sorting
    if ($i>0 && $cur_cat_arr["total_session_7"]==$all_cat_arr[$i-1]["total_session_7"]) {
        $score = $ws_score_arr[$all_cat_arr[$i-1]["cat_id"]]["score"];
    } else {
        $score = $max_score - $i;
    }   
    $ws_score_arr[$cur_cat_arr["cat_id"]] = array("all_cat_name" => $cur_cat_arr["all_cat_name"], "score" => $score);
}

//30 day session sorting
$month_session_arr = array_column($all_cat_arr,"total_session_30");
array_multisort($month_session_arr, SORT_DESC, $all_cat_arr);
$ms_score_arr = [];
for ($i=0; $i<count($all_cat_arr); $i++) {
    $cur_cat_arr = $all_cat_arr[$i];
    if ($i>0 && $cur_cat_arr["total_session_30"]==$all_cat_arr[$i-1]["total_session_30"]) {
        $score = $ms_score_arr[$all_cat_arr[$i-1]["cat_id"]]["score"];
    } else {
        $score = $max_score - $i;
    }   
    $ms_score_arr[$cur_cat_arr["cat_id"]] = array("all_cat_name" => $cur_cat_arr["all_cat_name"], "score" => $score);
}

// 7 day conversion sorting
$week_conversion_arr = array_column($all_cat_arr,"perf_total_sales_7to7_percentage");
array_multisort($week_conversion_arr, SORT_DESC, $all_cat_arr);
$wc_score_arr = [];
for ($i=0; $i<count($all_cat_arr); $i++) {
    $cur_cat_arr = $all_cat_arr[$i];   
    if ($i>0 && $cur_cat_arr["perf_total_sales_7to7_percentage"]==$all_cat_arr[$i-1]["perf_total_sales_7to7_percentage"]) {
        $score = $wc_score_arr[$all_cat_arr[$i-1]["cat_id"]]["score"];
    } else {
        $score = $max_score - $i;
    }   
    $wc_score_arr[$cur_cat_arr["cat_id"]] = array("all_cat_name" => $cur_cat_arr["all_cat_name"], "score" => $score);
}
// 30 day conversion sorting
$month_conversion_arr = array_column($all_cat_arr,"perf_total_sales_30to30_percentage");
array_multisort($month_conversion_arr, SORT_DESC, $all_cat_arr);
$mc_score_arr = [];
for ($i=0; $i<count($all_cat_arr); $i++) {
    $cur_cat_arr = $all_cat_arr[$i];
    if ($i>0 && $cur_cat_arr["perf_total_sales_30to30_percentage"]==$all_cat_arr[$i-1]["perf_total_sales_30to30_percentage"]) {
        $score = $mc_score_arr[$all_cat_arr[$i-1]["cat_id"]]["score"];
    } else {
        $score = $max_score - $i;
    }   
    $mc_score_arr[$cur_cat_arr["cat_id"]] = array("all_cat_name" => $cur_cat_arr["all_cat_name"], "score" => $score);

    $final_score = $score * 0.6 + $ws_score_arr[$cur_cat_arr["cat_id"]]["score"] + $ms_score_arr[$cur_cat_arr["cat_id"]]["score"] * 0.8 + $wc_score_arr[$cur_cat_arr["cat_id"]]["score"] * 0.8;

    $cat_level = $categoryController->getCatLevel($cur_cat_arr['cat_id']);

    if ($cat_level == 3) {
        $sql = "UPDATE" . $ecs->table('category') .
           "SET sort_order = '" . $final_score . "'" .
           "WHERE cat_id = '" . $cur_cat_arr['cat_id'] . "'";
        $db->query($sql);
    }
}
?>