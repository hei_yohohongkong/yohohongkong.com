<?php
if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}
$cron_lang = ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/cron/offline_payment.php';
if (file_exists($cron_lang))
{
    global $_LANG;

    include_once($cron_lang);
}

/* 模块的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
    $i = isset($modules) ? count($modules) : 0;

    /* 代码 */
    $modules[$i]['code']    = basename(__FILE__, '.php');

    /* 描述对应的语言项 */
    $modules[$i]['desc']    = 'offline_payment_desc';

    /* 作者 */
    $modules[$i]['author']  = 'dem';

    /* 版本号 */
    $modules[$i]['version'] = '1.0.0';

    /* 配置信息 */
    $modules[$i]['config']  = array(
        array('name' => 'amount_protect_days', 'type' => 'select', 'value' => '10'),
        array('name' => 'order_add_days', 'type' => 'select', 'value' => '3'),
    );

    return;
}

require_once ROOT_PATH . 'includes/Google_api_client/vendor/autoload.php';
require_once ROOT_PATH . 'includes/lib_order.php';
require_once ROOT_PATH . 'x/includes/lib_accounting.php';

close_op();

$valid_time = local_strtotime("-$cron[amount_protect_days] days midnight");
$add_time = local_strtotime("-$cron[order_add_days] days midnight");

$crmController = new Yoho\cms\Controller\CrmController();
$gmail_user = $crmController::GMAIL_USER;
$client = new Google_Client();
$client->setApplicationName("Yoho Offline Payment API");
$client->setAuthConfig($crmController::KEY_FILE_LOCATION);
$client->setScopes(['https://mail.google.com/']);
$client->setSubject($gmail_user);
$client->useApplicationDefaultCredentials();
$client->fetchAccessTokenWithAssertion();
$client->getAccessToken();
$service = new Google_Service_Gmail($client);

$labels = get_all_labels();
$active_apm = [];
$live_config = [
    "fps轉賬"  => ['pay_ids' => [21], 'account_id' => 4],
    "hsbc轉帳" => ['pay_ids' => [5, 9], 'account_id' => 2],
    "hs轉賬"   => ['pay_ids' => [5, 9], 'account_id' => 4],
];
$beta_config = [
    "dem-test-fps"  => ['pay_ids' => [21], 'account_id' => 4],
    "dem-test-hsbc" => ['pay_ids' => [5, 9], 'account_id' => 2],
    "dem-test-hs"   => ['pay_ids' => [5, 9], 'account_id' => 4],
];
$labelConfig = strpos($_SERVER['SERVER_NAME'], 'beta.') !== false ? $beta_config : $live_config;
$query = [];
foreach (array_keys($labelConfig) as $name) {
    $query[] = "label:$name";
}

$params = [
    'maxResults' => strpos($_SERVER['SERVER_NAME'], 'beta.') !== false ? 5 : 50,
    'q' => implode(" OR ", $query) . " -label:starred after:" . local_date("Y-m-d", local_strtotime("-1 week midnight"))
];
// die($params['q']);
$results = $service->users_threads->listUsersThreads($gmail_user, $params);

$client->setUseBatch(true);
$batch = new Google_Http_Batch($client);
$batch2 = new Google_Http_Batch($client);
$batch3 = new Google_Http_Batch($client);

// Step 1: Get targetted email list
foreach ($results->getThreads() as $row) {
    $mail_id = $row->id;
    $email = $service->users_messages->get($gmail_user, $mail_id, ['format' => 'metadata', 'metadataHeaders' => ['From', 'Date', 'Subject']]);
    $batch->add($email, $mail_id);
}
foreach ($batch->execute() as $key => $res) {
    if (empty($res) || get_class($res) != 'Google_Service_Gmail_Message') continue;
    $mail = [];
    $headers = $res->getPayload()->getHeaders();
    $mail_id = str_replace('response-', '', $key);
    $mail['info'] = $crmController->getUsefulHeaders($headers);

    // Ignore non payment emails
    if (empty($mail['info']['Payment'])) {
        continue;
    }
    // Ignore unwanted emails
    if (strpos($mail['info']['Subject'], "Inward Payments Notification") === false && strpos($mail['info']['Subject'], "入賬通知") === false) {
        continue;
    }
    foreach ($res->labelIds as $label) {
        if (!in_array($label, ['IMPORTANT', 'CATEGORY_PERSONAL', 'INBOX'])) {
            $mail['labels'][] = strtolower($label);
        }
    }
    // Ignore starred email
    if (in_array("starred", $mail['labels'])) {
        continue;
    }
    $mail['mail_id'] = $mail_id;
    $mail['ticket_id'] = $crmController->createTicket($mail_id, $res->getThreadId(), CRM_PLATFORM_GMAIL, $mail['info']['From_mail'], $mail['info']['Format_Date']);
    $email = $service->users_messages->get($gmail_user, $mail_id);
    $batch2->add($email, $res->getThreadId());
    $mail_list[$res->getThreadId()] = $mail;
}

// Step 2: Loop all emails
// See supported payment origin: CrmController -> getUsefulHeaders()
$cfg_origin = empty($_CFG['offline_payment_origin']) ? "" : $_CFG['offline_payment_origin'];
if (empty($cfg_origin)) {
    die("Empty payment origin");
}
if (strpos($_SERVER['SERVER_NAME'], 'beta.') !== false && $cfg_origin != "TEST" && empty($_REQUEST['force'])) {
    die("Please confirm your payment origin: $cfg_origin");
}
$payment_origin = explode(",", $cfg_origin);
foreach ($batch2->execute() as $key => $res) {
    $thread_id = str_replace('response-', '', $key);
    $payload = $res->getPayload();
    if (in_array($mail_list[$thread_id]['info']['Payment'], $payment_origin)) {
        $html = base64_decode(strtr($payload->body->data, '-_', '+/'));
        if (strpos($_SERVER['SERVER_NAME'], 'beta.') !== false) {
            $html = base64_decode(strtr($payload->parts[0]->body->data, '-_', '+/'));
        } else {
            $html = base64_decode(strtr($payload->body->data, '-_', '+/'));
        }
        preg_match("/(HKD[0-9,]+\.[0-9]{2}){1}/", $html, $amount);
        preg_match("/\d\d\d\d-\d\d-\d\d\s\d\d\:\d\d/", $html, $date);
    }
    if (empty($amount)) {
        unset($mail_list[$thread_id]);
    } else {
        $mail_list[$thread_id]['info']['Amount'] = $amount[0];
        $mail_list[$thread_id]['info']['Txn_date'] = $date[0];
    }
}

// Step 3: Match email amount with not fully paid orders
foreach ($mail_list as $thread_id => $mail) {
    $amount = preg_replace("/(HKD|,)/", "", $mail['info']['Amount']);
    $date = local_strtotime($mail['info']['Txn_date']);
    $email_pay_ids = [];
    foreach ($labels as $name => $label_id) {
        if (!empty($pay_id) || empty($labelConfig[$name])) {
            continue;
        }
        if (in_array($label_id, $mail['labels'])) {
            $email_pay_ids = $labelConfig[$name]['pay_ids'];
            $email_account_id = $labelConfig[$name]['account_id'];
        }
    }

    if (empty($email_pay_ids) || empty($email_account_id)) {
        continue;
    }
    $sql = "SELECT order_id, add_time FROM " . $ecs->table("order_info") .
            " WHERE pay_id " . db_create_in($email_pay_ids) . " AND pay_status != 2 AND order_amount = $amount" .
            " AND order_status NOT " . db_create_in([OS_CANCELED, OS_INVALID, OS_WAITING_REFUND]) .
            " AND pay_status != " . PS_PAYED .
            " AND add_time > $valid_time";
    $orders = $db->getAll($sql);
    if (count($orders) == 1) {
        $order = $orders[0];
        if ($order['add_time'] >= $add_time && $order['add_time'] < $date) {
            $op = $db->getRow("SELECT * FROM " . $ecs->table('offline_payment') . " WHERE order_id = $order[order_id] AND (status = " . OFFLINE_PAYTMENT_UPLOADED . " OR (status = " . OFFLINE_PAYTMENT_PENDING . " AND pay_id = 21))");
            if (!empty($op)) {
                mark_order_as_paid($order, $mail, $amount, $thread_id, $op, $email_account_id);
            }
        }
    } elseif (count($orders) > 1) {
        $tmp_orders = [];
        foreach ($orders as $order) {
            $tmp_orders[$order['order_id']] = $order['add_time'];
        }
        $ops = $db->getAll("SELECT * FROM " . $ecs->table('offline_payment') . " WHERE order_id " . db_create_in(array_keys($tmp_orders)) . " AND (status = " . OFFLINE_PAYTMENT_UPLOADED . " OR (status = " . OFFLINE_PAYTMENT_PENDING . " AND pay_id = 21)) ORDER BY upload_time DESC");
        if (count($ops) == 1) {
            $op = $ops[0];
            $order = [
                'order_id' => $op['order_id'],
                'add_time' => $tmp_orders[$op['order_id']]
            ];
            mark_order_as_paid($order, $mail, $amount, $thread_id, $op, $email_account_id);
        }
    }
}
$batch3->execute();

function get_all_labels()
{
    global $service, $gmail_user;
    $labelsResponse = $service->users_labels->listUsersLabels($gmail_user);
    $labels = [];
    if ($labelsResponse->getLabels()) {
        $labels = array_merge($labels, $labelsResponse->getLabels());
    }
    $result = [];
    foreach ($labels as $label) {
        $name = preg_replace('/[\s\/\(\)]/', '-', $label->name);
        $result[strtolower($name)] = strtolower($label->id);
    }
    return $result;
}


function close_op()
{
    global $db, $ecs;
    $close_ids = $db->getCol("SELECT op_id FROM " . $ecs->table("offline_payment") . " op LEFT JOIN " . $ecs->table("order_info") . " oi ON oi.order_id = op.order_id WHERE (oi.order_status " . db_create_in([OS_CANCELED, OS_INVALID, OS_WAITING_REFUND]) . " OR oi.pay_status = " . PS_PAYED . ") AND op.status < " . OFFLINE_PAYTMENT_FINISHED);
    $db->query("UPDATE " . $ecs->table("offline_payment") . " SET status = " . OFFLINE_PAYTMENT_CLOSED . " WHERE op_id " . db_create_in($close_ids));
}

function mark_order_as_paid($order, $mail, $amount, $thread_id, $op, $email_account_id)
{
    global $db, $service, $gmail_user, $batch3, $crmController, $actg;
    require_once ROOT_PATH . 'x/includes/lib_accounting.php';

    $payment = payment_info($op['pay_id']);

    if (strpos($payment['pay_code'], 'bank') !== false) {
        $banks = $payment['pay_config_list']['banks'];
        $op_account_id = -1;
        foreach ($banks as $key => $bank) {
            if ($key == $op['pay_account']) {
                $op_account_id = $db->getOne("SELECT account_id FROM actg_accounts WHERE account_number = '$bank[account]'");
            }
        }
    }
    if (!isset($active_apm[$payment['pay_code']])) {
        $active_apm[$payment['pay_code']] = $actg->getAccountPaymentMethods(array('pay_code' => $payment['pay_code'], 'active' => 1));
    }
    $actg_payment_methods = $active_apm[$payment['pay_code']];
    if (count($actg_payment_methods) == 1) {
        $actg_payment_method = $actg_payment_methods[0]['payment_method_id'];
    } elseif (count($actg_payment_methods) > 1) {
        foreach ($actg_payment_methods as $method) {
            if ((intval($method['account_id']) === intval($email_account_id)) && (intval($method['account_id']) === intval($op_account_id))) {
                $actg_payment_method = $method['payment_method_id'];
            }
        }
    }
    if (!empty($actg_payment_method)) {
        if ($crmController->offlinePayment($mail['ticket_id'], $order['order_id'], $amount, $op['op_id'], false, $actg_payment_method)) {
            $mods = new Google_Service_Gmail_ModifyThreadRequest();
            $mods->setAddLabelIds(["STARRED"]);
            $modify = $service->users_threads->modify($gmail_user, $thread_id, $mods);
            $batch3->add($modify, $thread_id);
        }
    }
}