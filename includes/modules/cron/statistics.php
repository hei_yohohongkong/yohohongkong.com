<?php

if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}
// Max execution time = 30mins
ini_set('max_execution_time', 1800);

$cron_lang          = ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/cron/statistics.php';
if (file_exists($cron_lang))
{
	global $_LANG;

	include_once($cron_lang);
}

/* 模块的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
	$i = isset($modules) ? count($modules) : 0;

	/* 代码 */
	$modules[$i]['code']	= basename(__FILE__, '.php');

	/* 描述对应的语言项 */
	$modules[$i]['desc']	= 'statistics_desc';

	/* 作者 */
	$modules[$i]['author']  = 'Dem';

	/* 版本号 */
	$modules[$i]['version'] = '1.0.0';

	/* 配置信息 */
	$modules[$i]['config']  = array(

    );

	return;
}

require_once ROOT_PATH . 'includes/lib_order.php';

$yesterday      = local_strtotime("yesterday midnight");
$sevendays      = local_strtotime("-7days midnight");
$fourteendays   = local_strtotime("-14days midnight");
$thirtydays     = local_strtotime("-30days midnight");
$sixtydays      = local_strtotime("-60days midnight");
$batched = 0;

$googleController = new Yoho\cms\Controller\GoogleController();
$statisticsController = new Yoho\cms\Controller\StatisticsController();
$client = $googleController->getClient();
$service = new \Google_Service_Analytics($client);
$id = 'ga:77620237';

ini_set("memory_limit", "256M");

$metrics = "ga:users,ga:pageviews,ga:uniquePageviews";
$optParams = [
    'dimensions'    => 'ga:pagePath,ga:landingPagePath,ga:medium',
    'filters'       => 'ga:pagePath=~product/',
    'max-results'   => 10000,
];

// Get last 60 days data if not exist
$start_day = $sixtydays;
while ($start_day <= $yesterday) {
    $has_record = $db->getOne("SELECT 1 FROM " . $ecs->table('google_statistics') . " WHERE date = '" . local_date("Y-m-d H:i:s", $start_day) . "'");
    if (!$has_record) {
        if ($batched > 0) {
            sleep($batched);
        }
        $insert_data = [];
        $target_date = local_date("Y-m-d", $start_day);
        $optParams['start-index'] = 1;
        $params = [
            'start_day' => $target_date,
            'end_day'   => $target_date,
            'metrics'   => $metrics,
            'optParams' => $optParams
        ];
        $result = $statisticsController->batchGetGoogleAnalyticData($service, $client, $params);
        if ($result['success']) {
            $insert_data = $result['insert_data'];
            foreach ($insert_data as $goods_id => $row) {
                $db->query("INSERT INTO " . $ecs->table("google_statistics") . " (goods_id, session, pageview, pageview_unique, landing_session, landing_pageview, landing_pageview_unique, date) VALUES ('$goods_id', '$row[session]', '$row[pageview]', '$row[pageview_unique]', '$row[landing_session]', '$row[landing_pageview]', '$row[landing_pageview_unique]', '$target_date')");
            }
        }
    }
    $start_day += 86400;
}

// Get lifetime data
$last_lifetime_update = $db->getOne("SELECT MAX(last_update) FROM " . $ecs->table("google_statistics_lifetime"));
$end_date = local_date("Y-m-d", $yesterday);
if (empty($last_lifetime_update)) {
    $insert_data = [];
    $succeed = true;
    $optParams['start-index'] = 1;
    $params = [
        'start_day' => '2013-01-01',
        'end_day'   => $end_date,
        'metrics'   => $metrics,
        'optParams' => $optParams
    ];

    $result = $statisticsController->batchGetGoogleAnalyticData($service, $client, $params);
    if ($result['success']) {
        foreach ($result['insert_data'] as $goods_id => $row) {
            $db->query("INSERT INTO " . $ecs->table("google_statistics_lifetime") . " (goods_id, last_update, session, pageview, pageview_unique, landing_session, landing_pageview, landing_pageview_unique) VALUES ('$goods_id', '$end_date', '$row[session]', '$row[pageview]', '$row[pageview_unique], '$row[landing_session]', '$row[landing_pageview]', '$row[landing_pageview_unique]'')");
        }
    }
} else {
    if (local_strtotime($end_date) - local_strtotime($last_lifetime_update) == 86400) {
        foreach ($insert_data as $goods_id => $row) {
            $db->query("INSERT INTO " . $ecs->table("google_statistics_lifetime") . " (goods_id, session, pageview, pageview_unique, landing_session, landing_pageview, landing_pageview_unique, last_update) VALUES ('$goods_id', '$row[session]', '$row[pageview]', '$row[pageview_unique]', '$row[landing_session]', '$row[landing_pageview]', '$row[landing_pageview_unique]', '$end_date') ON DUPLICATE KEY UPDATE session = session + $row[session], pageview = pageview + $row[pageview], pageview_unique = pageview_unique + $row[pageview_unique], landing_session = landing_session + $row[landing_session], landing_pageview = landing_pageview + $row[landing_pageview], landing_pageview_unique = landing_pageview_unique + $row[landing_pageview_unique], last_update = '$end_date'");
        }
    } elseif (local_strtotime($end_date) - local_strtotime($last_lifetime_update) > 86400) {
        $insert_data = [];
        $succeed = true;
        $optParams['start-index'] = 1;
        $start_date = local_date("Y-m-d", local_strtotime($last_lifetime_update) + 86400);
        $params = [
            'start_day' => $start_date,
            'end_day'   => $end_date,
            'metrics'   => $metrics,
            'optParams' => $optParams
        ];

        $result = $statisticsController->batchGetGoogleAnalyticData($service, $client, $params);
        if ($result['success']) {
            foreach ($result['insert_data'] as $goods_id => $row) {
                $db->query("INSERT INTO " . $ecs->table("google_statistics_lifetime") . " (goods_id, session, pageview, pageview_unique, landing_session, landing_pageview, landing_pageview_unique, last_update) VALUES ('$goods_id', '$row[session]', '$row[pageview]', '$row[pageview_unique]', '$row[landing_session]', '$row[landing_pageview]', '$row[landing_pageview_unique]', '$end_date') ON DUPLICATE KEY UPDATE session = session + $row[session], pageview = pageview + $row[pageview], pageview_unique = pageview_unique + $row[pageview_unique], landing_session = landing_session + $row[landing_session], landing_pageview = landing_pageview + $row[landing_pageview], landing_pageview_unique = landing_pageview_unique + $row[landing_pageview_unique], last_update = '$end_date'");
            }
        }
    }
}

if (!empty($insert_data) || empty($db->getOne("SELECT COUNT(*) FROM ". $ecs->table("google_statistics_temporary_view")))) {
    $db->query("START TRANSACTION");
    $db->query("TRUNCATE " . $ecs->table("google_statistics_temporary_view"));
    $db->query($statisticsController->getCronSql());
    $base_cats = $db->getCol("SELECT cat_id FROM " . $ecs->table("category") . " WHERE parent_id = 0");
    foreach ($base_cats as $cat_id) {
        $db->query("UPDATE " . $ecs->table("google_statistics_temporary_view") . " SET parent_cat_id = $cat_id WHERE cat_id " . db_create_in(array_unique(array_merge(array($cat_id), array_keys(cat_list($cat_id, 0, false))))));
    }
    $db->query("COMMIT");

    $start = 0;
    $size = 5000;
    $db->query("START TRANSACTION");
    $sql = "SELECT goods_id, SUM(goods_number) as sales FROM (" .
                "SELECT order_id, goods_id, goods_number FROM " . $ecs->table("order_goods") . ") og " .
                "LEFT JOIN (SELECT order_id, order_status, shipping_status, pay_status FROM " . $ecs->table("order_info") . ") oi ON oi.order_id = og.order_id " .
                "WHERE 1" . order_query_sql('finished', 'oi.') .
                "GROUP BY goods_id ORDER BY goods_id ASC";
    $sales = $db->getAll("$sql LIMIT $start, $size");
    while (count($sales) > 0) {
        foreach ($sales as $row) {
            $db->query("UPDATE " . $ecs->table("google_statistics_lifetime") . " SET sales = '$row[sales]' WHERE goods_id = $row[goods_id]");
        }
        $start += $size;
        $sales = $db->getAll("$sql LIMIT $start, $size");
        sleep(5);
    }
    $db->query("COMMIT");
}

function processRows($rows)
{
    global $insert_data;
    foreach ($rows as $row) {
        $organic = false;
        $row_count = count($row);
        if (preg_match('/\/product\/(\d+)/i', $row[0], $match)) {
            $goods_id = intval($match[1]);
            if (preg_match('/\/product\/(\d+)/i', $row[1], $match)) {
                $landing_goods_id = intval($match[1]);
                if ($row[2] == 'organic' && $landing_goods_id == $goods_id) {
                    $organic = true;
                }
            }
            if (!empty($goods_id)) {
                if (!array_key_exists($goods_id, $insert_data)) {
                    $insert_data[$goods_id] = [
                        'session'           => intval($row[$row_count - 3]),
                        'pageview'          => intval($row[$row_count - 2]),
                        'pageview_unique'   => intval($row[$row_count - 1]),
                        'landing_session'           => $organic ? intval($row[$row_count - 3]) : 0,
                        'landing_pageview'          => $organic ? intval($row[$row_count - 2]) : 0,
                        'landing_pageview_unique'   => $organic ? intval($row[$row_count - 1]) : 0,
                    ];
                } else {
                    $insert_data[$goods_id]['session']          += intval($row[$row_count - 3]);
                    $insert_data[$goods_id]['pageview']         += intval($row[$row_count - 2]);
                    $insert_data[$goods_id]['pageview_unique']  += intval($row[$row_count - 1]);
                    if ($organic) {
                        $insert_data[$goods_id]['landing_session']          += intval($row[$row_count - 3]);
                        $insert_data[$goods_id]['landing_pageview']         += intval($row[$row_count - 2]);
                        $insert_data[$goods_id]['landing_pageview_unique']  += intval($row[$row_count - 1]);
                    }
                }
            }
        }
    }
}

function batchGetGAData($start_day, $end_day, $metrics, $optParams, $total)
{
    global $service, $client, $id;
    $batched = 0;
    $success = true;
    $client->setUseBatch(true);
    while ($optParams['start-index'] + $optParams['max-results'] < $total) {
        if ($batched > 0) {
            sleep($batched);
        }
        $batch = new Google_Http_Batch($client);
        $batched = 0;
        while ($optParams['start-index'] + $optParams['max-results'] < $total && $batched < 10) {
            $optParams['start-index'] += $optParams['max-results'];
            $req = $service->data_ga->get($id, $start_day, $end_day, $metrics, $optParams);
            $batch->add($req, "result_" . $optParams['start-index']);
            $batched++;
        }
        foreach ($batch->execute() as $batch_res) {
            if (get_class($batch_res) !== "Google_Service_Analytics_GaData") {
                $success = false;
                break 2;
            }
            processRows($batch_res->getRows());
        }
    }
    $client->setUseBatch(false);
    return $success;
}