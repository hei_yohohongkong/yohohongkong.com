<?php

if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}
$cron_lang = ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/cron/expired_po.php';
if (file_exists($cron_lang))
{
	global $_LANG;

	include_once($cron_lang);
}

/* 模块的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
	$i = isset($modules) ? count($modules) : 0;

	/* 代码 */
	$modules[$i]['code']	= basename(__FILE__, '.php');

	/* 描述对应的语言项 */
	$modules[$i]['desc']	= 'expired_po_desc';

	/* 作者 */
	$modules[$i]['author']  = 'dem';

	/* 版本号 */
	$modules[$i]['version'] = '1.0.0';

	/* 配置信息 */
	$modules[$i]['config']  = array(
        array('name' => 'expired_po_month', 'type' => 'select', 'value' => '6'),
    );

	return;
}

empty($cron['expired_po_month']) && $cron['expired_po_month'] = 6;

// 設定刪除的期限
$time = strtotime(gmdate("M d Y H:i:s",strtotime('-' . $cron['expired_po_month'] . ' month')));

// 刪除逾時的採購訂單
$db->query('START TRANSACTION');
$sql = "SELECT order_id, order_sn FROM " . $ecs->table('erp_order') . " WHERE order_status = 1 AND create_time < " . $time;
$orders = $db->getAll($sql);
$sql = "DELETE FROM " . $ecs->table('erp_order') . " WHERE order_status = 1 AND create_time < " . $time;
if(!$db->query($sql)){
	$db->query('ROLLBACK');
}else{
	foreach($orders as $order){
		$action_remark = "移除逾時訂單 " . $order['order_sn'];
		$sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
				"VALUES(" . $order['order_id'] . ", " . ERP_CRON_REMOVE . ",1,'" . $action_remark . "')";
		$GLOBALS['db']->query($sql);
	}
}
$db->query('COMMIT');

// 刪除逾時的入貨訂單
$db->query('START TRANSACTION');
$sql = "DELETE FROM " . $ecs->table('erp_warehousing') . " WHERE status = 1 AND create_time < " . $time;
if(!$db->query($sql)){
	$db->query('ROLLBACK');
}
$db->query('COMMIT');

// 刪除逾時的出貨訂單
$db->query('START TRANSACTION');
$sql = "DELETE FROM " . $ecs->table('erp_delivery') . " WHERE status = 1 AND create_time < " . $time;
if(!$db->query($sql)){
	$db->query('ROLLBACK');
}
$db->query('COMMIT');
?>