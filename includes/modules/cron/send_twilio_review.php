<?php

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}
$cron_lang = ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/cron/send_twilio_review.php';
if (file_exists($cron_lang))
{
    global $_LANG;

    include_once($cron_lang);
}

/* 模块的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
	$i = isset($modules) ? count($modules) : 0;

	/* 代码 */
	$modules[$i]['code']	= basename(__FILE__, '.php');

	/* 描述对应的语言项 */
	$modules[$i]['desc']	= 'send_twilio_review_desc';

	/* 作者 */
	$modules[$i]['author']  = 'dem';

	/* 网址 */
	$modules[$i]['website'] = '';

	/* 版本号 */
	$modules[$i]['version'] = '1.0.0';

	/* 配置信息 */
	$modules[$i]['config']  = array(array('name' => 'auto_send_day', 'type' => 'text', 'value' => '2'));

	return;
}

/*------------------------------------------------------ */
//-- Send E-mail
/*------------------------------------------------------ */
$days = isset($cron['auto_send_day']) ? $cron['auto_send_day'] : 2;

$time = local_strtotime('-'.$days.' days');
$start_time = local_date("Y-m-d", $time);
$end_time = local_date("Y-m-d", $time + 86400);

$sql = "SELECT ctc.*, u.email, u.user_id, u.user_name, cl.create_at FROM " . $ecs->table('crm_twilio_call') . " ctc " .
        "LEFT JOIN " . $ecs->table('crm_list') . " cl ON cl.list_id = ctc.list_id " .
        "LEFT JOIN " . $ecs->table('crm_ticket_info') . " cti ON cti.ticket_id = cl.ticket_id " .
        "LEFT JOIN " . $ecs->table('users') . " u ON u.user_id = cti.user_id " .
        "WHERE cti.user_id != 0 AND ctc.sent_evaluate = 0 " .
        "AND ctc.duration > 0 " .
        "AND u.email NOT REGEXP '^[\\\+0-9-]+\\\@yohohongkong.com$'" .
        "AND cl.create_at BETWEEN '$start_time' AND '$end_time' ";

$records = $db->getAll($sql);

$data = [];
foreach ($records as $rec) {
    if (empty($rec['admin_id'])) {
        continue;
    }
    $data_key = "$rec[user_id]-$rec[admin_id]";
    if (empty($data[$data_key])) {
        $data[$data_key] = [
            'email'     => $rec['email'],
            'user_name' => $rec['user_name'],
            'detail'    => [],
        ];
    }
    $data[$data_key]['detail'][$rec['list_id']] = [
        'duration'  => intval($rec['duration']),
        'create_at' => $rec['create_at'],
    ];
}
foreach ($data as $data_key => $row) {
    if (can_send_mail($row['email'], true) && is_email($row['email'])) {
        if (empty($smarty)) {
            /* 创建 Smarty 对象。*/
            include_once ROOT_PATH . 'core/cls_template.php';
            $smarty = new cls_template;
        }

        $user_id = explode("-", $data_key)[0];
        $db->query("INSERT INTO " . $ecs->table("review_crm") . " (user_id) VALUES ($user_id)");
        $review_id = $db->insert_id();
        $db->query("UPDATE " . $ecs->table("crm_twilio_call") . " SET sent_evaluate = $review_id WHERE list_id " . db_create_in(array_keys($row['detail'])));
        $call_list = [];
        foreach ($row['detail'] as $call) {
            $date = local_date("Y-m-d", local_strtotime($call['create_at']));
            if (empty($call_list[$date])) {
                $call_list[$date] = [
                    'duration'  => 0,
                    'time'      => 0,
                ];
            }
            $call_list[$date]['duration'] += $call['duration'];
            $call_list[$date]['time']++;
        }
        foreach ($call_list as $date => $summary) {
            $time = [];
            if ($summary['duration'] >= 3600) {
                $time[] = floor($summary['duration'] / 3600) . "小時";
                $summary['duration'] -= floor($summary['duration'] / 3600) * 3600;
            }
            if ($summary['duration'] >= 60) {
                $time[] = floor($summary['duration'] / 60) . "分鐘";
                $summary['duration'] -= floor($summary['duration'] / 60) * 60;
            }
            if ($summary['duration'] > 0) {
                $time[] = $summary['duration'] . "秒";
            }
            $call_list[$date]['formatted_duration'] = implode(" ", $time);
        }

        $tpl = get_mail_template('review_cs_twilio');
        $smarty->assign('call_list', $call_list);
        $smarty->assign('shop_url', $ecs->url());
        $smarty->assign('review_link', $review_id);
        $smarty->assign('shop_name', $GLOBALS['_CFG']['shop_name']);
        $smarty->assign('send_date', local_date($GLOBALS['_CFG']['date_format']));
        $smarty->assign('sent_date', local_date($GLOBALS['_CFG']['date_format']));
        $content = $smarty->fetch('str:' . $tpl['template_content']);

        send_mail($row['user_name'], $row['email'], $tpl['template_subject'], $content, $tpl['is_html']);
    }
}

?>