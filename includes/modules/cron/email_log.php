<?php

if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}
$cron_lang = ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/cron/email_log.php';
if (file_exists($cron_lang))
{
	global $_LANG;

	include_once($cron_lang);
}

/* 模块的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
	$i = isset($modules) ? count($modules) : 0;

	/* 代码 */
	$modules[$i]['code']	= basename(__FILE__, '.php');

	/* 描述对应的语言项 */
	$modules[$i]['desc']	= 'email_log_desc';

	/* 作者 */
	$modules[$i]['author']  = 'dem';

	/* 版本号 */
	$modules[$i]['version'] = '1.0.0';

	/* 配置信息 */
	$modules[$i]['config']  = array(
        array('name' => 'email_log_year', 'type' => 'select', 'value' => '1'),
    );

	return;
}

empty($cron['email_log_year']) && $cron['email_log_year'] = 1;

// 設定刪除的期限
$time = strtotime(gmdate("M d Y H:i:s",strtotime('-' . $cron['email_log_year'] . ' year')));

// 刪除逾時的電郵副本
$db->query('START TRANSACTION');
$sql = "DELETE FROM " . $ecs->table('sent_mails') . " WHERE sent_time < " . $time;
if(!$db->query($sql)){
	$db->query('ROLLBACK');
}
$db->query('COMMIT');
?>