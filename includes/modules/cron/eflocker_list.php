<?php

if (!defined('IN_ECS')) {
    die('Hacking attempt');
}

$cron_lang = ROOT_PATH . 'languages/' . $GLOBALS['_CFG']['lang'] . '/cron/eflocker_list.php';
if (file_exists($cron_lang)) {
    global $_LANG;

    include_once $cron_lang;
}

/* 模块的基本信息 */
if (isset($set_modules) && $set_modules == true) {
    $i = isset($modules) ? count($modules) : 0;

    /* 代码 */
    $modules[$i]['code'] = basename(__FILE__, '.php');

    /* 描述对应的语言项 */
    $modules[$i]['desc'] = 'eflocker_list_desc';

    /* 作者 */
    $modules[$i]['author'] = 'Anthony';

    /* 网址 */
    $modules[$i]['website'] = '';

    /* 版本号 */
    $modules[$i]['version'] = '1.0.0';

    /* 配置信息 */
    $modules[$i]['config'] = array(
        array('name' => 'ef_locker_url', 'type' => 'text', 'value' => 'http://openapi.eflocker.com/hibox/expressCompany/queryEdmEdinfo'),
        array('name' => 'appId', 'type' => 'text', 'value' => 'yohohongkong'),
        array('name' => 'transId', 'type' => 'text', 'value' => 'IPL281316'),
    );
    return;
}


$memcache = new \Memcached();
$memcache->addServer('localhost', 11211);
//zh_tw

$today = local_strtotime('today');
$memcache->set('ef_list_' . $today . '_zh_tw', get_ef_list(1));
$today = local_strtotime('today');
$memcache->set('ef_list_' . $today . '_en_us',get_ef_list(0));

function get_ef_list($lang_num = 1)
{
    
    $url     = empty($cron['ef_locker_url']) ? 'http://openapi.eflocker.com/hibox/expressCompany/queryEdmEdinfo' : $cron['ef_locker_url'];
    $appId   = empty($cron['appId']) ? 'yohohongkong' : $cron['ef_locker_url'];
    $transId = empty($cron['transId']) ? 'IPL281316' : $cron['ef_locker_url'];
    $content = json_encode(["appId"=>$appId,"transId"=>$transId, "language"=>$lang_num]);
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt(
        $curl,
        CURLOPT_HTTPHEADER,
        array("Content-type: application/json")
    );
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

    $json_response = curl_exec($curl);

    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    if ($status != 200) {
        // TODO: Error handle.
        // die("Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
    }
    curl_close($curl);

    $response = json_decode($json_response, true);
    $list = $response['data'];
    $ef_list = [];
    foreach ($list as $key => $ef) {
        if(empty($ef['city']) || strpos($ef['city'], '澳門') !== false || strpos($ef['city'], 'macau') !== false ) continue;
        $path = '/yohohk/img/';
        $ef['shop_icon']    = $path.'icon_EF.png';
        $ef_list[$ef['edCode']] = $ef;
    }
    // echo 'ef_list_'.$json_response;
    return $ef_list;
}

?>