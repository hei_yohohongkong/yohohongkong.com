<?php

/***
* includes/modules/cron/sitemap.php
* by howang 2014-06-23
*
* ECSHOP 定期產生網站地圖
***/

if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}
$cron_lang = ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/cron/sitemap.php';
if (file_exists($cron_lang))
{
	global $_LANG;

	include_once($cron_lang);
}

/* 模块的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
	$i = isset($modules) ? count($modules) : 0;

	/* 代码 */
	$modules[$i]['code']	= basename(__FILE__, '.php');

	/* 描述对应的语言项 */
	$modules[$i]['desc']	= 'sitemap_desc';

	/* 作者 */
	$modules[$i]['author']  = 'howang';

	/* 网址 */
	$modules[$i]['website'] = 'http://www.howang.hk';

	/* 版本号 */
	$modules[$i]['version'] = '1.0.0';

	/* 配置信息 */
	$modules[$i]['config']  = array();

	return;
}
//error_reporting(E_ERROR );
/*------------------------------------------------------ */
//-- 生成站点地图
/*------------------------------------------------------ */
include_once(ROOT_PATH . ADMIN_PATH . '/includes/cls_phpzip.php');
include_once(ROOT_PATH . ADMIN_PATH . '/includes/cls_google_sitemap.php');
$feedController = new Yoho\cms\Controller\FeedController();

$feedController->init_permanent_link_table();

//if ($GLOBALS['_CFG']['perma_link_cache'] || !empty($_REQUEST['test_perma_link'])) $feedController->init_all_permanent_link(1);
$feedController->addAllProduct();

$domain = $ecs->url();
$domain = str_replace('https://', "http://", $domain);
$domain = substr($domain, 0, -1); // howang: I modified build_uri() to include /, so we need to remove it from the url here
$today  = local_date('Y-m-d');
$config = unserialize($_CFG['sitemap']);
$config['limit'] = 10000;

$sm  = new google_sitemap();
$smi = new google_sitemap_item($domain, $today, $config['homepage_changefreq'], $config['homepage_priority']);

$sm->add_item($smi);

/* 商品分类 */
$sql = "SELECT cat_id,cat_name,cperma FROM " .$ecs->table('category').  
 "c LEFT JOIN (SELECT id, table_name, lang, perma_link as cperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") bpl ON bpl.id = c.cat_id  AND bpl.table_name = 'category' AND bpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
 " ORDER BY parent_id";
$res = $db->getAll($sql);
$data_res['category'] = $res;

foreach ($res as $row)
{
	// $smi = new google_sitemap_item($domain . build_uri('category', array('cid' => $row['cat_id'], 'cname' => $row['cat_name'], 'cperma' => $row['cperma']), $row['cat_name']), $today,
	// 	$config['category_changefreq'], $config['category_priority']);
	// $sm->add_item(google_sitemap::SITEMAP_SECTION_CAT,$smi);
}

/* 商品品牌 */
$sql = "SELECT brand_id,brand_name, bperma FROM " .$ecs->table('brand'). 
"b LEFT JOIN (SELECT id, table_name, lang, perma_link as bperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") bpl ON bpl.id = b.brand_id  AND bpl.table_name = 'brand' AND bpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' ";
$res = $db->getAll($sql);
$data_res['brand'] = $res;
foreach ($res as $row)
{
	// $smi = new google_sitemap_item($domain . build_uri('brand', array('bid' => $row['brand_id'], 'bname' => $row['brand_name'], 'bperma' => $row['bperma']), $row['brand_name']), $today,
	// 	$config['category_changefreq'], $config['category_priority']);
	// $sm->add_item(google_sitemap::SITEMAP_SECTION_BRAND,$smi);
}

/* 文章分类 */
$sql = "SELECT cat_id,cat_name, acperma FROM " .$ecs->table('article_cat'). 
"ac LEFT JOIN (SELECT id, table_name, lang, perma_link as acperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") bpl ON bpl.id = ac.cat_id  AND bpl.table_name = 'article_cat' AND bpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
" WHERE cat_type=1";
$res = $db->getAll($sql);
$data_res['article_category'] = $res;

foreach ($res as $row)
{
	// $smi = new google_sitemap_item($domain . build_uri('article_cat', array('acid' => $row['cat_id'], 'acname' => $row['cat_name'], 'acperma' => $row['acperma']), $row['cat_name']), $today,
	// 	$config['category_changefreq'], $config['category_priority']);
	// $sm->add_item(google_sitemap::SITEMAP_SECTION_ARTICLECAT,$smi);
}

/* 商品 */
// 2018-07-01, we only show the product before the date create. The product after the date creates, put it in sitemap with https.
$https_start_time = local_strtotime('2018-07-01');
$sql = "SELECT goods_id, goods_name, gperma FROM " .$ecs->table('goods'). 
"g LEFT JOIN (SELECT id, table_name, lang, perma_link as gperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") bpl ON bpl.id = g.goods_id  AND bpl.table_name = 'goods' AND bpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
" WHERE is_delete = 0 AND is_hidden = 0 and add_time <'".$https_start_time."' ";
$res = $db->query($sql);

while ($row = $db->fetchRow($res))
{
	$smi = new google_sitemap_item($domain . build_uri('goods', array('gid' => $row['goods_id'], 'gname' => $row['goods_name'], 'gperma' => $row['gperma']), $row['goods_name']), $today,
		$config['content_changefreq'], $config['content_priority']);
	$sm->add_item(google_sitemap::SITEMAP_SECTION_PRODUCT,$smi);
}

/* 文章 */
$sql = "SELECT article_id,title,file_url,open_type,aperma FROM " .$ecs->table('article'). 
"a LEFT JOIN (SELECT id, table_name, lang, perma_link as aperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") bpl ON bpl.id = a.article_id  AND bpl.table_name = 'article' AND bpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
" WHERE is_open=1";
$res = $db->getAll($sql);
$data_res['article'] = $res;

foreach ($res as $row)
{
	// $article_url=$row['open_type'] != 1 ? build_uri('article', array('aid'=>$row['article_id'], 'aname' => $row['title'], 'aperma' => $row['aperma']), $row['title']) : trim($row['file_url']);
	// $smi = new google_sitemap_item($domain . $article_url,
	// 	$today, $config['content_changefreq'], $config['content_priority']);
	// $sm->add_item(google_sitemap::SITEMAP_SECTION_ARTICLE,$smi);
}

clear_cache_files();	// 清除缓存

$sm_file = ROOT_PATH . 'sitemap.xml';
try{
	$feedController->buildFeed();
    $res=$sm->build($sm_file,$ecs->url());
    if($res!==true){
    	$sm_file = ROOT_PATH . DATA_DIR . '/sitemap.xml';
    	$res=$sm->build($sm_file,$ecs->url());
    	if($res!==true){
    		error_log('Sitemap generation error:'.$res);
    	}else{
    		$sitemap_url = $ecs->url() . 'sitemap.xml';
    		error_log('Sitemap generated: ' . $sitemap_url);
    	}
    }else{
    	$sitemap_url = $ecs->url() . 'sitemap.xml';
    	error_log('Sitemap generated: ' . $sitemap_url);
    }

}catch(Exception $e){
	error_log('Sitemap generation error:'.$e->getMessage());
}

// to generate sitemap with https start  added by eric
$domain = $ecs->url();
$domain = str_replace('http://', "https://", $domain);
$domain = substr($domain, 0, -1); // remove '/' from the url tail
$today  = local_date('Y-m-d');
$config = unserialize($_CFG['sitemap']);
$config['limit'] = 1000;

$sm  = new google_sitemap();
$smi = new google_sitemap_item($domain, $today, $config['homepage_changefreq'], $config['homepage_priority']);

/* 商品 */
// 2018-07-01, we only show the product before the date create. The product after the date creates, put it in sitemap with https.
$https_start_time = local_strtotime('2018-07-01');
$sql = "SELECT goods_id, goods_name, gperma FROM " .$ecs->table('goods'). 
"g LEFT JOIN (SELECT id, table_name, lang, perma_link as gperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") bpl ON bpl.id = g.goods_id  AND bpl.table_name = 'goods' AND bpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
 " WHERE is_delete = 0 AND is_hidden = 0 and add_time >='".$https_start_time."' ";
$res = $db->query($sql);

while ($row = $db->fetchRow($res))
{
	$smi = new google_sitemap_item($domain . build_uri('goods', array('gid' => $row['goods_id'], 'gname' => $row['goods_name'], 'gperma' => $row['gperma']), $row['goods_name']), $today,
		$config['content_changefreq'], $config['content_priority']);
	$sm->add_item(google_sitemap::SITEMAP_SECTION_PRODUCT,$smi);
}

/* Promotions */
$sql = "SELECT t.topic_id, IFNULL(tl.title, t.title) as title, tperma " .
	   "FROM " . $ecs->table('topic') . " as t " .
	   "LEFT JOIN (SELECT id, table_name, lang, perma_link as tperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") bpl ON bpl.id = t.topic_id  AND bpl.table_name = 'topic' AND bpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
	   "LEFT JOIN " . $ecs->table('topic_lang') . " as tl ON tl.topic_id = t.topic_id AND tl.lang = '" . $_CFG['lang'] . "' " .
	   "ORDER BY t.sort_order ASC, t.topic_id DESC";
$res = $db->getAll($sql);
$data_res['topic'] = $res;

foreach ($res as $row)
{
	$topic_url=build_uri('topic', array('tid' => $row['topic_id'], 'tname' => $row['title'], 'tperma' => $row['tperma']), $row['title']);
	$smi = new google_sitemap_item($domain . $topic_url,
		$today, $config['content_changefreq'], $config['content_priority']);
	$sm->add_item(google_sitemap::SITEMAP_SECTION_TOPIC,$smi);
}

/* tags */
$sql = "SELECT distinct (tag_words) " .
	   "FROM " . $ecs->table('tag') . " ";
$res = $db->getAll($sql);
$data_res['tag'] = $res;

foreach ($res as $row)
{
	$topic_url= "/keyword/".urlencode($row['tag_words']);
	$smi = new google_sitemap_item($domain . $topic_url,
		$today, $config['content_changefreq'], $config['content_priority']);
	$sm->add_item(google_sitemap::SITEMAP_SECTION_TAG,$smi);
}

/* 文章 */
foreach ($data_res['article'] as $key => $row)
{
	$article_url=$row['open_type'] != 1 ? build_uri('article', array('aid'=>$row['article_id'], 'aname' => $row['title'],'aperma' => $row['aperma']), $row['title']) : trim($row['file_url']);
	$smi = new google_sitemap_item($domain . $article_url,
		$today, $config['content_changefreq'], $config['content_priority']);
	$sm->add_item(google_sitemap::SITEMAP_SECTION_ARTICLE,$smi);
}

/* 文章分类 */
foreach ($data_res['article_category'] as $key => $row)
{
	$smi = new google_sitemap_item($domain . build_uri('article_cat', array('acid' => $row['cat_id'], 'acname' => $row['cat_name'],'acperma' => $row['acperma']), $row['cat_name']), $today,
		$config['category_changefreq'], $config['category_priority']);
	$sm->add_item(google_sitemap::SITEMAP_SECTION_ARTICLECAT,$smi);
}

/* 商品品牌 */
foreach ($data_res['brand'] as $key => $row)
{
	$smi = new google_sitemap_item($domain . build_uri('brand', array('bid' => $row['brand_id'], 'bname' => $row['brand_name'],'bperma' => $row['bperma']), $row['brand_name']), $today,
		$config['category_changefreq'], $config['category_priority']);
	$sm->add_item(google_sitemap::SITEMAP_SECTION_BRAND,$smi);
}

foreach ($data_res['category'] as $key => $row)
{
	$smi = new google_sitemap_item($domain . build_uri('category', array('cid' => $row['cat_id'], 'cname' => $row['cat_name'],'cperma' => $row['cperma']), $row['cat_name']), $today,
		$config['category_changefreq'], $config['category_priority']);
	$sm->add_item(google_sitemap::SITEMAP_SECTION_CAT,$smi);
}

$smi = new google_sitemap_item($domain . '/new', $today,
		$config['category_changefreq'], $config['category_priority']);
$sm->add_item(google_sitemap::SITEMAP_SECTION_OTHERS,$smi);

$smi = new google_sitemap_item($domain . '/flashdeal', $today,
		$config['category_changefreq'], $config['category_priority']);
$sm->add_item(google_sitemap::SITEMAP_SECTION_OTHERS,$smi);

$smi = new google_sitemap_item($domain . '/sale', $today,
		$config['category_changefreq'], $config['category_priority']);
$sm->add_item(google_sitemap::SITEMAP_SECTION_OTHERS,$smi);

$smi = new google_sitemap_item($domain . '/support', $today,
		$config['category_changefreq'], $config['category_priority']);
$sm->add_item(google_sitemap::SITEMAP_SECTION_OTHERS,$smi);


clear_cache_files();	// 清除缓存

$sm_file = ROOT_PATH . 'sitemap_https.xml';
try{
    $res=$sm->build($sm_file,$ecs->url());
    if($res!==true){
    	$sm_file = ROOT_PATH . DATA_DIR . '/sitemap_https.xml';
    	$res=$sm->build($sm_file,$ecs->url());
    	if($res!==true){
    		error_log('Sitemap generation error:'.$res);
    	}else{
    		$sitemap_url = $ecs->url() . 'sitemap_https.xml';
    		error_log('Sitemap generated: ' . $sitemap_url);
    	}
    }else{
    	$sitemap_url = $ecs->url() . 'sitemap_https.xml';
    	error_log('Sitemap generated: ' . $sitemap_url);
    }

}catch(Exception $e){
	error_log('Sitemap generation error:'.$e->getMessage());
}
// to generate sitemap with https end


// to generate sitemap with https start (baidu)  added by eric
$domain = $ecs->url();
$domain = str_replace('http://', "https://", $domain);
$domain = substr($domain, 0, -1); // remove '/' from the url tail
$today  = local_date('Y-m-d');
$config = unserialize($_CFG['sitemap']);
$config['limit'] = 10000;

$sm  = new google_sitemap();
$smi = new google_sitemap_item($domain, $today, $config['homepage_changefreq'], $config['homepage_priority']);

/* 商品 */
$sql = "SELECT goods_id, goods_name , gperma FROM " .$ecs->table('goods').
"g LEFT JOIN (SELECT id, table_name, lang, perma_link as gperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") bpl ON bpl.id = g.goods_id  AND bpl.table_name = 'goods' AND bpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
" WHERE is_delete = 0 AND is_hidden = 0 ";
$res = $db->getAll($sql);

$index = 0;
$limit = $config['limit'];
foreach ($res as $key => $row)
{
	$smi = new google_sitemap_item($domain . build_uri('goods', array('gid' => $row['goods_id'], 'gname' => $row['goods_name'], 'gperma' => $row['gperma']), $row['goods_name']), $today,
		$config['content_changefreq'], $config['content_priority']);
	$sm->add_item(google_sitemap::SITEMAP_SECTION_PRODUCT.'_'.$index,$smi);
	
	if ($key > $limit) {
		$limit += $config['limit'];
		$index++;
	}
}

/* tags */
$index = 0;
$limit = $config['limit'];
foreach ($data_res['tag'] as $key => $row)
{
	$topic_url= "/keyword/".urlencode($row['tag_words']);
	$smi = new google_sitemap_item($domain . $topic_url,
		$today, $config['content_changefreq'], $config['content_priority']);
	$sm->add_item(google_sitemap::SITEMAP_SECTION_TAG.'_'.$index,$smi);
	
	if ($key > $limit) {
		$limit += $config['limit'];
		$index++;
	}
}

/* Promotions */
$index = 0;
$limit = $config['limit'];
foreach ($data_res['topic'] as $key => $row)
{
	$topic_url=build_uri('topic', array('tid' => $row['topic_id'], 'tname' => $row['title'],'tperma' => $row['tperma']), $row['title']);
	$smi = new google_sitemap_item($domain . $topic_url,
		$today, $config['content_changefreq'], $config['content_priority']);
	$sm->add_item(google_sitemap::SITEMAP_SECTION_TOPIC.'_'.$index,$smi);
	
	if ($key > $limit) {
		$limit += $config['limit'];
		$index++;
	}
}

/* 文章 */
$index = 0;
$limit = $config['limit'];
foreach ($data_res['article'] as $key => $row)
{
	$article_url=$row['open_type'] != 1 ? build_uri('article', array('aid'=>$row['article_id'], 'aname' => $row['title'],'aperma' => $row['aperma']), $row['title']) : trim($row['file_url']);
	$smi = new google_sitemap_item($domain . $article_url,
		$today, $config['content_changefreq'], $config['content_priority']);
	$sm->add_item(google_sitemap::SITEMAP_SECTION_ARTICLE.'_'.$index,$smi);

	if ($key > $limit) {
		$limit += $config['limit'];
		$index++;
	}
}

/* 文章分类 */
$index = 0;
$limit = $config['limit'];
foreach ($data_res['article_category'] as $key => $row)
{
	$smi = new google_sitemap_item($domain . build_uri('article_cat', array('acid' => $row['cat_id'], 'acname' => $row['cat_name'],'acperma' => $row['acperma']), $row['cat_name']), $today,
		$config['category_changefreq'], $config['category_priority']);
	$sm->add_item(google_sitemap::SITEMAP_SECTION_ARTICLECAT.'_'.$index,$smi);

	if ($key > $limit) {
		$limit += $config['limit'];
		$index++;
	}
}

/* 商品品牌 */
$index = 0;
$limit = $config['limit'];
foreach ($data_res['brand'] as $key => $row)
{
	$smi = new google_sitemap_item($domain . build_uri('brand', array('bid' => $row['brand_id'], 'bname' => $row['brand_name'],'bperma' => $row['bperma']), $row['brand_name']), $today,
		$config['category_changefreq'], $config['category_priority']);
	$sm->add_item(google_sitemap::SITEMAP_SECTION_BRAND.'_'.$index,$smi);

	if ($key > $limit) {
		$limit += $config['limit'];
		$index++;
	}
}

/* 商品分类 */
$index = 0;
$limit = $config['limit'];
foreach ($data_res['category'] as $key => $row)
{
	$smi = new google_sitemap_item($domain . build_uri('category', array('cid' => $row['cat_id'], 'cname' => $row['cat_name'],'cperma' => $row['cperma']), $row['cat_name']), $today,
		$config['category_changefreq'], $config['category_priority']);
	$sm->add_item(google_sitemap::SITEMAP_SECTION_CAT.'_'.$index,$smi);

	if ($key > $limit) {
		$limit += $config['limit'];
		$index++;
	}
}

clear_cache_files();	// 清除缓存

$sm_file = ROOT_PATH . 'sitemap_baidu.xml';
try{
    $res=$sm->build($sm_file,$ecs->url());
    if($res!==true){
    	$sm_file = ROOT_PATH . DATA_DIR . '/sitemap_baidu.xml';
    	$res=$sm->build($sm_file,$ecs->url());
    	if($res!==true){
    		error_log('Sitemap generation error:'.$res);
    	}else{
    		$sitemap_url = $ecs->url() . 'sitemap_baidu.xml';
    		error_log('Sitemap generated: ' . $sitemap_url);
    	}
    }else{
    	$sitemap_url = $ecs->url() . 'sitemap_baidu.xml';
    	error_log('Sitemap generated: ' . $sitemap_url);
    }

}catch(Exception $e){
	error_log('Sitemap generation error:'.$e->getMessage());
}
// to generate sitemap with https end

?>