<?php
if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}
$cron_lang = ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/cron/sendy.php';
if (file_exists($cron_lang))
{
	global $_LANG;

	include_once($cron_lang);
}

/* 模块的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
	$i = isset($modules) ? count($modules) : 0;

	/* 代码 */
	$modules[$i]['code']	= basename(__FILE__, '.php');

	/* 描述对应的语言项 */
	$modules[$i]['desc']	= 'sendy_desc';

	/* 作者 */
	$modules[$i]['author']  = 'dem';

	/* 版本号 */
	$modules[$i]['version'] = '1.0.0';

	/* 配置信息 */
	$modules[$i]['config']  = array(
        array('name' => 'email_list_id', 'type' => 'text', 'value' => '11'),
        array('name' => 'csv_max_rec', 'type' => 'text', 'value' => '5000'),
        array('name' => 'csv_delete', 'type' => 'select', 'value' => '0'),
    );

	return;
}

ini_set('memory_limit', '512M');

empty($cron['email_list_id']) && $cron['email_list_id'] = 11;
empty($cron['csv_max_rec']) && $cron['csv_max_rec'] = 5000;
empty($cron['csv_delete']) && $cron['csv_delete'] = 0;

$sql = "SELECT count(*) FROM " . $ecs->table('users') . " WHERE email NOT REGEXP '^[\\\+0-9-]+(@yohohongkong.com)$'";
$rec = $db->getOne($sql);

$base_path = strpos($_SERVER['SERVER_NAME'], 'beta.yohohongkong.com') === false ? "https://email.yohohongkong.com/s/" : "includes/sendy/";
$url_path = strpos($_SERVER['SERVER_NAME'], 'beta.yohohongkong.com') === false ? "https://email.yohohongkong.com/s/" : "beta.yohohongkong.com/includes/sendy/";
$path = strpos($_SERVER['SERVER_NAME'], 'beta.yohohongkong.com') === false ? "1-". $cron['email_list_id'] . "-" : "1-3-";

$sql = "SELECT min_points FROM " . $ecs->table('user_rank') . " WHERE rank_id = 2";
$min_points = intval($db->getOne($sql));

// $goods = [];
// $cat_tree = [];

// $cat_sql = 'SELECT cat_id FROM ' . $ecs->table('category');
// $cats = $db->getCol($cat_sql);
// foreach ($cats as $cat) {
//     $cat_id = $cat;
//     while (!empty($cat_id)) {
//         $cat_tree[$cat][] = $cat_id;
//         $cat_id = $db->getOne("SELECT parent_id FROM " . $ecs->table('category') . " WHERE cat_id = $cat_id");
//     }
// }
for($i = 0; $i < ceil($rec / $cron['csv_max_rec']); $i++){
    // $sql = "SELECT u.email, u.rank_points, u.birthday, u.reg_time, GROUP_CONCAT(DISTINCT og.goods_id) as goods, u.user_name FROM " . $ecs->table('users') . " u " .
    //         "LEFT JOIN (SELECT order_id, user_id FROM " . $ecs->table('order_info') . " WHERE shipping_status IN (1, 2)) oi ON u.user_id = oi.user_id " .
    //         "LEFT JOIN (SELECT order_id, goods_id FROM " . $ecs->table('order_goods') . ") og ON og.order_id = oi.order_id " .
    //         "WHERE email NOT REGEXP '^[\\\+0-9-]+(@yohohongkong.com)$' GROUP BY u.user_id LIMIT " . ($cron['csv_max_rec'] * $i) . ", " . $cron['csv_max_rec'];
    $sql = "SELECT u.email, u.rank_points, u.birthday, u.reg_time, u.user_name FROM " . $ecs->table('users') . " u " .
            "WHERE email NOT REGEXP '^[\\\+0-9-]+(@yohohongkong.com)$' LIMIT " . ($cron['csv_max_rec'] * $i) . ", " . $cron['csv_max_rec'];
    $data = $db->getAll($sql);
    foreach($data as $row){
        $bday = explode("-",$row['birthday']);
        $year = $bday[0] != "0000" ? intval($bday[0]) : "";
        $month = $bday[1] != "00" ? intval($bday[1]) : "";
        $reg_year = local_date('Y', $row['reg_time']);
        $is_vip = intval($row['rank_points'] >= ($min_points));
        $phone = $row['user_name'];
        // $user_goods = explode(",", $row['goods']);
        // $user_cats = [];
        // $user_brands = [];
        // foreach ($user_goods as $goods_id) {
        //     if (empty($goods[$goods_id]) && !empty($goods_id)) {
        //         $info = $db->getRow("SELECT cat_id, brand_id FROM " . $ecs->table("goods") . " WHERE goods_id = $goods_id");
        //         $goods[$goods_id] = [
        //             'cat'   => $cat_tree[$info['cat_id']],
        //             'brand' => $info['brand_id']
        //         ];
        //     }
        //     foreach ($goods[$goods_id]['cat'] as $cat_id) {
        //         if (!in_array($cat_id, $user_cats)) {
        //             $user_cats[] = $cat_id;
        //         }
        //     }
        //     if (!empty($goods[$goods_id]['brand']) && !in_array($goods[$goods_id]['brand'], $user_brands)) {
        //         $user_brands[] = $goods[$goods_id]['brand'];
        //     }
        // }
        // $user_cats = "-" . implode("-", $user_cats) . "-";
        // $user_brands = "-" . implode("-", $user_brands) . "-";
        $user_data[] = array("", $row['email'], $is_vip, $year, $month, $reg_year, $phone);
        $user_data_del[] = array($row['email']);
    }
    if(!file_exists(ROOT_PATH . 'data/sendy')){
        mkdir(ROOT_PATH . 'data/sendy', 0777, true);
    }
    $file_path = ROOT_PATH . 'data/sendy/' . $path . ($i + 1) . '.csv';
    $fp = fopen($file_path,'w');
    for($j = 0; $j < $cron['csv_max_rec'] && $j + $i * $cron['csv_max_rec'] < count($user_data); $j++){
        fputcsv($fp,$user_data[$j + $i * $cron['csv_max_rec']]);
    }
    fclose($fp);
    $post = array('csv_upload'=>'1');
    $cFile = new CURLFile(realpath($file_path), 'text/csv', basename($file_path));
    $post['csv_file'] = $cFile;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url_path . 'upload-csv.php');
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Auth-Token: pBcP6BYusAhrP9TGfDCi7NfHhg2CZtO5FzKJ5vcDFfwhSUxZuJfLkM7VQYlk'));
    curl_setopt($ch, CURLOPT_POST,1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    $result = curl_exec ($ch);
    if($result === FALSE) {
        $db->query("INSERT INTO dem_test_abnormal (logs) VALUES ('Loop$i: " . curl_error($ch) . "')");
        var_dump('Loop'.$i.': '.curl_error($ch));
    }
    if(curl_getinfo($ch, CURLINFO_HTTP_CODE) != 200){
        $db->query("INSERT INTO dem_test_abnormal (logs) VALUES ('Loop$i-Error: " . curl_getinfo($ch, CURLINFO_HTTP_CODE) . "')");
    }
    curl_close ($ch);
    if($cron['csv_delete']){
        unlink($file_path);
    }
    // case '1': success
    // case '2': fail to move to directory
    // case '3': file not receive
    // case '4': missing or incorrect parameter
    // case '5': authorization fail
    // default:  unknown error
    if($result != '1'){
        $db->query("INSERT INTO dem_test_abnormal (logs) VALUES ('Loop$i-Result: $result')");
    }
}
?>