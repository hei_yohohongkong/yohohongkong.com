<?php

/***
* includes/modules/cron/send_review_mail.php
* by howang 2014-06-23
*
* ECSHOP 自動寄填寫購物體驗電郵
***/

if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}
$cron_lang = ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/cron/send_review_mail.php';
$userController = new Yoho\cms\Controller\UserController();
$orderController = new Yoho\cms\Controller\OrderController();
if (file_exists($cron_lang))
{
	global $_LANG;

	include_once($cron_lang);
}

/* 模块的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
	$i = isset($modules) ? count($modules) : 0;

	/* 代码 */
	$modules[$i]['code']	= basename(__FILE__, '.php');

	/* 描述对应的语言项 */
	$modules[$i]['desc']	= 'send_review_mail_desc';

	/* 作者 */
	$modules[$i]['author']  = 'anthony';

	/* 网址 */
	$modules[$i]['website'] = '';

	/* 版本号 */
	$modules[$i]['version'] = '1.0.0';

	/* 配置信息 */
	$modules[$i]['config']  = array(array('name' => 'auto_manage_day', 'type' => 'text', 'value' => '2'));

	return;
}

/*------------------------------------------------------ */
//-- Send E-mail
/*------------------------------------------------------ */
$days = isset($cron['auto_manage_day']) ? $cron['auto_manage_day'] : 2;

$time = local_strtotime('-'.$days.' days');
$start_time = local_mktime(0, 0, 0, local_date('m', $time), local_date('d', $time), local_date('Y', $time));
$end_time = local_mktime(23, 59, 59, local_date('m', $time), local_date('d', $time), local_date('Y', $time));

$sql = "SELECT * FROM ".$ecs->table('order_info') . "WHERE 1 ";
// Exclude orders for the followiung user accounts: 11111111, 88888888, 99999999
$where_exclude = " AND (order_type<>'".$orderController::DB_ORDER_TYPE_WHOLESALE."' OR (order_type IS NULL AND user_id NOT IN (".implode(',',$userController->getWholesaleUserIds())."))) ";
// Exclude sales agent
$where_exclude .= " AND (order_type <> '" . $orderController::DB_ORDER_TYPE_SALESAGENT . "' OR order_type IS NULL) ";
// Exclude 展場POS
$where_exclude .= " AND (platform <> 'pos_exhibition') ";
$where_status = " AND order_status NOT IN (" . OS_CANCELED . ',' . OS_INVALID . ',' . OS_RETURNED . ") ".
	" AND shipping_status IN (" . SS_SHIPPED . ',' . SS_RECEIVED . ") ";
$where_today = " AND shipping_time BETWEEN " . $start_time . " AND " . $end_time;

$order_list = $db->getAll($sql.$where_exclude.$where_status.$where_today);
// $smarty = new cls_template;

foreach($order_list as $key => $order) {
	$order_id = $order['order_id'];
	$order_sn = $order['order_sn'];
	$order['consignee']     = empty($order['consignee']) ? '客人' : $order['consignee'];
	$order['shipping_time'] = local_date('Y-m-d', $order['shipping_time']);
	$goods_sql = "SELECT goods_name FROM ".$ecs->table('order_goods')." WHERE order_id = ".$order_id;
	$order['goods_list'] = $goods_list = $db->getCol($goods_sql);

	if (can_send_mail($order['email'], true) && is_email($order['email']))
	{
		if (empty($smarty))
		{
			/* 创建 Smarty 对象。*/
			require(ROOT_PATH . 'core/cls_template.php');
			$smarty = new cls_template;

		}
		$tpl = get_mail_template('remind_reivew');
		$smarty->assign('order', $order);
		$smarty->assign('shop_url', $ecs->url());
		$smarty->assign('review_link', $ecs->url() . 'review.php?order_id=' . $order['order_id']);
		$smarty->assign('shop_name', $GLOBALS['_CFG']['shop_name']);
		$smarty->assign('send_date', local_date($GLOBALS['_CFG']['date_format']));
		$smarty->assign('sent_date', local_date($GLOBALS['_CFG']['date_format']));
		$content = $smarty->fetch('str:' . $tpl['template_content']);

		send_mail($order['consignee'], $order['email'], $tpl['template_subject'] . ' - ' . $order['order_sn'], $content, $tpl['is_html']);
		order_action($order['order_sn'], $order['order_status'], $order['shipping_status'],  $order['pay_status'], '系統已自動發送填寫購物體驗電郵' , 'admin');
	}
}

?>