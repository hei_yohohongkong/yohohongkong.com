<?php
if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}
$cron_lang = ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/cron/stock_cost.php';
require_once(ROOT_PATH . 'includes/lib_order.php');
require_once ROOT_PATH . 'includes/lib_howang.php';
if (file_exists($cron_lang))
{
	global $_LANG;

	include_once($cron_lang);
}

/* 模块的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
	$i = isset($modules) ? count($modules) : 0;

	/* 代码 */
	$modules[$i]['code']	= basename(__FILE__, '.php');

	/* 描述对应的语言项 */
	$modules[$i]['desc']	= 'stock_cost_desc';

	/* 作者 */
	$modules[$i]['author']  = 'dem';

	/* 版本号 */
	$modules[$i]['version'] = '1.0.0';

	/* 配置信息 */
	$modules[$i]['config']  = array(
        array('name' => 'stock_cost_cat', 'type' => 'select', 'value' => '0'),        
        array('name' => 'stock_cost_brand', 'type' => 'select', 'value' => '0'),        
        array('name' => 'stock_cost_supp', 'type' => 'select', 'value' => '0'),        
    );

	return;
}
const COST_LOG_TYPE_CATEGORY = 1;
const COST_LOG_TYPE_BRAND = 2;
const COST_LOG_TYPE_SUPPLIER = 3;
const COST_LOG_TYPE_GOOD = 4;
ini_set('memory_limit', '512M');

empty($cron['stock_cost_cat']) && $cron['stock_cost_cat'] = 0;
empty($cron['stock_cost_brand']) && $cron['stock_cost_brand'] = 0;
empty($cron['stock_cost_supp']) && $cron['stock_cost_supp'] = 0;

if ($_REQUEST['force_update_stock_cost_supp'] == 1) {
   $cron['stock_cost_supp'] = 1;
}

$autoPricingController = new Yoho\cms\Controller\AutoPricingController();
$start_time = strtotime(date("Y/m/d")) - 86400;
$end_time = strtotime(date("Y/m/d"));
$log_time = time();
$sql = "SELECT user_id FROM " . $ecs->table('users') . " WHERE is_wholesale = 1";

$wholesale_user_id_ar = $db->getCol($sql);
$wholesale_user_id = implode(',',$wholesale_user_id_ar);

$sql = "SELECT GROUP_CONCAT(warehouse_id) FROM ". $ecs->table('erp_warehouse'). " WHERE is_valid = 1 AND is_on_sale = 1";
$warehouse_id = $db->getOne($sql);
$time_yesterday = local_strtotime('today'); // 23:59:59 yesterday
$time_30days_before = local_strtotime('-30 days midnight'); // 00:00:00 30 days ago
$time_60days_before = local_strtotime('-60 days midnight'); // 00:00:00 60 days ago
$time_90days_before = local_strtotime('-90 days midnight'); // 00:00:00 90 days ago

$dead_cost_log = [];
if ($cron['stock_cost_cat'] || $cron['stock_cost_brand'] || $cron['stock_cost_supp']) {
    $dead_goods = $autoPricingController->getDeadStockCost();
    foreach ($dead_goods as $goods_id => $goods) {
        if (!isset($dead_cost_log['brand'][$goods['brand_id']]))  {
            $dead_cost_log['brand'][$goods['brand_id']] = [
                '30' => ['cost' => 0, 'sales' => 0],
                '60' => ['cost' => 0, 'sales' => 0],
                '90' => ['cost' => 0, 'sales' => 0],
            ];
        }
        if (!isset($dead_cost_log['cat'][$goods['cat_id']]))  {
            $dead_cost_log['cat'][$goods['cat_id']] = [
                '30' => ['cost' => 0, 'sales' => 0],
                '60' => ['cost' => 0, 'sales' => 0],
                '90' => ['cost' => 0, 'sales' => 0],
            ];
        }
        $dead_cost_log['goods'][$goods_id] = [];
        foreach ($goods['data'] as $day => $data) {
            $dead_cost_log['brand'][$goods['brand_id']][$day]['cost'] = bcadd($dead_cost_log['brand'][$goods['brand_id']][$day]['cost'], $data['cost']);
            $dead_cost_log['brand'][$goods['brand_id']][$day]['sales'] = bcadd($dead_cost_log['brand'][$goods['brand_id']][$day]['sales'], $data['sales']);
            $dead_cost_log['cat'][$goods['cat_id']][$day]['cost'] = bcadd($dead_cost_log['cat'][$goods['cat_id']][$day]['cost'], $data['cost']);
            $dead_cost_log['cat'][$goods['cat_id']][$day]['sales'] = bcadd($dead_cost_log['cat'][$goods['cat_id']][$day]['sales'], $data['sales']);
            $dead_cost_log['goods'][$goods_id][$day] = [
                'total_cost' => $goods['total_cost'],
                'cost' => $data['cost'],
                'sales' => $data['sales'],
                'qty' => $data['qty'],
                'group' => min($data['d_group'], 180),
            ];
        }
    }

    foreach ($dead_cost_log['goods'] as $goods_id => $data) {
        $sql = "INSERT INTO " . $ecs->table('stock_cost_log') . "(report_date, created_at, type, type_id, cost, slowsoldcost_30, slowsoldcost_60, slowsoldcost_90, ndayssales_30, ndayssales_60, ndayssales_90, deadqty_60, deadqty_30, deadqty_90, deadgroup_30, deadgroup_60, deadgroup_90) VALUES (" . $start_time . ", " . $log_time . ", " . COST_LOG_TYPE_GOOD . ", " . $goods_id . ", '" . substr(price_format($data['total_cost']),4) . "', '" . substr(price_format($data['30']['cost']),4) . "', '" . substr(price_format($data['60']['cost']),4) . "', '" . substr(price_format($data['90']['cost']),4) . "', '" . $data['30']['sales'] . "', '" . $data['60']['sales'] . "', '" . $data['90']['sales'] . "', '" . $data['30']['qty'] . "', '" . $data['60']['qty'] . "', '" . $data['90']['qty'] . "', '" . $data['30']['group'] . "', '" . $data['60']['group'] . "', '" . $data['90']['group'] . "')";
        $db->query($sql);
    }
}

// Cost By Category
$where = " oi.shipping_time >= '" . $start_time . "' AND oi.shipping_time < '" . $end_time . "' " . order_query_sql('finished', 'oi.');
$where_ws = "(" . str_replace("oi.pay_status  IN ('", "oi.pay_status  IN ('".PS_UNPAYED."','", explode('AND', order_query_sql('finished', 'oi.'))[3]);
$where_ws .= " AND (oi.order_type = '".Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE."' OR (oi.user_id IN (". $wholesale_user_id .") AND oi.order_type IS NULL) ) ";
$where = str_replace("oi.pay_status  IN ('", "( oi.pay_status  IN ('", $where) . " OR " . $where_ws . " ) )";
if($cron['stock_cost_cat']){
    $sql = 'SELECT a.cat_id, a.cat_name, a.sort_order AS parent_order, a.cat_id, a.is_show,' .
        'b.cat_id AS child_id, b.cat_name AS child_name, b.sort_order AS child_order, ' .
        'c.cat_id AS grandson_id, c.cat_name AS grandson_name, c.sort_order AS grandson_order, ' .
        'IFNULL(sum(gn.goods_number),0) as total_stocks, ' .
        'IFNULL(sum(gn.goods_number * g.cost),0) as total_cost ' .
        // 'IFNULL(sum(gp.revenue),0) as total_revenue, ' .
        // 'IFNULL(sum(gp.cost),0) as total_gpcost, ' .
        // 'IFNULL(sum(gp.revenue - gp.cost),0) as total_profit, ' .
        // 'IFNULL(sum(gp.sap),0) as total_sa_revenue, '.
        // 'IFNULL(sum(gp.sap - gp.cost),0) as total_sa_profit, ' .
        // 'IFNULL(SUM(' .
        //     'CASE WHEN td.thirtydayssales IS NULL OR gn.goods_number * 3 >= td.thirtydayssales * 5 THEN gn.goods_number * g.cost ' .
        //     'ELSE 0 END' .
        // '), 0) as dead_cost_30, ' .
        // 'IFNULL(SUM(' .
        //     'CASE WHEN sd.sixtydayssales IS NULL OR gn.goods_number * 3 >= sd.sixtydayssales * 5 THEN gn.goods_number * g.cost ' .
        //     'ELSE 0 END' .
        // '), 0) as dead_cost_60, ' .
        // 'IFNULL(SUM(' .
        //     'CASE WHEN nd.ninetydayssales IS NULL OR gn.goods_number * 3 >= nd.ninetydayssales * 5 THEN gn.goods_number * g.cost ' .
        //     'ELSE 0 END' .
        // '), 0) as dead_cost_90 ' .
        'FROM ' . $ecs->table('category') . ' AS a ' .
        'LEFT JOIN ' . $ecs->table('category') . ' AS b ON b.parent_id = a.cat_id AND b.is_show = 1 ' .
        'LEFT JOIN ' . $ecs->table('category') . ' AS c ON c.parent_id = b.cat_id AND c.is_show = 1 ' .
        'LEFT JOIN ' . $ecs->table('goods') . ' AS g ON g.cat_id = c.cat_id AND g.is_delete = 0 ' .
        'LEFT JOIN (SELECT ega.goods_id, sum(ega.goods_qty) as goods_number FROM ' . $ecs->table('erp_goods_attr_stock') . ' AS ega WHERE ega.warehouse_id IN ('.$warehouse_id.') GROUP BY ega.goods_id) as gn ON gn.goods_id = g.goods_id ' .
        'LEFT JOIN (' .
        'SELECT og.goods_id, ' .
        'sum((IF(og.cost > 0, og.cost, g2.cost) + ' .
            'IF(oi.`pay_id` = 2,  IF(og.cost > 0, og.cost, g2.cost) * 0.0095,       0) + ' . // EPS: 0.95% fee
            'IF(oi.`pay_id` = 7,  IF(og.cost > 0, og.cost, g2.cost) * 0.032 + 2.35, 0) + ' . // PayPal: 3.2% fee + $2.35 per txn
            'IF(oi.`pay_id` = 10, IF(og.cost > 0, og.cost, g2.cost) * 0.029,        0) + ' . // UnionPay: 2.9% fee
            'IF(oi.`pay_id` = 12, IF(og.cost > 0, og.cost, g2.cost) * 0.023,        0) + ' . // Visa / MasterCard: 2.3% fee
            'IF(oi.`pay_id` = 13, IF(og.cost > 0, og.cost, g2.cost) * 0.0296,       0)   ' . // American Express: 2.96% fee
            ') * og.goods_number + ' .
            'IF(oi.`shipping_id` = 3, 30 * og.goods_price * og.goods_number / oi.goods_amount, 0) ' . // Delivery cost: $30
        ') as cost ' .
        // "sum( (( ((100 - IF(og.sa_rate_id > 0 , sa.sa_rate, 0) ) /100 ) * og.goods_price)*og.goods_number) - IF(oi.`pay_fee` < 0, oi.`pay_fee` * -1 * og.goods_price * og.goods_number / oi.goods_amount, 0) ) as sap, ".
        // 'sum(og.goods_price * og.goods_number - IF(oi.`pay_fee` < 0, oi.`pay_fee` * -1 * og.goods_price * og.goods_number / oi.goods_amount, 0)) as revenue ' .
        'FROM ' . $ecs->table('order_goods') . ' AS og ' .
        'LEFT JOIN ' . $ecs->table('order_info') . ' AS oi ON oi.order_id = og.order_id ' .
        'LEFT JOIN ' . $ecs->table('goods') . ' AS g2 ON g2.goods_id = og.goods_id ' .
        "LEFT JOIN " . $ecs->table('sales_agent_rate') . " as sa ON sa.sa_rate_id = og.sa_rate_id ".
        'WHERE ' .$where.
        'GROUP BY og.goods_id' .
        ') as gp ON gp.goods_id = g.goods_id ' .
        // 'LEFT JOIN (' .
        // 'SELECT og.goods_id, IFNULL(SUM(og.goods_number), 0) AS thirtydayssales FROM ' . $ecs->table('order_goods') . ' AS og ' .
        // 'LEFT JOIN ' . $ecs->table('order_info') . ' AS oi ON oi.order_id = og.order_id ' .
        // 'WHERE oi.order_status IN (1, 5) AND oi.shipping_status IN (1, 2) AND oi.pay_status IN (2, 1) AND ' .
        // '(oi.shipping_time BETWEEN ' . $time_30days_before . ' AND ' . $time_yesterday . ') GROUP BY og.goods_id' .
        // ') AS td ON td.goods_id = g.goods_id ' .
        // 'LEFT JOIN (' .
        // 'SELECT og.goods_id, IFNULL(SUM(og.goods_number), 0) AS sixtydayssales FROM ' . $ecs->table('order_goods') . ' AS og ' .
        // 'LEFT JOIN ' . $ecs->table('order_info') . ' AS oi ON oi.order_id = og.order_id ' .
        // 'WHERE oi.order_status IN (1, 5) AND oi.shipping_status IN (1, 2) AND oi.pay_status IN (2, 1) AND ' .
        // '(oi.shipping_time BETWEEN ' . $time_60days_before . ' AND ' . $time_yesterday . ') GROUP BY og.goods_id' .
        // ') AS sd ON sd.goods_id = g.goods_id ' .
        // 'LEFT JOIN (' .
        // 'SELECT og.goods_id, IFNULL(SUM(og.goods_number), 0) AS ninetydayssales FROM ' . $ecs->table('order_goods') . ' AS og ' .
        // 'LEFT JOIN ' . $ecs->table('order_info') . ' AS oi ON oi.order_id = og.order_id ' .
        // 'WHERE oi.order_status IN (1, 5) AND oi.shipping_status IN (1, 2) AND oi.pay_status IN (2, 1) AND ' .
        // '(oi.shipping_time BETWEEN ' . $time_90days_before . ' AND ' . $time_yesterday . ') GROUP BY og.goods_id' .
        // ') AS nd ON nd.goods_id = g.goods_id ' .
        "WHERE a.parent_id = '0' GROUP BY c.cat_id ORDER BY parent_order ASC, a.cat_id ASC, child_order ASC, grandson_order ASC, grandson_id ASC";

    $res = $db->getAll($sql);
    foreach ($res AS $row)
    {
        if($row['is_show'] == 1){
            $sql = "INSERT INTO " . $ecs->table('stock_cost_log') . "(report_date, created_at, type, type_id, cost, slowsoldcost_30, slowsoldcost_60, slowsoldcost_90) VALUES (" . $start_time . ", " . $log_time . ", " . COST_LOG_TYPE_CATEGORY . ", " . $row['grandson_id'] . ", '" . substr(price_format($row['total_cost']),4) . "', '" . substr(price_format($dead_cost_log['cat'][$row['grandson_id']]['30']['cost']),4) . "', '" . substr(price_format($dead_cost_log['cat'][$row['grandson_id']]['60']['cost']),4) . "', '" . substr(price_format($dead_cost_log['cat'][$row['grandson_id']]['90']['cost']),4) . "')";
            $db->query($sql);
        }
    }
}
if($cron['stock_cost_brand']){
    $sql = "SELECT g.brand_id, IFNULL(b.brand_name, '沒有品牌') as brand_name, b.sort_order, " .
        'IFNULL(sum(gn.goods_number),0) as total_stocks, ' .
        'IFNULL(sum(gn.goods_number * g.cost),0) as total_cost ' .
        // 'IFNULL(sum(gp.revenue),0) as total_revenue, ' .
        // 'IFNULL(sum(gp.cost),0) as total_gpcost, ' .
        // 'IFNULL(sum(gp.revenue - gp.cost),0) as total_profit, ' .
        // 'IFNULL(sum(gp.sap),0) as total_sa_revenue, '.
        // 'IFNULL(sum(gp.sap - gp.cost),0) as total_sa_profit, ' .
        // 'IFNULL(SUM(' .
        //     'CASE WHEN td.thirtydayssales IS NULL OR gn.goods_number * 3 >= td.thirtydayssales * 5 THEN gn.goods_number * g.cost ' .
        //     'ELSE 0 END' .
        // '), 0) as dead_cost_30, ' .
        // 'IFNULL(SUM(' .
        //     'CASE WHEN sd.sixtydayssales IS NULL OR gn.goods_number * 3 >= sd.sixtydayssales * 5 THEN gn.goods_number * g.cost ' .
        //     'ELSE 0 END' .
        // '), 0) as dead_cost_60, ' .
        // 'IFNULL(SUM(' .
        //     'CASE WHEN nd.ninetydayssales IS NULL OR gn.goods_number * 3 >= nd.ninetydayssales * 5 THEN gn.goods_number * g.cost ' .
        //     'ELSE 0 END' .
        // '), 0) as dead_cost_90 ' .
        'FROM ' . $ecs->table('goods') . ' AS g ' .
        'LEFT JOIN ' . $ecs->table('brand') . ' AS b ON g.brand_id = b.brand_id ' .
        'LEFT JOIN (SELECT ega.goods_id, sum(ega.goods_qty) as goods_number FROM ' . $ecs->table('erp_goods_attr_stock') . ' AS ega WHERE ega.warehouse_id IN ('. $warehouse_id .') GROUP BY ega.goods_id) as gn ON gn.goods_id = g.goods_id ' .
        'LEFT JOIN (' .
        'SELECT og.goods_id, ' .
        'sum((IF(og.cost > 0, og.cost, g2.cost) + ' .
            'IF(oi.`pay_id` = 2,  IF(og.cost > 0, og.cost, g2.cost) * 0.0095,       0) + ' . // EPS: 0.95% fee
            'IF(oi.`pay_id` = 7,  IF(og.cost > 0, og.cost, g2.cost) * 0.032 + 2.35, 0) + ' . // PayPal: 3.2% fee + $2.35 per txn
            'IF(oi.`pay_id` = 10, IF(og.cost > 0, og.cost, g2.cost) * 0.029,        0) + ' . // UnionPay: 2.9% fee
            'IF(oi.`pay_id` = 12, IF(og.cost > 0, og.cost, g2.cost) * 0.023,        0) + ' . // Visa / MasterCard: 2.3% fee
            'IF(oi.`pay_id` = 13, IF(og.cost > 0, og.cost, g2.cost) * 0.0296,       0)   ' . // American Express: 2.96% fee
            ') * og.goods_number + ' .
            'IF(oi.`shipping_id` = 3, 30 * og.goods_price * og.goods_number / oi.goods_amount, 0) ' . // Delivery cost: $30
        ') as cost ' .
        // "sum( (( ((100 - IF(og.sa_rate_id > 0 , sa.sa_rate, 0) ) /100 ) * og.goods_price)*og.goods_number) - IF(oi.`pay_fee` < 0, oi.`pay_fee` * -1 * og.goods_price * og.goods_number / oi.goods_amount, 0) ) as sap, ".
        // 'sum(og.goods_price * og.goods_number - IF(oi.`pay_fee` < 0, oi.`pay_fee` * -1 * og.goods_price * og.goods_number / oi.goods_amount, 0)) as revenue ' .
        'FROM ' . $ecs->table('order_goods') . ' AS og ' .
        'LEFT JOIN ' . $ecs->table('order_info') . ' AS oi ON oi.order_id = og.order_id ' .
        'LEFT JOIN ' . $ecs->table('goods') . ' AS g2 ON g2.goods_id = og.goods_id ' .
        "LEFT JOIN " . $ecs->table('sales_agent_rate') . " as sa ON sa.sa_rate_id = og.sa_rate_id ".
        'WHERE ' . $where .
        'GROUP BY og.goods_id' .
        ') as gp ON gp.goods_id = g.goods_id ' .
        // 'LEFT JOIN (' .
        // 'SELECT og.goods_id, IFNULL(SUM(og.goods_number), 0) AS thirtydayssales FROM ' . $ecs->table('order_goods') . ' AS og ' .
        // 'LEFT JOIN ' . $ecs->table('order_info') . ' AS oi ON oi.order_id = og.order_id ' .
        // 'WHERE oi.order_status IN (1, 5) AND oi.shipping_status IN (1, 2) AND oi.pay_status IN (2, 1) AND ' .
        // '(oi.shipping_time BETWEEN ' . $time_30days_before . ' AND ' . $time_yesterday . ') GROUP BY og.goods_id' .
        // ') AS td ON td.goods_id = g.goods_id ' .
        // 'LEFT JOIN (' .
        // 'SELECT og.goods_id, IFNULL(SUM(og.goods_number), 0) AS sixtydayssales FROM ' . $ecs->table('order_goods') . ' AS og ' .
        // 'LEFT JOIN ' . $ecs->table('order_info') . ' AS oi ON oi.order_id = og.order_id ' .
        // 'WHERE oi.order_status IN (1, 5) AND oi.shipping_status IN (1, 2) AND oi.pay_status IN (2, 1) AND ' .
        // '(oi.shipping_time BETWEEN ' . $time_60days_before . ' AND ' . $time_yesterday . ') GROUP BY og.goods_id' .
        // ') AS sd ON sd.goods_id = g.goods_id ' .
        // 'LEFT JOIN (' .
        // 'SELECT og.goods_id, IFNULL(SUM(og.goods_number), 0) AS ninetydayssales FROM ' . $ecs->table('order_goods') . ' AS og ' .
        // 'LEFT JOIN ' . $ecs->table('order_info') . ' AS oi ON oi.order_id = og.order_id ' .
        // 'WHERE oi.order_status IN (1, 5) AND oi.shipping_status IN (1, 2) AND oi.pay_status IN (2, 1) AND ' .
        // '(oi.shipping_time BETWEEN ' . $time_90days_before . ' AND ' . $time_yesterday . ') GROUP BY og.goods_id' .
        // ') AS nd ON nd.goods_id = g.goods_id ' .
        "WHERE (b.is_show IS NULL OR b.is_show = 1) AND g.is_delete = 0 GROUP BY g.brand_id ORDER BY brand_name ASC";

    $res = $db->getAll($sql);
    foreach ($res AS $row)
    {
        $sql = "INSERT INTO " . $ecs->table('stock_cost_log') . "(report_date, created_at, type, type_id, cost, slowsoldcost_30, slowsoldcost_60, slowsoldcost_90) VALUES (" . $start_time . ", " . $log_time . ", " . COST_LOG_TYPE_BRAND . ", ". $row['brand_id'] . ", '" . substr(price_format($row['total_cost']),4) . "', '" . substr(price_format($dead_cost_log['brand'][$row['brand_id']]['30']['cost']),4) . "', '" . substr(price_format($dead_cost_log['brand'][$row['brand_id']]['60']['cost']),4) . "', '" . substr(price_format($dead_cost_log['brand'][$row['brand_id']]['90']['cost']),4) . "')";
        $db->query($sql);
    }
}
if($cron['stock_cost_supp']){
    $cost_arr = [];
    // $dead_arr = [];
    $sql_supplier_list = 
        "SELECT eoi.goods_id,g.goods_num,eoi.warehousing_qty as qty,(g.goods_num-eoi.warehousing_qty) as sum,eoi.price as unit_cost ,eo.order_id,eo.supplier_id
        FROM ".$GLOBALS['ecs']->table('erp_order_item')." eoi 
        LEFT JOIN ".$GLOBALS['ecs']->table('erp_order')." eo ON eoi.order_id=eo.order_id ".
        "LEFT JOIN (SELECT g.goods_id, rg.goods_num FROM ".$GLOBALS['ecs']->table('goods')." as g ".
        " LEFT JOIN ( SELECT ".hw_goods_number_subquery('g.','goods_num').", g.goods_id FROM ".$GLOBALS['ecs']->table('goods')." as g ) as rg ON rg.goods_id = g.goods_id ".
        " WHERE rg.goods_num > 0 order by g.goods_id DESC ) as g on g.goods_id = eoi.goods_id ".
        "WHERE g.goods_num > 0 AND eo.order_status = 4 AND eoi.warehousing_qty > 0 ORDER BY eoi.goods_id DESC, eoi.order_item_id DESC";

    $po_list = $GLOBALS['db']->getAll($sql_supplier_list);

    // $n_days = [
    //     '30' => $time_30days_before,
    //     '60' => $time_60days_before,
    //     '90' => $time_90days_before
    // ];
    // foreach ($n_days as $key => $n_days_before) {
    //     $sql_n_day_sales = 'SELECT gn.goods_id, CASE WHEN td.ndayssales IS NULL OR gn.goods_number * 3 >= td.ndayssales * 5 THEN 1 ELSE 0 END AS dead ' .
    //     'FROM (SELECT ega.goods_id, sum(ega.goods_qty) as goods_number FROM ' . $ecs->table('erp_goods_attr_stock') . ' AS ega WHERE ega.warehouse_id IN ('. $warehouse_id .') GROUP BY ega.goods_id) as gn ' .
    //     'LEFT JOIN (SELECT og.goods_id, IFNULL(SUM(og.goods_number), 0) AS ndayssales FROM ' . $ecs->table('order_goods') . ' AS og ' .
    //     'LEFT JOIN ' . $ecs->table('order_info') . ' AS oi ON oi.order_id = og.order_id ' .
    //     'WHERE oi.order_status IN (1, 5) AND oi.shipping_status IN (1, 2) AND oi.pay_status IN (2, 1) AND ' .
    //     '(oi.shipping_time BETWEEN ' . $n_days_before . ' AND ' . $time_yesterday . ') GROUP BY og.goods_id' .
    //     ') AS td ON td.goods_id = gn.goods_id GROUP BY gn.goods_id HAVING dead = 1';
    //     $dead_list[$key] = $GLOBALS['db']->getAll($sql_n_day_sales);
    // }

    // foreach ($dead_list as $key => $goods) {
    //     foreach ($goods as $good){
    //         $dead_arr[$key][$good['goods_id']] = 1;
    //     }
    // }

    $next_goods = true;
    $goods_id   = 0;
    foreach ($po_list as $key => $po) {
        if($next_goods && $po['goods_id'] == $goods_id)continue;

        // What goods we processing
        $next_goods = false;
        $goods_num = ($po['goods_id'] == $goods_id)?$in_qty:$po['goods_num'];
        $goods_id = $po['goods_id'];
        $in_qty   = $goods_num - $po['qty'];
        if($in_qty > 0) // Stock > PO 入貨數
        {
            $qty  = $po['qty'];
            $cost = $qty * $po['unit_cost'];
        } elseif ($in_qty <= 0)
        {
            $qty  = $goods_num;
            $cost = $qty * $po['unit_cost'];
            $next_goods = true;
        }
        $cost_arr[$po['supplier_id']]['cost'] = isset($cost_arr[$po['supplier_id']]['cost']) ? $cost_arr[$po['supplier_id']]['cost']+= $cost: $cost;
        $cost_arr[$po['supplier_id']]['qty']  = isset($cost_arr[$po['supplier_id']]['qty']) ? $cost_arr[$po['supplier_id']]['qty']+= $qty: $qty;
        foreach ($dead_cost_log['goods'][$goods_id] as $day_key => $data) {
                $dead_stock = $data['sales'] - $qty;
                if ($dead_stock > 0) {
                    $dead_cost_log['goods'][$goods_id][$day_key]['sales'] -= intval($qty);
                } else {
                    if ( $data['group'] == 0) { // we ignore d_group is 0
                        $cost = 0;
                    } else {
                        $cost = bcmul(abs($dead_stock), $po['unit_cost']);
                    }

                    $cost_arr[$po['supplier_id']]['dead_cost_'.$day_key] = isset($cost_arr[$po['supplier_id']]['dead_cost_'.$day_key]) ? $cost_arr[$po['supplier_id']]['dead_cost_'.$day_key]+= $cost: $cost;
                    $dead_cost_log['goods'][$goods_id][$day_key]['sales'] = 0;
                }
            
        }
    }
    foreach ($cost_arr AS $supplier_id => $value)
    {
        
        if ($_REQUEST['force_update_stock_cost_supp'] == 1) {
            // update cost
            $sql = "update ". $ecs->table('stock_cost_log')." set slowsoldcost_30 = ".substr(price_format($value['dead_cost_30']),4).", slowsoldcost_60 = ".substr(price_format($value['dead_cost_60']),4).", slowsoldcost_90 = ".substr(price_format($value['dead_cost_90']),4)." where report_date = 1570032000 and type = 3 and type_id = ".$supplier_id." "; // 10 3
            $db->query($sql);
            $sql = "update ". $ecs->table('stock_cost_log')." set slowsoldcost_30 = ".substr(price_format($value['dead_cost_30']),4).", slowsoldcost_60 = ".substr(price_format($value['dead_cost_60']),4).", slowsoldcost_90 = ".substr(price_format($value['dead_cost_90']),4)." where report_date = 1570118400 and type = 3 and type_id = ".$supplier_id." "; // 10 4
            $db->query($sql);
            $sql = "update ". $ecs->table('stock_cost_log')." set slowsoldcost_30 = ".substr(price_format($value['dead_cost_30']),4).", slowsoldcost_60 = ".substr(price_format($value['dead_cost_60']),4).", slowsoldcost_90 = ".substr(price_format($value['dead_cost_90']),4)." where report_date = 1570204800 and type = 3 and type_id = ".$supplier_id." "; // 10 5
            $db->query($sql);
            $sql = "update ". $ecs->table('stock_cost_log')." set slowsoldcost_30 = ".substr(price_format($value['dead_cost_30']),4).", slowsoldcost_60 = ".substr(price_format($value['dead_cost_60']),4).", slowsoldcost_90 = ".substr(price_format($value['dead_cost_90']),4)." where report_date = 1570291200 and type = 3 and type_id = ".$supplier_id." "; // 10 6
            $db->query($sql);
            $sql = "update ". $ecs->table('stock_cost_log')." set slowsoldcost_30 = ".substr(price_format($value['dead_cost_30']),4).", slowsoldcost_60 = ".substr(price_format($value['dead_cost_60']),4).", slowsoldcost_90 = ".substr(price_format($value['dead_cost_90']),4)." where report_date = 1570377600 and type = 3 and type_id = ".$supplier_id." "; // 10 7
            $db->query($sql);
        } else {
            $sql = "INSERT INTO " . $ecs->table('stock_cost_log') . "(report_date, created_at, type, type_id, cost, slowsoldcost_30, slowsoldcost_60, slowsoldcost_90) VALUES (" . $start_time . ", " . $log_time . ", " . COST_LOG_TYPE_SUPPLIER . ", ". $supplier_id . ", '" . substr(price_format($value['cost']),4) . "', '" . substr(price_format($value['dead_cost_30']),4) . "', '" . substr(price_format($value['dead_cost_60']),4) . "', '" . substr(price_format($value['dead_cost_90']),4) . "')";
            $db->query($sql);
        }
    }
}
?>