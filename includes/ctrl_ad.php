<?php

/**
* ECShop YohoHongkong Development
* Ad Controller
 */
namespace Yoho\Controllers;

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}


//TODO: separate model, storing data in DB
class AdController extends YohoBaseController{

	protected $adPool=[],$zonePool=[];
	private $sessionKey = 'adServe';

	public function __construct(){
		parent::__construct();

	}

	public function hasActiveAd(){
		foreach($this->adPool as $ad){
			if($ad['active']==true)
				return true;
		}
		return false;
	}

	public function smartyAdAssign(&$smarty){
		$processedZone=[];
		foreach($this->adPool as $ad){
			if(!in_array($ad['zoneId'], $processedZone)){
				$smarty->assign('adzone'.$ad['zoneId'],$this->adServe($ad['zoneId'],false));
				$processedZone[]=$ad['zoneId'];
			}
		}
	}

	private function _checkFreq($adId,$freq){
		if(isset($_REQUEST['dev']) && $_REQUEST['dev']=='clearAd')
			if(isset($_SESSION[$this->sessionKey]))
				unset($_SESSION[$this->sessionKey]);

		if(!isset($_SESSION[$this->sessionKey]) || !isset($_SESSION[$this->sessionKey][$adId])){
			$_SESSION[$this->sessionKey] = array($adId => 1);
			return true;
		}else{
			if($_SESSION[$this->sessionKey][$adId]===false || intval($_SESSION[$this->sessionKey][$adId])>$freq){
				$_SESSION[$this->sessionKey][$adId]=false;
				return false;
			}else{
				$_SESSION[$this->sessionKey][$adId]+=1;
				return true;
			}
		}
	}

	public function adServe($zoneId,$echo=false){
		$serving=$this->_getAdByZone($zoneId);

		if(count($serving)>0){
			shuffle($serving);
			$serving = $serving[0];
		}else{
			return false;
		}



		if($this->_checkFreq($serving['adId'],$serving['freq'])==false){
			return null;
		}

		$out=str_replace('{url}',$serving['url'],str_replace('{tag}', $serving['tag'], $this->zonePool[$zoneId]['tag']));
		if($echo==false)
			return $out;
		else
			echo $out;
	}

	public function addZone($zoneId,$tag){
		$this->zonePool[$zoneId]=[
			'zoneId'=>intval($zoneId),
			'tag'=>$tag
		];

		return true;
	}

	public function addAd($zoneId,$adId,$tag,$clickUrl,$expiry,$active=false,$freq=1){
		if(!isset($this->zonePool[$zoneId]))
			return false;
		if(date('Ymdhi')>$expiry)
			$active=false;

		$this->adPool[]=[
			'zoneId'=>intval($zoneId),
			'adId'=>$adId,
			'tag'=>$tag,
			'url'=>$clickUrl,
			'freq'=>$freq,
			'active'=>$active
		];
		return true;
	}

	private function _getAdByZone($zoneId,$activeOnly=true){
		$out = [];
		foreach($this->adPool as $ad){
			if($ad['zoneId']==$zoneId){
				if($activeOnly==false || ($activeOnly==true && $ad['active']===true)){
					$out[]=$ad;
				}
			}
		}
		return $out;
	}
}