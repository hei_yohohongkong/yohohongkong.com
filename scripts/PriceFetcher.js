
/* jshint node: true */
"use strict";

if (process.argv.length < 3)
{
	console.error('Usage: ' + process.argv[0] + ' ' + process.argv[1] + ' <price.com.hk product id>');
	process.exit(1);
}

var price_com_hk_product_id = parseInt(process.argv[2]);
if (isNaN(price_com_hk_product_id) || price_com_hk_product_id <= 0)
{
	console.error('Error: Invalid price.com.hk product id: ' + process.argv[2]);
	process.exit(1);
}

var fetchprice = require('./lib/fetchprice');

fetchprice(price_com_hk_product_id, function (err, avg_price, low_price) {
	if (err)
	{
		console.error('Error:', err);
		process.exit(1);
	}
	
	console.log('Average:', avg_price);
	console.log('Lowest:', low_price);
	process.exit(0);
});
