"use strict";

var CONCURRENT_TASK_NUM = 4;
var db = require('./lib/db');
var async = require('async');
var http = require('http');
var event_file = require('../data/flashdeal_date.json');

function gmtime()
{
	return Math.floor((new Date()).getTime() / 1000) - 28800;
}

var now = gmtime();
var event_start = event_file.event_start || Math.floor((new Date("2019-03-01 00:00:00")).getTime() / 1000) - 28800;
var event_end   = event_file.event_end   || Math.floor((new Date("2019-03-14 00:30:00")).getTime() / 1000) - 28800;

if (now < event_start || now > event_end) {
    process.exit(0);
}
var options = {
    host: 'www.yohohongkong.com',
    path: '/api/flashdeal_clean_cache.php',
    method: 'GET'
};
var req1 = http.request(options, (get_res) => {
    const statusCode = get_res.statusCode;

    if (statusCode != 200) {
        console.error(`Error ${statusCode}: ${get_res.statusMessage}.`);
    } else console.log('done');
});
req1.end();

db.conn.query('SELECT * FROM ' + db.table('flashdeal_cart') + ' WHERE order_id = 0 AND expired < ' + now, function(err, res) {
	if (err)
	{
		console.error('Cannot get list of flashdeal cart', err);
		process.exit(1);
    }

    if (res.length == 0) {
        console.log("No record found");
    }

    var remove_session_ids = [];
    var remove_user_ids = [];
    var where_cart = [];
    res.map(function (v) {
        if (remove_session_ids.indexOf(v.session_id) == -1) {
            remove_session_ids.push(v.session_id)
        }
        if (remove_user_ids.indexOf(v.user_id) == -1 && v.user_id != 0) {
            remove_user_ids.push(v.user_id)
        }
    })
    if (remove_session_ids.length > 0) {
        where_cart.push("session_id" + db.create_in(remove_session_ids));
    }
    if (remove_user_ids.length > 0) {
        where_cart.push("user_id" + db.create_in(remove_user_ids));
    }
    async.waterfall([
        function deleteCartGoods(queryCallback) {
            if (where_cart.length > 0) {
                db.conn.query("DELETE FROM " + db.table('cart') + " WHERE (flashdeal_id > 0 AND (" + where_cart.join(" OR ") + ")) OR ( session_id " + db.create_in(remove_session_ids)+" AND is_gift <> 0)", function (error1, result1) {
                    if (error1) {
                        queryCallback('Cannot delete cart: ' + error1);
                    }
                    queryCallback(null);
                });
            } else {
                queryCallback(null);
            }
        },
        function deleteFlashdealCartGoods(queryCallback) {
            var remove_fg_ids = [];
            if (where_cart.length > 0) {
                db.conn.query("SELECT fg_id FROM " + db.table('flashdeal_cart') + " WHERE order_id = 0 AND (" + where_cart.join(" OR ") + ")", function (error2, result2) {
                    if (error2) {
                        queryCallback('Cannot get list of fg_id: ' + error2);
                    }
                    result2.map((v)=>remove_fg_ids.push(v.fg_id));
                    db.conn.query("DELETE FROM " + db.table('flashdeal_cart') + " WHERE order_id = 0 AND (" + where_cart.join(" OR ") + ")", function (error3, result3) {
                        if (error3) {
                            queryCallback('Cannot delete flashdeal_cart: ' + error3);
                        }
                        queryCallback(null, remove_fg_ids);
                    });
                });
            } else {
                queryCallback(null, null);
            }
        },
        function updateFlashdealGoods(remove_fg_ids, queryCallback) {
            if(remove_fg_ids === null){
                queryCallback(null);
            } else if (remove_fg_ids.length > 0) {
                db.conn.query("UPDATE " + db.table('flashdeal_goods') + " SET status = 0 WHERE fg_id " + db.create_in(remove_fg_ids), function (error4, result4) {
                    if (error4) {
                        queryCallback('Cannot update flashdeal_goods: ' + "UPDATE " + db.table('flashdeal_goods') + " SET status = 0 WHERE fg_id " + db.create_in(remove_fg_ids) + ", " + error4);
                    }
                    queryCallback(null);
                });
            } else {
                queryCallback(null);
            }
        },
        function selectExpiredOrder(queryCallback) {
            // get unpaid order (not cancelled / refunded / waiting refund)
            db.conn.query('SELECT fc.order_id FROM ' + db.table('flashdeal_cart') + ' fc ' +
                        'INNER JOIN ' + db.table('order_info') + ' oi ON oi.order_id = fc.order_id ' +
                        'WHERE oi.order_status NOT IN (2, 3, 7) AND oi.pay_status != 2 AND fc.order_id != 0 ' +
                        'AND oi.add_time + 300 < ' + now + ' AND fc.expired < ' + now + ' GROUP BY fc.order_id', function(error5, result5) {
                if (error5) {
                    queryCallback('Cannot get list of expired order: ' + error5);
                }
                if (result5.length == 0) {
                    console.log('expired order is empty.');
                    queryCallback(null);
                }
                var orders = [];
                result5.map((v)=>orders.push(v.order_id));
                if (orders.length > 0) {
                    var options = {
                        host: 'www.yohohongkong.com',
                        path: '/api/flashdeal_remove_order.php?order_ids=' + orders.join(","),
                        method: 'GET',
                        headers: {
                            'X-Auth-Token': 'NueU0nlhh4911uqnE1WlJuofRVOxRs5oqrVAI33gA',
                        }
                    };
                    var req = http.request(options, (get_res) => {
                        const statusCode = get_res.statusCode;

                        if (statusCode != 200) {
                            console.error(`Error ${statusCode}: ${get_res.statusMessage}.`);
                        }
                        get_res.on('data', function (chunk) {
                            queryCallback(null)
                        });
                    });
                    req.end();
                }
            })
        }
    ], function (err) {
        if (err) {
            console.log("ERROR: " + err);
        }
        db.conn.end(function (err) {
            process.exit(0);
        });
    });
})