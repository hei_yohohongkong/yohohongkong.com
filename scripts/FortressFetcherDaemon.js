
/* jshint node: true */
"use strict";

var CONCURRENT_TASK_NUM = 4;

var db = require('./lib/db');
var fetchfortress = require('./lib/fetchfortress');
var log = require('./lib/log');
var async = require('async');

function gmtime()
{
	return Math.floor((new Date()).getTime() / 1000) - 28800;
}

process.on('SIGTERM', function() {
	log.write('[Fortress]', 'SIGTERM received');
});

db.conn.query('SELECT fortress_id, GROUP_CONCAT(goods_id) as goods_id FROM ' + db.table('goods_fortress') + ' GROUP BY fortress_id ORDER BY last_fetch ASC, fortress_id ASC ', function(err, res) {
	if (err)
	{
		console.error('Cannot get list of fortress.com.hk product id', err);
		process.exit(1);
	}

	async.eachLimit(res, CONCURRENT_TASK_NUM, function (row, callback) {
		var fortress_id = row.fortress_id;
		var goods_id = row.goods_id;

		fetchfortress(fortress_id, function (err, price, discount) {
			if (err)
			{
				log.write('[Fortress][' + fortress_id + ']', err).then(function(){
					console.error('fetchfortress error:', err);
					callback(null); // just skip this and continue
					return;
				});
			}
			async.waterfall([
				function updateGoodsFortress(queryCallback){

					// Step 3 : Update goods_fortress table
					if (typeof price !== "undefined") {
						if (typeof discount === "undefined") {
							discount = price;
						}
						var sql = "UPDATE " + db.table('goods_fortress') + " " +
						"SET price = :price, " +
							"discount = :discount, " +
							"last_fetch = :last_fetch " +
						"WHERE fortress_id = :fortress_id ";
						db.conn.query(sql, {
							'price': price,
							'discount': discount,
							'last_fetch': gmtime(),
							'fortress_id': fortress_id
						}, function (err) {
							if (err)
							{
								queryCallback("Step 3: Update goods_fortress table Error"); // just skip this and continue
								return;
							}
							queryCallback(null);
						});
					} else {
						console.log("Error price not found: " + fortress_id)
						// Step 3.1 : Update goods_fortress table last_fetch
						var sql = "UPDATE " + db.table('goods_fortress') + " " +
									"SET last_fetch = :last_fetch " +
									"WHERE fortress_id = :fortress_id ";
						db.conn.query(sql, {
							'last_fetch': gmtime(),
							'fortress_id': fortress_id
						}, function (err) {
							if (err)
							{
								queryCallback("Step 3.1: Update goods_fortress table Error"); // just skip this and continue
								return;
							}
							queryCallback(null);
						});
					}
				}
			], function(err){
				if(err){
					log.write('[Fortress][' + fortress_id + ']', err).then(function(){
						console.log(err);
						callback(null); // just skip this and continue
						return;
					});
				}
				callback(null); // just skip this and continue
			});
		});
	}, function (err) {
		// Done, shutdown mysql connection
		db.conn.end(function (err) {
			if (err) {
				console.log(err)
			}
			// And finally exit
			process.exit(0);
		});
	});
});
