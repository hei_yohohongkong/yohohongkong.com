
/* jshint node: true */
"use strict";

var CONCURRENT_TASK_NUM = 4;

var db = require('./lib/db');
var fetchhktvmall = require('./lib/fetchhktvmall');
var log = require('./lib/log');
var async = require('async');

function gmtime()
{
	return Math.floor((new Date()).getTime() / 1000) - 28800;
}

process.on('SIGTERM', function() {
	log.write('[Fortress]', 'SIGTERM received');
});

db.conn.query('SELECT hktvmall_id, GROUP_CONCAT(goods_id) as goods_id FROM ' + db.table('goods_hktvmall') + ' GROUP BY hktvmall_id ORDER BY last_fetch ASC, hktvmall_id ASC ', function(err, res) {
	if (err)
	{
		console.error('Cannot get list of hktvmall.com product id', err);
		process.exit(1);
	}

	async.eachLimit(res, CONCURRENT_TASK_NUM, function (row, callback) {
		var hktvmall_id = row.hktvmall_id;
		var goods_id = row.goods_id;

		fetchhktvmall(hktvmall_id, function (err, price, discount) {
			if (err)
			{
				log.write('[Hktvmall][' + hktvmall_id + ']', err).then(function() {
					console.error('fetchhktvmall error:', err);
					callback(null); // just skip this and continue
					return;
				})
			}
			async.waterfall([
				function updateGoodsHktvmall(queryCallback){
					if (typeof price !== "undefined") {
						if (typeof discount === "undefined") {
							discount = price;
						}
						// Step 3 : Update goods_hktvmall table
						var sql = "UPDATE " + db.table('goods_hktvmall') + " " +
						"SET price = :price, " +
							"discount = :discount, " +
							"last_fetch = :last_fetch " +
						"WHERE hktvmall_id = :hktvmall_id ";
						db.conn.query(sql, {
							'price': price,
							'discount': discount,
							'last_fetch': gmtime(),
							'hktvmall_id': hktvmall_id
						}, function (err) {
							if (err)
							{
								queryCallback("Step 3: Update goods_hktvmall table Error"); // just skip this and continue
								return;
							}
							queryCallback(null);
						});
					} else {
						// Step 3.1 : Update goods_fortress table last_fetch
						var sql = "UPDATE " + db.table('goods_hktvmall') + " " +
									"SET last_fetch = :last_fetch " +
									"WHERE hktvmall_id = :hktvmall_id ";
						db.conn.query(sql, {
							'last_fetch': gmtime(),
							'hktvmall_id': hktvmall_id
						}, function (err) {
							if (err)
							{
								queryCallback("Step 3.1: Update goods_hktvmall table Error"); // just skip this and continue
								return;
							}
							queryCallback(null);
						});
					}
				}
			], function(err){
				if(err){
					log.write('[Hktvmall][' + hktvmall_id + ']', err).then(function() {
						console.log(err);
						callback(null); // just skip this and continue
						return;
					})
				}
				callback(null); // just skip this and continue
			});
		});
	}, function (err) {
		// Done, shutdown mysql connection
		db.conn.end(function (err) {
			// And finally exit
			process.exit(0);
		});
	});
});
