/* jshint node: true */
'use strict';

var CONCURRENT_TASK_NUM = 4;

var db = require('./lib/db');
var fetchprice = require('./lib/fetchprice');
var fetchhktvmall = require('./lib/fetchhktvmall');
var fetchfortress = require('./lib/fetchfortress');
var async = require('async');

function gmtime()
{
	return Math.floor((new Date()).getTime() / 1000) - 28800;
}

var platforms = ['pricecomhk', 'hktvmall', 'fortress'];
var processing = ['pricecomhk', 'hktvmall', 'fortress'];

if (process.argv.length < 3) {
	console.error('Usage: ' + process.argv[0] + ' ' + process.argv[1] + ' <product id> [<platform>]');
	process.exit(1);
}

var goods_id = parseInt(process.argv[2]);
var selected = '';

if (process.argv.length == 4) {
    selected = process.argv[3];
}

if (!goods_id) {
	console.error('Error: product id not valid');
	process.exit(1);
}
if (platforms.indexOf(selected) == -1 && selected != '') {
	console.error('Error: platform not valid');
	process.exit(1);
}

if (selected != '') {
    processing = [selected];
}
let all_data = {};
processing.map((v) => all_data[v] = {});
async.parallel({
    pricecomhk: function(queryCallback) {
        let platform = 'pricecomhk';
        if (processing.indexOf(platform) != -1) {
            let sql = 'SELECT ' + platform + '_id, is_water FROM ' + db.table('goods_' + platform) + ' WHERE goods_id = :goods_id';
            db.conn.query(sql, {goods_id: goods_id}, function(err, res) {
                if (err) {
                    queryCallback('No result in database', err);
                    return;
                }
                if (res.length == 0) {
                    queryCallback(null)
                    return;
                }
                res.forEach(function(row) {
                    let pricecomhk_id = row.pricecomhk_id;
                    let is_water = row.is_water;
                    fetchprice(pricecomhk_id, is_water, function (err, avg_price, low_price) {
                        if (err)
                        {
                            console.log(JSON.stringify({
                                error: err,
                                platform: platform,
                            }));
                            delete all_data[platform];
                            queryCallback(null); // just skip this and continue
                            return;
                        }
                        all_data[platform][pricecomhk_id] = {
                            price: avg_price,
                            discount: low_price
                        };
                        queryCallback(null);
                    })
                })
            })
        } else {
            delete all_data[platform];
            queryCallback(null);
        }
    },
    hktvmall: function(queryCallback) {
        let platform = 'hktvmall';
        if (processing.indexOf(platform) != -1) {
            let sql = 'SELECT ' + platform + '_id FROM ' + db.table('goods_' + platform) + ' WHERE goods_id = :goods_id';
            db.conn.query(sql, {goods_id: goods_id}, function(err, res) {
                if (err) {
                    queryCallback('No result in database', err);
                    return;
                }
                if (res.length == 0) {
                    queryCallback(null)
                    return;
                }
                res.forEach(function(row) {
                    let hktvmall_id = row.hktvmall_id;
                    fetchhktvmall(hktvmall_id, function (err, price, discount) {
                        if (err)
                        {
                            console.log(JSON.stringify({
                                error: err,
                                platform: platform,
                            }));
                            delete all_data[platform];
                            queryCallback(null); // just skip this and continue
                            return;
                        }
                        all_data[platform][hktvmall_id] = {
                            price: price,
                            discount: discount
                        };
                        queryCallback(null);
                    })
                })
            })
        } else {
            delete all_data[platform];
            queryCallback(null);
        }
    },
    fortress: function(queryCallback) {
        let platform = 'fortress';
        if (processing.indexOf(platform) != -1) {
            let sql = 'SELECT ' + platform + '_id FROM ' + db.table('goods_' + platform) + ' WHERE goods_id = :goods_id';
            db.conn.query(sql, {goods_id: goods_id}, function(err, res) {
                if (err) {
                    queryCallback('No result in database', err);
                    return;
                }
                if (res.length == 0) {
                    queryCallback(null)
                    return;
                }
                res.forEach(function(row) {
                    let fortress_id = row.fortress_id;
                    fetchfortress(fortress_id, function (err, price, discount) {
                        if (err)
                        {
                            console.log(JSON.stringify({
                                error: err,
                                platform: platform,
                            }));
                            delete all_data[platform];
                            queryCallback(null); // just skip this and continue
                            return;
                        }
                        all_data[platform][fortress_id] = {
                            price: price,
                            discount: discount
                        };
                        queryCallback(null)
                    })
                })
            })
        } else {
            delete all_data[platform];
            queryCallback(null);
        }
    },
}, function(err) {
    let sqls = [];
    for (let platform in all_data) {
        for (let id in all_data[platform]) {
            let sql = 'UPDATE ' + db.table('goods_' + platform) + ' SET ' +
                        (platform != 'pricecomhk' ? 'price' : 'avg_price') + ' = "' + all_data[platform][id].price + '", ' +
                        (platform != 'pricecomhk' ? 'discount' : 'low_price') + ' = "' + all_data[platform][id].discount + '", ' +
                        'last_fetch = "' + gmtime() + '" ' +
                        'WHERE ' + platform + '_id = "' + id + '"';
            sqls.push(sql);
        }
    }
    async.each(sqls, function(sql, queryCallback) {
        db.conn.query(sql, function (err) {
            if (err)
            {
                queryCallback("Update table Error: " + sql); // just skip this and continue
                return;
            }
            queryCallback(null);
        })
    }, function(err) {
        if (err) {
            console.log(JSON.stringify({
                error: err,
            }))
        }
        db.conn.end(function (err) {
            console.log(JSON.stringify({
                result: all_data,
            }))
			process.exit(0);
		});
    })
})