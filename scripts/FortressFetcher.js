
/* jshint node: true */
"use strict";

if (process.argv.length < 3)
{
	console.error('Usage: ' + process.argv[0] + ' ' + process.argv[1] + ' <fortress.com.hk product id>');
	process.exit(1);
}

var fortress_com_hk_product_id = parseInt(process.argv[2]);
if (fortress_com_hk_product_id == "")
{
	console.error('Error: Invalid fortress.com.hk product id: ' + process.argv[2]);
	process.exit(1);
}

var fetchfortress = require('./lib/fetchfortress');

fetchfortress(fortress_com_hk_product_id, function (err, price, discount) {
	if (err)
	{
		console.error('Error:', err);
		process.exit(1);
	}
	
	console.log('Price:', price);
	console.log('Discount:', discount);
	process.exit(0);
});
