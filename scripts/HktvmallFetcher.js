
/* jshint node: true */
"use strict";

if (process.argv.length < 3)
{
	console.error('Usage: ' + process.argv[0] + ' ' + process.argv[1] + ' <hktvmall.com product id>');
	process.exit(1);
}

var hktvmall_com_product_id = parseInt(process.argv[2]);
if (hktvmall_com_product_id == "")
{
	console.error('Error: Invalid hktvmall.com product id: ' + process.argv[2]);
	process.exit(1);
}

var fetchhktvmall = require('./lib/fetchhktvmall');

fetchhktvmall(hktvmall_com_product_id, function (err, price, discount) {
	if (err)
	{
		console.error('Error:', err);
		process.exit(1);
	}
	
	console.log('Price:', price);
	console.log('Discount:', discount);
	process.exit(0);
});
