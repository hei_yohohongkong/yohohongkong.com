
/* jshint node: true */
"use strict";

var CONCURRENT_TASK_NUM = 4;

var db = require('./lib/db');
var fetchprice = require('./lib/fetchprice');
var log = require('./lib/log');
var async = require('async');

function gmtime()
{
	return Math.floor((new Date()).getTime() / 1000) - 28800;
}

function arrayUnique(array) {
    var a = array.concat();
    for(var i=0; i<a.length; ++i) {
        for(var j=i+1; j<a.length; ++j) {
            if(a[i] === a[j])
                a.splice(j--, 1);
        }
    }
    return a;
}

process.on('SIGTERM', function() {
	log.write('[Pricecomhk]', 'SIGTERM received');
});

db.conn.query('SELECT pricecomhk_id, GROUP_CONCAT(goods_id) as goods_id, is_water FROM ' + db.table('goods_pricecomhk') + ' GROUP BY pricecomhk_id ORDER BY last_fetch ASC, pricecomhk_id ASC ', function(err, res) {
	if (err)
	{
		console.error('Cannot get list of price.com.hk product id', err);
		process.exit(1);
	}

	async.eachLimit(res, CONCURRENT_TASK_NUM, function (row, callback) {
		var pricecomhk_id = row.pricecomhk_id;
		var goods_id = row.goods_id;
		var is_water = row.is_water;
		
		fetchprice(pricecomhk_id, is_water, function (err, avg_price, low_price, keywords) {
			if (err)
			{
				log.write('[Pricecomhk][' + pricecomhk_id + ']', err).then(function() {
					console.error('fetchprice error:', err);
					callback(null); // just skip this and continue
					return;
				})
			}

			async.waterfall([
				function getGoodsKeywords(queryCallback){

					// Step 1 : getGoodsKeywords
					db.conn.query('SELECT keywords FROM ' + db.table('goods') + ' WHERE goods_id IN ('+goods_id+')', function(err, res) {
						if(err || res.length <= 0){
							queryCallback("Error Step 1 : getGoodsKeywords :"+goods_id);
							return;
						}
						var k = (typeof res[0].keywords === 'undefined') ? "" : res[0].keywords; // Our goods keywords
						var goods_keywords = k.split(" ");
		
						var p = (typeof keywords === 'undefined') ? [] : keywords.split(","); // pricecomhk keywords
						p.splice(0, 2);
						var price_keywords = [];
		
						// merge pricecomhk keywords with our goods keywords
						p.forEach(function(element) {
							var value = element.split(" ");
							price_keywords = price_keywords.concat(value);
						});
						// Remove duplicate elements
						var new_keywords = arrayUnique(goods_keywords.concat(price_keywords));
						// Update goods table
						var keywords_string = new_keywords.join(" ");

						queryCallback(null, keywords_string);
					});
				},
				function updateGoodsKeywords(keywords_string, queryCallback){
					if(keywords_string === null){
						queryCallback("NULL keywords_string");
						return;
					}
					// Step 2 : updateGoodsKeywords
					var sql = "UPDATE " + db.table('goods') + " " +
					"SET keywords = :keywords_string "+
					"WHERE goods_id IN ( :goods_id ) ";
					db.conn.query(sql, {
						'keywords_string': keywords_string,
						'goods_id': goods_id
					}, function (err) {
						if (err)
						{
							// console.error('update error:', err);
							queryCallback("Error Step 2 : updateGoodsKeywords: "+goods_id); // just skip this and continue
							return;
						}
						queryCallback(null);
					});
				},
				function updateGoodsPricecomhk(queryCallback){
					// Step 3 : Update goods_pricecomhk table
					var sql = "UPDATE " + db.table('goods_pricecomhk') + " " +
					"SET avg_price = :avg_price, " +
						"low_price = :low_price, " +
						"last_fetch = :last_fetch " +
					"WHERE pricecomhk_id = :pricecomhk_id ";
					db.conn.query(sql, {
						'avg_price': avg_price,
						'low_price': low_price,
						'last_fetch': gmtime(),
						'pricecomhk_id': pricecomhk_id
					}, function (err) {
						if (err)
						{
							queryCallback("Step 3: Update goods_pricecomhk table Error"); // just skip this and continue
							return;
						}
						queryCallback(null);
					});
				}
			], function(err){
				if(err){
					log.write('[Pricecomhk][' + pricecomhk_id + ']', err).then(function() {
						console.log(err);
						callback(null); // just skip this and continue
						return;
					})
				}
				callback(null); // just skip this and continue
			});
		});
	}, function (err) {
		// Done, shutdown mysql connection
		db.conn.end(function (err) {
			// And finally exit
			process.exit(0);
		});
	});
});
