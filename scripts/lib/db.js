
/* jshint node: true */
"use strict";

var path = require('path');
var fs = require('fs');
var mysql = require('mysql');

var config_php = fs.readFileSync(path.resolve(__dirname, '../../data/config.php'), 'utf-8');

var getPhpVariable = function(php_code, variable_name)
{
	var regex = new RegExp('\\$' + variable_name + '\\s*=\\s*(["\'])((?:[^\\1\\\\]|\\\\.)*?)\\1', '');
	var matches = php_code.match(regex);
	return (matches === null) ? '' : matches[2];
};

var db_host = getPhpVariable(config_php, 'db_host');
var db_name = getPhpVariable(config_php, 'db_name');
var db_user = getPhpVariable(config_php, 'db_user');
var db_pass = getPhpVariable(config_php, 'db_pass');
var prefix = getPhpVariable(config_php, 'prefix');

var conn = mysql.createConnection({
	'host': db_host,
	'user': db_user,
	'password': db_pass,
	'database': db_name
});

conn.config.queryFormat = function (query, values) {
	if (!values) return query;
	return query.replace(/\:(\w+)/g, function (txt, key) {
		if (values.hasOwnProperty(key)) {
			return this.escape(values[key]);
		}
		return txt;
	}.bind(this));
};

module.exports = {
	'conn': conn,
	'escape': conn.escape,
	'prefix': prefix,
	'table': function (table_name) {
		return '`' + db_name + '`.`' + prefix + table_name + '`';
	},
	'create_in': function (in_array) {
		return ' IN ("' + in_array.join('", "') + '") ';
	}
};
