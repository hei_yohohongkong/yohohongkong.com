var path = require('path');
var fs = require('fs');

var write = function(msg, error) {
    var parsed_error = "";
    if (typeof error === "undefined") {
    } else if (typeof error !== "string") {
        parsed_error = JSON.stringify(error);
    } else {
        parsed_error = error;
    }
    var log = '[' + new Date().toString().split(" GMT")[0] + ']' + msg + ' ' + parsed_error;

    return new Promise(function (resolve, reject) {
        fs.appendFile(path.resolve(__dirname, '../../data/node_cron_error.log'), log + "\n", function(err) {
            if (err) reject(err);
            resolve();
        })
    })
}

module.exports = {
    'write': write,
}