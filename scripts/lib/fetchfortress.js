
/* jshint node: true */
"use strict";

var path = require('path');
var fs = require('fs');
var jsdom = require('jsdom');

var jQuerySrc = fs.readFileSync(path.resolve(__dirname, '../../yohohk/js/jquery.1.11.3.min.js'), 'utf-8');

// List of top user agents extracted from logs as of 2016-01-16
// var userAgents = [
// 	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36',
// 	'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36',
// 	'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko',
// 	'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36',
// 	'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0',
// 	'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36',
// 	'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36',
// 	'Mozilla/5.0 (Windows NT 6.1; rv:43.0) Gecko/20100101 Firefox/43.0',
// 	'Mozilla/5.0 (Windows NT 6.1; Trident/7.0; rv:11.0) like Gecko'
// ];

// List of top user agents extracted from logs as of 2016-05-27
var userAgents = [
	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',
	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36',
	'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko',
	'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36',
	'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',
	'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0',
	'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',
	'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36',
	'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36',
	'Mozilla/5.0 (Windows NT 6.1; Trident/7.0; rv:11.0) like Gecko',
	'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.105',
	'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/601.5.17 (KHTML, like Gecko) Version/9.1 Safari/601.5.17',
];

module.exports = function (product_id, callback) {
	jsdom.env({
		'url': 'https://www.fortress.com.hk/zt/product/p/' + product_id,
		'userAgent': userAgents[Math.floor(Math.random() * userAgents.length)],
		//'referrer': '',
		'src': [jQuerySrc],
		'done': function (err, window) {
			if (err)
			{
				callback(err);
				return;
			}
			
			try
			{
				// Get jQuery from window
				var $ = window.$;
				
				var price = 0;
				var discount = 0;
				
				var product_id_number = product_id;
				if (product_id.match(/\d+/g)) {
					product_id_number = product_id.match(/\d+/g)[0];
				}
				var regex_min = new RegExp("priceMinList\\['" + product_id_number + "'\\] = 'HK\\$((\\d)+(,\\d+)*)';");
				var regex_max = new RegExp("priceMaxList\\['" + product_id_number + "'\\] = 'HK\\$((\\d)+(,\\d+)*)';");

				$("script").each(function(){
					var min = $(this).html().match(regex_min);
					var max = $(this).html().match(regex_max);
					if (min) {
						discount = parseFloat($.trim(min[1].replace(/[^0-9.]/g, '')));
					}
					if (max) {
						price = parseFloat($.trim(max[1].replace(/[^0-9.]/g, '')));
					}
				})
				
				// // Skip if price not valid
				if (isNaN(price) || price <= 0) {
					callback("No price found");
					return;
				} else {
					if (isNaN(discount) || discount <= 0) {
						// Set discount as price if invalid or no discount
						discount = price;
					}
					callback(null, price, discount);
				}

			}
			catch (err)
			{
				callback(err);
			}
		}//,
		//'virtualConsole': jsdom.createVirtualConsole().sendTo(console)
	});
};
