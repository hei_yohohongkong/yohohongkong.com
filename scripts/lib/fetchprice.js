
/* jshint node: true */
"use strict";

var path = require('path');
var fs = require('fs');
var jsdom = require('jsdom');

var jQuerySrc = fs.readFileSync(path.resolve(__dirname, '../../yohohk/js/jquery.1.11.3.min.js'), 'utf-8');

// List of top user agents extracted from logs as of 2016-01-16
// var userAgents = [
// 	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36',
// 	'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36',
// 	'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko',
// 	'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36',
// 	'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0',
// 	'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36',
// 	'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36',
// 	'Mozilla/5.0 (Windows NT 6.1; rv:43.0) Gecko/20100101 Firefox/43.0',
// 	'Mozilla/5.0 (Windows NT 6.1; Trident/7.0; rv:11.0) like Gecko'
// ];

// List of top user agents extracted from logs as of 2016-05-27
var userAgents = [
	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',
	'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36',
	'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko',
	'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36',
	'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',
	'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0',
	'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36',
	'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36',
	'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36',
	'Mozilla/5.0 (Windows NT 6.1; Trident/7.0; rv:11.0) like Gecko',
	'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.105',
	'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/601.5.17 (KHTML, like Gecko) Version/9.1 Safari/601.5.17',
];

module.exports = function (product_id, is_water, callback) {
	jsdom.env({
		'url': 'http://www.price.com.hk/product.php?p=' + product_id,
		'userAgent': userAgents[Math.floor(Math.random() * userAgents.length)],
		//'referrer': '',
		'src': [jQuerySrc],
		'done': function (err, window) {
			if (err)
			{
				callback(err);
				return;
			}
			
			try
			{
				// Get jQuery from window
				var $ = window.$;
				
				var price_sum = 0;
				var price_count = 0;
				var price_lowest = -1;
				
				var one_month_ago = new Date();
				one_month_ago.setMonth(one_month_ago.getMonth() - 1);
				
				$('.product-list-item-list .item-inner').each(function () {
					//var quote_date_str = $.trim($(this).find('.quote-shop-date span').text());
					//var quote_date_str = $.trim($(this).find('.quote-source p').text());
					var quote_date_str = $.trim($(this).find('.quote-source div').first().text());
					var quote_date;
					try
					{
						if (quote_date_str.indexOf('超過') != -1) return;
						
						if (quote_date_str.indexOf('日前') != -1)
						{
							quote_date_str = quote_date_str.substr(0, quote_date_str.indexOf('日前'));
							quote_date = new Date();
							quote_date.setDate(quote_date.getDate() - parseInt(quote_date_str));
						}
						else
						{
							quote_date_str = quote_date_str.match("([0-9-]+)")[1];
							quote_date = new Date(quote_date_str);
						}
					}
					catch (err)
					{
						// Cannot parse date
						return;
					}
					
					// Skip if date not valid
					if (quote_date.toString() == 'Invalid Date') return;
					
					// Skip if more than 1 month ago
					if (quote_date.getTime() < one_month_ago.getTime()) return;
					
					var price_str = $.trim($(this).find('.quote-price-' + (is_water ? 'water' : 'hong') + ' .product-price .text-price-number').text());
					var price = parseFloat(price_str.replace(/[^0-9.]/g, ''));
					
					// Skip if price not valid
					if (isNaN(price) || price <= 0) return;
					
					//console.log(quote_date, price);
					price_sum += price;
					price_count++;
					
					if (price_lowest == -1 || price < price_lowest)
					{
						price_lowest = price;
					}
				});
				
				// Prevent division by zero error
				var price_average = (price_count > 0) ? (price_sum / price_count) : -1;
				var keywords = $('meta[name=keywords]').attr("content");

				callback(null, price_average, price_lowest, keywords);
			}
			catch (err)
			{
				callback(err);
			}
		}//,
		//'virtualConsole': jsdom.createVirtualConsole().sendTo(console)
	});
};
