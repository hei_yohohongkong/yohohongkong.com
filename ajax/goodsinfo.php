<?php

/***
 * goods_info.php
 * by howang 2014-05-28
 *
 * Retrieve goods warranty & description
 ***/

define('IN_ECS', true);

// Skip session and template engine for faster loading
define('INIT_NO_USERS', true);
define('INIT_NO_SMARTY', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');

if (!empty($_GET['id']))
{
	$goods_id = (int) $_GET['id'];
}
else if (!empty($_POST['itemId']))
{
	$goods_id = (int) $_POST['itemId'];
}
else
{
	$goods_id = -1;
}

if ($goods_id > 0)
{
	$sql = "SELECT goods_desc as `desc`, seller_note as warranty, goods_thumb, goods_name, goods_id FROM " . $ecs->table('goods') . " WHERE goods_id = " . $goods_id . " LIMIT 1";
	$data = $db->getRow($sql);
	$result = array_merge(array('status' => 1), $data);
}
else
{
	$result = array('status' => 0);
}

header('Content-Type: application/json');
echo json_encode($result);
exit;

?>