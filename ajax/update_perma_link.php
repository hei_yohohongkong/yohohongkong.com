<?php

if (!defined('IN_ECS')) {
    define('IN_ECS', true);
}
require_once dirname(dirname(__FILE__)) . '/includes/init.php';

$output = [
    'error'     => 1,
    'message'   => 'Invalid Request'
];
if ($_REQUEST['act'] == 'test') {
    $link = trim($_REQUEST['link']);
    if (empty($link)) {
        $output['message'] = "請輸入鏈結";
    } else {
        $output = [
            'error'     => 0,
            'content'   => [
                'origin' => $link,
                'encode' => uri_encode($link)
            ]
        ];
    }
}

header('Content-Type: application/json');
echo json_encode($output);
exit;

?>