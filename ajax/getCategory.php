<?php
/***
* getCategory.php
*
* pc & mobile 全部產品分類
  AJAX get level 2 and level 3 category according level 1 category id
***/

define('IN_ECS', true);

// Skip template engine for faster loading
define('INIT_NO_SMARTY', true);

// If the user is already logged in, we will replace the session with new login session, 
// so updating user info for logged in user here is meaningless
define('INIT_NO_UPDATE_USER_INFO', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');
$categoryController = new Yoho\cms\Controller\CategoryController();
$cat_id = $_POST["cat_id"];
$brand = isset($_POST["brand"]) ? false : true ;
$result = array();
if ($brand) {
    //pc需要顯示品牌
    $brand_sql = "SELECT cb.`cat_id`, b.brand_id, b.brand_name, b.brand_logo, bperma, cperma " .
                "FROM " . $GLOBALS['ecs']->table('category_brand') . " as cb " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('brand') . " as b ON cb.brand_id = b.brand_id " .
                "LEFT JOIN (SELECT id, table_name, lang, perma_link as bperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") bpl ON bpl.id = cb.brand_id  AND bpl.table_name = 'brand' AND bpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
                "LEFT JOIN (SELECT id, table_name, lang, perma_link as cperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") cpl ON cpl.id = cb.cat_id  AND cpl.table_name = 'category' AND cpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
                "WHERE cb.cat_id = '".$cat_id."' " .
                "ORDER BY cb.`cat_id` ASC, cb.`sort_order` ASC";
    $brand_res = $GLOBALS['db']->getAll($brand_sql);
    $brand_res = localize_db_result('brand', $brand_res);

    foreach ($brand_res AS $b_key => $b_row)
    {
        $b_row['url'] = build_uri('category', array('cid' => $b_row['cat_id'], 'cname' => $options[$b_row['cat_id']]['name'], 'cperma' => $b_row['cperma'], 'bid' => $b_row['brand_id'], 'bname' => $b_row['brand_name'], 'bperma' => $b_row['bperma']), $b_row['brand_name']);
        $result['brands'][] = $b_row;
    }
    //mobile不需要顯示每個cat的圖片
    $level2_cat = $categoryController->getSubCategories($cat_id);
} else{
    //mobile需要顯示每個cat的圖片
    $level2_cat = $categoryController->getSubCategories($cat_id, $build_cat_url = true, $cat_img = true);
}

foreach($level2_cat as $key => $cat)
{
    $level2_cat_id = $cat['cat_id'];
    if ($brand) {
        $level3_cat = $categoryController->getSubCategories($level2_cat_id);
    }else{
        //mobile需要顯示每個cat的圖片
        $level3_cat = $categoryController->getSubCategories($level2_cat_id, $build_cat_url = true, $cat_img = true);
    }
    $level2_cat[$key]['children'] = $level3_cat;
}
$result["bottom_cat"] = $level2_cat;

header('Content-Type: application/json');
echo json_encode($result);
exit;
?>