<?php

/***
* ajaxuser.php
*
* AJAX user login
***/

define('IN_ECS', true);

// Skip template engine for faster loading
define('INIT_NO_SMARTY', true);

// If the user is already logged in, we will replace the session with new login session, 
// so updating user info for logged in user here is meaningless
define('INIT_NO_UPDATE_USER_INFO', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');

$reference = get_affiliate();

$array['reference'] = $reference;

// JSONP
if (!empty($_REQUEST['callback']))
{
	header('Content-Type: application/javascript');
	echo $_REQUEST['callback'] . '(' . json_encode($array) . ');';
}
else
{
	header('Content-Type: application/json');
	echo json_encode($array);
}
exit;

?>