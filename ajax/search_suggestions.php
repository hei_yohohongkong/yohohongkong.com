<?php

/***
 * search_suggestions.php
 * by howang 2014-05-26
 *
 * Suggests goods name based on the search query that user typed into the search box
 ***/

define('IN_ECS', true);

// Skip session and template engine for faster loading
define('INIT_NO_USERS', true);
define('INIT_NO_SMARTY', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');

if (!empty($_GET['q']))
{
	$val = $_GET['q'];
	
	$val = mysql_like_quote(trim($val));
	$where = "(g.goods_name LIKE '%$val%' OR g.goods_sn LIKE '%$val%' OR g.keywords LIKE '%$val%')";
	$where = "($where OR (gl.goods_name LIKE '%$val%' OR gl.keywords LIKE '%$val%'))";
	
	require_once(ROOT_PATH . 'includes/lib_order.php');
	$sql  = "SELECT goods_name, " . hw_goods_in_stock_sql() . " FROM (".
			"SELECT IFNULL(gl.goods_name, g.goods_name) as goods_name, " .
				"g.is_pre_sale, g.pre_sale_date, g.pre_sale_days, g.shop_price, ".
				hw_goods_number_subquery('g.') . ", " .
				hw_reserved_number_subquery('g.') . " " .
			"FROM " . $ecs->table('goods') . " as g " .
			"LEFT JOIN " . $ecs->table('goods_lang') . " as gl ON g.goods_id = gl.goods_id AND gl.lang = '" . $_CFG['lang'] . "' " .
			"WHERE g.is_delete = 0 AND g.is_hidden = 0 AND g.is_on_sale = 1 AND g.is_alone_sale = 1 AND $where " .
			"GROUP BY g.goods_id " .
			"ORDER BY g.shop_price DESC " .
			") as tmp ORDER BY in_stock DESC, shop_price DESC LIMIT 0, 10";
	$result = $db->getCol($sql);
}
else
{
	if (!empty($_CFG['search_keywords']))
	{
		$result = explode(',', trim($_CFG['search_keywords']));
	}
	else
	{
		$result = array();
	}
}

if (!empty($_REQUEST['m']))
{
	$i = 0;
	$result = array_map(function ($name) use (&$i) {
		return array(
			'name' => $name,
			'sort' => $i++,
		);
	}, $result);
}

header('Content-Type: application/json');
echo json_encode($result);
exit;

?>