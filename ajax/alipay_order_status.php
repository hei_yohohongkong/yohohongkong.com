<?php

/***
 * alipay_order_status.php
 * by billy 2019-06-05
 *
 * AJAX endpoint for checking Alipay payment status (HK and CN Wallet)
 ***/

define('IN_ECS', true);

// Skip template engine for faster loading
define('INIT_NO_SMARTY', true);
// Skip update user info for faster loading
define('INIT_NO_UPDATE_USER_INFO', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');
require(dirname(dirname(__FILE__)) . '/includes/lib_order.php');
require(dirname(dirname(__FILE__)) . '/includes/lib_payment.php');
$alipayController = new Yoho\cms\Controller\AlipayController();
$orderController = new Yoho\cms\Controller\OrderController();
global $db;

$payment_info = get_payment($_REQUEST['wallet']);
$user_id = $_SESSION['user_id'];
$host_name = '';
$sign_key = '';
$partner_id = '';
$wallet_type = $_REQUEST['wallet'];
if ($wallet_type == 'alipayhk') {
    $host_name = $payment_info['alipayhk_api_base_gateway'];
    $sign_key = $payment_info['alipayhk_md5_signature_key'];
    $partner_id = $payment_info['alipayhk_merchant_pid'];
} elseif ($wallet_type == 'alipaycn') {
    $host_name = $payment_info['alipaycn_api_base_gateway'];
    $sign_key = $payment_info['alipaycn_md5_signature_key'];
    $partner_id = $payment_info['alipaycn_merchant_pid'];
} else {
    return;
}

// Commented out for postman debug
if (!$user_id)
{
	header('Content-Type: application/json');
	echo json_encode(array(
		'status' => 0,
		'error' => '請先登入',
	));
	exit;
}

if (empty($_REQUEST['order_payment_id']) && empty($_REQUEST['gateway_transaction_sn'])) {
    echo json_encode([
        'status' => 0,
        'error' => 'Order Payment ID or Gateway Transaction SN should be provided'
    ]);
    exit;
}

if (!empty($_REQUEST['order_payment_id']) && !empty($_REQUEST['gateway_transaction_sn'])) {
    echo json_encode([
        'status' => 0,
        'error' => 'Either Order Payment ID or Gateway Transaction SN, but not both should be provided'
    ]);
    exit;
}

if (empty($host_name) || empty($sign_key)) {
    echo json_encode([
        'status' => 0,
        'error' => 'Invalid Wallet type input'
    ]);
    exit;
}

$parameter = [
    'service' => 'single_trade_query',
    'partner' => $partner_id,
    '_input_charset' => 'UTF-8',
];

if (!empty($_REQUEST['order_payment_id']) && empty($_REQUEST['gateway_transaction_sn'])) {
    $parameter['out_trade_no'] = $_REQUEST['order_payment_id'];
}

if (empty($_REQUEST['order_payment_id']) && !empty($_REQUEST['gateway_transaction_sn'])) {
    $parameter['trade_no'] = $_REQUEST['gateway_transaction_sn'];
}

$sign  = '';
ksort($parameter);
reset($parameter);
foreach ($parameter AS $key => $val)
{
    $sign  .= ecs_iconv(EC_CHARSET, 'utf-8', $key) . "=" . ecs_iconv(EC_CHARSET, 'utf-8', $val). "&";
}

$sign  = substr($sign, 0, -1). ecs_iconv(EC_CHARSET, 'utf-8', $sign_key);

$parameter['sign_type'] = 'MD5';
$parameter['sign'] = md5($sign);

$ch = curl_init($host_name);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($parameter));
curl_setopt($ch, CURLOPT_HTTPHEADER, [
    "Content-Type: application/x-www-form-urlencoded",
    "Content-Length: " . strlen(http_build_query($parameter)),
]);

$query_result = curl_exec($ch);

if (curl_errno($ch) != 0) // cURL error
{
    hw_error_log('Cannot connect to Alipay: ' . curl_error($ch));
    curl_close($ch);
    echo json_encode([
        'status' => 0,
        'error' => 'Query failed: Unable to connect to Payment Service'
    ]);
    exit;
}
else
{
    curl_close($ch);
    
    $query_result = simplexml_load_string($query_result);
    $query_result_data = json_encode($query_result, JSON_UNESCAPED_UNICODE);

    if ($query_result->is_success = 'F' && !empty($query_result->error)) {
        echo json_encode([
            'status' => 0,
            'error' => 'Query failed: ' . $query_result->error
        ]);
        exit;
    }

    $trade_info = $query_result->response->trade;

    foreach ($trade_info AS $key=>$val)
    {
        if ($key != 'sign' && $key != 'sign_type' && $key != 'code')
        {
            $sign .= "$key=$val&";
        }
    }

    $sign = substr($sign, 0, -1) . $sign_key;

    if ($query_result->sign_type != 'MD5' && (md5($sign) != $query_result->sign))
    {
        echo json_encode([
            'status' => 0,
            'error' => 'Signature Error'
        ]);
    }
    $trade_info->addChild("wallet_type", $wallet_type);
    $trade_info->addChild('currency', $wallet_type == 'alipaycn' ? "CNY" : "HKD");

    $log_id = substr(explode('_', trim($trade_info->out_trade_no))[0], 13);

    if ($trade_info->trade_status == 'TRADE_FINISHED') {
        if (!empty($log_id)) {
            $log = $alipayController->getPaymentLogByLogId($log_id);
            // TODO: cn wallet to hkd amount
            $orderController->order_payment_transaction_log($log_id, $payment_info['pay_id'], $trade_info->trade_no, 'HKD', $log['order_amount'],0);
        
        }
        $action_note = $payment_info['pay_code'] . ':' . $trade_info->trade_no . "（HKD $log[order_amount]）";
    
        // TODO: Insert returned data into database
        // TODO: Prevent double update (Updated by polling, then by notification, or vice versa)
        $alipayController->insertQueryResult($trade_info, $query_result_data);
    }

    $msg = 'FAILURE';
    $status_code = 0;

    if ($trade_info->trade_status == 'TRADE_FINISHED') {
        $status_code = 1;
        $msg = 'COMPLETE';
        order_paid($log_id,PS_PAYED,$action_note);
    } elseif ($trade_info->trade_statusd == 'WAIT_BUYER_PAY') {
        $status_code = 1;
        $msg = 'PENDING_PAYMENT';
    }

    header('Content-Type: application/json');
    echo json_encode([
        'status' => $status_code,
        'pay_status' => $msg,
        'data' => $query_result_data
    ]);
    exit;
}


?>