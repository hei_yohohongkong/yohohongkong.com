<?php
/***
* get_tos_article_content.php
*
* AJAX get tos or privacy content
***/

define('IN_ECS', true);

// Skip template engine for faster loading
define('INIT_NO_SMARTY', true);

// If the user is already logged in, we will replace the session with new login session, 
// so updating user info for logged in user here is meaningless
define('INIT_NO_UPDATE_USER_INFO', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');

require_once(ROOT_PATH . 'includes/lib_passport.php');
$title = $_POST["title"];
if($title=="tos"){
    $array= get_tos_article_content();
}
else{
    $array= get_privacy_article_content();
}


header('Content-Type: application/json');
echo json_encode($array);
exit;
?>