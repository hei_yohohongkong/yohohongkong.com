<?php

/***
 * wish_list.php
 * by howang 2014-08-22
 *
 * Wish list
 ***/

define('IN_ECS', true);

// Skip template engine for faster loading
define('INIT_NO_SMARTY', true);
// Skip update user info for faster loading
define('INIT_NO_UPDATE_USER_INFO', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');

$act = empty($_REQUEST['act']) ? '' : trim($_REQUEST['act']);

if (in_array($act, array('add','remove')))
{
	$goods_id = (empty($_POST['goods_id'])) ? '' : (int)$_POST['goods_id'];
	$user_id = empty($_SESSION['user_id']) ? 0 : intval($_SESSION['user_id']);
	
	if (empty($goods_id))
	{
		$result = array('status' => 0, 'error' => _L('wishlist_error_no_goods_id', '請選擇產品'));
	}
	else if ($user_id <= 0)
	{
		$result = array('status' => 0, 'error' => _L('wishlist_error_not_logged_in', '請先登入'), 'showlogin' => 1);
	}
	else
	{
		if ($act == 'add')
		{
			$sql = "INSERT INTO " . $ecs->table('wish_list') .
					" (`user_id`, `goods_id`, `add_time`)".
					" VALUES ('" . $user_id . "','" . $goods_id . "', '" . gmtime() . "')".
					" ON DUPLICATE KEY UPDATE `add_time` = '" . gmtime() . "'";
			$db->query($sql);
		}
		else if ($act == 'remove')
		{
			$sql = "DELETE FROM " . $ecs->table('wish_list') .
					" WHERE `user_id` = '" . $user_id . "' AND `goods_id` = '" . $goods_id . "' LIMIT 1";
			$db->query($sql);
		}
		
		$result = array('status' => 1);
	}
	
	header('Content-Type: application/json');
	echo json_encode($result);
	exit;
}
?>