<?php

/***
 * findpwd.php
 * by howang 2014-08-18
 *
 * Reset password function
 ***/

define('IN_ECS', true);

// Reset password page is for non-logged-in users, so we don't update user info here
define('INIT_NO_UPDATE_USER_INFO', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');

define('FINDPWD_STATUS_FAIL', 0);
define('FINDPWD_STATUS_SUCCESS', 1);
define('FINDPWD_STATUS_DEFAULT_PASSWORD', 2);
define('FINDPWD_STATUS_ENTER_EMAIL', 3);

define('RESETPWD_STATUS_FAIL', 0);
define('RESETPWD_STATUS_SUCCESS', 1);

if ($_REQUEST['act'] == 'checkUser')
{
	$type = isset($_POST['type']) ? trim($_POST['type']) : '';
	$val  = isset($_POST['val']) ? trim($_POST['val']) : '';
	
	$result = check_and_reset_password($type, $val);
}
elseif ($_REQUEST['act'] == 'resetPassword')
{
	$new_password = isset($_POST['new_password']) ? trim($_POST['new_password']) : '';
	$user_id	      = isset($_POST['uid']) ? intval($_POST['uid']) : 0;
	$code         = isset($_POST['code']) ? trim($_POST['code']) : '';
	
	$result = reset_password($user_id, $code, $new_password);
}
else
{
	$result = array('status' => 0, 'error' => '參數錯誤');
}

header('Content-Type: application/json');
echo json_encode($result);
exit;

function check_and_reset_password($type, $val)
{
	global $db, $ecs;
	
	if ($type == 'mobile')
	{
		if (!preg_match('/^[0-9]+$/', $val))
		{
			return array(
				'status' => FINDPWD_STATUS_FAIL,
				'error' => sprintf(_L('account_format_invalid', '%s格式有誤，再修改一下哦！'), _L('account_username', '手提電話號碼'))
			);
		}
		else
		{
			$ccc = isset($_POST['ccc']) ? trim($_POST['ccc']) : '';
			if (!empty($ccc))
			{
				$val = '+' . $ccc . '-' . $val;
			}
			$where = "`user_name` = '" . $val . "'";
		}
	}
	elseif ($type == 'email')
	{
		if (!is_email($val))
		{
			return array(
				'status' => FINDPWD_STATUS_FAIL,
				'error' => sprintf(_L('account_format_invalid', '%s格式有誤，再修改一下哦！'), _L('account_email', '電郵地址'))
			);
		}
		else
		{
			$where = "`email` = '" . $val . "'";
		}
	}
	else
	{
		return array('status' => FINDPWD_STATUS_FAIL, 'error' => '請選擇驗證方式');
	}
	
	$sql = "SELECT `user_id`, `email`, `user_name`, `password`, `ec_salt` FROM " . $ecs->table('users') . " WHERE " . $where;
	$row = $db->getRow($sql);
	
	if (!$row)
	{
		return array('status' => FINDPWD_STATUS_FAIL, 'error' => _L('account_reset_password_not_exists', '帳戶並不存在'));
	}
	
	$user_id = $row['user_id'];
	$email = $row['email'];
	$user_name = $row['user_name'];
	$password = $row['password'];
	$ec_salt = $row['ec_salt'];
	
	if (($password == md5('yohohongkong')) && ($ec_salt === null))
	{
		return array('status' => FINDPWD_STATUS_DEFAULT_PASSWORD);
	}
	
	if ((empty($email)) || (is_placeholder_email($email)))
	{
		return array(
			'status' => FINDPWD_STATUS_FAIL,
			'error' => _L('account_reset_password_missing_email', '您的帳戶還沒有輸入電子郵件地址，若要重設密碼請聯絡友和')
		);
	}
	
	if ($type == 'mobile')
	{
		// Mobile phone number vertified, ask user to enter email address
		// return array('status' => FINDPWD_STATUS_ENTER_EMAIL);
		// Only send email if user entered the email himself
		if (send_reset_password_email($user_id, $user_name, $email))
		{
			return array('status' => FINDPWD_STATUS_SUCCESS, 'email' =>$email);
		}
		else
		{
			return array(
				'status' => FINDPWD_STATUS_FAIL,
				'error' => _L('account_reset_password_email_send_failed', '發送驗證電郵失敗，請聯絡友和')
			);
		}
	}
	elseif ($type == 'email')
	{
		// Only send email if user entered the email himself
		if (send_reset_password_email($user_id, $user_name, $email))
		{
			return array('status' => FINDPWD_STATUS_SUCCESS, 'email' =>$email);
		}
		else
		{
			return array(
				'status' => FINDPWD_STATUS_FAIL,
				'error' => _L('account_reset_password_email_send_failed', '發送驗證電郵失敗，請聯絡友和')
			);
		}
	}
	
	// Shouldn't happen
	return array('status' => RESETPWD_STATUS_FAIL, 'error' => '參數錯誤');
}

function reset_password($user_id, $code, $new_password)
{
	global $user, $_CFG, $db, $ecs;

	include_once(ROOT_PATH . 'includes/lib_passport.php');

	if (strlen($new_password) < 6)
	{
		return array('status' => RESETPWD_STATUS_FAIL, 'error' => _L('account_password_too_short', '密碼不能少於 6 個字元'));
	}

	$user_info = $user->get_profile_by_id($user_id); //论坛记录

	if ($user_info && (!empty($code) && verify_password_reset_token($user_id, $code)))
	{
		if ($user->edit_user(array('username' => $user_info['user_name'], 'old_password' => null, 'password' => $new_password), 1))
		{
			$sql = "UPDATE " . $ecs->table('users') . "SET `ec_salt`='', `salt`='0', `passchg_time`='".gmtime()."' WHERE user_id = '".$user_id."'";
			$db->query($sql);
			$user->logout();
			return array('status' => RESETPWD_STATUS_SUCCESS);
		}
		else
		{
			return array('status' => RESETPWD_STATUS_FAIL, 'error' => _L('account_reset_password_failed', '修改密碼失敗，請稍後再試。'));
		}
	}
	else
	{
		return array('status' => RESETPWD_STATUS_FAIL, 'error' => _L('account_reset_password_failed', '修改密碼失敗，請稍後再試。'));
	}
}

function send_reset_password_email($user_id, $user_name, $email)
{
	include_once(ROOT_PATH . 'includes/lib_passport.php');
	
	$code = generate_password_reset_token($user_id);
	
	return send_pwd_email($user_id, $user_name, $email, $code);
}

?>