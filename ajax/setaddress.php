<?php
/***
 * setaddress.php
 * by howang 2015-08-05
 *
 * Set an address to user's default address
 ***/

define('IN_ECS', true);

// Skip template engine for faster loading
define('INIT_NO_SMARTY', true);
// Skip update user info for faster loading
define('INIT_NO_UPDATE_USER_INFO', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');

if (empty($_REQUEST['id'])) exit;
if (empty($_SESSION['user_id'])) exit;

$id = $_REQUEST['id'];
$user_id = $_SESSION['user_id'];

// 取得地址
$sql = "SELECT ua.* " .
        "FROM " . $ecs->table('user_address') . " as ua " .
        "WHERE ua.address_id = '$id'";
$consignee = $db->getRow($sql);

// 檢查地址是否屬於該用戶
if ($consignee['user_id'] != $user_id)
{
    header('Content-Type: application/json');
    echo json_encode(array('status' => 0));
    exit;
}

// 設定為用戶的預設收貨地址
$sql = "UPDATE " . $ecs->table('users') .
       " SET address_id = '$id' WHERE user_id = '$user_id'";
$res = $db->query($sql);

// 保存到 session
$_SESSION['flow_consignee'] = $consignee;

header('Content-Type: application/json');
echo json_encode(array('status' => 1));
exit;

?>