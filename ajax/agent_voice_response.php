<?php

/**
 * Interactive voice response for CRM Agent Side
 */

define('IN_ECS', true);

require dirname(dirname(__FILE__)) . '/includes/init.php';
require dirname(dirname(__FILE__)) . '/includes/twiliosdk/twilio/sdk/Twilio/autoload.php';

const STEP_AGENT_INIT = 1;
const STEP_AGENT_DEQUEUE = 2;
const ACCOUNTSID    = 'ACda3ccb910dcf4d76d32125960ed49c6f';
const AUTHTOKEN     = 'e0b1d61362c603751aca842ae9f9ec9b';

$step = empty($_REQUEST['step']) ? STEP_AGENT_DEQUEUE : intval($_REQUEST['step']);
switch ($step) {
    case STEP_AGENT_INIT:
        init();
        break;
    case STEP_AGENT_DEQUEUE:
        answerQueue();
        break;
}

function init()
{
    $clientName = "";
    if (strpos($_SERVER['SERVER_NAME'], 'beta.') !== false) {
        $appSid = 'AP1bf5bffe96f8dc2fe58cafed31469776';
        $clientName = 'Test';

    } else {
        $appSid     = "AP93910f7c89694b53b3584dd5030fa8d1";
    }

    $capability = new Twilio\Jwt\ClientToken(ACCOUNTSID, AUTHTOKEN);
    $clientName .= "TwilioClient$_REQUEST[admin_name]";
    $capability->allowClientIncoming("$clientName");
    $capability->allowClientOutgoing($appSid);
    $token = $capability->generateToken(12 * 60 * 60);

    $response = [
        "identity"  => $clientName,
        "token"     => $token
    ];
    echo json_encode($response);
    exit;
}
function answerQueue()
{
    $response = new Twilio\Twiml();
    $crmController = new Yoho\cms\Controller\CrmController();
    $response->say("正在接通客人", $crmController::IVR_LANG_CONFIG[$crmController::IVR_LANG_CANTONESE]);
    $config = [
        'action' => 'https://' . $_SERVER['SERVER_NAME'] . '/api/twilioVoiceWebhook.php',
    ];
    $dial = $response->dial('', $config);
    $dial->queue((strpos($_SERVER['SERVER_NAME'], 'beta.') !== false ? 'test-' : '') . "support", ['url' => '/ajax/voice_response.php?step=8']);
    outputXml($response);
}

function outputXml($response)
{
    header('Content-Type: text/xml');
    echo $response;
    exit;
}