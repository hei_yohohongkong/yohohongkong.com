<?php

/***
 * bonus_use.php
 * by howang 2014-06-16
 *
 * Use 紅包 for current order
 ***/

define('IN_ECS', true);

require(dirname(__FILE__) . '/../includes/init.php');

include_once(ROOT_PATH .'includes/lib_order.php');
$orderController = new Yoho\cms\Controller\OrderController();
$order_fee_platform = $orderController::ORDER_FEE_PLATFORM_WEB;
$user_id = $_SESSION['user_id'];

if (!$user_id)
{
	header('Content-Type: application/json');
	echo json_encode(array(
		'status' => 0,
		'error' => '請先登入',
	));
	exit;
}

$bonus_id = empty($_REQUEST['bonus_id']) ? 0 : trim($_REQUEST['bonus_id']);

if (empty($bonus_id))
{
	header('Content-Type: application/json');
	echo json_encode(array(
		'status' => 0,
		'error' => '請選擇優惠券',
	));
	exit;
}

// 取得购物类型
$flow_type = isset($_SESSION['flow_type']) ? intval($_SESSION['flow_type']) : CART_GENERAL_GOODS;

if (!((!isset($_CFG['use_bonus']) || $_CFG['use_bonus'] == '1')
	&& ($flow_type != CART_GROUP_BUY_GOODS && $flow_type != CART_EXCHANGE_GOODS)))
{
	header('Content-Type: application/json');
	echo json_encode(array(
		'status' => 0,
		'error' => '不允許使用優惠券',
	));
	exit;
}

// 检查购物车中是否有商品
$sql = "SELECT COUNT(*) FROM " . $ecs->table('cart') .
	" WHERE session_id = '" . SESS_ID . "' " .
	"AND parent_id = 0 AND is_gift = 0 AND rec_type = '$flow_type'";

if ($db->getOne($sql) == 0)
{
	header('Content-Type: application/json');
	echo json_encode(array(
		'status' => 0,
		'error' => '購物車中沒有產品',
	));
	exit;
}

$consignee = get_consignee($_SESSION['user_id']);

$_SESSION['flow_consignee'] = $consignee;

// 对商品信息赋值
$cart_goods = cart_goods($flow_type); // 取得商品列表，计算合计

// 取得訂單
$order = flow_order_info();

// 取得紅包資訊
$bonus = bonus_info($bonus_id);
if (empty($bonus))
{
	header('Content-Type: application/json');
	echo json_encode(array(
		'status' => 0,
		'error' => '優惠券無效',
	));
	exit;
}

// 驗證紅包
if (((!empty($bonus) && $bonus['user_id'] == $_SESSION['user_id']) || ($bonus['type_money'] > 0 && empty($bonus['user_id']))) && $bonus['order_id'] <= 0)
{
	$now = gmtime();
	if ($now > $bonus['use_end_date'])
	{
		header('Content-Type: application/json');
		echo json_encode(array(
			'status' => 0,
			'error' => '優惠券已過期',
		));
		exit;
	}
}
else
{
	$order['bonus_id'] = '';
	header('Content-Type: application/json');
	echo json_encode(array(
		'status' => 0,
		'error' => '優惠券無效或已使用',
	));
	exit;
}

// 使用紅包
$order['bonus_id'] = $bonus['bonus_id'];
$order['bonus_sn'] = $$bonus['bonus_sn'];

// 计算订单的费用
$total = $orderController->order_fee_flow($order, $cart_goods, $consignee, $order_fee_platform);

if ($total['goods_price'] < $bonus['min_goods_amount'])
{
	$order['bonus_id'] = '';
	// 重新计算订单
	$total = $orderController->order_fee_flow($order, $cart_goods, $consignee, $order_fee_platform);
	
	header('Content-Type: application/json');
	echo json_encode(array(
		'status' => 0,
		'error' => '優惠券並不適用於此訂單',
	));
	exit;
}

header('Content-Type: application/json');
echo json_encode(array(
	'status' => 1,
	'coupon_value' => $total['bonus']
));
exit;

?>
