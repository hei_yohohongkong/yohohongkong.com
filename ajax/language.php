<?php

/***
 * language.php
 * by howang 2015-07-13
 *
 * Load language for use in javascript files
 ***/

define('IN_ECS', true);

$responseType = empty($_REQUEST['type']) ? '' : trim($_REQUEST['type']);
$responseType = in_array($responseType, array('js', 'json')) ? $responseType : 'json';
$languagePack = empty($_REQUEST['pack']) ? '' : trim($_REQUEST['pack']);
$languagePack = in_array($languagePack, array('common', 'index', 'brand', 'category', 'search', 'goods', 'flashdeal', 'user', 'account', 'topic', 'article', 'cart', 'checkout', 'payment', 'mobile')) ? $languagePack : 'common';

if (empty($_REQUEST['lang']))
{
	require_once(dirname(dirname(__FILE__)) . '/includes/inc_constant.php');
	require_once(dirname(dirname(__FILE__)) . '/includes/lib_i18n.php');
	$language = user_language();
}
else
{
	$language = basename($_REQUEST['lang']);
}

$language_file = dirname(dirname(__FILE__)) . '/languages/' . $language . '/yoho.php';

// Enable caching if request with rewritten URL
if (!empty($_REQUEST['hash']))
{
	// Allow browser to cache for 1 year
	header('Cache-Control: max-age=31536000');
	header('Expires: ' . gmdate('D, d M Y H:i:s', time() + 31536000) . ' GMT');
	
	// ETag (quotes are required by spec)
	$hash = trim($_REQUEST['hash']);
	$etag = '"' . $hash . '"';
	header('ETag: ' . $etag);
	
	// // Last Modified
	// $last_modified = file_exists($language_file) ? filemtime($language_file) : time();
	// header('Last-Modified: ' . gmdate('D, d M Y H:i:s', $last_modified) . ' GMT');
	
	// If If-None-Match exists, check it and return 304 if match
	if (isset($_SERVER['HTTP_IF_NONE_MATCH']) && trim($_SERVER['HTTP_IF_NONE_MATCH']) == $etag)
	{
		header('HTTP/1.1 304 Not Modified');
    	exit;
	}
	
	// // If If-Modified-Since exists, check it and return 304 if not modified
	// if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) == $last_modified)
	// {
    // 	header('HTTP/1.1 304 Not Modified');
    // 	exit;
	// }
}

$languages = array();

if (file_exists($language_file))
{
	require_once(dirname(dirname(__FILE__)) . '/includes/inc_constant.php');
	require_once($language_file);
	
	require_once(dirname(dirname(__FILE__)) . '/includes/lib_i18n.php');
	$langKeys = language_keys_for_pack($languagePack);
	
	foreach ($langKeys as $key)
	{
		if (isset($_LANG[$key]))
		{
			$languages[$key] = $_LANG[$key];
		}
	}
}

if ($responseType == 'json')
{
	header('Content-Type: application/json;charset=utf-8');
	echo json_encode($languages);
	exit;
}
else
{
	header('Content-Type: application/javascript;charset=utf-8');
	echo 'var YOHO=YOHO||{};YOHO.lang=' . json_encode($language) . ';YOHO.languages=YOHO.languages||{};';
	echo '$.extend(YOHO.languages, ' . json_encode($languages) . ');' . "\n";
	exit;
}

?>