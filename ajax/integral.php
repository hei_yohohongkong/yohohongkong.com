<?php

/***
* checkout.php
*
* AJAX checkout page
***/

define('IN_ECS', true);

// Skip template engine for faster loading
define('INIT_NO_SMARTY', true);
// Skip update user info for faster loading
define('INIT_NO_UPDATE_USER_INFO', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');

// define('DESKTOP_ONLY', true); // This page don't have mobile version

define('FORCE_HTTPS', true); // Require HTTPS for security
require(ROOT_PATH . 'includes/lib_order.php');

// If user not logged in, we are done here
if (empty($_SESSION['user_id']))
{
	header('Content-Type: application/json');
	echo json_encode(array(
		'status' => 0
	));
	exit;
}

$total    = empty($_REQUEST['total']) ? 0 : intval($_REQUEST['total']);
$shipping = empty($_REQUEST['shipping']) ? 0 : intval($_REQUEST['shipping']);
$coupon   = empty($_REQUEST['coupon']) ? 0 : floatval($_REQUEST['coupon']);
$integral = $_REQUEST['integral'];

// Check if input is not number
$bool     = empty($integral) ? false: is_numeric($integral);
if(!$bool){
    header('Content-Type: application/json');
	echo json_encode(array(
		'status' => 2,
        'bool'   => $bool,
        'integral'=>$integral
	));
	exit;
}

// Check integral to use
$user_id = $_SESSION['user_id'];
if ($user_id > 0)
{
    $integral = intval($integral);
    $integral_amount = value_of_integral($integral);
    if($coupon > 0)$total -= $coupon;
    
    // user integral
    $user_info = user_info($user_id);
    $user_points = $user_info['pay_points'];
    
    // order max integral
    $sql = "SELECT SUM(g.integral * c.goods_number) ".
            "FROM " . $GLOBALS['ecs']->table('cart') . " AS c, " . $GLOBALS['ecs']->table('goods') . " AS g " .
            "WHERE c.session_id = '" . SESS_ID . "' AND c.goods_id = g.goods_id AND c.is_gift = 0 AND g.integral > 0 " .
            "AND c.rec_type = '" . CART_GENERAL_GOODS . "'";
    $val = intval($GLOBALS['db']->getOne($sql));
    $order_max_integral = integral_of_value($val);
    
    $max_integral = min($user_points, $order_max_integral);
    
    if ($integral < 1 || $integral > $max_integral)
    {
        header('Content-Type: application/json');
    	echo json_encode(array(
    		'status' => 3
    	));
    	exit;
    }
    
    if($integral_amount > $total)
    {
        header('Content-Type: application/json');
    	echo json_encode(array(
    		'status' => 4
    	));
    	exit;
    } else {
        header('Content-Type: application/json');
    	echo json_encode(array(
    		'status' => 1
    	));
    	exit;
    }
    
    
} else {
    header('Content-Type: application/json');
	echo json_encode(array(
		'status' => 0
	));
	exit;
}


header('Content-Type: application/json');
echo json_encode(array(
    'status' => 1,
    'total' => $total
));
exit;


?>
