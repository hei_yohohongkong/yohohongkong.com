<?php
/***
* getRelatedArticles.php
*
* AJAX get related articles, topic list and promotion link list in product detail page
***/

define('IN_ECS', true);

// Skip template engine for faster loading
define('INIT_NO_SMARTY', true);

// If the user is already logged in, we will replace the session with new login session, 
// so updating user info for logged in user here is meaningless
define('INIT_NO_UPDATE_USER_INFO', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');
$topicController = new Yoho\cms\Controller\TopicController();
$goods_id = $_POST["goods_id"];
$goods = array();
// 推廣鏈結
$sql = "SELECT * FROM " . $GLOBALS['ecs']->table('goods_promo_links') . " WHERE `goods_id` = '" . $goods_id . "' ORDER BY `link_order` ASC";
$goods['promo_links_list'] = $GLOBALS['db']->getAll($sql);
        
//專題
$goods['topic_list'] = $topicController->getRelatedTopic($goods_id);
//相關文章
$goods['article_list'] =  get_linked_articles($goods_id);

header('Content-Type: application/json');
echo json_encode($goods);
exit;
?>