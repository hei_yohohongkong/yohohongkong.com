<?php

/***
 * email_popup_box.php
 * by howang 2014-06-18
 *
 * Output HTML to display in the Email popup box
 ***/

?>
<div id="email_prompt" style="text-align: center;">
	<span style="font-size: 200%; line-height: 150%; font-color: #00acee;">登記您的電郵地址，<br/>獲取最新優惠資訊！</span><br/><br/>
	<form method="post" action="http://email.yohohongkong.com/?p=subscribe&id=1" id="email_subscription_form">
		電郵地址 &nbsp;&nbsp;<input type="text" name="email" placeholder="user@example.com" value="" size="40"/>
		<input type="hidden" name="emailconfirm" value=""/>
		<input type="hidden" name="htmlemail" value="1"/>
		<input type="hidden" name="list[2]" value="signup"/>
		<input type="hidden" name="listname[2]" value="YOHO 友和 Newsletter"/>
		<span style="display:none;"><input type="text" name="VerificationCodeX" value="" size="20"></span>
		<input type="hidden" name="from_website" value="www.yohohongkong.com"/>
		<br><br>
		<input type="submit" name="subscribe" value="登記電郵地址" class="btn-css3">
	</form>
</div>