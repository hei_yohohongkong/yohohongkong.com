<?php
/***
* getGoodsOffer.php
*
* AJAX get package goods list and favourable goods list
***/

define('IN_ECS', true);

// Skip template engine for faster loading
define('INIT_NO_SMARTY', true);

// If the user is already logged in, we will replace the session with new login session, 
// so updating user info for logged in user here is meaningless
define('INIT_NO_UPDATE_USER_INFO', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');
$packageController = new Yoho\cms\Controller\PackageController();
$orderController   = new Yoho\cms\Controller\OrderController();
$order_fee_platform = $orderController::ORDER_FEE_PLATFORM_WEB;
$modal_type = $_POST["modal_type"];
$goods_id = $_POST["goods_id"];
$act_id = $_POST["act_id"];
$result = array();
if ($modal_type == 'offerSet') {
    $result = $packageController->get_package_goods_list($goods_id);
} else {
    $favourable_list = $orderController->favourable_list_by_goods($_SESSION['user_rank'], $_SESSION['user_id'], $goods_id, $order_fee_platform);
    foreach ($favourable_list as $favourable) {
        if ($favourable[0]['act_id'] == $act_id) {
            $result = $favourable[0];
        }

    }
}
header('Content-Type: application/json');
echo json_encode($result);
exit;
?>