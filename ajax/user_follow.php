<?php

/***
 * user_follow.php
 * by howang 2014-09-05
 *
 * AJAX script for follow / unfollow users
 ***/

define('IN_ECS', true);

// Skip template engine for faster loading
define('INIT_NO_SMARTY', true);
// Skip update user info for faster loading
define('INIT_NO_UPDATE_USER_INFO', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');

$act = empty($_REQUEST['act']) ? '' : trim($_REQUEST['act']);

if (in_array($act, array('follow','unfollow')))
{
	$follow_id = (empty($_POST['follow_id'])) ? '' : (int)$_POST['follow_id'];
	$user_id = empty($_SESSION['user_id']) ? 0 : intval($_SESSION['user_id']);
	
	if (empty($follow_id))
	{
		$result = array('status' => '0', 'error' => '請選擇跟隨的對象');
	}
	else if ($user_id <= 0)
	{
		$result = array('status' => '0', 'error' => '請先登入');
	}
	else
	{
		$sql = "SELECT info_completed FROM " . $ecs->table('users') .
				" WHERE `user_id` = '" . $follow_id . "'";
		if (!$db->getOne($sql))
		{
			$result = array('status' => '0', 'error' => '會員不存在或未啟用跟隨功能');
		}
		else if ($act == 'follow')
		{
			$sql = "INSERT INTO " . $ecs->table('user_follows') .
					" (`user_id`, `follow_id`, `add_time`)".
					" VALUES ('" . $user_id . "','" . $follow_id . "', '" . gmtime() . "')".
					" ON DUPLICATE KEY UPDATE `add_time` = '" . gmtime() . "'";
			$db->query($sql);
		}
		else if ($act == 'unfollow')
		{
			$sql = "DELETE FROM " . $ecs->table('user_follows') .
					" WHERE `user_id` = '" . $user_id . "' AND `follow_id` = '" . $follow_id . "' LIMIT 1";
			$db->query($sql);
		}
		
		$result = array('status' => '1');
	}
	
	header('Content-Type: application/json');
	echo json_encode($result);
	exit;
}
?>