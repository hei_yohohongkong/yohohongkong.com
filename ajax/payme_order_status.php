<?php

/***
 * payme_order_status.php
 * by billy 2019-06-12
 *
 * AJAX endpoint for checking HSBC Payme Payment Request Status
 ***/

define('IN_ECS', true);

// Skip update user info for faster loading
define('INIT_NO_UPDATE_USER_INFO', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');
require(dirname(dirname(__FILE__)) . '/includes/lib_order.php');
require(dirname(dirname(__FILE__)) . '/includes/lib_payment.php');
$paymeController = new Yoho\cms\Controller\PaymeController();
$orderController = new Yoho\cms\Controller\OrderController();
global $db;

// Test request ids with pre-set status, and should not be updated
$test_request_ids = [
    "591ec407-401d-40a6-9db0-b48a35fad8a3",
    " b15e090a-5e97-4b44-a67e-542eb2aa0f4d",
    "25f90d96-4052-4c47-8ec1-f818c0e7a212"
];

$payment_info = get_payment('payme');
$user_id = $_SESSION['user_id'];

$host_name = $payment_info['payme_api_base_url'];

// Commented out for postman debug
if (!$user_id)
{
	header('Content-Type: application/json');
	echo json_encode(array(
		'status' => 0,
		'error' => '請先登入',
	));
	exit;
}

if (empty($_REQUEST['payment_request_id'])) {
    echo json_encode([
        'status' => 0,
        'error' => 'Payment Request ID should be provided'
    ]);
    exit;
}

$token = $paymeController->getAccessToken();
$token = $token->accessToken;

$result = $paymeController->getPaymentRequestInfo($_REQUEST['payment_request_id'], $token);

$result_data = json_decode($result);

if (isset($result_data->status) && $result_data->status == 0) {
    return $result;
}

$msg = 'FAILURE';
$status_code = 0;

if ($result_data->statusCode == 'PR001') {
    $status_code = 1;
    $msg = 'PENDING_PAYMENT';
} elseif ($result_data->statusCode == 'PR005') {
    $status_code = 1;
    $msg = 'COMPLETED';
    $transaction_ids = isset($result_data->transactions) ? implode(",", $result_data->transactions) : $result_data->paymentRequestId;
    $action_note = $payment_info['pay_code'] . ':' . $transaction_ids . '（' . $result_data->currencyCode . ($result_data->totalAmount) . '）';
    // TODO: update the paymennt request for complete the payment request
    $log_id = $paymeController->getLogIdByPaymentRequestId($result_data->paymentRequestId);
    if (!empty($log_id)) {
        $orderController->order_payment_transaction_log($log_id, $payment_info['pay_id'], $transaction_ids, $result_data->currencyCode,($result_data->totalAmount),0);
        $paymeController->insertStatusIntoPaymentRequestRecord($result);
        order_paid($log_id,PS_PAYED,$action_note);
    }
} elseif ($result_data->statusCode == 'PR007') {
    $msg = 'EXPIRED';
    // TODO: update the payment request for the expired payment request
    $paymeController->insertStatusIntoPaymentRequestRecord($result);
}

header('Content-Type: application/json');
echo json_encode([
    'status' => $status_code,
    'pay_status' => $msg,
    'data' => $result
]);
exit;

?>
