<?php

/***
 AJAX get and set mobile thumb of level 3 category 
***/

define('IN_ECS', true);

// Skip session and template engine for faster loading
define('INIT_NO_USERS', true);
define('INIT_NO_SMARTY', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');
$categoryController = new Yoho\cms\Controller\CategoryController();
if (!empty($_POST['type']) && $_POST['type'] == 'select') {
    if (!empty($_POST['cat_id']))
    {
        $cat_id =  $_POST['cat_id'];
        $sql = "SELECT g.goods_thumb, g.goods_name FROM " . $GLOBALS['ecs']->table('goods') . " AS g WHERE goods_id = (SELECT c.goods_thumb_id FROM " . $GLOBALS['ecs']->table('category') . " AS c WHERE `cat_id` = '" . $cat_id . "')";
        $res = $GLOBALS['db']->getRow($sql);
        if ($res) {
            $goods_thumb = $res['goods_thumb'];
            $goods_name = $res['goods_name'];
        } else {
            $row = $categoryController->getCategoryThumb($cat_id);
            $goods_thumb = $row['goods_thumb'];
            if ($row['goods_name']) {
                $goods_name = $row['goods_name'];
            } else {
                $sql = "SELECT g.goods_name FROM " . $GLOBALS['ecs']->table('goods') . " AS g WHERE goods_id = '" . $row['goods_id'] . "'";
                $goods_name = $GLOBALS['db']->getOne($sql);
            }
        }  
        $result = array('status' => '1', 'goods_thumb' => $goods_thumb, 'goods_name' => $goods_name);
    } else {
        $result = array('status' => '0');
    }
} else {
    if (!empty($_POST['goods_thumb_id']) && !empty($_POST['cat_id'])) {
        $goods_thumb_id =  $_POST['goods_thumb_id'];
        $cat_id =  $_POST['cat_id'];
        $sql = "UPDATE " . $ecs->table("category") . " SET goods_thumb_id = '$goods_thumb_id' WHERE cat_id = '$cat_id' ";
        $db->query($sql);
        $result = array('status' => '1');
        $categoryController->delMemcache($cat_id);
    } else {
        $result = array('status' => '0');
    }
}

header('Content-Type: application/json');
echo json_encode($result);
exit;

?>