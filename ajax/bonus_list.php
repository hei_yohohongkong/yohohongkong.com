<?php

/***
 * bonus_list.php
 * by howang 2014-06-16
 *
 * Return a list of 紅包 that the current user owns
 ***/

define('IN_ECS', true);

require(dirname(__FILE__) . '/../includes/init.php');

include_once(ROOT_PATH .'includes/lib_order.php');
$orderController = new Yoho\cms\Controller\OrderController();
$order_fee_platform = $orderController::ORDER_FEE_PLATFORM_WEB;
// modified from user_bonus() in lib_order.php
function user_bonus_hw($user_id, $goods_amount = 0)
{
    $day    = getdate();
    $today  = local_mktime(23, 59, 59, $day['mon'], $day['mday'], $day['year']);

    $sql = "SELECT t.type_id, t.type_name, t.type_money, b.bonus_id, b.bonus_sn " .
            "FROM " . $GLOBALS['ecs']->table('user_bonus') . " AS b " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('bonus_type') . " AS t " .
            "ON t.type_id = b.bonus_type_id " .
            "WHERE t.use_start_date <= '$today' " .
            "AND t.use_end_date >= '$today' " .
            "AND t.min_goods_amount <= '$goods_amount' " .
            "AND b.user_id<>0 " .
            "AND b.user_id = '$user_id' " .
            "AND b.order_id = 0";
    return $GLOBALS['db']->getAll($sql);
}

$user_id = $_SESSION['user_id'];

if (!$user_id)
{
	header('Content-Type: application/json');
	echo json_encode(array(
		'status' => 0,
		'error' => '請先登入',
	));
	exit;
}

// 取得购物类型
$flow_type = isset($_SESSION['flow_type']) ? intval($_SESSION['flow_type']) : CART_GENERAL_GOODS;

if (!((!isset($_CFG['use_bonus']) || $_CFG['use_bonus'] == '1')
	&& ($flow_type != CART_GROUP_BUY_GOODS && $flow_type != CART_EXCHANGE_GOODS)))
{
	header('Content-Type: application/json');
	echo json_encode(array(
		'status' => 0,
		'error' => '不允許使用優惠券',
	));
	exit;
}

// 检查购物车中是否有商品
$sql = "SELECT COUNT(*) FROM " . $ecs->table('cart') .
	" WHERE session_id = '" . SESS_ID . "' " .
	"AND parent_id = 0 AND is_gift = 0 AND rec_type = '$flow_type'";

if ($db->getOne($sql) == 0)
{
	header('Content-Type: application/json');
	echo json_encode(array(
		'status' => 0,
		'error' => '購物車中沒有產品',
	));
	exit;
}

$consignee = get_consignee($_SESSION['user_id']);

$_SESSION['flow_consignee'] = $consignee;

// 对商品信息赋值
$cart_goods = cart_goods($flow_type); // 取得商品列表，计算合计

// 取得訂單
$order = flow_order_info();

// 计算订单的费用
$total = $orderController->order_fee_flow($order, $cart_goods, $consignee, $order_fee_platform);

// 取得用戶持有的紅包
$bonus = user_bonus_hw($user_id, $total['goods_price']);

$coup = array();

foreach ($bonus as $coupon)
{
	$coup[] = array(
		'value' => $coupon['bonus_id'],
		'text' => $coupon['bonus_sn'] . ' - ' . $coupon['type_name']
	);
}

header('Content-Type: application/json');
echo json_encode(array(
	'status' => 1,
	'coup' => $coup
));
exit;

?>
