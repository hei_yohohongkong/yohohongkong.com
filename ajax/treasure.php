<?php
if (!defined('IN_ECS')) {
    define('IN_ECS', true);
}
require_once dirname(dirname(__FILE__)) . '/includes/init.php';

if (empty($_CFG['treasure_start']) || $_CFG['treasure_start'] == 2) {
    if (empty($_SESSION['treasure'])) {
        $fail = true;
    }
}

$now_time = gmtime();
// $now_time = gmtime() + 10 * 3600;
$wait_unlock_time = strpos($_SERVER['SERVER_NAME'], 'beta.') !== false ? 5 : 60;
$wait_expire_time = strpos($_SERVER['SERVER_NAME'], 'beta.') !== false ? 960 : 15 * 60;

// if (!in_array($_SESSION['user_id'], [155600, 9481, 6796, 39, 160113])) {
//     $_REQUEST['treasure_action'] == "invalid";
// }
$real_ip = real_ip();
if ($real_ip == "0.0.0.0" || empty($real_ip) || !defined("SESS_ID") || SESS_ID == "SESS_ID" || is_spider() || $fail) {
    $_REQUEST['treasure_action'] = "fail";
} else {
    $treasure_id = $db->getOne("SELECT MIN(treasure_id) FROM " . $ecs->table("treasure") . " WHERE confirm = 0 AND open_time + $wait_expire_time > $now_time AND ip = '$real_ip'");
    if (empty($treasure_id)) {
        unset($_SESSION['treasure']);
        $treasure_output = [
            'status' => 1,
            'step' => 'fail',
        ];
    }
}
if (intval($treasure_id) != $_SESSION['treasure']['treasure_id']) {
    $db->query("UPDATE " . $ecs->table("treasure") . " SET ip = null, session_id = null, open_time = 0, confirm = 0 WHERE confirm = 0 AND open_time + $wait_expire_time > $now_time AND ip = '$real_ip' AND treasure_id != $treasure_id");
    $treasures = $db->getRow("SELECT * FROM " . $ecs->table("treasure") . " WHERE treasure_id = " . $treasure_id);
    $open_time = empty($treasures['open_time']) ? $now_time : $treasures['open_time'];
    $treasure_tnc = empty($treasures['treasure_tnc']) ? "" : ("<li>" . implode("</li><li>", explode("|", $treasures['treasure_tnc'])) . "</li>");
    $_SESSION['treasure'] = [
        'treasure_id'   => $treasures['treasure_id'],
        'treasure_name' => $treasures['treasure_name'],
        'treasure_desc' => $treasures['treasure_desc'],
        'treasure_tnc' => $treasure_tnc,
        'coupon_code'   => "",
        'unlock_time'   => -1,
        'reward'        => 0,
        'expire_time'   => $open_time + $wait_expire_time,
        'remain_time'   => $wait_expire_time,
        'close_time'    => $wait_expire_time,
        'step'          => 'found'
    ];
    if (!empty($treasures['coupon_id'])) {
        $coupon = $db->getRow("SELECT coupon_code, start_time, end_time FROM " . $ecs->table("coupons") . " WHERE coupon_id = $treasures[coupon_id]");
        if (!empty($coupon)) {
            $_SESSION['treasure']['coupon_code'] = $coupon['coupon_code'];
            $now_time = local_strtotime("now");
            if ($now_time < intval($coupon['start_time'])) {
                $_SESSION['treasure']['coupon_date'] = local_date("Y年m月d日", $coupon['start_time']);
            } else {
                $_SESSION['treasure']['coupon_date'] = "即日";
            }
            $_SESSION['treasure']['coupon_date'] .= " 至 " . local_date("Y年m月d日", $coupon['end_time'] - 86400);
            $_SESSION['treasure']['reward'] = 1;
            if (empty($treasures['treasure_desc'])) {
                $_SESSION['treasure']['reward'] = 2;
            }
        }
    }
}
if ($_REQUEST['treasure_action'] == "unlock_treasure") {
    if (!empty($_SESSION['treasure']) && $_SESSION['treasure']['step'] == "found") {
        $_SESSION['treasure']['step'] = "unlock";
        $remain_time = $wait_unlock_time;
        if ($_SESSION['treasure']['unlock_time'] == -1) {
            $_SESSION['treasure']['unlock_time'] = $now_time + $remain_time;
        } else {
            $remain_time = $_SESSION['treasure']['unlock_time'] - $now_time;
        }
        $treasure_output = [
            'status' => 1,
            'remain_time' => $remain_time,
        ];
    }
} elseif ($_REQUEST['treasure_action'] == "open_treasure") {
    if (!empty($_SESSION['treasure']) && in_array($_SESSION['treasure']['step'], ["unlock", "prize"])) {
        if ($_SESSION['treasure']['unlock_time'] < $now_time && $_SESSION['treasure']['unlock_time'] != -1 && $now_time >= $_SESSION['treasure']['open_time'] + $wait_unlock_time) {
            if (intval($_SESSION['treasure']['treasure_id']) == $_SESSION['treasure_promise']['treasure_id']) {
                $_SESSION['treasure_promise']['claim']++;
            }
            if (count($_SESSION['treasure_promise']['urls']) >= $_SESSION['treasure_promise']['free_count']) {
                $_SESSION['treasure_promise']['free_count'] = 0;
            }
            if ($_SESSION['treasure_promise']['free_count'] == 0 && count($_SESSION['treasure_promise']['urls']) >= max($_SESSION['treasure_promise']['count'])) {
                $_SESSION['treasure_promise']['urls'] = [];
            }
            $treasure_output = [
                'status' => 1,
                'reward' => $_SESSION['treasure']['reward'],
                'treasure_name' => $_SESSION['treasure']['treasure_name'],
                'treasure_tnc' => $_SESSION['treasure']['treasure_tnc'],
                'coupon_code'   => $_SESSION['treasure']['coupon_code'],
                'coupon_date'   => $_SESSION['treasure']['coupon_date'],
            ];
            $db->query("UPDATE " . $ecs->table("treasure") . " SET confirm = 1 WHERE treasure_id = " . $_SESSION['treasure']['treasure_id']);
            unset($_SESSION['treasure']);
        }
    }
} elseif ($_REQUEST['treasure_action'] == "get_info") {
    $step = "fail";
    $remain_time = 0;
    $close_time = 0;
    if (!empty($_SESSION['treasure'])) {
        $close_time = $_SESSION['treasure']['expire_time'] - $now_time;
        $close_time = $close_time > 0 ? $close_time : 0;
        if ($_SESSION['treasure']['unlock_time'] == -1) {
            $step = "found";
        } elseif ($_SESSION['treasure']['unlock_time'] > $now_time) {
            $step = "unlock";
            $remain_time = $_SESSION['treasure']['unlock_time'] - $now_time;
            $remain_time = $remain_time > 0 ? $remain_time : 0;
        } elseif ($_SESSION['treasure']['unlock_time'] <= $now_time) {
            $step = "prize";
            $treasure = [];
            $treasure = [
                'reward' => $_SESSION['treasure']['reward'],
                'treasure_name' => $_SESSION['treasure']['treasure_name'],
                'treasure_desc' => $_SESSION['treasure']['treasure_desc'],
            ];
            if (empty($_SESSION['treasure']['reward'])) {
                $step = "no_prize";
            }
        }
    }
    $treasure_output = [
        'status' => 1,
        'remain_time' => $remain_time,
        'step' => $step,
        'treasure' => $treasure,
        'close_time' => $close_time,
    ];
} elseif ($_REQUEST['treasure_action'] == "confirm") {
    if (!empty($_SESSION['treasure'])) {
        if (intval($_SESSION['treasure']['treasure_id']) == $_SESSION['treasure_promise']['treasure_id']) {
            $_SESSION['treasure_promise']['claim']++;
        }
        if (count($_SESSION['treasure_promise']['urls']) >= $_SESSION['treasure_promise']['free_count']) {
            $_SESSION['treasure_promise']['free_count'] = 0;
        }
        if ($_SESSION['treasure_promise']['free_count'] == 0 && count($_SESSION['treasure_promise']['urls']) >= max($_SESSION['treasure_promise']['count'])) {
            $_SESSION['treasure_promise']['urls'] = [];
        }
        $db->query("UPDATE " . $ecs->table("treasure") . " SET confirm = 1 WHERE treasure_id = " . $_SESSION['treasure']['treasure_id']);
        unset($_SESSION['treasure']);
        $treasure_output = [
            'status' => 1,
        ];
    }
}

if (empty($treasure_output)) {
    $treasure_output = [
        'status' => 0,
        'message' => '非法請求',
    ];
}
header('Content-Type: application/json');
echo json_encode($treasure_output);
exit;
?>