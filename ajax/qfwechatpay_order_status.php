<?php

/***
 * qfwechatpay_order_status.php
 * by billy 2019-05-15
 *
 * AJAX endpoint for checking QF WeChat Pay order status
 ***/

define('IN_ECS', true);

// Skip template engine for faster loading
define('INIT_NO_SMARTY', true);
// Skip update user info for faster loading
define('INIT_NO_UPDATE_USER_INFO', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');
require(dirname(dirname(__FILE__)) . '/includes/lib_order.php');
require(dirname(dirname(__FILE__)) . '/includes/lib_payment.php');
$wechat_pay_controller = new Yoho\cms\Controller\WeChatPayLogController();
$orderController = new Yoho\cms\Controller\OrderController();
global $db;

$payment_info = get_payment('qfwechatpay');
$user_id = $_SESSION['user_id'];

$host_name = (strpos($_SERVER['SERVER_NAME'], 'beta.yohohongkong.com') === false) ? "https://openapi.qfpay.com" : "https://openapi-test.qfpay.com";

// Commented out for postman debug
if (!$user_id)
{
	header('Content-Type: application/json');
	echo json_encode(array(
		'status' => 0,
		'error' => '請先登入',
	));
	exit;
}

if (empty($_REQUEST['order_payment_id']) && empty($_REQUEST['gateway_transaction_sn'])) {
    echo json_encode([
        'status' => 0,
        'error' => 'Order Payment ID or Gateway Transaction SN should be provided'
    ]);
    exit;
}

if (!empty($_REQUEST['order_payment_id']) && !empty($_REQUEST['gateway_transaction_sn'])) {
    echo json_encode([
        'status' => 0,
        'error' => 'Either Order Payment ID or Gateway Transaction SN, but not both should be provided'
    ]);
    exit;
}

if (!empty($_REQUEST['order_payment_id']) && empty($_REQUEST['gateway_transaction_sn'])) {
    $parameter['out_trade_no'] = $_REQUEST['order_payment_id'];
}

if (empty($_REQUEST['order_payment_id']) && !empty($_REQUEST['gateway_transaction_sn'])) {
    $parameter['syssn'] = $_REQUEST['gateway_transaction_sn'];
}

$sign  = '';

foreach ($parameter AS $key => $val)
{
    $sign  .= ecs_iconv(EC_CHARSET, 'utf-8', $key) . "=" . ecs_iconv(EC_CHARSET, 'utf-8', $val). "&";
}

$sign  = substr($sign, 0, -1). ecs_iconv(EC_CHARSET, 'utf-8', $payment_info['qfwechatpay_key']);

$ch = curl_init($host_name.'/trade/v1/query');
curl_setopt($ch, CURLOPT_HEADER, true);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($parameter));
curl_setopt($ch, CURLOPT_HTTPHEADER, [
    "X-QF-APPCODE: " . $payment_info['qfwechatpay_code'],
    "X-QF-SIGN: " . md5($sign),
    "Content-Type: application/x-www-form-urlencoded",
    "Content-Length: " . strlen(http_build_query($parameter)),
]);

$wechat_pay_query_result = curl_exec($ch);

if (curl_errno($ch) != 0) // cURL error
{
    hw_error_log('Cannot connect to QFPay: ' . curl_error($ch));
    curl_close($ch);
    echo json_encode([
        'status' => 0,
        'error' => 'Query failed: Unable to connect to Payment Service'
    ]);
    exit;
}
else
{
    // Serarate response headers and body
    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $wechat_pay_query_result_header = substr($wechat_pay_query_result, 0, $header_size);
    $wechat_pay_query_result = substr($wechat_pay_query_result, $header_size);

    // Get headers for sign checking
    $headers = [];
    $data = explode("\n",$wechat_pay_query_result_header);
    $headers['status'] = $data[0];
    array_shift($data);

    foreach($data as $part){
        $middle = explode(":",$part);
        $headers[trim($middle[0])] = trim($middle[1]);
    }

    $result_sign  = strtoupper(md5($wechat_pay_query_result . ecs_iconv(EC_CHARSET, 'utf-8', $payment_info['qfwechatpay_key'])));

    // Parse and compare the signature
    if (!(!empty($headers['X-QF-SIGN']) && $headers['X-QF-SIGN'] == $result_sign)) {
        curl_close($ch);
        echo json_encode([
            'status' => 0,
            'error' => 'Query failed: Unmatched signature',
            'data' => json_encode($wechat_pay_query_result)
        ]);
        exit;
    }
    curl_close($ch);
}

$wechat_pay_query_result = json_decode($wechat_pay_query_result);
if ($wechat_pay_query_result->respcd != '0000') {
    echo json_encode([
        'status' => 0,
        'error' => 'Query failed: Gateway returned error code '.$wechat_pay_query_result->respcd,
        'data' => json_encode($wechat_pay_query_result)
    ]);
    exit;
}

if (count($wechat_pay_query_result->data) > 0) {
    // Find the entry that matches the syssn or out_trade_id
    $wechat_pay_query_result = $wechat_pay_query_result->data[0];
}

$log_id = substr(explode('_', trim($wechat_pay_query_result->out_trade_no))[0], 13);

// Mark the transaction complete if transaction complete
if ($wechat_pay_query_result->respcd == '0000' && $wechat_pay_query_result->cancel == '0') {
    if (!empty($log_id)) {
        $orderController->order_payment_transaction_log($log_id, $payment_info['pay_id'], $wechat_pay_query_result->syssn, $wechat_pay_query_result->txcurrcd,($wechat_pay_query_result->txamt/100),0);
    }
    
    $action_note = $payment_info['pay_code'] . ':' . $wechat_pay_query_result->syssn . '（' . $wechat_pay_query_result->txcurrcd . ($wechat_pay_query_result->txamt / 100) . '）';

    // TODO: Insert returned data into database
    // TODO: Prevent double update (Updated by polling, then by notification, or vice versa)
    $wechat_pay_controller->insert_wechat_pay_log($wechat_pay_query_result, $log_id);
}

$msg = 'FAILURE';
$status_code = 0;
if ($wechat_pay_query_result->respcd == '0000') {
    $status_code = 1;
    $msg = 'COMPLETE';
    order_paid($log_id,PS_PAYED,$action_note);
} elseif ($wechat_pay_query_result->respcd == '1143') {
    $status_code = 1;
    $msg = 'PENDING_PAYMENT';
} if ($wechat_pay_query_result->respcd == '1145') {
    $status_code = 1;
    $msg = 'PENDING_PAYMENT';
}

header('Content-Type: application/json');
echo json_encode([
    'status' => $status_code,
    'pay_status' => $msg,
    'data' => json_encode($wechat_pay_query_result)
]);
exit;
?>