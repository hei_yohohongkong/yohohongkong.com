<?php

define('IN_ECS', true);

// Skip template engine for faster loading
define('INIT_NO_SMARTY', true);
// Skip update user info for faster loading
define('INIT_NO_UPDATE_USER_INFO', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');
require_once(ROOT_PATH . 'includes/lib_order.php');

/* 载入语言文件 */
require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/user.php');
require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/shopping_flow.php');

/*------------------------------------------------------ */
//-- sbsn LY  AJAX  address  
/*------------------------------------------------------ */
require_once(ROOT_PATH . 'includes/lib_transaction.php');

define('SAVE_ADDRESS_STATUS_FAIL', 0);
define('SAVE_ADDRESS_STATUS_SUCCESS', 1);
define('SAVE_ADDRESS_STATUS_TOO_LONG', 2);
define('SAVE_ADDRESS_STATUS_TOO_MANY_ADDRESSES', 4);
define('SAVE_ADDRESS_STATUS_INVALID_FORMAT', 7);
define('SAVE_ADDRESS_STATUS_REGISTER_FAIL', 8);
$user_id = isset($_POST['user_id']) ? $_POST['user_id'] : $_SESSION['user_id'];
$consignee = array(
	'address_id'          => empty($_POST['id'])                  ? 0    :   intval($_POST['id']),
	'consignee'           => empty($_POST['receiver'])            ? ''   :   compile_str(trim($_POST['receiver'])),
	'email'               => empty($_POST['email'])               ? $_SESSION['email']   :   compile_str($_POST['email']),
	'tel'                 => empty($_POST['phone'])               ? ''   :   compile_str(make_semiangle(trim($_POST['phone']))),
	'mobile'              => empty($_POST['phone'])               ? ''   :   compile_str(make_semiangle(trim($_POST['phone']))),
	'id_doc'              => empty($_POST['id_doc'])               ? ''   :  compile_str(make_semiangle(trim($_POST['id_doc']))),
	'country'             => empty($_POST['country'])             ? 3409 :   intval($_POST['country']),
	'zipcode'             => empty($_POST['zipcode'])             ? ''   :   compile_str(make_semiangle(trim($_POST['zipcode']))),
	'administrative_area' => empty($_POST['administrative_area']) ? ''   :   compile_str($_POST['administrative_area']),
	'phase'            	  => empty($_POST['phase'])               ? ''   :   compile_str(make_semiangle(trim($_POST['phase']))),
	'block'            	  => empty($_POST['block'])               ? ''   :   compile_str(make_semiangle(trim($_POST['block']))),
	'locality'            => empty($_POST['locality'])            ? ''   :   compile_str(make_semiangle(trim($_POST['locality']))),
	'street_name'         => empty($_POST['street'])              ? ''   :   compile_str(make_semiangle(trim($_POST['street']))),
	'street_num'          => empty($_POST['street_num'])          ? ''   :   compile_str(make_semiangle(trim($_POST['street_num']))),
	'sign_building'       => empty($_POST['building'])            ? ''   :   compile_str($_POST['building']),
	'floor'               => empty($_POST['floor'])               ? ''   :   compile_str(make_semiangle(trim($_POST['floor']))),
	'room'                => empty($_POST['room'])                ? ''   :   compile_str(make_semiangle(trim(strtoupper($_POST['room'])))),

	'lift'                => empty($_POST['lift'])                ? ''   :   compile_str(make_semiangle(trim($_POST['lift']))),
	'dest_type'        => empty($_POST['address_type'])        ? ''   :   compile_str(make_semiangle(trim($_POST['address_type']))),

	'address'             => empty($_POST['address'])             ? ''   :   compile_str($_POST['address']),
	'google_place'        => empty($_POST['google_place'])        ? ''   :   compile_str(make_semiangle(trim($_POST['google_place']))),
	'best_time'           => empty($_POST['best_time'])           ? ''   :   compile_str($_POST['best_time'])
);
if ($consignee['id_doc'] != ""){
	$consignee['id_doc'] = encrypt_str($consignee['id_doc']);
}
$userController = new Yoho\cms\Controller\UserController();
$addressController    = new Yoho\cms\Controller\AddressController();
$consignee      = $addressController->handle_address_detail($consignee);

$user_staying_time = empty($_POST['staying_time']) ? 0 : intval($_POST['staying_time']);
setcookie("order_staying_time", $user_staying_time, time() + 3600, '/');
//============如何会员登录了===============================
if ($user_id > 0)
{
	//如果用户已经登录，则保存收货人信息
	$consignee['user_id'] = $user_id;

    $save = save_ly_consignee($consignee, true);
    $status = $save['status'];
    $consignee['address_id'] = $save['id'];
}
else //没登陆的情况
{
	require(ROOT_PATH . 'includes/lib_passport.php');
	// 自動生成帳號
	if (register(trim($consignee['mobile']), 'yohohongkong', trim($consignee['email'])))
	{
		$user->login(trim($consignee['mobile']), 'yohohongkong', 1);
		update_user_info();
		
		$consignee['user_id'] = $user_id;

        $save = save_ly_consignee($consignee, true);
        $status = $save['status'];
        $consignee['address_id'] = $save['id'];
	}
	else
	{
		// if(check_consignee_info($consignee, $flow_type))
		// {
		// 	$status = SAVE_ADDRESS_STATUS_SUCCESS;
		// }
		// else
		// {
			$status = SAVE_ADDRESS_STATUS_REGISTER_FAIL;
		// }
	}
}

if ($status == SAVE_ADDRESS_STATUS_SUCCESS)
{
	$_SESSION['flow_consignee'] = stripslashes_deep($consignee);
}

ecs_header('Content-Type: application/json');
echo json_encode(array(
	'status' => $status,
	'obj' => array(
		'id' => $consignee['address_id'],
		'area' => gr($consignee['district']),
		'city' => gr($consignee['city']),
		'country' => gr($consignee['country']),
		'phone' => $consignee['mobile'],
		'province' => gr($consignee['province']),
		'receiver' => $consignee['consignee'],
		'street' => $consignee['address'],
		'zipcode' => $consignee['zipcode'],
		'user' => array('id' => $user_id)
	),
	'sc_sr' => array(
		'country_id' => $consignee['country'],
		'province_id' => $consignee['province'],
		'city_id' => $consignee['city'],
		'area_id' => $consignee['district']
	)
));
exit;

function save_ly_consignee($consignee, $default=false)
{
	if ($consignee['address_id'] > 0)
	{
		/* 修改地址 */
		$res = $GLOBALS['db']->autoExecute($GLOBALS['ecs']->table('user_address'), $consignee, 'UPDATE', 'address_id = ' . $consignee['address_id']);
	}
	else
	{
		// Check if too many address already
		$address_count = $GLOBALS['db']->getOne("SELECT count(*) FROM " . $GLOBALS['ecs']->table('user_address') . " WHERE user_id = " . intval($consignee['user_id']));
		
		if ($address_count >= 6)
		{
			return ['status' => SAVE_ADDRESS_STATUS_TOO_MANY_ADDRESSES];
		}
		
		/* 添加地址 */
		$res = $GLOBALS['db']->autoExecute($GLOBALS['ecs']->table('user_address'), $consignee, 'INSERT');
		$consignee['address_id'] = $GLOBALS['db']->insert_id();
	}
	
	if ($res !== false && $default)
	{
		/* 保存为用户的默认收货地址 */
		$sql = "UPDATE " . $GLOBALS['ecs']->table('users') . " SET address_id = '$consignee[address_id]' WHERE user_id = '$user_id'";
		$res = $GLOBALS['db']->query($sql);
	}
	
	return ['status' => ($res !== false) ? SAVE_ADDRESS_STATUS_SUCCESS : SAVE_ADDRESS_STATUS_FAIL, 'id' => $consignee['address_id']];
}

function gr($id = 0)  //ly sbsn 购物车中对应商品的数量
{
	if($id > 0)
	{
		$sql = 'SELECT region_name  '.
				'FROM '.$GLOBALS['ecs']->table('region'). 
				" WHERE region_id=$id ";
		$row= $GLOBALS['db']->getOne($sql);
	}
	else
	{
		//$row = "未知";
		$row = "";
	}
	
	return $row;
}

?>