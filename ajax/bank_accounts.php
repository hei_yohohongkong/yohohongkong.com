<?php

/***
 * order_status.php
 * by howang 2015-06-15
 *
 * Simple AJAX endpoint for checking order status
 ***/

define('IN_ECS', true);

// Skip template engine for faster loading
define('INIT_NO_SMARTY', true);
// Skip update user info for faster loading
define('INIT_NO_UPDATE_USER_INFO', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');

$act = empty($_REQUEST['act']) ? '' : trim($_REQUEST['act']);

if ($act == 'bankings')
{
	$sql = "SELECT p.pay_code,p.pay_id,u.language FROM " . $ecs->table('order_info') . " oi " .
		   "LEFT JOIN " . $ecs->table('payment') . " p ON p.pay_id = oi.pay_id " .
		   "LEFT JOIN " . $ecs->table('users') . " u ON u.user_id = oi.user_id " .
	       "WHERE oi.order_id = '" . $_REQUEST['order_id'] . "'";
	$res = $db->getRow($sql);
	$code = $res['pay_code'];
	$pid = $res['pay_id'];
	$lang = $res['language'];

	$localizable_fields = localizable_fields_for_table('payment');
	$localized_versions = get_localized_versions('payment', $pid, $localizable_fields);
	$pay_config = unserialize($localized_versions[$lang]['pay_config']);

	$sql = "SELECT pay_config FROM " . $ecs->table('payment') . "WHERE pay_code = '" . $code . "'";
	$result = $db->getOne($sql);
	$user = '';
	$banks = [];
	if ($result !== false)
	{
		$store = unserialize($result);
		foreach($store as $key => $value){
			if($value['name'] == 'bank_user')
				$user = $value['value'];
			elseif($value['name'] == 'banks')
				$banks = $value['value'];
		}
		$output = array('bank_user'=>$user,'banks'=>$banks);
		//Multi-language checking
		if($pay_config != "" && $lang != "zh_tw"){
			foreach($pay_config as $key => $value){
				if($value['name'] == 'banks'){
					foreach($value['value'] as $k => $v){
						$output['banks'][$k]['name'] = $v['name'];
					}
				}
			}
		}
	}
	else
	{
		$output = array('status' => 0, 'error' => '找不到資料');
	}
}
elseif ($act == 'update'){
	$sql = "SELECT pay_id FROM " .  $ecs->table('payment') . " WHERE pay_code = '" . $_REQUEST['code'] ."'";
	$pid = $db->getOne($sql);
	$sql = "SELECT * FROM " .  $ecs->table('payment') . " WHERE pay_id = " . $pid;
	$result = $db->getAll($sql);
	$count = 0;
	foreach($result as $r){
		$pay_name = $r['pay_name'];
		$pay_desc = $r['pay_desc'];
		$pay_config = unserialize($r['pay_config']);
		foreach($pay_config as $key => $value){
			if($value['name'] == 'banks'){
				$count = sizeof($value['value']);
				$pay_config[$key]['value'][sizeof($value['value'])] = array('name'=>'銀行名稱','account'=>'銀行帳號');
			}
		}
		$new_pay_config = serialize($pay_config);
		$sql = "UPDATE " .  $ecs->table('payment') . " SET pay_config = '" . $new_pay_config . "' WHERE pay_id = " . $pid;
		$db->query($sql);
	}
	$output = array('count' => $count);
}
elseif ($act == 'init')
{
	$sql = "SELECT pay_id,pay_config FROM " .  $ecs->table('payment') . " WHERE pay_code = '" . $_REQUEST['code'] ."'";
	$res = $db->getRow($sql);
	$pid = $res['pay_id'];
	$pay_config = unserialize($res['pay_config']);
	foreach($pay_config as $key => $value){
		if($value['name'] == 'banks'){
			$sql = "SELECT * FROM " .  $ecs->table('payment') . " WHERE pay_id = $pid";
			$config = $db->getrow($sql);
			$sql = "SELECT lang, pay_config FROM " .  $ecs->table('payment_lang') . " WHERE pay_id = " . $pid;
			$version = $db->getAll($sql);
			if($version == NULL){
				$sql = "INSERT INTO " .  $ecs->table('payment_lang') . " (pay_id, lang, pay_name, pay_desc, pay_config)" .
				" VALUES ('".$config['pay_id']."', 'en_us', '".$config['pay_name']."', '".$config['pay_desc']."', '".$config['pay_config']."')" .
				" ON DUPLICATE KEY UPDATE pay_config = '".$config['pay_config']. "', pay_id = " . $pid;
				$db->query($sql);
			}else{
				foreach($version as $data){
					$lang_config = unserialize($data['pay_config']);
					$hasBank = false;
					foreach($lang_config as $lang_key => $lang_value){
						if($lang_value['name'] == 'banks')
							$hasBank = true;
					}
					if($hasBank == false){
						$sql = "INSERT INTO " .  $ecs->table('payment_lang') . " (pay_id, lang, pay_name, pay_desc, pay_config)" .
						" VALUES ('".$config['pay_id']."', '" . $data['lang'] ."', '".$config['pay_name']."', '".$config['pay_desc']."', '".$config['pay_config']."')" .
						" ON DUPLICATE KEY UPDATE pay_config = '".$config['pay_config']. "', pay_id = " . $pid;
						 $db->query($sql);
					}
				}
			}
		}
	}
}
else
{
	$output = array('status' => 0, 'error' => '非法請求');
}

header('Content-Type: application/json');
echo json_encode($output);
exit;

?>
