<?php

/***
 * cart_popup_box.php
 * by howang 2015-01-06
 *
 * Display cart popup box
 ***/

define('IN_ECS', true);

// Skip template engine for faster loading
define('INIT_NO_SMARTY', true);
// Skip update user info for faster loading
define('INIT_NO_UPDATE_USER_INFO', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');

header('Content-Type: application/json');

if (isset($_REQUEST['total_update']) && $_REQUEST['total_update'] == 'y'){
    $countResult = array(
        'totalCount' => insert_cart_info(),
        'totalPrice' => insert_cart_infoamount()
    );
    echo json_encode($countResult);
} else {
    echo insert_cart_info2();
}

?>