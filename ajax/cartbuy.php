<?php

/***
* cartbuy.php
*
* AJAX add to cart
***/

/**为了解决JQ和ECSHOPjs冲突，单独写了一个简单的表单提交订单的方式**/
/**本表单商品价格只判断了 促销商品，组合优惠，会员组直接价格**/

define('IN_ECS', true);
// Skip template engine for faster loading
define('INIT_NO_SMARTY', true);
// Skip update user info for faster loading
define('INIT_NO_UPDATE_USER_INFO', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');
require(ROOT_PATH . '/includes/lib_order.php');

// 提交数据要产品的ID 和产品的数量
$bc = isset($_REQUEST['ac']) ? $_REQUEST['ac'] : '';

$userController = new Yoho\cms\Controller\UserController();
$flashdealController = new \Yoho\cms\Controller\FlashdealController();
$orderController = new Yoho\cms\Controller\OrderController();
//Blocking Wholesale Customer
if(in_array(@$_SESSION['user_id'],$userController->getWholesaleUserIds())){
	$output = array('status' => 0, 'text' => '批發客戶請直接聯絡營業員訂購', 'totalPrice' => 0);
	header('Content-Type: application/json');
	echo json_encode($output);
	exit;
}

if ($bc == 'ot')
{
	$gnum = isset($_REQUEST['gnum']) ? intval($_REQUEST['gnum']) : 1;
	$gid = isset($_REQUEST['id'])  ? intval($_REQUEST['id']) : (isset($_REQUEST['gid']) ? intval($_REQUEST['gid']) : 0);
	$spec = empty($_REQUEST['gid']) ? array() : array(intval($_REQUEST['gid']));
	$parent = isset($_REQUEST['parent']) ? intval($_REQUEST['parent']) : 0;
	$insurance = isset($_REQUEST['ins_id']) ? ($_REQUEST['ins_id']) : array();
	
	$flashdeal_id = $flashdealController->check_goods_is_flashdeal($gid);
	$original_price = insert_cart_infoamount();
	$original_qty = insert_cart_info();
	if($flashdeal_id > 0) {
		/* 20181023: If flashdeal_id, return false and insert error_log.*/
	 	$ok = false;
	 	$info['info'] = 'User: '.$_SESSION['user_id'].', IP： '.real_ip().', Goods_id: '.$gid;
	 	$info['file'] = basename(__FILE__, '.php');
	 	$info['time'] = gmtime();
	 	$GLOBALS['db']->autoExecute($GLOBALS['ecs']->table('error_log'), $info, 'INSERT');
	} else {
		$ok = addto_cart($gid, $gnum, $spec, $parent);
	}
	
	// howang: check if addto_cart() complete successfully, and return error message if any
	if ($ok)
	{
		$new_price = insert_cart_infoamount();
		if (is_array($insurance) && sizeof($insurance) > 0){
			$all_ok = TRUE;
			$count_insurance = 0;
			foreach($insurance as $ins_id){
				if ($ins_id > 0){
					$ok = addto_cart($ins_id, $gnum, $spec, $parent, $gid);
					//$ok = insurance_addto_cart($gid, $gnum, $spec, $parent, $ins_id);
					if (!$ok){
						$all_ok = FALSE;
					} else {
						$count_insurance++;
					}
				}
			}
			if ($ok && $count_insurance > 0){
				$new_price = insert_cart_infoamount();
				$new_qty = insert_cart_info();
				$output = array('status' => 1, 'count' => $new_qty, 'totalPrice' => $new_price, 'addedAmount' => ($new_price - $original_price), 'addedQty' => ($new_qty - $original_qty));
			} else {
				$output = array('status' => 1, 'count' => insert_cart_info(), 'totalPrice' => $new_price, 'addedAmount' => ($new_price - $original_price));			
			}
		} else {
			$output = array('status' => 1, 'count' => insert_cart_info(), 'totalPrice' => $new_price, 'addedAmount' => ($new_price - $original_price));
		}
	}
	else
	{
		$output = array('status' => 0, 'text' => array_pop($GLOBALS['err']->last_message()), 'totalPrice' => 0);
	}
}
else if ($bc == 'favourable_ot')
{
	$gnum = isset($_REQUEST['gnum']) ? intval($_REQUEST['gnum']) : 1;
	$gid = isset($_REQUEST['id'])  ? intval($_REQUEST['id']) : (isset($_REQUEST['gid']) ? intval($_REQUEST['gid']) : 0);
	$spec = empty($_REQUEST['gid']) ? array() : array(intval($_REQUEST['gid']));
	$parent = isset($_REQUEST['parent']) ? intval($_REQUEST['parent']) : 0;
	$act_id = isset($_REQUEST['act_id']) ? intval($_REQUEST['act_id']) : 0;
	$gift_id = isset($_REQUEST['gift_id']) ? intval($_REQUEST['gift_id']) : 0;
	$flashdeal_id = $flashdealController->check_goods_is_flashdeal($gid);
	$original_price = insert_cart_infoamount();
	$original_qty = insert_cart_info();

	$favourable = favourable_info($act_id);
	if($flashdeal_id > 0) {
		/* 20181023: If flashdeal_id, return false and insert error_log.*/
	 	$ok = false;
	 	$info['info'] = 'User: '.$_SESSION['user_id'].', IP： '.real_ip().', Goods_id: '.$gid;
	 	$info['file'] = basename(__FILE__, '.php');
	 	$info['time'] = gmtime();
	 	$GLOBALS['db']->autoExecute($GLOBALS['ecs']->table('error_log'), $info, 'INSERT');
	} else {
		$ok = favourable_addto_cart($gid, $gnum, $spec, $parent, $act_id, $gift_id);
	}
	
	// howang: check if addto_cart() complete successfully, and return error message if any
	if ($ok)
	{
		$new_price = insert_cart_infoamount();
		$new_qty = insert_cart_info();
		$output = array('status' => 1, 'count' => $new_qty, 'totalPrice' => $new_price, 'addedAmount' => ($new_price - $original_price), 'addedQty' => ($new_qty - $original_qty), 'favourable_kind' => $favourable["act_kind"]);
	}
	else
	{
		$output = array('status' => 0, 'text' => array_pop($GLOBALS['err']->last_message()), 'totalPrice' => 0);
	}
}
else if ($bc == 'package_ot')
{
	$package_id = isset($_REQUEST['package_id']) ? intval($_REQUEST['package_id']) : 0;
	$gnum = isset($_REQUEST['gnum']) ? intval($_REQUEST['gnum']) : 1;
	$gid = isset($_REQUEST['id'])  ? intval($_REQUEST['id']) : (isset($_REQUEST['gid']) ? intval($_REQUEST['gid']) : 0);
	$spec = empty($_REQUEST['gid']) ? array() : array(intval($_REQUEST['gid']));
	$original_qty = insert_cart_info();

	$ok = add_package_to_cart($package_id, $spec);
	
	// check if add_package_to_cart() complete successfully, and return error message if any
	if ($ok)
	{
		$new_price = insert_cart_infoamount();
		$new_qty = insert_cart_info();
		$output = array('status' => 1, 'count' => insert_cart_info(), 'totalPrice' => $new_price, 'addedAmount' => ($new_price - $original_price), 'addedQty' => ($new_qty - $original_qty));
	}
	else
	{
		$output = array('status' => 0, 'text' => array_pop($GLOBALS['err']->last_message()), 'totalPrice' => 0);
	}
}
else if ($bc == 'got')
{
    $secret="6LetXnYUAAAAALa_gKXC2ku_OoA161iAxSQaRXkS";
	$response = $_POST["g"];
	$ip = $_SERVER["REMOTE_ADDR"];
    $verify=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret={$secret}&response={$response}&remoteip={$ip}");
    $captcha_success=json_decode($verify);
    $payload  = $_POST['pd'];
    if ($captcha_success->success==false) {
        //This user was not verified by recaptcha.
        $output = array('status' => 0, 'type'=>'captcha');
    }
    else if ($captcha_success->success==true) {
        //This user is verified by recaptcha
        $output = $flashdealController->flashdealAddToCart($payload);
    }
}
else if($bc == 'pifa')
{
	$goods_id =isset($_REQUEST['id'])  ? intval($_REQUEST['id']) : intval($_REQUEST['gid']);
	$goods_number = isset($_REQUEST['gnum']) ? intval($_REQUEST['gnum']) : 0;
	$spec =array(intval($_REQUEST['gid']));
	
	$count = $GLOBALS['db']->getOne('SELECT goods_number FROM ' .$GLOBALS['ecs']->table('goods').
			" WHERE goods_id = '$goods_id  ' " );
	
	if($goods_number > $count)	
	{
		$output = array('status' => 999, 'count' => insert_cart_info(), 'totalPrice' => insert_cart_infoamount());
	}
	else
	{
		if(!empty($goods_id))
		{
			/* 检查：商品数量是否合法 */
			if (!is_numeric($goods_number))
			{
				$status = 0;
			}
			/* 更新：购物车 */
			else
			{
				$GLOBALS['db']->query("delete from ".$GLOBALS['ecs']->table('cart'). " where goods_id=$goods_id and session_id='".SESS_ID."'");
				addto_cart($goods_id, $goods_number, $spec, $parent = 0);
				$status = 1;
			}
		}
		$output = array('status' => $status, 'count' => insert_cart_info(), 'totalPrice' => insert_cart_infoamount());
	}
}
else
{
	$output = array('status' => 0, 'text' => '非法請求', 'totalPrice' => 0);
}

header('Content-Type: application/json');
echo json_encode($output);
exit;

?>