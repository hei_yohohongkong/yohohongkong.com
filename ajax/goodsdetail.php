<?php

define('IN_ECS', true);

// Skip update user info for faster loading
define('INIT_NO_UPDATE_USER_INFO', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');

/*------------------------------------------------------ */
//-- INPUT
/*------------------------------------------------------ */

$goods_id = isset($_REQUEST['goods_id']) ? intval($_REQUEST['goods_id']) : 0;

 /* 获得商品的信息 */
$goods = get_goods_info($goods_id);

if (!$goods) exit;

// YOHO: Don't show accurate stocks
$goods['goods_number'] = $goods['goods_number'] < 20 ? $goods['goods_number'] : 20;

$smarty->assign('goods', $goods);
$smarty->assign('today', get_today_info());

$smarty->display('goods_desc.dwt');

?>