<?php

/***
* address.php
*
* AJAX user address book
***/

define('IN_ECS', true);

// Skip template engine for faster loading
define('INIT_NO_SMARTY', true);
// Skip update user info for faster loading
define('INIT_NO_UPDATE_USER_INFO', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');
$userController       = new Yoho\cms\Controller\UserController();
$addressController    = new Yoho\cms\Controller\AddressController();
if ($_REQUEST['act'] && $_REQUEST['act'] == 'hkgov')
{
	$is_eng_address = ($_CFG['lang'] == 'en_us') ? true : false;

	if($_REQUEST['check_lang']){ //Using to check is Chi/Eng address without $_CFG['lang'].
		$str = $_REQUEST['kw'];
		$m   = mb_strlen($str,'utf-8');
		$s   = strlen($str);
		$is_eng_address = false;
		if($s==$m){
			$is_eng_address = true; // Eng address
		} else {
			$is_eng_address = false;
		}
	}

	$result = $addressController->hk_gov_address_analysis($_REQUEST['kw'], $is_eng_address, 2);
	foreach ($result as $key => $address) {
		$consignee_list[$key]      = $address['consignee'];
		$format_address_list[$key] = $address['formatted_address'];
	}

	header('Content-Type: application/json');
	echo json_encode(array(
		'status' => 1,
		'consignee_list' => $consignee_list,
		'format_address_list' => $format_address_list
	));
	exit;
}
else if ($_REQUEST['act'] && $_REQUEST['act'] == 'get_format_address')
{
    $address = [];
    $address['country'] = isset($_GET['country']) ? json_str_iconv(trim($_GET['country'])) : 0;
    $address['sign_building'] = isset($_GET['sign_building']) ? json_str_iconv(trim($_GET['sign_building'])) : '';
    $address['phase'] = isset($_GET['phase']) ? json_str_iconv(trim($_GET['phase'])) : '';
    $address['block'] = isset($_GET['block']) ? json_str_iconv(trim($_GET['block'])) : '';
	$address['floor'] = isset($_GET['floor']) ? json_str_iconv(trim($_GET['floor'])) : '';
	$address['address'] = isset($_GET['address']) ? json_str_iconv(trim($_GET['address'])) : '';
    $address['room'] = isset($_GET['room']) ? json_str_iconv(trim($_GET['room'])) : '';
    $address['street_name'] = isset($_GET['street_name']) ? json_str_iconv(trim($_GET['street_name'])) : '';
    $address['street_num'] = isset($_GET['street_num']) ? json_str_iconv(trim($_GET['street_num'])) : '';
    $address['locality'] = isset($_GET['locality']) ? json_str_iconv(trim($_GET['locality'])) : '';
    $address['administrative_area'] = isset($_GET['administrative_area']) ? json_str_iconv(trim($_GET['administrative_area'])) : '';
    $address = $addressController->handle_address_detail($address);

	if ($_LANG['country_' . $address['country']] == "Hong Kong" || $_LANG['country_' . $address['country']] == "香港"){
		$check_result = $addressController->check_order_address_record($address['country'], $address['administrative_area'], $address['sign_building'], $address['phase'], $address['block'], $address['street_name'], $address['street_num']);
		if ($check_result && $check_result['type_id'] !=0){
			$address['type'] = $check_result['type_id'];
		} else {
			$address['type'] = FALSE;
		}
		if ($check_result && $check_result['lift'] !=0){
			$address['lift'] = $check_result['lift'];
		} else {
			$address['lift'] = FALSE;
		}
	} else {
		$address['type'] = $address['lift'] = 0;
	}
	
    header('Content-Type: application/json');
	echo json_encode(array(
		'status'  => 1,
		'error'   => 0,
		'message' => '',
		'address' => $address
	));
	exit;
}else if ($_REQUEST['act'] && $_REQUEST['act'] == 'getList') {
	$data = $_REQUEST;
	$user_id = empty($data['user_id']) ? $_SESSION['user_id']: $data['user_id'];
	include_once('includes/lib_order.php');
	$shipping = shipping_area_info(intval($data['shipping_id']), [intval($data['country'])]);
	include_once('includes/modules/shipping/' . $shipping['shipping_code'] . '.php');
	$shipping_obj    = new $shipping['shipping_code'](unserialize($shipping['configure']));
	if (method_exists($shipping_obj, 'getPickUpList')) {
		$list = $shipping_obj->getPickUpList(true, $shipping['shipping_area_id']);
	} else {
		$list = [];
	}

	$sql = "SELECT * FROM ".$ecs->table('user_address')." WHERE user_id = ".$user_id." AND address_type = '".$addressController::ADDRESS_TYPE_LOCKER."' LIMIT 1 ";
	$address = $db->getRow($sql);
	$type_code = $address['type_code'];
	$consignee = $address['consignee'];
	$tel       = $address['tel'];
	$locker = [];
	if($type_code) {
		$code = unserialize($type_code)[0];
		if (method_exists($shipping_obj, 'getPickUpList')) {
			$tmp_list = $shipping_obj->getPickUpList(0, $data['shipping_area_id']);
		} else {
			$tmp_list = [];
		}
		$locker   = $tmp_list[$code];
		if(unserialize($type_code)[1]) {
			$locker['arCode'] = unserialize($type_code)[1];
		}
		$locker['consignee']  = $consignee;
		$locker['tel']        = $tel;
	}
	header('Content-Type: application/json');
    exit(json_encode(['status' => 1, 'list' => $list, 'locker' => $locker]));
}else if ($_REQUEST['act'] && $_REQUEST['act'] == 'get_verified_addr_break') {
	$data = $_REQUEST;

	$building = $_REQUEST['building'];
	$place = $_REQUEST['place'];
	$street = $_REQUEST['street'];
	$street_num = $_REQUEST['street_num'];
	$admin_area = $_REQUEST['admin_area'];

	$get_result = $addressController->get_verified_addr_break($building, $place, $street, $street_num, $admin_area);

	if ($get_result && sizeof($get_result > 0)){
		header('Content-Type: application/json');
		exit(json_encode(['status' => 1, 'list' => $get_result]));

	} else {
		header('Content-Type: application/json');
		exit(json_encode(['status' => 0]));
	}
	
}
// If user not logged in, we are done here
if (empty($_SESSION['user_id']))
{
	header('Content-Type: application/json');
	echo json_encode(array(
		'status' => 0,
		'addr' => array()
	));
	exit;
}

// Get logged in user's address list
$consignee_list = $userController->get_user_address_list($_SESSION['user_id']);

header('Content-Type: application/json');
echo json_encode(array(
	'status' => 1,
	'addr' => $consignee_list
));

exit;

?>