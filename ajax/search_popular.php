<?php

/***
 * search_popular.php
 * by howang 2014-05-26
 *
 * Return a list of "熱門" keywords in JSON array
 ***/

define('IN_ECS', true);

// Skip session and template engine for faster loading
define('INIT_NO_USERS', true);
define('INIT_NO_SMARTY', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');

if (!empty($GLOBALS['_CFG']['search_keywords']))
{
	$searchkeywords = explode(',', trim($GLOBALS['_CFG']['search_keywords']));
}
else
{
	$searchkeywords = array();
}

header('Content-Type: application/json');
echo json_encode($searchkeywords);
exit;

?>