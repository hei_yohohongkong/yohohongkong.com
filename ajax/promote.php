<?php

/***
* cartbuy.php
*
* AJAX add to cart
***/

/**为了解决JQ和ECSHOPjs冲突，单独写了一个简单的表单提交订单的方式**/
/**本表单商品价格只判断了 促销商品，组合优惠，会员组直接价格**/

define('IN_ECS', true);
// Skip template engine for faster loading
define('INIT_NO_SMARTY', true);
// Skip update user info for faster loading
define('INIT_NO_UPDATE_USER_INFO', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');
require(ROOT_PATH . '/includes/lib_promote.php');

// 提交数据要产品的ID 和产品的数量
$bc = isset($_REQUEST['ac']) ? $_REQUEST['ac'] : '';

$userController = new Yoho\cms\Controller\UserController();
$flashdealController = new \Yoho\cms\Controller\FlashdealController();
$orderController = new Yoho\cms\Controller\OrderController();

if ($bc == 'get_cat_info')
{
	$promote_kind = in_array($_REQUEST['promote_kind'], array("featured", "gift", "redeem", "package", "info")) ? $_REQUEST['promote_kind'] : "featured";
	$cat = isset($_REQUEST['cat'])  ? intval($_REQUEST['cat']) : (isset($_REQUEST['cat']) ? intval($_REQUEST['cat']) : 0);
	
	$promote_info = get_promotion_info_by_cat($cat, $promote_kind);
	$output = array('status' => 1, 'data' => $promote_info);
	/*
	if($cat > 0) {
		$promote_info = get_promotion_info_by_cat($cat, $promote_kind);

		$output = array('status' => 1, 'data' => $promote_info);
	} else {
		$output = array('status' => 0, 'text' => '請選擇產品目錄', 'totalPrice' => 0);
	}
	*/
	
}
else
{
	$output = array('status' => 0, 'text' => '非法請求', 'totalPrice' => 0);
}




header('Content-Type: application/json');
echo json_encode($output);
exit;

?>