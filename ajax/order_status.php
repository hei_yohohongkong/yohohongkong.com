<?php

/***
 * order_status.php
 * by howang 2015-06-15
 *
 * Simple AJAX endpoint for checking order status
 ***/

define('IN_ECS', true);

// Skip template engine for faster loading
define('INIT_NO_SMARTY', true);
// Skip update user info for faster loading
define('INIT_NO_UPDATE_USER_INFO', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');
require(dirname(dirname(__FILE__)) . '/includes/lib_order.php');
require(dirname(dirname(__FILE__)) . '/includes/lib_transaction.php');
require(dirname(dirname(__FILE__)) . '/includes/lib_payment.php');
require(dirname(dirname(__FILE__)) . '/includes/lib_clips.php');

$user_id = $_SESSION['user_id'];
$payment_method_need_alert_on_success = [
	'qfwechatpay',
	'alipayhk',
	'alipaycn',
	'payme',
	'stripegooglepay'
];

if (!$user_id)
{
	header('Content-Type: application/json');
	echo json_encode(array(
		'status' => 0,
		'error' => '請先登入',
	));
	exit;
}

$act = empty($_REQUEST['act']) ? '' : trim($_REQUEST['act']);

if ($act == 'pay_status')
{
	$order_id = $_REQUEST['order_id'];
	
	$sql = "SELECT order_status, pay_status FROM " . $ecs->table('order_info') .
			"WHERE order_id = '" . $order_id . "' " .
			"AND user_id = '" . $user_id . "' ";
	$status = $db->getRow($sql);
	$order_status = $status['order_status'];
	$pay_status = $status['pay_status'];

	// Get Payment Method
	$order = get_order_detail($order_id, $user_id);
	$payment_info = payment_info($order['pay_id']);

    if(isset($_SESSION['webhooks_error'])) {
        unset($_SESSION['webhooks_error']);
        $output = array('status' => 2, 'error' => _L('payment_failed', '付款失敗。如需協助請聯絡我們。'));
    } elseif (in_array($order_status, [OS_CANCELED, OS_INVALID, OS_WAITING_REFUND])) {
		$output = array('status' => 0, 'error' => '訂單無法付款');
	} elseif ($pay_status !== false) {
		$output = array('status' => 1, 'pay_status' => $pay_status);
		if (($pay_status == PS_PAYED) && (in_array($payment_info['pay_code'], $payment_method_need_alert_on_success))) {
			$output['payment_success'] = true;
		} else {
			$output['payment_success'] = false;
		}
	} else {
		$output = array('status' => 0, 'error' => '找不到該訂單');
	}
}
else
{
	$output = array('status' => 0, 'error' => '非法請求');
}

header('Content-Type: application/json');
echo json_encode($output);
exit;

?>