<?php

/***
 * yuding.php
 * by howang 2014-05-28
 *
 * Save pre-order requests from customers
 ***/

define('IN_ECS', true);

// Skip template engine for faster loading
define('INIT_NO_SMARTY', true);
// Skip update user info for faster loading
define('INIT_NO_UPDATE_USER_INFO', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');

if ($_REQUEST['act'] == 'pre_submit')
{
	$pre_type = (empty($_POST['pre_type'])) ? '' : $_POST['pre_type'];
	$goods_id = (empty($_POST['goods_id'])) ? '' : (int)$_POST['goods_id'];
	$pre_name = (empty($_POST['pre_name'])) ? '' : $_POST['pre_name'];
	$pre_im = (empty($_POST['pre_im'])) ? '' : $_POST['pre_im'];
	$pre_value = (empty($_POST['pre_value'])) ? '' : $_POST['pre_value'];
	
	if (empty($pre_type) || empty($goods_id) || empty($pre_name) || empty($pre_im) || empty($pre_value))
	{
		$result = array('status' => '0', 'error' => 'Missing information');
	}
	else if ((!in_array($pre_type, array('pre','dai'))) || ($goods_id <= 0) || (!in_array($pre_im, array('tel','email','whatsapp','weisms'))))
	{
		$result = array('status' => '0', 'error' => 'Invalid information');
	}
	else
	{
		$typeToTableName = array('pre' => 'goods_book', 'dai' => 'goods_dai');
		$table = $typeToTableName[$pre_type];
		
		$user_id = empty($_SESSION['user_id']) ? 0 : $_SESSION['user_id'];
		$user_name = empty($_SESSION['user_name']) ? '' : $_SESSION['user_name'];
		$email = ($pre_im == 'email') ? $pre_value : '';
		
		$sql = "INSERT INTO " . $ecs->table($table) .
				" (goods_id, comment_type, user_id, user_name, email, pre_name, pre_im, pre_value, add_time, ip_address, status, language)".
				" VALUES ('" . $goods_id . "', 2, '" . $user_id . "', '" . $user_name . "', '" . $email . "', '" . $pre_name . "', ".
				" '" . $pre_im . "', '" . $pre_value . "', '" . gmtime() . "', '" . real_ip() . "', '0', '" . $_CFG['lang'] . "')";
		$db->query($sql);
		
		$result = array('status' => '1');
	}
	
	header('Content-Type: application/json');
	echo json_encode($result);
	exit;
}
?>