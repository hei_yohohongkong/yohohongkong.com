<?php

define('IN_ECS', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');
require_once(dirname(dirname(__FILE__)) . '/includes/lib_order.php');
require_once(dirname(dirname(__FILE__)) . '/includes/lib_payment.php');
$stripeBrowserPaymentController = new Yoho\cms\Controller\StripeWebBrowserPaymentController();
$orderController = new Yoho\cms\Controller\OrderController();
$user_id = $_SESSION['user_id'];

if (!$user_id)
{
	header('Content-Type: application/json');
	echo json_encode(array(
		'status' => 0,
		'error' => '請先登入',
	));
	exit;
}

if (empty($_REQUEST['order_sn']) || empty($_REQUEST['payment_id']) ||empty($_REQUEST['token']) || empty($_REQUEST['pay_code'])) {
    header('Content-Type: application/json');
	echo json_encode(array(
		'status' => 0,
		'error' => 'Parameter Error',
	));
	exit;
}
$result = $stripeBrowserPaymentController->stripeBrowserPaymentAction($_REQUEST['order_sn'], $_REQUEST['payment_id'], $_REQUEST['token'], $_REQUEST['pay_code']);

?>
