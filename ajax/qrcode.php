<?php

/***
 * qrcode.php
 * by howang 2017-09-05
 *
 * Create QR code
 ***/

define('IN_ECS', true);

// Skip template engine for faster loading
define('INIT_NO_SMARTY', true);
// Skip update user info for faster loading
define('INIT_NO_UPDATE_USER_INFO', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');
require_once(ROOT_PATH . '/includes/phpqrcode/qrlib.php');

$qr_code_url = empty($_REQUEST['qr_code_url']) ? '' : $_REQUEST['qr_code_url'];
$order_id = empty($_REQUEST['order_id']) ? '0' : $_REQUEST['order_id'];
if (!file_exists('../images/tmpqrcode/')) {
    mkdir('../images/tmpqrcode/', 0777, true);
}
$path = "../images/tmpqrcode/".$order_id.".png";
try {
    QRcode::png($qr_code_url, $path , 'L', 4, 2);
} catch (Exception $e) {        // 被忽略
    echo json_encode(array(
        'status' => 0,
        'qr_code_url' => $qr_code_url,
        'error' => '系統錯誤'
    ));
}
header('Content-Type: application/json');
echo json_encode(array(
    'status' => 1,
    'qr_code_url' => $qr_code_url,
    'qr_code'     => "/images/tmpqrcode/".$order_id.".png"
));
exit;