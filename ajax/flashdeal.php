<?php

/***
 * flashdeal.php
 * by howang 2015-02-11
 *
 * Flash deals
 ***/

define('IN_ECS', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');
require(ROOT_PATH . '/includes/lib_order.php');

$act = empty($_REQUEST['act']) ? '' : trim($_REQUEST['act']);

if ($act == 'addtocart')
{
    $info['info'] = 'User: '.$_SESSION['user_id'].', IP： '.real_ip().', Goods_id: '.$gid;
    $info['file'] = basename(__FILE__, '.php');
    $info['time'] = gmtime();
    $GLOBALS['db']->autoExecute($GLOBALS['ecs']->table('error_log'), $info, 'INSERT');
    return array('status' => 0, 'text' => 'error');
}
elseif ($act == 'notifyme')
{
	$flashdeal_id = empty($_REQUEST['flashdeal_id']) ? 0 : intval($_REQUEST['flashdeal_id']);
	$email = empty($_REQUEST['email']) ? '' : trim($_REQUEST['email']);
	$user_id = empty($_SESSION['user_id']) ? 0 : $_SESSION['user_id'];
	
	if (!preg_match('/^([a-zA-Z0-9_.+-])+@([a-z0-9][a-z0-9-]*\.)+[a-z]{2,5}$/i', $email))
	{
		$output = array('status' => 0, 'text' => _L('flashdeal_notifyme_error_invalid_email', '電郵地址格式錯誤'));
	}
	else
	{
		$sql = "SELECT '1' FROM " . $ecs->table('flashdeal_notify') .
				"WHERE flashdeal_id = '" . $flashdeal_id . "' " .
				"AND email = '" . $email . "' ";
		if ($db->getOne($sql))
		{
			$output = array('status' => 1, 'text' => _L('flashdeal_notifyme_success_duplicate', '您已經登記過了，開賣時會以電郵通知您。'));
		}
		else
		{
			$data = array(
				'flashdeal_id' => $flashdeal_id,
				'email' => $email,
				'user_id' => $user_id,
				'enter_time' => gmtime()
			);
			$db->autoExecute($ecs->table('flashdeal_notify'), $data, 'INSERT');
			
			$output = array('status' => 1, 'text' => _L('flashdeal_notifyme_success', '您已成功登記，開賣時會以電郵通知您。'));
		}
	}
}
else
{
	$output = array('status' => 0, 'text' => '非法請求');
}

header('Content-Type: application/json');
echo json_encode($output);
exit;

?>