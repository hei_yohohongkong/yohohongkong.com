<?php

/***
* ajaxuser_reg.php
*
* AJAX user register
***/

define('IN_ECS', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');

/* 载入语言文件 */
require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/user.php');

require_once(ROOT_PATH . 'includes/lib_passport.php');

$ccc       = empty($_POST['ccc'])        ? '' : trim($_POST['ccc']);
$username  = empty($_POST['mobile'])     ? '' : trim($_POST['mobile']);
$username  = (!empty($ccc) && !empty($username)) ? '+' . $ccc . '-' . $username : '';
$mobile    = empty($_POST['mobile'])     ? '' : trim($_POST['mobile']);
$email     = empty($_POST['email'])      ? '' : trim($_POST['email']);
$password  = empty($_POST['password'])   ? '' : trim($_POST['password']);
$password2 = empty($_POST['password_'])  ? '' : trim($_POST['password_']);
$reference = empty($_POST['reference'])  ? '' : trim($_POST['reference']);

if($reference != '') {
	$status = $GLOBALS['db']->getOne('SELECT count(*) FROM '.$GLOBALS['ecs']->table('users').' WHERE user_id ="'.$reference.'" LIMIT 1');
	if(!$status) {
		$response = array(
			'status' => 0,
			'error' => _L('account_not_exist_ref', '你的推薦號碼不正確'),
		);
		header('Content-Type: application/json');
		echo json_encode($response);
		exit;
	}
}

// If user add reference in here, add cookie:
if($reference && $reference != get_affiliate()) {
	$_GET['u'] = $reference;
	set_affiliate();
}
if (register($username, $password, $email, array('mobile_phone' => $mobile)))
{
	// 處理 UCenter 回傳資料
	$ucdata = empty($user->ucdata) ? '' : $user->ucdata;
	if (!empty($ucdata))
	{
		$_SESSION['cached_ucdata'] = $ucdata;
	}
	
	// howang: Actually the register function includes the login code, so we don't need to call login again
	//$user->login($username, $password, 1);
	
	if($_SESSION['user_id'] > 0)
	{
		// send verification email
		send_regiter_hash_async($_SESSION['user_id']);
		
		update_user_info();
		recalculate_price();//重新计算购物车中的商品价格
		
		$response = array('status' => 1, 'send_verify_sms' => $_CFG['send_verify_sms']);
		if($_SESSION['user_rank_program_special']) {
			$response['redirect'] = $_SESSION['user_rank_program_special']['url'];
		}
	}
	else
	{
		// Shouldn't happen
		$response = array(
			'status' => 0,
			'error' => _L('account_login_failed', '登入失敗！'),
		);
	}
}
else
{
	$response = array(
		'status' => 0,
		'error' => array_pop($GLOBALS['err']->last_message()),
	);
}

header('Content-Type: application/json');
echo json_encode($response);
exit;

?>