<?php

define('IN_ECS', true);

// Skip update user info for faster loading
define('INIT_NO_UPDATE_USER_INFO', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');

/*------------------------------------------------------ */
//-- INPUT
/*------------------------------------------------------ */

$goods_id = isset($_REQUEST['itemId']) ? intval($_REQUEST['itemId']) : 0;
$showPrice = $_REQUEST['showPrice'];

 /* 获得商品的信息 */
$goods = get_goods_info($goods_id);

if (!$goods) exit;

// YOHO: Don't show accurate stocks
$goods['goods_number'] = $goods['goods_number'] < 20 ? $goods['goods_number'] : 20;

$properties = get_goods_properties($goods_id);  // 获得商品的规格和属
$smarty->assign('rank_prices', get_user_rank_prices($goods['goods_id'], $goods['shop_price'])); // 会员等级价格
$smarty->assign('specification', $properties['spe']); // 商品规格
$smarty->assign('goods', $goods);
$smarty->assign('today', get_today_info());

//if($showPrice=="normal")
//{
	$smarty->display('ajax_goodspromote_putong.dwt', $cache_id);
//}
//else if($showPrice=="promote")
//{
//	$smarty->display('ajax_goodspromote_xianshi.dwt', $cache_id);
//}

/**
 * 获得指定商品的各会员等级对应的价格
 *
 * @access  public
 * @param   integer     $goods_id
 * @return  array
 */
function get_user_rank_prices($goods_id, $shop_price)
{
    $goods_id = (int) $goods_id;
    $shop_price = (double) $shop_price;
    $sql = "SELECT rank_id, IFNULL(mp.user_price, r.discount * $shop_price / 100) AS price, r.rank_name, r.discount " .
            'FROM ' . $GLOBALS['ecs']->table('user_rank') . ' AS r ' .
            'LEFT JOIN ' . $GLOBALS['ecs']->table('member_price') . " AS mp ".
                "ON mp.goods_id = '$goods_id' AND mp.user_rank = r.rank_id " .
            "WHERE r.show_price = 1 OR r.rank_id = '$_SESSION[user_rank]'";
    $res = $GLOBALS['db']->query($sql);

    $arr = array();
    while ($row = $GLOBALS['db']->fetchRow($res))
    {

        $arr[$row['rank_id']] = array(
                        'rank_name' => htmlspecialchars($row['rank_name']),
                        'price'     => price_format($row['price']));
    }

    return $arr;
}

?>