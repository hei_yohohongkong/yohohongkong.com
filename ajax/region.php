<?php

/***
* region.php
* by howang 2015-01-09
*
* Get current level region list
***/

define('IN_ECS', true);

// Skip session and template engine for faster loading
define('INIT_NO_USERS', true);
define('INIT_NO_SMARTY', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');

$parent = !empty($_REQUEST['id']) ? intval($_REQUEST['id']) : 0;

$sql = 'SELECT region_id, region_name FROM ' . $ecs->table('region') .
        " WHERE parent_id = '$parent'";
$regions = $db->getAll($sql);
$output = array();
foreach ($regions as $region)
{
	$arr = array();
	$arr['id'] = $region['region_id'];
	$arr['name'] = $region['region_name'];
	$arr['parent'] = $parent;
	$output[] = $arr;
}

header('Content-Type: application/json');
echo json_encode($output);
exit;

?>