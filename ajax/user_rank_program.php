<?php

define('IN_ECS', true);
require dirname(dirname(__FILE__)) . '/includes/init.php';
require_once dirname(dirname(__FILE__)) . '/includes/cls_json.php';

$json = new JSON;

ini_set("memory_limit", "256M");

if ($_REQUEST['act'] == "get_program") {
    if (empty($_REQUEST['pid'])) {
        $response = [
            "error" => 1,
            "msg" => "user_rank_program_not_selected"
        ];
    } else {
        $sql = "SELECT program_id, program_name, validate FROM " . $ecs->table("user_rank_program") . " WHERE program_id = $_REQUEST[pid]";
        $program = $db->getRow($sql);
        if (empty($program)) {
            $response = [
                "error" => 1,
                "msg" => "user_rank_program_not_found"
            ];
        } else {
            $response = [
                "error" => 0,
                "data" => [
                    "program" => $program,
                    "user_name" => $_SESSION['user_name'],
                    "secret" => md5("$_SESSION[user_id]_$_REQUEST[pid]")
                ]
            ];
        }
    }
} elseif ($_REQUEST['act'] == "apply_program") {
    if (empty($_REQUEST['pid'])) {
        $response = [
            "error" => 1,
            "msg" => "user_rank_program_not_selected"
        ];
    } else {
        $userRankController = new \Yoho\cms\Controller\UserRankController();
        $response = $userRankController->sumbitUserRegister($_REQUEST);
    }
} elseif ($_REQUEST['act'] == "secret_file") {
    if (empty($_REQUEST['pid'])) {
        $response = [
            "error" => 1,
            "msg" => "user_rank_program_not_selected"
        ];
    }
    $rank_program_file_data = $_REQUEST['secret'];
    $user_id = $_SESSION['user_id'];
    $pid = $_REQUEST['pid'];
    $_SESSION['user_rank_program_data'][$pid] = $rank_program_file_data;
    $response = [
        "error" => 0,
        "msg" => "user_rank_program_received"
    ];
}

header('Content-Type: application/json');
echo json_encode($response);
exit;
?>