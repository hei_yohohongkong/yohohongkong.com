<?php

/***
* ajaxuser.php
*
* AJAX user login
***/

define('IN_ECS', true);

// Skip template engine for faster loading
define('INIT_NO_SMARTY', true);

// If the user is already logged in, we will replace the session with new login session, 
// so updating user info for logged in user here is meaningless
define('INIT_NO_UPDATE_USER_INFO', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');

require_once(ROOT_PATH . 'includes/lib_passport.php');

$ccc = empty($_REQUEST['ccc']) ? '' : $_REQUEST['ccc'];
$username = empty($_REQUEST['username']) ? '' : $_REQUEST['username'];
$password = empty($_REQUEST['password']) ? '' : $_REQUEST['password'];

if (!empty($ccc))
{
	$username = '+' . $ccc . '-' . $username;
}
$user->login($username, $password, 1);

if($_SESSION['user_id'] > 0)
{
	// 處理 UCenter 回傳資料
	$ucdata = empty($user->ucdata) ? '' : $user->ucdata;
	if (!empty($ucdata))
	{
		$_SESSION['cached_ucdata'] = $ucdata;
	}
	
	restore_user_saved_language();
	update_user_info();
	recalculate_price();//重新计算购物车中的商品价格
	
	$sql = 'SELECT info_completed FROM ' . $ecs->table('users') . "  WHERE user_id = '" . $_SESSION['user_id'] . "'";
	$user_info_completed = $db->getOne($sql);
	$first_login = ($user_info_completed == 0);
	
	$array = array('status' => 1, 'first_login' => $first_login);
	if($_SESSION['user_rank_program_special']) {
		$array['redirect'] = $_SESSION['user_rank_program_special']['url'];
	}

	if (!empty($_SESSION['redirect_to'])) {
		$array['redirect'] = $_SESSION['redirect_to'];
		unset($_SESSION['redirect']);
	}
}
else
{
	$_SESSION['login_fail']++;
	
	$array = array('status' => 0); 
}

// JSONP
if (!empty($_REQUEST['callback']))
{
	header('Content-Type: application/javascript');
	echo $_REQUEST['callback'] . '(' . json_encode($array) . ');';
}
else
{
	header('Content-Type: application/json');
	echo json_encode($array);
}
exit;

?>