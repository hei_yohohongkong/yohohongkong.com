<?php

/***
 * feedback.php
 * by howang 2015-06-24
 *
 * Save feedbacks from customers
 ***/

define('IN_ECS', true);

// Skip template engine for faster loading
define('INIT_NO_SMARTY', true);
// Skip update user info for faster loading
define('INIT_NO_UPDATE_USER_INFO', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');

if (($_REQUEST['act'] == 'submit_problem') || ($_REQUEST['act'] == 'submit_price_suggestion'))
{
	$goods_id = (empty($_POST['goods_id'])) ? '' : (int)$_POST['goods_id'];
	$user_id = empty($_SESSION['user_id']) ? 0 : intval($_SESSION['user_id']);
	$user_name = empty($_SESSION['user_name']) ? '' : mysql_escape_string($_SESSION['user_name']);
	
	if (empty($goods_id))
	{
		outputJSON(array('status' => '0', 'error' => _L('goods_feedback_error_no_goods_id', '沒有選擇產品')));
	}
	if ($user_id <= 0)
	{
		outputJSON(array('status' => '0', 'error' => _L('goods_feedback_error_not_logged_in', '請先登入'), 'showlogin' => 1));
	}
	
	$feedback_type = '';
	$content = '';
	
	if ($_REQUEST['act'] == 'submit_problem')
	{
		$problem = (empty($_POST['problem'])) ? '' : trim($_POST['problem']);
		if (empty($problem))
		{
			outputJSON(array('status' => '0', 'error' => _L('goods_feedback_error_empty_content', '請填寫資料')));
		}
		$feedback_type = 'problem';
		$content = $problem;
	}
	elseif ($_REQUEST['act'] == 'submit_price_suggestion')
	{
		$price = (empty($_POST['price'])) ? '' : trim($_POST['price']);
		$source = (empty($_POST['source'])) ? '' : trim($_POST['source']);
		$comment = (empty($_POST['comment'])) ? '' : trim($_POST['comment']);
		if (empty($price))
		{
			outputJSON(array('status' => '0', 'error' => _L('goods_feedback_error_empty_price', '請填寫售價')));
		}
		if (empty($source))
		{
			outputJSON(array('status' => '0', 'error' => _L('goods_feedback_error_empty_source', '請填寫報價來源')));
		}
		$feedback_type = 'price';
		$content = mysql_escape_string(json_encode(array_map('stripslashes', compact('price', 'source', 'comment'))));
	}
	
	$sql = "INSERT INTO " . $ecs->table('goods_feedback') .
			" (goods_id, feedback_type, user_id, user_name, content, add_time, status)".
			" VALUES ('" . $goods_id . "', '" . $feedback_type . "', '" . $user_id . "', '" . $user_name . "', '" . $content . "', ".
			" '" . gmtime() . "', 'pending')";
	$db->query($sql);
	
	outputJSON(array('status' => '1'));
}

function outputJSON($result)
{
	header('Content-Type: application/json;charset=utf-8');
	echo json_encode($result);
	exit;
}
?>