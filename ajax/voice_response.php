<?php

/**
 * Interactive voice response for CRM
 */

define('IN_ECS', true);

require dirname(dirname(__FILE__)) . '/includes/init.php';
require dirname(dirname(__FILE__)) . '/includes/twiliosdk/twilio/sdk/Twilio/autoload.php';

CONST BASE_URL = "/ajax/voice_response.php";
CONST STEP_GREETING = 0;
CONST STEP_MENU     = 1;
CONST STEP_CALL     = 2;
CONST STEP_PROMOTE  = 3;
CONST STEP_LANG     = 4;
CONST STEP_ENQUEUE  = 5;
CONST STEP_QUEUE    = 6;
CONST STEP_DEQUEUE  = 7;
CONST STEP_BRIDGED  = 8;

$crmController = new Yoho\cms\Controller\CrmController();

CONST LANG_CANTONESE = 1;
CONST LANG_ENGLISH   = 2;
CONST LANG_MANDARIN  = 3;
CONST LANG = Yoho\cms\Controller\CrmController::IVR_LANG_CONFIG;

$ivr_language = $crmController->get_ivr_language();
$ivr_language_enabled = $crmController->get_ivr_language_enabled();

$step = empty($_REQUEST['step']) ? STEP_GREETING : intval($_REQUEST['step']);
$lang = empty($_REQUEST['lang']) ? $crmController::IVR_LANG_CANTONESE : intval($_REQUEST['lang']);
if (!in_array($lang, $ivr_language_enabled)) {
    $lang = $crmController::IVR_LANG_CANTONESE;
}

if (trim($_REQUEST['CallStatus']) == 'completed') {
    $ticket_id = $db->getOne("SELECT ticket_id FROM " . $ecs->table("crm_list") . " WHERE unique_Id = '$_REQUEST[CallSid]'");
    $levels = returnLevels();
    $type = CRM_REQUEST_UNCATEGORIZED;
    if (count($levels) > 1) {
        $type = intval($levels[1]) + 1;
    }
    $db->query("UPDATE " . $ecs->table('crm_ticket_info') . " SET type = $type, status = " . CRM_STATUS_FINISHED . ", last_update = CURRENT_TIMESTAMP WHERE ticket_id = $ticket_id");
    $db->query("INSERT INTO " . $ecs->table('crm_log') . " (ticket_id, admin_id, type) VALUES ($ticket_id, 1, " . CRM_LOG_TYPE_UPDATE . ")");
}

switch ($step) {
    case STEP_GREETING:
        showWelcome();
        break;
    case STEP_MENU:
        showMenu();
        break;
    case STEP_PROMOTE:
        showPromote();
        break;
    case STEP_CALL:
        dialToAircall();
        break;
    case STEP_LANG:
        setLanguage();
        break;
    case STEP_ENQUEUE:
        enqueueCall();
        break;
    case STEP_QUEUE:
        enqueueWaiting();
        break;
    case STEP_DEQUEUE:
        dequeueCall();
        break;
    case STEP_BRIDGED:
        bridgedCall();
        break;
}

function showWelcome()
{
    global $crmController, $ivr_language, $ivr_language_enabled, $lang;
    if (!empty($_REQUEST['CallSid'])) {
        $crmController->createTicket($_REQUEST['CallSid'], $_REQUEST['From'], CRM_PLATFORM_TWILIO, $_REQUEST['From'], date('Y-m-d H:i:s'));
    }
    $response = new Twilio\Twiml();
    foreach ($ivr_language_enabled as $curr_lang) {
        addSayOrPlay($response, $ivr_language["GREET"], $curr_lang);
    }
    if (count($ivr_language_enabled) > 1) {
        $gather = $response->gather(
            [
                'numDigits' => 1,
                'action' => buildURL(["step" => STEP_LANG])
            ]
        );
        foreach ($ivr_language_enabled as $curr_lang) {
            addSay($gather, pressKey($crmController::IVR_LANG_NAME, $curr_lang, $curr_lang), $curr_lang);
        }
        addSayOrPlay($response, $ivr_language["RESPOND"], $lang);
        addHangup($response);
    } else {
        $response->redirect(buildURL(["step" => STEP_PROMOTE]));
    }
    outputXml($response);
}

function setLanguage()
{
    global $lang, $ivr_language_enabled;

    $selected_language = empty($_REQUEST['Digits']) ? $lang : trim($_REQUEST['Digits']);
    if (!in_array($selected_language, $ivr_language_enabled)) {
        $selected_language = $lang;
    }
    $lang = $selected_language;
    $response = new Twilio\Twiml();
    $response->redirect(buildURL(["step" => STEP_PROMOTE]));
    outputXml($response);
}

function showPromote()
{
    global $lang, $ivr_language;
    $response = new Twilio\Twiml();
    if ($_REQUEST['From'] != "+85298365415" && $_REQUEST['From'] != "+85268715490") {
        addSayOrPlay($response, $ivr_language["PROMOTE"], $lang);
    }
    $response->redirect(buildURL(["step" => STEP_MENU]));
    outputXml($response);
}

function showMenu()
{
    global $lang, $ivr_language;
    $response = new Twilio\Twiml();
    $levels = returnLevels();
    $current_menu = [['menu' => $ivr_language["MENU"]]];
    foreach ($levels as $level) {
        if (array_key_exists($level, $current_menu)) {
            if (array_key_exists('menu', $current_menu[$level])) {
                if (!empty($current_menu[$level]['menu'])) {
                    $current_menu = $current_menu[$level]['menu'];
                }
            } elseif (array_key_exists('ans', $current_menu[$level]) || array_key_exists('red', $current_menu[$level]) || array_key_exists('rec', $current_menu[$level])) {
                $current_menu = [
                    'ans' => $current_menu[$level]['ans'],
                    'red' => $current_menu[$level]['red'],
                    'rec' => $current_menu[$level]['rec'],
                ];
            }
        } else {
            $current_menu = "invalid";
            addInvalid($response, $levels);
        }
    }

    if (array_key_exists("ans", $current_menu)) {
        addSayOrPlay($response, $current_menu, $lang, true);
        if (!empty($current_menu['red'])) {
            addDial($response);
        }
        addPause($response, 2);
        array_pop($levels);
        $param = [
            "step"  => STEP_MENU,
            "level" => implode("_", $levels),
        ];
        $response->redirect(buildURL($param));
    } elseif (is_array($current_menu)) {
        $param = [
            "step"  => STEP_MENU,
            "level" => implode("_", $levels),
        ];
        $gather = $response->gather(
            [
                'numDigits' => 1,
                'action' => buildURL($param)
            ]
        );
        foreach ($current_menu as $key => $select) {
            if (!empty($select['menu']) || !empty($select['ans']) || !empty($select['red'])) {
                if (!empty($select['namerec'][$lang])) {
                    addPlay($gather, '/ajax/voice_recording/' . $select['namerec'][$lang]);
                    addSay($gather, pressKey("", (intval($key) + 1)), $lang);
                } else {
                    addSay($gather, pressKey($select['name'], (intval($key) + 1)), $lang);
                }
            }
        }
        addRepeat($gather);
        if (count($levels) > 1) {
            addPrev($gather);
        }
        addSayOrPlay($response, $ivr_language["RESPOND"], $lang);
        if (!empty($_REQUEST['repeat'])) {
            addHangup($response);
        } else {
            $param['repeat'] = 1;
            $response->redirect(buildURL($param));
        }
    }

    outputXml($response);
}

function addPause($response, $sec = 1)
{
    $response->pause(['length' => $sec]);
}

function addPrev($response)
{
    global $lang, $ivr_language;
    addSay($response, pressKey("BACK", "*"), $lang);
}

function addRepeat($response)
{
    global $lang, $ivr_language;
    addSay($response, pressKey("REPEAT", "0"), $lang);
}

function addDial($response, $levels)
{
    global $lang, $ivr_language;

    $current_time = intval(local_date("His"));
    if (gmtime() >= local_strtotime('2019-02-04 16:30:00') && gmtime() <= local_strtotime('2019-02-08')){
        addSayOrPlay($response, $ivr_language["OUTOFSERVICE"], $lang);
    } elseif ($current_time > 110000 && $current_time < 190000) {
        $levels = returnLevels();
        addSayOrPlay($response, $ivr_language["REDIRECT"], $lang);
        $param = [
            "step"  => STEP_ENQUEUE,
            // "step"  => strpos($_SERVER['SERVER_NAME'], 'beta.') !== false ? STEP_ENQUEUE : STEP_CALL,
            "level" => implode("_", $levels),
        ];
        $response->redirect(buildURL($param));
    } else {
        addSayOrPlay($response, $ivr_language["OUTOFSERVICE"], $lang);
    }
}

function dialToAircall()
{
    global $db, $ecs;
    $caller = trim($_REQUEST['From']);

    $response = new Twilio\Twiml();
    $dial = $response->dial('', ['callerId' => $caller, 'hangupOnStar' => true]);
    $config = [
        "statusCallbackEvent" => 'initiated answered completed',
        'statusCallback' => 'https://' . $_SERVER['SERVER_NAME'] . '/api/twilioVoiceWebhook.php?level=' . $_REQUEST['level'],
        'statusCallbackMethod' => 'POST',
    ];
    $dial->number('+85258030047', $config);
    addHangup($response);
    outputXml($response);
}

function enqueueCall()
{
    global $db, $ecs, $crmController;
    $levels = returnLevels();
    if (!empty($_REQUEST['CallSid'])) {
        $ticket_id = $crmController->createTicket($_REQUEST['CallSid'], $_REQUEST['From'], CRM_PLATFORM_TWILIO, $_REQUEST['From'], date('Y-m-d H:i:s'));
        $list_id = $db->getOne("SELECT list_id FROM " . $ecs->table("crm_list") . " WHERE unique_id = '$_REQUEST[CallSid]'");
        $db->query("INSERT INTO " . $ecs->table("crm_twilio_call") . " (list_id, duration, levels) VALUES ($list_id, -1, '$_REQUEST[level]')");
        $db->query("INSERT INTO " . $ecs->table("sse") . " (time, target, event, data) VALUES (" . gmtime() . ", 'crm', 'twilio', '$ticket_id-queued')");

        // Update ticket status
        $type = CRM_REQUEST_UNCATEGORIZED;
        if (count($levels) > 1) {
            $type = intval($levels[1]) + 1;
        }
        $db->query("UPDATE " . $ecs->table('crm_ticket_info') . " SET type = $type, status = " . CRM_STATUS_PENDING . ", last_update = CURRENT_TIMESTAMP WHERE ticket_id = $ticket_id");
        $db->query("INSERT INTO " . $ecs->table('crm_log') . " (ticket_id, admin_id, type) VALUES ($ticket_id, 1, " . CRM_LOG_TYPE_UPDATE . ")");
    }

    $response = new Twilio\Twiml();
    $param = [
        "step"  => STEP_QUEUE,
        "level" => implode("_", $levels),
    ];
    $param2 = [
        "step"  => STEP_DEQUEUE,
    ];
    $config = [
        'waitUrl' => buildURL($param),
        'action' => buildURL($param2),
    ];
    $response->enqueue((strpos($_SERVER['SERVER_NAME'], 'beta.') !== false ? 'test-' : '') . "support", $config);
    outputXml($response);
}

function dequeueCall()
{
    global $db, $ecs, $crmController;
    $result = trim($_REQUEST['QueueResult']);
    $action = 'error';
    $ticket_id = $crmController->createTicket($_REQUEST['CallSid'], $_REQUEST['From'], CRM_PLATFORM_TWILIO, $_REQUEST['From'], date('Y-m-d H:i:s'));
    $list_id = $db->getOne("SELECT list_id FROM " . $ecs->table("crm_list") . " WHERE unique_id = '$_REQUEST[CallSid]'");
    if (in_array($result, ["bridged", "bridging-in-process"])) {
        $action = 'bridged';
    } elseif (in_array($result, ["hangup", "leave"])) {
        $action = 'hangup';
    } elseif (in_array($result, ["redirected", "redirected-from-bridged"])) {
        $action = 'redirected';
    }
    if ($action != "bridged") {
        $db->query("INSERT INTO " . $ecs->table("sse") . " (time, target, event, data) VALUES (" . gmtime() . ", 'crm', 'twilio', '$ticket_id-$action')");
        if (empty($_REQUEST['QueueTime'])) {
            $_REQUEST['QueueTime'] = 1;
        }
        $db->query("UPDATE " . $ecs->table("crm_twilio_call") . " SET waited = $_REQUEST[QueueTime] WHERE list_id = $list_id");
    } else {
        $db->query("DELETE FROM " . $ecs->table("crm_twilio_call") . " WHERE admin_id = '0' AND waited = '0' AND duration = '-1' AND list_id < $list_id");
    }

    $response = new Twilio\Twiml();
    addHangup($response);
    outputXml($response);
}

function enqueueWaiting()
{
    global $db, $ecs, $lang, $ivr_language;
    $response = new Twilio\Twiml();
    if ($_REQUEST['QueueTime'] >= 30) {
        $list_id = $db->getOne("SELECT list_id FROM " . $ecs->table("crm_list") . " WHERE unique_id = '$_REQUEST[CallSid]'");
        $db->query("UPDATE " . $ecs->table("crm_twilio_call") . " SET waited = $_REQUEST[QueueTime] WHERE list_id = $list_id");
        addSayOrPlay($response, $ivr_language["REDIRECTFAIL"], $lang);
        addHangup($response);
    } else {
        addPlay($response, '/ajax/voice_waiting.wav', ['loop' => 5]);
    }
    outputXml($response);
}

function addInvalid($response, $levels = [0], $return = true)
{
    global $lang, $ivr_language;
    addSayOrPlay($response, $ivr_language["INVALID"], $lang);
    if ($return) {
        array_pop($levels);
        $param = [
            "step"  => STEP_MENU,
            "level" => implode("_", $levels),
        ];
        $response->redirect(buildURL($param));
    }
}

function returnLevels()
{
    $levels = empty($_REQUEST['level']) ? [0] : explode("_", trim($_REQUEST['level']));
    if (!empty($_REQUEST['Digits'])) {
        if (trim($_REQUEST['Digits']) == "*") {
            array_pop($levels);
        } else {
            $levels[] = intval(trim($_REQUEST['Digits'])) - 1;
        }
    }
    return $levels;
}
function outputXml($response)
{
    header('Content-Type: text/xml');
    echo $response;
    exit;
}

function pressKey($item, $key, $target_lang)
{
    global $lang, $ivr_language;
    $target_lang = empty($target_lang) ? $lang : $target_lang;
    if (is_array($item)) {
        return $item[$target_lang] . " " . $ivr_language["PRESS"][$target_lang] . ",,,,$key,,,,";
    }
    return $ivr_language[$item][$target_lang] . " " . $ivr_language["PRESS"][$target_lang] . ",,,,$key,,,,";
}

function addSay($response, $message, $lang = 1)
{
    if (!empty($message)) {
        $response->say($message, LANG[$lang]);
    }
}

function buildURL($param)
{
    global $lang;
    $url = BASE_URL . "?lang=$lang";
    foreach ($param as $key => $value) {
        $url .= "&$key=$value";
    }
    return $url;
}

function addHangup($response)
{
    global $lang, $ivr_language;
    addSayOrPlay($response, $ivr_language["END"], $lang);
    $response->hangup();
}

function bridgedCall()
{
    global $lang, $ivr_language, $db, $ecs, $crmController;
    $ticket_id = $crmController->createTicket($_REQUEST['CallSid'], $_REQUEST['From'], CRM_PLATFORM_TWILIO, $_REQUEST['From'], date('Y-m-d H:i:s'));
    $list_id = $db->getOne("SELECT list_id FROM " . $ecs->table("crm_list") . " WHERE unique_id = '$_REQUEST[CallSid]'");
    $db->query("INSERT INTO " . $ecs->table("sse") . " (time, target, event, data) VALUES (" . gmtime() . ", 'crm', 'twilio', '$ticket_id-bridged')");
    $db->query("UPDATE " . $ecs->table("crm_twilio_call") . " SET waited = $_REQUEST[QueueTime], duration = 0 WHERE list_id = $list_id");

    $response = new Twilio\Twiml();
    $response->play('/ajax/beep.wav');
    outputXml($response);
}

function addPlay($response, $path, $config)
{
    $response->play($path, $config);
}

function addSayOrPlay($response, $languages, $curr_lang, $ans = false)
{
    if (empty($languages['rec'][$curr_lang])) {
        if ($ans) {
            addSay($response, $languages['ans'][$curr_lang], $curr_lang);
        } else {
            addSay($response, $languages[$curr_lang], $curr_lang);
        }
    } else {
        addPlay($response, '/ajax/voice_recording/' . $languages['rec'][$curr_lang]);
    }
}