<?php

/***
* checkout.php
*
* AJAX checkout page
***/

define('IN_ECS', true);

// Skip template engine for faster loading
define('INIT_NO_SMARTY', true);
// Skip update user info for faster loading
define('INIT_NO_UPDATE_USER_INFO', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');

// define('DESKTOP_ONLY', true); // This page don't have mobile version

define('FORCE_HTTPS', true); // Require HTTPS for security
require(ROOT_PATH . 'includes/lib_order.php');

/* 载入语言文件 */
require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/user.php');
require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/shopping_flow.php');

$paymentController = new Yoho\cms\Controller\PaymentController();
$orderController = new Yoho\cms\Controller\OrderController();
$shippingController = new Yoho\cms\Controller\ShippingController();
$order_fee_platform = $orderController::ORDER_FEE_PLATFORM_WEB;
$actg = $paymentController->actg;
$actgHooks = $paymentController->actgHooks;
$action = empty($_REQUEST['act']) ? '' : $_REQUEST['act'];

// Validate integral / discount / coupon
if ($action == 'calculate_amount')
{

    $total = empty($_REQUEST['total']) ? 0 : intval($_REQUEST['total']);
    $shipping = empty($_REQUEST['shipping']) ? 0 : intval($_REQUEST['shipping']);
    $shipping_area = empty($_REQUEST['shipping_area']) ? 0 : intval($_REQUEST['shipping_area']);
    $payment = empty($_REQUEST['payment']) ? 0 : intval($_REQUEST['payment']);
    $surplus = empty($_REQUEST['surplus']) ? 0 : floatval($_REQUEST['surplus']);
    $coupon_code = empty($_REQUEST['coupon']) ? array() : $_REQUEST['coupon'];
    $integral = empty($_REQUEST['integral']) ? 0 : floatval($_REQUEST['integral']);
    $shipping_fod = empty($_POST['shipping_fod']) ? 0 : $_POST['shipping_fod'];
    $shipping_type = empty($_POST['shipping_type']) ? 0 : $_POST['shipping_type'];
    $user_id = $_SESSION['user_id'];

    // If user not logged in, we are done here
    // if (empty($_SESSION['user_id']))
    // {
    // 	header('Content-Type: application/json');
    // 	echo json_encode(array(
    // 		'status' => 0
    // 	));
    // 	exit;
    // }

    /* 取得購物類型 */
    $flow_type = isset($_SESSION['flow_type']) ? intval($_SESSION['flow_type']) : CART_GENERAL_GOODS;

    /* 獲得收貨人信息 */
    $consignee = get_consignee($_SESSION['user_id']);
    if(empty($consignee) && empty($_SESSION['user_id']) || isset($_REQUEST['is_quotation']))
    {
        $consignee = array();
        $consignee['country'] = '3409';
        $consignee['province'] = '0';
        $consignee['city'] = '0';
        $consignee['district'] = '0';
    }
    /* 對商品信息賦值 */
    $cart_goods = cart_goods(); // 取得商品列表，計算合計
    $order = array();
    /* 取得訂單信息 */
    $order = flow_order_info();

    // Apply Shipping / Payment method if provided
    if (isset($shipping))
    {
        $order['shipping_id'] = $shipping;
    }
    if (isset($shipping_area))
    {
        $order['shipping_area_id'] = $shipping_area;
    }
    if (isset($payment))
    {
        $order['pay_id'] = $payment;
    }

    $order['shipping_fod'] = $shipping_fod;

    // Reset coupons before calculation
    $order['coupon_id'] = array();
    if (isset($order['exclusive_coupon'])) unset($order['exclusive_coupon']);

    // Update order type
    $order['order_type'] = NULL;

    // Apply coupon if provided
    if (!empty($coupon_code))
    {
        // trim whitspace -> filter empty string -> upper case -> unique
        //$coupon_code = array_unique(array_map('strtoupper', array_filter(array_map('trim', $coupon_code))));

        $invalid_codes = array();
        $error_msg = '';

        foreach ($coupon_code as $code)
        {
            $coupon = coupon_info(0, $code);

            $error = 0;
            if (verify_coupon($coupon, $order, cart_amount(true, $flow_type), $cart_goods, $user_id, $error))
            {
                // Assign coupon_id to order
                $order['coupon_id'][] = $coupon['coupon_id'];
            }
            else
            {
                $invalid_codes[] = $code;
                $error_msg .= (empty($error_msg) ? '' : '<br>') . '優惠券「' . $code . '」驗證失敗： ' . coupon_error_message($error);
            }
        }

        if (!empty($invalid_codes))
        {
            $valid_codes = array_filter($coupon_code, function ($c) use ($invalid_codes) { return !in_array($c, $invalid_codes); });
            header('Content-Type: application/json');
            echo json_encode(array(
                'status' => 0,
                'coupon_code' => implode(',', $valid_codes),
                'error' => $error_msg
            ));
        }
    }

    // Apply integral to use
    if ($user_id > 0)
    {
        $user_info = user_info($user_id);

        $order['surplus'] = min($order['surplus'], $user_info['user_money'] + $user_info['credit_line']);
        if ($order['surplus'] < 0)
        {
            $order['surplus'] = 0;
        }

        // 查询用户有多少积分
        $user_points = $user_info['pay_points']; // 用户的积分总数

        $order['integral'] = min($integral, $user_points);
        if ($order['integral'] < 0)
        {
            $order['integral'] = 0;
        }
    }
    else
    {
        $order['surplus']  = 0;
        $order['integral'] = 0;
    }

    $order['cn_ship_tax'] = 0;

    $total = $orderController->order_fee_flow($order, $cart_goods, $consignee, $order_fee_platform);
    
    $total['cart_max_integral'] = min($user_points, $total['max_integral']);
    if ($deposit > 0)
    {
        $due_amount = $total['amount'] - $deposit;
    }

    header('Content-Type: application/json');
    echo json_encode(array(
        'status' => 1,
        'total' => $total
    ));
    exit;

}
if ($action == 'select_step')
{
    /*------------------------------------------------------ */
    //-- 改变配送顃型
    /*------------------------------------------------------ */
    include_once('includes/cls_json.php');
    $json = new JSON;

    $shipping_type_code = $_REQUEST['code'];
    $step               = empty($_REQUEST['step'])? 'shipping_type' : $_REQUEST['step'];
    $data               = $_REQUEST['data'];
    $nextStep = $orderController->getStepByShippingType($shipping_type_code, $step, $data, $order_fee_platform);
    $result = array('status' => 1, 'nextStep' => $nextStep);

    echo $json->encode($result);
    exit;

}

if ($action == 'check_coupon_promote')
{
    include_once('includes/cls_json.php');
    $json = new JSON;
    $result = $orderController->checkCouponPromote($_REQUEST['coupons'],$_REQUEST['payment_type']); 

    header('Content-Type: application/json');
    echo $json->encode($result);
    exit;
}

if ($action == 'check_coupon_promote_credit_card_bin') {
    include_once('includes/cls_json.php');
    $json = new JSON;
    $result = $orderController->checkCouponPromoteCreditCardBin($_REQUEST['order_id'],$_REQUEST['credit_card_bin']); 

    header('Content-Type: application/json');
    echo $json->encode($result);
    exit;
}

if ($action == 'check_address_info_filled') {
    include_once('includes/cls_json.php');
    $json = new JSON;
    $result = $orderController->checkAddressInfoFilled($_REQUEST['user_addr_id']); 
    header('Content-Type: application/json');
    $arr_result = array('status' => 1, 'res' => $result);
    echo $json->encode($arr_result);
    exit;
}

?>
