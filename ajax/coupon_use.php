<?php

/***
 * coupon_use.php
 * by howang 2014-09-08
 *
 * Use coupon for current order
 ***/

define('IN_ECS', true);

// Skip template engine for faster loading
define('INIT_NO_SMARTY', true);
// Skip update user info for faster loading
define('INIT_NO_UPDATE_USER_INFO', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');

require_once(ROOT_PATH .'includes/lib_order.php');
$orderController = new Yoho\cms\Controller\OrderController();
$order_fee_platform = $orderController::ORDER_FEE_PLATFORM_WEB;
$user_id = $_SESSION['user_id'];

if (!$user_id)
{
	header('Content-Type: application/json');
	echo json_encode(array(
		'status' => 0,
		'error' => '請先登入',
	));
	exit;
}

$coupon_codes = empty($_REQUEST['code']) ? array() : $_REQUEST['code'];
$coupon_codes = is_array($coupon_codes) ? $coupon_codes : array($coupon_codes);
$coupon_codes = array_map('trim', $coupon_codes);
$coupon_codes = array_filter($coupon_codes);
$coupon_codes = array_map('strtoupper', $coupon_codes);
$coupon_codes = array_unique($coupon_codes);

if (empty($coupon_codes))
{
	header('Content-Type: application/json');
	echo json_encode(array(
		'status' => 0,
		'error' => _L('coupon_error_empty', '請輸入優惠券號碼'),
	));
	exit;
}

// 取得购物类型
$flow_type = isset($_SESSION['flow_type']) ? intval($_SESSION['flow_type']) : CART_GENERAL_GOODS;

if (!((!isset($_CFG['use_bonus']) || $_CFG['use_bonus'] == '1')
	&& ($flow_type != CART_GROUP_BUY_GOODS && $flow_type != CART_EXCHANGE_GOODS)))
{
	header('Content-Type: application/json');
	echo json_encode(array(
		'status' => 0,
		'error' => _L('coupon_error_disabled', '不允許使用優惠券'),
	));
	exit;
}

// 检查购物车中是否有商品
$sql = "SELECT COUNT(*) FROM " . $ecs->table('cart') .
	" WHERE session_id = '" . SESS_ID . "' " .
	"AND parent_id = 0 AND is_gift = 0 AND rec_type = '$flow_type'";

if ($db->getOne($sql) == 0)
{
	header('Content-Type: application/json');
	echo json_encode(array(
		'status' => 0,
		'error' => _L('coupon_error_cart_empty', '購物車中沒有產品'),
	));
	exit;
}

$consignee = get_consignee($_SESSION['user_id']);

$_SESSION['flow_consignee'] = $consignee;

// 对商品信息赋值
$cart_goods = cart_goods($flow_type); // 取得商品列表，计算合计

// 取得訂單
$order = flow_order_info();

// 重設訂單的優惠券
$order['coupon_id'] = array();
if (isset($order['exclusive_coupon'])) unset($order['exclusive_coupon']);

// 計算使用優惠券前的訂單費用
$total_before_coupon = $orderController->order_fee_flow($order, $cart_goods, $consignee, $order_fee_platform);

$coupon_list = array();
$promote_list = array();

foreach ($coupon_codes as $coupon_code)
{
	// 取得Coupon資訊
	$coupon = coupon_info(0, $coupon_code);
	
	// 驗證Coupon
	$error = 0;
	if (!verify_coupon($coupon, $order, cart_amount(true, $flow_type), $cart_goods, $_SESSION['user_id'], $error))
	{
		if ($error == COUPON_ERROR_MIN_GOODS_AMOUNT)
		{
			$error_msg = sprintf(_L('coupon_error_min_goods_amount', '未達到優惠券「%s」指定的最低訂單金額'), $coupon_code);
		}
		elseif ($error == COUPON_ERROR_BEFORE_START)
		{
			$error_msg = sprintf(_L('coupon_error_before_start', '優惠券「%s」還未生效'), $coupon_code);
		}
		elseif ($error == COUPON_ERROR_AFTER_END)
		{
			$error_msg = sprintf(_L('coupon_error_after_end', '優惠券「%s」已過期'), $coupon_code);
		}
		elseif ($error == COUPON_ERROR_SPECIFIC_GOODS)
		{
			$error_msg = sprintf(_L('coupon_error_specific_goods', '您並未選購優惠券「%s」指定的產品'), $coupon_code);
		}
		elseif ($error == COUPON_ERROR_NOT_REUSABLE)
		{
			$error_msg = sprintf(_L('coupon_error_not_reusable', '優惠券「%s」已被使用'), $coupon_code);
		}
		elseif ($error == COUPON_ERROR_REACHED_THE_USER_LIMIT)
		{
			$error_msg = sprintf(_L('coupon_error_reached_the_user_limit', '優惠券「%s」已達使用人數'), $coupon_code);
		}
		elseif ($error == COUPON_ERROR_REACHED_THE_COUNT_LIMIT)
		{
			$error_msg = sprintf(_L('coupon_error_reached_the_count_limit', '優惠券「%s」已達使用次數'), $coupon_code);
		}
		elseif ($error == COUPON_ERROR_EXCLUSIVE)
		{
			$error_msg = sprintf(_L('coupon_error_exclusive', '優惠券「%s」不能與其他優惠券同時使用'), $coupon_code);
		}
		elseif ($error == COUPON_ERROR_EXISTS_EXCLUSIVE)
		{
			$error_msg = sprintf(_L('coupon_error_exists_exclusive', '優惠券「%s」不能與其他優惠券同時使用'), $order['exclusive_coupon']['coupon_code']);
		}
		elseif ($error == COUPON_ERROR_SMS_VERIFED)
		{
			$error_msg = sprintf(_L('coupon_error_sms_verifed', '優惠券「%s」要通過SMS驗證的會員才可使用'),$coupon_code);
		}
		else
		{
			// COUPON_ERROR_EMPTY_COUPON
			// COUPON_ERROR_SPECIFIC_USER
			$error_msg = sprintf(_L('coupon_error_invalid', '優惠券「%s」無效'), $coupon_code);
		}
		
	    header('Content-Type: application/json');
	    echo json_encode(array(
	        'status' => 0,
	        'error' => $error_msg
	    ));
	    exit;
	}
	
	// 使用Coupon
	$order['coupon_id'][] = $coupon['coupon_id'];
	if (!empty($coupon['promote_id'])) {
		$promote_list[$coupon_code] = intval($coupon['promote_id']);
		$promote_info = $orderController->getCouponPromoteInfo($coupon['promote_id']);
		$promote_extra_message = '<b>'.$promote_info['promote_desc'].'</b>';
	}
	
	$coupon_list[$coupon['coupon_id']] = strtoupper($coupon['coupon_code']);
}

// 防止同一張 coupon 使用多次
$order['coupon_id'] = array_unique($order['coupon_id']);

// 计算订单的费用
$total = $orderController->order_fee_flow($order, $cart_goods, $consignee, $order_fee_platform);

// 計算後重設SESSION中訂單的優惠券，以免影響其他計算
$order['coupon_id'] = array();
if (isset($order['exclusive_coupon'])) unset($order['exclusive_coupon']);
$_SESSION['flow_order'] = $order;

// 是否有運費減免？
$shipping_discount = $total_before_coupon['shipping_fee'] - $total['shipping_fee'];

header('Content-Type: application/json');
echo json_encode(array(
	'status' => 1,
	'coupon_list' => $coupon_list,
	'coupon_value' => $total['coupon'] + $shipping_discount,
	'promote_list' => $promote_list,
	'extra_message' => $promote_extra_message,
));
exit;

?>
