<?php

/***
 * verify_sms.php
 * by Anthony 2018-03-06
 *
 * AJAX script for twilio SMS
 ***/

define('IN_ECS', true);
require(dirname(dirname(__FILE__)) . '/includes/init.php');

$act         = empty($_POST['act'])         ? '' : trim($_POST['act']);
$ccc         = empty($_POST['ccc'])         ? '852' : trim($_POST['ccc']);
$mobile      = empty($_POST['mobile'])      ? '' : trim($_POST['mobile']);
$send_phone  = empty($_POST['send_phone'])  ? '' : trim($_POST['send_phone']);
$verify_code = empty($_POST['verify_code']) ? '' : trim($_POST['verify_code']);

$send_verify_sms  = $_CFG['send_verify_sms'];
$twilio_authy_api = $_CFG['twilio_authy_api'];


if(empty($send_verify_sms) || empty($_SESSION['user_id'])) {
    $response = array(
        'status' => 3
    );
    header('Content-Type: application/json');
    echo json_encode($response);
    exit;
}
if(empty($mobile) && isset($send_phone)) {
    $send_phone = str_replace('+', '', $send_phone);
    $tmp = explode("-", $send_phone);
    $ccc = $tmp[0];
    $mobile = $tmp[1];
}
require(dirname(dirname(__FILE__)) . '/includes/twilio/authy/php/Authy.php');
$authy_api = new Authy\AuthyApi($twilio_authy_api);
if($act == 'send_sms') {
    $api_response = $authy_api->phoneVerificationStart($mobile, $ccc, 'sms');
    if($api_response->ok()) {
        $response = array(
            'status' => 1
        );
    } else {
        $response = array(
            'status' => 0,
            'error'  => $api_response->message()
        );
    }
} else if($act == 'verify_sms') {
    $api_response = $authy_api->PhoneVerificationCheck($mobile, $ccc, $verify_code);
    if($api_response->ok()) {
        // Update user is_sms_validated = 1
        $GLOBALS['db']->query('UPDATE '.$GLOBALS['ecs']->table('users').' SET is_sms_validated = 1 WHERE user_id = '.$_SESSION['user_id']);
        $GLOBALS['db']->query('INSERT INTO '.$GLOBALS['ecs']->table('verify_code').' (verify_code, verify_type, verify_target, user_id, status, create_at) VALUES ("' . $verify_code . '", "' . Yoho\cms\Controller\YohoBaseController::VERIFY_TYPE_SMS . '", "+' . $ccc . '-' . $mobile . '", "' . $_SESSION['user_id'] . '", ' . VERIFY_CODE_FINISH . ', ' . gmtime() . ')');
        $response = array(
            'status' => 1
        );
    } else {
        $response = array(
            'status' => 0,
            'error'  => $api_response->message()
        );
    }
}

header('Content-Type: application/json');
echo json_encode($response);
exit;