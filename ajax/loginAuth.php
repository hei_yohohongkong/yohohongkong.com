<?php

/***
* loginAuth.php
*
* AJAX get logged in user info
***/

define('IN_ECS', true);

// Skip template engine for faster loading
define('INIT_NO_SMARTY', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');

header('Content-Type: application/json');
echo insert_member_info3();

?>