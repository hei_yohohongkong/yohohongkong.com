<?php

/**
 * ECSHOP 会员中心
 * ============================================================================
 * * 版权所有 2005-2012 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liubo $
 * $Id: user.php 17217 2011-01-19 06:29:08Z liubo $
*/

define('IN_ECS', true);
define('FORCE_HTTPS', true);
require(dirname(__FILE__) . '/includes/init.php');
require(dirname(__FILE__) . '/includes/lib_order.php');

/* 载入语言文件 */
require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/user.php');
// Re-load yoho language pack to avoid languages overrided by user pack
require(ROOT_PATH . 'languages/' .$_CFG['lang']. '/yoho.php');

$user_id = $_SESSION['user_id'];
$action  = isset($_REQUEST['act']) ? trim($_REQUEST['act']) : 'default';
$back_act='';
$userController     = new Yoho\cms\Controller\UserController();
$paymentController  = new Yoho\cms\Controller\PaymentController();
$orderController    = new Yoho\cms\Controller\OrderController();
$feedController     = new Yoho\cms\Controller\FeedController();
$userRankController = new Yoho\cms\Controller\UserRankController();
$erpController      = new Yoho\cms\Controller\ErpController();
$actg = $paymentController->actg;
$actgHooks = $paymentController->actgHooks;

// Enforce HTTPS connection on important pages
if (in_array($action, array('register', 'login', 'get_password', 'profile', 'affiliate')))
{
	if (!$usingSSL)
	{
		redirect_to_https();
	}
}

// 不需要登录的操作或自己验证是否登录（如ajax处理）的act
$not_login_arr =
array('login','act_login','register','act_register','act_edit_password','get_password','send_pwd_email','password', 'signin', 'add_tag', 'collect', 'return_to_cart', 'logout', 'email_list', 'validate_email', 'send_hash_mail', 'load_tracking_info', 'order_query', 'is_registered', 'check_email','clear_history','qpassword_name', 'get_passwd_question', 'check_answer','check_email2','vip','order_detail_daifu','channel','bank_type', 'oath', 'oath_login', 'other_login');//sbsn check_email2 增加判断接口

/* 显示页面的action列表 */
$ui_arr = array('register', 'login', 'profile', 'order_list', 'order_detail', 'address_list', 'collection_list',
'message_list', 'tag_list', 'get_password', 'reset_password', 'booking_list', 'add_booking', 'account_raply',
'account_deposit', 'account_log', 'account_detail', 'act_account', 'pay', 'default', 'bonus', 'group_buy', 'group_buy_detail', 'affiliate', 'comment_list','validate_email','track_packages', 'transform_points', 'points', 'qpassword_name', 'get_passwd_question', 'check_answer','check_email2','vip','order_detail_daifu','channel','bank_type','redirect', 'programs', 'validate_program');

/* 未登录处理 */
if (empty($_SESSION['user_id']))
{
	if (!in_array($action, $not_login_arr))
	{
		if (in_array($action, $ui_arr))
		{
			/* 如果需要登录,并是显示页面的操作，记录当前操作，用于登录后跳转到相应操作
			if ($action == 'login')
			{
				if (isset($_REQUEST['back_act']))
				{
					$back_act = trim($_REQUEST['back_act']);
				}
			}
			else
			{}*/
			if (!empty($_SERVER['QUERY_STRING']))
			{
				$back_act = 'user.php?' . strip_tags($_SERVER['QUERY_STRING']);
				if ($action == 'validate_program') {
					$_SESSION['user_rank_program_special']['url'] = $back_act;
				}
			}
			$action = 'login';
		}
		else
		{
			//未登录提交数据。非正常途径提交数据！
			die($_LANG['require_login']);
		}
	}
}

// Affiliate
// $affiliate = unserialize($GLOBALS['_CFG']['affiliate']);
// $smarty->assign('affiliate', $affiliate);
// $is_affiliate = empty($user_id) ? '' : $db->getOne("SELECT is_affiliate FROM " . $ecs->table('users') . " WHERE user_id = '" . $user_id . "'");
// $smarty->assign('is_affiliate', $is_affiliate);

/* 如果是显示页面，对页面进行相应赋值  公共部分*/
if (in_array($action, $ui_arr))
{
	assign_template();
	$position = assign_ur_here(0, _L('user_user_center', '會員帳戶'));
	assign_user_page_title($position['title']); // 页面标题
	$smarty->assign('ur_here',    $position['ur_here']);
	// /* 是否显示积分兑换 */
	// if (!empty($_CFG['points_rule']) && unserialize($_CFG['points_rule']))
	// {
	// 	$smarty->assign('show_transform_points', 1);
	// }
	$smarty->assign('helps',      get_shop_help());  // 网店帮助
	$smarty->assign('data_dir',   DATA_DIR);         // 数据目录
	$smarty->assign('action',     $action);
	$smarty->assign('lang',       $_LANG);
	$CategoryController = new Yoho\cms\Controller\CategoryController();
    $t1 = $CategoryController->getTopCategories(0);
	$smarty->assign('t1',              $t1);
	$sql = "SELECT user_id, user_name, pay_points, rank_points FROM " . $ecs->table('users')  . " WHERE user_id='$user_id'";
	$row = $db->getRow($sql);
	$smarty->assign('shop_points', $row);
	$urp = $userRankController->getUserRankProgram($user_id, 1);
	$smarty->assign('urp_list', $urp);
	$aurp = $userRankController->getActiveProgram($user_id);
	$smarty->assign('active_program', $aurp);
}

//用户中心欢迎页
if ($action == 'default')
{
	// howang: If user login / register from checkout page, send them back to checkout
	if ($_SESSION['from_checkout'])
	{
		$_SESSION['from_checkout'] = false;
		unset($_SESSION['from_checkout']);
		
		ecs_header("Location: /checkout\n");
		exit();
	}
	
	//補充資料送積分
	$sql = 'SELECT info_completed FROM ' . $ecs->table('users') . "  WHERE user_id = '$user_id'";
	$user_info_completed = $db->getOne($sql);
	if ($user_info_completed == 0)
	{
		show_message(_L('user_incomplete_login_prompt', '您是首次登入，補填個人資料並驗證電郵，送積分哦！'),
					 _L('user_edit_profile', '修改資料'), 'user.php?act=profile', 'info');
	}
	
	// Temporary disable user center, auto redirect to profile page
	header('Location: /user.php?act=profile');
	exit;
	
	include_once(ROOT_PATH .'includes/lib_clips.php');
	if ($rank = get_rank_info())
	{
		$smarty->assign('rank_name', sprintf($_LANG['your_level'], $rank['rank_name']));
		if (!empty($rank['next_rank_name']))
		{
			$smarty->assign('next_rank_name', sprintf($_LANG['next_level'], $rank['next_rank'] ,$rank['next_rank_name']));
		}
	}

	$info = get_user_default($user_id);
	$smarty->assign('info',		$info);

	$smarty->assign('user_notice', $_CFG['user_notice']);
	$smarty->assign('prompt',	  get_user_prompt($user_id));
	$smarty->display('user_clips.dwt');
}

/* 显示会员注册界面 */
if ($action == 'register')
{
	if ((!isset($back_act)||empty($back_act)) && isset($GLOBALS['_SERVER']['HTTP_REFERER']))
	{
		$back_act = strpos($GLOBALS['_SERVER']['HTTP_REFERER'], 'user.php') ? './index.php' : $GLOBALS['_SERVER']['HTTP_REFERER'];
	}

	/* 取出注册扩展字段 */
	$sql = 'SELECT * FROM ' . $ecs->table('reg_fields') . ' WHERE type < 2 AND display = 1 ORDER BY dis_order, id';
	$extend_info_list = $db->getAll($sql);
	$smarty->assign('extend_info_list', $extend_info_list);

	/* 验证码相关设置 */
	if ((intval($_CFG['captcha']) & CAPTCHA_REGISTER) && gd_version() > 0)
	{
		$smarty->assign('enabled_captcha', 1);
		$smarty->assign('rand',			mt_rand());
	}

	// If have Reference
	$reference = get_affiliate();
	if($reference) {
		$smarty->assign('reference', $reference);
	}

	/* 密码提示问题 */
	$smarty->assign('passwd_questions', $_LANG['passwd_questions']);

	/* 增加是否关闭注册 */
	$smarty->assign('shop_reg_closed', $_CFG['shop_reg_closed']);
	
	// 從文章取得使用條款
	$smarty->assign('tos_content', get_tos_article_content());
	$smarty->assign('privacy_content', get_privacy_article_content());
	$smarty->assign('available_login_countries', implode(',', available_login_countries()));
	$smarty->assign('available_register_countries', implode(',', available_register_countries()));
	
//	$smarty->assign('back_act', $back_act);
	$smarty->display('user_passport.dwt');
}

/****************************** Disable unused code start ******************************
// 注册会员的处理
elseif ($action == 'act_register')
{
	//是否关闭注册
	if ($_CFG['shop_reg_closed'])
	{
		header('Location: user.php?act=register');
		exit;
	}
	else
	{
		include_once(ROOT_PATH . 'includes/lib_passport.php');

		$username = isset($_POST['username']) ? trim($_POST['username']) : '';
		$password = isset($_POST['password']) ? trim($_POST['password']) : '';
		$email	= isset($_POST['email']) ? trim($_POST['email']) : '';
		$other['msn'] = isset($_POST['extend_field1']) ? $_POST['extend_field1'] : '';
		$other['qq'] = isset($_POST['extend_field2']) ? $_POST['extend_field2'] : '';
		$other['office_phone'] = isset($_POST['extend_field3']) ? $_POST['extend_field3'] : '';
		$other['home_phone'] = isset($_POST['extend_field4']) ? $_POST['extend_field4'] : '';
		$other['mobile_phone'] = isset($_POST['extend_field5']) ? $_POST['extend_field5'] : '';
		$sel_question = empty($_POST['sel_question']) ? '' : compile_str($_POST['sel_question']);
		$passwd_answer = isset($_POST['passwd_answer']) ? compile_str(trim($_POST['passwd_answer'])) : '';


		$back_act = isset($_POST['back_act']) ? trim($_POST['back_act']) : '';

		if(empty($_POST['agreement']))
		{
			show_message($_LANG['passport_js']['agreement']);
		}
		if (strlen($username) < 3)
		{
			show_message($_LANG['passport_js']['username_shorter']);
		}

		if (strlen($password) < 6)
		{
			show_message($_LANG['passport_js']['password_shorter']);
		}

		if (strpos($password, ' ') > 0)
		{
			show_message($_LANG['passwd_balnk']);
		}

		//验证码检查
		if ((intval($_CFG['captcha']) & CAPTCHA_REGISTER) && gd_version() > 0)
		{
			if (empty($_POST['captcha']))
			{
				show_message($_LANG['invalid_captcha'], $_LANG['sign_up'], 'user.php?act=register', 'error');
			}

			//检查验证码
			include_once('includes/cls_captcha.php');

			$validator = new captcha();
			if (!$validator->check_word($_POST['captcha']))
			{
				show_message($_LANG['invalid_captcha'], $_LANG['sign_up'], 'user.php?act=register', 'error');
			}
		}

		if (register($username, $password, $email, $other) !== false)
		{
			//把新注册用户的扩展信息插入数据库
			$sql = 'SELECT id FROM ' . $ecs->table('reg_fields') . ' WHERE type = 0 AND display = 1 ORDER BY dis_order, id';   //读出所有自定义扩展字段的id
			$fields_arr = $db->getAll($sql);

			$extend_field_str = '';	//生成扩展字段的内容字符串
			foreach ($fields_arr AS $val)
			{
				$extend_field_index = 'extend_field' . $val['id'];
				if(!empty($_POST[$extend_field_index]))
				{
					$temp_field_content = strlen($_POST[$extend_field_index]) > 100 ? mb_substr($_POST[$extend_field_index], 0, 99) : $_POST[$extend_field_index];
					$extend_field_str .= " ('" . $_SESSION['user_id'] . "', '" . $val['id'] . "', '" . compile_str($temp_field_content) . "'),";
				}
			}
			$extend_field_str = substr($extend_field_str, 0, -1);

			if ($extend_field_str)	  //插入注册扩展数据
			{
				$sql = 'INSERT INTO '. $ecs->table('reg_extend_info') . ' (`user_id`, `reg_field_id`, `content`) VALUES' . $extend_field_str;
				$db->query($sql);
			}

			//写入密码提示问题和答案
			if (!empty($passwd_answer) && !empty($sel_question))
			{
				$sql = 'UPDATE ' . $ecs->table('users') . " SET `passwd_question`='$sel_question', `passwd_answer`='$passwd_answer'  WHERE `user_id`='" . $_SESSION['user_id'] . "'";
				$db->query($sql);
			}
			//判断是否需要自动发送注册邮件
			if ($GLOBALS['_CFG']['member_email_validate'] && $GLOBALS['_CFG']['send_verify_email'])
			{
				send_regiter_hash($_SESSION['user_id']);
			}
			$ucdata = empty($user->ucdata)? "" : $user->ucdata;
			show_message(sprintf($_LANG['register_success'], $username . $ucdata), array(_L('global_go_back', '返回上一頁'), $_LANG['profile_lnk']), array($back_act, 'user.php'), 'info');
		}
		else
		{
			$err->show($_LANG['sign_up'], 'user.php?act=register');
		}
	}
}
****************************** Disable unused code end ******************************/

/************************** 3rd party login disabled by howang **************************
//  第三方登录接口
elseif($action == 'oath')
{
	$type = empty($_REQUEST['type']) ?  '' : $_REQUEST['type'];
	
	if($type == "taobao"){
		header("location:includes/website/tb_index.php");exit;
	}
	
	include_once(ROOT_PATH . 'includes/website/jntoo.php');

	$c = &website($type);
	if($c)
	{
		if (empty($_REQUEST['callblock']))
		{
			if (empty($_REQUEST['callblock']) && isset($GLOBALS['_SERVER']['HTTP_REFERER']))
			{
				$back_act = strpos($GLOBALS['_SERVER']['HTTP_REFERER'], 'user.php') ? 'index.php' : $GLOBALS['_SERVER']['HTTP_REFERER'];
			}
			else
			{
				$back_act = 'index.php';
			}
		}
		else
		{
			$back_act = trim($_REQUEST['callblock']);
		}

		if($back_act[4] != ':') $back_act = $ecs->url().$back_act;
		$open = empty($_REQUEST['open']) ? 0 : intval($_REQUEST['open']);

		$url = $c->login($ecs->url().'user.php?act=oath_login&type='.$type.'&callblock='.urlencode($back_act).'&open='.$open);
		if(!$url)
		{
			show_message( $c->get_error() , '首页', $ecs->url() , 'error');
		}
		header('Location: '.$url);
	}
	else
	{
		show_message('服务器尚未注册该插件！' , '首页',$ecs->url() , 'error');
	}
}



//  处理第三方登录接口
elseif($action == 'oath_login')
{
	$type = empty($_REQUEST['type']) ?  '' : $_REQUEST['type'];
	
	include_once(ROOT_PATH . 'includes/website/jntoo.php');
	$c = &website($type);
	if($c)
	{
		$access = $c->getAccessToken();
		if(!$access)
		{
			show_message( $c->get_error() , '首页', $ecs->url() , 'error');
		}
		$c->setAccessToken($access);
		$info = $c->getMessage();
		if(!$info)
		{
			show_message($c->get_error() , '首页' , $ecs->url() , 'error' , false);
		}
		if(!$info['user_id'])
			show_message($c->get_error() , '首页' , $ecs->url() , 'error' , false);


		$info_user_id = $type .'_'.$info['user_id']; //  加个标识！！！防止 其他的标识 一样  // 以后的ID 标识 将以这种形式 辨认
		$info['name'] = str_replace("'" , "" , $info['name']); // 过滤掉 逗号 不然出错  很难处理   不想去  搞什么编码的了
		if(!$info['user_id'])
			show_message($c->get_error() , '首页' , $ecs->url() , 'error' , false);


		$sql = 'SELECT user_name,password,aite_id FROM '.$ecs->table('users').' WHERE aite_id = \''.$info_user_id.'\' OR aite_id=\''.$info['user_id'].'\'';

		$count = $db->getRow($sql);
		if(!$count)   // 没有当前数据
		{
			if($user->check_user($info['name']))  // 重名处理
			{
				$info['name'] = $info['name'].'_'.$type.(rand(10000,99999));
			}
			$user_pass = $user->compile_password(array('password'=>$info['user_id']));
			$sql = 'INSERT INTO '.$ecs->table('users').'(user_name , password, aite_id , sex , reg_time , user_rank , is_validated) VALUES '.
					"('$info[name]' , '$user_pass' , '$info_user_id' , '$info[sex]' , '".gmtime()."' , '$info[rank_id]' , '1')" ;
			$db->query($sql);
		}
		else
		{
			$sql = '';
			if($count['aite_id'] == $info['user_id'])
			{
				$sql = 'UPDATE '.$ecs->table('users')." SET aite_id = '$info_user_id' WHERE aite_id = '$count[aite_id]'";
				$db->query($sql);
			}
			if($info['name'] != $count['user_name'])   // 这段可删除
			{
				if($user->check_user($info['name']))  // 重名处理
				{
					$info['name'] = $info['name'].'_'.$type.(rand()*1000);
				}
				$sql = 'UPDATE '.$ecs->table('users')." SET user_name = '$info[name]' WHERE aite_id = '$info_user_id'";
				$db->query($sql);
			}
		}
		$user->set_session($info['name']);
		$user->set_cookie($info['name']);
		update_user_info();
		recalculate_price();

		if(!empty($_REQUEST['open']))
		{
			die('<script>window.opener.window.location.reload(); window.close();</script>');
		}
		else
		{
			ecs_header('Location: '.$_REQUEST['callblock']);

		}

	}

}

//  处理其它登录接口
elseif($action == 'other_login')
{
	$type = empty($_REQUEST['type']) ?  '' : $_REQUEST['type'];
	session_start();
	$info = $_SESSION['user_info'];

	if(empty($info)){
		show_message("非法访问或请求超时！" , '首页' , $ecs->url() , 'error' , false);
	}
	if(!$info['user_id'])
		show_message("非法访问或访问出错，请联系管理员！", '首页' , $ecs->url() , 'error' , false);


	$info_user_id = $type .'_'.$info['user_id']; //  加个标识！！！防止 其他的标识 一样  // 以后的ID 标识 将以这种形式 辨认
	$info['name'] = str_replace("'" , "" , $info['name']); // 过滤掉 逗号 不然出错  很难处理   不想去  搞什么编码的了


	$sql = 'SELECT user_name,password,aite_id FROM '.$ecs->table('users').' WHERE aite_id = \''.$info_user_id.'\' OR aite_id=\''.$info['user_id'].'\'';

	$count = $db->getRow($sql);
	$login_name = $info['name'];
	if(!$count)   // 没有当前数据
	{
		if($user->check_user($info['name']))  // 重名处理
		{
			$info['name'] = $info['name'].'_'.$type.(rand()*1000);
		}
		$login_name = $info['name'];
		$user_pass = $user->compile_password(array('password'=>$info['user_id']));
		$sql = 'INSERT INTO '.$ecs->table('users').'(user_name , password, aite_id , sex , reg_time , user_rank , is_validated) VALUES '.
				"('$info[name]' , '$user_pass' , '$info_user_id' , '$info[sex]' , '".gmtime()."' , '$info[rank_id]' , '1')" ;
		$db->query($sql);
	}
	else
	{
		$login_name = $count['user_name'];
		$sql = '';
		if($count['aite_id'] == $info['user_id'])
		{
			$sql = 'UPDATE '.$ecs->table('users')." SET aite_id = '$info_user_id' WHERE aite_id = '$count[aite_id]'";
			$db->query($sql);
		}
	}
	
	
	
	$user->set_session($login_name);
	$user->set_cookie($login_name);
	update_user_info();
	recalculate_price();

	$redirect_url = str_replace("user.php", "index.php", $_SERVER["REQUEST_URI"]);
	header('Location: '.$redirect_url);

}
************************** end 3rd party login **************************/

/* 验证用户注册邮件 */
elseif ($action == 'validate_email')
{
	$hash = empty($_GET['hash']) ? '' : trim($_GET['hash']);
	if ($hash)
	{
		include_once(ROOT_PATH . 'includes/lib_passport.php');
		$id = register_hash('decode', $hash);
		if ($id > 0)
		{
			$sql = 'SELECT first_validated, info_completed FROM ' . $ecs->table('users') . "  WHERE user_id = '$id'";
			$row = $db->getRow($sql);
			$first_validated = $row['first_validated'];
			$info_completed = $row['info_completed'];
			
			$sql = "UPDATE " . $ecs->table('users') . " SET is_validated = 1, first_validated = 1 WHERE user_id='$id'";
			$db->query($sql);
			
			if (($first_validated == 0) && ($info_completed == 1))
			{
				$profile_count_point = 2000; // As requested by Franz, only give away 2000 points
				log_account_change($id, 0, 0, 0, $profile_count_point, '首次修改資料並驗證電郵獲得積分', ACT_OTHER);
				show_message(sprintf(_L('user_complete_reward', '首次修改資料並驗證電郵成功，系統贈送給您%s分'), $profile_count_point),
							_L('user_my_profile', '個人資料'), 'user.php?act=profile', 'info');
			}
			else
			{
				$sql = 'SELECT user_name, email FROM ' . $ecs->table('users') . " WHERE user_id = '$id'";
				$row = $db->getRow($sql);
				$incomplete = (($first_validated == 0) && ($info_completed == 0));
				$msg = sprintf(_L('user_email_validate_success', '%s 您好，您的電郵地址 %s 已通過驗證%s'),
							$row['user_name'], $row['email'],
							$incomplete ? _L('user_email_validate_complete_profile', '，補填個人資料，送積分哦！') : '');
				show_message($msg, _L('user_my_profile', '個人資料'), 'user.php?act=profile');
			}
		}
	}
	show_message(_L('user_email_validate_failed', '驗證失敗，請確認你的驗證連結是否正確'));
}

/****************************** Disable unused code start ******************************
// 验证用户注册用户名是否可以注册
elseif ($action == 'is_registered')
{
	include_once(ROOT_PATH . 'includes/lib_passport.php');

	$username = trim($_GET['username']);
	$username = json_str_iconv($username);

	if ($user->check_user($username) || admin_registered($username))
	{
		echo 'false';
	}
	else
	{
		echo 'true';
	}
}

// 验证用户邮箱地址是否被注册
elseif($action == 'check_email')
{
	$email = trim($_GET['email']);
	if ($user->check_email($email))
	{
		echo 'false';
	}
	else
	{
		echo 'ok';
	}
}
elseif($action == 'check_email2')
{
	$email = trim($_GET['email']);
	
	if ($user->check_email($email))
	{
		echo 'false';
	}
	else
	{
		echo 'true';
		
	}
}
****************************** Disable unused code end ******************************/

/* 用户登录界面 */
elseif ($action == 'login')
{
	if (empty($back_act))
	{
		if (empty($back_act) && isset($GLOBALS['_SERVER']['HTTP_REFERER']))
		{
			$back_act = strpos($GLOBALS['_SERVER']['HTTP_REFERER'], 'user.php') ? './index.php' : $GLOBALS['_SERVER']['HTTP_REFERER'];
		}
		else
		{
			$back_act = 'user.php';
		}
	}

	$captcha = intval($_CFG['captcha']);
	if (($captcha & CAPTCHA_LOGIN) && (!($captcha & CAPTCHA_LOGIN_FAIL) || (($captcha & CAPTCHA_LOGIN_FAIL) && $_SESSION['login_fail'] > 2)) && gd_version() > 0)
	{
		$GLOBALS['smarty']->assign('enabled_captcha', 1);
		$GLOBALS['smarty']->assign('rand', mt_rand());
	}
	
	// 從文章取得使用條款
	$smarty->assign('tos_content', get_tos_article_content());
	$smarty->assign('privacy_content', get_privacy_article_content());
	$smarty->assign('available_login_countries', implode(',', available_login_countries()));
	$smarty->assign('available_register_countries', implode(',', available_register_countries()));
	
	$smarty->assign('back_act', $back_act);
	$smarty->display('user_passport.dwt');
}

/* 处理会员的登录 */
elseif ($action == 'act_login')
{
	$username = isset($_POST['username']) ? trim($_POST['username']) : '';
	$password = isset($_POST['password']) ? trim($_POST['password']) : '';
	$back_act = isset($_POST['back_act']) ? trim($_POST['back_act']) : '';
	
	// If login requested from admin, can login user's account without password
	$admin_login = isset($_POST['admin_login']) ? trim($_POST['admin_login']) : '';
	if (!empty($admin_login))
	{
		$admin = json_decode(base64_decode($admin_login), true);
		if (($admin == null) || (empty($admin['id'])) || (empty($admin['pw'])))
		{
			show_message($_LANG['login_failure'] . __LINE__, $_LANG['relogin_lnk'], 'user.php', 'error');
			exit;
		}
		
		$sql = 'SELECT user_id, user_name, password ' .
				' FROM ' .$ecs->table('admin_user') .
				" WHERE user_id = '" . intval($admin['id']) . "'";
		$row = $db->getRow($sql);
		
		if (md5($row['password'] . $_CFG['hash_code']) != $admin['pw'])
		{
			show_message($_LANG['login_failure'] . __LINE__, $_LANG['relogin_lnk'], 'user.php', 'error');
			exit;
		}
		
		// Clear previous session if any
		$user->set_session();
		
		// Login the user account
		$user->set_session($username);
		$user->set_cookie($username, 1);
		
		update_user_info();
		recalculate_price();
		
		$ucdata = isset($user->ucdata) ? $user->ucdata : '';
		show_message($_LANG['login_success'] . $ucdata , array(_L('global_go_back', '返回上一頁'), $_LANG['profile_lnk']), array($back_act,'user.php'), 'info');
	}
	
	$captcha = intval($_CFG['captcha']);
	if (($captcha & CAPTCHA_LOGIN) && (!($captcha & CAPTCHA_LOGIN_FAIL) || (($captcha & CAPTCHA_LOGIN_FAIL) && $_SESSION['login_fail'] > 2)) && gd_version() > 0)
	{
		if (empty($_POST['captcha']))
		{
			show_message($_LANG['invalid_captcha'], $_LANG['relogin_lnk'], 'user.php', 'error');
		}

		/* 检查验证码 */
		include_once('includes/cls_captcha.php');

		$validator = new captcha();
		$validator->session_word = 'captcha_login';
		if (!$validator->check_word($_POST['captcha']))
		{
			show_message($_LANG['invalid_captcha'], $_LANG['relogin_lnk'], 'user.php', 'error');
		}
	}

	if ($user->login($username, $password,isset($_POST['remember'])))
	{
		update_user_info();
		recalculate_price();

		$ucdata = isset($user->ucdata)? $user->ucdata : '';
		show_message($_LANG['login_success'] . $ucdata , array(_L('global_go_back', '返回上一頁'), $_LANG['profile_lnk']), array($back_act,'user.php'), 'info');
	}
	else
	{
		$_SESSION['login_fail'] ++ ;
		show_message(_L('account_login_failed', '您輸入的手提電話號碼和密碼不相符！'), $_LANG['relogin_lnk'], 'user.php', 'error');
	}
}

/****************************** Disable unused code start ******************************
// 处理 ajax 的登录请求
elseif ($action == 'signin')
{
	include_once('includes/cls_json.php');
	$json = new JSON;

	$username = !empty($_POST['username']) ? json_str_iconv(trim($_POST['username'])) : '';
	$password = !empty($_POST['password']) ? trim($_POST['password']) : '';
	$captcha = !empty($_POST['captcha']) ? json_str_iconv(trim($_POST['captcha'])) : '';
	$result   = array('error' => 0, 'content' => '');

	$captcha = intval($_CFG['captcha']);
	if (($captcha & CAPTCHA_LOGIN) && (!($captcha & CAPTCHA_LOGIN_FAIL) || (($captcha & CAPTCHA_LOGIN_FAIL) && $_SESSION['login_fail'] > 2)) && gd_version() > 0)
	{
		if (empty($captcha))
		{
			$result['error']   = 1;
			$result['content'] = $_LANG['invalid_captcha'];
			die($json->encode($result));
		}

		// 检查验证码
		include_once('includes/cls_captcha.php');

		$validator = new captcha();
		$validator->session_word = 'captcha_login';
		if (!$validator->check_word($_POST['captcha']))
		{

			$result['error']   = 1;
			$result['content'] = $_LANG['invalid_captcha'];
			die($json->encode($result));
		}
	}

	if ($user->login($username, $password))
	{
		update_user_info();  //更新用户信息
		recalculate_price(); // 重新计算购物车中的商品价格
		$smarty->assign('user_info', get_user_info());
		$ucdata = empty($user->ucdata)? "" : $user->ucdata;
		$result['ucdata'] = $ucdata;
		$result['content'] = $smarty->fetch('library/member_info.lbi');
	}
	else
	{
		$_SESSION['login_fail']++;
		if ($_SESSION['login_fail'] > 2)
		{
			$smarty->assign('enabled_captcha', 1);
			$result['html'] = $smarty->fetch('library/member_info.lbi');
		}
		$result['error']   = 1;
		$result['content'] = $_LANG['login_failure'];
	}
	die($json->encode($result));
}
****************************** Disable unused code end ******************************/

/* 退出会员中心 */
elseif ($action == 'logout')
{
	if ((!isset($back_act)|| empty($back_act)) && isset($GLOBALS['_SERVER']['HTTP_REFERER']))
	{
		$back_act = strpos($GLOBALS['_SERVER']['HTTP_REFERER'], 'user.php') ? './' : $GLOBALS['_SERVER']['HTTP_REFERER'];
	}

	$user->logout();
	$ucdata = empty($user->ucdata) ? '' : $user->ucdata;
	show_message(_L('user_logout_success', '登出成功！') . $ucdata,
				array(_L('global_go_back', '返回上一頁'), _L('global_go_home', '返回首頁')),
				array($back_act, './'), 'info');
}

/* 个人资料页面 */
elseif ($action == 'profile')
{
	include_once(ROOT_PATH . 'includes/lib_transaction.php');

	// if isset back_act_from_event session
	if (!empty($_SESSION["back_act_from_event"])) {
		$back_act_from_event = $_SESSION["back_act_from_event"];
		unset($_SESSION["back_act_from_event"]);
		header('Location: /'.$back_act_from_event);
		exit;
	}

	$user_info = get_profile($user_id);

	if($_CFG['send_verify_sms'] == 1 && $user_info['is_sms_validated'] == 0) {
		$smarty->assign('display_sms_verify', 1);
	}

	//Blocking Wholesale Customer
	if(in_array($user_id,$userController->getWholesaleUserIds()))
		show_message('批發客戶請直接聯絡營業員訂購');

	if ($user_info['email'] == $_SESSION['user_name'] . '@yohohongkong.com') // Prevent user keep using default system generated email
	{
		$user_info['email'] = '';
	}

	/* 取出注册扩展字段 */
	$sql = 'SELECT * FROM ' . $ecs->table('reg_fields') . ' WHERE type < 2 AND display = 1 ORDER BY dis_order, id';
	$extend_info_list = $db->getAll($sql);

	$sql = 'SELECT reg_field_id, content ' .
		   'FROM ' . $ecs->table('reg_extend_info') .
		   " WHERE user_id = $user_id";
	$extend_info_arr = $db->getAll($sql);

	$temp_arr = array();
	foreach ($extend_info_arr AS $val)
	{
		$temp_arr[$val['reg_field_id']] = $val['content'];
	}

	foreach ($extend_info_list AS $key => $val)
	{
		switch ($val['id'])
		{
			case 1:	 $extend_info_list[$key]['content'] = $user_info['msn']; break;
			case 2:	 $extend_info_list[$key]['content'] = $user_info['qq']; break;
			case 3:	 $extend_info_list[$key]['content'] = $user_info['office_phone']; break;
			case 4:	 $extend_info_list[$key]['content'] = $user_info['home_phone']; break;
			case 5:	 $extend_info_list[$key]['content'] = $user_info['mobile_phone']; break;
			default:	$extend_info_list[$key]['content'] = empty($temp_arr[$val['id']]) ? '' : $temp_arr[$val['id']] ;
		}
		if (in_array($val['id'], array(11,12))) // 常住地區/工作地區
		{
			// Display localized area name if available
			$extend_info_list[$key]['content_display'] = _L('area_select_' . $extend_info_list[$key]['content'], $extend_info_list[$key]['content']);
		}
		// Display localized field name if available
		$extend_info_list[$key]['reg_field_name'] = _L('user_profile_extend_field'.$val['id'], $val['reg_field_name']);
		$extend_info_list[$key]['field'] = 'extend_field'.$val['id'];
	}

	$smarty->assign('extend_info_list', $extend_info_list);

	/* 密码提示问题 */
	$smarty->assign('passwd_questions', $_LANG['passwd_questions']);

	// 是否已補填資料
	$sql = 'SELECT info_completed FROM ' . $ecs->table('users') . "  WHERE user_id = '$user_id'";
	$smarty->assign('user_info_completed', $db->getOne($sql));
	
	$smarty->assign('profile', $user_info);
	//$smarty->display('user_transaction.dwt');
	$smarty->assign('lbi_file', 'library/user_profile.lbi');
	$smarty->display('user_center.dwt');
}

/* 修改个人资料的处理 */
elseif ($action == 'act_edit_profile')
{
	include_once(ROOT_PATH . 'includes/lib_transaction.php');
	
	$sql = 'SELECT info_completed, phpbb_userid FROM ' . $ecs->table('users') . "  WHERE user_id = '$user_id'";
	$row = $db->getRow($sql);
	$user_info_completed = $row['info_completed'];
	$phpbb_id = $row['phpbb_userid'];
	
	$birthdayYear = empty($_POST['birthdayYear']) ? '' : trim($_POST['birthdayYear']);
	$birthdayMonth = empty($_POST['birthdayMonth']) ? '' : trim($_POST['birthdayMonth']);
	$birthdayDay = empty($_POST['birthdayDay']) ? '' : trim($_POST['birthdayDay']);
	$birthday = $birthdayYear .'-'. $birthdayMonth .'-'. $birthdayDay;
	$email = empty($_POST['email']) ? '' : trim($_POST['email']);
	$other['msn'] = $msn = isset($_POST['extend_field1']) ? trim($_POST['extend_field1']) : '';
	$other['qq'] = $qq = isset($_POST['extend_field2']) ? trim($_POST['extend_field2']) : '';
	$other['office_phone'] = $office_phone = isset($_POST['extend_field3']) ? trim($_POST['extend_field3']) : '';
	$other['home_phone'] = $home_phone = isset($_POST['extend_field4']) ? trim($_POST['extend_field4']) : '';
	$other['mobile_phone'] = $mobile_phone = isset($_POST['extend_field5']) ? trim($_POST['extend_field5']) : '';
	$sel_question = empty($_POST['sel_question']) ? '' : compile_str($_POST['sel_question']);
	$passwd_answer = isset($_POST['passwd_answer']) ? compile_str(trim($_POST['passwd_answer'])) : '';

	if (!empty($office_phone) && !preg_match( '/^[\d|\_|\-|\s]+$/', $office_phone ) )
	{
		show_message($_LANG['passport_js']['office_phone_invalid']);
	}
	if (!empty($home_phone) && !preg_match( '/^[\d|\_|\-|\s]+$/', $home_phone) )
	{
		show_message($_LANG['passport_js']['home_phone_invalid']);
	}
	
	if (empty($email))
	{
		show_message(sprintf(_L('user_profile_error_missing', '請輸入您的%s'), _L('user_profile_email', '電子郵件地址')));
	}
	
	if ((!is_email($email)) ||
		($email == $_SESSION['user_name'] . '@yohohongkong.com')) // Prevent user keep using default system generated email
	{
		show_message(sprintf(_L('user_profile_error_invalid', '請輸入正確的%s'), _L('user_profile_email', '電子郵件地址')));
	}
	
	$user_info = get_profile($user_id);
	
	if (!$user_info_completed)
	{
		if (($birthday == '--') || ($birthday == '0000-00-00'))
		{
			show_message(sprintf(_L('user_profile_error_missing', '請輸入您的%s'), _L('user_profile_birthday', '生日日期')));
		}
		
		if (!validate_date($birthday))
		{
			show_message(sprintf(_L('user_profile_error_invalid', '請輸入正確的%s'), _L('user_profile_birthday', '生日日期')));
		}
		
		if (intval($_POST['sex']) <= 0)
		{
			show_message(sprintf(_L('user_profile_error_missing', '請輸入您的%s'), _L('user_profile_gender', '性別')));
		}
		
		if (intval($_POST['sex']) > 2) // 0 = 保密, 1 = 男, 2 = 女, >2 = invalid
		{
			show_message(sprintf(_L('user_profile_error_invalid', '請輸入正確的%s'), _L('user_profile_gender', '性別')));
		}
	}
	else
	{
		$birthday = $user_info['birthday'];
		$_POST['sex'] = $user_info['sex'];
	}
	
	// 公開顯示名稱
	$display_name = $_POST['extend_field10'];
	if (!empty($display_name))
	{
		if (mb_strlen($display_name,'utf-8') > 20)
		{
			show_message(sprintf(_L('user_profile_error_too_long', '%s太長了'), _L('user_profile_extend_field10', '公開顯示名稱')));
		}
		$display_name = htmlspecialchars($display_name);
		$sql = "SELECT 1 FROM " . $ecs->table('reg_extend_info') . "  WHERE reg_field_id = '10' AND user_id != '$user_id' AND content = '$display_name'";
		$res = $db->getOne($sql);
		if (!empty($res))
		{
			show_message(sprintf(_L('user_profile_error_already_taken', '您的%s（%s）已被使用'), _L('user_profile_extend_field10', '公開顯示名稱'), $display_name));
		}
		$sql = "SELECT content FROM " . $ecs->table('reg_extend_info') . "  WHERE reg_field_id = '10' AND user_id = '$user_id'";
		$old_display_name = $db->getOne($sql);
	}
	
	if (!empty($msn) && !is_email($msn))
	{
		show_message($_LANG['passport_js']['msn_invalid']);
	}
	if (!empty($qq) && !preg_match('/^\d+$/', $qq))
	{
		show_message($_LANG['passport_js']['qq_invalid']);
	}
	if (!empty($mobile_phone) && !preg_match('/^[\d-\s]+$/', $mobile_phone))
	{
		show_message($_LANG['passport_js']['mobile_phone_invalid']);
	}
	
	/* 更新用户扩展字段的数据 */
	$sql = 'SELECT id,reg_field_name,is_need FROM ' . $ecs->table('reg_fields') . ' WHERE type = 0 AND display = 1 ORDER BY dis_order, id';   //读出所有扩展字段的id
	$fields_arr = $db->getAll($sql);
	
	foreach ($fields_arr AS $val)	   //循环更新扩展用户信息
	{
		$extend_field_index = 'extend_field' . $val['id'];
		$temp_field_content = isset($_POST[$extend_field_index]) ? $_POST[$extend_field_index] : '';
		$temp_field_content = strlen($temp_field_content) > 100 ? mb_substr(htmlspecialchars($temp_field_content), 0, 99) : htmlspecialchars($temp_field_content);
		
		if ((empty($temp_field_content)) && ($val['is_need'] == '1'))
		{
			show_message(sprintf(_L('user_profile_error_missing', '請輸入您的%s'), _L('user_profile_extend_field' . $val['id'], $val['reg_field_name'])));
		}
		
		$sql = "SELECT * FROM " . $ecs->table('reg_extend_info') . "  WHERE reg_field_id = '" . $val['id'] . "' AND user_id = '" . $user_id . "'";
		if ($db->getOne($sql))	  //如果之前没有记录，则插入
		{
			$sql = "UPDATE " . $ecs->table('reg_extend_info') .
					" SET content = '" . $temp_field_content . "'" .
					" WHERE reg_field_id = '" . $val['id'] . "' AND user_id = '" . $user_id . "'";
		}
		else
		{
			$sql = "INSERT INTO " . $ecs->table('reg_extend_info') .
					" (`user_id`, `reg_field_id`, `content`)" .
					" VALUES ('" . $user_id . "', '" . $val['id'] . "', '" . $temp_field_content . "')";
		}
		$db->query($sql);
	}

	/* 写入密码提示问题和答案 */
	if (!empty($passwd_answer) && !empty($sel_question))
	{
		$sql = 'UPDATE ' . $ecs->table('users') . " SET `passwd_question`='$sel_question', `passwd_answer`='$passwd_answer'  WHERE `user_id`='" . $_SESSION['user_id'] . "'";
		$db->query($sql);
	}

	$profile  = array(
		'user_id'  => $user_id,
		'email'	=> $email,
		'sex'	  => isset($_POST['sex'])   ? intval($_POST['sex']) : 0,
		'birthday' => $birthday,
		'other'	=> isset($other) ? $other : array()
	);

	if (edit_profile($profile))
	{
		if ($user_info['email'] != $profile['email'])
		{
			// Email changed, mark as not validated
			$sql="UPDATE ".$ecs->table('users'). "SET `is_validated` = '0' WHERE `user_id` = '".$user_id."' LIMIT 1";
			$db->query($sql);
			
			// send verify email
			include_once(ROOT_PATH .'includes/lib_passport.php');
			send_regiter_hash_async($user_id);
			
			if (!empty($phpbb_id))
			{
				require_once ROOT_PATH . 'core/lib_phpbb.php';
				phpbb_call(array(
					'act' => 'update_email',
					'phpbb_userid' => $phpbb_id,
					'new_email' => $profile['email'],
				));
			}
		}
		
		if ($old_display_name != $display_name)
		{
			if (!empty($phpbb_id))
			{
				require_once ROOT_PATH . 'core/lib_phpbb.php';
				phpbb_call(array(
					'act' => 'update_user_name',
					'phpbb_userid' => $phpbb_id,
					'new_name' => $display_name,
				));
			}
		}
		
		if ($user_info_completed == 0)
		{
			// Mark as user info completed
			$sql="UPDATE ".$ecs->table('users'). "SET `info_completed` = '1' WHERE `user_id` = '".$user_id."' LIMIT 1";
			$db->query($sql);
		}
		
		$sql = 'SELECT is_validated, first_validated FROM ' . $ecs->table('users') . "  WHERE user_id = '$user_id'";
		$row = $db->getRow($sql);
		$is_validated = $row['is_validated'];
		$first_validated = $row['first_validated'];
		
		if (($user_info_completed == 0) && ($is_validated == 1))
		{
			$sql="UPDATE ".$ecs->table('users'). "SET `info_completed` = '1' WHERE `user_id` = '".$user_id."' LIMIT 1";
			$db->query($sql);
			
			$profile_count_point = 2000; // As requested by Franz, only give away 2000 points
			log_account_change($user_id, 0, 0, 0, $profile_count_point, "首次修改資料並驗證電郵獲得積分", ACT_OTHER);
			show_message(sprintf(_L('user_complete_reward', '首次修改資料並驗證電郵成功，系統贈送給您%s分'), $profile_count_point),
						_L('user_my_profile', '個人資料'), 'user.php?act=profile', 'info');
		}
		else
		{
			show_message(sprintf(_L('user_profile_updated', '您的個人資料已經成功修改%s！'),
						($first_validated == 0 ? _L('user_profile_update_validate_email', '，驗證電郵可獲贈積分哦') : '')),
						_L('user_my_profile', '個人資料'), 'user.php?act=profile', 'info');
		}
	}
	else
	{
		if ($user->error == ERR_EMAIL_EXISTS)
		{
			$msg = sprintf(_L('user_profile_error_already_taken', '您的%s（%s）已被使用。'),
							_L('user_profile_email' , '電子郵件地址'), $profile['email']);
		}
		else
		{
			$msg = _L('user_profile_error_other', '修改個人資料失敗，請稍後再試。');
		}
		show_message($msg, '', '', 'info');
	}
}

/* 密码找回-->修改密码界面 */
elseif ($action == 'get_password')
{
	include_once(ROOT_PATH . 'includes/lib_passport.php');

	if (isset($_GET['code']) && isset($_GET['uid'])) //从邮件处获得的act
	{
		$code = trim($_GET['code']);
		$uid  = intval($_GET['uid']);
		
		/* 判断链接的合法性 */
		$user_info = $user->get_profile_by_id($uid);
		if ((empty($user_info)) || (!verify_password_reset_token($uid, $code)))
		{
			show_message(
				_L('account_reset_password_token_invalid', '你所使用的重設密碼鏈結並不正確或已經失效。'),
				_L('global_go_home', '返回首頁'), '/', 'info');
		}
		
		$smarty->assign('uid', $uid);
		$smarty->assign('code', $code);
		$smarty->assign('reset_pwd_user', $user_info['user_name']);
	}
	
	// 從文章取得使用條款
	$smarty->assign('tos_content', get_tos_article_content());
	$smarty->assign('privacy_content', get_privacy_article_content());
	$smarty->assign('available_login_countries', implode(',', available_login_countries()));
	$smarty->assign('available_register_countries', implode(',', available_register_countries()));
	
	$smarty->assign('action', 'reset_password');
	$smarty->display('user_passport.dwt');
}

/************************** ECShop original password recovery code disabled by howang **************************

// 密码找回-->输入用户名界面
elseif ($action == 'qpassword_name')
{
	//显示输入要找回密码的账号表单
	$smarty->display('user_passport.dwt');
}

// 密码找回-->根据注册用户名取得密码提示问题界面
elseif ($action == 'get_passwd_question')
{
	if (empty($_POST['user_name']))
	{
		show_message($_LANG['no_passwd_question'], _L('global_go_home', '返回首頁'), './', 'info');
	}
	else
	{
		$user_name = trim($_POST['user_name']);
	}

	//取出会员密码问题和答案
	$sql = 'SELECT user_id, user_name, passwd_question, passwd_answer FROM ' . $ecs->table('users') . " WHERE user_name = '" . $user_name . "'";
	$user_question_arr = $db->getRow($sql);

	//如果没有设置密码问题，给出错误提示
	if (empty($user_question_arr['passwd_answer']))
	{
		show_message($_LANG['no_passwd_question'], _L('global_go_home', '返回首頁'), './', 'info');
	}

	$_SESSION['temp_user'] = $user_question_arr['user_id'];  //设置临时用户，不具有有效身份
	$_SESSION['temp_user_name'] = $user_question_arr['user_name'];  //设置临时用户，不具有有效身份
	$_SESSION['passwd_answer'] = $user_question_arr['passwd_answer'];   //存储密码问题答案，减少一次数据库访问

	$captcha = intval($_CFG['captcha']);
	if (($captcha & CAPTCHA_LOGIN) && (!($captcha & CAPTCHA_LOGIN_FAIL) || (($captcha & CAPTCHA_LOGIN_FAIL) && $_SESSION['login_fail'] > 2)) && gd_version() > 0)
	{
		$GLOBALS['smarty']->assign('enabled_captcha', 1);
		$GLOBALS['smarty']->assign('rand', mt_rand());
	}

	$smarty->assign('passwd_question', $_LANG['passwd_questions'][$user_question_arr['passwd_question']]);
	$smarty->display('user_passport.dwt');
}

// 密码找回-->根据提交的密码答案进行相应处理
elseif ($action == 'check_answer')
{
	$captcha = intval($_CFG['captcha']);
	if (($captcha & CAPTCHA_LOGIN) && (!($captcha & CAPTCHA_LOGIN_FAIL) || (($captcha & CAPTCHA_LOGIN_FAIL) && $_SESSION['login_fail'] > 2)) && gd_version() > 0)
	{
		if (empty($_POST['captcha']))
		{
			show_message($_LANG['invalid_captcha'], $_LANG['back_retry_answer'], 'user.php?act=qpassword_name', 'error');
		}

		// 检查验证码
		include_once('includes/cls_captcha.php');

		$validator = new captcha();
		$validator->session_word = 'captcha_login';
		if (!$validator->check_word($_POST['captcha']))
		{
			show_message($_LANG['invalid_captcha'], $_LANG['back_retry_answer'], 'user.php?act=qpassword_name', 'error');
		}
	}

	if (empty($_POST['passwd_answer']) || $_POST['passwd_answer'] != $_SESSION['passwd_answer'])
	{
		show_message($_LANG['wrong_passwd_answer'], $_LANG['back_retry_answer'], 'user.php?act=qpassword_name', 'info');
	}
	else
	{
		$_SESSION['user_id'] = $_SESSION['temp_user'];
		$_SESSION['user_name'] = $_SESSION['temp_user_name'];
		unset($_SESSION['temp_user']);
		unset($_SESSION['temp_user_name']);
		$smarty->assign('uid',	$_SESSION['user_id']);
		$smarty->assign('action', 'reset_password');
		$smarty->display('user_passport.dwt');
	}
}

// 发送密码修改确认邮件
elseif ($action == 'send_pwd_email')
{
	include_once(ROOT_PATH . 'includes/lib_passport.php');

	// 初始化会员用户名和邮件地址
	$user_name = !empty($_POST['user_name']) ? trim($_POST['user_name']) : '';
	$email	 = !empty($_POST['email'])	 ? trim($_POST['email'])	 : '';

	//用户名和邮件地址是否匹配
	$user_info = $user->get_user_info($user_name);

	if ($user_info && $user_info['email'] == $email)
	{
		//生成code
		 //$code = md5($user_info[0] . $user_info[1]);

		$code = md5($user_info['user_id'] . $_CFG['hash_code'] . $user_info['reg_time']);
		//发送邮件的函数
		if (send_pwd_email($user_info['user_id'], $user_name, $email, $code))
		{
			show_message($_LANG['send_success'] . $email, _L('global_go_home', '返回首頁'), './', 'info');
		}
		else
		{
			//发送邮件出错
			show_message($_LANG['fail_send_password'], $_LANG['back_page_up'], './', 'info');
		}
	}
	else
	{
		//用户名与邮件地址不匹配
		show_message($_LANG['username_no_email'], $_LANG['back_page_up'], '', 'info');
	}
}

// 重置新密码
elseif ($action == 'reset_password')
{
	//显示重置密码的表单
	$smarty->display('user_passport.dwt');
}
********************************** ECShop original password recovery code end *********************************/

/* 修改会员密码 */
// Updated by howang to disable change password by uid+code (part of original password recovery logic)
elseif ($action == 'act_edit_password')
{
	include_once(ROOT_PATH . 'includes/lib_passport.php');

	$old_password = isset($_POST['old_password']) ? trim($_POST['old_password']) : null;
	$new_password = isset($_POST['new_password']) ? trim($_POST['new_password']) : '';
	//$user_id	  = isset($_POST['uid'])  ? intval($_POST['uid']) : $user_id;
	//$code		 = isset($_POST['code']) ? trim($_POST['code'])  : '';

	if (strlen($new_password) < 6)
	{
		show_message(_L('account_password_too_short', '密碼長度最少6個字哦！'));
	}

	$user_info = $user->get_profile_by_id($user_id); //论坛记录

	//if (($user_info && (!empty($code) && md5($user_info['user_id'] . $_CFG['hash_code'] . $user_info['reg_time']) == $code)) || ($_SESSION['user_id']>0 && $_SESSION['user_id'] == $user_id && $user->check_user($_SESSION['user_name'], $old_password)))
	if ($_SESSION['user_id']>0 && $_SESSION['user_id'] == $user_id && $user->check_user($_SESSION['user_name'], $old_password))
	{
		
		//if ($user->edit_user(array('username'=> (empty($code) ? $_SESSION['user_name'] : $user_info['user_name']), 'old_password'=>$old_password, 'password'=>$new_password), empty($code) ? 0 : 1))
		if ($user->edit_user(array('username'=> $_SESSION['user_name'], 'old_password'=>$old_password, 'password'=>$new_password), 0))
		{
			// howang: added password change time
			$sql="UPDATE ".$ecs->table('users'). "SET `ec_salt`='',`salt`='0',`passchg_time`='".gmtime()."' WHERE user_id= '".$user_id."'";
			$db->query($sql);
			$user->logout();
			show_message(_L('account_reset_password_success', '修改密碼成功，請重新登入！'), _L('account_reset_password_back_to_login', '返回登入'), 'user.php?act=login', 'info');
		}
		else
		{
			show_message(_L('account_reset_password_failed', '修改密碼失敗，請稍後再試。'), _L('global_go_back', '返回上一頁'), '', 'info');
		}
	}
	else
	{
		show_message(_L('user_old_password_incorrect', '您輸入的現用密碼不正確！'), _L('global_go_back', '返回上一頁'), '', 'info');
	}

}

/* 查看订单列表 */
elseif ($action == 'order_list')
{
	include_once(ROOT_PATH . 'includes/lib_transaction.php');

	//Blocking Wholesale Customer
	if(in_array($user_id,$userController->getWholesaleUserIds()))
		show_message('批發客戶請直接聯絡營業員訂購');

	$page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;

	$record_count = $db->getOne("SELECT COUNT(*) FROM " .$ecs->table('order_info'). " WHERE user_id = '$user_id' AND order_status != " . OS_CANCELED);

	$pager  = get_pager('user.php', array('act' => $action), $record_count, $page);

	// $orders = get_user_orders($user_id, $pager['size'], $pager['start']);
	// $merge  = get_user_merge($user_id);
	$has_remind = get_order_not_upload_receipt($user_id);
	if ($has_remind) {
		$remind = "<div class='bd'>" .
						"<div id='upload_remind' class='notice_box'>" . sprintf(_L('user_require_upload', "<a href='/order_receipts/order/%s'><b>按此</b></a> 立即上傳訂單 <b>%s</b> 之付款收據。"), $has_remind['order_id'], $has_remind['order_sn']) . "</div>" .
					"</div>";
	}

	// $smarty->assign('merge',  $merge);
	$smarty->assign('pager',  $pager);
	// $smarty->assign('orders', $orders);
	$smarty->assign('remind', $remind);
	//$smarty->display('user_transaction.dwt');
	$smarty->assign('lbi_file', 'library/user_order_list.lbi');
	$smarty->display('user_center.dwt');
}

/* 查看订单详情 */
elseif ($action == 'order_detail')
{
	include_once(ROOT_PATH . 'includes/lib_transaction.php');
	include_once(ROOT_PATH . 'includes/lib_payment.php');
	include_once(ROOT_PATH . 'includes/lib_order.php');
	include_once(ROOT_PATH . 'includes/lib_clips.php');

	//Blocking Wholesale Customer
	if(in_array($user_id,$userController->getWholesaleUserIds()))
		show_message('批發客戶請直接聯絡營業員訂購');

	$order_id = isset($_GET['order_id']) ? intval($_GET['order_id']) : 0;

	/* 订单详情 */
	$order = get_order_detail($order_id, $user_id);
	$order_goods = order_goods($order_id, false);

	//check stock if it is enough
	if ($order['money_paid'] == 0 && $order['is_online_pay'] == 1) {
		$need_to_cancel_order = false;
		foreach ($order_goods as $goods_item) {
			// check current stock 
			//dd($erpController);
			if ($goods_item['is_gift'] != 0 || $goods_item['insurance_of'] != 0  || $goods_item['flashdeal_id'] != 0) {
				continue;
			}

			$goods_info = get_goods_info($goods_item['goods_id']);
			$current_stock = $goods_info['goods_number'];
			//$current_stock = $erpController->getSellableProductQty($goods_item['goods_id']);
			if ($current_stock <= 0 || $current_stock < ($goods_item['goods_number'] - $goods_item['send_number'])) {
				// check if per sales
				if ($goods_info['is_pre_sale'] != 1 && $goods_info['pre_sale_days'] > 0) {
					$need_to_cancel_order = false;
					break;
				} else {
					$need_to_cancel_order = true;
				}
			} else {
				$need_to_cancel_order = false;
				break;
			}
		}
		
		if ($need_to_cancel_order == true) {
			//cancel_order($order_id);
			show_message('“尊敬的客戶，由於您在購買 ['.$goods_info['goods_name'].'] 時只有1件庫存，同一時間有另外顧客下單，但比您先完成付款，因此此單系統會自動幫您取消，請重新下單，給您帶來不便，敬請諒解！”');
			exit;
		}
	}

	// get coupon promote
	$can_change_order_payment_type = ($orderController->canChangeOrderPaymentType($order_id) && $orderController->canChangeOrderPaymentTypeWithFlashDeal($order_id));
	
	$smarty->assign('can_change_order_payment_type', $can_change_order_payment_type);

	if ($order === false)
	{
		$err->show(_L('global_go_home', '返回首頁'), '/');

		exit;
	}
	
	// 如果不是門市購買或門市自取才顯示收貨人資料及地址
	$shipping = shipping_info($order['shipping_id']);
	if (!in_array($shipping['shipping_code'], array('cac','pickup')))
	{
		$smarty->assign('show_consignee', 1);
	}
	
	/* 是否显示添加到购物车 */
	if ($order['extension_code'] != 'group_buy' && $order['extension_code'] != 'exchange_goods')
	{
		$smarty->assign('allow_to_cart', 1);
	}

	/* 订单商品 */
	$goods_detail = order_goods($order_id, true);
	$goods_list = $goods_detail['goods'];
	foreach ($goods_list AS $key => $value)
	{
		$goods_list[$key]['market_price'] = price_format($value['market_price'], false);
		$goods_list[$key]['goods_price']  = price_format($value['goods_price'], false);
		$goods_list[$key]['subtotal']	 = price_format($value['subtotal'], false);
		
		$goods_thumb = $db->getOne("SELECT `goods_thumb` FROM " . $ecs->table('goods') . " WHERE `goods_id` = '" . $value['goods_id'] . "'");
		$goods_list[$key]['goods_thumb'] = get_image_path($value['goods_id'], $goods_thumb, true);
		$goods_list[$key]['goods_url'] = build_uri('goods', array('gid' => $value['goods_id'], 'gname' => $value['goods_name'], 'gperma' => $value['gperma']));
	}

	 /* 设置能否修改使用余额数 */
	if ($order['order_amount'] > 0)
	{
		if ($order['order_status'] == OS_UNCONFIRMED || $order['order_status'] == OS_CONFIRMED)
		{
			$user = user_info($order['user_id']);
			if ($user['user_money'] + $user['credit_line'] > 0)
			{
				$smarty->assign('allow_edit_surplus', 1);
				$smarty->assign('max_surplus', sprintf($_LANG['max_surplus'], $user['user_money']));
			}
		}
	}

	/* 未发货，未付款时允许更换支付方式 */
	if ($order['order_amount'] > 0 && $order['pay_status'] == PS_UNPAYED && $order['shipping_status'] == SS_UNSHIPPED)
	{
		$order['current_platform'] = $paymentController::PLATFORM_WEB;
        $user = user_info($order['user_id']);
		$online_only = false;
		$order_goods = order_goods($order_id);
		foreach ($order_goods as $og_k => $og) {
			// Have flashdeal goods, only can pay with online payment
			$sql = "SELECT is_online_pay FROM ".$ecs->table('flashdeals').' WHERE flashdeal_id = '.$og['flashdeal_id'];
			if($db->getOne($sql)) {
				$online_only = true;
				break;
			}

		}
		$filter_data['country']          = $order['country'];
		$filter_data['pay_id']           = $order['pay_id'];
		$filter_data['goods_amount']     = $order['goods_amount'];
		$filter_data['rank_points']      = $user['rank_points'];
		$filter_data['step']             = 'checkout';
		$filter_data['current_platform'] = $paymentController::PLATFORM_WEB;
		$filter_data['is_online_pay']    = $online_only;
		$payment_list = $paymentController->available_payment_list(false, 0, true, $filter_data);

		$smarty->assign('payment_list', $payment_list);
	}
	
	// Order Status, YOHO's way
	if (in_array($order['order_status'], array(OS_CANCELED, OS_INVALID, OS_RETURNED, OS_WAITING_REFUND)))
	{
		$order['yoho_order_status'] = $_LANG['os'][$order['order_status']];
	}
	else if (in_array($order['pay_status'], array(PS_UNPAYED, PS_PAYING)))
	{
		$order['yoho_order_status'] = $_LANG['ps'][$order['pay_status']];
	}
	else
	{
		$order['yoho_order_status'] = $order['shipping_status'] == SS_RECEIVED ? $_LANG['ss_received'] : $_LANG['ss'][$order['shipping_status']];
	}
	
	// Order step
	if (in_array($order['order_status'], array(OS_CANCELED, OS_INVALID, OS_RETURNED, OS_WAITING_REFUND)))
	{
		$order['step'] = 0;
	}
	else if (in_array($order['pay_status'], array(PS_UNPAYED, PS_PAYING)))
	{
		$order['step'] = 1;
	}
	else if (!in_array($order['shipping_status'], array(SS_SHIPPED, SS_RECEIVED)))
	{
		$order['step'] = 2;
	}
	else if ($order['shipping_status'] != SS_RECEIVED)
	{
		$order['step'] = 3;
	}
	else if (!$order['reviewed'])
	{
		$order['step'] = 4;
	}
	else
	{
		$order['step'] = 5;
	}
	$order['progress'] = $order['step'] * 20;

	$smarty->assign('goods_list', $goods_list);
	$smarty->assign('packages', $goods_detail['packages']);

	if($order['step'] == 3){
		//Get invoice no Carrier
        $sql = "SELECT l.logistics_url, l.logistics_code, l.logistics_name, do.invoice_no as trackingNumber FROM ".$ecs->table("delivery_order")." as do ".
		"LEFT JOIN ".$ecs->table("logistics")." as l ON do.logistics_id = l.logistics_id ".
		"WHERE do.order_id = '".$order['order_id']."' AND do.logistics_id > 0";
		$delivery_orders = $db->getAll($sql);
		// Check $delivery_orders
		$empty_delivery = false;
		foreach ($delivery_orders as $key => $value) {
			if(array_filter($value) && !empty($value['trackingNumber']) && $value['logistics_code'] != 'remark'){
				$empty_delivery = true;
			} else {
				unset($delivery_orders[$key]);
			}
		}
		if($empty_delivery){
			$smarty->assign('show_tracking_info', 1);
			$smarty->assign('delivery_orders',      $delivery_orders);
		}
	}
	if ($order['order_amount'] > 0)
	{
		require_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_accounting.php');
		$smarty->assign('payment_amount', $actgHooks->getOrderPaymentAmount($order));
	}
	
	// 付款
	$payl = isset($_REQUEST['payl']) ? trim($_REQUEST['payl']) : '0';
	if($payl == 1 && ($order['order_status'] == OS_CANCELED || $order['pay_status'] != PS_UNPAYED)){
		show_message(_L('global_can_not_paid', '無法支付此訂單'));
		exit;
	}
	$smarty->assign('payl', $payl);
	// If is 線上付款 及 付款成功, add purchase Event.
	if($_REQUEST['paysuccess'] && $_REQUEST['paysuccess']=='1' && $order['pay_status'] == PS_PAYED){
		$feed = $feedController->purchase_insert_code($order, $goods_list);
        $smarty->assign('stats_code_insert', $feed['stats_code_insert']);
        $smarty->assign('extra_insert_code', $feed['extra_insert_code']);
        $smarty->assign('just_online_paid', 1);

	}
	/* 订单 支付 配送 状态语言项 */
	$order['order_status'] = $_LANG['os'][$order['order_status']];
	$order['pay_status'] = $_LANG['ps'][$order['pay_status']];
	$order['shipping_status'] = $_LANG['ss'][$order['shipping_status']];

	$smarty->assign('order',	  $order);
	if ((!empty($_REQUEST['payment_processing'])) && ($_REQUEST['payment_processing'] == 1))
	{
		$smarty->display('user_paying.dwt');
	}
	else if($payl == 1)
	{
		if ($order['order_amount'] > 0)
		{
			$payment = payment_info($order['pay_id']);

			include_once('includes/modules/payment/' . $payment['pay_code'] . '.php');
			
			// $pay_obj	= new $payment['pay_code'];
			$pay_online = $order['pay_online'];
			
			$order['pay_desc'] = $payment['pay_desc'];
			
			//$smarty->assign('norder',	 order_info($order['order_sn']));
            $smarty->assign('payment', $payment);
			$smarty->assign('pay_online', $pay_online);
			$smarty->assign('is_online_pay', $payment['is_online_pay']);
		}
		$smarty->display('user_pay.dwt');
	}
	else
	{
		//$smarty->display('user_transaction.dwt');
		$smarty->assign('lbi_file', 'library/user_order_detail.lbi');
		$smarty->display('user_center.dwt');
	}
}



/****************************** Disable unused code start ******************************
// 详情代付
elseif ($action == 'order_detail_daifu')
{
	include_once(ROOT_PATH . 'includes/lib_transaction.php');
	include_once(ROOT_PATH . 'includes/lib_payment.php');
	include_once(ROOT_PATH . 'includes/lib_order.php');
	include_once(ROOT_PATH . 'includes/lib_clips.php');

	$order_id = isset($_GET['order_id']) ? intval($_GET['order_id']) : 0;
	$channel=$_GET['channel'];
		$channelarr=explode("_", $_GET['channel']); //   sbsn  获取数组  [0] pay_id	[1] branks
	   $pay_id = intval($channelarr[0]);
	   $bank_type=$channelarr[1];
	

	
	

	// 订单详情
	$order = get_order_detail($order_id, $user_id);

	if ($order === false)
	{
		$err->show(_L('global_go_home', '返回首頁'), './');

		exit;
	}

	// 是否显示添加到购物车
	if ($order['extension_code'] != 'group_buy' && $order['extension_code'] != 'exchange_goods')
	{
		$smarty->assign('allow_to_cart', 1);
	}

	// 订单商品
	$goods_list = order_goods($order_id);
	foreach ($goods_list AS $key => $value)
	{
		$goods_list[$key]['market_price'] = price_format($value['market_price'], false);
		$goods_list[$key]['goods_price']  = price_format($value['goods_price'], false);
		$goods_list[$key]['subtotal']	 = price_format($value['subtotal'], false);
	}

	// 设置能否修改使用余额数
	if ($order['order_amount'] > 0)
	{
		if ($order['order_status'] == OS_UNCONFIRMED || $order['order_status'] == OS_CONFIRMED)
		{
			$user = user_info($order['user_id']);
			if ($user['user_money'] + $user['credit_line'] > 0)
			{
				$smarty->assign('allow_edit_surplus', 1);
				$smarty->assign('max_surplus', sprintf($_LANG['max_surplus'], $user['user_money']));
			}
		}
	}

	// 未发货，未付款时允许更换支付方式
	if ($order['order_amount'] > 0 && $order['pay_status'] == PS_UNPAYED && $order['shipping_status'] == SS_UNSHIPPED)
	{
		$payment_list = $paymentController->available_payment_list(false, 0, true);

		// 过滤掉当前支付方式和余额支付方式
		if(is_array($payment_list))
		{
			foreach ($payment_list as $key => $payment)
			{
				if ($payment['pay_id'] == $order['pay_id'] || $payment['pay_code'] == 'balance')
				{
					unset($payment_list[$key]);
				}
			}
		}
		$smarty->assign('payment_list', $payment_list);
	}

	// 订单 支付 配送 状态语言项
	$order['order_status'] = $_LANG['os'][$order['order_status']];
	$order['pay_status'] = $_LANG['ps'][$order['pay_status']];
	$order['shipping_status'] = $_LANG['ss'][$order['shipping_status']];

	$smarty->assign('bank_type',	  $bank_type);
	$smarty->assign('norder',	  $order);//获取订单支付方式ID  ly sbsn
	$smarty->assign('order',	  $order);
	$smarty->assign('goods_list', $goods_list);
	$smarty->display('daifu.dwt');
}
****************************** Disable unused code end ******************************/

// 取消订单
elseif ($action == 'cancel_order')
{
	include_once(ROOT_PATH . 'includes/lib_transaction.php');
	include_once(ROOT_PATH . 'includes/lib_order.php');

	$order_id = isset($_GET['order_id']) ? intval($_GET['order_id']) : 0;

	if (cancel_order($order_id, $user_id))
	{
		$db->query("UPDATE " . $ecs->table("offline_payment") . " SET status = " . OFFLINE_PAYTMENT_CLOSED . " WHERE order_id = $order_id AND status < " . OFFLINE_PAYTMENT_FINISHED);
		header('Location: user.php?act=order_list');
		exit;
	}
	else
	{
		$err->show(_L('user_my_orders', '我的訂單'), 'user.php?act=order_list');
	}
}

/****************************** Disable unused code start ******************************
// 收货地址列表界面
elseif ($action == 'address_list')
{
	include_once(ROOT_PATH . 'includes/lib_transaction.php');
	include_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/shopping_flow.php');
	$smarty->assign('lang',  $_LANG);

	// 取得国家列表、商店所在国家、商店所在国家的省列表
	$smarty->assign('country_list',	   get_regions());
	$smarty->assign('shop_province_list', get_regions(1, $_CFG['shop_country']));

	// 获得用户所有的收货人信息
	$consignee_list = get_consignee_list($_SESSION['user_id']);

	if (count($consignee_list) < 5 && $_SESSION['user_id'] > 0)
	{
		// 如果用户收货人信息的总数小于5 则增加一个新的收货人信息
		$consignee_list[] = array('country' => $_CFG['shop_country'], 'email' => isset($_SESSION['email']) ? $_SESSION['email'] : '');
	}

	$smarty->assign('consignee_list', $consignee_list);

	//取得国家列表，如果有收货人列表，取得省市区列表
	foreach ($consignee_list AS $region_id => $consignee)
	{
		$consignee['country']  = isset($consignee['country'])  ? intval($consignee['country'])  : 0;
		$consignee['province'] = isset($consignee['province']) ? intval($consignee['province']) : 0;
		$consignee['city']	 = isset($consignee['city'])	 ? intval($consignee['city'])	 : 0;

		$province_list[$region_id] = get_regions(1, $consignee['country']);
		$city_list[$region_id]	 = get_regions(2, $consignee['province']);
		$district_list[$region_id] = get_regions(3, $consignee['city']);
	}

	// 获取默认收货ID
	$address_id  = $db->getOne("SELECT address_id FROM " .$ecs->table('users'). " WHERE user_id='$user_id'");

	// 赋值于模板
	$smarty->assign('real_goods_count', 1);
	$smarty->assign('shop_country',	 $_CFG['shop_country']);
	$smarty->assign('shop_province',	get_regions(1, $_CFG['shop_country']));
	$smarty->assign('province_list',	$province_list);
	$smarty->assign('address',		  $address_id);
	$smarty->assign('city_list',		$city_list);
	$smarty->assign('district_list',	$district_list);
	$smarty->assign('currency_format',  $_CFG['currency_format']);
	$smarty->assign('integral_scale',   $_CFG['integral_scale']);
	$smarty->assign('name_of_region',   array($_CFG['name_of_region_1'], $_CFG['name_of_region_2'], $_CFG['name_of_region_3'], $_CFG['name_of_region_4']));

	$smarty->display('user_transaction.dwt');
}

// 添加/编辑收货地址的处理
elseif ($action == 'act_edit_address')
{
	include_once(ROOT_PATH . 'includes/lib_transaction.php');
	include_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/shopping_flow.php');
	$smarty->assign('lang', $_LANG);

	$address = array(
		'user_id'	=> $user_id,
		'address_id' => intval($_POST['address_id']),
		'country'	=> isset($_POST['country'])   ? intval($_POST['country'])  : 0,
		'province'   => isset($_POST['province'])  ? intval($_POST['province']) : 0,
		'city'	   => isset($_POST['city'])	  ? intval($_POST['city'])	 : 0,
		'district'   => isset($_POST['district'])  ? intval($_POST['district']) : 0,
		'address'	=> isset($_POST['address'])   ? compile_str(trim($_POST['address']))	: '',
		'consignee'  => isset($_POST['consignee']) ? compile_str(trim($_POST['consignee']))  : '',
		'email'	  => isset($_POST['email'])	 ? compile_str(trim($_POST['email']))	  : '',
		'tel'		=> isset($_POST['tel'])	   ? compile_str(make_semiangle(trim($_POST['tel']))) : '',
		'mobile'	 => isset($_POST['mobile'])	? compile_str(make_semiangle(trim($_POST['mobile']))) : '',
		'best_time'  => isset($_POST['best_time']) ? compile_str(trim($_POST['best_time']))  : '',
		'sign_building' => isset($_POST['sign_building']) ? compile_str(trim($_POST['sign_building'])) : '',
		'zipcode'	   => isset($_POST['zipcode'])	   ? compile_str(make_semiangle(trim($_POST['zipcode']))) : '',
		);

	if (update_address($address))
	{
		show_message($_LANG['edit_address_success'], $_LANG['address_list_lnk'], 'user.php?act=address_list');
	}
}

// 删除收货地址
elseif ($action == 'drop_consignee')
{
	include_once('includes/lib_transaction.php');

	$consignee_id = intval($_GET['id']);

	if (drop_consignee($consignee_id))
	{
		ecs_header("Location: user.php?act=address_list\n");
		exit;
	}
	else
	{
		show_message($_LANG['del_address_false']);
	}
}

// 保存订单详情收货地址
elseif ($action == 'save_order_address')
{
	include_once(ROOT_PATH .'includes/lib_transaction.php');

	$address = array(
		'consignee' => isset($_POST['consignee']) ? compile_str(trim($_POST['consignee']))  : '',
		'email'	 => isset($_POST['email'])	 ? compile_str(trim($_POST['email']))	  : '',
		'address'   => isset($_POST['address'])   ? compile_str(trim($_POST['address']))	: '',
		'zipcode'   => isset($_POST['zipcode'])   ? compile_str(make_semiangle(trim($_POST['zipcode']))) : '',
		'tel'	   => isset($_POST['tel'])	   ? compile_str(trim($_POST['tel']))		: '',
		'mobile'	=> isset($_POST['mobile'])	? compile_str(trim($_POST['mobile']))	 : '',
		'sign_building' => isset($_POST['sign_building']) ? compile_str(trim($_POST['sign_building'])) : '',
		'best_time' => isset($_POST['best_time']) ? compile_str(trim($_POST['best_time']))  : '',
		'order_id'  => isset($_POST['order_id'])  ? intval($_POST['order_id']) : 0
		);
	if (save_order_address($address, $user_id))
	{
		ecs_header('Location: user.php?act=order_detail&order_id=' .$address['order_id']. "\n");
		exit;
	}
	else
	{
		$err->show($_LANG['order_list_lnk'], 'user.php?act=order_list');
	}
}

// 显示收藏商品列表
elseif ($action == 'collection_list')
{
	include_once(ROOT_PATH . 'includes/lib_clips.php');

	$page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;

	$record_count = $db->getOne("SELECT COUNT(*) FROM " .$ecs->table('collect_goods').
								" WHERE user_id='$user_id' ORDER BY add_time DESC");

	$pager = get_pager('user.php', array('act' => $action), $record_count, $page);
	$smarty->assign('pager', $pager);
	$smarty->assign('goods_list', get_collection_goods($user_id, $pager['size'], $pager['start']));
	$smarty->assign('url',		$ecs->url());
	$lang_list = array(
		'UTF8'   => $_LANG['charset']['utf8'],
		'GB2312' => $_LANG['charset']['zh_cn'],
		'BIG5'   => $_LANG['charset']['zh_tw'],
	);
	$smarty->assign('lang_list',  $lang_list);
	$smarty->assign('user_id',  $user_id);
	$smarty->display('user_clips.dwt');
}

// 删除收藏的商品
elseif ($action == 'delete_collection')
{
	include_once(ROOT_PATH . 'includes/lib_clips.php');

	$collection_id = isset($_GET['collection_id']) ? intval($_GET['collection_id']) : 0;

	if ($collection_id > 0)
	{
		$db->query('DELETE FROM ' .$ecs->table('collect_goods'). " WHERE rec_id='$collection_id' AND user_id ='$user_id'" );
	}

	ecs_header("Location: user.php?act=collection_list\n");
	exit;
}

// 添加关注商品
elseif ($action == 'add_to_attention')
{
	$rec_id = (int)$_GET['rec_id'];
	if ($rec_id)
	{
		$db->query('UPDATE ' .$ecs->table('collect_goods'). "SET is_attention = 1 WHERE rec_id='$rec_id' AND user_id ='$user_id'" );
	}
	ecs_header("Location: user.php?act=collection_list\n");
	exit;
}
// 取消关注商品
elseif ($action == 'del_attention')
{
	$rec_id = (int)$_GET['rec_id'];
	if ($rec_id)
	{
		$db->query('UPDATE ' .$ecs->table('collect_goods'). "SET is_attention = 0 WHERE rec_id='$rec_id' AND user_id ='$user_id'" );
	}
	ecs_header("Location: user.php?act=collection_list\n");
	exit;
}
// 显示留言列表
elseif ($action == 'message_list')
{
	include_once(ROOT_PATH . 'includes/lib_clips.php');

	$page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;

	$order_id = empty($_GET['order_id']) ? 0 : intval($_GET['order_id']);
	$order_info = array();

	// 获取用户留言的数量
	if ($order_id)
	{
		$sql = "SELECT COUNT(*) FROM " .$ecs->table('feedback').
				" WHERE parent_id = 0 AND order_id = '$order_id' AND user_id = '$user_id'";
		$order_info = $db->getRow("SELECT * FROM " . $ecs->table('order_info') . " WHERE order_id = '$order_id' AND user_id = '$user_id'");
		$order_info['url'] = 'user.php?act=order_detail&order_id=' . $order_id;
	}
	else
	{
		$sql = "SELECT COUNT(*) FROM " .$ecs->table('feedback').
		   " WHERE parent_id = 0 AND user_id = '$user_id' AND user_name = '" . $_SESSION['user_name'] . "' AND order_id=0";
	}

	$record_count = $db->getOne($sql);
	$act = array('act' => $action);

	if ($order_id != '')
	{
		$act['order_id'] = $order_id;
	}

	$pager = get_pager('user.php', $act, $record_count, $page, 5);

	$smarty->assign('message_list', get_message_list($user_id, $_SESSION['user_name'], $pager['size'], $pager['start'], $order_id));
	$smarty->assign('pager',		$pager);
	$smarty->assign('order_info',   $order_info);
	$smarty->display('user_clips.dwt');
}

// 显示评论列表
elseif ($action == 'comment_list')
{
	include_once(ROOT_PATH . 'includes/lib_clips.php');

	$page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;

	// 获取用户留言的数量
	$sql = "SELECT COUNT(*) FROM " .$ecs->table('comment').
		   " WHERE parent_id = 0 AND user_id = '$user_id'";
	$record_count = $db->getOne($sql);
	$pager = get_pager('user.php', array('act' => $action), $record_count, $page, 5);

	$smarty->assign('comment_list', get_comment_list($user_id, $pager['size'], $pager['start']));
	$smarty->assign('pager',		$pager);
	$smarty->display('user_clips.dwt');
}

// 添加我的留言
elseif ($action == 'act_add_message')
{
	include_once(ROOT_PATH . 'includes/lib_clips.php');

	$message = array(
		'user_id'	 => $user_id,
		'user_name'   => $_SESSION['user_name'],
		'user_email'  => $_SESSION['email'],
		'msg_type'	=> isset($_POST['msg_type']) ? intval($_POST['msg_type'])	 : 0,
		'msg_title'   => isset($_POST['msg_title']) ? trim($_POST['msg_title'])	 : '',
		'msg_content' => isset($_POST['msg_content']) ? trim($_POST['msg_content']) : '',
		'order_id'=>empty($_POST['order_id']) ? 0 : intval($_POST['order_id']),
		'upload'	  => (isset($_FILES['message_img']['error']) && $_FILES['message_img']['error'] == 0) || (!isset($_FILES['message_img']['error']) && isset($_FILES['message_img']['tmp_name']) && $_FILES['message_img']['tmp_name'] != 'none')
		 ? $_FILES['message_img'] : array()
	 );

	if (add_message($message))
	{
		show_message($_LANG['add_message_success'], $_LANG['message_list_lnk'], 'user.php?act=message_list&order_id=' . $message['order_id'],'info');
	}
	else
	{
		$err->show($_LANG['message_list_lnk'], 'user.php?act=message_list');
	}
}

// 标签云列表
elseif ($action == 'tag_list')
{
	include_once(ROOT_PATH . 'includes/lib_clips.php');

	$good_id = isset($_GET['id']) ? intval($_GET['id']) : 0;

	$smarty->assign('tags',	  get_user_tags($user_id));
	$smarty->assign('tags_from', 'user');
	$smarty->display('user_clips.dwt');
}

// 删除标签云的处理
elseif ($action == 'act_del_tag')
{
	include_once(ROOT_PATH . 'includes/lib_clips.php');

	$tag_words = isset($_GET['tag_words']) ? trim($_GET['tag_words']) : '';
	delete_tag($tag_words, $user_id);

	ecs_header("Location: user.php?act=tag_list\n");
	exit;

}

// 显示缺货登记列表
elseif ($action == 'booking_list')
{
	include_once(ROOT_PATH . 'includes/lib_clips.php');

	$page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;

	// 获取缺货登记的数量
	$sql = "SELECT COUNT(*) " .
			"FROM " .$ecs->table('booking_goods'). " AS bg, " .
					 $ecs->table('goods') . " AS g " .
			"WHERE bg.goods_id = g.goods_id AND user_id = '$user_id'";
	$record_count = $db->getOne($sql);
	$pager = get_pager('user.php', array('act' => $action), $record_count, $page);

	$smarty->assign('booking_list', get_booking_list($user_id, $pager['size'], $pager['start']));
	$smarty->assign('pager',		$pager);
	$smarty->display('user_clips.dwt');
}
// 添加缺货登记页面
elseif ($action == 'add_booking')
{
	include_once(ROOT_PATH . 'includes/lib_clips.php');

	$goods_id = isset($_GET['id']) ? intval($_GET['id']) : 0;
	if ($goods_id == 0)
	{
		show_message($_LANG['no_goods_id'], $_LANG['back_page_up'], '', 'error');
	}

	// 根据规格属性获取货品规格信息
	$goods_attr = '';
	if ($_GET['spec'] != '')
	{
		$goods_attr_id = $_GET['spec'];

		$attr_list = array();
		$sql = "SELECT a.attr_name, g.attr_value " .
				"FROM " . $ecs->table('goods_attr') . " AS g, " .
					$ecs->table('attribute') . " AS a " .
				"WHERE g.attr_id = a.attr_id " .
				"AND g.goods_attr_id " . db_create_in($goods_attr_id);
		$res = $db->query($sql);
		while ($row = $db->fetchRow($res))
		{
			$attr_list[] = $row['attr_name'] . ': ' . $row['attr_value'];
		}
		$goods_attr = join(chr(13) . chr(10), $attr_list);
	}
	$smarty->assign('goods_attr', $goods_attr);

	$smarty->assign('info', get_goodsinfo($goods_id));
	$smarty->display('user_clips.dwt');

}

// 添加缺货登记的处理
elseif ($action == 'act_add_booking')
{
	include_once(ROOT_PATH . 'includes/lib_clips.php');

	$booking = array(
		'goods_id'	 => isset($_POST['id'])	  ? intval($_POST['id'])	 : 0,
		'goods_amount' => isset($_POST['number'])  ? intval($_POST['number']) : 0,
		'desc'		 => isset($_POST['desc'])	? trim($_POST['desc'])	 : '',
		'linkman'	  => isset($_POST['linkman']) ? trim($_POST['linkman'])  : '',
		'email'		=> isset($_POST['email'])   ? trim($_POST['email'])	: '',
		'tel'		  => isset($_POST['tel'])	 ? trim($_POST['tel'])	  : '',
		'booking_id'   => isset($_POST['rec_id'])  ? intval($_POST['rec_id']) : 0
	);

	// 查看此商品是否已经登记过
	$rec_id = get_booking_rec($user_id, $booking['goods_id']);
	if ($rec_id > 0)
	{
		show_message($_LANG['booking_rec_exist'], $_LANG['back_page_up'], '', 'error');
	}

	if (add_booking($booking))
	{
		show_message($_LANG['booking_success'], $_LANG['back_booking_list'], 'user.php?act=booking_list',
		'info');
	}
	else
	{
		$err->show($_LANG['booking_list_lnk'], 'user.php?act=booking_list');
	}
}

// 删除缺货登记
elseif ($action == 'act_del_booking')
{
	include_once(ROOT_PATH . 'includes/lib_clips.php');

	$id = isset($_GET['id']) ? intval($_GET['id']) : 0;
	if ($id == 0 || $user_id == 0)
	{
		ecs_header("Location: user.php?act=booking_list\n");
		exit;
	}

	$result = delete_booking($id, $user_id);
	if ($result)
	{
		ecs_header("Location: user.php?act=booking_list\n");
		exit;
	}
}
****************************** Disable unused code end ******************************/

// 确认收货
elseif ($action == 'affirm_received')
{
	include_once(ROOT_PATH . 'includes/lib_transaction.php');

	$order_id = isset($_GET['order_id']) ? intval($_GET['order_id']) : 0;

	if (affirm_received($order_id, $user_id))
	{
		header('Location: user.php?act=order_list');
		exit;
	}
	else
	{
		$err->show(_L('user_my_orders', '我的訂單'), 'user.php?act=order_list');
	}
}

/****************************** Disable unused code start ******************************
// 会员退款申请界面
elseif ($action == 'account_raply')
{
	$smarty->display('user_transaction.dwt');
}

// 会员预付款界面
elseif ($action == 'account_deposit')
{
	include_once(ROOT_PATH . 'includes/lib_clips.php');

	$surplus_id = isset($_GET['id']) ? intval($_GET['id']) : 0;
	$account	= get_surplus_info($surplus_id);

	$smarty->assign('payment', get_online_payment_list(false));
	$smarty->assign('order',   $account);
   // $smarty->display('user_transaction.dwt');
	  $smarty->display('user_chongzhi.dwt'); //sbsn ly  修改充值方式
}

// 会员账目明细界面
elseif ($action == 'account_detail')
{
	include_once(ROOT_PATH . 'includes/lib_clips.php');

	$page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;

	$account_type = 'user_money';

	// 获取记录条数
	$sql = "SELECT COUNT(*) FROM " .$ecs->table('account_log').
		   " WHERE user_id = '$user_id'" .
		   " AND $account_type <> 0 ";
	$record_count = $db->getOne($sql);

	//分页函数
	$pager = get_pager('user.php', array('act' => $action), $record_count, $page);

	//获取剩余余额
	$surplus_amount = get_user_surplus($user_id);
	if (empty($surplus_amount))
	{
		$surplus_amount = 0;
	}

	//获取余额记录
	$account_log = array();
	$sql = "SELECT * FROM " . $ecs->table('account_log') .
		   " WHERE user_id = '$user_id'" .
		   " AND $account_type <> 0 " .
		   " ORDER BY log_id DESC";
	$res = $GLOBALS['db']->selectLimit($sql, $pager['size'], $pager['start']);
	while ($row = $db->fetchRow($res))
	{
		$row['change_time'] = local_date($_CFG['date_format'], $row['change_time']);
		$row['type'] = $row[$account_type] > 0 ? $_LANG['account_inc'] : $_LANG['account_dec'];
		$row['user_money'] = price_format(abs($row['user_money']), false);
		$row['frozen_money'] = price_format(abs($row['frozen_money']), false);
		$row['rank_points'] = abs($row['rank_points']);
		$row['pay_points'] = abs($row['pay_points']);
		$row['short_change_desc'] = sub_str($row['change_desc'], 60);
		$row['amount'] = $row[$account_type];
		$account_log[] = $row;
	}

	//模板赋值
	$smarty->assign('surplus_amount', price_format($surplus_amount, false));
	$smarty->assign('account_log',	$account_log);
	$smarty->assign('pager',		  $pager);
	$smarty->display('user_transaction.dwt');
}

// 会员充值和提现申请记录
elseif ($action == 'account_log')
{
	include_once(ROOT_PATH . 'includes/lib_clips.php');

	$page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;

	// 获取记录条数
	$sql = "SELECT COUNT(*) FROM " .$ecs->table('user_account').
		   " WHERE user_id = '$user_id'" .
		   " AND process_type " . db_create_in(array(SURPLUS_SAVE, SURPLUS_RETURN));
	$record_count = $db->getOne($sql);

	//分页函数
	$pager = get_pager('user.php', array('act' => $action), $record_count, $page);

	//获取剩余余额
	$surplus_amount = get_user_surplus($user_id);
	if (empty($surplus_amount))
	{
		$surplus_amount = 0;
	}

	//获取余额记录
	$account_log = get_account_log($user_id, $pager['size'], $pager['start']);

	//模板赋值
	$smarty->assign('surplus_amount', price_format($surplus_amount, false));
	$smarty->assign('account_log',	$account_log);
	$smarty->assign('pager',		  $pager);
	$smarty->display('user_transaction.dwt');
}

// 对会员余额申请的处理
elseif ($action == 'act_account')
{
	include_once(ROOT_PATH . 'includes/lib_clips.php');
	include_once(ROOT_PATH . 'includes/lib_order.php');
	$amount = isset($_POST['amount']) ? floatval($_POST['amount']) : 0;
	if ($amount <= 0)
	{
		show_message($_LANG['amount_gt_zero']);
	}

	// 变量初始化
	
	$bank=explode("_", $_POST['channel']); //   sbsn  获取数组  [0] pay_id	[1] branks
	$surplus = array(
			'user_id'	  => $user_id,
			'rec_id'	   => !empty($_POST['rec_id'])	  ? intval($_POST['rec_id'])	   : 0,
			'process_type' => isset($_POST['surplus_type']) ? intval($_POST['surplus_type']) : 0,
			'payment_id'   => isset($_POST['channel'])   ? intval($bank[0])   : 0,//  获取PAY_ID SBSN LY
			'banks'   =>  isset($bank[1])   ? $bank[1]   : "",//  获取银行信息
			'user_note'	=> isset($_POST['user_note'])	? trim($_POST['user_note'])	  : '',
			'amount'	   => $amount
	);
	

	// 退款申请的处理
	if ($surplus['process_type'] == 1)
	{
		// 判断是否有足够的余额的进行退款的操作
		$sur_amount = get_user_surplus($user_id);
		if ($amount > $sur_amount)
		{
			$content = $_LANG['surplus_amount_error'];
			show_message($content, $_LANG['back_page_up'], '', 'info');
		}

		//插入会员账目明细
		$amount = '-'.$amount;
		$surplus['payment'] = '';
		$surplus['rec_id']  = insert_user_account($surplus, $amount);

		// 如果成功提交
		if ($surplus['rec_id'] > 0)
		{
			$content = $_LANG['surplus_appl_submit'];
			show_message($content, $_LANG['back_account_log'], 'user.php?act=account_log', 'info');
		}
		else
		{
			$content = $_LANG['process_false'];
			show_message($content, $_LANG['back_page_up'], '', 'info');
		}
	}
	// 如果是会员预付款，跳转到下一步，进行线上支付的操作
	else
	{
		if ($surplus['payment_id'] <= 0)
		{
			show_message($_LANG['select_payment_pls']);
		}

		include_once(ROOT_PATH .'includes/lib_payment.php');

		//获取支付方式名称
		$payment_info = array();
		$payment_info = payment_info($surplus['payment_id']);
		$surplus['payment'] = $payment_info['pay_name'];

		if ($surplus['rec_id'] > 0)
		{
			//更新会员账目明细
			$surplus['rec_id'] = update_user_account($surplus);
		}
		else
		{
			//插入会员账目明细
			$surplus['rec_id'] = insert_user_account($surplus, $amount);
		}

		//取得支付信息，生成支付代码
		$payment = unserialize_config($payment_info['pay_config']);

		//生成伪订单号, 不足的时候补0
		$order = array();
		$order['order_sn']	   = $surplus['rec_id'];
		$order['user_name']	  = $_SESSION['user_name'];
		$order['surplus_amount'] = $amount;

		//计算支付手续费用
		$payment_info['pay_fee'] = pay_fee($surplus['payment_id'], $order['surplus_amount'], 0);

		//计算此次预付款需要支付的总金额
		$order['order_amount']   = $amount + $payment_info['pay_fee'];

		//记录支付log
		$order['log_id'] = insert_pay_log($surplus['rec_id'], $order['order_amount'], $type=PAY_SURPLUS, 0);
		$order['bank'] =$bank[1];

		// 调用相应的支付方式文件
		include_once(ROOT_PATH . 'includes/modules/payment/' . $payment_info['pay_code'] . '.php');

		// 取得在线支付方式的支付按钮
		$pay_obj = new $payment_info['pay_code'];
		$payment_info['pay_button'] = $pay_obj->get_code($order, $payment);

		// 模板赋值
		$smarty->assign('payment', $payment_info);
		$smarty->assign('pay_fee', price_format($payment_info['pay_fee'], false));
		$smarty->assign('amount',  price_format($amount, false));
		$smarty->assign('order',   $order);
		$smarty->display('daifu_chongzhi.dwt');
	}
}

// 删除会员余额
elseif ($action == 'cancel')
{
	include_once(ROOT_PATH . 'includes/lib_clips.php');

	$id = isset($_GET['id']) ? intval($_GET['id']) : 0;
	if ($id == 0 || $user_id == 0)
	{
		ecs_header("Location: user.php?act=account_log\n");
		exit;
	}

	$result = del_user_account($id, $user_id);
	if ($result)
	{
		ecs_header("Location: user.php?act=account_log\n");
		exit;
	}
}

// 会员通过帐目明细列表进行再付款的操作
elseif ($action == 'pay')
{
	include_once(ROOT_PATH . 'includes/lib_clips.php');
	include_once(ROOT_PATH . 'includes/lib_payment.php');
	include_once(ROOT_PATH . 'includes/lib_order.php');

	//变量初始化
	$surplus_id = isset($_GET['id'])  ? intval($_GET['id'])  : 0;
	$payment_id = isset($_GET['pid']) ? intval($_GET['pid']) : 0;

	if ($surplus_id == 0)
	{
		ecs_header("Location: user.php?act=account_log\n");
		exit;
	}

	//如果原来的支付方式已禁用或者已删除, 重新选择支付方式
	if ($payment_id == 0)
	{
		ecs_header("Location: user.php?act=account_deposit&id=".$surplus_id."\n");
		exit;
	}

	//获取单条会员帐目信息
	$order = array();
	$order = get_surplus_info($surplus_id);

	//支付方式的信息
	$payment_info = array();
	$payment_info = payment_info($payment_id);

	// 如果当前支付方式没有被禁用，进行支付的操作
	if (!empty($payment_info))
	{
		//取得支付信息，生成支付代码
		$payment = unserialize_config($payment_info['pay_config']);

		//生成伪订单号
		$order['order_sn'] = $surplus_id;

		//获取需要支付的log_id
		$order['log_id'] = get_paylog_id($surplus_id, $pay_type = PAY_SURPLUS);

		$order['user_name']	  = $_SESSION['user_name'];
		$order['surplus_amount'] = $order['amount'];

		//计算支付手续费用
		$payment_info['pay_fee'] = pay_fee($payment_id, $order['surplus_amount'], 0);

		//计算此次预付款需要支付的总金额
		$order['order_amount']   = $order['surplus_amount'] + $payment_info['pay_fee'];

		//如果支付费用改变了，也要相应的更改pay_log表的order_amount
		$order_amount = $db->getOne("SELECT order_amount FROM " .$ecs->table('pay_log')." WHERE log_id = '$order[log_id]'");
		if ($order_amount <> $order['order_amount'])
		{
			$db->query("UPDATE " .$ecs->table('pay_log').
					   " SET order_amount = '$order[order_amount]' WHERE log_id = '$order[log_id]'");
		}

		// 调用相应的支付方式文件
		include_once(ROOT_PATH . 'includes/modules/payment/' . $payment_info['pay_code'] . '.php');

		// 取得在线支付方式的支付按钮
		$pay_obj = new $payment_info['pay_code'];
		$payment_info['pay_button'] = $pay_obj->get_code($order, $payment);

		// 模板赋值
		$smarty->assign('payment', $payment_info);
		$smarty->assign('order',   $order);
		$smarty->assign('pay_fee', price_format($payment_info['pay_fee'], false));
		$smarty->assign('amount',  price_format($order['surplus_amount'], false));
		$smarty->assign('action',  'act_account');
		if($payment_id>0)
		{$smarty->display('daifu_chongzhi.dwt');}
		else
		{
		$smarty->display('user_transaction.dwt');
		}
	}
	// 重新选择支付方式
	else
	{
		include_once(ROOT_PATH . 'includes/lib_clips.php');

		$smarty->assign('payment', get_online_payment_list());
		$smarty->assign('order',   $order);
		$smarty->assign('action',  'account_deposit');
		$smarty->display('user_transaction.dwt');
	}
}

// 添加标签(ajax)
elseif ($action == 'add_tag')
{
	include_once('includes/cls_json.php');
	include_once('includes/lib_clips.php');

	$result = array('error' => 0, 'message' => '', 'content' => '');
	$id	 = isset($_POST['id']) ? intval($_POST['id']) : 0;
	$tag	= isset($_POST['tag']) ? json_str_iconv(trim($_POST['tag'])) : '';

	if ($user_id == 0)
	{
		// 用户没有登录
		$result['error']   = 1;
		$result['message'] = $_LANG['tag_anonymous'];
	}
	else
	{
		add_tag($id, $tag); // 添加tag
		clear_cache_files('goods'); // 删除缓存

		// 重新获得该商品的所有缓存
		$arr = get_tags($id);

		foreach ($arr AS $row)
		{
			$result['content'][] = array('word' => htmlspecialchars($row['tag_words']), 'count' => $row['tag_count']);
		}
	}

	$json = new JSON;

	echo $json->encode($result);
	exit;
}

// 添加收藏商品(ajax)
elseif ($action == 'collect')
{
	include_once(ROOT_PATH .'includes/cls_json.php');
	$json = new JSON();
	$result = array('error' => 0, 'message' => '');
	$goods_id = $_GET['id'];

	if (!isset($_SESSION['user_id']) || $_SESSION['user_id'] == 0)
	{
		$result['error'] = 1;
		$result['message'] = $_LANG['login_please'];
		die($json->encode($result));
	}
	else
	{
		// 检查是否已经存在于用户的收藏夹
		$sql = "SELECT COUNT(*) FROM " .$GLOBALS['ecs']->table('collect_goods') .
			" WHERE user_id='$_SESSION[user_id]' AND goods_id = '$goods_id'";
		if ($GLOBALS['db']->GetOne($sql) > 0)
		{
			$result['error'] = 1;
			$result['message'] = $GLOBALS['_LANG']['collect_existed'];
			die($json->encode($result));
		}
		else
		{
			$time = gmtime();
			$sql = "INSERT INTO " .$GLOBALS['ecs']->table('collect_goods'). " (user_id, goods_id, add_time)" .
					"VALUES ('$_SESSION[user_id]', '$goods_id', '$time')";

			if ($GLOBALS['db']->query($sql) === false)
			{
				$result['error'] = 1;
				$result['message'] = $GLOBALS['db']->errorMsg();
				die($json->encode($result));
			}
			else
			{
				$result['error'] = 0;
				$result['message'] = $GLOBALS['_LANG']['collect_success'];
				die($json->encode($result));
			}
		}
	}
}

// 删除留言
elseif ($action == 'del_msg')
{
	$id = isset($_GET['id']) ? intval($_GET['id']) : 0;
	$order_id = empty($_GET['order_id']) ? 0 : intval($_GET['order_id']);

	if ($id > 0)
	{
		$sql = 'SELECT user_id, message_img FROM ' .$ecs->table('feedback'). " WHERE msg_id = '$id' LIMIT 1";
		$row = $db->getRow($sql);
		if ($row && $row['user_id'] == $user_id)
		{
			// 验证通过，删除留言，回复，及相应文件
			if ($row['message_img'])
			{
				@unlink(ROOT_PATH . DATA_DIR . '/feedbackimg/'. $row['message_img']);
			}
			$sql = "DELETE FROM " .$ecs->table('feedback'). " WHERE msg_id = '$id' OR parent_id = '$id'";
			$db->query($sql);
		}
	}
	ecs_header("Location: user.php?act=message_list&order_id=$order_id\n");
	exit;
}

// 删除评论
elseif ($action == 'del_cmt')
{
	$id = isset($_GET['id']) ? intval($_GET['id']) : 0;
	if ($id > 0)
	{
		$sql = "DELETE FROM " .$ecs->table('comment'). " WHERE comment_id = '$id' AND user_id = '$user_id'";
		$db->query($sql);
	}
	ecs_header("Location: user.php?act=comment_list\n");
	exit;
}

// 合并订单
elseif ($action == 'merge_order')
{
	include_once(ROOT_PATH .'includes/lib_transaction.php');
	include_once(ROOT_PATH .'includes/lib_order.php');
	$from_order = isset($_POST['from_order']) ? trim($_POST['from_order']) : '';
	$to_order   = isset($_POST['to_order']) ? trim($_POST['to_order']) : '';
	if (merge_user_order($from_order, $to_order, $user_id))
	{
		show_message($_LANG['merge_order_success'],$_LANG['order_list_lnk'],'user.php?act=order_list', 'info');
	}
	else
	{
		$err->show($_LANG['order_list_lnk']);
	}
}
// 将指定订单中商品添加到购物车
elseif ($action == 'return_to_cart')
{
	include_once(ROOT_PATH .'includes/cls_json.php');
	include_once(ROOT_PATH .'includes/lib_transaction.php');
	$json = new JSON();

	$result = array('error' => 0, 'message' => '', 'content' => '');
	$order_id = isset($_POST['order_id']) ? intval($_POST['order_id']) : 0;
	if ($order_id == 0)
	{
		$result['error']   = 1;
		$result['message'] = $_LANG['order_id_empty'];
		die($json->encode($result));
	}

	if ($user_id == 0)
	{
		// 用户没有登录
		$result['error']   = 1;
		$result['message'] = $_LANG['login_please'];
		die($json->encode($result));
	}

	// 检查订单是否属于该用户
	$order_user = $db->getOne("SELECT user_id FROM " .$ecs->table('order_info'). " WHERE order_id = '$order_id'");
	if (empty($order_user))
	{
		$result['error'] = 1;
		$result['message'] = $_LANG['order_exist'];
		die($json->encode($result));
	}
	else
	{
		if ($order_user != $user_id)
		{
			$result['error'] = 1;
			$result['message'] = $_LANG['no_priv'];
			die($json->encode($result));
		}
	}

	$message = return_to_cart($order_id);

	if ($message === true)
	{
		$result['error'] = 0;
		$result['message'] = $_LANG['return_to_cart_success'];
		die($json->encode($result));
	}
	else
	{
		$result['error'] = 1;
		$result['message'] = $_LANG['order_exist'];
		die($json->encode($result));
	}

}

// 编辑使用余额支付的处理
elseif ($action == 'act_edit_surplus')
{
	// 检查是否登录
	if ($_SESSION['user_id'] <= 0)
	{
		ecs_header("Location: ./\n");
		exit;
	}

	// 检查订单号
	$order_id = intval($_POST['order_id']);
	if ($order_id <= 0)
	{
		ecs_header("Location: ./\n");
		exit;
	}

	// 检查余额
	$surplus = floatval($_POST['surplus']);
	if ($surplus <= 0)
	{
		$err->add($_LANG['error_surplus_invalid']);
		$err->show($_LANG['order_detail'], 'user.php?act=order_detail&order_id=' . $order_id);
	}

	include_once(ROOT_PATH . 'includes/lib_order.php');

	// 取得订单
	$order = order_info($order_id);
	if (empty($order))
	{
		ecs_header("Location: ./\n");
		exit;
	}

	// 检查订单用户跟当前用户是否一致
	if ($_SESSION['user_id'] != $order['user_id'])
	{
		ecs_header("Location: ./\n");
		exit;
	}

	// 检查订单是否未付款，检查应付款金额是否大于0
	if ($order['pay_status'] != PS_UNPAYED || $order['order_amount'] <= 0)
	{
		$err->add($_LANG['error_order_is_paid']);
		$err->show($_LANG['order_detail'], 'user.php?act=order_detail&order_id=' . $order_id);
	}

	// 计算应付款金额（减去支付费用）
	$order['order_amount'] -= $order['pay_fee'];

	// 余额是否超过了应付款金额，改为应付款金额
	if ($surplus > $order['order_amount'])
	{
		$surplus = $order['order_amount'];
	}

	// 取得用户信息
	$user = user_info($_SESSION['user_id']);

	// 用户帐户余额是否足够
	if ($surplus > $user['user_money'] + $user['credit_line'])
	{
		$err->add($_LANG['error_surplus_not_enough']);
		$err->show($_LANG['order_detail'], 'user.php?act=order_detail&order_id=' . $order_id);
	}

	// 修改订单，重新计算支付费用
	$order['surplus'] += $surplus;
	$order['order_amount'] -= $surplus;
	if ($order['order_amount'] > 0)
	{
		$cod_fee = 0;
		if ($order['shipping_id'] > 0)
		{
			$regions  = array($order['country'], $order['province'], $order['city'], $order['district']);
			$shipping = shipping_area_info($order['shipping_id'], $regions);
			if ($shipping['support_cod'] == '1')
			{
				$cod_fee = $shipping['pay_fee'];
			}
		}

		$pay_fee = 0;
		if ($order['pay_id'] > 0)
		{
			$pay_fee = pay_fee($order['pay_id'], $order['order_amount'], $cod_fee);
		}

		$order['pay_fee'] = $pay_fee;
		$order['order_amount'] += $pay_fee;
	}

	// 如果全部支付，设为已确认、已付款
	if ($order['order_amount'] == 0)
	{
		if ($order['order_status'] == OS_UNCONFIRMED)
		{
			$order['order_status'] = OS_CONFIRMED;
			$order['confirm_time'] = gmtime();
		}
		$order['pay_status'] = PS_PAYED;
		$order['pay_time'] = gmtime();
	}
	$order = addslashes_deep($order);
	update_order($order_id, $order);

	// 更新用户余额
	$change_desc = sprintf($_LANG['pay_order_by_surplus'], $order['order_sn']);
	log_account_change($user['user_id'], (-1) * $surplus, 0, 0, 0, $change_desc);

	// 跳转
	ecs_header('Location: user.php?act=order_detail&order_id=' . $order_id . "\n");
	exit;
}
****************************** Disable unused code end ******************************/

elseif ($action == 'act_edit_payment')
{
	/* 检查是否登录 */
	if ($_SESSION['user_id'] <= 0)
	{
		show_message($_LANG['not_login'], $_LANG['login_now'], 'user.php?act=login', 'error');
		exit;
	}
	
	$pay_id = empty($_REQUEST['pay_id']) ? -1 : $_REQUEST['pay_id'];
	
	if ($pay_id <= 0)
	{
		show_message(
			_L('payment_change_method_error_missing', '請選擇付款方式'),
			_L('global_go_back', '返回上一頁'), 'javascript:history.back(-1)', 'warning');
		exit;
	}
	
	include_once(ROOT_PATH . 'includes/lib_order.php');
	
	$payment_info = payment_info($pay_id);

	if (empty($payment_info))
	{
		show_message(
			_L('payment_change_method_error_invalid', '錯誤的付款方式'),
			_L('global_go_back', '返回上一頁'), 'javascript:history.back(-1)', 'error');
		exit;
	}
	
	/* 检查订单号 */
	$order_id = intval($_POST['order_id']);
	if ($order_id <= 0)
	{
		show_message(
			_L('user_order_not_exists', '訂單並不存在'),
			_L('global_go_back', '返回上一頁'), 'javascript:history.back(-1)', 'error');
		exit;
	}
	
	/* 取得订单 */
	$order = order_info($order_id);
	
	if (empty($order))
	{
		show_message(
			_L('user_order_not_exists', '訂單並不存在'),
			_L('global_go_back', '返回上一頁'), 'javascript:history.back(-1)', 'error');
		exit;
	}
	
	/* 检查订单用户跟当前用户是否一致 */
	if ($_SESSION['user_id'] != $order['user_id'])
	{
		show_message(
			_L('user_order_not_exists', '訂單並不存在'),
			_L('global_go_back', '返回上一頁'), 'javascript:history.back(-1)', 'error');
		exit;
	}
	
	/* 未发货，未付款时允许更换支付方式 */
	if (!($order['order_amount'] > 0 && $order['pay_status'] == PS_UNPAYED && $order['shipping_status'] == SS_UNSHIPPED))
	{
		show_message(
			_L('payment_change_method_error_order_status', '此訂單不允許修改付款方式。'),
			_L('global_go_back', '返回上一頁'), 'javascript:history.back(-1)', 'error');
		exit;
	}

	// Update order_amount according to new pay_fee
	$order_amount = $order['order_amount'] - $order['pay_fee'];
	$pay_fee = pay_fee($pay_id, $order_amount);
	$order_amount += $pay_fee;
	
	$sql = "UPDATE " . $ecs->table('order_info') .
		   " SET pay_id='$pay_id', pay_name='$payment_info[display_name]', pay_fee='$pay_fee', order_amount='$order_amount'".
		   " WHERE order_id = '$order_id' LIMIT 1";
	$db->query($sql);
	
	//如果支付费用改变了，也要相应的更改pay_log表的order_amount
	$pay_log_amount = $db->getOne("SELECT order_amount FROM " .$ecs->table('pay_log')." WHERE order_id = '$order_id' AND order_type = 'PAY_SURPLUS' AND is_paid = 0");
	
	if ($pay_log_amount != $order_amount)
	{
		$db->query("UPDATE " .$ecs->table('pay_log').
				" SET order_amount = '$order_amount' WHERE order_id = '$order_id' AND order_type = 'PAY_SURPLUS' AND is_paid = 0");
	}

	if ($payment_info['is_online_pay'] == '0') {
		$crmController = new Yoho\cms\Controller\CrmController();
		$crmController->init_offline_payment_flow($order_id);
	} else {
		$db->query("UPDATE " . $ecs->table("offline_payment") . " SET status = " . OFFLINE_PAYTMENT_CLOSED . " WHERE order_id = $order_id AND status < " . OFFLINE_PAYTMENT_FINISHED);
	}

	/* 跳转 */
	header('Location: user.php?act=order_detail&order_id=' . $order_id . '&payl=1');
	exit;
}

/****************************** Disable unused code start ******************************
// 我的红包列表
elseif ($action == 'bonus')
{
	include_once(ROOT_PATH .'includes/lib_transaction.php');

	$page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
	$record_count = $db->getOne("SELECT COUNT(*) FROM " .$ecs->table('user_bonus'). " WHERE user_id = '$user_id'");

	$pager = get_pager('user.php', array('act' => $action), $record_count, $page);
	$bonus = get_user_bouns_list($user_id, $pager['size'], $pager['start']);

	$smarty->assign('pager', $pager);
	$smarty->assign('bonus', $bonus);
	$smarty->display('user_transaction.dwt');
}

// 添加一个红包
elseif ($action == 'act_add_bonus')
{
	include_once(ROOT_PATH . 'includes/lib_transaction.php');

	$bouns_sn = isset($_POST['bonus_sn']) ? intval($_POST['bonus_sn']) : '';

	if (add_bonus($user_id, $bouns_sn))
	{
		show_message($_LANG['add_bonus_sucess'], _L('global_go_back', '返回上一頁'), 'user.php?act=bonus', 'info');
	}
	else
	{
		$err->show(_L('global_go_back', '返回上一頁'), 'user.php?act=bonus');
	}
}

// 我的团购列表
elseif ($action == 'group_buy')
{
	include_once(ROOT_PATH .'includes/lib_transaction.php');

	//待议
	$smarty->display('user_transaction.dwt');
}

// 团购订单详情
elseif ($action == 'group_buy_detail')
{
	include_once(ROOT_PATH .'includes/lib_transaction.php');

	//待议
	$smarty->display('user_transaction.dwt');
}
****************************** Disable unused code end ******************************/

// 用户推荐页面
elseif ($action == 'affiliate')
{
	// If user is not an affiliate, redirect back to user center
	if (!$is_affiliate && $affiliate['on'] != 1)
	{
		header('Location: user.php?act=default');
		exit;
	}
	
	empty($affiliate) && $affiliate = array();
	
	$cookie_expire = $affiliate['config']['expire'] . $_LANG['expire_unit'][$affiliate['config']['expire_unit']];
	$smarty->assign('cookie_expire', $cookie_expire);
	
	// Get rewards list
    $sql = "SELECT ar.* " .
            "FROM " . $GLOBALS['ecs']->table('affiliate_rewards') . " as ar " .
            "WHERE ar.user_id = '" . $user_id . "' " .
            "ORDER BY ar.reward_id DESC";
    $data = $GLOBALS['db']->getAll($sql);
    
    $rewards_list = array();
    foreach ($data as $row)
    {
        $row['start_date'] = local_date($_CFG['date_format'], $row['start_time']);
        $row['end_date'] = local_date($_CFG['date_format'], $row['end_time']);
        
        $rewards_list[$row['reward_id']] = $row;
    }
    
    $reward_ids = array_map(function ($row) { return $row['reward_id']; }, $data);
    
    $sql = "SELECT ard.*, g.goods_id, g.goods_name, c.cat_id, c.cat_name " .
            "FROM " . $GLOBALS['ecs']->table('affiliate_rewards_detail') . " as ard " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON g.goods_id = ard.related_id AND ard.type = 'goods' " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('category') . " as c ON c.cat_id = ard.related_id AND ard.type = 'category' " .
            "WHERE ard.reward_id " . db_create_in($reward_ids) . " " .
            "ORDER BY ard.reward_id, ard.type";
    $details = $GLOBALS['db']->getAll($sql);
    
    foreach ($details as $detail)
    {
		if ($detail['type'] == 'goods')
		{
			$detail['link'] = build_uri('goods', array('gid' => $detail['goods_id'], 'gname' => $detail['goods_name']));
		}
		elseif ($detail['type'] == 'category')
		{
			$detail['link'] = build_uri('category', array('cid' => $detail['cat_id'], 'cname' => $detail['cat_name']));
		}
		
        if (!isset($rewards_list[$detail['reward_id']]['details']))
        {
            $rewards_list[$detail['reward_id']]['details'] = array($detail);
        }
        else
        {
            $rewards_list[$detail['reward_id']]['details'][] = $detail;
        }
    }
	
	$smarty->assign('rewards_list', $rewards_list);

	$sql = "SELECT oi.user_id, oi.order_sn, al.money, al.point, al.time FROM " . $ecs->table('affiliate_log') . " al " .
			"LEFT JOIN " . $ecs->table('order_info') . " oi ON oi.order_id = al.order_id " .
			"WHERE al.user_id = " . $user_id . " ORDER BY al.time DESC LIMIT 30";
	$affLog = $db->getAll($sql);
	foreach($affLog as $key => $log)
	{
		$order_sn = substr($log['order_sn'], 0, strlen($log['order_sn']) - 5) . "***" . substr($log['order_sn'], -2, 2);
		$affLog[$key]['info'] = sprintf($_LANG['user_affiliate_separate_info'], $order_sn, $log['point']);
		$affLog[$key]['date'] = local_date($_CFG['date_format'], $log['time']);
	}

	$smarty->assign('aff_log', $affLog);

	$cfg_config = unserialize($GLOBALS['_CFG']['affiliate']);
	$config = $cfg_config['config'];
	$content = '';
	if($config['level_point_all'] || $config['instant_money_discount'])
	{
		$level_order_num = $config['level_order_num'] > 0 ? sprintf($_LANG['user_affiliate_order_time'], ($config['level_order_num'] > 1 ? $config['level_order_num'] > 0 : '')) : '';
		$level_order_amount = $config['level_order_amount'] > 0 ? sprintf($_LANG['user_affiliate_order_amount'], $config['level_order_amount']) : '';
		$level_point_all = !empty($config['level_point_all']) ? (strpos($config['level_point_all'], '%') === false ? '$' . value_of_integral($config['level_point_all']) : sprintf($_LANG['user_affiliate_whole_order'], $config['level_point_all'])) : '';
		$instant_money_discount = !empty($config['instant_money_discount']) ? (strpos($config['instant_money_discount'], '%') === false ? '$' . $config['instant_money_discount'] : sprintf($_LANG['user_affiliate_whole_order'], $config['instant_money_discount'])) : '';
		$content = sprintf($_LANG['user_affiliate_type_content_0'], $level_order_num, $level_order_amount) . (!empty($instant_money_discount) ? sprintf($_LANG['user_affiliate_type_content_1'], $instant_money_discount) : '') . (!empty($level_point_all) ? sprintf($_LANG['user_affiliate_type_content_2'], $level_point_all) : '');
		if($_CFG['lang'] == 'en_us') {
			$content = (!empty($instant_money_discount) ? sprintf($_LANG['user_affiliate_type_content_0'], $instant_money_discount) : '') .
			sprintf($_LANG['user_affiliate_type_content_1'], $level_order_amount, $level_order_num) .
			(!empty($level_point_all) ? sprintf($_LANG['user_affiliate_type_content_2'], $level_point_all) : '');
		}
	}
	$smarty->assign('user_affiliate_type_intro_content', sprintf($_LANG['user_affiliate_type_intro_content_pre'], $level_point_all) . sprintf($_LANG['user_affiliate_type_intro_content'], $content));
	
	$page	   = !empty($_REQUEST['page'])  && intval($_REQUEST['page'])  > 0 ? intval($_REQUEST['page'])  : 1;
	$size	   = !empty($_CFG['page_size']) && intval($_CFG['page_size']) > 0 ? intval($_CFG['page_size']) : 10;
	
	$sqlcount = "SELECT count(*) " .
				"FROM " . $ecs->table('affiliate_rewards_log') .
				"WHERE `user_id` = '" . $user_id . "' " .
				"AND `reward_amount` > 0 ";
	
	$sql = "SELECT arl.*, oi.order_sn, oi.add_time, g.goods_name " .
			"FROM " . $ecs->table('affiliate_rewards_log') . " as arl " .
			"LEFT JOIN " . $ecs->table('order_info') . " as oi ON oi.order_id = arl.order_id " .
			"LEFT JOIN " . $ecs->table('goods') . " as g ON g.goods_id = arl.goods_id " .
			"WHERE arl.`user_id` = '" . $user_id . "' " .
			"AND arl.`reward_amount` > 0 " .
			"ORDER BY arl.order_id DESC";
	
	$count = $db->getOne($sqlcount);

	$max_page = ($count> 0) ? ceil($count / $size) : 1;
	if ($page > $max_page)
	{
		$page = $max_page;
	}

	$logdb = $db->getAll($sql . " LIMIT 30");
	foreach ($logdb as $key => $rt)
	{
		$rt['order_sn'] = substr($rt['order_sn'], 0, strlen($rt['order_sn']) - 5) . "***" . substr($rt['order_sn'], -2, 2);
		$rt['add_date_formatted'] = local_date($_CFG['date_format'], $rt['add_time']);
		$rt['reward_amount_formatted'] = price_format($rt['reward_amount'], false);
		$rt['goods_url'] = build_uri('goods', array('gid' => $rt['goods_id']));
		
		$logdb[$key] = $rt;
	}

	$url_format = "user.php?act=affiliate&page=";

	$pager = array(
				'page'  => $page,
				'size'  => $size,
				'sort'  => '',
				'order' => '',
				'record_count' => $count,
				'page_count'   => $max_page,
				'page_first'   => $url_format. '1',
				'page_prev'	=> $page > 1 ? $url_format.($page - 1) : "javascript:;",
				'page_next'	=> $page < $max_page ? $url_format.($page + 1) : "javascript:;",
				'page_last'	=> $url_format. $max_page,
				'array'		=> array()
			);
	for ($i = 1; $i <= $max_page; $i++)
	{
		$pager['array'][$i] = $i;
	}

	$smarty->assign('url_format', $url_format);
	$smarty->assign('pager', $pager);

	$smarty->assign('affiliate_intro', $affiliate_intro);
	$smarty->assign('affiliate_type', $affiliate['config']['separate_by']);

	$smarty->assign('logdb', $logdb);

	$smarty->assign('shopname', $_CFG['shop_name']);
	$smarty->assign('userid', $user_id);
	$smarty->assign('shopurl', $ecs->url());
	$smarty->assign('logosrc', 'themes/' . $_CFG['template'] . '/images/logo.gif');

	//$smarty->display('user_clips.dwt');
	$smarty->assign('lbi_file', 'library/user_affiliateyoho.lbi');
	$smarty->display('user_center.dwt');
}

/****************************** Disable unused code start ******************************
// 用户推荐页面
elseif ($action == 'affiliate')
{
	$goodsid = intval(isset($_REQUEST['goodsid']) ? $_REQUEST['goodsid'] : 0);
	if(empty($goodsid))
	{
		//我的推荐页面

		$page	   = !empty($_REQUEST['page'])  && intval($_REQUEST['page'])  > 0 ? intval($_REQUEST['page'])  : 1;
		$size	   = !empty($_CFG['page_size']) && intval($_CFG['page_size']) > 0 ? intval($_CFG['page_size']) : 10;

		empty($affiliate) && $affiliate = array();

		if(empty($affiliate['config']['separate_by']))
		{
			//推荐注册分成
			$affdb = array();
			$num = count($affiliate['item']);
			$up_uid = "'$user_id'";
			$all_uid = "'$user_id'";
			for ($i = 1 ; $i <=$num ;$i++)
			{
				$count = 0;
				if ($up_uid)
				{
					$sql = "SELECT user_id FROM " . $ecs->table('users') . " WHERE parent_id IN($up_uid)";
					$query = $db->query($sql);
					$up_uid = '';
					while ($rt = $db->fetch_array($query))
					{
						$up_uid .= $up_uid ? ",'$rt[user_id]'" : "'$rt[user_id]'";
						if($i < $num)
						{
							$all_uid .= ", '$rt[user_id]'";
						}
						$count++;
					}
				}
				$affdb[$i]['num'] = $count;
				$affdb[$i]['point'] = $affiliate['item'][$i-1]['level_point'];
				$affdb[$i]['money'] = $affiliate['item'][$i-1]['level_money'];
			}
			$smarty->assign('affdb', $affdb);

			$sqlcount = "SELECT count(*) FROM " . $ecs->table('order_info') . " o".
		" LEFT JOIN".$ecs->table('users')." u ON o.user_id = u.user_id".
		" LEFT JOIN " . $ecs->table('affiliate_log') . " a ON o.order_id = a.order_id" .
		" WHERE o.user_id > 0 AND (u.parent_id IN ($all_uid) AND o.is_separate = 0 OR a.user_id = '$user_id' AND o.is_separate > 0)";

			$sql = "SELECT o.*, a.log_id, a.user_id as suid,  a.user_name as auser, a.money, a.point, a.separate_type FROM " . $ecs->table('order_info') . " o".
					" LEFT JOIN".$ecs->table('users')." u ON o.user_id = u.user_id".
					" LEFT JOIN " . $ecs->table('affiliate_log') . " a ON o.order_id = a.order_id" .
		" WHERE o.user_id > 0 AND (u.parent_id IN ($all_uid) AND o.is_separate = 0 OR a.user_id = '$user_id' AND o.is_separate > 0)".
					" ORDER BY order_id DESC" ;

				//
				// SQL解释：
				//
				// 订单、用户、分成记录关联
				// 一个订单可能有多个分成记录
				//
				// 1、订单有效 o.user_id > 0
				// 2、满足以下之一：
				// 	a.直接下线的未分成订单 u.parent_id IN ($all_uid) AND o.is_separate = 0
				// 		其中$all_uid为该ID及其下线(不包含最后一层下线)
				// 	b.全部已分成订单 a.user_id = '$user_id' AND o.is_separate > 0
				//

			$affiliate_intro = nl2br(sprintf($_LANG['affiliate_intro'][$affiliate['config']['separate_by']], $affiliate['config']['expire'], $_LANG['expire_unit'][$affiliate['config']['expire_unit']], $affiliate['config']['level_register_all'], $affiliate['config']['level_register_up'], $affiliate['config']['level_money_all'], $affiliate['config']['level_point_all']));
		}
		else
		{
			//推荐订单分成
			$sqlcount = "SELECT count(*) FROM " . $ecs->table('order_info') . " o".
					" LEFT JOIN".$ecs->table('users')." u ON o.user_id = u.user_id".
					" LEFT JOIN " . $ecs->table('affiliate_log') . " a ON o.order_id = a.order_id" .
					" WHERE o.user_id > 0 AND (o.parent_id = '$user_id' AND o.is_separate = 0 OR a.user_id = '$user_id' AND o.is_separate > 0)";


			$sql = "SELECT o.*, a.log_id,a.user_id as suid, a.user_name as auser, a.money, a.point, a.separate_type,u.parent_id as up FROM " . $ecs->table('order_info') . " o".
					" LEFT JOIN".$ecs->table('users')." u ON o.user_id = u.user_id".
					" LEFT JOIN " . $ecs->table('affiliate_log') . " a ON o.order_id = a.order_id" .
					" WHERE o.user_id > 0 AND (o.parent_id = '$user_id' AND o.is_separate = 0 OR a.user_id = '$user_id' AND o.is_separate > 0)" .
					" ORDER BY order_id DESC" ;

				//
				// SQL解释：
				//
				// 订单、用户、分成记录关联
				// 一个订单可能有多个分成记录
				//
				// 1、订单有效 o.user_id > 0
				// 2、满足以下之一：
				// 	a.订单下线的未分成订单 o.parent_id = '$user_id' AND o.is_separate = 0
				// 	b.全部已分成订单 a.user_id = '$user_id' AND o.is_separate > 0
				//
				//

			$affiliate_intro = nl2br(sprintf($_LANG['affiliate_intro'][$affiliate['config']['separate_by']], $affiliate['config']['expire'], $_LANG['expire_unit'][$affiliate['config']['expire_unit']], $affiliate['config']['level_money_all'], $affiliate['config']['level_point_all']));

		}

		$count = $db->getOne($sqlcount);

		$max_page = ($count> 0) ? ceil($count / $size) : 1;
		if ($page > $max_page)
		{
			$page = $max_page;
		}

		$res = $db->SelectLimit($sql, $size, ($page - 1) * $size);
		$logdb = array();
		while ($rt = $GLOBALS['db']->fetchRow($res))
		{
			if(!empty($rt['suid']))
			{
				//在affiliate_log有记录
				if($rt['separate_type'] == -1 || $rt['separate_type'] == -2)
				{
					//已被撤销
					$rt['is_separate'] = 3;
				}
			}
			$rt['order_sn'] = substr($rt['order_sn'], 0, strlen($rt['order_sn']) - 5) . "***" . substr($rt['order_sn'], -2, 2);
			$logdb[] = $rt;
		}

		$url_format = "user.php?act=affiliate&page=";

		$pager = array(
					'page'  => $page,
					'size'  => $size,
					'sort'  => '',
					'order' => '',
					'record_count' => $count,
					'page_count'   => $max_page,
					'page_first'   => $url_format. '1',
					'page_prev'	=> $page > 1 ? $url_format.($page - 1) : "javascript:;",
					'page_next'	=> $page < $max_page ? $url_format.($page + 1) : "javascript:;",
					'page_last'	=> $url_format. $max_page,
					'array'		=> array()
				);
		for ($i = 1; $i <= $max_page; $i++)
		{
			$pager['array'][$i] = $i;
		}

		$smarty->assign('url_format', $url_format);
		$smarty->assign('pager', $pager);


		$smarty->assign('affiliate_intro', $affiliate_intro);
		$smarty->assign('affiliate_type', $affiliate['config']['separate_by']);

		$smarty->assign('logdb', $logdb);
	}
	else
	{
		//单个商品推荐
		$smarty->assign('userid', $user_id);
		$smarty->assign('goodsid', $goodsid);

		$types = array(1,2,3,4,5);
		$smarty->assign('types', $types);

		$goods = get_goods_info($goodsid);
		$shopurl = $ecs->url();
		$goods['goods_img'] = (strpos($goods['goods_img'], 'http://') === false && strpos($goods['goods_img'], 'https://') === false) ? $shopurl . $goods['goods_img'] : $goods['goods_img'];
		$goods['goods_thumb'] = (strpos($goods['goods_thumb'], 'http://') === false && strpos($goods['goods_thumb'], 'https://') === false) ? $shopurl . $goods['goods_thumb'] : $goods['goods_thumb'];
		$goods['shop_price'] = price_format($goods['shop_price']);

		$smarty->assign('goods', $goods);
	}

	$smarty->assign('shopname', $_CFG['shop_name']);
	$smarty->assign('userid', $user_id);
	$smarty->assign('shopurl', $ecs->url());
	$smarty->assign('logosrc', 'themes/' . $_CFG['template'] . '/images/logo.gif');

	$smarty->display('user_clips.dwt');
}

//首页邮件订阅ajax操做和验证操作
elseif ($action =='email_list')
{
	$job = $_GET['job'];

	if($job == 'add' || $job == 'del')
	{
		if(isset($_SESSION['last_email_query']))
		{
			if(time() - $_SESSION['last_email_query'] <= 30)
			{
				die($_LANG['order_query_toofast']);
			}
		}
		$_SESSION['last_email_query'] = time();
	}

	$email = trim($_GET['email']);
	$email = htmlspecialchars($email);

	if (!is_email($email))
	{
		$info = sprintf($_LANG['email_invalid'], $email);
		die($info);
	}
	$ck = $db->getRow("SELECT * FROM " . $ecs->table('email_list') . " WHERE email = '$email'");
	if ($job == 'add')
	{
		if (empty($ck))
		{
			$hash = substr(md5(time()), 1, 10);
			$sql = "INSERT INTO " . $ecs->table('email_list') . " (email, stat, hash) VALUES ('$email', 0, '$hash')";
			$db->query($sql);
			$info = $_LANG['email_check'];
			$url = $ecs->url() . "user.php?act=email_list&job=add_check&hash=$hash&email=$email";
			send_mail('', $email, $_LANG['check_mail'], sprintf($_LANG['check_mail_content'], $email, $_CFG['shop_name'], $url, $url, $_CFG['shop_name'], local_date('Y-m-d')), 1);
		}
		elseif ($ck['stat'] == 1)
		{
			$info = sprintf($_LANG['email_alreadyin_list'], $email);
		}
		else
		{
			$hash = substr(md5(time()),1 , 10);
			$sql = "UPDATE " . $ecs->table('email_list') . "SET hash = '$hash' WHERE email = '$email'";
			$db->query($sql);
			$info = $_LANG['email_re_check'];
			$url = $ecs->url() . "user.php?act=email_list&job=add_check&hash=$hash&email=$email";
			send_mail('', $email, $_LANG['check_mail'], sprintf($_LANG['check_mail_content'], $email, $_CFG['shop_name'], $url, $url, $_CFG['shop_name'], local_date('Y-m-d')), 1);
		}
		die($info);
	}
	elseif ($job == 'del')
	{
		if (empty($ck))
		{
			$info = sprintf($_LANG['email_notin_list'], $email);
		}
		elseif ($ck['stat'] == 1)
		{
			$hash = substr(md5(time()),1,10);
			$sql = "UPDATE " . $ecs->table('email_list') . "SET hash = '$hash' WHERE email = '$email'";
			$db->query($sql);
			$info = $_LANG['email_check'];
			$url = $ecs->url() . "user.php?act=email_list&job=del_check&hash=$hash&email=$email";
			send_mail('', $email, $_LANG['check_mail'], sprintf($_LANG['check_mail_content'], $email, $_CFG['shop_name'], $url, $url, $_CFG['shop_name'], local_date('Y-m-d')), 1);
		}
		else
		{
			$info = $_LANG['email_not_alive'];
		}
		die($info);
	}
	elseif ($job == 'add_check')
	{
		if (empty($ck))
		{
			$info = sprintf($_LANG['email_notin_list'], $email);
		}
		elseif ($ck['stat'] == 1)
		{
			$info = $_LANG['email_checked'];
		}
		else
		{
			if ($_GET['hash'] == $ck['hash'])
			{
				$sql = "UPDATE " . $ecs->table('email_list') . "SET stat = 1 WHERE email = '$email'";
				$db->query($sql);
				$info = $_LANG['email_checked'];
			}
			else
			{
				$info = $_LANG['hash_wrong'];
			}
		}
		show_message($info, _L('global_go_home', '返回首頁'), 'index.php');
	}
	elseif ($job == 'del_check')
	{
		if (empty($ck))
		{
			$info = sprintf($_LANG['email_invalid'], $email);
		}
		elseif ($ck['stat'] == 1)
		{
			if ($_GET['hash'] == $ck['hash'])
			{
				$sql = "DELETE FROM " . $ecs->table('email_list') . "WHERE email = '$email'";
				$db->query($sql);
				$info = $_LANG['email_canceled'];
			}
			else
			{
				$info = $_LANG['hash_wrong'];
			}
		}
		else
		{
			$info = $_LANG['email_not_alive'];
		}
		show_message($info, _L('global_go_home', '返回首頁'), 'index.php');
	}
}
****************************** Disable unused code end ******************************/

/* ajax 发送验证邮件 */
elseif ($action == 'send_hash_mail')
{
	include_once(ROOT_PATH .'includes/cls_json.php');
	include_once(ROOT_PATH .'includes/lib_passport.php');
	$json = new JSON();

	$result = array('error' => 0, 'message' => '', 'content' => '');

	if ($user_id == 0)
	{
		/* 用户没有登录 */
		$result['error']   = 1;
		$result['message'] = _L('user_email_validate_send_error_not_logged_in', '請先登入');
		die($json->encode($result));
	}

	if (send_regiter_hash_async($user_id))
	{
		$result['message'] = _L('user_email_validate_send_success', '驗證郵件發送成功');
		die($json->encode($result));
	}
	else
	{
		$result['error'] = 1;
		$result['message'] = $GLOBALS['err']->last_message();
	}

	die($json->encode($result));
}

/****************************** Disable unused code ******************************
else if ($action == 'track_packages')
{
	include_once(ROOT_PATH . 'includes/lib_transaction.php');
	include_once(ROOT_PATH .'includes/lib_order.php');

	$page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;

	$orders = array();

	$sql = "SELECT order_id,order_sn,invoice_no,shipping_id FROM " .$ecs->table('order_info').
			" WHERE user_id = '$user_id' AND shipping_status = '" . SS_SHIPPED . "'";
	$res = $db->query($sql);
	$record_count = 0;
	while ($item = $db->fetch_array($res))
	{
		$shipping   = get_shipping_object($item['shipping_id']);

		if (method_exists ($shipping, 'query'))
		{
			$query_link = $shipping->query($item['invoice_no']);
		}
		else
		{
			$query_link = $item['invoice_no'];
		}

		if ($query_link != $item['invoice_no'])
		{
			$item['query_link'] = $query_link;
			$orders[]  = $item;
			$record_count += 1;
		}
	}
	$pager  = get_pager('user.php', array('act' => $action), $record_count, $page);
	$smarty->assign('pager',  $pager);
	$smarty->assign('orders', $orders);
	$smarty->display('user_transaction.dwt');
}
else if ($action == 'order_query')
{
	$_GET['order_sn'] = trim(substr($_GET['order_sn'], 1));
	$order_sn = empty($_GET['order_sn']) ? '' : addslashes($_GET['order_sn']);
	include_once(ROOT_PATH .'includes/cls_json.php');
	$json = new JSON();

	$result = array('error'=>0, 'message'=>'', 'content'=>'');

	if(isset($_SESSION['last_order_query']))
	{
		if(time() - $_SESSION['last_order_query'] <= 10)
		{
			$result['error'] = 1;
			$result['message'] = $_LANG['order_query_toofast'];
			die($json->encode($result));
		}
	}
	$_SESSION['last_order_query'] = time();

	if (empty($order_sn))
	{
		$result['error'] = 1;
		$result['message'] = $_LANG['invalid_order_sn'];
		die($json->encode($result));
	}

	$sql = "SELECT order_id, order_status, shipping_status, pay_status, ".
		   " shipping_time, shipping_id, invoice_no, user_id ".
		   " FROM " . $ecs->table('order_info').
		   " WHERE order_sn = '$order_sn' LIMIT 1";

	$row = $db->getRow($sql);
	if (empty($row))
	{
		$result['error'] = 1;
		$result['message'] = $_LANG['invalid_order_sn'];
		die($json->encode($result));
	}

	$order_query = array();
	$order_query['order_sn'] = $order_sn;
	$order_query['order_id'] = $row['order_id'];
	$order_query['order_status'] = $_LANG['os'][$row['order_status']] . ',' . $_LANG['ps'][$row['pay_status']] . ',' . $_LANG['ss'][$row['shipping_status']];

	if ($row['invoice_no'] && $row['shipping_id'] > 0)
	{
		$sql = "SELECT shipping_code FROM " . $ecs->table('shipping') . " WHERE shipping_id = '$row[shipping_id]'";
		$shipping_code = $db->getOne($sql);
		$plugin = ROOT_PATH . 'includes/modules/shipping/' . $shipping_code . '.php';
		if (file_exists($plugin))
		{
			include_once($plugin);
			$shipping = new $shipping_code;
			$order_query['invoice_no'] = $shipping->query((string)$row['invoice_no']);
		}
		else
		{
			$order_query['invoice_no'] = (string)$row['invoice_no'];
		}
	}

	$order_query['user_id'] = $row['user_id'];
	// 如果是匿名用户显示发货时间
	if ($row['user_id'] == 0 && $row['shipping_time'] > 0)
	{
		$order_query['shipping_date'] = local_date($GLOBALS['_CFG']['date_format'], $row['shipping_time']);
	}
	$smarty->assign('order_query',	$order_query);
	$result['content'] = $smarty->fetch('library/order_query.lbi');
	die($json->encode($result));
}
****************************** Disable unused code end ******************************/

elseif ($action == 'points')
{
	//Blocking Wholesale Customer
	if(in_array($user_id,$userController->getWholesaleUserIds()))
		show_message('批發客戶請直接聯絡營業員訂購');

	$sql = "SELECT user_id, user_name, pay_points, rank_points FROM " . $ecs->table('users')  . " WHERE user_id='$user_id'";
	$row = $db->getRow($sql);
	$smarty->assign('shop_points', $row);
	$smarty->assign('action',	$action);
	$smarty->assign('lbi_file', 'library/user_points.lbi');
	$smarty->display('user_center.dwt');
}
elseif ($action == 'transform_points')
{
	header('Location: /user.php?act=points');
	exit;
	
	// $rule = array();
	// if (!empty($_CFG['points_rule']))
	// {
	// 	$rule = unserialize($_CFG['points_rule']);
	// }
	// $cfg = array();
	// if (!empty($_CFG['integrate_config']))
	// {
	// 	$cfg = unserialize($_CFG['integrate_config']);
	// 	$_LANG['exchange_points'][0] = empty($cfg['uc_lang']['credits'][0][0])? $_LANG['exchange_points'][0] : $cfg['uc_lang']['credits'][0][0];
	// 	$_LANG['exchange_points'][1] = empty($cfg['uc_lang']['credits'][1][0])? $_LANG['exchange_points'][1] : $cfg['uc_lang']['credits'][1][0];
	// }
	// $sql = "SELECT user_id, user_name, pay_points, rank_points FROM " . $ecs->table('users')  . " WHERE user_id='$user_id'";
	// $row = $db->getRow($sql);
	// if ($_CFG['integrate_code'] == 'ucenter')
	// {
	// 	$exchange_type = 'ucenter';
	// 	$to_credits_options = array();
	// 	$out_exchange_allow = array();
	// 	foreach ($rule as $credit)
	// 	{
	// 		$out_exchange_allow[$credit['appiddesc'] . '|' . $credit['creditdesc'] . '|' . $credit['creditsrc']] = $credit['ratio'];
	// 		if (!array_key_exists($credit['appiddesc']. '|' .$credit['creditdesc'], $to_credits_options))
	// 		{
	// 			$to_credits_options[$credit['appiddesc']. '|' .$credit['creditdesc']] = $credit['title'];
	// 		}
	// 	}
	// 	$smarty->assign('selected_org', $rule[0]['creditsrc']);
	// 	$smarty->assign('selected_dst', $rule[0]['appiddesc']. '|' .$rule[0]['creditdesc']);
	// 	$smarty->assign('descreditunit', $rule[0]['unit']);
	// 	$smarty->assign('orgcredittitle', $_LANG['exchange_points'][$rule[0]['creditsrc']]);
	// 	$smarty->assign('descredittitle', $rule[0]['title']);
	// 	$smarty->assign('descreditamount', round((1 / $rule[0]['ratio']), 2));
	// 	$smarty->assign('to_credits_options', $to_credits_options);
	// 	$smarty->assign('out_exchange_allow', $out_exchange_allow);
	// }
	// else
	// {
	// 	$exchange_type = 'other';
	// 
	// 	$bbs_points_name = $user->get_points_name();
	// 	$total_bbs_points = $user->get_points($row['user_name']);
	// 
	// 	/* 论坛积分 */
	// 	$bbs_points = array();
	// 	foreach ($bbs_points_name as $key=>$val)
	// 	{
	// 		$bbs_points[$key] = array('title'=>$_LANG['bbs'] . $val['title'], 'value'=>$total_bbs_points[$key]);
	// 	}
	// 
	// 	/* 兑换规则 */
	// 	$rule_list = array();
	// 	foreach ($rule as $key=>$val)
	// 	{
	// 		$rule_key = substr($key, 0, 1);
	// 		$bbs_key = substr($key, 1);
	// 		$rule_list[$key]['rate'] = $val;
	// 		switch ($rule_key)
	// 		{
	// 			case TO_P :
	// 				$rule_list[$key]['from'] = $_LANG['bbs'] . $bbs_points_name[$bbs_key]['title'];
	// 				$rule_list[$key]['to'] = $_LANG['pay_points'];
	// 				break;
	// 			case TO_R :
	// 				$rule_list[$key]['from'] = $_LANG['bbs'] . $bbs_points_name[$bbs_key]['title'];
	// 				$rule_list[$key]['to'] = $_LANG['rank_points'];
	// 				break;
	// 			case FROM_P :
	// 				$rule_list[$key]['from'] = $_LANG['pay_points'];$_LANG['bbs'] . $bbs_points_name[$bbs_key]['title'];
	// 				$rule_list[$key]['to'] =$_LANG['bbs'] . $bbs_points_name[$bbs_key]['title'];
	// 				break;
	// 			case FROM_R :
	// 				$rule_list[$key]['from'] = $_LANG['rank_points'];
	// 				$rule_list[$key]['to'] = $_LANG['bbs'] . $bbs_points_name[$bbs_key]['title'];
	// 				break;
	// 		}
	// 	}
	// 	$smarty->assign('bbs_points', $bbs_points);
	// 	$smarty->assign('rule_list',  $rule_list);
	// }
	// $smarty->assign('shop_points', $row);
	// $smarty->assign('exchange_type',	 $exchange_type);
	// $smarty->assign('action',	 $action);
	// $smarty->assign('lang',	   $_LANG);
	// //$smarty->display('user_transaction.dwt');
	// $smarty->assign('lbi_file', 'library/user_transform_points.lbi');
	// $smarty->display('user_center.dwt');
} elseif ($action == 'programs') {
    if (!$_REQUEST['program']) {
        header('Location: user.php');
        exit;
    }

    // 特殊會員
    $now = local_strtotime(local_date('Y-m-d H:i:s'));
    $sql = "SELECT urp.program_id, urp.program_code, IFNULL(urpl.program_name, urp.program_name) as program_name, IFNULL(urpl.description, urp.description) as description, urp.theme_color, urp.status, urp.verifiy_type, urp.special_code, urp.require_proof  " .
        " FROM " . $ecs->table("user_rank_program") . " as urp " .
        "LEFT JOIN " . $ecs->table('user_rank_program_lang') . " as urpl ON urpl.program_id = urp.program_id AND urpl.lang = '" . $_CFG['lang'] . "' " .
        " WHERE urp.program_code = '" . $_REQUEST['program'] . "' ";
    $program = $db->getRow($sql);
    $program_id = $program['program_id'];

    $record = $db->getRow("SELECT * FROM " . $ecs->table("user_rank_program_application") . " WHERE program_id = $program_id AND user_id = $_SESSION[user_id]");
	if (!empty($record)) {
		$stage = $record['stage'];
		$now = strtotime(local_date('Y-m-d H:i:s'));
		$expiry = strtotime($record['expiry']);
		if ($expiry < $now) {
			$stage = RANK_APPLICATION_FURTHER;
			if ($program['status'] == RANK_PROGRAM_END) {
				$stage = 5;
			}
		}
	} else {
		$stage = -1;
		if(!in_array($program['status'], [RANK_PROGRAM_HIDE, RANK_PROGRAM_SHOW])) {
			header('Location: user.php');
			exit;
		}
	}
	$record['expiry'] = date("Y-m-d", strtotime($record['expiry']));
    $smarty->assign('program', $program);
    $smarty->assign('stage', $stage);
    $smarty->assign('record', $record);
    $smarty->assign('program_code', $_REQUEST['program']);
    $smarty->assign('lbi_file', 'library/user_rank_program.lbi');
    $smarty->display('user_center.dwt');
} elseif ($action == 'validate_program') { /* 验证特殊會員 */
    $verify = empty($_REQUEST['verify']) ? '' : trim($_REQUEST['verify']);
    if ($verify) {
        $success = $userRankController->validateProgram($_REQUEST);
        if ($success['error'] == 0) {
            $program = $success['program'];
            $msg = sprintf(_L('user_email_validate_success', '%s 您好，您的電郵地址 %s 已通過驗證%s'),
                '', $success['email'],
                $incomplete ? _L('user_email_validate_complete_profile', '，補填個人資料，送積分哦！') : '');
            show_message($msg, $program['program_name'], '/programs/' . $program['program_code']);
        }
    }
    show_message(_L('user_email_validate_failed', '驗證失敗，請確認你的驗證連結是否正確'));
}
/****************************** Disable "Transform Points" function ******************************
elseif ($action == 'act_transform_points')
{
	// YOHO: Transform Points function is not used
	header('Location: user.php?act=transform_points');
	exit;
	
	$rule_index = empty($_POST['rule_index']) ? '' : trim($_POST['rule_index']);
	$num = empty($_POST['num']) ? 0 : intval($_POST['num']);


	if ($num <= 0 || $num != floor($num))
	{
		show_message($_LANG['invalid_points'], $_LANG['transform_points'], 'user.php?act=transform_points');
	}

	$num = floor($num); //格式化为整数

	$bbs_key = substr($rule_index, 1);
	$rule_key = substr($rule_index, 0, 1);

	$max_num = 0;

	// 取出用户数据
	$sql = "SELECT user_name, user_id, pay_points, rank_points FROM " . $ecs->table('users') . " WHERE user_id='$user_id'";
	$row = $db->getRow($sql);
	$bbs_points = $user->get_points($row['user_name']);
	$points_name = $user->get_points_name();

	$rule = array();
	if ($_CFG['points_rule'])
	{
		$rule = unserialize($_CFG['points_rule']);
	}
	list($from, $to) = explode(':', $rule[$rule_index]);

	$max_points = 0;
	switch ($rule_key)
	{
		case TO_P :
			$max_points = $bbs_points[$bbs_key];
			break;
		case TO_R :
			$max_points = $bbs_points[$bbs_key];
			break;
		case FROM_P :
			$max_points = $row['pay_points'];
			break;
		case FROM_R :
			$max_points = $row['rank_points'];
	}

	// 检查积分是否超过最大值
	if ($max_points <=0 || $num > $max_points)
	{
		show_message($_LANG['overflow_points'], $_LANG['transform_points'], 'user.php?act=transform_points' );
	}

	switch ($rule_key)
	{
		case TO_P :
			$result_points = floor($num * $to / $from);
			$user->set_points($row['user_name'], array($bbs_key=>0 - $num)); //调整论坛积分
			log_account_change($row['user_id'], 0, 0, 0, $result_points, $_LANG['transform_points'], ACT_OTHER);
			show_message(sprintf($_LANG['to_pay_points'],  $num, $points_name[$bbs_key]['title'], $result_points), $_LANG['transform_points'], 'user.php?act=transform_points');

		case TO_R :
			$result_points = floor($num * $to / $from);
			$user->set_points($row['user_name'], array($bbs_key=>0 - $num)); //调整论坛积分
			log_account_change($row['user_id'], 0, 0, $result_points, 0, $_LANG['transform_points'], ACT_OTHER);
			show_message(sprintf($_LANG['to_rank_points'], $num, $points_name[$bbs_key]['title'], $result_points), $_LANG['transform_points'], 'user.php?act=transform_points');

		case FROM_P :
			$result_points = floor($num * $to / $from);
			log_account_change($row['user_id'], 0, 0, 0, 0-$num, $_LANG['transform_points'], ACT_OTHER); //调整商城积分
			$user->set_points($row['user_name'], array($bbs_key=>$result_points)); //调整论坛积分
			show_message(sprintf($_LANG['from_pay_points'], $num, $result_points,  $points_name[$bbs_key]['title']), $_LANG['transform_points'], 'user.php?act=transform_points');

		case FROM_R :
			$result_points = floor($num * $to / $from);
			log_account_change($row['user_id'], 0, 0, 0-$num, 0, $_LANG['transform_points'], ACT_OTHER); //调整商城积分
			$user->set_points($row['user_name'], array($bbs_key=>$result_points)); //调整论坛积分
			show_message(sprintf($_LANG['from_rank_points'], $num, $result_points, $points_name[$bbs_key]['title']), $_LANG['transform_points'], 'user.php?act=transform_points');
	}
}
elseif ($action == 'act_transform_ucenter_points')
{
	$rule = array();
	if ($_CFG['points_rule'])
	{
		$rule = unserialize($_CFG['points_rule']);
	}
	$shop_points = array(0 => 'rank_points', 1 => 'pay_points');
	$sql = "SELECT user_id, user_name, pay_points, rank_points FROM " . $ecs->table('users')  . " WHERE user_id='$user_id'";
	$row = $db->getRow($sql);
	$exchange_amount = intval($_POST['amount']);
	$fromcredits = intval($_POST['fromcredits']);
	$tocredits = trim($_POST['tocredits']);
	$cfg = unserialize($_CFG['integrate_config']);
	if (!empty($cfg))
	{
		$_LANG['exchange_points'][0] = empty($cfg['uc_lang']['credits'][0][0])? $_LANG['exchange_points'][0] : $cfg['uc_lang']['credits'][0][0];
		$_LANG['exchange_points'][1] = empty($cfg['uc_lang']['credits'][1][0])? $_LANG['exchange_points'][1] : $cfg['uc_lang']['credits'][1][0];
	}
	list($appiddesc, $creditdesc) = explode('|', $tocredits);
	$ratio = 0;

	if ($exchange_amount <= 0)
	{
		show_message($_LANG['invalid_points'], $_LANG['transform_points'], 'user.php?act=transform_points');
	}
	if ($exchange_amount > $row[$shop_points[$fromcredits]])
	{
		show_message($_LANG['overflow_points'], $_LANG['transform_points'], 'user.php?act=transform_points');
	}
	foreach ($rule as $credit)
	{
		if ($credit['appiddesc'] == $appiddesc && $credit['creditdesc'] == $creditdesc && $credit['creditsrc'] == $fromcredits)
		{
			$ratio = $credit['ratio'];
			break;
		}
	}
	if ($ratio == 0)
	{
		show_message($_LANG['exchange_deny'], $_LANG['transform_points'], 'user.php?act=transform_points');
	}
	$netamount = floor($exchange_amount / $ratio);
	include_once(ROOT_PATH . './includes/lib_uc.php');
	$result = exchange_points($row['user_id'], $fromcredits, $creditdesc, $appiddesc, $netamount);
	if ($result === true)
	{
		$sql = "UPDATE " . $ecs->table('users') . " SET {$shop_points[$fromcredits]}={$shop_points[$fromcredits]}-'$exchange_amount' WHERE user_id='{$row['user_id']}'";
		$db->query($sql);
		$sql = "INSERT INTO " . $ecs->table('account_log') . "(user_id, {$shop_points[$fromcredits]}, change_time, change_desc, change_type)" . " VALUES ('{$row['user_id']}', '-$exchange_amount', '". gmtime() ."', '" . $cfg['uc_lang']['exchange'] . "', '98')";
		$db->query($sql);
		show_message(sprintf($_LANG['exchange_success'], $exchange_amount, $_LANG['exchange_points'][$fromcredits], $netamount, $credit['title']), $_LANG['transform_points'], 'user.php?act=transform_points');
	}
	else
	{
		show_message($_LANG['exchange_error_1'], $_LANG['transform_points'], 'user.php?act=transform_points');
	}
}
****************************** "Transform Points" function end ******************************/
 
/* 清除商品浏览历史 */
elseif ($action == 'clear_history')
{
	setcookie('ECS[history]',   '', 1);
}

/* 上傳用戶頭像 */
elseif ($action == 'act_upload_avatar')
{
	$save_path = ROOT_PATH . 'uploads/avatars/';
	$save_path .= $_SESSION['user_id'];
	
	if (!isset($_FILES['avatar_image']))
	{
		show_message(_L('user_avatar_upload_error_empty', '若要更改頭像，請上傳圖片。'));
	}
	
	$error = $_FILES['avatar_image']['error'];
	if (($error == UPLOAD_ERR_PARTIAL) || ($error == UPLOAD_ERR_NO_FILE))
	{
		show_message(_L('user_avatar_upload_error_empty', '若要更改頭像，請上傳圖片。'));
	}
	else if (($error == UPLOAD_ERR_INI_SIZE) || ($error == UPLOAD_ERR_FORM_SIZE))
	{
		show_message(_L('user_avatar_upload_error_filesize', '圖片過大，請先把圖片縮小至 1 MB 以內。'));
	}
	else if ($error != UPLOAD_ERR_OK)
	{
		show_message(_L('user_avatar_upload_error_other', '圖片上傳錯誤，請稍後再試。'));
	}
	
	$tmp_name = $_FILES['avatar_image']['tmp_name'];
	
	$size = getimagesize($tmp_name);
	
	$width = $size[0];
	$height = $size[1];
	$type = $size['mime'];
	
	if ($width > 2048 || $height > 2048)
	{
		show_message(_L('user_avatar_upload_error_resolution', '圖片尺寸過大，請先把圖片縮小至 512x512 以內。'));
	}
	
	$animated_gif_fix = '';
	if ($type == 'image/jpeg')
	{
		$tmp_path = $save_path . '_tmp.jpg';
	}
	else if ($type == 'image/gif')
	{
		$tmp_path = $save_path . '_tmp.gif';
		$animated_gif_fix = '[0]'; // Get first frame only
	}
	else if ($type == 'image/png')
	{
		$tmp_path = $save_path . '_tmp.png';
	}
	else
	{
		show_message(_L('user_avatar_upload_error_image_format', '圖片格式錯誤，請確認圖片格式是 jpeg 、 gif 或 png。'));
	}
	
	if (!move_uploaded_file($tmp_name, $tmp_path))
	{
		show_message(_L('user_avatar_upload_error_other', '圖片上傳錯誤，請稍後再試。'));
	}
	
	exec('/usr/bin/convert ' . escapeshellarg($tmp_path . $animated_gif_fix) . ' -auto-orient -resize 200x200^ -gravity center -extent 200x200 ' . escapeshellarg($save_path . '.png'));
	exec('/usr/bin/convert ' . escapeshellarg($save_path . '.png') . ' -resize 100x100 ' . escapeshellarg($save_path . '_100x100.png'));
	exec('/usr/bin/convert ' . escapeshellarg($save_path . '.png') . ' -resize 60x60 ' . escapeshellarg($save_path . '_60x60.png'));
	
	unlink($tmp_path);
	
	show_message(_L('user_avatar_upload_success', '上傳頭像成功，更新頭像可能需要數分鐘時間。'), _L('user_my_profile', '個人資料'), '/user.php?act=profile', 'info');
}

/* Redirect function */
elseif ($action == 'redirect')
{
	$url = empty($_REQUEST['direct']) ? '' : $_REQUEST['direct'];

	if($_SESSION['user_id'] > 0){
		ecs_header("Location: /$url");
		exit();
	} else {
		ecs_header("Location: /user.php?act=login");
		exit();
	}

}

elseif ($action == "load_tracking_info")
{
	include_once(ROOT_PATH .'includes/cls_json.php');
	include_once(ROOT_PATH .'includes/lib_passport.php');
	$json = new JSON();

	$result = array('error' => 0, 'message' => '', 'content' => '');
	$res = $orderController->get_order_tracking_info($_REQUEST['order_sn']);
	$result['message'] = $res;
	die($json->encode($res));
}

function get_user_need_profile_count($arr)
{
	$count = 0;
	
	$count += (validate_date($arr['birthday'])) ? 1 : 0;
	$count += !empty($arr['sex']) ? 1 : 0; // 0 = 保密, 1 = 男, 2 = 女
	$count += !empty($arr['email']) ? 1 : 0;
	$count += !empty($_POST['extend_field7']) ? 1 : 0;
	$count += !empty($_POST['extend_field8']) ? 1 : 0;
	$count += !empty($_POST['extend_field9']) ? 1 : 0;
	$count += !empty($_POST['extend_field10']) ? 1 : 0;
	$count += !empty($_POST['extend_field11']) ? 1 : 0;
	$count += !empty($_POST['extend_field12']) ? 1 : 0;
	
	return $count;
}

function validate_date($date)
{
	$d = DateTime::createFromFormat('Y-m-d', $date);
	return $d && $d->format('Y-m-d') == $date && intval($d->format('Y')) != 0;
}

function assign_user_page_title($title_suffix = '')
{
	global $action, $_LANG, $smarty;
	
	$title = '';
	if ($action == 'default')
	{
		$title = _L('user_dashboard', '帳戶概覽');
	}
	elseif ($action == 'profile')
	{
		$title = _L('user_my_profile', '個人資料');
	}
	elseif ($action == 'order_list')
	{
		$title = _L('user_my_orders', '我的訂單');
	}
	elseif ($action == 'order_detail')
	{
		$title = _L('user_order_detail', '訂單詳情');
	}
	elseif ($action == 'transform_points' || $action == 'points')
	{
		$title = _L('user_my_points', '我的積分');
	}
	elseif ($action == 'affiliate')
	{
		$title = _L('user_affiliate', '推廣大使');
	}
	elseif ($action == 'affiliate_type')
	{
		$title = _L('user_affiliate_type', '推廣大使');
	}
	/***** These pages are not used *****
	elseif ($action == 'address_list')
	{
		$title = $_LANG['label_address'];
	}
	elseif ($action == 'collection_list')
	{
		$title = $_LANG['label_collection'];
	}
	elseif ($action == 'message_list')
	{
		$title = $_LANG['label_message'];
	}
	elseif ($action == 'tag_list')
	{
		$title = $_LANG['label_tag'];
	}
	elseif ($action == 'booking_list')
	{
		$title = $_LANG['label_booking'];
	}
	elseif ($action == 'add_booking')
	{
		$title = $_LANG['label_booking'];
	}
	elseif ($action == 'bonus')
	{
		$title = $_LANG['label_bonus'];
	}
	elseif ($action == 'comment_list')
	{
		$title = $_LANG['label_comment'];
	}
	elseif ($action == 'track_packages')
	{
		$title = $_LANG['label_track_packages'];
	}
	elseif (in_array($action, array('account_log', 'account_deposit', 'account_raply', 'account_detail', 'act_account', 'pay')))
	{
		$title = $_LANG['label_user_surplus'];
	}
	***** Unused pages end *****/
	
	$title = (!empty($title) ? $title . ' - ' : '') . $title_suffix;
	$smarty->assign('page_title', $title);
}

?>
