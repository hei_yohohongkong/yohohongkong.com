<?php

/***
 * pos.php
 * by howang 2014-06-05
 *
 * POS System for YOHO Hong Kong
 ***/

define('IN_ECS', true);

define('DESKTOP_ONLY', true); // This page don't have mobile version

define('FORCE_HTTPS', true); // Require HTTPS for security

define('IS_POS', true);

require(dirname(__FILE__) . '/includes/init.php');

require(ROOT_PATH . 'includes/lib_order.php');
require(ROOT_PATH . 'includes/lib_order_1.php');

/* 载入语言文件 */
require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/user.php');
require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/shopping_flow.php');

$paymentController = new Yoho\cms\Controller\PaymentController();
$orderController = new Yoho\cms\Controller\OrderController();
$rspController = new Yoho\cms\Controller\RspController();
$order_fee_platform = $orderController::ORDER_FEE_PLATFORM_POS;
$actg = $paymentController->actg;
$actgHooks = $paymentController->actgHooks;

$smarty->assign('lang',	$_LANG);

if ((DEBUG_MODE & 2) != 2)
{
	$smarty->caching = true;
}

// Check admin login
$admin = check_admin_login();
if (!$admin)
{
	if (!empty($_REQUEST['from_app']))
	{
		setcookie('login_from_pos', '1', 0, '/', false, true, true);
		header('Location: /x/privilege.php?act=login');
		exit;
	}
	
	show_message($_LANG['not_login'], $_LANG['back_home'], $ecs->url(), 'error');
	exit;
}
$smarty->assign('admin', $admin);

$action = empty($_REQUEST['act']) ? '' : $_REQUEST['act'];
// Add Goods to Cart
if ($action == 'add_to_cart')
{
	/* 取得購物類型 */
	$flow_type = isset($_SESSION['flow_type']) ? intval($_SESSION['flow_type']) : CART_GENERAL_GOODS;
	$smarty->assign('config', $_CFG);
	// $user_info = user_info($_SESSION['user_id']);
	$user_rank = empty($_REQUEST['user_rank']) ? 0 : intval($_REQUEST['user_rank']);
	$shipping = empty($_REQUEST['shipping']) ? 0 : floatval($_REQUEST['shipping']);
	$payment = empty($_REQUEST['payment']) ? 0 : floatval($_REQUEST['payment']);
	$is_gift = empty($_REQUEST['is_gift']) ? 0 : intval($_REQUEST['is_gift']);
	$is_package = empty($_REQUEST['is_package']) ? 0 : intval($_REQUEST['is_package']);
	$clean_integral = isset($_REQUEST['clean_integral']) ? intval($_REQUEST['clean_integral']) : 1;
	
	$goods_id = empty($_REQUEST['goods_id']) ? 0 : $_REQUEST['goods_id']; /*商品ID*/
	$goods_number = isset($_REQUEST['quantity']) ? intval($_REQUEST['quantity']) : 0;
	$spec = array();

	//ungroup package not available
	$orderController->auto_ungroup_package($order_fee_platform);
	
	// Auto group product to package
	$orderController->auto_group_package($order_fee_platform);
	
	/* delete all Favourable / Package product which not available */
	$orderController->check_cart_favourable_goods($user_rank, $_SESSION['user_id'], $order_fee_platform);


	if(!empty($goods_id))
	{
		/* 檢查：商品數量是否合法 */
		if (!is_numeric($goods_number))
		{
			output_json(array(
				'status' => 0,
				'goods_id' => $goods_id,
				'goods_number' => $goods_number,
				'error' => $_LANG['invalid_number']
			));
		}
		/* 更新：購物車 */
		else
		{
			if ($goods_number > 0) // This step only handle normal goods, not include gift goods.
			{
				/* 取得商品信息 */
				$sql = "SELECT g.goods_id, g.goods_name, g.goods_sn, g.give_integral, g.is_on_sale, g.is_real,g.integral, ".
					"g.market_price, g.shop_price AS org_price, g.promote_price, g.promote_start_date, ".
					"g.promote_end_date, g.goods_weight, g.integral, g.extension_code, ".
					"g.goods_number, g.is_alone_sale, g.is_shipping,".
					"IFNULL(mp.user_price, g.shop_price * '$_SESSION[discount]') AS shop_price ".
				" FROM " .$GLOBALS['ecs']->table('goods'). " AS g ".
				" LEFT JOIN " . $GLOBALS['ecs']->table('member_price') . " AS mp ".
						"ON mp.goods_id = g.goods_id AND mp.user_rank = '$user_rank' ".
				" WHERE g.goods_id = '$goods_id'" .
				" AND g.is_delete = 0";
				
				$goods = $GLOBALS['db']->getRow($sql);
				
				/* 計算商品的促銷價格 */
				$spec_price             = spec_price($spec);
				$final_price     = get_final_price($goods_id, $goods_number, true, $spec, $user_rank);
                $goods_price     = $final_price['final_price'];
                $goods_user_rank = $final_price['user_rank_id'];
				$goods['market_price'] += $spec_price;
				$goods_attr             = get_goods_attr_info($spec);
				$goods_attr_id          = join(',', $spec);
				
				$goods['user_id']       = $_SESSION['user_id'];
				$goods['goods_price']   = $goods_price;
				$goods['goods_attr']    = $goods_attr;
				$goods['parent_id']     = 0;
				$goods['is_gift']       = 0;
				$goods['is_package']    = 0;
				$goods['is_shipping']   = 0;
				$goods['goods_number']  = $goods_number;
				$goods['subtotal']      = $goods_price * $goods_number;
				$goods['session_id']    = SESS_ID;
				$goods['user_rank']     = $goods_user_rank;
				//echo "1";die("DELETE FROM ".$GLOBALS['ecs']->table('cart1'). " WHERE goods_id=$goods_id AND is_gift=0 AND is_package=$is_package AND session_id='".SESS_ID."'");
				$GLOBALS['db']->query("DELETE FROM ".$GLOBALS['ecs']->table('cart1'). " WHERE goods_id=$goods_id AND is_gift=0 AND is_package=$is_package AND session_id='".SESS_ID."'");
				$GLOBALS['db']->autoExecute($GLOBALS['ecs']->table('cart1'), addslashes_deep($goods), 'INSERT');
				/* 保存購物信息 */
			}
			else
			{
				// goods_number = 0: remove from cart
				//echo "2";die("DELETE FROM ".$GLOBALS['ecs']->table('cart1'). " WHERE goods_id=$goods_id AND is_gift=$is_gift AND is_package=$is_package AND session_id='".SESS_ID."'");
				$GLOBALS['db']->query("DELETE FROM ".$GLOBALS['ecs']->table('cart1'). " WHERE goods_id=$goods_id AND is_gift=$is_gift AND is_package=$is_package AND session_id='".SESS_ID."'" );
				if($is_package){
					$GLOBALS['db']->query("DELETE FROM ".$GLOBALS['ecs']->table('cart1'). " WHERE is_package=$is_package AND session_id='".SESS_ID."'" );
				}
			}
		}
	}
	// Update order type
    $order_type = isset($_REQUEST['order_mode']) ? $_REQUEST['order_mode'] : 'normal';
	if($order_type == 'sales-agent' || $order_type == 'wholesale') { 
		/* 把赠品删除 */
		$sql = "DELETE FROM " . $GLOBALS['ecs']->table('cart1') . " WHERE session_id = '" . SESS_ID . "' AND is_gift <> 0";
		$GLOBALS['db']->query($sql);
	}

	// Auto group product to package
	$orderController->auto_group_package($order_fee_platform);
	
	$cart_goods = get_cart_goods_1($user_rank);
	usort($cart_goods['goods_list'], function ($a, $b) { return intval($a['rec_id']) - intval($b['rec_id']); });
	$goods_list = array();
	$in_cart_goods_ids = [];
    foreach ($cart_goods['goods_list'] as $goods) {
         $in_cart_goods_ids[] = $goods['goods_id'];
    }
	foreach ($cart_goods['goods_list'] as $goods)
	{
		$goods_list[] = array(
			'goods_id' => $goods['goods_id'],
			'goods_name' => $goods['goods_name'],
			'goods_number' => $goods['goods_number'],
			'goods_price' => $goods['goods_price'],
			'goods_img' => $goods['goods_thumb'],
			'subtotal' => $goods['subtotal'],
			'is_vip' => $goods['is_vip'],
			'is_gift' => $goods['is_gift'],
			'is_package' => $goods['is_package'],
			'is_auto_add' => $goods['is_auto_add'],
			'has_require_goods' => empty($goods['require_goods_id']) || in_array($goods['require_goods_id'], $in_cart_goods_ids) ? 0 : 1
		);
	}
	
	/*
	 * 取得訂單信息
	 */
	$order = flow_order_info();
	
	// pos will trigger add_to_cart with goods_id = 0 and goods_number = 0 on page load
	// If integral is set but didn't checkout, we have to clear it so it won't carry over
	if (empty($goods_id) && empty($goods_number) && $clean_integral != 2)
	{
		$order['integral'] = 0;
		$order['coupon_id'] = array();
		if (isset($order['exclusive_coupon'])) unset($order['exclusive_coupon']);
	}
	
	// Update shipping / payment method
	$order['shipping_id'] = $shipping;
	$order['pay_id'] = $payment;
	
    // Update order type
    $order_type = isset($_REQUEST['order_mode']) ? $_REQUEST['order_mode'] : 'normal';
	if($order_type == 'sales-agent') { 
		$order['order_type']   = Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT;
	} elseif($order_type == 'wholesale') { 
		$order['order_type'] = Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE;
	}
	else {
        $order['order_type'] = NULL;
    }
    
	$smarty->assign('order', $order);
	
	$consignee['country'] = isset($_REQUEST['country']) ? intval($_REQUEST['country']) : 0;
	$consignee['province'] = isset($_REQUEST['province']) ? intval($_REQUEST['province']) : 0;
	$consignee['city'] = isset($_REQUEST['city']) ? intval($_REQUEST['city']) : 0;
	$consignee['district'] = isset($_REQUEST['district']) ? intval($_REQUEST['district']) : 0;

	/*
	 * 計算訂單的費用
	 */
	$cart_goods = cart_goods_1($flow_type, null, null, $user_rank); // 取得商品列表，計算合計
	$total = $orderController->order_fee_flow($order, $cart_goods, $consignee, $order_fee_platform);
	
	$smarty->assign('total', $total);
	$smarty->assign('shopping_money', sprintf($_LANG['shopping_money'], $total['formated_goods_price']));
	$smarty->assign('market_price_desc', sprintf($_LANG['than_market_price'], $total['formated_market_price'], $total['formated_saving'], $total['save_rate']));
	
	$_SESSION['flow_order'] = $order;
	
	// 檢查是否包括需訂貨產品
	$preorder_msg = '';
	if (!empty($cart_goods))
	{
	    $goods_preorder = check_goods_preorder($cart_goods);
	    if (!empty($goods_preorder))
	    {
	        foreach ($goods_preorder as $preorder)
	        {
	            $preorder_msg .= $preorder['goods_name'] . ' 訂貨大約' . $preorder['pre_sale_days'] . '工作天' . "\n";
	        }
	    }
	}
	$need_removal_list = $rspController->needRemovalGoods($cart_goods);
    if(!empty($need_removal_list)) {
        $smarty->assign('need_removal', $need_removal_list);
    }
	output_json(array(
		'status' => 1,
		'cart_list' => $goods_list,
		'goods_amount' => $total['goods_price_formated'],
		'order_total' => $smarty->fetch('library/pos_order_total.lbi'),
		'preorder_msg' => $preorder_msg,
		'need_removal' => $need_removal_list

	));
}

// Retrieve Goods Information with Barcode
else if ($action == 'get_goods_by_sn')
{
	$goods_sn = empty($_REQUEST['goods_sn']) ? '' : trim($_REQUEST['goods_sn']);
	$user_rank = empty($_REQUEST['user_rank']) ? 0 : intval($_REQUEST['user_rank']);

	if (empty($goods_sn))
	{
		output_json(array(
			'status' => 0,
			'goods_sn' => $goods_sn,
			'error' => '請輸入 Barcode'
		));
	}
	
	$sql = "SELECT goods_id FROM " . $ecs->table("goods") . " WHERE goods_sn ='$goods_sn' AND is_on_sale = 1 AND is_alone_sale = 1 AND is_delete = 0 LIMIT 1";
	$goods_ids = $db->getCol($sql);
	
	if (empty($goods_ids))
	{
		// Try searching by name
		$search = '(' . implode(' AND ', array_map(function ($keyword) { return "goods_name LIKE '%" . mysql_like_quote($keyword) . "%'"; } , preg_split('/\s+/', $goods_sn))) . ')';
		$sql = "SELECT goods_id FROM " . $ecs->table("goods") . " WHERE " . $search . " AND is_on_sale = 1 AND is_alone_sale = 1 AND is_delete = 0";
		if($_REQUEST['order_mode'] == 'normal') $sql .= " AND (goods_sn = goods_id OR LOWER(goods_sn) = CONCAT('no', goods_id)) "; //  We only can search by goods_name if goods is no barcode.(No barcode : goods_id = goods_sn)
		$goods_ids = $db->getCol($sql);
		
		if (empty($goods_ids))
		{
			output_json(array(
				'status' => 0,
				'goods_sn' => $goods_sn,
				'error' => '沒有產品符合您輸入的 Barcode: ' . $goods_sn
			));
		}
	}
	
	$goods_list = array();
	foreach ($goods_ids as $goods_id)
	{
		// get_goods_info() declared in lib_goods.php, which is loaded by lib_order.php already
		$goods = get_goods_info($goods_id, true);

		// Get price
		$final_price     = get_final_price($goods_id, 1, true, array(), $user_rank);
		$shop_price      = $final_price['final_price'];
		$goods_user_rank = $final_price['user_rank_id'];
		$youhui_price = $row['shop_price'] > $shop_price ? ($row['shop_price'] - $shop_price) : 0;
		
		// Create response object
		$goodsOutput = array(
			'goods_id' => $goods['goods_id'],
			'goods_name' => $goods['goods_name'],
			'goods_sn' => $goods['goods_sn'],
			'goods_img' => $goods['goods_thumb'],
			'shop_price' => $shop_price,
			'youhui_price' => $youhui_price,
			//'goods_number' => $goods['goods_number'],
			'goods_number' => $goods['goods_number_shop'][1],  // we dont count on FLS stock
			'goods_number_str' => $goods['goods_number_sn'],
			'goods_user_rank' => $goods_user_rank
		);

		$goods_list[] = $goodsOutput;
	}
	
	output_json(array(
		'status' => 1,
		'goods_sn' => $goods_sn,
		'goods_list' => $goods_list
	));
}

// Get goods preview popup
else if ($action == 'get_goods_preview')
{
	$goods_id = empty($_REQUEST['goods_id']) ? 0 : intval($_REQUEST['goods_id']);
	
	$goods = get_goods_info($goods_id, true);
	
	if (!$goods)
	{
		output_json(array(
			'status' => 0,
			'goods_id' => $goods_id,
			'error' => '查無此產品ID'
		));
	}
	
	$smarty->assign('goods', $goods);
	$smarty->assign('goods_id', $goods['goods_id']);
	
	$html = $smarty->fetch('library/pos_goods_preview.lbi');
	
	output_json(array(
		'status' => 1,
		'goods_id' => $goods_id,
		'result' => $html
	));
}

// Check if account exists, and return account information
else if ($action == 'validate_account')
{
	$user_name = empty($_REQUEST['user_name']) ? '' : trim($_REQUEST['user_name']);
	
	if (empty($user_name))
	{
		output_json(array(
			'status' => 0,
			'user_name' => $user_name,
			'error' => '請輸入會員ID'
		));
	}
	
	$sql = "SELECT user_id FROM ". $ecs->table('users')."  WHERE user_name = '$user_name' LIMIT 1";
	$user_id = $db->getOne($sql);
	
	if (!$user_id)
	{
		output_json(array(
			'status' => 0,
			'user_name' => $user_name,
			'error' => '查無此會員ID'
		));
	}
	
	$user = user_info($user_id);

	$ws_company = "";
	if (!empty($user['wsid'])) {
		$ws_company = $db->getOne("SELECT company FROM " . $ecs->table('wholesale_customer') . " WHERE wsid = $user[wsid]");
	}
	output_json(array(
		'status' => 1,
		'user_id' => $user_id,
		'user_name' => $user_name,
		'email' => $user['email'],
		'user_money' => ($user['user_money'] + $user['credit_line']), // 餘額 + 信用額
		'pay_points' => $user['pay_points'], // 積分
		'is_wholesale' => $user['is_wholesale'],
		'ws_customer_id' => $user['wsid'], // 批發客ID
		'ws_company' => $ws_company, // 批發客公司
        'user_rank'    => $user['user_rank'],
		'rank_name'    => $user['rank_name']
	));
}

// Validate integral / discount / coupon
else if ($action == 'calculate_amount')
{
	$shipping = empty($_REQUEST['shipping']) ? 0 : intval($_REQUEST['shipping']);
	$payment = empty($_REQUEST['payment']) ? 0 : intval($_REQUEST['payment']);
	
	$integral = empty($_REQUEST['integral']) ? 0 : intval($_REQUEST['integral']);
	$discount = empty($_REQUEST['discount']) ? 0 : floatval($_REQUEST['discount']);
	$coupon_code = empty($_REQUEST['coupon']) ? array() : explode(',',trim($_REQUEST['coupon']));
	$deposit = empty($_REQUEST['deposit']) ? 0 : floatval($_REQUEST['deposit']);
	$promote_list = array();
	
	if ($integral < 0)
	{
		output_json(array(
			'status' => 0,
			'integral' => $integral,
			'error' => '使用積分不能少於 0！'
		));
	}
	
	$user_name = empty($_REQUEST['user_name']) ? '' : trim($_REQUEST['user_name']);
	
	if (!empty($user_name))
	{
		$sql = "SELECT user_id FROM ". $ecs->table('users')."  WHERE user_name = '$user_name' LIMIT 1";
		$user_id = $db->getOne($sql);
		
		if (!$user_id)
		{
			output_json(array(
				'status' => 0,
				'user_name' => $user_name,
				'error' => '查無此會員ID'
			));
		}
		
		$user = user_info($user_id);
	}
	
	// Integral is used, validate it
	if ($integral > 0)
	{
		// Must provide user name to use integral
		if (empty($user_name))
		{
			output_json(array(
				'status' => 0,
				'user_name' => $user_name,
				'error' => '請輸入會員ID'
			));
		}
		
		if ($integral > $user['pay_points'])
		{
			output_json(array(
				'status' => 0,
				'integral' => $integral,
				'pay_points' => $user['pay_points'],
				'error' => '使用積分('.$integral.')大於會員擁有的積分('.$user['pay_points'].')！'
			));
		}
		
		// 该订单允许使用的积分
		$flow_points = flow_available_points_1();
		if ($integral > $flow_points)
		{
			output_json(array(
				'status' => 0,
				'integral' => $integral,
				'flow_points' => $flow_points,
				'error' => '使用積分('.$integral.')大於訂單允許使用的積分('.$flow_points.')！'
			));
		}
	}
	
	if ($discount == 0)
	{
		output_json(array(
			'status' => 0,
			'discount' => $discount,
			'vardiscount' => 1,
			'error' => '折扣不能設為 0！'
		));
	}
	
	/* 取得購物類型 */
	$flow_type = isset($_SESSION['flow_type']) ? intval($_SESSION['flow_type']) : CART_GENERAL_GOODS;

	/* 獲得收貨人信息 */
   	$consignee['country'] = isset($_REQUEST['country']) ? intval($_REQUEST['country']) : 0;
	$consignee['province'] = isset($_REQUEST['province']) ? intval($_REQUEST['province']) : 0;
	$consignee['city'] = isset($_REQUEST['city']) ? intval($_REQUEST['city']) : 0;
	$consignee['district'] = isset($_REQUEST['district']) ? intval($_REQUEST['district']) : 0;

	/* 對商品信息賦值 */
	$cart_goods = cart_goods_1(null, null, $user_name); // 取得商品列表，計算合計
	
	if (empty($cart_goods))
	{
		output_json(array(
			'status' => 0,
			'error' => '您的訂單中沒有任何產品'
		));
	}
	
	/* 取得購物流程設置 */
	$smarty->assign('config', $_CFG);
	
	/* 取得訂單信息 */
	$order = flow_order_info();
	
	// Apply Shipping / Payment method if provided
	if (!empty($shipping))
	{
		$order['shipping_id'] = $shipping;
	}
	if (!empty($payment))
	{
		$order['pay_id'] = $payment;
	}
	
	// Reset coupons before calculation
	$order['coupon_id'] = array();
	if (isset($order['exclusive_coupon'])) unset($order['exclusive_coupon']);
    
    // Update order type
    $order_type = isset($_REQUEST['order_mode']) ? $_REQUEST['order_mode'] : 'normal';
    if($order_type == 'sales-agent'){ $order['order_type']   = Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT; }
	elseif($order_type == 'wholesale'){ $order['order_type'] = Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE; }
	else {
        $order['order_type'] = NULL;
    }
	
	// Apply coupon if provided
	if (!empty($coupon_code))
	{
		// trim whitspace -> filter empty string -> upper case -> unique
		$coupon_code = array_unique(array_map('strtoupper', array_filter(array_map('trim', $coupon_code))));
		
		$invalid_codes = array();
		$error_msg = '';
		
		foreach ($coupon_code as $code)
		{
			$coupon = coupon_info(0, $code);
			
			$error = 0;
			if (verify_coupon($coupon, $order, cart_amount_1(true, $flow_type), $cart_goods, $user_id, $error))
			{
				// Assign coupon_id to order
				$order['coupon_id'][] = $coupon['coupon_id'];
				if (!empty($coupon['promote_id'])) {
					$promote_list[$code] = intval($coupon['promote_id']);
				}
			}
			else
			{
				$invalid_codes[] = $code;
				$error_msg .= (empty($error_msg) ? '' : '<br>') . '優惠券「' . $code . '」驗證失敗： ' . coupon_error_message($error);
			}
		}
		
		if (!empty($invalid_codes))
		{
			$valid_codes = array_filter($coupon_code, function ($c) use ($invalid_codes) { return !in_array($c, $invalid_codes); });
			output_json(array(
				'status' => 0,
				'coupon_code' => implode(',', $valid_codes),
				'error' => $error_msg
			));
		}
	}
	
	// Apply integral to use
	$order['integral'] = $integral;
	
	$total = $orderController->order_fee_flow($order, $cart_goods, $consignee, $order_fee_platform, $discount);
	$smarty->assign('total', $total);
	
	// Handle deposit if provided
	$smarty->assign('deposit', $deposit);
	if ($deposit > 0)
	{
		$smarty->assign('deposit_formated', price_format($deposit));
		$due_amount = $total['amount'] - $deposit;
		$smarty->assign('due_amount', $due_amount);
		$smarty->assign('due_amount_formated', price_format($due_amount));
	}
	
	output_json(array(
		'status' => 1,
		'total' => $total,
		'order_total' => $smarty->fetch('library/pos_order_total.lbi'),
		'promote_list' => $promote_list,
	));
}

// Load existing order
else if ($action == 'load_existing_order')
{
	$order_sn = empty($_REQUEST['order_sn']) ? '' : trim($_REQUEST['order_sn']);
	
	if (empty($order_sn))
	{
		output_json(array(
			'status' => 0,
			'order_sn' => $order_sn,
			'error' => '請先輸入訂單號'
		));
	}
	
	$order = order_info(0, $order_sn); //$db->getRow("SELECT * FROM " . $ecs->table('order_info') . " WHERE order_sn = '" . $order_sn . "'");
	
	$order_id = empty($order['order_id']) ? 0 : intval($order['order_id']);
	if (!$order_id)
	{
		output_json(array(
			'status' => 0,
			'order_sn' => $order_sn,
			'error' => '查無此訂單號'
		));
	}
	
    $platforms = Yoho\cms\Controller\OrderController::DB_SELLING_PLATFORMS;
    $order['platform_display'] = isset($platforms[$order['platform']]) ? $platforms[$order['platform']] : $order['platform'];
	
	$user_id = empty($order['user_id']) ? 0 : intval($order['user_id']);
	$user_name = $user_id ? $db->getOne("SELECT user_name FROM " . $ecs->table('users') . " WHERE user_id = '" . $user_id . "'") : '';
	
	$goods = $db->getAll("SELECT * FROM " . $ecs->table('order_goods') . " WHERE order_id = '" . $order_id . "'");
	
	output_json(array(
		'status' => 1,
		'order_id' => $order_id,
		'order' => $order,
		'user_name' => $user_name,
		'order_goods' => $goods
	));
}

// Load user input
else if ($action == 'load_user_input')
{
	$sql = "SELECT ui.`rec_id`, ui.`ccc`, ui.`mobile`, ui.`user_name`, ui.`email`, u.`user_id` " .
			" FROM " . $ecs->table('pos_user_input') . " as ui " .
			" LEFT JOIN " . $ecs->table('users') . " as u ON ui.`user_name` = u.`user_name` " .
			" WHERE ui.`add_time` > " . (gmtime() - 300) . // Only show input from last 5 minutes
			" ORDER BY ui.`add_time` DESC LIMIT 10";
	$list = $db->getAll($sql);
	
	output_json(array(
		'status' => 1,
		'input_list' => $list
	));
}

// Place the order
else if ($action == 'place_order')
{
	// use quick_buy.php?step=done instead
	// no time to refactor code here for now
}

// Query sales agent rates
else if ($action == 'get_sales_agent')
{
	$sa_id = empty($_REQUEST['sa_id']) ? 0 : $_REQUEST['sa_id'];

	if(!empty($sa_id)){

		$salesagent = new  Yoho\cms\Model\Salesagent($sa_id);
		$rates = $salesagent->getRates();

		output_json(array(
				'status' => 1,
				'rates' => $rates
		));
	}
}

// Update order with new salesagent price
else if ($action == 'update_order_price'){

	/* 取得購物類型 */
	$flow_type = isset($_SESSION['flow_type']) ? intval($_SESSION['flow_type']) : CART_GENERAL_GOODS;
	$smarty->assign('config', $_CFG);
	$user_info = user_info($_SESSION['user_id']);

	$shipping = empty($_REQUEST['shipping']) ? 0 : floatval($_REQUEST['shipping']);
	$payment = empty($_REQUEST['payment']) ? 0 : floatval($_REQUEST['payment']);

	$sa_price =  empty($_REQUEST['goods_replace_price']) ? array() : $_REQUEST['goods_replace_price'];


	$cart_goods = get_cart_goods_1();

	usort($cart_goods['goods_list'], function ($a, $b) { return intval($a['rec_id']) - intval($b['rec_id']); });
	$goods_list = array();
	foreach ($cart_goods['goods_list'] as $goods)
	{
		$goods_list[] = array(
			'goods_id' => $goods['goods_id'],
			'goods_name' => $goods['goods_name'],
			'goods_number' => $goods['goods_number'],
			'goods_price' => $goods['goods_price'],
			'subtotal' => $goods['subtotal'],
			'is_auto_add' => $goods['is_auto_add']
		);
	}

	/*
	 * 取得訂單信息
	 */
	$order = flow_order_info();
	$order['integral'] = 0;
	$order['coupon_id'] = array();
	if (isset($order['exclusive_coupon'])) unset($order['exclusive_coupon']);

	// Update shipping / payment method
	$order['shipping_id'] = $shipping;
	$order['pay_id'] = $payment;
    
    // Update order type
    $order_type = isset($_REQUEST['order_mode']) ? $_REQUEST['order_mode'] : 'normal';
    if($order_type == 'sales-agent'){ $order['order_type'] = Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT; } else {
        $order['order_type'] = NULL;
    }
    
	$smarty->assign('order', $order);

	$consignee['country'] = isset($_REQUEST['country']) ? intval($_REQUEST['country']) : 0;
	$consignee['province'] = isset($_REQUEST['province']) ? intval($_REQUEST['province']) : 0;
	$consignee['city'] = isset($_REQUEST['city']) ? intval($_REQUEST['city']) : 0;
	$consignee['district'] = isset($_REQUEST['district']) ? intval($_REQUEST['district']) : 0;

	/*
	 * 計算訂單的費用
	 */
	$cart_goods = cart_goods_1($flow_type, $sa_price); // 取得商品列表，計算合計
	$total = $orderController->order_fee_flow($order, $cart_goods, $consignee, $order_fee_platform);
	$goods_list = array();
	foreach ($cart_goods as $goods)
	{
		$goods_list[] = array(
			'goods_id' => $goods['goods_id'],
			'goods_name' => $goods['goods_name'],
			'goods_number' => $goods['goods_number'],
			'goods_price' => $goods['formated_goods_price'],
			'subtotal' => $goods['formated_subtotal'],
			'custom_price' => $goods['formated_custom_price'],
			'custom_subtotal' => $goods['formated_custom_subtotal'],
			'is_auto_add' => $goods['is_auto_add']
		);
	}
	$smarty->assign('total', $total);
	$smarty->assign('shopping_money', sprintf($_LANG['shopping_money'], $total['formated_goods_price']));
	$smarty->assign('market_price_desc', sprintf($_LANG['than_market_price'], $total['formated_market_price'], $total['formated_saving'], $total['save_rate']));

	$_SESSION['flow_order'] = $order;

	// 檢查是否包括需訂貨產品
	$preorder_msg = '';
	if (!empty($cart_goods))
	{
	    $goods_preorder = check_goods_preorder($cart_goods);
	    if (!empty($goods_preorder))
	    {
	        foreach ($goods_preorder as $preorder)
	        {
	            $preorder_msg .= $preorder['goods_name'] . ' 訂貨大約' . $preorder['pre_sale_days'] . '工作天' . "\n";
	        }
	    }
	}

	output_json(array(
		'status' => 1,
		'cart_list' => $goods_list,
		'goods_amount' => $total['goods_price_formated'],
		'order_total' => $smarty->fetch('library/pos_order_total.lbi'),
		'preorder_msg' => $preorder_msg
	));
}

else if ($action == 'update_shipping_country'){
	$shipping_id = empty($_REQUEST['shipping']) ? 3 : $_REQUEST['shipping'];
	if($shipping_id == 99)$shipping_id = 0;
	output_json(array(
		'status' => 1,
		'shipping_id' => $shipping_id,
		'country_list' => get_shipping_available_countries($shipping_id)
	));
}

// ajax: handle favourable goods
else if ($action == 'load_favourable')
{
	extract($_REQUEST);
	$favourable_list = $orderController->favourable_list($user_rank, $user_id, $order_fee_platform);

	if (isset($_SESSION['pos_update_cart'])) {
		$need_to_update_cart = true;
		unset($_SESSION['pos_update_cart']);
	} else {
		$need_to_update_cart = !$orderController->check_cart_favourable_goods($user_rank, $user_id, $order_fee_platform);
	}
	output_json(array(
		'status' => 1,
		'favourable_list' => $favourable_list,
		'need_update' => $need_to_update_cart
	));
}
// ajax: add favourable goods
else if ($action == 'add_favourable')
{
	extract($_REQUEST);

	$res = $orderController->add_favourable_goods($goods_id, $quantity, $act_id, $user_rank, $user_id, $order_fee_platform);
	if($res){
		$info = $orderController->get_pos_order_info($user_rank);
		$goods_list = $info['goods_list'];
		$total = $info['total'];
		$smarty->assign('total', $total);
		output_json(array(
			'status' => 1,
			'cart_list' => $goods_list,
			'goods_amount' => $total['goods_price_formated'],
			'order_total' => $smarty->fetch('library/pos_order_total.lbi')
		));
	} else {
		output_json(array(
			'status' => 0,
			'error' => '添加優惠失敗!'
		));
	}
}
// ajax: handle require goods
else if ($action == 'load_requiregoods')
{
	extract($_REQUEST);
	$require_goods_list = [];

	// get all goods_id in cart
	$in_cart_goods =  $db->getAll("SELECT goods_id, goods_name FROM " . $ecs->table("cart1") . " WHERE session_id = '" . SESS_ID . "'");

	foreach ($in_cart_goods as $cart_goods) {
		$in_cart_goods_id[] = $cart_goods['goods_id'];
	}
	foreach ($in_cart_goods as $cart_goods) {
		$require_goods_id = $db->getOne("SELECT require_goods_id FROM " . $ecs->table("goods") . " WHERE goods_id = $cart_goods[goods_id]");
		$goods_list = [];

		if (!empty($require_goods_id) && !in_array($require_goods_id, $in_cart_goods_id)) {
			// get_goods_info() declared in lib_goods.php, which is loaded by lib_order.php already
			$goods = get_goods_info($require_goods_id, true);

			// Get price
			$shop_price  = get_final_price($require_goods_id, 1, true, array(), $user_rank)['final_price'];
			$youhui_price = $row['shop_price'] > $shop_price ? ($row['shop_price'] - $shop_price) : 0;

			// Create response object
			$goodsOutput = array(
				'goods_id' => $goods['goods_id'],
				'goods_name' => $goods['goods_name'],
				'goods_sn' => $goods['goods_sn'],
				'goods_img' => $goods['goods_thumb'],
				'shop_price' => $shop_price,
				'youhui_price' => $youhui_price,
				'goods_number' => $goods['goods_number'],
				'goods_number_str' => $goods['goods_number_sn'],
				'formatted_shop_price' => price_format($shop_price),
			);

			$goods_list[] = $goodsOutput;
		}
		if (!empty($goods_list)) {
			$require_goods_list[] = [
				'goods_id' => $cart_goods['goods_id'],
				'goods_name' => $cart_goods['goods_name'],
				'goods_list' => $goods_list,
			];
		}
	}

	output_json(array(
		'status' => 1,
		'require_goods_list' => $require_goods_list,
		'in_cart_goods' => $in_cart_goods_id
	));
}
/*------------------------------------------------------ */
//-- 判断是否存在缓存，如果存在则调用缓存，反之读取相应内容
/*------------------------------------------------------ */
/* 缓存编号 */
$cache_id = sprintf('%X', crc32($_SESSION['user_rank'] . '-' . $_CFG['lang']));
if (!$smarty->is_cached('pos.dwt', $cache_id))
{
	assign_template();
	//$cat = get_cat_info($cat_id);   // 获得分类的相关信息
	$position = assign_ur_here(0, 'POS 系統');
	$smarty->assign('page_title',	  $position['title']);	// 页面标题
	$smarty->assign('ur_here',		 $position['ur_here']);  // 当前位置
	/* meta information */
	$smarty->assign('keywords',		htmlspecialchars($_CFG['shop_keywords']));
	$smarty->assign('description',	 htmlspecialchars($_CFG['shop_desc']));
	
	// Shipping methods
	$shipping_list = shipping_list(1);
	if(count($shipping_list) <= 0)$shipping_list = available_shipping_list(array(1,3409,3438)); // array(3409)
	$shipping_list_output = array();
	foreach ($shipping_list as $shipping)
	{
		$shipping_list_output[] = array(
			'shipping_id' => $shipping['shipping_id'],
			'shipping_name' => $shipping['shipping_name']
		);
	}
	$smarty->assign('shipping_list', $shipping_list_output);
	
	// Warehouselist
	$erpController = new Yoho\cms\Controller\ErpController();
	//$smarty->assign('warehouse_list', $erpController->getWarehouseList());
	$shop_warehouse_id = $erpController->getShopWarehouseId();
	$smarty->assign('warehouse_list', $erpController->getWarehouseListByShopId($shop_warehouse_id));

	// Payment methods
	$filter_data['is_pos'] = true;
	$payment_list = $paymentController->available_payment_list(true, 0, false, $filter_data);

	$smarty->assign('payment_list', $payment_list);
	
	// Integral scale
	$smarty->assign('integral_scale', $_CFG['integral_scale']);
	
	// List of salesperson available in POS
	$smarty->assign('salespeople', $db->getAll('SELECT sales_id, sales_name FROM ' . $GLOBALS['ecs']->table('salesperson') . ' AS sp WHERE available = 1 AND allow_pos = 1'));

	// List of sales agent with rate
	$smarty->assign('sales_agents', get_pos_sales_agent());
    
	// List country **( 3 is hard code shipping id : 速遞)
	$smarty->assign('country_list',      get_shipping_available_countries(3));
}

$smarty->display('pos.dwt', $cache_id);

exit;

function output_json($obj)
{
	header('Content-Type: application/json');
	echo json_encode($obj);
	exit;
}

?>
