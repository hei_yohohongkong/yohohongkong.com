<?php

/***
* pending_receive.php
* by howang 2015-01-14
*
* Display a list of pending receive products base on purchase orders
***/

define('IN_ECS', true);
require(dirname(__FILE__) . '/includes/init.php');
$controller = new Yoho\cms\Controller\YohoBaseController('erp_order_item');
if ($_REQUEST['act'] == 'list')
{
    $smarty->assign('ur_here',      $_LANG['pending_receive']);
    $smarty->assign('keyword', empty($_REQUEST['keyword']) ? '' : $_REQUEST['keyword']);
    $smarty->assign('action_link', array('text' => '即將送貨採購訂單', 'href' => 'erp_order_manage.php?act=order_list&pending_receive=1'));

    assign_query_info();
    $smarty->display('pending_receive_list.htm');
}
elseif ($_REQUEST['act'] == 'query')
{
    $list = get_pending_receive_products();
    $smarty->assign('list',         $list['data']);
    $smarty->assign('record_count', $list['record_count']);
    $controller->ajaxQueryAction($list['data'], $list['record_count'], false);

}

function get_pending_receive_products()
{
    $_REQUEST['sort_by']      = empty($_REQUEST['sort_by']) ? 'expected_delivery_time' : trim($_REQUEST['sort_by']);
    $_REQUEST['sort_order']   = empty($_REQUEST['sort_order']) ? 'ASC' : trim($_REQUEST['sort_order']);
    
    $_REQUEST['keyword']      = empty($_REQUEST['keyword']) ? '' : trim($_REQUEST['keyword']);
    
    $where = "o.order_status = 4 AND w.warehousing_id IS NULL AND o.expected_delivery_time > 0 ";
    
    if (!empty($_REQUEST['keyword']))
    {
        $where .= " AND (g.goods_sn LIKE '%" . mysql_like_quote($_REQUEST['keyword']) . "%' ";
        $where .= " OR g.goods_name LIKE '%" . mysql_like_quote($_REQUEST['keyword']) . "%' ";
        $tmp = preg_split('/\s+/', $_REQUEST['keyword']);
        if (count($tmp) > 1)
        {
            $where .= " OR ( 1";
            foreach ($tmp as $kw)
            {
                $where .= " AND g.goods_name LIKE '%" . mysql_like_quote($kw) . "%' ";
            }
            $where .= ") ";
        }
        $where .= " OR g.goods_id = '" . mysql_like_quote($_REQUEST['keyword']) . "') ";
    }
    
    $sql = "SELECT count(*) " .
            "FROM " . $GLOBALS['ecs']->table('erp_order_item') . " as oi " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('erp_order') . " as o ON o.order_id = oi.order_id " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('erp_warehousing') . " as w ON w.order_id = o.order_id AND w.warehousing_style_id = 1 AND w.status = 3 " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON g.goods_id = oi.goods_id " .
            "WHERE " . $where;
    $_REQUEST['record_count'] = $GLOBALS['db']->getOne($sql);
    
    /* 查询 */
    $sql = "SELECT g.`goods_thumb`, g.`goods_id`, g.`goods_name`, g.`goods_sn`, (oi.`order_qty` - oi.`warehousing_qty`) as `order_qty`, o.`order_id`, o.`order_sn`, o.`expected_delivery_time` " .
            "FROM " . $GLOBALS['ecs']->table('erp_order_item') . " as oi " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('erp_order') . " as o ON o.order_id = oi.order_id " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('erp_warehousing') . " as w ON w.order_id = o.order_id AND w.warehousing_style_id = 1 AND w.status = 3 " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON g.goods_id = oi.goods_id " .
            "WHERE " . $where . 
            "ORDER BY " . $_REQUEST['sort_by'] . " " . $_REQUEST['sort_order'] . " " .
            "LIMIT " . $_REQUEST['start'] . "," . $_REQUEST['page_size'];
    $data = $GLOBALS['db']->getAll($sql);
    
    foreach ($data as $key => $row)
    {
        $data[$key]['expected_delivery_date'] = local_date('Y-m-d', $row['expected_delivery_time']);
        $data[$key]['expected_delivery_days'] = intval(ceil(($row['expected_delivery_time'] - gmtime()) / 86400));
    }

    $arr = array('data' => $data, 'record_count' => $_REQUEST['record_count']);

    return $arr;
}

?>