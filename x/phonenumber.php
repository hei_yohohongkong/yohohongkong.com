<?php

define('IN_ECS', true);
require dirname(__FILE__) . '/includes/init.php';

if ($_REQUEST['act'] == 'list') {
    if (empty($_CFG['phonenumber_config'])) {
        $_CFG = load_config(true);
    }
    $smarty->assign('config', json_decode($_CFG['phonenumber_config'], true));
    $smarty->display('phonenumber.htm');
} elseif ($_REQUEST['act'] == "update") {
    $coupons = [];
    foreach ($_REQUEST['coupon_id'] as $key => $val) {
        $coupons[intval($key) + 1] = [
            'coupon_id' => $val,
            'limit' => $_REQUEST['coupon_limit'][$key],
        ];
    }
    $config = [
        'coupons' => $coupons,
        'start_time' => trim($_REQUEST['start_time']) . ":00",
        'end_time' => trim($_REQUEST['end_time']) . ":00",
    ];
    $db->query("UPDATE " . $ecs->table('shop_config') . " SET value = '" . json_encode($config) . "' WHERE code = 'phonenumber_config'");
    $_CFG = load_config(true);
    sys_msg("已更新設置");
}