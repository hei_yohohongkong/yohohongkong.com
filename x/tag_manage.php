<?php

/**
 * ECSHOP 后台标签管理
 * ============================================================================
 * 版权所有 2005-2010 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liuhui $
 * $Id: tag_manage.php 17063 2010-03-25 06:35:46Z liuhui $
*/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
$tagController = new Yoho\cms\Controller\TagController();
$admin_priv    = $tagController::DB_ACTION_CODE_TAG_MANAGE;
/* act操作项的初始化 */
$_REQUEST['act'] = trim($_REQUEST['act']);
if (empty($_REQUEST['act']))
{
    $_REQUEST['act'] = 'list';
}
$smarty->assign('type_list',      $tagController->get_tag_type());
$smarty->assign('tag_words_list', $tagController->get_tag_words_list());

/*------------------------------------------------------ */
//-- 获取标签数据列表
/*------------------------------------------------------ */
if ($_REQUEST['act'] == 'list')
{
    /* 权限判断 */
    admin_priv($admin_priv);

    /* 模板赋值 */
    $smarty->assign('ur_here',      $_LANG['tag_list']);
    $smarty->assign('action_link2', array('href' => 'tag_manage.php?act=add', 'text' => $_LANG['add_tag']));
    $smarty->assign('action_link', array('href' => 'tag_manage.php?act=edit_tag_words', 'text' => $_LANG['edit_tag_words']));
    $smarty->assign('full_page',    1);

    /* 页面显示 */
    assign_query_info();
    $smarty->display('tag_manage.htm');
}

/*------------------------------------------------------ */
//-- 添加 ,编辑
/*------------------------------------------------------ */

elseif($_REQUEST['act'] == 'add' || $_REQUEST['act'] == 'edit')
{
    admin_priv($admin_priv);

    $is_add = $_REQUEST['act'] == 'add';
    $smarty->assign('insert_or_update', $is_add ? 'insert' : 'update');

    if($is_add)
    {
        $tag = array(
            'tag_id' => 0,
            'tag_words' => '',
            'goods_id' => 0,
            'goods_name' => $_LANG['pls_select_goods']
        );
        $smarty->assign('ur_here',      $_LANG['add_tag']);
    }
    else
    {
        $tag_id = $_GET['id'];
        $tag = $tagController->get_tag_info($tag_id);
        if($tag['goods_id'] > 0) $tag['tag_type'] = 1;
        else if($tag['brand_id'] > 0) $tag['tag_type'] = 2;
        else if($tag['cat_id'] > 0) $tag['tag_type'] = 3;
        $smarty->assign('ur_here',      $_LANG['tag_edit']);
    }

    $localizable_fields = localizable_fields_for_table('tag');
    $localized_versions = get_localized_versions('tag', $tag['tag_id'], $localizable_fields);
    $smarty->assign('localizable_fields', $localizable_fields);
    $smarty->assign('localized_versions', $localized_versions);

    $smarty->assign('tag', $tag);
    $smarty->assign('action_link', array('href' => 'tag_manage.php?act=list', 'text' => $_LANG['tag_list']));
    $smarty->assign('cat_list',     cat_list(0, $tag['cat_id']));
    $smarty->assign('brand_list',   get_brand_list());
    assign_query_info();
    $smarty->display('tag_edit.htm');
}

/*------------------------------------------------------ */
//-- 更新
/*------------------------------------------------------ */

elseif($_REQUEST['act'] == 'insert' || $_REQUEST['act'] == 'update')
{
    admin_priv($admin_priv);

    $is_insert = $_REQUEST['act'] == 'insert';
    $post = $_POST;

    $res = $tagController->insert_update_tag($post, $is_insert);
    if($res)
    {
        /* 清除缓存 */
        clear_cache_files();

        $link[0]['text'] = $_LANG['back_list'];
        $link[0]['href'] = 'tag_manage.php?act=list';

        sys_msg($_LANG['tag_edit_success'], 0, $link);
    }
}

/*------------------------------------------------------ */
//-- 翻页，排序
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'query')
{
    check_authz_json($admin_priv);

    $tag_list = $tagController->get_tag_list();
    $tag_list['tags'] = localize_db_result_lang($_REQUEST['edit_lang'], 'tag', $tag_list['tags']);
    $tagController->ajaxQueryAction($tag_list['tags'],  $tag_list['record_count']);
}

/*------------------------------------------------------ */
//-- 搜索
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'search_goods')
{
    check_authz_json($admin_priv);

    include_once(ROOT_PATH . 'includes/cls_json.php');

    $json   = new JSON;
    $filter = $json->decode($_GET['JSON']);
    $arr    = get_goods_list($filter);
    if (empty($arr))
    {
        $arr[0] = array(
            'goods_id'   => 0,
            'goods_name' => ''
        );
    }

    make_json_result($arr);
}

/*------------------------------------------------------ */
//-- 批量删除标签
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'batch_drop')
{
    check_authz_json($admin_priv);

    if (isset($_POST['checkboxes']))
    {
        $count = 0;
        foreach ($_POST['checkboxes'] AS $key => $id)
        {
            $sql = "DELETE FROM " .$ecs->table('tag'). " WHERE tag_id='$id'";
            $db->query($sql);

            $count++;
        }

        admin_log($count, 'remove', $admin_priv);
        clear_cache_files();

        make_json_response(sprintf($_LANG['drop_success'], $count));
    }
    else
    {
        make_json_error($_LANG['no_select_tag']);
    }
}

/*------------------------------------------------------ */
//-- 删除标签
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'remove')
{
    check_authz_json($admin_priv);

    include_once(ROOT_PATH . 'includes/cls_json.php');
    $json = new JSON;

    $id = intval($_GET['id']);

    /* 获取删除的标签的名称 */
    $tag_name = $db->getOne("SELECT tag_words FROM " .$ecs->table('tag'). " WHERE tag_id = '$id'");

    $sql = "DELETE FROM " .$ecs->table('tag'). " WHERE tag_id = '$id'";
    $result = $GLOBALS['db']->query($sql);
    if ($result)
    {
        /* 管理员日志 */
        admin_log(addslashes($tag_name), 'remove', $admin_priv);

        $url = 'tag_manage.php?act=query&' . str_replace('act=remove', '', $_SERVER['QUERY_STRING']);
        ecs_header("Location: $url\n");
        exit;
    }
    else
    {
       make_json_error($db->error());
    }
}

elseif ($_REQUEST['act'] == 'edit_tag_words')
{
    check_authz_json($admin_priv);

    $smarty->assign('ur_here',      $_LANG['edit_tag_words']);
    $smarty->assign('action_link', array('href' => 'tag_manage.php?act=list', 'text' => $_LANG['tag_list']));
    assign_query_info();

    $smarty->display('tag_words_edit.htm');
}

elseif ($_REQUEST['act'] == 'update_tag_words')
{
    $res = $tagController->update_tag_words($_POST);

    if($res)
    {
        /* 清除缓存 */
        clear_cache_files();

        $link[0]['text'] = $_LANG['back_list'];
        $link[0]['href'] = 'tag_manage.php?act=list';

        sys_msg($_LANG['tag_edit_success'], 0, $link);
    } else {
        sys_msg($_LANG['tag_edit_error'], 1, array(), false);
    }
}

?>
