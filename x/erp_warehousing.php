<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require_once(ROOT_PATH .ADMIN_PATH .'/includes//ERP/lib_erp_common.php');
require_once(ROOT_PATH .ADMIN_PATH .'/includes/ERP/lib_erp_warehouse.php');
require_once(ROOT_PATH .ADMIN_PATH .'/includes/ERP/lib_erp_goods.php');
require_once(ROOT_PATH .ADMIN_PATH .'/includes/ERP/lib_erp_goods_attr.php');
$erpController = new Yoho\cms\Controller\ErpController();
$orderProgressController = new Yoho\cms\Controller\OrderProgressController();
if ($_REQUEST['act'] == 'change_warehouse')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_warehouse_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$warehousing_id=isset($_REQUEST['warehousing_id']) &&intval($_REQUEST['warehousing_id'])>0?intval($_REQUEST['warehousing_id']) : 0;
	$warehouse_id=isset($_REQUEST['warehouse_id']) &&intval($_REQUEST['warehouse_id'])>0?intval($_REQUEST['warehouse_id']) : 0;
	if(empty($warehousing_id) ||empty($warehouse_id))
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$warehousing_info=is_warehousing_exist($warehousing_id);
	if(!$warehousing_info)
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_warehousing_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if(!is_agency_warehousing($warehousing_id,$agency_id))
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_warehousing_no_agency_access'];
		die($json->encode($result));
	}
	if($warehousing_info['status']!=1)
	{
		$result['error']=6;
		$result['message']=$_LANG['erp_warehousing_no_acts'];
		die($json->encode($result));
	}
	if(!is_admin_warehousing($warehousing_id ,erp_get_admin_id()))
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_warehousing_no_access'];
		die($json->encode($result));
	}
	$warehouse_info=is_warehouse_exist($warehouse_id);
	if(!$warehouse_info)
	{
		$result['error']=4;
		$result['message']=$_LANG['erp_warehouse_not_exist'];
		die($json->encode($result));
	}
	if(!is_admin_warehouse($warehouse_id ,erp_get_admin_id()))
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_warehouse_no_access'];
		die($json->encode($result));
	}
	if(lock_table($warehousing_id,'warehousing','edit'))
	{
		$sql="update ".$ecs->table('erp_warehousing')." set warehouse_id='".$warehouse_id."' where warehousing_id='".$warehousing_id."'";
		$db->query($sql);
		$result['error']=0;
		die($json->encode($result));
	}
	else
	{
		$result['error']=-1;
		$result['message']=$_LANG['erp_warehousing_no_accessibility'];
		die($json->encode($result));
	}
}
elseif ($_REQUEST['act'] == 'change_warehousing_from')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_warehouse_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$warehousing_id=isset($_REQUEST['warehousing_id']) &&intval($_REQUEST['warehousing_id'])>0?intval($_REQUEST['warehousing_id']): 0;
	$warehousing_from=isset($_REQUEST['warehousing_from']) ?trim($_REQUEST['warehousing_from']) : '';
	if(empty($warehousing_id))
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$warehousing_info=is_warehousing_exist($warehousing_id);
	if(!$warehousing_info)
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_warehousing_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if(!is_agency_warehousing($warehousing_id,$agency_id))
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_warehousing_no_agency_access'];
		die($json->encode($result));
	}
	if($warehousing_info['status']!=1)
	{
		$result['error']=6;
		$result['message']=$_LANG['erp_warehousing_no_acts'];
		die($json->encode($result));
	}
	if(!is_admin_warehousing($warehousing_id ,erp_get_admin_id()))
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_warehousing_no_access'];
		die($json->encode($result));
	}
	if(lock_table($warehousing_id,'warehousing','edit'))
	{
		$sql="update ".$ecs->table('erp_warehousing')." set warehousing_from='".$warehousing_from."' where warehousing_id='".$warehousing_id."'";
		$db->query($sql);
		$result['error']=0;
		die($json->encode($result));
	}
	else
	{
		$result['error']=-1;
		$result['message']=$_LANG['erp_warehousing_no_accessibility'];
		die($json->encode($result));
	}
}
elseif ($_REQUEST['act'] == 'change_operator')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_warehouse_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$warehousing_id=isset($_REQUEST['warehousing_id']) &&intval($_REQUEST['warehousing_id'])>0?intval($_REQUEST['warehousing_id']): 0;
	$operator=isset($_REQUEST['operator']) ? trim($_REQUEST['operator']) : '';
	if(empty($warehousing_id))
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$warehousing_info=is_warehousing_exist($warehousing_id);
	if(!$warehousing_info)
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_warehousing_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if(!is_agency_warehousing($warehousing_id,$agency_id))
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_warehousing_no_agency_access'];
		die($json->encode($result));
	}
	if($warehousing_info['status']!=1)
	{
		$result['error']=6;
		$result['message']=$_LANG['erp_warehousing_no_acts'];
		die($json->encode($result));
	}
	if(!is_admin_warehousing($warehousing_id ,erp_get_admin_id()))
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_warehousing_no_access'];
		die($json->encode($result));
	}
	if(lock_table($warehousing_id,'warehousing','edit'))
	{
		$sql="update ".$ecs->table('erp_warehousing')." set operator='".$operator."' where warehousing_id='".$warehousing_id."'";
		$db->query($sql);
		$result['error']=0;
		die($json->encode($result));
	}
	else
	{
		$result['error']=-1;
		$result['message']=$_LANG['erp_warehousing_no_accessibility'];
		die($json->encode($result));
	}
}
elseif ($_REQUEST['act'] == 'change_warehousing_style')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_warehouse_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$warehousing_id=isset($_REQUEST['warehousing_id']) &&intval($_REQUEST['warehousing_id'])>0?intval($_REQUEST['warehousing_id']) : 0;
	$warehousing_style_id=isset($_REQUEST['warehousing_style_id']) &&intval($_REQUEST['warehousing_style_id'])>0?intval($_REQUEST['warehousing_style_id']) : 0;
	if(empty($warehousing_id) ||empty($warehousing_style_id))
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$warehousing_info=is_warehousing_exist($warehousing_id);
	if(!$warehousing_info)
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_warehousing_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if(!is_agency_warehousing($warehousing_id,$agency_id))
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_warehousing_no_agency_access'];
		die($json->encode($result));
	}
	if($warehousing_info['status']!=1)
	{
		$result['error']=6;
		$result['message']=$_LANG['erp_warehousing_no_acts'];
		die($json->encode($result));
	}
	if(!is_admin_warehousing($warehousing_id ,erp_get_admin_id()))
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_warehousing_no_access'];
		die($json->encode($result));
	}
	$warehousing_style_info=is_warehousing_style_exist($warehousing_style_id);
	if(!$warehousing_style_info)
	{
		$result['error']=4;
		$result['message']=$_LANG['erp_warehousing_style_not_exist'];
		die($json->encode($result));
	}
	if($warehousing_info['order_id']!='' && $warehousing_info['warehousing_style_id'] == $erpController->getWarehousingStyleIdByName($erpController::DB_WAREHOUSING_STYLE_REPAIRANDSTOCK) && $warehousing_style_id == $erpController->getWarehousingStyleIdByName($erpController::DB_WAREHOUSING_STYLE_REPAIRANDSTOCK)){
		$result['error']=6;
		$result['message']='POS維修單不能變更類型';
		die($json->encode($result));
	}

	if(lock_table($warehousing_id,'warehousing','edit'))
	{
		$sql="update ".$ecs->table('erp_warehousing')." set warehousing_style_id ='".$warehousing_style_id."' where warehousing_id='".$warehousing_id."'";
		$db->query($sql);
		if($warehousing_style_id!=1)
		{
			$sql="update ".$ecs->table('erp_warehousing')." set order_id='0' where warehousing_id='".$warehousing_id."'";
			$db->query($sql);
			$sql="delete from ".$ecs->table('erp_warehousing_item')." where warehousing_id='".$warehousing_id."'";
			$db->query($sql);
		}
		$result['error']=0;
		die($json->encode($result));
	}
	else
	{
		$result['error']=-1;
		$result['message']=$_LANG['erp_warehousing_no_accessibility'];
		die($json->encode($result));
	}
}
elseif ($_REQUEST['act'] == 'change_order')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_warehouse_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$warehousing_id=isset($_REQUEST['warehousing_id']) &&intval($_REQUEST['warehousing_id'])>0?intval($_REQUEST['warehousing_id']) : 0;
	$order_id=isset($_REQUEST['order_id']) &&intval($_REQUEST['order_id'])>0?intval($_REQUEST['order_id']) : 0;
	if(empty($warehousing_id) ||empty($order_id))
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$warehousing_info=is_warehousing_exist($warehousing_id);
	if(!$warehousing_info)
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_warehousing_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if(!is_agency_warehousing($warehousing_id,$agency_id))
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_warehousing_no_agency_access'];
		die($json->encode($result));
	}
	if($warehousing_info['status']!=1)
	{
		$result['error']=6;
		$result['message']=$_LANG['erp_warehousing_no_acts'];
		die($json->encode($result));
	}
	if(!is_admin_warehousing($warehousing_id ,erp_get_admin_id()))
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_warehousing_no_access'];
		die($json->encode($result));
	}
	if($warehousing_info['warehousing_style_id']==6){
		
	}else{
		$order_info=is_order_exist($order_id);
		if(!$order_info)
		{
			$result['error']=4;
			$result['message']=$_LANG['erp_order_not_exist'];
			die($json->encode($result));
		}
	}

	if(lock_table($warehousing_id,'warehousing','edit'))
	{
		$sql="update ".$ecs->table('erp_warehousing')." set order_id ='".$order_id."' where warehousing_id='".$warehousing_id."'";
		$db->query($sql);
		$sql="delete from ".$ecs->table('erp_warehousing_item')." where warehousing_id='".$warehousing_id."'";
		$db->query($sql);
		if($warehousing_info['warehousing_style_id']==6){
			if($erpController->addWarehousingItemByWsId($warehousing_id)){
				$result['error']=0;
				die($json->encode($result));
			}
		}else{
			if(create_warehousing_item_by_order($order_id,$warehousing_id)){
				$result['error']=0;
				die($json->encode($result));
			}
		}
	}
	else
	{
		$result['error']=-1;
		$result['message']=$_LANG['erp_warehousing_no_accessibility'];
		die($json->encode($result));
	}
}
elseif ($_REQUEST['act'] == 'change_received_qty')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_warehouse_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$warehousing_id=isset($_REQUEST['warehousing_id']) &&intval($_REQUEST['warehousing_id'])>0?intval($_REQUEST['warehousing_id']):0;
	$warehousing_item_id=isset($_REQUEST['warehousing_item_id']) &&intval($_REQUEST['warehousing_item_id'])>0?intval($_REQUEST['warehousing_item_id']):0;
	$received_qty=isset($_REQUEST['received_qty']) &&intval($_REQUEST['received_qty'])>0?intval($_REQUEST['received_qty']):0;
	if(empty($warehousing_id) ||empty($warehousing_item_id))
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$warehousing_info=is_warehousing_exist($warehousing_id);
	if(!$warehousing_info)
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_warehousing_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if(!is_agency_warehousing($warehousing_id,$agency_id))
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_warehousing_no_agency_access'];
		die($json->encode($result));
	}
	if($warehousing_info['status']!=1)
	{
		$result['error']=6;
		$result['message']=$_LANG['erp_warehousing_no_acts'];
		die($json->encode($result));
	}
	if(!is_admin_warehousing($warehousing_id ,erp_get_admin_id()))
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_warehousing_no_access'];
		die($json->encode($result));
	}
	$warehousing_item_info=is_warehousing_item_exist($warehousing_item_id);
	if(!$warehousing_item_info)
	{
		$result['error']=4;
		$result['message']=$_LANG['erp_warehousing_item_not_exist'];
		die($json->encode($result));
	}
	if(lock_table($warehousing_id,'warehousing','edit'))
	{
		$sql="update ".$ecs->table('erp_warehousing_item')." set received_qty='".$received_qty."',account_payable=price*".$received_qty." where warehousing_item_id='".$warehousing_item_id."'";
		$db->query($sql);
		$result['error']=0;
		die($json->encode($result));
	}
	else
	{
		$result['error']=-1;
		$result['message']=$_LANG['erp_warehousing_no_accessibility'];
		die($json->encode($result));
	}
}
elseif ($_REQUEST['act'] == 'delete_warehousing_item')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_warehouse_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$warehousing_id=isset($_REQUEST['warehousing_id']) &&intval($_REQUEST['warehousing_id'])>0?intval($_REQUEST['warehousing_id']):0;
	$warehousing_item_id=isset($_REQUEST['warehousing_item_id']) &&intval($_REQUEST['warehousing_item_id'])>0?intval($_REQUEST['warehousing_item_id']):0;
	if(empty($warehousing_id) ||empty($warehousing_item_id))
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$warehousing_info=is_warehousing_exist($warehousing_id);
	if(!$warehousing_info)
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_warehousing_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if(!is_agency_warehousing($warehousing_id,$agency_id))
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_warehousing_no_agency_access'];
		die($json->encode($result));
	}
	if($warehousing_info['status']!=1)
	{
		$result['error']=6;
		$result['message']=$_LANG['erp_warehousing_no_acts'];
		die($json->encode($result));
	}
	if(!is_admin_warehousing($warehousing_id ,erp_get_admin_id()))
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_warehousing_no_access'];
		die($json->encode($result));
	}
	$warehousing_item_info=is_warehousing_item_exist($warehousing_item_id);
	if(!$warehousing_item_info)
	{
		$result['error']=4;
		$result['message']=$_LANG['erp_warehousing_item_not_exist'];
		die($json->encode($result));
	}
	if(lock_table($warehousing_id,'warehousing','edit'))
	{
		$sql="delete from ".$ecs->table('erp_warehousing_item')." where warehousing_item_id='".$warehousing_item_id."'";
		$db->query($sql);
		$result['error']=0;
		die($json->encode($result));
	}
	else
	{
		$result['error']=-1;
		$result['message']=$_LANG['erp_warehousing_no_accessibility'];
		die($json->encode($result));
	}
}
elseif ($_REQUEST['act'] == 'delete_warehousing')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_warehouse_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$warehousing_id=isset($_REQUEST['warehousing_id']) &&intval($_REQUEST['warehousing_id'])>0?intval($_REQUEST['warehousing_id']) : 0;
	if(empty($warehousing_id))
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$warehousing_info=is_warehousing_exist($warehousing_id);
	if(!$warehousing_info)
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_warehousing_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if(!is_agency_warehousing($warehousing_id,$agency_id))
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_warehousing_no_agency_access'];
		die($json->encode($result));
	}
	if($warehousing_info['status']!=1)
	{
		$result['error']=6;
		$result['message']=$_LANG['erp_warehousing_no_acts'];
		die($json->encode($result));
	}
	if(!is_admin_warehousing($warehousing_id ,erp_get_admin_id()))
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_warehousing_no_access'];
		die($json->encode($result));
	}
	if($warehousing_info['status']!=1)
	{
		$result['error']=6;
		$result['message']=$_LANG['erp_warehousing_no_acts'];
		die($json->encode($result));
	}
	if(lock_table($warehousing_id,'warehousing','delete'))
	{
		$sql="delete from ".$ecs->table('erp_warehousing_item')." where warehousing_id='".$warehousing_id."'";
		$db->query($sql);
		$sql="delete from ".$ecs->table('erp_warehousing')." where warehousing_id='".$warehousing_id."'";
		$db->query($sql);
		$result['error']=0;
		die($json->encode($result));
	}
	else
	{
		$result['error']=-1;
		$result['message']=$_LANG['erp_warehousing_no_accessibility'];
		die($json->encode($result));
	}
}
elseif ($_REQUEST['act'] == 'check_goods_barcode')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	$goods_barcode=trim($_REQUEST['goods_barcode']);
	$barcode_id=check_goods_barcode($goods_barcode);
	if(empty($barcode_id))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_warehousing_wrong_goods_barcode'];
		die($json->encode($result));
	}
	$goods_attr=get_goods_attr_by_barcode($barcode_id);
	$smarty->assign('goods_attr',$goods_attr);
	$smarty->assign('is_dynamic',1);
	$attr_info=$smarty->fetch('erp_select_goods_attr.htm');
	$result['attr_info']=$attr_info;
	$result['attr_num']=count($goods_attr);
	$result['error']=0;
	die($json->encode($result));
}
elseif ($_REQUEST['act'] == 'check_goods_sn'||$_REQUEST['act'] == 'check_goods_name')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if($_REQUEST['act'] == 'check_goods_sn')
	{
		$goods_sn=trim($_REQUEST['goods_sn']);
		$goods_id=check_goods_sn($goods_sn);
		if(empty($goods_id))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_warehousing_wrong_goods_sn'];
			die($json->encode($result));
		}
	}
	else
	{
		$goods_name=trim($_REQUEST['goods_name']);
		$goods_id=check_goods_name($goods_name);
		if(empty($goods_id))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_warehousing_wrong_goods_name'];
			die($json->encode($result));
		}
	}
	$goods_attr=goods_attr($goods_id);
	$smarty->assign('goods_attr',$goods_attr);
	$smarty->assign('is_dynamic',1);
	$attr_info=$smarty->fetch('erp_select_goods_attr.htm');
	$result['attr_info']=$attr_info;
	$result['attr_num']=count($goods_attr);
	$result['error']=0;
	die($json->encode($result));
}
elseif ($_REQUEST['act'] == 'goods_barcode_tips')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	$goods_barcode=trim($_REQUEST['goods_barcode']);
	$fetch_goods_barcode=array();
	$sql="select barcode from ".$GLOBALS['ecs']->table('erp_goods_barcode')." where barcode like '%".$goods_barcode."%' limit 10";
	$res=$GLOBALS['db']->query($sql);
	while($row=mysql_fetch_assoc($res))
	{
		$fetch_goods_barcode[]=$row['barcode'];
	}
	$smarty->assign('goods_barcode',$fetch_goods_barcode);
	$content=$smarty->fetch('erp_goods_barcode_tips.htm');
	$result['error']=0;
	$result['content']=$content;
	$result['num']=count($fetch_goods_barcode);
	die($json->encode($result));
}
elseif ($_REQUEST['act'] == 'goods_sn_tips')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	$goods_sn=trim($_REQUEST['goods_sn']);
	$fetch_goods_sn=array();
	$sql="select g.goods_sn from ".$GLOBALS['ecs']->table('goods')." as g ";
	$sql.=" where g.goods_sn like '%".$goods_sn."%' order by goods_id desc limit 10";
	$res=$GLOBALS['db']->query($sql);
	while($row=mysql_fetch_assoc($res))
	{
		$fetch_goods_sn[]=$row['goods_sn'];
	}
	$smarty->assign('goods_sn',$fetch_goods_sn);
	$content=$smarty->fetch('erp_goods_sn_tips.htm');
	$result['error']=0;
	$result['content']=$content;
	$result['num']=count($fetch_goods_sn);
	die($json->encode($result));
}
elseif ($_REQUEST['act'] == 'goods_name_tips')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	$goods_name=trim(urldecode($_REQUEST['goods_name']));
	$fetch_goods_name=array();
	$sql="select g.goods_name from ".$GLOBALS['ecs']->table('goods')." as g ";
	$sql.=" where g.goods_name like '%".$goods_name."%' order by goods_id desc limit 10";
	$res=$GLOBALS['db']->query($sql);
	while($row=mysql_fetch_assoc($res))
	{
		$fetch_goods_name[]=$row['goods_name'];
	}
	$smarty->assign('goods_name',$fetch_goods_name);
	$content=$smarty->fetch('erp_goods_name_tips.htm');
	$result['error']=0;
	$result['content']=$content;
	$result['num']=count($fetch_goods_name);
	die($json->encode($result));
}
elseif ($_REQUEST['act'] == 'update_warehousing')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_warehouse_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$warehousing_id=isset($_REQUEST['warehousing_id']) &&intval($_REQUEST['warehousing_id'])>0?intval($_REQUEST['warehousing_id']):0;
	$goods_sn=isset($_REQUEST['goods_sn'])?trim($_REQUEST['goods_sn']):'';
	$goods_name=isset($_REQUEST['goods_name'])?trim($_REQUEST['goods_name']):'';
	$goods_barcode=isset($_REQUEST['goods_barcode'])?trim($_REQUEST['goods_barcode']):'';
	$ids=isset($_REQUEST['ids'])?trim($_REQUEST['ids']):'';
	$dynamic_attrs=isset($_REQUEST['dynamic_attrs'])?trim($_REQUEST['dynamic_attrs']):'';
	$goods_qty=isset($_REQUEST['goods_qty']) &&intval($_REQUEST['goods_qty'])>0?intval($_REQUEST['goods_qty']):1;
	if(empty($warehousing_id))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	if(empty($goods_sn) &&empty($goods_name) &&empty($goods_barcode))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_warehousing_goods_sn_or_goods_name_or_goods_barcode_required'];
		die($json->encode($result));
	}
	$warehousing_info=is_warehousing_exist($warehousing_id);
	if(!$warehousing_info)
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_warehousing_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if(!is_agency_warehousing($warehousing_id,$agency_id))
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_warehousing_no_agency_access'];
		die($json->encode($result));
	}
	if($warehousing_info['status']!=1)
	{
		$result['error']=6;
		$result['message']=$_LANG['erp_warehousing_no_acts'];
		die($json->encode($result));
	}
	if(!is_admin_warehousing($warehousing_id ,erp_get_admin_id()))
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_warehousing_no_access'];
		die($json->encode($result));
	}
	if($warehousing_info['status']!=1)
	{
		$result['error']=6;
		$result['message']=$_LANG['erp_warehousing_no_acts'];
		die($json->encode($result));
	}
	if(!empty($goods_sn))
	{
		$goods_id=check_goods_sn($goods_sn);
		if(empty($goods_id))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_warehousing_wrong_goods_sn'];
			die($json->encode($result));
		}
	}
	elseif(!empty($goods_name))
	{
		$goods_id=check_goods_name($goods_name);
		if(empty($goods_id))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_warehousing_wrong_goods_name'];
			die($json->encode($result));
		}
	}
	elseif(!empty($goods_barcode))
	{
		$barcode_id=check_goods_barcode($goods_barcode);
		if(empty($barcode_id))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_warehousing_wrong_goods_barcode'];
			die($json->encode($result));
		}
		$goods_id=get_goods_id_by_barcode_id($barcode_id);
		$ids=empty($ids)?get_attr_id_list_by_barcode_id($barcode_id) : $ids;
	}
	if(!empty($dynamic_attrs))
	{
		$dynamic_attrs=explode(',',trim($dynamic_attrs,','));
		$ids=explode(',',trim($ids,','));
		foreach($dynamic_attrs as $key =>$dynamic_attr)
		{
			$dynamic_attr=explode('|',$dynamic_attr);
			$attr_id=$dynamic_attr[0];
			$attr_value=$dynamic_attr[1];
			$goods_attr_id=0;
			if(!empty($attr_value))
			{
				$sql="select attr_values from ".$ecs->table('attribute')." where attr_id='".$attr_id."'";
				$attr_values=$db->getOne($sql);
				$attr_values=explode("\r\n",$attr_values);
				if(!in_array($attr_value,$attr_values))
				{
					$attr_values[]=$attr_value;
				}
				$attr_values=implode("\r\n",$attr_values);
				$sql="update ".$ecs->table('attribute')." set attr_values='".$attr_values."' where attr_id='".$attr_id."'";
				$db->query($sql);
				$sql="select goods_attr_id from ".$ecs->table('goods_attr')." where goods_id='".$goods_id."' and attr_id='".$attr_id."' and attr_value='".$attr_value."'";
				$goods_attr_id=$db->getOne($sql);
				if(empty($goods_attr_id))
				{
					$sql="insert into ".$ecs->table('goods_attr')." set goods_id='".$goods_id."',attr_id='".$attr_id."',attr_value='".$attr_value."'";
					$res=$db->query($sql);
					$goods_attr_id=$db->insert_id();
				}
			}
			if($goods_attr_id >0)
			{
				$ids[$key]=$goods_attr_id;
			}
		}
		update_attr($goods_id);
		update_attr_stock($goods_id);
		auto_fix_barcode($goods_id);
		$ids=implode(',',$ids);
	}
	if(empty($ids))
	{
		$sql="select warehousing_item_id from ".$ecs->table('erp_warehousing_item')." where warehousing_id='".$warehousing_id."' and goods_id='".$goods_id."'";
		$warehousing_item_id=$db->getOne($sql);
		if(empty($warehousing_item_id))
		{
			$attr_id=get_attr_id($goods_id);
			$sql="insert into ".$ecs->table('erp_warehousing_item')." set warehousing_id='".$warehousing_id."',goods_id='".$goods_id."',attr_id='".$attr_id."', expected_qty='0',received_qty='".$goods_qty."'";
			$db->query($sql);
			$result['error']=0;
			die($json->encode($result));
		}
		else
		{
			$sql="update ".$ecs->table('erp_warehousing_item')." set received_qty=received_qty+'".$goods_qty."' where warehousing_item_id='".$warehousing_item_id."'";
			$db->query($sql);
			$result['error']=0;
			die($json->encode($result));
		}
	}
	else
	{
		$ids=trim($ids,',');
		$attr_id=get_attr_id($goods_id,$ids);
		$sql="select warehousing_item_id from ".$ecs->table('erp_warehousing_item')." where warehousing_id='".$warehousing_id."' and goods_id='".$goods_id."' and attr_id='".$attr_id."'";
		$warehousing_item_id=$db->getOne($sql);
		if(empty($warehousing_item_id))
		{
			$sql="insert into ".$ecs->table('erp_warehousing_item')." set warehousing_id='".$warehousing_id."',goods_id='".$goods_id."',attr_id='".$attr_id."', expected_qty='0',received_qty='".$goods_qty."'";
			$db->query($sql);
			$result['error']=0;
			die($json->encode($result));
		}
		else
		{
			$sql="update ".$ecs->table('erp_warehousing_item')." set received_qty=received_qty+'".$goods_qty."' where warehousing_item_id='".$warehousing_item_id."'";
			$db->query($sql);
			$result['error']=0;
			die($json->encode($result));
		}
	}
}
elseif ($_REQUEST['act'] == 'search_goods_warehousing')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_warehouse_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}

	$goods_sn=isset($_REQUEST['goods_sn'])?trim($_REQUEST['goods_sn']):'';
	$goods_name=isset($_REQUEST['goods_name'])?trim($_REQUEST['goods_name']):'';
	$goods_barcode=isset($_REQUEST['goods_barcode'])?trim($_REQUEST['goods_barcode']):'';
	$goods_qty=isset($_REQUEST['goods_qty']) &&intval($_REQUEST['goods_qty'])>0?intval($_REQUEST['goods_qty']):1;
	
	if(empty($goods_sn) &&empty($goods_name) &&empty($goods_barcode))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_warehousing_goods_sn_or_goods_name_or_goods_barcode_required'];
		die($json->encode($result));
	}
	
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if(!is_agency_warehousing($warehousing_id,$agency_id))
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_warehousing_no_agency_access'];
		die($json->encode($result));
	}
	
	if(!empty($goods_sn))
	{
		$goods_id=check_goods_sn($goods_sn);
		if(empty($goods_id))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_warehousing_wrong_goods_sn'];
			die($json->encode($result));
		}
	}
	elseif(!empty($goods_name))
	{
		$goods_name = urldecode($goods_name);
		$goods_id=check_goods_name($goods_name);
		
		if(empty($goods_id))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_warehousing_wrong_goods_name'];
			die($json->encode($result));
		}
	}
	elseif(!empty($goods_barcode))
	{
		$barcode_id=check_goods_barcode($goods_barcode);
		if(empty($barcode_id))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_warehousing_wrong_goods_barcode'];
			die($json->encode($result));
		}
		$goods_id=get_goods_id_by_barcode_id($barcode_id);
		$ids=empty($ids)?get_attr_id_list_by_barcode_id($barcode_id) : $ids;
	}
	if(empty($ids))
	{
		$goods_sn=$goods_sn;
		$sql="select cat_id,goods_id,goods_sn,goods_name,goods_thumb,goods_img from ";
		$sql.=$GLOBALS['ecs']->table('goods')." where goods_id='".$goods_id."'";
		$goods_details=$GLOBALS['db']->getRow($sql);
		if(!empty($goods_details['goods_thumb']))
		{
			$goods_details['goods_thumb']='../'.$goods_details['goods_thumb'];
		}
		if(!empty($goods_details['goods_img']))
		{
			$goods_details['goods_img']='../'.$goods_details['goods_img'];
		}
		$goods_details['goods_qty'] = $goods_qty;
		$item['goods_details']=$goods_details;
		$item['goods_id']=$goods_details['goods_id'];
		$item['goods_attr']=goods_attr($item['goods_id']);
		$attr_info=get_attr_info($item['attr_id']);
		if(!empty($attr_info['attr_info']))
		{
			foreach($attr_info['attr_info'] as $k =>$attr)
			{
				$attrs=$warehousing_item_info[$key]['goods_attr'][$attr['attr_id']];
				foreach($attrs as $i =>$a)
				{
					if($a['goods_attr_id']==$attr['goods_attr_id'])
					{
						$attrs[$i]['selected']=1;
					}
					else
					{
						$attrs[$i]['selected']=0;
					}
				}
				$attr_info['attr_info'][$k]['attrs']=$attrs;
			}
		}
		$item['selected_attr']=$attr_info['attr_info'];
		$sql="select gb.barcode from ".$GLOBALS['ecs']->table('erp_goods_barcode')." as gb,".$GLOBALS['ecs']->table('erp_goods_attr')." as ga where ga.attr_id='".$item['attr_id']."' and ga.barcode_id=gb.id limit 1";
		$item['barcode']=$GLOBALS['db']->getOne($sql);

		$result['error']= 0;
		$result['item']= $item;

		die($json->encode($result));
	}
	else
	{
		$ids=trim($ids,',');
		$attr_id=get_attr_id($goods_id,$ids);
		$sql="select warehousing_item_id from ".$ecs->table('erp_warehousing_item')." where warehousing_id='".$warehousing_id."' and goods_id='".$goods_id."' and attr_id='".$attr_id."'";
		$warehousing_item_id=$db->getOne($sql);
		if(empty($warehousing_item_id))
		{
			$sql="insert into ".$ecs->table('erp_warehousing_item')." set warehousing_id='".$warehousing_id."',goods_id='".$goods_id."',attr_id='".$attr_id."', expected_qty='0',received_qty='".$goods_qty."'";
			$db->query($sql);
			$result['error']=0;
			die($json->encode($result));
		}
		else
		{
			$sql="update ".$ecs->table('erp_warehousing_item')." set received_qty=received_qty+'".$goods_qty."' where warehousing_item_id='".$warehousing_item_id."'";
			$db->query($sql);
			$result['error']=0;
			die($json->encode($result));
		}
	}
}
elseif ($_REQUEST['act'] == 'post_warehousing')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_warehouse_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$warehousing_id=isset($_REQUEST['warehousing_id']) &&intval($_REQUEST['warehousing_id'])>0?intval($_REQUEST['warehousing_id']):0;
	if(empty($warehousing_id))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$warehousing_info=is_warehousing_exist($warehousing_id);
	if(!$warehousing_info)
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_warehousing_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if(!is_agency_warehousing($warehousing_id,$agency_id))
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_warehousing_no_agency_access'];
		die($json->encode($result));
	}
	if($warehousing_info['status']!=1)
	{
		$result['error']=6;
		$result['message']=$_LANG['erp_warehousing_no_acts'];
		die($json->encode($result));
	}
	if(!is_admin_warehousing($warehousing_id ,erp_get_admin_id()))
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_warehousing_no_access'];
		die($json->encode($result));
	}
	if($warehousing_info['status']!=1)
	{
		$result['error']=6;
		$result['message']=$_LANG['erp_warehousing_no_acts'];
		die($json->encode($result));
	}
	if(empty($warehousing_info['operator']))
	{
		$result['error']=1;
		$result['message']='提交審核前請先填寫操作員。';
		die($json->encode($result));
	}
	$error=check_warehousing_integrity($warehousing_id);
	if($error>0)
	{
		$result['error']=4;
		$result['message']=$_LANG['erp_warehousing_not_completed'];
		die($json->encode($result));
	}
	if(lock_table($warehousing_id,'warehousing','post_to_approve'))
	{
		$sql="update ".$ecs->table('erp_warehousing')." set status='2',post_by='".erp_get_admin_id()."',post_time='".gmtime()."' where warehousing_id='".$warehousing_id."'";
		if($db->query($sql))
		{
			$result['error']=0;
			die($json->encode($result));
		}
	}
	else
	{
		$result['error']=-1;
		$result['message']=$_LANG['erp_warehousing_no_accessibility'];
		die($json->encode($result));
	}
}
elseif ($_REQUEST['act'] == 'post_warehousing_create')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	
	if(empty($_REQUEST['operator']))
	{
		$result['error']=1;
		$result['message']='提交審核前請先填寫操作員。';
		die($json->encode($result));
	}

	if ($_REQUEST['act_step'] == 'submit') 
	{
		if(!admin_priv('erp_warehouse_manage','',false))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_no_permit'];
			die($json->encode($result));
		}
	}

	if ($_REQUEST['act_step'] == 'one_click_finish') 
	{
		if(!admin_priv('erp_warehouse_manage','',false) || !admin_priv('erp_warehouse_approve','',false))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_no_permit'];
			die($json->encode($result));
		}
	}

	$order_id = $_REQUEST['order_id'];
	$warehousing_from = $_REQUEST['input_warehousing_from'];
	$warehousing_style_id = $_REQUEST['select_warehousing_style'];
	$input_received_qty = $_REQUEST['input_received_qty'];

	if (empty($input_received_qty)) {
		$result['error']=1;
		$result['message']='沒有入貨產品資訊';
		die($json->encode($result));
	}

	$warehouse_id = $_REQUEST['select_warehouse'];
	if (empty($warehouse_id)) {
		$result['error']=1;
		$result['message']='請選擇收貨貨倉';
		die($json->encode($result));
	}

	$amount_add = 0;
	$exchange_add = 0;
	$po_info = $db->getRow("SELECT order_sn, type, supplier_id, currency_code FROM " . $ecs->table('erp_order') . " WHERE order_id = $order_id");

	$warehousing_id=add_warehousing($warehouse_id);
	
	if(lock_table($warehousing_id,'warehousing','edit'))
	{
		if ($warehousing_style_id != 1) {
			$order_id = 0;
		}
		$sql="update ".$ecs->table('erp_warehousing')." set warehousing_style_id = '".$warehousing_style_id."', order_id ='".$order_id."',  warehousing_from='".$warehousing_from."' where warehousing_id='".$warehousing_id."'";
		$db->query($sql);
		//$sql="delete from ".$ecs->table('erp_warehousing_item')." where warehousing_id='".$warehousing_id."'";
		//$db->query($sql);

		if ($warehousing_style_id==1) {
			foreach ($input_received_qty as $received_item) {
				$goods_id = str_replace('input_received_qty_','',$received_item['name']);
				$received_qty = $received_item['value'];
				$sql="select order_item_id,goods_id,attr_id,(order_qty-warehousing_qty) as expected_qty,(price * order_qty - cn_amount) / order_qty as price, price as unit_price, original_price from ".$GLOBALS['ecs']->table('erp_order_item')." where order_id='".$order_id."' and goods_id = '".$goods_id."' and order_qty>warehousing_qty";
				$res=$GLOBALS['db']->query($sql);
				while(list($order_item_id,$goods_id,$attr_id,$expected_qty,$price,$unit_price,$original_price)=mysql_fetch_row($res))
				{
					if ($expected_qty < $received_qty) {
						$result['error']=1;
						$result['message']='實收數量大於應收數量';
						die($json->encode($result));
					}
					$sql="insert into ".$GLOBALS['ecs']->table('erp_warehousing_item')." set warehousing_id='".$warehousing_id."'";
					$sql.=",goods_id='".$goods_id."',attr_id='".$attr_id."',expected_qty='".$expected_qty."',received_qty='".$received_qty."',price='".$price."',account_payable=0,paid_amount=0,order_item_id='".$order_item_id."'";
					$GLOBALS['db']->query($sql);
					if ($po_info['type'] == '0' || $po_info['type'] == '2') { // 正常採購, 寄賣
						$amount_add = bcadd($amount_add, bcmul($original_price, $received_qty, 2), 2);
						$exchange_add = bcadd($exchange_add, bcmul($unit_price, $received_qty, 2), 2);
					}
				}
			}
		}else{
			foreach ($input_received_qty as $received_item) {
				$goods_id = str_replace('input_received_qty_','',$received_item['name']);
				$received_qty = $received_item['value'];
				$attr_id=get_attr_id($goods_id);
				$sql="insert into ".$ecs->table('erp_warehousing_item')." set warehousing_id='".$warehousing_id."',goods_id='".$goods_id."',attr_id='".$attr_id."', expected_qty='0',received_qty='".$received_qty."'";
				$db->query($sql);
			}
		}
	}

	// post_to_approve directly
	if(lock_table($warehousing_id,'warehousing','post_to_approve'))
	{
		if (bccomp($exchange_add, 0) == 1) {
			$accountingController = new Yoho\cms\Controller\AccountingController();
			$param = [
				'from_actg_type_id' => $accountingController->getAccountingType(['actg_type_code' => '14001', 'enabled' => 1], 'actg_type_id'), // Inventory (Owned)
				'to_actg_type_id' 	=> $accountingController->getAccountingType(['actg_type_code' => '45000', 'enabled' => 1], 'actg_type_id'), // Cost of Sales
				'currency_code' 	=> $po_info['currency_code'],
				'amount'			=> $amount_add,
				'exchanged_amount'	=> $exchange_add,
				'erp_order_id'		=> $order_id,
				'warehousing_id'	=> $warehousing_id,
				'supplier_id'		=> $po_info['supplier_id'],
				'remark'			=> "採購訂單 $po_info[order_sn] 入貨"
			];
			$accountingController->insertDoubleEntryTransactions($param);
		}

		// check if order goods is all enough
		// get warehousing item
		$sql = "select goods_id from ".$ecs->table('erp_warehousing_item')." where warehousing_id = ".$warehousing_id." ";
		$warehousing_items = $db->getCol($sql);
		$orderProgressController->checkOrderAllInStock('PO',$warehousing_id,$warehousing_items);
		$sql="update ".$ecs->table('erp_warehousing')." set status='2',post_by='".erp_get_admin_id()."',post_time='".gmtime()."' where warehousing_id='".$warehousing_id."'";
		if($db->query($sql))
		{
			$result['error']=0;
			$result['warehousing_id']= $warehousing_id;
			die($json->encode($result));
		}
	}
	else
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_warehousing_no_accessibility'];
		die($json->encode($result));
	}
}
elseif ($_REQUEST['act'] == 'add_warehousing')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_warehouse_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$warehousing_id=add_warehousing();
	if($warehousing_id==-1)
	{
		$result['error']=1;
		$result['message']=$GLOBALS['_LANG']['erp_warehousing_without_valid_warehouse'];
		die($json->encode($result));
	}
	else
	{
		$result['error']=0;
		$result['warehousing_id']=$warehousing_id;
		die($json->encode($result));
	}
}
elseif ($_REQUEST['act'] == 'withdrawal_to_edit')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_warehouse_approve','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$warehousing_id=isset($_REQUEST['warehousing_id']) &&intval($_REQUEST['warehousing_id'])>0?intval($_REQUEST['warehousing_id']):0;
	if(empty($warehousing_id))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$warehousing_info=is_warehousing_exist($warehousing_id);
	if(!$warehousing_info)
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_warehousing_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if(!is_agency_warehousing($warehousing_id,$agency_id))
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_warehousing_no_agency_access'];
		die($json->encode($result));
	}
	if($warehousing_info['status']!=2)
	{
		$result['error']=6;
		$result['message']=$_LANG['erp_warehousing_no_acts'];
		die($json->encode($result));
	}
	$sql="update ".$ecs->table('erp_warehousing')." set status='1' where warehousing_id='".$warehousing_id."'";
	$db->query($sql);
	$result['error']=0;
	die($json->encode($result));
}
elseif ($_REQUEST['act'] == 'approve_pass')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_warehouse_approve','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$warehousing_id=isset($_REQUEST['warehousing_id']) &&intval($_REQUEST['warehousing_id'])>0?intval($_REQUEST['warehousing_id']):0;
	$approve_remark=isset($_REQUEST['approve_remark']) ?trim($_REQUEST['approve_remark']): '';
	if(empty($warehousing_id))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$warehousing_info=is_warehousing_exist($warehousing_id);
	if(!$warehousing_info)
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_warehousing_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if(!is_agency_warehousing($warehousing_id,$agency_id))
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_warehousing_no_agency_access'];
		die($json->encode($result));
	}
	if($warehousing_info['status']!=2)
	{
		$result['error']=6;
		$result['message']=$_LANG['erp_warehousing_no_acts'];
		die($json->encode($result));
	}
	if(lock_table($warehousing_id,'warehousing','approve'))
	{
		$GLOBALS['db']->query('START TRANSACTION');
		$warehousing_style_id=$warehousing_info['warehousing_style_id'];
		$warehouse_id=$warehousing_info['warehouse_id'];
		$order_id=$warehousing_info['order_id'];
		$warehousing_sn=$warehousing_info['warehousing_sn'];
		$warehousing_from=$warehousing_info['warehousing_from'];
		if(!empty($order_id))
		{
			$sql="select * from ".$ecs->table('erp_order')." where order_id='".$order_id."'";
			$res=$db->getRow($sql);
			$order_sn=$res['order_sn'];
			$supplier_id=$res['supplier_id'];
		}
		else
		{
			$order_sn='';
			$supplier_id=0;
		}
		$warehousing_item_info=get_warehousing_item_info($warehousing_id);
		if($warehousing_style_id==1)
		{
			foreach($warehousing_item_info as $warehousing_item)
			{
				$order_item_id=$warehousing_item['order_item_id'];
				$received_qty=$warehousing_item['received_qty'];
				$sql="update ".$ecs->table('erp_order_item')." set warehousing_qty=warehousing_qty+".$received_qty." where order_item_id='".$order_item_id."'";
				if(!$db->query($sql))
				{
					$GLOBALS['db']->query('ROLLBACK');
					$result['error']=9;
					$result['message']=$_LANG['erp_warehousing_approve_pass_failed'];
					die($json->encode($result));
				}
			}
		}
		foreach($warehousing_item_info as $warehousing_item)
		{
			$goods_id=$warehousing_item['goods_id'];
			$attr_id=$warehousing_item['attr_id'];
			$qty=$warehousing_item['received_qty'];
			$price=$warehousing_item['price'];
			$sql="select stock from ".$ecs->table('erp_stock')." where goods_id='".$goods_id."' and goods_attr_id='".$attr_id."' and warehouse_id='".$warehouse_id."' order by stock_id desc limit 1";
			$stock=$db->getOne($sql);
			$stock=!empty($stock)?$stock: 0;
			$sql="insert into ".$ecs->table('erp_stock')." set goods_id='".$goods_id."',goods_attr_id='".$attr_id."',qty='".$qty."',price='".$price."',amount=qty*price,act_time='".gmtime()."',warehouse_id='".$warehouse_id."'";
			$sql.=", w_d_name='".$warehousing_from."', w_d_style_id='".$warehousing_style_id."',order_sn='".$order_sn."',stock_style='w',w_d_sn='".$warehousing_sn."',stock=".($stock+$qty).",act_date='".local_date('Y-m-d',gmtime())."'";
			
			//auto update related repair order status-----
			$orderController = new Yoho\cms\Controller\OrderController();
			$ros = $orderController->getRepairOrderByProductId($goods_id);
			if(count($ros)>0){
				$updateCounter = 0;
				foreach($ros as $ro){
					//Just update received qty for repair orders
					if($updateCounter >= $qty)
						break;
					//proceed update
					if($ro['repair_order_status']==$orderController::DB_REPAIR_ORDER_STATUS_REPAIRING){
						$orderController->updateRepairOrderStatus($ro['repair_id'],$orderController::DB_REPAIR_ORDER_STATUS_REPAIRED);
						$updateCounter++;
					}
				}
			}
			//auto update related repair order status-----

			if(!$db->query($sql))
			{
				$GLOBALS['db']->query('ROLLBACK');
				$result['error']=9;
				$result['message']=$_LANG['erp_warehousing_approve_pass_failed'];
				die($json->encode($result));
			}
		}
		// Calculate Average 現貨 cost
		foreach($warehousing_item_info as $warehousing_item)
		{
			$goods_id=$warehousing_item['goods_id'];
			$qty=$warehousing_item['received_qty'];
			$price=$warehousing_item['price'];
			//if ($price != 0) // do not calculate if 0 cost
			//{
				$sql = "SELECT cost FROM " . $ecs->table('goods') . " WHERE goods_id = '".$goods_id."'";
				$original_cost = $db->getOne($sql);
				//TODO: clarify business logic, shoudl the non sellable warehouse be excluded?
				$sql = "SELECT sum(goods_qty) as goods_number FROM " . $ecs->table('erp_goods_attr') . " WHERE goods_id = '".$goods_id."'";
				$original_qty = $db->getOne($sql);
				$new_cost = ($original_cost * $original_qty + $price * $qty) / ($original_qty + $qty);

				if ($original_qty + $qty == 0) {
					$new_cost = $price;
				}

				if (is_nan($new_cost)) $new_cost = $original_cost;
				if ($original_cost != $new_cost)
				{
					$sql = "UPDATE " . $ecs->table('goods') . " SET cost = " . $new_cost . " WHERE goods_id = '" . $goods_id . "'";
					if(!$db->query($sql))
					{
						$GLOBALS['db']->query('ROLLBACK');
						$result['error']=9;
						$result['message']=$_LANG['erp_warehousing_approve_pass_failed'].__LINE__;
						die($json->encode($result));
					}
					// Log the cost changes
					$GLOBALS['db']->autoExecute($GLOBALS['ecs']->table('goods_cost_log'), array(
						'goods_id' => $goods_id,
						'old_cost' => $original_cost,
						'new_cost' => $new_cost,
						'update_type' => 1,
						'update_time' => gmtime(),
						'admin_id' => $_SESSION['admin_id'],
						'warehousing_id' => $warehousing_id,
						'warehousing_sn' => $warehousing_info['warehousing_sn']
					), 'INSERT');
					// Check for abnormal cost (different is greater than 5%)
					$difference = ($original_cost == 0) ? 1 : (($price - $original_cost) / $original_cost);
					if (abs($difference) > 0.05)
					{
						$GLOBALS['db']->autoExecute($GLOBALS['ecs']->table('abnormal_cost_log'), array(
							'goods_id' => $goods_id,
							'difference' => $difference,
							'original_cost' => $original_cost,
							'warehousing_cost' => $price,
							'erp_order_id' => $order_id,
							'erp_order_sn' => $order_sn,
							'warehousing_id' => $warehousing_id,
							'warehousing_sn' => $warehousing_info['warehousing_sn'],
							'supplier_id' => $supplier_id,
						), 'INSERT');
					}
				}
			//}
		}
		foreach($warehousing_item_info as $warehousing_item)
		{
			$goods_id=$warehousing_item['goods_id'];
			$attr_id=$warehousing_item['attr_id'];
			$qty=$warehousing_item['received_qty'];
			if(empty($attr_id))
			{
				$sql="select attr_id from ".$ecs->table('erp_goods_attr')." where goods_id='".$goods_id."' and goods_attr_id_list='$$'";
				$attr_id=$db->getOne($sql);
			}
			$sql="update ".$ecs->table('erp_goods_attr')." set goods_qty=goods_qty+".$qty." where goods_id='".$goods_id."' and attr_id='".$attr_id."'";
			if(!$db->query($sql))
			{
				$GLOBALS['db']->query('ROLLBACK');
				$result['error']=9;
				$result['message']=$_LANG['erp_warehousing_approve_pass_failed'];
				die($json->encode($result));
			}
			$sql="update ".$ecs->table('erp_goods_attr_stock')." set goods_qty =goods_qty +".$qty." where goods_id='".$goods_id."' and attr_id='".$attr_id."' and warehouse_id='".$warehouse_id."'";
			if(!$db->query($sql))
			{
				$GLOBALS['db']->query('ROLLBACK');
				$result['error']=9;
				$result['message']=$_LANG['erp_warehousing_approve_pass_failed'];
				die($json->encode($result));
			}
		}
		foreach($warehousing_item_info as $warehousing_item)
		{
			$goods_id = $warehousing_item['goods_id'];
			$received_quantity = $warehousing_item['received_qty'];
			// Back in stock notification
			$new_quantity = $db->getOne("SELECT SUM(goods_qty) FROM " . $ecs->table('erp_goods_attr') . " WHERE goods_id='".$goods_id."'");
			//TODO: why compare total qty with the warehousing order received qty???
			if ($new_quantity == $received_quantity)
			{
				if (@include_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_notifications.php'))
				{
					notify_goods_stock($db->getRow("SELECT goods_id, goods_name, goods_sn FROM " . $ecs->table('goods') . " WHERE goods_id = '$goods_id'"), true);
				}
				else
				{
					error_log(__FILE__.':'.__LINE__.' Cannot include lib_notifications.php');
				}
			}
		}
		$sql="update ".$ecs->table('erp_warehousing')." set status='3',approve_time='".gmtime()."',approved_by='".erp_get_admin_id()."',approve_remark='".$approve_remark."' where warehousing_id='".$warehousing_id."'";
		if(!$db->query($sql))
		{
			$GLOBALS['db']->query('ROLLBACK');
			$result['error']=9;
			$result['message']=$_LANG['erp_delivery_approve_pass_failed'];
			die($json->encode($result));
		}
		else
		{
			$GLOBALS['db']->query('COMMIT');
			$result['error']=0;
			die($json->encode($result));
		}
	}
	else
	{
		$result['error']=-1;
		$result['message']=$_LANG['erp_warehousing_no_accessibility'];
		die($json->encode($result));
	}
}
elseif ($_REQUEST['act'] == 'approve_reject')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_warehouse_approve','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$warehousing_id=isset($_REQUEST['warehousing_id']) &&intval($_REQUEST['warehousing_id'])>0?intval($_REQUEST['warehousing_id']):0;
	$approve_remark=isset($_REQUEST['approve_remark']) ?trim($_REQUEST['approve_remark']):'';
	if(empty($warehousing_id))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$warehousing_info=is_warehousing_exist($warehousing_id);
	if(!$warehousing_info)
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_warehousing_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if(!is_agency_warehousing($warehousing_id,$agency_id))
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_warehousing_no_agency_access'];
		die($json->encode($result));
	}
	if($warehousing_info['status']!=2)
	{
		$result['error']=6;
		$result['message']=$_LANG['erp_warehousing_no_acts'];
		die($json->encode($result));
	}
	if(lock_table($warehousing_id,'warehousing','approve'))
	{
		$sql="update ".$ecs->table('erp_warehousing')." set status='5',approve_remark='".$approve_remark."' where warehousing_id='".$warehousing_id."'";
		if($db->query($sql))
		{
			$result['error']=0;
			die($json->encode($result));
		}
	}
	else
	{
		$result['error']=-1;
		$result['message']=$_LANG['erp_warehousing_no_accessibility'];
		die($json->encode($result));
	}
}
elseif ($_REQUEST['act'] == 'update_attr')
{
	if(!admin_priv('erp_warehouse_manage','',false))
	{
		$link[] = array('text'=>$_LANG['go_back'],'href'=>'javascript:history.back(-1)');
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
	$warehousing_id=isset($_REQUEST['warehousing_id']) &&intval($_REQUEST['warehousing_id'])>0?intval($_REQUEST['warehousing_id']):0;
	$warehousing_item_id=isset($_REQUEST['warehousing_item_id']) &&intval($_REQUEST['warehousing_item_id'])>0?intval($_REQUEST['warehousing_item_id']):0;
	if(empty($warehousing_id))
	{
		$link[] = array('text'=>$_LANG['go_back'],'href'=>'javascript:history.back(-1)');
		sys_msg($_LANG['erp_wrong_parameter'],0,$link);
	}
	$warehousing_info=is_warehousing_exist($warehousing_id);
	if(!$warehousing_info)
	{
		$link[] = array('text'=>$_LANG['go_back'],'href'=>'javascript:history.back(-1)');
		sys_msg($_LANG['erp_warehousing_not_exist'],0,$link);
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if(!is_agency_warehousing($warehousing_id,$agency_id))
	{
		$link[] = array('text'=>$_LANG['go_back'],'href'=>'javascript:history.back(-1)');
		sys_msg($_LANG['erp_warehousing_no_agency_access'],0,$link);
	}
	if($warehousing_info['status']!=1)
	{
		$link[] = array('text'=>$_LANG['go_back'],'href'=>'javascript:history.back(-1)');
		sys_msg($_LANG['erp_warehousing_no_acts'],0,$link);
	}
	if(lock_table($warehousing_id,'warehousing','edit'))
	{
		$db->query('START TRANSACTION');
		$sql="select wi.goods_id,ga.goods_attr_id_list,wi.order_item_id from ".$ecs->table('erp_warehousing_item')." as wi,".$ecs->table('erp_goods_attr')." as ga where wi.warehousing_item_id='".$warehousing_item_id."' and wi.attr_id=ga.attr_id for update";
		$goods=$db->getRow($sql);
		$goods_id=$goods['goods_id'];
		$order_item_id=$goods['order_item_id'];
		$goods_attr_id_list=array();
		$sql="select goods_attr_id,attr_id from ".$ecs->table('goods_attr')." where goods_attr_id in (".$goods['goods_attr_id_list'].")";
		$res=$db->query($sql);
		while(list($goods_attr_id,$attr_id)=mysql_fetch_row($res))
		{
			$goods_attr_id_list[$attr_id]=$goods_attr_id;
		}
		foreach($_REQUEST as $key =>$value)
		{
			if(strpos($key,'input_attr_')!== false &&!empty($value))
			{
				$attr_id=str_replace('input_attr_','',$key);
				$attr_value=$value;
				$sql="select attr_values from ".$ecs->table('attribute')." where attr_id='".$attr_id."' for update";
				$attr_values=$db->getOne($sql);
				$attr_values=explode("\r\n",$attr_values);
				if(!in_array($attr_value,$attr_values))
				{
					$attr_values[]=$attr_value;
				}
				$attr_values=implode("\r\n",$attr_values);
				$sql="update ".$ecs->table('attribute')." set attr_values='".$attr_values."' where attr_id='".$attr_id."'";
				if(!$db->query($sql))
				{
					$db->query('ROLLBACK');
				}
				$sql="select goods_attr_id from ".$ecs->table('goods_attr')." where goods_id='".$goods_id."' and attr_id='".$attr_id."' and attr_value='".$attr_value."'";
				$goods_attr_id=$db->getOne($sql);
				if(empty($goods_attr_id))
				{
					$sql="insert into ".$ecs->table('goods_attr')." set goods_id='".$goods_id."',attr_id='".$attr_id."',attr_value='".$attr_value."'";
					if(!$db->query($sql))
					{
						$db->query('ROLLBACK');
					}
					$goods_attr_id=$db->insert_id();
				}
				if(!update_attr($goods_id))
				{
					$db->query('ROLLBACK');
				}
				if(!update_attr_stock($goods_id))
				{
					$db->query('ROLLBACK');
				}
				$goods_attr_id_list[$attr_id]=$goods_attr_id;
				$attr_id=get_attr_id($goods_id,$goods_attr_id_list);
				$sql="update ".$ecs->table('erp_warehousing_item')." set attr_id='".$attr_id."' where warehousing_item_id='".$warehousing_item_id."'";
				if(!$db->query($sql))
				{
					$db->query('ROLLBACK');
				}
				if($order_item_id>0)
				{
					$sql="update ".$ecs->table('erp_order_item')." set attr_id='".$attr_id."' where order_item_id='".$order_item_id."'";
					if(!$db->query($sql))
					{
						$db->query('ROLLBACK');
					}
				}
				break;
			}
			elseif(strpos($key,'select_attr_')!== false &&!empty($value))
			{
				$attr_id=str_replace('select_attr_','',$key);
				$goods_attr_id=intval($value);
				$goods_attr_id_list[$attr_id]=$goods_attr_id;
				$attr_id=get_attr_id($goods_id,$goods_attr_id_list);
				$sql="update ".$ecs->table('erp_warehousing_item')." set attr_id='".$attr_id."' where warehousing_item_id='".$warehousing_item_id."'";
				if(!$db->query($sql))
				{
					$db->query('ROLLBACK');
				}
				if($order_item_id>0)
				{
					$sql="update ".$ecs->table('erp_order_item')." set attr_id='".$attr_id."' where order_item_id='".$order_item_id."'";
					if(!$db->query($sql))
					{
						$db->query('ROLLBACK');
					}
				}
				break;
			}
		}
		$db->query('COMMIT');
		ecs_header('Location: erp_warehousing_manage.php?act=edit&id='.$warehousing_id."\n");
	}
	else
	{
		$link[] = array('text'=>$_LANG['go_back'],'href'=>'javascript:history.back(-1)');
		sys_msg($_LANG['erp_warehousing_no_accessibility'],0,$link);
	}
}
elseif ($_REQUEST['act'] == 'batch_warehousing')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_warehouse_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$warehousing_id=intval($_REQUEST['warehousing_id']);
	if(isset($_FILES['file_batch_warehousing']))
	{
		$upload_file_info=$_FILES['file_batch_warehousing'];
		if($upload_file_info['type']!='text/plain')
		{
			$href="erp_warehouse_manage.php?act=edit_warehousing&id=".$warehousing_id;
			$text=$_LANG['erp_warehousing_return'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_warehousing_only_text_file'],0,$link);
		}
		$souce_file=$upload_file_info['tmp_name'];
		$upload_file=format_file($souce_file);
		$warehousing_info=check_file($upload_file);
		if(!is_array($warehousing_info))
		{
			if($warehousing_info==1)
			{
				$href="erp_warehouse_manage.php?act=edit_warehousing&id=".$warehousing_id;
				$text=$_LANG['erp_warehousing_return'];
				$link[] = array('href'=>$href,'text'=>$text);
				sys_msg($_LANG['erp_warehousing_invalid_file'],0,$link);
			}
			elseif($warehousing_info==2)
			{
				$href="erp_warehouse_manage.php?act=edit_warehousing&id=".$warehousing_id;
				$text=$_LANG['erp_warehousing_return'];
				$link[] = array('href'=>$href,'text'=>$text);
				sys_msg($_LANG['erp_warehousing_invalid_goods_sn'],0,$link);
			}
			elseif($warehousing_info==3)
			{
				$href="erp_warehouse_manage.php?act=edit_warehousing&id=".$warehousing_id;
				$text=$_LANG['erp_warehousing_return'];
				$link[] = array('href'=>$href,'text'=>$text);
				sys_msg($_LANG['erp_warehousing_invalid_goods_size'],0,$link);
			}
		}
		else
		{
			foreach($warehousing_info as $warehousing_item)
			{
				$goods_sn=$warehousing_item['goods_info']['goods_sn'];
				$goods_size=$warehousing_item['goods_info']['goods_size'];
				$warehousing_qty=$warehousing_item['qty'];
				$sql="select goods_id from ".$ecs->table('goods')." where goods_sn='".$goods_sn."'";
				$goods_id=$db->getOne($sql);
				if(lock_table($warehousing_id,'warehousing','edit'))
				{
					$sql="select warehousing_item_id from ".$ecs->table('erp_warehousing_item')." where warehousing_id='".$warehousing_id."' and goods_id='".$goods_id ."' and goods_size='".$goods_size."'";
					$warehousing_item_id=$db->getOne($sql);
					if(!empty($warehousing_item_id))
					{
						$sql="update ".$ecs->table('erp_warehousing_item')." set received_qty=received_qty+".$warehousing_qty." where warehousing_item_id='".$warehousing_item_id."'";
						$db->query($sql);
					}
					else
					{
						$sql="insert into ".$ecs->table('erp_warehousing_item')." set warehousing_id=".$warehousing_id.",goods_id='".$goods_id."',goods_size='".$goods_size."',received_qty=".$warehousing_qty;
						$db->query($sql);
					}
				}
				else
				{
					$href="erp_warehouse_manage.php?act=edit_warehousing&id=".$warehousing_id;
					$text=$_LANG['erp_warehousing_return'];
					$link[] = array('href'=>$href,'text'=>$text);
					sys_msg($_LANG['erp_warehousing_no_accessibility'],0,$link);
				}
			}
		}
	}
	$href="erp_warehouse_manage.php?act=edit_warehousing&id=".$warehousing_id;
	$text=$_LANG['erp_warehousing_return'];
	$link[] = array('href'=>$href,'text'=>$text);
	sys_msg($_LANG['erp_warehousing_batch_warehousing_success'],0,$link);
}
?>