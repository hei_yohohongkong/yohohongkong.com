<?php


define('IN_ECS', true);
require(dirname(__FILE__) . '/includes/init.php');
require_once(ROOT_PATH . 'includes/lib_order.php');
$orderController = new Yoho\cms\Controller\OrderController();
$userRankProgramController = new Yoho\cms\Controller\UserRankProgramController();
$adminuserController = new Yoho\cms\Controller\AdminUserController();

if ($_REQUEST['act'] == 'list'){
    $smarty->assign('ur_here', $_LANG['order_list_rank_program']);
    $rank_program = $userRankProgramController->get_programlist();
    $salesperson_options = $adminuserController->getAdminsByPriv(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_ORDERMSG, 'name', ['user_name' => 'value']);
    $smarty->assign('salesperson_options', $salesperson_options);
    //echo '<pre>', print_r($rank_program, true), '</pre>';
    $smarty->assign('rank_program_list', $rank_program);
    assign_query_info();
    $smarty->display('order_list_rank.htm');

} elseif ($_REQUEST['act'] == 'query') {
    $userRankProgramController->ajaxQueryAction();
}


?>