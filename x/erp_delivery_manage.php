<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_base.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_warehouse.php');
//require(dirname(__FILE__) .'/includes/ERP/cls/cls_date.php');
if($_REQUEST['act'] == 'list')
{
	if(admin_priv('erp_warehouse_view','',false) ||admin_priv('erp_warehouse_manage','',false) ||admin_priv('erp_warehouse_approve','',false))
	{
		// $page=isset($_REQUEST['page']) &&intval($_REQUEST['page']) >0 ?intval($_REQUEST['page']):1;
		// $warehouse_id=isset($_REQUEST['w_id']) &&intval($_REQUEST['w_id'])>0 ?intval($_REQUEST['w_id']):0;
		// $delivery_style_id=isset($_REQUEST['d_s_id']) &&intval($_REQUEST['d_s_id'])>0 ?intval($_REQUEST['d_s_id']):0;
		// $delivery_status=isset($_REQUEST['d_s']) &&intval($_REQUEST['d_s'])>0 ?intval($_REQUEST['d_s']):0;
		// $start_date=isset($_REQUEST['s_date'])?trim($_REQUEST['s_date']):'';
		// $end_date=isset($_REQUEST['e_date'])?trim($_REQUEST['e_date']):'';
		// $delivery_to=isset($_REQUEST['d_t'])?trim($_REQUEST['d_t']):'';
		// $delivery_sn=isset($_REQUEST['delivery_sn'])?trim($_REQUEST['delivery_sn']):'';
		// include('./includes/ERP/page.class.php');
		// $num_per_page=20;
		// $mode=1;
		// $page_bar_num=6;
		// $page_style="page_style";
		// $current_page_style="current_page_style";
		// $start=$num_per_page*($page-1);
		// $cls_date=new cls_date();
		// $start_time=!empty($start_date)?$cls_date->date_to_stamp($start_date): 0;
		// $end_time=!empty($end_date)?$cls_date->date_to_stamp($end_date):0;
		// $agency_id=get_admin_agency_id($_SESSION['admin_id']);
		// $paras=array(
		// 	'status'=>$delivery_status,
		// 	'start_time'=>$start_time,
		// 	'end_time'=>$end_time,
		// 	'delivery_style_id'=>$delivery_style_id,
		// 	'warehouse_id'=>$warehouse_id,
		// 	'delivery_to'=>$delivery_to,
		// 	'delivery_sn'=>$delivery_sn,
		// 	'agency_id'=>$agency_id
		// );
		// $total_num=get_delivery_count($paras);
		// $delivery_list=get_delivery_list($paras,$start,$num_per_page);
		// $smarty->assign('delivery_list',$delivery_list);
		// $url=fix_url($_SERVER['REQUEST_URI'],array(
		// 	'page'=>'',
		// 	'w_id'=>$warehouse_id,
		// 	'd_s_id'=>$delivery_style_id,
		// 	'd_s'=>$delivery_status,
		// 	's_date'=>$start_date,
		// 	'e_date'=>$end_date,
		// 	'd_t'=>$delivery_to,
		// 	'sn'=>$delivery_sn,
		// ));
		// $smarty->assign('w_id',$warehouse_id);
		// $smarty->assign('d_s_id',$delivery_style_id);
		// $smarty->assign('d_s',$delivery_status);
		// $smarty->assign('s_date',$start_date);
		// $smarty->assign('e_date',$end_date);
		// $smarty->assign('d_t',$delivery_to);
		// $smarty->assign('sn',$delivery_sn);
		// $pager=new page(array('total_data'=>$total_num,'data_per_page'=>$num_per_page,'url'=>$url,'mode'=>$mode,'page_bar_num'=>$page_bar_num,'page_style'=>$page_style,'current_page_style'=>$current_page_style));
		// $smarty->assign('pager',$pager->show());
		
		$delivery_list = get_delivery_listtable();
	    $smarty->assign('delivery_list', $delivery_list['delivery_list']);
	    $smarty->assign('filter',        $delivery_list['filter']);
	    $smarty->assign('record_count',  $delivery_list['record_count']);
	    $smarty->assign('page_count',    $delivery_list['page_count']);
		$smarty->assign('w_id',          empty($_REQUEST['w_id']) ? 0 : intval($_REQUEST['w_id']));
		$smarty->assign('d_s_id',        empty($_REQUEST['d_s_id']) ? 0 : intval($_REQUEST['d_s_id']));
		$smarty->assign('d_s',           empty($_REQUEST['d_s']) ? 0 : intval($_REQUEST['d_s']));
		$smarty->assign('s_date',        empty($_REQUEST['s_date']) ? '' : trim($_REQUEST['s_date']));
		$smarty->assign('e_date',        empty($_REQUEST['e_date']) ? '' : trim($_REQUEST['e_date']));
		$smarty->assign('d_t',           empty($_REQUEST['d_t']) ? '' : trim($_REQUEST['d_t']));
		$smarty->assign('sn',            empty($_REQUEST['delivery_sn']) ? '' : trim($_REQUEST['delivery_sn']));
		$smarty->assign('full_page',     1);
		
		$warehouse_list=get_warehouse_list(0,$agency_id,1);
		$smarty->assign('warehouse_list',$warehouse_list);
		$delivery_style_list=get_delivery_style_list(1);
		$smarty->assign('delivery_style_list',$delivery_style_list);
		$delivery_status=get_delivery_status();
		$smarty->assign('delivery_status',$delivery_status);
		if((admin_priv('erp_warehouse_manage','',false)))
		{
			$action_link = array('href'=>'javascript: add_delivery();','text'=>$_LANG['erp_goods_delivery']);
			$smarty->assign('action_link',$action_link);
		}
		$smarty->assign('ur_here',$_LANG['erp_goods_delivery_list']);
		assign_query_info();
		$smarty->display('erp_delivery_list.htm');
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif ($_REQUEST['act'] == 'query')
{
	if(admin_priv('erp_warehouse_view','',false) ||admin_priv('erp_warehouse_manage','',false) ||admin_priv('erp_warehouse_approve','',false))
	{
		if ((!empty($_REQUEST['s_date'])) && (strstr($_REQUEST['s_date'], '-') === false))
	    {
	        $_REQUEST['s_date'] = local_date('Y-m-d', $_REQUEST['s_date']);
		}
		if ((!empty($_REQUEST['e_date'])) && (strstr($_REQUEST['e_date'], '-') === false))
		{
	        $_REQUEST['e_date'] = local_date('Y-m-d', $_REQUEST['e_date']);
	    }
		
		$delivery_list = get_delivery_listtable();
		$smarty->assign('delivery_list', $delivery_list['delivery_list']);
		$smarty->assign('filter',        $delivery_list['filter']);
		$smarty->assign('record_count',  $delivery_list['record_count']);
		$smarty->assign('page_count',    $delivery_list['page_count']);
		$smarty->assign('w_id',          empty($_REQUEST['w_id']) ? 0 : intval($_REQUEST['w_id']));
		$smarty->assign('d_s_id',        empty($_REQUEST['d_s_id']) ? 0 : intval($_REQUEST['d_s_id']));
		$smarty->assign('d_s',           empty($_REQUEST['d_s']) ? 0 : intval($_REQUEST['d_s']));
		$smarty->assign('s_date',        empty($_REQUEST['s_date']) ? '' : trim($_REQUEST['s_date']));
		$smarty->assign('e_date',        empty($_REQUEST['e_date']) ? '' : trim($_REQUEST['e_date']));
		$smarty->assign('d_t',           empty($_REQUEST['d_t']) ? '' : trim($_REQUEST['d_t']));
		$smarty->assign('sn',            empty($_REQUEST['delivery_sn']) ? '' : trim($_REQUEST['delivery_sn']));

    	make_json_result($smarty->fetch('erp_delivery_list.htm'), '', array('filter' => $delivery_list['filter'], 'page_count' => $delivery_list['page_count']));
	}
	else
	{
		make_json_error($_LANG['erp_no_permit']);
	}
}
elseif($_REQUEST['act'] == 'view')
{
	if(admin_priv('erp_warehouse_view','',false) ||admin_priv('erp_warehouse_manage','',false) ||admin_priv('erp_warehouse_approve','',false))
	{
		$delivery_id=isset($_REQUEST['id']) &&intval($_REQUEST['id']) ?intval($_REQUEST['id']):0;
		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		if(!is_agency_delivery($delivery_id,$agency_id))
		{
			$href="erp_delivery_manage.php?act=list";
			$text=$_LANG['erp_delivery_return_to_delivery_list'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_delivery_no_agency_access'],0,$link);
		}
		$delivery_info=get_delivery_info($delivery_id);
		$delivery_item_info=get_delivery_item_info($delivery_id);
		$smarty->assign('delivery_info',$delivery_info);
		$smarty->assign('delivery_item_info',$delivery_item_info);
		$smarty->assign('act','view');
		if((admin_priv('erp_warehouse_approve','',false)))
		{
			$smarty->assign('price_visible','true');
		}
		$smarty->assign('ur_here',$_LANG['erp_delivery_view_delivery']);
		assign_query_info();
		$smarty->display('erp_delivery_info.htm');
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif ($_REQUEST['act'] == 'edit')
{
	if((admin_priv('erp_warehouse_manage','',false)))
	{
		$delivery_id=isset($_REQUEST['id']) &&intval($_REQUEST['id'])>0 ?intval($_REQUEST['id']) : 0;
		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		if(!is_admin_delivery($delivery_id,erp_get_admin_id()))
		{
			$href="erp_delivery_manage.php?act=list";
			$text=$_LANG['erp_delivery_return_to_delivery_list'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_delivery_no_access'],0,$link);
		}
		if(!is_agency_delivery($delivery_id,$agency_id))
		{
			$href="erp_delivery_manage.php?act=list";
			$text=$_LANG['erp_delivery_return_to_delivery_list'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_delivery_no_agency_access'],0,$link);
		}
		if(lock_table($delivery_id,'delivery','edit'))
		{
			$delivery_info=get_delivery_info($delivery_id);
			$delivery_item_info=get_delivery_item_info($delivery_id);
			$warehouse_list=get_warehouse_list(erp_get_admin_id(),$agency_id,1);
			$delivery_style_list=get_delivery_style_list(1,-1,-1,4);
			if($_CFG['stock_dec_time']==0)
			{
				foreach($delivery_style_list as $k =>$delivery_style)
				{
					if($delivery_style['delivery_style_id']==1)
					{
						unset($delivery_style_list[$k]);
					}
				}
			}
			if($_CFG['stock_dec_time']==1 &&$delivery_info['delivery_style_id']==1)
			{
				$order_info=get_order_not_delivered();
				$smarty->assign('order_info',$order_info);
			}
			$smarty->assign('delivery_info',$delivery_info);
			$smarty->assign('delivery_item_info',$delivery_item_info);
			$smarty->assign('warehouse_list',$warehouse_list);
			$smarty->assign('delivery_style_list',$delivery_style_list);
			$smarty->assign('act','edit');
			if(admin_priv('erp_warehouse_approve','',false))
			{
				$smarty->assign('price_visible','true');
			}
			$smarty->assign('ur_here',$_LANG['erp_delivery_edit_delivery']);
			assign_query_info();
			$smarty->display('erp_delivery_info.htm');
		}
		else
		{
			$href="erp_delivery_manage.php?act=list";
			$text=$_LANG['erp_delivery_return_to_delivery_list'];
			$link[] = array('href'=>$href,'text'=>$text);
			$href="erp_delivery_manage.php?act=view&id=".$delivery_id;
			$text=$_LANG['erp_delivery_view_delivery'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_delivery_no_accessibility'],0,$link);
		}
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif ($_REQUEST['act'] == 'approve')
{
	if((admin_priv('erp_warehouse_approve','',false)))
	{
		$delivery_id=isset($_REQUEST['id']) &&intval($_REQUEST['id'])>0?intval($_REQUEST['id']):0;
		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		if(!is_agency_delivery($delivery_id,$agency_id))
		{
			$href="erp_delivery_manage.php?act=list";
			$text=$_LANG['erp_delivery_return_to_delivery_list'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_delivery_no_agency_access'],0,$link);
		}
		if(lock_table($delivery_id,'delivery','approve'))
		{
			$delivery_info=get_delivery_info($delivery_id);
			$delivery_item_info=get_delivery_item_info($delivery_id);
			$smarty->assign('delivery_info',$delivery_info);
			$smarty->assign('delivery_item_info',$delivery_item_info);
			$smarty->assign('act','approve');
			if((admin_priv('erp_warehouse_approve','',false)))
			{
				$smarty->assign('price_visible','true');
			}
			$smarty->assign('ur_here',$_LANG['erp_delivery_approve_delivery']);
			assign_query_info();
			$smarty->display('erp_delivery_info.htm');
		}
		else
		{
			$href="erp_delivery_manage.php?act=list";
			$text=$_LANG['erp_delivery_return_to_delivery_list'];
			$link[] = array('href'=>$href,'text'=>$text);
			$href="erp_delivery_manage.php?act=view&id=".$delivery_id;
			$text=$_LANG['erp_delivery_view_delivery'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_delivery_no_accessibility'],0,$link);
		}
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif ($_REQUEST['act'] == 'print')
{
	if(admin_priv('erp_warehouse_view','',false) ||admin_priv('erp_warehouse_manage','',false) ||admin_priv('erp_warehouse_approve','',false))
	{
		$delivery_id=isset($_REQUEST['id']) &&intval($_REQUEST['id'])>0 ?intval($_REQUEST['id']) : 0;
		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		if(!is_agency_delivery($delivery_id,$agency_id))
		{
			$href="erp_delivery_manage.php?act=list";
			$text=$_LANG['erp_delivery_return_to_delivery_list'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_delivery_no_agency_access'],0,$link);
		}
		$delivery_info=get_delivery_info($delivery_id);
		$delivery_item_info=get_delivery_item_info($delivery_id);
		$smarty->assign('delivery_info',$delivery_info);
		$smarty->assign('delivery_item_info',$delivery_item_info);
		$smarty->assign('act','view');
		assign_query_info();
		$smarty->display('erp_delivery_print.htm');
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}

function get_delivery_listtable()
{
	$warehouse_id = isset($_REQUEST['w_id']) &&intval($_REQUEST['w_id'])>0 ?intval($_REQUEST['w_id']):0;
	$delivery_style_id = isset($_REQUEST['d_s_id']) &&intval($_REQUEST['d_s_id'])>0 ?intval($_REQUEST['d_s_id']):0;
	$delivery_status = isset($_REQUEST['d_s']) &&intval($_REQUEST['d_s'])>0 ?intval($_REQUEST['d_s']):0;
	$s_date=isset($_REQUEST['s_date'])?trim($_REQUEST['s_date']):'';
	$e_date=isset($_REQUEST['e_date'])?trim($_REQUEST['e_date']):'';
	$delivery_to=isset($_REQUEST['d_t'])?trim($_REQUEST['d_t']):'';
	$delivery_sn=isset($_REQUEST['delivery_sn'])?trim($_REQUEST['delivery_sn']):'';
	$start_time=!empty($s_date)?local_strtotime($s_date): 0;
	$end_time=!empty($e_date)?local_strtotime($e_date):0;
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	
	$filter = array(
		// Search parameters
		'status' => $delivery_status,
		'start_time' => $start_time,
		'end_time' => $end_time,
		'delivery_style_id' => $delivery_style_id,
		'warehouse_id' => $warehouse_id,
		'delivery_to' => $delivery_to,
		'delivery_sn' => $delivery_sn,
		'agency_id' => $agency_id,
		// Parameters with different name
		'w_id' => $warehouse_id,
		'd_s_id' => $delivery_style_id,
		'd_s' => $delivery_status,
		's_date' => $s_date,
		'e_date' => $e_date,
		'd_t' => $delivery_to
	);
	
	$filter['record_count'] = get_delivery_count($filter);
    
    /* 分页大小 */
    $filter = page_and_size($filter);
	
	$delivery_list = get_delivery_list($filter, $filter['start'], $filter['page_size']);
	
	$arr = array(
        'delivery_list' => $delivery_list,
        'filter' => $filter,
        'page_count' => $filter['page_count'],
        'record_count' => $filter['record_count']
    );
    return $arr;
}
?>