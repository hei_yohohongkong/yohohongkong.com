<?php

/***
* currency_exchanges.php
* by howang 2014-11-25
*
* Accounting System
***/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_accounting.php');
$adminuserController = new Yoho\cms\Controller\AdminuserController();
$accountingController = new Yoho\cms\Controller\AccountingController();

if ($_REQUEST['act'] == 'list')
{
    $list = get_currency_exchanges();
    $smarty->assign('ur_here',      '轉帳紀錄');
    $smarty->assign('full_page',    1);

    $smarty->assign('list',         $list['data']);
    $smarty->assign('filter',       $list['filter']);
    $smarty->assign('record_count', $list['record_count']);
    $smarty->assign('page_count',   $list['page_count']);
    
    $smarty->assign('action_link', array('text' => '新增轉帳', 'href' => 'currency_exchanges.php?act=add'));
    $smarty->assign('action_link2', array('text' => $_LANG['actg_transactions'], 'href' => 'accounting_report.php?act=list'));

    $sort_flag  = sort_flag($list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);

    assign_query_info();
    $smarty->display('currency_exchanges_list.htm');
}
elseif ($_REQUEST['act'] == 'query')
{
    $list = get_currency_exchanges();
    $smarty->assign('list',         $list['data']);
    $smarty->assign('filter',       $list['filter']);
    $smarty->assign('record_count', $list['record_count']);
    $smarty->assign('page_count',   $list['page_count']);

    $sort_flag  = sort_flag($list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);

    make_json_result($smarty->fetch('currency_exchanges_list.htm'), '', array('filter' => $list['filter'], 'page_count' => $list['page_count']));
}
elseif ($_REQUEST['act'] == 'add')
{
    $smarty->assign('ur_here',     '新增轉帳');
    
    $smarty->assign('action_link', array('text' => '轉帳紀錄', 'href' => 'currency_exchanges.php?act=list'));
    $smarty->assign('action_link2', array('text' => $_LANG['actg_transactions'], 'href' => 'accounting_report.php?act=list'));
    $salespeople = $adminuserController->getAdminsByPriv(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_PAYMENT_OP, 'sales_name', [], true, true);
    $smarty->assign('salesperson_list', $salespeople);
    $smarty->assign('admin_id', $_SESSION['admin_id']);
    $smarty->assign('admin_name', $_SESSION['admin_name']);
    
    $smarty->assign('accounts', $actg->getAccounts(['is_hidden' => 0]));
    $smarty->assign('payment_types', $actg->getPaymentTypes([], ['where' => 'AND payment_type_id < 4']));
    
    assign_query_info();
    $smarty->display('currency_exchange_info.htm');
}
elseif ($_REQUEST['act'] == 'insert')
{
    $operator = empty($_POST['operator']) ? '' : trim($_POST['operator']);
    if (empty($operator))
    {
        sys_msg('必須輸入操作員');
    }
    
    $from_account = empty($_POST['from_account']) ? 0 : intval($_POST['from_account']);
    if (empty($from_account))
    {
        sys_msg('必須輸入來源帳戶');
    }
    // $acc = $actg->getAccount(array('account_id' => $from_account));
    // if (empty($acc))
    // {
    //     sys_msg('來源帳戶不正確');
    // }
    
    // $from_currency = $acc['currency_code'];
    // $from_actg_type_id = $acc['actg_type_id'];
    
    $from_amount = empty($_POST['from_amount']) ? 0.0 : doubleval($_POST['from_amount']);
    if (empty($from_amount))
    {
        sys_msg('必須輸入金額');
    }
    // if ((!$acc['allow_debit']) && (bccomp($from_amount, $acc['account_balance']) > 0))
    // {
    //     sys_msg('金額超出帳戶結餘');
    // }
    
    $from_payment_type = empty($_POST['from_payment_type']) ? 0 : intval($_POST['from_payment_type']);
    if (empty($from_payment_type))
    {
        sys_msg('必須選擇來源付款方式');
    }
    $pay_type = $actg->getPaymentType(array('payment_type_id' => $from_payment_type));
    if (empty($pay_type))
    {
        sys_msg('來源付款方式不正確');
    }
    
    $to_account = empty($_POST['to_account']) ? 0 : intval($_POST['to_account']);
    if (empty($to_account))
    {
        sys_msg('必須輸入目標帳戶');
    }
    // $acc = $actg->getAccount(array('account_id' => $to_account));
    // if (empty($acc))
    // {
    //     sys_msg('目標帳戶不正確');
    // }
    
    // $to_currency = $acc['currency_code'];
    // $to_actg_type_id = $acc['actg_type_id'];
    
    // $to_amount = empty($_POST['to_amount']) ? 0.0 : doubleval($_POST['to_amount']);
    // if (empty($to_amount))
    // {
    //     sys_msg('必須輸入目標金額');
    // }
    
    $to_payment_type = empty($_POST['to_payment_type']) ? 0 : intval($_POST['to_payment_type']);
    if (empty($to_payment_type))
    {
        sys_msg('必須選擇目標付款方式');
    }
    $pay_type = $actg->getPaymentType(array('payment_type_id' => $to_payment_type));
    if (empty($pay_type))
    {
        sys_msg('目標付款方式不正確');
    }
    
    $remark = empty($_POST['remark']) ? '' : trim($_POST['remark']);
    
    $datetime = $actg->dbdate();
    
    $actg->start_transaction();

    $param = [
        'from_account' => $from_account,
        'from_payment_type' => $from_payment_type,
        'to_account' => $to_account,
        'to_payment_type' => $to_payment_type,
        'amount' => $from_amount,
        'txn_date' => $datetime,
        'remark' => $remark,
        'created_by' => $operator,
    ];
    // $param = [
    //     'from' => [
    //         'account_id' => $from_account,
    //         'actg_type_id' => $from_actg_type_id,
    //         'currency_code' => $from_currency,
    //         'payment_type' => $from_payment_type,
    //         'amount' => $from_amount
    //     ],
    //     'to' => [
    //         'account_id' => $to_account,
    //         'actg_type_id' => $to_actg_type_id,
    //         'currency_code' => $to_currency,
    //         'payment_type' => $to_payment_type,
    //         'amount' => $to_amount
    //     ],
    //     'txn_date' => $datetime,
    //     'remark' => $remark,
    //     'created_by' => $operator,
    // ];

    if ($accountingController->insertExchanges($param)) {
        $actg->commit_transaction();
        $link[0]['text'] = $_LANG['accounting_system'];
        $link[0]['href'] = 'accounting_system.php?act=list';
        sys_msg('轉帳成功', 0, $link, true);
    } else {
        $actg->rollback_transaction();
        sys_msg('轉帳失敗');
    }

    // $exchange = array(
    //     'exchange_date' => $datetime,
    //     'from_account' => $from_account,
    //     'from_currency' => $from_currency,
    //     'from_amount' => $from_amount,
    //     'from_payment_type' => $from_payment_type,
    //     'to_account' => $to_account,
    //     'to_currency' => $to_currency,
    //     'to_amount' => $to_amount,
    //     'to_payment_type' => $to_payment_type,
    //     'operator' => $operator,
    //     'remark' => $remark
    // );
    // $exchange_id = $actg->addCurrencyExchange($exchange);

    // if ($exchange_id)
    // {
    //     $actg->commit_transaction();

    //     $link[0]['text'] = '返回列表';
    //     $link[0]['href'] = 'currency_exchanges.php?act=list';

    //     sys_msg('轉帳成功', 0, $link, true);
    // }
    // else
    // {
    //     $actg->rollback_transaction();

    //     sys_msg('轉帳失敗');
    // }
}
elseif ($_REQUEST['act'] == 'calc_target_amount')
{
    $from_account = empty($_REQUEST['from_account']) ? 0 : $_REQUEST['from_account'];
    $from_amount = empty($_REQUEST['from_amount']) ? 0 : $_REQUEST['from_amount'];
    $to_account = empty($_REQUEST['to_account']) ? 0 : $_REQUEST['to_account'];
    if (empty($from_account) || empty($from_amount) || empty($to_account))
    {
        make_json_error('Missing parameters');
    }
    
    $from_acc = $actg->getAccount(array('account_id' => $from_account));
    if (empty($from_acc))
    {
        make_json_error('來源帳戶不正確');
    }
    $to_acc = $actg->getAccount(array('account_id' => $to_account));
    if (empty($to_acc))
    {
        make_json_error('目標帳戶不正確');
    }
    
    //$to_amount = bcdiv(bcmul($from_amount, $from_acc['currency_rate']), $to_acc['currency_rate']);
    $to_amount = $actgHooks->convertCurrency($from_amount, $from_acc, $to_acc);
    
    make_json_result('', '', array(
        'amount' => round($to_amount, 2)
    ));
}

function get_currency_exchanges()
{
    global $actg, $db;
    
    $filter['sort_by']      = empty($_REQUEST['sort_by']) ? 'exchange_id' : trim($_REQUEST['sort_by']);
    $filter['sort_order']   = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);

    $sql = "SELECT count(*) FROM `actg_currency_exchanges`";
    $filter['record_count'] = $db->getOne($sql);

    /* 分页大小 */
    $filter = page_and_size($filter);

    /* 查询 */
    $data = $actg->getCurrencyExchanges(array(), array(
        'orderby' => $filter['sort_by'] . ' ' . $filter['sort_order'],
        'limit' => $filter['start'] . ',' . $filter['page_size']
    ));
    
    foreach ($data as $key => $row)
    {
        $data[$key]['exchange_date_formatted'] = local_date($GLOBALS['_CFG']['time_format'], strtotime($row['exchange_date']));
        $data[$key]['from_amount_formatted'] = $actg->money_format($row['from_amount'], $row['from_currency_format']);
        $data[$key]['to_amount_formatted'] = $actg->money_format($row['to_amount'], $row['to_currency_format']);
    }
    
    $arr = array('data' => $data, 'filter' => $filter, 'page_count' => $filter['page_count'], 'record_count' => $filter['record_count']);
    
    return $arr;
}

?>