<?php

define('IN_ECS', true);

require dirname(__FILE__) . '/includes/init.php';
require_once(ROOT_PATH . 'includes/php-xlsx-writer/xlsxwriter.class.php');

$inventory = getInventoryAdjustReport();
$depreciation = getdepreciationReport();
$smarty->assign("ur_here", $_LANG['write_off']);
$smarty->assign("inventory", $inventory);
$smarty->assign("depreciation", $depreciation);
$smarty->assign("start_time", empty($_REQUEST['start_time']) ? date("Y-m-d", strtotime("first day of this month")) : $_REQUEST['start_time']);
$smarty->assign("end_time", empty($_REQUEST['end_time']) ? date("Y-m-d", strtotime("last day of this month")) : $_REQUEST['end_time']);

if ($_REQUEST['act'] === 'inventory_adjust_report_export') {
    exit (exportReport($inventory));
}

if ($_REQUEST['act'] === 'depreciation_report_export') {
    exit (exportReport($depreciation));
}

$smarty->display("write_off.htm");

function getInventoryAdjustReport()
{
    global $db, $ecs;

    $start_time = empty($_REQUEST['start_time']) ? strtotime("first day of this month") : strtotime($_REQUEST['start_time']);
    $end_time = (empty($_REQUEST['end_time']) ? strtotime("last day of this month") : strtotime($_REQUEST['end_time'])) + 86399;

    $sql_in = "SELECT goods_id, SUM(received_qty) as total_in_qty FROM " . $ecs->table("erp_warehousing_item") . " ewi " . 
                "LEFT JOIN " . $ecs->table("erp_warehousing") . " ew ON ew.warehousing_id = ewi.warehousing_id " .
                "WHERE approve_time BETWEEN $start_time AND $end_time AND warehousing_style_id = 4 AND warehousing_from NOT LIKE '%rma%' GROUP BY goods_id";
    $sql_out = "SELECT goods_id, SUM(delivered_qty) as total_out_qty FROM " . $ecs->table("erp_delivery_item") . " ewi " . 
                "LEFT JOIN " . $ecs->table("erp_delivery") . " ew ON ew.delivery_id = ewi.delivery_id " .
                "WHERE approve_time BETWEEN $start_time AND $end_time AND delivery_style_id = 4 AND delivery_to NOT LIKE '%rma%' GROUP BY goods_id";
    $total_in = $db->getAll($sql_in);
    $total_out = $db->getAll($sql_out);

    $goods = [];
    foreach ($total_in as $row) {
        $goods[$row['goods_id']]['qty'] = intval($row['total_in_qty']);
    }
    foreach ($total_out as $row) {
        $goods[$row['goods_id']]['qty'] = empty($goods[$row['goods_id']]) ? -intval($row['total_out_qty']) : $goods[$row['goods_id']]['qty'] - intval($row['total_out_qty']);
    }
    $goods_ids = array_keys($goods);

    $sql_cost = "SELECT goods_id, goods_name, cost FROM ". $ecs->table("goods") . " WHERE goods_id " . db_create_in($goods_ids);
    $goods_cost = $db->getAll($sql_cost);
    foreach ($goods_cost as $row) {
        $goods[$row['goods_id']]['goods_id'] = $row['goods_id'];
        $goods[$row['goods_id']]['goods_name'] = $row['goods_name'];
        $goods[$row['goods_id']]['cost'] = price_format(floatval($row['cost']) * $goods[$row['goods_id']]['qty']);
    }
    return $goods;
}

function getdepreciationReport()
{
    global $db, $ecs;

    $start_time = empty($_REQUEST['start_time']) ? strtotime("first day of this month") : strtotime($_REQUEST['start_time']);
    $end_time = (empty($_REQUEST['end_time']) ? strtotime("last day of this month") : strtotime($_REQUEST['end_time'])) + 86399;

    $sql = "SELECT g.goods_id, g.goods_name, g.cost, SUM(delivered_qty) as qty FROM " . $ecs->table("erp_delivery_item") . " ewi " . 
                "LEFT JOIN " . $ecs->table("erp_delivery") . " ew ON ew.delivery_id = ewi.delivery_id " .
                "LEFT JOIN " . $ecs->table("goods") . " g ON g.goods_id = ewi.goods_id " .
                "WHERE approve_time BETWEEN $start_time AND $end_time AND delivery_style_id = 2 GROUP BY g.goods_id ORDER BY g.goods_id";
    $goods = $db->getAll($sql);

    foreach ($goods as $key => $row) {
        $goods[$key]['cost'] = price_format(floatval($row['cost']) * -intval($row['qty']));
    }
    return $goods;
}

function exportReport($reportData)
{
    $writer = new \XLSXWriter();

    $header = [
        '編號' => 'string',
        '產品名稱' => 'string',
        '數量' => 'string',
        '金額' => 'string'
    ];

    $writer->writeSheetHeader('Sheet1', $header);

    foreach ($reportData as $reportRow) {
        $item = [
            $reportRow['goods_id'],
            $reportRow['goods_name'],
            $reportRow['qty'],
            $reportRow['cost'],
        ];
        $writer->writeSheetRow('Sheet1', $item);
    }

    header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.‌​sheet; charset=utf-8');
    header('Content-Disposition: attachment; filename=' . local_date('YmdHis', gmtime()).'-'.$_REQUEST['act'].'.xlsx');
    header('Cache-Control: max-age=0');

    $writer->writeToStdOut();
}