<?php

define('IN_ECS',true);
require_once(dirname(__FILE__) .'/includes/init.php');
require_once(dirname(__FILE__) .'/includes/ERP/lib_erp_base.php');
require_once(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require_once(dirname(__FILE__) .'/includes/ERP/lib_erp_goods.php');
require_once(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
require_once(dirname(__FILE__) .'/includes/ERP/lib_erp_warehouse.php');
require_once(dirname(__FILE__) .'/includes/ERP/cls/cls_date.php');
$warehouseTransferController = new Yoho\cms\Controller\WarehouseTransferController();
$adminuserController = new Yoho\cms\Controller\AdminuserController();

if($_REQUEST['act'] == 'list' || $_REQUEST['act'] == ''){
	if( admin_priv('erp_warehouse_view','',false) || admin_priv('erp_warehouse_manage','',false) || admin_priv('erp_warehouse_approve','',false))
	{
        include('./includes/ERP/page.class.php');

		if (isset($_REQUEST['goods_sn']) &&!empty($_REQUEST['goods_sn'])) {
			$filter['goods_sn'] = $_REQUEST['goods_sn'];
		}

		if (isset($_REQUEST['warehouse_to']) &&!empty($_REQUEST['warehouse_to'])) {
			$filter['warehouse_to'] = $_REQUEST['warehouse_to'];
		}

		$smarty->assign('filter', $filter);
		$smarty->assign('type_options', $warehouseTransferController->getTypeOptions());
        $smarty->assign('status_options', $warehouseTransferController->getStatusOptions());
		$smarty->assign('reason_type_options', $warehouseTransferController->getReasonTypeOptions());
		$smarty->assign('warehouse_list_options', $warehouseTransferController->getWareHouseListOptions());
		$smarty->assign('operator_options', $warehouseTransferController->personInChargeOption(false));
        $smarty->assign('ur_here', '調倉列表');
        $smarty->assign('full_page', 1);
        $smarty->assign('start_date', $result['start_date']);
        $smarty->assign('end_date', $result['end_date']);
        $smarty->assign('cfg_lang', $_CFG['lang']);
        $smarty->assign('lang', $_LANG);
		$smarty->assign('action_link2', array('href' => 'erp_shop_to_warehouse_suggestion_transfer.php?act=list', 'text' => '調貨建議(門市回總倉)'));
        $smarty->display('erp_warehouse_transfer_list.htm');
    }else{
        $href="index.php";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
    }
} elseif ($_REQUEST['act'] == 'query') {
    $result = $warehouseTransferController->getWarehouseTransferList();
    $warehouseTransferController->ajaxQueryAction($result['data'],$result['record_count'],false);
} elseif ($_REQUEST['act'] == 'post_warehouse_transfer_create') {
    $result = $warehouseTransferController->warehouseTransferCreate();
	if(isset($result['error'])){
		make_json_error($result['error']);
	}else{
		make_json_result($result);
	}
} elseif ($_REQUEST['act'] == 'post_transfer_type_change') {
    $result = $warehouseTransferController->transferTypeChange();
	if($result !== false){
		make_json_result($result);
	}else{
		make_json_error('更新時發生錯誤');
	}
} elseif ($_REQUEST['act'] == 'post_warehouse_transfer_follow_up_remark_submit') {
    $result = $warehouseTransferController->warehouseTransferFollowUpRemarkSubmit();
    if($result !== false){
        make_json_result($result);
    }else{
        make_json_error('更新時發生錯誤');
    }
}


?>