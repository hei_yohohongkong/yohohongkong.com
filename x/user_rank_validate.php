<?php
define('IN_ECS', true);
require dirname(__FILE__) . '/includes/init.php';
$controller = new Yoho\cms\Controller\YohoBaseController('user_rank_program_application');

if ($_REQUEST['act'] == "list") {
    $validate_ranks = $db->getAll(
        "SELECT program_id, program_name FROM " . $ecs->table("user_rank_program") . " urp " .
        "LEFT JOIN " . $ecs->table("user_rank") . " ur ON ur.rank_id = urp.rank_id " .
        "WHERE special_rank = 1"
    );
    $smarty->assign("ur_here", $_LANG['rank_program_application']);
    $smarty->assign("validate_ranks", $validate_ranks);
    $smarty->display("user_rank_validate.htm");
} elseif ($_REQUEST['act'] == "query") {
    $program_id = empty($_REQUEST['program_id']) ? 0 : intval($_REQUEST['program_id']);
    $_REQUEST['page_size']  = empty($_REQUEST['page_size'])  ? 50          : intval($_REQUEST['page_size']);
    $_REQUEST['start']      = empty($_REQUEST['start'])      ? 0           : intval($_REQUEST['start']);
    $_REQUEST['sort_by']    = empty($_REQUEST['sort_by']) ? 'rec_id' : trim($_REQUEST['sort_by']);
    $_REQUEST['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
    $sql = "SELECT u.user_name, ur.rank_name, urp.program_name, urpa.* FROM " . $ecs->table("user_rank_program_application") . " urpa " .
        "INNER JOIN " . $ecs->table("user_rank_program") . " urp ON urp.program_id = urpa.program_id " .
        "INNER JOIN " . $ecs->table("user_rank") . " ur ON ur.rank_id = urp.rank_id " .
        "INNER JOIN " . $ecs->table("users") . " u ON u.user_id = urpa.user_id " .
        (empty($program_id) ? "" : "WHERE urpa.program_id = $program_id ") .
        "ORDER BY verified ASC, ".$_REQUEST['sort_by']." ".$_REQUEST['sort_order'];

    $sql .= ' LIMIT ' . $_REQUEST['start'] . ', ' . $_REQUEST['page_size'];
    $list = $db->getAll($sql);
    $count = $db->getOne("SELECT count(urpa.user_id) FROM " . $ecs->table("user_rank_program_application") . " urpa " .
        "INNER JOIN " . $ecs->table("user_rank_program") . " urp ON urp.program_id = urpa.program_id " .
        "INNER JOIN " . $ecs->table("user_rank") . " ur ON ur.rank_id = urp.rank_id " .
        "INNER JOIN " . $ecs->table("users") . " u ON u.user_id = urpa.user_id " .
        (empty($program_id) ? "" : "WHERE urpa.program_id = $program_id "));
    foreach ($list as $key => $row) {
        $action = [
            "approve" => ["name" => "審核", "class" => "info"],
            "edit" => ["name" => "編輯", "class" => "primary"],
            "view" => ["name" => "查看", "class" => "default"],
        ];
        $act = check_authz("user_rank_validate") ? (!in_array($row['stage'], [RANK_APPLICATION_APPROVED, RANK_APPLICATION_DISAPPROVE]) ? 'approve' : 'edit') : 'view';
        $custom_actions = [
            'action' => [
                'link' => "user_rank_validate.php?act=info&rec_id=$row[rec_id]&action=$act",
                'title' => $action[$act]['name'] . "申請",
                'label' => $action[$act]['name'],
                'btn_class' => "btn btn-" . $action[$act]['class']
            ]
        ];
        $list[$key]['formatted_stage'] = $_LANG['formatted_apllication_status'][$row['stage']];
        $list[$key]['_action'] = $controller->generateCrudActionBtn($row['rec_id'], 'id', null, [], $custom_actions);
    }
    $controller->ajaxQueryAction($list, $count, false);
} elseif ($_REQUEST['act'] == "info") {
    $rec_id = empty($_REQUEST['rec_id']) ? "" : $_REQUEST['rec_id'];
    if (empty($rec_id)) {
        sys_msg("請選擇欲處理的申請");
    }
    $sql = "SELECT u.user_name, ur.rank_name, urp.program_name, urpa.*, urp.organization_ids FROM " . $ecs->table("user_rank_program_application") . " urpa " .
        "INNER JOIN " . $ecs->table("user_rank_program") . " urp ON urp.program_id = urpa.program_id " .
        "INNER JOIN " . $ecs->table("user_rank") . " ur ON ur.rank_id = urp.rank_id " .
        "INNER JOIN " . $ecs->table("users") . " u ON u.user_id = urpa.user_id " .
        "WHERE urpa.rec_id = $rec_id";
    $record = $db->getRow($sql);
    $record['key'] = md5("$record[user_id]_$record[program_id]");
    $record['file'] = file_get_contents(ROOT_PATH . "data/rank_program/$record[key].txt");
    $record['foramtted_expiry'] = date("Y-m-d", strtotime($record['expiry']));
	$organization_ids = [];
	if (!empty($record['organization_ids'])) $organization_ids = unserialize($record['organization_ids']);
	$selected_organization_list = $db->getAll("SELECT organization_id, organization_type, organization_name FROM " . $ecs->table("program_organization") . " WHERE organization_id" . db_create_in($organization_ids));
	$smarty->assign('selected_organization_list', $selected_organization_list);
    $action = empty($_REQUEST['action']) ? "view" : trim($_REQUEST['action']);
    $smarty->assign("action", $action);
    $smarty->assign("ur_here", $_LANG['rank_program_validate']);
    $smarty->assign('action_link', array('text' => "返回申請列表", 'href' => 'user_rank_validate.php?act=list'));
    $smarty->assign("record", $record);
    $smarty->display("user_rank_validate_info.htm");
} elseif ($_REQUEST['act'] == "verify") {
    if (empty($_REQUEST["id"]) || empty($_REQUEST["uid"]) || empty($_REQUEST["pid"])) {
        make_json_error("Missing parameters!");
    }

    $rank_id = $db->getOne("SELECT rank_id FROM " . $ecs->table("user_rank_program") . " WHERE program_id = $_REQUEST[pid]");
    $program_ids = $db->getCol("SELECT program_id FROM " . $ecs->table("user_rank_program") . " WHERE rank_id = $rank_id AND status = " . RANK_PROGRAM_SHOW);

    $sql = "SELECT COUNT(*) FROM " . $ecs->table("user_rank_program_application") . " WHERE user_id != $_REQUEST[uid] AND unique_id = '$_REQUEST[id]' AND program_id " . db_create_in($program_ids) ." AND organization_id = " . $_REQUEST['oid'];
    $r = $sql;
    if (empty($db->getOne($sql))) {
        $sql = "UPDATE " . $ecs->table("user_rank_program_application") . " SET unique_id = '$_REQUEST[id]', verified = 1, organization_id = $_REQUEST[oid] WHERE user_id = $_REQUEST[uid] AND program_id = $_REQUEST[pid]";
        $db->query($sql);
        make_json_response("success  " . $r);
    } else {
        make_json_error("already_exist");
    }
} elseif ($_REQUEST['act'] == "update") {
    if (empty($_REQUEST['rec_id'])) {
        sys_msg("沒有選擇申請", 1);
    }
    $record = $db->getRow("SELECT * FROM " . $ecs->table("user_rank_program_application") . " WHERE rec_id = $_REQUEST[rec_id]");
    if (empty($record)) {
        sys_msg("沒有此申請此紀錄", 1);
    }
    $stage = isset($_REQUEST['stage']) ? intval($_REQUEST['stage']) : $record['stage'];
    $expiry = empty($_REQUEST['expiry']) ? "" : $_REQUEST['expiry'] . " 23:59:59";
    if ($stage == RANK_APPLICATION_APPROVED && $record['verified'] == 0) {
        sys_msg("未驗證證件ID，無法通過完全審核", 1);
    }
    $sql = "UPDATE " . $ecs->table("user_rank_program_application") . " SET stage = $stage" . (empty($expiry) ? "" : ", expiry = '$expiry'") . " WHERE rec_id = $_REQUEST[rec_id]";
    $db->query($sql);
    
    if ($stage == RANK_APPLICATION_PREPARE) {
        $sql = "UPDATE " . $ecs->table("user_rank_program_application") . " SET unique_id = '$_REQUEST[id]', verified = 1  WHERE  rec_id = $_REQUEST[rec_id]";
        $db->query($sql);
    }
 
    $db->query($sql);
    sys_msg("已更新申請", 0, [
        [
            'text' => $_LANG['rank_program_application'],
            'href' => "user_rank_validate.php?act=list"
        ]
    ]);
}

?>