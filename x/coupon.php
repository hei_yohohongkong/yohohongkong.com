<?php

/***
* coupon.php
* by howang 2014-09-12
*
* Manage coupons
***/

define('IN_ECS', true);
require(dirname(__FILE__) . '/includes/init.php');

// define controller
$userController = new Yoho\cms\Controller\UserController();
$controller     = new Yoho\cms\Controller\yohoBaseController('coupons');
$theClubMaxPoint = 75000;
$theClubAccrual = [
    [
        'code'  => 'YOHO002HKD_20181127_001',
        'eng'   => 'Yoho online shop CP earning $10=1CP',
        'chin'  => '友和網店購物 HK$10賺1CP',
    ],
    [
        'code'  => 'YOHO002HKD_20181127_002',
        'eng'   => 'Yoho Retail CP earning 50CP',
        'chin'  => '友和門市購物賺分 - 50CP',
    ],
    [
        'code'  => 'YOHO002HKD_20181127_003',
        'eng'   => 'Yoho Retail CP earning 100CP',
        'chin'  => '友和門市購物賺分 - 100CP',
    ],
    [
        'chin'  => "<span class='red' title='金額不足'>不合資格</span>"
    ],
    [
        'chin'  => "<span class='red' title='逾時支付'>不合資格</span>"
    ],
    [
        'code'  => 'YOHO002HKD_20190122_001',
        'eng'   => 'Yoho Retail shop CP earning $10=1CP',
        'chin'  => '友和門市購物 HK$10賺1CP',
    ],
    [
        'code'  => 'YOHO002HKD_20190122_002',
        'eng'   => 'Yoho Retail shop CP earning $10=2CP',
        'chin'  => '友和門市購物 HK$10賺2CP',
    ]
];

if ($_REQUEST['act'] == 'list')
{
    // /* 检查权限 */
    // admin_priv('bonus_manage');
    $allow_manage = admin_priv('bonus_manage','',false);
    $smarty->assign('allow_manage', $allow_manage);
    $smarty->assign('ur_here',      $_LANG['16_coupons']);

    if ($allow_manage)
    {
        $smarty->assign('action_link3', array('text' => '新增信用卡推廣', 'href' => 'coupon_promote_manage.php?act=list'));
        $smarty->assign('action_link', array('text' => '新增優惠券', 'href' => 'coupon.php?act=add'));
        $smarty->assign('action_link2', array('text' => '使用紀錄', 'href' => 'coupon.php?act=log'));
    }
    assign_query_info();
    $smarty->display('coupon_list.htm');
}
elseif ($_REQUEST['act'] == 'query')
{
    // /* 检查权限 */
    // check_authz_json('bonus_manage');
    $allow_manage = admin_priv('bonus_manage','',false);
    $list = get_coupon($allow_manage);
    $smarty->assign('list',         $list['data']);
    $smarty->assign('record_count', $list['record_count']);
    $controller->ajaxQueryAction($list['data'], $list['record_count'], false);

}
elseif ($_REQUEST['act'] == 'log')
{
    /* 检查权限 */
    admin_priv('bonus_manage');

    $coupon_id = empty($_REQUEST['id']) ? 0 : intval($_REQUEST['id']);
    $coupon = get_coupon_info($coupon_id);

    $smarty->assign('ur_here',      $_LANG['16_coupons'] . (empty($coupon) ? '' : $coupon['coupon_name']) . '使用紀錄');
    $smarty->assign('full_page',    1);

    $smarty->assign('list',         $list['data']);
    $smarty->assign('filter',       $list['filter']);
    $smarty->assign('record_count', $list['record_count']);
    $smarty->assign('page_count',   $list['page_count']);
    
    $smarty->assign('action_link', array('text' => '返回列表', 'href' => 'coupon.php?act=list'));

    $sort_flag  = sort_flag($list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);

    assign_query_info();
    $smarty->display('coupon_log.htm');
}
elseif ($_REQUEST['act'] == 'query_log')
{
    /* 检查权限 */
    admin_priv('bonus_manage');

    $list = get_coupon_log();
    $controller->ajaxQueryAction($list['data'], $list['record_count'], false);
    $smarty->assign('list',         $list['data']);
    $smarty->assign('filter',       $list['filter']);
    $smarty->assign('record_count', $list['record_count']);
    $smarty->assign('page_count',   $list['page_count']);

    $sort_flag  = sort_flag($list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);

    make_json_result($smarty->fetch('coupon_log.htm'), '', array('filter' => $list['filter'], 'page_count' => $list['page_count']));
}
elseif (in_array($_REQUEST['act'], array('add','info','edit')))
{
    /* 检查权限 */
    if ($_REQUEST['act'] != 'info')
    {
        admin_priv('bonus_manage');
    }
    
    $smarty->assign('ur_here',      $_LANG['16_coupons']);
    
    if ($_REQUEST['act'] != 'add')
    {
        $coupon_id = empty($_REQUEST['id']) ? 0 : intval($_REQUEST['id']);
        $coupon = get_coupon_info($coupon_id);
        
        if (!$coupon)
        {
            sys_msg('優惠券編號錯誤');
        }
        
        $smarty->assign('coupon',       $coupon);
    }
    else
    {
        $coupon = array(
            'coupon_name' => '',
            'coupon_code' => '',
            'coupon_amount' => 0,
            'coupon_discount' => 0,
            'goods_list' => '',
            'users_list' => '',
            'min_goods_amount' => 0,
            'free_shipping' => 0,
            'reusable' => 0,
            'cartwide' => 1,
            'exclusive' => 0,
            'start_date' => local_date('Y-m-01 00:00:00'),
            'end_date' => local_date('Y-m-t 23:59:59'),
            'sale_exclude' => 0,
			'is_remark' => 0
        );
        $smarty->assign('coupon',       $coupon);
    }
    
    $smarty->assign('action_link', array('text' => '返回列表', 'href' => 'coupon.php?act=list'));
    if ($_REQUEST['act'] == 'info')
    {
        if (admin_priv('bonus_manage','',false))
        {
            $smarty->assign('action_link2', array('text' => '編輯優惠券', 'href' => 'coupon.php?act=edit&id=' . $coupon_id));
        }
    }
    elseif ($_REQUEST['act'] == 'edit')
    {
        $smarty->assign('action_link2', array('text' => '查看優惠券', 'href' => 'coupon.php?act=info&id=' . $coupon_id));
    }
    
    $smarty->assign('act',          $_REQUEST['act']);
    
    $smarty->assign('cfg_lang',     $_CFG['lang']);
    $smarty->assign('coupon_promote', $db->getAll("SELECT * FROM " . $ecs->table("coupon_promote")));
    
    assign_query_info();
    $smarty->display('coupon_info.htm');
}
elseif (in_array($_REQUEST['act'], array('insert','update')))
{
    /* 检查权限 */
    admin_priv('bonus_manage');
    
    if ($_REQUEST['act'] == 'update')
    {
        $coupon_id = empty($_REQUEST['coupon_id']) ? 0 : intval($_REQUEST['coupon_id']);
        
        if (!$coupon_id)
        {
            sys_msg('優惠券編號錯誤');
        }
    }
    
    $coupon = array();
    
    if (!empty($_POST['coupon_name']))
    {
        $coupon['coupon_name'] = trim($_POST['coupon_name']);
    }
    else
    {
        sys_msg('請輸入優惠券名稱');
    }
    if ($_REQUEST['act'] == 'insert')
    {
        if (!empty($_POST['coupon_code']))
        {
            $coupon['coupon_code'] = trim($_POST['coupon_code']);
            
            $test = $db->getOne("SELECT coupon_name FROM " . $ecs->table('coupons') . " WHERE coupon_code = '" . $coupon['coupon_code'] . "'");
            if (!empty($test))
            {
                sys_msg('優惠券號碼已被另一張優惠券「' . $test . '」使用');
            }
        }
        else
        {
            sys_msg('請輸入優惠券號碼');
        }
    }
    if (isset($_POST['coupon_amount']))
    {
        $coupon['coupon_amount'] = $_POST['coupon_amount'];
    }
    if (isset($_POST['coupon_discount']))
    {
        $coupon['coupon_discount'] = $_POST['coupon_discount'];
    }
    if (isset($_POST['goods_list']))
    {
        if ($_POST['goods_list'] != '')
        {
            $test = json_decode($_POST['goods_list'], true);
            if (!is_array($test))
            {
                sys_msg('適用產品列表格式錯誤');
            }
            foreach ($test as $test2)
            {
                if (!is_array($test2))
                {
                    sys_msg('適用產品列表格式錯誤');
                }
                foreach ($test2 as $test3)
                {
                    if (!is_int($test3))
                    {
                        sys_msg('適用產品列表格式錯誤');
                    }
                }
            }
        }
        $coupon['goods_list'] = $_POST['goods_list'];
    }
    if (isset($_POST['users_list']))
    {
        if ($_POST['users_list'] != '')
        {
            $test = json_decode($_POST['users_list'], true);
            if (!is_array($test))
            {
                sys_msg('適用會員列表格式錯誤');
            }
            foreach ($test as $test2)
            {
                if (!is_int($test2))
                {
                    sys_msg('適用會員列表格式錯誤');
                }
            }
        }
        $coupon['users_list'] = $_POST['users_list'];
    }
    if (isset($_POST['cat_list']))
    {
        $coupon['cat_list'] = implode(",", $_POST['cat_list']);
    } else {
        $coupon['cat_list'] = "";
    }
    if (isset($_POST['exclude_cat_list']))
    {
        $coupon['exclude_cat_list'] = implode(",", $_POST['exclude_cat_list']);
    } else {
        $coupon['exclude_cat_list'] = "";
    }
    if (isset($_POST['cat_list']) && isset($_POST['exclude_cat_list']))
    {
        $tmp = array_intersect($_POST['cat_list'], $_POST['exclude_cat_list']);
        if($tmp != false) {
            sys_msg('適用分類跟不適用分類不可重覆');
        }
    }
    if (isset($_POST['brand_list']))
    {
        $coupon['brand_list'] = implode(",", $_POST['brand_list']);
    } else {
        $coupon['brand_list'] = "";
    }
    if (isset($_POST['exclude_brand_list']))
    {
        $coupon['exclude_brand_list'] = implode(",", $_POST['exclude_brand_list']);
    } else {
        $coupon['exclude_brand_list'] = "";
    }
    if (isset($_POST['brand_list']) && isset($_POST['exclude_brand_list']))
    {
        $tmp = array_intersect($_POST['brand_list'], $_POST['exclude_brand_list']);
        if($tmp != false) {
            sys_msg('適用品牌跟不適用品牌不可重覆');
        }
    }
    if (isset($_POST['min_goods_amount']))
    {
        $coupon['min_goods_amount'] = $_POST['min_goods_amount'];
    }
    if (isset($_POST['free_shipping']))
    {
        $coupon['free_shipping'] = $_POST['free_shipping'] ? 1 : 0;
    }
    if (isset($_POST['user_limit']))
    {
        $coupon['user_limit'] = 0;
    } else {
        $coupon['user_limit'] = 1;
    }
    if (isset($_POST['max_user_used']))
    {
        $coupon['max_user_used'] = $_POST['max_user_used'];
    } else {
        $coupon['max_user_used'] = 0;
    }
    if (isset($_POST['times_limit']))
    {
        $coupon['times_limit'] = 0;
    } else {
        $coupon['times_limit'] = 1;
    }
    if (isset($_POST['max_times_used']))
    {
        $coupon['max_times_used'] = $_POST['max_times_used'];
    } else {
        $coupon['max_times_used'] = 0;
    }
    if (isset($_POST['cartwide']))
    {
        $coupon['cartwide'] = $_POST['cartwide'] ? 1 : 0;
        if ((!$coupon['cartwide']) &&  (empty($coupon['goods_list']) && empty($coupon['cat_list']) && empty($coupon['exclude_cat_list']) && empty($coupon['brand_list']) && empty($coupon['exclude_brand_list'])))
        {
            sys_msg('若不是全單計算，必需輸入適用產品列表');
        }
    }
    if (isset($_POST['exclusive']))
    {
        $coupon['exclusive'] = $_POST['exclusive'] ? 1 : 0;
    }
    if (isset($_POST['sms_verified']))
    {
        $coupon['sms_verified'] = $_POST['sms_verified'] ? 1 : 0;
    }
    if (isset($_POST['exclude_flashdeal']))
    {
        $coupon['exclude_flashdeal'] = $_POST['exclude_flashdeal'] ? 1 : 0;
    }
    if (!empty($_POST['start_date']))
    {
        $coupon['start_time'] = local_strtotime($_POST['start_date']);
    }
    if (!empty($_POST['end_date']))
    {
        $coupon['end_time'] = local_strtotime($_POST['end_date']);
    }
	if (isset($_POST['sale_exclude']))
	{
		$coupon['sale_exclude'] = $_POST['sale_exclude'] ? 1 : 0;
	}

	if (isset($_POST['is_remark']))
	{
		$coupon['is_remark'] = $_POST['is_remark'] ? 1 : 0;
	}

	if (isset($_POST['promote_id']))
	{
		$coupon['promote_id'] = empty($_POST['promote_id']) ? 0 : intval($_POST['promote_id']);
	}
    
    if ($_REQUEST['act'] == 'update')
    {
        $db->autoExecute($ecs->table('coupons'), $coupon, 'UPDATE', " `coupon_id` = '" . $coupon_id . "'");
    }
    else
    {
        $coupon['creator'] = $_SESSION['admin_name'];
        $db->autoExecute($ecs->table('coupons'), $coupon, 'INSERT');
        
        $coupon_id = $db->insert_id();
    }
    
    $link[0]['text'] = '查看優惠券';
    $link[0]['href'] = 'coupon.php?act=info&id=' . $coupon_id;
    
    $link[1]['text'] = '返回列表';
    $link[1]['href'] = 'coupon.php?act=list';

    sys_msg(($_REQUEST['act'] == 'update' ? '編輯' : '新增') . '優惠券成功', 0, $link, true);
}
elseif($_REQUEST['act'] == 'search_user')
{
    $type       = $_REQUEST['type'];
    $value      = $_REQUEST['value'];
    $users_list = $_REQUEST['users_list'];
    $users_list = $userController->findUserByType($type, $value, $users_list);
    echo json_encode($users_list);
}
elseif ($_REQUEST['act'] == 'promote_info') {
    $promote_id = empty($_REQUEST['promote_id']) ? 0 : intval($_REQUEST['promote_id']);
    $action_link_list = [
        // ["href" => "coupon.php?act=promote_export&promote_id=$promote_id", "text" => "下載資料"],
        // ["href" => "coupon.php?act=promote_export&ftp=1&promote_id=$promote_id", "text" => "傳送資料"],
    ];
    if ($promote_id == 1) {
        $sql = "SELECT * FROM " . $ecs->table("coupon_promote_info") .
                " WHERE promote_id = $promote_id AND data LIKE '%export%'";
        $list = $db->getAll($sql);
        foreach ($list as $row) {
            $data = \json_decode($row['data'], true);
            $theClubMaxPoint -= floatval($data['rewarded']);
        }
        $smarty->assign("remain_point", $theClubMaxPoint);
        $sql = "SELECT oi.goods_amount, oi.order_status, oi.pay_status, oi.pay_time, oi.platform, c.end_time FROM " . $ecs->table("coupon_promote_info") . " cpi " .
                "LEFT JOIN " . $ecs->table("order_info") . " oi ON oi.order_id = cpi.order_id " .
                "LEFT JOIN " . $ecs->table("coupons") . " c ON c.coupon_id = cpi.coupon_id " .
                "WHERE cpi.promote_id = $promote_id AND data NOT LIKE '%export%' AND oi.pay_time < c.end_time";
        $list = $db->getAll($sql);
        foreach ($list as $row) {
            $clubInfo = calculate_the_club_point($row['goods_amount'], $row['pay_time'], $row['platform'], $row['end_time'], false);
            if (!in_array($row['order_status'], [OS_CONFIRMED, OS_SPLITED, OS_SPLITING_PART]) || $row['pay_status'] != PS_PAYED) {
                $clubInfo['reward'] = 0;
            }

            $theClubMaxPoint -= floatval($clubInfo['reward']);
        }
        $smarty->assign("estimate_remain_point", $theClubMaxPoint);
    }
    $smarty->assign("action_link_list", $action_link_list);
    $smarty->assign("promote_id", $promote_id);
    $smarty->assign("promote_name", $db->getOne("SELECT promote_name FROM " . $ecs->table("coupon_promote") . " WHERE promote_id = $promote_id"));
    $smarty->display("coupon_promote.htm");
}
elseif ($_REQUEST['act'] == 'promote_info_query') {
    $promote_id = empty($_REQUEST['promote_id']) ? 0 : intval($_REQUEST['promote_id']);
    $start = empty($_REQUEST['start']) ? 0 : intval($_REQUEST['start']);
    $length = empty($_REQUEST['length']) ? 50 : intval($_REQUEST['length']);
    $sort_by = empty($_REQUEST['sort_by']) ? "rec_id" : trim($_REQUEST['sort_by']);
    $sort_order = empty($_REQUEST['sort_order']) ? "DESC" : trim($_REQUEST['sort_order']);

    $sql = "SELECT cpi.*, oi.order_sn, oi.pay_time, oi.platform, oi.goods_amount, oi.order_status, oi.pay_status, cpi.coupon_id, c.coupon_code, c.end_time FROM " . $ecs->table("coupon_promote_info") . " cpi " .
            "LEFT JOIN " . $ecs->table("order_info") . " oi ON oi.order_id = cpi.order_id " .
            "LEFT JOIN " . $ecs->table("coupons") . " c ON c.coupon_id = cpi.coupon_id " .
            "WHERE cpi.promote_id = $promote_id " .
            "ORDER BY $sort_by $sort_order " .
            "LIMIT $start, $length";
    $list = $db->getAll($sql);
    foreach ($list as $key => $row) {
        $data = json_decode($row['data'], true);
        $list[$key]['pay_date'] = empty($row['pay_time']) ? "--" : local_date("Y-m-d H:i:s", $row['pay_time']);
        if ($promote_id == 1) {
            $clubInfo = calculate_the_club_point($row['goods_amount'], (empty($data['rewarded']) ? $row['pay_time'] : $row['end_time']), $row['platform'], $row['end_time']);
            $list[$key]['order_sn'] = "<a href='order.php?act=info&order_id=$row[order_id]' target='_blank'>$row[order_sn]</a>";
            $list[$key]['coupon_code'] = "<a href='coupon.php?act=info&id=$row[coupon_id]' target='_blank'>$row[coupon_code]</a>";
            $list[$key]['valid_reward'] = empty($data['rewarded']) ? $clubInfo['reward'] : "<span class='green'>$data[rewarded]</span>";
            $list[$key]['valid_accrual'] = $theClubAccrual[$clubInfo['accrual']]['chin'];
            if (!in_array($row['order_status'], [OS_CONFIRMED, OS_SPLITED, OS_SPLITING_PART]) || $row['pay_status'] != PS_PAYED) {
                $list[$key]['valid_reward'] = 0;
                $list[$key]['valid_accrual'] = "<span class='red' title='非法狀態'>不合資格</span>";
            }
        }
        foreach ($data as $k => $v) {
            $list[$key][$k] = $v;
        }
    }
    $count = $db->getOne("SELECT COUNT(rec_id) FROM " . $ecs->table("coupon_promote_info") . " WHERE promote_id = $promote_id");
    $controller->ajaxQueryAction($list, $count, false);
}
elseif ($_REQUEST['act'] == 'promote_export') {
    ini_set('memory_limit', '256M');
    define('PCLZIP_TEMPORARY_DIR', ROOT_PATH . 'temp/');

    include_once ROOT_PATH . "includes/lib_order.php";

    $promote_id = empty($_REQUEST['promote_id']) ? 0 : intval($_REQUEST['promote_id']);
    $sql = "SELECT cpi.*, oi.order_sn, oi.pay_time, oi.platform, oi.goods_amount, c.end_time FROM " . $ecs->table("coupon_promote_info") . " cpi " .
            "LEFT JOIN " . $ecs->table("order_info") . " oi ON oi.order_id = cpi.order_id " .
            "LEFT JOIN " . $ecs->table("coupons") . " c ON c.coupon_id = cpi.coupon_id " .
            "WHERE cpi.promote_id = $promote_id AND oi.pay_status = " . PS_PAYED .
            " AND oi.order_status " . db_create_in(array(OS_CONFIRMED, OS_SPLITED, OS_SPLITING_PART)) .
            " AND oi.pay_time < c.end_time" .
            " ORDER BY oi.pay_time ASC";
    $list = $db->getAll($sql);
    if (empty($promote_id) || empty($list)) {
        sys_msg("系統沒有紀錄");
    }

    $promote_name = $db->getOne("SELECT promote_name FROM " . $ecs->table("coupon_promote") . " WHERE promote_id = $promote_id");
    $promote_name = strtolower(str_replace(" ", "-", $promote_name));

    include_once ROOT_PATH . 'includes/excel/PHPExcel.php';
    include_once ROOT_PATH . 'includes/excel/PHPExcel/IOFactory.php';
    \PHPExcel_Settings::setZipClass(\PHPExcel_Settings::PCLZIP);
    $fileName     = ROOT_PATH . "data/promotion/$promote_name.xlsx";
    if (!file_exists($fileName)) {
        sys_msg("無法下載");
    }
    $newFile      = "$promote_name-" . gmtime() . ".xlsx";
    $fileType     = \PHPExcel_IOFactory::identify($fileName);
    $objReader    = \PHPExcel_IOFactory::createReader($fileType);
    $objPHPExcel  = $objReader->load($fileName);
    $objWorksheet = $objPHPExcel->getSheet(0);
    $current_row = 2;

    $update = [];
    foreach ($list as $key => $row) {
        if (floatval($row['goods_amount']) < 1000) {
            continue;
        }
        $data = \json_decode($row['data'], true);
        if (!empty($data['exported'])) {
            $theClubMaxPoint -= intval($data['rewarded']);
            continue;
        }
        if ($theClubMaxPoint <= 0) {
            continue;
        }
        if (!empty($_REQUEST['ftp'])) {
            $update[] = $row['rec_id'];
        }
        $txn_date = local_date("Ymd", $row['pay_time']);
        $clubInfo = calculate_the_club_point($row['goods_amount'], $row['pay_time'], $row['platform'], $row['end_time'], false);
        $valid_reward = $clubInfo['reward'];
        $count = $clubInfo['count'];
        $data['exported'] = 1;
        $data['rewarded'] = $valid_reward;
        $theClubMaxPoint -= $valid_reward;
        $db->query("UPDATE " . $ecs->table("coupon_promote_info") . " SET data = '" . \json_encode($data) . "' WHERE rec_id = $row[rec_id]");

        while ($count > 0) {
            $count--;
            $objWorksheet->setCellValueByColumnAndRow(0, $current_row, $theClubAccrual[$clubInfo['accrual']]['code']);
            $objWorksheet->setCellValueByColumnAndRow(1, $current_row, "YOHO");
            $objWorksheet->setCellValueByColumnAndRow(2, $current_row, $theClubAccrual[$clubInfo['accrual']]['eng']);
            $objWorksheet->setCellValueByColumnAndRow(3, $current_row, $theClubAccrual[$clubInfo['accrual']]['chin']);
            $objWorksheet->setCellValueByColumnAndRow(4, $current_row, $txn_date);
            $objWorksheet->setCellValueByColumnAndRow(5, $current_row, $data['theClubId']);
            $objWorksheet->setCellValueByColumnAndRow(8, $current_row, $row['order_sn']);
            $objWorksheet->setCellValueByColumnAndRow(9, $current_row, "Credit");
            $objWorksheet->setCellValueByColumnAndRow(10, $current_row, floor($valid_reward / $clubInfo['count']));
            $current_row++;
        }
    }
    $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, $fileType);
    $objWriter->setPreCalculateFormulas(false);

    if (empty($_REQUEST['ftp'])) {
        header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.‌​sheet; charset=utf-8');
        header('Content-Disposition: attachment; filename=' . $newFile);
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
    } else {
        $save_to = PCLZIP_TEMPORARY_DIR . $newFile;
        $objWriter->save($save_to);

        $fp = fopen($save_to, 'r');
        $ftp = ftp_connect('localhost');
        $success = ftp_fput($ftp, $newFile, $fp, FTP_ASCII);
        ftp_close($ftp);
        fclose($fp);
        unlink($save_to);

        if ($success) {
            foreach ($list as $row) {
                if (in_array($row['rec_id'], $update)) {
                    $data = json_decode($row['data'], true);
                    $data['exported'] = 1;
                    $data['rewarded'] = $row['rewarded'];
                    $db->query("UPDATE " . $ecs->table("coupon_promote_info") . " SET data = '" . \json_encode($data) . "' WHERE rec_id = $row[rec_id]");
                }
            }
            sys_msg("成功傳送");
        } else {
            if (!empty($_REQUEST['debug'])) {
                die(var_dump(error_get_last()));
            }
            sys_msg("傳送失敗", 1);
        }
    }
    exit;
}
elseif ($_REQUEST['act'] == 'update_one_time') {
    $sql = "SELECT coupon_id, reusable, users_list FROM " . $GLOBALS['ecs']->table('coupons');
    $list = $db->getAll($sql);
    foreach ($list as $key => $row) {
        $coupon_id = $row["coupon_id"];
        $reusable = $row["reusable"];
        $users_list = $row["users_list"];
        if ($reusable == '1') {
            $sql = "UPDATE " . $ecs->table('coupons') .
                " SET user_limit = '0', max_user_used = '0', times_limit = '0', max_times_used = '0' " .
                " WHERE coupon_id = '" . $coupon_id . "'";       
        } else {
            if ($users_list == '') {
                 //無適用會員，限制1人，限制1次
                $sql = "UPDATE " . $ecs->table('coupons') .
                " SET user_limit = '1', max_user_used = '1', times_limit = '1', max_times_used = '1' " .
                " WHERE coupon_id = '" . $coupon_id . "'";
            } else {
                 //有適用會員，限制1次
                $sql = "UPDATE " . $ecs->table('coupons') .
                " SET user_limit = '0', max_user_used = '0', times_limit = '1', max_times_used = '1' " .
                " WHERE coupon_id = '" . $coupon_id . "'";
            }
        }
        $db->query($sql);
    }
}
function get_coupon($allow_manage)
{
    global $controller;
    $_REQUEST['sort_by']      = empty($_REQUEST['sort_by']) ? 'coupon_id' : trim($_REQUEST['sort_by']);
    $_REQUEST['sort_order']   = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
    
    $where = '1 ';
    
    // If not allowed to manage coupons, only show active coupons
    if (!$allow_manage)
    {
        $where .= " AND ('" . gmtime() . "' BETWEEN c.`start_time` AND c.`end_time`) ";
    }
    //if set keyword
    if (isset($_POST['keyword'])) {
        $keyword = $_POST['keyword'];
        $where .= " AND c.coupon_name like '%{$keyword}%' OR c.coupon_code like '%{$keyword}%' ";
    } 
    $sql = "SELECT count(*) FROM " . $GLOBALS['ecs']->table('coupons') . " as c WHERE " . $where;
    $_REQUEST['record_count'] = $GLOBALS['db']->getOne($sql);

    /* 查询 */
    $sql = "SELECT c.*, IFNULL(u.user_used_exclude_os_unconfirmed,0) AS user_used_exclude_os_unconfirmed, IFNULL(uc.user_used_include_os_unconfirmed,0) AS user_used_include_os_unconfirmed  FROM " . $GLOBALS['ecs']->table('coupons') . " as c " .
            "LEFT JOIN (SELECT COUNT(DISTINCT  cl.user_id) AS user_used_exclude_os_unconfirmed, cl.coupon_id FROM " . $GLOBALS['ecs']->table('coupon_log') . " AS cl " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('order_info') . " AS oi ON oi.order_id = cl.order_id " .
            "WHERE oi.order_status NOT IN(".OS_UNCONFIRMED.",".OS_CANCELED.",".OS_INVALID.") AND oi.pay_status = '2' " .
            "GROUP BY cl.`coupon_id`) AS u ON u.coupon_id = c.coupon_id " .
            "LEFT JOIN (SELECT COUNT(DISTINCT  cl.user_id) AS user_used_include_os_unconfirmed, cl.coupon_id FROM " . $GLOBALS['ecs']->table('coupon_log') . " AS cl " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('order_info') . " AS oi ON oi.order_id = cl.order_id " .
            "WHERE oi.order_status NOT IN(".OS_CANCELED.",".OS_INVALID.") " .
            "GROUP BY cl.`coupon_id`) AS uc ON uc.coupon_id = c.coupon_id " .
            "WHERE " . $where .
            "ORDER by " . $_REQUEST['sort_by'] . " " . $_REQUEST['sort_order'] . " " .
            "LIMIT " . $_REQUEST['start'] . "," . $_REQUEST['page_size'];
    $data = $GLOBALS['db']->getAll($sql);
    
    foreach ($data as $key => $row)
    {
        $data[$key]['user_used'] = $row['user_used'];
        $data[$key]['coupon_amount_formatted'] = price_format($row['coupon_amount'], false);
        $data[$key]['coupon_discount_formatted'] = $row['coupon_discount'] . '%';
        $data[$key]['min_goods_amount_formatted'] = price_format($row['min_goods_amount'], false);
        
        $data[$key]['start_date'] = local_date($GLOBALS['_CFG']['time_format'], $row['start_time']);
        $data[$key]['end_date'] = local_date($GLOBALS['_CFG']['time_format'], $row['end_time']);
        $data[$key]['active'] = ((gmtime() >= $row['start_time']) && (gmtime() <= $row['end_time']));
        $data[$key]['have_goods'] = (!empty($row['goods_list']));
        $data[$key]['have_users'] = (!empty($row['users_list']));
        $data[$key]['_action']    = $controller->generateCrudActionBtn($row['coupon_id'], 'id', null, ['edit'], ['log'=>['label'=>'記錄']]);
    }

    $arr = array('data' => $data, 'record_count' => $_REQUEST['record_count']);

    return $arr;
}

function get_coupon_log()
{
    $filter['coupon_id']    = empty($_REQUEST['id']) ? 0 : intval($_REQUEST['id']);
    
    $filter['sort_by']      = empty($_REQUEST['sort_by']) ? 'log_id' : trim($_REQUEST['sort_by']);
    $filter['sort_order']   = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
    
    $where = " 1 ";
    if (!empty($filter['coupon_id']))
    {
        $where .= " AND cl.`coupon_id` = '" . $filter['coupon_id'] . "' ";
    }

    $sql = "SELECT count(*) FROM " . $GLOBALS['ecs']->table('coupon_log') . " as cl WHERE " . $where;
    $record_count = $GLOBALS['db']->getOne($sql);

    /* 查询 */
    $sql = "SELECT cl.*, u.`user_name`, rei.`content` as display_name, o.`order_sn` ";
    $sql.= " FROM " . $GLOBALS['ecs']->table('coupon_log') . " as cl ";
    $sql.= " LEFT JOIN " . $GLOBALS['ecs']->table('users') . " as u ON cl.`user_id` = u.`user_id` ";
    $sql.= " LEFT JOIN " . $GLOBALS['ecs']->table('reg_extend_info') . " as rei ON rei.`user_id` = u.`user_id` AND rei.`reg_field_id` = 10 ";
    $sql.= " LEFT JOIN " . $GLOBALS['ecs']->table('order_info') . " as o ON cl.`order_id` = o.`order_id` ";
    $sql.= " WHERE " . $where;
    $sql.= " ORDER by " . $filter['sort_by'] . ' ' . $filter['sort_order'];
    $sql.= " LIMIT " . $_REQUEST['start'] . "," . $_REQUEST['page_size'];
    $data = $GLOBALS['db']->getAll($sql);

    foreach ($data as $key => $row)
    {
        $data[$key]['display_name'] = empty($row['display_name']) ? $row['user_name'] : $row['display_name'];
        $data[$key]['display_name'] = "<a href='users.php?act=list&keywords=$row[user_name]' title='$row[user_name]'>" . $data[$key]['display_name'] . "</a>";
        $data[$key]['use_date'] = local_date($GLOBALS['_CFG']['time_format'], $row['use_time']);
        $data[$key]['order_sn'] = "<a href='order.php?act=info&order_id=$row[order_id]'>$row[order_sn]</a>";
    }

    $arr = array('data' => $data, 'filter' => $filter, 'page_count' => $filter['page_count'], 'record_count' => $record_count);

    return $arr;
}

function get_coupon_info($coupon_id)
{
    $coupon_id = intval($coupon_id);
    
    if ($coupon_id <= 0)
    {
        return false;
    }
    
    $sql = "SELECT * ";
    $sql.= " FROM " . $GLOBALS['ecs']->table('coupons');
    $sql.= " WHERE `coupon_id` = '" . $coupon_id . "'";
    
    $coupon = $GLOBALS['db']->getRow($sql);
    
    if (!$coupon)
    {
        return false;
    }
    
    $coupon['coupon_amount_formatted'] = price_format($coupon['coupon_amount'], false);
    $coupon['coupon_discount_formatted'] = $coupon['coupon_discount'] . '%';
    $coupon['min_goods_amount_formatted'] = price_format($coupon['min_goods_amount'], false);
    
    $coupon['start_date'] = local_date($GLOBALS['_CFG']['time_format'], $coupon['start_time']);
    $coupon['end_date'] = local_date($GLOBALS['_CFG']['time_format'], $coupon['end_time']);
    
    if (!empty($coupon['goods_list']))
    {
        $goods_list = json_decode($coupon['goods_list'], true);
        
        $goods_ids = array_reduce($goods_list, 'array_merge', array());
        $sql = "SELECT goods_id, goods_name ";
        $sql.= " FROM " . $GLOBALS['ecs']->table('goods');
        $sql.= " WHERE goods_id " . db_create_in($goods_ids);
        $res = $GLOBALS['db']->getAll($sql);
        
        $coupon['goods_list_arr'] = array();
        foreach ($goods_list as $goods_set)
        {
            $arr = array();
            foreach ($goods_set as $goods_id)
            {
                foreach ($res as $g)
                {
                    if ($g['goods_id'] == $goods_id)
                    {
                        $arr[] = $g;
                        break;
                    }
                }
            }
            $coupon['goods_list_arr'][] = $arr;
        }
    }
    if (!empty($coupon['users_list']))
    {
        $users_list = json_decode($coupon['users_list'], true);
        
        $sql = "SELECT u.`user_name`, rei.`content` as display_name ";
        $sql.= " FROM " . $GLOBALS['ecs']->table('users') . " as u ";
        $sql.= " LEFT JOIN " . $GLOBALS['ecs']->table('reg_extend_info') . " as rei ON rei.`user_id` = u.`user_id` AND rei.`reg_field_id` = 10 ";
        $sql.= " WHERE u.`user_id` " . db_create_in($users_list);
        
        $coupon['users_list_arr'] = $GLOBALS['db']->getAll($sql);
    }
    if (!empty($coupon['cat_list']))
    {
        $cat_list = explode(",", $coupon['cat_list']);
        
        $sql = "SELECT c.cat_name, cat_id ";
        $sql.= " FROM " . $GLOBALS['ecs']->table('category') . " as c ";
        $sql.= " WHERE c.`cat_id` " . db_create_in($cat_list);
        $coupon['cat_list']     = $cat_list;
        $coupon['cat_list_arr'] = $GLOBALS['db']->getAll($sql);
    }
    if (!empty($coupon['exclude_cat_list']))
    {
        $exclude_cat_list = explode(",", $coupon['exclude_cat_list']);
        
        $sql = "SELECT c.`cat_name`, cat_id ";
        $sql.= " FROM " . $GLOBALS['ecs']->table('category') . " as c ";
        $sql.= " WHERE c.`cat_id` " . db_create_in($exclude_cat_list);
        $coupon['exclude_cat_list']     = $exclude_cat_list;
        $coupon['exclude_cat_list_arr'] = $GLOBALS['db']->getAll($sql);
    }
    if (!empty($coupon['brand_list']))
    {
        $brand_list = explode(",", $coupon['brand_list']);
        
        $sql = "SELECT b.`brand_name`, brand_id ";
        $sql.= " FROM " . $GLOBALS['ecs']->table('brand') . " as b ";
        $sql.= " WHERE b.`brand_id` " . db_create_in($brand_list);
        $coupon['brand_list']     = $brand_list;
        $coupon['brand_list_arr'] = $GLOBALS['db']->getAll($sql);
    }
    if (!empty($coupon['exclude_brand_list']))
    {
        $exclude_brand_list = explode(",", $coupon['exclude_brand_list']);
        
        $sql = "SELECT b.`brand_name`, brand_id ";
        $sql.= " FROM " . $GLOBALS['ecs']->table('brand') . " as b ";
        $sql.= " WHERE b.`brand_id` " . db_create_in($exclude_brand_list);
        $coupon['exclude_brand_list']     = $brand_list;
        $coupon['exclude_brand_list_arr'] = $GLOBALS['db']->getAll($sql);
    }
    
    return $coupon;
}

function calculate_the_club_point($goods_amount, $pay_time, $platform, $end_time, $list = true)
{
    global $theClubAccrual;
    $goods_amount = floatval($goods_amount);
    if ($goods_amount < 1000) {
        return [
            'reward'    => 0,
            'accrual'   => 3
        ];
    }
    if ($end_time < $pay_time) {
        return [
            'reward'    => 0,
            'accrual'   => 4
        ];
    }
    if ($platform == Yoho\cms\Controller\OrderController::ORDER_FEE_PLATFORM_POS) {
        if ($end_time <= 1546243200) {                      // from 2018-11-27 to 2019-01-01
            $count = floor($goods_amount / 500);
            $reward = $count * 50;
            $accrual = 1;
            if (local_date("N", $pay_time) === "7") {
                $reward *= 2;
                $accrual = 2;
            }
        } elseif ($end_time <= 1556611200) {                // from 2019-01-22 to 2019-05-01
            $count = 1;
            $reward = floor($goods_amount / 10);
            $accrual = 5;
            if (local_date("N", $pay_time) === "7") {
                $reward *= 2;
                $accrual = 6;
            }
        }
        return [
            'reward'    => $reward,
            'accrual'   => $accrual,
            'count'     => $count,
        ];
    } else {
        return [
            'reward'    => floor($goods_amount / 10),
            'accrual'   => 0,
            'count'     => 1,
        ];
    }
    return 0;
}
?>