<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_base.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_warehouse.php');
require(dirname(__FILE__) .'/includes/ERP/cls/cls_date.php');

$adminuserController = new Yoho\cms\Controller\AdminuserController();
$shopController = new Yoho\cms\Controller\ShopController();
$erpController = new Yoho\cms\Controller\ErpController();

if($_REQUEST['act'] == 'list' || $_REQUEST['act'] == ''){
	if( admin_priv('erp_warehouse_view','',false) || admin_priv('erp_warehouse_manage','',false) || admin_priv('erp_warehouse_approve','',false))
	{
        include('./includes/ERP/page.class.php');

		$shop_warehouse_id = 1; // shop
		$opeator_options = $shopController->getOperatorOptions();
		$sellable_warehouse_info = $erpController->getSellableWarehouseInfo();
		
		$smarty->assign('shop_warehouse_id',      $shop_warehouse_id);
		$smarty->assign('ur_here',      '門市補貨');
		$smarty->assign('opeator_options', $opeator_options);
		$smarty->assign('sellable_warehouse_info', $sellable_warehouse_info);
        $smarty->assign('full_page', 1);
        $smarty->assign('start_date', $result['start_date']);
        $smarty->assign('end_date', $result['end_date']);
        $smarty->assign('cfg_lang', $_CFG['lang']);
        $smarty->assign('lang', $_LANG);
        $smarty->display('erp_shop_restore_list.htm');
    }else{
        $href="index.php";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
    }

} elseif ($_REQUEST['act'] == 'query') {
   $result = $shopController->getShopOrderGoodsSoldList();
   $shopController->ajaxQueryAction($result['data'],$result['record_count'],false);
} elseif ($_REQUEST['act'] == 'custom_goods_query') {
	$result = $shopController->getCustomGoodsList();
	$shopController->ajaxQueryAction($result['data'],$result['record_count'],false);
} elseif ($_REQUEST['act'] == 'check_goods_exist') {
	$result = $shopController->checkGoodsExist($_REQUEST['goods_sn']);
	make_json_result($result);
} elseif ($_REQUEST['act'] == 'query_confirm_list') {
   $result = $shopController->getShopOrderGoodsSoldList();
   $shopController->ajaxQueryAction($result['data'],$result['record_count'],false);
} elseif ($_REQUEST['act'] == 'post_transfer_list_create') {
    $result = $shopController->transferListCreate();
	if(isset($result['error'])){
		make_json_error($result['error']);
	}else{
		make_json_result($result);
	}
}



?>