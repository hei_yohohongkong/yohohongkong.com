<?php

/* * *
 * erp_sales_agent.php
 * by Anthony 2017-04-013
 *
 * Sales agent backend function
 * * */
define('IN_ECS', true);
require(dirname(__FILE__) . '/includes/init.php');
require(dirname(__FILE__) . '/includes/ERP/lib_erp_base.php');
require(dirname(__FILE__) . '/includes/ERP/lib_erp_common.php');
$salesagentController = new Yoho\cms\Controller\SalesagentController();
$orderController = new Yoho\cms\Controller\OrderController();

use Yoho\cms\Model;

switch ($_REQUEST['act']) {

	case 'list':
		/* 检查权限 */
		//admin_priv('erp_sys_manage');
		$list = $salesagentController->get_sales_agent_list();
		// title
		$smarty->assign('ur_here', $_LANG['sales_agent']);
		$smarty->assign('full_page', 1);
		//function button
		$smarty->assign('action_link', array('text' => '新增代理銷售', 'href' => 'erp_sales_agent.php?act=add'));
		// list
		$smarty->assign('sales_agent_list', $list['data']);
		$smarty->assign('filter', $list['filter']);
		$smarty->assign('record_count', $list['record_count']);
		$smarty->assign('page_count', $list['page_count']);
		$sort_flag = sort_flag($list['filter']);
		$smarty->assign($sort_flag['tag'], $sort_flag['img']);
		assign_query_info();

		$smarty->display('erp_sales_agent_list.htm');
		break;

	case 'query':
		$list = $salesagentController->get_sales_agent_list();
		$smarty->assign('sales_agent_list', $list['data']);
		$smarty->assign('filter', $list['filter']);
		$smarty->assign('record_count', $list['record_count']);
		$smarty->assign('page_count', $list['page_count']);

		$sort_flag = sort_flag($list['filter']);
		$smarty->assign($sort_flag['tag'], $sort_flag['img']);

		make_json_result($smarty->fetch('erp_sales_agent_list.htm'), '', array('filter' => $list['filter'], 'page_count' => $list['page_count']));
		break;

	case 'add':
		/* 检查权限 */
		//admin_priv('erp_sys_manage');

		$smarty->assign('action_link', array('text' => '代理銷售列表', 'href' => 'erp_sales_agent.php?act=list'));
        $smarty->assign('ur_here', '新增代理銷售');
		$smarty->assign('form_action', 'insert');
		$smarty->display('erp_sales_agent_info.htm');
		break;

	case 'edit':
		/* 检查权限 */
		//admin_priv('erp_sys_manage');
		//function button
		$smarty->assign('action_link', array('text' => '代理銷售列表', 'href' => 'erp_sales_agent.php?act=list'));
		$sale_agent = new Model\Salesagent($_REQUEST['id']);
		$data = $sale_agent->data;
		$rates = $sale_agent->getAllRates();
		$goods_list = $sale_agent->getAgentGoods();

		if (empty($goods_list)){
			$count_goods = 0;
		} else {
			$count_goods = sizeof($goods_list);
		}

		assign_query_info();

        $smarty->assign('ur_here', '修改代理銷售');
		$smarty->assign('rates', $rates);
		$smarty->assign('goods_list', $goods_list);
		$smarty->assign('count_goods', $count_goods);
		$smarty->assign('form_action', 'insert');
		$smarty->assign('sale_agent', $data);
		$smarty->assign('sa_id', $_REQUEST['id']);
		$smarty->display('erp_sales_agent_info.htm');
		break;

	case 'good_list':
		/* 检查权限 */
		//admin_priv('erp_sys_manage');
		//function button
		$smarty->assign('action_link', array('text' => '代理銷售列表', 'href' => 'erp_sales_agent.php?act=list'));
		$smarty->assign('action_link2', array('text' => '編輯此代理銷售', 'href' => 'erp_sales_agent.php?act=edit&id='.$_REQUEST['id']));
		$sale_agent = new Model\Salesagent($_REQUEST['id']);
		$data = $sale_agent->data;
		$goods_list = $sale_agent->getAgentGoods();
		$sa_rate = $orderController->getSaRate($_REQUEST['id']);
		if ($sa_rate == ""){
			$sa_rate = "<span style='color: red; font-size: 16px; font-weight: bold;'>未設定! 請立即設定預設手續費</span>";
		} else {
			$sa_rate = $sa_rate."%";
		}

		if (empty($goods_list)){
			$count_goods = 0;
		} else {
			$count_goods = sizeof($goods_list);
		}

		assign_query_info();

		$smarty->assign('ur_here', '代理銷售商品列表');
		$smarty->assign('goods_list', $goods_list);
		$smarty->assign('count_goods', $count_goods);
		$smarty->assign('sale_agent', $data);
		$smarty->assign('sa_id', $_REQUEST['id']);
		$smarty->assign('sa_rate', $sa_rate);
		$smarty->display('erp_sales_agent_goods.htm');
		break;
	
	case 'insert':
		/* 检查权限 */
		//admin_priv('erp_sys_manage');
		
		/*  Check Insert or update  */
		if($_POST['sale_agent_id']){ // Update
			$salesagentController->update_sale_agent($_REQUEST);
		} else { // Insert
			$salesagentController->insert_sale_agent($_REQUEST);
		}

		clear_cache_files();
		$link[0]['text'] = '返回列表';
		$link[0]['href'] = 'erp_sales_agent.php?act=list';
		if($_POST['sale_agent_id']){
			sys_msg('已修改代理銷售', 0, $link, true);
		} else {
			sys_msg('已新增代理銷售', 0, $link, true);
		}
		break;

	case 'change_is_valid':

		include('../includes/cls_json.php');
		$json = new JSON;

		$result = $salesagentController->change_is_valid($_REQUEST['sales_agent_id']);
		die($json->encode($result));

		break;

	case 'ajax_search_goods':

		include('../includes/cls_json.php');
		$json = new JSON;

		if (isset($_REQUEST['keyword'])){
			$keyword = mysql_real_escape_string(trim($_REQUEST['keyword']));
		} else {
			$keyword = "";
		}
		if ($keyword != ""){
			$result = $salesagentController->searchGoods($keyword);
		} else {
			$result = FALSE;
		}

		make_json_result($result);

		break;

}