<?php

/***
* sale_payment.php
* by howang 2014-07-28
*
* Daily received payment list for YOHO Hong Kong
***/


define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require_once(ROOT_PATH . 'includes/lib_order.php');
require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/admin/statistic.php');
$smarty->assign('lang', $_LANG);
$userController = new Yoho\cms\Controller\UserController();
if (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'query' ||  $_REQUEST['act'] == 'download'))
{
    /* 检查权限 */
    check_authz_json('sale_order_stats');
    if (strstr($_REQUEST['start_date'], '-') === false)
    {
        $_REQUEST['start_date'] = local_date('Y-m-d', $_REQUEST['start_date']);
        $_REQUEST['end_date'] = local_date('Y-m-d', $_REQUEST['end_date']);
    }
    /*------------------------------------------------------ */
    //--Excel文件下载
    /*------------------------------------------------------ */
    if ($_REQUEST['act'] == 'download')
    {
        $file_name = $_REQUEST['start_date'].'_'.$_REQUEST['end_date'] . '_payment';
        $goods_sales_list = get_sale_payment(false);
        header("Content-type: application/vnd.ms-excel; charset=utf-8");
        header("Content-Disposition: attachment; filename=$file_name.xls");

        require_once ROOT_PATH . 'includes/excel/PHPExcel.php';
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        
        $objPHPExcel->getActiveSheet()->setTitle('門店帳目');
        
        /* 文件标题 */
        $objPHPExcel->getActiveSheet()
        ->setCellValue('A1', ecs_iconv(EC_CHARSET, 'UTF8', $_REQUEST['start_date']. $_LANG['to'] .$_REQUEST['end_date']. ' 門店帳目'))
        ->setCellValue('A2', ecs_iconv(EC_CHARSET, 'UTF8', '日期'))
        ->setCellValue('B2', ecs_iconv(EC_CHARSET, 'UTF8', '訂單總數'))
        ->setCellValue('C2', ecs_iconv(EC_CHARSET, 'UTF8', '銷售總額'))
        ->setCellValue('D2', ecs_iconv(EC_CHARSET, 'UTF8', '現金'))
        ->setCellValue('E2', ecs_iconv(EC_CHARSET, 'UTF8', 'EPS'))
        ->setCellValue('F2', ecs_iconv(EC_CHARSET, 'UTF8', '銀行轉帳'))
        ->setCellValue('G2', ecs_iconv(EC_CHARSET, 'UTF8', 'Paypal'))
        ->setCellValue('H2', ecs_iconv(EC_CHARSET, 'UTF8', '支付寶'))
        ->setCellValue('I2', ecs_iconv(EC_CHARSET, 'UTF8', '銀聯卡'))
        ->setCellValue('J2', ecs_iconv(EC_CHARSET, 'UTF8', 'Visa / MasterCard'))
        ->setCellValue('K2', ecs_iconv(EC_CHARSET, 'UTF8', '美國運通卡'))
        ->setCellValue('L2', ecs_iconv(EC_CHARSET, 'UTF8', '信用卡(Stripe)'));
        $objPHPExcel->getActiveSheet()->mergeCells('A1:L1');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
        
        $i = 3;
        foreach ($goods_sales_list['sale_payment_data'] AS $key => $value)
        {
            $objPHPExcel->getActiveSheet()
            ->setCellValue('A'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['date']))
            ->setCellValue('B'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['order_count']))
            ->setCellValue('C'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['total_amount']))
            ->setCellValue('D'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['amount_by_payment']['cash']))
            ->setCellValue('E'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['amount_by_payment']['eps']))
            ->setCellValue('F'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['amount_by_payment']['bank']))
            ->setCellValue('G'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['amount_by_payment']['paypal']))
            ->setCellValue('H'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['amount_by_payment']['alipay']))
            ->setCellValue('I'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['amount_by_payment']['unionpay']))
            ->setCellValue('J'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['amount_by_payment']['visamaster']))
            ->setCellValue('K'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['amount_by_payment']['americanexpress']))
            ->setCellValue('L'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['amount_by_payment']['stripe']));
            $objPHPExcel->getActiveSheet()->getStyle('C'.$i)->getNumberFormat()->setFormatCode('$0.00');
            $objPHPExcel->getActiveSheet()->getStyle('D'.$i)->getNumberFormat()->setFormatCode('$0.00');
            $objPHPExcel->getActiveSheet()->getStyle('E'.$i)->getNumberFormat()->setFormatCode('$0.00');
            $objPHPExcel->getActiveSheet()->getStyle('F'.$i)->getNumberFormat()->setFormatCode('$0.00');
            $objPHPExcel->getActiveSheet()->getStyle('G'.$i)->getNumberFormat()->setFormatCode('$0.00');
            $objPHPExcel->getActiveSheet()->getStyle('H'.$i)->getNumberFormat()->setFormatCode('$0.00');
            $objPHPExcel->getActiveSheet()->getStyle('I'.$i)->getNumberFormat()->setFormatCode('$0.00');
            $objPHPExcel->getActiveSheet()->getStyle('J'.$i)->getNumberFormat()->setFormatCode('$0.00');
            $objPHPExcel->getActiveSheet()->getStyle('K'.$i)->getNumberFormat()->setFormatCode('$0.00');
            $objPHPExcel->getActiveSheet()->getStyle('L'.$i)->getNumberFormat()->setFormatCode('$0.00');
            $i++;
        }
        
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, ecs_iconv(EC_CHARSET, 'UTF8', '總計'));
        for ($col = 'B'; $col <= 'L'; $col++)
        {
            $sum_formula = '=SUM('.$col.'3:'.$col.($i-1).')';
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($col.$i, $sum_formula, PHPExcel_Cell_DataType::TYPE_FORMULA);
            if ($col != 'B')
            {
                $objPHPExcel->getActiveSheet()->getStyle($col.$i)->getNumberFormat()->setFormatCode('$0.00');
            }
        }
        
        for ($col = 'A'; $col <= 'L'; $col++)
        {
            $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
        }
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        
        exit;
    }
    $sale_payment_data = get_sale_payment();
    $smarty->assign('payment_list',    $sale_payment_data['sale_payment_data']);
    $smarty->assign('filter',          $sale_payment_data['filter']);
    $smarty->assign('record_count',    $sale_payment_data['record_count']);
    $smarty->assign('page_count',      $sale_payment_data['page_count']);
    $smarty->assign('start_date',      $_REQUEST['start_date']);
    $smarty->assign('end_date',        $_REQUEST['end_date']);

    make_json_result($smarty->fetch('sale_payment.htm'), '', array('filter' => $sale_payment_data['filter'], 'page_count' => $sale_payment_data['page_count']));
}
/*------------------------------------------------------ */
//--商品明细列表
/*------------------------------------------------------ */
else
{
    /* 权限判断 */
    admin_priv('sale_order_stats');
    /* 时间参数 */
    if (!isset($_REQUEST['start_date']))
    {
        $start_date = local_strtotime(local_date('Y-m-01'));
    }
    if (!isset($_REQUEST['end_date']))
    {
        $end_date = local_strtotime(local_date('Y-m-t'));
    }
    
    $sale_payment_data = get_sale_payment();
    /* 赋值到模板 */
    $smarty->assign('filter',       $sale_payment_data['filter']);
    $smarty->assign('record_count', $sale_payment_data['record_count']);
    $smarty->assign('page_count',   $sale_payment_data['page_count']);
    $smarty->assign('payment_list', $sale_payment_data['sale_payment_data']);
    $smarty->assign('ur_here',      $_LANG['sale_payment']);
    $smarty->assign('full_page',    1);
    $smarty->assign('start_date',   local_date('Y-m-d', $start_date));
    $smarty->assign('end_date',     local_date('Y-m-d', $end_date));
    $smarty->assign('cfg_lang',     $_CFG['lang']);
    $smarty->assign('action_link',  array('text' => '下載門店帳目報表','href'=>'#download'));
    
    /* 显示页面 */
    assign_query_info();
    $smarty->display('sale_payment.htm');
}

function get_sale_payment($is_pagination = true){

    /* 时间参数 */
    $filter['start_date'] = empty($_REQUEST['start_date']) ? local_strtotime(local_date('Y-m-01')) : local_strtotime($_REQUEST['start_date']);
    $filter['end_date'] = empty($_REQUEST['end_date']) ? local_strtotime(local_date('Y-m-t')) : local_strtotime($_REQUEST['end_date']);

    $filter['is_ex_pos'] = isset($_REQUEST['is_ex_pos'])?$_REQUEST['is_ex_pos'] : false;
    $filter['record_count'] = floor(abs($filter['start_date'] - $filter['end_date']) / 86400) + 1;
    
    /* 分页大小 */
    $filter = page_and_size($filter);
    
    $sale_payment_data = array();
    for($start_time = $filter['start_date'] + $filter['start'] * 86400;
        $start_time <= $filter['end_date'] && $start_time <= $filter['start_date'] + ($filter['start'] + $filter['page_size']) * 86400;
        $start_time += 86400)
    {
        $sale_payment_data[] = get_order_payment('shipped', $start_time, $start_time + 86399);
    }
    
    $arr = array(
        'sale_payment_data' => $sale_payment_data,
        'filter' => $filter,
        'page_count' => $filter['page_count'],
        'record_count' => $filter['record_count']
    );
    return $arr;
}

function get_order_payment($filter_mode = 'paid', $start_time = 0, $end_time = 0)
{
    global $db,$ecs,$userController;
    
    $start_time = empty($start_time) ? local_mktime(0, 0, 0, local_date('m'), local_date('d'), local_date('Y')) : $start_time;
    $end_time = empty($end_time) ? local_mktime(23, 59, 59, local_date('m'), local_date('d'), local_date('Y')) : $end_time;
    $is_ex_pos = isset($_REQUEST['is_ex_pos'])?$_REQUEST['is_ex_pos'] : false;
    $arr = array();
    $arr['date'] = local_date('Y-m-d', $start_time);
    
    // Exclude orders for the followiung user accounts: 11111111, 88888888, 99999999
    // Exclude sales agent and wholesale
    $where_exclude .= " AND (order_type NOT IN ('" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT . "','" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE . "') OR (order_type IS NULL AND user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).") ) ) ";
    // Exclude sales agent
    $where_exclude .= " AND (order_type <> '" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT . "' OR order_type IS NULL) ";
    // Exclude 展場POS
    if($is_ex_pos) $where_exclude .= " AND (oi.platform = 'pos_exhibition') ";
    else $where_exclude .= " AND (oi.platform <> 'pos_exhibition') ";
    
    if ($filter_mode == 'shipped')
    {
        $where_status = " AND order_status NOT IN (" . OS_CANCELED . ',' . OS_INVALID . ',' . OS_RETURNED . ") ".
                        " AND shipping_status IN (" . SS_SHIPPED . ',' . SS_RECEIVED . ") ";
        $where_date = " AND shipping_time BETWEEN " . $start_time . " AND " . $end_time;
        
        $arr['order_list_url'] = 'order.php?act=list&ship_start_time=' . $start_time . '&ship_end_time=' . $end_time . '&composite_status=201';
    }
    else //if ($filter_mode == 'paid')
    {
        $where_status = " AND order_status NOT IN (" . OS_CANCELED . ',' . OS_INVALID . ',' . OS_RETURNED . ") ".
                        " AND pay_status = " . PS_PAYED . " ";
        $where_date = " AND add_time BETWEEN " . $start_time . " AND " . $end_time;
        
        $arr['order_list_url'] = 'order.php?act=list&pay_start_time=' . $start_time . '&pay_end_time=' . $end_time . '&pay_status=2';
    }

    $sql = "SELECT COUNT(*) as order_count, ".
    " SUM(`money_paid` + `order_amount`) as total_amount ". // howang: money_paid(已付金額) + order_amount(應付金額) = 訂單總金額
    " FROM " .$ecs->table('order_info')." as oi ".
    " WHERE 1 " . $where_exclude . $where_status . $where_date;
    $row = $db->GetRow($sql);
    $arr['order_count'] = (double)$row['order_count'];
    $arr['total_amount'] = (double)$row['total_amount'];
    $arr['total_amount_formated'] = price_format($arr['total_amount'], false);
    
    $arr['amount_by_payment'] = array(
        'cash' => 0,
        'eps' => 0,
        'bank' => 0,
        'paypal' => 0,
        'alipay' => 0,
        'unionpay' => 0,
        'franzpay' => 0,
        'visamaster' => 0,
        'americanexpress' => 0,
        'stripe' => 0,
        'braintree' => 0,
    );
    $sql = "SELECT oi.pay_id, p.pay_name, p.pay_code, SUM( oi.money_paid +  oi.order_amount) as total_amount ".
    "FROM " . $ecs->table('order_info') ."as oi ".
    "LEFT JOIN ". $ecs->table('payment') ."as p ON p.pay_id = oi.pay_id ".
    " WHERE 1 " . $where_exclude . $where_status . $where_date .
    " GROUP BY oi.pay_id";

    $res = $db->GetAll($sql);
    foreach ($res as $row)
    {
        if ($row['pay_id'] == 3)
        {
            $arr['amount_by_payment']['cash'] = $row['total_amount'];
        }
        elseif ($row['pay_id'] == 2)
        {
            $arr['amount_by_payment']['eps'] = $row['total_amount'];
        }
        elseif (($row['pay_id'] == 5) || ($row['pay_id'] == 9) || ($row['pay_id'] == 18) || ($row['pay_id'] == 19))
        {
            $arr['amount_by_payment']['bank'] += $row['total_amount'];
        }
        elseif (($row['pay_id'] == 7) || ($row['pay_id'] == 17))
        {
            $arr['amount_by_payment']['paypal'] += $row['total_amount'];
        }
        elseif ($row['pay_id'] == 10)
        {
            $arr['amount_by_payment']['unionpay'] = $row['total_amount'];
        }
        elseif ($row['pay_id'] == 11)
        {
            $arr['amount_by_payment']['franzpay'] = $row['total_amount'];
        }
        elseif ($row['pay_id'] == 12)
        {
            $arr['amount_by_payment']['visamaster'] = $row['total_amount'];
        }
        elseif ($row['pay_id'] == 13)
        {
            $arr['amount_by_payment']['americanexpress'] = $row['total_amount'];
        }
        elseif ($row['pay_id'] == 14)
        {
            $arr['amount_by_payment']['alipay'] = $row['total_amount'];
        }
        elseif ($row['pay_id'] == 15)
        {
            $arr['amount_by_payment']['stripe'] = $row['total_amount'];
        }
        elseif ($row['pay_id'] == 16)
        {
            $arr['amount_by_payment']['braintree'] = $row['total_amount'];
        }
        
    }
    $arr['amount_by_payment_formated']['cash'] = price_format($arr['amount_by_payment']['cash'], false);
    $arr['amount_by_payment_formated']['eps'] = price_format($arr['amount_by_payment']['eps'], false);
    $arr['amount_by_payment_formated']['bank'] = price_format($arr['amount_by_payment']['bank'], false);
    $arr['amount_by_payment_formated']['paypal'] = price_format($arr['amount_by_payment']['paypal'], false);
    $arr['amount_by_payment_formated']['alipay'] = price_format($arr['amount_by_payment']['alipay'], false);
    $arr['amount_by_payment_formated']['unionpay'] = price_format($arr['amount_by_payment']['unionpay'], false);
    $arr['amount_by_payment_formated']['franzpay'] = price_format($arr['amount_by_payment']['franzpay'], false);
    $arr['amount_by_payment_formated']['visamaster'] = price_format($arr['amount_by_payment']['visamaster'], false);
    $arr['amount_by_payment_formated']['americanexpress'] = price_format($arr['amount_by_payment']['americanexpress'], false);
    $arr['amount_by_payment_formated']['stripe'] = price_format($arr['amount_by_payment']['stripe'], false);
    $arr['amount_by_payment_formated']['braintree'] = price_format($arr['amount_by_payment']['braintree'], false);
    
    return $arr;
}

?>
