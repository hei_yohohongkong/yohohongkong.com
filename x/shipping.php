<?php

/**
 * ECSHOP 配送方式管理程序
 * ============================================================================
 * 版权所有 2005-2010 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liuhui $
 * $Id: shipping.php 17063 2010-03-25 06:35:46Z liuhui $
*/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
$exc = new exchange($ecs->table('shipping'), $db, 'shipping_code', 'shipping_name');
$shippingController = new \Yoho\cms\Controller\ShippingController();
/*------------------------------------------------------ */
//-- 配送方式列表
/*------------------------------------------------------ */

if ($_REQUEST['act'] == 'list')
{
    $modules = get_shipping_modules();

    $smarty->assign('ur_here', $_LANG['03_shipping_list']);
    $smarty->assign('modules', $modules);
    $smarty->assign('type_options', $shippingController->getShippingTypeSelect());
    $smarty->assign('action_link', array('href'=>'shipping_type.php?act=list',
        'text' => $_LANG['shipping_type_list']));
    assign_query_info();
    $smarty->display('shipping_list.htm');
}
elseif ($_REQUEST['act'] == 'query')
{
    $modules = $shippingController->getShippingModules();
    $shippingController->ajaxQueryAction($modules, count($modules), false);
}
elseif ($_REQUEST['act'] == 'ajaxEdit')
{
    $shippingController->ajaxEditAction();
}

/*------------------------------------------------------ */
//-- 安装配送方式
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'install')
{
    admin_priv('ship_manage');

    $set_modules = true;
    include_once(ROOT_PATH . 'includes/modules/shipping/' . $_GET['code'] . '.php');

    /* 检查该配送方式是否已经安装 */
    $sql = "SELECT shipping_id FROM " .$ecs->table('shipping'). " WHERE shipping_code = '$_GET[code]'";
    $id = $db->GetOne($sql);

    if ($id > 0)
    {
        /* 该配送方式已经安装过, 将该配送方式的状态设置为 enable */
        $db->query("UPDATE " .$ecs->table('shipping'). " SET enabled = 1 WHERE shipping_code = '$_GET[code]' LIMIT 1");
    }
    else
    {
        /* 该配送方式没有安装过, 将该配送方式的信息添加到数据库 */
        $insure = empty($modules[0]['insure']) ? 0 : $modules[0]['insure'];
        $sql = "INSERT INTO " . $ecs->table('shipping') . " (" .
                    "shipping_code, shipping_name, shipping_desc, insure, support_cod, enabled, print_bg, config_lable, print_model" .
                ") VALUES (" .
                    "'" . addslashes($modules[0]['code']). "', '" . addslashes($_LANG[$modules[0]['code']]) . "', '" .
                    addslashes($_LANG[$modules[0]['desc']]) . "', '$insure', '" . intval($modules[0]['cod']) . "', 1, '" . addslashes($modules[0]['print_bg']) . "', '" . addslashes($modules[0]['config_lable']) . "', '" . $modules[0]['print_model'] . "')";
        $db->query($sql);
        $id = $db->insert_Id();

        // handle simplified chinese
        $localizable_fields = [
            "shipping_name" => addslashes($_LANG[$modules[0]['code']]),
            "shipping_desc" => addslashes($_LANG[$modules[0]['desc']]),
        ];
        to_simplified_chinese($localizable_fields);
        // Multiple language support
        save_localized_versions('shipping', $id);
    }

    /* 记录管理员操作 */
    admin_log(addslashes($_LANG[$modules[0]['code']]), 'install', 'shipping');

    /* 提示信息 */
    $lnk[] = array('text' => $_LANG['add_shipping_area'], 'href' => 'shipping_area.php?act=add&shipping=' . $id);
    $lnk[] = array('text' => $_LANG['go_back'], 'href' => 'shipping.php?act=list');
    sys_msg(sprintf($_LANG['install_succeess'], $_LANG[$modules[0]['code']]), 0, $lnk);
}

/*------------------------------------------------------ */
//-- 卸载配送方式
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'uninstall')
{
    global $ecs, $_LANG;

    admin_priv('ship_manage');

    /* 获得该配送方式的ID */
    $row = $db->GetRow("SELECT shipping_id, shipping_name, print_bg FROM " .$ecs->table('shipping'). " WHERE shipping_code='$_GET[code]'");
    $shipping_id = $row['shipping_id'];
    $shipping_name = $row['shipping_name'];

    /* 删除 shipping_fee 以及 shipping 表中的数据 */
    if ($row)
    {
        $all = $db->getCol("SELECT shipping_area_id FROM " .$ecs->table('shipping_area'). " WHERE shipping_id='$shipping_id'");
        $in  = db_create_in(join(',', $all));

        $db->query("DELETE FROM " .$ecs->table('area_region'). " WHERE shipping_area_id $in");
        $db->query("DELETE FROM " .$ecs->table('shipping_area'). " WHERE shipping_id='$shipping_id'");
        $db->query("DELETE FROM " .$ecs->table('shipping'). " WHERE shipping_id='$shipping_id'");

        //删除上传的非默认快递单
        if (($row['print_bg'] != '') && (!is_print_bg_default($row['print_bg'])))
        {
            @unlink(ROOT_PATH . $row['print_bg']);
        }

        //记录管理员操作
        admin_log(addslashes($shipping_name), 'uninstall', 'shipping');

        $lnk[] = array('text' => $_LANG['go_back'], 'href'=>'shipping.php?act=list');
        sys_msg(sprintf($_LANG['uninstall_success'], $shipping_name), 0, $lnk);
    }
}

/*------------------------------------------------------ */
//-- 模板Flash编辑器
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'print_index')
{
    //检查登录权限
    admin_priv('ship_manage');

    $shipping_id = !empty($_GET['shipping']) ? intval($_GET['shipping']) : 0;

    /* 检查该插件是否已经安装 取值 */
    $sql = "SELECT * FROM " .$ecs->table('shipping'). " WHERE shipping_id = '$shipping_id' LIMIT 0,1";
    $row = $db->GetRow($sql);
    if ($row)
    {
        include_once(ROOT_PATH . 'includes/modules/shipping/' . $row['shipping_code'] . '.php');
        $row['shipping_print'] = !empty($row['shipping_print']) ? $row['shipping_print'] : '';
        $row['print_bg'] = empty($row['print_bg']) ? '' : get_site_root_url() . $row['print_bg'];
    }
    $smarty->assign('shipping', $row);
    $smarty->assign('shipping_id', $shipping_id);

    $smarty->display('print_index.htm');
}

/*------------------------------------------------------ */
//-- 模板Flash编辑器
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'recovery_default_template')
{
    /* 检查登录权限 */
    admin_priv('ship_manage');

    $shipping_id = !empty($_POST['shipping']) ? intval($_POST['shipping']) : 0;

    /* 取配送代码 */
    $sql = "SELECT shipping_code FROM " .$ecs->table('shipping'). " WHERE shipping_id = '$shipping_id'";
    $code = $db->GetOne($sql);

    $set_modules = true;
    include_once(ROOT_PATH . 'includes/modules/shipping/' . $code . '.php');

    /* 恢复默认 */
    $db->query("UPDATE " .$ecs->table('shipping'). " SET print_bg = '" . addslashes($modules[0]['print_bg']) . "',  config_lable = '" . addslashes($modules[0]['config_lable']) . "' WHERE shipping_code = '$code' LIMIT 1");

    $url = "shipping.php?act=edit_print_template&shipping=$shipping_id";
    ecs_header("Location: $url\n");
}

/*------------------------------------------------------ */
//-- 模板Flash编辑器 上传图片
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'print_upload')
{
    //检查登录权限
    admin_priv('ship_manage');

    //设置上传文件类型
    $allow_suffix = array('jpg', 'png', 'jpeg');

    $shipping_id = !empty($_POST['shipping']) ? intval($_POST['shipping']) : 0;

    //接收上传文件
    if (!empty($_FILES['bg']['name']))
    {
        if(!get_file_suffix($_FILES['bg']['name'], $allow_suffix))
        {
            echo '<script language="javascript">';
            echo 'parent.alert("' . sprintf($_LANG['js_languages']['upload_falid'], implode('，', $allow_suffix)) . '");';
            echo '</script>';
            exit;
        }

        $name = date('Ymd');
        for ($i = 0; $i < 6; $i++)
        {
            $name .= chr(mt_rand(97, 122));
        }
        $name .= '.' . end(explode('.', $_FILES['bg']['name']));
        $target = ROOT_PATH . '/images/receipt/' . $name;

        if (move_upload_file($_FILES['bg']['tmp_name'], $target))
        {
            $src = '/images/receipt/' . $name;
        }
    }

    //保存
    $sql = "UPDATE " .$ecs->table('shipping'). " SET print_bg = '$src' WHERE shipping_id = '$shipping_id'";
    $res = $db->query($sql);
    if ($res)
    {
        echo '<script language="javascript">';
        echo 'parent.call_flash("bg_add", "' . get_site_root_url() . $src . '");';
        echo '</script>';
    }
}

/*------------------------------------------------------ */
//-- 修改POS顯示
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'toggle_is_pos')
{
    check_authz_json($admin_priv);

    $pay_code = json_str_iconv(trim($_POST['id']));
    $is_pos   = intval($_POST['val']);

    if ($exc->edit("is_pos = '$is_pos' ", $pay_code))
    {
        clear_cache_files();
        make_json_result($is_pos);
    }
}

/*------------------------------------------------------ */
//-- 模板Flash编辑器 删除图片
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'print_del')
{
    /* 检查权限 */
    check_authz_json('ship_manage');

    $shipping_id = !empty($_GET['shipping']) ? intval($_GET['shipping']) : 0;
    $shipping_id = json_str_iconv($shipping_id);

    /* 检查该插件是否已经安装 取值 */
    $sql = "SELECT print_bg FROM " .$ecs->table('shipping'). " WHERE shipping_id = '$shipping_id' LIMIT 0,1";
    $row = $db->GetRow($sql);
    if ($row)
    {
        if (($row['print_bg'] != '') && (!is_print_bg_default($row['print_bg'])))
        {
            @unlink(ROOT_PATH . $row['print_bg']);
        }

        $sql = "UPDATE " .$ecs->table('shipping'). " SET print_bg = '' WHERE shipping_id = '$shipping_id'";
        $res = $db->query($sql);
    }
    else
    {
        make_json_error($_LANG['js_languages']['upload_del_falid']);
    }

    make_json_result($shipping_id);
}

/*------------------------------------------------------ */
//-- 编辑打印模板
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'edit_print_template')
{
    admin_priv('ship_manage');

    $shipping_id = !empty($_GET['shipping']) ? intval($_GET['shipping']) : 0;

    /* 检查该插件是否已经安装 */
    $sql = "SELECT * FROM " .$ecs->table('shipping'). " WHERE shipping_id=$shipping_id";
    $row = $db->GetRow($sql);
    if ($row)
    {
        include_once(ROOT_PATH . 'includes/modules/shipping/' . $row['shipping_code'] . '.php');
        $row['shipping_print'] = !empty($row['shipping_print']) ? $row['shipping_print'] : '';
        $row['print_model'] = empty($row['print_model']) ? 1 : $row['print_model']; //兼容以前版本

        $smarty->assign('shipping', $row);
    }
    else
    {
        $lnk[] = array('text' => $_LANG['go_back'], 'href'=>'shipping.php?act=list');
        sys_msg($_LANG['no_shipping_install'] , 0, $lnk);
    }

    $smarty->assign('ur_here', $_LANG['03_shipping_list'] .' - '. $row['shipping_name'] .' - '. $_LANG['shipping_print_template']);
    $smarty->assign('action_link', array('text' => $_LANG['03_shipping_list'], 'href' => 'shipping.php?act=list'));
    $smarty->assign('shipping_id', $shipping_id);

    assign_query_info();

    $smarty->display('shipping_template.htm');
}
elseif ($_REQUEST['act'] == 'logo_edit'){
    if($_REQUEST['req'] == 'retrieve'){
        $sql = "SELECT shipping_logo,shipping_name FROM " . $ecs->table('shipping') . " WHERE shipping_code = '". $_REQUEST['code'] ."'";
        $res = $db->getRow($sql);
        if($res['shipping_logo']){
            $full_path = "../" . DATA_DIR . "/afficheimg/shipping_logo/" . $res['shipping_logo'];
            if(file_exists($full_path)){
                $type = pathinfo($full_path, PATHINFO_EXTENSION);
                $data = file_get_contents($full_path);
                $dataUri = 'data:' . mime_content_type($full_path) . ';base64,' . base64_encode($data);
                $type = $type == 'pdf' ? 'pdf' : 'image';
                echo json_encode(array('src'=>$dataUri,'name'=>$res['shipping_name']));
            }else{
                $sql = "UPDATE ". $ecs->table('shipping') ." SET shipping_logo = '' WHERE shipping_name = '" . $res['shipping_name'] . "'";
                $db->query($sql);
                echo json_encode(array('error'=>0,'name'=>$res['shipping_name']));
            }
        }else{
            echo json_encode(array('error'=>1,'name'=>$res['shipping_name']));
        }
    }else{
        admin_priv('ship_manage');
        if($_REQUEST['req'] == 'update'){
            if (!file_exists("../" . DATA_DIR . '/afficheimg/shipping_logo/')) {
                mkdir("../" . DATA_DIR . '/afficheimg/shipping_logo/', 0777, true);
            }
            $filename = $_REQUEST['code'] . "_shipping_logo." . pathinfo($_FILES['logo_file']['name'])['extension'];
            if(isset($_FILES['logo_file']) && $_FILES['logo_file']['error'] == UPLOAD_ERR_OK){
                $full_path = "../" . DATA_DIR . "/afficheimg/shipping_logo/" . $filename;
                if(move_upload_file($_FILES['logo_file']['tmp_name'], $full_path)){
                    if(file_exists($full_path)){
                        $sql = "UPDATE " . $ecs->table('shipping') . " SET shipping_logo = '" . $filename . "' WHERE shipping_code = '" . $_REQUEST['code'] ."'";
                        $db->query($sql);
                        echo json_encode(array('success'=>1));
                    }
                }else{
                    echo json_encode(array('error'=>2,'src'=>$full_path));
                }
            }else{
                echo json_encode(array('error'=>3));
            }
        }else{
            echo json_encode(array('error'=>4));
        }
    }
}
/*------------------------------------------------------ */
//-- 编辑打印模板
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'do_edit_print_template')
{
    /* 检查权限 */
    admin_priv('ship_manage');

    /* 参数处理 */
    $print_model = !empty($_POST['print_model']) ? intval($_POST['print_model']) : 0;
    $shipping_id = !empty($_REQUEST['shipping']) ? intval($_REQUEST['shipping']) : 0;

    /* 处理不同模式编辑的表单 */
    if ($print_model == 2)
    {
        //所见即所得模式
        $db->query("UPDATE " . $ecs->table('shipping'). " SET config_lable = '" . $_POST['config_lable'] . "', print_model = '$print_model'  WHERE shipping_id = '$shipping_id'");
    }
    elseif ($print_model == 1)
    {
        //代码模式
        $template = !empty($_POST['shipping_print']) ? $_POST['shipping_print'] : '';

        $db->query("UPDATE " . $ecs->table('shipping'). " SET shipping_print = '" . $template . "', print_model = '$print_model' WHERE shipping_id = '$shipping_id'");
    }

    /* 记录管理员操作 */
    admin_log(addslashes($_POST['shipping_name']), 'edit', 'shipping');

    $lnk[] = array('text' => $_LANG['go_back'], 'href'=>'shipping.php?act=list');
    sys_msg($_LANG['edit_template_success'], 0, $lnk);

}

/*------------------------------------------------------ */
//-- 编辑配送方式名称
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'edit_name')
{
    /* 检查权限 */
    check_authz_json('ship_manage');

    /* 取得参数 */
    $id  = json_str_iconv(trim($_POST['id']));
    $val = json_str_iconv(trim($_POST['val']));

    /* 检查名称是否为空 */
    if (empty($val))
    {
        make_json_error($_LANG['no_shipping_name']);
    }

    /* 检查名称是否重复 */
    if (!$exc->is_only('shipping_name', $val, $id))
    {
        make_json_error($_LANG['repeat_shipping_name']);
    }

    /* 更新支付方式名称 */
    // $exc->edit("shipping_name = '$val'", $id);
    
    // Multiple language support
    $edit_lang = empty($_REQUEST['edit_lang']) ? '' : trim($_REQUEST['edit_lang']);
    if ($edit_lang && !in_array($edit_lang, available_languages()))
    {
        make_json_error('Invalid language');
    }
    $shipping_id = $db->getOne("SELECT shipping_id FROM " . $ecs->table('shipping') . " WHERE shipping_code = '" . $id . "'");
    localized_update('shipping', $shipping_id, array('shipping_name' => $val), $edit_lang);
    
    make_json_result(stripcslashes($val));
}

/*------------------------------------------------------ */
//-- 编辑配送方式描述
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'edit_desc')
{
    /* 检查权限 */
    check_authz_json('ship_manage');

    /* 取得参数 */
    $id = json_str_iconv(trim($_POST['id']));
    $val = json_str_iconv(trim($_POST['val']));

    /* 更新描述 */
    // $exc->edit("shipping_desc = '$val'", $id);
    
    // Multiple language support
    $edit_lang = empty($_REQUEST['edit_lang']) ? '' : trim($_REQUEST['edit_lang']);
    if ($edit_lang && !in_array($edit_lang, available_languages()))
    {
        make_json_error('Invalid language');
    }
    $shipping_id = $db->getOne("SELECT shipping_id FROM " . $ecs->table('shipping') . " WHERE shipping_code = '" . $id . "'");
    localized_update('shipping', $shipping_id, array('shipping_desc' => $val), $edit_lang);
    
    make_json_result(stripcslashes($val));
}

/*------------------------------------------------------ */
//-- 修改配送方式保价费
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'edit_insure')
{
    /* 检查权限 */
    check_authz_json('ship_manage');

    /* 取得参数 */
    $id = json_str_iconv(trim($_POST['id']));
    $val = json_str_iconv(trim($_POST['val']));
    if (empty($val))
    {
        $val = 0;
    }
    else
    {
        $val = make_semiangle($val); //全角转半角
        if (strpos($val, '%') === false)
        {
            $val = floatval($val);
        }
        else
        {
            $val = floatval($val) . '%';
        }
    }

    /* 检查该插件是否支持保价 */
    $set_modules = true;
    include_once(ROOT_PATH . 'includes/modules/shipping/' .$id. '.php');
    if (isset($modules[0]['insure']) && $modules[0]['insure'] === false)
    {
        make_json_error($_LANG['not_support_insure']);
    }

    /* 更新保价费用 */
    $exc->edit("insure = '$val'", $id);
    make_json_result(stripcslashes($val));
}


/*------------------------------------------------------ */
//-- 编辑配送方式描述
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'toggle_support_fod')
{
    /* 检查权限 */
    check_authz_json('ship_manage');

    /* 取得参数 */
    $id = json_str_iconv(trim($_POST['id']));
    $val = intval($_POST['val']);

    /* 更新資料 */
    $exc->edit("support_fod = '$val'", $id);
    make_json_result($val);
}

elseif($_REQUEST['act'] == 'shipping_priv')
{
    check_authz_json('ship_manage');

    make_json_result('');
}
elseif($_REQUEST['act'] == 'reset_shipping_address')
{
    ini_set('memory_limit', '512M');
    $modules = $shippingController->getShippingModules();
    foreach ($modules as $k => $shipping) {
        if (empty($shipping['shipping_id'])) {
            continue;
        }
        include_once('includes/modules/shipping/' . $shipping['code'] . '.php');
        $list  = $db->getAll("select shipping_area_id, configure FROM ". $ecs->table('shipping_area')." WHERE shipping_id = ".$shipping['shipping_id']);
        foreach ($list as $shipping_area) {
            $configure = unserialize($shipping_area['configure']);
            $shipping_obj    = new $shipping['code']($configure);
            if (method_exists($shipping_obj, 'getPickUpList')) {
                $address_list = $shipping_obj->getPickUpList(false, $shipping_area['shipping_area_id'], true);
                echo "Shipping: ".$shipping['code']." reset.". PHP_EOL;
            }
        }
    }
    exit();
}

/**
 * 获取站点根目录网址
 *
 * @access  private
 * @return  Bool
 */
function get_site_root_url()
{
    return 'http://' . $_SERVER['HTTP_HOST'] . str_replace('/' . ADMIN_PATH . '/shipping.php', '', PHP_SELF);

}

/**
 * 判断是否为默认安装快递单背景图片
 *
 * @param   string      $print_bg      快递单背景图片路径名
 * @access  private
 *
 * @return  Bool
 */
function is_print_bg_default($print_bg)
{
    $_bg = basename($print_bg);

    $_bg_array = explode('.', $_bg);

    if (count($_bg_array) != 2)
    {
        return false;
    }

    if (strpos('|' . $_bg_array[0], 'dly_') != 1)
    {
        return false;
    }

    $_bg_array[0] = ltrim($_bg_array[0], 'dly_');
    $list = explode('|', SHIP_LIST);

    if (in_array($_bg_array[0], $list))
    {
        return true;
    }

    return false;
}

function get_shipping_modules()
{
    global $db, $ecs, $_LANG;
    
    $modules = read_modules('../includes/modules/shipping');

    for ($i = 0; $i < count($modules); $i++)
    {
        $lang_file = ROOT_PATH.'languages/' .$_CFG['lang']. '/shipping/' .$modules[$i]['code']. '.php';

        if (file_exists($lang_file))
        {
            include_once($lang_file);
        }

        /* 检查该插件是否已经安装 */
        $sql = "SELECT shipping_id, shipping_name, shipping_desc, insure, support_cod, support_fod, is_pos, sort_order FROM " .$ecs->table('shipping'). " WHERE shipping_code='" .$modules[$i]['code']. "'";
        $row = $db->GetRow($sql);
        
        // Multiple language support
        if (!empty($_REQUEST['edit_lang']))
        {
            $row = array_pop(localize_db_result_lang($_REQUEST['edit_lang'], 'shipping', array($row)));
        }

        if ($row)
        {
            /* 插件已经安装了，获得名称以及描述 */
            $modules[$i]['id']      = $row['shipping_id'];
            $modules[$i]['name']    = $row['shipping_name'];
            $modules[$i]['desc']    = $row['shipping_desc'];
            $modules[$i]['insure_fee']  = $row['insure'];
            $modules[$i]['cod']     = $row['support_cod'];
            $modules[$i]['fod']     = $row['support_fod'];
            $modules[$i]['is_pos']     = $row['is_pos'];
            $modules[$i]['install'] = 1;
            $modules[$i]['logo']    = $row['shipping_logo'];

            if (isset($modules[$i]['insure']) && ($modules[$i]['insure'] === false))
            {
                $modules[$i]['is_insure']  = 0;
            }
            else
            {
                $modules[$i]['is_insure']  = 1;
            }
        }
        else
        {
            $modules[$i]['name']    = $_LANG[$modules[$i]['code']];
            $modules[$i]['desc']    = $_LANG[$modules[$i]['desc']];
            $modules[$i]['insure_fee']  = empty($modules[$i]['insure'])? 0 : $modules[$i]['insure'];
            $modules[$i]['cod']     = $modules[$i]['cod'];
            $modules[$i]['fod']     = 0;
            $modules[$i]['install'] = 0;
        }
    }
    
    return $modules;
}
?>