<?php

define('IN_ECS', true);
require dirname(__FILE__) . '/includes/init.php';
$commentController = new Yoho\cms\Controller\CommentController();

if ($_REQUEST['act'] == 'list') {
    $avgs = $commentController->getReviewAvgScore($commentController::REVIEW_CRM_TABLE_ID);
    $smarty->assign('items', $avgs['items']);
    $smarty->assign('avg', $avgs['avg']);
    $smarty->assign('admin_avg', $avgs['admin_avg']);
    $smarty->assign('min_year', $avgs['min_year']);
    $smarty->assign('ur_here', $_LANG['review_cs']);

    $smarty->display('review_cs.htm');
} elseif ($_REQUEST['act'] == 'query') {
    $controller = new Yoho\cms\Controller\YohoBaseController();
    $res = $commentController->getReviewList($commentController::REVIEW_CRM_TABLE_ID);

    $controller->ajaxQueryAction($res['data'], $res['record_count'], false);
}
?>