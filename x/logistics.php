<?php

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
use Yoho\cms\Model;

$logisticsController = new Yoho\cms\Controller\LogisticsController();
$admin_priv = $logisticsController::ACTION_CODE_LOGISTICS;
require_once(ROOT_PATH  . '/includes/lib_trackingmore.php');
$key = $_CFG['trackingmore_api'];
$track = new \Trackingmore($key);


switch ($_REQUEST['act']) {

    case 'list':
        admin_priv($admin_priv);

        $data = $logisticsController->get_logistics_list($_REQUEST['edit_lang'], false);
        $localizable_fields = localizable_fields_for_table('logistics');
        $smarty->assign('ur_here', $_LANG['09_logistics_list']);
        $smarty->assign('localizable_fields', $localizable_fields);
        $smarty->assign('full_page', 1);
        //function button
		$smarty->assign('action_link', array('text' => $_LANG['add_logistics'], 'href' => 'logistics.php?act=add'));
		// list
		$smarty->assign('logistics_list', $data);
		assign_query_info();

		$smarty->display('logistics_list.htm');

        break;
    case 'query':

        $data = $logisticsController->get_logistics_list($_REQUEST['edit_lang'], false);
        // list
        $smarty->assign('logistics_list', $data);
        make_json_result($smarty->fetch('logistics_list.htm'), '', array('filter' => array(), 'page_count' => 1));
        break;
    case 'add':
        admin_priv($admin_priv);

        $localizable_fields = localizable_fields_for_table('logistics');
        $carrierList = $track->getCarrierList();
        $smarty->assign('carrierList', $carrierList);
        $smarty->assign('localizable_fields', $localizable_fields);
        $smarty->assign('ur_here', $_LANG['add_logistics']);
        $smarty->assign('action_link', array('text' => $_LANG['09_logistics_list'], 'href' => 'logistics.php?act=list'));
        assign_query_info();
        $smarty->assign('action', 'insert');
        $smarty->display('logistics_info.htm');

        break;
    case 'edit':
        admin_priv($admin_priv);

        $carrierList = $track->getCarrierList();
        $logistics = new Model\Logistics($_REQUEST['id']);
        $logistics = $logistics->data;
        $logistics['logistics_id'] = $_REQUEST['id'];
        $localizable_fields = localizable_fields_for_table('logistics');
        $localized_versions = get_localized_versions('logistics', $_REQUEST['id'], $localizable_fields);
        $smarty->assign('carrierList', $carrierList);
        $smarty->assign('logistics', $logistics);
        $smarty->assign('localizable_fields', $localizable_fields);
        $smarty->assign('localized_versions', $localized_versions);
        $smarty->assign('ur_here', $_LANG['edit_logistics']);
        $smarty->assign('action_link', array('text' => $_LANG['09_logistics_list'], 'href' => 'logistics.php?act=list'));
        assign_query_info();
        $smarty->assign('action', 'edit');
        $smarty->display('logistics_info.htm');

        break;
    case 'submit_logistics':
        admin_priv($admin_priv);

        $post = $_POST;
        $res = $logisticsController->submit_form_action($post);

        if($res){
            $action = $_POST['action'];
            $logistics_name = $_POST['logistics_name'];
            $tablename ='logistics';

            // 记录日志
            admin_log($logistics_name, $action, $tablename);
            $link[] = array('text' => $_LANG['back'], 'href' => 'logistics.php?act=list');
            sys_msg($_LANG['attradd_succed'], 0, $link);
        } else {
            sys_msg($_LANG['logistics_not_available']);
        }

        break;
    //Ajax
    case 'toggle_enabled':
        check_authz_json($admin_priv);

        $logistics_id = json_str_iconv(trim($_POST['id']));
        $enabled = intval($_POST['val']);
        if($logisticsController->update_logistics_status($logistics_id, $enabled)){
            make_json_result($enabled);
        }

        break;
}