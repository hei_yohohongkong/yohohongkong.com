<?php

/***
* flashdeal_event.php
* 2019-07-30
***/

define('IN_ECS', true);
require(dirname(__FILE__) . '/includes/init.php');
$flashdealController = new Yoho\cms\Controller\FlashdealController();
$userRankController = new Yoho\cms\Controller\UserRankController();

if (!isset($_REQUEST['act']) || empty($_REQUEST['act'])) exit;

$format = 'Y-m-d H:i:s';

if ($_REQUEST['act'] == 'list')
{
    admin_priv('flashdeal_events');

    $smarty->assign('ur_here',      $_LANG['flashdeal_events']);
    $smarty->assign('start_date',$_REQUEST['start_date']);
    $smarty->assign('search_type',$_REQUEST['search_type']);
    $smarty->assign('search_str',$_REQUEST['search_str']);

    $event_list = $flashdealController->getFlashdealEventList();

    $str_now = date('Y-m-d H:i:s');
    $datetime_now = DateTime::createFromFormat($format, $str_now);

    if (sizeof($event_list)){
        foreach($event_list as $index => $val){
            $datetime_start = DateTime::createFromFormat($format, $val['start_date']);
            $datetime_end = DateTime::createFromFormat($format, $val['end_date']);

            if ($datetime_start < $datetime_now && $datetime_end > $datetime_now){
                $event_list[$index]['class_date'] = 'event-processing';
                $event_list[$index]['txt_process'] = '在活動時間中';
                if ($val['status'] == 1){
                    $event_list[$index]['on_off'] = 1;
                } else {
                    $event_list[$index]['on_off'] = 0;
                }
            } elseif ($datetime_start > $datetime_now) {
                $event_list[$index]['class_date'] = 'event-coming';
                $event_list[$index]['txt_process'] = '未到活動開始時間';
                if ($val['status'] == 1){
                    $event_list[$index]['on_off'] = 2;
                } else {
                    $event_list[$index]['on_off'] = 0;
                }
            } else {
                $event_list[$index]['class_date'] = 'event-end';
                $event_list[$index]['txt_process'] = '已過活動結束時間';
                $event_list[$index]['on_off'] = 0;
            }
        }
    }

    $smarty->assign('events', $event_list);

    $smarty->assign('action_link', array('text' => '新增閃購', 'href' => 'flashdeal_event.php?act=add'));
    $smarty->assign('action_link2', array('text' => '閃購設置', 'href' => 'flashdeal.php?act=edit_flashdeal_config'));

    assign_query_info();
    $smarty->display('flashdeal_event_list.htm');
}
elseif ($_REQUEST['act'] == 'query')
{
    //$list = $flashdealController->get_cms_flashdeals();
    //$flashdealController->ajaxQueryAction($list['data'], $list['record_count'], false);
}
elseif (in_array($_REQUEST['act'], array('add', 'info', 'edit')))
{
    require_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/ERP/lib_erp_supplier.php');
    admin_priv('flashdeal_events');
    $suppliers = suppliers_list_name_by_admin();
    $suppliers_id = array();
    foreach ($suppliers as $id){
        array_push($suppliers_id, $id['supplier_id']);
    }
    $smarty->assign('suppliers_id', $suppliers_id);
 
    $advanced_flashdeal_admin = FALSE;
    if (admin_priv('flashdeal_events_on_off', '', FALSE)){
        $advanced_flashdeal_admin = TRUE;
    }

    $smarty->assign('advanced_admin', $advanced_flashdeal_admin);

    //$suppliers = $flashdealController->getSuppliersList();
    $suppliers = suppliers_list_name_by_admin();
    $smarty->assign('suppliers', $suppliers);

    $cats = cat_list(0, 0, false, 0, true, true);
    $smarty->assign('cats', $cats);

    $brands = get_brand_list();
    $smarty->assign('brands', $brands);

    $arr_user_rank_list = $userRankController->getUserRankProgramName();
    $smarty->assign('user_rank_list', $arr_user_rank_list);

    if ($_REQUEST['act'] != 'add')
    {
        $search_key = empty($_REQUEST['search-key']) ? '' : trim($_REQUEST['search-key']);
        $search_cat = empty($_REQUEST['search-cat']) ? -1 : ($_REQUEST['search-cat']);
        $search_brand = empty($_REQUEST['search-brand']) ? -1 : ($_REQUEST['search-brand']);

        $arr_sql_where = array();

        if ($search_cat > 0){
            $arr_cats = cat_list($search_cat, $search_cat, FALSE);
            $arr_cats_id = array();
            if (sizeof($arr_cats) > 0){
                foreach($arr_cats as $cat){
                    array_push($arr_cats_id, $cat['cat_id']);
                }
            }
            $str_cats_id = implode(",", $arr_cats_id);
            array_push($arr_sql_where, 'g.cat_id IN ('.$str_cats_id.')');
            $smarty->assign('search_cat', $search_cat);
        }
        if ($search_brand > 0){
            array_push($arr_sql_where, 'g.brand_id = '.$search_brand);
            $smarty->assign('search_brand', $search_brand);
        }
        if ($search_key != ''){
            $str_search_key = preg_replace(array('/,/', '/\s+/'), array('', ''), $search_key);
            if (is_numeric($str_search_key) && strpos($search_key, ',') !== FALSE){
                    array_push($arr_sql_where, "g.goods_id IN (".$search_key.")");
            } else {
                array_push($arr_sql_where, "(g.goods_id LIKE '%".$search_key."%' OR g.goods_sn LIKE '%".$search_key."%' OR g.goods_name LIKE '%".$search_key."%') ");
            }
            $smarty->assign('search_key', $search_key);
        }

        $flashdeal_event_id = empty($_REQUEST['id']) ? 0 : intval($_REQUEST['id']);
        $event = $flashdealController->getFlashdealEventById($flashdeal_event_id);

        if (!$event)
        {
            sys_msg('找不到該閃購');
        }

        if (empty($event['user_rank'])){
            $arr_event_user_rank = array();
        } else {
            $arr_event_user_rank = explode('^', $event['user_rank']);
        }

        $event['user_rank'] = $arr_event_user_rank;

        if ($_REQUEST['act'] == 'edit'){
            $form_action = 'update';
            $smarty->assign('form_action', $form_action);
        }

        $smarty->assign('event', $event);

        if (isset($_REQUEST['order']) && $_REQUEST['order'] != ''){
            $good_list_order = $_REQUEST['order'];
        } else {
            $good_list_order = 'flashdeal_id';
        }

        if (isset($_REQUEST['sort']) && $_REQUEST['sort'] != ''){
            $good_list_sort = $_REQUEST['sort'];
        } else {
            $good_list_sort = 'ASC';
        }

        $smarty->assign('list_order', $good_list_order);
        $smarty->assign('list_sort', $good_list_sort);

        $event_goods = $flashdealController->getFlashdealGoodsById($flashdeal_event_id, $good_list_order, $good_list_sort, $arr_sql_where);

        if ($event_goods){
            $erpController = new Yoho\cms\Controller\ErpController();
            foreach ($event_goods as $index => $val){
                if (!empty($val['goods_id'])){
                    $good_storage_wh = $erpController->getSellableProductQty($val['goods_id']);
                    $event_goods[$index]['stock'] = $good_storage_wh;
                    if (in_array($_REQUEST['act'], ['info', 'edit']) && !empty($val['flashdeal_id'])){
                        $no_good_sellable = $flashdealController->getSellableFlashdealGoodQty($val['flashdeal_id']);
                        $no_good_sold = $flashdealController->getSoldFlashdealGoodQty($val['flashdeal_id']);
                        $event_goods[$index]['no_good_sellable'] = $no_good_sellable;
                        $event_goods[$index]['no_good_sold'] = $no_good_sold;
                    }
                }
                $event_goods[$index]['shop_price'] = round($val['shop_price'], 2);
                $event_goods[$index]['deal_price'] = round($val['deal_price'], 2);
                $show_cost = FALSE;
                if (!empty($val['supplier_ids'])){  
                    $arr_supplier_ids = explode(',', $val['supplier_ids']);
                    foreach ($arr_supplier_ids as $supplier_id){
                        if (in_array($supplier_id, $suppliers_id)){
                            $show_cost = TRUE;
                        }
                    }
                }
                if ($show_cost){
                    $event_goods[$index]['cost'] = round($val['cost'], 2);
                } else {
                    $event_goods[$index]['cost'] = "-";
                }
            }
            if (in_array($good_list_order, ['no_good_sellable', 'no_good_sold'])){
                $sort_key = (string)$good_list_order;
                switch ($good_list_order) {
                    case "no_good_sellable":
                        if($good_list_sort == 'ASC'){
                            usort($event_goods, function($a, $b) {
                                return $a["no_good_sellable"] - $b["no_good_sellable"];
                            });
                        } else {
                            usort($event_goods, function($a, $b) {
                                return $b["no_good_sellable"] - $a["no_good_sellable"];
                            });
                        }
                        break;
                    case "no_good_sold":
                        if($good_list_sort == 'ASC'){
                            usort($event_goods, function($a, $b) {
                                return $a["no_good_sold"] - $b["no_good_sold"];
                            });
                        } else {
                            usort($event_goods, function($a, $b) {
                                return $b["no_good_sold"] - $a["no_good_sold"];
                            });
                        }
                        break;
                    default:
                    if($good_list_sort == 'ASC'){
                        usort($event_goods, function($a, $b) {
                            return $a["flashdeal_id"] - $b["flashdeal_id"];
                        });
                    } else {
                        usort($event_goods, function($a, $b) {
                            return $b["flashdeal_id"] - $a["flashdeal_id"];
                        });
                    }
                }
            }
        }

        $count_goods = 0;

        if ($event_goods){
            $count_goods = sizeof($event_goods);
        }

        $smarty->assign('count_goods', $count_goods);
        $smarty->assign('event_goods', $event_goods);
    }
    else
    {
        $arr_default_user_rank = array();
        foreach ($arr_user_rank_list as $user_rank){
            if (!empty($user_rank['rank_id'])){
                array_push($arr_default_user_rank, $user_rank['rank_id']);
            }
        }
        $event = array(
            'name' => '',
            'status' => 0,
            'start_date' => local_date('Y-m-d 00:00', local_strtotime('+1 days')),
            'end_date' => local_date('Y-m-d 23:59', local_strtotime('+1 days')),
            'user_rank' => $arr_default_user_rank,
            'detail_visible' => 1,
            'is_online_pay' => 1,
        );
        $form_action = 'insert';
        $smarty->assign('form_action', $form_action);
        $flashdeal_event_id = -1;
        $smarty->assign('event', $event);
    }

    $smarty->assign('event_id', $flashdeal_event_id);

    $smarty->assign('action_link', array('text' => '閃購活動列表', 'href' => 'flashdeal_event.php?act=list'));

    switch ($_REQUEST['act']){
        case 'info':
            $ur_text = '查看閃購活動';
            $action_link2_text = '編輯此閃購活動';
            $action_link2_url = 'flashdeal_event.php?act=edit&id=' . $flashdeal_event_id;
            break;

        case 'edit':
            $ur_text = '管理閃購活動';
            $action_link2_text = '查看此閃購活動';
            $action_link2_url = 'flashdeal_event.php?act=info&id=' . $flashdeal_event_id;
            break;

        case 'add':
            $ur_text = '新增閃購活動';
            $action_link2_text = '';
            $action_link2_url = '';
            break;
        
        default:

    }

    $smarty->assign('ur_here', $ur_text);

    if (!empty($action_link2_text) && !empty($action_link2_url)){
        $smarty->assign('action_link2', array('text' => $action_link2_text, 'href' => $action_link2_url));
    }

    $smarty->assign('act', $_REQUEST['act']);

    //$smarty->assign('cfg_lang', $_CFG['lang']);

    //assign_query_info();
    if ($_REQUEST['act'] != 'info'){
        $smarty->display('flashdeal_event_edit.htm');
    } else {
        $smarty->display('flashdeal_event_info.htm');
    }
}
elseif ($_REQUEST['act'] == 'convert')
{
        admin_priv('flashdeal_events');
        $arr_user_rank_list = $userRankController->getUserRankProgramName();
        $smarty->assign('user_rank_list', $arr_user_rank_list);


        $form_action = 'group';
        $smarty->assign('form_action', $form_action);

        $arr_gd_date = $flashdealController->getFlashdealGroupDate();

        if (sizeof($arr_gd_date) == 0){
            sys_msg('未找到舊版本閃購商品', 0, $link, true);
        } else {
            foreach ($arr_gd_date as $index => $date){
                if (!empty($date['deal_date'])){

                $arr_default_user_rank = array();
                foreach ($arr_user_rank_list as $user_rank){
                    if (!empty($user_rank['rank_id'])){
                        array_push($arr_default_user_rank, $user_rank['rank_id']);
                    }
                }
                $event = array(
                    'name' => '閃購 - '.local_date('Y-m-d H:i:s', $date['deal_date']),
                    'status' => 0,
                    'start_date' => local_date('Y-m-d H:i:s', $date['deal_date']),
                    'end_date' => local_date('Y-m-d 23:59', $date['deal_date']),
                    'user_rank' => implode('^', $arr_default_user_rank),
                    'detail_visible' => 1,
                    'is_online_pay' => 0,
                    'last_update_name' => 'auto',
                );

                $db->autoExecute($ecs->table('flashdeal_event'), $event, 'INSERT');
                $fd_event_id = $db->insert_id();

                $event_fd = $flashdealController->getFlashdealIdByDate($date['deal_date']);

                $flashdealController->convertFlashdeal($fd_event_id, $event_fd);

                }
            }
        }

        sys_msg('已完成轉換舊閃購活動', 0, $link, true);
}
elseif ($_REQUEST['act'] == 'group')
{
    admin_priv('flashdeal_events');

    $arr_flashdeal_event = [];

    $event_name = mysql_real_escape_string(trim($_POST['name']));

    if (empty($event_name)){
        sys_msg('欠缺閃購活動名稱');
    } else {
        $arr_flashdeal_event['name'] = $event_name;
    }

    if (empty($_POST['start_date']) || empty($_POST['end_date'])){
        sys_msg('欠缺閃購活動時間');
    } else {
        $event_start_date = DateTime::createFromFormat($format, $_POST['start_date'].':00');
        $event_start_date =  $event_start_date->format('Y-m-d H:i:s');
        $event_end_date = DateTime::createFromFormat($format, $_POST['end_date'].':59');
        $event_end_date = $event_end_date->format('Y-m-d H:i:s');
        $arr_flashdeal_event['start_date'] = $event_start_date;
        $arr_flashdeal_event['end_date'] = $event_end_date;
    }

    if (!isset($_POST['user_rank']) || empty($_POST['user_rank'])){
        $event_user_rank =  '';
    } else {
        $arr_event_user_rank = array();
        foreach ($_POST['user_rank'] as $val){
            array_push($arr_event_user_rank, $val);
        }
        $event_user_rank = implode('^', $arr_event_user_rank);
    }

    $arr_flashdeal_event['user_rank'] =  $event_user_rank;
    $arr_flashdeal_event['status'] = empty($_POST['status'])?0:$_POST['status'];
    $arr_flashdeal_event['detail_visible'] = empty($_POST['detail_visible'])?0:$_POST['detail_visible'];
    $arr_flashdeal_event['is_online_pay'] = empty($_POST['is_online_pay'])?0:$_POST['is_online_pay'];

    $event_last_update_name = empty($_SESSION['admin_name'])?'':$_SESSION['admin_name'];
    $arr_flashdeal_event['last_update_name'] = $event_last_update_name;

    $arr_checked_items = $_REQUEST['check-items'];
    $arr_fd_id = $_REQUEST['item-id'];

    if (sizeof($arr_checked_items) < 1){
        sys_msg('未選擇閃購商品');
    }
    
    $db->autoExecute($ecs->table('flashdeal_event'), $arr_flashdeal_event, 'INSERT');
    $fd_event_id = $db->insert_id();

    $flashdealController->convertFlashdeal($fd_event_id, $arr_checked_items);


    clear_cache_files();

    $link[0]['text'] = '返回閃講活動列表';
    $link[0]['href'] = 'flashdeal_event.php?act=list';

    sys_msg('返回閃講活動列表', 0, $link, true);
}
elseif (in_array($_REQUEST['act'], array('insert','update')))
{
    admin_priv('flashdeal_events');

    if ($_REQUEST['act'] == 'update')
    {
        $event_id = empty($_REQUEST['event_id']) ? 0 : intval($_REQUEST['event_id']);

        if (!$event_id)
        {
            sys_msg('閃講活動ID錯誤');
        }
    }

    $arr_flashdeal_event = [];

    $event_name = mysql_real_escape_string(trim($_POST['name']));

    if (empty($event_name)){
        sys_msg('欠缺閃購活動名稱');
    } else {
        $arr_flashdeal_event['name'] = $event_name;
    }

    if (empty($_POST['start_date']) || empty($_POST['end_date'])){
        sys_msg('欠缺閃購活動時間');
    } else {
        $event_start_date = DateTime::createFromFormat($format, $_POST['start_date'].':00');
        $event_start_date =  $event_start_date->format('Y-m-d H:i:s');
        $event_end_date = DateTime::createFromFormat($format, $_POST['end_date'].':59');
        $event_end_date = $event_end_date->format('Y-m-d H:i:s');
        $arr_flashdeal_event['start_date'] = $event_start_date;
        $arr_flashdeal_event['end_date'] = $event_end_date;
    }

    if (!isset($_POST['user_rank']) || empty($_POST['user_rank'])){
        $event_user_rank =  '';
    } else {
        $arr_event_user_rank = array();
        foreach ($_POST['user_rank'] as $val){
            array_push($arr_event_user_rank, $val);
        }
        $event_user_rank = implode('^', $arr_event_user_rank);
    }

    $arr_flashdeal_event['user_rank'] =  $event_user_rank;
    if (isset($_POST['status'])){
        $arr_flashdeal_event['status'] = empty($_POST['status'])?0:$_POST['status'];
    } else if ($_REQUEST['act'] == 'insert') {
        $arr_flashdeal_event['status'] = 0;
    }
    $arr_flashdeal_event['detail_visible'] = empty($_POST['detail_visible'])?0:$_POST['detail_visible'];
    $arr_flashdeal_event['is_online_pay'] = empty($_POST['is_online_pay'])?0:$_POST['is_online_pay'];

    $event_last_update_name = empty($_SESSION['admin_name'])?'':$_SESSION['admin_name'];
    $arr_flashdeal_event['last_update_name'] = $event_last_update_name;
    $arr_good_id = array();

    $arr_goods = array();
    $arr_duplicate_fd_id = array();
    foreach ($_REQUEST['item-id'] as $key => $val) {
        if ($val != "" && $_REQUEST['deal-quantity'][$key] > 0){
            if (!in_array($val, $arr_goods)){
                $arr_goods[$val] = array(
                        'goods_id' => $val,
                        'deal_price' => $_REQUEST['deal-price'][$key],
                        'deal_quantity' => $_REQUEST['deal-quantity'][$key],
                        'sort_order'  => $_REQUEST['sort-order'][$key],
                        'deal_date' => local_strtotime($event_start_date),
                        'detail_visible' => $_REQUEST['good-visible'][$key],
                        'is_online_pay' => $_REQUEST['good-online-pay'][$key],
                        'payme_only' => $_REQUEST['good-payme'][$key],
                        'alipay_only' => $_REQUEST['good-alipay'][$key],
                        //'is_mobile_pay' => $_REQUEST['good-mobile-pay'][$key],
                        'creator' => $event_last_update_name,
                        'fd_event_id'  => $_REQUEST['event_id']
                );
                array_push($arr_goods, $val);
            } else {
                if ($_REQUEST['item-fd-id'][$key] != '' && $_REQUEST['item-fd-id'][$key] != '-1'){
                    array_push($arr_duplicate_fd_id, $_REQUEST['item-fd-id'][$key]);
                }
            }
        }
    }

    if (sizeof($arr_duplicate_fd_id) > 0){ //delete duplicate flashdeal good in a  event
        foreach ($arr_duplicate_fd_id as $fd_id){
            $flashdealController->deleteEventGoods(-1, -1, $fd_id);
        }
    }

    
    $event_goods = $arr_goods;

    if ($_REQUEST['act'] == 'insert'){
        $db->autoExecute($ecs->table('flashdeal_event'), $arr_flashdeal_event, 'INSERT');
        $fd_event_id = $db->insert_id();

        $flashdealController->insertEventGoods($fd_event_id, $event_goods);

    } else {
        foreach ($arr_flashdeal_event as $index => $val){
            if ($index == 'status' && !admin_priv('flashdeal_events_on_off', '', FALSE)){
                continue;
            }
            $arr_flashdeal_event['sql_update'][$index] = $index." = '".$val."'";
        }
        if (empty($_REQUEST['item-id'])){
            $_REQUEST['item-id'] = array();
        }
        $arr_flashdeal_event['goods_id'] = $_REQUEST['item-id'];

        $flashdealController->updateFlashdealEvent($event_id, $arr_flashdeal_event, $event_goods);
    }

    clear_cache_files();

    $link[0]['text'] = '繼續編輯此閃購活動';
    if ($_REQUEST['act'] == 'insert'){
        $link[0]['href'] = 'flashdeal_event.php?act=edit&id='.$fd_event_id;
    } else {
        $link[0]['href'] = 'flashdeal_event.php?act=edit&id='.$event_id;
    }
    $link[1]['text'] = '返回閃購活動列表';
    $link[1]['href'] = 'flashdeal_event.php?act=list';

    sys_msg(($_REQUEST['act'] == 'update' ? '編輯' : '新增') . '閃購成功' . ((sizeof($arr_duplicate_fd_id) > 0) ? " (".sizeof($arr_duplicate_fd_id).")" : ""), 0, $link, true);
}
elseif ($_REQUEST['act'] == 'ajax_update_status')
{
    admin_priv('flashdeal_events_on_off');
    //admin_priv('flashdeal_events');
    if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0){
        $event_id = $_REQUEST['id'];
    }
    if (isset($_REQUEST['status']) && $_REQUEST['status'] != ''){
        if ($_REQUEST['status'] == 'true'){
            $new_status = 1;
        } elseif ($_REQUEST['status'] == 'false'){
            $new_status = 0;
        }
    }

    $flashdealController->updateFlashdealEventStatusById($event_id, $new_status);

    $current_event = $flashdealController->getFlashdealEventById($event_id);
    if ($current_event){
        $current_event_status = (string)$current_event['status'];

        $str_now = date('Y-m-d H:i:s');
        $datetime_now = DateTime::createFromFormat($format, $str_now);
        $datetime_start = DateTime::createFromFormat($format, $current_event['start_date']);
        $datetime_end = DateTime::createFromFormat($format, $current_event['end_date']);

        if ($datetime_start < $datetime_now && $datetime_end > $datetime_now){
            if ($current_event['status'] == 1){
                $current_event_status_on_off = '1';
            } else {
                $current_event_status_on_off = '0';
            }
        } elseif ($datetime_start > $datetime_now) {
            if ($current_event['status'] == 1){
                $current_event_status_on_off = '2';
            } else {
                $current_event_status_on_off = '0';
            }
        } else {
            $current_event_status_on_off = '0';
        }

        return make_json_result(array('status' => $current_event_status, 'on_off' => $current_event_status_on_off));
    }
    
}
elseif ($_REQUEST['act'] == 'ajax_get_top_stock')
{
    if (!empty($_REQUEST['supplier_id']) && $_REQUEST['supplier_id'] > 0){
        $supplier_id = $_REQUEST['supplier_id'];
    } else {
        return 'false';
    }

    if (isset($_REQUEST['no']) && $_REQUEST['no'] > 0){
        $no = $_REQUEST['no'];
    } else {
        return 'false';
    }

    if (isset($_REQUEST['exclude']) && sizeof($_REQUEST['exclude']) > 0){
        $exclude_good_id = implode(',', $_REQUEST['exclude']);
    } else {
        $exclude_good_id = FALSE;
    }

    $top_stock = $flashdealController->getTopStockGoods($supplier_id, $no, $exclude_good_id);

    if ($top_stock){
        foreach ($top_stock as $index => $good){
            $top_stock[$index]['stock'] = $erpController->getSellableProductQty($good['goods_id']);
        }
        usort($top_stock, function($a, $b) {
            return $b['stock'] - $a['stock'];
        });
        return make_json_result($top_stock);
    } else {
        return 'false';
    }
    
}
elseif ($_REQUEST['act'] == 'ajax_get_goods_by_keyword' || $_REQUEST['act'] == 'ajax_get_goods_by_cat')
{
    require_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/ERP/lib_erp_supplier.php');
    admin_priv('flashdeal_events');
    $suppliers = suppliers_list_name_by_admin();
    $suppliers_id = array();
    foreach ($suppliers as $id){
        array_push($suppliers_id, $id['supplier_id']);
    }

    $arr_sql_where = array();

    if (isset($_REQUEST['cat_id']) && $_REQUEST['cat_id'] > -1){
        $arr_cats = cat_list($_REQUEST['cat_id'], $_REQUEST['cat_id'], FALSE);
        $arr_cats_id = array();
        if (sizeof($arr_cats) > 0){
            foreach($arr_cats as $cat){
                array_push($arr_cats_id, $cat['cat_id']);
            }
        }
        $str_cats_id = implode(",", $arr_cats_id);
        array_push($arr_sql_where, 'g.cat_id IN ('.$str_cats_id.')');
    }
    if (isset($_REQUEST['brand_id']) && $_REQUEST['brand_id'] > -1){
        array_push($arr_sql_where, 'g.brand_id = '.$_REQUEST['brand_id']);
    }

    if (sizeof($arr_sql_where) == 0){
        $arr_sql_where = FALSE;
    }

    if (!$arr_sql_where && $_REQUEST['act'] == 'ajax_get_goods_by_cat'){
        return 'false';
    }

    $search_key = mysql_real_escape_string(trim($_REQUEST['search_key']));

    if ($search_key == '' && $_REQUEST['act'] != 'ajax_get_goods_by_cat'){
        return 'false';
    }

    //$goods = $flashdealController->getGoodsBykeyword($search_key, $arr_sql_where);
    $goods = $flashdealController->getGoodsBykeywordIncludeGoods($search_key, $arr_sql_where);

    foreach ($goods as $index => $val){
        $goods[$index]['shop_price'] = round($val['shop_price'], 2);
        $goods[$index]['deal_price'] = round($val['deal_price'], 2);
        $show_cost = FALSE;
        if (!empty($val['supplier_ids'])){  
            $arr_supplier_ids = explode(',', $val['supplier_ids']);
            foreach ($arr_supplier_ids as $supplier_id){
                if (in_array($supplier_id, $suppliers_id)){
                    $show_cost = TRUE;
                }
            }
        }
        if ($show_cost){
            $goods[$index]['cost'] = round($val['cost'], 2);
        } else {
            $goods[$index]['cost'] = "-";
        }
    }

    return make_json_result($goods);
    
}
elseif ($_REQUEST['act'] == 'remove')
{
    admin_priv('flashdeal_events');

    $flashdeal_event_id = intval($_GET['id']);

    if ($flashdeal_event_id){
        $flashdealController->deleteEvent($flashdeal_event_id);
    }

    $link[0]['text'] = '返回閃購活動列表';
    $link[0]['href'] = 'flashdeal_event.php?act=list';

    sys_msg('正在返回閃購活動列表', 0, $link, true);
    exit;
}
elseif ($_REQUEST['act'] == 'ajaxEdit')
{
    check_authz_json('flashdeal_events');
    $flashdeal_id = intval($_POST['id']);
    $value = $_POST['value'];
    $col = $_POST['col'];
    if ($col == 'deal_quantity') {
        if(intval($value) > 0) {
            $can = $flashdealController->update_flashdeal_goods($flashdeal_id, $value);
            if($can){
                $flashdealController->ajaxEditAction('flashdeals');
                $flashdealController->update_date_json();
            }
            else make_json_error('你輸入的閃購數量少於已購買人數');
        } else make_json_error('你輸入的閃購數量少於1');
    } else {
        $flashdealController->ajaxEditAction('flashdeals');
        $flashdealController->update_date_json();
    }


}
elseif ($_REQUEST['act'] == 'list_notify')
{
    $list = get_flashdeal_notifys();
    $smarty->assign('ur_here',      $_LANG['17_flashdeals'] . ' - 通知列表');
    $smarty->assign('full_page',    1);

    $smarty->assign('list',         $list['data']);
    $smarty->assign('filter',       $list['filter']);
    $smarty->assign('record_count', $list['record_count']);
    $smarty->assign('page_count',   $list['page_count']);

    $smarty->assign('action_link', array('text' => '返回閃購列表', 'href' => 'flashdeal.php?act=list'));

    $sort_flag  = sort_flag($list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);

    assign_query_info();
    $smarty->display('flashdeal_notify_list.htm');
}
elseif ($_REQUEST['act'] == 'query_notify')
{
    $list = get_flashdeal_notifys();
    $smarty->assign('list',         $list['data']);
    $smarty->assign('filter',       $list['filter']);
    $smarty->assign('record_count', $list['record_count']);
    $smarty->assign('page_count',   $list['page_count']);

    $sort_flag  = sort_flag($list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);

    make_json_result($smarty->fetch('flashdeal_notify_list.htm'), '', array('filter' => $list['filter'], 'page_count' => $list['page_count']));
}
elseif ($_REQUEST['act'] == 'sort_order')
{
    /* 检查权限 */
    check_authz_json('flashdeal_events');

    $flashdeal_id  = intval($_POST['id']);
    $sort_order = intval($_POST['val']);

    if ($sort_order < 0)
    {
        make_json_error('您輸入了一個非法的數量');
    }
    else
    {
        $sql = "UPDATE " . $ecs->table('flashdeals') .
                "SET `sort_order` = '" . $sort_order . "' " .
                "WHERE `flashdeal_id` = '" . $flashdeal_id . "' " .
                "LIMIT 1";
        if ($db->query($sql))
        {
            clear_cache_files();
            make_json_result($sort_order);
        }
        else
        {
            make_json_error('更新數量失敗');
        }
    }
}
elseif ($_REQUEST['act'] == 'edit_flashdeal_config')
{
    $smarty->assign('config', $flashdealController->get_flashdeal_config(true));
    assign_query_info();
    $smarty->display('flashdeal_config.htm');
}
elseif ($_REQUEST['act'] == 'update_json')
{
    $flashdealController->update_date_json();
    $flashdealController->read_date_json();
}
elseif ($_REQUEST['act'] == 'update_config')
{
    $flashdealController->update_flashdeal_config($_REQUEST);
    $link[0]['text'] = '返回列表';
    $link[0]['href'] = 'flashdeal.php?act=list';

    sys_msg( '編輯閃購設置成功', 0, $link, true);
}
elseif ($_REQUEST['act'] == 'mass_update_flashdeal_goods')
{
    $flashdealController->mass_update_flashdeal_goods($_REQUEST['fs_id'], $_REQUEST['fe_id']);
    $flashdealController->update_date_json();
}

function get_flashdeal_info($flashdeal_id)
{
    $flashdeal_id = intval($flashdeal_id);

    if ($flashdeal_id <= 0)
    {
        return false;
    }

    $sql = "SELECT fd.*, g.`goods_name`, g.`goods_sn`, g.`goods_thumb`, g.`market_price`, g.`shop_price` " .
            "FROM " . $GLOBALS['ecs']->table('flashdeals') . " as fd " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON fd.goods_id = g.goods_id " .
            "WHERE `flashdeal_id` = '" . $flashdeal_id . "'";

    $flashdeal = $GLOBALS['db']->getRow($sql);

    if (!$flashdeal)
    {
        return false;
    }

    $flashdeal['market_price_formatted'] = price_format($flashdeal['market_price'], false);
    $flashdeal['shop_price_formatted'] = price_format($flashdeal['shop_price'], false);
    $flashdeal['deal_price_formatted'] = price_format($flashdeal['deal_price'], false);
    $flashdeal['deal_date_formatted'] = local_date('Y-m-d H:i', $flashdeal['deal_date']);

    return $flashdeal;
}

function get_flashdeal_notifys()
{
    $filter['flashdeal_id'] = empty($_REQUEST['flashdeal_id']) ? 0 : intval($_REQUEST['flashdeal_id']);

    $filter['sort_by']      = empty($_REQUEST['sort_by']) ? 'notify_id' : trim($_REQUEST['sort_by']);
    $filter['sort_order']   = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);

    $sql = "SELECT count(*) " .
            "FROM " . $GLOBALS['ecs']->table('flashdeal_notify') .
            (empty($filter['flashdeal_id']) ? '' : "WHERE flashdeal_id = '" . $filter['flashdeal_id'] . "' ");
    $filter['record_count'] = $GLOBALS['db']->getOne($sql);

    /* 分页大小 */
    $filter = page_and_size($filter);

    /* 查询 */
    $sql = "SELECT fn.*, u.`user_name`, rei.`content` as display_name " .
            "FROM " . $GLOBALS['ecs']->table('flashdeal_notify') . " as fn " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('users') . " as u ON u.user_id = fn.user_id " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('reg_extend_info') . " as rei ON rei.user_id = fn.user_id AND rei.reg_field_id = 10 " .
            "WHERE 1 " . (empty($filter['flashdeal_id']) ? '' : "AND fn.flashdeal_id = '" . $filter['flashdeal_id'] . "' ") .
            "ORDER BY " . $filter['sort_by'] . ' ' . $filter['sort_order'] . ' ' .
            "LIMIT " . $filter['start'] . "," . $filter['page_size'];
    $data = $GLOBALS['db']->getAll($sql);

    foreach ($data as $key => $row)
    {
        $data[$key]['display_name'] = empty($row['display_name']) ? $row['user_name'] : $row['display_name'];
        $data[$key]['enter_date'] = local_date($GLOBALS['_CFG']['time_format'], $row['enter_time']);
    }

    $arr = array('data' => $data, 'filter' => $filter, 'page_count' => $filter['page_count'], 'record_count' => $filter['record_count']);

    return $arr;
}

?>