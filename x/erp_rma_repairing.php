<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_base.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_warehouse.php');
require(dirname(__FILE__) .'/includes/ERP/cls/cls_date.php');
$rmaController = new Yoho\cms\Controller\RmaController();
$adminuserController = new Yoho\cms\Controller\AdminuserController();

if ($_REQUEST['act'] == 'list' || $_REQUEST['act'] == '') {
    $rma_id = $_REQUEST['rma_id'];
    $step = 4;
    
    $rmaController->checkStatusRedirect($rma_id,'repairing');
    $personInChargeoption = $rmaController->personInChargeOption(true,'WarehouseTeam');
    $action_log = $rmaController->getActionLog($rma_id);

    // check can back to step 1
    $canBackToStep1 = $rmaController->canBackToStep1($rma_id);

    //$result = $stocktakeController->detail();
    //$goods_status_options = $stocktakeController->getGoodsStatusOptions('process');
    $rmaBasicInfo =  $rmaController->getRmaInfo($rma_id);
    $smarty->assign('person_in_charge_option',$personInChargeoption);
    $smarty->assign('action_log',$action_log);
    $smarty->assign('rma_basic_info',$rmaBasicInfo);
    $smarty->assign('channel',$rmaBasicInfo['channel']);
    $smarty->assign('rma_id',$rma_id);
    $smarty->assign('can_back_to_step1',$canBackToStep1);
    $smarty->assign('step',$step);
    $smarty->assign('full_page', 1);
    $smarty->assign('cfg_lang', $_CFG['lang']);
    $smarty->assign('lang', $_LANG);
    $smarty->assign('action_link', array('text' => 'RMA列表', 'href' => 'erp_rma.php'));
    assign_query_info();    
    $smarty->display('erp_rma_repairing.htm');
} else if ($_REQUEST['act'] == 'post_rma_supplier_returned_submit') {
    $result = $rmaController->rmaSupplierReturnedSubmit();
    if (!$result) {
        make_json_error('更新時發生錯誤');
    }else{
        make_json_result($result);
    }
} else if ($_REQUEST['act'] == 'post_rma_save_note') {
    $result = $rmaController->rmaSaveNote();
    if($result !== false){
        make_json_result($result);
    }else{
        make_json_error('更新時發生錯誤');
    }
} elseif ($_REQUEST['act'] == 'query') {
    $result = $rmaController->getRmaStockInOutLog();
    $rmaController->ajaxQueryAction($result['data'],$result['record_count'],false);
}
?>