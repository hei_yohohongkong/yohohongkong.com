<?php

/***
* goods_price_log.php
* by howang 2014-10-10
*
* Display the log of goods price changes
***/

define('IN_ECS', true);
require(dirname(__FILE__) . '/includes/init.php');
$goodsController  = new Yoho\cms\Controller\GoodsController();

if ($_REQUEST['act'] == 'list')
{
    /* 检查权限 */
    admin_priv('goods_manage');

    $smarty->assign('ur_here',      $_LANG['goods_price_log']);
    assign_query_info();
    $smarty->display('goods_price_log.htm');
}
elseif ($_REQUEST['act'] == 'query')
{
    $goodsController->goodsPriceLogAjaxQueryAction();
}
?>