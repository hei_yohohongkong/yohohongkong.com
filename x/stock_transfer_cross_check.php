<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_base.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_warehouse.php');
require(dirname(__FILE__) .'/includes/ERP/cls/cls_date.php');
$orderController = new Yoho\cms\Controller\OrderController();
$adminuserController = new Yoho\cms\Controller\AdminuserController();
$deliveryOrderController = new Yoho\cms\Controller\DeliveryOrderController();
$warehouseTransferController = new Yoho\cms\Controller\WarehouseTransferController();


if($_REQUEST['act'] == 'list' || $_REQUEST['act'] == ''){
	if( admin_priv('erp_warehouse_view','',false) || admin_priv('erp_warehouse_manage','',false) || admin_priv('erp_warehouse_approve','',false)) {
	    include('./includes/ERP/page.class.php');
		
		//get role id
		$role_id = $adminuserController->get_user_role(erp_get_admin_id());
		$smarty->assign('role_id', $role_id);
		$smarty->assign('operator', erp_get_admin_id());
		//$smarty->assign('type_options', $stocktakeController->getTypeOptions());
        //$smarty->assign('status_options', $stocktakeController->getStocktakeStatusOptions());
        //$smarty->assign('ur_here', '存貨盤點');
        $smarty->assign('full_page', 1);
        //$smarty->assign('start_date', $result['start_date']);
        //$smarty->assign('end_date', $result['end_date']);
        $smarty->assign('cfg_lang', $_CFG['lang']);
        $smarty->assign('lang', $_LANG);
        $smarty->display('stock_transfer_cross_check.htm');
    } else {
        $href="index.php";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
    }

} elseif ($_REQUEST['act'] == 'query') {
	$result = $warehouseTransferController->getTransferProductsForCrossCheck();
    //$result = $deliveryOrderController->getTheLastDeliveryOrdersGoods($_REQUEST['order_sn'],$_REQUEST['delivery_id']);
	$extraData = ['transfer_id' => $result['transfer_id'],'is_passed' => $result['is_passed']];
    $orderController->ajaxQueryAction($result['data'],$result['record_count'],false,$extraData);
} elseif ($_REQUEST['act'] == 'is_transfer_sn') {
    $result = $warehouseTransferController->isTransferSn($_REQUEST['transfer_sn']);
	if(isset($result['error'])){
		make_json_error($result['error']);
	}else{
		make_json_result($result);
	}
} else if ($_REQUEST['act'] == 'save_cross_check_error_history') {
	$result = $warehouseTransferController->save_cross_check_error_history();
	if(isset($result['error'])){
		make_json_error($result['error']);
	}else{
		make_json_result($result);
	}
}  else if ($_REQUEST['act'] == 'update_order_transfer_stock_out_qty') {
	$result = $warehouseTransferController->update_order_transfer_stock_out_qty();
	if(isset($result['error'])){
		make_json_error($result['error']);
	}else{
		make_json_result($result);
	}
} else if ($_REQUEST['act'] == 'save_cross_check_error') {
	$result = $warehouseTransferController->saveCrossCheckError();
	if(isset($result['error'])){
		make_json_error($result['error']);
	}else{
		make_json_result($result);
	}
}


?>