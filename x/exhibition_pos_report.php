<?php
/* This page is using to generate echibition pos report */
/* 2017-12-29 By Anthony */

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require_once(ROOT_PATH . 'includes/lib_order.php');
require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/admin/statistic.php');
$smarty->assign('lang', $_LANG);
$admin_priv = "report_ex_pos";

if (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'query' ||  $_REQUEST['act'] == 'download'))
{
	/* 检查权限 */
	check_authz_json($admin_priv);
	
	if (strstr($_REQUEST['start_date'], '-') === false)
	{
		$_REQUEST['start_date'] = local_date('Y-m-d', $_REQUEST['start_date']);
		$_REQUEST['end_date'] = local_date('Y-m-d', $_REQUEST['end_date']);
    }
    $start_date = local_strtotime($_REQUEST['start_date']);
    $end_date   = local_strtotime($_REQUEST['end_date']);
    if (!isset($_REQUEST['start_date']))
    {
        $start_date = month_start();
    }
    if (!isset($_REQUEST['end_date']))
    {
        $end_date = month_end();
    }
	/*------------------------------------------------------ */
	//--Excel文件下载
	/*------------------------------------------------------ */
	if ($_REQUEST['act'] == 'download')
	{
		header("Content-type: application/vnd.ms-excel; charset=utf-8");
		
		require_once ROOT_PATH . 'includes/excel/PHPExcel.php';
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		
		$objPHPExcel->getActiveSheet()->setTitle('展場報告');

		header('Content-type: application/vnd.ms-excel; charset=utf-8');
		header('Content-Disposition: attachment; filename=' . $file_name . '.xls');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		
		exit;
	}
	
    $data = get_sale_report($start_date, $end_date);
    $goods_data = get_goods_report($start_date, $end_date);

	/* 赋值到模板 */
	$smarty->assign('filter',           $data['filter']);
	$smarty->assign('record_count',     $data['record_count']);
	$smarty->assign('page_count',       $data['page_count']);
    $smarty->assign('data',             $data['data']);
    $smarty->assign('goods_data',       $goods_data['data']);
    $smarty->assign('salesperson_name', $data['salesperson_name']);
	$smarty->assign('ur_here',        $_LANG['stock_cost_supplier']);
	$smarty->assign('start_date',     local_date('Y-m-d', $start_date));
	$smarty->assign('end_date',       local_date('Y-m-d', $end_date));
	$smarty->assign('cfg_lang',       $_CFG['lang']);
	$smarty->assign('deprecated', $_REQUEST['deprecatedflow'] == 'Y');

	make_json_result($smarty->fetch('exhibition_pos_report.htm'), '', array('filter' => $data['filter'], 'page_count' => $data['page_count']));
}
else
{
	/* 权限判断 */
	admin_priv($admin_priv);
    $start_date = local_strtotime($_REQUEST['start_date']);
    $end_date   = local_strtotime($_REQUEST['end_date']);
    if (!isset($_REQUEST['start_date']))
    {
        $start_date = month_start();
    }
    if (!isset($_REQUEST['end_date']))
    {
        $end_date = month_end();
    }

    $data = get_sale_report($start_date, $end_date);
    $goods_data = get_goods_report($start_date, $end_date);
	/* 赋值到模板 */
	// $smarty->assign('filter',           $data['filter']);
	// $smarty->assign('record_count',     $data['record_count']);
    // $smarty->assign('page_count',       $data['page_count']);
    $smarty->assign('data',             $data['data']);
    $smarty->assign('goods_data',       $goods_data['data']);
    $smarty->assign('salesperson_name', $data['salesperson_name']);
	$smarty->assign('ur_here',        '展場報告');
	$smarty->assign('full_page',      1);
	$smarty->assign('start_date',     local_date('Y-m-d', $start_date));
	$smarty->assign('end_date',       local_date('Y-m-d', $end_date));
	$smarty->assign('cfg_lang',       $_CFG['lang']);
	$smarty->assign('deprecated', $_REQUEST['deprecatedflow'] == 'Y');
	/* 显示页面 */
	assign_query_info();
	$smarty->display('exhibition_pos_report.htm');
}

function month_start()
{
	return local_strtotime('-5 day');
}

function month_end()
{
	return  local_strtotime(local_date('Y-m-d'));
}


/* This is get sale report (With unshipping) */
function get_sale_report($start_date, $end_date){

    global $ecs, $db, $_LANG;
    $filter['start_date'] = empty($start_date) ? month_start() : $start_date;
    $filter['end_date'] = empty($end_date) ? month_end() : $end_date;
    $filter['record_count'] = floor(abs($filter['start_date'] - $filter['end_date']) / 86400) + 1;
    $filter['page_count'] = floor(abs($filter['start_date'] - $filter['end_date']) / 86400) + 1;

    $day_data = array();
    // We get all the payment id and name in this date:
    $sql = "SELECT pay_id, pay_name FROM ".$ecs->table('order_info')." WHERE 1 AND add_time BETWEEN " .  $filter['start_date'] . " AND " . $filter['end_date'] ." AND platform = 'pos_exhibition' GROUP BY pay_id";

    $payments = $db->getAll($sql);

    foreach($payments as $row){
        $pay[$row['pay_id']]['pay_name']    = $row['pay_name'];
        $pay[$row['pay_id']]['order_total'] = price_format(0, false);
    }

    // We get all the salesperson in this date:
    $sql = "SELECT salesperson FROM ".$ecs->table('order_info')." WHERE 1 AND add_time BETWEEN " .  $filter['start_date'] . " AND " . $filter['end_date'] ." AND platform = 'pos_exhibition' GROUP BY salesperson";
    $salesperson_name = $db->getCol($sql);

    foreach($salesperson_name as $row){
        $salesperson[$row]['name']                = $row;
        $salesperson[$row]['pay']                 = $pay;
        $salesperson[$row]['order_total']         = 0;
        $salesperson[$row]['order_total_foramt']  =  price_format(0, false);
    }
    for($start_time = $filter['start_date'];$start_time < ($filter['end_date']+86400);$start_time += 86400)
    {
        $daliy = [];
        $daliy['date']        = local_date('Y-m-d', $start_time);

        $daliy['salesperson'] = $salesperson;
        $daliy['order_total'] = 0;

        $end_time = $start_time + 86399;
        // money_paid(已付金額) + order_amount(應付金額) = 訂單總金額
        $sql = "SELECT IFNULL(sum(o.money_paid + o.order_amount), 0) as order_total, o.pay_id, count(o.order_id) as total_order_num, o.salesperson ".
        "FROM " .$ecs->table('order_info')." as o ".
        "WHERE 1 AND add_time BETWEEN " . $start_time . " AND " . $end_time ." AND o.platform = 'pos_exhibition' ".order_query_sql('payed', 'o.')." GROUP BY o.pay_id, o.salesperson";

        $res = $db->getAll($sql);
        foreach ($res as $row) {
            $key = $row['salesperson'];
            $daliy['salesperson'][$key]['pay'][$row['pay_id']]['order_total'] = price_format($row['order_total'], false);
            $daliy['salesperson'][$key]['order_total'] += $row['order_total'];
            $daliy['salesperson'][$key]['order_total_foramt'] = price_format($daliy['salesperson'][$key]['order_total'], false);
            $daliy['order_total'] += $row['order_total'];
        }
        $daliy['order_total'] = price_format($daliy['order_total'], false);
        $arr[] = $daliy;
    }

    return [
        'filter' => $filter,
        'page_count' => $filter['page_count'],
        'record_count' => $filter['record_count'],
        'salesperson_name' => $salesperson_name,
        'data' => $arr];
}

/* This is get goods report (Without shipping) */
function get_goods_report($start_date, $end_date){

    global $ecs, $db, $_LANG;
    $filter['start_date'] = empty($start_date) ? month_start() : $start_date;
    $filter['end_date'] = empty($end_date) ? month_end() : $end_date;
    $filter['record_count'] = floor(abs($filter['start_date'] - $filter['end_date']) / 86400) + 1;

    $sql = "SELECT SUM(og.goods_number) as sell_number, og.goods_name, og.goods_id, og.goods_sn, SUM(og.goods_number * og.goods_price) as goods_total ".
    "FROM ".$ecs->table('order_info')." as o ".
    "LEFT JOIN ".$ecs->table('order_goods')." as og on og.order_id = o.order_id ".
    "WHERE 1 AND add_time BETWEEN " . $start_date . " AND " . $end_date ." AND o.platform = 'pos_exhibition' ".order_query_sql('payed', 'o.')." and o.shipping_id != 3 group by og.goods_id order by sell_number desc, og.goods_name";

    $res = $db->getAll($sql);

    return [
        'filter' => $filter,
        'data' => $res
    ];
}