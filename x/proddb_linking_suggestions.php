<?php

/***
* proddb_linking_suggestions.php
* by howang 2015-10-29
*
* Simple page for linking goods with Product Database
***/

define('IN_ECS', true);
require(dirname(__FILE__) . '/includes/init.php');


if ($_REQUEST['act'] == 'list')
{
    $list = get_linking_suggestions();
    $smarty->assign('ur_here',      '產品資料庫連結');
    $smarty->assign('full_page',    1);

    $smarty->assign('list',         $list['data']);
    $smarty->assign('filter',       $list['filter']);
    $smarty->assign('record_count', $list['record_count']);
    $smarty->assign('page_count',   $list['page_count']);
    
    $sort_flag  = sort_flag($list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);

    assign_query_info();
    $smarty->display('proddb_linking_suggestions.htm');
}
elseif ($_REQUEST['act'] == 'query')
{
    $list = get_linking_suggestions();
    $smarty->assign('list',         $list['data']);
    $smarty->assign('filter',       $list['filter']);
    $smarty->assign('record_count', $list['record_count']);
    $smarty->assign('page_count',   $list['page_count']);

    $sort_flag  = sort_flag($list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);

    make_json_result($smarty->fetch('proddb_linking_suggestions.htm'), '', array('filter' => $list['filter'], 'page_count' => $list['page_count']));
}
elseif ($_REQUEST['act'] == 'edit_proddb_id')
{
    $goods_id = intval($_POST['id']);
    $proddb_id = intval($_POST['val']);
    
    if (!empty($goods_id) && !empty($proddb_id))
    {
        $sql = "UPDATE " . $ecs->table('goods') . " " .
                "SET `proddb_id` = '" . $proddb_id . "' " .
                ", `proddb_sync_time` = '0' " .
                "WHERE `goods_id` = '" . $goods_id . "'";
        if ($db->query($sql))
        {
            make_json_result($proddb_id);
        }
        else
        {
            make_json_error('產品連結失敗');
        }
    }
}

function get_linking_suggestions()
{
    $filter['sort_by']      = empty($_REQUEST['sort_by']) ? 'sales' : trim($_REQUEST['sort_by']);
    $filter['sort_order']   = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
    
    $sql = "SELECT count(*) " .
            "FROM " . $GLOBALS['ecs']->table('goods') . " as g " .
            "LEFT JOIN c0anyspecs.proddb_products as p " .
                "ON g.goods_name LIKE concat(p.product_name, '%') " .
            "WHERE g.proddb_id = 0 AND g.is_delete = 0 ";
    $filter['record_count'] = $GLOBALS['db']->getOne($sql);
    
    /* 分页大小 */
    $filter = page_and_size($filter);
    
    /* 查询 */
    $sql = "SELECT g.goods_id, g.goods_sn, g.goods_name, g.sales, p.product_id, p.product_name " .
            "FROM " . $GLOBALS['ecs']->table('goods') . " as g " .
            "LEFT JOIN c0anyspecs.proddb_products as p " .
                "ON g.goods_name LIKE concat(p.product_name, '%') " .
            "WHERE g.proddb_id = 0 AND g.is_delete = 0 " .
            "ORDER BY " .
                "CASE WHEN p.product_id IS NULL THEN 1 ELSE 0 END ASC, " .
                "g." . $filter['sort_by'] . " " . $filter['sort_order'] . ", " .
                "g.goods_id ASC, p.product_id ASC " .
            "LIMIT " . $filter['start'] . "," . $filter['page_size'];
    $data = $GLOBALS['db']->getAll($sql);
    
    $arr = array();
    foreach ($data as $key => $row)
    {
        if (!isset($arr[$row['goods_id']]))
        {
            $arr[$row['goods_id']] = array();
            $arr[$row['goods_id']]['goods_id'] = $row['goods_id'];
            $arr[$row['goods_id']]['goods_sn'] = $row['goods_sn'];
            $arr[$row['goods_id']]['goods_name'] = $row['goods_name'];
            $arr[$row['goods_id']]['sales'] = $row['sales'];
            $arr[$row['goods_id']]['possible_products'] = array();
            $arr[$row['goods_id']]['possible_products'][] = array(
                'value' => '',
                'name' => '請選擇...'
            );
        }
        if (!empty($row['product_id']))
        {
            $arr[$row['goods_id']]['possible_products'][] = array(
                'value' => $row['product_id'],
                'name' => $row['product_name']
            );
        }
    }
    
    $arr = array('data' => $arr, 'filter' => $filter, 'page_count' => $filter['page_count'], 'record_count' => $filter['record_count']);
    
    return $arr;
}
?>
