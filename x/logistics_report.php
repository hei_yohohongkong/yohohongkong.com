<?php

/***
* logistics_report.php
* by Anthony 20171201
*
* Logistics Report
***/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_goods.php');
require_once(ROOT_PATH . 'includes/lib_order.php');

$logisticsController = new Yoho\cms\Controller\LogisticsController();
$admin_priv = $logisticsController::ACTION_CODE_LOGISTICS;

$act = !empty($_REQUEST['act']) ? $_REQUEST['act'] : 'list';

if($act == 'list'){
    admin_priv($admin_priv);
    $_REQUEST['start_date'] = $_REQUEST['start_date'] ? $_REQUEST['start_date']:local_date('Y-m-d', strtotime('-1 month'));
    $_REQUEST['end_date']   = $_REQUEST['end_date']? $_REQUEST['end_date']:local_date('Y-m-d' );
    $report = $logisticsController->logistics_report();
    $smarty->assign('full_page',      1);
    $smarty->assign('ur_here',         "物流報表");
    $smarty->assign('data',            $report['data']);
	$smarty->assign('filter',          $report['filter']);
	$smarty->assign('start_date',      $_REQUEST['start_date']);
	$smarty->assign('end_date',        $_REQUEST['end_date']);

	// make_json_result($smarty->fetch('logistics_report.htm'), '', array('filter' => $data['filter']));
    /* 显示页面 */
	assign_query_info();
	$smarty->display('logistics_report.htm');
} elseif($act == 'query'){
    $report = $logisticsController->logistics_report();
    $smarty->assign('data',            $report['data']);
	$smarty->assign('filter',          $report['filter']);
	$smarty->assign('start_date',      $_REQUEST['start_date']);
	$smarty->assign('end_date',        $_REQUEST['end_date']);

	make_json_result($smarty->fetch('logistics_report.htm'), '', array('filter' => $report['filter']));
}



