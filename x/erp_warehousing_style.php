<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_base.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_warehouse.php');
if($_REQUEST['act'] == 'list')
{
	if((admin_priv('erp_sys_manage','',false)))
	{
		include('./includes/ERP/page.class.php');
		$page=isset($_REQUEST['page']) &&intval($_REQUEST['page']) >0 ?intval($_REQUEST['page']):1;
		$num_per_page=20;
		$mode=1;
		$page_bar_num=6;
		$page_style="page_style";
		$current_page_style="current_page_style";
		$start=$num_per_page*($page-1);
		$total_num=get_warehousing_style_count();
		$warehousing_style_list=get_warehousing_style_list(-1,$start,$num_per_page);
		$smarty->assign('warehousing_style_list',$warehousing_style_list);
		$url=fix_url($_SERVER['REQUEST_URI'],array('page'=>''));
		$pager=new page(array('total_data'=>$total_num,'data_per_page'=>$num_per_page,'url'=>$url,'mode'=>$mode,'page_bar_num'=>$page_bar_num,'page_style'=>$page_style,'current_page_style'=>$current_page_style));
		$smarty->assign('pager',$pager->show());
		$action_link = array('href'=>'erp_warehousing_style.php?act=add','text'=>$_LANG['erp_add_warehousing_style']);
		$smarty->assign('action_link',$action_link);
		$smarty->assign('url',$_SERVER['REQUEST_URI']);
		$smarty->assign('ur_here',$GLOBALS['_LANG']['erp_warehousing_style_list']);
		assign_query_info();
		$smarty->display('erp_warehousing_style_list.htm');
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif($_REQUEST['act'] == 'change_is_valid')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_sys_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	if(empty($_REQUEST['warehousing_style_id']))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	else
	{
		$warehousing_style_id=intval($_REQUEST['warehousing_style_id']);
		$sql="select is_valid from ".$ecs->table('erp_warehousing_style')." where warehousing_style_id='".$warehousing_style_id."'";
		$is_valid=$db->getOne($sql);
		if($is_valid==1)
		{
			$result['error']=0;
			$result['is_valid']=0;
			$sql="update ".$ecs->table('erp_warehousing_style')." set is_valid='0' where warehousing_style_id='".$warehousing_style_id."'";
			$db->query($sql);
		}
		else
		{
			$result['error']=0;
			$result['is_valid']=1;
			$sql="update ".$ecs->table('erp_warehousing_style')." set is_valid='1' where warehousing_style_id='".$warehousing_style_id."'";
			$db->query($sql);
		}
		die($json->encode($result));
	}
}
elseif($_REQUEST['act'] == 'remove')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_sys_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	if(empty($_REQUEST['warehousing_style_id']))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	else
	{
		$warehousing_style_id=intval($_REQUEST['warehousing_style_id']);
		$warehousing_style_info=is_warehousing_style_exist($warehousing_style_id);
		if($warehousing_style_info!==false)
		{
			$sql="select count(*) as warehousing_number from ".$ecs->table('erp_warehousing')." where warehousing_style_id='".$warehousing_style_id."'";
			$warehousing_number=$db->getOne($sql);
			if($warehousing_number>0)
			{
				$result['error']=2;
				$result['message']=$_LANG['erp_warehousing_style_has_warehousing'];
				die($json->encode($result));
			}
			$sql="delete from ".$ecs->table('erp_warehousing_style')." where warehousing_style_id='".$warehousing_style_id."'";
			if($db->query($sql))
			{
				$result['error']=0;
				die($json->encode($result));
			}
		}
		else
		{
			$result['error']=3;
			$result['message']=$_LANG['erp_warehousing_style_not_exist'];
			die($json->encode($result));
		}
	}
}
elseif($_REQUEST['act'] == 'add')
{
	if((admin_priv('erp_sys_manage','',false)))
	{
		$smarty->assign('act','insert');
		$smarty->assign('ur_here',$GLOBALS['_LANG']['erp_add_warehousing_style']);
		assign_query_info();
		$smarty->display('erp_warehousing_style_info.htm');
	}
	else
	{
		$href="erp_warehousing_style.php?act=list";
		$text=$_LANG['erp_warehousing_style_return'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif($_REQUEST['act'] == 'insert')
{
	if((admin_priv('erp_sys_manage','',false)))
	{
		$warehousing_style=isset($_REQUEST['warehousing_style']) ?trim($_REQUEST['warehousing_style']) : '';
		$is_valid=isset($_REQUEST['is_valid']) &&intval($_REQUEST['is_valid'])>0 ?intval($_REQUEST['is_valid']) : 0;
		if(empty($warehousing_style))
		{
			$href="erp_warehousing_style.php?act=list";
			$text=$_LANG['erp_warehousing_style_return'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_wrong_parameter'],0,$link);
		}
		$row=array(
			'warehousing_style'=>$warehousing_style,
			'is_valid'=>$is_valid
		);
		$GLOBALS['db']->autoExecute($GLOBALS['ecs']->table('erp_warehousing_style'),$row,'INSERT');
		$warehousing_style_id=$GLOBALS['db']->insert_id();
		$href="erp_warehousing_style.php?act=list";
		$text=$_LANG['erp_warehousing_style_return'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_warehousing_style_add_success'],0,$link);
	}
	else
	{
		$href="erp_warehousing_style.php?act=list";
		$text=$_LANG['erp_warehousing_style_return'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif($_REQUEST['act'] == 'edit')
{
	if((admin_priv('erp_sys_manage','',false)))
	{
		$warehousing_style_id=isset($_REQUEST['id']) &&intval($_REQUEST['id'])>0 ?intval($_REQUEST['id']):0;
		if(empty($warehousing_style_id))
		{
			$href="erp_warehousing_style.php?act=list";
			$text=$_LANG['erp_warehousing_style_return'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_wrong_parameter'],0,$link);
		}
		if(in_array($warehousing_style_id,array(1,2,3)))
		{
			$href="erp_warehousing_style.php?act=list";
			$text=$_LANG['erp_warehousing_style_return'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_warehousing_style_preserved'],0,$link);
		}
		$warehousing_style_info=warehousing_style_info($warehousing_style_id);
		$smarty->assign('warehousing_style_info',$warehousing_style_info);
		$smarty->assign('ur_here',$GLOBALS['_LANG']['erp_edit_warehousing_style']);
		$smarty->assign('act','update');
		assign_query_info();
		$smarty->display('erp_warehousing_style_info.htm');
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif($_REQUEST['act'] == 'update')
{
	if((admin_priv('erp_sys_manage','',false)))
	{
		$warehousing_style_id=isset($_REQUEST['warehousing_style_id']) &&intval($_REQUEST['warehousing_style_id'])>0 ?intval($_REQUEST['warehousing_style_id']) : 0;
		$warehousing_style=isset($_REQUEST['warehousing_style']) ?trim($_REQUEST['warehousing_style']) : '';
		$is_valid=isset($_REQUEST['is_valid']) &&intval($_REQUEST['is_valid'])>0 ?intval($_REQUEST['is_valid']) : 0;
		if($warehousing_style_id == 0 ||($warehousing_style_id >1 &&empty($warehousing_style)))
		{
			$href="erp_warehousing_style.php?act=list";
			$text=$_LANG['erp_warehousing_style_return'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_wrong_parameter'],0,$link);
		}
		if(in_array($warehousing_style_id,array(1,2,3)))
		{
			$href="erp_warehousing_style.php?act=list";
			$text=$_LANG['erp_warehousing_style_return'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_warehousing_style_preserved'],0,$link);
		}
		$row=array(
			'warehousing_style'=>$warehousing_style,
			'is_valid'=>$is_valid
		);
		$GLOBALS['db']->autoExecute($GLOBALS['ecs']->table('erp_warehousing_style'),$row,'UPDATE','warehousing_style_id='.$warehousing_style_id);
		$href="erp_warehousing_style.php?act=list";
		$text=$_LANG['erp_warehousing_style_return'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_warehousing_style_edit_success'],0,$link);
	}
	else
	{
		$href="erp_warehousing_style.php?act=list";
		$text=$_LANG['erp_warehousing_style_return'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
?>
