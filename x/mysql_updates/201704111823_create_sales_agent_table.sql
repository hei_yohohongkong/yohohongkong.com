CREATE TABLE `ecs_sales_agent` (
  `sa_id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `active` TINYINT NOT NULL DEFAULT 1,
  `created_at` DATETIME NULL,
  `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`sa_id`));



CREATE TABLE `ecs_sales_agent_rate` (
  `sa_rate_id` INT NOT NULL AUTO_INCREMENT,
  `sa_id` INT NOT NULL,
  `sa_rate` FLOAT NOT NULL,
  `active` TINYINT NOT NULL DEFAULT 1,
  `created_at` DATETIME NOT NULL,
  `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`sa_rate_id`),
  INDEX `sales_agent` (`sa_id` ASC));
ALTER TABLE `c0yohobeta`.`ecs_sales_agent_rate` 
ADD UNIQUE INDEX `rate_dedup` (`sa_id` ASC, `sa_rate` ASC);