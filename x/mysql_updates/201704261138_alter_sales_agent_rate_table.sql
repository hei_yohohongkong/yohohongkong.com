/**
 * Author:  Anthony
 * Created: 2017-4-26
 */

ALTER TABLE `ecs_sales_agent_rate` ADD `is_default` TINYINT(1) NOT NULL DEFAULT '0' AFTER `active`;