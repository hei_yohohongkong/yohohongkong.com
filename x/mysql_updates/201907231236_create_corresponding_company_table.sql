-- CREATE TABLE "ecs_corresponding_company" --------------------
CREATE TABLE `ecs_corresponding_company` ( 
	`corresponding_company_id` Int( 255 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`company_code` VarChar( 20 ) NULL,
	`company_name` VarChar( 100 ) NULL,
	PRIMARY KEY ( `corresponding_company_id` ),
	CONSTRAINT `unique_corresponding_company_id` UNIQUE( `corresponding_company_id` ) )
ENGINE = InnoDB;
-- -------------------------------------------------------------

-- INSERT TABLE "ecs_corresponding_company" --------------------
INSERT INTO `ecs_corresponding_company` (`company_code`, `company_name`) VALUES('yoho', 'Yoho Kong Kong Limited');
INSERT INTO `ecs_corresponding_company` (`company_code`, `company_name`) VALUES('globiz', 'Globiz Company (HK) Limited');
INSERT INTO `ecs_corresponding_company` (`company_code`, `company_name`) VALUES('yoho_holding', 'Yoho Group Holding Limited');
-- -------------------------------------------------------------
