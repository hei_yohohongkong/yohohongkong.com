CREATE TABLE ecs_shipping_type
(
    shipping_type_id int UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    shipping_type_code varchar(255) DEFAULT "",
    shipping_type_name varchar(255) DEFAULT "",
    shipping_type_desc text,
    shipping_type_icon varchar(255) DEFAULT "",
    enabled int DEFAULT 1
);

CREATE TABLE `ecs_shipping_type_lang` (
  `shipping_type_id`   INT(10) UNSIGNED NOT NULL,
  `lang`         CHAR(10)         NOT NULL,
  `shipping_type_name` VARCHAR(255),
  `shipping_type_desc`  TEXT,
  PRIMARY KEY (`shipping_type_id`, `lang`)
)
  ENGINE = InnoDB;


ALTER TABLE ecs_shipping ADD shipping_type_id int DEFAULT 0;

INSERT INTO ecs_shipping_type (shipping_type_id, shipping_type_code, shipping_type_name, shipping_type_desc, shipping_type_icon, enabled) VALUES (1, 'express', '速遞', '需時2至4日。快速送達至全澳洲各地區。如果聯邦快遞丟失包裹，友和承諾全額退款。請客人耐心等候收件。 *偏遠地區另有附加費', '', 1);
INSERT INTO ecs_shipping_type (shipping_type_id, shipping_type_code, shipping_type_name, shipping_type_desc, shipping_type_icon, enabled) VALUES (2, 'pickuppoint', '提貨點', '配送至超過500個順便智能櫃，取貨更容易。', '', 1);
INSERT INTO ecs_shipping_type (shipping_type_id, shipping_type_code, shipping_type_name, shipping_type_desc, shipping_type_icon, enabled) VALUES (3, 'store', '門市自取', '親身到觀塘門店自取', '', 1);