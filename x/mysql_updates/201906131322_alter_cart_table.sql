/**
 * Author:  Ken
 * Created: 2019-05-24
 * Sql for add package column for table "cart"
 */

ALTER TABLE `ecs_cart` ADD `is_package` SMALLINT(5) UNSIGNED NOT NULL AFTER `is_gift`;
ALTER TABLE `ecs_cart1` ADD `is_package` SMALLINT(5) UNSIGNED NOT NULL AFTER `is_gift`;