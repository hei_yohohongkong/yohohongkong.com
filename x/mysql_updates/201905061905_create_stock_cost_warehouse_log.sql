/**
 * Created: 2019-05-06
 * Sql for stock_cost_warehouse_log
 */

CREATE TABLE `ecs_stock_cost_warehouse_log` ( `log_id` INT(11) NOT NULL AUTO_INCREMENT , `data_str` LONGTEXT NOT NULL , `report_date` INT(10) NOT NULL , `created_at` INT(10) NOT NULL , PRIMARY KEY (`log_id`)) ENGINE = InnoDB CHARSET=utf8 COLLATE utf8_unicode_ci;
