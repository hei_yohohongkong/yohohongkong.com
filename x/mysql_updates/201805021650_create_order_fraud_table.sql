/**
 * Author:  Dem
 * Created: 2018-05-02
 * Sql for fraud order
 */

CREATE TABLE `ecs_order_fraud` (
    `ofid` INT NOT NULL AUTO_INCREMENT,
    `txn` VARCHAR(255),
    `order_id` INT NOT NULL,
    `pay_id` INT NOT NULL,
    `status` INT NOT NULL DEFAULT 0,
    PRIMARY KEY(`ofid`)
);
 