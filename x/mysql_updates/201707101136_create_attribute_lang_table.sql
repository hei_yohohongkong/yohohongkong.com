CREATE TABLE `ecs_attribute_lang`(
	`attr_id` SMALLINT(5) UNSIGNED NOT NULL ,
	`lang` CHAR(10) NOT NULL ,
	`attr_name` VARCHAR(255) NULL DEFAULT NULL ,
	`attr_values` TEXT NULL DEFAULT NULL,
	PRIMARY KEY (`attr_id`, `lang`)) ENGINE = InnoDB;

CREATE TABLE `ecs_goods_attr_lang` (
	`goods_attr_id` INT(10) UNSIGNED NOT NULL ,
	`lang` CHAR(10) NOT NULL ,
	`attr_value` TEXT NULL DEFAULT NULL ,
	PRIMARY KEY (`goods_attr_id`, `lang`)) ENGINE = InnoDB;
