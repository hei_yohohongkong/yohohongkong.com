CREATE TABLE `ecs_goals`
(
    `goal_id` int UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    `role_id` int UNSIGNED DEFAULT "0",
    `parent_goal_id` int UNSIGNED DEFAULT "0",
    `sender_id` int UNSIGNED DEFAULT "0",
    `admin_id` int UNSIGNED DEFAULT "0",
    `goal_type` varchar(255) DEFAULT "",
    `data_key` varchar(255) DEFAULT "",
    `data_value` varchar(255) DEFAULT "",
    `team_code` varchar(255) DEFAULT "",
    `year` varchar(255) DEFAULT "",
    `month` varchar(255) DEFAULT "",
    `start_time` INT DEFAULT "0",
    `end_time` INT DEFAULT "0",
    INDEX (`role_id`),
    INDEX (`admin_id`),
    INDEX (`team_code`)
);

ALTER TABLE `ecs_role` ADD COLUMN `team_code` varchar(255) DEFAULT "";

INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '187', 'goal_setting', '');
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '189', 'menu_goal_setting', '');
