-- CREATE TABLE "ecs_payme_activity_log" ------------------------
CREATE TABLE `ecs_payme_activity_log` ( 
	`payme_activity_log_id` Int( 10 ) AUTO_INCREMENT NOT NULL,
	`activity_initiate_datetime` Int( 50 ) NOT NULL,
	`activity_type` VarChar( 20 ) NOT NULL,
	`out_trade_code` VarChar( 100 ) NOT NULL,
	`raw_text` Text NULL,
	PRIMARY KEY ( `payme_activity_log_id` ),
	CONSTRAINT `unique_payme_activity_log_id` UNIQUE( `payme_activity_log_id` ) )
ENGINE = InnoDB;
-- -------------------------------------------------------------

-- CREATE FIELD "route" ----------------------------------------
ALTER TABLE `ecs_payme_activity_log` ADD COLUMN `route` Text NULL;
-- -------------------------------------------------------------

-- CREATE FIELD "api_version" ----------------------------------
ALTER TABLE `ecs_payme_activity_log` ADD COLUMN `api_version` Text NOT NULL;
-- -------------------------------------------------------------

-- CREATE FIELD "header" ---------------------------------------
ALTER TABLE `ecs_payme_activity_log` ADD COLUMN `header` Text NULL;
-- -------------------------------------------------------------

-- CREATE FIELD "signature" ------------------------------------
ALTER TABLE `ecs_payme_activity_log` ADD COLUMN `signature` Text NULL;
-- -------------------------------------------------------------

