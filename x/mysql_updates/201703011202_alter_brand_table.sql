/**
 * Author:  Anthony
 * Created: 2017-4-11
 */

ALTER TABLE `ecs_brand` ADD `brand_banner` VARCHAR(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `is_show`;