/**
 * Author:  Dem
 * Created: 2018-04-13
 * Sql for adding transaction group
 */

CREATE TABLE `actg_transaction_groups` (
    `txn_group_id` INT NOT NULL AUTO_INCREMENT,
    `txn_id` INT NOT NULL,
    PRIMARY KEY(`txn_group_id`)
);
INSERT INTO `actg_transaction_groups` (`txn_id`) SELECT DISTINCT `txn_id` FROM `actg_bank_transactions` WHERE `txn_id` != 0;
UPDATE `actg_bank_transactions` bt SET bt.`txn_id` = (SELECT `txn_group_id` FROM `actg_transaction_groups` tg WHERE tg.`txn_id` = bt.`txn_id`) WHERE bt.`txn_id` != 0;
ALTER TABLE `actg_bank_transactions` CHANGE `txn_id` `txn_group_id` INT DEFAULT 0;
ALTER TABLE `actg_account_transactions` ADD `txn_group_id` INT DEFAULT 0;
UPDATE `actg_account_transactions` t SET t.`txn_group_id` = (SELECT `txn_group_id` FROM `actg_transaction_groups` tg WHERE tg.`txn_id` = t.`txn_id`) WHERE t.`txn_id` IN (SELECT DISTINCT `txn_id` FROM `actg_transaction_groups`);
DROP TABLE `actg_transaction_groups`;
