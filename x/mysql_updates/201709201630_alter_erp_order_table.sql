/**
 * Author:  Dem
 * Created: 2017-9-20
 * Add column to store payment receipt location
 */

ALTER TABLE `ecs_erp_order` ADD `receipt_location` VARCHAR(200) DEFAULT NULL;
