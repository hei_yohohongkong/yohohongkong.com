/**
 * Author:  Anthony
 * Created: 2017-4-18
 */

ALTER TABLE `ecs_order_info` ADD `order_type` VARCHAR(30) NULL AFTER `language`,
ADD `type_id` INT(11) UNSIGNED NULL DEFAULT NULL AFTER `order_type`,
ADD `type_sn` VARCHAR(30) NULL AFTER `type_id`,
ADD INDEX (`order_type`), ADD INDEX (`type_id`);