/**
 * Author:  Dem
 * Created: 2018-06-11
 * Sql for crm system
 */

CREATE TABLE `ecs_crm_list` (
    `list_id` INT NOT NULL AUTO_INCREMENT,
    `ticket_id` INT NOT NULL,
    `unique_id` VARCHAR(255),
    `sender` VARCHAR(255),
    `create_at` DATETIME DEFAULT CURRENT_TIMESTAMP,
    `receive_at` DATETIME DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(`list_id`)
);

CREATE TABLE `ecs_crm_ticket_info` (
    `ticket_id` INT NOT NULL AUTO_INCREMENT,
    `platform` INT NOT NULL,
    `ref_id` VARCHAR(255),
    `status` INT NOT NULL DEFAULT 0,
    `type` INT NOT NULL DEFAULT 0,
    `priority` INT NOT NULL DEFAULT 0,
    `order_id` INT DEFAULT 0,
    `user_id` INT DEFAULT 0,
    `create_at` DATETIME DEFAULT CURRENT_TIMESTAMP,
    `last_update` DATETIME DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(`ticket_id`)
);

CREATE TABLE `ecs_crm_log` (
    `log_id` INT NOT NULL AUTO_INCREMENT,
    `admin_id` INT NOT NULL,
    `ticket_id` INT DEFAULT 0,
    `list_id` INT DEFAULT 0,
    `type` INT DEFAULT 0,
    `message` VARCHAR(255),
    `create_at` DATETIME DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(`log_id`)    
);