ALTER TABLE `ecs_delivery_order`
ADD `track_status` VARCHAR(255) NOT NULL DEFAULT '',
ADD `track_status_date` INT(10) UNSIGNED NOT NULL DEFAULT 0,
ADD INDEX (`track_status`);