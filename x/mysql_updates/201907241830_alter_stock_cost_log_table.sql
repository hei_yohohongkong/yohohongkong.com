/**
 * Author:  Dem
 * Created: 2019-07-24
 * Sql for dead stock cost log
 */

 ALTER TABLE `ecs_stock_cost_log` 
    ADD `ndayssales_30` INT DEFAULT 0,
    ADD `ndayssales_60` INT DEFAULT 0,
    ADD `ndayssales_90` INT DEFAULT 0,
    ADD `deadqty_30` INT DEFAULT 0,
    ADD `deadqty_60` INT DEFAULT 0,
    ADD `deadqty_90` INT DEFAULT 0,
    ADD `deadgroup_30` INT DEFAULT 0,
    ADD `deadgroup_60` INT DEFAULT 0,
    ADD `deadgroup_90` INT DEFAULT 0;