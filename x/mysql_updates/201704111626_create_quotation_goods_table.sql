/**
 * Author:  Anthony
 * Created: 2017-4-11
 */

CREATE TABLE `ecs_quotation_goods`
( `rec_id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT ,
`goods_id` MEDIUMINT(8) UNSIGNED NOT NULL ,
`goods_name` VARCHAR(120) NOT NULL ,
`goods_price` DECIMAL(10,2) UNSIGNED NOT NULL DEFAULT '0.00' ,
`goods_number` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0' , 
`quotation_id` MEDIUMINT(8) UNSIGNED NOT NULL ,
PRIMARY KEY (`rec_id`), INDEX (`goods_id`), INDEX (`quotation_id`)) ENGINE = InnoDB;