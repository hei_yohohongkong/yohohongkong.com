/**
 * Author:  Dem
 * Created: 2017-11-07
 * Create new table for storing daily stock cost
 */

CREATE TABLE `ecs_stock_cost_log` (
    `log_id` INT NOT NULL AUTO_INCREMENT,
    `type` INT NOT NULL DEFAULT 0,
    `type_id` INT NOT NULL DEFAULT 0,
    `cost` DECIMAL(30,2) NOT NULL DEFAULT '0.00',
    `report_date` INT(10) NOT NULL,
    `created_at` INT(10) NOT NULL,
    PRIMARY KEY (`log_id`),
    INDEX `log` (`report_date` ASC, `created_at` ASC, `type` ASC, `type_id` ASC)
)