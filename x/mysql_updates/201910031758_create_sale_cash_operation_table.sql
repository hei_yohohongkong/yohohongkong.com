
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`, `parent_cat`, `remarks`) VALUES (NULL, '189', 'menu_sale_cash_check', '', 'sale_cash_check', 'menu'), (NULL, '189', 'sale_cash_check', '', 'sale_cash_check', NULL);

CREATE TABLE `ecs_sale_cash_operation` (
  `id` int(11) NOT NULL,
  `operation_type` enum('income','expenses') NOT NULL,
  `operation_account` enum('C1','C2','D') NOT NULL,
  `operation_amount` decimal(10,2) NOT NULL,
  `operation_desc` text NOT NULL,
  `bank_name` text NOT NULL,
  `supplier_name` text NOT NULL,
  `sale_date` date NOT NULL,
  `shop_id` mediumint(8) NOT NULL,
  `user_id` mediumint(8) NOT NULL,
  `log_time` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `ecs_sale_cash_operation`
  ADD PRIMARY KEY (`id`);
  
ALTER TABLE `ecs_sale_cash_operation` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;



CREATE TABLE `ecs_sale_cash_report` (
  `id` int(11) NOT NULL,
  `report_date` date NOT NULL,
  `shop_id` mediumint(8) NOT NULL,
  `data` text NOT NULL,
  `reviewer_id` mediumint(8) NOT NULL,
  `reviewer_time` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `ecs_sale_cash_report`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `ecs_sale_cash_report` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;