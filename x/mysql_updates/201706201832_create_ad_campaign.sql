CREATE TABLE `ecs_ad_campaign` (
	`ad_campaign_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
	`ad_campaign_name` VARCHAR(255) NOT NULL DEFAULT '' ,
	`ad_campaign_desc` VARCHAR(255) NOT NULL DEFAULT '' ,
	`enabled` TINYINT(3) UNSIGNED NOT NULL DEFAULT '1' ,
	`start_time` INT(11) NOT NULL DEFAULT '0' ,
	`end_time` INT(11) NOT NULL DEFAULT '0' ,
	`created_at` DATE NOT NULL ,
	PRIMARY KEY (`ad_campaign_id`), INDEX (`enabled`)
) ENGINE = InnoDB;

ALTER TABLE `ecs_ad` ADD `ad_campaign_id` INT UNSIGNED NOT NULL DEFAULT '0' AFTER `position_id`, ADD INDEX (`ad_campaign_id`);

INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '189', 'menu_ad_campaign', '');
