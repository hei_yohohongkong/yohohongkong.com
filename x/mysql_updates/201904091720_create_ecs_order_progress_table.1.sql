
CREATE TABLE `ecs_order_progress` (
  `progress_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `shipping_id` tinyint(3) DEFAULT NULL,
  `logistics_id` int(11) DEFAULT NULL,
  `goods_status` tinyint(4) NOT NULL DEFAULT '0',
  `estimated_ship_time` int(10) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `grade` tinyint(4) DEFAULT NULL,
  `is_issue_order` tinyint(1) NOT NULL DEFAULT '0',
  `delay_seconds` int(11) NOT NULL DEFAULT '0',
  `create_date` int(10) DEFAULT NULL,
  `remark` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`progress_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '189', 'menu_order_progress_report', '');