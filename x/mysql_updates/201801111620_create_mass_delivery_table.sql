/**
 * Author:  Dem
 * Created: 2018-01-11
 * Create new table for storing public holidays
 */

CREATE TABLE `ecs_mass_delivery` (
    `mass_id` INT NOT NULL AUTO_INCREMENT,
    `order_id` INT NOT NULL UNIQUE,
    `mass_key` VARCHAR(255) NOT NULL,
    `process` INT NOT NULL DEFAULT 0,
    PRIMARY KEY(`mass_id`)
);