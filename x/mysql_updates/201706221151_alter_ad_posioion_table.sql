ALTER TABLE `ecs_ad_position` ADD `display_once` TINYINT(1) NOT NULL DEFAULT '0' AFTER `position_style`;

INSERT INTO `ecs_ad_position` (`position_id`, `position_name`, `ad_width`, `ad_height`, `position_desc`, `position_style`, `display_once`) VALUES ('47', '手機首頁彈出廣告', '900', '600', '', '<div class="SplashAd" style="display: block;"> <div class="SplashAd-container"> <i class="btnCloseSplashAd"></i> <div class="banner open"> {foreach from=$ads item=ad} {$ad} {/foreach} </div> </div> </div>', '0');

INSERT INTO `ecs_ad_position` (`position_id`, `position_name`, `ad_width`, `ad_height`, `position_desc`, `position_style`, `display_once`) VALUES ('48', '手機產品頁彈出廣告', '900', '600', '', '<div class="SplashAd" style="display: block;"> <div class="SplashAd-container"> <i class="btnCloseSplashAd"></i> <div class="banner open"> {foreach from=$ads item=ad} {$ad} {/foreach} </div> </div> </div>', '0');

INSERT INTO `ecs_ad_position` (`position_id`, `position_name`, `ad_width`, `ad_height`, `position_desc`, `position_style`, `display_once`) VALUES ('49', '手機分類頁彈出廣告', '900', '600', '', '<div class="SplashAd" style="display: block;"> <div class="SplashAd-container"> <i class="btnCloseSplashAd"></i> <div class="banner open"> {foreach from=$ads item=ad} {$ad} {/foreach} </div> </div> </div>', '0');
