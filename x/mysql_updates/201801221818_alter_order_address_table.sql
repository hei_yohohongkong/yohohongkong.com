/**
 * Author:  Dem
 * Created: 2018-01-22
 * Add column for order address
 */

ALTER TABLE `ecs_order_address` ADD `create_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;