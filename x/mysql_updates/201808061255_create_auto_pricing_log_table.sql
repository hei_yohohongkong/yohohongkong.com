/**
 * Author:  Dem
 * Created: 2018-08-06
 * Sql for auto pricing
 */
 
TRUNCATE TABLE `ecs_auto_pricing_suggestion`;
CREATE TABLE `ecs_auto_pricing_log` (
    `log_id` INT NOT NULL AUTO_INCREMENT,
    `suggest_id` INT NOT NULL,
    `goods_id` INT NOT NULL,
    `strategy_id` INT NOT NULL,
    `suggest_type` INT NOT NULL,
    `log_type` INT NOT NULL,
    `old_price` DECIMAL(10, 2) NOT NULL,
    `new_price` DECIMAL(10, 2) NOT NULL,
    `dead_group` INT DEFAULT 0,
    `log_date` DATETIME DEFAULT CURRENT_TIMESTAMP,
    `expire_date` DATETIME DEFAULT NULL,
    PRIMARY KEY(`log_id`)
);

ALTER TABLE `ecs_auto_pricing_suggestion` DROP `apply_date`;