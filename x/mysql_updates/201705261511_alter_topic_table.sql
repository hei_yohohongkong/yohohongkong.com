/**
 * @Date:   2017-05-26T14:55:59+08:00
 * @Filename: 20170526_alter_topic_table.sql
 * @Last modified time: 2017-05-26T14:55:04+08:00
 */

ALTER TABLE `ecs_topic` ADD `sort_order` TINYINT(3) UNSIGNED NOT NULL DEFAULT '50' AFTER `topic_cat_id`;
