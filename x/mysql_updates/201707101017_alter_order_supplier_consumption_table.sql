ALTER TABLE `ecs_order_goods_supplier_consumption` 
ADD COLUMN `last_updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `po_unit_cost`,
DROP INDEX `supplier_rec` ,
ADD INDEX `supplier_rec` (`order_goods_id` ASC, `supplier_id` ASC);



