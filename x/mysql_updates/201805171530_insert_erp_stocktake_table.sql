ALTER TABLE `ecs_goods` ADD `last_stocktake_time` INT(10) NULL AFTER `is_best_sellers`;

ALTER TABLE `ecs_goods_log` ADD `last_stocktake_time` INT(10) NULL AFTER `is_best_sellers`;

CREATE TABLE `ecs_erp_goods_stocktake` (
  `stocktake_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` int(10) UNSIGNED NOT NULL,
  `stocktake_person` int(11) DEFAULT NULL,
  `review_person` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `stocktake_finished_count` int(11) NOT NULL DEFAULT '0',
   PRIMARY KEY(`stocktake_id`)
);

CREATE TABLE `ecs_erp_goods_stocktake_item` (
  `goods_stocktake_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `stocktake_id` int(11) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `goods_id` int(11) DEFAULT NULL,
  `goods_sn` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `stocktake_start_time` int(10) DEFAULT NULL,
  `stocktake_end_time` int(10) DEFAULT NULL,
  `start_stock` int(11) DEFAULT NULL,
  `changed_stock` int(11) DEFAULT NULL,
  `end_stock` int(11) DEFAULT NULL,
  `update_time` int(10) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `adjust_stock` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY(`goods_stocktake_item_id`)
);
