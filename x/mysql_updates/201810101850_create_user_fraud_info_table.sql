/**
 * Author:  Dem
 * Created: 2018-10-10
 * Sql for fraud users info
 */
ALTER TABLE `ecs_order_fraud` ADD `success` INT DEFAULT 0;
ALTER TABLE `ecs_order_fraud` ADD INDEX(`order_id`);
ALTER TABLE `ecs_order_fraud` ADD INDEX(`pay_id`);

CREATE TABLE `ecs_fraud_user_info` (
    `rec_id` INT NOT NULL AUTO_INCREMENT,
    `value` VARCHAR(255) NOT NULL,
    `type` INT NOT NULL,
    PRIMARY KEY (`rec_id`)
);
INSERT INTO `ecs_fraud_user_info` (`value`, `type`) SELECT `last_ip`, 0 FROM `ecs_users` WHERE `is_fraud` != 0;
INSERT INTO `ecs_fraud_user_info` (`value`, `type`) SELECT `email`, 1 FROM `ecs_users` WHERE `is_fraud` != 0;