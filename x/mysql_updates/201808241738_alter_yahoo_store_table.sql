ALTER TABLE `ecs_yahoo_store`
  ADD `yahoo_sku` varchar(255) NOT NULL DEFAULT '',
  ADD `is_sale` tinyint(1) NOT NULL DEFAULT 1,
  ADD `is_stock` tinyint(1) NOT NULL DEFAULT 1;

ALTER TABLE ecs_yahoo_store ALTER COLUMN yahoo_quantity SET DEFAULT 100;