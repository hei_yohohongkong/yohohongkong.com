/**
 * Author:  Anthony
 * Created: 2017-4-11
 * Add article_cat icon column
 */

ALTER TABLE `ecs_article_cat` ADD `cat_icon` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `parent_id`;