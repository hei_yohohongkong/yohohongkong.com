ALTER TABLE `ecs_goods` ADD `is_best_sellers` TINYINT(1) NOT NULL DEFAULT '0';
ALTER TABLE `ecs_goods` ADD INDEX(`is_best_sellers`);
ALTER TABLE `ecs_goods_log` ADD `is_best_sellers` TINYINT(1) NOT NULL DEFAULT '0';