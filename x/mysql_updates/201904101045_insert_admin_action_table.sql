/**
 * Author:  Dem
 * Created: 2019-04-10
 * Sql for accounting system rework - trial balance
 */

INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '189', 'menu_trial_balance', '');

UPDATE `actg_accounting_types` SET `actg_type_code` = '11001' WHERE `actg_type_id` = 3;
UPDATE `actg_accounting_types` SET `actg_type_code` = '11002' WHERE `actg_type_id` = 4;
UPDATE `actg_accounting_types` SET `actg_type_code` = '11003' WHERE `actg_type_id` = 5;
UPDATE `actg_accounting_types` SET `actg_type_code` = '11004' WHERE `actg_type_id` = 6;
UPDATE `actg_accounting_types` SET `actg_type_code` = '11005' WHERE `actg_type_id` = 7;
UPDATE `actg_accounting_types` SET `actg_type_code` = '11006' WHERE `actg_type_id` = 8;
UPDATE `actg_accounting_types` SET `actg_type_code` = '11007' WHERE `actg_type_id` = 9;
UPDATE `actg_accounting_types` SET `actg_type_code` = '11008' WHERE `actg_type_id` = 10;
UPDATE `actg_accounting_types` SET `actg_type_code` = '11009' WHERE `actg_type_id` = 11;
UPDATE `actg_accounting_types` SET `actg_type_code` = '11010' WHERE `actg_type_id` = 12;
UPDATE `actg_accounting_types` SET `actg_type_code` = '11011' WHERE `actg_type_id` = 13;
UPDATE `actg_accounting_types` SET `actg_type_code` = '11012' WHERE `actg_type_id` = 14;
UPDATE `actg_accounting_types` SET `actg_type_code` = '11013' WHERE `actg_type_id` = 15;
UPDATE `actg_accounting_types` SET `actg_type_code` = '11014' WHERE `actg_type_id` = 16;
UPDATE `actg_accounting_types` SET `actg_type_code` = '11015' WHERE `actg_type_id` = 17;
UPDATE `actg_accounting_types` SET `actg_type_code` = '11016' WHERE `actg_type_id` = 18;
UPDATE `actg_accounting_types` SET `actg_type_code` = '11017' WHERE `actg_type_id` = 19;
UPDATE `actg_accounting_types` SET `actg_type_code` = '11018' WHERE `actg_type_id` = 20;
UPDATE `actg_accounting_types` SET `actg_type_code` = '11019' WHERE `actg_type_id` = 21;
UPDATE `actg_accounting_types` SET `actg_type_code` = '11020' WHERE `actg_type_id` = 22;
