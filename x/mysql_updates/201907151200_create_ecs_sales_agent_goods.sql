/**
 * Author:  Sing
 * Created: 2019-07-15
 */


CREATE TABLE `c0yohobeta`.`ecs_sales_agent_goods` 
    ( `sa_id` INT(11) NOT NULL , 
        `goods_id` MEDIUMINT(8) NOT NULL , 
        `sa_price` DECIMAL(10,2) NOT NULL DEFAULT '0.00' , 
        `updated_at` TIMESTAMP on update CURRENT_TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP , 
        PRIMARY KEY (`sa_id`, `goods_id`)
    );

ALTER TABLE `ecs_sales_agent` ADD `pay_type` TINYINT(3) NOT NULL DEFAULT '0' AFTER `name`;
