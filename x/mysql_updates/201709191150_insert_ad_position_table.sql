/* Index Ads */
INSERT INTO `ecs_ad_position`
    (`position_id`, `position_name`, `ad_width`, `ad_height`, `position_desc`, `position_style`, `display_once`)
    VALUES
    (NULL, '桌面版-首頁輪播右邊廣告-上', '330', '240', '2017/09/20-New UI AD', '<table cellpadding="0" cellspacing="0"> {foreach from=$ads item=ad} <tr><td>{$ad}</td></tr> {/foreach} </table>', '0');

INSERT INTO `ecs_ad_position`
    (`position_id`, `position_name`, `ad_width`, `ad_height`, `position_desc`, `position_style`, `display_once`)
    VALUES
    (NULL, '桌面版-首頁輪播右邊廣告-下', '330', '120', '2017/09/20-New UI AD', '<table cellpadding="0" cellspacing="0"> {foreach from=$ads item=ad} <tr><td>{$ad}</td></tr> {/foreach} </table>', '0');

INSERT INTO `ecs_ad_position`
    (`position_id`, `position_name`, `ad_width`, `ad_height`, `position_desc`, `position_style`, `display_once`)
    VALUES
    (NULL, '桌面版-首頁下方廣告-左', '420', '200', '2017/09/20-New UI AD', '<table cellpadding="0" cellspacing="0"> {foreach from=$ads item=ad} <tr><td>{$ad}</td></tr> {/foreach} </table>', '0');

INSERT INTO `ecs_ad_position`
    (`position_id`, `position_name`, `ad_width`, `ad_height`, `position_desc`, `position_style`, `display_once`)
    VALUES
    (NULL, '桌面版-首頁下方廣告-中', '420', '200', '2017/09/20-New UI AD', '<table cellpadding="0" cellspacing="0"> {foreach from=$ads item=ad} <tr><td>{$ad}</td></tr> {/foreach} </table>', '0');

INSERT INTO `ecs_ad_position`
    (`position_id`, `position_name`, `ad_width`, `ad_height`, `position_desc`, `position_style`, `display_once`)
    VALUES
    (NULL, '桌面版-首頁下方廣告-右', '420', '200', '2017/09/20-New UI AD', '<table cellpadding="0" cellspacing="0"> {foreach from=$ads item=ad} <tr><td>{$ad}</td></tr> {/foreach} </table>', '0');

INSERT INTO `ecs_ad_position`
    (`position_id`, `position_name`, `ad_width`, `ad_height`, `position_desc`, `position_style`, `display_once`)
    VALUES
    (NULL, '桌面版-首頁分類格下方廣告-1', '1330', '90', '2017/09/20-New UI AD', '<table cellpadding="0" cellspacing="0"> {foreach from=$ads item=ad} <tr><td>{$ad}</td></tr> {/foreach} </table>', '0');

INSERT INTO `ecs_ad_position`
    (`position_id`, `position_name`, `ad_width`, `ad_height`, `position_desc`, `position_style`, `display_once`)
    VALUES
    (NULL, '桌面版-首頁分類格下方廣告-2', '1330', '90', '2017/09/20-New UI AD', '<table cellpadding="0" cellspacing="0"> {foreach from=$ads item=ad} <tr><td>{$ad}</td></tr> {/foreach} </table>', '0');

/* Search, Category, Brands, Article, Article list Ads */
INSERT INTO `ecs_ad_position`
    (`position_id`, `position_name`, `ad_width`, `ad_height`, `position_desc`, `position_style`, `display_once`)
    VALUES
    (NULL, '桌面版-左邊分類樹下方廣告/文章右邊廣告', '239', '154', '2017/09/20-New UI AD(搜尋頁, 分類頁, 品牌頁, 文章頁, 文章分類頁)', '<table cellpadding="0" cellspacing="0"> {foreach from=$ads item=ad} <tr><td>{$ad}</td></tr> {/foreach} </table>', '0');

/* Category, Brands, Article, Article list, Product, Search Ads */
INSERT INTO `ecs_ad_position`
    (`position_id`, `position_name`, `ad_width`, `ad_height`, `position_desc`, `position_style`, `display_once`)
    VALUES
    (NULL, '桌面版-上方廣告位', '1330', '160', '2017/09/20-New UI AD(搜尋頁, 分類頁, 品牌頁, 文章頁, 文章分類頁, 產品頁)', '<table cellpadding="0" cellspacing="0"> {foreach from=$ads item=ad} <tr><td>{$ad}</td></tr> {/foreach} </table>', '0');

/* Product and Topic Ads */
INSERT INTO `ecs_ad_position`
    (`position_id`, `position_name`, `ad_width`, `ad_height`, `position_desc`, `position_style`, `display_once`)
    VALUES
    (NULL, '桌面版-產品頁右邊廣告/專題頁左邊廣告', '218', '115', '2017/09/20-New UI AD(產品頁, 專題頁)', '<table cellpadding="0" cellspacing="0"> {foreach from=$ads item=ad} <tr><td>{$ad}</td></tr> {/foreach} </table>', '0');

/* Article , Article list Ads */
INSERT INTO `ecs_ad_position`
    (`position_id`, `position_name`, `ad_width`, `ad_height`, `position_desc`, `position_style`, `display_once`)
    VALUES
    (NULL, '桌面版-文章內文右邊廣告', '200', '120', '2017/09/20-New UI AD', '<table cellpadding="0" cellspacing="0"> {foreach from=$ads item=ad} <tr><td>{$ad}</td></tr> {/foreach} </table>', '0');