/**
 * Author:  Dem
 * Created: 2017-9-11
 * Add columns related to bank account
 */

ALTER TABLE `ecs_erp_supplier` ADD `bank_name` VARCHAR(120) DEFAULT NULL, ADD `bank_account` VARCHAR(120) DEFAULT NULL, ADD `bank_contact` VARCHAR(120) DEFAULT NULL;
