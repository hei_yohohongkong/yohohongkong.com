/**
 * Author:  Dem
 * Created: 2018-02-09
 * Alter table for hidden accounts
 */

 ALTER TABLE `actg_accounts` ADD COLUMN `is_hidden` INT DEFAULT 0;