/**
 * Author:  Dem
 * Created: 2018-01-25
 * Add column for auto update description
 */

ALTER TABLE `ecs_brand` ADD `is_update` INT(1) NOT NULL DEFAULT 1;
ALTER TABLE `ecs_category` ADD `is_update` INT(1) NOT NULL DEFAULT 1;
UPDATE `ecs_category` SET `is_update` = 0 WHERE `parent_id` = 0 OR `parent_id` IN (SELECT * FROM (SELECT `cat_id` FROM `ecs_category` WHERE `parent_id` = 0) c)
