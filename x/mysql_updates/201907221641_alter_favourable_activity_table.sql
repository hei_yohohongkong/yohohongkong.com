-- ADD TAG OF TABLE "ecs_favourable_activity" --------------------------
ALTER TABLE `ecs_favourable_activity` ADD `act_tag` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `users_list`;
ALTER TABLE `ecs_favourable_activity_lang` ADD `act_tag` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `act_name`;
ALTER TABLE `ecs_favourable_activity` ADD `bulk_price` DECIMAL(10,2) NULL AFTER `act_tag`;

ALTER TABLE `ecs_favourable_activity` ADD `related_category` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `bulk_price`;
-- -------------------------------------------------------------

-- ADD TAG OF TABLE "ecs_goods_activity" --------------------------
ALTER TABLE `ecs_goods_activity` ADD `act_tag` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `ext_info`;
ALTER TABLE `ecs_goods_activity_lang` ADD `act_tag` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `act_desc`;

ALTER TABLE `ecs_goods_activity` ADD `related_category` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `act_tag`;
-- -------------------------------------------------------------

-- ADD TAG OF TABLE "ecs_topic" --------------------------
ALTER TABLE `ecs_topic` ADD `tag` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `redirection`;
ALTER TABLE `ecs_topic_lang` ADD `tag` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `description`;

ALTER TABLE `ecs_topic` ADD `related_category` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `tag`;
-- -------------------------------------------------------------
