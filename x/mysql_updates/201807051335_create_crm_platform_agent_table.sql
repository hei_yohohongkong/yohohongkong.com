/**
 * Author:  Dem
 * Created: 2018-07-05
 * Sql for crm system
 */

DROP TABLE `ecs_crm_chatra_agent`;

CREATE TABLE `ecs_crm_platform_agent` (
    `rec_id` INT NOT NULL AUTO_INCREMENT,
    `admin_id` INT NOT NULL,
    `platform` INT NOT NULL,
    `agent_id` VARCHAR(255) NOT NULL,
    PRIMARY KEY(`rec_id`)    
);

CREATE TABLE `ecs_crm_aircall_queue` (
    `queue_id` INT NOT NULL AUTO_INCREMENT,
    `admin_id` INT NOT NULL,
    `call_id` INT NOT NULL,
    `event` INT NOT NULL,
    `notify` INT DEFAULT 0,
    PRIMARY KEY(`queue_id`)    
);

INSERT INTO `ecs_crm_platform_agent` (`rec_id`, `admin_id`, `platform`, `agent_id`) VALUES (NULL, '1', '4', 'MWjJzH6SJZMaFRe6v');
INSERT INTO `ecs_crm_platform_agent` (`rec_id`, `admin_id`, `platform`, `agent_id`) VALUES (NULL, '1', '5', '215295');
INSERT INTO `ecs_crm_platform_agent` (`rec_id`, `admin_id`, `platform`, `agent_id`) VALUES (NULL, '109', '5', '216483');
INSERT INTO `ecs_crm_platform_agent` (`rec_id`, `admin_id`, `platform`, `agent_id`) VALUES (NULL, '97', '5', '216484');