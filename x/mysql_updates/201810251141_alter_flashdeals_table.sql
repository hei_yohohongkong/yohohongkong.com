ALTER TABLE `ecs_flashdeals`
  ADD `is_online_pay` tinyint(1) NOT NULL DEFAULT 0;

ALTER TABLE `ecs_flashdeal_notify`
  ADD `is_send` tinyint(1) NOT NULL DEFAULT 0;