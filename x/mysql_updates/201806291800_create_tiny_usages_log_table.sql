/**
 * Author:  Eric
 * Created: 2018-06-29
 * Sql for tiny image compression
 */
CREATE TABLE `ecs_tiny_usages_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `api_key` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `year` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `month` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_path` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `save_path` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `add_time` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `remark` text COLLATE utf8_unicode_ci,
    PRIMARY KEY(`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `ecs_goods_image_compressions_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `date_period` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `goods_id` int(11) DEFAULT NULL,
  `goods_create_date` int(10) UNSIGNED DEFAULT NULL,
  `goods_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `original_size` int(11) DEFAULT NULL,
  `final_size` int(11) DEFAULT NULL,
  `save` double DEFAULT NULL,
  `start_time` int(10) DEFAULT NULL,
  `finish_time` int(10) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
    PRIMARY KEY(`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '1', 'image_compression_product', '');
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '189', 'menu_image_compression_product', '');
