INSERT INTO
    `ecs_mail_templates` (
        `template_id`,
        `template_code`,
        `is_html`,
        `template_subject`,
        `template_content`,
        `last_modify`,
        `last_send`,
        `type`
    )
VALUES
    (
        NULL,
        'program_verify_email',
        '1',
        '驗證電郵',
        '{$verify_url}',
        '0',
        '0',
        'template'
    );