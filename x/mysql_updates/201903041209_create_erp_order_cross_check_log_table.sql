CREATE TABLE `ecs_erp_order_cross_check_log` (
  `cross_check_id` int(11) NOT NULL AUTO_INCREMENT,
  `erp_order_id` int(11) NOT NULL,
  `passed` tinyint(1) NOT NULL DEFAULT '0',
  `operator` int(11) NOT NULL,
  `log` text COLLATE utf8_unicode_ci,
  `add_time` int(10) DEFAULT NULL,
  `warehousing_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`cross_check_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
