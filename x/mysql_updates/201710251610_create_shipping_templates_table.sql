/**
 * Author:  Dem
 * Created: 2017-10-25
 * Create table for shipping templates and add column to ecs_goods table
 */

 CREATE TABLE `ecs_shipping_templates`(
	`template_id` INT(11) NOT NULL AUTO_INCREMENT,
	`template_name` VARCHAR(70) NOT NULL ,
	`template_content` TEXT NOT NULL ,
	PRIMARY KEY (`template_id`)) ENGINE = InnoDB;

CREATE TABLE `ecs_shipping_templates_lang`(
	`template_id` INT(11) NOT NULL AUTO_INCREMENT,
    `lang` CHAR(10) NOT NULL,
	`template_content` TEXT DEFAULT NULL,
	PRIMARY KEY (`template_id`)) ENGINE = InnoDB;

ALTER TABLE `ecs_goods` ADD `shipping_note` TEXT NOT NULL, ADD `shipping_template_id` INT(11) NOT NULL DEFAULT 0;
ALTER TABLE `ecs_goods_log` ADD `shipping_note` TEXT NOT NULL, ADD `shipping_template_id` INT(11) NOT NULL DEFAULT 0;
ALTER TABLE `ecs_goods_lang` ADD `shipping_note` TEXT DEFAULT NULL;

INSERT INTO `ecs_admin_action` (`parent_id`, `action_code`) VALUES ('189', 'menu_shipping_templates');