/**
 * Author:  Ken
 * Created: 2019-06-20
 * Sql for add package column for table "order_goods"
 */

ALTER TABLE `ecs_order_goods` ADD `is_package` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0' AFTER `warehouse_id`;