/**
 * @Author: Anthony
 * @Date:   2017-05-16T15:33:47+08:00
 * @Filename: 201705161530_create_hktvmall_table.sql
 */

CREATE TABLE `ecs_hktvmall` (
	`hktv_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
	`goods_id` INT UNSIGNED NOT NULL DEFAULT '0' ,
	`hktv_qty` INT NOT NULL DEFAULT '50' ,
	`hktv_price` DECIMAL(10,2) NOT NULL DEFAULT '0.00' ,
	`hktv_rate` VARCHAR(10) NOT NULL ,
	`hktv_brand` VARCHAR(60) NOT NULL ,
	`created_at` INT NOT NULL ,
	`creator` VARCHAR(60) NOT NULL ,
	INDEX (`goods_id`), PRIMARY KEY(`hktv_id`), INDEX (`hktv_brand`)
)	ENGINE = InnoDB;

/*  Insert hktvmall admin action */
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '187', 'hktvmall', '');
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '189', 'menu_hktvmall', '');
