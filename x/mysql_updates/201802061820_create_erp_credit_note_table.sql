/**
 * Author:  Dem
 * Created: 2018-02-06
 * Create table for erp credit note
 */

CREATE TABLE `ecs_erp_credit_note` (
    `cn_id` INT NOT NULL AUTO_INCREMENT,
    `cn_sn` VARCHAR(12) NOT NULL,
    `supplier_id` INT NOT NULL,
    `init_amount` DECIMAL(10,2) NOT NULL,
    `curr_amount` DECIMAL(10,2) NOT NULL,
    `create_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `admin_id` INT NOT NULL,
    PRIMARY KEY(`cn_id`)
);

CREATE TABLE `actg_credit_note_transactions` (
    `cn_txn_id` INT NOT NULL AUTO_INCREMENT,
    `cn_id` INT NOT NULL,
    `txn_id` INT DEFAULT NULL,
    `related_cn_txn_id` INT DEFAULT NULL,
    `amount` DECIMAL(10,2) NOT NULL,
    `remark`  VARCHAR(255) NOT NULL,
    `create_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(`cn_txn_id`)
);

INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '137', 'erp_credit_note', '');

INSERT INTO `actg_transaction_types` (`txn_type_id`, `txn_type_name`, `txn_type_parent`) VALUES (59, "Credit Note", NULL);

ALTER TABLE `actg_account_transactions` CHANGE `remark` `remark` VARCHAR(255) NOT NULL;

ALTER TABLE `ecs_erp_order_item` ADD `cn_id` INT NOT NULL, ADD `cn_amount` DECIMAL(10,2) NOT NULL, ADD `cn_txn_id` INT NOT NULL;