/**
 * Author:  Zoe
 * Created: 2019-07-29
 * Sql for restricting the number of users and times/per person using the coupon - backend coupon info page
 */

ALTER TABLE `ecs_coupons` ADD `user_limit` TINYINT NOT NULL AFTER `exclude_flashdeal`, ADD `max_user_used` INT NOT NULL AFTER `user_limit`, ADD `times_limit` TINYINT NOT NULL  AFTER `max_user_used`, ADD `max_times_used` TINYINT NOT NULL AFTER `times_limit`;  

ALTER TABLE `ecs_coupons` CHANGE `user_limit` `user_limit` MEDIUMINT(4) NOT NULL;
ALTER TABLE `ecs_coupons` CHANGE `times_limit` `times_limit` MEDIUMINT(4) NOT NULL;
ALTER TABLE `ecs_coupons` CHANGE `max_times_used` `max_times_used` MEDIUMINT(4) NOT NULL;