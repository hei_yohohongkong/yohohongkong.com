/**
 * Author:  Dem
 * Created: 2019-04-46
 * Sql for user rank program
 */

ALTER TABLE `ecs_user_rank_program` ADD `require_proof` INT DEFAULT 0;