/**
 * Author:  Dem
 * Created: 2018-03-29
 * Sql for bank reconciliation
 */

CREATE TABLE `actg_bank_transactions` (
    `rec_id` INT NOT NULL AUTO_INCREMENT,
    `account_id` INT NOT NULL,
    `date` DATETIME DEFAULT CURRENT_TIMESTAMP,
    `type` INT NOT NULL,
    `remark` VARCHAR(255),
    `amount` decimal(10,2) DEFAULT 0,
    `txn_id` INT NOT NULL,
    PRIMARY KEY(`rec_id`)
);

CREATE TABLE `actg_bank_codes` (
    `bank_id` INT NOT NULL AUTO_INCREMENT,
    `bank_code` VARCHAR(20),
    PRIMARY KEY(`bank_id`)
);

INSERT INTO `actg_bank_codes` (`bank_code`) VALUES ('BOC'), ('HS'), ('HSBC');
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '189', 'menu_bank_reconciliation', '');
ALTER TABLE `actg_accounts` ADD `bank_id` INT DEFAULT 0;