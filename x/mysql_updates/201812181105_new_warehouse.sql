ALTER TABLE `ecs_erp_warehouse` ADD `type` TINYINT NOT NULL DEFAULT '0' AFTER `code`;
UPDATE `ecs_erp_warehouse` SET `type` = '1' WHERE `ecs_erp_warehouse`.`warehouse_id` = 1;
UPDATE `ecs_erp_warehouse` SET `type` = '2' WHERE `ecs_erp_warehouse`.`warehouse_id` = 2;
UPDATE `ecs_erp_warehouse` SET `type` = '3' WHERE `ecs_erp_warehouse`.`warehouse_id` = 3;
UPDATE `ecs_erp_warehouse` SET `code` = 'YOHO_WH_KT' WHERE `ecs_erp_warehouse`.`warehouse_id` = 5;
ALTER TABLE `ecs_erp_warehouse` ADD `is_main` BOOLEAN NOT NULL DEFAULT FALSE AFTER `type`;
UPDATE `ecs_erp_warehouse` SET `is_main` = '1' WHERE `ecs_erp_warehouse`.`warehouse_id` = 1;

INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '189', 'menu_shop_restore', '');

ALTER TABLE `ecs_order_goods` ADD `warehouse_id` INT(11) UNSIGNED NULL AFTER `user_rank`;

ALTER TABLE `ecs_erp_warehouse` ADD `parents_id` TINYINT NOT NULL DEFAULT '0' AFTER `is_main`;

ALTER TABLE `ecs_erp_warehouse` ADD `is_repair_center` TINYINT NOT NULL DEFAULT '0' AFTER `parents_id`;

UPDATE `ecs_erp_warehouse` SET `is_repair_center` = '1' WHERE `ecs_erp_warehouse`.`warehouse_id` = 1;
UPDATE `ecs_erp_warehouse` SET `parents_id` = '1' WHERE `ecs_erp_warehouse`.`warehouse_id` = 2;
UPDATE `ecs_erp_warehouse` SET `parents_id` = '1' WHERE `ecs_erp_warehouse`.`warehouse_id` = 3;

ALTER TABLE `ecs_erp_stock_transfer` ADD `collected_time` INT(10) UNSIGNED NOT NULL DEFAULT '0' AFTER `transfer_time`;
ALTER TABLE `ecs_erp_stock_transfer` ADD `reason_type` TINYINT(4) UNSIGNED NOT NULL DEFAULT '0' AFTER `remark`;
ALTER TABLE `ecs_erp_stock_transfer` ADD `operator_id` INT(10) UNSIGNED NOT NULL DEFAULT '1' AFTER `reason_type`;
ALTER TABLE `ecs_erp_stock_transfer` ADD `transport_cost` DOUBLE NOT NULL DEFAULT '0' AFTER `operator_id`;
ALTER TABLE `ecs_erp_stock_transfer` ADD `sender_remark` TEXT NULL AFTER `transport_cost`;
ALTER TABLE `ecs_erp_stock_transfer` ADD `receiver_remark` TEXT NULL AFTER `sender_remark`;
ALTER TABLE `ecs_erp_stock_transfer` ADD `delivered_time` INT(10) UNSIGNED NOT NULL DEFAULT '0' AFTER `receiver_remark`;
ALTER TABLE `ecs_erp_stock_transfer` ADD `stock_not_enough_remark` TEXT NULL AFTER `delivered_time`;
ALTER TABLE `ecs_erp_stock_transfer` ADD `auto_created` TINYINT(1) NOT NULL DEFAULT '0' AFTER `stock_not_enough_remark`;
ALTER TABLE `ecs_erp_stock_transfer` ADD `rma_id` INT(11) NULL AFTER `auto_created`;
ALTER TABLE `ecs_erp_stock_transfer` ADD `type` TINYINT(4) NOT NULL DEFAULT '1' AFTER `rma_id`;

-- CREATE TABLE `ecs_erp_stock_transfer` (
--   `transfer_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
--   `transfer_sn` char(12) NOT NULL,
--   `warehouse_from` smallint(5) UNSIGNED NOT NULL,
--   `warehouse_to` smallint(5) UNSIGNED NOT NULL,
--   `admin_from` smallint(5) UNSIGNED NOT NULL,
--   `admin_to` smallint(5) UNSIGNED NOT NULL,
--   `create_time` int(10) UNSIGNED NOT NULL,
--   `transfer_time` int(10) UNSIGNED NOT NULL,
--   `collected_time` int(10) UNSIGNED NOT NULL,
--   `locked_by` smallint(5) UNSIGNED NOT NULL,
--   `locked_time` int(10) UNSIGNED NOT NULL,
--   `last_act_time` int(10) UNSIGNED NOT NULL,
--   `is_locked` tinyint(3) UNSIGNED NOT NULL,
--   `status` tinyint(3) UNSIGNED NOT NULL,
--   `agency_id` smallint(5) UNSIGNED NOT NULL,
--   `remark` varchar(45) DEFAULT NULL,
--   `reason_type` tinyint(4) UNSIGNED NOT NULL DEFAULT '0',
--   `operator_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
--   `transport_cost` double DEFAULT '0',
--   `sender_remark` text,
--   `receiver_remark` text,
--   `delivered_time` int(10) UNSIGNED NOT NULL DEFAULT '0',
--   `stock_not_enough_remark` text,
--   `auto_created` tinyint(1) NOT NULL DEFAULT '0',
--   `rma_id` int(11) DEFAULT NULL,
--   `type` tinyint(4) NOT NULL DEFAULT '1',
--     PRIMARY KEY (`transfer_id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `ecs_erp_stock_transfer_item` ADD `requested_qty` MEDIUMINT(8) UNSIGNED NULL DEFAULT NULL AFTER `transfer_qty`;
ALTER TABLE `ecs_erp_stock_transfer_item` ADD `checked_qty` INT(11) NULL DEFAULT NULL AFTER `requested_qty`;
ALTER TABLE `ecs_erp_stock_transfer_item` ADD `received_qty` INT(11) NOT NULL DEFAULT '0' AFTER `checked_qty`;

-- CREATE TABLE `ecs_erp_stock_transfer_item` (
--   `item_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
--   `transfer_id` int(10) UNSIGNED NOT NULL,
--   `goods_id` int(10) UNSIGNED NOT NULL,
--   `attr_id` int(10) UNSIGNED NOT NULL,
--   `transfer_qty` mediumint(8) UNSIGNED NOT NULL,
--   `requested_qty` mediumint(8) UNSIGNED DEFAULT NULL,
--   `checked_qty` int(11) DEFAULT NULL,
--   `received_qty` int(11) NOT NULL,
--     PRIMARY KEY (`item_id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `ecs_erp_stock_transfer_reserved` (
  `transfer_reserved_id` int(11) NOT NULL AUTO_INCREMENT,
  `warehouse_id` int(11) NOT NULL,
  `goods_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `transfer_id` int(11) NOT NULL,
  `status` tinyint(11) NOT NULL DEFAULT '0',
    PRIMARY KEY (`transfer_reserved_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `ecs_order_goods_transfer_reserved` (
  `order_goods_transfer_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `goods_id` int(11) NOT NULL,
  `transfer_qty` int(11) NOT NULL,
  `warehouse_from` int(11) UNSIGNED NOT NULL,
  `warehouse_to` int(11) UNSIGNED NOT NULL,
  `transfer_id` int(11) DEFAULT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '0',
     PRIMARY KEY (`order_goods_transfer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `ecs_erp_stock_transfer_cross_check` (
  `transfer_sn` varchar(12) CHARACTER SET utf8 NOT NULL,
  `transfer_type` int(11) NOT NULL DEFAULT '0',
  `transfer_id` int(10) UNSIGNED NOT NULL,
  `passed` tinyint(1) NOT NULL DEFAULT '0',
  `operator` int(11) DEFAULT NULL,
  `error` tinyint(1) NOT NULL DEFAULT '0',
  `log` text COLLATE utf8_unicode_ci,
  `last_update_time` int(11) DEFAULT NULL,
    PRIMARY KEY (`transfer_sn`,`transfer_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '189', 'menu_stock_transfer_cross_check', '');

ALTER TABLE `ecs_delivery_goods` ADD `warehouse_id` INT(11) UNSIGNED NULL AFTER `order_item_id`;
ALTER TABLE `ecs_erp_order` ADD `warehouse_id` INT(11) NULL AFTER `receipt_location`;

UPDATE `ecs_erp_warehouse` SET `parents_id` = '4' WHERE `ecs_erp_warehouse`.`warehouse_id` = 5;
UPDATE `ecs_erp_warehouse` SET `type` = '2' WHERE `ecs_erp_warehouse`.`warehouse_id` = 5;
ALTER TABLE `ecs_erp_rma` ADD `warehouse_id` INT(11) NULL DEFAULT '1' AFTER `goods_admin`;
ALTER TABLE `ecs_erp_rma` ADD `repair_warehouse_id` INT(11) NULL DEFAULT '1' AFTER `warehouse_id`;
ALTER TABLE `ecs_erp_rma` ADD `repair_transfer_id` INT(10) UNSIGNED NULL AFTER `repair_warehouse_id`;
ALTER TABLE `ecs_erp_rma` ADD `repaired_transfer_id` INT(10) UNSIGNED NULL AFTER `repair_transfer_id`;

update ecs_erp_rma set warehouse_id = 1, repair_warehouse_id = 5 where status = 2;
update ecs_erp_rma set warehouse_id = 1, repair_warehouse_id = 5 where status = 3;

ALTER TABLE `ecs_erp_stock_transfer` ADD `follow_up_remark` TEXT NULL AFTER `type`;
