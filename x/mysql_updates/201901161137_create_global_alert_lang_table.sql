CREATE TABLE `ecs_global_alert_lang` (
  `alert_id`   INT(10) UNSIGNED NOT NULL,
  `lang`         CHAR(10)         NOT NULL,
  `content`  TEXT,
  PRIMARY KEY (`alert_id`, `lang`)
)
  ENGINE = InnoDB;
