/**
 * Author:  Dem
 * Created: 2019-01-08
 * Sql for crm review
 */

 ALTER TABLE `ecs_review_crm` ADD `temp` INT DEFAULT 0;
 UPDATE `ecs_review_crm` SET `temp` = `complete`;
 UPDATE `ecs_review_crm` SET `complete` = NULL;
 ALTER TABLE `ecs_review_crm` CHANGE `complete` `complete` DATETIME DEFAULT NULL;
 UPDATE `ecs_review_crm` SET `complete` = CURRENT_TIMESTAMP WHERE `temp` != 0;
 ALTER TABLE `ecs_review_crm` DROP `temp`;