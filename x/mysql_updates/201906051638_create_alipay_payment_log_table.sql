-- CREATE TABLE "ecs_alipay_payment_log" -----------------------
CREATE TABLE `ecs_alipay_payment_log` ( 
	`alipay_payment_log_id` Int( 12 ) AUTO_INCREMENT NOT NULL,
	`log_id` Int( 20 ) NOT NULL,
	`order_id` Int( 20 ) NOT NULL,
	`wallet_type` VarChar( 20 ) NOT NULL,
	`transaction_create_datetime` Int( 20 ) NULL,
	`transaction_last_modified_datetime` Int( 20 ) NULL,
	`transaction_payment_datetime` Int( 20 ) NULL,
	`currency_code` VarChar( 5 ) NOT NULL DEFAULT 'HKD',
	`out_trade_no` Int( 100 ) NOT NULL,
	`gateway_transaction_sn` Int( 100 ) NOT NULL,
	`gateway_transaction_amount` Double( 10, 2 ) NULL,
	`gateway_transaction_status` VarChar( 20 ) NOT NULL,
	`raw_text` Text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	PRIMARY KEY ( `alipay_payment_log_id` ),
	CONSTRAINT `unique_alipay_payment_log_id` UNIQUE( `alipay_payment_log_id` ) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;
-- -------------------------------------------------------------

-- CHANGE "NAME" OF "FIELD "gateway_transaction_create_datetime" 
ALTER TABLE `ecs_alipay_payment_log` CHANGE `transaction_create_datetime` `gateway_transaction_create_datetime` Int( 20 ) NULL;
-- -------------------------------------------------------------

-- CHANGE "NAME" OF "FIELD "gateway_transaction_last_modified_datetime" 
ALTER TABLE `ecs_alipay_payment_log` CHANGE `transaction_last_modified_datetime` `gateway_transaction_last_modified_datetime` Int( 20 ) NULL;
-- -------------------------------------------------------------

-- CHANGE "NAME" OF "FIELD "gateway_transaction_payment_datetime" 
ALTER TABLE `ecs_alipay_payment_log` CHANGE `transaction_payment_datetime` `gateway_transaction_payment_datetime` Int( 20 ) NULL;
-- -------------------------------------------------------------

-- CHANGE "NAME" OF "FIELD "gateway_transaction_status_code" ---
ALTER TABLE `ecs_alipay_payment_log` CHANGE `gateway_transaction_status` `gateway_transaction_status_code` VarChar( 20 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
-- -------------------------------------------------------------

-- CHANGE "TYPE" OF "FIELD "out_trade_no" ----------------------
ALTER TABLE `ecs_alipay_payment_log` MODIFY `out_trade_no` VarChar( 100 ) NOT NULL;
-- -------------------------------------------------------------

-- CHANGE "TYPE" OF "FIELD "order_id" --------------------------
ALTER TABLE `ecs_alipay_payment_log` MODIFY `order_id` VarChar( 20 ) NOT NULL;
-- -------------------------------------------------------------

-- CHANGE "TYPE" OF "FIELD "gateway_transaction_sn" ------------
ALTER TABLE `ecs_alipay_payment_log` MODIFY `gateway_transaction_sn` VarChar( 100 ) NOT NULL;
-- -------------------------------------------------------------
