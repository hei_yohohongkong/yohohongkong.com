/**
 * Author:  Anthony
 * Created: 2017-4-12
 */

/*  Insert quotation admin action */
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '187', 'quotation', '');
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '189', 'menu_quotation', '');

/* Insert quotation mail template*/
INSERT INTO `ecs_mail_templates` (`template_id`, `template_code`, `is_html`, `template_subject`, `template_content`, `last_modify`, `last_send`, `type`) VALUES (NULL, 'send_quotation', '1', '報價單', '', '0', '0', 'template');