/**
 * Author:  Dem
 * Created: 2018-12-31
 * Sql for crm system offline payment
 */

 ALTER TABLE `ecs_offline_payment` ADD `files` TEXT;
 UPDATE `ecs_offline_payment` SET `files` = CONCAT(`token`, '.', `filetype`);
 ALTER TABLE `ecs_offline_payment` DROP `filetype`;
 UPDATE `ecs_offline_payment` SET `status` = 4 WHERE `status` = 3;
 UPDATE `ecs_offline_payment` SET `status` = 3 WHERE `status` = 2;
