ALTER TABLE `ecs_hktvmall`
  ADD `hktv_sku` varchar(255) NOT NULL DEFAULT '',
  ADD `is_sale` tinyint(1) NOT NULL DEFAULT 1,
  ADD `is_stock` tinyint(1) NOT NULL DEFAULT 1;

ALTER TABLE ecs_hktvmall ALTER COLUMN hktv_qty SET DEFAULT 100;