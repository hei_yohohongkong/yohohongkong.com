/**
 * Author:  Dem
 * Created: 2019-01-23
 * Sql for rearrange shop config
 */

 INSERT INTO `ecs_shop_config` (`parent_id`, `code`, `type`, `value`) VALUES ('0', 'api_config', 'group', '');
 INSERT INTO `ecs_shop_config` (`parent_id`, `code`, `type`, `value`) VALUES ('0', 'system_config', 'group', '');
 UPDATE `ecs_shop_config` SET `type` = 'hidden' WHERE `code` IN ("wap","shop_country","shop_province","shop_city","shop_address","qq","ww","skype","ym","msn","user_notice","shop_notice","default_storage","bgcolor","article_title_length","name_of_region_1","name_of_region_2","name_of_region_3","name_of_region_4","can_invoice","use_bonus","use_surplus","invoice_content","invoice_type","wap_config","wap_logo");
 UPDATE `ecs_shop_config` SET `parent_id` = '938' WHERE `code` IN ("trackingmore_api","fb_access_token","tiny_api_key_1","tiny_api_key_2","tiny_limit","tiny_enable","stats_code");
 UPDATE `ecs_shop_config` SET `parent_id` = '939' WHERE `code` IN ("lang","cache_time","cron_method","icp_number","icp_file","enable_gzip","timezone","upload_size_limit","rewrite");
 UPDATE `ecs_shop_config` SET `parent_id` = '2' WHERE `code` IN ("time_format","currency_format");
 UPDATE `ecs_shop_config` SET `parent_id` = '1' WHERE `code` IN ("search_keywords");
 UPDATE `ecs_shop_config` SET `sort_order` = '0' WHERE `code` IN ("shop_name","shop_title","shop_desc","shop_keywords","search_keywords","time_format","currency_format","integral_scale","integral_percent");
 UPDATE `ecs_shop_config` SET `sort_order` = '2' WHERE `code` IN ("watermark","watermark_place","watermark_alpha");
 UPDATE `ecs_shop_config` SET `type` = 'tel' WHERE `code` = "service_phone";
 UPDATE `ecs_shop_config` SET `type` = 'email' WHERE `code` = "service_email";
 UPDATE `ecs_shop_config` SET `type` = 'number' WHERE `code` IN ("watermark_alpha", "cache_time", "register_points", "min_goods_amount", "market_price_rate", "integral_scale", "integral_percent", "article_number", "article_page_size", "attr_related_number", "bought_goods", "comments_number", "goods_gallery_number", "goods_name_length", "history_number", "image_height", "image_width", "page_size", "related_goods_number", "thumb_height", "thumb_width", "top_number");
 
 CREATE TABLE `ecs_shop_config_lang` (
     `id` SMALLINT(5) UNSIGNED NOT NULL,
     `code` VARCHAR(30) NOT NULL,
     `lang` CHAR(10) NOT NULL,
     `value` TEXT NOT NULL
 );