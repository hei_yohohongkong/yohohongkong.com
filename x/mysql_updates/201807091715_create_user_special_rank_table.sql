/**
 * Author:  Dem
 * Created: 2018-07-09
 * Sql for special user rank
 */

CREATE TABLE `ecs_user_rank_program_application` (
  `rec_id`     INT NOT NULL AUTO_INCREMENT,
  `user_id`    INT NOT NULL,
  `program_id` INT NOT NULL,
  `proof`      TEXT         DEFAULT NULL,
  `verified`   INT          DEFAULT 0,
  `stage`      INT          DEFAULT 0,
  `unique_id`  VARCHAR(255),
  `create_at`  DATETIME     DEFAULT CURRENT_TIMESTAMP,
  `expiry`     DATETIME,
  PRIMARY KEY (`rec_id`),
  INDEX (`program_id`)
);

CREATE TABLE `ecs_user_rank_program` (
  `program_id`     INT          NOT NULL AUTO_INCREMENT,
  `program_code`   VARCHAR(255) NOT NULL DEFAULT '',
  `rank_id`        INT          NOT NULL,
  `program_name`   VARCHAR(255),
  `description`    TEXT,
  `status`         INT                   DEFAULT 0,
  `validate`       INT                   DEFAULT 0,
  `default_expiry` DATETIME              DEFAULT CURRENT_TIMESTAMP,
  `theme_color`    VARCHAR(255) NOT NULL DEFAULT '',
  `banner`         VARCHAR(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`program_id`),
  INDEX (`program_code`),
  INDEX (`rank_id`)
);

CREATE TABLE `ecs_user_rank_program_lang` (
  `program_id`   INT(10) UNSIGNED NOT NULL,
  `lang`         CHAR(10)         NOT NULL,
  `program_name` VARCHAR(255),
  `description`  TEXT,
  PRIMARY KEY (`program_id`, `lang`)
)
  ENGINE = InnoDB;

INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`)
VALUES (NULL, '3', 'user_rank_validate', '');
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`)
VALUES (NULL, '189', 'menu_06_user_rank_program', '');
ALTER TABLE `ecs_member_price`
  ADD `max_amount` INT(10) UNSIGNED NOT NULL DEFAULT '0';
ALTER TABLE `ecs_cart`
  ADD `user_rank` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0';
ALTER TABLE `ecs_cart1`
  ADD `user_rank` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0';
ALTER TABLE `ecs_order_goods`
  ADD `user_rank` TINYINT(3) UNSIGNED NULL DEFAULT '0',
  ADD INDEX (`user_rank`);

/* Using By Deployed.*/
UPDATE `ecs_order_goods`
SET `user_rank` = 2
WHERE `is_vip_price` = 1;
ALTER TABLE `ecs_order_goods`
  DROP `is_vip_price`;