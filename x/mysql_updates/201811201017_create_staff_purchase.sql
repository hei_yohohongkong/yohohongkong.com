CREATE TABLE `ecs_staff_purchase` (
    `purchase_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `applicant_admin` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
    `apply_admin` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
    `status` INT(1) NOT NULL DEFAULT '0',
    `create_at` INT NOT NULL DEFAULT '0',
    `apply_time` INT NOT NULL DEFAULT '0',
    `remark` TEXT DEFAULT '',
    PRIMARY KEY (`purchase_id`),
    INDEX (`applicant_admin`),
    INDEX (`status`)
);

CREATE TABLE `ecs_staff_purchase_goods` (
    `purchase_goods_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `purchase_id` INT UNSIGNED NOT NULL,
    `goods_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
    `price` decimal(10,2) UNSIGNED NOT NULL DEFAULT '0',
    `goods_number` smallint(5) NOT NULL DEFAULT '1',
    `type` INT NOT NULL DEFAULT '0',
    `percentage` INT NOT NULL DEFAULT '0',
    `create_at` INT NOT NULL DEFAULT '0',
    PRIMARY KEY (`purchase_goods_id`),
    INDEX (`purchase_id`),
    INDEX (`goods_id`),
    INDEX (`type`)
);

INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '187', 'staff_purchase', '');
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '189', 'menu_staff_purchase', '');