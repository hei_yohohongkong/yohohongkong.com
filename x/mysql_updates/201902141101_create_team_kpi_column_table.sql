CREATE TABLE `ecs_team_kpi_column`
(
    `column_id` int UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    `column_code` varchar(70) DEFAULT "",
    `team_code` varchar(255) DEFAULT "",
    `column_name` varchar(255) DEFAULT "",
    `column_rate` DECIMAL(30,2) NOT NULL DEFAULT '0.00',
    `is_reverse` tinyint(1) NOT NULL DEFAULT 0,
    `is_disabled` tinyint(1) NOT NULL DEFAULT 0,
    `reference_key` varchar(255) DEFAULT "",
    INDEX (`team_code`), UNIQUE KEY `column_code` (`column_code`)
);
CREATE TABLE `ecs_team_kpi_record`
(
    `column_id` int UNSIGNED ,
    `target` varchar(255) DEFAULT "",
    `value` varchar(255) DEFAULT "",
    `year` varchar(10) DEFAULT "",
    `month` varchar(10) DEFAULT "",
    `start_time` INT DEFAULT "0",
    `end_time` INT DEFAULT "0",
    INDEX (`column_id`),
    PRIMARY KEY(`column_id`, `year`, `month`),
    FOREIGN KEY (column_id) REFERENCES ecs_team_kpi_column(column_id) ON DELETE CASCADE
);

INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '187', 'team_kpi', '');
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '189', 'menu_team_kpi', '');