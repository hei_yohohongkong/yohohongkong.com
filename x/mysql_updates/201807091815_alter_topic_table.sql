/**
 * Author:  Dem
 * Created: 2018-07-09
 * Sql for multi category on topic
 */

 ALTER TABLE `ecs_topic` CHANGE COLUMN `topic_cat_id` `topic_cat_id` VARCHAR(255) DEFAULT 0;