/**
 * @Date:   2017-06-05T14:38:40+08:00
 * @Filename: 201706051436_insert_into_ad_position_table.sql
 * @Last modified time: 2017-06-05T14:49:06+08:00
 */

/* Mobile AD must be hardcode */
INSERT INTO `ecs_ad_position` (`position_id`, `position_name`, `ad_width`, `ad_height`, `position_desc`, `position_style`)
VALUES ('45', '手機版 - 閃購上方廣告', '960', '134', '', '<div class="gg-container" style="margin-bottom: 20px;">{foreach from=$ads item=ad}<div>{$ad}</div>{/foreach}</div>');
