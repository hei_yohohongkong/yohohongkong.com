ALTER TABLE `ecs_user_address`
ADD `address_type` VARCHAR(30) NOT NULL DEFAULT '',
ADD `type_code` text NOT NULL,
ADD `area_type` int(1) NOT NULL DEFAULT 0;

ALTER TABLE `ecs_order_address`
ADD `address_type` VARCHAR(30) NOT NULL DEFAULT '',
ADD `type_code` text NOT NULL,
ADD `area_type` int(1) NOT NULL DEFAULT 0;