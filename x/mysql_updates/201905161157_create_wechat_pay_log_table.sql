-- CREATE TABLE "ecs_wechat_pay_log" ---------------------------
CREATE TABLE `ecs_wechat_pay_log` (
  `wechat_pay_log_id` INT(12) NOT NULL AUTO_INCREMENT,
  `log_id` INT(20) NOT NULL,
  `order_id` INT(20) NOT NULL,
  `gateway_transaction_datetime` INT(20) NOT NULL,
  `gateway_payment_datetime` INT(20) NOT NULL,
  `error_msg` VARCHAR(100) NULL,
  `currency_code` VARCHAR(5) NOT NULL DEFAULT 'HKD',
  `gateway_request_datetime` INT(20) NOT NULL,
  `out_trade_no` VARCHAR(100) NOT NULL,
  `gateway_transaction_sn` VARCHAR(100) NOT NULL,
  `cancel` INT(1) NOT NULL DEFAULT 0,
  `order_type` VARCHAR(20) NOT NULL,
  `gateway_transaction_amount` INT(12) NOT NULL,
  PRIMARY KEY (`wechat_pay_log_id`),
  UNIQUE INDEX `wechat_pay_log_id_UNIQUE` (`wechat_pay_log_id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;
-- -------------------------------------------------------------

-- CREATE UNIQUE "unique_gateway_transaction_sn" ---------------
ALTER TABLE `ecs_wechat_pay_log` ADD CONSTRAINT `unique_gateway_transaction_sn` UNIQUE( `gateway_transaction_sn` );
-- -------------------------------------------------------------

-- CREATE FIELD "transaction_status_code" ----------------------
ALTER TABLE `ecs_wechat_pay_log` ADD COLUMN `gateway_transaction_status_code` VarChar( 5 ) NULL;
-- -------------------------------------------------------------