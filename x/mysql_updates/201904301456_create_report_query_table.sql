/**
 * Author:  Billy
 * Created: 2019-04-30
 * Sql for report query records
 */

CREATE TABLE `ecs_report_query` (
  `report_query_id` INT(11) NOT NULL AUTO_INCREMENT,
  `query_date` INT(10) NOT NULL,
  `operator` VARCHAR(45) NOT NULL,
  `type` VARCHAR(45) NOT NULL,
  `filename` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`report_query_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

