/**
 * 門市管理 shop management 
 * Author:  Zoe
 * Created: 2019-09-26
 */
CREATE TABLE `ecs_important_goods` (
  `gc_id` INT NOT NULL AUTO_INCREMENT,
  `goods_id` MEDIUMINT NOT NULL,
  `is_important` TINYINT NOT NULL,
  `time` VARCHAR(8) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `estimated_sales` MEDIUMINT NOT NULL,
  PRIMARY KEY (`gc_id`));

CREATE TABLE `ecs_shop_target` (
  `id` SMALLINT NOT NULL AUTO_INCREMENT,
  `time` VARCHAR(8) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `target_volumn` INT NOT NULL,
  PRIMARY KEY (`id`));


INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`, `parent_cat`, `remarks`) VALUES (NULL, '0', 'shop_manage', '', NULL, NULL);
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`, `parent_cat`, `remarks`) VALUES (NULL, '189', 'menu_product_commission', '', 'product_commission', 'menu'), (NULL, '189', 'menu_shop_commission', '', 'shop_commission', 'menu');
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`, `parent_cat`, `remarks`) VALUES (NULL, '189', 'menu_shop_salary', '', 'shop_salary', 'menu');
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`, `parent_cat`, `remarks`) VALUES (NULL, '189', 'product_commission_select', '', 'product_commission', '');
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`, `parent_cat`, `remarks`) SELECT NULL, action_id, 'product_commission_setting', '', 'product_commission', '' FROM `ecs_admin_action` WHERE `action_code` LIKE 'shop_manage';
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`, `parent_cat`, `remarks`) SELECT NULL, action_id, 'commission_statistics', '', 'product_commission', '' FROM `ecs_admin_action` WHERE `action_code` LIKE 'shop_manage';
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`, `parent_cat`, `remarks`) SELECT NULL, action_id, 'estimated_sales_manage', '', 'product_commission', '' FROM `ecs_admin_action` WHERE `action_code` LIKE 'shop_manage';
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`, `parent_cat`, `remarks`) SELECT NULL, action_id, 'product_commission_update', '', 'product_commission', '' FROM `ecs_admin_action` WHERE `action_code` LIKE 'shop_manage';
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`, `parent_cat`, `remarks`) SELECT NULL, action_id, 'shop_target_manage', '', 'shop_commission', '' FROM `ecs_admin_action` WHERE `action_code` LIKE 'shop_manage';