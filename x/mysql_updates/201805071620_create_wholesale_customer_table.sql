/**
 * Author:  Dem
 * Created: 2018-05-07
 * Sql for storing info for wholesale invoice
 */

CREATE TABLE `ecs_wholesale_customer` (
    `wsid` INT NOT NULL AUTO_INCREMENT,
    `company` VARCHAR(255),
    `name` VARCHAR(255),
    `phone` VARCHAR(255),
    `address` VARCHAR(255),
    PRIMARY KEY(`wsid`)
);

INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '189', 'menu_wholesale_user_info', '');