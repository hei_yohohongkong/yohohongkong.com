ALTER TABLE `ecs_users` ADD `is_sms_validated` TINYINT(1) NOT NULL DEFAULT '0';
/*-- Delete old SMS setting --*/
DELETE FROM `ecs_shop_config` where `parent_id` = 8;
/*-- Insert new SMS setting --*/
INSERT INTO `ecs_shop_config` (`id`, `parent_id`, `code`, `type`, `store_range`, `store_dir`, `value`, `sort_order`) VALUES
 (NULL, '8', 'twilio_authy_api', 'text', '', '', 'rjH5WJJxb6NU6b0o7PJcz9molDNjsgkw', '1'),
 (NULL, '8', 'send_verify_sms', 'select', '0,1', '', '1', '1');

INSERT INTO `ecs_shop_config` (`parent_id`, `code`, `type`, `store_dir`, `sort_order`,`value`)
  VALUES ('6', 'affiliate_btn_img', 'file', '../data/afficheimg/', '1','');

INSERT INTO `ecs_shop_config` (`parent_id`, `code`, `type`, `store_dir`, `sort_order`,`value`)
  VALUES ('6', 'affiliate_banner_img', 'file', '../data/afficheimg/', '1','');

  INSERT INTO `ecs_shop_config` (`parent_id`, `code`, `type`, `store_dir`, `sort_order`,`value`)
  VALUES ('8', 'sms_order_shipped', 'hidden', '', '1','0');
  INSERT INTO `ecs_shop_config` (`parent_id`, `code`, `type`, `store_dir`, `sort_order`,`value`)
  VALUES ('8', 'sms_shop_mobile', 'hidden', '', '1','');
  INSERT INTO `ecs_shop_config` (`parent_id`, `code`, `type`, `store_dir`, `sort_order`,`value`)
  VALUES ('8', 'sms_order_payed', 'hidden', '', '1','0');
  INSERT INTO `ecs_shop_config` (`parent_id`, `code`, `type`, `store_dir`, `sort_order`,`value`)
  VALUES ('8', 'sms_order_placed', 'hidden', '', '1','0');