/**
 * Author:  Dem
 * Created: 2019-02-08
 * Sql for ga statistics analysis
 */

CREATE TABLE `ecs_google_statistics_lifetime` (
    `goods_id` INT NOT NULL,
    `last_update` DATETIME NOT NULL,
    `session` INT NOT NULL,
    `pageview` INT NOT NULL,
    `pageview_unique` INT NOT NULL,
    `sales` INT NOT NULL,
    PRIMARY KEY (`goods_id`)
);

 ALTER TABLE `ecs_google_statistics_temporary_view` ADD `sales_7` INT DEFAULT 0 AFTER `pageview_unique_7`;
 ALTER TABLE `ecs_google_statistics_temporary_view` ADD `sales_7to7` INT DEFAULT 0 AFTER `pageview_unique_7to7`;
 ALTER TABLE `ecs_google_statistics_temporary_view` ADD `sales_30` INT DEFAULT 0 AFTER `pageview_unique_30`;
 ALTER TABLE `ecs_google_statistics_temporary_view` ADD `sales_30to30` INT DEFAULT 0 AFTER `pageview_unique_30to30`;
