/**
 * Author:  Dem
 * Created: 2019-02-14
 * Sql for accounting system rework
 */

CREATE TABLE `actg_accounting_transactions` (
   `actg_txn_id` INT NOT NULL AUTO_INCREMENT,
   `actg_type_id` INT NOT NULL,
   `actg_ref_code` VARCHAR(60) NOT NULL DEFAULT '',
   `currency_code` CHAR(5) NOT NULL,
   `amount` DECIMAL(15,2) NOT NULL,
   `exchanged_amount` DECIMAL(15,2) NOT NULL,
   `integral_amount` INT NOT NULL,
   `exchanged_integral_amount` DECIMAL(15,4) NOT NULL,
   `txn_date` DATETIME NOT NULL,
   `reversed_txn_id` INT DEFAULT NULL,
   `related_txn_id` INT DEFAULT NULL,
   `supplier_id` INT DEFAULT NULL,
   `wsid` INT DEFAULT NULL,
   `sa_id` INT DEFAULT NULL,
   `cn_txn_id` INT DEFAULT NULL,
   `erp_order_id` INT DEFAULT NULL,
   `sales_order_id` INT DEFAULT NULL,
   `warehousing_id` INT DEFAULT NULL,
   `delivery_id` INT DEFAULT NULL,
   `rma_id` INT DEFAULT NULL,
   `refund_id` INT DEFAULT NULL,
   `remark` TEXT,
   `posted` INT NOT NULL DEFAULT 0,
   `created_at` DATETIME DEFAULT CURRENT_TIMESTAMP,
   `created_by` INT DEFAULT 0,
   `actg_group_id` INT NOT NULL DEFAULT 0,
   `txn_group_id` INT NOT NULL DEFAULT 0,
   `reversed` INT DEFAULT 0,
   PRIMARY KEY (`actg_txn_id`)
);
CREATE TABLE `actg_accounting_types` (
   `actg_type_id` INT NOT NULL AUTO_INCREMENT,
   `actg_type_name` TEXT NOT NULL,
   `actg_type_code` TEXT NOT NULL,
   `parent_type_id` INT NOT NULL,
   `enabled` INT NOT NULL DEFAULT 1,
   PRIMARY KEY (`actg_type_id`)
);
CREATE TABLE `actg_accounting_posting_log` (
   `posting_id` INT NOT NULL AUTO_INCREMENT,
   `actg_txn_id` INT NOT NULL,
   `status` INT NOT NULL,
   `posted_at` DATETIME DEFAULT CURRENT_TIMESTAMP,
   `posted_by` INT DEFAULT 0,
   PRIMARY KEY (`posting_id`)
);

INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '189', 'menu_accounting_system', '');
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '421', 'assignable_posting_op', '');
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '3', 'wholesale_user_manage', '');

ALTER TABLE `actg_transaction_types` ADD `actg_type_id` INT NOT NULL DEFAULT 0;
ALTER TABLE `actg_accounts` ADD `actg_type_id` INT NOT NULL DEFAULT 0;
ALTER TABLE `actg_credit_note_transactions` ADD `actg_txn_id` INT DEFAULT NULL AFTER `txn_id`;
ALTER TABLE `actg_order_payments` ADD `actg_txn_id` INT DEFAULT NULL;
ALTER TABLE `actg_order_payments` CHANGE `txn_id` `txn_id` INT DEFAULT NULL;
ALTER TABLE `actg_wholesale_payments` ADD `actg_txn_id` INT DEFAULT NULL;
ALTER TABLE `actg_wholesale_payments` CHANGE `txn_id` `txn_id` INT DEFAULT NULL;
ALTER TABLE `actg_purchase_payments` ADD `actg_txn_id` INT DEFAULT NULL;
ALTER TABLE `actg_purchase_payments` CHANGE `txn_id` `txn_id` INT DEFAULT NULL;
ALTER TABLE `ecs_erp_order` ADD `currency_code` CHAR(5) NOT NULL DEFAULT "HKD", ADD `actg_txn_id` INT NOT NULL DEFAULT 0;
ALTER TABLE `ecs_erp_order_item` ADD `original_price` DECIMAL(15,2) NOT NULL AFTER `price`, ADD `original_shipping_price` DECIMAL(15,2) NOT NULL AFTER `shipping_price`, ADD `original_amount` DECIMAL(15,2) NOT NULL AFTER `amount`;
UPDATE `ecs_erp_order_item` SET `original_price` = `price`, `original_shipping_price` = `shipping_price`, `original_amount` = `amount`;
ALTER TABLE `ecs_order_goods_supplier_consumption` ADD `po_unit_value` DECIMAL(15,2);
UPDATE `ecs_order_goods_supplier_consumption` SET `po_unit_value` = `po_unit_cost`;
ALTER TABLE `ecs_erp_rma` ADD `po_unit_value` DECIMAL(15,2) DEFAULT 0;
ALTER TABLE `ecs_erp_rma_stock_log` ADD `actg_txn_id` INT DEFAULT NULL;
ALTER TABLE `ecs_users` ADD `wsid` INT DEFAULT 0;
ALTER TABLE `ecs_order_info` ADD `ar_txn_id` INT DEFAULT 0;
ALTER TABLE `ecs_account_log` ADD `actg_txn_id` INT DEFAULT NULL;
ALTER TABLE `ecs_refund_requests` ADD `apply_txn_id` INT DEFAULT NULL, ADD `complete_txn_id` INT DEFAULT NULL;
ALTER TABLE `actg_currency_exchanges` CHANGE `from_txn_id` `from_txn_id` INT DEFAULT NULL, CHANGE `to_txn_id` `to_txn_id` INT DEFAULT NULL;
ALTER TABLE `actg_currency_exchanges` ADD `from_actg_txn_id` INT DEFAULT NULL, ADD `to_actg_txn_id` INT DEFAULT NULL;

INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) VALUES ('ASSETS', '10000', 0);
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Bank and Cash', '11000', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '10000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT '門店現金', '10001', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '11000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Petty Cash', '10002', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '11000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT '友和 HSBC Savings', '10003', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '11000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT '友和 HSBC Current', '10004', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '11000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT '友和 Hang Seng Savings', '10005', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '11000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT '友和 Hang Seng Current', '10006', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '11000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT '友和 BOC Savings', '10007', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '11000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT '友和 BOC Current', '10008', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '11000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Globiz HSBC HKD Savings', '10009', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '11000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Globiz HSBC HKD Current', '10010', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '11000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Globiz HSBC USD Savings', '10011', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '11000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Globiz BEA HKD Current', '10012', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '11000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Globiz BEA USD Current', '10013', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '11000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT '澳門 中銀', '10014', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '11000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT '澳門 ICBC', '10015', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '11000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT '友和 DBS', '10016', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '11000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Globiz HSBC JPY Savings', '10017', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '11000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Yoho Group HSBC HKD Savings', '10018', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '11000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Yoho Group HSBC HKD Current', '10019', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '11000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Yoho Group HSBC USD Savings', '10020', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '11000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Short-Term Investments', '12000', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '10000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Accounts Receivable', '13100', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '10000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'B2B customer', '13110', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '13100';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Online customer', '13120', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '13100';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'B2B2C customer', '13130', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '13100';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Other receivables - Credit card/Merchant accounts', '13200', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '10000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'PayPal', '13201', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '13200';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Stripe', '13202', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '13200';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT '支付寶(國際)', '13203', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '13200';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Braintree', '13204', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '13200';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Global Payment (Visa/Master)', '13205', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '13200';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'EPS', '13206', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '13200';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'American Express', '13207', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '13200';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Global Payment (AE)', '13208', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '13200';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Western Union', '13209', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '13200';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'QFPay', '13210', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '13200';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'P-Card', '13211', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '13200';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Notes Receivable (Current)', '13300', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '10000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Amount due (to)/from Director', '13400', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '10000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Franz', '13401', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '13400';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Kathy', '13402', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '13400';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Intercompany Receivables', '13500', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '10000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Yoho Group Holding', '13501', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '13500';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Globiz', '13502', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '13500';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Yoho HK', '13503', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '13500';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Deposits', '13600', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '10000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Inventory', '14000', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '10000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Inventory (Owned)', '14001', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '14000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Consignment Inventory', '14002', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '14000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Inventory (RMA)', '14003', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '14000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Prepayment', '15000', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '10000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Prepaid Rent', '15001', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '15000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Prepaid Taxes', '15002', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '15000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Prepaid Suppliers', '15003', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '15000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Other Prepaids', '15004', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '15000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Property and Equipment', '16000', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '10000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Land', '16001', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '16000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Buildings', '16002', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '16000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Accumulated Depreciation-Buildings', '16003', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '16000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Leaseholds and Leasehold Improvements', '16004', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '16000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Accumulated Depreciation-Leaseholds', '16005', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '16000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Furniture and Fixtures', '16006', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '16000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Accumulated Depreciation-Furniture and Fixtures', '16007', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '16000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Equipment', '16008', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '16000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Accumulated Depreciation-Equipment', '16009', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '16000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Other Assets', '17000', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '10000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Deposits (Non-current)', '17001', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '17000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Long-Term Deferred Tax Asset', '17002', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '17000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Miscellaneous', '17003', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '17000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Non-current Receivables', '18000', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '10000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Investments (long-term)', '19000', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '10000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) VALUES ('LIABILITIES & EQUITY', '20000', 0);
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Capital ', '21000', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '20000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Paid-in Capital (Ordinary)', '21001', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '21000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Preference Shares', '21002', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '21000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Retained Earnings', '21003', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '21000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Non-current liabilities', '22000', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '20000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Non-current loan', '22001', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '22000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Capital Leases', '22002', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '22000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Other non-current loan', '22003', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '22000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Non-current Deferred Tax Liability', '22004', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '22000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Accounts Payable', '23100', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '20000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'AP - Supplier', '23101', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '23100';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'AP - General Customer', '23102', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '23100';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Dividends Payable', '23200', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '20000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Notes Payable', '23300', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '20000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Intercompany Payables', '23400', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '20000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Yoho Group Holding', '23401', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '23400';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Globiz', '23402', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '23400';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Yoho HK', '23403', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '23400';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Advance Deposits', '25000', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '20000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Accruals', '26000', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '20000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Accrued Expenses-Rent', '25001', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '26000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Accrued Expenses-Other', '25002', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '26000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Accrued Annual Leave', '25003', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '26000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Accrued Employees Remuneration', '25004', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '26000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Accrued Taxes', '25005', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '26000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Current Debt', '27000', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '20000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Other Current Liabilities', '28000', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '20000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Reward', '28001', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '28000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Other Payables', '28002', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '28000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Current Deferred Tax Liability', '29000', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '20000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) VALUES ('REVENUES', '30000', 0);
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Sales', '31000', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '30000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Sales - B2C (Online)', '31100', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '31000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Sales - B2C (Shop)', '31200', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '31000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Sales - B2B', '31300', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '31000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Sales - B2B2C', '31400', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '31000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Staff Sales', '31500', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '31000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Refund & Return', '32000', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '30000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Sales return', '32100', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '32000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Refund', '32200', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '32000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Sales Discounts', '33000', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '30000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Sales discounts', '33100', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '33000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Other Income', '34000', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '30000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Interest Income', '34001', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '34000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Sponsorship Income', '34002', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '34000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Other Income', '34099', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '34000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) VALUES ('COST OF SALES', '40000', 0);
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Purchases', '41000', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '40000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Local Purchase', '41100', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '41000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Global Purchase (Foreign currency)', '41200', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '41000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Purchase Return', '42000', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '40000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Purchase discounts', '43000', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '40000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Discount received', '43001', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '43000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Rebates from Supplier', '43002', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '43000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Shipping (Inwards)', '44000', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '40000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Shipping Cost', '44001', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '44000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Cost of Sales', '45000', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '40000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Inventory Write Off', '46000', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '40000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) VALUES ('EXPENSES', '50000', 0);
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Salaries and Wages', '51000', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '50000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Salary - Full Time', '51001', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '51000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Salary - Part Time', '51002', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '51000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'MPF', '51003', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '51000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Bonus', '51004', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '51000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Vacation, Holiday, and Sick Pay', '51005', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '51000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Recruitment Expense', '51006', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '51000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Staff welfare', '51007', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '51000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Other staff cost', '51099', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '51000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Rental & Leases', '52000', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '50000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Rent & Rates', '52001', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '52000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Management fee', '52002', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '52000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Other Leases', '52099', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '52000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Marketing Expenses', '53000', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '50000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Google Ads', '53001', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '53000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Yahoo/Bing Ads', '53002', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '53000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Street Banners', '53003', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '53000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Facebook Ads', '53004', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '53000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Other Marketing Expenses', '53099', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '53000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Reward program', '53100', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '53000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Delivery (to customers)', '54000', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '50000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'SF Lockers', '54001', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '54000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'SF Express', '54002', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '54000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'FedEx', '54003', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '54000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Toll/DPEX', '54004', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '54000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Quantium Solutions', '54005', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '54000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'HK Post', '54006', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '54000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'SF+', '54007', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '54000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Yamato', '54008', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '54000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Other Shipping', '54099', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '54000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Others', '54099', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Shop Utilities and Sundries', '55000', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '50000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Retail Supplies', '55001', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '55000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Depreciation', '56000', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '50000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Depreciation - Buildings', '56001', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '56000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Depreciation - Leaseholds and Leasehold Improvements', '56002', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '56000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Depreciation - Furniture and Fixtures', '56003', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '56000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Depreciation - Equipment', '56004', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '56000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Depreciation - Others', '56005', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '56000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Merchant fee', '57000', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '50000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'EPS', '57001', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '57000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Western Union', '57002', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '57000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Alipay', '57003', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '57000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Global payment', '57004', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '57000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Stripe', '57005', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '57000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Braintree', '57006', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '57000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Other merchant fee', '57099', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '57000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Office Utilities and Sundries', '58000', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '50000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Electricity, Water & Gas', '58001', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '58000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Broadband, phones and server', '58002', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '58000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Stationary', '58003', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '58000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Postage & Courier', '58004', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '58000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Software & License', '58005', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '58000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Printing expense', '58006', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '58000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Travel ', '58007', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '58000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Government related (BR, AR)', '58008', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '58000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Cleaning expense', '58009', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '58000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Insurance', '58010', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '58000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Other Office Supplies', '58099', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '58000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Warehouse Utilities and Sundries', '59000', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '50000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Warehouse supplies', '59001', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '59000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Packing Materials', '59002', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '59000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Others', '59003', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '59000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) VALUES ('EXPENSES', '60000', 0);
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Other Administrative Expenses', '61000', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '60000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Custom declaration', '61001', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '61000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Bank Charges', '61002', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '61000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Transportation', '61003', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '61000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Penalty', '61004', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '61000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Entertainment', '61005', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '61000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Other adminitrative expense', '61099', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '61000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Professional fee', '62000', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '60000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Company Secretary & Accounting Services', '62001', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '62000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Audit Fee', '62002', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '62000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Others professional fee', '62099', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '62000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Interest Expense', '63000', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '60000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Interest Expense', '63001', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '63000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Tax Expense', '64000', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '60000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Profit tax', '64001', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '64000';
INSERT INTO `actg_accounting_types` (`actg_type_name`, `actg_type_code`, `parent_type_id`) SELECT 'Other tax expense', '64099', `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '64000';

UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '31100') WHERE `txn_type_id` = 1;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '31400') WHERE `txn_type_id` = 2;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '34099') WHERE `txn_type_id` = 3;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '41100') WHERE `txn_type_id` = 4;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '53099') WHERE `txn_type_id` = 5;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '53001') WHERE `txn_type_id` = 6;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '53002') WHERE `txn_type_id` = 7;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '53003') WHERE `txn_type_id` = 8;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '53099') WHERE `txn_type_id` = 9;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '54099') WHERE `txn_type_id` = 10;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '54003') WHERE `txn_type_id` = 11;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '54004') WHERE `txn_type_id` = 12;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '54005') WHERE `txn_type_id` = 13;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '54006') WHERE `txn_type_id` = 14;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '54007') WHERE `txn_type_id` = 15;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '54008') WHERE `txn_type_id` = 16;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '54099') WHERE `txn_type_id` = 17;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '58002') WHERE `txn_type_id` = 18;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '58002') WHERE `txn_type_id` = 19;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '58002') WHERE `txn_type_id` = 20;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '58006') WHERE `txn_type_id` = 21;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '53099') WHERE `txn_type_id` = 22;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '58006') WHERE `txn_type_id` = 24;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '58099') WHERE `txn_type_id` = 25;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '58001') WHERE `txn_type_id` = 26;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '58006') WHERE `txn_type_id` = 27;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '16008') WHERE `txn_type_id` = 28;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '58099') WHERE `txn_type_id` = 29;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '51001') WHERE `txn_type_id` = 30;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '51001') WHERE `txn_type_id` = 31;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '51002') WHERE `txn_type_id` = 32;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '61099') WHERE `txn_type_id` = 33;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '58009') WHERE `txn_type_id` = 34;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '61001') WHERE `txn_type_id` = 35;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '61002') WHERE `txn_type_id` = 36;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '61099') WHERE `txn_type_id` = 37;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '61005') WHERE `txn_type_id` = 38;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '52001') WHERE `txn_type_id` = 39;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '52001') WHERE `txn_type_id` = 40;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '52001') WHERE `txn_type_id` = 41;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '58001') WHERE `txn_type_id` = 42;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '58001') WHERE `txn_type_id` = 43;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '13401') WHERE `txn_type_id` = 45;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '32200') WHERE `txn_type_id` = 46;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '58002') WHERE `txn_type_id` = 49;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '57001') WHERE `txn_type_id` = 50;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '54007') WHERE `txn_type_id` = 51;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '61003') WHERE `txn_type_id` = 52;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '61003') WHERE `txn_type_id` = 53;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '53004') WHERE `txn_type_id` = 54;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '59001') WHERE `txn_type_id` = 55;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '16008') WHERE `txn_type_id` = 56;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '59002') WHERE `txn_type_id` = 57;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '58003') WHERE `txn_type_id` = 58;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '43002') WHERE `txn_type_id` = 59;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '61099') WHERE `txn_type_id` = 60;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '51006') WHERE `txn_type_id` = 61;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '58008') WHERE `txn_type_id` = 62;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '58008') WHERE `txn_type_id` = 63;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '64001') WHERE `txn_type_id` = 64;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '51003') WHERE `txn_type_id` = 65;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '58005') WHERE `txn_type_id` = 67;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '58010') WHERE `txn_type_id` = 70;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '34001') WHERE `txn_type_id` = 71;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '34002') WHERE `txn_type_id` = 73;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '62002') WHERE `txn_type_id` = 74;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '61005') WHERE `txn_type_id` = 75;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '55001') WHERE `txn_type_id` = 77;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '55001') WHERE `txn_type_id` = 78;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '59002') WHERE `txn_type_id` = 79;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '16006') WHERE `txn_type_id` = 80;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '22001') WHERE `txn_type_id` = 84;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '23200') WHERE `txn_type_id` = 85;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '22001') WHERE `txn_type_id` = 86;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '54001') WHERE `txn_type_id` = 87;
UPDATE `actg_transaction_types` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '51007') WHERE `txn_type_id` = 88;

UPDATE `actg_accounts` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '10001') WHERE `account_id` = 1;
UPDATE `actg_accounts` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '10003') WHERE `account_id` = 2;
UPDATE `actg_accounts` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '10004') WHERE `account_id` = 3;
UPDATE `actg_accounts` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '10005') WHERE `account_id` = 4;
UPDATE `actg_accounts` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '10006') WHERE `account_id` = 5;
UPDATE `actg_accounts` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '10007') WHERE `account_id` = 6;
UPDATE `actg_accounts` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '10008') WHERE `account_id` = 7;
UPDATE `actg_accounts` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '10009') WHERE `account_id` = 8;
UPDATE `actg_accounts` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '10010') WHERE `account_id` = 9;
UPDATE `actg_accounts` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '10011') WHERE `account_id` = 10;
UPDATE `actg_accounts` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '10012') WHERE `account_id` = 11;
UPDATE `actg_accounts` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '10013') WHERE `account_id` = 12;
UPDATE `actg_accounts` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '13201') WHERE `account_id` = 13;
UPDATE `actg_accounts` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '13401') WHERE `account_id` = 14;
UPDATE `actg_accounts` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '10014') WHERE `account_id` = 15;
UPDATE `actg_accounts` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '10015') WHERE `account_id` = 16;
UPDATE `actg_accounts` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '10016') WHERE `account_id` = 17;
UPDATE `actg_accounts` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '13202') WHERE `account_id` = 18;
UPDATE `actg_accounts` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '13203') WHERE `account_id` = 19;
UPDATE `actg_accounts` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '13204') WHERE `account_id` = 20;
UPDATE `actg_accounts` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '13205') WHERE `account_id` = 21;
UPDATE `actg_accounts` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '13206') WHERE `account_id` = 22;
UPDATE `actg_accounts` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '13208') WHERE `account_id` = 23;
UPDATE `actg_accounts` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '10002') WHERE `account_id` = 24;
UPDATE `actg_accounts` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '10017') WHERE `account_id` = 26;
UPDATE `actg_accounts` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '13207') WHERE `account_id` = 27;
UPDATE `actg_accounts` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '13600') WHERE `account_id` = 28;
UPDATE `actg_accounts` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '10018') WHERE `account_id` = 29;
UPDATE `actg_accounts` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '10019') WHERE `account_id` = 30;
UPDATE `actg_accounts` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '10020') WHERE `account_id` = 31;
UPDATE `actg_accounts` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '13210') WHERE `account_id` = 32;
UPDATE `actg_accounts` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '13402') WHERE `account_id` = 33;
UPDATE `actg_accounts` SET `actg_type_id` = (SELECT `actg_type_id` FROM `actg_accounting_types` WHERE `actg_type_code` = '13211') WHERE `account_id` = 34;
