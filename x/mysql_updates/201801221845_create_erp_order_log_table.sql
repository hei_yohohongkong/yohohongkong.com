/**
 * Author:  Dem
 * Created: 2018-01-04
 * Create log table for erp order
 */

CREATE TABLE `ecs_erp_order_log` (
    `log_id` INT NOT NULL AUTO_INCREMENT,
    `order_id` INT NOT NULL,
    `action` INT NOT NULL,
    `create_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `create_by` INT NOT NULL,
    `remarks` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`log_id`)
);