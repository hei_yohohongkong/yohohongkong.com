CREATE TABLE `ecs_order_address_dest_type` (
    `address_dest_type_id` SMALLINT(4) NOT NULL AUTO_INCREMENT ,
    `address_dest_type_code` VARCHAR(20) NOT NULL ,
    `address_dest_type_cat` SMALLINT(4) NOT NULL DEFAULT '1' ,
    `address_dest_type_name` VARCHAR(60) NOT NULL ,
    PRIMARY KEY (`address_dest_type_id`)
) ENGINE = InnoDB;

ALTER TABLE `ecs_order_address_dest_type` ADD UNIQUE(`address_dest_type_code`);
ALTER TABLE `ecs_order_address_dest_type` ADD `enable` TINYINT(1) NOT NULL DEFAULT '0'  AFTER `address_dest_type_name`;

INSERT INTO `ecs_order_address_dest_type` (`address_dest_type_id`, `address_dest_type_code`, `address_dest_type_cat`, `address_dest_type_name`, `enable`) VALUES 
(NULL, 'housing_estate', '1', '住宅屋苑', '1'), 
(NULL, 'single_block', '1', '單幢式住宅', '1'), 
(NULL, 'village_house', '1', '村屋', '1'), 
(NULL, 'house', '1', '獨立屋', '1'), 
(NULL, 'shopping_mall', '2', '商場', '1'), 
(NULL, 'street_shop', '2', '街舖', '1'), 
(NULL, 'comm_ind', '2', '工商大廈', '1'),
('99', 'other', '99', '其他', '1');

CREATE TABLE `ecs_order_address_dest_type_lang` (
    `address_dest_type_id` SMALLINT(4) UNSIGNED NOT NULL , 
    `lang` CHAR(10) NOT NULL , 
    `address_dest_type_name` VARCHAR(60) NULL DEFAULT NULL , 
    PRIMARY KEY (`address_dest_type_id`, `lang`)
) ENGINE = InnoDB;

INSERT INTO `ecs_order_address_dest_type_lang` (`address_dest_type_id`, `lang`, `address_dest_type_name`) VALUES 
('1', 'en_us', 'Housing Estate'), 
('1', 'zh_cn', '住宅屋苑'), 
('2', 'en_us', 'Single-Block Residential Building'), 
('2', 'zh_cn', '单幢式住宅'), 
('3', 'en_us', 'Village House'), 
('3', 'zh_cn', '村屋'), 
('4', 'en_us', 'House'), 
('4', 'zh_cn', '獨立屋'), 
('5', 'en_us', 'Shopping Mall'), 
('5', 'zh_cn', '商场'), 
('6', 'en_us', 'Street Shop'), 
('6', 'zh_cn', '街铺'), 
('7', 'en_us', 'Commercial & Industrial Building'), 
('7', 'zh_cn', '工商大厦'),
('99', 'en_us', 'Other'), 
('99', 'zh_cn', '其他');

ALTER TABLE `ecs_order_address` ADD `dest_type` SMALLINT(4) NOT NULL DEFAULT '0' AFTER `area_type`;
ALTER TABLE `ecs_order_address` ADD `lift` TINYINT(1) NOT NULL DEFAULT '0' AFTER `dest_type`;
ALTER TABLE `ecs_order_address` ADD `verify` TINYINT(1) NOT NULL DEFAULT '0' AFTER `lift`;

ALTER TABLE `ecs_user_address` ADD `dest_type` SMALLINT(4) NOT NULL DEFAULT '0' AFTER `area_type`;
ALTER TABLE `ecs_user_address` ADD `lift` TINYINT(1) NOT NULL DEFAULT '0' AFTER `dest_type`;
