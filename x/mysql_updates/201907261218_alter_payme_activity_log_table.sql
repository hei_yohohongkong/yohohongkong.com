-- DROP FIELD "activity_type" ----------------------------------
ALTER TABLE `ecs_payme_activity_log` DROP COLUMN `activity_type`;
-- -------------------------------------------------------------

-- CHANGE "NAME" OF "FIELD "request_raw_text" ------------------
ALTER TABLE `ecs_payme_activity_log` CHANGE `raw_text` `request_raw_text` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL;
-- -------------------------------------------------------------

-- CHANGE "NAME" OF "FIELD "request_header" --------------------
ALTER TABLE `ecs_payme_activity_log` CHANGE `header` `request_header` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL;
-- -------------------------------------------------------------

-- CHANGE "NAME" OF "FIELD "request_signature" -----------------
ALTER TABLE `ecs_payme_activity_log` CHANGE `signature` `request_signature` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL;
-- -------------------------------------------------------------

-- CREATE FIELD "response_header" ------------------------------
ALTER TABLE `ecs_payme_activity_log` ADD COLUMN `response_header` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL;
-- -------------------------------------------------------------

-- CREATE FIELD "response_raw_text" ----------------------------
ALTER TABLE `ecs_payme_activity_log` ADD COLUMN `response_raw_text` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL;
-- -------------------------------------------------------------

-- CREATE FIELD "response_signature" ---------------------------
ALTER TABLE `ecs_payme_activity_log` ADD COLUMN `response_signature` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL;
-- -------------------------------------------------------------

