ALTER TABLE `ecs_logistics_lang` ADD `logistics_desc` TEXT NULL DEFAULT NULL ;
ALTER TABLE `ecs_logistics_lang` CHANGE COLUMN `logistics_name` `logistics_name` varchar(120) NULL DEFAULT NULL;
ALTER TABLE `ecs_logistics` CHANGE `logistics_desc` `logistics_desc` TEXT NOT NULL;