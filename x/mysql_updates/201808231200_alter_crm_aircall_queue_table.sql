/**
 * Author:  Dem
 * Created: 2018-08-23
 * Sql for crm aircall
 */

 ALTER TABLE `ecs_crm_aircall_queue` ADD `ticket_id` INT NOT NULL;
 CREATE INDEX `ticket_id` ON `ecs_crm_aircall_queue` (`ticket_id`);