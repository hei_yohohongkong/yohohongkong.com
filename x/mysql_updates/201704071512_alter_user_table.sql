ALTER TABLE `ecs_users` 
ADD COLUMN `is_wholesale` TINYINT NOT NULL DEFAULT 0 AFTER `language`;
ALTER TABLE `ecs_users` 
ADD INDEX `wholesale` (`user_id` ASC, `is_wholesale` ASC);

UPDATE `ecs_users` SET `is_wholesale`='1' WHERE `user_id` IN (4940,5022,5023);