ALTER TABLE `ecs_goods` ADD `goods_length` decimal(10,3) unsigned NOT NULL,
ADD `goods_width` decimal(10,3) unsigned NOT NULL,
ADD `goods_height` decimal(10,3) unsigned NOT NULL;

ALTER TABLE `ecs_goods_log` ADD `goods_length` decimal(10,3) unsigned NOT NULL,
ADD `goods_width` decimal(10,3) unsigned NOT NULL,
ADD `goods_height` decimal(10,3) unsigned NOT NULL;

ALTER TABLE `ecs_category` ADD `default_length` decimal(10,3) unsigned NOT NULL,
ADD `default_width` decimal(10,3) unsigned NOT NULL,
ADD `default_height` decimal(10,3) unsigned NOT NULL;