/**
 * Author:  Eric
 * Created: 2018-05-31
 * Sql for ad
 */
ALTER TABLE `ecs_ad` ADD `ad_title` VARCHAR(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL AFTER `ad_name`;