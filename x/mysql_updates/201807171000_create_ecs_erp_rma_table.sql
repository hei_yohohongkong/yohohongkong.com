/**
 * Author:  Eric
 * Created: 2018-07-20
 * Sql for tiny image compression
 */

ALTER TABLE `ecs_erp_stock` ADD `rma_id` INT(11) NULL AFTER `act_date`;


CREATE TABLE `ecs_erp_rma` (
  `rma_id` int(11) NOT NULL AUTO_INCREMENT,
  `repair_id` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `channel` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `repair_handle_type` int(11) DEFAULT NULL,
  `department` int(11) NOT NULL DEFAULT '0',
  `reason` text COLLATE utf8_unicode_ci,
  `create_time` int(11) NOT NULL,
  `operator` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `goods_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL DEFAULT '1',
  `person_in_charge` int(11) DEFAULT NULL,
  `replace_with_new` tinyint(1) NOT NULL DEFAULT '0',
  `special_handle` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(1) NOT NULL DEFAULT '1',
       PRIMARY KEY(`rma_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `ecs_erp_rma_action_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `rma_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `create_time` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY(`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `ecs_erp_rma_stock_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `rma_id` int(11) NOT NULL,
  `step` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `goods_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `time` int(11) NOT NULL,
    PRIMARY KEY(`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '187', 'rma', '');
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '189', 'menu_rma', '');

ALTER TABLE `ecs_erp_rma_stock_log` CHANGE `goods_id` `goods_id` INT(11) NULL;
ALTER TABLE `ecs_erp_rma_stock_log` CHANGE `warehouse_id` `warehouse_id` INT(11) NULL;
ALTER TABLE `ecs_erp_rma_stock_log` CHANGE `qty` `qty` INT(11) NULL;
