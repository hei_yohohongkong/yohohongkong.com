CREATE TABLE `ecs_program_organization` (
  `organization_id`     INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `organization_name`   VARCHAR(255) NOT NULL DEFAULT '',
  `organization_domain` VARCHAR(255) NOT NULL DEFAULT '',
  `organization_email`  VARCHAR(255) NOT NULL DEFAULT '',
  `organization_info`   TEXT         NOT NULL,
  `organization_type`   INT UNSIGNED NOT NULL DEFAULT '0',
  `create_at`           INT          NOT NULL DEFAULT '0',
  `enabled`             INT(1)       NOT NULL DEFAULT '1',
  PRIMARY KEY (`organization_id`),
  INDEX (`organization_type`)
)
  ENGINE = InnoDB;

ALTER TABLE `ecs_user_rank_program_application`
  ADD `verify_date` INT UNSIGNED NOT NULL DEFAULT '0',
  ADD `verify_mail` VARCHAR(255) NOT NULL DEFAULT '',
  ADD `verify_id` INT UNSIGNED NOT NULL DEFAULT '0',
  ADD `organization_id` INT UNSIGNED NOT NULL DEFAULT '0';
ALTER TABLE `ecs_user_rank_program`
  ADD `organization_ids` TEXT NOT NULL;