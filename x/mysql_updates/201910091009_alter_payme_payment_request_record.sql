ALTER TABLE ecs_payme_payment_request_record ADD gateway_transaction_id varchar(100) NULL;
ALTER TABLE ecs_payme_payment_request_record ADD gateway_refund_id varchar(100) NULL;
ALTER TABLE ecs_payme_payment_request_record ADD gateway_payer_id varchar(100) NULL;