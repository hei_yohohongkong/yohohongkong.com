ALTER TABLE `ecs_admin_action` ADD `parent_cat` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `relevance`;
ALTER TABLE `ecs_admin_action` ADD `remarks` VARCHAR(255) NULL DEFAULT NULL AFTER `parent_cat`;
