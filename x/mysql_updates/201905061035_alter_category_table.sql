/**
 * Author:  Ken
 * Created: 2019-05-06
 * Sql for add ad category
 */

ALTER TABLE `ecs_ad` ADD `cat` VARCHAR(255) NULL AFTER `end_time`;

ALTER TABLE `ecs_ad` CHANGE `position_id` `position_id` VARCHAR(255) NULL DEFAULT '0';