/**
 * Author:  Dem
 * Created: 2018-10-08
 * Sql for fraud orders and users
 */

ALTER TABLE `ecs_users` ADD `is_fraud` INT NOT NULL DEFAULT 0;
ALTER TABLE `ecs_order_info` ADD `is_hold` INT NOT NULL DEFAULT 0;
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '6', 'order_unhold', '');