/**
 * Author:  Dem
 * Created: 2018-01-01
 * Alter table for storing accounting month of transaction
 */
 
ALTER TABLE `actg_account_transactions` ADD (`expense_year` INT(4) DEFAULT NULL, `expense_month` INT(2) DEFAULT NULL);