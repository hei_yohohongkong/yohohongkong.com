ALTER TABLE `ecs_delivery_order` ADD `collected_time` INT(10) NULL AFTER `is_fls`;
ALTER TABLE `ecs_delivery_order` ADD `notified` BOOLEAN NULL DEFAULT FALSE AFTER `collected_time`;