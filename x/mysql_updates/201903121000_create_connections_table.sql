CREATE TABLE IF NOT EXISTS `ecs_connections`
(
  `ip`           char(15) NOT NULL,
  `create_at`   INT(10) UNSIGNED,
  `count`       INT(10) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`ip`),
  KEY `ip_create_at` (`ip`, `create_at`)
) DEFAULT CHARSET = utf8;

CREATE TABLE IF NOT EXISTS `ecs_jail`
(
  `ip`           char(15) NOT NULL,
  `create_at`   INT(10) UNSIGNED,
  `remark`   TEXT DEFAULT '',
  PRIMARY KEY (`ip`)
) DEFAULT CHARSET = utf8;

CREATE TABLE IF NOT EXISTS `ecs_whitelist`
(
  `ip`           char(15) NOT NULL,
  `remark`   TEXT DEFAULT '',
  PRIMARY KEY (`ip`)
) DEFAULT CHARSET = utf8;

INSERT INTO `ecs_shop_config` (`parent_id`, `code`, `type`, `store_dir`, `sort_order`,`value`)
VALUES ('6', 'open_jail', 'hidden', '', '1','0');
INSERT INTO `ecs_shop_config` (`parent_id`, `code`, `type`, `store_dir`, `sort_order`,`value`)
VALUES ('6', 'session_limit', 'hidden', '', '1','5000');
INSERT INTO `ecs_shop_config` (`parent_id`, `code`, `type`, `store_dir`, `sort_order`,`value`)
VALUES ('6', 'connection_limit', 'hidden', '', '1','9');