/**
 * Author:  Eric
 * Created: 2018-06-25
 * Sql for tiny image compression
 */
INSERT INTO `ecs_shop_config` (`id`, `parent_id`, `code`, `type`, `store_range`, `store_dir`, `value`, `sort_order`) VALUES (NULL, '1', 'tiny_api_key_1', 'text', '', '', '', '1');
INSERT INTO `ecs_shop_config` (`id`, `parent_id`, `code`, `type`, `store_range`, `store_dir`, `value`, `sort_order`) VALUES (NULL, '1', 'tiny_api_key_2', 'text', '', '', '', '1');
INSERT INTO `ecs_shop_config` (`id`, `parent_id`, `code`, `type`, `store_range`, `store_dir`, `value`, `sort_order`) VALUES (NULL, '1', 'tiny_limit', 'text', '', '', '500', '1');
INSERT INTO `ecs_shop_config` (`id`, `parent_id`, `code`, `type`, `store_range`, `store_dir`, `value`, `sort_order`) VALUES (NULL, '1', 'tiny_enable', 'select', '1,0', '', '1', '1');

CREATE TABLE `ecs_tiny_usages` (
  `tiny_usage_id` int(11) NOT NULL AUTO_INCREMENT,
  `used_total` int(11) NOT NULL DEFAULT '0',
  `year` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `month` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `api_key` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
   PRIMARY KEY(`tiny_usage_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;