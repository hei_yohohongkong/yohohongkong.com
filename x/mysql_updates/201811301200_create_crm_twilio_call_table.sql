/**
 * Author:  Dem
 * Created: 2018-11-30
 * Sql for crm twilio call
 */

 CREATE TABLE `ecs_crm_twilio_call` (
     `log_id` INT NOT NULL AUTO_INCREMENT,
     `list_id` INT NOT NULL,
     `duration` INT DEFAULT 0,
     PRIMARY KEY (`log_id`),
     INDEX `list_id` (`list_id` DESC)
 );