ALTER TABLE ecs_refund_requests ADD refund_payment_method_name varchar(30) NULL;
ALTER TABLE ecs_refund_requests CHANGE refund_payment_method_name refund_payment_method_name varchar(30) NULL AFTER order_payment_transaction_ids;
ALTER TABLE ecs_refund_requests ADD refund_payment_method_id int(10) unsigned NULL;
ALTER TABLE ecs_refund_requests CHANGE refund_payment_method_id refund_payment_method_id int(10) unsigned NULL AFTER order_payment_transaction_ids;
