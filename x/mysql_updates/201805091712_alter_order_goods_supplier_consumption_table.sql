ALTER TABLE `ecs_order_goods_supplier_consumption` CHANGE `order_goods_id` `order_goods_id` MEDIUMINT(8) UNSIGNED NOT NULL;
ALTER TABLE `ecs_order_goods_supplier_consumption` CHANGE `goods_id` `goods_id` MEDIUMINT(8) UNSIGNED NOT NULL;
ALTER TABLE `ecs_order_goods_supplier_consumption` CHANGE `supplier_id` `supplier_id` MEDIUMINT(8) UNSIGNED NOT NULL;