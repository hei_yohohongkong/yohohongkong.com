/**
 * Author:  Dem
 * Created: 2017-10-11
 * Add shipping logo column
 */
 
 ALTER TABLE `ecs_shipping` ADD `shipping_logo` VARCHAR(255) NOT NULL DEFAULT '';