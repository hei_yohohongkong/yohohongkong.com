/**
 * Author:  Dem
 * Created: 2018-05-23
 * Sql for adding slow sold product cost 60 and 90 day
 */

 ALTER TABLE `ecs_stock_cost_log` CHANGE `slowsoldcost` `slowsoldcost_30` DECIMAL(30,2) NOT NULL DEFAULT '0.00';
 ALTER TABLE `ecs_stock_cost_log` ADD `slowsoldcost_60` DECIMAL(30,2) NOT NULL DEFAULT '0.00', ADD `slowsoldcost_90` DECIMAL(30,2) NOT NULL DEFAULT '0.00';