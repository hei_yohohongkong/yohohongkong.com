ALTER TABLE `ecs_goods` ADD `pre_sale_time` INT(10) NOT NULL DEFAULT 0;
ALTER TABLE `ecs_goods_log` ADD `pre_sale_time` INT(10) NOT NULL DEFAULT 0;
ALTER TABLE `ecs_goods` ADD `pre_sale_string` INT(1) NOT NULL DEFAULT 0;
ALTER TABLE `ecs_goods_log` ADD `pre_sale_string` INT(1) NOT NULL DEFAULT 0;