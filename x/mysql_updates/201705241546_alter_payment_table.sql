/**
 * @Date:   2017-05-24T15:47:58+08:00
 * @Filename: 201705241546_alter_payment_table.sql
 * @Last modified time: 2017-05-24T18:16:52+08:00
 */

ALTER TABLE `ecs_payment` ADD COLUMN `display_name` varchar(120) NOT NULL DEFAULT '';

ALTER TABLE `ecs_payment_lang` ADD COLUMN `display_name` varchar(120) NULL;
