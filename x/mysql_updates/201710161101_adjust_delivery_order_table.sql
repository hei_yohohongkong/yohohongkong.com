# update delivery_order invoice_no : 宅急便, logistics_id = 1
#select * from ecs_delivery_order where invoice_no like "%宅急便%"
#select * from ecs_delivery_order where invoice_no like "%tqb%"
update ecs_delivery_order
   set invoice_no = REPLACE(invoice_no, '宅急便', ''),
        logistics_id = 1,
 where invoice_no like '%宅急便%';
update ecs_delivery_order
   set invoice_no = REPLACE(invoice_no, 'tqb', ''),
        logistics_id = 1,
 where invoice_no like '%tqb%';

# update delivery_order invoice_no : 順豐到+ , logistics_id = 3
#select * from ecs_delivery_order where invoice_no like "%SF2%"
#select * from ecs_delivery_order where invoice_no like "%順豐到家%"
update ecs_delivery_order
   set invoice_no = REPLACE(invoice_no, 'SF2', '2'),
        logistics_id = 3,
 where invoice_no like '%SF2%';
update ecs_delivery_order
   set invoice_no = REPLACE(invoice_no, '順豐到家 ', ''),
        logistics_id = 3,
 where invoice_no like '%順豐到家%';

# update delivery_order invoice_no : 順豐, logistics_id = 2
#select * from ecs_delivery_order where invoice_no like "%順豐%"
#select * from ecs_delivery_order where invoice_no like "%sf %"
update ecs_delivery_order
   set invoice_no = REPLACE(invoice_no, '順豐', ''),
        logistics_id = 2,
 where invoice_no like '%順豐%';
update ecs_delivery_order
   set invoice_no = REPLACE(invoice_no, 'sf ', ''),
        logistics_id = 2,
 where invoice_no like '%sf %';

# update delivery_order invoice_no : EMS HK, logistics_id = 4
#select * from ecs_delivery_order where invoice_no like "%ems%" AND invoice_no like "%HK%"
update ecs_delivery_order
   set invoice_no = REPLACE(invoice_no, 'EMS', ''),
        logistics_id = 4,
 where invoice_no like "%ems%" AND invoice_no like "%HK%";

# update delivery_order invoice_no : EMS SG, logistics_id = 5
#select * from ecs_delivery_order where invoice_no like "%ems%" AND invoice_no like "%SG%"
#38086
update ecs_delivery_order
   set invoice_no = REPLACE(invoice_no, 'EMS', ''),
        logistics_id = 5,
 where invoice_no like "%ems%" AND invoice_no like "%SG%";

# update delivery_order invoice_no : FedEx, logistics_id = 6
#select * from ecs_delivery_order where invoice_no like "%fedex%"
update ecs_delivery_order
   set invoice_no = REPLACE(invoice_no, 'fedex', ''),
        logistics_id = 6,
 where invoice_no like "%fedex%";

# update delivery_order invoice_no : 門店自取, logistics_id = 7
#select * from ecs_delivery_order where invoice_no like "%門%"
update ecs_delivery_order
   set invoice_no = '',
        logistics_id = 7,
 where invoice_no like "%門%";

# update delivery_order invoice_no : 代理送貨, logistics_id = 8
#select * from ecs_delivery_order where invoice_no like "%代理%"
update ecs_delivery_order
   set invoice_no = '',
        logistics_id = 8,
 where invoice_no like "%代理%";

# update delivery_order invoice_no : 司機送貨, logistics_id = 9
#select * from ecs_delivery_order where invoice_no like "%司機送貨%"
#13259
update ecs_delivery_order
   set invoice_no = '',
        logistics_id = 9,
 where invoice_no like "%司機%";

# update delivery_order invoice_no : 2 or more invoice no or have date
#select * from ecs_delivery_order where invoice_no like "%/%"
# This will Manual adjustment by Anthony

