INSERT INTO `ecs_erp_warehousing_style` (`warehousing_style`, `is_valid`) VALUES ('維修單-入暫存庫',TRUE);
INSERT INTO `ecs_erp_delivery_style` (`delivery_style`, `is_valid`) VALUES ('維修單-送修出庫',TRUE),('維修單-完成處理',TRUE);

ALTER TABLE `ecs_repair_orders` 
ADD COLUMN `repair_order_status` INT(1) NOT NULL DEFAULT 0 AFTER `is_deleted`,
ADD INDEX `status` (`repair_sn` ASC, `order_id` ASC, `repair_order_status` ASC);

ALTER TABLE `ecs_erp_stock_transfer` 
ADD COLUMN `remark` VARCHAR(45) NULL AFTER `agency_id`;