/**
 * Author:  Dem
 * Created: 2018-08-01
 * Sql for auto pricing
 */

DROP TABLE `ecs_auto_pricing_suggestion`;
CREATE TABLE `ecs_auto_pricing_suggestion` (
    `suggest_id` INT NOT NULL AUTO_INCREMENT,
    `goods_id` INT NOT NULL,
    `strategy_id` INT NOT NULL,
    `suggest_type` INT NOT NULL,
    `suggest_price` DECIMAL(10, 2) NOT NULL,
    `suggest_duration` INT NOT NULL,
    `suggest_date` DATETIME DEFAULT CURRENT_TIMESTAMP,
    `apply_date` DATETIME DEFAULT NULL,
    `status` INT DEFAULT 0,
    PRIMARY KEY(`suggest_id`),
    INDEX `goods_id` (`goods_id`),
    INDEX `goods_id_2` (`goods_id`,`suggest_type`)
);

ALTER TABLE `ecs_auto_pricing_strategy` DROP `type`;
ALTER TABLE `ecs_goods` DROP `price_strategy`;
ALTER TABLE `ecs_goods_log` DROP `price_strategy`;
ALTER TABLE `ecs_goods` ADD `price_strategy` INT DEFAULT 0;
ALTER TABLE `ecs_goods_log` ADD `price_strategy` INT DEFAULT 0;
