/**
 * Author:  Dem
 * Created: 2019-04-02
 * Sql for accounting system rework - PO revise
 */

ALTER TABLE `ecs_erp_order` ADD `revise_id` INT DEFAULT 0;

CREATE TABLE `ecs_erp_order_revise` (
   `revise_id` INT NOT NULL AUTO_INCREMENT,
   `order_id` INT NOT NULL,
   `create_at` DATETIME DEFAULT CURRENT_TIMESTAMP,
   `create_by` INT DEFAULT 0,
   `update_at` DATETIME,
   `update_by` INT DEFAULT 0,
   `approve_at` DATETIME,
   `approve_by` INT DEFAULT 0,
   `status` INT DEFAULT 0,
   `remark` TEXT,
   PRIMARY KEY (`revise_id`)
);

CREATE TABLE `ecs_erp_order_revise_item` (
   `revise_item_id` INT NOT NULL AUTO_INCREMENT,
   `revise_id` INT NOT NULL,
   `order_item_id` INT NOT NULL,
   `goods_id` INT NOT NULL,
   `old_price` DECIMAL(15,2),
   `new_price` DECIMAL(15,2),
   `old_shipping_price` DECIMAL(15,2),
   `new_shipping_price` DECIMAL(15,2),
   `old_order_qty` INT,
   `new_order_qty` INT,
   PRIMARY KEY (`revise_item_id`)
);