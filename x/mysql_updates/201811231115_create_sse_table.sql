/**
 * Author:  Dem
 * Created: 2018-11-23
 * Sql for crm sse
 */

 CREATE TABLE `ecs_sse` (
     `sse_id` INT NOT NULL AUTO_INCREMENT,
     `time` INT NOT NULL,
     `target` TEXT,
     `event` TEXT,
     `data` TEXT,
     PRIMARY KEY (`sse_id`),
     INDEX `time` (`time` DESC)
 );