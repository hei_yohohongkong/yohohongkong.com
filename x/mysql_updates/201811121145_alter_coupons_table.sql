/**
 * Author:  Dem
 * Created: 2018-11-12
 * Sql for promote coupon
 */

 ALTER TABLE `ecs_coupons` ADD `promote_id` INT DEFAULT 0;
 CREATE TABLE `ecs_coupon_promote` (
     `promote_id` INT NOT NULL AUTO_INCREMENT,
     `promote_name` VARCHAR (255) NOT NULL,
     PRIMARY KEY (`promote_id`)
 );
 CREATE TABLE `ecs_coupon_promote_info` (
     `rec_id` INT NOT NULL AUTO_INCREMENT,
     `order_id` INT NOT NULL,
     `promote_id` INT NOT NULL,
     `user_id` INT NOT NULL,
     `data` TEXT,
     PRIMARY KEY (`rec_id`)
 );
 INSERT INTO `ecs_coupon_promote` (`promote_name`) VALUE ("Yoho x The Club");