CREATE TABLE `ecs_target_log`
(
    `target_log_id` int UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    `platform` varchar(255) DEFAULT "",
    `data` LONGTEXT,
    `year` varchar(255) DEFAULT "",
    `month` varchar(255) DEFAULT "",
    `start_time` INT DEFAULT "0",
    `end_time` INT DEFAULT "0",
    INDEX (`target_log_id`),
    INDEX (`platform`)
);