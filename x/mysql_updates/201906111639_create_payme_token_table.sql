-- CREATE TABLE "ecs_payme_token" ------------------------------
CREATE TABLE `ecs_payme_token` ( 
	`ecs_payme_token_id` Int( 255 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`token` LongText NOT NULL,
	`expiry_datetime` Int( 100 ) NULL,
	`raw_text` Text NOT NULL,
	PRIMARY KEY ( `ecs_payme_token_id` ),
	CONSTRAINT `unique_ecs_payme_token_id` UNIQUE( `ecs_payme_token_id` ) )
ENGINE = InnoDB;
-- -------------------------------------------------------------

-- CREATE FIELD "user_id" --------------------------------------
ALTER TABLE `ecs_payme_token` ADD COLUMN `user_id` Int( 10 ) NOT NULL;
-- -------------------------------------------------------------
