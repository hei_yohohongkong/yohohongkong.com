ALTER TABLE `ecs_admin_taskes` ADD COLUMN `tmp_old_status` TEXT NULL AFTER `status`;
UPDATE `ecs_admin_taskes` SET tmp_old_status=status;

ALTER TABLE `ecs_admin_taskes` CHANGE COLUMN `status` `status` INT NOT NULL DEFAULT 0 ;



#TaskController::STATUS_NEW
UPDATE `ecs_admin_taskes` SET status=0 WHERE tmp_old_status='New';
#TaskController::STATUS_INPROGRESS
UPDATE `ecs_admin_taskes` SET status=1 WHERE tmp_old_status='In Progress';
#TaskController::STATUS_RESOLVED
UPDATE `ecs_admin_taskes` SET status=5 WHERE tmp_old_status='Finished';
#TaskController::STATUS_ARCHIVED
UPDATE `ecs_admin_taskes` SET status=6 WHERE deleted=TRUE;

#Change pic to receiver_id!
#Precondition: created sales person user(admin_id updated as admin but not the "parent id")
UPDATE ecs_admin_taskes t JOIN ecs_salesperson sp ON sp.sales_name=t.pic SET t.receiver_id=sp.admin_id;
#Update the pic name after binding with admin_id
UPDATE ecs_admin_taskes t JOIN ecs_admin_user u ON t.receiver_id=u.user_id SET t.pic=u.user_name;

ALTER TABLE `ecs_admin_taskes` DROP COLUMN `tmp_old_status`;