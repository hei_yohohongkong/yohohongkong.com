ALTER TABLE `ecs_admin_user` 
ADD COLUMN `avatar` TEXT NULL AFTER `role_id`,
ADD COLUMN `first_name` VARCHAR(255) NULL AFTER `avatar`,
ADD COLUMN `last_name` VARCHAR(255) NULL AFTER `first_name`,
ADD COLUMN `joined_date` DATE NULL AFTER `last_name`,
ADD COLUMN `emergency_contact` TEXT NULL AFTER `joined_date`,
ADD COLUMN `address` TEXT NULL AFTER `emergency_contact`,
ADD COLUMN `mobile` VARCHAR(45) NULL AFTER `address`,
ADD COLUMN `id_number` VARCHAR(45) NULL AFTER `mobile`,
ADD COLUMN `dob` DATE NULL AFTER `id_number`,
ADD COLUMN `position` VARCHAR(255) NULL AFTER `dob`,
ADD COLUMN `is_receive_stock_alert` TINYINT NULL DEFAULT 0 AFTER `position`,
ADD COLUMN `timestamp` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `is_receive_stock_alert`;
