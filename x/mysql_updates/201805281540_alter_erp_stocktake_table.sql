/**
 * Author:  Eric
 * Created: 2018-05-31
 * Sql for stocktake
 */

ALTER TABLE `ecs_erp_goods_stocktake_item` ADD `local_pick_up_stock` INT(11) NULL AFTER `stocktake_end_time`;

ALTER TABLE `ecs_erp_goods_stocktake` ADD `review_finished_rate` INT NOT NULL DEFAULT '0' AFTER `stocktake_finished_count`;

ALTER TABLE `ecs_erp_goods_stocktake` CHANGE `stocktake_finished_count` `stocktake_finished_rate` INT(11) NOT NULL DEFAULT '0';