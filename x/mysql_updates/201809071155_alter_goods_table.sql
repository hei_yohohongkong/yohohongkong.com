/**
 * Author:  Dem
 * Created: 2018-09-07
 * Sql for auto apply auto price suggestion
 */

ALTER TABLE `ecs_goods` ADD `auto_pricing` INT NOT NULL DEFAULT 0;
ALTER TABLE `ecs_goods_log` ADD `auto_pricing` INT NOT NULL DEFAULT 0;