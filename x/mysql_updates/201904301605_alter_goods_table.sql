/**
 * Author:  Dem
 * Created: 2019-04-25
 * Sql for permanent link
 */

 ALTER TABLE `ecs_goods_log` ADD `perma_link` TEXT;
 ALTER TABLE `ecs_tag_lang` ADD PRIMARY KEY (`tag_id`, `lang`);
 INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '412', 'edit_perma_link', '');
 INSERT INTO `ecs_shop_config` (`parent_id`, `code`, `type`, `value`) VALUES ('6', 'perma_link_cache', 'hidden', '0');
