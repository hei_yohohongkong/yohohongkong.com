CREATE TABLE `ecs_verify_code` (
    `verify_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `verify_code` VARCHAR(255) NOT NULL DEFAULT '',
    `verify_type` VARCHAR(255) NOT NULL DEFAULT '',
    `verify_target` VARCHAR(255) NOT NULL DEFAULT '',
    `user_id` INT UNSIGNED NOT NULL DEFAULT '0',
    `status` INT NOT NULL DEFAULT '0',
    `create_at` INT UNSIGNED NOT NULL DEFAULT '0',
    PRIMARY KEY (`verify_id`),
    INDEX (`user_id`)
) ENGINE = InnoDB;