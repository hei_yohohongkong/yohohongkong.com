/**
 * Author:  Sing
 * Created: 2019-04-25
 * Sql for new shipping - ZTO
 */

INSERT INTO `ecs_shipping` (`shipping_id`, `shipping_code`, `shipping_name`, `shipping_desc`, `insure`, `support_cod`, `enabled`, `shipping_print`, `print_bg`, `config_lable`, `print_model`, `shipping_order`, `support_fod`, `shipping_logo`, `is_pos`, `shipping_type_id`) VALUES (NULL, 'ztopickup', '中通智能櫃', '中通智能櫃', '0', '0', '0', '', NULL, NULL, '2', '0', '0', '', '0', '2');

INSERT INTO `ecs_shipping` (`shipping_id`, `shipping_code`, `shipping_name`, `shipping_desc`, `insure`, `support_cod`, `enabled`, `shipping_print`, `print_bg`, `config_lable`, `print_model`, `shipping_order`, `support_fod`, `shipping_logo`, `is_pos`, `shipping_type_id`) VALUES (NULL, 'ztopickuppoint', '其他自提點', '其他自提點', '0', '0', '0', '', NULL, NULL, '2', '0', '0', 'ztopickuppoint_shipping_logo.svg', '0', '2');

INSERT INTO `ecs_shipping` (`shipping_id`, `shipping_code`, `shipping_name`, `shipping_desc`, `insure`, `support_cod`, `enabled`, `shipping_print`, `print_bg`, `config_lable`, `print_model`, `shipping_order`, `support_fod`, `shipping_logo`, `is_pos`, `shipping_type_id`) VALUES (NULL, 'postoffice', '郵政局', '郵政局', '0', '0', '0', '', '', '', '2', '0', '0', 'postoffice_shipping_logo.svg', '0', '2');

INSERT INTO `ecs_logistics` (`logistics_id`, `logistics_code`, `logistics_name`, `logistics_desc`, `enabled`, `created_at`, `updated_at`, `logistics_url`, `logistics_logo`, `invoice_template`) VALUES (NULL, 'ztopickup', '中通', '', '1', '2019-04-23 12:42:45', '2019-04-25 11:08:37', '', '', '');
