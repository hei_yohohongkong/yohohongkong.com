/**
 * Author:  Dem
 * Created: 2019-02-25
 * Sql for 313 phone number event
 */

CREATE TABLE `ecs_phone_number_log` (
    `log_id` INT NOT NULL AUTO_INCREMENT,
    `phone` VARCHAR(13) NOT NULL,
    `ip` VARCHAR(15),
    `user_id` INT NOT NULL,
    `create_time` INT(10),
    `status` INT(1) DEFAULT 0,
    PRIMARY KEY (`log_id`)
);

INSERT INTO `ecs_shop_config` (`parent_id`, `code`, `type`, `value`) VALUES ('6', 'phonenumber_config', 'hidden', '{"coupons":{"1":{"coupon_id":"939","limit":"5000"},"2":{"coupon_id":"940","limit":"3000"},"3":{"coupon_id":"941","limit":"2000"},"4":{"coupon_id":"942","limit":"50"},"5":{"coupon_id":"943","limit":"10"},"6":{"coupon_id":"944","limit":"5"}},"start_time":"2019-03-01 00:00:00","end_time":"2019-04-01 00:00:00"}');