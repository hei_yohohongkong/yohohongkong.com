-- CREATE FIELD "payme_only" -----------------------------------
ALTER TABLE `ecs_flashdeals` ADD COLUMN `payme_only` TinyInt( 1 ) NOT NULL DEFAULT 0 AFTER `is_online_pay`;
-- -------------------------------------------------------------

-- CREATE FIELD "alipay_only" ----------------------------------
ALTER TABLE `ecs_flashdeals` ADD COLUMN `alipay_only` TinyInt( 1 ) NOT NULL DEFAULT 0 AFTER `is_online_pay`;
-- -------------------------------------------------------------
