/**
 * Author:  Dem
 * Created: 2019-01-25
 * Sql for account logging (rank_point / pay_point)
 */

 ALTER TABLE `ecs_account_log` ADD `admin_id` INT DEFAULT 0;
 INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '189', 'menu_account_log', '');