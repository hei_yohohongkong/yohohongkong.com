ALTER TABLE `ecs_order_progress` ADD `first_all_in_stock_time` INT(10) NULL AFTER `create_date`;
ALTER TABLE `ecs_order_progress` ADD `stock_status_history` TEXT NULL AFTER `first_all_in_stock_time`;
ALTER TABLE `ecs_order_progress` ADD `is_all_in_stock` BOOLEAN NOT NULL DEFAULT FALSE AFTER `stock_status_history`;
ALTER TABLE `ecs_order_progress` ADD `warehouse_handling_time` INT(10) NULL AFTER `is_all_in_stock`;