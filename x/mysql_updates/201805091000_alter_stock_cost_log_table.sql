/**
 * Author:  Dem
 * Created: 2018-05-09
 * Sql for adding slow sold product cost
 */

 ALTER TABLE `ecs_stock_cost_log` ADD `slowsoldcost` DECIMAL(30,2) NOT NULL DEFAULT '0.00';