CREATE TABLE `ecs_order_picker_packer` (
  `order_picker_packer_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_sn` varchar(20) CHARACTER SET utf8 NOT NULL,
  `picker_user_ids` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `packer_user_ids` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `date` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `update_time` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY(`order_picker_packer_id`)
)
