/**
 * Author:  Dem
 * Created: 2018-02-27
 * Alter table for online payment and refund
 */

ALTER TABLE `ecs_payment` ADD COLUMN `is_online_pay` INT DEFAULT 0;
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '6', 'order_pay', '');
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '6', 'order_cancel', '');
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '6', 'order_refund', '');
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '6', 'order_unpay', '');
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '6', 'order_remove', '');
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '6', 'order_after_service', '');
INSERT INTO `ecs_shop_config` (`parent_id`, `code`, `type`, `store_dir`, `sort_order`,`value`) VALUES ('6', 'default_refund_account', 'hidden', '', '1','5');
ALTER TABLE `ecs_refund_requests` ADD COLUMN `requester_id` INT DEFAULT 0, ADD COLUMN `operator_id` INT DEFAULT 0, ADD COLUMN `refund_type` INT DEFAULT 0;
UPDATE `ecs_refund_requests` SET `status` = 1 WHERE `status` = 'pending';
UPDATE `ecs_refund_requests` SET `status` = 2 WHERE `status` = 'processing';
UPDATE `ecs_refund_requests` SET `status` = 3 WHERE `status` = 'finished';
UPDATE `ecs_refund_requests` SET `status` = 4 WHERE `status` = 'rejected';
ALTER TABLE `ecs_refund_requests` CHANGE `status` `status` INT(1) DEFAULT 1;