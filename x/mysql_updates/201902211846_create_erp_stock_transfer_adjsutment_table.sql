CREATE TABLE `ecs_erp_stock_transfer_adjustment` (
  `adjustment_id` int(11) NOT NULL AUTO_INCREMENT, 
  `transfer_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `goods_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `remark` text COLLATE utf8_unicode_ci,
  `create_date` int(10) NOT NULL,
  `admin_from_confirm` int(11) NOT NULL,
  `admin_to_confirm` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `ref_no` int(11) DEFAULT NULL,
  PRIMARY KEY (`adjustment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;