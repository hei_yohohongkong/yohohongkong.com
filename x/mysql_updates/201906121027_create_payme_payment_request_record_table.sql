-- CREATE TABLE "ecs_payme_payment_request_record" -------------
CREATE TABLE `ecs_payme_payment_request_record` ( 
	`payme_payment_request_record_id` Int( 20 ) AUTO_INCREMENT NOT NULL,
	`order_id` Int( 20 ) NOT NULL,
	`user_id` Int( 20 ) NOT NULL,
	`log_id` Int( 20 ) NOT NULL,
	`payment_trace_id` VarChar( 50 ) NOT NULL,
	`out_trade_no` VarChar( 100 ) NOT NULL,
	`gateway_payment_request_id` VarChar( 50 ) NOT NULL,
	`gateway_total_amount` Decimal( 10, 2 ) NOT NULL,
	`gateway_currency_code` VarChar( 3 ) NULL,
	`gateway_payment_request_creation_datetime` Int( 20 ) NOT NULL,
	`gateway_payment_request_duration` Int( 4 ) NOT NULL,
	`gateway_payment_request_web_link` VarChar( 255 ) NOT NULL,
	`gateway_payment_request_status_description` VarChar( 255 ) NULL,
	`gateway_payment_request_status_code` VarChar( 10 ) NOT NULL,
	`request_raw_text` Text NULL,
	`response_raw_text` Text NULL,
	PRIMARY KEY ( `payme_payment_request_record_id` ),
	CONSTRAINT `unique_payme_payment_request_record_id` UNIQUE( `payme_payment_request_record_id` ) )
ENGINE = InnoDB;
-- -------------------------------------------------------------
