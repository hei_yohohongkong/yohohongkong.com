ALTER TABLE ecs_article_cat ADD cat_banner varchar(255) DEFAULT '' NOT NULL;
ALTER TABLE ecs_article ADD mobile_content TEXT;
ALTER TABLE ecs_article_lang ADD mobile_content TEXT;
ALTER TABLE ecs_topic ADD redirection int DEFAULT 0 NOT NULL;
INSERT INTO `ecs_article_cat` (`cat_name`, `cat_type`, `keywords`, `cat_desc`, `sort_order`, `show_in_nav`, `parent_id`, `cat_icon`) VALUES ('關於友和', 3, DEFAULT, DEFAULT, 1, 0, 0, '')