-- CREATE FIELD "status" -----------------------------------
ALTER TABLE `ecs_favourable_activity` ADD `status` TINYINT(1) NOT NULL DEFAULT '0' AFTER `end_time`;
ALTER TABLE `ecs_goods_activity` ADD `status` TINYINT(1) NOT NULL DEFAULT '0' AFTER `end_time`;
-- -------------------------------------------------------------

-- UPDATE PERMISSION TO "on/off" ----------------------------------
UPDATE `ecs_admin_action` SET `parent_cat` = 'mkt_method' WHERE `action_code` = 'flashdeal_events_on_off';
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`, `parent_cat`, `remarks`) VALUES (NULL, '189', 'favourable_on_off', '', 'mkt_method', NULL);
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`, `parent_cat`, `remarks`) VALUES (NULL, '189', 'package_on_off', '', 'mkt_method', NULL);
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`, `parent_cat`, `remarks`) VALUES (NULL, '189', 'flashdeal_events', '', 'mkt_method', NULL);
UPDATE `ecs_admin_action` SET `remarks` = 'hidden' WHERE `action_code` = 'gift_manage';
-- -------------------------------------------------------------
