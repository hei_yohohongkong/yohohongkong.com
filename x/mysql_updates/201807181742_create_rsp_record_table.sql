CREATE TABLE `ecs_rsp_record` (
    `rsp_id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT ,
    `create_from` VARCHAR(60) NOT NULL,
    `order_id` MEDIUMINT(8) UNSIGNED NOT NULL ,
    `order_sn` VARCHAR(20) NOT NULL ,
    `rsp_sn` VARCHAR(20) NOT NULL ,
    `address` VARCHAR(120) NOT NULL,
    `consignee` VARCHAR(60) NOT NULL ,
    `tel` VARCHAR(60) NOT NULL ,
    `call_date` INT NOT NULL ,
    `call_from` VARCHAR(60) NOT NULL ,
    `collection_date` INT NOT NULL ,
    `appointed_collection_date` INT NOT NULL ,
    `collection_name` VARCHAR(60) NOT NULL ,
    `buy_total` TEXT NOT NULL ,
    `collection_total` TEXT NOT NULL ,
    `real_collection_total` TEXT NOT NULL ,
    `status` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
    `remark` TEXT NOT NULL ,
    `create_at` INT NOT NULL ,
    PRIMARY KEY (`rsp_id`), INDEX (`order_id`)
) ENGINE = InnoDB;

INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '187', 'rsp_record', '');
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '189', 'menu_rsp_record', '');
