/**
 * Author:  Eric
 * Created: 2018-08-01
 * Sql for Lucky Draw
 */

CREATE TABLE `ecs_lucky_draw` (
  `lucky_draw_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` int(11) NOT NULL,
  `end_date` int(11) NOT NULL,
  `purchase_amount` double DEFAULT NULL,
  `show_on_menu` tinyint(1) NOT NULL DEFAULT '0',
  `game_image` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `game_image_mobile` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lucky_draw_license` text COLLATE utf8_unicode_ci,
  `hold_draw_status` tinyint(1) NOT NULL DEFAULT '0',
  `share_link_1` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `share_link_2` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
   PRIMARY KEY(`lucky_draw_id`)  
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `ecs_lucky_draw_desc` (
  `lucky_draw_desc_id` int(11) NOT NULL AUTO_INCREMENT,
  `lucky_draw_id` int(11) NOT NULL,
  `lang` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `intro` text COLLATE utf8_unicode_ci,
  `intro_mobile` text COLLATE utf8_unicode_ci,
  `finished` text COLLATE utf8_unicode_ci,
  `finished_mobile` text COLLATE utf8_unicode_ci,
  `tnc` text COLLATE utf8_unicode_ci,
  `update_date` int(10) NOT NULL,
    PRIMARY KEY(`lucky_draw_desc_id`)  
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `ecs_lucky_draw_gifts` (
  `gift_id` int(11) NOT NULL AUTO_INCREMENT,
  `lucky_draw_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `points` int(11) DEFAULT NULL,
  `goods_id` int(11) DEFAULT NULL,
  `coupon_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `must_show` tinyint(1) NOT NULL DEFAULT '0',
  `qty` int(11) DEFAULT NULL,
  `won` int(11) NOT NULL DEFAULT '0',
  `probability_level` int(11) DEFAULT NULL,
  `image` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image2` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `create_time` int(11) NOT NULL,
  PRIMARY KEY(`gift_id`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `ecs_lucky_draw_goods_redeem` (
  `redeem_id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_id` int(11) NOT NULL,
  `gift_id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `goods_id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `notified_time` int(11) DEFAULT NULL,
  `delivered_time` int(11) DEFAULT NULL,
 PRIMARY KEY(`redeem_id`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `ecs_lucky_draw_levels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `probability` double NOT NULL,
  `level_id` int(11) NOT NULL,
  PRIMARY KEY(`id`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `ecs_lucky_draw_ticket` (
  `ticket_id` int(11) NOT NULL AUTO_INCREMENT,
  `lucky_draw_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `for_order_id` int(11) DEFAULT NULL,
  `draw_results` text COLLATE utf8_unicode_ci,
  `gift_id` int(11) DEFAULT NULL,
  `opened` tinyint(1) NOT NULL DEFAULT '0',
  `draw_results_time` int(11) DEFAULT NULL,
  `opened_time` int(11) DEFAULT NULL,
  `redeemed` tinyint(1) NOT NULL DEFAULT '0',
  `remark` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `follow_up_remark` text COLLATE utf8_unicode_ci,
  `notified` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `add_date` int(11) NOT NULL,
   PRIMARY KEY(`ticket_id`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `ecs_mail_templates` (`template_code`, `is_html`, `template_subject`, `template_content`, `last_modify`, `last_send`, `type`) VALUES
('lucky_draw_result_notice', 1, '抽獎結果通知', '<table style="border: 1px solid; border-color: #e7e7e7; margin-left: auto; margin-right: auto; width: 600px;" border="0">\n<tbody>\n<tr>\n<td colspan="3"><img usemap="#m_-5847510787762023062_imgmap2017929151620" class="CToWUd" src="https://yohohongkong.com/images/upload/Image/1507831950789963146.png" border="0" alt=""><map id="m_-5847510787762023062imgmap2017929151620" name="m_-5847510787762023062_imgmap2017929151620"> \n<area shape="circle" alt="" coords="477,53,13" href="https://www.facebook.com/yohohongkong" target="_blank">\n \n<area shape="circle" alt="" coords="508,54,13" href="https://www.youtube.com/yohohongkong" target="_blank">\n \n<area shape="circle" alt="" coords="540,55,13" href="https://www.instagram.com/yohohongkong/" target="_blank">\n \n<area shape="circle" alt="" coords="571,55,13" href="https://plus.google.com/+yohohongkong" target="_blank">\n \n<area shape="rect" alt="" coords="209,7,390,72" href="https://yohohongkong.com/" target="_blank">\n </map></td>\n</tr>\n<tr>\n<td colspan="3"><strong style="padding-left: 5px;">你好!</strong>\n<div style="padding: 10px 5px;">{if $type== 0}<br>恭喜你成為「818 VIP狂歡節 - 幸運大抽獎」的得獎者，你可得{$points} 積分。有關積分已自動加至你的帳户內，你可到你的個人資料頁查看。謝謝你對是次活動的支持！<br>{/if}{if $type== 1}恭喜你成為「818 VIP狂歡節 - 幸運大抽獎」的得獎者，你可得{$prize}優惠券。於購物時，你只需要輸入優惠券編號【{$coupon_code}】，即可享有優惠。謝謝你對是次活動的支持！<br><br>立即購買：{$goods_url}<br>優惠卷使用方法： https://goo.gl/G9BzuP <br><br>*優惠碼有限期至2018 年 9 月 10 日<br>*每個優惠碼只限使用一 次<br>*以上優惠不能與其他優惠同時使用 <br>*如有任何爭議，友和 Yoho 保留最終決定權<br>{/if}{if $type== 2}<br>恭喜你成為「818 VIP狂歡節 - 幸運大抽獎」的得獎者，你可得{$prize}。我們將會於活動完結後一星期內，以電郵形式通知領獎詳情。謝謝你對是次活動的支持！<br>{/if}</div>\n</td>\n</tr>\n<tr style="margin-top: 5px;">\n<td colspan="3"><span style="padding-left: 5px; color: #999999;">感謝您對友和的支持，希望您購物愉快！</span><br><br><span style="padding-left: 5px; color: #999999;">{$shop_name}</span><br><span style="padding-left: 5px; color: #999999;">{$send_date}</span></td>\n</tr>\n<tr>\n<td colspan="3">&nbsp;</td>\n</tr>\n<tr>\n<td colspan="3"><a href="https://yohohongkong.com/vip"><img src="https://yohohongkong.com/images/upload/Image/1506968087046004808.png" border="0"></a></td>\n</tr>\n<tr>\n<td colspan="3"><img usemap="#imgmap2017929165943" src="https://yohohongkong.com/images/upload/Image/1506968194128151352.png" border="0"><map id="imgmap2017929165943" name="imgmap2017929165943"> \n<area shape="rect" alt="" coords="1,1,134,55" href="https://yohohongkong.com/cat/1-%E9%9B%BB%E5%AD%90%E7%94%A2%E5%93%81" target="">\n \n<area shape="rect" alt="" coords="137,1,265,58" href="https://yohohongkong.com/cat/23-%E7%BE%8E%E5%AE%B9%E5%8F%8A%E8%AD%B7%E7%90%86" target="">\n \n<area shape="rect" alt="" coords="269,0,378,57" href="https://yohohongkong.com/cat/184-%E5%AE%B6%E5%BA%AD%E9%9B%BB%E5%99%A8" target="">\n \n<area shape="rect" alt="" coords="382,1,493,55" href="https://yohohongkong.com/cat/44-%E9%9B%BB%E8%85%A6" target="">\n \n<area shape="rect" alt="" coords="496,1,601,56" href="https://yohohongkong.com/cat/171-生活時尚" target="">\n </map></td>\n</tr>\n<tr style="background: #00acee; color: white;">\n<td colspan="3">\n<div class="footer-content" style="float: left; padding: 10px; color: white; font-size: 12px;">請勿回覆此電郵，您可以發電郵至<a style="color: #0645ad;" href="mailto:info@yohohongkong.com" target="_blank" rel="noopener noreferrer">info@yohohongkong.com</a>&nbsp;或使用<a href="https://yohohongkong.com/">yohohongkong.com</a>&nbsp;/ 致電 53356996 與我們保持聯絡。</div>\n</td>\n</tr>\n</tbody>\n</table>', 1533776210, 0, 'template');


INSERT INTO `ecs_mail_templates` (`template_code`, `is_html`, `template_subject`, `template_content`, `last_modify`, `last_send`, `type`) VALUES
('lucky_draw_gift_pickup_notice', 1, '抽獎免費禮物領取通知', '<table style="border: 1px solid; border-color: #e7e7e7; margin-left: auto; margin-right: auto; width: 600px;" border="0">\n<tbody>\n<tr>\n<td colspan="3"><img usemap="#m_-5847510787762023062_imgmap2017929151620" class="CToWUd" src="https://yohohongkong.com/images/upload/Image/1507831950789963146.png" border="0" alt=""><map id="m_-5847510787762023062imgmap2017929151620" name="m_-5847510787762023062_imgmap2017929151620"> \n<area shape="circle" alt="" coords="477,53,13" href="https://www.facebook.com/yohohongkong" target="_blank">\n \n<area shape="circle" alt="" coords="508,54,13" href="https://www.youtube.com/yohohongkong" target="_blank">\n \n<area shape="circle" alt="" coords="540,55,13" href="https://www.instagram.com/yohohongkong/" target="_blank">\n \n<area shape="circle" alt="" coords="571,55,13" href="https://plus.google.com/+yohohongkong" target="_blank">\n \n<area shape="rect" alt="" coords="209,7,390,72" href="https://yohohongkong.com/" target="_blank">\n </map></td>\n</tr>\n<tr>\n<td colspan="3"><strong style="padding-left: 5px;">你好，</strong>\n<div style="padding: 10px 5px;">恭喜你成為「818 VIP狂歡節 - 幸運大抽獎」的得獎者，你可得{$prize}。貴客請憑此電郵訊息於2018年9月3日至9月10日到友和門市拎獎。<br><br>得獎編號：{$order_sn}<br>地址： 九龍觀塘巧明街112號友聯大廈6樓<br>辦公時間： 星期一至六 12PM - 7:30PM；星期日及假期 : 11AM - 6:30PM<br><br>請於換領期限內到以上指定地點領取奬品，逾期則當棄權論。重複換領，亦告作廢。<br><br>*友和Yoho 就以上獎品換領事宜保留最終決定權<br><br></div>\n</td>\n</tr>\n<tr style="margin-top: 5px;">\n<td colspan="3"><span style="padding-left: 5px; color: #999999;">感謝您對友和的支持，希望您購物愉快！</span><br><br><span style="padding-left: 5px; color: #999999;">{$shop_name}</span><br><span style="padding-left: 5px; color: #999999;">{$send_date}</span></td>\n</tr>\n<tr>\n<td colspan="3">&nbsp;</td>\n</tr>\n<tr>\n<td colspan="3"><a href="https://yohohongkong.com/vip"><img src="https://yohohongkong.com/images/upload/Image/1506968087046004808.png" border="0"></a></td>\n</tr>\n<tr>\n<td colspan="3"><img usemap="#imgmap2017929165943" src="https://yohohongkong.com/images/upload/Image/1506968194128151352.png" border="0"><map id="imgmap2017929165943" name="imgmap2017929165943"> \n<area shape="rect" alt="" coords="1,1,134,55" href="https://yohohongkong.com/cat/1-%E9%9B%BB%E5%AD%90%E7%94%A2%E5%93%81" target="">\n \n<area shape="rect" alt="" coords="137,1,265,58" href="https://yohohongkong.com/cat/23-%E7%BE%8E%E5%AE%B9%E5%8F%8A%E8%AD%B7%E7%90%86" target="">\n \n<area shape="rect" alt="" coords="269,0,378,57" href="https://yohohongkong.com/cat/184-%E5%AE%B6%E5%BA%AD%E9%9B%BB%E5%99%A8" target="">\n \n<area shape="rect" alt="" coords="382,1,493,55" href="https://yohohongkong.com/cat/44-%E9%9B%BB%E8%85%A6" target="">\n \n<area shape="rect" alt="" coords="496,1,601,56" href="https://yohohongkong.com/cat/171-生活時尚" target="">\n </map></td>\n</tr>\n<tr style="background: #00acee; color: white;">\n<td colspan="3">\n<div class="footer-content" style="float: left; padding: 10px; color: white; font-size: 12px;">請勿回覆此電郵，您可以發電郵至<a style="color: #0645ad;" href="mailto:info@yohohongkong.com" target="_blank" rel="noopener noreferrer">info@yohohongkong.com</a>&nbsp;或使用<a href="https://yohohongkong.com/">yohohongkong.com</a>&nbsp;/ 致電 53356996 與我們保持聯絡。</div>\n</td>\n</tr>\n</tbody>\n</table>', 1533780819, 0, 'template');

INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '7', 'lucky_draw', '');
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '189', 'menu_lucky_draw', '');

ALTER TABLE `ecs_lucky_draw_ticket` ADD INDEX( `lucky_draw_id`);
