/**
 * Author:  Dem
 * Created: 2017-9-6
 * Add payment config column
 */

ALTER TABLE `ecs_order_goods` ADD `is_vip_price` TINYINT(1) UNSIGNED DEFAULT 0;