/**
 * Author:  Anthony
 * Created: 2017-4-11
 */

CREATE TABLE `c0yohobeta`.`ecs_yahoo_store`
( `yahoo_id` INT(11) NOT NULL AUTO_INCREMENT , `goods_id` INT(11) NOT NULL ,
`yahoo_brand_id` INT(11) NOT NULL , `yahoo_quantity` INT(11) NOT NULL ,
`yahoo_price` DECIMAL(10,2) NOT NULL , `creator` VARCHAR(60) NOT NULL ,
`created_at` INT(11) NOT NULL , `yahoo_detail` TEXT NOT NULL ,
PRIMARY KEY (`yahoo_id`)) ENGINE = InnoDB;
