-- CREATE FIELD "corresponding_account" ------------------------
ALTER TABLE `ecs_erp_supplier` ADD COLUMN `corresponding_company` VarChar( 100 ) NULL;
-- -------------------------------------------------------------

-- CHANGE "NAME" OF "FIELD "corresponding_company_id" ----------
ALTER TABLE `ecs_erp_supplier` CHANGE `corresponding_company` `corresponding_company_id` Int( 10 ) NULL;
-- -------------------------------------------------------------

-- CHANGE "LENGTH, NAME, TYPE" OF "FIELD "corresponding_company" 
ALTER TABLE `ecs_erp_supplier` MODIFY `corresponding_company_id` Int( 10 ) NULL;
-- -------------------------------------------------------------