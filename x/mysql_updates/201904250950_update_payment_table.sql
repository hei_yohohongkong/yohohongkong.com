/**
 * Author:  Dem
 * Created: 2019-04-25
 * Sql for multi-language
 */

 UPDATE `ecs_payment_lang` SET `pay_config` = '' WHERE `pay_id` IN (5,9,18,19,21);

-- bank
 UPDATE `ecs_payment` SET `pay_config` = 'a:6:{i:0;a:3:{s:4:"name";s:10:"desc_zh_tw";s:4:"type";s:8:"textarea";s:5:"value";s:138:"轉帳後請上傳收據，以便核實您的付款。你可以隨時登入帳戶在［我的訂單］按［付款］找回上傳連結。";}i:1;a:3:{s:4:"name";s:10:"desc_zh_cn";s:4:"type";s:8:"textarea";s:5:"value";s:138:"转帐后请上传收据，以便核实您的付款。你可以随时登入帐户在［我的订单］按［付款］找回上传连结。";}i:2;a:3:{s:4:"name";s:10:"desc_en_us";s:4:"type";s:8:"textarea";s:5:"value";s:70:"To verify your payment, please upload your receipt after the transfer.";}i:3;a:3:{s:4:"name";s:9:"bank_user";s:4:"type";s:4:"text";s:5:"value";s:22:"Yoho Hong Kong Limited";}i:4;a:3:{s:4:"name";s:5:"banks";s:4:"type";s:6:"hidden";s:5:"value";a:3:{i:0;a:2:{s:4:"name";s:12:"滙豐銀行";s:7:"account";s:14:"456-348739-838";}i:1;a:2:{s:4:"name";s:12:"恒生銀行";s:7:"account";s:14:"799-041793-883";}i:2;a:2:{s:4:"name";s:12:"中國銀行";s:7:"account";s:18:"012-887-0-019977-9";}}}i:5;a:3:{s:4:"name";s:8:"tutorial";s:4:"type";s:6:"hidden";s:5:"value";s:138:"轉帳後請上傳收據，以便核實您的付款。你可以隨時登入帳戶在［我的訂單］按［付款］找回上傳連結。";}}' WHERE `pay_code` = 'bank';
 
-- ebanking
 UPDATE `ecs_payment` SET `pay_config` = 'a:6:{i:0;a:3:{s:4:"name";s:10:"desc_zh_tw";s:4:"type";s:8:"textarea";s:5:"value";s:138:"轉帳後請上傳收據，以便核實您的付款。你可以隨時登入帳戶在［我的訂單］按［付款］找回上傳連結。";}i:1;a:3:{s:4:"name";s:10:"desc_zh_cn";s:4:"type";s:8:"textarea";s:5:"value";s:138:"转帐后请上传收据，以便核实您的付款。你可以随时登入帐户在［我的订单］按［付款］找回上传连结。";}i:2;a:3:{s:4:"name";s:10:"desc_en_us";s:4:"type";s:8:"textarea";s:5:"value";s:70:"To verify your payment, please upload your receipt after the transfer.";}i:3;a:3:{s:4:"name";s:9:"bank_user";s:4:"type";s:4:"text";s:5:"value";s:22:"Yoho Hong Kong Limited";}i:4;a:3:{s:4:"name";s:5:"banks";s:4:"type";s:6:"hidden";s:5:"value";a:3:{i:0;a:2:{s:4:"name";s:12:"滙豐銀行";s:7:"account";s:14:"456-348739-838";}i:1;a:2:{s:4:"name";s:12:"恒生銀行";s:7:"account";s:14:"799-041793-883";}i:2;a:2:{s:4:"name";s:12:"中國銀行";s:7:"account";s:18:"012-887-0-019977-9";}}}i:5;a:3:{s:4:"name";s:8:"tutorial";s:4:"type";s:6:"hidden";s:5:"value";s:138:"轉帳後請上傳收據，以便核實您的付款。你可以隨時登入帳戶在［我的訂單］按［付款］找回上傳連結。";}}' WHERE `pay_code` = 'ebanking';

-- bankmo
 UPDATE `ecs_payment` SET `pay_config` = 'a:6:{i:0;a:3:{s:4:"name";s:10:"desc_zh_tw";s:4:"type";s:8:"textarea";s:5:"value";s:54:"轉帳後請上傳收據，以便核實您的付款。";}i:1;a:3:{s:4:"name";s:10:"desc_zh_cn";s:4:"type";s:8:"textarea";s:5:"value";s:54:"转帐后请上传收据，以便核实您的付款。";}i:2;a:3:{s:4:"name";s:10:"desc_en_us";s:4:"type";s:8:"textarea";s:5:"value";s:70:"To verify your payment, please upload your receipt after the transfer.";}i:3;a:3:{s:4:"name";s:9:"bank_user";s:4:"type";s:4:"text";s:5:"value";s:23:"胡發枝 / Wu Faat Chi";}i:4;a:3:{s:4:"name";s:5:"banks";s:4:"type";s:6:"hidden";s:5:"value";a:2:{i:0;a:2:{s:4:"name";s:18:"澳門中國銀行";s:7:"account";s:21:"0018-01-11-10-5761512";}i:1;a:2:{s:4:"name";s:18:"澳門工商銀行";s:7:"account";s:19:"0119100100004889869";}}}i:5;a:3:{s:4:"name";s:8:"tutorial";s:4:"type";s:6:"hidden";s:5:"value";s:54:"轉帳後請上傳收據，以便核實您的付款。";}}' WHERE `pay_code` = 'bankmo';

-- ebankingmo
 UPDATE `ecs_payment` SET `pay_config` = 'a:6:{i:0;a:3:{s:4:"name";s:10:"desc_zh_tw";s:4:"type";s:8:"textarea";s:5:"value";s:54:"轉帳後請上傳收據，以便核實您的付款。";}i:1;a:3:{s:4:"name";s:10:"desc_zh_cn";s:4:"type";s:8:"textarea";s:5:"value";s:54:"转帐后请上传收据，以便核实您的付款。";}i:2;a:3:{s:4:"name";s:10:"desc_en_us";s:4:"type";s:8:"textarea";s:5:"value";s:70:"To verify your payment, please upload your receipt after the transfer.";}i:3;a:3:{s:4:"name";s:9:"bank_user";s:4:"type";s:4:"text";s:5:"value";s:23:"胡發枝 / Wu Faat Chi";}i:4;a:3:{s:4:"name";s:5:"banks";s:4:"type";s:6:"hidden";s:5:"value";a:2:{i:0;a:2:{s:4:"name";s:18:"澳門中國銀行";s:7:"account";s:21:"0018-01-11-10-5761512";}i:1;a:2:{s:4:"name";s:18:"澳門工商銀行";s:7:"account";s:19:"0119100100004889869";}}}i:5;a:3:{s:4:"name";s:8:"tutorial";s:4:"type";s:6:"hidden";s:5:"value";s:54:"轉帳後請上傳收據，以便核實您的付款。";}}' WHERE `pay_code` = 'ebankingmo';

-- fps
 UPDATE `ecs_payment` SET `pay_config` = 'a:6:{i:0;a:3:{s:4:"name";s:5:"phone";s:4:"type";s:4:"text";s:5:"value";s:8:"53356996";}i:1;a:3:{s:4:"name";s:5:"email";s:4:"type";s:4:"text";s:5:"value";s:21:"info@yohohongkong.com";}i:2;a:3:{s:4:"name";s:6:"fps_id";s:4:"type";s:4:"text";s:5:"value";s:7:"5552476";}i:3;a:3:{s:4:"name";s:4:"desc";s:4:"type";s:6:"hidden";s:5:"value";s:272:"您可透過網上及流動理財使用「轉數快」進行即時轉賬，或使用支援 「轉數快」的儲值支付工具。 轉帳後請上傳收據，以便核實您的付款。你可以隨時登入帳戶在［我的訂單］按［付款］找回上傳連結。";}i:4;a:3:{s:4:"name";s:8:"desc_eng";s:4:"type";s:8:"textarea";s:5:"value";s:272:"您可透過網上及流動理財使用「轉數快」進行即時轉賬，或使用支援 「轉數快」的儲值支付工具。 轉帳後請上傳收據，以便核實您的付款。你可以隨時登入帳戶在［我的訂單］按［付款］找回上傳連結。";}i:5;a:1:{s:4:"name";s:8:"tutorial";}}' WHERE `pay_code` = 'fps';

 CREATE TABLE `ecs_tag_lang` (
     `tag_id` INT NOT NULL,
     `lang` CHAR(10) NOT NULL,
     `tag_words` TEXT
 );