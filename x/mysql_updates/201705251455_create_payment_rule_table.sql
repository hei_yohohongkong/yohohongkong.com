/**
 * @Date:   2017-05-25T14:56:25+08:00
 * @Filename: 201705251455_create_payment_rule_table.sql
 * @Last modified time: 2017-06-02T11:59:07+08:00
 */

CREATE TABLE `ecs_payment_rule` (
	`pay_rule_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
	`rule_type` VARCHAR(120) NOT NULL DEFAULT '' ,
	`rule_equivalent` SET('eq','neq','gt','lt','gte','lte') NOT NULL ,
	`pay_id` INT UNSIGNED NOT NULL DEFAULT '0' ,
	`rule_value` VARCHAR(120) NOT NULL DEFAULT '' ,
	`parent_id` INT NOT NULL DEFAULT '0' ,
	PRIMARY KEY (`pay_rule_id`), INDEX (`rule_type`), INDEX (`pay_id`), INDEX (`parent_id`)
) ENGINE = InnoDB;

/* Old rule:: */
/* 如果是香港或澳門，顯示銀行過數選項 */
INSERT INTO `ecs_payment_rule`
(`pay_rule_id`, `rule_type`, `rule_equivalent`, `pay_id`, `rule_value`, `parent_id`)
SELECT NULL, 'country', 'eq', `pay_id`, '3409', '0'
FROM `ecs_payment`
WHERE pay_code IN('bank', 'ebanking') AND enabled = 1;

INSERT INTO `ecs_payment_rule`
(`pay_rule_id`, `rule_type`, `rule_equivalent`, `pay_id`, `rule_value`, `parent_id`)
SELECT NULL, 'country', 'eq', `pay_id`, '3410', '0'
FROM `ecs_payment`
WHERE pay_code IN('bank', 'ebanking') AND enabled = 1;

/* 如果是澳門或台灣，顯示支付寶選項 */
INSERT INTO `ecs_payment_rule`
(`pay_rule_id`, `rule_type`, `rule_equivalent`, `pay_id`, `rule_value`, `parent_id`)
SELECT NULL, 'country', 'eq', `pay_id`, '3410', '0'
FROM `ecs_payment`
WHERE pay_code IN('alipay', 'alipayc2c') AND enabled = 1;

INSERT INTO `ecs_payment_rule`
(`pay_rule_id`, `rule_type`, `rule_equivalent`, `pay_id`, `rule_value`, `parent_id`)
SELECT NULL, 'country', 'eq', `pay_id`, '3411', '0'
FROM `ecs_payment`
WHERE pay_code IN('alipay', 'alipayc2c') AND enabled = 1;

/* 如果是中國，顯示支付寶境外收單選項 */
INSERT INTO `ecs_payment_rule`
(`pay_rule_id`, `rule_type`, `rule_equivalent`, `pay_id`, `rule_value`, `parent_id`)
SELECT NULL, 'country', 'eq', `pay_id`, '3436', '0'
FROM `ecs_payment`
WHERE pay_code IN('alipayglobal') AND enabled = 1;
