/**
 * Author:  Anthony
 * Created: 2017-4-11
 */

CREATE TABLE `ecs_quotation_info`
 ( `quotation_id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT ,
`quotation_sn` VARCHAR(20) NOT NULL ,
`company_name` VARCHAR(60) NOT NULL , `contact_name` VARCHAR(60) NOT NULL ,
`contact_email` VARCHAR(60) NOT NULL , `contact_tel` VARCHAR(60) NOT NULL ,
`shipping_id` MEDIUMINT(8) UNSIGNED NOT NULL , `shipping_fee` DECIMAL(10,2) NOT NULL DEFAULT '0.00' ,
`pay_id` MEDIUMINT(8) UNSIGNED NOT NULL , `pay_fee` DECIMAL(10,2) NOT NULL DEFAULT '0.00' ,
`quotation_fee` DECIMAL(10,2) NOT NULL DEFAULT '0.00' , `created_at` INT(11) NOT NULL ,
PRIMARY KEY (`quotation_id`)) ENGINE = InnoDB;