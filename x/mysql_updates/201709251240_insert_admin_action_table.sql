/**
 * Author:  Dem
 * Created: 2017-9-25
 * Insert new admin action for erp payment receipt
 */

INSERT INTO `ecs_admin_action` (`parent_id`, `action_code`) VALUES ('137', 'erp_order_receipt');