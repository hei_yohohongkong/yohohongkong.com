INSERT INTO `ecs_erp_warehousing_style` (`warehousing_style_id`, `warehousing_style`, `is_valid`) VALUES (NULL, '貨倉調貨', '1');
INSERT INTO `ecs_erp_delivery_style` (`delivery_style_id`, `delivery_style`, `is_valid`) VALUES (NULL, '貨倉調貨', '1');
ALTER TABLE `ecs_delivery_order` ADD `is_fls` BOOLEAN NOT NULL DEFAULT FALSE AFTER `track_start_date`;
ALTER TABLE `ecs_erp_warehouse` ADD `code` VARCHAR(10) NULL AFTER `is_on_sale`;