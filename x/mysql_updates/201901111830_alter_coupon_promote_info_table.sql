/**
 * Author:  Dem
 * Created: 2019-01-11
 * Sql for promote coupon
 */

 ALTER TABLE `ecs_coupon_promote_info` ADD `coupon_id` INT NOT NULL AFTER `promote_id`;
 UPDATE `ecs_coupon_promote_info` SET `coupon_id` = 930;