CREATE TABLE `ecs_logistics` (
    `logistics_id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT ,
    `logistics_code` VARCHAR(20) NOT NULL DEFAULT '' ,
    `logistics_name` VARCHAR(120) NOT NULL DEFAULT '' ,
    `logistics_desc` VARCHAR(255) NOT NULL DEFAULT '' ,
    `enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
    `created_at` TIMESTAMP  NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    `updated_at` TIMESTAMP  NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
    `logistics_url` VARCHAR(255) NOT NULL DEFAULT '' ,
    `logistics_logo` VARCHAR(255) NOT NULL DEFAULT '' ,
    PRIMARY KEY (`logistics_id`), INDEX (`enabled`)
) ENGINE = InnoDB;

CREATE TABLE `ecs_logistics_lang` (
    `logistics_id` MEDIUMINT(8) UNSIGNED NOT NULL ,
    `lang` CHAR(10) NOT NULL ,
    `logistics_name` VARCHAR(120) NOT NULL DEFAULT '' ,
    PRIMARY KEY (`logistics_id`, `lang`)
) ENGINE = InnoDB;

ALTER TABLE `ecs_delivery_order` ADD `logistics_id` MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0 , ADD INDEX (`logistics_id`);

INSERT INTO `ecs_shop_config` (`parent_id`, `code`, `type`, `sort_order`, `value`) VALUES ('1', 'trackingmore_api', 'text', '1', '159d182e-0915-412e-b1c8-88956b75235a');

/*  Insert admin action */
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '5', 'logistics', '');
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '189', 'menu_09_logistics_list', '');