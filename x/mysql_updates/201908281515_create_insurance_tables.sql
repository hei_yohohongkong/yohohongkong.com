/**
 * Author:  Sing
 * Created: 2019-08-28
 */


CREATE TABLE `ecs_insurance` ( 
`insurance_id` MEDIUMINT(8) NOT NULL AUTO_INCREMENT , 
`name` VARCHAR(30) NOT NULL , 
`duration_options` VARCHAR(50) NOT NULL , 
`html_content` TEXT NOT NULL , 
`status` TINYINT(1) NOT NULL DEFAULT '1' , 
PRIMARY KEY (`insurance_id`)
);

ALTER TABLE `ecs_insurance` ADD `duration` TINYINT(3) NOT NULL DEFAULT '0' AFTER `insurance_id`;

ALTER TABLE `ecs_insurance` DROP PRIMARY KEY, ADD PRIMARY KEY (`insurance_id`, `duration`) USING BTREE;

ALTER TABLE `ecs_insurance` DROP `duration_options`;

ALTER TABLE `ecs_insurance` ADD `target_goods_id` MEDIUMINT(8) NOT NULL AFTER `name`;

ALTER TABLE `ecs_insurance` ADD `code` VARCHAR(20) NOT NULL AFTER `duration`;

CREATE TABLE `ecs_insurance_goods` ( 
`insurance_goods_id` MEDIUMINT(8) NOT NULL AUTO_INCREMENT , 
`insurance_id` MEDIUMINT(8) NOT NULL , 
`goods_id` MEDIUMINT(8) NOT NULL , 
`duration` TINYINT(3) NOT NULL DEFAULT '0' , 
`price_percent` TINYINT(3) NOT NULL DEFAULT '0' , 
PRIMARY KEY (`insurance_goods_id`)
);

ALTER TABLE `ecs_insurance_goods` ADD UNIQUE(`insurance_id`, `goods_id`, `duration`);

CREATE TABLE `ecs_insurance_lang` ( `insurance_id` MEDIUMINT(8) NOT NULL , `lang` CHAR(10) NOT NULL , `name` VARCHAR(30) NULL DEFAULT NULL );

ALTER TABLE `ecs_insurance_lang` ADD PRIMARY KEY( `insurance_id`, `lang`);

ALTER TABLE `ecs_cart` ADD `is_insurance` TINYINT(1) NOT NULL DEFAULT '0' AFTER `is_shipping`;

ALTER TABLE `ecs_cart` ADD `insurance_of` MEDIUMINT(8) NOT NULL DEFAULT '0' AFTER `parent_id`;

ALTER TABLE `ecs_order_goods` ADD `is_insurance` TINYINT(1) NOT NULL DEFAULT '0' AFTER `is_package`;

ALTER TABLE `ecs_order_goods` ADD `insurance_of` MEDIUMINT(8) NOT NULL DEFAULT '0' AFTER `parent_id`;

ALTER TABLE `ecs_goods` ADD `is_insurance` TINYINT(1) NOT NULL DEFAULT '0' AFTER `is_promote`;

ALTER TABLE `ecs_goods_log` ADD `is_insurance` TINYINT(1) NOT NULL DEFAULT '0' AFTER `is_promote`;

ALTER TABLE `ecs_delivery_goods` ADD `is_insurance` TINYINT(1) NOT NULL DEFAULT '0' AFTER `warehouse_id`;

ALTER TABLE `ecs_delivery_goods` ADD `insurance_of` MEDIUMINT(8) NOT NULL DEFAULT '0' AFTER `is_insurance`;

CREATE TABLE `ecs_insurance_claim` (
`claim_id` MEDIUMINT(8) NOT NULL AUTO_INCREMENT , 
`order_id` MEDIUMINT(8) NOT NULL DEFAULT '0' , 
`order_goods_id` MEDIUMINT(8) NOT NULL DEFAULT '0' , 
`claim_amount` DECIMAL(10,2) NOT NULL DEFAULT '0.00' , 
`receipt_img` VARCHAR(255) NOT NULL , 
`claim_datetime` INT(10) NOT NULL , 
`approve_by` VARCHAR(60) NOT NULL , 
PRIMARY KEY (`claim_id`)
);

INSERT INTO `ecs_mail_templates` (
`template_code`, `is_html`, `template_subject`, `template_content`, `last_modify`, `last_send`, `type`
) 
VALUES (
'insurance_notice', 
'1', 
'保險服務通知', 
'<table style=\"border: 1px solid; border-color: #e7e7e7; margin-left: auto; margin-right: auto; width: 600px;\" border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td colspan=\"3\"><img usemap=\"#m_-5847510787762023062_imgmap2017929151620\" class=\"CToWUd\" src=\"https://yohohongkong.com/images/upload/Image/1507831950789963146.png\" border=\"0\" alt=\"\"><map id=\"m_-5847510787762023062imgmap2017929151620\" name=\"m_-5847510787762023062_imgmap2017929151620\"> \r\n<area shape=\"circle\" alt=\"\" coords=\"477,53,13\" href=\"https://www.facebook.com/yohohongkong\" target=\"_blank\">\r\n \r\n<area shape=\"circle\" alt=\"\" coords=\"508,54,13\" href=\"https://www.youtube.com/yohohongkong\" target=\"_blank\">\r\n \r\n<area shape=\"circle\" alt=\"\" coords=\"540,55,13\" href=\"https://www.instagram.com/yohohongkong/\" target=\"_blank\">\r\n \r\n<area shape=\"circle\" alt=\"\" coords=\"571,55,13\" href=\"https://plus.google.com/+yohohongkong\" target=\"_blank\">\r\n \r\n<area shape=\"rect\" alt=\"\" coords=\"209,7,390,72\" href=\"https://yohohongkong.com/\" target=\"_blank\">\r\n </map></td>\r\n</tr>\r\n<tr>\r\n<td colspan=\"3\"><strong style=\"padding-left: 5px;\">你好，</strong>\r\n<div style=\"padding: 10px 5px;\">您的訂單 [訂單編號 {$order.order_sn}] 已成功購買:<br>{$order.ins_goods_name}<br><br><a href=\"https://yohohongkong.com\" target=\"_blank\" rel=\"noopener noreferrer\">&lt;服務條款及細則&gt;</a></div>\r\n</td>\r\n</tr>\r\n<tr style=\"margin-top: 5px;\">\r\n<td colspan=\"3\"><span style=\"padding-left: 5px; color: #999999;\">感謝您對友和的支持，希望您購物愉快！</span><br><br><span style=\"padding-left: 5px; color: #999999;\">{$shop_name}</span><br><span style=\"padding-left: 5px; color: #999999;\">{$send_date}</span></td>\r\n</tr>\r\n<tr>\r\n<td colspan=\"3\">&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td colspan=\"3\"><a class=\"click-to-buy\" href=\"http://www.yohohongkong.com/vip\"><img src=\"http://www.yohohongkong.com/images/upload/Image/1566979368622276475.png\" border=\"0\"></a></td>\r\n</tr>\r\n<tr>\r\n<td colspan=\"3\"><img usemap=\"#imgmap2017929165943\" src=\"https://yohohongkong.com/images/upload/Image/1506968194128151352.png\" border=\"0\"><map id=\"imgmap2017929165943\" name=\"imgmap2017929165943\"> \r\n<area shape=\"rect\" alt=\"\" coords=\"1,1,134,55\" href=\"https://yohohongkong.com/cat/1-%E9%9B%BB%E5%AD%90%E7%94%A2%E5%93%81\" target=\"\">\r\n \r\n<area shape=\"rect\" alt=\"\" coords=\"137,1,265,58\" href=\"https://yohohongkong.com/cat/23-%E7%BE%8E%E5%AE%B9%E5%8F%8A%E8%AD%B7%E7%90%86\" target=\"\">\r\n \r\n<area shape=\"rect\" alt=\"\" coords=\"269,0,378,57\" href=\"https://yohohongkong.com/cat/184-%E5%AE%B6%E5%BA%AD%E9%9B%BB%E5%99%A8\" target=\"\">\r\n \r\n<area shape=\"rect\" alt=\"\" coords=\"382,1,493,55\" href=\"https://yohohongkong.com/cat/44-%E9%9B%BB%E8%85%A6\" target=\"\">\r\n \r\n<area shape=\"rect\" alt=\"\" coords=\"496,1,601,56\" href=\"https://yohohongkong.com/cat/171-生活時尚\" target=\"\">\r\n </map></td>\r\n</tr>\r\n<tr style=\"background: #00acee; color: white;\">\r\n<td colspan=\"3\">\r\n<div class=\"footer-content\" style=\"float: left; padding: 10px; color: white; font-size: 12px;\">請勿回覆此電郵，您可以發電郵至<a style=\"color: #0645ad;\" href=\"mailto:info@yohohongkong.com\" target=\"_blank\" rel=\"noopener noreferrer\">info@yohohongkong.com</a>&nbsp;或使用<a href=\"https://yohohongkong.com/\">yohohongkong.com</a>&nbsp;/ 致電 53356996 與我們保持聯絡。</div>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>', 
'1568922075', 
'0', 
'template'
);

INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `parent_cat`, `action_code`, `relevance`, `remarks`) VALUES (NULL, '189', '022_insurance_service', 'menu_022_insurance_service', '', 'menu');