/**
 * Author:  Dem
 * Created: 2018-08-10
 * Sql for auto pricing
 */
 
CREATE TABLE `ecs_flashdeal_goods` (
    `fg_id` INT NOT NULL AUTO_INCREMENT,
    `flashdeal_id` INT NOT NULL,
    `status` INT NOT NULL DEFAULT 0,
    `create_at` INT(10) UNSIGNED NOT NULL,
    `update_time` INT(10) UNSIGNED NOT NULL,
    PRIMARY KEY(`fg_id`),
    INDEX `flashdeal_id` (`flashdeal_id`)
);