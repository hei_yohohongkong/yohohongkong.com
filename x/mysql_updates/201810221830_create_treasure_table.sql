/**
 * Author:  Dem
 * Created: 2018-10-22
 * Sql for treasure hunting
 */

 CREATE TABLE `ecs_treasure` (
     `treasure_id` INT NOT NULL AUTO_INCREMENT,
     `treasure_name` VARCHAR(255),
     `treasure_desc` VARCHAR(255),
     `treasure_tnc` VARCHAR(255),
     `coupon_id` VARCHAR(255),
     `start_time` INT(10) DEFAULT 0,
     `end_time` INT(10) DEFAULT 0,
     `open_time` INT(10) DEFAULT 0,
     `confirm` INT(1) DEFAULT 0,
     `path` TEXT,
     `ip` VARCHAR(15) NOT NULL,
     `session_id` CHAR(32) NOT NULL,
     PRIMARY KEY (`treasure_id`),
     INDEX `time` (`start_time` ASC, `end_time` ASC)
 );
 INSERT INTO `ecs_shop_config` (`parent_id`, `code`, `type`, `value`) VALUES ('6', 'treasure_start', 'hidden', '0');
 INSERT INTO `ecs_shop_config` (`parent_id`, `code`, `type`, `value`) VALUES ('6', 'treasure_notice', 'hidden', '');