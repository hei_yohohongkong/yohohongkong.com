 CREATE TABLE `ecs_global_alert` (
     `alert_id` INT NOT NULL AUTO_INCREMENT,
     `create_at` INT NOT NULL DEFAULT '0',
     `content` TEXT,
     `url` VARCHAR(255) NOT NULL DEFAULT '',
     `end_time` INT(10) DEFAULT 0,
     PRIMARY KEY (`alert_id`)
 );

INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '7', 'support_manage', '');
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '189', 'menu_support_manage', '');
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '7', 'global_alert', '');
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '189', 'menu_global_alert', '');