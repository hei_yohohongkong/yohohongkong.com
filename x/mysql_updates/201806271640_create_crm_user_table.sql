/**
 * Author:  Dem
 * Created: 2018-06-27
 * Sql for crm system
 */

CREATE TABLE `ecs_crm_platform_user` (
    `rec_id` INT NOT NULL AUTO_INCREMENT,
    `user_id` INT NOT NULL,
    `platform` INT NOT NULL,
    `client_id` VARCHAR(255) NOT NULL,
    PRIMARY KEY(`rec_id`)    
);