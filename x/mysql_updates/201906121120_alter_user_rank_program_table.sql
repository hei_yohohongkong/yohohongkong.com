ALTER TABLE `ecs_user_rank_program` ADD `pricing_profile_id` INT(11) NULL AFTER `require_proof`;
ALTER TABLE `ecs_user_rank_program` ADD `need_verify` BOOLEAN NOT NULL DEFAULT TRUE AFTER `pricing_profile_id`;
ALTER TABLE `ecs_user_rank_program` ADD `show_on_menu` BOOLEAN NOT NULL DEFAULT FALSE AFTER `need_verify`;


CREATE TABLE `ecs_user_rank_program_pricing_profile` (
  `pricing_profile_id` int(11) NOT NULL AUTO_INCREMENT ,
  `profile_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_rank_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
    PRIMARY KEY (`pricing_profile_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `ecs_user_rank_program_pricing_profile_items` (
  `pricing_profile_items_id` int(11) NOT NULL  AUTO_INCREMENT ,
  `pricing_profile_id` int(11) NOT NULL,
  `goods_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `active` tinyint(1) NOT NULL DEFAULT '0',
    PRIMARY KEY (`pricing_profile_items_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
