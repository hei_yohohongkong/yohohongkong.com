/**
 * Author:  Dem
 * Created: 2018-08-22
 * Sql for create lucky draw free gift order
 */

 ALTER TABLE `ecs_lucky_draw` ADD `coupon_code` VARCHAR(255) DEFAULT NULL;
 ALTER TABLE `ecs_coupons` ADD `sale_exclude` INT DEFAULT 0;
 CREATE INDEX `coupon_id` ON `ecs_order_coupons` (`coupon_id`)