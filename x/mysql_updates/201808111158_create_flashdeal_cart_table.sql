/**
 * Created by Anthony.
 * Created: 2018-08-10
 */

CREATE TABLE `ecs_flashdeal_cart` (
  `fg_id` INT(10) NOT NULL ,
  `user_id` INT(10) NOT NULL DEFAULT 0,
  `create_at` INT(10) UNSIGNED NOT NULL DEFAULT 0,
  `expired` INT(10) UNSIGNED NOT NULL DEFAULT 0,
  `session_id` char(32) NOT NULL DEFAULT '',
  `flashdeal_id` INT(10) NOT NULL DEFAULT 0,
  `cart_id` INT(10) NOT NULL DEFAULT 0,
  `order_id` INT(10) NOT NULL DEFAULT 0,
  PRIMARY KEY(`fg_id`),
  INDEX `user_id` (`user_id`)
);
CREATE INDEX ecs_flashdeal_cart_user_id_expired_index ON ecs_flashdeal_cart (user_id, expired);