/**
 * Author:  Dem
 * Created: 2018-10-04
 * Sql for price.com get water price
 */

ALTER TABLE `ecs_goods_pricecomhk` ADD `is_water` INT NOT NULL DEFAULT 0;