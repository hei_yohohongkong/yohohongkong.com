/**
 * Author:  Dem
 * Created: 2019-01-29
 * Sql for ga statistics logging
 */

CREATE TABLE `ecs_google_statistics` (
    `rec_id` INT NOT NULL AUTO_INCREMENT,
    `goods_id` INT NOT NULL,
    `date` DATETIME NOT NULL,
    `session` INT NOT NULL,
    `pageview` INT NOT NULL,
    `pageview_unique` INT NOT NULL,
    PRIMARY KEY (`rec_id`),
    INDEX (`goods_id`),
    INDEX (`date`)
);