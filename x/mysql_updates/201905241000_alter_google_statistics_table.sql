/**
 * Author:  Dem
 * Created: 2019-05-14
 * Sql for ga statistics analysis - landing page
 */

ALTER TABLE `ecs_google_statistics` ADD UNIQUE (`goods_id`, `date`);

ALTER TABLE `ecs_google_statistics` ADD `landing_session` INT NOT NULL, ADD `landing_pageview` INT NOT NULL, ADD `landing_pageview_unique` INT NOT NULL;
ALTER TABLE `ecs_google_statistics_lifetime` ADD `landing_session` INT NOT NULL, ADD `landing_pageview` INT NOT NULL, ADD `landing_pageview_unique` INT NOT NULL;

ALTER TABLE `ecs_google_statistics_temporary_view` ADD `parent_cat_id` INT DEFAULT 0 AFTER `goods_id`;
ALTER TABLE `ecs_google_statistics_temporary_view` ADD `landing_session_7` INT DEFAULT 0 AFTER `pageview_unique_7`;
ALTER TABLE `ecs_google_statistics_temporary_view` ADD `landing_session_7to7` INT DEFAULT 0 AFTER `pageview_unique_7to7`;
ALTER TABLE `ecs_google_statistics_temporary_view` ADD `landing_session_30` INT DEFAULT 0 AFTER `pageview_unique_30`;
ALTER TABLE `ecs_google_statistics_temporary_view` ADD `landing_session_30to30` INT DEFAULT 0 AFTER `pageview_unique_30to30`;
ALTER TABLE `ecs_google_statistics_temporary_view` ADD `landing_pageview_7` INT DEFAULT 0 AFTER `landing_session_7`;
ALTER TABLE `ecs_google_statistics_temporary_view` ADD `landing_pageview_7to7` INT DEFAULT 0 AFTER `landing_session_7to7`;
ALTER TABLE `ecs_google_statistics_temporary_view` ADD `landing_pageview_30` INT DEFAULT 0 AFTER `landing_session_30`;
ALTER TABLE `ecs_google_statistics_temporary_view` ADD `landing_pageview_30to30` INT DEFAULT 0 AFTER `landing_session_30to30`;
ALTER TABLE `ecs_google_statistics_temporary_view` ADD `landing_pageview_unique_7` INT DEFAULT 0 AFTER `landing_pageview_7`;
ALTER TABLE `ecs_google_statistics_temporary_view` ADD `landing_pageview_unique_7to7` INT DEFAULT 0 AFTER `landing_pageview_7to7`;
ALTER TABLE `ecs_google_statistics_temporary_view` ADD `landing_pageview_unique_30` INT DEFAULT 0 AFTER `landing_pageview_30`;
ALTER TABLE `ecs_google_statistics_temporary_view` ADD `landing_pageview_unique_30to30` INT DEFAULT 0 AFTER `landing_pageview_30to30`;