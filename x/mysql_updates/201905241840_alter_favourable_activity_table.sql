/**
 * Author:  Ken
 * Created: 2019-05-24
 * Sql for add min quantity for table "favourable_activity"
 */

ALTER TABLE `ecs_favourable_activity` ADD `min_quantity` INT(11) NOT NULL AFTER `max_amount`;
UPDATE `ecs_favourable_activity` SET `min_quantity`=1;

ALTER TABLE `ecs_favourable_activity` ADD `act_kind` TINYINT(3) NOT NULL AFTER `min_quantity`;