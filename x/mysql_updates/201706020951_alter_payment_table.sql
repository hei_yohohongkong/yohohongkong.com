/**
 * @Date:   2017-06-02T09:53:14+08:00
 * @Filename: 201706020951_alter_payment_table.sql
 * @Last modified time: 2017-06-02T11:58:02+08:00
 */

ALTER TABLE `ecs_payment` ADD `platform` VARCHAR(120) NOT NULL DEFAULT '';

UPDATE `ecs_payment` SET `platform` = 'web,mobile' WHERE `ecs_payment`.`pay_code` IN('bank','ebanking','paypal') AND enabled = 1;
