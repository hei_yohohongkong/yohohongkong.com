-- CHANGE "TYPE, SCALE" OF "FIELD "gateway_transaction_amount" -
ALTER TABLE `ecs_wechat_pay_log` MODIFY `gateway_transaction_amount` Decimal( 10, 2 ) NOT NULL;
-- -------------------------------------------------------------