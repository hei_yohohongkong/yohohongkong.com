CREATE TABLE `ecs_admin_tasklog` (
  `tasklog_id` INT NOT NULL AUTO_INCREMENT,
  `task_id` INT NOT NULL,
  `sender_read` TINYINT NULL DEFAULT 0,
  `receiver_read` TINYINT NULL DEFAULT 0,
  `content` TEXT NOT NULL,
  `new_status` VARCHAR(45) NOT NULL,
  `created_by` INT NOT NULL,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`tasklog_id`),
  INDEX `task_id` (`task_id` ASC, `sender_read` ASC, `receiver_read` ASC, `created_by` ASC, `tasklog_id` ASC));
