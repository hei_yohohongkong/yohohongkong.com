/**
 * Author:  Sing
 * Created: 2019-07-30
 */


CREATE TABLE `ecs_flashdeal_event` (
    `fd_event_id` INT(11) NOT NULL AUTO_INCREMENT , 
    `name` VARCHAR(120) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , 
    `start_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , 
    `end_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , 
    `status` TINYINT(1) NOT NULL DEFAULT '0' , 
    `remark` TEXT NULL DEFAULT NULL , 
    `detail_visible` TINYINT(1) NOT NULL DEFAULT '0' , 
    `is_online_pay` TINYINT(1) NOT NULL DEFAULT '0' , 
    `user_rank` VARCHAR(255) NULL DEFAULT NULL , 
    `last_update_name` VARCHAR(30) NULL DEFAULT NULL , 
    `last_update_time` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , 
    PRIMARY KEY (`fd_event_id`)
);

ALTER TABLE `ecs_flashdeals` ADD `fd_event_id` INT(11) NULL DEFAULT NULL AFTER `flashdeal_id`;

ALTER TABLE `ecs_flashdeals` ADD INDEX(`fd_event_id`);

ALTER TABLE `ecs_flashdeals` ADD `status` TINYINT NOT NULL DEFAULT '0' AFTER `is_online_pay`;

INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '189', 'menu_flashdeal_events', '');

UPDATE `ecs_admin_action` SET `parent_cat` = 'mkt_method' WHERE `action_code` = 'menu_flashdeal_events';

INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `parent_cat`, `action_code`, `relevance`, `remarks`) VALUES (NULL, '189', 'mkt_method', 'flashdeal_events_on_off', '', '');
