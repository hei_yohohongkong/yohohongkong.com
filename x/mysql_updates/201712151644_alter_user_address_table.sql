ALTER TABLE `ecs_user_address` 
ADD `google_place` CHAR(255) NOT NULL DEFAULT '', 
ADD `street_name` CHAR(255) NOT NULL DEFAULT '', 
ADD `street_num` CHAR(255) NOT NULL DEFAULT '', 
ADD `locality` CHAR(255) NOT NULL DEFAULT '',
ADD `administrative_area` CHAR(255) NOT NULL DEFAULT '',
ADD `floor` CHAR(255) NOT NULL DEFAULT '' AFTER `sign_building`, 
ADD `room` CHAR(255) NOT NULL DEFAULT '' AFTER `floor`,
ADD `block` CHAR(255) NOT NULL DEFAULT '', 
ADD `phase` CHAR(255) NOT NULL DEFAULT '', 
ADD `hk_area` int(10) NOT NULL DEFAULT 0;

ALTER TABLE `ecs_order_info` 
ADD `address_id` int(10) UNSIGNED NOT NULL DEFAULT 0;

CREATE TABLE `ecs_order_address` ( `order_address_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
 `order_id` int(10) UNSIGNED NOT NULL DEFAULT 0, 
 `consignee` varchar(60) NOT NULL DEFAULT '', 
 `country` smallint(5) NOT NULL DEFAULT 0, 
 `province` smallint(5) NOT NULL DEFAULT 0, 
 `city` smallint(5) NOT NULL DEFAULT 0, 
 `district` smallint(5) NOT NULL DEFAULT 0, 
 `address` varchar(120) NOT NULL DEFAULT '', 
 `zipcode` varchar(60) NOT NULL DEFAULT '',
 `sign_building` varchar(120) NOT NULL DEFAULT '',
 `google_place` CHAR(255) NOT NULL DEFAULT '', 
 `street_name` CHAR(255) NOT NULL DEFAULT '', 
 `street_num` CHAR(255) NOT NULL DEFAULT '', 
 `locality` CHAR(255) NOT NULL DEFAULT '',
 `administrative_area` CHAR(255) NOT NULL DEFAULT '',
 `floor` CHAR(255) NOT NULL DEFAULT '', 
 `block` CHAR(255) NOT NULL DEFAULT '', 
 `phase` CHAR(255) NOT NULL DEFAULT '', 
 `room` CHAR(255) NOT NULL DEFAULT '',
 `hk_area` int(10) NOT NULL DEFAULT 0,
 `auto_break` int(1) NOT NULL DEFAULT 0,
 INDEX (`order_id`),
 INDEX (`sign_building`),
PRIMARY KEY (`order_address_id`));