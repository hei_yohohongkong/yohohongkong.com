/**
 * Author:  Dem
 * Created: 2017-8-29
 * Add payment config column
 */

ALTER TABLE `ecs_payment_lang` ADD `pay_config` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci;