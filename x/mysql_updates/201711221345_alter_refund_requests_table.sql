/**
 * Author:  Dem
 * Created: 2017-11-22
 * Add column to store status update time
 */

ALTER TABLE `ecs_refund_requests` ADD `update_time` int(11) DEFAULT 0;
