/**
 * Author:  Dem
 * Created: 2019-05-20
 * Sql for permanent link
 */

 CREATE TABLE `ecs_perma_link` (
     `link_id` INT NOT NULL AUTO_INCREMENT,
     `table_name` VARCHAR(255) NOT NULL,
     `id` INT NOT NULL,
     `lang` VARCHAR(255) NOT NULL,
     `perma_link` TEXT NOT NULL,
     `last_update` DATETIME DEFAULT CURRENT_TIMESTAMP,
     PRIMARY KEY (`link_id`),
     UNIQUE (`table_name`, `id`, `lang`),
     INDEX `item` (`table_name`, `id`, `lang`)
 );