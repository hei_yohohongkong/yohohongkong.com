
-- Add attribute desc
ALTER TABLE `ecs_attribute` ADD `attr_desc` VARCHAR(255) NOT NULL DEFAULT '' AFTER `attr_name`;

-- Update goods_attr attr_price to default 0
ALTER TABLE `ecs_goods_attr` CHANGE `attr_price` `attr_price` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0';

-- Update attribute menu priv
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '189', 'menu_08_attribute', '');
