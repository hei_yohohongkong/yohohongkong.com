/**
 * Author:  Dem
 * Created: 2018-08-27
 * Sql for click counts of homepage carousel
 */

 ALTER TABLE `ecs_homepage_carousel` ADD `clicks` INT DEFAULT 0;