ALTER TABLE `ecs_tag`
ADD `cat_id` INT(10) NOT NULL DEFAULT '0' AFTER `goods_id`,
ADD `brand_id` INT(10) NOT NULL DEFAULT '0' AFTER `cat_id`,
ADD `article_id` INT(10) NOT NULL DEFAULT '0' AFTER `brand_id`,
ADD INDEX (`cat_id`), ADD INDEX (`brand_id`), ADD INDEX (`article_id`), ADD INDEX (`tag_words`);
