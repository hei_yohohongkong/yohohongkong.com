/**
*   Created by: Billy Lee  
*   2019-05-03
*   Add path to report query entries
*/

ALTER TABLE `ecs_report_query` ADD COLUMN `path` VarChar( 255 ) NOT NULL DEFAULT 'x/reports/';