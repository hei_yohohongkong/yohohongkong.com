/**
 * Author:  Dem
 * Created: 2018-05-28
 * Sql for adding disable admin user
 */

ALTER TABLE `ecs_admin_user` ADD `is_disable` INT NOT NULL DEFAULT '0';
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '0', 'operator_list', '');
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '0', 'assignable_payment_op', '');
UPDATE `ecs_admin_action` SET `parent_id` = (SELECT * FROM (SELECT `action_id` FROM `ecs_admin_action` WHERE `action_code` = 'operator_list') temp) WHERE `action_code` IN ('assignable_preorder_operator', 'assignable_outofstock_firstop', 'assignable_outofstock_recop', 'assignable_refund_app', 'assignable_order_msg', 'assignable_po_op', 'assignable_payment_op', 'assignable_warehouse_op');