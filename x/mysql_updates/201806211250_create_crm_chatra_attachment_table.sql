/**
 * Author:  Dem
 * Created: 2018-06-21
 * Sql for crm system
 */

CREATE TABLE `ecs_crm_chatra_attachment` (
    `attach_id` INT NOT NULL AUTO_INCREMENT,
    `list_id` INT NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `url` VARCHAR(255) NOT NULL,
    `size` INT DEFAULT 0,
    `is_image` INT DEFAULT 0,
    PRIMARY KEY(`attach_id`)    
);

CREATE TABLE `ecs_crm_chatra_agent` (
    `agent_id` INT NOT NULL AUTO_INCREMENT,
    `admin_id` INT NOT NULL,
    `chatra_id` VARCHAR(255) NOT NULL,
    PRIMARY KEY(`agent_id`)    
);

INSERT INTO `ecs_crm_chatra_agent` (`agent_id`, `admin_id`, `chatra_id`) VALUES (NULL, '1', 'MWjJzH6SJZMaFRe6v');