/**
 * Author:  Ken
 * Created: 2019-05-24
 * Sql for add goods price for table "favourable_activity"
 */

ALTER TABLE `ecs_package_goods` ADD `goods_price` DECIMAL(10,2) NOT NULL DEFAULT '1' AFTER `goods_number`;