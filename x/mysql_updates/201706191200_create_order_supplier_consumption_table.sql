CREATE TABLE `ecs_order_goods_supplier_consumption` (
  `consumption_id` INT NOT NULL AUTO_INCREMENT,
  `order_goods_id` VARCHAR(45) NOT NULL,
  `goods_id` VARCHAR(45) NOT NULL,
  `supplier_id` VARCHAR(45) NOT NULL,
  `consumed_qty` INT(11) NOT NULL,
  `po_unit_cost` DECIMAL(10,2) NOT NULL,
  PRIMARY KEY (`consumption_id`),
  INDEX `supplier` (`supplier_id` ASC),
  INDEX `goods` (`goods_id` ASC, `order_goods_id` ASC),
  UNIQUE KEY `supplier_rec` (`order_goods_id`,`supplier_id`));