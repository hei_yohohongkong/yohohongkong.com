/**
 * Author:  Dem
 * Created: 2018-07-19
 * Sql for auto pricing
 */

CREATE TABLE `ecs_goods_hktvmall` (
    `goods_id` INT NOT NULL,
    `hktvmall_id` VARCHAR(255),
    `price` DECIMAL (10, 2) DEFAULT 0,
    `discount` DECIMAL (10, 2) DEFAULT 0,
    `last_fetch` INT (11) DEFAULT 0,
    PRIMARY KEY(`goods_id`)    
);

CREATE TABLE `ecs_goods_fortress` (
    `goods_id` INT NOT NULL,
    `fortress_id` VARCHAR(255),
    `price` DECIMAL (10, 2) DEFAULT 0,
    `discount` DECIMAL (10, 2) DEFAULT 0,
    `last_fetch` INT (11) DEFAULT 0,
    PRIMARY KEY(`goods_id`)    
);