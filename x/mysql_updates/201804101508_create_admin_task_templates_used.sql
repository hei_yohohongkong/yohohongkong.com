CREATE TABLE `ecs_admin_task_templates_used` (
  `task_templates_used_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) UNSIGNED NOT NULL,
  `template_id` int(11) UNSIGNED NOT NULL,
  `frequency` int(11) UNSIGNED NOT NULL,
  `last_modified_time` int(10) UNSIGNED NOT NULL,
	PRIMARY KEY(`task_templates_used_id`)
);

CREATE TABLE `ecs_admin_task_templates` (
  `template_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci,
  `priority` enum('Low','Normal','High','Urgent','Immediate') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Normal',
  `estimated_time` int(10) UNSIGNED NOT NULL,
	PRIMARY KEY(`template_id`)
);

INSERT INTO `ecs_admin_task_templates` (`template_id`, `title`, `message`, `priority`, `estimated_time`) VALUES
(1, 'Update Anyspecs and Sync', '', 'Normal', 120),
(2, 'Price.com Matching', NULL, 'Normal', 120),
(3, 'Update Anyspecs', NULL, 'Normal', 120),
(4, 'New Product', NULL, 'Normal', 120),
(5, 'Purchase', NULL, 'Normal', 120),
(6, 'New Supplier', NULL, 'Normal', 120),
(8, 'Sourcing', NULL, 'Normal', 120),
(9, '採購跟進', NULL, 'Normal', 120),
(10, '維修跟進', NULL, 'Normal', 120),
(11, 'Yahoo Pricing', '', 'Normal', 120),
(12, 'New Brand', NULL, 'Normal', 120),
(13, '新產品描述及簡介', NULL, 'Normal', 120),
(14, '產品上架', NULL, 'Normal', 120),
(15, '產品上架(不用更新產品內容)', NULL, 'Normal', 120),
(16, '新產品描述及簡介', NULL, 'Normal', 120),
(17, '新產品描述及簡介 (Lemon)', NULL, 'Normal', 120),
(18, '訂單跟進', NULL, 'Normal', 120),
(19, '客服跟進', NULL, 'Normal', 120);

INSERT INTO `ecs_admin_task_templates_used` (`task_templates_used_id`, `user_id`, `template_id`, `frequency`, `last_modified_time`) VALUES
(1, 45, 4, 60, 1523241515),
(2, 45, 1, 47, 1523241515),
(3, 45, 13, 61, 1523241515),
(4, 45, 14, 45, 1523241515);
