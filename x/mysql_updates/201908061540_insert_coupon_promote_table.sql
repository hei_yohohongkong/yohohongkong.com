INSERT INTO `ecs_coupon_promote` (`promote_id`, `promote_name`, `promote_desc`, `promote_type`, `config`) VALUES ('3', 'PayMe Promotion', '請使用PayMe付款', '2', NULL);

INSERT INTO `ecs_coupon_promote_lang` (`promote_id`, `lang`, `promote_desc`) VALUES ('3', 'zh_cn', '请使用PayMe付款');

INSERT INTO `ecs_coupon_promote_lang` (`promote_id`, `lang`, `promote_desc`) VALUES ('3', 'en_us', 'Please choose PayMe for the payment');