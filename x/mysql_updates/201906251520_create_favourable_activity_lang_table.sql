/**
 * Author:  Dem
 * Created: 2019-06-25
 * Sql for favourable activity language
 */

CREATE TABLE `ecs_favourable_activity_lang` (
    `act_id` SMALLINT(5) NOT NULL ,
    `lang` CHAR(10) NOT NULL,
    `act_name` VARCHAR(255),
    UNIQUE (`act_id`, `lang`)
);

CREATE TABLE `ecs_goods_activity_lang` (
    `act_id` SMALLINT(5) NOT NULL ,
    `lang` CHAR(10) NOT NULL,
    `act_name` VARCHAR(255),
    `act_desc` TEXT,
    UNIQUE (`act_id`, `lang`)
);