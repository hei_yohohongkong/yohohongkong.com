ALTER TABLE `ecs_coupon_promote` ADD `promote_type` TINYINT NOT NULL DEFAULT '0' AFTER `promote_name`;
ALTER TABLE `ecs_coupon_promote` ADD `config` TEXT NULL AFTER `promote_type`;

ALTER TABLE `ecs_coupon_promote` ADD `promote_desc` TEXT NULL AFTER `promote_name`;
CREATE TABLE `ecs_coupon_promote_lang` (
    `promote_id` INT NOT NULL,
    `lang` CHAR(10) NOT NULL,
    `promote_desc` TEXT,
    PRIMARY KEY (`promote_id`, `lang`)
);
