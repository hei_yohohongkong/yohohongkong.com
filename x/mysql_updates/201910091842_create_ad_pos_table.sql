/**
 * Author:  Yan
 * Created: 2019-10-09
 */
CREATE TABLE `ecs_ad_pos` (
  `ad_pos_id` smallint(5) UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT ,
  `media_type` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `ad_name` varchar(60) NOT NULL DEFAULT '',
  `ad_file` text NOT NULL,
  `start_time` int(11) NOT NULL DEFAULT '0',
  `end_time` int(11) NOT NULL DEFAULT '0',
  `enabled` tinyint(3) UNSIGNED NOT NULL DEFAULT '1'
)