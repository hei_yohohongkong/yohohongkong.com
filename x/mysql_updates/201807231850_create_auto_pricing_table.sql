/**
 * Author:  Dem
 * Created: 2018-07-23
 * Sql for auto pricing
 */

CREATE TABLE `ecs_auto_pricing_strategy` (
    `strategy_id` INT NOT NULL AUTO_INCREMENT,
    `strategy_name` VARCHAR(255),
    `enable` INT DEFAULT 0,
    `type` INT DEFAULT 0,
    `config` TEXT,
    PRIMARY KEY(`strategy_id`)
);

CREATE TABLE `ecs_auto_pricing_suggestion` (
    `goods_id` INT NOT NULL,
    `strategy_id` INT NOT NULL,
    `suggest_price` DECIMAL(10, 2) NOT NULL,
    `suggest_date` DATETIME DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(`goods_id`)
);

CREATE TABLE `ecs_auto_pricing_rules` (
    `rule_id` INT NOT NULL AUTO_INCREMENT,
    `rule_name` VARCHAR(255),
    `type` INT DEFAULT 0,
    `enable` INT DEFAULT 0,
    `config` TEXT,
    PRIMARY KEY(`rule_id`)
);

ALTER TABLE `ecs_goods` ADD `price_strategy` VARCHAR(255);
ALTER TABLE `ecs_goods_log` ADD `price_strategy` VARCHAR(255);

INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '189', 'menu_auto_pricing', '');
INSERT INTO `ecs_shop_config` (`parent_id`, `code`, `type`, `value`) VALUES ('1', 'default_auto_price', 'hidden', '0');
INSERT INTO `ecs_shop_config` (`parent_id`, `code`, `type`, `value`) VALUES ('1', 'default_auto_price_dead', 'hidden', '0');