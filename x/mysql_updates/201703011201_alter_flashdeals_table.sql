/**
 * Author:  Anthony
 * Created: 2017-4-11
 */

ALTER TABLE `ecs_flashdeals` ADD `sort_order` TINYINT(3) UNSIGNED NOT NULL DEFAULT '50' AFTER `creator`;