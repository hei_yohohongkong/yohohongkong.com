ALTER TABLE `ecs_comment` ADD `image` TEXT NOT NULL;

INSERT INTO `ecs_shop_config` (`id`, `parent_id`, `code`, `type`, `store_range`, `store_dir`, `value`, `sort_order`) VALUES (10, '0', 'comment_config', 'group', '', '', '', '1');
update `ecs_shop_config` set parent_id = 10  WHERE `id` IN (227,214);
INSERT INTO `ecs_shop_config` (`id`, `parent_id`, `code`, `type`, `store_range`, `store_dir`, `value`, `sort_order`) VALUES (NULL, '10', 'comment_integral', 'text', '', '', '0', '1');
INSERT INTO `ecs_shop_config` (`id`, `parent_id`, `code`, `type`, `store_range`, `store_dir`, `value`, `sort_order`) VALUES (NULL , 10, 'comment_open', 'select', '0,1', '', '0', '1');
ALTER TABLE `ecs_comment` CHANGE `content` `content` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL;	