
CREATE TABLE `ecs_order_cross_check` (
  `order_sn` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_id` int(11) NOT NULL,
  `passed` tinyint(1) NOT NULL DEFAULT '0',
  `operator` int(11) DEFAULT NULL,
  `log` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `ecs_order_cross_check`
  ADD PRIMARY KEY (`order_sn`,`delivery_id`);


INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '189', 'menu_order_goods_cross_check', '');

ALTER TABLE `ecs_order_cross_check` ADD `operator` INT(11) NULL AFTER `passed`;
ALTER TABLE `ecs_order_cross_check` ADD `serial_number_scanned` BOOLEAN NOT NULL DEFAULT FALSE AFTER `operator`;
ALTER TABLE `ecs_order_cross_check` ADD `error` BOOLEAN NOT NULL DEFAULT FALSE AFTER `serial_number_scanned`;

ALTER TABLE `ecs_order_cross_check` ADD `last_update_time` INT(11) NULL AFTER `log`;
INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '189', 'menu_order_goods_cross_check_report', '');