/**
 * Author:  Dem
 * Created: 2018-12-13
 * Sql for crm system offline payment
 */

 CREATE TABLE `ecs_offline_payment` (
     `op_id` INT NOT NULL AUTO_INCREMENT,
     `user_id` INT NOT NULL,
     `order_id` INT NOT NULL,
     `pay_id` INT NOT NULL,
     `status` INT DEFAULT 0,
     `ticket_id` INT DEFAULT 0,
     `pay_account` INT DEFAULT -1,
     `expire_time` INT DEFAULT 0,
     `token` TEXT NOT NULL,
     `filetype` TEXT,
     PRIMARY KEY (`op_id`)
 );

INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '189', 'menu_op_system', '');
