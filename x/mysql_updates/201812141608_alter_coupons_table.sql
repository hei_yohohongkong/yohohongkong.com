
 ALTER TABLE `ecs_coupons` ADD `cat_list` LONGTEXT;

 ALTER TABLE `ecs_coupons` ADD `exclude_cat_list` LONGTEXT;

 ALTER TABLE `ecs_coupons` ADD `exclude_brand_list` LONGTEXT;

 ALTER TABLE `ecs_coupons` ADD `brand_list` LONGTEXT;

 ALTER TABLE `ecs_coupons` ADD `exclude_flashdeal` INT(1);