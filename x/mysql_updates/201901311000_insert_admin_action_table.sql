/**
 * Author:  Dem
 * Created: 2019-01-31
 * Sql for ga statistics analysis
 */

INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '189', 'menu_goods_stats', '');

CREATE TABLE `ecs_google_statistics_temporary_view` (
    `goods_id` INT NOT NULL,
    `cat_id` INT NOT NULL,
    `brand_id` INT NOT NULL,
    `session_7` INT NOT NULL,
    `pageview_7` INT NOT NULL,
    `pageview_unique_7` INT NOT NULL,
    `session_7to7` INT NOT NULL,
    `pageview_7to7` INT NOT NULL,
    `pageview_unique_7to7` INT NOT NULL,
    `session_30` INT NOT NULL,
    `pageview_30` INT NOT NULL,
    `pageview_unique_30` INT NOT NULL,
    `session_30to30` INT NOT NULL,
    `pageview_30to30` INT NOT NULL,
    `pageview_unique_30to30` INT NOT NULL,
    PRIMARY KEY (`goods_id`)
);