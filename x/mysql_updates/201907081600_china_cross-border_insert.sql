/**
 * Author:  Sing
 * Created: 2019-07-08
 */

ALTER TABLE `ecs_order_info` ADD `id_doc` VARCHAR(70) NOT NULL AFTER `email`;

ALTER TABLE `ecs_user_address` ADD `id_doc` VARCHAR(70) NOT NULL AFTER `consignee`;

ALTER TABLE `ecs_order_info` ADD `cn_ship_tax` DECIMAL(10,2) NOT NULL DEFAULT '0.00' AFTER `coupon`;
