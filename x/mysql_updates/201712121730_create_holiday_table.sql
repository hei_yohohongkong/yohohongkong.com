/**
 * Author:  Dem
 * Created: 2017-12-12
 * Create new table for storing public holidays
 */

CREATE TABLE `ecs_holidays` (
    `h_id` INT NOT NULL AUTO_INCREMENT,
    `year` CHAR(4) NOT NULL,
    `date` CHAR(10) NOT NULL,
    `info` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`h_id`),
    INDEX `holiday_sort` (`year` ASC, `date` ASC)
);

INSERT INTO `ecs_admin_action` (`action_id`, `parent_id`, `action_code`, `relevance`) VALUES (NULL, '189', 'menu_holidays', '');