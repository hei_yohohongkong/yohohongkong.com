/**
 * Author:  Dem
 * Created: 2018-08-09
 * Sql for require transformer
 */

ALTER TABLE `ecs_goods` ADD `require_goods_id` INT NOT NULL DEFAULT 0;
ALTER TABLE `ecs_goods_log` ADD `require_goods_id` INT NOT NULL DEFAULT 0;
