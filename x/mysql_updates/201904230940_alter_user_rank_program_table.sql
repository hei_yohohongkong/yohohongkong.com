/**
 * Author:  Dem
 * Created: 2019-04-46
 * Sql for user rank program
 */

ALTER TABLE `ecs_user_rank_program` ADD `email_banner` TEXT;
UPDATE `ecs_user_rank_program` SET `email_banner` = '<a href="https://yohohongkong.com/programs/student-discounts-and-education-offers"><img src="http://yohohongkong.com/images/upload/Image/1535603573601978152.png" border="0"></a>' WHERE `program_id` = 1;
UPDATE `ecs_mail_templates` SET `template_content` = '<table style="border: 1px solid #e7e7e7; margin-left: auto; margin-right: auto; width: 611px;" border="0">
<tbody>
<tr>
<td style="width: 39.5px;" colspan="3"><img usemap="#m_-5847510787762023062_imgmap2017929151620" class="CToWUd" src="https://yohohongkong.com/images/upload/Image/1507831950789963146.png" border="0" alt=""><map id="m_-5847510787762023062imgmap2017929151620" name="m_-5847510787762023062_imgmap2017929151620"> 
<area shape="circle" alt="" coords="477,53,13" href="https://www.facebook.com/yohohongkong" target="_blank">
 
<area shape="circle" alt="" coords="508,54,13" href="https://www.youtube.com/yohohongkong" target="_blank">
 
<area shape="circle" alt="" coords="540,55,13" href="https://www.instagram.com/yohohongkong/" target="_blank">
 
<area shape="circle" alt="" coords="571,55,13" href="https://plus.google.com/+yohohongkong" target="_blank">
 
<area shape="rect" alt="" coords="209,7,390,72" href="https://yohohongkong.com/" target="_blank">
 </map></td>
</tr>
<tr>
<td style="width: 39.5px;" colspan="3">你好，<br><br>多謝你參與 {$program.program_name}，請即按以下連結以啟動帳戶，謝謝。<br>{$verify_url}<br><br></td>
</tr>
<tr style="margin-top: 5px;">
<td style="width: 39.5px;" colspan="3"><span style="padding-left: 5px; color: #999999;">感謝您對友和的支持，希望您購物愉快！</span><br><br><span style="padding-left: 5px; color: #999999;">{$shop_name}</span><br><span style="padding-left: 5px; color: #999999;">{$send_date}</span></td>
</tr>
<tr>
<td style="width: 39.5px;" colspan="3">&nbsp;</td>
</tr>
<tr>
<td id="banner" style="width: 39.5px;" colspan="3">{$program.email_banner}</td>
</tr>
<tr>
<td style="width: 39.5px;" colspan="3"><img usemap="#imgmap2017929165943" src="http://yohohongkong.com/images/upload/Image/1506968194128151352.png" border="0"><map id="imgmap2017929165943" name="imgmap2017929165943"> 
<area shape="rect" alt="" coords="1,1,134,55" href="https://yohohongkong.com/cat/1-%E9%9B%BB%E5%AD%90%E7%94%A2%E5%93%81" target="">
 
<area shape="rect" alt="" coords="137,1,265,58" href="https://yohohongkong.com/cat/23-%E7%BE%8E%E5%AE%B9%E5%8F%8A%E8%AD%B7%E7%90%86" target="">
 
<area shape="rect" alt="" coords="269,0,378,57" href="http://www.yohohongkong.com/cat/44-電腦" target="">
 
<area shape="rect" alt="" coords="382,1,493,55" href="https://yohohongkong.com/cat/184-%E5%AE%B6%E5%BA%AD%E9%9B%BB%E5%99%A8" target="">
 
<area shape="rect" alt="" coords="496,1,601,56" href="https://yohohongkong.com/cat/171-生活時尚" target="">
 </map></td>
</tr>
<tr style="background: #00acee; color: white;">
<td style="width: 39.5px;" colspan="3">
<div class="footer-content" style="float: left; padding: 10px; color: white; font-size: 12px;">請勿回覆此電郵，您可以發電郵至<a style="color: #0645ad;" href="mailto:info@yohohongkong.com" target="_blank" rel="noopener noreferrer">info@yohohongkong.com</a>&nbsp;或使用<a href="https://yohohongkong.com/">yohohongkong.com</a>&nbsp;/ 致電 53356996 與我們保持聯絡。</div>
</td>
</tr>
</tbody>
</table>' WHERE `template_code` = 'program_verify_email';