<?php

define('IN_ECS', true);
require(dirname(__FILE__) .'/includes/init.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_base.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_warehouse.php');
require(dirname(__FILE__) .'/includes/ERP/cls/cls_date.php');
$luckyDrawController = new Yoho\cms\Controller\LuckyDrawController();
$adminuserController = new Yoho\cms\Controller\AdminuserController();

if ($_REQUEST['act'] == 'list' || $_REQUEST['act'] == '') {
    include('./includes/ERP/page.class.php');
    //print_R($luckyDrawController->getGiftTypeOptions());
    $smarty->assign('type_options', $luckyDrawController->getGiftTypeOptions());
    $smarty->assign('open_options', $luckyDrawController->getTicketOpenOptions());
    // $smarty->assign('status_options', $rmaController->getStatusOptions());
    // $smarty->assign('department_options', $rmaController->getDepartmentOptions());
    $luckyDrawController->checkLuckyDrawId();
    $lucky_draw_id = $_REQUEST['lucky_draw_id'];
    $smarty->assign('ur_here', '抽獎列表');
    $smarty->assign('full_page', 1);
    $smarty->assign('start_date', $result['start_date']);
    $smarty->assign('end_date', $result['end_date']);
    $smarty->assign('cfg_lang', $_CFG['lang']);
    $smarty->assign('lucky_draw_id', $lucky_draw_id);
    $smarty->assign('lang', $_LANG);
	$smarty->assign('action_link', array('text' => '獎品管理', 'href' => 'lucky_draw_manage.php?act=list&lucky_draw_id='.$lucky_draw_id));
    $smarty->assign('action_link2', array('text' => '返回活動列表', 'href' => 'lucky_draw.php?act=list'));
    $smarty->display('lucky_draw_list.htm');
} elseif ($_REQUEST['act'] == 'query') {
    $lucky_draw_id = $_REQUEST['lucky_draw_id'];
    $result = $luckyDrawController->getLuckyDrawTicketList($lucky_draw_id);
    $luckyDrawController->ajaxQueryAction($result['data'], $result['record_count'], false);
} elseif ($_REQUEST['act'] == 'post_confirm_pickup') {
    $ticket_id = $_REQUEST['ticket_id'];
    $result = $luckyDrawController->confirmPickup($ticket_id);
    if ($result !== false) {
        make_json_result($result);
    } else {
        make_json_error('更新時發生錯誤');
    }
} elseif ($_REQUEST['act'] == 'post_bulk_ticket_free_gift_notify') {
    $result = $luckyDrawController->bulkTicketFreeGiftNotify();
    if ($result !== false) {
        make_json_result($result);
    } else {
        make_json_error('更新時發生錯誤');
    }
} elseif ($_REQUEST['act'] == 'post_ticket_follow_up_remark_submit') {
    $result = $luckyDrawController->ticketFollowUpRemarkSubmit();
    if($result !== false){
        make_json_result($result);
    }else{
        make_json_error('更新時發生錯誤');
    }
} elseif ($_REQUEST['act'] == 'post_bulk_ticket_free_gift_create_order') {
    $result = $luckyDrawController->bulkTicketFreeGiftCreateOrder();
    if ($result !== false) {
        make_json_result($result);
    } else {
        make_json_error('更新時發生錯誤');
    }
}
