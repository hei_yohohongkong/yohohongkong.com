<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_base.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_warehouse.php');
require(dirname(__FILE__) .'/includes/ERP/cls/cls_date.php');

$warehouseTransferController = new Yoho\cms\Controller\WarehouseTransferController();

if($_REQUEST['act'] == 'list')
{
	if(admin_priv('erp_warehouse_view','',false) ||admin_priv('erp_warehouse_manage','',false) ||admin_priv('erp_warehouse_approve','',false))
	{
		$page=isset($_REQUEST['page']) &&intval($_REQUEST['page']) >0 ?intval($_REQUEST['page']):1;
		$goods_sn=isset($_REQUEST['goods_sn'])?trim($_REQUEST['goods_sn']):'';
		$goods_name=isset($_REQUEST['goods_name'])?trim($_REQUEST['goods_name']):'';
		$warehouse_from=isset($_REQUEST['w_f']) ?trim($_REQUEST['w_f']) : 0;
		$warehouse_to=isset($_REQUEST['w_t']) &&intval($_REQUEST['w_t'])>0 ?intval($_REQUEST['w_t']):0;
		$admin_from=isset($_REQUEST['a_f']) ?trim($_REQUEST['a_f']) : 0;
		$admin_to=isset($_REQUEST['a_t']) &&intval($_REQUEST['a_t'])>0 ?intval($_REQUEST['a_t']):0;
		$status=isset($_REQUEST['status']) &&intval($_REQUEST['status'])>0?intval($_REQUEST['status']):0;
		$start_date=isset($_REQUEST['s_date'])?trim($_REQUEST['s_date']):'';
		$end_date=isset($_REQUEST['e_date'])?trim($_REQUEST['e_date']):'';
		include('./includes/ERP/page.class.php');
		$num_per_page=20;
		$mode=1;
		$page_bar_num=6;
		$page_style="page_style";
		$current_page_style="current_page_style";
		$start=$num_per_page*($page-1);
		$cls_date=new cls_date();
		$start_time=!empty($start_date)?$cls_date->date_to_stamp($start_date): 0;
		$end_time=!empty($end_date)?$cls_date->date_to_stamp($end_date):0;
		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		$paras=array(
			'goods_sn'=>$goods_sn,
			'goods_name'=>$goods_name,
			'warehouse_from'=>$warehouse_from,
			'warehouse_to'=>$warehouse_to,
			'admin_from'=>$admin_from,
			'admin_to'=>$admin_to,
			'status'=>$status,
			'start_time'=>$start_time,
			'end_time'=>$end_time,
			'agency_id'=>$agency_id
		);
		$total_num=get_stock_transfer_count($paras);
		$stock_transfer_list=get_stock_transfer_list($paras,$start,$num_per_page);
		$smarty->assign('stock_transfer_list',$stock_transfer_list);
		$url=fix_url($_SERVER['REQUEST_URI'],array('page'=>''));
		$pager=new page(array('total_data'=>$total_num,'data_per_page'=>$num_per_page,'url'=>$url,'mode'=>$mode,'page_bar_num'=>$page_bar_num,'page_style'=>$page_style,'current_page_style'=>$current_page_style));
		$smarty->assign('pager',$pager->show());
		$smarty->assign('goods_sn',$goods_sn);
		$smarty->assign('goods_name',$goods_name);
		$smarty->assign('warehouse_from',$warehouse_from);
		$smarty->assign('warehouse_to',$warehouse_to);
		$smarty->assign('admin_from',$admin_from);
		$smarty->assign('admin_to',$admin_to);
		$smarty->assign('status',$status);
		$smarty->assign('start_date',$start_date);
		$smarty->assign('end_date',$end_date);
		$smarty->assign('admin_id',erp_get_admin_id());
		$warehouse_list=get_warehouse_list(0,0,1);
		$smarty->assign('warehouse_list',$warehouse_list);
		$smarty->assign('admin_list',warehouse_admin_list());
		$smarty->assign('transfer_status_list',get_goods_transfer_status());
		if((admin_priv('erp_warehouse_manage','',false)))
		{
			$action_link = array('href'=>'javascript: add_transfer();','text'=>$_LANG['erp_add_stock_transfer']);
			$smarty->assign('action_link',$action_link);
		}
		$smarty->assign('ur_here',$_LANG['erp_stock_transfer']);
		assign_query_info();
		$smarty->display('erp_stock_transfer_list.htm');
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif ($_REQUEST['act'] == 'view'||$_REQUEST['act'] == 'print')
{
	if(admin_priv('erp_warehouse_view','',false) ||admin_priv('erp_warehouse_manage','',false) ||admin_priv('erp_warehouse_approve','',false))
	{
		$transfer_id=isset($_REQUEST['transfer_id']) &&intval($_REQUEST['transfer_id'])>0 ?intval($_REQUEST['transfer_id']) : 0;
		if($transfer_id==0){
			$sql="select transfer_id from ".$GLOBALS['ecs']->table('erp_stock_transfer')." WHERE transfer_sn='".$_REQUEST['transfer_sn']."' order by transfer_id desc limit 1";
	$transfer_id=$GLOBALS['db']->getOne($sql);
		}
		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		if(!is_agency_transfer($transfer_id,$agency_id))
		{
			$href="erp_stock_transfer_manage.php?act=list";
			$text=$_LANG['erp_stock_transfer_return_to_list'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_stock_transfer_no_agency_access'],0,$link);
		}

		$transfer_info = $warehouseTransferController->getTransferInfo($_REQUEST['transfer_id']);
		//$transfer_info=get_goods_transfer_info($transfer_id);
		$transfer_item_info=get_goods_transfer_item_info($transfer_id);
		$smarty->assign('transfer_info',$transfer_info);
		$smarty->assign('transfer_item_info',$transfer_item_info);
		$smarty->assign('act','view');
		$smarty->assign('ur_here',$GLOBALS['_LANG']['erp_stock_transfer_view']);
		assign_query_info();
		if($_REQUEST['act'] == 'view')
		{
			$smarty->display('erp_stock_transfer_info.htm');
		}
		elseif($_REQUEST['act'] == 'print')
		{
			$smarty->display('erp_stock_transfer_print.htm');
		}
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif($_REQUEST['act'] == 'edit')
{
	if((admin_priv('erp_warehouse_manage','',false)))
	{
		$transfer_id=isset($_REQUEST['transfer_id']) &&intval($_REQUEST['transfer_id'])>0 ?intval($_REQUEST['transfer_id']) : 0;
		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		if(!is_agency_transfer($transfer_id,$agency_id))
		{
			$href="erp_stock_transfer_manage.php?act=list";
			$text=$_LANG['erp_stock_transfer_return_to_list'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_stock_transfer_no_agency_access'],0,$link);
		}
		if(!is_admin_stock_transfer($transfer_id,erp_get_admin_id()))
		{
			$href="erp_stock_transfer_manage.php?act=list";
			$text=$_LANG['erp_stock_transfer_reurn'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_stock_transfer_no_access'],0,$link);
		}
		if(lock_stock_transfer_table($transfer_id,'edit'))
		{
			$warehouse_from=get_warehouse_list(erp_get_admin_id(),$agency_id,1);
			$smarty->assign('warehouse_from',$warehouse_from);
			$warehouse_to=get_warehouse_list(0,0,1);
			$smarty->assign('warehouse_to',$warehouse_to);
			$smarty->assign('url',$_SERVER["REQUEST_URI"]);
			$transfer_info=get_goods_transfer_info($transfer_id);
			$transfer_item_info=get_goods_transfer_item_info($transfer_id);
			$smarty->assign('transfer_info',$transfer_info);
			$smarty->assign('transfer_item_info',$transfer_item_info);
			if($transfer_info['status']==3)
			{
				$href="erp_stock_transfer_manage.php?act=print&transfer_id=".$transfer_id;
				$text=$_LANG['erp_order_operation_print'];
				$action_link = array('href'=>$href,'text'=>$text);
				$smarty->assign('action_link',$action_link);
			}
			$smarty->assign('act','edit');
			$smarty->assign('ur_here',$GLOBALS['_LANG']['erp_stock_transfer_edit']);
			assign_query_info();
			$smarty->display('erp_stock_transfer_info.htm');
		}
		else
		{
			$href="erp_stock_transfer_manage.php?act=list";
			$text=$_LANG['erp_stock_transfer_reurn'];
			$link[] = array('href'=>$href,'text'=>$text);
			$href="erp_stock_transfer_manage.php?act=view&transfer_id=".$transfer_id;
			$text=$_LANG['erp_stock_transfer_view'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_order_no_accessibility'],0,$link);
		}
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif($_REQUEST['act'] == 'confirm_transfer')
{
	if((admin_priv('erp_warehouse_manage','',false)))
	{
		$transfer_id=isset($_REQUEST['transfer_id']) &&intval($_REQUEST['transfer_id'])>0 ?intval($_REQUEST['transfer_id']) : 0;
		if(empty($transfer_id))
		{
			$href="erp_stock_transfer_manage.php?act=list";
			$text=$_LANG['erp_stock_transfer_reurn'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_wrong_parameter'],0,$link);
		}
		$transfer_info=is_stock_transfer_exist($transfer_id);
		if(!$transfer_info)
		{
			$href="erp_stock_transfer_manage.php?act=list";
			$text=$_LANG['erp_stock_transfer_reurn'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_stock_transfer_not_exists'],0,$link);
		}
		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		if(!is_agency_transfer($transfer_id,$agency_id))
		{
			$href="erp_stock_transfer_manage.php?act=list";
			$text=$_LANG['erp_stock_transfer_return_to_list'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_stock_transfer_no_agency_access'],0,$link);
		}
		if($transfer_info['status']!=2)
		{
			$href="erp_stock_transfer_manage.php?act=list";
			$text=$_LANG['erp_stock_transfer_reurn'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_stock_transfer_no_acts'],0,$link);
		}
		if(!is_admin_warehouse($transfer_info['warehouse_to'],erp_get_admin_id()))
		{
			$href="erp_stock_transfer_manage.php?act=list";
			$text=$_LANG['erp_stock_transfer_reurn'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_stock_transfer_no_confirm_accessibility'],0,$link);
		}
		if(lock_stock_transfer_table($transfer_id,'confirm'))
		{
			$warehouse_from=get_warehouse_list(erp_get_admin_id(),0,1);
			$smarty->assign('warehouse_from',$warehouse_from);
			$warehouse_to=get_warehouse_list(0,0,1);
			$smarty->assign('warehouse_to',$warehouse_to);
			$smarty->assign('url',$_SERVER["REQUEST_URI"]);
			$transfer_info=get_goods_transfer_info($transfer_id);
			$transfer_item_info=get_goods_transfer_item_info($transfer_id);
			$smarty->assign('transfer_info',$transfer_info);
			$smarty->assign('transfer_item_info',$transfer_item_info);
			if($transfer_info['status']==3)
			{
				$href="erp_stock_transfer_manage.php?act=print&transfer_id=".$transfer_id;
				$text=$_LANG['erp_order_operation_print'];
				$action_link = array('href'=>$href,'text'=>$text);
				$smarty->assign('action_link',$action_link);
			}
			$smarty->assign('act','confirm_transfer');
			$smarty->assign('ur_here',$GLOBALS['_LANG']['erp_stock_transfer_edit']);
			assign_query_info();
			$smarty->display('erp_stock_transfer_info.htm');
		}
		else
		{
			$href="erp_stock_transfer_manage.php?act=list";
			$text=$_LANG['erp_stock_transfer_reurn'];
			$link[] = array('href'=>$href,'text'=>$text);
			$href="erp_stock_transfer_manage.php?act=view&transfer_id=".$transfer_id;
			$text=$_LANG['erp_stock_transfer_view'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_order_no_accessibility'],0,$link);
		}
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
?>