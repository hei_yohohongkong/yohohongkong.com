<?php
/**
 * Created by Anthony.
 * User: Anthony
 * Date: 2018/9/10
 * Time: 下午 06:28
 */


define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require(ROOT_PATH . 'languages/' . $_CFG['lang'] . '/admin/shipping.php');
$shippingController = new \Yoho\cms\Controller\ShippingController();

if ($_REQUEST['act'] == 'list') {
    $shippingController->shippingTypeListAction();
} elseif ($_REQUEST['act'] == 'query') {
    $shippingController->shippingTypeAjaxQueryAction();
} elseif ($_REQUEST['act'] == 'add' || $_REQUEST['act'] == 'edit') {
    $shippingController->shippingTypeEditAction();
} elseif ($_REQUEST['act'] == 'insert' || $_REQUEST['act'] == 'update') {
    $shippingController->shippingTypeUpdateAction();
}