<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_base.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_warehouse.php');
require(dirname(__FILE__) .'/includes/ERP/cls/cls_date.php');
$luckyDrawController = new Yoho\cms\Controller\LuckyDrawController();
$adminuserController = new Yoho\cms\Controller\AdminuserController();

if ($_REQUEST['act'] == 'list' || $_REQUEST['act'] == '') {

    $luckyDrawController->checkLuckyDrawId();
    $lucky_draw_id = $_REQUEST['lucky_draw_id'];

    // get all gift by levle
    $all_gifts_chart_data = $luckyDrawController->getLuckyDrawAllGiftsChartData($lucky_draw_id);
    
//echo '<pre>'; 
//Print_r($all_gifts_chart_data);
//exit;
    $smarty->assign('lucky_draw_list',$lucky_draw_list);
    $smarty->assign('channel',$rma_basic_info['channel']);
    $smarty->assign('lucky_draw_id',$lucky_draw_id);
    $smarty->assign('step',$step);
    $smarty->assign('full_page', 1);
    $smarty->assign('cfg_lang', $_CFG['lang']);
    $smarty->assign('lang', $_LANG);
    $smarty->assign('action_link', array('text' => '返回中獎列表', 'href' => 'lucky_draw_detail.php?act=list&lucky_draw_id='.$lucky_draw_id));
    $smarty->assign('all_gifts_chart_data', $all_gifts_chart_data);

    assign_query_info();
    $smarty->display('lucky_draw_manage.htm');
} else if ($_REQUEST['act'] == 'post_change_gift_level_submit') {
    $result = $luckyDrawController->changeGiftLevelSubmit();
    if (isset($result['error'])) {
        make_json_error($result['error']);
    }else{
        make_json_result($result);
    }
} elseif ($_REQUEST['act'] == 'query') {
    $result = $luckyDrawController->getLuckyDrawGiftList();
    $luckyDrawController->ajaxQueryAction($result['data'],$result['record_count'],false);
}
?>