<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_base.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_warehouse.php');
require(dirname(__FILE__) .'/includes/ERP/cls/cls_date.php');

$adminuserController = new Yoho\cms\Controller\AdminuserController();
$shopController = new Yoho\cms\Controller\ShopController();
$erpController = new Yoho\cms\Controller\ErpController();
$warehouseTransferController = new Yoho\cms\Controller\warehouseTransferController();

if($_REQUEST['act'] == 'list' || $_REQUEST['act'] == ''){
	if( admin_priv('erp_warehouse_view','',false) || admin_priv('erp_warehouse_manage','',false) || admin_priv('erp_warehouse_approve','',false))
	{
        include('./includes/ERP/page.class.php');
		$shop_warehouse_id = 1; // shop
		$main_warehouse_id = $erpController->getMainWarehouseId();
		$opeator_options = $shopController->getOperatorOptions();
		$sellable_warehouse_info = $erpController->getSellableWarehouseInfo();

		$reason_type_options = $warehouseTransferController->getReasonTypeOptions();
		$reason_type_options_filtered = [];
		foreach ($reason_type_options as $key => $options) {
			if ($key == $warehouseTransferController::REASON_TYPE_STORE_PICK_UP || $key == $warehouseTransferController::REASON_TYPE_ORDER_OUT_OF_STOCK) {
				$reason_type_options_filtered[$key] = $options;
			}
		}

		$smarty->assign('reason_type_options_filtered', $reason_type_options_filtered);
		$smarty->assign('shop_warehouse_id',      $shop_warehouse_id);
		$smarty->assign('main_warehouse_id',      $main_warehouse_id);
		$smarty->assign('ur_here',      '訂單調貨紀錄');
		$smarty->assign('opeator_options', $opeator_options);
		$smarty->assign('sellable_warehouse_info', $sellable_warehouse_info);
        $smarty->assign('full_page', 1);
        $smarty->assign('start_date', $result['start_date']);
        $smarty->assign('end_date', $result['end_date']);
        $smarty->assign('cfg_lang', $_CFG['lang']);
        $smarty->assign('lang', $_LANG);
		$smarty->assign('action_link3', array('href' => 'erp_shop_to_warehouse_transfer.php?act=list', 'text' => '返回訂單調貨清單'));
        $smarty->display('erp_shop_pickup_transfer_history_list.htm');
    }else{
        $href="index.php";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
    }

} elseif ($_REQUEST['act'] == 'query') {
   $result = $shopController->getShopToWarehouseOrderGoodsTransferHistoryList();
   $shopController->ajaxQueryAction($result['data'],$result['record_count'],false);
}



?>