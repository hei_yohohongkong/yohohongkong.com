<?php
/* @Author: Anthony
* @Date:   2017-05-16T15:53:48+08:00
* @Filename: hktvmall.php
*/

define('IN_ECS', true);
require(dirname(__FILE__) . '/includes/init.php');

$hktvmallController = new Yoho\cms\Controller\HktvmallController();

use Yoho\cms\Model;

/*------------------------------------------------------ */
//-- List HKTVMALL item
/*------------------------------------------------------ */
if ($_REQUEST['act'] == 'list')
{
	$hktvmallController->listAction();
}
/*------------------------------------------------------ */
//-- Add HKTVMALL item
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'add')
{
    /* Check admin priv */
    admin_priv($hktvmallController::DB_ACTION_CODE_HKTVMALL);
    
    $smarty->assign('ur_here', $_LANG['hktvmall']);
    $smarty->assign('action_link', array(
        'text' => '返回列表', 'href' => 'hktvmall.php?act=list')
    );
    $smarty->assign('default_rate', $hktvmallController::DEFAULT_RATE);
    $smarty->assign('act', $_REQUEST['act']);
    $smarty->assign('cfg_lang', $_CFG['lang']);
    $smarty->assign('form_action', 'insert');
    $smarty->assign('goods_cat_list', cat_list());
    $smarty->assign('brand_list',     get_brand_list());
    
    assign_query_info();
    
    $smarty->display('hktvmall_info.htm');
}
/*------------------------------------------------------ */
//-- Insert HKTVMALL item
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'insert')
{
    /* Check admin priv */
    admin_priv($hktvmallController::DB_ACTION_CODE_HKTVMALL);
    
    /* Insert HKTV function */
    $result = $hktvmallController->insert_hktv_goods($_POST['hktv_rate'], $_POST['target_select']);
    if(!$result) return false;
    
    /* Clear cache */
    clear_cache_files();

	$link[0]['text'] = '返回列表';
	$link[0]['href'] = 'hktvmall.php?act=list';

	sys_msg('新增HKTVMall商品成功', 0, $link, true);
}
/*------------------------------------------------------ */
//-- List-Ajax : Query HKTVMall item
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'query')
{
    $list = $hktvmallController->get_hktv_list();
    $hktvmallController->ajaxQueryAction($list['data'], $list['record_count']);

}
/*------------------------------------------------------ */
//-- List-Ajax : Edit HKTVMall item quantity
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'edit_qty')
{
    check_authz_json($hktvmallController::DB_ACTION_CODE_HKTVMALL);
    
    $id  = intval($_POST['id']);
    $qty = intval($_POST['val']);
    
    if ($qty < 0 || $qty == 0 && $_POST['val'] != "$qty")
	{
		make_json_error('err');
	} else {
        $hktvmallController->ajax_edit_hktv($id, 'hktv_qty', $qty);
    }
}
/*------------------------------------------------------ */
//-- List-Ajax : Edit HKTVMall item brand name
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'edit_brand_name')
{
    check_authz_json($hktvmallController::DB_ACTION_CODE_HKTVMALL);
    
    $id  = intval($_POST['id']);
    $brand = $_POST['val'];
    
    if (!isset($brand) || preg_match('/^(N/A)$/', $brand))
	{
		make_json_error('err');
	} else {
        $hktvmallController->ajax_edit_hktv($id, 'hktv_brand', $brand);
    }
}

elseif ($_REQUEST['act'] == 'ajaxEdit') {
	$hktvmallController->ajaxEditAction();
}
/*------------------------------------------------------ */
//-- List-Ajax : Edit HKTVMall item rate
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'edit_rate')
{
    check_authz_json($hktvmallController::DB_ACTION_CODE_HKTVMALL);
    
    $id  = intval($_POST['id']);
    $rate = $_POST['val'];
    
    if (!isset($rate) || !preg_match('/^([0-9]+%)$/', $rate))
	{
		make_json_error($_LANG['hktv_edit_invalid']);
	} else {
        $hktvmallController->ajax_edit_hktv($id, 'hktv_rate', $rate);
    }
}
/*------------------------------------------------------ */
//-- List-Ajax : Delete HKTVMall item
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'remove')
{
	check_authz_json($hktvmallController::DB_ACTION_CODE_HKTVMALL);

	$id = intval($_GET['id']);
    $hktvmall = new Model\Hktvmall($id);

	if ($hktvmall->delHktv())
	{
		clear_cache_files();
	}
	$url = 'hktvmall.php?act=query&' . str_replace('act=remove', '', $_SERVER['QUERY_STRING']);

	ecs_header("Location: $url\n");
	exit;
}
/*------------------------------------------------------ */
//-- Info-Ajax : Search goods item
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'get_goods_list')
{
	$hktvmallController->search_goods($_GET['JSON']);
}
/*------------------------------------------------------ */
//-- Batch action
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'batch')
{
    /* Check admin priv */
    admin_priv($hktvmallController::DB_ACTION_CODE_HKTVMALL);

    /* Input handle */
	$ids = $_POST['id'];
    $type = $_POST['type'];
    
	if (isset($_POST['type']))
	{
        $hktvmallController->batch($type, $ids);
	}
    
}
/*------------------------------------------------------ */
//-- Export update price CSV
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'export')
{
    /* Check admin priv */
    admin_priv($hktvmallController::DB_ACTION_CODE_HKTVMALL);

    $hktvmallController->export_csv_hktv(array() , $_REQUEST['type']);

}
