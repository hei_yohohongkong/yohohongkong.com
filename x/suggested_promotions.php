<?php

/***
* goods_feedback.php
* by howang 2015-06-25
*
* View goods feedback from customers
***/

define('IN_ECS', true);
require(dirname(__FILE__) . '/includes/init.php');


if ($_REQUEST['act'] == 'list')
{
    $list = get_suggested_promotions();
    $smarty->assign('ur_here',      $_LANG['11_suggested_promotions']);
    $smarty->assign('full_page',    1);

    $smarty->assign('list',         $list['data']);
    $smarty->assign('filter',       $list['filter']);
    $smarty->assign('record_count', $list['record_count']);
    $smarty->assign('page_count',   $list['page_count']);
    
    // $sql = "SELECT `sales_name` as `name`, `sales_name` as `value` " .
    //         "FROM " . $ecs->table('salesperson') .
    //         "WHERE `available` = 1 ";
    // $operators = $db->getAll($sql);
    // array_unshift($operators, array('name'=>'請選擇...', 'value'=>''));
    // $smarty->assign('operators', $operators);
    
    $sort_flag  = sort_flag($list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);
    
    assign_query_info();
    $smarty->display('suggested_promotion_list.htm');
}
elseif ($_REQUEST['act'] == 'query')
{
    $list = get_suggested_promotions();
    $smarty->assign('list',         $list['data']);
    $smarty->assign('filter',       $list['filter']);
    $smarty->assign('record_count', $list['record_count']);
    $smarty->assign('page_count',   $list['page_count']);

    $sort_flag  = sort_flag($list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);

    make_json_result($smarty->fetch('suggested_promotion_list.htm'), '', array('filter' => $list['filter'], 'page_count' => $list['page_count']));
}
elseif ($_REQUEST['act'] == 'edit_suggested_price')
{
    $suggest_id = intval($_POST['id']);
    $value = doubleval(trim($_POST['val']));
    
    $db->query("UPDATE " . $ecs->table('suggested_promotions') . " " .
                "SET `suggested_price` = '" . $value . "' " .
                "WHERE `suggest_id` = '" . $suggest_id . "'");
    
    make_json_result(price_format($value));
}
elseif ($_REQUEST['act'] == 'edit_start_time')
{
    $suggest_id = intval($_POST['id']);
    $value = trim($_POST['val']);
    $time = strtotime($value);
    
    $db->query("UPDATE " . $ecs->table('suggested_promotions') . " " .
                "SET `start_time` = '" . $time . "' " .
                "WHERE `suggest_id` = '" . $suggest_id . "'");
    
    make_json_result(stripslashes($value));
}
elseif ($_REQUEST['act'] == 'edit_end_time')
{
    $suggest_id = intval($_POST['id']);
    $value = trim($_POST['val']);
    $time = strtotime($value);
    
    $db->query("UPDATE " . $ecs->table('suggested_promotions') . " " .
                "SET `end_time` = '" . $time . "' " .
                "WHERE `suggest_id` = '" . $suggest_id . "'");
    
    make_json_result(stripslashes($value));
}
elseif ($_REQUEST['act'] == 'accept_suggestion')
{
    $suggest_id = intval($_GET['id']);
    
    $sql = "UPDATE " . $ecs->table('suggested_promotions') . " as sp " .
                "INNER JOIN " . $ecs->table('goods') . " as g ON g.goods_id = sp.goods_id " .
                "SET sp.`status` = 'accepted', g.`is_promote` = '1', g.`promote_price` = sp.`suggested_price`, " .
                    "g.`promote_start_date` = sp.`start_time`, g.`promote_end_date` = sp.`end_time` + 86400 " .
                "WHERE sp.`suggest_id` = '" . $suggest_id . "'";
    $db->query($sql);
    
    clear_cache_files();
    
    $url = 'suggested_promotions.php?' . str_replace('act=' . $_REQUEST['act'], 'act=query', $_SERVER['QUERY_STRING']);
    
    header('Location: ' . $url);
    exit;
}
elseif ($_REQUEST['act'] == 'ignore_suggestion')
{
    $suggest_id = intval($_GET['id']);
    
    $db->query("UPDATE " . $ecs->table('suggested_promotions') . " " .
                "SET `status` = 'ignored' " .
                "WHERE `suggest_id` = '" . $suggest_id . "'");
    
    $url = 'suggested_promotions.php?' . str_replace('act=' . $_REQUEST['act'], 'act=query', $_SERVER['QUERY_STRING']);
    
    header('Location: ' . $url);
    exit;
}
elseif ($_REQUEST['act'] == 'accept_all_suggestions')
{
    $sql = "UPDATE " . $ecs->table('suggested_promotions') . " as sp " .
                "INNER JOIN " . $ecs->table('goods') . " as g ON g.goods_id = sp.goods_id " .
                "SET sp.`status` = 'accepted', g.`is_promote` = '1', g.`promote_price` = sp.`suggested_price`, " .
                    "g.`promote_start_date` = sp.`start_time`, g.`promote_end_date` = sp.`end_time` + 86400 " .
                "WHERE sp.`status` = 'pending'";
    $db->query($sql);
    
    clear_cache_files();
    
    make_json_result(true);
}
elseif ($_REQUEST['act'] == 'ignore_all_suggestions')
{
    $db->query("UPDATE " . $ecs->table('suggested_promotions') . " " .
                "SET `status` = 'ignored' " .
                "WHERE `status` = 'pending'");
    
    make_json_result(true);
}

function get_suggested_promotions()
{
    $filter['sort_by']      = empty($_REQUEST['sort_by']) ? 'suggest_id' : trim($_REQUEST['sort_by']);
    $filter['sort_order']   = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);

    $sql = "SELECT count(*) FROM " . $GLOBALS['ecs']->table('suggested_promotions') . " WHERE `status` = 'pending'";
    $filter['record_count'] = $GLOBALS['db']->getOne($sql);

    /* 分页大小 */
    $filter = page_and_size($filter);
    
    /* 查询 */
    $sql = "SELECT sp.`suggest_id`, sp.`suggested_price`, sp.`start_time`, sp.`end_time`, sp.`status`, g.`goods_sn`, g.`goods_name`, g.`goods_thumb`, g.`shop_price`, g.`cost` " .
            "FROM " . $GLOBALS['ecs']->table('suggested_promotions') . " as sp " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON g.goods_id = sp.goods_id " .
            "WHERE sp.status = 'pending' " .
            "ORDER BY " . $filter['sort_by'] . " " . $filter['sort_order'] . " " .
            "LIMIT " . $filter['start'] . "," . $filter['page_size'];
    $data = $GLOBALS['db']->getAll($sql);
    
    foreach ($data as $key => $row)
    {
        $data[$key]['shop_price_formatted'] = price_format($row['shop_price']);
        $data[$key]['cost_formatted'] = price_format($row['cost']);
        //$data[$key]['suggested_price'] = round(((double)$row['shop_price'] - (double)$row['cost']) * 0.75 + (double)$row['cost']);
        $data[$key]['suggested_price_formatted'] = price_format($data[$key]['suggested_price']);
        $data[$key]['promote_start_date'] = local_date('Y-m-d', $row['start_time']);
        $data[$key]['promote_end_date'] = local_date('Y-m-d', $row['end_time']);
    }
    
    $arr = array('data' => $data, 'filter' => $filter, 'page_count' => $filter['page_count'], 'record_count' => $filter['record_count']);
    
    return $arr;
}

?>