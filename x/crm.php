<?php
/**
 * Gmail:   thread_id   -> ref_id
 *          mail_id     -> unique_id
 * 
 * Chartra: chat_id     -> ref_id
 *          message_id  -> unique_id
 */
use function GuzzleHttp\json_decode;

define('IN_ECS', true);

require dirname(__FILE__) . '/includes/init.php';
require_once ROOT_PATH . 'includes/lib_order.php';
require_once ROOT_PATH . 'includes/Google_api_client/vendor/autoload.php';
require_once ROOT_PATH . 'languages/zh_tw/admin/order.php';
require_once ROOT_PATH . 'languages/zh_tw/admin/crm.php';

$crmController = new Yoho\cms\Controller\CrmController();
$crmController->set_crm_ivr_type();
$smarty->assign('lang', $_LANG);

$user = $crmController::GMAIL_USER;
$chatra_url = "https://app.chatra.io/api/";
$aircall_url = "https://api.aircall.io/v1/";
if (file_exists($crmController::SECRET_KEY_LOCATION)) {
    $contents = file_get_contents($crmController::SECRET_KEY_LOCATION);
    $secrets = \json_decode($contents, true);
} else {
    make_json_error("無法連接 API");
}

if ($_REQUEST['act'] == 'email' || $_REQUEST['act'] == 'draft') {
    $client = new Google_Client();
    $client->setApplicationName("Yoho Gmail API");
    $client->setAuthConfig($crmController::KEY_FILE_LOCATION);
    $client->setScopes(['https://mail.google.com/']);
    $client->setSubject($user);

    if (isset($_SESSION['google_access_token']) && isset($_SESSION['google_expiry']) && intval($_SESSION['google_expiry']) > gmtime()) {
        $client->setAccessToken($_SESSION['google_access_token']);
    } else {
        $client->useApplicationDefaultCredentials();
        $client->fetchAccessTokenWithAssertion();
        
        $token = $client->getAccessToken();
        $expiry = gmtime() + $token['expires_in'];
        $_SESSION['google_access_token'] = $token;
        $_SESSION['google_expiry'] = $expiry;
    }
    $service = new Google_Service_Gmail($client);
}

if ($_REQUEST['act'] == 'list') {
    $crm_js = addCrmJsFiles();
    $smarty->assign('crm_js', $crm_js);
    $smarty->assign('ur_here', $_LANG['crm_system']);
    $smarty->assign('action_link_list', [['href' => 'javascript:searchOfflinePayment()', 'text' => '線下付款']]);
    $smarty->display('crm.htm');
} elseif ($_REQUEST['act'] == 'info') {
    $user_name = empty($_REQUEST['user_name']) ? '' : preg_replace("/[\+\s-]/", "", trim($_REQUEST['user_name']));
    $order_sn = empty($_REQUEST['order_sn']) ? '' : trim($_REQUEST['order_sn']);
    $ticket_id = empty($_REQUEST['ticket_id']) ? '' : trim($_REQUEST['ticket_id']);
    $order_sort = "ORDER BY CASE WHEN order_status " . db_create_in([OS_CANCELED, OS_INVALID, OS_RETURNED]) . " THEN 999 " .
                  "WHEN pay_status = " . PS_UNPAYED . " THEN 0 " .
                  "WHEN pay_status = " . PS_PAYED . " AND shipping_status NOT " . db_create_in([SS_SHIPPED, SS_RECEIVED]) . " THEN 1 " .
                  "WHEN pay_status = " . PS_PAYED . " THEN 2 " .
                  "WHEN order_status = " . OS_WAITING_REFUND . " THEN 3 " .
                  "ELSE 4 END ASC, " .
                  "CASE WHEN shipping_status = " . SS_RECEIVED . " THEN 999 " .
                  "WHEN shipping_status = " . SS_SHIPPED . " THEN 998 " .
                  "ELSE shipping_status END ASC, " .
                  "CASE WHEN money_paid > 0 THEN 1 ELSE 0 END ASC, " .
                  "add_time DESC ";
    $user = [];
    $users = [];
    $order = [];
    $orders = [];
    $tab = "basic_info";
    if (!empty($order_sn)) {
        $sql_order = "SELECT * FROM " . $ecs->table('order_info') . " WHERE order_sn LIKE '%$order_sn%'";
        $orders = $db->getAll($sql_order);
        if (count($orders) == 1) {
            $order = processOrder($orders[0]['order_id']);
        }
        $orders = translateStatus($orders);        
    } elseif (!empty($user_name)) {
        $sql_user = "SELECT * FROM " . $ecs->table('users') . " WHERE REPLACE(user_name, '-', '') LIKE '%$user_name%'";
        $users = $db->getAll($sql_user);
        if (count($users) == 1) {
            $user = processUser($users[0]);
        }
    } elseif (!empty($ticket_id)) {
        $sql_crm = "SELECT * FROM " . $ecs->table('crm_ticket_info') . " WHERE ticket_id = $ticket_id";
        $crm_list = processCRMList($db->getAll($sql_crm));
        if (!empty($crm_list)) {
            $crm = $crm_list[0];
            $user_id = $crm['user_id'];
            $order_id = $crm['order_id'];
            if (!empty($user_id)) {
                $sql_user = "SELECT * FROM " . $ecs->table('users') . " WHERE user_id = $user_id";
                $user = processUser($db->getRow($sql_user));
            } elseif (!empty($order_id)) {
                $order = processOrder($order_id);
            }
        }
        $smarty->assign('crm_list', [$crm]);
        $tab = 'crm_info';
    }
    $smarty->assign('tab', $tab);
    $smarty->assign('lang', $_LANG);
    $smarty->assign('user', $user);
    $smarty->assign('order', $order);
    $smarty->assign('users', $users);
    $smarty->assign('orders', $orders);
    $smarty->assign('user_name', $_REQUEST['user_name']);
    $smarty->assign('order_sn', $_REQUEST['order_sn']);
    $smarty->display('crm_info.htm');
} elseif ($_REQUEST['act'] == 'salesperson') {
    $adminuserController = new Yoho\cms\Controller\AdminuserController();
    $salespeople = $adminuserController->getAdminsByPriv(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_ORDERMSG, 'name');    
    foreach ($salespeople as $key => $value) {
        $salespeople[$key] = mb_convert_case($value, MB_CASE_TITLE, "UTF-8");
        if ($value == $_SESSION['admin_name']) {
            $selected = $key;
        }
    }
    make_json_response(["sales" => $salespeople, "selected" => $selected]);
} elseif ($_REQUEST['act'] == 'email') {
    @ini_set('memory_limit', '128M');

    // Get all labels
    $results = $service->users_labels->listUsersLabels($user);
    foreach ($results->getLabels() as $row) {
        $saved_labels = labelDisplayName($saved_labels, $row->name, $row->id);
    }
    $saved_labels['system'][] = [
        'name' => '已讀郵件',
        'key' => 'label',
        'value' => 'read'
    ];
    asort($saved_labels['label']);

    if (empty($_REQUEST['step'])) {
        // Process label / category filter
        // $q = empty($_REQUEST['query']) ? "label:dem-test" : $_REQUEST['query'];
        $q = empty($_REQUEST['query']) ? "" : $_REQUEST['query'];
        $queries = processQuery($q);
        foreach ($queries as $q_type => $query_arraies) {
            if (!in_array($q_type, ['query', 'q'])) {
                foreach ($query_arraies as $type => $query_array) {
                    foreach ($query_array as $query) {
                        foreach ($saved_labels as $key => $col) {
                            foreach ($col as $index => $row) {
                                if ($row['value'] == $query) {
                                    $saved_labels[$key][$index]['class'] = $type == 1 ? "selected" : "intermediate";
                                }
                            }
                        }
                    }
                }
            }
        }

        $params = [
            'maxResults' => 30,
            'q' => empty($queries['q']) ? "-category:updates label:inbox" : $queries['q']//  -category:updates -in:sent label:read has:attachment
        ];

        if (!empty($_REQUEST['pageToken'])) {
            $params['pageToken'] = $_REQUEST['pageToken'];
        }
    
        $results = $service->users_threads->listUsersThreads($user, $params);

        $client->setUseBatch(true);
        $batch = new Google_Http_Batch($client);
        $batch2 = new Google_Http_Batch($client);

        foreach ($results->getThreads() as $row) {
            $mail_id = $row->id;
            $email = $service->users_messages->get($user, $mail_id, ['format' => 'metadata', 'metadataHeaders' => ['From', 'Date', 'Subject']]);
            $batch->add($email, $mail_id);
        }

        $labels = [];
        foreach ($batch->execute() as $key => $res) {
            if (empty($res) || get_class($res) != 'Google_Service_Gmail_Message') continue;
            $mail = [];
            $headers = $res->getPayload()->getHeaders();
            $mail_id = str_replace('response-', '', $key);
            $mail['info'] = $crmController->getUsefulHeaders($headers);

            foreach ($res->labelIds as $label) {
                if (!in_array($label, ['IMPORTANT', 'CATEGORY_PERSONAL', 'INBOX'])) {
                    $mail['labels'][] = strtolower($label);
                    if (!in_array($label, $labels)) {
                        $labels[] = $label;
                    }
                }
            }

            $mail['ticket_id'] = $crmController->createTicket($mail_id, $res->getThreadId(), CRM_PLATFORM_GMAIL, $mail['info']['From_mail'], $mail['info']['Format_Date']);
            if (!empty($mail['info']['Payment'])) {
                if (strpos($_SERVER['SERVER_NAME'], 'beta.') === false) {
                    if (in_array("unread", $mail['labels'])) {
                        $mods = new Google_Service_Gmail_ModifyMessageRequest();
                        $mods->setRemoveLabelIds(['UNREAD']);
                        $service->users_messages->modify($user, $mail_id, $mods);
                        array_splice($mail['labels'], array_search("unread", $mail['labels']), 1);
                    }
                }
                if (strpos($mail['info']['Subject'], "結餘高於預定金額提示") === false) {
                    $sql_pay = "UPDATE " . $ecs->table("crm_ticket_info") . " SET type = 3";
                    if (in_array("starred", $mail['labels'])) {
                        $sql_pay .= ", status = " . CRM_STATUS_FINISHED;
                    }
                    $sql_pay .=  " WHERE ticket_id = $mail[ticket_id]";
                    $db->query($sql_pay);
                    $email = $service->users_messages->get($user, $mail_id);
                    $batch2->add($email, $res->getThreadId());
                }
            }

            $ticket = $db->getRow("SELECT * FROM " . $ecs->table("crm_ticket_info") . " WHERE ticket_id = $mail[ticket_id]");
            $mail['ticket']['type'] = $ticket['type'];
            $mail['ticket']['status'] = $ticket['status'];
            $mail['ticket']['priority'] = $ticket['priority'];
            $mail['ticket']['type_formatted'] = $_LANG['ivr_type_label'][$ticket['type']];
            $mail['ticket']['status_formatted'] = $_LANG['crm_status_label'][$ticket['status']];
            $mail['ticket']['priority_formatted'] = $_LANG['crm_priority_label'][$ticket['priority']];
            $mail['ticket']['last_update'] =  $crmController->display_date($db->getOne("SELECT MAX(create_at) FROM " . $ecs->table("crm_list") . " WHERE ticket_id = $mail[ticket_id]"));
            $mail_list[$res->getThreadId()] = $mail;
        }

        foreach ($batch2->execute() as $key => $res) {
            $thread_id = str_replace('response-', '', $key);
            $payload = $res->getPayload();
            if (in_array($mail_list[$thread_id]['info']['Payment'], ["HSBC", "HS", "FPS", "HSBC-FPS"])) {
                $html = base64_decode(strtr($payload->body->data, '-_', '+/'));
                preg_match("/(HKD[0-9,]+\.[0-9]{2}){1}/", $html, $amount);
            } elseif ($mail_list[$thread_id]['info']['Payment'] == "PAYPAL") {
                $processed = $crmController->retrieveEmailContent($payload, true, true);
                $html = preg_replace("/\n/Us", " ", base64_decode($processed['body'][0]));
                preg_match("/Total: (\\\$[0-9,]+\.[0-9]{2}){1} HKD/", $html, $amount);
                if (empty($processed['body'][0])) {
                    $html = preg_replace("/\r\n/Us", " ", base64_decode(strtr($payload->body->data, '-_', '+/')));
                    preg_match("/You received a payment of (\\\$[0-9,]+\.[0-9]{2}){1} HKD/", $html, $amount);
                }
            } elseif ($mail_list[$thread_id]['info']['Payment'] == "TEST" && strpos($_SERVER['SERVER_NAME'], 'beta.') !== false) {
                $html = base64_decode(strtr($payload->parts[0]->body->data, '-_', '+/'));
                preg_match("/(HKD[0-9,]+\.[0-9]{2}){1}/", $html, $amount);
            }

            if (!empty($amount)) {
                $mail_list[$thread_id]['info']['Amount'] .= $amount[1];
            }
        }

        $smarty->assign('query', implode(' ', $queries['query']));
        $smarty->assign('currPageToken', $_REQUEST['pageToken']);
        $smarty->assign('pageToken', $results->nextPageToken);
        $smarty->assign('mail_list', $mail_list);
    } elseif ($_REQUEST['step'] == 1 && !empty($_REQUEST['mail_id'])) {
        $mail_id = $_REQUEST['mail_id'];
        $last_msg_id = $mail_id;
        if (empty($_REQUEST['mail_only'])) {
            $tmp_mail = $service->users_messages->get($user, $mail_id);
            $thread_id = $tmp_mail->getThreadId();
            $thread = $service->users_threads->get($user, $thread_id, ['format' => 'metadata', 'metadataHeaders' => ['From', 'Date']]);
            $last_sender = "";
            foreach ($thread->getMessages() as $row) {
                $sql = "SELECT user_name, avatar FROM " . $ecs->table('crm_log') . " cl " .
                        "LEFT JOIN " . $ecs->table('admin_user') . " au ON cl.admin_id = au.user_id " .
                        "WHERE cl.message = '" . $row->id . "'";
                $t_reply = $db->getRow($sql);
                $info = $crmController->getUsefulHeaders($row->getPayload()->getHeaders());
                $ticket_id = $crmController->createTicket($row->id, $mail_id, CRM_PLATFORM_GMAIL, $info['From_mail'], $info['Format_Date']);
                $last_sender = $info['From_mail'];
                $last_msg_id = $row->id;
                $t_msg[$row->id] = [
                    'snippet' => $row->snippet,
                    'info' => $info,
                    'reply' => $t_reply['user_name'],
                    'avatar' => empty($t_reply['avatar']) ? '/uploads/avatars/default_60x60.png' : '/' . $t_reply['avatar']
                ];
            }
            if (!empty($last_sender)) {
                if ($last_sender != $crmController::GMAIL_USER) {
                    $ticket_status = CRM_STATUS_PENDING;
                } else {
                    $ticket_status = CRM_STATUS_REPLIED;
                }
                $db->query("UPDATE " . $ecs->table("crm_ticket_info") . " SET status = $ticket_status WHERE ticket_id = $ticket_id AND NOT(type = 3 AND status = " . CRM_STATUS_FINISHED . ")");
            }
            $smarty->assign('thread', $t_msg);
        } else {
            $smarty->assign('mail_only', 1);
        }
        $email = $service->users_messages->get($user, $last_msg_id);
        $payload = $email->getPayload();
        $headers = $payload->getHeaders();
        $parts = $payload->getParts();
        $mail = $crmController->getUsefulHeaders($headers);
        $is_draft = false;

        foreach ($email->labelIds as $row) {
            if (!empty($saved_labels['label'][$row])) {
                $saved_labels['label'][$row]['class'] = "selected";
            }
        }

        if (in_array('DRAFT', $email->labelIds)) {
            $is_draft = true;
        }
        if (in_array('UNREAD', $email->labelIds)) {
            $is_unread = true;
        }

        if (strpos($_SERVER['SERVER_NAME'], 'beta.') === false && $is_unread) {
            $mods = new Google_Service_Gmail_ModifyMessageRequest();
            $mods->setRemoveLabelIds(['UNREAD']);
            $service->users_messages->modify($user, $mail_id, $mods);
            $crmController->createTicket($mail_id, $email->getThreadId(), CRM_PLATFORM_GMAIL, $mail['From_mail'], $mail['Format_Date']);
        }

        $processed = $crmController->retrieveEmailContent($payload);
        if (empty($processed['body'])) {
            $processed = $crmController->retrieveEmailContent($payload, true);
        }
        $mail = array_merge($mail, $processed);

        if (!is_null($payload->body->data)) {
            $mail['html_body'] = base64_decode(strtr($payload->body->data, '-_', '+/'));
        }
        foreach ($mail['body'] as $body) {
            $content = base64_decode($body);
            $mail['html_body'] .= $content;
        }
        if (!empty($mail['inline_attachment'])) {
            foreach ($mail['inline_attachment'] as $key => $attachment) {
                if (strpos($mail['html_body'], $key) === false) {
                    $mail['attachment'][] = $attachment;
                } else {
                    if (strpos($attachment['filename'], "~WRD") !== false) {
                        continue;
                    }
                    $mail['html_body'] = preg_replace("/(.*)<img (.*)src=\"$key\"(.*)>(.*)/Us", "$1<a class='a_dl' href='javascript:void(0)' download='$attachment[filename]' data-m='$last_msg_id' data-a='$attachment[attachment_id]' data-t='$attachment[mimeType]'>下載中...</a>$4", $mail['html_body']);
                }
            }
        }
        if (!empty($mail['attachment'])) {
            $mail['html_body'] .= "<div class='row'><div id='attachment_container'><hr style='margin-bottom: 0; border-color: #ddd;'>";
            foreach ($mail['attachment'] as $attachment) {
                if (strpos($attachment['filename'], "~WRD") !== false) {
                    continue;
                }
                $mail['html_body'] .= "<a class='email_attachment a_dl' href='javascript:void(0)' download='$attachment[filename]' data-m='$last_msg_id' data-a='$attachment[attachment_id]' data-t='$attachment[mimeType]'>" .
                        "<div class='a_title'>$attachment[filename]</div>" .
                        "<div class='a_name'>" .
                            "<div class='f_name'>$attachment[filename]</div>" .
                            "<div class='f_dl'>下載中...</div>" .
                        "</div>" .
                        "<div class='f_ext'><i class='f_icon fa fa-file$attachment[icon]'></i><div></div></div>";
                $mail['html_body'] .= "</a>";
            }
            $mail['html_body'] .= "</div></div>";
        }
        $mail['html_body'] = preg_replace("/<style(.*)<\/style>/Us", "", $mail['html_body']);

        $ticket = $db->getRow("SELECT * FROM " . $ecs->table('crm_ticket_info') . " WHERE ref_id = '" . $email->getThreadId() . "'");
        if (!empty($ticket['order_id'])) {
            $order_sn = $db->getOne("SELECT order_sn FROM " . $ecs->table('order_info') . " WHERE order_id = $ticket[order_id]");
        }
        if (!empty($ticket['user_id'])) {
            $user_name = $db->getOne("SELECT user_name FROM " . $ecs->table('users') . " WHERE user_id = $ticket[user_id]");
        } else {
            $user_name = $db->getOne("SELECT user_name FROM " . $ecs->table('users') . " WHERE email = '$mail[From_mail]' AND email != 'info@yohohongkong.com'");
        }
        if (empty($user_name)) {
            $sql = "SELECT u.user_name FROM " . $ecs->table('users') . " u " .
                    "INNER JOIN " . $ecs->table('crm_ticket_info') . " cti ON cti.user_id = u.user_id " .
                    "INNER JOIN " . $ecs->table('crm_list') . " cl ON cl.ticket_id = cti.ticket_id " .
                    "WHERE cl.sender = '$mail[From_mail]' AND cl.sender != 'info@yohohongkong.com'";
            $user_name = $db->getOne($sql);
        }
        if (empty($ticket['user_id']) && !empty($user_name)) {
            $user_id = $db->getOne("SELECT user_id FROM " . $ecs->table("users") . " WHERE user_name = '$user_name' ORDER BY user_id DESC");
            $db->query("UPDATE " . $ecs->table('crm_ticket_info') . " SET user_id = $user_id WHERE ticket_id = $ticket[ticket_id]");
        }
        $sql = "SELECT user_name, avatar FROM " . $ecs->table('crm_log') . " cl " .
                "LEFT JOIN " . $ecs->table('admin_user') . " au ON cl.admin_id = au.user_id " .
                "WHERE cl.message = '" . $mail_id . "'";
        $admin_reply = $db->getRow($sql);
        $mail['admin_reply'] = $admin_reply['user_name'];
        $mail['avatar'] = empty($admin_reply['avatar']) ? '/uploads/avatars/default_60x60.png' : '/' . $admin_reply['avatar'];

        foreach ($_LANG['crm_type'] as $key => $type) {
            $type_list[$key] = $type;
        }
        $smarty->assign('ticket_id', $ticket['ticket_id']);
        $smarty->assign('order_sn', $order_sn);
        $smarty->assign('user_name', $user_name);
        $smarty->assign('type_list', $type_list);
        $smarty->assign('mail', $mail);
        $smarty->assign('thread_id', $email->getThreadId());
        $smarty->assign('last_msg_id', $last_msg_id);
    } elseif ($_REQUEST['step'] == 2) {
        if (!empty($_REQUEST['mail_id'])) {
            $mail_ids = explode(",", $_REQUEST['mail_id']);
            foreach ($mail_ids as $mail_id) {
                $mods = new Google_Service_Gmail_ModifyThreadRequest();
                empty($_REQUEST['add']) ? true : $mods->setAddLabelIds([$_REQUEST['add']]);
                empty($_REQUEST['rm']) ? true : $mods->setRemoveLabelIds([$_REQUEST['rm']]);
                try {
                    $results = $service->users_threads->modify($user, $mail_id, $mods);
                    if (!empty($_REQUEST['archive'])) {
                        $ticket_id = $db->getOne("SELECT ticket_id FROM " . $ecs->table("crm_list") . " WHERE unique_id = '$mail_id'");
                        $db->query("UPDATE " . $ecs->table("crm_ticket_info") . " SET status = " . CRM_STATUS_ARCHIVED . " WHERE ticket_id = $ticket_id");
                    }
                } catch (Exception $e) {
                    $err = \json_decode($e->getMessage(), true);
                    make_json_error($err['error']['message']);
                }
            }
            make_json_response('success');
        } else {
            make_json_error("請指定電郵");
        }
    } elseif ($_REQUEST['step'] == 3) {
        $mail_id = trim($_REQUEST['mail_id']);
        $attachment_id = trim($_REQUEST['attachment_id']);
        $mimeType = trim($_REQUEST['mimeType']);
        if (!empty($mail_id) && !empty($attachment_id) && !empty($mimeType)) {
            $attachment = $service->users_messages_attachments->get($user, $mail_id, $attachment_id);
            $attachment_data = "data:" . $mimeType . ";base64," . strtr($attachment->getData(), array('-' => '+', '_' => '/'));
            make_json_response([
                "mime" => $mimeType,
                "data" => $attachment_data,
                "size" => formatBytes($attachment->getSize())
            ]);
        } else {
            make_json_error("Missing mail_id or attachment_id");
        }
    }
    $smarty->assign('is_draft', $is_draft);
    $smarty->assign('thread_id', $email->threadId);
    $smarty->assign('saved_labels', $saved_labels);
    $smarty->display('crm_email.htm');
} elseif ($_REQUEST['act'] == 'draft') {
    if (empty($_REQUEST['threadId']) && empty($_REQUEST['email'])) {
        make_json_error('無法發送電郵');
    } elseif (empty($_REQUEST['draft_body']) && empty($_REQUEST['empty_mail']) && empty($_FILES)) {
        make_json_response('empty');
    } else {
        $thread_id = $_REQUEST['threadId'];
        $mail_id = $_REQUEST['mailId'];
        $message = new Google_Service_Gmail_Message();
        $attachment = [];
        $inline_attachment = [];

        $draft_body = trim(stripcslashes($_REQUEST['draft_body']));
        $signature = (empty($draft_body) ? "" : "<br><br><br>") . 'Thank you for your support<br><br>Best Regards,<br>' . ucfirst($_SESSION['admin_name']) . '<div><div><div class=3D"gmail_signature" data-smartmail=3D"gmail_signature"><div dir=3D"ltr"><div dir=3D"ltr" style=3D"font-size:13px"><br><div><div><a href=3D"https://www.yohohongkong.com/" target=3D"_blank"><img src=3D"http://www.yohohongkong.com/images/yoho.gif" alt=3D"=3DE5=3D8F=3D8B=3DE5=3D92=3D8C YOHO"></a><br><br></div><div><font color=3D"#666666" size=3D"2"><a href=3D"mailto:info@yohohongkong.com" target=3D"_blank">info@yohohongkong.com</a> | 53356996 | <a href=3D"http://www.yohohongkong.com/" target=3D"_blank">www.yohohongkong.com</a></font></div></div><div><br></div><div><font color=3D"#999999">follow us<br></font></div><div><a href=3D"http://www.facebook.com/yohohongkong" target=3D"_blank"><img src=3D"http://www.yohohongkong.com/images/facebook.gif" alt=3D" Facebook"></a> <a href=3D"http://www.google.com/+yohohongkong" target=3D"_blank"><img src=3D"http://www.yohohongkong.com/images/google+.gif" alt=3D" Google+"></a> <a href=3D"http://www.twitter.com/yohohongkong" target=3D"_blank"><img src=3D"http://www.yohohongkong.com/images/twitter.gif" alt=3D" Twitter"></a> </div></div></div></div></div></div>';

        // Handle inline images
        preg_match_all("/<img src=\"..\/images\/upload\/Image\/(\d+.\w+)\"(.*)>/U", $draft_body, $match);
        if (!empty($match)) {
            foreach ($match[0] as $key => $val) {
                $path = "../images/upload/Image/" . $match[1][$key];
                if (file_exists($path)) {
                    $type = pathinfo($path, PATHINFO_EXTENSION);
                    $filename = pathinfo($path, PATHINFO_FILENAME);
                    $base64_image = base64_encode(file_get_contents($path));
                    $inline_attachment = array_merge($inline_attachment, [
                        "--boundary0",
                        'Content-Type: image/' . $type . '; name="' . $match[1][$key] . '"',
                        "Content-Transfer-Encoding: base64",
                        "Content-ID: <$filename>",
                        'Content-Disposition: inline; filename="' . $match[1][$key] . '"' . "\r\n",
                        $base64_image . "\r\n"
                    ]);
                    $img = str_replace("=", "=3D", $val);
                    $img = str_replace($path, "cid:" . $filename, $img);
                    $draft_body = str_replace($val, $img, $draft_body);
                    
                    unlink($path);
                }
            }
        }

        // Handle attachment
        if (!empty($_FILES['file'])) {
            foreach ($_FILES['file']['name'] as $key => $val) {
                $type = $_FILES['file']['type'][$key];
                $tmp_name =  str_replace("/", "_", $_FILES['file']['tmp_name'][$key]) . "_$thread_id";
                $base64_file = base64_encode(file_get_contents($_FILES['file']['tmp_name'][$key]));
                $attachment = array_merge($attachment, [
                    "--boundary1",
                    'Content-Type: ' . $type . '; name="' . $val . '"',
                    "Content-Transfer-Encoding: base64",
                    "Content-ID: <$tmp_name>",
                    'Content-Disposition: attachment; filename="' . $val . '"' . "\r\n",
                    $base64_file . "\r\n"
                ]);
            }
        }

        // Handle reply or new email
        if (empty($_REQUEST['email'])) {
            $email = $service->users_messages->get($user, $mail_id, ['format' => 'metadata', 'metadataHeaders' => ['Subject', 'From', 'Message-ID']]);
            $headers = $email->getPayload()->getHeaders();
            $info = $crmController->getUsefulHeaders($headers);

            $mime_line = [
                "MIME-Version: 1.0",
                "In-Reply-To: " . $info['Message-ID'],
                "References: " . $info['Message-ID'],
                "From: =?utf-8?B?" . base64_encode("友和 YOHO") . "?= <$user>",
                "To: =?utf-8?B?" . base64_encode($info['From_user']) . "?= <$info[From_mail]>",
                "Subject: =?utf-8?B?" . base64_encode($info['Subject']) . "?=",
            ];
        } else {
            $mime_line = [
                "MIME-Version: 1.0",
                "From: =?utf-8?B?" . base64_encode("友和 YOHO") . "?= <$user>",
                "To: $_REQUEST[email]",
                "Subject: =?utf-8?B?" . base64_encode($_REQUEST['subject']) . "?=",
            ];
        }

        $main_body = [
            "--boundary0",
            'Content-Type: text/html; charset="utf-8"',
            "Content-Transfer-Encoding: quoted-printable\r\n",
            $draft_body . $signature . "\r\n",
        ];

        if (!empty($attachment) && !empty($inline_attachment)) {
            $mime_line = array_merge($mime_line, [
                'Content-Type: multipart/mixed; boundary="boundary1"' . "\r\n",
                '--boundary1',
                'Content-Type: multipart/related; boundary="boundary0"' . "\r\n",
            ]);
            $mime_line = array_merge($mime_line, $main_body, $inline_attachment);
            $mime_line[] = "--boundary0--\r\n";
            $mime_line = array_merge($mime_line, $attachment);
            $mime_line[] = "--boundary1--";
        } elseif (!empty($attachment)) {
            $mime_line[] = 'Content-Type: multipart/mixed; boundary="boundary1"' . "\r\n";
            $main_body[0] = "--boundary1";
            $mime_line = array_merge($mime_line, $main_body, $attachment);
            $mime_line[] = "--boundary1--";
        } elseif (!empty($inline_attachment)) {
            $mime_line[] = 'Content-Type: multipart/related; boundary="boundary0"' . "\r\n";
            $mime_line = array_merge($mime_line, $main_body, $inline_attachment);
            $mime_line[] = "--boundary0--";
        } else {
            $mime_line[] = 'Content-Type: multipart/alternative; boundary="boundary0"' . "\r\n";
            $mime_line = array_merge($mime_line, $main_body);
            $mime_line[] = "--boundary0--";
        }
        $mime = implode("\r\n", $mime_line);
        $mime = rtrim(strtr(base64_encode($mime), '+/', '-_'), '=');
        $message->setRaw($mime);
        $message->setThreadId($thread_id);

        // Create draft or send immediately
        try {
            $ticket_id = $db->getOne("SELECT ticket_id FROM " . $ecs->table('crm_ticket_info') . " WHERE ref_id = '$thread_id'");
            if ($_REQUEST['draft_step'] == 'send') {
                $request = $service->users_messages->send($user, $message);
                $msg_id = $request->id;
                if (empty($_REQUEST['email'])) {
                    $list_id = $db->getOne("SELECT list_id FROM " . $ecs->table('crm_list') . " WHERE unique_id = '$mail_id'");
                    $db->query("UPDATE " . $ecs->table('crm_ticket_info') . " SET status = " . CRM_STATUS_REPLIED . ", last_update = CURRENT_TIMESTAMP WHERE ref_id = '$thread_id'");
                    $db->query("INSERT INTO " . $ecs->table('crm_log') . " (ticket_id, list_id, admin_id, type, message) VALUES ($ticket_id, $list_id, $_SESSION[admin_id], " . CRM_LOG_TYPE_REPLY . ", '$msg_id')");
                } else {
                    $db->query("INSERT INTO " . $ecs->table('crm_log') . " (admin_id, type, message) VALUES ($_SESSION[admin_id], " . CRM_LOG_TYPE_REPLY . ", '$msg_id')");
                }
            } elseif ($_REQUEST['draft_step'] == 'save') {
                $draft = new Google_Service_Gmail_Draft();
                $draft->setMessage($message);
                $request = $service->users_drafts->create($user, $draft);
            }
            if (empty($thread_id)) {
                make_json_response([]);
            } elseif (!empty($request->id) && !empty($ticket_id)) {
                if ($_REQUEST['draft_step'] == 'save') {
                    make_json_response(["ticket_id" => $ticket_id, "mail_id" => $mail_id]);
                } else {
                    make_json_response(["ticket_id" => $ticket_id, "mail_id" => $request->id]);
                }
            }
            make_json_response([]);
        } catch (Exception $e) {
            $err = \json_decode($e->getMessage(), true);
            make_json_error($err['error']['message']);
        }
    }
} elseif ($_REQUEST['act'] == 'link') {
    if (empty($_REQUEST['step'])) {
        $ticket_id = $_REQUEST['ticket'];
        if (empty($ticket_id)) {
            make_json_error('無法更新紀錄');
        }

        $log_type = CRM_LOG_TYPE_UPDATE;

        foreach ($_REQUEST as $key => $val) {
            if (in_array($key, ['type', 'status', 'priority', 'order_id']) && !empty($val)) {
                $req[] = "$key = $val";
                if ($key != 'order_id') {
                    $platform = $db->getOne("SELECT platform FROM " . $ecs->table("crm_ticket_info") . " WHERE ticket_id = $ticket_id");
                    if ($key == 'status' && $val == CRM_STATUS_REPLIED && $platform == CRM_PLATFORM_AIRCALL) {
                        $db->query("DELETE FROM " . $ecs->table("crm_aircall_queue") . " WHERE call_id IN (SELECT unique_id FROM " . $ecs->table("crm_list") . " WHERE ticket_id = $ticket_id) AND event = " . CRM_AIRCALL_MISSED);
                        $log_type = CRM_LOG_TYPE_REPLY;
                    }
                    $result[$key] = $_LANG['crm_' . $key . '_label'][$val];
                } else {
                    $user_id = $db->getOne("SELECT user_id FROM " . $ecs->table('order_info') . " WHERE order_id = $val");
                    if (!empty($user_id)) {
                        $req[] = "user_id = $user_id";
                    }
                }
            } elseif ($key == 'user_id' && !empty($val) && empty($_REQUEST['order_id'])) {
                $order = $db->getOne("SELECT order_id FROM " . $ecs->table("crm_ticket_info") . " WHERE ticket_id = $ticket_id");
                if (empty($order)) {
                    $req[] = "$key = $val";
                }
            }
        }

        if (!empty($req)) {
            if ($db->query("UPDATE " . $ecs->table('crm_ticket_info') . " SET " . implode(', ', $req) . ", last_update = CURRENT_TIMESTAMP WHERE ticket_id = $ticket_id")) {
                $db->query("INSERT INTO " . $ecs->table('crm_log') . " (ticket_id, admin_id, type) VALUES ($ticket_id, $_SESSION[admin_id], " . $log_type . ")");
                make_json_response($result);
            } else {
                make_json_error('更新紀錄失敗');
            }
        } else {
            make_json_error('請選擇更新欄位');
        }
    } elseif ($_REQUEST['step'] == 1) {
        $ticket_id = $_REQUEST['ticket_id'];
        if (empty($ticket_id)) {
            make_json_error("請選擇查詢");
        }
        $ticket = $db->getRow("SELECT * FROM " . $ecs->table("crm_ticket_info") . " WHERE ticket_id = $ticket_id");
        if (empty($ticket)) {
            make_json_error("無法獲取查詢資料");
        } else {
            $ticket['formatted_type'] = $_LANG['ivr_type_label'][$ticket['type']];
            $ticket['formatted_status'] = $_LANG['crm_status_label'][$ticket['status']];
            $ticket['formatted_priority'] = $_LANG['crm_priority_label'][$ticket['priority']];
        }
        $smarty->assign('ticket_status', 1);
        $smarty->assign('ticket', $ticket);
        $smarty->display("crm_email.htm");
    }
} elseif ($_REQUEST['act'] == 'chatra') {
    if (!empty($secrets['chatra'])) {
        $auth = $crmController::CHATRA_PUBLIC_KEY . ":$secrets[chatra]";
        $tickets = [];
        $messages = [];
        if (empty($_REQUEST['step'])) {
            $min = empty($_REQUEST['page']) ? 0 : intval($_REQUEST['page']) * 10;
            $update_date = date('Y-m-d H:i:s', floor($_REQUEST['update'] / 1000));
            $sql = "SELECT * FROM " . $ecs->table('crm_ticket_info') . " cti " .
                    "INNER JOIN (" .
                        "SELECT ticket_id, MAX(create_at) as recent FROM " . $ecs->table('crm_list') . (empty($_REQUEST['update']) ? "" : " WHERE create_at > '$update_date'") . " GROUP BY ticket_id" .
                    ") cl ON cl.ticket_id = cti.ticket_id" .
                    " WHERE platform = " . CRM_PLATFORM_CHATRA .
                    " ORDER BY cl.recent DESC " . (empty($_REQUEST['update']) ? "LIMIT $min, 10" : "");
            $tickets = $db->getAll($sql);

            $ticketIndex = [];
            $clientIndex = [];
            $mh = curl_multi_init();
            $mh2 = curl_multi_init();

            foreach ($tickets as $key => $ticket) {
                $last_msg = $db->getRow("SELECT * FROM " . $ecs->table("crm_list") . " WHERE ticket_id = $ticket[ticket_id] ORDER BY create_at DESC LIMIT 1");
                $client_id = $db->getOne("SELECT DISTINCT sender FROM " . $ecs->table("crm_list") . " WHERE ticket_id = $ticket[ticket_id] AND sender IN (SELECT client_id FROM " . $ecs->table("crm_platform_user") . " WHERE platform = " . CRM_PLATFORM_CHATRA . ")");
                $ticketIndex[$last_msg['unique_id']] = $key;
                $date = new DateTime($last_msg['create_at']);
                $today = new DateTime("today");
                $last_msg['date'] = $today > $date ? $date->format("Y-m-d") : $date->format("H:i");
                $tickets[$key]['msg'] = $last_msg;

                $url = $chatra_url . "messages/$last_msg[unique_id]";
                $ch = RESTfulAPICall("GET", $url, $auth, true);
                curl_multi_add_handle($mh, $ch);
                if (!empty($client_id)) {
                    $clientIndex[$client_id] = $key;
                    $url = $chatra_url . "clients/$client_id";
                    $ch2 = RESTfulAPICall("GET", $url, $auth, true);
                    curl_multi_add_handle($mh2, $ch2);
                }
            }
            do {
                curl_multi_exec($mh, $running);
            } while ($running > 0);
            while ($info = curl_multi_info_read($mh)) {
                $ch = $info['handle'];
                $chs[] = $ch;
                $result = \json_decode(curl_multi_getcontent($ch), true);
                $key = $ticketIndex[$result['id']];
                $tickets[$key]['msg']['text'] = preg_replace(["/\*(.*)\*/i", "/_(.*)_/i","/`/i"], ["<b>$1</b>", "<i>$1</i>", ""], $result['text']);
                $tickets[$key]['msg']['type'] = $result['type'];
                $tickets[$key]['recent'] = $result['createdAt'];
                $is_admin = $db->getOne("SELECT COUNT(*) FROM " . $ecs->table("crm_platform_agent") . " WHERE agent_id = '$result[agentId]' AND platform = " . CRM_PLATFORM_CHATRA);
                $tickets[$key]['new'] = !empty($_REQUEST['update']) && $is_admin == 0 ? 1 : 0;
                curl_multi_remove_handle($mh, $ch);
            }
            curl_multi_close($mh);
            foreach ($chs as $ch) {
                curl_close($ch);
            }
            if (!empty($clientIndex)) {
                do {
                    curl_multi_exec($mh2, $running2);
                } while ($running2 > 0);
                while ($info2 = curl_multi_info_read($mh2)) {
                    $ch2 = $info2['handle'];
                    $chs2[] = $ch2;
                    $result2 = \json_decode(curl_multi_getcontent($ch2), true);
                    $key2 = $clientIndex[$result2['id']];
                    $tickets[$key2]['user'] = $result2['id'];
                    $users[] = $result2;
                    curl_multi_remove_handle($mh2, $ch2);
                }
                curl_multi_close($mh2);
                foreach ($chs2 as $ch2) {
                    curl_close($ch2);
                }
            }

            foreach ($tickets as $ticket) {
                $ticket_json[$ticket['ref_id']] = $ticket;
            }
            $sql = "SELECT 1 FROM " . $ecs->table('crm_ticket_info') . " cti " .
                    "INNER JOIN (" .
                        "SELECT ticket_id, MAX(create_at) as recent FROM " . $ecs->table('crm_list') . (empty($_REQUEST['update']) ? "" : " WHERE create_at > '$update_date'") . " GROUP BY ticket_id" .
                    ") cl ON cl.ticket_id = cti.ticket_id" .
                    " WHERE platform = " . CRM_PLATFORM_CHATRA .
                    " ORDER BY cl.recent DESC " . (empty($_REQUEST['update']) ? "LIMIT " . ($min + 10) . ", 10" : "");
            $next_page = $db->getOne($sql);
            make_json_response([
                'tickets' => $ticket_json,
                'users' => $users,
                'page' => intval($_REQUEST['page']) + 1,
                'next_page' => empty($next_page) ? 0 : 1
            ]);
        } elseif ($_REQUEST['step'] == 1) {
            if (empty($_REQUEST['ticket_id'])) {
                make_json_error("請選擇對話");
            } else {
                $ticket_id = $_REQUEST['ticket_id'];
                $page = empty($_REQUEST['page']) ? 0 : intval($_REQUEST['page']);
                $messages = $db->getAll("SELECT * FROM " . $ecs->table('crm_list') . " WHERE ticket_id = $ticket_id " . (empty($_REQUEST['update']) || !empty($page) ? "" : "AND receive_at > '$_REQUEST[update]' ") . "ORDER BY create_at DESC LIMIT " . ($page * 10) . ",10");
                $messages = array_reverse($messages);

                $messageIndex = [];
                $mh = curl_multi_init();
                foreach ($messages as $key => $message) {
                    $messageIndex[$message['unique_id']] = $key;
                    $url = $chatra_url . "messages/$message[unique_id]";
                    $ch = RESTfulAPICall("GET", $url, $auth, true);
                    curl_multi_add_handle($mh, $ch);
                }

                do {
                    curl_multi_exec($mh, $running);
                } while ($running > 0);

                while ($info = curl_multi_info_read($mh)) {
                    $ch = $info['handle'];
                    $result = \json_decode(curl_multi_getcontent($ch), true);
                    $key = $messageIndex[$result['id']];
                    $this_list_id = $messages[$key]['list_id'];
                    $file = $db->getRow("SELECT * FROM " . $ecs->table("crm_chatra_attachment") . " WHERE list_id = $this_list_id");
                    if (empty($result['text']) && empty($file)) {
                        unset($messages[$key]);
                        continue;
                    }
                    $result['text'] = preg_replace(["/\n/i", "/\*(.*)\*/i", "/_(.*)_/i","/`/i"], ["<br>", "<b>$1</b>", "<i>$1</i>", ""], $result['text']);
                    $messages[$key]["msg"] = $result;
                    $messages[$key]["msg"]['file'] = $file;
                    if (!empty($messages[$key]["msg"]['file'])) {
                        $messages[$key]["msg"]['file']['formatted_size'] = formatBytes($messages[$key]["msg"]['file']['size']);
                        if (!$messages[$key]["msg"]['file']['is_image']) {
                            $messages[$key]["msg"]['file']['icon'] = fileIcon(explode(".", $messages[$key]["msg"]['file']['name'])[1]);
                        }
                    }
                    if ($result['type'] == 'client') {
                        if (empty($client_user)) {
                            $url = $chatra_url . "clients/$result[clientId]";
                            $client_user = \json_decode(RESTfulAPICall("GET", $url, $auth), true);
                        }
                        $user = $client_user['displayedName'];
                    } else {
                        $sql = "SELECT user_id, user_name FROM " . $ecs->table("crm_platform_agent") . " cca " .
                                "LEFT JOIN " . $ecs->table("admin_user") . " au ON au.user_id = cca.admin_id " .
                                "WHERE agent_id = '$result[agentId]' AND platform = " . CRM_PLATFORM_CHATRA;
                        $admin = $db->getRow($sql);
                        $actual = $db->getRow("SELECT cl.*, au.user_name FROM " . $ecs->table('crm_log') . " cl INNER JOIN " . $ecs->table('admin_user') . " au ON au.user_id = cl.admin_id WHERE message = '$result[id]' AND type = " . CRM_LOG_TYPE_REPLY);
                        $user = ucfirst($admin['user_name']) . (!empty($actual) ? ($actual['admin_id'] != $admin['user_id'] ? "<span class='from_admin_user'>$actual[user_name]</span>" : "") : "");
                    }
                    $messages[$key]['user'] = $user;
                }

                foreach ($messages as $key => $message) {
                    $date = new DateTime($message['create_at']);
                    $messages[$key]["time"] = $date->format("H:i");
                    $messages[$key]["create_at_time"] = $message['msg']['createdAt'];
                    $messages[$key]["create_at_date"] = $date->format("Y-m-d");
                }
                $today = new DateTime("today");
                $today = $today->format("Y-m-d");
                $yesterday = new DateTime("yesterday");
                $yesterday = $yesterday->format("Y-m-d");

                $ticket = $db->getRow("SELECT * FROM " . $ecs->table('crm_ticket_info') . " WHERE ticket_id = $ticket_id");
                $order_sn = "";
                $user_name = "";
                if (!empty($ticket['order_id'])) {
                    $order_sn = $db->getOne("SELECT order_sn FROM " . $ecs->table('order_info') . " WHERE order_id = $ticket[order_id]");
                }
                if (!empty($ticket['user_id'])) {
                    $user_name = $db->getOne("SELECT user_name FROM " . $ecs->table('users') . " WHERE user_id = $ticket[user_id]");
                }
                if (empty($user_name)) {
                    $sql = "SELECT u.user_name FROM " . $ecs->table('users') . " u " .
                            "INNER JOIN " . $ecs->table('crm_ticket_info') . " cti ON cti.user_id = u.user_id " .
                            "INNER JOIN " . $ecs->table('crm_list') . " cl ON cl.ticket_id = cti.ticket_id " .
                            "WHERE cl.sender = '$client_user[id]'";
                    $user_name = $db->getOne($sql);
                }

                if (empty($ticket['user_id']) && !empty($user_name)) {
                    $user_id = $db->getOne("SELECT user_id FROM " . $ecs->table("users") . " WHERE user_name = '$user_name' ORDER BY user_id DESC");
                    $db->query("UPDATE " . $ecs->table('crm_ticket_info') . " SET user_id = $user_id WHERE ticket_id = $ticket[ticket_id]");
                }

                $total_messages = $db->getOne("SELECT COUNT(*) FROM " . $ecs->table('crm_list') . " WHERE ticket_id = $ticket_id");
                if (intval($total_messages) <= ($page + 1) * 10) {
                    $page = 0;
                } else {
                    $page++;
                }

                $response = [
                    'page'      => $page,
                    'messages'  => $messages,
                    'ticket_id' => $ticket_id,
                    'order_sn'  => $order_sn,
                    'user_name' => $user_name,
                    'today'     => $today,
                    'yesterday' => $yesterday,
                ];
                make_json_response($response);
            }
        } elseif ($_REQUEST['step'] == 2) {
            if (empty($_REQUEST['clientId'])) {
                make_json_error("請選擇客戶");
            } else {
                $url = $chatra_url . "clients/$_REQUEST[clientId]";
                $result = \json_decode(RESTfulAPICall("GET", $url, $auth), true);
            }
        } elseif ($_REQUEST['step'] == 3) {
            if (empty($_REQUEST['messageId'])) {
                make_json_error("請選擇客戶");
            } else {
                $url = $chatra_url . "messages/$_REQUEST[messageId]";
                $result = \json_decode(RESTfulAPICall("GET", $url, $auth), true);
            }
        } elseif ($_REQUEST['step'] == 4) {
            $new = $db->getOne("SELECT COUNT(*) FROM " . $ecs->table("crm_list") . " WHERE ticket_id = $_REQUEST[ticket_id] AND receive_at > '$_REQUEST[update]'");
            $ticket = $db->getRow("SELECT * FROM " . $ecs->table('crm_ticket_info') . " WHERE ticket_id = $_REQUEST[ticket_id]");
            $order_sn = "";
            $user_name = "";
            if (!empty($ticket['order_id'])) {
                $order_sn = $db->getOne("SELECT order_sn FROM " . $ecs->table('order_info') . " WHERE order_id = $ticket[order_id]");
            }
            if (!empty($ticket['user_id'])) {
                $user_name = $db->getOne("SELECT user_name FROM " . $ecs->table('users') . " WHERE user_id = $ticket[user_id]");
            }
            if (empty($user_name)) {
                $sql = "SELECT u.user_name FROM " . $ecs->table('users') . " u " .
                        "INNER JOIN " . $ecs->table('crm_ticket_info') . " cti ON cti.user_id = u.user_id " .
                        "INNER JOIN " . $ecs->table('crm_list') . " cl ON cl.ticket_id = cti.ticket_id " .
                        "WHERE cl.sender = '$client_user[id]'";
                $user_name = $db->getOne($sql);
            }
            make_json_response([
                'update' => $new,
                'order_sn' => $order_sn,
                'user_name' => $user_name,
            ]);
        } elseif ($_REQUEST['step'] == 5) {
            if (empty($_REQUEST['ticket_id'])) {
                make_json_error("請選擇客戶");
            }
            if (empty($_REQUEST['text']) && empty($_FILES)) {
                make_json_error("無法發送空白訊息");
            }
            $agent_id = $db->getOne("SELECT agent_id FROM " . $ecs->table("crm_platform_agent") . " WHERE admin_id = $_SESSION[admin_id] AND platform = " . CRM_PLATFORM_CHATRA);
            if (empty($agent_id)) {
                $agent_id = $db->getOne("SELECT agent_id FROM " . $ecs->table("crm_platform_agent") . " WHERE admin_id = 1 AND platform = " . CRM_PLATFORM_CHATRA);
            }
            $client_id = $db->getOne("SELECT DISTINCT sender FROM " . $ecs->table("crm_list") . " WHERE ticket_id = $_REQUEST[ticket_id] AND sender NOT IN (SELECT agent_id FROM " . $ecs->table("crm_platform_agent") . " WHERE platform = " . CRM_PLATFORM_CHATRA . ")");
            $data = [
                'clientId'  => $client_id,
                'text'      => trim($_REQUEST['text']),
                'agentId'  => $agent_id
            ];
            $url = $chatra_url . "messages";
            $response = \json_decode(RESTfulAPICall("POST", $url, $auth, false, $data), true);
            if (empty($response['status'])) {
                $list_id = $db->getOne("SELECT MAX(list_id) FROM " . $ecs->table('crm_list') . " WHERE ticket_id = $_REQUEST[ticket_id] GROUP BY ticket_id");
                $db->query("UPDATE " . $ecs->table('crm_ticket_info') . " SET status = " . CRM_STATUS_REPLIED . ", last_update = CURRENT_TIMESTAMP WHERE ticket_id = $_REQUEST[ticket_id]");
                $db->query("INSERT INTO " . $ecs->table('crm_log') . " (ticket_id, list_id, admin_id, type, message) VALUES ($_REQUEST[ticket_id], $list_id, $_SESSION[admin_id], " . CRM_LOG_TYPE_REPLY . ", '$response[id]')");
                make_json_response($response);
            } else {
                make_json_error("Error $response[status]: $response[message]");
            }
        } elseif ($_REQUEST['step'] == 6) {
            $client_id = trim($_REQUEST['clientId']);
            if (empty($client_id)) {
                make_json_error("No client id");
            }
            $clients = array_unique(explode(",", $client_id));
            $mh = curl_multi_init();
            foreach ($clients as $client) {
                $url = $chatra_url . "clients/$client";
                $ch = RESTfulAPICall("GET", $url, $auth, true);
                curl_multi_add_handle($mh, $ch);
            }
            do {
                curl_multi_exec($mh, $running);
            } while ($running > 0);
            while ($info = curl_multi_info_read($mh)) {
                $ch = $info['handle'];
                $chs[] = $ch;
                $result = \json_decode(curl_multi_getcontent($ch), true);
                $client_user[] = $result;
                curl_multi_remove_handle($mh, $ch);
            }
            curl_multi_close($mh);
            foreach ($chs as $ch) {
                curl_close($ch);
            }
            make_json_response(['user' => $client_user]);
        }
        $smarty->assign('tickets', $tickets);
        $smarty->assign('messages', $messages);
        $smarty->display('crm_chatra.htm');
    } else {
        make_json_error("無法連接Chatra");
    }
} elseif ($_REQUEST['act'] == 'aircall') {
    if (!empty($secrets['aircall'])) {
        $auth = $crmController::AIRCALL_PUBLIC_KEY . ":$secrets[aircall]";
        if (empty($_REQUEST['step'])) {
            $start_date = date('Y-m-d H:i:s', strtotime(empty($_REQUEST['aircall_start_date']) ? "-1 week midnight" : $_REQUEST['aircall_start_date']));
            $end_date = date('Y-m-d H:i:s', strtotime(empty($_REQUEST['aircall_end_date']) ? "today" : $_REQUEST['aircall_end_date']) + 86399);
            $phone = empty($_REQUEST['aircall_phone']) ? "" : str_replace("-", "", $_REQUEST['aircall_phone']);

            $page = empty($_REQUEST['aircall_page']) ? 0 : $_REQUEST['aircall_page'];
            $min = empty($page) ? 0 : intval($page) * 10;

            $sql = "SELECT * FROM " . $ecs->table('crm_ticket_info') . " cti " .
                    "INNER JOIN (" .
                        "SELECT ticket_id, MAX(create_at) as recent FROM " . $ecs->table('crm_list') .
                        (empty($_REQUEST['missed'])
                            ? (!empty($phone) ? "" : " WHERE create_at BETWEEN '$start_date' AND '$end_date'")
                            : " WHERE unique_id IN (SELECT call_id FROM " . $ecs->table("crm_aircall_queue") . " WHERE event = 2)"
                        ) .
                        " GROUP BY ticket_id" .
                    ") cl ON cl.ticket_id = cti.ticket_id" .
                    " WHERE platform = " . CRM_PLATFORM_AIRCALL .
                    (empty($_REQUEST['missed']) ? (empty($phone) ? "" : " AND ref_id LIKE '%$phone%'") : "") .
                    " ORDER BY cl.recent DESC LIMIT $min, 10";
            $tickets = $db->getAll($sql);
            $ticketIndex = [];
            $mh = curl_multi_init();
            foreach ($tickets as $key => $ticket) {
                $last_msg = $db->getRow("SELECT * FROM " . $ecs->table("crm_list") . " WHERE ticket_id = $ticket[ticket_id] ORDER BY create_at DESC LIMIT 1");

                $ticketIndex[$ticket['ref_id']] = $key;
                $url = $aircall_url . "calls/$last_msg[unique_id]";
                $ch = RESTfulAPICall("GET", $url, $auth, true);
                curl_multi_add_handle($mh, $ch);
            }

            do {
                curl_multi_exec($mh, $running);
            } while ($running > 0);
            while ($info = curl_multi_info_read($mh)) {
                $ch = $info['handle'];
                $chs[] = $ch;
                $result = \json_decode(curl_multi_getcontent($ch), true);

                $call = $result["call"];
                if (empty($call['raw_digits'])) {
                    $call['raw_digits'] = "Anonymous$call[id]";
                }
                $k = implode("", explode(" ", $call['raw_digits']));
                $today = new DateTime("today");
                $call['create_date'] = date(intval($call['started_at']) > $today->getTimestamp() ? 'H:i' : 'Y-m-d H:i', $call['started_at']);

                if ($call['answered_at'] === null) {
                    $data["meta"]["not_answer"] = 1;
                } else {
                    $call['answer_date'] = intval($call['answered_at']) > $today->getTimestamp() ? date('H:i', $call['answered_at']) : date('Y-m-d H:i', $call['answered_at']);
                    $call['answer_time'] = formatted_duration($call['duration']);
                }
                $tickets[$ticketIndex[$k]]['call'] = $call;
                curl_multi_remove_handle($mh, $ch);
            }
            curl_multi_close($mh);
            foreach ($chs as $ch) {
                curl_close($ch);
            }

            $has_next_page = $db->getOne(
                "SELECT 1 FROM " . $ecs->table('crm_ticket_info') . " cti " .
                    "INNER JOIN (" .
                        "SELECT ticket_id, MAX(create_at) as recent FROM " . $ecs->table('crm_list') .
                        (empty($_REQUEST['missed'])
                            ? (!empty($phone) ? "" : " WHERE create_at BETWEEN '$start_date' AND '$end_date'")
                            : " WHERE unique_id IN (SELECT call_id FROM " . $ecs->table("crm_aircall_queue") . " WHERE event = 2)"
                        ) .
                        " GROUP BY ticket_id" .
                    ") cl ON cl.ticket_id = cti.ticket_id" .
                    " WHERE platform = " . CRM_PLATFORM_AIRCALL .
                    (empty($_REQUEST['missed']) ? (empty($phone) ? "" : " AND ref_id LIKE '%$phone%'") : "") .
                    " HAVING COUNT(*) > " . ($min + 10)
            );
            make_json_response([
                "list" => $tickets,
                "missed" => empty($_REQUEST['missed']) ? 0 : 1,
                "start_date" => date('Y-m-d', strtotime($start_date)),
                "end_date" => date('Y-m-d', strtotime($end_date)),
                "phone" => empty($_REQUEST['aircall_phone']) ? "" : $_REQUEST['aircall_phone'],
                "prev_page" => $page > 0 ? $page - 1 : -1,
                "next_page" => empty($has_next_page) ? -1 : $page + 1,
            ]);
        } elseif ($_REQUEST['step'] == 1) {
            if (empty($_REQUEST['ticket_id'])) {
                make_json_error("沒法找到相關資料");
            }
            $page = empty($_REQUEST['page']) ? 0 : intval($_REQUEST['page']);
            $min = $page * 5;
            $user_name = $db->getOne("SELECT ref_id FROM " . $ecs->table("crm_ticket_info") . " WHERE ticket_id = $_REQUEST[ticket_id]");

            $user_id = $db->getOne("SELECT user_id FROM " . $ecs->table("crm_ticket_info") . " WHERE ticket_id = $_REQUEST[ticket_id]");
            if (empty($user_id) && !empty($user_name)) {
                $user_id = $db->getOne("SELECT user_id FROM " . $ecs->table("users") . " WHERE REPLACE(user_name, '-', '') LIKE '%$user_name%' ORDER BY user_id DESC");
                if (!empty($user_id)) {
                    $db->query("UPDATE " . $ecs->table('crm_ticket_info') . " SET user_id = $user_id WHERE ticket_id = $_REQUEST[ticket_id]");
                }
            }
            $list = $db->getAll("SELECT * FROM " . $ecs->table("crm_list") . " WHERE ticket_id = $_REQUEST[ticket_id] ORDER BY create_at DESC LIMIT $min, 5");

            $mh = curl_multi_init();
            foreach ($list as $key => $row) {
                $url = $aircall_url . "calls/$row[unique_id]";
                $ch = RESTfulAPICall("GET", $url, $auth, true);
                $chs[] = $ch;
                curl_multi_add_handle($mh, $ch);
            }

            do {
                curl_multi_exec($mh, $running);
            } while ($running > 0);

            $list = [];
            while ($info = curl_multi_info_read($mh)) {
                $ch = $info['handle'];
                $result = \json_decode(curl_multi_getcontent($ch), true);

                $call = $result["call"];
                if (empty($call['raw_digits'])) {
                    $call['raw_digits'] = "Anonymous$call[id]";
                }
                $call['create_date'] = date('H:i', $call['started_at']);

                if ($call['answered_at'] === null) {
                    $data["meta"]["not_answer"] = 1;
                    $call['answer_time'] = "N/A";
                } else {
                    $call['answer_time'] = formatted_duration($call['duration']);
                }
                $list[date('Y-m-d', $call['started_at'])][] = $call;
                curl_multi_remove_handle($mh, $ch);
            }
            curl_multi_close($mh);
            foreach ($chs as $ch) {
                curl_close($ch);
            }

            $has_next_page = $db->getAll("SELECT * FROM " . $ecs->table("crm_list") . " WHERE ticket_id = $_REQUEST[ticket_id] ORDER BY create_at DESC LIMIT " . ($min + 5) . ", 5");
            make_json_response([
                "user_name" => $user_name,
                "next_page" => empty($has_next_page) ? -1 : $page + 1,
                "list" => $list,
            ]);
        } elseif ($_REQUEST['step'] == 2) {
            $queues = $db->getAll(
                "SELECT caq.*, au.user_name, cti.ref_id FROM " . $ecs->table("crm_aircall_queue") . " caq " .
                "LEFT JOIN " . $ecs->table("crm_ticket_info") . " cti ON caq.ticket_id = cti.ticket_id " .
                "LEFT JOIN " . $ecs->table("admin_user") . " au ON caq.admin_id = au.user_id " .
                "WHERE (caq.event != " . CRM_AIRCALL_MISSED ." OR (caq.event = " . CRM_AIRCALL_MISSED ." AND cti.ref_id NOT LIKE '%Anonymous%')) " .
                "GROUP BY cti.ref_id ORDER BY caq.queue_id DESC"
            );

            $list = [];
            $forward_users = $db->getCol("SELECT user_id FROM " . $ecs->table("admin_user") . " WHERE (role_id IN (3, 4, 10) AND is_disable != 1) OR user_id = 1");
            foreach ($queues as $row) {
                if ($row['event'] == CRM_AIRCALL_ANSWER) {
                    $query[$row['call_id']] = "answering";
                    if (in_array($_SESSION['admin_id'], $forward_users) && $row['admin_id'] != $_SESSION['admin_id']) {
                        $list["forward"][] = [
                            'admin' => $row['user_name'],
                            'raw_digits' => $row['ref_id'],
                            'ticket_id' => $row["ticket_id"],
                            'call_id' => $row['call_id']
                        ];
                    }
                    if ($row['admin_id'] != $_SESSION['admin_id']) continue;
                } elseif ($row['event'] == CRM_AIRCALL_MISSED) {
                    $query[$row['call_id']] = "missed";
                }
                if ($row['notify'] != 1) {
                    $db->query("UPDATE " . $ecs->table("crm_aircall_queue") . " SET notify = 1 WHERE queue_id = $row[queue_id]");
                }
                if (count($list[$query[$row['call_id']]]) < 50) {
                    $list[$query[$row['call_id']]][] = [
                        'ticket_id' => $row['ticket_id'],
                        'call_id' => $row['call_id'],
                        'raw_digits' => $row['ref_id'],
                        'notify' => $row['notify']
                    ];
                }
            }

            make_json_response([
                'list' => $list,
                'miss_count' => count($queues) - count($list['answering']),
            ]);
        } elseif ($_REQUEST['step'] == 3) {
            if (empty($_REQUEST['call_id'])) {
                make_json_error("fail");
            }
            $ticket_id = $db->getOne("SELECT ticket_id FROM " . $ecs->table("crm_list") . " WHERE unique_id = '$_REQUEST[call_id]'");
            if (empty($ticket_id)) {
                make_json_error("fail");
            }
            $call_ids = $db->getOne("SELECT GROUP_CONCAT(unique_id) FROM " . $ecs->table("crm_list") . " WHERE ticket_id = $ticket_id");
            $db->query("DELETE FROM " . $ecs->table("crm_aircall_queue") . " WHERE call_id IN ($call_ids) AND event = " . CRM_AIRCALL_MISSED);
            make_json_response("success");
        }
    } else {
        make_json_error("無法連接Aircall");
    }
} elseif ($_REQUEST['act'] == 'offline') {
    $start_time = empty($_REQUEST['date']) ? local_strtotime("today") : local_strtotime($_REQUEST['date']);
    $end_time = $start_time + 86400;
    $sql = "SELECT oi.order_id, oi.order_sn, oi.money_paid, oa.log_time, cti.ticket_id FROM " . $ecs->table("order_action") . " oa " .
            "LEFT JOIN " . $ecs->table("order_info") . " oi on oi.order_id = oa.order_id " .
            "LEFT JOIN " . $ecs->table("crm_ticket_info") . " cti on cti.order_id = oa.order_id " .
            " WHERE oa.action_note LIKE '%[系統備註]自動標籤已付款%' AND oa.log_time BETWEEN $start_time AND $end_time";
    $res = $db->getAll($sql);

    foreach ($res as $key => $row) {
        $res[$key]['formatted_log_time'] = local_date("Y-m-d H:i:s", $row['log_time']);
    }
    make_json_response($res);
} elseif ($_REQUEST['act'] == 'dashboard') {
    $data = $crmController->getFullList();
    $smarty->assign("sql", $data['sql']);
    $smarty->assign("list", $data['list']);
    $smarty->assign("data", $data['data']);
    $smarty->display("crm_dashboard.htm");
} elseif ($_REQUEST['act'] == 'twilio') {
    if (empty($_REQUEST['step'])) {
        $start_date = date('Y-m-d H:i:s', strtotime(empty($_REQUEST['twilio_start_date']) ? "-1 week midnight" : $_REQUEST['twilio_start_date']));
        $end_date = date('Y-m-d H:i:s', strtotime(empty($_REQUEST['twilio_end_date']) ? "today" : $_REQUEST['twilio_end_date']) + 86399);
        $phone = empty($_REQUEST['twilio_phone']) ? "" : str_replace("-", "", $_REQUEST['twilio_phone']);

        $page = empty($_REQUEST['twilio_page']) ? 0 : $_REQUEST['twilio_page'];
        $min = empty($page) ? 0 : intval($page) * 20;
        $where = "WHERE 1" .
                (empty($phone) ? "" : " AND tcl.sender LIKE '%$phone%'") .
                (empty($_REQUEST['missed']) ? "" : (" AND tcti.status < " . CRM_STATUS_REPLIED)) .
                (empty($start_date) ? "" : " AND tcl.receive_at >= '$start_date'") .
                (empty($end_date) ? "" : " AND tcl.receive_at <= '$end_date'");

        $sql = "SELECT * FROM (" .
                    "SELECT MAX(log_id) as max_log_id FROM " . $ecs->table("crm_twilio_call") . " tctc " .
                    "LEFT JOIN " . $ecs->table("crm_list") . " tcl ON tctc.list_id = tcl.list_id " .
                    "LEFT JOIN " . $ecs->table("crm_ticket_info") . " tcti ON tcl.ticket_id = tcti.ticket_id " .
                    "$where GROUP BY tcti.ticket_id) tmp " .
                "LEFT JOIN " . $ecs->table("crm_twilio_call") . " ctc ON ctc.log_id = tmp.max_log_id " .
                "LEFT JOIN " . $ecs->table("crm_list") . " cl ON ctc.list_id = cl.list_id " .
                "LEFT JOIN " . $ecs->table("crm_ticket_info") . " cti ON cl.ticket_id = cti.ticket_id " .
                "ORDER BY cl.receive_at DESC LIMIT $min, 20";
        $twilio_calls = $db->getAll($sql);
        $today = new DateTime("today");

        foreach ($twilio_calls as $key => $call) {
            $create_date = new DateTime($call['receive_at']);
            $twilio_calls[$key]['duration'] = formatted_duration($call['duration']);
            $twilio_calls[$key]['create_at'] = local_date($create_date->getTimestamp() > $today->getTimestamp() ? 'H:i' : 'Y-m-d H:i', local_strtotime($call['receive_at']));
        }

        $has_next_page = $db->getOne(
            "SELECT 1 FROM (" .
                "SELECT MAX(log_id) as max_log_id FROM " . $ecs->table("crm_twilio_call") . " tctc " .
                "LEFT JOIN " . $ecs->table("crm_list") . " tcl ON tctc.list_id = tcl.list_id " .
                "LEFT JOIN " . $ecs->table("crm_ticket_info") . " tcti ON tcl.ticket_id = tcti.ticket_id " .
                "$where GROUP BY tcti.ticket_id) tmp " .
            "LEFT JOIN " . $ecs->table("crm_twilio_call") . " ctc ON ctc.log_id = tmp.max_log_id " .
            "LEFT JOIN " . $ecs->table("crm_list") . " cl ON ctc.list_id = cl.list_id " .
            "LEFT JOIN " . $ecs->table("crm_ticket_info") . " cti ON cl.ticket_id = cti.ticket_id " .
            "HAVING COUNT(*) > " . ($min + 20)
        );
        make_json_response([
            "list" => $twilio_calls,
            "missed" => empty($_REQUEST['missed']) ? 0 : 1,
            "start_date" => date('Y-m-d', strtotime($start_date)),
            "end_date" => date('Y-m-d', strtotime($end_date)),
            "phone" => $phone,
            "prev_page" => $page > 0 ? $page - 1 : -1,
            "next_page" => empty($has_next_page) ? -1 : $page + 1,
        ]);
    } elseif ($_REQUEST['step'] == 1) {
        if (empty($_REQUEST['ticket_id'])) {
            make_json_error("沒法找到相關資料");
        }
        $page = empty($_REQUEST['page']) ? 0 : intval($_REQUEST['page']);
        $min = $page * 5;
        $user_name = $db->getOne("SELECT ref_id FROM " . $ecs->table("crm_ticket_info") . " WHERE ticket_id = $_REQUEST[ticket_id]");

        $user_id = $db->getOne("SELECT user_id FROM " . $ecs->table("crm_ticket_info") . " WHERE ticket_id = $_REQUEST[ticket_id]");
        if (empty($user_id) && !empty($user_name)) {
            $user_id = $db->getOne("SELECT user_id FROM " . $ecs->table("users") . " WHERE REPLACE(user_name, '-', '') LIKE '%$user_name%' ORDER BY user_id DESC");
            if (!empty($user_id)) {
                $db->query("UPDATE " . $ecs->table('crm_ticket_info') . " SET user_id = $user_id WHERE ticket_id = $_REQUEST[ticket_id]");
            }
        }
        $twilio_calls = $db->getAll(
            "SELECT * FROM " . $ecs->table("crm_twilio_call") . " ctc " .
            "LEFT JOIN " . $ecs->table("crm_list") . " cl ON ctc.list_id = cl.list_id " .
            "LEFT JOIN (SELECT user_id, user_name FROM " . $ecs->table("admin_user") . ") au ON au.user_id = ctc.admin_id " .
            "WHERE cl.ticket_id = $_REQUEST[ticket_id] ORDER BY create_at DESC LIMIT $min, 5"
        );

        foreach ($twilio_calls as $call) {
            $call['answer_time'] = formatted_duration($call['duration']);
            $call['create_date'] = local_date('H:i', local_strtotime($call['create_at']));
            $levels = explode("_", $call['levels']);
            array_shift($levels);
            $pressed = [];
            $menu = $crmController->get_ivr_language()['MENU'];
            foreach ($levels as $level) {
                $pressed[] = $menu[$level]['name'][$crmController::IVR_LANG_CANTONESE];
                if (!empty($menu[$level]['menu'])) {
                    $menu = $menu[$level]['menu'];
                }
            }
            $call['levels'] = implode("_", $pressed);
            $list[local_date('Y-m-d', local_strtotime($call['create_at']))][] = $call;
        }
        $has_next_page = $db->getOne(
            "SELECT * FROM " . $ecs->table("crm_twilio_call") . " ctc " .
            "LEFT JOIN " . $ecs->table("crm_list") . " cl ON ctc.list_id = cl.list_id " .
            "WHERE cl.ticket_id = $_REQUEST[ticket_id] ORDER BY create_at DESC LIMIT " . ($min + 5) . ", 5"
        );
        make_json_response([
            "user_name" => $user_name,
            "next_page" => empty($has_next_page) ? -1 : $page + 1,
            "list" => $list,
        ]);
    } elseif ($_REQUEST['step'] == 2) {
        if (empty($_REQUEST["list"])) {
            make_json_error("沒有紀錄");
        }
        $sql = "SELECT cti.ticket_id, cl.sender FROM " . $ecs->table("crm_ticket_info") . " cti " .
                "LEFT JOIN " . $ecs->table("crm_list") . " cl ON cl.ticket_id = cti.ticket_id " .
                "WHERE cl.ticket_id IN ($_REQUEST[list])";
        $list = $db->getAll($sql);
        make_json_response(['list' => $list]);
    }
}

function translateStatus($orders)
{
    foreach ($orders as $key => $row) {
        $orders[$key]['order_status']    = $GLOBALS["_LANG"]['os'][$row['order_status']];
        $orders[$key]['pay_status']      = $GLOBALS["_LANG"]['icon']['ps'][$row['pay_status']];
        $orders[$key]['shipping_status'] = $GLOBALS["_LANG"]['icon']['ss'][$row['shipping_status']];
    }
    return $orders;
}

function operable_list($order)
{
    $os = $order['order_status'];
    $ss = $order['shipping_status'];
    $ps = $order['pay_status'];

    $custom_service_list = ['compensate'];

    /* 取得订单操作权限 */
    $actions = $_SESSION['action_list'];
    if ($actions == 'all') {
        $priv_list  = array('os' => true, 'ss' => true, 'ps' => true, 'edit' => true);
    } else {
        $actions    = ',' . $actions . ',';
        $priv_list  = array(
            'os'    => strpos($actions, ',order_os_edit,') !== false,
            'ss'    => strpos($actions, ',order_ss_edit,') !== false,
            'ps'    => strpos($actions, ',order_ps_edit,') !== false,
            'edit'  => strpos($actions, ',order_edit,') !== false
        );
    }

    /* 取得订单支付方式是否货到付款 */
    $payment = payment_info($order['pay_id']);
    $is_cod  = $payment['is_cod'] == 1;

    /* 根据状态返回可执行操作 */
    $list = array();
    if (OS_UNCONFIRMED == $os) {
        /* 状态：未确认 => 未付款、未发货 */
        if ($priv_list['os']) {
            $list['confirm']    = true; // 确认
            $list['cancel']     = true; // 取消
            if ($is_cod) {
                /* 货到付款 */
                if ($priv_list['ss']) {
                    //$list['prepare'] = true; // 配货
                    $list['split'] = true; // 分单
                }
            } else {
                /* 不是货到付款 */
                if ($priv_list['ps']) {
                    $list['pay'] = true;  // 付款
                }
            }
        }
    } elseif (OS_CONFIRMED == $os || OS_SPLITED == $os || OS_SPLITING_PART == $os) {
        /* 状态：已确认 */
        if (PS_UNPAYED == $ps) {
            /* 状态：已确认、未付款 */
            if (SS_UNSHIPPED == $ss || SS_PREPARING == $ss) {
                /* 状态：已确认、未付款、未发货（或配货中） */
                if ($priv_list['os']) {
                    $list['cancel'] = true; // 取消
                }
                if ($is_cod) {
                    /* 货到付款 */
                    if ($priv_list['ss']) {
                        if (SS_UNSHIPPED == $ss) {
                            //$list['prepare'] = true; // 配货
                        }
                        $list['split'] = true; // 分单
                    }
                } else {
                    /* 不是货到付款 */
                    if ($priv_list['ps']) {
                        $list['pay'] = true; // 付款
                    }
                }
            } elseif (SS_SHIPPED_ING == $ss || SS_SHIPPED_PART == $ss) {
                /* 状态：已确认、未付款、发货中 */
                // 部分分单
                if (OS_SPLITING_PART == $os) {
                    // 如果有未付款, 不允許開發貨單
                    // $list['split'] = true; // 分单
                }
                //$list['to_delivery'] = true; // 去发货
                $list['pay'] = true; // 付款
                if ($order['is_wholesale']==true) {
                    $list['unship'] = true;
                }
            } else {
                /* 状态：已确认、未付款、已发货或已收货 => 货到付款 */
                if ($priv_list['ps']) {
                    $list['pay'] = true; // 付款
                }
                if ($priv_list['ss']) {
                    if (SS_SHIPPED == $ss) {
                        //$list['receive'] = true; // 收货确认
                    }
                    $list['unship'] = true; // 设为未发货
                    if ($priv_list['os']) {
                        $list['return'] = true; // 退货
                    }
                }
            }
        } else {
            /* 状态：已确认、已付款和付款中 */
            $list['invalid'] = true; // 退款
            $list['customer_service'] = true; // 售后

            /* 状态：已确认、已付款和付款中 */
            if (SS_UNSHIPPED == $ss || SS_PREPARING == $ss) {
                /* 状态：已确认、已付款和付款中、未发货（配货中） => 不是货到付款 */
                if ($priv_list['ss']) {
                    if (SS_UNSHIPPED == $ss) {
                        //$list['prepare'] = true; // 配货
                    }
                    $list['split'] = true; // 分单
                    $list['quick_ship'] = true;
                }
                if ($priv_list['ps']) {
                    $list['unpay'] = true; // 设为未付款
                    if ($priv_list['os']) {
                        $list['cancel'] = true; // 取消
                    }
                }
            } elseif (SS_SHIPPED_ING == $ss || SS_SHIPPED_PART == $ss) {
                /* 状态：已确认、已付款、发货中[已发货(部分商品) || 发货中(处理分单)] */
                // 部分分单
                if (OS_SPLITING_PART == $os) {
                    $list['split'] = true; // 分单
                    $list['quick_ship'] = true;
                    //$list['to_delivery'] = true; // 去发货
                    //$list['prepare'] = true;
                } elseif ( OS_CONFIRMED == $os && SS_SHIPPED_PART == $ss) { 
                    /* 已確認,已付款,已發貨(部分產品) */
                    $list['finish_order'] = true; //In this order status, sales team maybe need update sghiiping / discount fee, need this btn to manual finish order.
                } else {
                    //$list['to_delivery'] = true; // 去发货
                    //$list['prepare'] = true;
                    $list['split'] = true; // 分单
                    $list['quick_ship'] = true;
                }

                if ($order['is_wholesale']==true) {
                    $list['unship'] = true;
                }
            } else {
                /* 状态：已确认、已付款和付款中、已发货或已收货 */
                if ($priv_list['ss']) {
                    if (SS_SHIPPED == $ss) {
                        //$list['receive'] = true; // 收货确认
                    }
                    if (!$is_cod) {
                        $list['unship'] = true; // 设为未发货
                    }
                }
                if ($priv_list['ps'] && $is_cod) {
                    $list['unpay']  = true; // 设为未付款
                }
                if ($priv_list['os'] && $priv_list['ss'] && $priv_list['ps']) {
                    $list['return'] = true; // 退货（包括退款）
                }
            }
        }
        // Orders with user 11111111, 88888888, 99999999
        //if (in_array($order['user_id'], array('4940','5022','5023')))
        //REMARK: there isn't an "is_wholesale" in order,
        //TODO: make use of the new order_type field with constant OrderController::DB_ORDER_TYPE_WHOLESALE
        if ($order['is_wholesale']==true) {
            $list['pay'] = false;
            //$list['unship'] = false;
            //$list['invalid'] = false;
            $list['wholesale_pay'] = ($ps==PS_PAYED?false:true); // 批發訂單付款
            $list['quick_ship'] = true;
            $list['split'] = true;
        }
    } elseif (OS_CANCELED == $os) {
        /* 状态：取消 */
        if ($priv_list['os']) {
            $list['confirm'] = true;
        }
        if ($priv_list['edit']) {
            $list['remove'] = true;
        }
    } elseif (OS_INVALID == $os) {
        /* 状态：退款 */
        if ($priv_list['os']) {
            $list['confirm'] = true;
        }
        if ($priv_list['edit']) {
            $list['remove'] = true;
        }
    } elseif (OS_WAITING_REFUND == $os) {
        /* 状态：等待全單退款 */
        if ($priv_list['os']) {
            $list['confirm'] = true;
        }
        if ($priv_list['edit']) {
            $list['remove'] = true;
        }
        $list['customer_service'] = true; // 售后
    } elseif (OS_RETURNED == $os) {
        /* 状态：退货 */
        if ($priv_list['os']) {
            $list['confirm'] = true;
        }
    }

    /* 修正发货操作 */
    if (!empty($list['split'])) {
        /* 如果是团购活动且未处理成功，不能发货 */
        if ($order['extension_code'] == 'group_buy') {
            include_once(ROOT_PATH . 'includes/lib_goods.php');
            $group_buy = group_buy_info(intval($order['extension_id']));
            if ($group_buy['status'] != GBS_SUCCEED) {
                unset($list['split']);
                unset($list['quick_ship']);
                unset($list['to_delivery']);
            }
        }

        /* 如果部分发货 不允许 取消 订单 */
        if (order_deliveryed($order['order_id'])) {
            $list['return'] = true; // 退货（包括退款）
            unset($list['cancel']); // 取消
        }
    }

    /* 出貨後不能退款 */
    if (SS_UNSHIPPED != $ss) {
        $list['invalid'] = false;
    }

    /* 儲存備註 */
    $list['after_service'] = true;

    /* Set cs list */
    if ($list['customer_service']) {
        foreach ($custom_service_list as $item) {
            $list[$item] = true;
        }
    }
    return $list;
}

function order_deliveryed($order_id)
{
    $return_res = 0;

    if (empty($order_id)) {
        return $return_res;
    }

    $sql = 'SELECT COUNT(delivery_id)
            FROM ' . $GLOBALS['ecs']->table('delivery_order') . '
            WHERE order_id = \''. $order_id . '\'
            AND ( status = '.DO_DELIVERED.' or status = '.DO_COLLECTED.') ';
    $sum = $GLOBALS['db']->getOne($sql);

    if ($sum) {
        $return_res = 1;
    }

    return $return_res;
}

function formatBytes($bytes)
{
    $base = log($bytes, 1024);
    $units = array('B', 'KB', 'MB', 'GB', 'TB');
    return round(pow(1024, $base - floor($base)), 2) .' '. $units[floor($base)];
}

function labelDisplayName($labels, $name, $id = "")
{
    switch ($name) {
    case 'CATEGORY_PERSONAL':
    case 'CHAT':
    case 'STARRED':
        break;
    case 'CATEGORY_SOCIAL':
        $labels['category'][] = [
            'name' => '社交網路',
            'key' => 'category',
            'value' => 'social'
        ];
        break;
    case 'CATEGORY_FORUMS':
        $labels['category'][] = [
            'name' => '論壇',
            'key' => 'category',
            'value' => 'forums'
        ];
        break;
    case 'CATEGORY_UPDATES':
        $labels['category'][] = [
            'name' => '最新快訊',
            'key' => 'category',
            'value' => 'updates'
        ];
        break;
    case 'CATEGORY_PROMOTIONS':
        $labels['category'][] = [
            'name' => '促銷內容',
            'key' => 'category',
            'value' => 'promotions'
        ];
        break;
    case 'INBOX':
        $labels['system'][] = [
            'name' => '收件匣',
            'key' => 'label',
            'value' => 'inbox'
        ];
        break;
    case 'SENT':
        $labels['system'][] = [
            'name' => '寄件備份',
            'key' => 'label',
            'value' => 'sent'
        ];
        break;
    case 'TRASH':
        $labels['system'][] = [
            'name' => '垃圾桶',
            'key' => 'label',
            'value' => 'trash'
        ];
        break;
    case 'DRAFT':
        $labels['system'][] = [
            'name' => '草稿',
            'key' => 'label',
            'value' => 'draft'
        ];
        break;
    case 'SPAM':
        $labels['system'][] = [
            'name' => '垃圾郵件',
            'key' => 'label',
            'value' => 'span'
        ];
        break;
    case 'UNREAD':
        $labels['system'][] = [
            'name' => '未讀郵件',
            'key' => 'label',
            'value' => 'unread'
        ];
        break;
    case 'IMPORTANT':
        $labels['system'][] = [
            'name' => '重要郵件',
            'key' => 'label',
            'value' => 'important'
        ];
        break;
    default:
        if (strpos($name, "Dem Test") === false || strpos($_SERVER['SERVER_NAME'], 'beta.') !== false) {
            $labels['label'][$id] = [
                'name' => $name,
                'key' => 'label',
                'value' => strtolower(preg_replace('/[\s\/\(\)]/', '-', $name))
            ];
        }
        break;
    }
    return $labels;
}

function fileIcon($ext)
{
    $icon = "";
    switch ($ext) {
    case 'xlsx':
    case 'xls':
        $icon = '-excel';
        break;
    case 'docx':
    case 'doc':
        $icon = '-word';
        break;
    case 'pptx':
    case 'ppt':
        $icon = '-powerpoint';
        break;
    case 'pdf':
        $icon = '-pdf';
        break;
    case 'jpg':
    case 'jpeg':
    case 'png':
    case 'gif':
    case 'bmp':
        $icon = '-image';
        break;
    }
    return $icon;
}

function processQuery($query)
{
    $cats = [];
    $labels = [];
    $search = [];
    $searchArr = explode(" ", urldecode(trim($query)));
    foreach ($searchArr as $row) {
        if (!empty($row)) {
            $q[] = $row;
            preg_match("/(-?)(category:|label:|in:)(.*)/", $row, $res);
            if (empty($res)) {
                $search[] = $row;
            } else {
                if (strpos($res[0], 'label') !== false) {
                    $labels[empty($res[1]) ? 1 : 2][] = $res[3];
                } elseif (strpos($res[0], 'category') !== false) {
                    $cats[empty($res[1]) ? 1 : 2][] = $res[3];
                } elseif (strpos($res[0], 'in') !== false) {
                    $labels[empty($res[1]) ? 1 : 2][] = $res[3];
                    $cats[empty($res[1]) ? 1 : 2][] = $res[3];
                }
            }
        }
    }
    return [
        'query' => $search,
        'category' => $cats,
        'label' => $labels,
        'q' => implode(' ', $q)
    ];
}

function processCRMList($crm_list)
{
    global $db, $ecs, $_LANG, $smarty;

    foreach ($crm_list as $key => $row) {
        $crm_list[$key]['label_type'] = $_LANG['crm_type_label'][$row['type']];
        $crm_list[$key]['type'] = $_LANG['crm_type'][$row['type']];
        $crm_list[$key]['label_status'] = $_LANG['crm_status_label'][$row['status']];
        $crm_list[$key]['status'] = $_LANG['crm_status'][$row['status']];
        $crm_list[$key]['label_priority'] = $_LANG['crm_priority_label'][$row['priority']];
        $crm_list[$key]['priority'] = $_LANG['crm_priority'][$row['priority']];
        $crm_list[$key]['platform'] = $_LANG['crm_platform'][$row['platform']];
        
        $messages = $db->getAll("SELECT * FROM " . $ecs->table('crm_list') . " WHERE ticket_id = $row[ticket_id]");
        foreach ($messages as $msg) {
            $m = [
                'unique_id' => $msg['unique_id'],
                'create_at' => $msg['create_at'],
                'sender' => $msg['sender']
            ];
            if ($row['platform'] == CRM_PLATFORM_CHATRA) {
                $u = $db->getOne(
                    "SELECT user_name FROM " . $ecs->table("users") . " u " .
                    "INNER JOIN " . $ecs->table("crm_platform_user") . " cpu ON cpu.user_id = u.user_id " .
                    "WHERE cpu.platform = " . CRM_PLATFORM_CHATRA . " AND cpu.client_id = '$m[sender]'"
                );
                if (!empty($u)) {
                    $m['sender'] = $u;
                }
            }
            $crm_list[$key]['message'][strtotime($msg['create_at'])] = $m;
        }
        $replies = $db->getAll("SELECT * FROM " . $ecs->table('crm_log') . " WHERE ticket_id = $row[ticket_id] AND type = " . CRM_LOG_TYPE_REPLY);
        foreach ($replies as $reply) {
            $crm_list[$key]['message'][strtotime($reply['create_at'])] = [
                'unique_id' => $reply['message'],
                'create_at' => $reply['create_at'],
                'sender' => $db->getOne("SELECT user_name FROM " . $ecs->table('admin_user') . " WHERE user_id = $reply[admin_id]")
            ];
        }
    }
    if (count($crm_list) == 1) {
        $smarty->assign('crm_detail', 1);
    }
    $smarty->assign('crm_list', $crm_list);
    return $crm_list;
}

function processUser($user)
{
    global $db, $ecs, $smarty, $order_sort;

    $sql_user_info = "SELECT reg_field_id, content FROM " . $ecs->table('reg_extend_info') . " WHERE user_id = $user[user_id]";
    $user_info = $db->getAll($sql_user_info);
    foreach ($user_info as $row) {
        if ($row['reg_field_id'] == 8) {
            $user['first_name'] = $row['content'];
        } elseif ($row['reg_field_id'] == 9) {
            $user['last_name'] = $row['content'];
        }
    }
    $sql_latest_order = "SELECT * FROM " . $ecs->table('order_info') . " WHERE user_id = $user[user_id] " . $order_sort . " LIMIT 5";
    $latest_order = $db->getAll($sql_latest_order);

    $sql_crm = "SELECT * FROM " . $ecs->table('crm_ticket_info') . " WHERE user_id = $user[user_id]";
    processCRMList($db->getAll($sql_crm));
    $smarty->assign('latest_order', translateStatus($latest_order));
    return $user;
}

function processOrder($order_id)
{
    global $db, $ecs, $_LANG, $smarty, $_CFG;

    $order = order_info($order_id);
    $order['user_name'] = $db->getOne("SELECT user_name FROM " . $ecs->table("users") . " WHERE user_id = $order[user_id]");
    $order['op_list'] = operable_list($order);
    $order['order_status']      = $_LANG['os'][$order['order_status']];
    $order['pay_status']        = $_LANG['ps'][$order['pay_status']];
    $order['shipping_status']   = $_LANG['ss'][$order['shipping_status']];
    $order['formated_add_time'] = local_date($GLOBALS['_CFG']['time_format'], $order['add_time']);
    $platforms = Yoho\cms\Controller\OrderController::DB_SELLING_PLATFORMS;
    $order['platform_display'] = isset($platforms[$order['platform']]) ? $platforms[$order['platform']] : $order['platform'];

    $sql_region = "SELECT concat(IFNULL(c.region_name, ''), '  ', IFNULL(p.region_name, ''), " .
                "'  ', IFNULL(t.region_name, ''), '  ', IFNULL(d.region_name, '')) AS region " .
            "FROM " . $ecs->table('order_info') . " AS o " .
                "LEFT JOIN " . $ecs->table('region') . " AS c ON o.country = c.region_id " .
                "LEFT JOIN " . $ecs->table('region') . " AS p ON o.province = p.region_id " .
                "LEFT JOIN " . $ecs->table('region') . " AS t ON o.city = t.region_id " .
                "LEFT JOIN " . $ecs->table('region') . " AS d ON o.district = d.region_id " .
            "WHERE o.order_id = '$order[order_id]'";
    $order['region'] = $db->getOne($sql_region);

    $sql_order_goods = "SELECT goods_name, goods_number, goods_price FROM " . $ecs->table('order_goods') . " WHERE order_id = $order[order_id]";
    $all_goods = $db->getAll($sql_order_goods);
    foreach ($all_goods as $key => $row) {
        $all_goods[$key]['formated_subtotal'] = price_format($row['goods_price'] * $row['goods_number']);
    }
    $order['all_goods'] = $all_goods;

    $deliveryorderController = new Yoho\cms\Controller\DeliveryOrderController();
    $delivery_list = $deliveryorderController->get_order_delivery_info($order_id);
    foreach ($delivery_list as $key => $row) {
        $delivery_list[$key]['update_time'] = local_date($_CFG['time_format'], $row['update_time']);
    }
    $order['delivery_list'] = $delivery_list;

    /* display button according to admin role */
    $btn_access = array('order_pay', 'order_refund', 'order_cancel', 'order_unpay', 'order_remove', 'order_after_service');
    foreach ($btn_access as $item) {
        if (check_authz($item)) {
            $smarty->assign($item, 1);
        }
    }

    $sql_crm = "SELECT * FROM " . $ecs->table('crm_ticket_info') . " WHERE order_id = $order[order_id]";
    processCRMList($db->getAll($sql_crm));
    return $order;
}

function RESTfulAPICall($method, $url, $auth, $multi = false, $data = false)
{
    $ch = curl_init();
    $opt = [
        CURLOPT_HTTPHEADER      => ['Content-Type: application/json'],
        CURLOPT_HTTPAUTH        => CURLAUTH_BASIC,
        CURLOPT_USERPWD         => $auth,
        CURLOPT_RETURNTRANSFER  => 1
    ];
    switch ($method) {
    case "POST":
        $opt[CURLOPT_POST] = 1;
        if ($data) {
            $opt[CURLOPT_POSTFIELDS] = json_encode($data);
        }
        break;
    case "PUT":
        $opt[CURLOPT_PUT] = 1;
        break;
    default:
        if ($data) {
            $url = sprintf("%s?%s", $url, http_build_query($data));
        }
        break;
    }
    $opt[CURLOPT_URL] = $url;
    curl_setopt_array($ch, $opt);
    if ($multi) {
        return $ch;
    }
    $result = curl_exec($ch);
    curl_close($ch);

    return $result;
}

function formatted_duration($second)
{
    $duration = [];
    $second = intval($second);
    if ($second == 0) {
        return "接聽中";
    }
    if ($second > 3600) {
        $remain = $second % 3600;
        $duration[] = (($second - $remain) / 3600) . "小時";
        $second = $second % 3600;
    }
    if ($second > 60) {
        $remain = $second % 60;
        $duration[] = (($second - $remain) / 60) . "分";
        $second = $second % 60;
    }
    if ($second > 0 && count($duration) < 2) {
        $duration[] = $second . "秒";
    }
    return empty($duration) ? "N/A" :implode("", $duration);
}

function addCrmJsFiles()
{
    $files = [
        'js/crm_gmail.js',
        // 'js/crm_aircall.js',
        'js/crm_chatra.js',
        'js/crm_twilio.js',
        'js/crm.js',
    ];
    foreach ($files as $file) {
        if (file_exists(ROOT_PATH . ADMIN_PATH . "/$file")) {
            $js .= "<script src='$file?" . filemtime($file) . "'></script>";
        }
    }
    return $js;
}
?>