<?php

/***
* accounting_report.php
* by howang 2014-11-17
*
* Accounting System
***/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_accounting.php');
$paymentController = new Yoho\cms\Controller\PaymentController();
$accountingController = new Yoho\cms\Controller\AccountingController();
$adminuserController = new Yoho\cms\Controller\AdminuserController();
if (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'summary'))
{
    /* 权限判断 */
    admin_priv('sale_order_stats');

    /* 赋值到模板 */
    $smarty->assign('bank_options', $actg->getBankCodes());
    $smarty->assign('show_hidden', $_REQUEST['show_hidden']);
    $smarty->assign('ur_here',      '帳戶概覽');
    $smarty->assign('action_link',  array('text' => '新增帳戶','href'=>'accounting_report.php?act=add_account'));
    $smarty->assign('action_link2', array('text' => '貨幣及滙率', 'href' => 'currencies.php?act=list'));

    /* 显示页面 */
    assign_query_info();
    $smarty->display('accounting_report_summary.htm');
}
elseif (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'query_summary'))
{
    /* 检查权限 */
    check_authz_json('sale_order_stats');
    $accountingController->ajaxQueryAction();

}
elseif (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'query' ||  $_REQUEST['act'] == 'download'))
{
    /* 检查权限 */
    check_authz_json('sale_order_stats');
    
    if (strstr($_REQUEST['start_date'], '-') === false)
    {
        $_REQUEST['start_date'] = local_date('Y-m-d', $_REQUEST['start_date']);
        $_REQUEST['end_date'] = local_date('Y-m-d', $_REQUEST['end_date']);
    }
    
    /*------------------------------------------------------ */
    //--Excel文件下载
    /*------------------------------------------------------ */
    if ($_REQUEST['act'] == 'download')
    {
        $file_name = $_REQUEST['start_date'].'_'.$_REQUEST['end_date'] . '_accounting_report';
        $data = get_transactions(false);
        header("Content-type: application/vnd.ms-excel; charset=utf-8");
        header("Content-Disposition: attachment; filename=$file_name.xls");

        require_once ROOT_PATH . 'includes/excel/PHPExcel.php';
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        
        $objPHPExcel->getActiveSheet()->setTitle('交易紀錄');
        
        /* 文件标题 */
        $objPHPExcel->getActiveSheet()
        ->setCellValue('A1', ecs_iconv(EC_CHARSET, 'UTF8', $_REQUEST['start_date'] . $_LANG['to'] . $_REQUEST['end_date'] . ' 會計報告'))
        ->setCellValue('A2', ecs_iconv(EC_CHARSET, 'UTF8', '交易編號'))
        ->setCellValue('B2', ecs_iconv(EC_CHARSET, 'UTF8', '帳戶'))
        ->setCellValue('C2', ecs_iconv(EC_CHARSET, 'UTF8', '加入時間'))
        ->setCellValue('D2', ecs_iconv(EC_CHARSET, 'UTF8', '完成時間'))
        ->setCellValue('E2', ecs_iconv(EC_CHARSET, 'UTF8', '銀行時間'))
        ->setCellValue('F2', ecs_iconv(EC_CHARSET, 'UTF8', '備註'))
        ->setCellValue('G2', ecs_iconv(EC_CHARSET, 'UTF8', '貨幣'))
        ->setCellValue('H2', ecs_iconv(EC_CHARSET, 'UTF8', '金額'))
        ->setCellValue('I2', ecs_iconv(EC_CHARSET, 'UTF8', '帳戶結餘'))
        ->setCellValue('J2', ecs_iconv(EC_CHARSET, 'UTF8', '付款方式'))
        ->setCellValue('K2', ecs_iconv(EC_CHARSET, 'UTF8', '會計年份'))
        ->setCellValue('L2', ecs_iconv(EC_CHARSET, 'UTF8', '會計月份'))
        ->setCellValue('M2', ecs_iconv(EC_CHARSET, 'UTF8', '分類'))
        ->setCellValue('N2', ecs_iconv(EC_CHARSET, 'UTF8', '操作員'));
        
        $objPHPExcel->getActiveSheet()->mergeCells('A1:N1');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setVisible(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setVisible(false);

        $i = 3;
        foreach ($data['data'] AS $key => $value)
        {
            $objPHPExcel->getActiveSheet()
            ->setCellValue('A'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['txn_id']))
            ->setCellValue('B'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['account_name']))
            ->setCellValue('C'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['add_date_formatted']))
            ->setCellValue('D'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['complete_date_formatted']))
            ->setCellValue('E'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['bank_date_formatted']))
            ->setCellValue('F'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['remark']))
            ->setCellValue('G'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['currency_code']))
            ->setCellValue('H'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['amount']))
            ->setCellValue('I'.$i, ecs_iconv(EC_CHARSET, 'UTF8', ($data['filter']['txn_type'] == -1) ? 'N/A' : $value['closing_balance']))
            ->setCellValue('J'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['payment_type_name']))
            ->setCellValue('K'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['expense_year']))
            ->setCellValue('L'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['expense_month']))
            ->setCellValue('M'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['txn_type_name']))
            ->setCellValue('N'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['admin_name']));
            $objPHPExcel->getActiveSheet()->getStyle('H'.$i)->getNumberFormat()->setFormatCode('$0.00');
            $objPHPExcel->getActiveSheet()->getStyle('I'.$i)->getNumberFormat()->setFormatCode('$0.00');
            $i++;
        }
        
        for ($col = 'A'; $col <= 'N'; $col++)
        {
            $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
        }
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        
        exit;
    }
    $data = get_transactions();
    $smarty->assign('transactions',    $data['data']);
    $smarty->assign('filter',          $data['filter']);
    $smarty->assign('record_count',    $data['record_count']);
    $smarty->assign('page_count',      $data['page_count']);
    $smarty->assign('start_date',      $_REQUEST['start_date']);
    $smarty->assign('end_date',        $_REQUEST['end_date']);
    $smarty->assign('start_date_formatted', date($_CFG['time_format'], strtotime($_REQUEST['start_date'])));
    $smarty->assign('end_date_formatted', date($_CFG['time_format'], strtotime($_REQUEST['end_date'])));
    
    $sort_flag  = sort_flag($data['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);

    make_json_result($smarty->fetch('accounting_report.htm'), '', array('filter' => $data['filter'], 'page_count' => $data['page_count']));
}
elseif (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'add_txn'))
{
    /* 权限判断 */
    admin_priv('sale_order_stats');
    
    $smarty->assign('ur_here',     '手動新增交易');
    
    $smarty->assign('action_link', array('text' => $_LANG['actg_transactions'], 'href' => 'accounting_report.php?act=list'));
    $smarty->assign('action_link2',  array('text' => '交易分類','href'=>'accounting_report.php?act=txn_type'));
    $salespeople = $adminuserController->getAdminsByPriv(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_PAYMENT_OP, 'sales_name', [], true, true);
    $smarty->assign('salesperson_list', $salespeople);
    $smarty->assign('admin_id', $_SESSION['admin_id']);
    $smarty->assign('admin_name', $_SESSION['admin_name']);
    
    $smarty->assign('accounts', $actg->getAccounts(['is_hidden' => 0]));
    $smarty->assign('payment_types', $actg->getPaymentTypes([], ['where' => 'AND payment_type_id < 4']));
    $smarty->assign('txn_types', $actgHooks->getTransactionTypesTree());
    
    assign_query_info();
    $smarty->display('accounting_txn_info.htm');
}
elseif (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'insert_txn'))
{
    /* 权限判断 */
    admin_priv('sale_order_stats');
    
    $operator = empty($_POST['operator']) ? '' : trim($_POST['operator']);
    if (empty($operator))
    {
        sys_msg('必須輸入操作員');
    }
    
    $account_id = empty($_POST['account_id']) ? 0 : intval($_POST['account_id']);
    if (empty($account_id))
    {
        sys_msg('必須輸入帳戶');
    }
    $acc = $actg->getAccount(compact('account_id'));
    if (empty($acc))
    {
        sys_msg('帳戶不正確');
    }
    
    $currency_code = $acc['currency_code'];
    
    $completed = isset($_POST['completed']) ? (!empty($_POST['completed'])) : true;
    
    $amount = empty($_POST['amount']) ? 0.0 : doubleval($_POST['amount']);
    if (empty($amount))
    {
        sys_msg('必須輸入金額');
    }
    if (($completed) && (!$acc['allow_debit']) && (bccomp(bcadd($amount, $acc['account_balance']), 0) < 0))
    {
        sys_msg('金額超出帳戶結餘');
    }
    
    $payment_type_id = empty($_POST['payment_type_id']) ? 0 : intval($_POST['payment_type_id']);
    if (empty($payment_type_id))
    {
        sys_msg('必須選擇付款方式');
    }
    $pay_type = $actg->getPaymentType(compact('payment_type_id'));
    if (empty($pay_type))
    {
        sys_msg('付款方式不正確');
    }

    $expense_year = empty($_POST['expense_year']) ? null : intval($_POST['expense_year']);
    $expense_month = empty($_POST['expense_month']) ? null : intval($_POST['expense_month']);
    
    $txn_type_id = empty($_POST['txn_type_id']) ? 0 : intval($_POST['txn_type_id']);
    $txn_type = $actg->getTransactionType(compact('txn_type_id'));
    if (empty($txn_type))
    {
        sys_msg('交易分類不正確');
    }
    
    $bank_date = empty($_POST['bank_date']) ? 0 : trim($_POST['bank_date']);
    // $bank_date = empty($_POST['bank_date']) ? 0 : $actg->dbdate(local_strtotime(trim($_POST['bank_date'])));
    
    $remark = empty($_POST['remark']) ? '' : trim($_POST['remark']);
    
    $txn_remark = $remark;
    
    $datetime = local_date("Y-m-d");
    // $datetime = $actg->dbdate();
    
    $actg->start_transaction();

    $double_entries = [
        'from_actg_type_id' => $acc['actg_type_id'],
        'to_actg_type_id' => $txn_type['actg_type_id'],
        'currency_code' => $currency_code,
        'amount' => $amount,
        'txn_date' => $bank_date ? $bank_date : $datetime,
        'remark' => $txn_remark,
        'created_by' => $operator,
    ];
    if ($accountingController->insertDoubleEntryTransactions($double_entries)) {
        $actg->commit_transaction();
        $link[0]['text'] = $_LANG['accounting_system'];
        $link[0]['href'] = 'accounting_system.php?act=list';
        sys_msg('手動新增交易成功', 0, $link, true);
    } else {
        $actg->rollback_transaction();
        sys_msg('手動新增交易失敗');
    }

    // $txn = array(
    //     'add_date' => $datetime,
    //     'complete_date' => $completed ? $datetime : null,
    //     'bank_date' => $bank_date ? $bank_date : null,
    //     'currency_code' => $currency_code,
    //     'amount' => $amount,
    //     'payment_type_id' => $payment_type_id,
    //     'account_id' => $account_id,
    //     'remark' => $txn_remark,
    //     'txn_type_id' => $txn_type_id,
    //     'admin_id' => $operator,
    //     'expense_year' => $expense_year,
    //     'expense_month' => $expense_month
    // );
    // $txn_id = $actg->addTransaction($txn);

    // if ($txn_id)
    // {
    //     $actg->commit_transaction();

    //     $link[0]['text'] = $_LANG['actg_transactions'];
    //     $link[0]['href'] = 'accounting_report.php?act=list';

    //     sys_msg('手動新增交易成功', 0, $link, true);
    // }
    // else
    // {
    //     $actg->rollback_transaction();

    //     sys_msg('手動新增交易失敗');
    // }
}
elseif (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'complete_txn'))
{
    $is_ajax = empty($_REQUEST['is_ajax']) ? 0 : intval($_REQUEST['is_ajax']);
    $showError = function ($msg) use ($is_ajax) {
        if ($is_ajax)
        {
            make_json_error($msg);
        }
        else
        {
            sys_msg($msg);
        }
    };
    $txn_id = empty($_REQUEST['txn_id']) ? 0 : intval($_REQUEST['txn_id']);
    if (empty($txn_id))
    {
        $showError('必須輸入交易編號');
    }
    $txn = $actg->getTransaction(compact('txn_id'));
    if (empty($txn))
    {
        $showError('交易編號不正確');
    }
    
    if ($txn['complete_seq'])
    {
        $showError('交易已於 ' . local_date($GLOBALS['_CFG']['time_format'], strtotime($txn['complete_date'])) . ' 完成');
    }
    
    $actg->start_transaction();
    
    $ok = $actg->completeTransaction($txn_id);
    
    if ($ok)
    {
        $actg->commit_transaction();
        
        if ($is_ajax)
        {
            make_json_result('', '完成交易成功');
        }
        else
        {
            $link[0]['text'] = $_LANG['actg_transactions'];
            $link[0]['href'] = 'accounting_report.php?act=list';
            
            sys_msg('完成交易成功', 0, $link, true);
        }
    }
    else
    {
        $actg->rollback_transaction();
        
        $showError('完成交易失敗');
    }
}
elseif (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'edit_txn_bank_date'))
{
    /* 权限判断 */
    check_authz_json('sale_order_stats');

    $txn_id       = intval($_POST['id']);
    $bank_date    = trim($_POST['val']);
    $bank_date_db = $actg->dbdate(local_strtotime($bank_date));

    $param = array(
        'txn_id' => $txn_id,
        'bank_date' => $bank_date_db
    );

    if ($actg->updateTransaction($param))
    {
        make_json_result(stripslashes($bank_date));
    }
    else
    {
        make_json_error('更改銀行日期失敗');
    }
}
elseif (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'edit_complete_date'))
{
    /* 权限判断 */
    check_authz_json('sale_order_stats');

    $txn_id       = intval($_POST['id']);
    $complete_date    = trim($_POST['val']);
    $complete_date_db = $actg->dbdate(local_strtotime($complete_date));

    $param = array(
        'txn_id' => $txn_id,
        'complete_date' => $complete_date
    );

    if ($actg->updateTransaction($param))
    {
        make_json_result(stripslashes($complete_date));
    }
    else
    {
        make_json_error('更改完成時間失敗');
    }
}
elseif (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'edit_txn_type_id'))
{
    /* 权限判断 */
    check_authz_json('sale_order_stats');

    $txn_id       = intval($_POST['id']);
    $txn_type_id  = intval($_POST['val']);
    
    $txn_type = $actg->getTransactionType(compact('txn_type_id'));
    if (empty($txn_type))
    {
        make_json_error('交易分類不正確');
    }
    
    $param = array(
        'txn_id' => $txn_id,
        'txn_type_id' => $txn_type_id
    );

    if ($actg->updateTransaction($param))
    {
        make_json_result(stripslashes($txn_type['txn_type_name']));
    }
    else
    {
        make_json_error('更改交易分類失敗');
    }
}
elseif (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'add_account'))
{
    /* 权限判断 */
    admin_priv('sale_order_stats');
    
    $smarty->assign('ur_here',     '新增帳戶');
    
    $smarty->assign('action_link', array('text' => '返回帳戶概覽', 'href' => 'accounting_report.php?act=summary'));
    
    $smarty->assign('currencies', $actg->getCurrencies());
    
    assign_query_info();
    $smarty->display('accounting_account_info.htm');
}
elseif (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'insert_account'))
{
    /* 权限判断 */
    admin_priv('sale_order_stats');

    $account_name = empty($_POST['account_name']) ? '' : trim($_POST['account_name']);
    if (empty($account_name))
    {
        sys_msg('必須輸入帳戶名稱');
    }
    
    $account_number = empty($_POST['account_number']) ? '' : trim($_POST['account_number']);
    
    $currency_code = empty($_POST['currency_code']) ? 0 : trim($_POST['currency_code']);
    if (empty($currency_code))
    {
        sys_msg('必須輸入貨幣');
    }
    $currency = $actg->getCurrency(compact('currency_code'));
    if (empty($currency))
    {
        sys_msg('貨幣不正確');
    }
    
    $allow_debit = empty($_POST['allow_debit']) ? 0 : ($_POST['allow_debit'] ? 1 : 0);
    $is_bank = empty($_POST['is_bank']) ? 0 : ($_POST['is_bank'] ? 1 : 0);
    
    $actg->start_transaction();

    $account = array(
        'account_name' => $account_name,
        'account_number' => $account_number,
        'currency_code' => $currency_code,
        'allow_debit' => $allow_debit,
        'is_bank' => $is_bank,
    );
    $account_id = $accountingController->insertNewAccounts($account);

    if ($account_id)
    {
        $actg->commit_transaction();

        $link[0]['text'] = '返回帳戶概覽';
        $link[0]['href'] = 'accounting_report.php?act=summary';

        sys_msg('新增帳戶成功', 0, $link, true);
    }
    else
    {
        $actg->rollback_transaction();

        sys_msg('新增帳戶失敗');
    }
}
elseif (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'edit_account_name'))
{
    /* 权限判断 */
    check_authz_json('sale_order_stats');
    
    $account_id   = intval($_POST['id']);
    $account_name = json_str_iconv(trim($_POST['val']));
    
    $param = array(
        'account_id' => $account_id,
        'account_name' => $account_name
    );
    
    if ($actg->updateAccount($param))
    {
        make_json_result(stripslashes($account_name));
    }
    else
    {
        make_json_error('更改帳戶名稱失敗');
    }
}
elseif (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'edit_account_number'))
{
    /* 权限判断 */
    check_authz_json('sale_order_stats');
    
    $account_id   = intval($_POST['id']);
    $account_number = json_str_iconv(trim($_POST['val']));
    
    $param = array(
        'account_id' => $account_id,
        'account_number' => $account_number
    );
    
    if ($actg->updateAccount($param))
    {
        make_json_result(stripslashes($account_number));
    }
    else
    {
        make_json_error('更改帳戶號碼失敗');
    }
}
elseif (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'edit_account_audit_date'))
{
    /* 权限判断 */
    check_authz_json('sale_order_stats');

    $account_id    = intval($_POST['id']);
    $audit_date    = trim($_POST['val']);
    $audit_date_db = $actg->dbdate(local_strtotime($audit_date));

    $param = array(
        'account_id' => $account_id,
        'audit_date' => $audit_date_db
    );
    
    if ($actg->updateAccount($param))
    {
        make_json_result(stripslashes($audit_date));
    }
    else
    {
        make_json_error('更改最後對數日期失敗');
    }
}
elseif (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'edit_expense_year'))
{
    /* 权限判断 */
    check_authz_json('sale_order_stats');

    $txn_id       = intval($_POST['id']);
    $expense_year = intval($_POST['val']);
    if (empty($expense_year) || $expense_year < 0) {
        make_json_error('年份不正確');
    }

    $param = array(
        'txn_id' => $txn_id,
        'expense_year' => $expense_year
    );

    if ($actg->updateTransaction($param))
    {
        make_json_result(stripslashes($expense_year));
    }
    else
    {
        make_json_error('更改會計年份失敗');
    }
}
elseif (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'edit_expense_month'))
{
    /* 权限判断 */
    check_authz_json('sale_order_stats');

    $txn_id        = intval($_POST['id']);
    $expense_month = intval($_POST['val']);
    if (empty($expense_month) || $expense_month < 0 || $expense_month > 12) {
        make_json_error('月份不正確');
    }

    $param = array(
        'txn_id' => $txn_id,
        'expense_month' => $expense_month
    );

    if ($actg->updateTransaction($param))
    {
        make_json_result(stripslashes($expense_month));
    }
    else
    {
        make_json_error('更改會計月份失敗');
    }
}
elseif (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'edit_remark'))
{
    /* 权限判断 */
    check_authz_json('sale_order_stats');

    $txn_id = intval($_POST['id']);
    $remark = trim($_POST['val']);

    $param = array(
        'txn_id' => $txn_id,
        'remark' => $remark
    );

    if ($actg->updateTransaction($param))
    {
        make_json_result(stripslashes($remark));
    }
    else
    {
        make_json_error('更改會計月份失敗');
    }
}
// howang use only: clear transactions will remove all reversed transactions, and re-calculate all balances
elseif (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'clear_txns'))
{
    /* 权限判断 */
    if ($_SESSION['admin_name'] != 'admin' && !admin_priv('danger_zone_admin'))
    {
        sys_msg($_LANG['priv_error']);
    }
    
    $account_id = 0;
    $since_complete_seq = 0;
    
    if (!empty($_REQUEST['aid']))
    {
        $account_id = intval($_REQUEST['aid']);
    }
    if (!empty($_REQUEST['seq']))
    {
        $since_complete_seq = intval($_REQUEST['seq']);
    }
    
    echo 'Clearing all transactions ';
    if (!empty($account_id)) echo 'for account #' . $account_id . ' ';
    if (!empty($since_complete_seq)) echo 'since complete seq #' . $since_complete_seq . ' ';
    
    $rtn = $actg->clearTransactions($account_id, $since_complete_seq);
    echo ($rtn) ? '[ OK ]' : '[FAIL]';
    exit;
}
// list of transaction types
elseif (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'txn_type'))
{
    /* 权限判断 */
    if (!check_authz('sale_order_stats'))
    {
        sys_msg($_LANG['priv_error']);
    }
    assign_query_info();
    $smarty->assign('ur_here',      '交易分類列表');
    $smarty->assign('full_page',    1);
    $smarty->assign('action_link',  array('text' => '新增交易分類','href'=>'accounting_report.php?act=new_txn_type'));
    $smarty->assign('action_link2', array('text' => $_LANG['actg_transactions'], 'href' => 'accounting_report.php?act=list'));
    $smarty->assign('txn_types',    $actgHooks->getTransactionTypesTree());
    $smarty->display('accounting_txn_list.htm');
}
// edit transaction types
elseif (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'edit_txn_type'))
{
    /* 权限判断 */
    if (!check_authz('sale_order_stats'))
    {
        sys_msg($_LANG['priv_error']);
    }
    assign_query_info();
    $smarty->assign('ur_here',      '編輯交易分類');
    $smarty->assign('action_link',  array('text' => '新增交易分類','href'=>'accounting_report.php?act=new_txn_type'));
    $smarty->assign('action_link2', array('text' => '返回交易分類','href'=>'accounting_report.php?act=txn_type'));
    $smarty->assign('edit',         1);
    $smarty->assign('full_page',    1);
    $smarty->assign('txn_types',    $actgHooks->getTransactionTypesTree());
    $smarty->assign('txn_type',     $_REQUEST['txn_type']);
    $smarty->display('accounting_txn_edit.htm');
}
elseif (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'new_txn_type'))
{
    /* 权限判断 */
    if (!check_authz('sale_order_stats'))
    {
        sys_msg($_LANG['priv_error']);
    }
    assign_query_info();
    $smarty->assign('ur_here',      '新増交易分類');
    $smarty->assign('action_link',  array('text' => '返回交易分類','href'=>'accounting_report.php?act=txn_type'));
    $smarty->assign('edit',         0);
    $smarty->assign('full_page',    1);
    $smarty->assign('txn_types',    $actgHooks->getTransactionTypesTree());
    $smarty->display('accounting_txn_edit.htm');
}
// update transaction types
elseif (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'update_txn_type'))
{
    /* 权限判断 */
    if (!check_authz('sale_order_stats'))
    {
        sys_msg($_LANG['priv_error']);
    }
    $paymentController->update_transaction_type();
    $link[0]['text'] = '返回交易分類列表';
    $link[0]['href'] = 'accounting_report.php?act=txn_type';

    sys_msg('編輯交易分類成功', 0, $link, true);
}
// insert transaction types
elseif (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'insert_txn_type'))
{
    /* 权限判断 */
    if (!check_authz('sale_order_stats'))
    {
        sys_msg($_LANG['priv_error']);
    }
    $link[0]['text'] = '返回交易分類列表';
    $link[0]['href'] = 'accounting_report.php?act=txn_type';
    if($paymentController->insert_transaction_type())
        sys_msg('新增交易分類成功', 0, $link, true);
    else
        sys_msg('新增交易分類失敗', 0, $link, true);
}
// delete transaction types
elseif (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'delete_txn_type'))
{
    /* 权限判断 */
    if (!check_authz('sale_order_stats'))
    {
        return make_json_error("您沒有權限修改這條紀錄");
    }
    $paymentController->delete_transaction_type();
    make_json_response('success');
}
elseif ($_REQUEST['act'] == 'ajaxEdit') {
    $accountingController->ajaxEditAction();//'actg_accounts'
}
else
{
    /* 权限判断 */
    admin_priv('sale_order_stats');
    
    /* 时间参数 */
    $start_date = (empty($_REQUEST['start_date'])) ? month_start() : $_REQUEST['start_date'];
    $end_date = (empty($_REQUEST['end_date'])) ? month_end() : $_REQUEST['end_date'];
    
    $data = get_transactions();
    /* 赋值到模板 */
    $smarty->assign('filter',       $data['filter']);
    $smarty->assign('operators',       $data['admins']);
    $smarty->assign('record_count', $data['record_count']);
    $smarty->assign('page_count',   $data['page_count']);
    $smarty->assign('transactions', $data['data']);
    $smarty->assign('ur_here',      $_LANG['actg_transactions']);
    $smarty->assign('full_page',    1);
    $smarty->assign('start_date',   $start_date);
    $smarty->assign('end_date',     $end_date);
    $smarty->assign('cfg_lang',     $_CFG['lang']);
    $smarty->assign('action_link',  array('text' => '下載報表','href'=>'#download'));
    $smarty->assign('action_link4',  array('text' => '新增交易','href'=>'accounting_report.php?act=add_txn'));
    $smarty->assign('action_link2',  array('text' => '交易分類','href'=>'accounting_report.php?act=txn_type'));
    $smarty->assign('action_link3', array('text' => '新增轉帳', 'href' => 'currency_exchanges.php?act=add'));
    
    $smarty->assign('accounts',     $actg->getAccounts());
    $smarty->assign('payment_types',$actg->getPaymentTypes());
    $txnTypes = $actgHooks->getTransactionTypesTree();
    $smarty->assign('txn_types',    $txnTypes);
    
    $arr = array();
    foreach ($txnTypes as $type)
    {
        $arr[] = array('name' => $type['txn_type_name'], 'value' => $type['txn_type_id']);
        foreach ($type['children'] as $subtype)
        {
            $arr[] = array('name' => '　' . $subtype['txn_type_name'], 'value' => $subtype['txn_type_id']);
        }
    }
    $smarty->assign('txn_types_json', json_encode($arr));
    
    $sort_flag  = sort_flag($data['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);
    
    /* 显示页面 */
    assign_query_info();
    $smarty->display('accounting_report.htm');
}

function month_start()
{
    return local_date('Y-m-01');
}

function month_end()
{
    return local_date('Y-m-t');
}

function get_transactions($is_pagination = true)
{
    global $actg, $db;
    
    /* 排序参数 */
    $filter['sort_by']      = empty($_REQUEST['sort_by']) ? 'add_date' : trim($_REQUEST['sort_by']);
    $filter['sort_order']   = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
    
    /* 时间参数 */
    $filter['start_date'] = empty($_REQUEST['start_date']) ? month_start() : $_REQUEST['start_date'];
    $filter['end_date'] = empty($_REQUEST['end_date']) ? month_end() : $_REQUEST['end_date'];
    $start_date = $actg->dbdate(local_strtotime($filter['start_date']));
    $end_date = $actg->dbdate(local_strtotime($filter['end_date']) + 86399);
    $filter['expense_year'] = empty($_REQUEST['expense_year']) ? '' : $_REQUEST['expense_year'];
    $filter['expense_month'] = empty($_REQUEST['expense_month']) ? '' : $_REQUEST['expense_month'];
    
    /* 其他參數 */
    $filter['account'] = empty($_REQUEST['account']) ? 0 : intval($_REQUEST['account']);
    $filter['payment_type'] = empty($_REQUEST['payment_type']) ? 0 : intval($_REQUEST['payment_type']);
    $filter['txn_type'] = empty($_REQUEST['txn_type']) ? 0 : intval($_REQUEST['txn_type']);
    $filter['txn_remark'] = empty($_REQUEST['txn_remark']) ? '' : trim($_REQUEST['txn_remark']);
    $filter['txn_type_id'] = isset($_REQUEST['txn_type_id']) ? intval($_REQUEST['txn_type_id']) : -1;
    $filter['show_completetime'] = isset($_REQUEST['show_completetime']) ? intval($_REQUEST['show_completetime']) : 0;
    $filter['show_bankdate'] = isset($_REQUEST['show_bankdate']) ? intval($_REQUEST['show_bankdate']) : 0;
    $filter['show_reversetxn'] = isset($_REQUEST['show_reversetxn']) ? intval($_REQUEST['show_reversetxn']) : 0;
    $filter['admin_id'] = isset($_REQUEST['admin_id']) ? intval($_REQUEST['admin_id']) : -1;

    /* 查询数据的条件 */
    if (!empty($filter['expense_year']) || !empty($filter['expense_month'])) {
        !empty($filter['expense_year']) ? $acc_time[] = "t.expense_year = '" . $filter['expense_year'] . "'" : true;
        !empty($filter['expense_month']) ? $acc_time[] = "t.expense_month = '" . $filter['expense_month'] . "'" : true;
    }

    if (!empty($filter['txn_remark'])) {
        preg_match('/ACC(\d+)/', $filter['txn_remark'], $txn_id);
        if (!empty($txn_id)) {
            $formatted_txn_id = $txn_id[1];
        }
    }
    $log_time = $filter['txn_type'] >= 0 ? "(t.complete_date BETWEEN '" . $start_date . "' AND '" . $end_date . "')" :  "(t.add_date BETWEEN '" . $start_date . "' AND '" . $end_date . "')";
    $log_time = !empty($acc_time) ? "((" . implode(" AND ", $acc_time) . ") OR $log_time)" : $log_time;
    $where = (($filter['txn_type'] >= 0) ? $log_time : "$log_time AND (t.complete_seq = 0 OR t.complete_seq IS NULL) ") .
        (empty($filter['account']) ? '' : " AND t.account_id = '" . $filter['account'] . "' ") .
        (empty($filter['payment_type']) ? '' : " AND t.payment_type_id = '" . $filter['payment_type'] . "' ") .
        (($filter['txn_type'] <= 0) ? '' : " AND t.amount " . ($filter['txn_type'] == 1 ? '>' : '<') . " 0 ") . 
        (($filter['admin_id'] < 0) ? '' : " AND t.admin_id = '" . $filter['admin_id'] . "'") .
        (empty($filter['txn_remark']) ? '' : " AND (t.remark LIKE '%" . mysql_like_quote($filter['txn_remark']) . "%'" . (is_numeric($filter['txn_remark']) ? " OR t.amount = $filter[txn_remark] OR t.amount = $filter[txn_remark] * -1 OR t.txn_id = $filter[txn_remark]" : "") . (empty($formatted_txn_id) ? "" : " OR t.txn_id = $formatted_txn_id") . ")") .
        (($filter['txn_type_id'] < 0) ? '' : " AND (t.txn_type_id = '" . $filter['txn_type_id'] . "' OR tt.txn_type_parent = '" . $filter['txn_type_id'] . "') ");

    $sql = "SELECT count(*) " .
            "FROM `actg_account_transactions` as t " .
            "LEFT JOIN `actg_transaction_types` as tt ON t.txn_type_id = tt.txn_type_id " .
            "WHERE " . $where;
    $filter['record_count'] = $db->getOne($sql);

    /* 分页大小 */
    $filter = page_and_size($filter);
    
    $opt = array();
    $opt['orderby'] = $filter['sort_by'] . ' ' . $filter['sort_order'];
    // If we are listing incomplete transaction, cannot sort by completion sequence
    if (($filter['txn_type'] < 0) && ($filter['sort_by'] == 'complete_seq'))
    {
        $opt['orderby'] = 'add_date ' . $filter['sort_order'];
    }
    if ($is_pagination)
    {
        $opt['limit'] = $filter['start'] . ',' . $filter['page_size'];
    }
    
    /* 查询 */
    $data = $actg->getTransactions($where, $opt);

    if($filter['show_reversetxn'] != 1){
        $sql = 'SELECT related_txn_id FROM `actg_account_transactions` WHERE remark LIKE "Reverse transaction%"';
        $reverse = $db->getCol($sql);
    }
    $restricted_txn_types = [1, 4, 44, 46];         // Sales, Purchase, Internal Transfer, Refund
    foreach ($data as $key => $row)
    {
        $data[$key]['add_date_formatted'] = local_date($GLOBALS['_CFG']['time_format'], strtotime($row['add_date']));
        $data[$key]['complete_date_formatted'] = local_date($GLOBALS['_CFG']['time_format'], strtotime($row['complete_date']));
        $data[$key]['bank_date_formatted'] = local_date($GLOBALS['_CFG']['time_format'], strtotime($row['bank_date']));
        $data[$key]['amount_formatted'] = $actg->money_format($row['amount'], $row['currency_format']);
        $data[$key]['opening_balance_formatted'] = $actg->money_format($row['opening_balance'], $row['currency_format']);
        $data[$key]['closing_balance_formatted'] = $actg->money_format($row['closing_balance'], $row['currency_format']);
        $data[$key]['admin_name'] = empty($row['admin_id']) ? "系統生成" : $row['user_name'];
        if (!(in_array(intval($row['txn_type_id']), $restricted_txn_types) || in_array($row['txn_id'], $reverse) || strpos($row['remark'], 'Reverse transaction') !== false)) {
            $data[$key]['can_edit'] = 1;
        }
        // Process everything before this line
        if($filter['show_reversetxn'] != 1 && (in_array($row['txn_id'],$reverse) || strpos($row['remark'], 'Reverse transaction') !== false)){
            $filter['record_count']--;
            unset($data[$key]);
        }
    }

    $admins = $db->getAll("SELECT au.user_id, au.user_name FROM `actg_account_transactions` t LEFT JOIN " . $GLOBALS['ecs']->table('admin_user') . " au ON t.admin_id = au.user_id WHERE t.admin_id != 0 GROUP BY t.admin_id");
    
    $arr = array(
        'data' => $data,
        'admins' => $admins,
        'filter' => $filter,
        'page_count' => $filter['page_count'],
        'record_count' => $filter['record_count']
    );
    return $arr;
}

function get_supplier($order_sn){
    $sql = "SELECT es.name, eo.supplier_id FROM " . $GLOBALS['ecs']->table('erp_order') . " eo " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('erp_supplier') . " es ON eo.supplier_id = es.supplier_id " .
            "WHERE eo.order_sn = '$order_sn'";
    return $GLOBALS['db']->getRow($sql);
}

?>