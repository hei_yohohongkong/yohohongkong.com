<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require_once(ROOT_PATH . 'includes/lib_order.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_base.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_warehouse.php');
require(dirname(__FILE__) .'/includes/ERP/cls/cls_date.php');
$orderController = new Yoho\cms\Controller\OrderController();
$adminuserController = new Yoho\cms\Controller\AdminuserController();
$deliveryOrderController = new Yoho\cms\Controller\DeliveryOrderController();


if($_REQUEST['act'] == 'list' || $_REQUEST['act'] == ''){
	if( admin_priv('erp_warehouse_view','',false) || admin_priv('erp_warehouse_manage','',false) || admin_priv('erp_warehouse_approve','',false)) {
	    include('./includes/ERP/page.class.php');
		$operators = $deliveryOrderController->operatorWarehouseOption(true);
		$logistics = $deliveryOrderController->logisticOptions();

		$smarty->assign('logistics', $logistics);
		$smarty->assign('operators', $operators);
        $smarty->assign('full_page', 1);
        $smarty->assign('cfg_lang', $_CFG['lang']);
        $smarty->assign('lang', $_LANG);
        $smarty->display('order_express_collected_import.htm');
    } else {
        $href="index.php";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
    }

} elseif ($_REQUEST['act'] == 'post_order_express_collected_import') {
    $result = $deliveryOrderController->markExpressCollected();
	if (isset($result['error'])) {
        make_json_error(implode('<br>', $result['error']));
    } elseif ($result == false) {
        make_json_error('發生錯誤');
    } else {
        make_json_result($result);
    }
} elseif ($_REQUEST['act'] == 'post_order_express_collected_send_email') {
    $result = $deliveryOrderController->sendEmailAfterExpressCollected();
	if (isset($result['error'])) {
        make_json_error(implode('<br>', $result['error']));
    } elseif ($result == false) {
        make_json_error('發生錯誤');
    } else {
        make_json_result($result);
    }
} 


?>