<?php
/**
 * ad_campaign.php
 * $Author: Anthony@YOHO
 * $date: 20/06/2017
*/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/admin/ads.php');

// define controller
$adCampaignController = new Yoho\cms\Controller\AdCampaignController();

// define admin priv
$admin_priv = $adCampaignController::DB_ACTION_CODE_AD_MANAGE;

// define $_REQUEST
$act  = empty($_REQUEST['act']) ? 'list' : $_REQUEST['act'];
$lang = $_REQUEST['edit_lang'];

// check admin private
admin_priv($admin_priv);
use Yoho\cms\Model;
switch($act){
    /* ------------------------------------------------------ */
    //-- List Ad campaign
    /* ------------------------------------------------------ */
    case 'list':

        $data = $adCampaignController->get_campaign_list($lang);
        assign_query_info();
        $smarty->assign('ur_here', $_LANG['campaign_list']);
        $smarty->assign('action_link', array('text' => $_LANG['ad_list'], 'href' => 'ads.php?act=list'));
        $smarty->assign('action_link2', array('text' => $_LANG['add_campaign'], 'href' => 'ad_campaign.php?act=add'));
        $smarty->assign('full_page', 1);
        
        // list
        $smarty->assign('list'        , $data['data']);
        $smarty->assign('filter'      , $data['filter']);
        $smarty->assign('record_count', $data['record_count']);
        $smarty->assign('page_count'  , $data['page_count']);
        
        $sort_flag = sort_flag($data['filter']);
        $smarty->assign($sort_flag['tag'], $sort_flag['img']);
        
        assign_query_info();

        $smarty->display('ad_campaign_list.htm');
        break;
    /* ------------------------------------------------------ */
    //-- Query Ad campaign
    /* ------------------------------------------------------ */
    case 'query':
        $list = $adCampaignController->get_campaign_list();
        $smarty->assign('list'        , $list['data']);
        $smarty->assign('filter'      , $list['filter']);
        $smarty->assign('record_count', $list['record_count']);
        $smarty->assign('page_count'  , $list['page_count']);

        $sort_flag = sort_flag($list['filter']);
        $smarty->assign($sort_flag['tag'], $sort_flag['img']);

        make_json_result($smarty->fetch('ad_campaign_list.htm'), '', array('filter' => $list['filter'], 'page_count' => $list['page_count']));
        break;
    /* ------------------------------------------------------ */
    //-- Add Ad campaign
    /* ------------------------------------------------------ */
    case 'add':
        
        $start_time = local_date('Y-m-d 00:00:00');
        // Default end time is +30 days.
        $end_time   = local_date('Y-m-d 23:59:59', gmtime() + 3600 * 24 * 30);
        
        $smarty->assign('campaign'    , array('start_time' => $start_time,'end_time'=>$end_time, 'enabled'=>1));
        $smarty->assign('ur_here'     , $_LANG['campaign_add']);
        $smarty->assign('action_link' , array('href' => 'ad_campaign.php?act=list', 'text' => $_LANG['campaign_list']));
        $smarty->assign('action_link2', array('text' => $_LANG['ad_list'], 'href' => 'ads.php?act=list'));
        $smarty->assign('form_act'    , 'submit_campaign');
        $smarty->assign('action'      , $act);
        $smarty->assign('cfg_lang'    , $_CFG['lang']);
    
        assign_query_info();
        
        $smarty->display('ad_campaign_info.htm');
        break;
    /* ------------------------------------------------------ */
    //-- Edit Ad campaign
    /* ------------------------------------------------------ */
    case 'edit':
        
        $campaign = $adCampaignController->get_campaign($_REQUEST['id']);
        $smarty->assign('campaign'    , $campaign);
        $smarty->assign('ur_here'     , $_LANG['campaign_edit']);
        $smarty->assign('action_link' , array('href' => 'ad_campaign.php?act=list', 'text' => $_LANG['campaign_list']));
        $smarty->assign('action_link2', array('text' => $_LANG['ad_list'], 'href' => 'ads.php?act=list'));
        $smarty->assign('form_act'    , 'submit_campaign');
        $smarty->assign('action'      , $act);
        $smarty->assign('cfg_lang'    , $_CFG['lang']);
        
        assign_query_info();
        
        $smarty->display('ad_campaign_info.htm');
        break;
    /* ------------------------------------------------------ */
    //-- Submit campaign form action
    /* ------------------------------------------------------ */
    case 'submit_campaign':
        
        $result = $adCampaignController->insert_update_campaign($_REQUEST);
        if($result)
        {
            $action = $_POST['action'];
            $name = $_POST['ad_campaign_name'];
            $tablename ='ad_campaign';

            // 记录日志
            admin_log($name, $action, $tablename);
            $link[] = array('text' => $_LANG['back_list'], 'href' => 'ad_campaign.php?act=list');
            sys_msg($_LANG[$action.'_ok'], 0, $link);
        } else {
            sys_msg($_LANG['campaign_not_available']);
        }

        break;
}

 ?>
