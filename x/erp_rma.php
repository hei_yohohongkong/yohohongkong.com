<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_base.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_warehouse.php');
require(dirname(__FILE__) .'/includes/ERP/cls/cls_date.php');
$rmaController = new Yoho\cms\Controller\RmaController();
$adminuserController = new Yoho\cms\Controller\AdminuserController();

if($_REQUEST['act'] == 'list' || $_REQUEST['act'] == ''){
	if( admin_priv('erp_warehouse_view','',false) || admin_priv('erp_warehouse_manage','',false) || admin_priv('erp_warehouse_approve','',false))
	{
        include('./includes/ERP/page.class.php');

		// get myself rma issue count
		$issue_count_list = $rmaController->getIssueCountByPerson();
		$smarty->assign('issue_count_list', $issue_count_list);
		$smarty->assign('type_options', $rmaController->getTypeOptions());
        $smarty->assign('status_options', $rmaController->getStatusOptions());
		$smarty->assign('department_options', $rmaController->getDepartmentOptions());
		$smarty->assign('operator_options',$rmaController->personInChargeOption(false));
		$smarty->assign('channel_options',$rmaController->getChannelOptions());
        $smarty->assign('ur_here', 'RMA 列表');
        $smarty->assign('full_page', 1);
        $smarty->assign('start_date', $result['start_date']);
        $smarty->assign('end_date', $result['end_date']);
        $smarty->assign('cfg_lang', $_CFG['lang']);
        $smarty->assign('lang', $_LANG);
        $smarty->display('erp_rma_list.htm');
    }else{
        $href="index.php";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
    }

} elseif ($_REQUEST['act'] == 'query') {
    $result = $rmaController->getRmaList();
    $rmaController->ajaxQueryAction($result['data'],$result['record_count'],false);
} elseif ($_REQUEST['act'] == 'post_confirm_pickup') {
	$rma_id = $_REQUEST['rma_id'];
    $result = $rmaController->confirmPickup($rma_id);
	if($result !== false){
		make_json_result($result);
	}else{
		make_json_error('更新時發生錯誤');
	}
} elseif ($_REQUEST['act'] == 'post_rma_type_change') {
    $result = $rmaController->rmaTypeChange();
	if($result !== false){
		make_json_result($result);
	}else{
		make_json_error('更新時發生錯誤');
	}
} elseif ($_REQUEST['act'] == 'post_rma_bad_goods_repair_submit') {
    $result = $rmaController->rmaBadGoodsRepairSubmit();
	if(isset($result['error'])){
		make_json_error($result['error']);
	}else{
		make_json_result($result);
	}
} elseif ($_REQUEST['act'] == 'update_goods_admin') {
	if (!empty($_REQUEST['old_goods_admin'])) {
		$sql = "SELECT * FROM  ".$ecs->table('erp_rma')." where goods_admin = ".$_REQUEST['old_goods_admin']." ORDER BY `ecs_erp_rma`.`rma_id` DESC";
		$result = $db->getAll($sql);
		foreach ($result as $item) {
			$goods_user_admin = $rmaController->getGoodsUserAdmin($item['goods_id']);
			if ($goods_user_admin > 0) {
                $sql = "update ".$ecs->table('erp_rma')." set goods_admin = '".$goods_user_admin."' where rma_id = '".$item['rma_id']."' ";
				$db->query($sql);
				echo 'Rma ID '.$item['rma_id'] . 'update success';
				echo '<br>';
			}
		}
		
		exit;
	}
}



?>