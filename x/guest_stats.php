<?php

/**
 * ECSHOP 客户统计
 * ============================================================================
 * 版权所有 2005-2010 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liuhui $
 * $Id: guest_stats.php 17063 2010-03-25 06:35:46Z liuhui $
*/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require_once(ROOT_PATH . 'includes/lib_order.php');
require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/admin/statistic.php');
$userController = new Yoho\cms\Controller\UserController();
/* act操作项的初始化 */
if (empty($_REQUEST['act']))
{
    $_REQUEST['act'] = 'list';
}
else
{
    $_REQUEST['act'] = trim($_REQUEST['act']);
}

/*------------------------------------------------------ */
//-- 客户统计列表
/*------------------------------------------------------ */
if ($_REQUEST['act'] == 'list')
{
    /* 权限判断 */
    admin_priv('client_flow_stats');

    $new_data = [];
    $where =  "";
    $where_u = "";
    $where_ex_wssa = " AND (oi.order_type NOT IN ('" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT . "','" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE . "') OR (oi.order_type IS NULL AND oi.user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).") ) ) " .
         " AND (oi.order_type <> '" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT . "' OR oi.order_type IS NULL) ";
    $where_exclude = " AND oi.postscript NOT LIKE '%換貨，%' AND " . order_amount_field("oi.") . " > 0";
    if(isset($_REQUEST['reg_start_date']) && $_REQUEST['reg_start_date'] != ""){
        $where_u .= " AND u.reg_time >= " . strtotime($_REQUEST['reg_start_date']);
        $smarty->assign('reg_start_date',$_REQUEST['reg_start_date']);
    }
    if(isset($_REQUEST['reg_end_date']) && $_REQUEST['reg_end_date'] != ""){
        $where_u .= " AND u.reg_time <" . strtotime($_REQUEST['reg_end_date']);
        $smarty->assign('reg_end_date',$_REQUEST['reg_end_date']);
    }
    if(isset($_REQUEST['order_start_date']) && $_REQUEST['order_start_date'] != ""){
        $where .= " AND oi.shipping_time >= " . strtotime($_REQUEST['order_start_date']);
        $smarty->assign('order_start_date',$_REQUEST['order_start_date']);
    }
    if(isset($_REQUEST['order_end_date']) && $_REQUEST['order_end_date'] != ""){
        $where .= " AND oi.shipping_time < " . strtotime($_REQUEST['order_end_date']);
        $where_u .= " AND u.reg_time < " . strtotime($_REQUEST['order_end_date']);
        $smarty->assign('order_end_date',$_REQUEST['order_end_date']);
    }
    
    /* 取得会员总数 */
    $users      =& init_users();
    $sql = "SELECT COUNT(*) FROM " . $ecs->table("users") . ' u where 1 ' . $where_u;
    $res = $db->getCol($sql);
    $user_num   = $res[0];

    /* 计算订单各种费用之和的语句 */
    $total_fee = " SUM(" . order_amount_field('oi.') . ") AS turnover ";

    if ($_GET['flag'] == 'download' || empty($_REQUEST['step']) || $_REQUEST['step'] == '4') {
        /* 網上註冊會員數目 */

        // UPDATE 20170929: New method to sort online registration
        // Calculate number of users with same reg_time and add_time
        $offline_register_user = 0;
        $sql = 'SELECT COUNT(DISTINCT oi.user_id) as user_num FROM '.$ecs->table('order_info') . ' oi '.
                    'LEFT JOIN '.$ecs->table('users') . ' u ON u.user_id = oi.user_id '.
                    'WHERE (u.reg_time = oi.add_time OR u.email REGEXP "^\\\+?[0-9-]+(@yohohongkong.com)$") AND oi.platform = "pos"' . $where_u .
                    ' AND u.user_id NOT IN ('.implode(',', $userController->getWholesaleUserIds()).')' . $where_exclude;
        $new_data['off_reg_log_new'] = $db->getOne($sql);

        $sql = "SELECT COUNT(*) FROM ".$ecs->table('users') . " u WHERE user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).")". $where_u;
        $users_ex_ws = $db->getOne($sql);
        $new_data['online_register_user'] = $users_ex_ws - $new_data['off_reg_log_new'];
    }
    if ($_GET['flag'] == 'download' || empty($_REQUEST['step'])) {
        /* 取得男女比例 */
        $sql = "SELECT c.sex, COUNT(*) AS num FROM (" .
            "SELECT CASE WHEN u.sex = 1 THEN '男' WHEN u.sex = 2 THEN '女' END AS sex FROM ". $ecs->table("users") . " u WHERE u.sex != 0 " . $where_u .
            ") c GROUP by c.sex";
        $new_data['sex'] = $db->getAll($sql);
        /* 批發會員人數 */
        $sql = "SELECT COUNT(*) AS user_num FROM " . $ecs->table("users") . " u" .
        " WHERE u.is_wholesale = 1" . $where_u;
        $wholesale_user_num = $db->getRow($sql);

        /* 已驗證會員數目 */
        $validated_user = array();
        $sql = 'SELECT COUNT(*) AS user_num FROM ' .$ecs->table('users'). ' u '.
            " WHERE is_validated = 1".$where_u;
        $validated_user = $db->getRow($sql);

        /* 使用語言分佈 */
        $language_list = array();
        $sql = 'SELECT DISTINCT IF(u.language = "","",u.language) AS language FROM ' .$ecs->table('users') . ' u WHERE 1 '. $where_u .' ORDER BY u.language DESC';
        $language_list = $db->getAll($sql);
        foreach ($language_list as $language) {
        $l = $language[language];
        $sql = 'SELECT COUNT(*) AS user_num FROM ' .$ecs->table('users'). ' u ' .
                ' WHERE language = "'.$l.'"' . $where_u;
        $language_num = $db->getRow($sql);
        if ($l == "")
            $language_list_string .= "未知/";
        else if ($l == "zh_tw")
            $language_list_string .= "繁體中文/";
        else if ($l == "en_us")
            $language_list_string .= "英文/";
        $language_distribution_string .= $language_num['user_num'] . "/";
        }

        /* 代理銷售引入的會員數目 */
        $sales_agency_introduce_member = 0;
        $sql = 'SELECT COUNT(DISTINCT oi2.user_id) AS user_num FROM ' . $ecs->table('order_info') . ' oi2' .
            ' INNER JOIN (SELECT DISTINCT oi.user_id FROM ' . $ecs->table('order_info') . ' oi' .
                ' LEFT JOIN ' . $ecs->table('users') . ' u ON u.user_id = oi.user_id' .
                ' WHERE oi.order_id = (SELECT MIN(order_id) FROM' . $ecs->table('order_info') . ' WHERE user_id = oi.user_id) AND oi.order_type = "sa"'. $where . $where_u . $where_exclude .
            ') u2 ON u2.user_id = oi2.user_id' .
            ' WHERE (oi2.order_type IS NULL || (oi2.order_type NOT IN ("ws","sa")))';
            //' GROUP BY t2.user_id in (SELECT DISTINCT user_id FROM ' .$ecs->table('order_info') .'))'
        $res = $db->getRow($sql);
        $sales_agency_introduce_member = $res['user_num'];

        /* 曾登入系統會員數目 */
        $logined_user = array();
        $sql = 'SELECT COUNT(*) AS user_num FROM ' .$ecs->table('users'). ' u '.
            ' WHERE last_login != 0'.$where_u;
        $logined_user = $db->getRow($sql);

        /* 已填電郵會員數目 */
        $email_completed_user = array();
        $sql = 'SELECT COUNT(*) AS user_num FROM ' .$ecs->table('users').' u '.
            " WHERE u.email NOT REGEXP '^\\\+?[0-9-]+(@yohohongkong.com)$'".$where_u;
        $email_completed_user = $db->getRow($sql);

        /* 已填資料會員數目 */
        $info_completed_user = array();
        $sql = 'SELECT COUNT(*) AS user_num FROM ' .$ecs->table('users').' u '.
            " WHERE u.info_completed = 1".$where_u;
        $info_completed_user = $db->getRow($sql);

    }
    if ($_GET['flag'] == 'download' || $_REQUEST['step'] == '1') {
        /* 有过订单的会员数 */
        $sql = 'SELECT COUNT(DISTINCT user_id) FROM ' .$ecs->table('order_info'). ' oi ' .
            " WHERE oi.user_id > 0 " . order_query_sql('finished','oi.') . $where .
            " AND oi.user_id IN (SELECT user_id FROM " .$ecs->table('users'). " u WHERE 1 " . $where_u . $where_exclude .")";
        $have_order_usernum = $db->getOne($sql);

        /* 会员订单总数和订单总购物额 */
        $user_all_order = array();
        $sql = "SELECT COUNT(*) AS order_num, " . $total_fee.
            "FROM " .$ecs->table('order_info'). ' oi '.
            " WHERE oi.user_id > 0 " . order_query_sql('finished','oi.').$where .
            " AND oi.user_id IN (SELECT user_id FROM " .$ecs->table('users'). " u WHERE 1 " . $where_u . ")";
        $user_all_order = $db->getRow($sql);
        $user_all_order['turnover'] = floatval($user_all_order['turnover']);

        /* 匿名会员订单总数和总购物额 */
        $guest_all_order = array();
        $sql = "SELECT COUNT(*) AS order_num, " . $total_fee.
            "FROM " .$ecs->table('order_info'). ' oi '.
            " WHERE oi.platform = 'pos' AND oi.user_id = 0 " . order_query_sql('finished','oi.') . $where . $where_exclude;
        if(isset($_REQUEST['reg_start_date']) && $_REQUEST['reg_start_date'] != "")
            $sql .= " AND oi.shipping_time >= " .strtotime($_REQUEST['reg_start_date']);
        if(isset($_REQUEST['reg_end_date']) && $_REQUEST['reg_end_date'] != "")
            $sql .= " AND oi.shipping_time < " .strtotime($_REQUEST['reg_end_date']);
        $guest_all_order = $db->getRow($sql);

        /* 匿名会员平均订单额: 购物总额/订单数 */
        $guest_order_amount = ($guest_all_order['order_num'] > 0) ? floatval($guest_all_order['turnover'] / $guest_all_order['order_num']) : '0.00';
        // Dem: 20170925 update
        $left_join = " LEFT JOIN " .$ecs->table('users'). "u ON u.user_id = oi.user_id ";
        //$where = " AND (u.user_name NOT LIKE '%11111111%' OR (u.user_name LIKE '%11111111%' AND " . order_amount_field("oi.") . " > 0)) AND (u.user_name NOT LIKE '%77777777%' OR (u.user_name LIKE '%77777777%' AND " . order_amount_field("oi.") . " > 0)) ";

        /* Basket value */
        $where_basket = ["",$where_ex_wssa,
            " AND (oi.order_type = '" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE . "' OR oi.user_id IN (".implode(',', $userController->getWholesaleUserIds())."))",
            " AND oi.order_type = '" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT . "'",
            " AND oi.platform = 'pos'",
            " AND oi.platform = 'pos'" . $where_ex_wssa,
            " AND oi.platform = 'web'",
            " AND oi.platform = 'mobile'",
        ];
        foreach($where_basket as $k => $v){
            $new_data['user_all_order'][$k] = array();
            $sql = "SELECT COUNT(*) AS order_num, " . $total_fee.
                "FROM " .$ecs->table('order_info'). ' oi ' . $left_join .
                " WHERE oi.user_id > 0 " . order_query_sql('finished','oi.') . $where_exclude . $where . $v.
                " AND oi.user_id IN (SELECT user_id FROM " .$ecs->table('users'). " u WHERE 1 " . $where_u . ")";
            $new_data['user_all_order'][$k] = $db->getRow($sql);
            $new_data['user_all_order'][$k]['turnover'] = floatval($new_data['user_all_order'][$k]['turnover']);
            $new_data['user_all_order'][$k]['basket'] = $new_data['user_all_order'][$k]['order_num'] > 0 ? price_format($new_data['user_all_order'][$k]['turnover'] / $new_data['user_all_order'][$k]['order_num']) : 0;
        }
    }
    if ($_GET['flag'] == 'download' || $_REQUEST['step'] == '2') {
        /* Purchase cycle */
        /*
        $where_cycle = [" GROUP BY oi.user_id HAVING COUNT(*) = 1) a",
            " GROUP BY oi.user_id HAVING COUNT(*) > 1 AND each_dev > 1) a",
            " GROUP BY oi.user_id HAVING COUNT(*) > 1 AND each_dev > 0.5) a",
            " GROUP BY oi.user_id HAVING COUNT(*) > 2 AND each_dev > 1) a",
            " GROUP BY oi.user_id HAVING COUNT(*) > 2 AND each_dev > 0.5) a",
            " GROUP BY oi.user_id HAVING COUNT(*) = 2 AND each_dev > 1) a",
            " GROUP BY oi.user_id HAVING COUNT(*) = 2 AND each_dev > 0.5) a",
            " GROUP BY oi.user_id HAVING COUNT(*) > 1 AND each_dev > 0 AND each_dev < 1) a"
        ];
        foreach($where_cycle as $k => $v){
            $new_data['purchase_cycle'][$k] = array();
            $sql = "SELECT COALESCE(STD(each_dev),0) AS dev from (SELECT oi.user_id,STD(shipping_time/86400) as each_dev FROM " . $ecs->table('order_info'). " oi ";
            if($k == 0 || $k == 7)
                    $sql = "SELECT COUNT(*) AS count from (SELECT oi.user_id,STD(oi.shipping_time/86400) as each_dev FROM " . $ecs->table('order_info'). " oi ";
            else if($k > 0 && $k < 7)
                    $sql = "SELECT COUNT(*) AS count, COALESCE(STD(each_dev),0) AS dev from (SELECT user_id,STD(shipping_time/86400) as each_dev FROM " . $ecs->table('order_info'). " oi ";
                    $sql .= "WHERE oi.user_id != 0 " . //$where_ex_wssa .
                    " AND (oi.order_type != '" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE . "' OR oi.user_id NOT IN (".implode(',', $userController->getWholesaleUserIds())."))" .
                    " AND oi.shipping_time > 0 " . $where . $where_exclude .
                    " AND oi.user_id IN (SELECT user_id FROM " .$ecs->table('users'). " u WHERE 1 " . $where_u . ")" . $v;
            if($k == 0){
                    $new_data['norebuy'] = $db->getOne($sql);
            }else if($k > 0 && $k < 7){
                    $res = $db->getRow($sql);
                    $new_data['purchase_cycle'][$k]['cycle'] = number_format($res['dev'],2);
                    $new_data['rebuy'][$k] = $res['count'];
            }else if($k == 7){
                    $new_data['rebuy_day'] = $db->getOne($sql);
            }else{
                    $new_data['purchase_cycle'][$k]['cycle'] = number_format($db->getOne($sql),2);
            }
        }
        $new_data['dev_1d_avg'] = weightedAverage($new_data,3,5);
        $new_data['dev_12h_avg'] = weightedAverage($new_data,4,6);*/
        $now  = local_strtotime("now");
        $past_one_month = local_strtotime("-1 months");
        $past_six_month = local_strtotime("-6 months");
        $pc_label = [];
        $pc_label[] = '30天內';
        $pc_label[] = '30 - 59天';
        $pc_label[] = '60天或以上';
        // No order :
        $sql = "SELECT us.buy_total, COUNT(us.buy_total) as count FROM (" .
            "SELECT CASE" .
                " WHEN count(oi.order_id) = 0 THEN 0" .
                " WHEN count(oi.order_id) = 1 THEN 1" .
                " WHEN count(oi.order_id) > 1 THEN 2" .
                " ELSE 3 ".
            " END AS buy_total ".
            " FROM " . $ecs->table('users') ." as u ".
            " LEFT JOIN " . $ecs->table('order_info') ." as oi ON oi.user_id = u.user_id ".
            " AND order_status NOT IN (" . OS_CANCELED . ',' . OS_INVALID . ',' . OS_RETURNED . ") ".
            " AND pay_status = " . PS_PAYED . " ".
            " WHERE u.user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).")" . $where_u .
            " GROUP BY u.user_id ) us GROUP BY us.buy_total";
        $pc_data = $db->getAll($sql);
        $total = 0;
        $pc_result = [];
        foreach($pc_data as $key => $value) {
            $total += $value['count'];
            if($value['buy_total'] == 0 )$pc_result['zero'] = $value['count'];
            else if($value['buy_total'] == 1 )$pc_result['one']  = $value['count'];
            else if($value['buy_total'] == 2 )$pc_result['two_more']  = $value['count'];
            else if($value['buy_total'] == 3 )$pc_result['other']  = $value['count'];
        }
        $pc_result['total'] = $total;
        unset($total);

        $smarty->assign('pc_count',     $pc_result);

        /* Only 1 order */
        $sql = "SELECT us.buy_total, COUNT(us.buy_total) as count FROM (" .
            "SELECT CASE" .
                " WHEN ($now - oi.pay_time) / 86400 >= 0 AND ($now - oi.pay_time)/86400 < 30 THEN 0 ". // 0 to 1 month
                " WHEN ($now - oi.pay_time)/86400 >= 30 AND ( $now - oi.pay_time)/86400 < (6 * 30 ) THEN 1 ". // 1 to 6 month
                " WHEN ($now - oi.pay_time)/86400 >= (6 * 30) THEN 2 ".
                " ELSE 3 ".
            " END AS buy_total ".
            " FROM " . $ecs->table('users') ." as u ".
            " LEFT JOIN " . $ecs->table('order_info') ." as oi ON oi.user_id = u.user_id ".
            " AND order_status NOT IN (" . OS_CANCELED . ',' . OS_INVALID . ',' . OS_RETURNED . ") ".
            " AND pay_status = " . PS_PAYED . " ".
            " WHERE u.user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).")" .
            " GROUP BY u.user_id HAVING count(oi.order_id) = 1 ) us GROUP BY us.buy_total ";
        $pc_one_month_data = $db->getAll($sql);
        $total = 0;
        $pc_one_month = [];
        $data = [];
        $datasets = [];
        foreach ($pc_label as $key => $value) {
            $pc_one_month["data"][$key]['label']  = $value;
            $pc_one_month["data"][$key]['value']  = 0;
            $pc_one_month["data"][$key]['href']   = "/x/users.php?act=list&guest_stats_type=1&range=$key";
        }
        foreach ($pc_one_month_data as $data_value) {
            $total += $data_value['count'];
            $pc_one_month["data"][intval($data_value['buy_total'])]['value']  = intval($data_value['count']);
        }
        foreach($pc_one_month["data"] as $key => $value) {
            $color = '#'.chart_color($key);
            $data[$key] = $value['value'];
            $backgroundColor[$key] = $color;
        }
        $label = '未曾回購用戶數';
        $datasets[0]['data'] = $data;
        $datasets[0]['backgroundColor'] = $backgroundColor;
        $datasets[0]['label'] = $label;
        $pc_one_month['source'] = ['datasets' => $datasets, 'labels' => $pc_label];
        $pc_one_month['total'] = $total;
        unset($total);

        $smarty->assign('pc_one_month',     $pc_one_month);

        /* 2 order or more */
        // 註冊日到首次購物平均相隔天數 - online only
        $sql = "SELECT us.buy_total, COUNT(us.buy_total) as count FROM (" .
            "SELECT CASE" .
                " WHEN (cast(min(oi.pay_time) as signed) - cast(u.reg_time as signed)) / (86400) < 30 THEN 0 ". // 0 to 1 month
                " WHEN (cast(min(oi.pay_time) as signed) - cast(u.reg_time as signed)) / (86400) >= 30 AND (cast(min(oi.pay_time) as signed) - cast(u.reg_time as signed)) / (86400) < (6 * 30 ) THEN 1 ". // 1 to 6 month
                " WHEN (cast(min(oi.pay_time) as signed) - cast(u.reg_time as signed)) / (86400) >= (6 * 30) THEN 2 ".
                " ELSE 3 ".
            " END AS buy_total ".
            " FROM " . $ecs->table('users') ." as u ".
            " LEFT JOIN " . $ecs->table('order_info') ." as oi ON oi.user_id = u.user_id ".
            " AND order_status NOT IN (" . OS_CANCELED . ',' . OS_INVALID . ',' . OS_RETURNED . ") ".
            " AND pay_status = " . PS_PAYED . " ".
            " WHERE u.user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).")".
            " AND oi.platform NOT IN ('pos_exhibition','pos') ".
            " AND (oi.order_type <> '" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT . "' OR oi.order_type IS NULL) ".
            " GROUP BY u.user_id HAVING count(oi.order_id) > 1 ) us GROUP BY us.buy_total ";

        $pc_2_reg_data = $db->getAll($sql);
        $total = 0;
        $data = [];
        $datasets = [];
        $pc_2_reg = [];
        foreach ($pc_label as $key => $value) {
            $pc_2_reg["data"][$key]['label']  = $value;
            $pc_2_reg["data"][$key]['value']  = 0;
            $pc_2_reg["data"][$key]['href']   = "/x/users.php?act=list&guest_stats_type=2&range=$key";
        }
        foreach ($pc_2_reg_data as $data_value) {
            $total += $data_value['count'];
            $pc_2_reg["data"][intval($data_value['buy_total'])]['value']  = intval($data_value['count']);
        }
        foreach($pc_2_reg["data"] as $key => $value) {
            $color = '#'.chart_color($key);
            $data[$key] = $value['value'];
            $backgroundColor[$key] = $color;
        }
        $pc_2_reg['total'] = $total;
        $label = '曾回購用戶 - 購物相隔天數';
        $datasets[0]['data'] = $data;
        $datasets[0]['backgroundColor'] = $backgroundColor;
        $datasets[0]['label'] = $label;
        $pc_2_reg['source'] = ['datasets' => $datasets, 'labels' => $pc_label];
        unset($total);

        $smarty->assign('pc_2_reg',     $pc_2_reg);

        // 過去365天內有多次曾購買過的會員:平均購買間隔
        $sql = "SELECT us.buy_total, COUNT(us.buy_total) as count FROM (" .
            "SELECT CASE" .
                " WHEN (max(oi.pay_time) -  min(oi.pay_time)) / ((count(oi.order_id) - 1) * 86400) < 30 THEN 0 ". // 0 to 1 month
                " WHEN (max(oi.pay_time) -  min(oi.pay_time)) / ((count(oi.order_id) - 1) * 86400) >= 30 AND (max(oi.pay_time) -  min(oi.pay_time)) / ((count(oi.order_id) - 1) * 86400) < (6 * 30 ) THEN 1 ". // 1 to 6 month
                " WHEN (max(oi.pay_time) -  min(oi.pay_time)) / ((count(oi.order_id) - 1) * 86400) >= (6 * 30) THEN 2 ".
                " ELSE 3 ".
            " END AS buy_total ".
            " FROM " . $ecs->table('users') ." as u ".
            " LEFT JOIN " . $ecs->table('order_info') ." as oi ON oi.user_id = u.user_id ".
            " AND order_status NOT IN (" . OS_CANCELED . ',' . OS_INVALID . ',' . OS_RETURNED . ") ".
            " AND pay_status = " . PS_PAYED . " ".
            " WHERE u.user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).")" .
            " GROUP BY u.user_id HAVING count(oi.order_id) > 1 AND max(oi.pay_time) BETWEEN ($now - (365 * 86400)) AND $now ) us GROUP BY us.buy_total ";

        $pc_2_avg_data = $db->getAll($sql);
        $total = 0;
        $pc_2_avg = [];
        $data = [];
        $datasets = [];
        foreach ($pc_label as $key => $value) {
            $pc_2_avg["data"][$key]['label']  = $value;
            $pc_2_avg["data"][$key]['value']  = 0;
            $pc_2_avg["data"][$key]['href']   = "/x/users.php?act=list&guest_stats_type=3&range=$key";
        }
        foreach ($pc_2_avg_data as $data_value) {
            $total += $data_value['count'];
            $pc_2_avg["data"][intval($data_value['buy_total'])]['value']  = intval($data_value['count']);
        }
        foreach($pc_2_avg["data"] as $key => $value) {
            $color = '#'.chart_color($key);
            $data[$key] = $value['value'];
            $backgroundColor[$key] = $color;
        }
        $label = '曾回購用戶 - 活躍用戶平均購買間隔';
        $datasets[0]['data'] = $data;
        $datasets[0]['backgroundColor'] = $backgroundColor;
        $datasets[0]['label'] = $label;
        $pc_2_avg['source'] = ['datasets' => $datasets, 'labels' => $pc_label];
        $pc_2_avg['total'] = $total;
        unset($total);

        $smarty->assign('pc_2_avg',     $pc_2_avg);

        // over 365天有曾多次購買過的會員:平均購買間隔
        $sql = "SELECT us.buy_total, COUNT(us.buy_total) as count FROM (" .
            "SELECT CASE" .
                " WHEN (max(oi.pay_time) -  min(oi.pay_time)) / ((count(oi.order_id) - 1) * 86400) < 30 THEN 0 ". // 0 to 1 month
                " WHEN (max(oi.pay_time) -  min(oi.pay_time)) / ((count(oi.order_id) - 1) * 86400) >= 30 AND (max(oi.pay_time) -  min(oi.pay_time)) / ((count(oi.order_id) - 1) * 86400) < (6 * 30 ) THEN 1 ". // 1 to 6 month
                " WHEN (max(oi.pay_time) -  min(oi.pay_time)) / ((count(oi.order_id) - 1) * 86400) >= (6 * 30) THEN 2 ".
                " ELSE 3 ".
            " END AS buy_total ".
            " FROM " . $ecs->table('users') ." as u ".
            " LEFT JOIN " . $ecs->table('order_info') ." as oi ON oi.user_id = u.user_id ".
            " AND order_status NOT IN (" . OS_CANCELED . ',' . OS_INVALID . ',' . OS_RETURNED . ") ".
            " AND pay_status = " . PS_PAYED . " ".
            " WHERE u.user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).")" .
            " GROUP BY u.user_id HAVING count(oi.order_id) > 1 AND max(oi.pay_time) < $now - (365 * 86400) ) us GROUP BY us.buy_total ";

        $pc_2_inactive_avg_data = $db->getAll($sql);
        $total = 0;
        $pc_2_inactive_avg = [];
        $data = [];
        $datasets = [];
        foreach ($pc_label as $key => $value) {
            $pc_2_inactive_avg["data"][$key]['label']  = $value;
            $pc_2_inactive_avg["data"][$key]['value']  = 0;
            $pc_2_inactive_avg["data"][$key]['href']   = "/x/users.php?act=list&guest_stats_type=4&range=$key";
        }
        foreach ($pc_2_inactive_avg_data as $data_value) {
            $total += $data_value['count'];
            $pc_2_inactive_avg["data"][intval($data_value['buy_total'])]['value']  = intval($data_value['count']);
        }
        foreach($pc_2_inactive_avg["data"] as $key => $value) {
            $color = '#'.chart_color($key);
            $data[$key] = $value['value'];
            $backgroundColor[$key] = $color;
        }
        $label = '曾回購用戶 - 活躍用戶平均購買間隔';
        $datasets[0]['data'] = $data;
        $datasets[0]['backgroundColor'] = $backgroundColor;
        $datasets[0]['label'] = $label;
        $pc_2_inactive_avg['source'] = ['datasets' => $datasets, 'labels' => $pc_label];
        $pc_2_inactive_avg['total'] = $total;
        unset($total);

        $smarty->assign('pc_2_inactive_avg',     $pc_2_inactive_avg);
    }
    if ($_GET['flag'] == 'download' || $_REQUEST['step'] == '3') {
        /* Customer segment */
        $sql = "SELECT us.seg, COUNT(us.seg) as count FROM (" .
            "SELECT CASE" .
                " WHEN sum(oi.goods_amount) = 0 THEN 0" .
                " WHEN sum(oi.goods_amount) > 0 AND sum(oi.goods_amount) < 1000 THEN 1" .
                " WHEN sum(oi.goods_amount) >= 1000 AND sum(oi.goods_amount) < 1500 THEN 2" .
                " WHEN sum(oi.goods_amount) >= 1500 AND sum(oi.goods_amount) < 3000 THEN 3" .
                " WHEN sum(oi.goods_amount) >= 3000 AND sum(oi.goods_amount) < 7000 THEN 4" .
                " WHEN sum(oi.goods_amount) >= 7000 THEN 5" .
                " ELSE 6 ".
            " END AS seg ".
            " FROM " . $ecs->table('users') ." as u ".
            " LEFT JOIN " . $ecs->table('order_info') ." as oi ON oi.user_id = u.user_id ".
            " AND order_status NOT IN (" . OS_CANCELED . ',' . OS_INVALID . ',' . OS_RETURNED . ") ".
            " AND pay_status = " . PS_PAYED . " ".
            " WHERE u.user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).")" . $where_u .
            " GROUP BY u.user_id ) us GROUP BY us.seg";
        $new_data["aggr_cus_seg"] = $db->getAll($sql);
        $label = [];
        $label[] = 'HK$ 0';
        $label[] = 'HK$ 1 - HK$ 999';
        $label[] = 'HK$ 1000 - HK$ 1500';
        $label[] = 'HK$ 1500 - HK$ 2999';
        $label[] = 'HK$ 3000 - HK$ 7000';
        $label[] = 'HK$ 7000+';
        $label[] = '未曾購買會員';
        $new_data["aggr_cus_seg_total"] = 0;
        foreach($new_data["aggr_cus_seg"] as $key => $value) {
            $new_data["aggr_cus_seg"][$key]['href'] = '/x/users.php?act=list&cst=1&range='.intval($value['seg']);
            $new_data["aggr_cus_seg"][$key]['seg']  = $label[intval($value['seg'])];
            $new_data["aggr_cus_seg_total"]+= $value['count'];
        }
        /*
        $sql = "SELECT COUNT(*) AS err_num FROM ". $ecs->table('users') .
            " u WHERE user_id IN(SELECT user_id FROM ". $ecs->table('order_info') . " oi " .
                    " WHERE oi.user_id NOT IN (" .implode(',', $userController->getWholesaleUserIds()).") AND ". order_amount_field("oi.") . " > 0" .
                    " AND COALESCE(oi.order_type,'') NOT IN ('" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT. "') " .
                    order_query_sql('finished','oi.') . $where . $where_exclude . ")" .
            " AND rank_points = 0 " . $where_u;
        $new_data["aggr_cus_seg_err"] = $db->getOne($sql);
        $sql = "SELECT COUNT(*) AS err_num FROM ". $ecs->table('users') .
            " u WHERE user_id IN(SELECT user_id FROM ". $ecs->table('order_info') . " oi " .
                    " WHERE oi.user_id NOT IN (" .implode(',', $userController->getWholesaleUserIds()).") AND ". order_amount_field("oi.") . " > 0" .
                    " AND COALESCE(oi.order_type,'') NOT IN ('" .Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE . "','" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT. "') " .
                    order_query_sql('finished','oi.') . $where . $where_exclude . ")" .
            " AND rank_points = 0 " . $where_u;
        $new_data["aggr_cus_seg_error"] = $db->getOne($sql);
        */
        /*
        if($new_data["aggr_cus_seg_error"] != $new_data["aggr_cus_seg_err"])
            $new_data["aggr_cus_seg_diff"] = $new_data["aggr_cus_seg_err"] - $new_data["aggr_cus_seg_error"];
            */
        $sql = "SELECT us.seg, COUNT(us.seg) as count FROM (" .
            "SELECT CASE" .
                " WHEN sum(oi.money_paid+oi.order_amount) / count(oi.order_id) = 0 THEN 0" .
                " WHEN sum(oi.money_paid+oi.order_amount) / count(oi.order_id) > 0 AND sum(oi.money_paid+oi.order_amount) / count(oi.order_id) < 1000 THEN 1" .
                " WHEN sum(oi.money_paid+oi.order_amount) / count(oi.order_id) >= 1000 AND sum(oi.money_paid+oi.order_amount) / count(oi.order_id) < 1500 THEN 2" .
                " WHEN sum(oi.money_paid+oi.order_amount) / count(oi.order_id) >= 1500 AND sum(oi.money_paid+oi.order_amount) / count(oi.order_id) < 3000 THEN 3" .
                " WHEN sum(oi.money_paid+oi.order_amount) / count(oi.order_id) >= 3000 AND sum(oi.money_paid+oi.order_amount) / count(oi.order_id) < 7000 THEN 4" .
                " WHEN sum(oi.money_paid+oi.order_amount) / count(oi.order_id) >= 7000 THEN 5" .
                " ELSE 6 ".
            " END AS seg ".
            " FROM " . $ecs->table('users') ." as u ".
            " LEFT JOIN " . $ecs->table('order_info') ." as oi ON oi.user_id = u.user_id ".
            " AND order_status NOT IN (" . OS_CANCELED . ',' . OS_INVALID . ',' . OS_RETURNED . ") ".
            " AND pay_status = " . PS_PAYED . " ".
            " WHERE u.user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).")" . $where_u .
            " GROUP BY u.user_id ) us  GROUP BY us.seg ";
        $new_data["aggr_cus_seg_avg"] = $db->getAll($sql);
        $new_data["aggr_cus_seg_avg_total"] = 0;
        foreach($new_data["aggr_cus_seg_avg"] as $key => $value) {
            $new_data["aggr_cus_seg_avg"][$key]['href'] = '/x/users.php?act=list&csta=1&range='.intval($value['seg']);
            $new_data["aggr_cus_seg_avg"][$key]['seg']  = $label[intval($value['seg'])];
            $new_data["aggr_cus_seg_avg_total"]+= $value['count'];
        }
        $sql = "SELECT u.seg, COUNT(*) as count FROM (" .
            "SELECT CASE" .
                    " WHEN AVG(" . order_amount_field("oi.") . ") = 0 THEN 'HK$ 0'" .
                    " WHEN AVG(" . order_amount_field("oi.") . ") > 0 AND AVG(" . order_amount_field("oi.") . ") < 1000 THEN 'HK$ 1 - 999'" .
                    " WHEN AVG(" . order_amount_field("oi.") . ") >= 1000 AND AVG(" . order_amount_field("oi.") . ") < 1500 THEN 'HK$ 1000 - 1499'" .
                    " WHEN AVG(" . order_amount_field("oi.") . ") >= 1500 AND AVG(" . order_amount_field("oi.") . ") < 7000 THEN 'HK$ 1500 - 6999'" .
                    " WHEN AVG(" . order_amount_field("oi.") . ") >= 7000 THEN 'HK$ 7000 +'" .
            " END AS seg FROM " . $ecs->table('order_info') . " oi " .
            " WHERE oi.user_id IN (SELECT u.user_id FROM " . $ecs->table('users') . " u WHERE u.user_id != 0 AND u.user_id NOT IN (" .implode(',', $userController->getWholesaleUserIds()).")" . $where_u .")" .
            " AND oi.shipping_time > 0 AND COALESCE(oi.order_type,'') != '" .Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE . "' " . order_query_sql('finished','oi.') . $where . $where_exclude .
            " GROUP BY oi.user_id) u GROUP BY u.seg";
        $new_data["order_cus_seg"] = $db->getAll($sql);
        $sql = "SELECT u.seg, COUNT(*) as count FROM (" .
            "SELECT CASE" .
                    " WHEN " . order_amount_field("oi.") . " = 0 THEN 'HK$ 0'" .
                    " WHEN " . order_amount_field("oi.") . " > 0 AND " . order_amount_field("oi.") . " < 1000 THEN 'HK$ 1 - 999'" .
                    " WHEN " . order_amount_field("oi.") . " >= 1000 AND " . order_amount_field("oi.") . " < 1500 THEN 'HK$ 1000 - 1499'" .
                    " WHEN " . order_amount_field("oi.") . " >= 1500 AND " . order_amount_field("oi.") . " < 7000 THEN 'HK$ 1500 - 6999'" .
                    " WHEN " . order_amount_field("oi.") . " >= 7000 THEN 'HK$ 7000 +'" .
            " END AS seg FROM " . $ecs->table('order_info') . " oi " .
            " WHERE oi.platform = 'pos' AND oi.user_id = 0 " . order_query_sql('finished','oi.') . $where . $where_exclude;
        if(isset($_REQUEST['reg_start_date']) && $_REQUEST['reg_start_date'] != "")
            $sql .= " AND oi.shipping_time >= " .strtotime($_REQUEST['reg_start_date']);
        if(isset($_REQUEST['reg_end_date']) && $_REQUEST['reg_end_date'] != "")
            $sql .= " AND oi.shipping_time < " .strtotime($_REQUEST['reg_end_date']);
        $sql .= ") u GROUP BY u.seg";
        $new_data["order_cus_seg_ex"] = $db->getAll($sql);
        $new_data["final_order_cus_seg"] = array();
        foreach($new_data["order_cus_seg"] as $k => $v){
            $new_data["final_order_cus_seg"][$v['seg']][0] = $v['count'];
        }
        foreach($new_data["order_cus_seg_ex"] as $v){
            $new_data["final_order_cus_seg"][$v['seg']][1] = $v['count'];
        }
    }
    if ($_GET['flag'] == 'download' || $_REQUEST['step'] == '4') {
        //offline registered with login
        $sql = 'SELECT COUNT(*) FROM (SELECT DISTINCT oi2.user_id FROM ' . $ecs->table('order_info') . ' oi2 WHERE oi2.user_id IN(' .
            'SELECT oi.user_id FROM '.$ecs->table('order_info') . ' oi '.
                'LEFT JOIN '.$ecs->table('users') . ' u ON u.user_id = oi.user_id '.
                'WHERE (u.reg_time = oi.add_time OR u.email REGEXP "^\\\+?[0-9-]+(@yohohongkong.com)$") AND oi.platform = "pos" AND u.last_login != 0' . $where_u .
                ' AND u.user_id NOT IN ('.implode(',', $userController->getWholesaleUserIds()).')' . $where_exclude . ') GROUP BY oi2.user_id) a';
        $new_data['off_to_on_new'] = $db->getOne($sql);
        //offline registered with login and 1+ orders
        $sql = 'SELECT COUNT(*) FROM (SELECT DISTINCT oi2.user_id FROM ' . $ecs->table('order_info') . ' oi2 WHERE oi2.user_id IN(' .
            'SELECT oi.user_id FROM '.$ecs->table('order_info') . ' oi '.
                'LEFT JOIN '.$ecs->table('users') . ' u ON u.user_id = oi.user_id '.
                'WHERE (u.reg_time = oi.add_time OR u.email REGEXP "^\\\+?[0-9-]+(@yohohongkong.com)$") AND oi.platform = "pos" AND u.last_login != 0' . $where_u .
                ' AND u.user_id NOT IN ('.implode(',', $userController->getWholesaleUserIds()).')'.order_query_sql('finished','oi.') . $where_exclude . ') GROUP BY oi2.user_id HAVING COUNT(*) > 1) a';
        $new_data['off_to_on_buy_log_new'] = $db->getOne($sql);
        //offline registered with 1+ orders
        $sql = 'SELECT COUNT(*) FROM (SELECT DISTINCT oi2.user_id FROM ' . $ecs->table('order_info') . ' oi2 WHERE oi2.user_id IN(' .
            'SELECT oi.user_id FROM '.$ecs->table('order_info') . ' oi '.
                'LEFT JOIN '.$ecs->table('users') . ' u ON u.user_id = oi.user_id '.
                'WHERE (u.reg_time = oi.add_time OR u.email REGEXP "^\\\+?[0-9-]+(@yohohongkong.com)$") AND oi.platform = "pos" ' . $where_u .
                ' AND u.user_id NOT IN ('.implode(',', $userController->getWholesaleUserIds()).')'.order_query_sql('finished','oi.') . $where_exclude . ') GROUP BY oi2.user_id HAVING COUNT(*) > 1) a';
        $new_data['off_to_on_buy_new'] = $db->getOne($sql);
    }
    if ($_GET['flag'] == 'download' || $_REQUEST['step'] == '5') {
        // Active/Inactive Members:
        
        //Active:
        $now  = local_strtotime("now");
        // within 365 days with login activity or with purchase
        $sql  = " SELECT count(DISTINCT u.user_id) FROM ".$ecs->table('users')." as u ".
            " WHERE (u.last_login > (".local_strtotime("now")." - (365*86400) )".
            " OR u.user_id IN ( SELECT oi.user_id FROM " . $ecs->table('order_info') ." as oi ".
            " WHERE oi.pay_time > (".local_strtotime("now")." - (365*86400) )".
            " AND oi.order_status NOT IN (" . OS_CANCELED . ',' . OS_INVALID . ',' . OS_RETURNED . ") ".
            " AND oi.pay_status = " . PS_PAYED . " ".
            ") )AND u.user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).") ";
        $active = $db->getOne($sql);
        // within 10 days with login, no purchase
        $sql  = " SELECT count(DISTINCT u.user_id) FROM ".$ecs->table('users')." as u ".
            " WHERE (u.last_login > (".local_strtotime("now")." - (10 * 86400) ) ".
            " AND u.user_id NOT IN (SELECT oi.user_id FROM ". $ecs->table('order_info') ." as oi WHERE ".
            " oi.pay_time > (".local_strtotime("now")." - (10 * 86400) ) ".
            " AND oi.order_status NOT IN (" . OS_CANCELED . ',' . OS_INVALID . ',' . OS_RETURNED . ") ".
            " AND oi.pay_status = " . PS_PAYED . ") )".
            " AND u.user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).") ";
        $active_last_10 = $db->getOne($sql);
        // Inactive Members:
        // over 365 days without login activity or without purchase
        $sql  = " SELECT count(DISTINCT u.user_id) FROM ".$ecs->table('users')." as u ".
            " WHERE (u.last_login < (".local_strtotime("now")." - (365*86400) )".
            " AND u.user_id NOT IN ( SELECT oi.user_id FROM " . $ecs->table('order_info') ." as oi ".
            " WHERE oi.pay_time > (".local_strtotime("now")." - (365*86400) )".
            " AND oi.order_status NOT IN (" . OS_CANCELED . ',' . OS_INVALID . ',' . OS_RETURNED . ") ".
            " AND oi.pay_status = " . PS_PAYED . " ".
            ") ) AND u.user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).") ";
        $inactive = $db->getOne($sql);
        // over 365 days, only 1 visit count, without purchase
        $sql  = " SELECT count(DISTINCT u.user_id) FROM ".$ecs->table('users')." as u ".
            " WHERE (u.last_login < (".local_strtotime("now")." - (365 * 86400) ) ".
            " AND u.visit_count = 1 ".
            " AND u.user_id NOT IN (SELECT oi.user_id FROM ". $ecs->table('order_info') ." as oi WHERE ".
            " oi.order_status NOT IN (" . OS_CANCELED . ',' . OS_INVALID . ',' . OS_RETURNED . ") ".
            " AND oi.pay_status = " . PS_PAYED . ") )".
            " AND u.user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).") ";
        $inactive_one_visit = $db->getOne($sql);

        // Inactive members with strong purchase power: customer segment(客人平均消費力) $1500 above, over 1 year no login or purchase
        $sql  = " SELECT count(DISTINCT u.user_id) FROM ".$ecs->table('users')." as u ".
            " WHERE u.last_login < (".local_strtotime("now")." - (365*86400) )".
            " AND u.user_id IN (SELECT u.user_id ".
            " FROM " . $ecs->table('users') ." as u ".
            " LEFT JOIN " . $ecs->table('order_info') ." as oi ON oi.user_id = u.user_id ".
            " AND order_status NOT IN (" . OS_CANCELED . ',' . OS_INVALID . ',' . OS_RETURNED . ") ".
            " AND pay_status = " . PS_PAYED . " ".
            " WHERE u.user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).")" .
            " GROUP BY u.user_id HAVING sum(oi.money_paid+oi.order_amount) / count(oi.order_id) >= 1500 AND MAX(oi.pay_time) < (".local_strtotime("now")." - (365*86400) ) )";
        $inactive_strong_purchase  = $db->getOne($sql);
        // Only with offline purchase record, never login (within 6 months)
        $sql  = " SELECT count(DISTINCT u.user_id) FROM ".$ecs->table('users')." as u ".
            " WHERE  u.last_login = 0 AND u.is_wholesale = 0 ".
            " AND u.user_id IN (SELECT oi.user_id FROM ". $ecs->table('order_info') ." as oi WHERE ".
            " oi.pay_time > (".local_strtotime("now")." - (180 * 86400)) ".
            " AND oi.order_status NOT IN (" . OS_CANCELED . ',' . OS_INVALID . ',' . OS_RETURNED . ") ".
            " AND (oi.order_type<>'".Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE."' OR oi.order_type IS NULL) ".
            " AND oi.pay_status = " . PS_PAYED . " )";
        $offline_purchase_winthin = $db->getOne($sql);
        // Only with offline purchase record, never login (over 6 months)
        $sql  = " SELECT count(DISTINCT u.user_id) FROM ".$ecs->table('users')." as u ".
        " WHERE  u.last_login = 0 AND u.is_wholesale = 0 ".
        " AND u.user_id NOT IN (SELECT oi.user_id FROM ". $ecs->table('order_info') ." as oi WHERE ".
        " oi.pay_time > (".local_strtotime("now")." - (180 * 86400)) ".
        " AND oi.order_status NOT IN (" . OS_CANCELED . ',' . OS_INVALID . ',' . OS_RETURNED . ") ".
        " AND (oi.order_type<>'".Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE."' OR oi.order_type IS NULL) ".
        " AND oi.pay_status = " . PS_PAYED . " )";
        $offline_purchase_over = $db->getOne($sql);
        $sql  = " SELECT count(DISTINCT u.user_id) FROM ".$ecs->table('users')." as u ".
        " WHERE  u.last_login = 0 AND u.is_wholesale = 0 ";
        $offline_total = $db->getOne($sql);
        $active_list = ['active' => $active, 'active_last_10' => $active_last_10];
        $inactive_list = ['inactive' => $inactive, 'inactive_one_visit' => $inactive_one_visit, 'inactive_strong_purchase' => $inactive_strong_purchase];
        $offline_purchase_list = ['offline_purchase_winthin' => $offline_purchase_winthin, 'offline_purchase_over' => $offline_purchase_over, 'total' => $offline_purchase_winthin+$offline_purchase_over, 'offline_total' => $offline_total];
        $smarty->assign('active_list',   $active_list);
        $smarty->assign('a_in_total', $inactive + $active);
        $smarty->assign('inactive_list', $inactive_list);
        $smarty->assign('offline_list', $offline_purchase_list);

    }

    $_GET['flag'] = isset($_GET['flag']) ? 'download' : '';
    if($_GET['flag'] == 'download')
    {
        $filename = ecs_iconv(EC_CHARSET, 'GB2312', $_LANG['guest_statistics']);

        header("Content-type: application/vnd.ms-excel; charset=utf-8");
        header("Content-Disposition: attachment; filename=$filename.xls");

        /* 生成會員資料 */
        $data = $_LANG['member_data'] . "\t\n";
        $data .= $_LANG['member_count'] . "\t" . $_LANG['member_login_number'] . "\t" . $_LANG['member_online_register'] . "\t" .
                $_LANG['member_info_filled_number'] . "\t" . $_LANG['member_email_filled_number'] . "\t" . $_LANG['member_email_verified_number'] . "\t" .
                join(' / ',preg_split('@/@',$language_list_string, NULL, PREG_SPLIT_NO_EMPTY)) . "\t" . $_LANG['wholesale_member_count'] . "\t" . $_LANG['member_from_sales_agency'] . "\n";
        $data .= $user_num . "\t" . $logined_user['user_num'] . "\t" . $online_register_user . "\t" .
                $info_completed_user['user_num'] . "\t" . $email_completed_user['user_num'] . "\t" . $validated_user['user_num'] . "\t" .
                join(' / ',preg_split('@/@',$language_distribution_string, NULL, PREG_SPLIT_NO_EMPTY)) . "\t" . $wholesale_user_num['user_num'] . "\t" . $sales_agency_introduce_member['user_num'] . "\n\n";

        /* 生成会员购买率 */
        $data .= $_LANG['percent_buy_member'] . "\t\n";
        $data .= $_LANG['order_member_count'] . "\t" . $_LANG['member_order_count'] . "\t" . $_LANG['percent_buy_member'] . "\n";

        $data .= $have_order_usernum . "\t" .
                $user_all_order['order_num'] . "\t" . sprintf("%0.2f", ($user_num > 0 ? $have_order_usernum / $user_num : 0) * 100) . "%\n\n";

        /* 每会员平均订单数及购物额 */
        $data .= $_LANG['order_turnover_peruser'] . "\t\n";

        $data .= $_LANG['member_sum'] . "\t" . $_LANG['average_member_order'] . "\t" .
                $_LANG['member_order_sum'] . "\n";

        $ave_user_ordernum = $user_num > 0 ? sprintf("%0.2f", $user_all_order['order_num'] / $user_num) : 0;
        $ave_user_turnover = $user_num > 0 ? price_format($user_all_order['turnover'] / $user_num) : 0;

        $data .= price_format($user_all_order['turnover']) . "\t" . $ave_user_ordernum . "\t" . $ave_user_turnover . "\n\n";

        /* 每会员(曾購物)平均订单数及购物额 */
        $data .= $_LANG['ordered_order_turnover_peruser'] . "\t\n";

        $data .= $_LANG['member_sum'] . "\t" . $_LANG['ordered_average_member_order'] . "\t" .
                $_LANG['ordered_member_order_sum'] . "\n";

        $ordered_ave_user_ordernum = $have_order_usernum > 0 ? sprintf("%0.2f", $user_all_order['order_num'] / $have_order_usernum) : 0;
        $ordered_ave_user_turnover = $have_order_usernum > 0 ? price_format($user_all_order['turnover'] / $have_order_usernum) : 0;

        $data .= price_format($user_all_order['turnover']) . "\t" . $ordered_ave_user_ordernum . "\t" . $ordered_ave_user_turnover . "\n\n";

        /* 每会员平均订单数及购物额 */
        $data .= $_LANG['order_turnover_percus'] . "\t\n";
        $data .= $_LANG['guest_member_orderamount'] . "\t" . $_LANG['guest_member_ordercount'] . "\t" .
                $_LANG['guest_order_sum'] . "\n";

        $order_num = $guest_all_order['order_num'] > 0 ? price_format($guest_all_order['turnover'] / $guest_all_order['order_num']) : 0;
        $data .= price_format($guest_all_order['turnover']) . "\t" . $guest_all_order['order_num'] . "\t" .
                $order_num;

        echo ecs_iconv(EC_CHARSET, 'BIG5', $data) . "\t";
        exit;
    }

    /* 赋值到模板 */
    $smarty->assign('user_num',            $user_num);                                                                          // 会员总数
    $smarty->assign('wholesale_user_num',            $wholesale_user_num['user_num']);                                          // 批發会员总数
    $smarty->assign('have_order_usernum',  $have_order_usernum);                                                                // 有过订单的会员数
    $smarty->assign('user_order_turnover', $user_all_order['order_num']);                                                       // 会员总订单数
    $smarty->assign('user_order_turnover2', $new_data['user_all_order'][0]['order_num']);                                          // 会员总订单数(new)
    $smarty->assign('user_order_turnover3', $new_data['user_all_order'][1]['order_num']+$new_data['user_all_order'][3]['order_num']);                                          // 会员总订单数(new)
    $smarty->assign('all_turnover',   price_format($user_all_order['turnover'] + $guest_all_order['turnover']));                //购物总额
    $smarty->assign('all_turnover2',   price_format($new_data['user_all_order'][0]['turnover'] + $guest_all_order['turnover']));   //购物总额(new)
    $smarty->assign('all_turnover3',   price_format($new_data['user_all_order'][1]['turnover'] + $new_data['user_all_order'][3]['turnover'] + $guest_all_order['turnover']));   //购物总额(new)
    $smarty->assign('user_all_turnover',   price_format($user_all_order['turnover']));                                          //会员购物总额
    $smarty->assign('user_all_turnover2',   price_format($new_data['user_all_order'][0]['turnover']));                             //会员购物总额(new)
    $smarty->assign('user_all_turnover3',   price_format($new_data['user_all_order'][1]['turnover'] + $new_data['user_all_order'][3]['turnover']));                             //会员购物总额(new)
    $smarty->assign('guest_all_turnover',  price_format($guest_all_order['turnover']));                                         //匿名会员购物总额
    $smarty->assign('guest_order_num',     $guest_all_order['order_num']);                                                      //匿名会员订单总数
    $smarty->assign('info_completed_user',     $info_completed_user['user_num']);                                               //已填資料會員總數
    $smarty->assign('validated_user',     $validated_user['user_num']);                                                         //已驗證會員總數
    $smarty->assign('email_completed_user',     $email_completed_user['user_num']);                                             //已填電郵會員總數
    $smarty->assign('logined_user',     $logined_user['user_num']);                                                             //曾登入會員總數
    $smarty->assign('online_register_user',     $online_register_user);                                                         //網上註冊會員總數
    $smarty->assign('language_list_string',     join(' / ',preg_split('@/@',$language_list_string, NULL, PREG_SPLIT_NO_EMPTY)));                        //語言列表
    $smarty->assign('language_distribution_string',     join(' / ',preg_split('@/@',$language_distribution_string, NULL, PREG_SPLIT_NO_EMPTY)));        //語言分佈
    $smarty->assign('sales_agency_introduce_member', $sales_agency_introduce_member);
    foreach($new_data as $k => $v){if(!is_array($v) && is_float($v+0))$new_data[$k] = number_format($v,2);}
    $smarty->assign('new_data', $new_data);
    $smarty->assign('info_completed_user',     $info_completed_user['user_num']);              //已填資料會員總數
    $smarty->assign('validated_user',     $validated_user['user_num']);              //已驗證會員總數
    $smarty->assign('email_completed_user',     $email_completed_user['user_num']);              //已填電郵會員總數
    $smarty->assign('logined_user',     $logined_user['user_num']);              //曾登入會員總數
    $smarty->assign('online_register_user',     $online_register_user);              //網上註冊會員總數
    $smarty->assign('language_list_string',     join(' / ',preg_split('@/@',$language_list_string, NULL, PREG_SPLIT_NO_EMPTY)));              //語言列表
    $smarty->assign('language_distribution_string',     join(' / ',preg_split('@/@',$language_distribution_string, NULL, PREG_SPLIT_NO_EMPTY)));              //語言分佈
    $smarty->assign('sales_agency_introduce_member', $sales_agency_introduce_member);


    /* 每会员订单数 */
    $smarty->assign('ave_user_ordernum',  $user_num > 0 ? sprintf("%0.2f", $user_all_order['order_num'] / $user_num) : 0);
    $smarty->assign('ave_user_ordernum2',  $user_num > 0 ? sprintf("%0.2f", $new_data['user_all_order'][0]['order_num'] / $user_num) : 0);
    $smarty->assign('ave_user_ordernum3',  $user_num > 0 ? sprintf("%0.2f", ($new_data['user_all_order'][1]['order_num'] + $new_data['user_all_order'][3]['order_num']) / $user_num) : 0);

    /* 每有过订单会员订单数 */
    $smarty->assign('ordered_ave_user_ordernum',  $have_order_usernum > 0 ? sprintf("%0.2f", $user_all_order['order_num'] / $have_order_usernum) : 0);
    $smarty->assign('ordered_ave_user_ordernum2',  $have_order_usernum > 0 ? sprintf("%0.2f", $new_data['user_all_order'][0]['order_num'] / $have_order_usernum) : 0);
    $smarty->assign('ordered_ave_user_ordernum3',  $have_order_usernum > 0 ? sprintf("%0.2f", $new_data['user_all_order'][1]['order_num'] / $have_order_usernum) : 0);

    /* 每会员购物额 */
    $smarty->assign('ave_user_turnover',  $user_num > 0 ? price_format($user_all_order['turnover'] / $user_num) : 0);
    $smarty->assign('ave_user_turnover2',  $user_num > 0 ? price_format($new_data['user_all_order'][0]['turnover'] / $user_num) : 0);
    $smarty->assign('ave_user_turnover3',  $user_num > 0 ? price_format(($new_data['user_all_order'][1]['turnover'] + $new_data['user_all_order'][3]['turnover']) / $user_num) : 0);

    /* 會員平均訂單金額 */
    $smarty->assign('ave_basket_value',  $user_all_order['order_num'] > 0 ? price_format($user_all_order['turnover'] / $user_all_order['order_num']) : 0);
    $smarty->assign('ave_basket_value2',  $new_data['user_all_order'][0]['order_num'] > 0 ? price_format($new_data['user_all_order'][0]['turnover'] / $new_data['user_all_order'][0]['order_num']) : 0);
    $smarty->assign('ave_basket_value3',  ($new_data['user_all_order'][1]['order_num'] + $new_data['user_all_order'][3]['order_num']) > 0 ? price_format(($new_data['user_all_order'][1]['turnover'] + $new_data['user_all_order'][3]['turnover']) / ($new_data['user_all_order'][1]['order_num'] + $new_data['user_all_order'][3]['order_num'])) : 0);

    /* 每有过订单会员购物额 */
    $smarty->assign('ordered_ave_user_turnover',  $have_order_usernum > 0 ? price_format($user_all_order['turnover'] / $have_order_usernum) : 0);
    $smarty->assign('ordered_ave_user_turnover2',  $have_order_usernum > 0 ? price_format($new_data['user_all_order'][0]['turnover'] / $have_order_usernum) : 0);
    $smarty->assign('ordered_ave_user_turnover3',  $have_order_usernum > 0 ? price_format(($new_data['user_all_order'][1]['turnover'] + $new_data['user_all_order'][3]['turnover']) / $have_order_usernum) : 0);

    /* 注册会员购买率 */
    $smarty->assign('user_ratio', sprintf("%0.2f", ($user_num > 0 ? $have_order_usernum / $user_num : 0) * 100));

     /* 匿名会员平均订单额 */
    $smarty->assign('guest_order_amount', $guest_all_order['order_num'] > 0 ? price_format($guest_all_order['turnover'] / $guest_all_order['order_num']) : 0);

    $smarty->assign('all_order',          $user_all_order);    //所有订单总数以及所有购物总额
    $smarty->assign('ur_here',            $_LANG['report_guest']);
    $smarty->assign('lang',               $_LANG);

    $smarty->assign('action_link',  array('text' => $_LANG['down_guest_stats'],
          'href'=>'guest_stats.php?flag=download'));

    assign_query_info();
    $smarty->display('guest_stats.htm');
}
function weightedAverage($data,$a,$b){
    return ($data['purchase_cycle'][$a]['cycle'] * $data['rebuy'][$a] + $data['purchase_cycle'][$b]['cycle'] * $data['rebuy'][$b]) / ($data['rebuy'][$a] + $data['rebuy'][$b]);
}
?>
