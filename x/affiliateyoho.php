<?php

/***
* affiliateyoho.php
* by howang 2015-06-09
*
* Manage YOHO Affiliates
***/

define('IN_ECS', true);
require(dirname(__FILE__) . '/includes/init.php');

// ====================================================
// ==================== Affiliates ====================
// ====================================================
if ($_REQUEST['act'] == 'list')
{
    /* 检查权限 */
    admin_priv('affiliate');
    
    $list = get_affiliate_list();
    $smarty->assign('ur_here',      '推廣大使列表');
    $smarty->assign('full_page',    1);

    $smarty->assign('list',         $list['data']);
    $smarty->assign('filter',       $list['filter']);
    $smarty->assign('record_count', $list['record_count']);
    $smarty->assign('page_count',   $list['page_count']);
    
    $smarty->assign('action_link', array('text' => '任命推廣大使', 'href' => 'affiliateyoho.php?act=add'));

    $sort_flag  = sort_flag($list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);

    assign_query_info();
    $smarty->display('affiliateyoho_list.htm');
}
elseif ($_REQUEST['act'] == 'query')
{
    /* 检查权限 */
    check_authz_json('affiliate');
    
    $list = get_affiliate_list();
    $smarty->assign('list',         $list['data']);
    $smarty->assign('filter',       $list['filter']);
    $smarty->assign('record_count', $list['record_count']);
    $smarty->assign('page_count',   $list['page_count']);

    $sort_flag  = sort_flag($list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);

    make_json_result($smarty->fetch('affiliateyoho_list.htm'), '', array('filter' => $list['filter'], 'page_count' => $list['page_count']));
}
elseif ($_REQUEST['act'] == 'add')
{
    /* 检查权限 */
    admin_priv('affiliate');
    
    $smarty->assign('ur_here',      '任命推廣大使');
    
    $smarty->assign('action_link', array('text' => '返回列表', 'href' => 'affiliateyoho.php?act=list'));
    
    assign_query_info();
    $smarty->display('affiliateyoho_add.htm');
}
elseif ($_REQUEST['act'] == 'insert')
{
    /* 检查权限 */
    admin_priv('affiliate');
    
    $user_id = intval($_POST['user_id']);
    
    $sql = "UPDATE " . $ecs->table('users') .
            "SET is_affiliate = 1 " .
            "WHERE user_id = '$user_id' " .
            "LIMIT 1";
    $db->query($sql);
    
    $link[0]['text'] = '返回推廣大使列表';
    $link[0]['href'] = 'affiliateyoho.php?act=list';
    
    sys_msg('任命推廣大使成功', 0, $link, true);
}
elseif ($_REQUEST['act'] == 'remove')
{
    /* 检查权限 */
    check_authz_json('affiliate');
    
    $user_id = intval($_GET['id']);
    
    // Remove affiliate status from user
    $sql = "UPDATE " . $ecs->table('users') .
            "SET is_affiliate = 0 " .
            "WHERE user_id = '$user_id' " .
            "LIMIT 1";
    $db->query($sql);
    
    // Delete related rewards too
    $sql = "DELETE ard " .
            "FROM " . $ecs->table('affiliate_rewards_detail') . " as ard " .
            "LEFT JOIN " . $ecs->table('affiliate_rewards') . " as ar ON ar.reward_id = ard.reward_id " .
            "WHERE ar.user_id = '$user_id'";
    $db->query($sql);
    $sql = "DELETE FROM " . $ecs->table('affiliate_rewards') .
            "WHERE user_id = '$user_id'";
    $db->query($sql);
    
    clear_cache_files();
    
    $url = 'affiliateyoho.php?act=query&' . str_replace('act=remove', '', $_SERVER['QUERY_STRING']);
    header('Location: ' . $url);
    exit;
}
// =================================================
// ==================== Rewards ====================
// =================================================
elseif ($_REQUEST['act'] == 'list_rewards')
{
    /* 检查权限 */
    admin_priv('affiliate');
    
    $list = get_affiliate_rewards_list();
    $smarty->assign('ur_here',      '獎勵列表');
    $smarty->assign('full_page',    1);

    $smarty->assign('list',         $list['data']);
    $smarty->assign('filter',       $list['filter']);
    $smarty->assign('record_count', $list['record_count']);
    $smarty->assign('page_count',   $list['page_count']);

    $smarty->assign('action_link', array('text' => '返回推廣大使列表', 'href' => 'affiliateyoho.php?act=list'));
    $smarty->assign('action_link2', array('text' => '新增獎勵', 'href' => 'affiliateyoho.php?act=add_reward' . (!empty($_REQUEST['user_id']) ? '&user_id=' . $_REQUEST['user_id'] : '')));

    $sort_flag  = sort_flag($list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);

    assign_query_info();
    $smarty->display('affiliateyoho_rewards_list.htm');
}
elseif ($_REQUEST['act'] == 'query_rewards')
{
    /* 检查权限 */
    admin_priv('affiliate');
    
    $list = get_affiliate_rewards_list();
    $smarty->assign('list',         $list['data']);
    $smarty->assign('filter',       $list['filter']);
    $smarty->assign('record_count', $list['record_count']);
    $smarty->assign('page_count',   $list['page_count']);

    $sort_flag  = sort_flag($list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);

    make_json_result($smarty->fetch('affiliateyoho_rewards_list.htm'), '', array('filter' => $list['filter'], 'page_count' => $list['page_count']));
}
elseif (in_array($_REQUEST['act'], array('add_reward','reward_info','edit_reward','copy_reward')))
{
    /* 检查权限 */
    admin_priv('affiliate');
    
    $act_names = array(
        'add_reward' => '新增獎勵',
        'reward_info' => '獎勵詳情',
        'edit_reward' => '編輯獎勵',
        'copy_reward' => '複製獎勵'
    );
    $smarty->assign('ur_here', $act_names[$_REQUEST['act']]);
    
    if ($_REQUEST['act'] != 'add_reward')
    {
        $reward_id = empty($_REQUEST['reward_id']) ? 0 : intval($_REQUEST['reward_id']);
        $reward = get_affiliate_reward_info($reward_id);
        
        if (!$reward)
        {
            sys_msg('找不到該獎勵');
        }
        
        if ($_REQUEST['act'] == 'copy_reward')
        {
            unset($reward['reward_id']);
            unset($reward['user_id']);
        }
    }
    else
    {
        $user_id = empty($_REQUEST['user_id']) ? 0 : intval($_REQUEST['user_id']);
        
        // if (empty($user_id))
        // {
        //     sys_msg('新增獎勵前請先選擇推廣大使');
        // }
        
        $reward = array(
            // 'user_id' => $user_id,
            'start_date' => local_date('Y-m-d') . ' 00:00:00',
            'end_date' => local_date('Y-m-d', local_strtotime('+1 month'))  . ' 23:59:59'
        );
        
        if (!empty($user_id))
        {
            $sql = "SELECT u.user_name, rei.`content` as display_name " .
                    "FROM " . $ecs->table('users') . " as u " .
                    "LEFT JOIN " . $ecs->table('reg_extend_info') . " as rei ON rei.user_id = u.user_id AND rei.reg_field_id = 10 " .
                    "WHERE u.user_id = '" . $user_id . "' " .
                    "LIMIT 1";
            $res = $db->getRow($sql);
            
            if (!empty($res))
            {
                $reward['user_id'] = $user_id;
                $reward['user_name'] = $res['user_name'];
                $reward['display_name'] = empty($res['display_name']) ? $res['user_name'] : $res['display_name'];
            }
        }
    }
    $smarty->assign('reward', $reward);
    
    $smarty->assign('action_link', array('text' => '返回列表', 'href' => 'affiliateyoho.php?act=list_rewards&user_id=' . $reward['user_id']));
    if ($_REQUEST['act'] == 'reward_info')
    {
        $smarty->assign('action_link2', array('text' => '編輯獎勵', 'href' => 'affiliateyoho.php?act=edit_reward&reward_id=' . $reward_id));
    }
    elseif ($_REQUEST['act'] == 'edit_reward')
    {
        $smarty->assign('action_link2', array('text' => '查看獎勵', 'href' => 'affiliateyoho.php?act=reward_info&reward_id=' . $reward_id));
    }
    
    $smarty->assign('act',          $_REQUEST['act']);
    
    $smarty->assign('cfg_lang',     $_CFG['lang']);
    
    $smarty->assign('cat_list',     cat_list());
    
    // Find all leaf nodes in cat_list
    $sql = "SELECT c.cat_id " .
            "FROM " . $ecs->table('category') . " as c " .
            "LEFT JOIN " . $ecs->table('category') . " as cc ON cc.parent_id = c.cat_id " .
            "WHERE cc.cat_id IS NULL";
    $smarty->assign('cat_leaves',   $db->getCol($sql));
    
    assign_query_info();
    $smarty->display('affiliateyoho_reward_info.htm');
}
elseif (in_array($_REQUEST['act'], array('insert_reward','update_reward')))
{
    /* 检查权限 */
    admin_priv('affiliate');
    
    if ($_REQUEST['act'] == 'update_reward')
    {
        $reward_id = empty($_REQUEST['reward_id']) ? 0 : intval($_REQUEST['reward_id']);
        
        if (!$reward_id)
        {
            sys_msg('獎勵編號錯誤');
        }
    }
    
    $reward = array();
    
    $reward['reward_value'] = empty($_POST['reward_value']) ? '' : trim($_POST['reward_value']);
    if (empty($reward['reward_value']))
    {
        sys_msg('必須輸入獎勵金額');
    }
    
    if (!empty($_POST['user_id']))
    {
        $reward['user_id'] = intval($_POST['user_id']);
    }
    
    if (!empty($_POST['start_date']))
    {
        $reward['start_time'] = local_strtotime($_POST['start_date']);
    }
    if (!empty($_POST['end_date']))
    {
        $reward['end_time'] = local_strtotime($_POST['end_date']);
    }
    
    if ($_REQUEST['act'] == 'update_reward')
    {
        $db->autoExecute($ecs->table('affiliate_rewards'), $reward, 'UPDATE', " `reward_id` = '" . $reward_id . "'");
    }
    else
    {
        $db->autoExecute($ecs->table('affiliate_rewards'), $reward, 'INSERT');
        
        $reward_id = $db->insert_id();
    }
    
    // 是否全部產品都有獎勵
    if (isset($_POST['range']['all']))
    {
        if (!empty($_POST['range']['all']))
        {
            $sql = "SELECT '1' " .
                    "FROM " . $ecs->table('affiliate_rewards_detail') .
                    "WHERE reward_id = '" . $reward_id . "' " .
                    "AND type = 'all'";
            $haveAll = $db->getOne($sql);
            
            if (!$haveAll)
            {
                $sql = "INSERT INTO " . $ecs->table('affiliate_rewards_detail') .
                        " (`reward_id`, `type`, `related_id`) " .
                        "VALUES ('" . $reward_id . "', 'all', 0) ";
                $db->query($sql);
            }
        }
        else
        {
            $sql = "DELETE FROM " . $ecs->table('affiliate_rewards_detail') .
                    "WHERE reward_id = '" . $reward_id . "' " .
                    "AND type = 'all'";
            $db->query($sql);
        }
    }
    
    // 先刪除原有紀錄
    $sql = "DELETE FROM " . $ecs->table('affiliate_rewards_detail') .
            "WHERE reward_id = '" . $reward_id . "' " .
            "AND type IN ('category', 'goods')";
    $db->query($sql);
    
    // 是否指定分類有獎勵
    if (!empty($_POST['range']['category']) && is_array($_POST['range']['category']))
    {
        $sql = "INSERT INTO " . $ecs->table('affiliate_rewards_detail') .
                " (`reward_id`, `type`, `related_id`) " .
                " VALUES " .
                implode(',', array_map(function ($cat_id) use ($reward_id) {
                    return "('" . $reward_id . "', 'category', '" . $cat_id . "')";
                }, $_POST['range']['category']));
        $db->query($sql);
    }
    
    // 是否指定產品有獎勵
    if (!empty($_POST['range']['goods']) && is_array($_POST['range']['goods']))
    {
        $sql = "INSERT INTO " . $ecs->table('affiliate_rewards_detail') .
                " (`reward_id`, `type`, `related_id`) " .
                " VALUES " .
                implode(',', array_map(function ($goods_id) use ($reward_id) {
                    return "('" . $reward_id . "', 'goods', '" . $goods_id . "')";
                }, $_POST['range']['goods']));
        $db->query($sql);
    }
    
    // 任命會員成為推廣大使
    if (!empty($reward['user_id']))
    {
        $sql = "UPDATE " . $ecs->table('users') . " SET is_affiliate = 1 WHERE user_id = '" . $reward['user_id'] . "' LIMIT 1";
        $db->query($sql);
    }
    
    clear_cache_files();
    
    $link[0]['text'] = '查看獎勵';
    $link[0]['href'] = 'affiliateyoho.php?act=reward_info&reward_id=' . $reward_id;
    
    $link[1]['text'] = '返回列表';
    $link[1]['href'] = 'affiliateyoho.php?act=list_rewards' . (!empty($reward['user_id']) ? '&user_id=' . $reward['user_id'] : '');
    
    sys_msg(($_REQUEST['act'] == 'update_reward' ? '編輯' : '新增') . '獎勵成功', 0, $link, true);
}
elseif ($_REQUEST['act'] == 'remove_reward')
{
    /* 检查权限 */
    check_authz_json('affiliate');
    
    $reward_id = intval($_GET['id']);
    
    // Delete related reward details too
    $sql = "DELETE FROM " . $ecs->table('affiliate_rewards_detail') .
            "WHERE reward_id = '$reward_id'";
    $db->query($sql);
    
    $sql = "DELETE FROM " . $ecs->table('affiliate_rewards') .
            "WHERE reward_id = '$reward_id'";
    $db->query($sql);
    
    clear_cache_files();
    
    $url = 'affiliateyoho.php?act=query_rewards&' . str_replace('act=remove_reward', '', $_SERVER['QUERY_STRING']);
    header('Location: ' . $url);
    exit;
}
// ===========================================================
// ==================== Affiliated Orders ====================
// ===========================================================
elseif ($_REQUEST['act'] == 'list_orders')
{
    /* 检查权限 */
    admin_priv('affiliate_ck');
    
    // 載入語言文件
    require_once ROOT_PATH . 'languages/' . $_CFG['lang'] . '/admin/order.php';
    $smarty->assign('lang', $_LANG);
    
    $list = get_affiliate_order_list();
    $smarty->assign('ur_here',      '審批獎勵');
    $smarty->assign('full_page',    1);
    
    $smarty->assign('list',         $list['data']);
    $smarty->assign('filter',       $list['filter']);
    $smarty->assign('record_count', $list['record_count']);
    $smarty->assign('page_count',   $list['page_count']);
    
    $smarty->assign('action_link', array('text' => '返回推廣大使列表', 'href' => 'affiliateyoho.php?act=list'));
    
    $sort_flag  = sort_flag($list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);
    
    assign_query_info();
    $smarty->display('affiliateyoho_order_list.htm');
}
elseif ($_REQUEST['act'] == 'query_orders')
{
    /* 检查权限 */
    admin_priv('affiliate_ck');
    
    // 載入語言文件
    require_once ROOT_PATH . 'languages/' . $_CFG['lang'] . '/admin/order.php';
    $smarty->assign('lang', $_LANG);
    
    $list = get_affiliate_order_list();
    $smarty->assign('list',         $list['data']);
    $smarty->assign('filter',       $list['filter']);
    $smarty->assign('record_count', $list['record_count']);
    $smarty->assign('page_count',   $list['page_count']);
    
    $sort_flag  = sort_flag($list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);
    
    make_json_result($smarty->fetch('affiliateyoho_order_list.htm'), '', array('filter' => $list['filter'], 'page_count' => $list['page_count']));
}
elseif ($_REQUEST['act'] == 'edit_reward_amount')
{
    /* 检查权限 */
    check_authz_json('affiliate_ck');
    
    $order_goods_id = intval($_POST['id']);
    $reward_amount = doubleval($_POST['val']);
    
    $sql = "SELECT '1' FROM " . $ecs->table('affiliate_rewards_log') . " WHERE `order_goods_id` = '" . $order_goods_id . "'";
    if ($db->getOne($sql))
    {
        $log = array(
            'reward_amount' => $reward_amount,
            'reward_time' => gmtime(),
            'reward_operator' => $_SESSION['admin_name']
        );
        
        $db->autoExecute($ecs->table('affiliate_rewards_log'), $log, 'UPDATE', " `order_goods_id` = '" . $order_goods_id . "'");
    }
    else
    {
        $sql = "SELECT * FROM " . $ecs->table('order_goods') . " WHERE `rec_id` = '" . $order_goods_id . "'";
        $og = $db->getRow($sql);
        
        $reward_id = intval($_POST['reward_id']);
        $sql = "SELECT * FROM " . $ecs->table('affiliate_rewards') . " WHERE `reward_id` = '" . $reward_id . "'";
        $reward = $db->getRow($sql);
        
        $log = array(
            'order_goods_id' => $order_goods_id,
            'order_id' => $og['order_id'],
            'goods_id' => $og['goods_id'],
            'reward_id' => $reward_id,
            'user_id' => $reward['user_id'],
            'reward_amount' => $reward_amount,
            'reward_time' => gmtime(),
            'reward_operator' => $_SESSION['admin_name']
        );
        
        $db->autoExecute($ecs->table('affiliate_rewards_log'), $log, 'INSERT');
    }
    
    make_json_result(price_format($reward_amount));
}

// ===================================================
// ==================== Functions ====================
// ===================================================

function get_affiliate_list()
{
    global $db, $ecs;
    
    $filter['sort_by']      = empty($_REQUEST['sort_by']) ? 'user_id' : trim($_REQUEST['sort_by']);
    $filter['sort_order']   = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);

    $sql = "SELECT count(*) FROM " . $ecs->table('users') . "WHERE is_affiliate = 1";
    $filter['record_count'] = $db->getOne($sql);

    /* 分页大小 */
    $filter = page_and_size($filter);

    /* 查询 */
    $sql = "SELECT u.*, rei.`content` as display_name, IF(reward_id IS NULL, 0, 1) as active_rewards " .
            "FROM " . $ecs->table('users') . " as u " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('reg_extend_info') . " as rei ON rei.user_id = u.user_id AND rei.reg_field_id = 10 " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('affiliate_rewards') . " as ar ON ar.user_id = u.user_id " ."
                AND (" . gmtime() . " BETWEEN ar.start_time AND ar.end_time) " .
            "WHERE u.`is_affiliate` = 1 " .
            "GROUP BY u.`user_id` " .
            "ORDER BY " . $filter['sort_by'] . ' ' . $filter['sort_order'] . ' ' .
            "LIMIT " . $filter['start'] . "," . $filter['page_size'];
    $data = $db->getAll($sql);
    
    foreach ($data as $key => $row)
    {
        $data[$key]['display_name'] = empty($row['display_name']) ? $row['user_name'] : $row['display_name'];
    }

    $arr = array('data' => $data, 'filter' => $filter, 'page_count' => $filter['page_count'], 'record_count' => $filter['record_count']);

    return $arr;
}

function get_affiliate_info($user_id)
{
    $user_id = intval($user_id);
    
    if ($user_id <= 0)
    {
        return false;
    }
    
    $sql = "SELECT u.* " .
            "FROM " . $GLOBALS['ecs']->table('users') . " as u " .
            "WHERE u.`user_id` = '" . $user_id . "' AND u.`is_affiliate` = 1";
    
    $affiliate = $GLOBALS['db']->getRow($sql);
    
    if (!$affiliate)
    {
        return false;
    }
    
    return $affiliate;
}

function get_affiliate_rewards_list()
{
    $filter['user_id'] = empty($_REQUEST['user_id']) ? 0 : intval($_REQUEST['user_id']);
    
    $filter['sort_by']      = empty($_REQUEST['sort_by']) ? 'reward_id' : trim($_REQUEST['sort_by']);
    $filter['sort_order']   = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
    
    $sort_by = $filter['sort_by'];
    // Add table prefix to ambiguous
    if ($sort_by == 'reward_id')
    {
        $sort_by = 'ar.`reward_id`';
    }

    $sql = "SELECT count(*) " .
            "FROM " . $GLOBALS['ecs']->table('affiliate_rewards') . " as ar " .
            "WHERE 1 " . (empty($filter['user_id']) ? '' : "AND ar.user_id = '" . $filter['user_id'] . "' ");
    $filter['record_count'] = $GLOBALS['db']->getOne($sql);

    /* 分页大小 */
    $filter = page_and_size($filter);

    /* 查询 */
    $sql = "SELECT ar.*, u.*, rei.`content` as display_name " .
            "FROM " . $GLOBALS['ecs']->table('affiliate_rewards') . " as ar " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('users') . " as u ON u.user_id = ar.user_id " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('reg_extend_info') . " as rei ON rei.user_id = u.user_id AND rei.reg_field_id = 10 " .
            "WHERE 1 " . (empty($filter['user_id']) ? '' : "AND ar.user_id = '" . $filter['user_id'] . "' ") .
            "ORDER BY " . $sort_by . ' ' . $filter['sort_order'] . ' ' .
            "LIMIT " . $filter['start'] . "," . $filter['page_size'];
    $data = $GLOBALS['db']->getAll($sql);
    
    $list = array();
    foreach ($data as $row)
    {
        $row['display_name'] = empty($row['display_name']) ? $row['user_name'] : $row['display_name'];
        $row['start_date'] = local_date($GLOBALS['_CFG']['time_format'], $row['start_time']);
        $row['end_date'] = local_date($GLOBALS['_CFG']['time_format'], $row['end_time']);
        
        $list[$row['reward_id']] = $row;
    }
    
    $reward_ids = array_map(function ($row) { return $row['reward_id']; }, $data);
    
    $sql = "SELECT ard.*, g.*, c.* " .
            "FROM " . $GLOBALS['ecs']->table('affiliate_rewards_detail') . " as ard " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON g.goods_id = ard.related_id AND ard.type = 'goods' " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('category') . " as c ON c.cat_id = ard.related_id AND ard.type = 'category' " .
            "WHERE ard.reward_id " . db_create_in($reward_ids) . " " .
            "ORDER BY ard.reward_id, ard.type";
    $details = $GLOBALS['db']->getAll($sql);
    
    foreach ($details as $detail)
    {
        if (!isset($list[$detail['reward_id']]['details']))
        {
            $list[$detail['reward_id']]['details'] = array($detail);
        }
        else
        {
            $list[$detail['reward_id']]['details'][] = $detail;
        }
    }
    
    $arr = array('data' => $list, 'filter' => $filter, 'page_count' => $filter['page_count'], 'record_count' => $filter['record_count']);

    return $arr;
}

function get_affiliate_reward_info($reward_id)
{
    $reward_id = intval($reward_id);
    
    if ($reward_id <= 0)
    {
        return false;
    }
    
    $sql = "SELECT ar.*, u.*, rei.`content` as display_name " .
            "FROM " . $GLOBALS['ecs']->table('affiliate_rewards') . " as ar " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('users') . " as u ON u.user_id = ar.user_id " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('reg_extend_info') . " as rei ON rei.user_id = u.user_id AND rei.reg_field_id = 10 " .
            "WHERE ar.reward_id = '" . $reward_id . "' " .
            "LIMIT 1";
    $reward = $GLOBALS['db']->getRow($sql);
    
    if (!$reward)
    {
        return false;
    }
    else
    {
        $reward['display_name'] = empty($reward['display_name']) ? $reward['user_name'] : $reward['display_name'];
        $reward['start_date'] = local_date($GLOBALS['_CFG']['time_format'], $reward['start_time']);
        $reward['end_date'] = local_date($GLOBALS['_CFG']['time_format'], $reward['end_time']);
        
        $sql = "SELECT ard.*, g.*, c.* " .
                "FROM " . $GLOBALS['ecs']->table('affiliate_rewards_detail') . " as ard " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON g.goods_id = ard.related_id AND ard.type = 'goods' " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('category') . " as c ON c.cat_id = ard.related_id AND ard.type = 'category' " .
                "WHERE ard.reward_id = '" . $reward_id . "' " .
                "ORDER BY ard.reward_id, ard.type";
        $reward['details'] = $GLOBALS['db']->getAll($sql);
        
        $reward['range'] = array(
            'all' => false,
            'category' => array(),
            'goods' => array()
        );
        foreach ($reward['details'] as $detail)
        {
            if ($detail['type'] == 'all')
            {
                $reward['range']['all'] = true;
            }
            elseif ($detail['type'] == 'category')
            {
                $reward['range']['category'][] = $detail;
            }
            elseif ($detail['type'] == 'goods')
            {
                $reward['range']['goods'][] = $detail;
            }
        }
    }
    
    return $reward;
}

function get_affiliate_order_list()
{
    global $_CFG, $db, $ecs;
    
    $filter['user_id'] = empty($_REQUEST['user_id']) ? 0 : intval($_REQUEST['user_id']);
    
    $filter['sort_by']      = empty($_REQUEST['sort_by']) ? 'order_id' : trim($_REQUEST['sort_by']);
    $filter['sort_order']   = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
    
    $where_status = " AND oi.order_status NOT IN (" . OS_CANCELED . ',' . OS_INVALID . ',' . OS_RETURNED . ") ".
                    " AND oi.pay_status = " . PS_PAYED . " ";
    
    $sql = "SELECT count(oi.order_id) " .
            "FROM " . $ecs->table('order_info') ." as oi ".
            "LEFT JOIN " . $GLOBALS['ecs']->table('users') . " as u ON u.user_id = oi.parent_id " .
            "WHERE oi.parent_id != 0 " . $where_status .
            (!empty($filter['user_id']) ? "AND oi.parent_id = '" . $filter['user_id'] . "'" : ' AND u.`is_affiliate` = 1 '); // check user is 推廣大使
    $filter['record_count'] = $db->getOne($sql);
    
    /* 分页大小 */
    $filter = page_and_size($filter);
    
    /* 查询 */
    $sql = "SELECT oi.order_id, oi.order_sn, oi.order_status, oi.pay_status, oi.shipping_status, oi.add_time " .
            "FROM " . $ecs->table('order_info') ." as oi ".
            "LEFT JOIN " . $GLOBALS['ecs']->table('users') . " as u ON u.user_id = oi.parent_id " .
            "WHERE oi.parent_id != 0 " . $where_status .
            (!empty($filter['user_id']) ? "AND oi.parent_id = '" . $filter['user_id'] . "'" : ' AND u.`is_affiliate` = 1 ') . // check user is 推廣大使
            "ORDER BY " . $filter['sort_by'] . ' ' . $filter['sort_order'] . ' ' .
            "LIMIT " . $filter['start'] . "," . $filter['page_size'];
    $data = $db->getAll($sql);
    
    $list = array();
    foreach ($data as $key => $row)
    {
        $row['add_time_formatted'] = local_date($_CFG['time_format'], $row['add_time']);
        $list[$row['order_id']] = $row;
    }
    
    $order_ids = array_map(function ($row) { return $row['order_id']; }, $data);
    
    $sql = "SELECT og.rec_id, og.order_id, og.goods_number, og.goods_price, " .
                "g.goods_id, g.goods_sn, g.goods_name, g.cat_id, " .
                "arl.reward_amount, arl.reward_time, ar.reward_id, ar.reward_value, ard.type, " .
                "TRUNCATE(IF(RIGHT(ar.`reward_value`, 1) = '%', (og.goods_price * og.goods_number - IF(oi.`pay_fee` < 0, oi.`pay_fee` * -1 * og.goods_price * og.goods_number / oi.goods_amount, 0)) * ar.`reward_value` / 100, ar.`reward_value` * og.goods_number - IF(oi.`pay_fee` < 0 AND ar.`reward_value` > 0, oi.`pay_fee` * -1 * og.goods_price * og.goods_number / oi.goods_amount, 0)), 2) as suggested_reward " .
            "FROM " . $ecs->table('order_goods') . " as og " .
            "LEFT JOIN " . $ecs->table('order_info') . " as oi ON oi.order_id = og.order_id " .
            "LEFT JOIN " . $ecs->table('goods') . " as g ON g.goods_id = og.goods_id " .
            "LEFT JOIN " . $ecs->table('affiliate_rewards_log') . " as arl ON arl.order_goods_id = og.rec_id " .
            "LEFT JOIN " . $ecs->table('affiliate_rewards') . " as ar ON (oi.add_time BETWEEN ar.start_time AND ar.end_time) " .
                "AND (( " .
                    "arl.reward_id IS NOT NULL AND ar.reward_id = arl.reward_id ".
                ") OR (" .
                    "arl.reward_id IS NULL AND ar.user_id = oi.parent_id " .
                ")) " .
            "LEFT JOIN " . $ecs->table('affiliate_rewards_detail') . " as ard ON ard.reward_id = ar.reward_id " .
                "AND ((" .
                    "ard.type = 'goods' AND ard.related_id = og.goods_id ".
                ") OR (" .
                    "ard.type = 'category' AND ard.related_id = g.cat_id " .
                ") OR (" .
                    "ard.type = 'all' " .
                "))" .
            "WHERE og.order_id " . db_create_in($order_ids) . " " .
            "AND ard.detail_id IS NOT NULL "; // so this goods is eligible for a reward
    $order_goods = $db->getAll($sql);
    
    foreach ($order_goods as $og)
    {
        $og['reward_amount_formatted'] = price_format($og['reward_amount']);
        
        if (!isset($list[$og['order_id']]['goods']))
        {
            $list[$og['order_id']]['goods'] = array();
        }
        if (isset($list[$og['order_id']]['goods'][$og['rec_id']]))
        {
            // Multiple rewards matched for this entry
            // We prefer exact goods match over category match over all
            $type_priorities = array('all' => 0, 'category' => 1, 'goods' => 2);
            $type1 = $list[$og['order_id']]['goods'][$og['rec_id']]['type'];
            $type2 = $og['type'];
            if ($type_priorities[$type2] > $type_priorities[$type1])
            {
                $list[$og['order_id']]['goods'][$og['rec_id']] = $og;
            }
            // If both rewards have the same type, choose the one with better reward
            elseif ($type_priorities[$type2] == $type_priorities[$type1])
            {
                $suggested_reward1 = $list[$og['order_id']]['goods'][$og['rec_id']]['suggested_reward'];
                $suggested_reward2 = $og['suggested_reward'];
                if ($suggested_reward2 > $suggested_reward1)
                {
                    $list[$og['order_id']]['goods'][$og['rec_id']] = $og;
                }
            }
        }
        else
        {
            $list[$og['order_id']]['goods'][$og['rec_id']] = $og;
        }
    }
    
    // header('Content-Type: text/plain;charset=utf-8');
    // print_r($list);
    // exit;
    
    $arr = array('data' => $list, 'filter' => $filter, 'page_count' => $filter['page_count'], 'record_count' => $filter['record_count']);
    
    return $arr;
}

?>