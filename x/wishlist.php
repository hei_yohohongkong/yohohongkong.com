<?php

/***
* wishlist.php
* by howang 2014-08-25
*
* List user's wish list
***/

define('IN_ECS', true);
require(dirname(__FILE__) . '/includes/init.php');


if ($_REQUEST['act'] == 'list')
{
    /* 检查权限 */
    admin_priv('users_manage');
    
    $list = get_wishlist();
    $smarty->assign('ur_here',      $_LANG['13_wishlist']);
    $smarty->assign('full_page',    1);

    $smarty->assign('list',         $list['data']);
    $smarty->assign('filter',       $list['filter']);
    $smarty->assign('record_count', $list['record_count']);
    $smarty->assign('page_count',   $list['page_count']);
    
    $sort_flag = sort_flag($list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);

    assign_query_info();
    $smarty->display('wishlist.htm');
}
elseif ($_REQUEST['act'] == 'query')
{
    /* 检查权限 */
    check_authz_json('users_manage');
    
    $list = get_wishlist();
    $smarty->assign('list',         $list['data']);
    $smarty->assign('filter',       $list['filter']);
    $smarty->assign('record_count', $list['record_count']);
    $smarty->assign('page_count',   $list['page_count']);

    $sort_flag = sort_flag($list['filter']);
	$smarty->assign($sort_flag['tag'], $sort_flag['img']);

    make_json_result($smarty->fetch('wishlist.htm'), '', array('filter' => $list['filter'], 'page_count' => $list['page_count']));
}


function get_wishlist()
{
    $filter['keyword']      = empty($_REQUEST['keyword']) ? '' : trim($_REQUEST['keyword']);
    $filter['user']         = empty($_REQUEST['user']) ? '' : trim($_REQUEST['user']);
    
    $filter['sort_by']      = empty($_REQUEST['sort_by']) ? 'add_time' : trim($_REQUEST['sort_by']);
    $filter['sort_order']   = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
    
    $where = '1';
    
    if (!empty($filter['keyword']))
    {
        $where .= " AND (g.`goods_sn` LIKE '%" . mysql_like_quote($filter['keyword']) . "%'";
        $where .= " OR g.`goods_name` LIKE '%" . mysql_like_quote($filter['keyword']) . "%'";
        $where .= " OR g.`goods_id` = '" . mysql_like_quote($filter['keyword']) . "')";
    }
    
    if ($filter['user'])
    {
        if ($_SESSION['manage_cost']) // 如果是超級管理員，可以 wildcard match
        {
            $where .= " AND (u.`user_name` LIKE '%" . mysql_like_quote($filter['user']) ."%'";
            $where .= " OR u.`email` LIKE '%" . mysql_like_quote($filter['user']) ."%')";
        }
        else
        {
            // Non-admin, exact match only
            $where .= " AND (u.`user_name` = '" . $filter['keywords'] ."' OR u.`email` = '" . $filter['keywords'] ."')";
        }
    }
    
    $sql = "SELECT count(*) FROM " . $GLOBALS['ecs']->table('wish_list') . " as wl";
    $sql.= " LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON g.`goods_id` = wl.`goods_id`";
    $sql.= " LEFT JOIN " . $GLOBALS['ecs']->table('users') . " as u ON u.`user_id` = wl.`user_id`";
    $sql.= " WHERE " . $where;
    $filter['record_count'] = $GLOBALS['db']->getOne($sql);
    
    /* 分页大小 */
    $filter = page_and_size($filter);
    
    /* 查询 */
    $sql = "SELECT wl.*, g.`goods_name`, g.`goods_thumb`, g.`goods_sn`, u.`user_name`, rei.`content` as `display_name`,";
    $sql.= " (SELECT count(*) FROM " . $GLOBALS['ecs']->table('wish_list') . " as wl2 WHERE wl2.goods_id = wl.goods_id) as total_added";
    $sql.= " FROM " . $GLOBALS['ecs']->table('wish_list') . " as wl";
    $sql.= " LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON g.`goods_id` = wl.`goods_id`";
    $sql.= " LEFT JOIN " . $GLOBALS['ecs']->table('users') . " as u ON u.`user_id` = wl.`user_id`";
    $sql.= " LEFT JOIN " . $GLOBALS['ecs']->table('reg_extend_info') . " as rei ON rei.`user_id` = wl.`user_id` AND rei.`reg_field_id` = 10";
    $sql.= " WHERE " . $where;
    $sql.= " ORDER by " . $filter['sort_by'] . ' ' . $filter['sort_order'];
    $sql.= " LIMIT " . $filter['start'] . ",$filter[page_size]";
    set_filter($filter, $sql);

    $data = $GLOBALS['db']->getAll($sql);
    
    foreach ($data as $key => $row)
    {
        $data[$key]['display_name'] = empty($row['display_name']) ? $row['user_name'] : $row['display_name'];
        $data[$key]['add_date'] = local_date($GLOBALS['_CFG']['time_format'], $row['add_time']);
    }

    $arr = array('data' => $data, 'filter' => $filter, 'page_count' => $filter['page_count'], 'record_count' => $filter['record_count']);

    return $arr;
}
?>