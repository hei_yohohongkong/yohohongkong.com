<?php

define('IN_ECS', true);
require_once dirname(__FILE__) . '/includes/init.php';
require_once ROOT_PATH . '/languages/' . $_CFG['lang'] . '/admin/accounting_system.php';

$accountingController = new Yoho\cms\Controller\AccountingController();

if ($_REQUEST['act'] == 'list') {
    $action_link_list = [
        [
            'text' => $_LANG['accounting_insert_type'],
			'href' => 'javascript:insertNewCategory(0)'
        ]
    ];
    $smarty->assign('ur_here', $_LANG['accounting_types']);
    $smarty->assign('action_link_list', $action_link_list);
    $smarty->display('accounting_type_list.htm');
} elseif ($_REQUEST['act'] == 'query') {
    $accountingController->getAccountingTypesList();
} elseif ($_REQUEST['act'] == 'get_next_code') {
    $accountingController->getNextTypeCode();
} elseif ($_REQUEST['act'] == 'ajaxEdit') {
    if (!empty($_REQUEST['id']) && !empty($_REQUEST['col']))  {
        $db->query("UPDATE `actg_accounting_types` SET $_REQUEST[col] = '$_REQUEST[value]' WHERE actg_type_id = $_REQUEST[id]");
        make_json_response($_REQUEST['value']);
    }
    make_json_error('Missing parameters');
} elseif ($_REQUEST['act'] == 'insert') {
    $accountingController->insertAccountingTypes();
}

?>