<?php

define('IN_ECS', true);
require dirname(__FILE__) . '/includes/init.php';
ini_set('memory_limit', '256M');

$autoPricingController = new Yoho\cms\Controller\AutoPricingController();
$autoPricingStrategyController = new Yoho\cms\Controller\YohoBaseController("auto_pricing_strategy");
$autoPricingRuleController = new Yoho\cms\Controller\YohoBaseController("auto_pricing_rules");
$autoPricingSuggestionController = new Yoho\cms\Controller\YohoBaseController("auto_pricing_suggestion");
$erpController = new Yoho\cms\Controller\erpController();

$webs = $autoPricingController::AUTO_PRICING_WEBS;
$websites = $autoPricingController::AUTO_PRICING_WEBSITES;
$values = $autoPricingController->getAllValues();
$types = $autoPricingController::AUTO_PRICING_TYPES;
$rule_types = $autoPricingController::AUTO_PRICING_RULE_TYPE;
$dead_section = $autoPricingController::AUTO_PRICING_AUTO_ON_SALE_DEAD_SECTION;
$suggest_types = $autoPricingController::AUTO_PRICING_SUGGEST_TYPES;
$webs_column = $autoPricingController::AUTO_PRICING_WEBS_COLUMNS_NAMES;

$smarty->assign("webs", $webs);
$smarty->assign("websites", $websites);
$smarty->assign("values", $values);
$smarty->assign("types", $types);
$smarty->assign("rule_types", $rule_types);
$smarty->assign("dead_section", $dead_section);
$smarty->assign("suggest_types", $suggest_types);
$smarty->assign("webs_column", $webs_column);

if (in_array($_REQUEST['act'], ['edit', 'remove'])) {
    if (in_array($_REQUEST['step'], ["strategy", "rule"])) {
        $_REQUEST['act'] = "$_REQUEST[step]_$_REQUEST[act]";
    }
}
if ($_REQUEST['act'] == 'list') {
    $smarty->assign("ur_here", $_LANG['auto_pricing_product_list']);
    $smarty->assign("categorys", $autoPricingController->getCategoryList());
    $smarty->assign("brands", $autoPricingController->getBrandList());
    $smarty->assign("suppliers", $autoPricingController->getSupplierList());
    $smarty->assign("goods_warehouse_list", $erpController->getGoodsWarehouseList());

    $links = $_SESSION['manage_cost'] ? [['text' => $_LANG['auto_pricing_strategy_list'], 'href' => 'auto_pricing.php?act=strategy_list']] : [];
    $links[] = ['text' => $_LANG['auto_pricing_rule_list'], 'href' => 'auto_pricing.php?act=rule_list'];
    $smarty->assign('action_link_list', $links);
    assign_query_info();
    $smarty->display("auto_pricing.htm");
} elseif ($_REQUEST['act'] == 'query') {
    $list = $autoPricingController->getAutoPricingList();
    foreach ($list['list'] as $key => $row) {
        $list['list'][$key]["batch_select"] = "<td><input class='flat' type='checkbox' name='batch' value='$row[goods_id]'></td>";
        $custom_action = [
            'edit_auto_pricing' => ['icon_class'=> 'fa fa-edit','btn_class'=> 'btn-info', 'link' => './auto_pricing.php?act=edit&id='.$row['goods_id'], 'label' =>'編輯自動定價', 'target' => '_blank'],
            'edit_product' => ['icon_class'=> 'fa fa-edit','btn_class'=> 'btn-info', 'link' => './goods.php?act=edit&id='.$row['goods_id'], 'label' =>'編輯產品信息', 'target' => '_blank']
        ];
        $list['list'][$key]["_action"] = $autoPricingController->generateCrudActionBtn($row['goods_id'], 'id', null, '', $custom_action);
    }
    $autoPricingController->ajaxQueryAction($list['list'], $list['count'], false);
} elseif ($_REQUEST['act'] == 'edit') {
    $good = $autoPricingController->getAutoPricingProduct();
    $list = $autoPricingController->getAutoPricingStrategyList();

    if (!empty($_REQUEST['ids'])) {
        $smarty->assign("batch", 1);
    }
    $smarty->assign("good", $good);
    $smarty->assign("list", $list);
    $smarty->assign("ur_here", $_LANG['auto_pricing_product_edit']);
    $smarty->assign('action_link', array('text' => $_LANG['auto_pricing_product_list'], 'href' => 'auto_pricing.php?act=list'));
    assign_query_info();
    $smarty->display("auto_pricing_edit.htm");
} elseif ($_REQUEST['act'] == 'update') {
    $autoPricingController->updateAutoPricingProduct();
} elseif ($_REQUEST['act'] == 'strategy_list') {
    $smarty->assign("ur_here", $_LANG['auto_pricing_strategy_list']);
    $smarty->assign('action_link_list', [
        ['text' => $_LANG['auto_pricing_strategy_add'], 'href' => 'auto_pricing.php?act=strategy_add'],
        ['text' => $_LANG['auto_pricing_strategy_default'], 'href' => 'auto_pricing.php?act=strategy_default'],
        ['text' => $_LANG['auto_pricing_product_list'], 'href' => 'auto_pricing.php?act=list'],
    ]);
    assign_query_info();
    $smarty->display("auto_pricing_strategy_list.htm");
} elseif ($_REQUEST['act'] == 'strategy_query') {
    $list = $autoPricingController->getAutoPricingStrategyList();
    foreach ($list as $key => $row) {
        $list[$key]["_action"] = $autoPricingController->generateCrudActionBtn($row['strategy_id'], 'step=strategy&strategy_id', null);
    }
    $autoPricingStrategyController->ajaxQueryAction($list, count($list), false);    
} elseif ($_REQUEST['act'] == 'strategy_add' || $_REQUEST['act'] == 'strategy_edit') {
    if ($_REQUEST['act'] == 'strategy_edit') {
        if (!empty($_REQUEST['strategy_id'])) {
            $strategy = $autoPricingController->getAutoPricingStrategy($_REQUEST['strategy_id']);
            $smarty->assign("strategy", $strategy);
        }
        $smarty->assign("ur_here", $_LANG['auto_pricing_strategy_edit']);
    } else {
        $smarty->assign("ur_here", $_LANG['auto_pricing_strategy_add']);
    }
    $smarty->assign('action_link', array('text' => $_LANG['auto_pricing_strategy_list'], 'href' => 'auto_pricing.php?act=strategy_list'));
    assign_query_info();
    $smarty->display("auto_pricing_strategy_edit.htm");
} elseif ($_REQUEST['act'] == 'strategy_update') {
    $autoPricingController->updateAutoPricingStrategy();
} elseif ($_REQUEST['act'] == 'strategy_default') {
    $list = $autoPricingController->getAutoPricingStrategyList();
    $strategy_id = $autoPricingController->getDefaultAutoPriceStrategy($autoPricingController::AUTO_PRICING_DEFAULT_CODE);
    $smarty->assign("strategy_id", $strategy_id);
    $smarty->assign("list", $list);
    $smarty->assign("has_type", implode(",", $has_type));
    $smarty->assign("default_auto_price", $autoPricingController->getDefaultAutoPriceStrategy("default_auto_price"));
    $smarty->assign("default_auto_price_dead", $autoPricingController->getDefaultAutoPriceStrategy("default_auto_price_dead"));
    $smarty->assign("ur_here", $_LANG['auto_pricing_strategy_default']);
    $smarty->assign('action_link_list', [
        ['text' => $_LANG['auto_pricing_product_list'], 'href' => 'auto_pricing.php?act=list'],
    ]);
    assign_query_info();
    $smarty->display("auto_pricing_strategy_default.htm");
} elseif ($_REQUEST['act'] == 'strategy_default_update') {
    $autoPricingController->updateAutoPricingDefaultStrategy();
} elseif ($_REQUEST['act'] == 'rule_list') {
    $smarty->assign("ur_here", $_LANG['auto_pricing_rule_list']);
    $smarty->assign('action_link_list', [
        ['text' => $_LANG['auto_pricing_rule_add'], 'href' => 'auto_pricing.php?act=rule_add'],
        ['text' => $_LANG['auto_pricing_product_list'], 'href' => 'auto_pricing.php?act=list'],
    ]);
    assign_query_info();
    $smarty->display("auto_pricing_rule_list.htm");
} elseif ($_REQUEST['act'] == 'rule_query') {
    $list = $autoPricingController->getAutoPricingRuleList();
    foreach ($list as $key => $row) {
        $list[$key]["_action"] = $autoPricingController->generateCrudActionBtn($row['rule_id'], 'step=rule&rule_id', null);
    }
    $autoPricingStrategyController->ajaxQueryAction($list, count($list), false);
} elseif ($_REQUEST['act'] == 'rule_add' || $_REQUEST['act'] == 'rule_edit') {
    $cat_list = $autoPricingController->getCategoryList();
    $brand_list = $autoPricingController->getBrandList();
    $cat_names = [];
    $brand_names = [];
    if ($_REQUEST['act'] == 'rule_edit') {
        if (!empty($_REQUEST['rule_id'])) {
            $rule = $autoPricingController->getAutoPricingRule($_REQUEST['rule_id']);
            foreach ($cat_list as $key => $row) {
                if (in_array($row['cat_id'], $rule['config']['cats'])) {
                    $cat_list[$key]['selected'] = 1;
                    $cat_names[] = $row["cat_name"];
                }
            }
            foreach ($brand_list as $key => $row) {
                if (in_array($row['brand_id'], $rule['config']['brands'])) {
                    $brand_list[$key]['selected'] = 1;
                    $brand_names[] = $row["brand_name"];
                }
            }
        }
        $smarty->assign("ur_here", $_LANG['auto_pricing_rule_edit']);
    } else {
        $smarty->assign("ur_here", $_LANG['auto_pricing_rule_add']);
    }
    $rule['cat_names'] = empty($cat_names) ? "N/A" : implode(", ", $cat_names);
    $rule['brand_names'] = empty($brand_names) ? "N/A" : implode(", ", $brand_names);
    $smarty->assign("rule", $rule);
    $smarty->assign('cat_list', $cat_list);
    $smarty->assign('brand_list', $brand_list);
    $smarty->assign('action_link', array('text' => $_LANG['auto_pricing_rule_list'], 'href' => 'auto_pricing.php?act=rule_list'));
    assign_query_info();
    $smarty->display("auto_pricing_rule_edit.htm");
} elseif ($_REQUEST['act'] == 'rule_update') {
    $autoPricingController->updateAutoPricingRule();
} elseif ($_REQUEST['act'] == 'rule_remove') {
    $autoPricingController->removeAutoPricingRule();
} elseif ($_REQUEST['act'] == "ajaxEdit") {
    if ($_REQUEST['action'] == 'list') {
        // Log goods changes
        $sql = "INSERT INTO " . $ecs->table('goods_log') .
                "SELECT NULL, '" . gmtime() . "', '$_SESSION[admin_name]', g.* " .
                "FROM " . $ecs->table('goods') . " as g " .
                "WHERE goods_id = $_REQUEST[id]";
        $db->query($sql);

        if ($_REQUEST['col'] == "shop_price") {
            $val = $_REQUEST['value'];
            if (strpos($val, "%")) {
                $rate = strstr($val, "%", true);
                $sql = "SELECT market_price FROM " . $ecs->table('goods') .
                        " WHERE  goods_id = $_REQUEST[id]";
                $market_price = $db->getOne($sql);
                $goods_price = intval($market_price * ($rate/100));
            } else {
                $goods_price = intval($val);
            }

            if ($goods_price > 0) {
                $org_price = $db->getOne("SELECT shop_price FROM " . $ecs->table('goods') . " WHERE goods_id = $_REQUEST[id]");
                //判斷shop price是否小於vip price
                $sql = "SELECT user_price FROM " . $ecs->table('member_price') .
                " WHERE user_rank = '2' AND goods_id = '$_REQUEST[id]'";
                $vip_price = $db->getOne($sql);

                if ($goods_price < $vip_price) {
                    make_json_error("價格不能小於VIP價！");
                }
                if ($org_price != $goods_price) {
                    if (@include_once ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_notifications.php') {
                        notify_price_changed($db->getRow("SELECT goods_id, goods_name, goods_sn FROM " . $ecs->table('goods') . " WHERE goods_id = $_REQUEST[id]"), $org_price, $goods_price);
                    } else {
                        error_log(__FILE__.':'.__LINE__.' Cannot include lib_notifications.php');
                    }
                    $db->autoExecute(
                        $ecs->table('goods_price_log'), array(
                            'goods_id' => $_REQUEST['id'],
                            'old_price' => $org_price,
                            'new_price' => $goods_price,
                            'update_time' => gmtime(),
                            'admin_id' => $_SESSION['admin_id']
                        ), 'INSERT'
                    );

                    $db->query("UPDATE " . $ecs->table('goods') . " SET shop_price = $goods_price WHERE goods_id = $_REQUEST[id]");
                }
                make_json_result(number_format($goods_price, 2, '.', ''));
            }
        } elseif (in_array($_REQUEST['col'], ["goods_name", "market_price"])) {
            $autoPricingController->ajaxEditAction("goods");
        } elseif ($_REQUEST['col'] == "vip_price") {
            $sql = "SELECT shop_price FROM " . $ecs->table('goods') .
                    " WHERE  goods_id = '$_REQUEST[id]'";
            $good_price = $db->getOne($sql);
            /* If is a rate */
            if (strpos($_REQUEST['value'], "%")) {
                $rate = strstr($_REQUEST['value'], "%", true);
                $vip_price = intval($good_price * ($rate/100));
            } else {
                $vip_price = floatval($_REQUEST['value']);
            } 
            if ($vip_price == "@" || $vip_price == "無") {
                $db->query("DELETE FROM " . $ecs->table("member_price") . " WHERE goods_id = $_REQUEST[id] AND user_rank = 2");
                make_json_result("--");
            } elseif (is_numeric($vip_price) && floatval($vip_price) > 0) {
                $sql = "SELECT shop_price FROM " . $ecs->table('goods') .
                    " WHERE  goods_id = '$_REQUEST[id]'";
                $good_price = $db->getOne($sql);
                if ($vip_price > $good_price){
                    make_json_error("VIP價不能大於價格！");
                }
                if ($db->getOne("SELECT 1 FROM " . $ecs->table("member_price") . " WHERE goods_id = $_REQUEST[id] AND user_rank = 2")) {
                    $db->query("UPDATE " . $ecs->table("member_price") . " SET user_price = $vip_price WHERE goods_id = $_REQUEST[id] AND user_rank = 2");
                } else {
                    $db->query("INSERT INTO " . $ecs->table("member_price") . " (goods_id, user_rank, user_price) VALUES ($_REQUEST[id], 2, $vip_price) ");
                }
                make_json_result(number_format($vip_price, 2, '.', ''));
            } elseif ($_REQUEST['value'] < 0 || $_REQUEST['value'] == 00) {
                make_json_error("您好像输错了吧");
            }

        } elseif ($_REQUEST['col'] == "promote") {
            if ($_REQUEST['is_promote']) {
                $promote_date = empty($_REQUEST['promote_date']) ? "0 to 0" : $_REQUEST['promote_date'];
                $promote_price = empty($_REQUEST['promote_price']) ? 0 : floatval($_REQUEST['promote_price']);
                $promote_dates = explode(" to ", $promote_date);
                $promote_start_date = empty($promote_dates[0]) ? 0 : local_strtotime($promote_dates[0]);
                $promote_end_date = empty($promote_dates[1]) ? 0 : local_strtotime($promote_dates[1]);
                $db->query("UPDATE " . $ecs->table("goods") . " SET is_promote = 1, promote_price = '$promote_price', promote_start_date = '$promote_start_date', promote_end_date = '$promote_end_date' WHERE goods_id = $_REQUEST[id]");
            } else {
                $db->query("UPDATE " . $ecs->table("goods") . " SET is_promote = 0 WHERE goods_id = $_REQUEST[id]");
            }
        } elseif($_REQUEST['col'] == "pre_sale_days") {
            $autoPricingController->ajaxEditAction("goods"); 
        }
    } else {
        $table = $_REQUEST['action'] == 'strategy_list' ? "auto_pricing_strategy" : ($_REQUEST['action'] == 'rule_list' ? "auto_pricing_rules" : "");
        $autoPricingController->ajaxEditAction($table);
    }
    make_json_result($_REQUEST['value']);
} elseif ($_REQUEST['act'] == 'suggestion_query') {
    $list = $autoPricingController->getAutoPricingSuggestion();
    $autoPricingSuggestionController->ajaxQueryAction($list['list'], $list['count'], false);
} elseif ($_REQUEST['act'] == 'suggest') {
    $autoPricingController->updateAutoPricingSuggestion();
} elseif ($_REQUEST['act'] == 'web_price') {
    $autoPricingController->getAutoPricingPlatformPrice();
} elseif ($_REQUEST['act'] == 'update_web' || $_REQUEST['act'] == 'delete_web') {
    $autoPricingController->updateAutoPricingPlatformPrice();
} elseif ($_REQUEST['act'] == 'auto_update_goods') {
    $autoPricingController->updateAutoPricingGoodsAuto();
} elseif ($_REQUEST['act'] == 'update_goods_strategy') {
    $autoPricingController->updateAutoPricingGoodsStrategy();
} elseif ($_REQUEST['act'] == 'update_goods_price') {
    $autoPricingController->updatePlatformsGoodsPrice();
} elseif ($_REQUEST['act'] == 'fast_update') {
    $autoPricingController->updatePlatformsGoodsCode();
}
?>