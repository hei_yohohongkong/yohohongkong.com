<?php

/***
* flashdeal.php
* by howang 2015-02-09
*
* Manage flash deals
***/

define('IN_ECS', true);
require(dirname(__FILE__) . '/includes/init.php');
$fleashdealController = new Yoho\cms\Controller\FlashdealController();

if ($_REQUEST['act'] == 'list')
{   
    if (!isset($_REQUEST['test'])){
        sys_msg('請使用新版本閃購活動');
    }
    $smarty->assign('ur_here',      $_LANG['17_flashdeals']);
    $smarty->assign('start_date',$_REQUEST['start_date']);
    $smarty->assign('search_type',$_REQUEST['search_type']);
    $smarty->assign('search_str',$_REQUEST['search_str']);

    $smarty->assign('action_link', array('text' => '新增閃購', 'href' => 'flashdeal.php?act=add'));
    $smarty->assign('action_link2', array('text' => '閃購設置', 'href' => 'flashdeal.php?act=edit_flashdeal_config'));

    assign_query_info();
    $smarty->display('flashdeal_list.htm');
}
elseif ($_REQUEST['act'] == 'query')
{
    $list = $fleashdealController->get_cms_flashdeals();
    $fleashdealController->ajaxQueryAction($list['data'], $list['record_count'], false);
}
elseif (in_array($_REQUEST['act'], array('add','info','edit')))
{
    if (!isset($_REQUEST['test'])){
        sys_msg('請使用新版本閃購活動');
    }
    /* 检查权限 */
    if ($_REQUEST['act'] != 'info')
    {
        admin_priv('bonus_manage');
    }

    $smarty->assign('ur_here',      $_LANG['17_flashdeals']);

    if ($_REQUEST['act'] != 'add')
    {
        $flashdeal_id = empty($_REQUEST['id']) ? 0 : intval($_REQUEST['id']);
        $flashdeal = get_flashdeal_info($flashdeal_id);

        if (!$flashdeal)
        {
            sys_msg('找不到該閃購');
        }

        $smarty->assign('flashdeal',       $flashdeal);
    }
    else
    {
        $flashdeal = array(
            'deal_price' => 0,
            'deal_date_formatted' => local_date('Y-m-d H:i'),
            'detail_visible' => 0,
            'is_online_pay' => 0,
        );
        $smarty->assign('flashdeal',       $flashdeal);
    }

    $smarty->assign('action_link', array('text' => '返回列表', 'href' => 'flashdeal.php?act=list'));
    if ($_REQUEST['act'] == 'info')
    {
        $smarty->assign('action_link2', array('text' => '編輯閃購', 'href' => 'flashdeal.php?act=edit&id=' . $flashdeal_id));
    }
    elseif ($_REQUEST['act'] == 'edit')
    {
        $smarty->assign('action_link2', array('text' => '查看閃購', 'href' => 'flashdeal.php?act=info&id=' . $flashdeal_id));
    }

    $smarty->assign('act',          $_REQUEST['act']);

    $smarty->assign('cfg_lang',     $_CFG['lang']);

    assign_query_info();
    $smarty->display('flashdeal_info.htm');
}
elseif (in_array($_REQUEST['act'], array('insert','update')))
{
    if (!isset($_REQUEST['test'])){
        sys_msg('請使用新版本閃購活動');
    }
    /* 检查权限 */
    admin_priv('bonus_manage');

    if ($_REQUEST['act'] == 'update')
    {
        $flashdeal_id = empty($_REQUEST['flashdeal_id']) ? 0 : intval($_REQUEST['flashdeal_id']);

        if (!$flashdeal_id)
        {
            sys_msg('閃講編號錯誤');
        }
    }

    $flashdeal = array();

    if (!empty($_POST['goods_id']))
    {
        $goods_id = intval($_POST['goods_id']);
        $sql = "SELECT 1 FROM " . $ecs->table('goods') . " WHERE goods_id = '" . $goods_id . "'";
        if (!$db->getOne($sql))
        {
            sys_msg('您選擇的產品不存在');
        }
        $flashdeal['goods_id'] = $goods_id;
    }
    elseif ($_REQUEST['act'] == 'insert')
    {
        sys_msg('您必須選擇產品');
    }

    if (isset($_POST['deal_price']))
    {
        $flashdeal['deal_price'] = doubleval($_POST['deal_price']);
    }
    if (!empty($_POST['deal_date']))
    {
        $flashdeal['deal_date'] = local_strtotime($_POST['deal_date']);
    }
    if (isset($_POST['deal_quantity']))
    {
        if ($_REQUEST['act'] == 'update')
        {
            $can = $fleashdealController->update_flashdeal_goods($flashdeal_id, intval($_POST['deal_quantity']));
            if($can)$flashdeal['deal_quantity'] = intval($_POST['deal_quantity']);
            else sys_msg('你輸入的閃購數量少於已購買人數');
        } else {
            $flashdeal['deal_quantity'] = intval($_POST['deal_quantity']);
        }

    }
    if (isset($_POST['detail_visible']))
    {
        $flashdeal['detail_visible'] = intval($_POST['detail_visible']);
    }
    if (isset($_POST['is_online_pay']))
    {
        $flashdeal['is_online_pay'] = intval($_POST['is_online_pay']);
    }
    if (isset($_POST['sort_order']))
    {
        $flashdeal['sort_order'] = $_POST['sort_order'];
    }

    if ($_REQUEST['act'] == 'update')
    {
        $db->autoExecute($ecs->table('flashdeals'), $flashdeal, 'UPDATE', " `flashdeal_id` = '" . $flashdeal_id . "'");
    }
    else
    {
        $flashdeal['creator'] = $_SESSION['admin_name'];
        $db->autoExecute($ecs->table('flashdeals'), $flashdeal, 'INSERT');

        $flashdeal_id = $db->insert_id();
        $fleashdealController->update_flashdeal_goods($flashdeal_id, $flashdeal['deal_quantity']);
    }
    $fleashdealController->update_date_json();
    clear_cache_files();

    $link[0]['text'] = '查看閃講';
    $link[0]['href'] = 'flashdeal.php?act=info&id=' . $flashdeal_id;

    $link[1]['text'] = '返回列表';
    $link[1]['href'] = 'flashdeal.php?act=list';

    sys_msg(($_REQUEST['act'] == 'update' ? '編輯' : '新增') . '閃購成功', 0, $link, true);
}
elseif ($_REQUEST['act'] == 'remove')
{
    if (!isset($_REQUEST['test'])){
        sys_msg('請使用新版本閃購活動');
    }
    /* 检查权限 */
    check_authz_json('bonus_manage');

    $flashdeal_id = intval($_GET['id']);

    $sql = "DELETE FROM " . $ecs->table('flashdeals') .
            "WHERE flashdeal_id = '$flashdeal_id' " .
            "LIMIT 1";
    $db->query($sql);
    $sql_goods = "DELETE FROM " . $ecs->table('flashdeal_goods') . "WHERE flashdeal_id = '$flashdeal_id' ";
    $db->query($sql_goods);
    $fleashdealController->update_date_json();
    clear_cache_files();

    $url = 'flashdeal.php?act=query&' . str_replace('act=remove', '', $_SERVER['QUERY_STRING']);
    header('Location: ' . $url);
    exit;
}
elseif ($_REQUEST['act'] == 'ajaxEdit')
{
    check_authz_json('bonus_manage');
    $flashdeal_id = intval($_POST['id']);
    $value = $_POST['value'];
    $col = $_POST['col'];
    if ($col == 'deal_quantity') {
        if(intval($value) > 0) {
            $can = $fleashdealController->update_flashdeal_goods($flashdeal_id, $value);
            if($can){
                $fleashdealController->ajaxEditAction('flashdeals');
                $fleashdealController->update_date_json();
            }
            else make_json_error('你輸入的閃購數量少於已購買人數');
        } else make_json_error('你輸入的閃購數量少於1');
    } else {
        $fleashdealController->ajaxEditAction('flashdeals');
        $fleashdealController->update_date_json();
    }


}
elseif ($_REQUEST['act'] == 'list_notify')
{
    $list = get_flashdeal_notifys();
    $smarty->assign('ur_here',      $_LANG['17_flashdeals'] . ' - 通知列表');
    $smarty->assign('full_page',    1);

    $smarty->assign('list',         $list['data']);
    $smarty->assign('filter',       $list['filter']);
    $smarty->assign('record_count', $list['record_count']);
    $smarty->assign('page_count',   $list['page_count']);

    $smarty->assign('action_link', array('text' => '返回閃購列表', 'href' => 'flashdeal_event.php?act=list'));

    $sort_flag  = sort_flag($list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);

    assign_query_info();
    $smarty->display('flashdeal_notify_list.htm');
}
elseif ($_REQUEST['act'] == 'query_notify')
{
    $list = get_flashdeal_notifys();
    $smarty->assign('list',         $list['data']);
    $smarty->assign('filter',       $list['filter']);
    $smarty->assign('record_count', $list['record_count']);
    $smarty->assign('page_count',   $list['page_count']);

    $sort_flag  = sort_flag($list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);

    make_json_result($smarty->fetch('flashdeal_notify_list.htm'), '', array('filter' => $list['filter'], 'page_count' => $list['page_count']));
}
elseif ($_REQUEST['act'] == 'sort_order')
{
    /* 检查权限 */
    check_authz_json('bonus_manage');

    $flashdeal_id  = intval($_POST['id']);
    $sort_order = intval($_POST['val']);

    if ($sort_order < 0)
    {
        make_json_error('您輸入了一個非法的數量');
    }
    else
    {
        $sql = "UPDATE " . $ecs->table('flashdeals') .
                "SET `sort_order` = '" . $sort_order . "' " .
                "WHERE `flashdeal_id` = '" . $flashdeal_id . "' " .
                "LIMIT 1";
        if ($db->query($sql))
        {
            clear_cache_files();
            make_json_result($sort_order);
        }
        else
        {
            make_json_error('更新數量失敗');
        }
    }
}
elseif ($_REQUEST['act'] == 'edit_flashdeal_config')
{
    $smarty->assign('config', $fleashdealController->get_flashdeal_config(true));
    assign_query_info();
    $smarty->display('flashdeal_config.htm');
}
elseif ($_REQUEST['act'] == 'update_json')
{
    $fleashdealController->update_date_json();
    $fleashdealController->read_date_json();
}
elseif ($_REQUEST['act'] == 'update_config')
{
    $fleashdealController->update_flashdeal_config($_REQUEST);
    $link[0]['text'] = '返回閃購活動列表';
    $link[0]['href'] = 'flashdeal_event.php?act=list';

    sys_msg( '編輯閃購設置成功', 0, $link, true);
} elseif ($_REQUEST['act'] == 'mass_update_flashdeal_goods') {
    $fleashdealController->mass_update_flashdeal_goods($_REQUEST['fs_id'], $_REQUEST['fe_id']);
    $fleashdealController->update_date_json();
}

function get_flashdeal_info($flashdeal_id)
{
    $flashdeal_id = intval($flashdeal_id);

    if ($flashdeal_id <= 0)
    {
        return false;
    }

    $sql = "SELECT fd.*, g.`goods_name`, g.`goods_sn`, g.`goods_thumb`, g.`market_price`, g.`shop_price` " .
            "FROM " . $GLOBALS['ecs']->table('flashdeals') . " as fd " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON fd.goods_id = g.goods_id " .
            "WHERE `flashdeal_id` = '" . $flashdeal_id . "'";

    $flashdeal = $GLOBALS['db']->getRow($sql);

    if (!$flashdeal)
    {
        return false;
    }

    $flashdeal['market_price_formatted'] = price_format($flashdeal['market_price'], false);
    $flashdeal['shop_price_formatted'] = price_format($flashdeal['shop_price'], false);
    $flashdeal['deal_price_formatted'] = price_format($flashdeal['deal_price'], false);
    $flashdeal['deal_date_formatted'] = local_date('Y-m-d H:i', $flashdeal['deal_date']);

    return $flashdeal;
}

function get_flashdeal_notifys()
{
    $filter['flashdeal_id'] = empty($_REQUEST['flashdeal_id']) ? 0 : intval($_REQUEST['flashdeal_id']);

    $filter['sort_by']      = empty($_REQUEST['sort_by']) ? 'notify_id' : trim($_REQUEST['sort_by']);
    $filter['sort_order']   = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);

    $sql = "SELECT count(*) " .
            "FROM " . $GLOBALS['ecs']->table('flashdeal_notify') .
            (empty($filter['flashdeal_id']) ? '' : "WHERE flashdeal_id = '" . $filter['flashdeal_id'] . "' ");
    $filter['record_count'] = $GLOBALS['db']->getOne($sql);

    /* 分页大小 */
    $filter = page_and_size($filter);

    /* 查询 */
    $sql = "SELECT fn.*, u.`user_name`, rei.`content` as display_name " .
            "FROM " . $GLOBALS['ecs']->table('flashdeal_notify') . " as fn " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('users') . " as u ON u.user_id = fn.user_id " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('reg_extend_info') . " as rei ON rei.user_id = fn.user_id AND rei.reg_field_id = 10 " .
            "WHERE 1 " . (empty($filter['flashdeal_id']) ? '' : "AND fn.flashdeal_id = '" . $filter['flashdeal_id'] . "' ") .
            "ORDER BY " . $filter['sort_by'] . ' ' . $filter['sort_order'] . ' ' .
            "LIMIT " . $filter['start'] . "," . $filter['page_size'];
    $data = $GLOBALS['db']->getAll($sql);

    foreach ($data as $key => $row)
    {
        $data[$key]['display_name'] = empty($row['display_name']) ? $row['user_name'] : $row['display_name'];
        $data[$key]['enter_date'] = local_date($GLOBALS['_CFG']['time_format'], $row['enter_time']);
    }

    $arr = array('data' => $data, 'filter' => $filter, 'page_count' => $filter['page_count'], 'record_count' => $filter['record_count']);

    return $arr;
}

?>