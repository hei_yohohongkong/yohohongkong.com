<?php

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_accounting.php');
require_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/ERP/lib_erp_order.php');

$controller = new Yoho\cms\Controller\YohoBaseController('actg_credit_note_transactions');

if ($_REQUEST['act'] == 'summary' || $_REQUEST['act'] == 'list_credit')
{
    admin_priv('sale_order_stats');
    if($_REQUEST['act'] == 'summary')
    {
        $data = get_cn_supplier();
        $smarty->assign('suppliers', $data['suppliers']);
        $smarty->assign('ur_here',  $_LANG['erp_credit_summary']);
        $smarty->assign('act', 'summary');
    }
    else
    {
        $data = list_credit_note();
        $smarty->assign('credits', $data['credits']);
        $smarty->assign('action_link2',  array('text' => $_LANG['erp_credit_summary'],'href'=>'credit_note_report.php?act=summary'));
        $smarty->assign('ur_here',  $_LANG['erp_credit_list']);
        $smarty->assign('act', 'list_credit');
    }
    $smarty->assign('full_page', 1);
    $smarty->assign('filter',          $data['filter']);
    $smarty->assign('record_count',    $data['record_count']);
    $smarty->assign('page_count',      $data['page_count']);

    $sort_flag  = sort_flag($data['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);

    $smarty->assign('action_link',  array('text' => $_LANG['erp_credit_transaction'],'href'=>'credit_note_report.php?act=list'));
    assign_query_info();
    $smarty->display('credit_note_report_summary.htm');
}
elseif ($_REQUEST['act'] == 'query_summary' || $_REQUEST['act'] == 'query_list_credit')
{
    check_authz_json('sale_order_stats');
    
    if($_REQUEST['act'] == 'query_summary')
    {
        $data = get_cn_supplier();
        $controller->ajaxQueryAction($data['suppliers'], $data['record_count'], false);
    }
    else
    {
        $data = list_credit_note();
        $controller->ajaxQueryAction($data['credits'], $data['record_count'], false);
    }
}
elseif ($_REQUEST['act'] == 'query' ||  $_REQUEST['act'] == 'download')
{
    check_authz_json('sale_order_stats');
    if ($_REQUEST['act'] == 'download')
    {
        $file_name = $_REQUEST['start_date'].'_'.$_REQUEST['end_date'] . '_credit_note_report';
        $data = get_cn_transactions(false);
        header("Content-type: application/vnd.ms-excel; charset=utf-8");
        header("Content-Disposition: attachment; filename=$file_name.xls");

        require_once ROOT_PATH . 'includes/excel/PHPExcel.php';
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        
        $objPHPExcel->getActiveSheet()->setTitle('Credit Note交易紀錄');
        
        /* 文件标题 */
        $objPHPExcel->getActiveSheet()
        ->setCellValue('A1', ecs_iconv(EC_CHARSET, 'UTF8', $_REQUEST['start_date'] . $_LANG['to'] . $_REQUEST['end_date'] . ' Credit Note報告'))
        ->setCellValue('A2', ecs_iconv(EC_CHARSET, 'UTF8', '交易編號'))
        ->setCellValue('B2', ecs_iconv(EC_CHARSET, 'UTF8', '供應商'))
        ->setCellValue('C2', ecs_iconv(EC_CHARSET, 'UTF8', 'Credit Note'))
        ->setCellValue('D2', ecs_iconv(EC_CHARSET, 'UTF8', '建立時間'))
        ->setCellValue('E2', ecs_iconv(EC_CHARSET, 'UTF8', '備註'))
        ->setCellValue('F2', ecs_iconv(EC_CHARSET, 'UTF8', '金額'));
        
        $objPHPExcel->getActiveSheet()->mergeCells('A1:F1');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $i = 3;
        foreach ($data['transactions'] AS $key => $value)
        {
            $objPHPExcel->getActiveSheet()
            ->setCellValue('A'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['cn_txn_id']))
            ->setCellValue('B'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['name']))
            ->setCellValue('C'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['cn_sn']))
            ->setCellValue('D'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['create_date_formatted']))
            ->setCellValue('E'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['remark']))
            ->setCellValue('F'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['amount']));
            $objPHPExcel->getActiveSheet()->getStyle('F'.$i)->getNumberFormat()->setFormatCode('$0.00');
            $i++;
        }
        
        for ($col = 'A'; $col <= 'J'; $col++)
        {
            $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
        }
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        
        exit;
    }
    $suppliers = get_cn_supplier(false);
    $data = get_cn_transactions();

    $controller->ajaxQueryAction($data['transactions'], $data['record_count'], false);
}
elseif ($_REQUEST['act'] == 'paid_credit')
{
    if($_REQUEST['ajax'])
    {
        check_authz_json('sale_order_stats');
        $sql = "SELECT cn_id, cn_sn, curr_amount FROM " . $ecs->table('erp_credit_note') . " WHERE curr_amount < 0 AND supplier_id = " . $_REQUEST['supplier_id'] . " ORDER BY curr_amount ASC";
        make_json_result($db->getAll($sql));
    }
    else
    {
        admin_priv('sale_order_stats');
        $smarty->assign('accounts', $actg->getAccounts());
        $smarty->assign('payment_types', $actg->getPaymentTypes());

        $sql = "SELECT DISTINCT es.name,es.supplier_id FROM " . $ecs->table('erp_supplier') . " es LEFT JOIN " . $ecs->table('erp_credit_note') . " ecn ON ecn.supplier_id = es.supplier_id WHERE ecn.curr_amount < 0";
        $suppliers = $db->getAll($sql);
        $smarty->assign('supplier_list', $suppliers);

        $sql = "SELECT user_name as sales_name FROM ".$ecs->table('admin_user')." WHERE user_id=".$_SESSION['admin_id'];
        $salesperson = $db->getCol($sql);
        $smarty->assign('salesperson_list', $salesperson);

        $smarty->assign('ur_here', $_LANG['erp_paid_credit']);
        $smarty->assign('action_link',  array('text' => $_LANG['erp_credit_transaction'],'href'=>'credit_note_report.php?act=list'));
        assign_query_info();
        $smarty->display('credit_note_payment.htm');
    }
}
elseif ($_REQUEST['act'] == 'insert_credit')
{
    admin_priv('sale_order_stats');
    $cn_ids = empty($_POST['cn_id']) ? [] : explode(",", $_POST['cn_id']);
    if(empty($cn_ids))
    {
        sys_msg('必須選擇要支付的Credit Note');
    }

    $credits = get_credit_note($cn_ids, ""," ORDER BY ecn.curr_amount DESC");
    if(count($credits) != count($cn_ids))
    {
        sys_msg('選擇了無效的Credit Note');
    }

    $operator = empty($_POST['operator']) ? '' : trim($_POST['operator']);
    if (empty($operator))
    {
        sys_msg('必須輸入操作員');
    }

    $account_id = empty($_POST['account_id']) ? 0 : intval($_POST['account_id']);
    if (empty($account_id))
    {
        sys_msg('必須輸入帳戶');
    }
    $acc = $actg->getAccount(compact('account_id'));
    if (empty($acc))
    {
        sys_msg('帳戶不正確');
    }

    $currency_code = $acc['currency_code'];

    $completed = isset($_POST['completed']) ? (!empty($_POST['completed'])) : true;

    $amount = empty($_POST['amount']) ? 0.0 : doubleval($_POST['amount']);
    if (empty($amount))
    {
        sys_msg('必須輸入金額');
    }
    if (($completed) && (!$acc['allow_debit']) && (bccomp(bcadd(bcmul($amount, -1), $acc['account_balance']), 0) < 0))
    {
        sys_msg('金額超出帳戶結餘');
    }

    $payment_type_id = empty($_POST['payment_type_id']) ? 0 : intval($_POST['payment_type_id']);
    if (empty($payment_type_id))
    {
        sys_msg('必須選擇付款方式');
    }
    $pay_type = $actg->getPaymentType(compact('payment_type_id'));
    if (empty($pay_type))
    {
        sys_msg('付款方式不正確');
    }
    
    $bank_date = empty($_POST['bank_date']) ? 0 : $actg->dbdate(local_strtotime(trim($_POST['bank_date'])));

    $remark = empty($_POST['remark']) ? '' : trim($_POST['remark']);

    $datetime = $actg->dbdate();

    $expense_year = empty($_POST['expense_year']) ? null : intval($_POST['expense_year']);

    $expense_month = empty($_POST['expense_month']) ? null : intval($_POST['expense_month']);

    $remain_amount = $amount;

    $transaction_started = false;
    if (!$db->in_transaction)
    {
        $actg->start_transaction();
        $transaction_started = true;
    }

    foreach($credits as $cn)
    {
        if(floatval($cn['curr_amount']) < 0)
        {
            $paid_credit = $remain_amount > -$cn['curr_amount'] ? -$cn['curr_amount'] : $remain_amount;

            $txn_remark = "Credit Note " . $cn['cn_sn'] . " 付款" . 
                (empty($remark) ? '' : ' ' . $remark);
            $txn = array(
                'add_date' => $datetime,
                'bank_date' => $bank_date ? $bank_date : ($completed ? $datetime : null),
                'complete_date' => $completed ? $datetime : null,
                'currency_code' => $currency_code,
                'amount' => -$paid_credit,
                'payment_type_id' => $payment_type_id,
                'account_id' => $account_id,
			    'remark' => $txn_remark,
                'txn_type_id' => 59,
                'expense_year' => $expense_year,
                'expense_month' => $expense_month
            );
            $txn_id = $actg->addTransaction($txn);
            if(!$txn_id)
            {
                if ($transaction_started)
                {
                    $this->rollback_transaction();
                }
                sys_msg($_LANG['erp_paid_credit']."失敗",1,array(array('text'=>$_LANG['erp_credit_transaction'],'href'=>"credit_note_report.php?act=list")));
            }
            else
            {
                $param = array(
                    'cn_id' => $cn['cn_id'],
                    'curr_amount' => $cn['curr_amount'] + $paid_credit,
                    'txn_info' => array(
                        'amount' => $paid_credit,
                        'txn_id' => $txn_id,
                        'remark' => "支付: " . $cn['cn_sn']
                    )
                );
                $erpController = new Yoho\cms\Controller\ErpController();
                $erpController->update_credit_note($param);
            }
            $remain_amount -= $paid_credit;
        }
    }
    sys_msg("成功".$_LANG['erp_paid_credit'],0,array(array('text'=>$_LANG['erp_credit_transaction'],'href'=>"credit_note_report.php?act=list")));
}
elseif ($_REQUEST['act'] == 'reverse_credit')
{
    check_authz_json('sale_order_stats');
    $cn_txn_id = isset($_REQUEST['cn_txn_id']) ? $_REQUEST['cn_txn_id'] : 0;
    $payment_method_id = isset($_REQUEST['payment_method_id']) ? $_REQUEST['payment_method_id'] : 0;
    if(empty($_REQUEST['cn_txn_id']))
    {
        make_json_error('必須輸入交易編號');
    }
    $sql = "SELECT acnt.*, ecn.cn_sn, app.purchase_payment_id FROM `actg_credit_note_transactions` acnt " .
            "LEFT JOIN " . $ecs->table('erp_credit_note') . " ecn ON acnt.cn_id = ecn.cn_id " .
            "LEFT JOIN `actg_purchase_payments` app ON app.txn_id = acnt.txn_id AND app.actg_txn_id IS NULL " .
            "LEFT JOIN `actg_purchase_payments` app ON app.actg_txn_id = acnt.txn_id AND app.txn_id IS NULL " .
            "WHERE cn_txn_id = $cn_txn_id";
    $credit = $db->getRow($sql);
    if($payment_method_id)
    {
        $payment = $db->getRow("SELECT account_id, payment_type_id FROM `actg_account_payment_methods` WHERE payment_method_id = $payment_method_id");
        $account_id = $payment['account_id'];
        $payment_type_id = $payment['payment_type_id'];
    }
    if(strpos($credit['remark'], "支付") !== false)
    {
        $actg->start_transaction();
        if($actg->deleteTransaction($credit['txn_id'],$account_id,$payment_type_id))
        {
            $actg->commit_transaction();
            $actg->deletePurchasePayment($credit['purchase_payment_id']);
            make_json_response('刪除成功');
        }
        else
        {
            $actg->rollback_transaction();
            make_json_error('刪除失敗');
        }
    }
    make_json_error('無法刪除該交易');
}
elseif ($_REQUEST['act'] == 'reverse_payment')
{
    $cn_txn_id = isset($_REQUEST['cn_txn_id']) ? $_REQUEST['cn_txn_id'] : 0;
    if($cn_txn_id)
    {
        $sql = "SELECT (act.amount * -1) as amount, payment_method_id FROM `actg_credit_note_transactions` acnt " .
                "LEFT JOIN `actg_account_transactions` act ON act.txn_id = acnt.txn_id " .
                "LEFT JOIN `actg_account_payment_methods` acm ON acm.account_id = act.account_id AND acm.payment_type_id = acm.payment_type_id " .
                "WHERE cn_txn_id = $cn_txn_id";
        $txn = $db->getRow($sql);
        $arr['default_payment_method_id'] = $txn['payment_method_id'];
        $arr['refund_amount_formatted'] = price_format($txn['amount']);
        $arr['payment_methods'] = $actg->getAccountPaymentMethods("`payment_method_id` = '$txn[payment_method_id]' OR (`pay_code` NOT LIKE '%ebanking%' AND `is_hidden` = 0 AND `active` = 1)", array('orderby' => 'payment_type_id'));
        make_json_result($arr);
    }
    else
    {
        make_json_error("無效的交易紀錄");
    }
    make_json_error("錯誤");
}
else
{
    admin_priv('sale_order_stats');
    
    $suppliers = get_cn_supplier();
    $data = get_cn_transactions();

    $smarty->assign('filter',       $data['filter']);
    $smarty->assign('record_count', $data['record_count']);
    $smarty->assign('page_count',   $data['page_count']);
    $smarty->assign('transactions', $data['transactions']);
    $smarty->assign('suppliers',    $suppliers['suppliers']);
    $smarty->assign('ur_here',      'Credit Note 交易紀錄');
    $smarty->assign('full_page',    1);
    $smarty->assign('start_date',   $data['start_date']);
    $smarty->assign('end_date',     $data['end_date']);
    $smarty->assign('cfg_lang',     $_CFG['lang']);
    $smarty->assign('action_link',  array('text' => '下載報表','href'=>'#download'));
    $smarty->assign('action_link2',  array('text' => $_LANG['erp_add_credit'],'href'=>'erp_order_manage.php?act=add_credit'));
    // $smarty->assign('action_link3',  array('text' => $_LANG['erp_paid_credit'],'href'=>'credit_note_report.php?act=paid_credit'));
    
    $sort_flag  = sort_flag($data['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);

    assign_query_info();
    $smarty->display('credit_note_report.htm');
}
function get_cn_supplier($sum = true, $where = ""){
    global $actg, $controller;

    $filter['sort_by']      = empty($_REQUEST['sort_by']) ? 'supplier_id' : trim($_REQUEST['sort_by']);
    $filter['sort_order']   = empty($_REQUEST['sort_order']) ? 'ASC' : trim($_REQUEST['sort_order']);
    $sort_by = $filter['sort_by'] . ' ' . $filter['sort_order'];
    $sort_by = !empty(trim($sort_by)) ? " ORDER BY " . $sort_by : "";

    $filter['record_count'] = 0;
    $filter['page'] = 1;
    $filter['page_count'] = 1;
    $filter['start'] = 0;

    $sql = "SELECT name AS supplier_name, supplier_id, (SELECT SUM(curr_amount) FROM " . $GLOBALS['ecs']->table('erp_credit_note') . " WHERE supplier_id = e.supplier_id) AS supplier_balance FROM " . $GLOBALS['ecs']->table('erp_supplier') . " e HAVING supplier_balance != 0" . ($sum ? $sort_by : "");
    $suppliers = $GLOBALS['db']->getAll($sql);
    foreach($suppliers as $key => $supplier)
    {
        $supplier_balance = $supplier['supplier_balance'];
        $suppliers[$key]['supplier_balance_formatted'] = $actg->money_format($supplier_balance);
        $filter['record_count']++;
        $view_btn = [
            'link'  => "credit_note_report.php?act=list&supplier_id=$supplier[supplier_id]",
            'title' => '查看交易紀錄',
            'label' => '查看',
            'btn_class' => 'btn btn-primary'
        ];
        $custom_actions = ['view' => $view_btn];
        $suppliers[$key]['_action'] = $controller->generateCrudActionBtn($supplier['supplier_id'], 'id', null, [], $custom_actions);
    }
    $filter['page_size'] = $filter['record_count'];

    return array(
        'suppliers' => $suppliers,
        'filter' => $filter,
        'page_count' => $filter['page_count'],
        'record_count' => $filter['record_count']
    );
}

function get_cn_transactions($is_pagination = true)
{
    global $actg, $db, $controller;

    $filter = page_and_size($filter);
    $filter['sort_by']      = empty($_REQUEST['sort_by']) ? 'acnt.create_at' : trim($_REQUEST['sort_by']);
    $filter['sort_order']   = empty($_REQUEST['sort_order']) ? 'ASC' : trim($_REQUEST['sort_order']);

    if (in_array($filter['sort_by'], ["create_at", "cn_id"])) {
        $filter['sort_by'] = "acnt.create_at";
    }
    if (in_array($filter['sort_by'], ["supplier_id"])) {
        $filter['sort_by'] = "es.supplier_id";
    }

    $filter['start_date'] = (empty($_REQUEST['start_date'])) ? month_start() : $_REQUEST['start_date'];
    $filter['end_date'] = (empty($_REQUEST['end_date'])) ? month_end() : $_REQUEST['end_date'];
    $start_date = $filter['start_date'];
    $end_date = local_date('Y-m-d H:i:s',local_strtotime($filter['end_date']) + 86399);

    $filter['supplier_id'] = empty($_REQUEST['supplier_id']) ? 0 : intval($_REQUEST['supplier_id']);
    $filter['txn_type'] = empty($_REQUEST['txn_type']) ? 0 : intval($_REQUEST['txn_type']);
    $filter['txn_remark'] = empty($_REQUEST['txn_remark']) ? '' : trim($_REQUEST['txn_remark']);
    $filter['show_reversetxn'] = isset($_REQUEST['show_reversetxn']) ? intval($_REQUEST['show_reversetxn']) : 0;

    $filter2['cn_sn'] = empty($_REQUEST['cn_sn']) ? "" : trim($_REQUEST['cn_sn']);
    $filter2['cn_id'] = empty($_REQUEST['cn_id']) ? "" : intval($_REQUEST['cn_id']);

    $opt['orderby'] = $filter['sort_by'] . ' ' . $filter['sort_order'];
    if ($is_pagination)
    {
        $opt['limit'] = $filter['start'] . ',' . $filter['page_size'];
    }

    $where = ($filter['txn_type'] == 1 ? "acnt.amount > 0 " : ($filter['txn_type'] == 2 ? "acnt.amount < 0 " : "1 ")) .
             "AND acnt.create_at BETWEEN '$start_date' AND '$end_date' " .
             (!empty($filter['supplier_id']) ? "AND ecn.supplier_id = $filter[supplier_id] " : "") .
             (!empty($filter['txn_remark']) ? "AND acnt.remark LIKE '%$filter[txn_remark]%' " : "") .
             (!empty($filter2['cn_sn']) ? "AND ecn.cn_sn = '$filter2[cn_sn]' " : "") .
             (!empty($filter2['cn_id']) ? "AND ecn.cn_id = $filter2[cn_id] " : "");
    $transactions = $actg->getCreditNoteTransaction($where,$opt);
    $sql = "SELECT COUNT(*) FROM `actg_credit_note_transactions` acnt LEFT JOIN `ecs_erp_credit_note` ecn ON ecn.cn_id = acnt.cn_id WHERE " . (empty($where) ? "1" : $where);
    $filter['record_count'] = $db->getOne($sql);

    if((!empty($filter2['cn_id']) || !empty($filter2['cn_sn'])) && !$filter['show_reversetxn']){
        $filter['rev'] = 1;
    }
    if($filter['show_reversetxn'] != 1)
    {
        $sql = "SELECT related_cn_txn_id FROM `actg_credit_note_transactions` WHERE remark LIKE 'Reverse transaction%'";
        $reverse = $db->getCol($sql);
    }

    foreach($transactions as $key => $txn)
    {
        if(strpos($txn['remark'], '新增') !== false || ($filter['show_reversetxn'] != 1 && (in_array($txn['cn_txn_id'],$reverse) || strpos($txn['remark'], 'Reverse transaction') !== false)))
        {
            unset($transactions[$key]);
            $filter['record_count']--;
        }
        else
        {
            $transactions[$key]['amount_formatted'] = $actg->money_format($txn['amount'],'HK$%s');
            $transactions[$key]['create_date_formatted'] = date($GLOBALS['_CFG']['time_format'], strtotime($txn['create_at']));
            if(!empty($filter2['cn_id']) || !empty($filter2['cn_sn']))
            {
                if(strpos($txn['remark'], '支付') !== false)
                {
                    $transactions[$key]['reversible'] = 1;
                }
            }
        }
        if ($filter['rev']) {
            if (!empty($transactions[$key])) {
                $delete_btn = [
                    'link'  => "javascript:deleteCreditNotePayment($txn[cn_txn_id])",
                    'title' => '移除交易',
                    'label' => '移除',
                    'btn_class' => 'btn btn-danger' . ($transactions[$key]['reversible'] ? '' : ' disabled')
                ];
                $custom_actions = ['delete' => $delete_btn];
                $transactions[$key]['_action'] = $controller->generateCrudActionBtn($txn['cn_txn_id'], 'id', null, [], $custom_actions);
            }
        }
    }

    return array(
        'transactions' => array_values($transactions),
        'filter' => $filter,
        'start_date' => $filter['start_date'],
        'end_date' => $filter['end_date'],
        'record_count' => $filter['record_count'],
        'page_count' => ceil($filter['record_count'] / $filter['page_size'])
    );
}
function list_credit_note()
{
    global $actg, $db, $ecs;

    $filter['sort_by']      = empty($_REQUEST['sort_by']) ? 'cn_id' : trim($_REQUEST['sort_by']);
    $filter['sort_order']   = empty($_REQUEST['sort_order']) ? 'ASC' : trim($_REQUEST['sort_order']);
    $filter['supplier_id']  = empty($_REQUEST['supplier_id']) ? '' : trim($_REQUEST['supplier_id']);
    $filter['non_zero']     = empty($_REQUEST['non_zero']) ? 0 : intval($_REQUEST['non_zero']);
    if (in_array($filter['sort_by'], ["supplier_id"])) {
        $filter['sort_by'] = "es.supplier_id";
    }
    $sort_by = $filter['sort_by'] . ' ' . $filter['sort_order'];
    $sort_by = !empty(trim($sort_by)) ? " ORDER BY " . $sort_by : "";
    $where = " WHERE 1 " .
             (empty($filter['supplier_id']) ? "" : "AND ecn.supplier_id = $filter[supplier_id] ");
    $sql = "SELECT * FROM " . $ecs->table('erp_credit_note') . " ecn " .
            "LEFT JOIN " . $ecs->table('erp_supplier') . " es ON es.supplier_id = ecn.supplier_id " .
            $where . $sort_by;
    $credits = $db->getAll($sql);
    $filter['record_count'] = count($credits);
    $filter = page_and_size($filter);
    foreach($credits as $key => $credit)
    {
        $credits[$key]['curr_amount_balance'] = $actg->money_format($credit['curr_amount'],'HK$%s');
        $credits[$key]['remark'] = $db->getOne("SELECT remark FROM `actg_credit_note_transactions` WHERE cn_id = $credit[cn_id] ORDER BY cn_txn_id ASC LIMIT 1");
    }
    return array(
        'credits' => array_values($credits),
        'filter' => $filter,
        'record_count' => $filter['record_count'],
        'page_count' => $filter['page_count'],
    );
}
function month_start()
{
    return local_date('Y-m-01');
}

function month_end()
{
    return local_date('Y-m-t');
}
?>