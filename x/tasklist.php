<?php

/***
* tasklist.php
* by howang 2014-07-07
*
* Task list for YOHO Hong Kong admin page
***/
namespace Yoho\cms;
define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require_once(dirname(dirname(__FILE__)) . '/includes/lib_insert.php');


/* act操作项的初始化 */
$_REQUEST['act'] = trim($_REQUEST['act']);
if (empty($_REQUEST['act']))
{
    $_REQUEST['act'] = 'list';
}

// Define defaults
$ctrlTask = new Controller\TaskController();
$adminTaskTemplatesController = new Controller\AdminTaskTemplatesController();
$adminuserController = new Controller\AdminuserController();
$status_mappings = $ctrlTask->getStatusList();
$priority_mappings = $ctrlTask->getPriorityMappings();
$estimated_time_mappings = $ctrlTask->getEstimatedTimeMappings();

// $priority_mappings = array(
//     'Low' => '低',
//     'Normal' => '普通',
//     'High' => '高',
//     'Urgent' => '緊急',
//     'Immediate' => '即時'
// );

// $estimated_time_mappings = array(
//     10 => '10 mins',
//     20 => '20 mins',
//     30 => '30 mins',
//     40 => '40 mins',
//     50 => '50 mins',
//     60 => '1 hour',
//     90 => '1.5 hours',
//     120 => '2 hours',
//     180 => '3 hours',
//     240 => '4 hours',
//     300 => '5 hours',
//     360 => '6 hours',
//     420 => '7 hours',
//     480 => '8 hours'
// );

/*------------------------------------------------------ */
//-- 留言列表页面
/*------------------------------------------------------ */
if ($_REQUEST['act'] == 'list')
{
    $smarty->assign('ur_here',     '工作清單');


    $authzed = check_authz($ctrlTask::ACTION_CODE_ADVANCED_STATUS);
   
    // get the most frequently used users
    $contacts = $ctrlTask->getTheMostFrequentlyUsedUsers(5);
    // echo '<pre>';
    // print_r($contacts);
    // exit;
    $smarty->assign('contacts',     $contacts);
    $smarty->assign('authzed',      $authzed);
    
    $sql = "SELECT `sales_name` as `name`, `sales_name` as `value` " .
            "FROM " . $ecs->table('salesperson') .
            "WHERE `available` = 1 " .
            "AND `sales_name` != 'admin' " .
            // Only super admin can see task from all people, others can only see task assigned to themselves
            ($_SESSION['manage_cost'] ? '' : "AND `admin_id` = '" . $_SESSION['admin_id'] . "'");
    $salespeople = $adminuserController->getAdminsByPriv([], 'name', ['user_name' => 'value']);
    array_unshift($salespeople, array('name'=>'請選擇...','value'=>''));
    $smarty->assign('salespeople', $salespeople);
    $smarty->assign('statuslist', $ctrlTask->statusKey);

    $status_opts = array();
    foreach ($status_mappings as $val => $str)
    {
        $status_opts[] = array('name' => $str, 'value' => $val);
    }
    $smarty->assign('status_opts', $status_opts);
    
    $priority_opts = array();
    foreach ($priority_mappings as $val => $str)
    {
        if (in_array($val, array('Low','Normal','High')))
        {
            $priority_opts[] = array('name' => $str, 'value' => $val);
        }
    }
    $smarty->assign('priority_opts', $priority_opts);
    
    $time_opts = array();
    foreach ($estimated_time_mappings as $val => $str)
    {
        $time_opts[] = array('name' => $str, 'value' => $val);
    }
    $smarty->assign('time_opts', $time_opts);
    $list = get_task_list();
    
    $smarty->assign('msg_type', $list['msg_type']);
    $smarty->assign('task_list', $list['item']);
    $smarty->assign('record_count', $list['record_count']);
    $smarty->assign('action_link', array('text' => $_LANG['send_msg'], 'href' => 'tasklist.php?act=send'));
    $smarty->assign('action_link2', array('text' => '工作模版', 'href' => 'admin_task_templates.php?act=list'));
    $ctrlTask->clearMyUnread();
    assign_query_info();
    $smarty->display('task_list.htm');
}

/*------------------------------------------------------ */
//-- 翻页、排序
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'query')
{
    $list = get_task_list();
    $authzed = check_authz($ctrlTask::ACTION_CODE_ADVANCED_STATUS);
    $ctrlTask->ajaxQueryAction($list['item'], $list['record_count']);

}

/*------------------------------------------------------ */
//-- 留言发送页面
/*------------------------------------------------------ */
elseif (($_REQUEST['act'] == 'send') || ($_REQUEST['act'] == 'edit'))
{
    if ($_REQUEST['act'] == 'edit')
    {
        $task_id = empty($_REQUEST['id']) ? 0 : intval($_REQUEST['id']);
        
        $task = $db->getRow("SELECT * FROM " . $ecs->table('admin_taskes') . " WHERE `task_id` = '".$task_id."'");
        
      // get all templates

      $task_templates = $adminTaskTemplatesController->getTemplateList();
      $smarty->assign('task_templates', $task_templates);
     
      // get the most frequently used templates
      $task_templates_used = $ctrlTask->getTheMostFrequentlyUsedTemplatesByUser($_SESSION['admin_id']);
      $smarty->assign('task_templates_used', $task_templates_used);
      

        if (!$task)
        {
            sys_msg('Task not found');
        }
        elseif ($task['sender_id'] != $_SESSION['admin_id'] && !$_SESSION['manage_cost'])
        {
            sys_msg('您沒有權限修改這條紀錄!');
        }
        
        $task['dead_date'] = local_date('Y-m-d', $task['dead_time']);
    }
    else
    {
        $pic_user_id = empty($_REQUEST['pic_user_id']) ? 0 : intval($_REQUEST['pic_user_id']);
        
        // get all templates
        $task_templates = $adminTaskTemplatesController->getTemplateList();
        $smarty->assign('task_templates', $task_templates);
       
        // get the most frequently used templates
        $task_templates_used = $ctrlTask->getTheMostFrequentlyUsedTemplatesByUser($_SESSION['admin_id']);
        $smarty->assign('task_templates_used', $task_templates_used);
        
        $task = array(
            'pic_user_id' => $pic_user_id,
            'priority' => 'Normal',
            'dead_date' => local_date('Y-m-d', local_strtotime('tomorrow'))
        );
    }
    
    $sql = "SELECT `sales_name` ".
            "FROM " . $ecs->table('salesperson') .
            "WHERE `admin_id` = '" . $_SESSION['admin_id'] . "'";
    $same_account_sales = $db->getCol($sql);
    $smarty->assign('same_account_sales', $same_account_sales);
    
    /*$sql = "SELECT `sales_name` ".
            "FROM " . $ecs->table('salesperson') .
            "WHERE `available` = 1 " .
            // 2016-01-27 Allow staffs to assign task freely
            "AND `sales_name` != 'admin'";
            // // Only super admin can send task to all people, others can only send to themselves
            // ($_SESSION['manage_cost'] ? '' : "AND `admin_id` = '" . $_SESSION['admin_id'] . "'");*/
    $salespeople = $adminuserController->getAdminsByPriv([], "user_name", ['user_id' => 'user_id']);

    $smarty->assign('salespeople', $salespeople);
    
    $priority_options = array();
    foreach ($priority_mappings as $key => $val)
    {
        if (in_array($key, array('Low','Normal','High')))
        {
            $priority_options[$key] = $val;
        }
    }
    $smarty->assign('priority_options',  $priority_options);
    $smarty->assign('estimated_time_options', $estimated_time_mappings);
    $smarty->assign('cfg_lang', $_CFG['lang']);
    
    $smarty->assign('ur_here',     ($_REQUEST['act'] == 'send') ? $_LANG['send_msg'] : $_LANG['edit_msg']);
    $smarty->assign('action_link', array('href' => 'tasklist.php?act=list', 'text' => $_LANG['msg_list']));
    $smarty->assign('action_link2', array('href' => 'admin_task_templates.php?act=list', 'text' => '工作模版'));   
    $smarty->assign('form_act',    ($_REQUEST['act'] == 'send') ? 'insert' : 'update');
    
    $smarty->assign('task',        $task);
    
    assign_query_info();
    $smarty->display('task_info.htm');
}

/*------------------------------------------------------ */
//-- 处理留言的发送
/*------------------------------------------------------ */
elseif (($_REQUEST['act'] == 'insert') || ($_REQUEST['act'] == 'update'))
{
    $pic = empty($_POST['pic']) ? '' : trim($_POST['pic']);
    
    if (empty($pic))
    {
        sys_msg('必須選擇負責人');
    }
    
    // Get receiver_id base on pic
    //$sql = "SELECT admin_id FROM " . $ecs->table('salesperson') . " WHERE `sales_name` = '" . $pic . "' AND `available` = 1";
    //$receiver_id = $db->getOne($sql);
    $receiver_id = intval($_POST['pic']);
    if (!$receiver_id)
    {
        $pic = 'admin';
        $receiver_id = 1;
        sys_msg('必須選擇負責人');
    }
    $adminUser = new Model\Adminuser($receiver_id);
    if($adminUser->getId()<=0)
        sys_msg('Error while getting admin user');
    else
        $pic=$adminUser->data['user_name'];
    
    $priority = empty($_POST['priority']) ? '' : trim($_POST['priority']);
    $priority = (array_key_exists($priority, $priority_mappings)) ? $priority : 'Normal';
    
    $estimated_time = empty($_POST['estimated_time']) ? 0 : intval($_POST['estimated_time']);
    $estimated_time = (array_key_exists($estimated_time, $estimated_time_mappings)) ? $estimated_time : 0;
    
    $dead_time = empty($_POST['dead_date']) ? 0 : local_strtotime(trim($_POST['dead_date']));
    
    $title = empty($_POST['title']) ? '' : $_POST['title'];
    $message = empty($_POST['message']) ? '' : $_POST['message'];
    
    if (empty($title) || empty($message))
    {
        sys_msg('必須輸入標題和內容');
    }
    
    if ($_REQUEST['act'] == 'insert')
    {
        $sender_id = $_SESSION['admin_id'];
        $sender_name = empty($_POST['sender_name']) ? $_SESSION['admin_name'] : trim($_POST['sender_name']);

        $task = array(
            'sender_id' => $sender_id,
            'receiver_id' => $receiver_id,
            'sent_time' => gmtime(),
            'read_time' => 0,
            'status' => 'New',
            'priority' => $priority,
            'dead_time' => $dead_time,
            'estimated_time' => $estimated_time,
            'sender_name' => $sender_name,
            'pic' => $pic,
            'deleted' => 0,
            'title' => $title,
            'message' => $message
        );
        
        $db->autoExecute($ecs->table('admin_taskes'), $task, 'INSERT');
        $task_id = $db->insert_id();
        
        $act_name = $_LANG['send_msg'];

        // update user template table
        $ctrlTask->updateTaskTemplateUsed($sender_id,$_REQUEST['task_template_id']);
    }
    else
    {
        $task_id = empty($_REQUEST['id']) ? 0 : intval($_REQUEST['id']);

        $sender_id = $db->getOne('SELECT `sender_id` FROM ' .$ecs->table('admin_taskes')." WHERE task_id = '$task_id'");
        if (!$sender_id)
        {
            sys_msg('Task not found');
        }
        else if ($sender_id != $_SESSION['admin_id'] && !$_SESSION['manage_cost'])
        {
            sys_msg('您沒有權限修改這條紀錄!');
        }
        
        $task = array(
            'receiver_id' => $receiver_id,
            'priority' => $priority,
            'dead_time' => $dead_time,
            'estimated_time' => $estimated_time,
            'pic' => $pic,
            'title' => $title,
            'message' => $message
        );
        
        $db->autoExecute($ecs->table('admin_taskes'), $task, 'UPDATE', " `task_id` = '" . $task_id . "'");
        
        $act_name = $_LANG['edit_msg'];

        // update user template table
        $ctrlTask->updateTaskTemplateUsed($sender_id,$_REQUEST['task_template_id']);
    }
    
    /*添加链接*/
    $link = array();
    $link[0]['text'] = $_LANG['back_list'];
    $link[0]['href'] = 'tasklist.php?act=list';
    
    if ($_REQUEST['act'] == 'insert')
    {
        $link[1]['text'] = $_LANG['continue_send_msg'];
        $link[1]['href'] = 'tasklist.php?act=send';
    }
    
    sys_msg($act_name . "&nbsp;" . $_LANG['action_succeed'], 0, $link);
}

/*------------------------------------------------------ */
//-- 留言查看页面
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'view')
{

    $task_id = intval($_REQUEST['id']);
  // echo check_authz($ctrlTask->ACTION_CODE_ADVANCED_STATUS);
  //  exit;
    /* 获得管理员留言数据 */
    $task_arr = array();
    $sql     = "SELECT a.*, b.user_name, c.user_name as receiver_user_name ".
               "FROM " .$ecs->table('admin_taskes')." AS a ".
               "LEFT JOIN " .$ecs->table('admin_user')." AS b ON b.user_id = a.sender_id ".
               "LEFT JOIN " .$ecs->table('admin_user')." AS c ON c.user_id = a.receiver_id ".
               "WHERE a.task_id = '$task_id'";
    $task_arr = $db->getRow($sql);
    $task_arr['title']   = nl2br(htmlspecialchars($task_arr['title']));
    $task_arr['message'] = nl2br($task_arr['message']);
    $task_arr['send_date'] = local_date($GLOBALS['_CFG']['time_format'], $task_arr['sent_time']);
    $task_arr['read_date'] = local_date($GLOBALS['_CFG']['time_format'], $task_arr['read_time']);

    $task_arr['log'] = $ctrlTask->getTaskLogs($task_id);
    $task_arr['statusList'] = $ctrlTask->getStatusList();

    /* 如果还未阅读 */
    if ($task_arr['read_time'] == 0 && $task_arr['receiver_id'] == $_SESSION['admin_id'])
    {
        $task_arr['read_time'] = gmtime(); //阅读日期为当前日期

        //更新阅读日期和阅读状态
        $sql = "UPDATE " .$ecs->table('admin_taskes'). " SET ".
               "read_time = '" . $task_arr['read_time'] . "' ".
               "WHERE task_id = '$task_id'";
        $db->query($sql);
    }

    /*-----check if log-----*/
    $smarty->assign('availableStatus',$ctrlTask->getStatusList(true,$task_arr['status']));
    $smarty->assign('canUpdateStatus',  $ctrlTask->canUpdateStatus($task_arr['status']));

    //模板赋值，显示
    $smarty->assign('ur_here',     $_LANG['view_msg']);
    $smarty->assign('action_link', array('href' => 'tasklist.php?act=list', 'text' => $_LANG['msg_list']));
    $smarty->assign('admin_user',  $_SESSION['admin_name']);
    $smarty->assign('task_arr',     $task_arr);

    assign_query_info();
    $ctrlTask->clearCurrentUserUnread($task_id);
    $smarty->display('task_view.htm');

}

/*------------------------------------------------------ */
//-- 删除留言
/*------------------------------------------------------ */
elseif (in_array($_REQUEST['act'], array('remove','restore','permanent_delete')))
{
    $id = intval($_GET['id']);
    
    $sender_id = $db->getOne('SELECT `sender_id` FROM ' .$ecs->table('admin_taskes')." WHERE task_id = '" . $id . "'");
    if (!$sender_id)
    {
        make_json_error('Task not found');
    }
    else if ($sender_id != $_SESSION['admin_id'] && !$_SESSION['manage_cost'] && admin_priv(Controller\TaskController::ACTION_CODE_ADVANCED_STATUS))
    {
        sys_msg('您沒有權限操作這條紀錄!');
    }
    
    if ($_REQUEST['act'] == 'remove')
    {
        $sql = "UPDATE ".$ecs->table('admin_taskes').
                " SET deleted = 1 ".
                " WHERE task_id = '".$id."' ";
        $db->query($sql);
    }
    elseif ($_REQUEST['act'] == 'restore')
    {
        $sql = "UPDATE ".$ecs->table('admin_taskes').
                " SET deleted = 0 ".
                " WHERE task_id = '".$id."' ";
        $db->query($sql);
    }
    elseif ($_REQUEST['act'] == 'permanent_delete')
    {
        $sql = "DELETE FROM " . $ecs->table('admin_taskes') .
                " WHERE task_id = '".$id."' ";
        $db->query($sql);
    }

    $url = 'tasklist.php?act=list&' . urldecode(str_replace('act=' . $_REQUEST['act'], '', $_SERVER['QUERY_STRING']));

    // header('Location: ' . $url);
    ecs_header("Location: $url\n");
    exit;
}

/*------------------------------------------------------ */
//-- 修改狀態
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'edit_status')
{
    //check_authz_json('all');

    $task_id = intval($_POST['id']);
    $status = json_str_iconv(trim($_POST['val']));
    
    $status_org = $db->getOne("SELECT status FROM " .$ecs->table('admin_taskes'). " WHERE task_id = '$task_id'");
    
    if ($status != $status_org)
    {
        $sql = "UPDATE " .$ecs->table('admin_taskes'). " SET ".
               "status = '".$status."' ".
               "WHERE task_id = '$task_id'";
        $db->query($sql);
        
        $status = $db->getOne("SELECT status FROM " .$ecs->table('admin_taskes'). " WHERE task_id = '$task_id'");
        
        if ($status == 'Finished')
        {
            $sql = "UPDATE " .$ecs->table('admin_taskes'). " SET ".
                   "finish_time = '".gmtime()."' ".
                   "WHERE task_id = '$task_id'";
            $db->query($sql);
        }
    }
    
    make_json_result($status);
}

/*------------------------------------------------------ */
//-- 修改優先次序
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'edit_priority')
{
    //check_authz_json('all');
    
    $task_id = intval($_POST['id']);
    $priority = json_str_iconv(trim($_POST['val']));
    
    $sql = "UPDATE " .$ecs->table('admin_taskes'). " SET ".
           "priority = '".$priority."' ".
           "WHERE task_id = '$task_id'";
    $db->query($sql);
    
    $priority = $db->getOne("SELECT priority FROM " .$ecs->table('admin_taskes'). " WHERE task_id = '$task_id'");
    
    make_json_result($priority);
}


/*------------------------------------------------------ */
//-- 修改優先次序
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'edit_estimated_time')
{
    //check_authz_json('all');

    $task_id = intval($_POST['id']);
    $estimated_time = intval($_POST['val']);

    $sql = "UPDATE " .$ecs->table('admin_taskes'). " SET ".
           "estimated_time = '".$estimated_time."' ".
           "WHERE task_id = '$task_id'";
    $db->query($sql);

    $estimated_time = $db->getOne("SELECT estimated_time FROM " .$ecs->table('admin_taskes'). " WHERE task_id = '$task_id'");

    make_json_result($estimated_time);
}

/*------------------------------------------------------ */
//-- 修改跟進人
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'edit_pic')
{
    //check_authz_json('all');

    $task_id = intval($_POST['id']);
    $pic = json_str_iconv(trim($_POST['val']));
    $adminUser=new Model\Adminuser($pic);
    $sql = "UPDATE " .$ecs->table('admin_taskes'). " SET ".
           "receiver_id = ".$pic.", pic = '".$adminUser->data['user_name']."' ".
           "WHERE task_id = '$task_id'";
    $db->query($sql);

    $pic = $db->getOne("SELECT pic FROM " .$ecs->table('admin_taskes'). " WHERE task_id = '$task_id'");

    make_json_result($pic);
}
/*--------- YOHO Addon --------*/
elseif($_REQUEST['act']=='insertlog'){
    //controller
    $res = $ctrlTask->addTaskLog($_REQUEST['id'],$_POST['content'],$_POST['status']);
    if($res===true){
        sys_msg("更新工作紀錄成功",0,[['text'=>'返回','href'=>"tasklist.php?act=list"]]);
    }else{
        sys_msg("更新工作紀錄時發生錯誤",1);
    }
}
elseif ($_REQUEST['act'] == 'get_template_info'){
    $res = $adminTaskTemplatesController->getAdminTaskTemplate($_REQUEST['template_id']);

    make_json_result($res);
}elseif($_REQUEST['act'] == 'quick_insertlog'){
    $task_id = intval($_REQUEST['id']);
    /* 获得管理员留言数据 */
    $task_arr = array();
    $sql     = "SELECT a.*, b.user_name, c.user_name as receiver_user_name ".
               "FROM " .$ecs->table('admin_taskes')." AS a ".
               "LEFT JOIN " .$ecs->table('admin_user')." AS b ON b.user_id = a.sender_id ".
               "LEFT JOIN " .$ecs->table('admin_user')." AS c ON c.user_id = a.receiver_id ".
               "WHERE a.task_id = '$task_id'";
    $task_arr = $db->getRow($sql);
    $task_arr['title']   = nl2br(htmlspecialchars($task_arr['title']));
    $task_arr['message'] = nl2br($task_arr['message']);
    $task_arr['send_date'] = local_date($GLOBALS['_CFG']['time_format'], $task_arr['sent_time']);
    $task_arr['read_date'] = local_date($GLOBALS['_CFG']['time_format'], $task_arr['read_time']);

    $task_arr['log'] = $ctrlTask->getTaskLogs($task_id);
    $task_arr['statusList'] = $ctrlTask->getStatusList();
    
    /* 如果还未阅读 */
    if ($task_arr['read_time'] == 0 && $task_arr['receiver_id'] == $_SESSION['admin_id'])
    {
        $task_arr['read_time'] = gmtime(); //阅读日期为当前日期

        //更新阅读日期和阅读状态
        $sql = "UPDATE " .$ecs->table('admin_taskes'). " SET ".
               "read_time = '" . $task_arr['read_time'] . "' ".
               "WHERE task_id = '$task_id'";
        $db->query($sql);
    }

    $ctrlTask->clearCurrentUserUnread($task_id);

    $available_status = $ctrlTask->getStatusList(true,$task_arr['status']);
    $can_update_status = $ctrlTask->canUpdateStatus($task_arr['status']);
    
    if ($can_update_status) {
      $status_id = 4;
      if (array_key_exists(5,$available_status)){
        $status_id = 5;
      }
      $res = $ctrlTask->addTaskLog($_REQUEST['id'],'',$status_id);
      make_json_result($res);
    }else{
      make_json_result(true);
    }
}


/**
 *  获取管理员留言列表
 *
 * @return void
 */
function get_task_list()
{
    global $ctrlTask;
    /* 查询条件 */
    $_REQUEST['sort_by']    = empty($_REQUEST['sort_by'])    ? 'dead_time' : trim($_REQUEST['sort_by']);
    $_REQUEST['sort_order'] = empty($_REQUEST['sort_order']) ? 'ASC'      : trim($_REQUEST['sort_order']);
    $_REQUEST['pic']        = empty($_REQUEST['pic'])        ? ''          : trim($_REQUEST['pic']);
    $_REQUEST['status']     = empty($_REQUEST['status'])     ? ''          : intval($_REQUEST['status']);
    $_REQUEST['msg_type']   = !isset($_REQUEST['msg_type'])   ? 1           : intval($_REQUEST['msg_type']);
    $_REQUEST['priority']   = empty($_REQUEST['priority'])   ? ''          : explode(',',$_REQUEST['priority']);
    $_REQUEST['page_size']  = empty($_REQUEST['page_size'])  ? 50          : intval($_REQUEST['page_size']);
    $_REQUEST['start']      = empty($_REQUEST['start'])      ? 0           : intval($_REQUEST['start']);
    $_REQUEST['start_time'] = empty($_REQUEST['start_time']) ? 0 : (strpos($_REQUEST['start_time'], '-') !== false ? local_strtotime($_REQUEST['start_time']) : intval($_REQUEST['start_time']));
    $_REQUEST['end_time']   = empty($_REQUEST['end_time'])   ? 0 : (strpos($_REQUEST['end_time'], '-') !== false ? local_strtotime($_REQUEST['end_time']) : intval($_REQUEST['end_time']));
    $_REQUEST['start_finish_time'] = empty($_REQUEST['start_finish_time']) ? 0 : (strpos($_REQUEST['start_finish_time'], '-') !== false ? local_strtotime($_REQUEST['start_finish_time']) : intval($_REQUEST['start_finish_time']));
    $_REQUEST['end_finish_time']   = empty($_REQUEST['end_finish_time'])   ? 0 : (strpos($_REQUEST['end_finish_time'], '-') !== false ? local_strtotime($_REQUEST['end_finish_time']) : intval($_REQUEST['end_finish_time']));

    // show same role
    $sameRoleUserIds = $GLOBALS['db']->getCol("SELECT user_id FROM ".$GLOBALS['ecs']->table('admin_user')." WHERE role_id<>'' AND role_id = (SELECT role_id FROM ".$GLOBALS['ecs']->table('admin_user')." WHERE user_id=".$_SESSION['admin_id'].")");
    $sameRoleQuery= (count($sameRoleUserIds)>0?" receiver_id IN (".implode(',', $sameRoleUserIds).")":"receiver_id='".$_SESSION['admin_id']."'");

    /* 查询条件 */
    switch ($_REQUEST['msg_type']){
        case 1:
            $where = " receiver_id='" .$_SESSION['admin_id']. "' ";
            break;
        case 2:
            $where = " sender_id='".$_SESSION['admin_id']."' ";
            break;
        default:
            if ($_SESSION['manage_cost'])
            {
                $where = " 1 ";
            }
            else
            {
                $where = " (receiver_id='".$_SESSION['admin_id']."' OR sender_id='" .$_SESSION['admin_id']. "' OR ".$sameRoleQuery.") ";
            }
    }
    

    // Show normal or deleted message
    $where .= " AND deleted = '" . ($_REQUEST['msg_type'] == 3 ? '1' : '0') . "' ";
    
    // Filter by person in charge
    if (!empty($_REQUEST['pic']))
    {
        $where .= " AND (receiver_id = '" . $_REQUEST['pic'] . "' OR pic = '".$_REQUEST['pic']."') ";
    }
    if ($_REQUEST['priority']!='' && count($_REQUEST['priority'])>0)
        $where .= " AND priority IN ('".implode("','",$_REQUEST['priority'])."')";
    
    // Filter by status
    if ($_REQUEST['status']!='')
    {
        $where .= " AND status = '" . $_REQUEST['status'] . "' ";
    }
    
    // Filter by sent_time
    if (!empty($_REQUEST['start_time']))
    {
        $where .= " AND sent_time >= '" . $_REQUEST['start_time'] . "' ";
    }
    if (!empty($_REQUEST['end_time']))
    {
        $where .= " AND sent_time <= '" . $_REQUEST['end_time'] . "' ";
    }
    
    // Filter by finish_time
    if (!empty($_REQUEST['start_finish_time']))
    {
        $where .= " AND finish_time >= '" . $_REQUEST['start_finish_time'] . "' ";
    }
    if (!empty($_REQUEST['end_finish_time']))
    {
        $where .= " AND finish_time <= '" . $_REQUEST['end_finish_time'] . "' ";
    }
    
    $orderby = $_REQUEST['sort_by'] . " " . $_REQUEST['sort_order'];
    // if ($_REQUEST['sort_by'] == 'status')
    // {
    //     $orderby = "CASE " . 
    //         "WHEN status = 'Finished' THEN 2 " .
    //         "WHEN status = 'In Progress' THEN 1 " .
    //         "ELSE 0 " .
    //         "END " . $_REQUEST['sort_order'];
    // }
    // else if ($_REQUEST['sort_by'] == 'priority')
    // {
    //     $orderby = "CASE " . 
    //         "WHEN status = 'Immediate' THEN 4 " .
    //         "WHEN status = 'Urgent' THEN 3 " .
    //         "WHEN status = 'High' THEN 2 " .
    //         "WHEN status = 'Normal' THEN 1 " .
    //         "WHEN status = 'Low' THEN 0 " .
    //         "ELSE 0 " .
    //         "END " . $_REQUEST['sort_order'];
    // }
    // if ($_REQUEST['sort_by'] != 'status')
    // {
    //     $orderby = "CASE " . 
    //         "WHEN status = 'Finished' THEN 1 " .
    //         "ELSE 0 " .
    //         "END ASC, "  . $orderby;
    // }

    $sql = "SELECT COUNT(*) FROM ".$GLOBALS['ecs']->table('admin_taskes')." AS a WHERE 1 AND ". $where;

    $_REQUEST['record_count'] = $GLOBALS['db']->getOne($sql);

    /* 分页大小 */
    //$_REQUEST = page_and_size($_REQUEST);

    $sql = "SELECT *, " .
                "IFNULL((SELECT user_id FROM " . $GLOBALS['ecs']->table('salesperson') . " WHERE sales_name = ecs_admin_taskes.pic), 0) as pic_user_id, ".
                "(SELECT avatar  FROM " . $GLOBALS['ecs']->table('admin_user') . " WHERE user_name = ecs_admin_taskes.pic) as avatar, ".
                "(SELECT avatar  FROM " . $GLOBALS['ecs']->table('admin_user') . " WHERE user_id = ecs_admin_taskes.sender_id) as sender_avatar ".
            " FROM ".$GLOBALS['ecs']->table('admin_taskes') .
            " WHERE $where ".
            " ORDER BY ".$orderby.
            " LIMIT ". $_REQUEST['start'] .", $_REQUEST[page_size]";

    $row = $GLOBALS['db']->getAll($sql);

    global $status_mappings, $priority_mappings, $estimated_time_mappings;
    $unreadTaskIds = $ctrlTask->getUserUnreadTaskIds();
    foreach ($row AS $key=>$val)
    {
        $row[$key]['sent_time'] = local_date($GLOBALS['_CFG']['time_format'], $val['sent_time']);
        $row[$key]['read_time'] = local_date($GLOBALS['_CFG']['time_format'], $val['read_time']);
        $row[$key]['finish_time'] = local_date($GLOBALS['_CFG']['time_format'], $val['finish_time']);
        $row[$key]['dead_time'] = local_date($GLOBALS['_CFG']['date_format'], $val['dead_time']);
        $row[$key]['status_text'] = array_key_exists($val['status'], $status_mappings) ? $status_mappings[$val['status']] : 'N/A';
        $row[$key]['priority_text'] = array_key_exists($val['priority'], $priority_mappings) ? $priority_mappings[$val['priority']] : 'N/A';
        $val['estimated_time'] = intval($val['estimated_time']);
        $row[$key]['estimated_time_text'] = array_key_exists($val['estimated_time'], $estimated_time_mappings) ? $estimated_time_mappings[$val['estimated_time']] : ($val['estimated_time'] > 0 ? $val['estimated_time'] . ' mins' : 'N/A');
        $row[$key]['avatar'] = !empty($val['avatar']) ? '../'.$val['avatar'] : NULL;
        $row[$key]['sender_avatar'] = !empty($val['sender_avatar']) ? '../'.$val['sender_avatar'] : NULL;
        $row[$key]['isunread']=false;
        $row[$key]['message']=nl2br($val['message']);
        if(in_array($row[$key]['task_id'], $unreadTaskIds))
            $row[$key]['isunread']=true;
        
        $val = $row[$key];
        $id = $val['task_id'];
        $btn_list = [];
        $view_msg = _L('view_msg', '查看工作');
        $oneClick_btn= "";
        $view_msg_btn = [
            'icon_class' => 'fa fa-eye',
            'link'       => "tasklist.php?act=view&amp;id=".$id,
            'title'      => $view_msg,

        ];
        $btn_list['view_msg'] = $view_msg_btn;
        if($val['sender_id'] == $_SESSION['admin_id'] || $_SESSION['manage_cost']) {
            if($val['status'] != 5) {
                $edit_btn = [
                    'icon_class' => 'fa fa-edit',
                    'link'       => "tasklist.php?act=edit&amp;id=".$id,
                    'title'      => $view_msg
                ];
                $btn_list['edit'] = $edit_btn;
            }
            if($_REQUEST['msg_type'] == 3) {
                $restore_btn = [
                    'icon_class' => 'fa fa-repeat',
                    'link'       => "tasklist.php?act=restore&amp;id=".$id,
                    'title'      => '復原'
                ];
                $btn_list['restore'] = $restore_btn;
                $permanent_delete_btn = [
                    'icon_class' => 'fa fa-trash',
                    'link'       => 'javascript:;',
                    'title'      => '永久刪除',
                    'data'       => ['url'=>"tasklist.php?act=permanent_delete&amp;id=".$id],
                    'onclick'    => 'cellRemove(this);',

                ];
                $btn_list['permanent_delete'] = $permanent_delete_btn;
            } else {
                if($val['status'] != 5) {
                    $remove_btn = [
                        'icon_class' => 'fa fa-trash',
                        'link'       => 'javascript:;',
                        'title'      => '封存',
                        'data'       => ['url'=>"tasklist.php?act=remove&amp;id=".$id],
                        'onclick'    => 'cellRemove(this);',
                        'btn_class'  => 'btn-danger'

                    ];
                    $btn_list['remove'] = $remove_btn;
                } else {
                    $oneClickArchive_btn = [
                        'icon_class' => 'fa fa-trash',
                        'link'       => 'javascript:;',
                        'title'      => '一按封存',
                        'onclick'    => "oneClickArchive($id,&#039;您確認要封存這條記錄嗎?&#039;);",
                        'btn_class'  => 'btn-info',
                        'btn_size'   => 'btn btn-sm'

                    ];
                    // $btn_list['oneClickArchive'] = $oneClickArchive_btn;
                    $oneClick_btn = $ctrlTask->generateCrudActionBtn($id, 'id', 'tasklist.php', [], ['oneClickArchive'=>$oneClickArchive_btn]);
                }
            }
            if($val['status'] != 5 ) {
                $oneClickFinish_btn = [
                    'icon_class' => 'fa fa-circle',
                    'link'       => 'javascript:;',
                    'title'      => '一按完成',
                    'onclick'    => "oneClickFinish($id, &#039;您確認要完成這條工作記錄嗎?&#039;);",
                    'btn_class'  => 'btn-primary',
                    'btn_size'   => 'btn btn-sm'
                ];
                // $btn_list['oneClickFinish'] = $oneClickFinish_btn;
                $oneClick_btn = $ctrlTask->generateCrudActionBtn($id, 'id', 'tasklist.php', [], ['oneClickFinish'=>$oneClickFinish_btn]);
            }
        } else {
            if($val['status'] != 4 && $val['status'] != 5 ) {
                $oneClickFinish_btn = [
                    'icon_class' => 'fa fa-circle',
                    'link'       => 'javascript:;',
                    'title'      => '一按完成',
                    'onclick'    => "oneClickFinish($id, &#039;您確認要完成這條工作記錄嗎?&#039;);",
                    'btn_class'  => 'btn-primary',
                    'btn_size'   => 'btn btn-sm'
                ];
                // $btn_list['oneClickFinish'] = $oneClickFinish_btn;
                $oneClick_btn = $ctrlTask->generateCrudActionBtn($id, 'id', 'tasklist.php', [], ['oneClickFinish'=>$oneClickFinish_btn]);
            }
        }
        $action = $ctrlTask->generateCrudActionBtn($id, 'id', 'tasklist.php', [], $btn_list);
        $row[$key]['_action'] = $action."<div style='margin-top: 5px;'>".$oneClick_btn."</div>";
    }

    $arr = array('item' => $row,'msg_type'=>$_REQUEST['msg_type'], 'record_count' => $_REQUEST['record_count']);

    return $arr;
}

?>