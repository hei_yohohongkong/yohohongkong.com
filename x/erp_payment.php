<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require_once(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require_once(dirname(__FILE__) .'/includes/ERP/lib_erp_finance.php');
require_once(dirname(__FILE__) .'/includes/ERP/lib_erp_supplier.php');
require_once(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
if ($_REQUEST['act'] == 'edit_payment')
{
include('../includes/cls_json.php');
$json  = new JSON;
if(!admin_priv('erp_finance_manage','',false))
{
$result['error']=1;
$result['message']=$_LANG['erp_no_permit'];
die($json->encode($result));
}
$payment_id=isset($_REQUEST['payment_id']) &&intval($_REQUEST['payment_id'])>0?intval($_REQUEST['payment_id']):0;
$payment_info=is_payment_exists($payment_id);
if(empty($payment_info))
{
$result['error']=4;
$result['message']=$_LANG['erp_payment_not_exist'];
die($json->encode($result));
}
$agency_id=get_admin_agency_id($_SESSION['admin_id']);
if($agency_id >0 &&$agency_id!=$payment_info['agency_id'])
{
$result['error']=4;
$result['message']=$_LANG['erp_payment_no_agency_access'];
die($json->encode($result));
}
if($payment_info['payment_status']!=1)
{
$result['error']=4;
$result['message']=$_LANG['erp_payment_no_acts'];
die($json->encode($result));
}
if(lock_payment($payment_id,'edit'))	
{
if(payment_act_record($payment_id,'modify'))
{
$result['error']=0;
die($json->encode($result));
}
}
else{
$result['error']=-1;
$result['message']=$_LANG['erp_payment_no_accessibility'];
die($json->encode($result));
}
}
elseif ($_REQUEST['act'] == 'change_order')
{
include('../includes/cls_json.php');
$json  = new JSON;
if(!admin_priv('erp_finance_manage','',false))
{
$result['error']=1;
$result['message']=$_LANG['erp_no_permit'];
die($json->encode($result));
}
$payment_id=isset($_REQUEST['payment_id']) &&intval($_REQUEST['payment_id'])>0?intval($_REQUEST['payment_id']):0;
$order_id=isset($_REQUEST['order_id']) &&intval($_REQUEST['order_id'])>0?intval($_REQUEST['order_id']):0;
$pay_amount=isset($_REQUEST['pay_amount']) &&floatval($_REQUEST['pay_amount'])>0?floatval($_REQUEST['pay_amount']):0;
$payment_info=is_payment_exists($payment_id);
if(empty($payment_info))
{
$result['error']=4;
$result['message']=$_LANG['erp_payment_not_exist'];
die($json->encode($result));
}
$agency_id=get_admin_agency_id($_SESSION['admin_id']);
if($agency_id >0 &&$agency_id!=$payment_info['agency_id'])
{
$result['error']=4;
$result['message']=$_LANG['erp_payment_no_agency_access'];
die($json->encode($result));
}
if($payment_info['payment_status']!=1)
{
$result['error']=4;
$result['message']=$_LANG['erp_payment_no_acts'];
die($json->encode($result));
}
if(lock_payment($payment_id,'edit'))	
{
if(payment_act_record($payment_id,'modify'))
{
update_payment($payment_id,$order_id,$pay_amount);
$payment_info=get_payment_info($payment_id);
$result['pay_to']=$payment_info[0]['supplier_info'][0]['name'];
$result['bill_date']=$payment_info[0]['bill_date'];
$result['bill_amount']=$payment_info[0]['bill_amount'];
$result['bill_total_payable']=$payment_info[0]['bill_total_payable'];
$result['bill_paid_payable']=$payment_info[0]['bill_paid_payable'];
$result['bill_payable_balance']=$payment_info[0]['bill_payable_balance'];
$result['pay_amount']=$payment_info[0]['pay_amount'];
$result['bill_no']=$payment_info[0]['order_info']['order_sn'];
$result['error']=0;
die($json->encode($result));
}
}
else{
$result['error']=-1;
$result['message']=$_LANG['erp_payment_no_accessibility'];
die($json->encode($result));
}
}
elseif ($_REQUEST['act'] == 'change_bank_account')
{
include('../includes/cls_json.php');
$json  = new JSON;
if(!admin_priv('erp_finance_manage','',false))
{
$result['error']=1;
$result['message']=$_LANG['erp_no_permit'];
die($json->encode($result));
}
if(empty($_REQUEST['bank_account_id']))
{
$result['error']=2;
$result['message']=$_LANG['erp_wrong_parameter'];
die($json->encode($result));
}
$payment_id=isset($_REQUEST['payment_id']) &&intval($_REQUEST['payment_id'])>0?intval($_REQUEST['payment_id']):0;
$bank_account_id=isset($_REQUEST['bank_account_id']) &&intval($_REQUEST['bank_account_id'])>0?intval($_REQUEST['bank_account_id']):0;
$payment_info=is_payment_exists($payment_id);
if(empty($payment_info))
{
$result['error']=4;
$result['message']=$_LANG['erp_payment_not_exist'];
die($json->encode($result));
}
$agency_id=get_admin_agency_id($_SESSION['admin_id']);
if($agency_id >0 &&$agency_id!=$payment_info['agency_id'])
{
$result['error']=4;
$result['message']=$_LANG['erp_payment_no_agency_access'];
die($json->encode($result));
}
if($payment_info['payment_status']!=1)
{
$result['error']=4;
$result['message']=$_LANG['erp_payment_no_acts'];
die($json->encode($result));
}
if(lock_payment($payment_id,'edit'))	
{
if(payment_act_record($payment_id,'modify'))
{
$sql="update ".$GLOBALS['ecs']->table('erp_payment')." set account_id='".$bank_account_id."' where payment_id='".$payment_id."'";
$GLOBALS['db']->query($sql);
$account_info=get_bank_account_info($bank_account_id);
$result['error']=0;
$result['account_name']=$account_info['account_name'];
die($json->encode($result));
}
}
else{
$result['error']=-1;
$result['message']=$_LANG['erp_payment_no_accessibility'];
die($json->encode($result));
}
}
elseif ($_REQUEST['act'] == 'change_payment_remark')
{
include('../includes/cls_json.php');
$json  = new JSON;
if(!admin_priv('erp_finance_manage','',false))
{
$result['error']=1;
$result['message']=$_LANG['erp_no_permit'];
die($json->encode($result));
}
if(empty($_REQUEST['payment_id']))
{
$result['error']=2;
$result['message']=$_LANG['erp_wrong_parameter'];
die($json->encode($result));
}
$payment_id=isset($_REQUEST['payment_id']) &&intval($_REQUEST['payment_id'])>0?intval($_REQUEST['payment_id']):0;
$payment_remark=isset($_REQUEST['payment_remark'])?trim($_REQUEST['payment_remark']):'';
$payment_info=is_payment_exists($payment_id);
if(empty($payment_info))
{
$result['error']=4;
$result['message']=$_LANG['erp_payment_not_exist'];
die($json->encode($result));
}
$agency_id=get_admin_agency_id($_SESSION['admin_id']);
if($agency_id >0 &&$agency_id!=$payment_info['agency_id'])
{
$result['error']=4;
$result['message']=$_LANG['erp_payment_no_agency_access'];
die($json->encode($result));
}
if($payment_info['payment_status']!=1)
{
$result['error']=4;
$result['message']=$_LANG['erp_payment_no_acts'];
die($json->encode($result));
}
if(lock_payment($payment_id,'edit'))	
{
if(payment_act_record($payment_id,'modify'))
{
$sql="update ".$GLOBALS['ecs']->table('erp_payment')." set payment_remark='".$payment_remark."' where payment_id='".$payment_id."'";
$GLOBALS['db']->query($sql);
$result['error']=0;
die($json->encode($result));
}
}
else{
$result['error']=-1;
$result['message']=$_LANG['erp_payment_no_accessibility'];
die($json->encode($result));
}
}
elseif ($_REQUEST['act'] == 'change_pay_amount')
{
include('../includes/cls_json.php');
$json  = new JSON;
if(!admin_priv('erp_finance_manage','',false))
{
$result['error']=1;
$result['message']=$_LANG['erp_no_permit'];
die($json->encode($result));
}
$payment_id=isset($_REQUEST['payment_id']) &&intval($_REQUEST['payment_id'])>0?intval($_REQUEST['payment_id']):0;
$pay_amount=isset($_REQUEST['pay_amount']) &&floatval($_REQUEST['pay_amount'])>0?floatval($_REQUEST['pay_amount']):0;
$payment_info=is_payment_exists($payment_id);
if(empty($payment_info))
{
$result['error']=4;
$result['message']=$_LANG['erp_payment_not_exist'];
die($json->encode($result));
}
$agency_id=get_admin_agency_id($_SESSION['admin_id']);
if($agency_id >0 &&$agency_id!=$payment_info['agency_id'])
{
$result['error']=4;
$result['message']=$_LANG['erp_payment_no_agency_access'];
die($json->encode($result));
}
if($payment_info['payment_status']!=1)
{
$result['error']=4;
$result['message']=$_LANG['erp_payment_no_acts'];
die($json->encode($result));
}
if(lock_payment($payment_id,'edit'))	
{
if(payment_act_record($payment_id,'modify'))
{
$payment_info=get_payment_list(array('payment_id'=>$payment_id));
$order_id=$payment_info[0]['bill_id'];
$order_info=get_payable_order_list(array('order_id'=>$order_id));
update_payment($payment_id,$order_id,$pay_amount);
$result['error']=0;
die($json->encode($result));
}
}
else{
$result['error']=-1;
$result['message']=$_LANG['erp_payment_no_accessibility'];
die($json->encode($result));
}
}
elseif ($_REQUEST['act'] == 'delete_payment')
{
include('../includes/cls_json.php');
$json  = new JSON;
if(!admin_priv('erp_finance_manage','',false))
{
$result['error']=1;
$result['message']=$_LANG['erp_no_permit'];
die($json->encode($result));
}
$payment_id=isset($_REQUEST['payment_id']) &&intval($_REQUEST['payment_id'])>0?intval($_REQUEST['payment_id']):0;
$payment_info=is_payment_exists($payment_id);
if(empty($payment_info))
{
$result['error']=4;
$result['message']=$_LANG['erp_payment_not_exist'];
die($json->encode($result));
}
$agency_id=get_admin_agency_id($_SESSION['admin_id']);
if($agency_id >0 &&$agency_id!=$payment_info['agency_id'])
{
$result['error']=4;
$result['message']=$_LANG['erp_payment_no_agency_access'];
die($json->encode($result));
}
if($payment_info['payment_status']!=1)
{
$result['error']=4;
$result['message']=$_LANG['erp_payment_no_acts'];
die($json->encode($result));
}
if(lock_payment($payment_id,'delete'))	
{
if(delete_payment($payment_id) &&delete_payment_details($payment_id))
{
$result['error']=0;
$result['message']=$_LANG['erp_payment_delete_sucess'];
die($json->encode($result));
}
}
else{
$result['error']=-1;
$result['message']=$_LANG['erp_payment_no_accessibility'];
die($json->encode($result));
}
}
elseif ($_REQUEST['act'] == 'post_to_approve')
{
include('../includes/cls_json.php');
$json  = new JSON;
if(!admin_priv('erp_finance_manage','',false))
{
$result['error']=1;
$result['message']=$_LANG['erp_no_permit'];
die($json->encode($result));
}
$payment_id=isset($_REQUEST['payment_id']) &&intval($_REQUEST['payment_id'])>0?intval($_REQUEST['payment_id']):0;
$payment_info=is_payment_exists($payment_id);
if(empty($payment_info))
{
$result['error']=4;
$result['message']=$_LANG['erp_payment_not_exist'];
die($json->encode($result));
}
$agency_id=get_admin_agency_id($_SESSION['admin_id']);
if($agency_id >0 &&$agency_id!=$payment_info['agency_id'])
{
$result['error']=4;
$result['message']=$_LANG['erp_payment_no_agency_access'];
die($json->encode($result));
}
if($payment_info['payment_status']!=1)
{
$result['error']=4;
$result['message']=$_LANG['erp_payment_no_acts'];
die($json->encode($result));
}
if(lock_payment($payment_id,'post_to_approve'))	
{
if(payment_act_record($payment_id,'post'))
{
$sql="update ".$GLOBALS['ecs']->table('erp_payment')." set ";
$sql.="payment_status ='2' ";
$sql.="where payment_id='".$payment_id."'";
if($GLOBALS['db']->query($sql))
{
$result['error']=0;
$result['message']=$_LANG['erp_payment_post_success'];
die($json->encode($result));
}
}
}
else{
$result['error']=-1;
$result['message']=$_LANG['erp_payment_no_accessibility'];
die($json->encode($result));
}
}
elseif ($_REQUEST['act'] == 'approve_pass'||$_REQUEST['act'] == 'approve_reject')
{
include('../includes/cls_json.php');
$json  = new JSON;
if(!admin_priv('erp_finance_approve','',false))
{
$result['error']=1;
$result['message']=$_LANG['erp_no_permit'];
die($json->encode($result));
}
$payment_id=isset($_REQUEST['payment_id']) &&intval($_REQUEST['payment_id'])>0?intval($_REQUEST['payment_id']):0;
$approve_remark=isset($_REQUEST['approve_remark'])?trim($_REQUEST['approve_remark']):'';
$act=isset($_REQUEST['act'])?trim($_REQUEST['act']):'';
$payment_info=is_payment_exists($payment_id);
if(empty($payment_info))
{
$result['error']=4;
$result['message']=$_LANG['erp_payment_not_exist'];
die($json->encode($result));
}
$agency_id=get_admin_agency_id($_SESSION['admin_id']);
if($agency_id >0 &&$agency_id!=$payment_info['agency_id'])
{
$result['error']=4;
$result['message']=$_LANG['erp_payment_no_agency_access'];
die($json->encode($result));
}
if($payment_info['payment_status']!=2)
{
$result['error']=4;
$result['message']=$_LANG['erp_payment_no_acts'];
die($json->encode($result));
}
if(lock_payment($payment_id,'approve'))	
{
if(payment_act_record($payment_id,'approve'))
{
if($act=='approve_pass')
{
if(approve_payment_pass($payment_id,$approve_remark))
{
$result['error']=0;
$result['message']=$_LANG['erp_payment_approve_success'];
die($json->encode($result));
}
}
else if($act=='approve_reject')
{
if(approve_payment_reject($payment_id,$approve_remark))	
{
$result['error']=0;
$result['message']=$_LANG['erp_payment_approve_success'];
die($json->encode($result));
}
}
}
}
else{
$result['error']=-1;
$result['message']=$_LANG['erp_payment_no_accessibility'];
die($json->encode($result));
}
}
?>
