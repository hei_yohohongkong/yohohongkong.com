<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require_once(dirname(__FILE__) .'/includes//ERP/lib_erp_common.php');
require_once(dirname(__FILE__) .'/includes/ERP/lib_erp_warehouse.php');
require_once(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
require_once(dirname(__FILE__) .'/includes/ERP/lib_erp_goods.php');
if ($_REQUEST['act'] == 'change_warehouse_from')
{
include('../includes/cls_json.php');
$json  = new JSON;
if(!admin_priv('erp_warehouse_manage','',false))
{
$result['error']=1;
$result['message']=$_LANG['erp_no_permit'];
die($json->encode($result));
}
$transfer_id=isset($_REQUEST['transfer_id']) &&intval($_REQUEST['transfer_id'])>0?intval($_REQUEST['transfer_id']) : 0;
$warehouse_id=isset($_REQUEST['warehouse_id']) &&intval($_REQUEST['warehouse_id'])>0?intval($_REQUEST['warehouse_id']) : 0;
if(empty($transfer_id) ||empty($warehouse_id))
{
$result['error']=2;
$result['message']=$_LANG['erp_wrong_parameter'];
die($json->encode($result));
}
$transfer_info=is_stock_transfer_exist($transfer_id);
if(!$transfer_info)
{
$result['error']=3;
$result['message']=$_LANG['erp_stock_transfer_not_exists'];
die($json->encode($result));
}
$agency_id=get_admin_agency_id($_SESSION['admin_id']);
if(!is_agency_transfer($transfer_id,$agency_id))
{
$result['error']=2;
$result['message']=$_LANG['erp_stock_transfer_no_agency_access'];
die($json->encode($result));
}
if($transfer_info['status']!=1)
{
$result['error']=6;
$result['message']=$_LANG['erp_stock_transfer_no_acts'];
die($json->encode($result));
}
if(!is_admin_stock_transfer($transfer_id ,erp_get_admin_id()))
{
$result['error']=5;
$result['message']=$_LANG['erp_stock_transfer_no_access'];
die($json->encode($result));
}
$warehouse_info=is_warehouse_exist($warehouse_id);
if(!$warehouse_info)
{
$result['error']=4;
$result['message']=$_LANG['erp_warehouse_not_exist'];
die($json->encode($result));
}
if(!is_admin_warehouse($warehouse_id ,erp_get_admin_id()))
{
$result['error']=5;
$result['message']=$_LANG['erp_warehouse_no_access'];
die($json->encode($result));
}
$transfer_item=get_goods_transfer_item_info($transfer_id);
if(!empty($transfer_item))
{
$result['error']=5;
$result['message']=$_LANG['erp_stock_transfer_warehouse_cant_change'];
die($json->encode($result));
}
if(lock_stock_transfer_table($transfer_id,'edit'))
{
$sql="update ".$ecs->table('erp_stock_transfer')." set warehouse_from='".$warehouse_id."',admin_from='".erp_get_admin_id()."' where transfer_id='".$transfer_id."'";
$db->query($sql);
$result['error']=0;
die($json->encode($result));
}
else{
$result['error']=-1;
$result['message']=$_LANG['erp_stock_transfer_no_accessibility'];
die($json->encode($result));
}
}
if ($_REQUEST['act'] == 'change_warehouse_to')
{
include('../includes/cls_json.php');
$json  = new JSON;
if(!admin_priv('erp_warehouse_manage','',false))
{
$result['error']=1;
$result['message']=$_LANG['erp_no_permit'];
die($json->encode($result));
}
$transfer_id=isset($_REQUEST['transfer_id']) &&intval($_REQUEST['transfer_id'])>0?intval($_REQUEST['transfer_id']) : 0;
$warehouse_id=isset($_REQUEST['warehouse_id']) &&intval($_REQUEST['warehouse_id'])>0?intval($_REQUEST['warehouse_id']) : 0;
if(empty($transfer_id) ||empty($warehouse_id))
{
$result['error']=2;
$result['message']=$_LANG['erp_wrong_parameter'];
die($json->encode($result));
}
$transfer_info=is_stock_transfer_exist($transfer_id);
if(!$transfer_info)
{
$result['error']=3;
$result['message']=$_LANG['erp_stock_transfer_not_exists'];
die($json->encode($result));
}
$agency_id=get_admin_agency_id($_SESSION['admin_id']);
if(!is_agency_transfer($transfer_id,$agency_id))
{
$result['error']=2;
$result['message']=$_LANG['erp_stock_transfer_no_agency_access'];
die($json->encode($result));
}
if($transfer_info['status']!=1)
{
$result['error']=6;
$result['message']=$_LANG['erp_stock_transfer_no_acts'];
die($json->encode($result));
}
if(!is_admin_stock_transfer($transfer_id ,erp_get_admin_id()))
{
$result['error']=5;
$result['message']=$_LANG['erp_stock_transfer_no_access'];
die($json->encode($result));
}
$warehouse_info=is_warehouse_exist($warehouse_id);
if(!$warehouse_info)
{
$result['error']=4;
$result['message']=$_LANG['erp_warehouse_not_exist'];
die($json->encode($result));
}
if(lock_stock_transfer_table($transfer_id,'edit'))
{
if($warehouse_id==$transfer_info['warehouse_from'])
{
$result['error']=4;
$result['message']=$_LANG['erp_stock_transfer_warehouse_same'];
die($json->encode($result));
}
$sql="update ".$ecs->table('erp_stock_transfer')." set warehouse_to='".$warehouse_id."' where transfer_id='".$transfer_id."'";
$db->query($sql);
$result['error']=0;
die($json->encode($result));
}
else{
$result['error']=-1;
$result['message']=$_LANG['erp_stock_transfer_no_accessibility'];
die($json->encode($result));
}
}
elseif ($_REQUEST['act'] == 'change_transfer_qty')
{
include('../includes/cls_json.php');
$json  = new JSON;
if(!admin_priv('erp_warehouse_manage','',false))
{
$result['error']=1;
$result['message']=$_LANG['erp_no_permit'];
die($json->encode($result));
}
$transfer_id=isset($_REQUEST['transfer_id']) &&intval($_REQUEST['transfer_id'])>0?intval($_REQUEST['transfer_id']):0;
$item_id=isset($_REQUEST['item_id']) &&intval($_REQUEST['item_id'])>0?intval($_REQUEST['item_id']):0;
$transfer_qty=isset($_REQUEST['transfer_qty']) &&intval($_REQUEST['transfer_qty'])>0?intval($_REQUEST['transfer_qty']):0;
if(empty($transfer_id) ||empty($item_id))
{
$result['error']=2;
$result['message']=$_LANG['erp_wrong_parameter'];
die($json->encode($result));
}
$transfer_info=is_stock_transfer_exist($transfer_id);
if(!$transfer_info)
{
$result['error']=3;
$result['message']=$_LANG['erp_stock_transfer_not_exists'];
die($json->encode($result));
}
$agency_id=get_admin_agency_id($_SESSION['admin_id']);
if(!is_agency_transfer($transfer_id,$agency_id))
{
$result['error']=2;
$result['message']=$_LANG['erp_stock_transfer_no_agency_access'];
die($json->encode($result));
}
if($transfer_info['status']!=1)
{
$result['error']=6;
$result['message']=$_LANG['erp_stock_transfer_no_acts'];
die($json->encode($result));
}
if(!is_admin_stock_transfer($transfer_id ,erp_get_admin_id()))
{
$result['error']=5;
$result['message']=$_LANG['erp_stock_transfer_no_access'];
die($json->encode($result));
}
$transfer_item_info=is_stock_transfer_item_exist($item_id);
if(!$transfer_item_info)
{
$result['error']=4;
$result['message']=$_LANG['erp_stock_transfer_item_not_exists'];
die($json->encode($result));
}
if(empty($transfer_item_info['attr_id']))
{
$stock=get_goods_stock_by_warehouse($transfer_info['warehouse_from'],$transfer_item_info['goods_id']);
}
else{
$stock=get_goods_stock_by_warehouse($transfer_info['warehouse_from'],$transfer_item_info['goods_id'],$transfer_item_info['attr_id']);
}
if($transfer_qty >$stock)
{
$result['error']=2;
$result['message']=sprintf($_LANG['erp_stock_transfer_stock_not_enough'],$stock);
die($json->encode($result));
}
if(lock_stock_transfer_table($transfer_id,'edit'))
{
$sql="update ".$ecs->table('erp_stock_transfer_item')." set transfer_qty='".$transfer_qty."' where item_id='".$item_id."'";
$db->query($sql);
$result['error']=0;
die($json->encode($result));
}
else{
$result['error']=-1;
$result['message']=$_LANG['erp_stock_transfer_no_accessibility'];
die($json->encode($result));
}
}
elseif ($_REQUEST['act'] == 'delete_item')
{
include('../includes/cls_json.php');
$json  = new JSON;
if(!admin_priv('erp_warehouse_manage','',false))
{
$result['error']=1;
$result['message']=$_LANG['erp_no_permit'];
die($json->encode($result));
}
$transfer_id=isset($_REQUEST['transfer_id']) &&intval($_REQUEST['transfer_id'])>0?intval($_REQUEST['transfer_id']):0;
$item_id=isset($_REQUEST['item_id']) &&intval($_REQUEST['item_id'])>0?intval($_REQUEST['item_id']):0;
if(empty($transfer_id) ||empty($item_id))
{
$result['error']=2;
$result['message']=$_LANG['erp_wrong_parameter'];
die($json->encode($result));
}
$transfer_info=is_stock_transfer_exist($transfer_id);
if(!$transfer_info)
{
$result['error']=3;
$result['message']=$_LANG['erp_stock_transfer_not_exists'];
die($json->encode($result));
}
$agency_id=get_admin_agency_id($_SESSION['admin_id']);
if(!is_agency_transfer($transfer_id,$agency_id))
{
$result['error']=2;
$result['message']=$_LANG['erp_stock_transfer_no_agency_access'];
die($json->encode($result));
}
if($transfer_info['status']!=1)
{
$result['error']=6;
$result['message']=$_LANG['erp_stock_transfer_no_acts'];
die($json->encode($result));
}
if(!is_admin_stock_transfer($transfer_id ,erp_get_admin_id()))
{
$result['error']=5;
$result['message']=$_LANG['erp_stock_transfer_no_access'];
die($json->encode($result));
}
$item_info=is_stock_transfer_item_exist($item_id);
if(!$item_info)
{
$result['error']=4;
$result['message']=$_LANG['erp_stock_transfer_item_not_exists'];
die($json->encode($result));
}
if(lock_stock_transfer_table($transfer_id,'edit'))
{
$sql="delete from ".$ecs->table('erp_stock_transfer_item')." where item_id='".$item_id."'";
$db->query($sql);
$result['error']=0;
die($json->encode($result));
}
else{
$result['error']=-1;
$result['message']=$_LANG['erp_stock_transfer_no_accessibility'];
die($json->encode($result));
}
}
elseif ($_REQUEST['act'] == 'delete_transfer')
{
include('../includes/cls_json.php');
$json  = new JSON;
if(!admin_priv('erp_warehouse_manage','',false))
{
$result['error']=1;
$result['message']=$_LANG['erp_no_permit'];
die($json->encode($result));
}
$transfer_id=isset($_REQUEST['transfer_id']) &&intval($_REQUEST['transfer_id'])>0?intval($_REQUEST['transfer_id']) : 0;
if(empty($transfer_id))
{
$result['error']=2;
$result['message']=$_LANG['erp_wrong_parameter'];
die($json->encode($result));
}
$transfer_info=is_stock_transfer_exist($transfer_id);
if(!$transfer_info)
{
$result['error']=3;
$result['message']=$_LANG['erp_stock_transfer_not_exists'];
die($json->encode($result));
}
$agency_id=get_admin_agency_id($_SESSION['admin_id']);
if(!is_agency_transfer($transfer_id,$agency_id))
{
$result['error']=2;
$result['message']=$_LANG['erp_stock_transfer_no_agency_access'];
die($json->encode($result));
}
if($transfer_info['status']!=1)
{
$result['error']=6;
$result['message']=$_LANG['erp_stock_transfer_no_acts'];
die($json->encode($result));
}
if(!is_admin_stock_transfer($transfer_id ,erp_get_admin_id()))
{
$result['error']=5;
$result['message']=$_LANG['erp_stock_transfer_no_access'];
die($json->encode($result));
}
if(lock_stock_transfer_table($transfer_id,'delete'))
{
$sql="delete from ".$ecs->table('erp_stock_transfer_item')." where transfer_id='".$transfer_id."'";
$db->query($sql);
$sql="delete from ".$ecs->table('erp_stock_transfer')." where transfer_id='".$transfer_id."'";
$db->query($sql);
$result['error']=0;
die($json->encode($result));
}
else{
$result['error']=-1;
$result['message']=$_LANG['erp_stock_transfer_no_accessibility'];
die($json->encode($result));
}
}
elseif ($_REQUEST['act'] == 'check_goods_sn'||$_REQUEST['act'] == 'check_goods_name')
{
include('../includes/cls_json.php');
$json  = new JSON;
if($_REQUEST['act'] == 'check_goods_sn')
{
$goods_sn=trim($_REQUEST['goods_sn']);
$goods_id=check_goods_sn($goods_sn);
if(empty($goods_id))
{
$result['error']=1;
$result['message']=$_LANG['erp_stock_transfer_wrong_goods_sn'];
die($json->encode($result));
}
}
else{
$goods_name=trim($_REQUEST['goods_name']);
$goods_id=check_goods_name($goods_name);
if(empty($goods_id))
{
$result['error']=1;
$result['message']=$_LANG['erp_stock_transfer_wrong_goods_name'];
die($json->encode($result));
}
}
$goods_attr=goods_attr($goods_id);
$smarty->assign('goods_attr',$goods_attr);
$attr_info=$smarty->fetch('erp_select_goods_attr.htm');
$result['attr_info']=$attr_info;
$result['attr_num']=count($goods_attr);
$result['error']=0;
die($json->encode($result));
}
elseif ($_REQUEST['act'] == 'goods_sn_tips')
{
include('../includes/cls_json.php');
$json  = new JSON;
$goods_sn=trim($_REQUEST['goods_sn']);
$fetch_goods_sn=array();
$sql="select g.goods_sn from ".$GLOBALS['ecs']->table('goods')." as g ";
$sql.=" where g.goods_sn like '%".$goods_sn."%' order by goods_id desc limit 10";
$res=$GLOBALS['db']->query($sql);
while($row=mysql_fetch_assoc($res))
{
$fetch_goods_sn[]=$row['goods_sn'];
}
$smarty->assign('goods_sn',$fetch_goods_sn);
$content=$smarty->fetch('erp_goods_sn_tips.htm');
$result['error']=0;
$result['content']=$content;
$result['num']=count($fetch_goods_sn);
die($json->encode($result));
}
elseif ($_REQUEST['act'] == 'goods_name_tips')
{
include('../includes/cls_json.php');
$json  = new JSON;
$goods_name=trim($_REQUEST['goods_name']);
$fetch_goods_name=array();
$sql="select g.goods_name from ".$GLOBALS['ecs']->table('goods')." as g ";
$sql.=" where g.goods_name like '%".$goods_name."%' order by goods_id desc limit 10";
$res=$GLOBALS['db']->query($sql);
while($row=mysql_fetch_assoc($res))
{
$fetch_goods_name[]=$row['goods_name'];
}
$smarty->assign('goods_name',$fetch_goods_name);
$content=$smarty->fetch('erp_goods_name_tips.htm');
$result['error']=0;
$result['content']=$content;
$result['num']=count($fetch_goods_name);
die($json->encode($result));
}
elseif ($_REQUEST['act'] == 'update_transfer')
{
include('../includes/cls_json.php');
$json  = new JSON;
if(!admin_priv('erp_warehouse_manage','',false))
{
$result['error']=1;
$result['message']=$_LANG['erp_no_permit'];
die($json->encode($result));
}
$transfer_id=isset($_REQUEST['transfer_id']) &&intval($_REQUEST['transfer_id'])>0 ?intval($_REQUEST['transfer_id']):0;
$goods_sn=isset($_REQUEST['goods_sn'])?trim($_REQUEST['goods_sn']):'';
$goods_name=isset($_REQUEST['goods_name'])?trim($_REQUEST['goods_name']):'';
$ids=isset($_REQUEST['ids'])?trim($_REQUEST['ids']):'';
$goods_qty=isset($_REQUEST['goods_qty']) &&intval($_REQUEST['goods_qty'])>0?intval($_REQUEST['goods_qty']):1;
if(empty($transfer_id))
{
$result['error']=1;
$result['message']=$_LANG['erp_wrong_parameter'];
die($json->encode($result));
}
if(empty($goods_sn) &&empty($goods_name))
{
$result['error']=1;
$result['message']=$_LANG['erp_stock_transfer_goods_sn_or_goods_name_required'];
die($json->encode($result));
}
$transfer_info=is_stock_transfer_exist($transfer_id);
if(!$transfer_info)
{
$result['error']=3;
$result['message']=$_LANG['erp_stock_transfer_not_exists'];
die($json->encode($result));
}
$agency_id=get_admin_agency_id($_SESSION['admin_id']);
if(!is_agency_transfer($transfer_id,$agency_id))
{
$result['error']=2;
$result['message']=$_LANG['erp_stock_transfer_no_agency_access'];
die($json->encode($result));
}
if($transfer_info['status']!=1)
{
$result['error']=6;
$result['message']=$_LANG['erp_stock_transfer_no_acts'];
die($json->encode($result));
}
if(!is_admin_stock_transfer($transfer_id ,erp_get_admin_id()))
{
$result['error']=5;
$result['message']=$_LANG['erp_stock_transfer_no_access'];
die($json->encode($result));
}
if(!empty($goods_sn))
{
$goods_id=check_goods_sn($goods_sn);
if(empty($goods_id))
{
$result['error']=1;
$result['message']=$_LANG['erp_delivery_wrong_goods_sn'];
die($json->encode($result));
}
}
else{
$goods_id=check_goods_name($goods_name);
if(empty($goods_id))
{
$result['error']=1;
$result['message']=$_LANG['erp_delivery_wrong_goods_name'];
die($json->encode($result));
}
}
$warehouse_id=$transfer_info['warehouse_from'];
if(empty($ids))
{
$stock=get_goods_stock_by_warehouse($warehouse_id,$goods_id);
$sql="select item_id,transfer_qty from ".$ecs->table('erp_stock_transfer_item')." where transfer_id='".$transfer_id."' and goods_id='".$goods_id."'";
$transfer_item_info=$db->getRow($sql);
$attr_id=get_attr_id($goods_id);
if(empty($transfer_item_info))
{
if($goods_qty >$stock)
{
$result['error']=2;
$result['message']=sprintf($_LANG['erp_stock_transfer_stock_not_enough'],$stock);
die($json->encode($result));
}
$sql="insert into ".$ecs->table('erp_stock_transfer_item')." set transfer_id='".$transfer_id."',goods_id='".$goods_id."',attr_id='".$attr_id."',transfer_qty='".$goods_qty."'";
$db->query($sql);
$result['error']=0;
die($json->encode($result));
}
else{
if($goods_qty+$transfer_item_info['transfer_qty'] >$stock)
{
$result['error']=2;
$result['message']=sprintf($_LANG['erp_stock_transfer_stock_not_enough'],$stock);
die($json->encode($result));
}
$sql="update ".$ecs->table('erp_stock_transfer_item')." set transfer_qty=transfer_qty+'".$goods_qty."' where item_id='".$transfer_item_info['item_id']."'";
$db->query($sql);
$result['error']=0;
die($json->encode($result));
}
}
else{
$ids=trim($ids,',');
$attr_id=get_attr_id($goods_id,$ids);
$stock=get_goods_stock_by_warehouse($warehouse_id,$goods_id,$attr_id);
$sql="select item_id,transfer_qty from ".$ecs->table('erp_stock_transfer_item')." where transfer_id='".$transfer_id."' and goods_id='".$goods_id."' and attr_id='".$attr_id."'";
$transfer_item_info=$db->getRow($sql);
if(empty($transfer_item_info))
{
if($goods_qty >$stock)
{
$result['error']=2;
$result['message']=sprintf($_LANG['erp_stock_transfer_stock_not_enough'],$stock);
die($json->encode($result));
}
$sql="insert into ".$ecs->table('erp_stock_transfer_item')." set transfer_id='".$transfer_id."',goods_id='".$goods_id."',attr_id='".$attr_id."',transfer_qty='".$goods_qty."'";
$db->query($sql);
$result['error']=0;
die($json->encode($result));
}
else{
if($goods_qty+$transfer_item_info['transfer_qty'] >$stock)
{
$result['error']=2;
$result['message']=sprintf($_LANG['erp_stock_transfer_stock_not_enough'],$stock);
die($json->encode($result));
}
$sql="update ".$ecs->table('erp_stock_transfer_item')." set transfer_qty=transfer_qty+'".$goods_qty."' where item_id='".$transfer_item_info['item_id']."'";
$db->query($sql);
$result['error']=0;
die($json->encode($result));
}
}
}
elseif ($_REQUEST['act'] == 'post_transfer')
{
include('../includes/cls_json.php');
$json  = new JSON;
if(!admin_priv('erp_warehouse_manage','',false))
{
$result['error']=1;
$result['message']=$_LANG['erp_no_permit'];
die($json->encode($result));
}
$transfer_id=isset($_REQUEST['transfer_id'])?intval($_REQUEST['transfer_id']):0;
if(empty($transfer_id))
{
$result['error']=1;
$result['message']=$_LANG['erp_wrong_parameter'];
die($json->encode($result));
}
$transfer_info=is_stock_transfer_exist($transfer_id);
if(!$transfer_info)
{
$result['error']=3;
$result['message']=$_LANG['erp_stock_transfer_not_exists'];
die($json->encode($result));
}
$agency_id=get_admin_agency_id($_SESSION['admin_id']);
if(!is_agency_transfer($transfer_id,$agency_id))
{
$result['error']=2;
$result['message']=$_LANG['erp_stock_transfer_no_agency_access'];
die($json->encode($result));
}
if($transfer_info['status']!=1)
{
$result['error']=6;
$result['message']=$_LANG['erp_stock_transfer_no_acts'];
die($json->encode($result));
}
if(!is_admin_stock_transfer($transfer_id ,erp_get_admin_id()))
{
$result['error']=5;
$result['message']=$_LANG['erp_stock_transfer_no_access'];
die($json->encode($result));
}
$error=check_transfer_integrity($transfer_id);
if($error>0)
{
$result['error']=4;
$result['message']=$_LANG['erp_stock_transfer_not_completed'];
die($json->encode($result));
}
if(lock_stock_transfer_table($transfer_id,'post'))
{
$sql="update ".$ecs->table('erp_stock_transfer')." set status='2' where transfer_id='".$transfer_id."'";
if($db->query($sql))
{
$result['error']=0;
die($json->encode($result));
}
}
else{
$result['error']=-1;
$result['message']=$_LANG['erp_stock_transfer_no_accessibility'];
die($json->encode($result));
}
}
elseif ($_REQUEST['act'] == 'add_transfer')
{
include('../includes/cls_json.php');
$json  = new JSON;
if(!admin_priv('erp_warehouse_manage','',false))
{
$result['error']=1;
$result['message']=$_LANG['erp_no_permit'];
die($json->encode($result));
}
$transfer_id=add_transfer();
if($transfer_id==-1)
{
$result['error']=1;
$result['message']=$GLOBALS['_LANG']['erp_stock_transfer_without_valid_warehouse'];
die($json->encode($result));
}
else{
$result['error']=0;
$result['transfer_id']=$transfer_id;
die($json->encode($result));
}
}
elseif ($_REQUEST['act'] == 'withdrawal_to_edit')
{
include('../includes/cls_json.php');
$json  = new JSON;
if(!admin_priv('erp_warehouse_manage','',false))
{
$result['error']=1;
$result['message']=$_LANG['erp_no_permit'];
die($json->encode($result));
}
$transfer_id=isset($_REQUEST['transfer_id']) &&intval($_REQUEST['transfer_id'])>0?intval($_REQUEST['transfer_id']):0;
if(empty($transfer_id))
{
$result['error']=1;
$result['message']=$_LANG['erp_wrong_parameter'];
die($json->encode($result));
}
$transfer_info=is_stock_transfer_exist($transfer_id);
if(!$transfer_info)
{
$result['error']=3;
$result['message']=$_LANG['erp_stock_transfer_not_exists'];
die($json->encode($result));
}
$agency_id=get_admin_agency_id($_SESSION['admin_id']);
if(!is_agency_transfer($transfer_id,$agency_id))
{
$result['error']=2;
$result['message']=$_LANG['erp_stock_transfer_no_agency_access'];
die($json->encode($result));
}
if($transfer_info['status']!=2)
{
$result['error']=6;
$result['message']=$_LANG['erp_stock_transfer_no_acts'];
die($json->encode($result));
}
if(!is_admin_warehouse($transfer_info['warehouse_to'],erp_get_admin_id()))
{
$result['error']=6;
$result['message']=$_LANG['erp_stock_transfer_no_confirm_accessibility'];
die($json->encode($result));
}
$sql="update ".$ecs->table('erp_stock_transfer')." set status='1' where transfer_id='".$transfer_id."'";
$db->query($sql);
$result['error']=0;
die($json->encode($result));
}
elseif ($_REQUEST['act'] == 'reject_transfer')
{
include('../includes/cls_json.php');
$json  = new JSON;
if(!admin_priv('erp_warehouse_manage','',false))
{
$result['error']=1;
$result['message']=$_LANG['erp_no_permit'];
die($json->encode($result));
}
$transfer_id=intval($_REQUEST['transfer_id'])>0?intval($_REQUEST['transfer_id']):0;
if(empty($transfer_id))
{
$result['error']=1;
$result['message']=$_LANG['erp_wrong_parameter'];
die($json->encode($result));
}
$transfer_info=is_stock_transfer_exist($transfer_id);
if(!$transfer_info)
{
$result['error']=3;
$result['message']=$_LANG['erp_stock_transfer_not_exists'];
die($json->encode($result));
}
$agency_id=get_admin_agency_id($_SESSION['admin_id']);
if(!is_agency_transfer($transfer_id,$agency_id))
{
$result['error']=2;
$result['message']=$_LANG['erp_stock_transfer_no_agency_access'];
die($json->encode($result));
}
if($transfer_info['status']!=2)
{
$result['error']=6;
$result['message']=$_LANG['erp_stock_transfer_no_acts'];
die($json->encode($result));
}
if(!is_admin_warehouse($transfer_info['warehouse_to'],erp_get_admin_id()))
{
$result['error']=6;
$result['message']=$_LANG['erp_stock_transfer_no_confirm_accessibility'];
die($json->encode($result));
}
$sql="update ".$ecs->table('erp_stock_transfer')." set status='4',transfer_time='".gmtime()."',admin_to='".erp_get_admin_id()."' where transfer_id ='".$transfer_id ."'";
$db->query($sql);
$result['error']=0;
die($json->encode($result));
}
elseif ($_REQUEST['act'] == 'confirm_transfer')
{
include('../includes/cls_json.php');
$json  = new JSON;
if(!admin_priv('erp_warehouse_manage','',false))
{
$result['error']=1;
$result['message']=$_LANG['erp_no_permit'];
die($json->encode($result));
}
$transfer_id=isset($_REQUEST['transfer_id']) &&intval($_REQUEST['transfer_id'])>0?intval($_REQUEST['transfer_id']):0;
if(empty($transfer_id))
{
$result['error']=1;
$result['message']=$_LANG['erp_wrong_parameter'];
die($json->encode($result));
}
$transfer_info=is_stock_transfer_exist($transfer_id);
if(!$transfer_info)
{
$result['error']=3;
$result['message']=$_LANG['erp_stock_transfer_not_exists'];
die($json->encode($result));
}
$agency_id=get_admin_agency_id($_SESSION['admin_id']);
if(!is_agency_transfer($transfer_id,$agency_id))
{
$result['error']=2;
$result['message']=$_LANG['erp_stock_transfer_no_agency_access'];
die($json->encode($result));
}
if($transfer_info['status']!=2)
{
$result['error']=6;
$result['message']=$_LANG['erp_stock_transfer_no_acts'];
die($json->encode($result));
}
if(!is_admin_warehouse($transfer_info['warehouse_to'],erp_get_admin_id()))
{
$result['error']=6;
$result['message']=$_LANG['erp_stock_transfer_no_confirm_accessibility'];
die($json->encode($result));
}
if(lock_stock_transfer_table($transfer_id,'confirm'))
{
$GLOBALS['db']->query('START TRANSACTION');
$warehouse_from =$transfer_info['warehouse_from'];
$warehouse_to=$transfer_info['warehouse_to'];
$sql="select user_name from ".$GLOBALS['ecs']->table('admin_user')." where user_id='".$transfer_info['admin_from']."'";
$admin_from=$GLOBALS['db']->getOne($sql);
$sql="select user_name from ".$GLOBALS['ecs']->table('admin_user')." where user_id='".erp_get_admin_id()."'";
$admin_to=$GLOBALS['db']->getOne($sql);
$transfer_item_info=get_goods_transfer_item_info($transfer_id);
foreach($transfer_item_info as $transfer_item)
{
$goods_id=$transfer_item['goods_id'];
$attr_id=$transfer_item['attr_id'];
$qty=$transfer_item['transfer_qty'];
$sql="select stock from ".$ecs->table('erp_stock')." where goods_id='".$goods_id."' and goods_attr_id='".$attr_id."' and warehouse_id='".$warehouse_from."' order by stock_id desc limit 1";
$stock_from=$db->getOne($sql);
$stock_from=!empty($stock_from)?$stock_from: 0;
if($stock_from <$qty)
{
$GLOBALS['db']->query('ROLLBACK');
$result['error']=9;
$result['message']=$_LANG['erp_stock_transfer_confirm_failed_no_stock'];
die($json->encode($result));
}
$sql="insert into ".$ecs->table('erp_stock')." set goods_id='".$goods_id."',goods_attr_id='".$attr_id."',qty='".(0-$qty)."',act_time='".gmtime()."',warehouse_id='".$warehouse_from."'";
$sql.=", w_d_name='".$admin_from."', w_d_style_id='0',order_sn='',stock_style='d',w_d_sn='".$transfer_info['transfer_sn']."',stock=".($stock_from-$qty).",act_date='".local_date('Y-m-d',gmtime())."'";
if(!$db->query($sql))
{
$GLOBALS['db']->query('ROLLBACK');
$result['error']=9;
$result['message']=$_LANG['erp_stock_transfer_confirm_failed'];
die($json->encode($result));
}
$sql="select stock from ".$ecs->table('erp_stock')." where goods_id='".$goods_id."' and goods_attr_id='".$attr_id."' and warehouse_id='".$warehouse_to."' order by stock_id desc limit 1";
$stock_to=$db->getOne($sql);
$stock_to=!empty($stock_to)?$stock_to: 0;
$sql="insert into ".$ecs->table('erp_stock')." set goods_id='".$goods_id."',goods_attr_id='".$attr_id."',qty='".$qty."',act_time='".gmtime()."',warehouse_id='".$warehouse_to."'";
$sql.=", w_d_name='".$admin_to."', w_d_style_id='0',order_sn='',stock_style='w',w_d_sn='".$transfer_info['transfer_sn']."',stock=".($stock_to+$qty).",act_date='".local_date('Y-m-d',gmtime())."'";
if(!$db->query($sql))
{
$GLOBALS['db']->query('ROLLBACK');
$result['error']=9;
$result['message']=$_LANG['erp_stock_transfer_confirm_failed'];
die($json->encode($result));
}
if(empty($attr_id))
{
$sql="select attr_id from ".$ecs->table('erp_goods_attr')." where goods_id='".$goods_id."' and goods_attr_id_list='$$'";
$attr_id=$db->getOne($sql);
}
$sql="update ".$ecs->table('erp_goods_attr_stock')." set goods_qty =goods_qty -".$qty." where goods_id='".$goods_id."' and attr_id='".$attr_id."' and warehouse_id='".$warehouse_from."'";
if(!$db->query($sql))
{
$GLOBALS['db']->query('ROLLBACK');
$result['error']=9;
$result['message']=$_LANG['erp_stock_transfer_confirm_failed'];
die($json->encode($result));
}
$sql="update ".$ecs->table('erp_goods_attr_stock')." set goods_qty =goods_qty +".$qty." where goods_id='".$goods_id."' and attr_id='".$attr_id."' and warehouse_id='".$warehouse_to."'";
if(!$db->query($sql))
{
$GLOBALS['db']->query('ROLLBACK');
$result['error']=9;
$result['message']=$_LANG['erp_stock_transfer_confirm_failed'];
die($json->encode($result));
}
}
$sql="update ".$ecs->table('erp_stock_transfer')." set status='3',transfer_time='".gmtime()."',admin_to='".erp_get_admin_id()."' where transfer_id ='".$transfer_id ."'";
if(!$db->query($sql))
{
$GLOBALS['db']->query('ROLLBACK');
$result['error']=9;
$result['message']=$_LANG['erp_stock_transfer_confirm_failed'];
die($json->encode($result));
}
else{
$GLOBALS['db']->query('COMMIT');
$result['error']=0;
die($json->encode($result));
}
}
else{
$result['error']=-1;
$result['message']=$_LANG['erp_stock_transfer_no_accessibility'];
die($json->encode($result));
}
}
?>
