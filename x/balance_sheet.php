<?php
    define('IN_ECS',true);
    require(dirname(__FILE__) .'/includes/init.php');
    require(ROOT_PATH .'/includes/lib_order.php');

    if(isset($_REQUEST['act'])){
        admin_priv('menu_balance_sheet');
        if($_REQUEST['act'] == 'list'){
            // Inventory In Transit
            $sql = "SELECT SUM((eoi.order_qty - eoi.warehousing_qty) * price) AS cost FROM " . $ecs->table('erp_order_item') . " eoi " .
                    "LEFT JOIN " . $ecs->table('erp_order') . " eo ON eo.order_id = eoi.order_id " .
                    "WHERE eoi.order_id = eo.order_id AND eoi.warehousing_qty < eoi.order_qty AND eo.order_status = 4 AND eo.fully_paid = 1";
            $supplier_owe_us = $db->getOne($sql);
            // Accounts In Advance
            $sql = "SELECT SUM(goods_price * goods_number) AS cost FROM " . $ecs->table('order_info') . " oi " .
                    "LEFT JOIN " . $ecs->table('order_goods') . " og ON og.order_id = oi.order_id " .
                    "WHERE oi.pay_status = 2 AND oi.shipping_status IN (0,4) AND (oi.order_type NOT IN ('ws','sa') OR order_type IS NULL) " .
                    "AND user_id NOT IN(SELECT user_id FROM " . $ecs->table('users') . " WHERE is_wholesale = 1) " .
                    "AND og.rec_id NOT IN(SELECT order_item_id FROM " . $ecs->table('delivery_goods') . ")";
            $we_owe_consumer = $db->getOne($sql);
            // Accounts Payable - PO
            $sql = "SELECT SUM(eoi.warehousing_qty * price) AS cost FROM " . $ecs->table('erp_order_item') . " eoi " .
                    "LEFT JOIN " . $ecs->table('erp_order') . " eo ON eo.order_id = eoi.order_id " .
                    "WHERE eoi.order_id = eo.order_id AND eoi.warehousing_qty > 0 AND eo.order_status = 4 AND eo.fully_paid != 1 AND type IN (0, 1) AND eo.order_sn NOT LIKE '%PO14%' AND eo.order_sn NOT LIKE '%PO13%' AND price > 0 ";
            $we_owe_supplier_normal = $db->getOne($sql);
            // Inventory (Consignment)
            $sql = "SELECT SUM(eoi.warehousing_qty * price) AS cost FROM " . $ecs->table('erp_order_item') . " eoi " .
                    "LEFT JOIN " . $ecs->table('erp_order') . " eo ON eo.order_id = eoi.order_id " .
                    "WHERE eoi.order_id = eo.order_id AND eoi.warehousing_qty > 0 AND eo.order_status = 4 AND eo.fully_paid != 1 AND type = 2";
            $we_owe_supplier_consignment = $db->getOne($sql);
            // Accounts Receivable (B2B)
            $sql = "SELECT SUM(t_cost) AS cost FROM (" .
                    "SELECT CASE WHEN shipping_status = 2 THEN order_amount - actg_paid_amount ELSE SUM(goods_price * goods_number - goods_price * delivery_qty) END AS t_cost FROM " . $ecs->table('order_info') . " oi " .
                    "LEFT JOIN " . $ecs->table('order_goods') . " og ON og.order_id = oi.order_id " .
                    "WHERE oi.pay_status != 2 AND oi.shipping_status IN (2,4) AND (oi.order_type = 'ws' " .
                    "OR user_id IN(SELECT user_id FROM " . $ecs->table('users') . " WHERE is_wholesale = 1)) " .
                    "AND delivery_qty > 0 GROUP BY oi.order_id) a";
            $wholesale_owe_us = $db->getOne($sql);
            // Accounts In Advance (B2B)
            $sql = "SELECT SUM(t_cost) AS cost FROM (" .
                    "SELECT CASE WHEN shipping_status = 0 THEN order_amount ELSE SUM(goods_price * goods_number - goods_price * delivery_qty) END AS t_cost FROM " . $ecs->table('order_info') . " oi " .
                    "LEFT JOIN " . $ecs->table('order_goods') . " og ON og.order_id = oi.order_id " .
                    "WHERE oi.pay_status = 2 AND oi.shipping_status IN (0,4) AND (oi.order_type = 'ws' " .
                    "OR user_id IN(SELECT user_id FROM " . $ecs->table('users') . " WHERE is_wholesale = 1)) " .
                    "AND delivery_qty < goods_number GROUP BY oi.order_id) a";
            $we_owe_wholesale = $db->getOne($sql);
            // Accounts Payable - Refund
            $sql = "SELECT SUM(refund_amount) as refund_amount, SUM(integral_amount) as integral_amount FROM " . $GLOBALS['ecs']->table('refund_requests') . " WHERE status " . db_create_in(array(REFUND_PENDING, REFUND_PROCESS)) . " AND refund_type != " . REFUND_COMPENSATE;
            $refund = $db->getRow($sql);
            $refund_payable = floatval($refund['refund_amount']) + intval(value_of_integral($refund['integral_amount']));
            $table = array(
                array(
                    array('Accounts Receivable (B2B)','cons','wsou',price_format($wholesale_owe_us, false),'已出貨、未收款(批發)'),
                    array('','cons'),
                    array('','cons'),
                    array('Inventory In Transit','supp','sou',price_format($supplier_owe_us, false),'已付款、未到貨(供應商)'),
                    array('','supp'),
                ),
                array(
                    array('Accounts In Advance','cons','woc',price_format($we_owe_consumer, false),'已收款、未出齊貨(零售)'),
                    array('Accounts In Advance (B2B)','cons','wows',price_format($we_owe_wholesale, false),'已收款、未出齊貨(批發)'),
                    array('Accounts Payable - Refund','cons','ref',price_format($refund_payable, false),'未處理之退款申請'),
                    array('Accounts Payable - PO','supp','wosn',price_format($we_owe_supplier_normal, false),'已收貨、未付款(非寄賣)'),
                    array('Inventory (Consignment)','supp','wosc',price_format($we_owe_supplier_consignment, false),'已收貨、未付款(寄賣)'),
                )
            );
            $smarty->assign('full_page',        1);
            $smarty->assign('overview',         1);
            $smarty->assign('table',            $table);
        }else if($_REQUEST['act'] == 'info'){
            if($_REQUEST['item'] == "sou"){
                $sql = "SELECT eo.order_id, eo.order_sn, GROUP_CONCAT(eoi.goods_id) AS goods, es.name, " .
                        "SUM((eoi.order_qty - eoi.warehousing_qty) * price) AS cost FROM " . $ecs->table('erp_order_item') . " eoi " .
                        "LEFT JOIN " . $ecs->table('erp_order') . " eo ON eo.order_id = eoi.order_id " .
                        "LEFT JOIN " . $ecs->table('erp_supplier') . " es ON eo.supplier_id = es.supplier_id " .
                        "WHERE eoi.order_id = eo.order_id AND eoi.warehousing_qty < eoi.order_qty AND eo.order_status = 4 AND eo.fully_paid = 1 GROUP BY eo.order_id ORDER BY cost DESC";
                $res = $db->getAll($sql);
                $sou = PODataProcess($res);
                $sql = "SELECT eo.supplier_id, es.name, GROUP_CONCAT(eoi.goods_id) AS goods, COUNT(DISTINCT eo.order_id) AS count2, SUM(eoi.order_qty - eoi.warehousing_qty) AS count FROM " . $ecs->table('erp_order_item') . " eoi " .
                        "LEFT JOIN " . $ecs->table('erp_order') . " eo ON eo.order_id = eoi.order_id " .
                        "LEFT JOIN " . $ecs->table('erp_supplier') . " es ON es.supplier_id = eo.supplier_id " .
                        "WHERE eoi.order_id = eo.order_id AND eoi.warehousing_qty < eoi.order_qty AND eo.order_status = 4 AND eo.fully_paid = 1 AND eo.order_sn NOT LIKE '%PO14%' AND eo.order_sn NOT LIKE '%PO13%' AND price > 0 GROUP BY eo.supplier_id ORDER BY count DESC";
                $res = $db->getAll($sql);
                $sou_s = SODataProcess($res);
                $smarty->assign('sou',      $sou[0]);
                $smarty->assign('ignore',   $sou[1]);
                $smarty->assign('sou_s',    $sou_s);
            }else if($_REQUEST['item'] == "woc"){
                $sql = "SELECT oi.order_id, oi.order_sn, GROUP_CONCAT(og.goods_id) AS goods, " .
                        "SUM(goods_price * goods_number) AS cost FROM " . $ecs->table('order_info') . " oi " .
                        "LEFT JOIN " . $ecs->table('order_goods') . " og ON og.order_id = oi.order_id " .
                        "WHERE oi.pay_status = 2 AND oi.shipping_status IN (0,4) AND (oi.order_type NOT IN ('ws','sa') OR order_type IS NULL) " .
                        "AND user_id NOT IN(SELECT user_id FROM " . $ecs->table('users') . " WHERE is_wholesale = 1) " .
                        "AND og.rec_id NOT IN(SELECT order_item_id FROM " . $ecs->table('delivery_goods') . ") GROUP BY oi.order_id ORDER BY cost DESC";
                $res = $db->getAll($sql);
                $woc = PODataProcess($res);
                $sql = "SELECT og.goods_id, g.goods_name, COUNT(DISTINCT oi.order_id) AS count2, SUM(og.goods_number) AS count FROM " . $ecs->table('order_info') . " oi " .
                        "LEFT JOIN " . $ecs->table('order_goods') . " og ON og.order_id = oi.order_id " .
                        "LEFT JOIN " . $ecs->table('goods') . " g ON g.goods_id = og.goods_id " .
                        "WHERE oi.pay_status = 2 AND oi.shipping_status IN (0,4) AND (oi.order_type NOT IN ('ws','sa') OR order_type IS NULL) " .
                        "AND user_id NOT IN(SELECT user_id FROM " . $ecs->table('users') . " WHERE is_wholesale = 1) " .
                        "AND og.rec_id NOT IN(SELECT order_item_id FROM " . $ecs->table('delivery_goods') . ") GROUP BY og.goods_id ORDER BY count DESC";
                $res = $db->getAll($sql);
                $woc_p = [];
                foreach($res as $row){
                    $woc_p[] = array(
                        'goods_id'      => $row['goods_id'],
                        'goods_name'    => $row['goods_name'],
                        'count'         => $row['count'],
                        'count2'        => $row['count2']
                    );
                }
                $smarty->assign('woc',          $woc[0]);
                $smarty->assign('ignore',       $woc[1]);
                $smarty->assign('woc_p',        $woc_p);
            }else if($_REQUEST['item'] == "wosn"){
                $sql = "SELECT eo.order_id, eo.order_sn, GROUP_CONCAT(eoi.goods_id) AS goods, es.name, " .
                        "SUM(eoi.warehousing_qty * price) AS cost FROM " . $ecs->table('erp_order_item') . " eoi " .
                        "LEFT JOIN " . $ecs->table('erp_order') . " eo ON eo.order_id = eoi.order_id " .
                        "LEFT JOIN " . $ecs->table('erp_supplier') . " es ON eo.supplier_id = es.supplier_id " .
                        "WHERE eoi.order_id = eo.order_id AND eoi.warehousing_qty > 0 AND eo.order_status = 4 AND eo.fully_paid != 1 AND type IN(0,1) " .
                        "GROUP BY eo.order_id ORDER BY cost DESC";
                $res = $db->getAll($sql);
                $wosn = PODataProcess($res);
                $sql = "SELECT eo.supplier_id, es.name, GROUP_CONCAT(eoi.goods_id) AS goods, COUNT(DISTINCT eo.order_id) AS count2, SUM(eoi.warehousing_qty) AS count FROM " . $ecs->table('erp_order_item') . " eoi " .
                        "LEFT JOIN " . $ecs->table('erp_order') . " eo ON eo.order_id = eoi.order_id " .
                        "LEFT JOIN " . $ecs->table('erp_supplier') . " es ON es.supplier_id = eo.supplier_id " .
                        "WHERE eoi.order_id = eo.order_id AND eoi.warehousing_qty > 0 AND eo.order_status = 4 AND eo.fully_paid != 1 AND type IN (0,1) AND eo.order_sn NOT LIKE '%PO14%' AND eo.order_sn NOT LIKE '%PO13%' AND price > 0 " .
                        "GROUP BY eo.supplier_id ORDER BY count DESC";
                $res = $db->getAll($sql);
                $wosn_s = SODataProcess($res);
                $smarty->assign('wosn',     $wosn[0]);
                $smarty->assign('ignore',   $wosn[1]);
                $smarty->assign('wosn_s',   $wosn_s);
            } else if($_REQUEST['item'] == "wosc"){
                $sql = "SELECT eo.order_id, eo.order_sn, GROUP_CONCAT(eoi.goods_id) AS goods, es.name, " .
                        "SUM(eoi.warehousing_qty * price) AS cost FROM " . $ecs->table('erp_order_item') . " eoi " .
                        "LEFT JOIN " . $ecs->table('erp_order') . " eo ON eo.order_id = eoi.order_id " .
                        "LEFT JOIN " . $ecs->table('erp_supplier') . " es ON eo.supplier_id = es.supplier_id " .
                        "WHERE eoi.order_id = eo.order_id AND eoi.warehousing_qty > 0 AND eo.order_status = 4 AND eo.fully_paid != 1 AND type = 2 " .
                        "GROUP BY eo.order_id ORDER BY cost DESC";
                $res = $db->getAll($sql);
                $wosc = PODataProcess($res);
                $sql = "SELECT eo.supplier_id, es.name, GROUP_CONCAT(eoi.goods_id) AS goods, COUNT(DISTINCT eo.order_id) AS count2, SUM(eoi.warehousing_qty) AS count FROM " . $ecs->table('erp_order_item') . " eoi " .
                        "LEFT JOIN " . $ecs->table('erp_order') . " eo ON eo.order_id = eoi.order_id " .
                        "LEFT JOIN " . $ecs->table('erp_supplier') . " es ON es.supplier_id = eo.supplier_id " .
                        "WHERE eoi.order_id = eo.order_id AND eoi.warehousing_qty > 0 AND eo.order_status = 4 AND eo.fully_paid != 1 AND type = 2 AND eo.order_sn NOT LIKE '%PO14%' AND eo.order_sn NOT LIKE '%PO13%' AND price > 0 " .
                        "GROUP BY eo.supplier_id ORDER BY count DESC";
                $res = $db->getAll($sql);
                $wosc_s = SODataProcess($res);
                $smarty->assign('wosc',     $wosc[0]);
                $smarty->assign('ignore',   $wosc[1]);
                $smarty->assign('wosc_s',   $wosc_s);
            } else if($_REQUEST['item'] == "wsou"){
                $sql = "SELECT oi.order_id, oi.order_sn, " .
                        "CASE WHEN shipping_status = 2 THEN order_amount - actg_paid_amount ELSE SUM(goods_price * goods_number - goods_price * delivery_qty) END AS cost, " .
                        "GROUP_CONCAT(og.goods_id) AS goods FROM " . $ecs->table('order_info') . " oi " .
                        "LEFT JOIN " . $ecs->table('order_goods') . " og ON og.order_id = oi.order_id " .
                        "WHERE oi.pay_status != 2 AND oi.shipping_status IN (2,4) AND (oi.order_type = 'ws' " .
                        "OR user_id IN(SELECT user_id FROM " . $ecs->table('users') . " WHERE is_wholesale = 1)) " .
                        "AND delivery_qty > 0 GROUP BY oi.order_id ORDER BY cost DESC";
                $res = $db->getAll($sql);
                $wsou = PODataProcess($res);
                $sql = "SELECT u.user_id, u.user_name, COUNT(DISTINCT oi.order_id) AS count FROM " . $ecs->table('order_info') . " oi " .
                        "LEFT JOIN " . $ecs->table('order_goods') . " og ON og.order_id = oi.order_id " .
                        "LEFT JOIN " . $ecs->table('users') . " u ON u.user_id = oi.user_id " .
                        "WHERE oi.pay_status != 2 AND oi.shipping_status IN (2,4) AND (oi.order_type = 'ws' " .
                        "OR u.user_id IN(SELECT user_id FROM " . $ecs->table('users') . " WHERE is_wholesale = 1)) " .
                        "AND delivery_qty > 0 AND order_amount > 0 GROUP BY u.user_id ORDER BY count DESC";
                $res = $db->getAll($sql);
                $wsou_c = UODataProcess($res);
                $smarty->assign('wsou',     $wsou[0]);
                $smarty->assign('ignore',   $wsou[1]);
                $smarty->assign('wsou_c',   $wsou_c);
            } else if($_REQUEST['item'] == "wows"){
                $sql = "SELECT oi.order_id, oi.order_sn, " .
                        "CASE WHEN shipping_status = 0 THEN order_amount ELSE SUM(goods_price * goods_number - goods_price * delivery_qty) END AS cost, " .
                        "GROUP_CONCAT(og.goods_id) AS goods FROM " . $ecs->table('order_info') . " oi " .
                        "LEFT JOIN " . $ecs->table('order_goods') . " og ON og.order_id = oi.order_id " .
                        "WHERE oi.pay_status = 2 AND oi.shipping_status IN (0,4) AND (oi.order_type = 'ws' " .
                        "OR user_id IN(SELECT user_id FROM " . $ecs->table('users') . " WHERE is_wholesale = 1)) " .
                        "AND delivery_qty < goods_number GROUP BY oi.order_id ORDER BY cost DESC";
                $res = $db->getAll($sql);
                $wows = PODataProcess($res);
                $sql = "SELECT u.user_id, u.user_name, COUNT(DISTINCT oi.order_id) AS count FROM " . $ecs->table('order_info') . " oi " .
                        "LEFT JOIN " . $ecs->table('order_goods') . " og ON og.order_id = oi.order_id " .
                        "LEFT JOIN " . $ecs->table('users') . " u ON u.user_id = oi.user_id " .
                        "WHERE oi.pay_status = 2 AND oi.shipping_status IN (0,4) AND (oi.order_type = 'ws' " .
                        "OR u.user_id IN(SELECT user_id FROM " . $ecs->table('users') . " WHERE is_wholesale = 1)) " .
                        "AND delivery_qty < goods_number AND order_amount > 0 GROUP BY u.user_id ORDER BY count DESC";
                $res = $db->getAll($sql);
                $wows_c = UODataProcess($res);
                $smarty->assign('wows',     $wows[0]);
                $smarty->assign('ignore',   $wows[1]);
                $smarty->assign('wows_c',   $wows_c);
            } else if($_REQUEST['item'] == "ref"){
                $sql = "SELECT SUM(refund_amount) total_refund_amount, SUM(integral_amount) total_integral_amount, IFNULL(NULLIF(rr.order_sn,''), oi.order_sn) AS order_sn FROM " . $ecs->table('refund_requests') . " rr " .
                        "LEFT JOIN " . $ecs->table('order_info') . " oi ON oi.order_id = rr.order_id " .
                        " WHERE status " . db_create_in(array(REFUND_PENDING, REFUND_PROCESS)) . " AND refund_type != " . REFUND_COMPENSATE . " GROUP BY rr.order_id";
                $res = $db->getAll($sql);
                $smarty->assign('ref', $res);
            }
            $smarty->assign('type', $_REQUEST['item']);
            $smarty->assign('action_link',  array('text' => '返回 ' . $_LANG['balance_sheet'], 'href' => 'balance_sheet.php?act=list'));
            $smarty->assign('full_page',1);
            $smarty->assign('overview',0);
        }
        $smarty->assign('ur_here',  $_LANG['balance_sheet' . (isset($_REQUEST['item']) ? '_' . $_REQUEST['item'] : '')]);
        assign_query_info();
        $smarty->display('balance_sheet.htm');
    }
    function PODataProcess($res){
        $data = [];
        $ignore = 0;
        foreach($res as $row){
            if($row['cost'] > 0 && strpos($row['order_sn'], "PO14") === false && strpos($row['order_sn'], "PO13") === false){
                $data[] = array(
                    'order_id'  => $row['order_id'],
                    'order_sn'  => $row['order_sn'],
                    'name'      => $row['name'],
                    'goods'     => explode(",",$row['goods']),
                    'cost'      => price_format($row['cost'], false)
                );
            } else {
                $ignore++;
            }
        }
        return array($data,$ignore);
    }
    function SODataProcess($res){
        $data = [];
        foreach($res as $row){
            $data[] = array(
                'supplier_id'   => $row['supplier_id'],
                'name'          => $row['name'],
                'count'         => $row['count'],
                'count2'        => $row['count2']
            );
        }
        return $data;
    }
    function UODataProcess($res){
        $data = [];
        foreach($res as $row){
            $data[] = array(
                'user_id'       => $row['user_id'],
                'name'          => $row['user_name'],
                'count'         => $row['count']
            );
        }
        return $data;
    }
    function emptyRow($num,$class){
        $html = "";
        for($i = 0; $i < $num; $i++)
            $html .= '<tr><td class="' . $class . ' blank"></td><td class="' . $class . ' blank"></td></tr>';
        return $html;
    }
?>