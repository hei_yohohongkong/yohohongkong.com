<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_base.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_warehouse.php');
require(dirname(__FILE__) .'/includes/ERP/cls/cls_date.php');
if($_REQUEST['act'] == 'list'||$_REQUEST['act'] == 'print'||$_REQUEST['act'] == 'export')
{
	if(admin_priv('erp_warehouse_view','',false) ||admin_priv('erp_warehouse_manage','',false) ||admin_priv('erp_warehouse_approve','',false))
	{
		$page=isset($_REQUEST['page']) &&intval($_REQUEST['page']) >0 ?intval($_REQUEST['page']):1;
		$goods_sn=isset($_REQUEST['goods_sn'])?trim($_REQUEST['goods_sn']):'';
		$goods_name=isset($_REQUEST['goods_name'])?trim($_REQUEST['goods_name']):'';
		$goods_barcode=isset($_REQUEST['goods_barcode'])?trim($_REQUEST['goods_barcode']):'';
		$stock_style=isset($_REQUEST['s_s']) ?trim($_REQUEST['s_s']) : 'a';
		$warehousing_delivery_style_id=isset($_REQUEST['w_d_s_id']) &&intval($_REQUEST['w_d_s_id'])>0 ?intval($_REQUEST['w_d_s_id']):0;
		$warehouse_id=isset($_REQUEST['w_id']) &&intval($_REQUEST['w_id'])>0?intval($_REQUEST['w_id']):0;
		$start_date=isset($_REQUEST['s_date'])?trim($_REQUEST['s_date']):'';
		$end_date=isset($_REQUEST['e_date'])?trim($_REQUEST['e_date']):'';
		$supplier_id=isset($_REQUEST['s_id']) &&intval($_REQUEST['s_id']) >0 ?intval($_REQUEST['s_id']):0;
		$brand_id=isset($_REQUEST['b_id']) &&intval($_REQUEST['b_id']) >0 ?intval($_REQUEST['b_id']):0;
		include('./includes/ERP/page.class.php');
		$num_per_page=!empty($_COOKIE['ECSCP']['page_size']) ? intval($_COOKIE['ECSCP']['page_size']) : 20;
		$mode=1;
		$page_bar_num=6;
		$page_style="page_style";
		$current_page_style="current_page_style";
		$start=$num_per_page*($page-1);
		$cls_date=new cls_date();
		$start_time=!empty($start_date)?strtotime($start_date . ' 00:00:00'): 0;
		$end_time=!empty($end_date)?strtotime($end_date . ' 23:59:59'):0;
		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		$paras=array(
			'goods_sn'=>$goods_sn,
			'goods_name'=>$goods_name,
			'goods_barcode'=>$goods_barcode,
			'stock_style'=>$stock_style,
			'start_time'=>$start_time,
			'end_time'=>$end_time,
			'warehouse_id'=>$warehouse_id,
			'w_d_style_id'=>$warehousing_delivery_style_id,
			'agency_id'=>$agency_id,
			'supplier_id'=>$supplier_id,
			'brand_id'=>$brand_id
		);
		$total_num=get_stock_record_count($paras);
		if($_REQUEST['act'] == 'list'||$_REQUEST['act'] == 'print')
		{
			$stock_record=get_stock_record($paras,$start,$num_per_page);
		}
		else
		{
			$stock_record=get_stock_record($paras);
		}
		$smarty->assign('stock_record',$stock_record);
		$url=fix_url($_SERVER['REQUEST_URI'],array(
			'page'=>'',
			'goods_sn'=>$goods_sn,
			'goods_name'=>$goods_name,
			's_s'=>$stock_style,
			'w_id'=>$warehouse_id,
			'w_d_s_id'=>$warehousing_delivery_style_id,
			'start_date'=>$start_date,
			'end_date'=>$end_date,
			'supplier_id'=>$supplier_id,
			'brand_id'=>$brand_id
		));
		$pager=new page(array('total_data'=>$total_num,'data_per_page'=>$num_per_page,'url'=>$url,'mode'=>$mode,'page_bar_num'=>$page_bar_num,'page_style'=>$page_style,'current_page_style'=>$current_page_style));
		$smarty->assign('pager',$pager->show());
		$smarty->assign('goods_sn',$goods_sn);
		$smarty->assign('goods_name',$goods_name);
		$smarty->assign('goods_barcode',$goods_barcode);
		$smarty->assign('stock_style',$stock_style);
		$smarty->assign('warehousing_delivery_style_id',$warehousing_delivery_style_id);
		$smarty->assign('warehouse_id',$warehouse_id);
		$smarty->assign('start_date',$start_date);
		$smarty->assign('end_date',$end_date);
		$smarty->assign('supplier_id',$supplier_id);
		$smarty->assign('brand_id',$brand_id);
		$smarty->assign('page',$page);
		$warehouse_list=get_warehouse_list(0,$agency_id,1);
		$smarty->assign('warehouse_list',$warehouse_list);
		$smarty->assign('ur_here',$_LANG['erp_goods_warehousing_and_delivery_record']);
		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		//$smarty->assign('supplier_list',get_agency_suppliers($agency_id));
		$smarty->assign('supplier_list',suppliers_list_name_by_admin());
		$smarty->assign('brand_list',brand_list());
		if($_REQUEST['from_abnormal_goods_id'])
			$smarty->assign('action_link', array('href' => 'erp_stock_abnormal.php?act=detail&goods_id='.intval($_REQUEST['from_abnormal_goods_id']), 'text' => $_LANG['back']));
		if($_REQUEST['act'] == 'list'||$_REQUEST['act'] == 'print')
		{
			assign_query_info();
			$template=$_REQUEST['act'] == 'list'?'erp_stock_record.htm': 'erp_stock_record_print.htm';
			$smarty->display($template);
		}
		else
		{
			require('./includes/ERP/cls/cls_csv.php');
			require('./includes/ERP/cls/cls_file.php');
			include('../includes/cls_json.php');
			$export=array();
			$row['record_id']=$_LANG['erp_stock_record_id'];
			$row['goods_sn']=$_LANG['erp_stock_record_goods_sn'];
			$row['goods_name']=$_LANG['erp_stock_record_goods_name'];
			$row['goods_barcode']=$_LANG['erp_stock_record_goods_barcode'];
			$row['goods_attr']=$_LANG['erp_stock_record_goods_attr'];
			$row['warehouse_name']=$_LANG['erp_stock_record_warehouse_name'];
			$row['agency_name']=$_LANG['erp_stock_record_agency'];
			$row['w_d_style']=$_LANG['erp_stock_record_w_d_style'];
			$row['w_d_sn']=$_LANG['erp_stock_record_w_d_sn'];
			$row['order_sn']=$_LANG['erp_stock_record_order_sn'];
			$row['w_d_name']=$_LANG['erp_stock_record_w_d_name'];
			$row['record_date']=$_LANG['erp_stock_record_date'];
			$row['w_d_qty']=$_LANG['erp_stock_record_w_d_qty'];
			$row['stock']=$_LANG['erp_stock_record_stock'];
			$export[]=$row;
			if(!empty($stock_record))
			{
				foreach($stock_record as $record)
				{
					$row['record_id']=$record['stock_id'];
					$row['goods_sn']=$record['goods_info']['goods_sn'];
					$row['goods_name']=$record['goods_info']['goods_name'];
					$row['goods_barcode']=$record['barcode'];
					if(!empty($record['goods_attr']))
					{
						$attrs='';
						if(isset($record['goods_attr']['attr_info']) &&is_array($record['goods_attr']['attr_info']))
						{
							foreach($record['goods_attr']['attr_info'] as $attr)
							{
								$attrs=$attrs.' '.$attr['attr_value'];
							}
						}
						$row['goods_attr']=$attrs;
					}
					else
					{
						$row['goods_attr']='$$';
					}
					$row['warehouse_name']=$record['warehouse_name'];
					$row['agency_name']=$record['agency_name'];
					$row['w_d_style']=$record['w_d_style'];
					$row['w_d_sn']=$record['w_d_sn'];
					$row['order_sn']=$record['order_sn'];
					$row['w_d_name']=$record['w_d_name'];
					$row['record_date']=$record['act_date'];
					$row['w_d_qty']=$record['qty'];
					$row['stock']=$record['stock'];
					$export[]=$row;
				}
			}
			$csv=new cls_csv();
			$file_stream=$csv->encode($export);
			$file_stream=iconv('utf-8','gb2312',$file_stream);
			$file=new cls_file();
			$path=ROOT_PATH.'data/stock_record/';
			$random_name=random_string('numeric',8);
			$file_name=local_date('Y-m-d').'-'.$random_name.'.csv';
			while(file_exists($path.$file_name))
			{
				$random_name=random_string('numeric',8);
				$file_name=local_date('Y-m-d').'-'.$random_name.'.csv';
			}
			$file->write_file($path,$file_name,$file_stream,$op='rewrite');
			$json  = new JSON;
			$server_address=$_SERVER["SERVER_NAME"];
			$phpself=$_SERVER["PHP_SELF"];
			$port=$_SERVER["SERVER_PORT"];
			$path=str_replace(ADMIN_PATH.'/erp_stock_record.php','',$phpself);
			$base_url=$server_address.':'.$port.$path;
			$url='http://'.$base_url.'data/stock_record/'.$file_name;
			$result['error']=0;
			$result['url']=$url;
			die($json->encode($result));
		}
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
if ($_REQUEST['act'] == 'get_warehousing_style')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_warehouse_view','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$sql="select warehousing_style_id,warehousing_style from ".$GLOBALS['ecs']->table('erp_warehousing_style')." where is_valid='1'";
	$warehousing_style=$GLOBALS['db']->getAll($sql);
	$result['error']=0;
	$result['warehousing_style']=$warehousing_style;
	die($json->encode($result));
}
elseif ($_REQUEST['act'] == 'get_delivery_style')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_warehouse_view','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$sql="select delivery_style_id,delivery_style  from ".$GLOBALS['ecs']->table('erp_delivery_style')." where is_valid='1'";
	$delivery_style=$GLOBALS['db']->getAll($sql);
	$result['error']=0;
	$result['delivery_style']=$delivery_style;
	die($json->encode($result));
}
elseif ($_REQUEST['act'] == 'download') {
	ini_set('memory_limit', '256M');
	define('PCLZIP_TEMPORARY_DIR', ROOT_PATH . 'temp/');
	require_once ROOT_PATH . 'includes/excel/PHPExcel.php';
	require_once ROOT_PATH . 'includes/excel/PHPExcel/IOFactory.php';
	\PHPExcel_Settings::setZipClass(\PHPExcel_Settings::PCLZIP);
	$objPHPExcel = new \PHPExcel();

	$sql = "SELECT brand_id, brand_name FROM " . $ecs->table("brand");
	$brands = $db->getAll($sql);
	$export[] = [
		'品牌',
		'產品ID',
		'產品sn',
		'產品名稱',
		'入貨量',
		'出貨量',
		'截至' . date("Y") . '-03-31 數量'
	];
	foreach ($brands as $brand) {
		$export[] = [$brand['brand_name']];
		$start_time = strtotime("first day of april last year");
		$end_time = strtotime("first day of april this year");
		$sql = "SELECT goods_id, goods_sn, goods_name, SUM(CASE WHEN stock_style = 'w' AND w_d_style_id = 2 THEN qty WHEN stock_style = 'w' THEN 0 ELSE qty END) as qty_out, SUM(CASE WHEN stock_style = 'w' AND w_d_style_id = 2 THEN 0 WHEN stock_style = 'w' THEN qty ELSE 0 END) as qty_in FROM (SELECT es.qty, es.stock_style, es.w_d_style_id, g.brand_id, g.goods_id, g.goods_name, g.goods_sn FROM `ecs_erp_stock` as es LEFT JOIN `ecs_goods` as g ON g.goods_id = es.goods_id LEFT JOIN `ecs_erp_warehouse` as ew ON ew.warehouse_id = es.warehouse_id WHERE 1 AND g.brand_id = $brand[brand_id] AND es.act_time >= $start_time AND es.act_time < $end_time AND g.is_delete='0') a GROUP BY goods_id ORDER BY goods_id ASC";
		$goods = $db->getAll($sql);
		foreach ($goods as $good) {
			$export[] = [
				"",
				"$good[goods_id]",
				"$good[goods_sn]",
				$good['goods_name'],
				$good['qty_in'] == 0 ? "0" : intval($good['qty_in']),
				$good['qty_out'] == 0 ? "0" : intval($good['qty_out']) * -1,
				$db->getOne("SELECT stock FROM `ecs_erp_stock` WHERE goods_id = $good[goods_id] AND act_time < $end_time ORDER BY act_time DESC LIMIT 1")
			];
		}
		if (empty($goods)) {
			$export[] = [];
		}
	}
	$filename = '出貨數量紀錄.xlsx';
	$objPHPExcel->setActiveSheetIndex()->fromArray($export,null,'A1');
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.‌​sheet; charset=utf-8');
	header('Content-Disposition: attachment; filename=' . $filename);
	header('Cache-Control: max-age=0');
	$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
}
?>