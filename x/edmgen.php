<?php

/***
* edmgen.php
* by howang 2015-04-10
*
* Generate HTML code for EDM
***/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

if (empty($_REQUEST['act']))
{
    $_REQUEST['act'] = 'main';
}

if ($_REQUEST['act'] == 'main')
{
    // /* 权限判断 */
    // if (!$_SESSION['manage_cost'])
    // {
    //     sys_msg($GLOBALS['_LANG']['priv_error']);
    // }
    
    if (!empty($_REQUEST['header']))
    {
        $header = $_REQUEST['header'];
    }
    else
    {
        $header = array(
            'url' => 'http://www.yohohongkong.com/keyword/lg+g3',
            'img' => 'http://www.yohohongkong.com/images/edm/lgg3.png',
        );
    }
    
    if (!empty($_REQUEST['block1']))
    {
        $block1 = $_REQUEST['block1'];
    }
    else
    {
        $block1 = array(
            'url' => 'http://www.yohohongkong.com/keyword/lg+g3',
            'title' => 'LG G3 旗艦手機',
            'content' => '5.5吋2560x1440屏幕，S801 2.5GHz，3GB RAM，支援128GB TF，1300萬像主鏡頭，210萬像前鏡頭，3000mAh電量，重149g，支援 Knock Code，支援LTE。',
        );
    }
    
    $row_count = empty($_REQUEST['row_count']) ? 0 : intval($_REQUEST['row_count']);
    
    $default_row = array(
        'left_url' => 'http://www.yohohongkong.com/product/1154-Nikon-DSLR-D3300-%E9%80%A3AF-S-DX-NIKKOR-18-55mm-f-3.5-5.6G-VRII%E9%8F%A1%E9%A0%AD%E5%A5%97%E8%A3%9D-%E9%A6%99%E6%B8%AF%E8%A1%8C%E8%B2%A8',
        'left_img' => 'http://www.yohohongkong.com/images/201407/goods_img/1153_P_1406314505461.png',
        'left_title' => 'Nikon DSLR D3300 鏡頭套裝 香港行貨',
        'left_content' => '2420 萬像 CMOS、不設 Low-Pass Filter、1080p 全高清電影、Guide Mode 引導模式、EXPEED 4 影像處理引擎。',
        'right_url' => 'http://www.yohohongkong.com/product/1133-Panasonic-EW-DE43-%E8%81%B2%E6%B3%A2%E9%9C%87%E5%8B%95%E9%9B%A2%E5%AD%90%E7%89%99%E5%88%B7',
        'right_img' => 'http://www.yohohongkong.com/images/201407/goods_img/1133_P_1406172122077.png',
        'right_title' => 'Panasonic EW-DE43 聲波震動離子牙刷',
        'right_content' => '最新Panasonic EW-DE43聲波震動離子牙刷採用聲波加離子複合式設計，高速震頻的同時利用刷頭在口腔中釋放負電荷離子，導致牙菌斑與牙齒咬合力減弱，從而有效的去除頑固牙垢，刷得乾淨的同時不傷牙齒。',
    );
    
    if (!empty($_REQUEST['rows']) && is_array($_REQUEST['rows']))
    {
        $rows = $_REQUEST['rows'];
        $now_count = count($rows);
        if ($row_count == 0)
        {
            $row_count = $now_count;
        }
        elseif ($now_count < $row_count)
        {
            $rows = array_merge($rows, array_fill(0, $row_count - $now_count, $default_row));
        }
        elseif ($now_count > $row_count)
        {
            $rows = array_slice($rows, 0, $row_count);
        }
    }
    else
    {
        $row_count = empty($row_count) ? 2 : $row_count;
        $rows = array_fill(0, $row_count, $default_row);
    }
    
    
    $smarty->assign('header', $header);
    $smarty->assign('block1', $block1);
    $smarty->assign('rows', $rows);
    $smarty->assign('rows_count', $row_count);
    
    $smarty->assign('ur_here',      '促銷郵件產生器');
    
    assign_query_info();
    $smarty->display('edmgen.htm');
}
elseif ($_REQUEST['act'] == 'gen')
{
    foreach ($_REQUEST as $key => $val)
    {
        if (in_array($key, array('mode', 'header', 'block1', 'rows')))
        {
            $smarty->assign($key, stripslashes_deep($val));
        }
    }
    
    $smarty->assign('generated', $smarty->fetch('edmgen_template.htm'));
    
    $smarty->display('edmgen_output.htm');
}
elseif ($_REQUEST['act'] == 'search_goods')
{
    $keyword = empty($_REQUEST['keyword']) ? '' : trim($_REQUEST['keyword']);
    
    $filter = (object) array('keyword' => $keyword);
    
    $where = get_where_sql($filter); // 取得过滤条件
    
    /* 取得数据 */
    $sql = 'SELECT goods_id, goods_sn, goods_name, goods_img, goods_brief '.
           'FROM ' . $ecs->table('goods') . ' AS g ' . $where .
           'ORDER BY goods_name';
    $rows = $db->getAll($sql);
    
    $site_url = $ecs->url();
    foreach ($rows as $key => $row)
    {
        $rows[$key]['goods_img'] = $site_url . $row['goods_img'];
        $rows[$key]['goods_url'] = substr($site_url,0,-1) . build_uri('goods', array('gid' => $row['goods_id']));
    }
    
    make_json_result($rows);
}
?>