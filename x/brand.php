<?php

/**
 * ECSHOP 管理中心品牌管理
 * ============================================================================
 * 版权所有 2005-2010 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liuhui $
 * $Id: brand.php 17063 2010-03-25 06:35:46Z liuhui $
*/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
include_once(ROOT_PATH . 'includes/cls_image.php');
$image = new cls_image($_CFG['bgcolor']);
$brandController = new Yoho\cms\Controller\BrandController();
$exc = new exchange($ecs->table("brand"), $db, 'brand_id', 'brand_name');

/*------------------------------------------------------ */
//-- 品牌列表
/*------------------------------------------------------ */
if ($_REQUEST['act'] == 'list')
{
    admin_priv('brand_manage');

    $smarty->assign('ur_here',      $_LANG['06_goods_brand_list']);
    $smarty->assign('action_link',  array('text' => $_LANG['07_brand_add'], 'href' => 'brand.php?act=add'));
    $smarty->assign('full_page',    1);

    $smarty->assign('filter',       $brand_list['filter']);
    $smarty->assign('record_count', $brand_list['record_count']);
    $smarty->assign('page_count',   $brand_list['page_count']);

    assign_query_info();
    $smarty->display('brand_list.htm');
}

/*------------------------------------------------------ */
//-- 添加品牌
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'add' || $_REQUEST['act'] == 'edit')
{   
    
    $function = $_REQUEST['act'].'Action';
    
    if(method_exists($brandController, $function))
    {
        $brandController->$function();
    }
}
elseif ($_REQUEST['act'] == 'insert')
{
    /*检查品牌名是否重复*/
    admin_priv('brand_manage');

    $is_show = isset($_REQUEST['is_show']) ? intval($_REQUEST['is_show']) : 0;
    $is_update = isset($_REQUEST['is_update']) ? intval($_REQUEST['is_update']) : 0;
    $perma_link = isset($_REQUEST['perma_link']) ? trim($_REQUEST['perma_link']) : '';

    $is_only = $exc->is_only('brand_name', $_POST['brand_name']);

    if (!$is_only)
    {
        sys_msg(sprintf($_LANG['brandname_exist'], stripslashes($_POST['brand_name'])), 1);
    }

    if (empty($perma_link)) {
       sys_msg("請輸入永久鏈結", 1);
    }

    /*对描述处理*/
    if (!empty($_POST['brand_desc']))
    {
        $_POST['brand_desc'] = $_POST['brand_desc'];
    }

     /*处理图片*/
    // if (empty($_FILES['brand_logo']['tmp_name'])) {
    //     sys_msg('請上傳品牌LOGO', 1);
    // }
    if (!empty($_FILES['brand_logo']['tmp_name'])) {
        list($width, $height) = getimagesize($_FILES['brand_logo']['tmp_name']);

        if ($width != 200 || $height != 100 ){
            sys_msg('請檢查品牌LOGO大小(200px * 100px)', 1);
        }
    }
    $img_name = basename($image->upload_image($_FILES['brand_logo'],'brandlogo'));
    $banner_name = basename($image->upload_image($_FILES['brand_banner'],'brandbanner'));

     /*处理URL*/
    $site_url = sanitize_url( $_POST['site_url'] );

    // handle simplified chinese
    $localizable_fields = [
        "brand_name"    => $_POST["brand_name"],
        "brand_desc"    => $_POST['brand_desc'],
        "perma_link"    => empty($_POST["perma_link_raw"]) ? $_POST['brand_name'] : $_POST["perma_link_raw"],
    ];
    to_simplified_chinese($localizable_fields);

    /*插入数据*/

    $sql = "INSERT INTO ".$ecs->table('brand')."(brand_name, site_url, brand_desc, brand_logo, is_show, is_update, sort_order, brand_banner, perma_link) ".
           "VALUES ('$_POST[brand_name]', '$site_url', '$_POST[brand_desc]', '$img_name', '$is_show', '$is_update','$_POST[sort_order]','$banner_name', '$perma_link')";
    $db->query($sql);
    $brand_id = $db->insert_id();

    // Multiple language support
    save_localized_versions('brand', $brand_id);

    admin_log($_POST['brand_name'],'add','brand');

    /* 清除缓存 */
    clear_cache_files();

    $link[0]['text'] = $_LANG['continue_add'];
    $link[0]['href'] = 'brand.php?act=add';

    $link[1]['text'] = $_LANG['back_list'];
    $link[1]['href'] = 'brand.php?act=list';

    sys_msg($_LANG['brandadd_succed'], 0, $link);
}

elseif ($_REQUEST['act'] == 'update')
{
    admin_priv('brand_manage');
    if ($_POST['brand_name'] != $_POST['old_brandname'])
    {
        /*检查品牌名是否相同*/
        $is_only = $exc->is_only('brand_name', $_POST['brand_name'], $_POST['id']);

        if (!$is_only)
        {
            sys_msg(sprintf($_LANG['brandname_exist'], stripslashes($_POST['brand_name'])), 1);
        }
    }
    // if (empty($_FILES['brand_logo']['tmp_name']) && empty($db->getOne("SELECT brand_logo FROM " . $ecs->table('brand') . " WHERE brand_id = $_POST[id]"))) {
    //     sys_msg('請上傳品牌LOGO', 1);
    // }
 
    /*对描述处理*/
    if (!empty($_POST['brand_desc']))
    {
        $_POST['brand_desc'] = $_POST['brand_desc'];
    }

    $is_show = isset($_REQUEST['is_show']) ? intval($_REQUEST['is_show']) : 0;
    $is_update = isset($_REQUEST['is_update']) ? intval($_REQUEST['is_update']) : 0;
     /*处理URL*/
    $site_url = sanitize_url( $_POST['site_url'] );

    /* 处理图片 */

    if (!empty($_FILES['brand_logo']['tmp_name'])) {
        list($width, $height) = getimagesize($_FILES['brand_logo']['tmp_name']);
        
        if ($width != 200 || $height != 100 ){
        sys_msg('請檢查品牌LOGO大小(200px * 100px)', 1);
        }
    }

    $img_name = basename($image->upload_image($_FILES['brand_logo'],'brandlogo')); // 200 x 10 pixels
    $banner_name = basename($image->upload_image($_FILES['brand_banner'],'brandbanner'));
    $mobile_banner_name = basename($image->upload_image($_FILES['brand_mobile_banner'],'brandbanner'));
    $param = "brand_name = '$_POST[brand_name]',  site_url='$site_url', brand_desc='$_POST[brand_desc]', brand_keywords='$_POST[brand_keywords]', is_show='$is_show', is_update='$is_update', sort_order='$_POST[sort_order]' ";
    if (!empty($img_name))
    {
        //有图片上传
        $param .= " ,brand_logo = '$img_name' ";
    }

    if (!empty($banner_name))
    {
        //有图片上传
        $param .= " ,brand_banner = '$banner_name' ";
    }

    if (!empty($mobile_banner_name))
    {
        //有图片上传
        $param .= " ,brand_mobile_banner = '$mobile_banner_name' ";
    }

    // handle simplified chinese
    $localizable_fields = [
        "brand_name"    => $_POST["brand_name"],
        "brand_desc"    => $_POST['brand_desc'],
    ];
    $localizable_fields = $brandController->checkRequireSimplied("brand", $localizable_fields, $_POST['id']);
    to_simplified_chinese($localizable_fields);


    if ($exc->edit($param,  $_POST['id']))
    {
        // Multiple language support
        save_localized_versions('brand', $_POST['id']);

        /* 清除缓存 */
        clear_cache_files();

        admin_log($_POST['brand_name'], 'edit', 'brand');

        $link[0]['text'] = $_LANG['back_list'];
        $link[0]['href'] = 'brand.php?act=list&' . list_link_postfix();
        $note = vsprintf($_LANG['brandedit_succed'], $_POST['brand_name']);
        sys_msg($note, 0, $link);
    }
    else
    {
        die($db->error());
    }
}

elseif($_REQUEST['act'] == 'add_brand')
{
    $brand = empty($_REQUEST['brand']) ? '' : json_str_iconv(trim($_REQUEST['brand']));

    if(brand_exists($brand))
    {
        make_json_error($_LANG['brand_name_exist']);
    }
    else
    {
        $sql = "INSERT INTO " . $ecs->table('brand') . "(brand_name)" .
               "VALUES ( '$brand')";

        $db->query($sql);
        $brand_id = $db->insert_id();

        $arr = array("id"=>$brand_id, "brand"=>$brand);

        make_json_result($arr);
    }
}

elseif ($_REQUEST['act'] == 'ajaxEdit') {
    $brandController->ajaxEditAction();
}
/*------------------------------------------------------ */
//-- 删除品牌
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'remove')
{
    check_authz_json('brand_manage');

    $id = intval($_GET['id']);

    /* 删除该品牌的图标 */
    $sql = "SELECT brand_logo FROM " .$ecs->table('brand'). " WHERE brand_id = '$id'";
    $logo_name = $db->getOne($sql);
    if (!empty($logo_name))
    {
        @unlink(ROOT_PATH . DATA_DIR . '/brandlogo/' .$logo_name);
    }

    /* 删除该品牌的图标 */
    $sql = "SELECT brand_banner FROM " .$ecs->table('brand'). " WHERE brand_id = '$id'";
    $banner_name = $db->getOne($sql);
    if (!empty($banner_name))
    {
        @unlink(ROOT_PATH . DATA_DIR . '/brandbanner/' .$banner_name);
    }

    $exc->drop($id);

    /* 更新商品的品牌编号 */
    $sql = "UPDATE " .$ecs->table('goods'). " SET brand_id=0 WHERE brand_id='$id'";
    $db->query($sql);

    make_json_result('brand.php?act=list');
    exit;
}

/*------------------------------------------------------ */
//-- 删除品牌图片
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'drop_logo')
{
    /* 权限判断 */
    admin_priv('brand_manage');
    $brand_id = isset($_GET['id']) ? intval($_GET['id']) : 0;

    /* 取得logo名称 */
    $sql = "SELECT brand_logo FROM " .$ecs->table('brand'). " WHERE brand_id = '$brand_id'";
    $logo_name = $db->getOne($sql);

    if (!empty($logo_name))
    {
        @unlink(ROOT_PATH . DATA_DIR . '/brandlogo/' .$logo_name);
        $sql = "UPDATE " .$ecs->table('brand'). " SET brand_logo = '' WHERE brand_id = '$brand_id'";
        $db->query($sql);
    }
    $link= array(array('text' => $_LANG['brand_edit_lnk'], 'href' => 'brand.php?act=edit&id=' . $brand_id), array('text' => $_LANG['brand_list_lnk'], 'href' => 'brand.php?act=list'));
    sys_msg($_LANG['drop_brand_logo_success'], 0, $link);
}

/*------------------------------------------------------ */
//-- delete banner
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'drop_banner')
{
    /* 权限判断 */
    admin_priv('brand_manage');
    $brand_id = isset($_GET['id']) ? intval($_GET['id']) : 0;

    /* 取得banner名称 */
    $sql = "SELECT brand_banner FROM " .$ecs->table('brand'). " WHERE brand_id = '$brand_id'";
    $banner_name = $db->getOne($sql);

    if (!empty($banner_name))
    {
        @unlink(ROOT_PATH . DATA_DIR . '/brandbanner/' .$banner_name);
        $sql = "UPDATE " .$ecs->table('brand'). " SET brand_banner = '' WHERE brand_id = '$brand_id'";
        $db->query($sql);
    }
    $link= array(array('text' => $_LANG['brand_edit_lnk'], 'href' => 'brand.php?act=edit&id=' . $brand_id), array('text' => $_LANG['brand_list_lnk'], 'href' => 'brand.php?act=list'));
    sys_msg($_LANG['drop_brand_banner_success'], 0, $link);
}

/*------------------------------------------------------ */
//-- 排序、分页、查询
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'query')
{
    $brandController->ajaxQueryAction();
}

if ($_REQUEST['act'] == 'update_one_time') {
    $sql = "SELECT brand_id, brand_name FROM " . $GLOBALS['ecs']->table('brand');
    $list = $db->getAll($sql);
    foreach ($list as $key => $row) {
        $brand_id = $row["brand_id"];
        $brand_name = $row["brand_name"];
        $brand_desc_zh_tw = "購買 " . $brand_name . "  - " . $brand_name . "香港代理零售經銷" ;
        $sql = "UPDATE " . $ecs->table('brand') .
            " SET brand_desc = '" . mysql_escape_string($brand_desc_zh_tw) . "'" .
            " WHERE brand_id = '" . $brand_id . "'";        
        $db->query($sql);
    }

    $lang_sql = "SELECT brand_id, brand_name, lang FROM " . $GLOBALS['ecs']->table('brand_lang');
    $lang_list = $db->getAll($lang_sql);
    foreach ($lang_list as $key => $row) {
        $brand_id = $row["brand_id"];
        $brand_name = $row["brand_name"];
        $lang = $row["lang"];
        if($lang=="zh_cn"){
            $brand_desc_lang = "购买 " . $brand_name . "  -" . $brand_name . "香港代理零售经销" ;
        }
        if($lang=="en_us"){
            $brand_desc_lang = "Shop " . $brand_name . "  - " . $brand_name . " hk distributor reseller" ;
        }
        $sql = "UPDATE " . $ecs->table('brand_lang') .
            " SET brand_desc = '" . mysql_escape_string($brand_desc_lang) . "'" .
            " WHERE brand_id = '" . $brand_id . "' AND lang = '" .$lang."'";        
        $db->query($sql);


    }
    echo("success");
}

?>