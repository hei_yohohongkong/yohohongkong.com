<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_base.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_warehouse.php');
require(dirname(__FILE__) .'/includes/ERP/cls/cls_date.php');
$rmaController = new Yoho\cms\Controller\RmaController();
$adminuserController = new Yoho\cms\Controller\AdminuserController();

if ($_REQUEST['act'] == 'list' || $_REQUEST['act'] == '') {
    $rma_id = $_REQUEST['rma_id'];
    //$rmaController->checkStatusRedirect($stocktake_id,'process');
    $personInChargeoption = $rmaController->personInChargeList();
    //$result = $stocktakeController->detail();
    //$goods_status_options = $stocktakeController->getGoodsStatusOptions('process');
    $rmaBasicInfo =  $rmaController->getRmaInfo($rma_id);
    //$smarty->assign('goods_status_options',$goods_status_options);
    $smarty->assign('person_in_charge_option',$personInChargeoption);
    $smarty->assign('rma',$rmaBasicInfo);
    $smarty->assign('rma_id',$rma_id);
    $smarty->assign('full_page', 1);
    $smarty->assign('cfg_lang', $_CFG['lang']);
    $smarty->assign('lang', $_LANG);
    $smarty->assign('action_link', array('text' => '盤點工作清單', 'href' => 'erp_rma.php'));
    assign_query_info();
    $smarty->display('erp_rma_process.htm');

} else if ($_REQUEST['act'] == 'post_stocktake_basic_info') {
    $result = $stocktakeController->updateStocktakeBasicInfo();
    if($result !== false){
        make_json_result($result);
    }else{
        make_json_error('更新時發生錯誤');
    }
} else if ($_REQUEST['act'] == 'post_import_stocktake_data') {
    $result = $stocktakeController->importStocktakeData();
    if($result !== false){
        make_json_result($result);
    }else{
        //sys_msg("更新工作紀錄時發生錯誤",1);
    }
} else if ($_REQUEST['act'] == 'post_submit_review_request') {
    $result = $stocktakeController->submitReviewRequest();
    if (isset($result['error'])) {
        make_json_error($result['error']);
    }else{
        make_json_result($result);
    }
} elseif ($_REQUEST['act'] == 'query') {
  	$result = $stocktakeController->detail('process');
    $stocktakeController->ajaxQueryAction($result['data'],$result['record_count'],false);
} else if($_REQUEST['act'] == 'list'||$_REQUEST['act'] == 'print'||$_REQUEST['act'] == 'export') {

    include('./includes/ERP/page.class.php');
    $cls_date=new cls_date();
    $num_per_page=30;
    $mode=1;
    $page_bar_num=6;
    $page_style="page_style";
    $current_page_style="current_page_style";
    $page=isset($_REQUEST['page'])?($_REQUEST['page']):1;
    $start=$num_per_page*($page-1);
    $current_date=local_date('Y-m-d',gmtime());
    $first_date=$cls_date->get_first_day($current_date);
    $last_date=$cls_date->get_last_day($current_date);


    $start_date=isset($_REQUEST['s_date'])?($_REQUEST['s_date']):$first_date;
    $end_date=isset($_REQUEST['e_date'])?($_REQUEST['e_date']):$last_date;
    $warehouse_id=isset($_REQUEST['warehouse_id'])?intval($_REQUEST['warehouse_id']):0;
    $goods_sn=isset($_REQUEST['goods_sn'])?trim($_REQUEST['goods_sn']):'';
    $goods_name=isset($_REQUEST['goods_name'])?trim($_REQUEST['goods_name']):'';
    $goods_barcode=isset($_REQUEST['goods_barcode'])?trim($_REQUEST['goods_barcode']):'';
    $supplier_id=isset($_REQUEST['s_id']) &&intval($_REQUEST['s_id']) >0 ?intval($_REQUEST['s_id']):0;
    $brand_id=isset($_REQUEST['b_id']) &&intval($_REQUEST['b_id']) >0 ?intval($_REQUEST['b_id']):0;
    

    $result = $stocktakeController->detail('process',false);

    $smarty->assign('goods_status_options',$result['goods_status_options']);
    $smarty->assign('stocktake_id',$_REQUEST['stocktake_id']);
    $smarty->assign('warehouse_name_list',$result['warehouse_name_list']);
    $smarty->assign('filter', $result['filter']);
    $smarty->assign('record_count', $result['record_count']);
    $smarty->assign('page_count', $result['page_count']);
    $smarty->assign('data', $result['data']);
    $smarty->assign('full_page', 0);
    $smarty->assign('cfg_lang', $_CFG['lang']);
    $smarty->assign('lang', $_LANG);
 
    $stock_check_info=get_stock_check_list($paras,$start,$num_per_page);
   
    $url=fix_url($_SERVER['REQUEST_URI'], array(
    'page'=>'',
    's_date'=>$start_date,
    'e_date'=>$end_date,
    'warehouse_id'=>$warehouse_id,
    'goods_sn'=>$goods_sn,
    'goods_name'=>$goods_name,
    'supplier_id'=>$supplier_id,
    'brand_id'=>$brand_id
    ));
    $pager=new page(array('total_data'=>$total_num,'data_per_page'=>$num_per_page,'url'=>$url,'mode'=>$mode,'page_bar_num'=>$page_bar_num,'page_style'=>$page_style,'current_page_style'=>$current_page_style));
    $smarty->assign('start_date',$start_date);
    $smarty->assign('end_date',$end_date);
    $smarty->assign('warehouse_id',$warehouse_id);
    $smarty->assign('goods_sn',$goods_sn);
    $smarty->assign('goods_name',$goods_name);
    $smarty->assign('goods_barcode',$goods_barcode);
    $smarty->assign('page',$page);
    $smarty->assign('supplier_id',$supplier_id);
    $smarty->assign('brand_id',$brand_id);
    $smarty->assign('pager',$pager->show());
    $smarty->assign('stock_check_info',$stock_check_info);
    $warehouse_list=get_warehouse_list(0,$agency_id,1);
    $smarty->assign('warehouse_list',$warehouse_list);
    $smarty->assign('supplier_list',get_agency_suppliers($agency_id));
    $smarty->assign('brand_list',brand_list());
    $smarty->assign('ur_here',$_LANG['erp_stock_check']);
    
    $template=$_REQUEST['act'] == 'list'?'erp_stock_check.htm': 'erp_stocktake_process_print.htm';
    assign_query_info();
    $smarty->display($template);

}
?>