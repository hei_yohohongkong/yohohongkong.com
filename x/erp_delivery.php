<?php
	define('IN_ECS',true);
	require(dirname(__FILE__) .'/includes/init.php');
	require_once(ROOT_PATH .ADMIN_PATH .'/includes/ERP/lib_erp_common.php');
	require_once(ROOT_PATH .ADMIN_PATH .'/includes/ERP/lib_erp_warehouse.php');
	require_once(ROOT_PATH .ADMIN_PATH .'/includes/ERP/lib_erp_goods_attr.php');
	require_once(ROOT_PATH .ADMIN_PATH .'/includes/ERP/lib_erp_goods.php');
	$erpController = new Yoho\cms\Controller\ErpController();
	if ($_REQUEST['act'] == 'change_warehouse')
	{
		include('../includes/cls_json.php');
		$json  = new JSON;
		if(!admin_priv('erp_warehouse_manage','',false))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_no_permit'];
			die($json->encode($result));
		}
		$delivery_id=isset($_REQUEST['delivery_id']) &&intval($_REQUEST['delivery_id'])>0?intval($_REQUEST['delivery_id']) : 0;
		$warehouse_id=isset($_REQUEST['warehouse_id']) &&intval($_REQUEST['warehouse_id'])>0?intval($_REQUEST['warehouse_id']) : 0;
		if(empty($delivery_id) ||empty($warehouse_id))
		{
			$result['error']=2;
			$result['message']=$_LANG['erp_wrong_parameter'];
			die($json->encode($result));
		}
		$delivery_info=is_delivery_exist($delivery_id);
		if(!$delivery_info)
		{
			$result['error']=3;
			$result['message']=$_LANG['erp_delivery_not_exist'];
			die($json->encode($result));
		}
		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		if(!is_agency_delivery($delivery_id,$agency_id))
		{
			$result['error']=2;
			$result['message']=$_LANG['erp_delivery_no_agency_access'];
			die($json->encode($result));
		}
		if($delivery_info['status']!=1)
		{
			$result['error']=6;
			$result['message']=$_LANG['erp_delivery_no_acts'];
			die($json->encode($result));
		}
		if(!is_admin_delivery($delivery_id ,erp_get_admin_id()))
		{
			$result['error']=5;
			$result['message']=$_LANG['erp_delivery_no_access'];
			die($json->encode($result));
		}
		$warehouse_info=is_warehouse_exist($warehouse_id);
		if(!$warehouse_info)
		{
			$result['error']=4;
			$result['message']=$_LANG['erp_warehouse_not_exist'];
			die($json->encode($result));
		}
		if(!is_admin_warehouse($warehouse_id ,erp_get_admin_id()))
		{
			$result['error']=5;
			$result['message']=$_LANG['erp_warehouse_no_access'];
			die($json->encode($result));
		}
		if(lock_table($delivery_id,'delivery','edit'))
		{
			$sql="update ".$ecs->table('erp_delivery')." set warehouse_id='".$warehouse_id."' where delivery_id='".$delivery_id."'";
			$db->query($sql);
			$result['error']=0;
			die($json->encode($result));
		}
		else{
			$result['error']=-1;
			$result['message']=$_LANG['erp_delivery_no_accessibility'];
			die($json->encode($result));
		}
	}
	if ($_REQUEST['act'] == 'change_delivery_to')
	{
		include('../includes/cls_json.php');
		$json  = new JSON;
		if(!admin_priv('erp_warehouse_manage','',false))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_no_permit'];
			die($json->encode($result));
		}
		$delivery_id=isset($_REQUEST['delivery_id']) &&intval($_REQUEST['delivery_id'])>0?intval($_REQUEST['delivery_id']): 0;
		$delivery_to=isset($_REQUEST['delivery_to'])?trim($_REQUEST['delivery_to']) : '';
		if(empty($delivery_id))
		{
			$result['error']=2;
			$result['message']=$_LANG['erp_wrong_parameter'];
			die($json->encode($result));
		}
		$delivery_info=is_delivery_exist($delivery_id);
		if(!$delivery_info)
		{
			$result['error']=3;
			$result['message']=$_LANG['erp_delivery_not_exist'];
			die($json->encode($result));
		}
		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		if(!is_agency_delivery($delivery_id,$agency_id))
		{
			$result['error']=2;
			$result['message']=$_LANG['erp_delivery_no_agency_access'];
			die($json->encode($result));
		}
		if($delivery_info['status']!=1)
		{
			$result['error']=6;
			$result['message']=$_LANG['erp_delivery_no_acts'];
			die($json->encode($result));
		}
		if(!is_admin_delivery($delivery_id ,erp_get_admin_id()))
		{
			$result['error']=5;
			$result['message']=$_LANG['erp_delivery_no_access'];
			die($json->encode($result));
		}
		if(lock_table($delivery_id,'delivery','edit'))
		{
			$sql="update ".$ecs->table('erp_delivery')." set delivery_to='".$delivery_to."' where delivery_id='".$delivery_id."'";
			$db->query($sql);
			$result['error']=0;
			die($json->encode($result));
		}
		else{
			$result['error']=-1;
			$result['message']=$_LANG['erp_delivery_no_accessibility'];
			die($json->encode($result));
		}
	}
	elseif ($_REQUEST['act'] == 'change_delivery_style')
	{
		include('../includes/cls_json.php');
		$json  = new JSON;
		if(!admin_priv('erp_warehouse_manage','',false))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_no_permit'];
			die($json->encode($result));
		}
		$delivery_id=isset($_REQUEST['delivery_id']) &&intval($_REQUEST['delivery_id'])>0?intval($_REQUEST['delivery_id']) : 0;
		$delivery_style_id=isset($_REQUEST['delivery_style_id']) &&intval($_REQUEST['delivery_style_id'])>0?intval($_REQUEST['delivery_style_id']) : 0;
		if(empty($delivery_id) ||empty($delivery_style_id))
		{
			$result['error']=2;
			$result['message']=$_LANG['erp_wrong_parameter'];
			die($json->encode($result));
		}
		$delivery_info=is_delivery_exist($delivery_id);
		if(!$delivery_info)
		{
			$result['error']=3;
			$result['message']=$_LANG['erp_delivery_not_exist'];
			die($json->encode($result));
		}
		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		if(!is_agency_delivery($delivery_id,$agency_id))
		{
			$result['error']=2;
			$result['message']=$_LANG['erp_delivery_no_agency_access'];
			die($json->encode($result));
		}
		if($delivery_info['status']!=1)
		{
			$result['error']=6;
			$result['message']=$_LANG['erp_delivery_no_acts'];
			die($json->encode($result));
		}
		if(!is_admin_delivery($delivery_id ,erp_get_admin_id()))
		{
			$result['error']=5;
			$result['message']=$_LANG['erp_delivery_no_access'];
			die($json->encode($result));
		}
		$delivery_style_info=is_delivery_style_exist($delivery_style_id);
		if(!$delivery_style_info)
		{
			$result['error']=4;
			$result['message']=$_LANG['erp_delivery_style_not_exist'];
			die($json->encode($result));
		}
		if(lock_table($delivery_id,'delivery','edit'))
		{
			$sql="update ".$ecs->table('erp_delivery')." set delivery_style_id ='".$delivery_style_id."' where delivery_id='".$delivery_id."'";
			$db->query($sql);
			if($delivery_style_id!=1)
			{
				$sql="update ".$ecs->table('erp_delivery')." set order_id='0' where delivery_id='".$delivery_id."'";
				$db->query($sql);
				$sql="delete from ".$ecs->table('erp_delivery_item')." where delivery_id='".$delivery_id."'";
				$db->query($sql);
			}
			$result['error']=0;
			die($json->encode($result));
		}
		else{
			$result['error']=-1;
			$result['message']=$_LANG['erp_delivery_no_accessibility'];
			die($json->encode($result));
		}
	}
	elseif ($_REQUEST['act'] == 'change_order')
	{
		include('../includes/cls_json.php');
		$json  = new JSON;
		if(!admin_priv('erp_warehouse_manage','',false))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_no_permit'];
			die($json->encode($result));
		}
		$delivery_id=isset($_REQUEST['delivery_id']) &&intval($_REQUEST['delivery_id'])>0?intval($_REQUEST['delivery_id']) : 0;
		$order_id=isset($_REQUEST['order_id']) &&intval($_REQUEST['order_id'])>0?intval($_REQUEST['order_id']) : 0;
		if(empty($delivery_id) ||empty($order_id))
		{
			$result['error']=2;
			$result['message']=$_LANG['erp_wrong_parameter'];
			die($json->encode($result));
		}
		$delivery_info=is_delivery_exist($delivery_id);
		if(!$delivery_info)
		{
			$result['error']=3;
			$result['message']=$_LANG['erp_delivery_not_exist'];
			die($json->encode($result));
		}
		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		if(!is_agency_delivery($delivery_id,$agency_id))
		{
			$result['error']=2;
			$result['message']=$_LANG['erp_delivery_no_agency_access'];
			die($json->encode($result));
		}
		if($delivery_info['status']!=1)
		{
			$result['error']=6;
			$result['message']=$_LANG['erp_delivery_no_acts'];
			die($json->encode($result));
		}
		if(!is_admin_delivery($delivery_id ,erp_get_admin_id()))
		{
			$result['error']=5;
			$result['message']=$_LANG['erp_delivery_no_access'];
			die($json->encode($result));
		}
		$order_info=is_sales_order_exist($order_id);
		if(!$order_info)
		{
			$result['error']=4;
			$result['message']=$_LANG['erp_order_not_exist'];
			die($json->encode($result));
		}
		if(lock_table($delivery_id,'delivery','edit'))
		{
			$sql="update ".$ecs->table('erp_delivery')." set order_id ='".$order_id."' where delivery_id='".$delivery_id."'";
			$db->query($sql);
			$sql="delete from ".$ecs->table('erp_delivery_item')." where delivery_id='".$delivery_id."'";
			$db->query($sql);
			if(create_delivery_item_by_order($order_id,$delivery_id))
			{
				$result['error']=0;
				die($json->encode($result));
			}
			else{
				$result['error']=4;
				$result['message']=$_LANG['erp_order_not_exist'];
				die($json->encode($result));
			}
		}
		else{
			$result['error']=-1;
			$result['message']=$_LANG['erp_delivery_no_accessibility'];
			die($json->encode($result));
		}
	}
	elseif ($_REQUEST['act'] == 'change_delivered_qty')
	{
		include('../includes/cls_json.php');
		$json  = new JSON;
		if(!admin_priv('erp_warehouse_manage','',false))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_no_permit'];
			die($json->encode($result));
		}
		$delivery_id=isset($_REQUEST['delivery_id']) &&intval($_REQUEST['delivery_id'])>0?intval($_REQUEST['delivery_id']):0;
		$delivery_item_id=isset($_REQUEST['delivery_item_id']) &&intval($_REQUEST['delivery_item_id'])>0?intval($_REQUEST['delivery_item_id']):0;
		$delivered_qty=isset($_REQUEST['delivered_qty']) &&intval($_REQUEST['delivered_qty'])>0?intval($_REQUEST['delivered_qty']):0;
		if(empty($delivery_id) ||empty($delivery_item_id))
		{
			$result['error']=2;
			$result['message']=$_LANG['erp_wrong_parameter'];
			die($json->encode($result));
		}
		$delivery_info=is_delivery_exist($delivery_id);
		if(!$delivery_info)
		{
			$result['error']=3;
			$result['message']=$_LANG['erp_delivery_not_exist'];
			die($json->encode($result));
		}
		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		if(!is_agency_delivery($delivery_id,$agency_id))
		{
			$result['error']=2;
			$result['message']=$_LANG['erp_delivery_no_agency_access'];
			die($json->encode($result));
		}
		if($delivery_info['status']!=1)
		{
			$result['error']=6;
			$result['message']=$_LANG['erp_delivery_no_acts'];
			die($json->encode($result));
		}
		if(!is_admin_delivery($delivery_id ,erp_get_admin_id()))
		{
			$result['error']=5;
			$result['message']=$_LANG['erp_delivery_no_access'];
			die($json->encode($result));
		}
		$delivery_item_info=is_delivery_item_exist($delivery_item_id);
		if(!$delivery_item_info)
		{
			$result['error']=4;
			$result['message']=$_LANG['erp_delivery_item_not_exist'];
			die($json->encode($result));
		}
		if(empty($delivery_item_info['attr_id']))
		{
			$stock=get_goods_stock_by_warehouse($delivery_info['warehouse_id'],$delivery_item_info['goods_id']);
		}
		else{
			$stock=get_goods_stock_by_warehouse($delivery_info['warehouse_id'],$delivery_item_info['goods_id'],$delivery_item_info['attr_id']);
		}
		if($delivered_qty >$stock)
		{
			$result['error']=2;
			$result['message']=sprintf($_LANG['erp_delivery_stock_not_enough'],$stock);
			die($json->encode($result));
		}
		if(lock_table($delivery_id,'delivery','edit'))
		{
			$sql="update ".$ecs->table('erp_delivery_item')." set delivered_qty='".$delivered_qty."',account_due=price*".$delivered_qty." where delivery_item_id='".$delivery_item_id."'";
			$db->query($sql);
			$result['error']=0;
			die($json->encode($result));
		}
		else{
			$result['error']=-1;
			$result['message']=$_LANG['erp_delivery_no_accessibility'];
			die($json->encode($result));
		}
	}
	elseif ($_REQUEST['act'] == 'delete_delivery_item')
	{
		include('../includes/cls_json.php');
		$json  = new JSON;
		if(!admin_priv('erp_warehouse_manage','',false))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_no_permit'];
			die($json->encode($result));
		}
		$delivery_id=isset($_REQUEST['delivery_id']) &&intval($_REQUEST['delivery_id'])>0?intval($_REQUEST['delivery_id']):0;
		$delivery_item_id=isset($_REQUEST['delivery_item_id']) &&intval($_REQUEST['delivery_item_id'])>0?intval($_REQUEST['delivery_item_id']):0;
		if(empty($delivery_id) ||empty($delivery_item_id))
		{
			$result['error']=2;
			$result['message']=$_LANG['erp_wrong_parameter'];
			die($json->encode($result));
		}
		$delivery_info=is_delivery_exist($delivery_id);
		if(!$delivery_info)
		{
			$result['error']=3;
			$result['message']=$_LANG['erp_delivery_not_exist'];
			die($json->encode($result));
		}
		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		if(!is_agency_delivery($delivery_id,$agency_id))
		{
			$result['error']=2;
			$result['message']=$_LANG['erp_delivery_no_agency_access'];
			die($json->encode($result));
		}
		if($delivery_info['status']!=1)
		{
			$result['error']=6;
			$result['message']=$_LANG['erp_delivery_no_acts'];
			die($json->encode($result));
		}
		if(!is_admin_delivery($delivery_id ,erp_get_admin_id()))
		{
			$result['error']=5;
			$result['message']=$_LANG['erp_delivery_no_access'];
			die($json->encode($result));
		}
		$delivery_item_info=is_delivery_item_exist($delivery_item_id);
		if(!$delivery_item_info)
		{
			$result['error']=4;
			$result['message']=$_LANG['erp_delivery_item_not_exist'];
			die($json->encode($result));
		}
		if(lock_table($delivery_id,'delivery','edit'))
		{
			$sql="delete from ".$ecs->table('erp_delivery_item')." where delivery_item_id='".$delivery_item_id."'";
			$db->query($sql);
			$result['error']=0;
			die($json->encode($result));
		}
		else{
			$result['error']=-1;
			$result['message']=$_LANG['erp_delivery_no_accessibility'];
			die($json->encode($result));
		}
	}
	elseif ($_REQUEST['act'] == 'delete_delivery')
	{
		include('../includes/cls_json.php');
		$json  = new JSON;
		if(!admin_priv('erp_warehouse_manage','',false))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_no_permit'];
			die($json->encode($result));
		}
		$delivery_id=isset($_REQUEST['delivery_id']) &&intval($_REQUEST['delivery_id'])>0?intval($_REQUEST['delivery_id']) : 0;
		if(empty($delivery_id))
		{
			$result['error']=2;
			$result['message']=$_LANG['erp_wrong_parameter'];
			die($json->encode($result));
		}
		$delivery_info=is_delivery_exist($delivery_id);
		if(!$delivery_info)
		{
			$result['error']=3;
			$result['message']=$_LANG['erp_delivery_not_exist'];
			die($json->encode($result));
		}
		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		if(!is_agency_delivery($delivery_id,$agency_id))
		{
			$result['error']=2;
			$result['message']=$_LANG['erp_delivery_no_agency_access'];
			die($json->encode($result));
		}
		if($delivery_info['status']!=1)
		{
			$result['error']=6;
			$result['message']=$_LANG['erp_delivery_no_acts'];
			die($json->encode($result));
		}
		if(!is_admin_delivery($delivery_id ,erp_get_admin_id()))
		{
			$result['error']=5;
			$result['message']=$_LANG['erp_delivery_no_access'];
			die($json->encode($result));
		}
		if($delivery_info['status']!=1)
		{
			$result['error']=6;
			$result['message']=$_LANG['erp_delivery_no_acts'];
			die($json->encode($result));
		}
		if(lock_table($delivery_id,'delivery','delete'))
		{
			$sql="delete from ".$ecs->table('erp_delivery_item')." where delivery_id='".$delivery_id."'";
			$db->query($sql);
			$sql="delete from ".$ecs->table('erp_delivery')." where delivery_id='".$delivery_id."'";
			$db->query($sql);
			$result['error']=0;
			die($json->encode($result));
		}
		else{
			$result['error']=-1;
			$result['message']=$_LANG['erp_delivery_no_accessibility'];
			die($json->encode($result));
		}
	}
	elseif ($_REQUEST['act'] == 'check_goods_barcode')
	{
		include('../includes/cls_json.php');
		$json  = new JSON;
		$goods_barcode=trim($_REQUEST['goods_barcode']);
		$barcode_id=check_goods_barcode($goods_barcode);
		if(empty($barcode_id))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_warehousing_wrong_goods_barcode'];
			die($json->encode($result));
		}
		$goods_attr=get_goods_attr_by_barcode($barcode_id);
		$smarty->assign('goods_attr',$goods_attr);
		$smarty->assign('is_dynamic',1);
		$attr_info=$smarty->fetch('erp_select_goods_attr.htm');
		$result['attr_info']=$attr_info;
		$result['attr_num']=count($goods_attr);
		$result['error']=0;
		die($json->encode($result));
	}
	elseif ($_REQUEST['act'] == 'check_goods_sn'||$_REQUEST['act'] == 'check_goods_name')
	{
		include('../includes/cls_json.php');
		$json  = new JSON;
		if($_REQUEST['act'] == 'check_goods_sn')
		{
			$goods_sn=trim($_REQUEST['goods_sn']);
			$goods_id=check_goods_sn($goods_sn);
			if(empty($goods_id))
			{
				$result['error']=1;
				$result['message']=$_LANG['erp_order_wrong_goods_sn'];
				die($json->encode($result));
			}
		}
		else{
			$goods_name=trim($_REQUEST['goods_name']);
			$goods_id=check_goods_name($goods_name);
			if(empty($goods_id))
			{
				$result['error']=1;
				$result['message']=$_LANG['erp_order_wrong_goods_name'];
				die($json->encode($result));
			}
		}
		$goods_attr=goods_attr($goods_id);
		$smarty->assign('goods_attr',$goods_attr);
		$attr_info=$smarty->fetch('erp_select_goods_attr.htm');
		$result['attr_info']=$attr_info;
		$result['attr_num']=count($goods_attr);
		$result['error']=0;
		die($json->encode($result));
	}
	elseif ($_REQUEST['act'] == 'goods_barcode_tips')
	{
		include('../includes/cls_json.php');
		$json  = new JSON;
		$goods_barcode=trim($_REQUEST['goods_barcode']);
		$fetch_goods_barcode=array();
		$sql="select barcode from ".$GLOBALS['ecs']->table('erp_goods_barcode')." where barcode like '%".$goods_barcode."%' limit 10";
		$res=$GLOBALS['db']->query($sql);
		while($row=mysql_fetch_assoc($res))
		{
			$fetch_goods_barcode[]=$row['barcode'];
		}
		$smarty->assign('goods_barcode',$fetch_goods_barcode);
		$content=$smarty->fetch('erp_goods_barcode_tips.htm');
		$result['error']=0;
		$result['content']=$content;
		$result['num']=count($fetch_goods_barcode);
		die($json->encode($result));
	}
	elseif ($_REQUEST['act'] == 'goods_sn_tips')
	{
		include('../includes/cls_json.php');
		$json  = new JSON;
		$goods_sn=trim($_REQUEST['goods_sn']);
		$fetch_goods_sn=array();
		$sql="select g.goods_sn from ".$GLOBALS['ecs']->table('goods')." as g ";
		$sql.=" where g.goods_sn like '%".$goods_sn."%' order by goods_id desc limit 10";
		$res=$GLOBALS['db']->query($sql);
		while($row=mysql_fetch_assoc($res))
		{
			$fetch_goods_sn[]=$row['goods_sn'];
		}
		$smarty->assign('goods_sn',$fetch_goods_sn);
		$content=$smarty->fetch('erp_goods_sn_tips.htm');
		$result['error']=0;
		$result['content']=$content;
		$result['num']=count($fetch_goods_sn);
		die($json->encode($result));
	}
	elseif ($_REQUEST['act'] == 'goods_name_tips')
	{
		include('../includes/cls_json.php');
		$json  = new JSON;
		$goods_name=trim($_REQUEST['goods_name']);
		$fetch_goods_name=array();
		$sql="select g.goods_name from ".$GLOBALS['ecs']->table('goods')." as g ";
		$sql.=" where g.goods_name like '%".$goods_name."%' order by goods_id desc limit 10";
		$res=$GLOBALS['db']->query($sql);
		while($row=mysql_fetch_assoc($res))
		{
			$fetch_goods_name[]=$row['goods_name'];
		}
		$smarty->assign('goods_name',$fetch_goods_name);
		$content=$smarty->fetch('erp_goods_name_tips.htm');
		$result['error']=0;
		$result['content']=$content;
		$result['num']=count($fetch_goods_name);
		die($json->encode($result));
	}
	elseif ($_REQUEST['act'] == 'update_delivery')
	{
		include('../includes/cls_json.php');
		$json  = new JSON;
		if(!admin_priv('erp_warehouse_manage','',false))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_no_permit'];
			die($json->encode($result));
		}
		$delivery_id=isset($_REQUEST['delivery_id']) &&intval($_REQUEST['delivery_id'])>0?intval($_REQUEST['delivery_id']):0;
		$goods_sn=isset($_REQUEST['goods_sn'])?trim($_REQUEST['goods_sn']):'';
		$goods_name=isset($_REQUEST['goods_name'])?trim($_REQUEST['goods_name']):'';
		$goods_barcode=isset($_REQUEST['goods_barcode'])?trim($_REQUEST['goods_barcode']):'';
		$ids=isset($_REQUEST['ids'])?trim($_REQUEST['ids']):'';
		$goods_qty=isset($_REQUEST['goods_qty']) &&intval($_REQUEST['goods_qty'])>0?intval($_REQUEST['goods_qty']):1;
		if(empty($delivery_id))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_wrong_parameter'];
			die($json->encode($result));
		}
		if(empty($goods_sn) &&empty($goods_name) &&empty($goods_barcode))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_delivery_goods_sn_or_goods_name_or_goods_barcode_required'];
			die($json->encode($result));
		}
		$delivery_info=is_delivery_exist($delivery_id);
		if(!$delivery_info)
		{
			$result['error']=3;
			$result['message']=$_LANG['erp_delivery_not_exist'];
			die($json->encode($result));
		}
		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		if(!is_agency_delivery($delivery_id,$agency_id))
		{
			$result['error']=2;
			$result['message']=$_LANG['erp_delivery_no_agency_access'];
			die($json->encode($result));
		}
		if($delivery_info['status']!=1)
		{
			$result['error']=6;
			$result['message']=$_LANG['erp_delivery_no_acts'];
			die($json->encode($result));
		}
		if(!is_admin_delivery($delivery_id ,erp_get_admin_id()))
		{
			$result['error']=5;
			$result['message']=$_LANG['erp_delivery_no_access'];
			die($json->encode($result));
		}
		if(!empty($goods_sn))
		{
			$goods_id=check_goods_sn($goods_sn);
			if(empty($goods_id))
			{
				$result['error']=1;
				$result['message']=$_LANG['erp_delivery_wrong_goods_sn'];
				die($json->encode($result));
			}
		}
		elseif(!empty($goods_name))
		{
			$goods_id=check_goods_name($goods_name);
			if(empty($goods_id))
			{
				$result['error']=1;
				$result['message']=$_LANG['erp_delivery_wrong_goods_name'];
				die($json->encode($result));
			}
		}
		elseif(!empty($goods_barcode))
		{
			$barcode_id=check_goods_barcode($goods_barcode);
			if(empty($barcode_id))
			{
				$result['error']=1;
				$result['message']=$_LANG['erp_delivery_wrong_goods_barcode'];
				die($json->encode($result));
			}
			$goods_id=get_goods_id_by_barcode_id($barcode_id);
			$ids=empty($ids)?get_attr_id_list_by_barcode_id($barcode_id) : $ids;
		}
		$warehouse_id=$delivery_info['warehouse_id'];
		if(empty($ids))
		{
			$stock=get_goods_stock_by_warehouse($warehouse_id,$goods_id);
			$sql="select delivery_item_id,delivered_qty from ".$ecs->table('erp_delivery_item')." where delivery_id='".$delivery_id."' and goods_id='".$goods_id."'";
			$delivery_item_info=$db->getRow($sql);
			if(empty($delivery_item_info))
			{
				$attr_id=get_attr_id($goods_id);
				if($goods_qty >$stock)
				{
					$result['error']=2;
					$result['message']=sprintf($_LANG['erp_delivery_stock_not_enough'],$stock);
					die($json->encode($result));
				}
				$sql="insert into ".$ecs->table('erp_delivery_item')." set delivery_id='".$delivery_id."',goods_id='".$goods_id."',attr_id='".$attr_id."', expected_qty='0',delivered_qty='".$goods_qty."'";
				$db->query($sql);
				$result['error']=0;
				die($json->encode($result));
			}
			else{
				if($goods_qty+$delivery_item_info['delivered_qty'] >$stock)
				{
					$result['error']=2;
					$result['message']=sprintf($_LANG['erp_delivery_stock_not_enough'],$stock);
					die($json->encode($result));
				}
				$sql="update ".$ecs->table('erp_delivery_item')." set delivered_qty=delivered_qty+'".$goods_qty."' where delivery_item_id='".$delivery_item_info['delivery_item_id']."'";
				$db->query($sql);
				$result['error']=0;
				die($json->encode($result));
			}
		}
		else{
			$ids=trim($ids,',');
			$attr_id=get_attr_id($goods_id,$ids);
			$stock=get_goods_stock_by_warehouse($warehouse_id,$goods_id,$attr_id);
			$sql="select delivery_item_id,delivered_qty from ".$ecs->table('erp_delivery_item')." where delivery_id='".$delivery_id."' and goods_id='".$goods_id."' and attr_id='".$attr_id."'";
			$delivery_item_info=$db->getRow($sql);
			if(empty($delivery_item_info))
			{
				if($goods_qty >$stock)
				{
					$result['error']=2;
					$result['message']=sprintf($_LANG['erp_delivery_stock_not_enough'],$stock);
					die($json->encode($result));
				}
				$sql="insert into ".$ecs->table('erp_delivery_item')." set delivery_id='".$delivery_id."',goods_id='".$goods_id."',attr_id='".$attr_id."', expected_qty='0',delivered_qty='".$goods_qty."'";
				$db->query($sql);
				$result['error']=0;
				die($json->encode($result));
			}
			else{
				if($goods_qty+$delivery_item_info['delivered_qty'] >$stock)
				{
					$result['error']=2;
					$result['message']=sprintf($_LANG['erp_delivery_stock_not_enough'],$stock);
					die($json->encode($result));
				}
				$sql="update ".$ecs->table('erp_delivery_item')." set delivered_qty=delivered_qty+'".$goods_qty."' where delivery_item_id='".$delivery_item_info['delivery_item_id']."'";
				$db->query($sql);
				$result['error']=0;
				die($json->encode($result));
			}
		}
	}
	elseif ($_REQUEST['act'] == 'post_delivery')
	{
		include('../includes/cls_json.php');
		$json  = new JSON;
		if(!admin_priv('erp_warehouse_manage','',false))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_no_permit'];
			die($json->encode($result));
		}
		$delivery_id=isset($_REQUEST['delivery_id']) &&intval($_REQUEST['delivery_id'])>0?intval($_REQUEST['delivery_id']):0;
		if(empty($delivery_id))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_wrong_parameter'];
			die($json->encode($result));
		}
		$delivery_info=is_delivery_exist($delivery_id);
		if(!$delivery_info)
		{
			$result['error']=3;
			$result['message']=$_LANG['erp_delivery_not_exist'];
			die($json->encode($result));
		}
		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		if(!is_agency_delivery($delivery_id,$agency_id))
		{
			$result['error']=2;
			$result['message']=$_LANG['erp_delivery_no_agency_access'];
			die($json->encode($result));
		}
		if($delivery_info['status']!=1)
		{
			$result['error']=6;
			$result['message']=$_LANG['erp_delivery_no_acts'];
			die($json->encode($result));
		}
		if(!is_admin_delivery($delivery_id ,erp_get_admin_id()))
		{
			$result['error']=5;
			$result['message']=$_LANG['erp_delivery_no_access'];
			die($json->encode($result));
		}
		if($delivery_info['status']!=1)
		{
			$result['error']=6;
			$result['message']=$_LANG['erp_delivery_no_acts'];
			die($json->encode($result));
		}
		$error=check_delivery_integrity($delivery_id);
		if($error>0)
		{
			$result['error']=4;
			$result['message']=$_LANG['erp_delivery_not_completed'];
			die($json->encode($result));
		}
		if(lock_table($delivery_id,'delivery','post_to_approve'))
		{
			$sql="update ".$ecs->table('erp_delivery')." set status='2',post_by='".erp_get_admin_id()."',post_time='".gmtime()."' where delivery_id='".$delivery_id."'";
			if($db->query($sql))
			{
				$result['error']=0;
				die($json->encode($result));
			}
		}
		else{
			$result['error']=-1;
			$result['message']=$_LANG['erp_delivery_no_accessibility'];
			die($json->encode($result));
		}
	}
	elseif ($_REQUEST['act'] == 'add_delivery')
	{
		include('../includes/cls_json.php');
		$json  = new JSON;
		if(!admin_priv('erp_warehouse_manage','',false))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_no_permit'];
			die($json->encode($result));
		}
		if ($_REQUEST['manual_add'] == 'true'){
			$delivery_id=add_delivery(null, 4);
		} else {
			$delivery_id=add_delivery();
		}
		if($delivery_id==-1)
		{
			$result['error']=1;
			$result['message']=$GLOBALS['_LANG']['erp_delivery_without_valid_warehouse'];
			die($json->encode($result));
		}
		else{
			$result['error']=0;
			$result['delivery_id']=$delivery_id;
			die($json->encode($result));
		}
	}
	elseif ($_REQUEST['act'] == 'withdrawal_to_edit')
	{
		include('../includes/cls_json.php');
		$json  = new JSON;
		if(!admin_priv('erp_warehouse_approve','',false))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_no_permit'];
			die($json->encode($result));
		}
		$delivery_id=isset($_REQUEST['delivery_id']) &&intval($_REQUEST['delivery_id'])>0?intval($_REQUEST['delivery_id']):0;
		if(empty($delivery_id))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_wrong_parameter'];
			die($json->encode($result));
		}
		$delivery_info=is_delivery_exist($delivery_id);
		if(!$delivery_info)
		{
			$result['error']=3;
			$result['message']=$_LANG['erp_delivery_not_exist'];
			die($json->encode($result));
		}
		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		if(!is_agency_delivery($delivery_id,$agency_id))
		{
			$result['error']=2;
			$result['message']=$_LANG['erp_delivery_no_agency_access'];
			die($json->encode($result));
		}
		if($delivery_info['status']!=2)
		{
			$result['error']=6;
			$result['message']=$_LANG['erp_delivery_no_acts'];
			die($json->encode($result));
		}
		$sql="update ".$ecs->table('erp_delivery')." set status='1' where delivery_id='".$delivery_id."'";
		$db->query($sql);
		$result['error']=0;
		die($json->encode($result));
	}
	elseif ($_REQUEST['act'] == 'approve_pass')
	{
		include('../includes/cls_json.php');
		$json  = new JSON;
		if(!admin_priv('erp_warehouse_approve','',false))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_no_permit'];
			die($json->encode($result));
		}
		$delivery_id=isset($_REQUEST['delivery_id']) &&intval($_REQUEST['delivery_id'])>0?intval($_REQUEST['delivery_id']):0;
		$approve_remark=isset($_REQUEST['approve_remark']) ?trim($_REQUEST['approve_remark']) : '';
		if(empty($delivery_id))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_wrong_parameter'];
			die($json->encode($result));
		}
		$delivery_info=is_delivery_exist($delivery_id);
		if(!$delivery_info)
		{
			$result['error']=3;
			$result['message']=$_LANG['erp_delivery_not_exist'];
			die($json->encode($result));
		}
		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		if(!is_agency_delivery($delivery_id,$agency_id))
		{
			$result['error']=2;
			$result['message']=$_LANG['erp_delivery_no_agency_access'];
			die($json->encode($result));
		}
		if($delivery_info['status']!=2)
		{
			$result['error']=6;
			$result['message']=$_LANG['erp_delivery_no_acts'];
			die($json->encode($result));
		}
		if(lock_table($delivery_id,'delivery','approve'))
		{
			$GLOBALS['db']->query('START TRANSACTION');
			$delivery_style_id=$delivery_info['delivery_style_id'];
			$warehouse_id=$delivery_info['warehouse_id'];
			$order_id=$delivery_info['order_id'];
			$delivery_sn=$delivery_info['delivery_sn'];
			$delivery_to=$delivery_info['delivery_to'];
			if(!empty($order_id))
			{
				$sql="select order_sn from ".$ecs->table('order_info')." where order_id='".$order_id."'";
				$order_sn=$db->getOne($sql);
			}
			else{
				$order_sn='';
			}
			$delivery_item_info=get_delivery_item_info($delivery_id);
			if(($delivery_style_id==1) || ($delivery_style_id==3))
			{
				foreach($delivery_item_info as $delivery_item)
				{
					$order_item_id=$delivery_item['order_item_id'];
					$delivered_qty=$delivery_item['delivered_qty'];
					$sql="update ".$ecs->table('order_goods')." set delivery_qty=delivery_qty+".$delivered_qty." where rec_id='".$order_item_id."'";
					if(!$db->query($sql))
					{
						$GLOBALS['db']->query('ROLLBACK');
						$result['error']=9;
						$result['message']=$_LANG['erp_delivery_approve_pass_failed'];
						die($json->encode($result));
					}
				}
				$sql="select sum(goods_price*delivery_qty) as delivery_goods_amount from ".$GLOBALS['ecs']->table('order_goods')." where order_id='".$order_id."'";
				$delivery_goods_amount=$GLOBALS['db']->getOne($sql);
				$sql="update ".$GLOBALS['ecs']->table('order_info')." set delivery_goods_amount='".$delivery_goods_amount."' where order_id='".$order_id."'";
				if(!$db->query($sql))
				{
					$GLOBALS['db']->query('ROLLBACK');
					$result['error']=10;
					$result['message']=$_LANG['erp_delivery_approve_pass_failed'];
					die($json->encode($result));
				}
				//Supplier inventory consumption sequence
				$erpController->processConsumptionSequence($delivery_item_info);
			}
			foreach($delivery_item_info as $delivery_item)
			{
				$goods_id=$delivery_item['goods_id'];
				$attr_id=$delivery_item['attr_id'];
				
				/*UUECS 纠正错误 */
				if($delivery_item['attr_id']=='0'){
					$sql="select attr_id from ".$GLOBALS['ecs']->table('erp_goods_attr')." Where goods_id = '".$delivery_item['goods_id']."'";
					$attr_id=$GLOBALS['db']->getOne($sql);
				}
				$qty=$delivery_item['delivered_qty'];
				$price=$delivery_item['price'];
				$sql="select stock from ".$ecs->table('erp_stock')." where goods_id='".$goods_id."' and goods_attr_id='".$attr_id."' and warehouse_id='".$warehouse_id."' order by stock_id desc limit 1";
				$stock=$db->getOne($sql);
				$stock=!empty($stock)?$stock: 0;
				if($stock <$qty)
				{
					$GLOBALS['db']->query('ROLLBACK');
					$result['error']=9;
					$result['message']=$_LANG['erp_delivery_approve_pass_failed_no_stock'];
					die($json->encode($result));
				}
				$sql="insert into ".$ecs->table('erp_stock')." set goods_id='".$goods_id."',goods_attr_id='".$attr_id."',qty='".(0-$qty)."',price='".$price."',amount=qty*price,act_time='".gmtime()."',warehouse_id='".$warehouse_id."'";
				$sql.=", w_d_name='".$delivery_to."', w_d_style_id='".$delivery_style_id."',order_sn='".$order_sn."',stock_style='d',w_d_sn='".$delivery_sn."',stock=".($stock-$qty).",act_date='".local_date('Y-m-d',gmtime())."'";
				if(!$db->query($sql))
				{
					$GLOBALS['db']->query('ROLLBACK');
					$result['error']=9;
					$result['message']=$_LANG['erp_delivery_approve_pass_failed'];
					die($json->encode($result));
				}
			}
			foreach($delivery_item_info as $delivery_item)
			{
				$goods_id=$delivery_item['goods_id'];
				$attr_id=$delivery_item['attr_id'];
				$qty=$delivery_item['delivered_qty'];
				if(empty($attr_id))
				{
					$sql="select attr_id from ".$ecs->table('erp_goods_attr')." where goods_id='".$goods_id."' and goods_attr_id_list='$$'";
					$attr_id=$db->getOne($sql);
				}
				$sql="update ".$ecs->table('erp_goods_attr')." set goods_qty=goods_qty-".$qty." where goods_id='".$goods_id."' and attr_id='".$attr_id."'";
				if(!$db->query($sql))
				{
					$GLOBALS['db']->query('ROLLBACK');
					$result['error']=9;
					$result['message']=$_LANG['erp_delivery_approve_pass_failed'];
					die($json->encode($result));
				}
				$sql="update ".$ecs->table('erp_goods_attr_stock')." set goods_qty =goods_qty-".$qty." where goods_id='".$goods_id."' and attr_id='".$attr_id."' and warehouse_id='".$warehouse_id."'";
				if(!$db->query($sql))
				{
					$GLOBALS['db']->query('ROLLBACK');
					$result['error']=9;
					$result['message']=$_LANG['erp_delivery_approve_pass_failed'];
					die($json->encode($result));
				}
			}
			foreach($delivery_item_info as $delivery_item)
			{
				$goods_id = $delivery_item['goods_id'];
				$qty = intval($delivery_item['delivered_qty']);
				// Out of stock notification
				$new_quantity = $erpController->getSellableProductQty($goods_id);
				if ($new_quantity == 0)
				{
					if (@include_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_notifications.php'))
					{
						notify_goods_stock($db->getRow("SELECT goods_id, goods_name, goods_sn FROM " . $ecs->table('goods') . " WHERE goods_id = '$goods_id'"), false);
					}
					else
					{
						error_log(__FILE__.':'.__LINE__.' Cannot include lib_notifications.php');
					}
				}
				// Update sales count
				if(!empty($order_id))
				{
					$sql = "UPDATE " . $ecs->table('goods') .
						   " SET sales = sales + " . $qty .
						   " WHERE goods_id = '" . $goods_id . "'";
					if(!$db->query($sql))
					{
						$db->query('ROLLBACK');
						$result['error']=9;
						$result['message']=$_LANG['erp_delivery_approve_pass_failed'];
						die($json->encode($result));
					}
				}
			}
			$sql="update ".$ecs->table('erp_delivery')." set status='3',approve_time='".gmtime()."',approved_by='".erp_get_admin_id()."',approve_remark='".$approve_remark."' where delivery_id='".$delivery_id."'";
			if(!$db->query($sql))
			{
				$GLOBALS['db']->query('ROLLBACK');
				$result['error']=9;
				$result['message']=$_LANG['erp_delivery_approve_pass_failed'];
				die($json->encode($result));
			}
			else{
				$GLOBALS['db']->query('COMMIT');
				$result['error']=0;
				die($json->encode($result));
			}
		}
		else{
			$result['error']=-1;
			$result['message']=$_LANG['erp_delivery_no_accessibility'];
			die($json->encode($result));
		}
	}
	elseif ($_REQUEST['act'] == 'approve_reject')
	{
		include('../includes/cls_json.php');
		$json  = new JSON;
		if(!admin_priv('erp_warehouse_approve','',false))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_no_permit'];
			die($json->encode($result));
		}
		$delivery_id=isset($_REQUEST['delivery_id']) &&intval($_REQUEST['delivery_id'])>0?intval($_REQUEST['delivery_id']):0;
		$approve_remark=isset($_REQUEST['approve_remark']) ?trim($_REQUEST['approve_remark']) : '';
		if(empty($delivery_id))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_wrong_parameter'];
			die($json->encode($result));
		}
		$delivery_info=is_delivery_exist($delivery_id);
		if(!$delivery_info)
		{
			$result['error']=3;
			$result['message']=$_LANG['erp_delivery_not_exist'];
			die($json->encode($result));
		}
		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		if(!is_agency_delivery($delivery_id,$agency_id))
		{
			$result['error']=2;
			$result['message']=$_LANG['erp_delivery_no_agency_access'];
			die($json->encode($result));
		}
		if($delivery_info['status']!=2)
		{
			$result['error']=6;
			$result['message']=$_LANG['erp_delivery_no_acts'];
			die($json->encode($result));
		}
		if(lock_table($delivery_id,'delivery','approve'))
		{
			$sql="update ".$ecs->table('erp_delivery')." set status='5',approve_remark='".$approve_remark."' where delivery_id='".$delivery_id."'";
			if($db->query($sql))
			{
				$result['error']=0;
				die($json->encode($result));
			}
		}
		else{
			$result['error']=-1;
			$result['message']=$_LANG['erp_delivery_no_accessibility'];
			die($json->encode($result));
		}
	}
	elseif ($_REQUEST['act'] == 'batch_delivery')
	{
		include('../includes/cls_json.php');
		$json  = new JSON;
		if(!admin_priv('erp_warehouse_manage','',false))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_no_permit'];
			die($json->encode($result));
		}
		$delivery_id=intval($_REQUEST['delivery_id']);
		if(isset($_FILES['file_batch_delivery']))
		{
			$upload_file_info=$_FILES['file_batch_delivery'];
			if($upload_file_info['type']!='text/plain')
			{
				$href="erp_warehouse_manage.php?act=edit_delivery&id=".$delivery_id;
				$text=$_LANG['erp_delivery_return'];
				$link[] = array('href'=>$href,'text'=>$text);
				sys_msg($_LANG['erp_delivery_only_text_file'],0,$link);
			}
			$souce_file=$upload_file_info['tmp_name'];
			$upload_file=format_file($souce_file);
			$delivery_info=check_file($upload_file);
			if(!is_array($delivery_info))
			{
				if($delivery_info==1)
				{
					$href="erp_warehouse_manage.php?act=edit_delivery&id=".$delivery_id;
					$text=$_LANG['erp_delivery_return'];
					$link[] = array('href'=>$href,'text'=>$text);
					sys_msg($_LANG['erp_delivery_invalid_file'],0,$link);
				}
				elseif($delivery_info==2)
				{
					$href="erp_warehouse_manage.php?act=edit_delivery&id=".$delivery_id;
					$text=$_LANG['erp_delivery_return'];
					$link[] = array('href'=>$href,'text'=>$text);
					sys_msg($_LANG['erp_delivery_invalid_goods_sn'],0,$link);
				}
				elseif($delivery_info==3)
				{
					$href="erp_warehouse_manage.php?act=edit_delivery&id=".$delivery_id;
					$text=$_LANG['erp_delivery_return'];
					$link[] = array('href'=>$href,'text'=>$text);
					sys_msg($_LANG['erp_delivery_invalid_goods_size'],0,$link);
				}
			}
			else{
				foreach($delivery_info as $delivery_item)
				{
					$goods_sn=$delivery_item['goods_info']['goods_sn'];
					$goods_size=$delivery_item['goods_info']['goods_size'];
					$delivery_qty=$delivery_item['qty'];
					$sql="select goods_id from ".$ecs->table('goods')." where goods_sn='".$goods_sn."'";
					$goods_id=$db->getOne($sql);
					if(lock_table($delivery_id,'delivery','edit'))
					{
						$sql="select delivery_item_id from ".$ecs->table('erp_delivery_item')." where delivery_id='".$delivery_id."' and goods_id='".$goods_id ."' and goods_size='".$goods_size."'";
						$delivery_item_id=$db->getOne($sql);
						if(!empty($delivery_item_id))
						{
							$sql="update ".$ecs->table('erp_delivery_item')." set delivered_qty=delivered_qty+".$delivery_qty." where delivery_item_id='".$delivery_item_id."'";
							$db->query($sql);
						}
						else{
							$sql="insert into ".$ecs->table('erp_delivery_item')." set delivery_id=".$delivery_id.",goods_id='".$goods_id."',goods_size='".$goods_size."',delivered_qty=".$delivery_qty;
							$db->query($sql);
						}
					}
					else{
						$href="erp_warehouse_manage.php?act=edit_delivery&id=".$delivery_id;
						$text=$_LANG['erp_delivery_return'];
						$link[] = array('href'=>$href,'text'=>$text);
						sys_msg($_LANG['erp_delivery_no_accessibility'],0,$link);
					}
				}
			}
		}
		$href="erp_warehouse_manage.php?act=edit_delivery&id=".$delivery_id;
		$text=$_LANG['erp_delivery_return'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_delivery_batch_delivery_success'],0,$link);
	}
?>
