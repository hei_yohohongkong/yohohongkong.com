<?php

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
$paymeController = new Yoho\cms\Controller\PaymeController();
$smarty->assign('lang', $_LANG);

if (empty($_REQUEST['act']))
{
    $_REQUEST['act'] = 'list';
}
else
{
    $_REQUEST['act'] = trim($_REQUEST['act']);
}

if ($_REQUEST['act'] == 'list')
{
    $smarty->assign('ur_here', 'Payme Activity Log');
    $smarty->display('payme_activity_log.htm');
}
elseif ($_REQUEST['act'] == 'query')
{
    $res = $paymeController->getPaymeActivityLog();

    $paymeController->ajaxQueryAction($res);
}
// elseif ($_REQUEST['act'] == 'clear') {
//     $paymeController->clearOldActivityLogEntry();
// }

?>
