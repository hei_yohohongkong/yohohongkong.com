<?php

/***
* refund_requests.php
* by howang 2014-10-17
*
* Manage refund requests
***/

define('IN_ECS', true);
require(dirname(__FILE__) . '/includes/init.php');
require_once(ROOT_PATH . 'includes/lib_payment.php');

$refundController = new Yoho\cms\Controller\RefundController();
$adminUserController = new Yoho\cms\Controller\AdminuserController();

if (!empty($_REQUEST['crm'])) {
    $smarty->assign('crm', 1);
}

if ($_REQUEST['act'] == 'list')
{
    $smarty->assign('ur_here',      $_LANG['05_refund_requests']);

    // Default hidden for new refund flow
    if(!empty($_REQUEST['order_sn']))
    {
        $smarty->assign('action_link', array('text' => '申請退款／積分', 'href' => 'refund_requests.php?act=add&order_sn=' . $_REQUEST['order_sn']));
    }
    if ($_SESSION['manage_cost'] || $_SESSION['is_accountant']) {
        $smarty->assign('action_link', array('text' => '手動新增申請', 'href' => 'refund_requests.php?act=add&accounting=1'));
    }

    $operators = $refundController->getOperableAdmins();
    $is_operator = $operators['is_operator'];
    if ($is_operator) {
        $smarty->assign('action_link2', array('text' => '處理退款／積分申請', 'href' => 'refund_requests.php?act=order_list' . (empty($_REQUEST['order_sn']) ? "" : "&order_sn=" . $_REQUEST['order_sn'])));
    }
    $smarty->assign('operators', $operators['operators']);
    $smarty->assign('is_operator', $is_operator);

    if(empty($_REQUEST['order_sn']))
        $smarty->assign('action_link3', array('text' => '退款紀錄', 'href' => 'refund_requests.php?act=month_report'));
    $smarty->assign('order_sn', $_REQUEST['order_sn']);
    $smarty->assign('remark', $_REQUEST['remark']);
    assign_query_info();
    $smarty->display('refund_requests_list.htm');
}
elseif ($_REQUEST['act'] == 'order_list')
{
    if(!$_SESSION['manage_cost'] && !$_SESSION['is_accountant'] && !check_authz(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_REFUND_PIC))
    {
        $_REQUEST['crm'] ? make_json_error($_LANG['priv_error']) : sys_msg($_LANG['priv_error']);
    }

    $smarty->assign('ur_here',      '退款及積分處理');
    $smarty->assign('order_sn',     $_REQUEST['order_sn']);
    assign_query_info();
    $smarty->assign('action_link', array('text' => '返回列表', 'href' => 'refund_requests.php?act=list'));
    $smarty->display('refund_requests_list_order.htm');
}
elseif ($_REQUEST['act'] == 'query_order_list')
{
    $list = $refundController->getOrderRefundRequests();
    $refundController->ajaxQueryAction($list['data'], $list['record_count'], false);
}
elseif ($_REQUEST['act'] == 'query')
{
    $list = $refundController->getRefundRequests();
    $refundController->ajaxQueryAction($list['data'], $list['record_count'], false);
}
elseif ($_REQUEST['act'] == 'add')
{
    $order_sn = empty($_REQUEST['order_sn']) ? '' : trim($_REQUEST['order_sn']);
    $accounting = empty($_REQUEST['accounting']) ? '' : trim($_REQUEST['accounting']);
    
    if (empty($order_sn))
    {
        if (empty($accounting)) {
            ecs_header("Location: refund_requests.php?act=list\n");
        } elseif (!$_SESSION['manage_cost'] && !$_SESSION['is_accountant']) {
            $_REQUEST['crm'] ? make_json_error($_LANG['priv_error']) : sys_msg($_LANG['priv_error']);
        }
    }
    else
    {
        $refund_amount = !empty($_REQUEST['refund_amount']) ? floatval($_REQUEST['refund_amount']) : 0;        
        $refundController->assignRefundInfo($order_sn, "", $refund_amount, $_REQUEST['compensate']);
    }
        
    $smarty->assign('ur_here',      $_LANG['05_refund_requests']);
    
    $smarty->assign('action_link', array('text' => '返回列表', 'href' => 'refund_requests.php?act=list'));

    $salespeople = $adminUserController->getAdminsByPriv(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_REFUND_APP, "sales_name", ['user_id' => 'user_id']);
    $smarty->assign('salesperson_list', $salespeople);

    assign_query_info();
    $smarty->display('refund_request_info.htm');
}
elseif ($_REQUEST['act'] == 'insert')
{
    $link[0]['text'] = '返回訂單';
    $link[0]['href'] = 'order.php?act=info&order_id=' . $_POST['order_id'];
    
    if ($refundController->insertNewRequest()) {
        $msg = '成功提交申請';
    } else {
        $msg = '提交申請失敗';
    }
    $_REQUEST['crm'] ? ($msg == '成功提交申請' ? make_json_response($msg) : make_json_error($msg)) : sys_msg($msg, 0, $link, true);
}
elseif ($_REQUEST['act'] == 'edit_status')
{
    if (!$_SESSION['manage_cost'] && !$_SESSION['is_accountant'] && !check_authz(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_REFUND_PIC))
    {
        make_json_error('Admin only!');
    }
    $refundController->editStatus();
}
elseif ($_REQUEST['act'] == 'edit_operator')
{
    if (!$_SESSION['manage_cost'] && !$_SESSION['is_accountant'] && !check_authz(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_REFUND_PIC))
    {
        make_json_error('Admin only!');
    }

    $rec_id = intval($_POST['id']);
    $value = trim($_POST['val']);

    //remarks: previously hard coded usernames now replaced by the action code AdminuserController::DB_ACTION_CODE_ASSIGNABLE_REFUND_PIC
    //so we depends on the dropdown list
    $db->query("UPDATE " . $ecs->table('refund_requests') . " SET `operator` = '', `operator_id` = '" . $value . "' WHERE `rec_id` = '" . $rec_id . "'");
    make_json_result($value);

}
elseif ($_REQUEST['act'] == 'ajaxEdit')
{
    if (!$_SESSION['manage_cost'] && !$_SESSION['is_accountant'] && !check_authz(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_REFUND_PIC))
    {
        make_json_error('Admin only!');
    }

    $rec_id = intval($_POST['id']);
    $value = trim($_POST['value']);
    
    $db->query("UPDATE " . $ecs->table('refund_requests') . " SET `operator_remark` = '" . $value . "' WHERE `rec_id` = '" . $rec_id . "'");
    make_json_result($value);
}
elseif ($_REQUEST['act'] == 'edit_requester_remark')
{
    $rec_id = intval($_POST['id']);
    $value = $refundController->remarkEncode(-1);
    
    $status = $db->getOne("SELECT status FROM " . $ecs->table('refund_requests') . " WHERE `rec_id` = '" . $rec_id . "'");
    if ($status != REFUND_PENDING)
    {
        make_json_error('只可以更改未處理的申請');
    }
    
    $refundController->updateOrderPaymentTransactionIds($rec_id);
    
    $db->query("UPDATE " . $ecs->table('refund_requests') . " SET `requester_remark` = '" . $value . "' WHERE `rec_id` = '" . $rec_id . "'");
    make_json_result($refundController->remarkDecode($value));
}
elseif ($_REQUEST['act'] == 'edit_refund_amount' || $_REQUEST['act'] == 'edit_integral_amount')
{
    $rec_id = intval($_POST['id']);
    $value = doubleval($_POST['val']);
    
    $status = $db->getOne("SELECT status FROM " . $ecs->table('refund_requests') . " WHERE `rec_id` = '" . $rec_id . "'");
    if ($status != REFUND_PENDING)
    {
        make_json_error('只可以更改未處理的申請');
    }
    
    $db->query("UPDATE " . $ecs->table('refund_requests') . " SET " . ($_REQUEST['act'] == 'edit_refund_amount' ? "`refund_amount`" : "`integral_amount`") . " = '" . $value . "' WHERE `rec_id` = '" . $rec_id . "'");
    make_json_result($value);
}
elseif ($_REQUEST['act'] == 'order_refund')
{
    $refundController->orderRefundProcess();
}
elseif ($_REQUEST['act'] == 'process_refund')
{
    $refundController->initRefundProcess();
}
elseif ($_REQUEST['act'] == 'month_report')
{
    $list = $refundController->getMonthReport();
    $smarty->assign('list', $list);
    $smarty->assign('ur_here', '退款紀錄');
    $smarty->assign('action_link', array('text' => '返回列表', 'href' => 'refund_requests.php?act=list'));
    $smarty->display('refund_requests_report.htm');
}
elseif ($_REQUEST['act'] == 'get_pay_log')
{
    $order_id = !empty($_REQUEST['order_id']) ? $_REQUEST['order_id'] : "";
    if (!empty($order_id) && is_online_payment($order_id)) {
        return make_json_response($refundController->getOnlineTxnLog($order_id));
    }
    return make_json_response([]);
}
?>