<?php

/***
* upcoming_birthdays.php
* by howang 2014-08-21
*
* View users with upcoming birthday
***/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
$smarty->assign('lang', $_LANG);

if ((isset($_REQUEST['act'])) && (($_REQUEST['act'] == 'query') || ($_REQUEST['act'] == 'download')))
{
	/* 检查权限 */
	check_authz_json('users_manage');
	
	/*------------------------------------------------------ */
	//--Excel文件下载
	/*------------------------------------------------------ */
	if ($_REQUEST['act'] == 'download')
	{
		$users_data = get_upcoming_birthday_users(false);
		header("Content-type: application/vnd.ms-excel; charset=utf-8");
		header("Content-Disposition: attachment; filename=upcoming_birthdays.xls");

		require_once ROOT_PATH . 'includes/excel/PHPExcel.php';
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		
		$objPHPExcel->getActiveSheet()->setTitle('即將生日的會員');
		
		/* 文件标题 */
		$objPHPExcel->getActiveSheet()
		->setCellValue('A1', ecs_iconv(EC_CHARSET, 'UTF8', '即將生日的會員'))
		->setCellValue('A2', ecs_iconv(EC_CHARSET, 'UTF8', '會員ID'))
		->setCellValue('B2', ecs_iconv(EC_CHARSET, 'UTF8', '會員電話'))
		->setCellValue('C2', ecs_iconv(EC_CHARSET, 'UTF8', '會員名稱'))
		->setCellValue('D2', ecs_iconv(EC_CHARSET, 'UTF8', '會員電郵'))
		->setCellValue('E2', ecs_iconv(EC_CHARSET, 'UTF8', '生日日期'))
		->setCellValue('F2', ecs_iconv(EC_CHARSET, 'UTF8', '距離生日天數'))
		->setCellValue('G2', ecs_iconv(EC_CHARSET, 'UTF8', 'Wish List'));
		$objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
		$i = 3;
		foreach ($users_data['data'] AS $key => $value)
		{
			$wish_list = array_reduce($value['wish_list'], function ($str, $goods) {
				return (empty($str) ? '' : $str . "\r\n") . $goods['goods_name'];
			}, '');
			
			$objPHPExcel->getActiveSheet()
			->setCellValue('A'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['user_id']))
			->setCellValue('B'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['user_name']))
			->setCellValue('C'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['display_name']))
			->setCellValue('D'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['email']))
			->setCellValue('E'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['birthday']))
			->setCellValue('F'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['days_to_birthday']))
			->setCellValue('G'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $wish_list));
			$i++;
		}
		
		for ($col = 'A'; $col <= 'G'; $col++)
		{
			$objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
		}
		
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		
		exit;
	}
	
	$users_data = get_upcoming_birthday_users();
	$smarty->assign('users_list',      $users_data['data']);
	$smarty->assign('filter',          $users_data['filter']);
	$smarty->assign('record_count',    $users_data['record_count']);
	$smarty->assign('page_count',      $users_data['page_count']);

	$sort_flag = sort_flag($users_data['filter']);
	$smarty->assign($sort_flag['tag'], $sort_flag['img']);

	make_json_result($smarty->fetch('upcoming_birthdays.htm'), '', array('filter' => $users_data['filter'], 'page_count' => $users_data['page_count']));
}
else
{
	/* 权限判断 */
	admin_priv('users_manage');

	$users_data = get_upcoming_birthday_users();
	/* 赋值到模板 */
	$smarty->assign('filter',       $users_data['filter']);
	$smarty->assign('record_count', $users_data['record_count']);
	$smarty->assign('page_count',   $users_data['page_count']);
	$smarty->assign('users_list',   $users_data['data']);
	$smarty->assign('ur_here',      $_LANG['12_upcoming_birthdays']);
	$smarty->assign('full_page',    1);
	$smarty->assign('cfg_lang',     $_CFG['lang']);
	$smarty->assign('from_date',    $users_data['filter']['from_date']);
	$smarty->assign('days',         $users_data['filter']['days']);
	
	$smarty->assign('action_link',      array('text' => '下載Excel報表','href'=>'#download'));

	$sort_flag = sort_flag($users_data['filter']);
	$smarty->assign($sort_flag['tag'], $sort_flag['img']);

	/* 显示页面 */
	assign_query_info();
	$smarty->display('upcoming_birthdays.htm');
}

function get_upcoming_birthday_users($is_pagination = true)
{
	global $db, $ecs, $_CFG;

	$filter['from_date']  = empty($_REQUEST['from_date'])  ? local_date('Y-m-d') : trim($_REQUEST['from_date']);
	$filter['days']       = empty($_REQUEST['days'])       ? 7 : intval($_REQUEST['days']);

	$filter['sort_by']    = empty($_REQUEST['sort_by'])    ? 'days_to_birthday' : trim($_REQUEST['sort_by']);
	$filter['sort_order'] = empty($_REQUEST['sort_order']) ? 'ASC' : trim($_REQUEST['sort_order']);

	$sql = "SELECT count(*) FROM " . $ecs->table('users') .
		   " WHERE `birthday` != '0000-00-00' " .
		   " AND (" .
		   " DATEDIFF(CONCAT(YEAR('" . $filter['from_date'] . "'),'-',MONTH(`birthday`),'-',DAY(`birthday`)), '" . $filter['from_date'] . "') BETWEEN 0 AND " . $filter['days'] . " " .
		   " OR " .
		   " DATEDIFF(CONCAT(YEAR('" . $filter['from_date'] . "')+1,'-',MONTH(`birthday`),'-',DAY(`birthday`)), '" . $filter['from_date'] . "') BETWEEN 0 AND " . $filter['days'] . " " .
		   " ) ";
	$filter['record_count'] = $db->getOne($sql);

	/* 分页大小 */
	$filter = page_and_size($filter);

	$sql = "SELECT *, IF(diff1 < 0, diff2, diff1) as `days_to_birthday` " .
		   "FROM ( " .
			"SELECT u.*, rei.`content` as `display_name`, " .
			"DATEDIFF(CONCAT(YEAR('" . $filter['from_date'] . "'),'-',MONTH(`birthday`),'-',DAY(`birthday`)), '" . $filter['from_date'] . "') as diff1, " .
			"DATEDIFF(CONCAT(YEAR('" . $filter['from_date'] . "')+1,'-',MONTH(`birthday`),'-',DAY(`birthday`)), '" . $filter['from_date'] . "') as diff2 " .
			"FROM " . $ecs->table('users') . " as u " .
			"LEFT JOIN " . $ecs->table('reg_extend_info') . " as rei ON rei.`user_id` = u.`user_id` AND rei.`reg_field_id` = 10 " .
			"WHERE u.`birthday` != '0000-00-00' " .
		   ") as tmp " .
		   "HAVING days_to_birthday <= " . $filter['days'] . " " .
		   "ORDER BY " . $filter['sort_by'] . " " . $filter['sort_order'];

	if ($is_pagination)
	{
		$sql .= " LIMIT " . $filter['start'] . ', ' . $filter['page_size'];
	}
	$data = $db->getAll($sql);
	
	// Get Wish Lists for all selected users at once
	$sql = "SELECT wl.*, g.`goods_name` " .
		   "FROM " . $ecs->table('wish_list') . " as wl " .
		   "LEFT JOIN " . $ecs->table('goods') . " as g ON g.goods_id = wl.goods_id " .
		   "WHERE wl.user_id " . db_create_in(array_map(function ($row) { return $row['user_id']; }, $data));
	$wish_lists = $db->getAll($sql);
	
	foreach ($data as $key => $row)
	{
		$data[$key]['display_name'] = empty($row['display_name']) ? $row['user_name'] : $row['display_name'];
		
		$data[$key]['wish_list'] = array_filter($wish_lists, function ($list) use ($row) { return $list['user_id'] == $row['user_id']; } );
	}

	$arr = array(
		'data' => $data,
		'filter' => $filter,
		'page_count' => $filter['page_count'],
		'record_count' => $filter['record_count']
	);
	return $arr;
}

?>