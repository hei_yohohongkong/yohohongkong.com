<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require_once(ROOT_PATH .'/includes/lib_time.php');
$accountingController = new Yoho\cms\Controller\AccountingController();

if ($_REQUEST['act'] == 'list') {
    $start_date = checkDateIsValid($_REQUEST['start_date']) ? $_REQUEST['start_date'] : null;
    $end_date = checkDateIsValid($_REQUEST['end_date']) ? $_REQUEST['end_date'] : null;
    $smarty->assign('ur_here', $_LANG['trial_balance']);
    $smarty->assign('start_date', $start_date);
    $smarty->assign('end_date', $end_date);
    $smarty->assign('balance', $accountingController->getAccountingBalance($start_date, $end_date));
    $smarty->display('trial_balance.htm');
}

?>