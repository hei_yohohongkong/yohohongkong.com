<?php

define('IN_ECS',true);
require_once(dirname(__FILE__) .'/includes/init.php');
require_once(dirname(__FILE__) .'/includes/ERP/lib_erp_base.php');
require_once(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require_once(dirname(__FILE__) .'/includes/ERP/lib_erp_goods.php');
require_once(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
require_once(dirname(__FILE__) .'/includes/ERP/lib_erp_warehouse.php');
require_once(dirname(__FILE__) .'/includes/ERP/cls/cls_date.php');
$warehouseTransferController = new Yoho\cms\Controller\WarehouseTransferController();
$erpController = new Yoho\cms\Controller\ErpController();
$adminuserController = new Yoho\cms\Controller\AdminuserController();

if ($_REQUEST['act'] == 'list') {

	if (empty($_REQUEST['transfer_id'])) {
		header("Location: erp_warehouse_transfer.php?act=list");
		die();
	}
    
	$warehouseTransferController->checkStatusRedirect($_REQUEST['transfer_id'], 'finished');
	$warehouse_list = $erpController->getWarehouseList(erp_get_admin_id(),0);
	$reason_type_options = $warehouseTransferController->getReasonTypeOptions();
	$transfer_info = $warehouseTransferController->getTransferInfo($_REQUEST['transfer_id']);
	$pick_user_options = $warehouseTransferController->personInChargeOption();

	$adjust_warehouse_list_options[] = array ('id' => $transfer_info['warehouse_from'], 'name' => $transfer_info['warehouse_from_name'] );
	$adjust_warehouse_list_options[] = array ('id' => $transfer_info['warehouse_to'], 'name' => $transfer_info['warehouse_to_name'] );

	$smarty->assign('adjust_warehouse_list_options', $adjust_warehouse_list_options);
	$smarty->assign('pick_user_options', $pick_user_options);
	$smarty->assign('step', 7);
	$smarty->assign('transfer_info', $transfer_info);
	$smarty->assign('transfer_id', $_REQUEST['transfer_id']);
    $smarty->assign('warehouse_list',$warehouse_list);
	$smarty->assign('reason_type_options',$reason_type_options);
    $smarty->assign('brand_list',$result['brand_list']);
    $smarty->assign('ur_here', '新增調貨');
    $smarty->assign('full_page', 1);
    $smarty->assign('cfg_lang', $_CFG['lang']);
    $smarty->assign('lang', $_LANG);
    $smarty->assign('action_link', array('text' => '調貨清單', 'href' => 'erp_warehouse_transfer.php'));

    assign_query_info();
    $smarty->display('erp_warehouse_transfer_finished.htm');

} else if ($_REQUEST['act'] == 'post_search_product_by_sku') {
    $result = $warehouseTransferController->searchProductBySku();
    if(!isset($result['error'])){
        make_json_result($result);
    }else{
     	make_json_error($result['error']);
    }
} elseif ($_REQUEST['act'] == 'query') {
    $result = $warehouseTransferController->getTransferProducts(true,7);
    if ($result != false){
        $warehouseTransferController->ajaxQueryAction($result['data'],$result['record_count'],false,$result['extra_data']);
    }else{
        $result['data'] = array();
        $result['record_count'] = 0;
        $result['extra_data'] = array();
        $stocktakeController->ajaxQueryAction($result['data'],$result['record_count'],false,$result['extra_data']);
    }
} elseif ($_REQUEST['act'] == 'post_transfer_item_delete') {
    $result = $warehouseTransferController->transferItemDelete();
	if(isset($result['error'])){
		make_json_error($result['error']);
	}else{
		make_json_result($result);
	}
} elseif ($_REQUEST['act'] == 'post_transfer_qty_update') {
    $result = $warehouseTransferController->transferQtyUpdate();
	if(isset($result['error'])){
		make_json_error($result['error']);
	}else{
		make_json_result($result);
	}
} elseif ($_REQUEST['act'] == 'post_admin_from_update') {
    $result = $warehouseTransferController->adminFromUpdate();
	if(isset($result['error'])){
		make_json_error(implode('<br>',$result['error']));
	}else{
		make_json_result($result);
	}
} elseif ($_REQUEST['act'] == 'post_import_data_cross_check') {
    $result = $warehouseTransferController->importDataCrossCheck();
	
	make_json_result($result);
	
} elseif ($_REQUEST['act'] == 'post_transfer_confirm_check') {
    $result = $warehouseTransferController->transferConfirmCheck();
	if(isset($result['error'])){
		make_json_error(implode('<br>',$result['error']));
	}else{
		make_json_result($result);
	}
} elseif ($_REQUEST['act'] == 'post_transfer_confirm') {
    $result = $warehouseTransferController->transferConfirm();
	if(isset($result['error'])){
		make_json_error(implode('<br>',$result['error']));
	}else{
		make_json_result($result);
	}
} elseif ($_REQUEST['act'] == 'post_adjust_transfer_warehouse_stock' ) {
	$result = $warehouseTransferController->adjustTransferWarehouseStockSubmit();
	if(isset($result['error'])){
		make_json_error(implode('<br>',$result['error']));
	}else{
		make_json_result($result);
	}
} elseif ($_REQUEST['act'] == 'post_adjust_transfer_warehouse_stock_view' ) {
	$result = $warehouseTransferController->adjustTransferWarehouseStockView($_REQUEST['adjustment_id']);
	if(isset($result['error'])){
		make_json_error(implode('<br>',$result['error']));
	}else{
		make_json_result($result);
	}
} else if ($_REQUEST['act'] == 'post_adjust_warehouse_admin_confirm') {
	$result = $warehouseTransferController->adjustWarehouseAdminConfirm($_REQUEST['adjustment_id'],$_REQUEST['adjust_warehouse_side']);
	if(isset($result['error'])){
		make_json_error(implode('<br>',$result['error']));
	}else{
		make_json_result($result);
	}
} else if ($_REQUEST['act'] == 'post_adjust_warehouse_stock_delete') {
	$result = $warehouseTransferController->adjustWarehouseAdminDelete($_REQUEST['adjustment_id']);
	if(isset($result['error'])){
		make_json_error(implode('<br>',$result['error']));
	}else{
		make_json_result($result);
	}
}



?>