<?php

/**
 * ECSHOP 商品管理程序
 * ============================================================================
 * 版权所有 2005-2010 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: wangleisvn $
 * $Id: goods.php 17114 2010-04-16 07:13:03Z wangleisvn $
*/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_goods.php');
include_once(ROOT_PATH . '/includes/cls_image.php');
$image = new cls_image($_CFG['bgcolor']);
$exc = new exchange($ecs->table('goods'), $db, 'goods_id', 'goods_name');
$attributeController = new Yoho\cms\Controller\AttributeController();
$categoryController  = new Yoho\cms\Controller\CategoryController();
$goodsController  = new Yoho\cms\Controller\GoodsController();
$autoPricingController = new Yoho\cms\Controller\AutoPricingController();
require_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/ERP/lib_erp_supplier.php');
require_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/ERP/lib_erp_goods.php');
/*------------------------------------------------------ */
//-- 商品列表，商品回收站
/*------------------------------------------------------ */
if ($_REQUEST['act'] == 'list' || $_REQUEST['act'] == 'trash')
{
    $vip_abnormal_count = vip_abnormal_count(0);
    $smarty->assign('vip_abnormal_count', $vip_abnormal_count);
    $goodsController->listAction();
}

/*------------------------------------------------------ */
//-- 添加新商品 编辑商品
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'add' || $_REQUEST['act'] == 'edit' || $_REQUEST['act'] == 'copy')
{

    $category_list = cat_list(0, $goods['cat_id'], false, 0, true, true);

    $_REQUEST['goods_id']=intval($_REQUEST['id']);
    //copy x/auto_pricing.php start
    if (!empty($_REQUEST['id'])) {
        $good = $autoPricingController->getAutoPricingProduct();
    }
    $list = $autoPricingController->getAutoPricingStrategyList();
    if (!empty($_REQUEST['ids'])) {
        $smarty->assign("batch", 1);
    }
    $smarty->assign("good", $good);
    $smarty->assign("list", $list);
    //copy x/auto_pricing.php end
    $is_add = $_REQUEST['act'] == 'add'; // 添加还是编辑的标识
    $is_copy = $_REQUEST['act'] == 'copy'; //是否复制
    $unset_fields = [                   //fields to be cleared in copy mode
        'goods_name',
        'keywords',
        'goods_brief',
        'goods_desc',
        'inthebox',
        'unboxing_page',
        'official_website',
        'goods_number',
        'pricecomhk_id',
        'hktvmall_id',
        'fortress_id',
        'pricecomhk_water',
        'integral',
        'proddb_id',
        'proddb_name',
        'market_price',
        'market_price',
        'shop_price',
        'guarantee',
        'goods_length',
        'goods_width',
        'goods_height',
        'require_goods_id'
    ];
    $smarty->assign('is_copy', $is_copy);
    $code = empty($_REQUEST['extension_code']) ? '' : trim($_REQUEST['extension_code']);
    if ($code == 'virual_card')
    {
        admin_priv('virualcard'); // 检查权限
    }
    else
    {
        admin_priv('goods_manage'); // 检查权限g
    }

    /* 供货商名 */
    if ($is_add) // YOHO: default select NA supplier
    {
        $sql = "SELECT supplier_id FROM ".$GLOBALS['ecs']->table('erp_supplier')." WHERE name = 'NA'";
    	$supplier_ids = $GLOBALS['db']->getCol($sql);
    }
    else // edit / copy
    {
        $sql="select supplier_id from ".$GLOBALS['ecs']->table('erp_goods_supplier')." where goods_id='".$_REQUEST['goods_id']."'";
    	$supplier_ids = $GLOBALS['db']->getCol($sql);
    }
    $suppliers_list_name = suppliers_list_name_by_admin($supplier_ids);
    $smarty->assign('suppliers_list_name', $suppliers_list_name);
    $missing_supplier_ids = array();
    foreach ($supplier_ids as $supplier_id)
    {
        if (!isset($suppliers_list_name[$supplier_id]) && !in_array($supplier_id, $missing_supplier_ids))
        {
            $missing_supplier_ids[] = $supplier_id;
        }
    }
    $smarty->assign('missing_supplier_ids', $missing_supplier_ids);
    
    $warranty_templates = $db->getAll("SELECT template_id, template_name FROM " . $ecs->table('warranty_templates') . " ORDER BY template_id ASC");
    $smarty->assign('warranty_templates', $warranty_templates);
    $shipping_templates = $db->getAll("SELECT template_id, template_name FROM " . $ecs->table('shipping_templates') . " ORDER BY template_id ASC");
    $smarty->assign('shipping_templates', $shipping_templates);
    
    // Multiple language support
    $localizable_fields = localizable_fields_for_table('goods');
    $smarty->assign('localizable_fields', $localizable_fields);
    if (!$is_add)
    {
        $localized_versions = get_localized_versions('goods', $_REQUEST['goods_id'], $localizable_fields);
        
        if ($is_copy)
        {
            // Don't copy certain feilds
            foreach ($localized_versions as $l => $v)
            {
                foreach($unset_fields as $field){
                    if (isset($v[$field]))
                    {
                        unset($localized_versions[$l][$field]);
                    }
                }
            }
        }
        
        $smarty->assign('localized_versions', $localized_versions);
    }
    
    /* 如果是安全模式，检查目录是否存在 */
    if (ini_get('safe_mode') == 1 && (!file_exists('../' . IMAGE_DIR . '/'.date('Ym')) || !is_dir('../' . IMAGE_DIR . '/'.date('Ym'))))
    {
        if (@!mkdir('../' . IMAGE_DIR . '/'.date('Ym'), 0777))
        {

            $warning = sprintf($_LANG['safe_mode_warning'], '../' . IMAGE_DIR . '/'.date('Ym'));
            $smarty->assign('warning', $warning);
        }
    }

    /* 如果目录存在但不可写，提示用户 */
    elseif (file_exists('../' . IMAGE_DIR . '/'.date('Ym')) && file_mode_info('../' . IMAGE_DIR . '/'.date('Ym')) < 2)
    {
        $warning = sprintf($_LANG['not_writable_warning'], '../' . IMAGE_DIR . '/'.date('Ym'));
        $smarty->assign('warning', $warning);
    }

    /* 取得商品信息 */
    if ($is_add)
    {
        /* 默认值 */
        $last_choose = array(0, 0);
        if (!empty($_COOKIE['ECSCP']['last_choose']))
        {
            $last_choose = explode('|', $_COOKIE['ECSCP']['last_choose']);
            $sql = "SELECT brand_name FROM " . $ecs->table('brand') . " WHERE brand_id = '$last_choose[1]'";
            $brand_name = $db->getRow($sql)["brand_name"];
        }

        $goods = array(
            'goods_id'      => 0,
            'goods_desc'    => '',
            'guarantee'     => '',
            'inthebox'      => '',
            'cat_id'        => $last_choose[0],
            'brand_id'      => $last_choose[1],
            'brand_name'    => $brand_name,
            'is_on_sale'    => '1',
            'is_alone_sale' => '1',
            'is_shipping' => '0',
            'other_cat'     => array(), // 扩展分类
            'goods_type'    => 0,       // 商品类型
            'shop_price'    => 0,
            'promote_price' => 0,
            'market_price'  => 0,
            'integral'      => 0,
            'warn_number'   => 1,
            'promote_start_date' => local_date('Y-m-d H:i:s'),
            'promote_end_date'   => local_date('Y-m-d 23:59:59', local_strtotime('+1 month')),
            'goods_weight'  => 0,
            'give_integral' => -1,
            'rank_integral' => -1,
            'commission' => 0,
            'shipping_price' => 0,
            'proddb_id' => 0,
            'warranty_template_id' => 0,
            'shipping_template_id' => 0,
            'pricecomhk_id' => '',
            'hktvmall_id' => '',
            'fortress_id' => '',
            'pricecomhk_water' => 0,
            'goods_length'  => 0,
            'goods_width'  => 0,
            'goods_height'  => 0,
            'display_discount' => 1,
            'require_goods_id' => 0
        );
        $goods['cat_name'] = $category_list[$last_choose[0]]["cat_name"];

        /* 关联商品 */
        $link_goods_list = array();
        $sql = "DELETE FROM " . $ecs->table('link_goods') .
                " WHERE (goods_id = 0 OR link_goods_id = 0)" .
                " AND admin_id = '$_SESSION[admin_id]'";
        $db->query($sql);

        /* 组合商品 */
        $group_goods_list = array();
        $sql = "DELETE FROM " . $ecs->table('group_goods') .
                " WHERE parent_id = 0 AND admin_id = '$_SESSION[admin_id]'";
        $db->query($sql);

        /* 关联文章 */
        $goods_article_list = array();
        $sql = "DELETE FROM " . $ecs->table('goods_article') .
                " WHERE goods_id = 0 AND admin_id = '$_SESSION[admin_id]'";
        $db->query($sql);

        /* 属性 */
        $sql = "DELETE FROM " . $ecs->table('goods_attr') . " WHERE goods_id = 0";
        $db->query($sql);

        /* 图片列表 */
        $img_list = array();

        /* 自訂欄位 */
        $extra_info_list = array();

        /* 推廣鏈結 */
        $promo_links_list = array();
    }
    else
    {
        /* 商品信息 */
        $sql = "SELECT g.*, b.brand_name FROM " . $ecs->table('goods') . "as g LEFT JOIN " . $ecs->table('brand') . " AS b on  g.brand_id = b.brand_id WHERE g.goods_id = '$_REQUEST[goods_id]'";
        $goods = $db->getRow($sql);

        if (empty($goods) === true)
        {
            /* 默认值 */
            $goods = array(
                'goods_id'      => 0,
                'goods_desc'    => '',
                'guarantee'     => '',
                'inthebox'      => '',
                'cat_id'        => 0,
                'is_on_sale'    => '1',
                'is_alone_sale' => '1',
                'is_shipping' => '0',
                'other_cat'     => array(), // 扩展分类
                'goods_type'    => 0,       // 商品类型
                'shop_price'    => 0,
                'promote_price' => 0,
                'market_price'  => 0,
                'integral'      => 0,
                'warn_number'   => 1,
                'promote_start_date' => local_date('Y-m-d H:i:s'),
                'promote_end_date'   => local_date('Y-m-d 23:59:59', gmstr2time('+1 month')),
                'goods_weight'  => 0,
                'give_integral' => -1,
                'rank_integral' => -1,
                'goods_number' => 0,
                'commission' => 0,
                'shipping_price' => 0,
                'proddb_id' => 0,
                'warranty_template_id' => 0,
                'shipping_template_id' => 0,
                'pricecomhk_id' => '',
                'hktvmall_id' => '',
                'fortress_id' => '',
                'pricecomhk_water' => 0,
                'goods_length'  => 0,
                'goods_width'  => 0,
                'goods_height'  => 0,
                'display_discount' => 1,
                'require_goods_id' => 0
            );
        }
        else
        {
            $goods['goods_number'] = get_goods_qty($_REQUEST['goods_id']);
            $goods['pricecomhk_id'] = $db->getOne("SELECT pricecomhk_id FROM " . $ecs->table('goods_pricecomhk') . " WHERE goods_id = '" . $_REQUEST['goods_id'] . "'");
            $goods['hktvmall_id'] = $db->getOne("SELECT hktvmall_id FROM " . $ecs->table('goods_hktvmall') . " WHERE goods_id = '" . $_REQUEST['goods_id'] . "'");
            $goods['fortress_id'] = $db->getOne("SELECT fortress_id FROM " . $ecs->table('goods_fortress') . " WHERE goods_id = '" . $_REQUEST['goods_id'] . "'");
            $goods['pricecomhk_water'] = $db->getOne("SELECT is_water FROM " . $ecs->table('goods_pricecomhk') . " WHERE goods_id = '" . $_REQUEST['goods_id'] . "'");
            if($goods['warranty_template_id'] != 0){
                $goods['seller_note'] = $db->getOne("SELECT template_content FROM " . $ecs->table('warranty_templates') . " WHERE template_id = " .$goods['warranty_template_id']);
            }
            if($goods['shipping_template_id'] != 0){
                $goods['shipping_note'] = $db->getOne("SELECT template_content FROM " . $ecs->table('shipping_templates') . " WHERE template_id = " .$goods['shipping_template_id']);
            }
            $goods['cat_name'] = $category_list[$goods['cat_id']]["cat_name"];
        }

        /* 获取商品类型存在规格的类型 */
        $specifications = get_goods_type_specifications();
        $goods['specifications_id'] = $specifications[$goods['goods_type']];
        $_attribute = get_goods_specifications_list($goods['goods_id']);

        $goods['_attribute'] = empty($_attribute) ? '' : 1;

        /* 根据商品重量的单位重新计算 */
        if ($goods['goods_weight'] > 0)
        {
            $goods['goods_weight_by_unit'] = ($goods['goods_weight'] >= 1) ? $goods['goods_weight'] : ($goods['goods_weight'] / 0.001);
        }

        if (!empty($goods['goods_brief']))
        {
            //$goods['goods_brief'] = trim_right($goods['goods_brief']);
            $goods['goods_brief'] = $goods['goods_brief'];
        }
        if (!empty($goods['keywords']))
        {
            //$goods['keywords']    = trim_right($goods['keywords']);
            $goods['keywords']    = $goods['keywords'];
        }

        /* 如果不是促销，处理促销日期 */
        if (isset($goods['is_promote']) && $goods['is_promote'] == '0')
        {
            unset($goods['promote_start_date']);
            unset($goods['promote_end_date']);
            $goods['promote_start_date'] = local_date('Y-m-d H:i:s');
            $goods['promote_end_date']   = local_date('Y-m-d 23:59:59', local_strtotime('+1 month'));
        }
        else
        {
            if(empty($goods['promote_start_date'])) $goods['promote_start_date'] = local_date('Y-m-d H:i:s');
            else $goods['promote_start_date'] = local_date('Y-m-d H:i:s', $goods['promote_start_date']);
            if(empty($goods['promote_end_date'])) $goods['promote_end_date'] = local_date('Y-m-d 23:59:59', local_strtotime('+1 month'));
            else $goods['promote_end_date'] = local_date('Y-m-d H:i:s', $goods['promote_end_date']);
        }
        $goods = $goodsController->getGoodsPreOrderInfo($goods);
        $goods['pre_sale_date'] = $goods['old_pre_sale_date'];
        if (!empty($goods['pre_sale_date']))
        {
            $goods['pre_sale_date'] = local_date('Y-m-d', $goods['pre_sale_date']);
        } else {
            unset($goods['pre_sale_date']);
        }
        /* 如果是复制商品，处理 */
        if ($_REQUEST['act'] == 'copy')
        {
            // 商品信息
            $goods['goods_id'] = 0;
            $goods['goods_sn'] = '';
            $goods['goods_name'] = '';
            $goods['goods_img'] = '';
            $goods['goods_thumb'] = '';
            $goods['original_img'] = '';

            // 扩展分类不变

            // 关联商品
            $sql = "DELETE FROM " . $ecs->table('link_goods') .
                    " WHERE (goods_id = 0 OR link_goods_id = 0)" .
                    " AND admin_id = '$_SESSION[admin_id]'";
            $db->query($sql);

            $sql = "SELECT '0' AS goods_id, link_goods_id, is_double, '$_SESSION[admin_id]' AS admin_id" .
                    " FROM " . $ecs->table('link_goods') .
                    " WHERE goods_id = '$_REQUEST[goods_id]' ";
            $res = $db->query($sql);
            while ($row = $db->fetchRow($res))
            {
                $db->autoExecute($ecs->table('link_goods'), $row, 'INSERT');
            }

            $sql = "SELECT goods_id, '0' AS link_goods_id, is_double, '$_SESSION[admin_id]' AS admin_id" .
                    " FROM " . $ecs->table('link_goods') .
                    " WHERE link_goods_id = '$_REQUEST[goods_id]' ";
            $res = $db->query($sql);
            while ($row = $db->fetchRow($res))
            {
                $db->autoExecute($ecs->table('link_goods'), $row, 'INSERT');
            }

            // 配件
            $sql = "DELETE FROM " . $ecs->table('group_goods') .
                    " WHERE parent_id = 0 AND admin_id = '$_SESSION[admin_id]'";
            $db->query($sql);

            $sql = "SELECT 0 AS parent_id, goods_id, goods_price, '$_SESSION[admin_id]' AS admin_id " .
                    "FROM " . $ecs->table('group_goods') .
                    " WHERE parent_id = '$_REQUEST[goods_id]' ";
            $res = $db->query($sql);
            while ($row = $db->fetchRow($res))
            {
                $db->autoExecute($ecs->table('group_goods'), $row, 'INSERT');
            }

            // 关联文章
            $sql = "DELETE FROM " . $ecs->table('goods_article') .
                    " WHERE goods_id = 0 AND admin_id = '$_SESSION[admin_id]'";
            $db->query($sql);

            $sql = "SELECT 0 AS goods_id, article_id, '$_SESSION[admin_id]' AS admin_id " .
                    "FROM " . $ecs->table('goods_article') .
                    " WHERE goods_id = '$_REQUEST[goods_id]' ";
            $res = $db->query($sql);
            while ($row = $db->fetchRow($res))
            {
                $db->autoExecute($ecs->table('goods_article'), $row, 'INSERT');
            }

            // 图片不变

            // 商品属性
            $sql = "DELETE FROM " . $ecs->table('goods_attr') . " WHERE goods_id = 0";
            $db->query($sql);

            if (!$is_copy) {
                $sql = "SELECT 0 AS goods_id, attr_id, attr_value, attr_price " .
                        "FROM " . $ecs->table('goods_attr') .
                        " WHERE goods_id = '$_REQUEST[goods_id]' ";
                $res = $db->query($sql);
                while ($row = $db->fetchRow($res))
                {
                    $db->autoExecute($ecs->table('goods_attr'), addslashes_deep($row), 'INSERT');
                }
            }

            // 自訂欄位不变
        }

        // 扩展分类
        $other_cat_list = array();
        $sql = "SELECT cat_id FROM " . $ecs->table('goods_cat') . " WHERE goods_id = '$_REQUEST[goods_id]'";
        $goods['add_time'] = local_date('Y-m-d', $goods['add_time']);
        $goods['other_cat'] = $db->getCol($sql);
        foreach ($goods['other_cat'] AS $cat_id)
        {
            $other_cat_list[$cat_id] = cat_list(0, $cat_id);
        }
        $smarty->assign('other_cat_list', $other_cat_list);

        if(!$is_copy){
            $link_goods_list    = get_linked_goods($goods['goods_id']); // 关联商品
            $group_goods_list   = get_group_goods($goods['goods_id']); // 配件
            $goods_article_list = get_goods_articles($goods['goods_id']);   // 关联文章
            $extra_info_list    = get_goods_extra_info(($_REQUEST['act'] == 'copy') ? $_REQUEST['goods_id'] : $goods['goods_id']); // 自訂欄位
            $promo_links_list   = get_goods_promo_links($goods['goods_id']); // 推廣鏈結
        }
        // 2015-10-29 These fields have been moved into the goods table for simplicity
        // // Get Official website / Unboxing page / Country of origin
        // foreach ($extra_info_list as $extra_info)
        // {
        //     if ($extra_info['info_name'] == '官方網頁')
        //     {
        //         $goods['official_website'] = $extra_info['info_url'];
        //     }
        //     else if ($extra_info['info_name'] == '開箱及評測')
        //     {
        //         $goods['unboxing_page'] = $extra_info['info_url'];
        //     }
        //     else if ($extra_info['info_name'] == '原產地')
        //     {
        //         $goods['country_of_origin'] = $extra_info['info_value'];
        //     }
        // }

        /* 商品图片路径 */
        if (isset($GLOBALS['shop_id']) && ($GLOBALS['shop_id'] > 10) && !empty($goods['original_img']))
        {
            $goods['goods_img'] = get_image_path($_REQUEST['goods_id'], $goods['goods_img']);
            $goods['goods_thumb'] = get_image_path($_REQUEST['goods_id'], $goods['goods_thumb'], true);
        }

        /* 图片列表 */
        $sql = "SELECT * FROM " . $ecs->table('goods_gallery') . " WHERE goods_id = '$goods[goods_id]' ORDER BY `sort_order`";
        $img_list = $db->getAll($sql);

        /* 格式化相册图片路径 */
        if (isset($GLOBALS['shop_id']) && ($GLOBALS['shop_id'] > 0))
        {
            foreach ($img_list as $key => $gallery_img)
            {
                $gallery_img[$key]['img_url'] = get_image_path($gallery_img['goods_id'], $gallery_img['img_original'], false, 'gallery');
                $gallery_img[$key]['thumb_url'] = get_image_path($gallery_img['goods_id'], $gallery_img['img_original'], true, 'gallery');
            }
        }
        else
        {
            foreach ($img_list as $key => $gallery_img)
            {
                $gallery_img[$key]['thumb_url'] = '../' . (empty($gallery_img['thumb_url']) ? $gallery_img['img_url'] : $gallery_img['thumb_url']);
            }
        }
        
        // Get product_name from Product Database
        if (!empty($goods['proddb_id']))
        {
            require_once(ROOT_PATH . ADMIN_PATH . '/includes/lib_proddb.php');
            $goods['proddb_name'] = $proddb->get_product_name_by_id($goods['proddb_id']);
        }

        //Clear fields for copying
        if($is_copy){
            foreach($unset_fields as $field){
                if(isset($goods[$field])){
                    unset($goods[$field]);
                }
            }
        }
    }

    /* 拆分商品名称样式 */
    $goods_name_style = explode('+', empty($goods['goods_name_style']) ? '+' : $goods['goods_name_style']);

    /* 模板赋值 */
    $smarty->assign('code',    $code);
    $smarty->assign('ur_here', $is_add ? (empty($code) ? $_LANG['02_goods_add'] : $_LANG['51_virtual_card_add']) : ($_REQUEST['act'] == 'edit' ? $_LANG['edit_goods'] : $_LANG['copy_goods']));
    $smarty->assign('action_link', list_link($is_add, $code));
    $smarty->assign('goods', $goods);
    $smarty->assign('goods_name_color', $goods_name_style[0]);
    $smarty->assign('goods_name_style', $goods_name_style[1]);
    //$smarty->assign('cat_list', cat_list(0, $goods['cat_id']));
    $smarty->assign('cat_list', $category_list);
    $smarty->assign('brand_list', get_brand_list());
    $smarty->assign('unit_list', get_unit_list());
    $smarty->assign('user_rank_list', get_user_rank_list());
    $smarty->assign('weight_unit', $is_add ? '1' : ($goods['goods_weight'] >= 1 ? '1' : '0.001'));
    $smarty->assign('cfg', $_CFG);
    $smarty->assign('form_act', $is_add ? 'insert' : ($_REQUEST['act'] == 'edit' ? 'update' : 'insert'));
    if ($_REQUEST['act'] == 'add' || $_REQUEST['act'] == 'edit')
    {
        $smarty->assign('is_add', true);
    }
    if(!$is_add && !$is_copy)
    {
        $smarty->assign('member_price_list', get_member_price_list($_REQUEST['goods_id']));
    }
    $smarty->assign('link_goods_list', $link_goods_list);
    $smarty->assign('group_goods_list', $group_goods_list);
    $smarty->assign('goods_article_list', $goods_article_list);
    $smarty->assign('extra_info_list', $extra_info_list);
    $smarty->assign('promo_links_list', $promo_links_list);
    $smarty->assign('img_list', $img_list);
    // $smarty->assign('goods_type_list', goods_type_list($goods['goods_type']));
    $smarty->assign('goods_type_list', cat_list(0, $goods['cat_id']));

    $smarty->assign('has_stock', isset($goods['goods_number'])&& $goods['goods_number']>0 ? 1:0);

    $smarty->assign('gd', gd_version());
    $smarty->assign('thumb_width', $_CFG['thumb_width']);
    $smarty->assign('thumb_height', $_CFG['thumb_height']);
    $smarty->assign('goods_attr_html', build_attr_html($goods['goods_type'], $goods['goods_id']));
    $volume_price_list = '';
    if(isset($_REQUEST['goods_id']))
    {
        $volume_price_list = get_volume_price_list($_REQUEST['goods_id']);
    }
    if (empty($volume_price_list))
    {
        $volume_price_list = array('0'=>array('number'=>'','price'=>''));
    }
    $smarty->assign('volume_price_list', $volume_price_list);
    /* 显示商品信息页面 */
    assign_query_info();
    $smarty->display('goods_info.htm');
}

/*------------------------------------------------------ */
//-- 插入商品 更新商品
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'insert' || $_REQUEST['act'] == 'update')
{
    $code = empty($_REQUEST['extension_code']) ? '' : trim($_REQUEST['extension_code']);

    /* 是否处理缩略图 */
    $proc_thumb = (isset($GLOBALS['shop_id']) && $GLOBALS['shop_id'] > 0)? false : true;
    if ($code == 'virtual_card')
    {
        admin_priv('virualcard'); // 检查权限
    }
    else
    {
        admin_priv('goods_manage'); // 检查权限
    }

    /* 检查货号是否重复 */
    if ($_POST['goods_sn'])
    {
        $sql = "SELECT COUNT(*) FROM " . $ecs->table('goods') .
                " WHERE goods_sn = '$_POST[goods_sn]' AND is_delete = 0 AND goods_id <> '$_POST[goods_id]'";
        if ($db->getOne($sql) > 0)
        {
            sys_msg($_LANG['goods_sn_exists'], 1, array(), false);
        }
    }

    /* 检查图片：如果有错误，检查尺寸是否超过最大值；否则，检查文件类型 */
    if (isset($_FILES['goods_img']['error'])) // php 4.2 版本才支持 error
    {
        // 最大上传文件大小
        $php_maxsize = ini_get('upload_max_filesize');
        $htm_maxsize = '2M';

        // 商品图片
        if ($_FILES['goods_img']['error'] == 0)
        {
            if (!$image->check_img_type($_FILES['goods_img']['type']))
            {
                sys_msg($_LANG['invalid_goods_img'], 1, array(), false);
            }
        }
        elseif ($_FILES['goods_img']['error'] == 1)
        {
            sys_msg(sprintf($_LANG['goods_img_too_big'], $php_maxsize), 1, array(), false);
        }
        elseif ($_FILES['goods_img']['error'] == 2)
        {
            sys_msg(sprintf($_LANG['goods_img_too_big'], $htm_maxsize), 1, array(), false);
        }

        // 商品缩略图
        if (isset($_FILES['goods_thumb']))
        {
            if ($_FILES['goods_thumb']['error'] == 0)
            {
                if (!$image->check_img_type($_FILES['goods_thumb']['type']))
                {
                    sys_msg($_LANG['invalid_goods_thumb'], 1, array(), false);
                }
            }
            elseif ($_FILES['goods_thumb']['error'] == 1)
            {
                sys_msg(sprintf($_LANG['goods_thumb_too_big'], $php_maxsize), 1, array(), false);
            }
            elseif ($_FILES['goods_thumb']['error'] == 2)
            {
                sys_msg(sprintf($_LANG['goods_thumb_too_big'], $htm_maxsize), 1, array(), false);
            }
        }

        // 相册图片
        if (isset($_FILES['img_url']))
        {
            foreach ($_FILES['img_url']['error'] AS $key => $value)
            {
                if ($value == 0)
                {
                    if (!$image->check_img_type($_FILES['img_url']['type'][$key]))
                    {
                        sys_msg(sprintf($_LANG['invalid_img_url'], $key + 1), 1, array(), false);
                    }
                }
                elseif ($value == 1)
                {
                    sys_msg(sprintf($_LANG['img_url_too_big'], $key + 1, $php_maxsize), 1, array(), false);
                }
                elseif ($_FILES['img_url']['error'] == 2)
                {
                    sys_msg(sprintf($_LANG['img_url_too_big'], $key + 1, $htm_maxsize), 1, array(), false);
                }
            }
        }
    }
    /* 4.1版本 */
    else
    {
        // 商品图片
        if ($_FILES['goods_img']['tmp_name'] != 'none' && !empty($_FILES['goods_img']['tmp_name']))
        {
            if (!$image->check_img_type($_FILES['goods_img']['type']))
            {

                sys_msg($_LANG['invalid_goods_img'], 1, array(), false);
            }
        }

        // 商品缩略图
        if (isset($_FILES['goods_thumb']))
        {
            if ($_FILES['goods_thumb']['tmp_name'] != 'none' && !empty($_FILES['goods_thumb']['tmp_name']))
            {
                if (!$image->check_img_type($_FILES['goods_thumb']['type']))
                {
                    sys_msg($_LANG['invalid_goods_thumb'], 1, array(), false);
                }
            }
        }

        // 相册图片
        foreach ($_FILES['img_url']['tmp_name'] AS $key => $value)
        {
            if ($value != 'none')
            {
                if (!$image->check_img_type($_FILES['img_url']['type'][$key]))
                {
                    sys_msg(sprintf($_LANG['invalid_img_url'], $key + 1), 1, array(), false);
                }
            }
        }
    }

    /* 插入还是更新的标识 */
    $is_insert = $_REQUEST['act'] == 'insert';

    /* 处理商品图片 */
    $goods_img        = '';  // 初始化商品图片
    $goods_thumb      = '';  // 初始化商品缩略图
    $original_img     = '';  // 初始化原始图片
    $old_original_img = '';  // 初始化原始图片旧图
    // 如果上传了商品图片，相应处理
    $isUploadImg = $_POST['isUploadImg'];
    if ($isUploadImg == "1") {
        if (($_FILES['goods_img']['tmp_name'] != '' && $_FILES['goods_img']['tmp_name'] != 'none') or (($_POST['goods_img_url'] != $_LANG['lab_picture_url'] && $_POST['goods_img_url'] != 'http://') && $is_url_goods_img = 1))
            {
                if ($_REQUEST['goods_id'] > 0)
                {
                    /* 删除原来的图片文件 */
                    $sql = "SELECT goods_thumb, goods_img, original_img " .
                            " FROM " . $ecs->table('goods') .
                            " WHERE goods_id = '$_REQUEST[goods_id]'";
                    $row = $db->getRow($sql);
                    if ($row['goods_thumb'] != '' && is_file('../' . $row['goods_thumb']))
                    {
                        @unlink('../' . $row['goods_thumb']);
                    }
                    if ($row['goods_img'] != '' && is_file('../' . $row['goods_img']))
                    {
                        @unlink('../' . $row['goods_img']);
                    }
                    if ($row['original_img'] != '' && is_file('../' . $row['original_img']))
                    {
                        /* 先不处理，以防止程序中途出错停止 */
                        //$old_original_img = $row['original_img']; //记录旧图路径
                    }
                    /* 清除原来商品图片 */
                    if ($proc_thumb === false)
                    {
                        get_image_path($_REQUEST[goods_id], $row['goods_img'], false, 'goods', true);
                        get_image_path($_REQUEST[goods_id], $row['goods_thumb'], true, 'goods', true);
                    }
                }
        
                if (empty($is_url_goods_img))
                {
                    $original_img   = $image->upload_image($_FILES['goods_img'],'', '',false, false); // 原始图片
                }
                elseif (copy(trim($_POST['goods_img_url']), ROOT_PATH . 'temp/' . basename($_POST['goods_img_url'])))
                {
                    $original_img = 'temp/' . basename($_POST['goods_img_url']);
                }
        
                if ($original_img === false)
                {
                    sys_msg($image->error_msg(), 1, array(), false);
                }
                $goods_img      = $original_img;   // 商品图片
        
                /* 复制一份相册图片 */
                /* 添加判断是否自动生成相册图片 */
                if ($_CFG['auto_generate_gallery'])
                {
                    $img        = $original_img;   // 相册图片
                    $pos        = strpos(basename($img), '.');
                    $newname    = dirname($img) . '/' . $image->random_filename() . substr(basename($img), $pos);
                    if (!copy('../' . $img, '../' . $newname))
                    {
                        sys_msg('fail to copy file: ' . realpath('../' . $img), 1, array(), false);
                    }
                    $img        = $newname;
        
                    $gallery_img    = $img;
                    $gallery_thumb  = $img;
                }
        
                // 如果系统支持GD，缩放商品图片，且给商品图片和相册图片加水印
                if ($proc_thumb && $image->gd_version() > 0 && $image->check_img_function($_FILES['goods_img']['type']) || $is_url_goods_img)
                {
        
                    if (empty($is_url_goods_img))
                    {
                        // 如果设置大小不为0，缩放图片
                        if ($_CFG['image_width'] != 0 || $_CFG['image_height'] != 0)
                        {
                            $goods_img = $image->make_thumb('../'. $goods_img , $GLOBALS['_CFG']['image_width'],  $GLOBALS['_CFG']['image_height']);
                            if ($goods_img === false)
                            {
                                sys_msg($image->error_msg(), 1, array(), false);
                            }
                        }
        
                        /* 添加判断是否自动生成相册图片 */
                        if ($_CFG['auto_generate_gallery'])
                        {
                            $newname    = dirname($img) . '/' . $image->random_filename() . substr(basename($img), $pos);
                            if (!copy('../' . $img, '../' . $newname))
                            {
                                sys_msg('fail to copy file: ' . realpath('../' . $img), 1, array(), false);
                            }
                            $gallery_img        = $newname;
                        }
        
                        // 加水印
                        if (intval($_CFG['watermark_place']) > 0 && !empty($GLOBALS['_CFG']['watermark']))
                        {
                            if ($image->add_watermark('../'.$goods_img,'',$GLOBALS['_CFG']['watermark'], $GLOBALS['_CFG']['watermark_place'], $GLOBALS['_CFG']['watermark_alpha']) === false)
                            {
                                sys_msg($image->error_msg(), 1, array(), false);
                            }
                            /* 添加判断是否自动生成相册图片 */
                            if ($_CFG['auto_generate_gallery'])
                            {
                                if ($image->add_watermark('../'. $gallery_img,'',$GLOBALS['_CFG']['watermark'], $GLOBALS['_CFG']['watermark_place'], $GLOBALS['_CFG']['watermark_alpha']) === false)
                                {
                                    sys_msg($image->error_msg(), 1, array(), false);
                                }
                            }
                        }
                    }
        
                    // 相册缩略图
                    /* 添加判断是否自动生成相册图片 */
                    if ($_CFG['auto_generate_gallery'])
                    {
                        if ($_CFG['thumb_width'] != 0 || $_CFG['thumb_height'] != 0)
                        {
                            $gallery_thumb = $image->make_thumb('../' . $img, $GLOBALS['_CFG']['thumb_width'],  $GLOBALS['_CFG']['thumb_height']);
                            if ($gallery_thumb === false)
                            {
                                sys_msg($image->error_msg(), 1, array(), false);
                            }
                        }
                    }
                }
                /* 取消该原图复制流程 */
                // else
                // {
                //     /* 复制一份原图 */
                //     $pos        = strpos(basename($img), '.');
                //     $gallery_img = dirname($img) . '/' . $image->random_filename() . // substr(basename($img), $pos);
                //     if (!copy('../' . $img, '../' . $gallery_img))
                //     {
                //         sys_msg('fail to copy file: ' . realpath('../' . $img), 1, array(), false);
                //     }
                //     $gallery_thumb = '';
                // }
            }
    }
    


    // 是否上传商品缩略图
    if (isset($_FILES['goods_thumb']) && $_FILES['goods_thumb']['tmp_name'] != '' &&
        isset($_FILES['goods_thumb']['tmp_name']) &&$_FILES['goods_thumb']['tmp_name'] != 'none')
    {
        // 上传了，直接使用，原始大小
        $goods_thumb = $image->upload_image($_FILES['goods_thumb'], '', '',false, false);
        if ($goods_thumb === false)
        {
            sys_msg($image->error_msg(), 1, array(), false);
        }
    }
    else
    {
        // 未上传，如果自动选择生成，且上传了商品图片，生成所略图
        if ($proc_thumb && isset($_POST['auto_thumb']) && !empty($original_img))
        {
            // 如果设置缩略图大小不为0，生成缩略图
            if ($_CFG['thumb_width'] != 0 || $_CFG['thumb_height'] != 0)
            {
                $goods_thumb = $image->make_thumb('../' . $original_img, $GLOBALS['_CFG']['thumb_width'],  $GLOBALS['_CFG']['thumb_height']);
                if ($goods_thumb === false)
                {
                    sys_msg($image->error_msg(), 1, array(), false);
                }
            }
            else
            {
                $goods_thumb = $original_img;
            }
        }
    }


    /* 删除下载的外链原图 */
    if (!empty($is_url_goods_img))
    {
        unlink(ROOT_PATH . $original_img);
        empty($newname) || unlink(ROOT_PATH . $newname);
        $url_goods_img = $goods_img = $original_img = htmlspecialchars(trim($_POST['goods_img_url']));
    }


    /* 如果没有输入商品货号则自动生成一个商品货号 */
    if (empty($_POST['goods_sn']))
    {
        $max_id     = $is_insert ? $db->getOne("SELECT MAX(goods_id) + 1 FROM ".$ecs->table('goods')) : $_REQUEST['goods_id'];
        $goods_sn   = generate_goods_sn($max_id);
    }
    else
    {
        $goods_sn   = $_POST['goods_sn'];
    }

    /* 处理商品数据 */
    $shop_price = !empty($_POST['shop_price']) ? $_POST['shop_price'] : 0;
    $market_price = !empty($_POST['market_price']) ? $_POST['market_price'] : 0;
    $promote_price = !empty($_POST['promote_price']) ? floatval($_POST['promote_price'] ) : 0;
    $is_promote = empty($promote_price) ? 0 : 1;
    $promote_date = empty($_POST['promote_date']) ? "0 to 0" : $_POST['promote_date'];
    $promote_date = explode("to", $promote_date);
    $promote_start_date = ($is_promote && !empty($promote_date[0])) ? local_strtotime(trim($promote_date[0])) : 0;
    // 2016-03-28: We add one day to the end date to make the input value inclusive
    $promote_end_date = ($is_promote && !empty($promote_date[1])) ? local_strtotime(trim($promote_date[1])) : 0;
    $goods_weight = !empty($_POST['goods_weight']) ? $_POST['goods_weight'] * $_POST['weight_unit'] : 0;
    $is_fls_warehouse = isset($_POST['is_fls_warehouse']) ? 1 : 0;
    $is_best = isset($_POST['is_best']) ? 1 : 0;
    $is_new = isset($_POST['is_new']) ? 1 : 0;
    $is_hot = isset($_POST['is_hot']) ? 1 : 0;
    $is_best_sellers = isset($_POST['is_best_sellers']) ? 1 : 0;
    $is_on_sale = isset($_POST['is_on_sale']) ? 1 : 0;
    $is_alone_sale = isset($_POST['is_alone_sale']) ? 1 : 0;
    $is_shipping = isset($_POST['is_shipping']) ? 1 : 0;
    $warn_number = isset($_POST['warn_number']) ? $_POST['warn_number'] : 0;
    $goods_type = isset($_POST['goods_type_input']) ? $_POST['goods_type_input'] : 0;
    $give_integral = isset($_POST['give_integral']) ? intval($_POST['give_integral']) : '-1';
    $rank_integral = isset($_POST['rank_integral']) ? intval($_POST['rank_integral']) : '-1';
    $commission = isset($_POST['commission']) ? (floatval($_POST['commission']) . ((substr($_POST['commission'], -1) == '%') ? '%' : '')) : '1%';
    $shipping_price = !empty($_POST['shipping_price']) ? $_POST['shipping_price'] : 0;
    $official_website = !empty($_POST['official_website']) ? $_POST['official_website'] : '';
    $unboxing_page = !empty($_POST['unboxing_page']) ? $_POST['unboxing_page'] : '';
    $country_of_origin = !empty($_POST['country_of_origin']) ? $_POST['country_of_origin'] : '';
    $proddb_id = !empty($_POST['proddb_id']) ? $_POST['proddb_id'] : 0;
    $proddb_import = !empty($_POST['proddb_import']) ? $_POST['proddb_import'] : 0;
    $proddb_sync_time = ($proddb_id && $proddb_import) ? gmtime() : 0;
    $warranty_template_id = !empty($_POST['warranty_template_id']) ? intval($_POST['warranty_template_id']) : 0;
    $shipping_template_id = !empty($_POST['shipping_template_id']) ? intval($_POST['shipping_template_id']) : 0;
    $seller_note = $warranty_template_id == 0 && !empty($_POST['seller_note']) ? $_POST['seller_note'] : "";
    $shipping_note = $shipping_template_id == 0 && !empty($_POST['shipping_note']) ? $_POST['shipping_note'] : "";
    $pricecomhk_id = !empty($_POST['pricecomhk_id']) ? intval($_POST['pricecomhk_id']) : 0;
    $hktvmall_id = !empty($_POST['hktvmall_id']) ? trim($_POST['hktvmall_id']) : 0;
    $fortress_id = !empty($_POST['fortress_id']) ? intval($_POST['fortress_id']) : 0;
    $pricecomhk_water = !empty($_POST['pricecomhk_water']) ? intval($_POST['pricecomhk_water']) : 0;
    $display_discount = isset($_POST['display_discount']) ? 1 : 0;

    $pre_sale_date = !empty($_POST['pre_sale_date']) ? trim($_POST['pre_sale_date']) : 0;
    $pre_sale_date = local_strtotime($pre_sale_date);
    $pre_sale_time = !empty($_POST['pre_sale_time']) ? intval($_POST['pre_sale_time']) : 0;
    $pre_sale_days = !empty($_POST['pre_sale_days']) ? intval($_POST['pre_sale_days']) : 0;
    $pre_sale_string = !empty($_POST['pre_sale_string']) ? intval($_POST['pre_sale_string']) : 0;
    $goods_name_style = $_POST['goods_name_color'] . '+' . $_POST['goods_name_style'];

    $catgory_id = empty($_POST['cat_id']) ? '' : intval($_POST['cat_id']);
    $brand_id = empty($_POST['brand_id']) ? '' : intval($_POST['brand_id']);

    $goods_thumb = (empty($goods_thumb) && !empty($_POST['goods_thumb_url']) && goods_parse_url($_POST['goods_thumb_url'])) ? htmlspecialchars(trim($_POST['goods_thumb_url'])) : $goods_thumb;
    $goods_thumb = (empty($goods_thumb) && isset($_POST['auto_thumb']))? $goods_img : $goods_thumb;
    $goods_length = !empty($_POST['goods_length']) ? $_POST['goods_length'] : 0;
    $goods_width  = !empty($_POST['goods_width']) ? $_POST['goods_width'] : 0;
    $goods_height = !empty($_POST['goods_height']) ? $_POST['goods_height']: 0;
    $require_goods_id = !empty($_POST['require_goods_id']) ? $_POST['require_goods_id']: 0;
    $perma_link     = !empty($_POST['perma_link']) ? $_POST['perma_link'] : '';
    $cur_lang     = !empty($_POST['cur_lang']) ? $_POST['cur_lang'] : 'zh_tw';
    if (!empty($require_goods_id)) {
        // Check if transformer exist and category match
        if (!$db->getOne("SELECT 1 FROM " . $ecs->table("goods") . " WHERE goods_id = $require_goods_id AND cat_id = 295")) {
            $require_goods_id = 0;
        }
    }
    //auto pricing
    $price_strategy = empty($_REQUEST['strategy_select']) ? -1 : intval($_REQUEST['price_strategy']);
    /* 入库 */
    if ($is_insert)
    {
        if (empty($perma_link)) {
            sys_msg("請輸入永久鏈結");
        }
        if ($code == '')
        {
            $sql = "INSERT INTO " . $ecs->table('goods') . " (goods_name, goods_name_style, goods_sn, " .
                    "cat_id, brand_id, shop_price, market_price, is_promote, promote_price, " .
                    "promote_start_date, promote_end_date, goods_img, goods_thumb, original_img, keywords, goods_brief, " .
                    "seller_note, warranty_template_id, goods_weight, warn_number, is_fls_warehouse, integral, give_integral, is_best, is_new, is_hot, " .
                    "is_on_sale, is_alone_sale, is_shipping, goods_desc, guarantee, inthebox, official_website, unboxing_page, " .
                    "country_of_origin, add_time, last_update, commission, goods_type, rank_integral, shipping_price, proddb_id, proddb_sync_time, shipping_note,shipping_template_id, goods_length, goods_width, goods_height, display_discount, pre_sale_date, pre_sale_time, pre_sale_days, pre_sale_string, require_goods_id, perma_link, price_strategy)" .
                "VALUES ('$_POST[goods_name]', '$goods_name_style', '$goods_sn', '$catgory_id', " .
                    "'$brand_id', '$shop_price', '$market_price', '$is_promote','$promote_price', ".
                    "'$promote_start_date', '$promote_end_date', '$goods_img', '$goods_thumb', '$original_img', ".
                    "'$_POST[keywords]', '$_POST[goods_brief]', '$seller_note', '$warranty_template_id', '$goods_weight', ".
                    "'$warn_number', '$is_fls_warehouse', '$_POST[integral]', '$give_integral', '$is_best', '$is_new', '$is_hot', ".
                    "'$is_on_sale', '$is_alone_sale', $is_shipping, '$_POST[goods_desc]', '$_POST[guarantee]', '$_POST[inthebox]', ".
                    "'" . $official_website . "', '" . $unboxing_page . "', '" . $country_of_origin . "', '" . gmtime() . "', ".
                    "'". gmtime() ."', '$commission', '$goods_type', '$rank_integral', '$shipping_price', '$proddb_id', '$proddb_sync_time', '$shipping_note', '$shipping_template_id', $goods_length, $goods_width, $goods_height, $display_discount, $pre_sale_date, $pre_sale_time, $pre_sale_days, $pre_sale_string, $require_goods_id, '$perma_link', '$price_strategy')";
        }
        else
        {
            $sql = "INSERT INTO " . $ecs->table('goods') . " (goods_name, goods_name_style, goods_sn, " .
                    "cat_id, brand_id, shop_price, market_price, is_promote, promote_price, " .
                    "promote_start_date, promote_end_date, goods_img, goods_thumb, original_img, keywords, goods_brief, " .
                    "seller_note, warranty_template_id, goods_weight, warn_number, is_fls_warehouse, integral, give_integral, is_best, is_new, is_hot, is_real, " .
                    "is_on_sale, is_alone_sale, is_shipping, goods_desc, guarantee, inthebox, official_website, unboxing_page, " .
                    "country_of_origin, add_time, last_update, commission, goods_type, extension_code, rank_integral, shipping_price, proddb_id, proddb_sync_time, shipping_note,shipping_template_id, goods_length, goods_width, goods_height, display_discount, pre_sale_date, pre_sale_time, pre_sale_days, pre_sale_string, require_goods_id, perma_link, price_strategy)" .
                "VALUES ('$_POST[goods_name]', '$goods_name_style', '$goods_sn', '$catgory_id', " .
                    "'$brand_id', '$shop_price', '$market_price', '$is_promote','$promote_price', ".
                    "'$promote_start_date', '$promote_end_date', '$goods_img', '$goods_thumb', '$original_img', ".
                    "'$_POST[keywords]', '$_POST[goods_brief]', '$seller_note', '$warranty_template_id', '$goods_weight', ".
                    "'$warn_number', '$is_fls_warehouse', '$_POST[integral]', '$give_integral', '$is_best', '$is_new', '$is_hot', 0, ".
                    "'$is_on_sale', '$is_alone_sale', $is_shipping, '$_POST[goods_desc]', '$_POST[guarantee]', '$_POST[inthebox]', ".
                    "'" . $official_website . "', '" . $unboxing_page . "', '" . $country_of_origin . "', '" . gmtime() . "', ".
                    "'". gmtime() ."', '$commission', '$goods_type', '$code', '$rank_integral', '$shipping_price', '$proddb_id', '$proddb_sync_time', '$shipping_note', '$shipping_template_id', $goods_length, $goods_width, $goods_height, $display_discount, $pre_sale_date, $pre_sale_time, $pre_sale_days, $pre_sale_string, $require_goods_id, '$perma_link', '$price_strategy')";
        }
    }
    else
    {
        //auto pricing
        $db->query("UPDATE " . $ecs->table("goods") . " SET price_strategy = $price_strategy WHERE goods_id " . (empty($_REQUEST['batch']) ? "= $_REQUEST[goods_id]" : "IN ($_REQUEST[goods_id])"));
        /* 點擊刪除圖片的icon，删除原来的商品图 */
        if ($isUploadImg == "0") {
            $sql = "SELECT goods_img, original_img " .
                    " FROM " . $ecs->table('goods') .
                    " WHERE goods_id = '$_REQUEST[goods_id]'";
            $row = $db->getRow($sql);
            if ($row['goods_img'] && !goods_parse_url($row['goods_img']))
            {
                @unlink(ROOT_PATH . $row['goods_img']);
                @unlink(ROOT_PATH . $row['original_img']);
            }
            $sql = "UPDATE " . $ecs->table('goods') . " SET goods_img = '$goods_img', original_img = '$original_img' WHERE goods_id = '$_REQUEST[goods_id]' LIMIT 1";
            $db->query($sql);
        }
        /* 如果有上传图片，删除原来的商品图 */
        $sql = "SELECT goods_thumb, goods_img, original_img " .
                    " FROM " . $ecs->table('goods') .
                    " WHERE goods_id = '$_REQUEST[goods_id]'";
        $row = $db->getRow($sql);
        if ($proc_thumb && $goods_img && $row['goods_img'] && !goods_parse_url($row['goods_img']))
        {
            @unlink(ROOT_PATH . $row['goods_img']);
            @unlink(ROOT_PATH . $row['original_img']);
        }

        if ($proc_thumb && $goods_thumb && $row['goods_thumb'] && !goods_parse_url($row['goods_thumb']))
        {
            @unlink(ROOT_PATH . $row['goods_thumb']);
        }

        // Email notification for price changes
        $org_price = $db->getOne("SELECT shop_price FROM " . $ecs->table('goods') . " WHERE goods_id = '$_REQUEST[goods_id]'");
        if ($org_price != $shop_price)
        {
            if (@include_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_notifications.php'))
            {
                notify_price_changed(array(
                    'goods_id' => $_REQUEST['goods_id'],
                    'goods_name' => $_POST['goods_name'],
                    'goods_sn' => $goods_sn,
                ), $org_price, $shop_price);
            }
            else
            {
                error_log(__FILE__.':'.__LINE__.' Cannot include lib_notifications.php');
            }

            // Log the price changes
            $GLOBALS['db']->autoExecute($GLOBALS['ecs']->table('goods_price_log'), array(
                'goods_id' => $_REQUEST['goods_id'],
                'old_price' => $org_price,
                'new_price' => $shop_price,
                'update_time' => gmtime(),
                'admin_id' => $_SESSION['admin_id']
            ), 'INSERT');
        }

        // Log goods changes
        $sql = "INSERT INTO " . $ecs->table('goods_log') .
                "SELECT NULL, '" . gmtime() . "', '" . $_SESSION['admin_name'] . "', g.* " .
                "FROM " . $ecs->table('goods') . " as g " .
                "WHERE goods_id = '" . $_REQUEST['goods_id'] . "'";
        $db->query($sql);

            $sql = "UPDATE " . $ecs->table('goods') . " SET " .
                "goods_name = '$_POST[goods_name]', " .
                "goods_name_style = '$goods_name_style', " .
                "goods_sn = '$goods_sn', " .
                "cat_id = '$catgory_id', " .
                "brand_id = '$brand_id', " .
                "shop_price = '$shop_price', " .
                "market_price = '$market_price', " .
                "is_promote = '$is_promote', " .
                "promote_price = '$promote_price', " .
                "promote_start_date = '$promote_start_date', " .
                "promote_end_date = '$promote_end_date', ";
                
        if ($cur_lang == 'zh_tw' && $perma_link) {
            $sql .= "perma_link = '$perma_link', ";
            $sel_sql = "SELECT link_id FROM " . $GLOBALS['ecs']->table('perma_link') . " WHERE `id` = " . $_REQUEST['goods_id'] . " AND `lang` LIKE '" . $cur_lang . "'";
            $sel_res = $GLOBALS['db']->getOne($sel_sql);
            if ($sel_res) {
                $upd_sql = "UPDATE " . $GLOBALS['ecs']->table("perma_link") . " SET perma_link = '" . $perma_link . "' WHERE `id` = " . $_REQUEST['goods_id'] . " AND `lang` LIKE '" . $cur_lang . "'";
                $upd_res = $GLOBALS['db']->query($upd_sql);
            } else {
                $ins_sql ="INSERT INTO " . $GLOBALS['ecs']->table("perma_link") . " (`link_id`, `table_name`, `id`, `lang`, `perma_link`) VALUES (NULL, 'goods', '" . $_REQUEST['goods_id'] . "', '" . $cur_lang . "', '" . $perma_link . "')";
                $GLOBALS['db']->query($ins_sql);
            }
        }
        /* 如果有上传图片，需要更新数据库 */
        if ($goods_img)
        {
            $sql .= "goods_img = '$goods_img', original_img = '$original_img', ";
        }
        if ($goods_thumb)
        {
            $sql .= "goods_thumb = '$goods_thumb', ";
        }
        if ($code != '')
        {
            $sql .= "is_real=0, extension_code='$code', ";
        }
        if ($proddb_sync_time || !$proddb_id)
        {
            $sql .= "proddb_sync_time = '$proddb_sync_time', ";
        }
        $sql .= "keywords = '$_POST[keywords]', " .
                "goods_brief = '$_POST[goods_brief]', " .
                "seller_note = '$seller_note', " .
                "warranty_template_id = '$warranty_template_id', " .
                "shipping_note = '$shipping_note', " .
                "shipping_template_id = '$shipping_template_id', " .
                "goods_weight = '$goods_weight'," .
                "warn_number = '$warn_number', " .
                "is_fls_warehouse = '$is_fls_warehouse', " .
                "integral = '$_POST[integral]', " .
                "give_integral = '$give_integral', " .
                "rank_integral = '$rank_integral', " .
                "is_best = '$is_best', " .
                "is_new = '$is_new', " .
                "is_hot = '$is_hot', " .
                "is_on_sale = '$is_on_sale', " .
                "is_alone_sale = '$is_alone_sale', " .
                "is_shipping = '$is_shipping', " .
                "goods_desc = '$_POST[goods_desc]', " .
                "guarantee = '$_POST[guarantee]', " .
                "inthebox = '$_POST[inthebox]', " .
                "official_website = '" . $official_website . "', " .
                "unboxing_page = '" . $unboxing_page . "', " .
                "country_of_origin = '" . $country_of_origin . "', " .
                "last_update = '". gmtime() ."', " .
                "goods_type = '$goods_type', " .
                "shipping_price = '$shipping_price', " .
                "proddb_id = '$proddb_id', " .
                "goods_length = '$goods_length', " .
                "goods_width = '$goods_width', " .
                "goods_height = '$goods_height', " .
                "display_discount = '$display_discount', " .
                "pre_sale_date = '$pre_sale_date', " .
                "pre_sale_time = '$pre_sale_time', " .
                "pre_sale_days = '$pre_sale_days', " .
                "pre_sale_string = '$pre_sale_string', " .
                "require_goods_id = $require_goods_id " .
                "WHERE goods_id = '$_REQUEST[goods_id]' LIMIT 1";
    }

    // handle simplified chinese
    $localizable_fields = [
        "goods_name"    => $_POST["goods_name"],
        "keywords"      => $_POST['keywords'],
        "perma_link"      => $_POST['perma_link'],
        "goods_brief"   => $_POST['goods_brief'],
        "goods_desc"    => $_POST['goods_desc'],
        "seller_note"   => $seller_note,
        "guarantee"     => $_POST['guarantee'],
        "inthebox"      => $_POST['inthebox'],
        "shipping_note" => $shipping_note,
    ];
    if (!$is_insert) {
        $localizable_fields = $goodsController->checkRequireSimplied("goods", $localizable_fields, $_REQUEST['goods_id']);
    } else {
        $localizable_fields['perma_link'] = empty($_POST["perma_link_raw"]) ? $_POST["goods_name"] : $_POST["perma_link_raw"];
    }
    to_diff_lang($localizable_fields, $localized_versions = null,$cur_lang,$_REQUEST['goods_id']);

    $db->query($sql);

    /* 商品编号 */
    $goods_id = $is_insert ? $db->insert_id() : $_REQUEST['goods_id'];

    // 如果允許管理成本，可以設定 commission
    if (!empty($_SESSION['manage_cost']))
    {
        $sql = "UPDATE " . $ecs->table('goods') . " SET `commission` = '" . $commission . "' WHERE `goods_id` = '" . $goods_id . "'";
        $db->query($sql);
    }
    
    // 儲存 price.com.hk 對應的產品編號
    if (!empty($pricecomhk_id))
    {
        $sql = "INSERT INTO " . $ecs->table('goods_pricecomhk') . " (`goods_id`, `pricecomhk_id`, `is_water`) VALUES ('" . $goods_id . "', '" . $pricecomhk_id . "', '$pricecomhk_water') ON DUPLICATE KEY UPDATE `pricecomhk_id` = '" . $pricecomhk_id . "', `is_water` = '$pricecomhk_water'";
        $db->query($sql);
    }

    // 儲存 hktvmall.com 對應的產品編號
    if (!empty($hktvmall_id))
    {
        $sql = "INSERT INTO " . $ecs->table('goods_hktvmall') . " (`goods_id`, `hktvmall_id`) VALUES ('" . $goods_id . "', '" . $hktvmall_id . "') ON DUPLICATE KEY UPDATE `hktvmall_id` = '" . $hktvmall_id . "'";
        $db->query($sql);
    }

    // 儲存 fortress.com.hk 對應的產品編號
    if (!empty($fortress_id))
    {
        $sql = "INSERT INTO " . $ecs->table('goods_fortress') . " (`goods_id`, `fortress_id`) VALUES ('" . $goods_id . "', '" . $fortress_id . "') ON DUPLICATE KEY UPDATE `fortress_id` = '" . $fortress_id . "'";
        $db->query($sql);
    }
    
	//更改供应商
    $sql = "SELECT supplier_id FROM " . $ecs->table('erp_goods_supplier') . " WHERE goods_id = '" . $goods_id . "'";
    $existing_suppliers = $db->getCol($sql);
	$sql="DELETE FROM " . $ecs->table('erp_goods_supplier') . " WHERE goods_id = '" . $goods_id . "'";
	$db->query($sql);
    $new_suppliers = array();
    $missing_suppliers = array();
	if(isset($_REQUEST['supplier']))
	{
		foreach($_REQUEST['supplier'] as $key => $supplier_id)
		{
			$supplier_id = intval(trim($supplier_id));
			if ($supplier_id > 0)
			{
                $new_suppliers[] = $supplier_id;
			}
		}
        if (!empty($new_suppliers))
        {
            $sql = "INSERT INTO " . $ecs->table('erp_goods_supplier') . " (`goods_id`, `supplier_id`) VALUES " . implode(',', array_map(function ($supplier_id) use ($goods_id) {
                return "('" . $goods_id . "', '" . $supplier_id . "')";
            }, $new_suppliers));
            $db->query($sql);
        }
	}
    if (isset($_REQUEST['missing_supplier']))
    {
        foreach($_REQUEST['missing_supplier'] as $key => $supplier_id)
        {
            $supplier_id = intval(trim($supplier_id));
            if (($supplier_id > 0) && (!in_array($supplier_id, $new_suppliers)))
            {
                $missing_suppliers[] = $supplier_id;
            }
        }
        if (!empty($missing_suppliers))
        {
            $sql = "INSERT INTO " . $ecs->table('erp_goods_supplier') . " (`goods_id`, `supplier_id`) VALUES " . implode(',', array_map(function ($supplier_id) use ($goods_id) {
                return "('" . $goods_id . "', '" . $supplier_id . "')";
            }, $missing_suppliers));
            $db->query($sql);
        }
    }
    
    if (count($existing_suppliers) != count($new_suppliers) || count(array_intersect($existing_suppliers, $new_suppliers)) != count($existing_suppliers) || !empty($missing_suppliers))
    {
        $goods_supplier_changes_log = ROOT_PATH . DATA_DIR . '/goods_supplier_changes.log';
        ob_start();
        echo 'Goods ID: ' . $goods_id . '; ';
        echo 'Existing: ' . implode(',', $existing_suppliers) . '; ';
        echo 'New: ' . implode(',', $new_suppliers) . '; ';
        echo 'Missing: ' . implode(',', $missing_suppliers) . "\n";
        echo 'Admin = ' . $_SESSION['admin_name'] . "\n";
        echo '$_GET = '; var_dump($_GET); echo "\n";
        echo '$_POST = '; var_dump($_POST); echo "\n";
        echo '$_SERVER = '; var_dump($_SERVER); echo "\n";
        file_put_contents($goods_supplier_changes_log, ob_get_clean(), FILE_APPEND | LOCK_EX);
    }

    /* 记录日志 */
    if ($is_insert)
    {
        admin_log($_POST['goods_name'], 'add', 'goods');
    }
    else
    {
        admin_log($_POST['goods_name'], 'edit', 'goods');
    }

    /* 处理属性 */
    if ((isset($_POST['attr_id_list']) && isset($_POST['attr_value_list'])) || (empty($_POST['attr_id_list']) && empty($_POST['attr_value_list'])))
    {
        // 取得原有的属性值
        $goods_attr_list = array();

        $keywords_arr = explode(" ", $_POST['keywords']);

        $keywords_arr = array_flip($keywords_arr);
        if (isset($keywords_arr['']))
        {
            unset($keywords_arr['']);
        }

        $sql = "SELECT attr_id, attr_index FROM " . $ecs->table('attribute') . " WHERE cat_id = '$goods_type'";
        $attr_res = $db->query($sql);
        $attr_list = array();

        while ($row = $db->fetchRow($attr_res))
        {
            $attr_list[$row['attr_id']] = $row['attr_index'];
        }

        $sql = "SELECT g.*, a.attr_type
                FROM " . $ecs->table('goods_attr') . " AS g
                    LEFT JOIN " . $ecs->table('attribute') . " AS a
                        ON a.attr_id = g.attr_id
                WHERE g.goods_id = '$goods_id'";

        $res = $db->query($sql);

        while ($row = $db->fetchRow($res))
        {
            $goods_attr_list[$row['attr_id']][$row['attr_value']] = array('sign' => 'delete', 'goods_attr_id' => $row['goods_attr_id']);
        }
        // 循环现有的，根据原有的做相应处理
        if(isset($_POST['attr_id_list']))
        {
            foreach ($_POST['attr_id_list'] AS $key => $attr_id)
            {
                $attr_value = $_POST['attr_value_list'][$key];
                $attr_price = $_POST['attr_price_list'][$key];
                if (!empty($attr_value))
                {
                    if (isset($goods_attr_list[$attr_id][$attr_value]))
                    {
                        // 如果原来有，标记为更新
                        $goods_attr_list[$attr_id][$attr_value]['sign'] = 'update';
                        $goods_attr_list[$attr_id][$attr_value]['attr_price'] = $attr_price;
                    }
                    else
                    {
                        // 如果原来没有，标记为新增
                        $goods_attr_list[$attr_id][$attr_value]['sign'] = 'insert';
                        $goods_attr_list[$attr_id][$attr_value]['attr_price'] = $attr_price;
                    }
                    $val_arr = explode(' ', $attr_value);
                    foreach ($val_arr AS $k => $v)
                    {
                        if (!isset($keywords_arr[$v]) && $attr_list[$attr_id] == "1")
                        {
                            $keywords_arr[$v] = $v;
                        }
                    }
                }
            }
        }
        $keywords = join(' ', array_flip($keywords_arr));

        $sql = "UPDATE " .$ecs->table('goods'). " SET keywords = '$keywords' WHERE goods_id = '$goods_id' LIMIT 1";

        $db->query($sql);

        /* 插入、更新、删除数据 */

        include_once('includes/ERP/lib_erp_goods_attr.php');

        foreach ($goods_attr_list as $attr_id => $attr_value_list)
        {
            foreach ($attr_value_list as $attr_value => $info)
            {
                if ($info['sign'] == 'insert')
                {
                    $sql = "INSERT INTO " .$ecs->table('goods_attr'). " (attr_id, goods_id, attr_value, attr_price)".
                            "VALUES ('$attr_id', '$goods_id', '$attr_value', '$info[attr_price]')";
                    $db->query($sql);
                    $goods_attr_id = $db->insert_id();

                    $sql = "SELECT attr_values FROM ".$ecs->table('attribute')." WHERE attr_id = $attr_id LIMIT 1";
                    $attr_values = $db->getOne($sql);
                    $tmp = $attributeController->attr_values_to_array($attr_values);
                    $key = array_search($attr_value, $tmp);
                    if(isset($key))
                    {
                        $sql = "SELECT * FROM ".$ecs->table('attribute_lang')." WHERE attr_id = ".$attr_id;
                        $lang_list = $db->getAll($sql);
                        foreach ($lang_list as $k => $attr)
                        {
                            $lang = $attr['lang'];
                            $attr_values = $attributeController->attr_values_to_array($attr['attr_values']);
                            $attr_value_lang = isset($attr_values[$key]) ? $attr_values[$key] : $attr_value;

                            $sql = "INSERT INTO " .$ecs->table('goods_attr_lang'). " (goods_attr_id, lang, attr_value)".
                                "VALUES ('$goods_attr_id', '$lang', '$attr_value_lang')";
                            $db->query($sql);
                        }
                    }
                }
                elseif ($info['sign'] == 'update')
                {
                    
                    $sql = "UPDATE " .$ecs->table('goods_attr'). " SET attr_value = '$attr_value' WHERE goods_attr_id = '$info[goods_attr_id]' LIMIT 1";
                    $db->query($sql);

                    $sql = "SELECT attr_values FROM ".$ecs->table('attribute')." WHERE attr_id = $attr_id LIMIT 1";
                    $attr_values = $db->getOne($sql);
                    $tmp = $attributeController->attr_values_to_array($attr_values);
                    $key = array_search($attr_value, $tmp);
                    if(isset($key))
                    {
                        $sql = "SELECT * FROM ".$ecs->table('attribute_lang')." WHERE attr_id = ".$attr_id;
                        $lang_list = $db->getAll($sql);

                        foreach ($lang_list as $k => $attr)
                        {
                            $lang = $attr['lang'];
                            $attr_values = $attributeController->attr_values_to_array($attr['attr_values']);
                            $attr_value_lang = isset($attr_values[$key]) ? $attr_values[$key] : $attr_value;

                            $sql = "UPDATE " .$ecs->table('goods_attr_lang'). "SET attr_value = ".
                                "'$attr_value_lang' WHERE goods_attr_id = '$info[goods_attr_id]' AND lang = '$lang' ";
                            $db->query($sql);
                        }
                    }
                }
                else
                {
                	  if(!attr_has_qty($goods_id,$info['goods_attr_id']))
                	  {
	                    $sql = "DELETE FROM " .$ecs->table('goods_attr'). " WHERE goods_attr_id = '$info[goods_attr_id]' LIMIT 1";
	                		$db->query($sql);
                        $sql = "DELETE FROM " .$ecs->table('goods_attr_lang'). " WHERE goods_attr_id = '$info[goods_attr_id]' LIMIT 1";
	                		$db->query($sql);
                	  }
                }

                if(!empty($info['goods_attr_id']))
                {
                	delete_attr($goods_id,$info['goods_attr_id']);
                }
            }
        }

      	update_attr($goods_id);
      	update_attr_stock($goods_id);
      	auto_fix_barcode($goods_id);
    }


    /* 处理会员价格 */
    if (isset($_POST['user_rank']) && isset($_POST['user_price']))
    {
        handle_member_price($goods_id, $_POST['user_rank'], $_POST['user_price']);
    }

    /* 处理优惠价格 */
    if (isset($_POST['volume_number']) && isset($_POST['volume_price']))
    {
        $temp_num = array_count_values($_POST['volume_number']);
        foreach($temp_num as $v)
        {
            if ($v > 1)
            {
                sys_msg($_LANG['volume_number_continuous'], 1, array(), false);
                break;
            }
        }
        handle_volume_price($goods_id, $_POST['volume_number'], $_POST['volume_price']);
    }

    /* 处理扩展分类 */
    if (isset($_POST['other_cat']))
    {
        handle_other_cat($goods_id, array_unique($_POST['other_cat']));
    }

    if ($is_insert)
    {
        /* 处理关联商品 */
        handle_link_goods($goods_id);

        /* 处理组合商品 */
        handle_group_goods($goods_id);

        /* 处理关联文章 */
        handle_goods_article($goods_id);
    }

    // Handle predefined extra info
    function handle_predefined_extra_info($field_name, $display_name, $display_value = '', $display_url = '', $url_newtab = '1')
    {
        $info_field = empty($display_value) ? 'value' : 'url';

        // Ensure the extra info array is present
        if (!is_array($_POST['extra_info_name'])) $_POST['extra_info_name'] = array();
        if (!is_array($_POST['extra_info_value'])) $_POST['extra_info_value'] = array();
        if (!is_array($_POST['extra_info_url'])) $_POST['extra_info_url'] = array();
        if (!is_array($_POST['extra_info_url_newtab'])) $_POST['extra_info_url_newtab'] = array();

        if (!empty($_POST[$field_name]))
        {
            $found = false;
            foreach ($_POST['extra_info_name'] as $key => $info_name)
            {
                if ($info_name == $display_name)
                {
                    $_POST['extra_info_' . $info_field][$key] = $_POST[$field_name];

                    $found = true;
                    break;
                }
            }
            if (!$found)
            {
                array_unshift($_POST['extra_info_name'], $display_name);
                array_unshift($_POST['extra_info_value'], ($info_field == 'value') ? $_POST[$field_name] : $display_value);
                array_unshift($_POST['extra_info_url'], ($info_field == 'url') ? $_POST[$field_name] : $display_url);
                array_unshift($_POST['extra_info_url_newtab'], $url_newtab);
            }
        }
        else // info not provided, remove it from the array
        {
            if (is_array($_POST['extra_info_name']))
            {
                foreach ($_POST['extra_info_name'] as $key => $info_name)
                {
                    if ($info_name == $display_name)
                    {
                        unset($_POST['extra_info_name'][$key]);
                        unset($_POST['extra_info_value'][$key]);
                        unset($_POST['extra_info_url'][$key]);
                        unset($_POST['extra_info_url_newtab'][$key]);
                        break;
                    }
                }
            }
        }
    }
    // 2015-10-29 These fields have been moved into the goods table for simplicity
    // handle_predefined_extra_info('official_website', '官方網頁', '瀏覽官方網頁');
    // handle_predefined_extra_info('unboxing_page', '開箱及評測', '瀏覽開箱及評測');
    // handle_predefined_extra_info('country_of_origin', '原產地');

    /* 处理自訂欄位 */
    if ((is_array($_POST['extra_info_name'])) && (is_array($_POST['extra_info_value'])) &&
        (is_array($_POST['extra_info_url'])) && (is_array($_POST['extra_info_url_newtab'])))
    {
        handle_goods_extra_info($goods_id, $_POST['extra_info_name'], $_POST['extra_info_value'], $_POST['extra_info_url'], $_POST['extra_info_url_newtab']);
    }

    /* 处理推廣鏈結 */
    if ((is_array($_POST['promo_link_title'])) && (is_array($_POST['promo_link_url'])) &&
        (is_array($_POST['promo_link_newtab'])) && (is_array($_POST['promo_link_img'])) &&
        (is_array($_POST['promo_link_desc'])))
    {
        handle_goods_promo_links($goods_id, $_POST['promo_link_title'], $_POST['promo_link_url'], $_POST['promo_link_newtab'], $_POST['promo_link_img'], $_POST['promo_link_desc']);
    }

    /* 重新格式化图片名称 */
    $original_img = reformat_image_name('goods', $goods_id, $original_img, 'source');
    $goods_img = reformat_image_name('goods', $goods_id, $goods_img, 'goods');
    $goods_thumb = reformat_image_name('goods_thumb', $goods_id, $goods_thumb, 'thumb');
    if ($goods_img !== false)
    {
        $db->query("UPDATE " . $ecs->table('goods') . " SET goods_img = '$goods_img' WHERE goods_id='$goods_id'");
    }

    if ($original_img !== false)
    {
        $db->query("UPDATE " . $ecs->table('goods') . " SET original_img = '$original_img' WHERE goods_id='$goods_id'");
    }

    if ($goods_thumb !== false)
    {
        $db->query("UPDATE " . $ecs->table('goods') . " SET goods_thumb = '$goods_thumb' WHERE goods_id='$goods_id'");
    }

    /* 如果有图片，把商品图片加入图片相册 */
    if (isset($img))
    {
        /* 重新格式化图片名称 */
        if (empty($is_url_goods_img))
        {
            $img = reformat_image_name('gallery', $goods_id, $img, 'source');
            $gallery_img = reformat_image_name('gallery', $goods_id, $gallery_img, 'goods');
        }
        else
        {
            $img = $url_goods_img;
            $gallery_img = $url_goods_img;
        }

        $gallery_thumb = reformat_image_name('gallery_thumb', $goods_id, $gallery_thumb, 'thumb');
        $sql = "INSERT INTO " . $ecs->table('goods_gallery') . " (goods_id, img_url, img_desc, thumb_url, img_original) " .
                "VALUES ('$goods_id', '$gallery_img', '', '$gallery_thumb', '$img')";
        $db->query($sql);
    }

    /* 处理相册图片 */
    if (isset($_FILES['img_url']) &&
        isset($_POST['img_desc']) && is_array($_POST['img_desc']) &&
        isset($_POST['img_file']) && is_array($_POST['img_file']))
    {
        handle_gallery_image($goods_id, $_FILES['img_url'], $_POST['img_desc'], $_POST['img_file']);
    }

    /* 编辑时处理相册图片描述 */
    if (!$is_insert && isset($_POST['old_img_desc']))
    {
        foreach ($_POST['old_img_desc'] AS $img_id => $img_desc)
        {
            $sql = "UPDATE " . $ecs->table('goods_gallery') . " SET img_desc = '$img_desc' WHERE img_id = '$img_id' LIMIT 1";
            $db->query($sql);
        }
    }
    /* 编辑时处理相册图片排序 */
    if (!$is_insert && isset($_POST['old_img_sort']))
    {
        foreach ($_POST['old_img_sort'] AS $img_id => $img_sort)
        {
            $sql = "UPDATE " . $ecs->table('goods_gallery') . " SET sort_order = '$img_sort' WHERE img_id = '$img_id' LIMIT 1";
            $db->query($sql);
        }
    }

    /* 不保留商品原图的时候删除原图 */
    if ($proc_thumb && !$_CFG['retain_original_img'] && !empty($original_img))
    {
        $db->query("UPDATE " . $ecs->table('goods') . " SET original_img='' WHERE `goods_id`='{$goods_id}'");
        $db->query("UPDATE " . $ecs->table('goods_gallery') . " SET img_original='' WHERE `goods_id`='{$goods_id}'");
        @unlink('../' . $original_img);
        @unlink('../' . $img);
    }

    // Multiple language support
    save_localized_versions('goods', $goods_id);

    /* 记录上一次选择的分类和品牌 */
    setcookie('ECSCP[last_choose]', $catgory_id . '|' . $brand_id, gmtime() + 86400);
    /* 清空缓存 */
    clear_cache_files();
    // Flush Category Thumb memcached
    $categoryController->flushCatThumbMemcache($catgory_id);
    /* 提示页面 */
    $link = array();

    if ($code == 'virtual_card')
    {
        $link[1] = array('href' => 'virtual_card.php?act=replenish&goods_id=' . $goods_id, 'text' => $_LANG['add_replenish']);
    }
    if ($is_insert)
    {
        $link[2] = add_link($code);
    }
    $link[0] = list_link($is_insert, $code);

    if (empty($is_insert))
    {
        $key_array = array_keys($link);
        krsort($link);
        $link = array_combine($key_array, $link);
    }

    sys_msg($is_insert ? $_LANG['add_goods_ok'] : $_LANG['edit_goods_ok'], 0, $link);
}

/*------------------------------------------------------ */
//-- 批量操作
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'batch')
{
    $code = empty($_REQUEST['extension_code'])? '' : trim($_REQUEST['extension_code']);


    /* 取得要操作的商品编号 */
    $goods_id = !empty($_POST['id']) ? $_POST['id'] : 0;
    if (isset($_POST['type']))
    {
        /* 放入回收站 */
        if ($_POST['type'] == 'trash')
        {
            /* 检查权限 */
            if(!$_SESSION['manage_cost']){
                admin_priv('remove_back');
            }

            update_goods($goods_id, 'is_delete', '1');

            /* 记录日志 */
            admin_log('', 'batch_trash', 'goods');
        }
       
        /* 设为精品 */
        elseif ($_POST['type'] == 'best')
        {
            /* 检查权限 */
            admin_priv('goods_manage');
            update_goods($goods_id, 'is_best', '1');
        }

        /* 取消精品 */
        elseif ($_POST['type'] == 'not_best')
        {
            /* 检查权限 */
            admin_priv('goods_manage');
            update_goods($goods_id, 'is_best', '0');
        }

        /* 设为新品 */
        elseif ($_POST['type'] == 'new')
        {
            /* 检查权限 */
            admin_priv('goods_manage');
            update_goods($goods_id, 'is_new', '1');
        }

        /* 取消新品 */
        elseif ($_POST['type'] == 'not_new')
        {
            /* 检查权限 */
            admin_priv('goods_manage');
            update_goods($goods_id, 'is_new', '0');
        }

        /* 设为热销 */
        elseif ($_POST['type'] == 'hot')
        {
            /* 检查权限 */
            admin_priv('goods_manage');
            update_goods($goods_id, 'is_hot', '1');
        }

        /* 取消热销 */
        elseif ($_POST['type'] == 'not_hot')
        {
            /* 检查权限 */
            admin_priv('goods_manage');
            update_goods($goods_id, 'is_hot', '0');
        }

        /* 转移到供货商 */
        elseif ($_POST['type'] == 'suppliers_move_to')
        {
            /* 检查权限 */
            admin_priv('goods_manage');
            update_goods($goods_id, 'suppliers_id', $_POST['suppliers_id']);
        }

         /* 修改排序 */
         elseif ($_POST['type'] == 'batch_sort_order')
         {
             /* 检查权限 */
             if(!$_SESSION['manage_cost']){
                admin_priv('goods_manage');
                admin_priv('batch_sort_order');
             }

             update_goods($goods_id, 'sort_order', $_POST['sort_order']);
         }

        /* 新增Tag */
        elseif ($_POST['type'] == 'add_tag')
        {
            /* 检查权限 */
            $tagController = new Yoho\cms\Controller\TagController();
            if(!$_SESSION['manage_cost']){
                admin_priv('goods_manage');
                admin_priv('batch_add_tag');
            }

            $tagController = new Yoho\cms\Controller\TagController();
            $tagController->batch_add($goods_id, $_POST['tags']);
        }



        //////////////////////2019-04-26//////////////////////
         /* 上架 */
         elseif ($_POST['type'] == 'on_sale')
         {
             /* 检查权限 */
             if(!$_SESSION['manage_cost']){
                admin_priv('goods_manage');
                admin_priv('batch_on_sale');
             }

             update_goods($goods_id, 'is_on_sale', '1');
         }
 
         /* 下架 */
         elseif ($_POST['type'] == 'not_on_sale')
         {
             /* 检查权限 */
             if(!$_SESSION['manage_cost']){
                admin_priv('goods_manage');
                admin_priv('batch_not_on_sale');
             }
             
             update_goods($goods_id, 'is_on_sale', '0');
         }
 
        //另收運費
        elseif ($_POST['type'] == 'shipping_fee')
        {
            /* 检查权限 */
            if(!$_SESSION['manage_cost']){
                admin_priv('goods_manage');
                admin_priv('batch_shipping_fee');
            }

            update_goods($goods_id, 'shipping_price', $_POST['shipping_fee_change']);
        }

        //保養條款
        elseif ($_POST['type'] == 'warranty')
        {
            /* 检查权限 */
            if(!$_SESSION['manage_cost']){
                admin_priv('goods_manage');
                admin_priv('batch_warranty');
            }

            update_goods($goods_id, 'warranty_template_id', $_POST['warranty_change']);
        }

        //供應商
        elseif ($_POST['type'] == 'suppliers')
        {
            /* 检查权限 */
            if(!$_SESSION['manage_cost']){
                admin_priv('goods_manage');
                admin_priv('batch_suppliers');
            }

            $supplier_id = $_POST['suppliers_change'];
            $goods_id_arr = explode(',', $goods_id);
            foreach($goods_id_arr as $goods_id){
                $sql="DELETE FROM " . $ecs->table('erp_goods_supplier') . " WHERE goods_id = '" . $goods_id."'";
                $db->query($sql);
                $sql="INSERT INTO " . $ecs->table('erp_goods_supplier') . " (`goods_id`, `supplier_id`) VALUES ('" . $goods_id . "', '" . $supplier_id . "')";
                $db->query($sql);
            }
        }

        /* 品牌 */
        elseif ($_POST['type'] == 'brand')
        {
            /* 检查权限 */
            if(!$_SESSION['manage_cost']){
                admin_priv('goods_manage');
                admin_priv('batch_brand');
            }
            
            update_goods($goods_id, 'brand_id', $_POST['brand_change']);
        }

        /* 转移到分类 */
        elseif ($_POST['type'] == 'move_to')
        {
            /* 检查权限 */
            if(!$_SESSION['manage_cost']){
                admin_priv('goods_manage');
                admin_priv('batch_move_to');
            }

            $attributeController->remove_goods_attr_by_attr_value($goods_id);
            update_goods($goods_id, 'cat_id', $_POST['target_cat']);
        }
        
        /* 生成產品ID列表 */
        elseif ($_POST['type'] == 'id_list')
        {
            /* 检查权限 */
            if(!$_SESSION['manage_cost']){
                admin_priv('goods_manage');
                admin_priv('batch_id_list');
            }

            $links = array(
                list_link(true, $code),
                array('href' => 'users.php?act=list&amp;bought_goods=' . $goods_id, 'text' => '搜尋曾購買這些產品的會員'),
                array('href' => 'order.php?act=list&amp;includes_goods=' . $goods_id, 'text' => '搜尋包含這些產品的訂單')
            );
            sys_msg('產品ID列表: ' . $goods_id, 0, $links, false);
            exit;
        }
        
        //////////////////////2019-04-26//////////////////////

        /* 还原 */
        elseif ($_POST['type'] == 'restore')
        {
            /* 检查权限 */
            admin_priv('remove_back');

            update_goods($goods_id, 'is_delete', '0');

            /* 记录日志 */
            admin_log('', 'batch_restore', 'goods');
        }
        /* 删除 */
        elseif ($_POST['type'] == 'drop')
        {
            /* 检查权限 */
            admin_priv('remove_back');

            delete_goods($goods_id);

            /* 记录日志 */
            admin_log('', 'batch_remove', 'goods');
        }
        elseif ($_POST['type'] == 'export')
        {
            if(!$_SESSION['manage_cost']){
                admin_priv('goods_export');
            }

            header('Location: goods_export.php?act=goods_export&preselected_goods_ids=' . $goods_id);
            exit;
        }
        elseif ($_POST['type'] == 'edit')
        {
            if(!$_SESSION['manage_cost']){
                admin_priv('goods_batch');
            }

            header('Location: goods_batch.php?act=select&preselected_goods_ids=' . $goods_id);
            exit;
        }
        
    }

    /* 清除缓存 */
    clear_cache_files();

    if ($_POST['type'] == 'drop' || $_POST['type'] == 'restore')
    {
        $link[] = array('href' => 'goods.php?act=trash', 'text' => $_LANG['11_goods_trash']);
    }
    else
    {
        $link[] = list_link(true, $code);
    }
    sys_msg($_LANG['batch_handle_ok'], 0, $link);
}

/*------------------------------------------------------ */
//-- 显示图片
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'show_image')
{

    if (isset($GLOBALS['shop_id']) && $GLOBALS['shop_id'] > 0)
    {
        $img_url = $_GET['img_url'];
    }
    else
    {
        if (strpos($_GET['img_url'], 'http://') === 0)
        {
            $img_url = $_GET['img_url'];
        }
        else
        {
            $img_url = '../' . $_GET['img_url'];
        }
    }
    $smarty->assign('img_url', $img_url);
    $smarty->display('goods_show_image.htm');
}

elseif ($_REQUEST['act'] == 'check_goods_sn')
{
    check_authz_json('goods_manage');

    $goods_id = intval($_REQUEST['goods_id']);
    $goods_sn = json_str_iconv(trim($_REQUEST['goods_sn']));

    /* 检查是否重复 */
    if (!$exc->is_only('goods_sn', $goods_sn, $goods_id))
    {
        make_json_error($_LANG['goods_sn_exists']);
    }

    make_json_result('');
}

elseif ($_REQUEST['act'] == 'ajaxEdit') {

    check_authz_json('goods_manage');
    $col = $_REQUEST['col'];
    $value = $_REQUEST['value'];
    $key = $_REQUEST['id'];
    $type = $_REQUEST['type'];

    if($col == 'vip_price') { // Spec Update
        $goods_id       = intval($_POST['id']);
        $val = $_POST['value'];
        $sql = "SELECT shop_price FROM " . $ecs->table('goods') .
                    " WHERE  goods_id = '$goods_id'";
        $good_price = $db->getOne($sql);
        /* If is a rate */
        if (strpos($val, "%")) {
            $rate = strstr($val, "%", true);
            $vip_price = intval($good_price * ($rate/100));
        } else {
            $vip_price = floatval($val);
        }

        /* If input @, delete vip price */
        if ($val == "@" || $val == "無") {
            $sql = "DELETE FROM ".$ecs->table('member_price') .
                " WHERE user_rank = '2' AND goods_id = '$goods_id'";
            $db->query($sql);
            clear_cache_files();
            $vip_abnormal_count = vip_abnormal_count(0);
            make_json_result("無",$vip_abnormal_count);
        } elseif ($vip_price < 0 || $vip_price == 0) {
            make_json_error("您好像输错了吧");
        } else {
            if ($vip_price > $good_price){
                make_json_error("VIP價不能大於價格！");
            }
            $sql = "SELECT COUNT(user_price) FROM " . $ecs->table('member_price') .
                    " WHERE user_rank = '2' AND goods_id = '$goods_id'";
            if ($db->getOne($sql) > 0) {//判断是否有USER_PRICE数值
                $sql =	"update " . $ecs->table('member_price') .
                        " set user_price = '$vip_price' WHERE user_rank ='2' AND goods_id = '$goods_id'";
                $db->query($sql);
            } else {
                $sql =	"INSERT INTO " . $ecs->table('member_price') ." (user_price,user_rank,goods_id) " .
                        " VALUES ('$vip_price','2', '$goods_id')";
                $db->query($sql);
            }
            clear_cache_files();
            $vip_abnormal_count = vip_abnormal_count(0);
            make_json_result(number_format($vip_price, 2, '.', ''),$vip_abnormal_count);
        }
    } elseif($col == 'sales_adjust' || $col == 'commission') {
        if (!$_SESSION['manage_cost'])
        {
            make_json_error('Admin only!');
        }
        $goodsController->ajaxEditAction();
    } elseif($col == 'goods_sn') {
        $goods_id = $key;
        $goods_sn = $value;
        /* 检查是否重复 */
        if (!$exc->is_only('goods_sn', $goods_sn, $goods_id))
        {
            make_json_error($_LANG['goods_sn_exists']);
        }

        if ($exc->edit("goods_sn = '$goods_sn', last_update=" .gmtime(), $goods_id))
        {
            clear_cache_files();
            make_json_result(stripslashes($goods_sn));
        }
    } elseif($col == 'cost') {
        if (!$_SESSION['manage_cost'])
        {
            make_json_error('Admin only!');
        }
        $goods_id       = $key;
        $cost           = $value;

        if (floatval($cost) < 0)
        {
            make_json_error('您輸入了一個非法的成本價。');
        }
        else
        {
            $old_cost = $db->getOne('SELECT cost FROM ' . $ecs->table('goods') . ' WHERE goods_id = ' . $goods_id . ' LIMIT 1');
            if ($exc->edit("cost = '$cost', last_update=" .gmtime(), $goods_id))
            {
                if ($old_cost != $cost)
                {
                    // Log the cost changes
                    $GLOBALS['db']->autoExecute($GLOBALS['ecs']->table('goods_cost_log'), array(
                        'goods_id' => $goods_id,
                        'old_cost' => $old_cost,
                        'new_cost' => $cost,
                        'update_type' => 2,
                        'update_time' => gmtime(),
                        'admin_id' => $_SESSION['admin_id']
                    ), 'INSERT');
                }
                clear_cache_files();
                make_json_result($cost);
            }
        }
    } else if ($col == 'shop_price') {
        check_authz_json('goods_manage');

        $goods_id = intval($_POST['id']);
        $val = $value;

        /* If is a rate */
        if(strpos($val, "%")){
            $rate = strstr($val, "%", true);
            $sql = "SELECT market_price FROM " . $ecs->table('goods') .
                    " WHERE  goods_id = '$goods_id'";
            $market_price = $db->getOne($sql);
            $goods_price = intval($market_price * ($rate/100) );
        } else {
            $goods_price = floatval($_POST['value']);
        }
        
        if ($goods_price < 0 || $goods_price == 0)
        {
            make_json_error($_LANG['shop_price_invalid']);
        }
        else
        {
            //判斷shop price是否小於vip price
            $sql = "SELECT user_price FROM " . $ecs->table('member_price') .
                    " WHERE user_rank = '2' AND goods_id = '$goods_id'";
            $vip_price = $db->getOne($sql);
            if ($goods_price < $vip_price) {
                make_json_error("價格不能小於VIP價！");
            }
            // Email notification for price changes
            $org_price = $db->getOne("SELECT shop_price FROM " . $ecs->table('goods') . " WHERE goods_id = '$goods_id'");
            if ($org_price != $goods_price)
            {
                if (@include_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_notifications.php'))
                {
                    notify_price_changed($db->getRow("SELECT goods_id, goods_name, goods_sn FROM " . $ecs->table('goods') . " WHERE goods_id = '$goods_id'"), $org_price, $goods_price);
                }
                else
                {
                    error_log(__FILE__.':'.__LINE__.' Cannot include lib_notifications.php');
                }

                // Log the price changes
                $GLOBALS['db']->autoExecute($GLOBALS['ecs']->table('goods_price_log'), array(
                    'goods_id' => $goods_id,
                    'old_price' => $org_price,
                    'new_price' => $goods_price,
                    'update_time' => gmtime(),
                    'admin_id' => $_SESSION['admin_id']
                ), 'INSERT');
            }

            //if ($exc->edit("shop_price = '$goods_price', market_price = '$price_rate', last_update=" .gmtime(), $goods_id))
            if ($exc->edit("shop_price = '$goods_price', last_update=" .gmtime(), $goods_id))
            {
                clear_cache_files();
                $vip_abnormal_count = vip_abnormal_count(0);
                make_json_result(number_format($goods_price, 2, '.', ''),$vip_abnormal_count);
            }
        }
    } else { //Normal update
        $goodsController->ajaxEditAction();
    }
}

elseif ($_REQUEST['act'] == 'preorder_text_update') {
    $_REQUEST['pre_sale_date'] = local_strtotime($_REQUEST['pre_sale_date']);
    $res = $goodsController->getGoodsPreOrderInfo($_REQUEST);
    make_json_result($res);

    exit;
}

/*------------------------------------------------------ */
//-- 排序、分页、查询
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'query')
{
    $goodsController->ajaxQueryAction();
}

/*------------------------------------------------------ */
//-- 放入回收站
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'remove')
{
    $goods_id = intval($_REQUEST['id']);

    /* 检查权限 */
    check_authz_json('remove_back');

    if ($exc->edit("is_delete = 1", $goods_id))
    {
        clear_cache_files();
        $goods_name = $exc->get_name($goods_id);

        admin_log(addslashes($goods_name), 'trash', 'goods'); // 记录日志

        $url = 'goods.php?act=list&' . str_replace('act=remove', '', $_SERVER['QUERY_STRING']);
        make_json_result($url);

        exit;
    }
}

/*------------------------------------------------------ */
//-- 还原回收站中的商品
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'restore_goods')
{
    $goods_id = intval($_REQUEST['id']);

    check_authz_json('remove_back'); // 检查权限

    $exc->edit("is_delete = 0, add_time = '" . gmtime() . "'", $goods_id);
    clear_cache_files();

    $goods_name = $exc->get_name($goods_id);

    admin_log(addslashes($goods_name), 'restore', 'goods'); // 记录日志

    $url = 'goods.php?act=query&' . str_replace('act=restore_goods', '', $_SERVER['QUERY_STRING']);

    ecs_header("Location: $url\n");
    exit;
}

/*------------------------------------------------------ */
//-- 彻底删除商品
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'drop_goods')
{
    // 检查权限
    check_authz_json('remove_back');

    // 取得参数
    $goods_id = intval($_REQUEST['id']);
    if ($goods_id <= 0)
    {
        make_json_error('invalid params');
    }

    /* 取得商品信息 */
    $sql = "SELECT goods_id, goods_name, is_delete, is_real, goods_thumb, " .
                "goods_img, original_img " .
            "FROM " . $ecs->table('goods') .
            " WHERE goods_id = '$goods_id'";
    $goods = $db->getRow($sql);
    if (empty($goods))
    {
        make_json_error($_LANG['goods_not_exist']);
    }

    if ($goods['is_delete'] != 1)
    {
        make_json_error($_LANG['goods_not_in_recycle_bin']);
    }

    /* 删除商品图片和轮播图片 */
    if (!empty($goods['goods_thumb']))
    {
        @unlink('../' . $goods['goods_thumb']);
    }
    if (!empty($goods['goods_img']))
    {
        @unlink('../' . $goods['goods_img']);
    }
    if (!empty($goods['original_img']))
    {
        @unlink('../' . $goods['original_img']);
    }
    /* 删除商品 */
    $exc->drop($goods_id);

    /* 删除商品的货品记录 */
    $sql = "DELETE FROM " . $ecs->table('products') .
            " WHERE goods_id = '$goods_id'";
    $db->query($sql);

    /* 记录日志 */
    admin_log(addslashes($goods['goods_name']), 'remove', 'goods');

    /* 删除商品相册 */
    $sql = "SELECT img_url, thumb_url, img_original " .
            "FROM " . $ecs->table('goods_gallery') .
            " WHERE goods_id = '$goods_id'";
    $res = $db->query($sql);
    while ($row = $db->fetchRow($res))
    {
        if (!empty($row['img_url']))
        {
            @unlink('../' . $row['img_url']);
        }
        if (!empty($row['thumb_url']))
        {
            @unlink('../' . $row['thumb_url']);
        }
        if (!empty($row['img_original']))
        {
            @unlink('../' . $row['img_original']);
        }
    }

    $sql = "DELETE FROM " . $ecs->table('goods_gallery') . " WHERE goods_id = '$goods_id'";
    $db->query($sql);

    /* 删除相关表记录 */
    $sql = "DELETE FROM " . $ecs->table('collect_goods') . " WHERE goods_id = '$goods_id'";
    $db->query($sql);
    $sql = "DELETE FROM " . $ecs->table('goods_article') . " WHERE goods_id = '$goods_id'";
    $db->query($sql);
    $sql = "DELETE FROM " . $ecs->table('goods_attr') . " WHERE goods_id = '$goods_id'";
    $db->query($sql);
    $sql = "DELETE FROM " . $ecs->table('goods_cat') . " WHERE goods_id = '$goods_id'";
    $db->query($sql);
    $sql = "DELETE FROM " . $ecs->table('member_price') . " WHERE goods_id = '$goods_id'";
    $db->query($sql);
    $sql = "DELETE FROM " . $ecs->table('group_goods') . " WHERE parent_id = '$goods_id'";
    $db->query($sql);
    $sql = "DELETE FROM " . $ecs->table('group_goods') . " WHERE goods_id = '$goods_id'";
    $db->query($sql);
    $sql = "DELETE FROM " . $ecs->table('link_goods') . " WHERE goods_id = '$goods_id'";
    $db->query($sql);
    $sql = "DELETE FROM " . $ecs->table('link_goods') . " WHERE link_goods_id = '$goods_id'";
    $db->query($sql);
    $sql = "DELETE FROM " . $ecs->table('tag') . " WHERE goods_id = '$goods_id'";
    $db->query($sql);
    $sql = "DELETE FROM " . $ecs->table('comment') . " WHERE comment_type = 0 AND id_value = '$goods_id'";
    $db->query($sql);
    $sql = "DELETE FROM " . $ecs->table('booking_goods') . " WHERE goods_id = '$goods_id'";
    $db->query($sql);
    $sql = "DELETE FROM " . $ecs->table('goods_activity') . " WHERE goods_id = '$goods_id'";
    $db->query($sql);

    // 刪除相關預售登記
    $sql = "DELETE FROM " . $ecs->table('goods_book') . " WHERE goods_id = '$goods_id'";
    $db->query($sql);
    // 刪除相關缺貨登記
    $sql = "DELETE FROM " . $ecs->table('goods_dai') . " WHERE goods_id = '$goods_id'";
    $db->query($sql);
    // 刪除相關自訂欄位
    $sql = "DELETE FROM " . $ecs->table('goods_extra_info') . " WHERE goods_id = '$goods_id'";
    $db->query($sql);
    // 刪除相關推廣鏈結
    $sql = "DELETE FROM " . $ecs->table('goods_promo_links') . " WHERE goods_id = '$goods_id'";
    $db->query($sql);
    // 刪除pricecomhk
    $sql = "DELETE FROM " . $ecs->table('goods_pricecomhk') . " WHERE goods_id = '$goods_id'";
    $db->query($sql);
    // 刪除hktvmall
    $sql = "DELETE FROM " . $ecs->table('goods_hktvmall') . " WHERE goods_id = '$goods_id'";
    $db->query($sql);
    // 刪除fortress
    $sql = "DELETE FROM " . $ecs->table('goods_fortress') . " WHERE goods_id = '$goods_id'";
    $db->query($sql);

    /* 如果不是实体商品，删除相应虚拟商品记录 */
    if ($goods['is_real'] != 1)
    {
        $sql = "DELETE FROM " . $ecs->table('virtual_card') . " WHERE goods_id = '$goods_id'";
        if (!$db->query($sql, 'SILENT') && $db->errno() != 1146)
        {
            die($db->error());
        }
    }
    // Flush Category Thumb memcached
    $categoryController->flushCatThumbMemcache(0, $goods_id);
    clear_cache_files();
    $url = 'goods.php?act=query&' . str_replace('act=drop_goods', '', $_SERVER['QUERY_STRING']);

    ecs_header("Location: $url\n");

    exit;
}

/*------------------------------------------------------ */
//-- 切换商品类型
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'get_attr')
{
    check_authz_json('goods_manage');

    $goods_id   = empty($_GET['goods_id']) ? 0 : intval($_GET['goods_id']);
    $goods_type = empty($_GET['goods_type']) ? 0 : intval($_GET['goods_type']);

    $content    = build_attr_html($goods_type, $goods_id);

    make_json_result($content);
}

/*------------------------------------------------------ */
//-- 删除图片
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'drop_image')
{
    check_authz_json('goods_manage');

    $img_id = empty($_REQUEST['img_id']) ? 0 : intval($_REQUEST['img_id']);

    /* 删除图片文件 */
    $sql = "SELECT img_url, thumb_url, img_original " .
            " FROM " . $GLOBALS['ecs']->table('goods_gallery') .
            " WHERE img_id = '$img_id'";
    $row = $GLOBALS['db']->getRow($sql);

    if ($row['img_url'] != '' && is_file('../' . $row['img_url']))
    {
        @unlink('../' . $row['img_url']);
    }
    if ($row['thumb_url'] != '' && is_file('../' . $row['thumb_url']))
    {
        @unlink('../' . $row['thumb_url']);
    }
    if ($row['img_original'] != '' && is_file('../' . $row['img_original']))
    {
        @unlink('../' . $row['img_original']);
    }

    /* 删除数据 */
    $sql = "DELETE FROM " . $GLOBALS['ecs']->table('goods_gallery') . " WHERE img_id = '$img_id' LIMIT 1";
    $GLOBALS['db']->query($sql);

    clear_cache_files();

    header('Content-Type: application/json');
    make_json_result($img_id);
}

elseif ($_REQUEST['act'] == 'get_gallery_images')
{
    check_authz_json('goods_manage');

    $goods_id = empty($_REQUEST['goods_id']) ? 0 : intval($_REQUEST['goods_id']);

    /* 取得產品相冊圖片 */
    $sql = "SELECT * " .
            " FROM " . $GLOBALS['ecs']->table('goods_gallery') .
            " WHERE goods_id = '$goods_id'" .
            " ORDER BY `sort_order`";
    $res = $GLOBALS['db']->getAll($sql);

    header('Content-Type: application/json');
    make_json_result($res);
}

elseif ($_REQUEST['act'] == 'add_gallery_image')
{
    check_authz_json('goods_manage');

    $goods_id = intval($_POST['goods_id']);

    if (isset($_FILES['img_url']) &&
        isset($_POST['img_desc']) && is_array($_POST['img_desc']) &&
        isset($_POST['img_file']) && is_array($_POST['img_file']))
    {
        /* 处理相册图片 */
        handle_gallery_image($goods_id, $_FILES['img_url'], $_POST['img_desc'], $_POST['img_file']);
    }

    header('Content-Type: application/json');
    make_json_result($goods_id);
}

/*------------------------------------------------------ */
//-- 搜索商品，仅返回名称及ID
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'get_goods_list')
{
    include_once(ROOT_PATH . 'includes/cls_json.php');
    $json = new JSON;

    $filters = $json->decode($_GET['JSON']);

    $arr = get_goods_list($filters);
    $opt = array();

    foreach ($arr AS $key => $val)
    {
        $opt[] = array('value' => $val['goods_id'],
                        'text' => $val['goods_name'],
                        'data' => $val['shop_price']);
    }

    make_json_result($opt);
}

/*------------------------------------------------------ */
//-- 把商品加入关联
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'add_link_goods')
{
    include_once(ROOT_PATH . 'includes/cls_json.php');
    $json = new JSON;

    check_authz_json('goods_manage');

    $linked_array   = $json->decode($_GET['add_ids']);
    $linked_goods   = $json->decode($_GET['JSON']);
    $goods_id       = $linked_goods[0];
    $is_double      = $linked_goods[1] == true ? 0 : 1;

    foreach ($linked_array AS $val)
    {
        if ($is_double)
        {
            /* 双向关联 */
            $sql = "INSERT INTO " . $ecs->table('link_goods') . " (goods_id, link_goods_id, is_double, admin_id) " .
                    "VALUES ('$val', '$goods_id', '$is_double', '$_SESSION[admin_id]')";
            $db->query($sql, 'SILENT');
        }

        $sql = "INSERT INTO " . $ecs->table('link_goods') . " (goods_id, link_goods_id, is_double, admin_id) " .
                "VALUES ('$goods_id', '$val', '$is_double', '$_SESSION[admin_id]')";
        $db->query($sql, 'SILENT');
    }

    $linked_goods   = get_linked_goods($goods_id);
    $options        = array();

    foreach ($linked_goods AS $val)
    {
        $options[] = array('value'  => $val['goods_id'],
                        'text'      => $val['goods_name'],
                        'data'      => '');
    }

    clear_cache_files();
    make_json_result($options);
}

/*------------------------------------------------------ */
//-- 删除关联商品
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'drop_link_goods')
{
    include_once(ROOT_PATH . 'includes/cls_json.php');
    $json = new JSON;

    check_authz_json('goods_manage');

    $drop_goods     = $json->decode($_GET['drop_ids']);
    $drop_goods_ids = db_create_in($drop_goods);
    $linked_goods   = $json->decode($_GET['JSON']);
    $goods_id       = $linked_goods[0];
    $is_signle      = $linked_goods[1];

    if (!$is_signle)
    {
        $sql = "DELETE FROM " .$ecs->table('link_goods') .
                " WHERE link_goods_id = '$goods_id' AND goods_id " . $drop_goods_ids;
    }
    else
    {
        $sql = "UPDATE " .$ecs->table('link_goods') . " SET is_double = 0 ".
                " WHERE link_goods_id = '$goods_id' AND goods_id " . $drop_goods_ids;
    }
    if ($goods_id == 0)
    {
        $sql .= " AND admin_id = '$_SESSION[admin_id]'";
    }
    $db->query($sql);

    $sql = "DELETE FROM " .$ecs->table('link_goods') .
            " WHERE goods_id = '$goods_id' AND link_goods_id " . $drop_goods_ids;
    if ($goods_id == 0)
    {
        $sql .= " AND admin_id = '$_SESSION[admin_id]'";
    }
    $db->query($sql);

    $linked_goods = get_linked_goods($goods_id);
    $options      = array();

    foreach ($linked_goods AS $val)
    {
        $options[] = array(
                        'value' => $val['goods_id'],
                        'text'  => $val['goods_name'],
                        'data'  => '');
    }

    clear_cache_files();
    make_json_result($options);
}

/*------------------------------------------------------ */
//-- 增加一个配件
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'add_group_goods')
{
    include_once(ROOT_PATH . 'includes/cls_json.php');
    $json = new JSON;

    check_authz_json('goods_manage');

    $fittings   = $json->decode($_GET['add_ids']);
    $arguments  = $json->decode($_GET['JSON']);
    $goods_id   = $arguments[0];
    $price      = $arguments[1];

    foreach ($fittings AS $val)
    {
        $sql = "INSERT INTO " . $ecs->table('group_goods') . " (parent_id, goods_id, goods_price, admin_id) " .
                "VALUES ('$goods_id', '$val', '$price', '$_SESSION[admin_id]')";
        $db->query($sql, 'SILENT');
    }

    $arr = get_group_goods($goods_id);
    $opt = array();

    foreach ($arr AS $val)
    {
        $opt[] = array('value'      => $val['goods_id'],
                        'text'      => $val['goods_name'],
                        'data'      => '');
    }

    clear_cache_files();
    make_json_result($opt);
}

/*------------------------------------------------------ */
//-- 删除一个配件
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'drop_group_goods')
{
    include_once(ROOT_PATH . 'includes/cls_json.php');
    $json = new JSON;

    check_authz_json('goods_manage');

    $fittings   = $json->decode($_GET['drop_ids']);
    $arguments  = $json->decode($_GET['JSON']);
    $goods_id   = $arguments[0];
    $price      = $arguments[1];

    $sql = "DELETE FROM " .$ecs->table('group_goods') .
            " WHERE parent_id='$goods_id' AND " .db_create_in($fittings, 'goods_id');
    if ($goods_id == 0)
    {
        $sql .= " AND admin_id = '$_SESSION[admin_id]'";
    }
    $db->query($sql);

    $arr = get_group_goods($goods_id);
    $opt = array();

    foreach ($arr AS $val)
    {
        $opt[] = array('value'      => $val['goods_id'],
                        'text'      => $val['goods_name'],
                        'data'      => '');
    }

    clear_cache_files();
    make_json_result($opt);
}

/*------------------------------------------------------ */
//-- 搜索文章
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'get_article_list')
{
    include_once(ROOT_PATH . 'includes/cls_json.php');
    $json = new JSON;

    $filters =(array) $json->decode(json_str_iconv($_GET['JSON']));

    $where = " WHERE cat_id > 0 ";
    if (!empty($filters['title']))
    {
        $keyword  = trim($filters['title']);
        $where   .=  " AND title LIKE '%" . mysql_like_quote($keyword) . "%' ";
    }

    $sql        = 'SELECT article_id, title FROM ' .$ecs->table('article'). $where.
                  'ORDER BY article_id DESC LIMIT 50';
    $res        = $db->query($sql);
    $arr        = array();

    while ($row = $db->fetchRow($res))
    {
        $arr[]  = array('value' => $row['article_id'], 'text' => $row['title'], 'data'=>'');
    }

    make_json_result($arr);
}

/*------------------------------------------------------ */
//-- 添加关联文章
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'add_goods_article')
{
    include_once(ROOT_PATH . 'includes/cls_json.php');
    $json = new JSON;

    check_authz_json('goods_manage');

    $articles   = $json->decode($_GET['add_ids']);
    $arguments  = $json->decode($_GET['JSON']);
    $goods_id   = $arguments[0];

    foreach ($articles AS $val)
    {
        $sql = "INSERT INTO " . $ecs->table('goods_article') . " (goods_id, article_id, admin_id) " .
                "VALUES ('$goods_id', '$val', '$_SESSION[admin_id]')";
        $db->query($sql);
    }

    $arr = get_goods_articles($goods_id);
    $opt = array();

    foreach ($arr AS $val)
    {
        $opt[] = array('value'      => $val['article_id'],
                        'text'      => $val['title'],
                        'data'      => '');
    }

    clear_cache_files();
    make_json_result($opt);
}

/*------------------------------------------------------ */
//-- 删除关联文章
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'drop_goods_article')
{
    include_once(ROOT_PATH . 'includes/cls_json.php');
    $json = new JSON;

    check_authz_json('goods_manage');

    $articles   = $json->decode($_GET['drop_ids']);
    $arguments  = $json->decode($_GET['JSON']);
    $goods_id   = $arguments[0];

    $sql = "DELETE FROM " .$ecs->table('goods_article') . " WHERE " . db_create_in($articles, "article_id") . " AND goods_id = '$goods_id'";
    $db->query($sql);

    $arr = get_goods_articles($goods_id);
    $opt = array();

    foreach ($arr AS $val)
    {
        $opt[] = array('value'      => $val['article_id'],
                        'text'      => $val['title'],
                        'data'      => '');
    }

    clear_cache_files();
    make_json_result($opt);
}

/*------------------------------------------------------ */
//-- 產品資料庫相關
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'search_proddb')
{
    $keyword = empty($_REQUEST['keyword']) ? '' : $_REQUEST['keyword'];
    
    require_once(ROOT_PATH . ADMIN_PATH . '/includes/lib_proddb.php');
    
    header('Content-Type: application/json;charset=utf-8');
    echo json_encode($proddb->search_product($keyword));
    exit;
}
elseif ($_REQUEST['act'] == 'import_proddb')
{
    $product_id = empty($_REQUEST['product_id']) ? '' : $_REQUEST['product_id'];
    
    require_once(ROOT_PATH . ADMIN_PATH . '/includes/lib_proddb.php');
    
    header('Content-Type: application/json;charset=utf-8');
    echo json_encode($proddb->get_product_by_id($product_id));
    exit;
}
elseif ($_REQUEST['act'] == 'user_rank_price')
{
    $goodsController->urpAction();
}
elseif ($_REQUEST['act'] == 'update_urp')
{
    $goodsController->updateUrpAction();
}

/**
 * 列表链接
 * @param   bool    $is_add         是否添加（插入）
 * @param   string  $extension_code 虚拟商品扩展代码，实体商品为空
 * @return  array('href' => $href, 'text' => $text)
 */
function list_link($is_add = true, $extension_code = '')
{
    $href = 'goods.php?act=list';
    if (!empty($extension_code))
    {
        $href .= '&extension_code=' . $extension_code;
    }
    if (!$is_add)
    {
        $href .= '&' . list_link_postfix();
    }

    if ($extension_code == 'virtual_card')
    {
        $text = $GLOBALS['_LANG']['50_virtual_card_list'];
    }
    else
    {
        $text = $GLOBALS['_LANG']['01_goods_list'];
    }

    return array('href' => $href, 'text' => $text);
}


/**
 * 添加链接
 * @param   string  $extension_code 虚拟商品扩展代码，实体商品为空
 * @return  array('href' => $href, 'text' => $text)
 */
function add_link($extension_code = '')
{
    $href = 'goods.php?act=add';
    if (!empty($extension_code))
    {
        $href .= '&extension_code=' . $extension_code;
    }

    if ($extension_code == 'virtual_card')
    {
        $text = $GLOBALS['_LANG']['51_virtual_card_add'];
    }
    else
    {
        $text = $GLOBALS['_LANG']['02_goods_add'];
    }

    return array('href' => $href, 'text' => $text);
}

/**
 * 检查图片网址是否合法
 *
 * @param string $url 网址
 *
 * @return boolean
 */
function goods_parse_url($url)
{
    $parse_url = @parse_url($url);
    return (!empty($parse_url['scheme']) && !empty($parse_url['host']));
}

/**
 * 保存某商品的优惠价格
 * @param   int     $goods_id    商品编号
 * @param   array   $number_list 优惠数量列表
 * @param   array   $price_list  价格列表
 * @return  void
 */
function handle_volume_price($goods_id, $number_list, $price_list)
{
    $sql = "DELETE FROM " . $GLOBALS['ecs']->table('volume_price') .
           " WHERE price_type = '1' AND goods_id = '$goods_id'";
    $GLOBALS['db']->query($sql);


    /* 循环处理每个优惠价格 */
    foreach ($price_list AS $key => $price)
    {
        /* 价格对应的数量上下限 */
        $volume_number = $number_list[$key];

        if (!empty($price))
        {
            $sql = "INSERT INTO " . $GLOBALS['ecs']->table('volume_price') .
                   " (price_type, goods_id, volume_number, volume_price) " .
                   "VALUES ('1', '$goods_id', '$volume_number', '$price')";
            $GLOBALS['db']->query($sql);
        }
    }
}

?>
