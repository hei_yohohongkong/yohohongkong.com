<?php

/***
* stock_cost.php
* by howang 2014-06-30
*
* Current Stock Cost Calculator for YOHO Hong Kong
***/


define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require_once(ROOT_PATH . 'includes/lib_order.php');
require_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_goods.php');
require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/admin/statistic.php');
$smarty->assign('lang', $_LANG);
$userController = new Yoho\cms\Controller\UserController();
$erpController = new Yoho\cms\Controller\ErpController();
$categoryController = new Yoho\cms\Controller\CategoryController();
$_REQUEST['order_type'] = isset($_REQUEST['order_type']) ? $_REQUEST['order_type'] : 4;
if (isset($_REQUEST['act']) && $_REQUEST['act'] == 'detail')
{
	/* 权限判断 */
	admin_priv('sale_order_stats');
	
	$cat_id = empty($_REQUEST['cat_id']) ? 0 : intval($_REQUEST['cat_id']);
	if ($cat_id > 0)
	{
		$cat_name = $db->getOne('SELECT cat_name FROM ' . $ecs->table('category') . ' WHERE cat_id = \'' . $cat_id . '\'');
	}
	else
	{
		$cat_name = '所有分類';
	}

	$data = get_stock_cost_detail();

	$smarty->assign('filter',           $data['filter']);
	$smarty->assign('record_count',     $data['record_count']);
	$smarty->assign('page_count',       $data['page_count']);
	$smarty->assign('cost_list',        $data['data']);
	$smarty->assign('ur_here',          '成本總計詳情');
	$smarty->assign('full_page',        1);
	$smarty->assign('cat_id',           $cat_id);
	$smarty->assign('cat_name',         $cat_name);
	$smarty->assign('cat_list',         cat_list(0, $cat_id));
	$smarty->assign('cfg_lang',         $_CFG['lang']);
	$smarty->assign('action_link',      array('text' => '下載成本總計報表','href'=>'#download'));

	/* 显示页面 */
	assign_query_info();
	$smarty->display('stock_cost_detail.htm');
}
else if (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'query_detail' ||  $_REQUEST['act'] == 'download_detail'))
{
	/* 检查权限 */
	check_authz_json('sale_order_stats');
	
	$cat_id = empty($_REQUEST['cat_id']) ? 0 : intval($_REQUEST['cat_id']);
	if ($cat_id > 0)
	{
		$cat_name = $db->getOne('SELECT cat_name FROM ' . $ecs->table('category') . ' WHERE cat_id = \'' . $cat_id . '\'');
	}
	else
	{
		$cat_name = '所有分類';
	}
	
	/*------------------------------------------------------ */
	//--Excel文件下载
	/*------------------------------------------------------ */
	if ($_REQUEST['act'] == 'download_detail')
	{
		$file_name = local_date('Y-m-d') . '_' . $cat_name . '_stock_cost_detail';
		$data = get_stock_cost_detail(false);
		header('Content-type: application/vnd.ms-excel; charset=utf-8');
		header('Content-Disposition: attachment; filename=' . $file_name . '.xls');
		
		require_once ROOT_PATH . 'includes/excel/PHPExcel.php';
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		
		$objPHPExcel->getActiveSheet()->setTitle('成本總計詳情');
		
		/* 文件标题 */
		$objPHPExcel->getActiveSheet()
		->setCellValue('A1', ecs_iconv(EC_CHARSET, 'UTF8', local_date('Y-m-d') . ' 成本總計詳情'))
		->setCellValue('A2', ecs_iconv(EC_CHARSET, 'UTF8', '產品名稱'))
		->setCellValue('B2', ecs_iconv(EC_CHARSET, 'UTF8', '存貨'))
		->setCellValue('C2', ecs_iconv(EC_CHARSET, 'UTF8', '成本'))
		->setCellValue('D2', ecs_iconv(EC_CHARSET, 'UTF8', '總成本'));
		$objPHPExcel->getActiveSheet()->mergeCells('A1:D1');
		
		$i = 3;
		foreach ($data['data'] AS $key => $value)
		{
			$objPHPExcel->getActiveSheet()
			->setCellValue('A'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['goods_name']))
			->setCellValue('B'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['goods_number']))
			->setCellValue('C'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['cost']))
			->setCellValue('D'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['total_cost']));
			$i++;
		}
		
		for ($col = 'A'; $col <= 'D'; $col++)
		{
			$objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
		}
		
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		
		exit;
	}

	$data = get_stock_cost_detail();

	$smarty->assign('cost_list',       $data['data']);
	$smarty->assign('filter',          $data['filter']);
	$smarty->assign('record_count',    $data['record_count']);
	$smarty->assign('page_count',      $data['page_count']);

	make_json_result($smarty->fetch('stock_cost_detail.htm'), '', array('filter' => $data['filter'], 'page_count' => $data['page_count']));
}
else if (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'query' ||  $_REQUEST['act'] == 'download'))
{
	/* 检查权限 */
	check_authz_json('sale_order_stats');
	
	if (strstr($_REQUEST['start_date'], '-') === false)
	{
		$_REQUEST['start_date'] = local_date('Y-m-d', $_REQUEST['start_date']);
		$_REQUEST['end_date'] = local_date('Y-m-d', $_REQUEST['end_date']);
	}
	
	/*------------------------------------------------------ */
	//--Excel文件下载
	/*------------------------------------------------------ */
	if ($_REQUEST['act'] == 'download')
	{
		require_once ROOT_PATH . 'includes/excel/PHPExcel.php';
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		
		$objPHPExcel->getActiveSheet()->setTitle('成本總計');
		$column_list = array();
		foreach(range('A', 'Z') as $letter)
		{
			$column_list[] = $letter;
		}
		foreach(range('A', 'Z') as $letter)
		{
			$column_list[] = 'A'.$letter;
		}
		foreach(range('A', 'Z') as $letter)
		{
			$column_list[] = 'B'.$letter;
		}
		
		if (!empty($_REQUEST['period']))
		{
			$period = (int)$_REQUEST['period'];
			$file_name = local_date('Y-m', strtotime('-1 months', strtotime(month_start()))) . '_to_' . local_date('Y-m', strtotime('-' . $period . ' months', strtotime(month_start()))) . '_sale_category';

			if($_REQUEST['order_type'] == 4){
				$value_type_list = array(
					array('name'=>'平均存貨成本', 'value_key'=>'avg_stock_cost'),
					array('name'=>'Turnover', 'value_key'=>'turnover'),
					array('name'=>'實際銷售總額(扣除手續費&扣除折扣及優惠券)', 'value_key'=>'total_sa_revenue_n_ds'),
					array('name'=>'實際盈利總額(扣除手續費&扣除折扣及優惠券)', 'value_key'=>'total_sa_profit_n_ds'),
					array('name'=>'實際盈利率(扣除手續費&扣除折扣及優惠券)', 'value_key'=>'sa_profit_margin_n_ds_formated')
				);
			}else if($_REQUEST['order_type'] == 3){
				$value_type_list = array(
					array('name'=>'銷售總額', 'value_key'=>'total_revenue_n_ds'),
					array('name'=>'實際銷售總額(扣除手續費&扣除折扣及優惠券)', 'value_key'=>'total_sa_revenue_n_ds'),
					array('name'=>'盈利總額(扣除折扣及優惠券)', 'value_key'=>'total_profit_n_ds'),
					array('name'=>'實際盈利總額(扣除手續費&扣除折扣及優惠券)', 'value_key'=>'total_sa_profit_n_ds'),
					array('name'=>'盈利率(扣除折扣及優惠券)', 'value_key'=>'profit_margin_n_ds_formated'),
					array('name'=>'實際盈利率(扣除手續費&扣除折扣及優惠券)', 'value_key'=>'sa_profit_margin_n_ds_formated')
				);
			} else {
				$value_type_list = array(
					array('name'=>'銷售總額(扣除折扣及優惠券)', 'value_key'=>'total_revenue_n_ds'),
					array('name'=>'盈利總額(扣除折扣及優惠券)', 'value_key'=>'total_profit_n_ds'),
					array('name'=>'盈利率(扣除折扣及優惠券)', 'value_key'=>'profit_margin_n_ds_formated')
				);
			}
			$vi_num = count($value_type_list);
			for ($vi = 0; $vi < $vi_num; $vi++)
			{
				if ($vi > 0)
				{
					$objWorkSheet = $objPHPExcel->createSheet();
				}
				$objPHPExcel->setActiveSheetIndex($vi);
				$objPHPExcel->getActiveSheet()->setTitle($value_type_list[$vi]['name']);
				
				for ($pi = 0; $pi < $period; $pi++)
				{
					/* 文件标题 */
					$objPHPExcel->getActiveSheet()
					->setCellValue($column_list[$pi+1].'1', ecs_iconv(EC_CHARSET, 'UTF8', local_date('M Y', strtotime('-'.($pi+1).' months', strtotime(month_start()))) ));
				}
				$style = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					)
				);
				$objPHPExcel->getActiveSheet()
				->setCellValue($column_list[0].'2', ecs_iconv(EC_CHARSET, 'UTF8', '分類'))
				->setCellValue($column_list[1].'2', ecs_iconv(EC_CHARSET, 'UTF8', $value_type_list[$vi]['name']))
				->mergeCells($column_list[1].'2:'.$column_list[$period+1].'2')
				->getStyle($column_list[1].'2:'.$column_list[$period+1].'2')->applyFromArray($style);
				
				$_REQUEST['start_date'] = local_date('Y-m-01', strtotime('-'.($pi+1).' months', strtotime(month_start())));
				$_REQUEST['end_date'] = local_date('Y-m-t', strtotime('-'.($pi+1).' months', strtotime(month_start())));
				$stock_cost_data = $categoryController->get_stock_cost(false);
				$i = 3;
				foreach ($stock_cost_data['stock_cost_data'] as $value)
				{
					$objPHPExcel->getActiveSheet()
					->setCellValue($column_list[0].$i, ecs_iconv(EC_CHARSET, 'UTF8', ($i == 3 ? '' : '　') . $value['name']));
					$i++;
					if (isset($value['children']))
					foreach ($value['children'] as $child)
					{
						$objPHPExcel->getActiveSheet()
						->setCellValue($column_list[0].$i, ecs_iconv(EC_CHARSET, 'UTF8', '　　' . $child['name']));
						$i++;
						if (isset($child['children']))
						foreach ($child['children'] as $grandson)
						{
							$objPHPExcel->getActiveSheet()
							->setCellValue($column_list[0].$i, ecs_iconv(EC_CHARSET, 'UTF8', '　　　' . $grandson['name']));
							$i++;
						}
					}
				}
				
				for ($pi = 0; $pi < $period; $pi++)
				{
					$_REQUEST['start_date'] = local_date('Y-m-01', strtotime('-'.($pi+1).' months', strtotime(month_start())));
					$_REQUEST['end_date'] = local_date('Y-m-t', strtotime('-'.($pi+1).' months', strtotime(month_start())));
					$stock_cost_data = $categoryController->get_stock_cost(false);
					$i = 3;
					foreach ($stock_cost_data['stock_cost_data'] as $value)
					{
						$objPHPExcel->getActiveSheet()
						->setCellValue($column_list[1+$pi].$i, ecs_iconv(EC_CHARSET, 'UTF8', $value[$value_type_list[$vi]['value_key']]));
						if($value_type_list[$vi]['value_key'] != "total_stocks" && $value_type_list[$vi]['value_key'] != "turnover")
							$objPHPExcel->getActiveSheet()->getStyle($column_list[1+$pi].$i)->getNumberFormat()->setFormatCode('$0.00');
						$i++;
						if (isset($value['children']))
						foreach ($value['children'] as $child)
						{
							$objPHPExcel->getActiveSheet()
							->setCellValue($column_list[1+$pi].$i, ecs_iconv(EC_CHARSET, 'UTF8', $child[$value_type_list[$vi]['value_key']]));
							if($value_type_list[$vi]['value_key'] != "total_stocks" && $value_type_list[$vi]['value_key'] != "turnover")
								$objPHPExcel->getActiveSheet()->getStyle($column_list[1+$pi].$i)->getNumberFormat()->setFormatCode('$0.00');
							$i++;
							if (isset($child['children']))
							foreach ($child['children'] as $grandson)
							{
								$objPHPExcel->getActiveSheet()
								->setCellValue($column_list[1+$pi].$i, ecs_iconv(EC_CHARSET, 'UTF8', $grandson[$value_type_list[$vi]['value_key']]));
								if($value_type_list[$vi]['value_key'] != "total_stocks" && $value_type_list[$vi]['value_key'] != "turnover")
									$objPHPExcel->getActiveSheet()->getStyle($column_list[1+$pi].$i)->getNumberFormat()->setFormatCode('$0.00');
								$i++;
							}
						}
					}
				}
				
				for ($s = 0; $s < $period + 1; $s++)
				{
					$objPHPExcel->getActiveSheet()->getColumnDimension($column_list[$s])->setAutoSize(true);
				}
			}
			$objPHPExcel->setActiveSheetIndex(0);
		}
		else
		{
			$file_name = $_REQUEST['start_date'] . '_to_' . $_REQUEST['end_date'] . '_sale_category';
			
			$stock_cost_data = $categoryController->get_stock_cost(false);
			if($_REQUEST['order_type'] == 4){
				$objPHPExcel->getActiveSheet()
				->setCellValue('A1', ecs_iconv(EC_CHARSET, 'UTF8', $_REQUEST['start_date'] . '至' . $_REQUEST['end_date'] . ' 分類報告'))
				->setCellValue('A2', ecs_iconv(EC_CHARSET, 'UTF8', '分類'))
				->setCellValue('B2', ecs_iconv(EC_CHARSET, 'UTF8', '即時存貨總數'))
				->setCellValue('C2', ecs_iconv(EC_CHARSET, 'UTF8', '平均存貨成本'))
				->setCellValue('D2', ecs_iconv(EC_CHARSET, 'UTF8', 'Turnover'))
				->setCellValue('E2', ecs_iconv(EC_CHARSET, 'UTF8', '即時存貨成本(Avg.)'))
				->setCellValue('F2', ecs_iconv(EC_CHARSET, 'UTF8', '即時存貨成本(FIFO)'))
				->setCellValue('G2', ecs_iconv(EC_CHARSET, 'UTF8', '平均存貨成本(呆壞貨)'))
				->setCellValue('H2', ecs_iconv(EC_CHARSET, 'UTF8', '實際銷售總額(扣除手續費&扣除折扣及優惠券)'))
				->setCellValue('I2', ecs_iconv(EC_CHARSET, 'UTF8', '實際盈利總額(扣除手續費&扣除折扣及優惠券)'))
				->setCellValue('J2', ecs_iconv(EC_CHARSET, 'UTF8', '實際盈利率(扣除手續費&扣除折扣及優惠券)'));
				$objPHPExcel->getActiveSheet()->mergeCells('A1:J1');
			}else if($_REQUEST['order_type'] == 3){
				/* 文件标题 */
				$objPHPExcel->getActiveSheet()
				->setCellValue('A1', ecs_iconv(EC_CHARSET, 'UTF8', $_REQUEST['start_date'] . '至' . $_REQUEST['end_date'] . ' 分類報告'))
				->setCellValue('A2', ecs_iconv(EC_CHARSET, 'UTF8', '分類'))
				->setCellValue('B2', ecs_iconv(EC_CHARSET, 'UTF8', '即時存貨總數'))
				->setCellValue('C2', ecs_iconv(EC_CHARSET, 'UTF8', '銷售總額(扣除折扣及優惠券)'))
				->setCellValue('D2', ecs_iconv(EC_CHARSET, 'UTF8', '實際銷售總額(扣除手續費&扣除折扣及優惠券)'))
				->setCellValue('E2', ecs_iconv(EC_CHARSET, 'UTF8', '盈利總額(扣除折扣及優惠券)'))
				->setCellValue('F2', ecs_iconv(EC_CHARSET, 'UTF8', '實際盈利總額(扣除手續費&扣除折扣及優惠券)'))
				->setCellValue('G2', ecs_iconv(EC_CHARSET, 'UTF8', '盈利率(扣除折扣及優惠券)'))
				->setCellValue('H2', ecs_iconv(EC_CHARSET, 'UTF8', '實際盈利率(扣除手續費&扣除折扣及優惠券)'));
				$objPHPExcel->getActiveSheet()->mergeCells('A1:H1');
			} else {
				/* 文件标题 */
				$objPHPExcel->getActiveSheet()
				->setCellValue('A1', ecs_iconv(EC_CHARSET, 'UTF8', $_REQUEST['start_date'] . '至' . $_REQUEST['end_date'] . ' 分類報告'))
				->setCellValue('A2', ecs_iconv(EC_CHARSET, 'UTF8', '分類'))
				->setCellValue('B2', ecs_iconv(EC_CHARSET, 'UTF8', '即時存貨總數'))
				->setCellValue('C2', ecs_iconv(EC_CHARSET, 'UTF8', '銷售總額(扣除折扣及優惠券)'))
				->setCellValue('D2', ecs_iconv(EC_CHARSET, 'UTF8', '盈利總額(扣除折扣及優惠券)'))
				->setCellValue('E2', ecs_iconv(EC_CHARSET, 'UTF8', '盈利率(扣除折扣及優惠券)'));
				$objPHPExcel->getActiveSheet()->mergeCells('A1:E1');
			}
			$i = 3;
			foreach ($stock_cost_data['stock_cost_data'] as $value)
			{
				if($_REQUEST['order_type'] == 4){
					$objPHPExcel->getActiveSheet()
					->setCellValue('A'.$i, ecs_iconv(EC_CHARSET, 'UTF8', ($i == 3 ? '' : '　') . $value['name']))
					->setCellValue('B'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['total_stocks']))
					->setCellValue('C'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['avg_stock_cost']))
					->setCellValue('D'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['turnover']))
					->setCellValue('E'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['total_cost']))
					->setCellValue('F'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['total_fifo_cost']))
					->setCellValue('G'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['total_dead_cost']))
					->setCellValue('H'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['total_sa_revenue_n_ds']))
					->setCellValue('I'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['total_sa_profit_n_ds']))
					->setCellValue('J'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['sa_profit_margin_n_ds_formated']));
					$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->getNumberFormat()->setFormatCode('$0.00');
					$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->getNumberFormat()->setFormatCode('$0.00');
					$objPHPExcel->getActiveSheet()->getStyle('F'.$i)->getNumberFormat()->setFormatCode('$0.00');
					$objPHPExcel->getActiveSheet()->getStyle('G'.$i)->getNumberFormat()->setFormatCode('$0.00');
					$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->getNumberFormat()->setFormatCode('$0.00');
					$objPHPExcel->getActiveSheet()->getStyle('I'.$i)->getNumberFormat()->setFormatCode('$0.00');
				}else if($_REQUEST['order_type'] == 3){
					$objPHPExcel->getActiveSheet()
					->setCellValue('A'.$i, ecs_iconv(EC_CHARSET, 'UTF8', ($i == 3 ? '' : '　') . $value['name']))
					->setCellValue('B'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['total_stocks']))
					->setCellValue('C'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['total_revenue_n_ds']))
					->setCellValue('D'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['total_sa_revenue_n_ds']))
					->setCellValue('E'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['total_profit_n_ds']))
					->setCellValue('F'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['total_sa_profit_n_ds']))
					->setCellValue('G'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['profit_margin_n_ds_formated']))
					->setCellValue('H'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['sa_profit_margin_n_ds_formated']));
					$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->getNumberFormat()->setFormatCode('$0.00');
					$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->getNumberFormat()->setFormatCode('$0.00');
					$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->getNumberFormat()->setFormatCode('$0.00');
					$objPHPExcel->getActiveSheet()->getStyle('F'.$i)->getNumberFormat()->setFormatCode('$0.00');
				} else {
					$objPHPExcel->getActiveSheet()
					->setCellValue('A'.$i, ecs_iconv(EC_CHARSET, 'UTF8', ($i == 3 ? '' : '　') . $value['name']))
					->setCellValue('B'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['total_stocks']))
					->setCellValue('C'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['total_revenue_n_ds']))
					->setCellValue('D'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['total_profit_n_ds']))
					->setCellValue('E'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['profit_margin_n_ds_formated']));
					$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->getNumberFormat()->setFormatCode('$0.00');
					$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->getNumberFormat()->setFormatCode('$0.00');
				}
				
				$i++;
				if (isset($value['children'])){
					foreach ($value['children'] as $child)
					{
						if($_REQUEST['order_type'] == 4){
							$objPHPExcel->getActiveSheet()
							->setCellValue('A'.$i, ecs_iconv(EC_CHARSET, 'UTF8', ($i == 3 ? '' : '　') . $child['name']))
							->setCellValue('B'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $child['total_stocks']))
							->setCellValue('C'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $child['avg_stock_cost']))
							->setCellValue('D'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $child['turnover']))
							->setCellValue('E'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $child['total_cost']))
							->setCellValue('F'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $child['total_fifo_cost']))
							->setCellValue('G'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $child['total_dead_cost']))
							->setCellValue('H'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $child['total_sa_revenue_n_ds']))
							->setCellValue('I'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $child['total_sa_profit_n_ds']))
							->setCellValue('J'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $child['sa_profit_margin_n_ds_formated']));
							$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->getNumberFormat()->setFormatCode('$0.00');
							$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->getNumberFormat()->setFormatCode('$0.00');
							$objPHPExcel->getActiveSheet()->getStyle('F'.$i)->getNumberFormat()->setFormatCode('$0.00');
							$objPHPExcel->getActiveSheet()->getStyle('G'.$i)->getNumberFormat()->setFormatCode('$0.00');
							$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->getNumberFormat()->setFormatCode('$0.00');
							$objPHPExcel->getActiveSheet()->getStyle('I'.$i)->getNumberFormat()->setFormatCode('$0.00');
						}else if($_REQUEST['order_type'] == 3){
							$objPHPExcel->getActiveSheet()
							->setCellValue('A'.$i, ecs_iconv(EC_CHARSET, 'UTF8', ($i == 3 ? '' : '　') . $child['name']))
							->setCellValue('B'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $child['total_stocks']))
							->setCellValue('C'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $child['total_revenue_n_ds']))
							->setCellValue('D'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $child['total_sa_revenue_n_ds']))
							->setCellValue('E'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $child['total_profit_n_ds']))
							->setCellValue('F'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $child['total_sa_profit_n_ds']))
							->setCellValue('G'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $child['profit_margin_n_ds_formated']))
							->setCellValue('H'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $child['sa_profit_margin_n_ds_formated']));
							$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->getNumberFormat()->setFormatCode('$0.00');
							$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->getNumberFormat()->setFormatCode('$0.00');
							$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->getNumberFormat()->setFormatCode('$0.00');
							$objPHPExcel->getActiveSheet()->getStyle('F'.$i)->getNumberFormat()->setFormatCode('$0.00');
						} else {
							$objPHPExcel->getActiveSheet()
							->setCellValue('A'.$i, ecs_iconv(EC_CHARSET, 'UTF8', ($i == 3 ? '' : '　') . $child['name']))
							->setCellValue('B'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $child['total_stocks']))
							->setCellValue('C'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $child['total_revenue_n_ds']))
							->setCellValue('D'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $child['total_profit_n_ds']))
							->setCellValue('E'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $child['profit_margin_n_ds_formated']));
							$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->getNumberFormat()->setFormatCode('$0.00');
							$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->getNumberFormat()->setFormatCode('$0.00');
						}
						$i++;
						if (isset($child['children'])){
							foreach ($child['children'] as $grandson)
							{
								if($_REQUEST['order_type'] == 4){
									$objPHPExcel->getActiveSheet()
									->setCellValue('A'.$i, ecs_iconv(EC_CHARSET, 'UTF8', ($i == 3 ? '' : '　') . $grandson['name']))
									->setCellValue('B'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $grandson['total_stocks']))
									->setCellValue('C'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $grandson['avg_stock_cost']))
									->setCellValue('D'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $grandson['turnover']))
									->setCellValue('E'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $grandson['total_cost']))
									->setCellValue('F'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $grandson['total_fifo_cost']))
									->setCellValue('G'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $grandson['total_dead_cost']))
									->setCellValue('H'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $grandson['total_sa_revenue_n_ds']))
									->setCellValue('I'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $grandson['total_sa_profit_n_ds']))
									->setCellValue('J'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $grandson['sa_profit_margin_n_ds_formated']));
									$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->getNumberFormat()->setFormatCode('$0.00');
									$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->getNumberFormat()->setFormatCode('$0.00');
									$objPHPExcel->getActiveSheet()->getStyle('F'.$i)->getNumberFormat()->setFormatCode('$0.00');
									$objPHPExcel->getActiveSheet()->getStyle('G'.$i)->getNumberFormat()->setFormatCode('$0.00');
									$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->getNumberFormat()->setFormatCode('$0.00');
									$objPHPExcel->getActiveSheet()->getStyle('I'.$i)->getNumberFormat()->setFormatCode('$0.00');
								}else if($_REQUEST['order_type'] == 3){
									$objPHPExcel->getActiveSheet()
									->setCellValue('A'.$i, ecs_iconv(EC_CHARSET, 'UTF8', ($i == 3 ? '' : '　') . $grandson['name']))
									->setCellValue('B'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $grandson['total_stocks']))
									->setCellValue('C'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $grandson['total_revenue_n_ds']))
									->setCellValue('D'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $grandson['total_sa_revenue_n_ds']))
									->setCellValue('E'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $grandson['total_profit_n_ds']))
									->setCellValue('F'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $grandson['total_sa_profit_n_ds']))
									->setCellValue('G'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $grandson['profit_margin_n_ds_formated']))
									->setCellValue('H'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $grandson['sa_profit_margin_n_ds_formated']));
									$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->getNumberFormat()->setFormatCode('$0.00');
									$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->getNumberFormat()->setFormatCode('$0.00');
									$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->getNumberFormat()->setFormatCode('$0.00');
									$objPHPExcel->getActiveSheet()->getStyle('F'.$i)->getNumberFormat()->setFormatCode('$0.00');
								} else {
									$objPHPExcel->getActiveSheet()
									->setCellValue('A'.$i, ecs_iconv(EC_CHARSET, 'UTF8', ($i == 3 ? '' : '　') . $grandson['name']))
									->setCellValue('B'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $grandson['total_stocks']))
									->setCellValue('C'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $grandson['total_revenue_n_ds']))
									->setCellValue('D'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $grandson['total_profit_n_ds']))
									->setCellValue('E'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $grandson['profit_margin_n_ds_formated']));
									$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->getNumberFormat()->setFormatCode('$0.00');
									$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->getNumberFormat()->setFormatCode('$0.00');
								}
								
								$i++;
							}
						}
					}
				}
			}
			for ($col = 'A'; $col <= 'J'; $col++)
			{
				$objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
			}
		}
		
		header('Content-type: application/vnd.ms-excel; charset=utf-8');
		header('Content-Disposition: attachment; filename=' . $file_name . '.xls');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		
		exit;
	}
	
	$stock_cost_data = $categoryController->get_stock_cost();
	$smarty->assign('cost_list',       $stock_cost_data['stock_cost_data']);
	$smarty->assign('total_discount',  $stock_cost_data['total_discount']);
	$smarty->assign('filter',          $stock_cost_data['filter']);
	$smarty->assign('record_count',    $stock_cost_data['record_count']);
	$smarty->assign('page_count',      $stock_cost_data['page_count']);
	$smarty->assign('start_date',      $_REQUEST['start_date']);
	$smarty->assign('end_date',        $_REQUEST['end_date']);
	$smarty->assign('display', $_REQUEST['order_type']);

	make_json_result($smarty->fetch('stock_cost.htm'), '', array('filter' => $stock_cost_data['filter'], 'page_count' => $stock_cost_data['page_count']));
}
else
{
	/* 权限判断 */
	admin_priv('sale_order_stats');
	
	/* 时间参数 */
	if (!isset($_REQUEST['start_date']))
	{
		$start_date = strtotime(month_start());
	}
	if (!isset($_REQUEST['end_date']))
	{
		$end_date = strtotime(month_end());
	}
	
	$stock_cost_data = $categoryController->get_stock_cost();
	/* 赋值到模板 */
	$smarty->assign('filter',         $stock_cost_data['filter']);
	$smarty->assign('order_type',     4);
	$smarty->assign('record_count',   $stock_cost_data['record_count']);
	$smarty->assign('page_count',     $stock_cost_data['page_count']);
	$smarty->assign('cost_list',      $stock_cost_data['stock_cost_data']);
	$smarty->assign('total_discount', $stock_cost_data['total_discount']);
	$smarty->assign('ur_here',        $_LANG['stock_cost']);
	$smarty->assign('full_page',      1);
	$smarty->assign('start_date',     local_date('Y-m-d', $start_date));
	$smarty->assign('end_date',       local_date('Y-m-d', $end_date));
	$smarty->assign('cfg_lang',       $_CFG['lang']);
	$smarty->assign('action_link',    array('text' => '下載Excel版本','href'=>'#download'));
	$smarty->assign('action_link2',    array('text' => '下載最近六個月Excel版本','href'=>'#six_months'));
	$smarty->assign('action_link3',    array('text' => '下載最近十二個月Excel版本','href'=>'#twelve_months'));
	$smarty->assign('display', $_REQUEST['order_type']);

	/* 显示页面 */
	assign_query_info();
	$smarty->display('stock_cost.htm');
}

function month_start()
{
	return local_date('Y-m-01');
}

function month_end()
{
	return local_date('Y-m-t');
}

function get_stock_cost_detail($is_pagination = true)
{
	global $erpController;
	$filter['cat_id'] = empty($_REQUEST['cat_id']) ? 0 : intval($_REQUEST['cat_id']);
	
	$where = " AND g.is_delete = '0' ".
			(empty($filter['cat_id']) ? '' :  " AND " . get_children($filter['cat_id']));
	
	$sql = 'SELECT COUNT(*) '.
		   'FROM ' . $GLOBALS['ecs']->table('goods') . ' as g ' .
		   'WHERE 1 ' . $where;
	$filter['record_count'] = $GLOBALS['db']->getOne($sql);
	
	/* 分页大小 */
	$filter = page_and_size($filter);
	
	$sql = 'SELECT g.goods_id, g.goods_name, sum(ega.goods_qty) as goods_number, g.cost, sum(ega.goods_qty) * g.cost as total_cost ' .
		   'FROM ' . $GLOBALS['ecs']->table('goods') . ' as g ' .
		   'LEFT JOIN ' . $GLOBALS['ecs']->table('erp_goods_attr_stock') . ' as ega ON ega.goods_id = g.goods_id AND ega.warehouse_id IN ('.$erpController->getSellableWarehouseIds(true).')' .
		   'WHERE 1 ' . $where .
		   'GROUP BY g.goods_id ' .
		   'ORDER BY g.goods_id DESC';
	
	if ($is_pagination)
	{
		$sql .= " LIMIT " . $filter['start'] . ', ' . $filter['page_size'];
	}
	
	$data = $GLOBALS['db']->getAll($sql);
	
	foreach ($data as $key => $item)
	{
		$data[$key]['cost'] = price_format($data[$key]['cost'], false);
		$data[$key]['total_cost'] = price_format($data[$key]['total_cost'], false);
	}
	$arr = array(
		'data' => $data,
		'filter' => $filter,
		'page_count' => $filter['page_count'],
		'record_count' => $filter['record_count']
	);
	return $arr;
}

?>
