<?php

/**
 * ECSHOP 管理中心优惠活动管理
 * ============================================================================
 * 版权所有 2005-2010 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liuhui $
 * $Id: favourable.php 17063 2010-03-25 06:35:46Z liuhui $
 */

define('IN_ECS', true);
require(dirname(__FILE__) . '/includes/init.php');
require(ROOT_PATH . 'includes/lib_goods.php');

$exc = new exchange($ecs->table('favourable_activity'), $db, 'act_id', 'act_name');
// define controller
$userController = new Yoho\cms\Controller\UserController();
$controller     = new Yoho\cms\Controller\YohoBaseController('favourable_activity');

/*------------------------------------------------------ */
//-- 活动列表页
/*------------------------------------------------------ */

//use one time, remove it after bulk update
if ($_REQUEST['act'] == 'bulk_update_fav_giftdata_to_new_structure')
{
    admin_priv('favourable');
    
    $sql = "SELECT act_id, act_kind, act_type_ext, gift FROM " . $ecs->table('favourable_activity') .
        " WHERE act_range = '" . FAR_GOODS .  "'";
    $favs = $db->getAll($sql);
    foreach($favs as $fav){
        $new_gift = array();
        $fav['gift'] = unserialize($fav['gift']);
        $act_type_ext = $fav['act_kind']==1?0:$fav['act_type_ext'];

        foreach($fav['gift'] as $gift){
            $new_gift[] = array('id' => $gift['id'], 'name' => '', 'price' => ($fav['act_kind']==1?0:$gift['price']), 'qty' => 1);
        }

        $sql = "UPDATE " . $ecs->table('favourable_activity') .
            " SET gift = '".serialize($new_gift)."'" .
            " , act_type_ext = '".$act_type_ext."'" .
            " WHERE act_id = '".$fav['act_id']."'";
        $db->query($sql);
    }
}

//use one time, remove it after bulk update
if ($_REQUEST['act'] == 'bulk_update_fav_related_cat')
{
    admin_priv('favourable');
    
    $sql = "SELECT act_id, act_range_ext FROM " . $ecs->table('favourable_activity') .
        " WHERE act_range = '" . FAR_GOODS .  "'";
    $favs = $db->getAll($sql);
    foreach($favs as $fav){
        $sql = "SELECT cat_id FROM " . $ecs->table('goods') .
        " WHERE goods_id " . db_create_in($fav['act_range_ext']) . 
        " GROUP BY cat_id";
        $related_category = $db->getCol($sql);
        $related_category = join(',', $related_category);

        $sql = "UPDATE " . $ecs->table('favourable_activity') .
            " SET related_category = '".$related_category."'" .
            " WHERE act_id = '".$fav['act_id']."'";
        $db->query($sql);
    }
}

if ($_REQUEST['act'] == 'list')
{
    admin_priv('favourable');

    /* 模板赋值 */
    $smarty->assign('ur_here',     $_LANG['favourable_list']);
    $smarty->assign('action_link', array('href' => 'favourable.php?act=add', 'text' => $_LANG['add_favourable']));
    /* 显示商品列表页面 */
    assign_query_info();
    $smarty->display('favourable_list.htm');
}

if ($_REQUEST['act'] == 'list_gift')
{
    admin_priv('favourable');

    $list = favourable_list("gift");
    //print_r($list);
    $smarty->assign('favourable_list', $list);

    /* 模板赋值 */
    $smarty->assign('ur_here',     $_LANG['gift_list']);
    $smarty->assign('action_link', array('href' => 'favourable.php?act=add_gift', 'text' => $_LANG['add_gift']));
    $smarty->assign('act', "list_gift");
    //$smarty->assign('query_action', "query_gift");
    /* 显示商品列表页面 */
    assign_query_info();
    $smarty->display('favourable_list.htm');
}

if ($_REQUEST['act'] == 'list_redeem')
{
    admin_priv('favourable');

    $list = favourable_list("redeem");
    //print_r($list);
    $smarty->assign('favourable_list', $list);

    /* 模板赋值 */
    $smarty->assign('ur_here',     $_LANG['redeem_list']);
    $smarty->assign('action_link', array('href' => 'favourable.php?act=add_redeem', 'text' => $_LANG['add_redeem']));
    $smarty->assign('act', "list_redeem");
    $smarty->assign('search_parm', $_REQUEST);
    //$smarty->assign('query_action', "query_redeem");

    $abnormal_favourable = get_abnormal_favourble_ids();
    $smarty->assign('abnormal_favourable_count', count($abnormal_favourable));

    /* 显示商品列表页面 */
    assign_query_info();
    $smarty->display('favourable_list.htm');
}
/*------------------------------------------------------ */
//-- 分页、排序、查询
/*------------------------------------------------------ */
/*
elseif ($_REQUEST['act'] == 'query')
{
    $list = favourable_list();

    $controller->ajaxQueryAction($list['item'], $list['record_count']);
}

elseif ($_REQUEST['act'] == 'query_gift')
{
    $list = favourable_list("gift");

    $controller->ajaxQueryAction($list['item'], $list['record_count']);
}

elseif ($_REQUEST['act'] == 'query_redeem')
{
    $list = favourable_list("redeem");

    $controller->ajaxQueryAction($list['item'], $list['record_count']);
}
*/

/*------------------------------------------------------ */
//-- 删除
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'remove')
{
    check_authz_json('favourable');

    $id = intval($_GET['id']);
    $favourable = favourable_info($id);
    if (empty($favourable))
    {
        make_json_error($_LANG['favourable_not_exist']);
    }
    $name = $favourable['act_name'];
    $exc->drop($id);

    /* 记日志 */
    admin_log($name, 'remove', 'favourable');

    /* 清除缓存 */
    clear_cache_files();

    $back_action = $favourable["act_kind"]==1?"list_gift":"list_redeem";

    $url = 'favourable.php?act='.$back_action;

    ecs_header("Location: $url\n");
    exit;
}

/*------------------------------------------------------ */
//-- 批量操作
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'batch')
{
    /* 取得要操作的记录编号 */
    if (empty($_POST['checkboxes']))
    {
        sys_msg($_LANG['no_record_selected']);
    }
    else
    {
        /* 检查权限 */
        admin_priv('favourable');

        $ids = $_POST['checkboxes'];

        if (isset($_POST['drop']))
        {
            /* 删除记录 */
            $sql = "DELETE FROM " . $ecs->table('favourable_activity') .
                    " WHERE act_id " . db_create_in($ids);
            $db->query($sql);

            /* 记日志 */
            admin_log('', 'batch_remove', 'favourable');

            /* 清除缓存 */
            clear_cache_files();

            $links[] = array('text' => $_LANG['back_favourable_list'], 'href' => 'favourable.php?act=list&' . list_link_postfix());
            sys_msg($_LANG['batch_drop_ok']);
        }
    }
}

/*------------------------------------------------------ */
//-- 修改排序
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'ajaxEdit')
{
    check_authz_json('favourable');

    $id  = intval($_POST['id']);
    $val = intval($_POST['value']);

    $sql = "UPDATE " . $ecs->table('favourable_activity') .
            " SET sort_order = '$val'" .
            " WHERE act_id = '$id' LIMIT 1";
    $db->query($sql);

    make_json_result($val);
}

/*------------------------------------------------------ */
//-- 添加、编辑
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'add_gift' || $_REQUEST['act'] == 'add_redeem' || $_REQUEST['act'] == 'edit')
{
    /* 检查权限 */
    admin_priv('favourable');

    /* 是否添加 */
    $is_add = $_REQUEST['act'] == 'add_gift' || $_REQUEST['act'] == 'add_redeem';
    $smarty->assign('form_action', $is_add ? 'insert' : 'update');

    /* 取得用户等级 */
    $sql = "SELECT rank_id, rank_name FROM " . $ecs->table('user_rank');
    $user_ranks = $db->getAll($sql);

    /* 初始化、取得优惠活动信息 */
    if ($is_add)
    {
        $favourable = array(
            'act_id'        => 0,
            'act_name'      => '',
            'start_time'    => date('Y-m-d', time() + 86400),
            'end_time'      => date('Y-m-d', time() + 8 * 86400),
            'min_amount'    => 0,
            'max_amount'    => 0,
            'min_quantity'  => 1,
            'act_type'      => FAT_GOODS,
            'act_type_ext'  => '1',
            'gift'          => array()
        );
    }
    else
    {
        if (empty($_GET['id']))
        {
            sys_msg('invalid param');
        }
        $id = intval($_GET['id']);
        $favourable = favourable_info($id);
        if (empty($favourable))
        {
            sys_msg($_LANG['favourable_not_exist']);
        }
    }

    $smarty->assign('favourable', $favourable);

    $user_rank_list = array();
    $user_rank_list[] = array(
        'rank_id'   => 0,
        'rank_name' => $_LANG['not_user'],
        'checked'   => strpos(',' . $favourable['user_rank'] . ',', ',0,') !== false
    );
    foreach ($user_ranks as $row)
    {
        $row['checked'] = strpos(',' . $favourable['user_rank'] . ',', ',' . $row['rank_id']. ',') !== false;
        $user_rank_list[] = $row;
    }
    $smarty->assign('user_rank_list', $user_rank_list);

    /* 取得优惠范围 */
    $act_range_ext = array();
    if ($favourable['act_range'] != FAR_ALL && !empty($favourable['act_range_ext']))
    {
        if ($favourable['act_range'] == FAR_CATEGORY)
        {
            $sql = "SELECT cat_id AS id, cat_name AS name FROM " . $ecs->table('category') .
                " WHERE cat_id " . db_create_in($favourable['act_range_ext']);
        }
        elseif ($favourable['act_range'] == FAR_BRAND)
        {
            $sql = "SELECT brand_id AS id, brand_name AS name FROM " . $ecs->table('brand') .
                " WHERE brand_id " . db_create_in($favourable['act_range_ext']);
        }
        else
        {
            $sql = "SELECT goods_id AS id, goods_name AS name FROM " . $ecs->table('goods') .
                " WHERE goods_id " . db_create_in($favourable['act_range_ext']);
        }
        $act_range_ext = $db->getAll($sql);

    }
    $smarty->assign('act_range_ext', $act_range_ext);

    /* 赋值时间控件的语言 */
    $smarty->assign('cfg_lang', $_CFG['lang']);    

    // Multiple language support
    $localizable_fields = localizable_fields_for_table('favourable_activity');
    $smarty->assign('localizable_fields', $localizable_fields);
    $smarty->assign('localized_versions', get_localized_versions('favourable_activity', $_REQUEST['id'], $localizable_fields));

    if($_REQUEST['act'] == 'add_gift' || ($_REQUEST['act'] == 'edit' && $favourable["act_type"] == 0 && $favourable["act_kind"] == 1)){
        $action_template = 'favourable_gift_info.htm';
        $ur_here = $is_add?$_LANG['add_gift']:$_LANG['edit_gift'];
        $href = 'favourable.php?act=list_gift';
        $text = $_LANG['gift_list'];
    }
    elseif($_REQUEST['act'] == 'add_redeem' || ($_REQUEST['act'] == 'edit' && $favourable["act_type"] == 0 && $favourable["act_kind"] == 2)){
        $action_template = 'favourable_redeem_info.htm';
        $ur_here = $is_add?$_LANG['add_redeem']:$_LANG['edit_redeem'];
        $href = 'favourable.php?act=list_redeem';
        $text = $_LANG['redeem_list'];
    }

    if (!$is_add)
    {
        $href .= '&' . list_link_postfix();
    }
    $smarty->assign('ur_here', $ur_here);
    $smarty->assign('action_link', array('href' => $href, 'text' => $text));
    assign_query_info();

    $smarty->display($action_template);
    //$smarty->display('favourable_info.htm');
}

/*------------------------------------------------------ */
//-- 添加、编辑后提交
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'insert' || $_REQUEST['act'] == 'update')
{
    //print_r($_POST);die();
    /* 检查权限 */
    admin_priv('favourable');

    /* 是否添加 */
    $is_add = $_REQUEST['act'] == 'insert';

    /* 检查名称是否重复 */
    $act_name = sub_str($_POST['act_name'], 255, false);
    if (!$exc->is_only('act_name', $act_name, intval($_POST['id'])))
    {
        sys_msg($_LANG['act_name_exists']);
    }

    /* 检查優惠活動展示標籤 */
    $act_tag = sub_str($_POST['act_tag'], 255, false);

    /* 检查享受优惠的会员等级 */
    /*
    if (!isset($_POST['user_rank']))
    {
        sys_msg($_LANG['pls_set_user_rank']);
    }
    */

    /* 检查优惠范围扩展信息 */
    if (intval($_POST['act_range']) > 0 && !isset($_POST['act_range_ext']))
    {
        sys_msg($_LANG['pls_set_act_range']);
    }

    /* 检查金额上下限 */
    $min_amount = floatval($_POST['min_amount']) >= 0 ? floatval($_POST['min_amount']) : 0;
    $max_amount = floatval($_POST['max_amount']) >= 0 ? floatval($_POST['max_amount']) : 0;
    if ($max_amount > 0 && $min_amount > $max_amount)
    {
        sys_msg($_LANG['amount_error']);
    }

    /* 检查至少購買數量 */
    $min_quantity = intval($_POST['min_quantity']) >= 1 ? intval($_POST['min_quantity']) : 1;

    /* */
    $act_kind = intval($_POST['act_kind'])?intval($_POST['act_kind']):1;
    $act_type = intval($_POST['act_type']);
    $act_type_ext = floatval($_POST['act_type_ext']);
    if ($act_type == FAT_GOODS){
        //if 贈品
        if($act_kind == 1){
            if(!isset($_POST['bulk_qty_check'])){
                $act_type_ext = 0;
            }
        }
        elseif($act_kind == 2){
            if(isset($_POST['bulk_price_check'])){
                $act_type_ext = 1;
                $bulk_price = floatval($_POST['bulk_price']) >= 0 ? floatval($_POST['bulk_price']) : 0;
            }
        }
    }

    /* 取得赠品 */
    $gift = array();
    if (intval($_POST['act_type']) == FAT_GOODS && isset($_POST['gift_id']))
    {
        foreach ($_POST['gift_id'] as $key => $id)
        {
            $gift[] = array('id' => $id, 'name' => '', 'price' => ($act_kind==1?0:($bulk_price?$bulk_price:$_POST['gift_price'][$key])), 'qty' => ($act_kind==2||$_POST['bulk_qty_check']?1:$_POST['gift_qty'][$key]));
        }
    }


    /* mark related category*/
    if ($_POST['act_range'] == FAR_GOODS){
        $sql = "SELECT cat_id FROM " . $ecs->table('goods') .
        " WHERE goods_id " . db_create_in($_POST['act_range_ext']) . 
        " GROUP BY cat_id";
        $related_category = $db->getCol($sql);
        $related_category = join(',', $related_category);
    }

    /* 提交值 */
    $start_time = date('Y-m-d H:i:s', strtotime($_POST['start_time']));
    $end_time = date('Y-m-d H:i:s', strtotime($_POST['end_time'] . "+ 86399 SECONDS"));

    $favourable = array(
        'act_id'        => intval($_POST['id']),
        'act_name'      => $act_name,
        'act_tag'       => $act_tag,
        'start_time'    => local_strtotime($start_time),
        'end_time'      => local_strtotime($end_time),
        'user_rank'     => isset($_POST['user_rank']) ? join(',', $_POST['user_rank']) : '0',
        'act_range'     => intval($_POST['act_range']),
        'act_range_ext' => intval($_POST['act_range']) == 0 ? '' : join(',', $_POST['act_range_ext']),
        'min_amount'    => $min_amount,
        'max_amount'    => $max_amount,
        'min_quantity'  => $min_quantity,
        'act_kind'      => $act_kind,
        'act_type'      => $act_type,
        'act_type_ext'  => $act_type_ext,
        'gift'          => serialize($gift),
        'users_list'    => empty($_POST['users_list']) ? '' : $_POST['users_list'],
        'bulk_price'    => empty($bulk_price) ? '' : $bulk_price,
        'related_category' => empty($related_category) ? '' : $related_category,
    );
    if ($favourable['act_type'] == FAT_GOODS)
    {
        $favourable['act_type_ext'] = round($favourable['act_type_ext']);
    }

    // handle simplified chinese
    $localizable_fields = [
        "act_name" => $act_name,
        "act_tag"  => $act_tag,
    ];
    if (!$is_add) {
        $localizable_fields = $controller->checkRequireSimplied("favourable_activity", $localizable_fields, $_POST['id']);
    }
    to_simplified_chinese($localizable_fields);

    /* 保存数据 */
    if ($is_add)
    {
        $db->autoExecute($ecs->table('favourable_activity'), $favourable, 'INSERT');
        $favourable['act_id'] = $db->insert_id();
    }
    else
    {
        $db->autoExecute($ecs->table('favourable_activity'), $favourable, 'UPDATE', "act_id = '$favourable[act_id]'");
    }

    // Multiple language support
    save_localized_versions('favourable_activity', $favourable['act_id']);

    /* 记日志 */
    if ($is_add)
    {
        admin_log($favourable['act_name'], 'add', 'favourable');
    }
    else
    {
        admin_log($favourable['act_name'], 'edit', 'favourable');
    }

    /* 清除缓存 */
    clear_cache_files();

    /* 提示信息 */
    if ($is_add)
    {
        if($act_kind == 1){
            $links = array(
                array('href' => 'favourable.php?act=add_gift', 'text' => $_LANG['continue_add_gift']),
                array('href' => 'favourable.php?act=list_gift', 'text' => $_LANG['back_gift_list'])
            );

            sys_msg($_LANG['add_gift_ok'], 0, $links);
        }
        elseif($act_kind == 2){
            $links = array(
                array('href' => 'favourable.php?act=add_redeem', 'text' => $_LANG['continue_add_redeem']),
                array('href' => 'favourable.php?act=list_redeem', 'text' => $_LANG['back_redeem_list'])
            );

            sys_msg($_LANG['add_redeem_ok'], 0, $links);
        }
    }
    else
    {
        if($act_kind == 1){
            $links = array(
                array('href' => 'favourable.php?act=list_gift&' . list_link_postfix(), 'text' => $_LANG['back_gift_list'])
            );
            sys_msg($_LANG['edit_gift_ok'], 0, $links);
        }
        elseif($act_kind == 2){
            $links = array(
                array('href' => 'favourable.php?act=list_redeem&' . list_link_postfix(), 'text' => $_LANG['back_redeem_list'])
            );

            sys_msg($_LANG['add_redeem_ok'], 0, $links);
        }
    }
}

/*------------------------------------------------------ */
//-- 搜索商品
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'search')
{
    /* 检查权限 */
    check_authz_json('favourable');

    include_once(ROOT_PATH . 'includes/cls_json.php');

    $json   = new JSON;
    $filter = $json->decode($_GET['JSON']);
    $filter->keyword = json_str_iconv($filter->keyword);
    if ($filter->act_range == FAR_ALL)
    {
        $arr[0] = array(
            'id'   => 0,
            'name' => $_LANG['js_languages']['all_need_not_search']
        );
    }
    elseif ($filter->act_range == FAR_CATEGORY)
    {
        $sql = "SELECT cat_id AS id, cat_name AS name FROM " . $ecs->table('category') .
            " WHERE cat_name LIKE '%" . mysql_like_quote($filter->keyword) . "%' LIMIT 50";
        $arr = $db->getAll($sql);
    }
    elseif ($filter->act_range == FAR_BRAND)
    {
        $sql = "SELECT brand_id AS id, brand_name AS name FROM " . $ecs->table('brand') .
            " WHERE brand_name LIKE '%" . mysql_like_quote($filter->keyword) . "%' LIMIT 50";
        $arr = $db->getAll($sql);
    }
    else
    {
        if(strpos($filter->keyword, ",") > 0){
            $include_goods = explode(",", $filter->keyword);
            
            $include_goods_like_str = array_map(function ($goods_name) { return "goods_name like '%".trim($goods_name)."%'"; }, $include_goods);
            $include_goods_sn_like_str = array_map(function ($goods_sn) { return "goods_sn like '%".trim($goods_sn)."%'"; }, $include_goods);
            $include_goods_id_str = array_map(function ($goods_id) { return "goods_id = '".trim($goods_id)."'"; }, $include_goods);

            $sql = "SELECT goods_id AS id, goods_name AS name, shop_price FROM " . $ecs->table('goods') .
                " WHERE ".implode(" OR ", $include_goods_like_str)." OR ".implode(" OR ", $include_goods_sn_like_str)." OR ".implode(" OR ", $include_goods_id_str)." LIMIT 50";
        }
        else{
            $include_goods_where .= " AND (goods_sn LIKE '%" . mysql_like_quote($filter->keyword) . "%' ";
            $include_goods_where .= " OR goods_name LIKE '%" . mysql_like_quote($filter->keyword) . "%' ";
            $tmp = preg_split('/\s+/', $filter->keyword);
            if (count($tmp) > 1)
            {
                $include_goods_where .= " OR ( 1";
                foreach ($tmp as $kw)
                {
                    $include_goods_where .= " AND goods_name LIKE '%" . mysql_like_quote($kw) . "%' ";
                }
                $include_goods_where .= ") ";
            }
            $include_goods_where .= " OR goods_id = '" . mysql_like_quote($filter->keyword) . "')";

            $sql = "SELECT goods_id AS id, goods_name AS name, shop_price FROM " . $ecs->table('goods') .
                " WHERE 1 ".$include_goods_where." LIMIT 50";
        }
        $arr = $db->getAll($sql);
    }
    if (empty($arr))
    {
        $arr = array(0 => array(
            'id'   => 0,
            'name' => $_LANG['search_result_empty']
        ));
    }

    make_json_result($arr);
}
elseif($_REQUEST['act'] == 'search_user')
{
    $type       = $_REQUEST['type'];
    $value      = $_REQUEST['value'];
    $users_list = $_REQUEST['users_list'];
    $users_list = $userController->findUserByType($type, $value, $users_list);
    echo json_encode($users_list);
}
elseif($_REQUEST['act'] == 'search_range')
{
    $act_id = $_REQUEST['act_id'];
    $act_kind      = $_REQUEST['act_kind'];
    $act_range       = $_REQUEST['act_range'];
    $act_range_ext      = $_REQUEST['act_range_ext'];

    $now = gmtime();

    $sql = "SELECT act_id, act_name FROM " . $ecs->table('favourable_activity') .
        " WHERE act_range = '" . $act_range . "'" .
        " AND CONCAT(',', act_range_ext, ',') LIKE '%," . mysql_like_quote($act_range_ext) . ",%'" .
        " AND ((start_time <= '$now' AND end_time) >= '$now' OR end_time >= '$now')".
        " AND act_kind = '" . $act_kind . "'".
        " AND act_id != '" . $act_id . "' LIMIT 50";
    $arr = $db->getAll($sql);
    
    echo json_encode($arr);
}
elseif ($_REQUEST['act'] == 'ajax_update_status')
{
    admin_priv('favourable_on_off');
    //admin_priv('menu_flashdeal_events');
    if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0){
        $favourable_id = $_REQUEST['id'];
    }
    if (isset($_REQUEST['status']) && $_REQUEST['status'] != ''){
        if ($_REQUEST['status'] == 'true'){
            $new_status = 1;
        } elseif ($_REQUEST['status'] == 'false'){
            $new_status = 0;
        }
    }

    updateStatusById($favourable_id, $new_status);

    $current_favourable = favourable_info($favourable_id);
    if ($current_favourable){

        $current_favourable_status = (string)$current_favourable['status'];

        $str_now = date('Y-m-d H:i:s');
        $datetime_now = DateTime::createFromFormat($format, $str_now);
        $datetime_start = DateTime::createFromFormat($format, $current_favourable['start_time']);
        $datetime_end = DateTime::createFromFormat($format, $current_favourable['end_time']);

        if ($datetime_start < $datetime_now && $datetime_end > $datetime_now){
            if ($current_favourable['status'] == 1){
                $current_favourable_status_on_off = '1';
            } else {
                $current_favourable_status_on_off = '0';
            }
        } elseif ($datetime_start > $datetime_now) {
            if ($current_favourable['status'] == 1){
                $current_favourable_status_on_off = '2';
            } else {
                $current_favourable_status_on_off = '0';
            }
        } else {
            $current_favourable_status_on_off = '0';
        }

        return make_json_result(array('status' => $current_favourable_status, 'on_off' => $current_favourable_status_on_off));
    }
    
}

/*
 * 取得优惠活动列表
 * @return   array
 */
function favourable_list($kind = null)
{
    /* 过滤条件 */
    $_REQUEST['keyword']    = empty($_REQUEST['keyword']) ? '' : trim($_REQUEST['keyword']);
    if (isset($_REQUEST['is_ajax']) && $_REQUEST['is_ajax'] == 1)
    {
        $_REQUEST['keyword'] = json_str_iconv($_REQUEST['keyword']);
    }
    $_REQUEST['is_going']   = empty($_REQUEST['is_going']) ? 0 : 1;
    $_REQUEST['sort_by']    = empty($_REQUEST['sort_by']) ? 'act_id' : trim($_REQUEST['sort_by']);
    $_REQUEST['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
    $_REQUEST['start']      = empty($_REQUEST['start']) ? 0 : intval($_REQUEST['start']);
    $_REQUEST['page_size']  = empty($_REQUEST['page_size']) ? 50 : intval($_REQUEST['page_size']);
    $now = gmtime();
    //die($kind);

    if($kind == "redeem"){
        
        $price_abnormal_favourable = get_abnormal_favourble_ids();
        //print_r($price_abnormal_favourable);

    }

    $where = "";
    if (!empty($_REQUEST['keyword']))
    {
        $where .= " AND act_name LIKE '%" . mysql_like_quote($_REQUEST['keyword']) . "%'";
    }
    if ($_REQUEST['is_going'])
    {
        $where .= " AND start_time <= '$now' AND end_time >= '$now' ";
    }
    //check goods in the favourable event
    if ($_REQUEST['include_goods'])
    {
        $include_goods = explode(",", $_REQUEST['include_goods']);
        
        $include_goods_like_str = array_map(function ($goods_name) { return "goods_name like '%".trim(mysql_like_quote($goods_name))."%'"; }, $include_goods);
        $include_goods_id_str = array_map(function ($goods_id) { return "goods_id = '".trim(mysql_like_quote($goods_id))."'"; }, $include_goods);

        $sql = "SELECT goods_id ".
        "FROM " . $GLOBALS['ecs']->table('goods') . 
        " WHERE 1=1 and (".implode(" OR ", $include_goods_like_str)." OR ".implode(" OR ", $include_goods_id_str).")";
        $related_goods = $GLOBALS['db']->getAll($sql);
        $related_goods_id = array_map(function ($goods) { return $goods['goods_id']; }, $related_goods);

        //print_r($related_goods_id);
        if(!empty($related_goods_id)){
            $search_str = implode("|", $related_goods_id);
            $where .= " AND CONCAT(',', act_range_ext, ',') REGEXP '".$search_str."'";
        }
        else{
            $where .= " AND 1!=1";
        }
    }

    if ($_REQUEST['price_abnormal'])
    {
        $where .= " AND act_id " . db_create_in($price_abnormal_favourable);
    }

    /* Get Different kind of favourable*/
    if($kind == "gift"){
        $where .= " AND act_type = 0 AND act_kind = 1 ";
    }
    else if($kind == "redeem"){
        $where .= " AND act_type = 0 AND act_kind = 2 ";
    }

    $sql = "SELECT COUNT(*) FROM " . $GLOBALS['ecs']->table('favourable_activity') .
            " WHERE 1 $where";
    //die($sql);
    $record_count = $GLOBALS['db']->getOne($sql);

    /* 查询 */
    $sql = "SELECT * ".
            "FROM " . $GLOBALS['ecs']->table('favourable_activity') .
            " WHERE 1 $where ".
            " ORDER BY $_REQUEST[sort_by] $_REQUEST[sort_order] ".
            " LIMIT ". $_REQUEST['start'] .", $_REQUEST[page_size]";
    //die($sql);

    $_REQUEST['keyword'] = stripslashes($_REQUEST['keyword']);

    $res = $GLOBALS['db']->query($sql);

    $list = array();
    $now = date('Y-m-d H:i');
    while ($row = $GLOBALS['db']->fetchRow($res))
    {
        $row['start_time']  = local_date('Y-m-d H:i', $row['start_time']);
        $row['end_time']    = local_date('Y-m-d H:i', $row['end_time']);
        $row['price_abnormal']    = in_array($row['act_id'], $price_abnormal_favourable);
        if($row['start_time'] > $now || $row['end_time'] < $now){
            $row['is_not_on_going'] = true;
        }

        $list[] = $row;
    }

    return $list;
    //return array('item' => $list, 'record_count' => $record_count);
}

function get_abnormal_favourble_ids(){
    $sql = "SELECT act_id, gift ".
        "FROM " . $GLOBALS['ecs']->table('favourable_activity') . 
        " WHERE act_type = 0 AND act_kind = 2 ";
    $res = $GLOBALS['db']->query($sql);
    $redeem_favourable = array();
    $all_redeem = array();
    while ($row = $GLOBALS['db']->fetchRow($res)){
        $redeems = unserialize($row['gift']);
        foreach($redeems as $redeem){
            $info = array();
            $info['goods_id'] = $redeem['id'];
            $info['price'] = $redeem['price'];

            $all_redeem_ids[] = $redeem['id'];;

            $redeem_favourable[$row['act_id']][] = $info;
        }
    }
    
    //print_r($all_redeem_ids);
    //print_r($redeem_favourable);

    $sql = "SELECT goods_id, shop_price ".
        "FROM " . $GLOBALS['ecs']->table('goods') . 
        " WHERE goods_id " . db_create_in($all_redeem_ids);
    $res = $GLOBALS['db']->query($sql);
    $redeem_price = array();
    while ($row = $GLOBALS['db']->fetchRow($res)){
        $redeem_price[$row["goods_id"]] = $row["shop_price"];
    }
    //print_r($redeem_favourable);

    $price_abnormal_favourable = array();
    foreach ($redeem_favourable as $key => $favourable_goods) {
        foreach ($favourable_goods as $good) {
            //echo ($good["price"] ."|". $redeem_price[$good["goods_id"]]);
            //var_dump($good["price"] > floatval($redeem_price[$good["id"]]));
            //var_dump($good["price"] - $redeem_price[$good["goods_id"]] > 0);
            //die();
            if($good["price"] - $redeem_price[$good["goods_id"]] > 0){
                //echo $good["price"]."|".$redeem_price[$good["goods_id"]]."|".$good["goods_id"]."?";
                $price_abnormal_favourable[] = $key;
                break;
            }
        }
    }

    return $price_abnormal_favourable;

}

function updateStatusById($id, $status){
    $sql = "UPDATE ".$GLOBALS['ecs']->table('favourable_activity')." SET status = '$status' WHERE act_id = '$id'";
    $res = $GLOBALS['db']->query($sql);
}

?>