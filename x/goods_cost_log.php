<?php

/***
* goods_cost_log.php
* by howang 2014-10-09
*
* Display the log of goods cost changes
***/

define('IN_ECS', true);
require(dirname(__FILE__) . '/includes/init.php');
$goodsController  = new Yoho\cms\Controller\GoodsController();

if ($_REQUEST['act'] == 'list')
{
    /* 检查权限 */
    admin_priv('goods_manage');

    if (!$_SESSION['manage_cost'])
    {
        die('Admin only!');
    }

    $smarty->assign('ur_here',      $_LANG['goods_cost_log']);

    assign_query_info();
    $smarty->display('goods_cost_log.htm');
}
elseif ($_REQUEST['act'] == 'query')
{
    $goodsController->goodsCostLogAjaxQueryAction();
}
?>