<?php

/***
* homepage_carousel.php
* by howang 2014-11-14
*
* Manage the carousel on home page
***/

define('IN_ECS', true);
require(dirname(__FILE__) . '/includes/init.php');
require_once ROOT_PATH . ADMIN_PATH . '/includes/lib_country_data.php';
require_once ROOT_PATH  . ADMIN_PATH . '/includes/lib_image.php';

$controller = new Yoho\cms\Controller\YohoBaseController('homepage_carousel');
if ($_REQUEST['act'] == 'list')
{
    $smarty->assign('ur_here',      $_LANG['homepage_carousel']);
    $smarty->assign('action_link', array('text' => '新增廣告', 'href' => 'homepage_carousel.php?act=add'));

    assign_query_info();
    $smarty->display('homepage_carousel_list.htm');
}
elseif ($_REQUEST['act'] == 'query')
{
    $list = get_carousels();
    $controller->ajaxQueryAction($list['data'], $list['record_count']);

}
elseif (in_array($_REQUEST['act'], array('add','info','edit')))
{
    admin_priv('ad_manage');
    
    if ($_REQUEST['act'] != 'add')
    {
        $rec_id = empty($_REQUEST['id']) ? 0 : intval($_REQUEST['id']);
        $carousel = get_carousel_info($rec_id);
        
        if (!$carousel)
        {
            sys_msg('找不到該廣告！');
        }
    }
    else
    {
        $max_sort_order = $db->getOne("SELECT max(sort_order) FROM " . $ecs->table('homepage_carousel'));
        
        $carousel = array(
            'title' => '',
            'url' => '',
            'main_img' => '',
            'bg_img' => '',
            'area' => array(),
            'area_exclude' => array(),
            'language' => array(),
            'language_exclude' => array(),
            'start_date' => local_date('Y-m-01 00:00:00'),
            'end_date' => local_date('Y-m-t 23:59:59'),
            'sort_order' => ($max_sort_order + 1)
        );
    }
    $smarty->assign('carousel',       $carousel);
    
    $smarty->assign('ur_here',      $_LANG['homepage_carousel']);
    
    $smarty->assign('action_link', array('text' => '返回列表', 'href' => 'homepage_carousel.php?act=list'));
    
    if ($_REQUEST['act'] == 'info')
    {
        $smarty->assign('action_link2', array('text' => '編輯廣告', 'href' => 'homepage_carousel.php?act=edit&id=' . $rec_id));
    }
    elseif ($_REQUEST['act'] == 'edit')
    {
        $smarty->assign('action_link2', array('text' => '查看廣告', 'href' => 'homepage_carousel.php?act=info&id=' . $rec_id));
    }
    
    $smarty->assign('act',          $_REQUEST['act']);
    
    $smarty->assign('cfg_lang',     $_CFG['lang']);
    
    $country_data = get_country_data();
    $smarty->assign('country_list', $country_data['country_list']);
    $smarty->assign('country_names', $country_data['country_names']);
    
    $smarty->assign('language_list', available_language_names());
    
    assign_query_info();
    $smarty->display('homepage_carousel_info.htm');
}
elseif (($_REQUEST['act'] == 'insert') || ($_REQUEST['act'] == 'update'))
{
    admin_priv('ad_manage');
    
    if ($_REQUEST['act'] == 'update')
    {
        $rec_id = empty($_REQUEST['rec_id']) ? 0 : intval($_REQUEST['rec_id']);
        
        if (!$rec_id)
        {
            sys_msg('找不到該廣告！');
        }
    }
    else
    {
        $rec_id = $db->getOne("SELECT max(rec_id)+1 FROM " . $ecs->table('homepage_carousel'));
    }
    
    $carousel = array();
    
    $carousel['title'] = empty($_POST['title']) ? '' : trim($_POST['title']);
    if (empty($carousel['title']))
    {
        sys_msg('必須輸入標題');
    }
    
    $carousel['url'] = empty($_POST['url']) ? '' : trim($_POST['url']);
    if (empty($carousel['url']))
    {
        sys_msg('必須輸入鏈結網址');
    }
    
    $main_img_size = ($_FILES['main_img']['size'] > 0) ? getimagesize($_FILES['main_img']['tmp_name']) : array('mime' => '');
    $main_img_ext = ($main_img_size['mime'] == 'image/png') ? '.png' : '.jpg';
    $res = handle_upload_image('main_img', 'carousel_' . $rec_id, '800x360', $main_img_ext);
    if (($_REQUEST['act'] == 'insert') && (empty($res['rel_path'])))
    {
        sys_msg('必須上傳主圖');
    }
    if (!empty($res['rel_path']))
    {
        $carousel['main_img'] = $res['rel_path'];
    }
    
    $bg_img_size = ($_FILES['bg_img']['size'] > 0) ? getimagesize($_FILES['bg_img']['tmp_name']) : array('mime' => '');
    $bg_img_ext = ($bg_img_size['mime'] == 'image/png') ? '.png' : '.jpg';
    $res = handle_upload_image('bg_img', 'carousel_bg_' . $rec_id, '1x1', $bg_img_ext);
    if (($_REQUEST['act'] == 'insert') && (empty($res['rel_path'])))
    {
        sys_msg('必須上傳背景圖');
    }
    if (!empty($res['rel_path']))
    {
        $carousel['bg_img'] = $res['rel_path'];
    }
    
    $start_date = empty($_POST['start_date']) ? '' : trim($_POST['start_date']);
    if (empty($start_date))
    {
        sys_msg('必須輸入開始時間');
    }
    $carousel['start_time'] = local_strtotime($start_date);
    
    $end_date = empty($_POST['end_date']) ? '' : trim($_POST['end_date']);
    if (empty($end_date))
    {
        sys_msg('必須輸入結束時間');
    }
    $carousel['end_time'] = local_strtotime($end_date);
    
    $carousel['sort_order'] = empty($_POST['sort_order']) ? 0 : intval($_POST['sort_order']);
    
    $carousel['area'] = (!empty($_POST['area']) && is_array($_POST['area'])) ? implode(',', array_filter(array_map('trim', $_POST['area']))) : '';
    $carousel['area_exclude'] = (!empty($_POST['area_exclude']) && is_array($_POST['area_exclude'])) ? implode(',', array_filter(array_map('trim', $_POST['area_exclude']))) : '';
    $carousel['language'] = (!empty($_POST['language']) && is_array($_POST['language'])) ? implode(',', array_filter(array_map('trim', $_POST['language']))) : '';
    $carousel['language_exclude'] = (!empty($_POST['language_exclude']) && is_array($_POST['language_exclude'])) ? implode(',', array_filter(array_map('trim', $_POST['language_exclude']))) : '';
    
    if ($_REQUEST['act'] == 'update')
    {
        $db->autoExecute($ecs->table('homepage_carousel'), $carousel, 'UPDATE', " `rec_id` = '" . $rec_id . "'");
        
        $act_name = '修改';
    }
    else
    {
        $db->autoExecute($ecs->table('homepage_carousel'), $carousel, 'INSERT');
        $rec_id = $db->insert_id();
        
        $act_name = '新增';
    }
    
    clear_cache_files();
    
    $link[0]['text'] = '查看廣告';
    $link[0]['href'] = 'homepage_carousel.php?act=info&id=' . $rec_id;
    
    $link[1]['text'] = '返回列表';
    $link[1]['href'] = 'homepage_carousel.php?act=list';
    
    sys_msg($act_name . '廣告成功', 0, $link, true);
}
elseif ($_REQUEST['act'] == 'edit_sort_order')
{
    check_authz_json('ad_manage');
    
    $rec_id = intval($_POST['id']);
    $value = intval($_POST['val']);
    
    $db->query("UPDATE " . $ecs->table('homepage_carousel') . " SET `sort_order` = '" . $value . "' WHERE `rec_id` = '" . $rec_id . "'");
    
    clear_cache_files();
    
    make_json_result($value);
}
elseif ($_REQUEST['act'] == 'remove')
{
    check_authz_json('ad_manage');
    
    $rec_id = intval($_REQUEST['id']);
    
    $sql = "DELETE FROM " . $ecs->table('homepage_carousel') .
            " WHERE `rec_id` = '" . $rec_id . "' ";
    $db->query($sql);
    
    $url = 'homepage_carousel.php?act=query&' . str_replace('act=' . $_REQUEST['act'], '', $_SERVER['QUERY_STRING']);
    
    header('Location: ' . $url);
    exit;
}

function handle_upload_image($field_name, $save_name = '', $resize = '', $file_ext = '.jpg')
{
    $name = '';
    $rel_path = '';
    $save_path = '';
    $url = '';

    if (($_FILES[$field_name]['name']) && ($_FILES[$field_name]['size'] > 0))
    {
        $name = $save_name;
        $rel_path = DATA_DIR . '/afficheimg/' . $name;
        $save_path = ROOT_PATH . $rel_path;
        
        while (empty($name) || file_exists($save_path . $file_ext))
        {
            $name = $save_name . date('Ymd');
            for ($i = 0; $i < 6; $i++)
            {
                $name .= chr(mt_rand(97, 122));
            }
            $rel_path = DATA_DIR . '/afficheimg/' . $name;
            $save_path = ROOT_PATH . $rel_path;
        }
        
        $tmp_name = $_FILES[$field_name]['tmp_name'];     
        $size = getimagesize($tmp_name);
        $width = $size[0];
        $height = $size[1];
        $type = $size['mime'];
        
        if ($width > 2048 || $height > 2048)
        {
            sys_msg('圖片尺寸過大，請先把圖片縮小至 1024x1024 以內。');
        }
        
        if ($type == 'image/jpeg')
        {
            $tmp_path = $save_path . '_tmp.jpg';
        }
        else if ($type == 'image/gif')
        {
            $tmp_path = $save_path . '_tmp.gif';
        }
        else if ($type == 'image/png')
        {
            $tmp_path = $save_path . '_tmp.png';
        }
        else
        {
            sys_msg('圖片格式錯誤，請確認圖片格式是 jpg, gif, png');
        }

        if (move_uploaded_file($tmp_name, $tmp_path))
        {
            // Process the image, convert to jpg, and optionally resize it
            $cmd = '/usr/bin/convert ' . escapeshellarg($tmp_path) . ' ';
            if (!empty($resize))
            {
                $cmd .= '-resize ' . $resize . '^ -gravity center -extent ' . $resize . ' ';
            }

            $cmd .= escapeshellarg($save_path . $file_ext);
            exec($cmd);
            image_compression($save_path . $file_ext);            
            unlink($tmp_path);
            $name .= $file_ext;
            $rel_path .= $file_ext;
            $save_path .= $file_ext;
            $url = $GLOBALS['ecs']->url() . $rel_path;
            $rel_path = '/' . $rel_path;
        }
        else
        {
            $name = '';
            $rel_path = '';
            $save_path = '';
        }
    }
    
    return compact('name', 'rel_path', 'save_path', 'url');
}

function get_carousels()
{
    $_REQUEST['sort_by']      = empty($_REQUEST['sort_by']) ? 'sort_order' : trim($_REQUEST['sort_by']);
    $_REQUEST['sort_order']   = empty($_REQUEST['sort_order']) ? 'ASC' : trim($_REQUEST['sort_order']);

    $sql = "SELECT count(*) FROM " . $GLOBALS['ecs']->table('homepage_carousel');
    $_REQUEST['record_count'] = $GLOBALS['db']->getOne($sql);

    /* 查询 */
    $sql = "SELECT * ";
    $sql.= " FROM " . $GLOBALS['ecs']->table('homepage_carousel');
    $sql.= " ORDER BY " . $_REQUEST['sort_by'] . ' ' . $_REQUEST['sort_order'];
    $sql.= " LIMIT " . $_REQUEST['start'] . "," . $_REQUEST['page_size'];
    $data = $GLOBALS['db']->getAll($sql);
    
    foreach ($data as $key => $row)
    {
        $data[$key]['start_date'] = local_date($GLOBALS['_CFG']['time_format'], $row['start_time']);
        $data[$key]['end_date'] = local_date($GLOBALS['_CFG']['time_format'], $row['end_time']);
        $data[$key]['active'] = ((gmtime() >= $row['start_time']) && (gmtime() <= $row['end_time']));
    }

    $arr = array('data' => $data, 'record_count' => $_REQUEST['record_count']);

    return $arr;
}

function get_carousel_info($rec_id)
{
    $sql = "SELECT * FROM " . $GLOBALS['ecs']->table('homepage_carousel') . " WHERE `rec_id` = " . intval($rec_id) . " LIMIT 1";
    $carousel = $GLOBALS['db']->getRow($sql);
    
    $carousel['start_date'] = local_date($GLOBALS['_CFG']['time_format'], $carousel['start_time']);
    $carousel['end_date'] = local_date($GLOBALS['_CFG']['time_format'], $carousel['end_time']);
    $carousel['active'] = ((gmtime() >= $carousel['start_time']) && (gmtime() <= $carousel['end_time']));
    $carousel['area'] = array_filter(explode(',', $carousel['area']));
    $carousel['area_exclude'] = array_filter(explode(',', $carousel['area_exclude']));
    $carousel['language'] = array_filter(explode(',', $carousel['language']));
    $carousel['language_exclude'] = array_filter(explode(',', $carousel['language_exclude']));
    
    return $carousel;
}

?>