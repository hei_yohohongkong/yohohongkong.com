<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');

if($_REQUEST['act'] == 'list')
{
	if(admin_priv('erp_warehouse_view','',false) ||admin_priv('erp_warehouse_manage','',false) ||admin_priv('erp_warehouse_approve','',false))
	{
		$sql = "SELECT a.goods_id,g.goods_sn, g.goods_name,c.stock as recoreded_stock,b.qty as calculated_stock,ega.goods_qty as attr_stock,egas.goods_qty as attr_stock_stock FROM ".$ecs->table('erp_stock')." a ";
		$sql.="LEFT JOIN (SELECT goods_id,max(stock_id) as stock_id,sum(qty) as qty FROM ecs_erp_stock GROUP BY goods_id) b ON b.goods_id=a.goods_id AND b.stock_id=a.stock_id ".
            "LEFT JOIN (SELECT SUM(stock)as stock, goods_id FROM ".$ecs->table('erp_stock')." where stock_id IN(SELECT MAX(`stock_id`) AS stock_id FROM ".$ecs->table('erp_stock')." GROUP BY `goods_id`,`warehouse_id`) group by goods_id) as c on c.goods_id = a.goods_id ".
            "LEFT JOIN ecs_goods g ON g.goods_id=a.goods_id ".
			"LEFT JOIN (SELECT SUM(goods_qty) as goods_qty, goods_id FROM ecs_erp_goods_attr GROUP BY goods_id) ega ON ega.goods_id=g.goods_id ".
			"LEFT JOIN (SELECT SUM(goods_qty) as goods_qty, goods_id FROM ecs_erp_goods_attr_stock GROUP BY goods_id) egas ON egas.goods_id=g.goods_id ".
			"WHERE ( c.stock<>b.qty OR b.qty<>ega.goods_qty ) AND g.is_on_sale = 1 AND g.is_alone_sale = 1 AND g.is_delete = 0 ".
			"ORDER BY goods_id DESC";
		$stock_list = $db->getAll($sql);
        assign_query_info();
		$smarty->assign('ur_here','貨品庫存異常');
		$smarty->assign('stock_list',$stock_list);
		$smarty->display('erp_stock_abnormal.htm');
	}
}elseif($_REQUEST['act']=='detail'){
	$goods_id = intval($_REQUEST['goods_id']);
	$timeInterval = 100;
	$sql="SELECT a.stock_id,a.goods_id,g.goods_sn,b.stock_id AS bsid,a.qty, FROM_UNIXTIME(a.act_time) AS act_time,FROM_UNIXTIME(b.act_time) AS bacttime,b.act_date as date_from,a.act_date+ INTERVAL 1 DAY as date_to, (CAST(a.act_time AS signed)-cast(b.act_time AS signed)) AS time_diff,a.stock,a.order_sn FROM ecs_erp_stock a ".
	" INNER JOIN ecs_erp_stock b ON a.goods_id=b.goods_id AND a.stock_style=b.stock_style AND a.order_sn=b.order_sn AND a.stock_id<>b.stock_id AND a.stock=b.stock AND a.qty=b.qty ".
	" LEFT JOIN ecs_goods g ON g.goods_id = a.goods_id ".
	"WHERE a.goods_id=".$goods_id." AND (CAST(a.act_time AS SIGNED)-CAST(b.act_time AS SIGNED)) BETWEEN 0 AND ".$timeInterval." ".
	"ORDER BY a.stock_id,time_diff DESC";
	
	$erp_list = $db->getAll($sql);
	if(count($erp_list)==0){
		$sql=str_replace(' AND a.order_sn=b.order_sn', '', $sql);
		$erp_list = $db->getAll($sql);
		if(count($erp_list)>0)
			$smarty->assign('different_order_sn',true);
	}
    $sql = "SELECT a.stock_id, a.act_date- INTERVAL 1 DAY as date_from,a.act_date+ INTERVAL 1 DAY as date_to, b.stock_id as b_id , a.goods_id,IFNULL(b.stock,0) as prev_stock, a.qty, g.goods_name, g.goods_sn, w.name as warehouse_name , ".
            "(IFNULL(b.stock,0) + a.qty) as sum_stock,  a.stock, ".
            "(SELECT sum(d.qty) FROM ".$ecs->table('erp_stock')." as d WHERE d.goods_id = a.goods_id AND d.stock_id <= a.stock_id ) as sum_qty FROM ".$ecs->table('erp_stock')." as a ".
            "LEFT JOIN ".$ecs->table('erp_stock')." as b ON b.goods_id=a.goods_id and b.stock_id = (SELECT max(c.stock_id) as stock_id FROM ".$ecs->table('erp_stock')." c WHERE c.stock_id < a.stock_id and c.goods_id = a.goods_id AND c.warehouse_id = a.warehouse_id) ".
            "LEFT JOIN ".$ecs->table('goods')." as g ON g.goods_id = a.goods_id ".
            "LEFT JOIN ".$ecs->table('erp_warehouse')." as w ON w.warehouse_id = a.warehouse_id ".
            "WHERE a.goods_id = '".$goods_id."' and a.stock <> (IFNULL(b.stock,0) + a.qty)";
	$error_stock_list = $db->getAll($sql);
    assign_query_info();
	$smarty->assign('ur_here','貨品庫存異常 - 詳細列表');
	$smarty->assign('time_interval',$timeInterval);
	$smarty->assign('action_link', array('href' => 'erp_stock_abnormal.php?act=list', 'text' => $_LANG['back']));
	$smarty->assign('erp_list',$erp_list);
    $smarty->assign('error_stock_list',$error_stock_list);
	$smarty->display('erp_stock_abnormal.htm');
}

?>
