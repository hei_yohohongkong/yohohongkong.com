<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_base.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_warehouse.php');
require(dirname(__FILE__) .'/includes/ERP/cls/cls_date.php');
$luckyDrawController = new Yoho\cms\Controller\LuckyDrawController();
$adminuserController = new Yoho\cms\Controller\AdminuserController();

if ($_REQUEST['act'] == 'list' || $_REQUEST['act'] == '') {
    $lucky_draw_id = $_REQUEST['lucky_draw_id'];
    $gift_type_options = $luckyDrawController->getGiftTypeOptions();
    $lucky_draw_event_options = $luckyDrawController->getLuckyDrawEventOptions();
    $lucky_draw_event_info = $luckyDrawController->getLuckyDrawEventInfo($lucky_draw_id);
    $smarty->assign('lucky_draw_event_info',$lucky_draw_event_info);
    $smarty->assign('gift_type_options',$gift_type_options);
    $smarty->assign('lucky_draw_event_options',$lucky_draw_event_options);
    $smarty->assign('lucky_draw_id',$lucky_draw_id);
    $smarty->assign('full_page', 1);
    $smarty->assign('cfg_lang', $_CFG['lang']);
    $smarty->assign('lang', $_LANG);  
    $smarty->assign('action_link2', array('text' => '返回抽獎活動列表', 'href' => 'lucky_draw.php'));
    assign_query_info();
    $smarty->display('lucky_draw_create.htm');

} else if ($_REQUEST['act'] == 'post_create_lucky_draw_submit') {
    $result = $luckyDrawController->createLuckyDrawSubmit();
    if (isset($result['error'])) {
        make_json_error($result['error']);
    }else{
        make_json_result($result);
    }
}
?>