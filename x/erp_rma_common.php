<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_base.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_warehouse.php');
require(dirname(__FILE__) .'/includes/ERP/cls/cls_date.php');
$rmaController = new Yoho\cms\Controller\RmaController();
$adminuserController = new Yoho\cms\Controller\AdminuserController();

if ($_REQUEST['act'] == 'post_rma_follow_up_remark_submit') {
    $result = $rmaController->rmaFollowUpRemarkSubmit();
    if($result !== false){
        make_json_result($result);
    }else{
        make_json_error('更新時發生錯誤');
    }
} else if ($_REQUEST['act'] == 'post_rma_save_note') {
    $result = $rmaController->rmaSaveNote();
    if($result !== false){
        make_json_result($result);
    }else{
        make_json_error('更新時發生錯誤');
    }
} else if ($_REQUEST['act'] == 'post_rma_save_user') {
    $result = $rmaController->rmaSaveUser();
    if($result !== false){
        make_json_result($result);
    }else{
        make_json_error('更新時發生錯誤');
    }
} else if ($_REQUEST['act'] == 'post_rma_reverse_previous_step_submit') {
    $result = $rmaController->rmaReversePreviousStepSubmit();
    if(!isset($result['error']) && $result !== false){
        make_json_result($result);
    }else{
        if (isset($result['error'])) {
            make_json_error($result['error']);
        } else {
            make_json_error('更新時發生錯誤');
        }
    }
} else if ($_REQUEST['act'] == 'post_rma_special_replace_with_new_submit') {
    $result = $rmaController->rmaSpecialReplaceWithNewSubmit();
    if(!isset($result['error']) && $result !== false){
        make_json_result($result);
    }else{
        if (isset($result['error'])) {
            make_json_error($result['error']);
        } else {
            make_json_error('更新時發生錯誤');
        }
    }
}
?>