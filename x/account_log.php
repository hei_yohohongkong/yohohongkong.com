<?php

/**
 * ECSHOP 管理中心帐户变动记录
 * ============================================================================
 * 版权所有 2005-2010 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liuhui $
 * $Id: account_log.php 17063 2010-03-25 06:35:46Z liuhui $
 */

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
include_once(ROOT_PATH . 'includes/lib_order.php');

/*------------------------------------------------------ */
//-- 办事处列表
/*------------------------------------------------------ */
if ($_REQUEST['act'] == 'list')
{
    /* 检查参数 */
    $user_id = empty($_REQUEST['user_id']) ? 0 : intval($_REQUEST['user_id']);
    if ($user_id <= 0)
    {
        sys_msg('invalid param');
    }
    $user = user_info($user_id);
    if (empty($user))
    {
        sys_msg($_LANG['user_not_exist']);
    }
    $smarty->assign('user', $user);

    if (empty($_REQUEST['account_type']) || !in_array($_REQUEST['account_type'],
        array('user_money', 'frozen_money', 'rank_points', 'pay_points')))
    {
        $account_type = '';
    }
    else
    {
        $account_type = $_REQUEST['account_type'];
    }
    $smarty->assign('account_type', $account_type);

    $smarty->assign('ur_here',      $_LANG['account_list']);
    $smarty->assign('action_link',  array('text' => $_LANG['add_account'], 'href' => 'account_log.php?act=add&user_id=' . $user_id));
    $smarty->assign('full_page',    1);

    $account_list = get_accountlist($user_id, $account_type);
    $smarty->assign('account_list', $account_list['account']);
    $smarty->assign('filter',       $account_list['filter']);
    $smarty->assign('record_count', $account_list['record_count']);
    $smarty->assign('page_count',   $account_list['page_count']);

    assign_query_info();
    $smarty->display('account_list.htm');
}

/*------------------------------------------------------ */
//-- 排序、分页、查询
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'query')
{
    /* 检查参数 */
    $user_id = empty($_REQUEST['user_id']) ? 0 : intval($_REQUEST['user_id']);
    if ($user_id <= 0)
    {
        sys_msg('invalid param');
    }
    $user = user_info($user_id);
    if (empty($user))
    {
        sys_msg($_LANG['user_not_exist']);
    }
    $smarty->assign('user', $user);

    if (empty($_REQUEST['account_type']) || !in_array($_REQUEST['account_type'],
        array('user_money', 'frozen_money', 'rank_points', 'pay_points')))
    {
        $account_type = '';
    }
    else
    {
        $account_type = $_REQUEST['account_type'];
    }
    $smarty->assign('account_type', $account_type);

    $account_list = get_accountlist($user_id, $account_type);
    $smarty->assign('account_list', $account_list['account']);
    $smarty->assign('filter',       $account_list['filter']);
    $smarty->assign('record_count', $account_list['record_count']);
    $smarty->assign('page_count',   $account_list['page_count']);

    make_json_result($smarty->fetch('account_list.htm'), '',
        array('filter' => $account_list['filter'], 'page_count' => $account_list['page_count']));
}

/*------------------------------------------------------ */
//-- 调节帐户
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'add')
{
    /* 检查权限 */
    admin_priv('account_manage');

    /* 检查参数 */
    $user_id = empty($_REQUEST['user_id']) ? 0 : intval($_REQUEST['user_id']);
    if ($user_id <= 0)
    {
        sys_msg('invalid param');
    }
    $user = user_info($user_id);
    if (empty($user))
    {
        sys_msg($_LANG['user_not_exist']);
    }
    $smarty->assign('user', $user);

    /* 显示模板 */
    $smarty->assign('ur_here', $_LANG['add_account']);
    $smarty->assign('action_link', array('href' => 'account_log.php?act=list&user_id=' . $user_id, 'text' => $_LANG['account_list']));
    assign_query_info();
    $smarty->display('account_info.htm');
}

/*------------------------------------------------------ */
//-- 提交添加、编辑办事处
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'insert' || $_REQUEST['act'] == 'update')
{
    $ajax = empty($_REQUEST['ajax']) ? 0 : intval($_REQUEST['ajax']);
    /* 检查权限 */
    $ajax ? check_authz_json('account_manage') : admin_priv('account_manage');

    /* 检查参数 */
    $user_id = empty($_REQUEST['user_id']) ? 0 : intval($_REQUEST['user_id']);
    if ($user_id <= 0)
    {
        $ajax ? make_json_error('invalid param') : sys_msg('invalid param');
    }
    $user = user_info($user_id);
    if (empty($user))
    {
        $ajax ? make_json_error($_LANG['user_not_exist']) : sys_msg($_LANG['user_not_exist']);
    }

    $refund_id = empty($_REQUEST['refund_id']) ? 0 : intval($_REQUEST['refund_id']);
    $param = [];
    if (!empty($refund_id)) {
        $param['refund_id'] = $refund_id;
    }

    /* 提交值 */
    $change_desc    = sub_str($_POST['change_desc'], 255, false);
    $user_money     = floatval($_POST['add_sub_user_money']) * abs(floatval($_POST['user_money']));
    $frozen_money   = floatval($_POST['add_sub_frozen_money']) * abs(floatval($_POST['frozen_money']));
    $rank_points    = floatval($_POST['add_sub_rank_points']) * abs(floatval($_POST['rank_points']));
    $pay_points     = floatval($_POST['add_sub_pay_points']) * abs(floatval($_POST['pay_points']));

    if ($user_money == 0 && $frozen_money == 0 && $rank_points == 0 && $pay_points == 0)
    {
        $ajax ? make_json_error($_LANG['no_account_change']) : sys_msg($_LANG['no_account_change']);
    }

    /* 保存 */
    log_account_change($user_id, $user_money, $frozen_money, $rank_points, $pay_points, $change_desc, ACT_ADJUSTING, $_SESSION['admin_id'], $param);

    /* 提示信息 */
    $links = array(
        array('href' => 'account_log.php?act=list&user_id=' . $user_id, 'text' => $_LANG['account_list'])
    );
    $ajax ? make_json_result(true) : sys_msg($_LANG['log_account_change_ok'], 0, $links);
} elseif ($_REQUEST['act'] == 'log_list') {
    $smarty->assign('ur_here', $_LANG['account_log']);
    $smarty->assign('admins', getOperators());
    $smarty->display('account_log_list.htm');
} elseif ($_REQUEST['act'] == 'log_list_query') {
    $where = "WHERE 1";
    $sort_by = empty($_REQUEST['sort_by']) ? "log_id" : $_REQUEST['sort_by'];
    $sort_order = empty($_REQUEST['sort_order']) ? "desc" : $_REQUEST['sort_order'];
    $start = empty($_REQUEST['start']) ? 0 : intval($_REQUEST['start']);
    $page_size = empty($_REQUEST['page_size']) ? 100 : intval($_REQUEST['page_size']);

    if (!empty($_REQUEST['start_date'])) {
        $where .= " AND change_time >= " . local_strtotime(trim($_REQUEST['start_date']));
    }
    if (!empty($_REQUEST['end_date'])) {
        $where .= " AND change_time < " . (local_strtotime(trim($_REQUEST['end_date'])) + 86400);
    }
    if (!empty($_REQUEST['user_name'])) {
        $where .= " AND user_name LIKE '%" . trim($_REQUEST['user_name']) . "%'";
    }
    if (isset($_REQUEST['admin_id'])) {
        if ($_REQUEST['admin_id'] >= 0) {
            $where .= " AND admin_id = '$_REQUEST[admin_id]'";
        } elseif ($_REQUEST['admin_id'] == -2) {
            $where .= " AND admin_id != 0";
        }
    }
    $admins = getOperators();

    $sql = "SELECT al.*, u.user_name FROM " . $ecs->table('account_log') . " al " .
            "LEFT JOIN (SELECT user_id, user_name FROM " . $ecs->table('users') . ") u ON u.user_id = al.user_id " .
            $where . " ORDER BY $sort_by $sort_order LIMIT $start, $page_size";
    $res = $db->getAll($sql);

    foreach ($res as $key => $row) {
        $res[$key]['change_date'] = local_date($GLOBALS['_CFG']['time_format'], $row['change_time']);
        $res[$key]['admin_name'] = $admins[$row['admin_id']];
        $res[$key]['user_name'] = "<a href='users.php?act=list&keywords=$row[user_name]'>$row[user_name]</span>";
        $res[$key]['rank_points'] = "<span class='" . (intval($row['rank_points']) > 0 ? "green'>+" : (intval($row['rank_points']) < 0 ? "red'>" : "'>")) . "$row[rank_points]</span>";
        $res[$key]['pay_points'] = "<span class='" . (intval($row['pay_points']) > 0 ? "green'>+" : (intval($row['pay_points']) < 0 ? "red'>" : "'>")) . "$row[pay_points]</span>";
    }
    $count = $db->getOne(
        "SELECT COUNT(DISTINCT log_id) FROM " . $ecs->table('account_log') . " al " .
        "LEFT JOIN (SELECT user_id, user_name FROM " . $ecs->table('users') . ") u ON u.user_id = al.user_id " .
        $where
    );
    $controller = new Yoho\cms\Controller\YohoBaseController();
    $controller->ajaxQueryAction($res, $count, false);
}

/**
 * 取得帐户明细
 * @param   int     $user_id    用户id
 * @param   string  $account_type   帐户类型：空表示所有帐户，user_money表示可用资金，
 *                  frozen_money表示冻结资金，rank_points表示等级积分，pay_points表示消费积分
 * @return  array
 */
function get_accountlist($user_id, $account_type = '')
{
    /* 检查参数 */
    $where = " WHERE user_id = '$user_id' ";
    if (in_array($account_type, array('user_money', 'frozen_money', 'rank_points', 'pay_points')))
    {
        $where .= " AND $account_type <> 0 ";
    }

    /* 初始化分页参数 */
    $filter = array(
        'user_id'       => $user_id,
        'account_type'  => $account_type
    );

    /* 查询记录总数，计算分页数 */
    $sql = "SELECT COUNT(*) FROM " . $GLOBALS['ecs']->table('account_log') . $where;
    $filter['record_count'] = $GLOBALS['db']->getOne($sql);
    $filter = page_and_size($filter);

    $admins = getOperators();

    /* 查询记录 */
    $sql = "SELECT * FROM " . $GLOBALS['ecs']->table('account_log') . $where .
            " ORDER BY log_id DESC";
    $res = $GLOBALS['db']->selectLimit($sql, $filter['page_size'], $filter['start']);

    $arr = array();
    while ($row = $GLOBALS['db']->fetchRow($res))
    {
        $row['change_time'] = local_date($GLOBALS['_CFG']['time_format'], $row['change_time']);
        $row['admin_name'] = $admins[$row['admin_id']];
        $arr[] = $row;
    }

    return array('account' => $arr, 'filter' => $filter, 'page_count' => $filter['page_count'], 'record_count' => $filter['record_count'], 'admins' => $admins);
}

function getOperators()
{
    $admins = ["系統生成"];
    $operators = $GLOBALS['db']->getAll(
        "SELECT al.admin_id, au.user_name FROM " . $GLOBALS['ecs']->table('admin_user') . " au " .
        "LEFT JOIN " . $GLOBALS['ecs']->table('account_log') . " al ON au.user_id = al.admin_id " .
        "WHERE al.admin_id != 0"
    );
    foreach ($operators as $row) {
        $admins[$row['admin_id']] = $row['user_name'];
    }
    return $admins;
}

?>