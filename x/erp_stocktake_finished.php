<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_base.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_warehouse.php');
require(dirname(__FILE__) .'/includes/ERP/cls/cls_date.php');
$stocktakeController = new Yoho\cms\Controller\StocktakeController();
$adminuserController = new Yoho\cms\Controller\AdminuserController();

if ($_REQUEST['act'] == 'list' || $_REQUEST['act'] == '') {
	$stocktake_id = $_REQUEST['stocktake_id'];
	$stocktakeController->checkStatusRedirect($stocktake_id,'finished');
	//$result = $stocktakeController->detail('finished');
	
    $stocktake_person_option = $stocktakeController->stocktakePersonOption();
    $goods_status_options = $stocktakeController->getGoodsStatusOptions('review');
    $stocktake_person_list = $stocktakeController->stocktakePersonList();
    $stocktake = $stocktakeController->getStocktakeInfo($stocktake_id);
	$stocktakeController->updateStocktakeFinishedCount($stocktake_id);
	$smarty->assign('stocktake_person_list',$stocktake_person_list);
	$smarty->assign('stocktake',$stocktake);
	$smarty->assign('stocktake_id',$_REQUEST['stocktake_id']);
	//$smarty->assign('warehouse_name_list',$result['warehouse_name_list']);
	$smarty->assign('filter', $result['filter']);
	$smarty->assign('record_count', $result['record_count']);
	$smarty->assign('page_count', $result['page_count']);
	$smarty->assign('data', $result['data']);
	$smarty->assign('full_page', 1);
	$smarty->assign('cfg_lang', $_CFG['lang']);
	$smarty->assign('lang', $_LANG);
	$smarty->assign('action_link', array('text' => '盤點工作清單', 'href' => 'erp_stocktake.php'));

	//	$smarty->assign('action_link', array('text' => '盤點工作清單', 'href' => 'erp_stocktake.php'));
	assign_query_info();
	
	$smarty->display('erp_stocktake_finished.htm');


} elseif ($_REQUEST['act'] == 'query') {

	$result = $stocktakeController->detail('finished');

	$smarty->assign('goods_status_options',$result['goods_status_options']);
	$smarty->assign('stocktake_id',$_REQUEST['stocktake_id']);
	$smarty->assign('warehouse_name_list',$result['warehouse_name_list']);
	$smarty->assign('filter', $result['filter']);
	$smarty->assign('record_count', $result['record_count']);
	$smarty->assign('page_count', $result['page_count']);
	$smarty->assign('data', $result['data']);
	$smarty->assign('full_page', 0);
	$smarty->assign('cfg_lang', $_CFG['lang']);
	$smarty->assign('lang', $_LANG);

   $stocktakeController->ajaxQueryAction($result['data'],$result['record_count'],false);
	  

	// 
  	// $result = $stocktakeController->detail('finished');
    // $smarty->assign('goods_status_options',$result['goods_status_options']);
    // $smarty->assign('stocktake_id',$_REQUEST['stocktake_id']);
    // $smarty->assign('warehouse_name_list',$result['warehouse_name_list']);
    // $smarty->assign('filter', $result['filter']);
    // $smarty->assign('record_count', $result['record_count']);
    // $smarty->assign('page_count', $result['page_count']);
    // $smarty->assign('data', $result['data']);
    // $smarty->assign('full_page', 0);
    // $smarty->assign('cfg_lang', $_CFG['lang']);
    // $smarty->assign('lang', $_LANG);
    // make_json_result($smarty->process_inserts($smarty->fetch('erp_stocktake_finished.htm')), '',
    //     array('filter' => $result['filter'], 'page_count' => $result['page_count']));
}
?>