<?php

/***
 * yahoo_store.php
 * by Anthony 2017-03-13
 *
 * Manage yahoo store function
 ***/

//TODO: will be rewrite with MVC
define('IN_ECS', true);
require(dirname(__FILE__) . '/includes/init.php');

$default_address_id = '57246';
$erpController = new Yoho\cms\Controller\ErpController();
$controller = new Yoho\cms\Controller\YohoBaseController('yahoo_store');
$salesagentController = new Yoho\cms\Controller\SalesagentController();
/*------------------------------------------------------ */
//-- List Yahoo item
/*------------------------------------------------------ */
if ($_REQUEST['act'] == 'list')
{
	$salesagentController->yahooListAction();
}
/*------------------------------------------------------ */
//-- Query Yahoo item in list
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'query')
{
    $list = get_yahoo_list();

    $controller->ajaxQueryAction($list['data'], $list['record_count'], false);

}
/*------------------------------------------------------ */
//-- Add Yahoo item
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'add'){

    /* check admin priv */
    admin_priv('yahoo_store');

    $smarty->assign('ur_here', $_LANG['yahoo_store']);

    $smarty->assign('action_link', array(
        'text' => '返回列表', 'href' => 'yahoo_store.php?act=list'));

    $smarty->assign('act', $_REQUEST['act']);

    $smarty->assign('address_id', $default_address_id);
    $smarty->assign('cfg_lang', $_CFG['lang']);
    $smarty->assign('form_action', 'insert');
    $smarty->assign('goods_cat_list', cat_list());
    $smarty->assign('brand_list',     get_brand_list());

    assign_query_info();

    $smarty->display('yahoo_store_info.htm');
}
/*------------------------------------------------------ */
//-- Insert Yahoo item
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'insert')
{
    /* check admin priv */
    admin_priv('yahoo_store');

    $brand_id = $_POST['b_id'];
    $address_id = $_POST['address_id'];
    $goods_list = $_POST['target_select'];

    //Find duplicate goods
    $id_list = implode("', '", $goods_list);

    $sql = "SELECT goods_id " .
            "FROM " . $ecs->table('yahoo_store') . " " .
            "WHERE goods_id IN('" . $id_list . "')";

    $result = $db->getAll($sql);

    //Remove duplicate goods
    foreach ($result as $val) {

        foreach ($goods_list as $key => $row) {
            if ($val['goods_id'] == $row) {
                unset($goods_list[$key]);
            }
        }
    }

    //Get goods info
    $goods = get_goods_info($goods_list);

    //Insert to yahoo_store table
    foreach ($goods as $key => $row) {

        $yahoo_detail = "";
        $yahoo_detail .= '<p><span style="" id="yui_3_12_0_11_1526032900854_51">感謝您對 友和YOHO 的支持!</span></p><p><br></p><p>銷售條款及細則:</p><p>- 客戶必須確保所提交的資料正確無誤，如因不正確或不完整資料而導致未能送遞府上或一切之損失，本公司恕不負責。</p><p>- 交易一經付款程序確認後，所提交的資料將不可更改及所有銷售之貨品不得退換/退款。</p><p>- 客戶於收件時請檢查貨品及數量，如有任何查詢，請致電客戶服務熱線5335-6996，如客戶服務電話線路繁忙未能接通，請客人優先使用Whatsapp或電郵作出查詢。</p><p>- 如購買之貨品已售罄或需時訂購，閣下亦可要求全數退還款項。</p><p>- 本公司提供 7天有壞換貨保證，每張訂單只可進行 1次換貨。 (特定產品除外，請下單時留意產品備註)</p><p>- 本公司只接受產品質量問題之退/換貨申請。</p><p>- 如發現所收取的貨品因運送過程中構成損壞或與訂單內容有誤，請於 7天內 保留原有的完整包裝連同單據，攜帶至門市予店員檢閱或試機，經雙方同意後將安排更換貨品，時間需視乎存貨量及貨期而定。</p><p>- 資料及圖片只供參考，一切以實物為準。</p><p>- 如有任何爭議，友和 YOHO 保留最終決定權。</p><p>- 友和 YOHO 有權修改此使用條款及細則內容而無需另行通知。</p><p>&nbsp;</p><p>貨品運送條款及細則:</p><p>客戶可選擇以下兩種配送方法</p><p>速遞服務</p><p>- 購物滿港幣$1000或以上(淨貨品金額，以單一訂單計算)可享免費送貨，不足港幣$1000收取每件港幣$33。</p><p>- 速遞只限香港境內的地址。在一般情況下，閣下將於完成訂購後的7至10天收到貨品。</p><p>&nbsp;</p><p>門市自取</p><p>- 地址：九龍觀塘巧明街112號友聯大廈6樓</p><div><span style="font-weight:bold;"><span style="color:rgb(95, 95, 95);font-family:sans-serif, arial;font-weight:normal;" id="yui_3_12_0_11_1526032900854_45">- 出貨時我們將會有專人致電作出通知，請收到通知後再到門市取貨。</span></span></div><div><span style="font-weight:bold;"><br></span></div><div><span style="font-weight:bold;"><br></span></div>';
        if($row['seller_note'] != ""){
            $yahoo_detail .= '<div><span style="font-weight: bold;" id="yui_3_12_0_11_1491552381138_51">保養條款:</span></div><div>' . $row['seller_note'] . '</div><br>';
        }
        if($row['shipping_note'] != ""){
            $yahoo_detail .= '<div><span style="font-weight: bold;" id="yui_3_12_0_11_1491552381138_51">安裝條款:</span></div><div>' . $row['shipping_note'] . '</div>';
        }

        $yahoo_item = array(
            'goods_id' => $key,
            'yahoo_brand_id' => $brand_id,
            'yahoo_quantity' => 100, // hardcore: 100
            // 'yahoo_quantity' => ($row['goods_number'] > 0)? 0: 10,
            'yahoo_price' => $row['shop_price'],
            'creator' => $_SESSION['admin_name'],
            'created_at' => gmtime(),
            'yahoo_detail' => $yahoo_detail,
            'redemption_method' => $address_id ? $address_id : $default_address_id
        );

        $db->autoExecute($ecs->table('yahoo_store'), $yahoo_item, 'INSERT');
    }

    clear_cache_files();

    $link[0]['text'] = '返回列表';
    $link[0]['href'] = 'yahoo_store.php?act=list';

    sys_msg('新增Yahoo商品成功', 0, $link, true);
}
/*------------------------------------------------------ */
//-- batch function
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'batch')
{
    $code = empty($_REQUEST['extension_code']) ? '' : trim($_REQUEST['extension_code']);

    /* Get item ids  */
    $yahoo_ids = explode(",", $_POST['id']);

    if (isset($_POST['type'])) {
        /* Remove item  */
        if ($_POST['type'] == 'drop') {
            /* check admin priv */
            admin_priv('yahoo_store');

            delete_yahoo($yahoo_ids);

            /* save log */
            admin_log('', 'batch_remove', 'yahoo');

            /* clear cache */
            clear_cache_files();

            $lnk[] = array('text' => $_LANG['back_list'], 'href' => 'yahoo_store.php?act=list');

            sys_msg($_LANG['batch_handle_ok'], 0, $link);
        } else if ($_POST['type'] == 'export') {
            /* check admin priv */
            admin_priv('yahoo_store');

            export_batch_yahoo($yahoo_ids);

            /* save log */
            admin_log('', 'export_csv', 'yahoo');
        }
    }
}
/*------------------------------------------------------ */
//-- Edit Yahoo item
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'ajaxEdit') {
	$controller->ajaxEditAction();
}
/*------------------------------------------------------ */
//-- Delete yahoo item
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'remove')
 {
    check_authz_json('yahoo_manage');

    $id = intval($_GET['id']);

    $sql = "DELETE FROM " . $ecs->table('yahoo_store') . " WHERE " . "yahoo_id = $id";

    if ($db->query($sql)) {
        clear_cache_files();
    }
    $url = 'yahoo_store.php?act=query&' . str_replace('act=remove', '', $_SERVER['QUERY_STRING']);

    ecs_header("Location: $url\n");
    exit;
}
/*------------------------------------------------------ */
//-- Export CSV
/*------------------------------------------------------ */
elseif($_REQUEST['act'] == 'export'){

    $salesagentController->export_csv_yahoo([], $_REQUEST['type']);
}
/*------------------------------------------------------ */
//-- Search goods item
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'get_goods_list')
{
    include_once(ROOT_PATH . 'includes/cls_json.php');
    $json = new JSON;

    $_REQUESTs = $json->decode($_GET['JSON']);
    $args = $json->decode($_GET['JSON']);
    $arr = get_goods_list($_REQUESTs);
    $opt = array();

    foreach ($arr AS $key => $val) {
        $opt[] = array('value' => $val['goods_id'],
            'text' => $val['goods_id'] . " - " . $val['goods_name'],
            'data' => '');
    }

    make_json_result($opt);
}

/*------------------------------------------------------ */
//-- Function
/*------------------------------------------------------ */

function get_yahoo_list()
{
    global $db, $ecs;

    $_REQUEST['sort_by'] = empty($_REQUEST['sort_by']) ? 'yahoo_id' : trim($_REQUEST['sort_by']);
    $_REQUEST['sort_order'] = empty($_REQUEST['sort_order']) ? 'ASC' : trim($_REQUEST['sort_order']);
    $_REQUEST['search'] = empty($_REQUEST['search']) ? '' : trim($_REQUEST['search']);
	$where = (empty($_REQUEST['search']) ? "" : "WHERE g.goods_sn LIKE '%" . $_REQUEST['search'] . "%' OR g.goods_id LIKE '%" . $_REQUEST['search'] . "%' OR g.goods_name LIKE '%" . $_REQUEST['search'] . "%' OR ys.yahoo_sku LIKE '%" . $_REQUEST['search'] . "%' ");
    $sql = "SELECT count(*) FROM " . $ecs->table('yahoo_store') . " as ys LEFT JOIN " . $ecs->table('goods') . " as g ON g.goods_id = ys.goods_id " . $where;
    $_REQUEST['record_count'] = $db->getOne($sql);

    $sql = "SELECT ys.*, g.`goods_name`, g.`goods_sn`, g.`goods_thumb`, g.`market_price`, g.`shop_price` as goods_price,  g.`shipping_price`, g.is_shipping  " .
            "FROM " . $ecs->table('yahoo_store') . " as ys " .
            "LEFT JOIN " . $ecs->table('goods') . " as g ON g.goods_id = ys.goods_id " . $where .
            "ORDER BY " . $_REQUEST['sort_by'] . ' ' . $_REQUEST['sort_order'] . ' ' .
            "LIMIT " . $_REQUEST['start'] . "," . $_REQUEST['page_size'];
    $data = $db->getAll($sql);

    foreach ($data as $key => $row) {
		$shipping_price = 0;
		if($row['is_shipping'] == 1)$shipping_price = 0;
        elseif ($row['shipping_price'] > 0) $shipping_price = $row['shipping_price'];
        // elseif ($row['yahoo_price'] < 1000) $shipping_price = 33;
		$row['yahoo_price'] = $row['yahoo_price'] + $shipping_price;
		$yahoo_price_formatted = price_format($row['yahoo_price'], false);
		$data[$key]['yahoo_price_formatted'] = $yahoo_price_formatted ."(運費$$shipping_price)";
		$row['market_price'] = $row['market_price'] + $shipping_price;
		$market_price_formatted = price_format($row['market_price'], false);
		$data[$key]['market_price_formatted'] = $market_price_formatted ."(運費$$shipping_price)";
		$data[$key]['goods_price_formatted'] = price_format($row['goods_price'], false);
        $data[$key]['created_at_formatted'] = local_date($GLOBALS['_CFG']['date_format'], $row['created_at']);
    }
    $arr = array('data' => $data, 'record_count' => $_REQUEST['record_count']);
    return $arr;
}

function get_goods_info($ids) {

    global $db, $ecs, $erpController;

    $sql = "SELECT goods_id, goods_sn, goods_name, shop_price, " .
            "(CASE WHEN g.warranty_template_id = 0 THEN g.seller_note ELSE wt.template_content END) AS seller_note, " .
            "(CASE WHEN g.shipping_template_id = 0 THEN g.shipping_note ELSE st.template_content END) AS shipping_note, " .
            "goods_brief, (SELECT sum(ega.goods_qty) FROM " . $GLOBALS['ecs']->table('erp_goods_attr_stock') . " as ega WHERE ega.goods_id = goods_id AND ega.warehouse_id IN (".$erpController->getSellableWarehouseIds(true).")) as goods_number " .
            "FROM " . $ecs->table('goods') . " g " .
            "LEFT JOIN " . $ecs->table('warranty_templates') . " wt ON wt.template_id = g.warranty_template_id " .
            "LEFT JOIN " . $ecs->table('shipping_templates') . " st ON st.template_id = g.shipping_template_id " .
            "WHERE goods_id " . db_create_in($ids);

    $data = $db->getAll($sql);

    $arr = array();
    foreach ($data as $row) {
        $arr[$row['goods_id']] = $row;
    }

    return $arr;
}

function get_goods_gallery($goods_id)
{
    $sql = 'SELECT img_id, img_url' .
            ' FROM ' . $GLOBALS['ecs']->table('goods_gallery') .
            " WHERE goods_id = '$goods_id'" .
            " ORDER BY sort_order " .
            " LIMIT 5";
    $row = $GLOBALS['db']->getAll($sql);

    return $row;
}

function delete_yahoo($ids) {
    $sql = "DELETE FROM " . $GLOBALS['ecs']->table('yahoo_store') .
            " WHERE yahoo_id " . db_create_in($ids);
    $GLOBALS['db']->query($sql);

    clear_cache_files();
}

function export_batch_yahoo($ids) {
    global $db, $ecs;

    //File name
    $name = date("F-j-Y-g-i-s") . '-yahoo-csv';

    //Set CSV Header
    header("Content-Type: text/csv");
    header("Content-Disposition: attachment; filename=$name.csv");

    // Disable caching
    header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
    header("Pragma: no-cache"); // HTTP 1.0
    header("Expires: 0"); // Proxies

    $fp = fopen("php://output", "w");

    //Get item data
    $sql = "SELECT y.yahoo_id as id , y.goods_id as goods_id, y.yahoo_brand_id as brand_id, " .
            "y.yahoo_quantity as yahoo_quantity, y.yahoo_price as yahoo_price, " .
            "y.yahoo_detail as yahoo_detail, y.redemption_method as redemption_method, " .
            "g.goods_sn as sku , g.goods_name as goods_name , g.market_price as market_price, " .
            "g.goods_desc as goods_desc, g.goods_img as goods_img, g.shop_price as shop_price, g.shipping_price as shipping_price, g.is_shipping " .
            "FROM " . $ecs->table('yahoo_store') . "as y " .
            "LEFT JOIN " . $ecs->table('goods') . " AS g ON g.goods_id = y.goods_id " .
            "WHERE yahoo_id " . db_create_in($ids);

    $data = $db->getAll($sql);

    //Add yahoo csv header format
    $content = array();
    $content[] = array(
        'Category *', 'Title *', 'Main Spec', 'Sub Spec', 'Quantity *', 'Sale Price *',
        'Original Price', 'SKU', 'image1 *', 'image2', 'image3', 'image4',
        'image5', 'Start Date', 'End Date', 'Description *', 'Disclaimer *', 'Redemption Method Self Pickup'
    );

    //Add data
    foreach ($data as $row) {

        //Get gallery
        $imgs = get_goods_gallery($row['goods_id']);

        if (isset($imgs)) { //If have gallery
            $img1 = $imgs[0]['img_url'] ? get_full_url($imgs[0]['img_url']) : get_full_url($row['goods_img']);
            $img2 = $imgs[1]['img_url'] ? get_full_url($imgs[1]['img_url']) : '';
            $img3 = $imgs[2]['img_url'] ? get_full_url($imgs[2]['img_url']) : '';
            $img4 = $imgs[3]['img_url'] ? get_full_url($imgs[3]['img_url']) : '';
            $img5 = $imgs[4]['img_url'] ? get_full_url($imgs[4]['img_url']) : '';
        } else { //If not, use goods img
            $img1 = get_full_url($row['goods_img']);
            $img2 = '';
            $img3 = '';
            $img4 = '';
            $img5 = '';
        }

        $qty = $row['yahoo_quantity'] ? $row['yahoo_quantity'] : 100;
        // If have shipping price, add in sale price
		$shipping_price = 0;
		if ($row['is_shipping'] == 1) $shipping_price = 0;
        elseif ($row['shipping_price'] > 0) $shipping_price = $row['shipping_price'];
//        elseif ($row['shop_price'] < 1000) $shipping_price = 33;
        $sale_price = (int) $row['shop_price'] + (int) $shipping_price;
        $market_price = (int) $row['market_price'] + (int) $shipping_price;
        $disclaimer = $row['yahoo_detail'] ? $row['yahoo_detail'] : '未有相關資料';

        //Add item in array
        $content[] = array(
            $row['brand_id'], $row['goods_name'], '', '', $qty, $sale_price,
            $market_price, $row['sku'], $img1, $img2, $img3, $img4,
            $img5, '', '', $row['goods_desc'], $disclaimer, $row['redemption_method'] ? $row['redemption_method'] : $default_address_id
        );
    }

    //Output CSV
    foreach ($content as $fields) {
        fputcsv($fp, $fields);
    }

    fclose($fp);
}

function get_full_url($url) {

    //Get full path
    define('SERVER_DOMAIN', implode('.', array_slice(explode('.', $_SERVER['SERVER_NAME']), -2)));
    if (strpos($_SERVER['SERVER_NAME'], 'beta.') !== false) {
        define('DESKTOP_HOST', 'beta.' . SERVER_DOMAIN);
        define('MOBILE_HOST', 'm.beta.' . SERVER_DOMAIN);
    } else {
        define('DESKTOP_HOST', 'www.' . SERVER_DOMAIN);
        define('MOBILE_HOST', 'm.' . SERVER_DOMAIN);
    }

    $new_url = 'https://' . DESKTOP_HOST . (substr($url, 0, 1) == '/' ? '' : '/') . $url;

    return $new_url;
}
?>
