<?php

/**
 * ECSHOP 程序说明
 * ===========================================================
 * 版权所有 2005-2010 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ==========================================================
 * $Author: liuhui $
 * $Id: affiliate.php 17063 2010-03-25 06:35:46Z liuhui $
 */

define('IN_ECS', true);
require(dirname(__FILE__) . '/includes/init.php');
admin_priv('affiliate');
$config = get_affiliate();

/*------------------------------------------------------ */
//-- 分成管理页
/*------------------------------------------------------ */
if ($_REQUEST['act'] == 'list')
{
    assign_query_info();
    if (empty($_REQUEST['is_ajax']))
    {
        $smarty->assign('full_page', 1);
    }

    $smarty->assign('ur_here', $_LANG['affiliate']);
    $smarty->assign('config', $config);
    $sql = "SELECT * FROM " . $ecs->table('shop_config') . " WHERE parent_id > 0 AND type = 'file' AND code = 'affiliate_btn_img' ";
    $affiliate_btn_img = $db->getRow($sql);
    $smarty->assign('affiliate_btn_img', $affiliate_btn_img);
    $sql = "SELECT * FROM " . $ecs->table('shop_config') . " WHERE parent_id > 0 AND type = 'file' AND code = 'affiliate_banner_img' ";
    $affiliate_banner_img = $db->getRow($sql);
    $smarty->assign('affiliate_banner_img', $affiliate_banner_img);
    $smarty->display('affiliate.htm');
}
elseif ($_REQUEST['act'] == 'query')
{
    $smarty->assign('ur_here', $_LANG['affiliate']);
    $smarty->assign('config', $config);
    $sql = "SELECT * FROM " . $ecs->table('shop_config') . " WHERE parent_id > 0 AND type = 'file' AND code = 'affiliate_btn_img' ";
    $affiliate_btn_img = $db->getRow($sql);
    $smarty->assign('affiliate_btn_img', $affiliate_btn_img);
    $sql = "SELECT * FROM " . $ecs->table('shop_config') . " WHERE parent_id > 0 AND type = 'file' AND code = 'affiliate_banner_img' ";
    $affiliate_banner_img = $db->getRow($sql);
    $smarty->assign('affiliate_banner_img', $affiliate_banner_img);
    make_json_result($smarty->fetch('affiliate.htm'), '', null);
}
/*------------------------------------------------------ */
//-- 增加下线分配方案
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'add')
{
    if (count($config['item']) < 5)
    {
        //下线不能超过5层
        $_POST['level_point'] = (float)$_POST['level_point'];
        $_POST['level_money'] = (float)$_POST['level_money'];
        $maxpoint = $maxmoney = 100;
        foreach ($config['item'] as $key => $val)
        {
            $maxpoint -= $val['level_point'];
            $maxmoney -= $val['level_money'];
        }
        $_POST['level_point'] > $maxpoint && $_POST['level_point'] = $maxpoint;
        $_POST['level_money'] > $maxmoney && $_POST['level_money'] = $maxmoney;
        if (!empty($_POST['level_point']) && strpos($_POST['level_point'],'%') === false)
        {
            $_POST['level_point'] .= '%';
        }
        if (!empty($_POST['level_money']) && strpos($_POST['level_money'],'%') === false)
        {
            $_POST['level_money'] .= '%';
        }
        $items = array('level_point'=>$_POST['level_point'],'level_money'=>$_POST['level_money']);
        $links[] = array('text' => $_LANG['affiliate'], 'href' => 'affiliate.php?act=list');
        $config['item'][] = $items;
        $config['on'] = 1;
        $config['config']['separate_by'] = 0;

        put_affiliate($config);
    }
    else
    {
       make_json_error($_LANG['level_error']);
    }

    ecs_header("Location: affiliate.php?act=query\n");
    exit;
}
/*------------------------------------------------------ */
//-- 修改配置
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'updata')
{
    $config = get_affiliate();
    $separate_by = (intval($_POST['separate_by']) == 1) ? 1 : 0;

    $_POST['expire'] = (float) $_POST['expire'];
    $_POST['level_point_all'] = (empty($_POST['level_point_all'])) ? 0 : $_POST['level_point_all'];
    $_POST['level_money_all'] = (empty($_POST['level_money_all'])) ? 0 : $_POST['level_money_all'];

    $auto_ck = (intval($_POST['auto_ck']) == 1) ? 1 : 0;
    $level_order_num = (intval($_POST['level_order_num']) == 1) ? 1 : 0;

    // if (strpos($_POST['level_point_all'],'%') === false)
    // {

    // }
    // if (strpos($_POST['level_money_all'],'%') === false)
    // {

    // }
    $_POST['level_register_all'] = intval($_POST['level_register_all']);
    $_POST['level_register_up'] = intval($_POST['level_register_up']);

    $_POST['affiliate_text'] = str_replace('\"', '', $_POST['affiliate_text']);
    $_POST['affiliate_text'] = str_replace("\'", "", $_POST['affiliate_text']);

    $temp = array();
    $temp['config'] = array('expire'                 => $_POST['expire'],        //COOKIE过期数字
                            'expire_unit'            => $_POST['expire_unit'],   //单位：小时、天、周
                            'separate_by'            => $separate_by,            //分成模式：0、注册 1、订单
                            'level_point_all'        => $_POST['level_point_all'],    //积分分成比
                            'level_money_all'        => $_POST['level_money_all'],    //金钱分成比
                            'level_register_all'     => $_POST['level_register_all'], //推荐注册奖励积分
                            'level_register_up'      => $_POST['level_register_up'],   //推荐注册奖励积分上限
                            'auto_ck'                => $auto_ck,   //自動批准回贈
                            'level_order_num'        => $level_order_num,   //優惠訂單數
                            'level_order_amount'     => $_POST['level_order_amount'],   //訂單所需金額
                            'instant_money_discount' => $_POST['instant_money_discount'],   //即時訂單現金折扣
                            'instant_point_refund'   => $_POST['instant_point_refund'],   //即時訂單積分回贈
                            'affiliate_text'         => $_POST['affiliate_text'],   //即時訂單積分回贈
          );
    $temp['item'] = $config['item'];
    $temp['on'] = 1;
    $temp['affiliate_yoho_on'] = $config['affiliate_yoho_on'];
    put_affiliate($temp);
    $links[] = array('text' => $_LANG['affiliate'], 'href' => 'affiliate.php?act=list');

    /* 允许上传的文件类型 */
    $allow_file_types = '|GIF|JPG|PNG|BMP|';

    /* 处理上传文件 */
    $file_var_list = array();
    $sql = "SELECT * FROM " . $ecs->table('shop_config') . " WHERE parent_id > 0 AND type = 'file' AND code IN('affiliate_btn_img', 'affiliate_banner_img') ";
    $res = $db->getAll($sql);
    foreach ($res as $row) $file_var_list[$row['code']] = $row;

    foreach ($_FILES AS $code => $file)
    {
        /* 判断用户是否选择了文件 */
        if ((isset($file['error']) && $file['error'] == 0) || (!isset($file['error']) && $file['tmp_name'] != 'none'))
        {
            /* 检查上传的文件类型是否合法 */
            if (!check_file_type($file['tmp_name'], $file['name'], $allow_file_types))
            {
                //sys_msg(sprintf($_LANG['msg_invalid_file'], $file['name']));
            }
            else
            {
                if (!make_dir($file_var_list[$code]['store_dir']))
                {
                    /* 创建目录失败 */
                    return false;
                }
                $ext = array_pop(explode('.', $file['name']));
                $file_name = $file_var_list[$code]['store_dir'] . $code .'.'. $ext;

                /* 判断是否上传成功 */
                if (move_upload_file($file['tmp_name'], $file_name))
                {
                    $sql = "UPDATE " . $ecs->table('shop_config') . " SET value = '$file_name' WHERE code = '$code'";
                    $db->query($sql);
                }
            }
        }
    }

    /* 清除缓存 */
    clear_all_files();
    $_CFG = load_config();
    sys_msg($_LANG['edit_ok'], 0 ,$links);
}
/*------------------------------------------------------ */
//-- 推荐开关
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'on')
{

    $on = (intval($_POST['on']) == 1) ? 1 : 0;
    $affiliate_yoho_on = (intval($_POST['affiliate_yoho_on']) == 1) ? 1 : 0;

    $config['on'] = $on;
    $config['affiliate_yoho_on'] = $affiliate_yoho_on;
    put_affiliate($config);
    $links[] = array('text' => $_LANG['affiliate'], 'href' => 'affiliate.php?act=list');
    sys_msg($_LANG['edit_ok'], 0 ,$links);
}
/*------------------------------------------------------ */
//-- Ajax修改设置
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'edit_point')
{

    /* 取得参数 */
    $key = trim($_POST['id']) - 1;
    $val = (float)trim($_POST['val']);
    $maxpoint = 100;
    foreach ($config['item'] as $k => $v)
    {
        if ($k != $key)
        {
            $maxpoint -= $v['level_point'];
        }
    }
    $val > $maxpoint && $val = $maxpoint;
    if (!empty($val) && strpos($val,'%') === false)
    {
        $val .= '%';
    }
    $config['item'][$key]['level_point'] = $val;
    $config['on'] = 1;
    put_affiliate($config);
    make_json_result(stripcslashes($val));
}
/*------------------------------------------------------ */
//-- Ajax修改设置
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'edit_money')
{
    $key = trim($_POST['id']) - 1;
    $val = (float)trim($_POST['val']);
    $maxmoney = 100;
    foreach ($config['item'] as $k => $v)
    {
        if ($k != $key)
        {
            $maxmoney -= $v['level_money'];
        }
    }
    $val > $maxmoney && $val = $maxmoney;
    if (!empty($val) && strpos($val,'%') === false)
    {
        $val .= '%';
    }
    $config['item'][$key]['level_money'] = $val;
    $config['on'] = 1;
    put_affiliate($config);
    make_json_result(stripcslashes($val));
}
/*------------------------------------------------------ */
//-- 删除下线分成
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'del')
{
    $key = trim($_GET['id']) - 1;
    unset($config['item'][$key]);
    $temp = array();
    foreach ($config['item'] as $key => $val)
    {
        $temp[] = $val;
    }
    $config['item'] = $temp;
    $config['on'] = 1;
    $config['config']['separate_by'] = 0;
    put_affiliate($config);
    ecs_header("Location: affiliate.php?act=list\n");
    exit;
}

function get_affiliate()
{
    $config = unserialize($GLOBALS['_CFG']['affiliate']);
    empty($config) && $config = array();

    return $config;
}

function put_affiliate($config)
{
    $temp = serialize($config);
    $sql = "UPDATE " . $GLOBALS['ecs']->table('shop_config') .
           "SET  value = '$temp'" .
           "WHERE code = 'affiliate'";
    $GLOBALS['db']->query($sql);
    clear_all_files();
}
?>