<?php
/***
* ctrl_role.php
* by MichaelHui 20170328
*
* Order controller
***/

namespace Yoho\cms\Controller;
use Yoho\cms\Model;
//use function GuzzleHttp\json_decode;

if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}


class OrderController extends YohoBaseController{
	const DB_ORDER_TYPE_WHOLESALE = 'ws';
	const DB_ORDER_TYPE_SALESAGENT = 'sa';
	const DB_REPAIR_ORDER_STATUS_PENDING = 1;
	const DB_REPAIR_ORDER_STATUS_REPAIRING = 2;
	const DB_REPAIR_ORDER_STATUS_REPAIRED = 3;
	const DB_REPAIR_ORDER_STATUS_RESOLVED = 4;
	const DB_REPAIR_ORDER_STATUS = ['待處理','待處理','維修中','送修完成待處理','完成及可出貨'];
    const DB_SELLING_PLATFORMS = ['pos'=>'POS','web'=>'網站','mobile'=>'手機版網站','app'=>'手機App','pos_exhibition'=>'展場POS'];
    
    const ORDER_SEPARATE_WAIT   = 0; //未分成或等待分成
    const ORDER_SEPARATE_DONE   = 1; //已分成
    const ORDER_SEPARATE_CANCEL = 2; //取消分成
    const ORDER_SEPARATE_IGNORE = 3; //不處理分成 (只有推廣大使優惠的訂單)
    
    //array[ORDER_FEE_PLATFORM_WEB, ORDER_FEE_PLATFORM_POS, ORDER_FEE_PLATFORM_CMS, ORDER_FEE_PLATFORM_MONEY]
    const ORDER_FEE_PLATFORM_WEB   = 'web';
    const ORDER_FEE_PLATFORM_POS   = 'pos';
    const ORDER_FEE_PLATFORM_CMS   = 'cms';
    const ORDER_FEE_PLATFORM_MONEY = 'money';


	const ORDER_GOODS_RESERVE_TYPE_TRANSFER = '0';
	const ORDER_GOODS_RESERVE_TYPE_PREORDER = '1';

    const ORDER_GOODS_RESERVED_STATUS_PENDING = 0;
	const ORDER_GOODS_RESERVED_STATUS_TRANSFERRING = 1;
    const ORDER_GOODS_RESERVED_STATUS_FINISHED = 2;

    //China Cross-Border E-Commerce Tax rate
    const ORDER_CN_SHIP_TAX = 9.1; // in %

	public function __construct(){
        parent::__construct();
        $this->tableName = 'order_info';

	}

	public function getOrderIdBySn($orderSn){
		$cacheKey=$this->genCacheKey(['order','oid',$orderSn]);
		if($this->memcache->get($cacheKey)!=false){
			return $this->memcache->get($cacheKey);
		}else{
			$q = "SELECT order_id FROM ".$this->ecs->table('order_info')." WHERE order_sn='".$orderSn."'";
			$res = $this->db->getAll($q);
			if(count($res)<=0) return false;
			$this->memcache->set($cacheKey,$res[0]['order_id']);
			return $res[0]['order_id'];
		}
	}

	public function getPOIdBySn($poSn){
		$cacheKey=$this->genCacheKey(['po','oid',$poSn]);
		if($this->memcache->get($cacheKey)!=false){
			return $this->memcache->get($cacheKey);
		}else{
			$q = "SELECT order_id FROM ".$this->ecs->table('erp_order')." WHERE order_sn='".$poSn."'";
			$res = $this->db->getAll($q);
			if(count($res)<=0) return false;
			$this->memcache->set($cacheKey,$res[0]['order_id']);
			return $res[0]['order_id'];
		}
	}

    public function calculateOrderFee($order)
    {
        if(!$order) return false;
        $id = $order['order_id'];
        
        $i_sql = "SELECT integral FROM " . $this->ecs->table('order_info') ." WHERE order_id = ". $id ." LIMIT 1";
        $order_integral = $this->db->getOne($i_sql);
        
        $goods_sql = "SELECT SUM(og.goods_number * og.goods_price) as goods_amount, SUM(og.goods_number * g.integral) as goods_max_integral, SUM(og.goods_number * g.market_price) as market_price ".
        "FROM " . $this->ecs->table('order_goods') . " as og " .
        "LEFT JOIN " . $this->ecs->table('goods') . " as g ON g.goods_id = og.goods_id " .
        "WHERE og.order_id = '" . $id . "' ";
        $goods = $this->db->getRow($goods_sql);
        $order['goods_amount'] = $goods['goods_amount'];
        $order['goods_max_integral'] = $goods['goods_max_integral'];
        
        /* Step 1: Calculate the inital order amount */
        $order_amount = $order['goods_amount']
                        + $order['tax']
                        + $order['pack_fee']
                        + $order['card_fee'];
                        
        /* Step 2: Calculate discount */
		if($order['discount'] > $order['goods_amount']) $order['discount'] = $order['goods_amount'];
        $order_amount -= $order['discount'];
        
        /* Step 3: Calculate integral */
		$user = user_info($order['user_id']);
        $user_max_point = isset($user['pay_points']) ? $user['pay_points'] + $order_integral : 0 + $order_integral;
        $order_amount -= $order['coupon'];
        $integral   = $order['integral'];

        $o_shipping_fee = isset($order['shipping_fee']) ? $order['shipping_fee'] : 0;
        $o_cn_ship_tax = isset($order['cn_ship_tax']) ? $order['cn_ship_tax'] : 0;
        $o_money_paid   = isset($order['money_paid']) ? $order['money_paid'] : 0;
        $old_max_integral_money = $order_amount - ($o_money_paid - $o_shipping_fee - $o_cn_ship_tax);

        $max_integral = min(integral_of_value($order['goods_max_integral']), integral_of_value($order_amount), $integral, $user_max_point, integral_of_value($old_max_integral_money));
        // $max_integral = imperfect_integral($max_integral);
        $max_integral_amount = value_of_integral($max_integral);

        if($max_integral_amount < 0) $max_integral_amount = 0;
        
        $order_amount -= $max_integral_amount;
        $order['integral_money'] = $max_integral_amount;
        $order['integral'] = integral_of_value($max_integral_amount);
        $order['total_amount'] = $order['goods_amount'] + $order['tax'] + $order['pack_fee'] + $order['card_fee'] - $order['discount'] - $order['integral_money'] - $order['coupon'];

        if($order['total_amount'] < 0 && $order['discount'] > 0)
        {
            $order_amount      += $order['discount'];
            $order['discount'] += $order['total_amount'];
            $order_amount      -= $order['discount'];
            $order['total_amount'] = $order['goods_amount'] + $order['tax'] + $order['pack_fee'] + $order['card_fee'] - $order['discount'] - $order['integral_money'] - $order['coupon'];
        }
        /* Step 4: Calculate pay fee */
        $pay_fee       = pay_fee($order['pay_id'], $order['total_amount']);
        $order_amount += $pay_fee;
        $order['pay_fee'] = number_format($pay_fee, 2, '.', '');
        
        /* Step 5: Calculate shipping fee */
        $order_amount  += floatval($order['shipping_fee']) + $order['insure_fee'] + $order['cod_fee'] - $order['money_paid'];

        /* Step 5: Calculate China cross-border tax */
        if (isset($order['platform'])){
            if ($order['add_time'] > 1562700715 && ($order['platform'] == "web" || $order['platform'] == "mobile")){
                $order_amount += $order['cn_ship_tax'];
            }
        }
        $order['order_amount'] = $order_amount;
        $order['formated_order_amount'] = price_format($order['order_amount'], false);
        $order['formated_total_amount'] = price_format($order['total_amount'], false);
        
        return $order;
    }
    
    /* This function will recalculate cart/order fee
     * Calculate order fee flow:
     * * Step 1 : Initialize extension_code, group_buy (Unused in YOHO).
     * * Step 2 : Initialize order.
     * * Step 3 : Calculate goods total price.
     * * Step 4 : Calculate order discount.
     * * Step 5 : Calculate order tax (Unused in YOHO).
     * * Step 6 : Calculate order pack fee(包裝) (Unused in YOHO).
     * * Step 7 : Calculate order card fee(賀卡) (Unused in YOHO).
     * * Step 8 : Calculate order bonus(紅包) (Unused in YOHO).
     * * Step 9 : Calculate order bonus kill(線下紅包) (Unused in YOHO).
     * * Step 10: Calculate Coupon.
     * * Step 11: Calculate goods can use the total amount of bonus paid.
     * * Step 12: Calculate the maximum amount of bonus and integral that can be paid is the total amount of goods.
     * * Step 13: Calculate the initial order amount.
     * * Step 14: Calculate surplus(餘額) (Unused in YOHO).
     * * Step 15: Calculate integral(積分/user pay_point).
     * * Step 16: Save order flow into session.
     * * Step 17: Calculate pay fee(支付折扣).
     * * Step 18: Calculate shipping fee(運費).
     * * Step 19: Calculate user points and bonus (bonus is unused in YOHO).
     * * Step 20: Return order fee.
    */
    function order_fee_flow($order, $goods, $consignee, $platform = null, $discount_plus='1', $is_batch = false)
    {
        // If undefind platform, return
        if(!in_array($platform, [self::ORDER_FEE_PLATFORM_WEB, self::ORDER_FEE_PLATFORM_POS, self::ORDER_FEE_PLATFORM_CMS]))
        return false;
        
        /* Step 1 : Initialize extension_code, group_buy (Unused in YOHO). */
        if (!isset($order['extension_code']))
        {
            $order['extension_code'] = '';
        }
        if ($order['extension_code'] == 'group_buy')
        {
            $group_buy = group_buy_info($order['extension_id']);
        }
        
        /* Step 2 : Initialize order. */
        $total  = array('real_goods_count'   => 0,
                        'gift_amount'        => 0,
                        'goods_price'        => 0,
                        'market_price'       => 0,
                        'discount'           => 0,
                        'pack_fee'           => 0,
                        'card_fee'           => 0,
                        'shipping_fee'       => 0,
                        'cn_ship_tax'        => 0,
                        'shipping_insure'    => 0,
                        'integral_money'     => 0,
                        'bonus'              => 0,
                        'coupon'             => 0,
                        'surplus'            => 0,
                        'cp_price'           => 0,
                        'cod_fee'            => 0,
                        'pay_fee'            => 0,
                        'tax'                => 0,
                        'goods_max_integral' => 0,
                        'width'              => 0,
                        'length'             => 0,
                        'height'             => 0,
                    );
        $weight = 0;
        
        /* Step 3 : Calculate goods total price. */
        foreach ($goods AS $val)
        {
            // Count real goods
            if ($val['is_real'])
            {
                $total['real_goods_count']++;
            }
            if(isset($val['custom_price'])){
                $total['goods_price']  += $val['custom_price'] * $val['goods_number'];
            } else {
                $total['goods_price']  += $val['goods_price'] * $val['goods_number'];
            }
            if(isset($val['width'])){
                $total['width']  += $val['width'] * $val['goods_number'];
            }
            if(isset($val['length'])){
                $total['length']  += $val['length'] * $val['goods_number'];
            }
            if(isset($val['height'])){
                $total['height']  += $val['height'] * $val['goods_number'];
            }
            $total['market_price'] += $val['market_price'] * $val['goods_number'];
            $total['goods_max_integral'] += $val['max_integral'];
            // Calculate the item cost
            $sql = "SELECT cost FROM " . $GLOBALS['ecs']->table('goods') . " WHERE goods_id = '" . $val['goods_id'] . "'";
            $cp_price = $GLOBALS['db']->getOne($sql);
            $total['cp_price'] += round($cp_price * $val['goods_number'], 2);
        }
        $total['goods_max_integral_amount'] = value_of_integral($total['goods_max_integral']);
        $total['saving']    = $total['market_price'] - $total['goods_price'];
        $total['save_rate'] = $total['market_price'] ? round($total['saving'] * 100 / $total['market_price']) . '%' : 0;
        
        $total['goods_price_formated']  = price_format($total['goods_price'], false);
        $total['market_price_formated'] = price_format($total['market_price'], false);
        $total['saving_formated']       = price_format($total['saving'], false);

        /* Step 4 : Calculate order discount. */
        if ($order['extension_code'] != 'group_buy')
        {
            if($platform == self::ORDER_FEE_PLATFORM_WEB) $discount = compute_discount();
            if($platform == self::ORDER_FEE_PLATFORM_POS){
                if(in_array($order['order_type'], [self::DB_ORDER_TYPE_SALESAGENT, self::DB_ORDER_TYPE_WHOLESALE])) $discount['discount'] = $order['discount'];
                else $discount = compute_discount_1();
            }
            if($platform == self::ORDER_FEE_PLATFORM_CMS) $discount['discount'] = $order['discount'];
            
            $total['discount'] = $discount['discount'];
            if ($total['discount'] > $total['goods_price'])
            {
                $total['discount'] = $total['goods_price'];
            }
        }
        $total['discount_formated'] = price_format($total['discount'], false);
        
        /* Step 4.1 : Calculate YOHO POS discount. */
        if($platform == self::ORDER_FEE_PLATFORM_POS)
        {
            if($discount_plus < 1 && $discount_plus > 0)
            {
                $total['goods_price_discounted'] = $total['goods_price'] * $discount_plus;
            }
            elseif($discount_plus > 1 || $discount_plus < 0)
            {
                $total['goods_price_discounted'] = $total['goods_price'] + $discount_plus;
            }
            else
            {
                $total['goods_price_discounted'] = $total['goods_price'];
            }
            $total['discount'] += ($total['goods_price'] - $total['goods_price_discounted']);
            $total['discount_formated'] = price_format($total['discount'], false);
        }

        /* Step 5 : Calculate order tax (Unused in YOHO). */
        if (!empty($order['need_inv']) && $order['inv_type'] != '')
        {
            /* 查税率 */
            $rate = 0;
            foreach ($GLOBALS['_CFG']['invoice_type']['type'] as $key => $type)
            {
                if ($type == $order['inv_type'])
                {
                    $rate = floatval($GLOBALS['_CFG']['invoice_type']['rate'][$key]) / 100;
                    break;
                }
            }
            if ($rate > 0)
            {
                $total['tax'] = $rate * $total['goods_price'];
            }
        }
        $total['tax_formated'] = price_format($total['tax'], false);
        
        /* Step 6 : Calculate order pack fee(包裝) (Unused in YOHO). */
        if (!empty($order['pack_id']))
        {
            $total['pack_fee']      = pack_fee($order['pack_id'], $total['goods_price']);
        }
        $total['pack_fee_formated'] = price_format($total['pack_fee'], false);

        /* Step 7 : Calculate order card fee(賀卡) (Unused in YOHO). */
        if (!empty($order['card_id']))
        {
            $total['card_fee']      = card_fee($order['card_id'], $total['goods_price']);
        }
        $total['card_fee_formated'] = price_format($total['card_fee'], false);

        /* Step 8 : Calculate order bonus(紅包) (Unused in YOHO). */
        if (!empty($order['bonus_id']))
        {
            $bonus          = bonus_info($order['bonus_id']);
            $total['bonus'] = $bonus['type_money'];
        }
        $total['bonus_formated'] = price_format($total['bonus'], false);

        /* Step 9 : Calculate order bonus kill(線下紅包) (Unused in YOHO). */
        if (!empty($order['bonus_kill']))
        {
            $bonus                        = bonus_info(0,$order['bonus_kill']);
            $total['bonus_kill']          = $order['bonus_kill'];
            $total['bonus_kill_formated'] = price_format($total['bonus_kill'], false);
        }
        
        /* Step 10: Calculate Coupon. */
        $coupon_info = $this->calculateFlowCoupon($order['coupon_id'], $total['goods_price'], $goods);
    
        $coupons_flat          = $coupon_info['coupons_flat'];
        $coupons_percent       = $coupon_info['coupons_percent'];
        $coupons_free_shipping = $coupon_info['coupons_free_shipping'];
        $total['coupon']       = $coupon_info['coupon'];
        $total['coupon_formated'] = price_format($total['coupon'], false);
        
        /* Step 11: Calculate goods can use the total amount of bonus paid. */
        if($platform == self::ORDER_FEE_PLATFORM_WEB) $bonus_amount = compute_discount_amount();
        if($platform == self::ORDER_FEE_PLATFORM_POS && $is_batch == false) $bonus_amount = compute_discount_amount_1();
        if($platform == self::ORDER_FEE_PLATFORM_POS && $is_batch == true) $bonus_amount = 0; // Using for sa batch order.
        if($platform == self::ORDER_FEE_PLATFORM_CMS) $bonus_amount = $this->compute_discount_amount_cms($order['order_id'], $order['user_id']);
        /* Step 12: Calculate the maximum amount of bonus and integral that can be paid is the total amount of goods. */
        $max_amount = $total['goods_price'] == 0 ? $total['goods_price'] : max(0, $total['goods_price'] - $bonus_amount);

        /* Step 13: Calculate the initial order amount. */
        // If it's a group buy order, order amount = total goods price.
        if ($order['extension_code'] == 'group_buy' && $group_buy['deposit'] > 0)
        {
            $total['amount'] = $total['goods_price'];
        }
        else // else, calculate the initial order amount.
        {
            // Calculate the initial order amount.(Without shipping in this step.) Edited:Anthony@YOHO 2017-06-09
            // $total['amount'] = $total['goods_price'] - $total['discount'] + $total['tax'] + $total['pack_fee'] + $total['card_fee'] +
            // $total['shipping_fee'] + $total['shipping_insure'] + $total['cod_fee'];
            //** YOHO: $total['amount'] = goods_price - discount
            $total['amount']  = $total['goods_price'] - $total['discount'] + $total['tax'] + $total['pack_fee'] + $total['card_fee'];
            // if(isset($order['money_paid']) && $order['money_paid'] > 0)$total['amount'] -= $order['money_paid'];
            // Calculate the initial order amount without shipping.
            $total['amount3'] = $total['goods_price'] - $total['discount'] + $total['tax'] + $total['pack_fee'] + $total['card_fee'];
            
            // Deduct bonus amount
            $use_bonus = min($total['bonus'], $max_amount);
            
            // Deduct bonus kill amount
            if(isset($total['bonus_kill']))
            {
                $use_bonus_kill       = min($total['bonus_kill'], $max_amount);
                $total['bonus_kill']  = $use_bonus_kill;
                $total['amount']     -= number_format($total['bonus_kill'], 2, '.', '');
                $max_amount          -= $use_bonus_kill; // Calculate max amount bonus can use
            }
            $total['bonus']          = $use_bonus;
            $total['bonus_formated'] = price_format($total['bonus'], false);
            $total['amount']        -= $use_bonus; // Update new order amount
            $max_amount             -= $use_bonus; // Calculate max amount coupon can use
            
            // Deduct coupon amount
            $use_coupon               = min($total['coupon'], $max_amount); // Actual coupon amount used
            $total['coupon']          = $use_coupon;
            $total['coupon_formated'] = price_format($total['coupon'], false);
            $total['amount']         -= $use_coupon; // Update new order amount
            $max_amount              -= $use_coupon; // Calculate max amount integral can use
        }
        
        /* Step 14: Calculate surplus(餘額) (Unused in YOHO). */
        $order['surplus'] = $order['surplus'] > 0 ? $order['surplus'] : 0;
        if ($total['amount'] > 0)
        {
            if (isset($order['surplus']) && $order['surplus'] > $total['amount'])
            {
                $order['surplus'] = $total['amount'];
                $total['amount'] -= floatval($order['surplus']);
            }
            else
            {
                $total['amount'] -= floatval($order['surplus']);
            }
        }
        else
        {
            $order['surplus'] = 0;
            $total['amount'] -= floatval($order['surplus']);
        }
        $total['surplus'] = $order['surplus'];
        $total['surplus_formated'] = price_format($order['surplus'], false);
        
        /* Step 15: Calculate integral(積分/user pay_point). */
        $max_amount = min($total['goods_max_integral_amount'], $max_amount);
        $order['integral'] = $order['integral'] > 0 ? $order['integral'] : 0;
        //** $total['amount'] = goods_price - discount - tax + pack_fee + card_fee - bonus_kill(if have) - bonus - coupon - surplus
        //** YOHO: $total['amount'] = goods_price - discount - coupon
        if ($total['amount'] > 0 && $max_amount > 0 && $order['integral'] > 0)
        {
            $integral_money = value_of_integral($order['integral']);

            $o_shipping_fee = isset($order['shipping_fee']) ? $order['shipping_fee'] : 0;
            $o_money_paid   = isset($order['money_paid']) ? $order['money_paid'] : 0;
            $old_max_integral_money = $total['amount']- ($o_money_paid - $o_shipping_fee);
            // Use integral pay
            $use_integral            = min($total['amount'], $max_amount, $integral_money, $old_max_integral_money);
            if($use_integral < 0) $use_integral = 0;
            $total['amount']        -= $use_integral;
            $total['integral_money'] = $use_integral;
            $order['integral']       = integral_of_value($use_integral);
            
        }
        else
        {
            $total['integral_money'] = 0;
            $order['integral']       = 0;
        }
        $total['max_amount'] = $max_amount;
        $total['max_integral'] = integral_of_value($max_amount);
        $total['integral'] = $order['integral'];
        $total['integral_formated'] = price_format($total['integral_money'], false);
        
        /* Step 16: Save order flow into session.(For pos, web) */
        if(in_array($platform, [self::ORDER_FEE_PLATFORM_WEB, self::ORDER_FEE_PLATFORM_POS]))
        {
            $_SESSION['flow_order'] = $order;
            $se_flow_type = isset($_SESSION['flow_type']) ? $_SESSION['flow_type'] : '';
        } else {
            $se_flow_type = '';
        }
        
        /* Step 17: Calculate pay fee(支付折扣). */
        // If have order pay_id, have real goods, calculate pay fee.
        if (!empty($order['pay_id']) && ($total['real_goods_count'] > 0 || $se_flow_type != CART_EXCHANGE_GOODS))
        {
            // if(isset($order['money_paid']) && $order['money_paid'] > 0)$total['amount'] += $order['money_paid'];
            //** $total['amount'] = goods_price - discount - tax + pack_fee + card_fee - bonus_kill(if have) - bonus - coupon - surplus - integral_money
            //** YOHO: $total['amount'] = goods_price - discount - coupon - integral_money
			$amount = $total['amount'] < 0 ? 0 : $total['amount'];
            $total['pay_fee']      = pay_fee($order['pay_id'], $amount);
        }
        $total['pay_fee_formated']      = price_format($total['pay_fee'], false);
        $total['pay_discount_formated'] = price_format(-$total['pay_fee'], false);
        $total['amount']               += $total['pay_fee']; // Order amount + pay fee
        
        /* Step 18: Calculate shipping fee(運費). */
        $shipping_cod_fee = NULL;
        // If order have shipping id, have real goods, and order type is not sale agent, calculate shipping fee.
        if ($order['shipping_id'] > 0 && $total['real_goods_count'] > 0 && !in_array($order['order_type'], [self::DB_ORDER_TYPE_SALESAGENT]))
        {
            if(empty($order['shipping_area_id'])) {
                $region['country']  = $consignee['country'];
                $region['province'] = $consignee['province'];
                $region['city']     = $consignee['city'];
                $region['district'] = $consignee['district'];
                $shipping_info = shipping_area_info($order['shipping_id'], $region);
            } else {
                $shipping_info = shipping_area_info($order['shipping_id'], [], $order['shipping_area_id']);
            }

            if (!empty($shipping_info))
            {
                // Process free shipping coupons
                global $coupon_free_shipping_goods;
                $coupon_free_shipping_goods = array();
                if (!empty($order['coupon_id']))
                {
                    foreach ($coupons_free_shipping as $coupon)
                    {
                        if ($coupon['cartwide']) // percentage apply to all products in cart
                        {
                            foreach ($goods AS $g)
                            {
                                if(!in_array($g['goods_id'], $coupon['exclude_goods_list']))
                                $coupon_free_shipping_goods[] = $g['goods_id'];
                            }
                        }
                        else
                        {
                            $coupon_goods = array_reduce($coupon['goods_list'], 'array_merge', array());
                            foreach ($goods AS $g)
                            {
                                if (in_array($g['goods_id'], $coupon_goods))
                                {
                                    $coupon_free_shipping_goods[] = $g['goods_id'];
                                }
                            }
                        }
                    }
                    $coupon_free_shipping_goods = array_unique($coupon_free_shipping_goods);
                }
                // Check country can free shipping goods?
                $addressController = new AddressController();
                $free_region = $addressController->get_free_calculate_region();
                $cal_free_shipping = false;
                if (!empty($free_region['country'] && in_array($consignee['country'], $free_region['country']))) $cal_free_shipping = true;

                if ($order['extension_code'] == 'group_buy')
                {
                    if($platform == self::ORDER_FEE_PLATFORM_WEB) $weight_price = cart_weight_price(CART_GROUP_BUY_GOODS, $cal_free_shipping);
                    if($platform == self::ORDER_FEE_PLATFORM_POS) $weight_price = cart_weight_price_1(CART_GROUP_BUY_GOODS, $cal_free_shipping);
                    if($platform == self::ORDER_FEE_PLATFORM_CMS) $weight_price = order_weight_price($order['order_id'], $cal_free_shipping);
                }
                else
                {
                    if($platform == self::ORDER_FEE_PLATFORM_WEB) $weight_price = cart_weight_price(CART_GENERAL_GOODS, $cal_free_shipping);
                    if($platform == self::ORDER_FEE_PLATFORM_POS) $weight_price = cart_weight_price_1(CART_GENERAL_GOODS, $cal_free_shipping);
                    if($platform == self::ORDER_FEE_PLATFORM_CMS) $weight_price = order_weight_price($order['order_id'], $cal_free_shipping);
                }
                
                if(in_array($platform, [self::ORDER_FEE_PLATFORM_WEB, self::ORDER_FEE_PLATFORM_POS]))
                {
                    if($platform == self::ORDER_FEE_PLATFORM_WEB) $cart_db = $GLOBALS['ecs']->table('cart');
                    if($platform == self::ORDER_FEE_PLATFORM_POS) $cart_db = $GLOBALS['ecs']->table('cart1');

                    // 查看購物車中是否全為免運費商品，若是則把運費賦為零 (如果全免運費, 贈品不計算運費)
                    $sql = 'SELECT count(*) FROM ' . $cart_db . " WHERE  `session_id` = '" . SESS_ID. "' AND `is_shipping` = 0 AND is_gift = 0";
                    $shipping_count = $GLOBALS['db']->getOne($sql);

                    if ($shipping_count == 0 && $weight_price['free_shipping'] == 1 && $cal_free_shipping)
                    {
                        $total['shipping_fee'] = 0;
                    }
                    else
                    {
                        $total['shipping_fee'] = shipping_fee($shipping_info['shipping_code'], $shipping_info['configure'], $weight_price['weight'], $weight_price['amount'], $weight_price['number'], $weight_price['price']);
                    }
                }
                if ($platform == self::ORDER_FEE_PLATFORM_CMS)
                {
                    $total['shipping_fee'] = shipping_fee($shipping_info['shipping_code'],
                    $shipping_info['configure'], $weight_price['weight'], $weight_price['amount'], $weight_price['number'], $weight_price['price']);
                }
                if (!empty($order['need_insure']) && $shipping_info['insure'] > 0)
                {
                    $total['shipping_insure'] = shipping_insure_fee($shipping_info['shipping_code'], $total['goods_price'], $shipping_info['insure']);
                }
                else
                {
                    $total['shipping_insure'] = 0;
                }

                if ($shipping_info['support_cod'])
                {
                    $shipping_cod_fee = $shipping_info['pay_fee'];
                    $total['cod_fee'] = $shipping_cod_fee;
                }
                // If user wants to pay fee on delivery, set shipping_fee to 0
                // but if the user selected some product with its own shipping charge, we have to include them back
                if ($order['shipping_fod'])
                {
                    $total['shipping_fee'] = 0 + $weight_price['price'];
                }
            }
        }
        $total['shipping_fee_formated']    = price_format($total['shipping_fee'], false);
        $total['shipping_insure_formated'] = price_format($total['shipping_insure'], false);
        // Order amount calculate by shipping.
        //** $total['amount'] = goods_price - discount - tax + pack_fee + card_fee - bonus_kill(if have) - bonus - coupon - surplus - integral_money + pay_fee
        //** YOHO: $total['amount'] = goods_price - discount - coupon - integral_money + pay_fee
        $total['amount'] += $total['shipping_fee'] + $total['shipping_insure'] + $total['cod_fee'];
        if(isset($order['money_paid']) && $order['money_paid'] > 0)$total['amount'] -= $order['money_paid'];
        // If refund amount > money_paid ,recalculate discount and coupon
        if($total['amount'] < 0 && (isset($order['money_paid']) && $order['money_paid'] > 0))
        {
            $need_refund = $order['money_paid'] + $total['amount'];
            if($total['discount'] > 0 && $need_refund < 0 && $total['discount'] > $need_refund)
            {
                $total['amount']   += $total['discount'];
                $total['discount'] += $need_refund;
                $total['amount']   -= $total['discount'];
                $need_refund = $order['money_paid'] + $total['amount'];
            }
            if($total['coupon'] > 0 && $need_refund < 0 && $total['coupon'] > $need_refund)
            {
                $total['amount']   += $total['coupon'];
                $total['coupon']   += $need_refund;
                $total['amount']   -= $total['coupon'];
            }
        }
        /* Step 19 : Calculate China Cross-Border E-Commerce Tax. */
        if ($platform == 'cms'){
            $platform_check = $order['platform'];
        } else {
            $platform_check = $platform;
        }

        if (!$order['add_time']){
            $add_time = 1562700716;
        } else {
            $add_time = $order['add_time'];
        }

        if (($add_time > 1562700715) && ($platform_check == 'web' || $platform_check == 'mobile') && $consignee['country'] == 3436 && ($order['shipping_id'] == 3 || $order['shipping_name'] == '速遞')){
            $total['cn_ship_tax'] = ceil(($total['goods_price'] * ((self::ORDER_CN_SHIP_TAX) / 100)));
            $total['amount']  += $total['cn_ship_tax'];
        } else {
            $total['cn_ship_tax'] = 0;
        }
        $total['cn_ship_tax_formated'] = price_format($total['cn_ship_tax'], false);

        // Formated amount.
        $total['amount_formated']  = price_format($total['amount'], false);
        $total['amount_formated3']  = price_format($total['amount3'], false);
        $total['weight']           = $weight_price['weight'];

        /* Step 20: Calculate user points and bouns (bouns is unused in YOHO). */
        if ($order['extension_code'] == 'group_buy')
        {
            $total['will_get_integral'] = $group_buy['gift_integral'];
        }
        elseif ($order['extension_code'] == 'exchange_goods')
        {
            $total['will_get_integral'] = 0;
        }
        elseif (in_array($order['order_type'], [self::DB_ORDER_TYPE_SALESAGENT]) && $is_batch)
        {
            $total['will_get_integral'] = 0;
        }
        else
        {
            if($platform == self::ORDER_FEE_PLATFORM_WEB) $total['will_get_integral'] = get_give_integral($goods);
            if($platform == self::ORDER_FEE_PLATFORM_POS) $total['will_get_integral'] = get_give_integral_1($goods);
            if($platform == self::ORDER_FEE_PLATFORM_CMS) $total['will_get_integral'] = $this->get_order_give_integral($goods, $order['order_id']);
        }
        $total['will_get_bonus']        = $order['extension_code'] == 'exchange_goods' ? 0 : price_format(get_total_bonus(), false);
        $total['formated_goods_price']  = price_format($total['goods_price'], false);
        $total['formated_market_price'] = price_format($total['market_price'], false);
        $total['formated_saving']       = price_format($total['saving'], false);
        // Formated amount.
        $total['amount_formated']  = price_format($total['amount'], false);
        $total['amount_formated2'] = price_format($total['goods_price'], false); //sbsn ly 0323 更改显示总价的算法
        $total['amount_formated3'] = price_format($total['amount3'], false); //sbsn ly 0723 更改显示总价的算 不包括邮费

        if ($order['extension_code'] == 'exchange_goods')
        {
            if($platform == self::ORDER_FEE_PLATFORM_WEB)
            {
                $sql = 'SELECT SUM(eg.exchange_integral) '.
                       'FROM ' . $GLOBALS['ecs']->table('cart') . ' AS c,' . $GLOBALS['ecs']->table('exchange_goods') . 'AS eg '.
                       "WHERE c.goods_id = eg.goods_id AND c.session_id= '" . SESS_ID . "' " .
                       " AND c.rec_type = '" . CART_EXCHANGE_GOODS . "' " .
                       ' AND c.is_gift = 0 AND c.goods_id > 0 ' .
                       'GROUP BY eg.goods_id';
            }
            if($platform == self::ORDER_FEE_PLATFORM_POS)
            {
                $sql = 'SELECT SUM(eg.exchange_integral) '.
                       'FROM ' . $GLOBALS['ecs']->table('cart1') . ' AS c,' . $GLOBALS['ecs']->table('exchange_goods') . 'AS eg '.
                       "WHERE c.goods_id = eg.goods_id AND c.session_id= '" . SESS_ID . "' " .
                       " AND c.rec_type = '" . CART_EXCHANGE_GOODS . "' " .
                       ' AND c.is_gift = 0 AND c.goods_id > 0 ' .
                       'GROUP BY eg.goods_id';
            }
            if($platform == self::ORDER_FEE_PLATFORM_CMS)
            {
                $sql = 'SELECT SUM(eg.exchange_integral) '.
                       'FROM ' . $GLOBALS['ecs']->table('order_goods') . ' AS c,' . $GLOBALS['ecs']->table('exchange_goods') . 'AS eg '.
                       "WHERE c.goods_id = eg.goods_id AND c.order_id= '" . $order['order_id'] . "' " .
                       ' AND c.is_gift = 0 AND c.goods_id > 0 ' .
                       'GROUP BY eg.goods_id';
            }
            
            $exchange_integral = $GLOBALS['db']->getOne($sql);
            $total['exchange_integral'] = $exchange_integral;
        }
        
        /* Step 21: Return order fee. */
        return $total;
        
    }
    
    function calculateFlowCoupon($coupon_ids, $goods_price, $goods)
    {
        global $db, $ecs;
        // initial coupon value
        $total_coupon = 0;
        $coupons_flat          = array();
        $coupons_percent       = array();
        $coupons_free_shipping = array();

        if (!empty($coupon_ids))
        {
            // Loop order coupon id
            foreach ($coupon_ids as $coupon_id)
            {
                // Get coupon info
                $coupon = coupon_info($coupon_id);
                if ($coupon)
                {
                    $coupon_goods = array_reduce($coupon['goods_list'], 'array_merge', array());
                    $exclude_goods_list = [];
                    foreach ($goods AS $g)
                    {
                        if (empty($g['brand_id']) || empty($g['cat_id'])) {
                            $g_data = $db->getRow("SELECT brand_id, cat_id FROM ".$ecs->table('goods')." WHERE goods_id = ".$g['goods_id']);
                            $g['brand_id'] = $g_data['brand_id'];
                            $g['cat_id']   = $g_data['cat_id'];
                        }

                        if(empty($coupon['brand_list']) && in_array($g['cat_id'], $coupon['cat_list'])) { /* Only have cat list */
                            $coupon_goods[] = $g['goods_id'];
                        }else if(empty($coupon['cat_list']) && in_array($g['brand_id'], $coupon['brand_list'])) {/* Only have brand list */
                            $coupon_goods[] = $g['goods_id'];
                        }else if(in_array($g['cat_id'], $coupon['cat_list']) && in_array($g['brand_id'], $coupon['brand_list'])) {/* have brand list and cat list: must match 2 list  */
                            $coupon_goods[] = $g['goods_id'];
                        }
                        if(in_array($g['cat_id'], $coupon['exclude_cat_list']) && ($key = array_search($g['goods_id'], $coupon_goods)) !== false) {
                            unset($coupon_goods[$key]);
                            $exclude_goods_list[] = $g['goods_id'];
                        }
                        if(in_array($g['brand_id'], $coupon['exclude_brand_list']) && ($key = array_search($g['goods_id'], $coupon_goods)) !== false) {
                            unset($coupon_goods[$key]);
                            $exclude_goods_list[] = $g['goods_id'];
                        }
                    }
                    $coupon['goods_list'] = [];
                    $coupon['exclude_goods_list'] = $exclude_goods_list;
                    if (count($coupon_goods)>0) {
                        $coupon['goods_list'][] = $coupon_goods;
                    }
                    // If coupon is deduct amount.
                    if ($coupon['coupon_amount'] > 0)
                    {
                        $coupons_flat[] = $coupon;
                    }
                    // If coupon is a percent coupon.
                    elseif ($coupon['coupon_discount'] > 0)
                    {
                        $coupons_percent[] = $coupon;
                    }
                    // If coupon is free shipping coupon.
                    if ($coupon['free_shipping'])
                    {
                        $coupons_free_shipping[] = $coupon;
                    }
                }
            }
            // Process flat discount first, then percentage discount
            foreach ($coupons_flat as $coupon)
            {
                $total_coupon += $coupon['coupon_amount'];
            }
            foreach ($coupons_percent as $coupon)
            {
                if ($coupon['cartwide']) // percentage apply to all products in cart
                {
                    $goods_price = 0;
                    foreach ($goods AS $g)
                    {
                        if (!in_array($g['goods_id'], $exclude_goods_list))
                        {
                            $goods_price += $g['goods_price'] * $g['goods_number'];
                        }
                    }
                    $total_coupon += floor(($coupon['coupon_discount'] / 100) * $goods_price);
                }
                else
                {
                    $coupon_amount = 0;
                    $coupon_goods = array_reduce($coupon['goods_list'], 'array_merge', array());
                    foreach ($goods AS $g)
                    {
                        if (in_array($g['goods_id'], $coupon_goods))
                        {
                            $coupon_amount += floor(($coupon['coupon_discount'] / 100) * $g['goods_price'] * $g['goods_number']);
                        }
                    }
                    $total_coupon += $coupon_amount;
                }
            }
        }
        
        return array(
            'coupon'                => $total_coupon,
            'coupons_flat'          => $coupons_flat,
            'coupons_percent'       => $coupons_percent,
            'coupons_free_shipping' => $coupons_free_shipping
        );
    }

    /**
     * 計算訂單中的商品能享受红包支付的总额
     * @return  float   享受红包支付的总额
     */
    function compute_discount_amount_cms($order_id, $user_id)
    {
        /* Get user rank */
        $u_sql ="SELECT user_rank FROM ".$GLOBALS['ecs']->table('users').
                " WHERE user_id = ".$user_id." LIMIT 1";
        $user_rank = $this->db->getOne($u_sql);
        
        /* 查询优惠活动 */
        $now = gmtime();
        $user_rank = ',' . $user_rank . ',';
        $sql = "SELECT *" .
                "FROM " . $GLOBALS['ecs']->table('favourable_activity') .
                " WHERE start_time <= '$now'" .
                " AND end_time >= '$now'" .
                " AND status = 1" .
                " AND CONCAT(',', user_rank, ',') LIKE '%" . $user_rank . "%'" .
                " AND act_type " . db_create_in(array(FAT_DISCOUNT, FAT_PRICE));
        $favourable_list = $GLOBALS['db']->getAll($sql);
        if (!$favourable_list)
        {
            return 0;
        }

        /* 查询购物车商品 */
        $sql = "SELECT c.goods_id, c.goods_price * c.goods_number AS subtotal, g.cat_id, g.brand_id " .
                "FROM " . $GLOBALS['ecs']->table('order_goods') . " AS c, " . $GLOBALS['ecs']->table('goods') . " AS g " .
                "WHERE c.goods_id = g.goods_id " .
                "AND c.order_id = '" . $order_id . "' " .
                "AND c.parent_id = 0 " .
                "AND c.is_gift = 0 ";
        $goods_list = $GLOBALS['db']->getAll($sql);
        if (!$goods_list)
        {
            return 0;
        }

        /* 初始化折扣 */
        $discount = 0;
        $favourable_name = array();

        /* 循环计算每个优惠活动的折扣 */
        foreach ($favourable_list as $favourable)
        {
            if(!empty($favourable['users_list'])){
                $users_list = json_decode($favourable['users_list']);

                if(!in_array($user_id, $users_list)){
                    continue;
                }
            }
            $total_amount = 0;
            if ($favourable['act_range'] == FAR_ALL)
            {
                foreach ($goods_list as $goods)
                {
                    $total_amount += $goods['subtotal'];
                }
            }
            elseif ($favourable['act_range'] == FAR_CATEGORY)
            {
                /* 找出分类id的子分类id */
                $id_list = array();
                $raw_id_list = explode(',', $favourable['act_range_ext']);
                foreach ($raw_id_list as $id)
                {
                    $id_list = array_merge($id_list, array_keys(cat_list($id, 0, false)));
                }
                $ids = join(',', array_unique($id_list));

                foreach ($goods_list as $goods)
                {
                    if (strpos(',' . $ids . ',', ',' . $goods['cat_id'] . ',') !== false)
                    {
                        $total_amount += $goods['subtotal'];
                    }
                }
            }
            elseif ($favourable['act_range'] == FAR_BRAND)
            {
                foreach ($goods_list as $goods)
                {
                    if (strpos(',' . $favourable['act_range_ext'] . ',', ',' . $goods['brand_id'] . ',') !== false)
                    {
                        $total_amount += $goods['subtotal'];
                    }
                }
            }
            elseif ($favourable['act_range'] == FAR_GOODS)
            {
                foreach ($goods_list as $goods)
                {
                    if (strpos(',' . $favourable['act_range_ext'] . ',', ',' . $goods['goods_id'] . ',') !== false)
                    {
                        $total_amount += $goods['subtotal'];
                    }
                }
            }
            else
            {
                continue;
            }
            if ($total_amount > 0 && $total_amount >= $favourable['min_amount'] && ($total_amount <= $favourable['max_amount'] || $favourable['max_amount'] == 0))
            {
                if ($favourable['act_type'] == FAT_DISCOUNT)
                {
                    $discount += $total_amount * (1 - $favourable['act_type_ext'] / 100);
                }
                elseif ($favourable['act_type'] == FAT_PRICE)
                {
                    $discount += $favourable['act_type_ext'];
                }
            }
        }

        return $discount;
    }
    
    function get_order_give_integral($goods, $order_id)
    {
        $sql = "SELECT SUM(c.goods_number * IF(g.give_integral > -1, g.give_integral, c.goods_price))" .
                "FROM " . $GLOBALS['ecs']->table('order_goods') . " AS c, " .
                          $GLOBALS['ecs']->table('goods') . " AS g " .
                "WHERE c.goods_id = g.goods_id " .
                "AND c.order_id = '" . $order_id . "' " .
                "AND c.goods_id > 0 " .
                "AND c.parent_id = 0 " .
                "AND c.is_gift = 0";

        return intval($GLOBALS['db']->getOne($sql));
    }

	public function getRepairOrders(){
		return $this->db->getAll("SELECT repair_id as order_id,repair_sn as order_sn,add_time as create_time,remark as description,1 as supplier_id FROM ".$this->ecs->table('repair_orders')." WHERE repair_order_status NOT IN (".self::DB_REPAIR_ORDER_STATUS_RESOLVED.") ORDER BY repair_id ASC ");
	}

	public function getRepairOrderByProductId($productId){
		return $this->db->getAll("SELECT * FROM ".$this->ecs->table('repair_orders')." WHERE goods_id=".$productId." ORDER by repair_id ASC");
	}

	public function updateRepairOrderStatus($repairId,$status){
		//TODO:: admin log
		if(!in_array($status, array_keys(self::DB_REPAIR_ORDER_STATUS)))
			return false;
		if($this->db->query("UPDATE ".$this->ecs->table('repair_orders')." SET repair_order_status=".$status." WHERE repair_id=".$repairId)){
			if(in_array($status,[self::DB_REPAIR_ORDER_STATUS_REPAIRED,self::DB_REPAIR_ORDER_STATUS_RESOLVED])){
				$order = $this->db->getAll("SELECT oi.*,ro.repair_sn FROM ".$this->ecs->table('order_info')." oi , ".$this->ecs->table('repair_orders')." ro WHERE ro.order_id=oi.order_id AND ro.repair_id=".$repairId);
				$order = $order[0];
				order_action($order['order_sn'], $order['order_status'], $order['shipping_status'], $order['pay_status'],'維修單'.self::DB_REPAIR_ORDER_STATUS[$status].'-<b>'.$order['repair_sn'].'</b>' , $_SESSION['admin_name']);
			}
			return true;
		}else{
			return false;
		}
    }

    function is_vip($user_id){
        $sql = "SELECT rank_points FROM " .$this->ecs->table('users'). " WHERE user_id = '$user_id'";
        $rank_points = $this->db->getOne($sql);
        $sql = "SELECT min_points FROM " .$this->ecs->table('user_rank'). " WHERE rank_id = '2'";
        $min_points = $this->db->getOne($sql);
        if($rank_points >= $min_points)
            return true;
        return false;
    }

    function is_vip_price($goods_id,$user_id,$goods_price){
        if($this->is_vip($user_id)){
            $sql = "SELECT user_price FROM " .$this->ecs->table('member_price').
                    " WHERE goods_id = '$goods_id' AND user_rank = '2' ";
            $user_price = $this->db->getOne($sql);
            if($user_price == $goods_price)
                return 1;
        }
        return 0;
    }

    public function get_order_tracking_info($order_sn){
        global $db, $ecs, $_LANG, $_CFG;
        require(ROOT_PATH . 'languages/' .$_CFG['lang']. '/yoho.php');
        $info_list = array();

        $do_sql = "SELECT invoice_no as invoice, order_id FROM ".$ecs->table('delivery_order')." WHERE order_sn = $order_sn ";
        $delivery_list = $db->getAll($do_sql);
        if($delivery_list){
            $logisticsController = new LogisticsController();
            foreach ($delivery_list as $key => $value) {
                $info = $logisticsController->get_tracking_status($value);
                $info['status_img'] = empty($info['status']) ? "" : $info['status'];
                $info['status'] = isset($_LANG[$info['status']])?$_LANG[$info['status']]:$info['status'];
                $info['status'] = empty($info['status']) ? "--" : $info['status'];
                $info_list[] = $info;
            }
        }
        return $info_list;
    }

    /**
     * 取得订单商品
     * @param   array     $order  订单数组
     * @return array
     */
    function get_order_goods($order)
    {
        global $db, $ecs, $_LANG, $_CFG;
		$goods_list = array();
        $goods_attr = array();

        $sql = "SELECT o.*, o.goods_attr, IFNULL(b.brand_name, '') AS brand_name " .
                "FROM " . $GLOBALS['ecs']->table('order_goods') . " AS o ".
                "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " AS g ON o.goods_id = g.goods_id " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('brand') . " AS b ON g.brand_id = b.brand_id " .
                "WHERE o.order_id = '$order[order_id]' ";
        //$res = $GLOBALS['db']->query($sql);
		$res = $db->getAll($sql);
        if(!isset($order['agency_id']))
        {
            $sql="select agency_id from ".$GLOBALS['ecs']->table('order_info')." where order_id='".$order['order_id']."'";
            $order['agency_id']=$GLOBALS['db']->getOne($sql);
        }

        $agency_id=$order['agency_id'];
		
		// get product id
		$prodIds = [];
		foreach ($res as $row)
        {
			$prodIds[] = $row['goods_id'];
		}
		
		$erpController = new ErpController();
        $orderController = new OrderController();
        $packageController = new PackageController();
		$goods_storage_wh = $erpController->getSellableProductsQtyByWarehouses($prodIds,null,false);
        $storage_warehouse_id = $orderController->getOrderWarehouseIdForReduceStock($order['order_id']);
        
		// $fls_warehouse_id = $erpController->getFlsWarehouseId();
		// if (isset($_REQUEST['is_fls']) && $_REQUEST['is_fls'] == 1) {
		// 	$storage_warehouse_id = $fls_warehouse_id;
		// }

        //while ($row = $GLOBALS['db']->fetchRow($res))
		foreach ($res as $row)
        {
            //库存
            $row['storage']=get_goods_stock($row['goods_id'],$row['goods_attr_id'],$agency_id);
			
			$row['storage_wh']= $goods_storage_wh[$row['goods_id']]; 
        	// check which warehouse to see/reduce
            $row['storage_warehouse_id'] = $storage_warehouse_id;

            // 虚拟商品支持
            if ($row['is_real'] == 0)
            {
                /* 取得语言项 */
                $filename = ROOT_PATH . 'plugins/' . $row['extension_code'] . '/languages/common_' . $GLOBALS['_CFG']['lang'] . '.php';
                if (file_exists($filename))
                {
                    include_once($filename);
                    if (!empty($GLOBALS['_LANG'][$row['extension_code'].'_link']))
                    {
                        $row['goods_name'] = $row['goods_name'] . sprintf($GLOBALS['_LANG'][$row['extension_code'].'_link'], $row['goods_id'], $order['order_sn']);
                    }
                }
            }

            $row['formated_subtotal']       = price_format($row['goods_price'] * $row['goods_number']);
            $row['formated_goods_price']    = price_format($row['goods_price']);

            $goods_attr[] = explode(' ', trim($row['goods_attr'])); //将商品属性拆分为一个数组

            if ($row['extension_code'] == 'package_buy')
            {
                $row['storage'] = '';
                $row['brand_name'] = '';
                $row['package_goods_list'] = $packageController->get_package_goods_list($row['goods_id']);
            }

            //处理货品id
            $row['product_id'] = empty($row['product_id']) ? 0 : $row['product_id'];

            if ($row['is_insurance'] == 1 && $row['insurance_of'] > 0){
                $insurance_of_name_sn = $GLOBALS['db']->getRow("SELECT goods_name, goods_sn FROM ".$GLOBALS['ecs']->table('goods')." WHERE goods_id = '".$row['insurance_of']."' AND goods_id = '".$row['insurance_of']."' AND is_insurance = 0 ");
                if (!empty($insurance_of_name_sn)){
                    $row['insurance_of_sn'] = $insurance_of_name_sn['goods_sn'];
                    $row['insurance_of_name'] = $insurance_of_name_sn['goods_name'];
                }
            }

            $goods_list[] = $row;
        }

        $attr = array();
        $arr  = array();
        foreach ($goods_attr AS $index => $array_val)
        {
            foreach ($array_val AS $value)
            {
                $arr = explode(':', $value);//以 : 号将属性拆开
                $attr[$index][] =  @array('name' => $arr[0], 'value' => $arr[1]);
            }
        }

        return array('goods_list' => $goods_list, 'attr' => $attr);
    }

    /**
     * 超级礼包发货数处理
     * @param   array   超级礼包商品列表
     * @param   int     发货数量
     * @param   int     订单ID
     * @param   varchar 虚拟代码
     * @param   int     礼包ID
     * @return  array   格式化结果
     */
    function package_goods(&$package_goods, $goods_number, $order_id, $extension_code, $package_id)
    {
        $return_array = array();

        if (count($package_goods) == 0 || !is_numeric($goods_number))
        {
            return $return_array;
        }

        foreach ($package_goods as $key=>$value)
        {
            $return_array[$key] = $value;
            $return_array[$key]['order_send_number'] = $value['order_goods_number'] * $goods_number;
            $return_array[$key]['sended'] = package_sended($package_id, $value['goods_id'], $order_id, $extension_code, $value['product_id']);
            $return_array[$key]['send'] = ($value['order_goods_number'] * $goods_number) - $return_array[$key]['sended'];
            $return_array[$key]['storage'] = $value['goods_number'];


            if ($return_array[$key]['send'] <= 0)
            {
                $return_array[$key]['send'] = $GLOBALS['_LANG']['act_good_delivery'];
                $return_array[$key]['readonly'] = 'readonly="readonly"';
            }

            /* 是否缺货 */
            if ($return_array[$key]['storage'] <= 0 && $GLOBALS['_CFG']['use_storage'] == '1')
            {
                $return_array[$key]['send'] = $GLOBALS['_LANG']['act_good_vacancy'];
                $return_array[$key]['readonly'] = 'readonly="readonly"';
            }
        }

        return $return_array;
    }

    /**
     * Auto ungroup package
     */
    function auto_ungroup_package($platform = null)
    {
        global $db, $ecs, $_LANG, $_CFG;
        $packageController = new PackageController();
        // If undefind platform, return
        if(!in_array($platform, [self::ORDER_FEE_PLATFORM_WEB, self::ORDER_FEE_PLATFORM_POS]))
        return false;
        if($platform == self::ORDER_FEE_PLATFORM_WEB)$cart_table_name = 'cart';
        else if($platform == self::ORDER_FEE_PLATFORM_POS)$cart_table_name = 'cart1';

        $sql = "select is_package " .
        "FROM " . $GLOBALS['ecs']->table($cart_table_name) . " " .
        "WHERE session_id = '" . SESS_ID . "' " .
        "AND is_package > 0 " .
        "GROUP BY is_package";
        $package_arr = $GLOBALS['db']->getAll($sql);
        foreach($package_arr as $packages){
            $can_enjoy_package = true;
            $package_id = $packages['is_package'];

            $package_info = $packageController->get_package_info($package_id);
            $cart_goods = $this->get_package_goods_from_cart($platform, $package_id);
            
            foreach($package_info['goods_list'] as $pack_goods){
                if($cart_goods[$pack_goods['goods_id']]['goods_number'] < $pack_goods['goods_number']){
                    $can_enjoy_package = false;
                    break;
                }
            }

            if(!$can_enjoy_package){
                $sql = "DELETE FROM " . $GLOBALS['ecs']->table($cart_table_name) .
                    " WHERE is_package = '".$package_id."' AND session_id='".SESS_ID."'";
                $GLOBALS['db']->query($sql);
            }
        }

    }


    /**
     * Auto group product to package
     */
    function auto_group_package($platform = null)
    {
        global $db, $ecs, $_LANG, $_CFG;
        $packageController = new PackageController();

        // If undefind platform, return
        if(!in_array($platform, [self::ORDER_FEE_PLATFORM_WEB, self::ORDER_FEE_PLATFORM_POS]))
        return false;
        if($platform == self::ORDER_FEE_PLATFORM_WEB)$cart_table_name = 'cart';
        else if($platform == self::ORDER_FEE_PLATFORM_POS)$cart_table_name = 'cart1';

        $goods_list = $this->get_normal_goods_from_cart($platform);
        $package_id_arr = array();
        $package_arr = array();
        foreach($goods_list as $goods_id => $goods){
            $package_goods_list = $packageController->get_package_goods_list($goods_id);
            foreach($package_goods_list as $package){
                $package_goods = array();
                if(!in_array($package['act_id'], $package_id_arr)){
                    foreach($package['goods_list'] as $pack_goods){
                        $package_goods[$pack_goods['goods_id']]['goods_id'] = $pack_goods['goods_id'];
                        $package_goods[$pack_goods['goods_id']]['goods_number'] = $pack_goods['goods_number'];
                        $package_goods[$pack_goods['goods_id']]['goods_price'] = $pack_goods['goods_price'];
                    }

                    $package_id_arr[] = $package['act_id'];
                    $package_arr[$package['act_id']] = $package_goods;
                }
            }
        }

        foreach($package_arr as $package_id => $package){
            $can_enjoy_package = true;
            $update_qty = array();
            foreach($package as $goods_id => $goods){
                if($goods_list[$goods_id]['goods_number'] < $goods['goods_number']){
                    $can_enjoy_package = false;
                    break;
                }
                else{
                    $update_qty[$goods_list[$goods_id]['rec_id']] = $goods_list[$goods_id]['goods_number'] - $goods['goods_number'];
                }
            }

            if($can_enjoy_package){
	            $spec = array();
                /* 是否只有一個 */
                /*
                $sql = "SELECT COUNT(*) AS package_count FROM ".$GLOBALS['ecs']->table($cart_table_name)." WHERE is_package = '".$package_id."' AND session_id='".SESS_ID."'";
                $package_count = $GLOBALS['db']->getOne($sql);;
                if ($package_count > 0){
                    break;
                }
                */

                $ok = add_package_to_cart($package_id, $spec, 1, $cart_table_name);
                if($ok){
                    foreach($update_qty as $rec_id => $goods_number){
                        if($goods_number > 0){
                            $sql = "UPDATE " . $GLOBALS['ecs']->table($cart_table_name) . " SET goods_number = '".$goods_number."'" .
                                    " WHERE rec_id = '".$rec_id."' AND session_id='".SESS_ID."'";
                        }
                        else{
                            $sql = "DELETE FROM " . $GLOBALS['ecs']->table($cart_table_name) .
                                    " WHERE rec_id = '".$rec_id."' AND session_id='".SESS_ID."'";
                        }
                        $GLOBALS['db']->query($sql);
                    }
                }
                //$this->auto_group_package($platform);
                return false;
            }
        }

        return true;
    }

    function get_package_goods_from_cart($platform = null, $package_id){
        // If undefind platform, return
        if(!in_array($platform, [self::ORDER_FEE_PLATFORM_WEB, self::ORDER_FEE_PLATFORM_POS]))
        return false;
        if($platform == self::ORDER_FEE_PLATFORM_WEB)$cart_table_name = 'cart';
        else if($platform == self::ORDER_FEE_PLATFORM_POS)$cart_table_name = 'cart1';

        $sql = "select rec_id, goods_id, goods_number " .
        "FROM " . $GLOBALS['ecs']->table($cart_table_name) . " " .
        "WHERE session_id = '" . SESS_ID . "' " .
        "AND is_package = " . $package_id;
        $arr = $GLOBALS['db']->getAll($sql);

        $return_arr = array();
        foreach($arr as $goods){
            $return_arr[$goods['goods_id']]['goods_number'] = $goods['goods_number'];
            $return_arr[$goods['goods_id']]['rec_id'] = $goods['rec_id'];
        }

        return $return_arr;

    }

    function get_normal_goods_from_cart($platform = null){
        // If undefind platform, return
        if(!in_array($platform, [self::ORDER_FEE_PLATFORM_WEB, self::ORDER_FEE_PLATFORM_POS]))
        return false;
        if($platform == self::ORDER_FEE_PLATFORM_WEB)$cart_table_name = 'cart';
        else if($platform == self::ORDER_FEE_PLATFORM_POS)$cart_table_name = 'cart1';

        $sql = "select rec_id, goods_id, goods_number " .
        "FROM " . $GLOBALS['ecs']->table($cart_table_name) . " " .
        "WHERE session_id = '" . SESS_ID . "' " .
        "AND is_gift = 0 AND is_package = 0 AND flashdeal_id = 0 ";
        $arr = $GLOBALS['db']->getAll($sql);

        $return_arr = array();
        foreach($arr as $goods){
            $return_arr[$goods['goods_id']]['goods_number'] = $goods['goods_number'];
            $return_arr[$goods['goods_id']]['rec_id'] = $goods['rec_id'];
        }

        return $return_arr;

    }

    /**
     * 更新订单商品信息
     * @param   int     $order_id       订单 id
     * @param   array   $_sended        Array(‘商品id’ => ‘此单发货数量’)
     * @param   array   $goods_list
     * @return  Bool
     */
    function update_order_goods_sended($order_id, $_sended, $goods_list = array())
    {
        if (!is_array($_sended) || empty($order_id))
        {
            return false;
        }

        foreach ($_sended as $key => $value)
        {
            // 超值礼包
            if (is_array($value))
            {
                if (!is_array($goods_list))
                {
                    $goods_list = array();
                }

                foreach ($goods_list as $goods)
                {
                    if (($key != $goods['rec_id']) || (!isset($goods['package_goods_list']) || !is_array($goods['package_goods_list'])))
                    {
                        continue;
                    }

                    $goods['package_goods_list'] = $this->package_goods($goods['package_goods_list'], $goods['goods_number'], $goods['order_id'], $goods['extension_code'], $goods['goods_id']);
                    $pg_is_end = true;

                    foreach ($goods['package_goods_list'] as $pg_key => $pg_value)
                    {
                        if ($pg_value['order_send_number'] != $pg_value['sended'])
                        {
                            $pg_is_end = false; // 此超值礼包，此商品未全部发货

                            break;
                        }
                    }

                    // 超值礼包商品全部发货后更新订单商品库存
                    if ($pg_is_end)
                    {
                        $sql = "UPDATE " . $GLOBALS['ecs']->table('order_goods') . "
                                SET send_number = goods_number
                                WHERE order_id = '$order_id'
                                AND goods_id = '" . $goods['goods_id'] . "' ";

                        $GLOBALS['db']->query($sql, 'SILENT');
                    }
                }
            }
            // 商品（实货）（货品）
            elseif (!is_array($value))
            {
                /* 检查是否为商品（实货）（货品） */
                foreach ($goods_list as $goods)
                {
                    if ($goods['rec_id'] == $key && $goods['is_real'] == 1)
                    {
                        $sql = "UPDATE " . $GLOBALS['ecs']->table('order_goods') . "
                                SET send_number = send_number + $value
                                WHERE order_id = '$order_id'
                                AND rec_id = '$key' ";
                        $GLOBALS['db']->query($sql, 'SILENT');
                        break;
                    }
                }
            }
        }

        return true;
    }

    /**
     * 更新订单虚拟商品信息
     * @param   int     $order_id       订单 id
     * @param   array   $_sended        Array(‘商品id’ => ‘此单发货数量’)
     * @param   array   $virtual_goods  虚拟商品列表
     * @return  Bool
     */
    function update_order_virtual_goods($order_id, $_sended, $virtual_goods)
    {
        if (!is_array($_sended) || empty($order_id))
        {
            return false;
        }
        if (empty($virtual_goods))
        {
            return true;
        }
        elseif (!is_array($virtual_goods))
        {
            return false;
        }

        foreach ($virtual_goods as $goods)
        {
            $sql = "UPDATE ".$GLOBALS['ecs']->table('order_goods'). "
                    SET send_number = send_number + '" . $goods['num'] . "'
                    WHERE order_id = '" . $order_id . "'
                    AND goods_id = '" . $goods['goods_id'] . "' ";
            if (!$GLOBALS['db']->query($sql, 'SILENT'))
            {
                return false;
            }
        }

        return true;
    }

    /**
     * 订单中的商品是否已经全部发货
     * @param   int     $order_id  订单 id
     * @return  int     1，全部发货；0，未全部发货
     */
    function get_order_finish($order_id)
    {
        $return_res = 0;

        if (empty($order_id))
        {
            return $return_res;
        }

        $sql = 'SELECT COUNT(rec_id)
                FROM ' . $GLOBALS['ecs']->table('order_goods') . '
                WHERE order_id = \'' . $order_id . '\'
                AND goods_number > send_number';

        $sum = $GLOBALS['db']->getOne($sql);
        if (empty($sum))
        {
            $return_res = 1;
        }

        return $return_res;
    }

    function handle_ordering_remark($remark_str, $pay_status, $pay_time, $order_id){
        if(empty($remark_str)) return '';

        $remark_list = explode("\n", $remark_str);
        $remark_list = array_filter($remark_list);
        $new_remark = "";
        // Default using today, If order is payed, using pay day.
        $time = gmtime();
        if(isset($pay_status) && ($pay_status == PS_PAYED)) $time = $pay_time;

        foreach ($remark_list as $key => $remark) {

            //TODO: Hardcode handle pos/online-shop order postscript text
            $text = mb_split("訂貨",$remark);
            if (isset($text['0'])) {
                $text['0'] = $this->escape_string($text['0']); 
            }
            
            if (count($text) > 1){
                $pre_sale_string = $GLOBALS['db']->getOne("SELECT g.pre_sale_string FROM " . $GLOBALS['ecs']->table('order_goods') . " as og LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON g.goods_id = og.goods_id WHERE og.order_id = $order_id AND og.goods_name = '$text[0]'");
                $planned_delivery_date = $GLOBALS['db']->getOne("SELECT planned_delivery_date FROM " . $GLOBALS['ecs']->table('order_goods') . " WHERE order_id = $order_id AND goods_name = '$text[0]'");
                $pre_sale_days = intval(preg_replace('/\D/', '', $text[1]));
                $new_remark .= $remark;
                if(!empty($pre_sale_days)){
                    $date = local_date('Y-m-d', empty($planned_delivery_date) ? $this->get_yoho_next_work_day($time, $pre_sale_days) : intval($planned_delivery_date));
                    if($pre_sale_string == 0) $string = '出貨';
                    else $string = '派送';
                    $new_remark .= (isset($pay_status) && ($pay_status == PS_PAYED)) ? " (預計 ".$date." $string)" : " (即日付款預計 ".$date." $string)";
                }
            } else {
                $new_remark .= $remark;
            }
            $new_remark .= "<br>";
        }
        return $new_remark;
    }

    public function order_goods_planned_delivery_date($order_id){
        if(empty($order_id)) return false;
        require_once(ROOT_PATH . 'includes/lib_howang.php');
        $sql = "SELECT og.rec_id, oi.pay_time, g.goods_id, g.is_purchasing, g.is_pre_sale, g.pre_sale_date, g.pre_sale_time, g.pre_sale_days, og.planned_delivery_date, og.send_number, og.goods_number, ".
            hw_goods_number_subquery('g.', 'real_goods_number') . ", " .
            hw_reserved_number_subquery('g.') . " " .
            " FROM ".$GLOBALS['ecs']->table('goods')." as g ".
            " LEFT JOIN ".$GLOBALS['ecs']->table('order_goods')." as og ON og.goods_id = g.goods_id ".
            " LEFT JOIN ".$GLOBALS['ecs']->table('order_info')." as oi ON oi.order_id = og.order_id ".
            " WHERE og.order_id = $order_id AND og.send_number <> og.goods_number AND og.is_real = 1";

        $res = $GLOBALS['db']->getAll($sql);

        foreach ($res as $key => $row) {

            // Have old planned_delivery_date, dont't update.
            if(!empty($row['planned_delivery_date'])) continue;

            // Check stock
            $stock = intval($row['real_goods_number']) - intval($row['reserved_number']);

            if($stock >= 0) continue;

            // Cal planned_delivery_date
            if ($row['is_pre_sale']) // 預售
            {
                // 預售不能購買
            }
            elseif ($row['is_purchasing'] && $row['pre_sale_days'] > 0) // 外國代購
            {
                $row['planned_delivery_date'] = $this->get_yoho_next_work_day(gmtime(), $row['pre_sale_days']);
            }
            elseif ($row['pre_sale_date'] > gmtime()) // 即將到貨
            {
                $row['pre_sale_days'] = $this->count_yoho_work_day($row['pre_sale_date']);
                $row['planned_delivery_date'] = $this->get_yoho_next_work_day($row['pre_sale_date']);
            }
            elseif ($row['pre_sale_days'] > 0) // 訂貨
            {
                $hour = local_date('H', $row['pay_time']);
                if(empty($row['pre_sale_time'])) $row['pre_sale_time'] = 16;
                if($hour > $row['pre_sale_time']) {
                    $row['pre_sale_days'] = $row['pre_sale_days'] + 1;
                }
                $row['planned_delivery_date'] = $this->get_yoho_next_work_day(gmtime(), $row['pre_sale_days']);
            }
            else // 缺貨
            {
                // 缺貨不能購買
            }
            // Add order_goods planned_delivery_date
            $sql = "UPDATE ".$GLOBALS['ecs']->table('order_goods')." SET planned_delivery_date = ".$row['planned_delivery_date'].
                " WHERE rec_id = ".$row['rec_id'];

            $GLOBALS['db']->query($sql);
        }

        return true;
    }

    /** Handle POS/WEB favourable list
     * * Step 1 : Existing promotions and quantities in shopping cart
     * * Step 2 : Check user can get the offer
     * * Step 3 : Handle favourable users_list, If favourable have user list, check user
     * * Step 4 : Handle favourable data
     * * Step 5 : Handle favourable available
     */
    function favourable_list($user_rank = 0, $user_id = 0, $platform = null, $act_id = null)
    {
        global $db, $ecs, $_LANG, $_CFG;
        require(ROOT_PATH . 'languages/' .$_CFG['lang']. '/yoho.php');

        $erpController = new ErpController(); 
        $goodsController = new GoodsController(); 

        // If undefind platform, return
        if(!in_array($platform, [self::ORDER_FEE_PLATFORM_WEB, self::ORDER_FEE_PLATFORM_POS]))
        return false;
        if($platform == self::ORDER_FEE_PLATFORM_WEB){
            $cart_table_name = 'cart';
        }
        else if($platform == self::ORDER_FEE_PLATFORM_POS){
            $cart_table_name = 'cart1';
        }

        $cart_goods = $this->get_normal_goods_from_cart($platform);
        $cart_goods_ids = array_keys($cart_goods);
        if(empty($cart_goods_ids)){
            return array();
        }

        foreach($cart_goods_ids as $cart_goods_id){
            $search_list[] = ",".$cart_goods_id.",";
        }
        $search_str = implode("|", $search_list);

        /* Step 1 : Existing promotions and quantities in shopping cart */
        $in_cart_list = $this->cart_favourable($platform);

        /* Step 2 : Check user can get the offer */
        $favourable_list = array();
        $user_rank = ',' . $user_rank . ',';
        $now = gmtime();

        if($act_id){
            $act_search = " AND act_id = '" . $act_id . "'";
        }
        
        $sql = "SELECT * " .
                "FROM " . $ecs->table('favourable_activity') .
                " WHERE CONCAT(',', user_rank, ',') LIKE '%" . $user_rank . "%'" .
                " AND start_time <= '$now' AND end_time >= '$now'" .
                " AND status = 1" .
                " AND act_type = '" . FAT_GOODS . "'" .
                " AND CONCAT(',', act_range_ext, ',') REGEXP '".$search_str."'" .
                $act_search .
                " ORDER BY sort_order";      
        $res = $db->query($sql);

        while ($favourable = $GLOBALS['db']->fetchRow($res))
        {
            /* Step 3 : Handle favourable users_list, If favourable have user list, check user */
            if(!empty($favourable['users_list'])){
                $users_list = json_decode($favourable['users_list']);
                //$user_id = $_SESSION['user_id'];

                // If user_id not in users_list, continue.
                if(!in_array($user_id, $users_list)){
                    continue;
                }
            }

            /* Step 4 : Handle favourable data */
            $favourable['start_time_unformated'] = $favourable['start_time'];
            $favourable['end_time_unformated'] = $favourable['end_time'];
            $favourable['start_time'] = local_date($_CFG['time_format'], $favourable['start_time']);
            $favourable['end_time']   = local_date($_CFG['time_format'], $favourable['end_time']);
            $favourable['formated_min_amount'] = price_format($favourable['min_amount'], false);
            $favourable['formated_max_amount'] = price_format($favourable['max_amount'], false);
            $favourable['min_quantity'] = intval($favourable['min_quantity']) >= 1 ? intval($favourable['min_quantity']) : 1;

            /* Step 5 : Handle favourable available */
            // Can available
            $favourable['available'] = $this->favourable_available($favourable, $user_rank, $user_id, $platform);
           
            if (empty($favourable['available'])) {
                continue;
            }

            $favourable['gift'] = unserialize( $favourable['gift']);
            $goods_ids = array_map(function ($gift) { return $gift['id']; }, $favourable['gift']);
            $goods_stock = $erpController->getSumOfSellableProductsQtyByWarehouses($goods_ids,null,true,null,false);
            
            $sql = "SELECT g.goods_id, g.goods_name, g.goods_thumb, g.goods_img, g.shop_price, g.is_on_sale, g.goods_sn, gperma " ." FROM " . $ecs->table('goods') . "as g " .
            "LEFT JOIN (SELECT id, table_name, lang, perma_link as gperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") gpl ON gpl.id = g.goods_id  AND gpl.table_name = 'goods' AND gpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
            "WHERE g.goods_id " .  db_create_in($goods_ids);
            $goods_list = $db->getAll($sql);
            $goods_list = localize_db_result('goods', $goods_list);
            foreach ($favourable['gift'] as $key => $value)
            {
                $goods = null;
                foreach ($goods_list as $g)
                {
                    if ($g['goods_id'] == $value['id'])
                    {
                        $goods = $g;
                        break;
                    }
                }
                if(!$goods || !$goods['is_on_sale'])
                {
                    unset($favourable['gift'][$key]);
                    continue;
                }

                if ($favourable['act_kind'] == 1) {
                    //$stock = intval($goods['real_goods_number']) - intval($goods['reserved_number']);
                    $stock = $goods_stock[$value['id']];
                    $stock = $stock > 0 ? $stock : 0;
                    if($stock == 0){
                        unset($favourable['gift'][$key]);
                        continue;
                    }
                } else if ($favourable['act_kind'] == 2) {
                    //$goods_info = get_goods_info($goods['goods_id']);
                    $goods_info_pre_order = $goodsController->getGoodsPreOrderInfo($goods, $goods_stock[$value['id']], 1, 1);
                    if(!($goods_stock[$value['id']] > 0 || ($goods_info_pre_order['is_pre_sale'] != '1' && $goods_info_pre_order['pre_sale_days'] > 0) )  ){
                        unset($favourable['gift'][$key]);
                        continue;
                    }
                }
                $final_price_info = get_final_price($goods['goods_id'], 1, true);
                $final_price = $final_price_info['final_price'];
                
                $favourable['gift'][$key]['formated_price']       = price_format($value['price'], false);
                $favourable['gift'][$key]['goods_id']             = $goods['goods_id'];
                $favourable['gift'][$key]['goods_sn']             = $goods['goods_sn'];
                $favourable['gift'][$key]['goods_name']           = $goods['goods_name'];
                $favourable['gift'][$key]['goods_number']         = $stock;
                $favourable['gift'][$key]['goods_thumb']          = get_image_path($goods['goods_id'], $goods['goods_thumb'], true);
                $favourable['gift'][$key]['goods_img']            = get_image_path($goods['goods_id'], $row['goods_img']);
                $favourable['gift'][$key]['url']                  = build_uri('goods', array('gid' => $goods['goods_id'], 'gname' => $goods['goods_name'], 'gperma' => $goods['gperma']), $goods['goods_name']);
                $favourable['gift'][$key]['shop_price']           = $goods['shop_price'];
                $favourable['gift'][$key]['shop_price_formatted'] = price_format($goods['shop_price'], false);
                $favourable['gift'][$key]['final_price']          = $goods['final_price'];
                $favourable['gift'][$key]['final_price_formatted']= price_format($final_price, false);
            }
            
            $favourable['act_range_desc'] = $this->act_range_desc($favourable);
            $favourable['act_type_desc'] = sprintf($_LANG['fat_ext'][$favourable['act_type']], $favourable['act_type_ext']);
            
             
            if ($favourable['available'])
            {
                // Is used?
                $favourable['available'] = !$this->favourable_used($favourable, $in_cart_list, $platform);
            }

            if(count($favourable['gift']) > 0){
                $favourable_list[] = $favourable;
            }
        }
        
        usort($favourable_list, function ($a, $b)
        {
            if ($a['available'] == $b['available'])
            {
                if ($a['sort_order'] == $b['sort_order'])
                {
                    return 0;
                }
                else
                {
                    return $a['sort_order'] < $b['sort_order'] ? -1 : 1;
                }
            }
            else
            {
                return $a['available'] ? -1 : 1;
            }
        });
        
        // howang: only include available activity
        $favourable_list = array_filter($favourable_list, function ($favourable) { return $favourable['available']; });
        $adjusted_goods  = array_column($_SESSION['adjusted_goods'], 'goods_id');
        /* Step 6 : Special handle: If favourable is type FAT_GOODS, act_type_ext = gift number, and all the gift is free, auto add into cart */
        
        foreach ($favourable_list as $key => $favourable) {
            // if favourable is not FAT_GOODS, next.
            if($favourable['act_kind'] == 1 && $favourable['act_type_ext'] == 0){ //贈品 + 不限數量
                if(intval($favourable['act_type']) !== FAT_GOODS) continue;
                //echo intval($favourable['act_type_ext']) . "d".$gift_count."ffffffff";
                // if act_type_ext [贈品（特惠品）的最大數量] == 0,  count(gift) == 0, count(gift) != act_type_ext , next.
                $gift_count = count($favourable['gift']);

                if(/*intval($favourable['act_type_ext']) == 0 || */$gift_count == 0) continue;
                $is_free = false;
                foreach ($favourable['gift'] as $gkey => $gift) {
                    if(intval($gift['price']) == 0 && intval($favourable['act_type_ext']) == 0){
                        $is_free = true;
                    }
                    else {
                        $is_free = false;
                        break;
                    }

                    /*
                    // Check goods in cart
                    $sql = "SELECT goods_id" .
                    " FROM " . $ecs->table($cart_table_name) .
                    " WHERE session_id = '" . SESS_ID . "'" .
                    " AND rec_type = '" . CART_GENERAL_GOODS . "'" .
                    " AND is_gift = $favourable[act_id]" .
                    " AND goods_id = " . $gift['goods_id'];
                    $cart_goods_id = $db->getOne($sql);
                    if(in_array($gift['goods_id'], $adjusted_goods)|| $gift['goods_id'] == $cart_goods_id) {
                        $is_free = false;
                        if($gift['goods_id'] == $cart_goods_id)$favourable_list[$key]['in_cart_goods'][] = intval($gift['goods_id']);
                        break;
                    }
                    */
                }
                
                // If all goods is free (贈品, price = 0), auto add to cart
                if ($is_free) {

                    $fav_goods_list = array_column($favourable['gift'], 'goods_id');
                   
                    if($platform == self::ORDER_FEE_PLATFORM_POS) {
                        // POS: check gift stock, If zero, not add.
                        foreach($fav_goods_list as $g_k => $g_gid) {
                            $gift_info = get_goods_info($g_gid, true);
                            if($gift_info['goods_number'] <= 0) {
                                unset($fav_goods_list[$g_k]);
                            }
                        }
                    }
                    
                    if(empty($fav_goods_list)){
                        $added = false;
                    }
                    else{
                        $cart_favourable_quantity = $this->cart_favourable_quantity($favourable, $platform);
                        $can_enjoy_favourable_count = floor($cart_favourable_quantity / $favourable['min_quantity']);
                        $added = $this->add_favourable_goods(0, $can_enjoy_favourable_count, $favourable['act_id'], $user_rank, $user_id, $platform, $fav_goods_list);
                    }

                    // Not display in cart page.
                    if ($added) {
                        unset($favourable_list[$key]);
                        if($platform == self::ORDER_FEE_PLATFORM_POS) $_SESSION['pos_update_cart'] = true;
                    }

                }
            }
        }
        
        return $favourable_list;
    }

    /** Handle POS/WEB favourable list
     * * Step 1 : Existing promotions and quantities in shopping cart
     * * Step 2 : Check user can get the offer
     * * Step 3 : Handle favourable users_list, If favourable have user list, check user
     * * Step 4 : Handle favourable data
     * * Step 5 : Handle favourable available
     */
    function favourable_list_by_goods($user_rank = 0, $user_id = 0, $goods_id = 0, $platform = null)
    {
        global $db, $ecs, $_LANG, $_CFG;
        require(ROOT_PATH . 'languages/' .$_CFG['lang']. '/yoho.php');

        $erpController = new ErpController(); 
        $goodsController = new GoodsController(); 
        // If undefind platform, return
        if(!in_array($platform, [self::ORDER_FEE_PLATFORM_WEB, self::ORDER_FEE_PLATFORM_POS]))
        return false;
        if($platform == self::ORDER_FEE_PLATFORM_WEB)$cart_table_name = 'cart';
        else if($platform == self::ORDER_FEE_PLATFORM_POS)$cart_table_name = 'cart1';

        /* Step 1 : Existing promotions and quantities in shopping cart */
        $in_cart_list = $this->cart_favourable($platform);

        /* Step 2 : Check user can get the offer */
        $favourable_list = array();
        $user_rank = ',' . $user_rank . ',';
        $now = gmtime();
        $sql = "SELECT fa.*, fal.act_name as act_name_i18n " .
                "FROM " . $ecs->table('favourable_activity') . " fa 
                left join " . $ecs->table('favourable_activity_lang') . "fal on (fa.act_id = fal.act_id) and lang = '".$_CFG['lang']."' " .
                " WHERE CONCAT(',', user_rank, ',') LIKE '%" . $user_rank . "%'" .
                " AND start_time <= '$now' AND end_time >= '$now'" .
                " AND status = 1" .
                " AND act_type = '" . FAT_GOODS . "'" .
                " ORDER BY sort_order";
                
        $res = $db->query($sql);
        while ($favourable = $GLOBALS['db']->fetchRow($res))
        {
            if (!empty($favourable['act_name_i18n'])) {
                $favourable['act_name'] = $favourable['act_name_i18n'];
            }
            /* Step 3 : Handle favourable users_list, If favourable have user list, check user */
            if(!empty($favourable['users_list'])){
                $users_list = json_decode($favourable['users_list']);
                //$user_id = $_SESSION['user_id'];

                // If user_id not in users_list, continue.
                if(!in_array($user_id, $users_list)){
                    continue;
                }
            }

            $favourable['available'] = $this->favourable_available_by_goods($favourable, $user_rank, $user_id, $goods_id, $platform);

            if (empty($favourable['available'])) {
                continue;
            }
           
            /* Step 4 : Handle favourable data */
            $favourable['start_time_unformated'] = $favourable['start_time'];
            $favourable['end_time_unformated'] = $favourable['end_time'];
            $favourable['start_time'] = local_date($_CFG['time_format'], $favourable['start_time']);
            $favourable['end_time']   = local_date($_CFG['time_format'], $favourable['end_time']);
            $favourable['formated_min_amount'] = price_format($favourable['min_amount'], false);
            $favourable['formated_max_amount'] = price_format($favourable['max_amount'], false);
            $favourable['min_quantity'] = intval($favourable['min_quantity']) >= 1 ? intval($favourable['min_quantity']) : 1;
            if ($favourable['act_type'] == FAT_GOODS){
                $favourable['act_type_ext'] = intval($favourable['act_type_ext']);
            }
                  
            $favourable['gift'] = unserialize( $favourable['gift']);
          
            $goods_ids = array_map(function ($gift) { return $gift['id']; }, $favourable['gift']);
            $goods_stock = $erpController->getSumOfSellableProductsQtyByWarehouses($goods_ids,null,true,null,false);

            // get sellable stock 
            $sql = "SELECT g.goods_id, g.goods_name, g.goods_thumb, g.goods_img, g.shop_price, g.market_price, g.is_on_sale, g.goods_sn, g.pre_sale_date, g.pre_sale_days,g.is_pre_sale,g.is_purchasing "  .
            " FROM " . $ecs->table('goods') . "as g WHERE g.goods_id " .  db_create_in($goods_ids);

            $goods_list = $db->getAll($sql);
            $goods_list = localize_db_result('goods', $goods_list);
            $favourable['gift_total_value'] = 0;

            //////////////////////////////////
            $goods_count = 0;
            $price_list_count = 0;
            $price_arr = array();
            //////////////////////////////////
            foreach ($favourable['gift'] as $key => $value)
            {
                $goods = null;
                foreach ($goods_list as $g)
                {
                    if ($g['goods_id'] == $value['id'])
                    {
                        $goods = $g;
                        break;
                    }
                }

                if(!$goods || !$goods['is_on_sale'])
                {
                    unset($favourable['gift'][$key]);
                    continue;
                }

                if ($favourable['act_kind'] == 1) {
                    $stock = $goods_stock[$value['id']];
                    $stock = $stock > 0 ? $stock : 0;
                    if($stock == 0){
                        unset($favourable['gift'][$key]);
                        continue;
                    }
                } else if ($favourable['act_kind'] == 2) {
                    // get pre_sale_days
                    $goods_info_pre_order = $goodsController->getGoodsPreOrderInfo($goods, $goods_stock[$value['id']], 1, 1);
                    if(!($goods_stock[$value['id']] > 0 || ($goods_info_pre_order['is_pre_sale'] != '1' && $goods_info_pre_order['pre_sale_days'] > 0) )  ){
                        unset($favourable['gift'][$key]);
                        continue;
                    }
                }
                
                $final_price_info = get_final_price($goods['goods_id'], 1, true);
                $final_price = $final_price_info['final_price'];
                if($favourable['act_kind'] == 2){
                    if(!in_array($value['price'], $price_arr)){
                        //$favourable['gift_group_by_price']['price_list'][$price_list_count]['price'] = $value['price'];
                        //$favourable['gift_group_by_price']['price_list'][$price_list_count]['large_price'] = $goods['shop_price'];
                        $price_info[$value['price']]['price'] = $value['price'];
                        $price_info[$value['price']]['price_formatted'] = price_format($value['price']);
                        $price_info[$value['price']]['large_price'] = $goods['market_price'];

                        $price_arr[] = $value['price'];
                    }
                    else{
                        //$price_list_key = array_search($value['price'], $price_arr);
//
                        //if($goods['shop_price'] > $favourable['gift_group_by_price']['price_list'][$price_list_key]['large_price']){
                        //    $favourable['gift_group_by_price']['price_list'][$price_list_key]['large_price'] = $goods['shop_price'];
                        //}

                        /*
                        if($goods['shop_price'] > $price_info[$value['price']]['large_price']){
                            $price_info[$value['price']]['large_price'] = $goods['shop_price'];
                        }
                        */

                    }

                    $favourable['gift_group_by_price']['price_goods'][$value['price']]['gift'][$goods_count]['id']                     = $value['id'];
                    $favourable['gift_group_by_price']['price_goods'][$value['price']]['gift'][$goods_count]['name']                   = $value['name'];
                    $favourable['gift_group_by_price']['price_goods'][$value['price']]['gift'][$goods_count]['price']                  = $value['price'];
                    $favourable['gift_group_by_price']['price_goods'][$value['price']]['gift'][$goods_count]['formated_price']         = price_format($value['price'], false);
                    $favourable['gift_group_by_price']['price_goods'][$value['price']]['gift'][$goods_count]['goods_id']               = $goods['goods_id'];
                    $favourable['gift_group_by_price']['price_goods'][$value['price']]['gift'][$goods_count]['goods_sn']               = $goods['goods_sn'];
                    $favourable['gift_group_by_price']['price_goods'][$value['price']]['gift'][$goods_count]['goods_name']             = $goods['goods_name'];
                    $favourable['gift_group_by_price']['price_goods'][$value['price']]['gift'][$goods_count]['goods_number']           = $stock;
                    $favourable['gift_group_by_price']['price_goods'][$value['price']]['gift'][$goods_count]['goods_thumb']            = get_image_path($goods['goods_id'], $goods['goods_thumb'], true);
                    $favourable['gift_group_by_price']['price_goods'][$value['price']]['gift'][$goods_count]['goods_img']              = get_image_path($goods['goods_id'], $goods['goods_img']);
                    $favourable['gift_group_by_price']['price_goods'][$value['price']]['gift'][$goods_count]['url']                    = build_uri('goods', array('gid' => $goods['goods_id'], 'gname' => $goods['goods_name']), $goods['goods_name']);
                    $favourable['gift_group_by_price']['price_goods'][$value['price']]['gift'][$goods_count]['market_price']           = $goods['market_price'];
                    $favourable['gift_group_by_price']['price_goods'][$value['price']]['gift'][$goods_count]['market_price_formatted'] = price_format($goods['market_price'], false);
                    $favourable['gift_group_by_price']['price_goods'][$value['price']]['gift'][$goods_count]['shop_price']             = $goods['shop_price'];
                    $favourable['gift_group_by_price']['price_goods'][$value['price']]['gift'][$goods_count]['shop_price_formatted']   = price_format($goods['shop_price'], false);
                    $favourable['gift_group_by_price']['price_goods'][$value['price']]['gift'][$goods_count]['final_price']            = $final_price;
                    $favourable['gift_group_by_price']['price_goods'][$value['price']]['gift'][$goods_count]['final_price_formatted']  = price_format($final_price, false);
                    $favourable['gift_group_by_price']['price_goods'][$value['price']]['gift'][$goods_count]['goods_count']            = $goods_count;
                }
                else{
                    $favourable['gift'][$key]['formated_price']         = price_format($value['price'], false);
                    $favourable['gift'][$key]['goods_id']               = $goods['goods_id'];
                    $favourable['gift'][$key]['goods_sn']               = $goods['goods_sn'];
                    $favourable['gift'][$key]['goods_name']             = $goods['goods_name'];
                    $favourable['gift'][$key]['goods_number']           = $stock;
                    $favourable['gift'][$key]['goods_thumb']            = get_image_path($goods['goods_id'], $goods['goods_thumb'], true);
                    $favourable['gift'][$key]['goods_img']              = get_image_path($goods['goods_id'], $goods['goods_img']);
                    $favourable['gift'][$key]['url']                    = build_uri('goods', array('gid' => $goods['goods_id'], 'gname' => $goods['goods_name']), $goods['goods_name']);
                    $favourable['gift'][$key]['market_price']           = $goods['market_price'];
                    $favourable['gift'][$key]['market_price_formatted'] = price_format($goods['market_price'], false);
                    $favourable['gift'][$key]['shop_price']             = $goods['shop_price'];
                    $favourable['gift'][$key]['shop_price_formatted']   = price_format($goods['shop_price'], false);
                    $favourable['gift'][$key]['final_price']            = $final_price;
                    $favourable['gift'][$key]['final_price_formatted']  = price_format($final_price, false);
                    $favourable['gift'][$key]['goods_count']            = $goods_count;
                }
                $goods_count++;
                $favourable['gift_total_value'] += $goods['market_price'];
            }
            
            if($favourable['act_kind'] == 2){
                ksort($price_info);
                $i=0;
                foreach($price_info as $info){
                    $favourable['gift_group_by_price']['price_list'][$i]['price'] = $info['price'];
                    $favourable['gift_group_by_price']['price_list'][$i]['price_formatted'] = $info['price_formatted'];
                    $favourable['gift_group_by_price']['price_list'][$i]['large_price'] = $info['large_price'];
                    $i++;
                }
            }
            
            $favourable['act_range_desc'] = $this->act_range_desc($favourable);
            $favourable['act_type_desc'] = sprintf($_LANG['fat_ext'][$favourable['act_type']], $favourable['act_type_ext']);

            $favourable['gift_total_value_formatted'] = price_format($favourable['gift_total_value'], false);
            /* Step 5 : Handle favourable available */
            // Can available
            //print_r($favourable);
            //$favourable['available'] = $this->favourable_available_by_goods($favourable, $user_rank, $user_id, $goods_id, $platform);
           
            if(count($favourable['gift']) > 0){
                $favourable_list[] = $favourable;
            }
            
            if($favourable['act_kind'] == 2){
                unset($favourable['gift']);
            }
        }
      
        usort($favourable_list, function ($a, $b)
        {
            if ($a['available'] == $b['available'])
            {
                if ($a['sort_order'] == $b['sort_order'])
                {
                    return 0;
                }
                else
                {
                    return $a['sort_order'] < $b['sort_order'] ? -1 : 1;
                }
            }
            else
            {
                return $a['available'] ? -1 : 1;
            }
        });
        
        // howang: only include available activity
        $favourable_list = array_filter($favourable_list, function ($favourable) { return $favourable['available']; });
        //print_r($favourable_list);
        $adjusted_goods  = array_column($_SESSION['adjusted_goods'], 'goods_id');
        
        /* Step 6 : Sort the favourable list by act_kind */
        $favourable_list2 = array();
        foreach($favourable_list as $favourable){
            $favourable_list2[$favourable["act_kind"]][] = $favourable;
        }

        ksort($favourable_list2);
        return $favourable_list2;
    }

    /** Add favourable goods into cart
     * Step 1: Get favourable info
     * Step 2: Check user can use favourable
     * Step 3: Check cart have this favourable
     * Step 4: Handle favourable type action
     */
    function add_favourable_goods($goods_id = 0, $can_enjoy_favourable_count = 1, $act_id = 0, $user_rank = 0, $user_id = 0, $platform, $gift = array(), $ajax = false)
    {
        global $db, $ecs, $_LANG, $_CFG, $smarty;
        require(ROOT_PATH . 'languages/' .$_CFG['lang']. '/yoho.php');
        
        // If undefind platform, return
        if(!in_array($platform, [self::ORDER_FEE_PLATFORM_WEB, self::ORDER_FEE_PLATFORM_POS]))
        return false;
        if($platform == self::ORDER_FEE_PLATFORM_WEB)$cart_table_name = 'cart';
        else if($platform == self::ORDER_FEE_PLATFORM_POS){
            $cart_table_name = 'cart1';
            //POS handle check order_mode
            $order_type = isset($_REQUEST['order_mode']) ? $_REQUEST['order_mode'] : 'normal';
            if(!in_array($order_type, ['normal', 'reserve'])){
                $message = "只有正常銷售及訂貨可以使用優惠";
                $this->handle_error_massage($message, $platform);
            }
        }
        
        $message = '';
        /* Step 1: Get favourable info */
        $favourable = favourable_info($act_id);
        if (empty($favourable))
        {
            $message = $_LANG['favourable_not_exist'];
            if(!$ajax){
                $this->handle_error_massage($message, $platform);
            }
            else{
                $GLOBALS['err']->add($msg, ERR_NOT_EXIST);
            }
        }
        /* Step 2: Check user can use favourable */
        if (!$this->favourable_available($favourable, $user_rank, $user_id, $platform))
        {
            $message = $_LANG['favourable_not_available'];
            if(!$ajax){
                $this->handle_error_massage($message, $platform);
            }
            else{
                $GLOBALS['err']->add($message, ERR_NO_FAV_NOT_AVAILABLE);
                return false;
            }
        }

        /* Step 3: Check cart have this favourable */
        $cart_favourable  = $this->cart_favourable($platform);
        if ($this->favourable_used($favourable, $cart_favourable, $platform))
        {
            $message = $_LANG['favourable_used'];
            if(!$ajax){
                $this->handle_error_massage($message, $platform);
            }
            else{
                $GLOBALS['err']->add($message, ERR_FAV_USED);
                return false;
            }
        }
        
        /* Step 3.5: Check cart have this gift */
        if ($this->favourable_gift_used($favourable, $platform, $gift))
        {
            $message = $_LANG['favourable_gift_used'];
            if(!$ajax){
                $this->handle_error_massage($message, $platform);
            }
            else{
                $GLOBALS['err']->add($message, ERR_FAV_GIFT_USED);
                return false;
            }
        }
        
        /* Step 4: Handle favourable type action */
        if ($favourable['act_type'] == FAT_GOODS)
        {
            if (empty($gift) && empty($goods_id))
            {
                $message = $_LANG['pls_select_gift'];
                if(!$ajax){
                    $this->handle_error_massage($message, $platform);
                }
                else{
                    $GLOBALS['err']->add($message, ERR_PLS_SELECT_GIFT);
                    return false;
                }
            }
            
            if(empty($gift) && $goods_id > 0){
                $gift[] = $goods_id;
            }

            // Check goods limit
            $cart_favourable_quantity = $this->cart_favourable_quantity($favourable, $platform);
            $can_enjoy_favourable_count = floor($cart_favourable_quantity / $favourable['min_quantity']);
            if($favourable["act_kind"]==1){
                if($favourable["act_type_ext"] == 0){
                    
                    $sql = 'SELECT goods_id, goods_number ' .
                    ' FROM ' . $GLOBALS['ecs']->table($cart_table_name) .
                    " WHERE session_id = '" . SESS_ID . "' AND rec_type = '" . CART_GENERAL_GOODS . "' AND is_gift = '".$favourable["act_id"]."'";
                    $cart_gifts = $GLOBALS['db']->getAll($sql);
                    $cart_gifts_qty_info = array();
                    foreach($cart_gifts as $cart_gift){
                        $cart_gifts_qty_info["goods_id"] = $cart_gifts_qty_info["goods_number"];
                    }
                
                    foreach($favourable["gift"] as $gift_info){
                        if($cart_gifts_qty_info[$gift_info['goods_id']] >= ($gift_info['qty'] * $can_enjoy_favourable_count)){
                            $message = $_LANG['gift_count_exceed'];
                            
                            if(!$ajax){
                                $this->handle_error_massage($message, $platform);
                            }
                            else{
                                $GLOBALS['err']->add($message, ERR_GIFT_COUNT_EXCEED);
                                return false;
                            }
                        }
                    }
                }
                else{
                    if($cart_favourable[$favourable['act_id']] >= ($favourable['act_type_ext'] * $can_enjoy_favourable_count)){
                        $message = $_LANG['gift_count_exceed'];
                        
                        if(!$ajax){
                            $this->handle_error_massage($message, $platform);
                        }
                        else{
                            $GLOBALS['err']->add($message, ERR_GIFT_COUNT_EXCEED);
                            return false;
                        }
                    }
                }

                $erpController = new \Yoho\cms\Controller\ErpController(); 
                $goods_stock = $erpController->getSumOfSellableProductsQtyByWarehouses($gift,null,true,null,false);

                $sql = "SELECT g.goods_id, g.goods_name, g.goods_thumb, g.goods_img, g.shop_price, g.is_on_sale, g.goods_sn, gperma " ." FROM " . $ecs->table('goods') . "as g " .
                "LEFT JOIN (SELECT id, table_name, lang, perma_link as gperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") gpl ON gpl.id = g.goods_id  AND gpl.table_name = 'goods' AND gpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
                "WHERE g.goods_id " .  db_create_in($gift);
                $goods_list = $db->getAll($sql);
                $goods_list = localize_db_result('goods', $goods_list);
                foreach ($gift as $key => $value)
                {
                    $goods = null;
                    foreach ($goods_list as $g)
                    {
                        if ($g['goods_id'] == $value)
                        {
                            $goods = $g;
                            break;
                        }
                    }
                
                    if(!$goods || !$goods['is_on_sale'])
                    {
                        unset($gift[$key]);
                        continue;
                    }
                
                    $stock = $goods_stock[$value];
                    $stock = $stock > 0 ? $stock : 0;
                    if($stock == 0){
                       unset($gift[$key]);
                       continue;
                    }
                }

            }
            else{
                $total_act_type_ext = intval($favourable['act_type_ext'] * $can_enjoy_favourable_count);
            }

            if ($total_act_type_ext > 0 && $cart_favourable[$favourable['act_id']] >= $total_act_type_ext)
            {
                $message = $_LANG['gift_count_exceed'];
                if(!$ajax){
                    $this->handle_error_massage($message, $platform);
                }
                else{
                    $GLOBALS['err']->add($message, ERR_GIFT_COUNT_EXCEED);
                    return false;
                }
            }

            // Add to cart
            //print_r($favourable['gift']);echo "<br>";
            //print_r($gift);echo "<br>";
            foreach ($favourable['gift'] as $g)
            {
                if (in_array($g['id'], $gift))
                {
                    
                    $sql = "SELECT g.goods_id, g.goods_name, g.goods_thumb, g.goods_img, g.shop_price, g.market_price, g.is_on_sale, g.goods_sn,  ".hw_goods_number_subquery('g.', 'real_goods_number') . ", " .
                    hw_reserved_number_subquery('g.') . " " ." FROM " . $ecs->table('goods') . "as g WHERE g.goods_id " .  db_create_in($g['id']);
                    $stock_info = $db->getRow($sql);

                    $stock = intval($stock_info['real_goods_number']) - intval($stock_info['reserved_number']);
                    $stock = $stock > 0 ? $stock : 0;

                    // Check goods in cart
                    $sql = "SELECT goods_name" .
                    " FROM " . $ecs->table($cart_table_name) .
                    " WHERE session_id = '" . SESS_ID . "'" .
                    " AND rec_type = '" . CART_GENERAL_GOODS . "'" .
                    " AND is_gift = '$act_id'" .
                    " AND goods_id " . db_create_in($g['id']);
                    $gift_name = $db->getCol($sql);
                    $already_in_cart = false;

                    if (!empty($gift_name)){
                        $already_in_cart = true;
                    }

                    if($favourable["act_kind"]==1 && $favourable["act_type_ext"]==0){
                        $quantity = $can_enjoy_favourable_count * $g['qty'];
                    }
                    elseif(($favourable["act_kind"]==1 && $favourable["act_type_ext"] > 0) || $favourable["act_kind"]==2){
                        $quantity = 1;
                    }

                    if(!$already_in_cart){
                        $this->add_gift_to_cart($act_id, $g['id'], $g['price'], $quantity, $stock, $cart_table_name);
                    }
                    else{
                        $this->add_gift_qun_to_cart($act_id, $g['id'], $g['price'], $quantity, $stock, $cart_table_name);
                    }
                    
                }
            }
        }
        elseif ($favourable['act_type'] == FAT_DISCOUNT)
        {
            $this->add_favourable_to_cart($act_id, $favourable['act_name'], $this->cart_favourable_amount($favourable, $platform) * (100 - $favourable['act_type_ext']) / 100, $cart_table_name);
        }
        elseif ($favourable['act_type'] == FAT_PRICE)
        {
            $this->add_favourable_to_cart($act_id, $favourable['act_name'], $favourable['act_type_ext'], $cart_table_name);
        }

        return true;
    }

    /** Handle cart favourable, If find not match favourable, remove it.
     *
     */
    function check_cart_favourable_goods($user_rank, $user_id, $platform)
    {
        global $db, $ecs, $_LANG, $_CFG;
        $available = true;
        
        // If undefind platform, return
        if(!in_array($platform, [self::ORDER_FEE_PLATFORM_WEB, self::ORDER_FEE_PLATFORM_POS]))
        return false;
        if($platform == self::ORDER_FEE_PLATFORM_WEB)$cart_table_name = 'cart';
        else if($platform == self::ORDER_FEE_PLATFORM_POS)$cart_table_name = 'cart1';
       
        /* Step 1: Get cart favourable */
        $cart_favourable  = $this->cart_favourable($platform);
        foreach ($cart_favourable as $act_id => $num) {
            $favourable = favourable_info($act_id);
            
            /* Step 2: Check cart can use favourable */
            if (!$this->favourable_available($favourable, $user_rank, $user_id, $platform))
            {
                /* Step 3: If not available, remove this. */
                $db->query("DELETE FROM ".$ecs->table($cart_table_name). " WHERE is_gift=$act_id AND session_id='".SESS_ID."'" );
                $available = false;
            }

            /* Step 3: */
            if ($favourable['act_type'] == FAT_GOODS)
            {
                
                $cart_favourable_quantity = $this->cart_favourable_quantity($favourable, $platform);
                $can_enjoy_favourable_count = floor($cart_favourable_quantity / $favourable['min_quantity']);

                $sql = 'SELECT goods_id, goods_number ' .
                        ' FROM ' . $GLOBALS['ecs']->table($cart_table_name) .
                        " WHERE session_id = '" . SESS_ID . "' AND rec_type = '" . CART_GENERAL_GOODS . "' AND is_gift = '".$favourable["act_id"]."'";
                $cart_gifts = $GLOBALS['db']->getAll($sql);
                $cart_gifts_qty_info = array();
                foreach($cart_gifts as $cart_gift){
                    $cart_gifts_qty_info[$cart_gift["goods_id"]] = $cart_gift["goods_number"];
                }

                if($favourable["act_kind"]==1){
                    if($favourable["act_type_ext"] == 0){
                        foreach($favourable["gift"] as $gift){
                            //print_r($gift);
                            //echo $cart_gifts_qty_info[$gift['id']]."<br>";
                            //echo ($gift['qty'] * $can_enjoy_favourable_count)."<br>";
                            if($cart_gifts_qty_info[$gift['id']] > ($gift['qty'] * $can_enjoy_favourable_count)){
                                $db->query("DELETE FROM ".$ecs->table($cart_table_name). " WHERE is_gift=$act_id AND session_id='".SESS_ID."'" );
                                $available = false;
                            }
                        }
                    }
                    else{
                        //echo $cart_favourable[$favourable['act_id']]."<br>";
                        //echo ($favourable['act_type_ext'] * $can_enjoy_favourable_count)."<br>";
                        if($cart_favourable[$favourable['act_id']] > ($favourable['act_type_ext'] * $can_enjoy_favourable_count)){
                            $db->query("DELETE FROM ".$ecs->table($cart_table_name). " WHERE is_gift=$act_id AND session_id='".SESS_ID."'" );
                            $available = false;
                        }
                    }
                }
                else{
                    if($favourable["act_type_ext"] == 0){
                        foreach($favourable["gift"] as $gift){
                            if($cart_gifts_qty_info[$gift['id']] > $can_enjoy_favourable_count){
                                $db->query("DELETE FROM ".$ecs->table($cart_table_name). " WHERE is_gift=$act_id AND session_id='".SESS_ID."'" );
                                $available = false;
                            }
                        }
                    }
                    else{
                        $total_act_type_ext = intval($favourable['act_type_ext'] * $can_enjoy_favourable_count);

                        if($cart_favourable[$favourable['act_id']] > $total_act_type_ext){
                            $db->query("DELETE FROM ".$ecs->table($cart_table_name). " WHERE is_gift=$act_id AND session_id='".SESS_ID."'" );
                            $available = false;
                        }
                    }
                    
                }

            }

        }

        return $available;
    }

    /**
     * 取得购物车中已有的优惠活动及数量
     * @return  array
     */
    function cart_favourable($platform = self::ORDER_FEE_PLATFORM_WEB)
    {
        global $db, $ecs, $_LANG, $_CFG;
        $list = array();
        if($platform == self::ORDER_FEE_PLATFORM_WEB)$cart_table_name = 'cart';
        else if($platform == self::ORDER_FEE_PLATFORM_POS)$cart_table_name = 'cart1';

        $sql = "SELECT is_gift, SUM(goods_number) AS num " .
                "FROM " . $ecs->table($cart_table_name) .
                " WHERE session_id = '" . SESS_ID . "'" .
                " AND rec_type = '" . CART_GENERAL_GOODS . "'" .
                " AND is_gift > 0" .
                " GROUP BY is_gift";
        $res = $db->query($sql);
        while ($row = $db->fetchRow($res))
        {
            $list[$row['is_gift']] = $row['num'];
        }

        return $list;
    }

    function cart_favourable_goods($platform = self::ORDER_FEE_PLATFORM_WEB, $favourable_id){
        global $db, $ecs, $_LANG, $_CFG;
        $list = array();
        if($platform == self::ORDER_FEE_PLATFORM_WEB)$cart_table_name = 'cart';
        else if($platform == self::ORDER_FEE_PLATFORM_POS)$cart_table_name = 'cart1';

        $sql = "SELECT goods_id, goods_number " .
                "FROM " . $ecs->table($cart_table_name) .
                " WHERE session_id = '" . SESS_ID . "'" .
                " AND rec_type = '" . CART_GENERAL_GOODS . "'" .
                " AND is_gift = $favourable_id";
        $res = $db->query($sql);
        while ($row = $db->fetchRow($res))
        {
            $list[$row['goods_id']] = $row['goods_number'];
        }

        return $list;
    }

    /**
     * 取得优惠范围描述
     * @param   array   $favourable     优惠活动
     * @return  string
     */
    function act_range_desc($favourable)
    {
        if ($favourable['act_range'] == FAR_BRAND)
        {
            $sql = "SELECT brand_name FROM " . $GLOBALS['ecs']->table('brand') .
                    " WHERE brand_id " . db_create_in($favourable['act_range_ext']);
            return join(',', $GLOBALS['db']->getCol($sql));
        }
        elseif ($favourable['act_range'] == FAR_CATEGORY)
        {
            $sql = "SELECT cat_name FROM " . $GLOBALS['ecs']->table('category') .
                    " WHERE cat_id " . db_create_in($favourable['act_range_ext']);
            return join(',', $GLOBALS['db']->getCol($sql));
        }
        elseif ($favourable['act_range'] == FAR_GOODS)
        {
            $sql = "SELECT goods_name FROM " . $GLOBALS['ecs']->table('goods') .
                    " WHERE goods_id " . db_create_in($favourable['act_range_ext']);
            return join(',', $GLOBALS['db']->getCol($sql));
        }
        else
        {
            return '';
        }
    }

    /**
     * 根据购物车判断是否可以享受某优惠活动
     * @param   array   $favourable     优惠活动信息
     * @return  bool
     */
    function favourable_available($favourable, $user_rank = 0, $user_id = 0, $platform, $add_goods_id=null)
    {
        /* 会员等级是否符合 */
        $favourable_user_rank = explode(',', $favourable['user_rank']);
        $user_rank = intval($user_rank);
        if (!in_array($user_rank, $favourable_user_rank))
        {
            if($add_goods_id){
                $msg = _L('checkout_error_favourable_user_rank_not_enought', '會員等級不合適，因此不能加入購物車。');
                $GLOBALS['err']->add($msg, ERR_FAV_USER_RANK_NOT_ENOUGHT);
            }
            return false;
        }

        if(!empty($favourable['users_list'])){
            $users_list = json_decode($favourable['users_list']);
            // If user_id not in users_list, continue.
            if(!in_array($user_id, $users_list)){
                if($add_goods_id){
                    $msg = _L('checkout_error_favourable_user_require_not_enought', '不符合購買資格，因此不能加入購物車。');
                    $GLOBALS['err']->add($msg, ERR_FAV_USER_REQUIRE_NOT_ENOUGHT);
                }
                return false;
            }
        }

        /* 优惠范围内的商品總數 */
        $quantity = $this->cart_favourable_quantity($favourable, $platform);
        if($add_goods_id){
            $quantity += 1;

            $show_msg = true;
        }

        if($quantity < $favourable['min_quantity']){
            if($show_msg){
                $msg = sprintf(_L('checkout_error_favourable_below_min_require', '您購買的相關產品總數沒有達到此優惠的最低購買總數 %s ，因此不能加入購物車。'), $favourable['min_quantity']);
                $GLOBALS['err']->add($msg, ERR_FAV_REQUIRE_NOT_ENOUGHT);
            }
            return false;
        }

        /* expired? */
        $now = gmtime();
        if (!($favourable['start_time_unformated'] <= $now && $favourable['end_time_unformated'] >= $now))
        {
            //print_r($favourable);
            //die($favourable['start_time_unformated']."|".$now."|".$favourable['end_time_unformated']);
            return false;
        }
        
        return $this->check_favourible_amount($favourable, $platform, $add_goods_id);
    }

    function check_favourible_amount($favourable, $platform, $add_goods_id = null){
        
        $amount = $this->cart_favourable_amount($favourable, $platform);
        
        if($add_goods_id){
            $final_price     = get_final_price($add_goods_id, 1, true, array(), $_SESSION['user_rank']);
            $goods_price     = $final_price['final_price'];
            $amount += $goods_price;

            $show_msg = true;
        }

        if($amount < $favourable['min_amount']){
            if($show_msg){
                $msg = sprintf(_L('checkout_error_favourable_below_min_amount', '您購買的相關產品總值沒有達到此優惠的最低要求 %s ，因此不能加入購物車。'), $favourable['min_amount']);
                $GLOBALS['err']->add($msg, ERR_FAV_AMOUNT_NOT_ENOUGHT);
            }
            return false;
        }
        return true;
    }

    /**
     * 根据购物车判断是否可以享受某优惠活动
     * @param   array   $favourable     优惠活动信息
     * @return  bool
     */
    function favourable_available_by_goods($favourable, $user_rank = 0, $user_id = 0, $goods_id = 0, $platform)
    {
        /* 会员等级是否符合 */
        $favourable_user_rank = explode(',', $favourable['user_rank']);
        $user_rank = intval($user_rank);
        if (!in_array($user_rank, $favourable_user_rank))
        {
            return false;
        }
        
        if(!empty($favourable['users_list'])){
            $users_list = json_decode($favourable['users_list']);
            // If user_id not in users_list, continue.
            if(!in_array($user_id, $users_list)){
                return false;
            }
        }

        //
        /* 优惠范围内的商品總數 */
        $include_goods = false;
        if($favourable['act_range'] == FAR_GOODS){
            $act_range_ext = explode(',', $favourable['act_range_ext']);
            if(in_array($goods_id, $act_range_ext)){
                $include_goods = true;
            }
        }
        
        //$include_goods = $this->favourable_include_goods($favourable, $goods_id, $platform);
        return $include_goods;
    }

    /**
     * check the goods hv favourable?
     * @param   array   $favourable     优惠活动
     * @return  float
     */
    function favourable_include_goods($favourable, $goods_id, $platform)
    {
        if($platform == self::ORDER_FEE_PLATFORM_WEB)$cart_table_name = 'cart';
        else if($platform == self::ORDER_FEE_PLATFORM_POS)$cart_table_name = 'cart1';
        else return false;

        $include_goods = false;

        /* 查询优惠范围内商品总额的sql */
        $sql = "SELECT 1 " .
                "FROM " . $GLOBALS['ecs']->table('goods') . " " .
                "WHERE goods_id = " . $goods_id . " ";

        /* 根据优惠范围修正sql */
        if ($favourable['act_range'] == FAR_ALL)
        {
            // sql do not change
        }
        elseif ($favourable['act_range'] == FAR_CATEGORY)
        {
            /* 取得优惠范围分类的所有下级分类 */
            $id_list = array();
            $cat_list = explode(',', $favourable['act_range_ext']);
            foreach ($cat_list as $id)
            {
                $id_list = array_merge($id_list, array_keys(cat_list(intval($id), 0, false)));
            }

            $sql .= "AND cat_id " . db_create_in($id_list);
        }
        elseif ($favourable['act_range'] == FAR_BRAND)
        {
            $id_list = explode(',', $favourable['act_range_ext']);

            $sql .= "AND brand_id " . db_create_in($id_list);
        }
        else
        {
            $id_list = explode(',', $favourable['act_range_ext']);

            $sql .= "AND goods_id " . db_create_in($id_list);
        }
        //echo $sql."<br>";
        /* 优惠范围内的商品总额 */
        $goods = $GLOBALS['db']->getAll($sql);
        if(count($goods) > 0){
            $include_goods = true;
        }

        return $include_goods;
    }

    /**
     * 取得购物车中某优惠活动范围内的購買總數
     * @param   array   $favourable     优惠活动
     * @return  float
     */
    function cart_favourable_quantity($favourable, $platform)
    {
        if($platform == self::ORDER_FEE_PLATFORM_WEB)$cart_table_name = 'cart';
        else if($platform == self::ORDER_FEE_PLATFORM_POS)$cart_table_name = 'cart1';
        else return false;
        /* 查询优惠范围内商品总额的sql */
        $sql = "SELECT SUM(c.goods_number) " .
                "FROM " . $GLOBALS['ecs']->table($cart_table_name) . " AS c, " . $GLOBALS['ecs']->table('goods') . " AS g " .
                "WHERE c.goods_id = g.goods_id " .
                "AND c.session_id = '" . SESS_ID . "' " .
                "AND c.rec_type = '" . CART_GENERAL_GOODS . "' " .
                "AND c.is_gift = 0 " .
                "AND c.is_package = 0 " .
                "AND c.flashdeal_id = 0 " .
                "AND c.goods_id > 0 ";
        /* 根据优惠范围修正sql */
        if ($favourable['act_range'] == FAR_ALL)
        {
            // sql do not change
        }
        elseif ($favourable['act_range'] == FAR_CATEGORY)
        {
            /* 取得优惠范围分类的所有下级分类 */
            $id_list = array();
            $cat_list = explode(',', $favourable['act_range_ext']);
            foreach ($cat_list as $id)
            {
                $id_list = array_merge($id_list, array_keys(cat_list(intval($id), 0, false)));
            }

            $sql .= "AND g.cat_id " . db_create_in($id_list);
        }
        elseif ($favourable['act_range'] == FAR_BRAND)
        {
            $id_list = explode(',', $favourable['act_range_ext']);

            $sql .= "AND g.brand_id " . db_create_in($id_list);
        }
        else
        {
            $id_list = explode(',', $favourable['act_range_ext']);

            $sql .= "AND g.goods_id " . db_create_in($id_list);
        }

        /* 优惠范围内的商品总额 */
        return $GLOBALS['db']->getOne($sql);
    }
    /**
     * 取得购物车中某优惠活动范围内的总金额
     * @param   array   $favourable     优惠活动
     * @return  float
     */
    function cart_favourable_amount($favourable, $platform)
    {
        if($platform == self::ORDER_FEE_PLATFORM_WEB)$cart_table_name = 'cart';
        else if($platform == self::ORDER_FEE_PLATFORM_POS)$cart_table_name = 'cart1';
        else return false;
        /* 查询优惠范围内商品总额的sql */
        $sql = "SELECT SUM(c.goods_price * c.goods_number) " .
                "FROM " . $GLOBALS['ecs']->table($cart_table_name) . " AS c, " . $GLOBALS['ecs']->table('goods') . " AS g " .
                "WHERE c.goods_id = g.goods_id " .
                "AND c.session_id = '" . SESS_ID . "' " .
                "AND c.rec_type = '" . CART_GENERAL_GOODS . "' " .
                "AND c.is_gift = 0 " .
                "AND c.is_package = 0 " .
                "AND c.goods_id > 0 ";

        /* 根据优惠范围修正sql */
        if ($favourable['act_range'] == FAR_ALL)
        {
            // sql do not change
        }
        elseif ($favourable['act_range'] == FAR_CATEGORY)
        {
            /* 取得优惠范围分类的所有下级分类 */
            $id_list = array();
            $cat_list = explode(',', $favourable['act_range_ext']);
            foreach ($cat_list as $id)
            {
                $id_list = array_merge($id_list, array_keys(cat_list(intval($id), 0, false)));
            }

            $sql .= "AND g.cat_id " . db_create_in($id_list);
        }
        elseif ($favourable['act_range'] == FAR_BRAND)
        {
            $id_list = explode(',', $favourable['act_range_ext']);

            $sql .= "AND g.brand_id " . db_create_in($id_list);
        }
        else
        {
            $id_list = explode(',', $favourable['act_range_ext']);

            $sql .= "AND g.goods_id " . db_create_in($id_list);
        }

        /* 优惠范围内的商品总额 */
        return $GLOBALS['db']->getOne($sql);
    }

    /**
     * 购物车中是否已经有某优惠
     * @param   array   $favourable     优惠活动
     * @param   array   $cart_favourable购物车中已有的优惠活动及数量
     */
    function favourable_gift_used($favourable, $platform, $gift = array())
    {
        if($platform == self::ORDER_FEE_PLATFORM_WEB)$cart_table_name = 'cart';
        else if($platform == self::ORDER_FEE_PLATFORM_POS)$cart_table_name = 'cart1';

        if ($favourable['act_type'] == FAT_GOODS)
        {
            $quantity = $this->cart_favourable_quantity($favourable, $platform);
            if($gift){
                $cart_favourable_goods = $this->cart_favourable_goods($platform, $favourable["act_id"]);
                foreach($gift as $gift_id){
                    if($cart_favourable_goods[$gift_id] >= floor($quantity / $favourable['min_quantity'])){
                        return true;
                    }
                }
            }
        }
    }
    
    function favourable_used($favourable, $cart_favourable, $platform)
    {
        if($platform == self::ORDER_FEE_PLATFORM_WEB)$cart_table_name = 'cart';
        else if($platform == self::ORDER_FEE_PLATFORM_POS)$cart_table_name = 'cart1';

        if ($favourable['act_type'] == FAT_GOODS)
        {
            $cart_favourable_quantity = $this->cart_favourable_quantity($favourable, $platform);
            $can_enjoy_favourable_count = floor($cart_favourable_quantity / $favourable['min_quantity']);
            if($favourable["act_kind"]==1){
                if($favourable["act_type_ext"] == 0){
                    
                    $sql = 'SELECT goods_id, goods_number ' .
                    ' FROM ' . $GLOBALS['ecs']->table($cart_table_name) .
                    " WHERE session_id = '" . SESS_ID . "' AND rec_type = '" . CART_GENERAL_GOODS . "' AND is_gift = '".$favourable["act_id"]."'";
                    $cart_gifts = $GLOBALS['db']->getAll($sql);
                    $cart_gifts_qty_info = array();
                    foreach($cart_gifts as $cart_gift){
                        $cart_gifts_qty_info[$cart_gift["goods_id"]] = $cart_gift["goods_number"];
                    }

                    foreach($favourable["gift"] as $gift){
                        if($cart_gifts_qty_info[$gift['goods_id']] >= ($gift['qty'] * $can_enjoy_favourable_count)){
                            return true;
                        }
                    }
                }
                else{
                    if($cart_favourable[$favourable['act_id']] >= ($favourable['act_type_ext'] * $can_enjoy_favourable_count)){
                        return true;
                    }
                }
            }
            else{
                $total_act_type_ext = intval($favourable['act_type_ext'] * $can_enjoy_favourable_count);
            }

            return isset($cart_favourable[$favourable['act_id']]) &&
                $cart_favourable[$favourable['act_id']] >= $total_act_type_ext &&
                $total_act_type_ext > 0;
        }
        else
        {
            return isset($cart_favourable[$favourable['act_id']]);
        }
    }

    /**
     * 添加优惠活动（赠品）到购物车
     * @param   int     $act_id     优惠活动id
     * @param   int     $id         赠品id
     * @param   float   $price      赠品价格
     */
    function add_gift_to_cart($act_id, $id, $price, $goods_number, $stock, $cart_table_name = 'cart')
    {
        $sql = "INSERT INTO " . $GLOBALS['ecs']->table($cart_table_name) . " (" .
                    "user_id, session_id, goods_id, goods_sn, goods_name, market_price, goods_price, ".
                    "goods_number, is_real, extension_code, parent_id, is_gift, rec_type ) ".
                "SELECT '$_SESSION[user_id]', '" . SESS_ID . "', goods_id, goods_sn, goods_name, market_price, ".
                    "'$price', '$goods_number', is_real, extension_code, 0, '$act_id', '" . CART_GENERAL_GOODS . "' " .
                "FROM " . $GLOBALS['ecs']->table('goods') .
                " WHERE goods_id = '$id'";
        $GLOBALS['db']->query($sql);
    }

    function add_gift_qun_to_cart($act_id, $id, $price, $goods_number, $stock, $cart_table_name = 'cart')
    {
        if($stock == 0){
            return false;
        }

        if($stock >= $goods_number){
            if($goods_number>1){
                $sql = "UPDATE " . $GLOBALS['ecs']->table($cart_table_name) . " set goods_number='$goods_number' " .
                    "where user_id = '$_SESSION[user_id]' and session_id = '" . SESS_ID . "' and goods_id = '$id' and is_gift = '$act_id' and rec_type = '" . CART_GENERAL_GOODS . "'";
            }
            else{
                $sql = "UPDATE " . $GLOBALS['ecs']->table($cart_table_name) . " set goods_number=goods_number+1 " .
                    "where user_id = '$_SESSION[user_id]' and session_id = '" . SESS_ID . "' and goods_id = '$id' and is_gift = '$act_id' and rec_type = '" . CART_GENERAL_GOODS . "'";
            }
        }
        else{
            $sql = "UPDATE " . $GLOBALS['ecs']->table($cart_table_name) . " set goods_number='$stock' " .
                "where user_id = '$_SESSION[user_id]' and session_id = '" . SESS_ID . "' and goods_id = '$id' and is_gift = '$act_id' and rec_type = '" . CART_GENERAL_GOODS . "'";
        }
        
        $GLOBALS['db']->query($sql);
    }

    /**
     * 添加优惠活动（非赠品）到购物车
     * @param   int     $act_id     优惠活动id
     * @param   string  $act_name   优惠活动name
     * @param   float   $amount     优惠金额
     */
    function add_favourable_to_cart($act_id, $act_name, $amount, $cart_table_name = 'cart')
    {
        $sql = "INSERT INTO " . $GLOBALS['ecs']->table($cart_table_name) . "(" .
                    "user_id, session_id, goods_id, goods_sn, goods_name, market_price, goods_price, ".
                    "goods_number, is_real, extension_code, parent_id, is_gift, rec_type ) ".
                "VALUES('$_SESSION[user_id]', '" . SESS_ID . "', 0, '', '$act_name', 0, ".
                    "'" . (-1) * $amount . "', 1, 0, '', 0, '$act_id', '" . CART_GENERAL_GOODS . "')";
        $GLOBALS['db']->query($sql);
    }


    function getInsurancePlanByGoodsID($goods_id){
        global $db, $ecs, $_LANG, $_CFG;
        $sql = "SELECT ig.insurance_goods_id, ig.price_percent, ig.insurance_id, IFNULL(il.name, i.name) AS name, i.code, i.duration, i.target_goods_id, i.status, g.shop_price ".
                "FROM ".$ecs->table('insurance_goods')." AS ig ".
                "LEFT JOIN ".$ecs->table('insurance')." AS i ON (i.insurance_id = ig.insurance_id AND i.duration = ig.duration) ".
                "LEFT JOIN ".$ecs->table('insurance_lang')." AS il ON (il.insurance_id = ig.insurance_id AND il.lang = '".$_CFG['lang']."')".
                "LEFT JOIN ".$ecs->table('goods')." AS g ON g.goods_id = ig.goods_id ".
                "WHERE ig.goods_id = '".$goods_id."' AND i.status = 1 ".
                //"AND i.status = '1' ".
                "ORDER BY i.insurance_id ASC,  i.duration ASC ";
        $ins_res = $db->getAll($sql);
        if (empty($ins_res)){
            return array();
        } else {
            $ins_list_by_plan = [];
            foreach ($ins_res as $key => $ins){
                $price = bcmul(bcmul($ins['price_percent'], 0.01, 3), $ins['shop_price']);
                $price = ceil($price);

                if (!empty($price) && $price > 0){
                    $ins['price'] = $price;
                } else {
                    unset($ins_res[$key]);
                    continue;
                }

                if(!isset($ins_list_by_plan[$ins['code']]['name'])){
                    $ins_list_by_plan[$ins['code']]['name'] = $ins['name'];
                }

                if(!isset($ins_list_by_plan[$ins['code']][$ins['duration']])){
                    $ins_list_by_plan[$ins['code']]['plan'][$ins['duration']] = $ins;
                }

                if(!isset($ins_list_by_plan[$ins['code']]['tc_link'])){
                    $ins_list_by_plan[$ins['code']]['tc_link'] = $_LANG['goods_buy_service_terms_conditions_link_'.$ins['code']];
                }
            }
            foreach ($ins_list_by_plan as $key => $plan){
                if(is_array($plan)){
                    $ins_list_by_plan[$key]['plan'][-1] =array('duration' => -1, 'target_goods_id' => -1);
                }
            }
            return $ins_list_by_plan;
        }
    }

    function getTnsuranceDurationByInsGoodsId($target_goods_id){
        global $db, $ecs;
        $sql = "SELECT duration ".
                "FROM ".$ecs->table('insurance')." ".
                "WHERE target_goods_id = '".$target_goods_id."' ".
                "ORDER BY insurance_id ASC ";
        $row_duration = $db->getRow($sql);
        if (!empty($row_duration) && $row_duration['duration'] > 0){
            return $row_duration['duration'];
        } else {
            return 0;
        }
    }

    function getClaimTimesByOrderIdOrderGoodsId($order_id, $rec_id){
        global $db, $ecs;
        $sql = "SELECT COUNT(claim_id) ".
                "FROM ".$ecs->table('insurance_claim')." ".
                "WHERE order_id = '".$order_id."' ".
                "AND order_goods_id = '".$rec_id."' ";
        $claim_times = $db->getOne($sql);
        return $claim_times;
    }

    public function handle_error_massage($message, $platform)
    {
        $output_json = function($obj)
        {
            header('Content-Type: application/json');
            echo json_encode($obj);
            exit;
        };

        if($platform == self::ORDER_FEE_PLATFORM_WEB){
            show_message($message);
        } elseif ($platform == self::ORDER_FEE_PLATFORM_POS){
            $output_json(array(
                'status' => 0,
                'error' => $message
            ));
        }
    }

    /** Get pos order, order_goods, total
     * @return array [goods_list, total, order]
    */
    public function get_pos_order_info($user_rank){

        // Get cart_goods
        $cart_goods = get_cart_goods_1($user_rank);
		usort($cart_goods['goods_list'], function ($a, $b) { return intval($a['rec_id']) - intval($b['rec_id']); });
		$goods_list = array();
		foreach ($cart_goods['goods_list'] as $goods)
		{
			$goods_list[] = array(
				'goods_id' => $goods['goods_id'],
				'goods_name' => $goods['goods_name'],
				'goods_number' => $goods['goods_number'],
				'goods_price' => $goods['goods_price'],
				'subtotal' => $goods['subtotal'],
				'is_vip' => $goods['is_vip'],
				'is_gift' => $goods['is_gift']
			);
		}
		/*
		* 取得訂單信息
		*/
		$order = flow_order_info();

		// Update shipping / payment method
		$order['shipping_id'] = $shipping;
		$order['pay_id'] = $payment;

		// Update order type
		$order_type = isset($_REQUEST['order_mode']) ? $_REQUEST['order_mode'] : 'normal';
		if($order_type == 'sales-agent'){ $order['order_type'] = self::DB_ORDER_TYPE_SALESAGENT; } else {
			$order['order_type'] = NULL;
		}

		$consignee['country'] = isset($_REQUEST['country']) ? intval($_REQUEST['country']) : 0;
		$consignee['province'] = isset($_REQUEST['province']) ? intval($_REQUEST['province']) : 0;
		$consignee['city'] = isset($_REQUEST['city']) ? intval($_REQUEST['city']) : 0;
		$consignee['district'] = isset($_REQUEST['district']) ? intval($_REQUEST['district']) : 0;

		/*
		* 計算訂單的費用
		*/
		$flow_type = isset($_SESSION['flow_type']) ? intval($_SESSION['flow_type']) : CART_GENERAL_GOODS;
		$cart_goods = cart_goods_1($flow_type, null, null, $user_rank); // 取得商品列表，計算合計
        $total = $this->order_fee_flow($order, $cart_goods, $consignee, self::ORDER_FEE_PLATFORM_POS);

        return [
            'goods_list' => $goods_list,
            'total' => $total,
            'order' => $order
        ];
    }

    function update_order_address($address, $order_id){
        if(empty($order_id)) return false;

        global $ecs, $db;
        foreach($address as $ak => $a_value) {
            if(is_string($a_value))$address[$ak] = $this->escape_string($a_value);
        }
        // Find order have address
        $order_address_id = $db->getOne("SELECT order_address_id FROM ".$ecs->table('order_address')." WHERE order_id = $order_id LIMIT 1");

        if($order_address_id){
            $order_address = new Model\OrderAddress($order_address_id);
            $order_address->update($address);

            return $order_address_id;
        } else {
            $address['order_id'] = $order_id;
            $staying_time = isset($_COOKIE['order_staying_time']) ? intval($_COOKIE['order_staying_time']) : 0;
            $address['staying_time'] = $staying_time;
            $order_address = new Model\OrderAddress(null, $address);
            $order_address_id = $order_address->getId();
            if (isset($_COOKIE['order_staying_time'])) {
                unset($_COOKIE['order_staying_time']);
                setcookie('order_staying_time', NULL, time() - 3600, '/'); // empty value and old timestamp
            }

            $db->query("UPDATE ".$ecs->table('order_info')." SET address_id = $order_address_id WHERE order_id = ".$order_id);
            return $order_address_id;
        }
    }

    /** Batch create sales agent order
	 *
	 */
	public function batch_create_sa_order()
	{
		global $db,$ecs,$_LANG;
		require(ROOT_PATH . 'includes/lib_order.php');
        require(ROOT_PATH . 'includes/lib_howang.php');
        include_once(ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/common.php');
		$addressController = new AddressController();

		$sa_model = new Model\Salesagent($_REQUEST['sa_id']);
		$salesagent = $sa_model->data;
		if(empty($salesagent)) return json_encode(['error' => '沒有此代理銷售資料', 'status' => 0]);

		if(empty($_REQUEST['prepare_list'])) return json_encode(['status' => 0, 'error'=>'沒有訂單資料']);
        $action_note = "系統批量入單(".$salesagent['name'].") - ".local_strtotime(local_date('Y-m-d H:i:s'));
		// order list
		$prepare_list = json_decode(stripslashes($_REQUEST['prepare_list']),true);
		$order_fee_platform = self::ORDER_FEE_PLATFORM_POS;
		$operator   = ($_REQUEST['salesperson']) ? $_REQUEST['salesperson'] : $_SESSION['admin_name'];

		// Address Handle
		foreach ($prepare_list as $p_key => $prepare_order) {
			$prepare_order['country_code'] = $prepare_order['country_code'] ? $prepare_order['country_code'] : 'HK';
			$country = get_region_by_country_code(strtolower($prepare_order['country_code']));
			$address_list[$p_key]['address'] = $prepare_order['address'];
			$address_list[$p_key]['country'] = $country;
        }
        $address_book = $addressController->batch_address_curl($address_list);

		$success_order = [];
        foreach ($prepare_list as $p_key => $prepare_order) {
            $order = [];

            // check user is new user?
            $user_id = $prepare_order['not_show']['user_id'];

            // New user
            if(!$user_id || $user_id == 'false') {

				// In this time, we try to check again
				include_once(ROOT_PATH . 'includes/lib_passport.php');
				$userController = new UserController();
				$user_id = $userController->get_user_id($prepare_order['not_show']['phone'], $prepare_order['email']);
				if(!$user_id) {
					$username = $prepare_order['not_show']['phone'];
					$email    = $prepare_order['email'];
					$email    = is_email($email) ? $email : $username.'@yohohongkong.com';

					while ($username == "" || admin_registered($username))
					{
						$username = $prepare_order['not_show']['phone'] ? $prepare_order['not_show']['phone'] . "_" : "rand";
						$username .= mt_rand(100, 999);
					}

					$password = "yohohongkong";
					$other['msn']          = '';
					$other['qq']           = '';
					$other['office_phone'] = '';
					$other['home_phone']   = empty($prepare_order['not_show']['phone']) ? '' : make_semiangle(trim( $prepare_order['not_show']['phone']));
					$other['mobile_phone'] = empty($prepare_order['not_show']['phone']) ? '' : make_semiangle(trim( $prepare_order['not_show']['phone']));
					$other['is_wholesale'] = false;
					$other['reg_time'] = local_strtotime(local_date('Y-m-d H:i:s'));
					$other['passchg_time'] = $update_data['reg_time']; // Added by howang: password change time
					$is_pos = true;
					$users =& init_users();
					if (!$users->add_user($username, $password, $email))
					{
                        include_once(ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/admin/user.php');
						/* 插入会员数据失败 */
						if ($users->error == ERR_INVALID_USERNAME)
						{
							$msg = $_LANG['username_invalid'];
						}
						elseif ($users->error == ERR_USERNAME_NOT_ALLOW)
						{
							$msg = $_LANG['username_not_allow'];
						}
						elseif ($users->error == ERR_USERNAME_EXISTS)
						{
							$msg = $_LANG['username_exists'];
						}
						elseif ($users->error == ERR_INVALID_EMAIL)
						{
							$msg = $_LANG['email_invalid'];
						}
						elseif ($users->error == ERR_EMAIL_NOT_ALLOW)
						{
							$msg = $_LANG['email_not_allow'];
						}
						elseif ($users->error == ERR_EMAIL_EXISTS)
						{
							$msg = $_LANG['email_exists'];
						}
						else
						{
							//die('Error:'.$users->error_msg());
						}

                        $error = '會員註冊失敗('.$msg.')';
						$error_order[$p_key] = $prepare_order;
                        $error_order[$p_key]['error'] = $error;
						continue;
					} else {
						// 取得新註冊會員的 user_id
						$sql = "SELECT user_id FROM " . $GLOBALS['ecs']->table('users') . " WHERE user_name = '" . $username . "'";
						$user_id = $GLOBALS['db']->getOne($sql);

						/* 注册送积分 */
						if (!empty($GLOBALS['_CFG']['register_points']))
						{
							log_account_change($user_id, 0, 0, $GLOBALS['_CFG']['register_points'], $GLOBALS['_CFG']['register_points'], $GLOBALS['_LANG']['register_points']);
						}
						$GLOBALS['db']->autoExecute($GLOBALS['ecs']->table('users'), $other, 'UPDATE', 'user_id = ' . $user_id);
					}
				}

            }
            
            // If order have duplicate sa_number
            if($prepare_order['not_show']['duplicate_sa_number']) {
                $error = '重覆代理訂單號';
                $error_order[$p_key] = $prepare_order;
                $error_order[$p_key]['error'] = $error;
				continue;
            }
			$consignee = $address_book[$p_key];

			$consignee['address_id'] = 0;
			$consignee['consignee']  = $prepare_order['consignee'];
            $consignee['email']      = empty($prepare_order['email']) ? 0 : $prepare_order['email'];
            $consignee['zipcode']    = (empty($prepare_order['zip_code']) || $prepare_order['zip_code'] == 'NULL') ? $consignee['zipcode'] : make_semiangle(trim($prepare_order['zip_code']));
            $consignee['tel']        = empty($prepare_order['not_show']['phone']) ? '' : make_semiangle(trim($prepare_order['not_show']['phone']));
            $consignee['mobile']     = empty($prepare_order['not_show']['phone']) ? '' : make_semiangle(trim($prepare_order['not_show']['phone']));
            $consignee['best_time']  = '';
            $consignee['city']       = empty($consignee['city'])   ? 0   : $consignee['city'];
            $consignee['country']    = empty($consignee['country'])   ? 3409 : $consignee['country'];
            $consignee['district']   = empty($consignee['district']) ? 0 : $consignee['district'];

            $region = ['district'=>$consignee['district'],'city'=>$consignee['city'],'province'=>$consignee['province'],'country'=>$consignee['country']];
            $shipping_list = available_shipping_list($region);
            foreach ($shipping_list as $k => $shipping) {
                if (in_array($shipping['shipping_id'], [2, 4, 5])) {
                    unset($shipping_list[$k]);
                }
            }
            $shipping_list = array_values($shipping_list);
            // Default only get the first shipping and if not fund we will use '速遞'
			$shipping_id = empty($shipping_list[0]['shipping_id']) ? 3 : $shipping_list[0]['shipping_id'];

			$order = array(
				'shipping_id'     => $shipping_id,
				'pay_id'          => 11, // We hard code pay code=franzpay(自行結算)
				'pack_id'         => 0,
				'card_id'         => 0,
				'card_message'    => '',
				'surplus'         => 0.00,
				'integral'        => 0,
				'bonus_id'        => 0,
				'coupon_code'     => '',
				'need_inv'        => 0,
				'inv_type'        => '',
				'inv_payee'       => '',
				'inv_content'     => '',
				'postscript'      => '',
				'order_remarks'   => '',
				'serial_numbers'  => '',
				'how_oos'         => isset($_LANG['oos'][0]) ? addslashes($_LANG['oos'][0]) : '',
				'need_insure'     => 0,
				'user_id'         => $user_id,
				'add_time'        => gmtime(),
				'order_status'    => OS_UNCONFIRMED,
				'shipping_status' => SS_UNSHIPPED,
				'pay_status'      => PS_UNPAYED,
				'agency_id'       => get_agency_by_regions(array($consignee['country'], $consignee['province'], $consignee['city'], $consignee['district'])),
				'user_agency_id'  => 0,
				'discount'        => 0,
				'order_type'      => self::DB_ORDER_TYPE_SALESAGENT,
				'salesperson'     => $operator
			);
			/* 收货人信息 */
			foreach ($consignee as $key => $value)
			{
				$order[$key] = addslashes($value);
			}
			$cart_goods = [];
			foreach ($prepare_order['goods'] as $goods_id  => $goods) {
				if(!isset($goods['is_goods'])) {
					$cart_goods[$goods_id] = $goods;
				}
			}
			if(empty($cart_goods)) {
				$error = '沒有產品';
                $error_order[$p_key] = $prepare_order;
                $error_order[$p_key]['error'] = $error;
				continue;
            }
			// howang: 檢查是否包括需訂貨產品
			$goods_preorder = check_goods_preorder($cart_goods);
			if (!empty($goods_preorder))
			{
				$preorder_msg = '';
				$order_remarks_msg = '';
				foreach ($goods_preorder as $preorder)
				{
					$preorder_msg .= $preorder['goods_name'] . ' ' . $preorder['preorder_mode'] . ' ' . $preorder['pre_sale_days'] . '天';
					//$preorder_msg .= ' (' . $preorder['pre_sale_date_formatted'] . ')';
					$preorder_msg .= "\n";

					$order_remarks_msg .= $preorder['goods_name'] . ' 訂貨大約' . $preorder['pre_sale_days'] . "工作天\n";
				}
				// 商家備註
				$order['to_buyer'] = empty($order['to_buyer']) ? '' : $order['to_buyer'];
				// 客戶備註
				$order['postscript'] = empty($order['postscript']) ? '' : $order['postscript'];
				// 訂貨備註
				$order['order_remarks'] = addslashes($order_remarks_msg);
			}

			/* 订单中的总额 */
            $total = $this->order_fee_flow($order, $cart_goods, $consignee, $order_fee_platform, '1', true);

			$order['bonus']        = $total['bonus'];
			$order['coupon']       = $total['coupon'];
			$order['goods_amount'] = $total['goods_price'];
			$order['discount']     = $total['discount'];
			$order['surplus']      = $total['surplus'];
			$order['tax']          = $total['tax'];
			$order['cp_price']     = !empty($total['cp_price']) ? $total['cp_price'] : '0';

			/* 配送方式 */
			if ($order['shipping_id'] > 0)
			{
				$shipping = shipping_info($order['shipping_id']);
				$order['shipping_name'] = addslashes($shipping['shipping_name']);
			}
			$order['shipping_fee'] = $total['shipping_fee'];
			$order['insure_fee']   = $total['shipping_insure'];

			/* 支付方式 */
			if ($order['pay_id'] > 0)
			{
				$payment = payment_info($order['pay_id']);
				// $order['pay_name'] = addslashes($payment['pay_name']);
				$order['pay_name'] = addslashes($payment['display_name']);
			}
			$order['pay_fee'] = $total['pay_fee'];
			$order['cod_fee'] = $total['cod_fee'];
			$order['order_amount']  = number_format($total['amount'], 2, '.', '');

			// 代理銷售：初始為已確認、已付款、未發貨
			$order['order_status'] = OS_CONFIRMED;
			$order['pay_status'] = PS_PAYED;
			$order['shipping_status'] = SS_UNSHIPPED;
			$order['confirm_time'] = $order['add_time'];
			$order['pay_time'] = $order['add_time'];
			$order['shipping_time'] = 0;
			$order['money_paid'] = $order['order_amount'];
			$order['order_amount'] = 0;
			$order['type_id'] = $_REQUEST['sa_id'];
            $order['type_sn'] = $prepare_order['sa_number'];
            $order['from_ad'] = '0';
            $order['referer'] = !empty($_SESSION['referer']) ? addslashes($_SESSION['referer']) : '';
			$order['platform'] = self::ORDER_FEE_PLATFORM_POS;

			/* 插入订单表 */
			$error_no = 0;
			do
			{
				$order['order_sn'] = get_order_sn(); //获取新订单号
				$GLOBALS['db']->autoExecute($GLOBALS['ecs']->table('order_info'), $order, 'INSERT', '', 'SILENT');

				$error_no = $GLOBALS['db']->errno();

				if ($error_no > 0 && $error_no != 1062)
				{
					die($GLOBALS['db']->errorMsg());
				}
			}
			while ($error_no == 1062); //如果是订单号重复则重新提交数据

			$new_order_id = $db->insert_id();
			$order['order_id'] = $new_order_id;

			// Handle order_goods
			foreach($cart_goods as $goods_id => $goods) {
				$goods_price = ($goods['custom_price']) ? $goods['custom_price'] : $goods['org_price'];
				$sql = "INSERT INTO " . $ecs->table('order_goods') . "( " .
    			"order_id, goods_id, goods_name, goods_sn, product_id, goods_number, market_price, ".
    			"goods_price, goods_attr, is_real, extension_code, parent_id, is_gift, goods_attr_id, cost, sa_rate_id) ".
					" SELECT '$new_order_id', g.goods_id, g.goods_name, g.goods_sn, 0, '".$goods['goods_number']."', g.market_price, ".
					" '$goods_price', '', g.is_real, g.extension_code, 0, 0, '', g.cost, '".$goods['rate_id']."'  ".
					" FROM ". $ecs->table('goods') . " as g ".
					" WHERE g.goods_id = '".$goods['goods_id']."'";
				$db->query($sql);
            }
			/* Insert Order address */
			$order_address_id = $this->update_order_address($consignee, $new_order_id);
            $order['address_id'] = $order_address_id;

            $order['goods'] = $cart_goods;

			if ($order['money_paid'] > 0)
			{
				require_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_accounting.php');
				$actg = new \Accounting($this->db);
				$actgHooks = new \AccountingHooks($actg, $this->db);
				// Accounting System, order paid
				$actgHooks->orderPaid($new_order_id, $order['money_paid'], '', 0, false, $_SESSION['admin_id']);
			}
            order_action($order['order_sn'], $order['order_status'], $order['shipping_status'], $order['pay_status'], $action_note, $operator, 1);
			$success_order[$new_order_id] = $order;
		}

        clear_all_files();

        return json_encode(['status' => 1, 'success_order'=> $success_order, 'failed_list' => $error_order, 'a'=>$address_book]);
    }

    public function export_batch_order_excel()
    {
        global $db, $ecs, $_LANG;

        ini_set('memory_limit', '256M');
        define('PCLZIP_TEMPORARY_DIR', ROOT_PATH . 'temp/');
        include_once(ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/admin/order.php');
        require_once ROOT_PATH . 'includes/excel/PHPExcel.php';
        require_once ROOT_PATH . 'includes/excel/PHPExcel/IOFactory.php';
        \PHPExcel_Settings::setZipClass(\PHPExcel_Settings::PCLZIP);
        // Handle order data into excel
        $order_list = json_decode(stripslashes($_REQUEST['extra']),true);

        $sa_model = new Model\Salesagent($_REQUEST['sa_id']);
        $salesagent = $sa_model->data;
        $sa_name = $salesagent['name'];
        $filename = $sa_name.'_Batch Order'.'_'.local_date('Y-m-d').'.xlsx';
        $data = [];
        foreach ($order_list as $order_id => $order) {
            $data[$order_id]['order_sn']      = $order['order_sn'];
            $data[$order_id]['status']        = $_LANG['os'][$order['order_status']] . ',' . $_LANG['ps'][$order['pay_status']] . ',' . $_LANG['ss'][$order['shipping_status']];
            $data[$order_id]['goods']         = '';
            foreach($order['goods'] as $goods_id => $goods) {
                $goods_list[] = $goods['goods_name'] .' x '. $goods['goods_number'];
            }
            $data[$order_id]['goods']         = implode('\n', $goods_list);
            unset($goods_list);
            $data[$order_id]['type_sn']       = $order['type_sn'];
            $data[$order_id]['consignee']     = $order['consignee'];
            $data[$order_id]['mobile']        = $order['mobile'];
            $data[$order_id]['address']       = stripslashes($order['address']);
            $data[$order_id]['zipcode']       = $order['zipcode'];
            $data[$order_id]['email']         = $order['email'];
            $data[$order_id]['shipping_name'] = $order['shipping_name'];
            $data[$order_id]['money_paid']    = $order['money_paid'];
        }

        // Create new PHPExcel object
        $objPHPExcel = new \PHPExcel();

        // Create Col title
        $title_list = ['訂單號', '訂單狀態', '產品', '代理訂單號', '聯絡人', '電話', '地址', '郵遞區號', '電郵', '送貨方式', '金額'];
        foreach (range('A', 'K') as $char_key => $column){
            $objPHPExcel->getActiveSheet()->SetCellValue($column.'1', $title_list[$char_key]);
         }

        // Fill worksheet from values in array
        $objPHPExcel->getActiveSheet()->fromArray($data, null, 'A2');
        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Order');
        // Set AutoSize for name and email fields
        $lastrow = $objPHPExcel->getActiveSheet()->getHighestRow();
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getStyle('A2:A'.$lastrow)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);

        // Save Excel 2007 file
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.‌​sheet; charset=utf-8');
        header('Content-Disposition: attachment; filename=' . $filename);
        header('Cache-Control: max-age=0');
		$objWriter->save('php://output');
    }

    public function generator_order_info_excel($order_ids = [], $title = [])
    {
        global $db, $ecs, $_LANG;
        ini_set('memory_limit', '256M');
        define('PCLZIP_TEMPORARY_DIR', ROOT_PATH . 'temp/');
        include_once(ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/admin/order.php');
        // require(ROOT_PATH . 'includes/lib_order.php');
        require_once ROOT_PATH . 'includes/excel/PHPExcel.php';
        require_once ROOT_PATH . 'includes/excel/PHPExcel/IOFactory.php';
        \PHPExcel_Settings::setZipClass(\PHPExcel_Settings::PCLZIP);

        if(empty($title)) $title  = ['order_sn', 'all_status', 'consignee', 'phone', 'address', 'zipcode', 'email', 'shipping_name', 'total_fee', 'tracking_info'];

        $title_count = count($title);
        $filename = 'order_list'.'_'.local_date('Y-m-d').'.xls';
        // Create new PHPExcel object
        $objPHPExcel = new \PHPExcel();

        // Get order info
        $sql = "SELECT oi.*, l.logistics_name, do.invoice_no, (" . order_amount_field('oi.') . ") AS total_fee, (oi.money_paid + oi.order_amount) AS order_total ".
                "FROM ".$ecs->table('order_info')." as oi ".
                "LEFT JOIN ".$ecs->table('delivery_order')." as do ON do.order_id = oi.order_id ".
                "LEFT JOIN ".$ecs->table('logistics')." as l ON l.logistics_id = do.logistics_id ".
                "WHERE oi.order_id ".db_create_in($order_ids);
        $order_list = $db->getAll($sql);

        // handle order list data
        $data = [];
        foreach ($order_list as $key => $order) {
            // Handle tracking_info and order status
            $order['all_status']    = $_LANG['os'][$order['order_status']] . ',' . $_LANG['ps'][$order['pay_status']] . ',' . $_LANG['ss'][$order['shipping_status']];
            $order['sa_sn']         = ($order['order_type'] == self::DB_ORDER_TYPE_SALESAGENT) ? $order['type_sn'] : '';
            $order['ws_sn']         = ($order['order_type'] == self::DB_ORDER_TYPE_WHOLESALE)  ? $order['type_sn'] : '';
            $order['tracking_info'] = $order['logistics_name'].' '.$order['invoice_no'];
            $order['phone']         = ($order['tel']) ? $order['tel'] : $order['mobile'];
            $order['phone']         = str_replace('+', '', $order['phone']);
            $order['order_total']   = price_format($order['order_total'], false);
            $order['total_fee']     = price_format($order['total_fee'], false);

            foreach ($title as $key => $t) {
                $data[$order['order_id']][$t] = $order[$t];
                if($t == 'order_sn') $o_sn_key = chr(ord("A") + intval($key));
                if($t == 'phone')   $phone_key = chr(ord("A") + intval($key));
            }
        }
        $title_count = count($title);
        $last_title_char = chr(ord("A") + intval($title_count - 1));
        foreach (range('A', $last_title_char) as $char_key => $column){
            $word = $title[$char_key];
            $word = ($_LANG[$word]) ? $_LANG[$word] : $word;
            $objPHPExcel->getActiveSheet()->SetCellValue($column.'1', $word);
        }
        // Fill worksheet from values in array
        $objPHPExcel->getActiveSheet()->fromArray($data, null, 'A2');
        // $objPHPExcel->getActiveSheet()->SetCellValue('A2', json_encode($data));
        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Order');
        $lastrow = $objPHPExcel->getActiveSheet()->getHighestRow();
        $objPHPExcel->getActiveSheet()->getColumnDimension($o_sn_key)->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension($phone_key)->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getStyle($o_sn_key.'2:'.$o_sn_key.$lastrow)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
        $objPHPExcel->getActiveSheet()->getStyle($phone_key.'2:'.$phone_key.$lastrow)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
        // Create actual excel file data
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        // Output to browser
        header("Content-type: application/vnd.ms-excel; charset=utf-8");
        header("Content-Disposition: attachment; filename=$filename");
        $objWriter->save('php://output');
        exit;
    }

    /* Count user order (without 已取消, 无效, 退货) */
    public function count_user_order($user_id)
    {
        global $db, $ecs;
        if(empty($user_id)) return 0;
        $cancel_status = [OS_CANCELED, OS_INVALID, OS_RETURNED];
        $sql = "SELECT COUNT(*) FROM".$ecs->table('order_info')." WHERE order_status NOT ".db_create_in($cancel_status)." AND user_id = $user_id";
        return $db->getOne($sql);
    }

    public function auto_order_separate($order_id)
    {
        global $db, $ecs, $_LANG;
        if(empty($order_id)) return true;
        $userController = new UserController();
        include_once(ROOT_PATH . 'includes/lib_order.php');
        include_once(ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/admin/affiliate_ck.php');
        $affiliate = unserialize($GLOBALS['_CFG']['affiliate']);
        empty($affiliate) && $affiliate = array();

        $separate_by = $affiliate['config']['separate_by'];
        $order = $db->getRow("SELECT o.order_sn, o.is_separate, o.goods_amount, o.user_id FROM " . $GLOBALS['ecs']->table('order_info') . " o".
                    " LEFT JOIN " . $GLOBALS['ecs']->table('users') . " u ON o.user_id = u.user_id".
            " WHERE order_id = '$order_id'");

        $order_sn = $order['order_sn'];
        // Normal affiliate
        if(isset($affiliate['on']) && $affiliate['on'] == 1 && $affiliate['config']['auto_ck'] == 1)
        {
            $user_id = $order['user_id'];
            $total_amount = $order['goods_amount'];
            $can_affiliate = $this->check_order_can_separate($user_id, $total_amount, $order_id);
            
            // We can cal this order affiliate discount:
            if($can_affiliate && empty($order['is_separate'])) { //is_separate = 0: 未分成
                $level_point_all = $affiliate['config']['level_point_all'];
                if (strpos($level_point_all,'%') === false) {
                    $point = $level_point_all;
                } else {
                    $level_point_all /= 100;
                    $integral = integral_to_give(array('order_id' => $order_id, 'extension_code' => ''));
                    $point = round(floatval($level_point_all) * intval($integral['rank_points']), 0);
                }

                $level_money_all = $affiliate['config']['level_money_all'];
                if (strpos($level_money_all,'%') === false) {
                    $money = $level_money_all;
                } else {
                    $level_money_all /= 100;
                    $money = round(floatval($level_money_all) * $order['goods_amount'],2);
                }

                if(empty($separate_by))
                {
                    //推荐注册分成
                    $num = count($affiliate['item']);
                    for ($i=0; $i < $num; $i++)
                    {
                        $affiliate['item'][$i]['level_point'] = (float)$affiliate['item'][$i]['level_point'];
                        $affiliate['item'][$i]['level_money'] = (float)$affiliate['item'][$i]['level_money'];
                        if ($affiliate['item'][$i]['level_point'])
                        {
                            $affiliate['item'][$i]['level_point'] /= 100;
                        }
                        if ($affiliate['item'][$i]['level_money'])
                        {
                            $affiliate['item'][$i]['level_money'] /= 100;
                        }
                        $setmoney = round($money * $affiliate['item'][$i]['level_money'], 2);
                        $setpoint = round($point * $affiliate['item'][$i]['level_point'], 0);
                        $order = $db->getRow("SELECT o.parent_id as user_id,u.user_name FROM " . $GLOBALS['ecs']->table('users') . " o" .
                                " LEFT JOIN" . $GLOBALS['ecs']->table('users') . " u ON o.parent_id = u.user_id".
                                " WHERE o.user_id = '$order[user_id]'"
                            );
                        $up_uid = $order['user_id'];
                        if (empty($up_uid) || empty($order['user_name']))
                        {
                            break;
                        }
                        else
                        {
                            $info = sprintf($_LANG['separate_info'], $order_sn, $setmoney, $setpoint);
                            log_account_change($up_uid, $setmoney, 0, 0, $setpoint, $info);
                            $userController->write_affiliate_log($order_id, $up_uid, $order['user_name'], $setmoney, $setpoint, $separate_by);
                        }
                    }
                }
                else
                {
                    //推荐订单分成
                    $order = $db->getRow("SELECT o.parent_id, u.user_name FROM " . $GLOBALS['ecs']->table('order_info') . " o" .
                            " LEFT JOIN" . $GLOBALS['ecs']->table('users') . " u ON o.parent_id = u.user_id".
                            " WHERE o.order_id = '$order_id'"
                        );
                    $up_uid = $order['parent_id'];
                    if(!empty($up_uid) && $up_uid > 0)
                    {
                        $info = sprintf($_LANG['separate_info'], $order_sn, $point);
                        log_account_change($up_uid, $money, 0, 0, $point, $info);
                        $userController->write_affiliate_log($order_id, $up_uid, $order['user_name'], $money, $point, $separate_by);
                    }

                }
                $sql = "UPDATE " . $GLOBALS['ecs']->table('order_info') .
                    " SET is_separate = 1" .
                    " WHERE order_id = '$order_id'";
                $db->query($sql);
                $sql = "SELECT order_id, order_status, shipping_status, pay_status FROM ". $GLOBALS['ecs']->table('order_info') .
                    " WHERE order_id = '$order_id'";
                $order_log_info = $db->getRow($sql);
                order_action($order_sn, $order_log_info['order_status'], $order_log_info['shipping_status'], $order_log_info['pay_status'], '系統自動批核推薦回贈', 'admin');
            }
        }

    }

    public function check_order_can_separate($user_id, $goods_price, $order_id = null)
    {
        global $_CFG, $db, $ecs;
        $affiliate = unserialize($_CFG['affiliate']);
        $can_affiliate = true;
        if(isset($affiliate['on']) && $affiliate['on'] == 1)
        {
            // Check cart_goods amount and order is meets the affiliate
            if(!empty($affiliate['config']['level_order_num'])) {
                if($order_id) { // Have order_id, check order is first?
                    $cancel_status = [OS_CANCELED, OS_INVALID, OS_RETURNED];
                    $sql = "SELECT order_id FROM".$ecs->table('order_info')." WHERE order_status NOT ".db_create_in($cancel_status)." AND user_id = $user_id ORDER BY order_id ASC LIMIT 1 ";
                    $first_order_id = $GLOBALS['db']->getOne($sql);
                    $can_affiliate = ($first_order_id == $order_id) ? $can_affiliate : false;
                } else { // Check user order count.
                    $count = $this->count_user_order($user_id);
                    if($count > 0) $can_affiliate = false;
                }
            }
            if(!empty($level_order_amount = $affiliate['config']['level_order_amount'])) {
                if($level_order_amount > $goods_price) $can_affiliate = false;
            }

            if($_CFG['send_verify_sms'] == 1) {
                $sql = "SELECT is_sms_validated FROM ".$GLOBALS['ecs']->table('users').' WHERE user_id = '.$user_id.' LIMIT 1';
                $is_sms_validated = $GLOBALS['db']->getOne($sql);
                if($is_sms_validated == 0) $can_affiliate = false;
            }

            /* Hardcode check parent_id is fake user */
            $parent_sql = "SELECT parent_id FROM ".$GLOBALS['ecs']->table('users').' WHERE user_id = '.$user_id.' LIMIT 1';
            $parent_id = $GLOBALS['db']->getOne($parent_sql);
            if(empty($parent_id)) $can_affiliate = false;
            $have_parent_id = $GLOBALS['db']->getOne("SELECT 1 FROM ".$GLOBALS['ecs']->table('users').' WHERE user_id = '.intval($parent_id).' LIMIT 1');
            if(!$have_parent_id) {
                $can_affiliate = false;
            }
            $disable_list = ['0','1'];
            //TODO: Is this flow, we not using affiliateyoho for normal user, so if user is_affiliate = 1, is a active_rewards, is a fake user (social media like facebook, google, VIUTV)
            $sql = "SELECT u.user_id " .
                "FROM " . $ecs->table('users') . " as u " .
                "INNER JOIN " . $GLOBALS['ecs']->table('affiliate_rewards') . " as ar ON ar.user_id = u.user_id " ."
                AND (" . gmtime() . " BETWEEN ar.start_time AND ar.end_time) " .
                "WHERE u.`is_affiliate` = 1 " .
                "GROUP BY u.`user_id` ";
            $user_list = $db->getCol($sql);
            $user_list = array_merge($user_list, $disable_list);
            if(in_array($parent_id, $user_list)) {
                $can_affiliate = false;
            }

        }
        else //分成功能关闭
        {
            $can_affiliate = false;
        }

        return $can_affiliate;
    }

    // Moved from order.php
    /**
     * 退回余额、积分、红包（取消、无效、退货时），把订单使用余额、积分、红包设为0
     * @param   array   $order  订单信息
     */
    function returnUserSurplusIntegralBonus($order)
    {
        /* 处理余额、积分、红包 */
        if ($order['user_id'] > 0 && $order['surplus'] > 0)
        {
            $surplus = $order['money_paid'] < 0 ? $order['surplus'] + $order['money_paid'] : $order['surplus'];

            if(!log_account_change($order['user_id'], $surplus, 0, 0, 0, sprintf($GLOBALS['_LANG']['return_order_surplus'], $order['order_sn'])))
            {
                return false;
            }

            if(!$GLOBALS['db']->query("UPDATE ". $GLOBALS['ecs']->table('order_info') . " SET `order_amount` = '0' WHERE `order_id` =". $order['order_id']))
            {
                return false;
            }
        }

        if ($order['user_id'] > 0 && $order['integral'] > 0)
        {
            if(!log_account_change($order['user_id'], 0, 0, 0, $order['integral'], sprintf($GLOBALS['_LANG']['return_order_integral'], $order['order_sn']), ACT_OTHER, 0, ['type' => 'sales', 'sales_order_id' => $order['order_id']]))
            {
                return false;
            }
        }

        if ($order['bonus_id'] > 0)
        {
            if(!unuse_bonus($order['bonus_id']))
            {
                return false;
            }
        }

        /* 修改订单 */
        $arr = array(
            'bonus_id'  => 0,
            'bonus'     => 0,
            'integral'  => 0,
            'integral_money'    => 0,
            'surplus'   => 0
        );
        if(!update_order($order['order_id'], $arr))
        {
            return false;
        }
        return true;
    }

    function update_order_picker_packer($order_sn,$picker_user_ids,$packer_user_ids,$date)
    {
        global $_CFG, $db, $ecs;
        // check existed
        if (!empty($order_sn)) {
            $sql = "select count(order_sn) as count from ".$ecs->table('order_picker_packer')." where order_sn = '".$order_sn."' and `date` = '".$date."' ";
            $result = $db->getrow($sql);
            if ($result['count'] == 0) {
                $sql = "insert into ".$ecs->table('order_picker_packer')." (`order_sn`,	`picker_user_ids`,	`packer_user_ids`, `date`, `update_time` ) VALUES ('".$order_sn."','".$picker_user_ids."','".$packer_user_ids."','".$date."','".gmtime()."') ";
                $db->query($sql); 
            }else{
                $sql = "update ".$ecs->table('order_picker_packer')." set `picker_user_ids` = '".$picker_user_ids."',	`packer_user_ids` = '".$packer_user_ids."', `update_time` = '".gmtime()."' where `date` = '".$date."' ";
                $db->query($sql); 
            }
        }
    }

    function export_picker_packer_template()
    {
        // init data
        $today_date = local_date('Y') . local_date('m') . local_date('d');
        $file_name = 'order_pick_and_pack_import_'.$today_date;
        header("Content-type: application/vnd.ms-excel; charset=utf-8");
        header("Content-Disposition: attachment; filename=$file_name.xls");
        echo $file = file_get_contents('../data/order_pick_and_pack_import_template.xls', FILE_USE_INCLUDE_PATH);
        exit;
    }

    function import_picker_packer_excel()
    {
        global $_CFG, $db, $ecs;

        if (empty($_FILES)) {
                return	['error' => '請選擇檔案!'];	
        }

        // init data
        ini_set('memory_limit', '256M');
        define( 'PCLZIP_TEMPORARY_DIR', ROOT_PATH . 'temp/');

        $excelHeader = ['Order SN','Picker User ID','Packer User ID','Ship Date (xxxx-xx-xx)'];

        // Get excel data
        require_once ROOT_PATH . 'includes/excel/PHPExcel.php';
        require_once ROOT_PATH . 'includes/excel/PHPExcel/IOFactory.php';
        require_once ROOT_PATH . 'includes/excel/PHPExcel/Shared/Date.php';

        \PHPExcel_Settings::setZipClass(\PHPExcel_Settings::PCLZIP);
        $fileName     = $_FILES["import_excel"]["tmp_name"];

        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $file_format = finfo_file($finfo, $fileName);
    
        $valided_type = ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                        'application/vnd.ms-excel',
                        'application/vnd.ms-office'
                        ];

        if (!in_array($file_format,$valided_type)) {
                return	['error' => '檔案格式錯誤!請使用.xlsx .xls'];
        }

        $fileType     = \PHPExcel_IOFactory::identify($fileName);
        $objReader    = \PHPExcel_IOFactory::createReader($fileType);
        $objPHPExcel  = $objReader->load($fileName);
        $objWorksheet = $objPHPExcel->getSheet(0);
    
        $rows = [];
        $header = [];
        $errors = [];
        $all_order_sns = [];
        $all_picker_ids = [];
        $all_packer_ids = [];
        $update_data = [];
        foreach ($objWorksheet->getRowIterator() AS  $row) {
            $rowIndex = $row->getRowIndex();
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
            $cells = [];
            foreach ($cellIterator as $key => $cell) {
                if ($rowIndex == 1){
                    $header[] = trim($cell->getValue());
                } else {
                    if ($key == 3){
                        $InvDate = $cell->getValue();		
                        if (is_numeric($InvDate)) {
                                $InvDate = date('Y-m-d', \PHPExcel_Shared_Date::ExcelToPHP($InvDate)); 
                        }
                        $cells[] = trim($InvDate);		
                    }else{
                        $cells[] = trim($cell->getValue());										
                    }
                    
                    // save all order sns
                    if ($key == 0){
                        if (!empty(trim($cell->getValue()))) {
                            $all_order_sns[] = trim($cell->getValue());
                        }
                    }
                    // save all picker user ids
                    if ($key == 1){
                        if (!empty(trim($cell->getValue()))) {
                            $pickers_ar_temp = explode(',',trim($cell->getValue()));
                            $all_picker_ids = array_merge($all_picker_ids,$pickers_ar_temp);
                        }
                    }
                    // save all packer user ids
                    if ($key == 2){
                        if (!empty(trim($cell->getValue()))) {
                            $packers_ar_temp = explode(',',trim($cell->getValue()));
                            $all_packer_ids = array_merge($all_packer_ids,$packers_ar_temp);
                        }
                    }
                }
            }

            // check the first row header 
            if ($rowIndex == 1){
                foreach ($excelHeader as $value) {
                    if (!in_array($value,$header)) {	
                        $errors[] = 'Line '.$rowIndex.'- '.'Excel格式錯誤, 請確定有以下欄位: '.$value;
                    }
                }

                if (!empty($errors)) {
                    return	['error' => implode('<br>',$errors)];
                }
            }else{
                $rows[] = $cells;
            }
        }

        if (!empty($all_order_sns)) {
            $sql = "SELECT order_sn FROM ".$GLOBALS['ecs']->table('order_info').' WHERE order_sn in ("'.implode('","',$all_order_sns).'")';
            $existed_order_sns = $GLOBALS['db']->getCol($sql);
        }

        if (!empty($all_picker_ids)) {
            $sql = "SELECT user_id FROM ".$GLOBALS['ecs']->table('admin_user').' WHERE user_id in ("'.implode('","',$all_picker_ids).'")';
            $existed_picker_ids = $GLOBALS['db']->getCol($sql);
        }

        if (!empty($all_packer_ids)) {
            $sql = "SELECT user_id FROM ".$GLOBALS['ecs']->table('admin_user').' WHERE user_id in ("'.implode('","',$all_packer_ids).'")';
            $existed_packer_ids = $GLOBALS['db']->getCol($sql);
        }
        
        foreach ($rows as $index => $row) {
            // check null , not found order id, user id
            $order_sn = $row[0];
            $picker_user_ids = $row[1];
            $packer_user_ids = $row[2];
            $ship_date = $row[3];
            $rowIndex = $index + 2;

            // skip when the row is empty
            if (empty($order_sn) && empty($picker_user_ids) && empty($packer_user_ids)) {
                continue;
            }

            // check order SN
            if (empty($order_sn)) {
                $errors[] = 'Line '.$rowIndex.' - '.'['.$excelHeader[0].'] : 不能空白';
            } else if(!in_array($order_sn,$existed_order_sns)) {
                $errors[] = 'Line '.$rowIndex.' - '.'['.$excelHeader[0].'] : 找不到訂單號('.$order_sn.')';
            }

            // check picker user ids
            if (empty($picker_user_ids)) {
                $errors[] = 'Line '.$rowIndex.' - '.'['.$excelHeader[1].'] : 不能空白';
            } else {
                $picker_user_ids_ar = explode(',',$picker_user_ids);
                $picker_id_errors = [];
                foreach ($picker_user_ids_ar as $picker) {
                    if(!in_array($picker,$existed_picker_ids)) {
                        $picker_id_errors[] = $picker;
                    }
                }

                if (!empty($picker_id_errors)) {
                    $errors[] = 'Line '.$rowIndex.' - '.'['.$excelHeader[1].'] : 找不到員工編號('.implode(',',$picker_id_errors).')';
                }
            }

            // check packer user ids
            if (empty($packer_user_ids)) {
                $errors[] = 'Line '.$rowIndex.' - '.'['.$excelHeader[2].'] : 不能空白';
            } else {
                $packer_user_ids_ar = explode(',',$packer_user_ids);
                $packer_id_errors = [];
                foreach ($packer_user_ids_ar as $packer) {
                    if(!in_array($packer,$existed_packer_ids)) {
                        $packer_id_errors[] = $packer;
                    }
                }

                if (!empty($packer_id_errors)) {
                    $errors[] = 'Line '.$rowIndex.' - '.'['.$excelHeader[2].'] : 找不到員工編號('.implode(',',$packer_id_errors).')';
                }
            }

            // check date
            if (empty($ship_date)) {
                $errors[] = 'Line '.$rowIndex.' - '.'['.$excelHeader[3].'] : 不能空白';
            } else {
                if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$ship_date)) {
                    $errors[] = 'Line '.$rowIndex.' - '.'['.$excelHeader[3].'] : 日期格式錯誤,例子(2018-01-30)';
                } 
            }

            // updateData 
            $update_data[] = $row;
        }

        if (!empty($errors)) {
            return	['error' => implode('<br>',$errors)];
        } else {
            //update to order record;
            foreach ($update_data as $index => $data) {
                $this->update_order_picker_packer($data[0],$data[1],$data[2],$data[3]);
            }
        }

        return ['success' => '匯入成功,共'.count($update_data).'個記錄'];
    }

    function get_order_picker_packer_list($is_pagination = true)
    {
        global $db, $ecs, $userController ,$_CFG, $_LANG;

        $adminuserController = new AdminuserController();
        $today_date = local_date('Y-m-d');
        
        $_REQUEST['sort_by']          = empty($_REQUEST['sort_by']) ? 'shipping_time' : trim($_REQUEST['sort_by']);
        $_REQUEST['sort_order']       = empty($_REQUEST['sort_order']) ? 'ASC' : trim($_REQUEST['sort_order']);

        /* 分页大小 */
        if (!isset($_REQUEST['start_date'])) {
            $start_time = local_strtotime($today_date);
            $start_date = $today_date;
        } else {
            $start_time = local_strtotime($_REQUEST['start_date']);
            $start_date = $_REQUEST['start_date'];
        }

        if (!isset($_REQUEST['end_date'])) {
            $end_time = local_strtotime($today_date);
            $end_date = $today_date;
        }else{
            $end_time= local_strtotime($_REQUEST['end_date']);
            $end_date = $_REQUEST['end_date'];
        }

        // total record
        $sql = '';
        $sql .= "SELECT count(*) ";
        $sql .= " FROM " .$ecs->table('order_info') ." oi ";
        $sql .= " WHERE shipping_time BETWEEN " . ($start_time) . " AND " . ($end_time + 86399);
        $sql .= " And `shipping_id` != 2 order by shipping_time asc "  ;

        $total_count = $db->getOne($sql);

        $record_count = $total_count;

        /* get sub total 已付款,已出貸 */
        // 銷售金額
        $sql  = "SELECT oi.order_id,oi.order_sn, oi.order_status, oi.shipping_status, oi.pay_status, oi.shipping_time ,opp.picker_user_ids, opp.packer_user_ids, opp.date ";
        $sql .= " FROM " .$ecs->table('order_info') ." oi left join " . $ecs->table('order_picker_packer') . " opp on (oi.order_sn = opp.order_sn) ";
        $sql .= " WHERE shipping_time BETWEEN " . ($start_time) . " AND " . ($end_time + 86399);
        $sql .= " And `shipping_id` != 2 "  ;
        $sql .= " ORDER BY ".$_REQUEST['sort_by']." ".$_REQUEST['sort_order'];
        $sql .= ($is_pagination ? " LIMIT " . $_REQUEST['start'] . ", " . $_REQUEST['page_size'] : '');

        $res = $db->GetAll($sql);

        $order_data = [];
        foreach ($res as $key => $row) {
            $str = '';
            $order_data[$key]['order_sn'] = $row['order_sn'];
            $order_data[$key]['order_id'] = $row['order_id'];
            $order_data[$key]['order_status'] = $row['order_status'];
            $order_data[$key]['pay_status'] = $row['pay_status'];
            $order_data[$key]['shipping_status'] = $row['shipping_status'];
            $order_data[$key]['shipping_time'] = local_date($_CFG['time_format'], $row['shipping_time']);
            $order_data[$key]['all_status'] = $_LANG['icon']['ps'][$row['pay_status']]." | ".$_LANG['icon']['ss'][$row['shipping_status']];
            if (!empty($row['packer_user_ids'])) {
                $picker_name_ar = [];
                $picker_ids = explode(',',$row['picker_user_ids']);
                foreach ($picker_ids as $picker_id){
                    $temp = $adminuserController->get_user($picker_id);
                    if (!empty($temp))
                    $picker_name_ar[] = $temp['user_name'];
                }

                $packer_name_ar = [];
                $packer_ids = explode(',',$row['packer_user_ids']);
                foreach ($packer_ids as $packer_id){
                    $temp = $adminuserController->get_user($packer_id);
                    if (!empty($temp))
                    $packer_name_ar[] = $temp['user_name'];
                }

                $str = implode(',',$picker_name_ar) . ' | ' . implode(',',$packer_name_ar) . ' ('.$row['date'].')';

                if (!empty($order_data[$key]['picker_packer'])) {
                    $str = '<br>'.$str;
                }
            }

            $order_data[$key]['picker_packer'] .= $str;
        }


        $arr = array(
            'order_data' => $order_data,
            'record_count' => $record_count
        );
        return $arr;
    }

    function ajaxQueryOrderPickerAction()
    {
        $list = $this->get_order_picker_packer_list();
        parent::ajaxQueryAction($list['order_data'], $list['record_count'], false);
    }

    function get_today_order_stats($filter_mode = 'paid')
    {
        global $db,$ecs;

        $userController = new UserController();
        $today_order = array();
        $today_order['today'] = local_date('Y-m-d');

        // Exclude orders for the followiung user accounts: 11111111, 88888888, 99999999
        $where_exclude = " AND (order_type<>'".self::DB_ORDER_TYPE_WHOLESALE."' OR (order_type IS NULL AND user_id NOT IN (".implode(',',$userController->getWholesaleUserIds())."))) ";
        // Exclude sales agent
        $where_exclude .= " AND (order_type <> '" . self::DB_ORDER_TYPE_SALESAGENT . "' OR order_type IS NULL) ";
        // Exclude 展場POS
        $where_exclude .= " AND (platform <> 'pos_exhibition') ";

        $start_time = local_mktime(0, 0, 0, local_date('m'), local_date('d'), local_date('Y'));
        $end_time = local_mktime(23, 59, 59, local_date('m'), local_date('d'), local_date('Y'));

        if ($filter_mode == 'shipped')
        {
            $where_status = " AND order_status NOT IN (" . OS_CANCELED . ',' . OS_INVALID . ',' . OS_RETURNED . ") ".
                            " AND shipping_status IN (" . SS_SHIPPED . ',' . SS_RECEIVED . ") ";
            $where_today = " AND shipping_time BETWEEN " . $start_time . " AND " . $end_time;

            $today_order['order_list_url'] = 'order.php?act=list&ship_start_time=' . $start_time . '&ship_end_time=' . $end_time . '&composite_status=201&display_normal_order=1';
        }
        else //if ($filter_mode == 'paid')
        {
            $where_status = " AND order_status NOT IN (" . OS_CANCELED . ',' . OS_INVALID . ',' . OS_RETURNED . ") ".
                            " AND pay_status = " . PS_PAYED . " ";
            $where_today = " AND pay_time BETWEEN " . $start_time . " AND " . $end_time;

            $today_order['order_list_url'] = 'order.php?act=list&pay_start_time=' . $start_time . '&pay_end_time=' . $end_time . '&pay_status=2&display_normal_order=1';
        }

        $sql = "SELECT COUNT(*) as order_count, ".
        " SUM(`money_paid` + `order_amount`) as total_amount ". // howang: money_paid(已付金額) + order_amount(應付金額) = 訂單總金額
        " FROM " .$ecs->table('order_info').
        " WHERE 1 " . $where_exclude . $where_status . $where_today;
        $row = $db->GetRow($sql);
        $today_order['order_count'] = $row['order_count'];
        $today_order['total_amount'] = price_format($row['total_amount'], false);

        $today_order['amount_by_payment'] = array(
            'cash' => 0,
            'eps' => 0,
            'bank' => 0,
            'paypal' => 0,
            'alipay' => 0,
            'unionpay' => 0,
            'franzpay' => 0,
            'visamaster' => 0,
            'americanexpress' => 0,
            'stripe' => 0,
            'braintree' => 0,
            'qfpay' => 0,
        );
        $today_order['payment_label'] = array(
            'cash' => '現金',
            'eps' => 'EPS',
            'bank' => 'ATM/網上銀行轉帳',
            'paypal' => 'PayPal',
            'alipay' => '支付寶',
            'unionpay' => '銀聯卡',
            'franzpay' => '自行支付',
            'visamaster' => 'Visa/MasterCard',
            'americanexpress' => '美國運通卡',
            'stripe' => '信用卡(Stripe)',
            'braintree' => '信用卡(Braintree)',
            'qfpay' => '微信支付 / 支付寶',
        );
        $sql = "SELECT pay_id, SUM(`money_paid` + `order_amount`) as total_amount ".
        "FROM " . $ecs->table('order_info') .
        " WHERE 1 " . $where_exclude . $where_status . $where_today .
        " GROUP BY pay_id";
        $res = $db->GetAll($sql);
        foreach ($res as $row)
        {
            if ($row['pay_id'] == 3)
            {
                $today_order['amount_by_payment']['cash'] = $row['total_amount'];
            }
            elseif ($row['pay_id'] == 2)
            {
                $today_order['amount_by_payment']['eps'] = $row['total_amount'];
            }
            elseif (($row['pay_id'] == 5) || ($row['pay_id'] == 9))
            {
                $today_order['amount_by_payment']['bank'] += $row['total_amount'];
            }
            elseif ($row['pay_id'] == 17)
            {
                $today_order['amount_by_payment']['paypal'] = $row['total_amount'];
            }
            elseif ($row['pay_id'] == 10)
            {
                $today_order['amount_by_payment']['unionpay'] = $row['total_amount'];
            }
            elseif ($row['pay_id'] == 11)
            {
                $today_order['amount_by_payment']['franzpay'] = $row['total_amount'];
            }
            elseif ($row['pay_id'] == 12)
            {
                $today_order['amount_by_payment']['visamaster'] = $row['total_amount'];
            }
            elseif ($row['pay_id'] == 13)
            {
                $today_order['amount_by_payment']['americanexpress'] = $row['total_amount'];
            }
            elseif ($row['pay_id'] == 14)
            {
                $today_order['amount_by_payment']['alipay'] = $row['total_amount'];
            }
            elseif ($row['pay_id'] == 15)
            {
                $today_order['amount_by_payment']['stripe'] = $row['total_amount'];
            }
            elseif ($row['pay_id'] == 16)
            {
                $today_order['amount_by_payment']['braintree'] = $row['total_amount'];
            }
            elseif ($row['pay_id'] == 22)
            {
                $today_order['amount_by_payment']['qfpay'] = $row['total_amount'];
            }
        }
        // $today_order['amount_by_payment']['cash'] = price_format($today_order['amount_by_payment']['cash'], false);
        // $today_order['amount_by_payment']['eps'] = price_format($today_order['amount_by_payment']['eps'], false);
        // $today_order['amount_by_payment']['bank'] = price_format($today_order['amount_by_payment']['bank'], false);
        // $today_order['amount_by_payment']['paypal'] = price_format($today_order['amount_by_payment']['paypal'], false);
        // $today_order['amount_by_payment']['alipay'] = price_format($today_order['amount_by_payment']['alipay'], false);
        // $today_order['amount_by_payment']['unionpay'] = price_format($today_order['amount_by_payment']['unionpay'], false);
        // $today_order['amount_by_payment']['franzpay'] = price_format($today_order['amount_by_payment']['franzpay'], false);
        // $today_order['amount_by_payment']['visamaster'] = price_format($today_order['amount_by_payment']['visamaster'], false);
        // $today_order['amount_by_payment']['americanexpress'] = price_format($today_order['amount_by_payment']['americanexpress'], false);
        // $today_order['amount_by_payment']['stripe'] = price_format($today_order['amount_by_payment']['stripe'], false);
        // $today_order['amount_by_payment']['braintree'] = price_format($today_order['amount_by_payment']['braintree'], false);

        return $today_order;
    }

    function getOrderGoodsSnInfo()
    {
        global $db, $ecs, $userController ,$_CFG, $_LANG;
       
        $goods_sn_data = [];
        $record_count = 0;

        if (!empty($_REQUEST['order_sn'])) {
            $sql  = "SELECT oi.order_id,oi.order_sn, oi.serial_numbers";
            $sql .= " FROM " .$ecs->table('order_info') ." oi";
            $sql .= " WHERE order_sn = ".$_REQUEST['order_sn'];
            $res = $db->getRow($sql);
            $goods_sns = $res['serial_numbers'];
            
            if (!empty($goods_sns)) {
                $goods_sns_ar = explode(PHP_EOL,$goods_sns);
                //$goods_sns = $res['serial_numbers'];
                foreach ($goods_sns_ar as $key => $goods_sn) {
                    $goods_sn = preg_replace( "/\r|\n/", "", $goods_sn );
                    $goods_sn_data[] = array('id' => $key,
                                            'goods_sn' => '<span class="order_goods_sn" data-order-goods-sn="'.str_replace('\r','',$goods_sn).'" >'.$goods_sn.'</span>',
                                            'actions' => '<a class="collapse-link delete_confirm_action" href="javascript:void(0)"><i class="fa fa-trash red" style="font-size: 20px;"></i></a>'
                                        );
                }
            }
        }

        $arr = array(
            'data' => $goods_sn_data,
            'record_count' => $record_count
        );
        return $arr;
    }

    function saveOrderGoodsSerialNumbers()
    {
        global $db, $ecs, $userController ,$_CFG, $_LANG;
        
        $serial_numbers = '';
        if (!empty($_REQUEST['order_sn'])) {
            if (!empty($_REQUEST['serial_numbers'])) {
               $serial_numbers = implode("\n",$_REQUEST['serial_numbers']);
            }
            
            $sql = "update ".$ecs->table('order_info')." set `serial_numbers` = '".$serial_numbers."' where `order_sn` = '".$_REQUEST['order_sn']."' ";
            $db->query($sql);
        }

        return true;
    }

    function addOrderGoodsSerialNumbers()
    {
        global $db, $ecs, $userController ,$_CFG, $_LANG;

        $serial_numbers = '';
        if (!empty($_REQUEST['order_sn'])) {
            if (!empty($_REQUEST['new_serial_number'])) {
                $sql  = "SELECT oi.order_id,oi.order_sn,oi.order_status,oi.shipping_status,oi.pay_status,oi.serial_numbers";
                $sql .= " FROM " .$ecs->table('order_info') ." oi";
                $sql .= " WHERE order_sn = '".$_REQUEST['order_sn']."'";
                $res = $db->getRow($sql);

                if (empty($res['order_id'])) {
                    $result['error'] = '找不到訂單號['.$_REQUEST['order_sn'].']';
                    return $result;
                }

                $new_serial_numbers = $res['serial_numbers'].(empty($res['serial_numbers']) ? '' : "\n" ).$_REQUEST['new_serial_number'];
                $sql = "update ".$ecs->table('order_info')." set `serial_numbers` = '".$new_serial_numbers."' where `order_sn` = '".$_REQUEST['order_sn']."' ";
                $db->query($sql);

				// save log
				$log = '(產品序號)';
				$action = '備註更新';

			    $admin_id = $_SESSION['admin_id'];
			    $sql = "SELECT user_name FROM ".$GLOBALS['ecs']->table('admin_user')." WHERE user_id = ".$admin_id." LIMIT 1";
			    $admin_name = $GLOBALS['db']->getOne($sql);

				order_action($res['order_sn'], $res['order_status'], $res['shipping_status'], $res['pay_status'],
			     '[系統備註] ' .$action.  $log , $admin_name);
            }
        }
        return true;
    }

    /**
     * @param string $code
     * @param int $step
     * @param array $data
     * @return bool|array
     */
    public function getStepByShippingType($code = '', $step = 'shipping_type', $data = [], $platform)
    {
        // If undefind platform, return
        if(!in_array($platform, [self::ORDER_FEE_PLATFORM_WEB, self::ORDER_FEE_PLATFORM_POS, self::ORDER_FEE_PLATFORM_CMS]))
        return false;

        global $db, $ecs;
        $addressController = new AddressController();
        $userController = new UserController();
        $shippingController = new ShippingController();
        $paymentController = new PaymentController();
        if($code == 'express') {
            /* 1. Get user address */
            /* 2. Base on user address to display shipping modules */
            /* 3. Base on shipping modules to display payment modules */
            switch($step) {
                case 'shipping_type':
                    $sql = "SELECT address_id ".
                        "FROM " . $GLOBALS['ecs']->table('users') .
                        "WHERE user_id = '" . $_SESSION['user_id'] . "'";
                    $selected_address = $GLOBALS['db']->getOne($sql);
                    $address = $userController->get_user_address_list($_SESSION['user_id']);
                    return ['step' => $step, 'stepName' => 'address', 'list' => $address, 'default' => $selected_address];
                    break;
                case 'address':
                    $res = $userController->set_user_address_list($_SESSION['user_id'], $data['id']);
                    if($res) {
                       $consignee = get_consignee($_SESSION['user_id']);
                       $shipping = $shippingController->getFlowShippingList($code, $consignee, $platform);
                        return ['step' => $step, 'stepName' => 'shipping', 'data' => $shipping];
                    }
                    break;
                case 'shipping':
                    $res = $paymentController->getFlowPaymentList($_SESSION['user_id'], $data);
                    $res = $this->filterFlashDealPaymentMethod($res);
                    if(!is_null($res)) {
                        return ['step' => $step, 'stepName' => 'payment', 'data' => $res];
                    }
                    break;
            }
        } elseif($code == 'pickuppoint') {
            /* 1. Get this type shipping modules */
            /* 2. Base on shipping modules to display address list (module->pickuplist) */
            /* 3. Base on shipping modules to display payment modules */
            switch($step) {
                case 'shipping_type':
                    $shipping = $shippingController->getFlowShippingList($code, [], $platform);
                    return ['step' => $step, 'stepName' => 'shipping', 'data' => $shipping];
                    break;
                case 'address':
                    $res = $paymentController->getFlowPaymentList($_SESSION['user_id'], $data);
                    $res = $this->filterFlashDealPaymentMethod($res);
                    if(!is_null($res)) {
                        return ['step' => $step, 'stepName' => 'payment', 'data' => $res];
                    }
                    break;
                case 'shipping':
                    $res = $shippingController->getShippingAreaAddressList($data);
                    return ['step' => $step, 'stepName' => 'address', 'list' => $res['group_list'], 'default' => $res['locker']];
                    break;
            }
        } elseif ($code == 'store') {
            /* 1. Get this type shipping modules */
            /* 2. Base on shipping modules to display address list (module->pickuplist) */
            /* 3. Base on shipping modules to display payment modules */
            switch($step) {
                case 'shipping_type':

                    $consignee = ['country'=> 3409];
                    $shipping = $shippingController->getFlowShippingList($code, $consignee, $platform);
                    return ['step' => $step, 'stepName' => 'shipping', 'data' => $shipping];

                    break;
                case 'address':
                    /*
                     * */
                    break;
                case 'shipping':
                    $res = $paymentController->getFlowPaymentList($_SESSION['user_id'], $data);
                    $res = $this->filterFlashDealPaymentMethod($res);

                    if(!is_null($res)) {
                        return ['step' => $step, 'stepName' => 'payment', 'data' => $res];
                    }
                    break;
            }
        } else {
            return false;
        }
    }

    public function markFraudUserOrder($order_id) {
        global $db, $ecs, $_LANG;
        $userController = new UserController();

        $order = order_info($order_id);
        $user_id = $db->getOne("SELECT user_id FROM " . $ecs->table("order_info") . " WHERE order_id = $order_id");
        if ($userController->isFraudUser($user_id)) {
            $arr = array('is_hold' => 2, 'to_buyer' => $_LANG['hold_order_message']['user']);
            update_order($order_id, $arr);
            order_action($order['order_sn'], $order['order_status'], $order['shipping_status'], $order['pay_status'], "[系統備註] 本用戶已標籤為可疑會員", "admin");
        }
    }

	function orderAssignWarehouseId($order_id, $is_pos = false)
 	{
		global $db, $ecs, $_CFG, $_LANG;

		$erpController = new \Yoho\cms\Controller\ErpController(); 
		
		require_once(ROOT_PATH .'x/includes/ERP/lib_erp_common.php');
		require_once(ROOT_PATH .'x/includes/ERP/lib_erp_goods_attr.php');
		require_once(ROOT_PATH .'x/includes/ERP/lib_erp_warehouse.php');
		require_once(ROOT_PATH .'x/includes/ERP/lib_erp_goods.php');
		require_once(ROOT_PATH .'/includes/lib_erp.php');
		
		//$db->query('START TRANSACTION');

		$shop_warehouse_id = $erpController->getShopWarehouseId();
		$main_warehouse_id = $erpController->getMainWarehouseId();
		
		// update goods_order warehouse id is null
		// clean reserved history status  = 0
		//$this->cancelReservedOrderGoodsForUnpaid($order_id);
		
 		// (goods_number - send_number) exclude shipped qty.
		$order = order_info($order_id);
		
		// check if unpaid, then break;
		// if ($order['pay_status'] == PS_UNPAYED) {
		// 	// only clear reserved data
		// 	$db->query('COMMIT');
		// 	return true;
		// }

	    $order_goods = $this->get_order_goods($order);

		if ($order['shipping_id'] != 2 && $order['shipping_id'] != 5) {  // 2 門市購買, 5 門店自取
			$orderRulesType = 'online'; // logistics order
			$warehouse_id = $main_warehouse_id;
			$shop_id = 0;
		} else {
			$orderRulesType = 'store_pickup';
			if ($order['shipping_id'] == 2) {
				$warehouse_id = $shop_warehouse_id;
			} else {
				$warehouse_id = $main_warehouse_id;
			}
			
			$shop_id = $shop_warehouse_id;

			// online order , store pickup
			// if ($warehouse_id == 0 && $order['store_pickup_id'] > 0) {
			// 	$warehouse_id = $order['store_pickup_id'];
			// }
		}

		if ($is_pos == true || $order['platform'] == 'pos') {
			$shop_id = $shop_warehouse_id;
		}
		
		// $sql = "update ".$ecs->table('order_info')." set shop_id = ".$shop_id." where order_id = ".$order_id." ";
		// $db->query($sql);

		// only update 未出貨 goods
		$sql = "update ".$ecs->table('order_goods')." set warehouse_id = ".$warehouse_id." where order_id = ".$order_id." and send_number < goods_number";
		$db->query($sql);

		//$db->query('COMMIT');

		return ture;
	}

	function getOrderWarehouseIdForReduceStock($order_id, $is_pos = false)
	{
		global $db, $ecs, $_CFG, $_LANG;

		$erpController = new \Yoho\cms\Controller\ErpController(); 
		
		require_once(ROOT_PATH .'x/includes/ERP/lib_erp_common.php');
		require_once(ROOT_PATH .'x/includes/ERP/lib_erp_goods_attr.php');
		require_once(ROOT_PATH .'x/includes/ERP/lib_erp_warehouse.php');
		require_once(ROOT_PATH .'x/includes/ERP/lib_erp_goods.php');
		require_once(ROOT_PATH .'/includes/lib_erp.php');
		
		//$db->query('START TRANSACTION');

		$shop_warehouse_id = $erpController->getShopWarehouseId();
		$main_warehouse_id = $erpController->getMainWarehouseId();
        $fls_warehouse_id = $erpController->getFlsWarehouseId();
        $wilson_warehouse_id = $erpController->getWilsonWarehouseId();
		// update goods_order warehouse id is null
		// clean reserved history status  = 0
		//$this->cancelReservedOrderGoodsForUnpaid($order_id);
		
		// (goods_number - send_number) exclude shipped qty.
		$order = order_info($order_id);
		
		// check if unpaid, then break;
		// if ($order['pay_status'] == PS_UNPAYED) {
		// 	// only clear reserved data
		// 	$db->query('COMMIT');
		// 	return true;
		// }

		//$order_goods = $this->get_order_goods($order);

		if ($order['shipping_id'] != 2 && $order['shipping_id'] != 5) {  // 2 門市購買, 5 門店自取
			$orderRulesType = 'online'; // logistics order
			$warehouse_id = $main_warehouse_id;
            $shop_id = 0;

            // check fls
            if (isset($_REQUEST['is_fls']) && $_REQUEST['is_fls'] == 1) {
                $warehouse_id = $fls_warehouse_id;
            }

            // check wilson
            if (isset($_REQUEST['logistics']) && $_REQUEST['logistics'] == 'wilson') {
                $warehouse_id = $wilson_warehouse_id;
            }

		} else {
			$orderRulesType = 'store_pickup';
			$warehouse_id = $shop_warehouse_id;
			$shop_id = $shop_warehouse_id;

			// online order , store pickup
			// if ($warehouse_id == 0 && $order['store_pickup_id'] > 0) {
			// 	$warehouse_id = $order['store_pickup_id'];
			// }
		}

		if ($is_pos == true || $order['platform'] == 'pos') {
			$shop_id = $shop_warehouse_id;
		}
		
		// $sql = "update ".$ecs->table('order_info')." set shop_id = ".$shop_id." where order_id = ".$order_id." ";
		// $db->query($sql);

		// only update 未出貨 goods
		// $sql = "update ".$ecs->table('order_goods')." set warehouse_id = ".$warehouse_id." where order_id = ".$order_id." and send_number < goods_number";
		// $db->query($sql);

		//$db->query('COMMIT');
		//return 26;
		return $warehouse_id;
	}

	public function createOrderGoodsTransferReserved($order_id,$goods_id,$transfer_qty,$warehouse_from,$warehouse_to,$type = 0)
	{
		global $db, $ecs, $_CFG, $_LANG;

		// check if it reserved/preorder already
		//$sql = "select COALESCE(sum(transfer_qty),0) from ".$ecs->table('order_goods_transfer_reserved')." where order_id = ".$order_id." and goods_id = ".$goods_id." ";
		//$transfered_qty = $db->getOne($sql);
		//$transfer_diff = $transfer_qty - $transfered_qty;
		//if ($transfer_diff > 0){
			$sql = "insert into ".$ecs->table('order_goods_transfer_reserved')." (`order_id`,`goods_id`,`transfer_qty`,`warehouse_from`,`warehouse_to`,`type`) values (".$order_id.",".$goods_id.",".$transfer_qty.",".$warehouse_from.",".$warehouse_to.",".self::ORDER_GOODS_RESERVE_TYPE_TRANSFER.") ";
			$db->query($sql);
		//}

		return $db->Insert_ID();
	}

	public function changeOrderGoodsTransferReservedStatus($transfer_id,$status)
	{
		global $db, $ecs, $_CFG, $_LANG;

		$sql = "update ".$ecs->table('order_goods_transfer_reserved')." set status = ".$status." where  transfer_id = ".$transfer_id." ";
		$db->query($sql);
		return true;
	}

	public function updateOrderGoodsWarehouseId($order_id,$warehouse_id)
	{
		global $db, $ecs, $_CFG, $_LANG;

		$sql = "update ".$ecs->table('order_goods')." set warehouse_id = ".$warehouse_id." where order_id = ".$order_id." ";
		$db->query($sql);
		return true;
	}

    public function getWholesaleCustomer()
    {
        global $db, $ecs, $_CFG, $_LANG;
        
        $sql = "SELECT * FROM " . $ecs->table("wholesale_customer") . (!empty($_REQUEST['json']) ? " ORDER BY company ASC" : "");
        $res = $db->getAll($sql);

        return $res;
    }

    public function updateOrderWholesaleCompany($order_id,$ws_company_id)
	{
		global $db, $ecs, $_CFG, $_LANG;

        // update dn wholesale id order info
        $sql = "update ". $ecs->table("order_info") . " set ws_company_id = '".$ws_company_id."' where order_id =  ".$order_id. "";
        $db->query($sql);
    
		return true;
    }
    
    public function updateOrderWholesaleCustomer($order_id,$ws_customer_id)
	{
		global $db, $ecs, $_CFG, $_LANG;

        // update dn wholesale id order info
        $sql = "update ". $ecs->table("order_info") . " set ws_customer_id = '".$ws_customer_id."' where order_id =  ".$order_id. "";
        $db->query($sql);
    
		return true;
    }
    
    public function updateAddressInfo($sn, $cat = 0, $lift = 0, $verify = 0)
	{
        global $db, $ecs, $_CFG, $_LANG;

        $sql = "SELECT oa.order_address_id, oa.address, o.user_id ".
                "FROM ".$ecs->table("order_info")." AS o ".
                "LEFT JOIN ".$ecs->table("order_address")." AS oa ON o.address_id = oa.order_address_id ".
                "WHERE o.order_sn = ".$sn." ";

        $order_addr_key = $db->getRow($sql);

        $sql = "UPDATE ".$ecs->table("user_address")." ".
        "SET dest_type = '".$cat."', lift = '".$lift."' ".
        "WHERE user_id = ".$order_addr_key['user_id']." ".
        "AND address = '".mysql_real_escape_string($order_addr_key['address'])."' ";
        
        $db->query($sql);

        $sql = "UPDATE ".$ecs->table("order_address")." ".
        //"SET oa.dest_type = '".$cat."', oa.lift = '".$lift."', oa.verify = '".$verify."' ".
        "SET dest_type = '".$cat."', lift = '".$lift."' ".
        "WHERE order_address_id = ".$order_addr_key['order_address_id']." ";
        
        $db->query($sql);
    
		return true;
    }

    function orderSetShipmentEstimatedTime($order_id, $is_pos = false)
 	{
		global $db, $ecs, $_CFG, $_LANG;
        require_once ROOT_PATH . 'includes/lib_howang.php';
        require_once ROOT_PATH . 'includes/lib_main.php';
        require_once ROOT_PATH . 'includes/lib_goods.php';

        $erpController = new \Yoho\cms\Controller\ErpController();
        $goodsController = new \Yoho\cms\Controller\GoodsController();
        $shop_warehouse_id = $erpController->getShopWarehouseId();
        $main_warehouse_id = $erpController->getMainWarehouseId();
        
        $order = order_info($order_id);
        $order_goods = $this->get_order_goods($order);

        $goods_ids = [];
        $goods_qty = [];
        foreach ($order_goods['goods_list'] as $goods) {
            $goods_ids[] = $goods['goods_id'];
            $goods_qty[$goods['goods_id']] = $goods['goods_number']- $goods['send_number'];
        }
        
        // online - in stock 8 hours
        // online - out of stock (pre order) / transfer days
        // offline - in stock 8 hours
        // offline - out of stock (pre order) + shop transfer days
        
        // check stock sellable 
        $goods_stock = $erpController->getSellableProductsQtyByWarehouses($goods_ids,null,true,$order_id);
        
        //get sellable warehouses without shop
        $sql = "SELECT warehouse_id FROM `ecs_erp_warehouse` where is_on_sale =  1 and is_valid = 1 and type != 1 ";
        $sellableWarehouseIds = $GLOBALS['db']->getCol($sql);
        $erpController = new ErpController();
        $goods_stock_exclude_shop = $erpController->getSumOfSellableProductsQtyByWarehouses($goods_ids,$sellableWarehouseIds,true,$order_id);

        // echo '<pre>';
        // print_r($goods_stock);
        // echo 'in-stock:'. ($enough_stock ? 1:0);
        //echo '<br>';
        //$working_hours_start = strtotime(date("Y-m-d 08:30:00"));

        //$working_hours_end = strtotime(date("Y-m-d 17:30:00"));
        
        //$now = strtotime(date("Y-m-d H:i:s"));
        //echo date("Y-m-d 7:30:00");
        //$now = strtotime(date("Y-m-d 7:30:00"));
        
        $out_of_data = [];
        if ($order['shipping_id'] != 2 && $order['shipping_id'] != 5) {  // 2 門市購買, 5 門店自取
			$orderRulesType = 'online'; // logistics order
            $warehouse_id = $main_warehouse_id;

            // all enough stock
            $enough_stock = true;        
            foreach ($goods_ids as $goods_id) {
                if ( $goods_qty[$goods_id] > $goods_stock_exclude_shop[$goods_id]) {
                    $enough_stock = 0;
                    $out_of_data[$goods_id] = $goods_stock_exclude_shop[$goods_id] - $goods_qty[$goods_id];
                }
            }
		} else {
			$orderRulesType = 'store_pickup';
			if ($order['shipping_id'] == 2) {
				$warehouse_id = $shop_warehouse_id;
			} else {
				$warehouse_id = $main_warehouse_id;
            }
            
            // all enough stock
            $enough_stock = true;        
            foreach ($goods_ids as $goods_id) {
                if ($goods_qty[$goods_id] > $goods_stock[$goods_id][$warehouse_id]) {
                    $enough_stock = 0;
                    $out_of_data[$goods_id] = $goods_stock[$goods_id][$warehouse_id] - $goods_qty[$goods_id];
                }
            }
        }

        if ($enough_stock) {
            $handling_second = 8 * 3600;
        } else {
            // need transfer? pre order?
            $awaiting_days_final = 0;
            foreach ($goods_ids as $goods_id) {
                $goods_info = get_goods_info($goods_id);

                $total_goods_stock = 0;
                foreach ($goods_stock[$goods_id] as $stock) {
                    $total_goods_stock += $stock;
                }

                $row = $goodsController->getGoodsPreOrderInfo($goods_info, $total_goods_stock,1,$goods_qty[$goods_id]);

                if ($orderRulesType == 'store_pickup') {
                    $awaiting_days = $row['shop_transfer_day'] + $row['order_day'];
                } else if ( $orderRulesType == 'online') {
                    $awaiting_days = $row['transfer_day'] + $row['order_day'];
                }
   
                if ($awaiting_days_final < $awaiting_days) {
                    $awaiting_days_final = $awaiting_days;
                }
            }
            $handling_second = $awaiting_days_final * 3600 * 24;
        }

        // gmt time
        $estimated_time = $this->calculateShipmentEstimatedTime($handling_second);

        // if ($now > $working_hours_start  && $now < $working_hours_end) {
        //     //in working hours
        //     $used_second = $working_hours_end - $now;
        //     if ($awaiting_days_final > 0) {
        //         // need over one more day
        //         // echo 'estimated day:';
        //         $estimated_time = $working_hours_end + $awaiting_days_final * 86400;
        //     } else {
        //         if ($used_second < $handling_second) {
        //             // need next day to work
        //             $rest_second = $handling_second - $used_second;
        //             $working_hours_start_next_day = $working_hours_start + 1 * 86400;
        //             $estimated_time = $working_hours_start_next_day + $rest_second;
        //         } else {
        //             $estimated_time = $working_hours_start + $handling_second;
        //         }
        //     }
        // } else if ($now <= $working_hours_start) {
        //     if ($awaiting_days_final > 0) {
        //         $estimated_time = $working_hours_end + $awaiting_days_final * 86400;
        //     } else {
        //         // need next day to work
        //         $rest_second = $handling_second;
        //         $estimated_time = $working_hours_start + $rest_second; 
        //     }
        // } else if ($now >= $working_hours_end) {
        //     $working_hours_start_next_day = $working_hours_start + 1 * 86400;
        //     $working_hours_end_next_day = $working_hours_end + 1 * 86400;
        //     if ($awaiting_days_final > 0) {
        //        $estimated_time = $working_hours_end_next_day + $awaiting_days_final * 86400;
        //     } else {
        //         // need next day to work
        //         $rest_second = $handling_second;
        //         $estimated_time = $working_hours_start_next_day + $rest_second; 
        //     }
        // }

        // echo $estimated_time;
        // echo date("Y-m-d H:i:s",$estimated_time);

        // get gmt
        //echo local_strtotime(date("Y-m-d H:i:s",$estimated_time));
        //$estimated_time = local_strtotime(date("Y-m-d H:i:s",$estimated_time));
        
        $sql = "select count(*) from ".$ecs->table('order_progress')." where order_id = ".$order_id." ";
        $count = $db->getOne($sql);

        if ($count > 0) {
            $sql = "update " . $ecs->table('order_progress') . " set status = 2 where order_id = ".$order_id." ";
            $db->query($sql);
        }

        if ($enough_stock) {
            $first_all_in_stock_time = gmtime();
            $is_all_in_stock = 1;
            $logData['in_stock'] = 1;
            $handling_second = 8 * 3600;
            $handling_time = $estimated_time;
        } else {
            $first_all_in_stock_time = 'null';
            $is_all_in_stock = 0;
            $logData['in_stock'] = 0;
            $handling_time = 'null';
        }

        $logData['time'] = gmtime();
        $logData['goods_data'] = $out_of_data;
        $stock_status_history[] = $logData;
        $logDataJson = json_encode($stock_status_history);

        $sql = "insert into ".$ecs->table('order_progress')." (order_id,shipping_id,goods_status,estimated_ship_time,create_date,first_all_in_stock_time,is_all_in_stock,stock_status_history,warehouse_handling_time) 
                values (".$order_id.",".$order['shipping_id'].",".$enough_stock.",".$estimated_time.",".gmtime().",".$first_all_in_stock_time.",".$is_all_in_stock.",'".$logDataJson."',".$handling_time.") ";

        $db->query($sql);

        return true;
    }

    function calculateShipmentEstimatedTime($handling_second) {
        if ($handling_second >= 86400) {
            $working_hours_start = strtotime(date("Y-m-d 08:30:00"));
            $working_hours_end = strtotime(date("Y-m-d 17:30:00"));
            $working_hours_start_next_day = $working_hours_start + 1 * 86400;
            $working_hours_end_next_day = $working_hours_end + 1 * 86400;
        } else {
            if (!$this->is_holiday()) {
                $working_hours_start = strtotime(date("Y-m-d 08:30:00"));
                $working_hours_end = strtotime(date("Y-m-d 17:30:00"));
            
                $next_working_date_string =  $this->get_yoho_next_work_day(gmtime(), 1);
                $next_working_date = local_date('Y-m-d', $next_working_date_string );                
                $working_hours_start_next_day = strtotime(date($next_working_date . " 08:30:00"));
                $working_hours_end_next_day = strtotime(date($next_working_date ." 17:30:00"));
            } else {
                // find next working day
                $next_working_date_string =  $this->get_yoho_next_work_day(gmtime(), 1);
                $next_working_date = local_date('Y-m-d', $next_working_date_string );                
                $working_hours_start = strtotime(date($next_working_date . " 08:30:00"));
                $working_hours_end = strtotime(date($next_working_date ." 17:30:00"));

                // find next next working day
                $next_working_date_string =  $this->get_yoho_next_work_day(gmtime(), 2);
                $next_working_date = local_date('Y-m-d', $next_working_date_string );                
                $working_hours_start_next_day = strtotime(date($next_working_date . " 08:30:00"));
                $working_hours_end_next_day = strtotime(date($next_working_date ." 17:30:00"));
            }
        }

        $now = strtotime(date("Y-m-d H:i:s"));

        if ($now > $working_hours_start && $now < $working_hours_end) {
            //in working hours
            $used_second = $working_hours_end - $now;
            if ($handling_second >= 86400) {
                // need over one more day
                // echo 'estimated day:';
                $estimated_time = $working_hours_end + $handling_second;
            } else {
                if ($used_second < $handling_second) {
                    // need next day to work
                    $rest_second = $handling_second - $used_second;
                   // $working_hours_start_next_day = $working_hours_start + 1 * 86400;
                    $estimated_time = $working_hours_start_next_day + $rest_second;
                } else {
                    $estimated_time = $working_hours_start + $handling_second;
                }
            }
        } else if ($now <= $working_hours_start) {
            if ($handling_second >= 86400) {
                $estimated_time = $working_hours_end + $handling_second;
            } else {
                // need next day to work
                $rest_second = $handling_second;
                $estimated_time = $working_hours_start + $rest_second; 
            }
        } else if ($now >= $working_hours_end) {
            //$working_hours_start_next_day = $working_hours_start + 1 * 86400;
            //$working_hours_end_next_day = $working_hours_end + 1 * 86400;
            if ($handling_second >= 86400) {
               $estimated_time = $working_hours_end_next_day + $handling_second;
            } else {
                // need next day to work
                $rest_second = $handling_second;
                $estimated_time = $working_hours_start_next_day + $rest_second; 
            }
        }

        $estimated_time = local_strtotime(date("Y-m-d H:i:s",$estimated_time));
        return $estimated_time;
    }

    function checkCouponPromote($coupons,$payId) 
    {
        global $db, $ecs, $_CFG, $_LANG;
     
        $credit_card_payment = ["unionpay","visamaster","americanexpress","stripe","braintree"];

        // get payment code
        $sql = "select pay_code from ".$ecs->table('payment')." where pay_id = ".$payId." ";
        $pay_code = $db->getOne($sql);

        $result['success'] = true;
        $result['error_msg'] = '';

        // check coupons if it has promote id
        foreach ($coupons as $coupon) {
            $sql = "select * from ".$ecs->table('coupons')." c left join ".$ecs->table('coupon_promote')." cp on (c.promote_id = cp.promote_id) where coupon_code = '".$coupon."' and c.promote_id <> 0 "; 
            $coupon_info = $db->getRow($sql);
            if (!empty($coupon_info)) {
                // 1 credit card promote
                if ($coupon_info['promote_type'] == 1) {
                    if (in_array($pay_code,$credit_card_payment)) {
                        $result['success'] = true;
                        $result['error_msg'] = '';
                    } else {
                        $result['success'] = false;
                        $result['error_msg'] = sprintf($_LANG['coupon_error_promote_credit_card'], $coupon);
                    }
                    return $result;
                } else if ($coupon_info['promote_type'] == 2) { // payme
                    if ($pay_code == 'payme') {
                        $result['success'] = true;
                        $result['error_msg'] = '';
                    } else {
                        $result['success'] = false;
                        $result['error_msg'] = sprintf($_LANG['coupon_error_promote_payme'], $coupon);
                    }
                    return $result;
                }

            } else {
                return $result;
            }
        }
    }

    function checkCouponPromoteCreditCard($orderId,$bin)
    {
        global $db, $ecs, $_CFG, $_LANG;

        // get order coupons and check coupons if having promote and credit card type
        $sql = "select oc.*, cp.*, c.coupon_code from ".$ecs->table('order_coupons')." oc left join ".$ecs->table('coupons')." c on (oc.coupon_id = c.coupon_id) left join  ".$ecs->table('coupon_promote')." cp on (c.promote_id = cp.promote_id) where order_id = '".$orderId."' and cp.promote_id > 0 and cp.promote_type = 1 ";
        $coupons = $db->getAll($sql);

        $binPass = true;
        $errorMsg = '';
        $errorMsgForSystem = '';
        foreach ($coupons as $coupon) {
            // check bin
            $binPass = true;
            if (!empty($coupon['config'])) {
                $result['checked'] = true;
                $couponConfig = json_decode($coupon['config'],true);
                if (!in_array($bin,$couponConfig['card_bin'])) {
                    $binPass = false;
                    $errorMsgForSystem .= "信用卡(".$bin.") 並不適合". $coupon['promote_name'] . '-' .$coupon['coupon_code'] . "\n";
                    $errorMsg .= sprintf($_LANG['payment_error_coupon_promote_credit_card_msg'],$coupon['coupon_code']). "\n";
                } else {
                    // update promote info
                    $promoteData['bin'] = $bin;
                    $db->query("UPDATE " . $ecs->table("coupon_promote_info") . " SET data = '" . \json_encode($promoteData) . "' WHERE order_id = ".$orderId." and promote_id = ".$coupon['promote_id']." and coupon_id = ".$coupon['coupon_id']." ");
                }
            }
        }

        $result['success'] = $binPass;
        $result['error_msg'] = $errorMsg;
        $result['error_msg_system'] = $errorMsgForSystem;
        return $result;
    }

    function canChangeOrderPaymentType($orderId)
    {
        global $db, $ecs, $_CFG, $_LANG;
        // Get order coupons list
        $sql = "select oc.*, cp.*, c.coupon_code from ".$ecs->table('order_coupons')." oc left join ".$ecs->table('coupons')." c on (oc.coupon_id = c.coupon_id) left join  ".$ecs->table('coupon_promote')." cp on (c.promote_id = cp.promote_id) where order_id = '".$orderId."' and cp.promote_id > 0 and cp.promote_type != 0 ";
        $coupons = $db->getAll($sql);

        if (sizeof($coupons)>0) {
            return false;
        } else {
            return true;
        }
    }

    function canChangeOrderPaymentTypeWithFlashDeal($orderId)
    {
        global $db, $ecs;
        
        $flashdealPaymentInfoQuery = "SELECT is_online_pay AS is_online_pay, payme_only AS payme_only, alipay_only AS alipay_only 
        FROM " . $ecs->table('order_goods') . " AS og, " . $ecs->table('flashdeals') . " as fd 
        where og.flashdeal_id = fd.flashdeal_id
        and og.order_id = '$orderId'";

        $flashdealPaymentInfo = $db->getAll($flashdealPaymentInfoQuery);

        if (!isset($flashdealPaymentInfo) || sizeof($flashdealPaymentInfo) == 0) { // No flashdeal item -> allow to add
            return true;
        }

        foreach($flashdealPaymentInfo as $item) {
            if (array_sum($item) == 0) { // Unrestricted Flashdeal item
                continue;
            }
            return false;
        }

        return true;
    }

    function checkCouponPromoteCreditCardChangePayment($coupons,$payId) {
        global $db, $ecs, $_CFG, $_LANG;
     
        $credit_card_payment = "braintree";

        // get payment id
        $sql = "select pay_id from ".$ecs->table('payment')." where pay_code = 'braintree' ";
        $to_change_pay_id = $db->getOne($sql);

        // // get payment code
        // $sql = "select pay_code from ".$ecs->table('payment')." where pay_id = ".$payId." ";
        // $pay_code = $db->getOne($sql);

        $result['need_to_change'] = false;
        $result['to_change_payment_id'] = 0;
        // check coupons if it has promote id
        foreach ($coupons as $coupon) {
            $sql = "select * from ".$ecs->table('coupons')." c left join ".$ecs->table('coupon_promote')." cp on (c.promote_id = cp.promote_id) where coupon_code = '".$coupon."' and c.promote_id <> 0 and cp.promote_type = 1 "; 
            $coupon_info = $db->getRow($sql);

            if (sizeof($coupon_info) > 0) {
                // 1 credit card promote
                if ($coupon_info['promote_type'] == 1) {
                    if ($to_change_pay_id != $payId) {
                        $result['need_to_change'] = true;
                        $result['to_change_payment_id'] = $to_change_pay_id;
                    }
                    return $result;
                }
            } else {
                return $result;
            }
        }

        return $result;
    }

    function getCouponPromoteInfo($promote_id) {
        global $db, $ecs, $_CFG, $_LANG;
        
        $sql = "select cp.*, cpl.promote_desc as promote_desc_lang from ".$ecs->table('coupon_promote')." cp left join ".$ecs->table('coupon_promote_lang')." cpl on (cp.promote_id = cpl.promote_id and lang = '".$_CFG['lang']."' ) where promote_type != 0 and  cp.promote_id = ".$promote_id." ";

        $promote_info = $db->getRow($sql);

        if ($promote_info['promote_desc_lang'] != "") {
            $promote_info['promote_desc'] = $promote_info['promote_desc_lang'];
        }

        return $promote_info;
    }
    
    function checkGoodTransferStatus($order_id, $good_id) {
        $sql = "SELECT status, transfer_id FROM ".$GLOBALS['ecs']->table('order_goods_transfer_reserved')." ".
                "WHERE order_id = ".$order_id." AND goods_id = ".$good_id." AND transfer_qty > 0 ".
                "ORDER BY order_goods_transfer_id DESC";
        $result = $GLOBALS['db']->getAll($sql);
        if (sizeof($result) > 0){
            return $result[0];
        } else {
            return FALSE;
        }
    }

    function order_payment_transaction_log($pay_log_id, $pay_id, $transaction_id, $currency_code, $amount = 0, $refunded_amount = 0)
    {
        global $db, $ecs, $_CFG, $_LANG;

        $sql = "SELECT * FROM " . $GLOBALS['ecs']->table('pay_log') .
        " WHERE log_id = '$pay_log_id'";
        $pay_log = $db->getRow($sql);

        if (!empty($transaction_id) && !empty($pay_log['order_id']) && !empty($pay_id)) {
            $sql = "INSERT INTO " . $ecs->table('order_payment_transaction') . " (order_id, pay_id, gateway_transaction_id, currency_code, amount, refunded_amount) SELECT '" . $pay_log['order_id'] . "','" . $pay_id . "','" . $transaction_id  . "','" . $currency_code  . "','" . $amount  . "','" . $refunded_amount ."' FROM DUAL WHERE NOT EXISTS (SELECT * FROM " . $ecs->table('order_payment_transaction') . " WHERE `order_id` = '$pay_log[order_id]' AND `pay_id` = '$pay_id' AND `gateway_transaction_id` = '$transaction_id')";
            $db->query($sql);
        }
    }

    function checkAddressInfoFilled($user_adr_id)
    {
        global $db, $ecs;

        $sql = "SELECT country, consignee, id_doc, tel, mobile " .
                "FROM " . $GLOBALS['ecs']->table('user_address') . " " .
                "WHERE address_id = '$user_adr_id'";

        $addr_info = $db->getRow($sql);

        if (!empty($addr_info)) {
            if (!empty($addr_info['country']) && $addr_info['country'] == 3436 && (empty($addr_info['id_doc']) || empty($addr_info['tel']) || empty($addr_info['mobile']))) {
                return FALSE;
            } else {
                return TRUE;
            }
        } else {
            return FALSE;
        }
    }

    function getIdDocOrderInfo($id)
    {
        global $db, $ecs;

        $sql = "SELECT id_doc " .
                "FROM " . $GLOBALS['ecs']->table('order_info') . " " .
                "WHERE order_id = '$id'";

        $res = $db->getRow($sql);

        if (!empty($res['id_doc'])) {
            return $res['id_doc'];
        } else {
            return FALSE;
        }
    }

    function getGoodGaPrice($sa_id, $good_id){
        global $db, $ecs;

        $sql = "SELECT sa_price " .
                "FROM " . $GLOBALS['ecs']->table('sales_agent_goods') . " " .
                "WHERE sa_id = '$sa_id'" .
                "AND goods_id = '$good_id'" .
                "ORDER BY updated_at DESC ";

        $res = $db->getRow($sql);

        if (!empty($res['sa_price'])) {
            return $res['sa_price'];
        } else {
            return FALSE;
        }
    }

    function getSaRateId($sa_id){
        global $db, $ecs;

        $sql = "SELECT sa_rate_id " .
                "FROM " . $GLOBALS['ecs']->table('sales_agent_rate') . " " .
                "WHERE sa_id = '" . $sa_id . "' " .
                "AND active = '1' " .
                "ORDER BY is_default DESC ";

        $res = $db->getRow($sql);

        if (!empty($res['sa_rate_id'])) {
            return $res['sa_rate_id'];
        } else {
            return FALSE;
        }
    }

    function getSaRate($sa_id){
        global $db, $ecs;

        $sql = "SELECT sa_rate " .
                "FROM " . $GLOBALS['ecs']->table('sales_agent_rate') . " " .
                "WHERE sa_id = '" . $sa_id . "' " .
                "AND active = '1' " .
                "ORDER BY is_default DESC ";

        $res = $db->getRow($sql);

        if (isset($res['sa_rate']) && $res['sa_rate'] != "") {
            return $res['sa_rate'];
        } else {
            return FALSE;
        }
    }

    private function filterFlashDealPaymentMethod($payment_method_list)
    {
        $flashdealController = new FlashdealController();

        $res = [];
        $allowed_payment_method = $flashdealController->getFlashdealCommonPaymentMethod();
        if (is_null($allowed_payment_method)) { // No flashdeal item
            return $payment_method_list;
        } elseif (!empty($allowed_payment_method)){
            if(count($allowed_payment_method) > 0) {
                $res = array_filter($payment_method_list, function($item) use ($allowed_payment_method) {
                    return in_array($item['pay_code'], $allowed_payment_method);
                });
            } else {
                $res = [];
            }
        }
        return $res;
    }
}
