<?php

/***
* ctrl_file.php
* by MichaelHui 20170328
*
* File controller
***/

namespace Yoho\cms\Controller;
use Yoho\cms\Model;

if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}
require_once(ROOT_PATH . 'includes/cls_image.php');

class FileController extends YohoBaseController{
	const FILE_TYPE_IMAGE='image/*';
	private $supportedFormat=[],$image,$phpMaxSize,$htmlMaxSize;
	protected $currentFormat;
	public $fileObj;


	public static function getImgUrl($imgPath,$isThumbnail=false){
		if(file_exists(ROOT_PATH.$imgPath)){
			if($isThumbnail==true && file_exists(ROOT_PATH.str_replace('.','_thumb.',$imgPath)))
				return absolute_url(str_replace('.','_thumb.',$imgPath));
			else
				return absolute_url($imgPath);
		}else{
			return false;
		}
	}

	public function __construct($fileType,$fileObj=null){
		parent::__construct();
		$this->supportedFormat=[
			self::FILE_TYPE_IMAGE
		];
		
		$this->phpMaxSize = ini_get('upload_max_filesize');
        $this->htmlMaxSize = '2M';
        $this->returnObj=['status'=>false,'data'=>''];

		if(!in_array($fileType, $this->supportedFormat)){
			return false;
		}else{
			$this->currentFormat=$fileType;
			if($this->currentFormat==self::FILE_TYPE_IMAGE)
				$this->image = new \cls_image($_CFG['bgcolor']);
		}

		if(isset($fileObj)){
			$this->fileObj=$fileObj;
		}
		return true;
	}

	public function setFile($fileObj){
		$this->fileObj=$fileObj;
	}

	public function validate(){
		$this->returnObj=['status'=>false,'data'=>''];

		switch($this->currentFormat){
			case self::FILE_TYPE_IMAGE:
				if(isset($this->fileObj['error'])){
					if($this->fileObj['error']==0){
						if (!$this->image->check_img_type($this->fileObj['type']))
							$this->returnObj['data'] = "Invalid image format";
						else
							$this->returnObj['status'] = true;
					}elseif ($this->fileObj['error'] == 1){
						$this->returnObj['data'] = $_LANG['goods_img_too_big'].$this->phpMaxSize;
					}elseif ($this->fileObj['error'] == 2){
						$this->returnObj['data'] = $_LANG['goods_img_too_big'].$this->htmlMaxSize;
					}else{
						$this->returnObj['status'] = true;
					}
				}else{
					if (!$this->image->check_img_type($this->fileObj['type']))
						$this->returnObj['data'] = "Invalid image format";
					else
						$this->returnObj['status']=true;
				}
				break;
			default:
				$this->returnObj['data'] = "Unsupported format in controller";
				break;
		}
		return $this->returnObj;
	}


	public function upload($param=null){
		$this->returnObj=['status'=>false,'data'=>''];
		switch($this->currentFormat){
			case self::FILE_TYPE_IMAGE:
				$uploadResult = $this->image->upload_image($this->fileObj,null,$param['imgName'],true);
				
				if($uploadResult===false){
					$this->returnObj['data'] = $this->image->error_msg();
				}else{
					$this->returnObj['status']=true;
					$this->returnObj['data']=$uploadResult;
					if($param['makeThumb']==true){
						$thumbRes = $this->image->make_thumb(ROOT_PATH.$uploadResult,100,100);
						if($thumbRes===false){
							unlink(ROOT_PATH.$uploadResult);
							$this->returnObj['data']=$this->image->error_msg();
							$this->returnObj['status']=false;
						}else{
							rename(ROOT_PATH.$thumbRes,ROOT_PATH.str_replace('.', '_thumb.', $uploadResult));
						}
					}
				}
				break;
			default:
				break;
		}
		return $this->returnObj;
	}
}
