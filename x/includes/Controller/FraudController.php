<?php
namespace Yoho\cms\Controller;

use \Braintree;
use \Stripe;

require_once ROOT_PATH . 'includes/lib_order.php';
require_once ROOT_PATH . '/includes/stripe/init.php';
require_once ROOT_PATH . 'includes/braintree/lib/Braintree.php';

if (!defined('IN_ECS')) {
    die('Hacking attempt');
}

class FraudController extends YohoBaseController
{
    const PAYMENT_CODE_STRIPE = 'stripe';
    const PAYMENT_CODE_PAYPAL = 'braintreepaypal';
    const PAYMENT_CODE_BRAINTREE = 'braintree';
    const PAYMENT_CODES = [
        self::PAYMENT_CODE_STRIPE,
        self::PAYMENT_CODE_PAYPAL,
        self::PAYMENT_CODE_BRAINTREE,
    ];
    const HIGH_RISK_GOODS_CATEGORY = [
        '163',    // mobile
        '172',    // portable device e.g. watch
        '50',     // computer
        '227',    // monitor card
    ];
    const HIGH_RISK_EMAIL_DOMAIN = ["yahoo", "163"];
    const HIGH_RISK_EMAIL_DOMAIN_EXCLUDE = ["yahoo.com.hk"];
    const VALID_OFFLINE_PAYMENT = ["eps", "unionpay", "visamaster", "americanexpress", "cash", "bank", "ebanking", "bankmo", "ebankingmo", "fps"];

    public function calculateOverallRiskLevel($order_id)
    {
        global $db, $ecs;
        $rec = $db->getAll("SELECT * FROM " . $ecs->table("order_fraud") . " WHERE order_id = $order_id");
        $paid = $db->getOne("SELECT 1 FROM " . $ecs->table("order_info") . " WHERE order_id = $order_id AND pay_status = " . PS_PAYED);
        
        if (empty($rec)) {
            $current_risk = self::calculateRiskLevel($order_id);
            return [
                'overall_risk'  => $current_risk['risk_level'],
                'detail'        => [$current_risk['parts']],
                'paid'          => $paid,
            ];
        }

        $risk = [
            'overall_risk'  => 0,
            'detail'        => [],
            'paid'          => $paid,
        ];
        $count = count($rec);
        foreach ($rec as $row) {
            $txn_id = $row['txn'];
            $pay_id = $row['pay_id'];
            $payment = payment_info($pay_id);

            $transaction = self::getOrderTransaction($txn_id, $payment);
            if (empty($transaction)) {
                $current_risk = self::calculateRiskLevel($order_id);
            } else {
                $data = self::processTransaction($payment['pay_code'], $transaction);
                $current_risk = self::calculateRiskLevel($order_id, $data);
            }
            $risk['overall_risk'] += $current_risk['risk_level'];
            $current_risk['parts']['txn_id'] = $txn_id;
            $current_risk['parts']['success'] = $row['success'];
            if ($payment['pay_code'] == self::PAYMENT_CODE_STRIPE) {
                $current_risk['parts']['url'] = "https://dashboard.stripe.com/payments/$txn_id";
            } elseif (in_array($payment['pay_code'], [self::PAYMENT_CODE_BRAINTREE, self::PAYMENT_CODE_PAYPAL])) {
                $current_risk['parts']['url'] = "https://" . (Braintree\Configuration::environment() == "sandbox" ? "sandbox" : "www") . ".braintreegateway.com/merchants/" . Braintree\Configuration::merchantId() . "/transactions/$txn_id";
            }
            $risk['detail'][] = $current_risk['parts'];
        }
        $risk['overall_risk'] /= $count;
        return $risk;
    }

    public function getOrderTransaction($txn_id, $payment)
    {
        if ($payment['pay_code'] == self::PAYMENT_CODE_STRIPE) {
            if (strpos($_SERVER['SERVER_NAME'], 'beta.yohohongkong.com') === false) {
                Stripe\Stripe::setApiKey($payment['pay_config_list']['stripe_secret_key']);
            } else {
                Stripe\Stripe::setApiKey("sk_live_Jr25LQ8cNf0YHgshsppc5gYV");
            }
            $transaction = Stripe\Charge::retrieve($txn_id);
        } elseif (in_array($payment['pay_code'], [self::PAYMENT_CODE_BRAINTREE, self::PAYMENT_CODE_PAYPAL])) {
            if (strpos($_SERVER['SERVER_NAME'], 'beta.yohohongkong.com') === false) {
                Braintree\Configuration::environment('production');
                Braintree\Configuration::merchantId($payment['pay_config_list']['braintree_merchantId']);
                Braintree\Configuration::publicKey($payment['pay_config_list']['braintree_publicKey']);
                Braintree\Configuration::privateKey($payment['pay_config_list']['braintree_privateKey']);
            } else {
                Braintree\Configuration::environment('sandbox');
                Braintree\Configuration::merchantId('fd3vmpwh4cy7qcv3');
                Braintree\Configuration::publicKey('38k9tgrwxz324dkd');
                Braintree\Configuration::privateKey('69863db467746b406b5a7e951c35caf8');
            }

            $query = [Braintree\TransactionSearch::id()->is($txn_id)];
            $result = Braintree\Transaction::search($query);
            $transaction = $result->firstItem();
        }
        return $transaction;
    }

    public function processTransaction($pay_code, $transaction)
    {
        $data = [
            'pay_code' => $pay_code,
        ];
        if ($pay_code == self::PAYMENT_CODE_STRIPE) {
            $customFields = $transaction->metadata;
            $data['customer_info'] = [
                'raw'   => $customFields,
                'ip'    => $customFields->ip,
                'email' => $customFields->email,
            ];

            $source = $transaction->source;
            if (empty($source->three_d_secure)) {
                $card = $source->card;
                $data['payment_info'] = [
                    'raw'       => $card,
                    'auth'      => false,
                    'cardType'  => $card->brand,
                    'location'  => $card->country,
                    'cvv'       => $card->cvc_check == 'pass',
                ];
            } else {
                $card = $source->three_d_secure;
                $data['payment_info'] = [
                    'raw'           => $card,
                    'auth'          => $card->authenticated,
                ];
                if ($card['three_d_secure']) {
                    $data['payment_info']['cardType'] = $card->brand;
                    $data['payment_info']['location'] = $card->country;
                    $data['payment_info']['cvv']      = $card->cvc_check == 'pass';
                }
            }
        } elseif (in_array($pay_code, [self::PAYMENT_CODE_BRAINTREE, self::PAYMENT_CODE_PAYPAL])) {
            $customFields = $transaction->customFields;
            $data['customer_info'] = [
                'raw'   => $customFields,
                'ip'    => $customFields['ip'],
                'email' => $customFields['email'],
            ];

            if ($pay_code == self::PAYMENT_CODE_BRAINTREE) {
                $creditCardDetails = $transaction->creditCardDetails;
                $cardType = $creditCardDetails->cardType;
                $tds = $transaction->threeDSecureInfo;
                $data['payment_info'] = [
                    'raw'           => $creditCardDetails,
                    'cardType'      => $creditCardDetails->cardType,
                    'location'      => $creditCardDetails->countryOfIssuance,
                    'auth'          => $tds->enrolled == 'Y' && $tds->liabilityShifted && $tds->liabilityShiftPossible,
                    'cvv'           => $transaction->cvvResponseCode == 'M',
                ];
            } elseif ($pay_code == self::PAYMENT_CODE_PAYPAL) {
                $paypalDetails = $transaction->paypalDetails;
                $data['payment_info'] = [
                    'raw'           => $paypalDetails,
                    'auth'          => $paypalDetails->sellerProtectionStatus == "ELIGIBLE",
                ];
            }
        }
        return $data;
    }

    public function calculateRiskLevel($order_id, $data)
    {
        global $db, $ecs;
        $risk['overall_risk'] = 0;
        $parts = [];
        $order = order_info($order_id);
        $order_goods_cats = $db->getCol(
            "SELECT DISTINCT g.cat_id FROM " . $ecs->table("order_goods") . " og " .
            "LEFT JOIN " . $ecs->table("goods") . " g ON g.goods_id = og.goods_id " .
            "WHERE og.order_id = $order_id"
        );

        // Step 1: handle user risk
        $user = $db->getRow("SELECT * FROM " . $ecs->table("users") . " WHERE user_id = $order[user_id]");
        if ($user['is_fraud']) {
            $risk['overall_risk'] += 100;
            $parts["User"]["可疑用戶"] = 100;

            return [
                "risk_level" => $risk['overall_risk'],
                "parts" => $parts,
            ];
        } else {
            $ip = empty($data) ? $user['last_ip'] : $data['customer_info']['ip'];
            $email = empty($data) ? $user['email'] : $data['customer_info']['email'];
            if ($db->getOne("SELECT 1 FROM " . $ecs->table("fraud_user_info") . " WHERE type = 0 AND value = '$ip'")) {
                $risk['overall_risk'] += 100;
                $parts["User"]["可疑IP"] = 100;

                return [
                    "risk_level" => $risk['overall_risk'],
                    "parts" => $parts,
                ];
            }
            if ($db->getOne("SELECT 1 FROM " . $ecs->table("fraud_user_info") . " WHERE type = 1 AND value = '$email'")) {
                $risk['overall_risk'] += 100;
                $parts["User"]["可疑電郵"] = 100;

                return [
                    "risk_level" => $risk['overall_risk'],
                    "parts" => $parts,
                ];
            }
            $valid_payment_ids = $db->getCol("SELECT pay_id FROM " . $ecs->table("payment") . " WHERE pay_code ". db_create_in(self::VALID_OFFLINE_PAYMENT));
            if (intval($db->getOne("SELECT COUNT(*) FROM " . $ecs->table("order_info") . " WHERE user_id = $order[user_id] AND order_id != $order_id AND pay_status = " . PS_PAYED . " AND pay_id " . db_create_in($valid_payment_ids))) > 0) {
                $risk['overall_risk'] -= 10;
                $parts["User"]["曾線下付款"] = -10;
            }
            if (intval($user['rank_points']) == 0) {
                $risk['overall_risk'] += 10;
                $parts["User"]["首次購物"] = 10;
            }
            // define certain email domain as safe
            $email_excluded = false;
            foreach (self::HIGH_RISK_EMAIL_DOMAIN_EXCLUDE as $domain) {
                if (strpos($data['customer_info']['email'], $domain) !== false) {
                    $email_excluded = true;
                }
            }
            if (!$email_excluded) {
                foreach (self::HIGH_RISK_EMAIL_DOMAIN as $domain) {
                    if (strpos($data['customer_info']['email'], $domain) !== false) {
                        $risk['overall_risk'] += 10;
                        $parts["User"]["可疑電郵域名"] = 10;
                    }
                }
            }
            if (intval($db->getOne("SELECT COUNT(*) FROM " . $ecs->table("order_fraud") . " WHERE success = 0 AND order_id IN (SELECT order_id FROM " . $ecs->table("order_info") . " WHERE user_id = $order[user_id] AND order_id != $order_id)")) > 0) {
                $risk['overall_risk'] += 10;
                $parts["User"]["付款失敗過多"] = 10;
            }
        }

        if (!empty($data)) {
            // Step 1: handle payment risk
            // Stripe/Braintree: 3DS not pass/enable; Paypal: not ELIGIBLE
            if (!$data['payment_info']['auth']) {
                $risk['overall_risk'] += 30;
                $parts["Payment"][$data['pay_code'] == self::PAYMENT_CODE_PAYPAL ? "沒有Paypal 賣家保障" : "沒有3DS 驗證"] = 30;
            }
    
            // Stripe rules
            if ($data['pay_code'] == self::PAYMENT_CODE_STRIPE) {
                // Only process if 3DS not pass
                if (!$data['payment_info']['auth']) {
                    if (!$data['payment_info']['cvv']) {
                        $risk['overall_risk'] += 20;
                        $parts["Payment"]["CVV 驗證"] = 20;
                    }
                    if ($data['payment_info']['location'] != "HK") {
                        $risk['overall_risk'] += 20;
                        $parts["Payment"]["非香港地區"] = 20;
                    }
                    if ($data['payment_info']['cardType'] == "American Express") {
                        $risk['overall_risk'] += 20;
                        $parts["Payment"]["信用卡種類"] = 20;
                    }
                }
            }
    
            // Braintree rules
            if ($data['pay_code'] == self::PAYMENT_CODE_BRAINTREE) {
                if (!$data['payment_info']['cvv']) {
                    $risk['overall_risk'] += 20;
                    $parts["Payment"]["CVV 驗證"] = 20;
                }
                if ($data['payment_info']['location'] != "HKG") {
                    $risk['overall_risk'] += 20;
                    $parts["Payment"]["非香港地區"] = 20;
                }
                if ($data['payment_info']['cardType'] == "American Express") {
                    $risk['overall_risk'] += 20;
                    $parts["Payment"]["信用卡種類"] = 20;
                }
            }
    
            // Paypal rules
            if ($data['pay_code'] == self::PAYMENT_CODE_PAYPAL) {
            }
        }


        // Step 2: handle order risk
        // Order amount risk
        if (floatval($order['goods_amount']) >= 3000) {
            $risk['overall_risk'] += 20;
            $parts["Order"]["金額大於3000"] = 20;
        }

        if (intval($db->getOne("SELECT COUNT(*) FROM " . $ecs->table("order_fraud") . " WHERE success = 0 AND order_id = $order_id")) > 0) {
            $risk['overall_risk'] += 10;
            $parts["Order"]["付款失敗"] = 10;
        }

        // Order goods risk
        $high_risk_cats = self::getHighRiskCategory();
        if (!empty(array_intersect($high_risk_cats, $order_goods_cats))) {
            $risk['overall_risk'] += 20;
            $parts["Order"]["高危產品分類"] += 20;
        }
        // foreach ($order_goods_cats as $cat_id) {
        //     if (in_array($cat_id, $high_risk_cats)) {
        //         if (!isset($parts["Order"]["產品分類"])) {
        //             $parts["Order"]["產品分類"] = 0;
        //         }
        //         $risk['overall_risk'] += 5;
        //         $parts["Order"]["產品分類"] += 5;
        //     }
        // }

        return [
            "risk_level" => $risk['overall_risk'],
            "parts" => $parts,
        ];
    }

    public function fraudOrderControl($order_id, $pay_code, $transaction)
    {
        // include_once ROOT_PATH . ''
        global $db, $ecs, $_LANG;
        $order_info = order_info($order_id);

        // Only mark if order is not held before
        if ($order_info['is_hold'] == "0") {

            $data = self::processTransaction($pay_code, $transaction);
            $current_risk = self::calculateRiskLevel($order_id, $data);

            $risk_level = $current_risk['risk_level'];
            if ($risk_level >= 70) {
                $arr = array('is_hold' => 3, 'to_buyer' => "Hold - 高風險訂單");
                update_order($order_id, $arr);
                order_action($order_info['order_sn'], $order_info['order_status'], $order_info['shipping_status'], $order_info['pay_status'], "[系統備註] 高風險訂單: 風險系數 $risk_level", 'admin');
            }
        }
    }

    public function getHighRiskCategory($name = false)
    {
        global $db, $ecs;
        $high_risk_cats = [];
        foreach (self::HIGH_RISK_GOODS_CATEGORY as $cat) {
            $cat_ids = [$cat];
            while (!empty($cat_ids)) {
                $high_risk_cats = array_unique(array_merge($high_risk_cats, $cat_ids));
                $cat_ids = $db->getCol("SELECT cat_id FROM " . $ecs->table('category') . " WHERE parent_id " . db_create_in($cat_ids));
            }
        }

        if ($name) {
            return $db->getCol("SELECT cat_name FROM " . $ecs->table('category') . " WHERE cat_id " . db_create_in($high_risk_cats));
        }
        return $high_risk_cats;
    }
}