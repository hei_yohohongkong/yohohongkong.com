<?php

/***
* rmaController.php
*
*
*
***/
namespace Yoho\cms\Controller;

use Yoho\cms\Model;

use \Exception;

use Yoho\cms\Controller\ErpController;

if (!defined('IN_ECS')) {
    die('Hacking attempt');
}
class ShopController extends YohoBaseController
{
    const PENDING_FOR_CHECKING = '0';
    const NOTIFY_SUPPLIER = '1';
    const WAITING_FOR_RETURN_TO_SUPPLIER = '2';
    const REPAIRING = '3';
    const REPAIRED = '4';
    const FINISHED = '5';

    const TYPE_ARCHIVED = 0;
    const TYPE_ACTIVE = 1;
    // const TYPE_DELETED = 2;

    const CHANNEL_REPAIR = 0;
    const CHANNEL_REPLACEMENT = 1;
    const CHANNEL_BAD_GOODS = 2;

    const REPAIR_HANDLE_TYPE_SUPPLIER = 0;
    const REPAIR_HANDLE_TYPE_OURSELVES = 1;
    const REPAIR_HANDLE_TYPE_BUY_PARTS = 2;
    const REPAIR_HANDLE_TYPE_NO_NEED_TO_REPAIR = 3;
    const REPAIR_HANDLE_TYPE_WRITE_OFF = 4;

    const DEPARTMENT_SALE = 0;
    const DEPARTMENT_PRODUCT = 1;
    const DEPARTMENT_WAREHOUSE = 2;

    const ROLE_SUPER_ADMIN = 3;
    const ROLE_SALES_TEAM = 2;
    const ROLE_SALES_TEAM_MANAGER = 14;
    const ROLE_TECH_TEAM = 4;
    const ROLE_PRODUCT_TEAM = 5;
    const ROLE_WAREHOUSE_TEAM = 11;
    const ROLE_WAREHOUSE_MANAGER = 13;
    

    private static $statusValues=['等待檢查','通知供應商收貨','等待供應商回收','等待維修中','維修完成待處理','完成'];
    // private static $goodsStatusValues=['等待盤點','已盤點','等待覆核','已完成覆核','重新盤點'];
    private static $typeValues=['已封存','未封存'];
    private static $channelValues=['維修','換貨','壞貨'];
    private static $departmentValues=['Sale Team','Product Team','Wearhouse Team'];

    private $tablename='';
    
    public function __construct()
    {
        parent::__construct();
       
        $this->adminuserController = new AdminuserController();
        $this->erpController =  new ErpController();
        $this->warehouseTransferController =  new WarehouseTransferController();
        $this->orderController = new OrderController();
    }
   
    public function getShopOrderGoodsSoldList($is_pagination=false)
    {
        global $db, $ecs, $userController ,$_CFG;
        // only count 門市購買 2
        ini_set('memory_limit', '800M');
        if (!empty($_REQUEST['period'])) {
            if ($_REQUEST['period'] == 1 || $_REQUEST['period'] == 0) {
                $end_time_period= local_strtotime(date("Y-m-d", strtotime( date( 'Y-m-d' )." -".$_REQUEST['period']." days")));
                $start_time_period  = local_strtotime(date("Y-m-d", strtotime( date( 'Y-m-d' )." -".$_REQUEST['period']." days")));
            } else {
                $end_time_period= local_strtotime(date("Y-m-d", strtotime( date( 'Y-m-d' )." -0 months")));
                $start_time_period  = local_strtotime(date("Y-m-d", strtotime( date( 'Y-m-d' )." -".$_REQUEST['period']." days")));            
            }
        } else {
            // last 30 days
            $end_time_period= local_strtotime(date("Y-m-d", strtotime( date( 'Y-m-d' )." -0 months")));
            $start_time_period  = local_strtotime(date("Y-m-d H:i:s", strtotime( date( 'Y-m-d') ." -0 days")));
        }

        if (empty($_REQUEST['start_date'])) {
            $start_time = $start_time_period;
            $start_date = $today_date;
        } else {
            $start_time = local_strtotime($_REQUEST['start_date']);
            $start_date = $_REQUEST['start_date'];
        }

        if (empty($_REQUEST['end_date'])) {
            $end_time = $end_time_period;
            $end_date = $today_date;
        }else{
            $end_time= local_strtotime($_REQUEST['end_date']);
            $end_date = $_REQUEST['end_date'];
        }

        $shop_warehouse_id = 1; // shop
 
        $main_warehouse_id = $this->erpController->getMainWarehouseId();

        $sql = "select count(og.goods_id) from  ".$ecs->table('order_info')." oi ".
               " left join ".$ecs->table('order_goods')." og on (oi.order_id = og.order_id) ".
               " where platform = 'pos' and pay_status = ".PS_PAYED." and (order_type = '' or order_type is null) and shipping_id = 2 and pay_time BETWEEN " . ($start_time) . " AND " . ($end_time + 86399) .  
               " group by og.goods_id" ;
        
        $total_rows = $db->getAll($sql);
        $total_count = sizeof($total_rows);

        $_REQUEST['record_count'] = $total_count;
        $_REQUEST['sort_by']    = empty($_REQUEST['sort_by']) ? 'goods_id' : trim($_REQUEST['sort_by']);
        $_REQUEST['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
       
        //  sold items recently
        $sql = "select og.goods_sn,og.goods_id, g.goods_thumb, og.goods_name, sum(og.goods_number) as sold_goods_total  from  ".$ecs->table('order_info')." oi ".
               " left join ".$ecs->table('order_goods')." og on (oi.order_id = og.order_id) ".
               " left join ".$ecs->table('goods')." g on (og.goods_id = g.goods_id) ".
               " where platform = 'pos' and pay_status = ".PS_PAYED." and (order_type = '' or order_type is null) and shipping_id = 2 and pay_time BETWEEN " . ($start_time) . " AND " . ($end_time + 86399) .  
               " group by goods_id" ;

        $sql .= ' order by '. $_REQUEST['sort_by'] . ' ' .  $_REQUEST['sort_order'];
        $sql .= ($is_pagination ? " LIMIT " . $_REQUEST['start'] . ", " . $_REQUEST['page_size'] : '');

        $res = $db->getAll($sql);
        
        $goods_ids = [];
        foreach ($res as &$goods) {
            $goods_ids[] = $goods['goods_id'];
            $goods['goods_thumb'] = '<span id="goods_thumb_'.$goods['goods_id'].'"><img  width="40px" src="../'.$goods['goods_thumb'].'" /></span>';
        }

        // add po restock
        $sql = "select g.goods_sn, g.goods_id, g.goods_thumb, g.goods_name 
        from ".$ecs->table('erp_warehousing')." ew 
        left join ".$ecs->table("erp_warehousing_item")." ewi on (ew.warehousing_id = ewi.warehousing_id) 
        left join ".$ecs->table('goods')." g on (ewi.goods_id = g.goods_id) 
        where ew.warehousing_style_id = 1 and ew.status = 3 and ew.approve_time BETWEEN " . ($start_time) . " AND " . ($end_time + 86399) ." and warehouse_id = ".$main_warehouse_id."  
        group by g.goods_id";

        $sql .= ' order by '. $_REQUEST['sort_by'] . ' ' .  $_REQUEST['sort_order'];
        $sql .= ($is_pagination ? " LIMIT " . $_REQUEST['start'] . ", " . $_REQUEST['page_size'] : '');

        $res_po = $db->getAll($sql);

        foreach ($res_po as &$goods) {
            //$goods_ids[] = $goods['goods_id'];
            $goods['goods_thumb'] = '<span id="goods_thumb_'.$goods['goods_id'].'"><img  width="40px" src="../'.$goods['goods_thumb'].'" /></span>';
        }

        // merge array
        foreach ($res_po as $goods2) {
            if (!in_array($goods2['goods_id'],$goods_ids)) {
                $goods2['source'] = 'po';
                $res[] = $goods2;
                $goods_ids[] = $goods2['goods_id'];
            }
        }

        $sellable_warehouse_info = $this->erpController->getSellableWarehouseInfo();
        $shop_stock_sellable_info = $this->erpController->getSellableProductsQtyByWarehouses($goods_ids);
  
        // 1 months days sold
        $compare_days = 30;
        $end_time_period_compare_days= local_strtotime(date("Y-m-d", strtotime( date( 'Y-m-d' )." -0 months")));
        $start_time_period_compare_days  = local_strtotime(date("Y-m-d", strtotime( date( 'Y-m-d' )." -".$compare_days." days")));   
        $sql = "select og.goods_id, sum(og.goods_number) as sold_goods_total  from  ".$ecs->table('order_info')." oi ".
               " left join ".$ecs->table('order_goods')." og on (oi.order_id = og.order_id) ".
               " left join ".$ecs->table('goods')." g on (og.goods_id = g.goods_id) ".
               " where platform = 'pos' and pay_status = ".PS_PAYED." and (order_type = '' or order_type is null) and shipping_id = 2 and pay_time BETWEEN " . ($start_time_period_compare_days) . " AND " . ($end_time_period_compare_days + 86399) .  
               " group by goods_id" ;
        
        $res_compare = $db->getAll($sql);
        $goods_sold_compare = [];
        foreach ($res_compare as &$goods_compare) {
            $goods_sold_compare[$goods_compare['goods_id']] = $goods_compare['sold_goods_total'];
        }

        // echo '<pre>';
        // print_R($goods_sold_compare);
        // exit;

        $warehouseTransferController = $this->warehouseTransferController;
        $goodsShopRestockTransferReserved = $warehouseTransferController->getGoodsTransferReservedWarehouseToByReasonType($shop_warehouse_id,$warehouseTransferController::REASON_TYPE_SHOP_RESTOCK);
        
        foreach ($res as &$goods) {
            foreach ($sellable_warehouse_info as $wh) {
                if ($wh['warehouse_id'] == $shop_warehouse_id) {
                    $class_alert = 'shop_block';
                    
                } else {
                    $class_alert = '';
                }

                $goods['goods_sellable_total_'.$wh['warehouse_id'].'_html'] = '<a target="_blank" href="erp_stock_inquiry.php?act=list&goods_sn='.$goods['goods_sn'].'"><span id="warehouse_qty_'.$wh['warehouse_id'].'_'.$goods['goods_id'].'" class="'.$class_alert.'">'.(!empty($shop_stock_sellable_info[$goods['goods_id']][$wh['warehouse_id']])? $shop_stock_sellable_info[$goods['goods_id']][$wh['warehouse_id']] : 0 ) .'</span></a>';
                $goods['goods_sellable_total_'.$wh['warehouse_id']] = (!empty($shop_stock_sellable_info[$goods['goods_id']][$wh['warehouse_id']])? $shop_stock_sellable_info[$goods['goods_id']][$wh['warehouse_id']] : 0 ) ;
            }
            
            $goods['goods_sn_html'] = '<span id="goods_sn_'.$goods['goods_id'].'">' . $goods['goods_sn'] . '</span>';
            $goods['goods_name'] = '<span id="goods_name_'.$goods['goods_id'].'">' . $goods['goods_name'] . '</span>';

            $goods['forecast_goods_sale_7day'] = round($goods_sold_compare[$goods['goods_id']] / $compare_days * 7);
            

           
            $shop_stock_sellable_info[$goods['goods_id']][$shop_warehouse_id];
            //echo '<br>';

            if (isset($goodsShopRestockTransferReserved[$goods['goods_id']])) {
                $goods['goods_restocked'] = $goodsShopRestockTransferReserved[$goods['goods_id']];
            } else {
                $goods['goods_restocked'] = 0;
            }

            $goods['goods_restocked_html'] = '<a target="_blank" href="erp_warehouse_transfer.php?goods_sn='.$goods['goods_id'].'&warehouse_to='.$shop_warehouse_id.'">'.$goods['goods_restocked'].'</a>';

            // 3: 7
            // echo '<pre>'; 
            // print_r($shop_stock_sellable_info);

            if ($shop_stock_sellable_info[$goods['goods_id']][$shop_warehouse_id] > 0) {
                $shop_sellable_stock = $shop_stock_sellable_info[$goods['goods_id']][$shop_warehouse_id]; 
            } else {
                $shop_sellable_stock = 0;
            }

            if ($shop_stock_sellable_info[$goods['goods_id']][$main_warehouse_id] > 0) {
                $main_sellable_stock = $shop_stock_sellable_info[$goods['goods_id']][$main_warehouse_id]; 
            } else {
                $main_sellable_stock = 0;
            }

            $total_stock = $main_sellable_stock + $shop_sellable_stock;

            $shop_expected = round($total_stock * 0.3);
 
            if ($goods['forecast_goods_sale_7day'] == 0) {
                //$need_restore = $shop_expected >= 3 ? 3 : $shop_expected;
                $need_restore = $shop_expected - $goods['goods_restocked'] - $shop_sellable_stock;
            } else {
                if ($shop_expected > $goods['forecast_goods_sale_7day']) {
                    $shop_expected = $goods['forecast_goods_sale_7day'];
                }

                $need_restore = $shop_expected - $goods['goods_restocked'] - $shop_sellable_stock;
            }
           
            $goods['need_restore'] = $need_restore > 0 ? $need_restore : 0; 

            if ($goods['need_restore'] > 0) {
                $class_alert = 'tr_alert_block';
            } else {
                $class_alert = '';
            }
            $goods['forecast_goods_sale_7day_html'] = '<a target="_blank" href="order.php?act=list&includes_goods='.$goods['goods_sn'].'&pay_status=2&shipping_id=2"><span class="'.$class_alert.'">'.$goods['forecast_goods_sale_7day'].'</span></a>';

            $goods['need_restore_html'] = '<input id="need_restore_'.$goods['goods_id'].'" name="end_date" type="text" id="end_date" size="15" value="'.$goods['need_restore'].'" class="form-control" >';
            if (empty($goods['sold_goods_total'])) {
                $goods['sold_goods_total'] = 0;
            }

            if ($goods['source']== 'po') {
                $goods['restock_type'] = 'PO';
            } else {
                $goods['restock_type'] = '賣出';
            }
        }
     
        usort($res, "cmp");

        $arr = array(
            'data' => $res,
            'record_count' => $total_count
        );

        return $arr;
    }

    public function getCustomGoodsList($is_pagination=false)
    {
        global $db, $ecs, $userController ,$_CFG;
        // only count 門市購買 2
 
        // echo '<pre>';
        // print_r($_REQUEST);
        // exit;

        $shop_warehouse_id = 1; // shop
 
        $main_warehouse_id = $this->erpController->getMainWarehouseId();

        // $sql = "select count(og.goods_id) from  ".$ecs->table('order_info')." oi ".
        //        " left join ".$ecs->table('order_goods')." og on (oi.order_id = og.order_id) ".
        //        " where platform = 'pos' and pay_status = ".PS_PAYED." and (order_type = '' or order_type is null) and shipping_id = 2 and pay_time BETWEEN " . ($start_time) . " AND " . ($end_time + 86399) .  
        //        " group by og.goods_id" ;
        
        // $total_rows = $db->getAll($sql);
        // $total_count = sizeof($total_rows);

        //$_REQUEST['record_count'] = $total_count;
        $_REQUEST['sort_by']    = empty($_REQUEST['sort_by']) ? 'goods_id' : trim($_REQUEST['sort_by']);
        $_REQUEST['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
       
        $sql = "select g.goods_sn, g.goods_id, g.goods_thumb, g.goods_name 
                from ".$ecs->table('goods')." g  
                where  goods_sn ".  db_create_in($_REQUEST['customer_goods']) ."  
                group by g.goods_id";

        $sql .= ' order by '. $_REQUEST['sort_by'] . ' ' .  $_REQUEST['sort_order'];
        $sql .= ($is_pagination ? " LIMIT " . $_REQUEST['start'] . ", " . $_REQUEST['page_size'] : '');
        $res = $db->getAll($sql);
        
        $goods_ids = [];
        foreach ($res as &$goods) {
            $goods_ids[] = $goods['goods_id'];
            $goods['goods_thumb'] = '<span id="custom_goods_thumb_'.$goods['goods_id'].'"><img  width="40px" src="../'.$goods['goods_thumb'].'" /></span>';
        }

        $sellable_warehouse_info = $this->erpController->getSellableWarehouseInfo();
        $shop_stock_sellable_info = $this->erpController->getSellableProductsQtyByWarehouses($goods_ids);

        // 1 months days sold
        $compare_days = 30;
        $end_time_period_compare_days= local_strtotime(date("Y-m-d", strtotime( date( 'Y-m-d' )." -0 months")));
        $start_time_period_compare_days  = local_strtotime(date("Y-m-d", strtotime( date( 'Y-m-d' )." -".$compare_days." days")));   
        $sql = "select og.goods_id, sum(og.goods_number) as sold_goods_total  from  ".$ecs->table('order_info')." oi ".
               " left join ".$ecs->table('order_goods')." og on (oi.order_id = og.order_id) ".
               " left join ".$ecs->table('goods')." g on (og.goods_id = g.goods_id) ".
               " where platform = 'pos' and pay_status = ".PS_PAYED." and (order_type = '' or order_type is null) and shipping_id = 2 and pay_time BETWEEN " . ($start_time_period_compare_days) . " AND " . ($end_time_period_compare_days + 86399) .  
               " group by goods_id" ;
        
        $res_compare = $db->getAll($sql);
        $goods_sold_compare = [];
        foreach ($res_compare as &$goods_compare) {
            $goods_sold_compare[$goods_compare['goods_id']] = $goods_compare['sold_goods_total'];
        }

        // echo '<pre>';
        // print_R($goods_sold_compare);
        // exit;

        $warehouseTransferController = $this->warehouseTransferController;
        $goodsShopRestockTransferReserved = $warehouseTransferController->getGoodsTransferReservedWarehouseToByReasonType($shop_warehouse_id,$warehouseTransferController::REASON_TYPE_SHOP_RESTOCK);
        
        foreach ($res as &$goods) {
            foreach ($sellable_warehouse_info as $wh) {
                if ($wh['warehouse_id'] == $shop_warehouse_id) {
                    $class_alert = 'shop_block';
                    
                } else {
                    $class_alert = '';
                }

                $goods['goods_sellable_total_'.$wh['warehouse_id'].'_html'] = '<a target="_blank" href="erp_stock_inquiry.php?act=list&goods_sn='.$goods['goods_sn'].'"><span id="custom_warehouse_qty_'.$wh['warehouse_id'].'_'.$goods['goods_id'].'" class="'.$class_alert.'">'.(!empty($shop_stock_sellable_info[$goods['goods_id']][$wh['warehouse_id']])? $shop_stock_sellable_info[$goods['goods_id']][$wh['warehouse_id']] : 0 ) .'</span></a>';
                $goods['goods_sellable_total_'.$wh['warehouse_id']] = (!empty($shop_stock_sellable_info[$goods['goods_id']][$wh['warehouse_id']])? $shop_stock_sellable_info[$goods['goods_id']][$wh['warehouse_id']] : 0 ) ;
            }
            
            $goods['goods_sn_html'] = '<span id="custom_goods_sn_'.$goods['goods_id'].'">' . $goods['goods_sn'] . '</span>';
            $goods['goods_name'] = '<span id="custom_goods_name_'.$goods['goods_id'].'">' . $goods['goods_name'] . '</span>';

            $goods['forecast_goods_sale_7day'] = round($goods_sold_compare[$goods['goods_id']] / $compare_days * 7);
            // if ($goods['forecast_goods_sale_7day'] > $goods['goods_sellable_total_'.$shop_warehouse_id]) {
            //     $class_alert = 'tr_alert_block';
            // } else {
            //     $class_alert = '';
            // }

            //$goods['forecast_goods_sale_7day_html'] = '<a target="_blank" href="order.php?act=list&includes_goods='.$goods['goods_sn'].'&pay_status=2&shipping_id=2"><span class="'.$class_alert.'">'.$goods['forecast_goods_sale_7day'].'</span></a>';

            $shop_stock_sellable_info[$goods['goods_id']][$shop_warehouse_id];
            //echo '<br>';

            if (isset($goodsShopRestockTransferReserved[$goods['goods_id']])) {
                $goods['goods_restocked'] = $goodsShopRestockTransferReserved[$goods['goods_id']];
            } else {
                $goods['goods_restocked'] = 0;
            }

            $goods['goods_restocked_html'] = '<a target="_blank" href="erp_warehouse_transfer.php?goods_sn='.$goods['goods_id'].'&warehouse_to='.$shop_warehouse_id.'">'.$goods['goods_restocked'].'</a>';

            // 3: 7
            // echo '<pre>'; 
            // print_r($shop_stock_sellable_info);

            if ($shop_stock_sellable_info[$goods['goods_id']][$shop_warehouse_id] > 0) {
                $shop_sellable_stock = $shop_stock_sellable_info[$goods['goods_id']][$shop_warehouse_id]; 
            } else {
                $shop_sellable_stock = 0;
            }

            if ($shop_stock_sellable_info[$goods['goods_id']][$main_warehouse_id] > 0) {
                $main_sellable_stock = $shop_stock_sellable_info[$goods['goods_id']][$main_warehouse_id]; 
            } else {
                $main_sellable_stock = 0;
            }

            $total_stock = $main_sellable_stock + $shop_sellable_stock;

            $shop_expected = round($total_stock * 0.3);
 
            if ($goods['forecast_goods_sale_7day'] == 0) {
                //$need_restore = $shop_expected >= 3 ? 3 : $shop_expected;
                $need_restore = $shop_expected - $goods['goods_restocked'] - $shop_sellable_stock;
            } else {
                if ($shop_expected > $goods['forecast_goods_sale_7day']) {
                    $shop_expected = $goods['forecast_goods_sale_7day'];
                }

                $need_restore = $shop_expected - $goods['goods_restocked'] - $shop_sellable_stock;
            }
           
            $goods['need_restore'] = $need_restore > 0 ? $need_restore : 0; 

            if ($goods['need_restore'] > 0) {
                $class_alert = 'tr_alert_block';
            } else {
                $class_alert = '';
            }

            $goods['forecast_goods_sale_7day_html'] = '<a target="_blank" href="order.php?act=list&includes_goods='.$goods['goods_sn'].'&pay_status=2&shipping_id=2"><span class="'.$class_alert.'">'.$goods['forecast_goods_sale_7day'].'</span></a>';


            $goods['need_restore_html'] = '<input data-goods_id="'.$goods['goods_id'].'" id="custom_need_restore_'.$goods['goods_id'].'" name="customer_need_restore"  type="text" id="end_date" size="15" value="'.$goods['need_restore'].'" class="form-control custom_need_restore" >';

            if (empty($goods['sold_goods_total'])) {
                $goods['sold_goods_total'] = 0;
            }

            $goods['restock_type'] = '-';

        }

        usort($res, "cmp");

        $arr = array(
            'data' => $res,
            'record_count' => sizeof($res)
        );

        return $arr;
    }

    function checkGoodsExist($goods_sn)
    {
        global $db, $ecs, $userController ,$_CFG;

        $sql = "select count(*) from ".$ecs->table("goods")." where goods_sn = '".$goods_sn."' ";
        return $count = $db->getOne($sql);
    }

    function cmp($a, $b)
    {
        return strcmp($a["need_restore"], $b["need_restore"]);
    }

    function transferListCreate() 
    {
        //echo '<pre>';
        //print_r($_REQUEST);
        $warehouseTransferController = $this->warehouseTransferController;
        // create transfer list
        $warehouse_from = $this->erpController->getMainWarehouseId();
        $warehouse_to = $_REQUEST['shop_id'];
        $transfer_type = $warehouseTransferController::REASON_TYPE_SHOP_RESTOCK;
        $transfer_goods = $_REQUEST['restock_order_data'];
        $operator = $_REQUEST['operator'];

        $transfer_id = $warehouseTransferController->warehouseTransferCreateByGoodsTransfer($warehouse_from, $warehouse_to, $transfer_goods, $transfer_type, $operator, 0);
        return $transfer_id;
    }

    function getOperatorOptions($please_select = true)
    {
        global $db, $ecs, $userController ,$_CFG;
        $sql = "SELECT user_id as value,user_name as name FROM ".$ecs->table('admin_user') . ' where is_disable = 0 and role_id in (3,2,14)';
        $stocktake_person = $db->getAll($sql);
        if ($please_select) {
            array_unshift($stocktake_person, array('name'=>'請選擇...','value'=>''));
        }

        return $stocktake_person;
    }

    public function getShopPickupOrderGoodsTransferHistoryList($is_pagination=true)
    {
        global $db, $ecs, $userController ,$_CFG;

        $sql = "select count(*) from ".$ecs->table('order_goods_transfer_reserved')." ogtr 
                left join ".$ecs->table('erp_stock_transfer')." est on ( est.transfer_id = ogtr.transfer_id ) 
                left join ".$ecs->table('order_info')." oi on ( ogtr.order_id = oi.order_id ) 
                LEFT JOIN " . $ecs->table('goods') . " as g ON ogtr.goods_id = g.goods_id 
                 where 1 and ogtr.type = 0  and est.reason_type = 2 ";

        if (isset($_REQUEST['order_sn'])  && $_REQUEST['order_sn'] != '') {
            $sql.=" and oi.order_sn='".$_REQUEST['order_sn']."'";
        }

        if (isset($_REQUEST['goods_sn'])  && $_REQUEST['goods_sn'] != '') {
            $sql.=" and g.goods_sn='".$_REQUEST['goods_sn']."'";
        }

        $total_count = $db->getOne($sql);

        $sql = "select ogtr.*,est.transfer_sn,est.create_time,oi.*,g.goods_thumb,g.goods_name,g.goods_sn from ".$ecs->table('order_goods_transfer_reserved')." ogtr 
                left join ".$ecs->table('erp_stock_transfer')." est on ( est.transfer_id = ogtr.transfer_id ) 
                left join ".$ecs->table('order_info')." oi on ( ogtr.order_id = oi.order_id ) 
                LEFT JOIN " . $ecs->table('goods') . " as g ON ogtr.goods_id = g.goods_id 
                where 1 and ogtr.type = 0 and est.reason_type = 2 ";

         if (isset($_REQUEST['order_sn'])  && $_REQUEST['order_sn'] != '') {
             $sql.=" and oi.order_sn='".$_REQUEST['order_sn']."'";
         }


        if (isset($_REQUEST['goods_sn'])  && $_REQUEST['goods_sn'] != '') {
            $sql.=" and g.goods_sn='".$_REQUEST['goods_sn']."'";
        }

        $sql .= ' order by ogtr.transfer_id desc';

        $sql .= ($is_pagination ? " LIMIT " . $_REQUEST['start'] . ", " . $_REQUEST['page_size'] : '');
        $res = $db->getAll($sql);
        //echo $sql;exit;
        foreach ($res as &$order_goods_transfer) {
            $order_goods_transfer['goods_thumb'] = '<img width="50" src="../'.$order_goods_transfer['goods_thumb'].'"/>';
            $order_goods_transfer['transfer_id_html'] = '<a target="_blank" class="collapse-link" href="erp_warehouse_transfer_finished.php?act=list&amp;transfer_id='.$order_goods_transfer['transfer_id'].'">'.$order_goods_transfer['transfer_sn'].'</a>';
            $order_goods_transfer['order_sn_html'] = '<a target="_blank" class="collapse-link" href="order.php?act=info&amp;order_id='.$order_goods_transfer['order_id'].'">'.$order_goods_transfer['order_sn'].'</a>';
            if ($order_goods_transfer['status'] == 0) {
                $order_goods_transfer['status_html'] = '待調貨';
            } else if ($order_goods_transfer['status'] == 1) {
                $order_goods_transfer['status_html'] = '調貨中';
            } else if ($order_goods_transfer['status'] == 2) {
                $order_goods_transfer['status_html'] = '已調貨';
            }

            $order_goods_transfer['order_date_html'] = local_date($_CFG['time_format'], $order_goods_transfer['create_time']);
            
        }

        $arr = array(
           'data' => $res,
           'record_count' => $total_count
        );

        return $arr;
    }

    public function getShopToWarehouseOrderGoodsTransferHistoryList($is_pagination=true)
    {
        global $db, $ecs, $userController ,$_CFG;
    
        $reason_type_options = $this->warehouseTransferController->getReasonTypeOptions();
        
        if (isset($_REQUEST['reason_type']) && $_REQUEST['reason_type'] != '-1' && $_REQUEST['reason_type'] != '' ) {
            $where_sql = " and est.reason_type = ".$_REQUEST['reason_type']." ";
        } else {
            $where_sql = " and (est.reason_type = 5 or est.reason_type = 2 )";
        } 

        $sql = "select count(*) from ".$ecs->table('order_goods_transfer_reserved')." ogtr 
                left join ".$ecs->table('erp_stock_transfer')." est on ( est.transfer_id = ogtr.transfer_id ) 
                left join ".$ecs->table('order_info')." oi on ( ogtr.order_id = oi.order_id ) 
                LEFT JOIN " . $ecs->table('goods') . " as g ON ogtr.goods_id = g.goods_id 
                where 1 and ogtr.type = 0 ".$where_sql." ";

        if (isset($_REQUEST['order_sn'])  && $_REQUEST['order_sn'] != '') {
            $sql.=" and oi.order_sn='".$_REQUEST['order_sn']."'";
        }

        if (isset($_REQUEST['goods_sn'])  && $_REQUEST['goods_sn'] != '') {
            $sql.=" and g.goods_sn='".$_REQUEST['goods_sn']."'";
        }

        $total_count = $db->getOne($sql);

        $sql = "select ogtr.*,est.reason_type,est.transfer_sn, est.create_time, oi.*,g.goods_thumb,g.goods_name,g.goods_sn from ".$ecs->table('order_goods_transfer_reserved')." ogtr 
                left join ".$ecs->table('erp_stock_transfer')." est on ( est.transfer_id = ogtr.transfer_id ) 
                left join ".$ecs->table('order_info')." oi on ( ogtr.order_id = oi.order_id ) 
                LEFT JOIN " . $ecs->table('goods') . " as g ON ogtr.goods_id = g.goods_id 
                where 1 and ogtr.type = 0 ".$where_sql." ";

        if (isset($_REQUEST['order_sn'])  && $_REQUEST['order_sn'] != '') {
            $sql.=" and oi.order_sn='".$_REQUEST['order_sn']."'";
        }

        if (isset($_REQUEST['goods_sn'])  && $_REQUEST['goods_sn'] != '') {
            $sql.=" and g.goods_sn='".$_REQUEST['goods_sn']."'";
        }

        $sql .= ' order by ogtr.transfer_id desc';

        $sql .= ($is_pagination ? " LIMIT " . $_REQUEST['start'] . ", " . $_REQUEST['page_size'] : '');
        $res = $db->getAll($sql);
        //echo $sql;exit;
        $sellable_warehouse_info = $this->erpController->getSellableWarehouseInfo();
     
        $warehouse_names = [];
        foreach ($sellable_warehouse_info as $warehouse_info) {
            $warehouse_names[$warehouse_info['warehouse_id']] = $warehouse_info['name'];
        }

        foreach ($res as &$order_goods_transfer) {
            $order_goods_transfer['goods_thumb'] = '<img width="50" src="../'.$order_goods_transfer['goods_thumb'].'"/>';
            $order_goods_transfer['transfer_id_html'] = '<a target="_blank" class="collapse-link" href="erp_warehouse_transfer_finished.php?act=list&amp;transfer_id='.$order_goods_transfer['transfer_id'].'">'.$order_goods_transfer['transfer_sn'].'</a>';
            $order_goods_transfer['order_sn_html'] = '<a target="_blank" class="collapse-link" href="order.php?act=info&amp;order_id='.$order_goods_transfer['order_id'].'">'.$order_goods_transfer['order_sn'].'</a>';
            if ($order_goods_transfer['status'] == 0) {
                $order_goods_transfer['status_html'] = '待調貨';
            } else if ($order_goods_transfer['status'] == 1) {
                $order_goods_transfer['status_html'] = '調貨中';
            } else if ($order_goods_transfer['status'] == 2) {
                $order_goods_transfer['status_html'] = '已調貨';
            }
            $order_goods_transfer['warehouse_from_name'] = $warehouse_names[$order_goods_transfer['warehouse_from']];
            $order_goods_transfer['warehouse_from_to'] = $warehouse_names[$order_goods_transfer['warehouse_to']];
            $order_goods_transfer['order_date_html'] = local_date($_CFG['time_format'], $order_goods_transfer['create_time']);

            $order_goods_transfer['reason_type_name'] = $reason_type_options[$order_goods_transfer['reason_type']];
        }


        $arr = array(
           'data' => $res,
           'record_count' => $total_count
        );

        return $arr;
    }


    public function getShopToWarehouseOrderGoodsTransferList($is_pagination=false)
    {
        global $db, $ecs, $userController ,$_CFG;
        require_once(ROOT_PATH . 'includes/lib_order.php');
        ini_set('memory_limit', '800M');
        $main_warehouse_id = $this->erpController->getMainWarehouseId();
        $shop_warehouse_id = $this->erpController->getShopWarehouseId();

        // get transfered order id
        $orderController = $this->orderController;
        $sql = "select distinct order_id from ".$ecs->table('order_goods_transfer_reserved')." where type = ".$orderController::ORDER_GOODS_RESERVE_TYPE_TRANSFER." and status != ".$orderController::ORDER_GOODS_RESERVED_STATUS_FINISHED." ";
        $orderTransfered = $db->getCol($sql);


        // 按order pay time 
        $sql = "SELECT eo.client_order_sn, o.order_id, o.order_sn, o.add_time, o.order_status, o.shipping_id, o.shipping_status, o.order_amount, o.money_paid, o.cp_price, s.shipping_name, " .
                 "o.pay_status, o.order_type, o.type_sn, o.type_id, o.actg_paid_amount,o.pay_id, o.extension_code, o.extension_id, o.postscript, o.serial_numbers,  " .
                 "o.consignee, o.address, o.country, o.email, o.tel, o.pay_time, s.shipping_name, o.shipping_fod, o.language, p.pay_name, " .
                 "(" . order_amount_field('o.') . ") AS total_fee, o.to_buyer, " .
                 "(o.money_paid + o.order_amount) AS order_total, " .
                 "IFNULL(u.user_name, '" .$GLOBALS['_LANG']['anonymous']. "') AS buyer, o.user_id, o.salesperson, o.platform, ".
                 "o.fully_paid, o.actg_paid_amount, ". // Accounting System for wholesale orders
                 "IFNULL((SELECT count(*) FROM ".$GLOBALS['ecs']->table('order_action')." as oa WHERE oa.order_id = o.order_id), 0) as action_count " . // Action count

             " FROM " . $GLOBALS['ecs']->table('order_info') . " AS o " .
             " LEFT JOIN " .$GLOBALS['ecs']->table('erp_order'). " AS eo ON eo.client_order_sn=o.order_sn ".
             " LEFT JOIN " .$GLOBALS['ecs']->table('payment'). " AS p ON p.pay_id=o.pay_id ".
             " LEFT JOIN " .$GLOBALS['ecs']->table('shipping'). " AS s ON s.shipping_id=o.shipping_id ".
             " LEFT JOIN " .$GLOBALS['ecs']->table('users'). " AS u ON u.user_id=o.user_id ". 
             " where 1  ". order_query_sql('await_ship', 'o.') .
             " ORDER BY pay_time asc ";

        $row = $GLOBALS['db']->getAll($sql);

        $order_ids = array_map(function ($order) { return $order['order_id']; }, $row);
        
        $order_goods_ids = [];
        // get order goods ids
        $sql = "SELECT distinct og.goods_id " .
               "FROM " . $GLOBALS['ecs']->table('order_goods') . " as og " .
               "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON og.goods_id = g.goods_id " .
               "WHERE order_id " . db_create_in($order_ids);
        $order_goods_ids = $GLOBALS['db']->getCol($sql);

        // for main warehouse goods qty
        $goods_warehouse_qty = $this->erpController->getSellableProductsQtyByWarehouses($order_goods_ids,null,false);

        $goods_warehouse_qty_sellable = $this->erpController->getSellableProductsQtyByWarehouses($order_goods_ids,null,true);
        
        // $shop_warehouse_qty = $this->erpController->getSellableProductsQtyByWarehouses($order_goods_ids,$shop_warehouse_id,true);

        $fls_warehouse_id = $this->erpController->getFlsWarehouseId();

        $wilson_warehouse_id = $this->erpController->getWilsonWarehouseId();

        //echo '<pre>';
     
        // echo $main_warehouse_id;
        $main_goods_qty = [];
        foreach ($goods_warehouse_qty as $goods_id_key => $warehouse_qty) {
            $main_goods_qty[$goods_id_key] = $warehouse_qty[$main_warehouse_id];
        }

        $goods_qty_fls = [];
        foreach ($goods_warehouse_qty as $goods_id_key => $warehouse_qty) {
            $goods_qty_fls[$goods_id_key] = $warehouse_qty[$fls_warehouse_id];
        }

        $shop_warehouse_qty = [];
        foreach ($goods_warehouse_qty_sellable as $goods_id_key => $warehouse_qty) {
            $shop_warehouse_qty[$goods_id_key] = $warehouse_qty[$shop_warehouse_id];
        }


        $support_warehouse_qty = [];
        foreach ($goods_warehouse_qty as $goods_id_key => $warehouse_qty) {
            $support_warehouse_qty[$goods_id_key] = $main_goods_qty[$goods_id_key] + $warehouse_qty[$wilson_warehouse_id];
        }
       

        
        $sql = "SELECT og.*, g.goods_thumb, g.goods_height, g.goods_width, g.goods_length " .
           "FROM " . $GLOBALS['ecs']->table('order_goods') . " as og " .
           "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON og.goods_id = g.goods_id " .
           "WHERE order_id " . db_create_in($order_ids);

        $order_goods = $GLOBALS['db']->getAll($sql);

        $order_goods_by_order_id = [];
        foreach ($order_goods as $order_goods_key =>  $order_goods_info){
           // add storage to order_goods ar
           $order_goods[$order_goods_key]['storage'] = $goods_qty[$order_goods_info['goods_id']];
           $order_goods_by_order_id[$order_goods_info['order_id']][] = $order_goods[$order_goods_key];
        }

        $orders_can_ship = [];
        $orders_cant_ship = [];
        $orders_can_ship_fls = [];
        
        $row_temp = $row;
        $transfer_ar = [];
        //print_r($row);

        // check FLS Stock 
        foreach ($row_temp AS $key => $value)
        {
            if (isset($order_goods_by_order_id[$value['order_id']]))
            {
                $all_order_goods_in_stock = true;
                // FLS new flow
                // in 待出貨(本地速遞) HK
                if (in_array($value['shipping_id'],[3]) and $value['country'] == 3409) {
                    foreach ($order_goods_by_order_id[$value['order_id']] as $ogs)
                    {
                        if ( ($goods_qty_fls[$ogs['goods_id']] - $ogs['goods_number'] + $ogs['send_number']) >= 0) {
                            $goods_qty_fls[$ogs['goods_id']] = $goods_qty_fls[$ogs['goods_id']] -$ogs['goods_number'] + $ogs['send_number'];
                        } else {
                            $goods_qty_fls[$ogs['goods_id']] = 0;
                            $all_order_goods_in_stock = false;
                        }
                    }

                    // all in stock
                    if ($all_order_goods_in_stock == true) {
                      $orders_can_ship_fls[] = $value['order_id']; 
                    }
                }
            }
        }

        foreach ($row_temp AS $key => $value)
        {
            // skip directly, because it will be ship by FLS warehouse
            if (in_array($value['order_id'],$orders_can_ship_fls)) {
                continue;
            }

            $debug =0;
            // filter the order can't be shipped
            if (isset($order_goods_by_order_id[$value['order_id']]))
            {
                $all_order_goods_in_stock = true;
                //if (in_array($_REQUEST['composite_status'],[CS_AWAIT_SHIP_LOCKER,CS_AWAIT_PICK_HK,CS_AWAIT_SHIP_NON_HK]))
                //{
                    // goods qty without fls
                    foreach ($order_goods_by_order_id[$value['order_id']] as $ogs)
                    {
                        // order detail
                        $ogs['shipping_name'] = $value['shipping_name'];

                        $diff_qty = $support_warehouse_qty[$ogs['goods_id']] - $ogs['goods_number'] + $ogs['send_number'];

                        if ($ogs['goods_id'] == '738' && $debug == 1) {
                            
                            echo 'order_id:'.$value['order_id'].'<br>';
                            echo 'diff:'.$diff_qty.'<br>';
                            echo '<br>';
                        }

                        if ($diff_qty >= 0) {   
                            $support_warehouse_qty[$ogs['goods_id']] = $diff_qty;
                        } else {
                            //if store pick up, count shop sellable stock , if stock enough, then transfer
                            if (!in_array($value['shipping_id'],[2,5])) {

                                $diff_stock = $support_warehouse_qty[$ogs['goods_id']] - $ogs['goods_number'] + $ogs['send_number'] + $shop_warehouse_qty[$ogs['goods_id']];
                                
                                 if ($ogs['goods_id'] == '738'  && $debug == 1) {
                                     echo 'not enough stock'.'<br>';
                                     echo 'diff_stock:'.$diff_stock.'<br>';
                                     echo '<br>';
                                     echo  'main:'.$support_warehouse_qty[$ogs['goods_id']];
                                     echo '<br>';
                                     echo 'shop:'.$shop_warehouse_qty[$ogs['goods_id']];
                                     echo '<br>';
                                 }

                                if ($diff_stock >= 0){
                                    // bulid transfer array
                                    $shop_warehouse_qty[$ogs['goods_id']] = $diff_stock;
                                    if ($shop_warehouse_qty[$ogs['goods_id']] >= 0) {
                                        if ($ogs['goods_id'] == '738'  && $debug == 1) {
                                            echo 'insert'.'<br>';
                                        }
                                        $ogs['order_sn'] = $value['order_sn'];
                                        $ogs['transfer_qty'] = ($support_warehouse_qty[$ogs['goods_id']] - $ogs['goods_number'] + $ogs['send_number']) * (-1) ;
                                        $transfer_ar[] = $ogs;
                                        //   $transfer_ar[] = array($value['order_id'],$ogs['goods_id'],$main_goods_qty[$ogs['goods_id']],$main_warehouse_id,$shop_warehouse_id,0,0);
                                    }
                                } else {
                                    $all_order_goods_in_stock = false;
                                }
                            } else {
                                $all_order_goods_in_stock = false;
                            }

                            $main_goods_qty[$ogs['goods_id']] = 0;                            
                        }
                    }
                // } else {
                //     // goods qty global
                //     foreach ($order_goods_by_order_id[$value['order_id']] as $ogs)
                //     {
                //         if ($goods_qty[$ogs['goods_id']] - $ogs['goods_number'] + $ogs['send_number'] >= 0) {
                //             $goods_qty[$ogs['goods_id']] = $goods_qty[$ogs['goods_id']] -$ogs['goods_number'] + $ogs['send_number'];
                //         } else {
                //             $goods_qty[$ogs['goods_id']] = 0;
                //             $all_order_goods_in_stock = false;
                //         }
                //     }
                // }

                if ($all_order_goods_in_stock == true) {
                    $orders_can_ship[] = $value['order_id']; 
                } else {
                    $orders_cant_ship[] = $value['order_id']; 
                }
            }
        }

        // print_R($orders_can_ship);
        // print_R($orders_cant_ship);
        // print_R($orders_can_ship_fls);

        $transfer_orders = [];
        foreach ($row AS $key => $value)
        {
            if (in_array($value['order_id'],$orders_can_ship)) {
                if (!in_array($value['shipping_id'],[2,5])) {            
                    $transfer_orders[] = $value['order_id'];
                }
            }
        }
       
        $transfer_order_goods = [];
        foreach ($transfer_ar AS $key => &$transfer_info)
        {
            $transfer_info['goods_thumb'] = '<img width="50" src="../'.$transfer_info['goods_thumb'].'"/>';
            $hidden_field = '<input type="hidden" value="'.$transfer_info['order_id'].'" name="order_id[]" />';
            $hidden_field .= '<input type="hidden" value="'.$transfer_info['goods_id'].'" name="goods_id[]" />';
            $transfer_info['transfer_qty_html'] = $hidden_field.'<input name="transfer_qty[]" type="text" size="5" value="'.$transfer_info['transfer_qty'].'" class="form-control" />';

            $transfer_info['order_sn_html']  = '<a target="_blank" href="order.php?act=info&order_id='.$transfer_info['order_id'].'">'.$transfer_info['order_sn'].'</a>';
            $transfer_info['order_id_html'] = ''.$transfer_info['order_id'];

            // echo '<pre>';
            // print_r($transfer_info);
            // exit;

            if (in_array($transfer_info['order_id'],$transfer_orders) && !in_array($transfer_info['order_id'],$orderTransfered)  ) {
                $transfer_order_goods[] = $transfer_info;
            }
        }
        // echo '<pre>';
        // print_r($transfer_ar);
        // exit;
        $arr = array(
            'data' => $transfer_order_goods,
            'record_count' => sizeof($transfer_order_goods)
        );

        return $arr;

    }

    public function getWarehouse2ShopOrderGoodsTransferList($is_pagination=false,$transfer_warehouse_id,$shop_warehouse_id) // 西九->門市
    {
        global $db, $ecs, $userController ,$_CFG;
        require_once(ROOT_PATH . 'includes/lib_order.php');
        ini_set('memory_limit', '800M');
        //$transfer_warehouse_id = $this->erpController->getMainWarehouseId();
        //$shop_warehouse_id = $this->erpController->getShopWarehouseId();

        // get transfered order id
        $orderController = $this->orderController;
        $sql = "select distinct order_id from ".$ecs->table('order_goods_transfer_reserved')." where type = ".$orderController::ORDER_GOODS_RESERVE_TYPE_TRANSFER." and status != ".$orderController::ORDER_GOODS_RESERVED_STATUS_FINISHED . "";
        $orderTransfered = $db->getCol($sql);


        // 按order pay time 
        $sql = "SELECT eo.client_order_sn, o.order_id, o.order_sn, o.add_time, o.order_status, o.shipping_id, o.shipping_status, o.order_amount, o.money_paid, o.cp_price, s.shipping_name, " .
                 "o.pay_status, o.order_type, o.type_sn, o.type_id, o.actg_paid_amount,o.pay_id, o.extension_code, o.extension_id, o.postscript, o.serial_numbers,  " .
                 "o.consignee, o.address, o.country, o.email, o.tel, o.pay_time, s.shipping_name, o.shipping_fod, o.language, p.pay_name, " .
                 "(" . order_amount_field('o.') . ") AS total_fee, o.to_buyer, " .
                 "(o.money_paid + o.order_amount) AS order_total, " .
                 "IFNULL(u.user_name, '" .$GLOBALS['_LANG']['anonymous']. "') AS buyer, o.user_id, o.salesperson, o.platform, ".
                 "o.fully_paid, o.actg_paid_amount, ". // Accounting System for wholesale orders
                 "IFNULL((SELECT count(*) FROM ".$GLOBALS['ecs']->table('order_action')." as oa WHERE oa.order_id = o.order_id), 0) as action_count " . // Action count

             " FROM " . $GLOBALS['ecs']->table('order_info') . " AS o " .
             " LEFT JOIN " .$GLOBALS['ecs']->table('erp_order'). " AS eo ON eo.client_order_sn=o.order_sn ".
             " LEFT JOIN " .$GLOBALS['ecs']->table('payment'). " AS p ON p.pay_id=o.pay_id ".
             " LEFT JOIN " .$GLOBALS['ecs']->table('shipping'). " AS s ON s.shipping_id=o.shipping_id ".
             " LEFT JOIN " .$GLOBALS['ecs']->table('users'). " AS u ON u.user_id=o.user_id ". 
             " where 1  ". order_query_sql('await_ship', 'o.') .
             " ORDER BY pay_time asc ";

        //echo $sql;exit;
        $row = $GLOBALS['db']->getAll($sql);

        $order_ids = array_map(function ($order) { return $order['order_id']; }, $row);
        
        $order_goods_ids = [];
        // get order goods ids
        $sql = "SELECT distinct og.goods_id " .
               "FROM " . $GLOBALS['ecs']->table('order_goods') . " as og " .
               "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON og.goods_id = g.goods_id " .
               "WHERE order_id " . db_create_in($order_ids);
        $order_goods_ids = $GLOBALS['db']->getCol($sql);

        // for transfer warehouse goods qty
        $goods_warehouse_qty = $this->erpController->getSellableProductsQtyByWarehouses($order_goods_ids,null,false);
        $shop_warehouse_qty = $this->erpController->getSellableProductsQtyByWarehouses($order_goods_ids,$shop_warehouse_id,true);

        $fls_warehouse_id = $this->erpController->getFlsWarehouseId();
        $wilson_warehouse_id = $this->erpController->getWilsonWarehouseId();
        $main_warehouse_id = $this->erpController->getMainWarehouseId();
        //echo '<pre>';
     
        // echo $main_warehouse_id;
        $transfer_warehouse_qty = [];
        foreach ($goods_warehouse_qty as $goods_id_key => $warehouse_qty) {
            $transfer_warehouse_qty[$goods_id_key] = $warehouse_qty[$transfer_warehouse_id];
        }

        $fls_warehouse_qty = [];
        foreach ($goods_warehouse_qty as $goods_id_key => $warehouse_qty) {
            $fls_warehouse_qty[$goods_id_key] = $warehouse_qty[$fls_warehouse_id];
        }

        $shop_warehouse_qty = [];
        foreach ($goods_warehouse_qty as $goods_id_key => $warehouse_qty) {
            $shop_warehouse_qty[$goods_id_key] = $warehouse_qty[$shop_warehouse_id];
        }
        
        $support_warehouse_qty = [];
        if ($transfer_warehouse_id == $main_warehouse_id) {
            foreach ($goods_warehouse_qty as $goods_id_key => $warehouse_qty) {
                $support_warehouse_qty[$goods_id_key] = $shop_warehouse_qty[$goods_id_key] + $warehouse_qty[$wilson_warehouse_id];
            }
        } else if ($transfer_warehouse_id == $wilson_warehouse_id) {
            foreach ($goods_warehouse_qty as $goods_id_key => $warehouse_qty) {
                $support_warehouse_qty[$goods_id_key] = $shop_warehouse_qty[$goods_id_key] + $warehouse_qty[$main_warehouse_id];
            }
        } else {
            foreach ($goods_warehouse_qty as $goods_id_key => $warehouse_qty) {
                $support_warehouse_qty[$goods_id_key] = 0;
            }
        }
        
        //print_r($main_goods_qty);
        //print_r($goods_qty_fls);
        //print_r($shop_warehouse_qty);
        //$goods_qty_fls

        // $sql = "SELECT ega.goods_id, IFNULL(SUM(ega.goods_qty),0) as storage " .
        //        "FROM " . $GLOBALS['ecs']->table('erp_goods_attr_stock') . " AS ega " .
        //        "WHERE ega.goods_id " . db_create_in($order_goods_ids) ." AND ega.warehouse_id IN (".$sellableWarehouseIds.") ".
        //        "group by goods_id";
        // 
        // $order_goods = $GLOBALS['db']->getAll($sql);
        // 
        // $goods_qty = [];
        // foreach ($order_goods as $order_goods_info){
        //    $goods_qty[$order_goods_info['goods_id']] = $order_goods_info['storage'];
        // }
        // 
        // foreach ($order_goods_ids as $order_goods_id) {
        //    if (!isset($goods_qty[$order_goods_id])) {
        //        $goods_qty[$order_goods_id] = 0;
        //    } 
        // }

        $sql = "SELECT og.*, g.goods_thumb, g.goods_height, g.goods_width, g.goods_length " .
           "FROM " . $GLOBALS['ecs']->table('order_goods') . " as og " .
           "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON og.goods_id = g.goods_id " .
           "WHERE order_id " . db_create_in($order_ids);

        $order_goods = $GLOBALS['db']->getAll($sql);

        $order_goods_by_order_id = [];
        foreach ($order_goods as $order_goods_key =>  $order_goods_info){
           // add storage to order_goods ar
           $order_goods[$order_goods_key]['storage'] = $goods_qty[$order_goods_info['goods_id']];
           $order_goods_by_order_id[$order_goods_info['order_id']][] = $order_goods[$order_goods_key];
        }

        // for FLS warehouse
        // $sql = "SELECT og.*, g.goods_thumb " .
        //         (1 ? ", (SELECT IFNULL(SUM(ega.goods_qty),0) FROM " . $GLOBALS['ecs']->table('erp_goods_attr_stock') . " AS ega WHERE g.goods_id = ega.goods_id AND ega.warehouse_id IN (".$sellableWarehouseIdsFls.")) AS storage " : '') .
        //         "FROM " . $GLOBALS['ecs']->table('order_goods') . " as og " .
        //         "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON og.goods_id = g.goods_id " .
        //         "WHERE order_id " . db_create_in($order_ids);
        // $order_goods_fls = $GLOBALS['db']->getAll($sql);
        // $sql = "SELECT ega.goods_id, IFNULL(SUM(ega.goods_qty),0) as storage " .
        //        "FROM " . $GLOBALS['ecs']->table('erp_goods_attr_stock') . " AS ega " .
        //        "WHERE ega.goods_id " . db_create_in($order_goods_ids) ." AND ega.warehouse_id IN (".$sellableWarehouseIdsFls.") ".
        //        "group by goods_id";
        // 
        // $order_goods_fls = $GLOBALS['db']->getAll($sql);
        // 
        // $goods_qty_fls = [];
        // foreach ($order_goods_fls as $order_goods_info){
        //    $goods_qty_fls[$order_goods_info['goods_id']] = $order_goods_info['storage'];
        // }
        // 
        // foreach ($order_goods_ids_fls as $order_goods_id_fls) {
        //    if (!isset($goods_qty_fls[$order_goods_id_fls])) {
        //        $goods_qty_fls[$order_goods_id_fls] = 0;
        //    } 
        // }
        // 
        // // for main warehouse without fls
        // foreach ($goods_qty as $goods_key => $qty){
        //    $main_goods_qty[$goods_key] = $qty - $goods_qty_fls[$goods_key];
        // }

        $orders_can_ship = [];
        $orders_cant_ship = [];
        $orders_can_ship_fls = [];
        
        $row_temp = $row;
        $transfer_ar = [];
        //print_r($row);

        // check FLS Stock // 
        if ($transfer_warehouse_id == $this->erpController->getMainWarehouseId()) {
            foreach ($row_temp AS $key => $value)
            {
                if (isset($order_goods_by_order_id[$value['order_id']]))
                {
                    $all_order_goods_in_stock = true;
                    // FLS new flow
                    // in 待出貨(本地速遞) HK
                    if (in_array($value['shipping_id'],[3]) and $value['country'] == 3409) {
                        foreach ($order_goods_by_order_id[$value['order_id']] as $ogs)
                        {
                            if ( ($fls_warehouse_qty[$ogs['goods_id']] - $ogs['goods_number'] + $ogs['send_number']) >= 0) {
                                $fls_warehouse_qty[$ogs['goods_id']] = $fls_warehouse_qty[$ogs['goods_id']] -$ogs['goods_number'] + $ogs['send_number'];
                            } else {
                                $fls_warehouse_qty[$ogs['goods_id']] = 0;
                                $all_order_goods_in_stock = false;
                            }
                        }
    
                        // all in stock
                        if ($all_order_goods_in_stock == true) {
                          $orders_can_ship_fls[] = $value['order_id']; 
                        }
                    }
                }
            }
        }

        foreach ($row_temp AS $key => $value)
        {
            // if (in_array($_REQUEST['composite_status'],[CS_AWAIT_SHIP_HK,CS_AWAIT_SHIP_HK_YOHO]))
            // {
            //     // skip directly, because it will be ship by FLS warehouse
                if (in_array($value['order_id'],$orders_can_ship_fls)) {
                    continue;
                }
            // }
            $debug = 0;
            // filter the order can't be shipped
            if (isset($order_goods_by_order_id[$value['order_id']]))
            {
                $all_order_goods_in_stock = true;
                //if (in_array($_REQUEST['composite_status'],[CS_AWAIT_SHIP_LOCKER,CS_AWAIT_PICK_HK,CS_AWAIT_SHIP_NON_HK]))
                //{
                    // goods qty without fls
                    foreach ($order_goods_by_order_id[$value['order_id']] as $ogs)
                    {
                        // order detail
                        $ogs['shipping_name'] = $value['shipping_name'];

                        $diff_qty = $transfer_warehouse_qty[$ogs['goods_id']] - $ogs['goods_number'] + $ogs['send_number'];

                        if ($ogs['goods_id'] == '2591' && $debug == 1) {
                            echo 'order_id:'.$value['order_id'].'<br>';
                            echo 'diff:'.$diff_qty.'<br>';
                            echo '<br>';
                        }

                        if ($diff_qty >= 0) {
                            
                            $transfer_warehouse_qty[$ogs['goods_id']] = $diff_qty;

                            // bulid transfer array
                              
                            $transfer_qty =  $ogs['goods_number'] - $ogs['send_number'];
                               if ($ogs['goods_id'] == '2591'  && $debug == 1) {
                                   echo 'yes'.'<br>';
                                   echo 'transfer:'.$transfer_qty.'<br>';
                                   echo '<br>';
                               }
                            if ($transfer_qty > 0) {
                                if ($ogs['goods_id'] == '2591'  && $debug == 1) {
                                    echo 'insert'.'<br>';
                                }
                                $ogs['transfer_qty'] = $transfer_qty;
                                $ogs['order_sn'] = $value['order_sn'];
                                $transfer_ar[] = $ogs;
                            }
                        } else {
                            //if store pick up, count shop sellable stock , if stock enough, then transfer
                            if (in_array($value['shipping_id'],[2,5])) {
                                //$diff_stock = $transfer_warehouse_qty[$ogs['goods_id']] - $ogs['goods_number'] + $ogs['send_number'] + $shop_warehouse_qty[$ogs['goods_id']];
                                $diff_stock = $transfer_warehouse_qty[$ogs['goods_id']] - $ogs['goods_number'] + $ogs['send_number'] + $support_warehouse_qty[$ogs['goods_id']];
                                
                                if ($ogs['goods_id'] == '2591'  && $debug == 1) {
                                    echo 'no'.'<br>';
                                    echo 'diff_stock:'.$diff_stock.'<br>';
                                    echo '<br>';
                                    echo  'main:'.$transfer_warehouse_qty[$ogs['goods_id']];
                                }

                                if ($diff_stock >= 0){
                                    $support_warehouse_qty[$ogs['goods_id']] = $diff_stock;
                                    if ($transfer_warehouse_qty[$ogs['goods_id']] > 0) {
                                        if ($ogs['goods_id'] == '2591'  && $debug == 1) {
                                            echo 'insert'.'<br>';
                                        }
                                        $ogs['order_sn'] = $value['order_sn'];
                                        $ogs['transfer_qty'] = $transfer_warehouse_qty[$ogs['goods_id']];
                                        $transfer_ar[] = $ogs;
                                    }
                                } else {
                                    $all_order_goods_in_stock = false;
                                }
                            } else {
                                $all_order_goods_in_stock = false;
                            }

                            $transfer_warehouse_qty[$ogs['goods_id']] = 0;                            
                        }
                    }
                // } else {
                //     // goods qty global
                //     foreach ($order_goods_by_order_id[$value['order_id']] as $ogs)
                //     {
                //         if ($goods_qty[$ogs['goods_id']] - $ogs['goods_number'] + $ogs['send_number'] >= 0) {
                //             $goods_qty[$ogs['goods_id']] = $goods_qty[$ogs['goods_id']] -$ogs['goods_number'] + $ogs['send_number'];
                //         } else {
                //             $goods_qty[$ogs['goods_id']] = 0;
                //             $all_order_goods_in_stock = false;
                //         }
                //     }
                // }

                if ($all_order_goods_in_stock == true) {
                    $orders_can_ship[] = $value['order_id']; 
                } else {
                    $orders_cant_ship[] = $value['order_id']; 
                }
            }
        }

//         print_R($orders_can_ship);
//         print_R($orders_cant_ship);
//         print_R($orders_can_ship_fls);
// exit;
        $transfer_orders = [];
        foreach ($row AS $key => $value)
        {
            if (in_array($value['order_id'],$orders_can_ship)) {
                if (in_array($value['shipping_id'],[2,5])) {            
                    $transfer_orders[] = $value['order_id'];
                }
            }
        }
       
        $transfer_order_goods = [];
        foreach ($transfer_ar AS $key => &$transfer_info)
        {
            $transfer_info['goods_thumb'] = '<img width="50" src="../'.$transfer_info['goods_thumb'].'"/>';
            $hidden_field = '<input type="hidden" value="'.$transfer_info['order_id'].'" name="order_id[]" />';
            $hidden_field .= '<input type="hidden" value="'.$transfer_info['goods_id'].'" name="goods_id[]" />';
            $transfer_info['transfer_qty_html'] = $hidden_field.'<input name="transfer_qty[]" type="text" size="5" value="'.$transfer_info['transfer_qty'].'" class="form-control" />';

            $transfer_info['order_sn_html']  = '<a target="_blank" href="order.php?act=info&order_id='.$transfer_info['order_id'].'">'.$transfer_info['order_sn'].'</a>';
            $transfer_info['order_id_html'] = ''.$transfer_info['order_id'];

            // echo '<pre>';
            // print_r($transfer_info);
            // exit;

            if (in_array($transfer_info['order_id'],$transfer_orders) && !in_array($transfer_info['order_id'],$orderTransfered)  ) {
                $transfer_order_goods[] = $transfer_info;
            }
        }
        //echo '<pre>';
        //print_r($transfer_ar);
        //exit;
        $arr = array(
            'data' => $transfer_order_goods,
            'record_count' => sizeof($transfer_order_goods)
        );

        return $arr;
    }

    public function getShopPickupOrderGoodsTransferList($is_pagination=false)
    {
        global $db, $ecs, $userController ,$_CFG;
        require_once(ROOT_PATH . 'includes/lib_order.php');

        $main_warehouse_id = $this->erpController->getMainWarehouseId();
        $shop_warehouse_id = $this->erpController->getShopWarehouseId();

        // get transfered order id
        $orderController = $this->orderController;
        $sql = "select distinct order_id from ".$ecs->table('order_goods_transfer_reserved')." where type = ".$orderController::ORDER_GOODS_RESERVE_TYPE_TRANSFER."  ";
        $orderTransfered = $db->getCol($sql);


        // 按order pay time 
        $sql = "SELECT eo.client_order_sn, o.order_id, o.order_sn, o.add_time, o.order_status, o.shipping_id, o.shipping_status, o.order_amount, o.money_paid, o.cp_price, s.shipping_name, " .
                 "o.pay_status, o.order_type, o.type_sn, o.type_id, o.actg_paid_amount,o.pay_id, o.extension_code, o.extension_id, o.postscript, o.serial_numbers,  " .
                 "o.consignee, o.address, o.country, o.email, o.tel, o.pay_time, s.shipping_name, o.shipping_fod, o.language, p.pay_name, " .
                 "(" . order_amount_field('o.') . ") AS total_fee, o.to_buyer, " .
                 "(o.money_paid + o.order_amount) AS order_total, " .
                 "IFNULL(u.user_name, '" .$GLOBALS['_LANG']['anonymous']. "') AS buyer, o.user_id, o.salesperson, o.platform, ".
                 "o.fully_paid, o.actg_paid_amount, ". // Accounting System for wholesale orders
                 "IFNULL((SELECT count(*) FROM ".$GLOBALS['ecs']->table('order_action')." as oa WHERE oa.order_id = o.order_id), 0) as action_count " . // Action count

             " FROM " . $GLOBALS['ecs']->table('order_info') . " AS o " .
             " LEFT JOIN " .$GLOBALS['ecs']->table('erp_order'). " AS eo ON eo.client_order_sn=o.order_sn ".
             " LEFT JOIN " .$GLOBALS['ecs']->table('payment'). " AS p ON p.pay_id=o.pay_id ".
             " LEFT JOIN " .$GLOBALS['ecs']->table('shipping'). " AS s ON s.shipping_id=o.shipping_id ".
             " LEFT JOIN " .$GLOBALS['ecs']->table('users'). " AS u ON u.user_id=o.user_id ". 
             " where 1  ". order_query_sql('await_ship', 'o.') .
             " ORDER BY pay_time asc ";

        //echo $sql;exit;
        $row = $GLOBALS['db']->getAll($sql);

        $order_ids = array_map(function ($order) { return $order['order_id']; }, $row);
        
        $order_goods_ids = [];
        // get order goods ids
        $sql = "SELECT distinct og.goods_id " .
               "FROM " . $GLOBALS['ecs']->table('order_goods') . " as og " .
               "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON og.goods_id = g.goods_id " .
               "WHERE order_id " . db_create_in($order_ids);
        $order_goods_ids = $GLOBALS['db']->getCol($sql);

        // for main warehouse goods qty
        $goods_warehouse_qty = $this->erpController->getSellableProductsQtyByWarehouses($order_goods_ids,null,false);
        $shop_warehouse_qty = $this->erpController->getSellableProductsQtyByWarehouses($order_goods_ids,$shop_warehouse_id,true);

        $fls_warehouse_id = $this->erpController->getFlsWarehouseId();

        //echo '<pre>';
     
        // echo $main_warehouse_id;
        $main_goods_qty = [];
        foreach ($goods_warehouse_qty as $goods_id_key => $warehouse_qty) {
            $main_goods_qty[$goods_id_key] = $warehouse_qty[$main_warehouse_id];
        }

        $goods_qty_fls = [];
        foreach ($goods_warehouse_qty as $goods_id_key => $warehouse_qty) {
            $goods_qty_fls[$goods_id_key] = $warehouse_qty[$fls_warehouse_id];
        }

        $shop_warehouse_qty = [];
        foreach ($goods_warehouse_qty as $goods_id_key => $warehouse_qty) {
            $shop_warehouse_qty[$goods_id_key] = $warehouse_qty[$shop_warehouse_id];
        }
        
        //print_r($main_goods_qty);
        //print_r($goods_qty_fls);
        //print_r($shop_warehouse_qty);
        //$goods_qty_fls

        // $sql = "SELECT ega.goods_id, IFNULL(SUM(ega.goods_qty),0) as storage " .
        //        "FROM " . $GLOBALS['ecs']->table('erp_goods_attr_stock') . " AS ega " .
        //        "WHERE ega.goods_id " . db_create_in($order_goods_ids) ." AND ega.warehouse_id IN (".$sellableWarehouseIds.") ".
        //        "group by goods_id";
        // 
        // $order_goods = $GLOBALS['db']->getAll($sql);
        // 
        // $goods_qty = [];
        // foreach ($order_goods as $order_goods_info){
        //    $goods_qty[$order_goods_info['goods_id']] = $order_goods_info['storage'];
        // }
        // 
        // foreach ($order_goods_ids as $order_goods_id) {
        //    if (!isset($goods_qty[$order_goods_id])) {
        //        $goods_qty[$order_goods_id] = 0;
        //    } 
        // }

        $sql = "SELECT og.*, g.goods_thumb, g.goods_height, g.goods_width, g.goods_length " .
           "FROM " . $GLOBALS['ecs']->table('order_goods') . " as og " .
           "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON og.goods_id = g.goods_id " .
           "WHERE order_id " . db_create_in($order_ids);

        $order_goods = $GLOBALS['db']->getAll($sql);

        $order_goods_by_order_id = [];
        foreach ($order_goods as $order_goods_key =>  $order_goods_info){
           // add storage to order_goods ar
           $order_goods[$order_goods_key]['storage'] = $goods_qty[$order_goods_info['goods_id']];
           $order_goods_by_order_id[$order_goods_info['order_id']][] = $order_goods[$order_goods_key];
        }

        // for FLS warehouse
        // $sql = "SELECT og.*, g.goods_thumb " .
        //         (1 ? ", (SELECT IFNULL(SUM(ega.goods_qty),0) FROM " . $GLOBALS['ecs']->table('erp_goods_attr_stock') . " AS ega WHERE g.goods_id = ega.goods_id AND ega.warehouse_id IN (".$sellableWarehouseIdsFls.")) AS storage " : '') .
        //         "FROM " . $GLOBALS['ecs']->table('order_goods') . " as og " .
        //         "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON og.goods_id = g.goods_id " .
        //         "WHERE order_id " . db_create_in($order_ids);
        // $order_goods_fls = $GLOBALS['db']->getAll($sql);
        // $sql = "SELECT ega.goods_id, IFNULL(SUM(ega.goods_qty),0) as storage " .
        //        "FROM " . $GLOBALS['ecs']->table('erp_goods_attr_stock') . " AS ega " .
        //        "WHERE ega.goods_id " . db_create_in($order_goods_ids) ." AND ega.warehouse_id IN (".$sellableWarehouseIdsFls.") ".
        //        "group by goods_id";
        // 
        // $order_goods_fls = $GLOBALS['db']->getAll($sql);
        // 
        // $goods_qty_fls = [];
        // foreach ($order_goods_fls as $order_goods_info){
        //    $goods_qty_fls[$order_goods_info['goods_id']] = $order_goods_info['storage'];
        // }
        // 
        // foreach ($order_goods_ids_fls as $order_goods_id_fls) {
        //    if (!isset($goods_qty_fls[$order_goods_id_fls])) {
        //        $goods_qty_fls[$order_goods_id_fls] = 0;
        //    } 
        // }
        // 
        // // for main warehouse without fls
        // foreach ($goods_qty as $goods_key => $qty){
        //    $main_goods_qty[$goods_key] = $qty - $goods_qty_fls[$goods_key];
        // }

        $orders_can_ship = [];
        $orders_cant_ship = [];
        $orders_can_ship_fls = [];
        
        $row_temp = $row;
        $transfer_ar = [];
        //print_r($row);

        // check FLS Stock 
        foreach ($row_temp AS $key => $value)
        {
            if (isset($order_goods_by_order_id[$value['order_id']]))
            {
                $all_order_goods_in_stock = true;
                // FLS new flow
                // in 待出貨(本地速遞) HK
                if (in_array($value['shipping_id'],[3]) and $value['country'] == 3409) {
                    foreach ($order_goods_by_order_id[$value['order_id']] as $ogs)
                    {
                        if ( ($goods_qty_fls[$ogs['goods_id']] - $ogs['goods_number'] + $ogs['send_number']) >= 0) {
                            $goods_qty_fls[$ogs['goods_id']] = $goods_qty_fls[$ogs['goods_id']] -$ogs['goods_number'] + $ogs['send_number'];
                        } else {
                            $goods_qty_fls[$ogs['goods_id']] = 0;
                            $all_order_goods_in_stock = false;
                        }
                    }

                    // all in stock
                    if ($all_order_goods_in_stock == true) {
                      $orders_can_ship_fls[] = $value['order_id']; 
                    }
                }
            }
        }

        foreach ($row_temp AS $key => $value)
        {
            // if (in_array($_REQUEST['composite_status'],[CS_AWAIT_SHIP_HK,CS_AWAIT_SHIP_HK_YOHO]))
            // {
            //     // skip directly, because it will be ship by FLS warehouse
                if (in_array($value['order_id'],$orders_can_ship_fls)) {
                    continue;
                }
            // }
            $debug = 0;
            // filter the order can't be shipped
            if (isset($order_goods_by_order_id[$value['order_id']]))
            {
                $all_order_goods_in_stock = true;
                //if (in_array($_REQUEST['composite_status'],[CS_AWAIT_SHIP_LOCKER,CS_AWAIT_PICK_HK,CS_AWAIT_SHIP_NON_HK]))
                //{
                    // goods qty without fls
                    foreach ($order_goods_by_order_id[$value['order_id']] as $ogs)
                    {
                        // order detail
                        $ogs['shipping_name'] = $value['shipping_name'];

                        $diff_qty = $main_goods_qty[$ogs['goods_id']] - $ogs['goods_number'] + $ogs['send_number'];

                        if ($ogs['goods_id'] == '2591' && $debug == 1) {
                            echo 'order_id:'.$value['order_id'].'<br>';
                            echo 'diff:'.$diff_qty.'<br>';
                            echo '<br>';
                        }

                        if ($diff_qty >= 0) {
                            
                            $main_goods_qty[$ogs['goods_id']] = $diff_qty;

                            // bulid transfer array
                              
                            $transfer_qty =  $ogs['goods_number'] - $ogs['send_number'];
                               if ($ogs['goods_id'] == '2591'  && $debug == 1) {
                                   echo 'yes'.'<br>';
                                   echo 'transfer:'.$transfer_qty.'<br>';
                                   echo '<br>';
                               }
                            if ($transfer_qty > 0) {
                                if ($ogs['goods_id'] == '2591'  && $debug == 1) {
                                    echo 'insert'.'<br>';
                                }
                                $ogs['transfer_qty'] = $transfer_qty;
                                $ogs['order_sn'] = $value['order_sn'];
                                $transfer_ar[] = $ogs;
                               
                            }
                        } else {
                            //if store pick up, count shop sellable stock , if stock enough, then transfer
                            if (in_array($value['shipping_id'],[2,5])) {

                                $diff_stock = $main_goods_qty[$ogs['goods_id']] - $ogs['goods_number'] + $ogs['send_number'] + $shop_warehouse_qty[$ogs['goods_id']];

                                 if ($ogs['goods_id'] == '2591'  && $debug == 1) {
                                     echo 'no'.'<br>';
                                     echo 'diff_stock:'.$diff_stock.'<br>';
                                     echo '<br>';
                                     echo  'main:'.$main_goods_qty[$ogs['goods_id']];
                                 }

                                if ( $diff_stock >= 0){
                                  
                                    $shop_warehouse_qty[$ogs['goods_id']] = $diff_stock;
                                    if ($main_goods_qty[$ogs['goods_id']] > 0) {
                                        if ($ogs['goods_id'] == '2591'  && $debug == 1) {
                                            echo 'insert'.'<br>';
                                        }
                                        $ogs['order_sn'] = $value['order_sn'];
                                        $ogs['transfer_qty'] = $main_goods_qty[$ogs['goods_id']];
                                        $transfer_ar[] = $ogs;
                                     //   $transfer_ar[] = array($value['order_id'],$ogs['goods_id'],$main_goods_qty[$ogs['goods_id']],$main_warehouse_id,$shop_warehouse_id,0,0);
                                       
                                    }
                                } else {
                                    $all_order_goods_in_stock = false;
                                }
                            } else {
                                $all_order_goods_in_stock = false;
                            }

                            $main_goods_qty[$ogs['goods_id']] = 0;                            
                        }
                    }
                // } else {
                //     // goods qty global
                //     foreach ($order_goods_by_order_id[$value['order_id']] as $ogs)
                //     {
                //         if ($goods_qty[$ogs['goods_id']] - $ogs['goods_number'] + $ogs['send_number'] >= 0) {
                //             $goods_qty[$ogs['goods_id']] = $goods_qty[$ogs['goods_id']] -$ogs['goods_number'] + $ogs['send_number'];
                //         } else {
                //             $goods_qty[$ogs['goods_id']] = 0;
                //             $all_order_goods_in_stock = false;
                //         }
                //     }
                // }

                if ($all_order_goods_in_stock == true) {
                    $orders_can_ship[] = $value['order_id']; 
                } else {
                    $orders_cant_ship[] = $value['order_id']; 
                }
            }
        }

        // print_R($orders_can_ship);
        // print_R($orders_cant_ship);
        // print_R($orders_can_ship_fls);

        $transfer_orders = [];
        foreach ($row AS $key => $value)
        {
            if (in_array($value['order_id'],$orders_can_ship)) {
                if (in_array($value['shipping_id'],[2,5])) {            
                    $transfer_orders[] = $value['order_id'];
                }
            }
        }
       
        $transfer_order_goods = [];
        foreach ($transfer_ar AS $key => &$transfer_info)
        {
            $transfer_info['goods_thumb'] = '<img width="50" src="../'.$transfer_info['goods_thumb'].'"/>';
            $hidden_field = '<input type="hidden" value="'.$transfer_info['order_id'].'" name="order_id[]" />';
            $hidden_field .= '<input type="hidden" value="'.$transfer_info['goods_id'].'" name="goods_id[]" />';
            $transfer_info['transfer_qty_html'] = $hidden_field.'<input name="transfer_qty[]" type="text" size="5" value="'.$transfer_info['transfer_qty'].'" class="form-control" />';

            $transfer_info['order_sn_html']  = '<a target="_blank" href="order.php?act=info&order_id='.$transfer_info['order_id'].'">'.$transfer_info['order_sn'].'</a>';
            $transfer_info['order_id_html'] = ''.$transfer_info['order_id'];

            // echo '<pre>';
            // print_r($transfer_info);
            // exit;

            if (in_array($transfer_info['order_id'],$transfer_orders) && !in_array($transfer_info['order_id'],$orderTransfered)  ) {
                $transfer_order_goods[] = $transfer_info;
            }
        }
        //echo '<pre>';
        //print_r($transfer_ar);
        //exit;
        $arr = array(
            'data' => $transfer_order_goods,
            'record_count' => sizeof($transfer_order_goods)
        );

        return $arr;

    }

    public function shopPickupOrderGoodsTransferCreate()
    {
        global $db, $ecs, $userController ,$_CFG;
    //	echo '<pre>';
   // 	print_r($_REQUEST);
       // $db->query('START TRANSACTION');
        $orderController = new OrderController();
        $transfer_goods = [];
        foreach ($_REQUEST['transfer_qty'] as $key => $transfer_qty) {
            if ($transfer_qty > 0) {
                $transfer_goods[$_REQUEST['goods_id'][$key]] += $_REQUEST['transfer_qty'][$key];
            }
        }

        //  print_r($transfer_goods);
        $warehouseTransferController = $this->warehouseTransferController;
        
        // create transfer list
        $warehouse_from = $_REQUEST['main_warehouse_id'];
        $warehouse_to = $_REQUEST['shop_warehouse_id'];
        $transfer_type = $warehouseTransferController::REASON_TYPE_STORE_PICK_UP;
      
        $operator = erp_get_admin_id();
        
        $transfer_id = $warehouseTransferController->warehouseTransferCreateByGoodsTransfer($warehouse_from, $warehouse_to, $transfer_goods, $transfer_type, $operator, 0);
      
        if (isset($transfer_id['error'])) {
            return $transfer_id;
        }

        foreach ($_REQUEST['transfer_qty'] as $key => $transfer_qty) {
            if ($transfer_qty > 0) {
                $order_goods_transfer_id = $orderController->createOrderGoodsTransferReserved($_REQUEST['order_id'][$key],$_REQUEST['goods_id'][$key],$transfer_qty,$_REQUEST['main_warehouse_id'],$_REQUEST['shop_warehouse_id'],$orderController::ORDER_GOODS_RESERVE_TYPE_TRANSFER);
                $sql = 'update '.$ecs->table('order_goods_transfer_reserved').' set transfer_id = "'.$transfer_id.'" where order_goods_transfer_id = "'.$order_goods_transfer_id.'" ';
                $db->query($sql);
            }
        }

        return $transfer_id;
        //   $db->query("COMMIT");
    	exit;
    }

    public function shopToWarehouseOrderGoodsTransferCreate()
    {
        global $db, $ecs, $userController ,$_CFG;
    //	echo '<pre>';
   // 	print_r($_REQUEST);
       // $db->query('START TRANSACTION');
        $orderController = new OrderController();
        $transfer_goods = [];
        foreach ($_REQUEST['transfer_qty'] as $key => $transfer_qty) {
            if ($transfer_qty > 0) {
                $transfer_goods[$_REQUEST['goods_id'][$key]] += $_REQUEST['transfer_qty'][$key];
            }
        }

        //  print_r($transfer_goods);
        $warehouseTransferController = $this->warehouseTransferController;
        
        // create transfer list
        $warehouse_from = $_REQUEST['from_warehouse_id'];
        $warehouse_to = $_REQUEST['to_warehouse_id'];
        //$transfer_type = $warehouseTransferController::REASON_TYPE_ORDER_OUT_OF_STOCK;
        
        $transfer_type =  $_REQUEST['reason_type']; 

        $operator = erp_get_admin_id();
        
        $transfer_id = $warehouseTransferController->warehouseTransferCreateByGoodsTransfer($warehouse_from, $warehouse_to, $transfer_goods, $transfer_type, $operator, 0);
      
        if (isset($transfer_id['error'])) {
            return $transfer_id;
        }

        foreach ($_REQUEST['transfer_qty'] as $key => $transfer_qty) {
            if ($transfer_qty > 0) {
                $order_goods_transfer_id = $orderController->createOrderGoodsTransferReserved($_REQUEST['order_id'][$key],$_REQUEST['goods_id'][$key],$transfer_qty,$warehouse_from,$warehouse_to,$orderController::ORDER_GOODS_RESERVE_TYPE_TRANSFER);
                $sql = 'update '.$ecs->table('order_goods_transfer_reserved').' set transfer_id = "'.$transfer_id.'" where order_goods_transfer_id = "'.$order_goods_transfer_id.'" ';
                $db->query($sql);
            }
        }

        return $transfer_id;
        //   $db->query("COMMIT");
    	exit;
    }

}
