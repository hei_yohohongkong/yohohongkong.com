<?php

/**
 *  By Billy Lee 20190425
 * 
 *  SQL Reporting Controller
 * 
 */
 
namespace Yoho\cms\Controller;

if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}
include_once(ROOT_PATH . '/includes/cls_image.php');

class SqlReportController extends YohoBaseController
{
    private $dir_path;
    private $user_ranks;

    public function __construct(){
        $this->dir_path = ROOT_PATH.'data/export_reports/';
        if ( !file_exists( $this->dir_path ) && !is_dir( $this->dir_path ) ) {
            mkdir( $this->dir_path , 0777);       
        } 
        $this->user_ranks = $this->get_all_user_ranks();
        parent::__construct();
    }

    public function showSqlReportPage() {
        global $ecs, $db, $_LANG, $_CFG, $smarty;        
        include_once ROOT_PATH . 'languages/' . $_CFG['lang'] . '/admin/accounting_system.php';

        // Customer Purchase Reports
        $cat_list = cat_list();
        $brand_list = get_brand_list();

        $smarty->assign('ur_here', "Data Export");
        $smarty->assign('cat_list', $cat_list);
        $smarty->assign('brand_list', $brand_list);

        // Accounting Entries Export
        $accountingController = new AccountingController();

        $smarty->assign('action_link_list', $action_link_list);
        $smarty->assign('system_user', $accountingController::ACTG_SYSTEM_USER);
        $smarty->assign('operators', $accountingController->getAccountingOperators());
        $smarty->assign('txn_types', $accountingController->getAccountingTypes(0, false));
        $smarty->assign('suppliers', $accountingController->getAccountingSuppliersList());
        $smarty->assign('wholesales', $accountingController->getAccountingWholesaleCustomerList());
        $smarty->assign('salesagents', $accountingController->getAccountingSalesAgentList());

        // User List Export
        $all_ranks = $this->get_all_user_ranks();
        $ranks = array();
        foreach ($all_ranks as $row)
        {
            $ranks[$row['rank_id']] = $row['rank_name'];
        }
        $smarty->assign('user_ranks',   $ranks);

        $smarty->assign('lang', $_LANG);

        $smarty->display('sql_reports.htm');
    }

    public function getCustomerPurchaseQuery(Array $param) 
    {
        global $ecs, $db, $_LANG;
        $file_name = (isset($param['filename']) && !empty($param['filename']) && $this->verifyExportFilename($param['filename'])) ? $this->processFileName($param['filename']) : $this->processFileName(local_date('YmdHi') . '-customer_report.xlsx');

        // Record the query records for executing
        $query_timestamp = gmtime();
        $operator = $_SESSION['admin_name'] ? $_SESSION['admin_name']: 'unknown';
        $record_sql = "INSERT INTO " . $ecs->table('report_query') . "(query_date, operator, type, filename) VALUES (" . $query_timestamp . ", '" . $operator . "', 'customer_purchase', '" . $file_name . "')";
        $db->query($record_sql);

        // Should echo a acknowledgement before executing query
        $msg = array('error' => '0', 'message' => $_LANG['query_created'], 'content' => '');
        ob_end_clean();
        ob_start();
        echo json_encode($msg);
        $size = ob_get_length();
        header("Content-Length: $size");
        header("Connection: close");
        header("HTTP/1.1 200 OK");
        header("Content-Type: application/json;charset=utf-8");
        ob_end_flush();
        ob_flush();
        flush();

        sleep(3);

        // Initialize Query

        // Set session-specific memory and execution time limit
        ini_set('memory_limit', '1024M');
        set_time_limit(1200); // Maximum execute time is set to 20 minutes
        ignore_user_abort(true);

        // Call query function
        $customer_purchase_records = $this->getCustomerPurchaseReport($param);

        // Export and save to xls file
        $this->saveCustomerPurchaseQuery($customer_purchase_records, $file_name, ((isset($param['detail_query']) && $param['detail_query']) ? true : false));
        
    }

    public function getUserListQuery($param)
    {
        global $ecs, $db, $_LANG;
        $file_name = (isset($param['filename']) && !empty($param['filename']) && $this->verifyExportFilename($param['filename'])) ? $this->processFileName($param['filename']) : $this->processFileName(local_date('YmdHi') . '-user-list_report.xlsx');
        
        // Record the query records for executing
        $query_timestamp = gmtime();
        $operator = $_SESSION['admin_name'] ? $_SESSION['admin_name']: 'unknown';
        $record_sql = "INSERT INTO " . $ecs->table('report_query') . "(query_date, operator, type, filename) VALUES (" . $query_timestamp . ", '" . $operator . "', 'user_list', '" . $file_name . "')";
        $db->query($record_sql);

        // Should echo a acknowledgement before executing query
        $msg = array('error' => '0', 'message' => $_LANG['query_created'], 'content' => '');
        ob_end_clean();
        ob_start();
        echo json_encode($msg);
        $size = ob_get_length();
        header("Content-Length: $size");
        header("Connection: close");
        header("HTTP/1.1 200 OK");
        header("Content-Type: application/json;charset=utf-8");
        ob_end_flush();
        ob_flush();
        flush();

        sleep(3);

        // Set session-specific memory and execution time limit
        ini_set('memory_limit', '1024M');
        set_time_limit(3600); // Maximum execute time is set to 60 minutes (1 Hour)
        ignore_user_abort(true);

        // Initialize Query
        $user_list = $this->getUserListReport($param);

        // Export and save to xls file
        $this->saveUserListQuery($user_list, $file_name);
    }

    public function getReportQueryQueue($type = null)
    {
        global $ecs, $db, $_LANG;

        // TODO: Privilege check

        $query_sql = 'select * from '. $ecs->table('report_query');

        $valid_type = [
            'customer_purchase',
            'accounting_entry',
            'user_list'
        ];

        $expiry_time = [
            'customer_purchase' => 1200,
            'accounting_entry' => 3600,
            'user_list' => 3600
        ];

        if ($type) {
            if (in_array($type, $valid_type)) {
                $query_sql .= "where type = '$type'";
            }
            else
            {
                make_json_error('Invalid parameter');
            }
        }

        $query_sql .= ' order by report_query_id desc limit 5';

        $result = $db->getAll($query_sql);

        // Add file ready status
        foreach($result as $key => $value) {
            $report_ready = file_exists(ROOT_PATH.$value['path'].$value['filename']);
            $formatted_date = local_date('Y-m-d H:i:s', $value['query_date']);
            $expired = (gmtime() - $value['query_date']) > (!empty($type) ? $expiry_time[$type] : 1200);
            $result[$key]['formatted_query_date'] = $formatted_date;
            $result[$key]['report_ready'] = $report_ready;
            $result[$key]['status'] = $report_ready ? '<span class="label label-success">' . $_LANG['query_done'] . '</span>' : ($expired ? '<span class="label label-default">' . $_LANG['query_expired'] . '</span>' : '<span class="label label-primary">' . $_LANG['query_pending'] . '</span>');
            $result[$key]['download'] =$report_ready ? '<a href="sql_reports.php?act=download_report&query_id='. $value['report_query_id'] . '">' . $_LANG['download_query_report'] . '</a>' : $_LANG['download_query_report_not_available'];
        }

        // Return JSON result
        $this->ajaxQueryAction($result, count($result), false);
    }

    public function getAccountingEntryRecordQuery(Array $param)
    {
        global $db, $ecs, $_LANG;
        $file_name = (isset($param['filename']) && !empty($param['filename']) && $this->verifyExportFilename($param['filename'])) ? $this->processFileName($param['filename']) : $this->processFileName(local_date('YmdHi') . '-accounting_entry_report.xlsx');

        // Record the query records for executing
        $query_timestamp = gmtime();
        $operator = $_SESSION['admin_name'] ? $_SESSION['admin_name']: 'unknown';
        $record_sql = "INSERT INTO " . $ecs->table('report_query') . "(query_date, operator, type, filename) VALUES (" . $query_timestamp . ", '" . $operator . "', 'accounting_entry', '" . $file_name . "')";
        $db->query($record_sql);

        // Should echo a acknowledgement before executing query
        $msg = array('error' => '0', 'message' => $_LANG['query_created'], 'content' => '');
        ob_end_clean();
        ob_start();
        echo json_encode($msg);
        $size = ob_get_length();
        header("Content-Length: $size");
        header("Connection: close");
        header("HTTP/1.1 200 OK");
        header("Content-Type: application/json;charset=utf-8");
        ob_end_flush();
        ob_flush();
        flush();

        sleep(3);

        // Initialize Query

        // Set session-specific memory and execution time limit
        ini_set('memory_limit', '1024M');
        set_time_limit(3600); // Maximum execute time is set to 60 minutes (1 Hour)
        ignore_user_abort(true);

        // Retrieve records
        $accountingController = new AccountingController();
        $records = $accountingController->getAccountingTransactionsReport($param);

        // Save records
        $this->saveAccountingEntryReportQuery($records, $file_name);
    }

    public function downloadReport($report_id = null)
    {
        global $ecs, $db;

        // Privilege Check

        if (!$report_id || !intval($report_id)) {
            make_json_error('Missing Required Parameter');
        }

        $query_sql = 'select * from '. $ecs->table('report_query'). 'where report_query_id = "' . $report_id . '"';

        $result = $db->getRow($query_sql);
        if(empty($result)) {
            die();
        }

        $report_full_path = ROOT_PATH.$result['path'].$result['filename'];

        if (!file_exists($report_full_path)){
            make_json_error('Report is not ready');
        }

        // Prepare response for download
        header("HTTP/1.1 200 OK");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.‌​sheet; charset=utf-8');
        header("Content-Transfer-Encoding: Binary");
        header("Content-Length:".filesize($report_full_path));
        header("Content-Disposition: attachment; filename=".$result['filename']);
        readfile($report_full_path);
        exit();
    }

    private function verifyExportFilename($filename)
    {
        return preg_match('/^[\w|.|-]+.xlsx?$/', $filename);
    }

    private function processFileName($filename) {
        return rand(1000,9999) . '-' . $filename;
    }

    private function getCustomerPurchaseReport($param)
    {
        global $ecs, $db;
        // Build Query string

        // Base expressions: fields to be selected with joined table
        $simplified_select_query = "select distinct u.user_id, u.user_name, u.email, if(u.email is null or u.email = '' or u.email REGEXP '^[\\\+0-9-]+(@yohohongkong.com)$', '0', '1') as `user_email_registered`, u.pay_points";
        $detailed_select_query = "select date(convert_tz(from_unixtime(eoi.add_time), '+00:00', '+08:00')) as order_date, eoi.order_sn, eg.goods_name, eog.goods_number, u.user_id, u.user_name, u.email, if(u.email is null or u.email = '' or u.email REGEXP '^[\\\+0-9-]+(@yohohongkong.com)$', '0', '1') as `user_email_registered`, u.pay_points";
        $base_from_query = " from ecs_order_info as eoi, ecs_order_goods as eog, ecs_goods as eg, ecs_users as u where eoi.order_id = eog.order_id and eog.goods_id = eg.goods_id and u.user_id = eoi.user_id";

        $query_sql = $base_from_query;
        // Filtering options

        // Where clauses
        if (isset($param['gender']) && !empty($param['gender'])) {
            $query_sql .= " and u.sex = ". intval($param['gender']);
        }

        if (isset($param['productId']) && !empty($param['productId'])) {
            $query_sql .= " and eg.goods_id in (" . implode(',', array_map('intval', explode(',', $param['productId']))) . ")";
        }

        if (isset($param['brand']) && !empty($param['brand'])) {
            $query_sql .= " and eg.brand_id = ". intval($param['brand']);
        }

        if (isset($param['category']) && !empty($param['category'])) {
            // Get all child category on a given category
            $query_category_list = "(" . implode(', ', array_unique(array_keys(cat_list(intval($param['category']), 0, false, 0, false)))) . ")";
            $query_sql .= " and eg.cat_id in ". $query_category_list;
        }

        // for simplified version
        if (!isset($param['detail_query']) || (isset($param['detail_query']) && !$param['detail_query'])) {
            if (!empty($param['start_date']) && !empty($param['end_date'])) {
                $query_sql .= " and unix_timestamp(convert_tz(from_unixtime(eoi.add_time), '+00:00', '+08:00')) between unix_timestamp(date('" . $param['start_date'] . "')) and unix_timestamp(date('" . $param['end_date'] . "'))";
            }
            elseif (isset($param['dateRange']) && !empty($param['dateRange'])) {
                $query_sql .= " and unix_timestamp(convert_tz(from_unixtime(eoi.add_time), '+00:00', '+08:00')) between unix_timestamp(date(DATE_SUB(date(NOW()), INTERVAL ". intval($param['dateRange']) ." day))) and unix_timestamp(date(now()))";
            }
        }

        // Having clauses
        if 
        (   
            ((isset($param['detail_query']) && $param['detail_query']) && (isset($param['dateRange']) && !empty($param['dateRange']) || (!empty($param['start_date']) && !empty($param['end_date'])))) || 
            isset($param['includeEmail']) && !empty($param['includeEmail'])
        ) {
            $having_count = 0;

            if(isset($param['detail_query']) && $param['detail_query']) {
                if (!empty($param['start_date']) && !empty($param['end_date'])) {
                    if ($having_count > 0) {
                        $query_sql .= ' and ';
                    } else {
                        $query_sql .= " having";
                    }
                    $query_sql .= " order_date between date('" . $param['start_date'] . "') and date('" . $param['end_date'] . "')";
                    $having_count++;
                }
                elseif (isset($param['dateRange']) && !empty($param['dateRange'])) {
                    if ($having_count > 0) {
                        $query_sql .= ' and ';
                    }
                    $query_sql .= " order_date between date(DATE_SUB(date(NOW()), INTERVAL ". intval($param['dateRange']) ." day)) and date(now())";
                    $having_count++;
                }
            }

            if (isset($param['includeEmail']) && !empty($param['includeEmail'])) {
                if ($param['includeEmail'] != 'all') {

                    if ($having_count > 0) {
                        $query_sql .= ' and ';
                    } else {
                        $query_sql .= " having";
                    }

                    $query_sql .= " user_email_registered = '". ($param['includeEmail'] == 'yes' ? '1' : '0') . "'";
                    $having_count++;
                }
            }
        }
        $customer_purchase_records = [];

        // Set target fields
        if(isset($param['detail_query']) && $param['detail_query']) {
            $base_sql = $detailed_select_query .  $query_sql;
        } else {
            $base_sql = $simplified_select_query .  $query_sql;
        }
        // Get expected total size
        $count_sql = "select count(*) from (" . $base_sql . ") as base_query_table";
        $record_count = $db->getOne($count_sql);

        // Execute Query by crunch 
        $limit = 5000;
        
        for($already_get = 0; $already_get < $record_count; $already_get+=$limit) {
            $sql = $base_sql . "limit " . $already_get . ", " . $limit;
            $customer_purchase_records_crunch = $db->getAll($sql);
            foreach($customer_purchase_records_crunch as $customer_purchase_record_entry) {
                $customer_purchase_records[] = $customer_purchase_record_entry;
            }
        }

        return $customer_purchase_records;
    }

    private function saveCustomerPurchaseQuery($customer_purchase_records, $file_name, $detail_query = false)
    {
        global $_LANG;
        define('PCLZIP_TEMPORARY_DIR', ROOT_PATH . 'temp/');
        require_once ROOT_PATH . 'includes/excel/PHPExcel.php';
        require_once ROOT_PATH . 'includes/excel/PHPExcel/IOFactory.php';
        \PHPExcel_Settings::setZipClass(\PHPExcel_Settings::PCLZIP);
        $objPHPExcel = new \PHPExcel();

        if($detail_query) {
            $export[] = array(
                ecs_iconv(EC_CHARSET, 'UTF8', $_LANG['order_date']),
                ecs_iconv(EC_CHARSET, 'UTF8', $_LANG['order_sn']),
                ecs_iconv(EC_CHARSET, 'UTF8', $_LANG['goods_name']),
                ecs_iconv(EC_CHARSET, 'UTF8', $_LANG['goods_number']),
                ecs_iconv(EC_CHARSET, 'UTF8', $_LANG['user_id']),
                ecs_iconv(EC_CHARSET, 'UTF8', $_LANG['user_name']),
                ecs_iconv(EC_CHARSET, 'UTF8', $_LANG['email']),
                ecs_iconv(EC_CHARSET, 'UTF8', $_LANG['user_email_registered']),
                ecs_iconv(EC_CHARSET, 'UTF8', $_LANG['user_pay_points']),
            );
        
            foreach($customer_purchase_records as $customer_purchase_record) {
                $export[] = array(
                    $customer_purchase_record['order_date'],
                    $customer_purchase_record['order_sn'],
                    $customer_purchase_record['goods_name'],
                    $customer_purchase_record['goods_number'],
                    $customer_purchase_record['user_id'],
                    $customer_purchase_record['user_name'],
                    $customer_purchase_record['email'],
                    $customer_purchase_record['user_email_registered'],
                    $customer_purchase_record['pay_points'],
                );
            }

            $objPHPExcel->setActiveSheetIndex()->fromArray($export,null,'A1');
            foreach($customer_purchase_records as $index => $customer_purchase_record) {
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.($index+2), (string)$customer_purchase_record['order_sn'],\PHPExcel_Cell_DataType::TYPE_STRING);
            }
        }
        else
        {
            $export[] = array(
                ecs_iconv(EC_CHARSET, 'UTF8', $_LANG['user_id']),
                ecs_iconv(EC_CHARSET, 'UTF8', $_LANG['user_name']),
                ecs_iconv(EC_CHARSET, 'UTF8', $_LANG['email']),
                ecs_iconv(EC_CHARSET, 'UTF8', $_LANG['user_email_registered']),
                ecs_iconv(EC_CHARSET, 'UTF8', $_LANG['user_pay_points']),
            );
        
            foreach($customer_purchase_records as $customer_purchase_record) {
                $export[] = array(
                    $customer_purchase_record['user_id'],
                    $customer_purchase_record['user_name'],
                    $customer_purchase_record['email'],
                    $customer_purchase_record['user_email_registered'],
                    $customer_purchase_record['pay_points'],
                );
            }
            $objPHPExcel->setActiveSheetIndex()->fromArray($export,null,'A1');
        }

        header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.‌​sheet; charset=utf-8');
        header('Content-Disposition: attachment; filename=' . $file_name);
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save(ROOT_PATH.'data/export_reports/'.$file_name);
    }

    private function saveAccountingEntryReportQuery($accounting_records, $file_name)
    {
        global $_LANG;

        require_once(ROOT_PATH . 'includes/php-xlsx-writer/xlsxwriter.class.php');

        $writer = new \XLSXWriter();

        $export = [
            ecs_iconv(EC_CHARSET, 'UTF8', $_LANG['accounting_entry_id']) => 'string',
            ecs_iconv(EC_CHARSET, 'UTF8', $_LANG['accounting_entry_operator']) => 'string',
            ecs_iconv(EC_CHARSET, 'UTF8', $_LANG['accounting_entry_txn_date']) => 'string',
            ecs_iconv(EC_CHARSET, 'UTF8', $_LANG['accounting_entry_actg_month']) => 'string',
            ecs_iconv(EC_CHARSET, 'UTF8', $_LANG['accounting_entry_txn_account_id']) => 'string',
            ecs_iconv(EC_CHARSET, 'UTF8', $_LANG['accounting_entry_txn_account_name']) => 'string',
            ecs_iconv(EC_CHARSET, 'UTF8', $_LANG['accounting_entry_remark']) => 'string',
            ecs_iconv(EC_CHARSET, 'UTF8', $_LANG['accounting_entry_debit']) => 'string',
            ecs_iconv(EC_CHARSET, 'UTF8', $_LANG['accounting_entry_credit']) => 'string',
            ecs_iconv(EC_CHARSET, 'UTF8', $_LANG['accounting_entry_status']) => 'string'
        ];

        $writer->writeSheetHeader('Sheet1', $export);
    
        foreach($accounting_records as $accounting_record) {
            $item = [
                (!empty($accounting_record['related_txn_id']) ? $accounting_record['related_txn_id'] : $accounting_record['actg_txn_id']),
                $accounting_record['user_name'],
                $accounting_record['txn_date'],
                $accounting_record['actg_month'],
                $accounting_record['actg_type_code'],
                $accounting_record['actg_type_name'],
                $accounting_record['remark'],
                $accounting_record['debit'],
                $accounting_record['credit'],
                $accounting_record['status'],
            ];
            $writer->writeSheetRow('Sheet1', $item);
        }

        header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.‌​sheet; charset=utf-8');
        header('Content-Disposition: attachment; filename=' . $file_name);
        header('Cache-Control: max-age=0');
        
        $writer->writeToFile(ROOT_PATH . 'data/export_reports/' . $file_name);
    }

    private function getUserListReport($param)
    {
        global $ecs, $db, $_LANG, $userController;

        $param['keyword'] = empty($param['keyword']) ? '' : trim($param['keyword']);
        $param['user_rank'] = empty($param['user_rank']) ? 0 : intval($param['user_rank']);
        $param['pay_points_gt'] = empty($param['pay_points_gt']) ? 0 : intval($param['pay_points_gt']);
        $param['pay_points_lt'] = empty($param['pay_points_lt']) ? 0 : intval($param['pay_points_lt']);

        $param['bought_goods'] = empty($param['bought_goods']) ? '' : trim($param['bought_goods']);
        $param['have_email'] = empty($param['have_email']) ? 0 : intval($param['have_email']);

        $ex_where = "";
        if ($_SESSION['manage_cost']) // 如果是超級管理員，可以查看完整會員列表
        {
            if ($param['keyword'])
            {
                $ex_where .= " WHERE (u.user_name LIKE '%" . mysql_like_quote($param['keyword']) ."%' OR u.email LIKE '%" . mysql_like_quote($param['keyword']) ."%')";
            }
        }
        else
        {
            // Non-admin, only works if user_name / email exact match or user_name postfix match
            if ($param['keyword'])
            {
                $ex_where .= " WHERE (u.user_name LIKE '%" . mysql_like_quote($param['keyword']) ."' OR u.email = '" . $param['keyword'] ."')";
            }
        }

        if ($param['user_rank'])
        {
            $sql = "SELECT min_points, max_points, special_rank FROM ".$GLOBALS['ecs']->table('user_rank')." WHERE rank_id = '$param[user_rank]'";
            $row = $GLOBALS['db']->getRow($sql);
            if ($row['special_rank'] > 0)
            {
                /* 特殊等级 */
                $ex_where = $this->concatenateWhereClause($ex_where, "u.user_rank = '$param[user_rank]'");
            }
            else
            {
                $ex_where = $this->concatenateWhereClause($ex_where, ("u.rank_points >= " . intval($row['min_points']) . " AND u.rank_points < " . intval($row['max_points'])));
            }
        }
        if ($param['pay_points_gt'])
        {
            
            $ex_where = $this->concatenateWhereClause($ex_where, "u.pay_points >= '$param[pay_points_gt]'");
        }
        if ($param['pay_points_lt'])
        {
            $ex_where = $this->concatenateWhereClause($ex_where, "u.pay_points < '$param[pay_points_lt]'");
        }
        if ($param['bought_goods'])
        {
            if (!preg_match('/^([0-9]+,)*[0-9]+$/', $param['bought_goods']))
            {
                $error_msg = '「曾購買產品ID」欄位輸入錯誤，請輸入以逗號(,)分隔的產品ID';
                {
                    sys_msg($error_msg);
                }
            }
            
            require_once ROOT_PATH . 'includes/lib_order.php';
            
            $ex_where = $this->concatenateWhereClause($ex_where, "u.user_id IN (" .
                            "SELECT oi.user_id " . 
                            "FROM " . $GLOBALS['ecs']->table('order_info') . " as oi " .
                            "LEFT JOIN " . $GLOBALS['ecs']->table('order_goods') . " as og ON oi.order_id = og.order_id " . 
                            "WHERE og.goods_id IN ({$param['bought_goods']}) " . order_query_sql('finished', 'oi.') .
                        ")");
        }
        if ($param['have_email'])
        {
            $ex_where = $this->concatenateWhereClause($ex_where, "u.`email` != '' AND u.`email` NOT REGEXP '^\\\\+?[0-9-]+@yohohongkong\\\\.com$' ");
        }

        // Get expected total size
        $count_sql = "select count(*) FROM " . $GLOBALS['ecs']->table('users') . " as u " .
        "LEFT JOIN " . $GLOBALS['ecs']->table('reg_extend_info') . " as rei ON rei.user_id = u.user_id AND rei.reg_field_id = 11 " .
        "LEFT JOIN " . $GLOBALS['ecs']->table('reg_extend_info') . " as rei2 ON rei2.user_id = u.user_id AND rei2.reg_field_id = 12 " . $ex_where;
        $record_count = $db->getOne($count_sql);

        // crunching query
        $limit = 5000;
        $user_list = [];
        for($alread_get = 0; $alread_get < $record_count; $alread_get+=$limit){
            $sql = "SELECT u.user_id, u.user_name, u.email, u.is_validated, u.info_completed, u.user_money, u.frozen_money, u.rank_points, u.pay_points, u.reg_time, u.user_rank, u.last_login, u.visit_count, u.language, u.birthday, rei.content as live_area, rei2.content as work_area, u.sex, u.is_wholesale " .
                "FROM " . $GLOBALS['ecs']->table('users') . " as u " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('reg_extend_info') . " as rei ON rei.user_id = u.user_id AND rei.reg_field_id = 11 " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('reg_extend_info') . " as rei2 ON rei2.user_id = u.user_id AND rei2.reg_field_id = 12 " .
                $ex_where .
                " LIMIT " . $alread_get . ", " . $limit;
            $user_list_temp = $GLOBALS['db']->getAll($sql);
            foreach($user_list_temp as $temp){
                $user_list[] = $temp;
            }
        }

        // Build filtered user list
        $count = count($user_list);
        
        for ($i=0; $i<$count; $i++)
        {
            $user_list[$i]['reg_time'] = local_date($GLOBALS['_CFG']['date_format'], $user_list[$i]['reg_time']);
            $user_list[$i]['user_rank'] = $this->get_user_rankname($user_list[$i]['user_rank'],$user_list[$i]['rank_points']);
            $user_list[$i]['last_login'] = ($user_list[$i]['last_login']) ? local_date($GLOBALS['_CFG']['time_format'], $user_list[$i]['last_login']) :  '從未登入';
        }

        return $user_list;
    }

    private function saveUserListQuery($userList, $fileName)
    {
        global $_LANG;

        require_once(ROOT_PATH . 'includes/php-xlsx-writer/xlsxwriter.class.php');

        $writer = new \XLSXWriter();

        $sheetHeader = [
            ecs_iconv(EC_CHARSET, 'UTF8', "編號") => 'string',
            ecs_iconv(EC_CHARSET, 'UTF8', "會員名稱") => 'string',
            ecs_iconv(EC_CHARSET, 'UTF8', "會員等級") => 'string',
            ecs_iconv(EC_CHARSET, 'UTF8', "電郵地址") => 'string',
            ecs_iconv(EC_CHARSET, 'UTF8', "是否已驗證") => 'string',
            ecs_iconv(EC_CHARSET, 'UTF8', "已填資料") => 'string',
            ecs_iconv(EC_CHARSET, 'UTF8', "等級積分") => 'string',
            ecs_iconv(EC_CHARSET, 'UTF8', "消費積分") => 'string',
            ecs_iconv(EC_CHARSET, 'UTF8', "註冊日期") => 'string',
            ecs_iconv(EC_CHARSET, 'UTF8', "最後登入") => 'string',
            ecs_iconv(EC_CHARSET, 'UTF8', "瀏覽次數") => 'string'
        ];

        $writer->writeSheetHeader('Sheet1', $sheetHeader);

        foreach($userList as $index => $userListItem) {
            $item = [
                $userListItem['user_id'],
                $userListItem['user_name'],
                $userListItem['user_rank'],
                $userListItem['email'],
                $userListItem['is_validated'],
                $userListItem['info_completed'],
                $userListItem['rank_points'],
                $userListItem['pay_points'],
                $userListItem['reg_time'],
                $userListItem['last_login'],
                $userListItem['visit_count']
            ];

            $writer->writeSheetRow('Sheet1', $item);
        }

        header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.‌​sheet; charset=utf-8');
        header('Content-Disposition: attachment; filename=' . $fileName);
        header('Cache-Control: max-age=0');

        $writer->writeToFile(ROOT_PATH . 'data/export_reports/' . $fileName);
    }

    private function concatenateWhereClause($originalString, $incomingString)
    {
        $returnString = $originalString;
        if (empty($returnString)) {
            $returnString = " WHERE " . $incomingString;
        } else {
            if (strpos(strtolower($returnString), "where") > 0 ) {
                $returnString .= " AND " . $incomingString;
            } else {
                throw \Exception();
            }
        }

        return $returnString;
    }

    private function get_user_rankname($user_rank, $rank_points)
    {
        if (empty($this->user_ranks)) {
            $this->user_ranks = $this->get_all_user_ranks;
        }

        if ($user_rank > 0)
        {
            foreach ($this->user_ranks as $rank)
            {
                if ($rank['rank_id'] == $user_rank)
                {
                    return $rank['rank_name'];
                }
            }
        }
        else
        {
            $max_rank = '';
            $max_mp = 0;
            foreach ($this->user_ranks as $rank)
            {
                if (($rank['min_points'] <= $rank_points) && ($rank['min_points'] >= $max_mp) && $rank['special_rank'] == 0)
                {
                    $max_rank = $rank['rank_name'];
                    $max_mp = $rank['min_points'];
                }
            }
            if (!empty($max_rank))
            {
                return $max_rank;
            }
        }
        
        return $GLOBALS['_LANG']['undifine_rank'];
    }

    private function get_all_user_ranks()
    {
        static $all_ranks = null;
        if ($all_ranks === null)
        {
            $sql = "SELECT rank_id, rank_name, min_points, special_rank " .
                   " FROM " . $GLOBALS['ecs']->table('user_rank') .
                   " ORDER BY min_points ASC";
            $all_ranks = $GLOBALS['db']->getAll($sql);
            if ($all_ranks === false) $all_ranks = array();
        }
        return $all_ranks;
    }
}

?>