<?php

/***
* ctrl_role.php
* by MichaelHui 20170328
*
* Role controller
***/

namespace Yoho\cms\Controller;

if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}


class RoleController extends YohoBaseController{
	const FILE_TYPE_IMAGE='image/*';

	/* 获取角色列表 */
	public function get_role_list(){
		$list = array();
		$sql  = 'SELECT * '.
		'FROM ' .$GLOBALS['ecs']->table('role').' ORDER BY role_id DESC';
		$list = $GLOBALS['db']->getAll($sql);

		foreach ($list as $key => $row) {
			$list[$key]['_action'] = $this->generateCrudActionBtn($row['role_id'], 'id', null, ['edit', 'remove']);
		}

		return $list;
	}

	public function getRoleIdAutocreatesp(){
		$q = "SELECT role_id FROM ".$this->ecs->table('role')." WHERE is_autocreate_salesperson = TRUE";
		$res = $this->db->getAll($q);
		$out = [];
		foreach($res as $r)
			$out[]=$r['role_id'];
		
		return $out;
	}

	public function updateSalespersonStatus($roleId,$autoCreate){
		if(intval($roleId)<=0) return false;
		if(!is_bool($autoCreate)) return false;

		$q = "SELECT u.user_id,sp.sales_id FROM ".$this->ecs->table('admin_user')." u LEFT JOIN ".$this->ecs->table('salesperson')." sp ON sp.admin_id=u.user_id WHERE u.role_id = ".$roleId;
		$res = $this->db->getAll($q);
		$spController = new SalespersonController();
		foreach ($res as $u) {
			if($autoCreate==true){
				if(!$u['sales_id']){
					$adminUser = new \Yoho\cms\Model\Adminuser($u['user_id']);
					$spController->createFromAdmin($adminUser);
				}else{
					$spController->activate($u['sales_id']);
				}
			}else{
				if($u['sales_id'])
					$spController->deactivate($u['sales_id']);
			}
			
		}
		return true;
	}
}