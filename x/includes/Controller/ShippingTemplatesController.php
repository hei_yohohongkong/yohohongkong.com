<?php

/***
* ShippingTemplatesController.php
* by Dem 20171026
*
* ShippingTemplates controller
***/

namespace Yoho\cms\Controller;
use Yoho\cms\Model;
if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

class ShippingTemplatesController extends YohoBaseController{
    
	public function __construct(){
        parent::__construct();
	}
    
    function get_shipping_templates()
    {
        $_REQUEST['sort_by']      = empty($_REQUEST['sort_by']) ? 'template_id' : trim($_REQUEST['sort_by']);
        $_REQUEST['sort_order']   = empty($_REQUEST['sort_order']) ? 'ASC' : trim($_REQUEST['sort_order']);

        $sql = "SELECT count(*) FROM " . $GLOBALS['ecs']->table('shipping_templates');
        $_REQUEST['record_count'] = $GLOBALS['db']->getOne($sql);

        /* 查询 */
        $sql = "SELECT wt.*, " .
            "(SELECT count(*) FROM " . $GLOBALS['ecs']->table('goods') . " as g WHERE g.`shipping_template_id` = wt.`template_id`) as goods_count " .
            "FROM " . $GLOBALS['ecs']->table('shipping_templates') . " as wt " .
            "ORDER BY " . $_REQUEST['sort_by'] . " " . $_REQUEST['sort_order'] . " " .
            "LIMIT " . $_REQUEST['start'] . "," . $_REQUEST['page_size'];
        $data = $GLOBALS['db']->getAll($sql);

        // Multiple language support
        if (!empty($_REQUEST['edit_lang']))
        {
            $data = localize_db_result_lang($_REQUEST['edit_lang'], 'shipping_templates', $data);
        }

        foreach ($data as $key => $row)
        {
            $data[$key]['start_date'] = local_date($GLOBALS['_CFG']['time_format'], $row['start_time']);
            $data[$key]['end_date'] = local_date($GLOBALS['_CFG']['time_format'], $row['end_time']);
            $data[$key]['active'] = ((gmtime() >= $row['start_time']) && (gmtime() <= $row['end_time']));
        }
        $arr = array('data' => $data, '_REQUEST' => $_REQUEST, 'page_count' => $_REQUEST['page_count'], 'record_count' => $_REQUEST['record_count']);

        return $arr;
    }

    function get_shipping_template($id)
    {
        $get_template = new Model\ShippingTemplates($id);
        $template     = $get_template->data;
        if(!$template) return false;

        $template['template_id'] = $id;
        return $template;
    }

    function insert_update_shipping_template($request){
        $data = array(
            'template_name' => $request['template_name'],
            'template_content' => stripslashes($request['template_content'])
        );

        $localized_versions = stripslashes(str_replace(array('\n','\r\n'),'',$request['localized_versions']));
        // Empty multilanguage handling
        if(strlen($localized_versions) == 2) return false;

        $data_lang = array(
            'localized_versions' => $localized_versions
        );
        if(empty($request['template_id']))
        {
            $new_template = new Model\ShippingTemplates(null, $data);
            if($new_template->getId()){
                $template = new Model\ShippingTemplates($new_template->getId());
                if($template && $template->update_lang($data_lang))
                    return true;
                else
                    return false;
            }
            else return false;
        }
        else
        {
            $template = new Model\ShippingTemplates($request['template_id']);
            if($template && $template->update($data) && $template->update_lang($data_lang))return true;
            else return false;
        }
    }

    public function buildForm($formMapper = [], $id = 0, $act = '', $assign = [], $redirect = '')
    {
        if(empty($formMapper))
        $formMapper = [
            'template_name' => ['required' => true],
            'template_content' => ['type' => 'editor'],
            'template_id' => ['type' => 'hidden']
        ];
        parent::buildForm($formMapper, $id, $act, $assign, $redirect);
    }

}
