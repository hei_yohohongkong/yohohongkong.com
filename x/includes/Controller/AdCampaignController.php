<?php

/***
* AdCampaignController.php
* by Anthony 20170620
*
* AdCampaign controller
***/

namespace Yoho\cms\Controller;
use Yoho\cms\Model;
if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

class AdCampaignController extends YohoBaseController{
    
    const DB_ACTION_CODE_AD_MANAGE = 'ad_manage';
    private $tablename = 'ad_campaign';
    
	public function __construct(){
		parent::__construct();

	}
    
    function get_campaign_list($lang = null)
    {
        global $db, $ecs;

		$filter['sort_by'] = empty($_REQUEST['sort_by']) ? 'created_at' : trim($_REQUEST['sort_by']);
		$filter['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
		$sql = "SELECT count(*) FROM " . $this->ecs->table($this->tablename);
		$filter['record_count'] = $db->getOne($sql);
        
        /* Paging */
		$filter = page_and_size($filter);
        
        /* Query list */
        $list = array();
        $sql  = "SELECT c.*, IFNULL(SUM(a.click_count), 0) as click_count, SUM(o.ad_stats) AS ad_stats ".
        "FROM ".$this->ecs->table($this->tablename)." as c ".
        "LEFT JOIN ".$this->ecs->table('ad')." as a ON c.ad_campaign_id = a.ad_campaign_id ".
        'LEFT JOIN ( SELECT from_ad, COUNT(order_id) AS ad_stats FROM '.$this->ecs->table('order_info'). " GROUP BY from_ad ) AS o ON o.from_ad = a.ad_id ".
        "GROUP BY c.ad_campaign_id ".
        "ORDER BY " . $filter['sort_by'] . ' ' .
        $filter['sort_order'] . ' ' .
        "LIMIT " . $filter['start'] . "," . $filter['page_size'];

        $list = $this->db->getAll($sql);
        foreach ($list as $key => $row) {
            $list[$key]['start_date']    = local_date($GLOBALS['_CFG']['time_format'], $row['start_time']);
            $list[$key]['end_date']      = local_date($GLOBALS['_CFG']['time_format'], $row['end_time']);
            $list[$key]['after_start']   = (gmtime() >= $row['start_time']);
            $list[$key]['before_end']    = (gmtime() <= $row['end_time']);
		}
        
        $arr = array(
            'data'         => $list,
            'filter'       => $filter,
            'page_count'   => $filter['page_count'],
            'record_count' => $filter['record_count']
        );
        
		return $arr;

    }
    
    function insert_update_campaign($request)
    {
        /* Create new ad campaign */
        if(empty($request['id']))
        {
            $data = array(
                'ad_campaign_name' => $request['ad_campaign_name'],
                'ad_campaign_desc' => $request['ad_campaign_desc'],
                'start_time'       => local_strtotime($request['start_time']),
                'end_time'         => local_strtotime($request['end_time']),
                'enabled'          => $request['enabled'],
                'created_at'       => date("Y-m-d H:i:s")
            );

            $new_campaign = new Model\AdCampaign(null, $data);
            if($new_campaign->getId())return true;
            else return false;
        }
        /* Update ad campaign */
        else
        {
            $data = array(
                'ad_campaign_name' => $request['ad_campaign_name'],
                'ad_campaign_desc' => $request['ad_campaign_desc'],
                'start_time'       => local_strtotime($request['start_time']),
                'end_time'         => local_strtotime($request['end_time']),
                'enabled'          => $request['enabled']
            );

            $campaign = new Model\AdCampaign($request['id']);
            if($campaign && $campaign->update($data))return true;
            else return false;
        }
    }
    
    function get_enabled_list()
    {
        $sql = "SELECT ad_campaign_id, ad_campaign_name FROM ".$this->ecs->table($this->tablename).
        "WHERE enabled = 1 ";
        
        return $this->db->getAll($sql);
    }

    function get_campaign($id)
    {
        $get_campaign = new Model\AdCampaign($id);
        $campaign     = $get_campaign->data;
        if(!$campaign) return false;
        /* Format data */
        $campaign['ad_campaign_id'] = $id;
        $campaign['end_time']   = local_date($GLOBALS['_CFG']['time_format'], $campaign['end_time']);
        $campaign['start_time'] = local_date($GLOBALS['_CFG']['time_format'], $campaign['start_time']);

        return $campaign;

    }

}
