<?php

/***
 * WarehouseStockController.php
 *
 ***/

namespace Yoho\cms\Controller;

use Yoho\cms\Model;

if (!defined('IN_ECS')) {
    die('Hacking attempt');
}

class WarehouseStockController extends YohoBaseController
{

    public function get_sale_warehouse($is_pagination = false, $admin = 0, $house = false)
    {
        //ini_set('memory_limit', '1024M');
        ini_set('memory_limit', '2048M');
        require_once(ROOT_PATH . 'includes/lib_order.php');
        $userController = new UserController();
        $erpController = new ErpController();
        $orderController = new OrderController();
        $month_start = local_date('Y-m-01 00:00:00');
        $month_end = local_date('Y-m-t 23:59:59');
        set_time_limit(0);

        //FOR TESTING
        //$house = 1;
        //$_REQUEST['start_date'] = "2019-05-06";
        //$_REQUEST['end_date'] = "2019-05-15";

        $filter['start_date'] = empty($_REQUEST['start_date']) ? local_strtotime($month_start) : local_strtotime($_REQUEST['start_date']);
        $filter['end_date'] = empty($_REQUEST['end_date']) ? local_strtotime($month_end) : local_strtotime($_REQUEST['end_date']);
        $days = $GLOBALS['db']->getOne("SELECT COUNT(DISTINCT report_date) FROM " . $GLOBALS['ecs']->table('stock_cost_warehouse_log') . " WHERE report_date >= " . $filter['start_date'] . " AND report_date <= " . ($filter['end_date'] + 86400));
                
        $filter['order_type'] = empty($_REQUEST['order_type']) ? 0 : intval($_REQUEST['order_type']);
        $filter['deprecatedflow'] = empty($_REQUEST['deprecatedflow']) ? 0 : intval($_REQUEST['deprecatedflow']);

        $sql = "SELECT COUNT(*) FROM " . $GLOBALS['ecs']->table('brand') . " WHERE is_show = 1";
        $filter['record_count'] = intval($GLOBALS['db']->getOne($sql)) + 1; // + no supplier
        $where = " oi.shipping_time >= '" . $filter['start_date'] . "' AND oi.shipping_time < '" . ($filter['end_date'] + 86400) . "' " . order_query_sql('finished', 'oi.');

        $supplier = -1;
        $filter['supplier'] = empty($_REQUEST['supplier']) ? -1 : $_REQUEST['supplier'];

        $where_supplier_str_arr = $GLOBALS['db']->getAll("SELECT GROUP_CONCAT(goods_id) FROM " . $GLOBALS['ecs']->table('erp_goods_supplier') . " WHERE supplier_id = ".$filter['supplier']." ");
        
        if (isset($where_supplier_str_arr[0]['GROUP_CONCAT(goods_id)'])){
            $where_supplier_str = $where_supplier_str_arr[0]['GROUP_CONCAT(goods_id)'];
            if (substr($where_supplier_str, -1)){
                $where_supplier_str = rtrim($where_supplier_str,',');
            }
        } else {
            $where_supplier_str = -1;
        }

        $where_supplier = (explode(",",$where_supplier_str));

        $text_search = FALSE;
        if (isset($_REQUEST['search_text']) && isset($_REQUEST['search_text_type']) && htmlspecialchars($_REQUEST['search_text']) != ""){
            $filter['search_text'] = htmlspecialchars($_REQUEST['search_text']);
            $text_search = TRUE;
        }

        // Ignore 換貨訂單
        $where .= " AND oi.postscript NOT LIKE '%換貨，%'";

        if ($filter['order_type'] == 4) // 所有訂單
        {
            $where_ws = "(" . str_replace("oi.pay_status  IN ('", "oi.pay_status  IN ('" . PS_UNPAYED . "','", explode('AND', order_query_sql('finished', 'oi.'))[3]);
            $where_ws .= " AND (oi.order_type = '" . $orderController::DB_ORDER_TYPE_WHOLESALE . "' OR (oi.user_id IN (" . implode(',', $userController->getWholesaleUserIds()) . ") AND oi.order_type IS NULL) ) ";
            $where = str_replace("oi.pay_status  IN ('", "( oi.pay_status  IN ('", $where) . " OR " . $where_ws . " ) )";
        } else if ($filter['order_type'] == 1) // 批發訂單
        {
            // Only include orders for the followiung user account: 99999999
            $where = str_replace("oi.pay_status  IN ('", "oi.pay_status  IN ('" . PS_UNPAYED . "','", $where);
            $where .= " AND (oi.order_type = '" . $orderController::DB_ORDER_TYPE_WHOLESALE . "' OR (oi.user_id IN (" . implode(',', $userController->getWholesaleUserIds()) . ") AND oi.order_type IS NULL) ) ";
        } else if ($filter['order_type'] == 2) // 借貨訂單
        {
            // Only include orders for the followiung user accounts: 11111111, 88888888
            $where .= " AND oi.user_id IN (4940,5023) ";
        } else if ($filter['order_type'] == 3) // 代理銷售訂單
        {
            $where .= " AND oi.order_type = '" . $orderController::DB_ORDER_TYPE_SALESAGENT . "' ";
        } else if ($filter['order_type'] == 0) // Normal order
        {
            // Exclude orders for the followiung user accounts: 11111111, 88888888, 99999999
            // Exclude order type sa, ws
            $where .= " AND (oi.order_type NOT IN ('" . $orderController::DB_ORDER_TYPE_SALESAGENT . "','" . $orderController::DB_ORDER_TYPE_WHOLESALE . "') OR (oi.order_type IS NULL AND oi.user_id NOT IN (" . implode(',', $userController->getWholesaleUserIds()) . ")) )";
            $where .= " AND (oi.platform <> 'pos_exhibition') ";
        } else if ($filter['order_type'] == 5) // Exhibition order
        {
            // Exclude orders for the followiung user accounts: 11111111, 88888888, 99999999
            // Exclude order type sa, ws
            $where .= " AND (oi.order_type NOT IN ('" . $orderController::DB_ORDER_TYPE_SALESAGENT . "','" . $orderController::DB_ORDER_TYPE_WHOLESALE . "') OR (oi.order_type IS NULL AND oi.user_id NOT IN (" . implode(',', $userController->getWholesaleUserIds()) . ")) )";
            $where .= " AND (oi.platform = 'pos_exhibition') ";
        }
        
        // Exclude order with sale exclude coupon
        $where .= " AND (c.sale_exclude = 0 OR c.sale_exclude IS NULL) ";
        //if($admin>0)$where_u = " IN (SELECT distinct supplier_id FROM " . $GLOBALS['ecs']->table('erp_supplier_admin') . " WHERE admin_id = " .$admin . ")";
        /* 分页大小 */
        $filter = page_and_size($filter);

        $sql_2 = "SELECT g.goods_id, g.goods_name, " .
        //"IFNULL(SUM(sc.cost),0) as total_cost, " .
        "IFNULL(SUM(sc.consumed_qty),0) as total_sold_qty," .
        //"IFNULL(SUM(sc.revenue-sc.total_discount),0) as total_revenue," .
        "IFNULL(sum(sc.sap-sc.total_discount),0) as total_sa_revenue, " .
        //"IFNULL(sum(sc.total_discount),0) as total_discount, " .
        //"IFNULL(SUM(sc.revenue - sc.total_discount - sc.cost),0) as total_profit, " .
        "IFNULL(sum(sc.sap - sc.total_discount - sc.cost),0) as total_sa_profit " .
        "FROM " . $GLOBALS['ecs']->table('goods') . " AS g " .
        "LEFT JOIN (" .
        "SELECT ogsc.*, (oi2.total_o_ds/oi2.total_og_price)*(og.goods_price * IFNULL(ogsc.consumed_qty,og.delivery_qty)) as total_discount ," .
        "((ogsc.po_unit_cost + " .
        "IF(oi.`pay_id` = 2,  ogsc.po_unit_cost * 0.0095,       0) + " . // EPS: 0.95% fee
        "IF(oi.`pay_id` = 7,  ogsc.po_unit_cost * 0.032 + 2.35, 0) + " . // PayPal: 3.2% fee + $2.35 per txn
        "IF(oi.`pay_id` = 10, ogsc.po_unit_cost * 0.029,        0) + " . // UnionPay: 2.9% fee
        "IF(oi.`pay_id` = 12, ogsc.po_unit_cost * 0.023,        0) + " . // Visa / MasterCard: 2.3% fee
        "IF(oi.`pay_id` = 13, ogsc.po_unit_cost * 0.0296,       0)   " . // American Express: 2.96% fee
        ") * IFNULL(ogsc.consumed_qty,og.delivery_qty) + " .
        "IF(oi.`shipping_id` = 3, 30 * og.goods_price * IFNULL(ogsc.consumed_qty,og.delivery_qty) / oi.goods_amount, 0) " . // Delivery cost: $30
        ") as cost," .
        "( (( ((100 - IF(og.sa_rate_id > 0 , sa.sa_rate, 0) ) /100 ) * og.goods_price)*IFNULL(ogsc.consumed_qty,og.delivery_qty)) - IF(oi.`pay_fee` < 0, oi.`pay_fee` * -1 * og.goods_price * IFNULL(ogsc.consumed_qty,og.delivery_qty) / oi.goods_amount, 0) ) as sap, " .
        "(og.goods_price * IFNULL(ogsc.consumed_qty,og.delivery_qty) - IF(oi.`pay_fee` < 0, oi.`pay_fee` * -1 * og.goods_price * IFNULL(ogsc.consumed_qty,og.delivery_qty) / oi.goods_amount, 0)) as revenue " .
        "FROM " . $GLOBALS['ecs']->table('order_goods_supplier_consumption') . " as ogsc " .
        "LEFT JOIN " . $GLOBALS['ecs']->table('delivery_goods') . "   as dg ON dg.order_item_id = ogsc.order_goods_id " .
        "LEFT JOIN " . $GLOBALS['ecs']->table('order_goods') . "      as og ON og.rec_id = ogsc.order_goods_id " .
        "LEFT JOIN " . $GLOBALS['ecs']->table('delivery_order') . "   as do ON do.delivery_id = dg.delivery_id " .
        "LEFT JOIN " . $GLOBALS['ecs']->table('erp_supplier') . "     as es ON ogsc.supplier_id = es.supplier_id " .
        "LEFT JOIN " . $GLOBALS['ecs']->table('order_info') . "       as oi ON oi.order_id = og.order_id " .
        "LEFT JOIN " . $GLOBALS['ecs']->table('sales_agent_rate') . " as sa ON sa.sa_rate_id = og.sa_rate_id " .
        "LEFT JOIN (SELECT (oi.`discount` + oi.`bonus` + oi.`coupon`)as total_o_ds, sum(og.goods_number * og.goods_price)as total_og_price, oi.order_id from " . $GLOBALS['ecs']->table('order_info') . " as oi left join " . $GLOBALS['ecs']->table('order_goods') . " AS og ON oi.order_id = og.order_id group by oi.order_id) as oi2 ON oi2.order_id = og.order_id " .
        'LEFT JOIN ' . $GLOBALS['ecs']->table('order_coupons') . ' AS oc ON oc.order_id = oi.order_id ' .
        'LEFT JOIN ' . $GLOBALS['ecs']->table('coupons') . ' AS c ON c.coupon_id = oc.coupon_id ' .
        "WHERE $where" . "  " ;
        if ($house == 1){
            $sql_2.= "AND (dg.warehouse_id = 1 OR dg.warehouse_id IS NULL) ";
        } else {
            $sql_2.= "AND dg.warehouse_id = ".$house." ";
        }
        $sql_2 .= "GROUP BY ogsc.consumption_id) as sc ON sc.goods_id = g.goods_id " .
        "LEFT JOIN " . $GLOBALS['ecs']->table('order_goods') . "      as og ON og.rec_id = sc.order_goods_id " .
        "LEFT JOIN " . $GLOBALS['ecs']->table('order_info') . "       as oi ON oi.order_id = og.order_id " .
        "LEFT JOIN " . $GLOBALS['ecs']->table('delivery_goods') . "   as dg ON dg.order_item_id = sc.order_goods_id " .
        "LEFT JOIN " . $GLOBALS['ecs']->table('sales_agent_rate') . " as sa ON sa.sa_rate_id = og.sa_rate_id ";

 
        if (isset($_REQUEST['on_sale_only']) && $_REQUEST['on_sale_only']){
            $sql_2 .= "WHERE g.is_on_sale = 1 ";
        } else {
            $sql_2 .= "WHERE 1 ";
        }

        $sql_2 .= "AND g.is_delete = 0 AND g.is_real = 1 ";

        if ($house == 1){
            $sql_2.= "AND (dg.warehouse_id = 1 OR dg.warehouse_id IS NULL) ";
        } else {
            $sql_2.= "AND dg.warehouse_id = ".$house." ";
        }

        if ($filter['supplier'] != -1 && $where_supplier_str){
            $sql_2.= "AND g.goods_id IN (".$where_supplier_str.") ";
        }

        if ($text_search){
            $sql_2.= "AND LOWER(g.".$_REQUEST['search_text_type'].") LIKE '%".$_REQUEST['search_text']."%' ";
        }

        $sql_2 .= " GROUP BY g.goods_id DESC";

        $res = $GLOBALS['db']->getAll($sql_2);

        $good_list_sql = "SELECT g.goods_id, g.goods_name, ".
        "IF(g.goods_length > 0, g.goods_length, IF(c.default_length > 0, c.default_length,pc.default_length)) AS good_length, ".
        "IF(g.goods_width > 0, g.goods_width, IF(c.default_width > 0, c.default_width,pc.default_width)) AS good_width, ".
        "IF(g.goods_height > 0, g.goods_height, IF(c.default_height > 0, c.default_height,pc.default_height)) AS good_height ".
        "FROM " . $GLOBALS['ecs']->table('goods') . "AS g ".
        "LEFT JOIN ".$GLOBALS['ecs']->table('category')." AS c ON c.cat_id = g.cat_id ".
        "LEFT JOIN ".$GLOBALS['ecs']->table('category')." AS pc ON pc.cat_id = c.parent_id ";
        if ($house && isset($_REQUEST['on_sale_only']) && $_REQUEST['on_sale_only']){
            $good_list_sql .= " WHERE g.is_on_sale = 1 ";
        } else {
            $good_list_sql .= " WHERE 1 ";
        }
        $good_list_sql .= "AND g.is_delete = 0 AND g.is_real = 1 ";

        $good_list = $GLOBALS['db']->getAll($good_list_sql);

        $good_volume = array();
        foreach ($good_list as $good){
            if ($good['good_length'] > 0 && $good['good_width'] > 0 && $good['good_height'] > 0){
                $lwh = ($good['good_length']/100) * ($good['good_width']/100) * ($good['good_height']/100);
                $good_volume[$good['goods_id']] = $lwh;
            }
        }

        $sql_avg_cost = "SELECT data_str FROM " . $GLOBALS['ecs']->table('stock_cost_warehouse_log') . " WHERE report_date >= " . $filter['start_date'] .
        " AND report_date <= " . ($filter['end_date'] + 86400) . " AND created_at IN (SELECT MAX(created_at) " .
        "FROM " . $GLOBALS['ecs']->table('stock_cost_warehouse_log') . " GROUP BY report_date) ORDER BY created_at DESC";
        $avg_cost_str_arr = $GLOBALS['db']->getAll($sql_avg_cost);

        $avg_cost = [];
        $avg_volume = [];

        $merged_arr = [];

        foreach ($avg_cost_str_arr as $v){
            if (isset($v['data_str']) && $v['data_str'] != ""){
                $temp_arr = unserialize($v['data_str']);
                if (isset($temp_arr[$house])){
                    //$id = 0;
                    foreach ($temp_arr[$house] as $gid => $good){
                    //$log_arr = [];
                    //$log_arr['good_id'] = $gid;
                    //$log_arr['avg_stock_cost'] = $good['c'];
                    //$log_arr['sum_volume'] = $good['v'];

                    /*
                    if (sizeof($merged_arr[$gid]) ==  0){
                        $merged_arr[$gid] = [];
                    }
                    */
                    $real_time_volume = isset($good_volume[$gid])?$good_volume[$gid]:0;
                    //$day_sum_volume = $good['v']>0?$good['v']:($real_time_volume>0?$real_time_volume*$good['q']:0);
                    $day_sum_volume = ($real_time_volume*$good['q']);

                    if (!isset($merged_arr[$gid])){

                        $merged_arr[$gid] = array("goods_id" => $gid, "sum_stock_cost" => $good['c'], "sum_volume" => $day_sum_volume);
                    } else {
                        $merged_arr[$gid]["sum_stock_cost"] += $good['c'];
                        $merged_arr[$gid]["sum_volume"] += $day_sum_volume;
                    }

                    //array_push($merged_arr[$gid], $log_arr);
                    }
                }
                unset($temp_arr);
            }
            //$i++;
        }
        unset($avg_cost_str_arr);

        /*
        foreach ($merged_arr as $k => $v){
            $avg_cost[$k] = [];
            $temp_sum_cost = 0;
            $temp_sum_volume = 0;
            foreach ($v as $log){
                $temp_sum_cost += $log['avg_stock_cost'];
                $temp_sum_volume += $log['sum_volume'];
            }
            $avg_cost[$k]['goods_id'] = $k;
            $avg_cost[$k]['avg_stock_cost'] = $temp_sum_cost/$days;
            $sum_volume[$k] = $temp_sum_volume/$days;
        }
        */

        /*
        $sql_dead_cost = "SELECT type_id AS supplier_id, SUM(slowsoldcost_90) / $days AS avg_dead_cost FROM " . $GLOBALS['ecs']->table('stock_cost_warehouse_log') . " WHERE report_date >= " . $filter['start_date'] .
        " AND report_date <= " . ($filter['end_date'] + 86400) . " AND created_at IN (SELECT MAX(created_at) FROM " . $GLOBALS['ecs']->table('stock_cost_warehouse_log') . " GROUP BY report_date) AND type = 3 ".($admin > 0 ? " AND type_id " . $where_u : '')." AND report_date > 1527091200 GROUP BY supplier_id";
        */
        //$dead_cost = $GLOBALS['db']->getAll($sql_dead_cost);
        
        $data = array();
        $all = array(
            'name' => '所有貨品',
            'display' => isset($_REQUEST['order_type']) ? $_REQUEST['order_type'] : 4,
        );
        $cost_arr = array();
        $avg_cost_arr = array();

        /* 存貨成本 */
        require_once ROOT_PATH . 'includes/lib_howang.php';

        $sql_warehouse_list = 
        "SELECT g.goods_id AS good_id, g.cost AS good_cost, gas.goods_qty AS good_qty, gas.warehouse_id AS warehouse_id ".
        "FROM ".$GLOBALS['ecs']->table('erp_goods_attr_stock')."AS gas ".
        "LEFT JOIN ".$GLOBALS['ecs']->table('goods')." AS g ON g.goods_id = gas.goods_id ".
        "WHERE gas.warehouse_id = ".$house." ";
        if (isset($_REQUEST['on_sale_only']) && $_REQUEST['on_sale_only']){
            $sql_warehouse_list .= " AND g.is_on_sale = 1 ";
        }
        if ($filter['supplier'] != -1){
            $sql_warehouse_list.= "AND g.goods_id IN (".$where_supplier_str.") ";
        }
        if ($text_search){
            $sql_warehouse_list.= "AND g.".$_REQUEST['search_text_type']." LIKE '%".$_REQUEST['search_text']."%' ";
        }
        $sql_warehouse_list .= "ORDER BY g.goods_id DESC, gas.warehouse_id ASC";
    
        $po_list = $GLOBALS['db']->getAll($sql_warehouse_list);

        $goods_id = 0;

        foreach ($po_list as $key => $po) {
            $temp_arr = [];
            if($next_goods && $po['goods_id'] == $goods_id)continue;
        
            // What goods we processing
            //$next_goods = false;
            $qty = $po['good_qty'];
            $sum_cost = $po['good_cost'] * $qty;
        
            $cost_arr[$po['good_id']]['cost'] = number_format(isset($cost_arr[$po['good_id']]['cost']) ? $cost_arr[$po['good_id']]['cost']+= $sum_cost: $sum_cost, 2, '.', '');
            $cost_arr[$po['good_id']]['qty']  = (isset($cost_arr[$po['good_id']]['qty']) ? $cost_arr[$po['good_id']]['qty']+= $qty: $qty);
            //foreach (array_keys($dead_arr) as $day_key){
            //    if (!empty($dead_arr[$day_key][$goods_id])) {
            //        $cost_arr[$po['supplier_id']]['dead_cost_'.$day_key] = isset($cost_arr[$po['supplier_id']]['dead_cost_'.$day_key]) ? $cost_arr[$po['supplier_id']]['dead_cost_'.$day_key]+= $cost: $cost;
            //    }
            //}
        }

        $data['all'] = $all;

        foreach ($merged_arr as $row) {
            $avg_cost_arr[$row['goods_id']] = $row['sum_stock_cost']/$days;
            $avg_volume[$row['goods_id']] = $row['sum_volume']/$days;
        }

        /*
        foreach ($dead_cost as $row) {
            $avg_dead_arr[$row['supplier_id']] = $row['avg_dead_cost'];
        }
        */

        foreach ($res as $row) {
            /*
            if ($text_search){
                if (strpos(strtolower(strval($row[$_REQUEST['search_text_type']])), strtolower(strval($_REQUEST['search_text']))) === false && strtolower(strval($row[$_REQUEST['search_text_type']])) != strtolower(strval($_REQUEST['search_text']))){
                continue;
                }
            }
            */

            $curr_avg_cost = isset($avg_cost_arr[$row['goods_id']]) ? $avg_cost_arr[$row['goods_id']] : 0;

            if ($curr_avg_cost == 0 && (!isset($row['total_sa_revenue']) || !(int)$row['total_sa_revenue'])) {
                continue;
            }

            $item = isset($data[$row['goods_id']]) ? $data[$row['goods_id']] : array();

            $curr_cost = isset($cost_arr[$row['goods_id']]['cost']) ? $cost_arr[$row['goods_id']]['cost'] : 0;
            $curr_qty = isset($cost_arr[$row['goods_id']]['qty']) ? $cost_arr[$row['goods_id']]['qty'] : 0;
            //$curr_dead_cost = isset($avg_dead_arr[$row['goods_id']]) ? $avg_dead_arr[$row['goods_id']] : 0;
            $volume = isset($avg_volume[$row['goods_id']]) ? $avg_volume[$row['goods_id']] : 0;


            //if ((!isset($row['total_revenue']) || !(int)$row['total_revenue']) && (!isset($row['total_cost']) || !(int)$row['total_cost'])  && (!isset($row['total_profit']) || !(int)$row['total_profit'])) {
            //    continue;
            //}

            //if ($house) {
            //    if ($curr_cost != 0 || $curr_qty != 0 || $curr_avg_cost != 0 || $curr_dead_cost != 0){
                   //break;
            //    }
            //}
            $item['id'] = $row['goods_id'];
            $item['name'] = $row['goods_name'];
            $item['total_sold_qty'] = (isset($item['total_sold_qty']) ? $item['total_sold_qty'] : 0) + $row['total_sold_qty'];
            $data['all']['total_sold_qty'] += $item['total_sold_qty'];
            //$item['total_cost'] = (isset($item['total_cost']) ? $item['total_cost'] : 0) + $row['total_cost'];
            //$data['all']['total_cost'] += $item['total_cost'];
            //$item['total_cost_formated'] = price_format($item['total_cost'], false);
            $data['all']['total_fifo_cost'] += $curr_cost;
            $item['total_fifo_cost'] = $curr_cost;
            $item['total_fifo_cost_formated'] = price_format($item['total_fifo_cost'], false);
            //$data['all']['total_dead_cost'] += $curr_dead_cost;
            //$item['total_dead_cost'] = $curr_dead_cost;
            //$item['total_dead_cost_formated'] = price_format($item['total_dead_cost'], false);
            $data['all']['total_fifo_qty'] += $curr_qty;
            $item['total_fifo_qty'] = $curr_qty;
            $data['all']['total_volume'] += $volume;
            $item['total_volume'] = $volume;
            $item['total_volume_formated'] = intval($item['total_volume']*1000)/1000;
            $data['all']['avg_stock_cost'] += $curr_avg_cost;
            $item['avg_stock_cost'] = $curr_avg_cost;
            $item['avg_stock_cost_formated'] = price_format($item['avg_stock_cost'], false);
            //$item['total_revenue'] = (isset($item['total_revenue']) ? $item['total_revenue'] : 0) + $row['total_revenue'];
            //$data['all']['total_revenue'] += $item['total_revenue'];
            //$item['total_revenue_formated'] = price_format($item['total_revenue'], false);
            $item['total_gpcost'] = (isset($item['total_gpcost']) ? $item['total_gpcost'] : 0) + $row['total_gpcost'];
            $data['all']['total_gpcost'] += $item['total_gpcost'];
            //$item['total_profit'] = (isset($item['total_profit']) ? $item['total_profit'] : 0) + $row['total_profit'];
            //$data['all']['total_profit'] += $item['total_profit'];
            //$item['total_profit_formated'] = price_format($item['total_profit'], false);
            //$item['profit_margin'] = $item['total_revenue'] > 0 ? ($item['total_profit'] / $item['total_revenue']) : 0;
            //$item['profit_margin_formated'] = number_format($item['profit_margin'] * 100, 2, '.', '') . '%';
            $item['total_sa_revenue'] = (isset($item['total_sa_revenue']) ? $item['total_sa_revenue'] : 0) + $row['total_sa_revenue'];
            $item['total_sa_revenue'] = $item['total_sa_revenue'] == null ? 0 : $item['total_sa_revenue'];
            $data['all']['total_sa_revenue'] += $item['total_sa_revenue'];
            $item['total_sa_revenue_formated'] = price_format($item['total_sa_revenue'], false);
            $item['total_sa_profit'] = (isset($item['total_sa_profit']) ? $item['total_sa_profit'] : 0) + $row['total_sa_profit'];
            $item['total_sa_profit'] = $item['total_sa_profit'] == null ? 0 : $item['total_sa_profit'];
            $data['all']['total_sa_profit'] += $item['total_sa_profit'];
            $item['total_sa_profit_formated'] = price_format($item['total_sa_profit'], false);
            //$item['sa_profit_margin'] = $item['total_sa_revenue'] > 0 ? ($item['total_sa_profit'] / $item['total_sa_revenue']) : 0;
            //$item['sa_profit_margin_formated'] = number_format($item['sa_profit_margin'] * 100, 2, '.', '') . '%';
            //$item['total_dead_cost_rate_formated'] = round(($item['total_dead_cost']/$item['avg_stock_cost']) * 100).'%';
            $item['display'] = isset($_REQUEST['order_type']) ? $_REQUEST['order_type'] : 4;

            $data[$row['goods_id']] = $item;
        }

        foreach ($good_list as $good) {
            if ($filter['supplier'] > 0 && !in_array($good['goods_id'], $where_supplier)){continue;}
            if ($text_search){
                if (strpos(strtolower(strval($good[$_REQUEST['search_text_type']])), strtolower(strval($_REQUEST['search_text']))) === false && strtolower(strval($good[$_REQUEST['search_text_type']])) != strtolower(strval($_REQUEST['search_text']))){
                  continue;
                }
            }
            if (!isset($data[$good['goods_id']])) {
                $curr_avg_cost = isset($avg_cost_arr[$good['goods_id']]) ? $avg_cost_arr[$good['goods_id']] : 0;
                if ($curr_avg_cost == 0 && (!isset($data[$good['goods_id']]['total_sa_revenue']) || !(int)$data[$good['goods_id']]['total_sa_revenue'])) {
                    continue;
                }
                $item = array();
                $curr_cost = isset($cost_arr[$good['goods_id']]['cost']) ? $cost_arr[$good['goods_id']]['cost'] : 0;
                $curr_qty = isset($cost_arr[$good['goods_id']]['qty']) ? $cost_arr[$good['goods_id']]['qty'] : 0;
                $curr_dead_cost = isset($avg_dead_arr[$good['goods_id']]) ? $avg_dead_arr[$good['goods_id']] : 0;
                $volume = isset($avg_volume[$good['goods_id']]) ? $avg_volume[$good['goods_id']] : 0;
                $item['id'] = $good['goods_id'];
                $item['name'] = $good['goods_name'];

                $item['total_sold_qty'] = 0;
                //$item['total_cost'] = 0;
                //$item['total_revenue'] = 0;
                $item['total_gpcost'] = 0;
                //$item['total_profit'] = 0;
                $item['total_fifo_cost'] = $curr_cost;
                $item['total_fifo_qty'] = $curr_qty;
                //$item['profit_margin'] = 0;
                $item['total_sa_revenue'] = 0;
                $item['total_sa_profit'] = 0;
                //$item['sa_profit_margin'] = 0;
                $item['total_volume'] = $volume;
                $item['avg_stock_cost'] = $curr_avg_cost;
                $data['all']['total_fifo_cost'] += $curr_cost;
                $data['all']['total_fifo_qty'] += $curr_qty;
                $data['all']['avg_stock_cost'] += $curr_avg_cost;
                //$data['all']['total_dead_cost'] += $curr_dead_cost;
                $data['all']['total_volume'] += $volume;

                //$item['total_cost_formated'] = price_format($item['total_cost'], false);
                //$item['total_revenue_formated'] = price_format($item['total_revenue'], false);
                //$item['total_profit_formated'] = price_format($item['total_profit'], false);
                //$item['profit_margin_formated'] = number_format($item['profit_margin'] * 100, 2, '.', '') . '%';
                $item['total_sa_revenue_formated'] = price_format($item['total_sa_revenue'], false);
                $item['total_sa_profit_formated'] = price_format($item['total_sa_profit'], false);
                //$item['sa_profit_margin_formated'] = number_format($item['sa_profit_margin'] * 100, 2, '.', '') . '%';
                $item['total_fifo_cost_formated'] = price_format($item['total_fifo_cost'], false);
                $item['avg_stock_cost_formated'] = price_format($item['avg_stock_cost'], false);
                $item['total_volume_formated'] = intval($item['total_volume']*1000)/1000;

                $item['display'] = isset($_REQUEST['order_type']) ? $_REQUEST['order_type'] : 4;

                $data[$good['goods_id']] = $item;
            }
        }

        $data['all']['total_sold_qty'] = $data['all']['total_sold_qty'] > 0 ? $data['all']['total_sold_qty'] : 0;
        $data['all']['total_sa_profit'] = $data['all']['total_sa_profit'] > 0 ? $data['all']['total_sa_profit'] : 0;
        $data['all']['total_sa_revenue'] = $data['all']['total_sa_revenue'] > 0 ? $data['all']['total_sa_revenue'] : 0;
        //$data['all']['profit_margin'] = $data['all']['total_revenue'] > 0 ? ($data['all']['total_profit'] / $data['all']['total_revenue']) : 0;
        //$data['all']['sa_profit_margin'] = $data['all']['total_sa_revenue'] > 0 ? ($data['all']['total_sa_profit'] / $data['all']['total_sa_revenue']) : 0;
        //$data['all']['total_cost_formated'] = price_format($data['all']['total_cost'], false);
        $data['all']['total_fifo_cost_formated'] = price_format($data['all']['total_fifo_cost'], false);
        $data['all']['avg_stock_cost_formated'] = price_format($data['all']['avg_stock_cost'], false); // req
        //$data['all']['total_dead_cost_formated'] = price_format($data['all']['total_dead_cost'], false);
        //$data['all']['total_dead_cost_rate_formated'] = round(($data['all']['total_dead_cost']/$data['all']['avg_stock_cost']) * 100).'%';
        //$data['all']['total_revenue_formated'] = price_format($data['all']['total_revenue'], false);
        //$data['all']['total_profit_formated'] = price_format($data['all']['total_profit'], false);
        //$data['all']['profit_margin_formated'] = number_format($data['all']['profit_margin'] * 100, 2, '.', '') . '%';
        $data['all']['total_sa_revenue_formated'] = price_format($data['all']['total_sa_revenue'], false); // req
        $data['all']['total_sa_profit_formated'] = price_format($data['all']['total_sa_profit'], false); // req
        //$data['all']['sa_profit_margin_formated'] = number_format($data['all']['sa_profit_margin'] * 100, 2, '.', '') . '%';
        $data['all']['total_volume_formated'] = intval($data['all']['total_volume']*1000)/1000;

        if (!$data['all']['total_fifo_qty'])$data['all']['total_fifo_qty']= 0; // req

        foreach ($data as $key => $value) {
            $data[$key]['turnover'] = $data[$key]['total_sa_revenue'] / $data[$key]['avg_stock_cost'] !== false ? number_format($data[$key]['total_sa_revenue'] / $data[$key]['avg_stock_cost'], 3, '.', '') : "--";
            $data[$key]['turnover_day'] = number_format($days/$data[$key]['turnover'], 2);
            // req

            //$data[$key]['inf_turnover'] = $data[$key]['total_cost'] / $data[$key]['avg_stock_cost'] !== false ? number_format($data[$key]['total_cost'] / $data[$key]['avg_stock_cost'], 3, '.', '') : "--";
            //$data[$key]['inf_turnover_day'] = number_format($days/$data[$key]['inf_turnover'], 2);

        }

        $sort_data = array();

        $data['all']['id'] = '';

        array_push($sort_data, $data['all']);

        array_shift($data);

        if (!isset($_REQUEST['sort_by'])){
            //$_REQUEST['sort_by'] = 'inf_turnover';
            $_REQUEST['sort_by'] = 'avg_stock_cost_formated';
        }
        if (!isset($_REQUEST['sort_order'])){
            $_REQUEST['sort_order'] = 'desc';
        }

        usort($data, function ($item1, $item2) {
            $i1 = str_replace("HK$ ", "", $item1[$_REQUEST['sort_by']]);
            $i2 = str_replace("HK$ ", "", $item2[$_REQUEST['sort_by']]);
            if ($i1 == $i2) {
                return 0;
            } else if ($_REQUEST['sort_order'] == 'desc') {
                return $i1 > $i2 ? -1 : 1;
            } else {
                return $i1 < $i2 ? -1 : 1;
            }
        });
        
        $filter['record_count'] = sizeof($data);

        if (isset($_REQUEST['start']) && isset($_REQUEST['page_size'])){
            $data = array_slice($data, $_REQUEST['start'], $_REQUEST['page_size']);
        }

        foreach ($data as $k => $v){
            if ($v['name'] != "所有貨品"){
                array_push($sort_data, $v);
            }
        }
        $arr = array(
            'data' => $sort_data,
            //'total_discount' => $total_discount,
            'filter' => $filter,
            'page_count' => $filter['page_count'],
            'record_count' => $filter['record_count'],
        );

        return $arr;
    }
}
