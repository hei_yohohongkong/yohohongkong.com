<?php

namespace Yoho\cms\Controller;
use Yoho\cms\Model;

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

class KpiController extends YohoBaseController
{
    const KPI_EVALUATE = [ // label => [range = [min, max], color => Hex Code ]
        'pool' => [
            'range' => [0, 2],
            'color' => '#cd0000'
        ],
        'unsatisfactory' => [
            'range' => [3, 4],
            'color' => '#e16666'
        ],
        'average' => [
            'range' => [5, 6],
            'color' => '#e79235'
        ],
        'good' => [
            'range' => [7, 8],
            'color' => '#f2c42f'
        ],
        'outstanding' => [
            'range' => [9, 10],
            'color' => '#94c57e'
        ],
    ];
    function menuAction()
    {
        global $ecs, $db, $smarty;
        $smarty->assign('ur_here', '團隊KPI');
        if ($_SESSION['manage_cost'] || $_SESSION['role_id'] == 4) { //super admin, role_id = tech team, using for testing.
            $title_list = StatisticsController::TEAM_LIST;
            $smarty->assign('can_set_kpi', 1);
            $smarty->assign('action_link2', array('text' => '設定團隊KPI項目', 'href' => 'team_kpi.php?act=build_column'));
        } else {
            $title_list = [];
            $role_id = $_SESSION['role_id'];
            $team_code = $db->getOne('SELECT team_code FROM '.$ecs->table('role') ." WHERE role_id = $role_id ");
            $title_list[$team_code] = StatisticsController::TEAM_LIST[$team_code];
        }
        $year = $this->getNextYear(10, 1);
        $smarty->assign('year', $year);
        $smarty->assign('month', StatisticsController::MONTH);
        $smarty->assign('now_year', local_date('Y'));
        $smarty->assign('now_month', local_date('m'));
        $smarty->assign('title_list', $title_list);
        assign_query_info();
        $smarty->display('team_kpi.htm');
    }

    function buildKpiColumn($team_code = '')
    {
        global $smarty, $ecs, $db, $_LANG;
        if ($_SESSION['manage_cost'] || $_SESSION['role_id'] == 4) { //super admin, *role_id = 4 = tech team, using for testing.
            $title_list = StatisticsController::TEAM_LIST;
            $smarty->assign('team_list', $title_list);
            $sql = 'SELECT * FROM '.$ecs->table('team_kpi_column')." WHERE team_code = '$team_code' AND is_disabled = 0 ";
            $columns = $db->getAll($sql);
            $smarty->assign('team_column_list', $columns);
            assign_query_info();
            $smarty->display('team_kpi_column.htm');
        } else {
            $link[] = array('text' => $_LANG['go_back'], 'href' => 'javascript:history.back(-1)');
            sys_msg('你沒有該權限', 0, $link);
        }
    }

    function updateKpiColumns($data)
    {
        global $ecs, $db, $_LANG;
        $team_code   = $data['team_code'];
        $column_name = $data['column_name'];
        $column_rate = $data['column_rate'];
        $column_reverse = $data['column_reverse'];
        $columns = [];
        $total_rate = 0;
        if(empty($team_code) || empty($column_name) || empty($column_rate)) {
            $link[] = array('text' => $_LANG['go_back'], 'href' => 'javascript:history.back(-1)');
            sys_msg('設定錯誤', 0, $link);
        }
        foreach ($column_name as $key => $column) {
            if($column_rate[$key] == 0) continue;
            $columns[] = [
                'column_name' => trim($column),
                'column_rate' => $column_rate[$key],
                'team_code'   => $team_code,
                'is_reverse'  => $column_reverse[$key],
                'column_code' => strtolower($team_code.'_'.preg_replace("/[^a-zA-Z0-9]+/", "", trim($column)))
            ];
            $total_rate += $column_rate[$key];
        }
        if($total_rate > 10 || $total_rate < 1) {
            $link[] = array('text' => $_LANG['go_back'], 'href' => 'javascript:history.back(-1)');
            sys_msg('比重總值設定錯誤: '.$total_rate, 0, $link);
        }
        $delete = $db->query('UPDATE '.$ecs->table('team_kpi_column')." SET is_disabled = 1 WHERE team_code = '$team_code' AND column_code NOT ".db_create_in(array_column($columns, 'column_code')));
        $insert_sql = 'INSERT INTO '.$ecs->table('team_kpi_column').' (column_code, column_name, column_rate, team_code, is_reverse) VALUES ';
        foreach($columns as $row){
            $valuesArr[] = "('$row[column_code]', '".$this->escape_string($row['column_name'])."', '$row[column_rate]', '$row[team_code]', '$row[is_reverse]')";
        }
        $insert_sql .= implode(',', $valuesArr)." ON DUPLICATE KEY UPDATE
    column_code = VALUES(column_code),column_name = VALUES(column_name),column_rate = VALUES(column_rate),is_reverse = VALUES(is_reverse)";
        $success = $db->query($insert_sql);
        if($success) {
            $link[] = array('text' => '返回列表', 'href' => 'team_kpi.php');
            sys_msg('設定項目成功', 0, $link);
        } else {
            $link[] = array('text' => $_LANG['go_back'], 'href' => 'javascript:history.back(-1)');
            sys_msg('設定錯誤', 0, $link);
        }
    }

    function listAction($assign, $template)
    {
        global $ecs, $db, $smarty;
        $data = $_REQUEST;
        $month_list = StatisticsController::MONTH;
        unset($month_list['Q1'], $month_list['Q2'], $month_list['Q3'], $month_list['Q4']);
        $sql = "SELECT tkc.column_id, tkc.column_name " .
            "FROM " . $ecs->table('team_kpi_column') . " as tkc ";
        $sql .= " WHERE tkc.team_code = '$data[team_code]' AND  tkc.is_disabled = 0 ";
        $tmp_columns = $db->getAll($sql);
        $columns = [];
        $kpi_list = [];
        $ctx_data = [];
        foreach ($tmp_columns as $column) {
            $columns[$column['column_id']] = $column;
        }
        $sql = "SELECT tkr.*, tkc.column_name, tkc.column_rate, tkc.is_reverse FROM " . $ecs->table('team_kpi_record') . " as tkr " .
            " LEFT JOIN " . $ecs->table('team_kpi_column') . " as tkc ON tkc.column_id = tkr.column_id " .
            " WHERE tkc.team_code = '" . $data['team_code'] . "' AND tkr.year = '" . $data['year'] . "'";
        $result = $db->getAll($sql);
        // Group data by month
        foreach ($result as $row) {
            if (empty($kpi_list[$row['month']]['columns'])) {
                $kpi_list[$row['month']]['columns'] = $columns;
            }
            $row['target_format'] = $this->priceToFloat($row['target']);
            $row['value_format']  = $this->priceToFloat($row['value']);
            // Cal this kpi column achieving rate
            if ($row['is_reverse']) {
                $row['achieving_rate'] = ($row['target_format'] / $row['value_format']);
            } else {
                $row['achieving_rate'] = ($row['value_format'] / $row['target_format']);
            }
            if(strpos($row['value'], '$') === false && strpos($row['value'], '%') === false) {
                $row['value'] = number_format($this->priceToFloat($row['value']), 2);
            }
            if(strpos($row['target'], '$') === false && strpos($row['target'], '%') === false) {
                $row['target'] = number_format($this->priceToFloat($row['target']), 2);
            }
            $row['rank_point'] = $row['achieving_rate'] * $row['column_rate'];
            $row['rank_point'] = number_format($row['rank_point'], 2);
            $row['achieving_rate'] = intval($row['achieving_rate'] * 100) . '%';
            // Rank point only can: 0 >= rank point <= column rate
            $row['rank_point'] = max($row['rank_point'], 0);
            $row['rank_point'] = min($row['rank_point'], $row['column_rate']);
            $kpi_list[$row['month']]['columns'][$row['column_id']] = $row;
            $kpi_list[$row['month']]['rate_point'] += $row['rank_point'];
        }
        foreach ($month_list as $month => $month_label) {
            $kpi_list[$month]['label'] = $month_label;
            $kpi_list[$month]['columns'] = empty($kpi_list[$month]['columns']) ? $columns : $kpi_list[$month]['columns'];
            if (empty($kpi_list[$month]['rate_point'])) {
                $kpi_list[$month]['rate_point'] = null;
                $kpi_list[$month]['evaluate']['label'] = 'N/A';
                $kpi_list[$month]['color'] = '';
            } else {
                $kpi_list[$month]['evaluate'] = $this->CalRateEvaluate($kpi_list[$month]['rate_point']);
                $kpi_list[$month]['color'] = $kpi_list[$month]['evaluate']['color'];
            }
        }
        ksort($kpi_list);
        $title = $data['year'] . '年 ' . StatisticsController::TEAM_LIST[$data['team_code']]['role_name'] . ' KPI';
        if ($columns) {
            $ctx_data['labels'] = array_column($kpi_list, 'label');
            $ctx_data['datasets'][] = [
                'label' => $title.' 線狀圖',
                'fill' =>  'false',
//                'borderColor'  =>  '#00acee',
//                'pointBorderWidth'  =>  '10',
//                'pointHoverRadius'  =>  '10',
                'pointHoverBorderWidth'  =>  '1',
//                'pointBorderColor'  =>  array_column($kpi_list, 'color'),
                'pointBackgroundColor'  =>  array_column($kpi_list, 'color'),
//                'pointHoverBackgroundColor'  =>  array_column($kpi_list, 'color'),
//                'pointHoverBorderColor'  =>  array_column($kpi_list, 'color'),
//                'backgroundColor' =>  'rgba(178, 236, 152, 0.3)',
                'data' => array_column($kpi_list, 'rate_point'),
                'type'  => 'line'
            ];
            $ctx_data['datasets'][] = [
                'label' => $title.'',
                'data'  => array_column($kpi_list, 'rate_point'),
                'backgroundColor' =>  array_column($kpi_list, 'color'),
                'type'  => 'bar'
            ];
        }

        $smarty->assign('kpi_list', $kpi_list);
        $smarty->assign('team_code', $data['team_code']);
        $smarty->assign('year', $data['year']);
        $smarty->assign('title', $title);
        $smarty->assign('ctx_data', $ctx_data);
        $smarty->assign('act', $data['act']);
        $smarty->display('library/team_kpi_list.lbi');
    }

    function CalRateEvaluate($rate)
    {
        $ranks = self::KPI_EVALUATE;
        $rate = intval($rate);
        foreach($ranks as $label => $rank) {
            $rank['label'] = ucfirst($label);
            if (filter_var($rate, FILTER_VALIDATE_INT, array("options"=> array("min_range"=>$rank['range'][0], "max_range"=>$rank['range'][1]))) !== false) {
                return $rank;
            }
        }
        return false;
    }


    public function addAction()
    {
        global $smarty, $ecs, $db, $_LANG;
        if (!($_SESSION['manage_cost'])) { //super admin, *role_id = 4 = tech team, using for testing.
            $link[] = array('text' => $_LANG['go_back'], 'href' => 'javascript:history.back(-1)');
            sys_msg('你沒有該權限', 0, $link);
        }
        if(empty($_REQUEST['team'])) {
            $link[] = array('text' => $_LANG['go_back'], 'href' => 'javascript:history.back(-1)');
            sys_msg('你沒有選擇團隊', 0, $link);
        }
        $team_code = $_REQUEST['team'];
        $get_kpi_data = !empty(intval($_REQUEST['year'])) && !empty(intval($_REQUEST['month']));
        $sql = 'SELECT tkc.column_id, tkc.column_name, tkc.column_rate '.($get_kpi_data ? ', tkr.target as column_target, tkr.value as column_value ' : '').
            'FROM '.$ecs->table('team_kpi_column')." as tkc ";
        if($get_kpi_data)
        {
            $sql .= " LEFT JOIN ".$ecs->table('team_kpi_record')." as tkr ON tkr.column_id = tkc.column_id AND year = '".intval($_REQUEST['year'])."' AND month = '".intval($_REQUEST['month'])."'";
        }
        $sql .= " WHERE tkc.team_code = '$team_code' AND  tkc.is_disabled = 0 ";
        $columns = $db->getAll($sql);

        $smarty->assign('team_column_list', $columns);
        $year = $this->getNextYear(3, 1);
        $smarty->assign('year', $year);
        $smarty->assign('type', $_REQUEST['type']);
        $smarty->assign('team', $_REQUEST['team']);
        assign_query_info();
        $smarty->display('team_kpi_set.htm');
    }

    public function insertAction()
    {
        global $smarty, $ecs, $db, $_LANG;
        if (!($_SESSION['manage_cost'])) { //super admin, *role_id = 4 = tech team, using for testing.
            $link[] = array('text' => $_LANG['go_back'], 'href' => 'javascript:history.back(-1)');
            sys_msg('你沒有該權限', 0, $link);
        }
        $team_code = $_REQUEST['team'];
        $year      = intval($_REQUEST['year']);
        $month     = intval($_REQUEST['month']);
        $type      = $_REQUEST['type'];
        $kpi       = $_REQUEST['kpi'];

        if(empty($team_code) || empty($kpi) || (empty($month) && empty($year))) {
            $link[] = array('text' => $_LANG['go_back'], 'href' => 'javascript:history.back(-1)');
            sys_msg('設定錯誤', 0, $link);
        }
        $date = $this->getQarter($year, $month);
        $start_date = local_strtotime($date['start_date']);
        $end_date   = local_strtotime($date['end_date']);
        $insert_sql = 'INSERT INTO '.$ecs->table('team_kpi_record').' (`column_id`, `target`, `value`, `year`, `month`, `start_time`, `end_time`) VALUES ';
        foreach($kpi as $column_id => $row) {
            if(!isset($row['target'])) {
                $link[] = array('text' => $_LANG['go_back'], 'href' => 'javascript:history.back(-1)');
                sys_msg('設定錯誤', 0, $link);
            }
            $row['value']  = isset($row['value']) ? $row['value'] : 0;
            $valuesArr[] = "('$column_id', '$row[target]', '$row[value]', '$year', '$month', '$start_date', '$end_date' )";
        }
        $insert_sql .= implode(',', $valuesArr)." ON DUPLICATE KEY UPDATE
    column_id = VALUES(column_id),target = VALUES(target), value = VALUES(value)";
        $success = $db->query($insert_sql);
        if($success) {
            $link[] = array('text' => '返回列表', 'href' => 'team_kpi.php');
            sys_msg('設定KPI成功', 0, $link);
        } else {
            $link[] = array('text' => $_LANG['go_back'], 'href' => 'javascript:history.back(-1)');
            sys_msg('設定錯誤', 0, $link);
        }
    }
}
