<?php

/***
* CommissionController.php
* by Anthony 20171020
*
* Commission controller
***/

namespace Yoho\cms\Controller;
use Yoho\cms\Model;
if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}
require_once(ROOT_PATH . 'includes/lib_order.php');
class CommissionController extends YohoBaseController{
    
    public function getGoodsBykeyword($key, $arr_where) {
        $erpController = new ErpController();

        global $ecs, $db;
        $sql = "SELECT g.goods_id, g.goods_name, g.shop_price, g.cost, g.goods_sn, IFNULL(SUM(gas.goods_qty), 0) AS stock  ".
                "FROM ".$ecs->table('goods')." AS g ".
                "LEFT JOIN ".$ecs->table('erp_goods_attr_stock')." AS gas ON gas.goods_id = g.goods_id ".
                "WHERE gas.warehouse_id IN (".$erpController->getSellableWarehouseIds(true).") ".
                "AND g.is_real = 1 ".
                "AND g.is_on_sale = 1 ";
        if ($arr_where){
            $sql .= "AND ".implode(" AND ", $arr_where)." ";
        }
        if ($key != ''){
            if (!preg_match('/^([0-9]+,)*[0-9]+$/', $key))
            {
                $sql .= "AND g.goods_name LIKE '%".$key."%' OR g.goods_id LIKE '%".$key."%'  OR g.goods_sn LIKE '%".$key."%' ";
            } else {
                $sql .= "AND g.goods_id IN ({$key}) ";
            }
        }
        $sql .= "GROUP BY g.goods_id ".
                "ORDER BY g.goods_name ASC ";

        $res = $GLOBALS['db']->getAll($sql);

        return $res;
    }
    public function get_sale_commission($is_pagination = true,$start_date,$end_date){
        $end_time = local_strtotime($end_date);
        $start_time = local_strtotime($start_date);
        $orderController = new OrderController();
        $order_type = $orderController::DB_ORDER_TYPE_SALESAGENT;  
        /* 查询数据的条件 */
        $where = order_query_sql('finished', 'oi.') .
               " AND (oi.order_type <> '" . $order_type . "' OR oi.order_type IS NULL) ".
                 " AND oi.shipping_time >= '".$start_time."' AND oi.shipping_time <= '" . ($end_time+86399) . "' ";
        
        $sql = 'SELECT COUNT(*) FROM ( SELECT DISTINCT oi.`salesperson` '.
               'FROM ' . $GLOBALS['ecs']->table('order_info') . ' AS oi '.
               'WHERE 1 ' . $where .
               ') as tmp';
        $filter['record_count'] = $GLOBALS['db']->getOne($sql);
    
        /* 分页大小 */
        $filter = page_and_size($filter);
    
        $sql = '('.
               'SELECT salesperson, COUNT(*) as orders, SUM(total_amount) as volume, SUM(commission + commission_adjust) as commission FROM (' .
                   'SELECT oi.`salesperson`, (oi.`money_paid` + oi.`order_amount`) as total_amount, oi.`commission_adjust`, '.
                   'SUM(IF(RIGHT(g.`commission`, 1) = \'%\', (og.goods_price * og.goods_number - IF(oi.`pay_fee` < 0, oi.`pay_fee` * -1 * og.goods_price * og.goods_number / oi.goods_amount, 0)) * g.commission / 100, g.commission * og.goods_number - IF(oi.`pay_fee` < 0 AND g.commission > 0, oi.`pay_fee` * -1 * og.goods_price * og.goods_number / oi.goods_amount, 0))) as commission '.
                   'FROM ' . $GLOBALS['ecs']->table('order_info') . ' as oi '.
                   'LEFT JOIN ' . $GLOBALS['ecs']->table('order_goods') . ' as og ON oi.`order_id` = og.`order_id` '.
                   'LEFT JOIN ' . $GLOBALS['ecs']->table('goods') . ' as g ON og.`goods_id` = g.`goods_id` '.
                   'WHERE 1 ' . $where .
                   'GROUP BY oi.`order_id` '.
                   ') as tmp '.
               'GROUP BY `salesperson` '.
               'ORDER BY `salesperson` ASC'.
               ') UNION ('.
               'SELECT \'Payable\' as salesperson, COUNT(*) as orders, SUM(total_amount) as volume, SUM(commission + commission_adjust) as commission FROM (' .
               'SELECT oi.`salesperson`, (oi.`money_paid` + oi.`order_amount`) as total_amount, oi.`commission_adjust`, '.
               'SUM(IF(RIGHT(g.`commission`, 1) = \'%\', (og.goods_price * og.goods_number - IF(oi.`pay_fee` < 0, oi.`pay_fee` * -1 * og.goods_price * og.goods_number / oi.goods_amount, 0)) * g.commission / 100, g.commission * og.goods_number - IF(oi.`pay_fee` < 0 AND g.commission > 0, oi.`pay_fee` * -1 * og.goods_price * og.goods_number / oi.goods_amount, 0))) as commission '.
               'FROM ' . $GLOBALS['ecs']->table('order_info') . ' as oi '.
               'LEFT JOIN ' . $GLOBALS['ecs']->table('order_goods') . ' as og ON oi.`order_id` = og.`order_id` '.
               'LEFT JOIN ' . $GLOBALS['ecs']->table('goods') . ' as g ON og.`goods_id` = g.`goods_id` '.
               'WHERE 1 ' . $where . " AND oi.`salesperson` IN ('kanyiukwong','online','yen','natalie','wai','him')".
               'GROUP BY oi.`order_id` '.
               ') as tmp2 '.
               ')';
        if ($is_pagination)
        {
            $sql .= " LIMIT " . $filter['start'] . ', ' . $filter['page_size'];
        }
    
        $sale_commission_data = $GLOBALS['db']->getAll($sql);
    
        foreach ($sale_commission_data as $key => $item)
        {
            $sale_commission_data[$key]['volume_formatted'] = price_format($sale_commission_data[$key]['volume']);
            $sale_commission_data[$key]['commission_formatted'] = price_format($sale_commission_data[$key]['commission']);
        }
        $arr = array(
            'sale_commission_data' => $sale_commission_data,
            'filter' => $filter,
            'page_count' => $filter['page_count'],
            'record_count' => $filter['record_count']
        );
        return $arr;
    }
    public function getImportantGoodsList($end_date,$is_pagination = true)
    {
        global $db, $ecs, $userController ,$_CFG;
        $end_time = local_strtotime($end_date);
        $_REQUEST['sort_by'] = empty($_REQUEST['sort_by']) ? 'goods_id' : trim($_REQUEST['sort_by']);
        $_REQUEST['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
        $_REQUEST['start'] = isset($_REQUEST['start']) ?  $_REQUEST['start'] : 0;
        /* 分页大小 */
        $_REQUEST['page'] = empty($_REQUEST['page']) || (intval($_REQUEST['page']) <= 0) ? 1 : intval($_REQUEST['page']);
        $where = 'where `is_important` = 1';
        if (isset($_REQUEST['page_size']) && intval($_REQUEST['page_size']) > 0)
        {
            $_REQUEST['page_size'] = intval($_REQUEST['page_size']);
        }
        else
        {
            $_REQUEST['page_size'] = 50;
        }
        if ($_REQUEST['page_size'] > 1000)
        {
            $_REQUEST['page_size'] = 1000;
        }
        $selected_time = local_date('Y-m',$end_time);
        $where .= " AND gc.time like '" . $selected_time . "' ";
        $base_sql = "SELECT gc.goods_id, g.goods_name, IF(RIGHT(g.`commission`, 1) = '%',round(g.shop_price * g.commission/100,2), g.commission) AS commission, gc.estimated_sales, IF(RIGHT(g.`commission`, 1) = '%',round(g.shop_price * g.commission/100,2)*gc.estimated_sales, g.commission*gc.estimated_sales) AS estimated_total_commission FROM " . $ecs->table('important_goods') . " AS gc " .
                "LEFT JOIN " . $ecs->table('goods') . " AS g ON g.goods_id = gc.goods_id LEFT JOIN " . $ecs->table('order_goods') . " AS og ON og.goods_id = g.goods_id " .
                "LEFT JOIN " . $ecs->table('order_info') . " AS oi ON oi.order_id = og.order_id " . $where .
                " GROUP BY gc.goods_id,gc.estimated_sales ORDER BY $_REQUEST[sort_by] $_REQUEST[sort_order]";
        $count_sql = "SELECT COUNT(c.goods_id) FROM ( ". $base_sql . ") AS c";
        $record_count   = $db->getOne($count_sql);   
        if ($is_pagination)
        {
            $sql = $base_sql ." LIMIT " . $_REQUEST['start'] . ",$_REQUEST[page_size]"; 
        } else {
            $sql = $base_sql; 
        }
        $row = $db->getAll($sql);  
        $total_row = $db->getAll($base_sql);
        $allsalesperson = $this->getSalesperson();   
        $cursalesperson = [];
        foreach ($allsalesperson as $sales) {
            $salesname = $sales['user_name'];
            $curTotalSalesVolumn = 0;
            foreach ($total_row as $key => $val) {
                $goods_id = $val['goods_id'];
                $curSalesVolumn = $this->getSalesGoodsVolumn($salesname,$goods_id,$end_date);
                $curTotalSalesVolumn += $curSalesVolumn;
            }
            if($curTotalSalesVolumn !=0) {
                array_push($cursalesperson, $salesname);
            }
        }

        foreach ($row as $key => $val) {
            $goods_id = $val['goods_id'];
            foreach ($cursalesperson as $salesname) {
                $val[$salesname] = $this->getSalesGoodsVolumn($salesname,$goods_id,$end_date);
            }
            $val['estimated_sales_underline'] = "<span style='text-decoration: underline;cursor:pointer'>" . $val['estimated_sales'] . "</span>";
            $val['commission_formatted'] = "<span data-commission='".$val['commission']."'>" . price_format($val['commission']) . "</span>";
            $val['estimated_total_commission_formatted'] = price_format($val['estimated_total_commission']);
            $row[$key] = $val;
        }
        $arr = array('important_goods' => $row, 'record_count' => $record_count,'cursalesperson' => $cursalesperson);
        return $arr;
    }
    public function getEstimatedSales($goods_id)
    {
        global $db, $ecs;
        $orderController = new OrderController();
        $order_type_sa = $orderController::DB_ORDER_TYPE_SALESAGENT;
        // 1 months days sold
        $compare_days = 31;
        $end_time_period_compare_days= local_strtotime(date("Y-m-d", strtotime( date( 'Y-m-d' )." -0 months")));
        $start_time_period_compare_days  = local_strtotime(date("Y-m-d", strtotime( date( 'Y-m-d' )." -".$compare_days." days")));   
        $where = order_query_sql('finished', 'oi.') .
		   " AND (oi.order_type <> '" . $order_type_sa . "' OR oi.order_type IS NULL) " .
             " AND oi.shipping_time >= '".$start_time_period_compare_days."' AND oi.shipping_time <= '" . ($end_time_period_compare_days+86399) . "' ";
        $sql = "select sum(og.goods_number) as sold_goods_total  from  ".$ecs->table('order_info')." oi ".
               " left join ".$ecs->table('order_goods')." og on (oi.order_id = og.order_id) ".
               " left join ".$ecs->table('goods')." g on (og.goods_id = g.goods_id) ".
               " where g.goods_id = " . $goods_id . $where .  
               " group by g.goods_id" ;
        $estimated_sales = $db->getOne($sql);        
        return $estimated_sales;
    }
    public function getSalesGoodsVolumn($sales,$goods_id,$end_date)
    {
        global $db, $ecs;
        $orderController = new OrderController();
        $order_type_sa = $orderController::DB_ORDER_TYPE_SALESAGENT;
        $end_time = local_strtotime($end_date);
        $start_date  = local_date('Y-m-01',$end_time);   
        $start_time = local_strtotime($start_date);
        $where = order_query_sql('finished', 'oi.') .
		   " AND (oi.order_type <> '" . $order_type_sa . "' OR oi.order_type IS NULL) " .
             " AND oi.shipping_time >= '".$start_time."' AND oi.shipping_time <= '" . ($end_time+86399) . "' ";
        $sql = "select sum(og.goods_number) as sold_goods_total  from  ".$ecs->table('order_info')." oi ".
               " left join ".$ecs->table('order_goods')." og on (oi.order_id = og.order_id) ".
               " left join ".$ecs->table('goods')." g on (og.goods_id = g.goods_id) ".
               " where g.goods_id = " . $goods_id . " AND oi.salesperson LIKE '" . $sales . "' " .  $where .  
               " group by g.goods_id" ;
        $volumn = $db->getOne($sql);    
        $volumn = empty($volumn) ? 0 : $volumn;
        return $volumn;
    }
    //get Sales Team 2 and Sales Team Manager 14
    public function getSalesperson($goods_id)
    {
        global $db, $ecs;
        $sql = "SELECT user_name FROM ".$ecs->table('admin_user')." WHERE (`role_id` = 2 or `role_id` = 14) AND `user_name` NOT IN ('salesteam','TestAccount1','TestAccount2') ORDER BY `user_id` ASC" ;
        $salesperson = $db->getAll($sql);  
        return $salesperson;
    }
    //門市佣金計算
    public function getShopCommissionList($end_date) {
        global $db, $ecs;
        $orderController = new OrderController();
        $order_type_sa = $orderController::DB_ORDER_TYPE_SALESAGENT;
        $order_type_ws = $orderController::DB_ORDER_TYPE_WHOLESALE;
        $end_time = local_strtotime($end_date);
        $start_date  = local_date('Y-m-01',$end_time);   
        $start_time = local_strtotime($start_date);
        //Sales Executives : oi.order_type <> 'sa' OR oi.order_type IS NULL
        $where = order_query_sql('finished', 'oi.') .	   
             " AND oi.shipping_time >= '".$start_time."' AND oi.shipping_time <= '" . ($end_time+86399) . "' ";
        $sales_where = $where . " AND (oi.order_type <> '" . $order_type_sa . "' OR oi.order_type IS NULL) ";
        $sql = '('.
        'SELECT salesperson, position, joined_date, user_id, COUNT(*) as orders, SUM(total_amount) as volume, SUM(commission + commission_adjust) as commission FROM (' .
            'SELECT oi.`salesperson`, au.position, au.joined_date, au.user_id,  (oi.`money_paid` + oi.`order_amount`) as total_amount, oi.`commission_adjust`, '.
            'SUM(IF(RIGHT(g.`commission`, 1) = \'%\', (og.goods_price * og.goods_number - IF(oi.`pay_fee` < 0, oi.`pay_fee` * -1 * og.goods_price * og.goods_number / oi.goods_amount, 0)) * g.commission / 100, g.commission * og.goods_number - IF(oi.`pay_fee` < 0 AND g.commission > 0, oi.`pay_fee` * -1 * og.goods_price * og.goods_number / oi.goods_amount, 0))) as commission '.
            'FROM ' . $ecs->table('order_info') . ' as oi '.
            'LEFT JOIN ' . $ecs->table('order_goods') . ' as og ON oi.`order_id` = og.`order_id` '.
            'LEFT JOIN ' . $ecs->table('goods') . ' as g ON og.`goods_id` = g.`goods_id` '.
            'LEFT JOIN ' . $ecs->table('admin_user') . ' AS au ON oi.salesperson = au.user_name ' .
            'WHERE 1 ' . $sales_where . ' AND (au.role_id = 2 or au.role_id = 14) ' .
            'GROUP BY oi.`order_id` '.
            ') as tmp '.
        'GROUP BY `salesperson` '.
        'ORDER BY `salesperson` ASC'.
        ')';
        $sales = $db ->getAll($sql);
        $manager_volumn = 0;
        foreach ($sales as $key => $item)
        {   
            if($item['position'] == 'Shop Manager') {
                $manager_volumn += $item['volume'];
            }
            if($item['position'] == 'Vice Shop Manager') {
                $manager_volumn += $item['volume'];
            }
            $sales[$key]['volume_formatted'] = price_format($sales[$key]['volume']);
            $sales[$key]['commission_formatted'] = price_format($sales[$key]['commission']);
            $total_volumn += round($sales[$key]['volume'],2);
            $total_commission += round($sales[$key]['commission'],2);
        }
        //$total_sales_volumn:門市所有人銷售總額
        $total_sales_volumn = $total_volumn;
        //total_shop_weight_commission門市計比重後所有人佣金之和
        $total_shop_weight_commission = $total_commission;
        $manager_grade_sales = round($manager_volumn / $total_sales_volumn * 100);
        //其他平台(批發)
        $ws_where = $where . " AND oi.order_type = '" . $order_type_ws . "' ";
        $wholesale = $this->getCommissionExceptSales(0.8,$ws_where);
        //網上下單門店自取
        $pickup_where = $where . " AND oi.`salesperson` LIKE 'online' AND oi.`shipping_id` = 5 ";
        $pickup = $this->getCommissionExceptSales(0.8,$pickup_where);
        //其他平台(ESD)
        $esd_where = $where . " AND oi.order_type = '" . $order_type_sa . "' AND oi.`type_id` = 11 ";
        $esd = $this->getCommissionExceptSales(0.8,$esd_where);
        //網上扣除門店自取
        $online_except_pickup_where = $where . " AND oi.`salesperson` LIKE 'online' AND oi.`shipping_id` <> 5 ";
        $online_except_pickup = $this->getCommissionExceptSales(0.1,$online_except_pickup_where);
        //其它平台(AsiaMiles,Price,Yahoo, Habittzz, the club)
        $other_sa_where = $where . " AND oi.order_type = '" . $order_type_sa . "' AND oi.`type_id` IN (2,3,5,6,7) ";
        $other_sa = $this->getCommissionExceptSales(0.1,$other_sa_where);
        $total_volumn += $wholesale[0]['original_volume'] + $pickup[0]['original_volume']+ $esd[0]['original_volume']+ $online_except_pickup[0]['original_volume']+ $other_sa[0]['original_volume'];
        $total_commission += $wholesale[0]['original_commission'] + $pickup[0]['original_commission']+ $esd[0]['original_commission']+ $online_except_pickup[0]['original_commission']+ $other_sa[0]['original_commission'];
        //total_except_shop_weight_commission非門市計比重後佣金之和
        $total_except_shop_weight_commission = $wholesale[0]['original_weight_commission'] + $pickup[0]['original_weight_commission']+ $esd[0]['original_weight_commission']+ $online_except_pickup[0]['original_weight_commission']+ $other_sa[0]['original_weight_commission'];
        //total_weight_commission計比重後佣金總金額 = total_except_shop_weight_commission非門市計比重後佣金之和+total_shop_weight_commission門市計比重後所有人佣金之和
        $total_weight_commission = $total_except_shop_weight_commission +  $total_shop_weight_commission;
        $shop_target_time = local_date('Y-m',$end_time);
        $shop_target = floatval($this->getShopTarget($shop_target_time)['target_volumn']);
        $meet_target = $total_sales_volumn >= $shop_target ? 1 : 0;
        return array("sales" => $sales, "wholesale" => $wholesale, "pickup" => $pickup, "esd" => $esd, "online_except_pickup" => $online_except_pickup, "other_sa" => $other_sa,
            "total_volumn" => price_format($total_volumn), "total_commission" => price_format($total_commission), "total_weight_commission" => price_format($total_weight_commission), 
            "total_shop_weight_commission" => $total_shop_weight_commission, "total_except_shop_weight_commission" => $total_except_shop_weight_commission,
            "manager_grade_sales" => $manager_grade_sales, "total_sales_volumn" => $total_sales_volumn, "shop_target" => $shop_target, "meet_target" => $meet_target, "shop_target_time" => $shop_target_time);
    }


    public function getCommissionExceptSales($weight,$where) {
        global $db, $ecs;
        $sql = '('.
        'SELECT COUNT(*) as orders, SUM(total_amount) as volume, SUM(commission + commission_adjust) as commission, SUM(commission + commission_adjust)* '.$weight.' as weight_commission FROM (' .
            'SELECT (oi.`money_paid` + oi.`order_amount`) as total_amount, oi.`commission_adjust`, '.
            'SUM(IF(RIGHT(g.`commission`, 1) = \'%\', (og.goods_price * og.goods_number - IF(oi.`pay_fee` < 0, oi.`pay_fee` * -1 * og.goods_price * og.goods_number / oi.goods_amount, 0)) * g.commission / 100, g.commission * og.goods_number - IF(oi.`pay_fee` < 0 AND g.commission > 0, oi.`pay_fee` * -1 * og.goods_price * og.goods_number / oi.goods_amount, 0))) as commission '.
            'FROM ' . $ecs->table('order_info') . ' as oi '.
            'LEFT JOIN ' . $ecs->table('order_goods') . ' as og ON oi.`order_id` = og.`order_id` '.
            'LEFT JOIN ' . $ecs->table('goods') . ' as g ON og.`goods_id` = g.`goods_id` '.
            'WHERE 1 ' . $where .
            'GROUP BY oi.`order_id` '.
            ') as tmp '.
        ')';
        $data = $db ->getAll($sql);
        foreach ($data as $key => $item)
        {
            $data[$key]['original_volume'] = round($data[$key]['volume'],2);
            $data[$key]['original_commission'] = round($data[$key]['commission'],2);
            $data[$key]['original_weight_commission'] = round($data[$key]['weight_commission'],2);
            $data[$key]['volume_formatted'] = price_format($data[$key]['volume']);
            $data[$key]['commission_formatted'] = price_format($data[$key]['commission']);
            $data[$key]['weight_commission_formatted'] = price_format($data[$key]['weight_commission']);
        }
        return $data;
    }

    // get shop target 
    public function getShopTarget($time) {
        global $db, $ecs;
        $sql = "SELECT target_volumn FROM ". $ecs->table('shop_target') ." WHERE `time` LIKE '" . $time . "' ";
        return $db -> getRow($sql);
    }

    // get shop salary report
    public function getShopSalaryList($end_date) {
        global $db, $ecs;
        $commentController = new CommentController();
        $sales_table_id = $commentController::REVIEW_SALES_TABLE_ID;  
        $shopCommission = $this->getShopCommissionList($end_date);     
        $sales = $shopCommission["sales"];
        //門市計比重後所有人佣金之和
        $total_shop_weight_commission = $shopCommission["total_shop_weight_commission"];
        //非門市計比重後佣金之和
        $total_except_shop_weight_commission = $shopCommission["total_except_shop_weight_commission"];
        $total_private_pontis = 0;
        $avgs = $commentController->getReviewAvgScore($sales_table_id);
        $admin_avg = $avgs['admin_avg'];
        $importantGoodsList = $this->getImportantGoodsList($end_date,$is_pagination = false)['important_goods'];
        $importantGoodsCommission = [];
        foreach ($sales as $key => $item)
        {   
            $position_points = 0;
            $salesname =$item['salesperson'];
            $user_id = $item['user_id'];
            $admin_total_avg = $admin_avg[$user_id]['total_avg'];
            $position = $item['position'];
            $joined_date = $item['joined_date'];
            if (!$joined_date) {
                $month_points = 0;
            } else {
                $month_points = $this -> getMonthRange($joined_date,$end_date)['count'];
            }
            $is_review_award = $admin_total_avg > 9 ? 1 : 0;
            if($position == 'Shop Manager') {
                $position_points = 200;
            }
            if($position == 'Vice Shop Manager') {
                $position_points = 160;
            }
            if($position == 'Sales Supervisor') {
                $position_points = 120;
            }
            if($position == 'Senior Sales Executive') {
                $position_points = 80;
            }
            if($position == 'Sales Executive') {
                $position_points = 40;
            }
            //set position_points and month_points for salesteam account
            if($user_id == 4) {
                $position_points = 0;
                $month_points = 0;
            }
            foreach ($importantGoodsList as $val) {
                $importantGoodsCommission[$salesname] += intval($val[$salesname]) * floatval($val['commission']);
            }
            $sales[$key]['is_review_award'] = $is_review_award;
            $sales[$key]['position_points'] = $position_points;
            $sales[$key]['month_points'] = $month_points;
            $sales[$key]['private_pontis'] = $position_points + $month_points;
            $sales[$key]['important_goods_award'] = $importantGoodsCommission[$salesname];
            $sales[$key]['important_goods_award_format'] = price_format($sales[$key]['important_goods_award']);
            $total_private_pontis += $sales[$key]['private_pontis']; 
            //私佣
            $sales[$key]['private_commission'] = $item['commission'] * 0.3;
            $sales[$key]['private_commission_format'] = price_format($sales[$key]['private_commission']);
        }
        foreach ($sales as $key => $item) {
            //公佣
            $sales[$key]['public_commission'] = ($total_except_shop_weight_commission + $total_shop_weight_commission * 0.7) * ($item['private_pontis'] / $total_private_pontis);
            $sales[$key]['public_commission_format'] = price_format($sales[$key]['public_commission']);
            //評分獎
            $sales[$key]['review_award'] = $sales[$key]['is_review_award'] ? ($sales[$key]['public_commission'] + $sales[$key]['private_commission']) * 0.1 : 0;
            $sales[$key]['review_award_format'] = price_format($sales[$key]['review_award']);
            
        }
        // dd($sales);
        return $sales;
    }

    public function getMonthRange($joined_date,$end_date) {
        $joined_time = local_strtotime($joined_date)+86399;
        $end_time = local_strtotime($end_date)+86399;
        $result = [];
        $time   = $joined_time;
        $last   = date('Y-m', $end_time);
        do {
            $month = date('Y-m', $time);
            $result[] = [
                'month' => $month,
            ];
            $time = strtotime('+1 month', $time);
        } while ($month != $last);
        return(array('month_range' => $result, 'count' => count($result) ));
    }

    /**
     *  获取產品佣金列表信息
     *
     * @access  public
     * @param
     *
     * @return void
     */
    function goods_commission_list()
    {
        ini_set('memory_limit', '256M');
        global $ecs, $db, $_LANG, $erpController;
        $where = "";
        $_REQUEST['cat_id'] = empty($_REQUEST['cat_id']) ? 0 : intval($_REQUEST['cat_id']);
        $_REQUEST['brand_id'] = empty($_REQUEST['brand_id']) ? 0 : intval($_REQUEST['brand_id']);
        $_REQUEST['keyword'] = empty($_REQUEST['keyword']) ? '' : trim($_REQUEST['keyword']);
        $_REQUEST['time'] = empty($_REQUEST['time']) ? '' : $_REQUEST['time'];
        $_REQUEST['is_important'] = ($_REQUEST['is_important'] != "0" && $_REQUEST['is_important'] != "1") ? '' : $_REQUEST['is_important'];
        $_REQUEST['sort_by'] = empty($_REQUEST['sort_by']) ? 'goods_id' : trim($_REQUEST['sort_by']);
        $_REQUEST['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
        $_REQUEST['start'] = isset($_REQUEST['start']) ?  $_REQUEST['start'] : 0;
        /* 分页大小 */
        $_REQUEST['page'] = empty($_REQUEST['page']) || (intval($_REQUEST['page']) <= 0) ? 1 : intval($_REQUEST['page']);

        if (isset($_REQUEST['page_size']) && intval($_REQUEST['page_size']) > 0)
        {
            $_REQUEST['page_size'] = intval($_REQUEST['page_size']);
        }
        else
        {
            $_REQUEST['page_size'] = 50;
        }
        // To prevent exceeding memory limit, max page_size is limited to 1000
        if ($_REQUEST['page_size'] > 1000)
        {
            $_REQUEST['page_size'] = 1000;
        }
        if ($_REQUEST['keyword'])
        {
            if (!preg_match('/^([0-9]+,)*[0-9]+$/', $_REQUEST['keyword']))
            {
                $where ="AND g.goods_name LIKE '%".$_REQUEST['keyword']."%' OR gc.goods_id LIKE '%".$_REQUEST['keyword']."%'  OR g.goods_sn LIKE '%".$_REQUEST['keyword']."%' ";
            } else {
                $where ="AND gc.goods_id IN ({$_REQUEST['keyword']}) ";
            }
            
        }
        if (isset($_REQUEST['cat_id']) && $_REQUEST['cat_id'] > -1)
        {
            $arr_cats = cat_list($_REQUEST['cat_id'], $_REQUEST['cat_id'], FALSE);
            $arr_cats_id = array();
            if (sizeof($arr_cats) > 0){
                foreach($arr_cats as $cat){
                    array_push($arr_cats_id, $cat['cat_id']);
                }
            }
            $str_cats_id = implode(",", $arr_cats_id);
            $where .= "AND g.cat_id IN (" . $str_cats_id . ") ";
        }
        if ($_REQUEST['brand_id'] && $_REQUEST['brand_id'] > -1)
        {
            $where .= "AND g.brand_id = '" . $_REQUEST['brand_id'] . "' ";
        }
        if ($_REQUEST['time'])
        {
            $where .= "AND gc.time like '" . $_REQUEST['time'] . "' ";
        }
        if ($_REQUEST['is_important'] == "1")
        {
            $where .= "AND gc.is_important = '" . $_REQUEST['is_important'] . "' ";
        }
        $_REQUEST['page_count']     = $_REQUEST['record_count'] > 0 ? ceil($_REQUEST['record_count'] / $_REQUEST['page_size']) : 1;
        $base_sql = "SELECT gc.time, g.goods_id, g.goods_name, g.shop_price, g.brand_id, g.cat_id, g.goods_sn, g.commission, IFNULL(SUM(gas.goods_qty), 0) AS goods_stock, gc.is_important, IF(gc.is_important=1, '是', '否') AS is_important_str FROM ".$ecs->table('important_goods')." AS gc LEFT JOIN ".$ecs->table('goods')." AS g ON g.goods_id = gc.goods_id " .
                "LEFT JOIN ".$ecs->table('erp_goods_attr_stock')." AS gas ON gas.goods_id = g.goods_id ".
                "WHERE gas.warehouse_id IN (".$erpController->getSellableWarehouseIds(true).") ".
                "AND g.is_real = 1 AND g.is_on_sale = 1 ". $where .
                "GROUP BY g.goods_id, is_important, gc.time, g.goods_name, g.shop_price " . 
                "ORDER BY $_REQUEST[sort_by] $_REQUEST[sort_order] ";
        $count_sql = "SELECT COUNT(c.goods_id) FROM ( ". $base_sql . ") AS c";
        $record_count   = $db->getOne($count_sql);     
        $sql = $base_sql ." LIMIT " . $_REQUEST['start'] . ",$_REQUEST[page_size]";
        $row = $db->getAll($sql); 
        foreach ($row as $key => $val) {
            $val['commission_underline'] = "<span style='text-decoration: underline;cursor:pointer'>" . $val['commission'] . "</span>";
            $val['shop_price_formatted'] = price_format($val['shop_price']);
            $row[$key] = $val;
        } 
        $arr = array('goods_commission' => $row, 'record_count' => $record_count);
        return $arr;
    }

}

?>