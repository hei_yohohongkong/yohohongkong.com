<?php

/***
* CategoryController.php
* by Anthony 20170804
*
* Category controller
***/

namespace Yoho\cms\Controller;
use Yoho\cms\Model;
if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

class CategoryController extends YohoBaseController
{
    function groupCat($cat_list = array())
    {
        global $ecs, $db;
        if(empty($cat_list)) return false;

        foreach($cat_list as $key => $cat)
        {
            $row = $this->getCategoryThumb($cat['cat_id']);
            $cat_list[$key]['img'] = get_image_path($row['goods_id'], $row['goods_thumb']);
        }

        $level_list = array();
        $catList    = array();

        /* Group category by level */
        foreach ($cat_list as $c_id => $category){
            $level = $category['level'];
            $level_list[$level][$c_id] = $category;
        }

        $max_level = max(array_keys($level_list));
        for ($i = $max_level; $i > 0 ; $i--){
            foreach($level_list[$i] as $c_id => $category)
            {
                $p_id = $category['parent_id'];
                $level_list[($i - 1)][$p_id]['children'][$c_id] = $category;
                $level_list[($i - 1)][$p_id]['count'] = count($level_list[($i - 1)][$p_id]['children']);
                if(isset($level_list[($i - 1)][$p_id]['is_show']) && $level_list[($i - 1)][$p_id]['is_show'] == '0'){
                    unset($level_list[($i - 1)][$p_id]);
                }

                /* //If need count sub Category
                if(!isset($level_list[($i - 1)][$p_id]['sub_count'])) $level_list[($i - 1)][$p_id]['sub_count'] = 0;
                $level_list[($i - 1)][$p_id]['sub_count'] += isset($level_list[($i - 1)][$p_id]['children'][$c_id]['count'])? $level_list[($i - 1)][$p_id]['children'][$c_id]['count']:0;
                */
            }
        }
        $cat_list = $level_list[0];
        // var_dump($cat_list);
        // exit;
        return $cat_list;
    }

    function get_cat_list($cat_list = array())
    {
        global $ecs, $db;
        if(empty($cat_list)) return false;

        foreach($cat_list as $key => $cat)
        {
            $row = $this->getCategoryThumb($cat['cat_id']);
            $cat_list[$key]['img'] = get_image_path($row['goods_id'], $row['goods_thumb']);
        }

        $level_list = array();
        $catList    = array();

        /* Group category by level */
        foreach ($cat_list as $c_id => $category){
            $level = $category['level'];
            $level_list[$level][$c_id] = $category;
        }

        $max_level = max(array_keys($level_list));
        for ($i = $max_level; $i > 0 ; $i--){
            foreach($level_list[$i] as $c_id => $category)
            {
                $p_id = $category['parent_id'];
                $level_list[($i - 1)][$p_id]['children'][$c_id] = $category;
                $level_list[($i - 1)][$p_id]['count'] = count($level_list[($i - 1)][$p_id]['children']);
                if(isset($level_list[($i - 1)][$p_id]['is_show']) && $level_list[($i - 1)][$p_id]['is_show'] == '0'){
                    unset($level_list[($i - 1)][$p_id]);
                }

                /* //If need count sub Category
                if(!isset($level_list[($i - 1)][$p_id]['sub_count'])) $level_list[($i - 1)][$p_id]['sub_count'] = 0;
                $level_list[($i - 1)][$p_id]['sub_count'] += isset($level_list[($i - 1)][$p_id]['children'][$c_id]['count'])? $level_list[($i - 1)][$p_id]['children'][$c_id]['count']:0;
                */
            }
        }
        $cat_list = $level_list[0];
        // var_dump($cat_list);
        // exit;
        return $cat_list;
    }

    function getHotCategory($num = 8, $cat_id = 0)
    {
        global $ecs, $db, $_CFG;
        // check time
        if (isset($_CFG['index_goods_recom_memcache']) && $_CFG['index_goods_recom_memcache'] == true) {
            $current_mins = date('i');
            if ($current_mins == 18 || $current_mins == 38) {
                $this->memcache->delete('home_page_HotCategory'.$num.'_'.$cat_id.'_'.$_CFG['lang']);
            }

            $current_hour = date('H');
            if ($current_hour == '08') {
                $this->memcache->delete('home_page_HotCategory'.$num.'_'.$cat_id.'_'.$_CFG['lang']);
            }

            $res = $this->memcache->get('home_page_HotCategory'.$num.'_'.$cat_id.'_'.$_CFG['lang']);
        } else {
            $res = false;
        }

        if ($res === false) {    
            $sql = "SELECT c.cat_id, c.style, c.cat_name, c.measure_unit, c.parent_id, c.is_show, c.show_in_nav, c.grade, c.sort_order,c.hot_category ".
                'FROM ' . $ecs->table('category') . " AS c ".
                "WHERE c.hot_category = 1 AND c.is_show = 1 ";
            if($cat_id > 0 ) {
                $children = db_create_in(array_unique(array_merge(array($cat_id), array_keys(cat_list($cat_id, 0, false)))));
                $sql .= " AND c.parent_id $children ";
            }
            $sql .= "ORDER BY c.sort_order DESC LIMIT $num";

            $res = $db->getAll($sql);
            $res = localize_db_result('category', $res);

            foreach($res as $key => $cat)
            {
                $row = $this->getCategoryThumb($cat['cat_id']);
                $res[$key]['img'] = get_image_path($row['goods_id'], $row['goods_thumb']);
                $res[$key]['url'] = build_uri('category', array('cid' => $cat['cat_id'], 'cname' => $cat['cat_name']), $cat['cat_name']);
            }
            $this->memcache->set('home_page_HotCategory'.$num.'_'.$cat_id.'_'.$_CFG['lang'], $res);
        }

        return $res;
    }

    function getRankItem($num = 12)
    {
        global $ecs, $db;

        $res = $this->getTopLevelCat();
        $list = array();
        foreach ($res as $key => $value) {
            $list[$value['cat_id']]['cat_name']  = $value['cat_name'];
            $list[$value['cat_id']]['style']  = $value['style'];
            $list[$value['cat_id']]['goods'] = category_get_goods_all($value['cat_id'], $brand, $min, $max, 'g.sort_order asc, g.sales ', 'desc', $is_promote=0, $num, $biao, $tj="");
            $i = 1;
            foreach ($list[$value['cat_id']]['goods'] as $key => $goods) {
                if($i > 3)break;
                $list[$value['cat_id']]['goods'][$key]['rank_index'] = $i;
                $i++;
            }
        }

        return $list;
    }

    function getIndexCatSession($type = 'best')
    {
        global $ecs, $db, $_CFG;
        $goodsController = new GoodsController();

        $res = $this->getTopLevelCat();
        $list = array();
        foreach ($res as $key => $value) {
            $list[$value['cat_id']]['cat_name']  = $value['cat_name'];
            $list[$value['cat_id']]['style']  = $value['style'];
            $children = get_children($value['cat_id']);

            // check time 
            if (isset($_CFG['index_goods_recom_memcache']) && $_CFG['index_goods_recom_memcache'] == true) {
                $current_mins = date('i');
                if ($current_mins == 18 || $current_mins == 38) {
                    $this->memcache->delete('m_home_page_ads_goods-'. $_SESSION['user_rank'] . '_' . $type.'_'.$value['cat_id'].'_'.$_CFG['lang']);
                }

                $current_hour = date('H');
                if ($current_hour == '08') {
                    $this->memcache->delete('m_home_page_ads_goods-'. $_SESSION['user_rank'] . '_' . $type.'_'.$value['cat_id'].'_'.$_CFG['lang']);
                }

                $m_goods = $this->memcache->get('m_home_page_ads_goods-'. $_SESSION['user_rank'] . '_' . $type.'_'.$value['cat_id'].'_'.$_CFG['lang']);
            } else {
                $m_goods = false;
            }
            
            if ($m_goods === false) {
                $m_goods = hw_category_get_goods($children, 0, 0, 0, "", 12, 1, "last_update", "DESC", 0, $GLOBALS['display'], []);
                $this->memcache->set('m_home_page_ads_goods-'. $_SESSION['user_rank'] . '_' . $type.'_'.$value['cat_id'].'_'.$_CFG['lang'],$m_goods);
            }

            if(count($m_goods) > 0){
                $goods_id_arr = array();
                foreach($m_goods as $val){
                    $goods_id_arr[] = $val['goods_id'];
                }
            
                //get favourable list
                $favourable_arr = $goodsController->product_list_get_favourable_info($_SESSION['user_rank'], $goods_id_arr);
            
                //get package list
                $package_arr = $goodsController->product_list_get_package_info($goods_id_arr);

                //get topic list
                $topic_arr = $goodsController->product_list_get_topic_info($goods_id_arr);
            }
            else{
                $favourable_arr = array();
                $package_arr = array();
            }

            $list[$value['cat_id']]['goods'] = $m_goods;
            $list[$value['cat_id']]['favourable_list'] = $favourable_arr;
            $list[$value['cat_id']]['package_list'] = $package_arr;
            $list[$value['cat_id']]['topic_list'] = $topic_arr;
            if(count($list[$value['cat_id']]['goods']) == 0)unset($list[$value['cat_id']]);
        }

        return $list;
    }

    function getIndexHotCatWithGoods($num = 5, $type = 'best')
    {
        global $ecs, $db, $_CFG;
        $goodsController = new GoodsController();

        $sql = "SELECT c.cat_id, c.cat_name, IFNULL(pl.cat_name, p.cat_name) as parent_name, p.style, p.cat_id as p_id, p.sort_order FROM ".$ecs->table('category')." as c ".
        "LEFT JOIN ".$ecs->table('category')." as p2 ON c.parent_id = p2.cat_id ".
        "LEFT JOIN ".$ecs->table('category')." as p ON p2.parent_id = p.cat_id and p.parent_id = 0 ".
        "LEFT JOIN ".$ecs->table('category_lang')." as pl ON p.cat_id = pl.cat_id AND pl.lang = '$_CFG[lang]' ".
        " WHERE c.hot_category = 1 and c.is_show = 1 and p.is_show = 1 and p2.is_show = 1 ORDER BY c.sort_order DESC";
        $res = $db->getAll($sql);
        $res = localize_db_result('category', $res);
        $list = array();
        foreach ($res as $key => $value) {
            $list[$value['p_id']]['parent_name'] = $value['parent_name'];
            $list[$value['p_id']]['sort_order']  = $value['sort_order'];
            $list[$value['p_id']]['style']       = $value['style'];
            $list[$value['p_id']]['p_id']        = $value['p_id'];
            
            $children = get_children($value['cat_id']);

            // check time 
            if (isset($_CFG['index_goods_recom_memcache']) && $_CFG['index_goods_recom_memcache'] == true) {
                $current_mins = date('i');
                if ($current_mins == 18 || $current_mins == 38) {
                    $this->memcache->delete('home_page_ads_goods-'. $_SESSION['user_rank'] . '_'.$type.'_'.$value['cat_id'].'_'.$_CFG['lang']);
                }

                $current_hour = date('H');
                if ($current_hour == '08') {
                    $this->memcache->delete('home_page_ads_goods-'. $_SESSION['user_rank'] . '_'.$type.'_'.$value['cat_id'].'_'.$_CFG['lang']);
                }

                $goods = $this->memcache->get('home_page_ads_goods-'. $_SESSION['user_rank'] . '_'.$type.'_'.$value['cat_id'].'_'.$_CFG['lang']);
            } else {
                $goods = false;
            }
    
            if ($goods === false) {
                $goods = hw_category_get_goods($children, 0, 0, 0, "", 12, 1, "last_update", "DESC", 0, $GLOBALS['display'], []);
                $this->memcache->set('home_page_ads_goods-'. $_SESSION['user_rank'] . '_' .$type.'_'.$value['cat_id'].'_'.$_CFG['lang'],$goods);
            }
            
            if(count($goods) > 0){
                $goods_id_arr = array();
                foreach($goods as $val){
                    $goods_id_arr[] = $val['goods_id'];
                }
            
                //get favourable list
                $favourable_arr = $goodsController->product_list_get_favourable_info($_SESSION['user_rank'], $goods_id_arr);
            
                //get package list
                $package_arr = $goodsController->product_list_get_package_info($goods_id_arr);

                //get topic list
                $topic_arr = $goodsController->product_list_get_topic_info($goods_id_arr);

                $list[$value['p_id']]['hot_cat'][$value['cat_id']]['cat_name']  = $value['cat_name'];
                $list[$value['p_id']]['hot_cat'][$value['cat_id']]['goods'] = $goods;
                $list[$value['p_id']]['hot_cat'][$value['cat_id']]['favourable_list'] = $favourable_arr;
                $list[$value['p_id']]['hot_cat'][$value['cat_id']]['package_list'] = $package_arr;
                $list[$value['p_id']]['hot_cat'][$value['cat_id']]['topic_list'] = $topic_arr;
            }
        }
        usort($list, function($a, $b) {
            return $b['sort_order'] - $a['sort_order'];
        });

        foreach ($list as $key => $value) {
            if(count($list[$key]['hot_cat']) <= 0){
                unset($list[$key]);
            } else {
                $list[$key]['hot_cat'] = array_slice($value['hot_cat'], 0, $num, true);
                /* Get Category best_seller */
                $parent_children      = get_children($value['p_id']);
                // check time
                if (isset($_CFG['index_goods_recom_memcache']) && $_CFG['index_goods_recom_memcache'] == true) {
                    $current_mins = date('i');
                    if ($current_mins == 18 || $current_mins == 38) {
                        $this->memcache->delete('home_page_ads_goods-best_sellers_'.$_SESSION['user_rank'] . '_'. $value['p_id'].'_'.$_CFG['lang']);
                    }
                    $current_hour = date('H');
                    if ($current_hour == '08') {
                        $this->memcache->delete('home_page_ads_goods-best_sellers_'.$_SESSION['user_rank'] . '_'. $value['p_id'].'_'.$_CFG['lang']);
                    }
                    $best_seller_goods = $this->memcache->get('home_page_ads_goods-best_sellers_'.$_SESSION['user_rank'] . '_'. $value['p_id'].'_'.$_CFG['lang']);
                } else {
                    $best_seller_goods = false;
                }

                if ($best_seller_goods === false) {
                    // $best_seller_goods    = get_category_recommend_goods('best_sellers', $parent_children);
                    $best_seller_goods = hw_category_get_goods($parent_children, 0, 0, 0, "", 12, 1, "last_update", "DESC", 0, $GLOBALS['display'], []);
                    $this->memcache->set('home_page_ads_goods-best_sellers_'.$_SESSION['user_rank'] . '_'. $value['p_id'].'_'.$_CFG['lang'],$best_seller_goods);
                }
                if(count($best_seller_goods) > 0){
                    $bs_array = [ $value['p_id'].'_s' =>['cat_name' => _L('categorylist_best_sellers_cat', '暢銷產品'), 'goods' => $best_seller_goods] ];
                    $goods_id_arr = array();
                    foreach($best_seller_goods as $val){
                        $goods_id_arr[] = $val['goods_id'];
                    }
                    //get favourable list
                    $favourable_arr = $goodsController->product_list_get_favourable_info($_SESSION['user_rank'], $goods_id_arr);
                    //get package list
                    $package_arr = $goodsController->product_list_get_package_info($goods_id_arr);
                    //get topic list
                    $topic_arr = $goodsController->product_list_get_topic_info($goods_id_arr);

                    $list[$key]['hot_cat'] = $bs_array + $list[$key]['hot_cat'];
                    $list[$key]['hot_cat'][ $value['p_id'].'_s']['favourable_list'] = $favourable_arr;
                    $list[$key]['hot_cat'][ $value['p_id'].'_s']['package_list'] = $package_arr;
                    $list[$key]['hot_cat'][ $value['p_id'].'_s']['topic_list'] = $topic_arr;
                }
                $sql = "SELECT cb.`cat_id`, b.* " .
                "FROM " . $GLOBALS['ecs']->table('category_brand') . " as cb " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('brand') . " as b ON cb.brand_id = b.brand_id " .
                "WHERE cb.cat_id = '" . "$value[p_id]" . "' " .
                "ORDER BY cb.`cat_id` ASC, cb.`sort_order` ASC";
                $res = $GLOBALS['db']->getAll($sql);
                $res = localize_db_result('brand', $res);
                $list[$key]['brands'] = $res;
            }
        }

        return $list;
    }

    function getTopLevelCat()
    {
        global $ecs, $db;
        $sql = "SELECT cat_id, cat_name, style FROM ".$ecs->table('category')." WHERE parent_id = 0 AND is_show = 1 ";
        $res = $db->getAll($sql);
        $res = localize_db_result('category', $res);

        return $res;
    }

    function getCatLevel($cat_id = null)
    {
        global $ecs, $db;

        if(empty($cat_id)) return 0;
        $sql = "SELECT  count(*)".
                "FROM (SELECT @r AS `_id`, (SELECT @r := `parent_id`
                    FROM ".$ecs->table('category')."
                    WHERE `cat_id` = `_id`) AS `parent_id` ".
                " FROM (SELECT  @r := '$cat_id', @l := 0) AS `vars`, ".$ecs->table('category').
                " WHERE @r <> 0 ) AS `q` ".
                " LEFT JOIN ".$ecs->table('category')." AS `r` ".
                " ON `r`.`cat_id` = `q`.`_id` ";

        $res = $db->getOne($sql);
        return $res;
    }

    function getParentLevel($cat_id = null, $build_cat_url = true)
    {
        global $ecs, $db, $_CFG;

        if(empty($cat_id)) return 0;
        $sql = "SELECT  CAST(`_id` AS UNSIGNED) AS `cat_id`, `q`.`parent_id`, IFNULL(cl.cat_name, r.cat_name) as cat_name, r.sort_order as sort_order, r.is_show as is_show, cperma  ".
                "FROM (SELECT @r AS `_id`, (SELECT @r := `parent_id`
                    FROM ".$ecs->table('category')."
                WHERE `cat_id` = `_id`) AS `parent_id` ".
                " FROM (SELECT  @r := '$cat_id', @l := 0) AS `vars`, ".$ecs->table('category').
                " WHERE @r <> 0 ) AS `q` ".
                " LEFT JOIN ".$ecs->table('category')." AS `r` ".
                 "LEFT JOIN ".$ecs->table('category_lang'). " as cl ON r.cat_id = cl.cat_id AND cl.lang = '" . $_CFG['lang']  . "' " .
                'LEFT JOIN (SELECT id, table_name, lang, perma_link as cperma FROM ' . $GLOBALS['ecs']->table('perma_link') . ') cpl ON cpl.id = r.cat_id  AND cpl.table_name = "category" AND cpl.lang = "' . $GLOBALS['_CFG']['lang'] . '"' .
                " ON `r`.`cat_id` = `q`.`_id` ";

        $res = $db->getAll($sql);
        if($build_cat_url == true){
            // Use build_uri to obtain SEO friendly URLs for the category
            foreach ($res as $key => $row)
            {
                $res[$key]['url'] = build_uri('category', array('cid' => $row['cat_id'], 'cname' => $row['cat_name'], 'cperma' => $row['cperma']), $row['cat_name']);
                $res[$key]['level'] = count($res) - $key;
            }
        }
        return $res;
    }
    //全部產品分類的level1 cat
    function getTopCategories($cat_id, $build_cat_url = true)
    {
        global $_CFG, $db, $ecs;

        $sql = "SELECT c.cat_id as id, IFNULL(cl.cat_name, c.cat_name) as name, c.style, cperma " .
                "FROM " . $ecs->table('category') . " as c " .
                "LEFT JOIN " . $ecs->table('category_lang') . " as cl ON c.cat_id = cl.cat_id AND cl.lang = '" . $_CFG['lang']  . "' " .
                'LEFT JOIN (SELECT id, table_name, lang, perma_link as cperma FROM ' . $GLOBALS['ecs']->table('perma_link') . ') cpl ON cpl.id = c.cat_id  AND cpl.table_name = "category" AND cpl.lang = "' . $GLOBALS['_CFG']['lang'] . '"' .
                "WHERE c.parent_id = '$cat_id' AND c.is_show = 1 " .
                "ORDER BY c.sort_order DESC, c.cat_id ";
        $res = $db->getAll($sql);
        if($build_cat_url == true){
            // Use build_uri to obtain SEO friendly URLs for the category
            foreach ($res as $key => $row)
            {
                $res[$key]['url'] = build_uri('category', array('cid' => $row['id'], 'cname' => $row['name'], 'cperma' => $row['cperma']), $row['name']);    
            }
        }      
        return $res;
    }

    function getSubCategories($cat_id, $build_cat_url = true, $cat_img = false)
    {
        global $_CFG, $db, $ecs;

        $sql = "SELECT c.cat_id as `cat_id`, IFNULL(cl.cat_name, c.cat_name) as `cat_name`, IFNULL(pl.cat_name, p.cat_name)  as `parent_name`, cperma " .
                "FROM " . $ecs->table('category') . " as c " .
                "LEFT JOIN " . $ecs->table('category') . " as p ON c.parent_id = p.cat_id " .
                "LEFT JOIN " . $ecs->table('category_lang') . " as cl ON c.cat_id = cl.cat_id AND cl.lang = '" . $_CFG['lang']  . "' " .
                "LEFT JOIN " . $ecs->table('category_lang') . " as pl ON p.cat_id = pl.cat_id AND pl.lang = '" . $_CFG['lang']  . "' " .
                'LEFT JOIN (SELECT id, table_name, lang, perma_link as cperma FROM ' . $GLOBALS['ecs']->table('perma_link') . ') cpl ON cpl.id = c.cat_id  AND cpl.table_name = "category" AND cpl.lang = "' . $GLOBALS['_CFG']['lang'] . '"' .
                "WHERE p.cat_id = '$cat_id' AND c.is_show = 1 " .
                "ORDER BY c.sort_order DESC, c.cat_id ";
        $res = $db->getAll($sql);
        if ($cat_img== true) {
            foreach ($res as $key => $value) {    
                $cat_id = $value['cat_id'];    
                $gt_sql = "SELECT g.goods_thumb FROM " . $GLOBALS['ecs']->table('goods') . " AS g WHERE goods_id = (SELECT c.goods_thumb_id FROM " . $GLOBALS['ecs']->table('category') . " AS c WHERE `cat_id` = '" . $cat_id . "')";
                $cat_thumb = $GLOBALS['db']->getOne($gt_sql);
                if ($cat_thumb) {
                    $res[$key]['img'] = $cat_thumb;
                } else {
                    $row = $this->getCategoryThumb($cat_id);
                    $res[$key]['img'] = get_image_path($row['goods_id'], $row['goods_thumb']);
                }  
            }
        }

        if($build_cat_url == true){
            // Use build_uri to obtain SEO friendly URLs for the category
            foreach ($res as $key => $row)
            {
                $res[$key]['url'] = build_uri('category', array('cid' => $row['cat_id'], 'cname' => $row['cat_name'], 'cperma' => $row['cperma']), $row['cat_name']);
            }
        }
        return $res;
    }
    //get level 3 cat from level1 cat id 
    function getBottomCategories($cat_id)
    {
        global $_CFG, $db, $ecs;

        $sql = "SELECT c.cat_id as `cat_id`, c.sort_order as `sort_order`, IFNULL(cl.cat_name, c.cat_name) as `cat_name`, IFNULL(pl.cat_name, p.cat_name)  as `parent_name` " .
                "FROM " . $ecs->table('category') . " as c " .
                "LEFT JOIN " . $ecs->table('category') . " as p ON c.parent_id = p.cat_id " .
                "LEFT JOIN " . $ecs->table('category_lang') . " as cl ON c.cat_id = cl.cat_id AND cl.lang = '" . $_CFG['lang']  . "' " .
                "LEFT JOIN " . $ecs->table('category_lang') . " as pl ON p.cat_id = pl.cat_id AND pl.lang = '" . $_CFG['lang']  . "' " .
                "WHERE p.cat_id IN (SELECT p.cat_id as `cat_id` FROM " . $ecs->table('category') . " as p LEFT JOIN " . $ecs->table('category') . " as gp ON p.parent_id = gp.cat_id WHERE gp.cat_id = '$cat_id' AND p.is_show = 1 ORDER BY p.cat_id) AND c.is_show = 1 " .
                "ORDER BY c.sort_order DESC, c.cat_id LIMIT 5 ";
        $res = $db->getAll($sql);
        return $res;
    }

    function getLeftCatMenu($cat_id, $page = 'category', $where = '1', $b_id = 0, $search_parameters = array())
    {
        global $_CFG, $db, $ecs;
        switch ($page) {
            case 'category':
                $level     = $this->getCatLevel($cat_id);
                $p_id      = 0;
                $p_name    = '';
                $cat       = get_cat_info($cat_id);
                $sub_child = $this->getSubCategories($cat_id);
                $allParentCat = array_reverse($this->getParentLevel($cat_id));
                $list = [];
                foreach ($allParentCat as $key => $value) {
                    $value['level'] = $key; //if cat is level 3, this will be 2
                    $list[$value['cat_id']] =  $value;
                    if(($level == 1 || $level == 2) && $value['cat_id'] == $cat_id){
                        $list[$value['cat_id']]['children'] = $this->getSubCategories($value['cat_id']);
                    } else {
                        if($value['level'] == $level - 2 && $level > 2) {
                            $list[$value['cat_id']]['children'] = $this->getSubCategories($value['cat_id']);
                        }
                    }
                }
                if($level > 2) unset($list[$cat_id]);
                $new_list = $this->groupCat($list);
                break;

            default: //using for vip, brands page
                // 1. Get category and their parent category by goods
                $sql = "SELECT c.cat_id, IFNULL(cl.cat_name, c.cat_name) as cat_name, c.parent_id, c.is_show, cperma, parent_cperma, " .
                    "p.parent_id as p_id, IFNULL(pcl.cat_name, p.cat_name) as parent_name, p.sort_order as parent_sort_order " .
                    "FROM " . $ecs->table('goods') . " as g " .
                    "LEFT JOIN  " . $ecs->table('member_price'). " AS mp ON g.goods_id = mp.goods_id AND mp.user_rank = ".$_SESSION['user_rank'] . " ".
                    "LEFT JOIN " . $ecs->table('category') . " AS c ON g.cat_id = c.cat_id " .
                    "LEFT JOIN " . $ecs->table('category') . " as p ON c.parent_id  = p.cat_id AND p.is_show = 1 " .
                    "LEFT JOIN " . $ecs->table('category_lang') . " as cl ON c.cat_id = cl.cat_id AND cl.lang = '" . $_CFG['lang'] . "' " .
                    "LEFT JOIN " . $ecs->table('category_lang') . " as pcl ON pcl.cat_id = p.cat_id AND pcl.lang = '" . $_CFG['lang'] . "' " .
                    "LEFT JOIN (SELECT id, table_name, lang, perma_link as cperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") cpl ON cpl.id = c.cat_id  AND cpl.table_name = 'category' AND cpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
                    "LEFT JOIN (SELECT id, table_name, lang, perma_link as parent_cperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") pcpl ON pcpl.id = p.cat_id  AND pcpl.table_name = 'category' AND pcpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
                    "WHERE $where " .
                    "AND c.parent_id != 0 AND c.is_show = 1 " .
                    "GROUP BY c.cat_id ORDER BY c.sort_order DESC, c.cat_id ASC";

                $goodsCat = $db->getAll($sql);
                if (!empty($b_id)) {
                    $bperma = $this->db->getOne("SELECT perma_link FROM " . $this->ecs->table('perma_link') . " WHERE id = $b_id  AND table_name = 'brand' AND lang = '" . $GLOBALS['_CFG']['lang'] . "'");
                }

                // Count Cat use for menu display.
                $totalSubCount = count($goodsCat);
                // Group by parent category
                $group_parent = array();
                foreach ($goodsCat as $value) {
                    // Build url to the bottom category
                    if($page == 'search'){
                        $value['url'] = '/search.php?' . http_build_query(array('category' => $value['cat_id'], 'intro' => $search_parameters['intro'], 'keywords' => $search_parameters['keywords']));
                    } else {
                        $value['url'] = build_uri($page, array( 'cid' => $value['cat_id'], 'cperma' => $value['cperma'], 'bid' => $b_id, 'bperma' => $bperma));
                    }

                    // Build parent category array
                    $group_parent[$value['parent_id']]['cat_id']   = $value['parent_id'];
                    $group_parent[$value['parent_id']]['cat_name']   = $value['parent_name'];
                    $group_parent[$value['parent_id']]['parent_id']   = $value['p_id'];
                    $group_parent[$value['parent_id']]['sort_order']   = $value['parent_sort_order'];
                    $group_parent[$value['parent_id']]['cperma'] = $value['parent_cperma'];
                    // Build url to the parent category
                    if($page == 'search'){
                        $group_parent[$value['parent_id']]['url'] = '/search.php?' . http_build_query(array('category' => $value['parent_id'], 'intro' => $search_parameters['intro'], 'keywords' => $search_parameters['keywords']));
                    } else {
                        $group_parent[$value['parent_id']]['url'] = build_uri($page, array(
                            'cid' => $value['parent_id'],
                            'cperma' => $value['parent_cperma'],
                            'bid' => $b_id, 'bperma' => $bperma)
                        );
                    }
                    // merge children category into parent category array
                    $group_parent[$value['parent_id']]['children'][$value['cat_id']] = $value;
                    $group_parent[$value['parent_id']]['count'] = count($group_parent[$value['parent_id']]['children']);
                }

                // Get all parent category
                $ungroupCat = array();
                foreach ($group_parent as $key => $value) {

                    $parent_cat = array_reverse($this->getParentLevel($key, false));

                    // Set level and url into category
                    foreach ($parent_cat as $key => $value) {
                        if($group_parent[$value['cat_id']]){
                            $parent_cat[$key] = array_merge($group_parent[$value['cat_id']], $parent_cat[$key]);
                        }
                        $parent_cat[$key]['level'] = $key;
                        if($page == 'search'){
                            $parent_cat[$key]['url'] = '/search.php?' . http_build_query(array('category' => $value['cat_id'], 'intro' => $search_parameters['intro'], 'keywords' => $search_parameters['keywords']));
                        } else {
                            $parent_cat[$key]['url'] = build_uri($page, array(
                                'cid' => $value['cat_id'],
                                'cperma' => $value['cperma'],
                                'bid' => $b_id, 'bperma' => $bperma)
                            );
                        }
                        $ungroupCat[$value['cat_id']] = $parent_cat[$key];
                    }
                }

                // Sort by category sort_order
                uasort($ungroupCat, function ($catArr1, $catArr2) {
                    if ($catArr1['sort_order'] == $catArr2['sort_order']){
                        return $catArr1['cat_id'] > $catArr2['cat_id'] ? 1 : -1;
                    } else {
                        return $catArr1['sort_order'] < $catArr2['sort_order'] ? 1 : -1;
                    }
                    
                });

                // Group category by level
                $new_list = $this->groupCat($ungroupCat);
                $new_list['count'] = $totalSubCount;
                break;
        }

        return $new_list;
    }

    function getRelatedBrands($cat_id, $search_prams)
    {
        global $_CFG, $db, $ecs;
        extract($search_prams);
        if($cid) $cat = get_cat_info($cat_id);
        $where = $this->getRelatedWhere($cat_id);

        /* 品牌筛选 */
        $sql = "SELECT b.brand_logo, b.brand_id, IFNULL(bl.brand_name, b.brand_name) as brand_name, COUNT(*) AS goods_num, bperma ".
            "FROM " . $ecs->table('goods') . " as g " .
            "LEFT JOIN " . $ecs->table('brand') . "as b ON g.brand_id = b.brand_id " .
            "LEFT JOIN " . $ecs->table('brand_lang') . "as bl ON bl.brand_id = b.brand_id AND bl.lang = '" . $_CFG['lang'] . "' " .
            "LEFT JOIN (SELECT id, table_name, lang, perma_link as bperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") bpl ON bpl.id = b.brand_id  AND bpl.table_name = 'brand' AND bpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
            "WHERE " . $where . " AND b.is_show = 1 " .
            "GROUP BY g.brand_id ORDER BY b.sort_order ASC, b.brand_id ASC";
        $brands = $db->getAll($sql);

        foreach ($brands AS $key => $val)
        {
            $brands[$key]['brand_name'] = $val['brand_name'];
            $brands[$key]['brand_logo'] = empty($val['brand_logo']) ? null: '/' . DATA_DIR . '/brandlogo/' . $val['brand_logo'];
            $brands[$key]['url'] = build_uri('category', array(
                'cid' => $cat_id, 'cname' => $cat['cat_name'], 'cperma' => $cat['cperma'],
                'bid' => $val['brand_id'], 'bname' => $val['brand_name'], 'bperma' => $val['bperma'],
                'price_min'=>$price_min, 'price_max'=> $price_max,
                'filter_attr'=>$filter_attr_str), $cat['cat_name']);

            /* 判断品牌是否被选中 */
            if ($bid == $val['brand_id'])
            {
                $brands[$key]['selected'] = 1;
                $brands[$key]['url'] = build_uri('category', array(
                'cid' => $cat_id, 'cname' => $cat['cat_name'], 'cperma' => $cat['cperma'],
                'bid' => 0, 'bname' => $val['brand_name'],
                'price_min' => $price_min, 'price_max'=> $price_max,
                'filter_attr' => $filter_attr_str), $cat['cat_name']);
            }
            else
            {
                $brands[$key]['selected'] = 0;
            }
        }

        return $brands;
    }

    function getDescBrands($cat_id)
    {
        global $_CFG, $db, $ecs;
        if($cid) $cat = get_cat_info($cat_id);
        $where = $this->getRelatedWhere($cat_id);
        if($_CFG['lang']=="zh_tw"){
            $lang = "en_us";
            $sql = "SELECT bl.brand_name as bl_brand_name, b.brand_name as b_brand_name ".
            "FROM " . $ecs->table('goods') . " as g " .
            "LEFT JOIN " . $ecs->table('brand') . "as b ON g.brand_id = b.brand_id " .
            "LEFT JOIN " . $ecs->table('brand_lang') . "as bl ON bl.brand_id = b.brand_id AND bl.lang = 'en_us' " .
            "WHERE " . $where . " AND b.is_show = 1 " .
            "GROUP BY g.brand_id ORDER BY b.sort_order ASC, b.brand_id ASC LIMIT 5";
        }
        if($_CFG['lang']=="zh_cn"){
            $lang = "en_us";
            $sql = "SELECT bl.brand_name as bl_brand_name, IFNULL(bll.brand_name, b.brand_name) as b_brand_name ".
            "FROM " . $ecs->table('goods') . " as g " .
            "LEFT JOIN " . $ecs->table('brand') . "as b ON g.brand_id = b.brand_id " .
            "LEFT JOIN " . $ecs->table('brand_lang') . "as bl ON bl.brand_id = b.brand_id AND bl.lang = 'en_us' " .
            "LEFT JOIN " . $ecs->table('brand_lang') . "as bll ON bll.brand_id = b.brand_id AND bll.lang = 'zh_cn' " .
            "WHERE " . $where . " AND b.is_show = 1 " .
            "GROUP BY g.brand_id ORDER BY b.sort_order ASC, b.brand_id ASC LIMIT 5";
        }
        if($_CFG['lang']=="en_us"){
            $lang = "en_us";
            $sql = "SELECT IFNULL(bl.brand_name, b.brand_name) as bl_brand_name ".
            "FROM " . $ecs->table('goods') . " as g " .
            "LEFT JOIN " . $ecs->table('brand') . "as b ON g.brand_id = b.brand_id " .
            "LEFT JOIN " . $ecs->table('brand_lang') . "as bl ON bl.brand_id = b.brand_id AND bl.lang = 'en_us' " .
            "WHERE " . $where . " AND b.is_show = 1 " .
            "GROUP BY g.brand_id ORDER BY b.sort_order ASC, b.brand_id ASC LIMIT 5";
        }
        /* 品牌筛选 */
       
        $brands = $db->getAll($sql);
        return $brands;
    }

    function getRelatedWhere($cat_id)
    {

        $children = get_children($cat_id);
        $where = "(" . $children . " OR " . get_extension_goods($children) . ") AND g.is_delete = 0 AND g.is_hidden = 0 AND g.is_on_sale = 1 AND g.is_alone_sale = 1";
        return $where;
    }

    function getCategoryThumb($cat_id){
        global $_CFG, $db, $ecs;
        // Memcached handle:
        $cacheKey = $this->genCacheKey(['category_thumb', $cat_id]);
        $row = $this->memcache->get($cacheKey);
        if(!$row || !file_exists(ROOT_PATH.$row['goods_thumb'])){// No memcache, Get by SQL, and save into memcache
            $sql = "SELECT g.goods_id, g.goods_thumb, g.goods_name FROM " . $ecs->table('goods') . " AS g WHERE goods_id = (SELECT c.goods_thumb_id FROM " .  $ecs->table('category') . " AS c WHERE `cat_id` = '" . $cat_id . "')";
            $row = $db->getRow($sql);
           
            if (empty($row)) {
                $row = $db->getRow('SELECT goods_id, goods_thumb, goods_name FROM ' . $ecs->table('goods') . ' AS g WHERE '.get_children($cat_id).' AND g.goods_thumb != \'\' AND g.is_on_sale = 1 AND g.is_alone_sale = 1 AND g.is_delete = 0 ORDER BY last_update DESC LIMIT 1');
            }
            $this->memcache->set($cacheKey, $row);
        }

        return $row;
    }
    function delMemcache($cat_id) {
        $cacheKey = $this->genCacheKey(['category_thumb', $cat_id]);
        $this->memcache->delete($cacheKey);
    }
    public function flushCatThumbMemcache($cat_id = 0, $goods_id = 0){
        global $_CFG, $db, $ecs;
        $cats = array();
        if(!empty($cat_id)){
            // Get parent category id
            $cats = $this->getParentLevel($cat_id, false);
        } elseif(empty($cat_id) && $goods_id > 0){
            $cat_id = $db->getOne("SELECT cat_id FROM ". $ecs->table('goods') ." WHERE goods_id = $goods_id LIMIT 1");
            $cats = $this->getParentLevel($cat_id, false);
        }

        foreach ($cats as $key => $cat) {
            $cacheKey = $this->genCacheKey(['category_thumb', $cat_id]);
            $this->memcache->flush($cacheKey);
        }

        return true;
    }

    /* This Function is for cms sorting */
    public function sort_cat_list($cat_list, $request)
    {
        $arr = [
            'cat_list' => [],
            'filter' => [],
        ];

        if(empty($cat_list)) return $arr;

        $cat_list = cat_list(0, 0, false);
        $i = 0;
        foreach ($cat_list as $key => $row)
        {
            $cat_list[$key]['tree_sort'] = $i;
            $i++;
        }
        $filter['sort_by']    = empty($request['sort_by']) ? 'sort_order' : trim($request['sort_by']);
        $filter['sort_order'] = empty($request['sort_order']) ? 'DESC' : trim($request['sort_order']);
        // Default is sort by level 1 sort order
        if ($filter['sort_by'] != 'sort_order'){
            $sort = array();
            foreach ($cat_list as $key => $row)
            {
                $sort[$key] = $row[$filter['sort_by']];
            }
            $sort_order  = strtoupper($filter['sort_order']) == 'ASC' ? SORT_ASC : SORT_DESC;
            array_multisort($sort, $sort_order, $cat_list);
        } else {
            $cat_list = cat_list(0, 0, false);
        }
        // var_dump($cat_list);

        return $arr = [
            'cat_list' => $cat_list,
            'filter' => $filter,
        ];

    }

    /** This function is find top visit sold rate products
     *
     */
    public function top_visit_sold_goods($show_all = true)
    {
        global $ecs, $db;
        require_once(ROOT_PATH . 'includes/lib_order.php');
        // Get Top level Category
        $cat_list = $this->getTopLevelCat();

        foreach($cat_list as $top_cat) {
            $cat_id = $top_cat['cat_id'];
            $children = get_children($cat_id);

            /* Step 1. Get Top 100 scale goods (click_count must > 50)  */
            $where = " WHERE g.is_hidden = 0 AND g.is_on_sale != 0" . order_query_sql('finished', 'oi.');
            $limit = " LIMIT 100 ";
            $where .= " AND " . get_children($cat_id);
            $order = "ORDER BY (CASE WHEN click_count > 50 THEN 0 ELSE 1 END), scale DESC ";

            $sql = "SELECT *, sold_times / click_count * 100 AS scale FROM (" .
                "SELECT g.goods_id, g.goods_name, g.click_count, COUNT(og.goods_id) AS sold_times FROM " . $ecs->table('goods') ." AS g " .
                "LEFT JOIN " .$ecs->table('order_goods') . " AS og ON og.goods_id = g.goods_id " .
                "LEFT JOIN " .$ecs->table('order_info') . " AS oi ON og.order_id = oi.order_id " . $where .
                "AND g.goods_id IN (".
                    "SELECT ega.goods_id  FROM " . $ecs->table('erp_goods_attr') . " ega LEFT JOIN " . $ecs->table('goods') . " g2 ON g2.goods_id = ega.goods_id " .
                    "WHERE g2.is_hidden = 0 AND g2.is_on_sale != 0 AND ((ega.goods_qty <= 0 AND (g2.pre_sale_date > " . gmtime() . " OR g2.pre_sale_days > 0)) OR ega.goods_qty > 0)" .
                ") " .
            " GROUP BY g.goods_id) stat " . $order . $limit;
            $res = $db->query($sql);

            while ($item = $db->fetchRow($res))
            {
                if ($item['click_count'] <= 0)
                {
                    $item['scale'] = 0;
                }
                else
                {
                    /* 每一百个点击的订单比率 */
                    $item['scale'] = sprintf("%0.2f", ($item['scale'])) .'%';
                }

                $cat_top_vs_goods[$cat_id][] = $item['goods_id'];
            }

            /* Step 2. Get Category avg. price */
            $sql = "SELECT COUNT(g.goods_id) as total_goods_count, SUM(g.shop_price) as total_goods_price, SUM(g.shop_price)/COUNT(g.goods_id) as avg_goods_price ".
                    " FROM ".$ecs->table('goods')." as g WHERE ".$children." AND g.is_delete = 0 AND g.is_hidden = 0 ";
            $cat_avg = $db->getRow($sql);

            /* Step 3. Get goods GOOGLE pageview */
            $googleController = new GoogleController();
            $client = $googleController->getClient();
            $service = new \Google_Service_Analytics($client);
            $id = 'ga:77620237';
            $start_date = date('Y-m-d', strtotime('-7 day'));
            $end_date = 'today';
            $metrics = "ga:uniquePageviews";
            $optParams['dimensions'] = 'ga:pagePath';
            $goods_list = [];
            $goods_count = [];
            $i = 0;
            foreach ($cat_top_vs_goods[$cat_id] as $key => $goods_id) {
                $goods_list[] = $goods_id.'\D';
                $i++;
                if($i % 12 == 0 || $i == count($cat_top_vs_goods[$cat_id])) {
                    $goods = implode('|', $goods_list);
                    $optParams['filters'] = "ga:pagePath=~/product/(".$goods.")";
                    $total = $service->data_ga->get($id,$start_date,$end_date,$metrics,$optParams);
                    $rows = $total->getRows();
                    $goods_list = [];

                    foreach ($rows as $key => $value) {

                        $url   = $value[0];
                        $url   = preg_match('/product\/\d+/', $url, $matches);
                        $count = $value[1];
                        if($matches[0]) {
                            $gid = substr($matches[0], 8);
                            $goods_count[$gid] = ($goods_count[$gid]) ? intval($goods_count[$gid]) + intval($count) : intval($count);
                        }
                    }
                    // We set delay 1 sec for google delay
                    sleep(1);
                }
            }

            /* Step 4. Get goods last 7 days transaction */
            $sql = "SELECT og.goods_id, SUM(og.goods_number) as total_goods_number, g.shop_price, g.goods_name ".
                    " FROM ".$ecs->table('order_goods')." as og ".
                    " LEFT JOIN ".$ecs->table('goods')." as g ON g.goods_id = og.goods_id ".
                    " LEFT JOIN ".$ecs->table('order_info')." as oi ON oi.order_id = og.order_id ".
                    "WHERE og.goods_id ".db_create_in($cat_top_vs_goods[$cat_id])." AND oi.add_time BETWEEN ".local_strtotime('-7 day')." AND ".local_strtotime('today')." ".order_query_sql('payed', 'oi.')." AND og.is_gift = 0 GROUP BY og.goods_id";
            $go = $db->getAll($sql);

            $new_goods_rate = [];
            foreach ($go as $goods) {
                $total_goods_number = $goods['total_goods_number'];
                $pageview           = $goods_count[$goods['goods_id']];
                $goods['pageview']  = intval($pageview);
                $goods['new_rate']  = intval($total_goods_number) / intval($pageview);
                if(empty($goods['new_rate'])) $goods['new_rate'] = 0;
                $goods['new_rate']  = number_format($goods['new_rate'], 2, '.', '');
                $new_goods_rate[$goods['goods_id']] = $goods;
            }

            uasort($new_goods_rate, function ($a, $b) {
                if($a['new_rate']==$b['new_rate']) return 0;
                return $a['new_rate'] < $b['new_rate']?1:-1;
            });

            $final_goods = [];
            require_once(ROOT_PATH . 'includes/lib_main.php');
            $type2lib = array('best'=>'recommend_best', 'new'=>'recommend_new', 'hot'=>'recommend_hot', 'promote'=>'recommend_promotion');
            $num = \get_library_number($type2lib['hot'], 'index');
            $i = 1;
            foreach($new_goods_rate as $key => $goods) {
                if($goods['shop_price'] >= $cat_avg['avg_goods_price']) {
                    $goods['is_match'] = 1;
                    $goods['rate_sort'] = $i;
                    $i++;
                    $final_goods[] = $goods;
                    unset($new_goods_rate[$key]);
                }
            }


            uasort($new_goods_rate, function ($a, $b) {
                if($a['pageview']==$b['pageview']) return 0;
                return $a['pageview'] < $b['pageview']?1:-1;
            });
            foreach($new_goods_rate as $key => $goods) {
                $goods['is_match'] = 0;
                $goods['rate_sort'] = $i;
                $i++;
                $final_goods[] = $goods;
                unset($new_goods_rate[$key]);
            }
            if(!$show_all)$final_goods = array_slice($final_goods, 0, $num, true);
            $top_cat['goods'] = $final_goods;
            $top_cat['avg_goods_price'] = $cat_avg['avg_goods_price'];
            $result[$cat_id]  = $top_cat;
        }
        return $result;
    }

    function ajaxQueryAction($list = [], $totalCount = 0, $action_btn = ['edit', 'remove'], $extraData = [])
    {
        global $_CFG;
        if (!empty($_REQUEST['edit_lang']))
        {
            $old_lang = $_CFG['lang'];
            $_CFG['lang'] = $_REQUEST['edit_lang'];
        }
        $cat_list = cat_list(0, 0, false);
        $cat_list = $this->sort_cat_list($cat_list, $_REQUEST);
        foreach ($cat_list['cat_list'] as $c_key => $c) {
            $c['_action'] = '';
            if ($c['level'] == 0) $c['_action'] .='<a href="javascript:;" onclick="editMenu('.$c['cat_id'].')">選單設定</a> |';
            $c['_action'] .='<a href="javascript:;" onclick="editImages('.$c['cat_id'].','.$c['level'].')">圖片設定</a> |';
            $c['_action'] .='<a href="attribute.php?act=cat_goods_list&amp;cat_id='.$c['cat_id'].'">'._L('edit_goods_attr', 'edit_goods_attr').'</a> |';
            $c['_action'] .='<a href="category.php?act=move&cat_id='.$c['cat_id'].'">'._L('move_goods', 'move_goods').'</a> |';
            $c['_action'] .='<a href="category.php?act=edit&amp;cat_id='.$c['cat_id'].'">'._L('edit', 'edit').'</a> |';
            $c['_action'] .='<a href="javascript:;" onclick="listTable.remove('.$c['cat_id'].', \''._L('edit', 'drop_confirm').'\')" title="'._L('remove', 'remove').'">'._L('remove', 'remove').'</a>';
            $cat_list['cat_list'][$c_key] = $c;
        }
        $this->tableList = $cat_list['cat_list'];
        $this->tableTotalCount = count($cat_list['cat_list']);

        // Multiple language support
        if (!empty($old_lang))
        {
            $_CFG['lang'] = $old_lang;
        }
        parent::ajaxQueryAction([], '', false);
    }

    
    function get_stock_cost($is_pagination = true)
    {
        $userController   = new UserController();
        $erpController    = new ErpController();
        $orderController  = new OrderController();
        require_once(ROOT_PATH . 'includes/lib_order.php');
        require_once ROOT_PATH . 'includes/lib_howang.php';
        ini_set('memory_limit', '512M');
        $filter['start_date'] = empty($_REQUEST['start_date']) ? strtotime(local_date('Y-m-01')) : strtotime($_REQUEST['start_date']);
        $filter['end_date'] = empty($_REQUEST['end_date']) ? strtotime(local_date('Y-m-t')) : strtotime($_REQUEST['end_date']);
        $filter['order_type'] = empty($_REQUEST['order_type']) ? 0 : intval($_REQUEST['order_type']);
        
        $sql = 'SELECT COUNT(*) FROM ' . $GLOBALS['ecs']->table('category');
        $filter['record_count'] = $GLOBALS['db']->getOne($sql);
        
        /* 分页大小 */
        $filter = page_and_size($filter);
        
        $where = " oi.shipping_time >= '".$filter['start_date']."' AND oi.shipping_time < '" . ($filter['end_date'] + 86400) . "' ".
        order_query_sql('finished', 'oi.');

        // Ignore 換貨訂單
        $where .= " AND oi.postscript NOT LIKE '%換貨，%'";

        if ($filter['order_type'] == 4) // 所有訂單
        {
            $where_ws = "(" . str_replace("oi.pay_status  IN ('", "oi.pay_status  IN ('".PS_UNPAYED."','", explode('AND', order_query_sql('finished', 'oi.'))[3]);
            $where_ws .= " AND (oi.order_type = '".$orderController::DB_ORDER_TYPE_WHOLESALE."' OR (oi.user_id IN (".implode(',', $userController->getWholesaleUserIds()).") AND oi.order_type IS NULL) ) ";
            $where = str_replace("oi.pay_status  IN ('", "( oi.pay_status  IN ('", $where) . " OR " . $where_ws . " ) )";
        }
        else if ($filter['order_type'] == 1) // 批發訂單
        {
            // Only include orders for the followiung user account: 99999999
            $where = str_replace("oi.pay_status  IN ('", "oi.pay_status  IN ('".PS_UNPAYED."','", $where);
            $where .= " AND (oi.order_type = '".$orderController::DB_ORDER_TYPE_WHOLESALE."' OR (oi.user_id IN (".implode(',', $userController->getWholesaleUserIds()).") AND oi.order_type IS NULL) ) ";
        }
        else if ($filter['order_type'] == 2) // 借貨訂單
        {
            // Only include orders for the followiung user accounts: 11111111, 88888888
            $where .= " AND oi.user_id IN (4940,5023) ";
        }
        else if ($filter['order_type'] == 3) // 代理銷售訂單
        {
            $where .= " AND oi.order_type = '".$orderController::DB_ORDER_TYPE_SALESAGENT."' ";
        }
        else if ($filter['order_type'] == 0) // Normal order
        {
            // Exclude orders for the followiung user accounts: 11111111, 88888888, 99999999
            $where .= " AND (oi.order_type NOT IN ('".$orderController::DB_ORDER_TYPE_SALESAGENT."','".$orderController::DB_ORDER_TYPE_WHOLESALE."') OR (oi.order_type IS NULL AND oi.user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).")) ) ";
            $where .= " AND (oi.platform <> 'pos_exhibition') ";
        }
        else if ($filter['order_type'] == 5) // Exhibition order
        {
            // Exclude orders for the followiung user accounts: 11111111, 88888888, 99999999
            $where .= " AND (oi.order_type NOT IN ('".$orderController::DB_ORDER_TYPE_SALESAGENT."','".$orderController::DB_ORDER_TYPE_WHOLESALE."') OR (oi.order_type IS NULL AND oi.user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).")) ) ";
            $where .= " AND (oi.platform = 'pos_exhibition') ";
        }

        // Exclude order with sale exclude coupon
        $where .= " AND (c.sale_exclude = 0 OR c.sale_exclude IS NULL) ";

        $time_yesterday = local_strtotime('today'); // 23:59:59 yesterday
        $time_90days_before = local_strtotime('-90 days midnight'); // 00:00:00 90 days ago
        $sql = 'SELECT a.cat_id, a.cat_name, a.sort_order AS parent_order, a.cat_id, a.is_show,' .
                'b.cat_id AS child_id, b.cat_name AS child_name, b.sort_order AS child_order, ' .
                'c.cat_id AS grandson_id, c.cat_name AS grandson_name, c.sort_order AS grandson_order, ' .
                'IFNULL(sum(gn.goods_number),0) as total_stocks, ' .
                'IFNULL(sum(gn.goods_number * g.cost),0) as total_cost, ' .
                'IFNULL(sum(gp.revenue),0) as total_revenue, ' .
                'IFNULL(sum(gp.revenue - gp.total_discount),0) as total_revenue_n_ds, ' .
                'IFNULL(sum(gp.total_discount),0) as total_discount, ' .
                'IFNULL(sum(gp.cost),0) as total_gpcost, ' .
                'IFNULL(sum(gp.revenue - gp.cost),0) as total_profit, ' .
                'IFNULL(sum(gp.revenue - gp.total_discount - gp.cost),0) as total_profit_n_ds, ' .
                'IFNULL(sum(gp.sap),0) as total_sa_revenue, '.
                'IFNULL(sum(gp.sap - gp.total_discount),0) as total_sa_revenue_n_ds, '.
                'IFNULL(sum(gp.sap - gp.cost),0) as total_sa_profit, ' .
                'IFNULL(sum(gp.sap - gp.total_discount - gp.cost),0) as total_sa_profit_n_ds ' .
            'FROM ' . $GLOBALS['ecs']->table('category') . ' AS a ' .
            'LEFT JOIN ' . $GLOBALS['ecs']->table('category') . ' AS b ON b.parent_id = a.cat_id AND b.is_show = 1 ' .
            'LEFT JOIN ' . $GLOBALS['ecs']->table('category') . ' AS c ON c.parent_id = b.cat_id AND c.is_show = 1 ' .
            'LEFT JOIN ' . $GLOBALS['ecs']->table('goods') . ' AS g ON g.cat_id = c.cat_id AND g.is_delete = 0 ' .
            'LEFT JOIN (SELECT ega.goods_id, sum(ega.goods_qty) as goods_number FROM ' . $GLOBALS['ecs']->table('erp_goods_attr_stock') . ' AS ega WHERE ega.warehouse_id IN ('.$erpController->getSellableWarehouseIds(true).') GROUP BY ega.goods_id) as gn ON gn.goods_id = g.goods_id ' .
            'LEFT JOIN (' .
                'SELECT og.goods_id, SUM((oi2.total_o_ds/oi2.total_og_price)*(og.goods_price * og.goods_number)) as total_discount ,' .
                'sum((IF(og.cost > 0, og.cost, g2.cost) + ' .
                    'IF(oi.`pay_id` = 2,  IF(og.cost > 0, og.cost, g2.cost) * 0.0095,       0) + ' . // EPS: 0.95% fee
                    'IF(oi.`pay_id` = 7,  IF(og.cost > 0, og.cost, g2.cost) * 0.032 + 2.35, 0) + ' . // PayPal: 3.2% fee + $2.35 per txn
                    'IF(oi.`pay_id` = 10, IF(og.cost > 0, og.cost, g2.cost) * 0.029,        0) + ' . // UnionPay: 2.9% fee
                    'IF(oi.`pay_id` = 12, IF(og.cost > 0, og.cost, g2.cost) * 0.023,        0) + ' . // Visa / MasterCard: 2.3% fee
                    'IF(oi.`pay_id` = 13, IF(og.cost > 0, og.cost, g2.cost) * 0.0296,       0)   ' . // American Express: 2.96% fee
                    ') * og.goods_number + ' .
                    'IF(oi.`shipping_id` = 3, 30 * og.goods_price * og.goods_number / oi.goods_amount, 0) ' . // Delivery cost: $30
                ') as cost, ' .
                "sum( (( ((100 - IF(og.sa_rate_id > 0 , sa.sa_rate, 0) ) /100 ) * og.goods_price)*og.goods_number) - IF(oi.`pay_fee` < 0, oi.`pay_fee` * -1 * og.goods_price * og.goods_number / oi.goods_amount, 0) ) as sap, ".
                'sum(og.goods_price * og.goods_number - IF(oi.`pay_fee` < 0, oi.`pay_fee` * -1 * og.goods_price * og.goods_number / oi.goods_amount, 0)) as revenue ' .
                'FROM ' . $GLOBALS['ecs']->table('order_goods') . ' AS og ' .
                'LEFT JOIN ' . $GLOBALS['ecs']->table('order_info') . ' AS oi ON oi.order_id = og.order_id ' .
                'LEFT JOIN ' . $GLOBALS['ecs']->table('goods') . ' AS g2 ON g2.goods_id = og.goods_id ' .
                "LEFT JOIN " . $GLOBALS['ecs']->table('sales_agent_rate') . " as sa ON sa.sa_rate_id = og.sa_rate_id ".
                'LEFT JOIN (SELECT (oi.`discount` + oi.`bonus` + oi.`coupon`)as total_o_ds, sum(og.goods_number * og.goods_price)as total_og_price, oi.order_id from ' . $GLOBALS['ecs']->table('order_info') . ' as oi left join ' . $GLOBALS['ecs']->table('order_goods') . ' AS og ON oi.order_id = og.order_id group by oi.order_id) as oi2 ON oi2.order_id = og.order_id '.
                'LEFT JOIN ' . $GLOBALS['ecs']->table('order_coupons') . ' AS oc ON oc.order_id = oi.order_id ' .
                'LEFT JOIN ' . $GLOBALS['ecs']->table('coupons') . ' AS c ON c.coupon_id = oc.coupon_id ' .
                'WHERE ' .$where.
                'GROUP BY og.goods_id' .
            ') as gp ON gp.goods_id = g.goods_id ' .
            "WHERE a.parent_id = '0' GROUP BY c.cat_id ORDER BY parent_order ASC, a.cat_id ASC, child_order ASC, grandson_order ASC, grandson_id ASC";
        require_once ROOT_PATH . 'includes/lib_howang.php';
        $sql_cat_list = 
        "SELECT eoi.goods_id,g.goods_num,eoi.warehousing_qty as qty,(g.goods_num-eoi.warehousing_qty) as sum,eoi.price as unit_cost ,eo.order_id, g.cat_id, g.child_id, g.grandson_id
        FROM ".$GLOBALS['ecs']->table('erp_order_item')." eoi 
        LEFT JOIN ".$GLOBALS['ecs']->table('erp_order')." eo ON eoi.order_id=eo.order_id ".
        "LEFT JOIN (SELECT g.cat_id as grandson_id, c.parent_id as child_id, gc.parent_id as cat_id,g.goods_id, rg.goods_num FROM ".$GLOBALS['ecs']->table('goods')." as g ".
        " LEFT JOIN ( SELECT ".hw_goods_number_subquery('g.','goods_num').", g.goods_id FROM ".$GLOBALS['ecs']->table('goods')." as g ) as rg ON rg.goods_id = g.goods_id ".
        ' LEFT JOIN ' . $GLOBALS['ecs']->table('category') . ' AS c ON g.cat_id = c.cat_id AND c.is_show = 1 ' .
        ' LEFT JOIN ' . $GLOBALS['ecs']->table('category') . ' AS gc ON c.parent_id = gc.cat_id AND gc.is_show = 1 ' .
        " WHERE rg.goods_num > 0 order by g.goods_id DESC ) as g on g.goods_id = eoi.goods_id ".
        "WHERE g.goods_num > 0 AND eo.order_status = 4 AND eoi.warehousing_qty > 0 ORDER BY eoi.goods_id DESC, eoi.order_item_id DESC";
        $po_list = $GLOBALS['db']->getAll($sql_cat_list);

        $sql_avg_cost = "SELECT type_id AS cat_id, AVG(cost) AS avg_stock_cost FROM " . $GLOBALS['ecs']->table('stock_cost_log') . " WHERE report_date >= " . $filter['start_date'] .
            " AND report_date <= " . ($filter['end_date'] + 86400) . " AND created_at IN (SELECT MAX(created_at) FROM " . $GLOBALS['ecs']->table('stock_cost_log') . " GROUP BY report_date) AND type = 1 GROUP BY cat_id";
        $sql_dead_cost = "SELECT type_id AS cat_id, AVG(slowsoldcost_90) AS avg_dead_cost FROM " . $GLOBALS['ecs']->table('stock_cost_log') . " WHERE report_date >= " . $filter['start_date'] .
            " AND report_date <= " . ($filter['end_date'] + 86400) . " AND created_at IN (SELECT MAX(created_at) FROM " . $GLOBALS['ecs']->table('stock_cost_log') . " GROUP BY report_date) AND type = 1 AND report_date > 1527091200 GROUP BY cat_id";
        $res = $GLOBALS['db']->getAll($sql);
        // foreach($res as $v)if($v['total_discount']>0) var_dump($v);
        $avg_cost = $GLOBALS['db']->getAll($sql_avg_cost);
        $dead_cost = $GLOBALS['db']->getAll($sql_dead_cost);
        $total_stocks = 0;
        $total_cost = 0;
        $total_revenue = 0;
        $total_gpcost = 0;
        $total_profit = 0;
        $cat_arr = array();
        $cost_arr = array();
        $avg_cost_arr = array();
        $dead_cost_arr = array();

        $next_goods = true;
        $goods_id   = 0;
        foreach ($po_list as $key => $po) {
            if($next_goods && $po['goods_id'] == $goods_id)continue;

            // What goods we processing
            $next_goods = false;
            $goods_num = ($po['goods_id'] == $goods_id)?$in_qty:$po['goods_num'];
            $goods_id = $po['goods_id'];
            $in_qty   = $goods_num - $po['qty'];
            if($in_qty > 0) // Stock > PO 入貨數
            {
                $qty  = $po['qty'];
                $cost = $qty * $po['unit_cost'];
            } elseif ($in_qty <= 0)
            {
                $qty  = $goods_num;
                $cost = $qty * $po['unit_cost'];
                $next_goods = true;
            }
            $cost_arr[$po['grandson_id']]['cost'] = isset($cost_arr[$po['grandson_id']]['cost']) ? $cost_arr[$po['grandson_id']]['cost']+= $cost: $cost;
            $cost_arr[$po['grandson_id']]['qty']  = isset($cost_arr[$po['grandson_id']]['qty']) ? $cost_arr[$po['grandson_id']]['qty']+= $qty: $qty;
        }
        foreach($avg_cost as $row){
            $avg_cost_arr[$row['cat_id']] = $row['avg_stock_cost'];
        }
        foreach($dead_cost as $row){
            $dead_cost_arr[$row['cat_id']] = $row['avg_dead_cost'];
        }
        $ungroup = [];
        foreach ($res AS $row)
        {
            // if ($row['is_show'])
            {
                $curr_cost = isset($cost_arr[$row['grandson_id']]['cost']) ? $cost_arr[$row['grandson_id']]['cost'] : 0;
                $curr_avg_cost = isset($avg_cost_arr[$row['grandson_id']]) ? $avg_cost_arr[$row['grandson_id']] : 0;
                $curr_dead_cost = isset($dead_cost_arr[$row['grandson_id']]) ? $dead_cost_arr[$row['grandson_id']] : 0;
                $total_stocks += $row['total_stocks'];
                $total_cost += $row['total_cost'];
                $total_revenue += $row['total_revenue'];
                $total_revenue_n_ds += $row['total_revenue_n_ds'];
                $total_gpcost += $row['total_gpcost'];
                $total_profit += $row['total_profit'];
                $total_profit_n_ds += $row['total_profit_n_ds'];
                $total_sa_revenue += $row['total_sa_revenue'];
                $total_sa_profit += $row['total_sa_profit'];
                $total_sa_revenue_n_ds += $row['total_sa_revenue_n_ds'];
                $total_sa_profit_n_ds += $row['total_sa_profit_n_ds'];
                $total_fifo_cost += $curr_cost;
                $total_avg_cost += $curr_avg_cost;
                $total_dead_cost += $curr_dead_cost;
                
                $cat0 = isset($cat_arr[$row['cat_id']]) ? $cat_arr[$row['cat_id']] : array();
                $cat0['id']   = $row['cat_id'];
                $cat0['name'] = $row['cat_name'];
                $cat0['total_stocks']  = (isset($cat0['total_stocks']) ? $cat0['total_stocks'] : 0) + $row['total_stocks'];
                $cat0['total_cost']  = (isset($cat0['total_cost']) ? $cat0['total_cost'] : 0) + $row['total_cost'];
                $cat0['total_cost_formated'] = price_format($cat0['total_cost'], false);
                $cat0['total_revenue']  = (isset($cat0['total_revenue']) ? $cat0['total_revenue'] : 0) + $row['total_revenue'];
                $cat0['total_revenue_formated'] = price_format($cat0['total_revenue'], false);
                $cat0['total_revenue_n_ds']  = (isset($cat0['total_revenue_n_ds']) ? $cat0['total_revenue_n_ds'] : 0) + $row['total_revenue_n_ds'];
                $cat0['total_revenue_n_ds_formated'] = price_format($cat0['total_revenue_n_ds'], false);
                $cat0['total_gpcost']  = (isset($cat0['total_gpcost']) ? $cat0['total_gpcost'] : 0) + $row['total_gpcost'];
                $cat0['total_profit']  = (isset($cat0['total_profit']) ? $cat0['total_profit'] : 0) + $row['total_profit'];
                $cat0['total_profit_formated'] = price_format($cat0['total_profit'], false);
                $cat0['profit_margin'] = $cat0['total_revenue'] > 0 ? ($cat0['total_profit'] / $cat0['total_revenue']) : 0;
                $cat0['profit_margin_formated'] = number_format($cat0['profit_margin'] * 100, 2, '.', '') . '%';
                $cat0['total_profit_n_ds']  = (isset($cat0['total_profit_n_ds']) ? $cat0['total_profit_n_ds'] : 0) + $row['total_profit_n_ds'];
                $cat0['total_profit_n_ds_formated'] = price_format($cat0['total_profit_n_ds'], false);
                $cat0['profit_margin_n_ds'] = $cat0['total_revenue_n_ds'] > 0 ? ($cat0['total_profit_n_ds'] / $cat0['total_revenue_n_ds']) : 0;
                $cat0['profit_margin_n_ds_formated'] = number_format($cat0['profit_margin_n_ds'] * 100, 2, '.', '') . '%';
                $cat0['total_fifo_cost']  = (isset($cat0['total_fifo_cost']) ? $cat0['total_fifo_cost'] : 0) + $curr_cost;
                $cat0['total_fifo_cost_formated']  = price_format($cat0['total_fifo_cost'], false);
                $cat0['avg_stock_cost']  = (isset($cat0['avg_stock_cost']) ? $cat0['avg_stock_cost'] : 0) + $curr_avg_cost;
                $cat0['avg_stock_cost_formated']  = price_format($cat0['avg_stock_cost'], false);
                $cat0['total_dead_cost']  = (isset($cat0['total_dead_cost']) ? $cat0['total_dead_cost'] : 0) + $curr_dead_cost;
                $cat0['total_dead_cost_formated']  = price_format($cat0['total_dead_cost'], false);
                
                $cat0['total_sa_revenue_n_ds']  = (isset($cat0['total_sa_revenue_n_ds']) ? $cat0['total_sa_revenue_n_ds'] : 0) + $row['total_sa_revenue_n_ds'];
                $cat0['total_sa_revenue_n_ds_formated'] = price_format($cat0['total_sa_revenue_n_ds'], false);
                $cat0['total_sa_profit_n_ds']  = (isset($cat0['total_sa_profit_n_ds']) ? $cat0['total_sa_profit_n_ds'] : 0) + $row['total_sa_profit_n_ds'];
                $cat0['total_sa_profit_n_ds_formated'] = price_format($cat0['total_sa_profit_n_ds'], false);
                $cat0['sa_profit_margin_n_ds'] = $cat0['total_sa_revenue'] > 0 ? ($cat0['total_sa_profit_n_ds'] / $cat0['total_sa_revenue_n_ds']) : 0;
                $cat0['sa_profit_margin_n_ds_formated'] = number_format($cat0['sa_profit_margin_n_ds'] * 100, 2, '.', '') . '%';
                $cat0['total_sa_revenue']  = (isset($cat0['total_sa_revenue']) ? $cat0['total_sa_revenue'] : 0) + $row['total_sa_revenue'];
                $cat0['total_sa_revenue_formated'] = price_format($cat0['total_sa_revenue'], false);
                $cat0['total_sa_profit']  = (isset($cat0['total_sa_profit']) ? $cat0['total_sa_profit'] : 0) + $row['total_sa_profit'];
                $cat0['total_sa_profit_formated'] = price_format($cat0['total_sa_profit'], false);
                $cat0['sa_profit_margin'] = $cat0['total_sa_revenue'] > 0 ? ($cat0['total_sa_profit'] / $cat0['total_sa_revenue']) : 0;
                $cat0['sa_profit_margin_formated'] = number_format($cat0['sa_profit_margin'] * 100, 2, '.', '') . '%';
                $cat0['display'] = isset($_REQUEST['order_type']) ? $_REQUEST['order_type'] : 4;

                if ($row['child_id'] != NULL)
                {
                    $cat1 = isset($cat0['children'][$row['child_id']]) ? $cat0['children'][$row['child_id']] : array();
                    $cat1['id']   = $row['child_id'];
                    $cat1['name'] = $row['child_name'];
                    
                    if ($row['grandson_id'] != NULL)
                    {
                        $cat1['total_stocks']  = (isset($cat1['total_stocks']) ? $cat1['total_stocks'] : 0) + $row['total_stocks'];
                        $cat1['total_cost']  = (isset($cat1['total_cost']) ? $cat1['total_cost'] : 0) + $row['total_cost'];
                        $cat1['total_cost_formated'] = price_format($cat1['total_cost'], false);
                        $cat1['total_fifo_cost']  = (isset($cat1['total_fifo_cost']) ? $cat1['total_fifo_cost'] : 0) + $curr_cost;
                        $cat1['total_fifo_cost_formated'] = price_format($cat1['total_fifo_cost'], false);
                        $cat1['avg_stock_cost']  = (isset($cat1['avg_stock_cost']) ? $cat1['avg_stock_cost'] : 0) + $curr_avg_cost;
                        $cat1['avg_stock_cost_formated'] = price_format($cat1['avg_stock_cost'], false);
                        $cat1['total_dead_cost']  = (isset($cat1['total_dead_cost']) ? $cat1['total_dead_cost'] : 0) + $curr_dead_cost;
                        $cat1['total_dead_cost_formated'] = price_format($cat1['total_dead_cost'], false);
                        $cat1['total_revenue']  = (isset($cat1['total_revenue']) ? $cat1['total_revenue'] : 0) + $row['total_revenue'];
                        $cat1['total_revenue_formated'] = price_format($cat1['total_revenue'], false);
                        $cat1['total_revenue_n_ds']  = (isset($cat1['total_revenue_n_ds']) ? $cat1['total_revenue_n_ds'] : 0) + $row['total_revenue_n_ds'];
                        $cat1['total_revenue_n_ds_formated'] = price_format($cat1['total_revenue_n_ds'], false);
                        $cat1['total_gpcost']  = (isset($cat1['total_gpcost']) ? $cat1['total_gpcost'] : 0) + $row['total_gpcost'];
                        $cat1['total_profit']  = (isset($cat1['total_profit']) ? $cat1['total_profit'] : 0) + $row['total_profit'];
                        $cat1['total_profit_formated'] = price_format($cat1['total_profit'], false);
                        $cat1['profit_margin'] = $cat1['total_revenue'] > 0 ? ($cat1['total_profit'] / $cat1['total_revenue']) : 0;
                        $cat1['profit_margin_formated'] = number_format($cat1['profit_margin'] * 100, 2, '.', '') . '%';
                        $cat1['total_profit_n_ds']  = (isset($cat1['total_profit_n_ds']) ? $cat1['total_profit_n_ds'] : 0) + $row['total_profit_n_ds'];
                        $cat1['total_profit_n_ds_formated'] = price_format($cat1['total_profit_n_ds'], false);
                        $cat1['profit_margin_n_ds'] = $cat1['total_revenue_n_ds'] > 0 ? ($cat1['total_profit_n_ds'] / $cat1['total_revenue_n_ds']) : 0;
                        $cat1['profit_margin_n_ds_formated'] = number_format($cat1['profit_margin_n_ds'] * 100, 2, '.', '') . '%';
                        $cat1['total_sa_revenue_n_ds']  = (isset($cat1['total_sa_revenue_n_ds']) ? $cat1['total_sa_revenue_n_ds'] : 0) + $row['total_sa_revenue_n_ds'];
                        $cat1['total_sa_revenue_n_ds_formated'] = price_format($cat1['total_sa_revenue_n_ds'], false);
                        $cat1['total_sa_profit_n_ds']  = (isset($cat1['total_sa_profit_n_ds']) ? $cat1['total_sa_profit_n_ds'] : 0) + $row['total_sa_profit_n_ds'];
                        $cat1['total_sa_profit_n_ds_formated'] = price_format($cat1['total_sa_profit_n_ds'], false);
                        $cat1['sa_profit_margin_n_ds'] = $cat1['total_sa_revenue_n_ds'] > 0 ? ($cat1['total_sa_profit_n_ds'] / $cat1['total_sa_revenue_n_ds']) : 0;
                        $cat1['sa_profit_margin_n_ds_formated'] = number_format($cat1['sa_profit_margin_n_ds'] * 100, 2, '.', '') . '%';
                        $cat1['total_sa_revenue']  = (isset($cat1['total_sa_revenue']) ? $cat1['total_sa_revenue'] : 0) + $row['total_sa_revenue'];
                        $cat1['total_sa_revenue_formated'] = price_format($cat1['total_sa_revenue'], false);
                        $cat1['total_sa_profit']  = (isset($cat1['total_sa_profit']) ? $cat1['total_sa_profit'] : 0) + $row['total_sa_profit'];
                        $cat1['total_sa_profit_formated'] = price_format($cat1['total_sa_profit'], false);
                        $cat1['sa_profit_margin'] = $cat1['total_sa_revenue'] > 0 ? ($cat1['total_sa_profit'] / $cat1['total_sa_revenue']) : 0;
                        $cat1['sa_profit_margin_formated'] = number_format($cat1['sa_profit_margin'] * 100, 2, '.', '') . '%';
                        $cat1['display'] = isset($_REQUEST['order_type']) ? $_REQUEST['order_type'] : 4;
                        
                        $cat2 = isset($cat1['children'][$row['grandson_id']]) ? $cat1['children'][$row['grandson_id']] : array();
                        $cat2['id']   = $row['grandson_id'];
                        $cat2['name'] = $row['grandson_name'];
                        $cat2['total_stocks']  = intval($row['total_stocks']);
                        $cat2['total_cost']  = $row['total_cost'];
                        $cat2['total_cost_formated'] = price_format($cat2['total_cost'], false);
                        $cat2['total_fifo_cost']  = $curr_cost;
                        $cat2['total_fifo_cost_formated'] = price_format($cat2['total_fifo_cost'], false);
                        $cat2['avg_stock_cost']  = $curr_avg_cost;
                        $cat2['avg_stock_cost_formated'] = price_format($cat2['avg_stock_cost'], false);
                        $cat2['total_dead_cost']  = $curr_dead_cost;
                        $cat2['total_dead_cost_formated'] = price_format($cat2['total_dead_cost'], false);
                        $cat2['total_revenue']  = $row['total_revenue'];
                        $cat2['total_revenue_formated'] = price_format($cat2['total_revenue'], false);
                        $cat2['total_revenue_n_ds']  = $row['total_revenue_n_ds'];
                        $cat2['total_revenue_n_ds_formated'] = price_format($cat2['total_revenue_n_ds'], false);
                        $cat2['total_gpcost']  = $row['total_gpcost'];
                        $cat2['total_profit']  = $row['total_profit'];
                        $cat2['total_profit_formated'] = price_format($cat2['total_profit'], false);
                        $cat2['profit_margin'] = $cat2['total_revenue'] > 0 ? ($cat2['total_profit'] / $cat2['total_revenue']) : 0;
                        $cat2['profit_margin_formated'] = number_format($cat2['profit_margin'] * 100, 2, '.', '') . '%';
                        $cat2['total_profit_n_ds']  = $row['total_profit_n_ds'];
                        $cat2['total_profit_n_ds_formated'] = price_format($cat2['total_profit_n_ds'], false);
                        $cat2['profit_margin_n_ds'] = $cat2['total_revenue_n_ds'] > 0 ? ($cat2['total_profit_n_ds'] / $cat2['total_revenue_n_ds']) : 0;
                        $cat2['profit_margin_n_ds_formated'] = number_format($cat2['profit_margin_n_ds'] * 100, 2, '.', '') . '%';
                        $cat2['total_sa_revenue']  = (isset($cat2['total_sa_revenue']) ? $cat2['total_sa_revenue'] : 0) + $row['total_sa_revenue'];
                        $cat2['total_sa_revenue_formated'] = price_format($cat2['total_sa_revenue'], false);
                        $cat2['total_sa_profit']  = (isset($cat2['total_sa_profit']) ? $cat2['total_sa_profit'] : 0) + $row['total_sa_profit'];
                        $cat2['total_sa_profit_formated'] = price_format($cat2['total_sa_profit'], false);
                        $cat2['sa_profit_margin'] = $cat2['total_sa_revenue'] > 0 ? ($cat2['total_sa_profit'] / $cat2['total_sa_revenue']) : 0;
                        $cat2['sa_profit_margin_formated'] = number_format($cat2['sa_profit_margin'] * 100, 2, '.', '') . '%';
                        $cat2['total_sa_revenue_n_ds']  = (isset($cat2['total_sa_revenue_n_ds']) ? $cat2['total_sa_revenue_n_ds'] : 0) + $row['total_sa_revenue_n_ds'];
                        $cat2['total_sa_revenue_n_ds_formated'] = price_format($cat2['total_sa_revenue_n_ds'], false);
                        $cat2['total_sa_profit_n_ds']  = (isset($cat2['total_sa_profit_n_ds']) ? $cat2['total_sa_profit_n_ds'] : 0) + $row['total_sa_profit_n_ds'];
                        $cat2['total_sa_profit_n_ds_formated'] = price_format($cat2['total_sa_profit_n_ds'], false);
                        $cat2['sa_profit_margin_n_ds'] = $cat2['total_sa_revenue_n_ds'] > 0 ? ($cat2['total_sa_profit_n_ds'] / $cat2['total_sa_revenue_n_ds']) : 0;
                        $cat2['sa_profit_margin_n_ds_formated'] = number_format($cat2['sa_profit_margin_n_ds'] * 100, 2, '.', '') . '%';
                        $cat2['display'] = isset($_REQUEST['order_type']) ? $_REQUEST['order_type'] : 4;
                        
                        $cat1['children'][$row['grandson_id']] = $cat2;
                    }
                    else
                    {
                        $cat1['total_stocks']  = $row['total_stocks'];
                        $cat1['total_cost']  = $row['total_cost'];
                        $cat1['total_cost_formated'] = price_format($cat1['total_cost'], false);
                        $cat1['total_fifo_cost']  = $curr_cost;
                        $cat1['total_fifo_cost_formated'] = price_format($cat1['total_fifo_cost'], false);
                        $cat1['avg_stock_cost']  = $curr_avg_cost;
                        $cat1['avg_stock_cost_formated'] = price_format($cat1['avg_stock_cost'], false);
                        $cat1['total_dead_cost']  = $curr_avg_cost;
                        $cat1['total_dead_cost_formated'] = price_format($cat1['total_dead_cost'], false);
                        $cat1['total_revenue']  = $row['total_revenue'];
                        $cat1['total_revenue_formated'] = price_format($cat1['total_revenue'], false);
                        $cat1['total_revenue_n_ds']  = $row['total_revenue_n_ds'];
                        $cat1['total_revenue_n_ds_formated'] = price_format($cat1['total_revenue_n_ds'], false);
                        $cat1['total_gpcost']  = $row['total_gpcost'];
                        $cat1['total_profit']  = $row['total_profit'];
                        $cat1['total_profit_formated'] = price_format($cat1['total_profit'], false);
                        $cat1['profit_margin'] = $cat1['total_revenue'] > 0 ? ($cat1['total_profit'] / $cat1['total_revenue']) : 0;
                        $cat1['profit_margin_formated'] = number_format($cat1['profit_margin'] * 100, 2, '.', '') . '%';
                        $cat1['total_profit_n_ds']  = $row['total_profit_n_ds'];
                        $cat1['total_profit_n_ds_formated'] = price_format($cat1['total_profit_n_ds'], false);
                        $cat1['profit_margin_n_ds'] = $cat1['total_revenue_n_ds'] > 0 ? ($cat1['total_profit_n_ds'] / $cat1['total_revenue_n_ds']) : 0;
                        $cat1['profit_margin_n_ds_formated'] = number_format($cat1['profit_margin_n_ds'] * 100, 2, '.', '') . '%';
                        $cat1['total_sa_revenue_n_ds']  = (isset($cat1['total_sa_revenue_n_ds']) ? $cat1['total_sa_revenue_n_ds'] : 0) + $row['total_sa_revenue_n_ds'];
                        $cat1['total_sa_revenue_n_ds_formated'] = price_format($cat1['total_sa_revenue_n_ds'], false);
                        $cat1['total_sa_profit_n_ds']  = (isset($cat1['total_sa_profit_n_ds']) ? $cat1['total_sa_profit_n_ds'] : 0) + $row['total_sa_profit_n_ds'];
                        $cat1['total_sa_profit_n_ds_formated'] = price_format($cat1['total_sa_profit_n_ds'], false);
                        $cat1['sa_profit_margin_n_ds'] = $cat1['total_sa_revenue_n_ds'] > 0 ? ($cat1['total_sa_profit_n_ds'] / $cat1['total_sa_revenue_n_ds']) : 0;
                        $cat1['sa_profit_margin_n_ds_formated'] = number_format($cat1['sa_profit_margin_n_ds'] * 100, 2, '.', '') . '%';
                        $cat1['total_sa_revenue']  = (isset($cat1['total_sa_revenue']) ? $cat1['total_sa_revenue'] : 0) + $row['total_sa_revenue'];
                        $cat1['total_sa_revenue_formated'] = price_format($cat1['total_sa_revenue'], false);
                        $cat1['total_sa_profit']  = (isset($cat1['total_sa_profit']) ? $cat1['total_sa_profit'] : 0) + $row['total_sa_profit'];
                        $cat1['total_sa_profit_formated'] = price_format($cat1['total_sa_profit'], false);
                        $cat1['sa_profit_margin'] = $cat1['total_sa_revenue'] > 0 ? ($cat1['total_sa_profit'] / $cat1['total_sa_revenue']) : 0;
                        $cat1['sa_profit_margin_formated'] = number_format($cat1['sa_profit_margin'] * 100, 2, '.', '') . '%';
                        $cat1['display'] = isset($_REQUEST['order_type']) ? $_REQUEST['order_type'] : 4;
                    }
                    
                    $cat0['children'][$row['child_id']] = $cat1;
                }
                
                $cat_arr[$row['cat_id']] = $cat0;
            }
        }	
        array_unshift($cat_arr, array(
            'id' => 0,
            'name' => '所有分類',
            'total_stocks' => $total_stocks,
            'total_cost' => $total_cost,
            'total_cost_formated' => price_format($total_cost, false),
            'total_revenue' => $total_revenue,
            'total_revenue_formated' => price_format($total_revenue, false),
            'total_revenue_n_ds' => $total_revenue_n_ds,
            'total_revenue_n_ds_formated' => price_format($total_revenue_n_ds, false),
            'total_gpcost' => $total_gpcost,
            'total_profit' => $total_profit,
            'total_profit_formated' => price_format($total_profit, false),
            'profit_margin' => $total_revenue > 0 ? $total_profit / $total_revenue : 0,
            'profit_margin_formated' => number_format(($total_revenue > 0 ? $total_profit / $total_revenue : 0) * 100, 2, '.', '') . '%',
            'total_profit_n_ds' => $total_profit_n_ds,
            'total_profit_n_ds_formated' => price_format($total_profit_n_ds, false),
            'profit_margin_n_ds' => $total_revenue_n_ds > 0 ? $total_profit_n_ds / $total_revenue_n_ds : 0,
            'profit_margin_n_ds_formated' => number_format(($total_revenue_n_ds > 0 ? $total_profit_n_ds / $total_revenue_n_ds : 0) * 100, 2, '.', '') . '%',
            'total_sa_revenue' => $total_sa_revenue,
            'total_sa_revenue_formated' => price_format($total_sa_revenue, false),
            'total_sa_profit' => $total_sa_profit,
            'total_sa_profit_formated' => price_format($total_sa_profit, false),
            'total_sa_revenue_n_ds' => $total_sa_revenue_n_ds,
            'total_sa_revenue_n_ds_formated' => price_format($total_sa_revenue_n_ds, false),
            'total_sa_profit_n_ds' => $total_sa_profit_n_ds,
            'total_sa_profit_n_ds_formated' => price_format($total_sa_profit_n_ds, false),
            'total_fifo_cost' => $total_fifo_cost,
            'total_fifo_cost_formated' => price_format($total_fifo_cost, false),
            'avg_stock_cost' => $total_avg_cost,
            'avg_stock_cost_formated' => price_format($total_avg_cost, false),
            'total_dead_cost' => $total_dead_cost,
            'total_dead_cost_formated' => price_format($total_dead_cost, false),
            'sa_profit_margin' => $total_sa_revenue > 0 ? $total_sa_profit / $total_sa_revenue : 0,
            'sa_profit_margin_formated' => number_format(($total_sa_revenue > 0 ? $total_sa_profit / $total_sa_revenue : 0) * 100, 2, '.', '') . '%',
            'sa_profit_margin_n_ds' => $total_sa_revenue_n_ds > 0 ? $total_sa_profit_n_ds / $total_sa_revenue_n_ds : 0,
            'sa_profit_margin_n_ds_formated' => number_format(($total_sa_revenue_n_ds > 0 ? $total_sa_profit_n_ds / $total_sa_revenue_n_ds : 0) * 100, 2, '.', '') . '%',
            'display' => isset($_REQUEST['order_type']) ? $_REQUEST['order_type'] : 4
        ));
        foreach($cat_arr as $key => $value){
            $cat_arr[$key]['turnover'] = $cat_arr[$key]['total_sa_revenue'] / $cat_arr[$key]['avg_stock_cost'] !== false ? number_format($cat_arr[$key]['total_sa_revenue'] / $cat_arr[$key]['avg_stock_cost'],3) : "--";
            $ungroup[$key] = $cat_arr[$key];
            foreach($value as $ke => $val){
                foreach($val as $k => $v){
                    $cat_arr[$key][$ke][$k]['turnover'] = $cat_arr[$key][$ke][$k]['total_sa_revenue'] / $cat_arr[$key][$ke][$k]['avg_stock_cost'] !== false ? number_format($cat_arr[$key][$ke][$k]['total_sa_revenue'] / $cat_arr[$key][$ke][$k]['avg_stock_cost'],3) : "--";
                    $ungroup[$k] = $cat_arr[$key][$ke][$k];
                    if(isset($v['children'])){
                        foreach($v['children'] as $i => $v2){
                            $cat_arr[$key][$ke][$k]['children'][$i]['turnover'] = $cat_arr[$key][$ke][$k]['children'][$i]['total_sa_revenue'] / $cat_arr[$key][$ke][$k]['children'][$i]['avg_stock_cost'];
                            $cat_arr[$key][$ke][$k]['children'][$i]['turnover'] = $cat_arr[$key][$ke][$k]['children'][$i]['turnover'] !== false ? number_format($cat_arr[$key][$ke][$k]['children'][$i]['turnover'],3) : "--";
                            $ungroup[$i] = $cat_arr[$key][$ke][$k]['children'][$i];
                        }
                    }
                }
            }
        }
        /*foreach($cat_arr as $key => $value){
            foreach($value as $ke => $val){
                foreach($val as $k => $v){
                    if(isset($v['children'])){
                        foreach($v['children'] as $v2){
                            $add1 = false;
                            foreach($res_cost as $cost){
                                if($cost['cat_id'] == $v2['id']){
                                    $v2['fifo_cost'] = $cost['final_stock_cost'];
                                    $v['fifo_cost'] += $cost['final_stock_cost'];
                                    $add1 = true;
                                    $add0 = true;
                                }
                            }
                            if($add1 == false){
                                $v2['fifo_cost'] = "0";
                            }
                            $v2['fifo_cost_formated'] = price_format($v2['fifo_cost'], false);
                            $cat_arr[$key][$ke][$k]['children'] = $v2;
                        }
                        if($add0 == false){
                            $v['fifo_cost'] = "0";
                        }
                        $v['fifo_cost_formated'] = price_format($v['fifo_cost'], false);
                        $cat_arr[$key][$ke][$k] = $v;var_dump($cat_arr[$key][$ke][$k]);
                    }
                    $cat_arr[$key][$ke] = $val;
                }
                $cat_arr[$key] = $value;
                var_dump($cat_arr[$key]);
            }
        }die();*/
        $sql = 'SELECT sum(`discount` + `bonus` + `coupon`) ' .
            'FROM ' . $GLOBALS['ecs']->table('order_info') . ' AS oi ' .
            'LEFT JOIN ' . $GLOBALS['ecs']->table('order_coupons') . ' AS oc ON oc.order_id = oi.order_id ' .
            'LEFT JOIN ' . $GLOBALS['ecs']->table('coupons') . ' AS c ON c.coupon_id = oc.coupon_id ' .
            'WHERE '. $where; // Exclude orders from 11111111, 88888888, 99999999
        $total_discount = $GLOBALS['db']->getOne($sql);
        $total_discount = price_format($total_discount, false);
        $arr = array(
            'stock_cost_data' => $cat_arr,
            'total_discount' => $total_discount,
            'filter' => $filter,
            'page_count' => $filter['page_count'],
            'record_count' => $filter['record_count'],
            'ungroup' => $ungroup,
        );
        return $arr;
    }

    function getCategoryPermaLink($cat_id, $tree = [])
    {
        $cperma = "";
        if (!empty($cat_id)) {
            foreach ($tree as $pcid => $parent) {
                if (!empty($cperma)) continue;
                if ($pcid == $cat_id) {
                    $cperma = $parent['cperma'];
                } else {
                    foreach ($parent['children'] as $ccid => $child) {
                        if (!empty($cperma)) continue;
                        if ($ccid == $cat_id) {
                            $cperma = $child['cperma'];
                        } else {
                            foreach ($child['children'] as $gcid => $gchild) {
                                if (!empty($cperma)) continue;
                                if ($gcid == $cat_id) {
                                    $cperma = $gchild['cperma'];
                                }
                            }
                        }
                    }
                }
            }
            if (empty($cperma)) {
                $cperma = $this->db->getOne("SELECT perma_link FROM " . $this->ecs->table('perma_link') . " WHERE id = $cat_id AND table_name = 'category' AND lang = '" . $GLOBALS['_CFG']['lang'] . "'");
            }
        }
        return $cperma;
    }
}