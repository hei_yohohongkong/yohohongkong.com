<?php

/***
* ctrl_template.php
* by MichaelHui 20170328
*
* YOHO - TemplateController
***/
namespace Yoho\cms\Controller;
if (!defined('IN_ECS')){
	die('Hacking attempt');
}

//require necessary class
//require_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/cls_salesperson.php');


class TemplateController extends YohoBaseController{
	private $smarty,$breadCrumb;

	public function __construct(){
		global $smarty;
		$this->smarty=$smarty;
	}

	public function initbreadCrumb($data){
		$this->breadCrumb=$data;
	}

	private function _buildBreadcrumb($currentPageKey){
		$path=[];
		foreach($this->breadCrumb as $parentKey=>$parentData){
			foreach($parentData['child'] as $childKey=>$childData){
				if($currentPageKey==$childKey)
					$path[]=['url'=>$parentData['url'],'text'=>$parentKey];
				foreach($childData['child'] as $child2Key=>$child2Data){
					$path[]=['url'=>$childData['url'],'text'=>$childKey];
				}
			}
		}

		$this->smarty->assign('breadcrumb',$path);
	}

	public function smartyView($file,$params,$currentPageKey=null){
		foreach($params as $k=>$v)
			$this->smarty->assign($k,$v);
		
		if(isset($currentPageKey))
			$this->_buildBreadcrumb($currentPageKey);
		
		$this->smarty->display($file);
	}
}