<?php

/***
* ctrl_task.php
* by MichaelHui 20170328
*
* Task controller
***/

namespace Yoho\cms\Controller;

if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

class TaskController extends YohoBaseController{
	/*
	TODO: separate model stuff from controller later
	*/
	const STATUS_NEW = 0;
	const STATUS_INPROGRESS = 1;
	const STATUS_ONHOLD = 2;
	const STATUS_WONTFIX = 3;
	const STATUS_REVIEW = 4;
	const STATUS_RESOLVED = 5;
	const STATUS_ARCHIVED = 6;
	const ACTION_CODE_ADVANCED_STATUS = 'task_adv_status';
	public $statusKey=['新工作','進行中','暫緩或等待中','不需處理','完成並提交覆核','已完成'];
	public $markDownPrefix = ['#PO'=>'採購單號:','#'=>'訂單號:'];
	private $tablename='admin_tasklog';
	private $currentUserId;
	private $unreadCacheKey=['admin','uid',null,'unreadcnt'];
	private static $priorityMappings = array('Low' => '低','Normal' => '普通','High' => '高','Urgent' => '緊急','Immediate' => '即時');	
	private static $estimatedTimeMappings = array(10 => '10 mins',20 => '20 mins',30 => '30 mins',40 => '40 mins',50 => '50 mins',60 => '1 hour',90 => '1.5 hours',120 => '2 hours',180 => '3 hours',240 => '4 hours',300 => '5 hours',360 => '6 hours',420 => '7 hours',480 => '8 hours');

	public function __construct(){
		parent::__construct();
		$this->tableName = 'admin_tasklog';
		$this->tablename=$this->ecs->table($this->tablename);
		$this->currentUserId=@intval($_SESSION['admin_id']);
		if($this->currentUserId<=0) return false;
	}

	public function canUpdateStatus($currentStatusId){
		if(!check_authz(self::ACTION_CODE_ADVANCED_STATUS) && in_array($currentStatusId, [self::STATUS_RESOLVED,self::STATUS_ARCHIVED]))
			return false;
		return true;
	}


	public function getStatusList($withPermission=false,$currentStatusId=null){
		$out = [];
		foreach ($this->statusKey as $key => $value) {
			if($withPermission==false){
				$out[$key]=$value;
			}else{
				if(!check_authz(self::ACTION_CODE_ADVANCED_STATUS) && in_array($value, ['已完成']))
					continue;

				$out[$key]=$value;
			}
		}
		return $out;
	}

	public function getCacheUnreadCount($uid){
		$this->unreadCacheKey[2]=$uid;
		return $this->memcache->get($this->genCacheKey($this->unreadCacheKey));
	}

	public function getUserUnreadTaskIds($userId=0){
		$userId=intval($userId);
		if($userId==0) $userId=$this->currentUserId;
		$q="SELECT t.task_id FROM ".$this->ecs->table('admin_taskes')." t, ".$this->ecs->table('admin_tasklog')." tl WHERE  t.task_id=tl.task_id AND (tl.sender_read=false AND t.sender_id=".$userId." OR tl.receiver_read=false AND t.receiver_id=".$userId.")";
		$res = $this->db->query($q);

		if(mysql_num_rows($res)>0){
			$out=[];
			while($r=mysql_fetch_assoc($res)){
				$out[]=$r['task_id'];
			}
			return $out;
		}else{
			return [];
		}
	}

	private function _checkPermission($targetStatus){
		if(check_authz(self::ACTION_CODE_ADVANCED_STATUS) || (!check_authz(self::ACTION_CODE_ADVANCED_STATUS) && !in_array($targetStatus, ['Resolved','Archived'])))
			return true;
		return sys_msg('You are not allowed to set task as this status.',1);
	}

	public function addTaskLog($taskId,$content,$newStatus){
		$taskId=intval($taskId);
		//get task info and make sure it's a valid one
		$q = "SELECT * FROM ".$this->ecs->table('admin_taskes')." WHERE ".(check_authz(self::ACTION_CODE_ADVANCED_STATUS)?'1':"(sender_id=".$this->currentUserId." OR receiver_id=".$this->currentUserId.")")." AND task_id=".$taskId;
		$task = mysql_fetch_assoc($this->db->query($q));
		if($task!==false && $task['task_id']!=''){
			//handle read/unread indication
			$senderRead=($this->currentUserId==$task['sender_id'])?1:0;
			$receiverRead=($this->currentUserId==$task['receiver_id'])?1:0;
		}else{
			return false;
		}
		
		//assert action permission
		if($this->_checkPermission($newStatus)==false) return false;

		//DB insert
		$this->db->query('SET NAMES utf8');
		$q="INSERT INTO ".$this->tablename." (task_id,sender_read,receiver_read,content,new_status,created_by) VALUE (".$task['task_id'].",".$senderRead.",".$receiverRead.",'".$content."','".$newStatus."',".$this->currentUserId.")";
		if($this->db->query($q)){
			$tasklogId = $this->db->Insert_ID();

			//update unread cache
			$this->_insertCacheUnread($this->currentUserId==$task['sender_id'] ? $task['receiver_id']:$task['sender_id']);

			//Update task status
			$addQuery='';
			if(in_array($newStatus, [self::STATUS_REVIEW,self::STATUS_WONTFIX]))
				$addQuery = " , finish_time='".gmtime()."' ";
			elseif(in_array($newStatus, [self::STATUS_ARCHIVED]))
				$addQuery = " , deleted=TRUE";

			$q = "UPDATE ".$this->ecs->table('admin_taskes')." SET status='".$newStatus."' ".$addQuery." WHERE task_id=".$taskId;
			
			//we should use transaction here , but u know.. the system....
			//remove log if update failed
			//TODO
			if($this->db->query($q)){
				return true;
			}else{
				$this->db->query("DELETE FROM ".$this->tablename." WHERE tasklog_id=".$tasklogId);
				return false;
			}
		}else{
			return false;
		}
	}

	public function clearMyUnread(){
		$this->unreadCacheKey[2]=$this->currentUserId;
		$this->memcache->set($this->genCacheKey($this->unreadCacheKey),0);
	}

	private function _insertCacheUnread($uid=null){
		if(!isset($uid)) $uid=$this->currentUserId;
		$this->unreadCacheKey[2]=$uid;

		if($this->memcache->get($this->genCacheKey($this->unreadCacheKey))==false){
			if(!$this->memcache->set($this->genCacheKey($this->unreadCacheKey),1))
				return false; //TODO: system log for memcache not running!
		}else{
			$this->memcache->increment($this->genCacheKey($this->unreadCacheKey));
		}
	}

	public function getTaskLogs($taskId){
		$taskId=intval($taskId);
		$orderController = new OrderController();

		$q="SELECT t.*,u.user_name FROM ".$this->tablename." t, ".$this->ecs->table('admin_user')." u WHERE t.created_by=u.user_id AND t.task_id=".$taskId." ORDER BY t.tasklog_id DESC";
		$res = $this->db->query($q);
		if(mysql_num_rows($res)>0){
			$out=[];
			
			while($r=mysql_fetch_assoc($res)){
				foreach($this->markDownPrefix as $k=>$v){

					//markdown stuff
					preg_match_all('/'.$k.'[A-Z0-9]*/', $r['content'], $matches, PREG_OFFSET_CAPTURE);
					foreach($matches as $m){
						foreach($m as $m2){
							//in this case, order is important
							if($k=='#PO'){
								$actualId = $orderController->getPOIdBySn('PO'.str_replace($k, '', $m2[0]));
								if($actualId!=false)
									$replaceEle = '<a href="erp_order_manage.php?act=view_order&order_id='.$actualId.'">'.str_replace($k, $v.'PO', $m2[0])."</a>";
							}elseif($k=='#'){
								$actualId = $orderController->getOrderIdBySn(str_replace($k, '', $m2[0]));
								if($actualId!=false)
									$replaceEle = '<a href="order.php?act=info&order_id='.$actualId.'">'.str_replace($k, $v, $m2[0])."</a>";
							}
							
							if(isset($replaceEle))
								$r['content']=str_replace($m2[0], $replaceEle, $r['content']);

							unset($replaceEle);
							unset($actualId);
						}
					}

				}
				$out[]=$r;
			}
			return $out;
		}else{
			return [];
		}
	}

	public function getCurrentUserUnreadCount(){
		$q = "SELECT tl.tasklog_id FROM ".$this->tablename." tl, ".$this->ecs->table('admin_taskes')." t WHERE t.task_id=tl.task_id AND (t.sender_id=".$this->currentUserId." AND tl.sender_read=0 OR t.receiver_id=".$this->currentUserId." AND tl.receiver_read=0)";
		return mysql_num_rows($this->db->query($q));
	}

	public function clearCurrentUserUnread($taskId){
		$q = "UPDATE ".$this->tablename." l JOIN ".$this->ecs->table('admin_taskes')." t ON t.task_id = l.task_id SET l.sender_read=1,l.receiver_read=1 WHERE l.task_id=".$taskId." AND (l.sender_read=0 AND t.sender_id=".$this->currentUserId." OR l.receiver_read=0 AND t.receiver_id=".$this->currentUserId.")";
		return $this->db->query($q);
	}

	public function getTheMostFrequentlyUsedUsers($limit){
		$start_time = local_mktime(0, 0, 0, local_date('m'), local_date('d'), local_date('Y'));
		$start_time = $start_time - (86400 * 30);
		$q = "SELECT sender_name, receiver_id, pic, count(*) as count, au.avatar FROM `ecs_admin_taskes` left join ecs_admin_user au on (receiver_id = au.user_id) where sender_id = '".$this->currentUserId."' and sent_time > '".$start_time."' and title not like '需訂貨 但未填寫備註%' and title not like '訂貨快到期但未處理訂單%' GROUP By receiver_id,sender_name, pic ORDER BY `count` DESC LIMIT ".$limit;
		//$q = "SELECT sender_name, receiver_id, pic, count(*) as count, au.avatar FROM `ecs_admin_taskes` left join ecs_admin_user au on (receiver_id = au.user_id) where sender_id = '".$this->currentUserId."' GROUP By receiver_id,sender_name, pic ORDER BY `count` DESC LIMIT ".$limit;
		return $this->db->getAll($q);
	}

	public function getTheMostFrequentlyUsedTemplatesByUser($user_id,$limit = 12){
		$q = "SELECT attu.template_id, att.title, attu.frequency FROM ". $this->ecs->table('admin_task_templates_used')." attu join ".$this->ecs->table('admin_task_templates')." att on (attu.template_id = att.template_id) where user_id = '".$user_id."' order by frequency desc, title LIMIT ".$limit;
		return $this->db->getAll($q);
	}

	public function updateTaskTemplateUsed($user_id,$template_id) {
		// check if the record existed, then insert else update 
		if ($user_id != '' && $template_id > 0){
			$q = "SELECT * FROM ". $this->ecs->table('admin_task_templates_used')." where user_id = '".$user_id."' and template_id = '".$template_id."'";
			$count = mysql_num_rows($this->db->query($q));
			if (!$count){
				$q = "INSERT INTO ".$this->ecs->table('admin_task_templates_used')." (user_id,template_id,frequency,last_modified_time) VALUE (".$user_id.",".$template_id.",1,".gmtime().")";
				$this->db->query($q);
			}else{
				$q = "UPDATE ".$this->ecs->table('admin_task_templates_used')." set frequency = frequency + 1, last_modified_time = ".gmtime()." where user_id= ".$user_id." and template_id = ".$template_id." ";
				$this->db->query($q);
			}
		}
	}

	public function deleteTaskTemplateUsed($template_id) {
		$sql = "DELETE FROM " . $this->ecs->table('admin_task_templates_used') .
	             " WHERE `template_id` = '" . $template_id . "' ";
		$this->db->query($sql);
	}

	public static function getPriorityMappings() {
			return self::$priorityMappings;
	}

	public static function getEstimatedTimeMappings() {
			return self::$estimatedTimeMappings;
	}

	public function ajaxQueryAction($list, $record_count)
	{
		parent::ajaxQueryAction($list, $record_count, false);
	}
}