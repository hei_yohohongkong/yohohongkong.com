<?php
 
namespace Yoho\cms\Controller;

if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}
include_once(ROOT_PATH . '/includes/cls_image.php');
require_once ROOT_PATH . 'includes/excel/PHPExcel.php';
require_once ROOT_PATH . 'includes/excel/PHPExcel/IOFactory.php';
require_once ROOT_PATH . 'includes/excel/PHPExcel/Shared/Date.php';

class GoodsShippingDimensionController extends YohoBaseController
{
    const GOODS_HEADING = ['Goods S/N', 'Length', 'Width', 'Height', 'Weight'];
    const CATEGORY_HEADING = ['Category ID', 'Length', 'Width', 'Height', 'Weight'];

    const GOODS_SN_LABEL = self::GOODS_HEADING[0];
    const CATEGORY_ID_LABEL = self::CATEGORY_HEADING[0];
    
    public function __construct()
    {
        parent::__construct();
        global $db, $_LANG;
    }

    private function getImportedDocumentHeaderDifferenceCount($header)
    {
        $category_count = count(array_diff(self::CATEGORY_HEADING, array_intersect($header, self::CATEGORY_HEADING)));
        $goods_count = count(array_diff(self::GOODS_HEADING, array_intersect($header, self::GOODS_HEADING)));
        return min($category_count, $goods_count);
    } 

    private function loadShippingDimension($header, $worksheet, $type)
    {
        $header_map = [];
        // Traverse all rows
        foreach ($worksheet->getRowIterator() AS  $row){
            $rowIndex = $row->getRowIndex();
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
            $cells = [];
            foreach ($cellIterator as $key => $cell) {
                // Create Column Mapping at first row
                if ($rowIndex == 1) { // Header row
                    $heading = $type == 'category' ? self::CATEGORY_HEADING : self::GOODS_HEADING ;
                    if (in_array($header[$key], $heading)) {
                        $header_map[$key] = $header[$key];
                    }
                } else { // Non-header row
                    // Fetch all cell value to associated array
                    if (array_key_exists($key, $header_map)) {
                        $cells[$header_map[$key]] = trim($this->getCellValue($cell));
                    }
                }
            }

            // Save parsed row
            if(!empty($cells)) {
                $rows[] = $cells;
            }
        }
        return $rows;
    }

    private function getCleanedUpRows($rows, $type)
    {
        global $ecs, $db, $_CFG;
        $errors = [];
        $update_data = [];

        $entry_id_label = '';
        $identifier_message_string = '';
        $affected_identifiey_list = array_column($rows, $entry_id_label);

        // Preparing existing identifier based on type
        if ($type == 'category') {
            $entry_id_label = self::CATEGORY_ID_LABEL;
            $affected_identifiey_list = array_column($rows, $entry_id_label);
            $existing_id_query_string = "SELECT cat_id FROM " . $ecs->table('category') . "WHERE cat_id in ('" . implode("','", $affected_identifiey_list) . "')";
            $identifier_message_string = '類別編號';
        } else if ($type == 'goods') {
            $entry_id_label = self::GOODS_SN_LABEL;
            $affected_identifiey_list = array_column($rows, $entry_id_label);
            $existing_id_query_string = "SELECT goods_sn FROM " . $ecs->table('goods') . "WHERE goods_sn in ('" . implode("','", $affected_identifiey_list) . "')";
            $identifier_message_string = '產品條碼';
        }        

        // Get existing category id
        $existing_identifier_id = $db->getCol($existing_id_query_string);
        
        foreach($rows as $index => $row) {
            // Get initial parameters
            $identifier_id = $row[$entry_id_label];
            $length = $row['Length'];
            $height = $row['Height'];
            $width = $row['Width'];
            $weight = $row['Weight'];
            $row_index = $index + 2;

            // Check non-existent identifiey
            if (empty($identifier_id)) {
                $errors[] = "Line ".$row_index." = "."[".$identifier_id."] : 不能空白";
            } else {
                if (!in_array($identifier_id, $existing_identifier_id)) {
                    $errors[] = "Line ".$row_index." = "."[".$entry_id_label."] : 找不到".$identifier_message_string."(".$identifier_id.")";
                }
            }

            // Check Length
            if (empty($length)) {
                $errors[] = "Line ".$row_index." = "."[Length] : 不能空白";
            } else {
                if (!is_numeric($length)) {
                    $errors[] = "Line ".$row_index." = "."[Length] : 須為數字";
                }
            }

            // Check Height
            if (empty($height)) {
                $errors[] = "Line ".$row_index." = "."[Height] : 不能空白";
            } else {
                if (!is_numeric($height)) {
                    $errors[] = "Line ".$row_index." = "."[Height] : 須為數字";
                }
            }

            // Check Width
            if (empty($width)) {
                $errors[] = "Line ".$row_index." = "."[Width] : 不能空白";
            } else {
                if (!is_numeric($width)) {
                    $errors[] = "Line ".$row_index." = "."[Width] : 須為數字";
                }
            }

            // Check Weight
            if (empty($weight)) {
                $errors[] = "Line ".$row_index." = "."[Weight] : 不能空白";
            } else {
                if (!is_numeric($weight)) {
                    $errors[] = "Line ".$row_index." = "."[Weight] : 須為數字";
                }
            }

            // Add to Update data as indication of finishing screening
            $update_data[] = $row;
        }

        return (object) [
            "error" => $errors,
            "update_data" => $update_data
        ];
    }

    private function update_goods_shipping_dimension($update_data, $type, $update_category_default = false)
    {
        global $ecs, $db;
        $affected_row_count = 0;

        foreach($update_data as $index => $data) {
            $update_sql_string = '';
            if ($type == 'category') {
                if ($update_category_default) {
                    $update_sql_string = "UPDATE ".$ecs->table('category')."SET `default_length` = '".$data['Length']."', `default_width` = '".$data['Width']."', `default_height` = '".$data['Height']."'  WHERE cat_id = ('" . intval($data['Category ID']) . "')";
                } else {
                    $category_list = array_column(cat_list(intval($data['Category ID']), 0, false, 0, false), 'cat_id');
                    $update_sql_string = "UPDATE ".$ecs->table('goods')."SET `goods_length` = '".$data['Length']."', `goods_width` = '".$data['Width']."', `goods_height` = '".$data['Height']."', `goods_weight` = '".$data['Weight']."'  WHERE cat_id IN ('". implode("','", $category_list) . "')";
                }
            } elseif ($type == 'goods') {
                $update_sql_string = "UPDATE ".$ecs->table('goods')."SET `goods_length` = '".$data['Length']."', `goods_width` = '".$data['Width']."', `goods_height` = '".$data['Height']."', `goods_weight` = '".$data['Weight']."'  WHERE goods_sn = '".$data['Goods S/N']."'";
            }

            $result = $db->query($update_sql_string);
                if ($result == true) {
                    $affected_row_count += $db->affected_rows();
                }
        }

        return $affected_row_count;
    }

    private function getCellValue($cell) 
    {
        if ($cell->getValue() instanceof \PHPExcel_RichText) {
            return $cell->getValue()->getPlainText();
        } else {
            return $cell->getValue();
        }
    }

    public function showGoodsShippingDimensionImportPage() 
    {
        global $ecs, $db, $_LANG, $smarty;

        $smarty->assign('ur_here', $_LANG['goods_shipping_dimension_import']);
        $smarty->display('goods_shipping_dimension_import.htm');
    }

    public function uploadGoodsShippingDimensionFile($updateCategoryDefault = false)
    {
        global $ecs, $db, $_CFG;
        $imported_table_type = ''; // Storing imported list type either goods or category

        if (empty($_FILES)) {
            return	['error' => '請選擇檔案!'];	
        }

        // init data
        ini_set('memory_limit', '256M');
        define( 'PCLZIP_TEMPORARY_DIR', ROOT_PATH . 'temp/');

        \PHPExcel_Settings::setZipClass(\PHPExcel_Settings::PCLZIP);
        $fileName     = $_FILES["import_excel"]["tmp_name"];

        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $file_format = finfo_file($finfo, $fileName);
    
        $valided_type = ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                        'application/vnd.ms-excel',
                        'application/vnd.ms-office'
                        ];

        if (!in_array($file_format,$valided_type)) {
            make_json_error('檔案格式錯誤!請使用.xlsx .xls');
        }

        $fileType     = \PHPExcel_IOFactory::identify($fileName);
        $objReader    = \PHPExcel_IOFactory::createReader($fileType);
        $objPHPExcel  = $objReader->load($fileName);
        $objWorksheet = $objPHPExcel->getSheet(0);

        // Try to read first row to determine which update query should be executed
        $row = $objPHPExcel->getActiveSheet()->getRowIterator(1)->current();
        $cellIterator = $row->getCellIterator();
        $cellIterator->setIterateOnlyExistingCells(false);

        $header = [];
        foreach ($cellIterator as $cell) {
            $header[] = $this->getCellValue($cell);            
        }

        if($this->getImportedDocumentHeaderDifferenceCount($header) > 0 ) {
            make_json_error('格式錯誤, 請使用指定模板');
        }

        if (in_array(self::CATEGORY_ID_LABEL, $header)) {
            $imported_table_type = 'category';
        } else if (in_array(self::GOODS_SN_LABEL, $header)) {
            $imported_table_type = 'goods';
        }

        if (empty($imported_table_type)) {
            make_json_error('unknown import data type');
        }

        // Load file content
        $rows = $this->loadShippingDimension($header, $objWorksheet, $imported_table_type);

        $cleaned_up_rows = $this->getCleanedUpRows($rows, $imported_table_type);

        $update_result = false;
        if (!empty($cleaned_up_rows->error)) {
            make_json_error(implode("<br>", $cleaned_up_rows->error));
        } else {
            $update_result = $this->update_goods_shipping_dimension($cleaned_up_rows->update_data, $imported_table_type, $updateCategoryDefault);
        }

        if (!update_result) {
            make_json_error('匯入失敗: 沒有進行資料庫更新');
        } else {
            make_json_response('', '0', '匯入成功,共更新'.$update_result.'個記錄');
        }
    }

    public function downloadCategoryShippingDimensionTemplate()
    {
        // init data
        $today_date = local_date('Ymd');
        $file_name = 'goods_category_shipping_dimension_template_'.$today_date;
        header("Content-type: application/vnd.ms-excel; charset=utf-8");
        header("Content-Disposition: attachment; filename=$file_name.xls");
        echo $file = file_get_contents('../data/goods_category_shipping_dimension_template.xls', FILE_USE_INCLUDE_PATH);
        exit;
    }

    public function downloadGoodsShippingDimensionTemplate()
    {
        // init data
        $today_date = local_date('Ymd');
        $file_name = 'goods_shipping_dimension_template_'.$today_date;
        header("Content-type: application/vnd.ms-excel; charset=utf-8");
        header("Content-Disposition: attachment; filename=$file_name.xls");
        echo $file = file_get_contents('../data/goods_shipping_dimension_template.xls', FILE_USE_INCLUDE_PATH);
        exit;
    }

    public function getAllGoodsWithIncompleteShippingDimension()
    {
        global $ecs, $db;
        $filename = local_date("Ymd").'-goods_with_incomplete_shipping_dimension.xlsx';

        $erpController = new ErpController();
        $sql = "SELECT goods_sn, goods_name, goods_length, goods_width, goods_height, goods_weight FROM " . $ecs->table('goods') . " as g WHERE  (SELECT sum(ega.goods_qty) FROM " . $GLOBALS['ecs']->table('erp_goods_attr_stock') . " as ega WHERE ega.goods_id = g.goods_id AND ega.warehouse_id IN (".$erpController->getSellableWarehouseIds(true).")) > 0 and (goods_height <= 0 or goods_width <= 0 or goods_length <= 0) and g.is_on_sale = 1 and g.is_delete = 0 ";

        $result = $db->getAll($sql);

        if (count($result) == 0) {
            return;
        }

        // Empty those zero-valued fields to empty
        foreach($result as $row_index => $row_value) {
            foreach ($row_value as $column_index => $column_value) {
                if (is_numeric($column_value) && floatval($column_value) == 0 ) {
                    $result[$row_index][$column_index] = null;
                }
            }
        }

        // Get Stock info of the products
        $product_sn_list = (array_column($result, 'goods_sn'));
        $goods_stock_info_list = $erpController->getSellableProductsQtyByWarehouses($product_sn_list, null, false);
        $warehouse_info = $erpController->getWarehouseList(0, -1, -1, 1);
        $warehouse_names = array_column($warehouse_info, 'name');

        // Parse info key into warehouse name
        foreach($goods_stock_info_list as $goods_id => $goods_stock_info) {
            foreach($goods_stock_info as $warehouse_id => $stock_count) {
                $warehouse_index = array_search($warehouse_id, array_column($warehouse_info, 'warehouse_id'));
                $warehouse_name = $warehouse_info[$warehouse_index]['name'];
                $goods_stock_info_list[$goods_id][$warehouse_name] = $stock_count;
                unset($goods_stock_info_list[$goods_id][$warehouse_id]);
            }
        }

        // Merge the arrays
        foreach($result as $row_index => $row_item) {
            $stock_info = $goods_stock_info_list[$row_item['goods_sn']];
            foreach($stock_info as $stock_info_item_key => $stock_info_item_value) {
                $result[$row_index][$stock_info_item_key] =  $stock_info_item_value;
            }
        }

        // Create new PHPExcel object
        define('PCLZIP_TEMPORARY_DIR', ROOT_PATH . 'temp/');
        \PHPExcel_Settings::setZipClass(\PHPExcel_Settings::PCLZIP);
        $objPHPExcel = new \PHPExcel();

        // Create Col title
        $title_list = ['Goods S/N', 'Goods Name', 'Length', 'Width', 'Height', 'Weight'];
        $title_list = array_merge($title_list, $warehouse_names);
        $target_column = chr(ord('A')+sizeof($title_list)-1);
        foreach (range('A', $target_column) as $char_key => $column){
            $objPHPExcel->getActiveSheet()->SetCellValue($column.'1', $title_list[$char_key]);
         }

        // Fill worksheet from values in array
        $objPHPExcel->getActiveSheet()->fromArray($result, null, 'A2', true);

        // Set AutoSize for product S/N and dimension fields
        $lastrow = $objPHPExcel->getActiveSheet()->getHighestRow();
        for ($i=0;$i<sizeof($title_list);$i++) {
            $objPHPExcel->getActiveSheet()->getColumnDimension(chr(ord('A') + $i))->setAutoSize(true);
        }

        foreach($result as $index => $goods) {
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.($index+2), (string)$goods['goods_sn'],\PHPExcel_Cell_DataType::TYPE_STRING);
        }

        // Save Excel 2007 file
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.‌​sheet; charset=utf-8');
        header('Content-Disposition: attachment; filename=' . $filename);
        header('Cache-Control: max-age=0');
		$objWriter->save('php://output');
    }

}

?>
