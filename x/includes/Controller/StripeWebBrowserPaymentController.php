<?php
namespace Yoho\cms\Controller;

if (!defined('IN_ECS')) {
    die('Hacking attempt');
}

require_once(ROOT_PATH . '/includes/stripe/init.php');

class StripeWebBrowserPaymentController extends YohoBaseController
{
    public function getPaymentButton($order, $payment, $is_sandbox = false, $pay_code = '')
    {
        require_once ROOT_PATH . ADMIN_PATH . '/includes/lib_accounting.php';

        global $actgHooks, $_LANG;

        $image_path = "/yohohk/img/payment-icon/$pay_code.svg";
        
        $publishable_key = (empty($payment['stripe_publishable_key']) ? 'pk_test_E0Stl002F54obSM0TUWwAgix' : $payment['stripe_publishable_key']);
        $secret_key      = (empty($payment['stripe_secret_key']) ? 'sk_test_fB3Ene1AWkvnEc0IdfHLTgEH' : $payment['stripe_secret_key']);

		$stripe = array(
				'secret_key'      => $secret_key,
				'publishable_key' => $publishable_key
		);

        \Stripe\Stripe::setApiKey($stripe['secret_key']);

        $supportedPlatformList = "<ul style='width:90%;margin:auto;'>";

        foreach($_LANG[$pay_code.'_supported_platform'] as $platform)
        {
            $supportedPlatformList .= "<li style='list-style-type:circle;'>$platform</li>";
        }

        $supportedPlatformList .="</ul>";
        
        $paymentButton = (!defined("YOHO_MOBILE") ? "<div><div style='margin:auto;text-align:center;'><img style='width:100px;' src='$image_path' title='test'/></div></div>" : "") . 
        "<script src='https://js.stripe.com/v3/'></script>
        <style>
            .bg-danger {
                background-color: #f2dede;
            }
            .text-danger {
                color: #a94442;
            }
            .stripe-payment-btn {
                width: ".(!defined("YOHO_MOBILE") ? "300px" : "100%").";
                background: 0 0 #ffffff;
                border: 1px solid #00acee;
                border-radius: 0;
                box-shadow: unset;
                padding: 10px !important;
                transition: 0.5s;
            }
            .stripe-payment-btn:hover {
                background-color: #f8f8f8;
                border-bottom-color: #00acee;
                box-shadow: unset;
            }
            .stripe-payment-btn > .stripe-payment-btn-img{
                height: 1.5rem;
                margin: auto;
                display: block;
            }
            ul > li {
                list-style-type: disc;
                list-style-position: outside;
                text-align: justify;
            }
            ul > li.stripe-payment-error-message-header {
                list-style-type: none;
                text-align: center;
            }
        </style>
        <div style='padding:1rem;text-align:center;display:none;' class='payme_mobile_payment_button_container' id='payme_mobile_payment_button_container'>
            <p>" . $_LANG['stripe_pay_now_message'] . "</p>
            <br/>
            <button class='btn btn-default stripe-payment-btn' id='stripe-payment-btn'><img class='stripe-payment-btn-img' src='/yohohk/img/payment-icon/".$pay_code."_inverted.svg'></img></button>
        </div>
        <script defer='defer'>
            // Curtesy of https://juejin.im/post/5b972dcf5188255c781ca01b
            var browser = {
                versions: function () {
                var u = navigator.userAgent;
                return {
                    trident: u.indexOf('Trident') > -1, //IE内核
                    presto: u.indexOf('Presto') > -1, //opera内核
                    webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
                    gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1,//火狐内核
                    mobile: !!u.match(/AppleWebKit.*Mobile.*/), //是否为移动终端
                    ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
                    android: u.indexOf('Android') > -1 || u.indexOf('Adr') > -1, //android终端
                    iPhone: u.indexOf('iPhone') > -1, //是否为iPhone或者QQHD浏览器
                    iPad: u.indexOf('iPad') > -1, //是否iPad
                    webApp: u.indexOf('Safari') == -1, //是否web应该程序，没有头部与底部
                    weixin: u.indexOf('MicroMessenger') > -1, //是否微信
                    qq: u.match(/\sQQ/i) == ' qq', //是否QQ
                    chrome: u.indexOf('Chrome/') > -1,
                    chromeMobile: u.indexOf('Chrome/') > -1 && u.indexOf('Mobile') > -1,
                    edge: u.indexOf('Edge/') > -1,
                };
                }(),
                language: (navigator.browserLanguage || navigator.language).toLowerCase()
            };

            function initStripeBrowserPayment(){
                var paymentMethod = '" . $pay_code . "';
                var stripe = Stripe('$publishable_key');
                
                var paymentRequest = stripe.paymentRequest({
                    country: 'HK',
                    currency: 'hkd',
                    total: {
                        label: '" . sprintf("%s %s", $_LANG['stripe_order_header'], $order['order_sn']) . "',
                        amount: " . $order['order_amount'] * 100 . ",
                    },
                    requestPayerName: false,
                    requestPayerEmail: false,
                });

                paymentRequest.canMakePayment().then(function(result){
                    var container = $('.payme_mobile_payment_button_container');
                    if (result && checkIfUserAgentMatchPaymentMethod(paymentMethod, result)) {
                    } else {
                        container.html(\"<br/><div class='bg-danger text-danger' style='width:" . (defined('YOHO_MOBILE') ? '100%' : '50%') . ";margin:auto;padding:0.5rem;'>\
                            <ul style='width:60%;margin:auto;'>\
                                <li class='stripe-payment-error-message-header'>" . $_LANG[$pay_code.'_unable_to_make_payment'] . "</li>\
                                <li>" . $_LANG[$pay_code.'_supported_platform_heading'] . "\
                                    $supportedPlatformList \
                                </li>\
                                <li>" . $_LANG[$pay_code.'_configure_instruction'] . "</li>\
                            </ul></div>\");
                    }
                    container.show();
                });
                document.getElementById('stripe-payment-btn').onclick=function(){
                    paymentRequest.show();
                }
                paymentRequest.on('token', function(ev) {
                    $.ajax({
                        url: '/ajax/initiateStripeBrowserPayment.php',
                        type: 'post',
                        dataType: 'json',
                        data: {
                            'order_sn': '" . $order['order_sn'] . "',
                            'order_id': '" . $order['order_id'] . "',
                            'payment_id': '" . $order['log_id'] . "',
                            'pay_code': '" . ( !empty($order['origin_pay_code']) ? $order['origin_pay_code'] : $pay_code). "',
                            'token': ev.token.id,
                        },
                        success: function(data) {
                            if (!data || data.status == 0) {
                                ev.complete('fail');
                                return;
                            }
                            ev.complete('success');
                        },
                        error: function(xhr, status, errorThrown) {
                            ev.complete('fail');
                        }
                    });
                });
            }

            function checkIfUserAgentMatchPaymentMethod(paymentMethod, result)
            {
                switch (paymentMethod) {
                    case 'stripegooglepay':
                        return browser.versions.chromeMobile && !(result.applePay);
                    case 'stripeapplepay':
                        return result.applePay;
                    default:
                        return false;
                }
            }
            document.addEventListener('DOMContentLoaded', function() {
                initStripeBrowserPayment();
                " . ((defined("YOHO_MOBILE") ? "checkPayStatus();" : "")) . "
            });
        </script>
        ";

        return $paymentButton;
    }

    public function stripeBrowserPaymentAction($order_sn, $payment_id, $token, $pay_code)
    {
        $fraudController = new FraudController();

        // Get Order Info
        $oSql = "SELECT o.order_sn, o.order_id, o.consignee, o.email, o.tel, o.mobile, o.pay_id, o.address, o.order_status, o.pay_status, o.shipping_status, r.region_name, u.reg_time, u.last_login, u.last_ip, u.user_name, u.new_user_name, u.rank_points, u.pay_points, u.user_id ".
        " FROM " . $GLOBALS['ecs']->table('order_info') ." as o ".
        " LEFT JOIN " . $GLOBALS['ecs']->table('region') ." as r ON r.region_id = o.country ".
        " LEFT JOIN " . $GLOBALS['ecs']->table('users') ." as u ON o.user_id = u.user_id ".
        " WHERE order_sn = '" . $order_sn . "'";

        $orderInfo = $GLOBALS['db']->getRow($oSql);
        $logId = $payment_id;
        $payment = get_payment($pay_code);
        $sql = "SELECT * FROM " . $GLOBALS['ecs']->table('pay_log') . " WHERE log_id = '" . $logId . "'";
        $fraud = FRAUD_CLEAR;
        $payLog = $GLOBALS['db']->getRow($sql);
        
        if (empty($orderInfo) || empty($payLog)) {
            echo json_encode([
                'status' => 0,
                'pay_status' => "ORDER_PAYMENT_NOT_FOUND"
            ]);
            exit;
        }

        $orderInfo['reg_time_formatted'] = local_date($GLOBALS['_CFG']['date_format'], $orderInfo['reg_time']);
		$orderInfo['last_login_formatted'] = local_date($GLOBALS['_CFG']['date_format'], $orderInfo['last_login']);
		if($orderInfo['rank_points'] > 1000 ){
			$orderInfo['rank_points_op'] = true;
		} else {
			$orderInfo['rank_points_op'] = false;
        }
        
        $publishable_key = (empty($payment['stripe_publishable_key']) ? 'pk_test_E0Stl002F54obSM0TUWwAgix' : $payment['stripe_publishable_key']);
        $secret_key      = (empty($payment['stripe_secret_key']) ? 'sk_test_fB3Ene1AWkvnEc0IdfHLTgEH' : $payment['stripe_secret_key']);

		$stripe = array(
				'secret_key'      => $secret_key,
				'publishable_key' => $publishable_key
        );
        
        \Stripe\Stripe::setApiKey($stripe['secret_key']);
        
        // If order is cancelled / refunded
        if (in_array($orderInfo['order_status'], [OS_CANCELED, OS_INVALID, OS_WAITING_REFUND])) {
            order_action($orderInfo['order_sn'], $orderInfo['order_status'], $orderInfo['shipping_status'], $orderInfo['pay_status'], 'Stripe Error: 此訂單已取消, 無法付款.');
            $_SESSION['webhooks_error'] = 1;
            echo json_encode([
                'status' => 0,
                'pay_status' => "ORDER_CANCELLED"
            ]);
            exit;
        }

        // If order is payed, Don't pay again
		if($orderInfo['pay_status'] == PS_PAYED)
		{
			/* 记录订单操作记录 */
			order_action($orderInfo['order_sn'], $orderInfo['order_status'], $orderInfo['shipping_status'], $orderInfo['pay_status'], 'Stripe Error: 此訂單已付款成功, 無需再付款.');
			echo json_encode([
                'status' => 0,
                'pay_status' => "PAID_ALREADY"
            ]);
            exit;
        }
        
        try {

            $currency = !empty($payLog['currency_code']) ? $payLog['currency_code'] : $payment['stripe_currency'];
            $charge = \Stripe\Charge::create(array(
                'source'   => $token,
                'amount'   => $payLog['order_amount'] * 100,
                'metadata' => array(
                    'order_id'  	 => $orderInfo['order_sn'],
                    'consignee' 	 => $orderInfo['consignee'],
                    'email'     	 => $orderInfo['email'],
                    'tel'       	 => $orderInfo['tel'],
                    'mobile'    	 => $orderInfo['mobile'],
                    'log_id'    	 => $logId,
                    'address'   	 => $orderInfo['address'],
                    'register_time'  => $orderInfo['reg_time_formatted'],
                    'last_login'     => $orderInfo['last_login_formatted'],
                    'country'        => $orderInfo['region_name'],
                    'ip'			 => $orderInfo['last_ip'],
                    'register_tel'   => $orderInfo['user_name'],
                    'rank_points'    => $orderInfo['rank_points'],
                    'rank_points_op' => $orderInfo['rank_points_op']

                ),
                'currency' => $currency
            ));

        } catch (\Exception $e) {
            $body = $e->getJsonBody();
            $error = $e->getMessage();

            if ($body['error']['decline_code'] == "fraudulent") {
                $fraud = FRAUD_CONFIRM;
            }
            if (in_array($body['error']['decline_code'], ["incorrect_cvc", "invalid_cvc", "incorrect_pin", "invalid_pin"])) {
                $fraud = FRAUD_MEDIUM;
            }
            if (in_array($body['error']['decline_code'], ["do_not_honor", "lost_card", "pickup_card", "pin_try_exceeded", "restricted_card", "stolen_card"])) {
                $fraud = FRAUD_HIGH;
            }
            payment_risk_level_log($body['error']['charge'], $orderInfo['order_id'], $orderInfo['pay_id'], $fraud);

            /* 记录订单操作记录 */
            order_action($orderInfo['order_sn'], $orderInfo['order_status'], $orderInfo['shipping_status'], $orderInfo['pay_status'], '付款失敗(Stripe):'.$error.$token, $GLOBALS['_LANG']['buyer']);
            echo json_encode([
                'status' => 0,
                'pay_status' => "PAYMENT_FAILED",
                // 'detail' => json_encode($body)
            ]);
            exit;
        }
        if (!isset($error) || empty($error)) {

            $txn_id = $charge->id;
            $action_note = $pay_code. ':' . $txn_id . '（' . $payLog['currency_code'] . $payLog['order_amount'] . '）';

            if ($charge->outcome->reason == "incorrect_cvc") {
                $fraud = FRAUD_HIGH;
            } elseif ($charge->outcome->risk_level == "elevated") {
                $fraud = FRAUD_MEDIUM;
            }
            payment_risk_level_log($charge->id, $orderInfo['order_id'], $logId, $fraud, 1);

            /* 改变订单状态 */
            order_paid($logId, PS_PAYED, $action_note);
            
            echo json_encode([
                'status' => 1,
                'pay_status' => "PAYMENT_COMPLETE",
                // 'data' => json_encode($charge)
            ]);
            exit;

        } else {
            echo json_encode([
                'status' => 0,
                'pay_status' => "PAYMENT_FAILED",
                // 'detail' => 'unknown error'
            ]);
            exit;
        }
    }
}
?>