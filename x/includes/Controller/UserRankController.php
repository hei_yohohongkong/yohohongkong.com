<?php

namespace Yoho\cms\Controller;

use Yoho\cms\Model;

if (!defined('IN_ECS')) {
	die('Hacking attempt');
}

class UserRankController extends YohoBaseController
{

	public $tableName = 'user_rank_program';
	const ORGANIZATION_TYPE_COMPANY = 1;
	const ORGANIZATION_TYPE_SCHOOL = 2;
	const ORGANIZATION_TYPE_OTHER = 3;
	const ORGANIZATION_TYPE_STRING_ARRAY = [self::ORGANIZATION_TYPE_COMPANY => '公司', self::ORGANIZATION_TYPE_SCHOOL => '院校', self::ORGANIZATION_TYPE_OTHER => '其他'];

	/** CMS add/edit action
	 **/
	public function editAction()
	{
		global $ecs, $db, $_LANG;
		$insert = false;
		if (!empty($_REQUEST['act'] == "add")) {
			$insert = true;
		}
		$program_id = empty($_REQUEST['id']) ? "" : $_REQUEST['id'];
		if (empty($program_id) && !$insert) {
			sys_msg("請選擇欲處理的計劃");
		}
		$localizable_fields = localizable_fields_for_table($this->tableName);
		$assign['localizable_fields'] = $localizable_fields;
		if (!$insert) {
			$sql = "SELECT * FROM " . $ecs->table("user_rank_program") .
				" WHERE program_id = $program_id";
			$record = $db->getRow($sql);
			$assign["record"] = $record;
			$organization_ids = [];
			if (!empty($record['organization_ids'])) $organization_ids = unserialize($record['organization_ids']);
			$selected_organization_list = $db->getAll("SELECT organization_id, organization_type FROM " . $ecs->table("program_organization") . " WHERE organization_id" . db_create_in($organization_ids));
			$assign['selected_organization_list'] = $selected_organization_list;
			$localized_versions = get_localized_versions($this->tableName, $program_id, $localizable_fields);
			$assign['localized_versions'] = $localized_versions;
		}

		$valid_ranks = $db->getAll(
			"SELECT rank_id, rank_name FROM " . $ecs->table("user_rank") .
			" WHERE special_rank = 1"
		);

		$pricing_profile = $db->getAll("select * from ".$ecs->table("user_rank_program_pricing_profile")." where status = 1 ");

		$organizations_list = $db->getAll(
			"SELECT organization_id, organization_name, organization_type FROM " . $ecs->table("program_organization") .
			" WHERE enabled = 1"
		);

		$organizations = [];
		foreach ($organizations_list as $key => $value) {
			$organizations[$value['organization_type']][] = $value;
		}
		$assign["pricing_profile"] = $pricing_profile;
		$assign["organizations"] = $organizations;
		$assign["valid_ranks"] = $valid_ranks;
		$assign['add'] = $insert;
		$assign['tab'] = $_REQUEST['tab'];
		
		$assign['ur_here'] = $_LANG['add_rank_program'];
		$assign["organization_type"] = self::ORGANIZATION_TYPE_STRING_ARRAY;
		$assign['action_link'] = array('text' => "返回計劃列表", 'href' => 'user_rank_program.php?act=list');
		$template = "user_rank_program_info.htm";

		
		$this->generateActionTemplate($assign, $template);
	}

	/** Get user joining program
	 * @return Array|Boolean  rank_id list
	 **/
	public function getUserRankProgram($user_id, $info = false)
	{
		global $ecs, $db, $_CFG;
		if ($user_id == 0) {
			return false;
		}

		// Get not time to check expiry time.
		$now = local_date('Y-m-d H:i:s');
		if ($info == false) {
			$sql = "SELECT urp.rank_id FROM " . $ecs->table('user_rank_program_application') . " as urpa " .
				"LEFT JOIN " . $ecs->table('user_rank_program') . " as urp ON urp.program_id = urpa.program_id " .
				"WHERE urpa.user_id = $user_id AND urpa.verified = 1 AND urpa.stage IN(".RANK_APPLICATION_PREPARE.",".RANK_APPLICATION_APPROVED.") AND urpa.expiry > '" . $now . "' ";

			$rank_list = $db->getCol($sql);

			return $rank_list;
		} else {
			$sql = "SELECT urp.rank_id, IFNULL(urpl.program_name, urp.program_name) as program_name, IFNULL(urpl.description, urp.description) as description, urp.program_id, urp.theme_color FROM " . $ecs->table('user_rank_program_application') . " as urpa " .
				"LEFT JOIN " . $ecs->table('user_rank_program') . " as urp ON urp.program_id = urpa.program_id " .
				"LEFT JOIN " . $ecs->table('user_rank_program_lang') . " as urpl ON urpl.program_id = urp.program_id AND urpl.lang = '" . $_CFG['lang'] . "' " .
				"WHERE urpa.user_id = $user_id AND urpa.verified = 1 AND urpa.stage IN(".RANK_APPLICATION_PREPARE.",".RANK_APPLICATION_APPROVED.") AND urpa.expiry > '" . $now . "' ";
			$rank_list = $db->getAll($sql);
			return $rank_list;
		}

	}

	/** Get goods special price.
	 * @param Number goods_id
	 * @return Array|Boolean  price list with rank program info
	 **/
	public function getGoodsMemberPrices($goods_id)
	{
		global $ecs, $db;

		$now = local_date('Y-m-d H:i:s');
		$sql = "SELECT ur.rank_id, ur.rank_name, urp.program_name, urp.status, urp.description, IFNULL(mp.price_id, 0) as price_id, IFNULL(mp.user_price, 0) as user_price, mp.max_amount FROM " . $ecs->table('user_rank') . " as ur " .
			"LEFT JOIN " . $ecs->table('user_rank_program') . " as urp ON urp.rank_id = ur.rank_id " .
			"LEFT JOIN " . $ecs->table('member_price') . " as mp ON mp.user_rank = ur.rank_id AND mp.goods_id = " . $goods_id . " " .
			"WHERE ur.rank_id > 1 "; //1 is default normal member
		$price_list = $db->getAll($sql);

		return $price_list;

	}

	/** Get User can use min price program.
	 * @param Number goods_id
	 * @param Number user_id
	 * @return array|Boolean  Program info
	 **/
	public function getUserMinRank($goods_ids, $user_id)
	{
		global $ecs, $db, $_CFG;
		$now = local_date('Y-m-d H:i:s');
		if (defined('IS_POS') || empty($user_id) || empty($goods_ids)) { // IN POS, We not user this function
			return [];
		}
		if ($_SESSION['user_id'] == $user_id && !empty($_SESSION['user_rank_program'])) {
			$rank_id_list = $this->getUserRankProgram($user_id);
		} else {
			$rank_id_list = $_SESSION['user_rank_program'];
		}

		$sql = "SELECT mp.goods_id, ur.rank_id, ur.rank_name, IFNULL(urpl.program_name, urp.program_name) as program_name, mp.user_price as user_price, mp.max_amount, urp.theme_color FROM " . $ecs->table('user_rank') . " as ur " .
			"LEFT JOIN " . $ecs->table('user_rank_program') . " as urp ON urp.rank_id = ur.rank_id " .
			"LEFT JOIN " . $ecs->table('user_rank_program_application') . " as urpa ON urp.program_id = urpa.program_id " .
			"LEFT JOIN " . $ecs->table('user_rank_program_lang') . " as urpl ON urpl.program_id = urp.program_id AND urpl.lang = '" . $_CFG['lang'] . "' " .
			"INNER JOIN " . $ecs->table('member_price') . " as mp ON mp.user_rank = ur.rank_id AND mp.goods_id " . db_create_in($goods_ids) .
			"LEFT JOIN " . $ecs->table('user_rank_program_pricing_profile') . " AS urppp ON urp.`pricing_profile_id` = urppp.`pricing_profile_id` INNER JOIN " . $ecs->table('user_rank_program_pricing_profile_items') . " AS urpppi " .
			"ON urpppi.`pricing_profile_id` = urppp.`pricing_profile_id` AND urpppi.goods_id = mp.goods_id " .
			"WHERE ur.rank_id " . db_create_in($rank_id_list) . " AND urpa.user_id = $user_id AND urpa.verified = 1 AND urpa.expiry > '" . $now . "' ";
		$result = $db->getAll($sql);
		$res = [];
        $price_list = [];
		$can_buy = 0;
		foreach ($result as $row) {
			if ($row['user_price'] == null || $row['goods_id'] == null) {
				continue;
			}
            $price_list[$row['goods_id']][$row['rank_id']] = $row;
		}
		foreach($price_list as $goods_id => $rank_list) {
			$firstKey = key($rank_list);
			$res[$goods_id] = $rank_list[$firstKey];
			$min_price = $res[$goods_id]["user_price"];
            foreach($rank_list as $rank_id => $value) {
                if ($value['user_price'] < $min_price) {
                    if ($value['max_amount'] > 0) {
                        $sql = "SELECT SUM(og.goods_number) as total FROM " . $ecs->table('order_info') . " as oi " .
                            " LEFT JOIN " . $ecs->table('order_goods') . " as og ON oi.order_id = og.order_id " .
                            " WHERE 1 AND oi.order_status NOT IN(" . OS_CANCELED . ", " . OS_INVALID . ", " . OS_RETURNED . ") AND og.goods_id = $goods_id AND oi.user_id = $user_id AND og.user_rank = $rank_id";
                        $total = $db->getOne($sql);
                        if ($total >= $value['max_amount']) {
                            continue;
                        } else {
                            $value['can_buy'] = $value['max_amount'] - $total;
                        }
                    }
                    $min_price = $value['user_price'];
					$res[$goods_id] = $value;
				}
			}
		}
		return $res;
	}

	public function getActiveProgram($user_id = 0)
	{
		global $ecs, $db, $_CFG;
		$now = local_date('Y-m-d H:i:s');
		$sql = "SELECT urp.program_id, urp.program_code, IFNULL(urpl.program_name, urp.program_name) as program_name, IFNULL(urpl.description, urp.description) as description " .
			" FROM " . $ecs->table("user_rank_program") . " as urp " .
			(($user_id > 0) ? "LEFT JOIN " . $ecs->table('user_rank_program_application') . " as urpa ON urp.program_id = urpa.program_id " : "") .
			"LEFT JOIN " . $ecs->table('user_rank_program_lang') . " as urpl ON urpl.program_id = urp.program_id AND urpl.lang = '" . $_CFG['lang'] . "' " .
			(($user_id > 0) ? "WHERE (urp.status = 1 && urp.show_on_menu = 1) OR (urpa.user_id = $user_id AND urpa.verified = 1 AND urpa.stage IN(".RANK_APPLICATION_PREPARE.",".RANK_APPLICATION_APPROVED.") AND urpa.expiry > '" . $now . "') " : " WHERE urp.status = 1 && urp.show_on_menu = 1 ");
		$sql .= " GROUP BY urp.program_id";
		return $programs = $db->getAll($sql);
	}

	function getGoodsList($rank_id, $cat_id, $brand_id, $min, $max, $ext, $size, $page, $sort, $order, $is_promote = 0, $display = 'grid', $tags_goods = array())
	{
		global $ecs, $db;

		/* Get SQL format */
		$ext = " AND g.goods_id IN (SELECT goods_id FROM " . $this->ecs->table('member_price') . " WHERE user_rank = '" . $rank_id . "' ) ";;
		$children = get_children($cat_id);
		$categories = ($cat_id > 0) ? ' AND ' . get_children($cat_id) : '';
		$brand = $brand_id ? " AND g.brand_id = '$brand_id'" : '';
		$min_price = $min != 0 ? " AND IFNULL(mp.user_price, g.shop_price ) >= '$min'" : '';
		$max_price = $max != 0 || $min < 0 ? " AND IFNULL(mp.user_price, g.shop_price) <= '$max'" : '';
		$where = " g.is_delete = 0 AND g.is_hidden = 0 AND g.is_on_sale = 1 AND g.is_alone_sale = 1 $ext ";
		$categoryController = new CategoryController();

		/* Get user_program goods category */
		$cat_menu = $categoryController->getLeftCatMenu($cat_id, "programs", $where, 0);

		/* Get user_program price range */
		hw_assign_price_range("g.is_delete = 0 AND g.is_hidden = 0 AND g.is_on_sale = 1 AND g.is_alone_sale = 1 $ext " .
			"AND (( 1 " . $categories . $brand . " )) ", $rank_id);

		/* Get user_program goods brands */
		// 1. Get brand
		$fsql = "SELECT b.brand_logo, b.brand_id, IFNULL(bl.brand_name, b.brand_name) as brand_name, count(b.brand_id) as num, bperma " .
			"FROM " . $ecs->table('goods') . " AS g " .
			"LEFT JOIN " . $ecs->table('brand') . " AS b ON g.brand_id = b.brand_id " .
			"LEFT JOIN " . $ecs->table('brand_lang') . "as bl ON bl.brand_id = b.brand_id AND bl.lang = '" . $_CFG['lang'] . "' " .
            "LEFT JOIN (SELECT id, table_name, lang, perma_link as bperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") bpl ON bpl.id = b.brand_id  AND bpl.table_name = 'brand' AND bpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
			"WHERE $where " . //判断总数  $where
			"AND (( 1 " . $categories . " ) ) " .
			"AND b.is_show = 1 " .
			"GROUP BY b.brand_id ORDER BY b.sort_order ASC, b.brand_id ASC";
		$brands = $db->getAll($fsql);

		// 2. Create brands filter data
		foreach ($brands as $key => $val) {
			$brands[$key]['brand_id'] = $val['brand_id'];
			$brands[$key]['brand_name'] = $val['brand_name'];
			$brands[$key]['num'] = $val['num'];
			$brands[$key]['brand_logo'] = empty($val['brand_logo']) ? null : '/' . DATA_DIR . '/brandlogo/' . $val['brand_logo'];
			$brands[$key]['url'] = build_uri('programs', array('cid' => $cat_id, 'bid' => $val['brand_id'], 'bperma' => $val['bperma']));

			if ($brand_id == $val['brand_id']) {
				$brands[$key]['selected'] = 1;
				$brands[$key]['url'] = build_uri('programs', array('cid' => $cat_id, 'bid' => 0));
			} else {
				$brands[$key]['selected'] = 0;
			}
		}

		/* Get user_program goods count */
		$sql_where = "g.is_delete = 0 AND g.is_hidden = 0 AND g.is_on_sale = 1 AND g.is_alone_sale = 1 $ext " .
			"AND (( 1 " . $categories . $brand . $min_price . $max_price . " )) ";
		$sql = "SELECT COUNT(*) FROM " . $ecs->table('goods') . " AS g " .
			" LEFT JOIN  " . $ecs->table('member_price') . " AS mp ON g.goods_id = mp.goods_id AND mp.user_rank = $rank_id " .
			"WHERE $sql_where ";

		$count = $db->getOne($sql);

		$max_page = ($count > 0) ? ceil($count / $size) : 1;
		if ($page > $max_page) {
			$page = $max_page;
		}

		/* Get user_program goods */
		$sql = hw_goods_list_sql($sql_where, $sort, $order, $size, ($page - 1) * $size);
		$res = $db->getAll($sql);
		$goodslist = hw_process_goods_rows($res);

		return array(
			'goodslist' => $goodslist,
			'count' => $count,
			'left_cat_menu' => $cat_menu,
			'left_cat_menu_count' => count($fenlei),
			'brands' => $brands,
			'brand_id' => $brand_id
		);
	}

	public function sumbitUserRegister($data)
	{
		global $ecs, $db, $smarty;
		$user_id = $_SESSION['user_id'];
		if(empty($user_id)) {
		 	return $response = [
		 	  	"error" => 1,
		 	 	"msg" => "user_rank_user_not_selected"
		 	];
		}

		$program_id = $data['pid'];
		$proof = $data['secret'];
		$verify_email = $data['verify_email'];
		$foreign_id = FALSE;
		if (isset($data['foreign_id']) && $data['foreign_id']){
			$foreign_id = $data['foreign_id'];
		}
		$model = new Model\YohoBaseModel($this->tableName, $program_id);
		$app_model = new Model\YohoBaseModel("user_rank_program_application");
		$program = $model->getData();

		// Step 1. Check this register is applied?
		$applied = $db->getOne("SELECT COUNT(*) FROM " . $ecs->table("user_rank_program_application") . " WHERE program_id = $program_id AND user_id = $user_id");
		
		if ($applied) {
			$applied_user = $db->getRow("SELECT stage, expiry FROM " . $ecs->table("user_rank_program_application") . " WHERE program_id = $program_id AND user_id = $user_id");
			$stage  = $applied_user['stage'];
			$expiry = strtotime($applied_user['expiry']);
			$now = strtotime('now');
			if($expiry > $now) {
				if (in_array($stage, [RANK_APPLICATION_PREPARE, RANK_APPLICATION_APPROVED])) {
			 		return $response = [
			 			"error" => 1,
						"msg" => "user_rank_program_applied"
			 		];
				}
				if (in_array($stage, [RANK_APPLICATION_DISAPPROVE])) {
					return $response = [
						"error" => 1,
						"msg" => "user_rank_program_rejected"
					];
				}
			}
		}
		$type = $program['verifiy_type'];
		$verify_code = $program['special_code'];

		if (!empty($_SESSION['user_rank_program_data'][$program_id])) {
			// Step 2. Handle user program verify photo.
			$path = ROOT_PATH . "data/rank_program";
			if (!file_exists($path)) {
				mkdir($path, 0755, true);
			}
			$file = fopen("$path/" . md5($user_id . "_" . $program_id) . ".txt", "w");
			fwrite($file, $_SESSION['user_rank_program_data'][$program_id]);
			fclose($file);
		}

		if ($program['validate'] == 0) {
			// Step 3. Check program need validate, if No need to verify: Auto apply. verified = 1
			if (empty($applied)) {
				$default_expiry = $db->getOne("SELECT default_expiry FROM " . $ecs->table("user_rank_program") . " WHERE program_id = $program_id");
				$sql = "INSERT INTO " . $ecs->table("user_rank_program_application") . " (user_id, program_id, proof, expiry, stage, verify_mail, verified) VALUES ($user_id, $program_id, '$proof', '$default_expiry', '" . RANK_APPLICATION_PREPARE . "', '$verify_email', 1)";
			} else {  //is applied, update.
				$sql = "UPDATE " . $ecs->table("user_rank_program_application") . " SET proof = '$proof', stage = " . RANK_APPLICATION_PREPARE . ", verify_mail = '$verify_email', verified = 1  WHERE program_id = $program_id AND user_id = $user_id";
			}
			$db->query($sql, 'SILENT');
			unset($_SESSION['user_rank_program_data'][$program_id]);
			return $response = [
				"error" => 0,
				"msg" => "user_rank_program_received"
			];
		}
		if($type == 1) {  // use organization email
			// Step 4. Check program organization list
			$organization_ids = $db->getOne("SELECT organization_ids FROM " . $ecs->table('user_rank_program') . " WHERE program_id = " . $program_id . " LIMIT 1");
			if (empty($organization_ids)) { // Step 4.1. No organization list: need to manual apply
				if (empty($applied)) {
					$default_expiry = $db->getOne("SELECT default_expiry FROM " . $ecs->table("user_rank_program") . " WHERE program_id = $program_id");
					$sql = "INSERT INTO " . $ecs->table("user_rank_program_application") . " (user_id, program_id, proof, expiry, stage, verify_mail) VALUES ($user_id, $program_id, '$proof', '$default_expiry', '" . RANK_APPLICATION_WAIT . "', '$verify_email')";
				} else {
					$sql = "UPDATE " . $ecs->table("user_rank_program_application") . " SET proof = '$proof', stage = " . RANK_APPLICATION_WAIT . ", verify_mail = '$verify_email'  WHERE program_id = $program_id AND user_id = $user_id";
				}
				$db->query($sql, 'SILENT');
				unset($_SESSION['user_rank_program_data'][$program_id]);
				return $response = [
					"error" => 0,
					"msg" => "user_rank_program_received"
				];
			} else {
				if (empty($verify_email)) $response = ["error" => 1, "msg" => sprintf(_L('account_please_fill_in', '請輸入%s'), _L('account_email', '電郵地址'))];
				// Step 4.0. Check duplicate email
				if ($db->getOne("SELECT COUNT(*) FROM " . $ecs->table("user_rank_program_application") . " WHERE program_id = $program_id AND verify_mail = '$verify_email' AND user_id != $user_id AND stage " . db_create_in([RANK_APPLICATION_PREPARE, RANK_APPLICATION_APPROVED]))) {
					return $response = [
						"error" => 1,
						"msg" => "user_rank_program_email_duplicate"
					];
				}

				// Step 4.1. Have organization list: check user eligible
				$organization_ids = unserialize($organization_ids);
				$organization_domains = $db->getAll("SELECT * FROM " . $ecs->table("program_organization") . " WHERE organization_id" . db_create_in($organization_ids));
				$check_domain = substr(strrchr($verify_email, "@"), 1);
				$verify_domain = false;
				$selected_organization = [];
				foreach ($organization_domains as $k => $organization) {
					if ($check_domain == $organization['organization_domain']) {
						$selected_organization = $organization;
						$verify_domain = true;
						break;
					}
				}

				if ($verify_domain) { // Step 4.2.1 Send verify email
					// generate verify code
					$verify_info = [
						'verify_type' => self::VERIFY_TYPE_MAIL,
						'verify_target' => $verify_email,
						'user_id' => $user_id
					];
					$verify_data = $this->generateVerifyCode($verify_info);
					$url = $ecs->url() . 'user.php?act=validate_program&pid=' . $program_id . '&verify=' . $verify_data['code'];
					if (can_send_mail($verify_email, true)) {
						$tpl = get_mail_template('program_verify_email');
						$smarty->assign('program', $program);
						$smarty->assign('verify_url', $url);
						$smarty->assign('shop_url', $ecs->url());
						$smarty->assign('shop_name', $GLOBALS['_CFG']['shop_name']);
						$smarty->assign('send_date', local_date($GLOBALS['_CFG']['date_format']));
						$smarty->assign('sent_date', local_date($GLOBALS['_CFG']['date_format']));
						$content = $smarty->fetch('str:' . $tpl['template_content']);
						send_mail('', $verify_email, $GLOBALS['_CFG']['shop_name'] . " - " . $program['program_name'], $content, $tpl['is_html']);
					}
					if (empty($applied)) {
						$default_expiry = $db->getOne("SELECT default_expiry FROM " . $ecs->table("user_rank_program") . " WHERE program_id = $program_id");
						$sql = "INSERT INTO " . $ecs->table("user_rank_program_application") . " (user_id, program_id, proof, expiry, stage, verify_mail, organization_id) VALUES ($user_id, $program_id, '$proof', '$default_expiry', '" . RANK_APPLICATION_WAIT . "', '$verify_email', ".$selected_organization['organization_id'].")";
					} else {
						$sql = "UPDATE " . $ecs->table("user_rank_program_application") . " SET organization_id = ".$selected_organization['organization_id'].", proof = '$proof', stage = " . RANK_APPLICATION_WAIT . ", verify_mail = '$verify_email'  WHERE program_id = $program_id AND user_id = $user_id";
					}
					$db->query($sql, 'SILENT');
					unset($_SESSION['user_rank_program_data'][$program_id]);
					return $response = [
						"error" => 0,
						"msg" => "user_rank_program_received"
					];
				} else { // Step 4.2.2 email domain not find.
					unset($_SESSION['user_rank_program_data'][$program_id]);
					return $response = [
						"error" => 1,
						"msg" => "user_rank_program_email_domain_not_find"
					];
				}
			}
		} elseif ($type == 2 || $type == 3) { // use special code
			if ($type == 3 && !$foreign_id){
				return $response = [
					"error" => 1,
					"msg" => "user_rank_program_foreign_id_not_received"
				];
			}
			// Step 4. Check program special code
			$special = $data['special'];
			if($special == $verify_code) {
                $default_expiry = $db->getOne("SELECT default_expiry FROM " . $ecs->table("user_rank_program") . " WHERE program_id = $program_id");
				if (empty($applied)) {
					if ($type == 3){
						$sql = "INSERT INTO " . $ecs->table("user_rank_program_application") . " (user_id, program_id, proof, expiry, stage, verify_mail, verified, ref_id) VALUES ($user_id, $program_id, '', '$default_expiry', '" . RANK_APPLICATION_PREPARE . "', '', 1, '$foreign_id')";
					} else {
						$sql = "INSERT INTO " . $ecs->table("user_rank_program_application") . " (user_id, program_id, proof, expiry, stage, verify_mail, verified) VALUES ($user_id, $program_id, '', '$default_expiry', '" . RANK_APPLICATION_PREPARE . "', '', 1)";
					}
				} else {  //is applied, update.
					$sql = "UPDATE " . $ecs->table("user_rank_program_application") . " SET stage = " . RANK_APPLICATION_PREPARE . ", verified = 1, expiry = '$default_expiry'  WHERE program_id = $program_id AND user_id = $user_id";
				}
				$db->query($sql, 'SILENT');
				unset($_SESSION['user_rank_program_special']);
				$rank_list = $this->getUserRankProgram($_SESSION['user_id']);
				if($rank_list && !defined('IS_POS')) {
					$_SESSION['user_rank_program'] = $rank_list;
				} else {
					unset($_SESSION['user_rank_program']);
				}
				return $response = [
					"error" => 0,
					"msg" => "user_rank_program_received"
				];
			}
		}
	}

	/** Validate program user by email link.
	 * @param $data
	 * @return array
	 */
	public function validateProgram($data)
	{
		global $ecs, $db;
		// Get info
		$user_id = $_SESSION['user_id'];
		$program_id = $data['pid'];
		$sql = "SELECT verify_mail FROM " . $ecs->table('user_rank_program_application') . " WHERE program_id = $program_id AND user_id = $user_id";
		$email = $db->getOne($sql);
		$sql = "SELECT urp.program_id, urp.program_code, IFNULL(urpl.program_name, urp.program_name) as program_name, IFNULL(urpl.description, urp.description) as description, urp.theme_color, urp.status " .
			" FROM " . $ecs->table("user_rank_program") . " as urp " .
			"LEFT JOIN " . $ecs->table('user_rank_program_lang') . " as urpl ON urpl.program_id = urp.program_id AND urpl.lang = '" . $_CFG['lang'] . "' " .
			" WHERE urp.program_id = '" . $program_id . "' ";
		$program = $db->getRow($sql);
		if (empty($email)) return ['error' => 1, 'message' => 'null_verify_email'];
		$result = $this->verifyCode($data['verify'], $email, $user_id);
		if ($result['error'] == 0) { // Success, we auto update user application to prepare approved
			if ($db->getOne("SELECT COUNT(*) FROM " . $ecs->table("user_rank_program_application") . " WHERE program_id = $program_id AND verify_mail = '$email' AND user_id != $user_id AND stage " . db_create_in([RANK_APPLICATION_PREPARE, RANK_APPLICATION_APPROVED]))) {
				return $response = [
					"error" => 1,
					"msg" => "user_rank_program_email_duplicate"
				];
			}
			$sql = "UPDATE " . $ecs->table("user_rank_program_application") . " SET stage = " . RANK_APPLICATION_PREPARE . ", verified = 1 WHERE program_id = $program_id AND user_id = $user_id";
			$db->query($sql, 'SILENT');
			return ['error' => 0, 'email' => $email, 'program' => $program];
		} else return $result; // return error
	}

	/* -----------Organization------------ */

	public function editOrganizationAction()
	{
		global $ecs, $db, $_LANG;
		$insert = false;
		if (!empty($_REQUEST['act'] == "add_organization")) {
			$insert = true;
		}
		$organization_id = empty($_REQUEST['id']) ? "" : $_REQUEST['id'];
		if (empty($organization_id) && !$insert) {
			sys_msg("沒有已選擇的項目");
		}
		if (!$insert) {
			$sql = "SELECT * FROM " . $ecs->table("program_organization") .
				" WHERE organization_id = $organization_id";
			$record = $db->getRow($sql);
			$assign["record"] = $record;
		}

		$assign["organization_type"] = self::ORGANIZATION_TYPE_STRING_ARRAY;
		$assign['ur_here'] = ($_REQUEST['act'] == "add_organization") ? $_LANG['add_organization'] : $_LANG['edit_organization_ur_here'];
		$assign['action'] = 'update_o';
		$assign['organization_id'] = $organization_id;
		$assign['action_link'] = array('text' => $_LANG['list_organization'], 'href' => 'user_rank_program.php?act=list_organization');
		$template = "program_organization_info.htm";

		$this->generateActionTemplate($assign, $template);
	}

	public function addProgramRemark($order_id)
	{
		global $ecs, $db;
		$rank_programs = $db->getCol(
			"SELECT DISTINCT urp.program_name FROM " . $ecs->table("user_rank_program") . " urp " .
			"LEFT JOIN " . $ecs->table("order_goods") . " og ON og.user_rank = urp.rank_id " .
			"WHERE og.user_rank NOT IN (0, 2) AND order_id = $order_id"
		);
		$postscript = $db->getOne("SELECT postscript FROM " . $ecs->table("order_info") . " WHERE order_id = $order_id");
		$postscript .= (empty($postscript) ? "" : "\n") . implode(", ", $rank_programs);
		$db->query("UPDATE " . $ecs->table("order_info") . " SET postscript = '$postscript' WHERE order_id = $order_id");
	}

	public function getPricingProfileList() {
		global $ecs, $db ,$userRankController,$controller;
		$sql = "SELECT urppp.*,count(goods_id) as goods_total FROM " . $ecs->table("user_rank_program_pricing_profile"). " urppp left join ".$ecs->table("user_rank_program_pricing_profile_items")." urpppi on (urppp.pricing_profile_id = urpppi.pricing_profile_id) " .
				" WHERE 1 ".
				" group by pricing_profile_id  ORDER BY $_REQUEST[sort_by] $_REQUEST[sort_order]"." LIMIT " . $_REQUEST['start'] . ",$_REQUEST[page_size] ";
		
		$list = $db->getAll($sql);
		$sql = "SELECT count(*) FROM " . $ecs->table("user_rank_program_pricing_profile") .
				"WHERE 1 ";
		$count = $db->getOne($sql);
		foreach ($list as $key => $row) {
			$list[$key]["type_string"] = $userRankController::ORGANIZATION_TYPE_STRING_ARRAY[$row['organization_type']];
			$list[$key]["_action"] = '<span class="_action" data-source="_action"><a href="user_rank_program.php?act=edit_pricing_profile&pricing_profile_id='.$row['pricing_profile_id'].'" class="btn btn-info act_edit btn-xs"><i class="fa fa-edit"> </i> 編輯</a>';
			$list[$key]["_action"] .= '<a href="#" class="btn btn-danger  btn-xs confirm_delete_action_profile" data-pricing-profile-id="'.$row['pricing_profile_id'].'" ><i class="fa fa-trash"></i> 刪除</a></span>';
		}

		$arr = array(
            'data' => $list,
            'record_count' => sizeof($list)
          );
        
        return $arr;
	}

	function deletePricingProfile()
	{
		global $ecs, $db, $_LANG;

		// check if the profile is still using.
		$sql = "select count(*) from ".$ecs->table("user_rank_program")." where pricing_profile_id = ".$_REQUEST['pricing_profile_id']." ";
		$count = $db->getOne($sql);

		$ar = [];
		if ($count>0) {
			$ar['error'] = 1;
			$ar['message'] = '價格方案有計劃使用中,不能刪除';
			return $ar;
		}
		$db->query('START TRANSACTION');
		$sql = "delete from ".$ecs->table("user_rank_program_pricing_profile")." where pricing_profile_id = ".$_REQUEST['pricing_profile_id']." ";
		$db->query($sql);

		$sql = "delete from ".$ecs->table("user_rank_program_pricing_profile_items")." where pricing_profile_id = ".$_REQUEST['pricing_profile_id']." ";
		$db->query($sql);
		$db->query('COMMIT');
		return true;
	}

	function searchPricingProfileProductsList()
	{
		global $ecs, $db, $_LANG;

		$is_pagination = false;

		// handel filter
		if ($_REQUEST['sort_by'] == 'goods_sn_html') {
			$_REQUEST['sort_by'] = 'goods_sn';
		}

		if (!isset($_REQUEST['action']) || $_REQUEST['action'] == false) {

			$arr = array(
				'data' => [],
				'record_count' => 0
			  );
			
			return $arr;
		}

		$sql = "select urppp.*, urpppi.*, goods_sn,goods_name,goods_thumb,goods_img,market_price,shop_price, ". 
		" IFNULL((SELECT mp.user_price FROM " . $ecs->table('member_price') . " as mp WHERE mp.goods_id = g.goods_id AND mp.user_rank = '2'),'-1') as vip_price ".
		"from ".$ecs->table('user_rank_program_pricing_profile')." urppp 
		left join ".$ecs->table('user_rank_program_pricing_profile_items')." urpppi on (urppp.pricing_profile_id = urpppi.pricing_profile_id) 
		left join ".$ecs->table('goods')." g  on (urpppi.goods_id = g.goods_id) where g.goods_id is not null ";
		
		

		if (!empty($_REQUEST['goods_sn'])) {
			$sql .= " and g.goods_sn = ".$_REQUEST['goods_sn']." ";
		}


		if (!empty($_REQUEST['goods_name'])) {
			$sql .= " and g.goods_name like '%".$_REQUEST['goods_name']."%' ";
		}

		$sql .= ' order by '. $_REQUEST['sort_by'] . ' ' .  $_REQUEST['sort_order'];
		$sql .= ($is_pagination ? " LIMIT " . $_REQUEST['start'] . ", " . $_REQUEST['page_size'] : '');

		// echo $sql;
		// exit;

		$res=$db->getAll($sql);

		foreach ($res as $key => &$item) {
			$goods_id=$item['goods_id'];
			$item['goods_name'] = '<a target="_blank" href="goods.php?act=edit&id='.$item['goods_id'].'">'.$item['goods_name'] .'</a>';

			if(!empty($item['goods_thumb']))
			{
				$item['goods_thumb'] = '<img width="50" src="../'.$item['goods_thumb'].'" />';
			}
			if(!empty($item['goods_img']))
			{
				$item['goods_img'] ='../'.$item['goods_img'];
			}

			$item['goods_sn_html'] = '<span class="goods_id_'.$item['goods_id'].'" >'.$item['goods_sn'].'</span>'; 

			// check if the price > vip / original price
			$price_alert = '';
			if ( ($item['price'] > $item['vip_price'] && $item['vip_price'] >0) || $item['price'] > $item['shop_price']) {
				$price_alert = '<i class="fas fa-exclamation-circle"></i>';
			}

			$item['user_price_html'] = '<span class="editable" style="cursor:pointer" data-price-id="'.$item['pricing_profile_items_id'].'" id="span_user_price_'. $item['pricing_profile_items_id'] .'">'.$price_alert . $item['price'] .'</span>';
			$item['user_price_html'] .= '<input size=10 maxlength="10" style="display:none;" data-goods-id="'.$item['goods_id'].'" data-price-id="'.$item['pricing_profile_items_id'].'" data-pricing-profile-id="'.$item['pricing_profile_id'].'"  class="input_user_price" name="input_user_price_' . $item['pricing_profile_items_id'] . '" id="input_user_price_' . $item['pricing_profile_items_id'] . '" value="'. $item['price'] .'" />';

			$item['action_html'] = ' <div style="display: inline-block;">
									<label >
									<input type="checkbox" data-goods-id="'.$item['goods_id'].'" data-price-id="'.$item['pricing_profile_items_id'].'"  data-pricing-profile-id="'.$item['pricing_profile_id'].'"   class="js-switch" '.($item['active'] ? 'checked' : '' ).' /> 
									</label>
									</div>'; 
			$item['action_html'] .= '<div style="display: inline-block;" ><a class="confirm_delete_action" data-pricing-profile-id="'.$item['pricing_profile_id'].'" data-goods-id="'.$item['goods_id'].'" data-price-id="'.$item['pricing_profile_items_id'].'" href="javascript:void(0)"> <i class="fa fa-trash red"></i> </a></div>';
		}
		
		$arr = array(
            'data' => $res,
            'record_count' => sizeof($res)
          );
		
        return $arr;
	}


	public function addPricingProfile()
	{	
		global $ecs, $db, $_LANG;
		$insert = false;
		if (!empty($_REQUEST['act'] == "add_pricing_profile")) {
			$insert = true;
		}
	
		$pricing_profile_id = empty($_REQUEST['id']) ? "" : $_REQUEST['id'];
		if (empty($pricing_profile_id) && !$insert) {
			sys_msg("沒有已選擇的項目");
		}
		
		$sql = "SELECT * FROM " . $ecs->table("user_rank_program_pricing_profile") .
			" WHERE status = 1" ;
		$profiles = $db->getAll($sql);

		$assign["profiles"] = $profiles;
		$assign['ur_here'] = ($_REQUEST['act'] == "add_organization") ? $_LANG['add_organization'] : $_LANG['edit_pricing_profile_ur_here'];
		$assign['action'] = 'insert_pricing_profile';
		$assign['pricing_profile_id'] = $pricing_profile_id;
		$assign['action_link'] = array('text' => $_LANG['list_pricing_profile'], 'href' => 'user_rank_program.php?act=list_pricing_profile');
		$template = "program_pricing_profile_info_create.htm";

		$this->generateActionTemplate($assign, $template);
	}

	public function insertPricingProfile()
	{
		global $ecs, $db, $_LANG;

		$db->query('START TRANSACTION');
		$sql = "insert into ". $ecs->table("user_rank_program_pricing_profile")." (profile_name,status)  values ('".$_REQUEST['profile_name']."',1) ";
		$db->query($sql);
		$pricing_profile_id = $db->Insert_ID();
		
		if (empty($pricing_profile_id)) {
			sys_msg("系統錯誤", 1);
		}

		if (isset($_REQUEST['old_pricing_profile_id']) && $_REQUEST['old_pricing_profile_id'] >0) {
			// copy items	
			// create pricing profile items
			$price_sql = "select * from ".$ecs->table('user_rank_program_pricing_profile_items')." where pricing_profile_id = ".$_REQUEST['old_pricing_profile_id']." ";
			$res_prices = $db->getAll($price_sql);
			foreach ($res_prices as $price) {
				$create_sql = "insert into ".$ecs->table('user_rank_program_pricing_profile_items')." (`pricing_profile_id`, `goods_id`, `price`, `active`)  VALUES ('". $pricing_profile_id ."', '".$price['goods_id']."', '".$price['price']."', '1') ";
			    $db->query($create_sql);
		   }
		}
		
		$db->query('COMMIT');
		/* 管理员日志 */
		//admin_log(trim($rank_name), 'add', 'user_rank');
		clear_cache_files();
		header("Location: user_rank_program.php?act=edit_pricing_profile&pricing_profile_id=".$pricing_profile_id);
		exit;
	}

	public function editPricingProfile() 
	{
		global $ecs, $db, $_LANG;
		$insert = false;
	
		$pricing_profile_id = empty($_REQUEST['pricing_profile_id']) ? "" : $_REQUEST['pricing_profile_id'];
		if (empty($pricing_profile_id) && !$insert) {
			sys_msg("沒有已選擇的項目");
		}
		
		// get user program list
		$sql = "select * from ".$ecs->table("user_rank_program")." where pricing_profile_id = ". $_REQUEST['pricing_profile_id']." ";
		$rank_programms = $db->getAll($sql);

		$sql = "SELECT * FROM " . $ecs->table("user_rank_program_pricing_profile") .
			" WHERE pricing_profile_id = $pricing_profile_id";
		$record = $db->getRow($sql);
		$assign["record"] = $record;
		
		$assign['ur_here'] = ($_REQUEST['act'] == "add_organization") ? $_LANG['add_organization'] : $_LANG['edit_pricing_profile_ur_here'];
		$assign['action'] = 'update_pricing_profile';
		$assign['rank_programms'] = $rank_programms;
		$assign['pricing_profile_id'] = $pricing_profile_id;
		$assign['action_link'] = array('text' => $_LANG['list_pricing_profile'], 'href' => 'user_rank_program.php?act=list_pricing_profile');
		$template = "program_pricing_profile_info_edit.htm";

		$this->generateActionTemplate($assign, $template);
	}

	public function searchAndInsertGoods() 
	{
		global $ecs, $db, $_LANG;

		require_once(ROOT_PATH .ADMIN_PATH .'/includes/ERP/lib_erp_goods.php');
		include_once(ROOT_PATH . 'includes/cls_json.php');
		$json  = new \JSON;
	
		$goods_sn=isset($_REQUEST['goods_sn'])?trim($_REQUEST['goods_sn']):'';
		$goods_name=isset($_REQUEST['goods_name'])?trim($_REQUEST['goods_name']):'';
		$goods_qty=isset($_REQUEST['goods_qty']) &&intval($_REQUEST['goods_qty'])>0?intval($_REQUEST['goods_qty']):1;
		$user_rank_id = $_REQUEST['user_rank_id'];
		$goods_price = $_REQUEST['goods_price'];
		$pricing_profile_id = $_REQUEST['pricing_profile_id'];

		if(empty($goods_sn) &&empty($goods_name) &&empty($goods_barcode))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_warehousing_goods_sn_or_goods_name_or_goods_barcode_required'];
			return $result;
		}
		
		if(!empty($goods_sn))
		{
			$goods_id=check_goods_sn($goods_sn);
			if(empty($goods_id))
			{
				$result['error']=1;
				$result['message']=$_LANG['erp_warehousing_wrong_goods_sn'];
				return $result;
			}
		}
		elseif(!empty($goods_name))
		{
			$goods_name = urldecode($goods_name);
			$goods_id=check_goods_name($goods_name);
			
			if(empty($goods_id))
			{
				$result['error']=1;
				$result['message']=$_LANG['erp_warehousing_wrong_goods_name'];
				return $result;
			}
		}
		
		$this->insertUserPrice($pricing_profile_id,$goods_id,$goods_price);
	
		// $goodsController = new GoodsController();
		// $data = [];
		// $data['id'] = $goods_id;
		// $data['member_price'][$user_rank_id] = $goods_price;
		// $data['return'] = true;
		// $goodsController->updateUrpAction($data);

		$res = $goods_id;

		return $res;
	}

	function insertUserPrice($pricing_profile_id,$goods_id,$goods_price) {
		global $ecs, $db, $_LANG;

		$sql = "SELECT COUNT(*) FROM " . $ecs->table('user_rank_program_pricing_profile_items') .
		" WHERE pricing_profile_id = '$pricing_profile_id' AND goods_id = '$goods_id'";
		if ($db->getOne($sql) > 0) {//判断是否有USER_PRICE数值
			$sql = "update ".$ecs->table("user_rank_program_pricing_profile_items")." set price = '".$goods_price."' where pricing_profile_id = ".$pricing_profile_id." and goods_id = ".$goods_id." ";
			$db->query($sql);
		} else {
			$sql = "insert into ".$ecs->table('user_rank_program_pricing_profile_items')." (pricing_profile_id,goods_id,price) values (".$pricing_profile_id.",$goods_id,'".$goods_price."') ";
			$db->query($sql);
			// update member price also
		}
		//$this->updateMemberPrice('save',$pricing_profile_id,$goods_id,$goods_price);
		return true;
	}

	function getPricingProfileItems($pricing_profile_id)
	{
		global $ecs, $db, $_LANG;


		// get goods sold only count the first programm
		$sql = "SELECT * FROM " . $ecs->table("user_rank_program") .
			" WHERE pricing_profile_id = $pricing_profile_id";
		$record = $db->getRow($sql);
		$rank_id = $record['rank_id'];

		if (!empty($rank_id)) {
			$sql = "select user_rank,goods_id, count(goods_id) as count from ".$ecs->table('order_goods') ." og  left join ".$ecs->table('order_info')." o  on (og.order_id = o.order_id) where o.pay_status = ".PS_PAYED." and  user_rank = ".$rank_id." group by user_rank, goods_id ";
			$order_goods_record = $db->getAll($sql);
		}
		
		$order_goods_ar = [];
		foreach ($order_goods_record as $order_goods) {
			$order_goods_ar[$order_goods['goods_id']] = $order_goods['count']; 
		}

		$is_pagination = false;
		$sql="select urppp.*, urpppi.*, goods_sn,goods_name,goods_thumb,goods_img,market_price,shop_price, ". 
		
		" IFNULL((SELECT mp.user_price FROM " . $ecs->table('member_price') . " as mp WHERE mp.goods_id = g.goods_id AND mp.user_rank = '2'),'-1') as vip_price ".
		"from ".$ecs->table('user_rank_program_pricing_profile')." urppp left join ".$ecs->table('user_rank_program_pricing_profile_items')." urpppi on (urppp.pricing_profile_id = urpppi.pricing_profile_id) left join ".$ecs->table('goods')." g  on (urpppi.goods_id = g.goods_id) where urppp.pricing_profile_id ='".$pricing_profile_id."' and g.goods_id is not null ";

		$sql .= ' order by '. $_REQUEST['sort_by'] . ' ' .  $_REQUEST['sort_order'];
        $sql .= ($is_pagination ? " LIMIT " . $_REQUEST['start'] . ", " . $_REQUEST['page_size'] : '');

		$res=$db->getAll($sql);

		foreach ($res as $key => &$item) {
			$goods_id=$item['goods_id'];
			$item['goods_name'] = '<a target="_blank" href="goods.php?act=edit&id='.$item['goods_id'].'">'.$item['goods_name'] .'</a>';

			if(!empty($item['goods_thumb']))
			{
				$item['goods_thumb'] = '<img width="50" src="../'.$item['goods_thumb'].'" />';
			}
			if(!empty($item['goods_img']))
			{
				$item['goods_img'] ='../'.$item['goods_img'];
			}

			if (isset($order_goods_ar[$goods_id])) {
				$item['sold'] = $order_goods_ar[$goods_id];
			} else {
				$item['sold'] = 0;
			}

			$item['goods_sn_html'] = '<span class="goods_id_'.$item['goods_id'].'" >'.$item['goods_sn'].'</span>'; 

			// check if the price > vip / original price
			$price_alert = '';
			if ( ($item['price'] > $item['vip_price'] && $item['vip_price'] >0) || $item['price'] > $item['shop_price']) {
				$price_alert = '<i class="fas fa-exclamation-circle"></i>';
			}

			$item['user_price_html'] = '<span class="editable" style="cursor:pointer" data-price-id="'.$item['pricing_profile_items_id'].'" id="span_user_price_'. $item['pricing_profile_items_id'] .'">'. $price_alert  . $item['price'] .'</span>';
			$item['user_price_html'] .= '<input size=10 maxlength="10" style="display:none;" data-goods-id="'.$item['goods_id'].'" data-price-id="'.$item['pricing_profile_items_id'].'"  class="input_user_price" name="input_user_price_' . $item['pricing_profile_items_id'] . '" id="input_user_price_' . $item['pricing_profile_items_id'] . '" value="'. $item['price'] .'" />';

			$item['action_html'] = ' <div style="display: inline-block;">
									<label >
									<input type="checkbox" data-goods-id="'.$item['goods_id'].'" data-price-id="'.$item['pricing_profile_items_id'].'"  class="js-switch" '.($item['active'] ? 'checked' : '' ).' /> 
									</label>
									</div>'; 
			  $item['action_html'] .= '<div style="display: inline-block;" ><a class="confirm_delete_action" data-goods-id="'.$item['goods_id'].'" data-price-id="'.$item['pricing_profile_items_id'].'" href="javascript:void(0)"> <i class="fa fa-trash red"></i> </a></div>';
		}
		
		$arr = array(
            'data' => $res,
            'record_count' => sizeof($res)
          );
		
        return $arr;
	}

	function updateUserPrice($pricing_profile_id, $goods_id, $update_price) {
		global $ecs, $db, $_LANG;

		$sql = "update ".$ecs->table("user_rank_program_pricing_profile_items")." set price = '".$update_price."' where pricing_profile_id = ".$pricing_profile_id." and goods_id = ".$goods_id." ";
		$db->query($sql);

		// query the new price
		$sql = "select price from ".$ecs->table("user_rank_program_pricing_profile_items")."  where pricing_profile_id = ".$pricing_profile_id." and goods_id = ".$goods_id." ";
		$price = $db->getOne($sql);
		$result = array('success' => '1','user_price' => number_format($price,2,'.',''));

		// update member price also
		$this->updateMemberPrice('save',$pricing_profile_id,$goods_id,$price);

		return $result;
	}

	function updateMemberPrice($action,$pricing_profile_id,$goods_id,$price) {
		global $ecs, $db, $_LANG;

		// find user rank using that profile
		$sql = "select rank_id from ".$ecs->table("user_rank_program")." where pricing_profile_id = ".$pricing_profile_id." ";
		$rank_ids = $db->getCol($sql);
		foreach ($rank_ids as $rank_id) {
			if ($action == 'update') {
				//if ($price > 0) {
				    $sql = "update ".$ecs->table("member_price")." set user_price = ".$price." where goods_id = ".$goods_id." and user_rank = ".$rank_id." ";
					$db->query($sql);
				//}
			} else if ($action == 'insert') {
				//if ($price > 0) {
					$sql =	"INSERT INTO " . $ecs->table('member_price') ." (user_price,user_rank,goods_id, max_amount) VALUES ('". $price ."','".$rank_id."','".$goods_id."', 0)";
					$db->query($sql);
				//}
			} else if ($action == 'delete') {
				$sql =	"delete from " . $ecs->table('member_price') ." where user_rank = ".$rank_id." and goods_id = '".$goods_id."' ";
				$db->query($sql);
			} else if ($action == 'save') {
				$sql = "SELECT COUNT(*) FROM " . $ecs->table('member_price') .
				" WHERE user_rank = '$rank_id' AND goods_id = '$goods_id'";
				if ($db->getOne($sql) > 0) {
					$sql = "update ".$ecs->table("member_price")." set user_price = ".$price." where goods_id = ".$goods_id." and user_rank = ".$rank_id." ";
					$db->query($sql);
				} else {
					$sql =	"INSERT INTO " . $ecs->table('member_price') ." (user_price,user_rank,goods_id, max_amount) VALUES ('". $price ."','".$rank_id."','".$goods_id."', 0)";
					$db->query($sql);
				}
			}
		}
	}

	function deleteUserPrice($pricing_profile_id,$goods_id) {
		global $ecs, $db, $_LANG;
		$db->query('START TRANSACTION');
		$sql = "delete from ".$ecs->table("user_rank_program_pricing_profile_items")." where pricing_profile_id = ".$pricing_profile_id." and goods_id = ".$goods_id." ";
		$db->query($sql);

		// update member price also
		$this->updateMemberPrice('delete',$pricing_profile_id,$goods_id,'');

		$db->query('COMMIT');
		return true;
	}

	function updatePricingProfileName($pricing_profile_id,$profile_name) {
		global $ecs, $db, $_LANG;

		$sql = "update ".$ecs->table("user_rank_program_pricing_profile")." set profile_name = '".$profile_name."' where pricing_profile_id = ".$pricing_profile_id." ";
		$db->query($sql);
		return true;
	}

	function getUserRankProgramBasicInfo($program_id) {
		global $ecs, $db, $_LANG;
		$sql = "select * from ".$ecs->table("user_rank_program")." where program_id = '".$program_id."' ";
		$row = $db->getRow($sql);

		return $row;
	}

	function updateProgramRankId($program_id,$rank_id) {
		global $ecs, $db, $_LANG;
		$sql = "update ".$ecs->table("user_rank_program")." set rank_id = ".$rank_id." where program_id = '".$program_id."' ";
		$db->query($sql);
		return true;
	}

	function getPricingProfileInfo($pricing_profile_id) {
		global $ecs, $db, $_LANG;
		$sql = "select * from ".$ecs->table("user_rank_program_pricing_profile")." where pricing_profile_id = '".$pricing_profile_id."' ";
		$row = $db->getRow($sql);

		return $row;
	}

	function creatNewUserRank($rank_name) {
		global $ecs, $db, $_LANG;
		// create a new user rank level 
		$sql = "select count(*) from ".$ecs->table('user_rank')." ur where rank_name = '".$rank_name."' ";
		$res = $db->getOne($sql);

		if ($res>0) {
			sys_msg(sprintf($_LANG['rank_name_exists'], trim($_POST['rank_name'])), 1);
			return false;
		}

		$sql = "INSERT INTO " .$ecs->table('user_rank') ."( ".
				"rank_name, min_points, max_points, discount, special_rank, show_price".
			") VALUES (".
				"'".$rank_name."', '0', '0', ".
				"'100', '1', '0')";
		$db->query($sql);

		$user_rank_id = $db->Insert_ID();
		admin_log(trim($rank_name), 'add', 'user_rank');
		clear_cache_files();
		return $user_rank_id ;

		// // update pricing profile
		// $sql = "update ".$ecs->table('user_rank_program_pricing_profile')." set user_rank_id = ".$user_rank_id ." where pricing_profile_id = ".$pricing_profile_id." ";
		// $db->getOne($sql);
	}

	function updateMemberPriceByRankIdAndProfileId($rank_id,$pricing_profile_id) {
		global $ecs, $db, $_LANG;

		// get pricing profile
		$sql = "select * from ".$ecs->table('user_rank_program_pricing_profile_items')." where pricing_profile_id = '".$pricing_profile_id."' and active = 1 ";
		$result = $db->getAll($sql);
		
		// delete all member price of this rank
	    $sql = "delete from ".$ecs->table("member_price")." where user_rank = ".$rank_id." ";
		$db->query($sql);
		
		// insert new member price from pricing profile 
		foreach ($result as $item) {
			$sql =	"INSERT INTO " . $ecs->table('member_price') ." (user_price,user_rank,goods_id, max_amount) VALUES ('". $item['price'] ."','".$rank_id."','".$item['goods_id']."', 0)";
			$db->query($sql);
		}

		return true;
	}

	function updatePricingProfileItemStatus($active,$pricing_profile_id,$goods_id,$update_price) {
		global $ecs, $db, $_LANG;

		// update active status
		$sql = "update ".$ecs->table("user_rank_program_pricing_profile_items")." set active = ".$active." where pricing_profile_id = ".$pricing_profile_id." and goods_id = ".$goods_id." ";
		$db->query($sql);
	
		if ($active) {
			// insert or update member price
			$this->updateMemberPrice('save',$pricing_profile_id,$goods_id,$update_price);
		} else {
			// delete member price
			$this->updateMemberPrice('delete',$pricing_profile_id,$goods_id,$update_price);
		}
	}

	function getProfileItemWrongPrice() {
		global $ecs, $db, $_LANG;

		$sql = "select urppp.*, urpppi.*,g.shop_price,
		 		IFNULL((SELECT mp.user_price FROM " . $ecs->table('member_price') . " as mp WHERE mp.goods_id = g.goods_id AND mp.user_rank = '2'),'-1') as vip_price
				from ".$ecs->table("user_rank_program_pricing_profile_items")." urpppi 
				left join ".$ecs->table("user_rank_program_pricing_profile")." urppp on (urpppi.pricing_profile_id = urppp.pricing_profile_id) 
				left join ".$ecs->table("goods")." g on (urpppi.goods_id = g.goods_id) 
				where urpppi.active = 1";
			
		$res = $db->getAll($sql);
		// dd($res);
		// exit;
		$wrong_profiles = [];

		foreach ($res as $item) {
			if ( ($item['price'] > $item['vip_price'] && $item['vip_price'] >0) || $item['price'] > $item['shop_price']) {

				if (!in_array($item['pricing_profile_id'],array_column($wrong_profiles,'id'))) {
					$wrong_profiles[]= array ('id' => $item['pricing_profile_id'],'name' => $item['profile_name'] );
				}
			}
		}
		return $wrong_profiles;
	}

	function convert_pricing_profile() {
		global $ecs, $db, $_LANG;

		$sql = "select * from ". $ecs->table('user_rank_program')." where pricing_profile_id is null ";

		$res = $db->getAll($sql);
		$db->query('START TRANSACTION');
		foreach ($res as $program) {
			echo $program['program_name'];
			echo '<br>';
			// create pricing profile
			$create_sql = "insert into ".$ecs->table('user_rank_program_pricing_profile')." ( `profile_name`, `status`)  VALUES ('". $program['program_name']."','1') ";
			$db->query($create_sql);

			$pricing_profile_id = $db->Insert_ID();			
			
			// create pricing profile items
			$member_price_sql = "select * from ".$ecs->table('member_price')." where user_rank = ".$program['rank_id']." ";
			$res_member_prices = $db->getAll($member_price_sql);

			foreach ($res_member_prices as $member_price) {
			 	$create_sql = "insert into ".$ecs->table('user_rank_program_pricing_profile_items')." (`pricing_profile_id`, `goods_id`, `price`, `active`)  VALUES ('". $pricing_profile_id ."', '".$member_price['goods_id']."', '".$member_price['user_price']."', '1') ";
				$db->query($create_sql);
			}

			$update_sql = "update ".$ecs->table('user_rank_program')." SET `pricing_profile_id` = '".$pricing_profile_id ."' WHERE `program_id` = ".$program['program_id']."; ";
			$db->query($update_sql);
		}
		$db->query('COMMIT');
		echo '<br>';
		echo 'Finished';
		exit;
	}

	function getUserRankProgramName() {
		global $ecs, $db;

		$sql = "SELECT ur.rank_id, IFNULL(urp.program_name, ur.rank_name) AS rank_name 
				FROM ".$ecs->table('user_rank')." AS ur 
				LEFT JOIN ".$ecs->table('user_rank_program')." AS urp ON urp.rank_id = ur.rank_id 
				ORDER BY ur.rank_id ASC";
			
		$res = $db->getAll($sql);

		if (!empty($res) && sizeof($res) > 0){
			return $res;
		} else {
			return FALSE;
		}
	}

}
