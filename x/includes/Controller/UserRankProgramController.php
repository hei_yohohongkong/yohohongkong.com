<?php

/***
* UserRankProgramController.php
* by Sing 20180319
*
* Order form (User Program) controller
***/

namespace Yoho\cms\Controller;

use Yoho\cms\Model;

if (!defined('IN_ECS')) {
    die('Hacking attempt');
}

class UserRankProgramController extends YohoBaseController{

	const DB_REPAIR_ORDER_STATUS = ['待處理','待處理','維修中','送修完成待處理','完成及可出貨'];
    const DB_SELLING_PLATFORMS = ['pos'=>'POS','web'=>'網站','mobile'=>'手機版網站','app'=>'手機App','pos_exhibition'=>'展場POS'];

    public function ajaxQueryAction()
    {
        $list = $this->get_orderlist();

        $this->tableList = $list['orders'];
        $this->tableTotalCount = $list['record_count'];

        parent::ajaxQueryAction();
    }

    public function get_orderlist()
    {
        ini_set('memory_limit', '256M');

        $_REQUEST['start'] = isset($_REQUEST['start']) ?  $_REQUEST['start'] : 0;

        $_REQUEST['page'] = empty($_REQUEST['page']) || (intval($_REQUEST['page']) <= 0) ? 1 : intval($_REQUEST['page']);

        if (isset($_REQUEST['page_size']) && intval($_REQUEST['page_size']) > 0){
            $_REQUEST['page_size'] = intval($_REQUEST['page_size']);
        } else {
            $_REQUEST['page_size'] = 50;
        }
        
        if ($_REQUEST['page_size'] > 1000) {
            $_REQUEST['page_size'] = 1000;
        }

        $_REQUEST['sort_by'] = empty($_REQUEST['sort_by']) ? 'add_time' : trim($_REQUEST['sort_by']);
        $_REQUEST['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);

        $where = "WHERE og.user_rank IS NOT NULL AND pg.program_id IS NOT NULL ";

        if (isset($_POST['order_sn']) && $_POST['order_sn']) {
            $where .= "AND o.order_sn LIKE '%".$_POST['order_sn']."%' ";
        }

        if (isset($_POST['rank_program']) && $_POST['rank_program']) {
            $where .= "AND pg.program_id = ".$_POST['rank_program']." ";
        }

        if (isset($_POST['pay_status']) && $_POST['pay_status'] && $_POST['pay_status'] != 'payed') {
            if ($_POST['pay_status'] == 'unpayed') {
                $where .= "AND o.pay_status = 0 ";
            } elseif ( $_POST['pay_status'] == 'paying') {
                $where .= "AND o.pay_status = 1 ";
            }
        } else {
            $where .= "AND o.pay_status = 2 ";
        }
        
        $sql = "SELECT COUNT(distinct o.order_id) ".
                "FROM " . $GLOBALS['ecs']->table('order_info') . " AS o " .
                "LEFT JOIN " .$GLOBALS['ecs']->table('order_goods'). " AS og ON og.order_id=o.order_id ".
                "LEFT JOIN " .$GLOBALS['ecs']->table('user_rank_program'). " AS pg ON og.user_rank = pg.rank_id ".
                $where;

        $record_count   = $GLOBALS['db']->getOne($sql);

        $sql = "SELECT distinct o.order_id, o.order_sn, o.add_time, o.order_status, o.shipping_id, o.shipping_status, o.order_amount, o.money_paid, o.cp_price, " .
                    "o.pay_status, o.order_type, o.type_sn, o.type_id, o.actg_paid_amount,o.pay_id, o.extension_code, o.extension_id, o.postscript, o.serial_numbers,  " .
                    "o.consignee, o.address, o.country, o.email, o.tel, o.pay_time, s.shipping_name, o.shipping_fod, o.language, p.pay_name, " .
                    "(" . order_amount_field('o.') . ") AS total_fee, o.to_buyer, " .
                    "(o.money_paid + o.order_amount) AS order_total, " .
                    "IFNULL(u.user_name, 'anonymous') AS buyer, o.user_id, o.salesperson, o.platform, ".
                    "o.fully_paid, o.actg_paid_amount, ". 
                    "IFNULL((SELECT count(*) FROM ".$GLOBALS['ecs']->table('order_action')." as oa WHERE oa.order_id = o.order_id), 0) as action_count, " . 
                    "IFNULL((SELECT 1 FROM ".$GLOBALS['ecs']->table('order_action')." as oa WHERE oa.order_id = o.order_id AND oa.action_user = 'buyer' AND oa.order_status = '" . OS_CANCELED . "'), 0) as buyer_cancel, " . 
                    "IFNULL(pg.program_name, 'N/A') AS program " .
                    "FROM " . $GLOBALS['ecs']->table('order_info') . " AS o " .
                    "LEFT JOIN " .$GLOBALS['ecs']->table('payment'). " AS p ON p.pay_id=o.pay_id ".
                    "LEFT JOIN " .$GLOBALS['ecs']->table('shipping'). " AS s ON s.shipping_id=o.shipping_id ".
                    "LEFT JOIN " .$GLOBALS['ecs']->table('users'). " AS u ON u.user_id=o.user_id ".
                    "LEFT JOIN " .$GLOBALS['ecs']->table('order_goods'). " AS og ON og.order_id=o.order_id ".
                    "LEFT JOIN " .$GLOBALS['ecs']->table('user_rank_program'). " AS pg ON og.user_rank = pg.rank_id ".
                    $where .
                    "ORDER BY $_REQUEST[sort_by] $_REQUEST[sort_order] ";
                    ;
                   
        $sql .= "LIMIT " . $_REQUEST['start'] . ",$_REQUEST[page_size]";

        $row = $GLOBALS['db']->getAll($sql);

        $platforms = UserRankProgramController::DB_SELLING_PLATFORMS;

        $order_ids = array_map(function ($order) {
            return $order['order_id']; 
        }, $row);

        $order_goods_ids = [];
        $sql = "SELECT distinct og.goods_id " .
                "FROM " . $GLOBALS['ecs']->table('order_goods') . " as og " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON og.goods_id = g.goods_id " .
                "WHERE order_id " . db_create_in($order_ids);
        $order_goods_ids = $GLOBALS['db']->getCol($sql);
    
        $sql = "SELECT ega.goods_id, IFNULL(SUM(ega.goods_qty),0) as storage " .
                "FROM " . $GLOBALS['ecs']->table('erp_goods_attr_stock') . " AS ega " .
                "group by goods_id";
        
        $order_goods = $GLOBALS['db']->getAll($sql);
    
        $goods_qty = [];
        foreach ($order_goods as $order_goods_info){
            $goods_qty[$order_goods_info['goods_id']] = $order_goods_info['storage'];
        }
    
        foreach ($order_goods_ids as $order_goods_id) {
            if (!isset($goods_qty[$order_goods_id])) {
                $goods_qty[$order_goods_id] = 0;
            }
        }
    
        $sql = "SELECT og.*, g.goods_thumb, g.goods_height, g.goods_width, g.goods_length " .
            "FROM " . $GLOBALS['ecs']->table('order_goods') . " as og " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON og.goods_id = g.goods_id " .
            "WHERE order_id " . db_create_in($order_ids);
    
        $order_goods = $GLOBALS['db']->getAll($sql);
    
        $order_goods_by_order_id = [];

        foreach ($order_goods as $order_goods_key =>  $order_goods_info){
            $order_goods[$order_goods_key]['storage'] = $goods_qty[$order_goods_info['goods_id']];
            $order_goods_by_order_id[$order_goods_info['order_id']][] = $order_goods[$order_goods_key];
        }


        foreach ($row AS $key => $value){
            $row[$key]['formated_order_amount'] = price_format($value['order_amount']);
            $row[$key]['formated_money_paid'] = price_format($value['money_paid']);
            $row[$key]['formated_total_fee'] = price_format($value['total_fee']);
            $row[$key]['formated_order_total'] = price_format($value['order_total']);
            $row[$key]['short_order_time'] = local_date('m-d H:i', $value['add_time']);
            $row[$key]['formated_order_cp_price'] = price_format($value['cp_price']);
            if ($value['order_status'] == OS_INVALID || $value['order_status'] == OS_CANCELED){
                $row[$key]['can_remove'] = 1;
            } else {
                $row[$key]['can_remove'] = 0;
            }

            $row[$key]['platform_display'] = isset($platforms[$value['platform']]) ? $platforms[$value['platform']] : $value['platform'];
            
            $row[$key]['goods_list'] = array();
            $most_expensive = -1;
            foreach ($order_goods as $og) {
                if ($og['order_id'] == $value['order_id']) {
                    $og['goods_thumb'] = get_image_path($og['goods_id'], $og['goods_thumb'], true);
                    $og['goods_thumb'] = ((strpos($og['goods_thumb'], 'http://') === 0) || (strpos($og['goods_thumb'], 'https://') === 0)) ? $og['goods_thumb'] : $GLOBALS['ecs']->url() . $og['goods_thumb'];
                    $og['formated_subtotal'] = price_format($og['goods_price'] * $og['goods_number']);
                    $og['formated_goods_price'] = price_format($og['goods_price']);
                    $og['formated_cost'] = price_format($og['cost'], false);
                    
                    if($_REQUEST['display_order_goods'])$row[$key]['goods_list'][] = $og;
                    if ($og['goods_price'] > $most_expensive) {
                        $most_expensive = $og['goods_price'];
                        $row[$key]['ex_goods_thumb'] = $og['goods_thumb'];
                    }
                }
            }
    
            
            $row[$key]['risky'] = false;
            if ($value['pay_id'] == '7'){
                if ((($value['country'] == '3409') && (doubleval($value['order_total']) >= 6000)) || // 香港, $6000 or above
                    (($value['country'] == '3436') && (doubleval($value['order_total']) >= 3000)) || // 中國, $3000 or above
                    (($value['country'] == '3411') && (doubleval($value['order_total']) >= 3000)))   // 台灣, $3000 or above
                {
                    $row[$key]['risky'] = true;
                }
            }
            if ($value['pay_id'] == '3' && $value['pay_status'] == PS_PAYED && in_array($value['shipping_status'], [SS_SHIPPED, SS_RECEIVED])) {
                $oa_sql = "SELECT * FROM ".$GLOBALS['ecs']->table('order_action')." WHERE order_id = ".$value['order_id']." ORDER BY action_id DESC ";
                $log_list = $GLOBALS['db']->getAll($oa_sql);
    
                $have_unshipping_log = false;
                foreach($log_list as $log_key => $log) {
                    $status = $log['shipping_status'];
                    if(in_array($status, [SS_SHIPPED, SS_RECEIVED])) {
                        if($have_unshipping_log == true) {
                            $row[$key]['reship_order'] = true; // reship order
                            break;
                        }
                    } else if(in_array($status, [SS_UNSHIPPED])) {
                        $have_unshipping_log = true;
                    }
                }
            }
    
            $consignee_info = '';
            if ($value['consignee'])$consignee_info .= "<i class='fa fa-user'></i> <a href='mailto:$value[email]'>$value[consignee]</a>";
            if ($value['tel'])$consignee_info .= " <i class='fa fa-phone-square'></i>". $value[tel]. ($value['LWH'] > 0 ? '<b style="color:#9CC2CB"> (LWH - '.$value['LWH'].')</b>':'');
            if ($row[$key]['is_wholesale']) $consignee_info .= "<br/><i class='fa fa-truck'></i> <b>$value[shipping_name] (批發訂單$value[type_sn])</b>";
            else $consignee_info .= "<br/><b><i class='fa fa-truck'></i> $value[shipping_name] : ".(($value['shipping_fod'])?'(運費到付)':'')."</b> $value[address]";
            
            if (strlen(trim($value['postscript'])) > 0) $consignee_info .= "<br /><b><i class='fa fa-file-alt'></i> 客戶備註: $value[postscript]</b>";
            if (strlen(trim($value['to_buyer'])) > 0) $consignee_info .= "<br /><b class='blue'><i class='fa fa-thumbtack'></i> 內部備註: $value[to_buyer]</b>";
            $row[$key]['consignee_info'] = $consignee_info;
    
            $salesperson_info = $value['salesperson'];
            $salesperson_info .= "<br>".$row[$key]['platform_display'];
            $lang_list = available_language_names();
            if ($row[$key]['language'])$salesperson_info .= "<br>".($lang_list[$value['language']] ? $lang_list[$value['language']]: $value['language']);
            if ($row[$key]['is_sa'])$salesperson_info .= "<br>".$row[$key]['sa_name'];
            $row[$key]['salesperson_info'] = $salesperson_info;
            global $_LANG;
            $all_status = $_LANG['icon']['ps'][$value['pay_status']]." | ".$_LANG['icon']['ss'][$value['shipping_status']];
    
    
            if (in_array($_REQUEST['composite_status'], [CS_AWAIT_SHIP,CS_AWAIT_SHIP_HK,CS_AWAIT_SHIP_LOCKER,CS_AWAIT_PICK_HK,CS_AWAIT_SHIP_NON_HK,CS_AWAIT_SHIP_HK_YOHO,CS_AWAIT_HK_FLS,CS_AWAIT_HK_WILSON])) {
                if (in_array($value['order_id'],$orders_can_ship) || in_array($value['order_id'],$orders_can_ship_fls) || in_array($value['order_id'],$orders_can_ship_wilson) || in_array($value['order_id'],$orders_can_ship_shop)) {
                    $all_status.= " | ".$_LANG['icon']['all_in_stock']['Y'];
                } else {
                    $all_status.= " | ".$_LANG['icon']['all_in_stock']['N'];
                }
            }
    
    
            if (!empty($po_sn_list_ar[$row[$key]['order_id']])) {
                foreach ($po_sn_list_ar[$row[$key]['order_id']] as $po_order_sn) {
                    $row[$key]['po_sn_list'] .= '<a target="_blank" href="erp_order_manage.php?act=order_list&order_sn='.$po_order_sn.'">'.$po_order_sn.'</a>';
                    $row[$key]['po_sn_list'] .= '<br>';
                }
            } else {
                $row[$key]['po_sn_list'] = '';
            }
            
            $row[$key]['all_status'] = $all_status;
            $action = '';
            $action .= '<a class="badge '.($value['ordered_from_supplier'] ? 'bg-green':'bg-blue').'" href="javascript:;" onclick="addRemarkPopup('.$value['order_id'].')">'.$value['action_count'].'</a>';
              if ($row[$key]['can_remove']) $action .= '<br /><a href="javascript:;" data-url="order.php?act=remove&amp;id='.$value['order_id'].'" onclick="cellRemove(this);">'.$_LANG['remove'].'</a>';
            if ($row[$key]['is_wholesale'] || $_REQUEST['wholesale_receivable'])
            $action .= ($row[$key]['fully_paid'] ? '<br /><a href="wholesale_payments.php?act=list&order_sn='.$value['order_sn'].'">付款紀錄</a>' : '<br /><a href="wholesale_payments.php?act=add&order_id='.$value['order_id'].'">付款</a>').
              ' | <a href="javascript:;" onclick="javascript:completeOrderPayment('.$value['order_id'].')">完成</a>';
            $row[$key]['action'] = $action;
        }
    
        $arr = array('orders' => $row, 'record_count' => $record_count);
    
        return $arr;
    }
    
    public function get_programlist()
    {
        $sql = "SELECT p.program_id AS p_id, " .
            "p.program_name AS p_name " .
            "FROM " . $GLOBALS['ecs']->table('user_rank_program') . " AS p " .
            "ORDER BY p.program_id ASC";

        $row = $GLOBALS['db']->getAll($sql);

		return $row;
    }
    
}
