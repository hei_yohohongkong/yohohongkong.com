<?php

/***
* TagController.php
* by Anthony 20170627
*
* Tag controller
***/

namespace Yoho\cms\Controller;
use Yoho\cms\Model;
if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

class TagController extends YohoBaseController{

    const DB_ACTION_CODE_TAG_MANAGE = 'tag_manage';
    private $tablename = 'tag';

	public function __construct(){
		parent::__construct();

	}

    /**
    * 判断同一商品的标签是否唯一
    *
    * @param $name  标签名
    * @param $id  标签id
    * @return bool
    */
    function tag_is_only($name, $tag_id, $goods_id = '')
    {
        if(empty($goods_id))
        {
            $db = $GLOBALS['db'];
            $sql = 'SELECT goods_id FROM ' . $GLOBALS['ecs']->table('tag') . " WHERE tag_id = '$tag_id'";
            $row = $GLOBALS['db']->getRow($sql);
            $goods_id = $row['goods_id'];
        }

        $sql = 'SELECT COUNT(*) FROM ' . $GLOBALS['ecs']->table('tag') . " WHERE tag_words = '$name'" .
            " AND goods_id = '$goods_id' AND tag_id != '$tag_id'";

        if($GLOBALS['db']->getOne($sql) > 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    /**
    * 获取标签数据列表
    * @access  public
    * @return  array
    */
    function get_tag_list()
    {
        $filter['sort_by']    = empty($_REQUEST['sort_by']) ? 't.tag_id' : trim($_REQUEST['sort_by']);
        $filter['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
        $filter['is_goods']   = $_REQUEST['is_show'] == 1;
        $filter['is_brand']   = $_REQUEST['is_show'] == 2;
        $filter['is_cat']     = $_REQUEST['is_show'] == 3;
        $filter['tag_words']  = empty($_REQUEST['tag_words']) ? '' : trim($_REQUEST['tag_words']);

        $sql = "SELECT COUNT(*) FROM ".$GLOBALS['ecs']->table('tag');
        $filter['record_count'] = $GLOBALS['db']->getOne($sql);

        $filter = page_and_size($filter);

        $sql = "SELECT t.*, u.user_name, g.goods_name, c.cat_name, b.brand_name ".
                "FROM " .$GLOBALS['ecs']->table('tag'). " AS t ".
                "LEFT JOIN " .$GLOBALS['ecs']->table('users'). "     AS u ON u.user_id=t.user_id ".
                "LEFT JOIN " .$GLOBALS['ecs']->table('goods'). "     AS g ON g.goods_id=t.goods_id ".
                "LEFT JOIN " .$GLOBALS['ecs']->table('brand'). "     AS b ON b.brand_id=t.brand_id ".
                "LEFT JOIN " .$GLOBALS['ecs']->table('category'). "  AS c ON c.cat_id=t.cat_id ";

        if($filter['is_goods']){
            $sql .= " WHERE t.goods_id > 0 ";
        } else if($filter['is_brand']){
            $sql .= " WHERE t.brand_id > 0 ";
        } else if ($filter['is_cat']){
            $sql .= " WHERE t.cat_id > 0 ";
        }

        if(!empty($filter['tag_words']))
        {
            if(strpos($sql,"WHERE")) $sql .= " AND t.tag_words = '".$filter['tag_words']."' ";
            else $sql .= " WHERE t.tag_words = '".$filter['tag_words']."' ";
        }

        $sql .= "ORDER by $filter[sort_by] $filter[sort_order] LIMIT ". $filter['start'] .", ". $filter['page_size'];

        $row = $GLOBALS['db']->getAll($sql);
        foreach($row as $k=>$v)
        {
            $row[$k]['tag_words'] = htmlspecialchars($v['tag_words']);
            $row[$k]['cat_name']  = empty($v['cat_name']) ? 'N/A' : $v['cat_name'];
            $row[$k]['brand_name'] = empty($v['brand_name']) ? 'N/A' : $v['brand_name'];
        }

        $arr = array(
            'tags'         => $row,
            'filter'       => $filter,
            'page_count'   => $filter['page_count'],
            'record_count' => $filter['record_count']
            );

        return $arr;
    }

    /**
    * 取得标签的信息
    * return array
    */

    function get_tag_info($tag_id)
    {
        $sql = 'SELECT t.*, g.goods_name FROM ' . $GLOBALS['ecs']->table('tag') . ' AS t' .
            ' LEFT JOIN ' . $GLOBALS['ecs']->table('goods') . ' AS g ON t.goods_id=g.goods_id' .
            " WHERE tag_id = '$tag_id'";
        $row = $GLOBALS['db']->getRow($sql);

        return $row;
    }

    function insert_update_tag($post, $is_insert = false)
    {
        $type       = $post['tag_type'];
        $goods_id   = isset($post['goods_id']) ? $post['goods_id'] : 0;
        $brand_id   = isset($post['brand_id']) ? $post['brand_id'] : 0;
        $cat_id     = isset($post['cat_id']) ? $post['cat_id'] : 0;
        $article_id = isset($post['article_id']) ? $post['article_id'] : 0;
        $tag_words  = isset($post['tag_words']) ? $post['tag_words'] : '';

        /* if null value, return false */
        if(empty($goods_id) && empty($brand_id) && empty($cat_id) && empty($article_id)) return false;

        switch($type){
            case 1: //goods
                $brand_id = 0;
                $cat_id   = 0;
                break;
            case 2: //brands
                $goods_id = 0;
                $cat_id   = 0;
                break;
            case 3: //cat
                $brand_id = 0;
                $goods_id = 0;
                break;
        }
        $data = array(
            'goods_id'   => $goods_id,
            'cat_id'     => $cat_id,
            'brand_id'   => $brand_id,
            'article_id' => $article_id,
            'tag_words'  => $tag_words
        );
        // handle simplified chinese
        $localizable_fields = [
            "tag_words" => $tag_words,
        ];
        if (!$is_insert) {
            $localizable_fields = $this->checkRequireSimplied("tag", $localizable_fields, $post['id']);
        }
        to_simplified_chinese($localizable_fields);
        /* New tag */
        if($is_insert)
        {
            $new_tag = new Model\Tag(null, $data);
            if($tag_id = $new_tag->getId()){
                save_localized_versions('tag', $tag_id);
                return true;
            }
            else return false;
        }
        /* Update tag */
        else
        {
            $tag = new Model\Tag($post['id']);
            if($tag && $tag->update($data)){
                save_localized_versions('tag', $post['id']);
                return true;
            }
            else return false;
        }
    }

    function get_tag_type()
    {
        global $_LANG;

        $type_list = array(
            1 => $_LANG['goods_id'],
            2 => $_LANG['brand'],
            3 => $_LANG['cat']
        );
        return $type_list;
    }

    /* cms goods page batch add tag function */
    function batch_add($goods_id, $tags)
    {
        if(empty($goods_id) || empty($tags)) return false;

        /* Format tag: string -> array */
        $tag_array = array();
        $tag_array = explode(",",$tags);

        $goods_ids = explode(',', $goods_id);

        /* Add tag */
        foreach ($tag_array as $key => $tag) {
            $tag = trim($tag);
            $exist_goods_id = $this->db->getCol("SELECT goods_id FROM " . $this->ecs->table($this->tablename) . " WHERE tag_words = '$tag' AND goods_id " . \db_create_in($goods_id));
            foreach ($goods_ids as $id) {
                if (empty($exist_goods_id) || !in_array($id, $exist_goods_id)) {
                    $sql = "INSERT INTO ".$this->ecs->table($this->tablename)."(goods_id, tag_words) VALUES ($id,'$tag')";
                    $this->db->query($sql);
                    $tag_id = $this->db->insert_id();
                    $simplified = translate_to_simplified_chinese($tag);
                    if ($simplified != $tag) {
                        $this->db->query("INSERT INTO ".$this->ecs->table($this->tablename . "_lang")." (tag_id, lang, tag_words) VALUES ($tag_id, 'zh_cn', '$simplified') ON DUPLICATE KEY UPDATE tag_words = '$simplified', lang = 'zh_cn', tag_id = $tag_id");
                    }
                }
            }
        }

        /* Delete duplicate row */
        $del_sql = "DELETE t1 FROM ".$this->ecs->table($this->tablename)." as t1, ".$this->ecs->table($this->tablename)." as t2 ".
                    " WHERE (t1.goods_id ". \db_create_in($goods_id). " AND t1.tag_id > t2.tag_id AND( t1.goods_id = t2.goods_id AND t1.tag_words = t2.tag_words)) OR t1.tag_words = '' ";
        $this->db->query($del_sql);

        clear_cache_files();
    }

    function get_tags_by_goods($goods_id)
    {
        if(empty($goods_id)) return false;

        $sql = "SELECT cat_id, brand_id FROM ".$this->ecs->table('goods')." WHERE goods_id = $goods_id LIMIT 1";
        $res = $this->db->getRow($sql);

        $cat_id = $res['cat_id'];
        $brand_id = $res['brand_id'];
        require_once(ROOT_PATH . 'includes/lib_goods.php');
        require_once(ROOT_PATH . 'includes/lib_howang.php');
        $cat = get_ly_newtree($cat_id);

        $cat_ids   = array();
        $cat_ids[] = $cat_id;
        foreach ($cat as $key => $value) {
            $cat_ids[] = $value['id'];
        }

        $sql = "SELECT DISTINCT IFNULL(tl.tag_words, t.tag_words) as tag_words FROM ".$this->ecs->table($this->tablename)." as t ".
                " LEFT JOIN ".$this->ecs->table($this->tablename . '_lang') ." as tl ON tl.tag_id = t.tag_id AND lang = '" . $GLOBALS['_CFG']['lang'] . "'".
                " LEFT JOIN ".$this->ecs->table('goods') ." as g ON g.goods_id = t.goods_id".
                " WHERE t.goods_id = ".$goods_id;
                if($brand_id >0)$sql .= " OR t.brand_id = $brand_id";
                if(count($cat_ids)>0)$sql.=" OR t.cat_id".\db_create_in($cat_ids);

        $res = $this->db->getCol($sql);

        $tags = array();
        foreach ($res as $key => $tag) {
            $tags[$key]['tag_url'] = "/keyword/".urlencode($tag);
            $tags[$key]['tag'] = preg_replace('/\s/', '_', $tag);
        }

        return $tags;
    }

    function find_tag_words($tag_words)
    {
        $sql = "SELECT COUNT(*) FROM ".$this->ecs->table($this->tablename)." WHERE tag_words = '".$tag_words."' ";
        $count = $this->db->getOne($sql);

        return $count;
    }

    function get_goods_id_by_tag($tag_words)
    {
        if(empty($tag_words)) return false;

        $sql = "SELECT DISTINCT goods_id, brand_id, cat_id  FROM ".$this->ecs->table($this->tablename).
                "WHERE tag_words LIKE '%".$tag_words."%'";
        $arr = $this->db->getAll($sql);
        $sql_lang = "SELECT DISTINCT goods_id, brand_id, cat_id  FROM ".$this->ecs->table($this->tablename). " g " .
                "LEFT JOIN " . $this->ecs->table($this->tablename . '_lang'). " gl ON gl.tag_id = g.tag_id " .
                "WHERE gl.tag_words LIKE '%".$tag_words."%'";
        $arr_lang = $this->db->getAll($sql_lang);
        if(empty($arr) && empty($arr_lang)) return [];
        $goods_ids = array();
        $brand_ids = array();
        $cat_ids   = array();
        $all_cats  = array();
        foreach ($arr as $value)
        {
            if(!empty($value['goods_id'])) $goods_ids[] = $value['goods_id'];
            if(!empty($value['brand_id'])) $brand_ids[] = $value['brand_id'];
            if(!empty($value['cat_id']))   $cat_ids[]   = $value['cat_id'];
        }
        foreach ($arr_lang as $value)
        {
            if(!empty($value['goods_id'])) $goods_ids[] = $value['goods_id'];
            if(!empty($value['brand_id'])) $brand_ids[] = $value['brand_id'];
            if(!empty($value['cat_id']))   $cat_ids[]   = $value['cat_id'];
        }
        $where = [];
        foreach ($cat_ids as $c_id) $all_cats += array_unique(array_merge(array($c_id), array_keys(cat_list($c_id, 0, false))));
        if(!empty($goods_ids)) $where[] = " goods_id ".db_create_in($goods_ids);
        if(!empty($brand_ids)) $where[] = " brand_id ".db_create_in($brand_ids);
        if(!empty($all_cats)) $where[] = " cat_id ".db_create_in($all_cats);
        $where_sql = implode('OR', $where);
        if(empty($where_sql)) return [];
        $sql = "SELECT goods_id FROM ".$this->ecs->table('goods').
                " WHERE ( $where_sql ) AND is_delete = 0 AND is_on_sale = 1 AND is_alone_sale = 1 ";
        $goods_ids = $this->db->getCol($sql);

        return array_unique($goods_ids);

    }

    function tag_related_cat($goods_id, $tag_words)
    {
        $sql = "SELECT g.cat_id, IFNULL(cl.cat_name, c.cat_name) as child_cat_name,p.parent_id as p_id, IFNULL(pcl.cat_name, p.cat_name) as parent_cat_name, COUNT(g.goods_id) AS goods_count " .
                "FROM " . $GLOBALS['ecs']->table('goods') . " as g " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('category') . " as c ON c.cat_id = g.cat_id " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('category') . " as p ON c.parent_id  = p.cat_id AND p.is_show = 1 " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('category_lang') . " as cl ON cl.cat_id = c.cat_id AND cl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('category_lang') . " as pcl ON pcl.cat_id = p.cat_id AND pcl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
                "WHERE g.goods_id ".db_create_in($goods_id)." AND g.is_on_sale = 1 AND g.is_alone_sale = 1 AND g.is_delete = 0 " .
                "GROUP BY g.cat_id";
        $res = $GLOBALS['db']->getAll($sql);

        $array = array();
        foreach ($res as $key => $row) {
            $array[$row['parent_cat_name']][$key] = $row;
            $array[$row['parent_cat_name']][$key]['url'] = build_uri('tags', array(
                'cid' => $row['cat_id'], 'cname' => $row['child_cat_name'],
                'tag_words' => $tag_words), $row['child_cat_name']);
            $array[$row['parent_cat_name']][$key]['child_cat_name'] = $row['child_cat_name'];
            $array[$row['parent_cat_name']][$key]['parent_cat_name'] = $row['parent_cat_name'];
            $array[$row['parent_cat_name']]['id'] = $row['p_id'];
        }
        $result = array();
        $result[0] = $array;
        $result[1] = count($res);
        return $result;
    }

    function get_tag_words_list()
    {
        $sql = "SELECT DISTINCT tag_words FROM ".$this->ecs->table($this->tablename);
        $res = $this->db->getCol($sql);

        $list = array();
        foreach ($res as $value)
        {
            $list[$value] = $value;
        }
        return $list;
    }

    function update_tag_words($post)
    {
        $old_words = $post['tag_words'];
        $new_words = $post['new_tag_words'];

        if(empty($old_words) || empty($new_words)) return false;

        $tag_ids = $this->db->getCol("SELECT tag_id FROM " . $this->ecs->table($this->tablename) . " WHERE tag_words ".db_create_in($old_words));

        $simplified = translate_to_simplified_chinese($new_words);
        if ($simplified != $new_words) {
            foreach ($tag_ids as $tag_id) {
                $this->db->query("INSERT INTO ".$this->ecs->table($this->tablename . "_lang")." (tag_id, lang, tag_words) VALUES ($tag_id, 'zh_cn', '$simplified') ON DUPLICATE KEY UPDATE tag_words = '$simplified', lang = 'zh_cn', tag_id = $tag_id");
            }
        }

        $sql = "UPDATE ".$this->ecs->table($this->tablename)." SET tag_words = '".$new_words."' ".
        "WHERE tag_id ".db_create_in($tag_ids);

        return $this->db->query($sql);
    }

    function getTagI18nByKeyword($keyword) {
        global $ecs, $db, $_CFG;

        $sql = " select tl.*, t.tag_words as default_tag_words from ".$ecs->table('tag_lang')." tl left join ".$ecs->table('tag')." t on (tl.tag_id = t.tag_id) where tl.tag_id = (select tag_id from ".$ecs->table('tag_lang')." where tag_words = '".mysql_escape_string($keyword)."' limit 1) ";
        $result = $db->getAll($sql);

        $i18n = [];
        foreach ($result as $item) {
            $i18n[$item['lang']] = $item['tag_words']; 
        }

        // for zh_tw
        $sql = " select t.tag_words from ".$ecs->table('tag')." t where t.tag_words = '".mysql_escape_string($keyword)."' limit 1 ";
        $result = $db->getOne($sql);
        $i18n['zh_tw'] = $result;

        return $i18n;
    }

}
