<?php

/***
* rmaController.php
*
*
*
***/
namespace Yoho\cms\Controller;

use Yoho\cms\Model;

use \Exception;

use Yoho\cms\Controller\ErpController;

if (!defined('IN_ECS')) {
    die('Hacking attempt');
}
class OrderWholesaleDeliveryController extends YohoBaseController
{
    
    private $tablename='';
    
    public function __construct()
    {
        parent::__construct();
       
        //$this->adminuserController = new AdminuserController();
        //$this->erpController =  new ErpController();
        //$this->warehouseTransferController =  new WarehouseTransferController();
        //$this->orderController = new OrderController();
    }
   
    public function getOrderWholesaleDeliveryList($is_pagination=true)
    {
        global $db, $ecs, $userController ,$_CFG;

        $deliveryOrderController = new DeliveryOrderController();

        $_REQUEST['status'] = (!isset($_REQUEST['status']) || isset($_REQUEST['status']) && $_REQUEST['status'] == '-1') ? '-1' : $_REQUEST['status'];
        

        $sql = "select count(*) from ".$ecs->table('delivery_order')." do 
                left join ".$ecs->table('order_info')." oi on ( do.order_id = oi.order_id ) 
                where oi.order_type = 'ws' and status != ".DO_NORMAL."  ";

        $sql .= (!isset($_REQUEST['status']) || isset($_REQUEST['status']) && $_REQUEST['status'] == '-1') ? '' : " and status = '".$_REQUEST['status']."'";        
     
        if (isset($_REQUEST['order_sn'])  && $_REQUEST['order_sn'] != '') {
            $sql.=" and oi.order_sn='".$_REQUEST['order_sn']."'";
        }

        $total_count = $db->getOne($sql);

        $sql = "select * from ".$ecs->table('delivery_order')." do 
                left join ".$ecs->table('order_info')." oi on ( do.order_id = oi.order_id ) 
                where oi.order_type = 'ws' and status != ".DO_NORMAL." ";

        $sql .= (!isset($_REQUEST['status']) || isset($_REQUEST['status']) && $_REQUEST['status'] == '-1') ? '' : " and status = '".$_REQUEST['status']."'";        
        
        if (isset($_REQUEST['order_sn'])  && $_REQUEST['order_sn'] != '') {
            $sql.=" and oi.order_sn like '%".$_REQUEST['order_sn']."'";
        }

        if (isset($_REQUEST['goods_sn'])  && $_REQUEST['goods_sn'] != '') {
            $sql.=" and g.goods_sn='".$_REQUEST['goods_sn']."'";
        }

        $sql .= ' order by do.delivery_id desc';

        $sql .= ($is_pagination ? " LIMIT " . $_REQUEST['start'] . ", " . $_REQUEST['page_size'] : '');
        $res = $db->getAll($sql);

        // echo '<pre>';
        // print_r($res);
        // echo $sql;exit;

        foreach ($res as &$order_goods_transfer) {
            if (!empty($order_goods_transfer['ws_company_id']) && !empty($order_goods_transfer['ws_customer_id'])) {
                $action_html = '<input class="btn btn-success form-btn dn_download" type="button"  data-delivery-id="'.$order_goods_transfer['delivery_id'].'"  name="transfer_search" value="下載DN"> | ';

                if ($order_goods_transfer['status'] == DO_PACKED) { // 4 己pack 貨
                    $action_html .= '<div class="pickup_label_group"><div class="order-status-label light-green picked_up_label" title="已Pack貨">已Pack貨</div><span class="badge picked_up_label_undo" data-delivery-id="'.$order_goods_transfer['delivery_id'].'" data-delivery-status="0" title="取消已Pack貨"><i class="fas fa-times"></i></span></div>';
                    //$action_html .= '<input class="btn btn-success form-btn dn_packed" type="button"  data-delivery-id="'.$order_goods_transfer['delivery_id'].'"  data-delivery-status="'.DO_PACKED.'"  name="transfer_search" value="已Pack貨">';
                    $action_html .= '<input class="btn btn-dark form-btn dn_collected" type="button"  data-delivery-id="'.$order_goods_transfer['delivery_id'].'"   data-delivery-status="'.DO_RECEIVED.'" name="transfer_search" value="取貨確認">';
                } else if ($order_goods_transfer['status'] == DO_RECEIVED) { // 3 己收貨
                    $action_html .= '<div class="pickup_label_group"><div class="order-status-label light-green picked_up_label" title="已Pack貨">已Pack貨</div></div>';
                    $action_html .= ' <div class="pickup_label_group"><div class="order-status-label light-green picked_up_label" title="取貨確認">取貨確認</div><span class="badge picked_up_label_undo" data-delivery-id="'.$order_goods_transfer['delivery_id'].'" data-delivery-status="4" title="取消取貨確認"><i class="fas fa-times"></i></span></div>';                
                } else if ($order_goods_transfer['status'] == DO_DELIVERED) { // 0 已發貨
                    $action_html .= '<input class="btn btn-dark form-btn dn_packed" type="button"  data-delivery-id="'.$order_goods_transfer['delivery_id'].'"  data-delivery-status="'.DO_PACKED.'"  name="transfer_search" value="已Pack貨">';
                    $action_html .= '<input class="btn btn-dark form-btn dn_collected" type="button"  data-delivery-id="'.$order_goods_transfer['delivery_id'].'"   data-delivery-status="'.DO_RECEIVED.'" name="transfer_search" value="取貨確認">';
                }
            } else {
                $action_html = '<span class="red">請先設定DN資訊</span>';
            }

            //$action_html .= '<input class="btn btn-dark form-btn dn_packed" type="button"  data-delivery-id="'.$order_goods_transfer['delivery_id'].'"  data-delivery-status="'.DO_PACKED.'"  name="transfer_search" value="已Pack貨">';
            //$action_html .= '<input class="btn btn-dark form-btn dn_collected" type="button"  data-delivery-id="'.$order_goods_transfer['delivery_id'].'"   data-delivery-status="'.DO_RECEIVED.'" name="transfer_search" value="取貨確認">';    
            $order_goods_transfer['status_html'] =  $deliveryOrderController->getDeliveryStatusName($order_goods_transfer['status']);
            $order_goods_transfer['order_date_html'] = local_date($_CFG['time_format'], $order_goods_transfer['update_time']);
            $order_goods_transfer['follow_up_remark_1'] = '<span class="follow_up_remark" data-delivery-id="'.$order_goods_transfer['delivery_id'].'" data-toggle="modal" data-target="#popup_follow_up_remark_modal" >'.(empty($order_goods_transfer['follow_up_remark'])? '請輸入' : $order_goods_transfer['follow_up_remark']).'</span>';
            $order_goods_transfer['action_html'] = $action_html; 
        }

        $arr = array(
           'data' => $res,
           'record_count' => $total_count
        );

        return $arr;
    }

    public function orderWholesaleDeliveryFollowUpRemarkSubmit()
    {
        global $db, $ecs, $userController, $_CFG;

        $delivery_id = $_REQUEST['delivery_id'];
        
        if (!empty($delivery_id)) {
            $follow_up_remark = mysql_real_escape_string($_REQUEST['follow_up_remark']);
            $sql = 'update '.$ecs->table('delivery_order').' set follow_up_remark = "'.$follow_up_remark.'" where delivery_id = "'.$delivery_id.'" ';
            $db->query($sql);
            return true;
        }
    }
    
}
