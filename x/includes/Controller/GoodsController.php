<?php

/***
* by Anthony 20180410
*
* Goods controller
***/

namespace Yoho\cms\Controller;

if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}
include_once(ROOT_PATH . '/includes/cls_image.php');
class GoodsController extends YohoBaseController {

    public function __construct(){

        parent::__construct();
        global $db,$ecs,$_LANG;
        // $exc = new \exchange($ecs->table('goods'), $db, 'goods_id', 'goods_name');
        // $this->exc = $exc;
        // $this->tableName = 'goods';
        $this->admin_priv = 'goods_manage';
    }

    public function listAction()//$_REQUST['act'] == 'list'
	{
        require_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_goods.php');
        admin_priv('goods_manage');
        global $ecs, $db, $_LANG;

        // As requested by Franz, we default to show 上架 goods only
        if (($_REQUEST['act'] == 'list') && (!isset($_REQUEST['is_on_sale']))) {
            // Empty string = 全部, string '1' = 上架, string '0' = 下架
            $_REQUEST['is_on_sale'] = '1';
        }

        $cat_id = empty($_REQUEST['cat_id']) ? 0 : intval($_REQUEST['cat_id']);
        $code   = empty($_REQUEST['extension_code']) ? '' : trim($_REQUEST['extension_code']);
        $suppliers_id = isset($_REQUEST['suppliers_id']) ? (empty($_REQUEST['suppliers_id']) ? '' : trim($_REQUEST['suppliers_id'])) : '';
        $is_on_sale = isset($_REQUEST['is_on_sale']) ? ((empty($_REQUEST['is_on_sale']) && $_REQUEST['is_on_sale'] === 0) ? '' : trim($_REQUEST['is_on_sale'])) : '';
        $warranty_template_id = isset($_REQUEST['warranty_template_id']) ? intval($_REQUEST['warranty_template_id']) : -1;
        $shipping_template_id = isset($_REQUEST['shipping_template_id']) ? intval($_REQUEST['shipping_template_id']) : -1;
        $keyword = empty($_REQUEST['keyword']) ? '' : $_REQUEST['keyword'];
        /* Unused
        $handler_list = array();
        $handler_list['virtual_card'][] = array('url'=>'virtual_card.php?act=card', 'title'=>$_LANG['card'], 'img'=>'icon_send_bonus.gif');
        $handler_list['virtual_card'][] = array('url'=>'virtual_card.php?act=replenish', 'title'=>$_LANG['replenish'], 'img'=>'icon_add.gif');
        $handler_list['virtual_card'][] = array('url'=>'virtual_card.php?act=batch_card_add', 'title'=>$_LANG['batch_card_add'], 'img'=>'icon_output.gif');

        if ($_REQUEST['act'] == 'list' && isset($handler_list[$code]))
        {
            $smarty->assign('add_handler',      $handler_list[$code]);
        }
        */

        /* 供货商名 */
        //$suppliers_list_name = suppliers_list_name();
        $suppliers_list_name = suppliers_list_name_by_admin();

        /* 保養條款 */
        $warranty_templates = $db->getAll("SELECT template_id, template_name FROM " . $ecs->table('warranty_templates') . " ORDER BY template_id ASC");
        $shipping_templates = $db->getAll("SELECT template_id, template_name FROM " . $ecs->table('shipping_templates') . " ORDER BY template_id ASC");

        /* 模板赋值 */
        $goods_ur = array('' => $_LANG['01_goods_list'], 'virtual_card'=>$_LANG['50_virtual_card_list']);
        $ur_here = ($_REQUEST['act'] == 'list') ? $goods_ur[$code] : $_LANG['11_goods_trash'];

        $action_link = ($_REQUEST['act'] == 'list') ? $this->add_link($code) : array('href' => 'goods.php?act=list', 'text' => $_LANG['01_goods_list']);

        $cat_list = cat_list(0, $cat_id, false, 0, true, true); //cat_list(0, $cat_id);
        $brand_list = get_brand_list();
        $intro_list = get_intro_list();

        $list_type          = $_REQUEST['act'] == 'list' ? 'goods' : 'trash';
        $use_storage        = empty($_CFG['use_storage']) ? 0 : 1;
        $show_pricecomhk    = empty($_REQUEST['show_pricecomhk']) ? 0 : 1;
        $show_slowsoldgoods = empty($_REQUEST['show_slowsoldgoods']) ? 0 : 1;

        if(!$_SESSION['manage_cost'] && check_authz(AdminuserController::DB_ACTION_CODE_ONLYSELFSUPPLIERS)){
            $self_supplier = 1;
        }

        $assign = get_defined_vars();
		
		$erpController = new erpController();
		$warehouseId = $erpController->getFlsWarehouseId();
		if (!empty($warehouseId)) {
			$warehouseFLS = $erpController->getWarehouseById($warehouseId);
		}
		$goods_warehouse_list[] = $warehouseFLS;  
		$assign['goods_warehouse_list'] = $goods_warehouse_list;
        $assign['suppliers_list_name'] = $suppliers_list_name;
        $assign['warranty_templates'] = $warranty_templates;

        $assign['session_action_list'] = explode(",", $_SESSION['action_list']);
        $assign['session_manage_cost'] = $_SESSION['manage_cost'];

        $template = ($_REQUEST['act'] == 'list') ?
            'goods_list.htm' : (($_REQUEST['act'] == 'trash') ? 'goods_trash.htm' : 'group_list.htm');

        parent::listAction($assign, $template);
    }

    public function ajaxQueryAction()
	{
        require_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_goods.php');
        $is_delete = 0;
        if($_REQUEST['list_type'] == 'trash') {
            $is_delete = 1;
        }
        if(!isset($_REQUEST['is_on_sale'])) {
            $_REQUEST['is_on_sale'] = 1;
        }
        $gd = goods_list($is_delete); //TODO: update into Controller later.

		// We're using dataTables Ajax to query.
		$this->tableList       = $gd['goods'];
		$this->tableTotalCount = $gd['record_count'];

        parent::ajaxQueryAction(null, null, false);
    }

    /**
     * 添加链接
     * @param   string  $extension_code 虚拟商品扩展代码，实体商品为空
     * @return  array('href' => $href, 'text' => $text)
     */
    function add_link($extension_code = '')
    {
        $href = 'goods.php?act=add';
        if (!empty($extension_code))
        {
            $href .= '&extension_code=' . $extension_code;
        }

        if ($extension_code == 'virtual_card')
        {
            $text = $GLOBALS['_LANG']['51_virtual_card_add'];
        }
        else
        {
            $text = $GLOBALS['_LANG']['02_goods_add'];
        }

        return array('href' => $href, 'text' => $text);
    }

    public function get_goods_cost_log()
    {

        require_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_goods.php');
        $_REQUEST['keyword']      = empty($_REQUEST['keyword']) ? '' : trim($_REQUEST['keyword']);
        if (isset($_REQUEST['is_ajax']) && $_REQUEST['is_ajax'] == 1)
        {
            $_REQUEST['keyword'] = json_str_iconv($_REQUEST['keyword']);
        }

        $_REQUEST['sort_by']      = empty($_REQUEST['sort_by']) ? 'log_id' : trim($_REQUEST['sort_by']);
        $_REQUEST['sort_order']   = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
        if($_REQUEST['sort_by'] == 'update_date') $_REQUEST['sort_by'] = 'update_time';

        $where = " WHERE 1 ";

        $orderby = $_REQUEST['sort_by'] . ' ' . $_REQUEST['sort_order'] . ' ';

        if (!empty($_REQUEST['keyword']))
        {
            $where .= " AND (g.`goods_id` = '" . mysql_like_quote($_REQUEST['keyword']) . "' ";
            $where .= " OR g.`goods_name` LIKE '%" . mysql_like_quote($_REQUEST['keyword']) . "%' ";
            $tmp = preg_split('/\s+/', $_REQUEST['keyword']);
            if (count($tmp) > 1)
            {
                $where .= " OR ( 1";
                foreach ($tmp as $kw)
                {
                    $where .= " AND g.`goods_name` LIKE '%" . mysql_like_quote($kw) . "%' ";
                }
                $where .= ") ";
            }
            $where .= " OR g.`goods_sn` LIKE '%" . mysql_like_quote($_REQUEST['keyword']) . "%') ";

            // howang: Prioritize exact matches
            $orderby = "CASE " .
            "WHEN g.`goods_id` = '" . $_REQUEST['keyword'] . "' THEN 0 " .
            "WHEN g.`goods_sn` = '" . $_REQUEST['keyword'] . "' THEN 1 " .
            "WHEN g.`goods_name` = '" . $_REQUEST['keyword'] . "' THEN 2 " .
            "ELSE 3 " .
            "END ASC, " . $orderby;
        }

        $sql = "SELECT count(*) ";
        $sql.= " FROM " . $GLOBALS['ecs']->table('goods_cost_log') . " as gcl ";
        $sql.= " LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON gcl.`goods_id` = g.`goods_id`";
        $sql.= $where;
        $record_count = $GLOBALS['db']->getOne($sql);


        /* 查询 */
        $sql = "SELECT gcl.*, au.`user_name` as admin_name, g.`goods_name`, g.`goods_thumb`, g.`goods_sn` ";
        $sql.= " FROM " . $GLOBALS['ecs']->table('goods_cost_log') . " as gcl ";
        $sql.= " LEFT JOIN " . $GLOBALS['ecs']->table('admin_user') . " as au ON gcl.admin_id = au.user_id ";
        $sql.= " LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON gcl.`goods_id` = g.`goods_id`";
        $sql.= $where;
        $sql.= " ORDER BY " . $orderby;
        $sql.= " LIMIT " . $_REQUEST['start'] . ",$_REQUEST[page_size]";

        $data = $GLOBALS['db']->getAll($sql);

        foreach ($data as $key => $row)
        {
            if ($row['update_type'] == 1)
            {
                $data[$key]['update_type_text'] = '入貨自動重新計算';
            }
            elseif ($row['update_type'] == 2)
            {
                $data[$key]['update_type_text'] = '手動更改';
            }
            else
            {
                $data[$key]['update_type_text'] = '不明';
            }
            $data[$key]['update_date'] = local_date($GLOBALS['_CFG']['time_format'], $row['update_time']);
        }

        $arr = array('data' => $data, 'record_count' => $record_count);

        return $arr;
    }

    public function goodsCostLogAjaxQueryAction()
    {
        /* 检查权限 */
        check_authz_json('goods_manage');

        if (!$_SESSION['manage_cost'])
        {
            make_json_error('Admin only!');
        }

        $list = $this->get_goods_cost_log();
        parent::ajaxQueryAction($list['data'], $list['record_count']);
    }

    function get_goods_price_log()
    {

        $_REQUEST['keyword']      = empty($_REQUEST['keyword']) ? '' : trim($_REQUEST['keyword']);
        if (isset($_REQUEST['is_ajax']) && $_REQUEST['is_ajax'] == 1)
        {
            $_REQUEST['keyword'] = json_str_iconv($_REQUEST['keyword']);
        }

        $_REQUEST['sort_by']      = empty($_REQUEST['sort_by']) ? 'log_id' : trim($_REQUEST['sort_by']);
        $_REQUEST['sort_order']   = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
        if($_REQUEST['sort_by'] == 'update_date') $_REQUEST['sort_by'] = 'update_time';
        $where = " WHERE 1 ";

        $orderby = $_REQUEST['sort_by'] . ' ' . $_REQUEST['sort_order'] . ' ';

        if (!empty($_REQUEST['keyword']))
        {
            $where .= " AND (g.`goods_id` = '" . mysql_like_quote($_REQUEST['keyword']) . "' ";
            $where .= " OR g.`goods_name` LIKE '%" . mysql_like_quote($_REQUEST['keyword']) . "%' ";
            $tmp = preg_split('/\s+/', $_REQUEST['keyword']);
            if (count($tmp) > 1)
            {
                $where .= " OR ( 1";
                foreach ($tmp as $kw)
                {
                    $where .= " AND g.`goods_name` LIKE '%" . mysql_like_quote($kw) . "%' ";
                }
                $where .= ") ";
            }
            $where .= " OR g.`goods_sn` LIKE '%" . mysql_like_quote($_REQUEST['keyword']) . "%') ";

            // howang: Prioritize exact matches
            $orderby = "CASE " .
            "WHEN g.`goods_id` = '" . $_REQUEST['keyword'] . "' THEN 0 " .
            "WHEN g.`goods_sn` = '" . $_REQUEST['keyword'] . "' THEN 1 " .
            "WHEN g.`goods_name` = '" . $_REQUEST['keyword'] . "' THEN 2 " .
            "ELSE 3 " .
            "END ASC, " . $orderby;
        }

        $sql = "SELECT count(*) ";
        $sql.= " FROM " . $GLOBALS['ecs']->table('goods_price_log') . " as gcl ";
        $sql.= " LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON gcl.`goods_id` = g.`goods_id`";
        $sql.= $where;
        $record_count = $GLOBALS['db']->getOne($sql);

        /* 查询 */
        $sql = "SELECT gcl.*, au.`user_name` as admin_name, g.`goods_name`, g.`goods_thumb`, g.`goods_sn` ";
        $sql.= " FROM " . $GLOBALS['ecs']->table('goods_price_log') . " as gcl ";
        $sql.= " LEFT JOIN " . $GLOBALS['ecs']->table('admin_user') . " as au ON gcl.admin_id = au.user_id ";
        $sql.= " LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON gcl.`goods_id` = g.`goods_id`";
        $sql.= $where;
        $sql.= " ORDER BY " . $orderby;
        $sql.= " LIMIT " . $_REQUEST['start'] . ",$_REQUEST[page_size]";

        $data = $GLOBALS['db']->getAll($sql);

        foreach ($data as $key => $row)
        {
            $data[$key]['old_price_formated'] = price_format($row['old_price']);
            $data[$key]['new_price_formated'] = price_format($row['new_price']);
            $data[$key]['update_date'] = local_date($GLOBALS['_CFG']['time_format'], $row['update_time']);
            if ($row['admin_id'] == 0) {
                $data[$key]['admin_name'] = '自動促銷';
            }
        }

        $arr = array('data' => $data, 'record_count' => $record_count);

        return $arr;
    }

    public function goodsPriceLogAjaxQueryAction()
    {
        /* 检查权限 */
        check_authz_json('goods_manage');

        $list = $this->get_goods_price_log();
        parent::ajaxQueryAction($list['data'], $list['record_count']);
    }

    /**
     * 获取评论列表
     * @access  public
     * @return  array
     */
    function get_goods_comment_list()
    {
        $erpController = new ErpController();
        /* 查询条件 */
        $_REQUEST['mode']         = empty($_REQUEST['mode']) ? 'normal' : trim($_REQUEST['mode']);
        $_REQUEST['sort_by']      = empty($_REQUEST['sort_by']) ? 'add_time' : trim($_REQUEST['sort_by']);
        $_REQUEST['sort_order']   = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);

        if ($_REQUEST['mode'] == 'normal')
        {
            $where = " WHERE gb.is_deleted = '0' ";
        }
        elseif ($_REQUEST['mode'] == 'deleted')
        {
            $where = " WHERE gb.is_deleted = '1' ";
        }

        $sql = "SELECT count(*) FROM " . $GLOBALS['ecs']->table('goods_book') . " as gb $where";
        $record_count = $GLOBALS['db']->getOne($sql);

        /* 获取评论数据 */
        $sql  = "SELECT g.`goods_name`, gb.*, ".
                "(SELECT SUM(ega.goods_qty) FROM " . $GLOBALS['ecs']->table('erp_goods_attr_stock') . " AS ega WHERE g.goods_id = ega.goods_id AND ega.warehouse_id IN (".$erpController->getSellableWarehouseIds(true).")) as goods_number " .
                " FROM " . $GLOBALS['ecs']->table('goods_book') . " as gb " .
                " LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON g.goods_id = gb.goods_id " .
                $where .
                " ORDER BY " . $_REQUEST['sort_by'] . " " . $_REQUEST['sort_order'] .
                " LIMIT " . $_REQUEST['start'] . "," . $_REQUEST['page_size'];
        $res  = $GLOBALS['db']->getAll($sql);

        $arr = array();
        foreach ($res as $row)
        {
            $row['pre_im'] = $this->get_im_name($row['pre_im']);
            $row['add_time'] = local_date($GLOBALS['_CFG']['time_format'], $row['add_time']);
            $row['operator'] = empty($row['operator']) ? "N/A" : $row['operator'];
            $row['content'] = empty($row['content']) ? "N/A" : $row['content'];
            $row['content2'] = empty($row['content2']) ? "N/A" : $row['content2'];
            $row['content3'] = empty($row['content3']) ? "N/A" : $row['content3'];
            $row['content3'] = empty($row['content3']) ? "N/A" : $row['content3'];
            if($row['is_deleted']) {
                $row['_action'] = '<a href="goods_book.php?act=recover&id='.$row['id'].'">還原</a>';
            } else {
                $row['_action'] = '<a href="javascript:void(0)" data-url="goods_book.php?act=remove&id='.$row['id'].'" onClick="cellRemove(this);">封存</a>';
            }
            $arr[] = $row;
        }

        $arr = array('data' => $arr, 'record_count' => $record_count);

        return $arr;
    }
    public function goodsBookAjaxQueryAction()
    {
        $list = $this->get_goods_comment_list();
        parent::ajaxQueryAction($list['data'], $list['record_count'], false);
    }

    /**
     * 获取评论列表
     * @access  public
     * @return  array
     */
    function get_goods_dai_list()
    {
        $erpController = new  ErpController();
        /* 查询条件 */
        $_REQUEST['mode']         = empty($_REQUEST['mode']) ? 'normal' : trim($_REQUEST['mode']);
        $_REQUEST['sort_by']      = empty($_REQUEST['sort_by']) ? 'add_time' : trim($_REQUEST['sort_by']);
        $_REQUEST['sort_order']   = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);

        if ($_REQUEST['mode'] == 'normal')
        {
            $where = " WHERE gb.is_deleted = '0' ";
        }
        elseif ($_REQUEST['mode'] == 'deleted')
        {
            $where = " WHERE gb.is_deleted = '1' ";
        }

        $sql = "SELECT count(*) FROM " .$GLOBALS['ecs']->table('goods_dai'). " as gb $where";
        $record_count = $GLOBALS['db']->getOne($sql);

        /* 获取评论数据 */
        $sql  = "SELECT g.`goods_name`, gb.*, ".
                "(SELECT SUM(ega.goods_qty) FROM " . $GLOBALS['ecs']->table('erp_goods_attr_stock') . " AS ega WHERE g.goods_id = ega.goods_id AND ega.warehouse_id IN (".$erpController->getSellableWarehouseIds(true).")) as goods_number " .
                " FROM " . $GLOBALS['ecs']->table('goods_dai') . " as gb " .
                " LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON g.goods_id = gb.goods_id " .
                $where .
                " ORDER BY " . $_REQUEST['sort_by'] . " " . $_REQUEST['sort_order'] .
                " LIMIT " . $_REQUEST['start'] . "," . $_REQUEST['page_size'];
        $res  = $GLOBALS['db']->getAll($sql);

        $arr = array();
        foreach ($res as $row)
        {
            $row['pre_im_name'] = $this->get_im_name($row['pre_im']);
            $row['add_time'] = local_date($GLOBALS['_CFG']['time_format'], $row['add_time']);
            $row['operator'] = empty($row['operator']) ? "N/A" : $row['operator'];
            $row['content'] = empty($row['content']) ? "N/A" : $row['content'];
            $row['operator2'] = empty($row['operator2']) ? "N/A" : $row['operator2'];
            $row['content2'] = empty($row['content2']) ? "N/A" : $row['content2'];
            $row['content3'] = empty($row['content3']) ? "N/A" : $row['content3'];

            if($row['is_deleted']) {
                $row['_action'] = '<a href="goods_dai.php?act=recover&id='.$row['id'].'">還原</a>';
            } else {
                $row['_action'] = '<a href="javascript:void(0)" data-url="goods_dai.php?act=remove&id='.$row['id'].'" onClick="cellRemove(this);">封存</a>';
            }

            $arr[] = $row;
        }

        $arr = array('data' => $arr, 'record_count' => $record_count);

        return $arr;
    }
    public function goodsDaiAjaxQueryAction()
    {
        $list = $this->get_goods_dai_list();
        parent::ajaxQueryAction($list['data'], $list['record_count'], false);
    }
    // howang: get contact method name
    function get_im_name($im)
    {
        $im = strtolower($im);
        if ($im == 'tel')
        {
            return "電話";
        }
        else if ($im == 'email')
        {
            return "Email";
        }
        else if ($im == 'whatsapp')
        {
            return "WhatsApp";
        }
        else if ($im == 'weisms')
        {
            return "微訊";
        }
    }

    function getGoodsPreOrderInfo($goods, $stock = 0, $show = 1, $qty = 1)
    {
        global $ecs, $db, $_CFG, $_LANG;
        require(ROOT_PATH . 'languages/' . $_CFG['lang'] . '/yoho.php');
        $time = gmtime();
        $today = $this->get_today_info();

        $goods['old_pre_sale_date'] = $goods['pre_sale_date'];
        $goods['old_pre_sale_days'] = $goods['pre_sale_days'];
        $erpController = new ErpController();
        //$flsQty = $erpController->getSellableProductQtyOfFls($goods['goods_id']); // Get FLS stock
		$flsQty = $goods['goods_number_fls'];
        $warehouseQty = $goods['goods_number_main_warehouse'];
        $shopTotalQty = 0;
        foreach ($goods['goods_number_shop'] as $no){
           $shopTotalQty += $no;
        }
        $length = empty($goods['goods_length']) ? floatval($goods['default_length']) : floatval($goods['goods_length']);
        $width = empty($goods['goods_width']) ? floatval($goods['default_width']) : floatval($goods['goods_width']);
        $height = empty($goods['goods_height']) ? floatval($goods['default_height']) : floatval($goods['goods_height']);
        $lwh_sum = $length + $width + $height;
        // $goods['pre_sale_time'] = empty($goods['pre_sale_time']) ? 16: $goods['pre_sale_time'];

        $order_day = $goods['pre_sale_days'];
        $transfer_day = 0;
        $shop_transfer_day = 0;

        // 沒有存貨時，自動判定訂貨模式
        if ($stock < $qty)
        {
            if ($goods['is_pre_sale']) // 預售
            {
                // Nothing
                $goods['availability'] = 'PreOrder';
                $shop_transfer_day++;
            }
            elseif ($goods['is_purchasing'] && $goods['pre_sale_days'] > 0) // 外國代購
            {
                $goods['pre_sale_date'] = $this->get_yoho_next_work_day(gmtime(), $goods['pre_sale_days']);
                $goods['availability'] = 'PreOrder';
                $shop_transfer_day++;
            }
            elseif ($goods['pre_sale_date'] > $time) // 即將到貨
            {
                $goods['pre_sale_days'] = $this->count_yoho_work_day($goods['pre_sale_date']);
                $order_day = $goods['pre_sale_days'];
                $goods['availability'] = 'PreOrder';
                $shop_transfer_day++;
            }
            elseif ($goods['pre_sale_days'] > 0) // 訂貨
            {
                $goods['pre_sale_date'] = $this->get_yoho_next_work_day(gmtime(), $goods['pre_sale_days']);
                $goods['availability'] = 'PreOrder';
                $shop_transfer_day++;
            }
            else // 缺貨
            {
                $goods['pre_sale_days'] = 0;
                $goods['pre_sale_date'] = 0;
                $goods['availability'] = 'OutOfStock';
            }
            $goods['pre_sale_date_format'] = local_date(_L('goods_pre_sale_date_format', 'Y年 m月 d日'), $goods['pre_sale_date']);

        }
        else // 有貨，正常銷售
        {
            if($goods['pre_sale_days'] > 0 || $goods['pre_sale_date'] > $time)
            {
                $goods['can_pre_sale'] = 1;
            }
            $goods['pre_sale_days'] = 0;
            $order_day = $goods['pre_sale_days'];
            $shop_transfer_day++;
            $goods['pre_sale_date'] = 0;
            $goods['availability'] = 'InStock';
        }

        $delivery_text_title = "";
        $delivery_text = "";
        $delivery_remark = "";
        $goods['pre_sale_time'] = empty($goods['pre_sale_time']) ? 16 : $goods['pre_sale_time'];
        if($_CFG['lang'] == 'en_us') {
            $clock = local_date('g a', local_strtotime($goods['pre_sale_time'].":00"))." ";
            $string = ($goods['pre_sale_string'] == 0) ? 'shipped' : 'delivery';
        } else {
            $time_system = local_date('A', local_strtotime($goods['pre_sale_time'].":00"))." ";
            $time_system = ($time_system == 'AM ') ? "上午" : "下午";
            $clock = $time_system.local_date('g', local_strtotime($goods['pre_sale_time'].":00"))."點";
            $string = ($goods['pre_sale_string'] == 0) ? '出貨' : '派送';
        }

        if ($goods['pre_sale_days'] > 0) { //訂貨
            if($_CFG['lang'] == 'en_us') {
                $delivery_remark = sprintf($_LANG['goods_delivery_timing_preorder'], $clock, $string, $goods['pre_sale_days']); //一般於 %s 個工作天內出貨
            } else {
                $delivery_remark = sprintf($_LANG['goods_delivery_timing_preorder'], $clock, $goods['pre_sale_days'], $string); //一般於 %s 個工作天內出貨
            }
        } else if($today['holiday']) { //有貨, 假日
            if ($flsQty >= $qty) {
                if ($lwh_sum > 100) { //FLS SF+
                    $delivery_remark = $_LANG['goods_delivery_timing_holiday_fls_sfplus']; //現在結帳，下一個工作天早上寄出
                } else { //FLS Yamato
                    $delivery_remark = $_LANG['goods_delivery_timing_holiday_fls_ymt']; //現在結帳，下一個工作天寄出
                }

				if ($shopTotalQty >= $qty) {
                    $transfer_text = $_LANG['goods_delivery_timing_shop_pickup_fastest']; //門市自取最快即日取貨。
                } else {
                    $delivery_warn = $_LANG['goods_delivery_timing_shop_pickup_need_transfer']; //由於貨品位於不同貨倉，需要額外一至兩個工作天進行處理。
                }
            } else {
				if ($lwh_sum > 100) { //SF+
                    $delivery_remark = $_LANG['goods_delivery_timing_holiday_warehouse_sfplus']; // "現在結帳，下一個工作天早上寄出"
                } else { //SF or Yamato
                    // Yamato: 住宅下一個工作天
                    // SF: 工商最早當日
                    $delivery_remark= $_LANG['goods_delivery_timing_holiday_warehouse_ymt_sp']; // "現在結帳，下一個工作天寄出"
                }

				if ($shopTotalQty >= $qty) {
                    $transfer_text =  $_LANG['goods_delivery_timing_shop_pickup_fastest'];  //門市自取最快即日取貨。
                }
                
				if ($warehouseQty < $qty && $shopTotalQty >= $qty) {
                    $transfer_day++;
                    $delivery_warn = $_LANG['goods_delivery_timing_warehouse_need_transfer']; // "由於貨品位於不同貨倉，需要額外一至兩個工作天進行處理。"
                } elseif ($shopTotalQty < $qty) {
                    $delivery_warn_shop = $_LANG['goods_delivery_timing_shop_pickup_need_transfer']; //"門市自取需要額外一至兩個工作天進行調貨。"
                }

                //$delivery_remark = $_LANG['goods_delivery_timing_holiday']; //現在結帳，下一個工作天寄出
            }
        } else if($today['before_holiday']) { //有貨, 假日前
            if ($flsQty >= $qty) {
                if ($lwh_sum > 100) { //FLS SF+
                    if($today['hour'] < 9) {
                        $delivery_remark = $_LANG['goods_delivery_timing_before_holiday_9am_fls_sfplus']; //早上9點前確認付款，即日寄出，當晚派送
                    } elseif ($today['hour'] < $goods['pre_sale_time']) {
                        $delivery_remark = sprintf($_LANG['goods_delivery_timing_before_holiday_fls_sfplus'], $clock); //%s前確認付款，即日寄出，當晚派送
                    } else {
                        $delivery_remark = $_LANG['goods_delivery_timing_before_holiday_late_fls_sfplus']; //現在結帳，下一個工作天早上寄出，當晚派送
                    }
                } else { //FLS Yamato
                    if($today['hour'] < 14) {
                        $delivery_remark = $_LANG['goods_delivery_timing_working_day_2pm_fls_ymt']; //下午2點前確認付款，即日寄出，下一個工作天派送
                    } elseif ($today['hour'] < $goods['pre_sale_time']) {
                        $delivery_remark = sprintf($_LANG['goods_delivery_timing_working_day_fls_ymt'], $clock); //%s前確認付款，下一個工作天寄出，之後一個工作天派送
                    } else {
                        $delivery_remark = $_LANG['goods_delivery_timing_holiday_fls_ymt']; //現在結帳，下一個工作天寄出，之後一個工作天派送
                    }
                }

				if ($shopTotalQty >= $qty) {
                    $transfer_text = $_LANG['goods_delivery_timing_shop_pickup_fastest']; //  "門市自取最快即日取貨。";
                } else {
                    $delivery_warn_shop = $_LANG['goods_delivery_timing_shop_pickup_need_transfer']; //  "門市自取需要額外一至兩個工作天進行調貨。";
                }

            } else {

				if ($lwh_sum > 100) { //SF+
                    if ($today['hour'] < 9) {
                        $delivery_remark = $_LANG['goods_delivery_timing_before_holiday_warehouse_sfplus']; // "早上9點前確認付款，即日寄出，當晚派送。"
                    } elseif ($today['hour'] < $goods['pre_sale_time']) {
                        $delivery_remark = sprintf($_LANG['goods_delivery_timing_before_holiday_fls_sfplus'], $clock); //%s前確認付款，即日寄出，當晚派送
                    } else {
                        $delivery_remark = $_LANG['goods_delivery_timing_before_holiday_late_fls_sfplus']; //"現在結帳，下一個工作天早上寄出，當晚派送。";
                    }
                } else { //SF or Yamato
                    // Yamato: 住宅下一個工作天
                    // SF: 工商最早當日
                    if ($today['hour'] < 9) {
                        $delivery_remark = $_LANG['goods_delivery_timing_before_holiday_warehouse_yamato_sf_9']; //  "早上9點前確認付款，即日寄出，下一個工作天派送。";
                    } elseif ($today['hour'] < 13) {
                        $delivery_remark = $_LANG['goods_delivery_timing_before_holiday_warehouse_yamato_sf_13']; // "下午點前確認付款，即日寄出，下一個工作天派送"
                    } elseif ($today['hour'] < $goods['pre_sale_time']) {
                        $delivery_remark = sprintf($_LANG['goods_delivery_timing_warehouse_yamato_sf_sale_time'], $clock); // "%s前確認付款，下一個工作天寄出";
                    } else {
                        $delivery_remark = $_LANG['goods_delivery_timing_before_holiday_warehouse_yamato_sf']; // "現在結帳，下一個工作天寄出，之後一個工作天派送。"
                    }
                }

 				if ($shopTotalQty >= $qty) {
                    $transfer_text = $_LANG['goods_delivery_timing_shop_pickup_fastest'];
                }
                if ($warehouseQty < $qty && $shopTotalQty >= $qty) {
                    $transfer_day++;
                    $delivery_warn = $_LANG['goods_delivery_timing_warehouse_need_transfer'];
                } elseif ($shopTotalQty < $qty) {
                    $delivery_warn_shop = $_LANG['goods_delivery_timing_shop_pickup_need_transfer'];
                }

                // if($today['hour'] < 9) {
                //     $delivery_remark = $_LANG['goods_delivery_timing_before_holiday_9am']; //早上9點前確認付款，即日寄出
                // } elseif ($today['hour'] < $goods['pre_sale_time']) {
                //     $delivery_remark = sprintf($_LANG['goods_delivery_timing_before_holiday'], $clock); //%s前確認付款，即日寄出。
                // } else {
                //     $delivery_remark = $_LANG['goods_delivery_timing_before_holiday_late']; //現在結帳，下一個工作天寄出
                // }
            }
        } else {
            if ($flsQty >= $qty) {
                if ($lwh_sum > 100) { //FLS SF+
                    if($today['hour'] < 9) {
                        $delivery_remark = $_LANG['goods_delivery_timing_working_day_9am_fls_sfplus']; //早上9點前確認付款，即日寄出
                    } elseif ($today['hour'] < $goods['pre_sale_time']) {
                        $delivery_remark = sprintf($_LANG['goods_delivery_timing_working_day_fls_sfplus'], $clock); //%s前確認付款，即日寄出
                    } else {
                        $delivery_remark = $_LANG['goods_delivery_timing_working_day_late_fls_sfplus']; //現在結帳，明天早上寄出
                    }
                } else { //FLS Yamato
                    if($today['hour'] < 14) {
                        $delivery_remark = $_LANG['goods_delivery_timing_working_day_2pm_fls_ymt']; //下午2點前確認付款，即日寄出
                    } elseif ($today['hour'] < $goods['pre_sale_time']) {
                        $delivery_remark = sprintf($_LANG['goods_delivery_timing_working_day_fls_ymt'], $clock); //%s前確認付款，下一個工作天寄出
                    } else {
                        $delivery_remark = $_LANG['goods_delivery_timing_working_day_late_fls_ymt']; //現在結帳，下一個工作天寄出
                    }
                }

				if ($shopTotalQty >= $qty) {
                    $transfer_text = $_LANG['goods_delivery_timing_shop_pickup_fastest'];
                } else {
                    $delivery_warn_shop = $_LANG['goods_delivery_timing_shop_pickup_need_transfer'];
                }
            } else {
				if ($lwh_sum > 100) { //SF+
                    if ($today['hour'] < 9) {
                        $delivery_remark = $_LANG['goods_delivery_timing_warehouse_sfplus_9']; // "早上9點前確認付款，即日寄出。"
                    } elseif ($today['hour'] < $goods['pre_sale_time']) {
                        $delivery_remark = sprintf($_LANG['goods_delivery_timing_before_holiday_fls_sfplus'], $clock); //%s前確認付款，即日寄出
                    } else {
                        $delivery_remark = $_LANG['goods_delivery_timing_warehouse_sfplus']; //  "現在結帳，下一個工作天早上寄出。";
                    }
                } else { //SF or Yamato
                    // Yamato: 住宅下一個工作天
                    // SF: 工商最早當日
					// 工商區:SF (09:00截單,11:00收件,當日派送 | 16:00截單, 17:00收件,下一工作天派送)  
					// 住宅:Yamato (14:00截單, 16:00收件, 下一工作天派送)
                    if ($today['hour'] < 9) {
                        $delivery_remark = $_LANG['goods_delivery_timing_warehouse_yamato_sf_9'] ; //  "早上9點前確認付款，即日寄出";
                    } elseif ($today['hour'] < 13) {
                        $delivery_remark = $_LANG['goods_delivery_timing_warehouse_yamato_sf_13'] ; // "下午1點前確認付款，即日寄出";
                    } elseif ($today['hour'] < $goods['pre_sale_time']) {
                        $delivery_remark = sprintf($_LANG['goods_delivery_timing_warehouse_yamato_sf_sale_time'], $clock); // "%s前確認付款，下一個工作天寄出";
                    } else {
                        $delivery_remark = $_LANG['goods_delivery_timing_warehouse_yamato_sf']; // "現在結帳，下一個工作天寄出";
                    }
                }

				if ($shopTotalQty >= $qty) {
					$transfer_text = $_LANG['goods_delivery_timing_shop_pickup_fastest'];
				}
				if ($warehouseQty < $qty && $shopTotalQty >= $qty) {
                    $transfer_day++;
					$delivery_warn = $_LANG['goods_delivery_timing_warehouse_need_transfer'];
				} elseif ($shopTotalQty < $qty) {
					$delivery_warn_shop = $_LANG['goods_delivery_timing_shop_pickup_need_transfer'];
				}
                // if($today['hour'] < 9) {
                //     $delivery_remark = $_LANG['goods_delivery_timing_working_day_9am']; //早上9點前確認付款，即日寄出
                // } elseif ($today['hour'] < $goods['pre_sale_time']) {
                //     $delivery_remark = sprintf($_LANG['goods_delivery_timing_working_day'], $clock); //%s前確認付款，即日寄出
                // } else {
                //     $delivery_remark = $_LANG['goods_delivery_timing_working_day_late']; //現在結帳，明天早上寄出
                // }
            }
        }
        if($show)$text = sprintf($_LANG['goods_buy_online_available'], $delivery_remark); //接受網上下單，%s
        else $text = $delivery_remark;
        $goods['delivery_remark'] = $text;
        
        if ($transfer_day == 0 && $order_day == 0){
            $delivery_text_title = $_LANG['goods_delivery_8hs_title'];
            $delivery_text = $_LANG['goods_delivery_8hs'];
        } else {
            $delivery_text_title = $_LANG['goods_buy_delivery_ordering_title'];
            $total_day = ($transfer_day + $order_day);
            if ($transfer_day > 0){
                $to_day = "&#32;-&#32;" . ($total_day + 1);
            } else {
                $to_day = "";
            }

            $delivery_text = sprintf($_LANG['goods_buy_delivery_ordering'], $total_day . $to_day);
        }

        $goods['delivery_text_title'] = $delivery_text_title;
        $goods['delivery_text'] = $delivery_text;

		$erpController = new ErpController();
		if ($erpController->getMainWarehouseId() == 1) {
			$goods['delivery_warn'] = '';
			$goods['delivery_warn_shop'] = '';
			$goods['transfer_text'] = '';
		} else {
			$goods['delivery_warn'] = $delivery_warn;
			$goods['delivery_warn_shop'] = $delivery_warn_shop;
			$goods['transfer_text'] = $transfer_text;
        }

        $shop_total_day = array();
        $all_shop_got_stock = TRUE;
        $least_one_shop_got_stock = FALSE;
        foreach ($goods['goods_number_shop'] as $key => $no){
            if ($no >= $qty){
                $shop_total_day[$key] = 0;
                $least_one_shop_got_stock = TRUE;
            } else {
                $shop_total_day[$key] = $shop_transfer_day + $order_day;
                $all_shop_got_stock = FALSE;
            }
        }

        if ($least_one_shop_got_stock){
            $all_shop_total_day = 0;
        } else {
            $all_shop_total_day = $shop_transfer_day + $order_day;
        }
        
        $goods['transfer_day'] = $transfer_day;
        $goods['shop_transfer_day'] = $shop_transfer_day;
        $goods['order_day'] = $order_day;

        $goods['all_shop_total_day'] = $all_shop_total_day; // wait day for all shops (only used if no stock in all shops) 

        $goods['warehouse_total_day'] = $transfer_day + $order_day; // wait day for warehouse
        $goods['shop_total_day'] = $shop_total_day; // wait day for each shop - shop_total_day[shop_id] = stock no

        return $goods;
    }

    public function urpAction()
    {
        $goods_id = empty($_REQUEST['id']) ? '' : $_REQUEST['id'];
        $userRankController = new UserRankController();
        $list = $userRankController->getGoodsMemberPrices($goods_id);
		$assign['member_list'] = $list;
		global $smarty;
		$act = 'update_urp';
		if($this->admin_priv)admin_priv($this->admin_priv);
		$old_act = $_REQUEST['act'];
		// Create act info
		if(empty($act) && empty($id)) $act = 'insert';
		else if(empty($act)) {
			if($old_act == 'add') $act = 'insert';
			else if($old_act == 'edit') $act = 'update';
		}

		$assign['ur_here'] = _L($old_act.'_'.$this->className, strtoupper($old_act." ".$this->className));
		$assign['form_action'] = $act;
		$assign['action_link'] = ['text'=>'產品列表', 'href'=>'goods.php?act=list'];

		if($goods_id > 0) {
			$assign['id'] = $goods_id;
		}

		$this->generateActionTemplate($assign, 'goods_user_rank_price.htm');

    }

    public function updateUrpAction($data = [])
    {
        global $ecs, $db;

        if(empty($data)) extract($_REQUEST);
        else  extract($data);

        $goods_id = empty($_REQUEST['id']) ? '' : $_REQUEST['id'];

        if (isset($data['id']) && !empty($data['id'])) {
            $goods_id = $data['id'];
        }

        $sql = "SELECT shop_price FROM " . $ecs->table('goods') .
                    " WHERE  goods_id = '$goods_id'";
        $good_price = $db->getOne($sql);

        if(empty($member_price)) $member_price = [];

        foreach ($member_price as $rank_id => $val) {
            $max_amount = !empty($max_amount[$rank_id]) ? $max_amount[$rank_id] : 0;
            if (strpos($val, "%")) {
                $rate = strstr($val, "%", true);
                $price = intval($good_price * ($rate/100));
            } else {
                $price = floatval($val);
            }

            /* If input -1, delete price */
            if ($val == "-1") {
                $sql = "DELETE FROM ".$ecs->table('member_price') .
                    " WHERE user_rank = $rank_id AND goods_id = '$goods_id'";
                $db->query($sql);
            } elseif ($price < 0 || $price == 0) {
                continue;
            } else {
                $sql = "SELECT COUNT(user_price) FROM " . $ecs->table('member_price') .
                        " WHERE user_rank = '$rank_id' AND goods_id = '$goods_id'";
                if ($db->getOne($sql) > 0) {//判断是否有USER_PRICE数值
                    $sql =	"update " . $ecs->table('member_price') .
                            " set user_price = '$price', max_amount = $max_amount  WHERE user_rank ='$rank_id' AND goods_id = '$goods_id'";
                    $db->query($sql);
                } else {
                    $sql =	"INSERT INTO " . $ecs->table('member_price') ." (user_price,user_rank,goods_id, max_amount) " .
                            " VALUES ('$price','$rank_id', '$goods_id', $max_amount)";
                    $db->query($sql);
                }
            }
        }

        /* 清除缓存 */
        clear_cache_files();

		admin_log('修改產品特殊格價: '.$id, 'edit', 'member_price');

        if (isset($data['return']) && $data['return'] == true) {
            return true;
        }

		$href = 'goods.php?act=list';

		$link[0]['text'] = _L('back_list', '返回列表');
        $link[0]['href'] = $href;
        $note = _L('attradd_succed', '操作成功');
        sys_msg($note, 0, $link);
    }

    /**
     * Use for Wholesale & Staff purchase pricing criteria
     */
    public function corporatePriceSuggestion($goods = [], $goods_id = 0, $crp_info, $price_only = false)
    {
        require_once(ROOT_PATH . 'includes/lib_goods.php');
        global $ecs, $db;
        if(empty($goods) && $goods_id > 0) $goods = $db->getRow("SELECT * FROM ".$ecs->table('goods')." WHERE goods_id = ".$goods_id);
        if($price_only)$sp_price =  0; else $sp_price = '';
        $info = '';
        $error = '';
        $market_price = $goods['market_price'];
        $cost = $goods['cost'];
        if ($goods['promote_price'] > 0)
        {
            $promote_price = bargain_price($goods['promote_price'], $goods['promote_start_date'], $goods['promote_end_date']);
        }
        else
        {
            $promote_price = 0;
        }
        if($promote_price > 0) $goods['shop_price'] = min($goods['shop_price'], $promote_price);
        if($crp_info['crp_type'] == 4) return $goods['shop_price'];
        $yoho_min_price = $goods['shop_price'] * 0.97;
        $yoho_min_price = number_format($yoho_min_price, 2, '.', '');
        if($cost == 0) $error = '價格錯誤(成本價不正確)';
        else if($crp_info['crp_type'] == 1) {
            /**
             * 市價折扣:
             * (SRP) Discount (input percentage)% OFF
             * and
             * smaller and equal to yoho  最低價 (selling price *0.97)
             * and
             * larger than yoho cost x 1.1
             */
            $sp_price = $market_price * ( 1 - ($crp_info['srp_percentage'] / 100));
            $sp_price = number_format($sp_price, 2, '.', '');
            if (!($sp_price <= $yoho_min_price && $sp_price > ($cost * 1.1))) {
                $error = '價格錯誤';
            }
            $info.= ('(市價折扣: '.$crp_info['srp_percentage'].'%OFF)');
        } elseif ($crp_info['crp_type'] == 2) {
            /**
             * 大批量:
             * Cost *1.15
             * and
             * smaller and equal to yoho  最低價 (selling price *0.97)
             * if cost*1.15 is larger than yoho  最低價, show yoho  最低價
             */
            $sp_price = $cost * 1.15;
            $sp_price = number_format($sp_price, 2, '.', '');
            if($sp_price > $yoho_min_price) {
                $sp_price = $yoho_min_price;
            }
            if($sp_price < $cost) $error = '價格錯誤';
            $info.= ('(大批量價格)');

        } elseif ($crp_info['crp_type'] == 3) {
            /**
             * 小批量:
             * Cost *1.2
             * and
             * smaller and equal to yoho  最低價 (selling price *0.97)
             * if cost*1.2 is larger than yoho  最低價, show yoho  最低價
             */
            $sp_price = $cost * 1.2;
            $sp_price = number_format($sp_price, 2, '.', '');
            if($sp_price > $yoho_min_price) {
                $sp_price = $yoho_min_price;
            }
            if($sp_price < $cost) $error = '價格錯誤';
            $info.= ('(小批量價格)');
        }

        if($price_only) return $sp_price;
        else {
            if(!empty($error)) return $error.$info;
            return $sp_price.$info;
        }
    }

    function getGoodsPreSaleDaysAll()
    {
        global $ecs, $db;
        $sql = "SELECT SUM(pre_sale_days) AS total_day, COUNT(*) AS total_product FROM " . $ecs->table('goods') . " WHERE pre_sale_days > 0";
        $res = $db->getRow($sql);
        return $res;
    }

    public function getGoodsCategoryList($goods_id_list)
    {
        global $ecs, $db;
        if (empty($goods_id_list)) {
            return null;
        }

        $query = "SELECT g.goods_id, c.cat_name FROM " . $ecs->table('goods') . " AS g, " . $ecs->table('category') . " AS c WHERE (g.goods_id IN ('" . implode("', '", $goods_id_list) . "')) AND (g.cat_id = c.cat_id)";
        $result = $db->getAll($query);
        return $result;
    }

    function product_list_get_favourable_info($user_rank = 0, $goods_list = array())
    {
        global $db, $ecs, $_LANG, $_CFG;
        require(ROOT_PATH . 'languages/' .$_CFG['lang']. '/yoho.php');
        
        if(empty($goods_list)){
            return array();
        }  

        $favourable_list = array();
        $user_rank = ',' . $user_rank . ',';
        $now = gmtime();
        
        $search_list = array();
        foreach($goods_list as $goods_id){
            $search_list[] = ",".$goods_id.",";
        }
        $search_str = implode("|", $search_list);

        /*
        $sql = "SELECT act_id, act_name, act_range_ext, act_type, act_kind " .
                "FROM " . $ecs->table('favourable_activity') .
                " WHERE CONCAT(',', user_rank, ',') LIKE '%" . $user_rank . "%'" .
                " AND start_time <= '$now' AND end_time >= '$now'" .
                " AND act_type = '" . FAT_GOODS . "'" .
                " AND CONCAT(',', act_range_ext, ',') REGEXP '".$search_str."'" .
                " ORDER BY sort_order";
        */
        $sql = "SELECT fa.act_id, fa.act_range_ext, fa.act_type, fa.act_kind, fa.gift, fa.act_name, fal.act_name as act_name_i18n, fa.act_tag, fal.act_tag as act_tag_i18n " .
                "FROM " . $ecs->table('favourable_activity') . " fa 
                left join " . $ecs->table('favourable_activity_lang') . "fal on (fa.act_id = fal.act_id) and lang = '".$_CFG['lang']."' " .
                " WHERE CONCAT(',', user_rank, ',') LIKE '%" . $user_rank . "%'" .
                " AND start_time <= '$now' AND end_time >= '$now'" .
                " AND status = 1" .
                " AND act_type = '" . FAT_GOODS . "'" .
                " AND CONCAT(',', act_range_ext, ',') REGEXP '".$search_str."'" .
                " ORDER BY sort_order";
        $res = $db->query($sql);

        $erpController = new ErpController();
        $gifts_ids_arr = array();
        $related_favourable = array();
        while ($favourable = $GLOBALS['db']->fetchRow($res))
        {
            //If 贈品, save the gift id for checking
            if($favourable["act_kind"] == 1){
                $gifts = unserialize( $favourable['gift']);
                $gifts_ids = array_map(function ($gift) { return $gift['id']; }, $gifts);
                foreach($gifts_ids as $id){
                    if(!in_array($id, $gifts_ids_arr)){
                        $gifts_ids_arr[] = $id;
                    }
                }
            }

            $related_favourable[] = $favourable;
        }

        $gifts_stock = $erpController->getSumOfSellableProductsQtyByWarehouses($gifts_ids_arr,null,true,null,false);
        foreach($related_favourable as $favourable){
            $favourable_info = array();
            $favourable_info["act_type"] = $favourable["act_type"];
            $favourable_info["act_kind"] = $favourable["act_kind"];

            //If 贈品, check the stock
            if($favourable["act_kind"] == 1){
                $gifts = unserialize( $favourable['gift']);
                $gifts_ids = array_map(function ($gift) { return $gift['id']; }, $gifts);

                $sql = "SELECT g.goods_id, g.goods_name, g.goods_thumb, g.goods_img, g.shop_price, g.is_on_sale, g.goods_sn, gperma " ." FROM " . $ecs->table('goods') . "as g " .
                "LEFT JOIN (SELECT id, table_name, lang, perma_link as gperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") gpl ON gpl.id = g.goods_id  AND gpl.table_name = 'goods' AND gpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
                "WHERE g.goods_id " .  db_create_in($gifts_ids);
                $gifts_goods_list = $db->getAll($sql);
                $gifts_goods_list = localize_db_result('goods', $gifts_goods_list);
                
                foreach ($gifts as $key => $value)
                {
                    $goods = null;
                    foreach ($gifts_goods_list as $g)
                    {
                        if ($g['goods_id'] == $value['id'])
                        {
                            $goods = $g;
                            break;
                        }
                    }
                    if(!$goods || !$goods['is_on_sale'])
                    {
                        unset($gifts[$key]);
                        continue;
                    }

                    $stock = $gifts_stock[$value['id']];
                    $stock = $stock > 0 ? $stock : 0;
                    if($stock == 0){
                        unset($gifts[$key]);
                        continue;
                    }
                }
                
                if(count($gifts) == 0){
                    continue;
                }
            }

            //handle different language
            if (!empty($favourable['act_name_i18n'])) {
                $favourable['act_name'] = $favourable['act_name_i18n'];
            }
            if (!empty($favourable['act_tag_i18n'])) {
                $favourable['act_tag'] = $favourable['act_tag_i18n'];
            }
            
            $favourable_info["favourable_desc"] = $favourable['act_tag'];
            //$favourable_info["favourable_desc"] = $favourable["act_name"];
            
            //assign favourable by goods_id if it's in the input goods_list
            $goods_id_list = explode(",", $favourable["act_range_ext"]);
            foreach($goods_id_list as $goods_id){
                if(in_array($goods_id, $goods_list)){
                    $favourable_list[$goods_id][] = $favourable_info;
                }
            }
        }

        return $favourable_list;
    }

    function product_list_get_package_info($goods_list = array())
    {
        global $db, $ecs, $_LANG, $_CFG;
        require(ROOT_PATH . 'languages/' .$_CFG['lang']. '/yoho.php');
        
        if(empty($goods_list)){
            return array();
        }  

        $package_list = array();
        $now = gmtime();
        
        $search_str = implode(",", $goods_list);

        /*
        $sql = "SELECT act_id, act_name, pg.goods_id " .
                "FROM " . $ecs->table('goods_activity') . " ga LEFT JOIN " . $ecs->table('package_goods') . " pg ON ga.act_id = pg.package_id ".
                " WHERE start_time <= '$now' AND end_time >= '$now'" .
                " AND pg.goods_id in (".$search_str.")";
        */
        $sql = "SELECT ga.act_id, pg.goods_id, ga.act_name, gal.act_name as act_name_i18n, ga.act_tag, gal.act_tag as act_tag_i18n 
                FROM " . $ecs->table('goods_activity') . " AS ga left join ".$ecs->table('goods_activity_lang')." gal on (ga.act_id = gal.act_id) and lang = '".$_CFG['lang']."'   , " . $ecs->table('package_goods') . " AS pg
                WHERE pg.package_id = ga.act_id
                AND ga.start_time <= '" . $now . "'
                AND ga.end_time >= '" . $now . "'
                AND ga.status = 1
                AND pg.goods_id in (".$search_str.")";
        $res = $db->query($sql);

        while ($package = $GLOBALS['db']->fetchRow($res)){
             //handle different language
             if (!empty($package['act_name_i18n'])) {
                $package['act_name'] = $package['act_name_i18n'];
            }
            if (!empty($package['act_tag_i18n'])) {
                $package['act_tag'] = $package['act_tag_i18n'];
            }
            
            //assign package by goods_id if it's in the input goods_list
            if(in_array($package['goods_id'], $goods_list) && empty($package_list[$package['goods_id']])){
                $package_list[$package['goods_id']][] = $package['act_tag'];
            }
        }

        return $package_list;
    }

    function product_list_get_topic_info($goods_list = array())
    {
        global $db, $ecs, $_LANG, $_CFG;
        require(ROOT_PATH . 'languages/' .$_CFG['lang']. '/yoho.php');
        
        if(empty($goods_list)){
            return array();
        }  

        $topic_list = array();
        $now = gmtime();
        
        $search_list = array();
        foreach($goods_list as $goods_id){
            $search_list[] = "[|]".$goods_id."\"";
        }
        $search_str = implode("|", $search_list);

        
        $sql = "SELECT t.*, tperma FROM " . $GLOBALS['ecs']->table('topic') .  
            " t LEFT JOIN (SELECT id, table_name, lang, perma_link as tperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") acpl ON acpl.id = t.topic_id  AND acpl.table_name = 'topic' AND acpl.lang = '" . $_CFG['lang'] . "' " .
            "WHERE (" . gmtime() . " BETWEEN start_time AND end_time + 86399) " .
            "AND `data` REGEXP '".$search_str."' " .
            "ORDER BY `sort_order` ASC,`topic_id` ASC";

        $res = $db->query($sql);
        
        while ($topic = $GLOBALS['db']->fetchRow($res)){
            if(file_exists(ROOT_PATH . DATA_DIR . '/afficheimg/topic/' . $topic['topic_id'] . '_icon.png')){
                $datas_set = unserialize($topic['data']);
                foreach($datas_set as $datas){
                    foreach($datas as $data){
                        $goods_ids = end(explode('|', $data));

                        //assign topic by goods_id if it's in the input goods_list
                        if(in_array($goods_ids, $goods_list)  && empty($topic_list[$goods_ids])){
                            $topic_list[$goods_ids][] = $topic['tag'];
                        }
                    }
                }
            }          
        }
        
        return $topic_list;
    }
}