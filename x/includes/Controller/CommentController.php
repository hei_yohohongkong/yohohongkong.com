<?php

/***
 * CommentController.php
 * by Anthony 20180606
 *
 * Comment controller
 ***/

namespace Yoho\cms\Controller;

if (!defined('IN_ECS')) {
    die('Hacking attempt');
}

class CommentController extends YohoBaseController
{
    //用家評價的類型;0評價的是商品,1評價的是文章
    const COMMENT_TYPE_GOODS = 0;
    const COMMENT_TYPE_ARTICLE = 1;
    const REVIEW_SHOP_TABLE_ID = 0;
    const REVIEW_SALES_TABLE_ID = 1;
    const REVIEW_CRM_TABLE_ID = 2;
    const REVIEW_TABLES = [
        self::REVIEW_SHOP_TABLE_ID    => "review_shop",
        self::REVIEW_SALES_TABLE_ID   => "review_sales",
        self::REVIEW_CRM_TABLE_ID     => "review_crm",
    ];
    const REVIEW_TABLES_ITEMS = [
        self::REVIEW_SHOP_TABLE_ID => [
            "score1" => "友和寄貨",
            "score2" => "速遞送貨",
            "score3" => "整體評分"
        ],
        self::REVIEW_SALES_TABLE_ID   => [
            "score1" => "服務態度",
            "score2" => "產品知識",
            "score3" => "整體評分"
        ],
        self::REVIEW_CRM_TABLE_ID => [
            "score1" => "服務態度",
            "score2" => "解決問題",
        ],
    ];

    /**
     * Using User can comment in goods page
     */
    public function user_can_comment($type = self::COMMENT_TYPE_GOODS, $goods_id)
    {
        global $_CFG, $_LANG, $db, $ecs;
        $user_id = $_SESSION['user_id'];
        // can comment type
        $factor = intval($_CFG['comment_factor']);
        $can_comment = true;
        $error = '';

        /* 只有商品才检查评论条件 */
        if ($type == self::COMMENT_TYPE_GOODS) {
            switch ($factor) {

                case COMMENT_ALL: // All people can comment
                    $can_comment = true;
                    break;

                case COMMENT_LOGIN: // Only login user can comment
                    if ($user_id == 0) {
                        $can_comment = false;
                        $error = $_LANG['comment_login'];
                    }
                    break;

                case COMMENT_CUSTOM: // Only custom can comment
                    if ($user_id > 0) {
                        $sql = "SELECT o.order_id FROM " . $ecs->table('order_info') . " AS o " .
                            " WHERE user_id = '" . $user_id . "'" .
                            " AND (o.order_status = '" . OS_CONFIRMED . "' or o.order_status = '" . OS_SPLITED . "') " .
                            " AND (o.pay_status = '" . PS_PAYED . "' OR o.pay_status = '" . PS_PAYING . "') " .
                            " AND (o.shipping_status = '" . SS_SHIPPED . "' OR o.shipping_status = '" . SS_RECEIVED . "') " .
                            " LIMIT 1";

                        $tmp = $db->getOne($sql);
                        if (empty($tmp)) {
                            $error = $_LANG['comment_custom'];
                            $can_comment = false;
                        }
                    } else {
                        $error = $_LANG['comment_custom'];
                        $can_comment = false;
                    }
                    break;
                case COMMENT_BOUGHT: // Only buy this product custom can comment
                    if ($user_id > 0) {
                        $sql = "SELECT COUNT(o.order_id)" .
                        " FROM " . $ecs->table('order_info') . " AS o, " .
                        $ecs->table('order_goods') . " AS og " .
                            " WHERE o.order_id = og.order_id" .
                            " AND o.user_id = '" . $user_id . "'" .
                            " AND og.goods_id = '" . $goods_id . "'" .
                            " AND og.delivery_qty > 0 " .
                            " AND (o.order_status = '" . OS_CONFIRMED . "' or o.order_status = '" . OS_SPLITED . "') " .
                            " AND (o.pay_status = '" . PS_PAYED . "' OR o.pay_status = '" . PS_PAYING . "') " .
                            " AND (o.shipping_status = '" . SS_SHIPPED . "' OR o.shipping_status = '" . SS_RECEIVED . "' OR o.shipping_status = '" . SS_SHIPPED_PART . "') " .
                            " LIMIT 1";
                        $bought_count = $db->getOne($sql);
                        if (!$bought_count) {
                            $can_comment = false;
                            $error = $_LANG['comment_brought'];
                        } else {
                            $sql = "SELECT COUNT(comment_id) FROM " . $ecs->table('comment') .
                                " WHERE user_id = '" . $user_id . "'" .
                                " AND id_value= '" . $goods_id . "'" .
                                " LIMIT 1";
                            $comment_count = $db->getOne($sql);
                            if ($comment_count >= $bought_count) {
                                $can_comment = false;
                                $error = $_LANG['comment_repeat'];
                            }
                        }
                    } else {
                        $can_comment = false;
                        $error = $_LANG['comment_login'];
                    }
            }
            return ['can_comment' => $can_comment, 'message' => $error];
        } else {
            return ['can_comment' => true, 'message' => ''];
        }

    }

    public function listAction()
    {
        global $smarty, $_LANG;
        /* 检查权限 */
        admin_priv('comment_priv');
        //
        $action_link = array('href' => 'comment_manage.php?act=edit_comment_config', 'text' => '評價設置');
        $smarty->assign('action_link', $action_link);
        $smarty->assign('ur_here', $_LANG['05_comment_manage']);
        assign_query_info();
        $smarty->display('comment_list.htm');
    }

    public function ajaxQueryAction()
    {
        $list = $this->get_comment_list();
        // We're using dataTables Ajax to query.
        $this->tableList = $list['item'];
        $this->tableTotalCount = $list['record_count'];

        parent::ajaxQueryAction(null, null, ['view']);

    }

    /**
     * 获取评论列表
     * @access  public
     * @return  array
     */
    public function get_comment_list()
    {
        /* 查询条件 */
        $_REQUEST['keywords'] = empty($_REQUEST['keywords']) ? 0 : trim($_REQUEST['keywords']);
        if (isset($_REQUEST['is_ajax']) && $_REQUEST['is_ajax'] == 1) {
            $_REQUEST['keywords'] = json_str_iconv($_REQUEST['keywords']);
        }
        $_REQUEST['sort_by'] = empty($_REQUEST['sort_by']) ? 'add_time' : trim($_REQUEST['sort_by']);
        $_REQUEST['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);

        $where = (!empty($_REQUEST['keywords'])) ? " AND content LIKE '%" . mysql_like_quote($_REQUEST['keywords']) . "%' " : '';

        $sql = "SELECT count(*) FROM " . $GLOBALS['ecs']->table('comment') . " WHERE parent_id = 0 $where";
        $_REQUEST['record_count'] = $GLOBALS['db']->getOne($sql);

        /* 获取评论数据 */
        $arr = array();
        $sql = "SELECT * FROM " . $GLOBALS['ecs']->table('comment') . " WHERE parent_id = 0 $where " .
            " ORDER BY $_REQUEST[sort_by] $_REQUEST[sort_order] " .
            " LIMIT " . $_REQUEST['start'] . ", $_REQUEST[page_size]";
        $res = $GLOBALS['db']->query($sql);

        while ($row = $GLOBALS['db']->fetchRow($res)) {
            $sql = ($row['comment_type'] == 0) ?
            "SELECT goods_name FROM " . $GLOBALS['ecs']->table('goods') . " WHERE goods_id='$row[id_value]'" :
            "SELECT title FROM " . $GLOBALS['ecs']->table('article') . " WHERE article_id='$row[id_value]'";
            $row['title'] = $GLOBALS['db']->getOne($sql);

            /* 标记是否回复过 */

            $row['add_time'] = local_date($GLOBALS['_CFG']['time_format'], $row['add_time']);
            $row['have_image'] = !empty($row['image']);

            $arr[] = $row;
        }
        $_REQUEST['keywords'] = stripslashes($_REQUEST['keywords']);
        $arr = array('item' => $arr, 'record_count' => $_REQUEST['record_count']);

        return $arr;
    }

    public function comment_image($images)
    {
        $img_list = unserialize($images);
        if (!$img_list) {
            return [];
        } else {
            $list = [];
            foreach ($img_list as $key => $img) {
                $ext = end(explode('.', $img));
                $name = basename($img, '.' . $ext);
                $thumb = $name . "_thumb.png";
                $url = '/uploads/comment/' . $img;
                $thumb_url = '/uploads/comment/' . $thumb;
                $list[$key]['source'] = $url;
                $list[$key]['thumb'] = $thumb_url;
            }
        }

        return $list;
    }

    /**
     * 添加评论内容
     *
     * @access  public
     * @param   object  $cmt
     * @return  void
     */
    public function add_comment($cmt)
    {
        include_once ROOT_PATH . 'includes/lib_order.php';
        /* 评论是否需要审核 */
        $status = 1 - $GLOBALS['_CFG']['comment_check'];
        $integral = $GLOBALS['_CFG']['comment_integral'];

        $user_id = empty($_SESSION['user_id']) ? 0 : $_SESSION['user_id'];
        $email = empty($cmt['email']) ? $_SESSION['email'] : trim($cmt['email']);
        $user_name = empty($cmt['username']) ? $_SESSION['user_name'] : '';
        $email = htmlspecialchars($email);
        $user_name = htmlspecialchars($user_name);
        $lei = $cmt['lei'];

        /* 保存评论内容 */
        $GLOBALS['db']->query('SET NAMES utf8mb4');
        $sql = " INSERT INTO " . $GLOBALS['ecs']->table('comment') .
        "(comment_type, id_value, email, user_name, content, comment_rank, add_time, ip_address, status, parent_id, user_id,lei) VALUES " .
        "('" . $cmt['type'] . "', '" . $cmt['id'] . "', '$email', '$user_name', '" . $cmt['content'] . "', '" . $cmt['rank'] . "', " . gmtime() . ", '" . real_ip() . "', '$status', '0', '$user_id', '$lei')";

        $result = $GLOBALS['db']->query($sql);
        $id = $GLOBALS['db']->insert_id();
        if (!empty($cmt['image'])) {
            $img_list = [];
            foreach ($cmt['image']['file']['name'] as $key => $value) {
                if ($cmt['image']['file']['error'][$key] !== 0) {
                    continue;
                }

                $tmp_name = $cmt['image']['file']['tmp_name'][$key];
                $size = getimagesize($tmp_name);
                $width = $size[0];
                $height = $size[1];
                $type = $size['mime'];
                $save_path = ROOT_PATH . 'uploads/comment/';
                if (!make_dir($save_path)) {
                    return ['error' => 1, 'message' => _L('user_avatar_upload_error_other', '圖片上傳錯誤，請稍後再試。')];
                }
                $save_path .= $id . "_" . $key . '_' . rand(1001, 9999);

                $animated_gif_fix = '';
                if ($type == 'image/jpeg') {
                    $tmp_path = $save_path . '_tmp.jpg';
                } else if ($type == 'image/gif') {
                    $tmp_path = $save_path . '_tmp.gif';
                    $animated_gif_fix = '[0]'; // Get first frame only
                } else if ($type == 'image/png') {
                    $tmp_path = $save_path . '_tmp.png';
                } else {
                    return ['error' => 1, 'message' => _L('user_avatar_upload_error_other', '圖片上傳錯誤，請稍後再試。')];
                }
                if (!move_uploaded_file($tmp_name, $tmp_path)) {
                    return ['error' => 1, 'message' => _L('user_avatar_upload_error_other', '圖片上傳錯誤，請稍後再試。')];
                }
                exec('/usr/bin/convert ' . escapeshellarg($tmp_path . $animated_gif_fix) . '  -auto-orient -gravity center -strip ' . escapeshellarg($save_path . '.jpg'));
                exec('/usr/bin/convert ' . escapeshellarg($tmp_path . $animated_gif_fix) . '  -auto-orient -resize 120x120^ -gravity center -extent 120x120 ' . escapeshellarg($save_path . '_thumb.png'));
                exec('/usr/bin/convert ' . escapeshellarg($save_path . '.jpg') . ' -auto-orient -quality 75 ' . escapeshellarg($save_path . '.jpg'));
                $img_list[] = str_replace(ROOT_PATH . 'uploads/comment/', '', $save_path . '.jpg');
                unlink($tmp_path);
            }
        }
        $image = (empty($img_list)) ? '' : serialize($img_list);
        $sql = "UPDATE " . $GLOBALS['ecs']->table('comment') .
            "set image = " .
            "'" . $image . "' where comment_id = " . $id;

        $result = $GLOBALS['db']->query($sql);

        //

        clear_cache_files('comments_list.lbi');
        if ($status > 0) {
            $goods_name = $GLOBALS['db']->getOne('SELECT goods_name FROM ' . $GLOBALS['ecs']->table('goods') . " WHERE goods_id = " . $cmt['id'] . " LIMIT 1");
            log_account_change($user_id, 0, 0, 0, $integral, '填寫評價送積分: ' . mysql_real_escape_string($goods_name));
        }
        return $result;
    }

    public function get_comment_config($get_all = false)
    {

        global $db, $ecs, $_CFG;
        $sql = "SELECT * FROM " . $ecs->table('shop_config') .
        " WHERE parent_id = (SELECT id FROM " . $ecs->table('shop_config') . " WHERE code = 'comment_config' and type = 'group' )";
        $config_list = $db->getAll($sql);
        $config = array();
        if ($get_all) { //CMS
            require_once ROOT_PATH . 'languages/' . $_CFG['lang'] . '/admin/shop_config.php';
            foreach ($config_list as $key => $item) {

                $item['name'] = isset($_LANG['cfg_name'][$item['code']]) ? $_LANG['cfg_name'][$item['code']] : $item['code'];
                $item['desc'] = isset($_LANG['cfg_desc'][$item['code']]) ? $_LANG['cfg_desc'][$item['code']] : '';
                if ($item['store_range']) {
                    $item['store_options'] = explode(',', $item['store_range']);

                    foreach ($item['store_options'] as $k => $v) {
                        $item['display_options'][$k] = isset($_LANG['cfg_range'][$item['code']][$v]) ?
                        $_LANG['cfg_range'][$item['code']][$v] : $v;
                    }
                }
                $config[] = $item;
            }
        } else { //comment page

            foreach ($config_list as $key => $value) {
                $config[$value['code']] = $value['value'];
            }
        }

        return $config;
    }

    public function update_comment_config($request)
    {

        global $db, $ecs, $_CFG, $_LANG;
        require_once ROOT_PATH . 'languages/' . $_CFG['lang'] . '/admin/shop_config.php';
        $type = empty($request['type']) ? '' : $request['type'];
        /* 允许上传的文件类型 */
        $allow_file_types = '|GIF|JPG|PNG|BMP|';

        /* 保存变量值 */
        $count = count($request['value']);

        $arr = array();
        $sql = 'SELECT id, value FROM ' . $ecs->table('shop_config') .
        " WHERE parent_id = (SELECT id FROM " . $ecs->table('shop_config') . " WHERE code = 'comment_config' and type = 'group' )";
        $res = $db->query($sql);
        while ($row = $db->fetchRow($res)) {
            $arr[$row['id']] = $row['value'];
        }
        foreach ($request['value'] as $key => $val) {
            if ($arr[$key] != $val) {
                $sql = "UPDATE " . $ecs->table('shop_config') . " SET value = '" . trim($val) . "' WHERE id = '" . $key . "'";
                $db->query($sql);
            }
        }

        /* 记录日志 */
        admin_log('', 'edit', 'comment_config');
        /* 清除缓存 */
        clear_all_files();
        $_CFG = load_config();
    }

    /**
     * 查询评论内容
     *
     * @access  public
     * @params  integer     $id
     * @params  integer     $type
     * @params  integer     $page
     * @return  array
     */
    public function assign_comment($id, $type, $page = 1, $lei = 1)
    {
        $url_format = '';
        $userController = new UserController();
        /* 取得评论列表 */
      $count = $GLOBALS['db']->getOne('SELECT COUNT(*) FROM ' . $GLOBALS['ecs']->table('comment') .
            " WHERE id_value = '$id' AND comment_type = '$type' AND status = 1 AND parent_id = 0  and lei='$lei' ");
        $size = !empty($GLOBALS['_CFG']['comments_number']) ? $GLOBALS['_CFG']['comments_number'] : 5;

        $page_count = ($count > 0) ? intval(ceil($count / $size)) : 1;

        $GLOBALS['db']->query('SET NAMES utf8mb4');
        $sql = 'SELECT * FROM ' . $GLOBALS['ecs']->table('comment') .
            " WHERE id_value = '$id' AND comment_type = '$type' AND status = 1 AND parent_id = 0 and lei='$lei'" .
            ' ORDER BY comment_id DESC';
        $res = $GLOBALS['db']->selectLimit($sql, $size, ($page - 1) * $size);

        $arr = array();
        $ids = '';
        while ($row = $GLOBALS['db']->fetchRow($res)) {
            $user = $userController->get_user_info($row['user_id']);
            $ids .= $ids ? ",$row[comment_id]" : $row['comment_id'];
            $arr[$row['comment_id']]['id'] = $row['comment_id'];
            $arr[$row['comment_id']]['email'] = $user['email'];

            $tel_prefix = strrpos($user['user_name'], "-");
            if ($tel_prefix === false) {
                $user['user_name'] = substr($user['user_name'], 0, 4);
                $user['user_name'] = $user['user_name'] . "***";
            } else {
                $user['user_name'] = substr($user['user_name'], 0, $tel_prefix + 4);
                $user['user_name'] = $user['user_name'] . "***";
            }
            $arr[$row['comment_id']]['username'] = $user['user_name'];
            $arr[$row['comment_id']]['display_name'] = $user['display_name'];
            $arr[$row['comment_id']]['user_rank'] = $user['user_rank'];
            $arr[$row['comment_id']]['user_id'] = $row['user_id'];
            $arr[$row['comment_id']]['content'] = str_replace('\r\n', '<br />', htmlspecialchars($row['content']));
            $arr[$row['comment_id']]['content'] = nl2br(str_replace('\n', '<br />', $arr[$row['comment_id']]['content']));
            $arr[$row['comment_id']]['image'] = $this->comment_image($row['image']);
            $arr[$row['comment_id']]['rank'] = $row['comment_rank'];
            $arr[$row['comment_id']]['add_time'] = local_date('Y-m-d', $row['add_time']);
            $arr[$row['comment_id']]['avatar'] = insert_avatar_url(['id' => $row['user_id'], 'size' => 'm']);

        }
        /* 取得已有回复的评论 */
        if ($ids) {
            $sql = 'SELECT * FROM ' . $GLOBALS['ecs']->table('comment') .
                " WHERE parent_id IN( $ids )";
            $res = $GLOBALS['db']->query($sql);
            while ($row = $GLOBALS['db']->fetch_array($res)) {
                $arr[$row['parent_id']]['re_content'] = nl2br(str_replace('\n', '<br />', htmlspecialchars($row['content'])));
                $arr[$row['parent_id']]['re_add_time'] = local_date('Y-m-d', $row['add_time']);
                $arr[$row['parent_id']]['re_email'] = $row['email'];
                $arr[$row['parent_id']]['re_username'] = $row['user_name'];
            }
        }
        /* 分页样式 */
        $_pagenum = 5; // 显示的页码
        $_offset = 2; // 当前页偏移值
        $_from = $_to = 0; // 开始页, 结束页
        if ($_pagenum > $page_count) {
            $_from = 1;
            $_to = $page_count;
        } else {
            $_from = $page - $_offset;
            $_to = $_from + $_pagenum - 1;
            if ($_from < 1) {
                $_to = $page + 1 - $_from;
                $_from = 1;
                if ($_to - $_from < $_pagenum) {
                    $_to = $_pagenum;
                }
            } elseif ($_to > $page_count) {
                $_from = $page_count - $_pagenum + 1;
                $_to = $page_count;
            }
        }
        $pager['styleid'] = isset($GLOBALS['_CFG']['page_style']) ? intval($GLOBALS['_CFG']['page_style']) : 0;
        $pager['page'] = $page;
        $pager['size'] = $size;
        $pager['record_count'] = $count;
        $pager['page_count'] = $page_count;
        $pager['page_first'] = "1";
        $pager['page_prev'] = $page > 1 ? $page - 1 : '1';
        $pager['page_next'] = $page < $page_count ? $page + 1 : '1';
        $pager['page_last'] = $page < $page_count ? $page_count : '1';
        $pager['page_number'] = array();
        for ($i = $_from; $i <= $_to; ++$i) {
            $pager['page_number'][$i] = $url_format . $i;
        }
        $comments = array_values($arr);

        // If comment is first/second, check rank, if rank < 2 : hide first.
        if ($count == 1 || $count == 2) {
            foreach ($comments as $key => $comment) {
                $rank = $comment['rank'];
                $user_id = $comment['user_id'];

                if ($rank < 3 && $user_id != $_SESSION['user_id']) {
                    unset($comments[$key]);
                    $count--;
                }
            }
        }

        $cmt = array('comments' => $comments, 'pager' => $pager, 'lei' => $lei, 'total' => $count);

        return $cmt;
    }

    public function getGoodsRatingInfo($goods_id)
    {
        global $ecs, $db;
        $sql= "select sum(comment_rank) as tnumber , count(*) as num from ".$GLOBALS['ecs']->table('comment')." where id_value='".$goods_id."'  AND status = 1 AND comment_type = '".self::COMMENT_TYPE_GOODS."' ";
        $row = $GLOBALS['db']->GetRow($sql);
        if ($row['num']>0)
        {
            $avg_rating = intval($row['tnumber'])/$row['num'];
            $avg_rating = sprintf("%01.1F", $avg_rating);
            if($row['num'] < 3 && $avg_rating <= 3) {
                $avg_rating = 0;
            }
        }
        else
        {
            $avg_rating = 0;
        }

        return [
            'avg_rating' => $avg_rating,
            'total'      => $row['num']
        ];
    }

    /**
     * Get review list from different table
     * 
     * @param integer $table 0: reivew_shop, 1: review_sales, 2: review_crm
     * @param string  $where conditions
     * 
     * @return array
     */
    public function getReviewList($table = 0)
    {
        global $ecs, $db, $_CFG;
        $where = self::getReviewCondition($table);
        $sort_by = empty($_REQUEST['sort_by']) ? "review_id" : $_REQUEST['sort_by'];
        $sort_order = empty($_REQUEST['sort_order']) ? "desc" : $_REQUEST['sort_order'];
        $start = empty($_REQUEST['start']) ? 0 : intval($_REQUEST['start']);
        $page_size = empty($_REQUEST['page_size']) ? 100 : intval($_REQUEST['page_size']);
        if ($table == self::REVIEW_SHOP_TABLE_ID) {
            $sql = "SELECT r.*, u.user_name, rei.content as display_name, o.order_sn ".
                    "FROM " . $ecs->table('review_shop') . " as r " .
                    "LEFT JOIN " . $ecs->table('users') . " as u ON r.user_id = u.user_id " .
                    "LEFT JOIN " . $ecs->table('reg_extend_info') . " as rei ON rei.user_id = u.user_id AND rei.reg_field_id = 10 " .
                    "LEFT JOIN " . $ecs->table('order_info') . " as o ON r.order_id = o.order_id " .
                    $where;
        } elseif ($table == self::REVIEW_SALES_TABLE_ID) {
            $sql = "SELECT r.*, u.user_name, rei.content as display_name, o.order_sn, au.user_id as admin_id, au.user_name as admin_name, au.avatar ".
                    "FROM " . $ecs->table('review_sales') . " as r " .
                    "LEFT JOIN " . $ecs->table('users') . " as u ON r.user_id = u.user_id " .
                    "LEFT JOIN " . $ecs->table('reg_extend_info') . " as rei ON rei.user_id = u.user_id AND rei.reg_field_id = 10 " .
                    "LEFT JOIN " . $ecs->table('order_info') . " as o ON r.order_id = o.order_id " .
                    "LEFT JOIN (SELECT sales_id, admin_id FROM " . $ecs->table("salesperson") . ") sp ON sp.sales_id = r.sales_id " .
                    "LEFT JOIN (SELECT user_id, user_name, avatar FROM " . $ecs->table("admin_user") . ") au ON au.user_id = sp.admin_id " .
                    $where;
        } elseif ($table == self::REVIEW_CRM_TABLE_ID) {
            $sql = "SELECT rc.*, u.user_name, rei.content as display_name, au.user_id as admin_id, au.user_name as admin_name, au.avatar FROM " . $ecs->table("review_crm") . " rc " .
                    "LEFT JOIN " . $ecs->table("users") . " u ON u.user_id = rc.user_id " .
                    "LEFT JOIN " . $ecs->table('reg_extend_info') . " as rei ON rei.user_id = u.user_id AND rei.reg_field_id = 10 " .
                    "LEFT JOIN " . $ecs->table("crm_twilio_call") . " ctc ON ctc.sent_evaluate = rc.review_id " .
                    "LEFT JOIN (SELECT user_id, user_name, avatar FROM " . $ecs->table("admin_user") . ") au ON au.user_id = ctc.admin_id " .
                    $where;
        }
        if (empty($sql)) {
            return [
                'data' => [],
                'record_count' => 0,
            ];
        }
        $sql .= "GROUP BY review_id " .
                "ORDER BY $sort_by $sort_order " .
                "LIMIT $start, $page_size";
        $res = $db->getAll($sql);
        foreach ($res as $key => $row) {
            if (empty($row['display_name'])) {
                $row['display_name'] = $row['user_name'];
            }
            if (!empty($row['order_id']) && !empty($row['order_sn'])) {
                $res[$key]['order_sn'] = "<a href='order.php?act=info&order_id=$row[order_id]'>$row[order_sn]</a>";
            }
            $res[$key]['user_name'] = "<a href='users.php?act=list&keywords=$row[user_name]'>$row[display_name]</a>";
            $res[$key]['score1'] = self::drawStars($row['score1']);
            $res[$key]['score2'] = self::drawStars($row['score2']);
            if (isset($row['score3'])) {
                $res[$key]['score3'] = self::drawStars($row['score3']);
            }
            if ($table != self::REVIEW_SHOP_TABLE_ID) {
                if (empty($row['avatar'])) {
                    $row['avatar'] = 'uploads/avatars/default_60x60.png';
                }
                $res[$key]['avatar'] = empty($row['admin_id']) ? "" : "<img class='task_avatar' src='/$row[avatar]' width='40'><br>$row[admin_name]";
            }
            if (in_array($table, [self::REVIEW_SHOP_TABLE_ID, self::REVIEW_SALES_TABLE_ID])) {
                $res[$key]['date'] = local_date($_CFG['date_format'], $row['time']);
            } elseif ($table == self::REVIEW_CRM_TABLE_ID) {
                $date = new \DateTime($row['complete']);
                $res[$key]['format_complete'] = $date->format($_CFG['date_format']);
            }

        }

        $sql_count = "SELECT COUNT(*) FROM " . $ecs->table(self::REVIEW_TABLES[$table]) . $where;
        $record_count = $db->getOne($sql_count);
        return [
            'data' => $res,
            'record_count' => $record_count,
        ];
    }

    /**
     * Get review average score from different table
     * 
     * @param integer $table 0: reivew_shop, 1: review_sales, 2: review_crm
     * @param string  $where conditions
     * 
     * @return array
     */
    public function getReviewAvgScore($table = 0)
    {
        global $db, $ecs;
        $where = self::getReviewCondition($table);
        if (in_array($table, [self::REVIEW_SHOP_TABLE_ID, self::REVIEW_SALES_TABLE_ID])) {
            $avg = $db->getRow("SELECT ROUND(NULLIF(AVG(NULLIF(score1, 0)), 0), 1) as score1, ROUND(NULLIF(AVG(NULLIF(score2, 0)), 0), 1) as score2, ROUND(NULLIF(AVG(NULLIF(score3, 0)), 0), 1) as score3 FROM " . $ecs->table(self::REVIEW_TABLES[$table]) . $where);
            $min_year = $db->getOne("SELECT MIN(DATE_FORMAT(FROM_UNIXTIME(time + 8 * 3600), '%Y')) FROM " . $ecs->table(self::REVIEW_TABLES[$table]));
            $result = [
                'avg'       => $avg,
                'min_year'  => $min_year,
                'items'     => self::REVIEW_TABLES_ITEMS[$table],
            ];
            if ($table == self::REVIEW_SALES_TABLE_ID) {
                $admin_avg = $db->getAll(
                    "SELECT sp.admin_id, au.user_name, au.avatar, ROUND(AVG(score1), 1) as score1, ROUND(AVG(score2), 1) as score2, ROUND(AVG(score3), 1) as score3 FROM " . $ecs->table("review_sales") . " r " .
                    "LEFT JOIN (SELECT sales_id, admin_id FROM " . $ecs->table("salesperson") . ") sp ON sp.sales_id = r.sales_id " .
                    "LEFT JOIN (SELECT user_id, user_name, avatar FROM " . $ecs->table("admin_user") . ") au ON au.user_id = sp.admin_id " .
                    $where . " AND sp.admin_id != 0 GROUP BY sp.admin_id"
                );
                $result['admin_avg'] = $admin_avg;
            }
        } elseif ($table == self::REVIEW_CRM_TABLE_ID) {
            $avg = $db->getRow("SELECT ROUND(AVG(score1), 1) as score1, ROUND(AVG(score2), 1) as score2 FROM " . $ecs->table("review_crm") . $where);
            $admin_avg = $db->getAll(
                "SELECT ctc.admin_id, au.user_name, au.avatar, ROUND(AVG(score1), 1) as score1, ROUND(AVG(score2), 1) as score2 FROM " . $ecs->table("review_crm") . " r " .
                "LEFT JOIN " . $ecs->table("crm_twilio_call") . " ctc ON ctc.sent_evaluate = r.review_id " .
                "LEFT JOIN (SELECT user_id, user_name, avatar FROM " . $ecs->table("admin_user") . ") au ON au.user_id = ctc.admin_id " .
                $where . " AND ctc.admin_id != 0 GROUP BY ctc.admin_id"
            );
            $min_year = $db->getOne("SELECT MIN(DATE_FORMAT(complete, '%Y')) FROM " . $ecs->table(self::REVIEW_TABLES[$table]));
            $result = [
                'avg'       => $avg,
                'admin_avg' => $admin_avg,
                'min_year'  => $min_year,
                'items'     => self::REVIEW_TABLES_ITEMS[$table],
            ];
        }
        if($result['admin_avg']) {
            $format_admin_avg = [];
            foreach ($result['admin_avg'] as $admin) {
                if ($table == self::REVIEW_SALES_TABLE_ID) $admin['total_avg'] = round(floatval(($admin['score1'] + $admin['score2'] + $admin['score3']) / 3), 2);
                elseif ($table == self::REVIEW_CRM_TABLE_ID)  $admin['total_avg'] = round(floatval(($admin['score1'] + $admin['score2']) / 2), 2);
                $format_admin_avg[$admin['admin_id']] = $admin;
            }
        }
        $result['admin_avg'] = $format_admin_avg;

        if (in_array($table, [self::REVIEW_SHOP_TABLE_ID, self::REVIEW_SALES_TABLE_ID])) $result['total_avg'] = round(floatval(($result['avg']['score1'] + $result['avg']['score2'] + $result['avg']['score3']) / 3), 2);
        elseif ($table == self::REVIEW_CRM_TABLE_ID) $result['total_avg'] = round(floatval(($result['avg']['score1'] + $result['avg']['score2']) / 2), 2);

        return $result;
    }

    /**
     * Get pre-defined review conditions
     * 
     * @param integer $table 0: reivew_shop, 1: review_sales, 2: review_crm
     * @param integer $year  target year
     * @param integer $month target month
     * 
     * @return array
     */
    function getReviewCondition($table = 0, $year = 0, $month = 0)
    {
        $where = "WHERE 1 ";
        $year = empty($year) ? (empty($_REQUEST['year']) ? 0 : $_REQUEST['year']) : $year;
        $month = empty($month) ? (empty($_REQUEST['month']) ? 0 : $_REQUEST['month']) : $month;
        $date = $this->getQarter($year, $month);
        $date_start = local_strtotime(!empty($date['start_date']) ? $date['start_date'] : local_date("Y-m-01"));
        $date_end = !empty($date['end_date']) ? local_strtotime($date['end_date']) : local_strtotime(local_date('Y-m-t 23:59:59')) ;
        if (in_array($table, [self::REVIEW_SHOP_TABLE_ID, self::REVIEW_SALES_TABLE_ID])) {
            if (!empty($year)) {
                $where .= "AND time BETWEEN $date_start AND $date_end ";
            }
        } elseif ($table == self::REVIEW_CRM_TABLE_ID) {
            $where .= "AND complete IS NOT NULL ";
            $date_start = date('Y-m-d 00:00:00', $date_start);
            $date_end = date('Y-m-d 23:59:59', $date_end);
            if (!empty($year)) {
                $where .= "AND complete BETWEEN '$date_start' AND '$date_end' ";
            }
        }
        return $where;
    }

    /**
     * Create stars for reivew list
     * 
     * @param integer $score score for a column
     * 
     * @return array
     */
    function drawStars($score)
    {
        if (empty($score)) {
            return "";
        }
        $add = $score;
        $minus = 10 - $score;
        $active = false;
        $div = "<div class='rating'><span class='rating-stars'>";
        while ($add > 0) {
            $div .= "<span></span>";
            $add--;
        }
        while ($minus > 0) {
            $div .= "<span" . (!$active ? " class='active'" : "") ."></span>";
            $active = true;
            $minus--;
        }
        $div .= "</span></div><div class='rating-score'><span class='score'>" . $score . "</span>分</div>";
        return $div;
    }
}
