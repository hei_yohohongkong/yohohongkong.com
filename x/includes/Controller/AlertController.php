<?php


/***
* AlertController.php
* 置頂公告
* by Anthony
*
* Alert controller
***/

namespace Yoho\cms\Controller;
use Yoho\cms\Model;
if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

class AlertController extends YohoBaseController
{
    private $tablename = 'global_alert';

    public function __construct(){
        parent::__construct();
        $this->tableName = 'global_alert';
    }

    public function buildForm($formMapper = [], $id = 0, $act = '', $assign = [], $redirect = '', $jsFile = '')
    {
        if(empty($formMapper))
        $formMapper = [
            'content' => ['label' => '公告內容'],
            'url' => ['label' => '公告連結'],
            'end_time' => ['label' => '公告結束時間', 'type' => 'date'],
            'create_at' => ['value' => local_strtotime('now'), 'type' => 'hidden'],
        ];
        if(!empty($id) && empty($this->editData)) {
			$dataModel       = new Model\YohoBaseModel($this->tableName, $id);
			$this->editData  = $dataModel->getData();
        }
        $this->editData['end_time'] = local_date('Y-m-d', $this->editData['end_time'] );
        parent::buildForm($formMapper, $id, $act, $assign, $redirect);
    }

    public function getAlert($num = 0)
    {
        global $ecs, $db, $_CFG;
        $now = local_strtotime('now');
        $today = local_date('Y-m-d', $now);
        $static_cache_name = 'global_alert_cache'.$today.$num.$_CFG['lang'];
        $res = read_static_cache($static_cache_name);
        if ($res === false) {
            $sql = "SELECT * FROM ".$ecs->table('global_alert')." WHERE end_time >= ".$now." ORDER BY create_at DESC ";
            if($num > 0) $sql .=" LIMIT $num ";

            $res = $db->getAll($sql);
            $res = localize_db_result('global_alert', $res);
            write_static_cache($static_cache_name, $res);
        }

        $list = array();
        foreach($res as $row) {
            $date = local_date('d.m.Y', $row['create_at']);
            $row['date'] = $date;
            $list[] = $row;
        }

        return $list;
    }

    public function ajaxQueryAction($list, $totalCount, $action_btn = ['edit', 'remove'], $extraData = [])
    {
        $list = $this->get_alertlist();
        // We're using dataTables Ajax to query.
        $this->tableList       = $list['alert'];
        $this->tableTotalCount = $list['record_count'];

        parent::ajaxQueryAction();
    }

    public function get_alertlist()
    {
        /* 分页大小 */
        $filter = $_REQUEST;

        /* 记录总数以及页数 */
        $sql = "SELECT COUNT(*) FROM ".$GLOBALS['ecs']->table('global_alert');
        $filter['record_count'] = $GLOBALS['db']->getOne($sql);

        /* 查询记录 */
        $sql = "SELECT * FROM ".$GLOBALS['ecs']->table('global_alert')." ORDER BY ".$_POST['sort_by']." ".$_POST['sort_order'];
        $res = $GLOBALS['db']->selectLimit($sql, $filter['page_size'], $filter['start']);

        $arr = array();
        while ($rows = $GLOBALS['db']->fetchRow($res)) {
            $rows['end_time_format'] = local_date('Y-m-d H:i:s', $rows['end_time']);
            $arr[] = $rows;
        }

        return array('alert' => $arr, 'filter' => $filter, 'page_count' => $filter['page_count'], 'record_count' => $filter['record_count']);
    }
}