<?php

/***
* ctrl_salesperson.php
* by MichaelHui 20170328
*
* Salesperson system - the controller
***/
namespace Yoho\cms\Controller;
use Yoho\cms\Model;
if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

class AdminuserController extends YohoBaseController{
	const DB_ACTION_CODE_SUPERADMIN='super_admin';
	const DB_ACTION_CODE_ONLYSELFSUPPLIERS='only_self_suppliers';
	const DB_ACTION_CODE_ASSIGNABLE_PREORDER_OP='assignable_preorder_operator';	//預售登記 - 可被指派為「操作員」'
	const DB_ACTION_CODE_ASSIGNABLE_OUTOFSTOCK_FIRSTOP = 'assignable_outofstock_firstop';//'缺貨登記 - 可被指派為「首次操作員」'; //189
	const DB_ACTION_CODE_ASSIGNABLE_OUTOFSTOCK_RECOP = 'assignable_outofstock_recop';//'缺貨登記 - 可被指派為「到貨操作員」';
	const DB_ACTION_CODE_ASSIGNABLE_REFUND_PIC = 'assignable_refund_pic';//退款及積分申請 - 可被指派為「跟進人」';
	const DB_ACTION_CODE_ASSIGNABLE_REFUND_APP = 'assignable_refund_app';//退款及積分申請 - 可被指派為「申請人」';
	const DB_ACTION_CODE_ASSIGNABLE_PO_OP = 'assignable_po_op';//採購訂單列表 - 添加新定單 > 操作員
	const DB_ACTION_CODE_PO_UNAPPROVE= 'erp_order_unapprove';
	const DB_ACTION_CODE_ASSIGNABLE_WAREHOUSE_OP = 'assignable_warehouse_op'; //入貨訂單 - 編輯入貨單 - 操作員
	const DB_ACTION_CODE_ASSIGNABLE_ORDERMSG = 'assignable_order_msg';
	const DB_ACTION_CODE_ASSIGNABLE_PAYMENT_OP = 'assignable_payment_op'; //新增交易, 新增轉帳, 採購訂單付款 - 操作員
	const DB_ACTION_CODE_ASSIGNABLE_POSTING_OP = 'assignable_posting_op'; //審核會計紀錄 - 操作員
	const DB_ACTION_CODE_STAFF_PURCHASE='staff_purchase';
	const STAFF_PURCHASE_STATUS_WAIT    = 0;
	const STAFF_PURCHASE_STATUS_SUCCESS = 1;
	const STAFF_PURCHASE_STATUS_FAIL    = 2;
	const STAFF_PURCHASE_STATUS = [
		self::STAFF_PURCHASE_STATUS_WAIT => '等待審核',
		self::STAFF_PURCHASE_STATUS_SUCCESS => '審核成功',
		self::STAFF_PURCHASE_STATUS_FAIL => '審核失敗'
	];
	const ACCOUNTING_TEAM_ROLE_IDS = [8, 16];
	private $tablename='admin_user';

	public function __construct(){
		parent::__construct();
		$this->tablename=$this->ecs->table($this->tablename);
	}

	public function prepareFormData($userId=null){
		$out=[];
		if($userId>0){
			$u = new Model\Adminuser($userId);
		}
		foreach(Model\Adminuser::fillable as $key){
			if(in_array($key,['role_id','user_name','email'])) continue;
			$type="input";
			if(in_array($key, ['address','emergency_contact'])){
				$type="textarea";
			}elseif(in_array($key, ['avatar'])){
				$type="file";
				if(isset($u->data) && $u->data['avatar']!='')
					$u->data['avatar']=FileController::getImgUrl($u->data['avatar'],true);
			}elseif(in_array($key, ['dob','joined_date'])){
				$type="date";
			}elseif(in_array($key, ['is_receive_stock_alert', 'is_disable'])){
				$type="checkbox";
			}
			$out[$key]=[
				'title'=>ucwords(str_replace('_', ' ', $key)),
				'type'=>$type,
				'value'=>(isset($u->data)?$u->data[$key]:''),
			];
		}
		return $out;
	}

	public function updateFormData($userId,$data){
		$params=[];
		$u = new Model\Adminuser($userId);
		if($u==false || $u->getId()<=0) return false;

		//avatar first
		if(isset($_FILES['avatar']) && $_FILES['avatar']!=''){
			$img=new FileController(FileController::FILE_TYPE_IMAGE,$_FILES['avatar']);
			$imgres = $img->validate();
			if($imgres['status']!==false){
				$imgres = $img->upload(['makeThumb'=>true,'imgName'=>'admin_user_'.$userId]);
				if($imgres['status']!==false)
					$data['avatar']=$imgres['data'];
				else
					unset($data['avatar']);
			}
		}
		$data_keys = array_keys($data);
		foreach(Model\Adminuser::fillable as $key){
			if(in_array($key,['role_id','user_name','email'])) continue;
			if(!in_array($key,$data_keys)) continue;
			if(!in_array($key,['is_receive_stock_alert'])) $data[$key]="'".$data[$key]."'";
			else $data[$key]=intval($data[$key]);
			$params[]=$key.'='.$data[$key];
		}

		if(count($params)==0) return true;
		$q = "UPDATE ".$this->tablename." SET ".implode(',',$params)." WHERE user_id=".$u->getId();


		return $this->db->query($q);
	}

	public function updateRole($userId,$newRoleId){
		//This function is not aimed to just update the role_id in admin_users
		//Instead it handle salesperson status

		$u = new Model\Adminuser($userId);
		$spController = new SalespersonController();
		$sp = $spController->getByAdmin($u->getId());
		$roleController = new RoleController();
		$autoSpIds = $roleController->getRoleIdAutocreatesp();
		if($u->data['role_id']>0){		
			//skip if no change at all
			if($newRoleId == $u->data['role_id'])
				return true;

			$result = false;
			if(in_array($u->data['role_id'], $autoSpIds) && !in_array($newRoleId, $autoSpIds)){
				//deactivete sp account
				$spController->deactivate($sp->getId());
				$result = true;
			}elseif(!in_array($u->data['role_id'], $autoSpIds) && in_array($newRoleId, $autoSpIds)){
				//activate or create sp account
				if($sp==false){
					if($spController->createFromAdmin($u))
						$sp = $spController->getByAdmin($u->getId());
					if($sp!=false) $result=true;
				}elseif($sp->getId()>0){
					$result = $spController->activate($sp->getId());
				}
			}elseif(!in_array($u->data['role_id'], $autoSpIds) && !in_array($newRoleId, $autoSpIds)){
				$result = true;
			}else{
				if($sp && $sp->getId()>0){
					//in case the new role is removed from "auto create sales person", but sp created previously
					$result = true;
				}else{
					$result = false;	
				}
			}

			if($result==true){
				if($sp && $sp->getId()>0)admin_log($sp->getId().'-'.$sp->data['sales_name'],'role_change','role_change_target');
				admin_log($newRoleId,'role_change','role_change_to');	
			}else{
				admin_log($userId,'role_change','FAILED');
			}
			
			return $result;
		}else{
			if($newRoleId==0){
				return true;
			}elseif(in_array($newRoleId, $autoSpIds)){
				if($spController->createFromAdmin($u))
						$sp = $spController->getByAdmin($u->getId());
				if($sp!=false) return true;
			}else{
				return true;
			}
			return false;
		}
	}

	/**
	 * Get admin users by privilege
	 * @param array  $priv_arr			array of privileges
	 * @param string $user_name_alias	optional alias
	 * @param string $queries			optional query
	 * @param string $self_only			only current admin
	 * @param string $accountant		also allow accountant
	 * @param string $priv_cond			privilege match all or one (AND / OR)
	 * @return array admin users
	 */
	public function getAdminsByPriv($priv_arr = [], $user_name_alias = "user_name", $queries = [], $self_only = false, $accountant = false, $priv_cond = "AND"){
		$query = "";
		foreach ($queries as $key => $value) {
			$query .= ", $key as $value";
		}
		$sql = "SELECT user_name as $user_name_alias $query FROM " . $GLOBALS['ecs']->table('admin_user') . " u LEFT JOIN " . $GLOBALS['ecs']->table('role') . " r ON r.role_id = u.role_id WHERE u.is_disable = 0 " . ($self_only ? "AND user_id = $_SESSION[admin_id] " : "");
		if(!empty($priv_arr)){
			$priv_arr = is_array($priv_arr) ? $priv_arr : array($priv_arr);
			$where = implode(" $priv_cond ", array_map(function ($priv){
				return "(u.action_list LIKE '%" . $priv . "%' OR r.action_list LIKE '%" . $priv . "%') ";
			}, $priv_arr));
			$where = $accountant ? ("($where) OR (u.role_id " . db_create_in(self::ACCOUNTING_TEAM_ROLE_IDS) . ") ") : $where;
			$sql .= "AND $where";
		}
		$admins = empty($query) ? $GLOBALS['db']->getCol($sql) : $GLOBALS['db']->getAll($sql);
		$admins = empty($admins) ? [] : $admins;
		if ($_SESSION['manage_cost'] == 1) {
			$no_admin = true;
			foreach ($admins as $key => $row) {
				$no_admin = $row[$user_id_alias] != $_SESSION['admin_id'] && $no_admin;
			}
			if ($no_admin) {
				$no_sql = "SELECT user_name as $user_name_alias $query FROM " . $GLOBALS['ecs']->table('admin_user') . " u LEFT JOIN " . $GLOBALS['ecs']->table('role') . " r ON r.role_id = u.role_id WHERE u.is_disable = 0 AND u.user_id = $_SESSION[admin_id]";
				$manage_cost_admin = empty($query) ? $GLOBALS['db']->getOne($no_sql) : $GLOBALS['db']->getRow($no_sql);
				array_unshift($admins, $manage_cost_admin);
			}
		}
		return $admins;
	}

	public function get_user($uid){
		global $db, $ecs;

		$q = "SELECT user_id,user_name,email FROM ".$ecs->table('admin_user')." WHERE user_id='".$uid."'";
		$result = $this->db->getRow($q);
		return $result;
	}

	public function get_user_list(){
		global $db, $ecs;

		$q = "SELECT user_id,user_name,email,avatar FROM ".$ecs->table('admin_user')." WHERE 1";
		$result = $this->db->getAll($q);
		return $result;
	}


	public function getSalesperson()
	{
		global $db, $ecs;
		$sql = "SELECT sales_name FROM " . $ecs->table('salesperson') . " WHERE available = 1";
		return $db->getCol($sql);
	}

	public function ajaxQueryStaffPurchaseAction()
	{
		$show_all = true;
		if ($_SESSION['action_list'] == 'all')
		{
			$is_admin = true;
		} else if(strpos(',' . $_SESSION['action_list'] . ',', ',' . self::DB_ACTION_CODE_STAFF_PURCHASE . ',') === false) {
			$is_admin = false;
		} else {
			$is_admin = true;
		}

		$list = $this->get_staff_purchase_list($is_admin);
        parent::ajaxQueryAction($list['list'], $list['record_count'], false);
	}

	function get_staff_purchase_list($is_admin = false)
	{
		global $db, $ecs;
		$where = " WHERE 1";
		if(!$is_admin) $where .= " AND applicant_admin = '".$_SESSION['admin_id']."' ";

		/* Get list count */
		$sql = "SELECT COUNT(*) FROM ".$GLOBALS['ecs']->table('staff_purchase').$where;
		$record_count = $db->getOne($sql);

		/* Get list */
		$sql = "SELECT sp.*, au.user_name as applicant_admin_name, au2.user_name as apply_admin_name FROM ".$ecs->table('staff_purchase')." as sp ".
		" LEFT JOIN ".$ecs->table('admin_user')." as au ON au.user_id = sp.applicant_admin ".
		" LEFT JOIN ".$ecs->table('admin_user')." as au2 ON au2.user_id = sp.apply_admin ".
		$where." ORDER BY ".$_REQUEST['sort_by']." ".$_REQUEST['sort_order']." , sp.status ASC " ;
		$res = $db->selectLimit($sql, $_REQUEST['page_size'], $_REQUEST['start']);
		$list = array();
        while ($rows = $db->fetchRow($res)) {

			$custom_actions = [];
			$rows['status_string'] = self::STAFF_PURCHASE_STATUS[$rows['status']];
			$rows['create_date_format']   = local_date('Y-m-d H:i', $rows['create_at']);
			$view_btn = [
				'link'  => "staff_purchase.php?act=view&id=".$rows['purchase_id'],
				'title' => '查看',
				'label' => '查看',
				'btn_class' => 'btn btn-primary',
				'icon_class' => 'fa fa-eye'
			];
			$custom_actions['view'] = $view_btn;
			if($is_admin && $rows['status'] != self::STAFF_PURCHASE_STATUS_SUCCESS) {
				$approve_btn = [
					'link'  => "staff_purchase.php?act=approve&id=".$rows['purchase_id'],
					'title' => '審核',
					'label' => "審核",
					'btn_class' => 'btn btn-success',
					'icon_class' => 'fa fa-check'
				];
				$custom_actions['approve'] = $approve_btn;
			}
			$rows['_action'] = $this->generateCrudActionBtn($rows['purchase_id'], 'id', null, [], $custom_actions);
            $list[] = $rows;
		}

		return ['list'=>$list, 'record_count'=>$record_count];
	}

	public function applyStaffPurchase($data)
	{
		global $db, $ecs;
		$goods_ids = $data['target_select'];
		if(empty($goods_ids)) return false;
		/* Create Staff Purchase form */
		$form = [];
		$form['applicant_admin'] = $_SESSION['admin_id'];
		$form['status']          = self::STAFF_PURCHASE_STATUS_WAIT;
		$form['create_at']       = local_strtotime('now');
		$model = new Model\YohoBaseModel('staff_purchase');
		$purchase_id = $model->insert($form);

		/* Insert goods */
		$model = new Model\YohoBaseModel('staff_purchase_goods');
		foreach($goods_ids as $goods_id) {
			$goods_info = [];
			$goods_info['purchase_id']  = $purchase_id;
			$goods_info['goods_id']     = $goods_id;
			$goods_info['price']        = 0;
			$goods_info['goods_number'] = 1;
			$goods_info['type']         = 0;
			$goods_info['percentage']   = 0;
			$goods_info['create_at']    = local_strtotime('now');
			$model->insert($goods_info);
		}

		return true;
	}

	public function getStaffPurchase($purchase_id, $approve = 1)
	{
		global $db, $ecs;
		if(empty($purchase_id)) return false;
		$sql = "SELECT sp.*,  au.user_name as applicant_admin_name, au2.user_name as apply_admin_name FROM ".$ecs->table('staff_purchase')." as sp ".
		" LEFT JOIN ".$ecs->table('admin_user')." as au ON au.user_id = sp.applicant_admin ".
		" LEFT JOIN ".$ecs->table('admin_user')." as au2 ON au2.user_id = sp.apply_admin ".
		" WHERE sp.purchase_id = ".$purchase_id;
		$staff_purchase = $db->getRow($sql);
		$staff_purchase['status_string'] = self::STAFF_PURCHASE_STATUS[$staff_purchase['status']];
		$staff_purchase['create_date_format']   = local_date('Y-m-d H:i', $staff_purchase['create_at']);

		$sql = "SELECT spg.*,  g.* FROM ".$ecs->table('staff_purchase_goods')." as spg ".
		" LEFT JOIN ".$ecs->table('goods')." as g ON spg.goods_id = g.goods_id ".
		" WHERE spg.purchase_id = ".$purchase_id;
		$staff_purchase_goods = $db->getAll($sql);
		if($approve) {
			require_once(ROOT_PATH . 'includes/lib_goods.php');
			foreach($staff_purchase_goods as $key => $goods) {
				if ($goods['promote_price'] > 0) {
					$promote_price = bargain_price($goods['promote_price'], $goods['promote_start_date'], $goods['promote_end_date']);
				} else {
					$promote_price = 0;
				}
				if($promote_price > 0) $goods['selling_price'] = min($goods['shop_price'], $promote_price);
				else $goods['selling_price'] = $goods['shop_price'];
				$staff_purchase_goods[$key] = $goods;
			}
		}
		$staff_purchase['goods'] = $staff_purchase_goods;
		$status = $staff_purchase['status'];

		return $staff_purchase;
	}

	public function approveStaffPurchase($data)
	{
		global $db, $ecs;
		$list = $data['crp_price'];
		$goodsController = new GoodsController();
		foreach($list as $goods_id => $crp_type) {
			$crp = ['srp_percentage' => $data['srp_percentage'][$goods_id], 'crp_type' => $crp_type];
			$sp_price = $goodsController->corporatePriceSuggestion([], $goods_id, $crp, 1);
			if($sp_price == 0) {
				$crp_type = 4; // 當前售價
				$crp = ['srp_percentage' => $data['srp_percentage'][$goods_id], 'crp_type' => $crp_type];
				$sp_price = $goodsController->corporatePriceSuggestion([], $goods_id, $crp, 1);
			}
			$sql = "UPDATE ".$ecs->table('staff_purchase_goods')." SET price = '".$sp_price."', type = '$crp_type', percentage = '".intval($data['srp_percentage'][$goods_id])."' WHERE purchase_goods_id = ".intval($data['purchase_goods'][$goods_id]);
			$db->query($sql);
		}
		/* Update status and admin */
		$apply_admin = $_SESSION['admin_id'];
		$remark = $data['remark'];
		$status = self::STAFF_PURCHASE_STATUS_SUCCESS;
		$apply_time = local_strtotime('now');
		$sql = "UPDATE ".$ecs->table('staff_purchase').
		" SET apply_admin = '".$apply_admin."', remark = '$remark', status = '".$status."', apply_time = '$apply_time' WHERE purchase_id = ".intval($data['purchase_id']);
		$db->query($sql);

		return true;
	}

	public function get_user_role($uid){
		global $db, $ecs;

		$q = "SELECT role_id FROM ".$ecs->table('admin_user')." WHERE user_id='".$uid."'";
		$result = $this->db->getOne($q);
		return $result;
	}

	function getTeamRoleUserList($team)
	{
		global $ecs, $db;
		$sql = "SELECT role_id FROM ".$ecs->table('role')." WHERE team_code = '$team' GROUP BY role_id ";
		$data = $db->getCol($sql);
		if(empty($data)) {
			$sql = "SELECT role_id FROM ".$ecs->table('role')." ";
			$data = $db->getCol($sql);
		}

		$sql = "SELECT user_id as admin_id, user_name as admin_name  FROM ".$ecs->table('admin_user')." WHERE role_id ". db_create_in($data)." AND is_disable = 0 ";
		$res = $db->getAll($sql);
		return $res;
	}

	public function getTeamRevence($team, $date, $all_sales = false)
	{
		global $ecs, $db;
		include_once ROOT_PATH . 'includes/lib_order.php';
		$userController = new UserController();
		$admin_list = $this->getTeamRoleUserList($team);
		$salespersons = array_column($admin_list, 'admin_name');
		$date_start = local_strtotime(!empty($date['start_date']) ? $date['start_date'] : local_date("Y-m-01"));
		$date_end = !empty($date['end_date']) ? local_strtotime($date['end_date']) : local_strtotime(local_date('Y-m-t 23:59:59')) ;

		$sql = "SELECT salesperson, COUNT(*) as orders, SUM(total_amount) as volume, AVG(goods_count) as avg_goods, sum(goods_count) as total_goods " .
			"FROM (" .
			"SELECT oi.`salesperson`, (oi.`money_paid` + oi.`order_amount`) as total_amount, sum(og.goods_number) as goods_count " .
			"FROM " . $ecs->table('order_info') . " as oi " .
			"LEFT JOIN " . $ecs->table('order_goods') . " as og ON oi.`order_id` = og.`order_id` " .
			"WHERE 1 " . order_query_sql('finished', 'oi.') .
			"AND oi.shipping_time >= '" . $date_start . "' AND oi.shipping_time < '" . $date_end . "' " .
			// Exclude orders for the followiung user accounts: 11111111, 88888888, 99999999
			"AND (oi.user_id NOT IN (".implode(',',$userController->getWholesaleUserIds()).") AND (oi.order_type <> '".OrderController::DB_ORDER_TYPE_WHOLESALE."' OR oi.order_type IS NULL))" .
            ($all_sales ? " AND (oi.order_type <> '" . OrderController::DB_ORDER_TYPE_SALESAGENT . "' OR oi.order_type IS NULL) ".
                " AND (oi.platform = 'pos') " : " ").
			"GROUP BY oi.`order_id` " .
			") as tmp " .
            ($all_sales ? " " : "WHERE `salesperson`  ".db_create_in($salespersons)." ").
			"GROUP BY `salesperson` ";
		$res = $db->getAll($sql);
		$evaluation_data = [];
		$evaluation_data['all'] = [
			'salesperson' => '所有',
			'orders' => 0,
			'volume' => 0,
			'avg_goods' => 0
		];
		foreach ($res as $row)
		{
			$key = ucwords($row['salesperson']);
			$row['avg_goods'] = round(doubleval($row['avg_goods']), 2);
			$evaluation_data['all']['orders'] += $row['orders'];
			$evaluation_data['all']['volume'] += $row['volume'];
			$evaluation_data['all']['total_goods'] += $row['total_goods'];
			$evaluation_data[$key] = $row;
		}
		$evaluation_data['all']['avg_goods'] = round(doubleval($evaluation_data['all']['orders'] / $evaluation_data['all']['total_goods']), 2);
		return $evaluation_data;
	}
}