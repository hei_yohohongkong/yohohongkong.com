<?php

/* * *
 * QuotationController.php
 * by Anthony 20170410
 *
 * Quotation controller
 * * */

namespace Yoho\cms\Controller;

if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

class QuotationController extends YohoBaseController {

	public function get_quotation_list() {
		global $db, $ecs;

		$filter['sort_by'] = empty($_REQUEST['sort_by']) ? 'created_at' : trim($_REQUEST['sort_by']);
		$filter['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
		$sql = "SELECT count(*) FROM " . $ecs->table('quotation_info');
		$filter['record_count'] = $db->getOne($sql);

		/* Paging */
		$filter = page_and_size($filter);

		$sql = "SELECT * " .
						"FROM " . $ecs->table('quotation_info') . " " .
						"ORDER BY " . $filter['sort_by'] . ' ' . $filter['sort_order'] . ' ' .
						"LIMIT " . $filter['start'] . "," . $filter['page_size'];
		$data = $db->getAll($sql);

		foreach ($data as $key => $row) {
			$data[$key]['quotation_price_formatted'] = price_format($row['quotation_fee'], false);
			$data[$key]['created_at_formatted'] = local_date($GLOBALS['_CFG']['date_format'], $row['created_at']);
		}
		$arr = array('data' => $data, 'filter' => $filter, 'page_count' => $filter['page_count'], 'record_count' => $filter['record_count']);
		return $arr;
	}

	/* get quotation with goods item */

	public function get_quotation($id) {

		$sql = "SELECT qg.* , IFNULL(b.brand_name, '') AS brand_name , g.goods_thumb , g.brand_id, g.goods_sn, g.cost, " .
						"qg.goods_price * qg.goods_number AS subtotal " .
						"FROM " . $GLOBALS['ecs']->table('quotation_goods') . " as qg " .
						"LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON g.goods_id = qg.goods_id " .
						"LEFT JOIN " . $GLOBALS['ecs']->table('brand') . " as b ON g.brand_id = b.brand_id " .
						"WHERE qg.quotation_id = '" . $id . "' ";

		$arr = $GLOBALS['db']->getAll($sql);

		/* Set quotation goods format */
		foreach ($arr as $key => $value) {
			$arr[$key]['formated_cost'] = price_format($value['cost'], false);
			$arr[$key]['formated_goods_price'] = price_format($value['goods_price'], false);
			$arr[$key]['formated_subtotal'] = price_format($value['subtotal'], false);

			$arr[$key]['goods_thumb'] = get_image_path($value['goods_id'], $value['goods_thumb'], true); //ly sbsn 增加订单中图片显示
			$arr[$key]['url'] = build_uri('goods', array('gid' => $value['goods_id'], 'gname' => $value['goods_name']), $value['goods_name']);
		}
		return $arr;
	}

	public function view_pdf($sn) {

		//pdf Setting
		$pdfname = 'quotation_' . $sn . '.pdf';
		$file = ROOT_PATH . DATA_DIR . '/quotationpdf/' . $pdfname;

		header("Content-type: application/pdf"); // add here more headers for diff. extensions
		header("Content-Disposition: inline; filename=" . urlencode($file));

		flush(); // this doesn't really matter.
		$fp = fopen($file, "r");

		while (!feof($fp)) {
			echo fread($fp, 65536);
			flush(); // this is essential for large downloads
		}
		fclose($fp);
	}

}
