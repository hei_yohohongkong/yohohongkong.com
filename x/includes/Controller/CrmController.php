<?php

namespace Yoho\cms\Controller;
use Yoho\cms\Model;
if (!defined('IN_ECS')) {
    die('Hacking attempt');
}


class CrmController extends YohoBaseController
{
    CONST KEY_FILE_LOCATION =  ROOT_PATH . 'data/yoho_service_account.json';
    CONST SECRET_KEY_LOCATION = ROOT_PATH . 'data/yoho_crm_account.json';
    CONST CHATRA_PUBLIC_KEY = "ehYf6fARrNGzkr7tr";
    CONST AIRCALL_PUBLIC_KEY = "0647b80f6f122ef8063460ea308aa03f";
    CONST GMAIL_USER = 'info@yohohongkong.com';
    CONST IVR_LANG_CANTONESE = 1;
    CONST IVR_LANG_ENGLISH   = 2;
    CONST IVR_LANG_MANDARIN  = 3;
    CONST IVR_LANG_CONFIG = [
        self::IVR_LANG_CANTONESE  => ["voice" => "alice", "language" => "zh-HK"],
        self::IVR_LANG_ENGLISH    => ["voice" => "alice", "language" => "en-US"],
        self::IVR_LANG_MANDARIN   => ["voice" => "alice", "language" => "zh-CN"],
    ];
    CONST IVR_LANG_NAME = [
        self::IVR_LANG_CANTONESE  => "廣東話",
        self::IVR_LANG_ENGLISH    => "English",
        self::IVR_LANG_MANDARIN   => "普通話",
    ];

    public function getUsefulHeaders($headers)
    {
        $result = [];
        foreach ($headers as $row) {
            if (in_array($row['name'], ['Subject', 'Message-ID'])) {
                $result[$row['name']] = $row['value'];
            } elseif ($row['name'] == 'From') {
                if (preg_match("/<(.*)>/", $row['value'], $name)) {
                    $result['From_mail'] = $name[1];
                    $result['From_user'] = trim(str_replace([$name[0], '"'], "", $row['value']));
                } else {
                    $result['From_mail'] = $row['value'];
                }
            } elseif ($row['name'] == 'Date') {
                try {
                    $date = new \DateTime($row['value']);
                } catch (\Exception $e) {
                    $date = new \DateTime(explode("(", $row['value'])[0]);
                }
                $date = $date->setTimezone(new \DateTimeZone('Asia/Hong_Kong'));
                $result['Format_Date'] = $date->format("Y-m-d H:i:s");
                $today = new \DateTime("today");
                $result['Date'] = $today > $date ? $date->format("Y-m-d H:i") : $date->format("H:i");
            }
        }
        if (preg_match("/bizalert\.([0-9.])+@informationservices\.hsbc\.com\.hk/", $result['From_mail']) || preg_match("/dbb\.notificationcentre\.([0-9.])+@webtrack4\.ems\.hsbc\.com\.hk/", $result['From_mail'])) {
            $result['Payment'] = "HSBC";
        } elseif (preg_match("/commercial\.payment\.and\.transfer\.([0-9.])+@mailnotification\.hsbc\.com\.hk/", $result['From_mail'])) {
            $result['Payment'] = "HSBC-FPS";
        } elseif (preg_match("/business_e-alert\.([0-9.])+@mail\.hangseng\.com/", $result['From_mail'])) {
            $result['Payment'] = "HS";
        } elseif (preg_match("/(service|member)@paypal\.com\.hk/", $result['From_mail'])) {
            $result['Payment'] = "PAYPAL";
        } elseif (preg_match("/e-alert\.([0-9.])+@mailnotification\.hangseng\.com/", $result['From_mail'])) {
            $result['Payment'] = "FPS";
        } elseif (preg_match("/dem@yohohongkong\.com/", $result['From_mail']) && strpos($_SERVER['SERVER_NAME'], 'beta.') !== false && (strpos($result['Subject'], 'Inward') !== false || strpos($result['Subject'], '入賬通知') !== false)) {
            $result['Payment'] = "TEST";
        }
        return $result;
    }

    public function createTicket($unique_id, $ref_id, $platform, $sender, $create_time)
    {
        global $ecs, $db;
        $ticket_id = $db->getOne("SELECT ticket_id FROM " . $ecs->table('crm_list') . " WHERE unique_id = '$unique_id'");
        if (empty($ticket_id)) {
            $sql = "SELECT ticket_id FROM " . $ecs->table('crm_ticket_info') . " WHERE platform = $platform AND ref_id = '$ref_id'";
            $ticket_id = $db->getOne($sql);
            if (empty($ticket_id)) {
                $db->query("INSERT INTO " . $ecs->table('crm_ticket_info') . " (ref_id, platform, last_update) VALUES ('$ref_id', $platform, '$create_time')");
                $ticket_id = $db->insert_id();
            } else {
                $db->query("UPDATE " . $ecs->table('crm_ticket_info') . " SET last_update = '$create_time' WHERE ticket_id = $ticket_id");
            }
            $sql = "INSERT INTO " . $ecs->table('crm_list') . " (ticket_id, unique_id, sender, create_at) VALUES ($ticket_id, '$unique_id', '$sender', '$create_time')";
            $db->query($sql);
            $list_id = $db->insert_id();
            $admin_id = empty($_SESSION['admin_id']) ? 1 : $_SESSION['admin_id'];
            $db->query("INSERT INTO " . $ecs->table('crm_log') . " (ticket_id, list_id, admin_id, type) VALUES ($ticket_id, $list_id, $admin_id, " . CRM_LOG_TYPE_INSERT . ")");
        }
        return $ticket_id;
    }

    public function offlinePayment($ticket_id, $order_id, $amount, $op_id = 0, $manual = false, $actg_payment_method = 0)
    {
        global $ecs, $db, $actgHooks;
        include_once ROOT_PATH . ADMIN_PATH . '/includes/lib_accounting.php';
        $order = order_info($order_id);
        if ((empty($ticket_id) && !$manual) || empty($order_id) || empty($amount) || !$order || ($manual && empty($actg_payment_method))) {
            return 0;
        }
        if ($order['order_status'] != OS_CONFIRMED && $order['order_status'] != OS_SPLITED && $order['order_status'] != OS_SPLITING_PART) {
            $arr['order_status'] = OS_CONFIRMED;
            $arr['confirm_time'] = gmtime();
        }
        $arr['pay_status']  = PS_PAYED;
        $arr['pay_time']    = gmtime();
        $arr['money_paid']  = $order['money_paid'] + $amount;
        $arr['order_amount']= $order['order_amount'] - $amount;
        update_order($order_id, $arr);
        $actgHooks->orderPaid($order_id, $amount, "", $actg_payment_method, false, ($manual ? (empty($_SESSION['admin_id']) ? 1 : $_SESSION['admin_id']) : -2));
        $order = order_info($order_id);

        if ($manual) {
            $admin = empty($_SESSION['admin_name']) ? "unknown" : $_SESSION['admin_name'];
            $remark = "[系統備註]手動標籤已付款" . (empty($ticket_id) ? "" : " (#$ticket_id)");
        } else {
            $admin = "Yoho Bravo 1";
            $remark = "[系統備註]自動標籤已付款 (#$ticket_id)";
        }
        order_action($order['order_sn'], $order['order_status'], $order['shipping_status'], PS_PAYED, $remark, $admin);
        if (!empty($ticket_id)) {
        if ($db->query("UPDATE " . $ecs->table('crm_ticket_info') . " SET order_id = $order_id, type = 3, status = " . CRM_STATUS_FINISHED . ", last_update = CURRENT_TIMESTAMP WHERE ticket_id = $ticket_id")) {
            $db->query("INSERT INTO " . $ecs->table('crm_log') . " (ticket_id, admin_id, type) VALUES ($ticket_id, 1, " . CRM_LOG_TYPE_UPDATE . ")");
        }
        }

        if (!empty($op_id)) {
            $db->query("UPDATE " . $ecs->table("offline_payment") . " SET status = " . OFFLINE_PAYTMENT_FINISHED . " WHERE op_id = $op_id");
        }
        $db->query("UPDATE " . $ecs->table("offline_payment") . " SET status = " . OFFLINE_PAYTMENT_CLOSED . " WHERE order_id = $order_id AND status < " . OFFLINE_PAYTMENT_FINISHED);
        return 1;
    }

    public function getFullList()
    {
        include_once ROOT_PATH . "languages/zh_tw/admin/crm.php";
        global $db, $ecs, $_LANG;
        $ticket_id = empty($_REQUEST['ticket_id']) ? 0 : intval($_REQUEST['ticket_id']);
        $platform = !isset($_REQUEST['platform']) ? 0 : intval($_REQUEST['platform']);
        $status = !isset($_REQUEST['status']) ? -1 : intval($_REQUEST['status']);
        $type = !isset($_REQUEST['type']) ? -1 : intval($_REQUEST['type']);
        $priority = !isset($_REQUEST['priority']) ? -1 : intval($_REQUEST['priority']);
        $order_id = empty($_REQUEST['order_id']) ? 0 : intval($_REQUEST['order_id']);
        $user_name = empty($_REQUEST['user_name']) ? "" : trim($_REQUEST['user_name']);
        $last_update_start = empty($_REQUEST['last_update_start']) ? local_date("Y-m-d") : trim($_REQUEST['last_update_start']);
        $last_update_end = empty($_REQUEST['last_update_end']) ? local_date("Y-m-d") : trim($_REQUEST['last_update_end']);
        $page = !isset($_REQUEST['page']) ? 0 : intval($_REQUEST['page']);
        $where = "";

        if (!empty($ticket_id)) {
            $where .= "AND cti.ticket_id = $ticket_id ";
        }
        if (!empty($platform)) {
            $where .= "AND platform = $platform ";
        }
        if ($status != -1) {
            $where .= "AND status = $status ";
        }
        if ($status != CRM_STATUS_ARCHIVED) {
            $where .= "AND status != " . CRM_STATUS_ARCHIVED . " ";
        }
        if ($type != -1) {
            $where .= "AND type = $type ";
        }
        if ($priority != -1) {
            $where .= "AND priority = $priority ";
        }
        if (!empty($order_id)) {
            $where .= "AND order_id = $order_id ";
        }
        if (!empty($user_name)) {
            $user_id = $db->getOne("SELECT user_id FROM " . $ecs->table("users") . " WHERE user_name LIKE '%$user_name%'");
            if (!empty($user_id)) {
                $where .= "AND user_id = $user_id ";
            }
        }
        if (!empty($last_update_start)) {
            $where .= "AND cl.create_at >= '$last_update_start 00:00:00' ";
        }
        if (!empty($last_update_end)) {
            $where .= "AND cl.create_at <= '$last_update_end 23:59:59' ";
        }

        $start = $page * 50;
        $agents = $db->getAll("SELECT IFNULL(user_name, 'Unknown Admin') as user_name, agent_id FROM " . $ecs->table("crm_platform_agent") . " cpa LEFT JOIN " . $ecs->table("admin_user") . " au ON au.user_id = cpa.admin_id");
        foreach ($agents as $agent) {
            $admins[$agent['agent_id']] = $agent['user_name'];
        }
        $sql = "SELECT cl.*, cti.user_id, GROUP_CONCAT(DISTINCT cl.sender) as senders, MAX(cl.create_at) as create_at, cti.status, cti.type, cti.priority, cti.platform FROM " . $ecs->table("crm_ticket_info") . " cti " .
                "LEFT JOIN " . $ecs->table("crm_list") . " cl ON cl.ticket_id = cti.ticket_id " .
                " WHERE sender NOT REGEXP 'hangseng|hsbc|facebookmail|anonymous|stripe' " . $where . " GROUP BY cl.ticket_id ORDER BY cl.create_at DESC LIMIT $start, 50";
        $list = $db->getAll($sql);
        foreach ($list as $key => $row) {
            $senders = explode(",", $row['senders']);
            $formatted_sender = [];
            foreach ($senders as $sender) {
                if (array_key_exists($sender, $admins)) {
                    $formatted_sender['admin'][] = $admins[$sender];
                } else {
                    if (!empty($row['user_id'])) {
                        $user = $db->getOne("SELECT user_name FROM " . $ecs->table("users") . " WHERE user_id = $row[user_id]");
                    } else {
                        $user = $db->getOne("SELECT user_name FROM " . $ecs->table("crm_platform_user") . " cpu LEFT JOIN " . $ecs->table("users") . " u ON u.user_id = cpu.user_id WHERE client_id = '$sender'");
                    }
                    if (empty($user)) {
                        $formatted_sender['unknown'][] = $sender;
                    } else {
                        $formatted_sender['user'][] = $user;
                    }
                }
            }
            $list[$key]['formatted_create_at'] = self::display_date($row['create_at']);
            $list[$key]['formatted_platform'] = $_LANG['crm_platform'][$row['platform']];
            $list[$key]['platform_img'] = strtolower($list[$key]['formatted_platform']);
            if ($row['platform'] == CRM_PLATFORM_GMAIL) {
                $list[$key]['actions'] = "<span class='label btn-inline btn-info' onclick='$(\"#gmail_tab\").click();displayEmail(\"$row[unique_id]\", \"\", $row[ticket_id])'>查看</span>";
            } else if ($row['platform'] == CRM_PLATFORM_AIRCALL) {
                $list[$key]['actions'] = "<span class='label btn-inline btn-info' onclick='$(\"#aircall_tab\").click();displayAircall($row[ticket_id])'>查看</span>";
            } else if ($row['platform'] == CRM_PLATFORM_CHATRA) {
                $list[$key]['actions'] = "<span class='label btn-inline btn-info' onclick='$(\"#chatra_tab\").click();displayChatra($row[ticket_id])'>查看</span>";
            } else if ($row['platform'] == CRM_PLATFORM_TWILIO) {
                $list[$key]['actions'] = "<span class='label btn-inline btn-info' onclick='$(\"#twilio_tab\").click();displayTwilio($row[ticket_id])'>查看</span>";
            }
            if ($row['ticket_id'] == 922) {
                var_dump([$formatted_sender]);
            }
            if (!empty($formatted_sender['user'])) {
                $list[$key]['sender'] = $formatted_sender['user'][0];
            } elseif (!empty($formatted_sender['unknown'])) {
                $list[$key]['sender'] = $formatted_sender['unknown'][0];
            } elseif (!empty($formatted_sender['admin'])) {
                $list[$key]['sender'] = $formatted_sender['admin'][0];
            }
        }

        $total_record = $db->getOne(
            "SELECT COUNT(DISTINCT cl.ticket_id) FROM " . $ecs->table("crm_ticket_info") . " cti " .
            "LEFT JOIN " . $ecs->table("crm_list") . " cl ON cl.ticket_id = cti.ticket_id " .
            " WHERE sender NOT REGEXP 'hangseng|hsbc|facebookmail|stripe' " . $where
        );

        $data = [
            "total_record"  => $total_record,
            "total_ticket"  => 0,
            "total_process" => 0,
        ];

        if ($start == 0) {
            $sql = "SELECT cl.ticket_id, cti.status, cti.type, cti.priority, cti.platform FROM " . $ecs->table("crm_ticket_info") . " cti " .
                    "LEFT JOIN " . $ecs->table("crm_list") . " cl ON cl.ticket_id = cti.ticket_id " .
                    " WHERE 1 " . $where . " GROUP BY cl.ticket_id";
            $stats = $db->getAll($sql);
            $data['total_ticket'] = count($stats);
            foreach ($stats as $row) {
                $status     = $row['status'];
                $type       = $row['type'];
                $priority   = $row['priority'];
                $platform   = $row['platform'];
                if ($platform == CRM_PLATFORM_TWILIO && $status == CRM_STATUS_PENDING && $type == CRM_REQUEST_UNCATEGORIZED) {
                    $data['total_ticket']--;
                    continue;
                }
                if ($status == CRM_STATUS_PENDING || $status == CRM_STATUS_PROCESSING) {
                    $data['total_process']++;
                }
                $status_lang = $status == CRM_STATUS_FINISHED && $platform == CRM_PLATFORM_TWILIO ? $_LANG['crm_status_auto'] : $_LANG['crm_status'][$status];
                if (empty($data['total_status'][$status_lang])) {
                    $data['total_status'][$status_lang] = 1;
                } else {
                    $data['total_status'][$status_lang]++;
                }
                if (empty($data['total_type'][$_LANG['ivr_type'][$type]])) {
                    $data['total_type'][$_LANG['ivr_type'][$type]] = 1;
                } else {
                    $data['total_type'][$_LANG['ivr_type'][$type]]++;
                }
                if (empty($data['total_priority'][$_LANG['crm_priority'][$priority]])) {
                    $data['total_priority'][$_LANG['crm_priority'][$priority]] = 1;
                } else {
                    $data['total_priority'][$_LANG['crm_priority'][$priority]]++;
                }
                if (empty($data['total_platform'][$_LANG['crm_platform'][$platform]])) {
                    $data['total_platform'][$_LANG['crm_platform'][$platform]] = 1;
                } else {
                    $data['total_platform'][$_LANG['crm_platform'][$platform]]++;
                }
                if ($platform == CRM_PLATFORM_TWILIO) {
                    if (empty($data['total_twilio'][$status_lang])) {
                        $data['total_twilio'][$status_lang] = 1;
                    } else {
                        $data['total_twilio'][$status_lang]++;
                    }
                }
            }
        }
        return [
            'list'  => $list,
            'data'  => $data,
        ];
    }

    function display_date($date)
    {
        $new_date = new \DateTime($date);
        $today = new \DateTime("today");
        return $today > $new_date ? $new_date->format("Y-m-d H:i") : $new_date->format("H:i");
    }

    function get_ivr_language_enabled()
    {
        global $_CFG;
        $ivr_language_enabled = explode(",", $_CFG['ivr_language_enabled']);
        return $ivr_language_enabled;
    }

    function get_ivr_language($key = "")
    {
        global $_CFG;
        $ivr_language = json_decode($_CFG['ivr_language'], true);
        return empty($key) ? $ivr_language : $ivr_language[$key];
    }

    function set_crm_ivr_type()
    {
        global $_LANG;
        $ivr_language = self::get_ivr_language("MENU");
        $_LANG['ivr_type'][0] = $_LANG['crm_type'][CRM_REQUEST_UNCATEGORIZED];
        $_LANG['ivr_type_label'][0] = "<span class='label label-danger'>" . $_LANG['ivr_type'][0] . "</span>";
        foreach ($ivr_language as $key => $val) {
            $name = $val['name'][self::IVR_LANG_CANTONESE];
            $_LANG['ivr_type'][$key + 1] = $name;
            $class = "primary";
            // if (strpos($name, "訂單") !== false) {
            //     $class = "info";
            // } elseif (strpos($name, "售後") !== false) {
            //     $class = "info";
            // } elseif (strpos($name, "優惠") !== false) {
            //     $class = "info";
            // } elseif (strpos($name, "其它") !== false) {
            //     $class = "info";
            // }
            $_LANG['ivr_type_label'][$key + 1] = "<span class='label label-$class'>$name</span>";
        }
    }

    function init_offline_payment_flow($order, $link = true)
    {
        global $db, $ecs;
        include_once ROOT_PATH . 'includes/lib_order.php';

        $receipt_link = "";

        if (!is_array($order)) {
            $order = order_info($order);
        }

        if (!empty($order)) {
            $user_id    = $order['user_id'];
            $order_id   = $order['order_id'];
            $pay_id     = $order['pay_id'];
            $expire     = gmtime() + 86400;
            $token      = md5("$user_id-$order_id-0-$expire");

            $payment = payment_info($pay_id);
            if ($payment['is_online_pay'] == '0') {
                $exist_record = $db->getRow("SELECT * FROM " . $ecs->table("offline_payment") . " WHERE order_id = $order_id AND status < " . OFFLINE_PAYTMENT_FINISHED);
                if (empty($exist_record)) {
                    $db->query("INSERT INTO " . $ecs->table("offline_payment") . "(user_id, order_id, pay_id, expire_time, token) VALUES ($user_id, $order_id, $pay_id, $expire, '$token')");
                } else {
                    $token = $exist_record['token'];
                    if ($pay_id != $exist_record['pay_id']) {
                        $db->query("UPDATE " . $ecs->table("offline_payment") . " SET pay_id = $pay_id, status = " . OFFLINE_PAYTMENT_PENDING . ", pay_account = -1 WHERE token = '$token'");
                    }
                }
                $receipt_link = $token;
                // $receipt_link = ($link ? 'https://' . $_SERVER['SERVER_NAME'] . '/order_receipts/' : '') . $token;
            }
        }
        return $receipt_link;
    }

    function offline_payment_list()
    {
        global $db, $ecs;

        $sort_by = empty($_REQUEST['sort_by']) ? "suggest_id" : $_REQUEST['sort_by'];
        $sort_order = empty($_REQUEST['sort_order']) ? "asc" : $_REQUEST['sort_order'];
        $start = empty($_REQUEST['start']) ? 0 : intval($_REQUEST['start']);
        $page_size = empty($_REQUEST['page_size']) ? 50 : intval($_REQUEST['page_size']);
        $where = "WHERE oi.order_sn IS NOT NULL";

        if (!empty($_REQUEST['order_sn'])) {
            $where .= " AND oi.order_sn LIKE '%$_REQUEST[order_sn]%'";
        }
        if (!empty($_REQUEST['order_amount'])) {
            $where .= " AND oi.order_amount = '$_REQUEST[order_amount]'";
        }
        if (!empty($_REQUEST['pay_id'])) {
            $where .= " AND oi.pay_id = '$_REQUEST[pay_id]'";
        }
        if (isset($_REQUEST['status'])) {
            $where .= " AND op.status = '$_REQUEST[status]'";
        }

        $sql = "SELECT op.*, oi.order_sn, oi.order_amount, " .
                "CASE WHEN op.status = " . OFFLINE_PAYTMENT_UPLOADED . " THEN 999 " .
                "WHEN op.status = " . OFFLINE_PAYTMENT_FINISHED . " THEN -1 " .
                "WHEN op.status = " . OFFLINE_PAYTMENT_CLOSED . " THEN -2 " .
                "ELSE op.status END as sort_status " .
                "FROM " . $ecs->table("offline_payment") . " op " .
                "LEFT JOIN " . $ecs->table("order_info") . " oi ON oi.order_id = op.order_id " .
                $where . " ORDER BY sort_status DESC, $sort_by $sort_order " .
                "LIMIT $start, $page_size";
        $list = $db->getAll($sql);
        $count = $db->getOne("SELECT COUNT(*) FROM " . $ecs->table("offline_payment") . " op " . "LEFT JOIN " . $ecs->table("order_info") . " oi ON oi.order_id = op.order_id " . $where);

        return [
            'data' => $list,
            'record_count' => $count,
        ];
    }

    function get_offlinne_payment_request($op_id)
    {
        global $db, $ecs;
        
        $record = $db->getRow(
            "SELECT op.*, oi.order_sn FROM " . $ecs->table("offline_payment") . " op " .
            "LEFT JOIN " . $ecs->table("order_info") . " oi ON oi.order_id = op.order_id " .
            "WHERE op_id = $op_id"
        );

        if (!empty($record)) {
            $ref = $db->getAll(
                "SELECT * FROM " . $ecs->table("offline_payment") . " WHERE order_id = $record[order_id] AND op_id != $record[op_id]"
            );
        }

        return [
            "record"    => $record,
            "ref"       => $ref,
        ];
    }

    function retrieveEmailContent($parts, $plain = false, $plain_only = false)
    {
        global $service, $user, $mail_id;
        $mail = [
            'body' => [],
            'attachment' => [],
            'inline_attachment' => [],
        ];
        $processed_attachment = [];
        foreach ($parts->parts as $part) {
            if ((strpos($part->mimeType, 'multipart/') !== false || strpos($part->mimeType, 'message/') !== false) && !$plain_only) {
                $processed_content = self::retrieveEmailContent($part);
                empty($processed_content['body']) ? true :  $mail['body'] = array_merge($mail['body'], $processed_content['body']);
                empty($processed_content['attachment']) ? true : $mail['attachment'] = array_merge($mail['attachment'], $processed_content['attachment']);
                empty($processed_content['inline_attachment']) ? true : $mail['inline_attachment'] = array_merge($mail['inline_attachment'], $processed_content['inline_attachment']);
            } elseif ($part->mimeType == 'text/html' && !$plain_only) {
                $mail['body'][] = strtr($part->body->data, '-_', '+/');
            } elseif ($part->mimeType != 'text/plain' && !$plain_only) {
                $cid = "";
                $filename = "";
                foreach ($part->headers as $head) {
                    if (strtolower($head['name']) == strtolower('Content-Id')) {
                        if (preg_match("/<(.*)>/", $head['value'], $id)) {
                            $cid = $id[1];
                        }
                    } elseif (strtolower($head['name']) == strtolower('Content-Disposition')) {
                        if (preg_match("/(.*)filename=\"(.*)\"(.*)/U", $head['value'], $name)) {
                            $filename = $name[2];
                        }
                    }
                }
                $attachment_id = $part->body->attachmentId;
                if (!is_null($attachment_id)) {
                    $process = true;
                    $xid = "";
                    foreach ($part->headers as $header) {
                        if ($header['name'] == "X-Attachment-Id") {
                            $xid = $header['value'];
                            if (in_array($xid, $processed_attachment)) {
                                $process = false;
                            }
                        }
                    }
                    if (($process && (!empty($xid) || !empty($attachment_id))) || !empty($cid)) {
                        $processed_attachment[] = $xid;
                        $attach = [
                            "mimeType" => $part->mimeType,
                            "fileType" => explode("/", $part->mimeType)[0],
                            "filename" => $filename,
                            'icon' => fileIcon(explode(".", $filename)[1]),
                            "attachment_id" => $attachment_id,
                        ];
                        if (empty($cid)) {
                            $mail['attachment'][] = $attach;
                        } else {
                            $mail['inline_attachment']["cid:$cid"] = $attach;
                        }
                    }
                }
            } elseif ($part->mimeType == 'text/plain' && $plain) {
                $mail['body'][] = strtr($part->body->data, '-_', '+/');
            }
        }
        return $mail;
    }
}