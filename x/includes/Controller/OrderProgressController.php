<?php
/***
* by Eric 20190402
*
* Order Progress controller
***/

namespace Yoho\cms\Controller;
use Yoho\cms\Model;
//use function GuzzleHttp\json_decode;

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}


class OrderProgressController extends YohoBaseController
{
    const GOODS_IN_STOCK = '0';
    const GOODS_OUT_OF_STOCK = '1';
    const GOODS_NEED_PRE_ORDER = '2';
    const GOODS_NEED_TRANSFER = '3';

    const PENDING = '0';
    const FINISHED = '1';
    const DELETED = '2';


    function getlogisticsClosestCollectTime($logistics_id) {
        $logistics[2] = array (
            local_strtotime(date("Y-m-d 10:30:00")), // sf-express
            local_strtotime(date("Y-m-d 16:30:00")), // sf-express
           // local_strtotime(date("Y-m-d 10:30:00", strtotime( date( 'Y-m-d 10:30:00' )." +1 days"))),
        );
        $logistics[3] = array (
            local_strtotime(date("Y-m-d 13:00:00")), // sf +
          //  local_strtotime(date("Y-m-d 13:00:00", strtotime( date( 'Y-m-d 13:00:00' )." +1 days"))),
        );
        $logistics[1] = array (
            local_strtotime(date("Y-m-d 15:00:00")), // 宅急便
          //  local_strtotime(date("Y-m-d 14:00:00", strtotime( date( 'Y-m-d 14:00:00' )." +1 days"))),
        );
        $logistics[12] = array (
            local_strtotime(date("Y-m-d 14:00:00")), // lockerlife
          //  local_strtotime(date("Y-m-d 14:00:00", strtotime( date( 'Y-m-d 14:00:00' )." +1 days"))),
        );
        $logistics[13] = array (
            local_strtotime(date("Y-m-d 16:00:00")), // pakpobox
          //  local_strtotime(date("Y-m-d 16:00:00", strtotime( date( 'Y-m-d 16:00:00' )." +1 days"))),
        );

        if (isset($logistics[$logistics_id])) {

            foreach ($logistics[$logistics_id] as $time) {
                if ($time > gmtime()) {
                    return $time; 
                }
            }

            return $logistics[$logistics_id][0]; 
        } else {
           return false; 
        }
    }

    function getOrderProgressList($is_pagination=true, $table='remain') {
        global $db, $ecs, $userController ,$_CFG, $_LANG;
        ini_set('memory_limit', '512M');

        if (!empty($_REQUEST['period'])) {
            $end_time_period= local_strtotime(date("Y-m-d", strtotime( date( 'Y-m-d' )." -0 months")));
            $start_time_period  = local_strtotime(date("Y-m-d", strtotime( date( 'Y-m-d' )." -".$_REQUEST['period']." days")));      
        } else {
            // last 30 days
            $end_time_period= local_strtotime(date("Y-m-d", strtotime( date( 'Y-m-d' )." -0 months")));
            $start_time_period  = local_strtotime(date("Y-m-d", strtotime( date( 'Y-m-d' )." -30 days")));      
        }

        if (empty($_REQUEST['start_date'])) {
            $start_time = $start_time_period;
            $start_date = $today_date;
        } else {
            $start_time = local_strtotime($_REQUEST['start_date']);
            $start_date = $_REQUEST['start_date'];
        }

        if (empty($_REQUEST['end_date'])) {
            $end_time = $end_time_period;
 
        }else{
            $end_time= local_strtotime($_REQUEST['end_date']);
        }

        if ($table == 'remain') {
            $sql_where = '(UNIX_TIMESTAMP() - (8*3600) - warehouse_handling_time)  < 0 and op.status = 0 and is_all_in_stock = 1 ';
        } else if ($table == 'delay') {
            $sql_where = '(UNIX_TIMESTAMP() - (8*3600) - warehouse_handling_time)  > 0 and op.status = 0 and is_all_in_stock = 1 ';
        } else if ($table == 'history') {
            $sql_where = ' op.status = 1 ';
        }

        if (isset($_REQUEST['ship_status']) && $_REQUEST['ship_status'] != '' ) {
            $sql_where .= ' and oi.shipping_status = '.$_REQUEST['ship_status'].' ';
        }

        $sql = " select count(*) 
                    FROM " . $ecs->table('order_progress') . " op 
                    left join ". $ecs->table('order_info') ." oi on (op.order_id = oi.order_id) 
                    left join ". $ecs->table('delivery_order') ." do on (oi.order_id = do.order_id)
                    left join ". $ecs->table('logistics') ." l on (l.logistics_id = do.logistics_id) 
                    left join ". $ecs->table('order_cross_check') ." occ on (occ.delivery_id = do.delivery_id)
                WHERE  ".$sql_where."  and (op.status != ".self::DELETED.") and oi.shipping_id != 2 and oi.shipping_id != 5 and  (do.is_fls = 0 or do.is_fls is null) and (l.logistics_id is null or ( l.logistics_id !=14 and l.logistics_id !=8)) and (order_type != 'ws' or order_type is null) and pay_status in (".PS_PAYED.") and shipping_status in (".SS_UNSHIPPED.",".SS_SHIPPED.",".SS_SHIPPED_PART.") and create_date BETWEEN " . ($start_time) . " AND " . ($end_time + 86399) ;

        $total_count = $db->getOne($sql);

        $sql = " select op.*, oi.pay_time, oi.order_status , oi.shipping_status, 
                    DATE_FORMAT(from_unixtime(estimated_ship_time + (8*3600)), '%Y-%m-%d %H:%i') as estimated_ship_time_display, 
                    DATE_FORMAT(from_unixtime(warehouse_handling_time + (8*3600)), '%Y-%m-%d %H:%i:%s') as warehouse_handling_time_display, 
                    warehouse_handling_time - UNIX_TIMESTAMP() - (8*3600) as handling_goods_remaining_time, 
                    UNIX_TIMESTAMP() - (8*3600) - estimated_ship_time as delay_time,
                    UNIX_TIMESTAMP() - (8*3600) - warehouse_handling_time as delay_warehouse_handling_time,  
                    do.logistics_id, do.collected_time, oi.shipping_name, do.status as delivery_order_status, l.logistics_name, occ.passed, occ.error, oi.order_sn
                    FROM " . $ecs->table('order_progress') . " op 
                    left join ". $ecs->table('order_info') ." oi on (op.order_id = oi.order_id) 
                    left join ". $ecs->table('delivery_order') ." do on (oi.order_id = do.order_id)
                    left join ". $ecs->table('logistics') ." l on (l.logistics_id = do.logistics_id) 
                    left join ". $ecs->table('order_cross_check') ." occ on (occ.delivery_id = do.delivery_id)
                WHERE  ".$sql_where." and (op.status != ".self::DELETED.") and oi.shipping_id != 2 and oi.shipping_id != 5 and (do.is_fls = 0 or do.is_fls is null) and (l.logistics_id is null or ( l.logistics_id !=14 and l.logistics_id !=8)) and (order_type != 'ws' or order_type is null) and pay_status in (".PS_PAYED.") and shipping_status in (".SS_UNSHIPPED.",".SS_SHIPPED.",".SS_SHIPPED_PART.") and create_date BETWEEN " . ($start_time) . " AND " . ($end_time + 86399) ;

                // logistic id 8 代理送貨, 14 Wilson
            //     $sql = " select op.*, oi.pay_time, oi.order_status , oi.shipping_status, 
            //     DATE_FORMAT(from_unixtime(estimated_ship_time + (8*3600)), '%Y-%m-%d %H:%i:%s') as estimated_ship_time_display, 
            //     DATE_FORMAT(from_unixtime(warehouse_handling_time + (8*3600)), '%Y-%m-%d %H:%i:%s') as warehouse_handling_time_display, 
            //     warehouse_handling_time - UNIX_TIMESTAMP() - (8*3600) as handling_goods_remaining_time, 
            //     UNIX_TIMESTAMP() - (8*3600) - estimated_ship_time as delay_time,
            //     UNIX_TIMESTAMP() - (8*3600) - warehouse_handling_time as delay_warehouse_handling_time,  
            //     do.logistics_id, do.collected_time, oi.shipping_name, do.status as delivery_order_status, l.logistics_name, occ.passed, occ.error, oi.order_sn
            //     FROM " . $ecs->table('order_progress') . " op 
            //     left join ". $ecs->table('order_info') ." oi on (op.order_id = oi.order_id) 
            //     left join ". $ecs->table('delivery_order') ." do on (oi.order_id = do.order_id)
            //     left join ". $ecs->table('logistics') ." l on (l.logistics_id = do.logistics_id) 
            //     left join ". $ecs->table('order_cross_check') ." occ on (occ.delivery_id = do.delivery_id)
            // WHERE   (order_type != 'ws' or order_type is null) and pay_status in (".PS_PAYED.") and shipping_status in (".SS_UNSHIPPED.",".SS_SHIPPED.",".SS_SHIPPED_PART.") and create_date BETWEEN " . ($start_time) . " AND " . ($end_time + 86399) ;


        $sql .= ' order by '. $_REQUEST['sort_by'] . ' ' .  $_REQUEST['sort_order'];
        $sql .= ($is_pagination ? " LIMIT " . $_REQUEST['start'] . ", " . $_REQUEST['page_size'] : '');

        // echo $sql;
        // exit;

        $res = $db->getAll($sql);

        foreach ($res as &$record) {
            $record['paid_time_display'] = local_date($_CFG['time_format'], $record['pay_time']);
            
            $record['goods_status_name'] = $record['goods_status'] == 1 ? '齊貨' : '未齊貨';

            $record['stock_status_history'] = json_decode($record['stock_status_history'],true);

            $html_stock_history ='<div class="dashboard-widget-content">
            <ul class="list-unstyled timeline widget">';

            foreach ($record['stock_status_history'] as $stock_status) {
                if ($stock_status['trigger_type'] == 'PO') {
                    $source_link_html =   '<span>'. local_date('Y-m-d H:i',$stock_status['time']).'</span> '.(empty($stock_status['trigger_type']) ? '' : '<a target="_blank" href="erp_warehousing_manage.php?act=view&id='.$stock_status['trigger_type_ref_no'].'" >PO 入貨</a>') ;
                } else if ($stock_status['trigger_type'] == 'TR') {
                    $source_link_html =   '<span>'. local_date('Y-m-d H:i',$stock_status['time']).'</span> '.(empty($stock_status['trigger_type']) ? '' : '<a target="_blank" href="erp_warehouse_transfer_finished.php?act=list&transfer_id='.$stock_status['trigger_type_ref_no'].'" >調貨</a>') ;
                } else {
                    $source_link_html =   '<span>'. local_date('Y-m-d H:i',$stock_status['time']).'</span> ';
                }

                $html_stock_history .= '<li>
                <div class="block">
                  <div class="block_content">
                    <h2 class="title">
                        <a>'.($stock_status['in_stock'] == 1 ? '齊貨' : '未齊貨' ).'</a>
                    </h2>
                    <div class="byline">
                        '.$source_link_html.'
                    </div>
                    <p class="excerpt"></a>
                    </p>
                  </div>
                </div>
              </li>';
            }
            
            $html_stock_history .= '
            </ul>
            </div>';
            
            if ( $record['estimated_ship_time_display'] < date('Y-m-d H:i')) {
                $add_alert = 'class="red"';
            } else {
                $add_alert = '';
            }
            
            $record['order_sn_link'] = '<div class="order_sn_block"><a target="_blank" href="order.php?act=info&order_id='.$record['order_id'].'">'.$record['order_sn'].'</a> 
            <div class="tip_block">
            付款時間 : '.$record['paid_time_display'].'<br>
            承諾客人 : <span '.$add_alert.' >'.$record['estimated_ship_time_display'] .'</span><br>
            產品狀態 : '.$html_stock_history.'

           </div>';


            // dd($record['stock_status_history']);
            // exit;

            //echo $record['goods_remaining_time'] = $record['estimated_ship_time'] - gmtime();
           
            if ($record['estimated_ship_time_display'] < date('Y-m-d H:i:s')) {
                $record['goods_remaining_time'] = 0;
            } else {
                $record['goods_remaining_time'] = '<div class="goods_remaining_time_block" data-remaining-time="'.( strtotime($record['estimated_ship_time_display']) - strtotime(date('Y-m-d H:i:s'))).'">'. $this->get_time_difference($record['estimated_ship_time_display'],date('Y-m-d H:i:s')) . '</div>';
            }

            if ($record['warehouse_handling_time_display'] < date('Y-m-d H:i:s')) {
                $record['handling_goods_remaining_time'] = 0;
            } else {
                $record['handling_goods_remaining_time'] = '<div class="goods_remaining_time_block" data-remaining-time="'.( strtotime($record['warehouse_handling_time_display']) - strtotime(date('Y-m-d H:i:s'))).'">'. $this->get_time_difference($record['warehouse_handling_time_display'],date('Y-m-d H:i:s')) . '</div>';
            }

            $filename = ROOT_PATH . 'languages/zh_tw/admin/order.php';
            if (file_exists($filename))
            {
                include_once($filename);
            }

            // [0] => <div class="order-status-label" title="未出貨"><i class="icon icon-ship-hover"></i> 未出貨</div>
            // [3] => <div class="order-status-label light-blue" title="配貨中"><i class="icon icon-ship-hover"></i> 配貨中</div>
            // [1] => <div class="order-status-label red" title="已出貨"><i class="icon icon-ship-hover"></i> 已出貨</div>
            // [2] => <div class="order-status-label dark-blue" title="收貨確認"><i class="icon icon-ship-hover"></i> 收貨確認</div>
            // [4] => <div class="order-status-label orange" title="已出貨(部分產品)"><i class="icon icon-ship-hover"></i> 已出貨(部分產品)</div>
            // [5] => <div class="order-status-label green" title="出貨中"><i class="icon icon-ship-hover"></i> 出貨中</div>
        

           // dd($_LANG['ss']);

            $record['ship_status'] = '<div class="all_status">' .$_LANG['ss'][$record['shipping_status']].'</div>';
            // $record['logistics'] = '';

            //exit;

            if ($record['logistics_name'] != '') {
                if ($record['passed'] == '') {
                    $record['cross_check'] = '<span class="label label-default">未核對</span>';
                } else if ($record['passed'] == 1) {
                    $record['cross_check'] = '<span class="label label-success ">全部正確</span>';
                } else if ($record['passed'] == 0) {
                    $record['cross_check'] = '<span class="label label-danger view_cross_check_history">不正確</span>';
                } 
    
                if ($record['cross_check'] == '') {
                    $record['collected'] =  '<span class="label label-default">待收件</span>';
                } else if ($record['delivery_order_status'] == '5') {
                    $record['collected'] = '<span class="label label-success">已收件</span>';
                } else {
                    $record['collected'] = '<span class="label label-default">待收件</span>';
                }

                // echo $_CFG['time_format'];
                // exit;
                $logistics_time = $this->getlogisticsClosestCollectTime($record['logistics_id']);
                if (gmtime() < $logistics_time) {
                    
                    $record['closest_collect_time'] = '<span style="color:#5cb85c">'.local_date('H:i',$logistics_time).'</span>';
                } else {
                    $record['closest_collect_time'] = '<span style="color:#eea236">'.local_date('H:i',$logistics_time).'</span>';
                }
            } else {
                $record['cross_check'] = '';
                $record['collected'] = '';
            }

            // collected time > estimated time  fail
            $nowtimestring = gmtime();
            if (empty($record['collected_time'])) {
                if ($nowtimestring > $record['estimated_ship_time']) {
                    $record['send_on_time'] = '<span class="label label-danger">否</span>';
                    $record['delay_time'] = $this->get_time_difference(date('Y-m-d H:i:s',$nowtimestring),date('Y-m-d H:i:s',$record['estimated_ship_time']));
                } else{
                    $record['send_on_time'] = '';
                    $record['delay_time'] = '';
                }

                if ($nowtimestring > $record['warehouse_handling_time']) {
                    $record['warehouse_handling_on_time'] = '<span class="label label-danger">否</span>';
                    $record['delay_warehouse_handling_time'] = $this->get_time_difference(date('Y-m-d H:i:s',$nowtimestring),date('Y-m-d H:i:s',$record['warehouse_handling_time']));
                } else{
                    $record['warehouse_handling_on_time'] = '';
                    $record['delay_warehouse_handling_time'] = '';
                }

            } else {
              
                if ($record['collected_time'] > $record['estimated_ship_time']) {
                    $record['send_on_time'] = '<span class="label label-danger">否</span>'; 
                    $record['delay_time'] = $this->get_time_difference(date('Y-m-d H:i:s',$record['collected_time']),date('Y-m-d H:i:s',$record['estimated_ship_time']));
                } else {
                    $record['send_on_time'] = '<span class="label label-success">是</span>';
                    $record['delay_time'] = '';
                } 

                if ($record['collected_time'] > $record['warehouse_handling_time']) {
                    $record['warehouse_handling_on_time'] = '<span class="label label-danger">否</span>'; 
                    $record['delay_warehouse_handling_time'] = $this->get_time_difference(date('Y-m-d H:i:s',$record['collected_time']),date('Y-m-d H:i:s',$record['warehouse_handling_time']));
                } else {
                    $record['warehouse_handling_on_time'] = '<span class="label label-success">是</span>';
                    $record['delay_warehouse_handling_time'] = '';
                }  
            }

            $record['remark'] = '<span class="follow_up_remark" data-progress-id="'.$record['progress_id'].'" data-toggle="modal" data-target="#popup_remark_modal" >'.(empty($record['remark'])? '請輸入' : $record['remark']).'</span>';
            // { if $order.passed == '1'} <span class="label label-success view_cross_check_history" data-order-sn="{$order.order_sn}" data-delivery-id="{$order.delivery_id}" >全部正確</span> {/if} 
            // { if $order.passed === '0'}<span class="label label-danger view_cross_check_history" data-order-sn="{$order.order_sn}" data-delivery-id="{$order.delivery_id}">不正確</span>  {/if}
            // { if $order.passed == ''}<span class="label label-default view_cross_check_history" data-order-sn="{$order.order_sn}" data-delivery-id="{$order.delivery_id}">未核對</span>  {/if}
            
            // { if $order.status == '5'}<span class="label label-success">已收件</span>{else}<span class="label label-default">待收件</span>{/if}<br>

            $record['estimated_ship_time_display'] = date("Y-m-d H:i", strtotime($record['estimated_ship_time_display']));

            $record['shipping_logistics_name'] = $record['shipping_name'] ;

            if ($record['logistics_name'] != '') {
                $record['shipping_logistics_name'] .= '<br>'. $record['logistics_name'];
            }

            if (!empty($record['closest_collect_time'])) {
                $record['shipping_logistics_name'] .= '<br>'. '收件時間:' . $record['closest_collect_time'];
            }
        }

        $arr = array(
            'data' => $res,
            'record_count' => $total_count
          );
        
        return $arr;
    }

    // function getOrderProgressList($is_pagination=true, $table='remain') {
    //     global $db, $ecs, $userController ,$_CFG, $_LANG;
    //     ini_set('memory_limit', '512M');

    //     if (!empty($_REQUEST['period'])) {
    //         $end_time_period= local_strtotime(date("Y-m-d", strtotime( date( 'Y-m-d' )." -0 months")));
    //         $start_time_period  = local_strtotime(date("Y-m-d", strtotime( date( 'Y-m-d' )." -".$_REQUEST['period']." days")));      
    //     } else {
    //         // last 30 days
    //         $end_time_period= local_strtotime(date("Y-m-d", strtotime( date( 'Y-m-d' )." -0 months")));
    //         $start_time_period  = local_strtotime(date("Y-m-d", strtotime( date( 'Y-m-d' )." -30 days")));      
    //     }

    //     if (empty($_REQUEST['start_date'])) {
    //         $start_time = $start_time_period;
    //         $start_date = $today_date;
    //     } else {
    //         $start_time = local_strtotime($_REQUEST['start_date']);
    //         $start_date = $_REQUEST['start_date'];
    //     }

    //     if (empty($_REQUEST['end_date'])) {
    //         $end_time = $end_time_period;
 
    //     }else{
    //         $end_time= local_strtotime($_REQUEST['end_date']);

    //     }

    //     if ($table == 'remain') {
    //         $sql_where = '(UNIX_TIMESTAMP() - (8*3600) - estimated_ship_time)  < 0 and op.status = 0';
    //     } else if ($table == 'depay') {
    //         $sql_where = '(UNIX_TIMESTAMP() - (8*3600) - estimated_ship_time)  > 0 ';
    //     } else if ($table == 'history') {
    //         $sql_where = ' op.status = 1 ';
    //     }

    //     $sql = " select count(*) 
    //                 FROM " . $ecs->table('order_progress') . " op 
    //                 left join ". $ecs->table('order_info') ." oi on (op.order_id = oi.order_id) 
    //                 left join ". $ecs->table('delivery_order') ." do on (oi.order_id = do.order_id)
    //                 left join ". $ecs->table('logistics') ." l on (l.logistics_id = do.logistics_id) 
    //                 left join ". $ecs->table('order_cross_check') ." occ on (occ.delivery_id = do.delivery_id)
    //             WHERE  ".$sql_where."  and (op.status != ".self::DELETED.") and (order_type != 'ws' or order_type is null) and pay_status in (".PS_PAYED.") and shipping_status in (".SS_UNSHIPPED.",".SS_SHIPPED.",".SS_SHIPPED_PART.") and create_date BETWEEN " . ($start_time) . " AND " . ($end_time + 86399) ;

    //     $total_count = $db->getOne($sql);

    //     $sql = " select op.*, oi.pay_time, oi.order_status , oi.shipping_status, DATE_FORMAT(from_unixtime(estimated_ship_time + (8*3600)), '%Y-%m-%d %H:%i:%s') as estimated_ship_time_display,  estimated_ship_time - UNIX_TIMESTAMP() as goods_remaining_time,  UNIX_TIMESTAMP() - (8*3600) - estimated_ship_time as delay_time,  
    //                 do.logistics_id, do.collected_time, oi.shipping_name, do.status as delivery_order_status, l.logistics_name, occ.passed, occ.error, oi.order_sn
    //                 FROM " . $ecs->table('order_progress') . " op 
    //                 left join ". $ecs->table('order_info') ." oi on (op.order_id = oi.order_id) 
    //                 left join ". $ecs->table('delivery_order') ." do on (oi.order_id = do.order_id)
    //                 left join ". $ecs->table('logistics') ." l on (l.logistics_id = do.logistics_id) 
    //                 left join ". $ecs->table('order_cross_check') ." occ on (occ.delivery_id = do.delivery_id)
    //             WHERE  ".$sql_where." and (op.status != ".self::DELETED.") and (order_type != 'ws' or order_type is null) and pay_status in (".PS_PAYED.") and shipping_status in (".SS_UNSHIPPED.",".SS_SHIPPED.",".SS_SHIPPED_PART.") and create_date BETWEEN " . ($start_time) . " AND " . ($end_time + 86399) ;

    //     $sql .= ' order by '. $_REQUEST['sort_by'] . ' ' .  $_REQUEST['sort_order'];
    //     $sql .= ($is_pagination ? " LIMIT " . $_REQUEST['start'] . ", " . $_REQUEST['page_size'] : '');

    //     $res = $db->getAll($sql);

    //     foreach ($res as &$record) {
    //         $record['paid_time_display'] = local_date($_CFG['time_format'], $record['pay_time']);
    //         $record['goods_status_name'] = $record['goods_status'] == 1 ? '齊貨' : '未齊貨';
    //         $record['order_sn_link'] = '<div class="order_sn_block" ><a  target="_blank" href="order.php?act=info&order_id='.$record['order_id'].'">'.$record['order_sn'].'</a> <div class="tip_block">付款時間:'.$record['paid_time_display'].'('.$record['goods_status_name'].')</div> </div>';

    //         //echo $record['goods_remaining_time'] = $record['estimated_ship_time'] - gmtime();
           
    //         if ($record['estimated_ship_time_display'] < date('Y-m-d H:i:s')) {
    //             $record['goods_remaining_time'] = 0;
    //         } else {
    //             $record['goods_remaining_time'] = '<div class="goods_remaining_time_block" data-remaining-time="'.( strtotime($record['estimated_ship_time_display']) - strtotime(date('Y-m-d H:i:s'))).'">'. $this->get_time_difference($record['estimated_ship_time_display'],date('Y-m-d H:i:s')) . '</div>';
    //         }
    //         $filename = ROOT_PATH . 'languages/zh_tw/admin/order.php';
    //         if (file_exists($filename))
    //         {
    //             include_once($filename);
    //         }

    //         // [0] => <div class="order-status-label" title="未出貨"><i class="icon icon-ship-hover"></i> 未出貨</div>
    //         // [3] => <div class="order-status-label light-blue" title="配貨中"><i class="icon icon-ship-hover"></i> 配貨中</div>
    //         // [1] => <div class="order-status-label red" title="已出貨"><i class="icon icon-ship-hover"></i> 已出貨</div>
    //         // [2] => <div class="order-status-label dark-blue" title="收貨確認"><i class="icon icon-ship-hover"></i> 收貨確認</div>
    //         // [4] => <div class="order-status-label orange" title="已出貨(部分產品)"><i class="icon icon-ship-hover"></i> 已出貨(部分產品)</div>
    //         // [5] => <div class="order-status-label green" title="出貨中"><i class="icon icon-ship-hover"></i> 出貨中</div>
        

    //        // dd($_LANG['ss']);

    //         $record['ship_status'] = '<div class="all_status">' .$_LANG['ss'][$record['shipping_status']].'</div>';
    //         // $record['logistics'] = '';

    //         //exit;

    //         if ($record['logistics_name'] != '') {
    //             if ($record['passed'] == '') {
    //                 $record['cross_check'] = '<span class="label label-default">未核對</span>';
    //             } else if ($record['passed'] == 1) {
    //                 $record['cross_check'] = '<span class="label label-success ">全部正確</span>';
    //             } else if ($record['passed'] == 0) {
    //                 $record['cross_check'] = '<span class="label label-danger view_cross_check_history">不正確</span>';
    //             } 
    
    //             if ($record['cross_check'] == '') {
    //                 $record['collected'] =  '<span class="label label-default">待收件</span>';
    //             } else if ($record['delivery_order_status'] == '5') {
    //                 $record['collected'] = '<span class="label label-success">已收件</span>';
    //             } else {
    //                 $record['collected'] = '<span class="label label-default">待收件</span>';
    //             }

    //             // echo $_CFG['time_format'];
    //             // exit;
    //             $logistics_time = $this->getlogisticsClosestCollectTime($record['logistics_id']);
    //             if (gmtime() < $logistics_time) {
                    
    //                 $record['closest_collect_time'] = '<span style="color:#5cb85c">'.local_date('H:i',$logistics_time).'</span>';
    //             } else {
    //                 $record['closest_collect_time'] = '<span style="color:#eea236">'.local_date('H:i',$logistics_time).'</span>';
    //             }
    //         } else {
    //             $record['cross_check'] = '';
    //             $record['collected'] = '';
    //         }

    //         // collected time > estimated time  fail
    //         $nowtimestring = gmtime();
    //         if (empty($record['collected_time'])) {
    //             if ($nowtimestring > $record['estimated_ship_time']) {
    //                 $record['send_on_time'] = '<span class="label label-danger">否</span>';
    //                 $record['delay_time'] = $this->get_time_difference(date('Y-m-d H:i:s',$nowtimestring),date('Y-m-d H:i:s',$record['estimated_ship_time']));
    //             } else{
    //                 $record['send_on_time'] = '';
    //                 $record['delay_time'] = '';
    //             }
    //         } else {
              
    //             if ($record['collected_time'] > $record['estimated_ship_time']) {
    //                 $record['send_on_time'] = '<span class="label label-danger">否</span>'; 
    //                 $record['delay_time'] = $this->get_time_difference(date('Y-m-d H:i:s',$record['collected_time']),date('Y-m-d H:i:s',$record['estimated_ship_time']));
    //             } else {
    //                 $record['send_on_time'] = '<span class="label label-success">是</span>';
    //                 $record['delay_time'] = '';
    //             }    
    //         }

    //         $record['remark'] = '<span class="follow_up_remark" data-progress-id="'.$record['progress_id'].'" data-toggle="modal" data-target="#popup_remark_modal" >'.(empty($record['remark'])? '請輸入' : $record['remark']).'</span>';
    //         // { if $order.passed == '1'} <span class="label label-success view_cross_check_history" data-order-sn="{$order.order_sn}" data-delivery-id="{$order.delivery_id}" >全部正確</span> {/if} 
    //         // { if $order.passed === '0'}<span class="label label-danger view_cross_check_history" data-order-sn="{$order.order_sn}" data-delivery-id="{$order.delivery_id}">不正確</span>  {/if}
    //         // { if $order.passed == ''}<span class="label label-default view_cross_check_history" data-order-sn="{$order.order_sn}" data-delivery-id="{$order.delivery_id}">未核對</span>  {/if}
            
    //         // { if $order.status == '5'}<span class="label label-success">已收件</span>{else}<span class="label label-default">待收件</span>{/if}<br>

    //         $record['estimated_ship_time_display'] = date("Y-m-d H:i", strtotime($record['estimated_ship_time_display']));

    //         $record['shipping_logistics_name'] = $record['shipping_name'] ;

    //         if ($record['logistics_name'] != '') {
    //             $record['shipping_logistics_name'] .= '<br>'. $record['logistics_name'];
    //         }

    //         if (!empty($record['closest_collect_time'])) {
    //             $record['shipping_logistics_name'] .= '<br>'. '收件時間:' . $record['closest_collect_time'];
    //         }
             
    //     }

    //     $arr = array(
    //         'data' => $res,
    //         'record_count' => $total_count
    //       );
        
    //     return $arr;
    // }

    function updateOrderEstimatedTime()
    {
        global $db, $ecs, $userController ,$_CFG, $_LANG;

        $orderController = new OrderController();

        $order_id = $_REQUEST['order_id'];
        $orderController->orderSetShipmentEstimatedTime($order_id);
        exit;
    }

    function getProgressDashboard()
    {
        global $db, $ecs, $userController ,$_CFG, $_LANG;

        ini_set('memory_limit', '512M');
        
        //$start_time= local_strtotime(date("Y-m-d"));

        if (isset($_REQUEST['period']) && $_REQUEST['period'] != '') {
            $end_time_period= local_strtotime(date("Y-m-d", strtotime( date( 'Y-m-d' )." -0 months")));
            $start_time_period  = local_strtotime(date("Y-m-d", strtotime( date( 'Y-m-d' )." -".$_REQUEST['period']." days")));      
        } else {
            $end_time_period= local_strtotime(date("Y-m-d", strtotime( date( 'Y-m-d' )." -0 months")));
            $start_time_period  = local_strtotime(date("Y-m-d", strtotime( date( 'Y-m-d' )." -30 days")));      
        }

        if (empty($_REQUEST['start_date'])) {
            $start_time = $start_time_period;
            $start_date = $today_date;
        } else {
            $start_time = local_strtotime($_REQUEST['start_date']);
            $start_date = $_REQUEST['start_date'];
        }

        if (empty($_REQUEST['end_date'])) {
            $end_time = $end_time_period;
        }else{
            $end_time= local_strtotime($_REQUEST['end_date']);
        }

        $sql = "select do.*, op.delay_seconds from ".$ecs->table('delivery_order')." do left join ".$ecs->table('order_progress')." op on (do.order_id = op.order_id and op.status != 2) where delay_seconds is not null and collected_time BETWEEN " . ($start_time) . " AND " . ($end_time + 86399) ."";

        $collectedRes = $db->getAll($sql);

        $ar['collectedTotal'] = 0;
        $ar['collectedDalay'] = 0;
        $ar['collectedOnTime'] = 0;
        foreach ($collectedRes as $collected) {
            if ($collected['delay_seconds']>0) {
                $ar['collectedDalay'] ++;
            } else {
                $ar['collectedOnTime']++;
            }
        }

        $ar['collectedTotal'] = sizeof($collectedRes);
        $ar['collectedDalay_percentage'] = round($ar['collectedDalay']/$ar['collectedTotal']*100);
        $ar['collectedOnTime_percentage'] = round($ar['collectedOnTime']/$ar['collectedTotal']*100);

        $sql = "SELECT count(*) FROM ".$ecs->table('order_cross_check')." where  last_update_time BETWEEN " . ($start_time) . " AND " . ($end_time + 86399) ."";
        $ar['todayCrossCheckedTotal'] = $db->getOne($sql);

        if ($_REQUEST['period'] == 0) {
            $buildHtml =  
            '<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count" style="width: 200px;" >
                <span class="count_top"><i class="fa fa-user"></i> 今日已收件</span>
                <div class="count">'.$ar['collectedTotal'].'</div>
                <span class="count_bottom"><i class="green">'.$ar['collectedOnTime_percentage'].'% </i> 準時</span> |  <i class="red">'.$ar['collectedDalay_percentage'].'% </i> 延遲</span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count" style="width: 200px;">
                <span class="count_top"><i class="fa fa-user"></i> 今日已核對</span>
                <div class="count">'.$ar['todayCrossCheckedTotal'].'</div>
            </div>';
        } else {
            $buildHtml =  
            '<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count" style="width: 200px;" >
                <span class="count_top"><i class="fa fa-user"></i> 已收件</span>
                <div class="count">'.$ar['collectedTotal'].'</div>
                <span class="count_bottom"><i class="green">'.$ar['collectedOnTime_percentage'].'% </i> 準時</span> |  <i class="red">'.$ar['collectedDalay_percentage'].'% </i> 延遲</span>
            </div>';
        }

        return $buildHtml;
    }

    function get_time_difference($time_1,$time_2) {
        $datetime1 = new \DateTime($time_1);
        $datetime2 = new \DateTime($time_2);
        $interval = $datetime1->diff($datetime2);
        $elapsed = $interval->format('%y years %m months %a days %h hours %i minutes %s seconds');

        // dd($interval);

        // echo $elapsed;
        $output = '';

        if ($interval->m > 0) {
            $output .= $interval->m .'月';
            return $output;
        }

        if ($interval->d > 0) {
            $output .= $interval->d .'日';
            return $output;
        }

        if ($interval->h > 0) {
            $output .= $interval->h .'時';
        }

        if ($interval->i > 0) {
            $output .= $interval->i .'分';
            return $output;
        }

        if ($interval->s > 0) {
            $output .= $interval->s .'秒';
        }

        return $output;
    }

    public function progressRemarkSubmit()
    {
        global $db, $ecs, $userController, $_CFG;

        $remark = mysql_real_escape_string($_REQUEST['remark']);
        $sql = 'update '.$ecs->table('order_progress').' set remark = "'.$remark.'" where progress_id = "'.$_REQUEST['progress_id'].'" ';
        $db->query($sql);
        return true;
    }

    public function checkOrderAllInStock($triggerType = null,$refNo = null,$updateOrderGoods = [])
    {
        global $db, $ecs, $userController, $_CFG;
        require_once(ROOT_PATH . 'includes/lib_order.php');
        ini_set('memory_limit', '512M');
        $orderController = new OrderController();

        //echo 'checkOrderAllInStock';
        $sql = "select * from ".$ecs->table('order_info')." o where 1 ". order_query_sql('await_ship', 'o.') . " ";
        $rows = $db->getAll($sql);

        $order_ids = array_map(function ($order) { return $order['order_id']; }, $rows);

        $orders_can_ship = [];
        $orders_cant_ship = [];

        // get order goods ids
        $order_goods_ids = [];
        $sql = "SELECT distinct og.goods_id " .
                "FROM " . $GLOBALS['ecs']->table('order_goods') . " as og " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON og.goods_id = g.goods_id " .
                "WHERE order_id " . db_create_in($order_ids);
        $order_goods_ids = $GLOBALS['db']->getCol($sql);

        //get sellable warehouses without shop
        $sql = "SELECT warehouse_id FROM `ecs_erp_warehouse` where is_on_sale =  1 and is_valid = 1 and type != 1 ";
        $sellableWarehouseIds = $GLOBALS['db']->getCol($sql);

        $erpController = new ErpController();
        $goods_qty = $erpController->getSumOfSellableProductsQtyByWarehouses($order_goods_ids,$sellableWarehouseIds,false);

        $sql = "SELECT og.*, g.goods_thumb, g.goods_height, g.goods_width, g.goods_length " .
        "FROM " . $GLOBALS['ecs']->table('order_goods') . " as og " .
        "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON og.goods_id = g.goods_id " .
        "WHERE order_id " . db_create_in($order_ids);

        $order_goods = $GLOBALS['db']->getAll($sql);

        $order_goods_by_order_id = [];
        foreach ($order_goods as $order_goods_key => $order_goods_info){
            // add storage to order_goods ar
            $order_goods_by_order_id[$order_goods_info['order_id']][] = $order_goods[$order_goods_key];
        }

        foreach ($rows as $order) {
            // check if in-stock
            $all_order_goods_in_stock = true;
            $out_of_data = [];
            foreach ($order_goods_by_order_id[$order['order_id']] as $ogs)
            {
                $diff = $goods_qty[$ogs['goods_id']] - $ogs['goods_number'] + $ogs['send_number'];
                if ($diff >= 0) {
                    $goods_qty[$ogs['goods_id']] = $diff;
                } else {
                    $goods_qty[$ogs['goods_id']] = 0;
                    $all_order_goods_in_stock = false;
                    $out_of_data[$ogs['goods_id']] = $diff;
                }
            }

            if ($all_order_goods_in_stock == true) {
                $orders_can_ship[$order['order_id']] = $out_of_data; 
            } else {
                $orders_cant_ship[$order['order_id']] = $out_of_data; 
            }
        }

        // exit;
        // get current all_in_stock
        $sql = "select order_id, is_all_in_stock from ".$ecs->table('order_progress')." where order_id ".db_create_in($order_ids)." "; 
        $orderProgress = $db->getAll($sql);
        $orderCurrentAllInStockStatus = []; 
        foreach ($orderProgress as $order) {
            $orderCurrentAllInStockStatus[$order['order_id']] = $order['is_all_in_stock'];
        }

        //dd($orders_can_ship);
        // update out-of-stock
        foreach ($orders_cant_ship as $order_id_key => $order_data) {
            if (isset($orderCurrentAllInStockStatus[$order_id_key]) &&  $orderCurrentAllInStockStatus[$order_id_key] == 1) {
                // get history
                $sql = "select stock_status_history from ".$ecs->table('order_progress')." where order_id = ".$order_id_key." and status = ".self::PENDING." ";
                $history = $db->getRow($sql);

                if (!empty($history['stock_status_history'])) {
                    $stock_status_history = json_decode($history['stock_status_history'],true);
                } else {
                    $stock_status_history = [];
                }
                
                $logData['time'] = gmtime();
                $logData['goods_data'] = $order_data;
                $logData['in_stock'] = 0;
                $logData['trigger_type'] = $triggerType;
                $logData['trigger_type_ref_no'] = $refNo;

                $stock_status_history[] = $logData;
                $logDataJson = json_encode($stock_status_history);

                $needUpdate = false;
 
                if (sizeof($updateOrderGoods)>0) {
                    if (isset($order_goods_by_order_id[$order_id_key])) {
                        foreach ($order_goods_by_order_id[$order_id_key] as $orderGoodsData) {
                            if (in_array($orderGoodsData['goods_id'],$updateOrderGoods)) {
                                $needUpdate = true;
                            }
                        }
                    }
                } else {
                    $needUpdate = true;
                }
                
                if ($needUpdate) {
                    $sql = "update ".$ecs->table('order_progress')." og set is_all_in_stock = false, warehouse_handling_time = null where og.order_id = ".$order_id_key." and status = ".self::PENDING." ";
                    $db->query($sql);
                }
            }
        }

        // update in-stock
        foreach ($orders_can_ship as $order_id_key => $order_data) {
            if (isset($orderCurrentAllInStockStatus[$order_id_key]) &&  $orderCurrentAllInStockStatus[$order_id_key] == 0) {
                // get history
                $sql = "select stock_status_history from ".$ecs->table('order_progress')." where order_id = ".$order_id_key." and status = ".self::PENDING." ";
                $history = $db->getRow($sql);

                if (!empty($history['stock_status_history'])) {
                    $stock_status_history = json_decode($history['stock_status_history'],true);
                } else {
                    $stock_status_history = [];
                }

                // log data
                $logData['time'] = gmtime();
                $logData['goods_data'] = $order_data;
                $logData['in_stock'] = 1;
                $logData['trigger_type'] = $triggerType;
                $logData['trigger_type_ref_no'] = $refNo;

                $stock_status_history[] = $logData;
                $logDataJson = json_encode($stock_status_history);

                // update packing handling time
                // +8 hours
                $handling_second = 8 * 3600;
                $handling_time = $orderController->calculateShipmentEstimatedTime($handling_second);
                
                $needUpdate = false;
                if (sizeof($updateOrderGoods)>0) {
                    if (isset($order_goods_by_order_id[$order_id_key])) {
                        foreach ($order_goods_by_order_id[$order_id_key] as $orderGoodsData) {
                            if (in_array($orderGoodsData['goods_id'],$updateOrderGoods)) {
                                $needUpdate = true;
                            }
                        }
                    }
                } else {
                    $needUpdate = true;
                }

                if ($needUpdate) {
                    $sql = "update ".$ecs->table('order_progress')." og set is_all_in_stock = true , stock_status_history = '".$logDataJson."', warehouse_handling_time = ". $handling_time ." where og.order_id = ".$order_id_key." and status = ".self::PENDING." ";
                    $db->query($sql);
                    $sql = "update ".$ecs->table('order_progress')." og set first_all_in_stock_time = ".gmtime()."  where og.order_id = ".$order_id_key." and (first_all_in_stock_time = 0 or first_all_in_stock_time is null) and status = ".self::PENDING." ";
                    $db->query($sql);
                }
            }
        }
            
    }

}