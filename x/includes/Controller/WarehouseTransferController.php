<?php

/***
* WarehouseTransferController.php
*
*
*
***/
namespace Yoho\cms\Controller;

use Yoho\cms\Model;

use \Exception;

use Yoho\cms\Controller\ErpController;

if (!defined('IN_ECS')) {
    die('Hacking attempt');
}
class WarehouseTransferController extends YohoBaseController
{
    const PENDING = '0';
    const CREATING = '1';
    const TRANSFERING = '2';
    const RECEIVED = '3';
    const REJECTED = '4'; // no use for now
    const PICKING_AND_PACKING = '5';
    const NEED_ADJUSTMENT = '6';  // no use for now
    const AWAITING_TRANSPORT = '7';
    const CANCELLED = '8';
    const REVIEW = '9'; // no use for now
    const TRANSFER_STOCK_IN = '10';

    const TYPE_ARCHIVED = 0;
    const TYPE_ACTIVE = 1;
    const TYPE_DELETED = 2;

    const REASON_TYPE_NORMAL = 0;
    const REASON_TYPE_SHOP_RESTOCK= 1;
    const REASON_TYPE_STORE_PICK_UP= 2;
    const REASON_TYPE_BAD_GOODS_TO_REPAIR_CENTER = 3;
    const REASON_TYPE_BAD_GOODS_TO_SHOP = 4;
    const REASON_TYPE_ORDER_OUT_OF_STOCK  = 5;
    const REASON_TYPE_OTHERS = 6;

    const TRANSFER_RESERVED_PENDING = 0;
    const TRANSFER_RESERVED_PACKED = 1;
    const TRANSFER_RESERVED_TRANSFERRING = 2;
    const TRANSFER_RESERVED_DELIVERED = 3;


    private static $statusValues=[ 1 => '調貨產品列表建立中', 9 =>'等待執貨處理', 5 =>'執貨處理中', 7 => '等待運輸收件', 2 => '運輸中',  10 => '調貨入貨',  3 => '已調貨', 8 => '取消調貨'];
    private static $typeValues=['已封存','未封存','已刪除'];
    private static $reasonTypeValues=[ 0 => '倉內部調貨', 1 => '門市補貨', 2 => '門店自取單(調貨)', 5 => '訂單缺貨', 3 => '維修(送去倉)', 4 => '維修完畢(送回門市)',  6 => '其他'];

    private $tablename='erp_goods_stocktake';
    
    public function __construct()
    {
        parent::__construct();
       
        $this->adminuserController = new AdminuserController();
        $this->erpController =  new ErpController();
        $this->orderController = new OrderController();
        //   $this->rmaController =  new RmaController();
    }
    
    // public function createWarehouseTransfer($channel, $repair_id, $order_id, $goods_id, $qty, $operator, $reason)
    // {
    //     global $db, $ecs, $userController, $_CFG;
    //
    //     if (empty($repair_id)) {
    //         $repair_id = 'null';
    //     }
    //
    //     $user_id = $this->getDefaultSaleTeamPersonInCharge();
    //
    //     $sql = "insert into ".$ecs->table('erp_rma')." (channel,repair_id,order_id,goods_id,qty,person_in_charge,create_time,operator,reason)
    //             values ('".$channel."',".$repair_id.",'".$order_id."','".$goods_id."','".$qty."','".$user_id."','".gmtime()."','".$operator."','".$reason."') ";
    //     $db->query($sql);
    //     return $db->Insert_ID();
    // }

    public function checkStatusRedirect($transfer_id, $stage)
    {
        global $db, $ecs ,$_CFG;

        $step_create = array(self::CREATING,self::PENDING);
        $step_review = array(self::REVIEW);
        $step_packing = array(self::PICKING_AND_PACKING);
        $step_notify = array(self::AWAITING_TRANSPORT);
        $step_transferring = array(self::TRANSFERING);
        $step_transfer_stock_in = array(self::TRANSFER_STOCK_IN);
        $step_finished = array(self::RECEIVED);
        $step_cancelled = array(self::CANCELLED);
        
        // get status
        $status = $this->getCurrentStatus($transfer_id);
        
        if (in_array($status, $step_create)) {
            // redirect
            if ($stage != 'create') {
                header("Location: erp_warehouse_transfer_create.php?act=list&transfer_id=".$transfer_id);
                die();
            }
        } elseif (in_array($status, $step_review)) {
            // redirect
            if ($stage != 'review') {
                header("Location: erp_warehouse_transfer_review.php?act=list&transfer_id=".$transfer_id);
                die();
            }
        } elseif (in_array($status, $step_packing)) {
            // redirect
            if ($stage != 'packing') {
                header("Location: erp_warehouse_transfer_packing.php?act=list&transfer_id=".$transfer_id);
                die();
            }
        } elseif (in_array($status, $step_notify)) {
            // redirect
            if ($stage != 'notify') {
                header("Location: erp_warehouse_transfer_transport.php?act=list&transfer_id=".$transfer_id);
                die();
            }
        } elseif (in_array($status, $step_transferring)) {
            // redirect
            if ($stage != 'transferring') {
                header("Location: erp_warehouse_transfer_transferring.php?act=list&transfer_id=".$transfer_id);
                die();
            }
        } elseif (in_array($status, $step_transfer_stock_in)) {
            // redirect
            if ($stage != 'transfer_stock_in') {
                header("Location: erp_warehouse_transfer_stock_in.php?act=list&transfer_id=".$transfer_id);
                die();
            }
        } elseif (in_array($status, $step_finished)) {
            // redirect
            if ($stage != 'finished') {
                header("Location: erp_warehouse_transfer_finished.php?act=list&transfer_id=".$transfer_id);
                die();
            }
        } elseif (in_array($status, $step_cancelled)) {
            // redirect
            if ($stage != 'cancelled') {
                header("Location: erp_warehouse_transfer_cancelled.php?act=list&transfer_id=".$transfer_id);
                die();
            }
        }
    }

    public function importDataCrossCheck()
    {
        global $db, $ecs, $_CFG, $_LANG;
        
        $transfer_id = $_REQUEST['transfer_id'];
        $import_data = explode(PHP_EOL, $_REQUEST['import_data']);

        $result = [];

        // first check format
        foreach ($import_data as $stocktake) {
            if (empty(trim($stocktake))) {
                continue;
            }

            $stocktake_info = explode(',', $stocktake);
            if (sizeof($stocktake_info) != 2) {
                $result['error'][] = '['.$stocktake.']資料格式錯誤!';
            }
        }

        if (sizeof($result) > 0) {
            return array(
                'success_message' => '',
                'error_message' => implode('<br>', $result['error'])
            );
        }

        // import part
        $success_count = 0;
        foreach ($import_data as $stocktake) {
            if (empty(trim($stocktake))) {
                continue;
            }

            $stocktake_info = explode(',', $stocktake);
            $stocktake_sku = $stocktake_info[0];
            $theFirstChar = substr($stocktake_sku, 0, 1);
            if ($theFirstChar == '0') {
                $stocktake_sku_temp = substr($stocktake_sku, 1);
                // checking product exist
                $sql = "select count(*) from ".$ecs->table('goods')." where goods_sn = '".$stocktake_sku_temp."' " ;
                $count = $db->getOne($sql);

                if ($count == 1) {
                    $stocktake_sku = $stocktake_sku_temp;
                }
            }

            $stocktake_goods_sn = $stocktake_sku;
            $stocktake_goods_stock = $stocktake_info[1];
            $stocktake_goods_id = $this->erpController->getGoodsIdBySn($stocktake_goods_sn);

            // save to record
            // check if it is a target goods
            $sql = "select count(*) from  ".$ecs->table('erp_stock_transfer_item')." where goods_id = '".$stocktake_goods_id."' and transfer_id = '".$transfer_id."' ";
            $count = $db->getOne($sql);

            if ($count == 1) {
                $success_count++;
                $sql = "update ".$ecs->table('erp_stock_transfer_item')." set transfer_qty = '".$stocktake_goods_stock."' ,checked_qty = '".$stocktake_goods_stock."' where goods_id = '".$stocktake_goods_id."' and transfer_id = '".$transfer_id."' ";
                $db->query($sql);
            } else {
                $result['error'][] = '['.$stocktake_goods_sn.']在調貨列表找不到';
            }
        }

        // check all transfer list
        $sql = "select * from  ".$ecs->table('erp_stock_transfer_item')." where transfer_id = '".$transfer_id."' ";
        $transfer_items = $db->getAll($sql);

        $correct_count = 0;
        $incorrect_count = 0;
        $not_check_count = 0;
        foreach ($transfer_items as $item) {
            if ($item['transfer_qty'] == $item['requested_qty']) {
                $correct_count++;
            } elseif ($item['checked_qty'] == '') {
                $not_check_count++;
            } elseif ($item['transfer_qty'] != $item['requested_qty']) {
                $incorrect_count++;
            }
        }

        return array(
            'success_message' => '核對資料:<span class="'.($incorrect_count > 0 ? 'red' : 'green').'" >與要求調貨數量不對'.$incorrect_count.'</span>,<span class="'.($not_check_count > 0 ? 'red' : 'green').'" >未核對'.$not_check_count.'</span>',
            'error_message' => implode('<br>', $result['error'])
        );
    }

    public function searchProductBySku($is_pagination = true)
    {
        global $db, $ecs, $_CFG, $_LANG;

        $warehouse_id = $_REQUEST['warehouse_id'];
        $goods_sn = $_REQUEST['goods_sn'];
        $goods_qty = $_REQUEST['goods_qty'];
        $transfer_id = $_REQUEST['transfer_id'];
        
        $this->warehouseTransferStatusActive($transfer_id);
        $transfer_info=is_stock_transfer_exist($transfer_id);

        if (!$transfer_info) {
            $result['error']=$_LANG['erp_stock_transfer_not_exists'];
            return $result;
        }

        //get goods id;
        $goods_id =  $this->erpController->getGoodsIdBySn($goods_sn);
        if (empty($goods_id)) {
            $result['error']=$_LANG['erp_delivery_wrong_goods_sn'];
            return $result;
        }

        $sellable_qty = $this->erpController->getSellableProductQty($goods_id);
        
        // insert transfer items
        $attr_id=get_attr_id($goods_id);

        $sql="select item_id,transfer_qty from ".$ecs->table('erp_stock_transfer_item')." where transfer_id='".$transfer_id."' and goods_id='".$goods_id."'";
        $transfer_item_info=$db->getRow($sql);

        if (empty($transfer_item_info)) {
            // if($goods_qty > $sellable_qty)
            // {
            //     $result['error']=sprintf($_LANG['erp_stock_transfer_stock_not_enough'],$sellable_qty);
            //     return $result;
            // }
            $sql="insert into ".$ecs->table('erp_stock_transfer_item')." set transfer_id='".$transfer_id."',goods_id='".$goods_id."',attr_id='".$attr_id."',requested_qty='".$goods_qty."'";
            $db->query($sql);
            return true;
        } else {
            // if($goods_qty+$transfer_item_info['transfer_qty'] > $sellable_qty)
            // {
            //     $result['error']=sprintf($_LANG['erp_stock_transfer_stock_not_enough'],$sellable_qty);
            //     return $result;
            // }
            $sql="update ".$ecs->table('erp_stock_transfer_item')." set requested_qty=requested_qty+'".$goods_qty."' where item_id='".$transfer_item_info['item_id']."'";
            $db->query($sql);
            return true;
        }
    }

    public function saveTransferProducts($transfer_id, $goods_id, $goods_qty)
    {
        global $db, $ecs, $_CFG, $_LANG;
       
        $attr_id=get_attr_id($goods_id);
        $sql="select item_id,transfer_qty from ".$ecs->table('erp_stock_transfer_item')." where transfer_id='".$transfer_id."' and goods_id='".$goods_id."'";
        $transfer_item_info=$db->getRow($sql);

        if (empty($transfer_item_info)) {
            $sql="insert into ".$ecs->table('erp_stock_transfer_item')." set transfer_id='".$transfer_id."',goods_id='".$goods_id."',attr_id='".$attr_id."',transfer_qty=0,requested_qty='".$goods_qty."'";
            $db->query($sql);
            return true;
        } else {
            $sql="update ".$ecs->table('erp_stock_transfer_item')." set requested_qty=requested_qty+'".$goods_qty."' where item_id='".$transfer_item_info['item_id']."'";
            $db->query($sql);
            return true;
        }
    }

    public function getTransferProducts($is_pagination = true, $step=1)
    {
        global $db, $ecs, $_CFG, $_LANG;

        $transfer_id = $_REQUEST['transfer_id'];
        
        // total record
        $sql = "select count(*) 
                from  ".$ecs->table('erp_stock_transfer')."  est 
                    left join ". $ecs->table('erp_stock_transfer_item') ." esti on (est.transfer_id = esti.transfer_id) 
                where est.transfer_id = '".$transfer_id."' ";
        $total_count = $db->getOne($sql);

        $_REQUEST['record_count'] = $total_count;
        $_REQUEST['sort_by']    = empty($_REQUEST['sort_by']) ? 'goods_sn' : trim($_REQUEST['sort_by']);
        $_REQUEST['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
        
 
        // pagination
        $sql = "select est.*,esti.*,g.goods_name, g.goods_sn, g.goods_thumb ";
        $sql .= "from  ".$ecs->table('erp_stock_transfer_item')."  esti ";
        $sql .= "left join ". $ecs->table('erp_stock_transfer') ." est on (est.transfer_id = esti.transfer_id) ";
        $sql .= "left join ". $ecs->table('goods') ." g on (esti.goods_id = g.goods_id) ";
        $sql .= "where est.transfer_id = '".$transfer_id."' ";
        $sql .= ' order by '. $_REQUEST['sort_by'] . ' ' .  $_REQUEST['sort_order'];
        $sql .= ($is_pagination ? " LIMIT " . $_REQUEST['start'] . ", " . $_REQUEST['page_size'] : '');
        $res = $db->getAll($sql);

        $warehouse_id_to_name = $this->getWarehouseId2Name();
     
        foreach ($res as $key => &$row) {
            $row['image'] = '<img style="width: 50px;" src="../'.$row['goods_thumb'].'" />';
            $row['warehouse_from_name'] = $warehouse_id_to_name[$row['warehouse_from']];

            if ($step == 1 || $step == 2 || $step == 3 || $step == 4 ) {
                $target_warehouse_id = $row['warehouse_from'];
            } else {
                $target_warehouse_id = $row['warehouse_to'];
            }

            $goods_sellable_qty = $this->erpController->getSellableProductsQtyByWarehouses($row['goods_id'],$target_warehouse_id);
            $goods_physical_qty = $this->erpController->getSellableProductsQtyByWarehouses($row['goods_id'],$target_warehouse_id,false);

            // echo '<pre>';
            // print_r($goods_sellable_qty);
            // exit;

            if ($step == 1) {
                $row['sellable_qty'] = $goods_sellable_qty[$row['goods_id']][$target_warehouse_id];
            } else if ($step == 6) {
                $row['sellable_qty'] = $goods_sellable_qty[$row['goods_id']][$target_warehouse_id];
                $row['physical_qty'] = $goods_physical_qty[$row['goods_id']][$target_warehouse_id];
            } else {
                $row['sellable_qty'] = $goods_sellable_qty[$row['goods_id']][$target_warehouse_id];
                $row['physical_qty'] = $goods_physical_qty[$row['goods_id']][$target_warehouse_id];
            }

            $row['sellable_qty_html'] = '<a target="_blank" class="stock_breakdown" data-gid="'.$row['goods_id'].'" href="erp_stock_inquiry.php?act=list&goods_sn='.$row['goods_sn'].'&goods_name=">'.$row['sellable_qty'].'</a>'; 
            $row['physical_qty_html'] = '<a target="_blank" class="stock_breakdown" data-gid="'.$row['goods_id'].'" href="erp_stock_inquiry.php?act=list&goods_sn='.$row['goods_sn'].'&goods_name=">'.$row['physical_qty'].'</a>'; 

            $row['stock_check'] = '<a target="_blank" href="erp_stock_inquiry.php?act=list&goods_sn='.$row['goods_sn'].'&goods_name=">庫存</a>'; 

            // echo '<pre>';
            // print_r( $row['sellable_qty']);
            // exit;
            $transfer_qty_temp = $row['transfer_qty'];
            $requested_qty_temp = $row['requested_qty'];

            if ($step==1) {
                $transfer_qty_color = ($row['sellable_qty'] < $row['requested_qty'] ? 'red' : '');
            } else {
                $transfer_qty_color = ($row['requested_qty'] != $row['transfer_qty'] ? 'red' : '');
                $checked_qty_color = ($row['checked_qty'] != $row['transfer_qty'] ? 'red' : '');
                $stock_qty_color = ($row['requested_qty'] != $row['transfer_qty'] ? 'red' : '');
                $received_qty_color = ($row['transfer_qty'] != $row['received_qty'] ? 'red' : '');
            }
  
            $row['transfer_html'] = '<span data-item-id="'.$row['item_id'].'"  data-requested-qty="'.$row['requested_qty'].'" data-sellable-qty="'.$row['sellable_qty'].'" class="editable_text transfer_qty_text '.$transfer_qty_color .' " >'.$transfer_qty_temp.'</span>';
            $row['transfer_html'] .= '<input data-item-id="'.$row['item_id'].'" style="display: none;width:80px" name="order_sn" type="text" class="transfer_qty_input form-control '.(($row['sellable_qty'] < $row['transfer_qty'])? 'red': '').' "  value="'.$transfer_qty_temp.'">';
            
            $row['requested_qty_html'] = '<span data-item-id="'.$row['item_id'].'"  data-requested-qty="'.$row['requested_qty'].'" data-sellable-qty="'.$row['sellable_qty'].'" class="editable_text requested_qty_text '.$transfer_qty_color .' " >'.$requested_qty_temp.'</span>';
            $row['requested_qty_html'] .= '<input data-item-id="'.$row['item_id'].'" style="display: none;width:80px" name="order_sn" type="text" class="requested_qty_input form-control '.(($row['sellable_qty'] < $row['requested_qty'])? 'red': '').' "  value="'.$requested_qty_temp.'">';
             

            if ($step == 6) {
                $row['received_html'] = '<span data-item-id="'.$row['item_id'].'" class=" received_qty_text '.$received_qty_color .' " >'.$row['received_qty'].'</span>';
            } else {
                $row['received_html'] = '<span data-item-id="'.$row['item_id'].'"  data-transfer-qty="'.$row['transfer_qty'].'"  data-received-qty="'.$row['received_qty'].'" data-sellable-qty="'.$row['sellable_qty'].'" class="editable_text received_qty_text '.$received_qty_color .' " >'.$row['received_qty'].'</span>';
                $row['received_html'] .= '<input data-item-id="'.$row['item_id'].'" style="display: none;width:80px" name="order_sn" type="text" class="received_qty_input form-control '.(($row['transfer_qty'] != $row['received_qty'])? 'red': '').' "  value="'.$row['received_qty'].'">';
            }

            if ($row['auto_created']) {
                $row['actions'] = '-';
                $row['requested_qty_html']  = $row['requested_qty'];
            } else {
                $row['actions'] = '<a  class="collapse-link delete-transfer-item" data-item-id="'.$row['item_id'].'" href="javascript:void(0)"><i class="fa fa-trash red" style="font-size: 20px;"></i></a>';
            }
            
            if ($step == 7) {
                $row['stock_action_html'] = '';
                $row['stock_adjust_history_html'] = '';
                if ($row['transfer_qty'] != $row['received_qty']) {
                    $row['stock_action_html'] = '<input class="btn btn-success  form-btn adjust_warehouse_stock" type="button" id="adjust_warehouse_stock_'.$row['goods_id'].'"  data-goods-id = "'.$row['goods_id'].'" data-toggle="modal" data-target="#popup_adjust_warehouse_stock_create_modal" value="庫存調整">';
                    $stockAdjustHistory = $this->getTransferStockAjustHistory($transfer_id,$row['goods_id']);
                    
                    foreach ($stockAdjustHistory as $history) {
                        $row['stock_adjust_history_html'] .= '<div data-adjust-id = '.$history['adjustment_id'].' class="view_adjust_warehouse_stock"> '.$warehouse_id_to_name[$history['warehouse_id']].'('.$history['qty'].') '.($history['status'] == 0 ? '<span class="">待確認</span>' : '<span class="green">已確認</span>' ).' <br>('.local_date($_CFG['time_format'], $history['create_date']).')</div>'.'<br>';
                    }
                }
            }
            
        }

        $arr = array(
            'data' => $res,
            'record_count' => $_REQUEST['record_count']
        );

        return $arr;
    }

    public function adjustTransferWarehouseStockView($adjustment_id)
    {
        global $db, $ecs, $_CFG, $_LANG;

        $sql = "select sta.*, g.goods_name from ".$ecs->table('erp_stock_transfer_adjustment')." sta left join " . $ecs->table('goods') . " g on (sta.goods_id = g.goods_id)  where adjustment_id = '".$adjustment_id."' ";
        $adjust_info = $db->getRow($sql);

        $adjust_info['create_date'] = local_date($_CFG['time_format'], $adjust_info['create_date']);
        $adjust_info['adjust_remark'] = $adjust_info['remark'];
        $adjust_info['adjust_status'] = $adjust_info['status'];


        // get transfer info
        $transfer_info = $this->getTransferInfo($adjust_info['transfer_id']);

        $adjust_info = array_merge($adjust_info,$transfer_info);

        if ($adjust_info['warehouse_from'] == $adjust_info['warehouse_id']) {
            $adjust_info['warehouse_name'] = $adjust_info['warehouse_from_name'];
        } else if ($adjust_info['warehouse_to'] == $adjust_info['warehouse_id']) {
            $adjust_info['warehouse_name'] = $adjust_info['warehouse_to_name'];
        }

        $adjust_info['adjust_remark'] = ($adjust_info['adjust_remark'] == '')? '-' : $adjust_info['adjust_remark']; 

        $adjust_info['admin_from_confirm_html'] = '';
        $adjust_info['admin_to_confirm_html'] = '';
        $adjust_info['admin_from_confirm_sign_html'] = '';
        $adjust_info['admin_to_confirm_sign_html'] = '';

        // get user info
        $usersInfo = $this->getUserId2Info();

        $adjust_info['admin_from_confirm_html'] = '<input '.($adjust_info['admin_from_confirm'] != '0' ? 'disabled' : '' ) .' class=" btn btn-success adjust_warehouse_admin_confirm " data-warehouse-side ="from" type="button" id="adjust_warehouse_from_admin_confirm" name="adjust_warehouse_from_admin_confirm" data-adjust-id = "'.$adjust_info['adjustment_id'].'" value="出貨倉確定 ('.$adjust_info['warehouse_from_name'].')" placeholder=""><br>';
        
        // user_id,user_name,avatar
        if ($adjust_info['admin_from_confirm'] != '0') {
            $adjust_info['admin_from_confirm_html'] .= '<img class="img-circle profile_img" width="30px" src="../'.$usersInfo[$adjust_info['admin_from_confirm']]['avatar'].'" /> <span class="green"> 己確認<i class="fas fa-check"></i></span>';
        } else {
            $adjust_info['admin_from_confirm_html'] .= '<div style="padding-top: 22px;">待確認</div>';
        }
        
  
        $adjust_info['admin_to_confirm_html'] = '<input '. ($adjust_info['admin_to_confirm'] != '0' ? 'disabled' : '' ) .' class=" btn btn-success adjust_warehouse_admin_confirm" data-warehouse-side ="to" type="button" id="submit_adjust_warehouse_to_admin_confirm" name="submit_adjust_warehouse_to_admin_confirm" data-adjust-id = "'.$adjust_info['adjustment_id'].'" value="收貨倉確定 ('.$adjust_info['warehouse_to_name'].')" placeholder=""><br>';
        if ($adjust_info['admin_to_confirm'] != '0') {
            $adjust_info['admin_to_confirm_html'] .= '<img class="img-circle profile_img" width="30px" src="../'.$usersInfo[$adjust_info['admin_to_confirm']]['avatar'].'" /><span class="green"> 己確認<i class="fas fa-check"></i></span>';
        } else {
            $adjust_info['admin_to_confirm_html'] .= '<div style="padding-top: 22px;">待確認</div>';
        }

        if ($adjust_info['qty'] > 0 && !empty($adjust_info['ref_no'])) {
            $warehousing_sn = $this->getWarehousingSN($adjust_info['ref_no']);
            $adjust_info['finish_ref_no_path'] = '庫存調整已完成 <a target="_blank" href="erp_warehousing_manage.php?act=view&id='.$adjust_info['ref_no'].'" >'.$warehousing_sn.'</a>';
        }else if ($adjust_info['qty'] < 0 && !empty($adjust_info['ref_no'])) {
            $delivery_sn = $this->getDeliverySN($adjust_info['ref_no']);
            $adjust_info['finish_ref_no_path'] = '庫存調整已完成 <a target="_blank" href="erp_delivery_manage.php?act=view&id='.$adjust_info['ref_no'].'" >'.$delivery_sn.'</a>';
        }

        if (empty($adjust_info['ref_no'])){
            $adjust_info['delete_button_html'] = '<input class="btn btn-danger  form-btn adjust_warehouse_stock_delete" type="button" data-adjust-id="'.$adjust_info['adjustment_id'].'"  value="刪除">';
        } else {
            $adjust_info['delete_button_html'] = '';
        }
 
        return $adjust_info;
    }

    public function getWarehousingSN($warehousing_id) {
        global $db, $ecs, $_CFG, $_LANG;

        $sql = "select warehousing_sn from ".$ecs->table('erp_warehousing')." where warehousing_id = '".$warehousing_id."' ";
        return $db->getOne($sql);
    }

    public function getDeliverySN($delivery_id) {
        global $db, $ecs, $_CFG, $_LANG;

        $sql = "select delivery_sn from ".$ecs->table('erp_delivery')." where delivery_id = '".$delivery_id."' ";
        return $db->getOne($sql);
    }

    public function getTransferStockAjustHistory($transfer_id,$goods_id)
    {
        global $db, $ecs, $_CFG, $_LANG;

        $sql = "select * from ".$ecs->table('erp_stock_transfer_adjustment')." where transfer_id = '".$transfer_id."' and goods_id = '".$goods_id."' ";
        $db->getAll($sql);

        return $db->getAll($sql);
    }

    public function getTransferProductsForCrossCheck()
    {
        global $db, $ecs, $_CFG, $_LANG;

        $transfer_sn = $_REQUEST['transfer_sn'];
        
        // get transfer id 
        $sql = "select transfer_id 
                from  ".$ecs->table('erp_stock_transfer')."  est 
                where est.transfer_sn = '".$transfer_sn."' ";
        
        $transfer_id = $db->getOne($sql);
        
        if (empty($transfer_id)) {
            $arr = array(
                'transfer_id' => 0,
                'data' => [],
                'record_count' => 0,
                'is_passed' => 0
            );
            return $arr;
        }

        // total record
        // $sql = "select count(*) 
        //         from  ".$ecs->table('erp_stock_transfer')."  est 
        //             left join ". $ecs->table('erp_stock_transfer_item') ." esti on (est.transfer_id = esti.transfer_id) 
        //         where est.transfer_id = '".$transfer_id."' ";
        // $total_count = $db->getOne($sql);

        $sql = "select passed from ".$ecs->table('erp_stock_transfer_cross_check')." where transfer_id = '".$transfer_id."' and passed = 1 limit 1";
        $is_passed = $db->getOne($sql);

        if (empty($is_passed)) {
            $is_passed = 0;
        } else {
            $is_passed = 1;
        }
 
        // pagination
        $sql = "select est.*,esti.*,g.goods_name, g.goods_sn, g.goods_thumb ";
        $sql .= "from  ".$ecs->table('erp_stock_transfer_item')."  esti ";
        $sql .= "left join ". $ecs->table('erp_stock_transfer') ." est on (est.transfer_id = esti.transfer_id) ";
        $sql .= "left join ". $ecs->table('goods') ." g on (esti.goods_id = g.goods_id) ";
        $sql .= "where est.transfer_id = '".$transfer_id."' ";
        $sql .= ' order by goods_id  ';
        $res = $db->getAll($sql);

        $warehouse_id_to_name = $this->getWarehouseId2Name();
     
        foreach ($res as $key => &$row) {
            $row['image'] = '<img style="width: 50px;" src="../'.$row['goods_thumb'].'" />';
            $row['warehouse_from_name'] = $warehouse_id_to_name[$row['warehouse_from']];
           // $transfer_qty_temp = $row['transfer_qty'];
            $row['stocktake_number'] = 0;
            $row['requested_qty_html'] = '<span class="requested-qty-'.$row['goods_sn'].'" >'.$row['requested_qty'].'</span>';
            $row['stocktake_number_html'] = '<span class="stocktake-'.$row['goods_sn'].'" data-stocktake-number="0" >0</span>';
            $row['goods_list_html_for_special'] = '<div class="radio"><label><input type="radio" checked="" value="'.$row['goods_sn'].'" name="goods_sn_special"> '. $row['goods_sn'] . '(' . $row['goods_name'] . ')</label></div>';
            $row['change_requested_qty_html'] = '<input class="btn btn-warning form-btn change_goods_requested_qty" data-goods-sn="'.$row['goods_sn'].'" type="button" id="change_goods_requested_qty_'.$row['goods_sn'].'" name="change_goods_requested_qty_'.$row['goods_sn'].'" value="數量正確" data-toggle="modal">';
        }

        $record_count = sizeof($res);

        $arr = array(
            'transfer_id' => $transfer_id,
		    'data' => $res,
		    'record_count' => $record_count,
            'is_passed' => $is_passed
		);


        return $arr;
    }

    public function update_order_transfer_stock_out_qty()
    {
        global $db, $ecs, $_LANG;

        if (!empty($_REQUEST['stocktake_order_goods'])) {
            // get goods id
            $goods_sns = array_keys($_REQUEST['stocktake_order_goods']);
            $sql = 'select goods_id,goods_sn from '.$ecs->table('goods').' where goods_sn in ("'. implode('","',$goods_sns) .'") ';
            $res = $db->getAll($sql);

            foreach ($res as $stocktake_goods){
                $sql = 'update '.$ecs->table('erp_stock_transfer_item').' set transfer_qty = '.$_REQUEST['stocktake_order_goods'][$stocktake_goods['goods_sn']].' where  goods_id = '.$stocktake_goods['goods_id'].' and transfer_id = '.$_REQUEST['transfer_id'].' ';
                $db->query($sql);
            }
        }
    }

    public function isTransferSn($transfer_sn)
    {
        global $db, $ecs, $_LANG;

        $sql = "SELECT count(*) FROM ".$ecs->table('erp_stock_transfer')." where transfer_sn = '".$transfer_sn."' ";
        $res = $db->getOne($sql);

        if ($res > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function save_cross_check_error_history()
    {
        global $db, $ecs, $_LANG;

        $sql="insert into ".$GLOBALS['ecs']->table('erp_stock_transfer_cross_check')." (transfer_sn,transfer_type,transfer_id,passed,operator,log,last_update_time) values ( 
            '".$_REQUEST['transfer_sn'].
            "','".$_REQUEST['transfer_type'].
            "','".$_REQUEST['transfer_id'].
            "','".$_REQUEST['passed'].
            "','".$_REQUEST['operator'].
            "','".$_REQUEST['log']."','".gmtime()."') ON DUPLICATE KEY UPDATE passed = '".$_REQUEST['passed']."' , operator = '".$_REQUEST['operator']."', `log` = CONCAT(`log`, '".$_REQUEST['log']."' ) , last_update_time = '".gmtime()."' ";

        $db->query($sql);
        
        return true;
    }

    public function saveCrossCheckError()
    {
        global $db, $ecs, $userController ,$_CFG, $_LANG;

        $sql = "update ".$GLOBALS['ecs']->table('erp_stock_transfer_cross_check')." set error = 1 where transfer_id = '".$_REQUEST['transfer_id']."' ";
        $db->query($sql);
        return true;
    }

    public function transferDeleteForRMA($transfer_id,$rma_id)
    {
        global $db, $ecs, $_CFG, $_LANG;

        // delete
        $rmaController = new RmaController();
        $rma_info = $rmaController->getRmaInfo($rma_id);

        // get goods qty
        $sql = "select requested_qty from ".$ecs->table('erp_stock_transfer_item')." where goods_id = ".$rma_info['goods_id']." and transfer_id = ".$transfer_id." ";
        $requested_qty = $db->getOne($sql);
        
        if ($requested_qty == 1) {
            $sql = "delete from ".$ecs->table('erp_stock_transfer_item')." where transfer_id = ".$transfer_id." and goods_id = ".$rma_info['goods_id']." ";
            $db->query($sql);
        } else {
            $sql = "update ".$ecs->table('erp_stock_transfer_item')." set requested_qty = requested_qty - ".$rma_info['qty']." where transfer_id = ".$transfer_id." and goods_id = ".$rma_info['goods_id']." ";
            $db->query($sql);
        }

        $sql = "select count(*) from ".$ecs->table('erp_stock_transfer_item')." where transfer_id = ".$transfer_id."  group by transfer_id ";
        $goods_total = $db->getOne($sql);

        if ($goods_total == 0) {
            $sql = "delete from ".$ecs->table('erp_stock_transfer')." where transfer_id = ".$transfer_id."  ";
            $db->query($sql);

            $sql = "delete from ".$ecs->table('erp_stock_transfer_item')." where transfer_id = ".$transfer_id."  ";
            $db->query($sql);
        }
        return true;

    }

    public function transferApprove()
    {
        global $db, $ecs, $_CFG, $_LANG;

        $transfer_id = $_REQUEST['transfer_id'];
        $db->query('START TRANSACTION');
        $this->warehouseTransferChangeStatus($transfer_id, self::PICKING_AND_PACKING);

        $db->query("COMMIT");
        return true;
    }

    public function transferCancel()
    {
        global $db, $ecs, $_CFG, $_LANG;

        $transfer_id = $_REQUEST['transfer_id'];
        
        $db->query('START TRANSACTION');


        // delete erp_stock_transfer_reserved records
        $sql = "delete from ".$ecs->table('erp_stock_transfer_reserved')." where transfer_id = ".$transfer_id." ";
        $db->query($sql);

        // delete order_goods_transfer_reserved records
        $sql = "delete from ".$ecs->table('order_goods_transfer_reserved')." where transfer_id = ".$transfer_id." ";
        $db->query($sql);

        if ($this->getCurrentStatus($transfer_id) == self::AWAITING_TRANSPORT) {
            // add stock back to original warehouse
            $sql = "select est.*,esti.*,g.goods_name, g.goods_sn, g.goods_thumb ";
            $sql .= "from  ".$ecs->table('erp_stock_transfer_item')."  esti ";
            $sql .= "left join ". $ecs->table('erp_stock_transfer') ." est on (est.transfer_id = esti.transfer_id) ";
            $sql .= "left join ". $ecs->table('goods') ." g on (esti.goods_id = g.goods_id) ";
            $sql .= "where est.transfer_id = '".$transfer_id."' ";
            $res = $db->getAll($sql);

            $error = [];
            foreach ($res as $item) {
                if ($item['transfer_qty'] > 0) {
                    $attrId = get_attr_id($item['goods_id']);
                    $items[]="(".$item['transfer_id'].",".$item['goods_id'].",".$attrId.",".$item['transfer_qty'].")";
                    $toWsStock=get_goods_stock_by_warehouse($item['warehouse_from'], $item['goods_id']);
                    $wdName = $this->getUserNameById($item['admin_from']).'(取消調貨)';
                    $stockQueries=[];
                    $sql="INSERT INTO ".$ecs->table('erp_stock')." SET goods_id='".$item['goods_id']."',goods_attr_id='".$attrId."',qty='".($item['transfer_qty'])."',act_time='".gmtime()."',warehouse_id='".$item['warehouse_from']."'";
                    $sql.=", w_d_name='".$wdName."', w_d_style_id='".$this->erpController->getDeliveryStyleIdByName('調倉')."',order_sn='".$item['transfer_sn']."',stock_style='d',w_d_sn='".$item['transfer_sn']."',stock=".($toWsStock+$item['transfer_qty']).",act_date='".local_date('Y-m-d', gmtime())."'";
                 
                    $stockQueries[]=$sql;
                    
                    $sql="UPDATE ".$ecs->table('erp_goods_attr_stock')." SET goods_qty =goods_qty +".$item['transfer_qty']." where goods_id='".$item['goods_id']."' and attr_id='".$attrId."' and warehouse_id='".$item['warehouse_from']."'";
                    $stockQueries[]=$sql;
                    foreach ($stockQueries as $query) {
                        if (!$db->query($query)) {
                            $result['error'] = $_LANG['erp_stock_transfer_confirm_failed'];
                            return $result;
                        }
                    }
                }
            }
        }

        // change status to CANCELLED 8
        $this->warehouseTransferChangeStatus($transfer_id, self::CANCELLED);

        $db->query("COMMIT");
        return true;
    }

    public function transferConfirm()
    {
        global $db, $ecs, $_CFG, $_LANG;

        $transfer_id = $_REQUEST['transfer_id'];
        $rmaController = new RmaController();
        $transfer_info = $this->getTransferInfo($transfer_id);
        $orderController = new OrderController();

        $db->query('START TRANSACTION');
        $sql = "select est.*,esti.*,g.goods_name, g.goods_sn, g.goods_thumb ";
        $sql .= "from  ".$ecs->table('erp_stock_transfer_item')."  esti ";
        $sql .= "left join ". $ecs->table('erp_stock_transfer') ." est on (est.transfer_id = esti.transfer_id) ";
        $sql .= "left join ". $ecs->table('goods') ." g on (esti.goods_id = g.goods_id) ";
        $sql .= "where est.transfer_id = '".$transfer_id."' ";
        $res = $db->getAll($sql);

        $error = [];
        foreach ($res as $item) {
            if ($item['transfer_qty'] > 0) {
                $attrId = get_attr_id($item['goods_id']);
                $items[]="(".$item['transfer_id'].",".$item['goods_id'].",".$attrId.",".$item['transfer_qty'].")";
                $fromWsStock=get_goods_stock_by_warehouse($item['warehouse_from'], $item['goods_id']);
                if ($item['transfer_qty']>$fromWsStock) {
                    $result['error'] = '['.$item['goods_sn'].'] '.sprintf($_LANG['erp_stock_transfer_stock_not_enough'], $fromWsStock);
                    return $result;
                }

                $wdName = $this->getUserNameById($item['admin_from']);

                $stockQueries=[];
                //2-2 add to new ws
                //2-2-a stock
                $sql="INSERT INTO ".$ecs->table('erp_stock')." SET goods_id='".$item['goods_id']."',goods_attr_id='".$attrId."',qty='".(0-$item['transfer_qty'])."',act_time='".gmtime()."',warehouse_id='".$item['warehouse_from']."'";
                $sql.=", w_d_name='".$wdName."', w_d_style_id='".$this->erpController->getDeliveryStyleIdByName('調倉')."',order_sn='".$item['transfer_sn']."',stock_style='d',w_d_sn='".$item['transfer_sn']."',stock=".($fromWsStock-$item['transfer_qty']).",act_date='".local_date('Y-m-d', gmtime())."'";
                
                if ($transfer_info['rma_id'] > 0) {
                   $sql.=",rma_id = ".$transfer_info['rma_id']."  ";
                }

                $stockQueries[]=$sql;
                //2-2-b attr
                $sql="UPDATE ".$ecs->table('erp_goods_attr_stock')." SET goods_qty =goods_qty -".$item['transfer_qty']." where goods_id='".$item['goods_id']."' and attr_id='".$attrId."' and warehouse_id='".$item['warehouse_from']."'";
                $stockQueries[]=$sql;

                foreach ($stockQueries as $query) {
                    if (!$db->query($query)) {
                        $result['error'] = $_LANG['erp_stock_transfer_confirm_failed'];
                        return $result;
                    }
                }
            }

            $diff_qty = $item['transfer_qty'] - $item['requested_qty'];

            if ($diff_qty < 0) {
                $not_enough_remark .= $item['goods_sn'] .'('.($diff_qty).')'.'\n';
            }
            
            // update transfer reserved table
            $sql = "update ".$ecs->table('erp_stock_transfer_reserved')." set qty = ".$item['transfer_qty'] ." , status = ".self::TRANSFER_RESERVED_PACKED." where transfer_id = ".$transfer_id." and goods_id = ".$item['goods_id']." ";
            $db->query($sql);
        }

        if (!empty($not_enough_remark)) {
            $sql = "update ".$ecs->table('erp_stock_transfer')." set stock_not_enough_remark = '".$not_enough_remark."' where transfer_id = ".$transfer_id." " ;
            $db->query($sql);
        }
        
        admin_log($wdName .'-' .$transfer_id, 'edit', 'goods_stock');

        // change status to PICKING_AND_PACKING 5
        $this->warehouseTransferChangeStatus($transfer_id, self::AWAITING_TRANSPORT);

        $sql = "update ".$ecs->table('erp_stock_transfer')." set transfer_time = '".gmtime()."' where transfer_id = '".$transfer_id."' ";
        $db->query($sql);


        // if transfer reason type = stocre pick up
        if ($transfer_info['reason_type'] == self::REASON_TYPE_STORE_PICK_UP || $transfer_info['reason_type'] == self::REASON_TYPE_ORDER_OUT_OF_STOCK ) {
            // update order_goods_transfer_reserved
            $orderController =$this->orderController;
            $this->orderController->changeOrderGoodsTransferReservedStatus($transfer_id,$orderController::ORDER_GOODS_RESERVED_STATUS_TRANSFERRING);
        }
        

        //$orderController
        $db->query("COMMIT");

        return true;
    }

    public function transferConfirmCheck()
    {
        global $db, $ecs, $_CFG, $_LANG;

        $transfer_id = $_REQUEST['transfer_id'];

        $sql = "select est.*,esti.*,g.goods_name, g.goods_sn, g.goods_thumb ";
        $sql .= "from  ".$ecs->table('erp_stock_transfer_item')."  esti ";
        $sql .= "left join ". $ecs->table('erp_stock_transfer') ." est on (est.transfer_id = esti.transfer_id) ";
        $sql .= "left join ". $ecs->table('goods') ." g on (esti.goods_id = g.goods_id) ";
        $sql .= "where est.transfer_id = '".$transfer_id."' ";
        $res = $db->getAll($sql);

        $error = [];
        foreach ($res as $item) {
            if ($item['requested_qty'] != $item['transfer_qty']) {
                $error[] = '貨號:['.$item['goods_sn'].']與要求調貨數量不對('.$item['transfer_qty'].'/'.$item['requested_qty'] .')';
            }
        }

        if (sizeof($error) > 0) {
            if ($item['reason_type'] == self::REASON_TYPE_BAD_GOODS_TO_REPAIR_CENTER || $item['reason_type'] == self::REASON_TYPE_BAD_GOODS_TO_SHOP) {
                $error[] = '壞貨調貨數量不對,如要出貨,請先將RMA取消調貨';
            } else {
                $error[] = '是否繼續出貨?<input class="form-control  btn btn-warning" type="button" id="transfer_confirm_force" value="繼續出貨" >';
            }

          
            $result['error'] = $error;
            return $result;
        } else {
            return true;
        }

      

        // if (empty($error)) {
        //     $db->query('START TRANSACTION');
        //     // change status to PICKING_AND_PACKING 5
        //     $this->warehouseTransferChangeStatus($transfer_id,self::PICKING_AND_PACKING);
        //
        //     // update request stock to transfer stock
        //     $sql = "update ".$ecs->table('erp_stock_transfer_item')." set requested_qty = transfer_qty where transfer_id = '".$transfer_id."' ";
        //     $db->query($sql);
        //
        //     $db->query('COMMIT');
        //     return true;
        // } else {
        //     $result['error'] = $error;
        //     return $result;
        // }
    }

    public function transferReservedCreate($warehouse_id, $goods_id, $qty, $transfer_id)
    {
        global $db, $ecs, $_CFG, $_LANG;

        $sql ="insert into ".$ecs->table('erp_stock_transfer_reserved')." (warehouse_id,goods_id,qty,transfer_id) values (".$warehouse_id.",".$goods_id.",".$qty.",".$transfer_id.") ";
        $db->query($sql);
        return true;
    }

    public function transferQtyCheck()
    {
        global $db, $ecs, $_CFG, $_LANG;

        $transfer_id = $_REQUEST['transfer_id'];

        $sql = "select est.*,esti.*,g.goods_name, g.goods_sn, g.goods_thumb ";
        $sql .= "from  ".$ecs->table('erp_stock_transfer_item')."  esti ";
        $sql .= "left join ". $ecs->table('erp_stock_transfer') ." est on (est.transfer_id = esti.transfer_id) ";
        $sql .= "left join ". $ecs->table('goods') ." g on (esti.goods_id = g.goods_id) ";
        $sql .= "where est.transfer_id = '".$transfer_id."' ";
        $res = $db->getAll($sql);

        $error = [];
        foreach ($res as $item) {
            if ($item['requested_qty'] == 0) {
                $error[] = '貨號:'.$item['goods_sn'].'調貨數量不能為0';
            }
        }

        foreach ($res as $item) {
            $sellable_qty = $goods_sellable_qty = $this->erpController->getSellableProductsQtyByWarehouses($item['goods_id'],$item['warehouse_from']);
            if ($item['requested_qty'] > $sellable_qty) {
                $error[] = '貨號:'.$item['goods_sn'].'庫存不足,沒法進行調貨('.$item['transfer_qty'].'/'.$sellable_qty.')';
            }
        }

        if (empty($error)) {
            $db->query('START TRANSACTION');

            // add record to transfer reserved table
            foreach ($res as $item) {
                $this->transferReservedCreate($item['warehouse_from'], $item['goods_id'], $item['requested_qty'], $transfer_id);
            }
            // change status to PICKING_AND_PACKING
            $this->warehouseTransferChangeStatus($transfer_id, self::PICKING_AND_PACKING);

            // update request stock to transfer stock
            //$sql = "update ".$ecs->table('erp_stock_transfer_item')." set requested_qty = transfer_qty where transfer_id = '".$transfer_id."' ";
            //$db->query($sql);
            $sql = "update ".$ecs->table('erp_stock_transfer_item')." set transfer_qty = 0 where transfer_id = '".$transfer_id."' ";
            $db->query($sql);
            
            $db->query('COMMIT');
            return true;
        } else {
            $result['error'] = $error;
            return $result;
        }
    }

    public function warehouseTransferChangeStatus($transfer_id, $status)
    {
        global $db, $ecs, $_CFG, $_LANG;

        $sql = " update ".$ecs->table('erp_stock_transfer')." set status = ".$status." where transfer_id = ".$transfer_id." ";
        $db->query($sql);

        return true;
    }

    public function transferQtyUpdate()
    {
        global $db, $ecs, $_CFG, $_LANG;

        $transfer_id = $_REQUEST['transfer_id'];
        $item_id = $_REQUEST['item_id'];
        $transfer_qty = $_REQUEST['transfer_qty'];
        
        $sql = "update ".$ecs->table('erp_stock_transfer_item')." set transfer_qty = '".$transfer_qty."' where item_id = '".$item_id."' and transfer_id = '".$transfer_id."' ";
        $db->query($sql);

        return true;
    }

    public function requestedQtyUpdate()
    {
        global $db, $ecs, $_CFG, $_LANG;

        $transfer_id = $_REQUEST['transfer_id'];
        $item_id = $_REQUEST['item_id'];
        $requested_qty = $_REQUEST['requested_qty'];
        
        $sql = "update ".$ecs->table('erp_stock_transfer_item')." set requested_qty = '".$requested_qty."' where item_id = '".$item_id."' and transfer_id = '".$transfer_id."' ";
        $db->query($sql);

        return true;
    }

    public function receivedQtyUpdate()
    {
        global $db, $ecs, $_CFG, $_LANG;

        $transfer_id = $_REQUEST['transfer_id'];
        $item_id = $_REQUEST['item_id'];
        $received_qty = $_REQUEST['received_qty'];
        
        $sql = "update ".$ecs->table('erp_stock_transfer_item')." set received_qty = '".$received_qty."' where item_id = '".$item_id."' and transfer_id = '".$transfer_id."' ";
        $db->query($sql);

        return true;
    }

    public function transferItemDelete()
    {
        global $db, $ecs, $_CFG, $_LANG;
        
        $transfer_id = $_REQUEST['transfer_id'];
        $item_id = $_REQUEST['item_id'];
        
        $this->warehouseTransferStatusActive($transfer_id);

        $sql = "delete from ".$ecs->table('erp_stock_transfer_item')." where item_id = ".$item_id." and transfer_id = ".$transfer_id." ";
        $db->query($sql);

        return true;
    }

    public function transferInfoChangeReasonType()
    {
        global $db, $ecs, $_CFG, $_LANG;
        
        $this->warehouseTransferStatusActive($_REQUEST['transfer_id']);

        $sql = " update ".$ecs->table('erp_stock_transfer')." set warehouse_from = 0,  warehouse_to = 0 , reason_type = ".$_REQUEST['reason_type']." where transfer_id = ".$_REQUEST['transfer_id']." ";
        $db->query($sql);

        return true;
    }

    public function transferInfoChange()
    {
        global $db, $ecs, $_CFG, $_LANG;
        
        $this->warehouseTransferStatusActive($_REQUEST['transfer_id']);

        $sql = " update ".$ecs->table('erp_stock_transfer')." set warehouse_from = ".$_REQUEST['warehouse_from'].",  warehouse_to = ".$_REQUEST['warehouse_to']." , reason_type = ".$_REQUEST['reason_type']." where transfer_id = ".$_REQUEST['transfer_id']." ";
        $db->query($sql);

        return true;
    }

    public function getTransferInfo($transfer_id)
    {
        global $db, $ecs, $_CFG, $_LANG;
        
        $reason_type_options = $this->getReasonTypeOptions();

        $sql = "select * from ".$ecs->table('erp_stock_transfer')." where transfer_id = '".$transfer_id."' ";

        $warehouse_id_to_name = $this->getWarehouseId2Name();
        $user_id_2_info = $this->getUserId2Info();
        $reason_type = $this->getReasonTypeOptions();
                       
        $result = $db->getRow($sql);

        $result['warehouse_from_name'] = $this->getWarehouseNameById($result['warehouse_from']);
        $result['warehouse_to_name'] = $this->getWarehouseNameById($result['warehouse_to']);
        $result['operator'] = $user_id_2_info[$result['operator_id']];
        $result['admin_from_name'] = $user_id_2_info[$result['admin_from']]['user_name'];
        $result['admin_to_name'] = $user_id_2_info[$result['admin_to']]['user_name'];

        $result['reason_type_name'] = $this->getReasonTypeById($result['reason_type']);
        $result['status_name'] = $this->getStatusNameById($result['status']);
        $result['transfer_time'] = local_date($_CFG['time_format'], $result['transfer_time']);
        $result['delivered_time'] = local_date($_CFG['time_format'], $result['delivered_time']);
        $result['create_time'] = local_date($_CFG['date_format'], $result['create_time']);

        // get cross check stock out
        $sql = "select * from ".$ecs->table('erp_stock_transfer_cross_check')." where transfer_id = ".$transfer_id." and transfer_type = 0 ";
        $cross_check_stock_out = $db->getRow($sql);

        if (empty($cross_check_stock_out)) {
            $result['cross_check_status'] = '未核對';
        } else {
            if ($cross_check_stock_out['passed'] == 1) {
                $result['cross_check_status'] = '核對正確';
            }else {
                $result['cross_check_status'] = '核對錯誤';
            }
        }

        // get rma ids
        $sql = "select * from ".$ecs->table('erp_rma')." where repair_transfer_id = ".$transfer_id." or repaired_transfer_id = ".$transfer_id." ";
        $rma_list = $db->getAll($sql); 

        $result['rma_list'] = $rma_list;
        $result['rma_list_count'] = sizeof($rma_list);

        // add barcode invoice no
        require_once(ROOT_PATH . 'includes/php-barcode-generator/src/BarcodeGenerator.php');
        require_once(ROOT_PATH . 'includes/php-barcode-generator/src/BarcodeGeneratorJPG.php');
        $generator = new \Picqer\Barcode\BarcodeGeneratorJPG();
        $result['barcode_transfer_sn'] = '<img  src="data:image/jpeg;base64,' . base64_encode($generator->getBarcode($result['transfer_sn'], $generator::TYPE_CODE_128,1,20)) . '">';

        return $result;
    }

    public function getTransferSn($transfer_id)
    {
        global $db, $ecs, $_CFG, $_LANG;
        
        $sql = "select transfer_sn from ".$ecs->table('erp_stock_transfer')." where transfer_id = '".$transfer_id."' ";
        return $db->getOne($sql);
    }    

    public function warehouseTransferStatusActive($transfer_id)
    {
        global $db, $ecs, $_CFG, $_LANG;
        
        $sql = " update ".$ecs->table('erp_stock_transfer')." set status = 1 where transfer_id = ".$transfer_id." and status = 0 ";
        $db->query($sql);

        return true;
    }

    public function getStatusOptions()
    {
        return self::$statusValues;
    }

    public function getStatusNameById($id)
    {
        $statusValues = self::$statusValues;

        return $statusValues[$id];
    }

    public function getReasonTypeOptions()
    {
        $type_filtered = [];
        foreach (self::$reasonTypeValues as $key => $type) {
            if ($key != 0) {
                $type_filtered[$key] = $type;
            }
        }

        return $type_filtered;
    }

    public function getReasonTypeById($id)
    {
        $reasonTypeValues = self::$reasonTypeValues;

        return $reasonTypeValues[$id];
    }

    public function getDefaultSaleWarehouse()
    {
        return 1;
    }

    public function getDefaultTemporaryWarehouse()
    {
        return 2;
    }

    public function getDefaultSaleTeamPersonInCharge()
    {
        return 64; // steven
    }

    public function getDefaultProductTeamPersonInCharge()
    {
        return 57; //chris
    }

    public function getDefaultWarehouseTeamPersonInCharge()
    {
        return 99; // nelson
    }

    public function getTypeOptions()
    {
        return self::$typeValues;
    }

    public function personInChargeOption($please_select = true)
    {
        global $db, $ecs, $userController ,$_CFG;
        $sql = "SELECT user_id as value,user_name as name FROM ".$ecs->table('admin_user') . ' where is_disable = 0';
        $stocktake_person = $db->getAll($sql);
        if ($please_select) {
            array_unshift($stocktake_person, array('name'=>'請選擇...','value'=>''));
        }

        return $stocktake_person;
    }

    public function personInChargeList()
    {
        global $db, $ecs, $userController ,$_CFG;
        $sql = "SELECT user_id,user_name FROM ".$ecs->table('admin_user'). ' where is_disable = 0';
        $stocktake_person = $db->getAll($sql);
        $person_list = [];
        foreach ($stocktake_person as $person) {
            $person_list[$person['user_id']] = $person['user_name'];
        }
        return $person_list;
    }

    public function updatePersonInCharge($rma_id, $user_id)
    {
        global $db, $ecs, $userController ,$_CFG;

        $sql = "update ".$ecs->table('erp_rma')." set person_in_charge = '".$user_id."' where rma_id = '".$rma_id."' ";
        $db->query($sql);
        return true;
    }

    public function getWareHouseListOptions()
    {
        global $db, $ecs ,$_CFG;

        $erpController = new ErpController();

        $sql="select warehouse_id,name,description,is_on_sale, type, parents_id from ".$this->ecs->table('erp_warehouse')." where 1";
		$sql.=" and is_valid= 1 and (type = 1 or type = 2 ) ";
		$sql.=" order by warehouse_id asc ";
		$res = $this->db->getAll($sql);

        return $res;
    }

    public function adminFromUpdate()
    {
        global $db, $ecs ,$_CFG;
        
        $transfer_id = $_REQUEST['transfer_id'];
        $admin_from = $_REQUEST['admin_from'];

        $sql = "update ".$ecs->table('erp_stock_transfer')." set admin_from = '".$admin_from."' where transfer_id = '".$transfer_id."' ";
        $db->query($sql);
        return true;
    }

    public function getCurrentStatus($transfer_id)
    {
        global $db, $ecs, $_CFG;
    
        $sql = 'select status from '.$ecs->table('erp_stock_transfer').' where transfer_id = "'.$transfer_id.'" ';
        return $status = $db->getOne($sql);
    }

    public function getWarehouseId2Name()
    {
        global $db, $ecs, $userController, $_CFG;

        $sql = "select * from  ".$ecs->table('erp_warehouse')." where 1 ";
        $warehouses = $db->getAll($sql);

        $result = [];
        foreach ($warehouses as $warehouse) {
            $result[$warehouse['warehouse_id']] = $warehouse['name'];
        }

        return $result;
    }

    public function getWarehouseNameById($id)
    {
        global $db, $ecs, $userController, $_CFG;

        $sql = "select name from  ".$ecs->table('erp_warehouse')." where warehouse_id = ".$id." ";
        return $db->getOne($sql);
    }

    public function getUserId2Info()
    {
        global $db, $ecs, $userController, $_CFG;

        $sql = "select user_id,user_name,avatar from  ".$ecs->table('admin_user')." where 1 ";
        $admin_users = $db->getAll($sql);

        $result = [];
        foreach ($admin_users as $user) {
            $result[$user['user_id']] = $user;
        }

        return $result;
    }

    public function getUserNameById($id)
    {
        global $db, $ecs, $_CFG;

        $sql = "select user_name from  ".$ecs->table('admin_user')." where user_id= ".$id." ";
        return $admin_user = $db->getOne($sql);
    }

    public function warehouseTransferCreate()
    {
        global $db, $ecs, $_CFG;

        require_once(dirname(__FILE__) .'/../ERP/lib_erp_warehouse.php');
        require_once(dirname(__FILE__) .'/../ERP/lib_erp_common.php');
        if (!admin_priv('erp_warehouse_manage', '', false)) {
            $result['error'] = $GLOBALS['_LANG']['erp_no_permit'];
            return $result;
        }
       
        $transfer_id=add_transfer(0);

        $sql = "update ".$ecs->table('erp_stock_transfer')." set admin_from = 0 where transfer_id = '".$transfer_id."' ";
        $db->query($sql);

        if ($transfer_id==-1) {
            $result['error']=$GLOBALS['_LANG']['erp_stock_transfer_without_valid_warehouse'];
            return $result;
        } else {
            return $transfer_id;
        }
    }

    public function warehouseTransferCreateByRmaTransfer($rma_id,$type='to_repair_center',$new_one = true)
    {
        global $db, $ecs, $_CFG;
        
        $db->query('START TRANSACTION');
        $rmaController = new RmaController();
        $rma_info = $rmaController->getRmaInfo($rma_id);
        if ($type == 'to_repair_center') {
            $warehouse_from = $rmaController->getRepairTemporaryWarehouse($rma_info['warehouse_id']);     
            $warehouse_to = $rmaController->getRepairTemporaryWarehouse($rma_info['repair_warehouse_id']);  
        } else if ($type == 'to_shop') {
            $warehouse_from = $rmaController->getRepairTemporaryWarehouse($rma_info['repair_warehouse_id']);     
            $warehouse_to = $rmaController->getRepairTemporaryWarehouse($rma_info['warehouse_id']);
        }

        if ($new_one == false){
            // check if have transfer order for repair
            if ($type == 'to_repair_center') {
                $sql= "select transfer_id from ".$ecs->table('erp_stock_transfer')." where warehouse_from = ". $warehouse_from ." and warehouse_to = ". $warehouse_to ." and status = ".self::CREATING." and reason_type = ". self::REASON_TYPE_BAD_GOODS_TO_REPAIR_CENTER . " order by transfer_id desc limit 1 ";
                $transfer_id = $db->getOne($sql);
            } else if ($type == 'to_shop') {
                $sql= "select transfer_id from ".$ecs->table('erp_stock_transfer')." where warehouse_from = ". $warehouse_from ." and warehouse_to = ". $warehouse_to ." and status = ".self::CREATING." and reason_type = " . self::REASON_TYPE_BAD_GOODS_TO_SHOP . " order by transfer_id desc limit 1 ";
                $transfer_id = $db->getOne($sql);
            }

            if (!empty($transfer_id)) {
                $this->saveTransferProducts($transfer_id, $rma_info['goods_id'], $rma_info['qty']);
                $db->query("COMMIT");
                return $transfer_id;
            }
        }

        $transfer_id = $this->warehouseTransferCreate(0);

        if (isset($transfer_id['error'])) {
            return $transfer_id;
        }

        // get temp warehouse id
        if ($type == 'to_repair_center') {
            $warehouse_from = $rmaController->getRepairTemporaryWarehouse($rma_info['warehouse_id']);     
            $warehouse_to = $rmaController->getRepairTemporaryWarehouse($rma_info['repair_warehouse_id']);     
            $sql = "update ".$ecs->table('erp_stock_transfer')." set admin_from = 1, auto_created = 1, status = ".self::CREATING.",  warehouse_from = ".$warehouse_from.",  warehouse_to = ".$warehouse_to." , reason_type = ".self::REASON_TYPE_BAD_GOODS_TO_REPAIR_CENTER." where transfer_id = '".$transfer_id."' ";
            $db->query($sql);
        } else if ($type == 'to_shop') {
            $warehouse_from = $rmaController->getRepairTemporaryWarehouse($rma_info['repair_warehouse_id']);     
            $warehouse_to = $rmaController->getRepairTemporaryWarehouse($rma_info['warehouse_id']);     
            $sql = "update ".$ecs->table('erp_stock_transfer')." set admin_from = 1, auto_created = 1, status = ".self::CREATING.",  warehouse_from = ".$warehouse_from.",  warehouse_to = ".$warehouse_to." , reason_type = ".self::REASON_TYPE_BAD_GOODS_TO_SHOP." where transfer_id = '".$transfer_id."' ";
            $db->query($sql);
        }
      
        $this->saveTransferProducts($transfer_id, $rma_info['goods_id'], $rma_info['qty']);

        //$this->warehouseTransferChangeStatus($transfer_id, self::PICKING_AND_PACKING);

        // update request stock to transfer stock
        //$sql = "update ".$ecs->table('erp_stock_transfer_item')." set requested_qty = transfer_qty where transfer_id = '".$transfer_id."' ";
        //$db->query($sql);
        $db->query("COMMIT");
        return $transfer_id;
    }

    public function warehouseTransferCreateByGoodsTransfer($warehouse_from, $warehouse_to, $transfer_goods, $transfer_type = 0, $operator = null, $auto_created = 1)
    {
        global $db, $ecs, $_CFG;
        $transfer_id = $this->warehouseTransferCreate(0);

        if (isset($transfer_id['error'])) {
            return $transfer_id;
        }

        $sql = "update ".$ecs->table('erp_stock_transfer')." set admin_from = 1, auto_created = ".$auto_created." , status = ".self::CREATING.",  warehouse_from = ".$warehouse_from.",  warehouse_to = ".$warehouse_to." , reason_type = ".$transfer_type." where transfer_id = '".$transfer_id."' ";
        $db->query($sql);

        foreach ($transfer_goods as $goods_id => $goods_qty) {
            $this->saveTransferProducts($transfer_id, $goods_id, $goods_qty);
        }

        foreach ($transfer_goods as $goods_id => $goods_qty) {
            $this->transferReservedCreate($warehouse_from, $goods_id, $goods_qty, $transfer_id);
        }

        // change status to PICKING_AND_PACKING 5
        $this->warehouseTransferChangeStatus($transfer_id, self::REVIEW);

        // update request stock to transfer stock
        //$sql = "update ".$ecs->table('erp_stock_transfer_item')." set requested_qty = transfer_qty where transfer_id = '".$transfer_id."' ";
        //$db->query($sql);

        if (!empty($operator)) {
            $sql = "update ".$ecs->table('erp_stock_transfer')." set operator_id = ".$operator." where transfer_id = '".$transfer_id."' ";
            $db->query($sql);
        }

        return $transfer_id;
    }

    public function transportInfoUpdate()
    {
        global $db, $ecs, $_CFG;

        $sql = "update ".$ecs->table('erp_stock_transfer')." set transport_cost = '".$_REQUEST['transport_cost']."' where transfer_id = '".$_REQUEST['transfer_id']."' ";
        $db->query($sql);

        $sql = "update ".$ecs->table('erp_stock_transfer')." set sender_remark = '".$_REQUEST['sender_remark']."' where transfer_id = '".$_REQUEST['transfer_id']."' ";
        $db->query($sql);

        return false;
    }

    public function transportReceiverRemarkUpdate()
    {
        global $db, $ecs, $_CFG;
        $sql = "update ".$ecs->table('erp_stock_transfer'). " set admin_to = ".$_REQUEST['admin_to']." , receiver_remark = '".$_REQUEST['receiver_remark']."' where transfer_id = ".$_REQUEST['transfer_id']." ";
        $res = $db->query($sql);
        return true;
    }

    public function transportAccepted()
    {
        global $db, $ecs, $_CFG;

        $db->query('START TRANSACTION');
        
        $transfer_id = $_REQUEST['transfer_id'];
        // change status to TRANSFERING 2
        $this->warehouseTransferChangeStatus($transfer_id, self::TRANSFERING);

        // update transfer reserved table
        $sql = "update ".$ecs->table('erp_stock_transfer_reserved')." set status = ".self::TRANSFER_RESERVED_TRANSFERRING." where transfer_id = ".$transfer_id." ";
        $db->query($sql);
        
        $db->query("COMMIT");
        return true;
    }

    public function transportDelivered()
    {
        global $db, $ecs, $_CFG, $_LANG;

        $transfer_id = $_REQUEST['transfer_id'];

        $transfer_info = $this->getTransferInfo($transfer_id);

        $db->query('START TRANSACTION');

        $_REQUEST['admin_to'] = erp_get_admin_id();

        $sql = "update ".$ecs->table('erp_stock_transfer'). " set admin_to = ".$_REQUEST['admin_to']." , receiver_remark = '".$_REQUEST['receiver_remark']."' where transfer_id = ".$transfer_id." ";
        $res = $db->query($sql);

        // change status to RECEIVED 3
        $this->warehouseTransferChangeStatus($transfer_id, self::TRANSFER_STOCK_IN);

        $sql = "update ".$ecs->table('erp_stock_transfer')." set delivered_time = '".gmtime()."' where transfer_id = '".$transfer_id."' ";
        $db->query($sql);

        $sql = "update ".$ecs->table('erp_stock_transfer_item')." set received_qty = transfer_qty where transfer_id = '".$transfer_id."' ";
        $db->query($sql);

        // update transfer reserved table
        $sql = "update ".$ecs->table('erp_stock_transfer_reserved')." set status = ".self::TRANSFER_RESERVED_DELIVERED ." where transfer_id = ".$transfer_id." ";
        $db->query($sql);

        $db->query("COMMIT");
        return true;
    }

    public function transferStockIn()
    {
        global $db, $ecs, $_CFG, $_LANG;

        $transfer_id = $_REQUEST['transfer_id'];
        $transfer_info = $this->getTransferInfo($transfer_id);

        $db->query('START TRANSACTION');

        $_REQUEST['admin_to'] = erp_get_admin_id();

        //$sql = "update ".$ecs->table('erp_stock_transfer'). " set admin_to = ".$_REQUEST['admin_to']." , receiver_remark = '".$_REQUEST['receiver_remark']."' where transfer_id = ".$transfer_id." ";
        //$res = $db->query($sql);

        $sql = "select est.*,esti.*,g.goods_name, g.goods_sn, g.goods_thumb ";
        $sql .= "from  ".$ecs->table('erp_stock_transfer_item')."  esti ";
        $sql .= "left join ". $ecs->table('erp_stock_transfer') ." est on (est.transfer_id = esti.transfer_id) ";
        $sql .= "left join ". $ecs->table('goods') ." g on (esti.goods_id = g.goods_id) ";
        $sql .= "where est.transfer_id = '".$transfer_id."' ";
        $res = $db->getAll($sql);
        $transferItems = [];
        $error = [];
        foreach ($res as $item) {
            if ($item['received_qty'] > 0) {
                $attrId = get_attr_id($item['goods_id']);
                $transferItems[] = $item['goods_id'];
                $items[]="(".$item['transfer_id'].",".$item['goods_id'].",".$attrId.",".$item['received_qty'].")";
                $toWsStock=get_goods_stock_by_warehouse($item['warehouse_to'], $item['goods_id']);

                $wdName = $this->getUserNameById($item['admin_to']);

                $stockQueries=[];
                
                $sql="INSERT INTO ".$ecs->table('erp_stock')." SET goods_id='".$item['goods_id']."',goods_attr_id='".$attrId."',qty='".($item['received_qty'])."',act_time='".gmtime()."',warehouse_id='".$item['warehouse_to']."'";
                $sql.=", w_d_name='".$wdName."', w_d_style_id='".$this->erpController->getWarehousingStyleIdByName('調倉')."',order_sn='".$item['transfer_sn']."',stock_style='w',w_d_sn='".$item['transfer_sn']."',stock=".($toWsStock+$item['received_qty']).",act_date='".local_date('Y-m-d', gmtime())."'";
             
                if ($transfer_info['rma_id'] > 0) {
                    $sql.=",rma_id = ".$transfer_info['rma_id']."  ";
                }

                $stockQueries[]=$sql;
                //2-3-b attr
                $sql="UPDATE ".$ecs->table('erp_goods_attr_stock')." SET goods_qty =goods_qty +".$item['received_qty']." where goods_id='".$item['goods_id']."' and attr_id='".$attrId."' and warehouse_id='".$item['warehouse_to']."'";
                $stockQueries[]=$sql;

                foreach ($stockQueries as $query) {
                    if (!$db->query($query)) {
                        $result['error'] = $_LANG['erp_stock_transfer_confirm_failed'];
                        return $result;
                    }
                }
            }
        }

        admin_log($wdName .'-' .$transfer_id, 'edit', 'goods_stock');

        // change status to RECEIVED 3
        $this->warehouseTransferChangeStatus($transfer_id, self::RECEIVED);

        // update transfer reserved table
        $sql = "delete from ".$ecs->table('erp_stock_transfer_reserved')." where transfer_id = ".$transfer_id." ";
        $db->query($sql);

        // if transfer reason type = stocre pick up
        if ($transfer_info['reason_type'] == self::REASON_TYPE_STORE_PICK_UP || $transfer_info['reason_type'] == self::REASON_TYPE_ORDER_OUT_OF_STOCK) {
            // update order_goods_transfer_reserved
            $orderController =$this->orderController;
            $this->orderController->changeOrderGoodsTransferReservedStatus($transfer_id,$orderController::ORDER_GOODS_RESERVED_STATUS_FINISHED);

            $sql = 'select * from '.$ecs->table('order_goods_transfer_reserved').' where transfer_id = '.$transfer_id.'  ';
            $result_order_goods_transfer_reserved = $db->getAll($sql);

            if ($transfer_info['reason_type'] == self::REASON_TYPE_STORE_PICK_UP) {
                foreach ($result_order_goods_transfer_reserved as $order_goods) {
                    $this->orderController->updateOrderGoodsWarehouseId($order_goods['order_id'],$order_goods['warehouse_to']);
                }
            }            
        }
      
        $db->query("COMMIT");

        // update rma status  
        // get transfer's rma id
        $sql = "select * from ".$ecs->table('erp_rma')." er where repair_transfer_id = ".$transfer_id." ";
        $rmas = $db->getAll($sql);
        $rmaController = new RmaController();
        foreach ($rmas as $rma_info) {
            if ($transfer_info['reason_type'] == self::REASON_TYPE_BAD_GOODS_TO_REPAIR_CENTER){ // REASON_TYPE_BAD_GOODS_TO_REPAIR_CENTER
                $sql = "update ".$ecs->table('erp_rma')." set person_in_charge = ".$rma_info['goods_admin'].", status = ".$rmaController::NOTIFY_SUPPLIER." where repair_transfer_id = ".$transfer_id."  and rma_id = ".$rma_info['rma_id'] ."  ";
               
                $db->query($sql);
                $rmaController->rmaActionLog($rma_info['rma_id'], erp_get_admin_id(), '己調貨完成', $currentStatus);
            }
        }

        $sql = "select * from ".$ecs->table('erp_rma')." er where repaired_transfer_id = ".$transfer_id." ";
        $rmas = $db->getAll($sql);
        $rmaController = new RmaController();
        foreach ($rmas as $rma_info) {
            $rma_id = $rma_info['rma_id'];
            if ($transfer_info['reason_type'] == self::REASON_TYPE_BAD_GOODS_TO_SHOP){ // REASON_TYPE_BAD_GOODS_TO_SHOP
                $sql = "update ".$ecs->table('erp_rma')." set status = ".$rmaController::FINISHED." where repair_transfer_id = ".$transfer_id."  and rma_id = ".$rma_info['rma_id'] ."  ";
                $db->query($sql);
                $rmaController->rmaActionLog($transfer_info['rma_id'], erp_get_admin_id(), '己調貨完成', $currentStatus);

                // temp warehouse to customer
                $actg_txn_id = $rmaController->rmaCustomerReadyToPick($rma_id);
                if (isset($actg_txn_id['error'])) {
                    return $actg_txn_id;
                } else {
                    //$temp_warehouse_id = $rmaController->getDefaultTemporaryWarehouse();
                    $temp_warehouse_id =  $rmaController->getRepairTemporaryWarehouse($rma_info['warehouse_id']);

                    $currentStatus = $rmaController->getCurrentStatus($rma_id);
                    $rmaController->updateRepairOrderStatus($rma_info['repair_id'], 4);
                    $result = $rmaController->rmaChangeStatus($rma_id, $rmaController::FINISHED);
                    $rmaController->rmaActionLog($rma_id, $user_id, $final_note, $currentStatus);
                    $rmaController->saveStepStockLog($rma_id, 5, $rma_info['status'], $rma_info['goods_id'], $temp_warehouse_id, $rma_info['qty']*-1,$actg_txn_id);
                    return true;
                }
            }
        }

        // if ($transfer_info['rma_id'] > 0) {
        //     $rmaController = new RmaController();
        //     $currentStatus = $rmaController->getCurrentStatus($transfer_info['rma_id']);
        //     if ($transfer_info['reason_type'] == self::REASON_TYPE_BAD_GOODS_TO_SHOP){ // REASON_TYPE_BAD_GOODS_TO_SHOP
        //         $sql = "update ".$ecs->table('erp_rma')." set status = ".$rmaController::REPAIRED."  where repaired_transfer_id = ".$transfer_id." and rma_id = ".$transfer_info['rma_id'] ."  ";
        //         $db->query($sql);
        //         $rmaController->rmaActionLog($transfer_info['rma_id'], erp_get_admin_id(), '己調貨完成', $currentStatus);
        //     } else if ($transfer_info['reason_type'] == self::REASON_TYPE_BAD_GOODS_TO_REPAIR_CENTER){ // REASON_TYPE_BAD_GOODS_TO_REPAIR_CENTER
        //         $sql = "update ".$ecs->table('erp_rma')." set status = ".$rmaController::PENDING_FOR_CHECKING." where repair_transfer_id = ".$transfer_id."  and rma_id = ".$transfer_info['rma_id'] ."  ";
        //         $db->query($sql);
        //         $rmaController->rmaActionLog($transfer_info['rma_id'], erp_get_admin_id(), '己調貨完成', $currentStatus);
        //     }
        // }

        if ($transfer_info['reason_type'] != self::REASON_TYPE_BAD_GOODS_TO_SHOP && $transfer_info['reason_type'] != self::REASON_TYPE_BAD_GOODS_TO_REPAIR_CENTER) {
            $orderProgressController = new OrderProgressController();
            $orderProgressController->checkOrderAllInStock('TR',$transfer_id,$transferItems);    
        }
        
        return true;
    }

    public function transferTypeChange() 
    {
        global $db, $ecs, $userController ,$_CFG;

        if (!empty($_REQUEST['transfer_id']) && $_REQUEST['change_type'] != '' ) {
            $sql = "update ".$ecs->table('erp_stock_transfer')." set type = '".$_REQUEST['change_type']."' where transfer_id = '".$_REQUEST['transfer_id']."' ";
            $db->query($sql);
        }

        return true;
    }

    public function getWarehouseTransferList($is_pagination = true)
    {
        global $db, $ecs, $userController, $_CFG;

        $_REQUEST['type'] = !isset($_REQUEST['type']) ? '1' : $_REQUEST['type'];

        // total record
        $sql="select count(*) from ".$ecs->table('erp_stock_transfer')." as s where 1";
        if (isset($_REQUEST['status']) &&!empty($_REQUEST['status'] && $_REQUEST['status'] != -1)) {
            $sql.=" and s.status='".$_REQUEST['status']."' ";
        }
        if (isset($_REQUEST['transfer_sn']) &&!empty($_REQUEST['transfer_sn'])) {
            $sql.=" and s.transfer_sn='".$_REQUEST['transfer_sn']."'";
        }
        if (isset($_REQUEST['reason_type']) && $_REQUEST['reason_type'] != -1 ) {
            $sql.=" and s.reason_type='".$_REQUEST['reason_type']."'";
        }
        if (isset($_REQUEST['start_time']) &&!empty($_REQUEST['start_time'])) {
            $sql.=" and s.create_time>='".$_REQUEST['start_time']."'";
        }
        if (isset($_REQUEST['end_time']) &&!empty($_REQUEST['end_time'])) {
            $sql.=" and s.create_time<='".$_REQUEST['end_time']."'";
        }
        if (isset($_REQUEST['warehouse_from']) && $_REQUEST['warehouse_from'] != -1) {
            $sql.=" and s.warehouse_from='".$_REQUEST['warehouse_from']."'";
        }
        if (isset($_REQUEST['warehouse_to']) && $_REQUEST['warehouse_to'] != -1) {
            $sql.=" and s.warehouse_to='".$_REQUEST['warehouse_to']."'";
        }
        if (isset($_REQUEST['operator_id'])  && $_REQUEST['operator_id'] != -1) {
            $sql.=" and s.operator_id='".$_REQUEST['operator_id']."'";
        }
        if (isset($_REQUEST['admin_from']) &&!empty($_REQUEST['admin_from'])) {
            $sql.=" and s.admin_from='".$_REQUEST['admin_from']."'";
        }
        if (isset($_REQUEST['admin_to']) &&!empty($_REQUEST['admin_to'])) {
            $sql.=" and s.admin_to='".$_REQUEST['admin_to']."'";
        }
        if (isset($_REQUEST['goods_sn']) &&!empty($_REQUEST['goods_sn'])) {
            $sql.=" and s.transfer_id in (select i.transfer_id from ".$ecs->table('erp_stock_transfer_item')." as i,".$ecs->table('goods')." as g where g.goods_sn like '%".$_REQUEST['goods_sn']."%' and g.goods_id=i.goods_id)";
        }
        if (isset($_REQUEST['goods_name']) &&!empty($_REQUEST['goods_name'])) {
            $sql.=" and s.transfer_id in (select i.transfer_id from ".$ecs->table('erp_stock_transfer_item')." as i,".$ecs->table('goods')." as g where g.goods_name like '%".$_REQUEST['goods_name']."%' and g.goods_id=i.goods_id)";
        }
        if (isset($_REQUEST['agency_id']) &&intval($_REQUEST['agency_id']) >0) {
            $sql.=" and (warehouse_from in (select warehouse_id from ".$ecs->table('erp_warehouse')." where agency_id='".$_REQUEST['agency_id']."') or warehouse_to in (select warehouse_id from ".$ecs->table('erp_warehouse')." where agency_id='".$_REQUEST['agency_id']."'))";
        }

        if (isset($_REQUEST['type'])&& $_REQUEST['type'] != -1) {
            $sql.=" and s.type='".$_REQUEST['type']."'";
        }     

        $sql.=" and s.reason_type != 0 ";

        $total_count = $db->getOne($sql);
        
        $_REQUEST['record_count'] = $total_count;
        $_REQUEST['sort_by']    = empty($_REQUEST['sort_by']) ? 'transfer_id' : trim($_REQUEST['sort_by']);
        $_REQUEST['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
        //$_REQUEST['status'] = (!isset($_REQUEST['status']) || isset($_REQUEST['status']) && $_REQUEST['status'] == '-1') ? '-1' : $_REQUEST['status'];
       // $_REQUEST['type'] = !isset($_REQUEST['type']) ? '1' : $_REQUEST['type'];

        // pagination
        $sql="select s.* from ".$ecs->table('erp_stock_transfer')." as s where 1";
        if (isset($_REQUEST['transfer_sn']) &&!empty($_REQUEST['transfer_sn'])) {
            $sql.=" and s.transfer_sn='".$_REQUEST['transfer_sn']."'";
        }

        if (isset($_REQUEST['status']) &&!empty($_REQUEST['status'])  && $_REQUEST['status'] != -1) {
            $sql.=" and s.status='".$_REQUEST['status']."' ";
        }

        if (isset($_REQUEST['reason_type']) && $_REQUEST['reason_type'] != -1 ) {
            $sql.=" and s.reason_type='".$_REQUEST['reason_type']."'";
        }

        if (isset($_REQUEST['start_time']) &&!empty($_REQUEST['start_time'])) {
            $sql.=" and s.create_time>='".$_REQUEST['start_time']."'";
        }
        if (isset($_REQUEST['end_time']) &&!empty($_REQUEST['end_time'])) {
            $sql.=" and s.create_time<='".$_REQUEST['end_time']."'";
        }
        if (isset($_REQUEST['warehouse_from']) && $_REQUEST['warehouse_from'] != -1) {
            $sql.=" and s.warehouse_from='".$_REQUEST['warehouse_from']."'";
        }
        if (isset($_REQUEST['warehouse_to']) && $_REQUEST['warehouse_to'] != -1) {
            $sql.=" and s.warehouse_to='".$_REQUEST['warehouse_to']."'";
        }
        if (isset($_REQUEST['operator_id'])  && $_REQUEST['operator_id'] != -1) {
            $sql.=" and s.operator_id='".$_REQUEST['operator_id']."'";
        }
        if (isset($_REQUEST['admin_from']) &&!empty($_REQUEST['admin_from'])) {
            $sql.=" and s.admin_from='".$_REQUEST['admin_from']."'";
        }
        if (isset($_REQUEST['admin_to']) &&!empty($_REQUEST['admin_to'])) {
            $sql.=" and s.admin_to='".$_REQUEST['admin_to']."'";
        }
        if (isset($_REQUEST['goods_sn']) &&!empty($_REQUEST['goods_sn'])) {
            $sql.=" and s.transfer_id in (select i.transfer_id from ".$ecs->table('erp_stock_transfer_item')." as i,".$ecs->table('goods')." as g where g.goods_sn like '%".$_REQUEST['goods_sn']."%' and g.goods_id=i.goods_id)";
        }
        if (isset($_REQUEST['goods_name']) &&!empty($_REQUEST['goods_name'])) {
            $sql.=" and s.transfer_id in (select i.transfer_id from ".$ecs->table('erp_stock_transfer_item')." as i,".$ecs->table('goods')." as g where g.goods_name like '%".$_REQUEST['goods_name']."%' and g.goods_id=i.goods_id)";
        }
        if (isset($_REQUEST['agency_id']) &&intval($_REQUEST['agency_id']) >0) {
            $sql.=" and (warehouse_from in (select warehouse_id from ".$ecs->table('erp_warehouse')." where agency_id='".$_REQUEST['agency_id']."') or warehouse_to in (select warehouse_id from ".$ecs->table('erp_warehouse')." where agency_id='".$_REQUEST['agency_id']."'))";
        }

        if (isset($_REQUEST['type'])&& $_REQUEST['type'] != -1) {
            $sql.=" and s.type='".$_REQUEST['type']."'";
        } 

        $sql.=" and s.reason_type != 0 ";
        $sql .= ' and s.status != 0 ';
        $sql .= ' order by '. $_REQUEST['sort_by'] . ' ' .  $_REQUEST['sort_order'];
        $sql .= ($is_pagination ? " LIMIT " . $_REQUEST['start'] . ", " . $_REQUEST['page_size'] : '');
        
        $res = $db->getAll($sql);

        $statusOptions = $this->getStatusOptions();
        $reasonTypeOptions = $this->getReasonTypeOptions();
        $warehouse_id_to_name = $this->getWarehouseId2Name();
        $user_id_2_info = $this->getUserId2Info();
        foreach ($res as $key => &$row) {
            // image
            $row['warehouse_from_name'] = $warehouse_id_to_name[$row['warehouse_from']];
            $row['warehouse_to_name'] = $warehouse_id_to_name[$row['warehouse_to']];
            $row['operator_avatar'] = '<img class="img-circle profile_img" style="margin-left: 0%;" title="'.$user_id_2_info[$row['operator_id']]['user_name'].'" src="'.(empty($user_id_2_info[$row['operator_id']]['avatar']) ? '/uploads/avatars/default_60x60.png' : '/'.$user_id_2_info[$row['operator_id']]['avatar']).'" >';
            if (!empty($user_id_2_info[$row['admin_from']]['user_name'])) {
                $row['admin_from_avatar'] = '<img class="img-circle profile_img" style="margin-left: 0%;" title="'.$user_id_2_info[$row['admin_from']]['user_name'].'" src="'.(empty($user_id_2_info[$row['admin_from']]['avatar']) ? '/uploads/avatars/default_60x60.png' : '/'.$user_id_2_info[$row['admin_from']]['avatar']).'" >';
            } else {
                $row['admin_from_avatar'] = '';
            }
            if (!empty($user_id_2_info[$row['admin_to']]['user_name'])) {
                 $row['admin_to_avatar'] = '<img class="img-circle profile_img" style="margin-left: 0%;" title="'.$user_id_2_info[$row['admin_to']]['user_name'].'" src="'.(empty($user_id_2_info[$row['admin_to']]['avatar']) ? '/uploads/avatars/default_60x60.png' : '/'.$user_id_2_info[$row['admin_to']]['avatar']).'" >';
            } else {
                $row['admin_to_avatar'] = '';
            }
           
            $row['out_of_stock'] = '';
            // $stocktake_progress_html = '<div class="widget_summary">
            //         <div class="w_center w_55" style="width: 100%;">
            //           <div class="progress">
            //             <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" title = "'.$row['stocktake_finished_rate'].'%" style="width: '.$row['stocktake_finished_rate'].'%;">
            //               <span class="sr-only">60% Complete</span>
            //             </div>
            //           </div>
            //         </div>
            //         <div class="clearfix"></div>
            //       </div>';
            // $reviw_progress_html = '<div class="widget_summary">
            //         <div class="w_center w_55" style="width: 100%;">
            //           <div class="progress">
            //             <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" title = "'.$row['review_finished_rate'].'%" style="width: '.$row['review_finished_rate'].'%;">
            //               <span class="sr-only">60% Complete</span>
            //             </div>
            //           </div>
            //         </div>
            //         <div class="clearfix"></div>
            //       </div>';

            $row['follow_up_remark_1'] = '<span class="follow_up_remark" data-transfer-id="'.$row['transfer_id'].'" data-toggle="modal" data-target="#popup_follow_up_remark_modal" >'.(empty($row['follow_up_remark'])? '請輸入' : $row['follow_up_remark']).'</span>';
            
            $actions_html = '<ul class="nav navbar-right panel_toolbox">';
            $actions_html .= '<li><a title="View" class="collapse-link" href="erp_warehouse_transfer_create.php?act=list&amp;transfer_id='.$row['transfer_id'].'"><i class="fa fa-eye green" style="font-size: 20px;"></i></a></li>';

            if ($row['type'] != 1) {
                $actions_html .= '<li><a data-transfer-id='.$row['transfer_id'].' data-change-type="1" title="Undo" class="collapse-link confirm_action" href="javascript:void(0);"><i class="fa fa-undo " style="font-size: 20px; color:#f0ad4e""></i></a></li>'; 
            } else {
                $actions_html .= '<li><a data-transfer-id='.$row['transfer_id'].' data-change-type="0" title="Archive" class="collapse-link confirm_action" href="javascript:void(0);"><i class="fa fa-archive " style="font-size: 20px; color:#f0ad4e""></i></a></li>';
                $actions_html .= '<li><a data-transfer-id='.$row['transfer_id'].' data-change-type="2" title="Delete" class="collapse-link confirm_action" href="javascript:void(0);"><i class="fa fa-trash red" style="font-size: 20px;"></i></a></li>';
            }

            $actions_html .= '<li><a target="_blank" title="Print" class="collapse-link" href="erp_stock_transfer_manage.php?act=print&amp;transfer_id='.$row['transfer_id'].'"><i class="fa fa-print grey" style="font-size: 20px;"></i></a></li>';
            $actions_html .= '</ul>';


            $row['create_time'] = local_date($_CFG['time_format'], $row['create_time']);
            $row['transfer_time'] = local_date($_CFG['time_format'], $row['transfer_time']);
            if ($row['delivered_time'] > 0) {
                $row['delivered_time'] = local_date($_CFG['time_format'], $row['delivered_time']);
            } else {
                $row['delivered_time'] = '';
            }

            // check if transfer qty != received qty
            $sql = "select sti.*, esta.status as adjust_status from ".$ecs->table('erp_stock_transfer_item')." sti left join ".$ecs->table('erp_stock_transfer_adjustment')." esta on (sti.transfer_id = esta.transfer_id and sti.goods_id = esta.goods_id) where sti.transfer_id = ".$row['transfer_id']." ";
            $transfer_goods = $db->getAll($sql); 
            
         

            $transfer_goods_qty_is_correct = true; 
            foreach ($transfer_goods as $goods) {
                if ($goods['transfer_qty'] != $goods['received_qty']) {
                    if ($goods['adjust_status'] != 1) {
                        $transfer_goods_qty_is_correct = false;
                    }
                }
            }

            if ($row['status'] == SELF::RECEIVED || $row['status'] == SELF::CANCELLED) {
                if ($transfer_goods_qty_is_correct == false && $row['status'] == SELF::RECEIVED) {
                    $tr_class_block_alert = 'tr_is_not_correct_block';
                } else {
                    $tr_class_block_alert = 'tr_finished_block';
                }
            } else {
                $tr_class_block_alert = '';
            }
            
            $row['status'] = '<span class=" '. $tr_class_block_alert .' ">'.$statusOptions[$row['status']].'</a>';
            $row['reason_type'] = $reasonTypeOptions[$row['reason_type']];
            $row['avatar'] = '<img title="'.$row['user_name'].'" src="'.(empty($row['avatar']) ? '/uploads/avatars/default_60x60.png' : '/'.$row['avatar']).'" >';
            //$row['stocktake_progress'] = $stocktake_progress_html;
            $row['review_progress'] = $reviw_progress_html;
            $row['image'] = '<a target="_blank" href="../goods.php?id='.$row['goods_id'].'"><img width=50px src="./../'.$row['goods_thumb'].'"/>';
            $row['actions'] = $actions_html;
            //$row['remark2'] = '<span class="follow_up_remark" data-rma-id="'.$row['rma_id'].'" data-toggle="modal" data-target="#popup_follow_up_remark_modal" >'.(empty($row['remark2'])? '請輸入' : $row['remark2']).'</span>';
            
            // if ($row['pick_new'] == 1) {
            //     $row['customer_pick_up_status'] = '等待取貨<button type="button" class="btn btn-success btn-xs">確定客戶取貨</button>';
            // }
            //$row['repair_fee'] = '$'.$row['repair_fee'];

            // if ($row['repair_order_status'] == 4) {  // 完成及可出貨
            //     if ($row['pickup_time'] == 0) {
            //         $row['pickup_status'] = '待取貨 <button type="button" data-rma-id="'.$row['rma_id'].'" class="btn btn-success btn-xs confirm-pickup">確定客戶取貨</button>';
            //     } else {
            //         $row['pickup_status'] = '客人已取貨<br>('.local_date($_CFG['time_format'], $row['pickup_time']).')';
            //     }
            // } else {
            //     $row['pickup_status'] = '';
            // }
        }
        // $sort_order  = $_REQUEST['sort_order'] == 'ASC' ? SORT_ASC : SORT_DESC;
        // array_multisort($sort, $sort_order, $res);



        $arr = array(
            'data' => $res,
            'record_count' => $_REQUEST['record_count']
        );

        return $arr;
    }

    function getGoodsTransferReservedWarehouseToByReasonType($warehouse_id,$reason_type)
    {
        global $db, $ecs, $userController, $_CFG;

        $sql = "select sum(qty) as total,goods_id from ".$ecs->table('erp_stock_transfer_reserved')." estr left join ". $ecs->table('erp_stock_transfer') ." est on (estr.transfer_id = est.transfer_id) where warehouse_to = ".$warehouse_id." and reason_type = ".$reason_type." and est.status != ".self::PENDING." and est.status != ".self::CREATING." group by goods_id ";
        $res = $db->getAll($sql);

        $goodsReserved = [];
        foreach ($res as $goods) {
            $goodsReserved[$goods['goods_id']] = $goods['total'];
        }

        return $goodsReserved;
    }

    public function warehouseTransferFollowUpRemarkSubmit()
    {
        global $db, $ecs, $userController, $_CFG;

        $transfer_id = $_REQUEST['transfer_id'];
        
        if (!empty($transfer_id)) {
            $follow_up_remark = mysql_real_escape_string($_REQUEST['follow_up_remark']);
            $sql = 'update '.$ecs->table('erp_stock_transfer').' set follow_up_remark = "'.$follow_up_remark.'" where transfer_id = "'.$transfer_id.'" ';
            $db->query($sql);
            return true;
        }
    }

    public function getShopToWarehouseSuggestionTransferList($is_pagination=false)
    {
        global $db, $ecs, $userController ,$_CFG;
        require_once(ROOT_PATH . 'includes/lib_order.php');

        $main_warehouse_id = $this->erpController->getMainWarehouseId();
        $shop_warehouse_id = $this->erpController->getShopWarehouseId();

        //$main_warehouse_name = $this->erpController->getWarehouseNameById($main_warehouse_id);
        //$shop_warehouse_name = $this->erpController->getWarehouseNameById($shop_warehouse_id);

        // shop > 3
		$sql = "SELECT eg.goods_id, eg.goods_sn, eg.goods_name, egas.warehouse_id, egas.goods_qty, eg.goods_thumb  FROM  ". $this->ecs->table('goods')  ." eg left join " . $this->ecs->table('erp_goods_attr_stock') . " egas on (eg.goods_id = egas.goods_id) WHERE (egas.warehouse_id  = ".$shop_warehouse_id." || egas.warehouse_id  = ".$main_warehouse_id." ) and egas.goods_qty != 0 ";
		$res = $this->db->getAll($sql);
        
        $target_goods_stock = [];
        $target_goods_info = [];
        foreach ($res as $goods) {
            $target_goods_info[$goods['goods_id']] = $goods;

            if (!isset($target_goods_stock[$goods['goods_id']][$main_warehouse_id])) {
                $target_goods_stock[$goods['goods_id']][$main_warehouse_id] = 0;
            }
            if (!isset($target_goods_stock[$goods['goods_id']][$shop_warehouse_id])) {
                $target_goods_stock[$goods['goods_id']][$shop_warehouse_id] = 0;
            }
            $target_goods_stock[$goods['goods_id']][$goods['warehouse_id']] = $goods['goods_qty'];
        }

        $transfer_goods = [];
        foreach ($target_goods_stock as $target_goods_stock_key => $target_goods_stock_info) {
            if ($target_goods_stock_info[$shop_warehouse_id] > $target_goods_stock_info[$main_warehouse_id]) {
                if ($target_goods_stock_info[$main_warehouse_id] < 20) {
                    $transfer_qty = $target_goods_stock_info[$shop_warehouse_id] - floor(($target_goods_stock_info[$shop_warehouse_id] + $target_goods_stock_info[$main_warehouse_id]) * 30 / 100);
                    if ($transfer_qty <= 2) {
                        $transfer_qty = 0;
                    }
                } else {
                    $transfer_qty = 0;
                }

                $hidden_field = '';
                $goods_thumb = '<img width="50" src="../'.$target_goods_info[$target_goods_stock_key]['goods_thumb'].'"/>';
                $hidden_field .= '<input type="hidden" value="'.$target_goods_stock_key.'" name="goods_id[]" />';
                $transfer_qty_html = $hidden_field.'<input name="transfer_qty[]" type="text" size="5" value="'.$transfer_qty.'" class="form-control '.($transfer_qty > 0? 'tr_alert_block' : '' ).' " />';
                $transfer_goods[] = array('goods_id' => $target_goods_stock_key,
                                        'transfer_qty' =>$transfer_qty, 
                                        'shop_warehouse_qty' => $target_goods_stock_info[$shop_warehouse_id], 
                                        'main_warehouse_qty' => $target_goods_stock_info[$main_warehouse_id],
                                        'goods_sn' => $target_goods_info[$target_goods_stock_key]['goods_sn'],
                                        'goods_name' => $target_goods_info[$target_goods_stock_key]['goods_name'],
                                        'goods_thumb' => $goods_thumb,
                                        'transfer_qty_html' =>$transfer_qty_html
                                    ) ; 

               
            }
        }

        usort($transfer_goods, function ($a, $b) {
            return $a['main_warehouse_qty'] - $b['main_warehouse_qty'];
        });

        $arr = array(
            'data' => $transfer_goods,
            'record_count' => sizeof($transfer_goods)
        );

        return $arr;
    }

    public function shopToWarehouseSuggestionTransferCreate()
    {
        global $db, $ecs, $userController ,$_CFG;

       // $db->query('START TRANSACTION');
        $orderController = new OrderController();
        $transfer_goods = [];
        foreach ($_REQUEST['transfer_qty'] as $key => $transfer_qty) {
            if ($transfer_qty > 0) {
                $transfer_goods[$_REQUEST['goods_id'][$key]] += $_REQUEST['transfer_qty'][$key];
            }
        }

        //  print_r($transfer_goods);
  
        
        // create transfer list
        $warehouse_from = $_REQUEST['shop_warehouse_id'];
        $warehouse_to = $_REQUEST['main_warehouse_id'];
        $transfer_type = self::REASON_TYPE_OTHERS;
      
        $operator = erp_get_admin_id();
        
        $transfer_id = $this->warehouseTransferCreateByGoodsTransfer($warehouse_from, $warehouse_to, $transfer_goods, $transfer_type, $operator, 0);
      
        if (isset($transfer_id['error'])) {
            return $transfer_id;
        }

        return $transfer_id;
    }

    public function adjustTransferWarehouseStockSubmit()
    {
        global $db, $ecs, $userController ,$_CFG;
        
        if ($_REQUEST['adjust_operator'] == '-') {
            $_REQUEST['adjust_goods_qty'] = $_REQUEST['adjust_goods_qty'] * -1;
        }

        $sql="INSERT INTO ".$ecs->table('erp_stock_transfer_adjustment')." SET transfer_id = '".$_REQUEST['adjust_transfer_id']."', warehouse_id='".$_REQUEST['adjust_warehouse_id']."',goods_id='".$_REQUEST['adjust_goods_id']."',qty='".($_REQUEST['adjust_goods_qty'])."',create_date='".gmtime()."', remark='".$_REQUEST['adjust_remark']."'";
       
        $db->query($sql);        

        return true;
    }

    public function adjustWarehouseAdminConfirm($adjustment_id,$adjust_warehouse_side)
    {
        global $db, $ecs, $userController ,$_CFG;

        // check permission
        
        if(!admin_priv('erp_warehouse_approve','',false))
        {
            $result['error'][] = '你沒有出入庫權限';
            return $result;
        }

        $db->query('START TRANSACTION');

        if ($adjust_warehouse_side == 'from') {
            $sql = "update ".$ecs->table('erp_stock_transfer_adjustment')." set admin_from_confirm = '".erp_get_admin_id()."' where adjustment_id = ".$adjustment_id." ";
        } else {
            $sql = "update ".$ecs->table('erp_stock_transfer_adjustment')." set admin_to_confirm = '".erp_get_admin_id()."' where adjustment_id = ".$adjustment_id." ";
        }

        $db->query($sql);

        // check if both confirm
        $sql = "select * from ".$ecs->table('erp_stock_transfer_adjustment')." where admin_from_confirm > 0 and admin_to_confirm > 0 and adjustment_id = ".$adjustment_id." ";
        $adjust_info = $db->getRow($sql);

        if (!empty($adjust_info)) {
            // adjust stock
            $transfer_info = $this->getTransferInfo($adjust_info['transfer_id']);

            if ($adjust_info['qty'] > 0) {
                //add warehousing  (in)
                $warehouse_id = $adjust_info['warehouse_id'];
                $order_id = 0;
                $warehousing_from = '調貨庫存調整(Ref:'.$transfer_info['transfer_sn'].')';
                $warehousing_style_id = 4;
                $warehousing_id=add_warehousing($adjust_info['warehouse_id']);
                
                // get warehouseing sn
                $sql = 'select warehousing_sn from '.$ecs->table('erp_warehousing').' where warehousing_id = '.$warehousing_id.' ';
                $warehousing_sn = $db->getOne($sql);
                
                $approved_remark = '調貨庫存調整(Ref:'.$transfer_info['transfer_sn'].')';
                $sql="update ".$ecs->table('erp_warehousing')." set order_id = '0', warehousing_style_id = '".$warehousing_style_id."' ,warehousing_from='".$warehousing_from."',  approved_by='1',  approve_time= '".gmtime()."', approve_remark= '".$approved_remark."', last_act_time= '".gmtime()."', status = 3 where warehousing_id='".$warehousing_id."'";
                $db->query($sql);
                
                $goods_id = $adjust_info['goods_id'];
                $received_qty = $adjust_info['qty'];
                $attr_id=get_attr_id($goods_id);
                $sql="insert into ".$ecs->table('erp_warehousing_item')." set warehousing_id='".$warehousing_id."',goods_id='".$goods_id."',attr_id='".$attr_id."', expected_qty='0',received_qty='".$received_qty."'";
                $db->query($sql);

                if (!add_stock_by_warehousing($warehousing_id)) {
                    $error = true;
                } else {
                    $sql="update ".$ecs->table('erp_stock_transfer_adjustment')." set status = '1', ref_no = ".$warehousing_id." where adjustment_id='".$adjustment_id."'";
                    $db->query($sql);
                }
            }

            if ($adjust_info['qty'] < 0) {
                //add warehousing  (out)
                $warehouse_id = $adjust_info['warehouse_id'];
                $goods_id = $adjust_info['goods_id'];
                $delivered_qty = $adjust_info['qty'];

                $delivered_qty = $delivered_qty * -1;

                //stock check
                $attrId=\get_attr_id($goods_id);
                $stock=\get_goods_stock_by_warehouse($warehouse_id, $goods_id, $attrId);
                if ($stock<$delivered_qty) {
                    $result['error'][] = '庫存不足, 請重新確認 (Requested:'.$delivered_qty.'-'.$warehouse_id.')';
                    $db->query('ROLLBACK');
                    return $result;
                }

                $order_id = 0;
                $receiver = '調貨庫存調整(Ref:'.$transfer_info['transfer_sn'].')';
                $delivery_style_id = 4;
                $delivery_id = add_delivery();
                //$deliveryInfo=\get_delivery_info($delivery_id);

                // get delivery sn
                $sql = 'select delivery_sn from '.$ecs->table('erp_delivery').' where delivery_id = '.$delivery_id.' ';
                $delivery_sn = $db->getOne($sql);

                $approved_remark = '調貨庫存調整(Ref:'.$transfer_info['transfer_sn'].')';
                $sql="update ".$ecs->table('erp_delivery')." set order_id = '0', warehouse_id=".$warehouse_id.", delivery_style_id = '".$delivery_style_id."' ,delivery_to='".$receiver."',  approved_by='1',  approve_time= '".gmtime()."', approve_remark= '".$approved_remark."', post_by= '".gmtime()."', status = 3 where delivery_id='".$delivery_id."'";
                $db->query($sql);

                $attr_id=get_attr_id($goods_id);
                $sql="insert into ".$ecs->table('erp_delivery_item')." set delivery_id='".$delivery_id."',goods_id='".$goods_id."',attr_id='".$attr_id."', expected_qty='0',delivered_qty='".$delivered_qty."'";
                $db->query($sql);

                if (!deduct_stock_by_delivery($delivery_id)) {
                    $error = true;
                } else {
                    $error = false;
                    $sql="update ".$ecs->table('erp_stock_transfer_adjustment')." set status = '1' , ref_no = ".$delivery_id." where adjustment_id='".$adjustment_id."'";
                    $db->query($sql);
                }

            }

         
        }
        if ($error == true) {
            $db->query('ROLLBACK');
            return false;
        } else {
            $db->query('COMMIT');
            return true;
        }

        return true;
    }

    function adjustWarehouseAdminDelete($adjustment_id) {
        global $db, $ecs, $userController ,$_CFG;

        $sql = "delete from ".$ecs->table('erp_stock_transfer_adjustment'). " where adjustment_id = ".$adjustment_id." ";
        $db->query($sql);
        return true;
    }

}
