<?php
/***
* AddressController.php
* by Anthony 20180112
*
* Address controller
* Using to handle address break off, google/hk gov api 
***/
namespace Yoho\cms\Controller;
use Yoho\cms\Model;
if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

class AddressController extends YohoBaseController{

    public function __construct(){
		parent::__construct();
    }

    const ADDRESS_TYPE_LOCKER = 'locker'; // 櫃, 提貨點, 便利店, 郵局
    const ADDRESS_TYPE_EXPRESS = 'express'; // 速遞地址

    const AREA_TYPE_DEFAULT  = 0; // default
    const AREA_TYPE_FLAT     = 1; // 住宅
    const AREA_TYPE_BUSINESS = 2; // 工商

    // Using to merge formatted address
	public function handle_address_detail($address, $need_format = true){

		global $ecs, $db, $_LANG;
		// Using to check is Chi/Eng address.
		$str = $address['sign_building']. $address['street_name']. $address['locality']. $address['administrative_area'];
		$m   = mb_strlen($str,'utf-8');
		$s   = strlen($str);
		$is_eng_address = false;
		if($s==$m){
			$is_eng_address = true; // Eng address
			require_once(ROOT_PATH . 'languages/en_us/yoho.php');
		} else {
			$is_eng_address = false;
			require_once(ROOT_PATH . 'languages/zh_tw/yoho.php');
		}
		// Format address
		$address['country_name']   = (isset($_LANG['country_' . $address['country']])) ? $_LANG['country_' . $address['country']] : $address['country_name'];
        //if(empty($address['address'])){ // If user '手動填寫詳細地址'
        if(1){ // If user '手動填寫詳細地址'
			$format_address = "";
			if($is_eng_address || !in_array($address['country'], [3409,3410,3436,3411])){ // Is english address
                $format_address .= ($address['room']?"Room ".$address['room'].", ":"").($address['floor']?$address['floor']."/F, ":""); // Room X , X/F, 

				$addr_phase = (empty($address['phase']) ? '' : $address['phase'].', '); // Phase
                $addr_building .= (empty($address['sign_building']) ? "" : $address['sign_building'].', '); // Building
				$addr_block = (empty($address['block']) ? '' : ((empty($addr_phase) && empty($addr_building)) ? $address['block'].', ' : 'BLOCK '.$address['block'].' ')); // BLOCK X

                if(empty($addr_phase)) {
                    $format_address .= $addr_block.$addr_building.$addr_phase; // BLOCK X Building, Phase
                } else {
                    $format_address .= $addr_building.$addr_block.$addr_phase; // Building, BLOCK X Phase
                }
                
				if(!empty($address['street_name'])) $format_address .= $address['street_num']." ".$address['street_name'].", ";
				$format_address .= (empty($address['locality'])?'':$address['locality'].', ') . (empty($address['administrative_area'])?'':$address['administrative_area'].', ');
				$format_address .= $address['country_name'];
			} else {
				// if($address['country'] == 3409 && !empty($address['street_num'])) $street_num = $address['street_num'] . "號";
				if(in_array($address['country'], [3409,3410,3436,3411]) && !empty($address['street_num'])) $street_num = $address['street_num'] . ($address['country'] == 3436 ? "号" : "號");
                else $street_num = $address['street_num']; 
                $format_address .= $address['country_name'].""; // 國家
				$format_address .= (empty($address['administrative_area'])?'':$address['administrative_area'].'').(empty($address['locality'])?'':$address['locality'].''); // 國家 地區 城市
				if(!empty($address['street_name'])) $format_address .= $address['street_name']."".$street_num.""; // 國家 地區 城市 街道
				$addr_phase = (empty($address['phase']) ? '' : $address['phase']); // 屋苑
				$addr_block = strtoupper((empty($address['block']) ? '' : $address['block'].'座')); // X座
                $addr_building .= (empty($address['sign_building']) ? "" : $address['sign_building']." "); // 大廈
				if(empty($addr_phase)) {
                    $format_address .= $addr_building."".$addr_block; // 冇屋苑 -> 屋苑 大廈 X座
                } else {
                    $format_address .= $addr_phase."".$addr_block."".$addr_building; // 有屋苑 -> 屋苑 X座 大廈
                }
                $addr_floor = ((empty($address['floor'])&&$address['floor']!='0')?"":(($address['floor']=="地下"||$address['floor']=='0'||strtoupper($address['floor'])=="G")?"地下":($address['floor']=="地舖"?"地舖":$address['floor']."樓")));
                $format_address .= $addr_floor;
                if (!empty($address['room'])) {
                    if((is_numeric($address['room']) || is_numeric(str_replace([" - ", "-"], ["", ""], $address['room'])) || preg_match('/\d+[a-zA-Z]$/', $address['room']) || (preg_match('/[a-zA-Z]/', $address['room']) && strlen($address['room']) == 1)) && $addr_floor != "地舖") {
                        $format_address .= in_array($address['country'], [3409,3410,3436,3411]) ? " " . $address['room']."室" : ", " . $address['room'];
                    } else {
                        $format_address .= ", " . $address['room'];
                    }
                }
                    
			}
		} else { // else: Handle Google Map Address:
			// $format_address = $address['address'];
			$building = empty($address['sign_building']) ? $address['google_place'] : $address['sign_building'];
			if (mb_strpos($address['address'], $building, 0,'UTF-8') !== false) {
				$building = "";
			}
			if($is_eng_address){ // Is english address
				$format_address = ((empty($address['room']))?"":"Room ".$address['room'].", ").((empty($address['floor']))?"":$address['floor']."/F, ").((empty($building))?"":$building.", ").$address['address'];
			} else {
                $format_address = $address['address'].(empty($building)?"":" ".$building).(empty($address['floor'])?"":" ".$address['floor']."/F");
                if (!empty($address['room'])) {
                    if(is_numeric($address['room']) || preg_match('/\d+[a-zA-Z]$/', $address['room']) || (preg_match('/[a-zA-Z]/', $address['room']) && strlen($address['room']) == 1)) {
                        $format_address .= in_array($address['country'], [3409,3410,3436,3411]) ? " " . $address['room']."室" : ", " . $address['room'];
                    } else {
                        $format_address .= ", " . $address['room'];
                    }
                }
			}
		}
		$address['address'] = ($need_format == true || empty($address['address'])) ? $format_address : $address['address'];

		// Handle Hong Kong address region & district
		if($address['country'] == 3409){
			// TODO: If we need more handle Hong Kong address in future:
			// https://www.als.ogcio.gov.hk/lookup?q={$address}&n=1  香港政府地址搜尋服務 API
			// https://data.gov.hk/tc-data/dataset/hk-landsd-openmap-street-name 香港街道名稱

			// Add HK area_id
			$sql = "SELECT area_id FROM ".$ecs->table('hk_areas')." WHERE (area_chi_name = '".$address['locality']."') OR (area_eng_name = '".$address['locality']."') LIMIT 1";
			$hk_area_id = $db->getOne($sql);
			$address['hk_area']  = empty($hk_area_id) ? 0 : $hk_area_id;

			// Hard code handle HK province
			if(in_array($address['administrative_area'], ['Kowloon','九龍']) ){
				$address['province'] = 3412;
			} elseif (in_array($address['administrative_area'], ['New Territories','新界']) ){
				$address['province'] = 3413;
			} elseif (in_array($address['administrative_area'], ['Hong Kong Island','香港島']) ){
				$address['province'] = 3414;
			} else {
				$address['province'] = 0;
			}
		}
		return $address;
	}

    // HK gov Address API
	public function hk_gov_address_analysis($origin_address, $is_eng_address = false, $n = 1, $res= []){

        $value = rawurlencode($origin_address);
        if(empty($res)) {
            $response = file_get_contents('https://www.als.ogcio.gov.hk/lookup?n='.$n.'&q='.$value);
            $res = json_decode(json_encode((array) simplexml_load_string($response)),1);
        }

        $arr = [];
        if (isset($res['SuggestedAddress']['Address'])){
            $suggestedAddress_list[] = $res['SuggestedAddress'];
        } else {
            $suggestedAddress_list =  $res['SuggestedAddress'];
        }
        foreach ($suggestedAddress_list as $key => $suggestedAddress) {

            $consignee = [
                'building' => '',
                'street_name' => '',
                'street_num' => '',
                'locality' => '',
                'administrative_area' => '',
                'block' => '',
                'administrative_area' => '',
                'google_place' => '',
                'phase' => '',
                'zipcode' => ''
            ];
            // Handle single HK gov address format
            $premisesAddress   = $suggestedAddress['Address']['PremisesAddress'];

            $formatted_address = "";
            if($is_eng_address){
                $address  = $premisesAddress['EngPremisesAddress'];
                $block    = $address['EngBlock'];
                $district = $address['EngDistrict'];
                $estate   = $address['EngEstate'];
                $phase    = '';
                if($estate['EngPhase']){
                    $phase  = $estate['EngPhase']['PhaseName'];
                    $estate['EstateName'] .= " ".$phase;
                }
                $street   = $address['EngStreet'];
                $region   = $address['Region'];
                $building = ($address['BuildingName'])?$address['BuildingName'].", ":"";

                if($block['BlockDescriptor'] == 'BLK') $block['BlockDescriptor'] = "Block";
                $formatted_address = $building.$estate['EstateName']." ".$block['BlockDescriptor']." ".$block['BlockNo'].", ".$street['BuildingNoFrom']." ".$street['StreetName'].", ".$region;
                $locality = (explode(", ",$street['StreetName'])[1]) ? explode(", ",$street['StreetName'])[1] : "";
                $street_name = (explode(", ",$street['StreetName'])[1]) ? explode(", ",$street['StreetName'])[0] : $street['StreetName'];
                $format_building = $building.$estate['EstateName']." ".$block['BlockDescriptor']." ".$block['BlockNo'];
                if($region == 'NT') $region = 'New Territories';
                elseif($region == 'KLN') $region = 'Kowloon';
                elseif($region == 'HK') $region = 'Hong Kong Island';
            } else {
                $address  = $premisesAddress['ChiPremisesAddress'];
                $block    = $address['ChiBlock'];
                $district = $address['ChiDistrict'];
                $estate   = $address['ChiEstate'];
                $phase    = '';
                if($estate['ChiPhase']){
                    $phase  = $estate['ChiPhase']['PhaseName'];
                    $estate['EstateName'] .= $phase;
                }
                $street   = $address['ChiStreet'];
                $region   = $address['Region'];
                $building = ($address['BuildingName'])?" ".$address['BuildingName']:"";
                $formatted_address = $region." ".$street['StreetName']."".(($street['BuildingNoFrom'])?$street['BuildingNoFrom']."號 ":"").$estate['EstateName'].$block['BlockNo'].$block['BlockDescriptor'].$building;
                $locality = (explode(" ",$street['StreetName'])[1]) ? explode(" ",$street['StreetName'])[0] : "";
                $street_name = (explode(" ",$street['StreetName'])[1]) ? explode(" ",$street['StreetName'])[1] : $street['StreetName'];
                $format_building = $estate['EstateName'].$block['BlockNo'].$block['BlockDescriptor'].$building;
                if($region == '香港') $region = '香港島';
            }
            $consignee = [
                'building' => trim($format_building),
                'sign_building' => trim($format_building),
                'street_name' => $street_name,
                'street_num' => $street['BuildingNoFrom'],
                'locality' => $locality,
                'administrative_area' => $region,
                'block' => ($block['BlockNo'])?$block['BlockNo']:"",
                'phase' => $phase,
                'google_place' => '',
                'zipcode' => ''
            ];

            $arr[] = [
                'consignee'         => $consignee,
                'formatted_address' => $formatted_address
            ];
        }

        return $arr;
    }

    // Google Geocode API
    /* This function like checkout.js : geocoder.geocode */
    public function google_geo_address_analysis($origin_address, $is_eng_address = false, $response = []){

        $status = '';
        $run_time = 0;
        if(!empty($response)) {
            $status = $response['status'];
        }

        // Spec function : try to loop again api if status is ZERO_RESULTS
        while ($status != 'OK' AND $run_time < 4 ) {
            $run_time += 1;
            $search_address = str_replace (" ", "+", urlencode($origin_address));
            $lang           = ($is_eng_address) ? 'en_us' : 'zh_tw';
            $key            = "AIzaSyDHUTKhF0H7C7omzT-ifncxaPh1mW-tvDo"; //This Key is only can used by yohongkong.com's IP
            if (strpos($_SERVER['SERVER_NAME'], 'beta.') !== false)$key = "AIzaSyBhsbwdQdsL2ugyuylW3KA3q_pytM4Keaw";
            $details_url    = "https://maps.googleapis.com/maps/api/geocode/json?address=".$search_address."&key=".$key."&language=".$lang;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $details_url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $response = json_decode(curl_exec($ch), true);
            $status = $response['status'];
            if($response['status'] == 'ZERO_RESULTS'){ // Try to loop by cut address
                if($is_eng_address){
                    $address_arr = explode(",", $origin_address);
                    unset($address_arr[0]);
                    $origin_address = implode(",", $address_arr);
                } else {
                    $address_arr = explode(" ", $origin_address);
                    if(count($address_arr) < 3)$address_arr = explode(",", $origin_address);
                    unset($address_arr[count($address_arr) - 1]);
                    $origin_address = implode(" ", $address_arr);
                }
            } else if($response['status'] == 'OK'){
                break;
            } else {
                return [];
            }
        }


        $arr = [];
        foreach ($response['results'] as $key => $res) {
            $address_components = $res['address_components'];
            $formatted_address  = $res['formatted_address'];
            $geometry           = $res['geometry'];
            $address_format = [];
            $political = [];
            $country_code = '';
            foreach ($address_components as $key => $component) {
                $addressType = $component['types'][0]; // We only get the first type
                $name = $component['long_name'];
                $address_format[$addressType] = $name;
                if($addressType === 'country'){
                    $country_code = $component['short_name'];
                }
                if($addressType === 'political'){
                    $political[] = $name;
                }
            }
            
            // Using to handle spec address type : political
            if(count($political) > 0) {
                $political = array_reverse($political);
                $address_format['street_number'] = '';
                $address_format['route'] = (isset($address_format['route'])) ? $address_format['route'] :  '';
                foreach ($political as $index => $element) {
                    if($index == 0){
                        $address_format['route'] .= ($address_format['route'] != "" ? " " : "") . $element;
                    } else {
                        $address_format['street_number'] .= $element;
                        if($index != (count($political) - 1)){
                            $address_format['street_number'] .= "-";
                        }
                    }
                }
            }

            $consignee = [
                'building' => '',
                'street_name' => '',
                'street_num' => '',
                'locality' => '',
                'administrative_area' => '',
                'block' => '',
                'google_place' => '',
                'phase' => '',
                'zipcode' => ''
            ];
            $consignee['building']            = $address_format['premise'] ? $address_format['premise'] : '';
            $consignee['street_name']         = $address_format['route'];
            $consignee['street_num']          = $address_format['street_number'];
            $consignee['locality']            = $address_format['neighborhood'] ? $address_format['neighborhood'] : $address_format['locality'];
            $consignee['administrative_area'] = $address_format['administrative_area_level_1'];

            if(!empty($country_code)){
                $country_code = strtolower($country_code);
                $consignee['country'] = get_region_by_country_code($country_code);
            } else {
                $consignee['country'] = 3409;
            }

            $consignee['zipcode']       = ($address_format['postal_code'])?$address_format['postal_code']:'';
            $consignee['sign_building'] = $consignee['building'];

            $arr[] = [
                'consignee'         => $consignee,
                'formatted_address' => mb_convert_case($formatted_address, MB_CASE_TITLE, "UTF-8")
            ];
        }
        return $arr;
    }

	/* This is a beta to break off address (Using in POS) */
	public function breakOffAddress($address, $country = 0, $get_api = true, $consignee = []){
        $address = make_semiangle($address);

		$m   = mb_strlen($address,'utf-8');
		$s   = strlen($address);
		$is_eng_address = false;
		if($s==$m){
			$is_eng_address = true; // Eng address
		} else {
			$is_eng_address = false;
		}

		// IF have Simplified Chinese, change to Traditional Chinese
		if(!$is_eng_address){
            $tmp = iconv('UTF-8', 'GB2312', $address);
            if($tmp){
                $val     = iconv('GB2312', 'BIG5', $tmp);
                $address = iconv('BIG5', 'UTF-8', $val);
            }
		}
		$room  = '';
		$floor = '';
		$block = '';
		$phase = '';
        if (empty($address) || $address == "NULL") {
            $consignee = [
                'building' => '',
                'street_name' => '',
                'street_num' => '',
                'locality' => '',
                'administrative_area' => '',
                'block' => '',
                'google_place' => '',
                'phase' => '',
                'auto_break' => true,
                'room'  => '',
                'floor' => '',
                'block' => '',
                'zipcode' => 0
            ];
            $consignee['address'] = "NULL";
            return $consignee;
        }
        // Step 1:  Using HK gov/Google break off address:
        if($get_api || empty($consignee)) {
            if($country == 3409){
                $result = $this->hk_gov_address_analysis($address, $is_eng_address);
                $consignee = $result[0]['consignee']; // Only get First result
            } else { // Google Geo API
                $result = $this->google_geo_address_analysis($address, $is_eng_address);
                $consignee = $result[0]['consignee']; // Only get First result
            }
        }

		// Step 2: Handle if address is english address
		if($is_eng_address){

			// Try to break by ","
			$addr_arr  = explode(",", $address);
			$analytics = true;
			// If addr_arr length < 3, try to using " "
			if(count($addr_arr) < 3){$addr_arr = explode(" ", $address);$analytics = false;}

			// Step 2.1: Try to get room, floor, block, phase in address
			foreach($addr_arr as $brk_key => $brk_addr){

                // Step 2.1.1: Find Room, We using stripos — Find the position of the first occurrence of a case-insensitive substring in a string
				$room_string = ["house", "rm", "room", "shop", "flat", "apartment", 'unit'];
				if(empty($room)) foreach ($room_string as $key => $rm) {
					$stripos = stripos($brk_addr, $rm);
					if($stripos !== false){ // Found
						$room = substr($brk_addr, $stripos+strlen($rm)); //e.g. 'flat 5A 18/F' => $room = ' 5A 18/F'
						$room = trim($room);
                        $room = explode(' ', $room)[0];
						$room = (empty($room)) ? $addr_arr[$brk_key + 1] : $room;
						break;
					}
                }
                //**特殊比對: 如果為addr_arr陣列最後一個, 為數字/N個數字+單一字母/單一字母, 判斷為Room */
                if(empty($room) && $brk_key == count($addr_arr) - 1){
                    if(is_numeric($brk_addr) || preg_match('/\d+[a-zA-Z]$/', $brk_addr) || (preg_match('/[a-zA-Z]/', $brk_addr) && strlen($brk_addr) == 1)) $room = trim($brk_addr);
                }

				// Step 2.1.2: Find Floor, We using stripos — Find the position of the first occurrence of a case-insensitive substring in a string
				$floor_string = ['/f', 'floor'];
				if(empty($floor)) foreach ($floor_string as $key => $fr) {
					$stripos = stripos($brk_addr, $fr);
					
					if($fr == '/f' && $stripos !== false){
						$floor = substr($brk_addr, 0, $stripos); //e.g. 'flat 5A 18/f' => flat 5A 18
						$floor = trim($floor);

						$floor_arr = explode(' ', $floor);
						$floor = $floor_arr[count($floor_arr) - 1];
                        $floor = (empty($floor)) ? $addr_arr[$brk_key - 1] : $floor;
                        if(strlen($floor) > 3) $floor = '';
						break;
					} else if ($stripos !== false) {
						$floor = substr($brk_addr, $stripos+strlen($fr)); //e.g. 'flat 5A floor 18' => $room = ' 18'
						$floor = trim($floor);

						$floor = explode(' ', $floor)[0];
                        $floor = (empty($floor)) ? $addr_arr[$brk_key + 1] : $floor;
                        if(strlen($floor) > 3) $floor = '';
						break;
					}
				}

				// Step 2.1.3: Find Block, We using stripos — Find the position of the first occurrence of a case-insensitive substring in a string
				$block_string = ['block', 'blk', 'tower'];
				if(empty($block)) foreach ($block_string as $key => $brk) {
					$stripos = stripos($brk_addr, $brk);
					if($stripos !== false){
                        $block = substr($brk_addr, $stripos+strlen($brk));
                        $block = trim($block);

                        $block = explode(' ', $block)[0];
                        $block = (empty($block)) ? $addr_arr[$brk_key + 1] : $block;
                        break;
                    }
				}

				// Step 2.1.4: Find Phase, We using stripos — Find the position of the first occurrence of a case-insensitive substring in a string
				$phase_string = ['phase', 'site'];
				if(empty($phase)) foreach ($phase_string as $key => $p) {
					$stripos = stripos($brk_addr, $p);
					if($stripos !== false){
                        $phase = substr($brk_addr, $stripos+strlen($p));
                        $phase = trim($phase);

                        $phase = explode(' ', $phase)[0];
                        $phase = (empty($phase)) ? $addr_arr[$brk_key + 1] : $phase;
                        break;
                    }
				}
			}
		// Step 3: Handle if address is not english address
		} else {
			$tmp_arr = preg_split('//u', $address, -1, PREG_SPLIT_NO_EMPTY);;
			// Group Number/ Chinese Number
			$tmp = '';
			$arr = [];
			foreach ($tmp_arr as $key => $value) {
				$chi_num = ['零', '壹', '貳', '參', '肆', '伍', '陸', '柒', '捌', '玖', '一', '二', '三', '四', '五', '六', '七', '八', '九', '十', '百', '佰', '千', '仟', '萬'];
				// Check is Chinese Number
				if(in_array($value, $chi_num)) $tmp .= $value;
				else { // the next char is not chinese number, change to normal number

                    $tmp = (empty($tmp))?'':chtonum($tmp);
					if($tmp != ''){
                        $arr[] = $tmp;
                    }
                    $arr[] = $value;
					$tmp = '';
				}
			}
            $new_address = implode($arr);

			// Try to break by ","
			$addr_arr  = explode(",", $new_address);
            $analytics = true;
			// If addr_arr length < 3, try to using " "
			if(count($addr_arr) < 3){$addr_arr = explode(" ", $new_address);$analytics = false;}
			// Chinese address : will be no "," and " "
			if(count($addr_arr) < 3){
				$analytics = false;
				// We break off by Number
				$addr_arr = preg_split('/(\w+)/', $new_address, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE); 
            }
            // We  reverse addr_arr, because Chi address is reverse by eng address
            $addr_arr = array_reverse($addr_arr);

			// Step 3.1: Try to get room, floor, block, phase in address
			foreach($addr_arr as $brk_key => $brk_addr){

                // Step 3.1.1: Find Room, We using stripos — Find the position of the first occurrence of a case-insensitive substring in a string
                //**特殊比對: 如果為addr_arr陣列第一個, 為數字/N個數字+單一字母/單一字母, 判斷為Room */
                if(empty($room) && $brk_key == 0){
                    if(is_numeric($brk_addr) || preg_match('/\d+[a-zA-Z]$/', $brk_addr) || (preg_match('/[a-zA-Z]/', $brk_addr) && strlen($brk_addr) == 1)) $room = trim($brk_addr);
                }
				$room_string = ['室','號舖', "house", "rm", "room", "shop", "flat", "apartment", 'unit'];
				if(empty($room)) foreach ($room_string as $key => $rm) {
					$stripos = stripos($brk_addr, $rm);
                    if($stripos !== false){ // Found
                        if($rm == '室' || $rm == '號舖'){
                            $room = substr($brk_addr, 0, $stripos); //e.g. 'flat 5A 18/f' => flat 5A 18
                            $room = trim($room);

                            $room_arr = explode(' ', $room);
                            $room = $room_arr[count($room_arr) - 1];
                            $room = (empty($room)) ? $addr_arr[$brk_key + 1] : $room;
                            /* 特殊比對, 如果前有樓字, 消去 */
                            $room_arr = explode('樓', $room);
                            $room = $room_arr[count($room_arr) - 1];
                            break;
                        }else{
                            $room = substr($brk_addr, $stripos+strlen($rm)); //e.g. 'flat 5A 18/F' => $room = ' 5A 18/F'
						    $room = trim($room);
                            $room = explode(' ', $room)[0];
						    $room = (empty($room)) ? $addr_arr[$brk_key - 1] : $room;
						break;
                        }
					}
				}

				// Step 3.1.2: Find Floor, We using stripos — Find the position of the first occurrence of a case-insensitive substring in a string
				$floor_string = ['樓','層','/f', 'floor', '地下'];
				if(empty($floor)) foreach ($floor_string as $key => $fr) {
					$stripos = stripos($brk_addr, $fr);

                    if ($fr == 'floor' && $stripos !== false) {
                        $floor = substr($brk_addr, $stripos+strlen($fr)); //e.g. 'flat 5A floor 18' => $room = ' 18'
                        $floor = trim($floor);

                        $floor = explode(' ', $floor)[0];
                        $floor = (empty($floor)) ? $addr_arr[$brk_key - 1] : $floor;
                        if (strlen($floor) > 3) {
                            $floor = '';
                        }
                        break;
                    } else if($fr == '地下' && $stripos !== false){
						$floor = 'G';
						break;
					} else if($stripos !== false) {
						$floor = substr($brk_addr, 0, $stripos); //e.g. 'flat 5A 18/f' => flat 5A 18
						$floor = trim($floor);

						$floor_arr = explode(' ', $floor);
						$floor = $floor_arr[count($floor_arr) - 1];
                        $floor = (empty($floor) || (strlen($floor) != mb_strlen($floor, 'utf-8'))) ? $addr_arr[$brk_key + 1] : $floor;
                        if(strlen($floor) > 3) $floor = '';
						break;
					}
				}

				// Step 3.1.3: Find Block, We using stripos — Find the position of the first occurrence of a case-insensitive substring in a string
				$block_string = ['座', 'block', 'blk', 'tower'];
				if(empty($block)) foreach ($block_string as $key => $brk) {
					$stripos = stripos($brk_addr, $brk);
					if($stripos !== false){ // Found
						if($brk == '座'){
                            $block = $brk_addr;
                            $block_arr = explode(' ', $block);
                            $block = $block_arr[count($block_arr) - 1];
						    $block = (empty($block)) ? $addr_arr[$brk_key + 1] : $block;
                        } else {
                            $block = substr($brk_addr, $stripos+strlen($brk)); //block II or PAHSEII XXX?
						    $block = trim($block);
                            $block_arr = explode(' ', $block);
                            $block = $block_arr[0];
                            $block = (empty($block)) ? $addr_arr[$brk_key - 1] : $block;
                        }
						break;
					}
				}

				// Step 3.1.4: Find Phase, We using stripos — Find the position of the first occurrence of a case-insensitive substring in a string
				$phase_string = ['期', 'phase', 'site'];
				if(empty($phase)) foreach ($phase_string as $key => $p) {
					$stripos = stripos($brk_addr, $p);
                    if($stripos !== false){ // Found
                        if($p == '期'){
                            $phase = substr($brk_addr, 0, $stripos);

                            $phase_arr = explode(' ', $phase);
                            $phase = $phase_arr[count($phase_arr) - 1];
						    $phase = (empty($phase)) ? $addr_arr[$brk_key + 1] : $phase;
                        } else {
                            $phase = substr($brk_addr, $stripos+strlen($p)); //phase II or PAHSEII XXX?
						    $phase = trim($phase);
                            $phase_arr = explode(' ', $phase);
                            $phase = $phase_arr[0];
                            $phase = (empty($phase)) ? $addr_arr[$brk_key - 1] : $phase;
                        }

						break;
					}
				}
			}
		}
        $consignee['block']    = (empty($consignee['block'])) ? $block : $consignee['block'];
        $consignee['phase']    = (empty($consignee['phase'])) ? $phase : $consignee['phase'];
        $consignee['floor']    = (empty($consignee['floor'])) ? $floor : $consignee['floor'];
        $consignee['room']     = (empty($consignee['room']))  ? $room  : $consignee['room'];
        $consignee['country']  = (empty($country))            ? $consignee['country'] : $country;
        $consignee['address']  = $address;
        $consignee['auto_break']  = true;

        //Hard code for SF locker address, because the address cannot be broken down.
        if ($consignee['address'] == '[OK] 金鐘道93號金鐘廊1樓Lab Concept F01-02號舖') {
            $consignee['administrative_area'] = '香港島';
            $consignee['building'] = '金鐘廊';
            $consignee['sign_building'] = '金鐘廊';
            $consignee['street_name'] = '金鐘道';
            $consignee['street_num'] = '93';
            $consignee['block'] = '';
        }

        if ($consignee['address'] == '[OK] Shop No. F01-02, Lab Concept, 1/F Queensway Plaza, 93 Queensway, Hong Kong') {
            $consignee['administrative_area'] = 'Hong Kong Island';
            $consignee['building'] = 'Queensway Plaza';
            $consignee['sign_building'] = 'Queensway Plaza';
            $consignee['street_name'] = 'Queensway';
            $consignee['street_num'] = '93';
            $consignee['block'] = '';
        }

        return $consignee;
    }

    public function get_auto_break_list($country = 0){
		global $ecs, $db;
        $where_arr = ["oa.auto_break = 1","oa.address != 'NULL'"];
        if($country != 0){
            $where_arr[] = "oa.country = " . $country;
        }
        return $this->get_address($where_arr);
    }

    public function get_full_list($tab = "",$page_size,$page){
        global $smarty,$ecs, $db;
        if($tab != ""){
            $where = [];
            $limit = [];
            if(!empty($_REQUEST['start_date'])){
                $where[] = "oa.create_at > '" . $_REQUEST['start_date'] . " 00:00:00'";
            }
            if(!empty($_REQUEST['end_date'])){
                $where[] = "oa.create_at <= '" . $_REQUEST['end_date'] . " 23:59:59'";
            }
            if(!empty($_REQUEST['sorting'])){
                $limit[] = "oa.create_at " . ($_REQUEST['sorting'] == "old" ? "ASC" : "DESC");
            }
            if($tab == "time"){
                $where = array_merge($where,["oa.address != 'NULL'", "oa.staying_time > 0"]);
                $limit = array_merge($limit,["oa.staying_time DESC"]);
                $time_addr_list = $this->get_address($where, implode(", ", $limit));
                foreach($time_addr_list as $key => $address){
                    $time_addr_list[$key]['formatted_staying_time'] = $this->format_staying_time($address['staying_time']);
                }
                $where[] = 'oa.staying_time > 0';
                $sql = "SELECT AVG(oa.staying_time) FROM " . $ecs->table('order_address') ."AS oa WHERE 1 AND " . implode(' AND ', $where);
                $st = $db->getOne($sql);
                $smarty->assign('staying_time', $this->format_staying_time(ceil($st)));
                return $time_addr_list;
            }else if($tab == "sign"){
                $where = array_merge($where,["oa.address != 'NULL'", "oa.sign_building != ''"]);
                $limit = array_merge($limit,["count DESC", "sign_building DESC"]);
                $sql = "SELECT oa.sign_building," .
                        "SUM(CASE WHEN user_id = 0 THEN 1 ELSE 0 END) + COUNT(DISTINCT(CASE WHEN user_id > 0 THEN user_id END)) AS count " .
                        "FROM " . $ecs->table('order_address') . " oa LEFT JOIN " . $ecs->table('order_info') . " oi ON oa.order_id = oi.order_id " .
                        "WHERE " . implode(" AND ", $where) . " GROUP BY oa.sign_building ORDER BY " . implode(", ", $limit);
                return $db->getAll($sql);
            }else if($tab == "google"){
                $where = array_merge($where,["oa.address != 'NULL'", " oa.google_place != ''"]);
                $limit = array_merge($limit,["count DESC", "google_place DESC"]);
                $sql = "SELECT oa.google_place," .
                        "SUM(CASE WHEN user_id = 0 THEN 1 ELSE 0 END) + COUNT(DISTINCT(CASE WHEN user_id > 0 THEN user_id END)) AS count " .
                        "FROM " . $ecs->table('order_address') . " oa LEFT JOIN " . $ecs->table('order_info') . " oi ON oa.order_id = oi.order_id " .
                        "WHERE " . implode(" AND ", $where) . " GROUP BY oa.google_place ORDER BY " . implode(", ", $limit);
                return $db->getAll($sql);
            }else if($tab == "local"){
                $not_empty = ['oa.sign_building','oa.street_name','oa.street_num','oa.google_place','oa.administrative_area','oa.floor','oa.block','oa.phase','oa.room'];
                $not_empty = array_map(function($v){return $v." = ''";}, $not_empty);
                $where = array_merge($where,["oa.address != 'NULL'","oa.country = 3409","oa.hk_area = 0","oa.locality = ''", "NOT(".implode(" AND ",$not_empty).")"]);
                $limit = array_merge($limit,["oa.staying_time DESC"]);
                return $this->get_address($where, implode(", ", $limit));
            }else if($tab == "local2"){
                $where = array_merge($where,["oa.address != 'NULL'","oa.country = 3409","oa.hk_area = 0","oa.locality != ''"]);
                $limit = array_merge($limit,["oa.staying_time DESC"]);
                return $this->get_address($where, implode(", ", $limit));
            }
        }
    }

    public function count_address($where_array = ""){
        global $ecs, $db;
        if(!empty($where_array) && !is_array($where_array))
            $where_array = array($where_array);
        if(!empty($where_array)){
            $where = " WHERE " . implode(" AND ", $where_array) ;
        }

        $sql = "SELECT COUNT(oa.order_address_id) AS num ".
                " FROM " . $ecs->table('order_address') . " AS oa ".
                " LEFT JOIN ". $ecs->table('order_address_dest_type') . " AS oadt ON oadt.address_dest_type_id = oa.dest_type " . 
                " LEFT JOIN ". $ecs->table('order_info') . " AS oi ON oi.order_id = oa.order_id ".
                $where;
        $count = $db->getROW($sql);
        return $count['num'];
    }

    public function get_address($where_array = "", $order = "", $limit = "50", $offset = "0"){
        global $ecs, $db;
        if(!empty($where_array) && !is_array($where_array))
            $where_array = array($where_array);
        if(!empty($where_array)){
            $where = " WHERE " . implode(" AND ", $where_array);
        }
        if(!empty($order))
            $order = " ORDER BY " . $order;

        $limit = " LIMIT " . $limit;
        $offset = " OFFSET " . $offset;
        $sql = "SELECT oa.order_address_id, oa.order_id, oa.consignee, oa.country, oa.province, oa.city, oa.district, oa.address, oa.sign_building, oa.street_name, oa.street_num, oa.locality, oa.administrative_area, oa.floor, oa.block, oa.phase, oa.room, oa.hk_area, oa.area_type, oa.dest_type, oa.lift, oa.verify, oa.zipcode, oa.google_place, oa.staying_time, oadt.address_dest_type_name " .
                " FROM " . $ecs->table('order_address') . " AS oa " . 
                " LEFT JOIN ". $ecs->table('order_address_dest_type') . " AS oadt ON oadt.address_dest_type_id = oa.dest_type " . 
                " LEFT JOIN ". $ecs->table('order_info') . " AS oi ON oi.order_id = oa.order_id ".
                $where . " " . $order . " " . $limit . " " . $offset;
        $result = $db->getAll($sql);

        return $result;
    }

    public function update_address(){
        global $ecs, $db;
        foreach($_REQUEST as $key => $value){
            if(!in_array($key,['act','address_id','address','finish'])){
                $sql = "UPDATE " . $ecs->table('order_address') . " SET " . $key . " = '" . $value ."' WHERE order_address_id = " . $_REQUEST['address_id'];
                $db->query($sql);
            }
        }
        if($_REQUEST['administrative_area'] != ""){
            if(in_array($_REQUEST['administrative_area'], ['Kowloon','九龍']) ){
                $province = 3412;
            } elseif (in_array($_REQUEST['administrative_area'], ['New Territories','新界']) ){
                $province = 3413;
            } elseif (in_array($_REQUEST['administrative_area'], ['Hong Kong Island','香港島']) ){
                $province = 3414;
            } else {
                $province = 0;
            }
            $sql = "UPDATE " . $ecs->table('order_address') . " SET province = " . $province . " WHERE order_address_id = " . $_REQUEST['address_id'];
            $db->query($sql);
        }
        $sql = "UPDATE " . $ecs->table('order_address') . " SET auto_break = 0 WHERE order_address_id = " . $_REQUEST['address_id'];
        $db->query($sql);
    }

    public function format_staying_time($time = 0){
        $str = gmdate("z:H:i:s", intval($time));
        $str_arr = explode(':',$str);
        $format = ['日','小時','分鐘','秒'];
        $formatted_time = "";
        for($i = 0; $i < 4 && $formatted_time == ""; $i++){
            if(intval($str_arr[$i] > 0)){
                $formatted_time .= intval($str_arr[$i]) . $format[$i];
            }
        }
        if($formatted_time == "") $formatted_time = "0秒";
        return $formatted_time;
    }

    public function address_paging($addr_list_all,$page_size,$page){
        global $smarty;
        $record_count = count($addr_list_all);
        $page_count = ceil($record_count / $page_size);
        $prev_page = $page > 1 ? $page - 1 : 0;
        $next_page = $page < $page_count ? $page + 1 : 0;
        for($i = ($page - 1) * $page_size; $i < $page * $page_size && $i < count($addr_list_all); $i++){
            $addr_list[] = $addr_list_all[$i];
        }
        $smarty->assign('addr_list', $addr_list);
        $smarty->assign('data', $addr_list);

        $smarty->assign('record_count', $record_count);
        $smarty->assign('page_count', $page_count);
        $smarty->assign('prev_page', $prev_page);
        $smarty->assign('next_page', $next_page);
        $smarty->assign('page_size', $page_size);
        $smarty->assign('page',$page);
    }

    /** This function is handle which region can calculate free shipping goods. */
    public function get_free_calculate_region()
    {
        global $db, $ecs;

        $sql = "SELECT region_id, region_type FROM ".$ecs->table('region')." WHERE free_calculate = 1";
        $region_list = $db->getAll($sql);

        $arr = [
            'country'  => [],
            'province' => [],
            'city'     => [],
            'district' => []
        ];
        foreach ($region_list as $region) {
            $region_type = $region['region_type'];
            switch ($region_type) {
                case 0:
                    $level = 'country';
                    break;
                case 1:
                    $level = 'province';
                    break;
                case 2:
                    $level = 'city';
                    break;
                case 3:
                    $level = 'district';
                    break;
                default:
                    $level = 'level'.($region_type + 1).'_region';
                    break;
            }
            $arr[$level][] = $region['region_id'];
        }

        return $arr;
    }

    /** This function is using to async curl call hkgov/google API */
    public function batch_address_curl($address_list = [])
    {

        if (!is_array($address_list) || empty($address_list))
        return false;

        // Handle address get url:
        foreach ($address_list as $key => $address) {
            $address_string = make_semiangle($address['address']);
            $m   = mb_strlen($address_string, 'utf-8');
            $s   = strlen($address_string);
            $is_eng_address = false;
            if($s==$m){
                $is_eng_address = true; // Eng address
            } else {
                $is_eng_address = false;
            }
            // $address_string = rawurlencode($address_string);
            $country = $address['country'];
            $api_platform[$key]['address'] = $address['address'];
            $api_platform[$key]['is_eng_address'] = $is_eng_address;
            if($country == 3409) {// get HKGOV url
                $address_string = rawurlencode($address_string);
                $url = 'https://www.als.ogcio.gov.hk/lookup?n=1&q='.$address_string;
                $api_platform[$key]['platform'] = 'hkgov';
            } else { //get google geo url
                $lang        = ($is_eng_address) ? 'en_us' : 'zh_tw';
                $search_address = str_replace (" ", "+", urlencode($address_string));
                $google_key         = "AIzaSyDHUTKhF0H7C7omzT-ifncxaPh1mW-tvDo"; //This Key is only can used by yohongkong.com's IP
                if (strpos($_SERVER['SERVER_NAME'], 'beta.') !== false)$google_key = "AIzaSyBhsbwdQdsL2ugyuylW3KA3q_pytM4Keaw";
                $url = "https://maps.googleapis.com/maps/api/geocode/json?address=".$search_address."&key=".$google_key."&language=".$lang;
                $api_platform[$key]['platform'] = 'google';
            }
            $url_list[$key] = $url;
        }
        $data = $this->aync_get_curl($url_list, 15);
        foreach ($data as $key => $consignee) {
            $api = $api_platform[$key]['platform'];
            $is_eng_address = $api_platform[$key]['is_eng_address'];
            $country = $address_list[$key]['country'];
            $address = $api_platform[$key]['address'];
            if($api == 'hkgov') {
                $result = $this->hk_gov_address_analysis($address, $is_eng_address,1, $consignee);
            } else {
                $result = $this->google_geo_address_analysis($address, $is_eng_address, $consignee);
            }

            $consignee = $this->breakOffAddress($address, $country, false, $result[0]['consignee']);
            $return[$key] = $consignee;
        }
        return $return;
    }

    public function get_eflocker_list($need_group = true)
    {
        global $_CFG;
        $today = local_strtotime('today');
        $lang = $_CFG['lang'];
        $ef_list  = $this->memcache->get('ef_list_'.$today.'_'.$lang);
        if (!$ef_list) {

            $beta = (strpos($_SERVER['SERVER_NAME'], 'beta.yohohongkong.com') !== false);
            $lang_num = ($lang == 'zh_tw') ? 1 : 0;
            if($beta) {
                $url = 'http://ipl-hibox.sit.sf-express.com:4480/dropbox/hibox/expressCompany/queryEdmEdinfo';
                $content = json_encode(["appId"=>"sittest","transId"=>"IPL563490", "language"=>$lang_num]);
            } else {
                $url = "http://openapi.eflocker.com/hibox/expressCompany/queryEdmEdinfo";
                $content = json_encode(["appId"=>"yohohongkong","transId"=>"IPL281316", "language"=>$lang_num]);
            }
            // TODO: update $_CFG later.
            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt(
                $curl,
                CURLOPT_HTTPHEADER,
                    array("Content-type: application/json")
            );
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

            $json_response = curl_exec($curl);

            $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            if ($status != 201) {
                // TODO: Error handle.
            // die("Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
            }
            curl_close($curl);

            $response = json_decode($json_response, true);
            $list     = $response['data'];
            $ef_list = [];
            foreach($list as $key => $ef) {
                $ef_list[$ef['edCode']] = $ef;
            }
            $this->memcache->set('ef_list_'.$today.'_'.$lang, $ef_list);
        }

        if($need_group) {
            $group_list = [];
            foreach($ef_list as $key => $ef) {
                $group_list[$ef['city']][$ef['district']][$ef['allotRouter']][$ef['edCode']] = $ef;
            }
            return $group_list;
        } else {
            return $ef_list;
        }
    }

    function locker_consignee($user_id, $locker_info, $update_address = true)
    {
        global $db, $ecs, $_CFG;
        $area        = $locker_info['locker_area'];
        $region      = $locker_info['locker_region'];
        $allotRouter = $locker_info['allotRouter'];
        $lockerCode  = $locker_info['lockerCode'];
        $secondCode  = $locker_info['allotRouterCode'];
        $tel  = $locker_info['locker_tel'];
        $consignee   = $locker_info['locker_consignee'];
        $lang        = $_CFG['lang'];

        if(!empty($user_id)) {
            if(empty($area) || empty($region) || !isset($allotRouter) || empty($lockerCode)) {
                return get_consignee($user_id);
            }
    
            // Check user have locker type address:
            $sql = "SELECT * FROM ".$ecs->table('user_address')." WHERE user_id = $user_id AND address_type = '".self::ADDRESS_TYPE_LOCKER."' LIMIT 1";
            $address = $db->getRow($sql);
            if(!$address) {
                $old_address = get_consignee($user_id);
                $user        = user_info($user_id);
                $address = [];
                $consignee = empty($locker_info['locker_consignee']) ? $old_address['consignee'] : $locker_info['locker_consignee'];
                $email     = $user['email'];
                $mobile    = $user['user_name'];
                $tel       = empty($locker_info['locker_tel']) ? $user['user_name'] : $locker_info['locker_tel'];
                $id        = 0;
            } else {
                $consignee = empty($locker_info['locker_consignee']) ? $address['consignee'] : $locker_info['locker_consignee'];
                $email     = empty($locker_info['email']) ? $address['email'] : $locker_info['email'];
                $mobile    = empty($locker_info['mobile']) ? $address['mobile'] : $locker_info['mobile'];
                $tel       = empty($locker_info['locker_tel']) ? $address['tel'] : $locker_info['locker_tel'];
                $id        = $address['address_id'];
    
                $tmp = [$lockerCode];
                if(!empty($secondCode)) $tmp[] = $secondCode;
                $typeCode = unserialize($address['type_code']);
                if($typeCode == $tmp) {
                    $address['consignee']    = $consignee;
                    $address['email']        = $email;
                    $address['mobile']       = $mobile;
                    $address['tel']          = $tel;
                    return $address;
                }
            }
        }
       
        // Get locker list and locker info
        // Get shipping area address list
        $shipping = shipping_area_info($locker_info['shipping'], [], $locker_info['shipping_area']);
        include_once(ROOT_PATH.'includes/modules/shipping/' . $shipping['shipping_code'] . '.php');
        $shipping_obj    = new $shipping['shipping_code'](unserialize($shipping['configure']));
        if(method_exists($shipping_obj, 'getPickUpList')){
            $list = $shipping_obj->getPickUpList(false, $locker_info['shipping_area']);
        }else{
            $list = [];
        }
        $locker    = $list[$lockerCode];
        $type_code = [$lockerCode];

        // Try to break off locker address
        $address['address'] = explode("(", $locker['twThrowaddress'])[0];
        $address = $this->breakOffAddress($address['address'], $shipping['region_list'][0]);
        // Handle locker address format:
        if (empty($address['country'])) {
            $address['country'] = 3409;
        }
        $address['locality'] = str_replace("區", "", $locker['district']);

        if ($shipping['shipping_code'] == 'sfpickup' || $shipping['shipping_code'] == 'sfpickup_conv_shop' || $shipping['shipping_code'] == 'eflocker'){
            $pos_type_bracket = -1;
            if ($shipping['shipping_code'] == 'sfpickup_conv_shop'){
                $pos_type_bracket = strpos($locker['twThrowaddress'], ' [');
                if ($pos_type_bracket > 0){
                    $new_address = substr_replace($locker['twThrowaddress'], ' ['.$shipping['shipping_name'].': '.$lockerCode.']', $pos_type_bracket, 0);
                }
            } 
            if (!$pos_type_bracket || $pos_type_bracket <=0 ){
                $new_address = $locker['twThrowaddress'];
                if ($shipping['shipping_code'] == 'eflocker'){
                    $new_address .= ($lang == 'zh_cn') ? ' [首选柜: ' : ' [首選櫃: ';
                } else {
                    $new_address .= ' ['.$shipping['shipping_name'].': ';
                }
                $new_address .= $lockerCode."]";
            }
        } else {
            $new_address  = '['.$shipping['shipping_name'].': ';
            $new_address .= $lockerCode."] ";
            $new_address .= $locker['twThrowaddress'];
        }

        // ALTERNATE LOCKER handle
        if(!empty($secondCode)) {
            //$locker2 = $list[$secondCode];
            //$new_address .= ($lang == 'zh_tw') ? ' ['.$shipping['shipping_name'].': ' : ' [ALTERNATE '.$shipping['shipping_name'].': ';
            $new_address .= ($lang == 'zh_cn') ? ' [次选柜: ' : ' [次選櫃: ';
            $new_address .= $secondCode."]";
            $type_code[] = $secondCode;
        }

        // set sql data
        $address['address'] = $new_address;
        $address = $this->handle_address_detail($address, 0);
        if(in_array($locker['throwareaType'], ['Apartment', '住宅'])) {
            $address['area_type'] = self::AREA_TYPE_FLAT;
        } else {
            $address['area_type'] = self::AREA_TYPE_BUSINESS;
        }
        $address['address_type'] = self::ADDRESS_TYPE_LOCKER;
        $address['type_code']    = serialize($type_code);
        $address['user_id']      = $user_id;
        $address['consignee']    = $consignee;
        $address['email']        = $email;
        $address['mobile']       = $mobile;
        $address['tel']          = $tel;
        
        if(!empty($user_id)) {
            if($update_address == true) {
                // insert or update
                $model = new Model\YohoBaseModel('user_address');
                if($id > 0) {
                    $res = $model->update($address, $id);
                    $address['address_id'] = $id;
                } else {
                    $res = $model->insert($address);
                    $address['address_id'] = $res;
                }
            }
        }

        return $address;
    }

    public function check_order_address_record($address_country, $address_admin_area, $address_building, $address_phase, $address_block, $address_street_name, $address_street_num){
        if ($address_admin_area == "" || ($address_building == "" && $address_phase == "" && ($address_street_name == "" || $address_street_num == ""))){
            return false;
        } else {
            global $ecs, $db, $_LANG;
            $sql = "SELECT IF(oi.shipping_status > 0, '1', '0') AS ship, IFNULL(oadt.address_dest_type_cat, '-1') AS cat, oadt.address_dest_type_code AS type, oa.dest_type AS type_id, oa.lift AS lift ".
                    "FROM ".$ecs->table('order_address')." AS oa ".
                    "LEFT JOIN ".$ecs->table('order_info')." AS oi ON oi.order_id = oa.order_id ".
                    "LEFT JOIN ".$ecs->table('order_address_dest_type')." AS oadt ON oadt.address_dest_type_id = oa.dest_type ".
                    "WHERE oa.country = ".$address_country." ".
                    "AND oa.administrative_area = '".$address_admin_area."' ".
                    "AND (";
                    if ($address_building != "" && $address_phase != ""){
                        $sql .= "((oa.sign_building LIKE '%".$address_building."%' AND oa.sign_building !='') OR (oa.phase LIKE '%".$address_phase."%' AND oa.phase !='')) ";
                    } else if ($address_building != "" && $address_phase == ""){
                        $sql .= "(oa.sign_building LIKE '%".$address_building."%' AND oa.sign_building !='')  ";
                    } else if ($address_building == "" && $address_phase != "") {
                        $sql .= "(oa.phase LIKE '%".$address_phase."%' AND oa.phase !='') ";
                    }
                    if ($address_street_name != "" && $address_street_num != ""){
                        if ($address_building != "" || $address_phase != ""){
                            $sql .= " OR ";
                        }
                        $sql .= "(oa.street_name = '".$address_street_name."' AND oa.street_num = '".$address_street_num."' AND oa.street_name !='' AND oa.street_num !='') ";
                    }
                    $sql .= ") ";

                    $sql .= "AND (oa.dest_type != 0 AND oa.lift != 0) ".
                            "ORDER BY oa.verify DESC, ship DESC, oa.create_at DESC ";

                    $res = $db->getRow($sql);
            if (empty($res)){
                /*
                if ($_SESSION['user_id']){
                    $sql = "SELECT IFNULL(oadt.address_dest_type_cat, '-1') AS cat, oadt.address_dest_type_code AS type, ua.dest_type AS type_id, ua.lift AS lift ".
                    "FROM ".$ecs->table('user_address')." AS ua ".
                    "LEFT JOIN ".$ecs->table('order_address_dest_type')." AS oadt ON oadt.address_dest_type_id = ua.dest_type ".
                    "WHERE ua.country = ".$address_country." ".
                    "AND ua.administrative_area = '".$address_admin_area."' ".
                    "AND (ua.sign_building LIKE '%".$address_building."%' OR ua.phase LIKE '%".$address_phase."%') ".
                    "AND ua.user_id = '".$_SESSION['user_id']."' ".
                    "AND (ua.dest_type != 0 OR ua.lift != 0) ".
                    "ORDER BY ua.address_id DESC";

                    $res = $db->getRow($sql);

                    if (!empty($res)){
                        return $res;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
                */
                return false;
            } else {
                return $res;
            }
        }
    }
    
    public function get_address_types(){
        global $ecs, $db, $_LANG;
        $sql = "SELECT oadt.address_dest_type_code AS type_code, oadt.address_dest_type_cat AS type_cat, oadt.address_dest_type_id AS type_id,  IFNULL(oadtl.address_dest_type_name, oadt.address_dest_type_name) AS type_name ".
                "FROM ".$ecs->table('order_address_dest_type')." AS oadt ".
                "LEFT JOIN ".$ecs->table('order_address_dest_type_lang')." AS oadtl ON oadtl.address_dest_type_id = oadt.address_dest_type_id AND oadtl.lang = '".$GLOBALS['_CFG']['lang']."' ".
                "WHERE oadt.enable = 1 ".
                "ORDER BY oadt.address_dest_type_cat ASC, oadt.address_dest_type_id ASC";
        $res = $db->getAll($sql);
        if (empty($res)){
            return false;
        } else {
            return $res;
        }
    }
    
    public function get_address_types_groups(){
        global $ecs, $db, $_LANG;
        $sql = "SELECT DISTINCT address_dest_type_cat AS cat ".
                "FROM ".$ecs->table('order_address_dest_type')." ".
                "WHERE enable = 1 ".
                "ORDER BY address_dest_type_cat ASC ";
        $res = $db->getAll($sql);
        if (empty($res)){
            return false;
        } else {
            return $res;
        }
    }

    public function addressEditAction($table){
        global $ecs, $db, $_LANG;
        $col   = $_REQUEST['col'];
        $value = $_REQUEST['value'];
        $attribute = $_REQUEST['mkey'];
        $key   = $_REQUEST['id'];
        $type  = $_REQUEST['type'];

        $sql = "UPDATE ".$ecs->table($table)." ".
                'SET '.$col.' = "'.$value.'" '.
                "WHERE ".$attribute." = ".$key;

        $db->query($sql);
    }
    
    public function search_match_area_id($area_text){
        global $ecs, $db, $_LANG;

        $sql = "SELECT area_id ".
                "FROM ".$ecs->table('hk_areas')." ".
                "WHERE LOWER(area_eng_name) = LOWER('".$area_text."') OR area_chi_name = '".$area_text."' ";
        $res = $db->getAll($sql);
        if (sizeof($res) > 0){
            return $res[0]['area_id'];
        } else {
            return "-1";
        }
    }
    
    public function search_user_address($order_addr_id){
        global $ecs, $db, $_LANG;

        $sql = "SELECT oa.order_address_id, oa.address, o.user_id ".
                "FROM ".$ecs->table("order_address")." AS oa ".
                "LEFT JOIN ".$ecs->table("order_info")." AS o ON o.order_id = oa.order_id ".
                "WHERE oa.order_address_id = ".$order_addr_id." ";

        $order_addr_key = $db->getRow($sql);

        if (isset($order_addr_key) && !empty($order_addr_key) && !empty($order_addr_key['user_id'])){

            $user_addr_id = $this->search_match_user_address($order_addr_key);

            return $user_addr_id;
        } else {
            return FALSE;
        }

    }
    
    public function get_order_address_by_id($order_addr_id){
        global $ecs, $db, $_LANG;

        $sql = "SELECT * ".
                "FROM ".$ecs->table("order_address")." ".
                "WHERE order_address_id = ".$order_addr_id." ";

        $order_addr= $db->getRow($sql);

        if (isset($order_addr) && !empty($order_addr)){

            $full_addr = $this->handle_address_detail($order_addr);

            if(!empty($full_addr) && !empty($full_addr['address'])){
                return $full_addr['address'];
            } else {
                return FALSE;
            }

        } else {
            return FALSE;
        }

    }

    public function search_match_user_address($addr_key){
        global $ecs, $db, $_LANG;
        $sql = "SELECT address_id ".
                "FROM ".$ecs->table("user_address")." ".
                "WHERE user_id = ".$addr_key['user_id']." ".
                'AND address = "'.$addr_key["address"].'" ';

        $addr_id = $db->getRow($sql);

        if (!empty($addr_id) && $addr_id[address_id] > 0){
            return $addr_id[address_id];
        } else {
            return FALSE;
        }
    }

    public function get_verified_addr_break($building = "", $place = "", $street_name = "", $street_num = "", $admin_area = ""){
        global $ecs, $db, $_LANG;
        if (empty($admin_area) || (empty($building) && empty($place) && (empty($street_name) || empty($street_num)))){
            return FALSE;
        } else {
            $gov_keyword = (!empty($place)?$place:$building);
            $gov_building_name = "";
            $gov_estate_name = "";
            $url = 'https://www.als.ogcio.gov.hk/lookup?q='.$gov_keyword;
            $xml = simplexml_load_file($url);
            if($xml){
                $data = $xml->SuggestedAddress[0]->Address->PremisesAddress;
                if (isset($data)){
                    $m   = mb_strlen($gov_keyword,'utf-8');
                    $s   = strlen($gov_keyword);
                    $is_eng_address = false;
                    if($s==$m){
                        $is_eng_address = true; // eng
                    } else {
                        $is_eng_address = false;
                    }
                    $gov_building_name = ucwords(strtolower((string)($is_eng_address ? $data->EngPremisesAddress->BuildingName : $data->ChiPremisesAddress->BuildingName)));
                    $gov_estate_name = ucwords(strtolower((string)($is_eng_address ? $data->EngPremisesAddress->EngEstate->EstateName : $data->ChiPremisesAddress->ChiEstate->EstateName)));
                }
                
                if (!empty($building) && !empty($place) && $building == $place){
                    if (!empty($gov_building_name)) {$building = $gov_building_name;}
                    if (!empty($gov_estate_name)) {$place = $gov_estate_name;}
                }
            }
            $search_street_num  = array(' 號', '號', 'no ', 'no. ', 'no', 'no.');
            $replace_street_num = array('', '', '', '', '', '');
            $street_num = str_replace($search_street_num, $replace_street_num, strtolower($street_num));
            $street_num = str_replace("no.", "", str_replace("號", "", strtolower($street_num)));
            $sql = "SELECT country, province, sign_building, street_name, street_num, locality, administrative_area, phase, hk_area ".
                    "FROM ".$ecs->table("order_address")." ".
                    "WHERE administrative_area LIKE '%".$admin_area."%' ".
                    "AND verify = 1 ".
                    "AND country = 3409 ".
                    "AND (";
                    if ($building != ""){
                        $sql .= "(sign_building LIKE '%".$building."%' AND sign_building != '') OR (phase LIKE '%".$building."%' AND phase != '') OR ";
                    }
                    if ($place != ""){
                        $sql .= "(address LIKE '%".$place."%' AND address != '') OR (sign_building LIKE '%".$place."%' AND sign_building != '') OR (phase LIKE '%".$place."%' AND phase != '') OR ";
                    }
                    $sql .= "(street_name = '".$street_name."' AND street_num = '".$street_num."' AND street_name != '')) ";
                    $sql .="ORDER BY verify DESC, create_at DESC ";

            $addr_break = $db->getRow($sql);

            if (!empty($addr_break)){
                return $addr_break;
            } else {
                if (!empty($gov_building_name) || !empty($gov_estate_name)){
                    return array("country" => "", "province" => "", "sign_building" => $gov_building_name, "street_name" => "", "street_num" => "", "locality" => "", "administrative_area" => "", "phase" => $gov_estate_name, "hk_area" => "");
                } else {
                    return FALSE;
                }
            }
        }
    }

    public function count_building_no($where_array = ""){
        global $ecs, $db;
        if(!empty($where_array) && !is_array($where_array))
            $where_array = array($where_array);
        if(!empty($where_array)){
            $where = " WHERE oa.sign_building != '' AND " . implode(" AND ", $where_array);
        }
        $sql = "SELECT oa.sign_building, SUM(CASE WHEN user_id = 0 THEN 1 ELSE 0 END) + COUNT(DISTINCT(CASE WHEN user_id > 0 THEN user_id END)) AS count " .
                " FROM " . $ecs->table('order_address') . " AS oa " . 
                " LEFT JOIN " . $ecs->table('order_info') . " oi ON oa.order_id = oi.order_id " .
                " LEFT JOIN " . $ecs->table('order_address_dest_type') . " AS oadt ON oadt.address_dest_type_id = oa.dest_type " . 
                $where . " GROUP BY oa.sign_building ";
        
        $result = $db->getAll($sql);

        return sizeof($result);
    }

    public function count_building($where_array = "", $order = "", $limit = "50", $offset = "0"){
        global $ecs, $db;
        if(!empty($where_array) && !is_array($where_array))
            $where_array = array($where_array);
        if(!empty($where_array)){
            $where = " WHERE oa.sign_building != '' AND " . implode(" AND ", $where_array);
        }
        if(!empty($order))
            $order = " ORDER BY " . $order ." ";

        $limit = " LIMIT " . $limit;
        $offset = " OFFSET " . $offset;
        $sql = "SELECT oa.sign_building, SUM(CASE WHEN user_id = 0 THEN 1 ELSE 0 END) + COUNT(DISTINCT(CASE WHEN user_id > 0 THEN user_id END)) AS count " .
                " FROM " . $ecs->table('order_address') . " AS oa " . 
                " LEFT JOIN " . $ecs->table('order_info') . " oi ON oa.order_id = oi.order_id " .
                " LEFT JOIN " . $ecs->table('order_address_dest_type') . " AS oadt ON oadt.address_dest_type_id = oa.dest_type " . 
                $where . " GROUP BY oa.sign_building " . $order . " " . $limit . " " . $offset;
        $result = $db->getAll($sql);

        return $result;
    }

    public function count_google_place_no($where_array = ""){
        global $ecs, $db;
        if(!empty($where_array) && !is_array($where_array))
            $where_array = array($where_array);
        if(!empty($where_array)){
            $where = " WHERE oa.google_place != '' AND " . implode(" AND ", $where_array);
        }
        $sql = "SELECT oa.google_place, SUM(CASE WHEN user_id = 0 THEN 1 ELSE 0 END) + COUNT(DISTINCT(CASE WHEN user_id > 0 THEN user_id END)) AS count " .
                " FROM " . $ecs->table('order_address') . " AS oa " . 
                " LEFT JOIN " . $ecs->table('order_info') . " oi ON oa.order_id = oi.order_id " .
                " LEFT JOIN " . $ecs->table('order_address_dest_type') . " AS oadt ON oadt.address_dest_type_id = oa.dest_type " . 
                $where . " GROUP BY oa.google_place ";
        
        $result = $db->getAll($sql);

        return sizeof($result);
    }

    public function count_google_place($where_array = "", $order = "", $limit = "50", $offset = "0"){
        global $ecs, $db;
        if(!empty($where_array) && !is_array($where_array))
            $where_array = array($where_array);
        if(!empty($where_array)){
            $where = " WHERE oa.google_place != '' AND " . implode(" AND ", $where_array);
        }
        if(!empty($order))
            $order = " ORDER BY " . $order ." ";

        $limit = " LIMIT " . $limit;
        $offset = " OFFSET " . $offset;
        $sql = "SELECT oa.google_place, SUM(CASE WHEN user_id = 0 THEN 1 ELSE 0 END) + COUNT(DISTINCT(CASE WHEN user_id > 0 THEN user_id END)) AS count " .
                " FROM " . $ecs->table('order_address') . " AS oa " . 
                " LEFT JOIN " . $ecs->table('order_info') . " oi ON oa.order_id = oi.order_id " .
                " LEFT JOIN " . $ecs->table('order_address_dest_type') . " AS oadt ON oadt.address_dest_type_id = oa.dest_type " . 
                $where . " GROUP BY oa.google_place " . $order . " " . $limit . " " . $offset;
        $result = $db->getAll($sql);

        return $result;
    }

    public function check_user_address_correct($arr_addr){
        global $ecs, $db, $_LANG;

        if ($arr_addr['dest_type'] == 0 || $arr_addr['lift'] == 0 || empty($arr_addr['country']) || empty($arr_addr['locality']) || empty($arr_addr['administrative_area']) || (empty($arr_addr['sign_building']) && empty($arr_addr['phase']) && empty($arr_addr['street_name']))){
            return FALSE;
        } else {
            $sql = 'SELECT order_address_id '.
                    'FROM '.$ecs->table('order_address').' '.
                    'WHERE country = '.$arr_addr['country'].' '.
                    'AND locality = "'.$arr_addr['locality'].'" '.
                    'AND administrative_area = "'.$arr_addr['administrative_area'].'" '.
                    'AND sign_building = "'.$arr_addr['sign_building'].'" '.
                    'AND street_name = "'.$arr_addr['street_name'].'" '.
                    'AND street_num = "'.$arr_addr['street_num'].'" '.
                    'AND phase = "'.$arr_addr['phase'].'" '.
                    'AND verify = 1 '.
                    'ORDER BY order_address_id DESC ';

            $res = $db->getRow($sql);

            if (!empty($res)){
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    public function remap_address_to_ordera($arr_addr_id_be_merged, $target_addr_id){
        global $ecs, $db;
        $str_addr_id_be_merged = implode(",",$arr_addr_id_be_merged);
        $sql = "SELECT oi.user_id FROM ".$ecs->table('order_address')." AS oa LEFT JOIN ".$ecs->table('order_info')." AS oi ON oi.order_id = oa.order_id WHERE oa.order_address_id IN (".$target_addr_id.",".$str_addr_id_be_merged.") GROUP BY oi.user_id";
        $addr_user = $db->getAll($sql);
        if (!empty($addr_user) && sizeof($addr_user) == 1){
            $sql = "SELECT address FROM ".$ecs->table('order_address')." WHERE order_address_id = $target_addr_id";
            $arr_target_full_addr = $db->getRow($sql);
            if (!empty($arr_target_full_addr)){
                $target_full_addr = $arr_target_full_addr['address'];
                //$sql = "UPDATE ".$ecs->table('order_info')." SET address = '".$target_full_addr."', address_id = ".$target_addr_id." WHERE address_id IN (".$str_addr_id_be_merged.")";
                //$db->query($sql);
                //$sql = "UPDATE ".$ecs->table('order_address')." SET order_id = 0, verify = -1 WHERE order_address_id IN (".$str_addr_id_be_merged.")";
                $sql = "UPDATE ".$ecs->table('order_address')." SET verify = -1 WHERE order_address_id IN (".$str_addr_id_be_merged.")";
                $db->query($sql);
                $msg = "";
                return $msg;
            } else {
                $msg = "error: cannot get address";
                return $msg;
            }
        } else {
            $msg = "error: addresses not from same user";
            return $msg;
        }
    }

}
