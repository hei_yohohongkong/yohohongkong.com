<?php
# @Date:   2017-05-23T18:06:54+08:00
# @Filename: PaymentController.php
# @Last modified time: 2017-06-07T09:34:34+08:00
# @author: Anthony@YOHO

namespace Yoho\cms\Controller;

require_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_accounting.php');

use Yoho\cms\Model;

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}


class PaymentController extends YohoBaseController
{
    const DB_ACTION_CODE_PAYMENT  = 'payment';
    const PAYMENT_RULE_TYPE       = ['country', 'price', 'rank'];
    const PAYMENT_RULE_EQUIVALENT = ['eq', 'neq', 'gt', 'lt', 'gte', 'lte']; // ==, !=, >, <, >=, <=
    const PLATFORM_WEB            = 'web';
    const PLATFORM_MOBILE         = 'mobile';
    const LOGO_PATH               = "../" . DATA_DIR . "/afficheimg/payment_logo/";
    const EXHIBITION_POS          = ['cash', 'visamaster', 'stripe'];
    
    private $tablename = 'payment';
    
    public $actg;
    public $actgHooks;
    
    public function __construct()
    {
        parent::__construct();
        $actg = new \Accounting($this->db);
        $actgHooks = new \AccountingHooks($actg, $this->db);
        $this->actg = $actg;
        $this->actgHooks = $actgHooks;
    }
    
    /* Get payment modules list */
    public function get_payment_modules($lang = null)
    {
        global $db, $ecs, $_LANG;
        
        include_once(ROOT_PATH.'includes/lib_compositor.php');
        
        /* Query SQL */
        $pay_list = array();
        $sql = "SELECT * FROM " . $ecs->table('payment') . " WHERE enabled = '1' ORDER BY pay_order";
        $res = $db->getAll($sql);
        
        // Multiple language support
        if (!empty($lang))
        {
            $res = localize_db_result_lang($lang, 'payment', $res);
        }
        
        foreach ($res as $row)
        {
            $pay_list[$row['pay_code']] = $row;
        }
        
        /* 取得插件文件中的支付方式 */
        $modules = read_modules('../includes/modules/payment');
        for ($i = 0; $i < count($modules); $i++)
        {
            $code = $modules[$i]['code'];
            $modules[$i]['pay_code'] = $modules[$i]['code'];
            /* 如果数据库中有，取数据库中的名称和描述 */
            if (isset($pay_list[$code]))
            {
                $modules[$i]['name'] = $pay_list[$code]['pay_name'];
                $modules[$i]['display_name'] = (isset($pay_list[$code]['display_name']) && $pay_list[$code]['display_name'] != '') ? $pay_list[$code]['display_name'] : $pay_list[$code]['pay_name'];
                $modules[$i]['pay_fee'] =  $pay_list[$code]['pay_fee'];
                $modules[$i]['is_cod'] = $pay_list[$code]['is_cod'];
                $modules[$i]['desc'] = $pay_list[$code]['pay_desc'];
                $modules[$i]['pay_order'] = $pay_list[$code]['pay_order'];
                $modules[$i]['install'] = '1';
                $modules[$i]['is_pos'] = $pay_list[$code]['is_pos'];
                $modules[$i]['is_online'] = $pay_list[$code]['is_online'];
                $modules[$i]['is_online_pay'] = $pay_list[$code]['is_online_pay'];
                $modules[$i]['is_debug_mode'] = $pay_list[$code]['is_debug_mode'];
                if(isset($pay_list[$code]['platform']))
                {
                    $platform = explode(',', $pay_list[$code]['platform']);
                    foreach ($platform as $key => $value) {
                        $platform[$key] = isset($_LANG[$value]) ? $_LANG[$value] : $value;
                    }
                    $modules[$i]['platform'] = $platform;
                }
            }
            else
            {
                $modules[$i]['name'] = $_LANG[$modules[$i]['code']];
                $modules[$i]['is_pos'] = 0;
                if (!isset($modules[$i]['pay_fee']))
                {
                    $modules[$i]['pay_fee'] = 0;
                }
                $modules[$i]['desc'] = $_LANG[$modules[$i]['desc']];
                $modules[$i]['install'] = '0';
            }
            if ($modules[$i]['pay_code'] == 'tenpayc2c')
            {
                $tenpayc2c = $modules[$i];
            }
        }
        
        $data = array(
            'modules'   => $modules,
            'tenpayc2c' => $tenpayc2c
        );
        
        return $data;
    }
    
    /* Setup payment module */
    public function setup_payment_modules($code)
    {
        global $_LANG;
        /* 取相应插件信息 */
        $set_modules = true;
        include_once(ROOT_PATH.'includes/modules/payment/' . $code . '.php');
        
        $data = $modules[0];
        /* 对支付费用判断。如果data['pay_fee']为false无支付费用，为空则说明以配送有关，其它可以修改 */
        if (isset($data['pay_fee']))
        {
            $data['pay_fee'] = trim($data['pay_fee']);
        }
        else
        {
            $data['pay_fee']     = 0;
        }
        
        $pay['pay_code']    = $data['code'];
        $pay['pay_name']    = $_LANG[$data['code']];
        $pay['pay_desc']    = $_LANG[$data['desc']];
        $pay['is_cod']      = $data['is_cod'];
        $pay['pay_fee']     = $data['pay_fee'];
        $pay['is_online']   = $data['is_online'];
        $pay['is_online_pay']   = $data['is_online_pay'];
        $pay['is_debug_mode']   = $data['is_debug_mode'];
        $pay['pay_config']  = array();
        $banks = [];
        foreach ($data['config'] AS $key => $value)
        {
            if($data['config'][$key]['name'] == 'banks')
            {
                foreach ($data['config'][$key]['value'] as $k => $v) {
                    $index = $k + 1;
                    $banks[$index][0] = $v['name'];
                    $banks[$index][1] = $v['account'];
                }
                $pay['banks'] = $banks;
            }
            else
            {
                $config_desc = (isset($_LANG[$value['name'] . '_desc'])) ? $_LANG[$value['name'] . '_desc'] : '';
                    $pay['pay_config'][$key] = $value +
                    array('label' => $_LANG[$value['name']], 'value' => $value['value'], 'desc' => $config_desc);
                    if ($pay['pay_config'][$key]['type'] == 'select' ||
                    $pay['pay_config'][$key]['type'] == 'radiobox')
                    {
                        $pay['pay_config'][$key]['range'] = $_LANG[$pay['pay_config'][$key]['name'] . '_range'];
                    }
            }
        }
        return $pay;
    }
    
    /* Get payment from ROOT_PATH.includes/modules/payment/ */
    public function get_payment_modules_file($code)
    {
        global $db, $ecs, $_LANG;
        
        $payment = new Model\Payment($code);
        $pay = $payment->data;
        $pay['pay_id'] = $payment->getId();
        /* 取相应插件信息 */
        $set_modules = true;
        include_once(ROOT_PATH.'includes/modules/payment/' . $code . '.php');
        $data = $modules[0];
        $banks = [];
        /* 取得配置信息 */
        if (is_string($pay['pay_config']))
        {
            $store = unserialize($pay['pay_config']);
            /* 取出已经设置属性的code */
            $code_list = array();
            foreach ($store as $key=>$value)
            {
                if($value['name'] == 'banks')
                {
                    foreach($value['value'] as $k => $v)
                    {
                        $index = $k + 1;
                        $banks[$index][0] = $v['name'];
                        $banks[$index][1] = $v['account'];
                    }
                }
                elseif ($value['name'] == 'tutorial')
                {
                    $conf_tutorial = $value['value'];
                }
                else
                {
                    $code_list[$value['name']] = $value['value'];
                }
            }
            $pay['pay_config'] = array();
            /* 循环插件中所有属性 */
            foreach ($data['config'] as $key => $value)
            {
                $pay['pay_config'][$key]['desc'] = (isset($_LANG[$value['name'] . '_desc'])) ? $_LANG[$value['name'] . '_desc'] : '';
                $pay['pay_config'][$key]['label'] = $_LANG[$value['name']];
                $pay['pay_config'][$key]['name'] = $value['name'];
                $pay['pay_config'][$key]['type'] = $value['type'];
                
                if (isset($code_list[$value['name']]))
                {
                    $pay['pay_config'][$key]['value'] = $code_list[$value['name']];
                }
                else
                {
                    if($value['name'] == 'banks' && sizeof($banks) == 0)
                    {
                        foreach($value['value'] as $k => $v)
                        {
                            $index = $k + 1;
                            $banks[$index][0] = $v['name'];
                            $banks[$index][1] = $v['account'];
                        }
                    }
                    elseif ($value['name'] == 'tutorial' && empty($conf_tutorial))
                    {
                        $conf_tutorial = $value['value'];
                    }
                    else
                    {
                        $pay['pay_config'][$key]['value'] = $value['value'];
                    }
                }
                
                if ($pay['pay_config'][$key]['type'] == 'select' ||
                $pay['pay_config'][$key]['type'] == 'radiobox')
                {
                    $pay['pay_config'][$key]['range'] = $_LANG[$pay['pay_config'][$key]['name'] . '_range'];
                }
            }
        }
        $pay['banks'] = $banks;
        $pay['conf_tutorial'] = $conf_tutorial;
        /* 如果以前没设置支付费用，编辑时补上 */
        if (!isset($pay['pay_fee']))
        {
            if (isset($data['pay_fee']))
            {
                $pay['pay_fee'] = $data['pay_fee'];
            }
            else
            {
                $pay['pay_fee'] = 0;
            }
        }
        
        if(isset($pay['platform']))
        {
            $platform = explode(',', $pay['platform']);
            $pay_platform = array();
            foreach ($platform as $value) {
                $pay_platform[$value] = true;
            }
            $pay['platform'] = $pay_platform;
        }
        
        $pay['payment_logo'] = empty($pay['payment_logo']) ? null : self::LOGO_PATH. $pay['payment_logo'];
        $pay['rules'] = $this->get_rule_list($payment->getId());
        return $pay;
    }
    
    /* Payment config template */
    public function get_payment_config_template($code)
    {
        global $_LANG;
        /* 取相应插件信息 */
        $set_modules = true;
        include_once(ROOT_PATH.'includes/modules/payment/' . $code . '.php');
        $data = $modules[0]['config'];
        
        $config = '<table>';
        $range = '';
        foreach($data AS $key => $value)
        {
            $config .= "<tr><td width=80><span class='label'>";
            $config .= $_LANG[$data[$key]['name']];
            $config .= "</span></td>";
            if($data[$key]['type'] == 'text')
            {
                if($data[$key]['name'] == 'alipay_account')
                {
                    $config .= "<td><input name='cfg_value[]' type='text' value='" . $data[$key]['value'] . "' /><a href=\"https://www.alipay.com/himalayas/practicality.htm\" target=\"_blank\">".$_LANG['alipay_look']."</a></td>";
                }
                elseif($data[$key]['name'] == 'tenpay_account')
                {
                    $config .= "<td><input name='cfg_value[]' type='text' value='" . $data[$key]['value'] . "' />" . $_LANG['penpay_register'] . "</td>";
                }
                else
                {
                    $config .= "<td><input name='cfg_value[]' type='text' value='" . $data[$key]['value'] . "' /></td>";
                }
            }
            elseif($data[$key]['type'] == 'select')
            {
                $range = $_LANG[$data[$key]['name'] . '_range'];
                $config .= "<td><select name='cfg_value[]'>";
                foreach($range AS $index => $val)
                {
                    $config .= "<option value='$index'>" . $range[$index] . "</option>";
                }
                $config .= "</select></td>";
            }
            $config .= "</tr>";
            $config .= "<input name='cfg_name[]' type='hidden' value='" .$data[$key]['name'] . "' />";
            $config .= "<input name='cfg_type[]' type='hidden' value='" .$data[$key]['type'] . "' />";
            $config .= "<input name='cfg_lang[]' type='hidden' value='" .$data[$key]['lang'] . "' />";
        }
        $config .= '</table>';
        
        return $config;
    }
    
    /* Install or edit payment function */
    public function submit_form_action($post)
    {
        global $_LANG;
        $action       = $post['action'];
        /* payment info */
        $pay_code     = $post['pay_code'];
        $pay_name     = $post['pay_name'];
        $pay_fee      = empty($post['pay_fee']) ? 0 : $post['pay_fee'];
        $pay_id       = intval($post['pay_id']);
        $pay_desc     = isset($post['pay_desc']) ? $post['pay_desc'] : '';
        $is_cod       = empty($post['is_cod']) ? 0 : $post['is_cod'];
        $is_online    = empty($post['is_online']) ? 0 : $post['is_online'];
        $is_online_pay    = empty($post['is_online_pay']) ? 0 : $post['is_online_pay'];
        $is_debug_mode    = empty($post['is_debug_mode']) ? 0 : $post['is_debug_mode'];
        $is_pos       = empty($post['is_pos']) ? 0 : 1;
        $display_name = empty($post['display_name']) ? $post['pay_name'] : $post['display_name'];
        $platform     = empty($post['platform']) ? '' : implode(',', $post['platform']);
        $payment_logo = $this->upload_logo($_FILES, $pay_code);
        $conf_tutorial    = empty($post['conf_tutorial']) ? '' : trim($post['conf_tutorial']);
        $banks = [];

        foreach ($post as $key => $value) {
            preg_match('/bank(\d+)/', $key, $match);
            if (!empty($match)) {
                $index = $match[1];
                if ($post['bank' . $index] != '' || $post['ac' . $index] != '') {
                    $banks[$index - 1] = array('name'=>$post['bank' . $index],'account'=>$post['ac' . $index]);
                }
            }
        }
        /* 检查输入 */
        if (empty($pay_name) || empty($action))
        {
            sys_msg($_LANG['payment_name'] . $_LANG['empty']);
        }
        
        if ($this->check_payment_name_repeat($pay_code, $pay_name))
        {
            sys_msg($_LANG['payment_name'] . $_LANG['repeat'], 1);
        }
        
        /* 取得配置信息 */
        $pay_config = array();
        if (isset($post['cfg_value']) && is_array($post['cfg_value']))
        {
            for ($i = 0; $i < count($post['cfg_value']); $i++)
            {
                $pay_config[] = array(
                    'name'  => trim($post['cfg_name'][$i]),
                    'type'  => trim($post['cfg_type'][$i]),
                    'value' => trim($post['cfg_value'][$i])
                );
            }
        }
        $localized_versions = json_decode(stripslashes(trim($post['localized_versions'])), true);
        foreach($localized_versions as $lang => $data){
            $bks = [];
            $tutor = false;
            $new_config = [];
            foreach($data as $key => $value){
                preg_match('/bank(\d+)/', $key, $match);
                if(!empty($match)){
                    $i = $match[1];
                    if($data['bank'.$i] != '' || $data['ac'.$i] != ''){
                        if ($data['bank'.$i] != $banks[$i - 1]['name']) {
                            $bks[$i - 1] = array('name'=>$data['bank'.$i],'account'=>null);
                        }
                    }
                } elseif ($key == "conf_tutorial") {
                    if ($conf_tutorial != $data[$key]) {
                        $tutor = empty($data[$key]) ? true : $data[$key];
                    }
                }
            }
            if(sizeof($bks) > 0){
                $new_config[] = array('name'=>'banks','type'=>'hidden','value'=>$bks);
            }
            if(!empty($tutor)){
                $new_config[] = array('name'=>'tutorial','type'=>'hidden','value'=>($tutor === true ? '' : $tutor));
            }
            if (!empty($new_config)) {
                $localized_versions[$lang]['pay_config']=serialize($new_config);
            } else {
                $localized_versions[$lang]['pay_config']=null;
            }
        }

        $localizable_config = [];
        $localizable_fields = [
            "pay_name"      => $pay_name,
            "pay_desc"      => $pay_desc,
            "display_name"  => $display_name,
        ];
        $post['localized_versions'] = $localized_versions;
        if(sizeof($banks) > 0) {
            array_push($pay_config,array('name'=>'banks','type'=>'hidden','value'=>$banks));
            $tmp_banks = $banks;
            foreach ($tmp_banks as $key => $bank) {
                $tmp_banks[$key]['account'] = null;
            }
            array_push($localizable_config,array('name'=>'banks','type'=>'hidden','value'=>$tmp_banks));
        }
        if (isset($post['conf_tutorial'])) {
            array_push($pay_config,array('name'=>'tutorial','type'=>'hidden','value'=>$conf_tutorial));
            array_push($localizable_config,array('name'=>'tutorial','type'=>'hidden','value'=>$conf_tutorial));
        }
        if (!empty($localizable_config)) {
            $localizable_fields['pay_config'] = serialize($localizable_config);
        }

        $pay_config = serialize($pay_config);
        $new_data = array(
            'pay_name'     => $pay_name,
            'pay_desc'     => $pay_desc,
            'pay_config'   => $pay_config,
            'pay_fee'      => $pay_fee,
            'is_pos'       => $is_pos,
            'display_name' => $display_name,
            'platform'     => $platform,
            'is_debug_mode'=> $is_debug_mode,
        );
        if(isset($payment_logo)) $new_data['payment_logo'] = $payment_logo;
        /* 检查是编辑还是安装 */
        
        if ($action == 'edit')
        {
            /* 编辑 */
            $payment = new Model\Payment($pay_code);
            if($payment){

                $res = $payment->update($new_data);
                if(!$res){
                    return false;
                }
                // Multiple language support
                $pay_id = $payment->getId();
                $localizable_fields = $this->checkRequireSimplied($this->tablename, $localizable_fields, $pay_id);
                to_simplified_chinese($localizable_fields, $post['localized_versions']);
                save_localized_versions($this->tablename, $pay_id);
                
                /* payment rule */
                $pay_rule = array();
                foreach ($post['parent_id'] as $key => $parent_id)
                {
                    $parent = array(
                        'rule_type'       => $post['parent_type'][$key],
                        'rule_equivalent' => $post['parent_equiv'][$key],
                        'rule_value'      => empty($post['parent_value'][$key]) ? 0 : $post['parent_value'][$key],
                        'pay_rule_id'     => empty($parent_id) ? 0 : $parent_id
                    );
                    
                    $sub = array();
                    $sub[] = array(
                        'rule_type'       => $post['child_0_type'][$key],
                        'rule_equivalent' => $post['child_0_equiv'][$key],
                        'rule_value'      => empty($post['child_0_value'][$key]) ? '0' : $post['child_0_value'][$key],
                        'pay_rule_id'     => empty($post['child_0_rule_id'][$key]) ? 0 : $post['child_0_rule_id'][$key]
                    );
                    $sub[] = array(
                        'rule_type'       => $post['child_1_type'][$key],
                        'rule_equivalent' => $post['child_1_equiv'][$key],
                        'rule_value'      => empty($post['child_1_value'][$key]) ? '0' : $post['child_1_value'][$key],
                        'pay_rule_id'     => empty($post['child_1_rule_id'][$key]) ? 0 : $post['child_1_rule_id'][$key]
                    );
                    $remove_sub_ids = $post['removeRuleId'];
                    $this->setup_payment_rules($parent, $sub, $pay_id, $remove_sub_ids);
                    $post['removeRuleId'] = array();
                }
            }
            
            // Remove payment method
            $this->remove_payment_methods($post['removeId']);
            
            // Add payment method id
            $this->add_payment_methods($post['account_id'], $post['type_id'], $pay_code);
        }
        elseif ($action == 'install')
        {
            /* 安装，检查该支付方式是否曾经安装过 */
            $payment = new Model\Payment($pay_code);
            if($payment->getId()){
                
                /* 该支付方式已经安装过, 将该支付方式的状态设置为 enable */
                $payment->enablePayment();
                
                $res = $payment->update($new_data);
                if(!$res){
                    return false;
                }
                // Multiple language support
                $pay_id = $payment->getId();
                $localizable_fields = $this->checkRequireSimplied($this->tablename, $localizable_fields, $pay_id);
                to_simplified_chinese($localizable_fields, $post['localized_versions']);
                save_localized_versions($this->tablename, $pay_id);
                
                /* payment rule */
                $pay_rule = array();
                foreach ($post['parent_id'] as $key => $parent_id)
                {
                    $parent = array(
                        'rule_type'       => $post['parent_type'][$key],
                        'rule_equivalent' => $post['parent_equiv'][$key],
                        'rule_value'      => $post['parent_value'][$key],
                        'pay_rule_id'     => empty($parent_id) ? 0 : $parent_id
                    );
                    
                    $sub = array();
                    $sub[] = array(
                        'rule_type'       => $post['child_0_type'][$key],
                        'rule_equivalent' => $post['child_0_equiv'][$key],
                        'rule_value'      => $post['child_0_value'][$key],
                        'pay_rule_id'     => empty($post['child_0_rule_id'][$key]) ? 0 : $post['child_0_rule_id'][$key]
                    );
                    $sub[] = array(
                        'rule_type'       => $post['child_1_type'][$key],
                        'rule_equivalent' => $post['child_1_equiv'][$key],
                        'rule_value'      => $post['child_1_value'][$key],
                        'pay_rule_id'     => empty($post['child_1_rule_id'][$key]) ? 0 : $post['child_1_rule_id'][$key]
                    );
                    $remove_sub_ids = $post['removeRuleId'];
                    $this->setup_payment_rules($parent, $sub, $pay_id, $remove_sub_ids);
                    $post['removeRuleId'] = array();
                }
            }
            else
            {
                $new_data['pay_code'] = $pay_code;
                $new_data['is_cod'] = $is_cod;
                $new_data['is_online'] = $is_online;
                $new_data['is_online_pay'] = $is_online_pay;
                $new_data['is_debug_mode'] = $is_debug_mode;
                
                $payment = new Model\Payment(null, $new_data);
                $payment = new Model\Payment($pay_code);
                $payment->enablePayment();
                if($payment->getId())
                {
                    $pay_id = $payment->getId();
                    to_simplified_chinese($localizable_fields, $post['localized_versions']);
                    // Multiple language support
                    save_localized_versions($this->tablename, $pay_id);
                    
                    /* payment rule */
                    $pay_rule = array();
                    foreach ($post['parent_id'] as $key => $parent_id)
                    {
                        $parent = array(
                            'rule_type'       => $post['parent_type'][$key],
                            'rule_equivalent' => $post['parent_equiv'][$key],
                            'rule_value'      => empty($post['parent_value'][$key]) ? 0 : $post['parent_value'][$key],
                            'pay_rule_id'     => empty($parent_id) ? 0 : $parent_id
                        );
                        
                        $sub = array();
                        $sub[] = array(
                            'rule_type'       => $post['child_0_type'][$key],
                            'rule_equivalent' => $post['child_0_equiv'][$key],
                            'rule_value'      => empty($post['child_0_value'][$key]) ? '0' : $post['child_0_value'][$key],
                            'pay_rule_id'     => empty($post['child_0_rule_id'][$key]) ? 0 : $post['child_0_rule_id'][$key]
                        );
                        $sub[] = array(
                            'rule_type'       => $post['child_1_type'][$key],
                            'rule_equivalent' => $post['child_1_equiv'][$key],
                            'rule_value'      => empty($post['child_1_value'][$key]) ? '0' : $post['child_1_value'][$key],
                            'pay_rule_id'     => empty($post['child_1_rule_id'][$key]) ? 0 : $post['child_1_rule_id'][$key]
                        );
                        $remove_sub_ids = $post['removeRuleId'];
                        $this->setup_payment_rules($parent, $sub, $pay_id, $remove_sub_ids);
                        $post['removeRuleId'] = array();
                    }
                }
            }
            
            // Add payment method id
            $this->add_payment_methods($post['account_id'], $post['type_id'], $pay_code);
            
        }
        clear_cache_files();
        return true;
    }
    
    /* Check payment name is repeat */
    public function check_payment_name_repeat($pay_code, $pay_name)
    {
        $pay_code = isset($pay_code) ? $pay_code : '';
        $pay_name = isset($pay_name) ? $pay_name : '';
        
        $sql = "SELECT COUNT(*) FROM " . $this->ecs->table($this->tablename) .
        " WHERE pay_name = '$pay_name' AND pay_code <> '$pay_code'";
        if ($this->db->GetOne($sql) > 0 )
        {
            return true;
        }else{
            return false;
        }
    }
    
    /* Unactive payment method */
    public function remove_payment_methods($remove_ids)
    {
        if($remove_ids && is_array($remove_ids))
        {
            // Unactive payment mothods
            foreach ($remove_ids as $key => $payment_method_id) {
                $this->actg->unactiveAccountPaymentMethod($payment_method_id);
            }
        } else {
            return false;
        }
    }
    
    /* Add payment method */
    public function add_payment_methods($account_ids, $type_ids, $pay_code)
    {
        if($account_ids && is_array($account_ids))
        {
            foreach ($account_ids as $key => $account_id) {
                
                $type_id = isset($type_ids[$key]) ? $type_ids[$key] : 0;
                
                $account_payment_method = $this->actg->getAccountPaymentMethod(array(
                    'pay_code' => $pay_code,
                    'account_id' => $account_id)
                );
                
                // If non-selected value
                if($account_id == 0 || $type_id == 0)
                {
                    continue;
                }
                // If exist
                if($account_payment_method)
                {
                    // Update type
                    $this->actg->activeAccountPaymentMethod($account_payment_method['payment_method_id'], $type_id);
                }
                //Add new payment method
                else
                {
                    $this->actg->addAccountPaymentMethod(array(
                        'account_id'      => $account_id,
                        'pay_code'        => $pay_code,
                        'payment_type_id' => $type_id
                    ));
                }
            }
            
        } else {
            return false;
        }
    }
    
    /* Unactive payment */
    public function disable_payment($pay_code)
    {
        $payment = new Model\Payment($pay_code);
        
        if($payment->disablePayment())
        {
            // Unactive payment methods
            $account_payment_methods = $this->actg->getAccountPaymentMethods(array('pay_code' => $_REQUEST[code], 'active'=> 1));
            foreach($account_payment_methods as $key => $method){
                $this->actg->unactiveAccountPaymentMethod($method['payment_method_id']);
            }
            
            $sql = "DELETE p, c FROM ".$this->ecs->table('payment_rule')." as p ".
            "LEFT JOIN ".$this->ecs->table('payment_rule')." as c ON c.parent_id = p.pay_rule_id ".
            " WHERE p.pay_id = ".$payment->getId();
            
            return $this->db->query($sql);
        } else {
            return false;
        }
    }
    
    /* AJAX: update payment info with mulitiple language function */
    public function ajax_mulit_lang_info($value, $pay_code, $col_name, $edit_lang)
    {
        global $_LANG;
        
        /* 检查名称是否为空 */
        if (empty($value))
        {
            make_json_error($_LANG['name_is_null']);
        }
        
        /* 检查名称是否重复 */
        if ($col_name == 'pay_name' && $this->check_payment_name_repeat($pay_code, $value))
        {
            make_json_error($_LANG['name_exists']);
        }
        
        // Multiple language support
        
        if ($edit_lang && !in_array($edit_lang, available_languages()))
        {
            make_json_error('Invalid language');
        }
        $payment = new Model\Payment($pay_code);
        /* 更新支付方式名称 */
        $pay_id = $payment->getId();
        return localized_update($this->tablename, $pay_id, array($col_name => $value), $edit_lang);
    }
    
    /* Get pay fee */
    public function pay_fee($pay_fee)
    {
        if (empty($pay_fee))
        {
            $pay_fee = 0;
        } else {
            $pay_fee = make_semiangle($pay_fee); //全角转半角
            if (strpos($pay_fee, '%') === false)
            {
                $pay_fee = floatval($pay_fee);
            }
            else
            {
                $pay_fee = floatval($pay_fee) . '%';
            }
        }
        
        return $pay_fee;
    }
    
    /* Get payment rules */
    public function get_payment_rules($pay_id)
    {
        if(!isset($pay_id)) return false;
        $sql = "SELECT * FROM ".$this->ecs->table('payment_rule')." WHERE pay_id = ".$pay_id;
        return $this->db->getAll($sql);
    }
    
    /* Add/edit payment rules */
    public function setup_payment_rules($parent, $sub, $pay_id, $remove_sub_ids = null)
    {
        if(!is_array($parent) && !is_array($sub) && empty($pay_id))
        return false;
        
        /* Create or update parent rule */
        // Set rule info
        $type  = $parent['rule_type'];
        $equiv = $parent['rule_equivalent'];
        $value = $parent['rule_value'];
        $id    = $parent['pay_rule_id'];
        
        if($id > 0 && in_array($type, self::PAYMENT_RULE_TYPE) ){ //Have id
            
            $sql = "UPDATE ".$this->ecs->table('payment_rule').
            " SET `rule_type` = '$type', `rule_value` = '$value', `rule_equivalent` = '$equiv', `pay_id` = $pay_id ".
            " WHERE `pay_rule_id` = ".$id;
            if(!$this->db->query($sql)) return false;
        } else if($id == 0 && in_array($type, self::PAYMENT_RULE_TYPE)  ){ // new rules
            $sql = "INSERT INTO ".$this->ecs->table('payment_rule').
            " (`rule_type`, `rule_value`, `pay_id`, `rule_equivalent`) VALUE ('$type', '$value', $pay_id, '$equiv') ";
            if(!$this->db->query($sql)) return false;
        }
        
        $parent_id = empty($this->db->insert_id()) ? $parent['pay_rule_id'] : $this->db->insert_id();
        
        // Create or update child rule
        foreach ($sub as $key => $sub_rule) {
            // Set rule info
            $value = $sub_rule['rule_value'];
            $type  = $sub_rule['rule_type'];
            $equiv = $sub_rule['rule_equivalent'];
            $id    = $sub_rule['pay_rule_id'];
            // if(empty($value)) continue;
            if($id > 0 && in_array($type, self::PAYMENT_RULE_TYPE) ){ //Have id
                $sql = "UPDATE ".$this->ecs->table('payment_rule').
                " SET `rule_type` = '$type', `rule_value` = '$value', `rule_equivalent` = '$equiv', `pay_id` = $pay_id  ".
                " WHERE `pay_rule_id` = ".$id." AND `parent_id` = $parent_id";
                if(!$this->db->query($sql)) return false;
            } else if($id == 0 && in_array($type, self::PAYMENT_RULE_TYPE)  ){ // new rules
                $sql = "INSERT INTO ".$this->ecs->table('payment_rule').
                " (`rule_type`, `rule_value`, `pay_id`, `rule_equivalent`, `parent_id`) VALUE ('$type', '$value', $pay_id, '$equiv', $parent_id) ";
                if(!$this->db->query($sql)) return false;
            } else {
                continue;
            }
            
        }
        
        if(isset($remove_sub_ids) && is_array($remove_sub_ids)){
            $this->remove_rule($remove_sub_ids);
        }
        clear_cache_files();
        return true;
        
    }
    
    public function get_type_array()
    {
        
        global $_LANG;
        
        $arr = array();
        foreach (self::PAYMENT_RULE_TYPE as $value) {
            $arr[] = array(
                'name' => empty($_LANG['type_'.$value]) ? strtoupper($value) : $_LANG['type_'.$value],
                'type' => $value
            );
        }
        
        return $arr;
    }
    
    public function get_equiv_array()
    {
        
        global $_LANG;
        
        $arr = array();
        foreach (self::PAYMENT_RULE_EQUIVALENT as $value) {
            $arr[] = array(
                'name' => empty($_LANG[$value]) ? strtoupper($value) : $_LANG[$value],
                'equiv' => $value
            );
        }
        
        return $arr;
    }
    
    /* Get available payment list */
    public function available_payment_list($support_cod, $cod_fee = 0, $is_online = false, $filter = array())
    {
        
        $sql = 'SELECT pay_id, pay_code, pay_name, display_name, pay_fee, pay_desc, pay_config, is_cod, is_online, is_pos, platform, payment_logo, is_debug_mode ' .
        ' FROM ' . $this->ecs->table($this->tablename) .
        ' WHERE enabled = 1 ';
        if (!$support_cod)
        {
            $sql .= 'AND is_cod = 0 '; // 如果不支持货到付款
        }
        if ($is_online)
        {
            $sql .= "AND is_online = '1' ";
        }
        if (isset($filter['is_online_pay']) && $filter['is_online_pay'] == true)
        {
            $sql .= "AND is_online_pay = '1' ";
        }
        $sql .= 'ORDER BY pay_order'; // 排序
        $res = $this->db->getAll($sql);
        $res = localize_db_result('payment', $res);
        
        $pay_list = array();
        foreach ($res as $row)
        {
            if ($row['is_cod'] == '1')
            {
                $row['pay_fee'] = $cod_fee;
            }
            
            $row['format_pay_fee'] = strpos($row['pay_fee'], '%') !== false ? $row['pay_fee'] : price_format($row['pay_fee'], false);
            if ($row['pay_fee'] < 0)
            {
                $pay_discount = substr($row['pay_fee'],1); // remove leading - sign
                $row['format_pay_discount'] = strpos($pay_discount, '%') !== false ? $pay_discount : price_format($pay_discount, false);
            }
            $row['origin_pay_name'] = $row['pay_name'];
            $row['pay_name'] = empty($row['display_name']) ? $row['pay_name'] : $row['display_name'];
            
            // // 给货到付款的手续费加<span id>，以便改变配送的时候动态显示
            if ($row['is_cod'] == '1')
            {
                $row['format_pay_fee'] = '<span id="ECS_CODFEE">' . $row['format_pay_fee'] . '</span>';
            }

            if (!empty($row['payment_logo'])) {
                $path = self::LOGO_PATH . $row['payment_logo'];
                $extname = strtolower(substr($row['payment_logo'], strrpos($row['payment_logo'], '.') + 1));
                if (in_array($extname, ['svg', 'SVG'])) {
                    $row['payment_logo_svg'] = $this->svgIconContent($path, $row['pay_code']);
                }
                $row['payment_logo'] = $path;
            } else {
                $row['payment_logo'] = null;
            }

            $modules[] = $row;
        }
        
        if(isset($modules))
        {
            /* Filter payment */
            $modules = $this->filter_payment_list($modules, $filter);
            return $modules;
        }
    }
    
    /* Filter payment modules */
    public function filter_payment_list($payment_list, $filter = array())
    {
        global $db,$_CFG;
        // Filter data
        $country   = isset($filter['country']) ? intval($filter['country']) : 0; // Default 香港
        $price     = isset($filter['goods_amount']) ? floatval($filter['goods_amount']) : 0;
        $rank      = isset($filter['rank_points']) ? intval($filter['rank_points']) : 0; // Default 0
        $step      = empty($filter['step']) ? '' : $filter['step'];
        $is_pos    = isset($filter['is_pos']) ? $filter['is_pos'] : false;
        $is_ex_pos = isset($filter['is_ex_pos']) ? $filter['is_ex_pos'] : false;
        $is_cms    = isset($filter['is_cms']) ? $filter['is_cms'] : false;
        $platform  = $filter['current_platform'];
        $show_all  = isset($filter['show_all']) ? $filter['show_all'] : false;

        // Handle platform
        if($filter['platform']){
            switch ($filter['platform']) {
                case 'pos':
                    $is_pos    = isset($filter['is_pos']) ? $filter['is_pos'] : true;
                    break;
                case 'pos_exhibition':
                    $is_ex_pos = isset($filter['is_ex_pos']) ? $filter['is_ex_pos'] : true;
                    break;
                default:
                    break;
            }
        }
        
        if($show_all) return $payment_list;
        
        if($is_pos || $is_ex_pos)
        {
            $payment_list_output = array();
            foreach ($payment_list as $payment)
            {
                if (!$payment['is_pos'] && !$is_ex_pos) continue; // Skip non-POS payment method
                if ($is_ex_pos && !in_array($payment['pay_code'],self::EXHIBITION_POS))continue; // TODO: Hardcode skip non-exhibition POS payment method
                $payment_list_output[] = array(
                    'pay_id' => $payment['pay_id'],
                    'pay_code' => $payment['pay_code'],
                    'pay_name' => $payment['origin_pay_name'],
                    'origin_pay_name' => $payment['origin_pay_name'],
                    'pay_desc' => $payment['pay_desc'],
                    'pay_fee' => $payment['pay_fee']
                );
            }
            
            return $payment_list_output;
        }
        
        $new_payment_list = array();
        foreach ($payment_list as $key => $payment)
        {
            $pay_id = $payment['pay_id'];
            
            /* 過濾當前支付方式及余額支付方式  */
            if (!$is_cms && isset($filter['pay_id']) && ($payment['pay_id'] == $filter['pay_id'] || $payment['pay_code'] == 'balance'))
            {
                unset($payment_list[$key]);
                continue;
            }

            /* Step: checkout, 檢查登入, 未登入不顯示余額支付 */
            if($step == 'checkout' && $payment['pay_code'] == 'balance')
            {
                if ($_SESSION['user_id'] == 0)
                {
                    unset($payment_list[$key]);
                    continue;
                }
                // else
                // {
                //     if ($_SESSION['flow_order']['pay_id'] == $payment['pay_id'])
                //     {
                //         $GLOBALS['smarty']->assign('disable_surplus', 1);
                //     }
                // }
            }
            
            /* Check platform, if current is not match unset. */
            if(isset($platform))
            {
                $display = false;
                $pay_platform = explode(',', $payment['platform']);
                foreach ($pay_platform as $value) {
                    if($platform == $value) $display = true;
                }
                
                if(!$display){
                    unset($payment_list[$key]);
                    continue;
                }
            }
            
            /* Get payment rule list */
            $rules = $this->get_rule_list($pay_id);
            
            // Default: Not show payment
            $is_show = false;
            foreach ($rules as $rule_key => $rule) {
                
                /* Step 1: Check parent payment */
                $type  = $rule['rule_type'];
                $eqiv  = $rule['rule_equivalent'];
                $value = $rule['rule_value'];
                //['country', 'price', 'rank'];
                switch($type){
                    case 'country':
                    $is_show = $this->rule_check($eqiv, $value, $country);
                    break;
                    case 'price':
                    $is_show = $this->rule_check($eqiv, $value, $price);
                    break;
                    case 'rank':
                    $is_show = $this->rule_check($eqiv, $value, $rank);
                    break;
                }
                
                /* Step 2: If parent rule is true, check child rule */
                if($is_show && isset($rule['child']))
                {
                    foreach ($rule['child'] as $child) {
                        
                        if(!in_array($child['rule_type'], self::PAYMENT_RULE_TYPE)) continue;
                        
                        $type  = $child['rule_type'];
                        $eqiv  = $child['rule_equivalent'];
                        $value = $child['rule_value'];
                        //['country', 'price', 'rank'];
                        switch($type){
                            case 'country':
                            $is_show = $this->rule_check($eqiv, $value, $country);
                            break;
                            case 'price':
                            $is_show = $this->rule_check($eqiv, $value, $price);
                            break;
                            case 'rank':
                            $is_show = $this->rule_check($eqiv, $value, $rank);
                            break;
                        }
                        /* If have any 1 child rule is false, end child rule loop */
                        if(!$is_show) break;
                    }
                }
                /* If have any 1 rule can show, end rule */
                if($is_show) break;
            }

            if ($payment['is_debug_mode'] == 1){
                $orderController = new OrderController();
                $is_testing = $orderController->is_testing();
                
                if (!$is_testing) {
                    unset($payment_list[$key]);
                    continue;
                }
            }

            /* Step 3: If parent and child rules is pass, display payment and set multi-currency */
            if($is_show){
                // Multi-currency support
                $payment_list[$key]['currency'] = $this->actgHooks->getCurrencyForPaymentModule($payment_list[$key]['pay_code']);
                $new_payment_list[] = $payment_list[$key];
            }

            // if payment is a debug mode, only testing users can see it.
            //dd($payment);


        }
        
        /* Step 4: Return payment list */
        return $new_payment_list;
    }
    
    /* Check rule with using equiv */
    public function rule_check($eqiv, $value, $origin_value)
    {
        $result = false;
        //['eq', 'neq', 'gt', 'lt', 'gte', 'lte'];  ==, !=, >, <, >=, <=
        switch ($eqiv) {
            case 'eq':
            if ($origin_value == $value) $result = true;
            break;
            case 'neq':
            if($origin_value != $value) $result = true;
            break;
            case 'gt':
            if($origin_value > $value) $result = true;
            break;
            case 'lt':
            if($origin_value < $value) $result = true;
            break;
            case 'gte':
            if($origin_value >= $value) $result = true;
            break;
            case 'lte':
            if($origin_value <= $value) $result = true;
            break;
            default:
            $result = false;
            break;
        }
        
        return $result;
    }
    
    /* Get rule list */
    public function get_rule_list($id = null)
    {
        global $_LANG;
        
        $p_sql = "SELECT r.*, p.pay_name, IF(r.rule_type ='country', IFNULL(c.region_name, r.rule_value), r.rule_value) as value FROM ".$this->ecs->table('payment_rule')." as r".
        " LEFT JOIN ".$this->ecs->table($this->tablename). " as p ON r.pay_id = p.pay_id ".
        " LEFT JOIN ".$this->ecs->table('region'). " as c ON c.region_id = r.rule_value ".
        " WHERE 1 ";
        if(isset($id)){
            $p_sql .=" AND r.pay_id = ".$id;
        }
        $p_sql .=" ORDER BY r.pay_id ASC, r.pay_rule_id ASC";
        
        $parent_rules = $this->db->getAll($p_sql);
        $child_rules = array();
        foreach ($parent_rules as $p_key => $p_rule) {
            if($p_rule['parent_id'] != 0) {
                $child_rules[] = $p_rule;
                unset($parent_rules[$p_key]);
            }
        }
        /* Get child */
//        $c_sql = "SELECT r.*, p.pay_name, IF(r.rule_type ='country', IFNULL(c.region_name, r.rule_value), r.rule_value) as value FROM ".$this->ecs->table('payment_rule')." as r".
//        " LEFT JOIN ".$this->ecs->table($this->tablename). " as p ON r.pay_id = p.pay_id ".
//        " LEFT JOIN ".$this->ecs->table('region'). " as c ON c.region_id = r.rule_value ".
//        " WHERE r.parent_id <> 0 ";
//        if(isset($id)){
//            $c_sql .=" AND r.pay_id = ".$id;
//        }
//        $child_rules = array();
//        $child_rules = $this->db->getAll($c_sql);
        
        foreach ($parent_rules as $p_key => $p_rule) {
            foreach ($child_rules as $c_key => $c_rule) {
                
                $c_rule['format_rule_type'] = empty($_LANG['type_'.$c_rule['rule_type']]) ? strtoupper($c_rule['rule_type']) : $_LANG['type_'.$c_rule['rule_type']];
                $c_rule['format_rule_equivalent'] = empty($_LANG[$c_rule['rule_equivalent']]) ? strtoupper($c_rule['rule_equivalent']) : $_LANG[$c_rule['rule_equivalent']];
                
                $parent_id = $c_rule['parent_id'];
                if($parent_id == $p_rule['pay_rule_id']){
                    $parent_rules[$p_key]['child'][] = $c_rule;
                }
            }
            if(!isset($parent_rules[$p_key]['child']))
            {
                $parent_rules[$p_key]['child'][] = array('rule_type' => '0');
                $parent_rules[$p_key]['child'][] = array('rule_type' => '0');
            }
            else if(count($parent_rules[$p_key]['child']) == 1)
            {
                $parent_rules[$p_key]['child'][] = array('rule_type' => '0');
            }
            $parent_rules[$p_key]['format_rule_type'] = empty($_LANG['type_'.$p_rule['rule_type']]) ? strtoupper($p_rule['rule_type']) : $_LANG['type_'.$p_rule['rule_type']];
            $parent_rules[$p_key]['format_rule_equivalent'] = empty($_LANG[$p_rule['rule_equivalent']]) ? strtoupper($p_rule['rule_equivalent']) : $_LANG[$p_rule['rule_equivalent']];
        }
        
        return $parent_rules;
    }
    
    /* Return edit_rule page data */
    public function rule_page_data($id)
    {
        $data = array();
        
        /* Have Id : Get rule */
        if($id)
        {
            $sql = "SELECT * FROM ".$this->ecs->table('payment_rule').
            " WHERE pay_rule_id = ".$id;
            $rule = $this->db->getRow($sql);
            $data['rule'] = $rule;
            
            /* Get child */
            $sql = "SELECT * FROM ".$this->ecs->table('payment_rule').
            " WHERE parent_id = ".$id;
            $child = $this->db->getAll($sql);
            $data['child'] = $child;
        }
        
        /* Get payment array */
        $sql = "SELECT pay_id, pay_name FROM " . $this->ecs->table('payment') . " WHERE enabled = '1' AND is_online = '1' ORDER BY pay_order";
        $pay_list = $this->db->getAll($sql);
        $data['pay_list'] = $pay_list;
        
        /* Get equiv list */
        $equiv_list = $this->get_equiv_array();
        $data['equiv_list'] = $equiv_list;
        
        /* Get rule type list */
        $types = $this->get_type_array();
        $data['types'] = $types;
        
        return $data;
    }
    
    /* Submit edit_rule function */
    public function submit_rule_action($post)
    {
        global $_LANG;
        
        $pay_id = $post['pay_id'];
        
        $parent = array(
            'rule_type'       => $post['parent_type'],
            'rule_equivalent' => $post['parent_equiv'],
            'rule_value'      => empty($post['parent_value']) ? 0 : $post['parent_value'],
            'pay_rule_id'     => empty($post['pay_rule_id']) ? 0 : $post['pay_rule_id']
        );
        
        $sub = array();
        foreach ($post['rule_type'] as $key => $type) {
            $sub[] = array(
                'rule_type'       => $type,
                'rule_equivalent' => $post['rule_equivalent'][$key],
                'rule_value'      => empty($post['rule_value'][$key]) ? '0' : $post['rule_value'][$key],
                'pay_rule_id'     => empty($post['child_id'][$key]) ? 0 : $post['child_id'][$key]
            );
        }
        
        $remove_sub_ids = $post['removeRuleId'];
        
        return $this->setup_payment_rules($parent, $sub, $pay_id, $remove_sub_ids);
    }
    
    /* Remove rules */
    public function remove_rule($ids)
    {
        // If have child rule, remove with child
        $sql = "DELETE p, c FROM ".$this->ecs->table('payment_rule')." as p ".
        "LEFT JOIN ".$this->ecs->table('payment_rule')." as c ON c.parent_id = p.pay_rule_id ".
        " WHERE p.pay_rule_id ".db_create_in($ids);
        
        return $this->db->query($sql);
    }

    public function upload_logo($file, $pay_code)
    {
        global $_LANG;
        require_once(ROOT_PATH . 'includes/cls_image.php');
        $file_url = '';
    	$extname = strtolower(substr($file['payment_logo']['name'], strrpos($file['payment_logo']['name'], '.') + 1));
        if ((isset($file['payment_logo']['error']) && $file['payment_logo']['error'] == 0) || (!isset($file['payment_logo']['error']) && isset($file['payment_logo']['tmp_name']) && $file['payment_logo']['tmp_name'] != 'none'))
        {
            // 检查文件格式
            if (!in_array($extname, ['svg', 'SVG']))
            {
                sys_msg('檔案格式錯誤');
            }
    
            // 复制文件
            if (!make_dir(self::LOGO_PATH))
            {
                /* 创建目录失败 */
                $res =  false;
            }
        
            $filename = $pay_code.'_pay_logo' . substr($file['payment_logo']['name'], strpos($file['payment_logo']['name'], '.'));
            $path     = self::LOGO_PATH . $filename;
            if (move_upload_file($file['payment_logo']['tmp_name'], $path))
            {
                $res =  $filename;
            }
            else
            {
                $res =  false;

            }
            if ($res != false)
            {
                $file_url = $res;
            }
        }
        if ($file_url == '')
        {
            $file_url = $_POST['file_url'];
        }
        return $file_url;
    }

    public function update_transaction_type(){
        $this->actg->updateTransactionType(array('txn_type_id'=>$_REQUEST['type_id'],'txn_type_name'=>$_REQUEST['type_name']));
        foreach($_REQUEST['type_child_id'] as $key => $type_id){
            $child_name = $_REQUEST['type_child'][$key];
            $sql = "INSERT INTO actg_transaction_types (txn_type_id, txn_type_name, txn_type_parent) VALUE ($type_id, '$child_name', $_REQUEST[type_id]) " .
                "ON DUPLICATE KEY UPDATE txn_type_name = '$child_name', txn_type_parent = $_REQUEST[type_id]";
            $this->db->query($sql);
        }
    }
    public function insert_transaction_type(){
        if($_REQUEST['txn_type_id'] == '-1'){
            $sql = "INSERT INTO actg_transaction_types (txn_type_name, txn_type_parent) VALUE ('$_REQUEST[type_name]', NULL)";
            $this->db->query($sql);
            $insert_id = $this->db->insert_id();
        }else{
            $insert_id = $_REQUEST['txn_type_id'];
        }
        if(!$insert_id) return false;

        if(count($_REQUEST['type_child']) > 0){
            foreach($_REQUEST['type_child'] as $type_name){
                if(!$this->actg->addTransactionType(array('txn_type_parent'=>$insert_id,'txn_type_name'=>$type_name))) return false;
            }
        }
        return true;
    }
    public function delete_transaction_type(){
        $this->actg->deleteTransactionType($_REQUEST['txn_type_id']);
        if(isset($_REQUEST['parent'])){
            $children = $this->actg->getTransactionTypes(array('txn_type_parent'=>$_REQUEST['txn_type_id']));
            foreach($children as $child){
                $this->actg->deleteTransactionType($child['txn_type_id']);
            }
        }
    }

    public function getFlowPaymentList($user_id, $data)
    {
        global $ecs, $db;
        $flow_type = isset($_SESSION['flow_type']) ? intval($_SESSION['flow_type']) : CART_GENERAL_GOODS;
        $shipping_area_info = shipping_area_info($data['shipping_id'], [], $data['shipping_area_id']);

        /* 取得支付列表 */
        if ($data['shipping_id'] == 0)
        {
            $cod        = true;
            $cod_fee    = 0;
        }
        else
        {
            $shipping = shipping_info($data['shipping_id']);
            $cod = $shipping['support_cod'];

            if ($cod)
            {
                /* 如果是团购，且保证金大于0，不能使用货到付款 */
                if ($flow_type == CART_GROUP_BUY_GOODS)
                {
                    $group_buy_id = $_SESSION['extension_id'];
                    if ($group_buy_id <= 0)
                    {
                        show_message('error group_buy_id');
                    }
                    $group_buy = group_buy_info($group_buy_id);
                    if (empty($group_buy))
                    {
                        show_message('group buy not exists: ' . $group_buy_id);
                    }

                    if ($group_buy['deposit'] > 0)
                    {
                        $cod = false;
                        $cod_fee = 0;

                        /* 赋值保证金 */
                        $gb_deposit = $group_buy['deposit'];
                    }
                }

                if ($cod)
                {
                    $cod_fee = $shipping_area_info['pay_fee'];
                }
            }
            else
            {
                $cod_fee = 0;
            }
        }

        $user_info = user_info($user_id);
        foreach($shipping_area_info['region_list'] as $region) {
            $sql = "SELECT  CAST(`_id` AS UNSIGNED) AS `region_id` ".
                "FROM (SELECT @r AS `_id`, (SELECT @r := `parent_id`
                    FROM ".$ecs->table('region')."
                WHERE `region_id` = `_id`) AS `parent_id` ".
                " FROM (SELECT  @r := '$region', @l := 0) AS `vars`, ".$ecs->table('region').
                " WHERE @r <> 0 ) AS `q` ".
                " LEFT JOIN ".$ecs->table('region')." AS `r` ".
                " ON `r`.`region_id` = `q`.`_id` ";
            $list = $db->getAll($sql);
            $consignee['country'] = end($list)['region_id'];
        }
        $filter_data = $consignee;
        $online_only = false;
        $cart_goods = cart_goods($flow_type);
        $total = 0;
        foreach ($cart_goods as $cg_k => $cg) {
            // flashdeal goods, only can pay with online payment
            if($cg['flashdeal_id'] > 0) {
                $sql = "SELECT is_online_pay FROM ".$ecs->table('flashdeals').' WHERE flashdeal_id = '.$cg['flashdeal_id'];
                if($db->getOne($sql)) {
                    $online_only = true;
                    break;
                }
            }
            $total+= $cg['subtotal'];
        }
        $filter_data['goods_amount']     = $total;
        $filter_data['rank_points']      = $user_info['rank_points'];
        $filter_data['step']             = 'checkout';
        $filter_data['current_platform'] = (strpos($_SERVER['SERVER_NAME'], 'm.') !== false) ? $this::PLATFORM_MOBILE: $this::PLATFORM_WEB;
        $filter_data['is_online_pay']    = $online_only;

        return $payment_list = $this->available_payment_list(true, $cod_fee, false, $filter_data);
    }

    public function getWebOfflinePaymentMethodList()
    {
        global $db, $ecs;
        return $db->getAll("SELECT * FROM " . $ecs->table("payment") . " WHERE is_online_pay = 0 AND enabled = 1 AND is_online = 1 AND platform != ''");
    }
}
