<?php

/***
* PackageController.php
* by Ken 20190530
*
* Flashdeal controller
***/

namespace Yoho\cms\Controller;
use Yoho\cms\Model;
if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

class PackageController extends YohoBaseController{

    public $tableName = 'ecs_goods_activity';
    //public $admin_priv = 'bonus_manage';


    /**
    * 获取活动列表
    *
    * @access  public
    *
    * @return void
    */
    function get_packagelist(){
        global $db, $ecs, $_CFG;
        
        $package_abnormal_ids = $this->get_abnormal_package_ids();

        $result = get_filter();
        if ($result === false)
        {
            $where = "";

            /* 查询条件 */
            if (isset($_REQUEST['is_ajax']) && $_REQUEST['is_ajax'] == 1)
            {
                $filter['keywords'] = json_str_iconv($filter['keywords']);
            }
            $filter['sort_by']    = empty($_REQUEST['sort_by']) ? 'act_id' : trim($_REQUEST['sort_by']);
            $filter['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);

            $filter['keywords']   = empty($_REQUEST['keywords']) ? '' : trim($_REQUEST['keywords']);
            $where .= (!empty($filter['keywords'])) ? " AND act_name like '%". mysql_like_quote($filter['keywords']) ."%'" : '';

            $filter['is_going']   = empty($_REQUEST['is_going']) ? '' : trim($_REQUEST['is_going']);
            if($filter['is_going']){
                $now = gmtime();
                $where .= " AND start_time <= '$now' AND end_time >= '$now' ";
            }

            $filter['include_goods']   = empty($_REQUEST['include_goods']) ? '' : trim($_REQUEST['include_goods']);
            if($filter['include_goods']){
                if(strpos($filter['include_goods'], ",") > 0){
                    $include_goods = explode(",", $filter['include_goods']);
                    
                    $include_goods_like_str = array_map(function ($goods_name) { return "goods_name like '%".trim(mysql_like_quote($goods_name))."%'"; }, $include_goods);
                    $include_goods_id_str = array_map(function ($goods_id) { return "goods_id = '".trim(mysql_like_quote($goods_id))."'"; }, $include_goods);

                    $sql = "SELECT goods_id ".
                    "FROM " . $GLOBALS['ecs']->table('goods') . 
                    " WHERE 1=1 and (".implode(" OR ", $include_goods_like_str)." OR ".implode(" OR ", $include_goods_id_str).")";
                    
                }
                else{
                    $include_goods_where .= " AND (goods_sn LIKE '%" . mysql_like_quote($filter['include_goods']) . "%' ";
                    $include_goods_where .= " OR goods_name LIKE '%" . mysql_like_quote($filter['include_goods']) . "%' ";
                    $tmp = preg_split('/\s+/', $filter['include_goods']);
                    if (count($tmp) > 1)
                    {
                        $include_goods_where .= " OR ( 1";
                        foreach ($tmp as $kw)
                        {
                            $include_goods_where .= " AND goods_name LIKE '%" . mysql_like_quote($kw) . "%' ";
                        }
                        $include_goods_where .= ") ";
                    }
                    $include_goods_where .= " OR goods_id = '" . mysql_like_quote($filter['include_goods']) . "')";

                    $sql = "SELECT goods_id ".
                    "FROM " . $GLOBALS['ecs']->table('goods') . 
                    " WHERE 1=1 ".$include_goods_where;
                }

                $related_goods = $GLOBALS['db']->getAll($sql);
                $related_goods_id = array_map(function ($goods) { return $goods['goods_id']; }, $related_goods);

                $where .= " AND act_id in (SELECT DISTINCT package_id FROM " . $ecs->table('package_goods') . " WHERE goods_id " . db_create_in($related_goods_id).")";
            }

            $filter['price_abnormal']   = empty($_REQUEST['price_abnormal']) ? '' : trim($_REQUEST['price_abnormal']);
            if ($filter['price_abnormal'])
            {
                $where .= " AND act_id " . db_create_in($package_abnormal_ids);
            }

            $sql = "SELECT COUNT(*) FROM " . $ecs->table('goods_activity') .
                   " WHERE act_type =" . GAT_PACKAGE . $where;
            $filter['record_count'] = $db->getOne($sql);

            $filter = page_and_size($filter);

            /* 获活动数据 */
            $sql = "SELECT act_id, act_name AS package_name, act_tag AS package_tag, start_time, end_time, status, is_finished, ext_info ".
                   " FROM " . $ecs->table('goods_activity') .
                   " WHERE act_type = " . GAT_PACKAGE . $where .
                   " ORDER by $filter[sort_by] $filter[sort_order] LIMIT ". $filter['start'] .", " . $filter['page_size'];
            
            //die($sql);
            $filter['keywords'] = stripslashes($filter['keywords']);
            set_filter($filter, $sql);
        }
        else
        {
            $sql    = $result['sql'];
            $filter = $result['filter'];
        }

        $row = $db->getAll($sql);

        foreach ($row AS $key => $val)
        {
            $row[$key]['start_time'] = local_date($_CFG['time_format'], $val['start_time']);
            $row[$key]['end_time']   = local_date($_CFG['time_format'], $val['end_time']);
            $info = unserialize($row[$key]['ext_info']);
            unset($row[$key]['ext_info']);
            if ($info)
            {
                foreach ($info as $info_key => $info_val)
                {
                    $row[$key][$info_key] = $info_val;
                }
            }

            $now = date('Y-m-d H:i');
            if(in_array($row[$key]['act_id'], $package_abnormal_ids)){
                $row[$key]['price_abnormal'] = true;
            }
            if($row[$key]['start_time'] > $now || $row[$key]['end_time'] < $now){
                $row[$key]['is_not_on_going'] = true;
            }
        }

        //$arr = array('packages' => $row, 'filter' => $filter, 'page_count' => $filter['page_count'], 'record_count' => $filter['record_count']);
        //return $arr;

        return $row;

    }

    function get_abnormal_package_ids(){
        global $ecs, $db;

        $sql = "SELECT DISTINCT package_id FROM " . $ecs->table('package_goods') . " pg left join " . $ecs->table('goods') . " g on pg.goods_id = g.goods_id ".
                " WHERE pg.goods_price > g.shop_price";
        $package_abnormal = $db->getAll($sql);
        $package_abnormal_ids = array_map(function ($package) { return $package['package_id']; }, $package_abnormal);

        return $package_abnormal_ids;
    }

    function get_basic_package_info($id) {
        global $ecs, $db, $_CFG;
        $sql = "SELECT act_id as id, act_name AS package_name, act_tag AS package_tag, act_desc as package_desc FROM " . $ecs->table('goods_activity') .
                "WHERE act_id = '$id' AND act_type = " . GAT_PACKAGE;
        $package = $db->getRow($sql);
        $package = localize_db_result('goods_activity', [$package], ['id' => 'act_id', 'package_name' => 'act_name', 'package_tag' => 'act_tag', 'package_desc' => 'act_desc'])[0];
        $sql = "SELECT g.goods_id, g.goods_sn, g.goods_name, g.goods_thumb, pg.goods_number, pg.goods_price FROM " . $ecs->table('package_goods') . " pg " .
                "LEFT JOIN " . $ecs->table('goods') . " g ON g.goods_id = pg.goods_id " .
                "WHERE package_id = '$id'";
        $package_goods = $db->getAll($sql);
        $package_goods = localize_db_result('goods', $package_goods);
        $total_price = 0.00;
        $items = [];
        $images = [];
        $link = "";
        foreach ($package_goods as $goods) {
            $total_price = bcadd($total_price, bcmul(floatval($goods['goods_price']), intval($goods['goods_number'])), 2);
            $goods['goods_thumb'] = get_image_path($goods['goods_id'], $goods['goods_thumb'], true);
            $goods['url'] = build_uri('goods', array('gid' => $goods['goods_id'], 'gname' => $goods['goods_name']), $goods['goods_name']);
            if (!in_array($goods['goods_thumb'], $images)) {
                $images[] = $goods['goods_thumb'];
            }
            if (empty($link))  {
                $link = $goods['url'];
            }
            $items[$goods['goods_id']] = $goods;
        }

        $package['rowspan'] = count($package_goods) + 1;
        $package['link'] = $link;
        $package['images'] = $images;
        $package['items'] = $items;
        $package['base_price'] = $total_price;
        $package['formated_base_price'] = price_format($package['base_price'], false);

        return $package;
    }

    /**
     * 获取指定id package 的信息
     *
     * @access  public
     * @param   int         $id         package_id
     *
     * @return array       array(package_id, package_name, goods_id,start_time, end_time, min_price, integral)
     */
    function get_package_info($id){
        global $ecs, $db, $_CFG;

        $id = is_numeric($id)?intval($id):0;
        $now = gmtime();

        $sql = "SELECT act_id AS id,  act_name AS package_name, act_tag AS package_tag, goods_id , goods_name, start_time, end_time, status, act_desc, ext_info".
               " FROM " . $ecs->table('goods_activity') .
               " WHERE act_id='$id' AND act_type = " . GAT_PACKAGE;

        $package = $db->GetRow($sql);
        /* 将时间转成可阅读格式 */
        if ($package['start_time'] <= $now && $package['end_time'] >= $now)
        {
            $package['is_on_sale'] = "1";
        }
        else
        {
            $package['is_on_sale'] = "0";
        }
        $package['start_time'] = local_date('Y-m-d H:i', $package['start_time']);
        $package['end_time']   = local_date('Y-m-d H:i', $package['end_time']);
        $row = unserialize($package['ext_info']);
        unset($package['ext_info']);
        if ($row)
        {
            foreach ($row as $key=>$val)
            {
                $package[$key] = $val;
            }
        }

        $sql = "SELECT pg.package_id, pg.goods_id, pg.goods_number, pg.goods_price, pg.admin_id, ".
               " g.goods_sn, g.goods_name, g.market_price, g.goods_thumb, g.is_real, ".
               " IFNULL(mp.user_price, g.shop_price * '$_SESSION[discount]') AS rank_price " .
               " FROM " . $ecs->table('package_goods') . " AS pg ".
               "   LEFT JOIN ". $ecs->table('goods') . " AS g ".
               "   ON g.goods_id = pg.goods_id ".
               " LEFT JOIN " . $ecs->table('member_price') . " AS mp ".
                    "ON mp.goods_id = g.goods_id AND mp.user_rank = '$_SESSION[user_rank]' ".
               " WHERE pg.package_id = " . $id. " ".
               " ORDER BY pg.package_id, pg.goods_id";

        $goods_res = $db->getAll($sql);

        $market_price        = 0;
        $real_goods_count    = 0;
        $virtual_goods_count = 0;

        foreach($goods_res as $key => $val)
        {
            $final_price_info = get_final_price($val['goods_id'], 1, true);
            $final_price = $final_price_info['final_price'];

            $goods_res[$key]['goods_thumb']         = get_image_path($val['goods_id'], $val['goods_thumb'], true);
            $goods_res[$key]['market_price_format'] = price_format($val['market_price']);
            $goods_res[$key]['final_price_format']   = price_format($final_price);
            $market_price += $val['market_price'] * $val['goods_number'];
            /* 统计实体商品和虚拟商品的个数 */
            if ($val['is_real'])
            {
                $real_goods_count++;
            }
            else
            {
                $virtual_goods_count++;
            }
        }

        if ($real_goods_count > 0)
        {
            $package['is_real']            = 1;
        }
        else
        {
            $package['is_real']            = 0;
        }

        $package['goods_list']            = $goods_res;
        $package['market_package']        = $market_price;
        $package['market_package_format'] = price_format($market_price);

        //$total_price = $this->get_package_total_price($id);
        $package['total_price']  = $this->get_package_total_price($id);

        return $package;
    }

    /**
    * 获得指定礼包的商品
    *
    * @access  public
    * @param   integer $package_id
    * @return  array
    */
    function get_package_goods($package_id) { // 增加 g.goods_thumb  获取套餐缩略图
        global $db, $ecs, $_CFG;

        $sql = "SELECT pg.goods_id,g.goods_thumb,g.brand_id, g.goods_name, g.shop_price, pg.goods_number, pg.goods_price, p.goods_attr, p.product_number, p.product_id
                FROM " . $ecs->table('package_goods') . " AS pg
                    LEFT JOIN " .$ecs->table('goods') . " AS g ON pg.goods_id = g.goods_id
                    LEFT JOIN " . $ecs->table('products') . " AS p ON pg.product_id = p.product_id
                WHERE pg.package_id = '$package_id'";
        if ($package_id == 0)
        {
            $sql .= " AND pg.admin_id = '$_SESSION[admin_id]'";
        }

        $resource = $db->query($sql);
        if (!$resource)
        {
            return array();
        }

        $row = array();

        /* 生成结果数组 取存在货品的商品id 组合商品id与货品id */
        $good_product_str = '';
        while ($_row = $db->fetch_array($resource))
        {
            if ($_row['product_id'] > 0)
            {
                /* 取存商品id */
                $good_product_str .= ',' . $_row['goods_id'];

                /* 组合商品id与货品id */
                $_row['g_p'] = $_row['goods_id'] . '_' . $_row['product_id'];
            }
            else
            {
                /* 组合商品id与货品id */
                $_row['g_p'] = $_row['goods_id'];
            }

            //生成结果数组
            $row[] = $_row;
        }
        $good_product_str = trim($good_product_str, ',');

        /* 释放空间 */
        unset($resource, $_row, $sql);

        /* 取商品属性 */
        if ($good_product_str != '')
        {
            $sql = "SELECT goods_attr_id, attr_value FROM " .$ecs->table('goods_attr'). " WHERE goods_id IN ($good_product_str)";
            $result_goods_attr = $db->getAll($sql);

            $_goods_attr = array();
            foreach ($result_goods_attr as $value)
            {
                $_goods_attr[$value['goods_attr_id']] = $value['attr_value'];
            }
        }

        /* 过滤货品 */
        $format[0] = '%s[%s]X%d %s (原價: %s)';
        $format[1] = '%sX%d %s (原價: %s)';
    	$counti=0;
    	  foreach ($row as $key => $value)
        {
    		$counti++;
        }
        $total_price = 0;
        foreach ($row as $key => $value)
        {

            if ($value['goods_attr'] != '')
            {
                $goods_attr_array = explode('|', $value['goods_attr']);

                $goods_attr = array();
                foreach ($goods_attr_array as $_attr)
                {
                    $goods_attr[] = $_goods_attr[$_attr];
                }
                $row[$key]['goods_name'] = sprintf($format[0], $value['goods_name'], implode('，', $goods_attr), $value['goods_number'], price_format($value['goods_price'] * $value['goods_number']), price_format($value['shop_price'] * $value['goods_number']));
                $row[$key]['goods_thumb'] = get_image_path($value['goods_id'], $value['goods_thumb'], true);
                $row[$key]['highlight'] = $value['shop_price']>$value['goods_price']?false:true;
    			//$row[$key]['bname'] = hw_get_brand_name($value['brand_id']);
    			$row[$key]['url'] = build_uri('goods', array('gid' => $value['goods_id'], 'gname' => $value['goods_name']), $value['goods_name']);
            }
            else
            {
                $row[$key]['goods_name'] = sprintf($format[1], $value['goods_name'], $value['goods_number'], price_format($value['goods_price'] * $value['goods_number']), price_format($value['shop_price'] * $value['goods_number']));
                $row[$key]['goods_thumb'] = get_image_path($value['goods_id'], $value['goods_thumb'], true);
                $row[$key]['highlight'] = $value['shop_price']>$value['goods_price']?false:true;
    			//$row[$key]['bname'] = hw_get_brand_name($value['brand_id']);
    			$row[$key]['url'] = build_uri('goods', array('gid' => $value['goods_id'], 'gname' => $value['goods_name']), $value['goods_name']);
            }
            $row[$key]['goods_price'] = $value['goods_price'];
    		$row[$key]['i']=$counti;
        }

        return $row;
    }

    /**
    * 取得跟商品关联的礼包列表
    *
    * @param   string  $goods_id    商品编号
    *
    * @return  礼包列表
    */
    function get_package_goods_list($goods_id){
        global $db, $ecs, $_CFG;

        $now = gmtime();
        $sql = "SELECT pg.goods_id, ga.act_id, ga.act_name, ga.act_desc, ga.goods_name, ga.start_time,
                       ga.end_time, ga.is_finished, ga.ext_info, gal.act_name as act_name_i18n 
                FROM " . $ecs->table('goods_activity') . " AS ga left join ".$ecs->table('goods_activity_lang')." gal on (ga.act_id = gal.act_id) and lang = '".$_CFG['lang']."'   , " . $ecs->table('package_goods') . " AS pg
                WHERE pg.package_id = ga.act_id
                AND ga.start_time <= '" . $now . "'
                AND ga.end_time >= '" . $now . "'
                AND ga.status = 1
                AND pg.goods_id = " . $goods_id . "
                GROUP BY ga.act_id
                ORDER BY ga.act_id ";
        $res = $db->getAll($sql);

        foreach ($res as $tempkey => $value)
        {   $gnum = 0;
            $subtotal = 0;

            if (!empty($value['act_name_i18n'])) {
                $value['act_name'] = $value['act_name_i18n'];
                $res[$tempkey]['act_name']  = $value['act_name_i18n'];
            }

            $row = unserialize($value['ext_info']);
            unset($value['ext_info']);
            if ($row)
            {
                foreach ($row as $key=>$val)
                {
                    $res[$tempkey][$key] = $val;
                }
            }

            $sql = "SELECT pg.package_id, pg.goods_id, pg.goods_number, pg.goods_price, pg.admin_id, p.goods_attr, g.goods_sn, g.goods_name, g.market_price, g.goods_thumb, g.goods_img, IFNULL(mp.user_price, g.shop_price * '$_SESSION[discount]') AS rank_price
                    FROM " . $ecs->table('package_goods') . " AS pg
                        LEFT JOIN ". $ecs->table('goods') . " AS g
                            ON g.goods_id = pg.goods_id
                        LEFT JOIN ". $ecs->table('products') . " AS p
                            ON p.product_id = pg.product_id
                        LEFT JOIN " . $ecs->table('member_price') . " AS mp
                            ON mp.goods_id = g.goods_id AND mp.user_rank = '$_SESSION[user_rank]'
                    WHERE pg.package_id = " . $value['act_id']. "
                    ORDER BY pg.package_id, pg.goods_id";
            $goods_res = $db->getAll($sql);

            $total_price = $this->get_package_total_price($value['act_id']);

            $sql = "";
            $goods_count = 0;
            foreach($goods_res as $key => $val)
            {
                $final_price_info = get_final_price($val['goods_id'], 1, true);
                $final_price = $final_price_info['final_price'];

                $goods_id_array[] = $val['goods_id'];
                $goods_res[$key]['goods_count']  = $goods_count;
                $goods_res[$key]['goods_thumb']  = get_image_path($val['goods_id'], $val['goods_thumb'], true);
                $goods_res[$key]['goods_img']  = get_image_path($val['goods_id'], $val['goods_img']);
                $goods_res[$key]['goods_price'] = $val['goods_price'];
                $goods_res[$key]['market_price'] = price_format($val['market_price']);
                $goods_res[$key]['final_price']   = price_format($final_price);
                $goods_res[$key]['url']   = build_uri('goods', array('gid' => $val['goods_id'], 'gname' => $val['goods_name']), $val['goods_name']);
                $subtotal += $val['market_price'] * $val['goods_number'];
                $gnum+=$val['goods_number'];
                $goods_count++;
            }

            /* 取商品属性 */
            $sql = "SELECT ga.goods_attr_id, ga.attr_value
                    FROM " .$ecs->table('goods_attr'). " AS ga, " .$ecs->table('attribute'). " AS a
                    WHERE a.attr_id = ga.attr_id
                    AND a.attr_type = 1
                    AND " . db_create_in($goods_id_array, 'goods_id');
            $result_goods_attr = $db->getAll($sql);

            $_goods_attr = array();
            foreach ($result_goods_attr as $value)
            {
                $_goods_attr[$value['goods_attr_id']] = $value['attr_value'];
            }

            /* 处理货品 */
            $format = '[%s]';
            foreach($goods_res as $key => $val)
            {
                if ($val['goods_attr'] != '')
                {
                    $goods_attr_array = explode('|', $val['goods_attr']);

                    $goods_attr = array();
                    foreach ($goods_attr_array as $_attr)
                    {
                        $goods_attr[] = $_goods_attr[$_attr];
                    }

                    $goods_res[$key]['goods_attr_str'] = sprintf($format, implode('，', $goods_attr));
                }
            }

            //cal discount rate
            $discount = round(($total_price / $subtotal), 2) * 100;
            if($discount % 10 == 0){
                $discount = intval($discount / 10);
            }

            $res[$tempkey]['gnumber']       = $gnum;
            $res[$tempkey]['goods_list']    = $goods_res;
            $res[$tempkey]['subtotal']      = price_format($subtotal);
            $res[$tempkey]['saving']        = price_format($subtotal - $total_price);
            $res[$tempkey]['total_price']   = price_format($total_price);
            $res[$tempkey]['discount']      = $discount;
        }

        return $res;
    }


    function get_package_total_price($id){
        global $db, $ecs, $_CFG;

        $sql = "SELECT SUM(goods_price * goods_number) AS total_price " .
               "FROM " . $ecs->table('package_goods') . " " .
               "WHERE package_id = '" . $id . "' ";

        return $db->getOne($sql);
    }


}

