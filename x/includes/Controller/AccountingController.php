<?php

namespace Yoho\cms\Controller;

require_once ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_accounting.php';
require_once(ROOT_PATH . 'includes/php-xlsx-writer/xlsxwriter.class.php');

use Yoho\cms\Model;

if (!defined('IN_ECS')) {
    die('Hacking attempt');
}

class AccountingController extends YohoBaseController
{
    const CSV_BOC = 1;
    const CSV_HS= 2;
    const CSV_HSBC = 3;
    const CSV_BOC_MO = 4;
    const CSV_HSBC_GLOBIZ = 5;
    const CSV_CONFIG = [
        self::CSV_BOC => [
            'date' => 9,
            'dateFormat' => 'Y/m/d',
            'type' => [[-1 => 'D', 1 => 'C'], 12], // [[debit / credit], column]
            'remark' => 16,
            'amount' => 13,
            'balance' => 14,
            'open' => -1,
            'close' => -1,
            'file' => 0
        ],
        self::CSV_HS=> [
            'date' => 0,
            'dateFormat' => 'j/n/Y',
            'remark' => 1,
            'debit' => 2,
            'credit' => 3,
            'balance' => 4,
            'open' => 3,
            'close' => 0,
            'file' => 0
        ],
        self::CSV_HSBC => [
            'date' => 0,
            'dateFormat' => 'j/n/Y',
            'remark' => 1,
            'debit' => 2,
            'credit' => 3,
            'balance' => 4,
            'open' => 3,
            'close' => 0,
            'file' => 0
        ],
        self::CSV_BOC_MO => [
            'date' => 0,
            'dateFormat' => 'Y-m-d',
            'remark' => 1,
            'debit' => 4,
            'credit' => 5,
            'balance' => 6,
            'open' => -1,
            'close' => -1,
            'start' => 5,
            'file' => 1
        ],
        self::CSV_HSBC_GLOBIZ => [
            'date' => 0,
            'dateFormat' => 'j M Y',
            'remark' => 1,
            'debit' => 2,
            'credit' => 3,
            'balance' => 4,
            'open' => 3,
            'close' => 0,
            'file' => 0,
            'dr' => 5,
        ],
    ];
    const CSV_REC_TYPE_TXN = 0;
    const CSV_REC_TYPE_OPEN = 1;
    const CSV_REC_TYPE_CLOSE = 2;

    const POSTING_STATUS = [ACTG_POST_PENDING, ACTG_POST_APPROVED, ACTG_POST_REJECTED];

    const ACTG_SYSTEM_USER = [
        '-2' => 'Yoho Bravo 1',
    ];

    public $actg;
    public $actgHooks;

    public function __construct()
    {
        parent::__construct();
        $this->tableName = 'actg_accounts';
        $actg = new \Accounting($this->db);
        $actgHooks = new \AccountingHooks($actg, $this->db);
        $this->actg = $actg;
        $this->actgHooks = $actgHooks;
    }
    
    /** 
     * Import and extract bank records
     * 
     * @return void
     */
    public function importBankRecords()
    {
        global $db;
        /**
         * Get amount of a transaction
         * 
         * @param phpExcelWorksheet $objWorksheet current csv
         * @param int               $row          transaction row
         * @param array             $config       csv config
         * 
         * @return float amount
         */
        function getTxnAmount($objWorksheet, $row, $config)
        {
            if (empty($config['type'])) {
                $debit  = formatAmount($objWorksheet->getCellByColumnAndRow($config['debit'], $row)->getValue());
                $credit = formatAmount($objWorksheet->getCellByColumnAndRow($config['credit'], $row)->getValue());
                $amount = bcsub($credit, $debit);
            } else {
                $value = $objWorksheet->getCellByColumnAndRow($config['amount'], $row)->getValue();
                if (isset($config['dr'])) {
                    if ($objWorksheet->getCellByColumnAndRow($config['dr'], $row)->getValue() == "DR") {
                        $value = "DR" . $value;
                    }
                }
                $amount = formatAmount($value);
                $type = $objWorksheet->getCellByColumnAndRow($config['type'][1], $row)->getValue();
                $amount *= array_search($type, $config['type'][0]);
            }
            return $amount;
        }

        /**
         * Format money amount
         * 
         * @param string $input unformatted amount
         * 
         * @return float formatted amount
         */
        function formatAmount($input)
        {
            if (strpos($input, "DR") !== false) {
                $input = "-" . str_replace("DR", "", $input);
            }
            return floatval(str_replace(',', '', $input));
        }

        /**
         * Format date
         * 
         * @param string $input  unformatted date
         * @param string $format format
         * 
         * @return float formatted date
         */
        function formatDate($input, $format)
        {
            $arr = date_parse_from_format($format, trim($input));
            if ($arr['error_count']) {
                die(var_dump($arr, $input));
                sys_msg("日期格式錯誤");
            }
            return local_date("Y-m-d", strtotime("$arr[year]-$arr[month]-$arr[day]"));
        }

        // init data
        ini_set('memory_limit', '256M');
        define('PCLZIP_TEMPORARY_DIR', ROOT_PATH . 'temp/');
        $precision = ini_get('precision');
        $config = self::CSV_CONFIG[$_POST['bank_id']];

        // Get excel data
        include_once ROOT_PATH . 'includes/excel/PHPExcel.php';
        include_once ROOT_PATH . 'includes/excel/PHPExcel/IOFactory.php';
        \PHPExcel_Settings::setZipClass(\PHPExcel_Settings::PCLZIP);
        $fileName     = $_FILES["import_excel"]["tmp_name"];
        if ($config['file']) {
            $fileType = \PHPExcel_IOFactory::identify($fileName);
        } else {
            $fileType = 'CSV';
        }
        $objReader    = \PHPExcel_IOFactory::createReader($fileType);
        $objPHPExcel  = $objReader->load($fileName);
        $objWorksheet = $objPHPExcel->getSheet(0);

        ini_set('precision', $precision);

        $open = $config['open'];
        $close = $config['close'];
        $start = empty($config['start']) ? ($open == -1 ? 1 : $open + 1) : $config['start'];
        $end = $objWorksheet->getHighestRow() - ($close == -1 ? 0 : $close);
        $open_balance = 0;
        $close_balance = 0;

        $result = [
            'account_id' => $_POST['account_id'],
            'open_balance' => 0,
            'txns' => [],
            'close_balance' => 0,
            'check_sum' => 0
        ];

        foreach ($objWorksheet->getRowIterator() as $i => $row) {
            // Open balance
            if ($open != -1 && $open == $i) {
                $balance_value = $objWorksheet->getCellByColumnAndRow($config['balance'], $i)->getValue();
                if (isset($config['dr'])) {
                    if (trim($objWorksheet->getCellByColumnAndRow($config['dr'], $i)->getValue()) == "DR") {
                        $balance_value = "DR" . $balance_value;
                    }
                }
                $balance = formatAmount($balance_value);
                $date = $objWorksheet->getCellByColumnAndRow($config['date'], $i)->getValue();
                $formatted_date = formatDate($date, $config['dateFormat']);
                $result['open_balance'] = [
                    'date' => $formatted_date,
                    'amount' => $balance
                ];
            } elseif ($open == -1 && $i == $start) {
                $amount = getTxnAmount($objWorksheet, $i, $config);
                $balance_value = $objWorksheet->getCellByColumnAndRow($config['balance'], $i)->getValue();
                if (isset($config['dr'])) {
                    if (trim($objWorksheet->getCellByColumnAndRow($config['dr'], $i)->getValue()) == "DR") {
                        $balance_value = "DR" . $balance_value;
                    }
                }
                $balance = formatAmount($balance_value);
                $date = $objWorksheet->getCellByColumnAndRow($config['date'], $i)->getValue();
                $formatted_date = formatDate($date, $config['dateFormat']);
                $result['close_balance'] = [
                    'date' => $formatted_date,
                    'amount' => $balance
                ];
            }
            // Close balance
            if (($close != -1 && $end == $i) || ($close == -1 && $i == $end)) {
                $date = $objWorksheet->getCellByColumnAndRow($config['date'], $i)->getValue();
                $formatted_date = formatDate($date, $config['dateFormat']);
                $balance_value = $objWorksheet->getCellByColumnAndRow($config['balance'], $i)->getValue();
                if (isset($config['dr'])) {
                    if (trim($objWorksheet->getCellByColumnAndRow($config['dr'], $i)->getValue()) == "DR") {
                        $balance_value = "DR" . $balance_value;
                    }
                }
                $balance = formatAmount($balance_value);
                if ($close == -1) {
                    $amount = getTxnAmount($objWorksheet, $i, $config);
                    $balance = bcsub($balance, $amount);
                    $result['open_balance'] = [
                        'date' => $formatted_date,
                        'amount' => $balance
                    ];
                } else {
                    $result['close_balance'] = [
                        'date' => $formatted_date,
                        'amount' => $balance
                    ];
                }
            }
            // Transactions
            if ($i >= $start && ($i < $end || ($close == -1 && $i == $end))) {
                $date = $objWorksheet->getCellByColumnAndRow($config['date'], $i)->getValue();
                $formatted_date = formatDate($date, $config['dateFormat']);
                $amount = getTxnAmount($objWorksheet, $i, $config);
                $result['check_sum'] = bcadd($result['check_sum'], $amount);
                if (is_array($config['remark'])) {
                    $remarks = [];
                    foreach ($config['remark'] as $c) {
                        $remarks[] = trim($objWorksheet->getCellByColumnAndRow($c, $i)->getValue());
                    }
                    $remark = implode(" ", $remarks);
                } else {
                    $remark = trim($objWorksheet->getCellByColumnAndRow($config['remark'], $i)->getValue());
                }
                $result['txns'][$formatted_date][] = array(
                    'amount' => $amount,
                    'remark' => mysql_escape_string($remark)
                );
            }
        }

        // handle if no transaction on open / close balance date
        if (empty($result['txns'][$result['open_balance']['date']])) {
            $result['txns'][$result['open_balance']['date']][] = array('amount' => 0);
        }
        if (empty($result['txns'][$result['close_balance']['date']])) {
            $result['txns'][$result['close_balance']['date']][] = array('amount' => 0);
        }

        ksort($result['txns']);
        $last_date = local_strtotime($result['open_balance']['date']);
        $sql = "SELECT amount FROM `actg_bank_transactions` WHERE UNIX_TIMESTAMP(date) < $last_date AND account_id = $result[account_id] AND type = " . self::CSV_REC_TYPE_CLOSE . " ORDER BY date DESC";
        $last_close = $db->getOne($sql);
        $sql = "SELECT COUNT(*) FROM `actg_bank_transactions` WHERE account_id = $result[account_id]";
        $any_txn = $db->getOne($sql);
        $sql = "SELECT COUNT(*) FROM `actg_bank_transactions` WHERE date " . db_create_in(array_keys($result['txns'])) . " AND account_id = $result[account_id]";
        $imported = $db->getOne($sql);

        /* Allow insert new record if 
            1. no transaction in previous day
            2. reslut's open balance = previous day's close balance
        */
        if ($imported) {
            sys_msg('已存在上傳紀錄', 1);
        } elseif (($last_close && bccomp($last_close, $result['open_balance']['amount'])) == 0 || !$any_txn) {
            if (bcadd($result['open_balance']['amount'], $result['check_sum']) == $result['close_balance']['amount']) {
                $GLOBALS['db']->query('START TRANSACTION');
                $curr_balance = $result['open_balance']['amount'];
                foreach ($result['txns'] as $date => $rec) {
                    if ($open == -1) {
                        $rec = array_reverse($rec);
                    }
                    foreach ($rec as $key => $data) {
                        // Insert open balance
                        if ($key == 0) {
                            $sql_open = "INSERT INTO `actg_bank_transactions` (account_id, date, type, amount) VALUES ($result[account_id], '$date', " . self::CSV_REC_TYPE_OPEN . ", $curr_balance)";
                            $db->query($sql_open);
                        }
                        // Insert all transactions
                        if ($data['amount'] != 0) {
                            $sql_txn = "INSERT INTO `actg_bank_transactions` (account_id, date, type, amount, remark) VALUES ($result[account_id], '$date', " . self::CSV_REC_TYPE_TXN . ", $data[amount], '$data[remark]')";
                            $db->query($sql_txn);
                            $curr_balance = bcadd($curr_balance, $data['amount']);
                            $rec_id = $db->insert_id();
                            self::autoInsertTransaction($data, $result['account_id'], $rec_id, $date);
                        }
                        // Insert close balance
                        if ($key == count($rec) - 1) {
                            $sql_close = "INSERT INTO `actg_bank_transactions` (account_id, date, type, amount) VALUES ($result[account_id], '$date', " . self::CSV_REC_TYPE_CLOSE . ", $curr_balance)";
                            $db->query($sql_close);
                        }
                    }
                }
                $GLOBALS['db']->query('COMMIT');
                sys_msg('上傳完成', 0, array(array('href' => 'bank_reconciliation.php?act=list', 'text' => '返回銀行交易紀錄')));
            } else {
                sys_msg('Close balance 不符', 1, array(array('href' => 'bank_reconciliation.php?act=import', 'text' => '上傳銀行紀錄')));
            }
        } else {
            sys_msg('Open balance 不符', 1, array(array('href' => 'bank_reconciliation.php?act=import', 'text' => '上傳銀行紀錄')));
        }
    }

    /**
     * Auto insert transaction to accounting system
     *
     * @param array  $txn        bank transaction
     * @param int    $account_id target account
     * @param int    $rec_id     bank rec id
     * @param string $txn_date   bank rec date
     *
     * @return void
     */
    function autoInsertTransaction($txn, $account_id, $rec_id, $txn_date)
    {
        $txn_date = empty($txn_date) ? local_date('Y-m-d') : $txn_date;
        $internal_transfer = [
            [
                'id' => 18, // Stripe
                'pattern' => 'STS PYT-STRIPE PAYME'
            ],
            [
                'id' => 20, // Braintree
                'pattern' => 'SCBHK167810887'
            ],
            [
                'id' => 21, // Visa/Master
                'pattern' => '088007831800'
            ],
            [
                'id' => 22, // EPS
                'pattern' => 'EPSCO SUSPENSE'
            ],
            [
                'id' => 23, // AE
                'pattern' => 'AMEX 9810056044'
            ],
            [
                'id' => 20, // Braintree
                'pattern' => 'AMEX 9810648212'
            ],
            [
                'id' => 13, // PayPal
                'pattern' => 'PAYPAL HK LTD'
            ],
            [
                'id' => 32, // QFPay
                'pattern' => 'IFLARE HONG KONG LTD'
            ],
            [
                'id' => 35, // AlipayHK
                'pattern' => 'ALIPAY HONG KONG LIMITED'
            ],
            [
                'id' => 37, // PayMe
                'pattern' => 'PAYME(HSBC)'
            ],
        ];
        $new_transaction = [
            [
                'id' => 2, // Yahoo
                'pattern' => 'YAHOO HONG KONG LTD'
            ],
            [
                'id' => 1, // HKTV
                'pattern' => 'HONG KONG TV SHOPPIN'
            ],
            [
                'id' => 3, // Asia Miles
                'pattern' => 'CATHAY PACIFIC AIRWA'
            ],
            [
                'id' => 5, // Price.com 限時購
                'pattern' => 'PRICE COM HK LTD'
            ],
        ];
        foreach ($internal_transfer as $data) {
            if (strpos($txn['remark'], $data['pattern']) !== false) {
                $param = [
                    'from_account' => $data['id'],
                    'to_account' => $account_id,
                    'amount' => $txn['amount'],
                    'created_by' => $_SESSION['admin_id'],
                    'txn_date' => $this->actg->dbdate(strtotime($txn_date)),
                ];
                if ($actg_txn_id = self::insertExchanges($param)) {
                    $txn_id = "FIN" . $actg_txn_id;
                    self::linkBankTransactionRecord($txn_id, $rec_id);
                }
            }
        }
        foreach ($new_transaction as $data) {
            if (strpos($txn['remark'], $data['pattern']) !== false) {
                $sales_agent_pay_type = self::getSalesAgentPayType($data['id']);
                if (intval($sales_agent_pay_type) < 0){
                    continue;
                }
                $param = [
                    'from_actg_type_id' => self::getAccountingTypeByAccountId($account_id, 'actg_type_id'), // Cash / Bank
                    'to_actg_type_id'   => self::getAccountingType(['actg_type_code' => ($sales_agent_pay_type == 1 ? '28003' : '13130'), 'enabled' => 1], 'actg_type_id'), // Receipt in Advance-B2B2C / B2B2C customer
                    'amount'            => $txn['amount'],
                    'remark' 	        => "Sales ($txn[remark])",
                    'created_by'        => $_SESSION['admin_id'],
                    'txn_date'          => $txn_date,
                    'sa_id'             => $data['id'],
                ];

                if ($actg_txn_id = self::insertDoubleEntryTransactions($param)) {
                    $txn_id = "FIN" . $actg_txn_id;
                    self::linkBankTransactionRecord($txn_id, $rec_id);
                }
            }
        }
    }

    /**
     * Get imported bank records
     *
     * @return array
     */
    public function retriveBankRecords()
    {
        $filter['start_date'] = !empty($_REQUEST['start_date']) ? $_REQUEST['start_date'] : '';
        $filter['account_id'] = !empty($_REQUEST['account_id']) ? $_REQUEST['account_id'] : '';
        $filter['remark'] = !empty($_REQUEST['remark']) ? trim($_REQUEST['remark']) : '';

        $where = 'WHERE 1 ' .
                 (empty($filter['start_date']) || empty($filter['account_id']) ? ' AND type NOT IN (' . self::CSV_REC_TYPE_OPEN . ', ' . self::CSV_REC_TYPE_CLOSE . ')' : '') .
                 (!empty($filter['start_date']) ? " AND date = '$filter[start_date] 00:00:00' " : '') .
                 (!empty($filter['account_id']) ? " AND bt.account_id = " . $filter['account_id'] : '') .
                 (!empty($filter['remark']) ? " AND bt.remark LIKE '%$filter[remark]%'": '');

        $sql = "SELECT count(*) FROM `actg_bank_transactions` bt " . $where;
        $filter['record_count'] = $GLOBALS['db']->getOne($sql);
        $filter = page_and_size($filter);

        $sql = "SELECT * FROM `actg_bank_transactions` bt " .
                "LEFT JOIN `actg_accounts` a ON a.account_id = bt.account_id " .
                "LEFT JOIN (SELECT currency_code, currency_format FROM `actg_currencies`) as c ON a.currency_code = c.currency_code " . $where .
                " ORDER BY date DESC, rec_id DESC" .
                " LIMIT " . $filter['start'] . "," . $filter['page_size'];
        $records = $GLOBALS['db']->getAll($sql);

        foreach ($records as $key => $row) {
            $records[$key]['formatted_date'] = local_date('Y-m-d', strtotime($row['date']));
            $amount = $row['type'] == self::CSV_REC_TYPE_TXN ? abs($row['amount']) : $row['amount'];
            $records[$key]['formatted_amount'] = $this->actg->money_format($amount, $row['currency_format']);
        }
        return array(
            'records' => $records,
            'filter' => $filter,
            'page_count' => $filter['page_count'],
            'record_count' => $filter['record_count']
        );
    }

    /**
     * Get matched records (bank => transaction)
     *
     * @return array records
     */
    public function retriveMatchedRecords()
    {
        $filter['account_id'] = empty($_REQUEST['account_id']) ? 0 : $_REQUEST['account_id'];
        $filter['start_date'] = empty($_REQUEST['start_date']) ? local_date("Y-m-d") : $_REQUEST['start_date'];
        $filter['end_date'] = empty($_REQUEST['end_date']) ? (empty($filter['start_date']) ? 0 : $filter['start_date']) : $_REQUEST['end_date'];
        $filter['not_match'] = empty($_REQUEST['not_match']) ? 0 : $_REQUEST['not_match'];

        $where = (empty($filter['account_id']) ? "" : " AND bt.account_id = $filter[account_id] ") .
                (empty($filter['start_date']) ? "" : " AND bt.date BETWEEN '$filter[start_date] 00:00:00' AND '$filter[end_date] 23:59:59' ");

        $sql = "SELECT bt.rec_id, bt.date, bt.amount as b_amount, bt.remark as b_remark, bt.txn_group_id, a.account_name, c.currency_format " .
                "FROM `actg_bank_transactions` bt " .
                "LEFT JOIN `actg_accounts` a ON bt.account_id = a.account_id " .
                "LEFT JOIN (SELECT currency_code, currency_format FROM `actg_currencies`) as c ON a.currency_code = c.currency_code " .
                "WHERE bt.type = 0 AND bt.txn_group_id != 0 " . $where .
                "ORDER BY bt.date DESC, bt.rec_id DESC";
        $bank_rec = $GLOBALS['db']->getAll($sql);
        if (!empty($bank_rec)) {
            $unique_txns = [];
            foreach ($bank_rec as $rec) {
                $rec['formatted_bank_date'] = local_date('Y-m-d', strtotime($rec['date']));
                $rec['formatted_b_amount'] = $this->actg->money_format($rec['b_amount'], $rec['currency_format']);
                $unique_txns[$rec['txn_group_id']]['bank_rec'][] = $rec;
                $unique_txns[$rec['txn_group_id']]['account_name'] = $rec['account_name'];
            }
            $sql = "SELECT t.txn_id, t.complete_date, t.remark as t_remark, t.txn_group_id, t.amount as t_amount, c.currency_format " .
                "FROM `actg_account_transactions` t " .
                "LEFT JOIN (SELECT currency_code, currency_format FROM `actg_currencies`) as c ON t.currency_code = c.currency_code " .
                "WHERE txn_group_id " . db_create_in(array_keys($unique_txns)) .
                "ORDER BY t.txn_id DESC";
            $txns = $GLOBALS['db']->getAll($sql);
            foreach ($txns as $txn) {
                $txn['txn_id'] = "ACC" . $txn['txn_id'];
                $txn['formatted_txn_date'] = local_date($GLOBALS['_CFG']['time_format'], strtotime($txn['complete_date']));
                $txn['formatted_t_amount'] = $this->actg->money_format($txn['t_amount'], $txn['currency_format']);
                $unique_txns[$txn['txn_group_id']]['txns'][] = $txn;
            }

            $sql = "SELECT t.actg_txn_id, t.txn_date, t.remark as t_remark, t.txn_group_id, t.amount as t_amount, c.currency_format " .
                    "FROM `actg_accounting_transactions` t " .
                    "LEFT JOIN (SELECT currency_code, currency_format FROM `actg_currencies`) as c ON t.currency_code = c.currency_code " .
                    "WHERE txn_group_id " . db_create_in(array_keys($unique_txns)) .
                    "ORDER BY t.actg_txn_id DESC";
            $actg_txns = $GLOBALS['db']->getAll($sql);
            foreach ($actg_txns as $txn) {
                $txn['txn_id'] = "FIN" . $txn['actg_txn_id'];
                $txn['formatted_txn_date'] = $txn['txn_date'];
                $txn['formatted_t_amount'] = $this->actg->money_format($txn['t_amount'], $txn['currency_format']);
                $unique_txns[$txn['txn_group_id']]['actg_txns'][] = $txn;
            }
        }

        foreach ($unique_txns as $key => $group_txn) {
            $unique_txns[$key]['txn_sum'] = 0;
            $unique_txns[$key]['bank_sum'] = 0;
            foreach ($group_txn['txns'] as $txn) {
                $unique_txns[$key]['txn_sum'] = bcadd($unique_txns[$key]['txn_sum'], $txn['t_amount']);
            }
            foreach ($group_txn['actg_txns'] as $txn) {
                $unique_txns[$key]['txn_sum'] = bcadd($unique_txns[$key]['txn_sum'], $txn['t_amount']);
            }
            foreach ($group_txn['bank_rec'] as $txn) {
                $unique_txns[$key]['bank_sum'] = bcadd($unique_txns[$key]['bank_sum'], $txn['b_amount']);
            }
            $unique_txns[$key]['count'] = count($group_txn['bank_rec']);
            $unique_txns[$key]['diff'] = abs(bcsub($unique_txns[$key]['txn_sum'], $unique_txns[$key]['bank_sum']));
        }
        if ($filter['not_match']) {
            foreach ($unique_txns as $key => $txn) {
                if (abs(bcsub($txn['txn_sum'], $txn['bank_sum'])) < 0.1) {
                    unset($unique_txns[$key]);
                }
            }
        }
        return array(
            'txns' => $unique_txns,
            'filter' => $filter
        );
    }

    public function searchMatchedRecords()
    {
        global $db;
        $filter = [];
        $filter['account_id'] = empty($_REQUEST['account_id']) ? 0 : $_REQUEST['account_id'];
        $filter['not_match'] = empty($_REQUEST['not_match']) ? 0 : $_REQUEST['not_match'];
        $filter['transaction_code'] = !empty($_REQUEST['transaction_code']) ? $_REQUEST['transaction_code'] : '';

        // TODO: watch for the related transaction id may also contain the desgniated 
        $accounting_record_query = "SELECT CONCAT('FIN', t.actg_txn_id) as transaction_id, t.txn_date as `txn_date`, t.remark as t_remark, t.txn_group_id, t.amount as t_amount, c.currency_format " .
                                    "FROM `actg_accounting_transactions` t " .
                                    "LEFT JOIN (SELECT currency_code, currency_format FROM `actg_currencies`) as c ON t.currency_code = c.currency_code " .
                                    "WHERE txn_group_id != 0 ";
        
        $account_record_query = "SELECT CONCAT('ACC', t.txn_id) AS transaction_id, t.complete_date AS `txn_date`, t.remark as t_remark, t.txn_group_id, t.amount as t_amount, c.currency_format " .
                                "FROM `actg_account_transactions` t " .
                                "LEFT JOIN (SELECT currency_code, currency_format FROM `actg_currencies`) as c ON t.currency_code = c.currency_code " .
                                "WHERE txn_group_id != 0 " . $where;

        $reconciliation_accounting_record_query = "($accounting_record_query) UNION ($account_record_query)";

        $where = (empty($filter['account_id']) ? "" : " AND bt.account_id = $filter[account_id] ");
        $bank_record_query = "SELECT bt.rec_id, bt.date, bt.amount as b_amount, bt.remark as b_remark, bt.txn_group_id as bk_txn_group_id, a.account_name, c.currency_format as bk_currency_format " .
                "FROM `actg_bank_transactions` bt " .
                "LEFT JOIN `actg_accounts` a ON bt.account_id = a.account_id " .
                "LEFT JOIN (SELECT currency_code, currency_format FROM `actg_currencies`) as c ON a.currency_code = c.currency_code " .
                "WHERE bt.type = 0 AND bt.txn_group_id != 0 " . $where ;

        $reconciliation_query = "SELECT * FROM ($reconciliation_accounting_record_query) AS actg LEFT JOIN ($bank_record_query) AS bkr ON actg.txn_group_id = bkr.bk_txn_group_id UNION ALL SELECT * FROM ($reconciliation_accounting_record_query) AS actg2 RIGHT JOIN ($bank_record_query) AS bkr2 ON actg2.txn_group_id = bkr2.bk_txn_group_id WHERE actg2.transaction_id IS NULL";

        $reconciliation_query = "SELECT * FROM ($reconciliation_query) as rq WHERE transaction_id IS NOT NULL and rec_id IS NOT NULL";
        if (!empty($filter['transaction_code'])) {
            $reconciliation_query .= " AND txn_group_id IN (SELECT DISTINCT txn_group_id FROM ($reconciliation_query) as rqn WHERE transaction_id LIKE '%$filter[transaction_code]%')";
        }
        $reconciliation_entry = $db->getAll($reconciliation_query);

        // Build detailed transaction list
        $reconciliation_transaction = [];
        foreach($reconciliation_entry as $item) {
            if (isset($reconciliation_transaction[$item['txn_group_id']])) {
                // In case of having transaction data in a given transaction group id
                // In case of new bank record
                $search_result = array_search($item['rec_id'], array_column($reconciliation_transaction[$item['txn_group_id']]['bank_rec'], 'rec_id'));
                if (is_numeric($search_result) && $search_result >= 0) {
                } else {
                    $reconciliation_transaction[$item['txn_group_id']]['bank_rec'][] = [
                        "rec_id" => $item['rec_id'],
                        "date" => $item["date"],
                        "b_amount" => $item["b_amount"],
                        "b_remark" => $item["b_remark"],
                        "txn_group_id" => $item["bk_txn_group_id"],
                        "account_name" => $item["account_name"],
                        "currency_format" => $item["bk_currency_format"],
                        "formatted_bank_date" => local_date('Y-m-d', strtotime($item['date'])),
                        "formatted_b_amount" => $this->actg->money_format($item['b_amount'], $item['bk_currency_format']),
                    ];
                    $reconciliation_transaction[$item['txn_group_id']]['bank_sum'] += $item["b_amount"];
                    $reconciliation_transaction[$item['txn_group_id']]['count']++;
                }

                // In case of new transaction record
                $search_result = array_search($item['transaction_id'], array_column($reconciliation_transaction[$item['txn_group_id']]['actg_txns'], 'txn_id'));
                if (is_numeric($search_result) && $search_result >= 0) {
                } else {
                    $reconciliation_transaction[$item['txn_group_id']]['actg_txns'][] = [
                        "txn_id" => $item['transaction_id'],
                        "txn_date" => $item['txn_date'],
                        "t_remark" => $item['t_remark'],
                        "txn_group_id" => $item['txn_group_id'],
                        "t_amount" => $item['t_amount'],
                        "currency_format" => $item['currency_format'],
                        "formatted_txn_date" => $item['txn_date'],
                        "formatted_t_amount" =>  $this->actg->money_format($item['t_amount'], $item['currency_format']),
                    ];
                    $reconciliation_transaction[$item['txn_group_id']]['txn_sum'] += $item["t_amount"];
                }

                // Re-calculate difference
                $reconciliation_transaction[$item['txn_group_id']]['diff'] = abs(bcsub($reconciliation_transaction[$item['txn_group_id']]['txn_sum'],$reconciliation_transaction[$item['txn_group_id']]['bank_sum']));
            } else {
                // In case of having nothing in a given transaction group id
                // Setup Init Values
                $reconciliation_transaction[$item['txn_group_id']] = [
                    "bank_rec" => [[
                        "rec_id" => $item['rec_id'],
                        "date" => $item["date"],
                        "b_amount" => $item["b_amount"],
                        "b_remark" => $item["b_remark"],
                        "txn_group_id" => $item["bk_txn_group_id"],
                        "account_name" => $item["account_name"],
                        "currency_format" => $item["bk_currency_format"],
                        "formatted_bank_date" => local_date('Y-m-d', strtotime($item['date'])),
                        "formatted_b_amount" => $this->actg->money_format($item['b_amount'], $item['bk_currency_format']),
                    ]],
                    "account_name" => $item["account_name"],
                    "actg_txns" => [[
                        "txn_id" => $item['transaction_id'],
                        "txn_date" => $item['txn_date'],
                        "t_remark" => $item['t_remark'],
                        "txn_group_id" => $item['txn_group_id'],
                        "t_amount" => $item['t_amount'],
                        "currency_format" => $item['currency_format'],
                        "formatted_txn_date" => $item['txn_date'],
                        "formatted_t_amount" =>  $this->actg->money_format($item['t_amount'], $item['currency_format']),
                    ]],
                    "txn_sum" => $item['t_amount'],
                    "bank_sum" => $item['b_amount'],
                    "count" => 1,
                    "diff" => abs(bcsub($item['t_amount'], $item['b_amount'])),
                ];
            }
        }

        if ($filter['not_match']) {
            foreach ($reconciliation_transaction as $key => $txn) {
                if (bccomp($txn['diff'], '0') == 0) {
                    unset($reconciliation_transaction[$key]);
                }
            }
        }

        return array(
            'txns' => $reconciliation_transaction,
            'filter' => $filter
        );
    }

    /**
     * Get records in transaction group
     * 
     * @return array records
     */
    public function retriveTxnGroupRecords()
    {
        global $db;

        $txn_group_id = empty($_REQUEST['txn_group_id']) ? 0 : intval($_REQUEST['txn_group_id']);
        if (empty($txn_group_id)) {
            make_json_result([]);
        }

        // Records from bank
        $banks = $db->getAll("SELECT * FROM `actg_bank_transactions` WHERE txn_group_id = $txn_group_id");
        // Records from old accounting system
        $txns = $db->getAll("SELECT txn_id, account_id, amount, remark, complete_date FROM `actg_account_transactions` WHERE txn_group_id = $txn_group_id");
        // Records from old accounting system
        $actg_txns = $db->getAll(
            "SELECT actg_txn_id, account_id, amount, remark, txn_date FROM `actg_accounting_transactions` t " .
            "LEFT JOIN `actg_accounts` a ON a.actg_type_id = t.actg_type_id " .
            "WHERE txn_group_id = $txn_group_id"
        );

        $accounts = [];
        $bank_rec = [];
        $txn_rec = [];
        foreach ($banks as $row) {
            if (!array_key_exists($row['account_id'], $accounts)) {
                $accounts[$row['account_id']] = $this->actg->getAccount(['account_id' => $row['account_id']]);;
            }
            $row['account_name'] = $accounts[$row['account_id']]['account_name'];
            $row['formatted_date'] = local_date("Y-m-d", local_strtotime($row['date']));
            $bank_rec[] = $row;
        }
        foreach ($txns as $row) {
            if (!array_key_exists($row['account_id'], $accounts)) {
                $accounts[$row['account_id']] = $this->actg->getAccount(['account_id' => $row['account_id']]);;
            }
            $row['account_name'] = $accounts[$row['account_id']]['account_name'];
            $row['formatted_txn_id'] = "ACC$row[txn_id]";
            $row['formatted_date'] = local_date("Y-m-d H:i:s", strtotime($row['complete_date']));
            $row['amount'] = round($row['amount'], 2);
            $txn_rec[] = $row;
        }
        foreach ($actg_txns as $row) {
            if (!array_key_exists($row['account_id'], $accounts)) {
                $accounts[$row['account_id']] = $this->actg->getAccount(['account_id' => $row['account_id']]);;
            }
            $row['account_name'] = $accounts[$row['account_id']]['account_name'];
            $row['formatted_txn_id'] = "FIN$row[actg_txn_id]";
            $row['formatted_date'] = $row['txn_date'];
            $row['amount'] = round($row['amount'], 2);
            $txn_rec[] = $row;
        }

        make_json_result([
            'bank_rec' => $bank_rec,
            'txn_rec' => $txn_rec,
        ]);
    }

    /**
     * Get bank records for audit
     * 
     * @return file excel
     */
    public function generateBankAuditReport()
    {
        global $db;

        $start_date = trim($_REQUEST['start_date']);
        $end_date = trim($_REQUEST['end_date']);

        // default audit year: April-1 -> March-31
        if (empty($start_date)) {
            $start_date = date("Y-m-d", strtotime('last year first day of april'));
        }
        if (empty($end_date)) {
            $end_date = date("Y-m-d", strtotime('this year last day of march'));
        }

        if (empty($_REQUEST['account_id'])) {
            $account_ids = $db->getCol("SELECT DISTINCT account_id FROM `actg_bank_transactions` WHERE date BETWEEN '$start_date' AND '$end_date'");
        } else {
            $account_ids = explode(",", $_REQUEST['account_id']);
        }

        $account_list = $db->getAll("SELECT account_id, account_name, actg_type_id FROM `actg_accounts` WHERE account_id" . db_create_in($account_ids));
        foreach ($account_list as $row) {
            $accounts[$row['account_id']] = [
                'name' => $row['account_name'],
                'type' => $row['actg_type_id'],
            ];
        }

        $limit = 1000;
        include_once ROOT_PATH . 'includes/excel/PHPExcel.php';
        ini_set('memory_limit', '512M');
        set_time_limit(0);
        $objPHPExcel = new \PHPExcel();

        foreach (array_keys($accounts) as $key => $account_id) {
            if (empty($key)) {
                $objWorksheet = $objPHPExcel->setActiveSheetIndex($key);
            } else {
                $objWorksheet = $objPHPExcel->createSheet($key);
            }
            $objWorksheet->setTitle($accounts[$account_id]['name']);
            $objWorksheet
                ->setCellValue('A1', ecs_iconv(EC_CHARSET, 'UTF8', '銀行紀錄'))
                ->setCellValue('E1', ecs_iconv(EC_CHARSET, 'UTF8', 'Book Records'))
                ->setCellValue('J1', ecs_iconv(EC_CHARSET, 'UTF8', '會計紀錄'))
                ->setCellValue('O1', ecs_iconv(EC_CHARSET, 'UTF8', '錯誤'))
                ->setCellValue('A2', ecs_iconv(EC_CHARSET, 'UTF8', '交易編號'))
                ->setCellValue('B2', ecs_iconv(EC_CHARSET, 'UTF8', '交易日期'))
                ->setCellValue('C2', ecs_iconv(EC_CHARSET, 'UTF8', '備註'))
                ->setCellValue('D2', ecs_iconv(EC_CHARSET, 'UTF8', '金額'))
                ->setCellValue('E2', ecs_iconv(EC_CHARSET, 'UTF8', '交易編號'))
                ->setCellValue('F2', ecs_iconv(EC_CHARSET, 'UTF8', '交易月份'))
                ->setCellValue('G2', ecs_iconv(EC_CHARSET, 'UTF8', '備註'))
                ->setCellValue('H2', ecs_iconv(EC_CHARSET, 'UTF8', '分類'))
                ->setCellValue('I2', ecs_iconv(EC_CHARSET, 'UTF8', '金額'))
                ->setCellValue('J2', ecs_iconv(EC_CHARSET, 'UTF8', '交易編號'))
                ->setCellValue('K2', ecs_iconv(EC_CHARSET, 'UTF8', '交易月份'))
                ->setCellValue('L2', ecs_iconv(EC_CHARSET, 'UTF8', '備註'))
                ->setCellValue('M2', ecs_iconv(EC_CHARSET, 'UTF8', '分類'))
                ->setCellValue('N2', ecs_iconv(EC_CHARSET, 'UTF8', '金額'));

            $objWorksheet->mergeCells('A1:D1');
            $objWorksheet->mergeCells('E1:I1');
            $objWorksheet->mergeCells('J1:N1');
            $objWorksheet->mergeCells('O1:O2');
            $objWorksheet->freezePane('A3');
            $objWorksheet->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objWorksheet->getStyle('E1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objWorksheet->getStyle('J1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objWorksheet->getStyle('O:O')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objWorksheet->getStyle('O1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);

            $count = $db->getOne("SELECT COUNT(*) FROM `actg_bank_transactions` WHERE `date` BETWEEN '$start_date' AND '$end_date' AND `account_id` = $account_id");
            $index = 0;
            $bank_rec = [];
            while ($index * $limit < $count) {
                $sql = "SELECT bt.`rec_id`, DATE_FORMAT(bt.`date`, '%Y-%m-%d') as txn_date, bt.`remark` as b_remark, bt.`amount` as b_amount, txn_ids, t_month, t_remark , tt_name, t_amount, actg_txn_ids, at_month, at_remark , att_name, at_amount " .
                        "FROM `actg_bank_transactions` bt " .
                        "LEFT OUTER JOIN (" .
                            "SELECT t.`txn_group_id`, GROUP_CONCAT(DISTINCT CONCAT(t.`expense_year`, '-', t.`expense_month`)) as t_month, GROUP_CONCAT(DISTINCT t.`remark`) as t_remark, GROUP_CONCAT(DISTINCT CONCAT('ACC', t.`txn_id`))  as txn_ids, GROUP_CONCAT(DISTINCT tt.`txn_type_name`) as tt_name, SUM(t.`amount`) as t_amount " .
                            "FROM `actg_account_transactions` t " .
                            "LEFT JOIN (SELECT `txn_type_id`, `txn_type_name` FROM `actg_transaction_types`) tt ON t.`txn_type_id` = tt.`txn_type_id` " .
                            "WHERE t.`txn_type_id` != 69 AND t.`txn_group_id` != 0 AND t.`account_id` = $account_id " .
                            "GROUP BY t.`txn_group_id`" .
                        ") as gt ON gt.`txn_group_id` = bt.`txn_group_id` AND bt.`txn_group_id` != 0 " .
                        "LEFT OUTER JOIN (" .
                            "SELECT at.`txn_group_id`, GROUP_CONCAT(DISTINCT CONCAT(YEAR(at.`txn_date`), '-', MONTH(at.`txn_date`))) as at_month, GROUP_CONCAT(DISTINCT at.`remark`) as at_remark, GROUP_CONCAT(DISTINCT CONCAT('FIN', at.`actg_txn_id`))  as actg_txn_ids, GROUP_CONCAT(DISTINCT tt.`actg_type_name`) as att_name, SUM(at.`amount`) as at_amount " .
                            "FROM `actg_accounting_transactions` at " .
                            "LEFT JOIN (SELECT `actg_type_id`, `actg_type_name` FROM `actg_accounting_types`) tt ON at.`actg_type_id` = tt.`actg_type_id` " .
                            "WHERE at.`txn_group_id` != 0 AND at.`actg_type_id` = " . $accounts[$account_id]['type'] . " " .
                            "GROUP BY at.`txn_group_id`" .
                        ") as gat ON gat.`txn_group_id` = bt.`txn_group_id` AND bt.`txn_group_id` != 0 " .
                        "WHERE bt.`type` = 0 AND bt.`date` BETWEEN '$start_date' AND '$end_date' AND bt.`account_id` = $account_id " .
                        "ORDER BY bt.`rec_id` ASC " .
                        "LIMIT " . ($index * $limit) . ", $limit";
                $res = $db->getAll($sql);
                foreach ($res as $row) {
                    $bank_rec[] = $row;
                }
                $index++;
            }
            foreach ($bank_rec as $key => $row) {
                $i = $key + 3;
                $objWorksheet
                    ->setCellValue('A'.$i, $row['rec_id'])
                    ->setCellValue('B'.$i, $row['txn_date'])
                    ->setCellValue('C'.$i, $row['b_remark'])
                    ->setCellValue('D'.$i, $row['b_amount'])
                    ->setCellValue('E'.$i, $row['txn_ids'])
                    ->setCellValue('F'.$i, $row['t_month'])
                    ->setCellValue('G'.$i, $row['t_remark'])
                    ->setCellValue('H'.$i, $row['tt_name'])
                    ->setCellValue('I'.$i, $row['t_amount'])
                    ->setCellValue('J'.$i, $row['actg_txn_ids'])
                    ->setCellValue('K'.$i, $row['at_month'])
                    ->setCellValue('L'.$i, $row['at_remark'])
                    ->setCellValue('M'.$i, $row['att_name'])
                    ->setCellValue('N'.$i, $row['at_amount'])
                    ->setCellValue('O'.$i, '=IF(I'.$i.'+N'.$i.'<>0,IF(D'.$i.'-I'.$i.'-N'.$i.'=0,"","金額不符"),"未對數")');
            }
        }
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Bank_Audit_'.date('Y', strtotime($start_date)).'_'.date('Y', strtotime($end_date)).'_'.gmtime().'.xls"');
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->setPreCalculateFormulas(true);
        $objWriter->save('php://output');
    }

    /**
     * Auto match unmatched bank records to our transaction log
     *
     * @return void
     */
    public function matchTransactionsToBankRecords()
    {
        /**
         * Suggest link bank record to transaction with same amount
         *
         * @param float $txn          txn
         * @param array $bank_records bank record
         * @param int   $strict       strict search
         * @param bool  $actg         financial records
         *
         * @return array matched bank record ids
         */
        function inBankRecord($txn, $bank_records, $strict = 4, $actg = false)
        {
            $strict = 5 - $strict;
            foreach ($bank_records as $rec) {
                $min_date = strtotime($rec['date']) - $strict * 86400;
                $max_date = strtotime($rec['date']) + (2 + $strict) * 86400;
                $txn_date = strtotime($actg ? $txn['txn_date'] : $txn['complete_date']);
                $exact_match = bccomp($rec['amount'], $txn['amount'], 2) == 0;
                $inexact_match = abs(bcsub($rec['amount'], $txn['amount'])) < 0.1;
                if ((empty($txn['temp_sum']) && $exact_match) || bccomp($rec['amount'], $txn['temp_sum'], 2) == 0) {
                    if (($strict > 0 && $txn_date >= $min_date && $txn_date <= $max_date) || $strict == 5) {
                        return $rec['rec_id'];
                    }
                } elseif ($strict == 6) {
                    if ((empty($txn['temp_sum']) && $inexact_match) || abs(bcsub($rec['amount'], $txn['temp_sum'])) < 0.1) {
                        return $rec['rec_id'];
                    }
                }
            }
            return $result;
        }
        /**
         * Get all accounting month within ranges
         *
         * @param datetime $start min_date
         * @param datetime $end   max_date
         *
         * @return array
         */
        function accountingMonths($start, $end)
        {
            $year = date('Y', strtotime($start));
            $month = date('n', strtotime($start));
            $e_year = date('Y', strtotime($end));
            $e_month = date('n', strtotime($end));

            while ($year != $e_year && $month != $e_month) {
                $date[$year][] = $month;
                $month++;
                if ($month > 12) {
                    $year++;
                    $month -= 12;
                }
            }
            $date[$e_year][] = $e_month;
            return $date;
        }
        $account_id = empty($_REQUEST['account_id']) ? 0 : $_REQUEST['account_id'];
        if (empty($account_id)) {
            return array(
                'txns' => $txns,
                'banks' => $banks,
                'account_id' => $account_id
            );
        }

        $where = "WHERE txn_group_id = 0 AND account_id = $account_id AND type = 0 AND date > '2018-04-20 00:00:00'";

        //  Get all unmatched bank records
        $bank_sql = "SELECT * FROM `actg_bank_transactions` $where ORDER BY date ASC";
        $bank_records = $GLOBALS['db']->getAll($bank_sql);
        if (empty($bank_records)) {
            sys_msg('沒有可處理之銀行紀錄', 1, array(array('href'=>'bank_reconciliation.php?act=match', 'text'=>'返回上一頁')));
        } else {
            foreach ($bank_records as $key => $rec) {
                $bank_records[$key]['formatted_date'] = local_date('Y-m-d', strtotime($rec['date']));
                $bank_records[$key]['formatted_amount'] = $this->actg->money_format($rec['amount']);
            }
            //  Get relative transactions log
            $sql = "SELECT MIN(date) as min, MAX(date) as max FROM `actg_bank_transactions` $where";
            $date = $GLOBALS['db']->getRow($sql);
            $min_time = local_date("Y-m-1", strtotime($date['min']) - 5 * 86400);
            $max_time = local_date("Y-m-t 23:59:59", strtotime($date['max']) + 5 * 86400);
            $date = accountingMonths($min_time, $max_time);
            foreach ($date as $year => $month) {
                $where_d[] = "t.expense_year = $year AND t.expense_month " . db_create_in($month);
            }
            $txn_sql = "SELECT * FROM `actg_account_transactions` t " .
                        "LEFT JOIN `actg_accounts` as a ON t.account_id = a.account_id " .
                        "LEFT JOIN `actg_payment_types` as p ON t.payment_type_id = p.payment_type_id " .
                        "LEFT JOIN (SELECT related_txn_id as reversed FROM `actg_account_transactions` WHERE related_txn_id IS NOT NULL) rt ON rt.reversed = t.txn_id " .
                        "LEFT JOIN (SELECT txn_group_id as matched FROM `actg_bank_transactions` WHERE txn_group_id != 0) bt ON bt.matched = t.txn_group_id " .
                        "WHERE t.account_id = $account_id " .
                        "AND ((" . implode(") OR (", $where_d) . ") OR complete_date BETWEEN '$min_time' AND '$max_time') " .
                        "AND t.remark NOT LIKE '%Reverse transaction%' " .
                        "AND rt.reversed IS NULL " .
                        "AND t.txn_type_id != 69 " .
                        "AND t.complete_date > '2018-04-30 00:00:00' " .
                        "AND bt.matched IS NULL " .
                        "ORDER BY UNIX_TIMESTAMP(complete_date) ASC";
            $txns = $GLOBALS['db']->getAll($txn_sql);
            $actg_txn_sql = "SELECT * FROM `actg_accounting_transactions` t " .
                            "LEFT JOIN `actg_accounts` as a ON a.actg_type_id = t.actg_type_id " .
                            "LEFT JOIN (SELECT txn_group_id as matched FROM `actg_bank_transactions` WHERE txn_group_id != 0) bt ON bt.matched = t.txn_group_id " .
                            "WHERE a.account_id = $account_id " .
                            "AND t.txn_date BETWEEN '$min_time' AND '$max_time' " .
                            "AND t.reversed_txn_id IS NULL AND t.reversed = 0 " .
                            "AND posted != -1 " .
                            "AND bt.matched IS NULL " .
                            "ORDER BY t.txn_date ASC";
            $actg_txns = $GLOBALS['db']->getAll($actg_txn_sql);

            $all_txn = [];
            foreach ($txns as $key => $txn) {
                $txn['formatted_date'] = local_date($GLOBALS['_CFG']['time_format'], strtotime($txn['complete_date']));
                $txn['formatted_amount'] = $this->actg->money_format($txn['amount'], $txn['currency_format']);
                $txn['formatted_txn_id'] = "ACC" . $txn['txn_id'];
                $all_txn[$txn['txn_group_id']]['t'][] = $txn;
            }
            foreach ($actg_txns as $key => $txn) {
                $txn['formatted_date'] = local_date($GLOBALS['_CFG']['time_format'], strtotime($txn['txn_date']));
                $txn['formatted_amount'] = $this->actg->money_format($txn['amount'], $txn['currency_format']);
                $txn['formatted_txn_id'] = "FIN" . $txn['actg_txn_id'];
                $all_txn[$txn['txn_group_id']]['a'][] = $txn;
            }
            $result_txns = [];
            foreach ($all_txn as $txn_group_id => $txns) {
                if (empty($txn_group_id)) {
                    foreach ($txns['t'] as $txn) {
                        $result_txns[] = $txn;
                    }
                    foreach ($txns['a'] as $txn) {
                        $result_txns[] = $txn;
                    }
                } else {
                    $temp_ids = [];
                    $temp_actg_ids = [];
                    $sum = 0;
                    $group_txn = [];
                    foreach ($txns['t'] as $txn) {
                        if (empty($group_txn)) {
                            $group_txn = $txn;
                            $group_txn['group_txns']['count'] = 0;
                            $group_txn['group_txns']['txns'] = [];
                        } else {
                            $group_txn['group_txns']['txns'][] = $txn;
                        }
                        $group_txn['group_txns']['count']++;
                        $temp_ids[] = $txn['formatted_txn_id'];
                        $sum = bcadd($sum, $txn['amount'], 2);
                    }
                    foreach ($txns['a'] as $key => $txn) {
                        if (empty($group_txn)) {
                            $group_txn = $txn;
                            $group_txn['group_txns']['txns'] = [];
                        } else {
                            $group_txn['group_txns']['txns'][] = $txn;
                        }
                        $group_txn['group_txns']['count']++;
                        $temp_actg_ids[] = $txn['formatted_txn_id'];
                        $sum = bcadd($sum, $txn['amount'], 2);
                    }
                    $group_txn['group_txns']['count'] = count($group_txn['group_txns']['txns']) + 1;
                    $group_txn['group_txns']['temp_ids'] = implode(' / ', $temp_ids);
                    $group_txn['group_txns']['temp_actg_ids'] = implode(' / ', $temp_actg_ids);
                    $group_txn['temp_sum'] = $sum;
                    $group_txn['temp_ids'] = $group_txn['group_txns']['temp_ids'] . (!empty($temp_ids) && !empty($temp_actg_ids) ? " | " : "") . $group_txn['group_txns']['temp_actg_ids'];
                    $result_txns[] = $group_txn;
                }
            }
            $strict = 4;
            while ($strict != -2) {
                foreach ($result_txns as $key => $txn) {
                    if (empty($txn['bank_rec'])) {
                        $rec_id = inBankRecord($txn, $bank_records, $strict, (empty($txn['group_txns']) && !empty($txn['actg_txn_id']) ? true : false));
                        if (!empty($rec_id)) {
                            foreach ($bank_records as $index => $rec) {
                                if ($rec_id == $rec['rec_id']) {
                                    $result_txns[$key]['bank_rec'] = $rec;
                                    unset($bank_records[$index]);
                                }
                            }
                        }
                    }
                }
                $strict--;
            }
        }
        foreach ($bank_records as $rec) {
            $banks[] = $rec;
        }
        return array(
            'result_txns' => $result_txns,
            'accounts' => $this->actg->getAccountBankCodes(),
            'banks' => $banks,
            'account_id' => $account_id
        );
    }

    /**
     * Link bank record with transaction
     *
     * @param int $txn_id transaction id (only for auto insert)
     * @param int $rec_id bank record id (only for auto insert)
     * 
     * @return json/void
     */
    public function linkBankTransactionRecord($temp_txn_id = 0, $rec_id = 0)
    {
        $temp_txn_id = empty($temp_txn_id) ? (empty($_POST['txn_id']) ? 0 : $_POST['txn_id']) : $temp_txn_id;
        $rec_id = empty($rec_id) ? (empty($_POST['rec_id']) ? 0 : explode(',', $_POST['rec_id'])) : $rec_id;
        if (preg_match("/FIN(\d+)/", $temp_txn_id, $actg_txn_id)) {
            $actg_txn_id = $actg_txn_id[1];
            $txn_group_id = $GLOBALS['db']->getOne("SELECT txn_group_id FROM `actg_accounting_transactions` WHERE actg_txn_id = $actg_txn_id");
        } elseif (preg_match("/ACC(\d+)/", $temp_txn_id, $txn_id)) {
            $txn_id = $txn_id[1];
            $txn_group_id = $GLOBALS['db']->getOne("SELECT txn_group_id FROM `actg_account_transactions` WHERE txn_id = $txn_id");
        }
        $GLOBALS['db']->query('START TRANSACTION');
        if (empty($txn_group_id)) {
            $new_txn_group_id = self::getNewTxnGroupId();
            $GLOBALS['db']->query("UPDATE `actg_bank_transactions` SET txn_group_id = $new_txn_group_id WHERE rec_id " . db_create_in($rec_id));
            if (!empty($actg_txn_id)) {
                $GLOBALS['db']->query("UPDATE `actg_accounting_transactions` SET txn_group_id = $new_txn_group_id WHERE actg_txn_id = $actg_txn_id");
            }
            if (!empty($txn_id)) {
                $GLOBALS['db']->query("UPDATE `actg_account_transactions` SET txn_group_id = $new_txn_group_id WHERE txn_id = $txn_id");
            }
        } else {
            $GLOBALS['db']->query("UPDATE `actg_bank_transactions` SET txn_group_id = $txn_group_id WHERE rec_id " . db_create_in($rec_id));
        }
        $GLOBALS['db']->query('COMMIT');
        if (!empty($_POST['txn_id']) && !empty($_POST['rec_id'])) {
            make_json_result(true);
        }
    }

    /**
     * Transaction change account
     *
     * @return void
     */
    public function changeBelongingAccount()
    {
        $temp_txn_id = empty($_POST['txn_id']) ? 0 : $_POST['txn_id'];
        $account_id = empty($_POST['new_account_id']) ? 0 : $_POST['new_account_id'];
        if (preg_match("/FIN(\d+)/", $temp_txn_id, $actg_txn_id)) {
            $actg_txn_id = $actg_txn_id[1];
        } elseif (preg_match("/ACC(\d+)/", $temp_txn_id, $txn_id)) {
            $txn_id = $txn_id[1];
        } else {
            make_json_error("請選擇交易紀錄");
        }
        if (empty($account_id)) {
            make_json_error("請選擇目標帳戶");
        }
        $start = microtime(true);
        if (!empty($txn_id)) {
            $txn = $this->actg->getTransaction(array('txn_id' => $txn_id));
        } elseif (!empty($actg_txn_id)) {
            $txn = $GLOBALS['db']->getRow("SELECT * FROM `actg_accounting_transactions` WHERE actg_txn_id = $actg_txn_id");
        }
        $merged = !empty($txn['txn_group_id']);
        $sql = "SELECT 1 FROM `actg_bank_transactions` WHERE txn_group_id != 0 AND txn_group_id = $txn[txn_group_id]";
        $linked = $GLOBALS['db']->getOne($sql);
        if ($txn['txn_type_id'] == 69 || $linked) {
            make_json_error("無法轉移此交易");
        }
        if ($merged && empty($_POST['confirm'])) {
            make_json_error("confirm");
        }

        if ($merged) {
            $txn_ids = $GLOBALS['db']->getCol("SELECT txn_id FROM `actg_account_transactions` WHERE txn_group_id = $txn[txn_group_id] AND txn_type_id != 69");
            $actg_txn_ids = $GLOBALS['db']->getCol("SELECT actg_txn_id FROM `actg_accounting_transactions` WHERE txn_group_id = $txn[txn_group_id] AND posted != -1 AND reversed = 0");
        } else {
            $txn_ids = [$txn_id];
            $actg_txn_ids = [$actg_txn_id];
        }

        $account = $this->actg->getAccount(array('account_id' => $account_id));

        foreach ($txn_ids as $txn_id) {
            if (empty($txn_id)) continue;
            $txn = $this->actg->getTransaction(array('txn_id' => $txn_id));

            $datetime = $this->actg->dbdate();

            $withdraw['add_date'] = $datetime;
            $withdraw['complete_date'] = $datetime;
            $withdraw['bank_date'] = $txn['bank_date'];
            $withdraw['currency_code'] = $txn['currency_code'];
            $withdraw['amount'] = bcmul($txn['amount'], -1);
            $withdraw['account_id'] = $txn['account_id'];
            $withdraw['expense_year'] = $txn['expense_year'];
            $withdraw['expense_month'] = $txn['expense_month'];
            $withdraw['txn_group_id'] = $txn['txn_group_id'];
            $withdraw['txn_type_id'] = 69;    // Account Transfer
            $withdraw['payment_type_id'] = 2;   // 轉帳
            $withdraw['remark'] = mysql_escape_string("轉移至 $account[account_name]: $txn[remark]");

            $deposit['add_date'] = $datetime;
            $deposit['complete_date'] = $txn['complete_date'];
            $deposit['bank_date'] = $txn['bank_date'];
            $deposit['currency_code'] = $txn['currency_code'];
            $deposit['amount'] = $txn['amount'];
            $deposit['account_id'] = $account_id;
            $deposit['expense_year'] = $txn['expense_year'];
            $deposit['expense_month'] = $txn['expense_month'];
            $deposit['txn_group_id'] = $txn['txn_group_id'];
            $deposit['txn_type_id'] = $txn['txn_type_id'];
            $deposit['payment_type_id'] = $txn['payment_type_id'];
            $deposit['remark'] = mysql_escape_string("轉移自 $txn[account_name]: $txn[remark]");

            $this->actg->updateTransaction(
                array(
                    'txn_id' => $txn_id,
                    'txn_type_id' => 69
                )
            );
            $this->actg->addTransaction($withdraw);
            $this->actg->addTransaction($deposit);
        }
        $processed_actg_txn_ids = [];
        foreach ($actg_txn_ids as $actg_txn_id) {
            if (empty($actg_txn_id)) continue;
            $txn = $GLOBALS['db']->getRow("SELECT * FROM `actg_accounting_transactions` WHERE actg_txn_id = $actg_txn_id");
            // Exclude rejected transactions
            if ($txn['posted'] != -1) {
                $new_actg_type_id = self::getAccountingTypeByAccountId($account_id, 'actg_type_id');
                if ($txn['posted'] == 1) {
                    $parent_actg_txn_id = $actg_txn_id;
                    if (!empty($txn['related_txn_id'])) {
                        $parent_actg_txn_id = $txn['related_txn_id'];
                    }
                    $txn_group = $GLOBALS['db']->getAll(
                        "SELECT txn.*, type.actg_type_name FROM `actg_accounting_transactions` txn " .
                        "LEFT JOIN `actg_accounting_types` type ON type.actg_type_id = txn.actg_type_id " .
                        "WHERE actg_txn_id = $parent_actg_txn_id OR related_txn_id = $parent_actg_txn_id " .
                        "ORDER BY actg_txn_id ASC"
                    );
                    self::reverseAccountingTransaction(['actg_txn_id' => $parent_actg_txn_id]);
                    $related_txn_id = 0;
                    foreach ($txn_group as $curr_txn) {
                        if (in_array($curr_txn['actg_txn_id'], $processed_actg_txn_ids)) {
                            continue;
                        } else {
                            $processed_actg_txn_ids[] = $curr_txn['actg_txn_id'];
                        }

                        $new_txn = [];
                        foreach ($curr_txn as $key => $val) {
                            if (!in_array($key, ['actg_txn_id', 'related_txn_id', 'posted', 'created_at', 'created_by', 'actg_type_name'])) {
                                $new_txn[$key] = $val;
                            }
                        }
                        if ($curr_txn['actg_txn_id'] == $actg_txn_id) {
                            $new_txn['actg_type_id'] = $new_actg_type_id;
                            $new_txn['remark'] = "轉移自 $curr_txn[actg_type_name]: $curr_txn[remark]";
                        }
                        $new_txn['created_by'] = empty($_SESSION['admin_id']) ? $curr_txn['created_by'] : $_SESSION['admin_id'];

                        if (!empty($related_txn_id)) {
                            $new_txn['related_txn_id'] = $related_txn_id;
                            self::insertTransaction($new_txn);
                        } else {
                            $related_txn_id = self::insertTransaction($new_txn);
                        }
                    }
                } else {
                    $GLOBALS['db']->query("UPDATE `actg_accounting_transactions` SET actg_type_id = $new_actg_type_id WHERE actg_txn_id = $actg_txn_id");
                }
            }
        }
        make_json_response(true);
    }

    /**
     * Set transaction group to multiple transactions
     *
     * @return void
     */
    public function mergeTransaction()
    {
        $temp_txn_ids = empty($_POST['txn_ids']) ? "" : $_POST['txn_ids'];
        $splitted = explode(",", $temp_txn_ids);
        if (empty($temp_txn_ids) || count($splitted) < 2) {
            make_json_error("請選擇最少2筆交易");
        }

        foreach ($splitted as $temp_id) {
            if (preg_match("/FIN(\d+)/", $temp_id, $actg_txn_id)) {
                $actg_txn_ids[] = $actg_txn_id[1];
            } elseif (preg_match("/ACC(\d+)/", $temp_id, $txn_id)) {
                $txn_ids[] = $txn_id[1];
            }
        }
        $txn_ids = implode(",", $txn_ids);
        $actg_txn_ids = implode(",", $actg_txn_ids);

        if (!empty($txn_ids)) {
            $txn_group_ids = $GLOBALS['db']->getCol("SELECT DISTINCT txn_group_id FROM `actg_account_transactions` WHERE txn_id IN ($txn_ids) AND txn_group_id != 0");
        }

        if (!empty($actg_txn_ids)) {
            $actg_txn_group_ids = $GLOBALS['db']->getCol("SELECT DISTINCT txn_group_id FROM `actg_accounting_transactions` WHERE actg_txn_id IN ($actg_txn_ids) AND txn_group_id != 0");
        }

        if (count($txn_group_ids) + count($actg_txn_group_ids) <= 1) {
            $txn_group_id = 0;
            if (empty($txn_group_ids) && empty($actg_txn_group_ids)) {
                $txn_group_id = self::getNewTxnGroupId();
            } elseif (empty($txn_group_ids)) {
                $txn_group_id = $actg_txn_group_ids[0];
            } elseif (empty($actg_txn_group_ids)) {
                $txn_group_id = $txn_group_ids[0];
            }

            if (!empty($txn_ids)) {
                $GLOBALS['db']->query("UPDATE `actg_account_transactions` SET txn_group_id = $txn_group_id WHERE txn_id IN ($txn_ids)");
            }
            if (!empty($txn_group_id)) {
                $GLOBALS['db']->query("UPDATE `actg_accounting_transactions` SET txn_group_id = $txn_group_id WHERE actg_txn_id IN ($actg_txn_ids)");
            }
        } else {
            make_json_error("無法合併已合併之交易");
        }
        make_json_response(true);
    }

    /**
     * Return possible bank records from other accounts
     *
     * @return void
     */
    public function getPossibleBankRecords()
    {
        $temp_txn_id = empty($_POST['txn_id']) ? "" : $_POST['txn_id'];
        $account_id = empty($_POST['account_id']) ? "" : $_POST['account_id'];

        if (empty($temp_txn_id) || empty($account_id)) {
            make_json_response([]);
        }

        if (preg_match("/FIN(\d+)/", $temp_txn_id, $txn_id)) {
            $txn_id = $txn_id[1];
            $table = 'actg_accounting_transactions';
            $txn_column = 'actg_txn_id';
            $date_column = 'txn_date';
        } elseif (preg_match("/ACC(\d+)/", $temp_txn_id, $txn_id)) {
            $txn_id = $txn_id[1];
            $table = 'actg_account_transactions';
            $txn_column = 'txn_id';
            $date_column = 'complete_date';
        } else {
            make_json_response([]);
        }

        $txn = $GLOBALS['db']->getRow("SELECT * FROM `$table` WHERE $txn_column = $txn_id");

        if (empty($txn)) {
            make_json_response([]);
        }
        if ($txn['txn_group_id'] != 0) {
            $group_amount = bcadd($group_amount, $GLOBALS['db']->getOne("SELECT SUM(amount) FROM `actg_accounting_transactions` WHERE txn_group_id = $txn[txn_group_id] AND posted != -1 AND reversed = 0"), 2);
            $group_amount = bcadd($group_amount, $GLOBALS['db']->getOne("SELECT SUM(amount) FROM `actg_account_transactions` WHERE txn_group_id = $txn[txn_group_id]"), 2);
        }

        $sql = "SELECT *, DATEDIFF('$txn[$date_column]', date) as diff FROM `actg_bank_transactions` bt" .
                " LEFT JOIN `actg_accounts` a ON a.account_id = bt.account_id" .
                " WHERE amount = " . (empty($group_amount) ? "$txn[amount]" : "$group_amount") .
                " AND date > '2018-04-20 00:00:00'" .
                " AND txn_group_id = 0 AND bt.account_id != $account_id ORDER BY ABS(diff), diff";
        $records = $GLOBALS['db']->getAll($sql);
        foreach ($records as $key => $row) {
            $records[$key]['formatted_date'] = local_date('Y-m-d', strtotime($row['date']));
            $records[$key]['formatted_amount'] = price_format(abs($row['amount']));
        }
        make_json_response($records);
    }

    /**
     * Return possible transaction records from other accounts
     *
     * @return void
     */
    public function getPossibleTxnRecords()
    {
        $amount = empty($_POST['amount']) ? "" : $_POST['amount'];
        $date = empty($_POST['date']) ? local_date("Y-m-d") : $_POST['date'];
        $account_id = empty($_POST['account_id']) ? "" : $_POST['account_id'];

        if (empty($amount) || empty($account_id)) {
            make_json_response([]);
        }

        $start_date = local_date("Y-m-d", strtotime($date) - 5 * 86400);
        $end_date = local_date("Y-m-d", strtotime($date) + 5 * 86400);

        $sql = "SELECT * FROM `actg_account_transactions` t" .
                " LEFT JOIN `actg_accounts` a ON a.account_id = t.account_id" .
                " LEFT JOIN (SELECT related_txn_id as reversed FROM `actg_account_transactions` WHERE related_txn_id IS NOT NULL) rt ON rt.reversed = t.txn_id" .
                " WHERE txn_group_id = 0 AND amount = $amount AND t.account_id != $account_id" .
                " AND rt.reversed IS NULL " .
                " AND t.remark NOT LIKE '%Reverse transaction%' AND txn_type_id != 69" .
                " AND complete_date BETWEEN '$start_date' AND '$end_date'" .
                " AND complete_date > '2018-04-30 00:00:00' ORDER BY complete_date DESC";
        $txns = $GLOBALS['db']->getAll($sql);
        $sql = "SELECT account_name, txn_group_id, SUM(amount) as group_amount FROM `actg_account_transactions` t" .
                " LEFT JOIN `actg_accounts` a ON a.account_id = t.account_id" .
                " LEFT JOIN (SELECT txn_group_id as matched FROM `actg_bank_transactions` WHERE txn_group_id != 0) bt ON bt.matched = t.txn_group_id" .
                " WHERE txn_group_id != 0 AND t.account_id != $account_id" .
                " AND bt.matched IS NULL" .
                " GROUP BY txn_group_id HAVING group_amount = $amount";
        $group_txns = $GLOBALS['db']->getAll($sql);
        foreach ($txns as $key => $row) {
            $txns[$key]['formatted_date'] = local_date('Y-m-d', strtotime($row['complete_date']));
            $txns[$key]['formatted_amount'] = price_format(abs($row['amount']));
        }
        foreach ($group_txns as $key => $row) {
            $temp_txns = $GLOBALS['db']->getAll("SELECT * FROM `actg_account_transactions` WHERE txn_group_id = $row[txn_group_id]");
            foreach ($temp_txns as $key2 => $row2) {
                $temp_txns[$key2]['formatted_date'] = local_date('Y-m-d', strtotime($row2['complete_date']));
                $temp_txns[$key2]['formatted_amount'] = price_format(abs($row2['amount']));
            }
            $group_txns[$key]['txns'] = $temp_txns;
        }

        $sql = "SELECT * FROM `actg_accounting_transactions` t" .
                " LEFT JOIN `actg_accounts` a ON a.actg_type_id = t.actg_type_id" .
                " WHERE txn_group_id = 0 AND amount = $amount AND a.account_id != $account_id" .
                " AND t.remark NOT LIKE '%Reverse transaction%' AND posted != -1" .
                " AND txn_date BETWEEN '$start_date' AND '$end_date'" .
                " AND txn_date > '2018-04-30 00:00:00' ORDER BY txn_date DESC";
        $actg_txns = $GLOBALS['db']->getAll($sql);
        $sql = "SELECT account_name, txn_group_id, SUM(amount) as group_amount FROM `actg_accounting_transactions` t" .
                " LEFT JOIN `actg_accounts` a ON a.actg_type_id = t.actg_type_id" .
                " LEFT JOIN (SELECT txn_group_id as matched FROM `actg_bank_transactions` WHERE txn_group_id != 0) bt ON bt.matched = t.txn_group_id" .
                " WHERE txn_group_id != 0 AND a.account_id != $account_id" .
                " AND bt.matched IS NULL" .
                " GROUP BY txn_group_id HAVING group_amount = $amount";
        $actg_group_txns = $GLOBALS['db']->getAll($sql);
        foreach ($actg_txns as $key => $row) {
            $actg_txns[$key]['formatted_date'] = local_date('Y-m-d', strtotime($row['txn_date']));
            $actg_txns[$key]['formatted_amount'] = price_format(abs($row['amount']));
        }
        foreach ($actg_group_txns as $key => $row) {
            $temp_txns = $GLOBALS['db']->getAll("SELECT * FROM `actg_accounting_transactions` WHERE txn_group_id = $row[txn_group_id]");
            foreach ($temp_txns as $key2 => $row2) {
                $temp_txns[$key2]['formatted_date'] = local_date('Y-m-d', strtotime($row2['txn_date']));
                $temp_txns[$key2]['formatted_amount'] = price_format(abs($row2['amount']));
            }
            $actg_group_txns[$key]['txns'] = $temp_txns;
        }
        make_json_response(array($txns, $group_txns, $actg_txns, $actg_group_txns));
    }

    public function unbindBankReconciliationEntries($txn_group_ids) 
    {
        if (empty($txn_group_ids)) {
            make_json_error([]);
        }

        $ids = explode(',', $txn_group_ids);
        
        foreach($ids as $id) {
            $this->unlinkBankTransactionRecord($id);
        }

        make_json_response(true);
    }

    /**
     * Get all bank balance
     * 
     * @param int $account_id account id
     * 
     * @return array
     */
    function getCurrentBankBalance($account_id = 0)
    {
        $sql = "SELECT account_id, amount, date FROM `actg_bank_transactions` " .
                "WHERE rec_id IN (" .
                    "SELECT MAX(rec_id) FROM `actg_bank_transactions` WHERE type = 2 " .
                    (empty($account_id) ? "" : "AND account_id = $account_id ") .
                    "GROUP BY account_id" .
                ")";
        return $GLOBALS['db']->getAll($sql);
    }

    function get_accounts()
    {
        global $actg, $db;
        
        /* 排序参数 */
        $_REQUEST['sort_by']      = empty($_REQUEST['sort_by']) ? 'account_id' : trim($_REQUEST['sort_by']);
        $_REQUEST['sort_order']   = empty($_REQUEST['sort_order']) ? 'ASC' : trim($_REQUEST['sort_order']);
        
        /* 查询数据的条件 */
        $where = " 1 ";
        
        $sql = "SELECT count(*) FROM `actg_accounts` as a WHERE " . $where;
        $record_count = $db->getOne($sql);

        $opt = array();
        $opt['orderby'] = $_REQUEST['sort_by'] . ' ' . $_REQUEST['sort_order'];

        /* 查询 */
        $data = $actg->getAccounts($where, $opt);

        // // get book record from accounting entries.
        // foreach ($data as &$account_item) {
        //     $sql = "select sum(amount) as amount from actg_accounting_transactions where actg_type_id = '".$account_item['actg_type_id']."' ";
        //     $acc_tran_total = $db->getOne($sql);
        //     $account_item['account_balance'] = $acc_tran_total;
        // }
       
        $bank_opt = $actg->getBankCodes();
        $cfg = load_config(true);

        $balances = $this->getCurrentBankBalance();
        foreach ($balances as $rec) {
            $balance[$rec['account_id']] = [
                'amount'    => $rec['amount'],
                'date'      => $rec['date']
            ];
        }

        foreach($data as $key => $row)
        {
            if($cfg['default_refund_account'] == $row['account_id'])
            {
                $data[$key]['is_default'] = 1;
            }
            $data[$key]['account_balance_formatted'] = $actg->money_format($row['account_balance']);//, $row['currency_format']);
            $data[$key]['audit_date_formatted'] = local_date($GLOBALS['_CFG']['time_format'], strtotime($row['audit_date']));
            $data[$key]['bank_balance_formatted'] = $actg->money_format($balance[$row['account_id']]['amount']);
            $data[$key]['bank_date_formatted'] = local_date("Y-m-d", strtotime($balance[$row['account_id']]['date']));
            $data[$key]['_action'] = $this->generateCrudActionBtn($row['account_id'], 'account', null, ['list']);
        }


        $arr = array(
            'data' => $data,
            'record_count' => $record_count
        );
        return $arr;
    }

    public function ajaxQueryAction($list, $totalCount, $action_btn = ['edit', 'remove'], $extraData = [])
    {
        if($_REQUEST['act'] == 'query_summary') {
            $this->tableName = 'actg_accounts';
            $list = $this->get_accounts();
        }
        $extra_data = empty($list['extra_data']) ? [] : $list['extra_data'];
        parent::ajaxQueryAction($list['data'], $list['record_count'], false, $extra_data);
    }

    public function ajaxEditAction($tablename = 'actg_accounts')
    {
        global $ecs, $db, $actg;
        if($_REQUEST['action'] == 'summary') {
            $this->tableName = 'actg_accounts';

            if($_REQUEST['col'] == 'is_default') {
                $id     = intval($_POST['id']);
                $exc = new \exchange($ecs->table("shop_config"), $db, 'id', 'code');
                $exc->edit("value='$id'", $db->getOne("SELECT id FROM ".$ecs->table("shop_config")." WHERE code = 'default_refund_account'"));
                make_json_result(1, '', ['reload'=>true]);
            } else if($_REQUEST['col'] == 'bank_id') {

                check_authz_json('sale_order_stats');
                $bank_codes = $actg->getBankCodes();
                $bank_options = [];
                foreach($bank_codes as $key=>$row) {
                    $bank_options[$row['bank_id']] = $row['bank_code'];
                }
                $account_id    = intval($_POST['id']);
                $bank_id       = intval($_POST['value']);

                $param = array(
                    'account_id' => $account_id,
                    'bank_id' => $bank_id
                );
                
                if ($actg->updateAccount($param))
                {
                    make_json_result(stripslashes($bank_options[$bank_id]));
                }
                else
                {
                    make_json_error('更改CSV模版失敗');
                }
            }
             else {
                parent::ajaxEditAction('actg_accounts');
            }
        }
    }

    public function getAccountingTypesList()
    {
        include_once ROOT_PATH . '/languages/' . $_CFG['lang'] . '/admin/accounting_system.php';
        $types = self::getAccountingTypes();
        foreach ($types as $key => $row) {
            $actions = [];
            if (intval($row['actg_type_code']) % 100 == 0) {
                $actions = [
                    'category' => [
                        'link'  => "javascript:insertNewCategory($row[actg_type_id])",
                        'title' => '新增子類別',
                        'label' => '子類別',
                        'btn_class' => 'btn btn-primary',
                        'icon_class' => 'fa fa-plus',
                    ],
                    'item' => [
                        'link'  => "javascript:insertNewCategory($row[actg_type_id], true)",
                        'title' => '新增子帳戶',
                        'label' => '子帳戶',
                        'btn_class' => 'btn btn-info',
                        'icon_class' => 'fa fa-plus',
                    ],
                ];
            }
            $types[$key]['_action'] = self::generateCrudActionBtn($row['actg_type_id'], 'id', null, [], $actions);
        }
        parent::ajaxQueryAction($types, count($types), false);
    }
    public function getAccountingSuppliersList()
    {
        global $db, $ecs;
        return $db->getAll("SELECT supplier_id, code, name FROM " . $ecs->table('erp_supplier'));
    }
    public function getAccountingWholesaleCustomerList()
    {
        global $db, $ecs;
        return $db->getAll("SELECT wsid, company FROM " . $ecs->table('wholesale_customer'));
    }
    public function getAccountingSalesAgentList()
    {
        global $db, $ecs;
        return $db->getAll("SELECT sa_id, name FROM " . $ecs->table('sales_agent'));
    }
    function getAccountingTypes($base_type_id = 0, $list = true)
    {
        global $db, $ecs;
        $types = [];
        $sql = "SELECT t.*, a.currency_code FROM `actg_accounting_types` t " .
                "LEFT JOIN (SELECT actg_type_id, currency_code FROM `actg_accounts`) a ON a.actg_type_id = t.actg_type_id " .
                "WHERE parent_type_id = '$base_type_id'" . ($list ? "" : " AND enabled = 1") . " ORDER BY actg_type_code ASC";
        $base_types = $db->getAll($sql);
        foreach ($base_types as $type) {
            if ($list) {
                $type['parent_id'] = $base_type_id;
                $types[] = $type;
                $types = array_merge($types, self::getAccountingTypes($type['actg_type_id']));
            } else {
                $type['children'] = self::getAccountingTypes($type['actg_type_id'], false);
                $types[] = $type;
            }
        }
        return $types;
    }

    function getAccountingType($param, $col = '')
    {
        global $db;
        if (empty($col)) {
            return $db->getRow(
                "SELECT * FROM `actg_accounting_types` WHERE " . self::build_where($param)
            );
        } else {
            return $db->getOne(
                "SELECT $col FROM `actg_accounting_types` WHERE " . self::build_where($param)
            );
        }
        return $res;
    }

    function getAccountingTypeByAccountId($account_id, $col ='')
    {
        $actg_type_id = $GLOBALS['db']->getOne("SELECT actg_type_id FROM `actg_accounts` WHERE account_id = '$account_id'");
        if ($col == 'actg_type_id') return $actg_type_id;
        return self::getAccountingType([
            'actg_type_id'  => $actg_type_id,
            'enabled'       => 1,
        ], $col);
    }

    public function insertAccountingTypes()
    {
        global $db, $ecs;
        include_once ROOT_PATH . '/languages/' . $_CFG['lang'] . '/admin/accounting_system.php';

        $parent_id = empty($_REQUEST['parent_id']) ? 0 : intval($_REQUEST['parent_id']);
        $name = empty($_REQUEST['actg_type_name']) ? '' : trim($_REQUEST['actg_type_name']);
        $code = empty($_REQUEST['actg_type_code']) ? '' : trim($_REQUEST['actg_type_code']);
        if (empty($code)) {
            sys_msg($GLOBALS['_LANG']['insert_type_empty_code'], 1);
        }
        if (empty($name)) {
            sys_msg($GLOBALS['_LANG']['insert_type_empty_name'], 1);
        }
        if ($db->getOne("SELECT 1 FROM `actg_accounting_types` WHERE actg_type_code = '$code'")) {
            sys_msg($GLOBALS['_LANG']['insert_type_existing_code'], 1);
        }
        if (!empty($parent_id)) {
            if (!$db->getOne("SELECT 1 FROM `actg_accounting_types` WHERE actg_type_code LIKE '" . $code[0] . "%' AND actg_type_id = $parent_id")) {
                sys_msg($GLOBALS['_LANG']['insert_type_invalid_code'], 1);
            }
        }
        $db->query("INSERT INTO `actg_accounting_types` (actg_type_name, actg_type_code, parent_type_id) VALUES ('$name', '$code', '$parent_id')");
        sys_msg($GLOBALS['_LANG']['insert_type_success'], 0, [['text' => $GLOBALS['_LANG']['accounting_types'], 'href' => 'accounting_system_type.php?act=list']]);
    }

    function getNextTypeCode($parent_id = 0, $ajax = true)
    {
        global $db, $ecs;

        $parent_id = empty($_REQUEST['actg_type_id']) ? $parent_id : intval($_REQUEST['actg_type_id']);
        $used_codes = $db->getCol("SELECT actg_type_code FROM `actg_accounting_types` WHERE parent_type_id = $parent_id ORDER BY actg_type_code ASC");

        $res = [];
        $valid_code = [];
        if ($parent_id == 0) {
            $i = 1;
            while ($i < 10) {
                if (!in_array($i . "0000", $used_codes)) {
                    $valid_code[] = $i . "0000";
                }
                $i++;
            }
        } else {
            $type = $db->getRow("SELECT * FROM `actg_accounting_types` WHERE actg_type_id = $parent_id");
            $res['code'] = $type['actg_type_code'];
            $res['name'] = $type['actg_type_name'];
            $prefix = '';
            $i = 1;
            foreach (str_split($type['actg_type_code']) as $code) {
                if ($code != '0') {
                    $prefix .= $code;
                }
            }
            $start = intval($type['actg_type_code']) + 1;
            $end = intval($type['actg_type_code']) + pow(10, 5 - strlen($prefix));

            if ($end - $start > count($used_codes)) {
                $res['start'] = $start;
                $res['end'] = $end;
            }
        }
        if (count($valid_code) == 0 && empty($res['start']) && empty($res['end'])) {
            if ($ajax) {
                make_json_error("Category max number exceed");
            } else {
                return false;
            }
        }
        $res['vcode'] = $valid_code;
        $res['ucode'] = $used_codes;
        if ($ajax) {
            make_json_response($res);
        } else {
            return $res;
        }
    }

    // Create new account
    function insertNewAccounts($param)
    {
        global $db;

        if ($param['is_bank']) {
            $parent_id = self::getAccountingType(['actg_type_code' => '11000', 'enabled' => 1], 'actg_type_id'); // Bank and Cash
        } else {
            $parent_id = self::getAccountingType(['actg_type_code' => '13200', 'enabled' => 1], 'actg_type_id'); // Other Receivables
        }

        $next_type_code = self::getNextTypeCode($parent_id, false);
        if ($next_type_code) {
            $last_type_code = $next_type_code['code'];
            if (!empty($next_type_code['ucode'])) {
                $last_type_code = $next_type_code['ucode'][count($next_type_code['ucode']) - 1];
            }
            $code = intval($last_type_code) + 1;
            $db->query("INSERT INTO `actg_accounting_types` (actg_type_name, actg_type_code, parent_type_id) VALUES ('$param[account_name]', '$code', '$parent_id')");
            if ($actg_type_id = $db->insert_id()) {
                $param['actg_type_id'] = $actg_type_id;
                unset($param['is_bank']);
                return $this->actg->addAccount($param);
            }
        } else {
            sys_msg("無法新增子帳戶");
        }
        return false;
    }

    // Create accounting transaction
    function insertTransaction($param)
    {
        global $db, $ecs, $_CFG;

        if (empty($param)) return false;
        // Default currency: HKD
        if (empty($param['currency_code'])) {
            $param['currency_code'] = 'HKD';
        }
		if (!self::param_contains_keys($param, array(
			'actg_type_id', 'amount'
        ))) return false;

        $currency = $this->actg->getCurrency(['currency_code' => $param['currency_code']]);
        if (empty($currency)) return false;

        $type = self::getAccountingType([
            'actg_type_id'  => $param['actg_type_id'],
            'enabled'       => 1,
        ]);
        if (empty($type)) return false;

        if (!empty($param['exchange'])) {
            $target_type = self::getAccountingType([
                'actg_type_id'  => $param['exchange']['actg_type_id'],
                'enabled'       => 1,
            ]);
            if (empty($target_type)) return false;
            $param['remark'] = "轉帳" . ($param['exchange']['type'] == 'to' ? '自' : '至') . " $target_type[actg_type_name] " . $param['remark'];
        }

        if (empty($param['exchanged_amount'])) {
            $param['exchanged_amount'] = bcmul($param['amount'], $currency['currency_rate'], 2);
        }

        if (empty($param['txn_date'])) {
            $param['txn_date'] = local_date("Y-m-d H:i:s");
        }

        if (empty($param['created_by'])) {
            $param['created_by'] = $_SESSION['admin_id'];
        }

        if (empty($param['actg_month'])) {
            $dateTime = new \DateTime($param['txn_date']);
            $param['actg_month'] = $dateTime->format('Y/m');
        }

        if (!empty($param['sales_order_id'])) {
            $order = $db->getRow("SELECT order_type, ws_customer_id, type_id FROM " . $ecs->table('order_info') . " WHERE order_id = $param[sales_order_id]");
            if ($order['order_type'] == OrderController::DB_ORDER_TYPE_WHOLESALE && empty($param['wsid']) && !empty($order['ws_customer_id'])) {
                $param['wsid'] = $order['ws_customer_id'];
            } elseif ($order['order_type'] == OrderController::DB_ORDER_TYPE_SALESAGENT && empty($param['sa_id']) && !empty($order['type_id'])) {
                $param['sa_id'] = $order['type_id'];
            }
        }
        
        if (!empty($param['integral_amount']) && empty($param['exchanged_integral_amount'])) {
            $param['exchanged_integral_amount'] = bcmul(bcdiv($param['integral_amount'], 100, 4), $_CFG['integral_scale'], 4);
        }

		$param = self::param_filter_keys($param, array(
			'actg_type_id', 'actg_ref_code', 'currency_code', 'amount', 'exchanged_amount', 'integral_amount', 'exchanged_integral_amount', 'txn_date', 'related_txn_id', 'reversed_txn_id', 'supplier_id', 'wsid', 'sa_id', 'supplier_code', 'erp_order_id', 'sales_order_id', 'warehousing_id', 'delivery_id', 'rma_id', 'refund_id', 'cn_txn_id', 'remark', 'created_by', 'actg_group_id', 'txn_group_id', 'posted', 'actg_month'
        ));

        $unset_fields = [
            'related_txn_id', 'reversed_txn_id', 'wsid', 'sa_id', 'erp_order_id', 'sales_order_id', 'warehousing_id', 'delivery_id', 'rma_id', 'refund_id', 'actg_group_id', 'txn_group_id', 'cn_txn_id'
        ];
        foreach ($unset_fields as $field) {
            if (empty($param[$field])) {
                unset($param[$field]);
            }
        }
        if (!empty($param['supplier_code']) && empty($param['supplier_id'])) {
            $param['supplier_id'] = $db->getOne("SELECT supplier_id FROM " . $ecs->table('erp_supplier') . " WHERE code = '$param[supplier_code]'");
        }

        return $db->autoExecute('actg_accounting_transactions', $param, 'INSERT') ? $db->insert_id() : false;
    }

    // Create 1 to 1 double entry transaction
    function insertDoubleEntryTransactions($param)
    {
        if (empty($param)) return false;
		if (!self::param_contains_keys($param, array(
			'from_actg_type_id', 'to_actg_type_id', 'amount'
        ))) return false;
        if (empty($param['from_actg_type_id']) || empty($param['to_actg_type_id']) || $param['from_actg_type_id'] == $param['to_actg_type_id']) return false;

        $txn = [];
        foreach ($param as $key => $val) {
            if (strpos("actg_type_id", $key) === false) {
                $txn[$key] = $val;
            }
        }
        $txn['actg_type_id'] = $param['from_actg_type_id'];
        $txn['amount'] = $param['amount'];

        if (!empty($param['integral_amount'])) {
            $txn['integral_amount'] = $param['integral_amount'];
        }
        $related_txn_id = self::insertTransaction($txn);
        if ($related_txn_id)  {
            $txn['actg_type_id'] = $param['to_actg_type_id'];
            $txn['amount'] = bcmul($param['amount'], -1, 2);
            $txn['related_txn_id'] = empty($param['related_txn_id']) ? $related_txn_id : $param['related_txn_id'];
            if (!empty($param['exchanged_amount'])) {
                $txn['exchanged_amount'] = bcmul($param['exchanged_amount'], -1, 2);
            }
            if (!empty($param['integral_amount'])) {
                $txn['integral_amount'] = bcmul($param['integral_amount'], -1);
            }
            if (self::insertTransaction($txn)) {
                return $related_txn_id;
            }
            return false;
        }
        return false;
    }

    // Create Multi Entry transaction
    function insertMultiEntryTransactions($param)
    {
        if (empty($param)) return false;

        $sumOfTrans = 0;
        foreach ($param as $index => $entry){
            if (empty($entry['actg_type_id']) || !is_numeric($entry['amount'])){
                unset($param[$index]); // drop if no actg_type_id / amount
            } else {
                $sumOfTrans = bcadd($sumOfTrans, $entry['amount']);
            }
        }
        if (bccomp(0, $sumOfTrans) != 0 || sizeof($param) < 2){
            return FALSE; // return if sum of transactions != 0
        } else {
            $i = 0;
            $related_txn_id = 0;
            foreach ($param as $tran){
                if ($i == 0){
                    $related_txn_id = self::insertTransaction($tran);
                    if (!$related_txn_id){
                        return FALSE;
                    }
                } else {
                    $tran['related_txn_id'] = $related_txn_id;
                    $txn_id = self::insertTransaction($tran);
                }
                $i++;
            }
            return $related_txn_id;
        }
    }

    // Create exchange transaction
    function insertExchanges($param)
    {
        if (empty($param)) return false;
		if (!self::param_contains_keys($param, array(
			'from_account', 'to_account', 'amount'
        ))) return false;

        $from_account = $this->actg->getAccount(['account_id' => $param['from_account']]);
        $to_account = $this->actg->getAccount(['account_id' => $param['to_account']]);

        if (empty($from_account)) return false;
        if (empty($to_account)) return false;
        if (!$from_account['allow_debit'] && bccomp($param['amount'], $from_account['account_balance']) > 0) return false;

        $from_currency = $this->actg->getCurrency(['currency_code' => $from_account['currency_code']]);
        $to_currency = $this->actg->getCurrency(['currency_code' => $to_account['currency_code']]);

        if (empty($from_currency) || empty($to_currency)) return false;

        $from_amount = round(bcmul($param['amount'], $from_currency['currency_rate']), 2);
        $to_amount = round(bcdiv($from_amount, $to_currency['currency_rate']), 2);

        // default payment type: 轉帳
        if (empty($param['from_payment_type'])) $param['from_payment_type'] = 2;
        if (empty($param['to_payment_type'])) $param['to_payment_type'] = 2;
        if (empty($param['txn_date'])) {
            $param['txn_date'] = $this->actg->dbdate();
        }
        $txn_date = local_date('Y-m-d H:i:s', strtotime($param['txn_date']));

        $from_txn = [
            'amount'            => bcmul($param['amount'], -1),
            'remark'            => $param['remark'],
            'txn_date'          => $txn_date,
            'created_by'        => $param['created_by'],
            'actg_type_id'      => $from_account['actg_type_id'],
            'currency_code'     => $from_account['currency_code'],
            'exchanged_amount'  => bcmul($from_amount, -1),
            'exchange'          => [
                'type' => 'from',
                'actg_type_id' => $to_account['actg_type_id'],
            ]
        ];

        $to_txn = [
            'amount'            => $to_amount,
            'remark'            => $param['remark'],
            'txn_date'          => $txn_date,
            'created_by'        => $param['created_by'],
            'actg_type_id'      => $to_account['actg_type_id'],
            'currency_code'     => $to_account['currency_code'],
            'exchanged_amount'  => $from_amount,
            'exchange'          => [
                'type' => 'to',
                'actg_type_id' => $from_account['actg_type_id'],
            ]
        ];
        $from_actg_txn_id = self::insertTransaction($from_txn);
        if ($from_actg_txn_id) {
            $to_txn['related_txn_id'] = $from_actg_txn_id;
            $to_actg_txn_id = self::insertTransaction($to_txn);

            if ($to_actg_txn_id) {
                $currency_exchange = [
                    'exchange_date'     => $param['txn_date'],
                    'operator'          => $param['created_by'],
                    'remark'            => $param['remark'],
                    'from_account'      => $param['from_account'],
                    'from_currency'     => $from_account['currency_code'],
                    'from_amount'       => $param['amount'],
                    'from_payment_type' => $param['from_payment_type'],
                    'to_account'        => $param['to_account'],
                    'to_currency'       => $to_account['currency_code'],
                    'to_amount'         => $to_amount,
                    'to_payment_type'   => $param['to_payment_type'],
                    'from_actg_txn_id'  => $from_actg_txn_id,
                    'to_actg_txn_id'    => $to_actg_txn_id,
                ];
                $GLOBALS['db']->autoExecute('actg_currency_exchanges', $currency_exchange, 'INSERT');
                return $to_actg_txn_id;
            }
        }
        return false;
    }

    // Create purchase transaction on approving PO
    function insertPurchaseTransactions($param)
    {
        if (empty($param)) return false;
		if (!self::param_contains_keys($param, array(
			'supplier_id', 'currency_code', 'order_amount', 'shipping_amount', 'erp_order_id'
        ))) return false;

        $shipping_type_id = self::getAccountingType(['actg_type_code' => '44001', 'enabled' => 1], 'actg_type_id'); // Shipping Cost
        $purchase_type_id = self::getAccountingType(['actg_type_code' => '41100', 'enabled' => 1], 'actg_type_id'); // Local Purchase
        $payable_type_id = self::getAccountingType(['actg_type_code' => '23101', 'enabled' => 1], 'actg_type_id'); // AP - Supplier
        if ($param['currency_code'] != "HKD") {
            $purchase_type_id = self::getAccountingType(['actg_type_code' => '41200', 'enabled' => 1], 'actg_type_id'); // Global Purchase (Foreign currency)
        }

        $txn_date = empty($param['txn_date']) ? local_date("Y-m-d H:i:s") : $param['txn_date'];

        $this->actg->start_transaction();
        $baseTxn = [
            // TODO: Create transaction base on foreign currency
            // 'currency_code' => $param['currency_code'],
            'currency_code' => 'HKD',
            'created_by'    => $param['created_by'],
            'remark'        => $param['remark'],
            'erp_order_id'  => $param['erp_order_id'],
            'supplier_id'   => $param['supplier_id'],
            'txn_date'      => $txn_date,
        ];

        if (!empty($param['actg_group_id'])) {
            $baseTxn['actg_group_id'] = $param['actg_group_id'];
        }
        $purchase_txn = $baseTxn;
        $purchase_txn['actg_type_id'] = $purchase_type_id;
        $purchase_txn['amount'] = $param['order_amount'];

        if ($related_txn_id = self::insertTransaction($purchase_txn)) {
            if (!empty($param['shipping_amount'])) {
                $shipping_txn = $baseTxn;
                $shipping_txn['actg_type_id'] = $shipping_type_id;
                $shipping_txn['amount'] = $param['shipping_amount'];
                $shipping_txn['related_txn_id'] = $related_txn_id;

                if (!self::insertTransaction($shipping_txn)) {
                    $this->actg->rollback_transaction();
                    return false;
                }
            }
            $payable_txn = $baseTxn;
            $payable_txn['actg_type_id'] = $payable_type_id;
            $payable_txn['amount'] = bcmul(bcadd($param['order_amount'], $param['shipping_amount'], 2), -1, 2);
            $payable_txn['related_txn_id'] = $related_txn_id;

            if (self::insertTransaction($payable_txn)) {
                $this->actg->commit_transaction();
                return $related_txn_id;
            }
        }
        $this->actg->rollback_transaction();
        return false;
    }

    // Create purchase transaction on paying PO
    function insertPurchasePaymentTransactions($param)
    {
        require_once ROOT_PATH . '/' . ADMIN_PATH . '/includes/ERP/lib_erp_order.php';
        global $db, $ecs;

        if (empty($param)) return false;

		if (!self::param_contains_keys($param, array(
			'erp_order_id', 'supplier_id', 'actg_type_id', 'amount', 'currency_code'
		))) return false;

		$param = self::param_filter_keys($param, array(
			'erp_order_id', 'supplier_id', 'actg_type_id', 'amount', 'created_by', 'txn_date', 'currency_code', 'remark', 'cn_ids', 'cn_amount', 'fully_paid', 'exchanged_amount', 'actg_month'
		));

        $this->actg->start_transaction();
		if(!empty($param['cn_ids']))
		{
			$cns = get_credit_note($param['cn_ids'], "", " ORDER BY curr_amount ASC");
			$remain_credit = $param['cn_amount'];
			foreach($cns as $cn){
				if($remain_credit > 0){
					$curr_amount = doubleval($cn['curr_amount']);
					$used_credit = $curr_amount > $remain_credit ? $remain_credit : $curr_amount;
					$params[] = array(
						'cn_id' => $cn['cn_id'],
						'curr_amount' => $curr_amount - $used_credit,
						'txn_info' => array(
                            'cn_sn' => $cn['cn_sn'],
							'amount' => -$used_credit,
                            'txn_date' => $param['txn_date'],
                            'created_by' => $param['created_by'],
                            'erp_order_id' => $param['erp_order_id'],
                            'supplier_id' => $param['supplier_id'],
						)
					);
					$param['cn_sn'][] = $cn['cn_sn'];
					$remain_credit -= $curr_amount;
				}
			}
		}

		$order_sn = $db->getOne("SELECT order_sn FROM " . $ecs->table('erp_order') . " WHERE order_id = '" . $param['erp_order_id'] . "' LIMIT 1");
		$supplier = $db->getOne("SELECT name FROM " . $ecs->table('erp_order') . " eo " .
						"LEFT JOIN " . $ecs->table('erp_supplier') . " es ON eo.supplier_id = es.supplier_id " .
						"WHERE eo.order_sn = '$order_sn'");

		$txn_remark = '採購訂單 ' . $order_sn . ' (' . mysql_escape_string($supplier) . ') 付款' .
			(empty($param['remark']) ? '' : ' ' . $param['remark']);

        $doubleEntries = [
			'currency_code' => $param['currency_code'],
            'amount' => bcmul($param['amount'], -1, 2),
            'exchanged_amount' => bcmul($param['exchanged_amount'], -1, 2),
			'from_actg_type_id' => $param['actg_type_id'],
			'to_actg_type_id' => self::getAccountingType(['actg_type_code' => '23101', 'enabled' => 1], 'actg_type_id'), // AP - Supplier
			'remark' => $txn_remark,
            'created_by' => $param['created_by'],
            'erp_order_id' => $param['erp_order_id'],
            'supplier_id' => $param['supplier_id'],
            'actg_month' => $param['actg_month'],
        ];

        if ($related_txn_id = self::insertDoubleEntryTransactions($doubleEntries)) {
            if(!empty($param['cn_sn'])){
                $erpController = new ErpController();
				foreach($params as $p){
					$p['txn_info']['actg_txn_id'] = $related_txn_id;
					$p['txn_info']['remark'] = "支付: " . $order_sn;
					$p['txn_info']['txn_remark'] = $p['txn_info']['cn_sn'] . ' Rebate';
                    $erpController->update_credit_note($p);
				}
			}
        } else {
            $this->actg->rollback_transaction();
            return false;
        }

        $purchase = [
            'order_id'      => $param['erp_order_id'],
            'operator'      => $db->getOne("SELECT user_name FROM " . $ecs->table('admin_user') . " WHERE user_id = $param[created_by]"),
            'remark'        => $param['remark'],
            'actg_txn_id'   => $related_txn_id,
        ];

		if (!$db->autoExecute('actg_purchase_payments', $purchase, 'INSERT'))
		{
            $this->actg->rollback_transaction();
			return false;
		}
        $purchase_payment_id = $this->db->insert_id();
        $paid_amounts = $this->actgHooks->getPurchaseOrderPaidAmounts($param['erp_order_id']);
        $total_paid = $this->actgHooks->calculateTotalPaidAmount($paid_amounts);

        $sql = "UPDATE " . $ecs->table('erp_order') .
            "SET `actg_paid_amount` = '" . $total_paid . "' " .
            ($param['fully_paid'] ? ", `fully_paid` = 1 " : '') .
            "WHERE `order_id` = '" . $param['erp_order_id'] . "'";
        if (!$db->query($sql)) {
            $this->actg->rollback_transaction();
			return false;
        }
        $this->actg->commit_transaction();
		return $purchase_payment_id;
    }

    // Create rma transaction on receiving/delivering RMA goods
    function insertRMATransactions($param, $stockout = false, $inventory = false)
    {
        if (!self::param_contains_keys($param, array(
			'amount', 'rma_id'
        ))) return false;
        $inventory_type_id = $inventory ? self::getAccountingType(['actg_type_code' => '14001', 'enabled' => 1], 'actg_type_id') : self::getAccountingType(['actg_type_code' => '14003', 'enabled' => 1], 'actg_type_id'); // Inventory (Owned) / Inventory (RMA)
        $cos_type_id = self::getAccountingType(['actg_type_code' => '45000', 'enabled' => 1], 'actg_type_id'); // Cost of Sales
        $txn = [
            'from_actg_type_id'	=> $inventory_type_id,
            'to_actg_type_id' 	=> $cos_type_id,
        ];

        // Only when stock out
        if ($stockout) {
            $txn['from_actg_type_id'] = $cos_type_id;
            $txn['to_actg_type_id'] = $inventory_type_id;
        }

        foreach ($param as $key => $val) {
            $txn[$key] = $val;
        }
        return self::insertDoubleEntryTransactions($txn);
    }

    function insertWriteOffTransactions($param)
    {
        if (!self::param_contains_keys($param, array(
			'amount'
        ))) return false;

        $txn = [
            'from_actg_type_id'	=> self::getAccountingType(['actg_type_code' => '46000', 'enabled' => 1], 'actg_type_id'), // Inventory Write Off
            'to_actg_type_id' 	=> self::getAccountingType(['actg_type_code' => '14003', 'enabled' => 1], 'actg_type_id'), // Inventory (RMA)
        ];
        foreach ($param as $key => $val) {
            $txn[$key] = $val;
        }
        return self::insertDoubleEntryTransactions($txn);
    }

    // Create sales transaction on order payment
    function insertSalesTransactions($param)
    {
        global $db, $ecs;
        if (!self::param_contains_keys($param, array(
			'sales_order_id', 'amount'
        ))) return false;
        $order = $db->getRow("SELECT order_sn, platform, order_type, type_id, ar_txn_id, pay_id FROM " . $ecs->table('order_info') . " WHERE order_id = $param[sales_order_id]");
        if (empty($order)) return false;

        

        if ($order['order_type'] == 'sa' && $order['pay_id'] == '11' && $order['type_id'] != ""){ // SA Order
            $multiEntries = [];
            $sales_agent_pay_type = self::getSalesAgentPayType($order['type_id']);

            if (isset($sales_agent_pay_type) && $sales_agent_pay_type >= 0){
                switch ($sales_agent_pay_type) {
                    case 0:
                        $from_actg_type_id = self::getAccountingType(['actg_type_code' => '13130', 'enabled' => 1], 'actg_type_id'); // Sales - B2B2C - Monthly Pay
                        break;
                    case 1:
                        $from_actg_type_id = self::getAccountingType(['actg_type_code' => '28003', 'enabled' => 1], 'actg_type_id'); // Sales - B2B2C - Pre Pay
                        break;
                    default:
                        break;
                }
            }
            $to_actg_type_id = self::getAccountingType(['actg_type_code' => '31400', 'enabled' => 1], 'actg_type_id'); // Sales - B2B2C

            $orderController = new OrderController();
            $sales_agent_rate = $orderController ->getSaRate($order['type_id']);

            $from_amount = $param['amount'];

            if($sales_agent_rate !="" && $sales_agent_rate > 0){
                $rate_in_amount = round(bcmul($param['amount'], bcdiv($sales_agent_rate, 100)), 2);
                $amount_wo_rate = bcsub($param['amount'], $rate_in_amount, 2);
                $from_amount = $amount_wo_rate;
                $trans_fee_actg_type_id = self::getAccountingType(['actg_type_code' => '35001', 'enabled' => 1], 'actg_type_id'); // Sales - B2B2C - Transaction Fee
            }

            $from_entries = [
                'amount'            => $from_amount,
                'remark' 	        => $param['remark'],
                'created_by'        => $param['created_by'],
                'sales_order_id'    => $param['sales_order_id'],
                'currency_code' 	=> $param['currency_code'],
                'actg_type_id' 	    => $from_actg_type_id,
            ];

            array_push($multiEntries, $from_entries);

            if ($sales_agent_rate > 0 && $rate_in_amount > 0 && $amount_wo_rate){
                $trans_fee_entries = [
                    'amount'            => $rate_in_amount,
                    'remark' 	        => $param['remark']." - 手續費",
                    'created_by'        => $param['created_by'],
                    'sales_order_id'    => $param['sales_order_id'],
                    'currency_code' 	=> $param['currency_code'],
                    'actg_type_id' 	    => $trans_fee_actg_type_id,
                ];

                array_push($multiEntries, $trans_fee_entries);
            }

            $to_entries = [
                'amount'            => bcmul($param['amount'], -1, 2),
                'remark' 	        => $param['remark'],
                'created_by'        => $param['created_by'],
                'sales_order_id'    => $param['sales_order_id'],
                'currency_code' 	=> $param['currency_code'],
                'actg_type_id' 	    => $to_actg_type_id,
            ];

            array_push($multiEntries, $to_entries);

            return self::insertMultiEntryTransactions($multiEntries);

        } else {

            $doubleEntries = [
                'amount'            => $param['amount'],
                'remark' 	        => $param['remark'],
                'created_by'        => $param['created_by'],
                'sales_order_id'    => $param['sales_order_id'],
                'currency_code' 	=> $param['currency_code'],
            ];
            $doubleEntries['from_actg_type_id'] = self::getAccountingTypeByAccountId($param['account_id'], 'actg_type_id');
            if (strpos($order['platform'], 'pos') !== false) {
                if ($order['order_type'] == OrderController::DB_ORDER_TYPE_WHOLESALE) {
                    if (empty($order['ar_txn_id'])) return false;
                    $doubleEntries['to_actg_type_id'] = self::getAccountingType(['actg_type_code' => '13110', 'enabled' => 1], 'actg_type_id'); // Accounts Receivable - B2B customer
                } elseif ($order['order_type'] == OrderController::DB_ORDER_TYPE_SALESAGENT) {
                    $doubleEntries['to_actg_type_id'] = self::getAccountingType(['actg_type_code' => '31400', 'enabled' => 1], 'actg_type_id'); // Sales - B2B2C
                } else {
                    $doubleEntries['to_actg_type_id'] = self::getAccountingType(['actg_type_code' => '31200', 'enabled' => 1], 'actg_type_id'); // Sales - B2C (Shop)
                }
            } else {
                $doubleEntries['to_actg_type_id'] = self::getAccountingType(['actg_type_code' => '31100', 'enabled' => 1], 'actg_type_id'); // Sales - B2C (Online)
            }
            return self::insertDoubleEntryTransactions($doubleEntries);
        }
    }

    // Create wholesale account receivable transaction on confirm
    function insertWSAccountReceivableTransactions($param)
    {
        if (!self::param_contains_keys($param, array(
			'sales_order_id', 'amount', 'order_sn', 'wsid'
        ))) return false;

        $doubleEntries = [
            'sales_order_id'    => $param['sales_order_id'],
            'wsid'              => $param['wsid'],
            'from_actg_type_id' => self::getAccountingType(['actg_type_code' => '13110', 'enabled' => 1], 'actg_type_id'), // Accounts Receivable - B2B customer
            'to_actg_type_id'   => self::getAccountingType(['actg_type_code' => '31300', 'enabled' => 1], 'actg_type_id'), // Sales - B2B
            'amount'            => $param['amount'],
            'remark'            => "訂單 $param[order_sn] 應收款項",
        ];

        return self::insertDoubleEntryTransactions($doubleEntries);
    }

    // Create refund transaction on apply
    // type - 0: refund, 1: compensate
    function insertRefundApplyTransactions($param, $type = 0, $renew = false)
    {
        if (!self::param_contains_keys($param, array(
			'sales_order_id', 'amount', 'order_sn'
        ))) return false;

        $doubleEntries = [
            'sales_order_id'    => $param['sales_order_id'],
            'from_actg_type_id' => self::getAccountingType(['actg_type_code' => ($type == 0 ? '32200' : '53100'), 'enabled' => 1], 'actg_type_id'), // refund: Refund / compensate: Reward program
            'to_actg_type_id'   => self::getAccountingType(['actg_type_code' => '28002', 'enabled' => 1], 'actg_type_id'), // Other Payables
            'amount'            => $param['amount'],
            'remark'            => $renew ? ("編輯訂單 $param[order_sn] 退款申請 #$param[rec_id]") : ("訂單 $param[order_sn] 申請" . ($type == 0 ? "退款" : "積分補償")),
        ];

        if (!empty($param['rec_id'])) {
            $doubleEntries['refund_id'] = $param['rec_id'];
        }
        if (bccomp($param['integral_amount'], 0) == 1) {
            $doubleEntries['integral_amount'] = $param['integral_amount'];
        }

        return self::insertDoubleEntryTransactions($doubleEntries);
    }

    /**
     * Create refund transaction on request update
     * 
     * @param $param array refund data
     * @param $act   int   0 edit amount, 1 reject, 2 approve
     * @param $type  int   0 refund money, 1 refund integral, 2 compensate
     * 
     * @return int actg_txn_id OR false
     */ 
    function insertRefundUpdateTransactions($param, $act = 0, $type = 0)
    {
        global $db, $ecs;
        if (!self::param_contains_keys($param, array(
			'rec_id'
        ))) return false;

        $actg_txn_id = $db->getOne("SELECT apply_txn_id FROM " . $ecs->table('refund_requests') . " WHERE rec_id = $param[rec_id]");
        if (empty($actg_txn_id)) return false;

        if ($act == 0) {
            // Edit amount
            if (!self::param_contains_keys($param, array(
                'sales_order_id', 'amount', 'order_sn'
            ))) return false;
            if (self::reverseAccountingTransaction(['actg_txn_id' => $actg_txn_id])) {
                return self::insertRefundApplyTransactions($param, $type, true);
            }
        } elseif ($act == 1) {
            // Reject request
            if (self::reverseAccountingTransaction(['actg_txn_id' => $actg_txn_id])) {
                return $actg_txn_id;
            }
        } elseif ($act == 2) {
            // Approve request
            if (!self::param_contains_keys($param, array(
                'sales_order_id', 'amount', 'order_sn'
            ))) return false;

            $applied_amount = $db->getOne("SELECT amount FROM `actg_accounting_transactions` WHERE actg_txn_id = $actg_txn_id");
            // Amount not match
            if (bccomp($applied_amount, $param['amount']) != 0) return false;

            $doubleEntries = [
                'sales_order_id'    => $param['sales_order_id'],
                'from_actg_type_id' => self::getAccountingType(['actg_type_code' => '28002', 'enabled' => 1], 'actg_type_id'), // Other Payables
                'amount'            => $param['amount'],
                'refund_id'         => $param['rec_id'],
            ];
            if ($type == 0) {
                // Account not selected
                if (empty($param['account_id'])) return false;

                $doubleEntries['to_actg_type_id'] =  self::getAccountingTypeByAccountId($param['account_id'], 'actg_type_id'); // Bank / Cash / Other Receivables
                $doubleEntries['remark'] = "訂單 $param[order_sn] 完成退款";
            } elseif ($type == 1 || $type == 2) {
                $doubleEntries['to_actg_type_id'] =  self::getAccountingType(['actg_type_code' => '28001', 'enabled' => 1], 'actg_type_id'); // Reward
                $doubleEntries['integral_amount'] = $param['integral_amount'];
                $doubleEntries['remark'] = "訂單 $param[order_sn] 完成積分" . ($type == 1 ? "退款" : "補償");
            }
            $complete_txn_id = self::insertDoubleEntryTransactions($doubleEntries);
            if ($complete_txn_id) {
                $db->query("UPDATE " . $ecs->table('refund_requests') . " SET complete_txn_id = $complete_txn_id WHERE rec_id = $param[rec_id]");
            }
            return $complete_txn_id;
        }
        return false;
    }

    function getAccountingOperators($posted = false)
    {
        $field = $posted ? 'posted_by' : 'created_by';
        $table = $posted ? 'actg_accounting_posting_log' : 'actg_accounting_transactions';
        return $GLOBALS['db']->getAll("SELECT au.user_id, au.user_name FROM `$table` t LEFT JOIN " . $GLOBALS['ecs']->table('admin_user') . " au ON t.$field = au.user_id WHERE t.$field > 0 GROUP BY t.$field");
    }

    function getAccountingTransactions()
    {
        global $db, $ecs;
        include_once ROOT_PATH . '/languages/' . $_CFG['lang'] . '/admin/accounting_system.php';

        $sort_by = empty($_REQUEST['sort_by']) ? "actg_txn_id" : $_REQUEST['sort_by'];
        $sort_order = empty($_REQUEST['sort_order']) ? "desc" : $_REQUEST['sort_order'];
        $start = empty($_REQUEST['start']) ? 0 : intval($_REQUEST['start']);
        $page_size = empty($_REQUEST['page_size']) ? 50 : intval($_REQUEST['page_size']);

        $where = "WHERE 1 ";
        if (!empty($_REQUEST['start_date'])) {
            $where .= "AND txn_date > '" . trim($_REQUEST['start_date']) . "' ";
        }
        if (!empty($_REQUEST['end_date'])) {
            $where .= "AND txn_date <= '" . trim($_REQUEST['end_date']) . " 23:59:59' ";
        }
        if ($_REQUEST['created_by'] != '-1' && isset($_REQUEST['created_by'])) {
            $where .= "AND created_by = '" . trim($_REQUEST['created_by']) . "' ";
        }
        if (!empty($_REQUEST['actg_type_id'])) {
            $where .= "AND (actg_type_id = '" . trim($_REQUEST['actg_type_id']) . "' OR actg_type_id in (SELECT actg_type_id FROM `actg_accounting_types` WHERE parent_type_id = '" . trim($_REQUEST['actg_type_id']) . "')) ";
        }
        if (!empty($_REQUEST['supplier_id'])) {
            $where .= "AND supplier_id = '$_REQUEST[supplier_id]' ";
        }
        if (!empty($_REQUEST['wsid'])) {
            $where .= "AND wsid = '$_REQUEST[wsid]' ";
        }
        if (!empty($_REQUEST['sa_id'])) {
            $where .= "AND sa_id = '$_REQUEST[sa_id]' ";
        }
        if (!empty(trim($_REQUEST['search']))) {
            $search = trim($_REQUEST['search']);
            preg_match("/FIN(\d+)/", $search, $actg_txn_id);
            if (!empty($actg_txn_id)) {
                $formatted_txn_id = $actg_txn_id[1];
            }
            $where .= "AND (remark LIKE '%" . trim($_REQUEST['search']) . "%' " .
                        (is_numeric($search) ? "OR amount = '$search' OR exchanged_amount = '$search' OR actg_txn_id = '$search' " : "") .
                        (empty($formatted_txn_id) ? "" : "OR actg_txn_id = $formatted_txn_id ") .
                        ") ";
        }
        if (empty($_REQUEST['reversed'])) {
            // Show posted record even if reversed
            $where .= "AND posted != -1 ";
            // $where .= "AND posted != -1 AND reversed = 0 ";
        }
        if (isset($_REQUEST['status']) && $_REQUEST['status'] != "") {
            $where .= "AND posted = '$_REQUEST[status]' ";
        }
        if ($_REQUEST['posted_by'] != '-1' && isset($_REQUEST['posted_by'])) {
            $where .= "AND actg_txn_id IN (SELECT DISTINCT actg_txn_id FROM `actg_accounting_posting_log` WHERE posted_by = '" . trim($_REQUEST['posted_by']) . "') ";
        }

        $sql = "SELECT DISTINCT IFNULL(related_txn_id, actg_txn_id) FROM `actg_accounting_transactions` " . $where .
                "ORDER BY $sort_by $sort_order LIMIT $start, $page_size";
        $count_sql = "SELECT COUNT(DISTINCT IFNULL(related_txn_id, actg_txn_id)) FROM `actg_accounting_transactions` " . $where;

        $baseTxns = $db->getCol($sql);
        $count = $db->getOne($count_sql);

        $allTxns = $db->getAll(
            "SELECT txn.*, type.*, CASE WHEN txn.created_by = -2 THEN 'Yoho Bravo 1' WHEN txn.created_by = 0 THEN '系統生成' ELSE IFNULL(au.user_name, 'Unknown') END as user_name FROM `actg_accounting_transactions` txn " .
            "LEFT JOIN `actg_accounting_types` type ON type.actg_type_id = txn.actg_type_id " .
            "LEFT JOIN (SELECT user_id, user_name FROM " . $ecs->table('admin_user') . ") au ON au.user_id = txn.created_by AND txn.created_by > 0 " .
            "WHERE (actg_txn_id " . db_create_in($baseTxns) . " OR related_txn_id " . db_create_in($baseTxns) . ") " .
            (empty($_REQUEST['specific']) || empty($_REQUEST['actg_type_id']) ? "" : "AND (txn.actg_type_id = '" . trim($_REQUEST['actg_type_id']) . "' OR txn.actg_type_id in (SELECT actg_type_id FROM `actg_accounting_types` WHERE parent_type_id = '" . trim($_REQUEST['actg_type_id']) . "'))") .
            (empty($_REQUEST['zero']) ? " AND amount != 0" : "") .
            " ORDER BY CASE WHEN amount >= 0 THEN 1 ELSE 0 END DESC, CASE WHEN amount >= 0 THEN amount ELSE amount * -1 END DESC, actg_txn_id ASC"
        );

        $posted_info = $db->getAll(
            "SELECT apl.*, au.user_name FROM `actg_accounting_posting_log` apl " .
            "LEFT JOIN (SELECT user_id, user_name FROM " . $ecs->table('admin_user') . ") au ON au.user_id = apl.posted_by " .
            "WHERE apl.actg_txn_id " . db_create_in($baseTxns)
        );

        $supplier_list = self::getAccountingSuppliersList();
        foreach ($supplier_list as $supplier) {
            $suppliers[$supplier['supplier_id']] = $supplier['name'];
        }
        $wholesale_list = self::getAccountingWholesaleCustomerList();
        foreach ($wholesale_list as $wholesale) {
            $wholesales[$wholesale['wsid']] = $wholesale['company'];
        }
        $salesagent_list = self::getAccountingSalesAgentList();
        foreach ($salesagent_list as $salesagent) {
            $salesagents[$salesagent['sa_id']] = $salesagent['name'];
        }

        $txnGroup = [];
        foreach ($baseTxns as $txn_id) {
            $txnGroup[$txn_id] = [];
        }
        foreach ($posted_info as $log) {
            $posted_log[$log['actg_txn_id']] = [
                'posted_status' => $log['status'],
                'posted_admin' => $log['user_name'],
                'posted_date' => local_date("Y-m-d", local_strtotime($log['posted_at'])),
            ];
        }
        foreach ($allTxns as $txn) {
            if (in_array($txn['actg_txn_id'], $baseTxns)) {
                $txnGroup[$txn['actg_txn_id']][] = $txn;
            } elseif (in_array($txn['related_txn_id'], $baseTxns)) {
                $txnGroup[$txn['related_txn_id']][] = $txn;
            }
        }
        $class = ["groupOdd", "groupEven"];
        $list = [];
        foreach ($txnGroup as $group_id => $txns) {
            $checked = false;
            $reject = 0;
            foreach ($txns as $txn) {
                $date = $txn['txn_date'];
                if (!empty($txn['supplier_id']) && in_array($txn['actg_type_code'], ['23101', '41100', '43002'])) { // AP - Supplier, Local Purchase, Rebate(Credit note)
                    $txn['actg_type_name'] .= " - <span class='green'>" . $suppliers[$txn['supplier_id']] . "</span>";
                }
                if (!empty($txn['wsid']) && in_array($txn['actg_type_code'], ['13110', '31300'])) { // AR - Corporate customer, Sales - B2B
                    $txn['actg_type_name'] .= " - <span class='green'>" . $wholesales[$txn['wsid']] . "</span>";
                }
                if (!empty($txn['sa_id']) && in_array($txn['actg_type_code'], ['13130', '28003', '31400', '35001'])) { // AR - B2B2C Customer, Receipt in Advance-B2B2C, Sales - B2B2C, Txn Cost - B2B2C Customers
                    $txn['actg_type_name'] .= " - <span class='green'>" . $salesagents[$txn['sa_id']] . "</span>";
                }
                $txn['status'] = $GLOBALS['_LANG']['account_posting_status'][$txn['posted']];
                $txn['class'] = $class[array_search($group_id, array_keys($txnGroup), true) % 2];
                $txn['group_id'] = $group_id;
                $txn['txn_date'] = local_date("Y-m-d", local_strtotime($date));
                $txn['actg_month'] = isset($txn['actg_month']) ? $txn['actg_month'] : local_date("Y/m", local_strtotime($date));
                $amount = floatval($txn['exchanged_amount']);

                if (bccomp($amount, 0, 2) == -1) {
                    $txn['debit'] = "--";
                    $txn['credit'] = "<span class='red'>(" . number_format(bcmul($amount, -1, 2), 2) . ")</sapn>";
                    if (!empty($_REQUEST['specific']) && !empty($_REQUEST['actg_type_id'])) {
                        $txn['checkbox'] = "<input name='txn_amount' type='hidden' data-id='$group_id' data-amount='$amount'>";
                    }
                } else {
                    $txn['debit'] = number_format($txn['exchanged_amount'], 2);
                    $txn['credit'] = "--";
                    $txn['checkbox'] = "<input name='txn_amount' type='hidden' data-id='$group_id' data-amount='$amount'>";
                }
                $txn['checkbox'] .= ($checked ? " " : "<input name='txn_select' type='checkbox' class='flat' data-id='$group_id' data-posted='$txn[posted]'>");
                $checked = true;
                if (!empty($posted_log[$group_id])) {
                    $txn = array_merge($txn, $posted_log[$group_id]);
                }
                $list[] = $txn;
            }
        }
        return ['data' => $list, 'record_count' => $count];
    }

    public function getAccountingTransactionsReport(Array $param)
    {
        global $db, $ecs, $_CFG, $_LANG;
        include_once ROOT_PATH . 'languages/' . $_CFG['lang'] . '/admin/accounting_system.php';

        $sort_by = empty($param['sort_by']) ? "actg_txn_id" : $param['sort_by'];
        $sort_order = empty($param['sort_order']) ? "desc" : $param['sort_order'];

        $where = "WHERE 1 ";
        if (!empty($param['start_date'])) {
            $where .= "AND txn_date > '" . trim($param['start_date']) . "' ";
        }
        if (!empty($param['end_date'])) {
            $where .= "AND txn_date <= '" . trim($param['end_date']) . " 23:59:59' ";
        }
        if ($param['created_by'] != '-1' && isset($param['created_by'])) {
            $where .= "AND created_by = '" . trim($param['created_by']) . "' ";
        }
        if (!empty($param['actg_type_id'])) {
            $where .= "AND (actg_type_id = '" . trim($param['actg_type_id']) . "' OR actg_type_id in (SELECT actg_type_id FROM `actg_accounting_types` WHERE parent_type_id = '" . trim($param['actg_type_id']) . "')) ";
        }
        if (!empty($param['supplier_id'])) {
            $where .= "AND supplier_id = '$param[supplier_id]' ";
        }
        if (!empty($param['wsid'])) {
            $where .= "AND wsid = '$param[wsid]' ";
        }
        if (!empty($param['sa_id'])) {
            $where .= "AND sa_id = '$param[sa_id]' ";
        }
        if (!empty(trim($param['search']))) {
            $search = trim($param['search']);
            preg_match("/FIN(\d+)/", $search, $actg_txn_id);
            if (!empty($actg_txn_id)) {
                $formatted_txn_id = $actg_txn_id[1];
            }
            $where .= "AND (remark LIKE '%" . trim($param['search']) . "%' " .
                        (is_numeric($search) ? "OR amount = '$search' OR exchanged_amount = '$search' OR actg_txn_id = '$search' " : "") .
                        (empty($formatted_txn_id) ? "" : "OR actg_txn_id = $formatted_txn_id ") .
                        ") ";
        }
        if (empty($param['reversed'])) {
            // Show posted record even if reversed
            $where .= "AND posted != -1 ";
            // $where .= "AND posted != -1 AND reversed = 0 ";
        }
        if (isset($param['status']) && $param['status'] != "") {
            $where .= "AND posted = '$param[status]' ";
        }
        if ($param['posted_by'] != '-1' && isset($param['posted_by'])) {
            $where .= "AND actg_txn_id IN (SELECT DISTINCT actg_txn_id FROM `actg_accounting_posting_log` WHERE posted_by = '" . trim($param['posted_by']) . "') ";
        }

        $sql = "SELECT DISTINCT IFNULL(related_txn_id, actg_txn_id) FROM `actg_accounting_transactions` " . $where .
                "ORDER BY $sort_by $sort_order";

        $baseTxns = $db->getCol($sql);

        $allTxns = $db->getAll(
            "SELECT txn.*, type.*, CASE WHEN txn.created_by = -2 THEN 'Yoho Bravo 1' WHEN txn.created_by = 0 THEN '系統生成' ELSE IFNULL(au.user_name, 'Unknown') END as user_name FROM `actg_accounting_transactions` txn " .
            "LEFT JOIN `actg_accounting_types` type ON type.actg_type_id = txn.actg_type_id " .
            "LEFT JOIN (SELECT user_id, user_name FROM " . $ecs->table('admin_user') . ") au ON au.user_id = txn.created_by AND txn.created_by > 0 " .
            "WHERE (actg_txn_id " . db_create_in($baseTxns) . " OR related_txn_id " . db_create_in($baseTxns) . ") " .
            (empty($param['specific']) || empty($param['actg_type_id']) ? "" : "AND (txn.actg_type_id = '" . trim($param['actg_type_id']) . "' OR txn.actg_type_id in (SELECT actg_type_id FROM `actg_accounting_types` WHERE parent_type_id = '" . trim($param['actg_type_id']) . "'))") .
            (empty($param['zero']) ? " AND amount != 0" : "") .
            " ORDER BY CASE WHEN amount >= 0 THEN 1 ELSE 0 END DESC, CASE WHEN amount >= 0 THEN amount ELSE amount * -1 END DESC, actg_txn_id ASC"
        );

        $posted_info = $db->getAll(
            "SELECT apl.*, au.user_name FROM `actg_accounting_posting_log` apl " .
            "LEFT JOIN (SELECT user_id, user_name FROM " . $ecs->table('admin_user') . ") au ON au.user_id = apl.posted_by " .
            "WHERE apl.actg_txn_id " . db_create_in($baseTxns)
        );

        $supplier_list = self::getAccountingSuppliersList();
        foreach ($supplier_list as $supplier) {
            $suppliers[$supplier['supplier_id']] = $supplier['name'];
        }
        $wholesale_list = self::getAccountingWholesaleCustomerList();
        foreach ($wholesale_list as $wholesale) {
            $wholesales[$wholesale['wsid']] = $wholesale['company'];
        }
        $salesagent_list = self::getAccountingSalesAgentList();
        foreach ($salesagent_list as $salesagent) {
            $salesagents[$salesagent['sa_id']] = $salesagent['name'];
        }

        $txnGroup = [];
        foreach ($baseTxns as $txn_id) {
            $txnGroup[$txn_id] = [];
        }
        foreach ($posted_info as $log) {
            $posted_log[$log['actg_txn_id']] = [
                'posted_status' => $log['status'],
                'posted_admin' => $log['user_name'],
                'posted_date' => local_date("Y-m-d", local_strtotime($log['posted_at'])),
            ];
        }
        foreach ($allTxns as $txn) {
            if (in_array($txn['actg_txn_id'], $baseTxns)) {
                $txnGroup[$txn['actg_txn_id']][] = $txn;
            } elseif (in_array($txn['related_txn_id'], $baseTxns)) {
                $txnGroup[$txn['related_txn_id']][] = $txn;
            }
        }
        $class = ["groupOdd", "groupEven"];
        $list = [];
        foreach ($txnGroup as $group_id => $txns) {
            $checked = false;
            $reject = 0;
            foreach ($txns as $txn) {
                if (!empty($txn['supplier_id']) && in_array($txn['actg_type_code'], ['23101', '41100', '43002'])) { // AP - Supplier, Local Purchase, Rebate(Credit note)
                    $txn['actg_type_name'] .= " - " . $suppliers[$txn['supplier_id']];
                }
                if (!empty($txn['wsid']) && in_array($txn['actg_type_code'], ['13110', '31300'])) { // AR - Corporate customer, Sales - B2B
                    $txn['actg_type_name'] .= " - " . $wholesales[$txn['wsid']];
                }
                if (!empty($txn['sa_id']) && in_array($txn['actg_type_code'], ['13130', '28003', '31400', '35001'])) { // AR - B2B2C Customer, Receipt in Advance-B2B2C, Sales - B2B2C, Txn Cost - B2B2C Customers
                    $txn['actg_type_name'] .= " - " . $salesagents[$txn['sa_id']];
                }
                $txn['status'] = $_LANG['account_posting_status'][$txn['posted']];
                $txn['class'] = $class[array_search($group_id, array_keys($txnGroup), true) % 2];
                $txn['group_id'] = $group_id;
                $txn['txn_date'] = local_date("Y-m-d", local_strtotime($txn['txn_date']));
                $amount = floatval($txn['exchanged_amount']);

                if (bccomp($amount, 0, 2) == -1) {
                    $txn['debit'] = "--";
                    $txn['credit'] = "(" . number_format(bcmul($amount, -1, 2), 2) . ")";
                } else {
                    $txn['debit'] = number_format($txn['exchanged_amount'], 2);
                    $txn['credit'] = "--";
                }
                $checked = true;
                if (!empty($posted_log[$group_id])) {
                    $txn = array_merge($txn, $posted_log[$group_id]);
                }
                $list[] = $txn;
            }
        }
        return $list;
    }

    public function generateBankReconciliationReport($transactions)
    {
        $writer = new \XLSXWriter();

        // Headers
        $header = [
            ['編號', '會計紀錄', '','','','','銀行紀錄','','','','差額'],
            ['', '交易編號', '交易日期', '戶口', '備註', '金額', '交易編號', '交易日期', '備註', '金額', '']
        ];

        foreach($header as $row => $item) {
            $writer->writeSheetRow('Sheet1', $item);
        }

        // Header Format
        $writer->markMergedCell('Sheet1', $start_row = 0, $start_col = 0, $end_row = 1, $end_col = 0);
        $writer->markMergedCell('Sheet1', $start_row = 0, $start_col = 1, $end_row = 0, $end_col = 5);
        $writer->markMergedCell('Sheet1', $start_row = 0, $start_col = 6, $end_row = 0, $end_col = 9);
        $writer->markMergedCell('Sheet1', $start_row = 0, $start_col = 10, $end_row = 1, $end_col = 10);

        // Data
        $transactionIdx = 0;
        foreach($transactions as $index => $transaction) {
            // Get current Sheet Row as index
            $currentRow = $writer->countSheetRows();
            $bankRecordCount = count($transaction['bank_rec']);
            $transactionCount = count($transaction['txns']);
            $actgTransactionCount = count($transaction['actg_txns']);
            $rowCount = max($bankRecordCount, $transactionCount, $actgTransactionCount);

            $txn = 'txns';
            if ($transactionCount > 0) {
                $txn = 'txns';
            }
            if ($actgTransactionCount > 0 ) {
                $txn = 'actg_txns';
            }

            for ($i = 0 ; $i < $rowCount; $i++) {
                if ($i === 0) {
                    $row = [
                        ($transactionIdx + 1),
                        $transaction[$txn][$i]['txn_id'],
                        $transaction[$txn][$i]['formatted_txn_date'],
                        $transaction['account_name'],
                        $transaction[$txn][$i]['t_remark'],
                        $transaction[$txn][$i]['formatted_t_amount'],
                        $transaction['bank_rec'][$i]['rec_id'],
                        $transaction['bank_rec'][$i]['formatted_bank_date'],
                        $transaction['bank_rec'][$i]['b_remark'],
                        $transaction['bank_rec'][$i]['formatted_b_amount'],
                        ('HK$'.$transaction['diff'])
                    ];
                } else {
                    $row = [
                        '',
                        (!empty($transaction[$txn][$i])) ? $transaction[$txn][$i]['txn_id'] : '',
                        (!empty($transaction[$txn][$i])) ? $transaction[$txn][$i]['formatted_txn_date']: '',
                        '',
                        (!empty($transaction[$txn][$i])) ? $transaction[$txn][$i]['t_remark'] : '',
                        (!empty($transaction[$txn][$i])) ? $transaction[$txn][$i]['formatted_t_amount'] : '',
                        (!empty($transaction['bank_rec'][$i])) ? $transaction['bank_rec'][$i]['rec_id'] : '',
                        (!empty($transaction['bank_rec'][$i])) ? $transaction['bank_rec'][$i]['formatted_bank_date'] : '',
                        (!empty($transaction['bank_rec'][$i])) ? $transaction['bank_rec'][$i]['b_remark'] : '',
                        (!empty($transaction['bank_rec'][$i])) ? $transaction['bank_rec'][$i]['formatted_b_amount'] : '',
                        ''
                    ];
                }

                $writer->writeSheetRow('Sheet1', $row);
            }

            // Merge cells if necessary
            if ($rowCount > 1) {
                $mergeColumnIds = [0,10];
                if ($transactionCount > 1) {
                    $mergeColumnIds[] = 3;
                } else {
                    array_push($mergeColumnIds, 1, 2, 3, 4, 5);
                }
                if ($bankRecordCount = 1) {
                    array_push($mergeColumnIds, 6, 7, 8, 9);
                }

                foreach($mergeColumnIds as $columnId) {
                    $writer->markMergedCell('Sheet1', $start_row = $currentRow, $start_col = $columnId, $end_row = ($currentRow + $rowCount - 1), $end_col = $columnId);
                }
            }
            $transactionIdx++;
        }

        header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.‌​sheet; charset=utf-8');
        header('Content-Disposition: attachment; filename=' . local_date('YmdHis', gmtime()).'-'.$_REQUEST['act'].'.xlsx');
        header('Cache-Control: max-age=0');

        $writer->writeToStdOut();
    }

    function getAccountingBalanceItems($types, $balance, $level = 1)
    {
        $trial_balance = [];
        foreach ($types as $type) {
            if (array_key_exists($type['actg_type_id'], $balance)) {
                $trial_balance[$type['actg_type_id']] = [
                    'actg_type_id'      => $type['actg_type_id'],
                    'actg_type_name'    => $type['actg_type_name'],
                    'actg_type_code'    => $type['actg_type_code'],
                    'parent_type_id'    => $type['parent_type_id'],
                    'level'             => $level,
                    'amount'            => $balance[$type['actg_type_id']]['amount'],
                    'source'            => empty($balance[$type['actg_type_id']]['source']) ? 0 : 1,
                    'source_debit'      => $balance[$type['actg_type_id']]['source_debit'],
                    'source_credit'     => $balance[$type['actg_type_id']]['source_credit'],
                ];
                if (bccomp($trial_balance[$type['actg_type_id']]['amount'], 0) == 1) {
                    $trial_balance[$type['actg_type_id']]['debit'] = abs($trial_balance[$type['actg_type_id']]['amount']);
                } elseif (bccomp($trial_balance[$type['actg_type_id']]['amount'], 0) == -1) {
                    $trial_balance[$type['actg_type_id']]['credit'] = abs($trial_balance[$type['actg_type_id']]['amount']);
                }
            }
            if (!empty($type['children'])) {
                $children = $this->getAccountingBalanceItems($type['children'], $balance, $level + 1);
                if (!empty($children)) {
                    $trial_balance[$type['actg_type_id']]['children'] = $children;
                }
            }
        }
        return $trial_balance;
    }

    function getAccountingBalance($start_date = null, $end_date = null)
    {
        global $db;

        $sql = "SELECT txn.actg_type_id, parent_type_id, SUM(exchanged_amount) as amount " .
                "FROM `actg_accounting_transactions` txn " .
                "LEFT JOIN `actg_accounting_types` type ON type.actg_type_id = txn.actg_type_id " .
                "WHERE posted != -1 " . (!empty($start_date) ? " AND date(txn_date) >= date('$start_date')" : '') . (!empty($end_date) ? " AND date(txn_date) <= date('$end_date')" : '') .
                "GROUP BY txn.actg_type_id ORDER BY parent_type_id ASC, txn.actg_type_id ASC";
        $res = $db->getAll($sql);
        foreach ($res as $row) {
            $row['source'] = 1;
            if (bccomp($row['amount'], 0) == 1) {
                $row['source_debit'] = abs($row['amount']);
            } elseif (bccomp($row['amount'], 0) == -1) {
                $row['source_credit'] = abs($row['amount']);
            }
            $balance[$row['actg_type_id']] = $row;
        }
        foreach ($balance as $row) {
            if (!empty($row['parent_type_id'])) {
                $parent_id = $row['parent_type_id'];
                while (!empty($parent_id)) {
                    if (array_key_exists($parent_id, $balance)) {
                        $balance[$parent_id]['amount'] = round(bcadd($balance[$parent_id]['amount'], $row['amount'], 4), 2);
                    } else {
                        $balance[$parent_id] = [
                            'amount' => $row['amount'],
                        ];
                    }
                    $parent_id = $db->getOne("SELECT parent_type_id FROM `actg_accounting_types` WHERE actg_type_id = $parent_id");
                }
            }
        }
        $types = self::getAccountingTypes(0, false);
        $trial_balance = $this->getAccountingBalanceItems($types, $balance);

        return $trial_balance;
    }

    function getPostedAccountingTransactions()
    {
        $_REQUEST['status'] = ACTG_POST_APPROVED;
        return self::getAccountingTransactions();
    }
    function postAccountingTransactions($ids = [], $status = 0, $json = true)
    {
        global $db;

        $ids = empty($ids) ? preg_split('/,/', trim($_REQUEST['ids']), -1, PREG_SPLIT_NO_EMPTY) : (is_array($ids) ? $ids : [$ids]);
        $status = empty($status) ? intval($_REQUEST['status']) : $status;

        $valid_txn = $db->getAll("SELECT actg_txn_id, related_txn_id FROM `actg_accounting_transactions` WHERE posted = 0 AND (actg_txn_id " . db_create_in($ids) . " OR related_txn_id " . db_create_in($ids) . ")");

        if (empty($valid_txn)) {
            if ($json) {
                make_json_error("No valid entry selected");
            } else {
                return false;
            }
        }
        if (!in_array($status, self::POSTING_STATUS)) {
            if ($json) {
                make_json_error("Invalid status");
            } else {
                return false;
            }
        }

        $parent_ids = [];
        foreach ($valid_txn as $txn) {
            $db->query("UPDATE `actg_accounting_transactions` SET posted = '$status' WHERE actg_txn_id = '$txn[actg_txn_id]'");
            $parent_id = empty($txn['related_txn_id']) ? $txn['actg_txn_id'] : $txn['related_txn_id'];
            if (!in_array($parent_id, $parent_ids)) {
                $parent_ids[] = $parent_id;
            }

            if ($status == -1) { // When rejecting the accounting entries, release all binded entries
                $txn_group_id = $db->getOne("SELECT txn_group_id FROM `actg_accounting_transactions` WHERE actg_txn_id = '$txn[actg_txn_id]'");
                if (!isset($txn_group_id) || empty($txn_group_id) || intval($txn_group_id) == 0) {

                } else {
                    $this->unlinkBankTransactionRecord($txn_group_id);
                }
            }
        }
        foreach ($parent_ids as $parent_id) {
            $db->query("INSERT INTO `actg_accounting_posting_log` (actg_txn_id, posted_by, status) VALUES ('$parent_id', '$_SESSION[admin_id]', '$status')");
        }

        if ($json) {
            make_json_response('success');
        } else {
            return true;
        }
    }

    function reverseAccountingTransaction($param, $started = false)
    {
        global $db, $ecs;

        if (empty($param)) return false;
		if (!self::param_contains_keys($param, array(
			'actg_txn_id'
        ))) return false;
        $actg_txn = $db->getRow("SELECT * FROM `actg_accounting_transactions` WHERE actg_txn_id = $param[actg_txn_id] AND reversed = 0 AND reversed_txn_id IS NULL");

        if (empty($actg_txn)) {
            return true;
        }

        $reverse_cn = $db->getAll("SELECT * FROM `actg_credit_note_transactions` acnt LEFT JOIN " . $ecs->table('erp_credit_note') . " ecn ON ecn.cn_id = acnt.cn_id WHERE actg_txn_id = $param[actg_txn_id]");
        if (count($reverse_cn) > 0) {
            $erpController = new ErpController();
            $reduce_amount = 0;
            foreach($reverse_cn as $cn_txn) {
                $reverse_cn_txn = [
                    'actg_txn_id' => $param['actg_txn_id'],
                    'related_cn_txn_id' => $cn_txn['cn_txn_id'],
                    'amount' => bcmul($cn_txn['amount'], -1, 2),
                    'remark' => 'Reverse transaction #' . $cn_txn['cn_txn_id']
                ];
                $param_cn = [
                    'cn_id' => $cn_txn['cn_id'],
                    'curr_amount' => bcsub(floatval($cn_txn['curr_amount']), $cn_txn['amount'], 2),
                    'txn_info' => $reverse_cn_txn
                ];
                $erpController->update_credit_note($param_cn);
                $reduce_amount = bcadd($reduce_amount, $cn_txn['amount'], 2);
            }
            if (!empty($actg_txn['erp_order_id'])) {
                if (!$db->getOne("SELECT COUNT(*) FROM " . $ecs->table('erp_order_item') . " WHERE cn_txn_id = $cn_txn[cn_txn_id] AND order_id = $actg_txn[erp_order_id]")) {
                    $db->query("UPDATE " . $ecs->table('erp_order') . " SET `actg_paid_amount` = `actg_paid_amount` + $reduce_amount, `fully_paid` = 0 WHERE order_id = '$actg_txn[erp_order_id]'");
                }
            }
        }
        if (!$started) $this->actg->start_transaction();
        if (empty($actg_txn['posted'])) {
            if (!self::postAccountingTransactions($param['actg_txn_id'], -1, false)) {
                if (!$started) $this->actg->rollback_transaction();
                return false;
            }
            if (!$started) $this->actg->commit_transaction();
            return true;
        } elseif ($actg_txn['posted'] == '1') {
            $actg_txns = $db->getAll("SELECT * FROM `actg_accounting_transactions` WHERE actg_txn_id = $param[actg_txn_id] OR related_txn_id = $param[actg_txn_id]");
            $txn_date = $this->actg->dbdate();
            if (empty($actg_txns)) {
                if (!$started) $this->actg->rollback_transaction();
                return false;
            }
            $related_txn_ids = [];

            foreach ($actg_txns as $actg_txn) {
                $baseTxn = [
                    'currency_code' => $actg_txn['currency_code'],
                    'amount' => bcmul($actg_txn['amount'], -1, 2),
                    'exchanged_amount' => bcmul($actg_txn['exchanged_amount'], -1, 2),
                    'txn_date' => $txn_date,
                    'remark' => empty($param['remark']) ? "Reverse Transaction #$actg_txn[actg_txn_id]" : $param['remark'],
                    'actg_group_id' => 0,
                    'txn_group_id' => 0,
                    'reversed_txn_id' => $actg_txn['actg_txn_id'],
                    'created_by' => $param['created_by'],
                    'posted' => 0,
                ];
                $reverse_txn = $baseTxn;
                $reverse_txn['actg_type_id'] = $actg_txn['actg_type_id'];
                if (!empty($related_txn_ids)) {
                    $reverse_txn['related_txn_id'] = $related_txn_ids[0];
                }
                foreach ($actg_txn as $key => $val) {
                    if (!in_array($key, ['actg_txn_id', 'actg_ref_code', 'created_at', 'related_txn_id']) && !in_array($key, array_keys($baseTxn))) {
                        $reverse_txn[$key] = $val;
                    }
                }
                if ($related_txn_id = self::insertTransaction($reverse_txn)) {
                    $related_txn_ids[] = $related_txn_id;
                    $db->query("UPDATE `actg_accounting_transactions` SET reversed = 1 WHERE actg_txn_id = $actg_txn[actg_txn_id]");
                } else {
                    if (!$started) $this->actg->rollback_transaction();
                    return false;
                }
            }
            if (!$started) $this->actg->commit_transaction();
            return true;
        }
        if (!$started) $this->actg->rollback_transaction();
        return false;
    }

    function getNewActgGroupId()
    {
        return $GLOBALS['db']->getOne('SELECT MAX(actg_group_id) FROM `actg_accounting_transactions`') + 1;
    }

    function getNewTxnGroupId()
    {
        $max_account = $GLOBALS['db']->getOne('SELECT MAX(txn_group_id) FROM `actg_account_transactions`');
        $max_finance = $GLOBALS['db']->getOne('SELECT MAX(txn_group_id) FROM `actg_accounting_transactions`');
        $max_bank = $GLOBALS['db']->getOne('SELECT MAX(txn_group_id) FROM `actg_bank_transactions`');
        return max($max_account, $max_finance, $max_bank) + 1;
    }

    function getCurrencies($param = array(), $opt = array())
    {
        return $this->actg->getCurrencies($param, $opt);
    }

    function getSupplierCode($supplier_id)
    {
        return $GLOBALS['db']->getOne("SELECT code FROM " . $GLOBALS['ecs']->table('erp_supplier') . " WHERE supplier_id = $supplier_id");
    }

    // 0: wholesale, 1: PO
    function getTotalPaid($order_id, $type = 0)
    {
        if (empty($order_id)) return 0;
        if ($type == 0) {
            $paid_amounts = $this->actgHooks->getWholesaleOrderPaidAmounts($order_id);
        } elseif ($type == 1) {
            $paid_amounts = $this->actgHooks->getPurchaseOrderPaidAmounts($order_id);
        }
        return $this->actgHooks->calculateTotalPaidAmount($paid_amounts);
    }

    function money_format($price, $format = '')
    {
        return $this->actg->money_format($price, $format);
    }

    private function param_contains_keys($param, $required_keys)
	{
		foreach($required_keys as $key)
		{
			if (!array_key_exists($key, $param))
			{
				return false;
			}
		}
		return true;
	}

	private function param_filter_keys($param, $allowed_keys)
	{
		foreach(array_keys($param) as $key)
		{
			if (!in_array($key, $allowed_keys))
			{
				unset($param[$key]);
			}
		}
		return $param;
    }

    private function build_where($param, $prefix = '', $mode = 'AND')
	{
		if (is_string($param) && !empty($param)) return $param . ' ';
		if (!is_array($param)) return '';
		$where = '1 ';
		foreach ($param as $key => $value)
		{
			$pf = '';
			if (is_string($prefix))
			{
				$pf = $prefix;
			}
			elseif (is_array($prefix))
			{
				if (array_key_exists($key, $prefix))
				{
					$pf = $prefix[$key];
				}
				elseif (array_key_exists('_', $prefix))
				{
					$pf = $prefix['_'];
				}
			}
			$where .= $mode . " " . $pf . "`" . $key . "` = '" . $value . "' ";
		}
		return $where;
    }

    /**
     * Unlink bank record with transaction
     *
     * @param int $transaction_group_id transaction id (only for auto insert)
     * 
     * @return bool
     */
    private function unlinkBankTransactionRecord($transaction_group_id)
    {
        if (empty($transaction_group_id)) {
            return (false);
        }

        $GLOBALS['db']->query('START TRANSACTION');
        $GLOBALS['db']->query("UPDATE `actg_bank_transactions` SET txn_group_id = 0 WHERE txn_group_id = '$transaction_group_id'");
        $GLOBALS['db']->query("UPDATE `actg_accounting_transactions` SET txn_group_id = 0 WHERE txn_group_id = '$transaction_group_id'");
        $GLOBALS['db']->query("UPDATE `actg_account_transactions` SET txn_group_id = 0 WHERE txn_group_id = '$transaction_group_id'");
        $GLOBALS['db']->query('COMMIT');

        return (true);
    }

    function getSalesAgentPayType($sa_id){
        global $db, $ecs;

        $sql = "SELECT pay_type " .
                "FROM " . $GLOBALS['ecs']->table('sales_agent') . " " .
                "WHERE sa_id = '$sa_id' " .
                "ORDER BY updated_at DESC ";

        $res = $db->getRow($sql);

        if (isset($res['pay_type']) && $res['pay_type'] >= 0) {
            return $res['pay_type'];
        } else {
            return FALSE;
        }
    }

}