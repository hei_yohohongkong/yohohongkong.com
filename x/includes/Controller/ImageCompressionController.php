<?php

/***
* StocktakeController.php
*
*
*
***/
namespace Yoho\cms\Controller;

use Yoho\cms\Model;

use \Exception;

use Yoho\cms\Controller\ErpController;

if (!defined('IN_ECS')) {
    die('Hacking attempt');
}

require_once ROOT_PATH  . '/api/includes/config.php';
require_once ROOT_PATH  . '/includes/tinify/lib/Tinify/Exception.php';
require_once ROOT_PATH  . '/includes/tinify/lib/Tinify/ResultMeta.php';
require_once ROOT_PATH  . '/includes/tinify/lib/Tinify/Result.php';
require_once ROOT_PATH  . '/includes/tinify/lib/Tinify/Source.php';
require_once ROOT_PATH  . '/includes/tinify/lib/Tinify/Client.php';
require_once ROOT_PATH  . '/includes/tinify/lib/Tinify.php';
require_once ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_image.php';

class ImageCompressionController extends YohoBaseController
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function getProductList()
    {
        global $db, $ecs, $userController ,$_CFG;

        $date_period = $_REQUEST['date_period'];

        if (!empty($date_period)) {

            // finished status count 
            $sql = "select count(*) from ". $ecs->table('goods_image_compressions_log')." where date_period = '".$date_period."' and status = 2 ";
            $finished_count = $db->getOne($sql);

            $sql = "select count(*) from ". $ecs->table('goods_image_compressions_log')." where date_period = '".$date_period."' ";
            $count = $db->getOne($sql);

            if ($count == 0) {
                $date_period_ar = explode(',',$date_period);
                $start_time = local_strtotime($date_period_ar[0]);
                $end_time = local_strtotime($date_period_ar[1]);

                // only compress the images that show on front end
                $sql = "select g.goods_id, add_time, g.goods_thumb as main_thumb, egg.img_url as gallery_main, egg.thumb_url as gallery_thumb,  goods_name ";
                $sql .= "from ". $ecs->table('goods') ." g left join ecs_goods_gallery egg on (g.goods_id = egg.goods_id) ";
                $sql .= "where is_delete = 0 ";
                $sql .= "and add_time >= '".$start_time."' and add_time < '".$end_time."' ";
                $sql .= "order by goods_id";
               
                $res = $db->getAll($sql);

                $compress_data=[];
                $current_goods_id = 0;
                foreach ($res as $goods) {
                    if ($current_goods_id != $goods['goods_id']) {
                        $current_goods_id = $goods['goods_id'];
                        if (!empty($goods['main_thumb'])) {
                            $compress_data[] = array ( 
                                            'goods_id' => $goods['goods_id'],
                                            'create_date' => $goods['add_time'],
                                            'goods_name' => $goods['goods_name'],
                                            'image_type' => 'main_thumb',
                                            'image_url' => $goods['main_thumb'],
                                        );
                        }
                    }

                    if (!empty($goods['gallery_main'])) {
                        $compress_data[] = array ( 
                                         'goods_id' => $goods['goods_id'],
                                         'create_date' => $goods['add_time'],
                                         'goods_name' => $goods['goods_name'],
                                         'image_type' => 'gallery_main',
                                         'image_url' => $goods['gallery_main'],
                                    );
                    }

                   
                    if (!empty($goods['gallery_thumb'])) {
                        $compress_data[] = array ( 
                                        'goods_id' => $goods['goods_id'],
                                        'create_date' => $goods['add_time'],
                                        'goods_name' => $goods['goods_name'],
                                        'image_type' => 'gallery_thumb',
                                        'image_url' => $goods['gallery_thumb'],
                                    );
                    }

                }
                // echo '<pre>';
                // echo sizeof($compress_data);
                // print_r($compress_data);

                // insert to log table
                $sql = "select count(*) from ". $ecs->table('goods_image_compressions_log')." where date_period = '".$date_period."' ";
                $count = $db->getOne($sql);
  
                foreach ($compress_data as $data) {
                    // get size
                    $image_path = ROOT_PATH.$data['image_url'];
                    $image_size = filesize($image_path);
                    $sql = "INSERT INTO " . $ecs->table('goods_image_compressions_log') . ' (goods_id, date_period, goods_create_date, goods_name, image_type, image_url, original_size) ' .
                          " VALUES ( '".$data['goods_id']."', '".$date_period."', '".$data['create_date']."', '".$data['goods_name']."', '".$data['image_type']."', '".$data['image_url']."', '".$image_size."' )";
                    $db->query($sql);
                }
            }

            $is_pagination = true;
            $sql = "select * from  ".$ecs->table('goods_image_compressions_log')." where date_period = '".$date_period."' ";
            $res = $db->getAll($sql);
            $_REQUEST['record_count'] = sizeof($res);
            
            $sql = "select * from  ".$ecs->table('goods_image_compressions_log')." where date_period = '".$date_period."' ";
            $sql .= ($is_pagination ? " LIMIT " . $_REQUEST['start'] . ", " . $_REQUEST['page_size'] : '');
            $res = $db->getAll($sql);

            foreach ($res as &$record) {
                $record['original_size'] = round($record['original_size']/1000,3) .'K';
                $record['final_size'] = round($record['final_size']/1000,3) .'K';
                $record['save'] = round($record['save']*100,3) .'%';
                if (!empty($record['goods_create_date'])) {
                    $record['goods_create_date'] = local_date('Y-m-d H:i:s', $record['goods_create_date']);
                }
                if (!empty($record['start_time'])) {
                    $record['start_time'] = local_date('Y-m-d H:i:s', $record['start_time']);
                }
                if (!empty($record['finish_time'])) {
                    $record['finish_time'] = local_date('Y-m-d H:i:s', $record['finish_time']);
                }

                $record['image_url'] = 'https://'.$_SERVER['SERVER_NAME'].'/'.$record['image_url'];
                $record['image'] = '<img width="100" src="'.$record['image_url'].'" />';
    
                if ($record['status'] == 0) {
                    $record['status'] = '未處理';
                } else if ($record['status'] == 1) {
                    $record['status'] = '處理中';
                } else if ($record['status'] == 2) {
                    $record['status'] = '已完成';
                }

                //$record['image_link'] = $_SERVER['SERVER_NAME'].'/'.$record['image_url'];

            }

        }

        $arr = array(
            'data' => $res,
            'record_count' => $_REQUEST['record_count'],
            'extra_data' => array('finished_count'=>$finished_count)
        );

        return $arr;
    }

    public function image_compression_process()
    {
        global $db, $ecs, $userController ,$_CFG;
        $date_period = $_REQUEST['date_period'];
        $batch_count = $_REQUEST['batch_count'];
        $sql = "select * from ". $ecs->table('goods_image_compressions_log') ." where date_period = '".$date_period."' and status = 0 ". (($batch_count > 0)? "limit ".$batch_count : '');
        $rows = $db->GetAll($sql);

        foreach($rows as $row) {
            $sql = "update ".$ecs->table('goods_image_compressions_log')." set status = 1 , start_time = '".gmtime()."' where log_id = ".$row['log_id']." ";
            $db->query($sql);
            $image_path = ROOT_PATH.$row['image_url'];
            $this->image_compression($image_path);
            $image_size = filesize($image_path);
            
            if (!empty($row['original_size']) && $row['original_size'] != 0) {
                $save = round(($row['original_size'] - $image_size )/$row['original_size'],3);
            }else{
                $save = 0;
            }
            
            $sql = "update ".$ecs->table('goods_image_compressions_log')." set status = 2 , finish_time = '".gmtime()."', final_size = '".$image_size."', save = '".$save."' where log_id = ".$row['log_id']." ";
            $db->query($sql);
        }

        return true;
    }

    public function image_compression($image_path)
    {
        $save_path = $image_path;
        $path_parts = pathinfo($image_path);
        $file_name = $path_parts['filename'];
        $new_save_path = $path_parts['dirname'].'/'.$file_name.'_compress.'.$path_parts['extension'];
        $extension = $path_parts['extension'];
      
        if (strtolower($extension) == 'jpg') {
            // use imagemacgick lib to compress
            exec('/usr/bin/convert -quality 90% '.escapeshellarg($image_path) . ' ' .escapeshellarg($image_path));
        } else {
            // go to tiny to compress
            image_compression($image_path,$image_path,true);
        }
    }

    public function getTinyUsagesList($is_pagination = true)
    {
        global $db, $ecs, $userController ,$_CFG;

        // total record
        $sql = 'SELECT count(*) FROM ' . $ecs->table('tiny_usages') . " WHERE 1";
        $sql .= (!isset($_REQUEST['year']) || isset($_REQUEST['year']) && $_REQUEST['year'] == '-1') ? '' : " and year = '".$_REQUEST['year']."'";
        $row = $db->GetAll($sql);
        $total_count = $db->getOne($sql);
        
        $_REQUEST['record_count'] = $total_count;
        $_REQUEST['sort_by']    = empty($_REQUEST['sort_by']) ? 'tiny_usage_id' : trim($_REQUEST['sort_by']);
        $_REQUEST['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
        $_REQUEST['year'] = (!isset($_REQUEST['year']) || isset($_REQUEST['year']) && $_REQUEST['year'] == '-1') ? '-1' : $_REQUEST['year'];
        
        // pagination
        $sql = 'SELECT * FROM ' . $ecs->table('tiny_usages') . " WHERE 1 ";
        $sql .= (!isset($_REQUEST['year']) || isset($_REQUEST['year']) && $_REQUEST['year'] == '-1') ? '' : " and year = '".$_REQUEST['year']."'";
        $sql .= ' order by '. $_REQUEST['sort_by'] . ' ' .  $_REQUEST['sort_order'];
        $sql .= ($is_pagination ? " LIMIT " . $_REQUEST['start'] . ", " . $_REQUEST['page_size'] : '');
        $res = $db->getAll($sql);

        $arr = array(
            'data' => $res,
            'record_count' => $_REQUEST['record_count']
        );

        return $arr;
    }

}
