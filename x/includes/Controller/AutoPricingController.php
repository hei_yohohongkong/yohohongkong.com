<?php

namespace Yoho\cms\Controller;

use function GuzzleHttp\json_decode;


if (!defined('IN_ECS')) {
    die('Hacking attempt');
}
require_once ROOT_PATH . 'includes/lib_order.php';
require_once ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_supplier.php';

class AutoPricingController extends YohoBaseController
{
    const AUTO_PRICING_WEBS = [
        "pricecomhk"    => "Price.com",
        "hktvmall"      => "Hktvmall",
        "fortress"      => "豐澤",
    ];
    const AUTO_PRICING_TABLES = [
        "pricecomhk"    => ["avg_price", "low_price"],
        "hktvmall"      => ["price", "discount"],
        "fortress"      => ["price", "discount"],
    ];
    const AUTO_PRICING_WEBSITES = [
        "pricecomhk"    => "https://www.price.com.hk/product.php?p=",
        "hktvmall"      => "https://www.hktvmall.com/p/",
        "fortress"      => "https://www.fortress.com.hk/zt/product/p/",
    ];
    const AUTO_PRICING_VALUES = [
        "min"   => "最低",
        "max"   => "最高",
        "avg"   => "平均",
    ];
    const AUTO_PRICING_AUTO_ON_SALE_DEAD_SECTION = [
        0     => "非呆壞貨",
        30    => "30天呆壞貨",
        60    => "60天呆壞貨",
        90    => "90天呆壞貨",
        120   => "120天呆壞貨",
        180   => "180天呆壞貨",
    ];
    const AUTO_PRICING_WEBS_COLUMNS = [
        "pricecomhk"    => "avg_price",
        "hktvmall"      => "discount",
        "fortress"      => "discount",
    ];
    const AUTO_PRICING_WEBS_COLUMNS_NAMES = [
        "pricecomhk"    => "平均價",
        "hktvmall"      => "最低價",
        "fortress"      => "最低價",
    ];
    const AUTO_PRICING_TYPES = ["普通定價", "呆壞貨定價"];
    const AUTO_PRICING_RULE_TYPE = ["排除產品"];
    const AUTO_PRICING_DEFAULT_CODE = "default_auto_price";
    const AUTO_PRICING_SUGGEST_TYPES = ["普通定價", "促銷", "VIP價"];
    const DEFAULT_AUTO_PRICE = 1;
    const DEFAULT_AUTO_PRICE_DEAD = 0;
    const DEAD_STOCK_DAYS = 90;
    const AUTO_PRICING_STRATEGY_COLOR = ['blue', 'green', 'purple', 'orange', 'pink'];


    private function _getAllValues()
    {
        return array_merge(self::AUTO_PRICING_VALUES, self::AUTO_PRICING_WEBS);
    }

    private function _getShopConfig($cfg_name = "")
    {
        global $db, $ecs;
        if (!empty($cfg_name)) {
            return $db->getOne("SELECT value FROM " . $ecs->table("shop_config") . " WHERE code = '$cfg_name'");
        }
        return "";
    }

    private function _updateShopConfig($cfg_name = "", $value = "")
    {
        global $db, $ecs;
        if (!empty($cfg_name) && isset($value)) {
            $db->query("UPDATE " . $ecs->table("shop_config") . " SET value = '$value' WHERE code = '$cfg_name'");
            clear_cache_files();
        }
    }

    private function _getDefaultAutoPriceStrategy($cfg_name = "")
    {
        global $_CFG, $db, $ecs;
        if (!empty($cfg_name)) {
            $id = $_CFG[$cfg_name];
            if (empty($_CFG[$cfg_name])) {
                $id = self::_getShopConfig($cfg_name);
            }
            if (!empty($id)) {
                if ($db->getOne("SELECT 1 FROM " . $ecs->table("auto_pricing_strategy") . " WHERE strategy_id = $id")) {
                    return $id;
                }
            }
        }
        return 0;
    }

    public function getAllValues()
    {
        return self::_getAllValues();
    }

    public function getDefaultAutoPriceStrategy($cfg_name = "")
    {
        return self::_getDefaultAutoPriceStrategy($cfg_name);
    }

    public function getAutoPricingList($deadlist = false)
    {
        global $ecs, $db, $_LANG;
        require_once ROOT_PATH . 'includes/lib_howang.php';

        set_time_limit(900); // 15 minutes

        $erpController = new ErpController();
        $sort_by = empty($_REQUEST['sort_by']) ? "goods_id" : $_REQUEST['sort_by'];
        $sort_order = empty($_REQUEST['sort_order']) ? "asc" : $_REQUEST['sort_order'];
        $price_process = ['shop_price', 'cost', 'market_price', 'vip_price', 'suggest_normal_price', 'suggest_vip_price'];
        foreach (array_keys(self::AUTO_PRICING_WEBS) as $web) {
            $price_process[] = $web . "_high";
            $price_process[] = $web . "_low";
        }
        if (in_array($sort_by, array_merge($price_process, ["suggest_sale_price"])) && $sort_order == "asc") {
            $sort_by = "CASE WHEN $sort_by >= 0 THEN $sort_by ELSE 99999999 END";
        }
        $start = empty($_REQUEST['start']) ? 0 : intval($_REQUEST['start']);
        $page_size = $deadlist ? 5000 : (empty($_REQUEST['page_size']) ? 50 : intval($_REQUEST['page_size']));

        $self_supplier_id = get_self_supplier_id(true);
        if (!$_SESSION['manage_cost']) {
            if (count($self_supplier_id) > 0) {
                // $self_goods_sql = self::getSelfSupplierGoodsSql();
                $self_goods_id = get_supplier_goods_ids($self_supplier_id);
            }
        }
        if (!empty($_REQUEST["supplier_id"])) {
            $selected_goods_id = get_supplier_goods_ids([$_REQUEST["supplier_id"]]);
        }

        $exclude_sql = [];
        $exclude_rules = self::getAutoPricingRuleList(true);
        foreach ($exclude_rules as $rule) {
            $cats = empty($rule['config']['cats'][0]) ? [] : $rule['config']['cats'];
            $brands = empty($rule['config']['brands'][0]) ? [] : $rule['config']['brands'];
            $cond = [];
            if (!empty($cats)) {
                $cond[] = "(cat_id IN (SELECT cat_id FROM " . $ecs->table("category") . " WHERE cat_id " . db_create_in($cats) . " OR parent_id " . db_create_in($cats) . "))";
            }
            if (!empty($brands)) {
                $cond[] = "brand_id " . db_create_in($brands);
            }
            $exclude_sql[] = "(" . implode(" AND ", $cond) . ")";
        }
        $exclude_goods = empty($exclude_sql) ? [] : $db->getCol("SELECT goods_id FROM " . $ecs->table("goods") . (" WHERE " . implode(" OR ", $exclude_sql)));

        $days_before = self::DEAD_STOCK_DAYS;
        $time_yesterday = local_strtotime("today");
        $thirty_days_before = local_strtotime("-30 days midnight");
        $time_days_before = local_strtotime("-$days_before days midnight");
        $web_sql = self::getWebSql();
        $stock_cost_log = false;
        $max_cost_log_time = $db->getOne("SELECT MAX(report_date) FROM " . $ecs->table('stock_cost_log') . " WHERE type = 4");
        if (!empty($max_cost_log_time))  {
            $stock_cost_log = true;
        }
    
        // for FLS start
        $goods_warehouse_where = '';
        if (!empty($_REQUEST['goods_warehouse'])) {
            switch ($_REQUEST['goods_warehouse'])
            {
                case 'FLS':
                    $goods_warehouse_where .= " AND is_fls_warehouse = 1";
                    break;
            }
        }
        // for FLS end
        $count = $db->getOne(
            "SELECT COUNT(DISTINCT goods_id) FROM " . $ecs->table('goods'). " as g " .
            "WHERE g.is_delete = 0 AND g.is_on_sale = 1 AND g.is_real = 1 " .
            ($_SESSION['manage_cost'] || $_SESSION['admin_id'] == 71 ? "" : $self_goods_sql) .
            (empty($_REQUEST["cat_id"]) ? "" : "AND " . get_children($_REQUEST["cat_id"])) .
            (empty($_REQUEST["brand_id"]) ? "" : "AND g.brand_id = $_REQUEST[brand_id] ") .
            (empty($_REQUEST["supplier_id"]) ? "" : ("AND g.goods_id " . db_create_in($selected_goods_id) . " ")) .
            (empty($_REQUEST["goods_name"]) ? "" : self::searchGoodsByKeyword($_REQUEST['goods_name'])) .
            (empty($_REQUEST["include_goods"]) ? "" : self::searchGoodsByIncludeGoods($_REQUEST['include_goods'])) .
            (empty($goods_warehouse_where) ? "" : $goods_warehouse_where)
        );

        //  $sql_base = "SELECT *, CASE WHEN ndayssales > 0 AND goods_number > 0 AND last_act_time < $time_days_before THEN goods_number / ndayssales * $days_before WHEN ndayssales = 0 AND goods_number > 0 AND last_act_time < $time_days_before THEN goods_number * 100 WHEN ndayssales > 0 AND goods_number > 0 THEN goods_number / ndayssales * -$days_before ELSE -1 END as dead_day FROM (SELECT g.goods_id, g.goods_name, g.shop_price, g.cost, g.market_price, g.price_strategy, g.pre_sale_days, acl.difference, acl.log_id as abnormal_id, g.add_time, g.auto_pricing, " .
        $sql_base = "SELECT *, " .
        ($stock_cost_log
            ? "CASE WHEN deadgroup < 0 THEN deadqty * 1000 WHEN deadgroup > 0 THEN deadqty / ndayssales * $days_before ELSE deadqty END as dead_day "
            : "CASE WHEN ndayssales > 0 AND goods_number > 0 AND first_act_time < $time_days_before THEN goods_number / ndayssales * $days_before WHEN ndayssales = 0 AND goods_number > 0 AND first_act_time < $time_days_before THEN goods_number * 100 WHEN ndayssales > 0 AND goods_number > 0 THEN goods_number / ndayssales * -$days_before ELSE -1 END as dead_day "
        ) .
        "FROM (SELECT g.goods_id, g.goods_name, g.shop_price, g.cost, g.market_price, g.price_strategy, g.pre_sale_days, acl.difference, acl.log_id as abnormal_id, g.add_time, g.auto_pricing, " .
                    "g.is_promote, g.promote_start_date, g.promote_end_date, g.promote_price, " .
                    "IFNULL(mp.user_price, -1) as vip_price, " .
                    "IFNULL(sn.suggest_price, -1) as suggest_normal_price, " .
                    "IFNULL(sd.suggest_price, -1) as suggest_sale_price, " .
                    "IFNULL(sd.suggest_duration, -1) as suggest_sale_duration, " .
                    "IFNULL(sv.suggest_price, -1) as suggest_vip_price, " .
                    "IFNULL(es.first_act_time, g.add_time) as first_act_time, " .
                    $web_sql .
                    "IFNULL((SELECT sum(ega.goods_qty) FROM " . $ecs->table('erp_goods_attr_stock') . " as ega WHERE ega.goods_id = g.goods_id AND ega.warehouse_id IN (".$erpController->getSellableWarehouseIds(true).")), 0) as goods_number, " .
                    \hw_reserved_number_subquery('g.') . ", " .
                    ($stock_cost_log
                        ? (
                            "IFNULL(scl.thirtydayssales, 0) as thirtydayssales, " .
                            "IFNULL(scl.ndayssales, 0) as ndayssales, " .
                            "IFNULL(scl.deadqty, 0) as deadqty, " .
                            "IFNULL(scl.deadgroup, 0) as deadgroup "
                        )
                        : (
                            "IFNULL((SELECT SUM(og.goods_number) FROM " . $ecs->table('order_goods') . " as og LEFT JOIN " . $ecs->table('order_info') . " as oi ON oi.order_id = og.order_id WHERE og.goods_id = g.goods_id " . order_query_sql('finished', 'oi.') . " AND (oi.shipping_time BETWEEN " . $thirty_days_before . " AND " . $time_yesterday . ")), 0) as thirtydayssales, " .
                            "IFNULL((SELECT SUM(og.goods_number) FROM " . $ecs->table('order_goods') . " as og LEFT JOIN " . $ecs->table('order_info') . " as oi ON oi.order_id = og.order_id WHERE og.goods_id = g.goods_id " . order_query_sql('finished', 'oi.') . " AND (oi.shipping_time BETWEEN " . $time_days_before . " AND " . $time_yesterday . ")), 0) as ndayssales "
                        )) .
                "FROM " . $ecs->table('goods'). " as g " .
                "LEFT JOIN (SELECT goods_id, user_price FROM " . $ecs->table('member_price'). " WHERE user_rank = 2) as mp ON mp.goods_id = g.goods_id " .
                "LEFT JOIN (SELECT goods_id, suggest_price FROM " . $ecs->table('auto_pricing_suggestion') . " WHERE suggest_type = " . AUTO_PRICE_TYPE_NORMAL . " AND status = " . AUTO_PRICE_STATUS_SUGGESTED . ") as sn ON sn.goods_id = g.goods_id " .
                "LEFT JOIN (SELECT goods_id, suggest_price, suggest_duration FROM " . $ecs->table('auto_pricing_suggestion') . " WHERE suggest_type = " . AUTO_PRICE_TYPE_DEAD . " AND status = " . AUTO_PRICE_STATUS_SUGGESTED . ") as sd ON sd.goods_id = g.goods_id " .
                "LEFT JOIN (SELECT goods_id, suggest_price FROM " . $ecs->table('auto_pricing_suggestion') . " WHERE suggest_type = " . AUTO_PRICE_TYPE_VIP . " AND status = " . AUTO_PRICE_STATUS_SUGGESTED . ") as sv ON sv.goods_id = g.goods_id " .
                "LEFT JOIN (SELECT goods_id, MAX(log_id) as log_id FROM " . $ecs->table('abnormal_cost_log') . " WHERE is_archived = 0 AND original_cost != 0 AND ABS(difference) > 0.1 GROUP BY goods_id) as macl ON macl.goods_id = g.goods_id " .
                "LEFT JOIN (SELECT log_id, difference FROM " . $ecs->table('abnormal_cost_log') . ") as acl ON acl.log_id = macl.log_id " .
                "LEFT JOIN (SELECT goods_id, MAX(act_time) as last_act_time, Min(act_time) as first_act_time FROM " . $ecs->table('erp_stock') . " WHERE stock_style = 'w' AND w_d_style_id = 1 GROUP BY goods_id) as es ON es.goods_id = g.goods_id " .
                (
                    $stock_cost_log ? "LEFT JOIN (SELECT type_id, type, report_date, ndayssales_30 as thirtydayssales, ndayssales_$days_before as ndayssales, deadqty_$days_before as deadqty, deadgroup_$days_before as deadgroup FROM " . $ecs->table("stock_cost_log") . " WHERE type = 4 AND report_date = $max_cost_log_time GROUP BY report_date, type, type_id) scl ON scl.type_id = g.goods_id AND type = 4 AND report_date = $max_cost_log_time " : ""
                ) .
                "WHERE g.is_delete = 0 AND g.is_on_sale = 1 AND g.is_real = 1 " .
                // ($_SESSION['manage_cost'] || $_SESSION['admin_id'] == 71 ? "" : $self_goods_sql) .
                (empty($_REQUEST["cat_id"]) ? "" : "AND " . get_children($_REQUEST["cat_id"])) .
                (empty($_REQUEST["brand_id"]) ? "" : "AND g.brand_id = $_REQUEST[brand_id] ") .
                (empty($_REQUEST["supplier_id"]) ? "" : ("AND g.goods_id " . db_create_in($selected_goods_id) . " ")) .
                (empty($_REQUEST["goods_name"]) ? "" : self::searchGoodsByKeyword($_REQUEST['goods_name'])) .
                (empty($_REQUEST["include_goods"]) ? "" : self::searchGoodsByIncludeGoods($_REQUEST['include_goods'])) .
                (empty($goods_warehouse_where) ? "" : $goods_warehouse_where) .
                ") temp ";
        $sql = $sql_base . (empty($sort_by) ? "" : "ORDER BY $sort_by $sort_order ") . "LIMIT $start, $page_size";
        $res = $db->getAll($sql);

        if ($deadlist) {
            $start += $page_size;
            while ($start < $count) {
                $sql = $sql_base . (empty($sort_by) ? "" : "ORDER BY $sort_by $sort_order ") . "LIMIT $start, $page_size";
                $res = array_merge($res, $db->getAll($sql));
                $start += $page_size;
            }
            $deadstock90_goods = [];
            foreach ($res as $key => $row) {
                if (($stock_cost_log && empty($row['deadgroup'])) || (!$stock_cost_log && (empty($row['goods_number']) || $row['first_act_time'] > $time_days_before))) {
                    // ignore new goods or goods with lately purchase or goods with no stock
                } elseif (($stock_cost_log && $row['deadgroup'] == -1) || (!$stock_cost_log && empty($row['ndayssales']))) {
                    // ignore dead stock goods with no sales
                } else {
                    if ($stock_cost_log) {
                        $r_day = round(($row['deadqty'] + $row['ndayssales']) / $row['ndayssales'] * $days_before);
                    } else {
                        $r_day = round($row['goods_number'] / $row['ndayssales'] * $days_before);
                    }
                    if($r_day >= 90) {
                        $deadstock90_goods[] = $row["goods_id"];
                    }
                }
            }
            return $deadstock90_goods;
        } else {
            $strategies = self::getAutoPricingStrategyList();
            $strategy_names = [];
            foreach ($strategies as $strategy) {
                $strategy_names[$strategy['strategy_id']] = $strategy['strategy_name'];
            }
            foreach ($res as $key => $row) {
                foreach ($row as $name => $val) {
                    if (in_array($name, $price_process)) {
                        if ($val == -1) {
                            $res[$key][$name] = "--";
                        } else {
                            $res[$key][$name] = sprintf('%0.2f', $val);
                        }
                    }
                }
                //$row['last_act_time'] => 產品最近的入貨時間(如有)，添加產品的時間
                //產品無入貨時間，按照之前，用添加產品的時間$row['add_time']
                if (($stock_cost_log && empty($row['deadgroup'])) || (!$stock_cost_log && (empty($row['goods_number']) || $row['first_act_time'] > $time_days_before))) {
                    $res[$key]['dead_stock'] = "--";
                } elseif (($stock_cost_log && $row['deadgroup'] == -1) || (!$stock_cost_log && empty($row['ndayssales']))) {
                    $res[$key]['dead_stock'] = "∞";
                } else {
                    if ($stock_cost_log) {
                        $r_day = round(($row['deadqty'] + $row['ndayssales']) / $row['ndayssales'] * $days_before);
                        $d_group = $row['deadgroup'];
                    } else {
                        $r_day = round($row['goods_number'] / $row['ndayssales'] * $days_before);
                        $d_group = ($r_day - $r_day % 30) / 30;
                    }
                    $res[$key]['dead_group'] = $d_group;
                    $res[$key]['dead_stock'] = $r_day >= 180 ? "<span title='$r_day'>180</span>" : $d_group ;
                }
                $res[$key]['goods_number'] = ($row['goods_number'] - $row['reserved_number']);
                $res[$key]['last30_sold'] = $row['thirtydayssales'];
                $promote_start_date = local_date('Y-m-d H:i:s');
                $promote_end_date = local_date('Y-m-d H:i:s', local_strtotime("$promote_start_date +1 month"));
                if (!empty($row['promote_start_date'])) {
                    $promote_start_date = local_date('Y-m-d H:i:s', $row['promote_start_date']);
                }
                if (!empty($row['promote_end_date'])) {
                    $promote_end_date = local_date('Y-m-d H:i:s', $row['promote_end_date']);
                }
                $is_promote = 0;
                if ($row['is_promote'] && $row['promote_end_date'] > gmtime()) {
                    $is_promote = 1;
                }
                $res[$key]['promote_detail'] = "<span class='set_promote fa fa-" . ($is_promote ? "check green" : "times red") . "' data-on='$is_promote' data-gid='$row[goods_id]' data-sd='$promote_start_date' data-ed='$promote_end_date' data-p='$row[promote_price]' title='" . ($is_promote ? "促銷至$promote_end_date" : "未進行促銷") . "'></span>";
                // auto price platform price
                foreach (array_keys(self::AUTO_PRICING_WEBS) as $web) {
                    $res[$key][$web . '_formatted'] = $res[$key][$web . '_high'];
                    if ($res[$key][$web . '_high'] != "--") {
                        if ($res[$key][$web . '_high'] < $row['cost']) {
                            $res[$key][$web . '_formatted'] = "<span class='red' title='入貨成本過高'>" . $res[$key][$web . '_formatted'] . "</span>";
                        }
                    }
                    if ($res[$key][$web . '_low'] != "--" && $res[$key][$web . '_low'] != $res[$key][$web . '_high']) {
                        if ($res[$key][$web . '_low'] < $row['cost']) {
                            $res[$key][$web . '_low'] = "<span class='red' title='入貨成本過高'>" . $res[$key][$web . '_low'] . "</span>";
                        }
                        $res[$key][$web . '_formatted'] .= " | " . $res[$key][$web . '_low'];
                    }

                    if ($res[$key][$web . '_formatted'] != "--") {
                        $res[$key][$web . '_formatted'] = "<span class='edit-web' data-web='$web' data-gid='$row[goods_id]'>" . $res[$key][$web . '_formatted'] . '</span>';
                    }
                }

                // auto price on sale info
                $res[$key]['on_sale'] = "--";
                if ($row['suggest_sale_duration'] != -1 && $row['suggest_sale_price'] != -1) {
                    $res[$key]['on_sale'] = "$row[suggest_sale_price] | $row[suggest_sale_duration]日";
                }
                if ($row['price_strategy'] == -1 || in_array($row['goods_id'], $exclude_goods)) {
                    $res[$key]['formatted_price_strategy'] = "<span class='red fa fa-unlink' title='不處理'></span>";
                } elseif (empty($row['price_strategy'])) {
                    $res[$key]['formatted_price_strategy'] = "<span data-sid='" . self::getDefaultAutoPriceStrategy("default_auto_price") . "' data-gid='$row[goods_id]' class='toggle_auto grey fa fa-link' title='預設策略'></span>";
                } else {
                    $res[$key]['formatted_price_strategy'] = "<span data-sid='$row[price_strategy]' data-gid='$row[goods_id]' class='toggle_auto " . self::AUTO_PRICING_STRATEGY_COLOR[(intval($row['price_strategy']) - 1) % count(self::AUTO_PRICING_STRATEGY_COLOR)] . " fa fa-link' title='" . $strategy_names[$row['price_strategy']] . "'></span>";
                }

                // hide cost if not self supplier
                if (!$_SESSION['manage_cost']) {
                    if (!check_authz(AdminuserController::DB_ACTION_CODE_ONLYSELFSUPPLIERS) || !in_array($row['goods_id'], $self_goods_id)) {
                        $res[$key]['cost'] = "--";
                    }
                }

                // abnormal cost
                if (!empty($row['difference']) && $res[$key]['cost'] != "--") {
                    $diff = round($row['difference'] * 100, 2);
                    $res[$key]['cost'] = "<a href='javascript:showAbnormal($row[abnormal_id])'><span class='" . ($diff > 0 ? "red" : $diff < 0 ? "green" : "") . "' title='$diff%'>" . $res[$key]['cost'] .  "</span></a>";
                }
            }
            return [
                "list"      => $res,
                "count"     => $count,
            ];
        }
    }

    public function getDeadStockList()
    {
        // rewrite getDeadStockList to getAutoPricingList()
        return $this->getAutoPricingList(true);

        // 20190725: codes below are not used
        global $ecs, $db, $_LANG;
        require_once ROOT_PATH . 'includes/lib_howang.php';

        set_time_limit(900); // 15 minutes

        $erpController = new ErpController();
        $sort_by = empty($_REQUEST['sort_by']) ? "goods_id" : $_REQUEST['sort_by'];
        $sort_order = empty($_REQUEST['sort_order']) ? "asc" : $_REQUEST['sort_order'];
        $price_process = ['shop_price', 'cost', 'market_price', 'vip_price', 'suggest_normal_price', 'suggest_vip_price'];
        foreach (array_keys(self::AUTO_PRICING_WEBS) as $web) {
            $price_process[] = $web . "_high";
            $price_process[] = $web . "_low";
        }
        if (in_array($sort_by, array_merge($price_process, ["suggest_sale_price", "dead_day"])) && $sort_order == "asc") {
            $sort_by = "CASE WHEN $sort_by >= 0 THEN $sort_by ELSE 99999999 END";
        }
        $start = empty($_REQUEST['start']) ? 0 : intval($_REQUEST['start']);
        $page_size = empty($_REQUEST['page_size']) ? 50 : intval($_REQUEST['page_size']);

        $self_supplier_id = get_self_supplier_id(true);
        if (!$_SESSION['manage_cost']) {
            if (count($self_supplier_id) > 0) {
                // $self_goods_sql = self::getSelfSupplierGoodsSql();
                $self_goods_id = get_supplier_goods_ids($self_supplier_id);
            }
        }
        if (!empty($_REQUEST["supplier_id"])) {
            $selected_goods_id = get_supplier_goods_ids([$_REQUEST["supplier_id"]]);
        }

        $exclude_sql = "";
        $exclude_rules = self::getAutoPricingRuleList(true);
        foreach ($exclude_rules as $rule) {
            $cats = empty($rule['config']['cats'][0]) ? [] : $rule['config']['cats'];
            $brands = empty($rule['config']['brands'][0]) ? [] : $rule['config']['brands'];
            $cond = [];
            if (!empty($cats)) {
                $cond[] = "(cat_id IN (SELECT cat_id FROM " . $ecs->table("category") . " WHERE cat_id " . db_create_in($cats) . " OR parent_id " . db_create_in($cats) . "))";
            }
            if (!empty($brands)) {
                $cond[] = "brand_id " . db_create_in($brands);
            }
            $exclude_sql[] = "(" . implode(" AND ", $cond) . ")";
        }
        $exclude_goods = empty($exclude_sql) ? [] : $db->getCol("SELECT goods_id FROM " . $ecs->table("goods") . (" WHERE " . implode(" OR ", $exclude_sql)));

        $days_before = self::DEAD_STOCK_DAYS;
        $time_yesterday = local_strtotime("today");
        $thirty_days_before = local_strtotime("-30 days midnight");
        $time_days_before = local_strtotime("-$days_before days midnight");
        $web_sql = self::getWebSql();
    
        // for FLS start
        $goods_warehouse_where = '';
        if (!empty($_REQUEST['goods_warehouse'])) {
            switch ($_REQUEST['goods_warehouse'])
            {
                case 'FLS':
                    $goods_warehouse_where .= " AND is_fls_warehouse = 1";
                    break;
            }
        }
        // for FLS end
        $size = 5000;
        $count = $db->getOne(
            "SELECT COUNT(DISTINCT goods_id) FROM " . $ecs->table('goods'). " as g " .
            "WHERE g.is_delete = 0 AND g.is_on_sale = 1 AND g.is_real = 1 " .
            ($_SESSION['manage_cost'] || $_SESSION['admin_id'] == 71 ? "" : $self_goods_sql) .
            (empty($_REQUEST["cat_id"]) ? "" : "AND " . get_children($_REQUEST["cat_id"])) .
            (empty($_REQUEST["brand_id"]) ? "" : "AND g.brand_id = $_REQUEST[brand_id] ") .
            (empty($_REQUEST["supplier_id"]) ? "" : ("AND g.goods_id " . db_create_in($selected_goods_id) . " ")) .
            (empty($_REQUEST["goods_name"]) ? "" : self::searchGoodsByKeyword($_REQUEST['goods_name'])) .
            (empty($_REQUEST["include_goods"]) ? "" : self::searchGoodsByIncludeGoods($_REQUEST['include_goods'])) .
            (empty($goods_warehouse_where) ? "" : $goods_warehouse_where) 
        );
        $times = ceil($count/$size);
        $res = [];
        $sql_base = "SELECT *, CASE WHEN ndayssales > 0 AND goods_number > 0 AND add_time < $time_days_before THEN goods_number / ndayssales * $days_before ELSE -1 END as dead_day FROM (SELECT g.goods_id, g.goods_name, g.shop_price, g.cost, g.market_price, g.price_strategy, acl.difference, acl.log_id as abnormal_id, g.add_time, g.auto_pricing, " .
                    "g.is_promote, g.promote_start_date, g.promote_end_date, g.promote_price, " .
                    "IFNULL(mp.user_price, -1) as vip_price, " .
                    "IFNULL(sn.suggest_price, -1) as suggest_normal_price, " .
                    "IFNULL(sd.suggest_price, -1) as suggest_sale_price, " .
                    "IFNULL(sd.suggest_duration, -1) as suggest_sale_duration, " .
                    "IFNULL(sv.suggest_price, -1) as suggest_vip_price, " .
                    $web_sql .
                    "IFNULL((SELECT sum(ega.goods_qty) FROM " . $ecs->table('erp_goods_attr_stock') . " as ega WHERE ega.goods_id = g.goods_id AND ega.warehouse_id IN (".$erpController->getSellableWarehouseIds(true).")), 0) as goods_number, " .
                    \hw_reserved_number_subquery('g.') . ", " .
                    "IFNULL((SELECT SUM(og.goods_number) FROM " . $ecs->table('order_goods') . " as og LEFT JOIN " . $ecs->table('order_info') . " as oi ON oi.order_id = og.order_id WHERE og.goods_id = g.goods_id " . order_query_sql('finished', 'oi.') . " AND (oi.shipping_time BETWEEN " . $thirty_days_before . " AND " . $time_yesterday . ")), 0) as thirtydayssales, " .
                    "IFNULL((SELECT SUM(og.goods_number) FROM " . $ecs->table('order_goods') . " as og LEFT JOIN " . $ecs->table('order_info') . " as oi ON oi.order_id = og.order_id WHERE og.goods_id = g.goods_id " . order_query_sql('finished', 'oi.') . " AND (oi.shipping_time BETWEEN " . $time_days_before . " AND " . $time_yesterday . ")), 0) as ndayssales " .
                "FROM " . $ecs->table('goods'). " as g " .
                "LEFT JOIN (SELECT goods_id, user_price FROM " . $ecs->table('member_price'). " WHERE user_rank = 2) as mp ON mp.goods_id = g.goods_id " .
                "LEFT JOIN (SELECT goods_id, suggest_price FROM " . $ecs->table('auto_pricing_suggestion') . " WHERE suggest_type = " . AUTO_PRICE_TYPE_NORMAL . " AND status = " . AUTO_PRICE_STATUS_SUGGESTED . ") as sn ON sn.goods_id = g.goods_id " .
                "LEFT JOIN (SELECT goods_id, suggest_price, suggest_duration FROM " . $ecs->table('auto_pricing_suggestion') . " WHERE suggest_type = " . AUTO_PRICE_TYPE_DEAD . " AND status = " . AUTO_PRICE_STATUS_SUGGESTED . ") as sd ON sd.goods_id = g.goods_id " .
                "LEFT JOIN (SELECT goods_id, suggest_price FROM " . $ecs->table('auto_pricing_suggestion') . " WHERE suggest_type = " . AUTO_PRICE_TYPE_VIP . " AND status = " . AUTO_PRICE_STATUS_SUGGESTED . ") as sv ON sv.goods_id = g.goods_id " .
                "LEFT JOIN (SELECT goods_id, MAX(log_id) as log_id FROM " . $ecs->table('abnormal_cost_log') . " WHERE is_archived = 0 AND original_cost != 0 AND ABS(difference) > 0.1 GROUP BY goods_id) as macl ON macl.goods_id = g.goods_id " .
                "LEFT JOIN " . $ecs->table('abnormal_cost_log') . " as acl ON acl.log_id = macl.log_id " .
                "WHERE g.is_delete = 0 AND g.is_on_sale = 1 AND g.is_real = 1 " .
                // ($_SESSION['manage_cost'] || $_SESSION['admin_id'] == 71 ? "" : $self_goods_sql) .
                (empty($_REQUEST["cat_id"]) ? "" : "AND " . get_children($_REQUEST["cat_id"])) .
                (empty($_REQUEST["brand_id"]) ? "" : "AND g.brand_id = $_REQUEST[brand_id] ") .
                (empty($_REQUEST["supplier_id"]) ? "" : ("AND g.goods_id " . db_create_in($selected_goods_id) . " ")) .
                (empty($_REQUEST["goods_name"]) ? "" : self::searchGoodsByKeyword($_REQUEST['goods_name'])) .
                (empty($_REQUEST["include_goods"]) ? "" : self::searchGoodsByIncludeGoods($_REQUEST['include_goods'])) .
                (empty($goods_warehouse_where) ? "" : $goods_warehouse_where) .
                ") temp ";
        for($i=0; $i<$times; $i++) {
            $start = $size * $i;
            $sql = $sql_base . (empty($sort_by) ? "" : "ORDER BY $sort_by $sort_order ") . "LIMIT $start, $size";
            $result = $db->getAll($sql);
            foreach ($result as $key => $row) {  
                array_push($res, $row);
            }  
        }  
        $deadstock90_goods = [];
        foreach ($res as $key => $row) {
            if (empty($row['goods_number']) || $row['add_time'] > $time_days_before) {
                // $res[$key]['dead_stock'] = "--";
            } elseif (empty($row['ndayssales'])) {
                // $res[$key]['dead_stock'] = "∞";
            } else {
                $r_day = round($row['goods_number'] / $row['ndayssales'] * $days_before);
                $d_group = ($r_day - $r_day % 30) / 30;
                $res[$key]['dead_group'] = $d_group;
                $res[$key]['dead_stock'] = $r_day >= 180 ? 180 : $d_group * 30;
                if($res[$key]['dead_stock']>=90) {
                    array_push($deadstock90_goods,$res[$key]["goods_id"]);
                }
            }        
        }
        return $deadstock90_goods;
    }
    public function getAutoPricingProduct()
    {
        global $ecs, $db;
        if (empty($_REQUEST['id']) && empty($_REQUEST['ids'])) {
            sys_msg("請選擇產品");
        }
        if (!empty($_REQUEST['id'])) {
            $good = $db->getRow("SELECT goods_id, goods_name, price_strategy FROM " . $ecs->table("goods") . " WHERE goods_id = $_REQUEST[id]");
            $good['strategy_ids'] =  explode(",", $good['price_strategy']);
            foreach (array_keys(self::AUTO_PRICING_WEBS) as $key) {
                $res = $db->getRow("SELECT * FROM " . $ecs->table("goods_$key") . " WHERE goods_id = $_REQUEST[id]");
                $good['id'][$key]       = $res[$key . '_id'];
                $good['min'][$key]      = $key == 'pricecomhk' ? $res['low_price'] : $res['discount'];
                $good['max'][$key]      = $key == 'pricecomhk' ? $res['avg_price'] : $res['price'];
                $good['water'][$key]    = $key == 'pricecomhk' ? $res['is_water'] : 0;
            }
        } elseif (!empty($_REQUEST['ids'])) {
            $good = $db->getRow("SELECT GROUP_CONCAT(goods_id) as goods_id, GROUP_CONCAT(goods_name) as goods_name FROM " . $ecs->table("goods") . " WHERE goods_id IN ($_REQUEST[ids])");
        }
        return $good;
    }

    public function updateAutoPricingProduct()
    {
        global $ecs, $db, $_LANG;
        if (empty($_REQUEST['goods_id'])) {
            sys_msg("請選擇產品", 1);
        }
        $pricecomhk_water = empty($_REQUEST['pricecomhk_water']) ? 0 : intval($_REQUEST['pricecomhk_water']);
        foreach (array_keys(self::AUTO_PRICING_WEBS) as $key) {
            if (!empty($_REQUEST[$key])) {
                $db->query("INSERT INTO " . $ecs->table("goods_$key") . " (goods_id, " . $key . "_id" . ($key == "pricecomhk" ? ", is_water" : "") . ") VALUES ($_REQUEST[goods_id], '" . $_REQUEST[$key] . "'" . ($key == "pricecomhk" ? ", $pricecomhk_water" : "") . ") ON DUPLICATE KEY UPDATE " . $key . "_id = '" . $_REQUEST[$key] . "'" . ($key == "pricecomhk" ? ", is_water = $pricecomhk_water" : ""));
            }
        }

        if (empty($_REQUEST['batch'])) {
            $this->auto_pricing_single_product($_REQUEST[goods_id]);
        }

        $price_strategy = empty($_REQUEST['strategy_select']) ? -1 : intval($_REQUEST['price_strategy']);
        $db->query("UPDATE " . $ecs->table("goods") . " SET price_strategy = $price_strategy WHERE goods_id " . (empty($_REQUEST['batch']) ? "= $_REQUEST[goods_id]" : "IN ($_REQUEST[goods_id])"));
        sys_msg("已更新產品定價策略", 0, [["href" => "auto_pricing.php?act=list", "text" => $_LANG['auto_pricing_product_list']]]);
    }

    public function getAutoPricingStrategyList()
    {
        global $ecs, $db;
        $all_values = self::_getAllValues();
        $strategies = $db->getAll("SELECT * FROM " . $ecs->table("auto_pricing_strategy"));
        foreach ($strategies as $key => $strategy) {
            $config = \json_decode($strategy['config'], true);
            $formatted_reference = [];
            foreach ($config['reference'] as $k => $web) {
                $formatted_reference[] = self::AUTO_PRICING_WEBS[$web];
            }
            $formatted_reference = implode(", ", $formatted_reference);
            $formatted_config = [];
            if (!empty($config['normal'])) {
                $formatted_config[] = "<span>參考 <b>$formatted_reference</b></span><br>" .
                                "<span>當門店價格" . ($config['condition_percent'] == 0 ? "不等於" : "相差") . " <b>" . $all_values[$config["condition"]] . "</b> 價" . ($config['condition_percent'] == 0 ? "" : "多於 <b>$config[condition_percent]</b>% ") . "時,</span><br>" .
                                // "<span>當門店價格高於 <b>" . $all_values[$config["condition"]] . "</b> 價" . ($config['condition_percent'] == 100 ? "" : "之 <b>$config[condition_percent]</b>% ") . "時,</span><br>" .
                                "<span>調整門店價格為 <b>" . $all_values[$config["select"]] . "</b> 價" . ($config['percent'] == 100 ? "" : "之 <b>$config[percent]</b>%") . ",</span><br>" .
                                "<span>且 " . (empty($config['price_limit']) ? "<b>沒有最低價格</b>" : "價格不低於 <b>成本 " . ($config['price_limit']['amount'] != 0 ? (($config['price_limit']['amount'] > 0 ? "+" : "-") . ($config['price_limit']['type'] == 1 ? " $" : "") . abs($config['price_limit']['amount']) . ($config['price_limit']['type'] == 2 ? "%" : "")) : "")) . "</b></span>";
            }
            if (!empty($config['on_sale'])) {
                $on_sale = $config['on_sale'];
                if (!empty($on_sale['amount']['default'])) {
                    $formatted_on_sale_amount = self::priceOrPercent($on_sale['amount']['default'], $on_sale['type']);
                } else {
                    $formatted_on_sale_amount = self::priceOrPercent(min($on_sale['amount']), $on_sale['type']) . " - " . self::priceOrPercent(max($on_sale['amount']), $on_sale['type']);
                }
                $formatted_config[] = "<span>每次自動促銷維持 <b>$on_sale[duration]</b> 日,</span><br>" .
                                     "<span>促銷幅度為當前毛利減 <b>$formatted_on_sale_amount</b>,</span><br>" .
                                     "<span>直至產品變為 <b>" . self::AUTO_PRICING_AUTO_ON_SALE_DEAD_SECTION[$on_sale['target']] . "</b></span>";
            }
            if (!empty($config['vip']) && !empty($config['vip_percent'])) {
                $formatted_config[] = "<span>VIP價調整為新售價 / 促銷價之 <b>$config[vip_percent]%</b></span>";
            }
            $strategies[$key]["formatted_config"] = "<div style='display: inline-block; text-align: left'>" . implode("<br><br>", $formatted_config) . "</div>";
        }
        return $strategies;
    }

    public function priceOrPercent($val, $type) {
        if ($type == 1) {
            return "$$val";
        } else {
            return "$val%";
        }
    }

    public function getAutoPricingStrategy($strategy_id)
    {
        global $ecs, $db, $_LANG;
        $strategy = $db->getRow("SELECT * FROM " . $ecs->table("auto_pricing_strategy") . " WHERE strategy_id = $strategy_id");
        if (empty($strategy)) {
            sys_msg("找不到此策略", 1, [["href" => "auto_pricing.php?act=strategy_list", "text" => $_LANG['auto_pricing_strategy_list']]]);
        }
        $config = \json_decode($strategy['config'], true);
        foreach ($config['reference'] as $item) {
            $strategy[$item] = 1;
        }
        $strategy["reference"] = $config["reference"];
        $strategy["condition"] = $config["condition"];
        $strategy["condition_percent"] = $config["condition_percent"];
        $strategy["select"] = $config["select"];
        $strategy["percent"] = $config["percent"];
        $strategy["price_limit"] = $config["price_limit"];
        $strategy["normal"] = $config["normal"];
        $strategy["on_sale"] = $config["on_sale"];
        $strategy["vip"] = $config["vip"];
        $strategy["vip_percent"] = $config["vip_percent"];
        return $strategy;
    }

    public function updateAutoPricingStrategy()
    {
        global $ecs, $db, $_LANG;
        $strategy_name = trim($_REQUEST['strategy_name']);
        $enable = empty($_REQUEST['enable']) ? 0 : intval($_REQUEST['enable']);
        if (empty($strategy_name)) {
            sys_msg("請填寫策略名稱", 1);
        }
        if (!empty($_REQUEST["auto_normal"])) {
            $config['normal'] = 1;
            $config['reference'] = $_REQUEST['value_reference'];
            $config["condition"] = $_REQUEST["value_condition"];
            $config["condition_percent"] = $_REQUEST["value_condition_percent"];
            $config["select"] = $_REQUEST["value_select"];
            $config["percent"] = $_REQUEST["value_percent"];
            if (!empty($_REQUEST["price_limit"])) {
                $config["price_limit"]["type"] = $_REQUEST["price_limit_type"];
                $config["price_limit"]["amount"] = $_REQUEST["price_limit_amount"];
            }
        }
        if (!empty($_REQUEST["auto_on_sale"])) {
            $config["on_sale"]["duration"] = $_REQUEST["on_sale_duration"];
            $config["on_sale"]["type"] = $_REQUEST["on_sale_type"];
            $config["on_sale"]["target"] = $_REQUEST["on_sale_target"];
            if (!empty($_REQUEST["on_sale_amount_default"])) {
                $config["on_sale"]["amount"]["default"] = $_REQUEST["on_sale_amount_default"];
            } else {
                foreach (array_keys(self::AUTO_PRICING_AUTO_ON_SALE_DEAD_SECTION) as $key) {
                    if ($key > 0) {
                        $config["on_sale"]["amount"][$key] = $_REQUEST["on_sale_amount_$key"];
                    }
                }
            }
        }
        if (!empty($_REQUEST["auto_vip"])) {
            $config['vip'] = 1;
            $config['vip_percent'] = empty($_REQUEST["vip_percent"]) ? 90 :intval($_REQUEST["vip_percent"]);
        }
        $json_config = json_encode($config);
        if (empty($_REQUEST['strategy_id'])) {
            if ($db->getOne("SELECT 1 FROM " . $ecs->table("auto_pricing_strategy") . " WHERE strategy_name = '$strategy_name'")) {
                sys_msg("已存在此策略名稱", 1);            
            }
            $db->query("INSERT INTO " . $ecs->table("auto_pricing_strategy") . " (strategy_name, config, enable) VALUES ('$strategy_name', '$json_config', $enable)");
        } else {
            if (!$db->getOne("SELECT 1 FROM " . $ecs->table("auto_pricing_strategy") . " WHERE strategy_id = $_REQUEST[strategy_id]")) {
                sys_msg("不存在此策略", 1);            
            }
            $db->query("UPDATE " . $ecs->table("auto_pricing_strategy") . " SET strategy_name = '$strategy_name', config = '$json_config', enable = $enable WHERE strategy_id = $_REQUEST[strategy_id]");
        }
        sys_msg("已更新策略", 0, [["href" => "auto_pricing.php?act=strategy_list", "text" => $_LANG['auto_pricing_strategy_list']]]);
    }

    public function isEnabledStrategy($strategy_id)
    {
        global $db, $ecs;
        return $db->getOne("SELECT 1 FROM " . $ecs->table("auto_pricing_strategy") . " WHERE strategy_id = $strategy_id AND enable != 0");
    }

    public function getAutoPricingRuleList($exclude = false)
    {
        global $db, $ecs;
        $list = $db->getAll("SELECT * FROM " . $ecs->table("auto_pricing_rules") . ($exclude ? " WHERE enable != 0 AND type = 0" : ""));
        foreach ($list as $key => $rule) {
            $list[$key]['formatted_type'] = self::AUTO_PRICING_RULE_TYPE[$rule['type']];
            $config = \json_decode($rule['config'], true);
            $cat_names = $db->getOne("SELECT GROUP_CONCAT(cat_name) FROM " . $ecs->table("category") . " WHERE cat_id " . db_create_in($config['cats']));
            $brand_names = $db->getOne("SELECT GROUP_CONCAT(brand_name) FROM " . $ecs->table("brand") . " WHERE brand_id " . db_create_in($config['brands']));
            $list[$key]['config'] = $config;
            $list[$key]['formatted_config'] = (!empty($cat_names) ? "分類: <b>$cat_names</b><br>" : "") . (!empty($brand_names) ? "品牌: <b>$brand_names</b>" : "");
        }
        return $list;
    }

    public function getAutoPricingRule($rule_id)
    {
        global $db, $ecs;
        $rule = $db->getRow("SELECT * FROM " . $ecs->table("auto_pricing_rules") . " WHERE rule_id = $rule_id");
        if (empty($rule)) {
            sys_msg("找不到此規則", 1, [["href" => "auto_pricing.php?act=rule_list", "text" => $_LANG['auto_pricing_rule_list']]]);
        }
        $rule['config'] = \json_decode($rule['config'], true);
        $rule['config']['cat_ids'] = implode(",", $rule['config']['cats']);
        $rule['config']['brand_ids'] = implode(",", $rule['config']['brands']);
        return $rule;
    }

    public function updateAutoPricingRule()
    {
        global $ecs, $db, $_LANG;
        $rule_name = trim($_REQUEST['rule_name']);
        if (empty($rule_name)) {
            sys_msg("請填寫規則名稱", 1);
        }
        if (!isset($_REQUEST['rule_type']) || !in_array($_REQUEST['rule_type'], array_keys(self::AUTO_PRICING_RULE_TYPE))) {
            sys_msg("沒有此規則種類", 1);
        } else {
            $rule_type = intval($_REQUEST['rule_type']);
        }
        $enable = empty($_REQUEST['enable']) ? 0 : $_REQUEST['enable'];
        $rule_type = empty($_REQUEST['rule_type']) ? 0 : $_REQUEST['rule_type'];
        $config['cats'] = explode(",", $_REQUEST['cat_ids']);
        $config['brands'] = explode(",", $_REQUEST['brand_ids']);
        $json_config = json_encode($config);
        if (empty($_REQUEST['rule_id'])) {
            if ($db->getOne("SELECT 1 FROM " . $ecs->table("auto_pricing_rules") . " WHERE rule_name = '$rule_name'")) {
                sys_msg("已存在此規則名稱", 1);            
            }
            $db->query("INSERT INTO " . $ecs->table("auto_pricing_rules") . " (rule_name, type, enable, config) VALUES ('$rule_name', $rule_type, $enable, '$json_config')");
        } else {
            if (!$db->getOne("SELECT 1 FROM " . $ecs->table("auto_pricing_rules") . " WHERE rule_id = $_REQUEST[rule_id]")) {
                sys_msg("不存在此規則", 1);            
            }
            $db->query("UPDATE " . $ecs->table("auto_pricing_rules") . " SET rule_name = '$rule_name', type = $rule_type, enable = $enable, config = '$json_config' WHERE rule_id = $_REQUEST[rule_id]");
        }
        sys_msg("已更新規則", 0, [["href" => "auto_pricing.php?act=rule_list", "text" => $_LANG['auto_pricing_rule_list']]]);
    }

    public function getCategoryList()
    {
        global $db, $ecs;
        $cat_list = cat_list(0, 0, false);
        foreach ($cat_list as $key => $row) {
            $list[$key]['cat_id'] = $row['cat_id'];
            $list[$key]['cat_name'] = str_repeat('&nbsp;', $row['level'] * 4) . $row['cat_name'];
            $list[$key]['to_lower'] = strtolower($row['cat_name']);
        }
        return $list;
    }

    public function getBrandList()
    {
        global $db, $ecs;
        $list = $db->getAll("SELECT brand_id, brand_name FROM " . $ecs->table("brand"));
        foreach ($list as $key => $row) {
            $list[$key]['to_lower'] = strtolower($row['brand_name']);
        }
        return $list;
    }

    public function getSupplierList()
    {
        $self_suppliers = get_admin_supplier($_SESSION['admin_id'], true);
        foreach ($self_suppliers as $row) {
            $supplier_list[] = [
                'supplier_id' => $row['supplier_id'],
                'supplier_name' => $row['name'],
            ];
        }
        return $self_suppliers;
    }

    public function removeAutoPricingRule()
    {
        global $db, $ecs;
        $rule_id = $_REQUEST['rule_id'];
        if ($db->query("DELETE FROM " . $ecs->table("auto_pricing_rules") . " WHERE rule_id = $rule_id")) {
            make_json_response("1");
        } else {
            make_json_error("無法刪除此記錄");
        }
    }

    public function updateAutoPricingDefaultStrategy()
    {
        global $_LANG;
        self::_updateShopConfig(self::AUTO_PRICING_DEFAULT_CODE, $_REQUEST["price_strategy"]);
        sys_msg("已更新預設定價策略", 0, [["href" => "auto_pricing.php?act=strategy_list", "text" => $_LANG['auto_pricing_strategy_list']]]);
    }

    public function getAutoPricingSuggestion()
    {
        global $db, $ecs, $_LANG;
        $erpController = new ErpController();
        $sort_by = empty($_REQUEST['sort_by']) ? "suggest_id" : $_REQUEST['sort_by'];
        $sort_order = empty($_REQUEST['sort_order']) ? "asc" : $_REQUEST['sort_order'];
        $start = empty($_REQUEST['start']) ? 0 : intval($_REQUEST['start']);
        $page_size = empty($_REQUEST['page_size']) ? 50 : intval($_REQUEST['page_size']);

        $where = " WHERE 1 " .
                 (isset($_REQUEST['status']) ? " AND status = $_REQUEST[status] " : "") .
                 (isset($_REQUEST['suggest_type']) ? " AND suggest_type = $_REQUEST[suggest_type] " : "");

        // $self_supplier_id = get_self_supplier_id();
        if (!$_SESSION['manage_cost']) {
            $self_goods_sql = self::getSelfSupplierGoodsSql();
        }
        if (!empty($_REQUEST["supplier_id"])) {
            $selected_goods_id = get_supplier_goods_ids([$_REQUEST["supplier_id"]]);
        }

        // for FLS start
        $goods_warehouse_where = '';
        if (!empty($_REQUEST['goods_warehouse'])) {
            switch ($_REQUEST['goods_warehouse'])
            {
                case 'FLS':
                    $goods_warehouse_where .= " AND is_fls_warehouse = 1";
                    break;
            }
        }
        // for FLS end

        $web_sql = self::getWebSql(false);
        $sql_base = "SELECT aps.*, g.goods_name, g.cost, g.shop_price, g.market_price, " .
                $web_sql .
                "IFNULL((SELECT sum(ega.goods_qty) FROM " . $ecs->table('erp_goods_attr_stock') . " as ega WHERE ega.goods_id = g.goods_id AND ega.warehouse_id IN (".$erpController->getSellableWarehouseIds(true).")), 0) as goods_number, " .
                "IFNULL(mp.user_price, -1) as vip_price " .
                "FROM " . $ecs->table("auto_pricing_suggestion") . " aps " .
                "LEFT JOIN " . $ecs->table("goods") . " g ON g.goods_id = aps.goods_id " .
                "LEFT JOIN (SELECT goods_id, user_price FROM " . $ecs->table('member_price'). " WHERE user_rank = 2) as mp ON mp.goods_id = g.goods_id " .
                $where .
                ($_SESSION['manage_cost'] || $_SESSION['admin_id'] == 71 ? "" : $self_goods_sql) .
                (empty($_REQUEST["cat_id"]) ? "" : "AND " . get_children($_REQUEST["cat_id"])) .
                (empty($_REQUEST["brand_id"]) ? "" : "AND g.brand_id = $_REQUEST[brand_id] ") .
                (empty($_REQUEST["supplier_id"]) ? "" : ("AND g.goods_id " . db_create_in($selected_goods_id) . " ")) .
                (empty($_REQUEST["goods_name"]) ? "" : self::searchGoodsByKeyword($_REQUEST['goods_name'])) .
                (empty($_REQUEST["include_goods"]) ? "" : self::searchGoodsByIncludeGoods($_REQUEST['include_goods'])) .
                (empty($goods_warehouse_where) ? "" : $goods_warehouse_where) .
                (empty($sort_by) ? "" : " ORDER BY $sort_by $sort_order");
        $res = $db->getAll($sql_base . " LIMIT $start, $page_size");
        $price_process = ['shop_price', 'cost', 'market_price', 'vip_price'];
        foreach (array_keys(self::AUTO_PRICING_WEBS) as $web) {
            $price_process[] = $web . "_price";
        }
        die($sql_base . " LIMIT $start, $page_size");
        foreach ($res as $key => $row) {
            foreach ($row as $name => $val) {
                if (in_array($name, $price_process)) {
                    if ($val == -1) {
                        $res[$key][$name] = "--";
                    } else {
                        $res[$key][$name] = sprintf('%0.2f', $val);
                    }
                }
            }

            $prices = [];
            foreach (array_keys(self::AUTO_PRICING_WEBS) as $web) {
                if ($row[$web . "_price"] != -1) {
                    $prices[] = $row[$web . "_price"];
                }
            }

            $strategy = self::getAutoPricingStrategy($row['strategy_id']);
            $selected_price = 0;
            if (in_array($strategy['select'], array_keys(self::AUTO_PRICING_VALUES))) {
                $res[$key]['selected_value'] = self::AUTO_PRICING_VALUES[$strategy['select']];
                if ($strategy['select'] == 'min') {
                    $selected_price = min($prices);
                } elseif ($strategy['select'] == 'max') {
                    $selected_price = max($prices);
                }
            } else {
                $res[$key]['selected_value'] = self::AUTO_PRICING_WEBS[$strategy['select']] . " " . self::AUTO_PRICING_WEBS_COLUMNS_NAMES[$strategy['select']];
            }

            foreach (array_keys(self::AUTO_PRICING_WEBS) as $web) {
                if ($strategy['select'] == $web || $selected_price == $res[$key][$web . "_price"]) {
                    $res[$key][$web . "_price"] = "<span class='" . ( $row[$web . "_price"] != -1 ? (round($res[$key][$web . "_price"]) > round($row['cost']) ? "green" : "red") : "") . "'>" . $res[$key][$web . "_price"]  . "</span>";
                }
            }
            $res[$key]['formatted_gp'] = round((floatval($row['suggest_price']) - floatval($row['cost'])) / floatval($row['suggest_price']) * 100) . "%";
            $res[$key]['formatted_status'] = $_LANG['formatted_status'][$row['status']];
            $res[$key]['_action'] = "--";
            if ($row['status'] == AUTO_PRICE_STATUS_SUGGESTED) {
                $custom_actions = [
                    'accept' => [
                        'link'  => "javascript:updateSuggestion($row[suggest_id], " . AUTO_PRICE_STATUS_APPLIED . ")",
                        'title' => '接受建議',
                        'label' => '接受',
                        'btn_class' => 'btn btn-success',
                        'icon_class' => 'fa fa-check'
                    ],
                    'reject' => [
                        'link'  => "javascript:updateSuggestion($row[suggest_id], " . AUTO_PRICE_STATUS_REJECTED . ")",
                        'title' => '忽略建議',
                        'label' => '忽略',
                        'btn_class' => 'btn btn-danger',
                        'icon_class' => 'fa fa-times'
                    ]
                ];
                $res[$key]["batch_select$_REQUEST[suggest_type]"] = "<td><input class='flat' type='checkbox' name='batch$_REQUEST[suggest_type]' value='$row[suggest_id]'></td>";
                $res[$key]['_action'] = self::generateCrudActionBtn($row['suggest_id'], 'id', null, [], $custom_actions);
            }
        }
        return [
            "list" => $res,
            "count" => $db->getOne(
                "SELECT COUNT(aps.suggest_id) FROM " . $ecs->table("auto_pricing_suggestion") . " aps " .
                "LEFT JOIN " . $ecs->table("goods") . " g ON g.goods_id = aps.goods_id " .
                $where .
                ($_SESSION['manage_cost'] || $_SESSION['admin_id'] == 71 ? "" : $self_goods_sql) .
                (empty($_REQUEST["cat_id"]) ? "" : "AND " . get_children($_REQUEST["cat_id"])) .
                (empty($_REQUEST["brand_id"]) ? "" : "AND g.brand_id = $_REQUEST[brand_id] ") .
                (empty($_REQUEST["supplier_id"]) ? "" : ("AND g.goods_id " . db_create_in($selected_goods_id) . " ")) .
                (empty($_REQUEST["goods_name"]) ? "" : self::searchGoodsByKeyword($_REQUEST['goods_name'])) . 
                (empty($_REQUEST["include_goods"]) ? "" : self::searchGoodsByIncludeGoods($_REQUEST['include_goods'])) .
                (empty($goods_warehouse_where) ? "" : $goods_warehouse_where) 
            )
        ];
    }

    public function updateAutoPricingSuggestion($suggest_id = 0, $step = -1, $silent = false)
    {
        global $db, $ecs, $_LANG;
        include_once ROOT_PATH . 'languages/zh_tw/admin/auto_pricing.php';
        $suggest_id = empty($_REQUEST['suggest_id']) ? (empty($suggest_id) ? 0 : $suggest_id) : $_REQUEST['suggest_id'];
        $step = !isset($_REQUEST['step']) ? ($step == -1 ? -1 : $step) : $_REQUEST['step'];
        if (empty($suggest_id)) {
            if ($silent) {
                return false;
            }
            make_json_error("請選擇建議");
        }

        if (!in_array($step, array_keys($_LANG['formatted_status']))) {
            if ($silent) {
                return false;
            }
            make_json_error("請選擇有效的行動");
        }

        $suggest_ids = explode(',', $suggest_id);
        foreach ($suggest_ids as $suggest_id) {
            $suggest = $db->getRow("SELECT * FROM " . $ecs->table("auto_pricing_suggestion") . " WHERE suggest_id = $suggest_id");
            if (empty($suggest)) {
                if ($silent) {
                    return false;
                }
                make_json_error("沒有此建議紀錄");
            }

            $erpController = new ErpController();
            $strategy = self::getAutoPricingStrategy($suggest['strategy_id']);
            $days_before = self::DEAD_STOCK_DAYS;
            $time_yesterday = local_strtotime("today");
            $time_days_before = local_strtotime("-$days_before days midnight");

            $sql = "SELECT g.goods_id, g.goods_name, g.goods_sn, g.shop_price, g.cost, g.market_price, g.promote_price, " .
                    "IFNULL(mp.user_price, -1) as vip_price, " .
                    "IFNULL((SELECT sum(ega.goods_qty) FROM " . $ecs->table('erp_goods_attr_stock') . " as ega WHERE ega.goods_id = g.goods_id AND ega.warehouse_id IN (".$erpController->getSellableWarehouseIds(true).")), 0) as goods_number, " .
                    "IFNULL((SELECT SUM(og.goods_number) FROM " . $ecs->table('order_goods') . " as og LEFT JOIN " . $ecs->table('order_info') . " as oi ON oi.order_id = og.order_id WHERE og.goods_id = g.goods_id " . order_query_sql('finished', 'oi.') . " AND (oi.shipping_time BETWEEN " . $time_days_before . " AND " . $time_yesterday . ")), 0) as ndayssales " .
                    "FROM " . $ecs->table('goods'). " as g " .
                    "LEFT JOIN (SELECT goods_id, user_price FROM " . $ecs->table('member_price'). " WHERE user_rank = 2) as mp ON mp.goods_id = g.goods_id " .
                    "WHERE g.is_delete = 0 AND g.is_on_sale = 1 AND g.is_real = 1 AND g.add_time < $time_days_before " .
                    "AND g.goods_id = $suggest[goods_id]";
            $goods = $db->getRow($sql);
            $dead_group = 0;
            if (empty($goods['ndayssales'])) {
                $dead_group = "180";
            } else {
                $r_day = round($goods['goods_number'] / $goods['ndayssales'] * $days_before);
                $d_group = ($r_day - $r_day % 30) / 30;
                $dead_group = $r_day >= 180 ? "180" : $d_group * 30;
            }
            $expire_date = date("Y-m-d H:i:s");

            $db->query("START TRANSACTION");
            if ($step == AUTO_PRICE_STATUS_APPLIED) {
                $weekday = date("w");
                if ($weekday < 3) {
                    $suggest_day = 3 - $weekday;
                } elseif ($weekday < 6) {
                    $suggest_day = 6 - $weekday;
                } elseif ($weekday == 6) {
                    $suggest_day = 4;
                }
                $expire_date = date("Y-m-d H:i:s", strtotime("+$suggest_day days midnight"));

                // Log goods changes
                $sql = "INSERT INTO " . $ecs->table('goods_log') .
                    " SELECT NULL, '" . gmtime() . "', '" . (empty($_SESSION['admin_name']) ? 'admin' : $_SESSION['admin_name']) . "', g.* " .
                    "FROM " . $ecs->table('goods') . " as g " .
                    "WHERE goods_id = '$suggest[goods_id]'";
                $db->query($sql);

                if ($suggest['suggest_type'] == AUTO_PRICE_TYPE_NORMAL) {
                    $db->query("UPDATE " . $ecs->table("goods") . " SET shop_price = $suggest[suggest_price] WHERE goods_id = $suggest[goods_id]");
                } elseif ($suggest['suggest_type'] == AUTO_PRICE_TYPE_DEAD) {
                    $expire_date = date("Y-m-d H:i:s", strtotime("+$suggest[suggest_duration] days midnight"));
                    $db->query("UPDATE " . $ecs->table("goods") . " SET promote_price = $suggest[suggest_price], is_promote = 1, promote_start_date = '" . local_strtotime("now") . "', promote_end_date = '" . local_strtotime($expire_date) . "' WHERE goods_id = $suggest[goods_id]");
                } elseif ($suggest['suggest_type'] == AUTO_PRICE_TYPE_VIP) {
                    if ($db->getOne("SELECT 1 FROM " . $ecs->table("member_price") . " WHERE goods_id = $suggest[goods_id] AND user_rank = 2")) {
                        $db->query("UPDATE " . $ecs->table("member_price") . " SET user_price = $suggest[suggest_price] WHERE goods_id = $suggest[goods_id] AND user_rank = 2");
                    } else {
                        $db->query("INSERT INTO " . $ecs->table("member_price") . " (user_price, goods_id, user_rank) VALUES ($suggest[suggest_price], $suggest[goods_id], 2)");
                    }
                }
            }
            if ($suggest['suggest_type'] == AUTO_PRICE_TYPE_NORMAL) {
                $old_price = $goods['shop_price'];
                if ($step == AUTO_PRICE_STATUS_APPLIED) {
                    if (@include_once ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_notifications.php') {
                        notify_price_changed(
                            [
                                'goods_id' => $suggest['goods_id'],
                                'goods_name' => $goods['goods_name'],
                                'goods_sn' => $goods['goods_sn'],
                            ], $old_price, $suggest['suggest_price']
                        );
                    } else {
                        error_log(__FILE__.':'.__LINE__.' Cannot include lib_notifications.php');
                    }

                    // Log the price changes
                    $db->autoExecute(
                        $ecs->table('goods_price_log'), array(
                            'goods_id' => $suggest['goods_id'],
                            'old_price' => $old_price,
                            'new_price' => $suggest['suggest_price'],
                            'update_time' => gmtime(),
                            'admin_id' => empty($_SESSION['admin_id']) ? 1 : $_SESSION['admin_id']
                        ), 'INSERT'
                    );
                }
            } elseif ($suggest['suggest_type'] == AUTO_PRICE_TYPE_DEAD) {
                $old_price = $goods['promote_price'];
                $db->autoExecute(
                    $ecs->table('goods_price_log'), array(
                        'goods_id' => $suggest['goods_id'],
                        'old_price' => $old_price,
                        'new_price' => $suggest['suggest_price'],
                        'update_time' => gmtime(),
                        'admin_id' => 0
                    ), 'INSERT'
                );
            } elseif ($suggest['suggest_type'] == AUTO_PRICE_TYPE_VIP) {
                $old_price = $goods['vip_price'];
            }
            if (empty($old_price)) {
                $old_price = 0;
            }
            $db->query("INSERT INTO " . $ecs->table("auto_pricing_log") . " (suggest_id, goods_id, strategy_id, suggest_type, log_type, old_price, new_price, dead_group, expire_date) VALUES ($suggest_id, $suggest[goods_id], $strategy[strategy_id], $suggest[suggest_type], $step, $old_price, $suggest[suggest_price], $dead_group, '$expire_date')");
            $db->query("UPDATE " . $ecs->table("auto_pricing_suggestion") . " SET status = $step WHERE suggest_id = $suggest_id");
            $db->query("COMMIT");
        }
        if ($silent) {
            return true;
        }
        make_json_response("1");
    }

    public function getAutoPricingPlatformPrice()
    {
        global $db, $ecs;
        if (empty($_REQUEST['goods_id'])) {
            make_json_error("請選擇產品");
        }
        if (empty($_REQUEST['web'])) {
            make_json_error("請選擇平台");
        }
        $result = $db->getRow("SELECT * FROM " . $ecs->table("goods_$_REQUEST[web]") . " WHERE goods_id = $_REQUEST[goods_id]");
        $res = [
            'web' => $_REQUEST['web'],
            'goods_id' => $_REQUEST['goods_id'],
            'link' => self::AUTO_PRICING_WEBSITES[$_REQUEST['web']] . $result[$_REQUEST['web'] . "_id"],
            'min' => $result[self::AUTO_PRICING_TABLES[$_REQUEST['web']][1]],
            'max' => $result[self::AUTO_PRICING_TABLES[$_REQUEST['web']][0]],
        ];
        make_json_response($res);
    }

    public function updateAutoPricingPlatformPrice()
    {
        global $db, $ecs;
        if (empty($_REQUEST['goods_id'])) {
            make_json_error("請選擇產品");
        }
        if (empty($_REQUEST['web'])) {
            make_json_error("請選擇平台");
        }

        if ($_REQUEST['act'] == "delete_web") {
            $db->query("DELETE FROM " . $ecs->table("goods_$_REQUEST[web]") . " WHERE goods_id = $_REQUEST[goods_id]");
        } else {
            $db->query("UPDATE " . $ecs->table("goods_$_REQUEST[web]") . " SET " . self::AUTO_PRICING_TABLES[$_REQUEST['web']][1] . " = $_REQUEST[min], " . self::AUTO_PRICING_TABLES[$_REQUEST['web']][0] . " = $_REQUEST[max] WHERE goods_id = $_REQUEST[goods_id]");
        }
        make_json_response(1);
    }

    public function getWebSql($all = true)
    {
        $web_sql = "";
        $columns = $all ? self::AUTO_PRICING_TABLES : self::AUTO_PRICING_WEBS_COLUMNS;
        foreach ($columns as $key => $col) {
            $curr_table = "goods_$key";
            if ($all) {
                $web_sql .= "IFNULL((SELECT $col[0] FROM " . $GLOBALS['ecs']->table($curr_table) . " as $curr_table WHERE $curr_table.goods_id = g.goods_id AND $curr_table.$col[0] > 0), -1) as " . $key . "_high, " .
                            "IFNULL((SELECT $col[1] FROM " . $GLOBALS['ecs']->table($curr_table) . " as $curr_table WHERE $curr_table.goods_id = g.goods_id AND $curr_table.$col[1] > 0), -1) as " . $key . "_low, ";
            } else {
                $web_sql .= "IFNULL((SELECT $col FROM " . $GLOBALS['ecs']->table($curr_table) . " as $curr_table WHERE $curr_table.goods_id = g.goods_id AND $curr_table.$col > 0), -1) as " . $key . "_price, ";
            }
        }
        return $web_sql;
    }

    public function getSelfSupplierGoodsSql()
    {
        $where = "";
        if (!$_SESSION['manage_cost'] && check_authz(AdminuserController::DB_ACTION_CODE_ONLYSELFSUPPLIERS)) {
            $self_supplier_id = get_self_supplier_id(true);
            if (count($self_supplier_id) > 0) {
                $where = "SELECT goods_id FROM " . $GLOBALS['ecs']->table('erp_goods_supplier') .
                            " WHERE supplier_id " . db_create_in($self_supplier_id);
            }
        }
        return "AND g.goods_id IN ($where)";
    }

    public function updateAutoPricingGoodsAuto()
    {
        global $db, $ecs;
        if (!empty($_REQUEST['goods_id']) && isset($_REQUEST['status'])) {
            $db->query("UPDATE " . $ecs->table("goods") . " SET auto_pricing = $_REQUEST[status] WHERE goods_id = $_REQUEST[goods_id]");
        }
        make_json_response(1);
    }

    public function updateAutoPricingGoodsStrategy()
    {
        global $db, $ecs;
        if (!empty($_REQUEST['goods_id']) && !empty($_REQUEST['strategy_id'])) {
            $strategy = $db->getRow("SELECT strategy_id, strategy_name FROM " . $ecs->table("auto_pricing_strategy") . " WHERE strategy_id > $_REQUEST[strategy_id] AND enable = 1 ORDER BY strategy_id ASC LIMIT 1");
            if (empty($strategy)) {
                $strategy = $db->getRow("SELECT strategy_id, strategy_name FROM " . $ecs->table("auto_pricing_strategy") . " WHERE enable = 1 ORDER BY strategy_id ASC LIMIT 1");
            }
            $db->query("UPDATE " . $ecs->table("goods") . " SET price_strategy = $strategy[strategy_id] WHERE goods_id = $_REQUEST[goods_id]");
            make_json_response("<span data-sid='$strategy[strategy_id]' data-gid='$_REQUEST[goods_id]' class='toggle_auto " . self::AUTO_PRICING_STRATEGY_COLOR[(intval($strategy['strategy_id']) - 1) % count(self::AUTO_PRICING_STRATEGY_COLOR)] . " fa fa-link' title='$strategy[strategy_name]'></span>");
        }
        make_json_error("Something went wrong");
    }

    function searchGoodsByKeyword($keyword = "", $alias = 'g.')
    {
        $where = " AND ({$alias}goods_sn LIKE '%" . mysql_like_quote($keyword) . "%' ";
        $where .= " OR {$alias}goods_name LIKE '%" . mysql_like_quote($keyword) . "%' ";
        $tmp = preg_split('/\s+/', $keyword);
        if (count($tmp) > 1)
        {
            $where .= " OR ( 1";
            foreach ($tmp as $kw)
            {
                $where .= " AND {$alias}goods_name LIKE '%" . mysql_like_quote($kw) . "%' ";
            }
            $where .= ") ";
        }
        $where .= " OR {$alias}goods_id = '" . mysql_like_quote($keyword) . "') ";
        return $where;
    }

    function searchGoodsByIncludeGoods($include_goods = "", $alias = 'g.')
    {
        if(strpos($include_goods, ",") > 0){
            $include_goods = explode(",", $include_goods);
            
            $include_goods_like_str = array_map(function ($goods_name, $alias = 'g.') { return "{$alias}goods_name like '%".trim(mysql_like_quote($goods_name))."%'"; }, $include_goods);
            $include_goods_sn_like_str = array_map(function ($goods_sn, $alias = 'g.') { return "{$alias}goods_name like '%".trim(mysql_like_quote($goods_sn))."%'"; }, $include_goods);
            $include_goods_id_str = array_map(function ($goods_id, $alias = 'g.') { return "{$alias}goods_id = '".trim(mysql_like_quote($goods_id))."'"; }, $include_goods);

            $where .= " AND (".implode(" OR ", $include_goods_like_str)." OR ".implode(" OR ", $include_goods_sn_like_str)." OR ".implode(" OR ", $include_goods_id_str).")";
        }
        else{
            $where .= " AND (goods_sn LIKE '%" . mysql_like_quote($include_goods) . "%' ";
            $where .= " OR goods_name LIKE '%" . mysql_like_quote($include_goods) . "%' ";
            $tmp = preg_split('/\s+/', $include_goods);
            if (count($tmp) > 1)
            {
                $where .= " OR ( 1";
                foreach ($tmp as $kw)
                {
                    $where .= " AND {$alias}goods_name LIKE '%" . mysql_like_quote($kw) . "%' ";
                }
                $where .= ") ";
            }
            $where .= " OR {$alias}goods_id = '" . mysql_like_quote($include_goods) . "')";
        }
        return $where;
    }

    function updatePlatformsGoodsPrice()
    {
        $id = intval(trim($_REQUEST['id']));
        $web = trim($_REQUEST['web']);
        if (empty($id)) {
            make_json_error("未選擇產品");
        }

        $script_path = ROOT_PATH . "scripts/PriceCrawlerDaemon.js";
        exec("node $script_path $id $web 2>&1", $output);

        $error = [];
        $response = [];
        foreach ($output as $json) {
            $msg = \json_decode($json, true);
            if (!empty($msg['error'])) {
                $error[] = $msg['error'];
            } else {
                $response = $msg['result'];
            }
        }

        make_json_response([
            'error'     => $error,
            'response'  => $response,
        ]);
    }

    function updatePlatformsGoodsCode()
    {
        global $db, $ecs;

        $id = intval(trim($_REQUEST['id']));

        if (empty($id))  {
            make_json_error("未選擇產品");
        }

        if (empty($_REQUEST['json'])) {
            make_json_error("沒有任何更新");
        }

        foreach ($_REQUEST['json'] as $platform => $data) {
            if (empty($data['id'])) {
                $db->query("DELETE FROM " . $ecs->table("goods_$platform") . " WHERE goods_id = '$id'");
            } else {
                $db->query(
                    "INSERT INTO " . $ecs->table("goods_$platform") .
                    " (goods_id, " . $platform . "_id" . ($platform == "pricecomhk" ? ", is_water" : "") . ") VALUES ('$id', '$data[id]'" . ($platform == "pricecomhk" ? ", '$data[is_water]'" : "") . ") " .
                    " ON DUPLICATE KEY UPDATE " . $platform . "_id = '$data[id]'" . ($platform == "pricecomhk" ? ", is_water = '$data[is_water]'" : "")
                );
            }
        }

        make_json_response();
    }

    function getDeadStockCost ($days_before = 0)
    {
        global $db, $ecs;

        $result = [];
        $res = [];
        $time_yesterday = local_strtotime("today");
        if ($days_before == 0) {
            $time_days_before = [
                '30' => local_strtotime('-30 days midnight'), // 00:00:00 30 days ago
                '60' => local_strtotime('-60 days midnight'), // 00:00:00 60 days ago
                '90' => local_strtotime('-90 days midnight'), // 00:00:00 90 days ago
            ];
        } else {
            $time_days_before = local_strtotime("-$days_before days midnight");
        }
        $erpController = new ErpController();

        $count = empty($_REQUEST['page_size']) ? $db->getOne("SELECT COUNT(*) FROM " . $ecs->table("goods")) : intval($_REQUEST['page_size']);
        $start = empty($_REQUEST['start']) ? 0 : intval($_REQUEST['start']);
        $page_size = empty($_REQUEST['page_size']) ? 1000 : intval($_REQUEST['page_size']);
        $sort_by = empty($_REQUEST['sort_by']) ? "g.goods_id" : $_REQUEST['sort_by'];
        $sort_order = empty($_REQUEST['sort_order']) ? "ASC" : $_REQUEST['sort_order'];

        while ($start < $count) {
            $sql = "SELECT g.goods_id, g.cost, g.is_delete, g.is_on_sale, g.is_real, g.cat_id, g.brand_id, IFNULL(es.first_act_time, g.add_time) as first_act_time, " .
                "IFNULL((SELECT sum(ega.goods_qty) FROM " . $ecs->table('erp_goods_attr_stock') . " as ega WHERE ega.goods_id = g.goods_id AND ega.warehouse_id IN (".$erpController->getSellableWarehouseIds(true).")), 0) as goods_number, " .
                \hw_reserved_number_subquery('g.') . ", ";
            if ($days_before == 0) {
                $sql .= "IFNULL((SELECT SUM(og.goods_number) FROM " . $ecs->table('order_goods') . " as og LEFT JOIN " . $ecs->table('order_info') . " as oi ON oi.order_id = og.order_id WHERE og.goods_id = g.goods_id " . order_query_sql('finished', 'oi.') . " AND (oi.shipping_time BETWEEN " . $time_days_before['30'] . " AND " . $time_yesterday . ")), 0) as ndayssales_30, " .
                        "IFNULL((SELECT SUM(og.goods_number) FROM " . $ecs->table('order_goods') . " as og LEFT JOIN " . $ecs->table('order_info') . " as oi ON oi.order_id = og.order_id WHERE og.goods_id = g.goods_id " . order_query_sql('finished', 'oi.') . " AND (oi.shipping_time BETWEEN " . $time_days_before['60'] . " AND " . $time_yesterday . ")), 0) as ndayssales_60, " .
                        "IFNULL((SELECT SUM(og.goods_number) FROM " . $ecs->table('order_goods') . " as og LEFT JOIN " . $ecs->table('order_info') . " as oi ON oi.order_id = og.order_id WHERE og.goods_id = g.goods_id " . order_query_sql('finished', 'oi.') . " AND (oi.shipping_time BETWEEN " . $time_days_before['90'] . " AND " . $time_yesterday . ")), 0) as ndayssales_90 ";
            } else {
                $sql .= "IFNULL((SELECT SUM(og.goods_number) FROM " . $ecs->table('order_goods') . " as og LEFT JOIN " . $ecs->table('order_info') . " as oi ON oi.order_id = og.order_id WHERE og.goods_id = g.goods_id " . order_query_sql('finished', 'oi.') . " AND (oi.shipping_time BETWEEN " . $time_days_before . " AND " . $time_yesterday . ")), 0) as ndayssales_$days_before ";
            }
            
            $sql .= "FROM (SELECT goods_id, cost, is_delete, is_on_sale, is_real, cat_id, brand_id, add_time FROM " . $ecs->table('goods'). ") as g " .
                    "LEFT JOIN (SELECT goods_id, Min(act_time) as first_act_time, Max(act_time) as last_act_time FROM " . $ecs->table('erp_stock') . " WHERE stock_style = 'w' AND w_d_style_id = 1 GROUP BY goods_id) as es ON es.goods_id = g.goods_id " .
                    "ORDER BY $sort_by $sort_order LIMIT $start, $page_size";
            $list = $db->getAll($sql);
            $res = array_merge($res, $list);
            $start += $page_size;
        }

        foreach ($res as $row) {
            $goods_number = intval($row['goods_number']);
            $reserved_number = intval($row['reserved_number']);
            // $row['cost'] = round(floatval($row['cost']), 4);
            $goods = [
                'cat_id'        => $row['cat_id'],
                'brand_id'      => $row['brand_id'],
                'sellable'      => $goods_number - $reserved_number,
                'goods_number'  => $goods_number,
                'reserved'      => $reserved_number,
                'total_cost'    => bcmul($row['cost'], $goods_number),
            ];

            if ($days_before == 0) {
                foreach ($time_days_before as $days => $time) {
                    $goods['data'][$days] = $this->calculateProductDeadDays($row, $time, $days);
                }
            } else {
                $goods['data'] = $this->calculateProductDeadDays($row, $time_days_before, $days_before);
            }
            $result[$row['goods_id']] = $goods;
        }
        return $result;
    }

    function calculateProductDeadDays ($row, $time, $days = 0) {
        $dead_stock = 0;
        $ndayssales = intval($row['ndayssales' . (empty($days) ? "" : "_$days")]);
        $sellable = intval($row['goods_number']) - intval($row['reserved_number']);
        if ($sellable < 0) {
            $sellable = 0;
        }
        $d_group = 0;
        $cost = 0;
        if (empty($row['is_on_sale']) || !empty($row['is_delete']) || empty($row['is_real']) || empty($sellable) || $row['first_act_time'] > $time) {
            $r_day = 0;
        } elseif (empty($ndayssales)) {
            $r_day = -1;
        } else {
            $r_day = round($sellable / $ndayssales * $days);
        }

        if ($r_day < 0) {
            $d_group = -1;
        } else {
            $d_group = ($r_day - $r_day % 30);
        }

        if (($r_day >= $days && $days > 0) || $r_day == -1) {
            $dead_stock = $sellable - $ndayssales > 0 ? $sellable - $ndayssales : 0;
            $cost = bcmul($row['cost'], $dead_stock);
        }

        return [
            'cost'          => $cost,
            'r_day'         => $r_day,
            'd_group'       => $d_group,
            'sales'         => $ndayssales,
            'qty'           => $dead_stock,
        ];

    }

    public function auto_pricing_single_product($goods_id){
        global $db, $ecs;
        $erpController = new ErpController();
        include_once(ROOT_PATH . 'includes/modules/cron/products.php');
        $days_before = self::DEAD_STOCK_DAYS;
        foreach (self::AUTO_PRICING_WEBS_COLUMNS as $key => $col) {
            $curr_table = "goods_$key";
            $web_sql .= "IFNULL((SELECT $col FROM " . $ecs->table($curr_table) . " as $curr_table WHERE $curr_table.goods_id = g.goods_id AND $curr_table.$col > 0), -1) as " . $key . "_price, ";
        }
        //$exclude_rules = $this->getAutoPricingRuleList(true);

        $time_yesterday = local_strtotime("today");
        $time_days_before = local_strtotime("-$days_before days midnight");
        $default_strategy_id = $this->getDefaultAutoPriceStrategy("default_auto_price");

        $sql = "SELECT g.goods_id, g.goods_name, g.shop_price, g.cost, g.market_price, g.price_strategy, g.promote_price, g.add_time, " .
                     "IFNULL(mp.user_price, -1) as vip_price, g.auto_pricing, " .
                     $web_sql .
                     "IFNULL((SELECT sum(ega.goods_qty) FROM " . $ecs->table('erp_goods_attr_stock') . " as ega WHERE ega.goods_id = g.goods_id AND ega.warehouse_id IN (".$erpController->getSellableWarehouseIds(true).")), 0) as goods_number, " .
                     "IFNULL((SELECT SUM(og.goods_number) FROM " . $ecs->table('order_goods') . " as og LEFT JOIN " . $ecs->table('order_info') . " as oi ON oi.order_id = og.order_id WHERE og.goods_id = g.goods_id " . order_query_sql('finished', 'oi.') . " AND (oi.shipping_time BETWEEN " . $time_days_before . " AND " . ($time_yesterday+86400) . ")), 0) as ndayssales " .
                 "FROM " . $ecs->table('goods'). " as g " .
                 "LEFT JOIN (SELECT goods_id, user_price FROM " . $ecs->table('member_price'). " WHERE user_rank = 2) as mp ON mp.goods_id = g.goods_id " .
                 "WHERE g.is_delete = 0 AND g.is_on_sale = 1 AND g.is_real = 1 AND (g.is_promote = 0 OR g.promote_end_date < " . gmtime() . ") " . //"AND g.add_time < $time_days_before " .
                 " AND g.goods_id = " . $goods_id .
                 " ORDER BY g.goods_id ASC LIMIT " . ($loop * 1000) . ", 1000";
      
        $goods_list = $db->getAll($sql);

        // no on sale suggestion made if any on sale suggestion is running
        $ignore_dead_stock = $db->getOne("SELECT COUNT(*) FROM " . $ecs->table("auto_pricing_log") . " WHERE expire_date > CURRENT_TIMESTAMP AND suggest_type = " . AUTO_PRICE_TYPE_DEAD);
        foreach ($goods_list as $good) {
            $dead = false;
            $dead_day = 0;
            if ($good['goods_number'] > 0) {
                if ($good['ndayssales'] == 0) {
                    $dead_day = 180;
                } else {
                    $r_day = round($row['goods_number'] / $row['ndayssales'] * $days_before);
                    $d_group = ($r_day - $r_day % 30) / 30;
                    $dead_day = $r_day >= 180 ? 180 : $d_group * 30;
                }
            }
            if ($dead_day > 0 && $row['add_time'] < $time_days_before) {
                $dead = true;
            }
            $strategy_id = 0;
            
            // Step 2. get selected strategy
            if (empty($good['price_strategy']) && !empty($default_strategy_id)) {
                $strategy_id = $default_strategy_id;
            } elseif (!empty($good['price_strategy']) && $good['price_strategy'] != "-1") {
                $strategy_id = $good['price_strategy'];
            }

            if (!empty($strategy_id)) {
                $strategy = $this->getAutoPricingStrategy($strategy_id);
                $condition = $strategy['condition'];
                $select = $strategy['select'];
                $compare_price = 0;
                $select_price = 0;
                $price_limit = 0;
                $suggest_price = floatval($good['shop_price']);
                $sale_price = -1;
                $vip_price = -1;

                // get required price
                $prices = [];
                $prices_webs = [];
                foreach (array_keys($this::AUTO_PRICING_WEBS) as $col) {
                    $p = floatval($good[$col . "_price"]);
                    if ($p > 0 && in_array($col, $strategy['reference'])) {
                        $prices[$col] = $p;
                        $prices_webs[] = $p;
                    }
                }
                foreach (array_keys($this::AUTO_PRICING_VALUES) as $col) {
                    if ($col == 'max') {
                        $p = max($prices_webs);
                    } elseif ($col == 'min') {
                        $p = min($prices_webs);
                    } elseif ($col == 'avg') {
                        $p = array_sum($prices_webs) / count($prices_webs);
                    }
                    if ($p > 0) {
                        $prices[$col] = $p;
                    }
                }
                
                
                // get condition price
                $compare_price = $prices[$condition];
    
                // get select price
                $select_price = $prices[$select];
                // get price limit
                if (!empty($strategy['price_limit'])) {
                    if ($strategy['price_limit']['type'] == 1) {
                        $price_limit = $good['cost'] + intval($strategy['price_limit']['amount']);
                    } elseif ($strategy['price_limit']['type'] == 2) {
                        $price_limit = $good['cost'] * (1 + intval($strategy['price_limit']['amount']) / 100);
                    }
                }
                if ($price_limit < 0) {
                    $price_limit = 0;
                }
                
                // process auto on sale
                if ($dead && !empty($strategy['on_sale']) && !$ignore_dead_stock) {
                    $on_sale = $strategy['on_sale'];
                    if ($dead_day > intval($on_sale['target'])) {
                        $sale_amount = 0;
                        if (!empty($on_sale['amount']['default'])) {
                            $sale_amount = intval($on_sale['amount']['default']);
                        } elseif (!empty($strategy['on_sale']['amount'][$dead_day])) {
                            $sale_amount = intval($on_sale['amount'][$dead_day]);
                        }
                        $gross_profit = floatval($good['shop_price']) - floatval($good['cost']);
                        if ($on_sale['type'] == 1) {
                            $sale_price = floatval($good['shop_price']) - intval($sale_amount);
                        } else {
                            $sale_price = round(floatval($good['shop_price']) - ($gross_profit * intval($sale_amount) / 100));
                        }
                        
                        // Step 3. suggest promotion if D180
                        // 2018-09-11: On sale only if dead_day >= 180
                        if ($sale_price != -1 && $sale_price < $suggest_price && $sale_price > 0 && $dead_day >= 180) {
                            insertSuggestPrice($good['goods_id'], $strategy['strategy_id'], AUTO_PRICE_TYPE_DEAD, $sale_price, $good['promote_price'], $dead_day, $on_sale['duration']);
                        }
                    }
                }
                
                
                
                // adjust normal price
                if (!empty($strategy['normal']) && $strategy_id != $default_strategy_id) {
                    if ($compare_price > 0 && $select_price > 0) {
                        
                        if (!$db->getOne("SELECT 1 FROM " . $ecs->table("auto_pricing_log") . " WHERE expire_date > CURRENT_TIMESTAMP AND goods_id = $good[goods_id] AND suggest_type = 0")) {
                            
                            $compare_price_min = $compare_price * (1 - intval($strategy['condition_percent']) / 100);
                            $compare_price_max = $compare_price * (1 + intval($strategy['condition_percent']) / 100);
                            //echo $compare_price_min."|".$compare_price_max."|".$good['shop_price']."|".$price_limit."|";
                            
                            if ((floatval($good['shop_price']) < $compare_price_min || floatval($good['shop_price']) > $compare_price_max) && $good['shop_price'] > $price_limit) {
                                $suggest_price = round($select_price * intval($strategy['percent']) / 100);
                                
                                if ($price_limit > 0 && $suggest_price < $price_limit) {
                                    $suggest_price = $price_limit;
                                }
                                if ($suggest_price > $good['market_price']) {
                                    $suggest_price = $good['market_price'];
                                }
                                if ($suggest_price < 0) {
                                    $suggest_price = 0;
                                }
                                
                                // Step 4. suggest shop_price if auto process enabled
                                if (round($suggest_price) != floatval($good['shop_price'])) {
                                    insertSuggestPrice($good['goods_id'], $strategy['strategy_id'], AUTO_PRICE_TYPE_NORMAL, $suggest_price, $good['shop_price'], $dead_day);
                                }
                            }
                        }
                    }
                }
    
                // update vip price
                if (!empty($strategy['vip'])) {
                    if ($good['vip_price'] != -1) {
                        $vip_duration = 0;
                        if ($sale_price != -1) {
                            $vip_price = round($sale_price * intval($strategy["vip_percent"]) / 100);
                            $vip_duration = $on_sale['duration'];
                        } elseif ($suggest_price > 0) {
                            $vip_price = round($suggest_price * intval($strategy["vip_percent"]) / 100);
                        }
                        if ($vip_price != -1 && $vip_price < $good['vip_price']) {
                            // Step 5. suggest vip_price if auto process enabled
                            insertSuggestPrice($good['goods_id'], $strategy['strategy_id'], AUTO_PRICE_TYPE_VIP, $vip_price, $good['vip_price'], $dead_day, $vip_duration);
                        }
                    }
                }
            }
        }
    
        // Step 6. apply shop_price and vip_price suggestion
        foreach ($goods_list as $good) {
            $auto_price_suggestions = $db->getCol("SELECT suggest_id FROM " . $ecs->table("auto_pricing_suggestion") . " WHERE goods_id = ".$good['goods_id']." AND status = " . AUTO_PRICE_STATUS_SUGGESTED . " AND suggest_type " . db_create_in([AUTO_PRICE_TYPE_NORMAL, AUTO_PRICE_TYPE_VIP]));
            foreach ($auto_price_suggestions as $suggest_id) {
                $this->updateAutoPricingSuggestion($suggest_id, AUTO_PRICE_STATUS_APPLIED, true);
            }
        }
    
        // Step 7. apply dead stock suggestion
        //$this->updateAutoPricingSuggestion($suggest_id, AUTO_PRICE_STATUS_APPLIED, true);

        $on_sale_suggestion_ids = $db->getCol("SELECT suggest_id FROM " . $ecs->table("auto_pricing_suggestion") . " WHERE goods_id = ".$goods_id." and status = " . AUTO_PRICE_STATUS_SUGGESTED . " AND suggest_type = " . AUTO_PRICE_TYPE_DEAD);
        shuffle($on_sale_suggestion_ids);
        foreach ($on_sale_suggestion_ids as $key => $suggest_id) {
            // apply only first 500 suggestions, reject otherwise
            //if ($key < intval($cron['max_on_sale_number'])) {
                $this->updateAutoPricingSuggestion($suggest_id, AUTO_PRICE_STATUS_APPLIED, true);
            //} else {
            //    $this->updateAutoPricingSuggestion($suggest_id, AUTO_PRICE_STATUS_REJECTED, true);
            //}
        }
    }
}
?>