<?php
/***
* by Anthony 20171018
*
* Delivery Order controller
***/

namespace Yoho\cms\Controller;
use Yoho\cms\Model;
if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}


class DeliveryOrderController extends YohoBaseController
{

    const AREA = ["香港","九龍","新界","離島"];
    const DISTRICT = [
        ["中區","東區","南區","灣仔"],
        ["西九","東九"],
        ["元朗","天水圍","屯門","葵青荃灣","沙田","新界北"],
        ["離島"],
    ];
    const LOCATION = [
        [
            ["堅尼地城","西環","西營盤","上環","中環","半山","山頂","金鐘","薄扶林","香港大學","數碼港","摩星嶺"],
            ["天后","寶馬山","樹仁大學","炮臺山","北角","側魚涌","太古","西灣河","筲箕灣","柴灣","小西灣","杏花村"],
            ["陽明山莊","香港仔","鴨脷洲","黃竹坑","深水灣","淺水灣","舂坎角","赤柱","大潭","石澳"],
            ["灣仔","銅鑼灣","跑馬地","渣甸山","大坑"]
        ],
        [
            ["紅磡","理工大學","土瓜灣","九龍城","美孚","荔枝角","長沙灣","深水埗","南昌","大角咀","奧運","昂船洲","太子","旺角","油麻地","佐敦","尖沙咀","石硤尾","何文田","九龍站"],
            ["橫頭磡","樂富","啟德","慈雲山","黃大仙","新蒲崗","鑽石山","彩虹","飛鵝山","順利邨","秀茂坪","九龍灣","牛頭角","觀塘","藍田","油塘","九龍塘","城市大學","浸會大學","筆架山","大老山","蠔涌","大網仔","十四鄉","科技大學","銀線灣","大坳門","布袋澳","馬游塘","將軍澳","將軍澳工業邨","日出康城","井欄樹","西貢市中心","大網仔路段","清水灣道路段","寶琳","坑口","調景嶺","環保大道路段"]
        ],
        [
            ["元朗","大棠","錦田","錦鏽花園","加洲花園","石崗","大欖","八鄉"],
            ["洪水橋","流浮山","天水圍"],
            ["小欖","屯門","嶺南大學","龍鼓灘","藍地","兆康","虎地"],
            ["大帽山","荃灣","葵涌","青衣","汀九","深井","荔景","葵芳","葵興","上葵涌","下葵涌","大窩口"],
            ["金山","沙田","大圍","馬鞍山","西貢","中文大學","馬料水","九肚山","火炭","西沙路段"],
            ["落馬洲","沙頭角","軍地","古洞","上水","粉嶺","太和","康樂園","三門仔","大尾篤","教育學院","大埔"]
        ],
        [
            ["馬灣","愉景灣","東涌"]
        ]
    ];
    const BOX = ["1號 200 X 180 X 100 mm","2號 250 X 300 X 180 mm","3號 300 X 250 X 200 mm","4號 360 X 300 X 250 mm","5號 530 X 320 X 230 mm","6號 700 X 400 X 320 mm"];

    /** For CMS 生成發貨單 Btn
     * If $silent is true, we will save error to return but not call sys_msg.
     * Step 1: Check order finish
     * Step 2: Handle order/user info
     * Step 3: Get Order goods
     * Step 4: Get can ship qty 可發貨數量
     * @return Array ([error]|[order, exist_real_goods, attr, goods_list])
     */
    public function prepare_create_delivery_order($order_id, $action_note, $order = array(), $silent = false)
    {

        global $db, $ecs, $_LANG, $_CFG;
        $orderController = new OrderController();
        $return_data = array();

        // Query: order info
        if (!empty($order_id)) {
            if(empty($order))$order = order_info($order_id);
        }
        else {
            if (!$silent) die('order does not exist');
            else {
                $return_data['error'] = 'order does not exist';
                return $return_data;
            }
        }

        /* Step 1: Check order finish */
        // Query: order is finish and check admin priv
        if (order_finished($order)) {
            admin_priv('order_view_finished');
        }
        else {
            admin_priv('order_view');
        }

        // 如果管理员属于某个办事处，检查该订单是否也属于这个办事处
        $sql = "SELECT agency_id FROM " . $ecs->table('admin_user') . " WHERE user_id = '$_SESSION[admin_id]'";
        $agency_id = $db->getOne($sql);
        if ($agency_id > 0) {
            if ($order['agency_id'] != $agency_id)
            {
                if (!$silent) sys_msg($_LANG['priv_error'], 0);
                else {
                    $return_data['error'] = $_LANG['priv_error'];
                    return $return_data;
                }
            }
        }

        /* Step 2: Handle order/user info */
        if ($order['user_id'] > 0)
        {
            $user = user_info($order['user_id']);
            if (!empty($user))
            {
                $order['user_name'] = $user['user_name'];
            }
        }

        // Query: order region
        $sql = "SELECT concat(IFNULL(c.region_name, ''), '  ', IFNULL(p.region_name, ''), " .
                    "'  ', IFNULL(t.region_name, ''), '  ', IFNULL(d.region_name, '')) AS region " .
                "FROM " . $ecs->table('order_info') . " AS o " .
                    "LEFT JOIN " . $ecs->table('region') . " AS c ON o.country = c.region_id " .
                    "LEFT JOIN " . $ecs->table('region') . " AS p ON o.province = p.region_id " .
                    "LEFT JOIN " . $ecs->table('region') . " AS t ON o.city = t.region_id " .
                    "LEFT JOIN " . $ecs->table('region') . " AS d ON o.district = d.region_id " .
                "WHERE o.order_id = '$order[order_id]'";
        $order['region'] = $db->getOne($sql);

        // Query: other order handle
        $order['order_time'] = local_date($_CFG['time_format'], $order['add_time']);
        $order['invoice_no'] = $order['shipping_status'] == SS_UNSHIPPED || $order['shipping_status'] == SS_PREPARING ? $_LANG['ss'][SS_UNSHIPPED] : $order['invoice_no'];
        $order['insure_yn']  = empty($order['insure_fee']) ? 0 : 1;

        /* Step 3: Get Order goods */
        // Check exist real goods
        $exist_real_goods = exist_real_goods($order_id);

        // Get order goods
        $_goods = $orderController->get_order_goods(array('order_id' => $order['order_id'], 'order_sn' =>$order['order_sn']));

        $attr = $_goods['attr'];
        $goods_list = $_goods['goods_list'];
        unset($_goods);

        // check which warehouse to see/reduce
        $storage_warehouse_id = $orderController->getOrderWarehouseIdForReduceStock($order_id);

        // // check if fls
        // if (isset($_REQUEST['is_fls']) && $_REQUEST['is_fls'] == 1) {
        //     $erpController = new ErpController();
        //     $fls_warehouse_id = $erpController->getFlsWarehouseId();
        //     $storage_warehouse_id = $fls_warehouse_id;
        // }

        // // check if wilson
        // if (isset($_REQUEST['logistics']) && $_REQUEST['logistics'] == 'wilson') {
        //     $erpController = new ErpController();
        //     $wilson_warehouse_id = $erpController->getWilsonWarehouseId();
        //    echo $storage_warehouse_id = $wilson_warehouse_id;
        // }

        /* Step 4: Get can ship qty 可發貨數量 */
        if ($goods_list)
        {


            $goods_send = [];

            foreach ($goods_list as $key => $value){
                if ($value['is_package'] == 0 && $value['is_gift'] == 0){
                    $goods_send[$value['goods_id']]['rec_id'] = $value['rec_id'];
                    $goods_send[$value['goods_id']]['goods_number'] = $value['goods_number'];
                    $goods_send[$value['goods_id']]['send_number'] = $value['send_number'];
                    $goods_send[$value['goods_id']]['send'] = $value['goods_number'] - $value['send_number'];
                    $goods_send[$value['goods_id']]['storage_wh'] = $value['storage_wh'][$storage_warehouse_id];
                }
            }

            foreach ($goods_list as $key=> $goods_value)
            {
                if (!$goods_value['goods_id'])
                {
                    continue;
                }

                // Package_buy handle
                if (($goods_value['extension_code'] == 'package_buy') && (count($goods_value['package_goods_list']) > 0))
                {
                    $goods_list[$key]['package_goods_list'] = $orderController->package_goods($goods_value['package_goods_list'], $goods_value['goods_number'], $goods_value['order_id'], $goods_value['extension_code'], $goods_value['goods_id']);

                    foreach ($goods_list[$key]['package_goods_list'] as $pg_key => $pg_value)
                    {
                        $goods_list[$key]['package_goods_list'][$pg_key]['readonly'] = '';
                        /* 使用库存 是否缺货 */
                        if ($pg_value['storage'] <= 0 && $_CFG['use_storage'] == '1')
                        {
                            $goods_list[$key]['package_goods_list'][$pg_key]['send'] = $_LANG['act_good_vacancy'];
                            $goods_list[$key]['package_goods_list'][$pg_key]['readonly'] = 'readonly="readonly"';
                        }
                        /* 将已经全部发货的商品设置为只读 */
                        elseif ($pg_value['send'] <= 0)
                        {
                            $goods_list[$key]['package_goods_list'][$pg_key]['send'] = $_LANG['act_good_delivery'];
                            $goods_list[$key]['package_goods_list'][$pg_key]['readonly'] = 'readonly="readonly"';
                        }
                    }
                }
                else
                {
                    $goods_list[$key]['sended'] = $goods_value['send_number'];
                    $goods_list[$key]['send'] = $goods_value['goods_number'] - $goods_value['send_number'];
                    $goods_list[$key]['readonly'] = '';
                    // Check goods out of stock?
                    if ($goods_value['storage_wh'][$storage_warehouse_id] <= 0 && $_CFG['use_storage'] == '1')
                    {
                        $goods_list[$key]['send'] = $_LANG['act_good_vacancy'];
                        $goods_list[$key]['readonly'] = 'readonly="readonly"';
                    }
                    elseif ($goods_list[$key]['send'] <= 0)
                    {
                        $goods_list[$key]['send'] = $_LANG['act_good_delivery'];
                        $goods_list[$key]['readonly'] = 'readonly="readonly"';
                    }
                    elseif($goods_value['storage_wh'][$storage_warehouse_id] < $goods_list[$key]['send'])
                    {
                        $goods_list[$key]['send']=$goods_value['storage_wh'][$storage_warehouse_id];
                    }

                    // check if it is an insurance product
                    if ($goods_value['is_insurance'] == 1 && $goods_value['insurance_of'] > 0){
                        if (!isset($goods_send[$goods_value['insurance_of']])){
                            $goods_list[$key]['send'] = $_LANG['act_good_delivery'];
                            $goods_list[$key]['readonly'] = 'readonly="readonly"';
                        } elseif ($goods_list[$key]['send'] > min($goods_send[$goods_value['insurance_of']]['send'], $goods_send[$goods_value['insurance_of']]['storage_wh'])) {
                            $goods_list[$key]['send'] = min($goods_send[$goods_value['insurance_of']]['send'], $goods_send[$goods_value['insurance_of']]['storage_wh']);
                        }
                    }

                    //如果是手動開出庫扣庫存
                    if($_CFG['stock_dec_time']==1)
                    {
                        $goods_list[$key]['send'] = $goods_value['delivery_qty']-$goods_list[$key]['sended'] <= $goods_list[$key]['send'] ? $goods_value['delivery_qty']-$goods_list[$key]['sended']:$goods_list[$key]['send'];
                        $goods_list[$key]['send'] = $goods_list[$key]['send']>0 ? $goods_list[$key]['send'] : 0;
                        $goods_list[$key]['readonly'] = 'readonly="readonly"';
                    }
                }
            }
        }

        $return_data['order'] = $order;
        $return_data['exist_real_goods'] = $exist_real_goods;
        $return_data['attr'] = $attr;
        $return_data['goods_list'] = $goods_list;

        return $return_data;
    }

    /** For CMS 確認生成發貨單 Btn
     * Step 1: Handle trim in array value
     * Step 2: Check if order is OS_SPLITED(全部分單)
     * Step 3: Check delivery goods qty, and consolidate the same goods
     * Step 4: Handle previous step result (對上一步處理結果進行判斷兼容上一步判斷為假情況的處理)
     * Step 5: Check virtual goods, and handle package buy
     * Step 6: Insert delivery order
     * Step 7: Goods Issue Warehouse
     * Step 8: Handle order info
     * @return Array|Number [error]|delivery_id
     */
    public function create_delivery_order($order_id, $request, $action_note="", $order = array(), $silent = false)
    {

        global $db, $ecs, $_LANG, $_CFG;
        $orderController = new OrderController();
        // Query: order info
        if (!empty($order_id))
        {
            $order = order_info($order_id);
        }
        else
        {
            if (!$silent) die('order does not exist');
            else {
                $return_data['error'] = 'order does not exist';
                return $return_data;
            }
        }

        if (!empty($order['is_hold'])) {
            if (!$silent) die('order is held by admin');
            else {
                $return_data['error'] = 'order is held by admin';
                return $return_data;
            }
        }
        // Define time
        $utc_time = gmtime();

        /* Step 1: Handle trim in array value */
        array_walk($request['delivery'], [$this, 'trim_array_walk']);
        $delivery = $request['delivery'];
        array_walk($request['send_number'], [$this, 'trim_array_walk']);
        array_walk($request['send_number'], [$this, 'intval_array_walk']);
        $send_number = $request['send_number'];
        $operator    = $request['operator'];

        // Initial delivery data
        $delivery['user_id']      = intval($delivery['user_id']);
        $delivery['country']      = intval($delivery['country']);
        $delivery['province']     = intval($delivery['province']);
        $delivery['city']         = intval($delivery['city']);
        $delivery['district']     = intval($delivery['district']);
        $delivery['agency_id']    = intval($delivery['agency_id']);
        $delivery['insure_fee']   = floatval($delivery['insure_fee']);
        $delivery['shipping_fee'] = floatval($delivery['shipping_fee']);

        /* Step 2: Check if order is OS_SPLITED(全部分單) */
        if ($order['order_status'] == OS_SPLITED)
        {
            // Error
            if (!$silent) {
                $links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=info&order_id=' . $order_id);
                sys_msg(sprintf($_LANG['order_splited_sms'], $order['order_sn'],
                    $_LANG['os'][OS_SPLITED], $_LANG['ss'][SS_SHIPPED_ING], $GLOBALS['_CFG']['shop_name']), 1, $links);
            } else {
                $return_data['error'] = sprintf($_LANG['order_splited_sms'], $order['order_sn'],
                    $_LANG['os'][OS_SPLITED], $_LANG['ss'][SS_SHIPPED_ING], $GLOBALS['_CFG']['shop_name']);
                return $return_data;
            }
        
        }

        // Get order goods
        $_goods = $orderController->get_order_goods(array('order_id' => $order_id, 'order_sn' => $delivery['order_sn']));

        $goods_list = $_goods['goods_list'];

        /* Step 3: Check delivery goods qty, and consolidate the same goods */
        if (!empty($send_number) && !empty($goods_list))
        {
            $goods_no_package = array();
            $map_order_id_rec_id = [];

            foreach ($goods_list as $key => $value){
                if ($value['is_package'] == 0 && $value['is_gift'] == 0){
                    $map_order_id_rec_id[$value['goods_id']] = $value['rec_id'];
                }
            }
            foreach ($goods_list as $key => $value)
            {
                // check if it is an insurance product
                if ($value['is_insurance'] == 1 && $value['insurance_of'] > 0){
                    
                    if (!isset($send_number[$map_order_id_rec_id[$value['insurance_of']]])){
                        unset($send_number[$value['rec_id']], $goods_list[$key]);
                        continue;
                    } elseif ($send_number[$map_order_id_rec_id[$value['insurance_of']]] < $send_number[$value['rec_id']]) {
                        
                        $send_number[$value['rec_id']] = max($send_number[$map_order_id_rec_id[$value['insurance_of']]], 0);
                    }
                }

                // Normal order: Remove 0 qty goods
                if (!isset($value['package_goods_list']) || !is_array($value['package_goods_list']))
                {
                    // Get product Id by Key
                    $_key = empty($value['product_id']) ? $value['goods_id'] : ($value['goods_id'] . '_' . $value['product_id']);

                    // Sum goods total, cal goods send number
                    if (empty($goods_no_package[$_key]))
                    {
                        $goods_no_package[$_key] = $send_number[$value['rec_id']];
                    }
                    else
                    {
                        $goods_no_package[$_key] += $send_number[$value['rec_id']];
                    }

                    // Remove qty < 0 goods
                    if ($send_number[$value['rec_id']] <= 0)
                    {
                        unset($send_number[$value['rec_id']], $goods_list[$key]);
                        continue;
                    }
                }
                else
                {
                    // If is package goods list
                    $goods_list[$key]['package_goods_list'] = $orderController->package_goods($value['package_goods_list'], $value['goods_number'], $value['order_id'], $value['extension_code'], $value['goods_id']);

                    // Handle packge goods list
                    foreach ($value['package_goods_list'] as $pg_key => $pg_value)
                    {
                        // Get goods id
                        $_key = empty($pg_value['product_id']) ? $pg_value['goods_id'] : ($pg_value['goods_id'] . '_' . $pg_value['product_id']);

                        // Sum goods total, and cal send_number
                        if (empty($goods_no_package[$_key]))
                        {
                            $goods_no_package[$_key] = $send_number[$value['rec_id']][$pg_value['g_p']];
                        }
                        else
                        {
                            $goods_no_package[$_key] += $send_number[$value['rec_id']][$pg_value['g_p']];
                        }

                        // Remove qty < 0 goods
                        if ($send_number[$value['rec_id']][$pg_value['g_p']] <= 0)
                        {
                            unset($send_number[$value['rec_id']][$pg_value['g_p']], $goods_list[$key]['package_goods_list'][$pg_key]);
                        }
                    }

                    if (count($goods_list[$key]['package_goods_list']) <= 0)
                    {
                        unset($send_number[$value['rec_id']], $goods_list[$key]);
                        continue;
                    }
                }

                // If 發貨數量 != goods total
                if (!isset($value['package_goods_list']) || !is_array($value['package_goods_list']))
                {
                    $sended = $this->order_delivery_num($order_id, $value['goods_id'], $value['product_id']);
                    
                    if (($value['goods_number'] - $sended - $send_number[$value['rec_id']]) < 0)
                    {
                        // Error
                        if (!$silent) {
                            $links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=info&order_id=' . $order_id);
                            sys_msg($_LANG['act_ship_num'], 1, $links);
                        } else {
                            $return_data['error'] = $_LANG['act_ship_num'];
                            return $return_data;
                        }
                    }
                }
                else
                {
                    // Handle package goods list
                    foreach ($goods_list[$key]['package_goods_list'] as $pg_key => $pg_value)
                    {
                        if (($pg_value['order_send_number'] - $pg_value['sended'] - $send_number[$value['rec_id']][$pg_value['g_p']]) < 0)
                        {
                            // Error
                            if (!$silent) {
                                $links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=info&order_id=' . $order_id);
                                sys_msg($_LANG['act_ship_num'], 1, $links);
                            } else {
                                $return_data['error'] = $_LANG['act_ship_num'];
                                return $return_data;
                            }
                        }
                    }
                }
            }
        }

        /* Step 4 : Handle previous step result (對上一步處理結果進行判斷兼容上一步判斷為假情況的處理) */
        if (empty($send_number) || empty($goods_list))
        {
            // Error
            if (!$silent) {
                $links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=info&order_id=' . $order_id);
                sys_msg($_LANG['act_good_vacancy'], 1, $links);
            } else {
                $return_data['error'] = $_LANG['act_good_vacancy'];
                return $return_data;
            }
        }

        /* Step 5: Check virtual goods, and handle package buy */
        $virtual_goods = array();
        $package_virtual_goods = array();

        foreach ($goods_list as $key => $value)
        {
            // Package buy
            if ($value['extension_code'] == 'package_buy')
            {
                foreach ($value['package_goods_list'] as $pg_key => $pg_value)
                {
                    if ($pg_value['goods_number'] < $goods_no_package[$pg_value['g_p']] && ($_CFG['use_storage'] == '1' || ($_CFG['use_storage'] == '0' && $pg_value['is_real'] == 0)))
                    {
                        // Error
                        if (!$silent) {
                            $links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=info&order_id=' . $order_id);
                            sys_msg(sprintf($_LANG['act_good_vacancy'], $pg_value['goods_name']), 1, $links);
                        } else {
                            $return_data['error'] = $_LANG['act_good_vacancy'];
                            return $return_data;
                        }
                    }

                    // Package buy - virtual goods handle
                    if ($pg_value['is_real'] == 0)
                    {
                        $package_virtual_goods[] = array(
                            'goods_id' => $pg_value['goods_id'],
                            'goods_name' => $pg_value['goods_name'],
                            'num' => $send_number[$value['rec_id']][$pg_value['g_p']]
                        );
                    }
                }
            }
            // Virtual goods
            elseif ($value['extension_code'] == 'virtual_card' || $value['is_real'] == 0)
            {
                $sql = "SELECT COUNT(*) FROM " . $GLOBALS['ecs']->table('virtual_card') . " WHERE goods_id = '" . $value['goods_id'] . "' AND is_saled = 0 ";
                $num = $GLOBALS['db']->GetOne($sql);
                if ($num < $goods_no_package[$value['goods_id']] && $_CFG['use_storage'] == '1')
                {
                    // Error
                    if (!$silent) {
                        $links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=info&order_id=' . $order_id);
                        sys_msg(sprintf($GLOBALS['_LANG']['virtual_card_oos'] . '【' . $value['goods_name'] . '】'), 1, $links);
                    } else {
                        $return_data['error'] = sprintf($GLOBALS['_LANG']['virtual_card_oos'] . '【' . $value['goods_name'] . '】');
                        return $return_data;
                    }
                }

                // Virtual goods list
                if ($value['extension_code'] == 'virtual_card')
                {
                    $virtual_goods[$value['extension_code']][] = array('goods_id' => $value['goods_id'], 'goods_name' => $value['goods_name'], 'num' => $send_number[$value['rec_id']]);
                }
            }
            else
            {
                if (($send_number[$value['rec_id']] > $value['storage_wh'][$value['storage_warehouse_id']]+$value['delivery_qty']) && $_CFG['use_storage'] == '1')
                {
                    // Error
                    if (!$silent) {
                        $links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=info&order_id=' . $order_id);
                        sys_msg(sprintf($_LANG['act_good_vacancy'], $value['goods_name']), 1, $links);
                    } else {
                        $return_data['error'] = sprintf($_LANG['act_good_vacancy'], $value['goods_name']);
                        return $return_data;
                    }
                }
            }
        }

        /* Step 6: Insert delivery order */
        // Create delivery order data
        $delivery['delivery_sn'] = get_delivery_sn();
        $delivery_sn = $delivery['delivery_sn'];
        $delivery['action_user'] = $_SESSION['admin_name']; // action user , we log the login ac.
        $delivery['update_time'] = $utc_time;
        $delivery_time = $delivery['update_time'];
        $sql ="select add_time from ". $GLOBALS['ecs']->table('order_info') ." WHERE order_sn = '" . $delivery['order_sn'] . "'";
        $delivery['add_time'] =  $GLOBALS['db']->GetOne($sql);

        $delivery['status'] = DO_NORMAL; // 正常
        $delivery['order_id'] = $order_id;

        // Filter fileds
        $filter_fileds = array(
            'order_sn', 'add_time', 'user_id', 'how_oos', 'shipping_id', 'shipping_fee',
            'consignee', 'address', 'country', 'province', 'city', 'district', 'sign_building',
            'email', 'zipcode', 'tel', 'mobile', 'best_time', 'postscript', 'serial_numbers', 'insure_fee',
            'agency_id', 'delivery_sn', 'action_user', 'update_time',
            'status', 'order_id', 'shipping_name'
        );
        foreach ($filter_fileds as $value)
        {
            $_delivery[$value] = $this->escape_string($delivery[$value]);
        }
        $query = $db->autoExecute($ecs->table('delivery_order'), $_delivery, 'INSERT', '', 'SILENT');
        $delivery_id = $db->insert_id();
        /* Step 7: Goods Issue Warehouse */
        if ($delivery_id)
        {
            $delivery_goods = array();

            if (!empty($goods_list))
            {
                foreach ($goods_list as $value)
                {
                    // Handle goods
                    if ((empty($value['extension_code']) || $value['extension_code'] == "package") || $value['extension_code'] == 'virtual_card')
                    {
                        $delivery_goods = array(
                            'delivery_id' => $delivery_id,
                            'goods_id' => $value['goods_id'],
                            'product_id' => $value['product_id'],
                            'product_sn' => (isset($value['product_sn'])) ? $value['product_sn'] : '',
                            'goods_id' => $value['goods_id'],
                            'goods_name' => $this->escape_string($value['goods_name']),
                            'brand_name' => $this->escape_string($value['brand_name']),
                            'goods_sn' => $value['goods_sn'],
                            'send_number' => $send_number[$value['rec_id']],
                            'parent_id' => 0,
                            'is_real' => $value['is_real'],
                            'goods_attr' => $value['goods_attr'],
                            'goods_attr_id' => $value['goods_attr_id'],
                            'goods_price' => $value['goods_price'],
                            'order_item_id' => $value['rec_id'],
                            'warehouse_id' => $value['storage_warehouse_id'],
                            'is_insurance' => $value['is_insurance'],
                            'insurance_of' => $value['insurance_of']
                        );

                        $query = $db->autoExecute($ecs->table('delivery_goods'), $delivery_goods, 'INSERT', '', 'SILENT');
                    }
                    // Handle package buy
                    elseif ($value['extension_code'] == 'package_buy')
                    {
                        foreach ($value['package_goods_list'] as $pg_key => $pg_value)
                        {
                            $delivery_pg_goods = array(
                                'delivery_id' => $delivery_id,
                                'goods_id' => $pg_value['goods_id'],
                                'product_id' => $pg_value['product_id'],
                                'product_sn' => $pg_value['product_sn'],
                                'goods_name' => $this->escape_string($pg_value['goods_name']),
                                'brand_name' => '',
                                'goods_sn' => $pg_value['goods_sn'],
                                'send_number' => $send_number[$value['rec_id']][$pg_value['g_p']],
                                'parent_id' => $value['goods_id'], // 礼包ID
                                'extension_code' => $value['extension_code'], // 礼包
                                'is_real' => $pg_value['is_real']
                            );
                            $query = $db->autoExecute($ecs->table('delivery_goods'), $delivery_pg_goods, 'INSERT', '', 'SILENT');
                        }
                    }
                }
            }
        }
        else
        {
            // Error
            if (!$silent) {
                $links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=info&order_id=' . $order_id);
                sys_msg($_LANG['act_null_delivery_id'], 1, $links);
            } else {
                $return_data['error'] = $_LANG['act_null_delivery_id'];
                return $return_data;
            }
        }
        unset($filter_fileds, $delivery, $_delivery, $order_finish);

        /* Step 8: Handle order info */
        if (true)
        {
            $_sended = & $send_number;
            foreach ($_goods['goods_list'] as $key => $value)
            {
                if ($value['extension_code'] != 'package_buy')
                {
                    unset($_goods['goods_list'][$key]);
                }
            }
            foreach ($goods_list as $key => $value)
            {
                if ($value['extension_code'] == 'package_buy')
                {
                    unset($goods_list[$key]);
                }
            }
            $_goods['goods_list'] = $goods_list + $_goods['goods_list'];
            unset($goods_list);

            // Update order virual goods
            $_virtual_goods = isset($virtual_goods['virtual_card']) ? $virtual_goods['virtual_card'] : '';
            $orderController->update_order_virtual_goods($order_id, $_sended, $_virtual_goods);

            // Update order goods with sended
            $orderController->update_order_goods_sended($order_id, $_sended, $_goods['goods_list']);

            // Handle order status
            $order_finish = $orderController->get_order_finish($order_id);
            $shipping_status = SS_SHIPPED_ING;
            if ($order['order_status'] != OS_CONFIRMED && $order['order_status'] != OS_SPLITED && $order['order_status'] != OS_SPLITING_PART)
            {
                $arr['order_status']    = OS_CONFIRMED;
                $arr['confirm_time']    = $utc_time;
            }
            $arr['order_status'] = $order_finish ? OS_SPLITED : OS_SPLITING_PART; // 全部分单、部分分单
            $arr['shipping_status']     = $shipping_status;
            update_order($order_id, $arr);
        }

        // Order log
        order_action($order['order_sn'], $arr['order_status'], $shipping_status, $order['pay_status'], $action_note, $operator);

        // Clear cache
        clear_cache_files();

        return $delivery_id;
    }

    /** For CMS 發貨(發貨單發貨確認) Btn
     * Step 1: Start Sql transaction
     * Step 2: Find all not out of stock order item
     * Step 3: Handle and create out of stock delivery
     * Step 4: Handle virtual goods
     * Step 5: Update delivery order info
     * Step 6: Update order info and order log
     * Step 7: If order is finish, calculation user rank point
     * Step 8: Handle email and SMS
     * Step 9: End Sql transaction
     */
    public function delivery_ship($order_id, $request, $order = array(), $silent = false)
    {

        global $db, $ecs, $_LANG, $_CFG, $smarty;

        // Initial data
        $utc_time = gmtime();
        $delivery = array();
        $delivery_id   = intval(trim($request['delivery_id']));
        $delivery['invoice_no'] = isset($request['invoice_no']) ? trim(str_replace(" ","", $request['invoice_no'])) : '';
        $invoice_no = $delivery['invoice_no'];
        if(empty($order_id))$order_id = intval(trim($request['order_id']));
        $logistics_id   = isset($request['logistics_id']) && $request['logistics_id'] > 0 ?  $request['logistics_id'] : 0;
        if ($logistics_id > 0) {
            $sql = "select logistics_name from".$ecs->table('logistics')." where logistics_id = $logistics_id limit 1";
            $log_name = $db->getOne($sql);
            $action_note    = isset($request['action_note']) ? trim($request['action_note'])." $log_name: $delivery[invoice_no]" : "$log_name: $delivery[invoice_no]";
        } else {
            $action_note    = isset($request['action_note']) ? trim($request['action_note']) : '';
        }

        $operator = isset($_REQUEST['operator']) ? $_REQUEST['operator'] : $request['operator'];
        $operator = isset($operator) ? $operator : $_SESSION['admin_name'];

        // Get delivery_order info
        if (!empty($delivery_id))
        {
            $delivery_order = $this->delivery_order_info($delivery_id);
        }
        else
        {
            if (!$silent) die('delivery order does not exist');
            else {
                $return_data['error'] = 'delivery order does not exist';
                return $return_data;
            }
        }

        // Query: order info
        if (!empty($order_id))
        {
            $order = order_info($order_id);
        }
        else
        {
            if (!$silent) die('order does not exist');
            else {
                $return_data['error'] = 'order does not exist';
                return $return_data;
            }
        }
        /* Step 1: Start Sql transaction */
        $db->query('START TRANSACTION');
        $db->query('SELECT order_id FROM '.$GLOBALS['ecs']->table('order_info')." WHERE order_id=".$order_id." FOR UPDATE");

        $virtual_goods = array();

        /* Step 2: Find all not out of stock order item */
        $sql="select d.rec_id,d.goods_id,d.goods_name,d.send_number,d.goods_attr_id,d.goods_price,d.order_item_id,d.is_real,d.warehouse_id from ".$GLOBALS['ecs']->table('delivery_goods')." as d where d.delivery_id='".$delivery_id."'";
        $sql.=" and d.order_item_id in (select og.rec_id from ".$GLOBALS['ecs']->table('order_goods')." as og where og.order_id='".$order_id."' and og.delivery_qty<og.goods_number)";

        $res=$db->query($sql);

        $agency_id=$order['agency_id'];

        $erpController = new ErpController();
        $fls_warehouse_id = $erpController->getFlsWarehouseId();
        $wilson_logistics_id = $this->getWilsonLogisticsId();
        $wilson_warehouse_id = $erpController->getWilsonWarehouseId();

        /* Step 3: Handle and create out of stock delivery */
        while($row=mysql_fetch_assoc($res))
        {
            $attr_id=get_attr_id($row['goods_id'],$row['goods_attr_id']);

            if (empty($row['warehouse_id'])) {
                $row['warehouse_id'] = 1;
            }

            // Check warehouse
            $sql="select w.warehouse_id,gs.goods_qty from ".$GLOBALS['ecs']->table('erp_warehouse')." as w,"
                .$GLOBALS['ecs']->table('erp_goods_attr_stock')." as gs,"
                .$GLOBALS['ecs']->table('erp_warehouse_admin')." as wa "
                ."where w.warehouse_id=gs.warehouse_id and w.warehouse_id=wa.warehouse_id and wa.admin_id='".$_SESSION['admin_id']
                ."' and w.is_valid='1' and w.is_on_sale='1' and w.agency_id='".intval($agency_id)
                ."' and gs.goods_id='".$row['goods_id']."' and gs.attr_id='".$attr_id."'";
            
            // check if it ships by FLS
            if ($delivery_order['is_fls'] == 1 && $fls_warehouse_id > 0 ) {
                $sql .= " and w.warehouse_id = ".$fls_warehouse_id." ";
            } else if ($logistics_id == $wilson_logistics_id && $wilson_warehouse_id > 0 ) {
                $sql .= " and w.warehouse_id = ".$wilson_warehouse_id." ";
            } else {
                $sql .= " and w.warehouse_id = ".$row['warehouse_id']." ";
            }

            $stock=$db->getRow($sql);

            if(empty($stock))
            {
                // Error
                $db->query('ROLLBACK');
                if (!$silent) {
                    $links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=delivery_info&delivery_id=' . $delivery_id);
                    sys_msg($_LANG['erp_delivery_without_valid_warehouse'], 1, $links);
                    break;
                } else {
                    $return_data['error'] = $_LANG['erp_delivery_without_valid_warehouse'];
                    return $return_data;
                }
            }
            // Stock is not enough
            elseif($row['send_number'] > $stock['goods_qty'])
            {
                // Error
                $db->query('ROLLBACK');
                if (!$silent) {
                    $links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=delivery_info&delivery_id=' . $delivery_id);
                    sys_msg(sprintf($_LANG['act_good_vacancy'], $row['goods_name']), 1, $links);
                    break;
                } else {
                    $return_data['error'] = sprintf($_LANG['act_good_vacancy'], $row['goods_name']);
                    return $return_data;
                }
            }

            // Out of stock
            if($_CFG['stock_dec_time']==0)
            {
                require_once(ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_goods_attr.php');
                require_once(ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_warehouse.php');

                $erp_delivery_id=add_delivery();

                if($erp_delivery_id > 0)
                {
                    // Update info
                    $sql="update ".$GLOBALS['ecs']->table('erp_delivery')." set delivery_to='".$_SESSION['admin_name'].
                        "',warehouse_id ='".$stock['warehouse_id'].
                        "',order_id='".$order_id.
                        "',post_by='".$_SESSION['admin_id'].
                        "',post_time='".gmtime().
                        "',approved_by='".$_SESSION['admin_id'].
                        "',approve_time='".gmtime().
                        "',last_act_time='".gmtime().
                        "',approve_remark ='".$_LANG['erp_delivery_auto_on_ship'].
                        "',status='3' where delivery_id='".$erp_delivery_id."'";
                    if(!$db->query($sql))
                    {
                        // Error
                        $db->query('ROLLBACK');
                        if (!$silent) {
                            $links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=delivery_info&delivery_id=' . $delivery_id);
                            sys_msg($_LANG['erp_delivery_auto_on_ship_failed'], 1, $links);
                            break;
                        } else {
                            $return_data['error'] = $_LANG['erp_delivery_auto_on_ship_failed'];
                            return $return_data;
                        }
                    }

                    // Insert delivery item
                    $sql="insert into ".$GLOBALS['ecs']->table('erp_delivery_item')." set delivery_id='".$erp_delivery_id.
                        "',goods_id='".$row['goods_id'].
                        "',attr_id='".$attr_id.
                        "',expected_qty='".$row['send_number'].
                        "',delivered_qty='".$row['send_number'].
                        "',price='".$row['goods_price'].
                        "',account_due='".($row['goods_price']*$row['send_number']).
                        "',sys_delivery_item_id ='".$row['rec_id'].
                        "',order_item_id=".$row['order_item_id']."";
                    if(!$db->query($sql))
                    {
                        // Error
                        $db->query('ROLLBACK');
                        if (!$silent) {
                            $links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=delivery_info&delivery_id=' . $delivery_id);
                            sys_msg($_LANG['erp_delivery_auto_on_ship_failed'], 1, $links);
                            break;
                        } else {
                            $return_data['error'] = $_LANG['erp_delivery_auto_on_ship_failed'];
                            return $return_data;
                        }
                    }
                }
                elseif($erp_delivery_id==-1)
                {
                    // Error
                    $db->query('ROLLBACK');
                    if (!$silent) {
                        $links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=delivery_info&delivery_id=' . $delivery_id);
                        sys_msg($_LANG['erp_delivery_without_valid_warehouse'], 1, $links);
                        break;
                    } else {
                        $return_data['error'] = $_LANG['erp_delivery_without_valid_warehouse'];
                        return $return_data;
                    }
                }
                else
                {
                    // Error
                    $db->query('ROLLBACK');
                    if (!$silent) {
                        $links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=delivery_info&delivery_id=' . $delivery_id);
                        sys_msg($_LANG['erp_delivery_auto_on_ship_failed'], 1, $links);
                        break;
                    } else {
                        $return_data['error'] = $_LANG['erp_delivery_auto_on_ship_failed'];
                        return $return_data;
                    }
                }

                if(!deduct_stock_by_delivery($erp_delivery_id))
                {
                    // Error
                    $db->query('ROLLBACK');
                    if (!$silent) {
                        $links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=delivery_info&delivery_id=' . $delivery_id);
                        sys_msg($_LANG['erp_delivery_auto_on_ship_failed'], 1, $links);
                        break;
                    } else {
                        $return_data['error'] = $_LANG['erp_delivery_auto_on_ship_failed'];
                        return $return_data;
                    }
                }
            }

            // Handle virtual card
            if ($row['is_real'] == 0)
            {
                $virtual_goods[] = array(
                    'goods_id' => $row['goods_id'],
                    'goods_name' => $row['goods_name'],
                    'num' => $row['send_number']
                );
            }
        }

        /* Step 4: Handle virtual goods */
        if (is_array($virtual_goods) && count($virtual_goods) > 0)
        {
            foreach ($virtual_goods as $virtual_value)
            {
                if(!virtual_card_shipping($virtual_value,$order['order_sn'], $msg, 'split'))
                {
                    // Error
                    $db->query('ROLLBACK');
                    if (!$silent) {
                        $links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=delivery_info&delivery_id=' . $delivery_id);
                        sys_msg($_LANG['act_false'], 1, $links);
                        break;
                    } else {
                        $return_data['error'] = $_LANG['act_false'];
                        return $return_data;
                    }
                }
            }
        }

        /* Step 5: Update delivery order info */
        $_delivery['invoice_no'] = $invoice_no;
        $_delivery['status'] = DO_DELIVERED;
        $_delivery['logistics_id'] = $logistics_id;
        $query = $db->autoExecute($ecs->table('delivery_order'), $_delivery, 'UPDATE', "delivery_id = $delivery_id", 'SILENT');
        if (!$query)
        {
            // Error
            $db->query('ROLLBACK');
            if (!$silent) {
                $db->query('ROLLBACK');

                $links[] = array('text' => $_LANG['delivery_sn'] . $_LANG['detail'], 'href' => 'order.php?act=delivery_info&delivery_id=' . $delivery_id);
                sys_msg($_LANG['act_false'] . __LINE__, 1, $links);

            } else {
                $return_data['error'] = $_LANG['act_false'];
                return $return_data;
            }
        }

        /* Step 6: Update order info and order log */
        $order_finish = $this->get_all_delivery_finish($order_id);
        $shipping_status = ($order_finish == 1) ? SS_SHIPPED : SS_SHIPPED_PART;
        $order_status = ($order_finish == 1) ? OS_CONFIRMED : $order['order_status'];
        $arr['shipping_status']     = $shipping_status;
        $arr['order_status']        = $order_status;
        $arr['shipping_time']       = $utc_time;

        if(!update_order($order_id, $arr))
        {
            // Error
            $db->query('ROLLBACK');
            if (!$silent) {
                $db->query('ROLLBACK');

                $links[] = array('text' => $_LANG['delivery_sn'] . $_LANG['detail'], 'href' => 'order.php?act=delivery_info&delivery_id=' . $delivery_id);
                sys_msg($_LANG['act_false'] . __LINE__, 1, $links);

            } else {
                $return_data['error'] = $_LANG['act_false'];
                return $return_data;
            }
        }

        // Order log
        if(!order_action($order['order_sn'], $order_status, $shipping_status, $order['pay_status'], $action_note, $operator, 1))
        {
            // Error
            $db->query('ROLLBACK');
            if (!$silent) {
                $db->query('ROLLBACK');

                $links[] = array('text' => $_LANG['delivery_sn'] . $_LANG['detail'], 'href' => 'order.php?act=delivery_info&delivery_id=' . $delivery_id);
                sys_msg($_LANG['act_false'] . __LINE__, 1, $links);

            } else {
                $return_data['error'] = $_LANG['act_false'];
                return $return_data;
            }
        }

        if ($order_finish)
        {
            /* Step 7: If order is finish, calculation user rank point */
            if ($order['user_id'] > 0)
            {
                $user = user_info($order['user_id']);
                // Calculation rank point user can get
                $integral = integral_to_give($order);

                if(!log_account_change($order['user_id'], 0, 0, intval($integral['rank_points']), intval($integral['custom_points']), sprintf($_LANG['order_gift_integral'], $order['order_sn']), ACT_OTHER, 0, ['sales_order_id' => $order_id]))
                {
                    // Error
                    $db->query('ROLLBACK');
                    if (!$silent) {
                        $db->query('ROLLBACK');

                        $links[] = array('text' => $_LANG['delivery_sn'] . $_LANG['detail'], 'href' => 'order.php?act=delivery_info&delivery_id=' . $delivery_id);
                        sys_msg($_LANG['act_false'] . __LINE__, 1, $links);

                    } else {
                        $return_data['error'] = $_LANG['act_false'];
                        return $return_data;
                    }
                }

                // Send order bouns
                if(!send_order_bonus($order_id))
                {
                    // Error
                    $db->query('ROLLBACK');
                    if (!$silent) {
                        $db->query('ROLLBACK');

                        $links[] = array('text' => $_LANG['delivery_sn'] . $_LANG['detail'], 'href' => 'order.php?act=delivery_info&delivery_id=' . $delivery_id);
                        sys_msg($_LANG['act_false'] . __LINE__, 1, $links);

                    } else {
                        $return_data['error'] = $_LANG['act_false'];
                        return $return_data;
                    }
                }

                // If the order used a referral coupon, give reward for the referer user
                $coupons = order_coupons($order_id);
                if (!empty($coupons))
                {
                    foreach ($coupons as $coupon)
                    {
                        if ($coupon['referer_id'] > 0)
                        {
                            $referral_integral = 1000;
                            $log_msg = '推薦新會員 ' . $user['user_name'] . ' 訂單 ' . $order['order_sn'] . ' 贈送的積分';
                            if(!log_account_change($coupon['referer_id'], 0, 0, 0, $referral_integral, $log_msg, ACT_OTHER, 0, ['sales_order_id' => $order_id]))
                            {
                                // Error
                                $db->query('ROLLBACK');
                                if (!$silent) {
                                    $db->query('ROLLBACK');

                                    $links[] = array('text' => $_LANG['delivery_sn'] . $_LANG['detail'], 'href' => 'order.php?act=delivery_info&delivery_id=' . $delivery_id);
                                    sys_msg($_LANG['act_false'] . __LINE__, 1, $links);

                                } else {
                                    $return_data['error'] = $_LANG['act_false'];
                                    return $return_data;
                                }
                            }
                        }
                    }
                }
            }
            /* Step 8: Handle email and SMS, In silent mode, we not send email */
            if (!$silent)
            {
                $order['invoice_no'] = $invoice_no;
                $this->delivery_send_mail($order,$delivery_id);  // we don't send email at this moment, it should be send when courier picked up.
            }
        }

        clear_cache_files();

        /* Step 9: End Sql transaction */
        $db->query('COMMIT');

        // Operation Log
        admin_operation_log($operator, '訂單發貨', $order_id);
    }

    /** For CMS 一鍵發貨 Btn
     */
    public function quick_create_delivery_order($order_id, $request, $silent = false)
    {

        global $db, $ecs, $_LANG, $_CFG;

        // Query: order info
        if (!empty($order_id))
        {
            $order = order_info($order_id);
        }
        else
        {
            die('order does not exist');
        }
        // Initial data
        $logistics_id = intval(trim($request['logistics_id']));
        $invoice_no   = empty($request['invoice_no']) ? "" : $request['invoice_no'];
        $action_note  = isset($request['action_note']) ? trim($request['action_note']) : '';
        $operator     = isset($request['operator']) ? trim($request['operator']) : '';

        /* Step 1: Call prepare_create_delivery_order function */
        $data = $this->prepare_create_delivery_order($order_id, $action_note, $order);
 
        /* Step 2: Handle return data [order, exist_real_goods, attr, goods_list] */
        extract($data);

        $handle_data = array();
        $send_number = [];
        $delivery    = [];
        // Handle send number
        $send_number = $this->cal_send_number($goods_list);
        $erpController = new ErpController();

        foreach ($goods_list as $goods) {
            //$fls_qty = $erpController->getSellableProductQtyOfFls($goods['goods_id']);
            //$storage_without_fls =  $goods['storage'] - $fls_qty;

            // if ($storage_without_fls < 0) {
            //    $storage_without_fls = 0;
            // }

            if ($goods['storage_wh'][$goods['storage_warehouse_id']] < 0){
                $goods['storage_wh'][$goods['storage_warehouse_id']] = 0;
            }

            // main warehouse / shop
            if ($send_number[$goods['rec_id']] > $goods['storage_wh'][$goods['storage_warehouse_id']]) {
                $send_number[$goods['rec_id']] = $goods['storage_wh'][$goods['storage_warehouse_id']];
                
                if ($goods['storage_wh'][$goods['storage_warehouse_id']] == 0) {
                    $send_number[$goods['rec_id']] = $_LANG['act_good_vacancy'];
                }
            }
        }
        
        // Handle delivery info
        $delivery = $this->create_delivery_info($order);

        $handle_data['send_number'] = $send_number;
        $handle_data['delivery']    = $delivery;

        /* Step 3: Call create_delivery_order function */
        $delivery_id = $this->create_delivery_order($order_id, $handle_data, $action_note, $order);

        /* Step 4: Handle input delivery_ship data*/
        $input_data = [];
        $input_data['order_id']     = $order_id;
        $input_data['logistics_id'] = $logistics_id;
        $input_data['delivery_id']  = $delivery_id;
        $input_data['invoice_no']   = $invoice_no;
        $input_data['action_note']  = $action_note;

        /* Step 5: Call delivery_ship function */
        $this->delivery_ship($order_id, $input_data, $order, $silent);

        return $delivery_id;
    }

    /** CMS 批量出貨功能 Btn
     *** 此功能用於批量生成出貨單, 將直接扣除庫存及出貨
     */
    public function batch_create_delivery_order()
    {
        /* Step 1: Handle REQUEST */
        global $ecs, $db, $_LANG;

        $_REQUEST['operator'] = $operator;
        $logistics = $_REQUEST['logistics'];

        $sql = "SELECT order_sn FROM ".$ecs->table('order_info')." WHERE order_id ".db_create_in($_REQUEST['order_sn']);
        $sn_list    = $db->getCol($sql);
        $operator   = ($_REQUEST['salesperson']) ? $_REQUEST['salesperson'] : $_SESSION['admin_name'];
        $undo_list  = [];
        $error      = [];

        /* Step 2: Check order ststus */
        $sql = "SELECT oi.order_sn FROM ".$ecs->table('order_info')." as oi ".
            "WHERE 1 ".order_query_sql('await_ship', $alias = 'oi.'). " AND oi.order_sn ".db_create_in($sn_list)." order by oi.order_id ASC "; // **ASC, 愈早的訂單先出貨, 防止缺貨時先出新單
        $do_list = $db->getCol($sql);
        $undo_list = array_values(array_diff($sn_list, $do_list));
        foreach ($undo_list as $order_sn) {
            $error[$order_sn] = '訂單未付款/已完成出貨';
        }
        if ($do_list == false && empty($do_list)) {
            return ['mass_key' => $mass_key, 'finish_list' => [], 'undo_list' => $undo_list, 'error_list' => $error];
        }
        /* Step 2.1: Check order country(Only can use sf+ and only HK 3409 order.) */
        $sql = "SELECT oi.order_sn FROM ".$ecs->table('order_info')." as oi ".
            //"WHERE 1 AND oi.country = 3409 AND oi.order_sn ".db_create_in($do_list)." order by oi.order_id ASC "; // **ASC, 愈早的訂單先出貨, 防止缺貨時先出新單
            "WHERE 1 AND oi.order_sn ".db_create_in($do_list)." order by oi.order_id ASC "; // **ASC, 愈早的訂單先出貨, 防止缺貨時先出新單 , 取消地區限制
        $tmp_list = $db->getCol($sql);

        $undo_list = array_values(array_diff($do_list, $tmp_list));
        foreach ($undo_list as $order_sn) {
            $error[$order_sn] = '訂單送貨地址不在香港';
        }
        $do_list = $tmp_list;

        /* Step 2.2: Check order is held by admin */
        $sql = "SELECT oi.order_sn FROM ".$ecs->table('order_info')." as oi ".
            "WHERE oi.is_hold != 0 AND oi.order_sn ".db_create_in($do_list)." order by oi.order_id ASC ";
        $hold_list = $db->getCol($sql);

        $do_list = array_values(array_diff($do_list, $hold_list));
        foreach ($hold_list as $order_sn) {
            $error[$order_sn] = '訂單暫時無法出貨';
            $undo_list[] = $order_sn;
        }

        /* Step 3: Create Mass Column and unset sn if exist in mass table */
        $result = $this->create_mass_key($do_list, $logistics);

        // Error handle
        if ($result == false && !is_array($result)) {
            return ['mass_key' => $mass_key, 'finish_list' => [], 'undo_list' => $undo_list, 'error_list' => $error];
        }
        // New elements
        $mass_key  = $result['mass_key'];
        $do_list   = $result['do_list'];
        $undo_list = array_merge($result['undo_list'], $undo_list);
        foreach ($result['undo_list'] as $order_sn) {
            $error[$order_sn] = '訂單已在其他進程中';
        }

        // Error handle: No order_sn need to work in flow, return
        if (empty($do_list)) {
            return ['mass_key' => $mass_key, 'finish_list' => [], 'undo_list' => $undo_list, 'error_list' => $error];
        }

        // Step 4: Ready to 一鍵出貨 Step, Lock row by order_sn
        $finish_list = [];
        $undo_id     = [];
        
        $erpController = new ErpController();
        foreach ($do_list as $key => $order_sn) {
            $db->query('START TRANSACTION');
            $db->query('SELECT * FROM '.$GLOBALS['ecs']->table('order_info')." WHERE order_sn = $order_sn FOR UPDATE");
            $order       = order_info(null, $order_sn);
            $order_id    = $order['order_id'];
            $action_note = '系統批量出貨:'.$mass_key;

            $data = $this->prepare_create_delivery_order($order_id, $action_note, $order, true);

            // This order can not create delivery order, undo and rec error msg.
            if (isset($data['error'])) {
                $db->query('ROLLBACK');
                array_push($undo_list, $order_sn);
                $error[$order_sn] = $data['error'];
                $undo_id[] = $order_id;
                // We not stop the loop and only undo this order sn.
                continue;
            }

            extract($data);

            $handle_data = array();
            // Handle send number
            $send_number = $this->cal_send_number($goods_list);

            $fls_warehouse_id = $erpController->getFlsWarehouseId();
            $wilson_warehouse_id = $erpController->getWilsonWarehouseId();

            // for FLS & wilson
            foreach ($goods_list as $goods) {  
                //$fls_qty = $erpController->getSellableProductQtyOfFls($goods['goods_id']);
                $fls_qty = $goods['storage_wh'][$fls_warehouse_id];
                $wilson_qty = $goods['storage_wh'][$wilson_warehouse_id];

                if (isset($_REQUEST['is_fls']) && $_REQUEST['is_fls'] == 1) {
                    if ($fls_qty < 0) {
                        $fls_qty = 0;
                    }

                    // FLS warehouse
                    if ($send_number[$goods['rec_id']] > $fls_qty) {
                        $send_number[$goods['rec_id']] = $fls_qty;
                    }

                    if (gettype($send_number[$goods['rec_id']]) == 'string') {
                        unset($send_number[$goods['rec_id']]);
                    }

                } else if ($goods['storage_warehouse_id'] == $wilson_warehouse_id) {
                    if ($wilson_qty < 0) {
                        $wilson_qty = 0;
                    }
                    
                    // Wilson warehouse
                    if ($send_number[$goods['rec_id']] > $wilson_qty) {
                        $send_number[$goods['rec_id']] = $wilson_qty;
                    }

                    if (gettype($send_number[$goods['rec_id']]) == 'string') {
                        unset($send_number[$goods['rec_id']]);
                    }

                } else {

                    // $storage_without_fls =  $goods['storage'] - $fls_qty;
                    // 
                    // if ($storage_without_fls < 0) {
                    //     $storage_without_fls = 0;
                    // }

                    if ($goods['storage_wh'][$goods['storage_warehouse_id']] < 0){
                        $goods['storage_wh'][$goods['storage_warehouse_id']] = 0;
                    }

                    // main warehouse / shop
                    if ($send_number[$goods['rec_id']] > $goods['storage_wh'][$goods['storage_warehouse_id']]) {
                        //$send_number[$goods['rec_id']] = $storage_without_fls;
                        $send_number[$goods['rec_id']] = $_LANG['act_good_vacancy'];
                    }
                }  
            }

            foreach ($send_number as $order_goods => $send_num) {
                // check warehouse without fls
                if (gettype($send_num) == 'string') {

                    if ($send_num == '0') {
                        $send_num = 0;
                    }

                    if ($send_num == '貨已發完') {
                        unset($send_number[$order_goods]);
                    }
                }

                if (gettype($send_num) == 'integer') {
                    if ($send_num == 0) {
                        unset($send_number[$order_goods]);
                    }
                }
            }
            // for FLS & wilson end

            // Handle delivery info
            $delivery    = $this->create_delivery_info($order);

            $handle_data['send_number'] = $send_number;
            $handle_data['delivery']    = $delivery;
            $handle_data['operator']    = $operator;

            // If order have goods out of stock, we not delivery in batch function.
            $goods_error = '';

            foreach ($send_number as $order_goods => $send_num ) {
                if(!is_integer($send_num) || !($send_num > 0)) {
                    $goods_error = '有未齊貨/已經部份出貨的產品';
                    break;
                }
            }


            if (!empty($goods_error)) {
                $db->query('ROLLBACK');
                array_push($undo_list, $order_sn);
                $error[$order_sn] = $goods_error;
                $undo_id[] = $order_id;
                // We not stop the loop and only undo this order sn.
                continue;
            }

            // Call create_delivery_order function
            $delivery_id = $this->create_delivery_order($order_id, $handle_data, $action_note, $order, true);
            
            // This order can not create delivery order, undo and rec error msg.
            if (is_array($delivery_id) && isset($delivery_id['error'])) {
                $db->query('ROLLBACK');
                array_push($undo_list, $order_sn);
                $error[$order_sn] = $delivery_id['error'];
                $undo_id[] = $order_id;
                // We not stop the loop and only undo this order sn.
                continue;
            }

            // update order is FLS
            if (!empty($delivery_id) && isset($_REQUEST['is_fls']) && $_REQUEST['is_fls'] == 1) {
                $sql = "update ".$GLOBALS['ecs']->table('delivery_order')." set is_fls = 1 where delivery_id = ".$delivery_id." ";
                $db->query($sql);
            }

            // Handle input delivery_ship data
            $input_data = [];
            $input_data['order_id']     = $order_id;
            $input_data['logistics_id'] = 0; //In this flow we not add logistics
            $input_data['delivery_id']  = $delivery_id;
            $input_data['invoice_no']   = ''; //In this flow we not input invoice
            $input_data['action_note']  = $action_note;
            $input_data['operator']     = $operator;

            /* ** 門店自取 */
            if($logistics =='storepicking') {
                $input_data['logistics_id'] = 7;
                $input_data['action_note']  = $action_note;
            } else if ($logistics =='wilson') {
                $input_data['logistics_id'] = $this->getWilsonLogisticsId();
                $input_data['action_note']  = $action_note;
            }

            $res = $this->delivery_ship($order_id, $input_data, $order, true);
           
            // This order can not create delivery order, undo and rec error msg.
            if (is_array($res) && isset($res['error'])) {
                $db->query('ROLLBACK');
                array_push($undo_list, $order_sn);
                $error[$order_sn] = $res['error'];
                $undo_id[] = $order_id;
                // We not stop the loop and only undo this order sn.
                continue;
            }
            $db->query('COMMIT');
            $finish_list[$order_sn]['order_sn'] = $order_sn;
            if($logistics =='storepicking') {
                $finish_list[$order_sn]['process']  = MK_MAIL;
                $finish_list[$order_sn]['process_string']  = ($_LANG['mass_process_'.MK_MAIL]) ? $_LANG['mass_process_'.MK_MAIL] : MK_MAIL;
            } else if ($logistics =='wilson') {
                $finish_list[$order_sn]['process']  = MK_FINISH;
                $finish_list[$order_sn]['process_string']  = ($_LANG['mass_process_'.MK_FINISH]) ? $_LANG['mass_process_'.MK_FINISH] : MK_FINISH;
            } else {
                $finish_list[$order_sn]['process']  = MK_DELIVERY;
                $finish_list[$order_sn]['process_string']  = ($_LANG['mass_process_'.MK_DELIVERY]) ? $_LANG['mass_process_'.MK_DELIVERY] : MK_DELIVERY;
            }

            $finish_list[$order_sn]['delivery_id']  = $delivery_id;
            $finish_list[$order_sn]['order_id']    = $order_id;
        }

        // Step 5: Finish 一鍵出貨 Step, remove unfinishing order_sn on mass table
        if (count($undo_id) > 0) {
            $sql = "DELETE FROM ".$ecs->table('mass_delivery')." WHERE order_id ".db_create_in($undo_id)." AND mass_key = '$mass_key'";
            $db->query($sql);
        }

        // Step 6: Update mass key process
        if(!empty($finish_list)) {
            $sql_process = MK_DELIVERY;
            if($logistics =='storepicking') {
                $sql_process = MK_MAIL;
            } else if ($logistics =='wilson') {
                $sql_process = MK_FINISH;
            }
            $sql = "UPDATE ".$ecs->table('mass_delivery')."SET process = ".$sql_process.", ".
            "delivery_id = case ";
            foreach ($finish_list as $order_sn => $info) {
                $sql .= " when order_id = ".$info['order_id']." then ".$info['delivery_id'];
            }
            $sql .=" else delivery_id end ";
            $sql .=" WHERE mass_key = '$mass_key'";

            $db->query($sql);
            if($logistics =='storepicking' || $logistics =='wilson') {
                $this->mass_send_email($mass_key);
            }

            if($logistics =='wilson') {
                $sql = "UPDATE " . $ecs->table('mass_delivery') . " SET process = ".MK_FINISH." WHERE mass_key = '" . $mass_key . "' ";
                $db->query($sql);
            }
        }
        return ['mass_key' => $mass_key, 'finish_list' => $finish_list, 'undo_list' => $undo_list, 'error_list' => $error, 'logistics' => $logistics];
    }

    /** Calculate send_number
     * @var Array goods_list
     * @return Array
     */
    function cal_send_number($goods_list)
    {

        $send_number = [];
        foreach ($goods_list as $key => $goods) {
            if ($goods['goods_id'] > 0 && $goods['extension_code'] == 'package_buy') {
                foreach ($goods['package_goods_list'] as $k => $package) {
                    $send_number[$goods['rec_id']][$package['g_p']] = $package['send'];
                }
            } else {
                $send_number[$goods['rec_id']] = $goods['send'];
            }
        }

        return $send_number;
    }

    /** Create delivery info
     * @var Array order
     * @return Array
     */
    function create_delivery_info($order)
    {

        $delivery = [];

        $delivery['order_sn']       = $order['order_sn'];
        $delivery['add_time']       = $order['order_time'];
        $delivery['user_id']        = $order['user_id'];
        $delivery['how_oos']        = $order['how_oos'];
        $delivery['shipping_id']    = $order['shipping_id'];
        $delivery['shipping_fee']   = $order['shipping_fee'];
        $delivery['consignee']      = $order['consignee'];
        $delivery['address']        = $this->escape_string($order['address']);
        $delivery['country']        = $order['country'];
        $delivery['province']       = $order['province'];
        $delivery['city']           = $order['city'];
        $delivery['district']       = $order['district'];
        $delivery['sign_building']  = $order['sign_building'];
        $delivery['email']          = $order['email'];
        $delivery['zipcode']        = $order['zipcode'];
        $delivery['tel']            = $order['tel'];
        $delivery['mobile']         = $order['mobile'];
        $delivery['best_time']      = $order['best_time'];
        $delivery['postscript']     = $order['postscript'];
        $delivery['serial_numbers'] = $this->escape_string($order['serial_numbers']);
        $delivery['insure_fee']     = $order['insure_fee'];
        $delivery['agency_id']      = $order['agency_id'];
        $delivery['shipping_name']  = $order['shipping_name'];

        return $delivery;
    }

    /**
     * 判断订单的发货单是否全部发货
     * @param   int     $order_id  订单 id
     * @return  int     1，全部发货；0，未全部发货；-1，部分发货；-2，完全没发货；
     */
    function get_all_delivery_finish($order_id)
    {
        $return_res = 0;
        $orderController = new OrderController();
        if (empty($order_id))
        {
            return $return_res;
        }

        /* 未全部分单 */
        if (!$orderController->get_order_finish($order_id))
        {
            return $return_res;
        }
        /* 已全部分单 */
        else
        {
            // 是否全部发货
            $sql = "SELECT COUNT(delivery_id)
                    FROM " . $GLOBALS['ecs']->table('delivery_order') . "
                    WHERE order_id = '$order_id'
                    AND status = ".DO_NORMAL." ";
            $sum = $GLOBALS['db']->getOne($sql);
            // 全部发货
            if (empty($sum))
            {
                $return_res = 1;
            }
            // 未全部发货
            else
            {
                /* 订单全部发货中时：当前发货单总数 */
                $sql = "SELECT COUNT(delivery_id)
                FROM " . $GLOBALS['ecs']->table('delivery_order') . "
                WHERE order_id = '$order_id'
                AND status <> ".DO_RETURNED." ";
                $_sum = $GLOBALS['db']->getOne($sql);
                if ($_sum == $sum)
                {
                    $return_res = -2; // 完全没发货
                }
                else
                {
                    $return_res = -1; // 部分发货
                }
            }
        }

        return $return_res;
    }

    /**
     * 订单单个商品或货品的已发货数量
     *
     * @param   int     $order_id       订单 id
     * @param   int     $goods_id       商品 id
     * @param   int     $product_id     货品 id
     *
     * @return  int
     */
    function order_delivery_num($order_id, $goods_id, $product_id = 0)
    {
        $sql = 'SELECT SUM(G.send_number) AS sums
                FROM ' . $GLOBALS['ecs']->table('delivery_goods') . ' AS G, ' . $GLOBALS['ecs']->table('delivery_order') . ' AS O
                WHERE O.delivery_id = G.delivery_id
                AND O.status = 0
                AND O.order_id = ' . $order_id . '
                AND G.extension_code <> "package_buy"
                AND G.goods_id = ' . $goods_id;

        $sql .= ($product_id > 0) ? " AND G.product_id = '$product_id'" : '';

        $sum = $GLOBALS['db']->getOne($sql);

        if (empty($sum))
        {
            $sum = 0;
        }

        return $sum;
    }

    /**
     * 取得发货单信息
     * @param   int     $delivery_order   发货单id（如果delivery_order > 0 就按id查，否则按sn查）
     * @param   string  $delivery_sn      发货单号
     * @return  array   发货单信息（金额都有相应格式化的字段，前缀是formated_）
     */
    function delivery_order_info($delivery_id, $delivery_sn = '')
    {
        $return_order = array();
        if (empty($delivery_id) || !is_numeric($delivery_id))
        {
            return $return_order;
        }

        $where = '';
        /* 获取管理员信息 */
        $admin_info = admin_info();

        /* 如果管理员属于某个办事处，只列出这个办事处管辖的发货单 */
        if ($admin_info['agency_id'] > 0)
        {
            $where .= " AND agency_id = '" . $admin_info['agency_id'] . "' ";
        }

        /* 如果管理员属于某个供货商，只列出这个供货商的发货单 */
        if ($admin_info['suppliers_id'] > 0)
        {
            $where .= " AND suppliers_id = '" . $admin_info['suppliers_id'] . "' ";
        }

        $sql = "SELECT * FROM " . $GLOBALS['ecs']->table('delivery_order');
        if ($delivery_id > 0)
        {
            $sql .= " WHERE delivery_id = '$delivery_id'";
        }
        else
        {
            $sql .= " WHERE delivery_sn = '$delivery_sn'";
        }

        $sql .= $where;
        $sql .= " LIMIT 0, 1";
        $delivery = $GLOBALS['db']->getRow($sql);
        if ($delivery)
        {
            /* 格式化金额字段 */
            $delivery['formated_insure_fee']     = price_format($delivery['insure_fee'], false);
            $delivery['formated_shipping_fee']   = price_format($delivery['shipping_fee'], false);

            /* 格式化时间字段 */
            $delivery['formated_add_time']       = local_date($GLOBALS['_CFG']['time_format'], $delivery['add_time']);
            $delivery['formated_update_time']    = local_date($GLOBALS['_CFG']['time_format'], $delivery['update_time']);

            $return_order = $delivery;
        }

        return $return_order;
    }

    /**
     * Get all order to process
     * @param   bool            $process  will return status if true, default false
     * @return  mysql_result    concatenated order_sn (and the progress status)
     */
    public function get_order_sn($process = false)
    {
        global $db, $ecs, $_LANG;
        $sql = "SELECT oi.order_sn, oi.order_id " . ($process ? ",process" : ""). ", logistics, delivery_id FROM " . $ecs->table('mass_delivery') . " md " .
                "LEFT JOIN " . $ecs->table('order_info') . " oi ON oi.order_id = md.order_id " .
                "WHERE md.mass_key = '" . $_REQUEST['mass_key'] . "' AND md.process NOT IN ( ".MK_CANCEL.")";

        $res = $db->getAll($sql);
        $arr = [];
        foreach ($res as $key => $order) {

            $order_sn  = $order['order_sn'];
            $process   = $order['process'];
            $logistics = $order['logistics'];
            $arr[$order_sn]['delivery_id']  = $order['delivery_id'];;
            $arr[$order_sn]['order_sn']  = $order_sn;
            $arr[$order_sn]['order_id']  = $order['order_id'];
            $arr[$order_sn]['process']   = $process;
            $arr[$order_sn]['logistics'] = $logistics;
            $arr[$order_sn]['process_string'] = ($_LANG['mass_process_'.$process]) ? $_LANG['mass_process_'.$process] : $process;
        }

        return $arr;
    }

    /**
     * Generate a key for all orders in mass delivery
     */
    public function create_mass_key($order_arr, $logistics='sfplus')
    {
        global $db,$ecs;

        /* Step 1: Create random mass key */
        $characters = array_merge(range(0,9), range('a', 'z'),range('A', 'Z'));
        for($i = 0; $i < 10; $i++){
            $mass_key .= $characters[mt_rand(0,count($characters) - 1)];
        }
        /* Step 2: Check and remove is processing order_sn */
        $sql = "SELECT oi.order_sn FROM ". $ecs->table('mass_delivery') . " as md ".
        " LEFT JOIN " . $ecs->table('order_info') . " as oi on oi.order_id = md.order_id ".
        " WHERE md.order_id IN (SELECT order_id FROM " . $ecs->table('order_info') . " WHERE order_sn ".db_create_in($order_arr).") AND md.process NOT IN( ".MK_FINISH.", ".MK_CANCEL.") ";

        $undo_list = $db->getCol($sql);

        $do_list = array_values(array_diff($order_arr, $undo_list));

        $sql = "INSERT INTO " . $ecs->table('mass_delivery') . " (order_id, mass_key,process,logistics) ".
            " SELECT order_id,'" . $mass_key . "',".MK_UNDELIVERY.",'".$logistics."' FROM " . $ecs->table('order_info') .
            " WHERE order_sn ".db_create_in($do_list);

        if ($db->query($sql)) {
            return [
                'mass_key'  => $mass_key,
                'do_list'   => $do_list,
                'undo_list' => $undo_list
                ];
        } else {
            return false;
        }
    }

    public function remove_mass_key()
    {
        global $db, $ecs;
        $sql = "DELETE " . $ecs->table('mass_delivery') . " WHERE mass_key = '" . $_REQUEST['mass_key'] . "' AND process = ".MK_FINISH;
        $db->query($sql);
    }

    public function export_fls_excel()
    {
        global $db, $ecs;

        ini_set('memory_limit', '256M');
        define('PCLZIP_TEMPORARY_DIR', ROOT_PATH . 'temp/');
        require_once ROOT_PATH . 'includes/excel/PHPExcel.php';
        require_once ROOT_PATH . 'includes/excel/PHPExcel/IOFactory.php';
        \PHPExcel_Settings::setZipClass(\PHPExcel_Settings::PCLZIP);

        $logistics = $_REQUEST['logistics'];
        if(empty($logistics)) {
            $logistics = $db->getOne("SELECT logistics FROM ". $ecs->table('mass_delivery')." WHERE mass_key = '".$_REQUEST['mass_key']."' LIMIT 1");
        }

        if ($logistics == 'sfplus') {
            $courier = '順豐到+';
        } else if ($logistics == 'tqb') {
            $courier = '宅急便B2(Web)';
        } else {
            $courier = '其他';
        }

        $order_sn = $this->get_order_sn();
        $order_sn_list = array_column($order_sn, 'order_sn');
        
        $export = array();
        $mass_key = $_REQUEST['mass_key'];

        $newFile = 'yoho_export_to_fls_' . date('YmdHis') . '.xlsx';
        $objPHPExcel = new \PHPExcel();
        $fileType = 'Excel2007';
        /* Excel title */
        $title_list = ['ID', 'Order No', 'Ship Date', 'Delivered Date', 'Recipient Phone', 'Recipient Address', 'Recipient Name', 'Sender Phone', 'Sender Address', 'Sender Name', 'Brand', 'SKU ID' , 'Qty' , 'Barcode' , 'Goods Name', 'Weight' ,'Courier'];
        foreach (range('A', 'Q') as $char_key => $column) {
            $objPHPExcel->getActiveSheet()->SetCellValue($column . '1', $title_list[$char_key]);
        }
        foreach ($order_sn_list as $order_sn)
        {
            $order = order_info(0, $order_sn);
            // $weight_price = order_weight_price($order['order_id']);
            $sql = "SELECT c.cat_name, dg.send_number ,dg.send_number as qty, g.goods_name, g.goods_sn, g.goods_id, g.goods_weight, b.brand_name FROM " . $ecs->table('delivery_goods') ." as dg ".
                " LEFT JOIN ". $ecs->table('goods') ." as g ON dg.goods_id = g.goods_id ".
                " LEFT JOIN ". $ecs->table('category') ." as c ON c.cat_id = g.cat_id ".
                " LEFT JOIN ". $ecs->table('brand') ." as b ON g.brand_id = b.brand_id ".
                " WHERE delivery_id = (SELECT delivery_id FROM ".$ecs->table('mass_delivery')." WHERE order_id = ".$order['order_id']." AND mass_key = '".$mass_key."' ) ";

            $goods_info = $db->getAll($sql);
            $p_name = '';
            foreach ($goods_info as $goods) {
                $p_name = $goods['goods_name'] ;
                $tel = !empty($order['mobile']) ? $order['mobile'] : $order['tel'];
                $tel = explode('-', $tel);
                $tel = preg_replace('/\s(?=)/', '', $tel[count($tel) - 1]);
                $send_date = local_date('d/m/Y');
                $receive_date = local_date('d/m/Y', local_strtotime("+1 day"));
                // This array is base on TQB excel example:

                if (empty($goods['goods_weight'])) {
                    $goods['goods_weight'] = 0;
                }

                $export[] = array(
                    $_REQUEST['mass_key'],$order_sn, // 客戶管理編號, order_sn
                    $send_date, $receive_date,  //預定出貨日,預定收件日,
                    $tel,$order['address'], // 收件人電話, 收件人地址,
                    $order['consignee'],'85253356996', // 收件人姓名, 寄件人電話,
                    $sender_address,'友和YOHO', //寄件人地址,寄件人姓名,
                    $goods['brand_name'],
                    $goods['goods_id'],
                    $goods['qty'],
                    $goods['goods_sn'],
                    $p_name,// 商品
                    number_format($goods['goods_weight'],2) * $goods['qty'],
                    $courier
                );
            }
        }
        /* Fill worksheet from values in array */
        try {
            $objPHPExcel->getActiveSheet()->fromArray($export, null, 'A2');
            foreach($export as $k => $v) {
                //$i = $k + 2;
                //$objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, (string)$order_sn_list[$k],\PHPExcel_Cell_DataType::TYPE_STRING);
            }
        } catch (\PHPExcel_Exception $e) {
        }

       header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.‌​sheet; charset=utf-8');
       header('Content-Disposition: attachment; filename=' . $newFile);
       header('Cache-Control: max-age=0');
       $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, $fileType);
       $objWriter->save('php://output');
       exit;
    }

    public function export_wilson_excel()
    {
        global $db, $ecs;

        ini_set('memory_limit', '256M');
        define('PCLZIP_TEMPORARY_DIR', ROOT_PATH . 'temp/');
        require_once ROOT_PATH . 'includes/excel/PHPExcel.php';
        require_once ROOT_PATH . 'includes/excel/PHPExcel/IOFactory.php';
        \PHPExcel_Settings::setZipClass(\PHPExcel_Settings::PCLZIP);

        $logistics = $_REQUEST['logistics'];
        if(empty($logistics)) {
            $logistics = $db->getOne("SELECT logistics FROM ". $ecs->table('mass_delivery')." WHERE mass_key = '".$_REQUEST['mass_key']."' LIMIT 1");
        }

        if ($logistics == 'wilson') {
            $courier = 'wilson';
        } else {
            $courier = '其他';
        }

        $order_sn = $this->get_order_sn();
        $order_sn_list = array_column($order_sn, 'order_sn');
        
        $export = array();
        $mass_key = $_REQUEST['mass_key'];

        $newFile = 'yoho_export_to_wilson_' . date('YmdHis') . '.xlsx';
        $objPHPExcel = new \PHPExcel();
        $fileType = 'Excel2007';
        /* Excel title */
        $title_list = ['ID', 'Order No', 'Ship Date', 'Delivered Date', 'Recipient Phone', 'Recipient Address', 'Recipient Name', 'Sender Phone', 'Sender Address', 'Sender Name', 'Brand', 'SKU ID' , 'Qty' , 'Barcode' , 'Goods Name', 'Weight' ,'Courier'];
        foreach (range('A', 'Q') as $char_key => $column) {
            $objPHPExcel->getActiveSheet()->SetCellValue($column . '1', $title_list[$char_key]);
        }
        foreach ($order_sn_list as $order_sn)
        {
            $order = order_info(0, $order_sn);
            // $weight_price = order_weight_price($order['order_id']);
            $sql = "SELECT c.cat_name, dg.send_number ,dg.send_number as qty, g.goods_name, g.goods_sn, g.goods_id, g.goods_weight, b.brand_name FROM " . $ecs->table('delivery_goods') ." as dg ".
                " LEFT JOIN ". $ecs->table('goods') ." as g ON dg.goods_id = g.goods_id ".
                " LEFT JOIN ". $ecs->table('category') ." as c ON c.cat_id = g.cat_id ".
                " LEFT JOIN ". $ecs->table('brand') ." as b ON g.brand_id = b.brand_id ".
                " WHERE delivery_id = (SELECT delivery_id FROM ".$ecs->table('mass_delivery')." WHERE order_id = ".$order['order_id']." AND mass_key = '".$mass_key."' ) ";

            $goods_info = $db->getAll($sql);
            $p_name = '';
            foreach ($goods_info as $goods) {
                $p_name = $goods['goods_name'] ;
                $tel = !empty($order['tel'])? $order['tel'] : $order['mobile'];
                $tel = explode('-', $tel);
                $tel = preg_replace('/\s(?=)/', '', $tel[count($tel) - 1]);
                $send_date = local_date('d/m/Y');
                $receive_date = local_date('d/m/Y', local_strtotime("+1 day"));
                // This array is base on TQB excel example:

                if (empty($goods['goods_weight'])) {
                    $goods['goods_weight'] = 0;
                }

                $export[] = array(
                    $_REQUEST['mass_key'],$order_sn, // 客戶管理編號, order_sn
                    $send_date, $receive_date,  //預定出貨日,預定收件日,
                    $tel,$order['address'], // 收件人電話, 收件人地址,
                    $order['consignee'],'85253356996', // 收件人姓名, 寄件人電話,
                    $sender_address,'友和YOHO', //寄件人地址,寄件人姓名,
                    $goods['brand_name'],
                    $goods['goods_id'],
                    $goods['qty'],
                    $goods['goods_sn'],
                    $p_name,// 商品
                    number_format($goods['goods_weight'],2) * $goods['qty'],
                    $courier
                );
            }
        }
        /* Fill worksheet from values in array */
        try {
            $objPHPExcel->getActiveSheet()->fromArray($export, null, 'A2');
            foreach($export as $k => $v) {
                //$i = $k + 2;
                //$objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, (string)$order_sn_list[$k],\PHPExcel_Cell_DataType::TYPE_STRING);
            }
        } catch (\PHPExcel_Exception $e) {
        }

        header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.‌​sheet; charset=utf-8');
        header('Content-Disposition: attachment; filename=' . $newFile);
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, $fileType);
        $objWriter->save('php://output');
        exit;
    }

    public function export_excel()
    {
        global $db, $ecs;

        ini_set('memory_limit', '256M');
        define('PCLZIP_TEMPORARY_DIR', ROOT_PATH . 'temp/');
        require_once ROOT_PATH . 'includes/excel/PHPExcel.php';
        require_once ROOT_PATH . 'includes/excel/PHPExcel/IOFactory.php';
        \PHPExcel_Settings::setZipClass(\PHPExcel_Settings::PCLZIP);

        $logistics = $_REQUEST['logistics'];
        if(empty($logistics)) {
            $logistics = $db->getOne("SELECT logistics FROM ". $ecs->table('mass_delivery')." WHERE mass_key = '".$_REQUEST['mass_key']."' LIMIT 1");
        }

        $order_sn = $this->get_order_sn();
        $order_sn_list = array_column($order_sn, 'order_sn');
        $export = array();
        $mass_key = $_REQUEST['mass_key'];

        $is_fls_order_delivery = $this->isFlsOrderDelivery($mass_key,false);

        $erpController = new ErpController();
        $main_warehouse_id = $erpController->getMainWarehouseId();
        $main_warehouse_code = $erpController->getWarehouseCode($main_warehouse_id);

        if ($is_fls_order_delivery) {
            $sender_address = "Unit A-D, 11/F Goodman Kwai Chung Logistics Centre, 585-609 Castle Peak Road, Kwai Chung";
            $sender_address_1 = "Unit A-D, 11/F Goodman Kwai Chung Logistics Centre";
            $sender_address_2 = "585-609 Castle Peak Road";
            $sender_area = "新界";
            $sender_area_en = "N.T.";
            $sender_district = "葵青荃灣";
            $sender_district_en = "Tsuen Wan, Kwai Tsing";
            $sender_location = "葵涌";
            $sender_location_en = "Kwai Chung";
            $sender_contact = "FLS";
            $sender_contact_en = "FLS";
            $sender_phone = "21274660";
        } else {
            if ($main_warehouse_id == 1) {
                $sender_address = "6A Union Building, 112 How Ming Street, Kwun Tong, Kowloon";
                $sender_address_1 = "6A Union Building";
                $sender_address_2 = "112 How Ming Street";
                $sender_area = "九龍";
                $sender_area_en = "Kowloon";
                $sender_district = "東九";
                $sender_district_en = "Kowloon East";
                $sender_location = "觀塘";
                $sender_location_en = "Kwun Tong";
                $sender_contact = "友和YOHO";
                $sender_contact_en = "YOHO Hong Kong";
                $sender_phone = "53356996";
            } else if ($main_warehouse_code == 'YOHO_WH_KC') { // 西九1號
                $sender_address = "8/F, Toppy Tower, 45-51 Kwok Shui Rd, Kwai Chung, NT";
                $sender_address_1 = "8/F, Toppy Tower";
                $sender_address_2 = "45-51 Kwok Shui Rd";
                $sender_area = "新界";
                $sender_area_en = "N.T.";
                $sender_district = "葵青荃灣";
                $sender_district_en = "Tsuen Wan, Kwai Tsing";
                $sender_location = "葵涌";
                $sender_location_en = "Kwai Chung";
                $sender_contact = "友和YOHO";
                $sender_contact_en = "YOHO Hong Kong";
                $sender_phone = "53356996";
            } else { // 西九1號
                $sender_address = "8/F, Toppy Tower, 45-51 Kwok Shui Rd, Kwai Chung, NT";
                $sender_address_1 = "8/F, Toppy Tower";
                $sender_address_2 = "45-51 Kwok Shui Rd";
                $sender_area = "新界";
                $sender_area_en = "N.T.";
                $sender_district = "葵青荃灣";
                $sender_district_en = "Tsuen Wan, Kwai Tsing";
                $sender_location = "葵涌";
                $sender_location_en = "Kwai Chung";
                $sender_contact = "友和YOHO";
                $sender_contact_en = "YOHO Hong Kong";
                $sender_phone = "53356996";
            }
        }

        if($logistics == 'sfplus') {
            $extra = $_REQUEST['extra'];
            $extra_detail = json_decode(stripslashes($extra),true);
            if(file_exists("../data/mass_delivery/sfplus_order_template_plain.xlsx"))
            $template = "../data/mass_delivery/sfplus_order_template_plain.xlsx";

            $newFile = 'yoho_export_to_sfplus_' . date('YmdHis') . '.xlsx';

            $fileType =  \PHPExcel_IOFactory::identify($template);
            $objReader = \PHPExcel_IOFactory::createReader($fileType);
            $objPHPExcel = $objReader->load($template);

            foreach ($order_sn_list as $order_sn)
            {
                $order = order_info(0, $order_sn);
                // $weight_price = order_weight_price($order['order_id']);
                $sql = "SELECT c.cat_name, SUM(dg.send_number) as qty  FROM " . $ecs->table('delivery_goods') ." as dg ".
                    " LEFT JOIN ". $ecs->table('goods') ." as g ON dg.goods_id = g.goods_id ".
                    " LEFT JOIN ". $ecs->table('category') ." as c ON c.cat_id = g.cat_id ".
                    " WHERE delivery_id = (SELECT delivery_id FROM ".$ecs->table('mass_delivery')." WHERE order_id = ".$order['order_id']." AND mass_key = '$mass_key' LIMIT 1) AND g.is_insurance = 0 GROUP BY c.cat_name";
                $goods_info = $db->getAll($sql);
                $p_name = '';
                foreach ($goods_info as $goods) {
                    $p_name .= $goods['cat_name']." x ".$goods['qty']."\n";
                }

                $tel = !empty($order['tel']) ? $order['tel'] : $order['mobile'];
                $tel = explode('-', $tel);
                $tel = preg_replace('/\s(?=)/', '', $tel[count($tel) - 1]);
                $send_date = $this->sfplus_date_time();
                $receive_date = $this->sfplus_date_time('receive', $extra_detail[$order_sn]['location'], $extra_detail[$order_sn]['is_business']);

                // This array is base on SF+ excel example:
                $export[] = array(
                    count($export) + 1, // 序
                    '即收即送', '',     // 產品, 折扣碼
                    $sender_address, // 寄件地址
                    $sender_area, $sender_district, $sender_location, //寄件地區, 寄件區域, 寄件範圍
                    $sender_contact, $sender_phone, // 寄件聯絡人, 寄件人電話
                    $send_date['date'], $send_date['time'], //寄件日期, 寄件時間
                    $order['consignee'],$tel,'', // 收件聯絡人, 收件人電話, 收件人備用電話
                    $receive_date['date'], $receive_date['time'], //收件日期, 收件時間
                    '到家',$extra_detail[$order_sn]['area'], $extra_detail[$order_sn]['district'], $extra_detail[$order_sn]['location'], '', $order['address'], //收件方式, 收件地區, 收件區域, 收件範圍, 首選智能櫃編號, 首選收件地址
                    '','',$extra_detail[$order_sn]['box_type'], $extra_detail[$order_sn]['weight'], $extra_detail[$order_sn]['box_no'], $order_sn.'-'.$mass_key,'', // 次選智能櫃編號, 次選收件地址, 箱的大小, 貨物的重量, 數量, 客戶參考編號, 商品編號
                    $p_name,$extra_detail[$order_sn]['price'], '', // 貨物名稱, 貨物價值, 商品出庫數量
                    '否','','','',$extra_detail[$order_sn]['remark'] // 付款方式, 代收運費, 代收貨款, 代收總費用, 備註
                );
                $sql = "UPDATE " . $ecs->table('mass_delivery') . " SET process = ".MK_EXPORT." WHERE mass_key = '" . $_REQUEST['mass_key'] . "' AND order_id = ". $order['order_id'];
                $db->query($sql);
            }
                  
            $objPHPExcel->setActiveSheetIndex()->fromArray($export,null,'A2');
            foreach($export as $k => $v) {
                $i = $k + 2;
                $objPHPExcel->getActiveSheet()->getStyle('J'.$i)
    ->getNumberFormat()
    ->setFormatCode('dd/mm/yyyy');
                    $objPHPExcel->getActiveSheet()->getStyle('O'.$i)
        ->getNumberFormat()
        ->setFormatCode('dd/mm/yyyy');
    
                //$objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$i, (string)$send_date,\PHPExcel_Cell_DataType::TYPE_STRING);
                //$objPHPExcel->getActiveSheet()->setCellValueExplicit('P'.$i, (string)$receive_date,\PHPExcel_Cell_DataType::TYPE_STRING);
            }

        } elseif ($logistics == 'ztopickup' || $logistics == 'zto') {
            if(file_exists("../data/mass_delivery/zto_order_template_plain.xlsx"))
            $template = "../data/mass_delivery/zto_order_template_plain.xlsx";
            $newFile = 'yoho_export_to_zto_' . date('YmdHis') . '.xlsx';

            $fileType =  \PHPExcel_IOFactory::identify($template);
            $objReader = \PHPExcel_IOFactory::createReader($fileType);
            $objPHPExcel = $objReader->load($template);

            $i = 3;
            foreach ($order_sn_list as $order_sn)
            {
                $order = order_info(0, $order_sn);
                $order_arresss = $db->getRow("SELECT * FROM ".$ecs->table('order_address')." WHERE order_id = ".$order['order_id']." LIMIT 1");
                $typeCode = unserialize($order_arresss['type_code']);
                $locker = $typeCode[0];
                $sql = "SELECT c.cat_name, dg.send_number as qty, dg.goods_price, g.goods_weight, b.brand_name FROM " . $ecs->table('delivery_goods') ." as dg ".
                    " LEFT JOIN ". $ecs->table('goods') ." as g ON dg.goods_id = g.goods_id ".
                    " LEFT JOIN ". $ecs->table('category') ." as c ON c.cat_id = g.cat_id ".
                    " LEFT JOIN ". $ecs->table('brand') ." as b ON b.brand_id = g.brand_id ".
                    " WHERE delivery_id = (SELECT delivery_id FROM ".$ecs->table('mass_delivery')." WHERE order_id = ".$order['order_id']." AND mass_key = '$mass_key' LIMIT 1) AND g.is_insurance = 0 ";
                $goods_info = $db->getAll($sql);
                $p_name = '';
                
                foreach ($goods_info as $goods) {
                    $p_name = $goods['cat_name'];
                    $p_brand = $goods['brand_name'];
                    $p_qty = $goods['qty'];
                    if ($goods['goods_price'] != 0){
                        $p_price = $goods['goods_price'];
                    } else {
                        $p_price = 0;
                    }
                    if ($goods['goods_weight'] != 0){
                        $p_weight = $goods['goods_weight'];
                    } else {
                        $p_weight = 0.5;
                    }

                    $tel = !empty($order['tel']) ? $order['tel'] : $order['mobile'];
                    $tel = explode('-', $tel);
                    $tel = preg_replace('/\s(?=)/', '', $tel[count($tel) - 1]);
                    $send_date = local_date('dmY');
                    $receive_date = local_date('dmY', local_strtotime("+1 day"));
                    
                    unset($export);
                    //if ($logistics == 'ztopickup'){
                    if ($order_arresss['area_type'] == 2){
                        $export[] = array(
                            $order_sn.'-'.$mass_key, // 訂單序號
                            '1039上門收件(需額外加收$30取件費)', $sender_contact, $sender_address, $sender_phone, $sender_phone, // 寄件網點, 寄件連絡人, 寄件人地址, 寄件人手機號碼, 寄件人電話號碼
                            '自提服務', $locker, $order['consignee'], $tel, $tel, $order_arresss['address'],  // 派送/自提, 收件方式, 收件連絡人, 收件人手機號碼, 收件人聯繫號碼, 收件人詳細地址
                            $p_brand, $p_name, $p_price, 'HKD', $p_qty, '', $p_weight, // 商品品牌, 商品名稱, 商品單價, 商品幣種, 商品數量, 數量單位, 包裝後重量(KG)
                            '寄付', 'NO', 'NO', '' // 支付方式, 保險, 包裝費, 備註
                        );
                    } else {
                        $export[] = array(
                            $order_sn.'-'.$mass_key, // 訂單序號
                            '1039上門收件(需額外加收$30取件費)', $sender_contact, $sender_address, $sender_phone, $sender_phone, // 寄件網點, 寄件連絡人, 寄件人地址, 寄件人手機號碼, 寄件人電話號碼
                            '上門派送', '1002.快遞非工商區', $order['consignee'], $tel, $tel, $order_arresss['address'],  // 派送/自提, 收件方式, 收件連絡人, 收件人手機號碼, 收件人聯繫號碼, 收件人詳細地址
                            $p_brand, $p_name, $p_price, 'HKD', $p_qty, '', $p_weight, // 商品品牌, 商品名稱, 商品單價, 商品幣種, 商品數量, 數量單位, 包裝後重量(KG)
                            '寄付', 'NO', 'NO', '' // 支付方式, 保險, 包裝費, 備註
                        );
                    }

                    $sql = "UPDATE " . $ecs->table('mass_delivery') . " SET process = ".MK_EXPORT." WHERE mass_key = '" . $_REQUEST['mass_key'] . "' AND order_id = ". $order['order_id'];
                    $db->query($sql);
                    $objPHPExcel->setActiveSheetIndex()->fromArray($export,null,'A'.strval($i));
                    $i++;
                }
            }
            
        } elseif ($logistics == 'dpex') {
            if(file_exists("../data/mass_delivery/dpex_order_template_plain.xlsx"))
            $template = "../data/mass_delivery/dpex_order_template_plain.xlsx";
            $newFile = 'yoho_export_to_dpex_' . date('YmdHis') . '.xlsx';

            $fileType =  \PHPExcel_IOFactory::identify($template);
            $objReader = \PHPExcel_IOFactory::createReader($fileType);
            $objPHPExcel = $objReader->load($template);

            $i = 2;
            foreach ($order_sn_list as $order_sn){
                $order = order_info(0, $order_sn);
                $order_arresss = $db->getRow("SELECT * FROM ".$ecs->table('order_address')." WHERE order_id = ".$order['order_id']." LIMIT 1");
                
                $sql = "SELECT IF(cl.cat_name IS NULL OR cl.cat_name = '', c.cat_name, cl.cat_name) AS cat_name, dg.send_number as qty, dg.goods_price , g.goods_weight FROM " . $ecs->table('delivery_goods') ." as dg ".
                    " LEFT JOIN ". $ecs->table('goods') ." as g ON dg.goods_id = g.goods_id ".
                    " LEFT JOIN ". $ecs->table('category') ." as c ON c.cat_id = g.cat_id ".
                    " LEFT JOIN (SELECT cat_id, cat_name FROM ". $ecs->table('category_lang') ." WHERE lang = 'en_us') as cl ON cl.cat_id = c.cat_id ".
                    " WHERE delivery_id = (SELECT delivery_id FROM ".$ecs->table('mass_delivery')." WHERE order_id = ".$order['order_id']." AND mass_key = '$mass_key' LIMIT 1) AND g.is_insurance = 0 ORDER BY dg.rec_id ASC";
                $goods_info = $db->getAll($sql);
                $good_list = array();
                $good_price = 0;
                $good_weight = 0;

                foreach ($goods_info as $goods) {
                    $p_name = (!empty($goods['cat_name'])?$goods['cat_name']:"Item");
                    $p_qty = (int)$goods['qty'];

                    if ($goods['goods_price'] > 0){
                        $good_price += $goods['goods_price']*$p_qty;
                    }
                    
                    if ($goods['goods_weight'] > 0){
                        $good_weight += $goods['goods_weight']*$p_qty;
                    } else {
                        $good_weight += 0.5*$p_qty;
                    }

                    if (isset($good_list[$p_name])){
                        $good_list[$p_name] += $p_qty;
                    } else {
                        $good_list[$p_name] = $p_qty;
                    }
                    //array_push($good_list, array("name" => (!empty($p_name)?$p_name:"Item"), "qty" => $p_qty));
                }
                $str_good_list = "";
                $g_i = 0;
                foreach ($good_list as $index => $item){
                    if ($g_i != 0){
                        $str_good_list .= ", ";
                    }
                     $str_good_list .= $index." x".$item;
                     $g_i++;
                }
                $tel = !empty($order['tel']) ? $order['tel'] : $order['mobile'];
                $tel = explode('-', $tel);
                $tel = preg_replace('/\s(?=)/', '', $tel[count($tel) - 1]);
                $send_date = local_date('dmY');
                $receive_date = local_date('dmY', local_strtotime("+1 day"));
                $zip_code = ($order_arresss['country'] != 3409 && $order_arresss['country'] != 3410)?$order_arresss['zipcode']:'';
                $country_code = get_country_code_by_region($order_arresss['country']);
                
                unset($export);
                $export[] = array(
                    $sender_contact_en, $sender_address_1, $sender_address_2, '', $sender_location_en, '', '.', 'HK', '-', //寄件人, 寄件地址, 寄件所在地
                    $sender_phone, '', //寄件人電話, 寄件人電郵
                    $order['consignee'], $order_arresss['address'], '', '', $order_arresss['locality'], $order_arresss['administrative_area'], $zip_code, strtoupper($country_code), $order['consignee'], $tel, '', //收件人, 收件人地址, 收件所在地, 收件地區, ZIP code, 收件國家, 收件聯絡人, 收件人電話, 收件人電郵
                    'ECS', '1', $str_good_list, $good_weight, $good_price, 'HKD',  //service type, 件數(整箱), 貨物list,  重量, 價值, 貨幣
                    $order_sn.'-'.$mass_key, '', 'HK', 'Gift', '', 'DDP', '.', '' //友和ref no.
                );

                $sql = "UPDATE " . $ecs->table('mass_delivery') . " SET process = ".MK_EXPORT." WHERE mass_key = '" . $_REQUEST['mass_key'] . "' AND order_id = ". $order['order_id'];
                $db->query($sql);
                $objPHPExcel->setActiveSheetIndex()->fromArray($export,null,'A'.strval($i));
                $i++;
            }
            
        } elseif ($logistics == 'fedex') {
            if(file_exists("../data/mass_delivery/fedex_order_template_plain.xlsx"))
            $template = "../data/mass_delivery/fedex_order_template_plain.xlsx";
            $newFile = 'yoho_export_to_fedex_' . date('YmdHis') . '.xlsx';

            $fileType =  \PHPExcel_IOFactory::identify($template);
            $objReader = \PHPExcel_IOFactory::createReader($fileType);
            $objPHPExcel = $objReader->load($template);

            $i = 2;
            foreach ($order_sn_list as $order_sn){
                $order = order_info(0, $order_sn);
                $order_arresss = $db->getRow("SELECT * FROM ".$ecs->table('order_address')." WHERE order_id = ".$order['order_id']." LIMIT 1");
                
                $sql = "SELECT IF(cl.cat_name IS NULL OR cl.cat_name = '', c.cat_name, cl.cat_name) AS cat_name, dg.send_number as qty, dg.goods_price , g.goods_weight FROM " . $ecs->table('delivery_goods') ." as dg ".
                    " LEFT JOIN ". $ecs->table('goods') ." as g ON dg.goods_id = g.goods_id ".
                    " LEFT JOIN ". $ecs->table('category') ." as c ON c.cat_id = g.cat_id ".
                    " LEFT JOIN (SELECT cat_id, cat_name FROM ". $ecs->table('category_lang') ." WHERE lang = 'en_us') as cl ON cl.cat_id = c.cat_id ".
                    " WHERE delivery_id = (SELECT delivery_id FROM ".$ecs->table('mass_delivery')." WHERE order_id = ".$order['order_id']." AND mass_key = '$mass_key' LIMIT 1) AND g.is_insurance = 0 ORDER BY dg.rec_id ASC";
                $goods_info = $db->getAll($sql);
                $good_list = array();
                $good_price = 0;
                $good_weight = 0;

                foreach ($goods_info as $goods) {
                    $p_name = (!empty($goods['cat_name'])?$goods['cat_name']:"Item");
                    $p_qty = (int)$goods['qty'];

                    if ($goods['goods_price'] > 0){
                        $good_price += $goods['goods_price']*$p_qty;
                    }
                    
                    if ($goods['goods_weight'] > 0){
                        $good_weight += $goods['goods_weight']*$p_qty;
                    } else {
                        $good_weight += 0.5*$p_qty;
                    }

                    if (isset($good_list[$p_name])){
                        $good_list[$p_name] += $p_qty;
                    } else {
                        $good_list[$p_name] = $p_qty;
                    }
                    //array_push($good_list, array("name" => (!empty($p_name)?$p_name:"Item"), "qty" => $p_qty));
                }
                $str_good_list = "";
                $g_i = 0;
                foreach ($good_list as $index => $item){
                    if ($g_i != 0){
                        $str_good_list .= ", ";
                    }
                     $str_good_list .= $index." x".$item;
                     $g_i++;
                }
                $tel = !empty($order['tel']) ? $order['tel'] : $order['mobile'];
                $tel = explode('-', $tel);
                $tel = preg_replace('/\s(?=)/', '', $tel[count($tel) - 1]);
                $send_date = local_date('dmY');
                $receive_date = local_date('dmY', local_strtotime("+1 day"));
                $zip_code = ($order_arresss['country'] != 3409 && $order_arresss['country'] != 3410)?$order_arresss['zipcode']:'';
                $country_code = get_country_code_by_region($order_arresss['country']);
                
                unset($export);
                $export[] = array(
                    $sender_contact_en, $sender_address_1, $sender_address_2, '', $sender_location_en, '', '.', 'HK', '-', //寄件人, 寄件地址, 寄件所在地
                    $sender_phone, '', //寄件人電話, 寄件人電郵
                    $order['consignee'], $order_arresss['address'], '', '', $order_arresss['locality'], $order_arresss['administrative_area'], $zip_code, strtoupper($country_code), $order['consignee'], $tel, '', //收件人, 收件人地址, 收件所在地, 收件地區, ZIP code, 收件國家, 收件聯絡人, 收件人電話, 收件人電郵
                    'ECS', '1', $str_good_list, $good_weight, $good_price, 'HKD',  //service type, 件數(整箱), 貨物list,  重量, 價值, 貨幣
                    $order_sn.'-'.$mass_key, '', 'HK', 'Gift', '', 'DDP', '.', '' //友和ref no.
                );

                $sql = "UPDATE " . $ecs->table('mass_delivery') . " SET process = ".MK_EXPORT." WHERE mass_key = '" . $_REQUEST['mass_key'] . "' AND order_id = ". $order['order_id'];
                $db->query($sql);
                $objPHPExcel->setActiveSheetIndex()->fromArray($export,null,'A'.strval($i));
                $i++;
            }
            
        } elseif ($logistics == '4px') {
            if(file_exists("../data/mass_delivery/4px_order_template_plain.xlsx"))
            $template = "../data/mass_delivery/4px_order_template_plain.xlsx";
            $newFile = 'yoho_export_to_4px_' . date('YmdHis') . '.xlsx';

            $fileType =  \PHPExcel_IOFactory::identify($template);
            $objReader = \PHPExcel_IOFactory::createReader($fileType);
            $objPHPExcel = $objReader->load($template);

            $order_count = 1;
            $i = 2;

            foreach ($order_sn_list as $order_sn){
                $order = order_info(0, $order_sn);
                $order_arresss = $db->getRow("SELECT * FROM ".$ecs->table('order_address')." WHERE order_id = ".$order['order_id']." LIMIT 1");
                
                $sql = "SELECT IF(cl.cat_name IS NULL OR cl.cat_name = '', c.cat_name, cl.cat_name) AS cat_name, dg.send_number as qty, dg.goods_price, g.goods_id, g.goods_weight, IF(gl.goods_name IS NULL OR gl.goods_name = '', g.goods_name, gl.goods_name) AS goods_name, IF(bl.brand_name IS NULL OR bl.brand_name = '', b.brand_name, bl.brand_name) AS brand_name FROM " . $ecs->table('delivery_goods') ." as dg ".
                    " LEFT JOIN ". $ecs->table('goods') ." as g ON dg.goods_id = g.goods_id ".
                    " LEFT JOIN ". $ecs->table('category') ." as c ON c.cat_id = g.cat_id ".
                    " LEFT JOIN ". $ecs->table('brand') ." as b ON b.brand_id = g.brand_id ".
                    " LEFT JOIN (SELECT goods_id, goods_name FROM ". $ecs->table('goods_lang') ." WHERE lang = 'zh_cn') as gl ON gl.goods_id = g.goods_id ".
                    " LEFT JOIN (SELECT cat_id, cat_name FROM ". $ecs->table('category_lang') ." WHERE lang = 'zh_cn') as cl ON cl.cat_id = c.cat_id ".
                    " LEFT JOIN (SELECT brand_id, brand_name FROM ". $ecs->table('brand_lang') ." WHERE lang = 'zh_cn') as bl ON bl.brand_id = b.brand_id ".
                    " WHERE delivery_id = (SELECT delivery_id FROM ".$ecs->table('mass_delivery')." WHERE order_id = ".$order['order_id']." AND mass_key = '$mass_key' LIMIT 1) AND g.is_insurance = 0 ORDER BY dg.rec_id ASC";
                
                $goods_info = $db->getAll($sql);

                $tel = !empty($order['tel']) ? $order['tel'] : $order['mobile'];
                $tel = explode('-', $tel);
                $tel = preg_replace('/\s(?=)/', '', $tel[count($tel) - 1]);
                $send_date = local_date('dmY');
                $receive_date = local_date('dmY', local_strtotime("+1 day"));
                $zip_code = ($order_arresss['country'] != 3409 && $order_arresss['country'] != 3410)?$order_arresss['zipcode']:'';
                $country_code = get_country_code_by_region($order_arresss['country']);
                if ($order['id_doc'] != ""){
                    $order['id_doc'] = strval(decrypt_str($order['id_doc']));
                }

                unset($export);
                $export[] = array(
                    $order_count, '香港仓库', '电商清关模式-快捷服务', $order_sn,  //序号, 国外接货仓库, 转运产品, 国外物流号
                    $order['consignee'], 'China', $tel, $order_arresss['administrative_area'], $order_arresss['locality'], '', $order_arresss['address'], $zip_code, strval($order['id_doc']), //收货人, 收货国家, 手机号, 省, 市, 区/县, 详细地址, 邮编, 身份证号码
                    'China', '否', '否', '否', '是', '是', 'DDP', '' //目的地国家, 是否合箱, 是否清点, 是否加固, 是否保险, 是否顺丰派送, 税费模式, 备注, 商品
                );
                unset($arr_p);
                $arr_p = [];

                foreach ($goods_info as $goods) {
                    $p_cat = (!empty($goods['cat_name'])?$goods['cat_name']:"");
                    $p_name = (!empty($goods['goods_name'])?$goods['goods_name']:$p_cat);
                    $p_brand = (!empty($goods['brand_name'])?$goods['brand_name']:"");
                    $p_qty = (int)$goods['qty'];
                    $p_price = ($goods['goods_price'] == 0)?'1':$goods['goods_price'];

                    if (!isset($arr_p[$goods['goods_id']])){
                        $arr_p[$goods['goods_id']] = array("cat" => $p_cat, 'name' => $p_name, 'qty' => $p_qty, 'brand' => $p_brand, 'price' => $p_price);
                    } else {
                        $arr_p[$goods['goods_id']]['qty'] += $p_qty;
                    }
                }

                foreach ($arr_p as $p){
                    array_push($export[0], $p['cat'], $p['name'], "", "", $p['brand'], $p['price'], $p['qty'], "中国");
                }

                $sql = "UPDATE " . $ecs->table('mass_delivery') . " SET process = ".MK_EXPORT." WHERE mass_key = '" . $_REQUEST['mass_key'] . "' AND order_id = ". $order['order_id'];
                $db->query($sql);
                $objPHPExcel->setActiveSheetIndex()->fromArray($export,null,'A'.strval($i));
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$i, $export[0][12],\PHPExcel_Cell_DataType::TYPE_STRING);
                $i++;
                $order_count++;
            }
            
        } elseif ($logistics == 'tqb') {
            if(file_exists("../data/mass_delivery/tqb_order_template_plain.xls"))
            $template = "../data/mass_delivery/tqb_order_template_plain.xls";

            $newFile = 'yoho_export_to_tqb_' . date('YmdHis') . '.xls';

            $fileType =  \PHPExcel_IOFactory::identify($template);
            $objReader = \PHPExcel_IOFactory::createReader($fileType);
            $objPHPExcel = $objReader->load($template);

            foreach ($order_sn_list as $order_sn)
            {
                $order = order_info(0, $order_sn);
                // $weight_price = order_weight_price($order['order_id']);
                $sql = "SELECT c.cat_name, SUM(dg.send_number) as qty  FROM " . $ecs->table('delivery_goods') ." as dg ".
                    " LEFT JOIN ". $ecs->table('goods') ." as g ON dg.goods_id = g.goods_id ".
                    " LEFT JOIN ". $ecs->table('category') ." as c ON c.cat_id = g.cat_id ".
                    " WHERE delivery_id = (SELECT delivery_id FROM ".$ecs->table('mass_delivery')." WHERE order_id = ".$order['order_id']." AND mass_key = '$mass_key' LIMIT 1) AND g.is_insurance = 0 GROUP BY c.cat_name";
                $goods_info = $db->getAll($sql);
                $p_name = '';
                foreach ($goods_info as $goods) {
                    $p_name .= $goods['cat_name']." x ".$goods['qty']."\n";
                }

                $tel = !empty($order['tel']) ? $order['tel'] : $order['mobile'];
                $tel = explode('-', $tel);
                $tel = preg_replace('/\s(?=)/', '', $tel[count($tel) - 1]);
                $send_date = local_date('dmY');
                $receive_date = local_date('dmY', local_strtotime("+1 day"));
                // This array is base on TQB excel example:
                $export[] = array(
                    $mass_key, // 客戶管理編號
                    $send_date, $receive_date,'0000',  //預定出貨日,預定收件日, 希望收件時段
                    $order['consignee'], $tel, $order['province_name'], $order['address'], // 收件人姓名, 收件人電話, 收件人地區, 收件人詳細地址(須包涵街道)
                    $sender_contact,$sender_phone,$sender_area,$sender_address, //  寄件人姓名, 寄件人電話, 寄件人地區, 寄件人詳細地址(須包涵街道)
                    '01', //托運單種類
                    '','','','','','','','','','',//自取店設定, 自取店編號, 貨款金額, 同點派送, 同點派送編號, 回件, 回件聯絡人, 回件聯絡電話, 回件地區, 回件詳細地址(須包涵街道)
                    '0', $p_name, $order_sn//溫度帶設定, 商品名, 注意事項
                );
                $sql = "UPDATE " . $ecs->table('mass_delivery') . " SET process = ".MK_EXPORT." WHERE mass_key = '" . $_REQUEST['mass_key'] . "' AND order_id = ". $order['order_id'];
                $db->query($sql);
            }
            $objPHPExcel->setActiveSheetIndex()->fromArray($export,null,'A2');
            foreach($export as $k => $v) {
                $i = $k + 2;
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, (string)'0000',\PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$i, (string)'01',\PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, (string)$send_date,\PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, (string)$receive_date,\PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('Z'.$i, (string)$order_sn_list[$k],\PHPExcel_Cell_DataType::TYPE_STRING);
            }
        } elseif ($logistics == 'sf') {
            if(file_exists("../data/mass_delivery/sf_order_template_plain.xls"))
            $template = "../data/mass_delivery/sf_order_template_plain.xls";

            $newFile = 'yoho_export_to_sf_' . date('YmdHis') . '.xls';

            $fileType =  \PHPExcel_IOFactory::identify($template);
            $objReader = \PHPExcel_IOFactory::createReader($fileType);
            $objPHPExcel = $objReader->load($template);

            foreach ($order_sn_list as $order_sn)
            {
                $order = order_info(0, $order_sn);
                // $weight_price = order_weight_price($order['order_id']);
                $sql = "SELECT c.cat_name, SUM(dg.send_number) as qty, AVG(dg.goods_price) as price FROM " . $ecs->table('delivery_goods') ." as dg ".
                    " LEFT JOIN ". $ecs->table('goods') ." as g ON dg.goods_id = g.goods_id ".
                    " LEFT JOIN ". $ecs->table('category') ." as c ON c.cat_id = g.cat_id ".
                    " WHERE delivery_id = (SELECT delivery_id FROM ".$ecs->table('mass_delivery')." WHERE order_id = ".$order['order_id']." AND mass_key = '$mass_key' LIMIT 1) AND g.is_insurance = 0 GROUP BY c.cat_name";
                $goods_info = $db->getAll($sql);
                $p_name = '';
                // foreach ($goods_info as $goods) {
                //     $p_name .= $goods['cat_name']." x ".$goods['qty']."\n";
                // }

                $tel = !empty($order['tel']) ? $order['tel'] : $order['mobile'];
                $tel = explode('-', $tel);
                $tel = preg_replace('/\s(?=)/', '', $tel[count($tel) - 1]);
                $send_date = local_date('dmY');
                $receive_date = local_date('Y-m-d', local_strtotime("+1 day"));
                foreach ($goods_info as $goods) {
                    // This array is base on SF excel example:
                    $export[] = array(
                        $order_sn.'-'.$mass_key, // 訂單序號
                        $order['consignee'], $tel,'','香港 '.$order['address'],'', // 收件方信息: 連絡人, 聯繫電話, 手機號碼, 收件詳細地址, 買家昵稱
                        '',$goods['cat_name'],$goods['qty'],'','',$goods['price'], // 商品資訊: 商品編碼	商品標題	商品數量	銷售屬性	訂單金額	商品單價
                        '','','','','','','','','', // 附加服務: 代收金額, 保價金額, 紙箱費, 個性包裝費, 簽單返還, 自取件, 易碎件, 保鮮服務, 電子驗收
                        '標準快遞','寄付月結','1','','','如收方偏遠/非工商請上報客服入寄方月結','','','','8526255294','',$receive_date,'12:01-18:00','8526255294',$order_sn,'' //其他: 業務類型, 運費付款方式, 件數, 包裹重量, 買家備註, 賣家備註, 幣種, 國際頭程運單號, 支付工具, 支付號, 是否定時派送, 定時派送時日期, 定時派送時間, 自定義1, 自定義2, 自定義3
                    );
                }
                $sql = "UPDATE " . $ecs->table('mass_delivery') . " SET process = ".MK_EXPORT." WHERE mass_key = '" . $_REQUEST['mass_key'] . "' AND order_id = ". $order['order_id'];
                $db->query($sql);
            }
            $objPHPExcel->setActiveSheetIndex()->fromArray($export,null,'A3');
        } elseif ($logistics == 'other') {

            $newFile = 'yoho_export_to_other_' . date('YmdHis') . '.xlsx';
            $objPHPExcel = new \PHPExcel();
            $fileType = 'Excel2007';
            /* Excel title */
            $title_list = ['物流單號', '管理編號', '訂單號', '出貨日期', '收件日期', '收件人電話', '收件人地址', '收件人姓名', '寄件人電話', '寄件人地址', '寄件人姓名', '商品'];
            foreach (range('A', 'L') as $char_key => $column) {
                $objPHPExcel->getActiveSheet()->SetCellValue($column . '1', $title_list[$char_key]);
            }
            foreach ($order_sn_list as $order_sn)
            {
                $order = order_info(0, $order_sn);
                // $weight_price = order_weight_price($order['order_id']);
                $sql = "SELECT c.cat_name, SUM(dg.send_number) as qty  FROM " . $ecs->table('delivery_goods') ." as dg ".
                    " LEFT JOIN ". $ecs->table('goods') ." as g ON dg.goods_id = g.goods_id ".
                    " LEFT JOIN ". $ecs->table('category') ." as c ON c.cat_id = g.cat_id ".
                    " WHERE delivery_id = (SELECT delivery_id FROM ".$ecs->table('mass_delivery')." WHERE order_id = ".$order['order_id']." AND mass_key = '$mass_key' LIMIT 1) AND g.is_insurance = 0 GROUP BY c.cat_name";
                $goods_info = $db->getAll($sql);
                $p_name = '';
                foreach ($goods_info as $goods) {
                    $p_name .= $goods['cat_name']." x ".$goods['qty']."\n";
                }

                $tel = !empty($order['tel']) ? $order['tel'] : $order['mobile'];
                $tel = explode('-', $tel);
                $tel = preg_replace('/\s(?=)/', '', $tel[count($tel) - 1]);
                $send_date = local_date('d/m/Y');
                $receive_date = local_date('d/m/Y', local_strtotime("+1 day"));
                // This array is base on TQB excel example:
                $export[] = array(
                    '', $_REQUEST['mass_key'],$order_sn, // 客戶管理編號, order_sn
                    $send_date, $receive_date,  //預定出貨日,預定收件日,
                    $tel,$order['address'], // 收件人電話, 收件人地址,
                    $order['consignee'],'85253356996', // 收件人姓名, 寄件人電話,
                    $sender_address,'友和YOHO', //寄件人地址,寄件人姓名,
                    $p_name// 商品
                );
                $sql = "UPDATE " . $ecs->table('mass_delivery') . " SET process = ".MK_EXPORT." WHERE mass_key = '" . $_REQUEST['mass_key'] . "' AND order_id = ". $order['order_id'];
                $db->query($sql);
            }
            /* Fill worksheet from values in array */
            try {
                $objPHPExcel->getActiveSheet()->fromArray($export, null, 'A2');
                foreach($export as $k => $v) {
                    $i = $k + 2;
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, (string)$order_sn_list[$k],\PHPExcel_Cell_DataType::TYPE_STRING);
                }
            } catch (\PHPExcel_Exception $e) {
            }

        } elseif ($logistics == 'lockerlife') {

            if(file_exists("../data/mass_delivery/lockerlife_order_template_plain.xlsx"))
            $template = "../data/mass_delivery/lockerlife_order_template_plain.xlsx";

            $newFile = 'yoho_export_to_lockerlife_' . date('YmdHis') . '.xlsx';

            $fileType =  \PHPExcel_IOFactory::identify($template);
            $objReader = \PHPExcel_IOFactory::createReader($fileType);
            $objPHPExcel = $objReader->load($template);
            /* Excel title */
            $title_list = ['customer_name', 'customer_phone', 'customer_email', 'reference', 'Locker Code', 'LockerLife', 'locker_id', 'description', 'dimension_length', 'dimension_width', 'dimension_height', 'dimension_unit', 'weight_value', 'weight_unit', 'sms_language', 'box_size', 'items'];
            foreach (range('A', 'Q') as $char_key => $column) {
                $objPHPExcel->getActiveSheet()->SetCellValue($column . '1', $title_list[$char_key]);
            }
            $start = 2;
            foreach ($order_sn_list as $order_sn)
            {
                $order = order_info(0, $order_sn);
                $order_arresss = $db->getRow("SELECT * FROM ".$ecs->table('order_address')." WHERE order_id = ".$order['order_id']." LIMIT 1");
                $typeCode = unserialize($order_arresss['type_code']);
                $locker = $typeCode[0];
                // $weight_price = order_weight_price($order['order_id']);
                $sql = "SELECT c.cat_name, SUM(dg.send_number) as qty, g.goods_length, g.goods_width, g.goods_height, c.default_length, c.default_width, c.default_height, g.goods_weight ".
                    " FROM " . $ecs->table('delivery_goods') ." as dg ".
                    " LEFT JOIN ". $ecs->table('goods') ." as g ON dg.goods_id = g.goods_id ".
                    " LEFT JOIN ". $ecs->table('category') ." as c ON c.cat_id = g.cat_id ".
                    " WHERE delivery_id = (SELECT delivery_id FROM ".$ecs->table('mass_delivery')." WHERE order_id = ".$order['order_id']." AND mass_key = '$mass_key' LIMIT 1) AND g.is_insurance = 0 GROUP BY c.cat_name";
                $goods_info = $db->getAll($sql);
                $p_name = '';
                $total_width  = 0;
                $total_height = 0;
                $total_length = 0;
                $total_weight = 0;
                foreach ($goods_info as $goods) {
                    $p_name .= $goods['cat_name']." x ".$goods['qty']."\n";
                    $width  = (empty(floatval($goods['goods_width'])))  ? $goods['default_width']  : $goods['goods_width'];
                    $height = (empty(floatval($goods['goods_height']))) ? $goods['default_height'] : $goods['goods_height'];
                    $length = (empty(floatval($goods['goods_length']))) ? $goods['default_length'] : $goods['goods_length'];
                    $total_width  += ($width * $goods['qty']);
                    $total_height += ($height * $goods['qty']);
                    $total_length += ($length * $goods['qty']);
                    $total_weight = floatval($goods['goods_weight']) * $goods['qty'];
                }

                $tel = !empty($order['tel']) ? $order['tel'] : $order['mobile'];
                $tel = explode('-', $tel);
                $tel = preg_replace('/\s(?=)/', '', $tel[count($tel) - 1]);
                $send_date = local_date('d/m/Y');
                $receive_date = local_date('d/m/Y', local_strtotime("+1 day"));
                // This array is base on excel example:
                $export = array(
                    $order['consignee'],'852'.$tel, $order['email'],'', $locker,
                    '','', $order_sn.'-'. $_REQUEST['mass_key'],$total_length, $total_width, $total_height,
                    'cm', $total_weight, 'kg', 'zh_HK', 'M',
                    $p_name// 商品
                );
                $i = 0;
                $objWorksheet2 = $objPHPExcel->getSheet(1);
                foreach ($objWorksheet2->getRowIterator() as $key => $row) {
                    $cellIterator = $row->getCellIterator();
                    $cellIterator->setIterateOnlyExistingCells(false);
                    if ($key >= 4) {
                        foreach (range('M', 'O') as $char_key => $column) {
                            $tmp[$key][$column] = $objPHPExcel->getSheet(1)->getCell($column . $key)->getValue();
                        }
                    }
                }
                foreach($tmp as $k => $v) {
                    $locker_data[$v['M']]['address'] = $v['N'];
                    $locker_data[$v['M']]['locker_id'] = $v['O'];
                }
                foreach (range('A', 'Q') as $char_key => $column) {
                    if($column != "F" && $column != "G" ) $objPHPExcel->getActiveSheet()->SetCellValue($column . $start, $export[$i]);
                    elseif($column == "F") {
                        $objPHPExcel->getActiveSheet()->SetCellValue($column . $start, $locker_data[$locker]['address']);
                    }elseif($column == "G") {
                        $objPHPExcel->getActiveSheet()->SetCellValue($column . $start, $locker_data[$locker]['locker_id']);
                    }
                    $i++;
                }
                $start ++;
                $objPHPExcel->getActiveSheet()->removeRow($start, 101);
                $sql = "UPDATE " . $ecs->table('mass_delivery') . " SET process = ".MK_EXPORT." WHERE mass_key = '" . $_REQUEST['mass_key'] . "' AND order_id = ". $order['order_id'];
                $db->query($sql);
            }

        }elseif ($logistics == 'pakpobox') {

            if(file_exists("../data/mass_delivery/pakpobox_order_template_plain.xlsx"))
            $template = "../data/mass_delivery/pakpobox_order_template_plain.xlsx";

            $newFile = 'yoho_export_to_pakpobox_' . date('YmdHis') . '.xlsx';

            $fileType =  \PHPExcel_IOFactory::identify($template);
            $objReader = \PHPExcel_IOFactory::createReader($fileType);
            $objPHPExcel = $objReader->load($template);

            $start = 2;
            foreach ($order_sn_list as $order_sn)
            {
                $order = order_info(0, $order_sn);
                $delivery_order_sn = 'YOHO'.substr($order_sn, -5).rand(10000,99999);
                $order_arresss = $db->getRow("SELECT * FROM ".$ecs->table('order_address')." WHERE order_id = ".$order['order_id']." LIMIT 1");
                $typeCode = unserialize($order_arresss['type_code']);
                $locker = $typeCode[0];
                // $weight_price = order_weight_price($order['order_id']);
                $sql = "SELECT c.cat_name, SUM(dg.send_number) as qty, g.goods_length, g.goods_width, g.goods_height, c.default_length, c.default_width, c.default_height, g.goods_weight ".
                    " FROM " . $ecs->table('delivery_goods') ." as dg ".
                    " LEFT JOIN ". $ecs->table('goods') ." as g ON dg.goods_id = g.goods_id ".
                    " LEFT JOIN ". $ecs->table('category') ." as c ON c.cat_id = g.cat_id ".
                    " WHERE delivery_id = (SELECT delivery_id FROM ".$ecs->table('mass_delivery')." WHERE order_id = ".$order['order_id']." AND mass_key = '$mass_key' LIMIT 1) AND g.is_insurance = 0 GROUP BY c.cat_name";
                $goods_info = $db->getAll($sql);
                $p_name = '';
                $total_width  = 0;
                $total_height = 0;
                $total_length = 0;
                $total_weight = 0;
                foreach ($goods_info as $goods) {
                    $p_name .= $goods['cat_name']." x ".$goods['qty']."\n";
                    $width  = (empty(floatval($goods['goods_width'])))  ? $goods['default_width']  : $goods['goods_width'];
                    $height = (empty(floatval($goods['goods_height']))) ? $goods['default_height'] : $goods['goods_height'];
                    $length = (empty(floatval($goods['goods_length']))) ? $goods['default_length'] : $goods['goods_length'];
                    $total_width  += ($width * $goods['qty']);
                    $total_height += ($height * $goods['qty']);
                    $total_length += ($length * $goods['qty']);
                    $total_weight = floatval($goods['goods_weight']) * $goods['qty'];
                }

                $tel = !empty($order['tel']) ? $order['tel'] : $order['mobile'];
                $tel = explode('-', $tel);
                $tel = preg_replace('/\s(?=)/', '', $tel[count($tel) - 1]);
                $send_date = local_date('d/m/Y');
                $receive_date = local_date('d/m/Y', local_strtotime("+1 day"));
                // This array is base on excel example:
                $export = array(
                    $delivery_order_sn, $tel, $locker, $order['consignee'], $order['email'], $order_sn.'-'. $_REQUEST['mass_key'],
                    $total_weight, 'Yohohongkong', '', '0'
                );
                $i = 0;
                foreach (range('A', 'J') as $char_key => $column) {
                    $objPHPExcel->getActiveSheet()->SetCellValue($column . $start, $export[$i]);
                    $i++;
                }
                $start ++;
                $objPHPExcel->getActiveSheet()->removeRow($start, 101);
                $sql = "UPDATE " . $ecs->table('mass_delivery') . " SET process = ".MK_EXPORT." WHERE mass_key = '" . $_REQUEST['mass_key'] . "' AND order_id = ". $order['order_id'];
                $db->query($sql);
            }

        }
        header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.‌​sheet; charset=utf-8');
        header('Content-Disposition: attachment; filename=' . $newFile);
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, $fileType);
        $objWriter->save('php://output');
    }

    public function import_excel()
    {
        global $db,$ecs;

        ini_set('memory_limit', '256M');
        define( 'PCLZIP_TEMPORARY_DIR', ROOT_PATH . 'temp/');

        require_once ROOT_PATH . 'includes/excel/PHPExcel.php';
        require_once ROOT_PATH . 'includes/excel/PHPExcel/IOFactory.php';
        \PHPExcel_Settings::setZipClass(\PHPExcel_Settings::PCLZIP);
        $fileName     = $_FILES["import_excel"]["tmp_name"];
        if ($_REQUEST['logistics'] != 'sf') {
            $fileType = \PHPExcel_IOFactory::identify($fileName);
        } else {
            $fileType = 'CSV';
        }
        $objReader    = \PHPExcel_IOFactory::createReader($fileType);
        $objPHPExcel  = $objReader->load($fileName);
        $worksheet    = $objPHPExcel->getSheet();
        $order_sn     = $this->get_order_sn();
        $searchArray  = array_column($order_sn, 'order_sn');
        $logistics_id = isset($_REQUEST['logistics_id']) ? intval($_REQUEST['logistics_id']) : 0;

        $foundInCells = array();
        $excel_reverse = array_reverse($worksheet->toArray());
        $i = 0;
        foreach ($searchArray as $sn ){
            foreach ($excel_reverse as $excel_key => $row) {
                if(in_array($sn.'-'. $_REQUEST['mass_key'], $row) || in_array($sn, $row)) {
                    $foundInCells[$sn] = $row;
                    unset($excel_reverse[$excel_key]);
                    break;
                }
            }
        }

        if(count($foundInCells) > 0) {
            $logistics = $_REQUEST['logistics'];
            if(empty($logistics)) {
                $logistics = $db->getOne("SELECT logistics FROM ". $ecs->table('mass_delivery')." WHERE mass_key = '".$_REQUEST['mass_key']."' LIMIT 1");
            }
            if($logistics) {
                $l = $logistics;
                if($l == 'tqb') {
                    $logistics_id = 1;
                    $invoice_key = 16;
                } elseif ($l == 'sfplus') {
                    $logistics_id = 3;
                    $invoice_key = 0;
                } elseif ($l == 'sf') {
                    $logistics_id = 2;
                    $invoice_key = 4;
                } elseif ($l == 'other') {
                    $invoice_key = 0;
                } elseif ($l == 'lockerlife') {
                    $logistics_id = $db->getOne("SELECT logistics_id FROM ". $ecs->table('logistics')." WHERE logistics_code = 'lockerlife' LIMIT 1");
                    $invoice_key = 1;
                } elseif ($l == 'pakpobox') {
                    $logistics_id = $db->getOne("SELECT logistics_id FROM ". $ecs->table('logistics')." WHERE logistics_code = 'pakpobox' LIMIT 1");
                    $invoice_key = 0;
                } elseif($l == 'ztopickup') {
                    $logistics_id = $db->getOne("SELECT logistics_id FROM ". $ecs->table('logistics')." WHERE logistics_code = 'ztopickup' LIMIT 1");
                    $invoice_key = 0;
                } elseif($l == 'dpex') {
                    $logistics_id = $db->getOne("SELECT logistics_id FROM ". $ecs->table('logistics')." WHERE logistics_code = 'dpex' LIMIT 1");
                    $invoice_key = 35;
                } elseif($l == 'fedex') {
                    $logistics_id = $db->getOne("SELECT logistics_id FROM ". $ecs->table('logistics')." WHERE logistics_code = 'fedex' LIMIT 1");
                    $invoice_key = 35;
                } elseif($l == '4px') {
                    $logistics_id = $db->getOne("SELECT logistics_id FROM ". $ecs->table('logistics')." WHERE logistics_code = '4px' LIMIT 1");
                    $invoice_key = 0;
                }
                $logistics = new Model\Logistics($logistics_id);
                $logistics = $logistics->data;
            } else {
                $logistics = new Model\Logistics($logistics_id);
                $logistics = $logistics->data;
                $cell = reset($foundInCells);
                $invoice_key = null;
                foreach ($cell as $key => $value) {
                    $length           = strlen($value);
                    $first_word       = (strlen($value) > 2)?substr($value, 0, 2):'';
                    if(!empty($logistics['invoice_template']) && $length == $logistics['template_length'] && $first_word == $logistics['first_word']) {
                        $invoice_key = $key;
                        break;
                    }
                }

                if(!isset($invoice_key)) {
                    // We try to find in enabled logistics list:
                    $logisticsController = new LogisticsController();
                    $logistics_list = $logisticsController->get_logistics_list();

                    foreach ($logistics_list as $logistics) {

                        foreach ($cell as $key => $value) {
                            $length           = strlen($value);
                            $first_word       = (strlen($value) > 2)?substr($value, 0, 2):'';
                            if(!empty($logistics['invoice_template']) && $length == $logistics['template_length'] && $first_word == $logistics['first_word']) {
                                $logistics_id = $logistics['logistics_id'];
                                $invoice_key = $key;
                                break 2;
                            }
                        }
                    }
                }
            }
        }
        $invoice_key = isset($invoice_key) ? $invoice_key : 0;
        if(empty($foundInCells)) {
            return json_encode(['error' => '沒法找到訂單', 'status' => 0]);
        }
        $finish_list = [];

        foreach($foundInCells as $order_sn => $cell){
            $invoice_no = $cell[$invoice_key];
            if($_REQUEST['logistics'] == 'sf') {
                $invoice_no = str_replace("\"","",$invoice_no);
                $invoice_no = str_replace("=","",$invoice_no);
            }
            $order = order_info(null, $order_sn);
            $delivery_id  = $db->getOne("SELECT delivery_id FROM " . $ecs->table('mass_delivery') . " WHERE mass_key = '" . $_REQUEST['mass_key'] . "' AND order_id = ".$order['order_id']." LIMIT 1");
            $sql_1 = "UPDATE ".$ecs->table('delivery_order')." SET invoice_no = '".$invoice_no."', logistics_id = ".$logistics_id." WHERE delivery_id = ".$delivery_id;
            $db->query($sql_1);
            $sql = "UPDATE " . $ecs->table('mass_delivery') . " SET process = ".MK_IMPORT." WHERE mass_key = '" . $_REQUEST['mass_key'] . "' AND order_id = ".$order['order_id'];
            $db->query($sql);
            $action_note = '系統批量出貨:'.$logistics['logistics_name'].":".$invoice_no;
            order_action($order['order_sn'], $order['order_status'], $order['shipping_status'], $order['pay_status'], $action_note, null, 1);
            $finish_list[$order_sn]['invoice_no'] = $invoice_no;
            $finish_list[$order_sn]['order_id']   = $order['order_id'];
        }
        return json_encode(['finish_list' => $finish_list, 'status' => 1, 'foundInCells'=>$foundInCells]);
    }

    public function ajax_respond()
    {
        global $db,$ecs,$_LANG;
        $order = order_info(0, $_REQUEST['order_sn']);
        $mass_key = $_REQUEST['mass_key'];

        $sql = "SELECT g.goods_name, g.goods_weight, dg.send_number as qty  FROM " . $ecs->table('delivery_goods') ." as dg ".
                " LEFT JOIN ". $ecs->table('goods') ." as g ON dg.goods_id = g.goods_id ".
                " WHERE delivery_id = (SELECT delivery_id FROM ".$ecs->table('mass_delivery')." WHERE order_id = ".$order['order_id']." AND mass_key = '$mass_key' LIMIT 1)";
        $goods_list = $db->getAll($sql);

        $sql = "SELECT c.cat_name, SUM(dg.send_number) as qty  FROM " . $ecs->table('delivery_goods') ." as dg ".
                " LEFT JOIN ". $ecs->table('goods') ." as g ON dg.goods_id = g.goods_id ".
                " LEFT JOIN ". $ecs->table('category') ." as c ON c.cat_id = g.cat_id ".
                " WHERE delivery_id = (SELECT delivery_id FROM ".$ecs->table('mass_delivery')." WHERE order_id = ".$order['order_id']." AND mass_key = '$mass_key' LIMIT 1) GROUP BY c.cat_name";
        $goods_info = $db->getAll($sql);

        $sql = "SELECT oa.country AS country, oa.sign_building AS building, oa.administrative_area AS area FROM " . $ecs->table('order_info') ." AS o ".
                "LEFT JOIN ". $ecs->table('order_address') ." AS oa ON oa.order_address_id = o.address_id ".
                "WHERE o.order_id = " . $order['order_id'] . " ";
        $order_address_key = $db->getRow($sql);

        if (!empty($order_address_key) && $order_address_key['country'] != "" && $order_address_key['building'] != "" && $order_address_key['area'] != ""){
            $sql = "SELECT oa.verify AS verify FROM " . $ecs->table('order_address') ." AS oa ".
                    "WHERE oa.country = '".$order_address_key['country']."' ".
                    "AND oa.sign_building = '".mysql_real_escape_string($order_address_key['building'])."' ".
                    "AND oa.administrative_area = '".$order_address_key['area']."' ".
                    "AND oa.dest_type = '".$order['address_cat']."' ".
                    "AND oa.lift = '".$order['address_lift']."' ".
                    "AND oa.verify = '1' ";
            $address_history = $db->getAll($sql);
            if (!empty($address_history)){
                $verify_history = 1;
            } else {
                $verify_history = 0;
            }
        } else {
            $verify_history = 0;
        }

        $order['verify_history'] = $verify_history;

        $p_name = '';
        foreach ($goods_info as $goods) {
            $p_name .= $goods['cat_name']." x ".$goods['qty']."\n";
        }

        if (isset($order['hk_area']) && $order['hk_area'] > 0) {
            $area = $db->getOne("SELECT area_chi_name FROM ". $ecs->table('hk_areas') ." WHERE area_id = ".$order['hk_area']." LIMIT 1");
            $order_area = $this->find_location($area);
        }

        $address_types = $this->get_address_types();
        $types_groups = $this->get_address_types_groups();
        $address_types_groups = array();
        if (sizeof($types_groups) > 0){
            foreach ($types_groups as $group){
                $address_types_groups[$group['cat']] = $_LANG['address_type_'.$group['cat']];
            }
        }
        $address_types_groups = $address_types_groups;

        $result = array(
            'order_sn' => $_REQUEST['order_sn'],
            'address' => $order['address'],
            'goods' => $goods_list,
            'remarks' => $order['postscript'],
            'order' => $order,
            'order_area' => $order_area,
            'address_types' => $address_types,
            'address_types_groups' => $address_types_groups,
            'default_remarks' => $p_name
        );
        if (isset($order_area) && $order_area['location'] > 0) {
            $result['order_area'] = $order_area;
        }
        return json_encode($result);
    }

    public function find_location($area)
    {
        // $level_1 = self::AREA;
        // $level_2 = self::DISTRICT;
        $level_3 = self::LOCATION;
        $area_1  = 0;
        $area_2  = 0;
        $area_3  = 0;

        $result = null;
        foreach($level_3 as $key_1 => $a1) {
            foreach($a1 as $key_2 => $a2) {
                foreach($a2 as $key_3 => $location) {
                    if ($area == $location) {

                        $area_1 = $key_1;
                        $area_2 = $key_2;
                        $area_3 = $key_3;
                        $result = [
                            'area'     => $area_1,
                            'district' => $area_2,
                            'location' => $area_3
                        ];
                        break 3; // We break all foreach in this result.
                    }
                }
            }
        }
        return $result;
    }

    public function delivery_send_mail($order, $delivery_id = null ,$force = false)
    {
        if(empty($order)) return false;

        global $db, $ecs, $_LANG, $_CFG, $smarty,$userController;

        $cfg = $_CFG['send_ship_email'];
        if ($cfg == '1')
        {
            if (!empty($delivery_id)) {
                $sql = "SELECT * FROM " . $ecs->table('delivery_order') . "where delivery_id = '".$delivery_id."' ";
                $delivery_info = $db->getRow($sql);
            }

            // If the goods is not shipped by yoho, need to send email to customer (logistics 8 代理送貨 9 司機送貨 or wilson)
            $wholesaleUserIds = $userController->getWholesaleUserIds();

            $wilson_logistics_id = $this->getWilsonLogisticsId();

            if ($force == true || 
                $delivery_info['is_fls'] || 
                ( !empty($delivery_info) and ($delivery_info['logistics_id'] == '8' || $delivery_info['logistics_id'] == '9' ||  $delivery_info['logistics_id'] == $wilson_logistics_id ))  
            ) {
                $need_to_send_email = true;
            } else {
                $need_to_send_email = false;
            }

            if ($need_to_send_email) {
                if (can_send_mail($order['email'], true) && !in_array($order['order_type'], [OrderController::DB_ORDER_TYPE_WHOLESALE, OrderController::DB_ORDER_TYPE_SALESAGENT]))
                {
                    //Get invoice no Carrier
                    $sql = "SELECT l.logistics_url, l.logistics_code, l.logistics_id, IFNULL(la.logistics_desc, l.logistics_desc) AS logistics_desc, IFNULL(la.logistics_name, l.logistics_name) AS logistics_name, do.invoice_no as trackingNumber, do.delivery_id FROM ".$ecs->table("delivery_order")." as do ".
                    "LEFT JOIN ".$ecs->table("logistics")." as l ON do.logistics_id = l.logistics_id ".
                    "LEFT JOIN ".$ecs->table("logistics_lang")." as la ON l.logistics_id = la.logistics_id AND la.lang = '".$GLOBALS['_CFG']['lang']."' ".
                    "WHERE do.order_id = '".$order['order_id']."' ";

                    $delivery_orders = $db->getAll($sql);

                    $all_pickup = true;
                    foreach ($delivery_orders as $deliver){
                        if ($deliver['logistics_id'] != 7){ // 7 -> logistics_id of store pickup
                            $all_pickup = false;
                            break;
                        }
                    }

                    $tpl = ($all_pickup ? get_mail_template('pickup_notice') : get_mail_template('deliver_notice'));

                    if ($all_pickup && !$tpl){
                        $tpl = get_mail_template('deliver_notice');
                    }

                    $pickup_desc = ($all_pickup ? get_pickup_desc(7) : "");

                    $smarty->assign('order', $order);
                    $smarty->assign('delivery_orders', $delivery_orders);
                    $smarty->assign('send_time', local_date($_CFG['time_format']));
                    $smarty->assign('shop_name', $_CFG['shop_name']);
                    $smarty->assign('send_date', local_date($_CFG['date_format']));
                    $smarty->assign('sent_date', local_date($_CFG['date_format']));
                    $smarty->assign('confirm_url', $ecs->url() . 'receive.php?id=' . $order['order_id'] . '&con=' . rawurlencode($order['consignee']));
                    $smarty->assign('review_link', $ecs->url() . 'review.php?order_id=' . $order['order_id']);
                    $smarty->assign('send_msg_url',$ecs->url() . 'user.php?act=message_list&order_id=' . $order['order_id']);
                    $smarty->assign('pickup_desc', $pickup_desc);
                    $content = $smarty->fetch('str:' . $tpl['template_content']);
                    if (!send_mail($order['consignee'], $order['email'], $tpl['template_subject'] . ' - ' . $order['order_sn'], $content, $tpl['is_html']))
                    {
                        $msg = $_LANG['send_mail_fail'];
                    }
                    else 
                    {
                        if (!empty($delivery_id)) {
                            $sql = "update ".$ecs->table('delivery_order')." set notified = 1 where delivery_id = '".$delivery_id."'";
                            $result = $db->query($sql);
                        }
                    }
                }

                // Send SMS if need
                if (isset($GLOBALS['_CFG']['sms_order_shipped']) && $GLOBALS['_CFG']['sms_order_shipped'] == '1' && $order['mobile'] != '')
                {
                    include_once('../includes/cls_sms.php');
                    $sms = new sms();
                    $sms->send($order['mobile'], sprintf($GLOBALS['_LANG']['order_shipped_sms'], $order['order_sn'],
                        local_date($GLOBALS['_LANG']['sms_time_format']), $GLOBALS['_CFG']['shop_name']), 0);
                }
            }
        }
    }

    public function mass_send_email($mass_key, $id_list = [])
    {

        global $db, $ecs;

        $sql = "SELECT order_id,delivery_id FROM ".$ecs->table('mass_delivery')." WHERE mass_key = '$mass_key' AND process IN(".MK_IMPORT.",".MK_MAIL.",".MK_FINISH.") ";
        if(!empty($id_list)) $sql .= " AND order_id ".db_create_in($id_list);

        $order_list = $db->getAll($sql);
        $sn_list = [];
        foreach ($order_list as $order_info) {
            $order = order_info($order_info['order_id']);
            $true = $this->delivery_send_mail($order,$order_info['delivery_id']);
            $sql = "UPDATE " . $ecs->table('mass_delivery') . " SET process = ".MK_MAIL." WHERE mass_key = '" . $mass_key . "' AND order_id = ".$order_info['order_id'];
            $db->query($sql);
            $sn_list[] = $order['order_sn'];
        }

        return ['status' => 1, 'sn_list' => $sn_list];
    }

    /** SF+ date handle */
    public function sfplus_date_time($type = 'send', $location = '馬灣', $is_business = 0) {

        $check_now_time = (intval(local_date('H')) < 11);
        if($check_now_time) { // If time no later than 11:00 a.m. + 1 day
            $date = local_strtotime("now");
        } else {
            $date = local_strtotime("+1 day");
        }

        if($type == 'send') {
            // Check today/next day is holiday, If holiday, get next work day.
            $is_work_day = !$this->is_holiday($date, ['saturday']);
            if(!$is_work_day) $date = $this->get_yoho_next_work_day($date, 1, ['saturday']);
            $date = $date + local_date('Z', $date);
            $date = intval(25569 + $date/86400);
            $time = '12:00 - 16:00';
        } else { // $type == 'receive'
            if($is_business) {
                $date += 86400;
            }
            // Check today/next day is holiday, If holiday, get next work day.
            $is_work_day = !$this->is_holiday($date, ['saturday']);
            if(!$is_work_day) $date = $this->get_yoho_next_work_day($date, 1, ['saturday']);

            switch ($location) {
                case '馬灣':
                    if(in_array(local_date("N", $date), [1, 3, 5])) {
                        $date += 86400;
                    } else {
                        $i = (local_date("N", $date) == 6) ? 3 : 2;
                        $date = local_strtotime("+$i day", $date); // Next 2, 4, 6
                    }
                    $time = '12:00 - 16:00';
                    break;
                case '愉景灣':
                    if(in_array(local_date("N", $date), [1, 3, 5])) {
                        $date += 86400;
                    } else {
                        $i = (local_date("N", $date) == 6) ? 3 : 2;
                        $date = local_strtotime("+$i day", $date); // Next 2, 4, 6
                    }
                    $time = '12:00 - 16:00';
                    break;
                case '東涌':
                    if(in_array(local_date("N", $date), [1, 3, 5])) {
                        $date += 86400;
                    } else {
                        $i = (local_date("N", $date) == 6) ? 3 : 2;
                        $date = local_strtotime("+$i day", $date); // Next 2, 4, 6
                    }
                    $time = '16:00 - 22:30';
                    break;
                default:
                    if($is_business) $time = '10:30 - 17:00';
                    else $time = '18:00 - 23:00';
                    break;
            }

           
            $date = $date + local_date('Z', $date);
            $date = intval(25569 + $date/86400);
            
        }

        return ['date' => $date, 'time' => $time];
    }

    public function massInitAction()
    {
        global $smarty, $_LANG;
        $arr  = $this->batch_create_delivery_order();
        $mass_key     = $arr['mass_key'];
        $error_list   = $arr['error_list'];
        $undo_list    = $arr['undo_list'];
        $finish_list  = $arr['finish_list'];
        $logistics    = $_REQUEST['logistics'];
        $is_fls       = (isset($_REQUEST['is_fls'])? $_REQUEST['is_fls'] : 0 );
        $logistics_name = $_LANG[$logistics];
        $area = $this::AREA;
        $district = $this::DISTRICT;
        $location = $this::LOCATION;
        $box = $this::BOX;
        $enable_addr_edit = 0;
        $smarty->assign('box', $box);
        $smarty->assign('area', $area);
        $smarty->assign('logistics', $logistics);
        $smarty->assign('logistics_name', $logistics_name);
        $smarty->assign('district', $district);
        $smarty->assign('location', $location);
        // $smarty->assign('order_sn', $res['all_sn']);
        if($logistics == 'storepicking') {
            $smarty->assign('process', MK_MAIL);
        } else if($logistics == 'wilson') {
            $smarty->assign('process', MK_FINISH);
        } else {
            $smarty->assign('process', MK_DELIVERY);
        }
        $smarty->assign('mass_key', $mass_key);
        $smarty->assign('error_list', $error_list);
        if (!empty($undo_list)) $smarty->assign('undo_list', $undo_list);
        $smarty->assign('full_page', 1);

        if ($logistics == 'wilson' && !empty($mass_key)) {
            $wilson_token = $this->getWilsonPrintToken($mass_key);
            $smarty->assign('wilson_token', $wilson_token);
        }

        if ($logistics == 'dpex' || $logistics == '4px' || $logistics == 'fedex'){
            foreach ($finish_list as $key => $order){
                $arr_order_addr_info = $this->get_order_addr_info($order['order_id']);
                $order_id_doc = "";
                if ( $logistics == '4px'){
                    $order_id_doc = $this->get_order_id_doc($order['order_id']);
                    if (!$order_id_doc){
                        $order_id_doc = "";
                    } else {
                        $controller = new YohoBaseController();
                        $order_id_doc = decrypt_str($order_id_doc);
                    }
                }
                if ($arr_order_addr_info && !empty($arr_order_addr_info)){
                    $arr_order_addr_info['id_doc'] = $order_id_doc;
                    $finish_list[$key]['addr_info'] = $arr_order_addr_info;
                }
            }
            $arr_lift_txt = array();
            $i_lift = 0;
            while (isset($_LANG['address_type_lift_type_'.$i_lift])){
                array_push($arr_lift_txt, $_LANG['address_type_lift_type_'.$i_lift]);
                $i_lift++;
            }
            $smarty->assign('arr_lift_txt', $arr_lift_txt);

            //if (isset($finish_list['addr_info']) && $finish_list['addr_info']['order_address_id'] > 0){
                $enable_addr_edit = 1;
            //}
        }

        $smarty->assign('order_sn_array', $finish_list);
        $smarty->assign('enable_addr_edit', $enable_addr_edit);
        
        assign_query_info();

        if($logistics == 'sfplus') { //TODO: Old order_mass_delivery
            $smarty->display('order_mass_delivery.htm');
        } else {
            $smarty->display('mass_delivery_step.htm');
        }
    }

    public function massStepAction()
    {
        global $smarty, $_LANG;
        $area = $this::AREA;
        $district = $this::DISTRICT;
        $location = $this::LOCATION;
        $box = $this::BOX;
        $res = $this->get_order_sn(true);
        $is_fls_order_delivery = $this->isFlsOrderDelivery($_REQUEST['mass_key']);
        $logistics = max(array_column($res, 'logistics'));
        $logistics_name = $_LANG[$logistics];
        if (!empty($_REQUEST['mass_key'])) {
            $fls_token = $this->getFlsPrintToken($_REQUEST['mass_key']);
            $smarty->assign('fls_token', $fls_token);
            $wilson_token = $this->getWilsonPrintToken($_REQUEST['mass_key']);
            $smarty->assign('wilson_token', $wilson_token);
        }
        
        $smarty->assign('logistics', $logistics);
        $smarty->assign('logistics_name', $logistics_name);
        $smarty->assign('ur_here', $logistics_name.'-批量出貨');
        $smarty->assign('box', $box);
        $smarty->assign('area', $area);
        $smarty->assign('district', $district);
        $smarty->assign('location', $location);
        $smarty->assign('process', max(array_column($res, 'process')));
        $smarty->assign('mass_key', $_REQUEST['mass_key']);
        $smarty->assign('order_sn_array', $res);
        $smarty->assign('full_page', 1);
        $smarty->assign('is_fls_order_delivery', $is_fls_order_delivery);

        assign_query_info();
        if($logistics == 'sfplus') { //TODO: Old order_mass_delivery
            $smarty->display('order_mass_delivery.htm');
        } else {
            $smarty->display('mass_delivery_step.htm');
        }
    }

    public function get_order_addr_info($order_id)
    {
        global $db, $ecs;
        $sql = "SELECT oa.order_address_id, oa.address, oa.locality, oa.administrative_area, oa.dest_type AS type_id, oadt.address_dest_type_name AS type_name, oa.lift, oa.verify ".
            " FROM ".$ecs->table('order_address')." as oa ".
            " LEFT JOIN ".$ecs->table('order_address_dest_type')." AS oadt ON oadt.address_dest_type_id = oa.dest_type ".
            " WHERE oa.order_id = '".$order_id."' ORDER BY oa.order_address_id LIMIT 1 ";
        $order_addr = $db->getRow($sql);
        if (!empty($order_addr)){
            return $order_addr;
        } else {
            return FALSE;
        }
    }

    public function get_order_id_doc($order_id)
    {
        global $db, $ecs;
        $sql = "SELECT o.id_doc ".
            " FROM ".$ecs->table('order_info')." as o ".
            " WHERE o.order_id = '".$order_id."'";
        $res = $db->getRow($sql);
        if (!empty($res['id_doc'])){
            return $res['id_doc'];
        } else {
            return FALSE;
        }
    }

    public function get_order_delivery_info($order_id)
    {
        global $db, $ecs;
        $do_sql = "SELECT d.invoice_no, d.delivery_id, l.logistics_name, d.update_time, d.shipping_id,d.is_fls FROM ".$ecs->table('delivery_order')." as d ".
          " LEFT JOIN ".$ecs->table('logistics')." AS l ON l.logistics_id = d.logistics_id ".
          " WHERE d.order_id = $order_id order by update_time ";
        return $delivery_list = $db->getAll($do_sql);
    }

    public function get_delivery_goods_info($delivery_id)
    {
        global $db, $ecs;
        //$goods_sql ="SELECT * ".
        //$goods_sql ="SELECT rec_id, delivery_id, goods_id, product_id, product_sn, goods_name, brand_name, goods_sn, is_real, extension_code, parent_id, send_number, goods_attr, goods_attr_id, goods_price, order_item_id, warehouse_idrec_id, delivery_id, goods_id, product_id, product_sn, goods_name, brand_name, goods_sn, is_real, extension_code, parent_id, send_number, goods_attr, goods_attr_id, goods_price, order_item_id, warehouse_id ".
        $goods_sql ="SELECT `rec_id`, `delivery_id`, `goods_id`, `product_id`, `product_sn`, `goods_name`, `brand_name`, `goods_sn`, `is_real`, `extension_code`, `parent_id`, SUM(`send_number`) AS send_number, `goods_attr`, `goods_attr_id`, `goods_price`, `order_item_id`, `warehouse_id`, `is_insurance`, `insurance_of` ".
                    "FROM " . $ecs->table('delivery_goods') . " " . 
                    "WHERE delivery_id = " . $delivery_id . " " .
                    "AND is_insurance = '0' " .
                    "AND insurance_of = '0' " .
                    "GROUP BY goods_sn";
        return $goods_list = $GLOBALS['db']->getAll($goods_sql);
    }

    public function local_pickup_confirm($delivery_id,$delivery_status,$operator,$order_id)
    {
        global $db, $ecs;

        $sql =  "update " . $ecs->table('delivery_order') .
                "set status = ".$delivery_status." WHERE delivery_id = " . $delivery_id;

        if ($db->query($sql)) {

            /* 查询订单信息 */
            $sql = "select order_id, delivery_sn, logistics_name from ".$ecs->table('delivery_order')." do left join  ".$ecs->table('logistics')." l on (do.logistics_id = l.logistics_id) where do.delivery_id = '".$delivery_id."' ";
            $delivery_info = $db->getRow($sql);
            $order_id = $delivery_info['order_id'];
            $order = order_info($order_id);
            $action_note = '['.$delivery_info['logistics_name'].']'.$delivery_info['delivery_sn'].':'.($delivery_status == DO_RECEIVED ? '已取貨' : '未取貨');

            order_action($order['order_sn'], $order['order_status'], $order['shipping_status'], $order['pay_status'], $action_note, $pickup_operator, 1);   
            
            // interal remark (to_buyer)
            if ($delivery_status == DO_RECEIVED) {  // picked up
                if (!empty($operator)) {
                    $order['to_buyer']      = $_POST['to_buyer'];
                    $sql = "select to_buyer from ".$ecs->table('order_info')." where order_id = '".$order_id."'";
                    $to_buyer_remark = $db->getOne($sql);
                    $to_buyer_remark .= "\n". $operator .' taken';
                    $sql =  "update " . $ecs->table('order_info') . "set to_buyer = '".$to_buyer_remark."' WHERE order_id = " . $order_id;
                    $db->query($sql);
                    order_log('other', $order, '');
                    $result['to_buyer_remark'] = $to_buyer_remark;
                }
            } else if ($delivery_status == DO_PACKED) {  // packed
                if (!empty($operator)) {
                    $order['to_buyer']      = $_POST['to_buyer'];
                    $sql = "select to_buyer from ".$ecs->table('order_info')." where order_id = '".$order_id."'";
                    $to_buyer_remark = $db->getOne($sql);
                    $to_buyer_remark .= "\n". $operator .' packed';

                    $sql =  "update " . $ecs->table('order_info') . "set to_buyer = '".$to_buyer_remark."' WHERE order_id = " . $order_id;
                    $db->query($sql);
                    order_log('other', $order, '');
                    $result['to_buyer_remark'] = $to_buyer_remark;

                    // send email to notify customer
                    $order = order_info($order_id);
                    $this->delivery_send_mail($order,$delivery_id,true);
                }
            }

            $result['success'] = 'true'; 
            return $result;
        } else {
            return false;
        }
    }

    public function local_pickup_notified($delivery_id,$notified,$operator,$order_id)
    {
        global $db, $ecs;

        $sql =  "update " . $ecs->table('delivery_order') .
                "set pickup_notified = ".$notified." WHERE delivery_id = " . $delivery_id;

        $db->query($sql);

        if ($notified) {

            /* 查询订单信息 */
            if ($notified == true) {
                $sql = "select order_id, delivery_sn, logistics_name from ".$ecs->table('delivery_order')." do left join  ".$ecs->table('logistics')." l on (do.logistics_id = l.logistics_id) where do.delivery_id = '".$delivery_id."' ";
                $delivery_info = $db->getRow($sql);
                $order_id = $delivery_info['order_id'];
                $order = order_info($order_id);
                $action_note = '['.$delivery_info['logistics_name'].']'.$delivery_info['delivery_sn'].': 已通知客人取貨';
    
                order_action($order['order_sn'], $order['order_status'], $order['shipping_status'], $order['pay_status'], $action_note, $operator, 1);   
            }

            if (!empty($operator)) {
                $order['to_buyer']      = $_POST['to_buyer'];
                $sql = "select to_buyer from ".$ecs->table('order_info')." where order_id = '".$order_id."'";
                $to_buyer_remark = $db->getOne($sql);
                $to_buyer_remark .= "\n". $operator .' called';
                $sql =  "update " . $ecs->table('order_info') . "set to_buyer = '".$to_buyer_remark."' WHERE order_id = " . $order_id;
                $db->query($sql);
                order_log('other', $order, '');
                $result['to_buyer_remark'] = $to_buyer_remark;
            }

            $result['success'] = 'true'; 
            return $result;
        } else {
            $result['success'] = 'true'; 
            return $result;
        }
    }

    public function wholesale_dn_confirm($delivery_id,$delivery_status,$operator)
    {
        global $db, $ecs;

        $sql =  "update " . $ecs->table('delivery_order') .
                "set status = ".$delivery_status." WHERE delivery_id = " . $delivery_id;

        if ($db->query($sql)) {

            /* 查询订单信息 */
            $sql = "select order_id, delivery_sn, logistics_name from ".$ecs->table('delivery_order')." do left join  ".$ecs->table('logistics')." l on (do.logistics_id = l.logistics_id) where do.delivery_id = '".$delivery_id."' ";
            $delivery_info = $db->getRow($sql);
            $order_id = $delivery_info['order_id'];
            $order = order_info($order_id);

            if ($delivery_status == DO_PACKED) {
                $action_note = '['.$delivery_info['logistics_name'].']'.$delivery_info['delivery_sn'].':'. '已pack貨,未取貨';
            } else if ($delivery_status == DO_RECEIVED) {
                $action_note = '['.$delivery_info['logistics_name'].']'.$delivery_info['delivery_sn'].':'. '已取貨';
            } else if ($delivery_status == DO_DELIVERED) {
                $action_note = '['.$delivery_info['logistics_name'].']'.$delivery_info['delivery_sn'].':'. '未pack貨,未取貨';
            }

            //$action_note = '['.$delivery_info['logistics_name'].']'.$delivery_info['delivery_sn'].':'.($delivery_status == DO_RECEIVED ? '已取貨' : '未取貨');

            order_action($order['order_sn'], $order['order_status'], $order['shipping_status'], $order['pay_status'], $action_note, $pickup_operator, 1);   
            
            // // interal remark (to_buyer)
            // if ($delivery_status == DO_RECEIVED) {  // picked up
            //     if (!empty($operator)) {
            //         $order['to_buyer']      = $_POST['to_buyer'];
            //         $sql = "select to_buyer from ".$ecs->table('order_info')." where order_id = '".$order_id."'";
            //         $to_buyer_remark = $db->getOne($sql);
            //         $to_buyer_remark .= "\n". $operator .' taken';
            //         $sql =  "update " . $ecs->table('order_info') . "set to_buyer = '".$to_buyer_remark."' WHERE order_id = " . $order_id;
            //         $db->query($sql);
            //         order_log('other', $order, '');
            //         $result['to_buyer_remark'] = $to_buyer_remark;
            //     }
            // } else if ($delivery_status == DO_PACKED) {  // packed
            //     if (!empty($operator)) {
            //         $order['to_buyer']      = $_POST['to_buyer'];
            //         $sql = "select to_buyer from ".$ecs->table('order_info')." where order_id = '".$order_id."'";
            //         $to_buyer_remark = $db->getOne($sql);
            //         $to_buyer_remark .= "\n". $operator .' packed';

            //         $sql =  "update " . $ecs->table('order_info') . "set to_buyer = '".$to_buyer_remark."' WHERE order_id = " . $order_id;
            //         $db->query($sql);
            //         order_log('other', $order, '');
            //         $result['to_buyer_remark'] = $to_buyer_remark;
            //     }
            // }

            $result['success'] = 'true'; 
            return $result;
        } else {
            return false;
        }
    }

    public function uploadEflocker()
    {
        global $db, $ecs, $_LANG;

        // Get order list by mass key
        $order_sn = $this->get_order_sn();
        $order_sn_list = array_column($order_sn, 'order_sn');
        $mass_key = $_REQUEST['mass_key'];

        // Get EFlocker list
        $addressCotnroller = new AddressController();
        $locker_list = $addressCotnroller->get_eflocker_list(0);

        // Set today date format : ISO 8601 date (added in PHP 5) e.g.: 2004-02-12T15:19:21+00:00
        $time = local_date("c");
        $order_id_list = [];
        $finish_list = [];
        $fail_list = [];

        // Hardcode set logistics id = eflocker
        $logistics_id = 11;
        $logistics = new Model\Logistics($logistics_id);
        $logistics = $logistics->data;
        $beta = (strpos($_SERVER['SERVER_NAME'], 'beta.yohohongkong.com') !== false);
        if($beta) {
            $url = 'http://ipl-order.sit.eflocker.com:4240/ws/SendOrderRequest?WSDL';
            $companyCode = 'HK';
            $secretKey = '1b0a7be2953e4c5632ea26581ee76516';
        } else {
            $url = "http://ipl-order.eflocker.com/ws/SendOrderRequest?WSDL";
            $companyCode = 'yoho';
            $secretKey = '98020415d62f1522311561bdaa855bc0'; //This Key is only can used by server2.yohongkong.com's IP
        }

        // Batch add eflocker order:
        foreach ($order_sn_list as $order_sn) {
            $sendOrderDto = [];
            $order = order_info(0, $order_sn);
            $order_list[$order_sn] = $order;
            // $weight_price = order_weight_price($order['order_id']);
            $sql = "SELECT c.cat_name, SUM(dg.send_number) as qty, AVG(dg.goods_price) as price FROM " . $ecs->table('delivery_goods') ." as dg ".
                    " LEFT JOIN ". $ecs->table('goods') ." as g ON dg.goods_id = g.goods_id ".
                    " LEFT JOIN ". $ecs->table('category') ." as c ON c.cat_id = g.cat_id ".
                    " WHERE delivery_id = (SELECT delivery_id FROM ".$ecs->table('mass_delivery')." WHERE order_id = ".$order['order_id']." AND mass_key = '$mass_key' LIMIT 1) GROUP BY c.cat_name";
            $goods_info = $db->getAll($sql);
            $p_name = implode(',', array_column($goods_info, 'cat_name'));
            $price_total = 0;
            $qty_total = 0;
            foreach ($goods_info as $goods) {
                $qty_total += $goods['qty'];
                $price_total = bcadd($price_total, $goods['price']);
            }
            //$locker_list
            $code_list = unserialize($order['type_code']);
            $locker_address = '';
            $locker = [];
            $code_1 = '';
            $code_2 = '';
            if ($code_list) {
                $locker = $locker_list[$code_list[0]];
                $code_1 = ($code_list[0]) ? $code_list[0] : '';
                $locker_address = $locker['twThrowaddress'];
                $code_2 = ($code_list[1]) ? $code_list[1] : '';
            }
            $tel = !empty($order['tel']) ? $order['tel'] : $order['mobile'];
            $tel = explode('-', $tel);
            $tel = preg_replace('/\s(?=)/', '', $tel[count($tel) - 1]);

            if (empty($order['consignee'])) {
                $order['consignee'] = "先生 / 小姐";
            }
            // init eflocker data:
            $sendOrderDto[] = array(
                'originalWaybill' => '',
                'product' => 1, // 1.收中派 2.中派
                'orderType' => 1, //1.寄付
                'settleType' => 1, //1.月結
                'clientOrderNo' => $order_sn.$mass_key, //客戶訂單流水號
                'sendTm' => local_date("c"),
                'placeFee' => (float)number_format(0, 0, '.', ''),
                'placeMoney' => (float)number_format(0, 0, '.', ''),
                'goodContent' => $p_name,
                'goodCount' => $qty_total,
                'goodPrice' => (float)number_format(0, 0, '.', ''),
                'totalPrice' => (float)number_format($price_total, 0, '.', ''),
                'totalWeight' => 3,
                'currencyType' => 1, //貨幣類型 1.HKD 2.RAM(暫不開放)
                'senderCompanyName' => 'yohoHongKong',
                'senderPhone' => '53356996',
                'senderName' => 'yohoHongKong',
                'senderNationCode' => '',
                'senderPovinceCode' => '',
                'senderDistrictCode' => '',
                'senderAddress' => '九龍觀塘巧明街112號友聯大廈6樓',
                'recipientsCompanyName' => $order['consignee'],
                'recipientsPhone' => $tel,
                'recipientsName' => $order['consignee'],
                'recipientsNationCode' => '',
                'recipientsProvinceCode' => '',
                'recipientsDistrictCode' => '',
                'recipientsAddress' => $locker_address,
                'recipientsEdinfoAddress' => $locker_address,
                'recipientsEdinfoCode' => $code_1,
                'recipientsSparedEdCode' => $code_2,
                'remark' => $order_sn
            );

            $orderRequestDto = array(
            'companyCode' => $companyCode,
            'secretKey' => $secretKey,
            'sendOrderList' => $sendOrderDto,
            'responseType' => 1,
            'issuedStatus' => 1,
            'isbackWaybill' => 1,
            'notifyUrl' => ''
            );
            try {
                $client = new \SoapClient($url, array('trace' => 1));
                $result = $client->sendOrder(array('orderRequestDto'=>$orderRequestDto));
            } catch (Exception $e) {
                echo $e -> getMessage();
            }
            $result = json_decode(json_encode($result), true);
            $a[] = $result;

            $eforder = isset($result['return']['data']) ? $result['return']['data'] : $result['return'] ;
            $clientOrderNo = $eforder['clientOrderNo'];
            $code = $eforder['code'];
            $desc = $eforder['desc'];
            $order_sn = str_replace($mass_key, "", $eforder['clientOrderNo']);
            //$order = $order_list[$order_sn];
            if($code == 200) { // 成功
                $invoice_no = $eforder['waybill'];
                $delivery_id  = $db->getOne("SELECT delivery_id FROM " . $ecs->table('mass_delivery') . " WHERE mass_key = '" . $mass_key . "' AND order_id = ".$order['order_id']." LIMIT 1");
                $sql_1 = "UPDATE ".$ecs->table('delivery_order')." SET invoice_no = '".$invoice_no."', logistics_id = ".$logistics_id." WHERE delivery_id = ".$delivery_id;
                $db->query($sql_1);
                $sql = "UPDATE " . $ecs->table('mass_delivery') . " SET process = ".MK_IMPORT." WHERE mass_key = '" .$mass_key . "' AND order_id = ".$order['order_id'];
                $db->query($sql);
                $action_note = '系統批量出貨:'.$logistics['logistics_name'].":".$invoice_no;
                order_action($order['order_sn'], $order['order_status'], $order['shipping_status'], $order['pay_status'], $action_note, null, 1);
                $finish_list[$order_sn]['invoice_no'] = $invoice_no;
                $finish_list[$order_sn]['order_id']   = $order['order_id'];
            } else { // Fail.
                $sql = "UPDATE " . $ecs->table('mass_delivery') . " SET process = ".MK_CANCEL." WHERE mass_key = '" .$mass_key . "' AND order_id = ".$order['order_id'];
                $db->query($sql);
                $action_note = '批量失敗: '.$desc;
                order_action($order['order_sn'], $order['order_status'], $order['shipping_status'], $order['pay_status'], $action_note, null, 1);
                $fail_list[$order_sn]['desc'] = $desc;
                $fail_list[$order_sn]['order_id']   = $order['order_id'];
            }

        }
        header('Content-Type: application/json');
        echo json_encode(['finish_list' => $finish_list, 'status' => 1, 'fail_list'=>$fail_list]);
        exit;
    }

    public function isFlsOrderDelivery($mass_key,$check_process = true)
    {
        global $db, $ecs, $_LANG;

        $sql = "SELECT count(do.is_fls) FROM " . $ecs->table('mass_delivery') . " md " .
                "LEFT JOIN " . $ecs->table('delivery_order') . " do ON md.delivery_id = do.delivery_id " .
                "WHERE md.mass_key = '" . $_REQUEST['mass_key'] . "'  ".($check_process? "AND md.process IN ( ".MK_MAIL.",".MK_FINISH.")" : '' ) . " and is_fls = 1";

        $res = $db->getOne($sql);

        if ($res > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function isWilsonOrderDelivery($mass_key,$check_process = true)
    {
        global $db, $ecs, $_LANG;

        $sql = "SELECT count(*) FROM " . $ecs->table('mass_delivery') . " md " .
                "WHERE md.mass_key = '" . $_REQUEST['mass_key'] . "'  ".($check_process? "AND md.process IN ( ".MK_MAIL.",".MK_FINISH.")" : '' ) . " and logistics = 'wilson' ";

        $res = $db->getOne($sql);

        if ($res > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getFlsPrintToken($mass_key)
    {
        global $db, $ecs, $_LANG;

        $secret_key = 'yD<.PH6xCLtk{YR!P_FhR[Hx+&fn2&pH'; 
        return $token = md5($mass_key.$secret_key);
    }

    public function getWilsonPrintToken($mass_key)
    {
        global $db, $ecs, $_LANG;

        $secret_key = 't|TWprV7=iO4+x,2bWP>Qp$}H~3>||21'; 
        return $token = md5($mass_key.$secret_key);
    }

    public function validate_fls_print_token($mass_key,$fp_token)
    {
        global $db, $ecs, $_LANG;

        $secret_key = 'yD<.PH6xCLtk{YR!P_FhR[Hx+&fn2&pH'; 

        if ($fp_token == md5($mass_key.$secret_key)) {
            return true;
        } else {
            return false;
        }
    }

    public function validate_wilson_print_token($mass_key,$wilson_token)
    {
        global $db, $ecs, $_LANG;

        $secret_key = 't|TWprV7=iO4+x,2bWP>Qp$}H~3>||21'; 

        if ($wilson_token == md5($mass_key.$secret_key)) {
            return true;
        } else {
            return false;
        }
    }

    public function getTheLastDeliveryOrdersGoods($order_sn,$delivery_id,$invoice_no) 
    {
        global $db, $ecs, $_LANG,$_CFG;

        // exclude FLS order
        $sql = "select delivery_id,delivery_sn,is_fls, action_user from ".$ecs->table('delivery_order')." ".
                " where order_sn = '".$order_sn."' and (status = ".DO_DELIVERED." OR (logistics_id = '7' and shipping_id = '5')) ". // logisitcs id = 7, shipping id = 5 for production
                ($delivery_id > 0 ? ' and delivery_id = '.$delivery_id.'' : '').
                ((!empty($invoice_no) && ($invoice_no > 0 || sizeof($invoice_no) > 0)) ? " and invoice_no = '".$invoice_no."'" : "").
                " order by is_fls asc, delivery_id desc limit 1";
        $res = $db->getRow($sql);
        $delivery_id = $res['delivery_id'];

        if (empty($delivery_id)) {
            $arr = array(
                'is_fls' => 0,
                'delivery_id' => 0,
    		    'data' => [],
    		    'record_count' => 0,
                'is_passed' => 0,
                'delivery_orders' => []
    		);
            return $arr;
        }

        if ($res['is_fls'] == 1) {
            $arr = array(
                'is_fls' => 1,
                'delivery_id' => 0,
    		    'data' => [],
    		    'record_count' => 0,
                'is_passed' => 0,
                'delivery_orders' => []
    		);
            return $arr;
        }

        // get total delivery order 
        $sql = "select delivery_id,delivery_sn,is_fls,update_time from ".$ecs->table('delivery_order')." where order_sn = '".$order_sn."' and status = ".DO_DELIVERED." order by is_fls asc, delivery_id desc";
        $delivery_orders = $db->getAll($sql);

        foreach ($delivery_orders as &$delivery_order) {
            $delivery_order['delivery_order_list_html'] = '<div class="radio"><label><input type="radio" value="'.$delivery_order['delivery_id'].'" name="selected_delivery_order"> '. local_date($_CFG['time_format'], $delivery_order['update_time']) . ' ('. $delivery_order['delivery_sn'] . ')' .($delivery_order['is_fls']? ' - FLS' : '').' </label></div>';
        }

        $sql = "select passed from ".$ecs->table('order_cross_check')." where delivery_id = '".$delivery_id."' and passed = 1 limit 1";
        $is_passed = $db->getOne($sql);

        if (empty($is_passed)) {
            $is_passed = 0;
        } else {
            $is_passed = 1;
        }

        $order_goods = $this->get_delivery_goods_info($delivery_id);

        foreach ($order_goods as &$goods) {
            $goods['stocktake_number'] = 0;
            $goods['stocktake_number_html'] = '<span class="stocktake-'.$goods['goods_sn'].'" data-stocktake-number="0" >0</span>';
            $goods['goods_list_html_for_special'] = '<div class="radio"><label><input type="radio" checked="" value="'.$goods['goods_sn'].'" name="goods_sn_special"> '. $goods['goods_sn'] . '(' . $goods['goods_name'] . ')</label></div>';
        }

        $record_count = sizeof($order_goods);
		$arr = array(
            'delivery_id' => $delivery_id,
		    'data' => $order_goods,
		    'record_count' => $record_count,
            'is_passed' => $is_passed,
            'delivery_orders' => $delivery_orders,
            'action_user' => $res['action_user']
		);

		return $arr;
    }

    public function isOrderSn($order_sn)
    {
        global $db, $ecs, $_LANG;

        $sql = "SELECT count(*) FROM ".$ecs->table('order_info')." where order_sn = '".$order_sn."' ";
        $res = $db->getOne($sql);

        if ($res > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function save_cross_check_error_history()
    {
        global $db, $ecs, $_LANG;

        $sql="insert into ".$GLOBALS['ecs']->table('order_cross_check')." (order_sn,delivery_id,passed,operator,log,last_update_time) values ( 
            '".$_REQUEST['order_sn'].
            "','".$_REQUEST['delivery_id'].
            "','".$_REQUEST['passed'].
            "','".$_REQUEST['operator'].
            "','".$_REQUEST['log']."','".gmtime()."') ON DUPLICATE KEY UPDATE passed = '".$_REQUEST['passed']."' , operator = '".$_REQUEST['operator']."', `log` = CONCAT(`log`, '".$_REQUEST['log']."' ) , last_update_time = '".gmtime()."' ";
        
        //echo $sql;

        $db->query($sql);
        
        return true;
    }

    public function saveOrderGoodsSerialNumbers()
    {
        global $db, $ecs, $userController ,$_CFG, $_LANG;
        
        $serial_numbers = '';
        if (!empty($_REQUEST['order_sn'])) {
            if (!empty($_REQUEST['serial_numbers'])) {
               $serial_numbers = implode("\n",$_REQUEST['serial_numbers']);
            }
            
            $sql = "update ".$ecs->table('order_info')." set `serial_numbers` = '".$serial_numbers."' where `order_sn` = '".$_REQUEST['order_sn']."' ";
            $db->query($sql);
        }

        return true;
    }

    public function savePickerPacker()
    {
        global $db, $ecs, $userController ,$_CFG, $_LANG;
        $orderController = new OrderController();
        if (!empty($_REQUEST['order_sn']) && !empty($_REQUEST['operator'])) {
            $orderController->update_order_picker_packer($_REQUEST['order_sn'],$_REQUEST['operator'],$_REQUEST['operator'],local_date('Y-m-d'));
        }

        return true;
    }

    public function saveCrossCheckSerialNumber()
    {
        global $db, $ecs, $userController ,$_CFG, $_LANG;

        $sql = "update ".$GLOBALS['ecs']->table('order_cross_check')." set serial_number_scanned = 1 where delivery_id = '".$_REQUEST['delivery_id']."' ";
        $db->query($sql);
        return true;
    }

    public function saveCrossCheckError()
    {
        global $db, $ecs, $userController ,$_CFG, $_LANG;

        $sql = "update ".$GLOBALS['ecs']->table('order_cross_check')." set error = 1 where delivery_id = '".$_REQUEST['delivery_id']."' ";
        $db->query($sql);
        return true;
    }

    public function getOrderGoodsCrossCheckList($is_pagination=true)
    {
        global $db, $ecs, $userController ,$_CFG, $_LANG;
        ini_set('memory_limit', '512M');
        //$start_date = $_REQUEST['start_date'];
        //$end_date = $_REQUEST['end_date'];
        //$period = $_REQUEST['period'];

        if (!empty($_REQUEST['period'])) {
            $end_time_period= local_strtotime(date("Y-m-d", strtotime( date( 'Y-m-d' )." -0 months")));
            $start_time_period  = local_strtotime(date("Y-m-d", strtotime( date( 'Y-m-d' )." -".$_REQUEST['period']." days")));      
        } else {
            // last 30 days
            $end_time_period= local_strtotime(date("Y-m-d", strtotime( date( 'Y-m-d' )." -0 months")));
            $start_time_period  = local_strtotime(date("Y-m-d", strtotime( date( 'Y-m-d' )." -30 days")));      
        }

        if (empty($_REQUEST['start_date'])) {
            $start_time = $start_time_period;
            $start_date = $today_date;
        } else {
            $start_time = local_strtotime($_REQUEST['start_date']);
            $start_date = $_REQUEST['start_date'];
        }

        if (empty($_REQUEST['end_date'])) {
            $end_time = $end_time_period;
            $end_date = $today_date;
        }else{
            $end_time= local_strtotime($_REQUEST['end_date']);
            $end_date = $_REQUEST['end_date'];
        }

        $wholesaleUserIds = implode(',', $userController->getWholesaleUserIds());

        $sql = " select count(*) from ( SELECT DATE_FORMAT(from_unixtime(update_time + (8*3600)), '%Y-%m-%d') as shipping_date,count(do.delivery_id) as total_order 
                    FROM " . $ecs->table('delivery_order') . " do " . 
               "left join ". $ecs->table('order_cross_check') ." occ on (do.delivery_id = occ.delivery_id) ".
               "left join ". $ecs->table('order_info') ." oi on (do.order_id = oi.order_id) " .  
               " WHERE (do.status = ".DO_DELIVERED." or do.status = ".DO_COLLECTED." ) and do.is_fls = 0 and do.shipping_id != 2 and do.logistics_id != 8 and do.logistics_id != 14 and update_time BETWEEN " . ($start_time) . " AND " . ($end_time + 86399) .
               " and (order_type <>'". OrderController::DB_ORDER_TYPE_WHOLESALE . "' OR (oi.order_type IS NULL and oi.user_id NOT IN (". $wholesaleUserIds .")))" . 
               " group by shipping_date ) as t ";

        $total_count = $db->getOne($sql);

        $_REQUEST['record_count'] = $total_count;
        $_REQUEST['sort_by']    = empty($_REQUEST['sort_by']) ? 'shipping_date' : trim($_REQUEST['sort_by']);
        $_REQUEST['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);

        $sql = "SELECT DATE_FORMAT(from_unixtime(update_time + (8*3600)), '%Y-%m-%d') as shipping_date,count(do.delivery_id) as total_order, IFNULL(count(operator),0) as total_finish, IFNULL(sum(passed),0) as total_passed, IFNULL(sum(error),0) as total_error FROM " . $ecs->table('delivery_order') . " do " . 
                "left join ". $ecs->table('order_cross_check') ." occ on (do.delivery_id = occ.delivery_id) ".
                "left join ". $ecs->table('order_info') ." oi on (do.order_id = oi.order_id) " .  
                " WHERE (do.status = ".DO_DELIVERED." or do.status = ".DO_COLLECTED." ) and do.is_fls = 0 and do.shipping_id != 2 and do.logistics_id != 8 and do.logistics_id != 14 and update_time BETWEEN " . ($start_time) . " AND " . ($end_time + 86399) .
                " and (order_type <>'". OrderController::DB_ORDER_TYPE_WHOLESALE . "' OR (oi.order_type IS NULL and oi.user_id NOT IN (". $wholesaleUserIds .")))" . 
                " group by shipping_date ";

        $sql .= ' order by '. $_REQUEST['sort_by'] . ' ' .  $_REQUEST['sort_order'];
        $sql .= ($is_pagination ? " LIMIT " . $_REQUEST['start'] . ", " . $_REQUEST['page_size'] : '');

        $res = $db->getAll($sql);

        // break down start
        $sql = "SELECT do.delivery_id,do.order_id,do.update_time,do.shipping_id,do.logistics_id,do.status,do.is_fls,occ.passed FROM " . $ecs->table('delivery_order') . " do " . 
                "left join ". $ecs->table('order_cross_check') ." occ on (do.delivery_id = occ.delivery_id) ".
                "left join ". $ecs->table('order_info') ." oi on (do.order_id = oi.order_id) " .  
                " WHERE (do.status != ".DO_NORMAL." && do.status != ".DO_RETURNED.") and do.shipping_id != 4 and update_time BETWEEN " . ($start_time) . " AND " . ($end_time + 86399) 
                ;

        //echo $sql;
        $break_down_res = $db->getAll($sql);
        
        $order_break_down = [];
        foreach ($break_down_res as $break_down) {
            $break_down['shipping_date'] = local_date($_CFG['date_format'], $break_down['update_time']);
            $special_case = 0;
            if ($break_down['is_fls'] == 1) {
                $special_case = 1;
                $order_break_down[$break_down['shipping_date']]['logistic'][$break_down['logistics_id']]['fls']++;
            }

            // for store pickup
            if ($break_down['logistics_id'] == 7 && ($break_down['status'] == DO_PACKED || $break_down['status'] == DO_RECEIVED)) {
                $special_case = 1;
            }

            // for 代理送貨 / 司機送貨 / wilson
            if ($break_down['logistics_id'] == 9 || $break_down['logistics_id'] == 8 || $break_down['logistics_id'] == 14) {
                $special_case = 1;
            }

            if ($break_down['passed'] == '' && $special_case == 0) {
                if (!isset($order_break_down[$break_down['shipping_date']]['logistic'][$break_down['logistics_id']]['finish'])) {
                    $order_break_down[$break_down['shipping_date']]['logistic'][$break_down['logistics_id']]['finish'] = 0;
                }
                //$order_break_down[$break_down['shipping_date']]['logistic'][$break_down['logistics_id']]['unfinish']++;
            } else {
                $order_break_down[$break_down['shipping_date']]['logistic'][$break_down['logistics_id']]['finish']++;
            }
            $order_break_down[$break_down['shipping_date']]['logistic'][$break_down['logistics_id']]['total']++;
            $order_break_down[$break_down['shipping_date']]['all_orders_total']++;
        }
       // echo '<pre>';
       // print_r($order_break_down);
       // exit;

        $logistic_methods = $this->getLogisticsMethod();

        foreach ($res as &$record) {
            $record['progress_rate'] = ($record['total_finish'] / $record['total_order'] * 100);
            $progress_html = '<div class="widget_summary">
                    <div class="w_center w_55" style="width: 100%;">
                      <div class="progress" style="margin-bottom: 0px;">
                        <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" title = "'.$record['progress_rate'] .'%" style="width: '.$record['progress_rate'] .'%;">
                          <span class="sr-only">'.$record['progress_rate'] .'% Complete</span>
                        </div>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div style="width:40px">'.round($record['progress_rate']) .'%</div>
                  </div>';
            $record['progress_rate_html'] = $progress_html;

            // haven't finished
            $record['total_unfinished'] = $record['total_order'] - $record['total_finish'];
            $record['total_not_passed'] = $record['total_finish'] - $record['total_passed'];
            $record['total_finished'] = $record['total_finish'] - $record['total_passed'];
            $record['total_finished_html'] = '正確:'.$record['total_passed'] . ' 錯誤:' . $record['total_not_passed'];

            $weekday = date('w', strtotime($record['shipping_date']));
            $weeklist = array('日', '一', '二', '三', '四', '五', '六');
            $shipping_date = $record['shipping_date'];

            foreach ($logistic_methods as $logistic) {
                if (isset($order_break_down[$shipping_date]) && isset($order_break_down[$shipping_date]['logistic'][$logistic['logistics_id']]) ) {
                    if ($order_break_down[$shipping_date]['logistic'][$logistic['logistics_id']]['finish'] == $order_break_down[$shipping_date]['logistic'][$logistic['logistics_id']]['total']) {
                        $block_class = 'correct_block';
                    } else {
                        $block_class = '';
                    }

                    $persent = round($order_break_down[$shipping_date]['logistic'][$logistic['logistics_id']]['finish']/$order_break_down[$shipping_date]['logistic'][$logistic['logistics_id']]['total'] * 100);

                    if ($persent == 100) {
                        $block_class = 'correct_block_100';
                    } else if ($persent >= 75) {
                        $block_class = 'correct_block_75';
                    } else if ($persent >= 50) {
                        $block_class = 'correct_block_50';
                    } else if ($persent >= 25) {
                        $block_class = 'correct_block_25';
                    } else if ($persent >= 0) {
                        $block_class = 'correct_block_0';
                    }
                    // echo $shipping_date;
                    // echo $order_break_down[$shipping_date]['logistic'][$logistic['logistics_id']]['fls'];
                    // echo '<br>';
                    if (!empty($order_break_down[$shipping_date]['logistic'][$logistic['logistics_id']]['fls'])) {
                        $fls_string = '<br>FLS('.$order_break_down[$shipping_date]['logistic'][$logistic['logistics_id']]['fls'].')';
                    } else {
                        $fls_string = '';
                    }
                    $record['logistic_progress_'.$logistic['logistics_id']] = '<a class="link_text" target="_blank" href="order_goods_cross_check_report_detail.php?logistics_id='.$logistic['logistics_id'].'&date='.$record['shipping_date'].'"><span class="'.$block_class.' link_text">'. $order_break_down[$shipping_date]['logistic'][$logistic['logistics_id']]['finish'] . '/' . $order_break_down[$shipping_date]['logistic'][$logistic['logistics_id']]['total'] . '<br>('.$persent.'%) '.$fls_string.' </span></a>';
                } else {
                    $record['logistic_progress_'.$logistic['logistics_id']] = '<span class="na_block">'.'0/0'.'</span>';
                }
            }

            $record['all_orders_total'] = $order_break_down[$shipping_date]['all_orders_total'];

            $record['shipping_date'] = '<a href="order_goods_cross_check_report_detail.php?date='.$shipping_date.'">' . $record['shipping_date'] .  ' (星期' . $weeklist[$weekday].')</a>';

        }



        $arr = array(
          'data' => $res,
          'record_count' => $total_count
        );
        return $arr;
    }

    public function getOrderGoodsCrossCheckListByDate($date)
    {
        global $db, $ecs, $userController ,$_CFG, $_LANG;

        if (isset($_REQUEST['status']) && $_REQUEST['status'] == -1 ) {  
           // $sqlWhereStatus = ' and do.status = '.DO_DELIVERED.' '; 
        } else if (isset($_REQUEST['status']) && $_REQUEST['status'] == 0 ) {  
            $sqlWhereStatus = ' and do.status = '.DO_DELIVERED.' '; 
        } else if (isset($_REQUEST['status']) && $_REQUEST['status'] == 1 ) { 
            $sqlWhereStatus = ' and do.status = '.DO_COLLECTED.' ';
        }

        if (isset($_REQUEST['passed']) && $_REQUEST['passed'] != -1 ) {
            if ($_REQUEST['passed'] == '') {
                $sqlWherePassed = ' and passed is null '; 
            } else {
                $sqlWherePassed = ' and passed = '.$_REQUEST['passed'].' '; 
            }
        }

        if (!empty($_REQUEST['order_sn'])) {
            $sqlWhereOrderSn = ' and do.order_sn = '.$_REQUEST['order_sn'].' ';
            $sqlWhereStatus = '';
            $sqlWherePassed = '';
            $date = '';
            $sqlWhereDate = "";
        } else {
            $date_time = local_strtotime($date);
            $sqlWhereDate = " and  do.update_time BETWEEN " . ($date_time) . " AND " . ($date_time + 86399) ." "; 
        }

        if (!empty($_REQUEST['logistics_id']) && $_REQUEST['logistics_id'] != -1) {
            $sqlWherelogisticsId = ' and do.logistics_id = '.$_REQUEST['logistics_id'].' ';
        }
        
        $wholesaleUserIds = implode(',', $userController->getWholesaleUserIds());

        $sql = "SELECT do.*, au.user_name, g.goods_thumb,occ.delivery_id as cross_check_delivery_id, occ.passed, occ.operator, occ.serial_number_scanned, occ.error, occ.log ".
                " FROM " . $ecs->table('delivery_order') . " do " . 
                "left join ". $ecs->table('delivery_goods') ." dg on (do.delivery_id = dg.delivery_id) " . 
                "left join ". $ecs->table('goods') ." g on (dg.goods_id = g.goods_id) " . 
                "left join ". $ecs->table('order_cross_check') ." occ on (do.delivery_id = occ.delivery_id and do.order_sn = occ.order_sn) " . 
                "left join ". $ecs->table('admin_user') ." au on (occ.operator = au.user_id) " .
                "left join ". $ecs->table('order_info') ." oi on (do.order_id = oi.order_id) " .
                " WHERE (do.status = ".DO_DELIVERED." or do.status = ".DO_COLLECTED." or (do.logistics_id = '7' and do.shipping_id = '5')) and do.is_fls = 0 and do.shipping_id != 2 and do.logistics_id != 8  and do.logistics_id != 14 " .
                " and (order_type <>'". OrderController::DB_ORDER_TYPE_WHOLESALE . "' OR (oi.order_type IS NULL and oi.user_id NOT IN (". $wholesaleUserIds .")))" . 
                $sqlWhereDate . $sqlWhereStatus . $sqlWhereOrderSn . $sqlWherePassed . $sqlWherelogisticsId .
                " Group by delivery_id " .
                " order by occ.passed asc, occ.error asc, do.update_time desc ";

      
        $res = $db->getAll($sql);

        return $res;
    }

    public function getCrossCheckDetailByDeliveryId($delivery_id,$order_sn)
    {
        global $db, $ecs, $userController ,$_CFG, $_LANG;

        $date_time = local_strtotime($date);
        $sql = "SELECT do.*,g.goods_id, dg.goods_sn,dg.goods_name, dg.send_number, g.goods_thumb,occ.delivery_id as cross_check_delivery_id, occ.passed, occ.operator, occ.serial_number_scanned, occ.error, occ.log ".
                " FROM " . $ecs->table('delivery_order') . " do " . 
                "left join ". $ecs->table('delivery_goods') ." dg on (do.delivery_id = dg.delivery_id) " . 
                "left join ". $ecs->table('goods') ." g on (dg.goods_id = g.goods_id) " . 
                "left join ". $ecs->table('order_cross_check') ." occ on (do.delivery_id = occ.delivery_id) " . 
                
                " WHERE do.delivery_id = '".$delivery_id."' and occ.order_sn = '".$order_sn."' ";
               
        $res = $db->getAll($sql);

        $goods_html = '<div class="goods_info x_panel">
        <div class="table_header_title x_title"><b>出貨產品資訊</b></div>
            <table class="table table-striped">
                <thead><tr>
                    <td width="250">產品名稱 [ 品牌 ]</td><td>條碼</td><td>出貨數量</td></tr>
                </thead>
                <tbody>';
        
        foreach ($res as $goods) {
            $goods_html .= '<tr>
            <td><a href="../goods.php?id='.$goods['goods_id'].'" target="_blank">'.$goods['goods_name'].'</a></td>
            <td>'.$goods['goods_sn'].'</td>
            <td>'.$goods['send_number'].'</td>
            </tr>';
        }        

        $goods_html .= '</tbody>
            </table>
         <div class="table_header_title x_title"><b>記錄</b></div>
            <div class="cross_check_error_history">'.$res[0]['log'].'</div>
        </div>';
        
        
        $responseData['goods_html'] = $goods_html;

        return $responseData;
    }

    public function operatorWarehouseOption($please_select = true)
    {
        global $db, $ecs, $userController ,$_CFG;
        $sql = "SELECT user_id as value,user_name as name FROM ".$ecs->table('admin_user') . " where is_disable = 0 ";

        $roles_ids = [3,11,13];
        
        $sql .=  empty($roles_ids)? '' : " and role_id in ('" . implode("','",$roles_ids) ."') " ;
        $sql .= " order by user_name,user_id";
        $stocktake_person = $db->getAll($sql);
        if ($please_select) {
            array_unshift($stocktake_person, array('name'=>'請選擇...','value'=>''));
        }
        return $stocktake_person;
    }

    public function logisticOptions()
    {
        global $db, $ecs, $userController ,$_CFG;

        $sql = 'SELECT * '.
                'FROM ' . $GLOBALS['ecs']->table('logistics') .' as l '.
                ' WHERE l.enabled = 1';
        
        $res = $db->getAll($sql);
       
        return $res;
    }

    public function markExpressCollected()
    {
        global $db, $ecs, $userController ,$_CFG;

       // $operator = $_REQUEST['operator'];
        $logistic_id = $_REQUEST['logistic_id'];
        $tracking_data = $_REQUEST['tracking_data'];
        $tracking_data = explode(PHP_EOL, $tracking_data);

        if (empty($tracking_data) || empty($logistic_id)) {
            return false;
        }

        $result = [];
        // check the tracking no is correct
        foreach ($tracking_data as $tacking) {
            $sql = "select count(delivery_id) from ".$ecs->table('delivery_order')." where invoice_no = '".$tacking."' and logistics_id = ".$logistic_id." ";
            $tracking_count = $db->getOne($sql);
            if ($tracking_count == 0) {
                $result['error'][] = '['.$tacking.']找不到!';
            }
        }
        
        if (isset($result['error'])) {
            return $result;
        }

        // update
        $success_count = 0;
        $delivery_ids = [];
        foreach ($tracking_data as $tacking) {
            // get order sn;
            $sql = "select order_sn,status,order_id,delivery_id from ".$ecs->table('delivery_order')." where invoice_no = '".$tacking."' and logistics_id = ".$logistic_id." "; 
            $all_delivery_result = $db->getAll($sql);
            foreach ( $all_delivery_result as $delivery_order_info) {
                $delivery_id = $delivery_order_info['delivery_id'];
                $order_id = $delivery_order_info['order_id'];
                $order_sn = $delivery_order_info['order_sn'];
                $delivery_order_status = $delivery_order_info['status'];
                
                if ($delivery_order_status != DO_COLLECTED) {
                    $sql = "update ".$ecs->table('delivery_order')." set status = ".DO_COLLECTED.", collected_time = ".gmtime()." where invoice_no = '".$tacking."' and logistics_id= ".$logistic_id." ";
                    $result = $db->query($sql);
                    
                    // update delay day
                    $sql = "select * from ".$ecs->table('order_progress')." where order_id = ".$order_id." and status != 2 order by progress_id desc limit 1 ";
                    $progress_res = $db->getRow($sql);
                    $delay_seconds = gmtime() - $progress_res['estimated_ship_time']; 
                    if ($delay_seconds < 0) {
                        $delay_seconds = 0;
                    }
                    $update_sql = "update ".$ecs->table('order_progress')." set delay_seconds= ".$delay_seconds .", status = 1 where status = 0 and order_id = ".$order_id." ";
                   
                    $db->query($update_sql);
                    
                    if (!empty($order_sn)) {
                        $sql  = "SELECT oi.order_id,oi.order_sn,oi.order_status,oi.shipping_status,oi.pay_status,oi.serial_numbers";
                        $sql .= " FROM " .$ecs->table('order_info') ." oi";
                        $sql .= " WHERE order_sn = '".$order_sn."'";
                        $res = $db->getRow($sql);
    
                        $admin_id = $_SESSION['admin_id'];
                        $sql = "SELECT user_name FROM ".$GLOBALS['ecs']->table('admin_user')." WHERE user_id = ".$admin_id." LIMIT 1";
                        $admin_name = $GLOBALS['db']->getOne($sql);
                        
                        order_action($res['order_sn'], $res['order_status'], $res['shipping_status'], $res['pay_status'],
                         '[系統備註] 追蹤號:' .$tacking. ' 已收件' , $admin_name);
                        
                        $delivery_ids[] = $delivery_id;
                    }
                }
                $success_count ++;
            }
        }

        $ar['import_count'] = $success_count;
        $ar['delivery_ids'] = $delivery_ids;

        return $ar;
    }

    function sendEmailAfterExpressCollected()
    {
        global $db, $ecs, $userController ,$_CFG;

        $delivery_ids = $_REQUEST['delivery_ids'];

        $success_count = 0;
        foreach ($delivery_ids as $delivery_id) {
            $sql = "select order_id,delivery_id from ".$ecs->table('delivery_order')." where delivery_id = '".$delivery_id."'"; 
            $delivery_order_info = $db->getRow($sql);
            $order = order_info($delivery_order_info['order_id']);
            $this->delivery_send_mail($order,$delivery_id,true);
            $success_count ++;
        }

        return $success_count;
    }

    function getLogisticsMethod() 
    {
        global $db, $ecs, $userController ,$_CFG;

        $sql = "select * from ".$ecs->table('logistics')." where enabled = 1 ";
        $result = $db->getAll($sql);

        return $result;
    }


    function countAdminDelayShip($data)
    {
        global $ecs, $db;
        ini_set('memory_limit', '512M');
        $month_start = local_date('Y-m-01 00:00:00');
        $month_end = local_date('Y-m-t 23:59:59');
        set_time_limit(0);
        $date_start = empty($data['start_date']) ? local_strtotime($month_start) : local_strtotime($data['start_date']);
        $date_end = empty($data['end_date']) ? local_strtotime($month_end) : local_strtotime($data['end_date']);
        $sql_delivered = "SELECT ogsc.admin_id,
        au.user_name as admin_name,
        COUNT(og.rec_id) as total,
        SUM(CASE WHEN do.update_time > og.planned_delivery_date + 86400 THEN 1 ELSE 0 END) as total_delay,
        SUM(CASE WHEN cast(do.update_time as signed ) - cast(og.planned_delivery_date as signed ) BETWEEN 0 AND 86400 THEN 1 ELSE 0 END) as total_on_time,
        SUM(CASE WHEN do.update_time < og.planned_delivery_date THEN 1 ELSE 0 END) as total_early ".
        " FROM " . $ecs->table('delivery_order') . " do " .
        " INNER JOIN " . $ecs->table('delivery_goods') . " dg ON dg.delivery_id = do.delivery_id " .
        " INNER JOIN " . $ecs->table('order_goods') . " og ON og.rec_id = dg.order_item_id " .
        " INNER JOIN " . $ecs->table('order_info') . " oi ON oi.order_id = og.order_id " .
        " INNER JOIN ".
            "(SELECT supplier_id, order_goods_id, consumed_qty, MAX(admin_id) AS admin_id ".
                "FROM (SELECT esa.admin_id, ogsc2.* FROM  " . $ecs->table('erp_supplier_admin') . " esa ".
                    "LEFT JOIN (SELECT supplier_id, order_goods_id, MAX(consumption_id) AS consumption_id, consumed_qty ".
                        "FROM " . $ecs->table('order_goods_supplier_consumption') . " GROUP BY order_goods_id, supplier_id ".
                    ") ogsc2 ON esa.supplier_id = ogsc2.supplier_id".
                ") ogsc3 GROUP BY order_goods_id, supplier_id".
            ") ogsc ON ogsc.order_goods_id = og.rec_id " .
        " LEFT JOIN ".$ecs->table('admin_user') ." as au on au.user_id = ogsc.admin_id ".
        " LEFT JOIN ".$ecs->table('erp_supplier') ." as es on es.supplier_id = ogsc.supplier_id ".
        " WHERE do.update_time BETWEEN $date_start AND $date_end AND dg.send_number > 0 AND og.planned_delivery_date > 0 AND ogsc.consumed_qty > 0 AND oi.shipping_status != 0 group by admin_id ";
        $res_delivered = $db->getAll($sql_delivered);
        $count_list = [];
        $count_list['all']['total'] = 0;
        $count_list['all']['delay'] = 0;
        foreach($res_delivered as $key => $value) {
            $count_list[$value['admin_id']]['total'] = $value['total'];
            $count_list['all']['total'] += $count_list[$value['admin_id']]['total'];

            $count_list[$value['admin_id']]['delay'] = $value['total_delay'];
            $count_list['all']['delay'] += $count_list[$value['admin_id']]['delay'];
            $count_list[$value['admin_id']]['delay_rate']  = round(($count_list[$value['admin_id']]['delay'] / $count_list[$value['admin_id']]['total']) * 100);

            $count_list[$value['admin_id']]['on_time'] = $value['total_on_time'];
            $count_list['all']['on_time'] += $count_list[$value['admin_id']]['on_time'];
            $count_list[$value['admin_id']]['on_time_rate']  = round(($count_list[$value['admin_id']]['on_time'] / $count_list[$value['admin_id']]['total']) * 100);

            $count_list[$value['admin_id']]['early'] = $value['total_early'];
            $count_list['all']['early'] += $count_list[$value['admin_id']]['early'];
            $count_list[$value['admin_id']]['early_rate']  = round(($count_list[$value['admin_id']]['early'] / $count_list[$value['admin_id']]['total']) * 100);
        }
        $count_list['all']['delay_rate']  = round(($count_list['all']['delay'] / $count_list['all']['total']) * 100);
        $count_list['all']['on_time_rate']  = round(($count_list['all']['on_time'] / $count_list['all']['total']) * 100);
        $count_list['all']['early_rate']  = round(($count_list['all']['early'] / $count_list['all']['total']) * 100);

        return $count_list;
    }

    function getDeliveryStatusName($status)
    {
        $status_ar[DO_DELIVERED] = '已發貨';
        $status_ar[DO_RETURNED] = '退貨';
        $status_ar[DO_NORMAL] = '正常';
        $status_ar[DO_RECEIVED] = '己收貨';
        $status_ar[DO_PACKED] = '己pack 貨';
        $status_ar[DO_COLLECTED] = '速遞已收件';

        return $status_ar[$status];
    }

    function getWholesaleDeliveryStatusOption()
    {
        $statusOptions = array ( DO_DELIVERED => '已發貨',
                                 DO_PACKED => '己pack貨',
                                 DO_RECEIVED => '己收貨',
                        );
        return $statusOptions;
    }

    function getDeliveryOrderGoods($delivery_id)
    {
        global $ecs, $db;

        $sql = "SELECT dg.send_number, g.goods_name, g.goods_sn, g.goods_id, g.goods_weight FROM " . $ecs->table('delivery_goods') ." as dg ".
        " LEFT JOIN ". $ecs->table('goods') ." as g ON dg.goods_id = g.goods_id ".
        " WHERE delivery_id = ".$delivery_id." ";

        $orderGoods = $db->getAll($sql);

        return $orderGoods;
    }

    function getWilsonLogisticsId() {
        global $ecs, $db;
        $sql = 'select logistics_id from '.$ecs->table('logistics').' where logistics_code = "wilson" ';
        return $db->getOne($sql);
    }

    function get_address_types(){
        global $ecs, $db;
        $sql = "SELECT oadt.address_dest_type_code AS type_code, oadt.address_dest_type_cat AS type_cat, oadt.address_dest_type_id AS type_id,  oadt.address_dest_type_name AS type_name ".
                "FROM ".$ecs->table('order_address_dest_type')." AS oadt ".
                "WHERE oadt.enable = 1 ".
                "ORDER BY oadt.address_dest_type_cat ASC, oadt.address_dest_type_id ASC";
        $res = $db->getAll($sql);
        if (empty($res)){
            return false;
        } else {
            return $res;
        }
    }
    
    function get_address_types_groups(){
        global $ecs, $db;
        $sql = "SELECT DISTINCT address_dest_type_cat AS cat ".
                "FROM ".$ecs->table('order_address_dest_type')." ".
                "WHERE enable = 1 ".
                "ORDER BY address_dest_type_cat ASC ";
        $res = $db->getAll($sql);
        if (empty($res)){
            return false;
        } else {
            return $res;
        }
    }
    
    function getMassDeliveryAddressAttribute($addr_id, $logistic){
        $controller = new YohoBaseController();
        global $ecs, $db;
        $sql = "SELECT oa.order_address_id, oa.address, oa.locality, oa.administrative_area, oa.dest_type AS type_id , oa.lift, oa.verify, oadt.address_dest_type_name AS type_name ".
                "FROM ".$ecs->table('order_address')." AS oa ".
                "LEFT JOIN ".$ecs->table('order_address_dest_type')." AS oadt ON oadt.address_dest_type_id = oa.dest_type ".
                "WHERE oa.order_address_id = '".$addr_id."' ".
                "ORDER BY oa.order_address_id DESC ";
        $res = $db->getRow($sql);
        if (empty($res)){
            return FALSE;
        } else {
            if ($logistic == '4px'){
                $sql = "SELECT id_doc FROM ".$ecs->table('order_info')." WHERE address_id = '".$addr_id."' ORDER BY order_id DESC ";
                $res_id_doc = $db->getRow($sql);
                if (!empty($res_id_doc['id_doc'])){
                    $res['id_doc'] = decrypt_str($res_id_doc['id_doc']);
                } else {
                    $res['id_doc'] = "";
                }
                return $res;
            }
            return $res;
        }
    }

}