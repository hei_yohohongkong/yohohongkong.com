<?php

/***
* rmaController.php
*
*
*
***/
namespace Yoho\cms\Controller;

use Yoho\cms\Model;

use \Exception;

use Yoho\cms\Controller\ErpController;

if (!defined('IN_ECS')) {
    die('Hacking attempt');
}
class RmaController extends YohoBaseController
{
    const PENDING_FOR_CHECKING = '0';
    const NOTIFY_SUPPLIER = '1';
    const WAITING_FOR_RETURN_TO_SUPPLIER = '2';
    const REPAIRING = '3';
    const REPAIRED = '4';
    const FINISHED = '5';
    const TRANSFERRING_TO_REPAIR_CENTER = '6';
    const TRANSFERRING_TO_SHOP = '7';

    const TYPE_ARCHIVED = 0;
    const TYPE_ACTIVE = 1;
    // const TYPE_DELETED = 2;

    const CHANNEL_REPAIR = 0;
    const CHANNEL_REPLACEMENT = 1;
    const CHANNEL_BAD_GOODS = 2;

    const REPAIR_HANDLE_TYPE_SUPPLIER = 0;
    const REPAIR_HANDLE_TYPE_OURSELVES = 1;
    const REPAIR_HANDLE_TYPE_BUY_PARTS = 2;
    const REPAIR_HANDLE_TYPE_NO_NEED_TO_REPAIR = 3;
    const REPAIR_HANDLE_TYPE_WRITE_OFF = 4;

    const DEPARTMENT_SALE = 0;
    const DEPARTMENT_PRODUCT = 1;
    const DEPARTMENT_WAREHOUSE = 2;

    const ROLE_SUPER_ADMIN = 3;
    const ROLE_SALES_TEAM = 2;
    const ROLE_SALES_TEAM_MANAGER = 14;
    const ROLE_TECH_TEAM = 4;
    const ROLE_PRODUCT_TEAM = 5;
    const ROLE_WAREHOUSE_TEAM = 11;
    const ROLE_WAREHOUSE_MANAGER = 13;
    

    private static $statusValues=['等待檢查','通知供應商收貨','等待供應商回收','等待維修中','維修完成待處理','完成','調貨中(去總倉)','調貨中(回門市)'];
    // private static $goodsStatusValues=['等待盤點','已盤點','等待覆核','已完成覆核','重新盤點'];
    private static $typeValues=['已封存','未封存'];
    private static $channelValues=['維修','換貨','壞貨'];
    private static $departmentValues=['Sale Team','Product Team','Wearhouse Team'];

    private $tablename='erp_rma';
    
    public function __construct()
    {
        parent::__construct();
       
        $this->adminuserController = new AdminuserController();
        $this->erpController =  new ErpController();
        $this->accountingController = new AccountingController();
    }
    
    public function createRma($channel, $repair_id, $order_id, $goods_id, $qty, $operator, $reason)
    {
        global $db, $ecs, $userController, $_CFG;

        if (empty($repair_id)) {
            $repair_id = 'null';
        }

        $user_id = $this->getDefaultSaleTeamPersonInCharge();
        if (empty($order_id)) {
            $supplier_unit_price = $this->erpController->getSupplierStockSequence($goods_id,$qty);
            if (sizeof($supplier_unit_price) > 0){
                $po_unit_value = $supplier_unit_price[0]['unit_value'];
            } else {
                $po_unit_value = 0;
            }
        } else {
            $order_goods_id = $db->getOne("SELECT rec_id FROM " . $ecs->table('order_goods') . " WHERE order_id = $order_id AND goods_id = $goods_id");
            $po_unit_value = $db->getOne("SELECT po_unit_value FROM " . $ecs->table('order_goods_supplier_consumption') . " WHERE order_goods_id = $order_goods_id AND goods_id = $goods_id AND consumed_qty > 0 ORDER BY consumption_id DESC LIMIT 1");
        }

        $sql = "insert into ".$ecs->table('erp_rma')." (channel,repair_id,order_id,goods_id,qty,person_in_charge,create_time,operator,reason,po_unit_value) 
                values ('".$channel."',".$repair_id.",'".$order_id."','".$goods_id."','".$qty."','".$user_id."','".gmtime()."','".$operator."','".$reason."','".$po_unit_value."') ";
        $db->query($sql);

        $rma_id = $db->Insert_ID();
        if (!empty($po_unit_value) && bccomp($po_unit_value, 0) == 1) {
            $admin_id = $db->getOne("SELECT user_id FROM " . $ecs->table('admin_user') . " WHERE LOWER(user_name) LIKE LOWER('%$operator%')");
            $order_sn = $db->getOne("SELECT order_sn FROM " . $ecs->table('order_info') . " WHERE order_id = '$order_id'");
			$param = [
                'created_by'        => empty($admin_id) ? 1 : $admin_id,
                'sales_order_id'    => $order_id,
                'rma_id'            => $rma_id,
                'remark'            => "訂單 $order_sn 維修",
				'amount'			=> $po_unit_value,
			];
			$this->accountingController->insertRMATransactions($param);
        }
        return $rma_id;
    }

    public function getRepairCenterWarehouseIds()
    {
        global $db, $ecs, $_CFG;

        $sql = "select warehouse_id from  ".$ecs->table('erp_warehouse')." where is_repair_center  = 1 and is_valid = 1 ";

        $repair_center = $db->getCol($sql);

        if (empty($repair_center)) {
            return array(1);
        }else{
            return $db->getCol($sql);
        }
    }


    public function getChannelOptions()
    {
        return self::$channelValues;
    }

    public function getStatusOptions()
    {
        return self::$statusValues;
    }

    public function getDepartmentOptions()
    {
        return self::$departmentValues;
    }

    public function getDefaultSaleWarehouse()
    {
        return 1;
    }

    public function getDefaultTemporaryWarehouse()
    {
        return 2;
    }

    public function getDefaultSaleTeamPersonInCharge()
    {
        //return 64; // steven
        return 117;  //kei@yohohongkong.com
    }

    public function getDefaultProductTeamPersonInCharge()
    {
        return 57; //chris
    }

    public function getDefaultWarehouseTeamPersonInCharge()
    {
        return 99; // nelson
    }

    public function getTypeOptions()
    {
        return self::$typeValues;
    }

    public function getRepairTemporaryWarehouse($repair_warehouse_id)
    {
        global $db, $ecs, $_CFG;

        $erpController = new ErpController();
        return $erpController->getTempWarehouseIdByShopId($repair_warehouse_id);
    }


    public function personInChargeOption($please_select = true, $roles = '')
    {
        global $db, $ecs, $userController ,$_CFG;
        $sql = "SELECT user_id as value,user_name as name FROM ".$ecs->table('admin_user') . " where is_disable = 0 ";

        if ($roles == 'SaleTeam') {
            $roles_ids = [self::ROLE_SUPER_ADMIN,self::ROLE_SALES_TEAM,self::ROLE_SALES_TEAM_MANAGER];
        } else if ($roles == 'ProductTeam') {
            $roles_ids = [self::ROLE_SUPER_ADMIN,self::ROLE_PRODUCT_TEAM];
        } else if ($roles == 'WarehouseTeam') {
            $roles_ids = [self::ROLE_SUPER_ADMIN,self::ROLE_WAREHOUSE_TEAM,self::ROLE_WAREHOUSE_MANAGER];
        }

        $sql .=  empty($roles_ids)? '' : " and role_id in ('" . implode("','",$roles_ids) ."') " ;
        $sql .= " order by user_name,user_id";
        $stocktake_person = $db->getAll($sql);
        if ($please_select) {
            array_unshift($stocktake_person, array('name'=>'請選擇...','value'=>''));
        }
        return $stocktake_person;
    }

    public function personInChargeList()
    {
        global $db, $ecs, $userController ,$_CFG;
        $sql = "SELECT user_id,user_name FROM ".$ecs->table('admin_user'). ' where is_disable = 0';
        $stocktake_person = $db->getAll($sql);
        $person_list = [];
        foreach ($stocktake_person as $person) {
            $person_list[$person['user_id']] = $person['user_name'];
        }
        return $person_list;
    }

    public function getUserNameById($user_id)
    {
        global $db, $ecs ,$_CFG;

        $sql = "SELECT user_name FROM ".$ecs->table('admin_user'). ' where user_id = '.$user_id.' ';
        return $db->getOne($sql);
    }

    public function getIssueCountByPerson($all_user = false)
    {
        global $db, $ecs ,$_CFG;
        
        $user_ar = [];
        if ($all_user == false) {
            $user_ar = [];
            $user_id = erp_get_admin_id(); // yourself
            $user_ar[] = $user_id;
            
            // get role
            $sql = "select role_id from ".$ecs->table('admin_user')." where user_id = ".$user_id." ";
            $role_id = $db->getOne($sql);
            if ($role_id > 0) {
                //get teammate
                $sql = "select user_id from ".$ecs->table('admin_user')." where role_id = ".$role_id." and is_disable = 0 and user_id != ".$user_id." ";
                $res = $db->getCol($sql);
                if (sizeof($res) > 0) {
                    $user_ar = array_merge($user_ar,$res);
                }
            }
        }

        $sql =  "select count(*) as issue_total, user_id, user_name, avatar " ;
        $sql .= "from ".$ecs->table('admin_user')." ua left join ".$ecs->table('erp_rma')." er  on (er.person_in_charge = ua.user_id) "; 
        $sql .= "where 1 ". (!empty($user_ar)? " and ua.user_id in ('".implode("','",$user_ar)."') " : "" ) . " and status != ".self::FINISHED." and er.type = 1 group by user_id order by issue_total desc ";

        $issue_count_data = $db->getAll($sql);

        foreach ($issue_count_data as &$issue_count){
            //$issue_count['issue_total'] = 101;
        }

        return $issue_count_data;
    }

    public function checkStatusRedirect($rma_id, $stage)
    {
        global $db, $ecs, $userController ,$_CFG;

        $step1_check = array(self::PENDING_FOR_CHECKING,self::TRANSFERRING_TO_REPAIR_CENTER);
        $step2_notify = array(self::NOTIFY_SUPPLIER);
        $step3_awaiting = array(self::WAITING_FOR_RETURN_TO_SUPPLIER);
        $step4_repairing = array(self::REPAIRING);
        $step5_repaired = array(self::REPAIRED,self::TRANSFERRING_TO_SHOP);
        $step6_finished = array(self::FINISHED);
        
        // get rma info
        $rma_info = $this->getRmaInfo($rma_id);

        if (empty($rma_info['goods_admin'])){
            $goods_user_admin = $this->getGoodsUserAdmin($rma_info['goods_id']);
            if ($goods_user_admin > 0) {
                $sql = "update ".$ecs->table('erp_rma')." set goods_admin = '".$goods_user_admin."' where rma_id = '".$rma_id."' ";
                $db->query($sql);
            }
        }
        
        // get status
        $status = $this->getCurrentStatus($rma_id);
        
        if (in_array($status, $step1_check)) {
            // redirect
            if ($stage != 'check') {
                header("Location: erp_rma_check.php?act=list&rma_id=".$rma_id);
                die();
            }
        } elseif (in_array($status, $step2_notify)) {
            // redirect
            if ($stage != 'notify') {
                header("Location: erp_rma_notify.php?act=list&rma_id=".$rma_id);
                die();
            }
        } elseif (in_array($status, $step3_awaiting)) {
            // redirect
            if ($stage != 'awaiting') {
                header("Location: erp_rma_awaiting.php?act=list&rma_id=".$rma_id);
                die();
            }
        } elseif (in_array($status, $step4_repairing)) {
            // redirect
            if ($stage != 'repairing') {
                header("Location: erp_rma_repairing.php?act=list&rma_id=".$rma_id);
                die();
            }
        } elseif (in_array($status, $step5_repaired)) {
            // redirect
            if ($stage != 'repaired') {
                header("Location: erp_rma_repaired.php?act=list&rma_id=".$rma_id);
                die();
            }
        } elseif (in_array($status, $step6_finished)) {
            // redirect
            if ($stage != 'finished') {
                header("Location: erp_rma_finished.php?act=list&rma_id=".$rma_id);
                die();
            }
        }
    }

    public function updatePersonInCharge($rma_id, $user_id)
    {
        global $db, $ecs, $userController ,$_CFG;

        $sql = "update ".$ecs->table('erp_rma')." set person_in_charge = '".$user_id."' where rma_id = '".$rma_id."' ";
        $db->query($sql);
        return true;
    }

    public function getRmaStockInOutLog()
    {
        global $db, $ecs, $userController ,$_CFG;
        require_once(ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_warehouse.php');
        
        //$stockRecords = get_stock_record_rma(['goods_id'=>'2597','order_sn'=>'R2018071178','remark'=>'(RMA:2)'],0, 10);

        $rma_id = $_REQUEST['rma_id'];

        $rma_info = $this->getRmaInfo($rma_id);

        $sql ="SELECT es.*, ";
        $sql.=" g.cat_id, g.goods_sn, g.goods_name, g.goods_thumb, g.goods_img, g.original_img, ";
        $sql.=" ew.name as warehouse_name";
        $sql.=" FROM ".$GLOBALS['ecs']->table('erp_stock')." as es ";
        $sql.=" LEFT JOIN ".$GLOBALS['ecs']->table('goods')." as g ON g.goods_id = es.goods_id ";
        $sql.=" LEFT JOIN ".$GLOBALS['ecs']->table('erp_warehouse')." as ew ON ew.warehouse_id = es.warehouse_id ";
        //$sql.=" LEFT JOIN ".$GLOBALS['ecs']->table('repair_orders')." as ro ON BINARY es.order_sn = BINARY ro.repair_sn ";
        //$sql.=" LEFT JOIN ".$GLOBALS['ecs']->table('erp_stock_transfer')." as est ON BINARY es.order_sn = BINARY est.transfer_sn ";
        $sql.=" WHERE rma_id = ".$rma_id." ". ( !empty($rma_info['repair_transfer_sn']) ? " or (order_sn = '".$rma_info['repair_transfer_sn']."' and es.goods_id = '".$rma_info['goods_id'] ."') " : '' ) .  ( !empty($rma_info['repaired_transfer_sn']) ? " or (order_sn = '".$rma_info['repaired_transfer_sn']."' and es.goods_id = '".$rma_info['goods_id'] ."')  " : '' ) ;

        $sql.=" AND g.is_delete='0'";
        $sql.=" ORDER BY stock_id DESC ";
        
        $res=$GLOBALS['db']->query($sql);
        while ($row=mysql_fetch_assoc($res)) {
            // $goods_id=$row['goods_id'];
            // $sql="select goods_id,cat_id,goods_sn,goods_name,goods_thumb,goods_img,original_img from ".$GLOBALS['ecs']->table('goods')." where goods_id='".$goods_id."'";
            // $goods_info=$GLOBALS['db']->getRow($sql);
            // if(!empty($goods_info['goods_thumb']))
            // {
            // 	$goods_info['goods_thumb']='../'.$goods_info['goods_thumb'];
            // }
            // if(!empty($goods_info['goods_img']))
            // {
            // 	$goods_info['goods_img']='../'.$goods_info['goods_img'];
            // }
            // if(!empty($goods_info['original_img']))
            // {
            // 	$goods_info['original_img']='../'.$goods_info['original_img'];
            // }
            // $row['goods_info']=$goods_info;
            $row['goods_info']=array(
                'goods_id' => $row['goods_id'],
                'cat_id' => $row['cat_id'],
                'goods_sn' => $row['goods_sn'],
                'goods_name' => $row['goods_name'],
                'goods_thumb' => (empty($row['goods_thumb']) ? '' : '../'.$row['goods_thumb']),
                'goods_img' => (empty($row['goods_img']) ? '' : '../'.$row['goods_img']),
                'original_img' => (empty($row['original_img']) ? '' : '../'.$row['original_img'])
            );
    
            $sql ="SELECT ro.repair_id, est.transfer_id FROM ".$GLOBALS['ecs']->table('repair_orders')." as ro ";
            $sql.=" LEFT JOIN ".$GLOBALS['ecs']->table('erp_stock_transfer')." as est ON ro.repair_sn = est.transfer_sn ";
            $sql.=" WHERE ro.repair_sn = '".$row['order_sn']."'";
    
            $repair_info = $GLOBALS['db']->getRow($sql);
            $row['repair_id'] = $repair_info['repair_id'];
            $row['transfer_id'] = $repair_info['transfer_id'];
            
            $row['act_date']=local_date('Y-m-d <br/> H:i:s', $row['act_time']);
            
            // Goods Attr is not used
            //$row['goods_attr']=get_attr_info($row['goods_attr_id']);
            
            // Optimized to the main query
            //$warehouse_id=$row['warehouse_id'];
            //$sql=" select name from ".$GLOBALS['ecs']->table('erp_warehouse')." where warehouse_id='".$warehouse_id."'";
            //$row['warehouse_name']=$GLOBALS['db']->getOne($sql);
            
            $stock_style=$row['stock_style'];
            $w_d_style_id=$row['w_d_style_id'];
            if ($w_d_style_id==0) {
                $row['w_d_style']=$GLOBALS['_LANG']['erp_stock_transfer_w_d_style'];
            } else {
                if ($stock_style=='w') {
                    $sql=" select warehousing_style from ".$GLOBALS['ecs']->table('erp_warehousing_style')." where warehousing_style_id ='".$w_d_style_id."'";
                } elseif ($stock_style=='d') {
                    $sql=" select delivery_style from ".$GLOBALS['ecs']->table('erp_delivery_style')." where delivery_style_id ='".$w_d_style_id."'";
                }
                $row['w_d_style']=$GLOBALS['db']->getOne($sql);
            }

            if ($row['order_sn'] != '' && ($row['order_sn'] == $rma_info['repair_transfer_sn'] || $row['order_sn'] == $rma_info['repaired_transfer_sn']) ) {
                $row['qty'] = ( $row['stock_style'] == 'w' ? '' : '-') .$rma_info['qty'];
                //$row['stock'] = '-';            
            }
            // Agency is not used
            //$sql="select agency_name from ".$GLOBALS['ecs']->table('agency')." where agency_id in (select agency_id from ".$GLOBALS['ecs']->table('erp_warehouse')." where warehouse_id=".$row['warehouse_id'].")";
            //$row['agency_name']=$GLOBALS['db']->getOne($sql);
            
            // Barcode is not used
            //$sql="select gb.barcode from ".$GLOBALS['ecs']->table('erp_goods_barcode')." as gb,".$GLOBALS['ecs']->table('erp_goods_attr')." as ga where ga.attr_id='".$row['goods_attr_id']."' and ga.barcode_id=gb.id limit 1";
            //$row['barcode']=$GLOBALS['db']->getOne($sql);
            $stock_record[]=$row;
        }
        
        //$stock_record;

        //echo '<pre>';
        //print_r($stockRecords);
     
        //exit;
        //
        // $res=[];
        //
        // $temp1 = array ('warehouse' => '暫存倉',
        //                'warehousing_type' => '貨倉調貨',
        //                'reference_id' => 'DE1807020',
        //                'pick' => 'admin',
        //                'date' => '2018-07-10 10:20:01',
        //                 'qty' => '-1',
        //                 'stock' => '9',
        //         );
        //
        // $temp2 = array ('warehouse' => '暫存倉',
        //        'warehousing_type' => '維修單-入暫存庫',
        //        'reference_id' => 'DE1807020',
        //        'pick' => '供應商A',
        //        'date' => '2018-07-10 10:20:01',
        //         'qty' => '+1',
        //         'stock' => '10',
        // );
        //
        // $temp3 = array ('warehouse' => '暫存倉',
        //        'warehousing_type' => '維修單-送修出庫',
        //        'reference_id' => 'DE1807020',
        //        'pick' => '供應商A',
        //        'date' => '2018-07-10 10:20:01',
        //         'qty' => '-1',
        //         'stock' => '9',
        // );
        //
        // $res[] = $temp1;
        // $res[] = $temp2;
        // $res[] = $temp3;
        //
        // $_REQUEST['record_count'] = sizeof($res);

        $arr = array(
            'data' => $stock_record,
            'record_count' => sizeof($stock_record)
        );

        return $arr;
    }

    public function rmaPickupConfirm()
    {
        global $db, $ecs, $userController, $_CFG;
        // update pickup time
    }

    public function getStepByStatus($status_id)
    {
        global $db, $ecs, $userController, $_CFG;

        switch ($status_id) {
            case self::PENDING_FOR_CHECKING:
                $step = 1;
                break;
            case self::NOTIFY_SUPPLIER:
                $step = 2;
                break;
            case self::WAITING_FOR_RETURN_TO_SUPPLIER:
                $step = 3;
                break;
            case self::REPAIRING:
                $step = 4;
                break;
            case self::REPAIRED:
                $step = 5;
                break;
            case self::FINISHED:
                $step = 6;
                break;
        }

        return $step;
    }

    public function getStepName($step)
    {
        switch ($step) {
            case 1:
                $stepName = '產品檢查';
                break;
            case 2:
                $stepName = '通知供應商回收';
                break;
            case 3:
                $stepName = '等待供應商回收';
                break;
            case 4:
                $stepName = '等待維修中';
                break;
            case 5:
               $stepName = '維修完成待處理';
               break;
            case 6:
                $stepName = '完成';
                break;
        }

        return $stepName;
    }

    public function rmaChangeStatus($rma_id, $status_id)
    {
        global $db, $ecs, $userController ,$_CFG;

        $sql = "update ".$ecs->table('erp_rma')." set status = '".$status_id."' where rma_id = '".$rma_id."' ";
        $db->query($sql);

        $rma_info = $this->getRmaInfo($rma_id);

        // check department
        switch ($status_id) {
            case self::PENDING_FOR_CHECKING:
                $department = self::DEPARTMENT_SALE;
                $person_in_charge = $this->getDefaultSaleTeamPersonInCharge();
                break;
            case self::NOTIFY_SUPPLIER:
                $department = self::DEPARTMENT_PRODUCT;
                if ($rma_info['goods_admin'] > 0) {
                    $person_in_charge = $rma_info['goods_admin'];
                } else {
                    $person_in_charge = $this->getDefaultProductTeamPersonInCharge();
                }
                
                break;
            case self::WAITING_FOR_RETURN_TO_SUPPLIER:
                $department = self::DEPARTMENT_WAREHOUSE;
                $person_in_charge = $this->getDefaultWarehouseTeamPersonInCharge(); 
                break;
            case self::REPAIRING:
                $department = self::DEPARTMENT_WAREHOUSE;
                $person_in_charge = $this->getDefaultWarehouseTeamPersonInCharge(); 
                break;
            case self::REPAIRED:
                $department = self::DEPARTMENT_SALE;
                $person_in_charge = $this->getDefaultSaleTeamPersonInCharge();
                break;
            case self::FINISHED:
                $department = self::DEPARTMENT_SALE;
                $person_in_charge = $this->getDefaultSaleTeamPersonInCharge();
                break;
        }

        $sql = "update ".$ecs->table('erp_rma')." set last_action_time = '".gmtime()."',  department = '".$department."', person_in_charge = '".$person_in_charge."'  where rma_id = '".$rma_id."' ";
        
        $db->query($sql);

        return true;
    }

    public function getCurrentStatus($rma_id)
    {
        global $db, $ecs, $userController, $_CFG;
    
        $sql = 'select status from '.$ecs->table('erp_rma').' where rma_id = "'.$rma_id.'" ';
        return $status = $db->getOne($sql);
    }

    public function getActionLog($rma_id)
    {
        global $db, $ecs, $userController, $_CFG;
        
        $sql = 'select eral.*, au.user_name, au.avatar 
                from '.$ecs->table('erp_rma_action_log').' eral left join '. $ecs->table('admin_user').' au on (eral.user_id = au.user_id) 
                where rma_id = '.$rma_id.' order by create_time ';
        $result = $db->getAll($sql);
        
        foreach ($result as &$log) {
            $log['create_time'] = local_date($_CFG['time_format'], $log['create_time']);
            $log['step'] = $this->getStepByStatus($log['status']);
            $log['step_name'] = $this->getStepName($log['step']);
            $log['note'] =  nl2br($log['note']);
            $log['avatar'] = '<img class="img-circle profile_img" style="margin-left: 0%;" title="'.$log['user_name'].'" src="'.(empty($log['avatar']) ? '/uploads/avatars/default_60x60.png' : '/'.$log['avatar']).'" >';
        }

        $actionLog = $result;
    
        $current_group_step = '';
        $group = [];
        $current_index = 0;

        foreach ($actionLog as $newLog) {
            // echo 'log-step:'.$newLog['step'] ;
            //
            // echo 'log-id:'.$newLog['log_id'] ;
            // echo '<br>';
            $step = $newLog['step'];
            if ($step != $current_group_step) {
                $current_group_step = $step;
                $current_index = sizeof($group);
                $group_temp = [];
            }

            $group_temp[] = $newLog;

            if (!empty($group_temp)) {
                $group[$current_index] = $group_temp;
            }
        }
        // echo '<pre>';
        // print_r($grfoup);
        // exit;
        //$action_log = $group;

        return $group;
    }

    public function rmaSaveNote()
    {
        global $db, $ecs, $userController, $_CFG;
        
        $rma_id = $_REQUEST['rma_id'];
        $note = $_REQUEST['note'];
        $user_id = erp_get_admin_id();
        $currentStatus = $this->getCurrentStatus($rma_id);

        $this->rmaActionLog($rma_id, $user_id, $note, $currentStatus);
        return true;
    }

    public function rmaSaveUser()
    {
        global $db, $ecs, $userController, $_CFG;
        
        $rma_id = $_REQUEST['rma_id'];
        $user_id = $_REQUEST['user_id'];
       
        $currentStatus = $this->getCurrentStatus($rma_id);
        if ($currentStatus == self::NOTIFY_SUPPLIER) {
            $sql = 'update '.$ecs->table('erp_rma').' set goods_admin = '.$user_id.' where rma_id = '.$rma_id.' ';
            $db->query($sql);
        }

        $sql = 'update '.$ecs->table('erp_rma').' set person_in_charge = '.$user_id.' where rma_id = '.$rma_id.' ';
        $db->query($sql);
        return true;
    }

    public function updateRepairOrderStatus($repair_id, $status)
    {
        global $db, $ecs, $userController, $_CFG;

        $sql = 'update '. $ecs->table('repair_orders').' set repair_order_status = '.$status.' where repair_id = "'.$repair_id.'" ';
        $db->query($sql);

        if ($status == 1) {
            $sql = 'update '. $ecs->table('repair_orders').' set pickup_time = 0 where repair_id = "'.$repair_id.'" ';
            $db->query($sql);
        }

        return true;
    }

    public function updateRepairOrderPickupTime($repair_id)
    {
        global $db, $ecs, $userController, $_CFG;

        $sql = 'update '. $ecs->table('repair_orders').' set pickup_time = '.gmtime().' where repair_id = "'.$repair_id.'" ';
        $db->query($sql);

        return true;
    }

    public function saveStepStockLog($rma_id, $step, $status, $goods_id='null', $warehouse_id='null', $qty='null', $actg_txn_id='null')
    {
        global $db, $ecs, $userController, $_CFG;
        if (empty($actg_txn_id)) {
            $actg_txn_id = 'null';
        }
        $sql = ' insert into '. $ecs->table('erp_rma_stock_log') .' (rma_id,step,status,goods_id,warehouse_id,qty,time,actg_txn_id) values ('.$rma_id.','.$step.','.$status.','.$goods_id.','.$warehouse_id.','.$qty.','.gmtime().','.$actg_txn_id.')';
        $db->query($sql);
        return true;
    }

    public function rmaReplaceWithNewProcess($rma_id)
    {
        global $db, $ecs, $userController, $_CFG;
        
        $rma_info = $this->getRmaInfo($rma_id);
        
        $sale_warehouse_id = $this->getDefaultSaleWarehouse();
        $temp_warehouse_id = $this->getDefaultTemporaryWarehouse();
        
		//stock check for sale warehouse to temp warhouse
		$attrId=\get_attr_id($rma_info['goods_id']);
		$stock=\get_goods_stock_by_warehouse($sale_warehouse_id,$rma_info['goods_id'],$attrId);
        
		if($stock<$rma_info['qty']){
			$errmsg = '庫存不足, 請重新確認 (Requested:'.$rma_info['qty'].$sale_warehouse_id.')';
            $result['error'] = $errmsg;
            return $result;
		}

        // sale stock to temp stock
        $errmsg = $this->erpController->stockTransfer($sale_warehouse_id, $temp_warehouse_id, [$rma_info['goods_id']=>$rma_info['qty']], $rma_id);
        $this->saveStepStockLog($rma_id, 1, $rma_info['status'], $rma_info['goods_id'], $sale_warehouse_id, $rma_info['qty']*-1);
        $this->saveStepStockLog($rma_id, 1, $rma_info['status'], $rma_info['goods_id'], $temp_warehouse_id, $rma_info['qty']);

        // temp -1;
        $errmsg = $this->erpController->makeRODeliveryOrder($rma_info['repair_sn'], $rma_id);
        if ($errmsg !== true) {
            $result['error'] = $errmsg;
            return $result;
        }
        
        $param = [
            'sales_order_id'    => $rma_info['order_id'],
            'rma_id'            => $rma_id,
            'remark'            => "維修單 $rma_info[repair_sn] 直接換貨",
            'amount'			=> $rma_info['po_unit_value'],
        ];
        if (!empty($rma_info['supplier_id'])) {
            $param['supplier_id'] = $rma_info['supplier_id'];
        }
        $actg_txn_id = $this->accountingController->insertRMATransactions($param, true, true);

        $this->saveStepStockLog($rma_id, 1, $rma_info['status'], $rma_info['goods_id'], $temp_warehouse_id, $rma_info['qty']*-1,$actg_txn_id);
        
        $sql = 'update '. $ecs->table('erp_rma').' set replace_with_new = 1 where rma_id = "'.$rma_id.'" ';
        $db->query($sql);
       
        // update repair order status to 4
        $this->updateRepairOrderStatus($rma_info['repair_id'], 4);
 
        $result = true;
        if (!$errmsg == true) {
            $result['error'] = $errmsg;
        }
        
        return $result;
    }

    public function rmaCheckSaveStepStockLog($rma_id, $step, $status)
    {
        global $db, $ecs, $userController, $_CFG;
        
        $sql = 'select count(*) from '.$ecs->table('erp_rma_stock_log').' where rma_id = '.$rma_id.' and step = '.$step.' and status = '.$status.' ';
        $count = $db->getOne($sql);

        if ($count == 0) {
            $this->saveStepStockLog($rma_id, $step, $status);
            //$sql = ' insert into '. $ecs->table('erp_rma_stock_log') .' (rma_id,step,status,goods_id,warehouse_id,qty,time) values ('.$rma_id.','.$step.','.$status.','.$goods_id.','.$warehouse_id.','.$qty.','.gmtime().')';
            //$db->query($sql);
        }
    }

    public function rmaCustomerReadyToPick($rma_id)
    {
        global $db, $ecs, $userController, $_CFG;
        $rma_info = $this->getRmaInfo($rma_id);

        $errmsg = true;
        if (!empty($rma_info['repair_sn'])) {
            $errmsg =  $this->erpController->makeRODeliveryOrder($rma_info['repair_sn'], $rma_id);
        }

        if ($errmsg !== true) {
            $result['error'] = $errmsg;
            return $result;
        } else {
            $param = [
                'sales_order_id'    => $rma_info['order_id'],
                'rma_id'            => $rma_id,
                'remark'            => "維修單 $rma_info[repair_sn] 完成維修出貨",
                'amount'			=> $rma_info['po_unit_value'],
            ];
            $actg_txn_id = $this->accountingController->insertRMATransactions($param, true);
            if (!$actg_txn_id) {
                $result['error'] = 'Empty actg_txn_id';
                return $result;
            }
            return $actg_txn_id;
        }

        return true;
    }

    public function getGoodsUserAdmin($goods_id)
    {
        global $db, $ecs, $userController, $_CFG;

        $sql = "SELECT GROUP_CONCAT(distinct(au.user_id) ORDER BY au.user_id DESC) as user_ids, GROUP_CONCAT(distinct(au.user_name) ORDER BY au.user_id DESC) as user_names, GROUP_CONCAT(distinct(esa.supplier_id)) as supplier_ids, egs.goods_id ";
        $sql .= "FROM ". $ecs->table('erp_goods_supplier') ." as egs ";
        $sql .= "LEFT JOIN ". $ecs->table('erp_supplier_admin') ." as esa ON egs.supplier_id = esa.supplier_id ";
        $sql .= "LEFT JOIN ".$ecs->table('admin_user') ." as au ON esa.admin_id = au.user_id ";
        $sql .= "WHERE egs.goods_id". db_create_in($goods_id) ." AND (au.user_id > 0 AND au.user_id != 1) GROUP BY egs.goods_id ORDER BY user_ids DESC";

        $res = $db->getAll($sql);
        foreach ($res as $key => $value) {
            $goods[$value['goods_id']] = $value;
        }

        $task_admin = [];
        foreach ($goods as $key => &$value) {
            if(empty($value['user_ids'])) continue;
            
            $admins = explode(",",$value['user_names']);
            $admins_ids = explode(",",$value['user_ids']);
            foreach ($admins as $k => $admin) {
                $value['admins'][$admins_ids[$k]] = $admin;
            }

            // We only create task on first admin (Max(admin_id))
            $first_admin_id = current(array_keys($value['admins']));
            $first_admin = $value['admins'][$first_admin_id];
            $value['first_admin_id'] = $first_admin_id;
            $value['first_admin_name'] = $first_admin;
        }

        return $first_admin_id;
    }

    public function getUserInfo($user_id)
    {
        global $db, $ecs, $userController, $_CFG;

        if ($user_id > 0) {
            $sql = "select user_name,avatar  from ".$ecs->table('admin_user') ." where user_id = ".$user_id." ";
            $res = $db->getRow($sql);
        } else {
            $res = false;
        }
        
        return $res;
    }

    public function rmaCheckSubmit()
    {
        global $db, $ecs, $userController, $_CFG;
       
        $erpController = new ErpController();
        $rma_id = $_REQUEST['rma_id'];
        $note = $_REQUEST['note'];
        $user_id = $_REQUEST['user_id'];
        $repair_warehouse_id = $_REQUEST['repair_warehouse_id'];
        $shop_warehouse_id = $erpController->getShopWarehouseId();
        
        $final_note = '';
        
        $rma_info = $this->getRmaInfo($rma_id);

        if ($_REQUEST['replace_with_new'] == 'true') {
            $result = $this->rmaReplaceWithNewProcess($rma_id);
            if (isset($result['error'])){
                return $result;
            }
            $final_note .= '(已選取)先換一個全新的給客人,有問題的產品再之後維修'."\n";
        } else {
            // update repair order status to 1
            $this->updateRepairOrderStatus($rma_info['repair_id'], 1);
        }

        $currentStatus = $this->getCurrentStatus($rma_id);
        $temp_warehouse_id = $this->getDefaultTemporaryWarehouse();
        $step = 1;

        // insert accounting record
        $param = [
            'sales_order_id'    => $rma_info['order_id'],
            'rma_id'            => $rma_id,
            'remark'            => "維修單 $rma_info[repair_sn] 完成維修出貨",
            'amount'			=> $rma_info['po_unit_value'],
        ];
        switch ($_REQUEST['repair_type']) {
            case 'repair_by_supplier':
                // check if have transfer order for repair
                $warehouseTransferController = new WarehouseTransferController(); 

                $sql = 'update '. $ecs->table('erp_rma').' set repair_handle_type = '.self::REPAIR_HANDLE_TYPE_SUPPLIER.' where rma_id = "'.$rma_id.'" ';
                $db->query($sql);

                $final_note .= (empty($note)? '': '備註:'.$note.''."\n").'供應商維修處理';
                $this->rmaCheckSaveStepStockLog($rma_id, $step, $currentStatus);
                //$result = $this->rmaChangeStatus($rma_id, self::NOTIFY_SUPPLIER);
                //$status = self::NOTIFY_SUPPLIER;

                // create transfer order
                // set warehouse from to 
                $sql = 'update '. $ecs->table('erp_rma').' set warehouse_id = '.$shop_warehouse_id.' , repair_warehouse_id = '.$repair_warehouse_id.' where rma_id = "'.$rma_id.'" ';
                $db->query($sql);
                $transfer_id = $warehouseTransferController->warehouseTransferCreateByRmaTransfer($rma_id,'to_repair_center',false);

                $sql = 'update '. $ecs->table('erp_rma').' set repair_transfer_id = '.$transfer_id.' where rma_id = "'.$rma_id.'" ';
                $db->query($sql);
                
                $final_note .= (empty($note)? '': '備註:'.$note.''."\n").' 調送倉處理';
                $this->rmaCheckSaveStepStockLog($rma_id, $step, $currentStatus);
                $result = $this->rmaChangeStatus($rma_id, self::TRANSFERRING_TO_REPAIR_CENTER);
                $status = self::TRANSFERRING_TO_REPAIR_CENTER;

                break;
            case 'repair_by_ourselves':
                $sql = 'update '. $ecs->table('erp_rma').' set repair_handle_type = '.self::REPAIR_HANDLE_TYPE_OURSELVES.' where rma_id = "'.$rma_id.'" ';
                $db->query($sql);

                $final_note .= '自行維修處理'.(empty($note)? '': '('.$note.')');
                $this->rmaCheckSaveStepStockLog($rma_id, $step, $currentStatus);
                $result = $this->rmaChangeStatus($rma_id, self::REPAIRING);
                $status = self::REPAIRING;
                break;
            case 'buy_parts_handle':
                // check temp stock 
                $attrId=\get_attr_id($rma_info['goods_id']);
        		$stock=\get_goods_stock_by_warehouse($temp_warehouse_id,$rma_info['goods_id'],$attrId);
        		if($stock<$rma_info['qty']){
        			$errmsg = '暫存庫存不足, 請重新確認 (Requested:'.$rma_info['qty'].$temp_warehouse_id.')';
                    $result['error'] = $errmsg;
                    return $result;
        		}

                $sql = 'update '. $ecs->table('erp_rma').' set repair_handle_type = '.self::REPAIR_HANDLE_TYPE_BUY_PARTS.' where rma_id = "'.$rma_id.'" ';
                $db->query($sql);

                $final_note .= '顧客購買零件處理'.(empty($note)? '': '('.$note.')');
                $actg_txn_id = $this->rmaCustomerReadyToPick($rma_id);
                $this->saveStepStockLog($rma_id, $step, $rma_info['status'], $rma_info['goods_id'], $temp_warehouse_id, $rma_info['qty']*-1,$actg_txn_id);
                $result = $this->rmaChangeStatus($rma_id, self::FINISHED);
                $this->updateRepairOrderStatus($rma_info['repair_id'], 4);
                $this->updateRepairOrderPickupTime($rma_info['repair_id']);
                $status = self::FINISHED;
                break;
            case 'no_need_to_repair':
                // check temp stock 
                $attrId=\get_attr_id($rma_info['goods_id']);
        		$stock=\get_goods_stock_by_warehouse($temp_warehouse_id,$rma_info['goods_id'],$attrId);
        		if($stock<$rma_info['qty']){
        			$errmsg = '暫存庫存不足, 請重新確認 (Requested:'.$rma_info['qty'].$temp_warehouse_id.')';
                    $result['error'] = $errmsg;
                    return $result;
        		}

                $sql = 'update '. $ecs->table('erp_rma').' set repair_handle_type = '.self::REPAIR_HANDLE_TYPE_NO_NEED_TO_REPAIR.' where rma_id = "'.$rma_id.'" ';
                $db->query($sql);
                $final_note .= '不需要維修處理'.(empty($note)? '': '('.$note.')');

                if ($rma_info['channel'] == 1) { // for replacement
                    $result = $this->rmaChangeStatus($rma_id, self::REPAIRED);                    
                    break;
                } else {
                    $actg_txn_id = $this->rmaCustomerReadyToPick($rma_id);
                    $this->saveStepStockLog($rma_id, $step, $rma_info['status'], $rma_info['goods_id'], $temp_warehouse_id, $rma_info['qty']*-1,$actg_txn_id);
                    $result = $this->rmaChangeStatus($rma_id, self::FINISHED);
                    $this->updateRepairOrderStatus($rma_info['repair_id'], 4);
                    $status = self::FINISHED;
                    break;
                }
            case 'write_off':
                // temp warehouse to supplier
                $temp_warehouse_id = $this->getDefaultTemporaryWarehouse();

                // check temp stock 
                $attrId=\get_attr_id($rma_info['goods_id']);
        		$stock=\get_goods_stock_by_warehouse($temp_warehouse_id,$rma_info['goods_id'],$attrId);
        		if($stock<$rma_info['qty']){
        			$errmsg = '暫存庫存不足, 請重新確認 (Requested:'.$rma_info['qty'].$temp_warehouse_id.')';
                    $result['error'] = $errmsg;
                    return $result;
        		}
                
                $errmsg = $this->erpController->makeRMAWriteOffDeliveryOrder($temp_warehouse_id, $rma_info['supplier_id'], $rma_info['goods_id'], $rma_info['qty'], $rma_id);

                if ($errmsg !== true) {
                    $result['error'] = $errmsg;
                    return $result;
                } else {
                    $sql = 'update '. $ecs->table('erp_rma').' set repair_handle_type = '.self::REPAIR_HANDLE_TYPE_WRITE_OFF.' where rma_id = "'.$rma_id.'" ';
                    $db->query($sql);
                    $final_note .= '直接報廢'.(empty($note)? '': '('.$note.')');

                    $param = [
                        'sales_order_id'    => $rma_info['order_id'],
                        'rma_id'            => $rma_id,
                        'remark'            => "維修單 $rma_info[repair_sn] 直接報廢",
                        'amount'			=> $rma_info['po_unit_value'],
                    ];
                    if (!empty($rma_info['supplier_id'])) {
                        $param['supplier_id'] = $rma_info['supplier_id'];
                    }
                    $actg_txn_id = $this->accountingController->insertWriteOffTransactions($param);
                    //$result = $this->rmaCustomerReadyToPick($rma_id);
                    $this->saveStepStockLog($rma_id, $step, $rma_info['status'], $rma_info['goods_id'], $temp_warehouse_id, $rma_info['qty']*-1,$actg_txn_id);

                    // temp to write off
                    $result = $this->rmaChangeStatus($rma_id, self::FINISHED);
                    if ($rma_info['channel'] == 0) {
                        $this->updateRepairOrderStatus($rma_info['repair_id'], 4);
                    }
                    $status = self::FINISHED;
                    break;
                }
        }
        
        $this->rmaActionLog($rma_id, $user_id, $final_note, $currentStatus);

        return true;
    }

    public function rmaNotifySpecialSubmit()
    {
        global $db, $ecs, $userController, $_CFG;
        
        $rma_id = $_REQUEST['rma_id'];
        $note = $_REQUEST['note'];
        $user_id = $_REQUEST['user_id'];

        $rma_info = $this->getRmaInfo($rma_id);
        // temp warehouse to supplier
        //$temp_warehouse_id = $this->getDefaultTemporaryWarehouse();
        $temp_warehouse_id = $this->getRepairTemporaryWarehouse($rma_info['repair_warehouse_id']);
         
        $currentStatus = $this->getCurrentStatus($rma_id);

        // check temp stock 
        $attrId=\get_attr_id($rma_info['goods_id']);
        $stock=\get_goods_stock_by_warehouse($temp_warehouse_id,$rma_info['goods_id'],$attrId);
        if($stock<$rma_info['qty']){
            $errmsg = '暫存庫存不足, 請重新確認 (Requested:'.$rma_info['qty'].$temp_warehouse_id.')';
            $result['error'] = $errmsg;
            return $result;
        }
        
        $errmsg = $this->erpController->makeRMAWriteOffDeliveryOrder($temp_warehouse_id, $rma_info['supplier_id'], $rma_info['goods_id'], $rma_info['qty'], $rma_id);

        if ($errmsg !== true) {
            $result['error'] = $errmsg;
            return $result;
        } else {
            $sql = 'update '. $ecs->table('erp_rma').' set repair_handle_type = '.self::REPAIR_HANDLE_TYPE_WRITE_OFF.' where rma_id = "'.$rma_id.'" ';
            $db->query($sql);
            $final_note .= '直接報廢'.(empty($note)? '': '('.$note.')');

            $param = [
                'sales_order_id'    => $rma_info['order_id'],
                'rma_id'            => $rma_id,
                'remark'            => "維修單 $rma_info[repair_sn] 直接報廢",
                'amount'			=> $rma_info['po_unit_value'],
            ];
            if (!empty($rma_info['supplier_id'])) {
                $param['supplier_id'] = $rma_info['supplier_id'];
            }
            $actg_txn_id = $this->accountingController->insertWriteOffTransactions($param);
            //$result = $this->rmaCustomerReadyToPick($rma_id);
            $this->saveStepStockLog($rma_id, 2, $rma_info['status'], $rma_info['goods_id'], $temp_warehouse_id, $rma_info['qty']*-1,$actg_txn_id);

            // temp to write off
            $result = $this->rmaChangeStatus($rma_id, self::FINISHED);
            if ($rma_info['channel'] == 0) {
                $this->updateRepairOrderStatus($rma_info['repair_id'], 4);
            }
            $status = self::FINISHED;
            $this->rmaActionLog($rma_id, $user_id, $final_note, $currentStatus);
            return true;
        }
    }

    public function rmaNotifySubmit()
    {
        global $db, $ecs, $userController, $_CFG;
        $rma_id = $_REQUEST['rma_id'];
        $note = $_REQUEST['note'];
        $user_id = $_REQUEST['user_id'];
        $supplier_id = $_REQUEST['supplier_id'];
        $final_note = '';

        $rma_info = $this->getRmaInfo($rma_id);

        if ($_REQUEST['notified'] == true) {
            $currentStatus = $this->getCurrentStatus($rma_id);
            $this->rmaCheckSaveStepStockLog($rma_id, 2, $currentStatus);
            $this->rmaChangeStatus($rma_id, self::WAITING_FOR_RETURN_TO_SUPPLIER);
            $final_note .= '已經通知供應商回收'.(empty($note)? '': '('.$note.')');
            $sql = 'update '. $ecs->table('erp_rma') .' set supplier_id = "'.$supplier_id.'" where rma_id = "'.$rma_id.'" ';
            $db->query($sql);
            $status = self::NOTIFY_SUPPLIER;
            $this->rmaActionLog($rma_id, $user_id, $final_note, $currentStatus);
        }
        return true;
    }

    public function rmaSupplierPickedSubmit()
    {
        global $db, $ecs, $userController, $_CFG;
        $rma_id = $_REQUEST['rma_id'];
        $note = $_REQUEST['note'];
        $user_id = $_REQUEST['user_id'];
        $supplier_picked = $_REQUEST['supplier_picked'];
        $final_note = '';
        $rma_info = $this->getRmaInfo($rma_id);

        if ($_REQUEST['supplier_picked'] == 'true') {
            $final_note .= '確定供應商已經取貨'.(empty($note)? '': '('.$note.')');
            $rma_info = $this->getRmaInfo($rma_id);

            // temp repair warehouse id to supplier
            $rmaController = new RmaController();
            $temp_warehouse_id =  $rmaController->getRepairTemporaryWarehouse($rma_info['repair_warehouse_id']);
         
            $errmsg = $this->erpController->makeRMADeliveryOrder($temp_warehouse_id, $rma_info['supplier_id'], $rma_info['goods_id'], $rma_info['qty'], $rma_id);

            if ($errmsg !== true) {
                $result['error'] = $errmsg;
                return $result;
            } else {
                $currentStatus = $this->getCurrentStatus($rma_id);
                $this->rmaChangeStatus($rma_id, self::REPAIRING);
                // insert accounting record
                $param = [
                    'sales_order_id'    => $rma_info['order_id'],
                    'rma_id'            => $rma_id,
                    'supplier_id'       => $rma_info['supplier_id'],
                    'remark'            => "維修單 $rma_info[repair_sn] 供應商取貨",
                    'amount'			=> $rma_info['po_unit_value'],
                ];
                $actg_txn_id = $this->accountingController->insertRMATransactions($param, true);
                $this->saveStepStockLog($rma_id, 3, $rma_info['status'], $rma_info['goods_id'], $temp_warehouse_id, $rma_info['qty'] * -1,$actg_txn_id);
               
                $status = self::NOTIFY_SUPPLIER;
                $this->rmaActionLog($rma_id, $user_id, $final_note, $currentStatus);
                return true;
            }
        }
    }

    public function rmaSupplierReturnedSubmit()
    {
        global $db, $ecs, $userController, $_CFG;

        require_once(ROOT_PATH .ADMIN_PATH .'/includes/ERP/lib_erp_warehouse.php');
        require_once(ROOT_PATH .ADMIN_PATH .'/includes/ERP/lib_erp_goods.php');
        require_once(ROOT_PATH .ADMIN_PATH .'/includes/ERP/lib_erp_goods_attr.php');

        $rma_id = $_REQUEST['rma_id'];
        $note = $_REQUEST['note'];
        $user_id = $_REQUEST['user_id'];
        $supplier_returned = $_REQUEST['supplier_returned'];
        $final_note = '';
        $rma_info = $this->getRmaInfo($rma_id);

        if ($_REQUEST['supplier_returned'] == 'true') {
            $db->query('START TRANSACTION');
            $final_note .= '維修完成(已經取回維修產品)'.(empty($note)? '': '('.$note.')');
            $rma_info = $this->getRmaInfo($rma_id);

            if ($rma_info['repair_handle_type'] == 1) {
                $final_note = '自行維修完成'.(empty($note)? '': '('.$note.')');
                $currentStatus = $this->getCurrentStatus($rma_id);
                $this->saveStepStockLog($rma_id, 4, $rma_info['status']);
                $this->rmaChangeStatus($rma_id, self::REPAIRED);
                $this->rmaActionLog($rma_id, $user_id, $final_note, $currentStatus);
                $db->query('COMMIT');
                return true;
            }

            // supplier to temp warehouse
            $rmaController = new RmaController();
            $temp_warehouse_id =  $rmaController->getRepairTemporaryWarehouse($rma_info['repair_warehouse_id']);
            $order_id = $_REQUEST['order_id'];
            $warehousing_from = $rma_info['supplier_name'];
            $warehousing_style_id = 6;
            $warehousing_id=add_warehousing($warehouse_id);

            // get warehouseing sn
            $sql = 'select warehousing_sn from '.$ecs->table('erp_warehousing').' where warehousing_id = '.$warehousing_id.' ';
            $warehousing_sn = $db->getOne($sql);

            $approved_remark = 'RMA維修完成入暫存倉';
            $sql="update ".$ecs->table('erp_warehousing')." set order_id = '".$rma_info['repair_id']."', warehousing_style_id = '".$warehousing_style_id."', warehouse_id = '".$temp_warehouse_id."' ,warehousing_from='".$warehousing_from."',  approved_by='1',  approve_time= '".gmtime()."', approve_remark= '".$approved_remark."', last_act_time= '".gmtime()."', status = 3 where warehousing_id='".$warehousing_id."'";
            $db->query($sql);

            $goods_id = $rma_info['goods_id'];
            $received_qty = $rma_info['qty'];
            $attr_id=get_attr_id($goods_id);
            $sql="insert into ".$ecs->table('erp_warehousing_item')." set warehousing_id='".$warehousing_id."',goods_id='".$goods_id."',attr_id='".$attr_id."', expected_qty='0',received_qty='".$received_qty."'";
            $db->query($sql);
            
            //lock_table($warehousing_id,'warehousing','post_to_approve');
            //$sql="update ".$ecs->table('erp_warehousing')." set status='2',post_by='".erp_get_admin_id()."',post_time='".gmtime()."' where warehousing_id='".$warehousing_id."'";
            //$db->query($sql);

            if (!add_stock_by_warehousing($warehousing_id)) {
                $db->query('ROLLBACK');
                return false;
            } else {
                $currentStatus = $this->getCurrentStatus($rma_id);
                $this->rmaChangeStatus($rma_id, self::REPAIRED);
                $this->rmaActionLog($rma_id, $user_id, $final_note, $currentStatus);
                
                // insert accounting record
                $param = [
                    'sales_order_id'    => $rma_info['order_id'],
                    'rma_id'            => $rma_id,
                    'supplier_id'       => $rma_info['supplier_id'],
                    'remark'            => "維修單 $rma_info[repair_sn] 供應商寄回",
                    'amount'			=> $rma_info['po_unit_value'],
                ];
                $actg_txn_id = $this->accountingController->insertRMATransactions($param);
                $this->saveStepStockLog($rma_id, 4, $rma_info['status'], $rma_info['goods_id'], $temp_warehouse_id, $rma_info['qty'],$actg_txn_id);
                // update rma id
                $sql = 'update '.$ecs->table('erp_stock').' set order_sn = "'.$rma_info['repair_sn'].'", rma_id = '.$rma_id.' where w_d_sn = "'.$warehousing_sn.'" ';
                $db->query($sql);
                
                $db->query('COMMIT');
                return true;
            }
        }
    }

    public function rmaAfterRepairedSubmit()
    {
        global $db, $ecs, $userController, $_CFG;

        require_once(ROOT_PATH .ADMIN_PATH .'/includes/ERP/lib_erp_warehouse.php');
        require_once(ROOT_PATH .ADMIN_PATH .'/includes/ERP/lib_erp_goods.php');
        require_once(ROOT_PATH .ADMIN_PATH .'/includes/ERP/lib_erp_goods_attr.php');

        $rma_id = $_REQUEST['rma_id'];
        $note = $_REQUEST['note'];
        $user_id = $_REQUEST['user_id'];
        $returned_to = $_REQUEST['returned_to'];
        $final_note = '';
        $rma_info = $this->getRmaInfo($rma_id);

        if ($returned_to == 'sale') {
            $action_note = ' 轉至銷售倉銷售';
        } elseif ($returned_to == 'customer') {
            $action_note = ' 維修產品出貨給客人';
        }

        $final_note .= $action_note.(empty($note)? '': '('.$note.')');

        if ($returned_to == 'sale') {
            // temp warehouse to sale warehouse (repaired warehouse id)

            //$sale_warehouse_id = $this->getDefaultSaleWarehouse();
            //$temp_warehouse_id = $this->getDefaultTemporaryWarehouse();
            $rmaController = new RmaController();
            $sale_warehouse_id =  $rma_info['repair_warehouse_id'];
            $temp_warehouse_id =  $rmaController->getRepairTemporaryWarehouse($rma_info['repair_warehouse_id']);
            
            //  temp stock to sale stock
            $errmsg = $this->erpController->stockTransfer($temp_warehouse_id, $sale_warehouse_id, [$rma_info['goods_id']=>$rma_info['qty']], $rma_id);
            if ($errmsg !== true) {
                $result['error'] = $errmsg;
                return $result;
            } else {
                $currentStatus = $this->getCurrentStatus($rma_id);
                $this->rmaActionLog($rma_id, $user_id, $final_note, $currentStatus);
                $this->rmaChangeStatus($rma_id, self::FINISHED);
                //$this->updateRepairOrderStatus($rma_info['repair_id'], 1);
                // insert accounting record
                $param = [
                    'sales_order_id'    => $rma_info['order_id'],
                    'rma_id'            => $rma_id,
                    'remark'            => "維修單 $rma_info[repair_sn] 完成維修",
                    'amount'			=> $rma_info['po_unit_value'],
                ];
                $actg_txn_id = $this->accountingController->insertRMATransactions($param, true);
                $this->saveStepStockLog($rma_id, 5, $rma_info['status'], $rma_info['goods_id'], $temp_warehouse_id, $rma_info['qty']*-1,$actg_txn_id);
                $param = [
                    'sales_order_id'    => $rma_info['order_id'],
                    'rma_id'            => $rma_id,
                    'remark'            => "維修單 $rma_info[repair_sn] 轉入銷售倉",
                    'amount'			=> $rma_info['po_unit_value'],
                ];
                $actg_txn_id = $this->accountingController->insertRMATransactions($param, false, true);
                $this->saveStepStockLog($rma_id, 5, $rma_info['status'], $rma_info['goods_id'], $sale_warehouse_id, $rma_info['qty'], $actg_txn_id);
                return true;
            }
        } elseif ($returned_to == 'customer') {
            // temp warehouse to customer
            $actg_txn_id = $this->rmaCustomerReadyToPick($rma_id);
            if (isset($actg_txn_id['error'])) {
                return $actg_txn_id;
            } else {
                $temp_warehouse_id = $this->getDefaultTemporaryWarehouse();
                $currentStatus = $this->getCurrentStatus($rma_id);
                $this->updateRepairOrderStatus($rma_info['repair_id'], 4);
                $result = $this->rmaChangeStatus($rma_id, self::FINISHED);
                $this->rmaActionLog($rma_id, $user_id, $final_note, $currentStatus);
                $this->saveStepStockLog($rma_id, 5, $rma_info['status'], $rma_info['goods_id'], $temp_warehouse_id, $rma_info['qty']*-1,$actg_txn_id);
                return true;
            }
        } elseif ($returned_to == 'original_shop') {

            // first return to original shop
            $final_note = '';
            $rma_info = $this->getRmaInfo($rma_id);

            // create transfer order
            $warehouseTransferController = new WarehouseTransferController();
            $transfer_id = $warehouseTransferController->warehouseTransferCreateByRmaTransfer($rma_id,'to_shop');

            $sql = 'update '. $ecs->table('erp_rma').' set repaired_transfer_id = '.$transfer_id.' where rma_id = "'.$rma_id.'" ';
            $db->query($sql);

            $final_note .= (empty($note)? '': '備註:'.$note.''."\n").'調貨-送回 '.$rma_info['warehouse_name'];
            $result = $this->rmaChangeStatus($rma_id, self::TRANSFERRING_TO_SHOP);
            $currentStatus = $this->getCurrentStatus($rma_id);
            $this->rmaActionLog($rma_id, $user_id, $final_note, $currentStatus);
        }
    }

    public function rmaAfterRepairedSpecialSubmit()
    {
        global $db, $ecs, $userController, $_CFG;

        $rma_id = $_REQUEST['rma_id'];
        $note = $_REQUEST['note'];
        $user_id = $_REQUEST['user_id'];
        $special_case_1 = $_REQUEST['special_case_1'];
        $special_case_2 = $_REQUEST['special_case_2'];
        $final_note = '';
        $rma_info = $this->getRmaInfo($rma_id);

        if ($special_case_1 == 'true') {
            $final_note .= '無法維修,給顧客退款'."\n";
        }

        if ($special_case_2 == 'true') {
            $final_note .= '無法維修,產品額外處理'."\n";
        }

        $sql="update ".$ecs->table('erp_rma')." set special_handle = 1 where rma_id='".$rma_id."'";
        $db->query($sql);
        $this->saveStepStockLog($rma_id, 5, $rma_info['status']);
        $this->rmaChangeStatus($rma_id, self::FINISHED);
        $currentStatus = $this->getCurrentStatus($rma_id);
        $this->rmaActionLog($rma_id, $user_id, $final_note, $currentStatus);
        return true;
    }

    public function rmaSpecialReplaceWithNewSubmit()
    {
        global $db, $ecs, $userController, $_CFG;

        $rma_id = $_REQUEST['rma_id'];
        $user_id = $_REQUEST['user_id'];
        $replace_with_new = $_REQUEST['replace_with_new'];
      
        $final_note = '';
        $rma_info = $this->getRmaInfo($rma_id);

        if ($replace_with_new == 'true') {
            $result = $this->rmaReplaceWithNewProcess($rma_id);
            if (isset($result['error'])){
                return $result;
            }
            $currentStatus = $this->getCurrentStatus($rma_id);
            $final_note .= '(已選取)先換一個全新的給客人,有問題的產品再之後維修'."\n";
            $this->rmaActionLog($rma_id, $user_id, $final_note, $currentStatus);
        }

        return true;
    }

    public function canBackToStep1($rma_id)
    {
        global $db, $ecs, $userController, $_CFG;

        $sql = 'select count(*) from '.$ecs->table('erp_rma_stock_log').' ersl where rma_id = '.$rma_id.' ';
        $count = $db->getOne($sql);

        if ($count > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function rmaReversePreviousStepSubmit()
    {
        global $db, $ecs, $userController, $_CFG;

        $rma_id = $_REQUEST['rma_id'];
        $user_id = erp_get_admin_id();
        $rma_info = $this->getRmaInfo($rma_id);

        $currentStatus = $this->getCurrentStatus($rma_id);
        $previousStep = $this->getPreviousStep($rma_id);
        $warehouseTransferController = new warehouseTransferController();

        $db->query('START TRANSACTION');
        if (empty($previousStep)) {
            // back to step 1
            $final_note = 'Reverse to step 1 ('.self::$statusValues[0].')';
            $this->rmaActionLog($rma_id, $user_id, $final_note, $currentStatus);
            $this->rmaChangeStatus($rma_id, 0);
            return true;
        }

        $previousStepFirstStatus = $this->getPreviousStepFirstStatus($rma_id, $previousStep);

        if ($previousStepFirstStatus == 0) {
            $sql = 'update '. $ecs->table('erp_rma').' set repair_handle_type = null where rma_id = "'.$rma_id.'" ';
            $db->query($sql);
            
            $sql = 'update '. $ecs->table('erp_rma').' set replace_with_new = 0 where rma_id = "'.$rma_id.'" ';
            $db->query($sql);

            if ($rma_info['channel'] == self::CHANNEL_REPAIR) {
                $this->updateRepairOrderStatus($rma_info['repair_id'], 1);
            }

            // check transfer order if shipped
            $repair_transfer_status = $warehouseTransferController->getCurrentStatus($rma_info['repair_transfer_id']);
            $transfering_status = [$warehouseTransferController::TRANSFER_STOCK_IN,$warehouseTransferController::TRANSFERING,$warehouseTransferController::AWAITING_TRANSPORT];
            //if ($repair_transfer_status != $warehouseTransferController::CREATING) {
            if (in_array($repair_transfer_status,$transfering_status)) {
                $result['error'] = '調貨處理中,不能返回';
                $db->query('ROLLBACK');
                return $result;
            } else if ($repair_transfer_status == $warehouseTransferController::RECEIVED) {
                $result['error'] = '調貨已完成,不能返回';
                $db->query('ROLLBACK');
                return $result;
            } else {
                if (!empty($rma_info['repair_transfer_id'])) {
                    $warehouseTransferController->transferDeleteForRMA($rma_info['repair_transfer_id'],$rma_id);
                    $sql = 'update '. $ecs->table('erp_rma').' set repair_warehouse_id = warehouse_id, repair_transfer_id = null where rma_id = "'.$rma_id.'" ';
                    $db->query($sql);
                }
            }

            if ($rma_info['channel'] == self::CHANNEL_REPAIR) {
                $this->updateRepairOrderStatus($rma_info['repair_id'], 1);
            }
        }

        if ($previousStepFirstStatus == 4) {
            $sql = 'update '. $ecs->table('erp_rma').' set special_handle = 0 where rma_id = "'.$rma_id.'" ';
            $db->query($sql);
        }
   
        $final_note = 'Reverse to step '.$previousStep.'('.self::$statusValues[$previousStepFirstStatus].')';

        $step_stock_history = $this->getStepStockHistory($rma_id, $previousStep);

        $actg_txn_ids = $db->getCol("SELECT DISTINCT actg_txn_id FROM " . $ecs->table("erp_rma_stock_log") . " WHERE rma_id = $rma_id AND step = $previousStep");
        foreach ($actg_txn_ids as $actg_txn_id) {
            if (empty($actg_txn_id)) continue;
            $this->accountingController->reverseAccountingTransaction(['actg_txn_id' => $actg_txn_id]);
        }

        foreach ($step_stock_history as $history) {
            if (empty($history['qty'])) {
                continue;
            }
 
            //$final_note .= '維修完成(已經取回維修產品)'.(empty($note)? '': '('.$note.')');
            $rma_info = $this->getRmaInfo($rma_id);

            if ($history['qty'] < 0) {
                //add warehousing  (in)
                $warehouse_id = $history['warehouse_id'];
                $order_id = 0;
                $warehousing_from = 'RMA:'.$rma_info['rma_id'].'(Reverse)';
                $warehousing_style_id = 4;
                $warehousing_id=add_warehousing($warehouse_id);
                
                // get warehouseing sn
                $sql = 'select warehousing_sn from '.$ecs->table('erp_warehousing').' where warehousing_id = '.$warehousing_id.' ';
                $warehousing_sn = $db->getOne($sql);
                
                $approved_remark = 'RMA:'.$rma_info['rma_id'].'(Reverse)';
                $sql="update ".$ecs->table('erp_warehousing')." set order_id = '0', warehousing_style_id = '".$warehousing_style_id."' ,warehousing_from='".$warehousing_from."',  approved_by='1',  approve_time= '".gmtime()."', approve_remark= '".$approved_remark."', last_act_time= '".gmtime()."', status = 3 where warehousing_id='".$warehousing_id."'";
                $db->query($sql);
                
                $goods_id = $history['goods_id'];
                $received_qty = $history['qty']  * -1;
                $attr_id=get_attr_id($goods_id);
                $sql="insert into ".$ecs->table('erp_warehousing_item')." set warehousing_id='".$warehousing_id."',goods_id='".$goods_id."',attr_id='".$attr_id."', expected_qty='0',received_qty='".$received_qty."'";
                $db->query($sql);
                
                //lock_table($warehousing_id,'warehousing','post_to_approve');
                //$sql="update ".$ecs->table('erp_warehousing')." set status='2',post_by='".erp_get_admin_id()."',post_time='".gmtime()."' where warehousing_id='".$warehousing_id."'";
                //$db->query($sql);
                
                if (!add_stock_by_warehousing($warehousing_id)) {
                    $error = true;
                    $db->query('ROLLBACK');
                    return false;
                } else {
                    // update rma id
                    $sql = 'update '.$ecs->table('erp_stock').' set rma_id = '.$rma_id.' where w_d_sn = "'.$warehousing_sn.'" ';
                    $db->query($sql);
                }
            }

            if ($history['qty'] > 0) {
                //add warehousing  (out)
                $warehouse_id = $history['warehouse_id'];
                $goods_id = $history['goods_id'];
                $delivered_qty = $history['qty'];

                //stock check
                $attrId=\get_attr_id($goods_id);
                $stock=\get_goods_stock_by_warehouse($warehouse_id, $goods_id, $attrId);
                if ($stock<$delivered_qty) {
                    $result['error'] = '庫存不足, 請重新確認 (Requested:'.$delivered_qty.$warehouse_id.')';
                    $db->query('ROLLBACK');
                    return $result;
                }
                
                $order_id = 0;
                $receiver = 'RMA:'.$rma_info['rma_id'].'(Reverse)';
                $delivery_style_id = 4;
                $delivery_id = add_delivery();
                //$deliveryInfo=\get_delivery_info($delivery_id);
                
                // get delivery sn
                $sql = 'select delivery_sn from '.$ecs->table('erp_delivery').' where delivery_id = '.$delivery_id.' ';
                $delivery_sn = $db->getOne($sql);
                
                $approved_remark = 'RMA:'.$rma_info['rma_id'].'(Reverse)';
                $sql="update ".$ecs->table('erp_delivery')." set order_id = '0', warehouse_id=".$warehouse_id.", delivery_style_id = '".$delivery_style_id."' ,delivery_to='".$receiver."',  approved_by='1',  approve_time= '".gmtime()."', approve_remark= '".$approved_remark."', post_by= '".gmtime()."', status = 3 where delivery_id='".$delivery_id."'";
                $db->query($sql);
    
                $attr_id=get_attr_id($goods_id);
                $sql="insert into ".$ecs->table('erp_delivery_item')." set delivery_id='".$delivery_id."',goods_id='".$goods_id."',attr_id='".$attr_id."', expected_qty='0',delivered_qty='".$delivered_qty."'";
                $db->query($sql);
                
                //lock_table($warehousing_id,'warehousing','post_to_approve');
                //$sql="update ".$ecs->table('erp_warehousing')." set status='2',post_by='".erp_get_admin_id()."',post_time='".gmtime()."' where warehousing_id='".$warehousing_id."'";
                //$db->query($sql);

                if (!deduct_stock_by_delivery($delivery_id)) {
                    $error = true;
                } else {
                    $error = false;
                    // update rma id
                    $sql = 'update '.$ecs->table('erp_stock').' set rma_id = '.$rma_id.' where w_d_sn = "'.$delivery_sn.'" ';
                    $db->query($sql);
                }
            }
        }

        if ($error == true) {
            $db->query('ROLLBACK');
            return false;
        } else {
            $this->rmaActionLog($rma_id, $user_id, $final_note, $currentStatus);
            $this->rmaChangeStatus($rma_id, $previousStepFirstStatus);

            // delete step status log
            $sql = "delete from ".$ecs->table('erp_rma_stock_log')." where  rma_id = ".$rma_id." and step = ".$previousStep."  ";
            $db->query($sql);
            $db->query('COMMIT');
            return true;
        }
    }

    public function getPreviousStep($rma_id)
    {
        global $db, $ecs, $userController, $_CFG;
        
        $sql = 'select step from '.$ecs->table("erp_rma_stock_log").' where rma_id = '.$rma_id.' order by step desc limit 1';
        $step = $db->getOne($sql);

        return $step;
    }

    public function getStepStockHistory($rma_id, $step)
    {
        global $db, $ecs, $userController, $_CFG;
        
        $sql = 'select rma_id, goods_id, warehouse_id, sum(qty) as qty, actg_txn_id from '.$ecs->table("erp_rma_stock_log").' where rma_id = '.$rma_id.' and step = '.$step.' group by warehouse_id ';
        $stock_history = $db->getAll($sql);
        return $stock_history;
    }

    public function getPreviousStepFirstStatus($rma_id, $step)
    {
        global $db, $ecs, $userController, $_CFG;
        
        $sql = 'select status from '.$ecs->table("erp_rma_stock_log").' where rma_id = '.$rma_id.' and step = '.$step.' order by log_id limit 1 ';
        $status = $db->getOne($sql);
        return $status;
    }

    public function rmaFollowUpRemarkSubmit()
    {
        global $db, $ecs, $userController, $_CFG;

        if (isset($_REQUEST['rma_id']) && !isset($_REQUEST['repair_id'])) {
            $repair_id = $this->getRepairIdByRmaId($_REQUEST['rma_id']);
        } else {
            $repair_id = $_REQUEST['repair_id'];
        }

        if (!empty($repair_id)) {
            $follow_up_remark = mysql_real_escape_string($_REQUEST['follow_up_remark']);
            $sql = 'update '.$ecs->table('repair_orders').' set remark2 = "'.$follow_up_remark.'" where repair_id = "'.$repair_id.'" ';
            $db->query($sql);
            return true;
        } else {
            // save to rma follow up remark field
            $follow_up_remark = mysql_real_escape_string($_REQUEST['follow_up_remark']);
            $sql = 'update '.$ecs->table('erp_rma').' set follow_up_remark = "'.$follow_up_remark.'" where rma_id = "'.$_REQUEST['rma_id'].'" ';
            $db->query($sql);
            return true;
        }
    }

    public function rmaTypeChange()
    {
        global $db, $ecs, $userController ,$_CFG;

        if (!empty($_REQUEST['rma_id']) && $_REQUEST['change_type'] != '') {
            $sql = "update ".$ecs->table('erp_rma')." set type = '".$_REQUEST['change_type']."' where rma_id = '".$_REQUEST['rma_id']."' ";
            $db->query($sql);
        }

        return true;
    }

    public function rmaActionLog($rma_id, $user_id, $note, $status)
    {
        global $db, $ecs, $userController, $_CFG;
        $note = mysql_real_escape_string($note);

        // if (is_null($user_id))
        // {
        //     $username = defined('ECS_ADMIN') ? (!empty($_REQUEST['operator']) ? $_REQUEST['operator'] : $_SESSION['admin_name']) : 'unknown';
        // }

        $sql = 'insert into '. $ecs->table('erp_rma_action_log').' (rma_id,user_id,note,create_time,status) 
                values ("'.$rma_id.'","'.$user_id.'","'.$note.'","'.gmtime().'","'.$status.'") ';
        $db->query($sql);
        return true;
    }

    public function getRmaList($is_pagination = true)
    {
        global $db, $ecs, $userController, $_CFG;

        if (!empty($_REQUEST['goods_sn'])) {
            $goods_id = $this->getGoodsIdBySn($_REQUEST['goods_sn']);
        }

        $_REQUEST['status'] = (!isset($_REQUEST['status']) || isset($_REQUEST['status']) && $_REQUEST['status'] == '-1') ? '-1' : $_REQUEST['status'];
        $_REQUEST['type'] = !isset($_REQUEST['type']) ? '1' : $_REQUEST['type'];

        // total record
        $sql = 'SELECT count(*) FROM ' . $ecs->table('erp_rma') . " er left join ". $ecs->table('repair_orders') ." ro on (er.repair_id=ro.repair_id) left join ". $ecs->table('goods') ." g on (er.goods_id=g.goods_id) WHERE 1";
        $sql .= (!isset($_REQUEST['status']) || isset($_REQUEST['status']) && $_REQUEST['status'] == '-1') ? '' : " and status = '".$_REQUEST['status']."'";
        $sql .= " and type = '".$_REQUEST['type']."'";
        $sql .= (!empty($_REQUEST['repair_sn'])) ? " and repair_sn = '".$_REQUEST['repair_sn']."'" : '' ;
        $sql .= (!empty($_REQUEST['goods_name'])) ? " and goods_name like'%".$_REQUEST['goods_name']."%'" : '' ;
        $sql .= ($_REQUEST['show_repair_order'] == 'true') ? " and er.repair_id is not null " : '' ;
        $sql .= ($_REQUEST['show_repair_order_not_pick'] == 'true') ? " and ro.pickup_time = 0 " : '' ;
        $sql .= (!isset($_REQUEST['department']) || isset($_REQUEST['department']) && $_REQUEST['department'] == '-1') ? '' : " and department = '".$_REQUEST['department']."'";
        $sql .= (!isset($_REQUEST['channel']) || isset($_REQUEST['channel']) && $_REQUEST['channel'] == '-1') ? '' : " and channel = '".$_REQUEST['channel']."'";
        $sql .= (!isset($_REQUEST['person_in_charge']) || isset($_REQUEST['person_in_charge']) && $_REQUEST['person_in_charge'] == '-1') ? '' : " and person_in_charge = '".$_REQUEST['person_in_charge']."'";
        $sql .= !isset($goods_id) ? '' : " and er.goods_id = '".$goods_id."'";
        $sql .= empty($_REQUEST['rma_id']) ? '' : " and er.rma_id = '".$_REQUEST['rma_id']."'";

        $row = $db->GetAll($sql);
        $total_count = $db->getOne($sql);

        $_REQUEST['record_count'] = $total_count;
        $_REQUEST['sort_by']    = empty($_REQUEST['sort_by']) ? 'rma_id' : trim($_REQUEST['sort_by']);
        $_REQUEST['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
        
        // pagination
        $sql =  "SELECT er.*, ro.repair_sn, ro.contact_phone, ro.contact_name, ro.contact_email, er.operator, au.avatar, au.user_name, g.goods_thumb, g.goods_sn, g.goods_name, ro.repair_fee, ro.remark2, pickup_time, repair_order_status  
                FROM " . $ecs->table('erp_rma') . " er 
                left join ". $ecs->table('repair_orders') ." ro on (er.repair_id = ro.repair_id) 
                left join ". $ecs->table('admin_user')." au on (er.person_in_charge = au.user_id) 
                left join " . $ecs->table('goods') . " g on (g.goods_id = er.goods_id)
                WHERE 1 ";

        $sql .= (!isset($_REQUEST['status']) || isset($_REQUEST['status']) && $_REQUEST['status'] == '-1') ? '' : " and status = '".$_REQUEST['status']."'";
        $sql .= " and type = '".$_REQUEST['type']."'";
        $sql .= (!empty($_REQUEST['repair_sn'])) ? " and repair_sn = '".$_REQUEST['repair_sn']."'" : '' ;
        $sql .= (!empty($_REQUEST['goods_name'])) ? " and goods_name like'%".$_REQUEST['goods_name']."%'" : '' ;
        $sql .= ($_REQUEST['show_repair_order'] == 'true') ? " and er.repair_id is not null " : '' ;
        $sql .= ($_REQUEST['show_repair_order_not_pick'] == 'true') ? " and ro.pickup_time = 0 " : '' ;
        $sql .= (!isset($_REQUEST['department']) || isset($_REQUEST['department']) && $_REQUEST['department'] == '-1') ? '' : " and department = '".$_REQUEST['department']."'";
        $sql .= (!isset($_REQUEST['channel']) || isset($_REQUEST['channel']) && $_REQUEST['channel'] == '-1') ? '' : " and channel = '".$_REQUEST['channel']."'";
        $sql .= (!isset($_REQUEST['person_in_charge']) || isset($_REQUEST['person_in_charge']) && $_REQUEST['person_in_charge'] == '-1') ? '' : " and person_in_charge = '".$_REQUEST['person_in_charge']."'";
        $sql .= !isset($goods_id) ? '' : " and er.goods_id = '".$goods_id."'";
        $sql .= empty($_REQUEST['rma_id']) ? '' : " and er.rma_id = '".$_REQUEST['rma_id']."'";

        $sql .= ' order by '. $_REQUEST['sort_by'] . ' ' .  $_REQUEST['sort_order'];
        $sql .= ($is_pagination ? " LIMIT " . $_REQUEST['start'] . ", " . $_REQUEST['page_size'] : '');
        $res = $db->getAll($sql);
        
        $statusOptions = $this->getStatusOptions();
        foreach ($res as $key => &$row) {
            // image
            $stocktake_progress_html = '<div class="widget_summary">
                    <div class="w_center w_55" style="width: 100%;">
                      <div class="progress">
                        <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" title = "'.$row['stocktake_finished_rate'].'%" style="width: '.$row['stocktake_finished_rate'].'%;">
                          <span class="sr-only">60% Complete</span>
                        </div>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                  </div>';
            // $reviw_progress_html = '<div class="widget_summary">
            //         <div class="w_center w_55" style="width: 100%;">
            //           <div class="progress">
            //             <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" title = "'.$row['review_finished_rate'].'%" style="width: '.$row['review_finished_rate'].'%;">
            //               <span class="sr-only">60% Complete</span>
            //             </div>
            //           </div>
            //         </div>
            //         <div class="clearfix"></div>
            //       </div>';

            $actions_html = '<ul class="nav navbar-right panel_toolbox">';
            $actions_html .= '<li><a title="View" class="collapse-link" href="erp_rma_check.php?act=list&amp;rma_id='.$row['rma_id'].'"><i class="fa fa-eye green" style="font-size: 20px;"></i></a></li>';

            if ($row['type'] == self::TYPE_ARCHIVED) {
                $actions_html .= '<li><a title="Undo" class="collapse-link confirm_action" data-rma-id="'.$row['rma_id'].'" data-change-type="1" href="javascript:void(0)"><i class="fa fa-undo" style="font-size: 20px;color:#f0ad4e"></i></a></li>';
            } else {
                $actions_html .= '<li><a title="Archive" class="collapse-link confirm_action" data-rma-id="'.$row['rma_id'].'" data-change-type="0" href="javascript:void(0)"><i class="fa fa-archive" style="font-size: 20px;color:#f0ad4e"></i></a></li>';
            }

            //$actions_html .= '<li><a class="collapse-link confirm_action" data-rma-id="'.$row['rma_id'].'" data-change-type="0" href="javascript:void(0)"><i class="fa fa-archive" style="font-size: 20px;color:#f0ad4e"></i></a></li>';
            if ($row['channel'] == self::CHANNEL_REPAIR) {
                $actions_html .= '<li><a title="Print" class="collapse-link" data-rma-id="'.$row['rma_id'].'" data-change-type="0" target="_blank" href="/pos_repair.php?act=print&amp;repair_id='.$row['repair_id'].'"><i class="fa fa-print" style="font-size: 20px;"></i></a></li>';
            }
            $actions_html .= '<li><a class="close-link"><i class="fa fa-close"></i></a></li>';
            $actions_html .= '</ul>';
            $row['create_date'] = local_date($_CFG['time_format'], $row['create_time']);
            $row['status'] = $statusOptions[$row['status']];
            $row['avatar'] = '<img title="'.$row['user_name'].'" src="'.(empty($row['avatar']) ? '/uploads/avatars/default_60x60.png' : '/'.$row['avatar']).'" >';
            $row['stocktake_progress'] = $stocktake_progress_html;
            $row['review_progress'] = $reviw_progress_html;
            $row['image'] = '<a target="_blank" href="../goods.php?id='.$row['goods_id'].'"><img width=50px src="./../'.$row['goods_thumb'].'"/>';
            $row['actions'] = $actions_html;

            if ($row['channel'] == self::CHANNEL_REPAIR) {
                $row['remark2'] = '<span class="follow_up_remark" data-rma-id="'.$row['rma_id'].'" data-toggle="modal" data-target="#popup_follow_up_remark_modal" >'.(empty($row['remark2'])? '請輸入' : $row['remark2']).'</span>';
            } else {
                $row['remark2'] = '<span class="follow_up_remark" data-rma-id="'.$row['rma_id'].'" data-toggle="modal" data-target="#popup_follow_up_remark_modal" >'.(empty($row['follow_up_remark'])? '請輸入' : $row['follow_up_remark']).'</span>';
            }

            if ($row['channel'] == self::CHANNEL_REPAIR) {
                if ($row['pick_new'] == 1) {
                    $row['customer_pick_up_status'] = '等待取貨<button type="button" class="btn btn-success btn-xs">確定客戶取貨</button>';
                }
                $row['repair_fee'] = '$'.$row['repair_fee'];
            } else if ($row['channel'] == self::CHANNEL_REPLACEMENT) {
                $row['repair_sn'] = '換貨';
                $row['reason'] = empty($row['reason'])? '(需檢查/維修)' : $row['reason'];
                $row['customer_pick_up_status'] = 'N/A';
            } else if ($row['channel'] == self::CHANNEL_BAD_GOODS) {
                $row['repair_sn'] = '壞貨';
                $row['reason'] = empty($row['reason'])? '(需檢查/維修)' : $row['reason'];
                $row['customer_pick_up_status'] = 'N/A';
            }

            if ($row['repair_order_status'] == 4) {  // 完成及可出貨
                if ($row['pickup_time'] == 0) {
                    $row['pickup_status'] = '待取貨 <button type="button" data-rma-id="'.$row['rma_id'].'" class="btn btn-success btn-xs confirm-pickup">確定客戶取貨</button>';
                } else {
                    $row['pickup_status'] = '客人已取貨<br>('.local_date($_CFG['time_format'], $row['pickup_time']).')';
                }
            } else {
                $row['pickup_status'] = '';
            }
        }
        // $sort_order  = $_REQUEST['sort_order'] == 'ASC' ? SORT_ASC : SORT_DESC;
        // array_multisort($sort, $sort_order, $res);

        $arr = array(
            'data' => $res,
            'record_count' => $_REQUEST['record_count']
        );

        return $arr;
    }

    public function getRepairIdByRmaId($rma_id)
    {
        global $db, $ecs, $userController, $_CFG;

        $sql = 'select repair_id from '. $ecs->table('erp_rma') .' where rma_id = "'.$rma_id.'" ';
        $reapir_id = $db->getOne($sql);

        return $reapir_id;
    }

    public function confirmPickup($rma_id)
    {
        global $db, $ecs, $userController, $_CFG;

        // get repair order
        $repair_id = $this->getRepairIdByRmaId($rma_id);

        $sql = 'update '.  $ecs->table('repair_orders') .' set pickup_time = "'.gmtime().'" where repair_id = "'.$repair_id.'" ';
        $db->query($sql);
        
        return true;
    }

    public function getRmaInfo($rma_id)
    {
        global $db, $ecs, $userController, $_CFG;

        $sql =  "SELECT er.*, ro.repair_sn, ro.contact_phone, ro.contact_name, ro.contact_email, er.operator, au.avatar, au.user_name, g.goods_thumb,g.goods_sn,g.goods_name, ro.repair_fee, ro.repair_order_status, ro.pickup_time, ro.remark2, oi.order_sn 
                FROM " . $ecs->table('erp_rma') . " er 
                left join ". $ecs->table('repair_orders') ." ro on (er.repair_id = ro.repair_id) 
                left join ". $ecs->table('order_info') ." oi on (er.order_id = oi.order_id) 
                left join ". $ecs->table('admin_user')." au on (er.person_in_charge = au.user_id) 
                left join " . $ecs->table('goods') . " g on (g.goods_id = er.goods_id) 
                WHERE rma_id = ".$rma_id." ";
        //exit;

        //$sql = "select * from ".$ecs->table('erp_rma')." where rma_id = '".$rma_id."'";
        $rma = $db->getRow($sql);

        if ($rma['repair_order_status'] == 4) {  // 完成及可出貨
            if ($rma['pickup_time'] == 0) {
                $rma['pickup_status'] = '待取貨 ';
            } else {
                $rma['pickup_status'] = '客人已取貨<br>('.local_date($_CFG['time_format'], $rma['pickup_time']).')';
            }
        } else {
            $rma['pickup_status'] = '';
        }

        $rma['create_time'] = local_date($_CFG['time_format'], $rma['create_time']);

        if (!empty($rma['supplier_id'])) {
            $sql = 'select name from '.$ecs->table('erp_supplier').' es where supplier_id = "'.$rma['supplier_id'].'" ';
            $name = $db->getOne($sql);
            $rma['supplier_name'] = $name;
        }

        $channels = $this->getChannelOptions();
        $rma['channel_name'] = $channels[$rma['channel']];
        if ($rma['goods_admin'] > 0) {
            $rma['goods_admin'] = $this->getUserNameById($rma['goods_admin']);
        }

        $rma['warehouse_name'] = $this->erpController->getWarehouseNameById($rma['warehouse_id']);
        $rma['repair_warehouse_name'] = $this->erpController->getWarehouseNameById($rma['repair_warehouse_id']);

        $warehouseTransferController = new WarehouseTransferController();
        if (!empty($rma['repair_transfer_id'])) {
            $rma['repair_transfer_sn'] = $warehouseTransferController->getTransferSn($rma['repair_transfer_id']);
        }

        if (!empty($rma['repaired_transfer_id'])) {
            $rma['repaired_transfer_sn'] = $warehouseTransferController->getTransferSn($rma['repaired_transfer_id']);
        }
        
        return $rma;
    }

    public function getGoodsIdBySn($goods_sn)
    {
        global $db, $ecs, $userController, $_CFG;
        
        //get goods id
        $sql = "select goods_id from ".$ecs->table('goods')." where goods_sn = '".$goods_sn."' ";
        return $goods_id = $db->getOne($sql);
    }

    public function rmaBadGoodsRepairSubmit()
    {
        global $db, $ecs, $userController, $_CFG;

        $goods_id = $this->getGoodsIdBySn($_REQUEST['bad_goods_repair_goods_sn']);
        if (empty($goods_id)) {
            $errmsg = '系統找不到['.$_REQUEST['bad_goods_repair_goods_sn'].']';
            $result['error'] = $errmsg;
            return $result;
        }

        $sale_warehouse_id = $this->getDefaultSaleWarehouse();
        $temp_warehouse_id = $this->getDefaultTemporaryWarehouse();
        $goods_qty = 1;

        // reduce sale warehouse
        //stock check for sale warehouse to temp warhouse
        $attrId=\get_attr_id($goods_id);
        $stock=\get_goods_stock_by_warehouse($sale_warehouse_id,$goods_id,$attrId);
        if($stock<$goods_qty){
            $errmsg = '庫存不足, 請重新確認 (Requested:'.$goods_qty.$sale_warehouse_id.')';
            $result['error'] = $errmsg;
            return $result;
        }

        $operatior = $this->getUserNameById($_REQUEST['bad_goods_repair_operator_id']);

        // create rma
        $rma_id = $this->createRma(self::CHANNEL_BAD_GOODS, 0, 0, $goods_id, $goods_qty, $operatior, $_REQUEST['bad_goods_repair_remark_data']);

        // sale stock to temp stock
        if (!empty($rma_id)) {
            $errmsg = $this->erpController->stockTransfer($sale_warehouse_id, $temp_warehouse_id, [$goods_id=>$goods_qty], $rma_id);
            //$this->saveStepStockLog($rma_id, 1, $rma_info['status'], $rma_info['goods_id'], $sale_warehouse_id, $rma_info['qty']*-1);
            //$this->saveStepStockLog($rma_id, 1, $rma_info['status'], $rma_info['goods_id'], $temp_warehouse_id, $rma_info['qty']);
        }

        return $rma_id;
    }

    public function getReportData() 
    {
        global $db, $ecs, $userController, $_CFG;

        $current_month_start = date("Y-m-d", strtotime( date( 'Y-m-01' )." -0 months"));
        $current_month_end = date("Y-m-d", strtotime( date( 'Y-m-01' )." +1 months"));
        
        $start_time = local_strtotime($current_month_start);
        $end_time = local_strtotime($current_month_end);
        $sql = "select count(*) from ".$ecs->table('erp_rma')." where create_time >= '".$start_time."' and create_time < '".$end_time."' ";
        $data['rma_count_new'] = $db->getOne($sql);
        
        $sql = "select count(*) from ".$ecs->table('erp_rma')." where last_action_time >= '".$start_time."' and last_action_time < '".$end_time."' and  status = ".self::FINISHED." ";
        $data['rma_count_solved'] = $db->getOne($sql);

        $sql = "select count(*) from ".$ecs->table('erp_rma')." where status != ".self::FINISHED." ";
        $data['rma_count_unsolved_accumulate'] = $db->getOne($sql);

        $start_30days_ago = date("Y-m-d", strtotime( date( 'Y-m-d' )." -30 days"));
        $start_60days_ago = date("Y-m-d", strtotime( date( 'Y-m-d' )." -60 days"));
        
        $start_time = local_strtotime($start_30days_ago);
        $sql = "select count(*) from ".$ecs->table('erp_rma')." where create_time >= '".$start_time."' ";
        $data['rma_count_new_30days_ago'] = $db->getOne($sql);

        $start_time = local_strtotime($start_60days_ago);
        $end_time = local_strtotime($start_30days_ago);
        $sql = "select count(*) from ".$ecs->table('erp_rma')." where create_time >= '".$start_time."' and create_time < '".$end_time."' ";
        $data['rma_count_new_60days_ago'] = $db->getOne($sql);

        if ($data['rma_count_new_60days_ago'] == 0) {
            $data['rma_count_new_compare_last_30days'] = 0;
        } else {
            $data['rma_count_new_compare_last_30days'] = round(($data['rma_count_new_30days_ago'] - $data['rma_count_new_60days_ago']) /$data['rma_count_new_60days_ago'] * 100);
        }
        
        $start_time = local_strtotime($start_30days_ago);
        $sql = "select count(*) from ".$ecs->table('erp_rma')." where last_action_time >= '".$start_time."' and status = ".self::FINISHED." ";
        $data['rma_count_solved_30days_ago'] = $db->getOne($sql);

        $start_time = local_strtotime($start_60days_ago);
        $end_time = local_strtotime($start_30days_ago);
        $sql = "select count(*) from ".$ecs->table('erp_rma')." where last_action_time >= '".$start_time."' and last_action_time < '".$end_time."' and  status = ".self::FINISHED." ";
        $data['rma_count_solved_60days_ago'] = $db->getOne($sql);

        if ($data['rma_count_solved_60days_ago'] == 0) {
            $data['rma_count_solved_compare_last_30days'] = 0;
        } else {
            $data['rma_count_solved_compare_last_30days'] = round(($data['rma_count_solved_30days_ago'] - $data['rma_count_solved_60days_ago']) /$data['rma_count_solved_60days_ago'] * 100);
        }

        // echo '<pre>';
        // print_R($data);
        // exit;

        return $data;
    }

    public function getReportDataMonthly() 
    {
        global $db, $ecs, $userController, $_CFG;

        // generate last 12 months
        for ($i = 12; $i >= 0; $i--) {
            $monthly_data['new'][$i]['label'] = date("M", strtotime( date( 'Y-m-01' )." -$i months"));
            $monthly_data['new'][$i]['range_start'] = date("Y-m-d", strtotime( date( 'Y-m-01' )." -$i months"));
            $monthly_data['new'][$i]['range_end'] = date("Y-m-d", strtotime( date( 'Y-m-01' )." -$i months +1 months"));
            $monthly_data['new'][$i]['data'] = date("Y-m-d", strtotime( date( 'Y-m-01' )." -$i months"));

            $start_time = local_strtotime($monthly_data['new'][$i]['range_start']);
            $end_time = local_strtotime($monthly_data['new'][$i]['range_end']);
            $sql = "select count(*) from ".$ecs->table('erp_rma')." where create_time >= '".$start_time."' and create_time < '".$end_time."' ";
            $monthly_data['new'][$i]['data'] = $db->getOne($sql);
        }

        // generate last 12 months
        for ($i = 12; $i >= 0; $i--) {
            $monthly_data['finished'][$i]['label'] = date("M", strtotime( date( 'Y-m-01' )." -$i months"));
            $monthly_data['finished'][$i]['range_start'] = date("Y-m-d", strtotime( date( 'Y-m-01' )." -$i months"));
            $monthly_data['finished'][$i]['range_end'] = date("Y-m-d", strtotime( date( 'Y-m-01' )." -$i months +1 months"));
            $monthly_data['finished'][$i]['data'] = date("Y-m-d", strtotime( date( 'Y-m-01' )." -$i months"));

            $start_time = local_strtotime($monthly_data['finished'][$i]['range_start']);
            $end_time = local_strtotime($monthly_data['finished'][$i]['range_end']);
            $sql = "select count(*) from ".$ecs->table('erp_rma')." where last_action_time >= '".$start_time."' and last_action_time < '".$end_time."' and  status = ".self::FINISHED." ";
            $monthly_data['finished'][$i]['data'] = $db->getOne($sql);
        }

        // echo '<pre>';
        // print_r($monthly_data);
        // exit;
        return $monthly_data;
    }

    public function getRmaGoodsTop($limit)
    {
        global $db, $ecs, $userController, $_CFG;

        $end_time= local_strtotime(date("Y-m-d", strtotime( date( 'Y-m-d' )." -0 months")));
        
        $start_time  = local_strtotime(date("Y-m-d", strtotime( date( 'Y-m-d' )." -90 days")));
        
        $sql = "select sum(qty) as repair_count, er.goods_id, g.goods_sn, g.goods_name, g.goods_img from ".$ecs->table('erp_rma')." er left join ".$ecs->table('goods')." g on (er.goods_id = g.goods_id) where create_time >= '".$start_time."' and create_time < '".$end_time."' group by goods_id order by repair_count desc limit ".$limit." ";
        $result = $db->getAll($sql);
        
        foreach ($result as $key=> &$goods) {
            $key++;
            $goods['rank'] = $key;
            $goods['goods_admin'] = $this->getGoodsUserAdmin($goods['goods_id']);
            $user_info = $this->getUserInfo($goods['goods_admin']);
            $goods['goods_admin_name'] = $user_info['user_name'];
            $goods['goods_admin_avatar'] = $user_info['avatar'];
        }

        return $result;
    }

    public function getRmaStatusData()
    {
        global $db, $ecs, $userController, $_CFG;

        $sql = "select count(status) as status_count, status from ".$ecs->table('erp_rma')." er where status != ".self::FINISHED." group by status order by status ";
        $result = $db->getAll($sql);

        $all_status = self::$statusValues;

        foreach ($result as &$status) {
            $status['label'] = $all_status[$status['status']];
        }

        // echo '<pre>';
        // print_r($result);
        // exit;

        return $result;

    }

    public function rmaCancelRepairTransferSubmit($step = 1)
    {
        global $db, $ecs, $_CFG;
      
        $rma_id = $_REQUEST['rma_id'];

        //$rma_info = $this->getRmaInfo($rma_id);

        $result = $this->rmaReversePreviousStepSubmit($rma_id);

        return $result;
        // // check transfer order if shipped
        // $warehouseTransferController = new warehouseTransferController();
        // $repair_transfer_status = $warehouseTransferController->getCurrentStatus($rma_info['repair_transfer_id']);
        // $transfering_status = [$warehouseTransferController::TRANSFERING,$warehouseTransferController::AWAITING_TRANSPORT];
        // if (in_array($repair_transfer_status,$transfering_status)) {
        //     $result['error'] = '調貨處理中,不能取消';
        //     $db->query('ROLLBACK');
        //     return $result;
        // } else if ($repair_transfer_status == $warehouseTransferController::RECEIVED) {
        //     $result['error'] = '調貨已完成,不能取消,不能返回';
        //     $db->query('ROLLBACK');
        //     return $result;
        // } else {
        //     $warehouseTransferController->transferDeleteForRMA($rma_info['repair_transfer_id']);
        //     $sql = 'update '. $ecs->table('erp_rma').' set repair_transfer_id = null where rma_id = "'.$rma_id.'" ';
        //     $db->query($sql);
        // 
        //     $this->rmaReversePreviousStepSubmit($rma_id);
        // 
        // }
    }

    public function rmaCancelRepairedTransferSubmit($step = 5)
    {
        global $db, $ecs, $_CFG;
        
        $rma_id = $_REQUEST['rma_id'];
        $rma_info = $this->getRmaInfo($rma_id);
        $warehouseTransferController = new warehouseTransferController();
        $db->query('START TRANSACTION');
        // check if having transfer before
        $repair_transfer_status = $warehouseTransferController->getCurrentStatus($rma_info['repaired_transfer_id']);
        $transfering_status = [$warehouseTransferController::TRANSFER_STOCK_IN,$warehouseTransferController::TRANSFERING,$warehouseTransferController::AWAITING_TRANSPORT];
        if (in_array($repair_transfer_status,$transfering_status)) {
        //if ($repair_transfer_status != $warehouseTransferController::CREATING) {
            $result['error'] = '調貨處理中,不能取消';
            $db->query('ROLLBACK');
            return $result;
        } else if ($repair_transfer_status == $warehouseTransferController::RECEIVED) {
            $result['error'] = '調貨已完成,不能取消';
            $db->query('ROLLBACK');
            return $result;
        } else {
            $warehouseTransferController->transferDeleteForRMA($rma_info['repaired_transfer_id'],$rma_id);
            $sql = 'update '. $ecs->table('erp_rma').' set repaired_transfer_id = null where rma_id = "'.$rma_id.'" ';
            $db->query($sql);

            $final_note = '取消調貨';
            $this->rmaActionLog($rma_id, $user_id, $final_note, $currentStatus);
            $this->rmaChangeStatus($rma_id, 4);

            $db->query('COMMIT');
            return true;
        }
    }


}
