<?php

namespace Yoho\cms\Controller;

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

require_once(ROOT_PATH . 'includes/lib_order.php');

class GoodsPlaybackScreenController extends YohoBaseController {
    public function getTopHundredBestseller()
    {
        global $db, $ecs;

        $where = " WHERE og.order_id = oi.order_id AND og.goods_id = g.goods_id AND og.goods_id !='23987' ". order_query_sql('finished', 'oi.');

        $sql = 'SELECT og.goods_name, ' .
            'SUM(og.goods_number) AS goods_num, g.goods_img, b.`brand_name` ' .
            'FROM ' .$ecs->table('order_goods'). ' AS og, ' .
            $ecs->table('order_info'). ' AS oi, ' .$ecs->table('goods').' as g LEFT JOIN '.$ecs->table('brand').' as b ON g.brand_id = b.brand_id' .$where .
            ' GROUP BY og.goods_id ' .
            ' ORDER BY goods_num DESC LIMIT 100';

        $sales_order_data = $db->getAll($sql);
        return $sales_order_data;
    }

    public function getFiveStarReviewItems()
    {
        global $db, $ecs;

        $fiveStarReviewGoodsIds = $db->getAll('SELECT DISTINCT(`id_value`) FROM ' . $ecs->table('comment') . ' WHERE `comment_rank` = "5" AND `id_value` != "23987" ORDER BY `add_time` DESC LIMIT 100');
        $fiveStarReviewGoodsIds = array_column($fiveStarReviewGoodsIds, 'id_value');

        $sql = 'SELECT g.goods_id, g.goods_name, g.goods_img, b.brand_name FROM ' . $ecs->table('goods') . ' AS g LEFT JOIN ' . $ecs->table('brand') . ' AS b ON g.brand_id = b.brand_id WHERE g.goods_id in (' . implode(',', $fiveStarReviewGoodsIds) .')';

        $data = $db->getAll($sql);
        foreach ($data as $index => $item) {
            $data[$index]['additional_info'] = $this->genRatingSvg(5);
        }
        return $data;
    }

    public function getNewItem()
    {
        global $db, $ecs;

        $sql = 'SELECT g.goods_id, g.goods_name, g.goods_img, b.brand_name, g.add_time FROM ' . $ecs->table('goods') . ' AS g LEFT JOIN ' . $ecs->table('brand') . ' AS b ON g.brand_id = b.brand_id WHERE g.is_new IS TRUE AND g.is_on_sale IS TRUE LIMIT 100';

        $data = $db->getAll($sql);
        foreach($data as $index=>$item) {
            $addDate = local_getdate($item['add_time']);
            $data[$index]['add_date_day'] = $addDate['mday'];
            $data[$index]['add_date_month'] = $addDate['mon'];
        }
        return $data;
    }

    public function getSevenDayHotItem()
    {
        global $db, $ecs, $_LANG;

        $interval = new \DateInterval('P8D');
        $endDate = new \DateTime(local_date('Y-m-d 23:59:59'));
        $startDate = clone $endDate;
        $startDate = $startDate->sub($interval);

        $startDate = local_strtotime($startDate->format('Y-m-d H:i:s'));
        $endDate = local_strtotime($endDate->format('Y-m-d H:i:s'));

        $where = " WHERE og.order_id = oi.order_id AND og.goods_id = g.goods_id AND og.goods_id !='23987' AND oi.add_time >= '$startDate' AND oi.add_time <= '$endDate' ". order_query_sql('finished', 'oi.');

        $sql = 'SELECT og.goods_name, ' .
            'SUM(og.goods_number) AS goods_num, g.goods_img, b.`brand_name` ' .
            'FROM ' .$ecs->table('order_goods'). ' AS og, ' .
            $ecs->table('order_info'). ' AS oi, ' .$ecs->table('goods').' as g LEFT JOIN '.$ecs->table('brand').' as b ON g.brand_id = b.brand_id' .$where .
            ' GROUP BY og.goods_id ' .
            ' ORDER BY goods_num DESC LIMIT 100';

        $sales_order_data = $db->getAll($sql);
        foreach($sales_order_data as $index => $item) {
            $sales_order_data[$index]['additional_info'] = sprintf($_LANG['productlist_sells'], insert_goods_sells_text($item->goods_num));
        }
        return $sales_order_data;
    }
}