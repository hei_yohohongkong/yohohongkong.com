<?php

/***
 * SupplierController.php
 * by Anthony 20171009
 *
 ***/

namespace Yoho\cms\Controller;

if (!defined('IN_ECS')) {
    die('Hacking attempt');
}

class SupplierController extends YohoBaseController
{

    const payment_terms = [
        'immediate' => '立即',
        '7_day' => '7日',
        'month_statement' => '月結'
    ];

    public function getPaymentTerms()
    {
        return self::payment_terms;
    }

    public function getCorrespondingCompanyList()
    {
        global $db, $ecs;
        $sql = "SELECT * FROM " . $ecs->table('corresponding_company');
        $result = $db->getAll($sql);
        return $result;
    }

    public function get_sale_supplier($is_pagination = false, $admin = 0, $house = false)
    {
        ini_set('memory_limit', '512M');
        require_once(ROOT_PATH . 'includes/lib_order.php');
        $userController = new UserController();
        $erpController = new ErpController();
        $orderController = new OrderController();
        $month_start = local_date('Y-m-01 00:00:00');
        $month_end = local_date('Y-m-t 23:59:59');
        set_time_limit(0);

        $filter['start_date'] = empty($_REQUEST['start_date']) ? local_strtotime($month_start) : local_strtotime($_REQUEST['start_date']);
        $filter['end_date'] = empty($_REQUEST['end_date']) ? local_strtotime($month_end) : local_strtotime($_REQUEST['end_date']);
        $days = $GLOBALS['db']->getOne("SELECT COUNT(DISTINCT report_date) FROM " . $GLOBALS['ecs']->table('stock_cost_log') . " WHERE report_date >= " . $filter['start_date'] . " AND report_date <= " . ($filter['end_date'] + 86400));
                
        $filter['order_type'] = empty($_REQUEST['order_type']) ? 0 : intval($_REQUEST['order_type']);
        $filter['deprecatedflow'] = empty($_REQUEST['deprecatedflow']) ? 0 : intval($_REQUEST['deprecatedflow']);

        $sql = "SELECT COUNT(*) FROM " . $GLOBALS['ecs']->table('brand') . " WHERE is_show = 1";
        $filter['record_count'] = intval($GLOBALS['db']->getOne($sql)) + 1; // + no supplier
        $where = " oi.shipping_time >= '" . $filter['start_date'] . "' AND oi.shipping_time < '" . ($filter['end_date'] + 86400) . "' " . order_query_sql('finished', 'oi.');

        // Ignore 換貨訂單
        $where .= " AND oi.postscript NOT LIKE '%換貨，%'";

        if ($filter['order_type'] == 4) // 所有訂單
        {
            $where_ws = "(" . str_replace("oi.pay_status  IN ('", "oi.pay_status  IN ('" . PS_UNPAYED . "','", explode('AND', order_query_sql('finished', 'oi.'))[3]);
            $where_ws .= " AND (oi.order_type = '" . $orderController::DB_ORDER_TYPE_WHOLESALE . "' OR (oi.user_id IN (" . implode(',', $userController->getWholesaleUserIds()) . ") AND oi.order_type IS NULL) ) ";
            $where = str_replace("oi.pay_status  IN ('", "( oi.pay_status  IN ('", $where) . " OR " . $where_ws . " ) )";
        } else if ($filter['order_type'] == 1) // 批發訂單
        {
            // Only include orders for the followiung user account: 99999999
            $where = str_replace("oi.pay_status  IN ('", "oi.pay_status  IN ('" . PS_UNPAYED . "','", $where);
            $where .= " AND (oi.order_type = '" . $orderController::DB_ORDER_TYPE_WHOLESALE . "' OR (oi.user_id IN (" . implode(',', $userController->getWholesaleUserIds()) . ") AND oi.order_type IS NULL) ) ";
        } else if ($filter['order_type'] == 2) // 借貨訂單
        {
            // Only include orders for the followiung user accounts: 11111111, 88888888
            $where .= " AND oi.user_id IN (4940,5023) ";
        } else if ($filter['order_type'] == 3) // 代理銷售訂單
        {
            $where .= " AND oi.order_type = '" . $orderController::DB_ORDER_TYPE_SALESAGENT . "' ";
        } else if ($filter['order_type'] == 0) // Normal order
        {
            // Exclude orders for the followiung user accounts: 11111111, 88888888, 99999999
            // Exclude order type sa, ws
            $where .= " AND (oi.order_type NOT IN ('" . $orderController::DB_ORDER_TYPE_SALESAGENT . "','" . $orderController::DB_ORDER_TYPE_WHOLESALE . "') OR (oi.order_type IS NULL AND oi.user_id NOT IN (" . implode(',', $userController->getWholesaleUserIds()) . ")) )";
            $where .= " AND (oi.platform <> 'pos_exhibition') ";
        } else if ($filter['order_type'] == 5) // Exhibition order
        {
            // Exclude orders for the followiung user accounts: 11111111, 88888888, 99999999
            // Exclude order type sa, ws
            $where .= " AND (oi.order_type NOT IN ('" . $orderController::DB_ORDER_TYPE_SALESAGENT . "','" . $orderController::DB_ORDER_TYPE_WHOLESALE . "') OR (oi.order_type IS NULL AND oi.user_id NOT IN (" . implode(',', $userController->getWholesaleUserIds()) . ")) )";
            $where .= " AND (oi.platform = 'pos_exhibition') ";
        }

        // Exclude order with sale exclude coupon
        $where .= " AND (c.sale_exclude = 0 OR c.sale_exclude IS NULL) ";
        if($admin>0)$where_u = " IN (SELECT distinct supplier_id FROM " . $GLOBALS['ecs']->table('erp_supplier_admin') . " WHERE admin_id = " .$admin . ")";
        /* 分页大小 */
        $filter = page_and_size($filter);
        $sql_1 = "SELECT egs.supplier_id, IFNULL(es.name, '沒有供應商') as supplier_name, " .
        'IFNULL(sum(gn.goods_number),0) as total_fifo_qty, ' .
        'IFNULL(sum(gn.goods_number * g.cost),0) as total_cost, ' .
        'IFNULL(sum(gp.revenue),0) as total_revenue, ' .
        'IFNULL(sum(gp.cost),0) as total_gpcost, ' .
        'IFNULL(sum(gp.revenue - gp.cost),0) as total_profit, ' .
        'IFNULL(sum(gp.sap),0) as total_sa_revenue, ' .
        'IFNULL(sum(gp.sap - gp.cost),0) as total_sa_profit ' .
        'FROM ' . $GLOBALS['ecs']->table('erp_supplier') . ' AS es ' .
        'LEFT JOIN ' . $GLOBALS['ecs']->table('erp_goods_supplier') . ' AS egs ON egs.supplier_id = es.supplier_id ' .
        'LEFT JOIN ' . $GLOBALS['ecs']->table('goods') . ' AS g ON g.goods_id = egs.goods_id ' .
        'LEFT JOIN (SELECT ega.goods_id, sum(ega.goods_qty) as goods_number FROM ' . $GLOBALS['ecs']->table('erp_goods_attr_stock') . ' AS ega WHERE ega.warehouse_id IN (' . $erpController->getSellableWarehouseIds(true) . ') GROUP BY ega.goods_id) as gn ON gn.goods_id = g.goods_id ' .
        //'LEFT JOIN (SELECT ega.goods_id, sum(ega.goods_qty) as goods_number FROM ' . $GLOBALS['ecs']->table('erp_goods_attr_stock') . ' AS ega WHERE ega.warehouse_id IN () GROUP BY ega.goods_id) as gn ON gn.goods_id = g.goods_id ' .
        'LEFT JOIN (' .
        'SELECT og.goods_id, ' .
        'sum((IF(og.cost > 0, og.cost, g2.cost) + ' .
        'IF(oi.`pay_id` = 2,  IF(og.cost > 0, og.cost, g2.cost) * 0.0095,       0) + ' . // EPS: 0.95% fee
        'IF(oi.`pay_id` = 7,  IF(og.cost > 0, og.cost, g2.cost) * 0.032 + 2.35, 0) + ' . // PayPal: 3.2% fee + $2.35 per txn
        'IF(oi.`pay_id` = 10, IF(og.cost > 0, og.cost, g2.cost) * 0.029,        0) + ' . // UnionPay: 2.9% fee
        'IF(oi.`pay_id` = 12, IF(og.cost > 0, og.cost, g2.cost) * 0.023,        0) + ' . // Visa / MasterCard: 2.3% fee
        'IF(oi.`pay_id` = 13, IF(og.cost > 0, og.cost, g2.cost) * 0.0296,       0)   ' . // American Express: 2.96% fee
        ') * og.goods_number + ' .
        'IF(oi.`shipping_id` = 3, 30 * og.goods_price * og.goods_number / oi.goods_amount, 0) ' . // Delivery cost: $30
        ') as cost, ' .
        "sum( (( ((100 - IF(og.sa_rate_id > 0 , sa.sa_rate, 0) ) /100 ) * og.goods_price)*og.goods_number) - IF(oi.`pay_fee` < 0, oi.`pay_fee` * -1 * og.goods_price * og.goods_number / oi.goods_amount, 0) ) as sap, " .
        'sum(og.goods_price * og.goods_number - IF(oi.`pay_fee` < 0, oi.`pay_fee` * -1 * og.goods_price * og.goods_number / oi.goods_amount, 0)) as revenue ' .
        'FROM ' . $GLOBALS['ecs']->table('order_goods') . ' AS og ' .
        'LEFT JOIN ' . $GLOBALS['ecs']->table('order_info') . ' AS oi ON oi.order_id = og.order_id ' .
        'LEFT JOIN ' . $GLOBALS['ecs']->table('goods') . ' AS g2 ON g2.goods_id = og.goods_id ' .
        "LEFT JOIN " . $GLOBALS['ecs']->table('sales_agent_rate') . " as sa ON sa.sa_rate_id = og.sa_rate_id " .
        'LEFT JOIN ' . $GLOBALS['ecs']->table('order_coupons') . ' AS oc ON oc.order_id = oi.order_id ' .
        'LEFT JOIN ' . $GLOBALS['ecs']->table('coupons') . ' AS c ON c.coupon_id = oc.coupon_id ' .
            'WHERE ' . $where .
            'GROUP BY og.goods_id' .
            ') as gp ON gp.goods_id = g.goods_id ' .
            "WHERE (es.is_valid IS NULL OR es.is_valid = 1) AND g.is_delete = 0 GROUP BY egs.supplier_id ORDER BY supplier_name ASC";

        $sql_2 = "SELECT es.supplier_id, IFNULL(es.name, '沒有供應商') as supplier_name, " .
        "IFNULL(SUM(sc.cost),0) as total_cost, " .
        "IFNULL(SUM(sc.consumed_qty),0) as total_sold_qty," .
        "IFNULL(SUM(sc.revenue-sc.total_discount),0) as total_revenue," .
        "IFNULL(sum(sc.sap-sc.total_discount),0) as total_sa_revenue, " .
        "IFNULL(sum(sc.total_discount),0) as total_discount, " .
        "IFNULL(SUM(sc.revenue - sc.total_discount - sc.cost),0) as total_profit, " .
        "IFNULL(sum(sc.sap - sc.total_discount - sc.cost),0) as total_sa_profit " .
        "FROM " . $GLOBALS['ecs']->table('erp_supplier') . " AS es " .
        "LEFT JOIN (" .
        "SELECT ogsc.*, (oi2.total_o_ds/oi2.total_og_price)*(og.goods_price * IFNULL(ogsc.consumed_qty,og.delivery_qty)) as total_discount ," .
        "((ogsc.po_unit_cost + " .
        "IF(oi.`pay_id` = 2,  ogsc.po_unit_cost * 0.0095,       0) + " . // EPS: 0.95% fee
        "IF(oi.`pay_id` = 7,  ogsc.po_unit_cost * 0.032 + 2.35, 0) + " . // PayPal: 3.2% fee + $2.35 per txn
        "IF(oi.`pay_id` = 10, ogsc.po_unit_cost * 0.029,        0) + " . // UnionPay: 2.9% fee
        "IF(oi.`pay_id` = 12, ogsc.po_unit_cost * 0.023,        0) + " . // Visa / MasterCard: 2.3% fee
        "IF(oi.`pay_id` = 13, ogsc.po_unit_cost * 0.0296,       0)   " . // American Express: 2.96% fee
        ") * IFNULL(ogsc.consumed_qty,og.delivery_qty) + " .
        "IF(oi.`shipping_id` = 3, 30 * og.goods_price * IFNULL(ogsc.consumed_qty,og.delivery_qty) / oi.goods_amount, 0) " . // Delivery cost: $30
        ") as cost," .
        "( (( ((100 - IF(og.sa_rate_id > 0 , sa.sa_rate, 0) ) /100 ) * og.goods_price)*IFNULL(ogsc.consumed_qty,og.delivery_qty)) - IF(oi.`pay_fee` < 0, oi.`pay_fee` * -1 * og.goods_price * IFNULL(ogsc.consumed_qty,og.delivery_qty) / oi.goods_amount, 0) ) as sap, " .
        "(og.goods_price * IFNULL(ogsc.consumed_qty,og.delivery_qty) - IF(oi.`pay_fee` < 0, oi.`pay_fee` * -1 * og.goods_price * IFNULL(ogsc.consumed_qty,og.delivery_qty) / oi.goods_amount, 0)) as revenue " .
        "FROM " . $GLOBALS['ecs']->table('order_goods_supplier_consumption') . " as ogsc " .
        "LEFT JOIN " . $GLOBALS['ecs']->table('delivery_goods') . "   as dg ON dg.order_item_id = ogsc.order_goods_id " .
        "LEFT JOIN " . $GLOBALS['ecs']->table('order_goods') . "      as og ON og.rec_id = ogsc.order_goods_id " .
        "LEFT JOIN " . $GLOBALS['ecs']->table('delivery_order') . "   as do ON do.delivery_id = dg.delivery_id " .
        "LEFT JOIN " . $GLOBALS['ecs']->table('erp_supplier') . "     as es ON ogsc.supplier_id = es.supplier_id " .
        "LEFT JOIN " . $GLOBALS['ecs']->table('order_info') . "       as oi ON oi.order_id = og.order_id " .
        "LEFT JOIN " . $GLOBALS['ecs']->table('sales_agent_rate') . " as sa ON sa.sa_rate_id = og.sa_rate_id " .
        "LEFT JOIN (SELECT (oi.`discount` + oi.`bonus` + oi.`coupon`)as total_o_ds, sum(og.goods_number * og.goods_price)as total_og_price, oi.order_id from " . $GLOBALS['ecs']->table('order_info') . " as oi left join " . $GLOBALS['ecs']->table('order_goods') . " AS og ON oi.order_id = og.order_id group by oi.order_id) as oi2 ON oi2.order_id = og.order_id " .
        'LEFT JOIN ' . $GLOBALS['ecs']->table('order_coupons') . ' AS oc ON oc.order_id = oi.order_id ' .
        'LEFT JOIN ' . $GLOBALS['ecs']->table('coupons') . ' AS c ON c.coupon_id = oc.coupon_id ' .
        "WHERE $where" . " GROUP BY ogsc.consumption_id " .
        ") as sc ON sc.supplier_id = es.supplier_id " .
        "LEFT JOIN " . $GLOBALS['ecs']->table('order_goods') . "      as og ON og.rec_id = sc.order_goods_id " .
        "LEFT JOIN " . $GLOBALS['ecs']->table('order_info') . "       as oi ON oi.order_id = og.order_id " .
        "LEFT JOIN " . $GLOBALS['ecs']->table('sales_agent_rate') . " as sa ON sa.sa_rate_id = og.sa_rate_id ";
        if ($house){
            if (isset($_REQUEST['on_sale_only']) && $_REQUEST['on_sale_only']){
                $sql_2 .= "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as gs ON gs.goods_id = og.goods_id WHERE gs.is_on_sale = 1 ";
            } else {
                $sql_2 .= "WHERE 1 ";
            }
            if ($house != false && $house !="" && (int)$house > 0){
                if ($house == 1){
                    $sql_2.= "AND (og.warehouse_id = 1 OR og.warehouse_id IS NULL) ";
                } else {
                    $sql_2.= "AND og.warehouse_id = ".$house." ";
                }
            }
        } else {
            $sql_2 .= "WHERE 1 ".($admin > 0 ? 'AND es.supplier_id '.$where_u : '');
        }
        $sql_2 .= " GROUP BY es.supplier_id ORDER BY supplier_name ASC";
        $sql_avg_cost = "SELECT type_id AS supplier_id, SUM(cost) / $days AS avg_stock_cost FROM " . $GLOBALS['ecs']->table('stock_cost_log') . " WHERE report_date >= " . $filter['start_date'] .
        " AND report_date <= " . ($filter['end_date'] + 86400) . " AND created_at IN (SELECT MAX(created_at) FROM " . $GLOBALS['ecs']->table('stock_cost_log') . " GROUP BY report_date) AND type = 3 ".($admin > 0 ? " AND type_id " . $where_u : '')." GROUP BY supplier_id";
        $sql_dead_cost = "SELECT type_id AS supplier_id, SUM(slowsoldcost_90) / $days AS avg_dead_cost FROM " . $GLOBALS['ecs']->table('stock_cost_log') . " WHERE report_date >= " . $filter['start_date'] .
        " AND report_date <= " . ($filter['end_date'] + 86400) . " AND created_at IN (SELECT MAX(created_at) FROM " . $GLOBALS['ecs']->table('stock_cost_log') . " GROUP BY report_date) AND type = 3 ".($admin > 0 ? " AND type_id " . $where_u : '')." AND report_date > 1527091200 GROUP BY supplier_id";
        $supplier_list = $GLOBALS['db']->getAll("SELECT supplier_id, name as supplier_name FROM " . $GLOBALS['ecs']->table('erp_supplier').($admin > 0 ? " WHERE supplier_id " . $where_u : ''));

        $res = $GLOBALS['db']->getAll(($_REQUEST['deprecatedflow'] == 'Y' ? $sql_1 : $sql_2));

        $avg_cost = $GLOBALS['db']->getAll($sql_avg_cost);
        $dead_cost = $GLOBALS['db']->getAll($sql_dead_cost);

        $data = array();
        $all = array(
            'name' => '所有供應商',
            'display' => isset($_REQUEST['order_type']) ? $_REQUEST['order_type'] : 4,
        );
        $cost_arr = array();
        $avg_cost_arr = array();

        /* 存貨成本 */
        require_once ROOT_PATH . 'includes/lib_howang.php';
        require_once ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_supplier.php';

        $sql_supplier_list =
        "SELECT eoi.goods_id,g.goods_num,g.is_on_sale,eoi.warehousing_qty as qty,(g.goods_num-eoi.warehousing_qty) as sum,eoi.price as unit_cost ,eo.order_id,eo.supplier_id
		FROM " . $GLOBALS['ecs']->table('erp_order_item') . " eoi
		INNER JOIN " . $GLOBALS['ecs']->table('erp_order') . " eo ON eoi.order_id=eo.order_id AND eo.order_status = 4 " .
        "INNER JOIN (SELECT g.goods_id ,g.is_on_sale, rg.goods_num FROM " . $GLOBALS['ecs']->table('goods') . " as g " .
        " LEFT JOIN ( SELECT " . hw_goods_number_subquery('g.', 'goods_num', '' , $house) . ", g.goods_id FROM " . $GLOBALS['ecs']->table('goods') . " as g ) as rg ON rg.goods_id = g.goods_id " .
        " WHERE rg.goods_num > 0 order by g.goods_id DESC ) as g on g.goods_id = eoi.goods_id AND g.goods_num > 0  ";
        if ($house && isset($_REQUEST['on_sale_only']) && $_REQUEST['on_sale_only']){
            $sql_supplier_list .= "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as gd ON gd.goods_id = eoi.goods_id WHERE gd.is_on_sale = 1 ";
        } else {
            $sql_supplier_list .= "WHERE 1 ";
        }
        $sql_supplier_list .= "AND eoi.warehousing_qty > 0 "./*($admin > 0 ? " AND eo.supplier_id ".$where_u : '').*/" ORDER BY eoi.goods_id DESC, eoi.order_item_id DESC";
        $po_list = $GLOBALS['db']->getAll($sql_supplier_list);

        $next_goods = true;
        $goods_id = 0;
        foreach ($po_list as $key => $po) {
            if ($next_goods && $po['goods_id'] == $goods_id) {
                continue;
            }

            // What goods we processing
            $next_goods = false;
            $goods_num = ($po['goods_id'] == $goods_id) ? $in_qty : $po['goods_num'];
            $goods_id = $po['goods_id'];
            $in_qty = $goods_num - $po['qty'];
            if ($in_qty > 0) // Stock > PO 入貨數
            {
                $qty = empty($admin) || ($admin > 0 && is_supplier_admin($po['supplier_id'], $admin)) ? $po['qty'] : 0;
                $cost = $qty * $po['unit_cost'];
            } elseif ($in_qty <= 0) {
                $qty = empty($admin) || ($admin > 0 && is_supplier_admin($po['supplier_id'], $admin)) ? $goods_num : 0;
                $cost = $qty * $po['unit_cost'];
                $next_goods = true;
            }
            $cost_arr[$po['supplier_id']]['cost'] = isset($cost_arr[$po['supplier_id']]['cost']) ? $cost_arr[$po['supplier_id']]['cost'] += $cost : $cost;
            $cost_arr[$po['supplier_id']]['qty'] = isset($cost_arr[$po['supplier_id']]['qty']) ? $cost_arr[$po['supplier_id']]['qty'] += $qty : $qty;
            if (!empty($dead_arr[$goods_id])) {
                $cost_arr[$po['supplier_id']]['dead_cost'] = isset($cost_arr[$po['supplier_id']]['dead_cost']) ? $cost_arr[$po['supplier_id']]['dead_cost'] += $cost : $cost;
            }
        }
        $data['all'] = $all;

        foreach ($avg_cost as $row) {
            $avg_cost_arr[$row['supplier_id']] = $row['avg_stock_cost'];
        }
        foreach ($dead_cost as $row) {
            $avg_dead_arr[$row['supplier_id']] = $row['avg_dead_cost'];
        }

        foreach ($res as $row) {
            $item = isset($data[$row['supplier_id']]) ? $data[$row['supplier_id']] : array();

            $curr_cost = isset($cost_arr[$row['supplier_id']]['cost']) ? $cost_arr[$row['supplier_id']]['cost'] : 0;
            $curr_qty = isset($cost_arr[$row['supplier_id']]['qty']) ? $cost_arr[$row['supplier_id']]['qty'] : 0;
            $curr_avg_cost = isset($avg_cost_arr[$row['supplier_id']]) ? $avg_cost_arr[$row['supplier_id']] : 0;
            $curr_dead_cost = isset($avg_dead_arr[$row['supplier_id']]) ? $avg_dead_arr[$row['supplier_id']] : 0;

            if ($house && ((!isset($row['total_revenue']) || !(int)$row['total_revenue']) && (!isset($row['total_cost']) || !(int)$row['total_cost'])  && (!isset($row['total_profit']) || !(int)$row['total_profit']) && !$curr_cost && !$curr_qty && !$curr_avg_cost && !$curr_dead_cost)) {
                continue;
            }

            $curr_cost = isset($cost_arr[$row['supplier_id']]['cost']) ? $cost_arr[$row['supplier_id']]['cost'] : 0;
            $curr_qty = isset($cost_arr[$row['supplier_id']]['qty']) ? $cost_arr[$row['supplier_id']]['qty'] : 0;
            $curr_avg_cost = isset($avg_cost_arr[$row['supplier_id']]) ? $avg_cost_arr[$row['supplier_id']] : 0;
            $curr_dead_cost = isset($avg_dead_arr[$row['supplier_id']]) ? $avg_dead_arr[$row['supplier_id']] : 0;

            //if ($house) {
            //    if ($curr_cost != 0 || $curr_qty != 0 || $curr_avg_cost != 0 || $curr_dead_cost != 0){
                   //break;
            //    }
            //}
            $item['id'] = $row['supplier_id'];
            $item['name'] = $row['supplier_name'];
            $item['total_sold_qty'] = (isset($item['total_sold_qty']) ? $item['total_sold_qty'] : 0) + $row['total_sold_qty'];
            $data['all']['total_sold_qty'] += $item['total_sold_qty'];
            $item['total_cost'] = (isset($item['total_cost']) ? $item['total_cost'] : 0) + $row['total_cost'];
            $data['all']['total_cost'] += $item['total_cost'];
            $item['total_cost_formated'] = price_format($item['total_cost'], false);
            $data['all']['total_fifo_cost'] += $curr_cost;
            $item['total_fifo_cost'] = $curr_cost;
            $item['total_fifo_cost_formated'] = price_format($item['total_fifo_cost'], false);
            $data['all']['total_dead_cost'] += $curr_dead_cost;
            $item['total_dead_cost'] = $curr_dead_cost;
            $item['total_dead_cost_formated'] = price_format($item['total_dead_cost'], false);
            $data['all']['total_fifo_qty'] += $curr_qty;
            $item['total_fifo_qty'] = $curr_qty;
            $data['all']['avg_stock_cost'] += $curr_avg_cost;
            $item['avg_stock_cost'] = $curr_avg_cost;
            $item['avg_stock_cost_formated'] = price_format($item['avg_stock_cost'], false);
            $item['total_revenue'] = (isset($item['total_revenue']) ? $item['total_revenue'] : 0) + $row['total_revenue'];
            $data['all']['total_revenue'] += $item['total_revenue'];
            $item['total_revenue_formated'] = price_format($item['total_revenue'], false);
            $item['total_gpcost'] = (isset($item['total_gpcost']) ? $item['total_gpcost'] : 0) + $row['total_gpcost'];
            $data['all']['total_gpcost'] += $item['total_gpcost'];
            $item['total_profit'] = (isset($item['total_profit']) ? $item['total_profit'] : 0) + $row['total_profit'];
            $data['all']['total_profit'] += $item['total_profit'];
            $item['total_profit_formated'] = price_format($item['total_profit'], false);
            $item['profit_margin'] = $item['total_revenue'] > 0 ? ($item['total_profit'] / $item['total_revenue']) : 0;
            $item['profit_margin_formated'] = number_format($item['profit_margin'] * 100, 2, '.', '') . '%';
            $item['total_sa_revenue'] = (isset($item['total_sa_revenue']) ? $item['total_sa_revenue'] : 0) + $row['total_sa_revenue'];
            $item['total_sa_revenue'] = $item['total_sa_revenue'] == null ? 0 : $item['total_sa_revenue'];
            $data['all']['total_sa_revenue'] += $item['total_sa_revenue'];
            $item['total_sa_revenue_formated'] = price_format($item['total_sa_revenue'], false);
            $item['total_sa_profit'] = (isset($item['total_sa_profit']) ? $item['total_sa_profit'] : 0) + $row['total_sa_profit'];
            $item['total_sa_profit'] = $item['total_sa_profit'] == null ? 0 : $item['total_sa_profit'];
            $data['all']['total_sa_profit'] += $item['total_sa_profit'];
            $item['total_sa_profit_formated'] = price_format($item['total_sa_profit'], false);
            $item['sa_profit_margin'] = $item['total_sa_revenue'] > 0 ? ($item['total_sa_profit'] / $item['total_sa_revenue']) : 0;
            $item['sa_profit_margin_formated'] = number_format($item['sa_profit_margin'] * 100, 2, '.', '') . '%';
            $item['total_dead_cost_rate_formated'] = round(($item['total_dead_cost']/$item['avg_stock_cost']) * 100).'%';
            $item['display'] = isset($_REQUEST['order_type']) ? $_REQUEST['order_type'] : 4;

            $data[$row['supplier_id']] = $item;
        }

        foreach ($supplier_list as $supplier) {
            if (!isset($data[$supplier['supplier_id']])) {
                $item = array();
                $curr_cost = isset($cost_arr[$supplier['supplier_id']]['cost']) ? $cost_arr[$supplier['supplier_id']]['cost'] : 0;
                $curr_qty = isset($cost_arr[$supplier['supplier_id']]['qty']) ? $cost_arr[$supplier['supplier_id']]['qty'] : 0;
                $curr_avg_cost = isset($avg_cost_arr[$supplier['supplier_id']]) ? $avg_cost_arr[$supplier['supplier_id']] : 0;
                $curr_dead_cost = isset($avg_dead_arr[$supplier['supplier_id']]) ? $avg_dead_arr[$supplier['supplier_id']] : 0;

                if ($house && ($curr_cost == 0 || $curr_qty == 0 || $curr_avg_cost == 0 || $curr_dead_cost == 0)) {
                    continue;
                }
                $item['id'] = $supplier['supplier_id'];
                $item['name'] = $supplier['supplier_name'];

                $item['total_sold_qty'] = 0;
                $item['total_cost'] = 0;
                $item['total_revenue'] = 0;
                $item['total_gpcost'] = 0;
                $item['total_profit'] = 0;
                $item['total_fifo_cost'] = $curr_cost;
                $item['total_fifo_qty'] = $curr_qty;
                $item['profit_margin'] = 0;
                $item['total_sa_revenue'] = 0;
                $item['total_sa_profit'] = 0;
                $item['sa_profit_margin'] = 0;
                $item['avg_stock_cost'] = $curr_avg_cost;
                $data['all']['total_fifo_cost'] += $curr_cost;
                $data['all']['total_fifo_qty'] += $curr_qty;
                $data['all']['avg_stock_cost'] += $curr_avg_cost;
                $data['all']['total_dead_cost'] += $curr_dead_cost;

                $item['total_cost_formated'] = price_format($item['total_cost'], false);
                $item['total_revenue_formated'] = price_format($item['total_revenue'], false);
                $item['total_profit_formated'] = price_format($item['total_profit'], false);
                $item['profit_margin_formated'] = number_format($item['profit_margin'] * 100, 2, '.', '') . '%';
                $item['total_sa_revenue_formated'] = price_format($item['total_sa_revenue'], false);
                $item['total_sa_profit_formated'] = price_format($item['total_sa_profit'], false);
                $item['sa_profit_margin_formated'] = number_format($item['sa_profit_margin'] * 100, 2, '.', '') . '%';
                $item['total_fifo_cost_formated'] = price_format($item['total_fifo_cost'], false);
                $item['avg_stock_cost_formated'] = price_format($item['avg_stock_cost'], false);

                $item['display'] = isset($_REQUEST['order_type']) ? $_REQUEST['order_type'] : 4;

                $data[$supplier['supplier_id']] = $item;
            }
        }
        $data['all']['total_sold_qty'] = $data['all']['total_sold_qty'] > 0 ? $data['all']['total_sold_qty'] : 0;
        $data['all']['total_sa_profit'] = $data['all']['total_sa_profit'] > 0 ? $data['all']['total_sa_profit'] : 0;
        $data['all']['total_sa_revenue'] = $data['all']['total_sa_revenue'] > 0 ? $data['all']['total_sa_revenue'] : 0;
        $data['all']['profit_margin'] = $data['all']['total_revenue'] > 0 ? ($data['all']['total_profit'] / $data['all']['total_revenue']) : 0;
        $data['all']['sa_profit_margin'] = $data['all']['total_sa_revenue'] > 0 ? ($data['all']['total_sa_profit'] / $data['all']['total_sa_revenue']) : 0;
        $data['all']['total_cost_formated'] = price_format($data['all']['total_cost'], false);
        $data['all']['total_fifo_cost_formated'] = price_format($data['all']['total_fifo_cost'], false);
        $data['all']['avg_stock_cost_formated'] = price_format($data['all']['avg_stock_cost'], false);
        $data['all']['total_dead_cost_formated'] = price_format($data['all']['total_dead_cost'], false);
        $data['all']['total_dead_cost_rate_formated'] = round(($data['all']['total_dead_cost']/$data['all']['avg_stock_cost']) * 100).'%';
        $data['all']['total_revenue_formated'] = price_format($data['all']['total_revenue'], false);
        $data['all']['total_profit_formated'] = price_format($data['all']['total_profit'], false);
        $data['all']['profit_margin_formated'] = number_format($data['all']['profit_margin'] * 100, 2, '.', '') . '%';
        $data['all']['total_sa_revenue_formated'] = price_format($data['all']['total_sa_revenue'], false);
        $data['all']['total_sa_profit_formated'] = price_format($data['all']['total_sa_profit'], false);
        $data['all']['sa_profit_margin_formated'] = number_format($data['all']['sa_profit_margin'] * 100, 2, '.', '') . '%';

        foreach ($data as $key => $value) {
            $data[$key]['turnover'] = $data[$key]['total_sa_revenue'] / $data[$key]['avg_stock_cost'] !== false ? number_format($data[$key]['total_sa_revenue'] / $data[$key]['avg_stock_cost'], 3) : "--";
            $data[$key]['turnover_day'] = number_format($days/$data[$key]['turnover'], 2);

            if ($house){
                $data[$key]['inf_turnover'] = $data[$key]['total_cost'] / $data[$key]['avg_stock_cost'] !== false ? number_format($data[$key]['total_cost'] / $data[$key]['avg_stock_cost'], 3) : "--";
                $data[$key]['inf_turnover_day'] = number_format($days/$data[$key]['inf_turnover'], 2);
            }
        }

        $total_discount = 0;

        $total_discount = price_format($total_discount, false);

        $arr = array(
            'data' => $data,
            'total_discount' => $total_discount,
            'filter' => $filter,
            'page_count' => $filter['page_count'],
            'record_count' => $filter['record_count'],
        );
        return $arr;
    }
}
