<?php
# @Date:   2019-06-03T10:28:54+08:00
# @Filename: PaymeController.php
# @Last modified time: 
# @author: Billy@YOHO

namespace Yoho\cms\Controller;

require_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_accounting.php');
require_once(ROOT_PATH . '/includes/lib_time.php');
require_once(ROOT_PATH . '/includes/lib_order.php');
require_once(ROOT_PATH . '/includes/lib_payment.php');
require_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_uuid.php');
use Yoho\cms\Model;

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}


class PaymeController extends YohoBaseController
{
    private $payment_info = null;
    private $orderController = null;
    const PAYME_PAYMENT_REQUEST_EFFECTIVE_DURATION = 600;
    private $user_agent_string = '';

    public function __construct()
    {
        $this->payment_info = get_payment('payme');
        $this->orderController = new OrderController();
        $this->user_agent_string = "Mozilla/5.0 (Linux x86_64) libcurl/".curl_version()["version"]." PHP/".phpversion();
        parent::__construct();
    }

    public function getAccessToken(){
        $user_id = $_SESSION['user_id'];
        $admin_id = $_SESSION['admin_id'];
        if (empty($user_id) && empty($admin_id)) {
            return false;
        }

        $activeAccessToken = $this->getActiveAccessToken($user_id);

        if (!empty($activeAccessToken)){
            $now_time = gmtime();
            if ($activeAccessToken['expiry_datetime'] >= $now_time && (intval(bcsub($activeAccessToken['expiry_datetime'], strval($now_time))) >= (5 * 60) )) { // Not expired // TODO: add window/buffer time to prevent expire-on-arrival
                return json_decode($activeAccessToken['raw_text']);
            }
        }

        $parameter = [
            'client_id' => $this->payment_info['payme_client_id'],
            'client_secret' => $this->payment_info['payme_client_secret'],
        ];

        $request_header = [
            "Content-Type: application/x-www-form-urlencoded",
            "Accept: application/json",
            "Api-Version: ".$this->payment_info['payme_api_version'],
            "Content-Length: " . strlen(http_build_query($parameter)),
        ];

        $host = $this->payment_info['payme_api_base_url'];
        $ch = curl_init($host . '/oauth2/token');
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, $this->user_agent_string);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($parameter));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $request_header);

        $token_request_result = curl_exec($ch);

        if (curl_errno($ch) != 0) // cURL error
        {
            hw_error_log('Cannot connect to PayMe: ' . curl_error($ch));
            $header = $this->getResponseHeader($ch, $token_request_result);
            $this->logPaymeActivityLog(gmtime(), $this->payment_info['payme_api_version'], 'POST /oauth2/token', '-', json_encode($parameter), json_encode($request_header), '', curl_error($ch), $header);
            curl_close($ch);
            return false;
        }
        else
        {     
            $header = $this->getResponseHeader($ch, $token_request_result);
            $token_request_result = $this->getResponseBody($ch, $token_request_result);   
            $token_request_decoded_result = json_decode($token_request_result);    
            curl_close($ch);
            $this->logPaymeActivityLog(gmtime(), $this->payment_info['payme_api_version'], 'POST /oauth2/token', '-', json_encode($parameter), json_encode($request_header), '', $token_request_result, $header);
            if (isset($token_request_decoded_result->accessToken)) {
                $this->insertTokenEntry($token_request_result, $user_id);
            }
            return $token_request_decoded_result;
        }
    }

    public function createPaymentRequest($order, $token)
    {
        require_once ROOT_PATH . ADMIN_PATH . '/includes/lib_accounting.php';
        global $actgHooks;
        $pay_log = $actgHooks->paymentModuleGetPayLog(get_class($this), $order['log_id']);

        // TODO: Find still active payment record and return if exists and remain active
        $payment_id = $order['order_sn'] . $order['log_id'];
        $order_id = $payment_id . '_' . gmtime();
        $active_payment_request = $this->getActivePaymentRequest($order['order_id'], $order['log_id']);
        if (isset($active_payment_request) && !empty($active_payment_request['response_raw_text'])) {
            $raw_texts = explode('||', $active_payment_request['response_raw_text']);
            if (!empty($raw_texts)) {
                return $raw_texts[0];
            }
            return $active_payment_request['response_raw_text'];
        }

        if (!defined('EC_CHARSET'))
        {
            $charset = 'utf-8';
        }
        else
        {
            $charset = EC_CHARSET;
        }

        $message = [
            'totalAmount' => $pay_log['order_amount'],
            'currencyCode' => $pay_log['currency_code'],
            'effectiveDuration' => self::PAYME_PAYMENT_REQUEST_EFFECTIVE_DURATION,
            'merchantData' => [
                'orderId' => $order_id
            ]
        ];

        if (defined('YOHO_MOBILE')) {
            $callback =  return_url('payme') . "&" . http_build_query([
                'order_id' => $order['order_id'],
                'out_trade_no' => $order_id,
                'order_amount' => $pay_log['order_amount'],
                'currency_code' => $pay_log['currency_code']
            ]);
            $message["appSuccessCallback"] = $callback;
            $message["appFailCallback"] = $callback;
        }

        $request_header = [
            '(request-target)' => 'post /payments/paymentrequests',
            'Api-Version' => $this->payment_info['payme_api_version'],
            'Authorization' => 'Bearer ' . $token,
            'Trace-Id' => \UUID::v4(), // Generate UUID
            'Digest' => 'SHA-256=' . $this->getMessageDigest($message),
            'Request-Date-Time' => $this->getGmtTimeString(), // RFC3339/ISO8601 timestamp
        ];

        // Construct signing string
        $headerTitles = array_keys($request_header);
        $request_headers = [];
        foreach($headerTitles as $headerTitle) {
            $request_headers[] = strtolower($headerTitle) . ": " . $request_header[$headerTitle];
        }

        $signature = $this->getRequestSignature($request_headers);
        $request_header['Signature'] = sprintf('keyId="%s",algorithm="hmac-sha256",headers="%s",signature="%s"', $this->payment_info['payme_signing_key_id'], implode(' ', $headerTitles), $signature);
        $request_header['Accept'] = "application/json";
        $request_header['Content-Type'] = "application/json";

        unset($request_header["(request-target)"]);

        // Convert all header first char to upper case
        $sending_request_header = [];
        foreach($request_header as $headerTitle => $value) {
            $sending_request_header[] = $headerTitle . ": " . $value;
        }

        // Prepare payme request
        $host = $this->payment_info['payme_api_base_url'];
        $post_message = json_encode($message, JSON_PRESERVE_ZERO_FRACTION);
        $ch = curl_init($host . '/payments/paymentrequests');
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, $this->user_agent_string);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_message);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $sending_request_header);
        
        $payment_request_result = curl_exec($ch);

        if (curl_errno($ch) != 0) // cURL error
        {
            $header = $this->getResponseHeader($ch, $payment_request_result);
            hw_error_log('Cannot connect to PayMe: ' . curl_error($ch));
            $this->logPaymeActivityLog(gmtime(), $this->payment_info['payme_api_version'], 'POST /payments/paymentrequests', $order_id, json_encode($post_message), json_encode($sending_request_header), $request_header['Signature'], curl_error($ch), $header);
            curl_close($ch);
            return false;
        }
        else
        {      
            $header = $this->getResponseHeader($ch, $payment_request_result);
            $payment_request_result = $this->getResponseBody($ch, $payment_request_result);      
            curl_close($ch);

            $payment_request_decoded_result = json_decode($payment_request_result);
            if (empty($payment_request_decoded_result->errors)) {
                // Insert the return payment request into record
                $request_data = json_encode([
                    "header" => $request_header,
                    "message" => $message
                ], JSON_PRESERVE_ZERO_FRACTION);
                $this->insertPaymentRequestEntry($request_data, $payment_request_result, $order);
            }
            $this->logPaymeActivityLog(gmtime(), $this->payment_info['payme_api_version'], 'POST /payments/paymentrequests', $order_id, json_encode($post_message), json_encode($sending_request_header), $request_header['Signature'], json_encode($payment_request_result), json_encode($header));

            return $payment_request_result;
        }

        
    }

    public function getPaymentRequestInfo($request_id, $token)
    {
        $request_header = [
            '(request-target)' => 'get /payments/paymentrequests/'.$request_id,
            'Api-Version' => $this->payment_info['payme_api_version'],
            'Authorization' => 'Bearer ' . $token,
            'Trace-Id' => \UUID::v4(), // Generate UUID
            'Request-Date-Time' => $this->getGmtTimeString(), // RFC3339/ISO8601 timestamp
        ];

        // Construct signing string
        $headerTitles = array_keys($request_header);
        $header = [];
        foreach($headerTitles as $headerTitle) {
            $header[] = strtolower($headerTitle) . ": " . $request_header[$headerTitle];
        }

        $signature = $this->getRequestSignature($header);
        $request_header['Signature'] = sprintf('keyId="%s",algorithm="hmac-sha256",headers="%s",signature="%s"', $this->payment_info['payme_signing_key_id'], implode(' ', $headerTitles), $signature);
        $request_header['Accept'] = "application/json";
        $request_header['Content-Type'] = "application/json";

        unset($request_header["(request-target)"]);

        // Convert all header first char to upper case
        $header = [];
        foreach($request_header as $headerTitle => $value) {
            $header[] = $headerTitle . ": " . $value;
        }

        // Prepare payme request
        $host = $this->payment_info['payme_api_base_url'];
        $ch = curl_init($host . '/payments/paymentrequests/'.$request_id);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, $this->user_agent_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

        // Get Out trade no for reference
        $out_trade_no = $this->getOutTradeNoFromPaymentRequestId($request_id);
        $payment_request_query_result = curl_exec($ch);

        // Incase of error
        if (curl_errno($ch) != 0) // cURL error
        {
            $header = $this->getResponseHeader($ch, $payment_request_query_result);
            hw_error_log('Cannot connect to Payme Service: ' . curl_error($ch));
            $this->logPaymeActivityLog(gmtime(), $this->payment_info['payme_api_version'], 'GET /payments/paymentrequests/'.$request_id, $out_trade_no, "", json_encode($header), $request_header['Signature'], curl_error($ch), $header);
            curl_close($ch);
            return json_encode([
                'status' => 0,
                'error' => 'Query failed: Unable to connect to Payment Service'
            ]);
        }
        else
        {
            $header = $this->getResponseHeader($ch, $payment_request_query_result);
            $payment_request_query_result = $this->getResponseBody($ch, $payment_request_query_result);
            curl_close($ch);
            $this->logPaymeActivityLog(gmtime(), $this->payment_info['payme_api_version'], 'GET /payments/paymentrequests/'.$request_id, $out_trade_no, "", json_encode($header), $request_header['Signature'], $payment_request_query_result, json_encode($header));
            return $payment_request_query_result;
        }
    }

    public function getLogIdByPaymentRequestId($request_id) {
        $query = "SELECT log_id FROM " . $GLOBALS['ecs']->table('payme_payment_request_record') . " WHERE gateway_payment_request_id = '$request_id'";

        $result = $GLOBALS['db']->getOne($query);
        if (!isset($result) || empty($result)) {
            return false;
        } else {
            return $result;
        }
    }

    public function insertStatusIntoPaymentRequestRecord($payment_request_query_result, $log_id = null)
    {
        $result_data = json_decode($payment_request_query_result, false);
        if (!isset($result_data) || empty($result_data)) {
            return false;
        }

        $param = [
            'gateway_currency_code' => $result_data->currencyCode,
            'gateway_payment_request_id' => $result_data->paymentRequestId,
            'gateway_total_amount' => $result_data->totalAmount,
            'gateway_payment_request_status_code' => $result_data->statusCode,
            'gateway_payment_request_status_description' => $result_data->statusDescription,
            'gateway_transaction_id' => !empty($result_data->transactions) ? implode(',', $result_data->transactions) : '',
            'gateway_refund_id' => !empty($result_data->refundId) ? $result_data->refundId : '',
            'gateway_payer_id' => !empty($result_data->payerId) ? $result_data->payerId : ''
        ];

        return $this->updateExistingPaymentRequestRecord($param, $payment_request_query_result, $log_id);
    }

    public function getPaymeActivityLog()
    {
        global $ecs, $db;

        $filter['start_date'] = empty($_REQUEST['start_date']) ? null : $_REQUEST['start_date'];
        $filter['end_date'] = empty($_REQUEST['end_date']) ? (empty($filter['start_date']) ? 0 : $filter['start_date']) : $_REQUEST['end_date'];
        $filter['out_trade_no'] = empty($_REQUEST['out_trade_no']) ? null : $_REQUEST['out_trade_no'];
        $start = empty($_REQUEST['start']) ? 0 : intval($_REQUEST['start']);
        $page_size = empty($_REQUEST['page_size']) ? 50 : intval($_REQUEST['page_size']);

        $where = (empty($filter['out_trade_no']) ? "" : " AND out_trade_code like '%$filter[out_trade_no]%' ") .
                (empty($filter['start_date']) ? "" : " AND DATE(CONVERT_TZ(FROM_UNIXTIME(activity_initiate_datetime), '+00:00', '+08:00')) BETWEEN '$filter[start_date] 00:00:00' AND '$filter[end_date] 23:59:59' ");

        $query = "SELECT * FROM " . $ecs->table('payme_activity_log');
        if (!empty($where)) {
            $query .= " WHERE 1 " . $where;
        }
        $query .= " ORDER BY activity_initiate_datetime DESC, payme_activity_log_id DESC LIMIT $start, $page_size";

        $count_query = "SELECT count(*) FROM " . $ecs->table('payme_activity_log') . " WHERE 1" . $where;

        $result = $db->getAll($query);
        $count = $db->getOne($count_query);

        foreach($result as $index => $res) {
            $result[$index]['datetime'] = \local_date('Y-m-d H:i:s', $res['activity_initiate_datetime']);
        }

        return ['data' => $result, 'record_count' => $count];
    }

    public function ajaxQueryAction($list, $totalCount, $action_btn = ['edit', 'remove'], $extraData = [])
    {
        parent::ajaxQueryAction($list['data'], $list['record_count'], false, $extra_data);
    }

    private function getMessageDigest(Array $message) 
    {
        return base64_encode(hash('sha256', utf8_encode(json_encode($message, JSON_PRESERVE_ZERO_FRACTION)), true));
    }

    private function getRequestSignature(Array $header) 
    {
        $data = utf8_encode(implode("\n", $header));
        $sign = \base64_encode(hash_hmac('sha256', $data, \base64_decode($this->payment_info['payme_signing_key']), true));
        return $sign;
    }

    private function getGmtTimeString()
    {
        return sprintf("%s.%sZ", gmdate("Y-m-d\TH:i:s"), bcmul(microtime() * 1, '1000', 0));
    }

    private function insertTokenEntry($entry, $user_id)
    {
        global $ecs;
        $data = json_decode($entry);

        $query = "INSERT INTO " . $ecs->table('payme_token') . " (user_id, token, expiry_datetime, raw_text) VALUES ('$user_id', '$data->accessToken', " . gmstr2time($data->expiresOn) . ", '" . json_encode($data) . "')";

        $GLOBALS['db']->query($query);
        return $GLOBALS['db']->affected_rows();
    }

    private function getActiveAccessToken($user_id)
    {
        global $ecs;

        $time = gmtime();
        $query = "SELECT * FROM " . $ecs->table('payme_token') . " ORDER BY expiry_datetime DESC";
        
        $query_result = $GLOBALS['db']->getRow($query);
        return $query_result;
    }

    private function insertPaymentRequestEntry($request, $response, $order)
    {
        $request_data = json_decode($request);
        $response_data = json_decode($response);
        $sql_insert_query = "INSERT INTO " . $GLOBALS['ecs']->table('payme_payment_request_record') . " (order_id, user_id, log_id, payment_trace_id, out_trade_no, gateway_payment_request_id, gateway_total_amount, gateway_currency_code, gateway_payment_request_creation_datetime, gateway_payment_request_duration, gateway_payment_request_web_link, gateway_payment_request_status_code, gateway_payment_request_status_description, request_raw_text, response_raw_text) VALUES ("
            . '"' . $order['order_id'] . '"' . ", "
            . '"' . $order['user_id'] . '"' . ", "
            . '"' . $order['log_id'] . '"' . ", "
            . '"' . $request_data->header->{"Trace-Id"} . '"' . ", "
            . '"' . $request_data->message->merchantData->orderId . '"' . ", "
            . '"' . $response_data->paymentRequestId . '"' . ", "
            . '"' . $response_data->totalAmount . '"' . ", "
            . '"' . $response_data->currencyCode . '"' . ", "
            . '"' . gmstr2time($response_data->createdTime) . '"' . ", "
            . '"' . $response_data->effectiveDuration . '"' . ", "
            . '"' . $response_data->webLink . '"' . ", "
            . '"' . $response_data->statusCode . '"' . ", "
            . '"' . $response_data->statusDescription . '", ' 
            . "'" . $request . "', " 
            . "'" . $response . "'" 
            . ") ";

            $GLOBALS['db']->query($sql_insert_query);
            return $GLOBALS['db']->affected_rows();
    }

    private function getActivePaymentRequest($order_id, $log_id, $interval = 185)
    {
        if (empty($order_id) || empty($log_id)) {
            return false;
        }

        $active_payment_request_query = "SELECT * FROM " . $GLOBALS['ecs']->table('payme_payment_request_record') . " WHERE order_id = '$order_id' AND log_id = '$log_id' AND `gateway_payment_request_status_code` <> '' ORDER BY gateway_payment_request_creation_datetime";
        $active_payment_request = $GLOBALS['db']->getAll($active_payment_request_query);

        $latest_active_payment_request = null;
        $token = $this->getAccessToken();
        if (!isset($token) || empty($token)) {
            return false;
        }
        $token = $token->accessToken;

        foreach($active_payment_request as $key => $item)
        {
            // Skip all PayCode in Ending state (Not in PR001(Payment Request Initiated))
            $continue_status_code = [
                'PR001', // Payment Request Initiated
                'PR005', // Paid
            ];
            
            if (!in_array($item['gateway_payment_request_status_code'], $continue_status_code)) {
                continue;
            }

            // // Filter out all expired record
            // if (($item['gateway_payment_request_creation_datetime'] + $item['gateway_payment_request_duration']) - gmtime() < 0) {
            //     $param = [
            //         'currencyCode' => $item['gateway_currency_code'],
            //         'paymentRequestId' => $item['gateway_payment_request_id'],
            //         'totalAmount' => $item['gateway_total_amount'],
            //         'statusCode' => 'PR007',
            //         'statusDescription' => 'Payment Request Expired (System Internally)'
            //     ];

            //     // Update the payment request for the expired payment request
            //     $this->insertStatusIntoPaymentRequestRecord(json_encode($param));
            //     continue;
            // }
            
            $result = $this->getPaymentRequestInfo($item['gateway_payment_request_id'], $token);
            $result_data = json_decode($result);

            if (isset($result_data->status) && $result_data->status == 0) {
                continue;
            }

            if ($result_data->statusCode == 'PR005') {
                $item['gateway_payment_request_status_code'] = $result_data->statusCode;
                $item['gateway_payment_request_status_description'] = $result_data->statusDescription;
                $item['response_raw_text'] = $result . '||' . $item['response_raw_text'];

                $transaction_ids = isset($result_data->transactions) ? implode(",", $result_data->transactions) : $result_data->paymentRequestId;
                $action_note = 'payme' . ':' . $transaction_ids . '（' . $result_data->currencyCode . ($result_data->totalAmount) . '）';
                // Update the paymennt request for complete the payment request
                $log_id = $this->getLogIdByPaymentRequestId($result_data->paymentRequestId);
                if (!empty($log_id)) {
                    $this->orderController->order_payment_transaction_log($log_id, $this->payment_info['pay_id'], $transaction_ids, $result_data->currencyCode,($result_data->totalAmount),0);
                    $this->insertStatusIntoPaymentRequestRecord($result);
                    order_paid($log_id,PS_PAYED,$action_note);
                }
                $latest_active_payment_request = $item;
            } elseif ($result_data->statusCode == 'PR007') {
                // Update the payment request for the expired payment request
                $this->insertStatusIntoPaymentRequestRecord($result);
            } else {
                // Default case: if the returned case is not ending "PR005 - Complete" and "PR007 - Expired" set the selected one as latest active
                $latest_active_payment_request = $item;
            }
        }
        if ((($latest_active_payment_request['gateway_payment_request_creation_datetime'] + $latest_active_payment_request['gateway_payment_request_duration']) - gmtime()) >= $interval) { // There is enough remaining duration
            return $latest_active_payment_request;
        } else {
            return false;
        }
    }

    private function updateExistingPaymentRequestRecord($param, $raw_data, $log_id = null)
    {
        $update_column_value = '';

        $param_count = count($param);
        if ($param_count === 0 || (empty($param['gateway_payment_request_id']) && $log_id === null)) {
            return false;
        }

        // Fetch the current transaction record
        $sql = 'SELECT * FROM ' . $GLOBALS['ecs']->table('payme_payment_request_record');

        $where = '';

        if (!empty($param['gateway_payment_request_id'])) {
            $where = " WHERE gateway_payment_request_id = '$param[gateway_payment_request_id]'";
        }
        else if ($log_id !== null) {
            $where = " WHERE log_id = '$log_id'";
        }

        if (!empty($where)) {
            $sql .= $where;
        }

        $payme_payment_request_log = $GLOBALS['db']->getRow($sql);
        if (empty($payme_payment_request_log)) {
            return false;
        }

        $fields = [
            'gateway_currency_code',
            'gateway_payment_request_id',
            'gateway_total_amount',
            'gateway_payment_request_status_code',
            'gateway_payment_request_status_description',
            'gateway_transaction_id',
            'gateway_refund_id',
            'gateway_payer_id'
        ];

        // Prepare and determine if update is required
        foreach($fields as $field) {
            if (!empty($param[$field]) && $payme_payment_request_log[$field] != $param[$field]) {
                if (!empty($update_column_value)) {
                    $update_column_value .= ', ';
                }
                if ($field === 'gateway_refund_id' || $field === 'gateway_payer_id') {
                    if (empty($payme_payment_request_log[$field])) {
                        $update_column_value .= $field . ' = "' . $param[$field] . '"';
                    } else {
                        $update_column_value .= $field . " = concat($field, ', ', '" . $param[$field] . "')";
                    }
                } else {
                    $update_column_value .= $field . ' = "' . $param[$field] . '"';
                }
            }
        }

        if (empty($update_column_value)) {
            return false;
        }

        // Append the raw string into table raw_text field
        $update_column_value .= ', response_raw_text = concat(response_raw_text, "||", \'' . json_encode(json_decode($raw_data, false)) . '\')';

        // Update corresponding database record with concatenation of raw string
        $update_sql_query = 'UPDATE ' . $GLOBALS['ecs']->table('payme_payment_request_record') . " SET $update_column_value";
        if (!empty($where)) {
            $update_sql_query .= $where;
        }

        $GLOBALS['db']->query($update_sql_query);
        return $GLOBALS['db']->affected_rows();
    }

    private function getOutTradeNoFromPaymentRequestId($payment_request_id)
    {
        global $ecs, $db;
        $query = "SELECT out_trade_no FROM " . $ecs->table('payme_payment_request_record') . " WHERE gateway_payment_request_id = '$payment_request_id'";
        $result = $db->getOne($query);
        return $result;
    }

    /**
     * Insert an entry into database for record payme api call activity
     * 
     * @param $activity_init_datetime
     * @param $api_version
     * @param $activity_type
     * @param $route
     * @param $out_trade_code
     * @param $raw_text
     * @param $request_header
     * @param $request_signature
     * 
     * @return mixed
     */
    private function logPaymeActivityLog($activity_init_datetime, $api_version, $route, $out_trade_code = '-', $request_raw_text = '', $request_header = '', $request_signature = '', $response_raw_text = '', $response_header = '', $response_signature = '')
    {
        GLOBAL $ecs, $db;
        $query = "INSERT INTO " . $ecs->table('payme_activity_log') . " (activity_initiate_datetime, api_version, route, out_trade_code, request_raw_text, request_header, request_signature, response_raw_text, response_header, response_signature) VALUES (" 
            . "'$activity_init_datetime'" . ", "
            . "'$api_version'" . ", "
            . "'$route'" . ", "
            . "'$out_trade_code'" . ", "
            . "'$request_raw_text'" . ", "
            . "'$request_header'" . ", "
            . "'$request_signature'" . ", "
            . "'$response_raw_text'" . ", "
            . "'$response_header'" . ", "
            . "'$response_signature'"
            . ")";

        $res = $db->query($query);
        return $res;
    }

    private function getResponseHeader($curl_client, $body)
    {
        $header_size = curl_getinfo($curl_client, CURLINFO_HEADER_SIZE);
        return substr($body, 0, $header_size);
    }

    private function getResponseBody($curl_client, $body)
    {
        $header_size = curl_getinfo($curl_client, CURLINFO_HEADER_SIZE);
        return substr($body, $header_size);
    }

    private function get_payment_log($log_id)
    {
        $sql = "SELECT * FROM " . $GLOBALS['ecs']->table('pay_log') .
            " WHERE log_id = '$log_id'";
        return $GLOBALS['db']->getRow($sql);
    }

    /**
     *  Refund a transaction with designated amount
     */
    public function refund_transaction($log_id, $amount, $gateway_transaction_id)
    {
        $token = $this->getAccessToken();
        if (!isset($token) || empty($token)) {
            return false;
        }
        $token = $token->accessToken;

        $payment_log = $this->get_payment_log($log_id);
        if (empty($payment_log)) {
            return json_encode([
                'error' => '1',
                'message' => 'Payment Log Not Found',
                'content' => ''
            ]);
        }

        $payment_request_record_query = "SELECT gateway_transaction_id, response_raw_text, out_trade_no FROM " . $GLOBALS['ecs']->table('payme_payment_request_record') . " WHERE log_id = '" . $log_id . "'";
        $payment_request_record_query_result = $GLOBALS['db']->getRow($payment_request_record_query);
        if (empty($payment_request_record_query_result)) {
            return json_encode([
                'error' => '1',
                'message' => 'WeChat Pay Log Entry Not Found',
                'content' => ''
            ]);
        }

        $transactionId = $gateway_transaction_id;

        $message = [
            'amount' => (string) $amount,
            'currencyCode' => 'HKD',
            'reasonCode' => '00'
        ];

        $requestHeader = [
            '(request-target)' => "post /payments/transactions/$transactionId/refunds",
            'Api-Version' => $this->payment_info['payme_api_version'],
            'Authorization' => "Bearer $token",
            'Trace-Id' => \UUID::v4(),
            'Digest' => 'SHA-256=' . $this->getMessageDigest($message),
            'Request-Date-Time' => $this->getGmtTimeString(),
        ];

        $requestHeaders = [];
        $headerTitles = [];
        foreach($requestHeader as $key => $value) {
            $headerTitles[] = $key;
            $requestHeaders[] = mb_strtolower($key) . ': ' . $value;
        }

        $signature = $this->getRequestSignature($requestHeaders);
        $requestHeader['Signature'] = sprintf('keyId="%s",algorithm="hmac-sha256",headers="%s",signature="%s"', $this->payment_info['payme_signing_key_id'], implode(' ', $headerTitles), $signature);
        $requestHeader['Accept'] = 'application/json';
        $requestHeader['Content-Type'] = 'application/json';

        unset($requestHeader['(request-target)']);

        $sendingRequestHeader = [];
        foreach($requestHeader as $headerTitle => $value) {
            $sendingRequestHeader[] = $headerTitle . ': ' . $value;
        }

        // Prepare payme request
        $host = $this->payment_info['payme_api_base_url'];
        $post_message = json_encode($message, JSON_PRESERVE_ZERO_FRACTION);
        $ch = curl_init($host . "/payments/transactions/$transactionId/refunds");
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, $this->user_agent_string);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_message);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $sendingRequestHeader);

        $refund_request_result = curl_exec($ch);

        if (curl_errno($ch) != 0) // cURL error
        {
            $header = $this->getResponseHeader($ch, $refund_request_result);
            hw_error_log('Cannot connect to PayMe: ' . curl_error($ch));
            $this->logPaymeActivityLog(gmtime(), $this->payment_info['payme_api_version'], "POST /payments/transactions/$transactionId/refunds", $payment_request_record_query_result['out_trade_no'], json_encode($post_message), json_encode($sendingRequestHeader), $requestHeader['Signature'], curl_error($ch), $header);
            curl_close($ch);
            return false;
        }

        // Begin parse sync response
        // Serarate response headers and body
        $header = $this->getResponseHeader($ch, $refund_request_result);
        $refund_request_result = $this->getResponseBody($ch, $refund_request_result);
        $this->logPaymeActivityLog(gmtime(), $this->payment_info['payme_api_version'], "POST /payments/transactions/$transactionId/refunds", $payment_request_record_query_result['out_trade_no'], json_encode($post_message), json_encode($sendingRequestHeader), $requestHeader['Signature'], $refund_request_result, $header);
        curl_close($ch);

        $refundRequestDecodedResult = json_decode($refund_request_result, false);

        if (!empty($refundRequestDecodedResult->errors)) {
            return json_encode([
                'error' => '1',
                'message' => $refundRequestDecodedResult,
                'content' => ''
            ]);
        }

        $this->insertStatusIntoPaymentRequestRecord($refund_request_result, $log_id);

        return json_encode([
            'error' => '0',
            'message' => $refund_request_result,
            'content' => ''
        ]);
    }

    protected function get_pay_log_id_by_transaction_id($gateway_transaction_id)
    {
        if (!$this->check_if_payme_pay_log_exists($gateway_transaction_id)) {
            return false;
        }

        $payme_pay_log = $this->get_pay_log_by_transaction_id($gateway_transaction_id);
        return $payme_pay_log['log_id'];
    }

    protected function check_if_payme_pay_log_exists($transactionId)
    {
        $payme_pay_log = $this->get_pay_log_by_transaction_id($transactionId);
        return !empty($payme_pay_log);
    }

    private function get_pay_log_by_transaction_id($gateway_transaction_id)
    {
        $sql = "SELECT * FROM " . $GLOBALS['ecs']->table('payme_payment_request_record') .
            " WHERE gateway_transaction_id = '$gateway_transaction_id' OR response_raw_text LIKE '%$gateway_transaction_id%'"; // For compatibility concern

        return $GLOBALS['db']->getRow($sql);
    }

    public function clearOldActivityLogEntry()
    {
        global $ecs, $db;
        $thresholdDatetime = new \DateTime();
        $thresholdDatetime->setTimestamp(gmtime());
        $thresholdDatetime->setTime(0, 0, 0);
        $thresholdDatetime->sub(new \DateInterval('P14D'));
        $statement = "DELETE FROM " . $ecs->table('payme_activity_log') . " WHERE (activity_initiate_datetime < '" . $thresholdDatetime->getTimeStamp() . "')";
        die(); // Prevent from acciddental execution
        $db->query($statement);
    }

    public function updateOrderPaymeStatus($order_id, $log_id)
    {
        if (empty($order_id) || empty($log_id)) {
            return false;
        }

        $active_payment_request_query = "SELECT * FROM " . $GLOBALS['ecs']->table('payme_payment_request_record') . " WHERE order_id = '$order_id' AND log_id = '$log_id' AND `gateway_payment_request_status_code` <> '' ORDER BY gateway_payment_request_creation_datetime";
        $active_payment_request = $GLOBALS['db']->getAll($active_payment_request_query);

        $latest_active_payment_request = null;
        $token = $this->getAccessToken();
        if (!isset($token) || empty($token)) {
            return false;
        }
        $token = $token->accessToken;

        foreach($active_payment_request as $key => $item)
        {
            // Skip all PayCode in Ending state (Not in PR001(Payment Request Initiated))
            $continue_status_code = [
                'PR001', // Payment Request Initiated
                'PR005', // Paid
            ];
            
            if (!in_array($item['gateway_payment_request_status_code'], $continue_status_code)) {
                continue;
            }
            
            $result = $this->getPaymentRequestInfo($item['gateway_payment_request_id'], $token);
            $result_data = json_decode($result);

            if (isset($result_data->status) && $result_data->status == 0) {
                continue;
            }

            if ($result_data->statusCode == 'PR005') {
                $item['gateway_payment_request_status_code'] = $result_data->statusCode;
                $item['gateway_payment_request_status_description'] = $result_data->statusDescription;
                $item['response_raw_text'] = $result . '||' . $item['response_raw_text'];

                $transaction_ids = isset($result_data->transactions) ? implode(",", $result_data->transactions) : $result_data->paymentRequestId;
                $action_note = 'payme' . ':' . $transaction_ids . '（' . $result_data->currencyCode . ($result_data->totalAmount) . '）';
                // Update the paymennt request for complete the payment request
                $log_id = $this->getLogIdByPaymentRequestId($result_data->paymentRequestId);
                if (!empty($log_id)) {
                    $this->orderController->order_payment_transaction_log($log_id, $this->payment_info['pay_id'], $transaction_ids, $result_data->currencyCode,($result_data->totalAmount),0);
                    $this->insertStatusIntoPaymentRequestRecord($result);
                    order_paid($log_id,PS_PAYED,$action_note);
                }
                return true;
            } elseif ($result_data->statusCode == 'PR007') {
                // Update the payment request for the expired payment request
                $this->insertStatusIntoPaymentRequestRecord($result);
            } else {
                // Default case: if the returned case is not ending "PR005 - Complete" and "PR007 - Expired" , no action will be performed
            }
        }
        return false;
    }

    public function refund_by_transaction_id_and_amount($gateway_transaction_id, $amount)
    {
        if (empty($gateway_transaction_id) || empty($amount)) {
            return false;
        }

        $log_id = $this->get_pay_log_id_by_transaction_id($gateway_transaction_id);

        $refund_result = json_decode($this->refund_transaction($log_id, $amount, $gateway_transaction_id), false);

        return ($refund_result->error === '0');
    }

}

?>
