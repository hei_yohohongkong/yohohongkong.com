<?php
/***
* InsuranceController.php
* 20190828
*
***/
namespace Yoho\cms\Controller;
use Yoho\cms\Model;
if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

class InsuranceController extends YohoBaseController{

    public function __construct(){
		parent::__construct();
    }

    public function getInsList(){
        global $ecs, $db;
        $sql = "SELECT i.insurance_id, i.name, i.code, il.name AS name_en " .
                " FROM " . $ecs->table('insurance') . " AS i " .
                " LEFT JOIN " . $ecs->table('insurance_lang') . " AS il ON il.insurance_id = i.insurance_id " .
                " WHERE il.lang = 'en_us' " .
                " GROUP BY i.insurance_id " . 
                " ORDER BY i.insurance_id ASC ";
        $result = $db->getAll($sql);
        return $result;
    }

    public function getIns($arr_where = array(), $order = "insurance_id ASC", $page_size = 50, $offset = 0){
        global $ecs, $db;
        if(!empty($arr_where)){
            $where = " WHERE " . implode(" AND ", $arr_where);
        } else {
            $where = "";
        }
        if(!empty($order)){
            $order = " ORDER BY " . $order;
        }

        $limit = " LIMIT " . $page_size;
        $offset = " OFFSET " . $offset;
        //$sql = "SELECT i.insurance_id, i.name, GROUP_CONCAT(i.duration) AS duration, COUNT(ig.insurance_goods_id) AS count_goods, 0 AS count_bought, 0 AS count_claimed, i.status " .
        $sql = "SELECT i.insurance_id, i.name, GROUP_CONCAT(i.duration) AS duration, i.status, i.code " .
                " FROM " . $ecs->table('insurance') . " AS i ";
        if (!empty($where)){$sql .= $where . " ";}
        $sql .= " GROUP BY insurance_id ";
        if (!empty($order)){$sql .= $order . " ";}
        $sql .= $limit . " " . $offset;
        $result = $db->getAll($sql);
        if(empty($result)){
            return FALSE;
        } else {
            foreach ($result as $key => $val){
                $sql = "SELECT COUNT(insurance_goods_id) AS count_goods " .
                        " FROM " . $ecs->table('insurance_goods') . " " .
                        " WHERE insurance_id = '" . $val['insurance_id'] . "' ";
                        " GROUP BY insurance_id ";
                $result_count = $db->getOne($sql);
                $result[$key]['count_goods'] = $result_count;

                $sql = "SELECT COUNT(ic.claim_id) AS count_claims " .
                        " FROM " . $ecs->table('insurance_claim') . " AS ic " .
                        " LEFT JOIN " . $ecs->table('order_goods') . " AS og ON og.rec_id = ic.order_goods_id " .
                        " LEFT JOIN " . $ecs->table('insurance') . " AS i ON i.target_goods_id = og.goods_id " .
                        " WHERE i.insurance_id = '" . $val['insurance_id'] . "' ".
                        " GROUP BY insurance_id ";
                $result_count = $db->getOne($sql);
                $result[$key]['count_claimed'] = $result_count;

                $sql = "SELECT SUM(og.goods_number) AS count_bought " .
                        " FROM " . $ecs->table('order_goods') . " AS og  " .
                        " LEFT JOIN " . $ecs->table('insurance') . " AS i ON i.target_goods_id = og.goods_id " .
                        " LEFT JOIN " . $ecs->table('order_info') . " AS oi ON oi.order_id = og.order_id " .
                        " WHERE i.insurance_id = '" . $val['insurance_id'] . "' AND oi.pay_status = 2 ".
                        " AND og.is_insurance = 1 ";
                        " GROUP BY insurance_id ";
                $result_count = $db->getOne($sql);
                if (empty($result_count)){
                    $result_count = 0;
                }
                $result[$key]['count_bought'] = $result_count;

            }
            return $result;
        }
    }

    public function checkInsExisted($id, $duration){
        global $ecs, $db;
        $sql = "SELECT insurance_id " .
                "FROM " . $ecs->table('insurance').
                "WHERE insurance_id = '".$id."' ".
                "AND duration = '".$duration."' ";
        $result = $db->getRow($sql);
        if (empty($result)){
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function countIns(){
        global $ecs, $db;
        $sql = "SELECT COUNT(insurance_id) " .
                " FROM " . $ecs->table('insurance');
        $result = $db->getOne($sql);
        return $result;
    }

    public function insuranceEditAction($table = 'insurance'){
        parent::ajaxEditAction($table);
        /*
        global $ecs, $db, $_LANG;
        $col   = $_REQUEST['col'];
        $value = $_REQUEST['value'];
        $attribute = $_REQUEST['mkey'];
        $key   = $_REQUEST['id'];
        //$type  = $_REQUEST['type'];

        $sql = "UPDATE ".$ecs->table($table)." ".
                'SET '.$col.' = "'.$value.'" '.
                "WHERE ".$attribute." = ".$key;

        $db->query($sql);
        */
    }

    public function insertInsurance ($name = '', $name_en = '', $code='',  $duration = array(), $id = -1, $return_id = FALSE){
        global $ecs, $db, $_LANG;
        if (!$name || !$code || empty($duration)){
            return FALSE;
        } else {
            $name = mysql_real_escape_string($name);
            $name_en = mysql_real_escape_string($name_en);
            if (is_array($duration)){
                $duration = implode('^', $duration);
            }

            if ($id <= 0){
                $sql = "INSERT INTO ".$ecs->table('insurance')." ".
                "(duration, name, code, status) ".
                "VALUES ('".$duration."', '".$name."', '".$code."', 1)";
            } else {
                $sql = "INSERT INTO ".$ecs->table('insurance')." ".
                "(insurance_id, duration, name, code, status) ".
                "VALUES ('".$id."', '".$duration."', '".$name."', '".$code."', 1)";
            }

            $db->query($sql);

            if ($id <= 0){
                $ins_id = $db->insert_id();
                $db->query("INSERT INTO ".$ecs->table('insurance_lang')." ".
                            "(`insurance_id`, `lang`, `name`) ".
                            "VALUES ".
                            "('".$ins_id."', 'zh_cn', '".translate_to_simplified_chinese($name)."'), ".
                            "('".$ins_id."', 'en_us', '".$name_en."')"
                        );
            } else {
                $ins_id = $id;
            }
            
            if ($ins_id){
                require_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_goods.php');
                $max_id   = $db->getOne("SELECT MAX(goods_id) + 1 FROM ".$ecs->table('goods'));
                $goods_sn   = generate_goods_sn($max_id);

                $goods_name_zhtw = $name." - ".$duration.$_LANG['month_extended_warranty_zhtw'];
                $goods_name_en = $name_en." - ".$duration.$_LANG['month_extended_warranty_en'];
                $goods_name_zhcn = translate_to_simplified_chinese($goods_name_zhtw);

                $sql_new_good = "INSERT INTO ".$ecs->table('goods')." ".
                                "(`goods_id`, `cat_id`, `goods_sn`, `goods_name`, `goods_name_style`, `click_count`, `brand_id`, `provider_name`, `goods_number`, `goods_weight`, 
                                `market_price`, `shop_price`, `promote_price`, `promote_start_date`, `promote_end_date`, `warn_number`, `keywords`, `goods_brief`, `goods_desc`, 
                                `goods_thumb`, `goods_img`, `original_img`, `is_real`, `extension_code`, `is_on_sale`, `is_alone_sale`, `is_shipping`, `integral`, `add_time`, 
                                `sort_order`, `is_delete`, `is_hidden`, `is_best`, `is_new`, `is_hot`, `is_promote`, `bonus_type_id`, `last_update`, `goods_type`, `seller_note`, 
                                `warranty_template_id`, `give_integral`, `rank_integral`, `suppliers_id`, `is_check`, `is_pre_sale`, `pre_sale_date`, `pre_sale_days`, `is_purchasing`, 
                                `is_promote_cart`, `guarantee`, `inthebox`, `official_website`, `unboxing_page`, `country_of_origin`, `commission`, `cost`, `sales`, `sales_adjust`, 
                                `shipping_price`, `proddb_id`, `proddb_sync_time`, `shipping_note`, `shipping_template_id`, `is_best_sellers`, `last_stocktake_time`, `goods_length`, 
                                `goods_width`, `goods_height`, `display_discount`, `require_goods_id`, `auto_pricing`, `is_fls_warehouse`, `pre_sale_time`, `pre_sale_string`, 
                                `price_strategy`, `perma_link`, `is_insurance`) ".
                                "VALUES (".
                                "'".$goods_sn."', '181', '".$goods_sn."', '".$goods_name_zhtw."', '+', '0', '0', '', '0', '0.000', '0.00', '0.00', '0.00', '0', '0', '1', '', '', '', 'yohohk/img/insurance/thumb_img/".$code.".png', 'yohohk/img/insurance/goods_img/".$code.".png', '', '1', '', '1', '0', 
                                '0', '0', '0', '100', '0', '1', '0', '0', '0', '0', '0', '0', '0', '', '0', '-1', '-1', NULL, NULL, '0', '0', '0', '0', '0', '', '', '', '', '', '0', 
                                '0.00', '0', '0', '0.00', '0', '0', '', '0', '0', NULL, '0.000', '0.000', '0.000', '1', '0', '0', '0', '0', '0', '0', NULL, '1')";
                $db->query($sql_new_good);

                $good_id = $db->insert_id();

                $sql = "INSERT INTO ".$ecs->table('erp_goods_attr')." ".
                "(`goods_id`, `goods_attr_id_list`, `goods_qty`, `attr_id_no_dynamic`, `barcode_id`) ".
                "VALUES ('".$good_id."', '$$', '0', '0', '0')";
                
                $db->query($sql);

                $attr_id = $db->insert_id();

                if ($good_id && $attr_id){
                    $db->query("UPDATE ".$ecs->table('insurance')." SET `target_goods_id` = '".$good_id."' WHERE `insurance_id` = '".$ins_id."' AND `duration` = '".$duration."'");
                    $is_lang_repeated = $db->getOne("SELECT 1 FROM ".$ecs->table('goods_lang')." WHERE goods_id = '".$good_id."' ");
                    if (!$is_lang_repeated){
                    $db->query("INSERT INTO ".$ecs->table('goods_lang')." ".
                                "(`goods_id`, `lang`, `goods_name`, `keywords`) ".
                                "VALUES ".
                                "('".$good_id."', 'zh_cn', '".$goods_name_zhcn."', ''), ".
                                "('".$good_id."', 'en_us', '".$goods_name_en."', '')"
                            );
                    }

                $erpController = new ErpController();

                $warehouse_id = $this->db->getAll("select warehouse_id from ".$ecs->table('erp_warehouse')." where is_valid = 1");

                $sellableWarehouseIds = $erpController->getSellableWarehouseIds(FALSE); 

                foreach ($warehouse_id as $wh_id){
                //foreach ($sellableWarehouseIds as $wh_id){
                    $q = 0;
                    if (in_array($wh_id['warehouse_id'], $sellableWarehouseIds)){
                        $q = 9999;
                    }

                    $max_wa_id   = $db->getOne("SELECT MAX(warehousing_sn) FROM ".$ecs->table('erp_warehousing'));
                    $max_wa_id_num = substr($max_wa_id, 2);
                    $new_wa_id = 0;
    
                    if (is_numeric($max_wa_id_num)){
                        $new_wa_id = $max_wa_id_num + 1;
                    }
    
                    if (is_numeric($new_wa_id)){
                        $new_wa_id = "WA".$new_wa_id;
                    }
    
                    $is_wa_id_repeated = $db->getOne("SELECT 1 FROM ".$ecs->table('erp_warehousing')." WHERE warehousing_sn = '".$new_wa_id."' ");
    
                    //$main_warehouse_id = $db->getOne("SELECT warehouse_id FROM ".$ecs->table('erp_warehouse')." WHERE is_main = '1' ");
    
                    if (!empty($wh_id['warehouse_id']) && !$is_wa_id_repeated){
                        $time = gmtime();
                        $sql = "INSERT INTO ".$ecs->table('erp_warehousing')." ".
                                "(`warehousing_sn`, `warehousing_style_id`, `warehouse_id`, `warehousing_from`, `order_id`, `create_time`, `create_by`, `post_by`, `post_time`, `post_remark`, `approved_by`, `approve_time`, `approve_remark`, `locked_by`, `locked_time`, `last_act_time`, `is_locked`, `status`, `agency_id`, `operator`) ".
                                "VALUES ('".$new_wa_id."', '4', '".$wh_id['warehouse_id']."', 'system', '0', '".$time."', '1', '1', '".$time."', 'create insurance product', '1', '".$time."', '新增保險產品', '1', '".$time."', '".$time."', '1', '3', '0', 'admin')";
                        $db->query($sql);
    
                        $whing_id = $db->insert_id();
    
                        if (!empty($whing_id)){
                            $sql = "INSERT INTO ".$ecs->table('erp_warehousing_item')." ".
                                    "(`warehousing_id`, `goods_id`, `attr_id`, `expected_qty`, `received_qty`, `price`, `account_payable`, `paid_amount`, `order_item_id`, `remark`, `sys_delivery_item_id`) ".
                                    "VALUES ('".$whing_id."', '".$good_id."', '".$attr_id."', '0', '".$q."', '0.00', '0.00', '0.00', '0', 'insurance product', '0')";
                            $db->query($sql);
    
                            $today  = local_date('Y-m-d');
                            //$main_warehouse_id = $db->getOne("SELECT warehouse_id FROM ".$ecs->table('erp_warehouse')." WHERE is_main = '1' ");
                            $sql = "INSERT INTO ".$ecs->table('erp_stock')." ".
                                    "(`goods_id`, `goods_attr_id`, `qty`, `price`, `amount`, `act_time`, `warehouse_id`, `w_d_name`, `w_d_style_id`, `order_sn`, `w_d_sn`, `stock_style`, `stock`, `act_date`, `rma_id`) ".
                                    "VALUES ('".$good_id."', '".$attr_id."', '".$q."', '0.00', '0.00', '".$time."', '".$wh_id['warehouse_id']."', 'system', '4', '', '".$new_wa_id."', 'w', '".$q."', '".$today."', '')";
                            $db->query($sql);
    
                            $sql = "INSERT INTO ".$ecs->table('erp_goods_attr_stock')." ".
                                    "(`attr_id`, `goods_id`, `warehouse_id`, `goods_qty`, `begin_qty`, `delivery_qty`, `warehousing_qty`, `end_qty`) ".
                                    "VALUES ('".$attr_id."', '".$good_id."', '".$wh_id['warehouse_id']."', '".$q."', '0', '0', '0', '0')";
                            $db->query($sql);
                        }
                    }
                }
                        

                if ($return_id){
                    return $ins_id;
                } else {
                    return TRUE;
                }
                } else {
                    return FALSE;
                }
            } else {
                return FALSE;
            }
        }
    }

    public function getInsuranceInfo ($id){
        global $ecs, $db;
        if (!$id){
            return FALSE;
        } else {
            $sql = "SELECT insurance_id, GROUP_CONCAT(duration) AS duration, name, code, status FROM ".$ecs->table('insurance')." ".
                    "WHERE insurance_id = '".$id."' ".
                    "GROUP BY insurance_id ";

            $res = $db->getRow($sql);
            
            if (!empty($res)){
                $sql = "SELECT name FROM ".$ecs->table('insurance_lang')." ".
                "WHERE insurance_id = '".$id."' ".
                "AND lang = 'en_us' ";
                $res_name_en = $db->getOne($sql);

                $res['name_en'] = !empty($res_name_en)?$res_name_en:'';

                return $res;
            } else {
                return TRUE;
            }
        }
    }

    public function editInsuranceInfo ($id = 0, $arr_ins_info = array(), $arr_name_lang = array()){
        global $ecs, $db;
        if (!$arr_ins_info['name'] || !$arr_ins_info['code'] || !$id){
            return FALSE;
        } else {
            if (isset($arr_ins_info['name'])){
                $arr_ins_info['name'] = mysql_real_escape_string($arr_ins_info['name']);
            }
            /*
            if (isset($arr_ins_info['duration_options'])){
                $arr_ins_info['duration_options'] = implode('^', $arr_ins_info['duration_options']);
            }
            */
            if (sizeof($arr_ins_info) > 0){
                $arr_set = [];
                foreach ($arr_ins_info as $key => $val){
                    array_push($arr_set, $key." = '".$val."'");
                }
                $str_set = implode(', ', $arr_set);
                $sql = "UPDATE ".$ecs->table('insurance')." ".
                        "SET ".$str_set." ".
                        "WHERE insurance_id = '".$id."'";

                $res = $db->query($sql);

                if (!empty($arr_name_lang)){
                    $name_en = mysql_real_escape_string(isset($arr_name_lang['en'])?$arr_name_lang['en']:'');
                    $name_cn = mysql_real_escape_string(isset($arr_name_lang['cn'])?$arr_name_lang['cn']:'');
                    $sql = "UPDATE ".$ecs->table('insurance_lang')." ".
                    "SET name = '".$name_en."' ".
                    "WHERE insurance_id = '".$id."'".
                    "AND lang = 'en_us'";

                    $res = $db->query($sql);

                    $sql = "UPDATE ".$ecs->table('insurance_lang')." ".
                    "SET name = '".$name_cn."' ".
                    "WHERE insurance_id = '".$id."'".
                    "AND lang = 'zh_cn'";

                    $res = $db->query($sql);
                }
            }
        }
    }

    public function getGoodsBykeyword($sql_where, $operator = ' OR '){
        global $ecs, $db;

        if (empty($sql_where)){
            return FALSE;
        } else {
            $str_where = implode($operator, $sql_where);

            $sql = "SELECT goods_id, goods_name ".
            "FROM ".$ecs->table('goods')." ".
            "WHERE ($str_where) ".
            "AND is_real = 1 ".
            "AND is_on_sale = 1 ".
            "ORDER BY goods_name ASC ";

            $res = $db->getAll($sql);

            if (empty($res)){
                return 0;
            } else {
                return $res;
            }
        }
    }

    public function getExistedInsGoods($id, $duration, $items){
        if (empty($id) || empty($duration) || empty($items) || !is_array($items)){
            return FALSE;
        } else {
            global $ecs, $db;

            $str_items = implode(', ', $items);

            $sql = "SELECT goods_id ".
            "FROM ".$ecs->table('insurance_goods')." ".
            "WHERE goods_id IN (".$str_items.") ".
            "AND insurance_id = '".$id."' ".
            "AND duration = '".$duration."' ".
            "ORDER BY goods_id ASC ";

            $res = $db->getAll($sql);

            if (empty($res)){
                return FALSE;
            } else {
                return $res;
            }
        }
    }

    public function getGoodsPrice($items){
        if (empty($items) || !is_array($items)){
            return FALSE;
        } else {
            global $ecs, $db;

            $str_items = implode(', ', $items);

            $sql = "SELECT goods_id, shop_price ".
            "FROM ".$ecs->table('goods')." ".
            "WHERE goods_id IN (".$str_items.") ".
            "ORDER BY goods_id ASC ";

            $res = $db->getAll($sql);

            if (empty($res)){
                return FALSE;
            } else {
                return $res;
            }
        }
    }

    public function addInsGoods($id, $price_percent, $duration_items){
        if (empty($id) || empty($duration_items) || !is_array($duration_items)){
            return FALSE;
        } else {
            global $ecs, $db;
            //dd($duration_items);
            //exit;
            //$float_price_percent = bcdiv($price_percent, '100', 2);
            foreach ($duration_items as $duration => $items){
                $arr_item = array();
                $duration_percent = $price_percent[$duration];
                if (!$duration_percent){
                    continue;
                }
                foreach ($items as $item){
                    array_push($arr_item, "('".$id."', '".$item."', '".$duration."', '".$duration_percent."')");
                }
                //dd($arr_item);
                $str_item = implode(', ', $arr_item);
                $sql = "INSERT INTO ".$ecs->table('insurance_goods')." ".
                        "(insurance_id, goods_id, duration, price_percent) ".
                        "VALUES ".$str_item." ";
    
                $res = $db->query($sql);
            }
        }
    }

    public function deleteInsGoods($ins_id, $good_id){
        if (empty($ins_id) || empty($good_id)){
            return FALSE;
        } else {
            global $ecs, $db;
            $sql = "DELETE FROM ".$ecs->table('insurance_goods')." ".
                    "WHERE insurance_id = '".$ins_id."' ".
                    "AND goods_id = '".$good_id."' ";

            $del_res = $db->query($sql);

            $sql = "SELECT insurance_goods_id ".
                    "FROM ".$ecs->table('insurance_goods')." ".
                    "WHERE insurance_id = '".$ins_id."' ".
                    "AND goods_id = '".$good_id."' ";

            $get_res = $db->getRow($sql);
            if (empty($get_res)){
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    public function deleteInsAllGoods($ins_id){
        if (empty($ins_id)){
            return FALSE;
        } else {
            global $ecs, $db;
            $sql = "DELETE FROM ".$ecs->table('insurance_goods')." ".
                    "WHERE insurance_id = '".$ins_id."' ";

            $del_res = $db->query($sql);

            $sql = "SELECT COUNT(insurance_goods_id) ".
                    "FROM ".$ecs->table('insurance_goods')." ".
                    "WHERE insurance_id = '".$ins_id."' ".
                    "GROUP BY insurance_goods_id ";

            $get_res = $db->getRow($sql);

            if ($get_res == 0){
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    public function countInsGood($id, $where){
        if (empty($id)){
            return FALSE;
        } else {
            global $ecs, $db;
            //$sql = "SELECT COUNT(ig.insurance_goods_id) AS " .
            $sql = "SELECT COUNT(DISTINCT ig.goods_id)" .
                    "FROM " . $ecs->table('insurance_goods')." AS ig ".
                    "LEFT JOIN " . $ecs->table('goods')." AS g ON g.goods_id = ig.goods_id ".
                    "WHERE insurance_id = '".$id."' ";
            if (!empty($where)){
                $sql .= "AND ".implode(' AND ', $where)." ";
            }
            $result = $db->getOne($sql);
            return $result;
        }
    }

    public function getInsGoodList($id, $order = "insurance_goods_id DESC", $where, $page_size = 50, $offset = 0){
        if (empty($id)){
            return FALSE;
        } else {
            global $ecs, $db;
            if(!empty($order)){
                $order = " ORDER BY " . $order;
            }
            $limit = " LIMIT " . $page_size;
            $offset = " OFFSET " . $offset;
            $sql = "SELECT ig.insurance_goods_id, ig.goods_id, ig.duration, ig.price_percent, TRIM(ROUND(g.cost, 2)) + 0 AS cost, g.goods_name, g.shop_price ".
                    "FROM ".$ecs->table('insurance_goods')." AS ig ".
                    "LEFT JOIN ".$ecs->table('goods')." AS g ON g.goods_id = ig.goods_id ".
                    "WHERE ig.insurance_id = '".$id."' ";
            if (!empty($where)){
                $sql .= "AND ".implode(' AND ', $where)." ";
            }
            if (!empty($order)){$sql .= $order . " ";}
            $sql .= $limit . " " . $offset;
            $res = $db->getAll($sql);
            return $res;
        }
    }

    public function countInsGoodClaim($id, $where){
        if (empty($id)){
            return FALSE;
        } else {
            global $ecs, $db;
            //$sql = "SELECT COUNT(ig.insurance_goods_id) AS " .
            $sql = "SELECT COUNT(DISTINCT ic.claim_id) ".
                    " FROM " . $ecs->table('insurance_claim') . " AS ic " .
                    " LEFT JOIN " . $ecs->table('order_goods') . " AS og ON og.rec_id = ic.order_goods_id " .
                    " LEFT JOIN " . $ecs->table('insurance') . " AS i ON i.target_goods_id = og.goods_id " .
                    " LEFT JOIN ".$ecs->table('goods')." AS g ON g.goods_id = i.target_goods_id ".
                    " LEFT JOIN ".$ecs->table('order_info')." AS oi ON oi.order_id = og.order_id ".
                    " WHERE i.insurance_id = '" . $id . "' ";

                    
            if (!empty($where)){
                $sql .= "AND ".implode(' AND ', $where)." ";
            }
            $result = $db->getOne($sql);
            return $result;
        }
    }

    public function getInsGoodClaimList($id, $order = "og.claim_id DESC", $where, $page_size = 50, $offset = 0){
        if (empty($id)){
            return FALSE;
        } else {
            global $ecs, $db;
            if(!empty($order)){
                $order = " ORDER BY " . $order;
            }
            $limit = " LIMIT " . $page_size;
            $offset = " OFFSET " . $offset;

            $sql = "SELECT ic.claim_id, ic.claim_amount, ic.claim_datetime, og.goods_price, og.order_id, oi.order_sn, i.duration, g.goods_name " .
                    " FROM " . $ecs->table('insurance_claim') . " AS ic " .
                    " LEFT JOIN " . $ecs->table('order_goods') . " AS og ON og.rec_id = ic.order_goods_id " .
                    " LEFT JOIN " . $ecs->table('insurance') . " AS i ON i.target_goods_id = og.goods_id " .
                    " LEFT JOIN ".$ecs->table('goods')." AS g ON g.goods_id = i.target_goods_id ".
                    " LEFT JOIN ".$ecs->table('order_info')." AS oi ON oi.order_id = og.order_id ".
                    " WHERE i.insurance_id = '" . $id . "' ";

            if (!empty($where)){
                $sql .= "AND ".implode(' AND ', $where)." ";
            }
            if (!empty($order)){$sql .= $order . " ";}
            $sql .= $limit . " " . $offset;
            $res = $db->getAll($sql);
            return $res;
        }
    }

    public function countInsGoodBought($id, $where){
        if (empty($id)){
            return FALSE;
        } else {
            global $ecs, $db;
            //$sql = "SELECT COUNT(ig.insurance_goods_id) AS " .
            $sql = "SELECT COUNT(DISTINCT og.rec_id) ".
                    "FROM ".$ecs->table('order_goods')." AS og ".
                    "LEFT JOIN ".$ecs->table('insurance')." AS i ON i.target_goods_id = og.goods_id ".
                    "LEFT JOIN ".$ecs->table('goods')." AS g ON g.goods_id = og.goods_id ".
                    "LEFT JOIN ".$ecs->table('order_info')." AS oi ON oi.order_id = og.order_id ".
                    "WHERE i.insurance_id = '".$id."' ".
                    "AND og.is_insurance = 1 ".
                    "AND oi.pay_status = 2 ";
            if (!empty($where)){
                $sql .= "AND ".implode(' AND ', $where)." ";
            }
            $result = $db->getOne($sql);
            return $result;
        }
    }

    public function getInsGoodBoughtList($id, $order = "og.rec_id DESC", $where, $page_size = 50, $offset = 0){
        if (empty($id)){
            return FALSE;
        } else {
            global $ecs, $db;
            if(!empty($order)){
                $order = " ORDER BY " . $order;
            }
            $limit = " LIMIT " . $page_size;
            $offset = " OFFSET " . $offset;
            $sql = "SELECT og.rec_id, og.order_id, g.goods_name, i.duration, og.goods_price, oi.order_sn, oi.add_time, og.goods_number ".
                    "FROM ".$ecs->table('order_goods')." AS og ".
                    "LEFT JOIN ".$ecs->table('insurance')." AS i ON i.target_goods_id = og.goods_id ".
                    "LEFT JOIN ".$ecs->table('goods')." AS g ON g.goods_id = og.goods_id ".
                    "LEFT JOIN ".$ecs->table('order_info')." AS oi ON oi.order_id = og.order_id ".
                    "WHERE i.insurance_id = '".$id."' ".
                    "AND og.is_insurance = 1 ".
                    "AND oi.pay_status = 2 ";

            if (!empty($where)){
                $sql .= "AND ".implode(' AND ', $where)." ";
            }
            if (!empty($order)){$sql .= $order . " ";}
            $sql .= $limit . " " . $offset;
            $res = $db->getAll($sql);
            return $res;
        }
    }

    public function getClaimRecord($order_id = 0, $order_goods_id = 0){
        if (empty($order_id) || empty($order_goods_id)){
            return FALSE;
        } else {
            global $ecs, $db;
            $sql = "SELECT * ".
                    "FROM ".$ecs->table('insurance_claim')." ".
                    "WHERE order_id = '".$order_id."' ".
                    "AND order_goods_id = '".$order_goods_id."' ";
            $res = $db->getAll($sql);
            return $res;
        }
    }

    public function getInsByGoodID($id){
        if ($id){
            global $ecs, $db;
            $sql = "SELECT ig.insurance_goods_id, ig.insurance_id, ig.goods_id, ig.duration, ig.price_percent, i.target_goods_id, i.code, i.name ".
            "FROM ".$ecs->table('insurance_goods')." AS ig ".
            "LEFT JOIN ".$ecs->table('insurance')." AS i ON (i.insurance_id = ig.insurance_id AND i.duration = ig.duration) ".
            "WHERE ig.goods_id = '".$id."' ";

            $res = $db->getAll($sql);

            if (!empty($res)){
                return $res;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    public function getInsGoodIdByInsGoodId($ins_good_id, $duration){
        if ($ins_good_id){
            global $ecs, $db;
            $sql = "SELECT insurance_goods_id"." ".
            "FROM ".$ecs->table('insurance_goods')." ".
            "WHERE duration = '".$duration."' ".
            "AND insurance_id = '".$db->getOne("SELECT insurance_id FROM ".$ecs->table('insurance_goods')." WHERE insurance_goods_id = '".$ins_good_id."' ")."' ".
            "AND goods_id = '".$db->getOne("SELECT goods_id FROM ".$ecs->table('insurance_goods')." WHERE insurance_goods_id = '".$ins_good_id."' ")."' ";

            $res = $db->getRow($sql);

            if (!empty($res)){
                return $res['insurance_goods_id'];
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    public function addNewInsGood($ins_good_id, $duration, $parcent){
        if ($ins_good_id && $duration){
            global $ecs, $db;
            $sql = "SELECT insurance_id, goods_id ".
            "FROM ".$ecs->table('insurance_goods')." ".
            "WHERE insurance_goods_id = '".$ins_good_id."' ";

            $res = $db->getRow($sql);

            if (!empty($res)){
                $sql = "INSERT INTO ".$ecs->table('insurance_goods')." ".
                "(insurance_id, goods_id, duration, price_percent) ".
                "VALUES ('".$res['insurance_id']."', '".$res['goods_id']."', '".$duration."', '".$parcent."')";
                $db->query($sql);
                $new_ins_good_id = $db->insert_id();
                if ($new_ins_good_id){
                    return$new_ins_good_id;
                } else {
                    return FALSE;
                }
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    public function addClaimRecord($order_goods_id = 0, $order_id = 0, $claim_amount = 0, $receipt_img = '', $claim_datetime = 0,  $approve_by =''){
        if ($order_goods_id && $order_id && $claim_amount && $receipt_img){
            global $ecs, $db;
            $sql = "INSERT INTO ".$ecs->table('insurance_claim')." ".
                    "(order_id, order_goods_id, claim_amount, receipt_img, claim_datetime, approve_by) ".
                    "VALUES ('".$order_id."', '".$order_goods_id."', '".$claim_amount."', '".$receipt_img."', '".$claim_datetime."', '".$approve_by."')";

            $db->query($sql);

            $claim_id = $db->insert_id();

            if (!empty($claim_id)){
                $sql = "SELECT order_id, order_goods_id FROM ".$ecs->table('insurance_claim')." WHERE claim_id = '".$claim_id."' ";
                $res = $db->getRow($sql);

                if(!empty($res)){
                    if ($res['order_id'] == $order_id && $res['order_goods_id'] == $order_goods_id){
                        return TRUE;
                    }
                } else {
                    return FALSE;
                }
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

}
