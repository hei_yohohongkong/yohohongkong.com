<?php

/***
* AdPosController.php
* by Yan 20191010
*
* ad pos controller
***/

namespace Yoho\cms\Controller;
use Yoho\cms\Model;
if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

class AdPosController extends YohoBaseController{

    private $tablename = 'ad_pos';
    
	public function __construct(){
        parent::__construct();
    }
    
    function get_ad_pos_list() {
        global $db, $ecs, $_CFG, $_LANG;

        $ad_pos_model = new Model\AdPos();
        $_REQUEST['record_count'] = $ad_pos_model->count();

        $_REQUEST['sort_by']    = empty($_REQUEST['sort_by']) ? 'ad_name' : trim($_REQUEST['sort_by']);
        $_REQUEST['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);


        /* Paging */
        $filter = page_and_size($_REQUEST);

        /* Query */
        $arr = array();
        $res = $ad_pos_model->get_list_by_filter($filter);
        foreach($res as $rows) {
            /* name of media type */
            $rows['type']  = ($rows['media_type'] == 0) ? $_LANG['ad_img']   : '';
            $rows['type'] .= ($rows['media_type'] == 4) ? $_LANG['ad_video']  : '';

            /* format datatime */
            $rows['start_date']    = local_date($_CFG['date_format'], $rows['start_time']);
            $rows['end_date']      = local_date($_CFG['date_format'], $rows['end_time']);

            /* media url */
            $url = $ecs->url();
            if (strpos($rows['ad_file'], 'http://') === false && strpos($rows['ad_file'], 'https://') === false)
            {
                $rows['preview_url'] = $url . DATA_DIR . '/afficheimg/' . $rows['ad_file'];
            }
            else
            {
                $rows['preview_url'] = $rows['ad_file'];
            }

            $arr[] = $rows;
        }

        return array('data' => $arr, 'record_count' => $_REQUEST['record_count']);
    }

    function get_ad_pos($id)
    {
        global $_CFG, $_LANG;

        $get_ad_pos = new Model\AdPos($id);
        $ad_pos     = $get_ad_pos->data;
        if($ad_pos) {
            if(!$ad_pos) return false;
            /* Format data */
            $ad_pos['ad_pos_id'] = $id;
            $ad_pos['end_time']   = local_date($_CFG['date_format'], $ad_pos['end_time']);
            $ad_pos['start_time'] = local_date($_CFG['date_format'], $ad_pos['start_time']);

            if ($ad_pos['media_type'] == '0')
            {
                if (strpos($ad_pos['ad_file'], 'http://') === false && strpos($ad_pos['ad_file'], 'https://') === false)
                {
                    $ad_pos['img_src'] = '../' . DATA_DIR . '/afficheimg/'. $ad_pos['ad_file'];
                }
                else
                {
                    $ad_pos['img_url_src'] = $ad_pos['ad_file'];
                }
            }

            if ($ad_pos['media_type'] == '4')
            {
                if (strpos($ad_pos['ad_file'], 'http://') === false && strpos($ad_pos['ad_file'], 'https://') === false)
                {
                    $ad_pos['video_url'] = '../' . DATA_DIR . '/afficheimg/'. $ad_pos['ad_file'];
                }
                else
                {
                    $ad_pos['video_url_src'] = $ad_pos['ad_file'];
                }
            }

            return ['status' => true, 'data' => $ad_pos];
        } else {
            return ['status' => false, 'error_msg' => $_LANG['ad_pos_not_available']];
        }
    }

    function update($data) {
        /* Create new ad POS */
        if(empty($data['ad_pos_id']))
        {
            $new_ad_pos = new Model\AdPos(null, $data);
            if($new_ad_pos->getId())return true;
            else return false;
        }
        /* Update ad POS */
        else
        {
            $ad_pos = new Model\AdPos($data['ad_pos_id']);
            if($ad_pos && $ad_pos->update($data))return true;
            else return false;
        }
    }

    function parse_ad_pos_request($request) {

        global $_CFG, $_LANG;

        $data = array();
        $data['ad_pos_id']   = !empty($request['id'])      ? intval($request['id'])    : 0;
        $data['ad_name']     = !empty($request['ad_name']) ? trim($request['ad_name']) : '';
        $data['start_time']  = local_strtotime($request['start_time']);
        $data['end_time']    = local_strtotime($request['end_time']);
        $data['enabled']    = $request['enabled'];
        $data['media_type'] = $request['media_type'];

        $ad_file = '';
        if($data['media_type'] == 0) 
        {
            if ((isset($_FILES['ad_img']['error']) && $_FILES['ad_img']['error'] == 0) || (!isset($_FILES['ad_img']['error']) && isset($_FILES['ad_img']['tmp_name'] ) && $_FILES['ad_img']['tmp_name'] != 'none'))
            {
                include_once(ROOT_PATH . 'includes/cls_image.php');
                $image = new \cls_image($_CFG['bgcolor']);
                $upload_result = $image->upload_image($_FILES['ad_img'], 'afficheimg');
                if($upload_result){
                    $ad_file = basename($upload_result);
                } else {
                    return ['status' => false, 'error_msg' => $image->error_msg];
                }
            }

            if (!empty($request['img_url']))
            {
                $img_url = trim($request['img_url']);
                $url_extension = end(explode(".", $img_url));
                $allowed_exts = array("png", "jpg", "jpeg", "bmp");
                if(in_array($url_extension, $allowed_exts)) 
                {
                    $ad_file = $img_url;
                } 
                else 
                {
                    return ['status' => false, 'error_msg' => $_LANG['image_url_error']];
                }
            }
        } 
        else if($data['media_type'] == 4) 
        {
            // $allowed_exts = array("mp4", "mov", "avi", "flv");
            $allowed_exts = array("mp4");
            if ((isset($_FILES['ad_video']['error']) && $_FILES['ad_video']['error'] == 0) || (!isset($_FILES['ad_video']['error']) && isset($_FILES['ad_video']['tmp_name'] ) && $_FILES['ad_video']['tmp_name'] != 'none'))
            {   
                $video_file = $_FILES['ad_video'];
                $extension = end(explode(".", $video_file["name"]));

                // check file type
                if ((($video_file["type"] == "video/mp4")
                    // || ($video_file["type"] == "video/avi")
                    // || ($video_file["type"] == "video/flv")
                    // || ($video_file["type"] == "video/quicktime")
                    // || ($video_file["type"] == "video/mov")
                    // || ($video_file["type"] == "application/octet-stream")
                    && in_array($extension, $allowed_exts))) {
                        // generate file name
                        $urlstr = date('Ymd');
                        for ($i = 0; $i < 6; $i++)
                        {
                            $urlstr .= chr(mt_rand(97, 122));
                        }

                        $source_file = $video_file['tmp_name'];
                        $target      = ROOT_PATH . DATA_DIR . '/afficheimg/';
                        $file_name   = $urlstr .'.'. $extension;

                        // upload file
                        if (move_upload_file($source_file, $target.$file_name))
                        {
                            $ad_file = $file_name;
                        }
                        else
                        {
                            return ['status' => false, 'error_msg' => $_LANG['upfile_error']];
                        }
                } 
                else
                {
                    return ['status' => false, 'error_msg' => $_LANG['upfile_video_type']];
                }
            }

            if (!empty($request['video_url']))
            {
                $video_url = trim($request['video_url']);
                $x = parse_url($video_url);
                $url_extension = end(explode(".", $x['path']));
                if(in_array($url_extension, $allowed_exts)) 
                {
                    $ad_file = $video_url;
                } 
                else 
                {
                    return ['status' => false, 'error_msg' => $_LANG['video_url_error']];
                }
            }
        }

        $data['ad_file'] = $ad_file;
        return ['status'=>true, 'data' => $data];
    }

}


?>