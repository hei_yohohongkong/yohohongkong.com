<?php
/***
* by Eric 20190402
*
* Order Ship controller
***/

namespace Yoho\cms\Controller;
use Yoho\cms\Model;
//use function GuzzleHttp\json_decode;

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}


class OrderShipController extends YohoBaseController
{
    function getlogisticsClosestCutOffTime($logistics_type,$shipping_id) {
        $logistics['express_hk'][3] = array (  //3 速遞
            //local_strtotime(date("Y-m-d 13:00:00")), // 宅急便
            local_strtotime(date("Y-m-d 09:00:00")), // sf-express
            local_strtotime(date("Y-m-d 10:30:00")), // sf +
            local_strtotime(date("Y-m-d 14:00:00")), // sf-express
            // local_strtotime(date("Y-m-d 10:30:00", strtotime( date( 'Y-m-d 10:30:00' )." +1 days"))),
        );
        $logistics['express_hk'][9] = array (  //3 便利店
            local_strtotime(date("Y-m-d 09:00:00")), // sf-express
            local_strtotime(date("Y-m-d 14:00:00")), // sf-express
        );
        $logistics['express_hk'][10] = array (  //10 順豐服務中心
            local_strtotime(date("Y-m-d 09:00:00")), // sf-express
            local_strtotime(date("Y-m-d 14:00:00")), // sf-express
        );
        $logistics['express_hk'][16] = array (  //14 其他自提點 (Shipbao)
            local_strtotime(date("Y-m-d 15:30:00")), // 中通
        );
        $logistics['express_hk'][15] = array (  //15 郵政局
            local_strtotime(date("Y-m-d 15:30:00")), // 中通
        );

        $logistics['locker'][8] = array (  // 8 順便智能櫃
            local_strtotime(date("Y-m-d 09:00:00")), // sf-express
            local_strtotime(date("Y-m-d 14:00:00")), // sf-express
        );
        $logistics['locker'][11] = array (  //11 派寶箱 
            local_strtotime(date("Y-m-d 15:30:00")), // 派寶箱
        );
        $logistics['locker'][12] = array (  //11 Lockerlife 
            local_strtotime(date("Y-m-d 15:30:00")), // Lockerlife
        );
        $logistics['locker'][13] = array (  //11 中通智能櫃(showEc)
            local_strtotime(date("Y-m-d 15:30:00")), //中通
        );

        $logistics['express_oversea'][0] = array (  // (海外)
            local_strtotime(date("Y-m-d 15:00:00")), // Fedex / EMS (海外)
        );

        $logistics['express_cn'][3] = array (  // 中國
            local_strtotime(date("Y-m-d 15:00:00")),
        );

        $logistics['fedex'][6] = array (  // fedex
            local_strtotime(date("Y-m-d 15:00:00")), // fedex
        );
        $logistics['fedex'][7] = array (  // fedex
            local_strtotime(date("Y-m-d 15:00:00")), // register
        );

        $logistics['fls'][0] = array (  // FLS
            local_strtotime(date("Y-m-d 10:30:00")), // sf +
        );

        $logistics['wilson'][0] = array (  // FLS
            local_strtotime(date("Y-m-d 09:00:00")), // wilson
        );

        if (isset($logistics[$logistics_type][$shipping_id])) {
            $cut_off_time = $logistics[$logistics_type][$shipping_id][0];

            foreach ($logistics[$logistics_type][$shipping_id] as $time) {
                if ($time < gmtime()) {
                    $cut_off_time = $time; 
                }
            }

            return $cut_off_time;
        } else {
           return gmtime(); 
        }
    }

    function getOrderShipList($selected_logistic_type = null,$selected_shipping_id = null) {
        global $userController,$erpController,$_LANG;
        $order_detail = $this->getOrderShipDetail($selected_logistic_type,$selected_shipping_id);

        if ($_REQUEST['search_form'] == 1) {
            $selected_logistic_type = 'search_form';
            $selected_shipping_id = 0;
        }
        $row = $order_detail[$selected_logistic_type][$selected_shipping_id]['order_details']; 
        $order_ids = $order_detail[$selected_logistic_type][$selected_shipping_id]['orders'];
        
        $record_count = sizeof($row);
        $row = array_slice( $row, $_REQUEST['start'] , $_REQUEST['page_size'] ); 
       
        // get order goods ids
        $sql = "SELECT distinct og.goods_id " .
            "FROM " . $GLOBALS['ecs']->table('order_goods') . " as og " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON og.goods_id = g.goods_id " .
            "WHERE order_id " . db_create_in($order_ids);
        $order_goods_ids = $GLOBALS['db']->getCol($sql);

        $order_goods_qty = $erpController->getSumOfSellableProductsQtyByWarehouses($order_goods_ids,null,false,null);

        $sql = "SELECT og.*, g.goods_thumb, g.goods_height, g.goods_width, g.goods_length " .
        "FROM " . $GLOBALS['ecs']->table('order_goods') . " as og " .
        "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON og.goods_id = g.goods_id " .
        "WHERE order_id " . db_create_in($order_ids);

        $order_goods = $GLOBALS['db']->getAll($sql);

        foreach ($row AS $key => $value)
        {
            if ($row[$key]['tag_ids'] != '') {
                $row[$key]['star_status'] = '<i class="fas fa-star enable" data-order-id="'.$value['order_id'].'" data-tag-id = "1"  data-tag-status = "0" ></i>';
            } else {
                $row[$key]['star_status'] = '<i class="far fa-star disable" data-order-id="'.$value['order_id'].'" data-tag-id = "1" data-tag-status = "1" ></i>';
            }
         
            $row[$key]['formated_order_amount'] = price_format($value['order_amount']);
            $row[$key]['formated_money_paid'] = price_format($value['money_paid']);
            $row[$key]['formated_total_fee'] = price_format($value['total_fee']);
            $row[$key]['formated_order_total'] = price_format($value['order_total']);
            $row[$key]['short_order_time'] = local_date('m-d H:i', $value['add_time']);
            $row[$key]['formated_order_cp_price'] = price_format($value['cp_price']);
            if ($value['order_status'] == OS_INVALID || $value['order_status'] == OS_CANCELED)
            {
                /* 如果该订单为无效或取消则显示删除链接 */
                $row[$key]['can_remove'] = 1;
            }
            else
            {
                $row[$key]['can_remove'] = 0;
            }
            $row[$key]['platform_display'] = isset($platforms[$value['platform']]) ? $platforms[$value['platform']] : $value['platform'];
            
            // Extract order_goods for this order, and find most expensive goods
            $row[$key]['goods_list'] = array();
            $most_expensive = -1;
            foreach ($order_goods as $og)
            {
                $og['storage'] =  $order_goods_qty[$og['goods_id']];
                if ($og['order_id'] == $value['order_id'])
                {
                    $og['goods_thumb'] = get_image_path($og['goods_id'], $og['goods_thumb'], true);
                    $og['goods_thumb'] = ((strpos($og['goods_thumb'], 'http://') === 0) || (strpos($og['goods_thumb'], 'https://') === 0)) ? $og['goods_thumb'] : $GLOBALS['ecs']->url() . $og['goods_thumb'];
                    $og['formated_subtotal'] = price_format($og['goods_price'] * $og['goods_number']);
                    $og['formated_goods_price'] = price_format($og['goods_price']);
                    $og['formated_cost'] = price_format($og['cost'], false);
                    
                    if($_REQUEST['display_order_goods'])$row[$key]['goods_list'][] = $og;
                    if ($og['goods_price'] > $most_expensive)
                    {
                        $most_expensive = $og['goods_price'];
                        //$row[$key]['most_expensive_goods'] = $og;
                        $row[$key]['ex_goods_thumb'] = $og['goods_thumb'];
                    }
                }
            }

            // all goods weight
            foreach ($order_goods as $og)
            {
                if ($og['order_id'] == $value['order_id'])
                {
                    if ($og['goods_length'] > 0 && $og['goods_width'] > 0 && $og['goods_height'] > 0) {
                        $value['LWH'] += $og['goods_length'] + $og['goods_width'] + $og['goods_height'];
                    } else {
                        $value['LWH'] = 0;
                        break;
                    }
                }
            }

            $row[$key]['is_wholesale'] = ((in_array($value['user_id'], $userController->getWholesaleUserIds())||(isset($value['order_type']) && $value['order_type'] == OrderController::DB_ORDER_TYPE_WHOLESALE))?TRUE:FALSE);
            $row[$key]['due_amount'] = $value['fully_paid'] ? 0 : $value['order_amount'] - $value['actg_paid_amount'];
            $row[$key]['due_amount_formatted'] = price_format($row[$key]['due_amount']);
            
            // Flag risky transactions
            $row[$key]['risky'] = false;
            if ($value['pay_id'] == '7') // PayPal
            {
                if ((($value['country'] == '3409') && (doubleval($value['order_total']) >= 6000)) || // 香港, $6000 or above
                    (($value['country'] == '3436') && (doubleval($value['order_total']) >= 3000)) || // 中國, $3000 or above
                    (($value['country'] == '3411') && (doubleval($value['order_total']) >= 3000)))   // 台灣, $3000 or above
                {
                    $row[$key]['risky'] = true;
                }
            }
            if ($value['pay_id'] == '3' && $value['pay_status'] == PS_PAYED && in_array($value['shipping_status'], [SS_SHIPPED, SS_RECEIVED])) //POS單: 門市購買, 已付款, 已出貨/收貨確認
            {
                // Find order_action : check order is reship order.
                $oa_sql = "SELECT * FROM ".$GLOBALS['ecs']->table('order_action')." WHERE order_id = ".$value['order_id']." ORDER BY action_id DESC ";
                $log_list = $GLOBALS['db']->getAll($oa_sql);

                // Check order status
                $have_unshipping_log = false;
                foreach($log_list as $log_key => $log) {
                    $status = $log['shipping_status'];
                    if(in_array($status, [SS_SHIPPED, SS_RECEIVED])) {
                        if($have_unshipping_log == true) {
                            $row[$key]['reship_order'] = true; // reship order
                            break;
                        }
                    } else if(in_array($status, [SS_UNSHIPPED])) {
                        $have_unshipping_log = true;
                    }
                }
            }
            // If is sales agent order
            if($value['order_type'] == OrderController::DB_ORDER_TYPE_SALESAGENT)
            {
                //TODO: hardcode HKTV :H5119002
                //$row[$key]['special_sa'] = strpos($value['serial_numbers'], 'H5119002') !== false ? true:false;
                $row[$key]['is_sa'] = true;
                $row[$key]['sa_sn'] = $value['type_sn'];
                $row[$key]['sa_name']  = isset($salesagent[$value['type_id']]) ? $salesagent[$value['type_id']] : '';
            }

            // consignee info
            $consignee_info = '';
            $class_color_address_type = array(1 => "label-blue", 2 => "label-orange", 99 => "label-gray");
            $class_color_lift_type = array(1 => "label-green", 2 => "label-red", 3 => "label-green-red");
            if ($value['consignee'])$consignee_info .= "<i class='fa fa-user'></i> <a href='mailto:$value[email]'>$value[consignee]</a>";
            if ($value['tel'])$consignee_info .= " <i class='fa fa-phone-square'></i>". $value[tel]. ($value['LWH'] > 0 ? '<b style="color:#9CC2CB"> (LWH - '.$value['LWH'].')</b>':'');
            if ($row[$key]['is_wholesale']) {
                $consignee_info .= "<br/><i class='fa fa-truck'></i> <b>$value[shipping_name] (批發訂單$value[type_sn])</b>";
            } else {
                $consignee_info .= "<br/><b><i class='fa fa-truck'></i> $value[shipping_name] : ".(($value['shipping_fod'])?'(運費到付)':'')."</b> $value[address]".
                "<div>".($value['address_cat_id']>0?"<span class='address-label ".$class_color_address_type[$value['address_cat_id']]."'>".$_LANG['address_type_'.$value['address_cat_id']]."</span>":"").
                ($value['address_lift']>0?"<span class='address-label ".$class_color_lift_type[$value['address_lift']]."'>".$_LANG['address_type_lift_label_'.$value['address_lift']]."</span>":"")."</div>";
            }
            if (strlen(trim($value['postscript'])) > 0) $consignee_info .= "<br /><b><i class='fa fa-file-alt'></i> 客戶備註: $value[postscript]</b>";
            if (strlen(trim($value['to_buyer'])) > 0) $consignee_info .= "<br /><b class='blue'><i class='fa fa-thumbtack'></i> 內部備註: $value[to_buyer]</b>";
            $row[$key]['consignee_info'] = $consignee_info;

            // salesperson info
            $salesperson_info = $value['salesperson'];
            $salesperson_info .= "<br>".$row[$key]['platform_display'];
            if ($row[$key]['flashdeal_id'])$salesperson_info .= '(閃購)';
            $lang_list = available_language_names();
            if ($row[$key]['language'])$salesperson_info .= "<br>".($lang_list[$value['language']] ? $lang_list[$value['language']]: $value['language']);
            if ($row[$key]['is_sa'])$salesperson_info .= "<br>".$row[$key]['sa_name'];
            $row[$key]['salesperson_info'] = $salesperson_info;
            global $_LANG;
            $all_status = $_LANG['icon']['ps'][$value['pay_status']]." | ".$_LANG['icon']['ss'][$value['shipping_status']];

            //if ($value['all_in_stock']) {

            //if (in_array($_REQUEST['composite_status'], [CS_AWAIT_SHIP,CS_AWAIT_SHIP_HK,CS_AWAIT_SHIP_LOCKER,CS_AWAIT_PICK_HK,CS_AWAIT_SHIP_NON_HK,CS_AWAIT_SHIP_HK_YOHO,CS_AWAIT_HK_FLS,CS_AWAIT_HK_WILSON])) {
               // $all_status.= " | ".$_LANG['icon']['all_in_stock']['Y'];                
            //}
            //}

            $order_instock_ids = $order_detail[$selected_logistic_type][$selected_shipping_id]['order_instock'];

            if ($_REQUEST['search_form'] == 1) {
                if (in_array($row[$key]['order_id'],$order_instock_ids)) {
                    $all_status.= " | ".$_LANG['icon']['all_in_stock']['Y'];
                } else {
                    $all_status.= " | ".$_LANG['icon']['all_in_stock']['N'];
                }
            } else {
                $all_status.= " | ".$_LANG['icon']['all_in_stock']['Y'];
            }

            //if ($value['all_in_stock']) $all_status.= " | ".$_LANG['icon']['all_in_stock'][$value['all_in_stock']];

            if (!empty($po_sn_list_ar[$row[$key]['order_id']])) {
                foreach ($po_sn_list_ar[$row[$key]['order_id']] as $po_order_sn) {
                    $row[$key]['po_sn_list'] .= '<a target="_blank" href="erp_order_manage.php?act=order_list&order_sn='.$po_order_sn.'">'.$po_order_sn.'</a>';
                    $row[$key]['po_sn_list'] .= '<br>';
                }
                //$row[$key]['po_sn_list'] = $po_sn_list_ar[$row[$key]['order_id']];
            }else {
                $row[$key]['po_sn_list'] = '';
            }
            
            $row[$key]['all_status'] = $all_status;

            // action
            $action = '';
            $action .= '<a class="badge '.($value['ordered_from_supplier'] ? 'bg-green':'bg-blue').'" href="javascript:;" onclick="addRemarkPopup('.$value['order_id'].')">'.$value['action_count'].'</a>';
            //$action .= '<a href="order.php?act=info&order_id='.$value['order_id'].'">'.$_LANG['detail'].'</a>';
                if ($row[$key]['can_remove']) $action .= '<br /><a href="javascript:;" data-url="order.php?act=remove&amp;id='.$value['order_id'].'" onclick="cellRemove(this);">'.$_LANG['remove'].'</a>';
            if ($row[$key]['is_wholesale'] || $_REQUEST['wholesale_receivable'])
            $action .= ($row[$key]['fully_paid'] ? '<br /><a href="wholesale_payments.php?act=list&order_sn='.$value['order_sn'].'">付款紀錄</a>' : '<br /><a href="wholesale_payments.php?act=add&order_id='.$value['order_id'].'">付款</a>').
                ' | <a href="javascript:;" onclick="javascript:completeOrderPayment('.$value['order_id'].')">完成</a>';
            $row[$key]['_action'] = $action;
        }

        $arr = array(
            'data' => $row,
            'record_count' => $record_count
          );
        
        return $arr;

        exit;

    }

    function getOrderShipDetail($selected_logistic_type = null,$selected_shipping_id = null)
    {
        // howang: Increase memory limit
        ini_set('memory_limit', '256M');

        global $userController,$erpController, $_CFG;

        $stocktakeController = new StocktakeController();
        $adminuserController = new AdminuserController();
        $deliveryOrderController = new DeliveryOrderController();

        /* 过滤信息 */
        $_REQUEST['order_sn'] = empty($_REQUEST['order_sn']) ? '' : trim($_REQUEST['order_sn']);
        if (!empty($_GET['is_ajax']) && $_GET['is_ajax'] == 1)
        {
            $_REQUEST['consignee'] = json_str_iconv($_REQUEST['consignee']);
            //$_REQUEST['address'] = json_str_iconv($_REQUEST['address']);
        }
        $_REQUEST['consignee'] = empty($_REQUEST['consignee']) ? '' : trim($_REQUEST['consignee']);
        $_REQUEST['email'] = empty($_REQUEST['email']) ? '' : trim($_REQUEST['email']);
        $_REQUEST['address'] = empty($_REQUEST['address']) ? '' : trim($_REQUEST['address']);
        $_REQUEST['zipcode'] = empty($_REQUEST['zipcode']) ? '' : trim($_REQUEST['zipcode']);
        $_REQUEST['tel'] = empty($_REQUEST['tel']) ? '' : trim($_REQUEST['tel']);
        $_REQUEST['mobile'] = empty($_REQUEST['mobile']) ? 0 : intval($_REQUEST['mobile']);
        $_REQUEST['country'] = empty($_REQUEST['country']) ? 0 : intval($_REQUEST['country']);
        $_REQUEST['province'] = empty($_REQUEST['province']) ? 0 : intval($_REQUEST['province']);
        $_REQUEST['city'] = empty($_REQUEST['city']) ? 0 : intval($_REQUEST['city']);
        $_REQUEST['district'] = empty($_REQUEST['district']) ? 0 : intval($_REQUEST['district']);
        $_REQUEST['shipping_id'] = empty($_REQUEST['shipping_id']) ? 0 : intval($_REQUEST['shipping_id']);
        $_REQUEST['shipping_ids'] = empty($_REQUEST['shipping_ids']) ? 0 : implode(',',array_map('intval',explode(',',$_REQUEST['shipping_ids'])));
        $_REQUEST['pay_id'] = empty($_REQUEST['pay_id']) ? 0 : intval($_REQUEST['pay_id']);
        $_REQUEST['pay_ids'] = empty($_REQUEST['pay_ids']) ? 0 : implode(',',array_map('intval',explode(',',$_REQUEST['pay_ids'])));
        $_REQUEST['order_status'] = isset($_REQUEST['order_status']) ? intval($_REQUEST['order_status']) : -1;
        $_REQUEST['shipping_status'] = isset($_REQUEST['shipping_status']) ? $_REQUEST['shipping_status'] : -1;
        $_REQUEST['pay_status'] = isset($_REQUEST['pay_status']) ? intval($_REQUEST['pay_status']) : -1;
        $_REQUEST['user_id'] = empty($_REQUEST['user_id']) ? 0 : intval($_REQUEST['user_id']);
        $_REQUEST['user_ids'] = empty($_REQUEST['user_ids']) ? '' : trim($_REQUEST['user_ids']);
        $_REQUEST['user_name'] = empty($_REQUEST['user_name']) ? '' : trim($_REQUEST['user_name']);
        $_REQUEST['composite_status'] = isset($_REQUEST['composite_status']) ? intval($_REQUEST['composite_status']) : -1;
        $_REQUEST['group_buy_id'] = isset($_REQUEST['group_buy_id']) ? intval($_REQUEST['group_buy_id']) : 0;
        $_REQUEST['platform'] = empty($_REQUEST['platform']) ? '' : trim($_REQUEST['platform']);

        $_REQUEST['sort_by'] = empty($_REQUEST['sort_by']) ? 'add_time' : trim($_REQUEST['sort_by']);
        $_REQUEST['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);

        $_REQUEST['start_time'] = empty($_REQUEST['start_time']) ? '' : (strpos($_REQUEST['start_time'], '-') > 0 ?  local_strtotime($_REQUEST['start_time']) : $_REQUEST['start_time']);
        $_REQUEST['end_time'] = empty($_REQUEST['end_time']) ? '' : (strpos($_REQUEST['end_time'], '-') > 0 ?  local_strtotime($_REQUEST['end_time']) : $_REQUEST['end_time']);

        $_REQUEST['pay_start_time'] = empty($_REQUEST['pay_start_time']) ? '' : (strpos($_REQUEST['pay_start_time'], '-') > 0 ?  local_strtotime($_REQUEST['pay_start_time']) : $_REQUEST['pay_start_time']);
        $_REQUEST['pay_end_time'] = empty($_REQUEST['pay_end_time']) ? '' : (strpos($_REQUEST['pay_end_time'], '-') > 0 ?  local_strtotime($_REQUEST['pay_end_time']) : $_REQUEST['pay_end_time']);
        $_REQUEST['ship_start_time'] = empty($_REQUEST['ship_start_time']) ? '' : (strpos($_REQUEST['ship_start_time'], '-') > 0 ?  local_strtotime($_REQUEST['ship_start_time']) : $_REQUEST['ship_start_time']);
        $_REQUEST['ship_end_time'] = empty($_REQUEST['ship_end_time']) ? '' : (strpos($_REQUEST['ship_end_time'], '-') > 0 ?  local_strtotime($_REQUEST['ship_end_time']) : $_REQUEST['ship_end_time']);
        $_REQUEST['salesperson'] = empty($_REQUEST['salesperson']) ? '' : trim($_REQUEST['salesperson']);
        $_REQUEST['have_discount'] = empty($_REQUEST['have_discount']) ? 0 : intval($_REQUEST['have_discount']);
        $_REQUEST['includes_goods'] = empty($_REQUEST['includes_goods']) ? '' : trim($_REQUEST['includes_goods']);
        $_REQUEST['wholesale_receivable'] = empty($_REQUEST['wholesale_receivable']) ? 0 : intval($_REQUEST['wholesale_receivable']);
        $_REQUEST['serial_numbers'] = empty($_REQUEST['serial_numbers']) ? '' : trim($_REQUEST['serial_numbers']);
        $_REQUEST['flashdeal_id'] = empty($_REQUEST['flashdeal_id']) ? 0 : intval($_REQUEST['flashdeal_id']);
        $_REQUEST['replacement'] = empty($_REQUEST['replacement']) ? 0 : intval($_REQUEST['replacement']);
        $_REQUEST['have_preorder'] = empty($_REQUEST['have_preorder']) ? 0 : intval($_REQUEST['have_preorder']);
        $_REQUEST['stock_incorrect'] = empty($_REQUEST['stock_incorrect']) ? 0 : intval($_REQUEST['stock_incorrect']);
        $_REQUEST['display_order_goods'] = empty($_REQUEST['display_order_goods']) ? 0 : intval($_REQUEST['display_order_goods']);
        $_REQUEST['all_in_stock'] = (in_array($_REQUEST['composite_status'], [CS_AWAIT_SHIP,CS_AWAIT_SHIP_HK,CS_AWAIT_PICK_HK,CS_AWAIT_SHIP_NON_HK,CS_AWAIT_SHIP_HK_YOHO, CS_AWAIT_SHIP_LOCKER,CS_AWAIT_HK_FLS])) ? (empty($_REQUEST['all_in_stock']) ? 0 : intval($_REQUEST['all_in_stock'])) : 0;
        $_REQUEST['need_purchase'] = (in_array($_REQUEST['composite_status'], [CS_AWAIT_SHIP,CS_AWAIT_SHIP_HK,CS_AWAIT_PICK_HK,CS_AWAIT_SHIP_NON_HK,CS_AWAIT_SHIP_HK_YOHO, CS_AWAIT_SHIP_LOCKER,CS_AWAIT_HK_FLS])) ? (empty($_REQUEST['need_purchase']) ? 0 : intval($_REQUEST['need_purchase'])) : 0;
        $_REQUEST['invoice_no'] = empty($_REQUEST['invoice_no']) ? '' : trim($_REQUEST['invoice_no']);
        $_REQUEST['have_partial_ship_history'] = empty($_REQUEST['have_partial_ship_history']) ? 0 : intval($_REQUEST['have_partial_ship_history']);
        $_REQUEST['is_wholesale'] = empty($_REQUEST['is_wholesale']) ? 0 : intval($_REQUEST['is_wholesale']);
        $_REQUEST['logistics_id'] = empty($_REQUEST['logistics_id']) ? 0 : intval($_REQUEST['logistics_id']);
        $_REQUEST['display_sales_order'] = isset($_REQUEST['display_sales_order']) ?  $_REQUEST['display_sales_order'] : 0;
        $_REQUEST['display_sales_order'] = $_REQUEST['composite_status'] == CS_AWAIT_SHIP_HK_YOHO ? 2 : $_REQUEST['display_sales_order'];
        $_REQUEST['display_normal_order'] = isset($_REQUEST['display_normal_order']) ?  $_REQUEST['display_normal_order'] : 0;
        $_REQUEST['start'] = isset($_REQUEST['start']) ?  $_REQUEST['start'] : 0;
        //$_REQUEST['page_size'] = isset($_REQUEST['page_size']) ?  $_REQUEST['page_size'] : 50;
        $_REQUEST['is_fls'] = isset($_REQUEST['is_fls']) ?  $_REQUEST['is_fls'] : 0;
        $_REQUEST['total_fee'] = empty($_REQUEST['total_fee']) ? -1 : floatval($_REQUEST['total_fee']);
        
        if ($_REQUEST['composite_status'] == CS_AWAIT_HK_FLS) {
            $_REQUEST['all_in_stock'] = 1;
        }

        $_REQUEST['composite_status'] = CS_AWAIT_SHIP;
        $_REQUEST['all_in_stock'] = 1;

        $where = 'WHERE 1 ';
        if (!$_SESSION['manage_cost'])
        {
            // Hide 88888888, 99999999 orders from non-admin users
            //Deprecated: after deployed new order_type this logic does not apply anymore
            //$where .= " AND o.user_id NOT IN (5022,5023)";
        }
        if ($_REQUEST['order_sn'])
        {
            $where .= " AND o.order_sn LIKE '%" . mysql_like_quote($_REQUEST['order_sn']) . "%'";
        }
        if ($_REQUEST['consignee'])
        {
            $where .= " AND o.consignee LIKE '%" . mysql_like_quote($_REQUEST['consignee']) . "%'";
        }
        if ($_REQUEST['email'])
        {
            $where .= " AND o.email LIKE '%" . mysql_like_quote($_REQUEST['email']) . "%'";
        }
        if ($_REQUEST['address'])
        {
            $where .= " AND o.address LIKE '%" . mysql_like_quote($_REQUEST['address']) . "%'";
        }
        if ($_REQUEST['zipcode'])
        {
            $where .= " AND o.zipcode LIKE '%" . mysql_like_quote($_REQUEST['zipcode']) . "%'";
        }
        if ($_REQUEST['tel'])
        {
            $where .= " AND o.tel LIKE '%" . mysql_like_quote($_REQUEST['tel']) . "%'";
        }
        if ($_REQUEST['mobile'])
        {
            $where .= " AND o.mobile LIKE '%" .mysql_like_quote($_REQUEST['mobile']) . "%'";
        }
        if ($_REQUEST['country'])
        {
            $where .= " AND o.country = '$_REQUEST[country]'";
        }
        if ($_REQUEST['province'])
        {
            $where .= " AND o.province = '$_REQUEST[province]'";
        }
        if ($_REQUEST['city'])
        {
            $where .= " AND o.city = '$_REQUEST[city]'";
        }
        if ($_REQUEST['district'])
        {
            $where .= " AND o.district = '$_REQUEST[district]'";
        }
        //if ($_REQUEST['shipping_id'])
        //{
        //    $where .= " AND o.shipping_id  = '$_REQUEST[shipping_id]'";
        //}
        if ($_REQUEST['shipping_ids'])
        {
            $where .= " AND o.shipping_id  IN ($_REQUEST[shipping_ids])";
        }
        if ($_REQUEST['pay_id'])
        {
            $where .= " AND o.pay_id  = '$_REQUEST[pay_id]'";
        }
        if ($_REQUEST['pay_ids'])
        {
            $where .= " AND o.pay_id  IN ($_REQUEST[pay_ids])";
        }
        if ($_REQUEST['order_status'] != -1)
        {
            $where .= " AND o.order_status  = '$_REQUEST[order_status]'";
        } elseif (!in_array($_REQUEST['composite_status'], [OS_CANCELED, CS_CANCELED_SELF, CS_CANCELED_STAFF]) && !($_REQUEST['order_sn'] || $_REQUEST['user_id'] || $_REQUEST['user_ids'] || $_REQUEST['user_name'])) {
            $where .= " AND o.order_status != '" . OS_CANCELED . "'";
        }
        if ($_REQUEST['shipping_status'] != -1)
        {
            $where .= " AND o.shipping_status ".db_create_in(array_map('intval', explode(',',$_REQUEST['shipping_status'])));
        }
        if ($_REQUEST['pay_status'] != -1)
        {
            $where .= " AND o.pay_status = '$_REQUEST[pay_status]'";
        }
        if ($_REQUEST['user_id'])
        {
            $where .= " AND o.user_id = '$_REQUEST[user_id]'";
        }
        if ($_REQUEST['user_ids'])
        {
            $where .= " AND o.user_id " . db_create_in(array_map('intval', explode(',',$_REQUEST['user_ids'])));
        }
        if ($_REQUEST['user_name'])
        {
            $where .= " AND u.user_name LIKE '%" . mysql_like_quote($_REQUEST['user_name']) . "%'";
        }
        if ($_REQUEST['start_time'])
        {
            $where .= " AND o.add_time >= '$_REQUEST[start_time]'";
        }
        if ($_REQUEST['end_time'])
        {
            $where .= " AND o.add_time <= '$_REQUEST[end_time]'";
        }
        if ($_REQUEST['pay_start_time'])
        {
            $where .= " AND o.pay_time >= '$_REQUEST[pay_start_time]'";
        }
        if ($_REQUEST['pay_end_time'])
        {
            $where .= " AND o.pay_time <= '$_REQUEST[pay_end_time]'";
        }
        if ($_REQUEST['ship_start_time'])
        {
            $where .= " AND o.shipping_time >= '$_REQUEST[ship_start_time]'";
        }
        if ($_REQUEST['ship_end_time'])
        {
            $where .= " AND o.shipping_time <= '$_REQUEST[ship_end_time]'";
        }
        if ($_REQUEST['salesperson'])
        {
            $where .= " AND o.salesperson = '" . $_REQUEST['salesperson'] . "'";
        }
        if($_REQUEST['display_sales_order'] == 1){
            $where .=  " AND o.order_type = '" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT . "' ";
        }
        if($_REQUEST['display_normal_order'] || $_REQUEST['display_sales_order'] == 2){
            $where .= " AND (order_type <> '" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT . "' OR order_type IS NULL) ";
			$where .= " AND o.user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).") ";
        }
        if($_REQUEST['platform'])
        {
            $where .= " AND o.platform = '$_REQUEST[platform]'";
        }
	   
        if ($_REQUEST['have_discount'])
        {
            $where .= " AND o.discount > 0";
        }
        if ($_REQUEST['includes_goods'])
        {
            if (!preg_match('/^([0-9]+,)*[0-9]+$/', $_REQUEST['includes_goods']))
            {
                $error_msg = '「包含產品ID」欄位輸入錯誤，請輸入以逗號(,)分隔的產品ID';
                if ($_REQUEST['is_ajax'])
                {
                    make_json_error($error_msg);
                }
                else
                {
                    sys_msg($error_msg);
                }
            }
            $where .=" AND o.order_id IN (" .
                        "SELECT og.`order_id` " .
                        "FROM " . $GLOBALS['ecs']->table('order_goods') . " as og " .
                        "WHERE og.goods_id IN ({$_REQUEST['includes_goods']}) " .
                    ")";
        }
        if ($_REQUEST['wholesale_receivable'])
        {
            //To handle the old 999999
            $where .= " AND (o.order_type='".Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE."' OR o.user_id IN ('".implode("','",$userController->getWholesaleUserIds())."') OR o.user_id = 5022) AND o.fully_paid = 0";
        }
        if ($_REQUEST['serial_numbers'])
        {
            $where .= " AND (o.serial_numbers LIKE '%" . mysql_like_quote($_REQUEST['serial_numbers']) . "%' OR (o.order_type " . db_create_in(array( Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT, Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE)) . " AND o.type_sn LIKE '%" . mysql_like_quote($_REQUEST['serial_numbers']) . "%') )";
        }
        if ($_REQUEST['flashdeal_id'])
        {
            $where .=" AND EXISTS (" .
                        "SELECT og.`order_id` " .
                        "FROM " . $GLOBALS['ecs']->table('order_goods') . " as og " .
                        "WHERE og.order_id = o.order_id " .
                        "AND og.flashdeal_id = '" . $_REQUEST['flashdeal_id'] . "' " .
                    ")";
        }
        if ($_REQUEST['replacement'])
        {
            $where .= " AND o.postscript LIKE '%換貨，%'";
        }
        if ($_REQUEST['have_preorder'])
        {
            $where .= " AND (o.postscript LIKE '%訂貨%天%' OR o.order_remarks LIKE '%訂貨%天%' OR o.to_buyer LIKE '%訂貨%天%' OR o.order_id IN (SELECT order_id FROM " .$GLOBALS['ecs']->table('order_goods'). " where planned_delivery_date > 0 group by order_id )) ";
        }
        if ($_REQUEST['stock_incorrect'])
        {
            $where .= " AND o.platform != 'pos' " .
                    "AND EXISTS (" .
                        "SELECT og.`order_id` " .
                        "FROM " . $GLOBALS['ecs']->table('order_goods') . " as og " .
                        "WHERE og.order_id = o.order_id AND og.replaced_qty = 0 " .
                        "GROUP BY og.rec_id " .
                        "HAVING SUM(cast(og.goods_number as signed) - cast(og.delivery_qty as signed)) > 0" .
                    ") AND o.order_id NOT IN (" .
                        "SELECT DISTINCT replace_order_id FROM " . $GLOBALS['ecs']->table('order_info') .
                        " WHERE replace_order_id != 0" .
                    ")";
        }
        if ($_REQUEST['invoice_no'])
        {
            $where .= " AND (do.invoice_no LIKE '%" . mysql_like_quote($_REQUEST['invoice_no']) . "%' OR o.invoice_no LIKE '%" . mysql_like_quote($_REQUEST['invoice_no']) . "%') ";
        }
        if ($_REQUEST['logistics_id'] > 0)
        {
            $where .= " AND do.logistics_id = $_REQUEST[logistics_id] ";
        }

        if ($_REQUEST['is_fls']  > 0)
        {
            $where .= " AND do.is_fls = 1 ";
        }

        if ($_REQUEST['total_fee'] != -1)
        {
            $where .= " AND (" .
                "o.goods_amount + o.tax + o.shipping_fee + o.insure_fee + o.pay_fee + o.pack_fee + o.card_fee - o.coupon - o.integral_money - o.money_paid = $_REQUEST[total_fee] OR " .
                "o.order_amount = $_REQUEST[total_fee] OR " .
                "o.money_paid = $_REQUEST[total_fee]" .
            ") ";
        }

        if ($_REQUEST['have_partial_ship_history'])
        {
            $where .= " AND o.order_id IN (" .
                        "SELECT oa.`order_id` " .
                        "FROM " . $GLOBALS['ecs']->table('order_action') . " as oa " .
                        "WHERE oa.shipping_status = 4 " .
                    ")";
        }
        if($_REQUEST['is_wholesale']==1){
            $where .= " AND (o.user_id IN (".implode(',', $userController->getWholesaleUserIds()).") OR o.order_type='".Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE."')";
        }elseif(!$_REQUEST['wholesale_receivable']){
            $where .= " AND (o.order_type <> '".OrderController::DB_ORDER_TYPE_WHOLESALE."' OR o.order_type IS NULL) AND o.user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).")";
        }

        if ( in_array($_REQUEST['composite_status'], [CS_AWAIT_SHIP, CS_AWAIT_SHIP_HK,CS_AWAIT_SHIP_LOCKER,CS_AWAIT_PICK_HK,CS_AWAIT_SHIP_NON_HK,CS_AWAIT_SHIP_HK_YOHO,CS_AWAIT_HK_FLS,CS_AWAIT_HK_WILSON])) {
            $pagination_by_sql = false;
        } else {
            $pagination_by_sql = true;
        }
        
        //综合状态
        switch($_REQUEST['composite_status'])
        {
            case CS_AWAIT_PAY :
                $where .= order_query_sql('await_pay', 'o.');
                break;

            case CS_AWAIT_SHIP :
                $where .= order_query_sql('await_ship', 'o.');
                break;

            case CS_FINISHED :
                $where .= order_query_sql('finished', 'o.');
                break;

            case CS_SHIPPED :
                $where .= order_query_sql('shipped', 'o.');
                break;

            case PS_PAYING :
                if ($_REQUEST['composite_status'] != -1)
                {
                    $where .= " AND o.pay_status = '$_REQUEST[composite_status]' ";
                }
                break;

            case CS_AWAIT_SHIP_HK : case CS_AWAIT_SHIP_HK_YOHO :
                //if ($_REQUEST['all_in_stock'] > 0 || $_REQUEST['need_purchase'] > 0) {
                    $where .= order_query_sql('await_ship', 'o.');
                //} else {
                //    $where .= order_query_sql('await_ship', 'o.') . " AND o.shipping_id IN (3,9,10) AND o.country = 3409 ";
                //}
                break;
            case CS_AWAIT_HK_FLS :
                // if ($_REQUEST['all_in_stock'] > 0 || $_REQUEST['need_purchase'] > 0) {
                    $where .= order_query_sql('await_ship', 'o.');
                //} else {
                    
                //}

                break;
            case CS_AWAIT_HK_WILSON :
                // if ($_REQUEST['all_in_stock'] > 0 || $_REQUEST['need_purchase'] > 0) {
                    $where .= order_query_sql('await_ship', 'o.');
                //} else {
                    
                //}

                break;    
            case CS_AWAIT_SHIP_LOCKER :
                //if ($_REQUEST['all_in_stock'] > 0 || $_REQUEST['need_purchase'] > 0) {
                    $where .= order_query_sql('await_ship', 'o.');
                //} else {
                //   $where .= order_query_sql('await_ship', 'o.') . " AND o.shipping_id = 8 AND o.country = 3409 ";
                //}
                break;

            case CS_AWAIT_PICK_HK :
                //if ($_REQUEST['all_in_stock'] > 0 || $_REQUEST['need_purchase'] > 0) {
                    $where .= order_query_sql('await_ship', 'o.');
                //} else {
                //    $where .= order_query_sql('await_ship', 'o.') . " AND o.shipping_id IN (2,5) ";
                //}
                break;

            case CS_AWAIT_SHIP_NON_HK :
                //if ($_REQUEST['all_in_stock'] > 0 || $_REQUEST['need_purchase'] > 0) {
                    $where .= order_query_sql('await_ship', 'o.');
                //} else {
                //    $where .= order_query_sql('await_ship', 'o.') . " AND o.shipping_id = 3 AND o.country != 3409 ";
                //}
                break;

            case CS_FINISHED_PICK :
                $where .= order_query_sql('shipped_unconfirm', 'o.') . " AND o.shipping_id IN (2,5) ";

                // if ($_REQUEST['display_order_not_packed'] == 1) 
                // {
                //     $where .= " AND do.status != 4 And do.status != 3 And do.logistics_id = 7  ";
                   
                // } 
                // else if ($_REQUEST['display_order_not_pickup'] == 1)
                // {
                //     $where .= " AND do.status != 3 And do.logistics_id = 7  ";
                // }
               
                if ($_REQUEST['display_store_pickup_filter'] == 1) // 未Pack貨
                {
                    $where .= " AND do.status != 4 And do.status != 3 And do.logistics_id = 7  ";
                } 
                else if ($_REQUEST['display_store_pickup_filter'] == 2) //已Pack貨(未通知取件)
                {
                    $where .= " AND do.status = 4 And do.pickup_notified = 0 And do.logistics_id = 7  ";
                }
                else if ($_REQUEST['display_store_pickup_filter'] == 3) //未取貨(已通知取件)
                {
                    $where .= " AND do.status != 3 And do.pickup_notified = 1 And do.logistics_id = 7  ";   
                }
                else if ($_REQUEST['display_store_pickup_filter'] == 4) //未取貨
                {
                    $where .= " AND do.status != 3 And do.logistics_id = 7  ";
                }

                break;

            case CS_PAYED_CASH :
                $where .= " AND o.order_status IN (" . OS_CONFIRMED . ',' . OS_SPLITED . ',' . OS_SPLITING_PART . ") ".
                          " AND o.money_paid > 0 ".
                          " AND o.shipping_status NOT IN (" . SS_SHIPPED . ',' . SS_RECEIVED . ") ".
                          " AND o.pay_id IN (3) ";
                if(!$_REQUEST['platform']) $where .= " AND o.platform <> 'pos_exhibition'";
                break;

            case CS_CANCELED_STAFF : case CS_CANCELED_SELF :
                $where .= " AND o.order_status = " . OS_CANCELED . " ";
                break;

            default:
                if ($_REQUEST['composite_status'] != -1)
                {
                    $where .= " AND o.order_status = '$_REQUEST[composite_status]' ";
                }
        }

        /* 团购订单 */
        if ($_REQUEST['group_buy_id'])
        {
            $where .= " AND o.extension_code = 'group_buy' AND o.extension_id = '$_REQUEST[group_buy_id]' ";
        }

        /* 如果管理员属于某个办事处，只列出这个办事处管辖的订单 */
        $sql = "SELECT agency_id FROM " . $GLOBALS['ecs']->table('admin_user') . " WHERE user_id = '$_SESSION[admin_id]'";
        $agency_id = $GLOBALS['db']->getOne($sql);
        if ($agency_id > 0)
        {
            $where .= " AND o.agency_id = '$agency_id' ";
        }

        /* 分页大小 */
        $_REQUEST['page'] = empty($_REQUEST['page']) || (intval($_REQUEST['page']) <= 0) ? 1 : intval($_REQUEST['page']);

        if (isset($_REQUEST['page_size']) && intval($_REQUEST['page_size']) > 0)
        {
            $_REQUEST['page_size'] = intval($_REQUEST['page_size']);
        }
        // elseif (isset($_COOKIE['ECSCP']['page_size']) && intval($_COOKIE['ECSCP']['page_size']) > 0)
        // {
        //     $_REQUEST['page_size'] = intval($_COOKIE['ECSCP']['page_size']);
        // }
        else
        {
            $_REQUEST['page_size'] = 50;
        }
        
        // To prevent exceeding memory limit, max page_size is limited to 1000
        if ($_REQUEST['page_size'] > 1000)
        {
            $_REQUEST['page_size'] = 1000;
        }

        /* 记录总数 */
        if ( $_REQUEST['display_store_pickup_filter'] > 0 || $_REQUEST['display_order_not_pickup'] == 1 || $_REQUEST['display_order_not_packed'] == 1 ) {
            $sql = "SELECT COUNT( distinct o.order_id) " ;
        } else {
            $sql = "SELECT COUNT( o.order_id) " ;
        }

        // if ($_REQUEST['composite_status'] == CS_AWAIT_HK_FLS) {
        // 
        // } else {
        //     $sellableWarehouseIds = $erpController->getSellableWarehouseIds(true); 
        // }

        $wilson_warehouse_id = $erpController->getWilsonWarehouseId();
        $fls_warehouse_id = $erpController->getFlsWarehouseId();
        $main_warehouse_id = $erpController->getMainWarehouseId();
        $shop_warehouse_id = $erpController->getShopWarehouseId();
        $sellableWarehouseIdsFls = implode(',',[$fls_warehouse_id]);
        
        $sellableWarehouseIds = $erpController->getSellableWarehouseIds(true); 

        if ($pagination_by_sql == true) {
            $sql .=  " FROM " . $GLOBALS['ecs']->table('order_info') . " AS o " .
                    ( $_REQUEST['user_name'] != '' ? " left join " . $GLOBALS['ecs']->table('users') . " AS u on (o.user_id = u.user_id) " : '') .
                    (($_REQUEST['logistics_id']>0 || $_REQUEST['invoice_no'] || $_REQUEST['display_store_pickup_filter'] > 0 || $_REQUEST['display_order_not_pickup'] == 1  || $_REQUEST['display_order_not_packed'] == 1 || $_REQUEST['is_fls'] == 1) ? " LEFT JOIN " .$GLOBALS['ecs']->table('delivery_order'). " AS do ON do.order_id=o.order_id " : "").
                    $where .
                    (($_REQUEST['all_in_stock'] || $_REQUEST['need_purchase']) ? "AND (SELECT IF(min(stock) >= 0, 'Y', 'N') FROM (SELECT og.order_id, (SUM(ega.goods_qty) - og.goods_number + og.send_number) as stock FROM " . $GLOBALS['ecs']->table('erp_goods_attr_stock') . " as ega LEFT JOIN " .$GLOBALS['ecs']->table('order_goods'). " as og ON ega.goods_id = og.goods_id AND ega.warehouse_id IN (".$sellableWarehouseIds.") GROUP BY og.order_id, og.goods_id) as gs WHERE gs.order_id = o.order_id) = '" . ($_REQUEST['need_purchase'] ? "N" : "Y") . "'" : '') .
                     ($_REQUEST['need_purchase'] ? "AND IFNULL((SELECT max(1) FROM ".$GLOBALS['ecs']->table('order_action')." as oa WHERE oa.order_id = o.order_id AND oa.action_note LIKE '%已向供應商訂貨%'), 0) = 0" : '') ;

           
           $record_count   = $GLOBALS['db']->getOne($sql);
           
        }

        $_REQUEST['page_count']     = $_REQUEST['record_count'] > 0 ? ceil($_REQUEST['record_count'] / $_REQUEST['page_size']) : 1;

        /* 查询 */
        $sql = "SELECT o.order_id, o.order_sn, o.add_time, o.order_status, o.shipping_id, o.shipping_status, o.order_amount, o.money_paid, o.cp_price, o.is_hold, o.tag_ids, " .
                    "o.pay_status, o.order_type, o.type_sn, o.type_id, o.actg_paid_amount,o.pay_id, o.extension_code, o.extension_id, o.postscript, o.serial_numbers,  " .
                    "o.consignee, o.address, o.country, o.email, o.tel, o.pay_time, s.shipping_name, o.shipping_fod, o.language, p.pay_name, " .
                    "(" . order_amount_field('o.') . ") AS total_fee, o.to_buyer, oa.dest_type AS address_type_id, oa.lift AS address_lift, oadt.address_dest_type_cat AS address_cat_id, " .
                    "(o.money_paid + o.order_amount) AS order_total, " . // howang: money_paid(已付金額) + order_amount(應付金額) = 訂單總金額
                  //  (in_array($_REQUEST['composite_status'], [CS_AWAIT_SHIP,CS_AWAIT_SHIP_HK,CS_AWAIT_PICK_HK,CS_AWAIT_SHIP_NON_HK,CS_AWAIT_SHIP_HK_YOHO,CS_AWAIT_SHIP_LOCKER,CS_AWAIT_HK_FLS]) ? "IF((SELECT min(stock) FROM (SELECT og.order_id, (SUM(ega.goods_qty) - og.goods_number + og.send_number) as stock FROM " . $GLOBALS['ecs']->table('erp_goods_attr_stock') . " as ega LEFT JOIN " .$GLOBALS['ecs']->table('order_goods'). " as og ON ega.goods_id = og.goods_id AND ega.warehouse_id IN (".$sellableWarehouseIds.") GROUP BY og.order_id, og.goods_id) as gs WHERE gs.order_id = o.order_id) >= 0, 'Y', 'N') AS all_in_stock, " : ''). // Added by howang
                    "IFNULL((SELECT max(flashdeal_id) FROM " .$GLOBALS['ecs']->table('order_goods'). " as og1 WHERE og1.order_id = o.order_id GROUP BY og1.order_id), 0) as flashdeal_id, ".
                    "IFNULL(u.user_name, '" .$GLOBALS['_LANG']['anonymous']. "') AS buyer, o.user_id, o.salesperson, o.platform, ".
                    "o.fully_paid, o.actg_paid_amount, ". // Accounting System for wholesale orders
                    "IFNULL((SELECT count(*) FROM ".$GLOBALS['ecs']->table('order_action')." as oa WHERE oa.order_id = o.order_id), 0) as action_count, " . // Action count
                    "IFNULL((SELECT 1 FROM ".$GLOBALS['ecs']->table('order_action')." as oa WHERE oa.order_id = o.order_id AND oa.action_user = 'buyer' AND oa.order_status = '" . OS_CANCELED . "'), 0) as buyer_cancel, " . // Cancel count
                    "IFNULL((SELECT max(1) FROM ".$GLOBALS['ecs']->table('order_action')." as oa WHERE oa.order_id = o.order_id AND oa.action_note LIKE '%已向供應商訂貨%'), 0) as ordered_from_supplier " .
                " FROM " . $GLOBALS['ecs']->table('order_info') . " AS o " .
                //" LEFT JOIN " .$GLOBALS['ecs']->table('erp_order'). " AS eo ON eo.client_order_sn=o.order_sn ".
                (($_REQUEST['logistics_id']>0 || $_REQUEST['invoice_no'] || $_REQUEST['display_store_pickup_filter'] > 0 || $_REQUEST['display_order_not_pickup'] == 1 ||  $_REQUEST['display_order_not_packed'] == 1 || $_REQUEST['is_fls'] == 1 ) ? " LEFT JOIN " .$GLOBALS['ecs']->table('delivery_order'). " AS do ON do.order_id=o.order_id " : "").
                " LEFT JOIN " .$GLOBALS['ecs']->table('payment'). " AS p ON p.pay_id=o.pay_id ".
                " LEFT JOIN " .$GLOBALS['ecs']->table('shipping'). " AS s ON s.shipping_id=o.shipping_id ".
                " LEFT JOIN " .$GLOBALS['ecs']->table('order_address'). " AS oa ON oa.order_address_id=o.address_id ".
                " LEFT JOIN " .$GLOBALS['ecs']->table('order_address_dest_type'). " AS oadt ON oadt.address_dest_type_id=oa.dest_type ".
                " LEFT JOIN " .$GLOBALS['ecs']->table('users'). " AS u ON u.user_id=o.user_id ". $where .
                //" Group by o.order_id ".
                //($_REQUEST['all_in_stock'] ? "HAVING all_in_stock = 'Y' " : '') .
                //($_REQUEST['need_purchase'] ? "HAVING all_in_stock = 'N' AND ordered_from_supplier = 0 " : '') .
                ($_REQUEST['need_purchase'] ? "HAVING ordered_from_supplier = 0 " : '') .
                ($_REQUEST['composite_status'] == CS_CANCELED_SELF ? "HAVING buyer_cancel = 1 " : "") .
                ($_REQUEST['composite_status'] == CS_CANCELED_STAFF ? "HAVING buyer_cancel = 0 " : "") .
                ($_REQUEST['display_store_pickup_filter'] > 0 || $_REQUEST['display_order_not_pickup'] == 1 || $_REQUEST['display_order_not_packed'] == 1 ? "Group by o.order_id " : '') .
                " ORDER BY $_REQUEST[sort_by] $_REQUEST[sort_order] ";

                if ($pagination_by_sql == true) {
                    $sql .= " LIMIT " . $_REQUEST['start'] . ",$_REQUEST[page_size]";
                }

        foreach (array('order_sn', 'consignee', 'email', 'address', 'zipcode', 'tel', 'user_name') AS $val)
        {
            $_REQUEST[$val] = stripslashes($_REQUEST[$val]);
        }
        // set_filter($filter, $sql);

        $row = $GLOBALS['db']->getAll($sql);

        // grouping
        $platforms = OrderController::DB_SELLING_PLATFORMS;

        // Order goods list
        $order_ids = array_map(function ($order) { return $order['order_id']; }, $row);

        // check 代理送貨已出PO
        $po_sql = "select eo.order_sn as order_sn_po, oi.order_id from ".$GLOBALS['ecs']->table('erp_order')." eo left join ".$GLOBALS['ecs']->table('order_info')." oi on (eo.client_order_sn = oi.order_sn) where oi.order_id ". db_create_in($order_ids) ." ";

        //echo $po_sql;
        $po_sn_list = $GLOBALS['db']->getAll($po_sql);

        $po_sn_list_ar = [];
        foreach($po_sn_list as $po_sn) {
            $po_sn_list_ar[$po_sn['order_id']][] = $po_sn['order_sn_po'];
        }

        $order_goods_ids = [];
        // get order goods ids
        $sql = "SELECT distinct og.goods_id " .
                "FROM " . $GLOBALS['ecs']->table('order_goods') . " as og " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON og.goods_id = g.goods_id " .
                "WHERE order_id " . db_create_in($order_ids);
        $order_goods_ids = $GLOBALS['db']->getCol($sql);

        // for globals goods qty
        $sql = "SELECT ega.goods_id, IFNULL(SUM(ega.goods_qty),0) as storage " .
                "FROM " . $GLOBALS['ecs']->table('erp_goods_attr_stock') . " AS ega " .
                "WHERE ega.goods_id " . db_create_in($order_goods_ids) ." AND ega.warehouse_id IN (".$sellableWarehouseIds.") ".
                "group by goods_id";
        
        $order_goods = $GLOBALS['db']->getAll($sql);

        $goods_qty = [];
        foreach ($order_goods as $order_goods_info){
            
            $goods_qty[$order_goods_info['goods_id']] = $order_goods_info['storage'];
        }

        foreach ($order_goods_ids as $order_goods_id) {
            if (!isset($goods_qty[$order_goods_id])) {
                $goods_qty[$order_goods_id] = 0;
            }
        }

        $sql = "SELECT og.*, g.goods_thumb, g.goods_height, g.goods_width, g.goods_length " .
            "FROM " . $GLOBALS['ecs']->table('order_goods') . " as og " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON og.goods_id = g.goods_id " .
            "WHERE order_id " . db_create_in($order_ids);

        $order_goods = $GLOBALS['db']->getAll($sql);

        $order_goods_by_order_id = [];
        foreach ($order_goods as $order_goods_key =>  $order_goods_info){
            // add storage to order_goods ar
            $order_goods[$order_goods_key]['storage'] = $goods_qty[$order_goods_info['goods_id']];
            $order_goods_by_order_id[$order_goods_info['order_id']][] = $order_goods[$order_goods_key];
        }

        // get all warehouse qty
        $goods_storage_wh = $erpController->getSellableProductsQtyByWarehouses($order_goods_ids,null,false);

        // for FLS warehouse
        $goods_qty_fls = [];
        foreach ($goods_storage_wh as $goods_storage_wh_key => $goods_storage_wh_info){
            $goods_qty_fls[$goods_storage_wh_key] = isset($goods_storage_wh_info[$fls_warehouse_id])? $goods_storage_wh_info[$fls_warehouse_id]:0;
        }

        // for Wilson warehouse
        $goods_qty_wilson = [];
        foreach ($goods_storage_wh as $goods_storage_wh_key => $goods_storage_wh_info){
            $goods_qty_wilson[$goods_storage_wh_key] = isset($goods_storage_wh_info[$wilson_warehouse_id])? $goods_storage_wh_info[$wilson_warehouse_id]:0;
        }

        // only main warehouse (exclude fls, shop, wilson)
        $main_goods_qty = [];
        foreach ($goods_storage_wh as $goods_storage_wh_key => $goods_storage_wh_info){
            $main_goods_qty[$goods_storage_wh_key] = isset($goods_storage_wh_info[$main_warehouse_id])? $goods_storage_wh_info[$main_warehouse_id]:0;
        }

        // get shop warehouse
        $shop_goods_qty = [];
        foreach ($goods_storage_wh as $goods_storage_wh_key => $goods_storage_wh_info){
            $shop_goods_qty[$goods_storage_wh_key] = isset($goods_storage_wh_info[$shop_warehouse_id])? $goods_storage_wh_info[$shop_warehouse_id]:0;
        }

        // get qty excluded shop
        $goods_qty_excluded_shop = [];
        foreach ($order_goods_ids as $order_goods_id) {
            $goods_qty_excluded_shop[$order_goods_id] = $goods_qty[$order_goods_id] - $shop_goods_qty[$order_goods_id] ;
        }

        // Get sales agent name
        //$salesagent =get_sales_agent_name();
	
        if (in_array($_REQUEST['composite_status'], [CS_AWAIT_SHIP, CS_AWAIT_SHIP_HK,CS_AWAIT_SHIP_LOCKER,CS_AWAIT_PICK_HK,CS_AWAIT_SHIP_NON_HK,CS_AWAIT_SHIP_HK_YOHO,CS_AWAIT_HK_FLS,CS_AWAIT_HK_WILSON])) {
            $row_temp = $row;
            // sort by paid time
            usort($row_temp, function($a, $b) {
                return $a['pay_time'] - $b['pay_time'];
            });

            $orders_can_ship = [];
            $orders_cant_ship = [];
            $orders_can_ship_fls = [];
            $orders_can_ship_wilson = [];
            $orders_can_ship_shop = [];
            $orders_cant_ship_shop = [];

            $order_grouping = true;

            if ($order_grouping == true || in_array($_REQUEST['composite_status'], [CS_AWAIT_SHIP_HK,CS_AWAIT_SHIP_LOCKER,CS_AWAIT_PICK_HK,CS_AWAIT_SHIP_NON_HK,CS_AWAIT_SHIP_HK_YOHO,CS_AWAIT_HK_FLS,CS_AWAIT_HK_WILSON])) {
                // check FLS Stock 
                foreach ($row_temp AS $key => $value)
                {
                    if (isset($order_goods_by_order_id[$value['order_id']]))
                    {
                        $all_order_goods_in_stock = true;
                        // FLS new flow
                        // in 待出貨(本地速遞) HK
                        if (in_array($value['shipping_id'],[3]) and $value['country'] == 3409) {
                            foreach ($order_goods_by_order_id[$value['order_id']] as $ogs)
                            {
                                if ( ($goods_qty_fls[$ogs['goods_id']] - $ogs['goods_number'] + $ogs['send_number']) >= 0) {
                                    $goods_qty_fls[$ogs['goods_id']] = $goods_qty_fls[$ogs['goods_id']] -$ogs['goods_number'] + $ogs['send_number'];
                                } else {
                                    $goods_qty_fls[$ogs['goods_id']] = 0;
                                    $all_order_goods_in_stock = false;
                                }
                            }

                            // all in stock
                            if ($all_order_goods_in_stock == true) {
                                $orders_can_ship_fls[] = $value['order_id']; 
                            }
                        }
                    }
                }

                // check Wilson Stock 
                foreach ($row_temp AS $key => $value)
                {
                    if (isset($order_goods_by_order_id[$value['order_id']]))
                    {
                        $all_order_goods_in_stock = true;
                        // Wilson new flow
                        // in 待出貨(本地速遞) HK
                        if (in_array($value['shipping_id'],[3]) and $value['country'] == 3409) {
                            foreach ($order_goods_by_order_id[$value['order_id']] as $ogs)
                            {
                                if ( ($goods_qty_wilson[$ogs['goods_id']] - $ogs['goods_number'] + $ogs['send_number']) >= 0) {
                                    $goods_qty_wilson[$ogs['goods_id']] = $goods_qty_wilson[$ogs['goods_id']] -$ogs['goods_number'] + $ogs['send_number'];
                                } else {
                                    $goods_qty_wilson[$ogs['goods_id']] = 0;
                                    $all_order_goods_in_stock = false;
                                }
                            }

                            // all in stock
                            if ($all_order_goods_in_stock == true) {
                                $orders_can_ship_wilson[] = $value['order_id']; 
                            }
                        }
                    }
                }
            }

            if ($order_grouping == true || in_array($_REQUEST['composite_status'], [CS_AWAIT_PICK_HK])) {
                // check Shop Stock // for store pick up
                foreach ($row_temp AS $key => $value)
                {
                    if (isset($order_goods_by_order_id[$value['order_id']]))
                    {
                        $all_order_goods_in_stock = true;
                        //待出貨(門店自取)
                        if (in_array($value['shipping_id'],[2,5])) {
                            foreach ($order_goods_by_order_id[$value['order_id']] as $ogs)
                            {
                                if ( ($shop_goods_qty[$ogs['goods_id']] - $ogs['goods_number'] + $ogs['send_number']) >= 0) {
                                    $shop_goods_qty[$ogs['goods_id']] = $shop_goods_qty[$ogs['goods_id']] -$ogs['goods_number'] + $ogs['send_number'];
                                } else {
                                    $shop_goods_qty[$ogs['goods_id']] = 0;
                                    $all_order_goods_in_stock = false;
                                }
                            }

                            // all in stock
                            if ($all_order_goods_in_stock == true) {
                                $orders_can_ship_shop[] = $value['order_id']; 
                            } else {
                                $orders_cant_ship_shop[] = $value['order_id'];
                            }
                        }
                    }
                }
            }
            // echo '<pre>';
            // print_r($orders_can_ship_shop);
            // print_r($orders_cant_ship_shop);
            // exit;

            $grouping_ar['star'][0]['size'] = 0;
            $grouping_ar['star'][0]['cut_off_time'] =  local_date($_CFG['time_format'], $this->getlogisticsClosestCutOffTime('star',0));

            $grouping_ar['express_hk'][3]['size'] = 0;
            $grouping_ar['express_hk'][3]['cut_off_time'] =  local_date($_CFG['time_format'], $this->getlogisticsClosestCutOffTime('express_hk',3));

            $grouping_ar['express_hk'][9]['size'] = 0;
            $grouping_ar['express_hk'][9]['cut_off_time'] =  local_date($_CFG['time_format'], $this->getlogisticsClosestCutOffTime('express_hk',9));

            $grouping_ar['express_hk'][10]['size'] = 0;
            $grouping_ar['express_hk'][10]['cut_off_time'] =  local_date($_CFG['time_format'], $this->getlogisticsClosestCutOffTime('express_hk',10));

            $grouping_ar['express_hk'][14]['size'] = 0;
            $grouping_ar['express_hk'][14]['cut_off_time'] =  local_date($_CFG['time_format'], $this->getlogisticsClosestCutOffTime('express_hk',14));

            $grouping_ar['express_hk'][15]['size'] = 0;
            $grouping_ar['express_hk'][15]['cut_off_time'] =  local_date($_CFG['time_format'], $this->getlogisticsClosestCutOffTime('express_hk',15));

            $grouping_ar['express_hk'][16]['size'] = 0;
            $grouping_ar['express_hk'][16]['cut_off_time'] =  local_date($_CFG['time_format'], $this->getlogisticsClosestCutOffTime('express_hk',16));

            $grouping_ar['locker'][8]['size'] = 0;
            $grouping_ar['locker'][8]['cut_off_time'] =  local_date($_CFG['time_format'], $this->getlogisticsClosestCutOffTime('locker',8));

            $grouping_ar['locker'][11]['size'] = 0;
            $grouping_ar['locker'][11]['cut_off_time'] =  local_date($_CFG['time_format'], $this->getlogisticsClosestCutOffTime('locker',11));

            $grouping_ar['locker'][12]['size'] = 0;
            $grouping_ar['locker'][12]['cut_off_time'] =  local_date($_CFG['time_format'], $this->getlogisticsClosestCutOffTime('locker',12));
            
            $grouping_ar['locker'][13]['size'] = 0;
            $grouping_ar['locker'][13]['cut_off_time'] =  local_date($_CFG['time_format'], $this->getlogisticsClosestCutOffTime('locker',13));

            $grouping_ar['express_oversea'][0]['size'] = 0;
            $grouping_ar['express_oversea'][0]['cut_off_time'] =  local_date($_CFG['time_format'], $this->getlogisticsClosestCutOffTime('express_oversea',0));

            $grouping_ar['express_cn'][3]['size'] = 0;
            $grouping_ar['express_cn'][3]['cut_off_time'] =  local_date($_CFG['time_format'], $this->getlogisticsClosestCutOffTime('express_cn',3));

            $grouping_ar['fls'][0]['size'] = 0;
            $grouping_ar['fls'][0]['cut_off_time'] =  local_date($_CFG['time_format'], $this->getlogisticsClosestCutOffTime('fls',0));
          
            $grouping_ar['wilson'][0]['size'] = 0;
            $grouping_ar['wilson'][0]['cut_off_time'] =  local_date($_CFG['time_format'], $this->getlogisticsClosestCutOffTime('wilson',0));
          
            $grouping_ar['hold'][0]['size'] = 0;
            $grouping_ar['hold'][0]['cut_off_time'] =  local_date($_CFG['time_format'], $this->getlogisticsClosestCutOffTime('hold',0));

            $grouping_ar['search_form'][0]['size'] = 0;

            foreach ($row_temp AS $key => $value)
            {
                if ($order_grouping == true) 
                {
                    if ($_REQUEST['search_form'] == 1) {
                        $grouping_ar['search_form'][0]['orders'][] = $value['order_id'];
                    }

                    // skip hold order
                    if ($value['is_hold'] == true) {
                        $cut_off_time =  local_strtotime($grouping_ar['hold'][0]['cut_off_time']);
                        if ($value['pay_time'] <= $cut_off_time) {
                            $grouping_ar['hold'][0]['orders'][] = $value['order_id'];
                        }
                        continue;
                    }

                    // skip tags order
                    if (!empty($value['tag_ids'])) {
                        $cut_off_time =  local_strtotime($grouping_ar['star'][0]['cut_off_time']);
                        if ($value['pay_time'] <= $cut_off_time) {
                            $grouping_ar['star'][0]['orders'][] = $value['order_id'];
                        }
                        continue;
                    }
                    
                    // 速遞
                    // skip directly, because it will be ship by FLS warehouse
                    if (in_array($value['order_id'],$orders_can_ship_fls)) {
                        $cut_off_time =  local_strtotime($grouping_ar['fls'][0]['cut_off_time']);
                        if ($value['pay_time'] <= $cut_off_time) {
                            $grouping_ar['fls'][0]['orders'][] = $value['order_id'];
                        }
                        continue;
                    }

                    // skip directly, because it will be ship by WILSON warehouse
                    if (in_array($value['order_id'],$orders_can_ship_wilson)) {
                        $cut_off_time = $this->getlogisticsClosestCutOffTime('wilson',0);
                        if ($value['pay_time'] <= $cut_off_time) {
                            $grouping_ar['wilson'][0]['orders'][] = $value['order_id'];
                        }
                        continue;
                    }

                    // goods qty global - shop qty
                    $all_order_goods_in_stock = true;
                    foreach ($order_goods_by_order_id[$value['order_id']] as $ogs)
                    {
                        if ($goods_qty_excluded_shop[$ogs['goods_id']] - $ogs['goods_number'] + $ogs['send_number'] >= 0) {
                            $goods_qty_excluded_shop[$ogs['goods_id']] = $goods_qty_excluded_shop[$ogs['goods_id']] -$ogs['goods_number'] + $ogs['send_number'];
                        } else {
                            $goods_qty_excluded_shop[$ogs['goods_id']] = 0;
                            $all_order_goods_in_stock = false;
                        }
                    }

                    if ($all_order_goods_in_stock == true) {
                        //case CS_AWAIT_SHIP_HK : case CS_AWAIT_SHIP_HK_YOHO :
                        // 3 速遞 9 便利店 10 順豐服務中心  15 郵政局 16 其他自提點-中通
                        if (in_array($value['shipping_id'],[3,9,10,14,15,16]) and $value['country'] == 3409) {
                            // get cut off time 
                            $cut_off_time = $this->getlogisticsClosestCutOffTime('express_hk',$value['shipping_id']);
                            if ($value['pay_time'] <= $cut_off_time) {
                                $grouping_ar['express_hk'][$value['shipping_id']]['orders'][] = $value['order_id'];
                            }
                        } else if (in_array($value['shipping_id'],[8, 11, 12, 13]) and $value['country'] == 3409) {
                            // 8 順便智能櫃  11 派寶箱 12 Lockerlife 13 中通智能櫃
                            $cut_off_time = $this->getlogisticsClosestCutOffTime('locker',$value['shipping_id']);
                            if ($value['pay_time'] <= $cut_off_time) {
                                $grouping_ar['locker'][$value['shipping_id']]['orders'][] = $value['order_id'];
                            }
                        } else if (in_array($value['shipping_id'],[3,6,7]) and $value['country'] != 3409 and $value['country'] != 3436) {
                            $cut_off_time = $this->getlogisticsClosestCutOffTime('express_oversea',0);
                            if ($value['pay_time'] <= $cut_off_time) {
                                $grouping_ar['express_oversea'][0]['orders'][] = $value['order_id'];
                            }
                        } else if (in_array($value['shipping_id'],[3]) and $value['country'] == 3436) {
                            $cut_off_time = $this->getlogisticsClosestCutOffTime('express_cn',$value['shipping_id']);
                            if ($value['pay_time'] <= $cut_off_time) {
                                $grouping_ar['express_cn'][$value['shipping_id']]['orders'][] = $value['order_id'];
                            }
                        }
                    }
                }
            }

            $all_in_stock_order = [];
            foreach ($grouping_ar as $logistic_key => &$grouping_shippings) {
                foreach ($grouping_shippings as $shipping_key => &$grouping_shipping) {
                    //$selected_logistic_type = null,$selected_shipping_id = null
                    if (($logistic_key == $selected_logistic_type && $shipping_key == $selected_shipping_id) || $_REQUEST['search_form'] == 1) {
                        if (sizeof($grouping_shipping['orders']) > 0) {
                            $row_temp = [];
                            foreach ($row AS $key => $value)
                            {
                                if (in_array($value['order_id'],$grouping_shipping['orders'])) {
                                    $grouping_shipping['order_details'][] = $value;       
                                }
                            }
                        }
                    }

                    // for order search  
                    // gether all order id in stock
                    if (sizeof($grouping_shipping['orders']) > 0) {
                        $all_in_stock_order = array_merge($all_in_stock_order,$grouping_shipping['orders']);    
                    }

                    if ($_REQUEST['search_form'] == 1) {
                        $grouping_shipping['order_instock'] = $all_in_stock_order ;
                    }

                    $grouping_shipping['size'] = sizeof($grouping_shipping['orders']);
                }
            }

            // dd($grouping_ar);
            // exit;
            return $grouping_ar;
        }
    }

    function updateOrderTagId()
    {
        global $db, $ecs ,$_CFG, $_LANG;
        
        $order_id = $_REQUEST['order_id'];
        $tag_id = $_REQUEST['tag_id'];
        $tag_status = $_REQUEST['tag_status'];

        if (!empty($order_id) && $tag_id !='' && $tag_status !='' ) {
            if ($tag_status == 0) {
                $tag_id = null;
            } else {
                $tag_id = 1;
            }

            $sql = "update ".$ecs->table('order_info')." set tag_ids = '".$tag_id."' where order_id = '".$order_id."' ";
            $db->query($sql);
        }

        return true;
    }

    function updateOrderShipSummary()
    {
        global $db, $ecs ,$_CFG, $_LANG;
        
        $order_ship_detail = $this->getOrderShipDetail();
        
        return $order_ship_detail;

        //dd($order_ship_detail);
        //exit;

    }

}