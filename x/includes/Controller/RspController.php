<?php

/***
* by Anthony 20180410
* Removal Service Plan
* Rsp controller
***/

namespace Yoho\cms\Controller;
use Yoho\cms\Model;
if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}
class RspController extends YohoBaseController {

    const RSP_STATUS_UNCALL    = 1; // 未通知收集者
    const RSP_STATUS_CALLED    = 2; // 己通知收集者 - 待收集
    const RSP_STATUS_COLLECTED = 3; // 已收集
    const RSP_STATUS_CANCEL    = 4; // 不需要此服務
    const STATUS_STRING_ARRAY        = [
        self::RSP_STATUS_UNCALL    => '未通知收集者',
        self::RSP_STATUS_CALLED    => '己通知收集者',
        self::RSP_STATUS_COLLECTED => '收集者已收集',
        self::RSP_STATUS_CANCEL    => '不選用此服務'
    ];

    const RSP_FROM_VOICE    = "V";   // 口頭/電話通知
    const RSP_FROM_INTERNET = "NET"; // 互聯網
    const RSP_FROM_OTHER    = "W";   // 其他(Whatsapp/短訊/微信/傳真...etc)
    const FROM_STRING_ARRAY        = [
        self::RSP_FROM_VOICE    => '口頭/電話通知',
        self::RSP_FROM_INTERNET => '互聯網',
        self::RSP_FROM_OTHER    => '其他(Whatsapp/短訊/微信/傳真...etc)'
    ];

    public function __construct(){
        parent::__construct();
        $this->tableName = 'rsp_record';
        $this->admin_priv = 'rsp_record';
    }

    public function needRemovalGoods($goods_list)
    {
        global $ecs, $db, $_CFG;
        $goods_id_list = array_column($goods_list, 'goods_id');

        // Find goods_list have need_removal product.
        $sql = "SELECT g.goods_id, g.cat_id, c.need_removal, IFNULL(cl.cat_name, c.cat_name) as cat_name FROM ".$ecs->table('goods')." as g ".
        " LEFT JOIN ".$ecs->table('category')." as c ON c.cat_id = g.cat_id ".
        " LEFT JOIN ".$ecs->table('category_lang') . " as cl ON c.cat_id = cl.cat_id AND cl.lang = '" . $_CFG['lang'] . "' " .
        " WHERE g.goods_id ".db_create_in($goods_id_list)." AND c.need_removal = 1 ";

        $need_removal_list = $db->getAll($sql);
        $res = [];

        // Count need_removal total by category.
        foreach($need_removal_list as $key => $nr) {
            $nr_goods_id = $nr['goods_id'];
            foreach($goods_list as $gkey => $goods) {
                if($goods['goods_id'] == $nr_goods_id) {
                    $res[$nr['cat_id']]['cat_id']    = $nr['cat_id'];
                    $res[$nr['cat_id']]['cat_name']  = $nr['cat_name'];
                    if(!isset($res[$nr['cat_id']]['total'])) $res[$nr['cat_id']]['total'] = 0;
                    $res[$nr['cat_id']]['total']    += $goods['goods_number'];
                    break;
                }
            }
        }

        return $res;
    }
    public function ajaxfindOrderAction()
    {
        global $ecs, $db;
        require(ROOT_PATH . 'includes/lib_order.php');
        $order_sn = $_REQUEST['order_sn'];
        $order = order_info(0, $order_sn);
        if(!$order || $order['order_id'] == 0) {
            make_json_error('訂單不存在');
        } else {
            $goods_list = $db->getAll("SELECT * FROM ".$ecs->table('order_goods')." WHERE order_id = $order[order_id]");
            make_json_result('1','',['goods_list'=>$this->needRemovalGoods($goods_list), 'order'=>$order]);
        }
    }
    public function handle_insert_record($data, $from = self::RSP_FROM_INTERNET, $order_id = 0)
    {

        global $ecs, $db;

        // Default status is uncall.
        $status           = self::RSP_STATUS_UNCALL;
        // Set now time
        $create_at        = gmtime();
        if(empty($data['need_removal'])) {
            $status = self::RSP_STATUS_CANCEL;
            $collection_total = '';
        } else {
            // (應收數目)
            $collection_total = serialize(array_filter($data['nr_cat']));
        }
        // Consignee info:
        $address          = isset($data['nr_address']) ? $data['nr_address'] : '';
        $consignee        = isset($data['nr_consignee']) ? $data['nr_consignee'] : '';
        $tel              = isset($data['nr_phone']) ? $data['nr_phone'] : '';

        if($order_id > 0) {
            $order_sn = $db->getOne("SELECT order_sn FROM ".$ecs->table('order_info')." WHERE order_id = $order_id LIMIT 1");

            // Get buy total: How many can removal items in order.
            $sql = "SELECT SUM(og.goods_number) as total, g.cat_id FROM ".$ecs->table('order_goods')." as og ".
            " LEFT JOIN ".$ecs->table('goods')." as g ON g.goods_id = og.goods_id ".
            " LEFT JOIN ".$ecs->table('category')." as c ON c.cat_id = g.cat_id ".
            " WHERE og.order_id = $order_id AND c.need_removal = 1 GROUP BY g.cat_id ";

            $buy_total_list = $db->getAll($sql);
            $buy_total = [];
            foreach($buy_total_list as $key => $buy) {
                $buy_total[$buy['cat_id']] = $buy['total'];
            }
            $buy_total = serialize($buy_total);

            $count = $db->getOne("SELECT count(*) FROM ".$ecs->table($this->tableName)." WHERE order_id = $order_id LIMIT 1");
            // RS = Removal Service
            $rsp_sn = "RS".$order_sn.$count;
        } else {
            return false;
        }

        // Insert Data
        $rsp = [];
        $rsp['status']           = $status;
        $rsp['create_at']        = $create_at;
        $rsp['create_from']      = $from;
        $rsp['address']          = $address;
        $rsp['consignee']        = $consignee;
        $rsp['tel']              = $tel;
        $rsp['order_id']         = $order_id;
        $rsp['order_sn']         = $order_sn;
        $rsp['buy_total']        = $buy_total;
        $rsp['rsp_sn']           = $rsp_sn;
        $rsp['collection_total'] = $collection_total;

        $model = new  Model\YohoBaseModel($this->tableName);
        $id = $model->insert($rsp);

        // Add Order remark:
        $remark = empty($data['need_removal']) ? " 放棄WEEE回收服務" : " 選取WEEE回收服務";
        $postscript = $db->getOne("SELECT postscript FROM ".$ecs->table('order_info')." WHERE order_id = ".$order_id);
        $postscript = str_replace(' 放棄WEEE回收服務', '', $postscript);
        $postscript = str_replace(' 選取WEEE回收服務', '', $postscript);
        $postscript .= $remark;
        $db->query("UPDATE ".$ecs->table('order_info')." SET postscript = '$postscript' WHERE order_id = ".$order_id);

        return $id;
    }

    public function getList()
    {
        /* 查询条件 */
        $_REQUEST['order_sn'] = empty($_REQUEST['order_sn']) ? 0 : trim($_REQUEST['order_sn']);
        $_REQUEST['sort_by'] = empty($_REQUEST['sort_by']) ? 'add_time' : trim($_REQUEST['sort_by']);
        $_REQUEST['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);

        $where = (!empty($_REQUEST['order_sn'])) ? " AND order_sn LIKE '%". mysql_like_quote($_REQUEST['order_sn']) . "%' " : '';

        $sql = "SELECT count(*) FROM " . $GLOBALS['ecs']->table($this->tableName) . " WHERE 1 $where";
        $_REQUEST['record_count'] = $GLOBALS['db']->getOne($sql);

        /* 获取评论数据 */
        $arr = array();
        $sql = "SELECT rsp.*, o.user_id FROM " . $GLOBALS['ecs']->table($this->tableName) . " as rsp ".
            " LEFT JOIN ". $GLOBALS['ecs']->table('order_info') . " as o on o.order_id = rsp.order_id ".
            " WHERE 1 $where " .
            " ORDER BY $_REQUEST[sort_by] $_REQUEST[sort_order], rsp_id desc " .
            " LIMIT " . $_REQUEST['start'] . ", $_REQUEST[page_size]";
        $res = $GLOBALS['db']->query($sql);

        while ($row = $GLOBALS['db']->fetchRow($res)) {

            $arr[] = $this->format_rsp($row);
        }

        $arr = array('item' => $arr, 'record_count' => $_REQUEST['record_count']);

        return $arr;
    }

    function format_rsp($rsp)
    {
        $rsp['buy_total']                        = unserialize($rsp['buy_total']);
        $rsp['collection_total']                 = unserialize($rsp['collection_total']);
        $rsp['real_collection_total']            = unserialize($rsp['real_collection_total']);
        $rsp['create_date_format']               = local_date('Y-m-d', $rsp['create_at']);
        $rsp['call_date_format']                 = local_date('Y-m-d', $rsp['call_date']);
        $rsp['collection_date_format']           = local_date('Y-m-d', $rsp['collection_date']);
        $rsp['status_text']                      = self::STATUS_STRING_ARRAY[$rsp['status']];
        $rsp['call_from_text']                   = self::FROM_STRING_ARRAY[$rsp['call_from']];
        $rsp['create_from_text']                 = self::FROM_STRING_ARRAY[$rsp['create_from']];
        $rsp['appointed_collection_date_format'] = local_date('Y-m-d', $rsp['appointed_collection_date']);
        return $rsp;
    }
    public function listAction()
	{
        admin_priv($this->admin_priv);
        $order_sn = empty($_REQUEST['order_sn']) ? '' : $_REQUEST['order_sn'];
        $ur_here =  "WEEE回收安排記錄";
        $assign = [];
        $assign['order_sn'] = $order_sn;
        $assign['ur_here']  = $ur_here;
		parent::listAction($assign, 'rsp_record_list.htm');
    }

    public function ajaxQueryAction()
	{
        $list = $this->getList();

        // We're using dataTables Ajax to query.
        $this->tableList = $list['item'];
        $this->tableTotalCount = $list['record_count'];

        parent::ajaxQueryAction(null, null, ['view']);
    }

    public function buildForm($formMapper = [], $id = 0, $act = '', $assign = [], $redirect = '')
    {
        global $ecs, $db;
        $jsFile = 'rsp.js';
        $sql = "SELECT cat_name, cat_id FROM ".$ecs->table('category')." WHERE need_removal = 1 ";
        $cat_list = $db->getAll($sql);
        if(empty($formMapper))
        $formMapper = [
            'order_sn' => ['required' => true, 'events' => ['onChange' => 'checkExistOrder(this);']],
            'create_from' => ['required' => true, 'type' => 'select', 'options'=> self::FROM_STRING_ARRAY],
            'address' => ['required' => true, 'className' => 'disabled_input'],
            'tel' => ['required' => true, 'className' => 'disabled_input'],
            'consignee' => ['required' => true, 'className' => 'disabled_input'],
        ];
        foreach($cat_list as $k => $cat) {
            $formMapper['nr_cat['.$cat['cat_id'].']'] = ['type'=>'number', 'label'=>$cat['cat_name'], 'className' => 'disabled_input'];
        }
        parent::buildForm($formMapper, $id, $act, $assign, $redirect, $jsFile);
    }

    public function insertAction()
    {
        admin_priv($this->admin_priv);
        $order_id = $GLOBALS['db']->getOne("SELECT order_id FROM ".$GLOBALS['ecs']->table('order_info')." WHERE order_sn = '".$_REQUEST['order_sn']."' ");
        $_REQUEST['nr_address']   = $_REQUEST['address'];
        $_REQUEST['nr_consignee'] = $_REQUEST['consignee'];
        $_REQUEST['nr_phone']     = $_REQUEST['tel'];
        $_REQUEST['need_removal'] = 1;
        $id = $this->handle_insert_record($_REQUEST, $_REQUEST['create_from'], $order_id);
        if(!$id) {
            sys_msg('無法找到訂單號', 1, array(), false);
        }
        $act = $_REQUEST['act'];


        /* 清除缓存 */
        clear_cache_files();

		admin_log($this->tableName.': '.$id, ($act=='insert') ? 'add' : 'edit', $this->tableName);

		$e = new \Exception();
		$trace = $e->getTrace();
		$file = end($trace)['file'];
		$file = end(explode("/", $file));
		$href = $file.'?act=list';

		$link[0]['text'] = _L('back_list', '返回列表');
        $link[0]['href'] = $href;
        $note = _L('attradd_succed', '操作成功');
        sys_msg($note, 0, $link);
    }

    public function cancelAction()
    {
        global $ecs, $db;
        $id = $_REQUEST['id'];
        $model = new Model\YohoBaseModel($this->tableName, $id);
        $new_rsp = [
            'status' => self::RSP_STATUS_CANCEL
        ];

        $res = $model->update($new_rsp, $id);
        /* 清除缓存 */
        clear_cache_files();

		admin_log('放棄舊貨回收服務: '.$id, 'edit', $this->tableName);

		$e = new \Exception();
		$trace = $e->getTrace();
		$file = end($trace)['file'];
		$file = end(explode("/", $file));
		$href = $file.'?act=list';

		$link[0]['text'] = _L('back_list', '返回列表');
        $link[0]['href'] = $href;
        $note = _L('attradd_succed', '操作成功');
        sys_msg($note, 0, $link);
    }

    public function viewAction()
    {
        admin_priv($this->admin_priv);
        global $ecs, $db, $_LANG;
        if($_REQUEST[$pk]) $id = $_REQUEST[$pk];
		else  $id = $_REQUEST['id'];
        $model       = new Model\YohoBaseModel($this->tableName, $id);
        $rsp  = $this->format_rsp($model->getData());
        $sql = "SELECT cat_name, cat_id FROM ".$ecs->table('category')." WHERE need_removal = 1 ";
        $res = $db->getAll($sql);
        $cat_list = [];
        foreach($res as $k => $cat) {
            $cat_list[$cat['cat_id']] = $cat['cat_name'] ;
        }
        $assign['cat_list']     = $cat_list;
        $assign['rsp']          = $rsp;
        $assign['status']       = $rsp['status'];
        $assign['ur_here']      = 'WEEE回收記錄查看';
        $assign['action_link']  = array('href' => 'rsp_record.php?act=list', 'text' => $_LANG['rsp_list']);
        $assign['status_text']  = self::STATUS_STRING_ARRAY;
        $assign['from_option']  = self::FROM_STRING_ARRAY;
        $assign['today']        = local_date('Y-m-d');
        parent::generateActionTemplate($assign);
    }

    public function stepAction()
    {
        global $ecs, $db;
        admin_priv($this->admin_priv);
        extract($_REQUEST);
        $model = new Model\YohoBaseModel($this->tableName, $rsp_id);
        if($status == self::RSP_STATUS_UNCALL) {
            // Set now time
            $call_date = gmtime();
            // (應收數目)
            $collection_total = serialize(array_filter($nr_cat));
            $appointed_collection_date = local_strtotime($appointed_collection_date);
            $old_status = self::STATUS_STRING_ARRAY[$status];
            $status = self::RSP_STATUS_CALLED;

            $new_rsp = [
                'call_date'                 => $call_date,
                'collection_total'          => $collection_total,
                'appointed_collection_date' => $appointed_collection_date,
                'remark'                    => $remark,
                'status'                    => $status,
                'collection_name'           => $collection_name,
                'call_from'                 => $call_from
            ];

            $res = $model->update($new_rsp, $rsp_id);
        } elseif($status == self::RSP_STATUS_CALLED) {
            // (應收數目)
            $real_collection_total = serialize(array_filter($nr_cat));
            $collection_date       = local_strtotime($collection_date);
            $old_status = self::STATUS_STRING_ARRAY[$status];
            $status = self::RSP_STATUS_COLLECTED;

            $new_rsp = [
                'real_collection_total' => $real_collection_total,
                'collection_date'       => $collection_date,
                'remark'                => $remark,
                'status'                => $status
            ];

            $res = $model->update($new_rsp, $rsp_id);
        }


        /* 清除缓存 */
        clear_cache_files();

		admin_log('WEEE回收安排記錄Step '.$old_status, 'edit', $this->tableName);

		$e = new \Exception();
		$trace = $e->getTrace();
		$file = end($trace)['file'];
		$file = end(explode("/", $file));
		$href = $file.'?act=list';

		$link[0]['text'] = _L('back_list', '返回列表');
        $link[0]['href'] = $href;
        $note = _L('attradd_succed', '操作成功');
        sys_msg($note, 0, $link);
    }

}