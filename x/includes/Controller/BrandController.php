<?php

/***
* BrandController.php
* by Anthony 20170804
*
* Brand controller
***/

namespace Yoho\cms\Controller;

use Yoho\cms\Model;

if (!defined('IN_ECS')) {
    die('Hacking attempt');
}

class BrandController extends YohoBaseController
{

    protected $admin_priv = 'brand_manage';
    /**
    * 获得与指定品牌相关的分类
    *
    * @access  public
    * @param   integer $brand
    * @return  array
    */
    public function brand_related_cat($brand_id, $brand_name = '')
    {
        $sql = "SELECT g.cat_id, IFNULL(cl.cat_name, c.cat_name) as child_cat_name,p.parent_id as p_id, IFNULL(pcl.cat_name, p.cat_name) as parent_cat_name, COUNT(g.goods_id) AS goods_count " .
                "FROM " . $GLOBALS['ecs']->table('goods') . " as g " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('category') . " as c ON c.cat_id = g.cat_id " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('category') . " as p ON c.parent_id  = p.cat_id AND p.is_show = 1 " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('category_lang') . " as cl ON cl.cat_id = c.cat_id AND cl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('category_lang') . " as pcl ON pcl.cat_id = p.cat_id AND pcl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
                "WHERE g.brand_id = '" . $brand_id . "' AND g.is_on_sale = 1 AND g.is_alone_sale = 1 AND g.is_delete = 0 " .
                "GROUP BY g.cat_id order by c.sort_order";

        $res = $GLOBALS['db']->getAll($sql);
        global $smarty, $db, $ecs;

        $array      = array();
        $parent_ids = array();
        foreach ($res as $key => $row) {
            $array[$row['parent_cat_name']][$key] = $row;
            $array[$row['parent_cat_name']][$key]['url'] = build_uri('brand', array(
                'cid' => $row['cat_id'], 'cname' => $row['child_cat_name'],
                'bid' => $brand_id, 'bname' => $brand_name), $row['child_cat_name']);
            $array[$row['parent_cat_name']][$key]['child_cat_name'] = $row['child_cat_name'];
            $array[$row['parent_cat_name']][$key]['parent_cat_name'] = $row['parent_cat_name'];
            $array[$row['parent_cat_name']]['id'] = $row['p_id'];
            $parent_ids[] = $row['p_id'];
        }

        /* Anthony: Get Level 1 category sort order */
        $sql = "SELECT IFNULL(cl.cat_name, c.cat_name) as cat_name FROM ".$ecs->table('category') . " AS c " .
                " LEFT JOIN " . $ecs->table('category_lang') . " as cl ON c.cat_id = cl.cat_id AND cl.lang = '" . $_CFG['lang'] . "' ".
                " WHERE c.cat_id ".db_create_in($parent_ids)." ORDER BY c.sort_order ";
        $p_sort_order = $db->getCol($sql);

        /* Sort by Level 1 sort order  */
        $array = array_replace(array_flip($p_sort_order), $array);

        $result = array();
        $result[0] = $array;
        $result[1] = count($res);
        return $result;
    }

    public function ajaxQueryAction()
    {
        $list = $this->get_brandlist();
        // We're using dataTables Ajax to query.
        $this->tableList       = $list['brand'];
        $this->tableTotalCount = $list['record_count'];

        parent::ajaxQueryAction();
    }

    public function ajaxEditAction()
    {
        global $db, $ecs;
        $exc = new \exchange($ecs->table("brand"), $db, 'brand_id', 'brand_name');
        if($_REQUEST['col'] == "brand_name") {
            /* 检查名称是否重复 */
            if ($exc->num("brand_name",$_REQUEST['value'], $_REQUEST['id']) != 0)
            {
                make_json_error(sprintf(_L('brandname_exist', ''), $name));
            }
        }
        parent::ajaxEditAction();
    }


    /**
     * 获取品牌列表
     *
     * @access  public
     * @return  array
     */
    public function get_brandlist()
    {
        //$result = get_filter();
        //if ($result === false) {
            /* 分页大小 */
            $filter = $_REQUEST;

            /* 记录总数以及页数 */
            if (isset($_POST['brand_name'])) {
                $sql = "SELECT COUNT(*) FROM ".$GLOBALS['ecs']->table('brand') .' WHERE brand_name = \''.$_POST['brand_name'].'\'';
            } else {
                $sql = "SELECT COUNT(*) FROM ".$GLOBALS['ecs']->table('brand');
            }

            $filter['record_count'] = $GLOBALS['db']->getOne($sql);

            // $filter = page_and_size($filter);

            /* 查询记录 */
            if (isset($_POST['brand_name'])) {
                if (strtoupper(EC_CHARSET) == 'GBK') {
                    $keyword = iconv("UTF-8", "gb2312", $_POST['brand_name']);
                } else {
                    $keyword = $_POST['brand_name'];
                }
                $sql = "SELECT *, if(brand_logo is null OR brand_logo = '', 0, 1) as has_logo FROM ".$GLOBALS['ecs']->table('brand')." WHERE brand_name like '%{$keyword}%' ORDER BY ".$_POST['sort_by']." ".$_POST['sort_order'];
            } else {
                $sql = "SELECT *, if(brand_logo is null OR brand_logo = '', 0, 1) as has_logo FROM ".$GLOBALS['ecs']->table('brand')." ORDER BY ".$_POST['sort_by']." ".$_POST['sort_order'];
            }

            //set_filter($filter, $sql);
        $res = $GLOBALS['db']->selectLimit($sql, $filter['page_size'], $filter['start']);

        $arr = array();
        while ($rows = $GLOBALS['db']->fetchRow($res)) {
            $brand_logo = empty($rows['brand_logo']) ? '' :
            '<a href="../' . DATA_DIR . '/brandlogo/'.$rows['brand_logo'].'" target="_brank"><img src="images/picflag.gif" width="16" height="16" border="0" alt='.$GLOBALS['_LANG']['brand_logo'].' /></a>';
            $site_url   = empty($rows['site_url']) ? 'N/A' : '<a href="'.$rows['site_url'].'" target="_brank">'.$rows['site_url'].'</a>';

            $rows['brand_logo'] = $brand_logo;
            $rows['site_url']   = $site_url;

            $arr[] = $rows;
        }

        // Multiple language support
        if (!empty($_REQUEST['edit_lang'])) {
            $arr = localize_db_result_lang($_REQUEST['edit_lang'], 'brand', $arr);
        }

        return array('brand' => $arr, 'filter' => $filter, 'page_count' => $filter['page_count'], 'record_count' => $filter['record_count']);
    }

    public function buildForm($formMapper = [], $id = 0, $act = '', $assign = [], $redirect = '')
    {
        if(empty($formMapper))
        $formMapper = [
            'brand_name' => ['required' => true],
            'perma_link' => ['type' => 'perma_link', 'ref' => 'brand_name', 'label' => '永久鏈結', 'url' => 'brand'],
            'site_url' => ['default' => 'http://'],
            'brand_logo' => ['type' => 'file', 'notice'=> _L('up_brandlogo', '')],
            'brand_banner' => ['type' => 'file', 'notice'=> _L('up_brandbanner', '')],
            'brand_mobile_banner' => ['type' => 'file', 'notice'=> _L('up_brandbannermobile', '')],
            'brand_desc' => ['type' => 'textarea'],
            'brand_keywords' => [],
            'sort_order' => ['type' => 'number', 'default' => 50],
            'is_show' => ['type' => 'radio', 'default' => true, 'notice'=> _L('visibility_notes', '')],
            'is_update' => ['type' => 'radio', 'default' => 1],
            'only_market_invoice' => ['type' => 'radio', 'default' => 0]
        ];
        parent::buildForm($formMapper, $id, $act, $assign, $redirect);
    }

    function get_sale_brand($is_pagination = true)
    {
        $userController = new UserController();
        $erpController = new ErpController();
        $orderController = new OrderController();
        ini_set('memory_limit', '512M');
        require_once(ROOT_PATH . 'includes/lib_order.php');
        require_once ROOT_PATH . 'includes/lib_howang.php';
        $filter['start_date'] = empty($_REQUEST['start_date']) ? strtotime(local_date('Y-m-01')) : strtotime($_REQUEST['start_date']);
        $filter['end_date'] = empty($_REQUEST['end_date']) ? strtotime(local_date('Y-m-t')) : strtotime($_REQUEST['end_date']);
        $filter['order_type'] = empty($_REQUEST['order_type']) ? 0 : intval($_REQUEST['order_type']);
        
        $sql = "SELECT COUNT(*) FROM " . $GLOBALS['ecs']->table('brand') . " WHERE is_show = 1";
        $filter['record_count'] = intval($GLOBALS['db']->getOne($sql)) + 2; // + all brand + no brand = +2
        $userController = new UserController();
        
        $where = " oi.shipping_time >= '".$filter['start_date']."' AND oi.shipping_time < '" . ($filter['end_date'] + 86400) . "' ".
        order_query_sql('finished', 'oi.');
        // Ignore 換貨訂單
        $where .= " AND oi.postscript NOT LIKE '%換貨，%'";

        if ($filter['order_type'] == 4) // 所有訂單
        {
            $where_ws = "(" . str_replace("oi.pay_status  IN ('", "oi.pay_status  IN ('".PS_UNPAYED."','", explode('AND', order_query_sql('finished', 'oi.'))[3]);
            $where_ws .= " AND (oi.order_type = '".$orderController::DB_ORDER_TYPE_WHOLESALE."' OR (oi.user_id IN (".implode(',', $userController->getWholesaleUserIds()).") AND oi.order_type IS NULL) ) ";
            $where = str_replace("oi.pay_status  IN ('", "( oi.pay_status  IN ('", $where) . " OR " . $where_ws . " ) )";
        }
        else if ($filter['order_type'] == 1) // 批發訂單
        {
            // Only include orders for the followiung user account: 99999999
            $where = str_replace("oi.pay_status  IN ('", "oi.pay_status  IN ('".PS_UNPAYED."','", $where);
            $where .= " AND (oi.order_type = '".$orderController::DB_ORDER_TYPE_WHOLESALE."' OR (oi.user_id IN (".implode(',', $userController->getWholesaleUserIds()).") AND oi.order_type IS NULL) ) ";
        }
        else if ($filter['order_type'] == 2) // 借貨訂單
        {
            // Only include orders for the followiung user accounts: 11111111, 88888888
            $where .= " AND oi.user_id IN (4940,5023) ";
        }
        else if ($filter['order_type'] == 3) // 代理銷售訂單
        {
            $where .= " AND oi.order_type = '".$orderController::DB_ORDER_TYPE_SALESAGENT."' ";
        }
        else if ($filter['order_type'] == 0) // Normal order
        {
            // Exclude orders for the followiung user accounts: 11111111, 88888888, 99999999
            // Exclude all order_type for SA/WS
            $where .= " AND (oi.order_type NOT IN ('".$orderController::DB_ORDER_TYPE_SALESAGENT."','".$orderController::DB_ORDER_TYPE_WHOLESALE."') OR (oi.order_type IS NULL AND oi.user_id NOT IN (".implode(',',$userController->getWholesaleUserIds()).")) ) ";
            $where .= " AND (oi.platform <> 'pos_exhibition') ";
        }
        else if ($filter['order_type'] == 5) // Exhibition order
        {
            // Exclude orders for the followiung user accounts: 11111111, 88888888, 99999999
            // Exclude all order_type for SA/WS
            $where .= " AND (oi.order_type NOT IN ('".$orderController::DB_ORDER_TYPE_SALESAGENT."','".$orderController::DB_ORDER_TYPE_WHOLESALE."') OR (oi.order_type IS NULL AND oi.user_id NOT IN (".implode(',',$userController->getWholesaleUserIds()).")) ) ";
            $where .= " AND (oi.platform = 'pos_exhibition') ";
        }

        // Exclude order with sale exclude coupon
        $where .= " AND (c.sale_exclude = 0 OR c.sale_exclude IS NULL) ";
        
        /* 分页大小 */
        $filter = page_and_size($filter);
        $time_yesterday = local_strtotime('today'); // 23:59:59 yesterday
        $time_90days_before = local_strtotime('-90 days midnight'); // 00:00:00 90 days ago
        $sql = "SELECT g.brand_id, IFNULL(b.brand_name, '沒有品牌') as brand_name, b.sort_order, " .
                'IFNULL(sum(gn.goods_number),0) as total_stocks, ' .
                'IFNULL(sum(gn.goods_number * g.cost),0) as total_cost, ' .
                'IFNULL(sum(gp.revenue),0) as total_revenue, ' .
                'IFNULL(sum(gp.revenue - gp.total_discount),0) as total_revenue_n_ds, ' .
                'IFNULL(sum(gp.total_discount),0) as total_discount, ' .
                'IFNULL(sum(gp.cost),0) as total_gpcost, ' .
                'IFNULL(sum(gp.revenue - gp.cost),0) as total_profit, ' .
                'IFNULL(sum(gp.revenue - gp.total_discount - gp.cost),0) as total_profit_n_ds, ' .
                'IFNULL(sum(gp.sap),0) as total_sa_revenue, '.
                'IFNULL(sum(gp.sap - gp.total_discount),0) as total_sa_revenue_n_ds, '.
                'IFNULL(sum(gp.sap - gp.cost),0) as total_sa_profit, ' .
                'IFNULL(sum(gp.sap - gp.total_discount - gp.cost),0) as total_sa_profit_n_ds ' .
            'FROM ' . $GLOBALS['ecs']->table('goods') . ' AS g ' .
            'LEFT JOIN ' . $GLOBALS['ecs']->table('brand') . ' AS b ON g.brand_id = b.brand_id ' .
            // 'LEFT JOIN (SELECT ega.goods_id, sum(ega.goods_qty) as goods_number FROM ' . $GLOBALS['ecs']->table('erp_goods_attr_stock') . ' AS ega WHERE ega.warehouse_id IN ('.$erpController->getSellableWarehouseIds(true).') GROUP BY ega.goods_id) as gn ON gn.goods_id = g.goods_id ' .
            " LEFT JOIN (SELECT g.goods_id, rg.goods_num as goods_number FROM ".$GLOBALS['ecs']->table('goods')." as g ".
            " LEFT JOIN ( SELECT ".hw_goods_number_subquery('g.','goods_num').", g.goods_id FROM ".$GLOBALS['ecs']->table('goods')." as g ) as rg ON rg.goods_id = g.goods_id ".
            " WHERE rg.goods_num > 0 order by g.goods_id DESC ) as gn on gn.goods_id = g.goods_id ".
            'LEFT JOIN (' .
                'SELECT og.goods_id, SUM((oi2.total_o_ds/oi2.total_og_price)*(og.goods_price * og.goods_number)) as total_discount ,' .
                'sum((IF(og.cost > 0, og.cost, g2.cost) + ' .
                    'IF(oi.`pay_id` = 2,  IF(og.cost > 0, og.cost, g2.cost) * 0.0095,       0) + ' . // EPS: 0.95% fee
                    'IF(oi.`pay_id` = 7,  IF(og.cost > 0, og.cost, g2.cost) * 0.032 + 2.35, 0) + ' . // PayPal: 3.2% fee + $2.35 per txn
                    'IF(oi.`pay_id` = 10, IF(og.cost > 0, og.cost, g2.cost) * 0.029,        0) + ' . // UnionPay: 2.9% fee
                    'IF(oi.`pay_id` = 12, IF(og.cost > 0, og.cost, g2.cost) * 0.023,        0) + ' . // Visa / MasterCard: 2.3% fee
                    'IF(oi.`pay_id` = 13, IF(og.cost > 0, og.cost, g2.cost) * 0.0296,       0)   ' . // American Express: 2.96% fee
                    ') * og.goods_number + ' .
                    'IF(oi.`shipping_id` = 3, 30 * og.goods_price * og.goods_number / oi.goods_amount, 0) ' . // Delivery cost: $30
                ') as cost, ' .
                "sum( (( ((100 - IF(og.sa_rate_id > 0 , sa.sa_rate, 0) ) /100 ) * og.goods_price)*og.goods_number) - IF(oi.`pay_fee` < 0, oi.`pay_fee` * -1 * og.goods_price * og.goods_number / oi.goods_amount, 0) ) as sap, ".
                'sum(og.goods_price * og.goods_number - IF(oi.`pay_fee` < 0, oi.`pay_fee` * -1 * og.goods_price * og.goods_number / oi.goods_amount, 0)) as revenue ' .
                'FROM ' . $GLOBALS['ecs']->table('order_goods') . ' AS og ' .
                'LEFT JOIN ' . $GLOBALS['ecs']->table('order_info') . ' AS oi ON oi.order_id = og.order_id ' .
                'LEFT JOIN ' . $GLOBALS['ecs']->table('goods') . ' AS g2 ON g2.goods_id = og.goods_id ' .
                "LEFT JOIN " . $GLOBALS['ecs']->table('sales_agent_rate') . " as sa ON sa.sa_rate_id = og.sa_rate_id ".
                'LEFT JOIN (SELECT (oi.`discount` + oi.`bonus` + oi.`coupon`)as total_o_ds, sum(og.goods_number * og.goods_price)as total_og_price, oi.order_id from ' . $GLOBALS['ecs']->table('order_info') . ' as oi left join ' . $GLOBALS['ecs']->table('order_goods') . ' AS og ON oi.order_id = og.order_id group by oi.order_id) as oi2 ON oi2.order_id = og.order_id '.
                'LEFT JOIN ' . $GLOBALS['ecs']->table('order_coupons') . ' AS oc ON oc.order_id = oi.order_id ' .
                'LEFT JOIN ' . $GLOBALS['ecs']->table('coupons') . ' AS c ON c.coupon_id = oc.coupon_id ' .
                'WHERE ' . $where .
                'GROUP BY og.goods_id' .
            ') as gp ON gp.goods_id = g.goods_id ' .
            "WHERE (b.is_show IS NULL OR b.is_show = 1) AND g.is_delete = 0 GROUP BY g.brand_id ORDER BY brand_name ASC";
        // $sql_cost = "SELECT b.brand_id, " .
        // 	"(CASE WHEN sc.stock_cost > 0 THEN sc.stock_cost ELSE 0 END) AS final_stock_cost FROM " .
        // 		"(SELECT g.brand_id, g.is_delete, " .
        // 		"CASE WHEN rc.reduce_qty <= tc.purchase_qty THEN SUM(tc.total_cost - rc.reduce_cost + (gn.goods_number - tc.purchase_qty + rc.reduce_qty) * g.cost) ELSE SUM(gn.goods_number * g.cost) END AS stock_cost FROM ecs_goods g " .
        // 		"LEFT JOIN (SELECT eoi.goods_id, SUM(IFNULL(eoi.amount,0) + IFNULL(eoi.shipping_price,0)) AS total_cost, SUM(IFNULL(eoi.order_qty,0)) AS purchase_qty FROM " . $GLOBALS['ecs']->table('erp_order_item') . " eoi " .
        // 			"LEFT JOIN " . $GLOBALS['ecs']->table('erp_order') . " eo ON eo.order_id = eoi.order_id " .
        // 			"WHERE eo.locked_time >= 1500000000 GROUP BY eoi.goods_id) tc ON tc.goods_id = g.goods_id " .
        // 		"LEFT JOIN (SELECT ogsc.goods_id, SUM(ogsc.po_unit_cost * ogsc.consumed_qty) AS reduce_cost, SUM(IFNULL(ogsc.consumed_qty,0)) AS reduce_qty FROM " . $GLOBALS['ecs']->table('order_goods_supplier_consumption') . " ogsc " .
        // 			"GROUP BY ogsc.goods_id) rc ON rc.goods_id = g.goods_id " .
        // 		"LEFT JOIN (SELECT ega.goods_id, SUM(IFNULL(ega.goods_qty,0)) AS goods_number FROM " . $GLOBALS['ecs']->table('erp_goods_attr_stock') . " ega " .
        // 			"WHERE ega.warehouse_id IN (SELECT warehouse_id FROM " . $GLOBALS['ecs']->table('erp_warehouse') . " WHERE is_valid = 1 AND is_on_sale = 1) GROUP BY ega.goods_id ) gn ON gn.goods_id = g.goods_id " .
        // 		"WHERE g.is_delete = 0 GROUP BY g.brand_id) sc " .
        // 	"LEFT JOIN " . $GLOBALS['ecs']->table('brand') .  " b ON b.brand_id = sc.brand_id " .
        // 	"WHERE (b.is_show IS NULL OR b.is_show = 1) AND sc.is_delete = 0";
        // if ($is_pagination)
        // {
        //     $sql .= " LIMIT " . $filter['start'] . ', ' . $filter['page_size'];
        // }
        $sql_avg_cost = "SELECT type_id AS brand_id, AVG(cost) AS avg_stock_cost FROM " . $GLOBALS['ecs']->table('stock_cost_log') . " WHERE report_date >= " . $filter['start_date'] .
            " AND report_date <= " . ($filter['end_date'] + 86400) . " AND created_at IN (SELECT MAX(created_at) FROM " . $GLOBALS['ecs']->table('stock_cost_log') . " GROUP BY report_date) AND type = 2 GROUP BY brand_id";
        $sql_dead_cost = "SELECT type_id AS brand_id, AVG(slowsoldcost_90) AS avg_dead_cost FROM " . $GLOBALS['ecs']->table('stock_cost_log') . " WHERE report_date >= " . $filter['start_date'] .
            " AND report_date <= " . ($filter['end_date'] + 86400) . " AND created_at IN (SELECT MAX(created_at) FROM " . $GLOBALS['ecs']->table('stock_cost_log') . " GROUP BY report_date) AND type = 2 AND report_date > 1527091200 GROUP BY brand_id";
        $sql_brand_list = 
        "SELECT eoi.goods_id,g.goods_num,eoi.warehousing_qty as qty,(g.goods_num-eoi.warehousing_qty) as sum,eoi.price as unit_cost ,eo.order_id, g.brand_id
        FROM ".$GLOBALS['ecs']->table('erp_order_item')." eoi 
        LEFT JOIN ".$GLOBALS['ecs']->table('erp_order')." eo ON eoi.order_id=eo.order_id ".
        "LEFT JOIN (SELECT g.brand_id,g.goods_id, rg.goods_num FROM ".$GLOBALS['ecs']->table('goods')." as g ".
        " LEFT JOIN ( SELECT ".hw_goods_number_subquery('g.','goods_num').", g.goods_id FROM ".$GLOBALS['ecs']->table('goods')." as g ) as rg ON rg.goods_id = g.goods_id ".
        " WHERE rg.goods_num > 0 order by g.goods_id DESC ) as g on g.goods_id = eoi.goods_id ".
        "WHERE g.goods_num > 0 AND eo.order_status = 4 AND eoi.warehousing_qty > 0 ORDER BY eoi.goods_id DESC, eoi.order_item_id DESC";
            
        $po_list = $GLOBALS['db']->getAll($sql_brand_list);
        $cost_arr = array();
        $next_goods = true;
        $goods_id   = 0;
        foreach ($po_list as $key => $po) {
            if($next_goods && $po['goods_id'] == $goods_id)continue;

            // What goods we processing
            $next_goods = false;
            $goods_num = ($po['goods_id'] == $goods_id)?$in_qty:$po['goods_num'];
            $goods_id = $po['goods_id'];
            $in_qty   = $goods_num - $po['qty'];
            if($in_qty > 0) // Stock > PO 入貨數
            {
                $qty  = $po['qty'];
                $cost = $qty * $po['unit_cost'];
            } elseif ($in_qty <= 0)
            {
                $qty  = $goods_num;
                $cost = $qty * $po['unit_cost'];
                $next_goods = true;
            }
            $cost_arr[$po['brand_id']]['cost'] = isset($cost_arr[$po['brand_id']]['cost']) ? $cost_arr[$po['brand_id']]['cost']+= $cost: $cost;
            $cost_arr[$po['brand_id']]['qty']  = isset($cost_arr[$po['brand_id']]['qty']) ? $cost_arr[$po['brand_id']]['qty']+= $qty: $qty;
        }

        $res = $GLOBALS['db']->getAll($sql);
        $avg_cost = $GLOBALS['db']->getAll($sql_avg_cost);
        $dead_cost = $GLOBALS['db']->getAll($sql_dead_cost);
        $total_stocks = 0;
        $total_cost = 0;
        $total_revenue = 0;
        $total_gpcost = 0;
        $total_profit = 0;
        $data = array();
        $avg_cost_arr = array();
        $dead_cost_arr = array();

        foreach($avg_cost as $row){
            $avg_cost_arr[$row['brand_id']] = $row['avg_stock_cost'];
        }
        foreach($dead_cost as $row){
            $dead_cost_arr[$row['brand_id']] = $row['avg_dead_cost'];
        }
        foreach ($res AS $row)
        {
                $curr_cost = isset($cost_arr[$row['brand_id']]['cost']) ? $cost_arr[$row['brand_id']]['cost'] : 0;
                $curr_avg_cost = isset($avg_cost_arr[$row['brand_id']]) ? $avg_cost_arr[$row['brand_id']] : 0;
                $curr_dead_cost = isset($dead_cost_arr[$row['brand_id']]) ? $dead_cost_arr[$row['brand_id']] : 0;
                $total_stocks += $row['total_stocks'];
                $total_cost += $row['total_cost'];
                $total_revenue += $row['total_revenue'];
                $total_revenue_n_ds += $row['total_revenue_n_ds'];
                $total_gpcost += $row['total_gpcost'];
                $total_profit += $row['total_profit'];
                $total_profit_n_ds += $row['total_profit_n_ds'];
                $total_sa_revenue += $row['total_sa_revenue'];
                $total_sa_profit += $row['total_sa_profit'];
                $total_sa_revenue_n_ds += $row['total_sa_revenue_n_ds'];
                $total_sa_profit_n_ds += $row['total_sa_profit_n_ds'];
                $total_fifo_cost += $curr_cost;
                $total_avg_cost += $curr_avg_cost;
                $total_dead_cost += $curr_dead_cost;

                $item = isset($data[$row['brand_id']]) ? $data[$row['brand_id']] : array();
                $item['id']   = $row['brand_id'];
                $item['name'] = $row['brand_name'];
                $item['total_stocks']  = (isset($item['total_stocks']) ? $item['total_stocks'] : 0) + $row['total_stocks'];
                $item['avg_stock_cost']  = $curr_avg_cost;
                $item['avg_stock_cost_formated']  = price_format($item['avg_stock_cost'], false);
                $item['total_cost']  = (isset($item['total_cost']) ? $item['total_cost'] : 0) + $row['total_cost'];
                $item['total_cost_formated'] = price_format($item['total_cost'], false);
                $item['total_fifo_cost']  = $curr_cost;
                $item['total_fifo_cost_formated'] = price_format($item['total_fifo_cost'], false);
                $item['total_dead_cost']  = $curr_dead_cost;
                $item['total_dead_cost_formated'] = price_format($item['total_dead_cost'], false);
                $item['total_revenue']  = (isset($item['total_revenue']) ? $item['total_revenue'] : 0) + $row['total_revenue'];
                $item['total_revenue_formated'] = price_format($item['total_revenue'], false);
                $item['total_revenue_n_ds']  = (isset($item['total_revenue_n_ds']) ? $item['total_revenue_n_ds'] : 0) + $row['total_revenue_n_ds'];
                $item['total_revenue_n_ds_formated'] = price_format($item['total_revenue_n_ds'], false);
                $item['total_gpcost']  = (isset($item['total_gpcost']) ? $item['total_gpcost'] : 0) + $row['total_gpcost'];
                $item['total_profit_n_ds']  = (isset($item['total_profit_n_ds']) ? $item['total_profit_n_ds'] : 0) + $row['total_profit_n_ds'];
                $item['total_profit_n_ds_formated'] = price_format($item['total_profit_n_ds'], false);
                $item['profit_margin_n_ds'] = $item['total_revenue_n_ds'] > 0 ? ($item['total_profit_n_ds'] / $item['total_revenue_n_ds']) : 0;
                $item['profit_margin_n_ds_formated'] = number_format($item['profit_margin_n_ds'] * 100, 2, '.', '') . '%';
                $item['total_sa_revenue_n_ds']  = (isset($item['total_sa_revenue_n_ds']) ? $item['total_sa_revenue_n_ds'] : 0) + $row['total_sa_revenue_n_ds'];
                $item['total_sa_revenue_n_ds_formated'] = price_format($item['total_sa_revenue_n_ds'], false);
                $item['total_sa_profit_n_ds']  = (isset($item['total_sa_profit_n_ds']) ? $item['total_sa_profit_n_ds'] : 0) + $row['total_sa_profit_n_ds'];
                $item['total_sa_profit_n_ds_formated'] = price_format($item['total_sa_profit_n_ds'], false);
                $item['sa_profit_margin_n_ds'] = $item['total_sa_revenue_n_ds'] > 0 ? ($item['total_sa_profit_n_ds'] / $item['total_sa_revenue_n_ds']) : 0;
                $item['sa_profit_margin_n_ds_formated'] = number_format($item['sa_profit_margin_n_ds'] * 100, 2, '.', '') . '%';
                $item['total_profit']  = (isset($item['total_profit']) ? $item['total_profit'] : 0) + $row['total_profit'];
                $item['total_profit_formated'] = price_format($item['total_profit'], false);
                $item['profit_margin'] = $item['total_revenue'] > 0 ? ($item['total_profit'] / $item['total_revenue']) : 0;
                $item['profit_margin_formated'] = number_format($item['profit_margin'] * 100, 2, '.', '') . '%';
                $item['total_sa_revenue']  = (isset($item['total_sa_revenue']) ? $item['total_sa_revenue'] : 0) + $row['total_sa_revenue'];
                $item['total_sa_revenue_formated'] = price_format($item['total_sa_revenue'], false);
                $item['total_sa_profit']  = (isset($item['total_sa_profit']) ? $item['total_sa_profit'] : 0) + $row['total_sa_profit'];
                $item['total_sa_profit_formated'] = price_format($item['total_sa_profit'], false);
                $item['sa_profit_margin'] = $item['total_sa_revenue'] > 0 ? ($item['total_sa_profit'] / $item['total_sa_revenue']) : 0;
                $item['sa_profit_margin_formated'] = number_format($item['sa_profit_margin'] * 100, 2, '.', '') . '%';
                $item['display'] = isset($_REQUEST['order_type']) ? $_REQUEST['order_type'] : 4;
                
                $data[$row['brand_id']] = $item;
        }
        array_unshift($data, array(
            'id' => 0,
            'name' => '所有品牌',
            'total_stocks' => $total_stocks,
            'total_cost' => $total_cost,
            'total_cost_formated' => price_format($total_cost, false),
            'total_revenue' => $total_revenue,
            'total_revenue_formated' => price_format($total_revenue, false),
            'total_revenue_n_ds' => $total_revenue_n_ds,
            'total_revenue_n_ds_formated' => price_format($total_revenue_n_ds, false),
            'total_gpcost' => $total_gpcost,
            'total_profit' => $total_profit,
            'total_profit_formated' => price_format($total_profit, false),
            'profit_margin' => $total_revenue > 0 ? $total_profit / $total_revenue : 0,
            'profit_margin_formated' => number_format(($total_revenue > 0 ? $total_profit / $total_revenue : 0) * 100, 2, '.', '') . '%',
            'total_profit_n_ds' => $total_profit_n_ds,
            'total_profit_n_ds_formated' => price_format($total_profit_n_ds, false),
            'profit_margin_n_ds' => $total_revenue_n_ds > 0 ? $total_profit_n_ds / $total_revenue_n_ds : 0,
            'profit_margin_n_ds_formated' => number_format(($total_revenue_n_ds > 0 ? $total_profit_n_ds / $total_revenue_n_ds : 0) * 100, 2, '.', '') . '%',
            'total_sa_revenue' => $total_sa_revenue,
            'total_sa_revenue_formated' => price_format($total_sa_revenue, false),
            'total_sa_profit' => $total_sa_profit,
            'total_sa_profit_formated' => price_format($total_sa_profit, false),
            'total_sa_revenue_n_ds' => $total_sa_revenue_n_ds,
            'total_sa_revenue_n_ds_formated' => price_format($total_sa_revenue_n_ds, false),
            'total_sa_profit_n_ds' => $total_sa_profit_n_ds,
            'total_sa_profit_n_ds_formated' => price_format($total_sa_profit_n_ds, false),
            'total_fifo_cost' => $total_fifo_cost,
            'total_fifo_cost_formated' => price_format($total_fifo_cost, false),
            'total_dead_cost' => $total_dead_cost,
            'total_dead_cost_formated' => price_format($total_dead_cost, false),
            'sa_profit_margin' => $total_sa_revenue > 0 ? $total_sa_profit / $total_sa_revenue : 0,
            'sa_profit_margin_formated' => number_format(($total_sa_revenue > 0 ? $total_sa_profit / $total_sa_revenue : 0) * 100, 2, '.', '') . '%',
            'sa_profit_margin_n_ds' => $total_sa_revenue_n_ds > 0 ? $total_sa_profit_n_ds / $total_sa_revenue_n_ds : 0,
            'sa_profit_margin_n_ds_formated' => number_format(($total_sa_revenue_n_ds > 0 ? $total_sa_profit_n_ds / $total_sa_revenue_n_ds : 0) * 100, 2, '.', '') . '%',
            'avg_stock_cost' => $total_avg_cost,
            'avg_stock_cost_formated' => price_format($total_avg_cost, false),
            'display' => isset($_REQUEST['order_type']) ? $_REQUEST['order_type'] : 4
        ));
        foreach($data as $key => $value){
            $data[$key]['turnover'] = $data[$key]['total_sa_revenue'] / $data[$key]['avg_stock_cost'] !== false ? number_format($data[$key]['total_sa_revenue'] / $data[$key]['avg_stock_cost'],3) : "--";
        }
        $sql = 'SELECT sum(`discount` + `bonus` + `coupon`) ' .
            'FROM ' . $GLOBALS['ecs']->table('order_info') . ' AS oi ' .
            'LEFT JOIN ' . $GLOBALS['ecs']->table('order_coupons') . ' AS oc ON oc.order_id = oi.order_id ' .
            'LEFT JOIN ' . $GLOBALS['ecs']->table('coupons') . ' AS c ON c.coupon_id = oc.coupon_id ' .
            'WHERE '.$where;/*oi.shipping_time BETWEEN ' . $filter['start_date'] . ' AND ' . ($filter['end_date'] + 86399) . ' ' .
            order_query_sql('finished', 'oi.') .$where; // Exclude orders from 11111111, 88888888, 99999999*/
        $total_discount = $GLOBALS['db']->getOne($sql);
        $total_discount = price_format($total_discount, false);
        
        $arr = array(
            'data' => $data,
            'total_discount' => $total_discount,
            'filter' => $filter,
            'page_count' => $filter['page_count'],
            'record_count' => $filter['record_count']
        );
        return $arr;
    }
}
