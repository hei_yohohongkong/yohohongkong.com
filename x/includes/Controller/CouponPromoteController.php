<?php

/***
* TagController.php
* by Anthony 20170627
*
* Tag controller
***/

namespace Yoho\cms\Controller;
use Yoho\cms\Model;
if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

class CouponPromoteController extends YohoBaseController{


    private $tablename = 'coupon_promote';

	public function __construct(){
		parent::__construct();

	}


    /**
    * 获取标签数据列表
    * @access  public
    * @return  array
    */
    function get_coupon_promote_list()
    {
        $filter['sort_by']    = empty($_REQUEST['sort_by']) ? 't.tag_id' : trim($_REQUEST['sort_by']);
        $filter['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
        $filter['is_goods']   = $_REQUEST['is_show'] == 1;
        $filter['is_brand']   = $_REQUEST['is_show'] == 2;
        $filter['is_cat']     = $_REQUEST['is_show'] == 3;
        $filter['tag_words']  = empty($_REQUEST['tag_words']) ? '' : trim($_REQUEST['tag_words']);

        $sql = "SELECT COUNT(*) FROM ".$GLOBALS['ecs']->table('coupon_promote') ." where promote_type = 1 ";
        $filter['record_count'] = $GLOBALS['db']->getOne($sql);

        $filter = page_and_size($filter);

        $sql = "SELECT * FROM ".$GLOBALS['ecs']->table('coupon_promote') ." where promote_type = 1 ";
      
        $sql .= "ORDER by $filter[sort_by] $filter[sort_order] LIMIT ". $filter['start'] .", ". $filter['page_size'];

        $row = $GLOBALS['db']->getAll($sql);
        foreach($row as $k=>$v)
        {
            $row[$k]['config'] = json_decode($v['config']);
        }

        $arr = array(
            'promotes'     => $row,
            'filter'       => $filter,
            'page_count'   => $filter['page_count'],
            'record_count' => $filter['record_count']
            );

        return $arr;
    }

    /**
    * 取得标签的信息
    * return array
    */

    function get_coupon_promote_info($promote_id)
    {
        $sql = 'SELECT * FROM ' . $GLOBALS['ecs']->table('coupon_promote') . ' AS cp ' .
            " WHERE promote_id = '$promote_id'";
        $row = $GLOBALS['db']->getRow($sql);
        
        $row['config'] = json_decode($row['config'],true);
        
        return $row;
    }

    function insert_update_promote($post, $is_insert = false)
    {
        global $db, $ecs, $_CFG;

        $promote_type = 1;
        $promote_desc = $post['promote_desc'];
        $promote_name = $post['promote_name'];
        $promote_id = $post['id'];

        $bin_data = explode(PHP_EOL, $post['promote_bin']);
        $bin_format_pass = true;
        foreach ($bin_data as &$bin) {
            $bin = trim($bin);
            if (strlen($bin) != 6) {
                $bin_format_pass = false;
            }
            if (!preg_match( '/^[0-9]+$/', $bin)) {
                $bin_format_pass = false;
            }
        }

        if (!$bin_format_pass) {
            return false;
        }

        $config['card_bin'] = $bin_data;

        /* if null value, return false */
        //if(empty($goods_id) && empty($brand_id) && empty($cat_id) && empty($article_id)) return false;

        // handle simplified chinese
        $localizable_fields = [
            "promote_desc"  => $promote_desc,
        ];
        to_simplified_chinese($localizable_fields);
        
        if($is_insert)
        {
            $sql = "insert into ". $ecs->table("coupon_promote")." (promote_name,promote_desc,promote_type,config) 
                values ('".$promote_name."','".$promote_desc."','".$promote_type."','".json_encode($config)."')  ";
            $db->query($sql);
            $promote_id = $db->insert_id();
        }
        else
        {
            $localizable_fields = $this->checkRequireSimplied("coupon_promote", $localizable_fields, $promote_id);
            $sql = "update ".$ecs->table("coupon_promote")." set promote_name = '".$promote_name."', promote_desc = '".$promote_desc."', config = '".json_encode($config)."' where promote_id = ".$promote_id." ";
            $db->query($sql);
        }

        // Multiple language support
        save_localized_versions('coupon_promote', $promote_id);

        return true;
    }

    function get_tag_type()
    {
        global $_LANG;

        $type_list = array(
            1 => $_LANG['goods_id'],
            2 => $_LANG['brand'],
            3 => $_LANG['cat']
        );
        return $type_list;
    }

    /* cms goods page batch add tag function */
    function batch_add($goods_id, $tags)
    {
        if(empty($goods_id) || empty($tags)) return false;

        /* Format tag: string -> array */
        $tag_array = array();
        $tag_array = explode(",",$tags);

        /* Add tag */
        foreach ($tag_array as $key => $tag) {
            $sql = "INSERT INTO ".$this->ecs->table($this->tablename)."(goods_id, tag_words)".
                    " SELECT goods_id, '".trim($tag)."'".
                    " FROM ".$this->ecs->table('goods').
                    " WHERE goods_id ". \db_create_in($goods_id);

            $this->db->query($sql);
        }

        /* Delete duplicate row */
        $del_sql = "DELETE t1 FROM ".$this->ecs->table($this->tablename)." as t1, ".$this->ecs->table($this->tablename)." as t2 ".
                    " WHERE (t1.goods_id ". \db_create_in($goods_id). " AND t1.tag_id > t2.tag_id AND( t1.goods_id = t2.goods_id AND t1.tag_words = t2.tag_words)) OR t1.tag_words = '' ";
        $this->db->query($del_sql);

        clear_cache_files();
    }

    function get_tags_by_goods($goods_id)
    {
        if(empty($goods_id)) return false;

        $sql = "SELECT cat_id, brand_id FROM ".$this->ecs->table('goods')." WHERE goods_id = $goods_id LIMIT 1";
        $res = $this->db->getRow($sql);

        $cat_id = $res['cat_id'];
        $brand_id = $res['brand_id'];
        require_once(ROOT_PATH . 'includes/lib_goods.php');
        require_once(ROOT_PATH . 'includes/lib_howang.php');
        $cat = get_ly_newtree($cat_id);

        $cat_ids   = array();
        $cat_ids[] = $cat_id;
        foreach ($cat as $key => $value) {
            $cat_ids[] = $value['id'];
        }

        $sql = "SELECT DISTINCT t.tag_words FROM ".$this->ecs->table($this->tablename)." as t ".
                " LEFT JOIN ".$this->ecs->table('goods') ." as g ON g.goods_id = t.goods_id".
                " WHERE t.goods_id = ".$goods_id;
                if($brand_id >0)$sql .= " OR t.brand_id = $brand_id";
                if(count($cat_ids)>0)$sql.=" OR t.cat_id".\db_create_in($cat_ids);

        $res = $this->db->getCol($sql);

        $tags = array();
        foreach ($res as $key => $tag) {
            $tags[$key]['tag_url'] = "/keyword/".urlencode($tag);
            $tags[$key]['tag'] = $tag;
        }

        return $tags;
    }

    function find_tag_words($tag_words)
    {
        $sql = "SELECT COUNT(*) FROM ".$this->ecs->table($this->tablename)." WHERE tag_words = '".$tag_words."' ";
        $count = $this->db->getOne($sql);

        return $count;
    }

    function get_goods_id_by_tag($tag_words)
    {
        if(empty($tag_words)) return false;

        $sql = "SELECT DISTINCT goods_id, brand_id, cat_id  FROM ".$this->ecs->table($this->tablename).
                "WHERE tag_words = '".$tag_words."'";
        $arr = $this->db->getAll($sql);
        if(empty($arr)) return [];
        $goods_ids = array();
        $brand_ids = array();
        $cat_ids   = array();
        $all_cats  = array();
        foreach ($arr as $value)
        {
            if(!empty($value['goods_id'])) $goods_ids[] = $value['goods_id'];
            if(!empty($value['brand_id'])) $brand_ids[] = $value['brand_id'];
            if(!empty($value['cat_id']))   $cat_ids[]   = $value['cat_id'];
        }
        $where = [];
        foreach ($cat_ids as $c_id) $all_cats += array_unique(array_merge(array($c_id), array_keys(cat_list($c_id, 0, false))));
        if(!empty($goods_ids)) $where[] = " goods_id ".db_create_in($goods_ids);
        if(!empty($brand_ids)) $where[] = " brand_id ".db_create_in($brand_ids);
        if(!empty($all_cats)) $where[] = " cat_id ".db_create_in($all_cats);
        $where_sql = implode('OR', $where);
        if(empty($where_sql)) return [];
        $sql = "SELECT goods_id FROM ".$this->ecs->table('goods').
                " WHERE ( $where_sql ) AND is_delete = 0 AND is_on_sale = 1 AND is_alone_sale = 1 ";
        $goods_ids = $this->db->getCol($sql);

        return array_unique($goods_ids);

    }

    function tag_related_cat($goods_id, $tag_words)
    {
        $sql = "SELECT g.cat_id, IFNULL(cl.cat_name, c.cat_name) as child_cat_name,p.parent_id as p_id, IFNULL(pcl.cat_name, p.cat_name) as parent_cat_name, COUNT(g.goods_id) AS goods_count " .
                "FROM " . $GLOBALS['ecs']->table('goods') . " as g " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('category') . " as c ON c.cat_id = g.cat_id " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('category') . " as p ON c.parent_id  = p.cat_id AND p.is_show = 1 " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('category_lang') . " as cl ON cl.cat_id = c.cat_id AND cl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('category_lang') . " as pcl ON pcl.cat_id = p.cat_id AND pcl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
                "WHERE g.goods_id ".db_create_in($goods_id)." AND g.is_on_sale = 1 AND g.is_alone_sale = 1 AND g.is_delete = 0 " .
                "GROUP BY g.cat_id";
        $res = $GLOBALS['db']->getAll($sql);

        $array = array();
        foreach ($res as $key => $row) {
            $array[$row['parent_cat_name']][$key] = $row;
            $array[$row['parent_cat_name']][$key]['url'] = build_uri('tags', array(
                'cid' => $row['cat_id'], 'cname' => $row['child_cat_name'],
                'tag_words' => $tag_words), $row['child_cat_name']);
            $array[$row['parent_cat_name']][$key]['child_cat_name'] = $row['child_cat_name'];
            $array[$row['parent_cat_name']][$key]['parent_cat_name'] = $row['parent_cat_name'];
            $array[$row['parent_cat_name']]['id'] = $row['p_id'];
        }
        $result = array();
        $result[0] = $array;
        $result[1] = count($res);
        return $result;
    }

    function get_tag_words_list()
    {
        $sql = "SELECT DISTINCT tag_words FROM ".$this->ecs->table($this->tablename);
        $res = $this->db->getCol($sql);

        $list = array();
        foreach ($res as $value)
        {
            $list[$value] = $value;
        }
        return $list;
    }

    function update_tag_words($post)
    {
        $old_words = $post['tag_words'];
        $new_words = $post['new_tag_words'];

        if(empty($old_words) || empty($new_words)) return false;

        $sql = "UPDATE ".$this->ecs->table($this->tablename)." SET tag_words = '".$new_words."' ".
        "WHERE tag_words ".db_create_in($old_words);

        return $this->db->query($sql);
    }

}
