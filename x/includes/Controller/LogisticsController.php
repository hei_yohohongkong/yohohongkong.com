<?php

/***
* LogisticsController.php
* by Anthony 20171009
*
* Logistics controller
***/

namespace Yoho\cms\Controller;
use Yoho\cms\Model;
if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

class LogisticsController extends YohoBaseController{

    const ACTION_CODE_LOGISTICS = 'logistics';
    const LOGO_PATH             = '../' . DATA_DIR . "/afficheimg/logistics_logo/";
    const TRACKINGMORE_STATUS   = 
        [
        'pending',     // 查詢中
        'notfound',    // 查詢不到
        'transit',     // 運輸途中
        'pickup',      // 到達待取
        'delivered',   // 成功簽收
        'undelivered', // 投遞失敗
        'exception',   // 可能異常
        'expired',     // 運輸過久
        ];
	private $tablename='logistics';

    /*  Get logistics list */
    public function get_logistics_list($lang = null, $enabledOnly = true){
        global $db, $ecs, $_LANG;

        /* Query SQL */
        $logistics_list = array();
        $sql = "SELECT * FROM " . $ecs->table($this->tablename);
        if($enabledOnly)$sql .= " WHERE enabled = 1 ";
        $res = $db->getAll($sql);

        if (!empty($lang))
        {
            $res = localize_db_result_lang($lang, $this->tablename, $res);
        }
        foreach ($res as $key => $value) {
            $invoice_template = $value['invoice_template'];
            $length           = strlen($invoice_template);
            $first_word       = (strlen($invoice_template) > 2)?substr($invoice_template, 0, 2):'';
            $res[$key]['template_length'] = $length;
            $res[$key]['first_word']      = $first_word;

        }
        return $res;
    }

    public function submit_form_action($post){
        global $_LANG;
        $action       = $post['action'];
        /* logistics info */
        $logistics_code     = $post['logistics_code'];
        $logistics_name     = $post['logistics_name'];
        $logistics_id       = intval($post['logistics_id']);
        $logistics_desc     = isset($post['logistics_desc']) ? $post['logistics_desc'] : '';
        $logistics_url      = isset($post['logistics_url']) ? $post['logistics_url'] : '';
        $invoice_template   = isset($post['invoice_template']) ? $post['invoice_template'] : '';
        $enabled            = empty($post['enabled']) ? 0 : 1;
        if(!empty($post['logistics_logo_url'])){
            $logistics_logo = $post['logistics_logo_url'];
        } else{
            $logistics_logo = $this->upload_logo($_FILES, $logistics_id);
        }

        /* 检查输入 */
        if (empty($logistics_name) || empty($action))
        {
            sys_msg($_LANG['logistics_name'] . $_LANG['empty']);
        }

        $new_data = array(
            'logistics_name'   => $logistics_name,
            'logistics_desc'   => $logistics_desc,
            'logistics_code'   => $logistics_code,
            'logistics_url'    => $logistics_url,
            'invoice_template' => $invoice_template,
            'enabled'          => $enabled
        );
        if(isset($logistics_logo)) $new_data['logistics_logo'] = $logistics_logo;
        /* 检查是编辑还是安装 */

        // handle simplified chinese
        $localizable_fields = [
            "logistics_name"    => $new_data["logistics_name"],
            "logistics_desc"    => $new_data['logistics_desc'],
        ];
        if ($action == 'edit') {
            $localizable_fields = $this->checkRequireSimplied("logistics", $localizable_fields, $logistics_id);
        }
        to_simplified_chinese($localizable_fields);

        if ($action == 'edit')
        {
            /* 编辑 */
            $logistics = new Model\Logistics($logistics_id);
            if($logistics){

                $res = $logistics->update($new_data);
                if(!$res){
                    return false;
                }
                // Multiple language support
                save_localized_versions($this->tablename, $logistics_id);
            }
        }
        elseif ($action == 'insert')
        {
            $logistics = new Model\Logistics(null, $new_data);
            $logistics_id = $logistics->getId();
            if(!empty($logistics_id))
            {
                // Multiple language support
                save_localized_versions($this->tablename, $logistics_id);
            }
        }
        clear_cache_files();
        return true;
    }

    public function upload_logo($file, $logistics_id)
    {
        require_once(ROOT_PATH . 'includes/cls_image.php');
        $file_url = '';
        $allow_file_types = '|GIF|JPG|PNG|BMP';
        if ((isset($file['logistics_logo']['error']) && $file['logistics_logo']['error'] == 0) || (!isset($file['logistics_logo']['error']) && isset($file['logistics_logo']['tmp_name']) && $file['logistics_logo']['tmp_name'] != 'none'))
        {
            // 检查文件格式
            if (!check_file_type($file['logistics_logo']['tmp_name'], $file['logistics_logo']['name'], $allow_file_types))
            {
                sys_msg($_LANG['invalid_file']);
            }

            // 复制文件
            if (!make_dir(self::LOGO_PATH))
            {
                /* 创建目录失败 */
                $res =  false;
            }

            $filename = $logistics_id.'_logistics_logo' . substr($file['logistics_logo']['name'], strpos($file['logistics_logo']['name'], '.'));
            $path     = self::LOGO_PATH . $filename;
            if (move_upload_file($file['logistics_logo']['tmp_name'], $path))
            {
                $res =  $filename;
            }
            else
            {
                $res =  false;

            }
            if ($res != false)
            {
                $file_url = $path;
            }
        }
        if ($file_url == '')
        {
            $file_url = $_POST['file_url'];
        }
        return $file_url;
    }

    public function update_logistics_status($logistics_id, $enabled){

        global $db, $ecs, $_LANG;

        $sql = "UPDATE ".$ecs->table($this->tablename)." set enabled = $enabled WHERE logistics_id = $logistics_id ;";
        return $db->query($sql);
    }

    /*Trackingmore API function*/
    public function get_tracking_status($request){
        global $db, $ecs, $_LANG, $_CFG;

        // Remove whitespace
        $trackingNumber = urldecode($request['invoice']);
        $trackingNumber = str_replace(' ', '', $trackingNumber);
        $order_id = $request['order_id'];
        $key = $_CFG['trackingmore_api'];
        $lang = $_CFG['lang'] == "en_us" ? "en" : "cn";
        require_once(ROOT_PATH  . '/includes/lib_trackingmore.php');
        $track = new \Trackingmore($key);
        //Get invoice no detectCarrier
        $sql = "SELECT l.logistics_code, l.logistics_name, do.status FROM ".$ecs->table("delivery_order")." as do ".
                "LEFT JOIN ".$ecs->table("logistics")." as l ON do.logistics_id = l.logistics_id ".
                "WHERE do.order_id = '$order_id' AND do.invoice_no = '$trackingNumber' LIMIT 1 ";
        $logistics = $db->getRow($sql);
        $logistics_code = $logistics['logistics_code'];
        $logistics_name = $logistics['logistics_name'];
        
	
		// get delivery status 
		$sql = "select status, logistics_id, pickup_notified from ".$ecs->table("delivery_order")." where delivery_id = '".$request['delivery_id']."' ";
		$delivery = $db->getRow($sql);
		$delivery_status = $delivery['status'];
		//$shipping_id = $delivery['shipping_id'];
		$logistics_id = $delivery['logistics_id'];
        $pickup_notified = $delivery['pickup_notified'];

        if($logistics_code == 'remark'){
            return ['status'=>"--", 'delivery_status'=>$delivery_status, 'trackinfo'=>[], 'logistics_id'=>$logistics_id, 'logistics_name'=>$logistics_name, 'tracking_no' => $trackingNumber, 'pickup_notified' => $pickup_notified ];
        }

        if(empty($logistics_code)){
            $res = $track->detectCarrier($trackingNumber);
            $carrier = $res['data'][0]['code'];
        } else {
            $carrier = $logistics_code;
        }

        $extraInfo['order'] = $request['order_id'];
        $extraInfo['lang']  = $lang;
        // Check tracking info
        // $res = $track->getSingleTrackingResult($carrier, $trackingNumber, $extraInfo);
        // $result = $res['data'];
        $res = $track->getRealtimeTrackingResults($carrier, $trackingNumber, $extraInfo);
        $result = $res['data']['items'][0];

        extract($result);
	
        // Get tracking info
        $trackinfo = $destination_info['trackinfo'] ? $destination_info['trackinfo'] : $origin_info['trackinfo'];

        if(is_array($trackinfo) && count($trackinfo) > 0) {
            foreach ($trackinfo as $key => $value) {
                $statusDescription = $value['StatusDescription'];
                // IF have Simplified Chinese, change to Traditional Chinese
                $tmp = iconv('UTF-8', 'GB2312', $statusDescription);
                if($tmp){
                    $val = iconv('GB2312', 'BIG5', $tmp);
                    $statusDescription = iconv('BIG5', 'UTF-8',$val);
                }
                $trackinfo[$key]['StatusDescription'] = $statusDescription;
                $trackinfo[$key]['Details'] = empty($trackinfo[$key]['Details']) ? "--" : $trackinfo[$key]['Details'];
                if($key > 0){
                    $last_key = $last_key ? $last_key: $key-1;
                    if($trackinfo[$key]['StatusDescription'] == $trackinfo[$last_key]['StatusDescription']){
                        unset($trackinfo[$key]);
                    } else {
                        $last_key = $key;
                    }
                }
            }
        }

        return ['status'=>$status,'delivery_status'=>$delivery_status, 'logistics_id'=>$logistics_id, 'trackinfo'=>$trackinfo, 'logistics_name'=>$logistics_name, 'tracking_no' => $trackingNumber, 'pickup_notified' => $pickup_notified];

    }

    public function logistics_report(){
        global $db, $ecs;

        $filter['start_date'] = empty($_REQUEST['start_date']) ? strtotime(local_date('Y-m-01')) : strtotime($_REQUEST['start_date']);
        $filter['end_date'] = empty($_REQUEST['end_date']) ? strtotime(local_date('Y-m-t')) : strtotime($_REQUEST['end_date']);

        $logistics_list = $this->get_logistics_list(null, 0);
        $sql = "SELECT do.logistics_id, do.track_status, sub.avg_days as avg_days, count(do.delivery_id) as count, r.region_name as country_name, do.country ".
                " FROM ".$ecs->table('delivery_order')." as do ".
                " LEFT JOIN (SELECT ROUND(AVG((track_status_date - track_start_date )/86400),2)  as avg_days, track_status, logistics_id, country FROM ".$ecs->table('delivery_order')." as do WHERE do.track_start_date> 0 and do.track_status_date > 0 AND do.track_status ".db_create_in(self::TRACKINGMORE_STATUS).
                    " AND  do.add_time >= '".$filter['start_date']."' AND do.add_time < '" . ($filter['end_date'] + 86400)."' ".
                    " GROUP BY logistics_id, country, track_status) as sub on sub.track_status = do.track_status AND sub.logistics_id = do.logistics_id AND sub.country = do.country".
                " LEFT JOIN ".$ecs->table('region')." as r on r.region_id = do.country ".
                " WHERE do.track_status ".db_create_in(self::TRACKINGMORE_STATUS).
                 " AND  do.add_time >= '".$filter['start_date']."' AND do.add_time < '" . ($filter['end_date'] + 86400)."' ".
                " GROUP BY do.logistics_id, do.country, do.track_status";

        $res = $db->getAll($sql);
        foreach ($res as $key => $status) {
            $logistics_status[$status['logistics_id']][$status['country']][$status['track_status']]['avg_days'] = $status['avg_days'];
            $logistics_status[$status['logistics_id']][$status['country']][$status['track_status']]['count'] = $status['count'];
            $logistics_status[$status['logistics_id']][$status['country']]['country_name'] = $status['country_name'];
        }
        $n_logistics_list = [];

        foreach ($logistics_list as $key => $logistics) {
            $logistics['country_id'] = 0;
            foreach (self::TRACKINGMORE_STATUS as $string) {
                $logistics[$string]['avg_days'] = '--';
                $logistics[$string]['count'] = 0;
            }

            if(is_array($logistics_status[$logistics['logistics_id']])){
                // Create all country with logistics
                foreach ($logistics_status[$logistics['logistics_id']] as $country_id => $single_status) {
                    // Step 1: Update Name with region & Create New Key
                    $region_logistics = $logistics;
                    $region_logistics['logistics_name'] = $region_logistics['logistics_name']."($single_status[country_name])";
                    $key = $logistics['logistics_id']."_".$country_id;
                    $region_logistics['country_id'] = $country_id;
                    // Step 2: merge region logistics into array
                    $region_logistics = array_merge($region_logistics, $single_status);
                    $n_logistics_list[$key] = $region_logistics;
                }
            } else {
                $n_logistics_list[$logistics['logistics_id']] = $logistics;
            }
        }
        return ['data'=>$n_logistics_list,'filter'=>$filter];

    }
}