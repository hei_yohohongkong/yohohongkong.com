<?php

/***
* YOHO - FeedController
***/
namespace Yoho\cms\Controller;
if (!defined('IN_ECS')){
	die('Hacking attempt');
}

class FeedController extends YohoBaseController{

	private $products,$keyMap;

	// Ignore products in following category
	CONST IGNORE_CATEGORY = array('176', '154', '341');

	/**IGNORE_GOODS:(Violation of Shopping ads policy)
	 * Flevo Menthol 清涼薄荷補充彈 黑色 香港行貨
	 * Escort IX 隨插即用檢測器 香港行貨
	 * Beltronics GX998 車用GPS雷達測速器 香港行貨
	 * Flevo Starter Kit 電子煙 黑色 香港行貨
	 * Escort Passport Max 車用GPS雷達測速器 香港行貨
	 * Sea-Band 小童用防暈帶 一盒2條 綠色 香港行貨
	 * 窗口式冷氣機標準安裝 1.5 - 2匹
	 * Escort 9500ix Passport 車用GPS雷達測速器 香港行貨
	 * Beltronics V928i 車用雷達測速器 香港行貨
	 * 窗口式冷氣機標準安裝 3匹
	 * Escort X80-INT 隨插即用檢測器 香港行貨
	 * Flevo Vitamin Berry 維生素草莓口味補充彈 黑色 香港行貨
	 * TOUCHBeauty AS-0826B 按摩塑身美體刷 香港行貨
	 * Flevo Fruits Mix 雜果口味補充彈 白色 香港行貨
	 * Flevo Tobacco 純煙草味補充彈 黑色 香港行貨
	 * Flevo Menthol 清涼薄荷補充彈 白色 香港行貨
	 * Flevo Starter Kit 電子煙 白色 香港行貨
	 * Cobra DSP9200BT 電子狗 / 車用雷達探測器 香港行貨
	 * 窗口式冷氣機標準安裝 3/4 - 1匹
	 * Beltronics RX65i 車用雷達測速器 香港行貨
	 * Flevo Tobacco 純煙草味補充彈 白色 香港行貨
	 * 窗口式冷氣機標準安裝 2.5匹
	 * 掛牆分體式冷氣機標準安裝 1匹
	 * 掛牆分體式冷氣機標準安裝 1.5匹
	 * 掛牆分體式冷氣機標準安裝 2匹
	 * 掛牆分體式冷氣機標準安裝 2.5匹
	 * 掛牆分體式冷氣機標準安裝 3匹
	 * 窗口分體式冷氣機標準安裝 3/4匹-1匹
	 * 窗口分體式冷氣機標準安裝 1.5匹
	 * 窗口分體式冷氣機標準安裝 2匹
	 * 窗口分體式冷氣機標準安裝 2.5匹
	 **/
	CONST IGNORE_GOODS    = [18793,9230,18789,9222,18552,18786,18554,15302,18551,18782,18784,9229,9228,14426,15573,15301,18790,18792,9231,9233,18791,18553,20211,20212,20213,20214,20291,20292,20294,20295,20296,20297,20298,20299,20300];

	CONST PERMA_LINK_TABLES = [
		'goods' => [
			'pkey' 	=> 'goods_id',
			'col' 	=> 'goods_name',
		],
		'category' => [
			'pkey' 	=> 'cat_id',
			'col' 	=> 'cat_name',
		],
		'brand' => [
			'pkey' 	=> 'brand_id',
			'col' 	=> 'brand_name',
		],
		'article_cat' => [
			'pkey' 	=> 'cat_id',
			'col' 	=> 'cat_name',
		],
		'article' => [
			'pkey' 	=> 'article_id',
			'col' 	=> 'title',
		],
		'topic' => [
			'pkey' 	=> 'topic_id',
			'col' 	=> 'title',
		],
		'topic_category' => [
			'pkey' 	=> 'topic_cat_id',
			'col' 	=> 'topic_cat_name',
		],
	];

	public function __construct(){
		ini_set('memory_limit', '256M');
		parent::__construct();
		$this->products=[];
		$this->keyMap=[
			'id'=>'goods_id',
			'title'=>'goods_name',
			'description'=>'goods_brief',
			'link'=>'',
			'mobile_link'=>'',
			'image_link'=>'goods_img',
			'condition'=>'',
			'availability'=>'available',
			'availability_date' => 'availability_date',
			'price'=>'market_price',
			'sale_price'=>'shop_price',
			'shipping'=>[
				'country'=>'',
				'service'=>'',
				'price'=>'',
			],
			'gtin'=>'goods_sn',
			'brand'=>'brand_name',
			'mpn'=>'',
			'google_product_category'=>'',
			'product_type'=>'cat_name',
		];
	}

	public function addAllProduct(){
		require_once(ROOT_PATH . 'includes/lib_howang.php');
		require_once(ROOT_PATH . 'includes/lib_order.php');
		require_once(ROOT_PATH . 'includes/lib_main.php');
		require_once(ROOT_PATH . 'includes/lib_goods.php');
		$orderController = new OrderController();

		$onSaleWarehousIds = implode(',', $this->db->getCol("SELECT warehouse_id FROM ".$this->ecs->table('erp_warehouse')." WHERE is_valid=TRUE AND is_on_sale=TRUE "));
		$sql = "SELECT g.goods_id,g.goods_name,g.goods_sn,g.goods_brief,g.goods_img,g.market_price,g.shop_price,g.promote_price,g.promote_start_date,g.promote_end_date,
				g.is_purchasing, g.is_pre_sale, g.pre_sale_date, g.pre_sale_days, ".
                hw_goods_number_subquery('g.', 'real_goods_number') . ", " .
    			hw_reserved_number_subquery('g.') . ", " .
		" b.brand_name,c.cat_name,c.cat_id FROM ".$this->ecs->table('goods')." g ";

		$sql.= " LEFT JOIN ".$this->ecs->table('brand')." b ON g.brand_id=b.brand_id ";
		$sql.= " LEFT JOIN ".$this->ecs->table('category')." c ON g.cat_id=c.cat_id  ";
		$sql.= " WHERE g.is_delete=FALSE AND is_real=TRUE AND is_on_sale=TRUE ";
		$sql.= " AND g.goods_id NOT ".db_create_in(self::IGNORE_GOODS);
		$sql.= " AND g.market_price>0 AND g.shop_price>0";
		$sql.= " ORDER BY g.goods_id DESC";
		$products = $this->db->getAll($sql);

		$tmpCatTrees=[];
		//$products = goods_list(false);
		foreach($products as $key=>$pData){
			$product = [];
			// Quantity available for purchasing
			$pData['goods_number'] = intval($pData['real_goods_number']) - intval($pData['reserved_number']);
			// YOHO: limit quantity to 20 a time
			$pData['goods_number'] = min(intval($pData['goods_number']), 20);
			// Handle "in stock", "out of stock", "preorder"
			if ($pData['goods_number'] <= 0)
			{
				if ($pData['is_pre_sale']) // 預售
				{
					$pData['available'] = 'preorder';
					if($pData['pre_sale_date'] > 0) $pData['availability_date'] = local_date('Y-m-d', $pData['pre_sale_date']);
					elseif($pData['pre_sale_days'] > 0) $pData['availability_date'] = local_date('Y-m-d', $orderController->get_yoho_next_work_day(gmtime(), $pData['pre_sale_days']));
				}
				elseif ($pData['is_purchasing'] && $pData['pre_sale_days'] > 0) // 外國代購
				{
					$pData['available'] = 'preorder';
					$pData['availability_date'] = local_date('Y-m-d', $orderController->get_yoho_next_work_day(gmtime(), $pData['pre_sale_days']));
				}
				elseif ($pData['pre_sale_date'] > gmtime()) // 即將到貨
				{
					$pData['available'] = 'preorder';
					$pData['availability_date'] = local_date('Y-m-d', $pData['pre_sale_date']);
				}
				elseif ($pData['pre_sale_days'] > 0) // 訂貨
				{
					$pData['available'] = 'preorder';
					$pData['availability_date'] = local_date('Y-m-d', $orderController->get_yoho_next_work_day(gmtime(), $pData['pre_sale_days']));
				}
				else // 缺貨
				{
					// 缺貨不能購買
					$pData['available'] = 'out of stock';
				}
				// Check availability_date, If availability_date < today, remove and update to out of stock
				if($pData['available'] == 'preorder' && strtotime($pData['availability_date']) < strtotime(local_date("Y-m-d"))){
					// 缺貨不能購買
					$pData['available'] = 'out of stock';
					unset($pData['availability_date']);
				}
			} else {
				$pData['available'] = 'in stock';
			}

			foreach($this->keyMap as $gKey=>$yKey){
				if($yKey!=''){
					if($gKey=='image_link'){
						$product[$gKey]='<![CDATA['.absolute_url($pData[$yKey]).']]>';
					}elseif(in_array($gKey, ['price','sale_price'])){
						if($gKey=='sale_price' && $pData['promote_price']>0 && $pData['promote_start_date']>0 && $pData['promote_end_date']>0){
							$promotePrice = \bargain_price($pData['promote_price'], $pData['promote_start_date'], $pData['promote_end_date']);
							if($promotePrice>0){
								$pData[$yKey]=$promotePrice;
								$product['sale_price_effective_date']=local_date('c',$pData['promote_start_date']).'/'.local_date('c',$pData['promote_end_date']);
							}
							unset($promotePrice);
						}

						$product[$gKey]=$pData[$yKey]." HKD";
					}elseif(in_array($gKey, ['title','description'])){
						// Handle Facebook Pixel: 欄位含有的字母全部都是大楷: title, description
						$pData[$yKey] = $this->validate($pData[$yKey]);
						if($gKey=='description' && $pData[$yKey]==''){
							$pData[$yKey]="請到yohohongkong.com參考更多產品資料";
						}
						$product[$gKey]="<![CDATA[".strip_tags($pData[$yKey])."]]>";
					}elseif($gKey=='gtin'){
						//According to google GTIN format
						if($this->CheckGTIN($pData[$yKey]))
							$product[$gKey]=$pData[$yKey];
					}elseif($gKey=='product_type'){
						if(!array_key_exists($pData['cat_id'], $tmpCatTrees)){
							$tmplist = array();
							foreach(\get_parent_cats($pData['cat_id']) as $k=>$v){
								$tmplist[] = $v['cat_name'];
							}
							unset($tmplist[count($tmplist)-1]);
							$tmpCatTrees[$pData['cat_id']] = implode(' > ', $tmplist);
							unset($tmplist);
						}
						$product[$gKey]='<![CDATA['.$tmpCatTrees[$pData['cat_id']].']]>';
					}elseif($gKey=='brand'){
						$product[$gKey]='<![CDATA['.$pData[$yKey].']]>';
					}else{
						$product[$gKey]=$pData[$yKey];
					}
				}else{
					if($gKey=='link'){
						$perma = ($GLOBALS['_CFG']['perma_link_cache'] || !empty($_REQUEST['test_perma_link'])) ? $this->get_permanent_link_cache('goods', $pData['goods_id']) : '';
						$product[$gKey]=absolute_url('product/' . $pData['goods_id'] . '-' . (empty($perma) ? uri_encode($pData['goods_name']) : $perma)).'?lang=tc';
					}elseif($gKey=='condition'){
						$product[$gKey]='new';
					}
				}
			}
			$this->products[]=$product;
		}
	}

	public function buildFeed(){
        $error_log ="";
		$feed = '<?xml version="1.0" encoding="UTF-8"?>'."\n";
		$feed.= '<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">'."\n";
		$feed.= '<channel>'."\n";
		$feed.= "\t".'<title>友和 YOHO - O2O購物</title>'."\n";
		$feed.= "\t".'<link>https://www.yohohongkong.com/</link>'."\n";
		$feed.= "\t".'<description><![CDATA[電子產品及電器O2O商店 #智能手機 #美容及護理 #穿戴式裝置 #電腦文儀 #數碼影像 #電視影音 #家庭電器 #汽車用品]]></description>'."\n";
		foreach($this->products as $p){
            $item="\t\t"."<item>"."\n";
			foreach($p as $k=>$v) {
                $item.=$this->_processChild($k,$v);
            }
            $item.="\t\t"."</item>"."\n";
            libxml_use_internal_errors(true);
            
            $doc = simplexml_load_string($item);
            $xml = explode("\n", $item);
            if (!$doc === TRUE) {
                $error_log.="goods_id: ".$p['id']."\n";
                $errors = libxml_get_errors();
                foreach ($errors as $error) {
                    $error_log.= $error->message.",";
                }
                $error_log.= "\n";
                libxml_clear_errors();
            } else {
                $feed.=$item;
            }
			
		}
		if(!empty($error_log)) {
		    //TODO send task to Anthony.
            $now = gmtime();
            $next_work_day = $this->get_yoho_next_work_day($now);
            $task = array(
                'sender_id' => 1,
                'receiver_id' => 62,
                'sent_time' => $now,
                'read_time' => 0,
                'status' => '0',
                'priority' => "High",
                'dead_time' => $next_work_day,
                'estimated_time' => '480',
                'sender_name' => 'admin',
                'pic' => 'Anthony',
                'deleted' => 0,
                'title' => "google_merchant error log"
            );
            $message = $error_log;
            $task['message'] = mysql_escape_string($message);
            global $db, $ecs;
            $db->autoExecute($ecs->table('admin_taskes'), $task, 'INSERT');
            $task_id = $db->insert_id();
        }
		$feed.="</channel>"."\n";
		$feed.="</rss>";
		ob_flush();
		flush();
		$file = ROOT_PATH . 'google_merchant.xml';
		@chmod($file,0755);

		file_put_contents($file, $feed);

        return true;
	}

	private function _processChild($key,$value){
		return ($value==''?'':"\t\t\t"."<g:".$key.">".$value."</g:".$key.">"."\n");
	}

	/* This function is using to handle goods page add GA/FBpixel/ADwords code */
	public function goods_insert_code($goods){

		global $db, $ecs;

		$this->removeGtagCode();

		if(empty($goods) || strpos($_SERVER['SERVER_NAME'], 'beta.') !== false) return ['goods_insert_code'=>'','stats_code_insert'=>''];

		if ($goods['is_promote'] and $goods['gmt_end_time'] and $goods['promote_price'] < $goods['shop_price']){
			$goods_price = $goods['promote_price'];
		}else{
			$goods_price = $goods['shop_price'];
		}
		$cat_name = $db->getOne("SELECT cat_name FROM ".$ecs->table('category')." WHERE cat_id = $goods[cat_id]"); // We can get the chi category name
		
		// Facebook pixel
		$fb_pixel_code = "
			fbq('track', 'ViewContent', {
				content_name: '".$goods['goods_name']."',
				content_category: '".$cat_name."',
				content_ids: ['".$goods['goods_id']."'],
				content_type: 'product',
				value: $goods_price,
				currency: 'HKD'
			});";

		// Event remarketing page (Adwords)
		$gtag_insert_code = !in_array($goods['cat_id'], self::IGNORE_CATEGORY)
			? "gtag('event', 'page_view', {'send_to': 'AW-986254472',
				'ecomm_prodid': '".$goods['goods_id']."',
				'ecomm_pagetype': 'product',
				'ecomm_totalvalue': '".$goods_price."',
				'ecomm_category': '$cat_name'
			   });"
			: "";
		
		// GA ecommerce addProduct detail (In this case, need insert before "ga('send','pageview')")
		$ga_insert_code = "
			ga('require', 'ec');
			ga('ec:addProduct', {'id': '".$goods['goods_id']."','name': '".addslashes($g['goods_name'])."'});
			ga('ec:setAction', 'detail');
			";
		
		return ['extra_insert_code'=>$fb_pixel_code.$gtag_insert_code ,'stats_code_insert'=>$ga_insert_code]; // extra_insert_code is added on page_footer
	}

	/* This function is using to handle flow.php - checkout GA/FBpixel/ADwords code  */
	public function checkout_insert_code($goods_list){

		global $db, $ecs;

		$this->removeGtagCode();

		if(empty($goods_list)) return ['checkout_insert_code'=>'','stats_code_insert'=>''];

		$gaEcCheckout = "ga('require','ec');"."\n";

		$fbqInitCheckout = array(
			'content_ids' => array(),
			'value' => 0,
			'content_type' => 'product',
			'contents' => array(),
			'currency' => 'HKD'
		);

		$goods_ids = [];
		foreach($goods_list as $g){

			// Handle GA addProduct event
			$gaEcCheckout.="ga('ec:addProduct', {'id': '".$g['goods_id']."','name': '".addslashes($g['goods_name'])."','quantity': '".$g['goods_number']."'});"."\n";

			// Handle FBpixel InitiateCheckout event
			$fbqInitCheckout['contents'][] = array("id"=>$g["goods_id"],"name" => $g["goods_name"], "quantity" => $g["goods_number"], "price" => $g['goods_price']);
			$fbqInitCheckout['content_ids'][] = $g['goods_id'];
			$fbqInitCheckout['value'] += $g['goods_price'];

			// Handle Adwords ecomm_pagetype: 'cart' event
			if(!in_array($g['cat_id'], self::IGNORE_CATEGORY)){
				$goods_ids[] = $g['goods_id'];
			}
		}
		$goods_ids = implode(",", $goods_ids);
		$gaEcCheckout.="ga('ec:setAction','checkout', {'step': 2});"."\n";
		$fbPixelCheckout = 'fbq("track", "InitiateCheckout",' . json_encode($fbqInitCheckout) . ');'."\n";

		// Total value using with fbPixel total price.
		$gtagCheckout = "
			gtag('event', 'page_view', {
				ecomm_pagetype: 'cart',
				ecomm_prodid: [".$goods_ids."],
				ecomm_totalvalue: ".$fbqInitCheckout['value'].",
			});
		";
		return [
			'extra_insert_code' => $fbPixelCheckout.$gtagCheckout, // extra_insert_code is added on page_footer
			'stats_code_insert' => $gaEcCheckout
		];
	}

	/* This function is using to handle flow.php - done/ user.php - pay  GA/FBpixel/ADwords/Bing code  */
	public function purchase_insert_code($order, $goods_list){

		$this->removeGtagCode();

		// Ga: ecommerce purchase
		$gaEcTrans = "ga('require','ec');"."\n";
		$goods_ids = array();
		foreach($goods_list as $g){
			$gaEcTrans.="ga('ec:addProduct', {'id': '".$g['goods_id']."','name': '".addslashes($g['goods_name'])."','quantity': ".$g['goods_number']."});"."\n";
			$goods_ids[] = "'$g[goods_id]'";
			$contents[] = array("id"=>$g["goods_id"],"name" => $g["goods_name"], "quantity" => $g["goods_number"], "price" => $g['goods_price']);
		}
		$goods_ids = implode(",",$goods_ids);
		$gaEcTrans .= "ga('ec:setAction', 'purchase', {'id': '".$order['order_sn']."','affiliation': 'Yoho - Online Shop','revenue': '".$order['goods_amount']."','shipping': '".$order['shipping_fee']."'});";
		
		// Fbfixel: ecommerce purchase
		$fbPixelTrans = "fbq('track', 'Purchase', { content_ids: [ $goods_ids ],content_type: 'product',contents: ". json_encode($contents) . ",value: $order[goods_amount],currency: 'HKD'});"."\n";
			
		// Bing: ecommerce purchase
		$bingTrans = "window.uetq = window.uetq || [];"."\n";
		$bingTrans.="window.uetq.push({'ec':'product','ea':'purchase','ev':".$order['order_id'].",'gv':'".$order['goods_amount']."','gc':'HKD'});"."\n";

		// Adwords: conversion
		$adwordTrans = "
			<!-- Event snippet for Conversion-Yoho conversion page -->

			gtag('event', 'conversion', {
				'send_to': 'AW-986254472/9OYLCJHYt3kQiJmk1gM',
				'value': $order[goods_amount],
				'currency': 'HKD',
				'transaction_id': '$order[order_id]'
			});

		";

		// Adwords: page_view purchase
		$gtagPurchase = "
			gtag('event', 'page_view', {
				ecomm_pagetype: 'purchase',
				ecomm_totalvalue: ".$order['goods_amount'].",
			});
		";
		
		// Clickwise: sales/leads tracking
		$clickwiseTrack = strpos($_SERVER['SERVER_NAME'], 'beta.yohohongkong.com') === false
						? "PostAffTracker.setAccountId('755af38f');"."\n".
						  "var sale = PostAffTracker.createAction('beauty');"."\n".
						  "sale.setCampaignID('658202c1');"."\n".
						  "sale.setCurrency('HKD');"."\n".
						  "sale.setTotalCost('$order[order_amount]');"."\n".
						  "sale.setOrderID('$order[order_id]');"."\n".
						  "PostAffTracker.register();"
						: "";
		// orginal tag
		// <img src="https://my.pampanetwork.com/scripts/sale.php?AccountId=755af38f&CampaignID=658202c1&ActionCode=beauty&OrderID={$order.order_id}&TotalCost={$order.order_amount}&Currency=HKD" width="1" height="1"> 

		return [
			'extra_insert_code' => $fbPixelTrans.$bingTrans.$adwordTrans.$gtagPurchase.$clickwiseTrack, // extra_insert_code is added on page_footer
			'stats_code_insert' => $gaEcTrans
		];
	}

	function removeGtagCode(){
		global $smarty;
		$gtagCode = preg_replace("/gtag\('event','page_view',{(.*)}\);/","",$GLOBALS['_CFG']['stats_code']);
		$pvString="";
		preg_match("/ga(.*)\((.*)'send'(.*),(.*)'pageview'(.*)\);/",$gtagCode,$pvString);
		$pvString=@$pvString[0];
		$stats_code = substr($gtagCode,0,stripos($gtagCode,$pvString))."\n";
		$stats_code_pv = "\n".substr($gtagCode,stripos($gtagCode,$pvString));
		$smarty->assign('stats_code_pv', $stats_code_pv);
		$smarty->assign('stats_code', $stats_code);
	}

	function clear_permanent_link($table, $reset = 0)
	{
		if (!empty($reset)) {
			// Clear memcache
			$maxCacheKey = $this->genCacheKey(['permalink', $table, 'max']);
			$max_id = $this->memcache->get($maxCacheKey);
			$id = 0;
			while ($id <= $max_id) {
				$cacheKey = $this->genCacheKey(['permalink', $table, $id]);
				$this->memcache->delete($cacheKey);
				$id++;
			}
			$this->memcache->delete($maxCacheKey);

			// Clear database
			if ($reset == 2) {
				if (!empty($this->db->getOne("SHOW COLUMNS FROM " . $this->ecs->table($table) . " WHERE Field = 'perma_link'"))) {
					$this->db->query("ALTER TABLE " . $this->ecs->table($table) . " DROP `perma_link`");
				}
				if (!empty($this->db->getOne("SHOW COLUMNS FROM " . $this->ecs->table($table . "_lang") . " WHERE Field = 'perma_link'"))) {
					$this->db->query("ALTER TABLE " . $this->ecs->table($table . "_lang") . " DROP `perma_link`");
				}
			}
		}
	}

	function init_permanent_link($table, $id = 0, $reset = 0)
	{
		$cache = [];
		if (empty($id)) {
			ini_set('memory_limit', '512M');
			ini_set('max_execution_time', 1800);
			if (empty($reset)) {
				$cacheKey = $this->genCacheKey(['permalink', $table, 'max']);
				$max_id = $this->memcache->get($cacheKey);
				if (!$max_id) {
					$max_id = 0;
				}
				$id = 1;
				while ($id <= $max_id) {
					$cacheKey = $this->genCacheKey(['permalink', $table, $id]);
					$item = $this->memcache->get($cacheKey);
					if (!$item) {
						$cache[$id] = $item;
					}
					$id++;
				}
			}
			$id = 0;
		} else {
			$cacheKey = $this->genCacheKey(['permalink', $table, 'max']);
			$max_id = $this->memcache->get($cacheKey);
			if (!$max_id) {
				$max_id = 0;
			}
			$cacheKey = $this->genCacheKey(['permalink', $table, $id]);
			if (empty($reset)) {
				$cache[$id] = $this->memcache->get($cacheKey);
			}
		}
		$pkey = self::PERMA_LINK_TABLES[$table]['pkey'];
		$col = self::PERMA_LINK_TABLES[$table]['col'];
		$where = "";
		$default_language = default_language();
		$updated = false;

		if (!empty($id)) {
			$where = "WHERE $pkey = $id";
		}
		// Base table
		if (empty($this->db->getOne("SHOW COLUMNS FROM " . $this->ecs->table($table) . " WHERE Field = 'perma_link'"))) {
			$this->db->query("ALTER TABLE " . $this->ecs->table($table) . " ADD `perma_link` TEXT");
		}
		$count = $this->db->getOne("SELECT COUNT(*) FROM " . $this->ecs->table($table) . " $where");
		$start = 0;
		$limit = 10000;
		while ($start < $count) {
			$res = $this->db->getAll("SELECT $pkey, $col, perma_link FROM " . $this->ecs->table($table) . " $where LIMIT $start, $limit");
			foreach ($res as $row) {
				if (!empty(trim($row[$col])) && empty($cache[$row[$pkey]][$default_language])) {
					if (empty($row['perma_link'])) {
						$updated = true;
						$cache[$row[$pkey]][$default_language] = [
							'name' 	=> $row[$col],
							'link'	=> uri_encode($row[$col])
						];
						$this->db->query(
							"UPDATE " . $this->ecs->table($table) . " SET perma_link = '" . uri_encode($row[$col]) ."' " .
							"WHERE $pkey = $row[$pkey]"
						);
					} else {
						if ($cache[$row[$pkey]][$default_language]['name'] != $row[$col] || $cache[$row[$pkey]][$default_language]['link'] != $row['perma_link']) {
							$updated = true;
						}
						$cache[$row[$pkey]][$default_language] = [
							'name' 	=> $row[$col],
							'link'	=> $row['perma_link']
						];
					}
				}
			}
			$start += $limit;
		}

		// Language table
		if (empty($this->db->getOne("SHOW COLUMNS FROM " . $this->ecs->table($table . "_lang") . " WHERE Field = 'perma_link'"))) {
			$this->db->query("ALTER TABLE " . $this->ecs->table($table . "_lang") . " ADD `perma_link` TEXT");
		}
		$count = $this->db->getOne("SELECT COUNT(*) FROM " . $this->ecs->table($table . "_lang") . " $where");
		$start = 0;
		while ($start < $count) {
			$res = $this->db->getAll("SELECT $pkey, $col, lang, perma_link FROM " . $this->ecs->table($table . "_lang") . " $where LIMIT $start, $limit");
			foreach ($res as $row) {
				if (!empty(trim($row[$col])) && empty($cache[$row[$pkey]][$row['lang']])) {
					$updated = true;
					if (empty($row['perma_link'])) {
						$cache[$row[$pkey]][$row['lang']] = [
							'name' 	=> $row[$col],
							'link'	=> uri_encode($row[$col])
						];
						$this->db->query(
							"UPDATE " . $this->ecs->table($table . "_lang") . " SET perma_link = '" . uri_encode($row[$col]) ."' " .
							"WHERE $pkey = $row[$pkey] AND lang = '$row[lang]'"
						);
					} else {
						if ($cache[$row[$pkey]][$row['lang']]['name'] != $row[$col] || $cache[$row[$pkey]][$row['lang']]['link'] != $row['perma_link']) {
							$updated = true;
						}
						$cache[$row[$pkey]][$row['lang']] = [
							'name' 	=> $row[$col],
							'link'	=> $row['perma_link']
						];
					}
				}
			}
			$start += $limit;
		}
		$no_default_lang = [];
		foreach ($cache as $key => $data) {
			if (empty($data[$default_language])) {
				$no_default_lang[] = $key;
				continue;
			}
			foreach (available_languages() as $lang) {
				if ($lang != $default_language && empty($data[$lang])) {
					$updated = true;
					$translated = '';
					if ($lang == 'zh_cn') {
						$translated = translate_to_simplified_chinese($cache[$key][$default_language]['name']);
					}
					if ($translated != $cache[$key][$default_language]['name'] && $lang == 'zh_cn' && !empty($translated)) {
						$cache[$key][$lang]['name'] = $translated;
						$cache[$key][$lang]['link'] = uri_encode($translated);
					} else {
						$cache[$key][$lang] = $cache[$key][$default_language];
					}
				}
			}
		}
		foreach ($cache as $key => $data) {
			$cacheKey = $this->genCacheKey(['permalink', $table, $key]);
			if (in_array($key, $no_default_lang)) {
				$this->memcache->delete($cacheKey);
			} else {
				if ($reset) {
					$this->memcache->set($cacheKey, []);
				}
				if ($updated) {
					$this->memcache->set($cacheKey, $data);
				}
				if (empty($max_id) || intval($key) >= $max_id) {
					$max_id = intval($key);
				}
			}
		}
		if (!empty($max_id)) {
			$cacheKey = $this->genCacheKey(['permalink', $table, 'max']);
			$this->memcache->set($cacheKey, $max_id);
		}
	}

	function init_all_permanent_link($reset = 0)
	{
		foreach (array_keys(self::PERMA_LINK_TABLES) as $table) {
			$this->init_permanent_link($table, 0, $reset);
		}
	}

	function clear_all_permanent_link($reset = 0)
	{
		foreach (array_keys(self::PERMA_LINK_TABLES) as $table) {
			$this->clear_permanent_link($table, $reset);
		}
	}

	function init_permanent_link_table()
	{
		ini_set('memory_limit', '512M');
		ini_set('max_execution_time', 1800);
		$insert = 0;
		foreach (array_keys(self::PERMA_LINK_TABLES) as $table) {
			$cache = [];
			$max_id = $this->db->getOne("SELECT MAX(id) FROM " . $this->ecs->table('perma_link') . " WHERE `table_name` = '$table'");
			// Base table
			$pkey = self::PERMA_LINK_TABLES[$table]['pkey'];
			$col = self::PERMA_LINK_TABLES[$table]['col'];
			$default_language = default_language();
			$where = empty($max_id) ? "" : "WHERE $pkey > $max_id";
			$count = $this->db->getOne("SELECT COUNT(*) FROM " . $this->ecs->table($table) . " $where");
			$start = 0;
			$limit = 10000;
			while ($start < $count) {
				$res = $this->db->getAll("SELECT $pkey, $col, perma_link FROM " . $this->ecs->table($table) . " $where LIMIT $start, $limit");
				foreach ($res as $row) {
					if (!empty(trim($row[$col])) && empty($cache[$row[$pkey]][$default_language])) {
						$cache[$row[$pkey]][$default_language] = [
							'name' 	=> $row[$col],
							'link'	=> empty($row['perma_link']) ? uri_encode($row[$col]) : $row['perma_link']
						];
					}
				}
				$start += $limit;
			}

			// Language table
			$count = $this->db->getOne("SELECT COUNT(*) FROM " . $this->ecs->table($table . "_lang") . " $where");
			$start = 0;
			while ($start < $count) {
				$res = $this->db->getAll("SELECT $pkey, $col, lang, perma_link FROM " . $this->ecs->table($table . "_lang") . " $where LIMIT $start, $limit");
				foreach ($res as $row) {
					if (!empty(trim($row[$col])) && empty($cache[$row[$pkey]][$row['lang']])) {
						$cache[$row[$pkey]][$row['lang']] = [
							'name' 	=> $row[$col],
							'link'	=> empty($row['perma_link']) ? uri_encode($row[$col]) : $row['perma_link']
						];
					}
				}
				$start += $limit;
			}
			$no_default_lang = [];
			foreach ($cache as $key => $data) {
				if (empty($data[$default_language])) {
					$no_default_lang[] = $key;
					continue;
				}
				foreach (available_languages() as $lang) {
					if ($lang != $default_language && empty($data[$lang])) {
						$translated = '';
						if ($lang == 'zh_cn') {
							$translated = translate_to_simplified_chinese($cache[$key][$default_language]['name']);
						}
						if ($translated != $cache[$key][$default_language]['name'] && $lang == 'zh_cn' && !empty($translated)) {
							$cache[$key][$lang]['name'] = $translated;
							$cache[$key][$lang]['link'] = uri_encode($translated);
						} else {
							$cache[$key][$lang] = $cache[$key][$default_language];
						}
					}
				}
			}
			if ($time > 0) {
				sleep(5);
			}
			$time = 0;
			foreach ($cache as $id => $item) {
				if ((!empty($max_id) && intval($id) <= intval($max_id)) || in_array($id, $no_default_lang)) continue;
				foreach ($item as $lang => $data) {
					$time++;
					$insert++;
					$this->db->query("INSERT INTO " . $this->ecs->table('perma_link') . " (`table_name`, `id`, `lang`, `perma_link`) VALUES ('$table', '$id', '$lang', '$data[link]') ON DUPLICATE KEY UPDATE `perma_link` = '$data[link]', `last_update` = CURRENT_TIMESTAMP");
					if ($time == 500) {
						sleep(5);
						$time = 0;
					}
				}
			}
		}
		var_dump("Inserted: $insert");
	}
}