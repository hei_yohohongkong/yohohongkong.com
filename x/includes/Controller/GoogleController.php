<?php
/**
 * User: Anthony
 * Date: 2019/1/9
 * Time: 下午 04:55
 */

namespace Yoho\cms\Controller;
require_once(ROOT_PATH . 'includes/Google_api_client/vendor/autoload.php');

class GoogleController
{

    const VIEW_ID = "77620237";

    const DEFAULT_METRICS_LIST = [
        'ga_users' => 'ga:users',
        'ga_duration' => 'ga:avgSessionDuration',
        'ga_bouncerate' => 'ga:bounceRate',
        'ga_sessions' => 'ga:sessions',
        'ga_pagessession' => 'ga:pageviewsPerSession',
        'ga_pageviews' => 'ga:pageviews',
        'ga_uniquepageviews' => 'ga:uniquePageviews',
        'ga_overallconversion' => 'ga:transactions',
        'ga_overallconversionrate' => 'ga:transactionsPerSession',
        'pagePathLevel1' => [
            '/product/' => [
                'ga_productuniquepageviews' => 'ga:uniquePageviews'
            ],
            '/cart' => [
                'ga_cartuniquepageviews' => 'ga:uniquePageviews'
            ],
            '/checkout' => [
                'ga_orderuniquepageviews' => 'ga:uniquePageviews'
            ]
        ],
        'channelGrouping' => [
            'Organic Search' => [
                'ga_organicsearchconversion' => 'ga:transactions',
                'ga_Organicsearchconversionrate' => 'ga:transactionsPerSession'
            ],
            'Paid Search' => [
                'ga_paidsearchconversion' => 'ga:transactions',
                'ga_paidsearchconversionrate' => 'ga:transactionsPerSession'
            ],
            'Direct' => [
                'ga_directconversion' => 'ga:transactions',
                'ga_directconversionrate' => 'ga:transactionsPerSession'
            ],
            'Email' => [
                'ga_emailconversion' => 'ga:transactions',
                'ga_emailconversionrate' => 'ga:transactionsPerSession'
            ],

        ],
        'deviceCategory' => [
            'desktop' => [
                'ga_desktopconversionrate' => 'ga:transactionsPerSession',
            ],
            'mobile' => [
                'ga_mobileconversionrate' => 'ga:transactionsPerSession',
            ],
            'tablet' => [
                'ga_tabletconversionrate' => 'ga:transactionsPerSession',
            ]
        ]
    ];

    public function getClient()
    {

        $KEY_FILE_LOCATION = ROOT_PATH . 'data/yoho_service_account.json';
        $client = new \Google_Client();
        $client->setApplicationName("Yoho GA Report");
        $client->setAuthConfig($KEY_FILE_LOCATION);
        $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
        $client->useApplicationDefaultCredentials();
        $client->fetchAccessTokenWithAssertion();
        $token = $client->getAccessToken();

        return $client;
    }

    /**
     * Initializes an Analytics Reporting API V4 service object.
     *
     * @return \Google_Service_AnalyticsReporting authorized Analytics Reporting API V4 service object.
     */
    function initializeAnalytics()
    {
        $client = $this->getClient();
        $analytics = new \Google_Service_AnalyticsReporting($client);

        return $analytics;
    }

    /**
     * Queries the Analytics Reporting API V4.
     *
     * @param service An authorized Analytics Reporting API V4 service object.
     * @param array $metrics_list
     * @return array Analytics Reporting API V4 response.
     */
    function getGAReport($date, $metrics_list = [])
    {
        $analytics = $this->initializeAnalytics();
        $VIEW_ID = self::VIEW_ID;
        $start_date = empty($date['start_date']) ? local_date("Y-m-01") : date("Y-m-d", strtotime($date['start_date']));
        $end_date = empty($date['end_date']) ? local_date("Y-m-t") : date("Y-m-d", strtotime($date['end_date']));
        $result = [];
        // Create the DateRange object.
        $dateRange = new \Google_Service_AnalyticsReporting_DateRange();
        $dateRange->setStartDate($start_date);
        $dateRange->setEndDate($end_date);
        if (empty($metrics_list)) $metrics_list = self::DEFAULT_METRICS_LIST;
        $metrics = [];
        foreach ($metrics_list as $alias => $expression) {
            if (is_array($expression)) {
                foreach ($expression as $name => $dimensions) {
                    $sub_metrics = [];
                    foreach ($dimensions as $sub_alias => $expression) {
                        $tmp_metric = new \Google_Service_AnalyticsReporting_Metric();
                        $tmp_metric->setExpression($expression);
                        $tmp_metric->setAlias($sub_alias);
                        $sub_metrics[] = $tmp_metric;
                    }

                    //Create the Dimensions object.
                    $dimension = new \Google_Service_AnalyticsReporting_Dimension();
                    $dimension->setName("ga:" . $alias);

                    // Create Dimension Filter.
                    $dimensionFilter = new \Google_Service_AnalyticsReporting_SegmentDimensionFilter();
                    $dimensionFilter->setDimensionName("ga:" . $alias);
                    $dimensionFilter->setOperator("EXACT");
                    $dimensionFilter->setExpressions(array("$name"));
                    // Create the DimensionFilterClauses
                    $dimensionFilterClause = new \Google_Service_AnalyticsReporting_DimensionFilterClause();
                    $dimensionFilterClause->setFilters(array($dimensionFilter));

                    $request = new \Google_Service_AnalyticsReporting_ReportRequest();
                    $request->setViewId($VIEW_ID);
                    $request->setDateRanges($dateRange);
                    $request->setMetrics(array($sub_metrics));

                    $request->setDimensionFilterClauses(array($dimensionFilterClause));
                    $body = new \Google_Service_AnalyticsReporting_GetReportsRequest();
                    $body->setReportRequests(array($request));
                    try {
                        $aws = $analytics->reports->batchGet($body);
                        $result += $this->getGoogleResults($aws);
                    } catch (\Exception $e) {
                        echo 'Caught exception: ', $e->getMessage(), "\n";
                        exit();
                    }
                }
            } else {
                $tmp_metric = new \Google_Service_AnalyticsReporting_Metric();
                $tmp_metric->setExpression($expression);
                $tmp_metric->setAlias($alias);
                $metrics[] = $tmp_metric;
            }
        }
        // Create the ReportRequest object.
        $request = new \Google_Service_AnalyticsReporting_ReportRequest();
        $request->setViewId($VIEW_ID);
        $request->setDateRanges($dateRange);
        $request->setMetrics(array($metrics));

        $body = new \Google_Service_AnalyticsReporting_GetReportsRequest();
        $body->setReportRequests(array($request));
        try {
            $aws = $analytics->reports->batchGet($body);
            $result += $this->getGoogleResults($aws);
        } catch (\Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
            exit();
        }

        return $result;
    }

    /**
     * Parses and prints the Analytics Reporting API V4 response.
     *
     * @param An Analytics Reporting API V4 response.
     * @return array
     */
    function getGoogleResults($reports)
    {
        $res = [];
        for ($reportIndex = 0; $reportIndex < count($reports); $reportIndex++) {
            $report = $reports[$reportIndex];
            $header = $report->getColumnHeader();
            $dimensionHeaders = $header->getDimensions();
            $metricHeaders = $header->getMetricHeader()->getMetricHeaderEntries();
            $rows = $report->getData()->getRows();

            for ($rowIndex = 0; $rowIndex < count($rows); $rowIndex++) {
                $row = $rows[$rowIndex];
                $dimensions = $row->getDimensions();
                $metrics = $row->getMetrics();
                for ($i = 0; $i < count($dimensionHeaders) && $i < count($dimensions); $i++) {
                    $res[$dimensionHeaders[$i]] = $dimensions[$i];
                }

                for ($j = 0; $j < count($metrics); $j++) {
                    $values = $metrics[$j]->getValues();
                    for ($k = 0; $k < count($values); $k++) {
                        $entry = $metricHeaders[$k];
                        if ($entry->getType() == 'TIME') {
                            $t = round($values[$k]);
                            $values[$k] = sprintf('%02d:%02d:%02d', ($t / 3600), ($t / 60 % 60), $t % 60);
                        }
                        if ($entry->getType() == 'FLOAT') {
                            $values[$k] = number_format($values[$k], 2);
                        }
                        if ($entry->getType() == 'PERCENT') {
                            $values[$k] = number_format($values[$k], 2) . '%';
                        }
                        $res[$entry->getName()] = $values[$k];
                    }
                }
            }
        }
        return $res;
    }
}