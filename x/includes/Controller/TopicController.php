<?php

namespace Yoho\cms\Controller;
use Yoho\cms\Model;
if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

class TopicController extends YohoBaseController{


    public function get_index_promotion(){
        global $db, $ecs, $_CLG;

        $sql = "SELECT t.topic_id, IFNULL(tl.title, t.title) as title " .
        "FROM " . $ecs->table('topic') . " as t " .
        "LEFT JOIN " . $ecs->table('topic_lang') . " as tl ON tl.topic_id = t.topic_id AND tl.lang = '" . $_CFG['lang'] . "' " .
        "WHERE " . gmtime() . " BETWEEN t.start_time AND t.end_time + 86399 " .
        "AND t.topic_cat_id = 0 " .
        "ORDER BY t.sort_order ASC, t.topic_id DESC LIMIT 1";
        $promotion =  $db->getRow($sql);
        $promotion['url'] = build_uri('topic', array('tid' => $promotion['topic_id'], 'tname' => $promotion['title']), $promotion['title']);
		$promotion['title'] =  $promotion['title'];

        // $sql = "SELECT t.topic_id, IFNULL(tl.title, t.title) as title " .
        // "FROM " . $ecs->table('topic') . " as t " .
        // "LEFT JOIN " . $ecs->table('topic_lang') . " as tl ON tl.topic_id = t.topic_id AND tl.lang = '" . $_CFG['lang'] . "' " .
        // "WHERE " . gmtime() . " BETWEEN t.start_time AND t.end_time + 86399 " .
        // "AND t.topic_cat_id = 3 " .
        // "ORDER BY t.sort_order ASC, t.topic_id DESC LIMIT 1";
        // $featured =  $db->getRow($sql);
        // $featured['url'] = build_uri('topic', array('tid' => $featured['topic_id'], 'tname' => $featured['title']), $featured['title']);
		// $featured['title'] =  $featured['title'];

        $sql = "SELECT a.file_url,a.article_id, IFNULL(al.title, a.title) as title, a.sort_order " .
        "FROM " . $ecs->table('article') . " as a " .
        "LEFT JOIN " . $ecs->table('article_lang') . " as al ON al.article_id = a.article_id AND al.lang = '" . $_CFG['lang'] . "' " .
        "WHERE a.cat_id = 21 " .
        "ORDER BY a.sort_order ASC, a.article_id Desc LIMIT 1 , 1";
        $featured =  $db->getRow($sql);
        $featured['url'] = build_uri('article', array('aid' => $featured['article_id'], 'aname' => $featured['title']), $featured['title']);
        $featured['title'] =  $featured['title'];
        $featured['file_url'] =  $featured['file_url'];
        return ['promotion'=>$promotion,'featured'=>$featured];
    }

    /**
     * 获取专题列表
     * @access  public
     * @return void
     */
    function get_topic_list()
    {

        /* 查询条件 */
        $_REQUEST['sort_by']    = empty($_REQUEST['sort_by']) ? 'topic_id' : trim($_REQUEST['sort_by']);
        $_REQUEST['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);

        $sql = "SELECT COUNT(*) FROM ".$GLOBALS['ecs']->table('topic');
        $record_count = $GLOBALS['db']->getOne($sql);

        $sql = "SELECT t.*, tc.topic_cat_name " .
                "FROM " . $GLOBALS['ecs']->table('topic') . " as t " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('topic_category') . " as tc ON t.topic_cat_id = tc.topic_cat_id " .
                "ORDER BY redirection ASC, $_REQUEST[sort_by] $_REQUEST[sort_order]";

        $query = $GLOBALS['db']->selectLimit($sql, $_REQUEST['page_size'], $_REQUEST['start']);
        $res = array();

        while($topic = $GLOBALS['db']->fetch_array($query)){
            $topic['active']     = ((gmtime() >= $topic['start_time']) && (gmtime() <= $topic['end_time'] + 86399));
            $topic['start_time'] = local_date('Y-m-d',$topic['start_time']);
            $topic['end_time']   = local_date('Y-m-d',$topic['end_time']);
            $topic['url']        = build_uri('topic', array('tid' => $topic['topic_id']));
            $topic['_action']    = $this->generateCrudActionBtn($topic['topic_id'], 'id', null, null, ['view' => ['icon_class'=> 'fa fa-eye', 'link' => $topic['url'], 'target' => '_blank']]);
            $res[] = $topic;
        }

        // Multiple language support
        if (!empty($_REQUEST['edit_lang']))
        {
            $res = localize_db_result_lang($_REQUEST['edit_lang'], 'topic', $res);
            $res = localize_db_result_lang($_REQUEST['edit_lang'], 'topic_category', $res);
        }

        $arr = array('item' => $res, 'record_count' => $record_count);

        return $arr;
    }

    public function ajaxQueryAction($list, $totalCount, $action_btn = true)
	{
		// We're using dataTables Ajax to query.
		if(empty($list)) $list = $this->tableList;

		if(empty($totalCount)) $totalCount = $this->tableTotalCount;
		$draw = $_POST['draw'];
		$model = new Model\YohoBaseModel($this->tableName);
		$pk = $model->getPkey();
		if ($action_btn) {
			foreach($list as $key => $value) {
				$value['_action'] = $this->generateCrudActionBtn($value[$pk]);
				$list[$key] = $value;
			}
		}

		$info = [
			'draw'=> $draw,                 // ajax請求次數，作為標識符
			'recordsTotal'=>count($list),   // 獲取到的結果數(每頁顯示數量)
			'recordsFiltered'=>$totalCount, // 符合條件的總數據量
			'data'=>$list,                  // 獲取到的數據結果
		];
		//轉為json返回
		header('Content-Type: application/json');
		echo json_encode($info);
		exit();
    }
    //獲取某產品的相關專題
    public function getRelatedTopic($goods_id){
        $sql = "SELECT t.*, tperma FROM " . $GLOBALS['ecs']->table('topic') .  
        " t LEFT JOIN (SELECT id, table_name, lang, perma_link as tperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") acpl ON acpl.id = t.topic_id  AND acpl.table_name = 'topic' AND acpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
        "WHERE (" . gmtime() . " BETWEEN start_time AND end_time + 86399) " .
        "AND `data` LIKE '%|" . $goods_id . "\"%' " .
        "ORDER BY `sort_order` ASC,`topic_id` ASC";
        $topic_list = $GLOBALS['db']->getAll($sql);

        foreach ($topic_list as $key => $topic)
        {
            if (!file_exists(ROOT_PATH . DATA_DIR . '/afficheimg/topic/' . $topic['topic_id'] . '_icon.png'))
            {
            unset($topic_list[$key]);
            continue;
            }

            $topic_list[$key]['title'] = nl2br(trim($topic['title']));
            $topic_list[$key]['description'] = nl2br(trim($topic['description']));
            $topic_list[$key]['url'] = build_uri('topic', array('tid' => $topic['topic_id'],'tperma'=>$topic['tperma']));
            $topic_list[$key]['icon'] = DATA_DIR . '/afficheimg/topic/' . $topic['topic_id'] . '_icon.png';
        }
        return $topic_list;
    }
}