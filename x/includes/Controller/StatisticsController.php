<?php

namespace Yoho\cms\Controller;
use Yoho\cms\Model;
if (!defined('IN_ECS')) {
    die('Hacking attempt');
}

class StatisticsController extends YohoBaseController
{
    CONST GOOGLE_STATS_COLUMNS = [
        'session',
        'pageview',
        'pageview_unique',
        'landing_session',
        'landing_pageview',
        'landing_pageview_unique',
    ];
    CONST GOOGLE_STATS_WHERE_DAYS = [
        '7',
        '7to7',
        '30',
        '30to30',
    ];
    CONST GOOGLE_ANALYTICS_PROFILE_ID = 'ga:77620237';

    const TEAM_LIST = [
        'product' => [
            'role_name'=>'Product Team',
            'team'=>[
                'brand_sales' => '全部品牌-銷售總額',
                'brand_gp' => '全部品牌-盈利總額(GP)',
                'brand_avginventory' => '全部品牌-平均存貨成本',
                'brand_turnover' => '全部品牌-Turnover',
                'brand_d90' => '全部品牌-平均存貨成本(呆壞貨)',
                'brand_d90rate' => '全部品牌-呆壞貨存貨成本比率',
                'category_sales' => '分類-銷售總額',
                'category_gp' => '分類-盈利總額(GP)',
                'category_avginventory' => '分類-平均存貨成本',
                'category_turnover' => '分類-Turnover',
                'category_d90' => '分類-平均存貨成本(呆壞貨)',
                'category_d90rate' => '分類-呆壞貨存貨成本比率',
                'supplier_sales' => '全部供應商-銷售總額',
                'supplier_gp' => '全部供應商-盈利總額(GP)',
                'supplier_avginventory' => '全部供應商-平均存貨成本',
                'supplier_turnover' => '全部供應商-Turnover',
                'supplier_d90' => '全部供應商-平均存貨成本(呆壞貨)',
                'supplier_d90rate' => '全部供應商-呆壞貨存貨成本比率',
                'presale_avgdays' => '產品列表平均訂貨日數',
                'presale_delayrate' => '延誤出貨比率'
            ],
            'person'=>[
                'supplier_sales' => '負責供應商-銷售總額',
                'supplier_gp' => '負責供應商-盈利總額(GP)',
                'supplier_avginventory' => '負責供應商-平均存貨成本',
                'supplier_turnover' => '負責供應商-Turnover',
                'supplier_d90' => '負責供應商-平均存貨成本(呆壞貨)',
                'supplier_d90rate' => '負責供應商-呆壞貨存貨成本比率',
                'presale_delayrate' => '延誤出貨比率'
            ],
        ],
        'marketing' => [
            'role_name'=>'Marketing Team',
            'team'=>[
                'ga_users' => '[Google] Userviews(UV)',
                'ga_duration' => '[Google] Duration',
                'ga_bouncerate' => '[Google] 跳出率',
                'ga_sessions' => '[Google] Sessions',
                'ga_pagessession' => '[Google] Pages / Session',
                'ga_pageviews' => '[Google] Total Pageviews',
                'ga_uniquepageviews' => '[Google] Total Unique Pageviews',
                'ga_productuniquepageviews' => '[Google] Product Page Unique Pageviews',
                'ga_cartuniquepageviews' => '[Google] Cart Unique Pageviews',
                'ga_orderuniquepageviews' => '[Google] Order Unique Pageviews',
                'ga_overallconversion' => '[Google] Overall Conversion',
                'ga_overallconversionrate' => '[Google] Overall Conversion Rate',
                'ga_organicsearchconversion' => '[Google] Number of Organic Search Conversion',
                'ga_Organicsearchconversionrate' => '[Google] Organic Search Conversion Rate',
                'ga_paidsearchconversion' => '[Google] Number of Paid Search Conversion',
                'ga_paidsearchconversionrate' => '[Google] Paid Search Conversion Rate',
                'ga_directconversion' => '[Google] Number of Direct Conversion',
                'ga_directconversionrate' => '[Google] Direct Conversion Rate',
                'ga_emailconversion' => '[Google] Number of Email Conversion',
                'ga_emailconversionrate' => '[Google] Email Conversion Rate',
                'ga_mobileconversionrate' => '[Google] Mobile Conversion Rate',
                'ga_desktopconversionrate' => '[Google] Desktop Conversion Rate',
                'ga_tabletconversionrate' => '[Google] Tablet Conversion Rate',
                'edm_totalreach' => '[EDM] Total Reach',
                'edm_openrate' => '[EDM] Open Rate',
                'edm_clickrate' => '[EDM] Click Rate',
                'edm_clicktowebsite' => '[EDM] Clicks To Website',
                'youtube_newvideo' => '[EDM] New Videos',
                'youtube_viewsofnewvideos' => '[EDM] Views Of New Videos',
                'youtube_totalviewofallvideos' => '[EDM] Total Views Of All Videos',
                'youtube_clickstowebsite' => '[EDM] Clicks To Website',
                'facebook_totalfans' => '[Facebook] Total Fans',
                'facebook_organicreach' => '[Facebook] Organic Reach',
                'facebook_paidreach' => '[Facebook] Paid Reach',
                'facebook_spending' => '[Facebook] Spending',
                'facebook_click' => '[Facebook] Clicks To Website',
                'facebook_newvideo' => '[Facebook] New Video',
                'facebook_videorate' => '[Facebook] Video Viewthrough Rate',
                'facebook_videoreach' => '[Facebook] Video Reach',
                'facebook_videoclick' => '[Facebook] Video Post Clicks',
                'facebook_videocomment' => '[Facebook] Video Reactions, Comments & Shares',
                'facebook_sharevideo' => '[Facebook] Share Video',
                'facebook_sharevideoreach' => '[Facebook] Share Video Reach',
                'facebook_sharevideoclick' => '[Facebook] Share Video Post Clicks',
                'facebook_sharevideo_comment' => '[Facebook] Share Video Reactions, Comments & Shares',
                'facebook_photo' => '[Facebook] Photo',
                'facebook_photoreach' => '[Facebook] Photo Video Reach',
                'facebook_photoclick' => '[Facebook] Photo Video Post Clicks',
                'facebook_photocomment' => '[Facebook] Photo Video Reactions, Comments & Shares',
                'facebook_link' => '[Facebook] Link',
                'facebook_linkreach' => '[Facebook] Link Reach',
                'facebook_linkclick' => '[Facebook] Link Clicks',
                'facebook_linkcomment' => '[Facebook] Link Reactions, Comments & Shares'

            ],
            'person'=>[],
            'config'=>['upload_data'],
            'file' => '../' . DATA_DIR . '/target_excel/market_template.xlsx'
        ],
        'warehouse' => [
            'role_name'=>'Warehouse & Logistics Team',
            'team'=>[
                'wh_pototal' => '入貨單數目',
                'wh_poitemtotal' => '入貨量',
                'wh_deliveryordertotal' => '出貨總數',
                'whfile_wrongorder' => '錯誤出貨訂單數',
                'wh_avgorderofhead' => 'Orders Per Headcount (出貨單 / 人數)',
                'wh_inoffofhead' => 'In / Out Workload Per Headcount (出貨單+入貨單 / 人數)',
                'whfile_writeoffloss' => 'Write-Off Loss',
            ],
            'person'=>[
                'wh_pototal' => '入貨單數目',
                'wh_poitemtotal' => '入貨量'
            ],
            'config'=>['upload_data'],
            'file' => '../' . DATA_DIR . '/target_excel/warehouse_template.xlsx'
        ],
        'sale' => [
            'role_name'=>'Sales Team',
            'team'=>[
                'sale_revenue' => '總營業額',
                'sale_review' => '總平均評分',
                'sale_avgrevenueofhead' => 'Revenue Per Employee (營業額 / 人數)',
                'salefile_writeoffloss' => 'Write-Off Loss',
                'sale_emailregistrationrate' => '門市註冊有填電郵率',

            ],
            'person'=>[
                'sale_revenue' => '營業額',
                'sale_review' => '平均評分',
            ],
            'config'=>['upload_data'],
            'file' => '../' . DATA_DIR . '/target_excel/sale_template.xlsx'
        ],
        'cs' => [
            'role_name'=>'Customer Service Team',
            'team'=>[
                'cs_review' => '總平均評分',
                'csfile_googlereview' => 'Google 評分',
                'csfile_fbview' => 'Facebook 評分',
            ],
            'person'=>[
                'cs_review' => '總平均評分',
            ],
            'config'=>['upload_data'],
            'file' => '../' . DATA_DIR . '/target_excel/cs_template.xlsx'
        ],
        'corporate' => [
            'role_name'=>'Corporate Sales Team',
        ],
        'business' => [
            'role_name'=>'Business Development Team',
        ],
        'tech' => [
            'role_name'=>'Tech Team',
        ]
    ];

    const MONTH = [
        '1' => '一月',
        '2' => '二月',
        '3' => '三月',
        '4' => '四月',
        '5' => '五月',
        '6' => '六月',
        '7' => '七月',
        '8' => '八月',
        '9' => '九月',
        '10' => '十月',
        '11' => '十一月',
        '12' => '十二月',
        'Q1' => 'Q1',
        'Q2' => 'Q2',
        'Q3' => 'Q3',
        'Q4' => 'Q4'
    ];
    const GOAL_TYPE_CAT = [
        'team' => '部門',
        'person' => '個人'
    ];

    const TARGET_CONFIG = [
        'd90rate' => ['reverse'],
        'wrongorder' => ['no_chart', 'reverse'],
        'delayrate' => ['reverse'],
        'duration' => ['time'],
        'avgdays' => ['no_chart', 'no_reference']
    ];


    function getGoodsStatsColumns($index)
    {
        // change index to use other column, default: session
        $target = self::GOOGLE_STATS_COLUMNS[empty($index) ? 0 : $index];

        foreach (self::GOOGLE_STATS_WHERE_DAYS as $day) {
            $base[] = "SUM($target" . "_$day)" . " AS total_$target" . "_$day";
            $base[] = "SUM(sales_$day)" . " AS total_sales_$day";
            if (strpos($day, 'to') !== false) {
                $base[] = "SUM($target" . "_" . explode("to", $day)[0] . " - $target" . "_$day)" . " AS diff_total_$target" . "_$day";
                $extra[] = "($target" . "_" . explode("to", $day)[0] . " - $target" . "_$day)" . " AS diff_$target" . "_$day";
                $base[] = "SUM(sales_" . explode("to", $day)[0] . " - sales_$day)" . " AS diff_total_sales_$day";
                $extra[] = "(sales_" . explode("to", $day)[0] . " - sales_$day)" . " AS diff_sales_$day";
            }
        }
        $base[] = "gsl.$target" . " AS total_$target" . "_lifetime";
        $base[] = "gsl.sales" . " AS total_sales" . "_lifetime";
        $extra[] = "gsl.$target" . " AS $target" . "_lifetime";
        $extra[] = "gsl.sales" . " AS sales_lifetime";
        return [
            'target'    => $target,
            'base'      => $base,
            'extra'     => $extra,
        ];
    }
    
    function getGoogleStatsWhereConditions($og = false)
    {
        $yesterday      = local_strtotime("yesterday midnight");
        $sevendays      = local_strtotime("-7days midnight");
        $fourteendays   = local_strtotime("-14days midnight");
        $thirtydays     = local_strtotime("-30days midnight");
        $sixtydays      = local_strtotime("-60days midnight");

        if ($og) {
            return [
                '7'         => " AND shipping_time BETWEEN '$sevendays' AND '$yesterday'",
                '7to7'      => " AND shipping_time BETWEEN '$fourteendays' AND '" . ($sevendays - 1) . "'",
                '30'        => " AND shipping_time BETWEEN '$thirtydays' AND '$yesterday'",
                '30to30'    => " AND shipping_time BETWEEN '$sixtydays' AND '" . ($thirtydays - 1) . "'",
            ];
        }
        return [
            '7'         => " AND date BETWEEN '" . local_date("Y-m-d", $sevendays) . "' AND '" . local_date("Y-m-d", $yesterday) . "'",
            '7to7'      => " AND date BETWEEN '" . local_date("Y-m-d", $fourteendays) . "' AND '" . local_date("Y-m-d", $sevendays - 1) . "'",
            '30'        => " AND date BETWEEN '" . local_date("Y-m-d", $thirtydays) . "' AND '" . local_date("Y-m-d", $yesterday) . "'",
            '30to30'    => " AND date BETWEEN '" . local_date("Y-m-d", $sixtydays) . "' AND '" . local_date("Y-m-d", $thirtydays - 1) . "'",
        ];
    }

    function batchGetGoogleAnalyticData($service, $client, $params)
    {
        extract($params);
        $res = $service->data_ga->get(self::GOOGLE_ANALYTICS_PROFILE_ID, $start_day, $end_day, $metrics, $optParams);
        $total = $res->getTotalResults();
        $insert_data = $this->processGoogleStatsData($res->getRows());
        $success = true;
        $batched = 0;

        if ($total > $optParams['max-results']) {
            $client->setUseBatch(true);
            while ($optParams['start-index'] + $optParams['max-results'] < $total) {
                if ($batched > 0) {
                    sleep($batched);
                }
                $batch = new \Google_Http_Batch($client);
                $batched = 0;
                while ($optParams['start-index'] + $optParams['max-results'] < $total && $batched < 10) {
                    $optParams['start-index'] += $optParams['max-results'];
                    $req = $service->data_ga->get(self::GOOGLE_ANALYTICS_PROFILE_ID, $start_day, $end_day, $metrics, $optParams);
                    $batch->add($req, "result_" . $optParams['start-index']);
                    $batched++;
                }
                foreach ($batch->execute() as $batch_res) {
                    if (get_class($batch_res) !== "Google_Service_Analytics_GaData") {
                        $success = false;
                        break 2;
                    }
                    $insert_data = $this->processGoogleStatsData($batch_res->getRows(), $insert_data);
                }
            }
            $client->setUseBatch(false);
        }

        return [
            'success' => $success,
            'insert_data' => $insert_data,
            'batched' => $batched,
        ];
    }

    function processGoogleStatsData($rows, $insert_data = [])
    {
        foreach ($rows as $row) {
            $organic = false;
            $row_count = count($row);
            if (preg_match('/\/product\/(\d+)/i', $row[0], $match)) {
                $goods_id = intval($match[1]);
                if (preg_match('/\/product\/(\d+)/i', $row[1], $match)) {
                    $landing_goods_id = intval($match[1]);
                    if ($row[2] == 'organic' && $landing_goods_id == $goods_id) {
                        $organic = true;
                    }
                }
                if (!empty($goods_id)) {
                    if (!array_key_exists($goods_id, $insert_data)) {
                        $insert_data[$goods_id] = [
                            'session'           => intval($row[$row_count - 3]),
                            'pageview'          => intval($row[$row_count - 2]),
                            'pageview_unique'   => intval($row[$row_count - 1]),
                            'landing_session'           => $organic ? intval($row[$row_count - 3]) : 0,
                            'landing_pageview'          => $organic ? intval($row[$row_count - 2]) : 0,
                            'landing_pageview_unique'   => $organic ? intval($row[$row_count - 1]) : 0,
                        ];
                    } else {
                        $insert_data[$goods_id]['session']          += intval($row[$row_count - 3]);
                        $insert_data[$goods_id]['pageview']         += intval($row[$row_count - 2]);
                        $insert_data[$goods_id]['pageview_unique']  += intval($row[$row_count - 1]);
                        if ($organic) {
                            $insert_data[$goods_id]['landing_session']          += intval($row[$row_count - 3]);
                            $insert_data[$goods_id]['landing_pageview']         += intval($row[$row_count - 2]);
                            $insert_data[$goods_id]['landing_pageview_unique']  += intval($row[$row_count - 1]);
                        }
                    }
                }
            }
        }
        return $insert_data;
    }

    function getLastestGoodsStats()
    {
        global $db, $ecs;

        $sort_by = empty($_REQUEST['sort_by']) || $_REQUEST['sort_by'] == 'ranking' ? "session_7" : $_REQUEST['sort_by'];
        $sort_order = empty($_REQUEST['sort_order']) ? "asc" : $_REQUEST['sort_order'];
        $start = empty($_REQUEST['start']) ? 0 : intval($_REQUEST['start']);
        $page_size = empty($_REQUEST['page_size']) ? 50 : intval($_REQUEST['page_size']);

        $index = empty($_REQUEST['index']) ? 0 : intval($_REQUEST['index']);
        $columns = self::getGoodsStatsColumns($index);

        $sql = "SELECT gst.*, g.goods_name, " . implode(", ", $columns['extra']) . " FROM " . $ecs->table("google_statistics_temporary_view") . " gst " .
                "LEFT JOIN " . $ecs->table("google_statistics_lifetime") . " gsl ON gst.goods_id = gsl.goods_id " .
                "LEFT JOIN (SELECT goods_id, goods_name FROM " . $ecs->table("goods") . ") g ON g.goods_id = gst.goods_id " .
                "ORDER BY $sort_by $sort_order LIMIT $start, $page_size";

        $res = $db->getAll($sql);
        $rank = 1 + $start;
        foreach ($res as $key => $row) {
            $res[$key]['ranking'] = $rank;
            $rank++;
            $goods_ids[] = $row['goods_id'];
        }
        $erpController = new ErpController();
        $stocks = $erpController->getSumOfSellableProductsQtyByWarehouses($goods_ids);
        foreach ($res as $key => $row) {

            $res[$key]['stock'] = !empty($stocks[$row['goods_id']]) ? $stocks[$row['goods_id']] : 0;
        }
        $res = self::handleDataDiff($res);
        $count = $db->getOne("SELECT COUNT(*) FROM " . $ecs->table("google_statistics_temporary_view"));
        return [
            'data'  => $res,
            'count' => $count,
        ];
    }
    //function getLastestGoodsStats ,for goods generic sorting
    function getAllLastestGoodsStats()
    {
        global $db, $ecs;

        $sort_by = empty($_REQUEST['sort_by']) || $_REQUEST['sort_by'] == 'ranking' ? "session_7" : $_REQUEST['sort_by'];
        $sort_order = empty($_REQUEST['sort_order']) ? "desc" : $_REQUEST['sort_order'];

        $index = empty($_REQUEST['index']) ? 0 : intval($_REQUEST['index']);
        $columns = self::getGoodsStatsColumns($index);
        $count = $db->getOne("SELECT COUNT(*) FROM " . $ecs->table("google_statistics_temporary_view"));
        $size = 5000;
        $times = ceil($count/$size);
        $res = [];
        for($i=0; $i<$times; $i++) {
            $start = $size * $i;
            $sql = "SELECT gst.*, g.goods_name, " . implode(", ", $columns['extra']) . " FROM " . $ecs->table("google_statistics_temporary_view") . " gst " .
                "LEFT JOIN " . $ecs->table("google_statistics_lifetime") . " gsl ON gst.goods_id = gsl.goods_id " .
                "LEFT JOIN (SELECT goods_id, goods_name FROM " . $ecs->table("goods") . ") g ON g.goods_id = gst.goods_id " .
                "ORDER BY $sort_by $sort_order LIMIT $start, $size";
            $result = $db->getAll($sql);
            foreach ($result as $key => $row) {  
                array_push($res, $row);
            }  
        }  
        foreach ($res as $key => $row) {           
            $goods_ids[] = $row['goods_id'];
        }
        $erpController = new ErpController();
        $stocks = $erpController->getSumOfSellableProductsQtyByWarehouses($goods_ids);
        foreach ($res as $key => $row) {

            $res[$key]['stock'] = !empty($stocks[$row['goods_id']]) ? $stocks[$row['goods_id']] : 0;
        }
        $res = self::handleDataDiff($res);
        
        return [
            'data'  => $res,
            'count' => $count,
        ];
    }
    //function getAllLastestBrandsCatsStats is updated according to function getLastestBrandsCatsStats
    //function getLastestBrandsCatsStats is sorted by session_7 as default , and select top 20%, then calculate 7 day conversion rate
    //function getAllLastestBrandsCatsStats remove the restriction of order by session_7 and get all data, then  calculate 7 day conversion rate, then order by 7 day conversion rate, and select top 20% at last
    function getAllLastestBrandsCatsStats($type)
    {
        global $db, $ecs;
        $erpController = new ErpController();
        $sort_by = empty($_REQUEST['sort_by']) || $_REQUEST['sort_by'] == 'ranking' ? "session_7" : $_REQUEST['sort_by'];
        $index = empty($_REQUEST['index']) ? 0 : intval($_REQUEST['index']);
        $columns = self::getGoodsStatsColumns($index);
        
        // Select brand/category with more than 10 goods records (order by records count)
        if ($type == 'brand') {
            $sql = "SELECT gst.brand_id, b.brand_name, COUNT(DISTINCT gst.goods_id) as total, " . implode(", ", $columns['base']) . " FROM " . $ecs->table("google_statistics_temporary_view") . " gst " .
                    "LEFT JOIN " . $ecs->table("google_statistics_lifetime") . " gsl ON gst.goods_id = gsl.goods_id " .
                    "LEFT JOIN (SELECT brand_id, brand_name FROM " . $ecs->table("brand") . ") b ON b.brand_id = gst.brand_id " .
                    "GROUP BY gst.brand_id HAVING total > 10";
        } else {
            $cat_col = ($type == 'cat' ? "parent_" : "") . "cat_id";
            $cat_name = ($type == 'cat' ? "" : "all_") . "cat_name";
            $sql = "SELECT gst.$cat_col, c.cat_name as $cat_name, COUNT(DISTINCT gst.goods_id) as total, " . implode(", ", $columns['base']) . " FROM " . $ecs->table("google_statistics_temporary_view") . " gst " .
                    "LEFT JOIN " . $ecs->table("google_statistics_lifetime") . " gsl ON gst.goods_id = gsl.goods_id " .
                    "LEFT JOIN (SELECT cat_id, cat_name FROM " . $ecs->table("category") . ") c ON c.cat_id = gst.$cat_col " .
                    "GROUP BY gst.$cat_col HAVING total > 10";
        }

        $res = $db->getAll($sql);
        $top20_goods = [];
        $top5_goods = [];
        $over_avg_conversion = [];
        foreach (self::handleDataDiff($res) as $row) {
            $f_total = intval($row['total']);
            //  remove the restriction of order by session_7 and get all data, not just top 20%
            $size = 5000;
            $count = $db->getOne("SELECT COUNT(*) FROM " . $ecs->table("google_statistics_temporary_view"));
            $times = ceil($count/$size);
            $f_res = [];
            for($i=0; $i<$times; $i++) {
                $start = $size * $i;
                $f_sql = "SELECT gst.*, g.goods_name, " . implode(", ", $columns['extra']) . " FROM " . $ecs->table("google_statistics_temporary_view") . " gst " .
                        "LEFT JOIN " . $ecs->table("google_statistics_lifetime") . " gsl ON gst.goods_id = gsl.goods_id " .
                        "LEFT JOIN (SELECT goods_id, goods_name FROM " . $ecs->table("goods") . ") g ON g.goods_id = gst.goods_id " .
                        "WHERE " . ($type == 'brand' ? "brand_id = $row[brand_id] " : "$cat_col = $row[$cat_col] ") .
                        "LIMIT $start, $size";
                $result = $db->getAll($f_sql);
                foreach ($result as $key => $f_row) {  
                    array_push($f_res, $f_row);
                }  
            }  
            $final_res = [
                'total' => $f_total,
            ];
            foreach ($row as $name => $val) {
                if (strpos($name, 'total_') !== false) {
                    $final_res[$name] = strpos($name, 'perf_') !== false ? $val : intval($val);
                } elseif (preg_match('/(brand|cat)_(id|name)/', $name)) {
                    if (strpos($name, "_name") !== false && empty($val)) {
                        $final_res[$name] = '沒有' . ($type == 'brand' ? '品牌' : '分類');
                    } else {
                        $final_res[$name] = $val;
                    }
                }
            }
            foreach (self::GOOGLE_STATS_WHERE_DAYS as $day) {
                if (strpos($day, 'to') === false) {
                    $final_res['avg'][$columns['target'] . "_$day"] = round(intval($row["total" . "_$columns[target]" . "_$day"]) / $f_total, 2);
                    $final_res['avg']["sales_$day"] = round(intval($row["total_sales_$day"]) / $f_total, 2);
                }
            }
            $final_res['detail'] = self::handleDataDiff($f_res);
            $total_length = count($final_res['detail']);
            // var_dump($final_res['detail'] );
            //generic sorting part 2 —— select top 20% at last
            $day7_conversion_arr = array_column($final_res['detail'],"perf_sales_7to7_percentage");  // order by 7 day conversion rate
            array_multisort($day7_conversion_arr, SORT_DESC, $final_res['detail']);
            $top20 = floor($total_length * 20 / 100);
            for($i=0; $i<$top20; $i++) {
                array_push($top20_goods,$final_res['detail'][$i]["goods_id"]);
            }              
            //熱銷 —— 得到(7天转化率+同比上周转化率)/2，前5%的商品 
            $avg_day7_conversion_arr = array_column($final_res['detail'],"avg_sales_7to7_percentage");
            array_multisort($avg_day7_conversion_arr, SORT_DESC, $final_res['detail']);
            $top5 = floor($total_length * 5 / 100);
            for($i=0; $i<$total_length; $i++) {
                $goods_stock = $erpController->getSellableProductQty($final_res['detail'][$i]["goods_id"]);
                if($goods_stock!=0) {
                    array_push($top5_goods,$final_res['detail'][$i]["goods_id"]);
                    if(count($top5_goods) >= $top5 )  break; 
                }             
            }  
            //推介 —— 每個分類下7天表現值從低到高排序，其中轉化率perf_sales_7to7_percentage高於該分類下所有商品7天平均轉化率perf_total_sales_7to7_percentage,所給標籤數量為該分類下商品的10%
            $perf_total_sales_7to7_percentage = $final_res['perf_total_sales_7to7_percentage'];
            $session7_arr = array_column($final_res['detail'],"session_7");
            array_multisort($session7_arr, SORT_ASC, $final_res['detail']);       
            $top10_length = floor($total_length / 10);
            for($i=0; $i<$total_length; $i++) {
                $goods_stock = $erpController->getSellableProductQty($final_res['detail'][$i]["goods_id"]);
                if($goods_stock!=0 && $final_res['detail'][$i]['perf_sales_7to7_percentage'] > $perf_total_sales_7to7_percentage){
                    array_push($over_avg_conversion,$final_res['detail'][$i]["goods_id"]);
                    if(count($over_avg_conversion) >= $top10_length )  break; 
                }             
            }  
        }
        return array($top20_goods,$top5_goods,$over_avg_conversion);
    }

    function getLastestBrandsCatsStats($type)
    {
        global $db, $ecs;

        $sort_by = empty($_REQUEST['sort_by']) || $_REQUEST['sort_by'] == 'ranking' ? "session_7" : $_REQUEST['sort_by'];
        $index = empty($_REQUEST['index']) ? 0 : intval($_REQUEST['index']);
        $columns = self::getGoodsStatsColumns($index);

        // Select brand/category with more than 10 goods records (order by records count)
        if ($type == 'brand') {
            $sql = "SELECT gst.brand_id, b.brand_name, COUNT(DISTINCT gst.goods_id) as total, " . implode(", ", $columns['base']) . " FROM " . $ecs->table("google_statistics_temporary_view") . " gst " .
                    "LEFT JOIN " . $ecs->table("google_statistics_lifetime") . " gsl ON gst.goods_id = gsl.goods_id " .
                    "LEFT JOIN (SELECT brand_id, brand_name FROM " . $ecs->table("brand") . ") b ON b.brand_id = gst.brand_id " .
                    "GROUP BY gst.brand_id HAVING total > 10 ORDER BY total_$sort_by DESC";
        } else {
            $cat_col = ($type == 'cat' ? "parent_" : "") . "cat_id";
            $cat_name = ($type == 'cat' ? "" : "all_") . "cat_name";
            $sql = "SELECT gst.$cat_col, c.cat_name as $cat_name, COUNT(DISTINCT gst.goods_id) as total, " . implode(", ", $columns['base']) . " FROM " . $ecs->table("google_statistics_temporary_view") . " gst " .
                    "LEFT JOIN " . $ecs->table("google_statistics_lifetime") . " gsl ON gst.goods_id = gsl.goods_id " .
                    "LEFT JOIN (SELECT cat_id, cat_name FROM " . $ecs->table("category") . ") c ON c.cat_id = gst.$cat_col " .
                    "GROUP BY gst.$cat_col HAVING total > 10 ORDER BY total_$sort_by DESC";
        }

        $res = $db->getAll($sql);
        foreach (self::handleDataDiff($res) as $row) {
            $f_total = intval($row['total']);
            // Select top 20% goods in brand (max 10)/ level 1 category(max 50) / level 3 category(max 10)
            $f_sql = "SELECT gst.*, g.goods_name, " . implode(", ", $columns['extra']) . " FROM " . $ecs->table("google_statistics_temporary_view") . " gst " .
                        "LEFT JOIN " . $ecs->table("google_statistics_lifetime") . " gsl ON gst.goods_id = gsl.goods_id " .
                        "LEFT JOIN (SELECT goods_id, goods_name FROM " . $ecs->table("goods") . ") g ON g.goods_id = gst.goods_id " .
                        "WHERE " . ($type == 'brand' ? "brand_id = $row[brand_id] " : "$cat_col = $row[$cat_col] ") .
                        "ORDER BY $sort_by DESC LIMIT " . min(max(floor($f_total * 20 / 100), 1), ($type != 'cat' ? 10 : 50));
            $f_res = $db->getAll($f_sql);
            $final_res = [
                'total' => $f_total,
            ];
            foreach ($row as $name => $val) {
                if (strpos($name, 'total_') !== false) {
                    $final_res[$name] = strpos($name, 'perf_') !== false ? $val : intval($val);
                } elseif (preg_match('/(brand|cat)_(id|name)/', $name)) {
                    if (strpos($name, "_name") !== false && empty($val)) {
                        $final_res[$name] = '沒有' . ($type == 'brand' ? '品牌' : '分類');
                    } else {
                        $final_res[$name] = $val;
                    }
                }
            }
            foreach (self::GOOGLE_STATS_WHERE_DAYS as $day) {
                if (strpos($day, 'to') === false) {
                    $final_res['avg'][$columns['target'] . "_$day"] = round(intval($row["total" . "_$columns[target]" . "_$day"]) / $f_total, 2);
                    $final_res['avg']["sales_$day"] = round(intval($row["total_sales_$day"]) / $f_total, 2);
                }
            }
            $final_res['detail'] = self::handleDataDiff($f_res);
            foreach ($f_res as $goods) {
                $goods_ids[] = $goods['goods_id'];
            }
            $final_result[] = $final_res;
        }

        $erpController = new ErpController();
        $stocks = $erpController->getSumOfSellableProductsQtyByWarehouses($goods_ids);
        foreach ($final_result as $key => $row) {
            foreach ($row['detail'] as $key2 => $goods) {
                $final_result[$key]['detail'][$key2]['stock'] = !empty($stocks[$goods['goods_id']]) ? $stocks[$goods['goods_id']] : 0;
            }
        }
        return $final_result;
    }

    function handleDataDiff($res)
    {
        $index = empty($_REQUEST['index']) ? 0 : intval($_REQUEST['index']);
        $columns = self::getGoodsStatsColumns($index);
        foreach ($res as $key => $row) {
            foreach ($row as $name => $val) {
                if (preg_match('/^(?!diff_)([A-z_]+_[0-9]+)to[0-9]+/', $name, $current)) {
                    $percent = round(intval($row["diff_$name"]) / intval($val) * 100, 2);
                    $col_left = 5;
                    $col_right = 7;
                    $con_prev = "";
                    $con_curr = "";
                    $con_curr_percent_value = "";
                    if (strpos($current[1], "sales") !== false) {
                        $con_prev = intval($val) / intval($row[str_replace("sales", $columns['target'], $name)]);
                        $con_curr = intval($row[$current[1]]) / intval($row[str_replace("sales", $columns['target'], $current[1])]);
                        $res[$key]["prev_perf_$name".'_percentage'] = ($con_prev === false ? (intval($val) > 0 ? '∞' : '0') : (round($con_prev * 100, 2)));
                        $res[$key]["perf_$name".'_percentage'] = ($con_curr === false ? (intval($row[$current[1]]) > 0 ? '∞' : '0') : (round($con_curr * 100, 2)));  
                        if($res[$key]["prev_perf_$name".'_percentage'] == '∞') {
                            $res[$key]["prev_perf_$name".'_percentage'] = 0;
                        }
                        if($res[$key]["perf_$name".'_percentage'] == '∞') {
                            $res[$key]["perf_$name".'_percentage'] = 0;
                        }
                        //(7天转化率+同比上周转化率)/2
                        $res[$key]["avg_$name".'_percentage'] = ($res[$key]["prev_perf_$name".'_percentage'] + $res[$key]["perf_$name".'_percentage']) /2;  
                        $con_prev = "(" . ($con_prev === false ? (intval($val) > 0 ? '∞' : '0%') : (round($con_prev * 100, 2) . '%')) . ")";
                        $con_curr = "(" . ($con_curr === false ? (intval($row[$current[1]]) > 0 ? '∞' : '0%') : (round($con_curr * 100, 2) . '%')) . ")";
                        $con_curr_percent_value = round($con_curr * 100, 2);
                        $col_left = 7;
                        $col_right = 5;
                    }
                    if ($percent > 0) {
                        $span = "<span title='Previous period: $val $con_prev' class='green'>▲$percent%</span>";
                    } elseif ($percent < 0) {
                        $percent = abs($percent);
                        $span = "<span title='Previous period: $val $con_prev' class='red'>▼$percent%</span>";
                    } else {
                        $span = "<span title='Previous period: $val $con_prev'>--</span>";
                    }
                    if (strpos($name, "total") !== false) {
                        $res[$key]["perf_$name"] = "<div class='stat_sum'>" . $row[$current[1]] . " $con_curr<br>($span)</div>";
                    } else {
                        
                        $res[$key]["perf_$name"] = "<div class='col-md-$col_left' style='text-align: right'>" . $row[$current[1]] . " $con_curr</div><div class='col-md-$col_right' style='text-align: left'>($span)</div>";
                    }
                    
                    $res[$key]["perf_$name".'_value'] = $row[$current[1]] ;
                    $res[$key]["perf_$name".'_percent'] = $con_curr_percent_value;

                } elseif (preg_match('/[A-z_]+lifetime$/', $name, $current)) {
                    if (strpos($name, "sales") !== false) {
                        $sales = intval($val);
                        $count = intval($row[str_replace("sales", $columns['target'], $name)]);
                        $percent = round($sales / $count * 100, 2);
                        $res[$key]["perf_$name"] = "$val ($percent%)";
                        $res[$key]["perf_$name".'_value'] = $val;
                        $res[$key]["perf_$name".'_percent'] = $percent;
                    } else {
                        $res[$key]["perf_$name"] = "$val";
                    }
                }
            }
        }
        return $res;
    }

    function getCronSql()
    {
        require_once ROOT_PATH . 'includes/lib_order.php';

        $columns = [];
        $tables = [];
        $conds = self::getGoogleStatsWhereConditions();
        $ogConds = self::getGoogleStatsWhereConditions(true);
        $i = 0;
        foreach (self::GOOGLE_STATS_WHERE_DAYS as $day) {
            $i++;
            $sub_columns = [];
            foreach (self::GOOGLE_STATS_COLUMNS as $col) {
                $columns[] = $col . "_$day";
                $fields[] = "IFNULL($col" . "_$day, 0) AS $col" . "_$day";
                $sub_columns[] = "SUM($col" . ") AS $col" . "_$day";
            }
            $fields[] = "IFNULL(sales_$day, 0) AS sales_$day";
            $tables[] = "LEFT JOIN (SELECT goods_id, " . implode(", ", $sub_columns) . " FROM " . $GLOBALS['ecs']->table("google_statistics") . " WHERE 1" . $conds[$day] . " GROUP BY goods_id) AS gs$i ON gs$i.goods_id = g.goods_id";
            $tables[] = "LEFT JOIN (SELECT goods_id, SUM(goods_number) as sales_$day FROM " . $GLOBALS['ecs']->table("order_goods") . " og LEFT JOIN " . $GLOBALS['ecs']->table("order_info") . " oi ON oi.order_id = og.order_id WHERE 1" . order_query_sql('finished', 'oi.') . $ogConds[$day] . " GROUP BY goods_id) AS ogs$i ON ogs$i.goods_id = g.goods_id";
        }

        $sql = "INSERT INTO " . $GLOBALS['ecs']->table("google_statistics_temporary_view") .
                "SELECT g.goods_id, g.cat_id, g.cat_id, g.brand_id, " .
                implode(", ", $fields) .
                " FROM " . $GLOBALS['ecs']->table("goods") . " AS g " .
                implode(" ", $tables) .
                " HAVING " . implode(" + ", $columns) . " > 0";;
        return $sql;
    }

    function menuAction()
    {
        global $ecs, $db, $smarty;
        $smarty->assign('ur_here', 'KPI輔助');
        if ($_SESSION['manage_cost'] || $_SESSION['role_id'] == 4) { //super admin, role_id = tech team, using for testing.
            $title_list = self::TEAM_LIST;
            $smarty->assign('action_link', array('text' => '新增目標', 'href' => 'goal_setting.php?act=add'));
        } else {
            $title_list = [];
            $role_id = $_SESSION['role_id'];
            $team_code = $db->getOne('SELECT team_code FROM '.$ecs->table('role') ." WHERE role_id = $role_id ");
            $title_list[$team_code] = self::TEAM_LIST[$team_code];
        }
        $year = $this->getNextYear(10, 1);
        $smarty->assign('year', $year);
        $smarty->assign('month', self::MONTH);
        $smarty->assign('now_year', local_date('Y'));
        $smarty->assign('now_month', local_date('m'));
        $smarty->assign('title_list', $title_list);
        assign_query_info();
        $smarty->display('goal_setting.htm');
    }

    function getFormNextStep($data) {
        global $db, $ecs;
        $name  = $data['name'];
        $value = $data['value'];
        $extra = $data['extra'] ? $data['extra'] : [];
        $result = [];
        switch ($name) {
            case 'goal_type':
                if($value == 'team') {
                    $result = ['next'=> 'team'];
                    make_json_result($result);
                } elseif($value == 'person') {
                    if($extra['parent_id'] > 0) {
                        $sql = "SELECT * FROM ".$ecs->table('goals')." WHERE goal_id = ".$extra['parent_id'];
                        $res = $db->getRow($sql);
                        $new_data = [];
                        $new_data['name'] = 'team_code';
                        $new_data['value'] = $res['team_code'];
                        $this->getFormNextStep($new_data, $extra);
                    } else {
                        $result = ['next'=> 'team'];
                        make_json_result($result);
                    }
                }
                break;
            case 'team_code':
                $keyword = (self::TEAM_LIST[$value]['role_name']) ? self::TEAM_LIST[$value]['role_name'] : 'team';
                $sql = "SELECT role_id FROM ".$ecs->table('role')." WHERE role_name LIKE '%".$keyword."%' OR team_code = '$value' GROUP BY role_id ";
                $data = $db->getCol($sql);
                if(empty($data)) {
                    $sql = "SELECT role_id FROM ".$ecs->table('role')." ";
                    $data = $db->getAll($sql);
                }

                $sql = "SELECT * FROM ".$ecs->table('admin_user')." WHERE role_id ". db_create_in($data)." AND is_disable = 0 ";
                $data = $db->getAll($sql);
                if(empty($data)) {
                    $sql = "SELECT * FROM ".$ecs->table('admin_user')." WHERE is_disable = 0 ";
                    $data = $db->getAll($sql);
                }
                $select_data = [];
                foreach($data as $v) {
                    $select_data[$v['user_id']] = $v['user_name'];
                }
                $result = ['next'=> 'admin', 'data'=>$select_data];
                make_json_result($result);
                break;
            case 'role_id':
                $result = ['next'=> 'admin'];
                $data = [];
                if($value > 0) {
                    $sql = "SELECT * FROM ".$ecs->table('admin_user')." WHERE role_id ". db_create_in($value)." AND is_disable = 0 ";
                    $data = $db->getAll($sql);
                }
                if(empty($data)) {
                    $sql = "SELECT * FROM ".$ecs->table('admin_user')." WHERE is_disable = 0 ";
                    $data = $db->getAll($sql);
                }
                $select_data = [];
                foreach($data as $v) {
                    $select_data[$v['user_id']] = $v['user_name'];
                }
                $result = ['next'=> 'admin', 'data'=>$select_data];
                make_json_result($result);
                break;
            case 'admin_id':
                if($extra['parent_id'] > 0) {
                    $sql = "SELECT * FROM ".$ecs->table('goals')." WHERE goal_id = ".$extra['parent_id'];
                    $res = $db->getRow($sql);
                    $extra['team_code'] = $res['team_code'];
                    $data = self::TEAM_LIST[$extra['team_code']][$extra['goal_type']];
                    $result = ['next'=> 'target', 'data'=>$data];
                    make_json_result($result);
                } else {
                    $result = ['next'=> 'date'];
                    make_json_result($result);
                }

                break;
            case 'month':
                if($extra['parent_id'] > 0) {
                    $sql = "SELECT * FROM ".$ecs->table('goals')." WHERE goal_id = ".$extra['parent_id'];
                    $res = $db->getRow($sql);
                    $extra['team_code'] = $res['team_code'];
                }
                $data = self::TEAM_LIST[$extra['team_code']][$extra['goal_type']];
                $result = ['next'=> 'target', 'data'=>$data];
                make_json_result($result);
                break;
            case 'data_key':
                $array = explode('_', $value);
                $key = $array[0];
                if($key == 'category') {
                    $cat_list = '<option value="-1">請選擇</option>'.'<option value="0">所有分類</option>'.cat_list();
                    $result = ['next'=> 'extra_list', 'show'=>['extra_list'=>$cat_list]];
                } else {
                    if($extra['parent_id'] > 0) {
                        $sql = "SELECT * FROM ".$ecs->table('goals')." WHERE goal_id = ".$extra['parent_id'];
                        $res = $db->getRow($sql);
                        $extra['year']  = $res['year'];
                        $extra['month'] = $res['month'];
                    }
                    $data = $this->getTargetData($value, $extra);
                    $result = ['next'=>'submit-group', 'show'=> $data];
                }

                make_json_result($result);
                break;
            case 'extra_id':
                if($extra['parent_id'] > 0) {
                    $sql = "SELECT * FROM ".$ecs->table('goals')." WHERE goal_id = ".$extra['parent_id'];
                    $res = $db->getRow($sql);
                    $extra['year']  = $res['year'];
                    $extra['month'] = $res['month'];
                }
                $data = $this->getTargetData($extra['data_key'], $extra);
                $result = ['next'=>'submit-group', 'show'=> $data];

                make_json_result($result);
                break;
            default:
                break;
        }
    }

    function getTargetData($type, $data, $timing = 'last')
    {
        global $ecs, $db;
        $array = explode('_', $type);
        $today = local_strtotime('today');
        $key = $array[0];
        $value = $array[1];
        $date_list = [];

        static $targetData = [];
        $date_list['last_year']  = $this->getQarter($data['year'], $data['month'], 'year');
        $date_list['last_month']  = $this->getQarter($data['year'], $data['month'], 'month');
        if($timing != 'last') {
            $date_list['now']  = $this->getQarter($data['year'], $data['month']);
        }
        $result = [];
        foreach($date_list as $time => $date) {
            $start_ts = local_strtotime($date['start_date']);
            $end_ts = local_strtotime($date['end_date']);

            // Check that user date is between start & end
            $on_month  = (($today >= $start_ts) && ($today <= $end_ts));
            if($key == 'supplier') {
                $supplierController = new SupplierController();
                $admin = 0;
                if( $data['goal_type'] == 'person') {
                    $admin = $data['admin_id'];
                }
                $_REQUEST['order_type'] = '4';
                $_REQUEST['deprecatedflow'] = 'N';
                $_REQUEST['start_date'] = $date['start_date'];
                $_REQUEST['end_date'] = $date['end_date'];
                $static_cache_name = $date['start_date'].'-'.$date['end_date'].'supplier'.$admin;
                if($time == 'now' && $on_month) {
                    $static_cache_name = $static_cache_name.$today;
                }
                $sale_supplier = $this->memcache->get($static_cache_name);
                if($sale_supplier === false) {
                    $sale_supplier = $supplierController->get_sale_supplier(0, $admin);
                    $this->memcache->set($static_cache_name, $sale_supplier);
                }
                switch($value) {
                    case 'sales':
                        $result[$time] = $sale_supplier['data']['all']['total_sa_revenue_formated'];
                        break;
                    case 'gp':
                        $result[$time] = $sale_supplier['data']['all']['total_sa_profit_formated'];
                        break;
                    case 'avginventory':
                        $result[$time] = $sale_supplier['data']['all']['avg_stock_cost_formated'];
                        break;
                    case 'turnover':
                        $result[$time] = $sale_supplier['data']['all']['turnover'];
                        break;
                    case 'd90':
                        $result[$time] = $sale_supplier['data']['all']['total_dead_cost_formated'];
                        break;
                    case 'd90rate':
                        $result[$time] = round(($sale_supplier['data']['all']['total_dead_cost']/$sale_supplier['data']['all']['avg_stock_cost']) * 100).'%';
                        break;
                    default:
                        break;
                }
            }
            elseif($key == 'brand') {
                $brandController = new BrandController();
                $_REQUEST['order_type'] = '4';
                $_REQUEST['deprecatedflow'] = 'N';
                $_REQUEST['start_date'] = $date['start_date'];
                $_REQUEST['end_date'] = $date['end_date'];
                $static_cache_name = $date['start_date'].'-'.$date['end_date'].'brand';
                if($time == 'now' && $on_month) {
                    $static_cache_name = $static_cache_name.$today;
                }
                $sale_brand = $this->memcache->get($static_cache_name);
                if($sale_brand === false) {
                    $sale_brand = $brandController->get_sale_brand(0);
                    $this->memcache->set($static_cache_name, $sale_brand);
                }
                switch($value) {
                    case 'sales':
                        $result[$time] = $sale_brand['data'][0]['total_sa_revenue_formated'];
                        break;
                    case 'gp':
                        $result[$time] = $sale_brand['data'][0]['total_sa_profit_formated'];
                        break;
                    case 'avginventory':
                        $result[$time] = $sale_brand['data'][0]['avg_stock_cost_formated'];
                        break;
                    case 'turnover':
                        $result[$time] = $sale_brand['data'][0]['turnover'];
                        break;
                    case 'd90':
                        $result[$time] = $sale_brand['data'][0]['total_dead_cost_formated'];
                        break;
                    case 'd90rate':
                        $result[$time] = round(($sale_brand['data'][0]['total_dead_cost']/$sale_brand['data'][0]['avg_stock_cost']) * 100).'%';
                        break;
                    default:
                        break;
                }
            }
            elseif($key == 'category') {
                $categoryController = new CategoryController();
                $_REQUEST['order_type'] = '4';
                $_REQUEST['deprecatedflow'] = 'N';
                $_REQUEST['start_date'] = $date['start_date'];
                $_REQUEST['end_date'] = $date['end_date'];
                $static_cache_name = $date['start_date'].'-'.$date['end_date'].'category';
                if($time == 'now' && $on_month) {
                    $static_cache_name = $static_cache_name.$today;
                }
                $sale_category = $this->memcache->get($static_cache_name);
                if($sale_category === false) {
                    $sale_category = $categoryController->get_stock_cost(0);
                    $this->memcache->set($static_cache_name, $sale_category);
                }
                if($data['extra_id'] > 0) {
                    switch ($value) {
                        case 'sales':
                            $result[$time] = $sale_category['ungroup'][$data['extra_id']]['total_sa_revenue_formated'];
                            break;
                        case 'gp':
                            $result[$time] = $sale_category['ungroup'][$data['extra_id']]['total_sa_profit_formated'];
                            break;
                        case 'avginventory':
                            $result[$time] = $sale_category['ungroup'][$data['extra_id']]['avg_stock_cost_formated'];
                            break;
                        case 'turnover':
                            $result[$time] = $sale_category['ungroup'][$data['extra_id']]['turnover'];
                            break;
                        case 'd90':
                            $result[$time] = $sale_category['ungroup'][$data['extra_id']]['total_dead_cost_formated'];
                            break;
                        case 'd90rate':
                            $result[$time] = round(($sale_category['ungroup'][$data['extra_id']]['total_dead_cost']/$sale_category['ungroup'][$data['extra_id']]['avg_stock_cost']) * 100).'%';
                            break;
                        default:
                            break;
                    };
                } else {
                    switch ($value) {
                        case 'sales':
                            $result[$time] = $sale_category['stock_cost_data'][0]['total_sa_revenue_formated'];
                            break;
                        case 'gp':
                            $result[$time] = $sale_category['stock_cost_data'][0]['total_sa_profit_formated'];
                            break;
                        case 'avginventory':
                            $result[$time] = $sale_category['stock_cost_data'][0]['avg_stock_cost_formated'];
                            break;
                        case 'turnover':
                            $result[$time] = $sale_category['stock_cost_data'][0]['turnover'];
                            break;
                        case 'd90':
                            $result[$time] = $sale_category['stock_cost_data'][0]['total_dead_cost_formated'];
                            break;
                        case 'd90rate':
                            $result[$time] = round(($sale_category['stock_cost_data'][0]['total_dead_cost']/$sale_category['stock_cost_data'][0]['avg_stock_cost']) * 100).'%';
                            break;
                        default:
                            break;
                    };
                }
            }
            elseif($key == 'presale') {
                switch ($value) {
                    case 'avgdays':
                        if ($time == 'now') {
                            $goodsController = new GoodsController();
                            $res = $goodsController->getGoodsPreSaleDaysAll();
                            $result['now'] = $days = round(($res['total_day']) / ($res['total_product']), 1);
                        }
                        break;
                    case 'delayrate':
                        $deliveryOrderController = new DeliveryOrderController();
                        $res = $deliveryOrderController->countAdminDelayShip($date);
                        if( $data['goal_type'] == 'person') {
                            $admin = $data['admin_id'];
                        } else {
                            $admin = 'all';
                        }
                        $result[$time] = $res[$admin]['delay_rate'];
                        break;
                    default:
                        break;
                };
            }
            elseif($key == 'ga') {
                $static_cache_name = $date['start_date'].'-'.$date['end_date'].'ga';
                if($time == 'now' && $on_month) {
                    $static_cache_name = $static_cache_name.$today;
                }
                $gaReport = $this->memcache->get($static_cache_name);

                if($gaReport === false) {
                    /* Try to find in our datebase */
                    $sql = "SELECT data FROM ".$ecs->table('target_log')." WHERE platform = 'google' AND start_time = $start_ts AND end_time = $end_ts ";
                    $res = $db->getOne($sql);
                    if(!empty($res)) $gaReport = unserialize($res);
                    else {
                        $googleController = new GoogleController();
                        $gaReport = $googleController->getGAReport($date);

                        if(!$on_month) {
                            $sql = "INSERT INTO ".$ecs->table('target_log')." (platform, data, year, month, start_time, end_time) ".
                                "VALUES ('google', '".serialize($gaReport)."', ".$date['year'].", ".$date['month'].", $start_ts, $end_ts)";
                            $db->query($sql, 'SILENT');
                        }
                    }

                    $this->memcache->set($static_cache_name, $gaReport);
                }
                $result[$time] = $gaReport[$type];
            }
            elseif($key == 'wh') {
                $static_cache_name = $date['start_date'].'-'.$date['end_date'].'wh'.$value.$data['goal_type'];
                if($time == 'now' && $on_month) {
                    $static_cache_name = $static_cache_name.$today;
                }
                $report = $this->memcache->get($static_cache_name);
                switch ($value) {
                    case 'deliveryordertotal':
                        if($report === false) {
                            $_REQUEST['start_date'] = $date['start_date'];
                            $_REQUEST['end_date'] = $date['end_date'];
                            $deliveryOrderController = new DeliveryOrderController();
                            $report = $deliveryOrderController->getOrderGoodsCrossCheckList(false);
                            $this->memcache->set($static_cache_name, $report);
                        }
                        $all_orders_total = 0;
                        foreach ($report['data'] as $v) {
                            $all_orders_total += $v['all_orders_total'];
                        }
                        $result[$time] = $all_orders_total;
                        break;
                    case 'pototal':
                        if($report === false) {
                            $erpController = new ErpController();
                            $report = $erpController->incoming_warehousing_report(['start_date' => $date['start_date'], 'end_date' => $date['end_date']], 1);
                            $this->memcache->set($static_cache_name, $report);
                        }
                        if( $data['goal_type'] == 'person') {
                            $admin = $data['admin_id'];
                        } else {
                            $admin = 'all';
                        }
                        $result[$time] = $report[$admin]['total_order'];
                        break;
                    case 'poitemtotal':
                        if($report === false) {
                            $_REQUEST['start_date'] = $date['start_date'];
                            $_REQUEST['end_date'] = $date['end_date'];
                            $erpController = new ErpController();
                            $report = $erpController->incoming_warehousing_report(['start_date' => $date['start_date'], 'end_date' => $date['end_date']], 1);
                            $this->memcache->set($static_cache_name, $report);
                        }
                        if( $data['goal_type'] == 'person') {
                            $admin = $data['admin_id'];
                        } else {
                            $admin = 'all';
                        }
                        $result[$time] = $report[$admin]['total_items'];
                        break;
                    case 'avgorderofhead':
                        $orderdata = $this->getTargetData('wh_deliveryordertotal', $data, $timing);
                        $all_orders_total = $orderdata[$time];
                        $sql = "SELECT data FROM ".$ecs->table('target_log')." WHERE platform = 'whfile' AND start_time = $start_ts AND end_time = $end_ts ";
                        $res = $db->getOne($sql);
                        if(!empty($res)) {
                            $sale = unserialize($res);
                            $headcount = $sale['whfile_headcount'];
                        } else if(in_array($date['month'], ['Q1', 'Q2', 'Q3', 'Q4'])) {
                            $sql = "SELECT data FROM ".$ecs->table('target_log')." WHERE platform = 'whfile' AND start_time between $start_ts AND $end_ts AND end_time between $start_ts AND $end_ts ";
                            $res = $db->getCol($sql);
                            if(!empty($res)) {
                                foreach($res as $d){
                                    $tmp = unserialize($d);
                                    foreach ($tmp as $id=>$value) {
                                        $sale[$id]+=$value;
                                    }
                                }
                            }
                            $headcount = $sale['whfile_headcount'];
                        } else {
                            $headcount = 0;
                        }
                        $result[$time] = round(floatval($all_orders_total / $headcount), 2);
                        break;
                    case 'inoffofhead':
                        $orderdata = $this->getTargetData('wh_deliveryordertotal', $data, $timing);
                        $all_orders_total = $orderdata[$time];
                        $orderdata = $this->getTargetData('wh_pototal', $data, $timing);
                        $all_orders_total += $orderdata[$time];
                        $sql = "SELECT data FROM ".$ecs->table('target_log')." WHERE platform = 'whfile' AND start_time = $start_ts AND end_time = $end_ts ";
                        $res = $db->getOne($sql);
                        if(!empty($res)) {
                            $sale = unserialize($res);
                            $headcount = $sale['whfile_headcount'];
                        } else if(in_array($date['month'], ['Q1', 'Q2', 'Q3', 'Q4'])) {
                            $sql = "SELECT data FROM ".$ecs->table('target_log')." WHERE platform = 'whfile' AND start_time between $start_ts AND $end_ts AND end_time between $start_ts AND $end_ts ";
                            $res = $db->getCol($sql);
                            if(!empty($res)) {
                                foreach($res as $d){
                                    $tmp = unserialize($d);
                                    foreach ($tmp as $id=>$value) {
                                        $sale[$id]+=$value;
                                    }
                                }
                            }
                            $headcount = $sale['whfile_headcount'];
                        } else {
                            $headcount = 0;
                        }
                        $result[$time] = round(floatval($all_orders_total / $headcount), 2);
                        break;
                    default:
                        $result[$time] = [];
                        break;
                };
            } elseif($key == 'sale') {
                $static_cache_name = $date['start_date'].'-'.$date['end_date'].$key.$value.$data['goal_type'];
                if($time == 'now' && $on_month) {
                    $static_cache_name = $static_cache_name.$today;
                }
                $report = $this->memcache->get($static_cache_name);
                switch ($value) {
                    case 'revenue':
                        $adminuserController = new AdminuserController();
                        if( $data['goal_type'] == 'person') {
                            $admin = $data['admin_id'];
                        } else {
                            $admin = 'all';
                        }
                        $report = $adminuserController->getTeamRevence(($data['team_code'] ? $data['team_code'] : $key), $date, 1);
                        $result[$time] = $report[$admin]['volume'];
                        break;
                    case 'review':
                        $commentController = new CommentController();
                        $_REQUEST['year']  = $date['year'];
                        $_REQUEST['month'] = $date['month'];
                        $report = $commentController->getReviewAvgScore($commentController::REVIEW_SALES_TABLE_ID);
                        if( $data['goal_type'] == 'person') {
                            $admin = $data['admin_id'];
                            $result[$time] = $report['admin_avg'][$admin]['total_avg'];
                        } else {
                            $result[$time] = $report['total_avg'];
                        }
                        break;
                    case 'avgrevenueofhead':
                        $adminuserController = new AdminuserController();
                        if( $data['goal_type'] == 'person') {
                            $admin = $data['admin_id'];
                        } else {
                            $admin = 'all';
                        }
                        $report = $adminuserController->getTeamRevence(($data['team_code'] ? $data['team_code'] : $key), $date, 1);
                        $revenue = $report[$admin]['volume'];
                        $sql = "SELECT data FROM ".$ecs->table('target_log')." WHERE platform = 'salefile' AND start_time = $start_ts AND end_time = $end_ts ";
                        $res = $db->getOne($sql);
                        if(!empty($res)) {
                            $sale = unserialize($res);
                            $headcount = $sale['salefile_headcount'];
                        } else if(in_array($date['month'], ['Q1', 'Q2', 'Q3', 'Q4'])) {
                            $sql = "SELECT data FROM ".$ecs->table('target_log')." WHERE platform = 'salefile' AND start_time between $start_ts AND $end_ts AND end_time between $start_ts AND $end_ts ";
                            $res = $db->getCol($sql);
                            if(!empty($res)) {
                                foreach($res as $d){
                                    $tmp = unserialize($d);
                                    foreach ($tmp as $id=>$value) {
                                        $sale[$id]+=$value;
                                    }
                                }
                            }
                            $headcount = $sale['salefile_headcount'];
                        } else {
                            $headcount = 0;
                        }
                        $result[$time] = round(floatval($revenue / $headcount), 2);
                        break;
                    case 'emailregistrationrate':
                        $userController = new UserController();
                        $report = $userController->getPosRegEmailRate($date);
                        $result[$time] = $report['mail_rate'];
                        break;
                    default:
                        $result[$time] = [];
                        break;
                };
            } elseif($key == 'cs') {
                $static_cache_name = $date['start_date'].'-'.$date['end_date'].$key.$value.$data['goal_type'];
                if($time == 'now' && $on_month) {
                    $static_cache_name = $static_cache_name.$today;
                }
                $report = $this->memcache->get($static_cache_name);
                switch ($value) {
                    case 'review':
                        $commentController = new CommentController();
                        $_REQUEST['year']  = $date['year'];
                        $_REQUEST['month'] = $date['month'];
                        $report = $commentController->getReviewAvgScore($commentController::REVIEW_CRM_TABLE_ID);
                        if( $data['goal_type'] == 'person') {
                            $admin = $data['admin_id'];
                            $result[$time] = $report['admin_avg'][$admin]['total_avg'];
                        } else {
                            $result[$time] = $report['total_avg'];
                        }
                        break;
                    default:
                        $result[$time] = [];
                        break;
                };

            } else {
                $static_cache_name = $date['start_date'].'-'.$date['end_date'].$key;
                if($time == 'now' && $on_month) {
                    $static_cache_name = $static_cache_name.$today;
                }
                $report = false;
                if($report === false) {
                    /* Try to find in our datebase */
                    $sql = "SELECT data FROM ".$ecs->table('target_log')." WHERE platform = '".$key."' AND start_time = $start_ts AND end_time = $end_ts ";
                    $res = $db->getOne($sql);
                    if(!empty($res)) {
                        $report = unserialize($res);
                        $this->memcache->set($static_cache_name, $report);
                    } else if(in_array($date['month'], ['Q1', 'Q2', 'Q3', 'Q4'])) {
                        $sql = "SELECT data FROM ".$ecs->table('target_log')." WHERE platform = '".$key."' AND start_time between $start_ts AND $end_ts AND end_time between $start_ts AND $end_ts ";
                        $res = $db->getCol($sql);
                        if(!empty($res)) {
                            foreach($res as $d){
                                $tmp = unserialize($d);
                                foreach ($tmp as $id=>$value) {
                                    $report[$id]+=$value;
                                }
                            }
                        }
                        $this->memcache->set($static_cache_name, $report);
                    } else {
                        $report = [];
                    }
                }
                $result[$time] = $report[$type];
            }
        }

        return $result;

    }

    function insertAction()
    {
        global $ecs, $db, $_LANG;
        $data = $_REQUEST;
        $date = $this->getQarter($data['year'], $data['month']);
        $data['start_time'] = local_strtotime($date['start_date']);
        $data['end_time']   = local_strtotime($date['end_date']);
        if($data['parent_id'] > 0) {
            $sql = "SELECT * FROM ".$ecs->table('goals')." WHERE goal_id = ".$data['parent_id'];
            $parent = $db->getRow($sql);
            $data['team_code']      = $parent['team_code'];
            $data['start_time']     = $parent['start_time'];
            $data['end_time']       = $parent['end_time'];
            $data['year']           = $parent['year'];
            $data['month']          = $parent['month'];
            $data['parent_goal_id'] = $data['parent_id'];
        }
        $sql = "SELECT role_id FROM ".$ecs->table('admin_user')." WHERE user_id = ".$data['admin_id'];
        $role_id = $db->getOne($sql);
        $data['role_id']    = $role_id;
        $data['sender_id']  = $_SESSION['admin_id'];

        $model = new Model\YohoBaseModel('goals');
        $id = $model->insert($data);
        if($id > 0) $success = true;
        if($success) {
            $link[] = array('text' => '返回列表', 'href' => 'goal_setting.php');
            sys_msg('新增目標成功', 0, $link);
        }
    }

    function listAction($assign, $template)
    {
        global $ecs, $db, $smarty;
        $year = $_REQUEST['year'];
        $month = $_REQUEST['month'];
        $date = $this->getQarter($year, $month);
        $start_date = local_strtotime($date['start_date']);
        $end_date   = local_strtotime($date['end_date']);
        $sql = 'SELECT g.*, au.avatar, au.user_name '.
            ' FROM '.$ecs->table('goals') ." as g ".
            ' LEFT JOIN '.$ecs->table('admin_user')." as au ON au.user_id = g.admin_id ".
            " WHERE g.year = '$year' AND g.month = '$month' AND g.team_code = '".$_REQUEST['team_code']."' ";
        // if(!$_SESSION['manage_cost']) $sql .= " AND (g.admin_id = ".$_SESSION['admin_id']." OR g.sender_id = ".$_SESSION['admin_id'].")  ";//OR goal_type = 'team' )
        $list = $db->getAll($sql);
        $group_list = [];
        $underling  = [];
        foreach ($list as $goal) {
            if(!isset($goal['data_value'])) continue;
            $goal['year'] = $year;
            $goal['month'] = $month;
            $type = $goal['data_key'];
            $target = $this->getTargetData($type, $goal, 'now');
            $array = explode('_', $type);
            $key = $array[0];
            $value = $array[1];

            $configs = self::TARGET_CONFIG;
            if(isset($configs[$value])) {
                foreach ($configs[$value] as $config) {
                    $goal[$config] = 1;
                }
            }
            if($goal['time']) {
                $goal['now'] = $this->time_to_seconds($target['now']);
                $goal['data_value'] = $this->time_to_seconds($goal['data_value']);
                $goal['last_month'] = $this->time_to_seconds($target['last_month']);
                $goal['last_year'] = $this->time_to_seconds($target['last_year']);
            } else {
                $goal['now'] = $this->priceToFloat($target['now']);
                $goal['last_month'] = $this->priceToFloat($target['last_month']);
                $goal['data_value'] = $this->priceToFloat($goal['data_value']);
                $goal['last_year'] = $this->priceToFloat($target['last_year']);
            }
            if(!$goal['reverse']) {
                if($goal['last_month'] == 0) $goal['last_month_percent'] = '--';
                else $goal['last_month_percent'] = number_format(($goal['now'] - $goal['last_month']) / ($goal['last_month'] / 100), 0, '.', '');
                $goal['last_year'] = $this->priceToFloat($target['last_year']);
                if($goal['last_year'] == 0)$goal['last_year_percent'] = '--';
                else $goal['last_year_percent'] = number_format(($goal['now'] - $goal['last_year']) / ($goal['last_year'] / 100), 0, '.', '');
                $goal['now_percent'] = number_format(($goal['now'] / $goal['data_value']) * 100, 0, '.', '');
                $goal['year_grow'] = ($goal['last_year'] > $goal['now']);
                $goal['month_grow'] = ($goal['last_month'] > $goal['now']);
            } else {
                if($goal['last_month'] == 0) $goal['last_month_percent'] = '--';
                else $goal['last_month_percent'] = number_format(($goal['last_month'] - $goal['now']) / ($goal['now'] / 100), 0, '.', '');
                $goal['last_year'] = $this->priceToFloat($target['last_year']);
                if($goal['last_year'] == 0)$goal['last_year_percent'] = '--';
                else $goal['last_year_percent'] = number_format(($goal['last_year'] - $goal['now']) / ($goal['now'] / 100), 0, '.', '');
                $goal['now_percent'] = number_format(($goal['data_value'] / $goal['now']) * 100, 0, '.', '');
                $goal['year_grow'] = !($goal['last_year'] > $goal['now']);
                $goal['month_grow'] = !($goal['last_month'] > $goal['now']);
            }
            if(strpos($value, 'rate') !== false) {
                $goal['now'] = $goal['now'].'%';
                $goal['data_value'] = $goal['data_value'].'%';
                $goal['last_month'] = $goal['last_month'].'%';
                $goal['last_year'] = $goal['last_year'].'%';
            }
            if($goal['time']) {
                $goal['now'] = $this->seconds_to_time($goal['now']);
                $goal['data_value'] = $this->seconds_to_time($goal['data_value']);
                $goal['last_month'] = $this->seconds_to_time($goal['last_month']);
                $goal['last_year'] = $this->seconds_to_time($goal['last_year']);
            }
            $goal['name']   = (($goal['admin_id'] == $_SESSION['admin_id'] || $goal['goal_type'] != 'person') ? '' : $goal['user_name'].' - ').$this::TEAM_LIST[$goal['team_code']][$goal['goal_type']][$type];
            $array = explode('_', $type);
            $type_key = $array[0];
            if($type_key == 'category' && $goal['extra_id'] > 0) {
                $cat_name = $db->getOne("SELECT cat_name FROM ".$ecs->table('category')." WHERE cat_id = ".intval($goal['extra_id']));
                $goal['name'] .= "(".$cat_name.")";
            }
            if(!($goal['admin_id'] == $_SESSION['admin_id'] || $goal['goal_type'] != 'person')) {
                $goal['name_atatar'] = 1;
            }
            if ($goal['sender_id'] == $_SESSION['admin_id'] || $_SESSION['manage_cost']  || $_SESSION['role_id'] == 4) {
                $goal['can_remove'] = true;
            }
            if (($goal['admin_id'] == $_SESSION['admin_id'] || $_SESSION['manage_cost']  || $_SESSION['role_id'] == 4) && $goal['goal_type'] != 'person') {
                $goal['can_assign'] = true;
                /* Get this goal child goal */
                $sql = 'SELECT g.*, au.avatar, au.user_name '.
                    ' FROM '.$ecs->table('goals') ." as g ".
                    ' LEFT JOIN '.$ecs->table('admin_user')." as au ON au.user_id = g.admin_id ".
                    " WHERE g.team_code = '".$_REQUEST['team_code']."' AND g.parent_goal_id =  ".$goal['goal_id'];
                $child_list = $db->getAll($sql);
                foreach ($child_list as $c_key => $child) {
                    $child['year'] = $year;
                    $child['month'] = $month;
                    $type = $child['data_key'];
                    $target = $this->getTargetData($type, $child, 'now');
                    $child['now'] = $this->priceToFloat($target['now']);
                    $child['last_month'] = $this->priceToFloat($target['last_month']);
                    $child['last_year'] = $this->priceToFloat($target['last_year']);
                    $child['now_percent'] = number_format($child['now'] / $child['data_value'] * 100, 0, '.', '');
                    $child['name']   = $this::TEAM_LIST[$child['team_code']][$child['goal_type']][$type];
                    $child_list[$c_key] = $child;
                    $child['year_grow'] = !($child['last_year'] > $child['now']);
                    $child['month_grow'] = !($child['last_month'] > $child['now']);
                }
                $goal['child_list'] = $child_list;
            }
            if($goal['goal_type'] == 'person' && !($_SESSION['manage_cost'] || $goal['admin_id'] == $_SESSION['admin_id'] || $_SESSION['role_id'] == 4)) {
                continue;
            } else {
                $group_list[$goal['goal_type']][] = $goal;
            }

        }
        $smarty->assign('group_list', $group_list);
        $smarty->assign('underling', $underling);
        $smarty->assign('team_code', $_REQUEST['team_code']);
        $smarty->assign('act', $_REQUEST['act']);
        $smarty->display('library/goal_list.lbi');
    }

    function addAction()
    {
        global $ecs, $db, $smarty;
        $list = $this::GOAL_TYPE_CAT;
        if($_REQUEST['parent_id']) {
            $parent = $db->getRow('SELECT * FROM '.$ecs->table('goals') ." WHERE goal_id = ".intval($_REQUEST['parent_id']));
            if($parent['admin_id'] != $_SESSION['admin_id'] && !$_SESSION['manage_cost'] && !$_SESSION['role_id'] == 4) {
                $link[] = array('text' => $_LANG['go_back'], 'href' => 'javascript:history.back(-1)');
                sys_msg('你沒有該權限', 0, $link);
            }
            if($parent['goal_type'] == 'team') {
                unset($list['team']);
            }
            $smarty->assign('parent', $parent);
        } else {
            if(!$_SESSION['manage_cost'] && !$_SESSION['role_id'] == 4) {
                $link[] = array('text' => $_LANG['go_back'], 'href' => 'javascript:history.back(-1)');
                sys_msg('你沒有該權限', 0, $link);
            }
        }
        $smarty->assign('action_link', array('text' => '目標', 'href' => 'goal_setting.php'));
        $smarty->assign('team_list', $this::TEAM_LIST);
        $year = $this->getNextYear(3, 1);
        $smarty->assign('year', $year);
        $smarty->assign('list', $list);
        $smarty->display('goal_create.htm');
    }

    function uploadAction()
    {
        global $ecs, $db, $smarty;
        $smarty->assign('team_list', $this::TEAM_LIST);
        $smarty->assign('action_link', array('text' => '目標', 'href' => 'goal_setting.php'));
        $year = $this->getNextYear(10, 1);
        $smarty->assign('year', $year);
        $smarty->assign('month', $this::MONTH);
        $smarty->assign('data', $_REQUEST);
        $smarty->display('target_upload.htm');
    }

    function ajaxRemoveAction()
    {
        global $ecs, $db, $smarty;
        if($_REQUEST['id'] > 0) {
            $db->query('DELETE FROM '.$ecs->table('goals') ." WHERE goal_id = ".intval($_REQUEST['id']).' OR parent_goal_id = '.intval($_REQUEST['id']));
        }
        return true;
    }

    function importAction($input)
    {
        global $ecs, $db, $smarty;
        // init data
        ini_set('memory_limit', '256M');
        define( 'PCLZIP_TEMPORARY_DIR', ROOT_PATH . 'temp/');

        // Get excel data
        require_once ROOT_PATH . 'includes/excel/PHPExcel.php';
        require_once ROOT_PATH . 'includes/excel/PHPExcel/IOFactory.php';
        \PHPExcel_Settings::setZipClass(\PHPExcel_Settings::PCLZIP);
        $fileName     = $_FILES["import_excel"]["tmp_name"];
        $fileType     = \PHPExcel_IOFactory::identify($fileName);
        $objReader    = \PHPExcel_IOFactory::createReader($fileType);
        $objPHPExcel  = $objReader->load($fileName);
        $objWorksheet = $objPHPExcel->getSheet(0);
        $excelData = [];
        $date = $this->getQarter($input['year'], $input['month']);
        $start_ts = local_strtotime($date['start_date']);
        $end_ts = local_strtotime($date['end_date']);
        foreach ($objWorksheet->getRowIterator() as $key => $row) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false);
            // This loops all cells,
            // even if it is not set.
            // By default, only cells
            // that are set will be
            // iterated.
            foreach ($cellIterator as $c_key => $cell) {

                if($c_key == 0) {
                    $target_key = trim($cell->getValue());
                    $array = explode('_', $target_key);
                    $platform = $array[0];
                } elseif($c_key == 3) {
                    $set_value = trim($cell->getFormattedValue());
                }
                $excelData[$platform][$target_key] = $set_value;
            }
        }
        foreach ($excelData as $platform => $data) {
            if(empty($platform)) continue;
            $sql = "SELECT data FROM ".$ecs->table('target_log')." WHERE platform = '".$platform."' AND start_time = $start_ts AND end_time = $end_ts ";
            $res = $db->getOne($sql);
            if(!empty($res)) { // UPDATE
                $sql = "UPDATE ".$ecs->table('target_log')." SET data = '".serialize($data)."' WHERE platform = '".$platform."' AND start_time = $start_ts AND end_time = $end_ts ";
                $db->query($sql, 'SILENT');
            }
            else { // INSERT
                $sql = "INSERT INTO ".$ecs->table('target_log')." (platform, data, year, month, start_time, end_time) ".
                    "VALUES ('".$platform."', '".serialize($data)."', ".$input['year'].", ".$input['month'].", $start_ts, $end_ts)";
                $db->query($sql, 'SILENT');
            }
        }
        $link[] = array('text' => '返回列表', 'href' => 'goal_setting.php');
        sys_msg('新增目標成功', 0, $link);
    }
}
?>