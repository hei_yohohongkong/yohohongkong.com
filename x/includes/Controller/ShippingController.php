<?php
/**
 * Created by Anthony.
 * User: Anthony
 * Date: 2018/9/10
 * Time: 下午 05:47
 */

namespace Yoho\cms\Controller;

use Braintree\Address;
use Yoho\cms\Model;

class ShippingController extends YohoBaseController
{
    //array[ORDER_FEE_PLATFORM_WEB, ORDER_FEE_PLATFORM_POS, ORDER_FEE_PLATFORM_CMS, ORDER_FEE_PLATFORM_MONEY]
    const ORDER_FEE_PLATFORM_WEB   = 'web';
    const ORDER_FEE_PLATFORM_POS   = 'pos';
    const ORDER_FEE_PLATFORM_CMS   = 'cms';
    const ORDER_FEE_PLATFORM_MONEY = 'money';

    const SHIPPING_TYPE_LOGO_PATH = "../" . DATA_DIR . "/afficheimg/shipping_type_logo/";
    public $tableName = 'shipping';

    public function getShippingModules()
    {
        global $db, $ecs, $_LANG;

        $modules = read_modules('../includes/modules/shipping');

        for ($i = 0; $i < count($modules); $i++) {
            $lang_file = ROOT_PATH . 'languages/' . $_CFG['lang'] . '/shipping/' . $modules[$i]['code'] . '.php';

            if (file_exists($lang_file)) {
                include_once($lang_file);
            }

            /* 检查该插件是否已经安装 */
            $sql = "SELECT s.*, st.shipping_type_name as shipping_type_name " .
                "FROM " . $ecs->table('shipping') . " as s " .
                "LEFT JOIN " . $ecs->table('shipping_type') . " as st ON st.shipping_type_id = s.shipping_type_id " .
                " WHERE s.shipping_code='" . $modules[$i]['code'] . "'";
            $row = $db->GetRow($sql);

            // Multiple language support
            if (!empty($_REQUEST['edit_lang'])) {
                $row = array_pop(localize_db_result_lang($_REQUEST['edit_lang'], 'shipping', array($row)));
            }
            $html = '';
            if ($row) {
                /* 插件已经安装了，获得名称以及描述 */
                $modules[$i]['shipping_id'] = $row['shipping_id'];
                $modules[$i]['shipping_name'] = $row['shipping_name'];
                $modules[$i]['shipping_desc'] = $row['shipping_desc'];
                $modules[$i]['insure'] = $row['insure'];
                $modules[$i]['support_cod'] = $row['support_cod'];
                $modules[$i]['support_fod'] = $row['support_fod'];
                $modules[$i]['is_pos'] = $row['is_pos'];
                $modules[$i]['sort_order'] = $row['sort_order'];
                $modules[$i]['install'] = 1;
                $modules[$i]['logo'] = $row['shipping_logo'];
                $modules[$i]['shipping_type_id'] = $row['shipping_type_id'];
                $modules[$i]['shipping_type_name'] = $row['shipping_type_name'];

                if (isset($modules[$i]['insure']) && ($modules[$i]['insure'] === false)) {
                    $modules[$i]['is_insure'] = 0;
                } else {
                    $modules[$i]['is_insure'] = 1;
                }
                $html .= "<a href=\"javascript:logo_edit('$row[shipping_code]')\">設置logo</a> ";
                $html .= "<a href=\"javascript:confirm_redirect(lang_removeconfirm,'shipping.php?act=uninstall&code=$row[shipping_code]')\">卸載</a> ";
                $html .= "<a href=\"shipping_area.php?act=list&shipping=$row[shipping_id]\">$_LANG[shipping_area]</a>";
                $modules[$i]['_handle'] = $html;
            } else {
                $modules[$i]['shipping_name'] = $_LANG[$modules[$i]['code']];
                $modules[$i]['shipping_desc'] = $_LANG[$modules[$i]['desc']];
                $modules[$i]['insure'] = empty($modules[$i]['insure']) ? 0 : $modules[$i]['insure'];
                $modules[$i]['support_cod'] = $modules[$i]['cod'];
                $modules[$i]['support_fod'] = 0;
                $modules[$i]['install'] = 0;
                $modules[$i]['_handle'] = "<a href=\"shipping.php?act=install&code=" . $modules[$i]['code'] . "\">$_LANG[install]</a>";
            }
        }

        return $modules;
    }

    public function getShippingTypeSelect()
    {
        global $ecs, $db;

        $sql = "SELECT shipping_type_id as value, shipping_type_name as name FROM " . $ecs->table('shipping_type') . " WHERE enabled = 1";
        $res = $db->getAll($sql);
        array_unshift($res,['value'=>0, 'name'=>'N/A']);
        return $res;

    }

    public function shippingTypeListAction()
    {
        global $_LANG;
        $assign = [];
        $assign['ur_here'] = $_LANG['shipping_type_list'];
        $assign['modules'] = $modules;
        $assign['full_page'] = 1;
        $assign['action_link'] = array('href' => 'shipping.php?act=list',
            'text' => $_LANG['03_shipping_list']);
        $template = 'shipping_type_list.htm';
        $this->generateActionTemplate($assign, $template);
    }

    public function shippingTypeAjaxQueryAction()
    {
        global $ecs, $db;
        /* 记录总数以及页数 */
        $sql = "SELECT COUNT(*) FROM " . $ecs->table('shipping_type');

        $record_count = $db->getOne($sql);

        /* 查询记录 */
        $sql = "SELECT * FROM " . $ecs->table('shipping_type') . " ORDER BY " . $_POST['sort_by'] . " " . $_POST['sort_order'];

        //set_filter($filter, $sql);
        $res = $db->selectLimit($sql, $_POST['page_size'], $_POST['start']);

        $arr = array();
        while ($row = $db->fetchRow($res)) {

            $row['shipping_type_icon'] = empty($row['shipping_type_icon']) ? false : true;

            $arr[] = $row;
        }

        // Multiple language support
        if (!empty($_REQUEST['edit_lang'])) {
            $arr = localize_db_result_lang($_REQUEST['edit_lang'], 'shipping_type', $arr);
        }
        $this->tableName = 'shipping_type';
        parent::ajaxQueryAction($arr, $record_count, ['edit']);
    }

    public function shippingTypeEditAction()
    {
        global $ecs, $db, $_LANG;
        $insert = false;
        if (!empty($_REQUEST['act'] == "add")) {
            $insert = true;
        }
        $shipping_type_id = empty($_REQUEST['id']) ? "" : $_REQUEST['id'];
        if (empty($shipping_type_id) && !$insert) {
            sys_msg("請選擇欲處理的類型");
        }
        $localizable_fields = localizable_fields_for_table('shipping_type');
        $assign['localizable_fields'] = $localizable_fields;
        if (!$insert) {
            $sql = "SELECT * FROM " . $ecs->table("shipping_type") .
                " WHERE shipping_type_id = $shipping_type_id";
            $record = $db->getRow($sql);
            if (!empty($record['shipping_type_icon'])) $record['shipping_type_icon'] = self::SHIPPING_TYPE_LOGO_PATH . $record['shipping_type_icon'];
            $assign["record"] = $record;
            $localized_versions = get_localized_versions('shipping_type', $shipping_type_id, $localizable_fields);
            $assign['localized_versions'] = $localized_versions;
        }


        $assign['ur_here'] = "編輯配送類型";
        $assign['action_link'] = array('text' => "返回列表", 'href' => 'shipping_type.php?act=list');
        $template = "shipping_type_info.htm";

        $this->generateActionTemplate($assign, $template);
    }

    public function shippingTypeUpdateAction()
    {
        global $_LANG, $db, $ecs;
        require_once(ROOT_PATH . 'includes/cls_image.php');
        $act = $_REQUEST['act'];
        $list = $_REQUEST;
        if (empty($_REQUEST['shipping_type_id'])) $act = 'insert';
        $model = new Model\YohoBaseModel('shipping_type');

        // handle simplified chinese
        $localizable_fields = [
            "shipping_type_name"    => $_REQUEST['shipping_type_name'],
            "shipping_type_desc"    => $_REQUEST['shipping_type_desc'],
        ];

        if ($act == 'insert') {
            $id = $model->insert($list);
            if ($id > 0) $success = true;
        } elseif ($act == 'update') {
            $pk = $model->getPkey();
            $list[$pk] = $id = isset($_REQUEST[$pk]) ? $_REQUEST[$pk] : $_REQUEST['id'];
            if ($list['id'] && !isset($list[$pk])) {
                $list[$pk] = $list['id'];
            }
            $update = $model->update($list);
            if ($update > 0) $success = true;
            $localizable_fields = $this->checkRequireSimplied("shipping_type", $localizable_fields, $list[$pk]);
        }
        to_simplified_chinese($localizable_fields);

        $file_url = '';
        $extname = strtolower(substr($_FILES['shipping_type_icon']['name'], strrpos($_FILES['shipping_type_icon']['name'], '.') + 1));
        if ((isset($_FILES['shipping_type_icon']['error']) && $_FILES['shipping_type_icon']['error'] == 0) || (!isset($_FILES['shipping_type_icon']['error']) && isset($_FILES['shipping_type_icon']['tmp_name']) && $_FILES['shipping_type_icon']['tmp_name'] != 'none')) {
            // 检查文件格式
            if (!in_array($extname, ['svg', 'SVG'])) {
                sys_msg('檔案格式錯誤');
            }

            // 复制文件
            if (!make_dir(self::SHIPPING_TYPE_LOGO_PATH)) {
                /* 创建目录失败 */
                $res = false;
            }

            $filename = 'shipping_type_icon_' . $id . "." . $extname;
            $path = self::SHIPPING_TYPE_LOGO_PATH . $filename;
            if (move_upload_file($_FILES['shipping_type_icon']['tmp_name'], $path)) {
                $res = $filename;
            } else {
                $res = false;

            }
            if ($res != false) {
                $file_url = $res;
            }
            $db->query("UPDATE " . $ecs->table('shipping_type') . " SET shipping_type_icon = '" . $file_url . "' WHERE shipping_type_id = " . $id);
        }

        // Insert/Update success
        // Multiple language support
        save_localized_versions('shipping_type', $id);
        /* 清除缓存 */
        clear_cache_files();

        admin_log('shipping_type' . ': ' . $id, ($act == 'insert') ? 'add' : 'edit', 'shipping_type');

        $link[0]['text'] = _L('back_list', '返回列表');
        $link[0]['href'] = 'shipping_type.php?act=list';
        $note = _L('attradd_succed', '操作成功');
        sys_msg($note, 0, $link);

    }

    public function available_shipping_type_list()
    {
        global $db, $ecs;
        $sql = "SELECT * from " . $ecs->table('shipping_type') . " WHERE enabled = 1 ";
        $res = $db->getAll($sql);
        $res = localize_db_result('shipping_type', $res);
        $list = [];
        foreach ($res as $key => $value) {
            if (!empty($value['shipping_type_icon'])) { // Handle shipping type icon
                $path = self::SHIPPING_TYPE_LOGO_PATH . $value['shipping_type_icon'];
                $extname = strtolower(substr($value['shipping_type_icon'], strrpos($value['shipping_type_icon'], '.') + 1));
                if (in_array($extname, ['svg', 'SVG'])) {
                    $value['shipping_type_icon_svg'] = $this->svgIconContent($path, $value['shipping_type_code']);
                }
                $value['shipping_type_icon'] = $path;
            } else {
                $value['shipping_type_icon'] = null;
            }
            $list[$key] = $value;
        }
        return $list;
    }

    public function getFlowShippingList($shipping_type, $consignee, $platform)
    {
        global $ecs, $db, $_LANG;
        $addressController = new AddressController();
        if (!empty($consignee)) $region = array($consignee['country'], $consignee['province'], $consignee['city'], $consignee['district']);
        $flow_type = isset($_SESSION['flow_type']) ? intval($_SESSION['flow_type']) : CART_GENERAL_GOODS;
        $res = [];
        /* get free  */
        $free_region = $addressController->get_free_calculate_region();
        $cal_free_shipping = false;
        //  查看購物車中是否全為免運費商品，若是則把運費賦為零 (如果全免運費, 贈品不計算運費)
        if($platform == self::ORDER_FEE_PLATFORM_WEB) $cart_table = $GLOBALS['ecs']->table('cart');
        if($platform == self::ORDER_FEE_PLATFORM_POS) $cart_table = $GLOBALS['ecs']->table('cart1');

        $sql = 'SELECT count(*) FROM ' . $cart_table . " WHERE `session_id` = '" . SESS_ID. "' AND `extension_code` != 'package_buy' AND `is_shipping` = 0 AND is_gift = 0";
        $shipping_count = $db->getOne($sql);
        $shipping_list = available_shipping_list($region, $shipping_type);
        $shipping_group = [];
        foreach ($shipping_list AS $key => $val) {
            $cal_free_shipping = false;
            if (!empty($free_region['country'] && in_array($val['region_id'], $free_region['country']))) $cal_free_shipping = true;

            if($platform == self::ORDER_FEE_PLATFORM_WEB) $cart_weight_price = cart_weight_price($flow_type, $cal_free_shipping);
            if($platform == self::ORDER_FEE_PLATFORM_POS) $cart_weight_price = cart_weight_price_1($flow_type, $cal_free_shipping);

            $shipping_list[$key]['cal_free_shipping'] = $cal_free_shipping;
            $shipping_cfg = unserialize_config($val['configure']);
            if ($shipping_count == 0 AND $cart_weight_price['free_shipping'] == 1 && $cal_free_shipping) {
                $shipping_fee = 0;
            } else {
                $shipping_fee = shipping_fee($val['shipping_code'], unserialize($val['configure']), $cart_weight_price['weight'], $cart_weight_price['amount'], $cart_weight_price['number'], $cart_weight_price['price']);
            }
            if($shipping_fee == 0) 
            {
                $shipping_list[$key]['format_shipping_fee'] = "(" . _L('goods_free_shipping_eligable', '免運費') . ")";
            } else {
                $shipping_list[$key]['format_shipping_fee'] = _L('goods_shipping_fee', '運費') . " " . price_format($shipping_fee, false);
            }
            $shipping_list[$key]['shipping_fee'] = $shipping_fee;
            $shipping_list[$key]['free_money'] = price_format($shipping_cfg['free_money'], false);
            $shipping_list[$key]['insure_formated'] = strpos($val['insure'], '%') === false ?
                price_format($val['insure'], false) : $val['insure'];
            if (!empty($val['shipping_logo'])) {
                $path = '../' . DATA_DIR . '/afficheimg/shipping_logo/' . $val['shipping_logo'];
                $extname = strtolower(substr($val['shipping_logo'], strrpos($val['shipping_logo'], '.') + 1));
                if (in_array($extname, ['svg', 'SVG'])) {
                    $shipping_list[$key]['shipping_logo_svg'] = $this->svgIconContent($path, $val['shipping_code']);
                }
                $shipping_list[$key]['logo'] = $path;
            } else {
                $shipping_list[$key]['logo'] = null;
            }
            $shipping_list[$key]['logo'] = '../' . DATA_DIR . '/afficheimg/shipping_logo/' . $val['shipping_logo'];

            /* 当前的配送方式是否支持保价 */
            if ($val['shipping_id'] == $order['shipping_id']) {
                $insure_disabled = ($val['insure'] == 0);
                $cod_disabled = ($val['support_cod'] == 0);
            }

            /* 如果不是香港，不顯示運費到付選項 */
            if ((($consignee['country'] != 3409) && ($val['support_fod']) && $shipping_type == 'shipping') || ($val['region_id'] != 3409) && ($val['support_fod'])) {
                $shipping_list[$key]['support_fod'] = 0;
            }

            /* 如果訂單中有固定運費產品，不顯示運費到付選項 */
            if (($cart_weight_price['price'] > 0) && ($val['support_fod'])) {
                $shipping_list[$key]['support_fod'] = 0;
            }

            if (($cart_weight_price['price'] > 0) && ($val['region_id'] != 3409)) {
                unset($shipping_list[$key]);
            }
            if(isset($shipping_cfg['group_list']) && isset($shipping_list[$key])) {
                $shipping_group[$shipping_cfg['group_list']][$val['shipping_id']] = $shipping_list[$key];
                unset($shipping_list[$key]);
            }

        }
        $count = count($shipping_list);
        foreach($shipping_group as $group_name => $group) {
           // print_r($group);
            $i = $count + 1;
            $group_shipping = [];
            $group_shipping['shipping_name'] = ($_LANG['group_shipping'][$group_name] ? $_LANG['group_shipping'][$group_name] : $group_name);
            $group_shipping['shipping_fee'] = max(array_column($group, 'shipping_fee'));
            $min_shipping_fee = min(array_column($group, 'shipping_fee'));
            if($min_shipping_fee == $group_shipping['shipping_fee']) {
                if( $group_shipping['shipping_fee'] == 0) 
                {
                    $group_shipping['format_shipping_fee'] = "(" . _L('goods_free_shipping_eligable', '免運費') . ")";
                } else {      
                    $group_shipping['format_shipping_fee'] = _L('goods_shipping_fee', '運費') . " " . price_format($group_shipping['shipping_fee'], false);
                }                           
            } else {
                $group_shipping['format_shipping_fee'] = _L('goods_shipping_fee', '運費') . " " . price_format($min_shipping_fee, false).' ~ '.price_format($group_shipping['shipping_fee'], false);
            }
            $group_shipping['shipping_desc'] = '';
            $group_shipping['shipping_area_id'] = array_column($group, 'shipping_area_id');
            $group_shipping['shipping_id'] = array_column($group, 'shipping_id');
            $group_shipping['is_group'] = $group_name;
            /* We get this type icon to group icon */
            $sql = "SELECT shipping_type_icon from " . $ecs->table('shipping_type') . " WHERE shipping_type_code = '$shipping_type' ";
            $shipping_type_icon = $db->getOne($sql);
            
            if (!empty($shipping_type_icon)) { // Handle shipping type icon
                $path = self::SHIPPING_TYPE_LOGO_PATH . $shipping_type_icon;
                $extname = strtolower(substr($shipping_type_icon, strrpos($shipping_type_icon, '.') + 1));
                if (in_array($extname, ['svg', 'SVG'])) {
                    $group_shipping['shipping_logo_svg'] = $this->svgIconContent($path, $shipping_type);
                }
                $group_shipping['logo'] = $path;
                $group_shipping['shipping_logo'] = $path;
            } else {
                $group_shipping['logo'] = null;
                $group_shipping['shipping_logo'] = null;
            }

            array_unshift($shipping_list, $group_shipping);
        }

        /* 如果訂單中有固定運費產品，只送香港 */
        if ((($cart_weight_price['price'] > 0) && ($consignee['country'] != 3409) && $shipping_type == 'shipping' )|| ($cart_weight_price['price'] > 0) && (count($shipping_list) == 0)) {
            // Clear shipping_list
            $shipping_list = array();

            $sql = "SELECT c.goods_name " .
                "FROM " . $cart_table . " as c " .
                "LEFT JOIN " . $ecs->table('goods') . " as g ON g.goods_id = c.goods_id " .
                "WHERE c.session_id = '" . SESS_ID . "' " .
                "AND rec_type = '$flow_type' AND " . ($cal_free_shipping ? "g.is_shipping = 0" : " 1 ") . " AND g.shipping_price > 0 AND c.extension_code != 'package_buy'";
            $restricted_to_hk_goods = $db->getCol($sql);
            $res['restricted_to_hk_goods'] = $restricted_to_hk_goods;
        }

        
        $shoted_list = array();
        $locker = $otherPickUp = $SFCenter = $ConvStore = $PostOffice = $other = array();
        foreach ($shipping_list as $ship){

            if ($ship['shipping_name'] == "智能櫃" || $ship['shipping_name'] == "智能柜" || $ship['shipping_name'] == "Locker"){
                if (sizeof($locker) == 0){
                    $locker = $ship;
                }
            } elseif ($ship['shipping_name'] == "便利店自取" || $ship['shipping_name'] == "便利店自取" || $ship['shipping_name'] == "Convenience Store"){
                if (sizeof($ConvStore) == 0){
                $ConvStore = $ship;
            }
            } elseif ($ship['shipping_name'] == "順豐服務中心" || $ship['shipping_name'] == "顺丰服务中心" || $ship['shipping_name'] == "SF Service Center"){
                if (sizeof($SFCenter) == 0){
                $SFCenter = $ship;
            }
            } elseif ($ship['shipping_name'] == "郵政局" || $ship['shipping_name'] == "邮政局" || $ship['shipping_name'] == "Post Offices"){
                if (sizeof($PostOffice) == 0){
                $PostOffice = $ship;
            }
            } elseif ($ship['shipping_name'] == "其他自提點" || $ship['shipping_name'] == "其他自提点" || $ship['shipping_name'] == "Other Pick Up Centers"){
                if (sizeof($otherPickUp) == 0){
                $otherPickUp = $ship;
            }
            } else {
                array_push($other, $ship);
            }

        }

        if (sizeof($locker) > 0){
            array_push($shoted_list, $locker);
        }
        if (sizeof($ConvStore) > 0){
        array_push($shoted_list, $ConvStore);
        }
        if (sizeof($SFCenter) > 0){
        array_push($shoted_list, $SFCenter);
        }
        if (sizeof($PostOffice) > 0){
        array_push($shoted_list, $PostOffice);
        }

        if (sizeof($otherPickUp) > 0){
        array_push($shoted_list, $otherPickUp);
        }

        foreach ($other as $o){
            if (sizeof($o) > 0){
              array_push($shoted_list, $o);
            }
        }

        $res['shipping_list'] = $shoted_list;

        //dd($res['shipping_list']);exit;

        return $res;
    }

    public function cal_shipping_volumeric_oversize($source_vols = [], $target_vols = [])
    {
        $goods_vols['width'] = $source_vols['width'];
        $goods_vols['length'] = $source_vols['length'];
        $goods_vols['height'] = $source_vols['height'];
        $shipping_vols['max_width'] = $target_vols['max_width'];
        $shipping_vols['max_height'] = $target_vols['max_height'];
        $shipping_vols['max_length'] = $target_vols['max_length'];
        if(empty($shipping_vols)) return false;
        $total_size = $shipping_vols['max_width'] + $shipping_vols['max_height'] + $shipping_vols['max_length'];
        $goods_size = $goods_vols['width'] + $goods_vols['length'] + $goods_vols['height'];
        if ($goods_size >= ($total_size * 0.9)) {
            return true;
        }
        asort($shipping_vols);
        asort($goods_vols);
        foreach($shipping_vols as $skey => $vol) {
            foreach($goods_vols as $gkey => $gvol) {
                if($gvol <= $vol) {
                    unset($goods_vols[$gkey]);
                    break;
                } else {
                    return true;
                }
            }
        }

        return false;
    }

    public function getShippingAreaAddressList($data, $user_id) {
        global $db, $ecs;
        $addressController = new AddressController();

        $user_id = empty($user_id)? $_SESSION['user_id'] : $user_id;

        $list = [];
        $over_max_size_count = 0;
        //$data['shipping_id']="11,12,13";
        //$data['shipping_area_id']="50,49,51";
        if(strpos($data['shipping_id'], ",") === false) {
            $data_array[] = $data;
        } else {
            $id_list   = explode(',', $data['shipping_id']);
            $area_list = explode(',', $data['shipping_area_id']);
            $data_array = [];
            foreach($id_list as $k => $id) {
                $data_array[$k]['shipping_id'] = $id;
                $data_array[$k]['shipping_area_id'] = $area_list[$k];
            }
        }
        foreach ($data_array as $k => $data) {
            $shipping = shipping_area_info($data['shipping_id'], [], $data['shipping_area_id']);
            // Get shipping area address list
            include_once('includes/modules/shipping/' . $shipping['shipping_code'] . '.php');
            $shipping_obj    = new $shipping['shipping_code'](unserialize($shipping['configure']));
            if (method_exists($shipping_obj, 'getPickUpList')) {
                $address_list = $shipping_obj->getPickUpList(false, $data['shipping_area_id']);
                foreach ($address_list as $ak => $a) {
                    $a['shipping_area_id'] = $data['shipping_area_id'];
                    $a['shipping_id'] = $data['shipping_id'];
                    $a['shipping_desc'] = $shipping['shipping_desc'];
                    $a['sort_order'] = $shipping['sort_order'];
                    $address_list[$ak] = $a;
                }
            } else {
                $address_list = [];
            }

            // Get user last address
            $sql = "SELECT * FROM ".$ecs->table('user_address')." WHERE user_id = ".$user_id." AND address_type = '".$addressController::ADDRESS_TYPE_LOCKER."' LIMIT 1 ";
            $address = $db->getRow($sql);
            $type_code = $address['type_code'];
            $consignee = $address['consignee'];
            $tel       = $address['tel'];
            $locker = [];
            if ($type_code && !empty($address_list)) {
                $code = unserialize($type_code)[0];
                $tmp_list = $shipping_obj->getPickUpList(false, $data['shipping_area_id']);
                $locker   = $tmp_list[$code];
                if (unserialize($type_code)[1]) {
                    $locker['arCode'] = unserialize($type_code)[1];
                }
                $locker['consignee']  = $consignee;
                $locker['tel']        = $tel;
            }
            /* If shipping have w x h x l, cal w x h x l */
            $over_max_size = 0;
            $shipping_cfg = unserialize_config($shipping['configure']);
            if ($shipping_cfg['max_width'] && $shipping_cfg['max_height'] && $shipping_cfg['max_length']) {
                $flow_type = isset($_SESSION['flow_type']) ? intval($_SESSION['flow_type']) : CART_GENERAL_GOODS;
                $cart_goods = cart_goods($flow_type);
                $total['width'] = array_sum(array_column($cart_goods, 'total_width'));
                $total['length'] = array_sum(array_column($cart_goods, 'total_length'));
                $total['height'] = array_sum(array_column($cart_goods, 'total_height'));
                /* If shipping can't put this product, over_max_size is true */
                foreach($address_list as $address_key => $address) {
                    if($address['max_width'] && $address['max_length'] && $address['max_height']) {
                        $address['over_max_size'] = $this->cal_shipping_volumeric_oversize($total, $address);
                    } else {
                        $address['over_max_size'] = $this->cal_shipping_volumeric_oversize($total, $shipping_cfg);
                    }
                    $over_max_size_count += ($address['over_max_size'] == true);
                    $address_list[$address_key] = $address;
                }
            }
            $list = array_merge($list, $address_list);
        }
        usort($list, function ($a, $b){
            return $a['sort_order'] < $b['sort_order'] ? -1 : 1;
        });

        // all address is over size, remove address_list
        if($over_max_size_count == count($list)) $list = [];
            
        $group_list = [];
        foreach($list as $key => $ef) {
            $group_list[$ef['city']][$ef['district']][$ef['allotRouter']][$ef['edCode']] = $ef;
        }

        return array(
            'group_list' => $group_list,
            'locker' => $locker,
        );
    }
}