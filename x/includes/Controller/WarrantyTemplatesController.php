<?php

/***
* WarrantyTemplatesController.php
* by Anthony 20180423
*
* WarrantyTemplates controller
***/

namespace Yoho\cms\Controller;
use Yoho\cms\Model;
if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

class WarrantyTemplatesController extends YohoBaseController {

    function get_warranty_templates()
    {
        $_REQUEST['sort_by']      = empty($_REQUEST['sort_by']) ? 'template_id' : trim($_REQUEST['sort_by']);
        $_REQUEST['sort_order']   = empty($_REQUEST['sort_order']) ? 'ASC' : trim($_REQUEST['sort_order']);

        $sql = "SELECT count(*) FROM " . $GLOBALS['ecs']->table('warranty_templates');
        $record_count = $GLOBALS['db']->getOne($sql);

        /* 查询 */
        $sql = "SELECT wt.*, " .
            "(SELECT count(*) FROM " . $GLOBALS['ecs']->table('goods') . " as g WHERE g.`warranty_template_id` = wt.`template_id`) as goods_count " .
            "FROM " . $GLOBALS['ecs']->table('warranty_templates') . " as wt " .
            "ORDER BY " . $_REQUEST['sort_by'] . " " . $_REQUEST['sort_order'] . " " .
            "LIMIT " . $_REQUEST['start'] . "," . $_REQUEST['page_size'];
        $data = $GLOBALS['db']->getAll($sql);

        // Multiple language support
        if (!empty($_REQUEST['edit_lang']))
        {
            $data = localize_db_result_lang($_REQUEST['edit_lang'], 'warranty_templates', $data);
        }

        foreach ($data as $key => $row)
        {
            $data[$key]['start_date'] = local_date($GLOBALS['_CFG']['time_format'], $row['start_time']);
            $data[$key]['end_date'] = local_date($GLOBALS['_CFG']['time_format'], $row['end_time']);
            $data[$key]['active'] = ((gmtime() >= $row['start_time']) && (gmtime() <= $row['end_time']));
        }

        $arr = array('data' => $data, 'record_count' => $record_count);

        return $arr;
    }

    function ajaxQueryAction()
    {
        $list = $this->get_warranty_templates();
        parent::ajaxQueryAction($list['data'], $list['record_count']);
    }

    public function buildForm($formMapper = [], $id = 0, $act = '', $assign = [], $redirect = '')
    {
        if(empty($formMapper))
        $formMapper = [
            'template_name' => ['required' => true],
            'template_content' => ['type' => 'editor'],
            'template_id' => ['type' => 'hidden']
        ];
        parent::buildForm($formMapper, $id, $act, $assign, $redirect);
    }

}