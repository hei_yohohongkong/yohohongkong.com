<?php

namespace Yoho\cms\Controller;

if (!defined('IN_ECS')) {
    die('Hacking attempt');
}

require_once ROOT_PATH . 'includes/lib_order.php';
require_once ROOT_PATH . 'includes/lib_payment.php';

class RefundController extends YohoBaseController
{
    const REFUND_STATUS_STRING = array(
        REFUND_PENDING => array('pending', '未處理'),
        REFUND_PROCESS => array('processing', '退款處理中'),
        REFUND_PROCESSED => array('processed', '待退款確定'),
        REFUND_FINISH  => array('finished', '已完成'),
        REFUND_REJECT  => array('rejected', '不處理'),
    );
    const REFUND_STATUS = array(
        REFUND_PENDING,
        REFUND_PROCESS,
        REFUND_PROCESSED,
        REFUND_FINISH,
        REFUND_REJECT
    );
    const GROUP_BY_STATUS = "CASE WHEN status = " . REFUND_FINISH . " THEN 1 WHEN status = " . REFUND_REJECT . " THEN 2 ELSE 0 END";
    const REFUND_TYPE_NORMAL = 'edit_order';
    const REFUND_TYPE_WHOLE  = 'whole_order_refund';
    const REFUND_TYPE_COMPENSATE = 'compensate';
    const REFUND_TYPE = array(
        REFUND_NORMAL => '修改訂單',
        REFUND_WHOLE_ORDER => '全單取消',
        REFUND_COMPENSATE => '售後補償',
        REFUND_CUSTOM => '手動申請'
    );

    public function __construct()
    {
        parent::__construct();
        $this->tableName = 'refund_requests';
    }

    /**
     * Get total refund amount of the order (not finished)
     * 
     * @param int $order_id order id
     * 
     * @return float total refund amount (money + integral(convert to dollar))
     */
    public function currentProcessRefundAmount($order_id)
    {
        $sql = "SELECT SUM(refund_amount) as refund_amount, SUM(integral_amount) as integral_amount FROM " . $GLOBALS['ecs']->table('refund_requests') . " WHERE order_id = $order_id AND status " . db_create_in(array(REFUND_PENDING, REFUND_PROCESS,REFUND_PROCESSED)) . " AND refund_type != " . REFUND_COMPENSATE;
        $res = $GLOBALS['db']->getRow($sql);
        return $res['refund_amount'] + value_of_integral($res['integral_amount']);
    }

    /**
     * Assign refund information
     * 
     * @param int $order_sn      order sn
     * @param int $order_id      order id
     * @param int $refund_amount refund amount
     * @param int $compensate    is compensate
     * 
     * @return void
     */
    public function assignRefundInfo($order_sn = 0, $order_id = 0, $refund_amount = 0, $compensate = 0)
    {
        global $ecs, $db, $smarty;

        if (empty($order_sn) && !empty($order_id)) {
            $order_sn = $this->getOrderSnById($order_id);
        } elseif (!empty($order_sn) && empty($order_id)) {
            $order_id = $this->getOrderIdBySn($order_sn);
        }
        if (empty($order_sn) || empty($order_id)) {
            $_POST['crm'] ? make_json_error('沒有此訂單之紀錄') : sys_msg("沒有此訂單之紀錄");
        }

        $sql = "SELECT oi.`money_paid`, oi.`integral`, oi.`order_type`, oi.`actg_paid_amount`, u.`user_name`, u.`user_id` " .
                "FROM " . $ecs->table('order_info') . " as oi " .
                "LEFT JOIN " . $ecs->table('users') . " as u ON oi.user_id = u.user_id " .
                "WHERE oi.order_sn = '" . $order_sn . "'";
        $res = $db->getRow($sql);

        if (!empty($res)) {
            if (is_online_payment($order_id)) {

                // check order payment transaction log
                $txn = $this->getOnlineTxnLog($order_id);
                $smarty->assign('txns', $txn);
            }
            if (!empty($refund_amount)) {
                $cfg = load_config(true);
                $fixed_refund = array(
                    'money' => $refund_amount,
                    'point' => integral_of_value($refund_amount)
                );
                $smarty->assign('fixed_refund', $fixed_refund);
                $smarty->assign('type', self::REFUND_TYPE_NORMAL);
            } elseif (empty($compensate)) {
                $sql = "SELECT COUNT(*) FROM " . $ecs->table('refund_requests') . " WHERE order_id = $order_id AND status " . db_create_in(array(REFUND_PENDING, REFUND_PROCESS,REFUND_PROCESSED)) . " AND refund_type != " . REFUND_COMPENSATE;
                $req = $db->getOne($sql);
                $smarty->assign('req', $req);
                $current_refund = $this->currentProcessRefundAmount($order_id);
                $money = floatval($res['money_paid']) > $current_refund ? floatval($res['money_paid']) - $current_refund : 0;
                $point = $money == 0 && !empty($res['integral']) ? $res['integral'] : integral_of_value($money);
                if ($res['order_type'] == OrderController::DB_ORDER_TYPE_WHOLESALE) {
                    $money = floatval($res['actg_paid_amount']) > $current_refund ? floatval($res['actg_paid_amount']) - $current_refund : 0;
                    $point = 0;
                    $smarty->assign('order_type', 'ws');
                }
                $fixed_refund = array(
                    'money' => $money,
                    'point' => $point
                );
                $smarty->assign('fixed_refund', $fixed_refund);
                $smarty->assign('type', $_REQUEST['accounting'] && $res['order_type'] != OrderController::DB_ORDER_TYPE_WHOLESALE ? self::REFUND_TYPE_NORMAL : self::REFUND_TYPE_WHOLE);
            } else {
                $smarty->assign('type', self::REFUND_TYPE_COMPENSATE);
            }
            $smarty->assign('order_sn', $order_sn);
            $smarty->assign('order_id', $order_id);
            $smarty->assign('order_info', $res);
            $smarty->assign('requester_id', $_SESSION["admin_id"]);
        }
    }

    /**
     * Get order_sn by order_id
     * 
     * @param int $order_id order id
     * 
     * @return int order_sn
     */
    public function getOrderSnById($order_id)
    {
        if (!empty($order_id)) {
            $order_sn = $GLOBALS['db']->getOne("SELECT order_sn FROM " . $GLOBALS['ecs']->table('order_info') . " WHERE order_id = $order_id");
        }
        return $order_sn;
    }
    
    /**
     * Get order_id by order_sn
     * 
     * @param int $order_sn order_sn
     * 
     * @return int order_id
     */
    public function getOrderIdBySn($order_sn)
    {
        if (!empty($order_sn)) {
            $order_id = $GLOBALS['db']->getOne("SELECT order_id FROM " . $GLOBALS['ecs']->table('order_info') . " WHERE order_sn = $order_sn");
        }
        return $order_id;
    }

    /**
     * Retrieve refund requests
     * 
     * @return array requests
     */
    public function getRefundRequests($limit = true)
    {
        $_REQUEST['sort_by']      = empty($_REQUEST['sort_by']) ? 'rec_id' : trim($_REQUEST['sort_by']);
        $_REQUEST['sort_order']   = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
        $_REQUEST['order_sn']     = empty($_REQUEST['order_sn']) ? '' : trim($_REQUEST['order_sn']);
        $_REQUEST['remark']       = empty($_REQUEST['remark']) ? '' : trim($_REQUEST['remark']);
        $_REQUEST['start']        = empty($_REQUEST['start']) ? '0' : trim($_REQUEST['start']);
        $_REQUEST['page_size']    = empty($_REQUEST['page_size']) ? '10' : trim($_REQUEST['page_size']);

        $sql = "SELECT count(*) FROM " . $GLOBALS['ecs']->table('refund_requests');
        $record_count = $GLOBALS['db']->getOne($sql);

        $bottom_finished = self::GROUP_BY_STATUS . " ASC, ";

        /* 查询 */
        $sql = "SELECT rr.*, IFNULL(NULLIF(rr.requester,''), au.user_name) AS requester, IFNULL(NULLIF(rr.user_name,''), u.user_name) AS user_name, IFNULL(NULLIF(rr.order_sn,''), oi.order_sn) AS order_sn, oi.money_paid AS order_amount, IFNULL(NULLIF(rr.operator,''), au2.user_name) AS operator, p.is_online_pay, p.pay_name";
        $sql.= " FROM " . $GLOBALS['ecs']->table('refund_requests') . " rr " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('admin_user') . " au ON au.user_id = rr.requester_id " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('admin_user') . " au2 ON au2.user_id = rr.operator_id " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('users') . " u ON u.user_id = rr.user_id " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('order_info') . " oi ON oi.order_id = rr.order_id " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('payment') . " p ON oi.pay_id = p.pay_id ";
        $sql.= empty($_REQUEST['remark']) ? "" : " WHERE rr.requester_remark LIKE '%$_REQUEST[remark]%' OR rr.operator_remark LIKE '%$_REQUEST[remark]%'";
        $sql.= empty($_REQUEST['order_sn']) ? "" : " HAVING order_sn LIKE '%$_REQUEST[order_sn]%'";
        $sql.= " ORDER BY " . $bottom_finished . $_REQUEST['sort_by'] . ' ' . $_REQUEST['sort_order'];
        if($limit) $sql.= " LIMIT " . $_REQUEST['start'] . "," . $_REQUEST['page_size'];
        $data = $GLOBALS['db']->getAll($sql);

        foreach ($data as $key => $row) {
            $data[$key]['request_date'] = local_date($GLOBALS['_CFG']['time_format'], $row['request_time']);
            $data[$key]['update_date'] = $row['update_time'] > 0 ? local_date($GLOBALS['_CFG']['time_format'], $row['update_time']) : "--";
            $data[$key]['refund_amount_formatted'] = price_format($row['refund_amount'], false);
            if (in_array($row['status'], self::REFUND_STATUS)) {
                $data[$key]['status_text'] = self::REFUND_STATUS_STRING[$row['status']][1];
            }
            $data[$key]['operator_remark'] = $row['operator_remark'] ? $row['operator_remark'] : 'N/A';
            $data[$key]['type_text'] = self::REFUND_TYPE[$row['refund_type']];
            $data[$key]['req_rem'] = $this->remarkDecode($row['requester_remark']);
            if (in_array($row['status'], [REFUND_PENDING, REFUND_PROCESS,REFUND_PROCESSED]) && $row['is_online_pay']) {
                $op = $GLOBALS['db']->getRow("SELECT payment_type_id, txn_id, actg_txn_id FROM `actg_order_payments` WHERE order_id = $row[order_id]");
                if (!empty($op['txn_id'])) {
                    $purchase_payment_id = $GLOBALS['db']->getOne(
                        "SELECT payment_method_id FROM `actg_account_payment_methods` apm " .
                        "LEFT JOIN `actg_account_transactions` t ON t.account_id = apm.account_id " .
                        "WHERE t.txn_id = $op[txn_id] AND apm.payment_type_id = $op[payment_type_id]"
                    );
                } elseif (!empty($op['actg_txn_id'])) {
                    $purchase_payment_id = $GLOBALS['db']->getOne(
                        "SELECT payment_method_id FROM `actg_account_payment_methods` apm " .
                        "LEFT JOIN `actg_accounts` a ON a.account_id = apm.account_id " .
                        "LEFT JOIN `actg_accounting_transactions` t ON t.actg_type_id = a.actg_type_id " .
                        "WHERE t.actg_txn_id = $op[actg_txn_id] AND apm.payment_type_id = $op[payment_type_id]"
                    );
                }
                $data[$key]['purchase_payment_id'] = $purchase_payment_id;
            }
        }
        $arr = array('data' => $data, 'record_count' => $record_count);
    
        return $arr;
    }

    /**
     * Retrieve not processed refund requests
     * 
     * @return array requests
     */
    public function getOrderRefundRequests()
    {
        $orderController = new OrderController();

        $filter['sort_by']      = empty($_REQUEST['sort_by']) ? 'order_sn' : trim($_REQUEST['sort_by']);
        $filter['sort_order']   = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
        $filter['page_size']    = empty($_REQUEST['page_size']) ? 200 : $_REQUEST['page_size'];
        $filter['sort_by']      = empty($_REQUEST['sort_by']) ? 'request_time' : $_REQUEST['sort_by'];
        $filter['sort_order']   = empty($_REQUEST['sort_order']) ? 'DESC' : $_REQUEST['sort_order'];
        $filter['start']        = empty($_REQUEST['start']) ? 0 : intval($_REQUEST['start']);
        $_REQUEST['order_sn']   = trim($_REQUEST['order_sn']);

        /* 查询 */
        $sql = "SELECT order_id, order_sn, pay_name, pay_id, " .
                "SUM(refund_amount) AS refund_amount, SUM(integral_amount) AS integral_amount, " .
                "MIN(tmp_status) AS overall_status, MAX(request_time) AS request_time FROM (" .
            "SELECT rr.order_id, rr.refund_amount, rr.integral_amount, rr.request_time, IFNULL(NULLIF(rr.order_sn,''), oi.order_sn) AS order_sn, " . self::GROUP_BY_STATUS . " AS tmp_status, CASE WHEN p.is_online_pay = 1 THEN p.pay_name ELSE '銀行轉帳' END AS pay_name, p.pay_id";
        $sql.= " FROM " . $GLOBALS['ecs']->table('refund_requests') . " rr " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('admin_user') . " au ON au.user_id = rr.requester_id " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('users') . " u ON u.user_id = rr.user_id " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('order_info') . " oi ON oi.order_id = rr.order_id " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('payment') . " p ON oi.pay_id = p.pay_id";
                $sql .= !empty($_REQUEST['order_sn']) ? " WHERE oi.order_sn LIKE '%$_REQUEST[order_sn]%' OR rr.order_sn LIKE '%$_REQUEST[order_sn]%'" : "";
        $sql .= " HAVING tmp_status = 0) tmp GROUP BY order_id";
        $sql .= " ORDER BY overall_status ASC, " . $filter['sort_by'] . ' ' . $filter['sort_order'];
        $sql .= " LIMIT " . $filter['start'] . "," . $filter['page_size'];
        $data = $GLOBALS['db']->getAll($sql);
        
        foreach ($data as $key => $row) {
            $order = order_info($row['order_id']);
            $data[$key]['money_paid'] = $order['money_paid'];

            $sql_rem = "SELECT requester_remark FROM " . $GLOBALS['ecs']->table('refund_requests') . 
                        " WHERE order_id = $row[order_id] AND status " . db_create_in(array(REFUND_PENDING, REFUND_PROCESS,REFUND_PROCESSED)) .
                        " ORDER BY rec_id DESC LIMIT 1";
            $data[$key]['request_remark'] = $this->remarkDecode($GLOBALS['db']->getOne($sql_rem));
            $data[$key]['request_date'] = local_date($GLOBALS['_CFG']['time_format'], $row['request_time']);
            $data[$key]['update_date'] = $row['update_time'] > 0 ? local_date($GLOBALS['_CFG']['time_format'], $row['update_time']) : "--";
            $data[$key]['refund_amount_formatted'] = price_format($row['refund_amount'], false);
            $btn = [
                'icon_class' => 'fa fa-repeat',
                'link'       => 'javascript:;',
                'title'      => '退款處理',
                'label'      => '退款處理',
                'onclick'    => "retrieveRequest($row[order_sn])",
                'btn_class'  => 'btn-success',
                'btn_size'   => 'btn'
            ];
            $data[$key]['_action'] = $this->generateCrudActionBtn(0, 'id', null, [], ['退款處理' => $btn]);
        }

        $count = $GLOBALS['db']->getOne(
            "SELECT COUNT(DISTINCT rr.order_id) FROM " . $GLOBALS['ecs']->table('refund_requests') . " rr " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('order_info') . " oi ON oi.order_id = rr.order_id " .
            "WHERE status " . db_create_in(array(REFUND_PENDING, REFUND_PROCESS,REFUND_PROCESSED)) . (!empty($_REQUEST['order_sn']) ? " AND (oi.order_sn LIKE '%$_REQUEST[order_sn]%' OR rr.order_sn LIKE '%$_REQUEST[order_sn]%')" : ""));

        $arr = array('data' => $data, 'record_count' => $count);

        return $arr;
    }

    /**
     * Get month report
     * 
     * @return array list or records
     */
    public function getMonthReport()
    {
        $year = empty($_REQUEST['expense_year']) ? local_date("Y") : $_REQUEST['expense_year'];
        $month = empty($_REQUEST['expense_month']) ? local_date("m") : $_REQUEST['expense_month'];
        $day = local_date("t", local_strtotime("$year-$month-01"));
        $sql = "SELECT ROUND(SUM(amount) * -1, 2) total_amount, account_name, a.account_id FROM actg_account_transactions t LEFT JOIN actg_accounts a ON a.account_id = t.account_id LEFT JOIN actg_transaction_types tt ON t.txn_type_id = tt.txn_type_id WHERE txn_type_name = 'Refund' AND (expense_year = $year AND expense_month = $month) GROUP BY a.account_id";
        $list = $GLOBALS['db']->getAll($sql);
        foreach ($list as $key => $row) {
            $sql = "SELECT txn_id, remark, ROUND(amount * -1, 2) amount FROM actg_account_transactions t LEFT JOIN actg_accounts a ON a.account_id = t.account_id LEFT JOIN actg_transaction_types tt ON t.txn_type_id = tt.txn_type_id WHERE txn_type_name = 'Refund' AND (expense_year = $year AND expense_month = $month) AND a.account_id = $row[account_id]";
            $list[$key]['txns'] = $GLOBALS['db']->getAll($sql);
        }
        $GLOBALS['smarty']->assign('expense_year', $year);
        $GLOBALS['smarty']->assign('expense_month', $month);
        return $list;
    }

    /** 
     * Insert refund request
     * 
     * @return int record insert id
     */
    public function insertNewRequest()
    {
        global $ecs, $db;

        // Get requester info
        $requester_id = empty($_POST['requester_id']) ? 0 : intval($_POST['requester_id']);
        
        if (empty($requester_id)) {
            $_POST['crm'] ? make_json_error('必須輸入申請人') : sys_msg('必須輸入申請人');
        }
        $requester = $this->getUserNameById($requester_id);

        // Check refund type
        $refund_type = trim($_POST['reason']) == self::REFUND_TYPE_COMPENSATE ? REFUND_COMPENSATE : (trim($_POST['reason']) == self::REFUND_TYPE_WHOLE ? REFUND_WHOLE_ORDER : REFUND_NORMAL);

        // Get order info
        $order_id = empty($_POST['order_id']) ? '' : intval($_POST['order_id']);
        if (empty($order_id)) {
            $_POST['crm'] ? make_json_error('必須選擇訂單') : sys_msg('必須選擇訂單');
        } else {
            $order = order_info($order_id);
            if (empty($order)) {
                $_POST['crm'] ? make_json_error('找不到該訂單') : sys_msg('找不到該訂單');
            }
            // Default reject any refund with order status = refunded / waiting whole order refund / cancelled / splited
            // Exception 1: Force refund
            // Exception 2: Compensate refund
            // Exception 3: Partial refund for splited order
            if (in_array($order['order_status'], array(OS_INVALID, OS_WAITING_REFUND, OS_CANCELED, OS_SPLITED, OS_SPLITING_PART)) && empty($_POST['accounting']) && $refund_type != REFUND_COMPENSATE) {
                if (!($refund_type == REFUND_NORMAL && in_array($order['order_status'], array(OS_SPLITED, OS_SPLITING_PART)))) {
                    $_POST['crm'] ? make_json_error('本訂單無法申請退款') : sys_msg('本訂單無法申請退款');
                }
            }
        }

        // Get user info
        $user_id = empty($_POST['user_id']) ? 0 : intval($_POST['user_id']);
        $user_name = empty($_POST['user_name']) ? "" : trim($_POST['user_name']);
        if (empty($user_id) && !empty($user_name)) {
            $user_id = $db->getOne("SELECT `user_id` FROM " . $ecs->table('users') . " WHERE `user_name` = '$user_name'");
        }
        if (empty($user_id) && !empty($order['user_id'])) {
            $_POST['crm'] ? make_json_error('必須選擇會員') : sys_msg('必須選擇會員');
        }
        if (empty($user_name) && !empty($order['user_id'])) {
            if (empty($user_name = $this->getUserNameById($user_id, $table = 'users'))) {
                $_POST['crm'] ? make_json_error('找不到該會員') : sys_msg('找不到該會員');
            }
        }

        // Get refund info
        $refund_amount = empty($_POST['refund_amount']) ? 0.0 : doubleval($_POST['refund_amount']);
        $integral_amount = empty($_POST['integral_amount']) ? 0 : intval($_POST['integral_amount']);
        if (empty($refund_amount) && empty($integral_amount)) {
            $_POST['crm'] ? make_json_error('必須輸入退款金額或積分') : sys_msg('必須輸入退款金額或積分');
        }

        // Check max refund amount
        if ($refund_type != REFUND_COMPENSATE && !$_POST['accounting']) {
            $current_refund = $this->currentProcessRefundAmount($order_id);
            // Wholesale order depends on actg_paid_amount
            if ($order['order_type'] == OrderController::DB_ORDER_TYPE_WHOLESALE) {
                if ($order['actg_paid_amount'] <= 0) {
                    $_POST['crm'] ? make_json_error('此訂單無法退款') : sys_msg('此訂單無法退款');
                }
                $max_refund = floatval($order['actg_paid_amount']) - $current_refund;
                if ($refund_type != REFUND_WHOLE_ORDER) {
                    $_POST['crm'] ? make_json_error('批發訂單無法部分退款及退還積分') : sys_msg('批發訂單無法部分退款及退還積分');
                } else {
                    if ($max_refund != $refund_amount) {
                        $_POST['crm'] ? make_json_error('退款金額或積分不等於已付金額') : sys_msg('退款金額或積分不等於已付金額');
                    }
                }
            } else {
                if ($order['money_paid'] <= 0 && $order['integral'] <= 0) {
                    $_POST['crm'] ? make_json_error('此訂單無法退款') : sys_msg('此訂單無法退款');
                }
                $max_refund = floatval($order['money_paid']) - $current_refund;
                if ($refund_type != REFUND_WHOLE_ORDER) {
                    $max_refund = $current_refund - floatval($order['order_amount']);
                    if (($order['money_paid'] > 0 && ($max_refund < $refund_amount || $max_refund < value_of_integral($integral_amount))) || ($order['money_paid'] == 0 && $order['integral'] > 0 && $order['integral'] < $integral_amount)) {
                        $_POST['crm'] ? make_json_error('退款金額或積分超過上限') : sys_msg('退款金額或積分超過上限');
                    }
                } else {
                    if (($order['money_paid'] > 0 && ($max_refund != $refund_amount && $max_refund != value_of_integral($integral_amount))) || ($order['money_paid'] == 0 && $order['integral'] > 0 && $order['integral'] != $integral_amount)) {
                        $_POST['crm'] ? make_json_error('退款金額或積分不等於已付金額') : sys_msg('退款金額或積分不等於已付金額');
                    }
                }
            }
        }

        // Insert accounting transactions
        $param = [
            'sales_order_id'    => $order_id,
            'order_sn'          => $order['order_sn'],
            'amount'            => $refund_amount > 0 ? $refund_amount : value_of_integral($integral_amount),
            'integral_amount'   => $integral_amount,
        ];
        $accountingController = new AccountingController();
        $actg_txn_id = $accountingController->insertRefundApplyTransactions($param, trim($_POST['reason']) == self::REFUND_TYPE_COMPENSATE ? 1 : 0);
        if (!$actg_txn_id) {
            $_POST['crm'] ? make_json_error('建立會計紀錄失敗') : sys_msg('建立會計紀錄失敗');
        }

        // Set requester remark
        $requester_remark = $this->remarkEncode($refund_amount);

        // Check refund type
        $refund_type = trim($_POST['reason']) == self::REFUND_TYPE_COMPENSATE ? REFUND_COMPENSATE : ($_POST['accounting'] ? REFUND_CUSTOM : (trim($_POST['reason']) == self::REFUND_TYPE_WHOLE ? REFUND_WHOLE_ORDER : REFUND_NORMAL));

        // Insert order action log
        $log = trim($_POST['reason']) == self::REFUND_TYPE_COMPENSATE ? "已申請售後補償: $integral_amount 積分" : (($_POST['accounting'] ? "手動申請" : "退款 原已付金額: ".($order['order_type'] == OrderController::DB_ORDER_TYPE_WHOLESALE ? $order['actg_paid_amount'] : $order['money_paid']). ", 已申請") . ($refund_amount > 0 ? "退款: $$refund_amount" : ($integral_amount > 0 ? "退還積分: $integral_amount" : "")));
        order_action($order['order_sn'], $refund_type != REFUND_WHOLE_ORDER ? $order['order_status'] : OS_WAITING_REFUND, $order['shipping_status'], $order['pay_status'], $log, $requester);

        // Update order status
        if ($refund_type == REFUND_WHOLE_ORDER) {
            update_order($order_id, array('order_status' => OS_WAITING_REFUND));
        }

        if (isset($_POST['transaction'])) {
            $transaction_ids = [];
            foreach ($_POST['transaction'] as $transaction_info) {
                $info = explode(' ',$transaction_info);

                // find if the transaction id is real
                $temp_tans_id = trim($info[0]);
                $sql = "select count(*) from ".$ecs->table('order_payment_transaction')." where gateway_transaction_id = '".$temp_tans_id."' ";
                $count = $db->getOne($sql);
                
                if ($count > 0) {
                    $transaction_ids[] = trim($info[0]);
                }
            }

            $order_payment_transaction_ids = implode(',',$transaction_ids);    
        }

        // Insert refund request
        $req = array(
            'requester_id' => $requester_id,
            'request_time' => gmtime(),
            'user_id' => $user_id,
            'order_id' => $order_id,
            'refund_amount' => $refund_amount,
            'integral_amount' => $integral_amount,
            'requester_remark' => $requester_remark,
            'status' => REFUND_PENDING,
            'operator_id' => 0,
            'operator_remark' => '',
            'refund_type' => $refund_type,
            'apply_txn_id' => $actg_txn_id,
            'order_payment_transaction_ids' => $order_payment_transaction_ids
        );
    
        $db->autoExecute($ecs->table('refund_requests'), $req, 'INSERT');
        $refund_id = $db->insert_id();
        $db->query("UPDATE `actg_accounting_transactions` SET refund_id = $refund_id WHERE actg_txn_id = $actg_txn_id");

        return $refund_id;
    }

    /**
     * Reject all proccessing refund
     * 
     * @param int $order_id order id
     * 
     * @return array rejected request id
     */
    public function rejectProcessingRefund($order_id)
    {
        $order = order_info($order_id);
        // Check processing requests
        $req = $GLOBALS['db']->getCol("SELECT rec_id FROM " . $GLOBALS['ecs']->table('refund_requests') . " WHERE order_id = $order_id AND status " . db_create_in(array(REFUND_PENDING, REFUND_PROCESS,REFUND_PROCESSED)) . " AND refund_type != " . REFUND_COMPENSATE);
        if (!empty($req)) {
            $processed = [];
            foreach ($req as $rec_id) {
                $accountingController = new AccountingController();
                $complete_txn_id = $accountingController->insertRefundUpdateTransactions(['rec_id' => $rec_id], 1);
                if ($complete_txn_id) {
                    $GLOBALS['db']->query("UPDATE " . $GLOBALS['ecs']->table('refund_requests') . " SET update_time = " . gmtime() . ", status = " . REFUND_REJECT . ", operator_id = 1, complete_txn_id = $complete_txn_id WHERE rec_id = $rec_id ");
                    $processed[] = $rec_id;
                }
            }
            return $processed;
        }
        return array();
    }

    /**
     * Edit request status
     * 
     * @return json_response status
     */
    public function editStatus()
    {
        global $db, $ecs;
        $rec_id = intval($_POST['id']);
        $value = trim($_POST['val']);
        $operator_id = empty($_POST['oid']) ? (empty($_SESSION['admin_id']) ? 0 : $_SESSION['admin_id']) : intval($_POST['oid']);

        if (!in_array($value, self::REFUND_STATUS)) {
            make_json_error('您輸入了一個非法的狀態');
        } else {
            $arr = array();
            
            $sql = "SELECT rr.*, oi.order_status, oi.shipping_status, oi.pay_status, oi.order_sn " .
                    "FROM " . $ecs->table('refund_requests') . " as rr " .
                    "LEFT JOIN " . $ecs->table('order_info') . " as oi ON rr.order_id = oi.order_id " .
                    "WHERE rr.`rec_id` = '" . $rec_id . "'";
            $res = $db->getRow($sql);
            
            // No change, early exit here
            if ($value == $res['status']) {
                make_json_result($value, '', $arr);
            }
    
            if (empty($res['operator']) && empty($res['operator_id'])) {
                if (empty($operator_id)) {
                    make_json_error('修改狀態前，請先填寫跟進人');
                } else {
                    $db->query("UPDATE " . $ecs->table('refund_requests') . " SET `operator_id` = $operator_id WHERE `rec_id` = '$rec_id'");
                }
            }

            // Update status
            if ($value == REFUND_FINISH || $value == REFUND_REJECT) {
                if ($value == REFUND_REJECT) {
                    $accountingController = new AccountingController();
                    $complete_txn_id = $accountingController->insertRefundUpdateTransactions(['rec_id' => $rec_id], 1);
                }
                $db->query("UPDATE " . $ecs->table('refund_requests') . " SET `status` = " . $value . ", `update_time` = " . gmtime(). (empty($complete_txn_id) ? "" : ", complete_txn_id = $complete_txn_id") . " WHERE `rec_id` = '" . $rec_id . "'");
            } else {
                $db->query("UPDATE " . $ecs->table('refund_requests') . " SET `status` = " . $value . ", `update_time` = 0 WHERE `rec_id` = '" . $rec_id . "'");
            }

            // Add order log if needed
            $operator = $db->getOne("SELECT user_name FROM " . $ecs->table('admin_user') . " WHERE user_id = " . (empty($operator_id) ? $res['operator_id'] : $operator_id));
            if (!empty($_POST['log'])) {
                order_action($res['order_sn'], $res['order_status'], $res['shipping_status'], $res['pay_status'], trim($_POST['log']), $operator);
            }
            // Add order log for reject request
            if ($value == REFUND_REJECT) {
                include_once ROOT_PATH . "languages/zh_tw/admin/order.php";
                if ($res['refund_type'] == REFUND_COMPENSATE) {
                    order_action($res['order_sn'], $res['order_status'], $res['shipping_status'], $res['pay_status'], "[系統備註] 取消補償積分 #$res[rec_id] ($res[integral_amount] 積分)", $operator);
                } elseif ($res['refund_type'] == REFUND_WHOLE_ORDER) {
                    update_order($res['order_id'], array('order_status' => OS_CONFIRMED));
                    order_action($res['order_sn'], OS_CONFIRMED, $res['shipping_status'], $res['pay_status'], "[系統備註] 取消全單退款申請 #$res[rec_id]<br>訂單狀態修改: 由 <strong>" . $_LANG['os'][OS_WAITING_REFUND] . "</strong> 改成 <strong>" . $_LANG['os'][OS_CONFIRMED], $operator);
                } else {
                    order_action($res['order_sn'], $res['order_status'], $res['shipping_status'], $res['pay_status'], "[系統備註] 取消退款申請 #$res[rec_id] (" . ($res['refund_amount'] > 0 ? "$$res[refund_amount])" : "$res[integral_amount] 積分)"), $operator);
                }
            }
            make_json_result($value, '', $arr);
        }
    }

    /**
     * Process refund
     * 
     * @return json_response true
     */
    function orderRefundProcess()
    {
        global $ecs, $db, $actg;
        $rec_id = intval($_POST['rec_id']);
        $payment_method_id = intval($_POST['payment_method_id']);
        $whole_order_refund = intval($_POST['whole_order_refund']);
        $operator_remark = !empty($_POST['operator_remark']) ? $_POST['operator_remark'] : '';
        $refund_route = !empty($_POST['refund_route']) ? $_POST['refund_route'] : 'manual'; // Set Manual refund as default value in case of not set
        
        $sql = "SELECT * " .
                "FROM " . $ecs->table('refund_requests') .
                "WHERE `rec_id` = '" . $rec_id . "'";
        $res = $db->getRow($sql);
        $operator_id = empty($_POST['oid']) ? (empty($res['operator_id']) ? 0 : $res['operator_id']) : intval($_POST['oid']);

        if (empty($res)) {
            make_json_error('找不到該退款申請');
        }

        $order = order_info($res['order_id']);

        if (empty($order)) {
            make_json_error('找不到該訂單');
        }
        
        // get refund transaction id
        if (!empty($res['order_payment_transaction_ids'])) {
            $order_payment_transaction_ids = explode(',',$res['order_payment_transaction_ids']);

            $total_refundable_amount = 0;
            $total_refunded_amount = 0;
            foreach ($order_payment_transaction_ids as $order_payment_transaction_id) {
                $sql = "select * from ".$ecs->table('order_payment_transaction')." opt where opt.gateway_transaction_id = '".$order_payment_transaction_id."' ";
                $order_payment_transaction_info = $db->getRow($sql);
                //dd($order_payment_transaction_info);
//                $total_refunded_amount += $order_payment_transaction_info['refunded_amount'];
//                $total_refundable_amount += $order_payment_transaction_info['amount'] - $order_payment_transaction_info['refunded_amount'];
                // Apply BCMath for calculation
                $total_refunded_amount = bcadd($total_refunded_amount, $order_payment_transaction_info['refunded_amount'], 2);
                $total_refundable_amount = bcadd($total_refundable_amount, $order_payment_transaction_info['amount'], 2);
                $total_refundable_amount = bcsub($total_refundable_amount, $order_payment_transaction_info['refunded_amount'], 2);
            }

            //check if the amount is enough to refund
            if (bccomp($res['refund_amount'], $total_refundable_amount, 2) === 1) {
                make_json_error("不夠剩餘金額,已經Refund $$total_refunded_amount,可Refund $$total_refundable_amount");
            }
            
            //  call api refund
            $total_refund_amount = $res['refund_amount'];
            $remain_refund_amount = $res['refund_amount'];
            $calculated_refund_ar = [];
            foreach ($order_payment_transaction_ids as $order_payment_transaction_id) {
                $sql = "select opt.*, p.pay_code, opt.amount - opt.refunded_amount as refundable_amount from ".$ecs->table('order_payment_transaction')." opt left join ".$ecs->table('payment')." p on (opt.pay_id = p.pay_id) where opt.gateway_transaction_id = '".$order_payment_transaction_id."' ";
                $order_payment_transaction_info = $db->getRow($sql);
                //dd($order_payment_transaction_info);
                $calculated_refund_amount = 0;

                if ($remain_refund_amount > 0) {
                    $diff_amount = $remain_refund_amount - $order_payment_transaction_info['refundable_amount'];
                    if ($diff_amount > 0){
                        // not enough to refund
                        $calculated_refund_ar[] = array (
                            'calculated_refund_amount'=>$order_payment_transaction_info['refundable_amount'],
                            'calculated_refund_tnx'=> $order_payment_transaction_id,
                            'order_id' => $order_payment_transaction_info['order_id'],
                            'id' => $order_payment_transaction_info['id'],

                        );
                        $remain_refund_amount = $diff_amount;
                    } else {
                        //enough to refund
                        $calculated_refund_ar[] = array (
                            'calculated_refund_amount'=>$remain_refund_amount,
                            'calculated_refund_tnx'=> $order_payment_transaction_id,
                            'order_id' => $order_payment_transaction_info['order_id'],
                            'id' => $order_payment_transaction_info['id'],
                        );
                        $remain_refund_amount = 0;
                    }
                }
            }

            if (isset($order_payment_transaction_info['pay_code'])) {
                // find the payment module for refund
                // if the status is 5, the refund amount has been refunded from Payment Platform. that mains we don't need to call api refund.
                if ($res['status'] != REFUND_PROCESSED && $res['status'] != REFUND_PROCESS){
                    if (file_exists(ROOT_PATH . 'includes/modules/payment/'.$order_payment_transaction_info['pay_code'].'.php')) {
                        require_once(ROOT_PATH . 'includes/modules/payment/'.$order_payment_transaction_info['pay_code'].'.php');
                        $refund_result_error = false;
                        if ($refund_route == 'api') { // only route set to api will perform refund
                            if (method_exists($order_payment_transaction_info['pay_code'], 'do_refund')) {
                                $db->query("UPDATE " . $ecs->table('refund_requests') . " SET `status` = " . REFUND_PROCESS . ", `operator_remark` = '$operator_remark',`update_time` = " . gmtime() . " WHERE `rec_id` = '$rec_id'");
                                foreach ($calculated_refund_ar as $calculated_refund) {
                                    if ($calculated_refund['calculated_refund_amount'] > 0) {
                                        $refund_result = call_user_func(array($order_payment_transaction_info['pay_code'], 'do_refund'), $calculated_refund['calculated_refund_tnx'], $calculated_refund['calculated_refund_amount']);
                                        if ($refund_result == true) {
                                            // do something
                                        } else {
                                            $refund_result_error = true;
                                        }
                                    }
                                }
                                // update the request status
                                if ($refund_result_error == true) {
                                    make_json_error('退款發生錯誤,請聯絡相關技術同事');
                                    exit;
                                } else {
                                    $db->query("UPDATE " . $ecs->table('refund_requests') . " SET `status` = " . REFUND_PROCESSED . ", `operator_remark` = '$operator_remark',`update_time` = " . gmtime() . " WHERE `rec_id` = '$rec_id'");
                                }
                            }
                        }
                    }
                }

                foreach ($calculated_refund_ar as $calculated_refund) {
                    // update the refunded amount
                    $sql = 'update '.$ecs->table('order_payment_transaction').' set refunded_amount = refunded_amount + "'.$calculated_refund['calculated_refund_amount'].'" where order_id = '.$res['order_id'].' and id = "'.$calculated_refund['id'].'" ';
                    $db->query($sql);
                }
            }
        }

        // Update order (new flow)
        if ($whole_order_refund || $res['refund_type'] == REFUND_WHOLE_ORDER) {
            include_once ROOT_PATH . 'includes/lib_order.php';
            
            // Set order to OS_INVALID (已退款)
            update_order(
                $res['order_id'], array(
                    'order_status'    => OS_INVALID,
                    'confirm_time'    => 0,
                    'pay_status'      => PS_UNPAYED,
                    'pay_time'        => 0,
                    'money_paid'      => 0,
                    'integral'        => 0,
                    'integral_money'  => 0,
                    'order_amount'    => $order['order_amount'] + $order['money_paid'],
                    'shipping_status' => SS_UNSHIPPED,
                    'shipping_time'   => 0
                )
            );
        } elseif (!empty($res['requester_id'])) {
            $cfg = load_config(true);
            $ratio = floatval($cfg['integral_scale']);
            include_once ROOT_PATH . 'includes/lib_order.php';
            // Deduct money paid for new flow (old flow already deducted)
            if ($order['order_type'] != OrderController::DB_ORDER_TYPE_WHOLESALE)
            update_order(
                $res['order_id'], array(
                    'money_paid'      => $order['money_paid'] - $res['refund_amount'] - ($res['integral_amount'] / 100 * $ratio),
                    'order_amount'    => $order['order_amount'] + $res['refund_amount'] + ($res['integral_amount'] / 100 * $ratio)
                )
            );
        }

        // Accounting System - Refund (ignore B2B order)
        if ($res['refund_amount'] > 0 && $order['order_type'] != OrderController::DB_ORDER_TYPE_WHOLESALE) {
            include_once ROOT_PATH . ADMIN_PATH . '/includes/lib_accounting.php';
            $GLOBALS['actgHooks']->orderRefunded($res['order_id'], $res['refund_amount'], $payment_method_id, '', $rec_id);

            // Return 餘額 積分 紅包 (actually only 積分 is used by YOHO)
            OrderController::returnUserSurplusIntegralBonus($order);
        } elseif ($res['refund_amount'] > 0 && $order['order_type'] == OrderController::DB_ORDER_TYPE_WHOLESALE) {
            // Handle B2B order here
            update_order(
                $res['order_id'], array(
                    'fully_paid'        => 0,
                    'actg_paid_amount'  => bcsub($order['actg_paid_amount'], $res['refund_amount'], 2)
                )
            );
            include_once ROOT_PATH . ADMIN_PATH . '/includes/lib_accounting.php';
            $pay_method = $GLOBALS['actg']->getAccountPaymentMethod(compact('payment_method_id'));
            $accountingController = new AccountingController();
            $refundTxn = [
				'account_id' 		=> $pay_method['account_id'],
				'rec_id'            => $rec_id,
                'sales_order_id'    => $res['order_id'],
                'order_sn'          => $order['order_sn'],
                'amount'            => $res['refund_amount'],
            ];
            $accountingController->insertRefundUpdateTransactions($refundTxn, 2, 0);
        }
        
        // Record the order changes
        $operator = !empty($operator_id) ? $db->getOne("SELECT user_name FROM " .$ecs->table('admin_user') . " WHERE user_id = $operator_id") : "admin";
        $new_order = order_info($order['order_id']);
        if ($whole_order_refund) {
            // Record the order changes
            order_action($new_order['order_sn'], $new_order['order_status'], $new_order['shipping_status'], $new_order['pay_status'], '[系統備註] 完成全單退款 #' . $res['rec_id'], $operator);
        } else {
            $log = "[系統備註] 完成" . ($res['refund_type'] == REFUND_CUSTOM ? "手動申請" : "") . "退款 #$res[rec_id]" . ($res['refund_amount'] > 0 ? " ($$res[refund_amount])" : "") . ($res['integral_amount'] > 0 ? " ($res[integral_amount] 積分)" : "");
            order_action($new_order['order_sn'], $new_order['order_status'], $new_order['shipping_status'], $new_order['pay_status'], $log, $operator);
        }
        
        // Update request status
        if (!empty($operator_id)) {
            $db->query("UPDATE " . $ecs->table('refund_requests') . " SET `operator_id` = $operator_id WHERE `rec_id` = $rec_id");
        }

        $account_payment_method = $actg->getAccountPaymentMethod(compact('payment_method_id'));
        $db->query("UPDATE " . $ecs->table('refund_requests') . " SET `status` = " . REFUND_FINISH . ", `operator_remark` = '$operator_remark',`update_time` = " . gmtime(). ", refund_payment_method_id = '$payment_method_id', refund_payment_method_name = '$account_payment_method[payment_type_name] - $account_payment_method[account_name]' WHERE `rec_id` = '$rec_id'");
        make_json_result(true);
    }

    /**
     * Get required info to process refund
     * 
     * @return void
     */
    function initRefundProcess()
    {
        global $ecs, $db;
        $order_sn = empty($_REQUEST['order_sn']) ? 0 : $_REQUEST['order_sn'];
        if (empty($order_sn)) {
            make_json_error('請選擇欲處理之訂單');
        }
        $list = $this->getRefundRequests(false);
        $data = $list['data'];
        $order_id = 0;
        $wait = [];
        $comp = [];
        $done = [];
        $is_whole_refund = false;
        $all_custom = true;
        $purchase_payment_ids = [];

        // Group request by type and status
        foreach ($data as $key => $row) {
            $order_id = $row['order_id'];
            $data[$key]['request_date'] = local_date($GLOBALS['_CFG']['time_format'], $row['request_time']);
            $data[$key]['update_date'] = $row['update_time'] > 0 ? local_date($GLOBALS['_CFG']['time_format'], $row['update_time']) : "--";
            if (in_array($row['status'], array(REFUND_FINISH, REFUND_REJECT))) {
                $done[] = $data[$key];
            } else {
                if ($row['refund_type'] == REFUND_COMPENSATE) {
                    $comp[] = $data[$key];
                } else {
                    $wait[] = $data[$key];
                    $is_whole_refund = $is_whole_refund || $row['refund_type'] == REFUND_WHOLE_ORDER;
                    $all_custom = $row['refund_type'] == REFUND_CUSTOM && $all_custom;
                    if (!in_array($row['purchase_payment_id'], $purchase_payment_ids)) {
                        $purchase_payment_ids[] = $row['purchase_payment_id'];
                    }
                }
            }
        }

        // Check if order can refund all money paid
        $order_status = $db->getRow("SELECT order_status, shipping_status, pay_status, money_paid, integral, order_type, actg_paid_amount FROM " . $ecs->table('order_info') . " WHERE order_sn = $order_sn");
        $allow_whole_order_refund = $order_status['shipping_status'] == SS_UNSHIPPED;
        $allow_refund = ((floatval($order_status['money_paid']) > 0 || (floatval($order_status['money_paid']) == 0 && floatval($order_status['integral']) > 0)) && !in_array($order_status['order_status'], array(OS_CANCELED, OS_INVALID)) && $order_status['pay_status'] == PS_PAYED) || ($order_status['order_type'] == OrderController::DB_ORDER_TYPE_WHOLESALE && floatval($order_status['actg_paid_amount']) > 0) || $all_custom;
        $allow_refund = true;
        // if refund status is refund processing, it doesnt allow to refund
        $sql = "select count(*) from ".$ecs->table('refund_requests')." where order_id = ".$order_id." and status = ".REFUND_PROCESS." ";
        $refund_processing_count = $db->getOne($sql);

        if ($refund_processing_count>0) {
            $allow_refund = false;
        }

        // have order_transaction_id, mean call api refund real time
        $sql = "select count(*) from ".$ecs->table('refund_requests')." where order_id = ".$order_id." and order_payment_transaction_ids !='' and status in (".REFUND_PENDING.",".REFUND_PROCESSED." ) ";
        
        // TODO: Make sure only corresponding payment module with api_refund = true will return true
        // Get Payment Info

        $order_payment_transactions_query = "SELECT DISTINCT pay_id FROM " . $ecs->table('order_payment_transaction') . " WHERE order_id='$order_id' and refunded_amount <= amount";
        $order_payment_transaction_query_result = $db->getCol($order_payment_transactions_query);

        $api_refund = false;
        $manual_api_refund_override = false;

        foreach($order_payment_transaction_query_result as $pay_id) {
            $payment = payment_info($pay_id);

            include_once(ROOT_PATH . 'includes/modules/payment/' . $payment['pay_code'] . '.php');

            $pay_obj    = new $payment['pay_code'];
            if ($api_refund === false && (isset($pay_obj->api_refund) && $pay_obj->api_refund == true)) {
                $api_refund = true;
                if ($api_refund === true && ($manual_api_refund_override === false && (isset($pay_obj->manual_api_refund_override) && $pay_obj->manual_api_refund_override === true))) {
                    $manual_api_refund_override = true;
                }
                break;
            }

        }

        $is_refund_by_api = ((($db->getOne($sql) > 0) && $api_refund == true) ? true : false );
        $is_refund_by_api_allow_manual_override = ((($db->getOne($sql) > 0) && $manual_api_refund_override === true) ? true : false );

        // Get all payment method, set default refund method
        include_once ROOT_PATH . ADMIN_PATH . '/includes/lib_accounting.php';
        $cfg = load_config(true);
        $payment_methods = $GLOBALS['actg']->getAccountPaymentMethods("`pay_code` NOT LIKE '%ebanking%' AND `is_hidden` = 0 AND (a.`account_id` = '$cfg[default_refund_account]' OR `active` = 1)", array('orderby' => 'payment_type_id'));
        foreach ($payment_methods as $key => $method) {
            if (count($purchase_payment_ids) == 1) {
                if ($method['payment_method_id'] == $purchase_payment_ids[0]) {
                    $payment_methods[$key]['is_default'] = 1;
                }
            } elseif ($method['account_id'] == $cfg['default_refund_account']) {
                $payment_methods[$key]['is_default'] = 1;
            }
        }

        // Get operable admin
        $operators = $this->getOperableAdmins();

        // Get order action
        $act_list = array();
        include_once ROOT_PATH . "languages/zh_tw/admin/order.php";
        $sql = "SELECT * FROM " . $ecs->table('order_action') . " WHERE order_id = '$order_id' ORDER BY log_time DESC,action_id DESC";
        $res = $db->query($sql);
        while ($row = $db->fetchRow($res)) {
            $row['order_status']    = $_LANG['os'][$row['order_status']];
            $row['pay_status']      = $_LANG['ps'][$row['pay_status']];
            $row['shipping_status'] = $_LANG['ss'][$row['shipping_status']];
            $row['action_time']     = local_date($GLOBALS['_CFG']['time_format'], $row['log_time']);
            $act_list[] = $row;
        }

        make_json_result(
            array(
                'done' => $done,
                'wait' => $wait,
                'comp' => $comp,
                'action' => $act_list,
                'operator' => $operators['operators'],
                'payment_methods' => $payment_methods,
                'allow_refund' => $allow_refund && count($purchase_payment_ids) <= 1,
                'allow_whole_order_refund' => $allow_whole_order_refund,
                'is_whole_refund' => $is_whole_refund,
                'is_refund_by_api' => $is_refund_by_api,
                'is_refund_by_api_allow_manual_override' => $is_refund_by_api_allow_manual_override,
                'self_id' => $_SESSION['admin_id']
            )
        );
    }

    /**
     * Retrieve all operable admins
     * 
     * @return array list of admin, is operator
     */
    public function getOperableAdmins()
    {
        $adminUserController = new AdminuserController();
        $operators = $adminUserController->getAdminsByPriv(AdminuserController::DB_ACTION_CODE_ASSIGNABLE_REFUND_PIC, "name", ['user_id' => 'value'], true, true);
        array_unshift($operators, array('name'=>'請選擇...','value'=>''));

        $is_operator = 0;
        if ($_SESSION['manage_cost'] || $_SESSION['is_accountant']) {
            $is_operator = 1;
        }
        foreach ($operators as $operator) {
            if ($operator['user_id'] == $_SESSION['user_id']) {
                $is_operator = 1;
            }
        }
        return array('operators' => $operators, 'is_operator' => $is_operator);
    }

    /**
     * Get username
     * 
     * @param int $user_id user id
     * @param int $table   table name
     * 
     * @return string username
     */
    public function getUserNameById($user_id, $table = 'admin_user')
    {
        if (!empty($user_id)) {
            return $GLOBALS['db']->getOne("SELECT `user_name` FROM " . $GLOBALS['ecs']->table($table) . " WHERE `user_id` = '$user_id'");
        }
        return "";
    }

    /**
     * Decode requester remark
     * 
     * @param string $remark raw requester remark
     * 
     * @return array
     */
    public function remarkDecode($remark)
    {
        if (preg_match("/\[p\](.*)\[\/p\]/", $remark)) {
            preg_match("/\[r\](.*)\[\/r\]/", $remark, $requester);
            $arr = array(
                'r' => empty($requester) ? '' : $requester[1],
            );
        } elseif (preg_match("/\[r\](.*)\[\/r\]/", $remark) || preg_match("/\[t\](.*)\[\/t\]/", $remark)) {
            preg_match("/\[r\](.*)\[\/r\]/", $remark, $requester);
            preg_match("/\[t\](.*)\[\/t\]/", $remark, $transaction);
            preg_match("/\[b\](.*)\[\/b\]/", $remark, $bank);
            preg_match("/\[a\](.*)\[\/a\]/", $remark, $ac);
            preg_match("/\[u\](.*)\[\/u\]/", $remark, $user);
            $arr = array(
                'r' => empty($requester) ? '' : $requester[1],
                't' => empty($transaction) ? '' : $transaction[1],
                'b' => empty($bank) ? '' : $bank[1],
                'a' => empty($ac) ? '' : $ac[1],
                'u' => empty($user) ? '' : $user[1],
            );
            // clear transaction if bank info exist
            if (!(empty($arr['a']) && empty($arr['u']) && empty($arr['t']))) {
                // $arr['t'] = '';
            }
            // not online pay and no input bank info
            if (empty($arr['b']) && empty($arr['a']) && empty($arr['u']) && empty($arr['t'])) {
                $arr['t'] = '銀行資料後補';
            }
        } else {
            $arr = array('r' => $remark);
        }
        $arr['f'] = trim($arr['r'] . " " . ((empty($arr['b']) && empty($arr['a']) && empty($arr['u'])) ? $arr['t'] : "$arr[b] $arr[a] $arr[u]"));
        return $arr;
    }

    /**
     * Encode requester remark
     * 
     * @param float $refund_amount money refund
     * 
     * @return string encoded remark
     */
    public function remarkEncode($refund_amount = -1)
    {
        $bank = empty($_POST['bank']) ? '' : trim($_POST['bank']);
        $bank_ac = empty($_POST['bank_ac']) ? '' : trim($_POST['bank_ac']);
        $bank_user = empty($_POST['bank_user']) ? '' : trim($_POST['bank_user']);
        $refund_amount = $refund_amount == -1 ? $GLOBALS['db']->getOne("SELECT refund_amount FROM " . $GLOBALS['ecs']->table('refund_requests') . " WHERE rec_id = $_POST[id]") : $refund_amount;
        // custom remark
        $requester_remark = "[r]" . (empty($_POST['requester_remark']) ? '' : trim($_POST['requester_remark'])) . "[/r]";
        if ($refund_amount > 0) {
            // money remark
            $requester_remark .= "[t]" . ($_POST['offline'] ? ($refund_amount > 0 ? (($_POST['later'] == 1 || (empty($bank) && empty($bank_ac) && empty($bank_user))) ? '銀行資料後補' : "[b]" . $bank . "[/b][a]" . $bank_ac . "[/a][u]" . $bank_user . "[/u]") : '') : (empty($_POST['transaction']) ? '' : (is_array($_POST['transaction']) ? trim(implode('###', $_POST['transaction'])) : $_POST['transaction']))) . "[/t]";
        } else {
            // integral remark
            $requester_remark .= "[p][/p]";
        }
        return $requester_remark;
    }

    public function updateOrderPaymentTransactionIds($rec_id)
    {
        global $db, $ecs;
        
        $transactions = explode("###",$_POST['transaction']);

        $transaction_ids = [];
        foreach ($transactions as $transaction) {
            $transaction_split = explode(" ",$transaction);

            $temp_tans_id = trim($transaction_split[0]);
            $sql = "select count(*) from ".$ecs->table('order_payment_transaction')." where gateway_transaction_id = '".$temp_tans_id."' ";
            $count = $db->getOne($sql);
            if ($count > 0) {
                $transaction_ids[] = $temp_tans_id ;
            }
        }

        // update order payment transactions
        $sql = "update ".$ecs->table('refund_requests')." set order_payment_transaction_ids = '".implode(',',$transaction_ids)."' where rec_id = ".$rec_id." ";
        $db->query($sql);
    }

    /**
     * Return online payment log
     * 
     * @param int $order_id order id
     * 
     * @return array online payment log
     */
    public function getOnlineTxnLog($order_id)
    {
        global $db, $ecs;

        // check order payment transaction log 
        $sql = "SELECT * FROM " . $ecs->table('order_payment_transaction') . " WHERE order_id = $order_id ";
        $result = $db->getAll($sql);

        $txn = [];
        if (!empty($result)) {
            foreach ($result as $txn_info) {
                $txn[] = array(
                    'txn_id' => $txn_info['gateway_transaction_id'],
                    'txn_amount' => $txn_info['currency_code'] . ' ' . $txn_info['amount'] . ( bccomp($txn_info['refunded_amount'], 0) == 0 ? '' : ' (Refunded: $'.$txn_info['refunded_amount'].')' ),
                    'refunded_amount' => $txn_info['refunded_amount'],
                );
            }
        } else {
            $sql = "SELECT action_note FROM " . $ecs->table('order_action') . " WHERE order_id = $order_id AND pay_status = ". PS_PAYED . " AND action_note LIKE '%HKD%'";
            $actions = $db->getCol($sql);
            foreach ($actions as $act) {
                $info = explode('（', $act);
                $txn[] = array(
                    'txn_id' => str_replace(' 交易編號', '', $info[0]),
                    'txn_amount' => str_replace('）', '', $info[1])
                );
            }
        }

        
        return $txn;
    }

    public function getAllRefundRecords($order_id) 
    {
        global $db, $ecs;

        $sql = 'select * from ' . $ecs->table('refund_requests') . ' where order_id=' . $order_id;

        $result = $db->getAll($sql);

        return $result;
    }

    public function markAllProcessingRefundRequestToProcessed($order_id)
    {
        global $db, $ecs;

        $db->query("UPDATE " . $ecs->table('refund_requests') . " SET `status` = " . REFUND_PROCESSED . ", `operator_remark` = concat_ws(', ', nullif(`operator_remark`, ''), 'Force Refund'),`update_time` = " . gmtime(). " WHERE `status` = " . REFUND_PROCESS . " AND `order_id` = " . $order_id);
    
        return $db->affected_rows(); 
    }
}