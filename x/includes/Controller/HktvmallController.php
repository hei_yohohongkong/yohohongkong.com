<?php
# @Author: Anthony
# @Date:   2017-05-16T15:54:55+08:00
# @Filename: HktvmallController.php

namespace Yoho\cms\Controller;
use Yoho\cms\Model;
use Yoho\includes;

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

class HktvmallController extends YohoBaseController
{
    
    const DB_ACTION_CODE_HKTVMALL = 'hktvmall';
    const DB_ACTION_CODE_MENU_HKTVMALL = 'menu_hktvmall';
    const DEFAULT_RATE = '10%';

	private $tablename='hktvmall';
	private $merchant_id='H5119001';
    
    /*------------------------------------------------------ */
    //-- GET HKTVMALL LIST
    /*------------------------------------------------------ */
    public function get_hktv_list()
    {
        global $db, $ecs;
        
        /* Filter */
        $_REQUEST['sort_by'] = empty($_REQUEST['sort_by']) ? 'created_at' : trim($_REQUEST['sort_by']);
        $_REQUEST['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
        $_REQUEST['search'] = empty($_REQUEST['search']) ? '' : trim($_REQUEST['search']);
        $where = (empty($_REQUEST['search']) ? "" : "WHERE g.goods_sn LIKE '%" . $_REQUEST['search'] . "%' OR g.goods_id LIKE '%" . $_REQUEST['search'] . "%' OR g.goods_name LIKE '%" . $_REQUEST['search'] . "%' OR h.hktv_sku LIKE '%" . $_REQUEST['search'] . "%' ");
        $sql = "SELECT count(*) FROM " . $ecs->table($this->tablename) . " as h LEFT JOIN " . $ecs->table('goods') . " as g ON g.goods_id = h.goods_id " . $where;
        $_REQUEST['record_count'] = $db->getOne($sql);

        $sql = "SELECT h.* , IFNULL(h.hktv_brand, '') AS brand_name , g.`goods_name`, g.`goods_sn`, g.`goods_thumb`, g.`market_price`, g.`shop_price` as goods_price,  g.`shipping_price`, g.is_shipping " .
        "FROM " . $ecs->table($this->tablename) . " as h " .
        "LEFT JOIN " . $ecs->table('goods') . " as g ON g.goods_id = h.goods_id " . $where .
        "ORDER BY " . $_REQUEST['sort_by'] . ' ' . $_REQUEST['sort_order'] . ' ' .
        "LIMIT " . $_REQUEST['start'] . "," . $_REQUEST['page_size'];
        $data = $db->getAll($sql);
        
        /* HKTVMALL format */
        foreach ($data as $key => $row) {
			$shipping_price = 0;
			if($row['is_shipping'] == 1)$shipping_price = 0;
			elseif ($row['shipping_price'] > 0) $shipping_price = $row['shipping_price'];
			elseif ($row['hktv_price'] < 1000) $shipping_price = 33;
			$row['hktv_price'] = $row['hktv_price'] + $shipping_price;
            $hktv_price_formatted = price_format($row['hktv_price'], false);
			$data[$key]['hktv_price_formatted'] = $hktv_price_formatted ."(運費$$shipping_price)";
			$row['market_price'] = $row['market_price'] + $shipping_price;
			$market_price_formatted = price_format($row['market_price'], false);
			$data[$key]['market_price_formatted'] = $market_price_formatted ."(運費$$shipping_price)";
			$data[$key]['goods_price_formatted'] = price_format($row['goods_price'], false);
            $data[$key]['created_at_formatted'] = local_date($GLOBALS['_CFG']['date_format'], $row['created_at']);
        }
        $arr = array('data' => $data, 'record_count' => $_REQUEST['record_count']);
        return $arr;
    }

	/**
	 * @param array $assign
	 * @param string $template
	 */
	public function listAction($assign = [], $template = 'hktvmall_list.htm')
	{
		global $ecs, $_LANG, $db;
		require_once ROOT_PATH . 'includes/lib_order.php';
		require_once ROOT_PATH . 'includes/lib_howang.php';
		/* Check admin priv */
		admin_priv($this::DB_ACTION_CODE_HKTVMALL);

		/* Set title */
		$assign['ur_here'] = $_LANG['hktvmall'];

		/* Get updated price HKTV product count */
		$sql = "SELECT count(h.hktv_id) ".
			"FROM ". $ecs->table('hktvmall') ."as h ".
			"LEFT JOIN " . $ecs->table('goods') . " AS g ON g.goods_id = h.goods_id ".
			"WHERE  h.hktv_price <> g.shop_price ";
		$update_count = $db->getOne($sql);

		/* Get out of stock HKTV product count */
		$sql = "SELECT count(*) FROM ". $ecs->table('hktvmall') ."as h ".
			" LEFT JOIN (SELECT  h.goods_id, ".hw_goods_number_subquery('g.', 'real_goods_number') . ", " .hw_reserved_number_subquery('g.') . " " .
			"FROM ". $ecs->table('hktvmall') ."as h ".
			"LEFT JOIN " . $ecs->table('goods') . " AS g ON g.goods_id = h.goods_id ".
			"WHERE  1 ) as tmp ON tmp.goods_id = h.goods_id WHERE tmp.real_goods_number - tmp.reserved_number = 0 AND h.is_stock = 1";
		$no_stock_count = $db->getOne($sql);

		/* Get restock HKTV product count */
		$sql = "SELECT count(*) FROM ". $ecs->table('hktvmall') ."as h ".
			" LEFT JOIN (SELECT  h.goods_id, ".hw_goods_number_subquery('g.', 'real_goods_number') . ", " .hw_reserved_number_subquery('g.') . " " .
			"FROM ". $ecs->table('hktvmall') ."as h ".
			"LEFT JOIN " . $ecs->table('goods') . " AS g ON g.goods_id = h.goods_id ".
			"WHERE  1 ) as tmp ON tmp.goods_id = h.goods_id WHERE tmp.real_goods_number - tmp.reserved_number != 0 AND h.is_stock = 0";
		$re_stock_count = $db->getOne($sql);

		/* Get not sold HKTV product count */
		$sql = "SELECT count(*) ".
			"FROM ". $ecs->table('hktvmall') ."as h ".
			"LEFT JOIN " . $ecs->table('goods') . " AS g ON g.goods_id = h.goods_id ".
			"WHERE g.is_on_sale = 0 AND h.is_sale = 1 ";
		$no_sale_count = $db->getOne($sql);

		/* Create action link */
		$assign['action_link_list'][] =  array(
			'text' => $_LANG['export_csv'].($update_count > 0 ? "<span class='badge bg-green update_count'>$update_count</span>" : ""),
			'href' => 'hktvmall.php?act=export&type=update_price'
		);
		$assign['action_link_list'][] = array(
			'text' => '匯出無庫存產品'.($no_stock_count > 0 ? "<span class='badge bg-green update_count'>$no_stock_count</span>" : ""),
			'href' => 'hktvmall.php?act=export&type=out_of_stock'
		);
		$assign['action_link_list'][] = array(
			'text' => '匯出重新有庫存產品'.($re_stock_count > 0 ? "<span class='badge bg-green update_count'>$re_stock_count</span>" : ""),
			'href' => 'hktvmall.php?act=export&type=re_stock'
		);
		$assign['action_link_list'][] = array(
			'text' => '匯出已下架產品'.($no_sale_count > 0 ? "<span class='badge bg-green update_count'>$no_sale_count</span>" : ""),
			'href' => 'hktvmall.php?act=export&type=not_on_sale'
		);
		$assign['action_link'] = array('text' => $_LANG['add_hktv_item'],
			'href' => 'hktvmall.php?act=add'
		);

		parent::listAction($assign, 'hktvmall_list.htm');
	}
    
    /*------------------------------------------------------ */
    //-- INSERT HKTVMALL GOODS
    /*------------------------------------------------------ */
    public function insert_hktv_goods($rate, $goods_list)
    {
        global $db, $ecs;
        
        if(!isset($goods_list)) return false;
        // Default 10% rate if not input
        $rate = isset($rate) ? $rate : $this::DEFAULT_RATE;
        
        /* 1. Find and remove duplicate goods */
        // Find duplicate goods
        $sql = "SELECT goods_id ".
        "FROM ". $ecs->table($this->tablename) . " ".
        "WHERE goods_id ".db_create_in($goods_list)." ";
        $result = $db->getAll($sql);
        
        // Remove duplicate goods
        foreach ($result as $val){
            foreach ($goods_list as $key => $row){
                if($val['goods_id'] == $row) {
                    unset($goods_list[$key]);
                }
            }
        }

        /* 2. Insert goods into table */
        foreach ($goods_list as $val){
            $hktv_goods = array(
                    'goods_id'   => $val,
                    'creator'    => $_SESSION['admin_name'],
                    'hktv_rate'  => $rate
            );
            $result = new Model\Hktvmall(null, $hktv_goods);
            if($result == false) return false;
        }
        
        return true;
    }
    
    /*------------------------------------------------------ */
    //-- AJAX EDIT HKTVMALL GOODS
    /*------------------------------------------------------ */
    public function ajax_edit_hktv($id, $key, $value)
    {
    	global $_LANG;
        $sql =	"UPDATE " . $this->ecs->table($this->tablename) .
        "SET `".$key."` = '" . $value . "' " .
        "WHERE `hktv_id` = '" . $id . "' " .
        "LIMIT 1";
        
        if ($this->db->query($sql))
        {
            clear_cache_files();
            make_json_result($value);
        } else {
            make_json_error($_LANG['hktv_edit_error']);
        }

    }
    
    /*------------------------------------------------------ */
    //-- SEARCH GOODS ITEM
    /*------------------------------------------------------ */
    public function search_goods($data)
    {
        require_once(ROOT_PATH . 'includes/cls_json.php');
    	$json = new \JSON;
    
    	$filters = $json->decode($data);
    	$args = $json->decode($data);
    
    	$arr = get_goods_list($filters);
    	$opt = array();
    
    	foreach ($arr AS $key => $val)
    	{
    		$opt[] = array('value' => $val['goods_id'],
    				'text' => $val['goods_id'] . " - " . $val['goods_name'],
    				'data' => '');
    	}
    
    	make_json_result($opt);
    }
    
    /*------------------------------------------------------ */
    //-- BATCH HKTVMALL FUNCTION
    /*------------------------------------------------------ */
	/**
	 * @param $type
	 * @param $ids
	 * @return bool
	 * @throws \PHPExcel_Reader_Exception
	 * @throws \PHPExcel_Writer_Exception
	 */
	public function batch($type, $ids)
    {
        if(!isset($type) || !isset($ids)) return false;
		global $_LANG;
		$ids = explode(",", $ids);
        switch ($type) {
            
            /* Remove batch items */
            case 'drop':
                $sql = "DELETE FROM " . $this->ecs->table($this->tablename) .
                " WHERE hktv_id " . db_create_in($ids);

                $result = $this->db->query($sql);
				$link =[];
                if($result){
                    /* save log */
        			admin_log('', 'batch_remove', 'hktvmall');

                    /* Clear cache */
                    clear_cache_files();
                
                    $lnk[] = array('text' => $_LANG['back_list'], 'href' => 'hktvmall.php?act=list');

					sys_msg($_LANG['batch_handle_ok'], 0, $link);
                    
                } else {
                    return false;
                }
                
                break;
            
            /* Export batch items */
            case 'export':

                $this->export_csv_hktv($ids, 'batch');
                break;
            
            default:
                break;
        }
    }
    
    /*------------------------------------------------------ */
    //-- EXPORT HKTVMALL CSV/EXCEL
    /*------------------------------------------------------ */
	/**
	 * @param array $ids
	 * @param string $type
	 * @return bool
	 * @throws \PHPExcel_Reader_Exception
	 * @throws \PHPExcel_Writer_Exception
	 */
	public function export_csv_hktv($ids = array(), $type = "normal")
	{
		global $db, $ecs;

		/* Init PHPExcel */
		ini_set('memory_limit', '256M');
		define('PCLZIP_TEMPORARY_DIR', ROOT_PATH . 'temp/');
		require_once ROOT_PATH . 'includes/excel/PHPExcel.php';
		require_once ROOT_PATH . 'includes/excel/PHPExcel/IOFactory.php';
		require_once ROOT_PATH . 'includes/lib_order.php';
		require_once ROOT_PATH . 'includes/lib_howang.php';
		\PHPExcel_Settings::setZipClass(\PHPExcel_Settings::PCLZIP);
		// Create new PHPExcel object
		$objPHPExcel = new \PHPExcel();
		/* Export excel/csv by type */
		switch ($type) {
			case 'update_price':
				/* Get items */
				$sql = "SELECT h.*, g.market_price as goods_original_price, g.goods_name, g.`shipping_price`,  g.`shop_price`, g.is_shipping " .
					"FROM " . $ecs->table($this->tablename) . "as h " .
					"LEFT JOIN " . $ecs->table('goods') . " AS g ON g.goods_id = h.goods_id " .
					"WHERE  h.hktv_price <> g.shop_price ";
				$data = $db->getAll($sql);

				/* Excel title */
				// Merchant ID, Merchant SKU ID, % Discount, Fixed price discount, Start date(mm/dd/yyyy), End date(mm/dd/yyyy), Action(U or D, U for update, D for delete)
				$title_list = ['SKU Name', 'Merchant ID', 'Merchant SKU ID', '% Discount', 'Fixed price discount', 'Start date(mm/dd/yyyy)', 'End date(mm/dd/yyyy)', 'Action(U or D, U for update, D for delete)'];
				foreach (range('A', 'K') as $char_key => $column) {
					$objPHPExcel->getActiveSheet()->SetCellValue($column . '1', $title_list[$char_key]);
				}
				/* File name */
				$filename = date("F-j-Y-g-i-s") . '-hktvmall-update-price.xlsx';
				$content = array();
				if ($data) {
					foreach ($data as $key => $row) {
						/* Cal shipping price */
						$shipping_price = 0;
						if ($row['is_shipping'] == 1) $shipping_price = 0;
						elseif ($row['shipping_price'] > 0) $shipping_price = $row['shipping_price'];
						elseif ($row['shop_price'] < 1000) $shipping_price = 33;
						$row['shop_price'] = $row['shop_price'] + $shipping_price;
						$content[] = array(
							$row['goods_name'], $this->merchant_id, $row['hktv_sku'], '', $row['shop_price'], local_date('m/d/Y'), '', 'U'
						);
					}
				}
				/* Fill worksheet from values in array */
				try {
					$objPHPExcel->getActiveSheet()->fromArray($content, null, 'A2');
				} catch (\PHPExcel_Exception $e) {
				}
				/* Save Excel 2007 file */
				$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
				header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.‌​sheet; charset=utf-8');
				header('Content-Disposition: attachment; filename=' . $filename);
				header('Cache-Control: max-age=0');
				$objWriter->save('php://output');

				/* Update HKTV table when success to create excel */
				$update_sql = "UPDATE " . $this->ecs->table($this->tablename) . "as h " .
					"LEFT JOIN " . $this->ecs->table('goods') . " AS g ON g.goods_id = h.goods_id " .
					"SET h.hktv_price = g.shop_price " .
					"WHERE h.hktv_price <> g.shop_price ";
				if ($this->db->query($update_sql)) {
					clear_cache_files();
				}
				break;
			case 'out_of_stock':
				/* Get items */
				$sql = "SELECT h.*, tmp.goods_name, tmp.goods_sn FROM ". $ecs->table('hktvmall') ."AS h ".
				"LEFT JOIN (SELECT  g.goods_sn, g.goods_name, h.goods_id, ".hw_goods_number_subquery('g.', 'real_goods_number') . ", " .hw_reserved_number_subquery('g.') . " " .
				"FROM ". $ecs->table('hktvmall') ."AS h ".
				"LEFT JOIN " . $ecs->table('goods') . " AS g ON g.goods_id = h.goods_id ".
				"WHERE  1 ) as tmp ON tmp.goods_id = h.goods_id WHERE tmp.real_goods_number - tmp.reserved_number = 0 AND h.is_stock = 1";
				$data = $db->getAll($sql);

				/* Excel title */
				$title_list = ['產品貨號', 'HKTVMALL SKU ID', '產品名稱'];
				foreach (range('A', 'K') as $char_key => $column) {
					$objPHPExcel->getActiveSheet()->SetCellValue($column . '1', $title_list[$char_key]);
				}
				/* File name */
				$filename = date("F-j-Y-g-i-s") . '-hktvmall-out-of-stock.xlsx';
				$content = array();
				$need_update_ids = [];
				if ($data) {
					foreach ($data as $key => $row) {
						$need_update_ids[] = $row['goods_id'];
						/* Cal shipping price */
						$content[] = array(
							$row['goods_sn'], $row['hktv_sku'], $row['goods_name']
						);
					}
				}
				/* Fill worksheet from values in array */
				try {
					$objPHPExcel->getActiveSheet()->fromArray($content, null, 'A2');
				} catch (\PHPExcel_Exception $e) {
				}
				/* Save Excel 2007 file */
				$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
				header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.‌​sheet; charset=utf-8');
				header('Content-Disposition: attachment; filename=' . $filename);
				header('Cache-Control: max-age=0');
				$objWriter->save('php://output');

				/* Update HKTV table when success to create excel */
				$update_sql = "UPDATE " . $this->ecs->table($this->tablename) . "as h " .
					"SET h.is_stock = 0 " .
					"WHERE h.goods_id ".db_create_in($need_update_ids);
				if ($this->db->query($update_sql)) {
					clear_cache_files();
				}
				break;
			case 're_stock':
				/* Get items */
				$sql = "SELECT h.*, tmp.goods_name, tmp.goods_sn FROM ". $ecs->table('hktvmall') ."AS h ".
					"LEFT JOIN (SELECT  g.goods_sn, g.goods_name, h.goods_id, ".hw_goods_number_subquery('g.', 'real_goods_number') . ", " .hw_reserved_number_subquery('g.') . " " .
					"FROM ". $ecs->table('hktvmall') ."AS h ".
					"LEFT JOIN " . $ecs->table('goods') . " AS g ON g.goods_id = h.goods_id ".
					"WHERE  1 ) as tmp ON tmp.goods_id = h.goods_id WHERE tmp.real_goods_number - tmp.reserved_number != 0 AND h.is_stock = 0";
				$data = $db->getAll($sql);

				/* Excel title */
				$title_list = ['產品貨號', 'HKTVMALL SKU ID', '產品名稱'];
				foreach (range('A', 'K') as $char_key => $column) {
					$objPHPExcel->getActiveSheet()->SetCellValue($column . '1', $title_list[$char_key]);
				}
				/* File name */
				$filename = date("F-j-Y-g-i-s") . '-hktvmall-re-stock.xlsx';
				$content = array();
				$need_update_ids = [];
				if ($data) {
					foreach ($data as $key => $row) {
						$need_update_ids[] = $row['goods_id'];
						/* Cal shipping price */
						$content[] = array(
							$row['goods_sn'], $row['hktv_sku'], $row['goods_name']
						);
					}
				}
				/* Fill worksheet from values in array */
				try {
					$objPHPExcel->getActiveSheet()->fromArray($content, null, 'A2');
				} catch (\PHPExcel_Exception $e) {
				}
				/* Save Excel 2007 file */
				$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
				header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.‌​sheet; charset=utf-8');
				header('Content-Disposition: attachment; filename=' . $filename);
				header('Cache-Control: max-age=0');
				$objWriter->save('php://output');

				/* Update HKTV table when success to create excel */
				$update_sql = "UPDATE " . $this->ecs->table($this->tablename) . "as h " .
					"SET h.is_stock = 1 " .
					"WHERE h.goods_id ".db_create_in($need_update_ids);
				if ($this->db->query($update_sql)) {
					clear_cache_files();
				}
				break;
			case 'not_on_sale':
				/* Get items */
				$sql = "SELECT h.*, g.goods_sn, g.goods_name FROM ". $ecs->table('hktvmall') ."AS h ".
					"LEFT JOIN " . $ecs->table('goods') . " AS g ON g.goods_id = h.goods_id ".
					"WHERE g.is_on_sale = 0 AND h.is_sale = 1";
				$data = $db->getAll($sql);

				/* Excel title */
				$title_list = ['產品貨號', 'HKTVMALL SKU ID', '產品名稱'];
				foreach (range('A', 'K') as $char_key => $column) {
					$objPHPExcel->getActiveSheet()->SetCellValue($column . '1', $title_list[$char_key]);
				}
				/* File name */
				$filename = date("F-j-Y-g-i-s") . '-hktvmall-not-sale.xlsx';
				$content = array();
				$need_update_ids = [];
				if ($data) {
					foreach ($data as $key => $row) {
						$need_update_ids[] = $row['goods_id'];
						/* Cal shipping price */
						$content[] = array(
							$row['goods_sn'], $row['hktv_sku'], $row['goods_name']
						);
					}
				}
				/* Fill worksheet from values in array */
				try {
					$objPHPExcel->getActiveSheet()->fromArray($content, null, 'A2');
				} catch (\PHPExcel_Exception $e) {
				}
				/* Save Excel 2007 file */
				$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
				header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.‌​sheet; charset=utf-8');
				header('Content-Disposition: attachment; filename=' . $filename);
				header('Cache-Control: max-age=0');
				$objWriter->save('php://output');

				/* Update HKTV table when success to create excel */
				$update_sql = "UPDATE " . $this->ecs->table($this->tablename) . "as h " .
					"SET h.is_sale = 0 " .
					"WHERE h.goods_id ".db_create_in($need_update_ids);
				if ($this->db->query($update_sql)) {
					clear_cache_files();
				}
				break;
			case 'batch':
				/* Get items */
				$sql = "SELECT h.*, ".
					"g.shop_price, g.market_price as goods_original_price, g.goods_name as goods_cht_name, IFNULL(gl.goods_name, g.goods_name) as goods_eng_name , " .
					"g.goods_desc as goods_cht_desc, IFNULL(gl.goods_desc, g.goods_desc) as goods_eng_desc, g.`shipping_price`, g.is_shipping,  " .
					"g.goods_brief as goods_cht_brief, IFNULL(gl.goods_brief, g.goods_brief) as goods_eng_brief, " .
					"IFNULL(wt.template_content, g.seller_note) as goods_cht_seller_note, COALESCE(wtl.template_content, wt.template_content, gl.seller_note, g.seller_note) as goods_eng_seller_note " .
					"FROM " . $ecs->table($this->tablename) . "as h " .
					"LEFT JOIN " . $ecs->table('goods') . " AS g ON g.goods_id = h.goods_id " .
					"LEFT JOIN " . $ecs->table('goods_lang') . " AS gl ON gl.goods_id = h.goods_id AND gl.lang = 'en_US' " .
					"LEFT JOIN " . $ecs->table('warranty_templates') . " AS wt ON g.warranty_template_id = wt.template_id " .
					"LEFT JOIN " . $ecs->table('warranty_templates_lang') . " AS wtl ON g.warranty_template_id = wtl.template_id " .
					"WHERE h.hktv_id " . db_create_in($ids);
				$data = $db->getAll($sql);

				/* Excel title */
				$title_list = ['Brand','SKU Name(English)','SKU Name(中文)','Original Price','Selling Price',
					'Commission Rate','Qty','Cost', 'SKU Short Description(English)', 'SKU 商品簡介(中文)', 'SKU Long Description(English)', 'SKU 詳細介紹(中文)',  'Reference Link'];
				foreach (range('A', 'M') as $char_key => $column) {
					$objPHPExcel->getActiveSheet()->SetCellValue($column . '1', $title_list[$char_key]);
				}
				/* File name */
				$filename = date("F-j-Y-g-i-s") . '-hktvmall-batch.xlsx';
				//Add csv header format
				$content   = array();
				if($data){
					foreach ($data as $key => $row) {
						$url = build_uri('goods', array('gid'=>$row['goods_id'], 'gname'=>$row['goods_cht_name']));
						$goods_url = $this->getFullUrl($url);
						$shipping_price = 0;
						if ($row['is_shipping'] == 1) $shipping_price = 0;
						elseif ($row['shipping_price'] > 0) $shipping_price = $row['shipping_price'];
						elseif ($row['shop_price'] < 1000) $shipping_price = 33;
						$row['hktv_price'] = $row['hktv_price'] + $shipping_price;
						$row['goods_original_price'] = $row['goods_original_price'] + $shipping_price;
						$rate = strstr($row['hktv_rate'], "%", true);
						$cost = intval($row['hktv_price'] * ($rate/100));
						$content[] = array(
							$row['hktv_brand'],$row['goods_eng_name'],$row['goods_cht_name'] ,$row['goods_original_price'], $row['hktv_price'],
							$row['hktv_rate'],$row['hktv_qty'], $cost,  html_entity_decode(strip_tags($row['goods_eng_brief'])), html_entity_decode(strip_tags($row['goods_cht_brief'])),  html_entity_decode(strip_tags($row['goods_eng_seller_note'].$row['goods_eng_desc'])), html_entity_decode(strip_tags($row['goods_cht_seller_note'].$row['goods_cht_desc'])), $goods_url
						);
					}
				}
				/* Fill worksheet from values in array */
				try {
					$objPHPExcel->getActiveSheet()->fromArray($content, null, 'A2');
				} catch (\PHPExcel_Exception $e) {
				}
				/* Save Excel 2007 file */
				$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
				header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.‌​sheet; charset=utf-8');
				header('Content-Disposition: attachment; filename=' . $filename);
				header('Cache-Control: max-age=0');
				$objWriter->save('php://output');
				break;
			default: // Normal
				break;
		}
		return true;
	}
    
    
}
