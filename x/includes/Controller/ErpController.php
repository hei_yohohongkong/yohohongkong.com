<?php

/***
* ErpController.php
* by MichaelHui 20170517
*
* ERP related
***/
namespace Yoho\cms\Controller;
use Yoho\cms\Model;

use \Exception;

if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}
class ErpController extends YohoBaseController{
	//private $tablename='salesperson';

	const DB_WAREHOUSING_STYLE_REPAIRANDSTOCK = '維修單-入暫存庫';
	const DB_DELIVERY_STYLE_SENDOUT = '維修單-送修出庫';
	const DB_DELIVERY_STYLE_TRANSFERTOSELLABLE = '維修-調回可銷售庫存';
	const DB_DELIVERY_STYLE_REPAIREDSENDOUT = '維修單-完成處理';
	const PREFIX_REPAIR_PO = 'POS-R';
	const PO_ORDER_TYPE = ['正常採購','代理送貨','寄賣','RMA更換',self::DB_WAREHOUSING_STYLE_REPAIRANDSTOCK];
	const DB_DELIVERY_STYLE_WRITE_OFF = '折舊報廢';
	const DB_DELIVERY_STYLE_TRANSFER = '貨倉調貨';
	const DB_WAREHOUSING_STYLE_TRANSFER = '貨倉調貨';

	const warehouse_type_normal = '0';
	const warehouse_type_shop = '1';
	const warehouse_type_temporary = '2';
	const warehouse_type_display = '3';

	private static $warehouseTypeValues=['普通倉','門市倉','暫存倉','展示機倉'];

	private $cachekeySellableWarehouseIds,$cachekeyWarehouseList;

	public function __construct(){
		parent::__construct();
		//$this->tablename=$GLOBALS['ecs']->table($this->tablename);
		$this->cachekeySellableWarehouseIds = $this->genCacheKey(['erp_sellablewsids']);
		$this->cachekeyWarehouseList = $this->genCacheKey(['erp_wslist']);
	}

    public function getWarehouseTypes()
    {
        return self::$warehouseTypeValues;
    }

	public function getMainWarehouseId()
	{
		$sql = "select warehouse_id from ".$this->ecs->table('erp_warehouse')." ew where is_main = 1 order by warehouse_id ASC";
		$warehouse_id = $this->db->getOne($sql);
		if (empty($warehouse_id)) {
			return 0;
		} else {
			return $warehouse_id;
		}
	}

	public function getWarehouseCode($warehouse_id)
	{
		$sql = "select code from ".$this->ecs->table('erp_warehouse')." ew where warehouse_id = ".$warehouse_id." ";
		$warehouse_code = $this->db->getOne($sql);
		if (empty($warehouse_code)) {
			return 0;
		} else {
			return $warehouse_code;
		}
	}

	public function getShopWarehouseId($get_one = TRUE)
	{
		// 觀塘門市 warehouse_id = 1
		//return 1;

		 $sql = "select warehouse_id from ".$this->ecs->table('erp_warehouse')." ew where type = 1 and is_valid = 1";
		 if ($get_one){
			$warehouse_id = $this->db->getOne($sql);
		 } else {
			$warehouse_id = $this->db->getAll($sql);
		 }
		 if (empty($warehouse_id)) {
			if ($get_one){
				$warehouse_id = 1;
			} else {
				return array(0 => array("warehouse_id" => 1));
			}
		 } else {
			 return $warehouse_id;
		 }
	}

	public function getTempWarehouseIdByShopId($warehouse_id) {
		global $db, $ecs, $_CFG;
		
		$sql = "select warehouse_id from ".$ecs->table('erp_warehouse')." where parents_id = ".$warehouse_id." and type = ".self::warehouse_type_temporary." ";
		
		$temp_warehouse_id = $db->getOne($sql);
		
		if (empty($temp_warehouse_id)) {
			return $warehouse_id;  
		} else {
			return $temp_warehouse_id;
		}
	}

    public function getWarehouseNameById($id)
    {
        global $db, $ecs, $userController, $_CFG;

        $sql = "select name from  ".$ecs->table('erp_warehouse')." where warehouse_id = ".$id." ";
        return $db->getOne($sql);
    }

	public function getSellableProductsQtyByWarehouses($prodIds,$warehouseIds,$excludeReserve=TRUE,$excludeOrderId=null,$excludeOrderGoodsTransferReserved=TRUE)
	{
		require_once(ROOT_PATH . '/includes/lib_order.php');
		$warehouseIdsAr=[];
		$prodIdsAr=[];
		$orderController = new OrderController();
		$warehouseTransferController = new WarehouseTransferController();
		if (empty($warehouseIds)) {
			$warehouseIdsAr = $this->getSellableWarehouseIds();
		} else {
			if (gettype($warehouseIds) != 'array') {
				$warehouseIdsAr[] = $warehouseIds;
			} else {
				$warehouseIdsAr = $warehouseIds;
			}
		}

		if (gettype($prodIds) != 'array') {
			$prodIdsAr[] = $prodIds;
		} else {
			$prodIdsAr = $prodIds;
		}

		if (sizeof($prodIdsAr) == 1) {
			$search_goods_sql = " goods_id = ".$prodIdsAr[0]." ";
		} else {
			$search_goods_sql =  "goods_id in ('".implode("','",$prodIdsAr)."')";
		}
		
		$sql = "SELECT goods_id, warehouse_id, IFNULL(SUM(goods_qty),0) as goods_sellable_total FROM " . $this->ecs->table('erp_goods_attr_stock') . " WHERE ".$search_goods_sql." AND warehouse_id in (".implode(",",$warehouseIdsAr).") group by goods_id,warehouse_id ";
		$res = $this->db->getAll($sql);

		$goodsStock = [];
		foreach ($res as $goods) {
			$goodsStock[$goods['goods_id']][$goods['warehouse_id']] = $goods['goods_sellable_total'];
		}

		// reserved order start
		if ($excludeReserve == true) {
			$og_alias='og';
			$oi_alias='oi';

			if (sizeof($prodIdsAr) == 1) {
				$search_goods_sql = $og_alias.".goods_id = ".$prodIdsAr[0]." ";
			} else {
				$search_goods_sql =  $og_alias.".goods_id in ('".implode("','",$prodIdsAr)."')";
			}

			$exclude_order_sql = "";
			if (!empty($excludeOrderId)) {
				$exclude_order_sql = " and oi.order_id != ".$excludeOrderId." ";
			}

			$sql = "SELECT og.goods_id,og.warehouse_id,SUM(IF(og.send_number > og.goods_number, 0, og.goods_number - og.send_number))  as reserved_qty " .
					"FROM " . $GLOBALS['ecs']->table('order_goods') . " as og " .
					"LEFT JOIN " . $GLOBALS['ecs']->table('order_info') . " as oi ON og.order_id = oi.order_id " .
					"WHERE ".$search_goods_sql." ".$exclude_order_sql." AND og.warehouse_id in (".implode(",",$warehouseIdsAr).")" . \order_query_sql('await_ship', $oi_alias . '.') . ' group by og.warehouse_id, og.goods_id';

			$resReserved = $this->db->getAll($sql);

			$goodsReservedStock = [];
			foreach ($resReserved as $reservedGoods) {
				$goodsReservedStock[$reservedGoods['goods_id']][$reservedGoods['warehouse_id']] = $reservedGoods['reserved_qty'];
			}

			// order goods transfer reserved (store pickup)
			$orderGoodsTransferReservedStock = [];
			if($excludeOrderGoodsTransferReserved == true){
				$sql = "select ogtr.goods_id, ogtr.warehouse_from , sum(ogtr.transfer_qty) as transfer_qty from ".$this->ecs->table('order_goods_transfer_reserved')." ogtr left join  ".$this->ecs->table('erp_stock_transfer')." est on (ogtr.transfer_id = est.transfer_id)  where est.reason_type = ".$warehouseTransferController::REASON_TYPE_STORE_PICK_UP." and  ogtr.status = ".$orderController::ORDER_GOODS_RESERVED_STATUS_TRANSFERRING ." and ogtr.type = ".$orderController::ORDER_GOODS_RESERVE_TYPE_TRANSFER." group by ogtr.warehouse_from, ogtr.goods_id ";
				$resOrderGoodsTransferReserved = $this->db->getAll($sql);
			
				foreach ($resOrderGoodsTransferReserved as $transferReservedGoods) {
					$orderGoodsTransferReservedStock[$transferReservedGoods['goods_id']][$transferReservedGoods['warehouse_from']] = $transferReservedGoods['transfer_qty'];
				}
			}
			
		}
		// reserved order end

		foreach ($prodIdsAr as $prodId) {
			foreach ($warehouseIdsAr as $warehouseId) {
				if (!isset($goodsStock[$prodId][$warehouseId])) {
					$goodsStock[$prodId][$warehouseId] = 0;
				} else {
					// check if it needs to reduce stock reserved
					if (isset($goodsReservedStock[$prodId][$warehouseId])) {
						$goodsStock[$prodId][$warehouseId] -= $goodsReservedStock[$prodId][$warehouseId];
					}
					if (isset($orderGoodsTransferReservedStock[$prodId][$warehouseId])) {
						$goodsStock[$prodId][$warehouseId] += $orderGoodsTransferReservedStock[$prodId][$warehouseId];
					}
				}
			}
		}
		
		return $goodsStock;
	}

	public function getSumOfSellableProductsQtyByWarehouses($prodIds,$warehouseIds,$excludeReserve=TRUE,$excludeOrderId=null,$excludeOrderGoodsTransferReserved=TRUE)
	{
		$goodsStock = self::getSellableProductsQtyByWarehouses($prodIds, $warehouseIds, $excludeReserve,$excludeOrderId,$excludeOrderGoodsTransferReserved);
		$stocks = [];
		foreach ($goodsStock as $goods_id => $stock) {
			$sum = 0;
			foreach ($stock as $count) {
				$sum += intval($count);
			}
			$stocks[$goods_id] = $sum;
		}
		return $stocks;
	}

	public function getSellableWarehouseInfo(){
		$res = $this->db->getAll("SELECT warehouse_id,name FROM ".$this->ecs->table('erp_warehouse')." WHERE is_valid=TRUE AND is_on_sale=TRUE");
		return $res;
	}

	public function getSellableProductQty($prodId,$excludeReserve=TRUE){
		$qty = intval($this->db->getOne("SELECT SUM(goods_qty) FROM " . $this->ecs->table('erp_goods_attr_stock') . " WHERE goods_id='".$prodId."' AND warehouse_id IN (".$this->getSellableWarehouseIds(true).")"));
		if($excludeReserve==true){
			$qty-=$this->getReservedQty($prodId);
		}
		return $qty;
	}

	public function getFlsWarehouseId()
	{
		$sql = "select warehouse_id from ".$this->ecs->table('erp_warehouse')." ew where code = 'FLS' and is_valid = true and is_on_sale = true ";
		$warehouse_id = $this->db->getOne($sql);
		if (empty($warehouse_id)) {
			return 0;
		} else {
			return $warehouse_id;
		}
	}

	public function getWilsonWarehouseId()
	{
		$sql = "select warehouse_id from ".$this->ecs->table('erp_warehouse')." ew where code = 'WILSON' and is_valid = true and is_on_sale = true ";
		$warehouse_id = $this->db->getOne($sql);
		if (empty($warehouse_id)) {
			return 0;
		} else {
			return $warehouse_id;
		}
	}

	public function getSellableProductQtyOfFls($prodId){
		$fls_warehouse_id = $this->getFlsWarehouseId();

		$qty = intval($this->db->getOne("SELECT SUM(goods_qty) FROM " . $this->ecs->table('erp_goods_attr_stock') . " WHERE goods_id='".$prodId."' AND warehouse_id IN (".$this->getSellableWarehouseIds(true).") AND warehouse_id = ".$fls_warehouse_id." "));

		return $qty;
	}

	public function getSellableProductQtyWithoutFls($prodId,$excludeReserve=TRUE){
		$fls_warehouse_id = $this->getFlsWarehouseId();
		
		$qty = intval($this->db->getOne("SELECT SUM(goods_qty) FROM " . $this->ecs->table('erp_goods_attr_stock') . " WHERE goods_id='".$prodId."' AND warehouse_id IN (".$this->getSellableWarehouseIds(true).") AND warehouse_id != ".$fls_warehouse_id." "));
		if($excludeReserve==true){
			$qty-=$this->getReservedQty($prodId);
		}
		return $qty;
	}

	private function getReservedQty($prodId){
        require_once(ROOT_PATH . '/includes/lib_order.php');
		$og_alias='og';
		$oi_alias='oi';
		return intval($this->db->getOne("SELECT SUM(IF({$og_alias}.send_number > {$og_alias}.goods_number, 0, {$og_alias}.goods_number - {$og_alias}.send_number)) " .
				"FROM " . $GLOBALS['ecs']->table('order_goods') . " as {$og_alias} " .
				"LEFT JOIN " . $GLOBALS['ecs']->table('order_info') . " as {$oi_alias} ON {$og_alias}.order_id = {$oi_alias}.order_id " .
				"WHERE {$og_alias}.goods_id=".$prodId . \order_query_sql('await_ship', $oi_alias . '.')));
	}

	public function getWarehouseList($admin_id=0 ,$parents_id = -1, $type = -1, $is_on_sale = -1, $is_repair_center = -1){
		$sql="select warehouse_id,name,description,is_on_sale, type from ".$this->ecs->table('erp_warehouse')." where 1";
		if ($admin_id >0) {
			$sql.=" and warehouse_id in (select warehouse_id from ".$this->ecs->table('erp_warehouse_admin')." where admin_id='".$admin_id."')";
		}

		$sql.=" and is_valid= 1 ";
		
		if ($parents_id >= 0) {
			$sql.=" and parents_id='".$parents_id."'";
		}
	
		if (is_array($type) && !empty($type)) {
			$sql.=" and type in ( ".implode(',',$type)." )";
		} else {
			if($type >= 0){
				$sql.=" and type ='".$type."'";
			}
		}

		if ($is_on_sale > 0) {
			$sql.=" and is_on_sale = 1 ";
		}

		if($is_repair_center > 0){
			$sql.=" and is_repair_center = 1 ";
		}
	
	    $sql.=" order by warehouse_id asc ";

		$res = $this->db->getAll($sql);
		return $res;

		// $cachekey = $this->cachekeyWarehouseList;
		// $this->memcache->delete($this->cachekeyWarehouseList);
		// if($this->memcache->get($cachekey)===false){
		// 	$res = $this->db->getAll("SELECT warehouse_id,name,description,is_on_sale FROM ".$this->ecs->table('erp_warehouse')." WHERE is_valid=TRUE");
		// 	$this->memcache->set($cachekey,$res);
		// 	return $res;
		// }else{
		// 	return $this->memcache->get($cachekey);
		// }
	}

	public function getWarehouseListByShopId($shop_warehouse_id){
		$sql="select warehouse_id,name,description,is_on_sale, type from ".$this->ecs->table('erp_warehouse')." where 1";
		
		$sql.=" and is_valid= 1 and type != 3 ";
		$sql.=" and ( parents_id ='".$shop_warehouse_id."' or warehouse_id = ".$shop_warehouse_id." )";

		$sql.=" order by warehouse_id asc ";

		$res = $this->db->getAll($sql);
		return $res;

	}


	function getGoodsIdBySn($goods_sn)
	{
		global $db, $ecs;
		$sql = "select goods_id from ".$ecs->table('goods')." where goods_sn = '".$goods_sn."' ";
		return $db->getOne($sql);
	}


	public function getWarehouseById($warehouseId){
		$res = $this->db->getRow("SELECT warehouse_id,name,description,is_on_sale,code FROM ".$this->ecs->table('erp_warehouse')." WHERE warehouse_id = ".$warehouseId." and is_valid=TRUE");
		return $res;
	}

	public function getGoodsWarehouseList() {
		$goods_warehouse_list = [];
		$warehouseId = $this->getFlsWarehouseId();
		if (!empty($warehouseId)) {
			$warehouseFLS = $this->getWarehouseById($warehouseId);
		}
		$goods_warehouse_list[] = $warehouseFLS;
		return $goods_warehouse_list;
	}

	public function getSellableWarehouseIds($imploded=false){
		$cachekey = $this->cachekeySellableWarehouseIds;
		$this->memcache->delete($this->cachekeyWarehouseList);
		if($this->memcache->get($cachekey)===false){
			$res = $this->db->getCol("SELECT warehouse_id FROM ".$this->ecs->table('erp_warehouse')." WHERE is_valid=TRUE AND is_on_sale=TRUE");
			$this->memcache->set($cachekey,$res);
			return ($imploded?implode(',', $res):$res);
		}else{
			return ($imploded?implode(',', $this->memcache->get($cachekey)):$this->memcache->get($cachekey));
		}
	}

	public function clearWarehouseCache(){
		$this->memcache->delete($this->cachekeySellableWarehouseIds);
		$this->memcache->delete($this->cachekeyWarehouseList);
	}

	public function getWarehousingStyleIdByName($name){
		$cachekey = $this->genCacheKey('erp_wsid'.md5($name));
		$this->memcache->delete($cachekey);
		if($this->memcache->get($cachekey)==false){
			$res = $this->db->getCol("SELECT warehousing_style_id FROM ".$this->ecs->table('erp_warehousing_style')." WHERE warehousing_style='".$name."'");
			if(count($res)<=0){
				return false;
			}

			if(!$this->memcache->set($cachekey,intval($res[0])))
				return $res[0]; //TODO: to be replaced, system log for memcache not running!
			
			return $res[0];
		}else{
			return $this->memcache->get($cachekey);
		}
	}

	public function getDeliveryStyleIdByName($name){
		$cachekey = $this->genCacheKey('erp_dsid'.md5($name));
		$this->memcache->delete($cachekey);
		if($this->memcache->get($cachekey)==false){
			$res = $this->db->getCol("SELECT delivery_style_id FROM ".$this->ecs->table('erp_delivery_style')." WHERE delivery_style='".$name."'");
			if(count($res)<=0){
				return false;
			}

			if(!$this->memcache->set($cachekey,intval($res[0])))
				return $res[0]; //TODO: to be replaced, system log for memcache not running!
			
			return $res[0];
		}else{
			return $this->memcache->get($cachekey);
		}
	}

	private function _createWarehousing($repairOrder,$data){
		require_once(ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_goods_attr.php');
		require_once(ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_warehouse.php');
		$wsn=\gen_warehousing_sn();
		$sql="INSERT INTO ".$this->ecs->table('erp_warehousing')." SET ";
		$sql.="warehousing_sn='".$wsn."', ";
		$sql.="warehousing_from='".self::PREFIX_REPAIR_PO."',";
		$sql.="warehousing_style_id='".$data['warehousing_style_id']."',";
		$sql.="warehouse_id ='".$data['warehouse_id']."',";
		$sql.="create_time='".gmtime()."',";
		$sql.="create_by='1',";//TODO:get operator ID
		$sql.="order_id='".$repairOrder['repair_id']."',";
		$sql.="operator  ='".$repairOrder['operator']."',";
		$sql.="status   ='3',";
		$sql.="post_by='1',";
		$sql.="post_time='".gmtime()."'";
		if($this->db->query($sql)){
			return ['warehousingId'=>$this->db->Insert_ID(),'warehousingSn'=>$wsn];
		}else{
			return false;
		}
	}

	public function addWarehousingItemByWsId($warehousingId){
		return $this->_addWarehousingItem($warehousingId);
	}
	
	public function addDeliveryItemByWsId($warehousingId){
		return $this->_addWarehousingItem($warehousingId);
	}

	private function _addWarehousingItem($warehousingId,$product=null,$attr_id=null){

		//1)get product
		if(!isset($product) || !isset($attr_id)){
			$repairOrderId = $this->db->getCol("SELECT order_id FROM ".$this->ecs->table('erp_warehousing')." WHERE warehousing_id=".$warehousingId);
			$product = $this->_getProductByRepairOrderId($repairOrderId);
			$attr_id=get_attr_id($product['goods_id']);	
		}
		
		//3)add warehousing item
		$sql="INSERT INTO ".$this->ecs->table('erp_warehousing_item')." SET warehousing_id='".$warehousingId."',goods_id='".$product['goods_id']."',attr_id='".$attr_id."', expected_qty='0',received_qty='".$product['expected_qty']."'";
		return $this->db->query($sql);
	}

	public function stockBackFromPOS($repairOrder,$data){
		require_once(ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_goods_attr.php');
		require_once(ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_warehouse.php');
		$this->db->query('START TRANSACTION');

		//TODO:dynamic warehouse id
		$warehouseId=2;
		$warehousingStyleId=$this->getWarehousingStyleIdByName(self::DB_WAREHOUSING_STYLE_REPAIRANDSTOCK);
		$deliveryStyleId=$this->getDeliveryStyleIdByName(self::DB_DELIVERY_STYLE_SENDOUT);
		$data['warehouse_id'] = $warehouseId;
		$data['warehousing_style_id'] = $warehousingStyleId;
		$data['delivery_style_id'] = $deliveryStyleId;
		//1) create warehousing and delivery
		$warehousing = $this->_createWarehousing($repairOrder,$data);
		$warehousingId=$warehousing['warehousingId'];
		//$deliveryId = $this->_createDelivery($repairOrder,$data);

		//2)insert product to erp_warehousing
		//2-1) get product from order
		$product = $this->_getProductByRepairOrderId($repairOrder['repair_id']);
		if(!$product){
			$this->db->query('ROLLBACK');
			return false;
		}
		$attr_id=get_attr_id($product['goods_id']);
		
		if(!$this->_addWarehousingItem($warehousingId,$product,$attr_id)){
			$this->db->query('ROLLBACK');
			return false;
		}

		//3) approving the warehousing
		
		$order_sn=$repairOrder['order_sn'];
		$supplier_id=0;

		\add_stock_by_warehousing($warehousingId);

		return $this->db->query('COMMIT');
	}

	private function _getProductByRepairOrderId($repairOrderId){
		$sql="SELECT ro.goods_id, IFNULL(og.goods_attr_id,ega.attr_id) as goods_attr_id, 1 as expected_qty, IFNULL(og.goods_price,g.shop_price) as price FROM 
			".$this->ecs->table('repair_orders')." ro 
			LEFT JOIN ".$this->ecs->table('order_goods')." og ON og.order_id=ro.order_id 
			LEFT JOIN ".$this->ecs->table('goods')." g ON ro.goods_id=g.goods_id 
			LEFT JOIN ".$this->ecs->table('erp_goods_attr')." ega ON ega.goods_id=ro.goods_id 
			WHERE ro.repair_id=".$repairOrderId;
		$product=$this->db->getAll($sql);
		if(count($product)<=0){
			return false;
		}
		return $product[0];
	}


	public function getApprovedRepairWarehousingList(){
		return $this->db->getAll("SELECT warehousing_id as order_id, warehousing_sn as order_sn FROM ".$this->ecs->table('erp_warehousing')."  WHERE status=3 AND LEFT(warehousing_sn,1)='R'");

	}

	//referred from lib_erp_warehouse.php
	private function gen_warehousing_sn($prefix='POSR'){
		$sql="select warehousing_sn from ".$GLOBALS['ecs']->table('erp_warehousing')." where warehousing_sn like '".$prefix."%' order by warehousing_id desc limit 1";
		$result=$GLOBALS['db']->getOne($sql);
		if(!empty($result))
		{
			$year_date=substr($result,2,4);
			$current_year_date=local_date("ym");
			if($current_year_date==$year_date)
			{
				$sn=substr($result,6,3);
				$sn=intval($sn)+1;
				$sn=gen_zero_str(3-strlen($sn)).$sn;
				$sn=$prefix.$year_date.$sn;
				return $sn;
			}
			else
			{
				$sn=$prefix.$current_year_date."001";
				return $sn;
			}
		}
		else
		{
			$current_year_date=local_date("ym");
			$sn=$prefix.$current_year_date."001";
			return $sn;
		}
	}

	public function getSupplierByProductId($productId){
		return $this->db->getAll("SELECT egs.supplier_id,es.name FROM ".$this->ecs->table('erp_goods_supplier')." egs LEFT JOIN ".$this->ecs->table('erp_supplier')." es ON es.supplier_id=egs.supplier_id WHERE egs.goods_id=".$productId);
	}

	public function makeRODeliveryOrder($repairOrderSn,$rma_id = null){
		if (!empty($rma_id)) {
			$sql = "select warehouse_id from ".$this->ecs->table('erp_rma')." where rma_id = ".$rma_id." ";
			$warehouse_id = $this->db->getOne($sql);
			$warehouse_id = $this->getTempWarehouseIdByShopId($warehouse_id);
		}

		$data = $this->db->getAll("SELECT warehouse_id, goods_id FROM ".$this->ecs->table('erp_stock')." WHERE order_sn='".$repairOrderSn."'  ".(!empty($warehouse_id)? 'and warehouse_id = "'.$warehouse_id.'"' : '')."   ORDER BY stock_id DESC LIMIT 1");
		if(count($data)<=0){
			return false;
		}
		return $this->_makeDeliveryOrder($data[0]['warehouse_id'],$repairOrderSn,$data[0]['goods_id'],1,self::DB_DELIVERY_STYLE_REPAIREDSENDOUT,$rma_id);
	}

	public function makeRMADeliveryOrder($warehouseId,$supplierId,$productId,$qty,$rma_id = null){
		return $this->_makeDeliveryOrder($warehouseId,$supplierId,$productId,$qty,self::DB_DELIVERY_STYLE_SENDOUT,$rma_id);
	}

	public function makeRMAWriteOffDeliveryOrder($warehouseId,$supplierId,$productId,$qty,$rma_id = null){
		return $this->_makeDeliveryOrder($warehouseId,$supplierId,$productId,$qty,self::DB_DELIVERY_STYLE_WRITE_OFF,$rma_id);
	}

	private function _makeDeliveryOrder($warehouseId,$receiver,$productId,$qty,$type=self::DB_DELIVERY_STYLE_SENDOUT,$rma_id = null){
		require_once(ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_goods.php');
		require_once(ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_goods_attr.php');
		require_once(ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_supplier.php');
		require_once(ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_warehouse.php');
		$deliveryId=\add_delivery();
		$deliveryInfo=\get_delivery_info($deliveryId);
		$this->db->query("START TRANSACTION");
		try{
			$erpOrderSn='';
			//Receiver could be ID or order SN
			if($type==self::DB_DELIVERY_STYLE_REPAIREDSENDOUT){
				$erpOrderSn = $receiver;
				$receiver = self::PREFIX_REPAIR_PO;				
			}elseif($type==self::DB_DELIVERY_STYLE_SENDOUT){
				$supplier = \supplier_info($receiver);
				$receiver = $supplier['name'];
			}elseif($type==self::DB_DELIVERY_STYLE_WRITE_OFF){
				//$supplier = \supplier_info($receiver);
				$user_id = erp_get_admin_id();
				$sql="select user_name from ".$this->ecs->table('admin_user')." where user_id = ".$user_id." ";
				$user_name = $this->db->getOne($sql);
				$receiver = $user_name;
			}else{
				throw new Exception("錯誤的出貨單類別", 503);
			}
			
			//stock check
			$attrId=\get_attr_id($productId);
			$stock=\get_goods_stock_by_warehouse($warehouseId,$productId,$attrId);
			if($stock<$qty){
				throw new Exception('庫存不足, 請重新確認 (Requested:'.$qty.$warehouseId.')',503);
			}

			//An array to store all queries
			$stockQueries = [];

			$fromWsStock=\get_goods_stock_by_warehouse($warehouseId,$productId);
			//1) delivery order item
			$stockQueries[]="INSERT INTO ".$this->ecs->table('erp_delivery_item')." SET delivery_id='".$deliveryId."',goods_id='".$productId."',attr_id='".$attrId."', expected_qty='".$qty."',delivered_qty='".$qty."'";

			//2) erp_stock
			//$stockQueries[]="INSERT INTO ".$this->ecs->table('erp_stock')." SET goods_id='".$productId."',goods_attr_id='".$attrId."',qty='".(0-$qty)."',act_time='".gmtime()."',warehouse_id='".$warehouseId."', w_d_name='".$receiver."',w_d_style_id='".$this->getDeliveryStyleIdByName($type)."',order_sn='".$erpOrderSn."',stock_style='d', w_d_sn='".$deliveryInfo['delivery_sn']."',stock=".($fromWsStock-$qty).",act_date='".local_date('Y-m-d',gmtime())."'";

			if (!empty($rma_id)) {
				$stockQueries[]="INSERT INTO ".$this->ecs->table('erp_stock')." SET goods_id='".$productId."',goods_attr_id='".$attrId."',qty='".(0-$qty)."',act_time='".gmtime()."',warehouse_id='".$warehouseId."', w_d_name='".$receiver."',w_d_style_id='".$this->getDeliveryStyleIdByName($type)."',order_sn='".$erpOrderSn."',stock_style='d', w_d_sn='".$deliveryInfo['delivery_sn']."',stock=".($fromWsStock-$qty).",act_date='".local_date('Y-m-d',gmtime())."',rma_id='".$rma_id."'";
			} else {
				$stockQueries[]="INSERT INTO ".$this->ecs->table('erp_stock')." SET goods_id='".$productId."',goods_attr_id='".$attrId."',qty='".(0-$qty)."',act_time='".gmtime()."',warehouse_id='".$warehouseId."', w_d_name='".$receiver."',w_d_style_id='".$this->getDeliveryStyleIdByName($type)."',order_sn='".$erpOrderSn."',stock_style='d', w_d_sn='".$deliveryInfo['delivery_sn']."',stock=".($fromWsStock-$qty).",act_date='".local_date('Y-m-d',gmtime())."'";
			}

			//3) goods_attr_stock
			$stockQueries[]="UPDATE ".$this->ecs->table('erp_goods_attr_stock')." SET goods_qty =goods_qty - ".$qty." where goods_id='".$productId."' and attr_id='".$attrId."' and warehouse_id='".$warehouseId."'";
            $stockQueries[]="UPDATE ".$this->ecs->table('erp_goods_attr')." SET goods_qty =goods_qty - ".$qty." where goods_id='".$productId."' and attr_id='".$attrId."' ";

			//4) delivery order info
			$stockQueries[]="UPDATE ".$this->ecs->table('erp_delivery')." SET delivery_style_id ='".$this->getDeliveryStyleIdByName($type)."', warehouse_id=".$warehouseId.", approved_by = 1, approve_time = ".gmtime().",  delivery_to = '".$receiver."',status=3, post_by='".erp_get_admin_id()."' WHERE delivery_id='".$deliveryId."'";

			//loop through the array queries
			foreach($stockQueries as $query){
				if(!$this->db->query($query)){
					throw new Exception($_LANG['erp_stock_transfer_confirm_failed'],501);
				}
			}

			//5 update the repair orders
			$a=$this->updateRelatedRepairOrders($productId,$qty);
			if($a===false)
				throw new Exception("Error while updating repair orders", 501);

				
		}catch(\Exception $e){
			return ($this->db->query('ROLLBACK')?$e->getMessage():'Error during commit');
		}
		return ($this->db->query('COMMIT')?true:'Error during commit');
	}

	private function updateRelatedRepairOrders($productId,$qty,$status=OrderController::DB_REPAIR_ORDER_STATUS_REPAIRING){
		$orderController = new OrderController();
		//1: select all repair orders with product id and status not resolved
		//Rules: only apply to pending repair orders
		$repairOrders = $this->db->getAll("SELECT repair_id FROM ".$this->ecs->table('repair_orders')." WHERE goods_id=".$productId." AND repair_order_status IN (0,".OrderController::DB_REPAIR_ORDER_STATUS_PENDING.") ORDER BY repair_id DESC");
		if(count($repairOrders)==0) return true;
		foreach($repairOrders as $ro){
			if($qty<=0) break;
			if(!$orderController->updateRepairOrderStatus($ro['repair_id'],$status)) return false;
			$qty--;
		}
		return count($repairOrders);
	}

	public function stockWarehouseTransfer($fromWarehouseId,$toWarehouseId,$products,$remark){
		require_once(ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_goods_attr.php');
		require_once(ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_warehouse.php');
		//Only applicable to one-step auto add and approve

		$wNames = $GLOBALS['db']->getAll("SELECT fw.name as fwname,tw.name as twname FROM ".$GLOBALS['ecs']->table('erp_warehouse')." fw LEFT JOIN ".$GLOBALS['ecs']->table('erp_warehouse')." tw ON tw.warehouse_id=".$toWarehouseId." WHERE fw.warehouse_id=".$fromWarehouseId);
		
		$transferId=\add_transfer();
		$transferInfo=\get_goods_transfer_info($transferId);

		$this->db->query("START TRANSACTION");
		try{
			//1 prepare for transfer record
			
			if($transferId<=0) throw new Exception('Error while creating transferId');
			if($fromWarehouseId==$toWarehouseId){
				throw new Exception($this->lang['erp_wrong_parameter'], 501);				
			}

			$sql="UPDATE ".$this->ecs->table('erp_stock_transfer')." SET warehouse_from='".$fromWarehouseId."',warehouse_to='".$toWarehouseId."',admin_from='".erp_get_admin_id()."', status=3, remark = '".$remark."' WHERE transfer_id='".$transferId."'";
			if(!$this->db->query($sql)){
				throw new Exception($this->lang['erp_stock_transfer_not_exists'].'- From ERP Controller');
			}

			$sql="select user_name from ".$this->ecs->table('admin_user')." where user_id = ".erp_get_admin_id()." ";
			$operator = $this->db->getOne($sql);
		
			//2 loop through products and stock check
			$items=[];
			if($errMsg==''){
				foreach ($products as $pId => $pQty) {
					$attrId = get_attr_id($pId);
					$items[]="(".$transferId.",".$pId.",".$attrId.",".$pQty.")";
					$fromWsStock=get_goods_stock_by_warehouse($fromWarehouseId,$pId);
					$toWsStock=get_goods_stock_by_warehouse($toWarehouseId,$pId);

					if($pQty>$fromWsStock){
						throw new Exception(sprintf($this->lang['erp_stock_transfer_stock_not_enough'],$fromWsStock),501);
					}

					$stockQueries=[];
					//2-2 add to new ws
					//2-2-a stock
					$wdName = $operator;
					$sql="INSERT INTO ".$this->ecs->table('erp_stock')." SET goods_id='".$pId."',goods_attr_id='".$attrId."',qty='".(0-$pQty)."',act_time='".gmtime()."',warehouse_id='".$fromWarehouseId."'";
					$sql.=", w_d_name='".$wdName."', w_d_style_id='".$this->getDeliveryStyleIdByName(self::DB_DELIVERY_STYLE_TRANSFER)."',order_sn='".$transferInfo['transfer_sn']."',stock_style='d',w_d_sn='".$transferInfo['transfer_sn']."',stock=".($fromWsStock-$pQty).",act_date='".local_date('Y-m-d',gmtime())."'";
					$stockQueries[]=$sql;

					//2-2-b attr
			        $sql="select count(*) from ".$this->ecs->table('erp_goods_attr_stock')." where goods_id='".$pId."' and attr_id='".$attrId."' and warehouse_id='".$fromWarehouseId."' ";
			        $item_count = $this->db->getOne($sql);
			        if ($item_count == 0) {
			            $sql="insert into ".$this->ecs->table('erp_goods_attr_stock')." (`attr_id`,`goods_id`,`warehouse_id`, `goods_qty`, `begin_qty`, `delivery_qty`, `warehousing_qty`, `end_qty`) values ( ".$attrId.",".$pId.",".$fromWarehouseId.",".$pQty.",0,0,0,0 )";
			        } else {
			           $sql="UPDATE ".$this->ecs->table('erp_goods_attr_stock')." SET goods_qty =goods_qty -".$pQty." where goods_id='".$pId."' and attr_id='".$attrId."' and warehouse_id='".$fromWarehouseId."'";
			        }

					$stockQueries[]=$sql;

					//2-3 deduct from old ws
					//2-3-a stock
					$wdName = $operator;
					$sql="INSERT INTO ".$this->ecs->table('erp_stock')." SET goods_id='".$pId."',goods_attr_id='".$attrId."',qty='".($pQty)."',act_time='".gmtime()."',warehouse_id='".$toWarehouseId."'";
					$sql.=", w_d_name='".$wdName."', w_d_style_id='".$this->getWarehousingStyleIdByName(self::DB_WAREHOUSING_STYLE_TRANSFER)."',order_sn='".$transferInfo['transfer_sn']."',stock_style='w',w_d_sn='".$transferInfo['transfer_sn']."',stock=".($toWsStock+$pQty).",act_date='".local_date('Y-m-d',gmtime())."'";
					$stockQueries[]=$sql;
					//2-3-b attr
					$sql="select count(*) from ".$this->ecs->table('erp_goods_attr_stock')." where goods_id='".$pId."' and attr_id='".$attrId."' and warehouse_id='".$toWarehouseId."' ";
					$item_count = $this->db->getOne($sql);
					if ($item_count == 0) {
						$sql="insert into ".$this->ecs->table('erp_goods_attr_stock')." (`attr_id`,`goods_id`,`warehouse_id`, `goods_qty`, `begin_qty`, `delivery_qty`, `warehousing_qty`, `end_qty`) values ( ".$attrId.",".$pId.",".$toWarehouseId.",".$pQty.",0,0,0,0 )";
					} else {
						$sql="UPDATE ".$this->ecs->table('erp_goods_attr_stock')." SET goods_qty =goods_qty +".$pQty." where goods_id='".$pId."' and attr_id='".$attrId."' and warehouse_id='".$toWarehouseId."'";
					}

					$stockQueries[]=$sql;

					foreach($stockQueries as $query){
						if(!$this->db->query($query))
							throw new Exception($_LANG['erp_stock_transfer_confirm_failed'],501);
					}

					//2-4 auto resolve rapair_orders for case from Main stock to Temp
					//TODO: determine if this function should be implemented. The main concern is, we can not assume the stock from Main Warehouse is always "good-to-go" for resolving repair orders
					// To enable this feature, un comment the following, and WRITE TEST BEFORE DEPLOYMENT
					/*/auto update related repair order status-----
					$orderController = new Yoho\cms\Controller\OrderController();
					$ros = $orderController->getRepairOrderByProductId($pId);
					if(count($ros)>0){
						$updateCounter = 0;
						foreach($ros as $ro){
							//Just update received qty for repair orders
							if($updateCounter >= $pQty)
								break;
							//proceed update
							if($ro['repair_order_status']==$orderController::DB_REPAIR_ORDER_STATUS_REPAIRING){
								$orderController->updateRepairOrderStatus($ro['repair_id'],$orderController::DB_REPAIR_ORDER_STATUS_REPAIRED);
								$updateCounter++;
							}
						}
					}
					//auto update related repair order status-----*/
				}
			}

			//3 batch insert
			$sql="INSERT INTO ".$this->ecs->table('erp_stock_transfer_item')." (transfer_id,goods_id,attr_id,transfer_qty) VALUES ".implode(',', $items);
			if(!$this->db->query($sql)){
				throw new Exception('Database error during batch insert');
			}
		}catch(Exception $e){
			$this->db->query("ROLLBACK");
			$this->db->query("DELETE FROM ".$this->ecs->table('erp_stock_transfer')." WHERE transfer_id=".$transferId);
			return $e->getMessage();
		}
		admin_log($sn = $wdName."productId:".$pId." txnId:".$transferId, 'edit', 'goods_stock');
		$this->db->query("COMMIT");
		return true;
	}

	public function stockTransfer($fromWarehouseId,$toWarehouseId,$products,$rma_id = null){
		require_once(ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_goods_attr.php');
		require_once(ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_warehouse.php');
		//Only applicable to one-step auto add and approve
		//Main for RMA flow

		$transferId=\add_transfer();
		$transferInfo=\get_goods_transfer_info($transferId);


		$this->db->query("START TRANSACTION");
		try{
			//1 prepare for transfer record
			
			if($transferId<=0) throw new Exception('Error while creating transferId');
			if($fromWarehouseId==$toWarehouseId){
				throw new Exception($this->lang['erp_wrong_parameter'], 501);				
			}

			$sql="UPDATE ".$this->ecs->table('erp_stock_transfer')." SET warehouse_from='".$fromWarehouseId."',warehouse_to='".$toWarehouseId."',admin_from='".erp_get_admin_id()."', status=3 WHERE transfer_id='".$transferId."'";
			if(!$this->db->query($sql)){
				throw new Exception($this->lang['erp_stock_transfer_not_exists'].'- From ERP Controller');
			}

			$toWIsOnSale = $this->db->getOne("SELECT is_on_sale FROM ".$this->ecs->table('erp_warehouse')." WHERE warehouse_id=".$toWarehouseId);
			$wdName = ($toWIsOnSale=='1'?'RMA轉倉到可銷售':'RMA轉倉到暫存');
			//2 loop through products and stock check
			$items=[];
			if($errMsg==''){
				
				foreach ($products as $pId => $pQty) {
					$attrId = get_attr_id($pId);
					$items[]="(".$transferId.",".$pId.",".$attrId.",".$pQty.")";
					$fromWsStock=get_goods_stock_by_warehouse($fromWarehouseId,$pId);
					$toWsStock=get_goods_stock_by_warehouse($toWarehouseId,$pId);

					if($pQty>$fromWsStock){
						throw new Exception(sprintf($this->lang['erp_stock_transfer_stock_not_enough'],$fromWsStock),501);
					}

					$stockQueries=[];
					//2-2 add to new ws
					//2-2-a stock
					$sql="INSERT INTO ".$this->ecs->table('erp_stock')." SET goods_id='".$pId."',goods_attr_id='".$attrId."',qty='".(0-$pQty)."',act_time='".gmtime()."',warehouse_id='".$fromWarehouseId."'";
					$sql.=", w_d_name='".$wdName."', w_d_style_id='".$this->getDeliveryStyleIdByName(self::DB_DELIVERY_STYLE_TRANSFERTOSELLABLE)."',order_sn='".$transferInfo['transfer_sn']."',stock_style='d',w_d_sn='".$transferInfo['transfer_sn']."',stock=".($fromWsStock-$pQty).",act_date='".local_date('Y-m-d',gmtime())."'";
					$sql.=(!empty($rma_id)? ", rma_id='".$rma_id."'" : '');
					$stockQueries[]=$sql;
					//2-2-b attr
					$sql="UPDATE ".$this->ecs->table('erp_goods_attr_stock')." SET goods_qty =goods_qty -".$pQty." where goods_id='".$pId."' and attr_id='".$attrId."' and warehouse_id='".$fromWarehouseId."'";
					$stockQueries[]=$sql;

					//2-3 deduct from old ws
					//2-3-a stock
					$sql="INSERT INTO ".$this->ecs->table('erp_stock')." SET goods_id='".$pId."',goods_attr_id='".$attrId."',qty='".($pQty)."',act_time='".gmtime()."',warehouse_id='".$toWarehouseId."'";
					$sql.=", w_d_name='".$wdName."', w_d_style_id='".$this->getDeliveryStyleIdByName(self::DB_DELIVERY_STYLE_TRANSFERTOSELLABLE)."',order_sn='".$transferInfo['transfer_sn']."',stock_style='w',w_d_sn='".$transferInfo['transfer_sn']."',stock=".($toWsStock+$pQty).",act_date='".local_date('Y-m-d',gmtime())."'";
					$sql.=(!empty($rma_id)? ", rma_id='".$rma_id."'" : '');
					$stockQueries[]=$sql;
					//2-3-b attr
					$sql="UPDATE ".$this->ecs->table('erp_goods_attr_stock')." SET goods_qty =goods_qty +".$pQty." where goods_id='".$pId."' and attr_id='".$attrId."' and warehouse_id='".$toWarehouseId."'";
					$stockQueries[]=$sql;

					foreach($stockQueries as $query){
						if(!$this->db->query($query))
							throw new Exception($_LANG['erp_stock_transfer_confirm_failed'],501);
					}

					//2-4 auto resolve rapair_orders for case from Main stock to Temp
					//TODO: determine if this function should be implemented. The main concern is, we can not assume the stock from Main Warehouse is always "good-to-go" for resolving repair orders
					// To enable this feature, un comment the following, and WRITE TEST BEFORE DEPLOYMENT
					/*/auto update related repair order status-----
					$orderController = new Yoho\cms\Controller\OrderController();
					$ros = $orderController->getRepairOrderByProductId($pId);
					if(count($ros)>0){
						$updateCounter = 0;
						foreach($ros as $ro){
							//Just update received qty for repair orders
							if($updateCounter >= $pQty)
								break;
							//proceed update
							if($ro['repair_order_status']==$orderController::DB_REPAIR_ORDER_STATUS_REPAIRING){
								$orderController->updateRepairOrderStatus($ro['repair_id'],$orderController::DB_REPAIR_ORDER_STATUS_REPAIRED);
								$updateCounter++;
							}
						}
					}
					//auto update related repair order status-----*/
				}
			}

			//3 batch insert
			$sql="INSERT INTO ".$this->ecs->table('erp_stock_transfer_item')." (transfer_id,goods_id,attr_id,transfer_qty) VALUES ".implode(',', $items);
			if(!$this->db->query($sql)){
				throw new Exception('Database error during batch insert');
			}
		}catch(Exception $e){
			$this->db->query("ROLLBACK");
			$this->db->query("DELETE FROM ".$this->ecs->table('erp_stock_transfer')." WHERE transfer_id=".$transferId);
			return $e->getMessage();
		}
		admin_log($sn = $wdName."productId:".$pId." txnId:".$transferId, 'edit', 'goods_stock');
		$this->db->query("COMMIT");
		return true;
	}

	public function getProductStock(){}
	public function getProductErpStock($productId, $attrId = 0, $warehouseId=1, $stockType='attr'){
		//assume warehouse id 1 is the default and always sellable
		$stock = null;
		$stockQueries = [
			'erp_stock'=>'SELECT stock FROM '.$this->ecs->table('erp_stock')." WHERE goods_id=".$productId." AND goods_attr_id='".($attr_id!=0?$attrId:get_attr_id($productId))."' AND warehouse_id=".$warehouseId." ORDER BY stock_id DESC",
			'attr_stock'=>'SELECT'
		];
		foreach ($stockQueries as $key => $query) {
			if($key==$stockType."_stock")
				$stock=$this->db->getCol($query);
		}
		return $stock;
		$db->getOne("select stock from ".$ecs->table('erp_stock')." where goods_id='".$goods_id."' and goods_attr_id='".$attr_id."' and warehouse_id='".$warehouse_id."' order by stock_id desc limit 1");
	}

	public function stockAdd($productIds,$qty,$price,$warehouseId,$wDName,$wDStyleId,$orderSn,$wDSn){
		$_db = $this->db;
		foreach($productIds as $productId=>$productInfo){
			$attrId = get_attr_id($productId);
			$qty = $productInfo['qty'];
			$price = $productInfo['price'];
			
		}
					$goods_id=$warehousing_item['goods_id'];
			$attr_id=$warehousing_item['attr_id'];
			$qty=$warehousing_item['received_qty'];
			$price=$warehousing_item['price'];
			//$sql=;
			$stock=$db->getOne($sql);
			$stock=!empty($stock)?$stock: 0;
			$sql="insert into ".$ecs->table('erp_stock')." set goods_id='".$goods_id."',goods_attr_id='".$attr_id."',qty='".$qty."',price='".$price."',amount=qty*price,act_time='".gmtime()."',warehouse_id='".$warehouse_id."'";
			$sql.=", w_d_name='".$warehousing_from."', w_d_style_id='".$warehousing_style_id."',order_sn='".$order_sn."',stock_style='w',w_d_sn='".$warehousing_sn."',stock=".($stock+$qty).",act_date='".local_date('Y-m-d',gmtime())."'";
			if(!$db->query($sql))
			{
				$GLOBALS['db']->query('ROLLBACK');
				$result['error']=9;
				$result['message']=$_LANG['erp_warehousing_approve_pass_failed'];
				die($json->encode($result));
			}
	}

	/*
	This is a function to determine inventory consumption sequence by supplier PO
	Trigger point is on delivery_qty update in table order_goods
	*/
	public function processConsumptionSequence($deliveryItemInfo,$actionConsume=true,$baseTxn){
		$accountingInfo = [];
		foreach($deliveryItemInfo as $item){
			//for warehouseing_items, there should be received qty
			if(!$item['delivered_qty'] && $item['received_qty']){
				$item['delivered_qty']=$item['received_qty'];
			}
			//consume or stock in
			if($actionConsume){
				$seq = $this->getSupplierStockSequence($item['goods_id'],$item['delivered_qty']);
			}else{
				$item['delivered_qty']=($item['delivered_qty']*-1);
				$seq = $this->getSupplierConsumedSeq($item['order_item_id'],$item['delivered_qty']);
			}
			$accountingInfo[] = $seq;
			$this->addOrderItemConsumption($item['order_item_id'],$item['goods_id'],$seq);
		}

		$accountingAmount = 0;
		foreach ($accountingInfo as $info) {
			if(count($info) <= 0) continue;
			foreach ($info as $data) {
				$accountingAmount = bcadd($accountingAmount, bcmul(round($data['unit_value'], 2), $data['qty'], 2), 2);
			}
		}
		if ($accountingAmount != 0) {
			$accountingController = new AccountingController();
			$param = [
				'from_actg_type_id'	=> $accountingController->getAccountingType(['actg_type_code' => '45000', 'enabled' => 1], 'actg_type_id'), // Cost of Sales
				'to_actg_type_id' 	=> $accountingController->getAccountingType(['actg_type_code' => '14001', 'enabled' => 1], 'actg_type_id'), // Inventory (Owned)
				'currency_code' 	=> 'HKD',
				'amount'			=> $accountingAmount,
			];
			foreach ($baseTxn as $key => $val) {
				$param[$key] = $val;
			}
			$accountingController->insertDoubleEntryTransactions($param);
		}
	}

	private function getSupplierConsumedSeq($orderGoodsId,$qty){
		$data = $this->db->getAll("SELECT supplier_id,consumed_qty,po_unit_cost,po_unit_value FROM ".$this->ecs->table('order_goods_supplier_consumption')." WHERE order_goods_id=".$orderGoodsId." AND consumed_qty>0 ORDER BY consumption_id DESC");
		$output=[];
		foreach($data as $d){
			if($d['consumed_qty']+$qty>=0){
				$output[]=['supplier_id'=>$d['supplier_id'],'qty'=>$qty,'unit_cost'=>$d['po_unit_cost'],'unit_value'=>$d['po_unit_value']];
				break;
			}
			$output[]=['supplier_id'=>$d['supplier_id'],'qty'=>$d['consumed_qty'],'unit_cost'=>$d['po_unit_cost'],'unit_value'=>$d['po_unit_value']];
			$qty+=$d['consumed_qty'];
		}
		return $output;
	}

	public function getSupplierStockSequence($productId,$requestQty){
		//1. get a list of supplier PO records, in DESC order
		$data = $this->db->getAll("SELECT eo.supplier_id,eoi.warehousing_qty as qty,(eoi.price * eoi.order_qty - eoi.cn_amount) / eoi.order_qty as unit_cost, CASE WHEN eo.type = 0 THEN eoi.price ELSE 0 END as unit_value FROM ".$this->ecs->table('erp_order_item')." eoi LEFT JOIN ".$this->ecs->table('erp_order')." eo ON eoi.order_id=eo.order_id WHERE eoi.goods_id = ".$productId." ORDER BY eoi.order_item_id DESC");
		
		//2. get sellable stock of the product
		// 2018-01-05 Anthony: We only count SellableWarehouse stock (只計算銷售倉內有的庫存, 而不是計算已付款未出貨的實際銷售庫存)
		//TODO: how/should we handle cutoff here?
		$stock = $this->getSellableProductQty($productId, false);

		//3. get the PO records which sum is <= $stock
		// by doing it in descending, we can avoid some case like +500 then -500, making the logic to get a wrong slot.
		$dataTobeCounted=[];
		$stockCounter = $stock;
		foreach ($data as $d) {
			if($stockCounter-$d['qty']>0){
				$dataTobeCounted[]=$d;
			}else{
				if($stockCounter>0){
					$d['qty'] = $stockCounter;
					$dataTobeCounted[]=$d;
				}
				break;
			}
			$stockCounter-=$d['qty'];
		}
		//4. now with the latest valid supplier sequence and qty, we can start consuming the requested nowQty
		$suppliers=[];
		foreach (array_reverse($dataTobeCounted,TRUE) as $d) {
			if($d['qty']-$requestQty >= 0){
				//this slot can fulfill the entire request qty, so quitting the loop
				$suppliers[] = ['supplier_id' => $d['supplier_id'], 'qty'=>intval($requestQty),'unit_cost'=>floatval($d['unit_cost']), 'unit_value'=>floatval($d['unit_value'])];
				break;
			}else{
				//this slot can only fulfill part of the request qty, so leaving the remaining for the next slot
				$requestQty-=intval($d['qty']);
                $suppliers[] = ['supplier_id' => $d['supplier_id'], 'qty'=>intval($d['qty']),'unit_cost'=>floatval($d['unit_cost']), 'unit_value'=>floatval($d['unit_value'])];
			}
		}
		return $suppliers;
	}

	private function addOrderItemConsumption($orderItemId,$productId,$suppliers){
		$statment = [];
		if(count($suppliers)<=0)
			return false;
		foreach ($suppliers as $key=>$data) {
			$statment[]="(".$orderItemId.",".$productId.",".$data['supplier_id'].",".$data['qty'].",".$data['unit_cost'].",".$data['unit_value'].")";
		}
		//TODO: handle empty supplier etc...
		return $this->db->query("INSERT IGNORE INTO ".$this->ecs->table('order_goods_supplier_consumption')." (order_goods_id,goods_id,supplier_id,consumed_qty,po_unit_cost,po_unit_value) VALUES ".implode(',', $statment));
	}

	/** Handle pos erp_delivery ERP 出貨單
	 * 
	 */
	public function pos_erp_delivery($order_id, $admin, $pos_mode){
		
		global $ecs, $db, $_LANG;
		if(empty($order_id)) return false;

		// get shop id
		$isWholesale = $_POST['is_wholesale']=='1'?TRUE:FALSE;

		if ($isWholesale == true) {
			$shop_warehouse_id = $this->getMainWarehouseId();
		} else {
			$shop_warehouse_id = $this->getShopWarehouseId();
		}

		$order = order_info($order_id);
		// 新增 ERP 出貨單
		$erp_delivery = array(
			'delivery_style_id' => '3', // 出貨類型: POS銷售
			'create_by'         => empty($admin['user_id']) ? 1 : $admin['user_id'],
			'post_by'           => empty($admin['user_id']) ? 1 : $admin['user_id'],
			'delivery_sn'       => 'POS' . $order['order_id'],
			'warehouse_id'      => $shop_warehouse_id, // 貨倉: 門市
			'delivery_to'       => empty($order['salesperson']) ? 'POS' : $order['salesperson'], // 提貨人
			'order_id'          => $order['order_id'],
			'create_time'       => gmtime(),
			'post_time'         => gmtime(),
			'post_remark'       => $this->escape_string($order['postscript']),
			'status'            => '2', // 出貨單狀態: 主管審核中
		);
		$db->autoExecute($ecs->table('erp_delivery'), $erp_delivery, 'INSERT');
		
		$delivery_id = $db->insert_id();
		$erp_delivery['delivery_id'] = $delivery_id;
		
		/* 插入订单商品 */
		$sql = "INSERT INTO " . $ecs->table('erp_delivery_item') . "( " .
				"delivery_id, goods_id,attr_id, delivered_qty, price, order_item_id) ".
				" SELECT '$delivery_id', goods_id, goods_attr_id, goods_number, goods_price, rec_id ".
				" FROM " .$ecs->table('order_goods') .
				" WHERE order_id = '$order_id' ";
		$db->query($sql);
		
		// 審核 ERP 出貨單
		$this->approve_erp_delivery($erp_delivery, $order, 'POS發貨自動扣除庫存', $admin);
		
        // 正常購買才處理 積分／推薦優惠券
        if (in_array($pos_mode, ['normal', 'ex_normal']))
        {
    		/* 如果订单用户不为空，计算积分，并发给用户；发红包 */
    		if ($order['user_id'] > 0)
    		{
    			/* 取得用户信息 */
    			$user = user_info($order['user_id']);
    			
    			/* 计算并发放积分 */
    			$integral = integral_to_give($order);
    			
    			if(!log_account_change($order['user_id'], 0, 0, intval($integral['rank_points']), intval($integral['custom_points']), sprintf($_LANG['order_gift_integral'], $order['order_sn']), ACT_OTHER, 0, ['sales_order_id' => $order_id]))
    			{
    				$db->query('ROLLBACK');
    				show_message($_LANG['act_false'] . "积分发放失败，请联系管理员！");
    				exit;
    			}
    			
    			/* 发放红包 */
    			if(!send_order_bonus($order['order_id']))
    			{
    				$db->query('ROLLBACK');
    				
    				show_message($_LANG['act_false'] . "红包发放失败，请联系管理员！");
    				exit;
    			}
    			
    			// If the order used a referral coupon, give reward for the referer user
    			$coupons = order_coupons($order['order_id']);
    			if (!empty($coupons))
    			{
    				foreach ($coupons as $coupon)
    				{
    					if ($coupon['referer_id'] > 0)
    					{
    						$referral_integral = 1000;
    						$log_msg = '推薦新會員 ' . $user['user_name'] . ' 訂單 ' . $order['order_sn'] . ' 贈送的積分';
    						if(!log_account_change($coupon['referer_id'], 0, 0, 0, $referral_integral, $log_msg, ACT_OTHER, 0, ['sales_order_id' => $order['order_id']]))
    						{
    							$db->query('ROLLBACK');
    							
								show_message($_LANG['act_false'] . "赠送积分给推荐人失败，请联系管理员！");
								exit;
    						}
    					}
    				}
    			}
    		}
		}

		$action_note = "POS自動發貨";
		if ($pos_mode == "gift") {
			$action_note = "抽獎活動禮物";
		}
		
		// Insert Order action
		$new_order = order_info($order_id);
        order_action($new_order['order_sn'], $new_order['order_status'], $new_order['shipping_status'], $new_order['pay_status'], $action_note, (empty($order['salesperson']) ? 'POS' : $order['salesperson']));

		return true;
	}

	
	function approve_erp_delivery($delivery_info, $order, $approve_remark = '', $admin)
	{
		global $db, $ecs;
		
		$delivery_id = $delivery_info['delivery_id'];
		$delivery_style_id = $delivery_info['delivery_style_id'];
		$warehouse_id = $delivery_info['warehouse_id'];
		$order_id = $delivery_info['order_id'];
		$order_sn = $order['order_sn'];
		$delivery_sn = $delivery_info['delivery_sn'];
		$delivery_to = $delivery_info['delivery_to'];
		
		$db->query('START TRANSACTION');
		
		// lock_table()
		$sql = "UPDATE " . $ecs->table('erp_delivery') .
			" SET locked_by = '".$admin['user_id']."', locked_time = '".gmtime()."', last_act_time = '".gmtime()."', is_locked = '1' " .
			" WHERE delivery_id = '".$delivery_id."'";
		if(!$db->query($sql))
		{
			$db->query('ROLLBACK');
			return false;
		}
		
		// get_delivery_item_info()
		$sql = "SELECT * " .
			"FROM " . $ecs->table('erp_delivery_item') .
			"WHERE delivery_id = '".$delivery_id."'";
		$delivery_item_info = $db->getAll($sql);
		
		foreach($delivery_item_info as $delivery_item)
		{
			$order_item_id = $delivery_item['order_item_id'];
			$delivered_qty = intval($delivery_item['delivered_qty']);
			$sql = "UPDATE " . $ecs->table('order_goods') . 
				" SET delivery_qty = delivery_qty + " . $delivered_qty .
				" WHERE rec_id = '".$order_item_id."'";
			if(!$db->query($sql))
			{
				$db->query('ROLLBACK');
				return false;
			}
		}
		//Supplier inventory consumption sequence
		$baseTxn = [
			'sales_order_id'	=> $order_id,
			'delivery_id'		=> $delivery_id,
			'remark'			=> "訂單 $order_sn 出貨 $delivery_sn",
			'created_by'		=> $admin['user_id'],
		];
		$this->processConsumptionSequence($delivery_item_info, true, $baseTxn);
		$sql = "SELECT sum(goods_price*delivery_qty) as delivery_goods_amount " .
			"FROM " . $ecs->table('order_goods') .
			"WHERE order_id = '".$order_id."'";
		$delivery_goods_amount = $db->getOne($sql);
		$sql = "UPDATE " . $ecs->table('order_info') .
			" SET delivery_goods_amount = '" . $delivery_goods_amount . "'" .
			" WHERE order_id = '".$order_id."'";
		if(!$db->query($sql))
		{
			$db->query('ROLLBACK');
		}
		foreach($delivery_item_info as $delivery_item)
		{
			$goods_id = $delivery_item['goods_id'];
			$attr_id = $delivery_item['attr_id'];
			
			/*UUECS 纠正错误 */
			if($delivery_item['attr_id'] == '0')
			{
				$sql = "SELECT attr_id " .
					"FROM ".$ecs->table('erp_goods_attr') .
					"WHERE goods_id = '".$delivery_item['goods_id']."'";
				$attr_id=$db->getOne($sql);
			}
			$qty = $delivery_item['delivered_qty'];
			$price = $delivery_item['price'];
			$sql = "SELECT stock " .
				"FROM " . $ecs->table('erp_stock') .
				"WHERE goods_id = '" . $goods_id . "' AND goods_attr_id = '" . $attr_id . "' AND warehouse_id = '" . $warehouse_id . "'" .
				"ORDER BY stock_id DESC LIMIT 1";
			$stock = $db->getOne($sql);
			$stock = !empty($stock) ? $stock : 0;
			if($stock < $qty)
			{
				$db->query('ROLLBACK');
				//For wholesale orders, if there is any out of stock items
				//this queries would cancel all AUTO ERP entries creation,
				//so the salesperson can handle Delivery in CMS by normal order flow
				if($order['order_type']== OrderController::DB_ORDER_TYPE_WHOLESALE){
					//1 order status
					$db->query('UPDATE '.$ecs->table('order_info')." SET shipping_status=".SS_UNSHIPPED." WHERE order_id=".$order['order_id']." AND order_type='".OrderController::DB_ORDER_TYPE_WHOLESALE."'");
					//2remove deliver and items
					$db->query("DELETE FROM ".$ecs->table('erp_delivery')." WHERE delivery_id='".$delivery_id."'");
					$db->query("DELETE FROM ".$ecs->table('erp_delivery_item')." WHERE delivery_id='".$delivery_id."'");
				}
				return false;
			}
			$sql="INSERT INTO " . $ecs->table('erp_stock') .
				" SET goods_id = '".$goods_id."', goods_attr_id = '".$attr_id."', qty = '".(0-$qty)."', price='".$price."', amount = qty*price, act_time = '".gmtime()."', warehouse_id = '".$warehouse_id."', w_d_name = '".$delivery_to."', w_d_style_id = '" . $delivery_style_id . "', order_sn = '".$order_sn."', stock_style = 'd', w_d_sn = '".$delivery_sn."', stock = ".($stock-$qty).", act_date = '".local_date('Y-m-d',gmtime())."'";
			if(!$db->query($sql))
			{
				$db->query('ROLLBACK');
				return false;
			}
		}
		foreach($delivery_item_info as $delivery_item)
		{
			$goods_id = $delivery_item['goods_id'];
			$attr_id = $delivery_item['attr_id'];
			$qty = intval($delivery_item['delivered_qty']);
			if(empty($attr_id))
			{
				$sql = "SELECT attr_id " .
					"FROM " . $ecs->table('erp_goods_attr') .
					"WHERE goods_id = '".$goods_id."' AND goods_attr_id_list = '$$'";
				$attr_id = $db->getOne($sql);
			}
			$sql = "UPDATE " . $ecs->table('erp_goods_attr') .
				" SET goods_qty = goods_qty - " . $qty .
				" WHERE goods_id = '" . $goods_id . "' AND attr_id = '" . $attr_id . "'";
			if(!$db->query($sql))
			{
				$db->query('ROLLBACK');
				return false;
			}
			$sql = "UPDATE " . $ecs->table('erp_goods_attr_stock') .
				" SET goods_qty = goods_qty - " . $qty .
				" WHERE goods_id = '" . $goods_id . "' AND attr_id = '" . $attr_id . "' AND warehouse_id = '" . $warehouse_id . "'";
			if(!$db->query($sql))
			{
				$db->query('ROLLBACK');
				return false;
			}
		}
		foreach($delivery_item_info as $delivery_item)
		{
			$goods_id = $delivery_item['goods_id'];
			$qty = intval($delivery_item['delivered_qty']);
			// Out of stock notification
			$new_quantity = $this->getSellableProductQty($goods_id);
			if ($new_quantity == 0)
			{
				if (@include_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_notifications.php'))
				{
					notify_goods_stock($db->getRow("SELECT goods_id, goods_name, goods_sn FROM " . $ecs->table('goods') . " WHERE goods_id = '$goods_id'"), false);
				}
				else
				{
					error_log(__FILE__.':'.__LINE__.' Cannot include lib_notifications.php');
				}
			}
			// Update sales count
			$sql = "UPDATE " . $ecs->table('goods') .
				" SET sales = sales + " . $qty .
				" WHERE goods_id = '" . $goods_id . "'";
			if(!$db->query($sql))
			{
				$db->query('ROLLBACK');
				return false;
			}
		}
		
		$sql = "UPDATE " . $ecs->table('erp_delivery') .
			" SET status = '3', approve_time = '".gmtime()."', approved_by = '".$admin['user_id']."', approve_remark = '".$approve_remark."'" .
			" WHERE delivery_id = '".$delivery_id."'";
		if(!$db->query($sql))
		{
			$db->query('ROLLBACK');
			return false;
		}
		else
		{
			$db->query('COMMIT');
			return true;
		}
	}

	function get_erp_order_info($order_id)
	{
		global $db, $ecs;
		
		$sql = "select eo.*, SUM(eoi.price * eoi.order_qty) as order_amount, SUM(eoi.shipping_price) as shipping_amount from ".$ecs->table('erp_order')." eo " .
				"left join ".$ecs->table('erp_order_item')." eoi ON eoi.order_id = eo.order_id ".
				"where eo.order_id = ".$order_id." GROUP BY eo.order_id";
		$res = $db->getRow($sql);

		return $res;
	}

	function update_erp_order_warehouse_id($order_id,$warehouse_id)
	{
		global $db, $ecs;
		
		$sql = "update ".$ecs->table('erp_order')." set warehouse_id = ".$warehouse_id." where order_id = ".$order_id." ";
		$res = $db->getRow($sql);

		return $res;
	}


	function approve_erp_warehousing($warehousing_info, $approve_remark = '', $admin, $baseTxn)
	{
		global $db, $ecs;
		
		$warehousing_id = $warehousing_info['warehousing_id'];
		$warehousing_style_id = $warehousing_info['warehousing_style_id'];
		$warehouse_id = $warehousing_info['warehouse_id'];
		$order_id = $warehousing_info['order_id'];
		$warehousing_sn = $warehousing_info['warehousing_sn'];
		$warehousing_from = $warehousing_info['warehousing_from'];
		
		if (($warehousing_style_id==1) && (!empty($order_id)))
		{
			$sql="select order_sn from ".$ecs->table('erp_order')." where order_id='".$order_id."'";
			$order_sn=$db->getOne($sql);
		}
		elseif (($warehousing_style_id==2) || ($warehousing_style_id==3))
		{
			$sql="select order_sn from ".$ecs->table('order_info')." where order_id='".$order_id."'";
			$order_sn=$db->getOne($sql);
		}
		else
		{
			$order_sn='';
		}
		
		$db->query('START TRANSACTION');
		
		// lock_table()
		$sql = "UPDATE " . $ecs->table('erp_warehousing') .
			" SET locked_by = '".$admin['user_id']."', locked_time = '".gmtime()."', last_act_time = '".gmtime()."', is_locked = '1' " .
			" WHERE warehousing_id = '".$warehousing_id."'";
		if(!$db->query($sql))
		{
			$db->query('ROLLBACK');
			return false;
		}
		
		// get_warehousing_item_info()
		$sql = "SELECT * " .
			"FROM " . $ecs->table('erp_warehousing_item') .
			"WHERE warehousing_id = '".$warehousing_id."'";
		$warehousing_item_info = $db->getAll($sql);
		
		// Fix attr_id
		foreach ($warehousing_item_info as $key => $warehousing_item)
		{
			if(empty($warehousing_item['attr_id']))
			{
				$sql = "SELECT attr_id " .
					"FROM " . $ecs->table('erp_goods_attr') .
					"WHERE goods_id = '".$goods_id."' AND goods_attr_id_list = '$$'";
				$warehousing_item_info[$key]['attr_id'] = $db->getOne($sql);
			}
		}
		
		if($warehousing_style_id==1)
		{
			foreach($warehousing_item_info as $warehousing_item)
			{
				$order_item_id=$warehousing_item['order_item_id'];
				$received_qty=$warehousing_item['received_qty'];
				$sql="update ".$ecs->table('erp_order_item')." set warehousing_qty=warehousing_qty+".$received_qty." where order_item_id='".$order_item_id."'";
				if(!$db->query($sql))
				{
					$db->query('ROLLBACK');
					return false;
				}
			}
		}
		elseif($warehousing_style_id==2 ||$warehousing_style_id==3)
		{
			foreach($warehousing_item_info as $warehousing_item)
			{
				$order_item_id=$warehousing_item['order_item_id'];
				$received_qty=$warehousing_item['received_qty'];
				$sql="update ".$ecs->table('order_goods')." set delivery_qty=delivery_qty-".$received_qty." where rec_id='".$order_item_id."' and delivery_qty >= ".$received_qty.""; // "and delivery_qty >= $received_qty" added by howang so MySQL won't report error when delivery_qty goes below 0
				if(!$db->query($sql))
				{
					return false;
				}
				$sql="select order_id from ".$ecs->table('order_goods')." where rec_id='".$order_item_id."'";
				$shop_order_id=$db->getOne($sql);
				$sql="select sum(goods_price*delivery_qty) as delivery_goods_amount from ".$GLOBALS['ecs']->table('order_goods')." where order_id='".$shop_order_id."'";
				$delivery_goods_amount=$GLOBALS['db']->getOne($sql);
				$sql="update ".$GLOBALS['ecs']->table('order_info')." set delivery_goods_amount='".$delivery_goods_amount."' where order_id='".$shop_order_id."'";
				if(!$db->query($sql))
				{
					return false;
				}
			}
			//Supplier inventory consumption sequence
			$this->processConsumptionSequence($warehousing_item_info,false,$baseTxn);
		}
		foreach($warehousing_item_info as $warehousing_item)
		{
			$goods_id=$warehousing_item['goods_id'];
			$attr_id=$warehousing_item['attr_id'];
			$qty=$warehousing_item['received_qty'];
			$price=$warehousing_item['price'];
			$sql="select stock from ".$ecs->table('erp_stock')." where goods_id='".$goods_id."' and goods_attr_id='".$attr_id."' and warehouse_id='".$warehouse_id."' order by stock_id desc limit 1";
			$stock=$db->getOne($sql);
			$stock=!empty($stock)?$stock: 0;
			$sql="insert into ".$ecs->table('erp_stock')." set goods_id='".$goods_id."',goods_attr_id='".$attr_id."',qty='".$qty."',price='".$price."',amount=qty*price,act_time='".gmtime()."',warehouse_id='".$warehouse_id."'";
			$sql.=", w_d_name='".$warehousing_from."', w_d_style_id='".$warehousing_style_id."',order_sn='".$order_sn."',stock_style='w',w_d_sn='".$warehousing_sn."',stock=".($stock+$qty).",act_date='".local_date('Y-m-d',gmtime())."'";
			if(!$db->query($sql))
			{
				$db->query('ROLLBACK');
				return false;
			}
		}
		// Calculate Average 現貨 cost
		foreach($warehousing_item_info as $warehousing_item)
		{
			$goods_id=$warehousing_item['goods_id'];
			$qty=$warehousing_item['received_qty'];
			$price=$warehousing_item['price'];
			if ($price != 0)
			{
				$sql = "SELECT cost FROM " . $ecs->table('goods') . " WHERE goods_id = '".$goods_id."'";
				$original_cost = $db->getOne($sql);
				//TODO: clarify business logic, should we exclude non-sellable for cost calculation?
				$sql = "SELECT sum(goods_qty) as goods_number FROM " . $ecs->table('erp_goods_attr') . " WHERE goods_id = '".$goods_id."'";
				$original_qty = $db->getOne($sql);
				$new_cost = ($original_cost * $original_qty + $price * $qty) / ($original_qty + $qty);
				if (is_nan($new_cost)) $new_cost = $original_cost;
				if ($original_cost != $new_cost)
				{
					$sql = "UPDATE " . $ecs->table('goods') . " SET cost = " . $new_cost . " WHERE goods_id = '" . $goods_id . "'";
					if(!$db->query($sql))
					{
						$db->query('ROLLBACK');
						return false;
					}
					// Log the cost changes
					$GLOBALS['db']->autoExecute($GLOBALS['ecs']->table('goods_cost_log'), array(
						'goods_id' => $goods_id,
						'old_cost' => $original_cost,
						'new_cost' => $new_cost,
						'update_type' => 1,
						'update_time' => gmtime(),
						'admin_id' => $_SESSION['admin_id'],
						'warehousing_id' => $warehousing_id,
						'warehousing_sn' => $warehousing_info['warehousing_sn']
					), 'INSERT');
				}
			}
		}
		foreach($warehousing_item_info as $warehousing_item)
		{
			$goods_id=$warehousing_item['goods_id'];
			$attr_id=$warehousing_item['attr_id'];
			$qty=$warehousing_item['received_qty'];
			if(empty($attr_id))
			{
				$sql="select attr_id from ".$ecs->table('erp_goods_attr')." where goods_id='".$goods_id."' and goods_attr_id_list='$$'";
				$attr_id=$db->getOne($sql);
			}
			$sql="update ".$ecs->table('erp_goods_attr')." set goods_qty=goods_qty+".$qty." where goods_id='".$goods_id."' and attr_id='".$attr_id."'";
			if(!$db->query($sql))
			{
				$db->query('ROLLBACK');
				return false;
			}
			$sql="update ".$ecs->table('erp_goods_attr_stock')." set goods_qty =goods_qty +".$qty." where goods_id='".$goods_id."' and attr_id='".$attr_id."' and warehouse_id='".$warehouse_id."'";
			if(!$db->query($sql))
			{
				$db->query('ROLLBACK');
				return false;
			}
		}
		foreach($warehousing_item_info as $warehousing_item)
		{
			$goods_id = $warehousing_item['goods_id'];
			$received_quantity = $warehousing_item['received_qty'];
			// Back in stock notification
			$new_quantity = $this->getSellableProductQty($goods_id);
			if ($new_quantity == $received_quantity)
			{
				if (@include_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_notifications.php'))
				{
					notify_goods_stock($db->getRow("SELECT goods_id, goods_name, goods_sn FROM " . $ecs->table('goods') . " WHERE goods_id = '$goods_id'"), true);
				}
				else
				{
					error_log(__FILE__.':'.__LINE__.' Cannot include lib_notifications.php');
				}
			}
			// Update sales count
			if ((!empty($order_id)) && (($warehousing_style_id == 2) || ($warehousing_style_id == 3)))
			{
				$sql = "UPDATE " . $ecs->table('goods') .
					" SET sales = sales - " . $received_quantity .
					" WHERE goods_id = '" . $goods_id . "'";
				if(!$db->query($sql))
				{
					return false;
				}
			}
		}
		
		$sql="update ".$ecs->table('erp_warehousing')." set status='3',approve_time='".gmtime()."',approved_by='".$admin['user_id']."',approve_remark='".$approve_remark."' where warehousing_id='".$warehousing_id."'";
		
		if(!$db->query($sql))
		{
			$db->query('ROLLBACK');
			return false;
		}
		else
		{
			$db->query('COMMIT');
			return true;
		}
	}

	function incoming_warehousing_report($date, $group_data = false)
	{
		global $ecs, $db;

		$date_start = local_strtotime(!empty($date['start_date']) ? $date['start_date'] : "-7 days midnight");
		$date_end = !empty($date['end_date']) ? local_strtotime($date['end_date']) : local_strtotime("today midnight") - 1;

		$warehousing_sql = "SELECT au.user_name, COUNT(DISTINCT ew.warehousing_id) as total_order, SUM(ewi.received_qty) as total_items ".($group_data ? ", au.user_id as admin_id ": "").
			"FROM " . $ecs->table("erp_warehousing") . " ew " .
			"LEFT JOIN " . $ecs->table("erp_warehousing_item") . " ewi ON ew.warehousing_id = ewi.warehousing_id " .
			"LEFT JOIN " . $ecs->table("admin_user") . " au ON ew.approved_by = au.user_id " .
			"WHERE warehousing_style_id = 1 AND approve_time BETWEEN $date_start AND $date_end " .
			"GROUP BY ew.approved_by";

		$warehousing_table = $db->getAll($warehousing_sql);
		$result = [];
		if($group_data) {
			$result = [];
			$result['all'] = [
				'total_order' => 0,
				'total_items' => 0,
				'user_name'   => '所有',
				'admin_id'     => 0
			];
			foreach($warehousing_table as $admin) {
				$result['all']['total_order'] += $admin['total_order'];
				$result['all']['total_items'] += $admin['total_items'];
				$result[$admin['admin_id']] = $admin;
			}
		} else {
			$result = $warehousing_table;
		}
		return $result;
	}

	public function po_stock_in_import_data()
    {
		global $db, $ecs, $userController, $_CFG;
		
		//echo 'po_stock_in_import_data';
		//exit;

        $stocktake_data = $_REQUEST['stocktake_data'];
        $stocktake_data = explode(PHP_EOL, $stocktake_data);
       
        if (empty($stocktake_data)) {
            return false;
        }

        // combine product for the duplicateion
        // get all goods sn
        $result = [];
        $update_data = [];
        $stocktake_goods_sns = [];
        foreach ($stocktake_data as $stocktake) {
            if (trim($stocktake) != '') {
                $stocktake_info = explode(',', $stocktake);
                if (sizeof($stocktake_info) != 2) {
                    $result['error'][] = '['.$stocktake.']資料格式錯誤!';
                }
                
                $stocktake_sku = $stocktake_info[0];
                $theFirstChar = substr($stocktake_sku, 0, 1);
                if ($theFirstChar == '0') {
                    $stocktake_sku_temp = substr($stocktake_sku, 1);
                    // try to search whitout 0
                    // checking product exist
                    $sql = "select count(*) from ".$ecs->table('goods')." where goods_sn = '".$stocktake_sku_temp."' " ;
                    $count = $db->getOne($sql);
                    
                    if ($count == 1) {
                        $stocktake_sku = $stocktake_sku_temp;
                    }
                }
               
                if (!in_array($stocktake_sku, $stocktake_goods_sns)) {
                    $stocktake_goods_sns[] = $stocktake_sku;
                }

                $update_data[$stocktake_sku]['goods_sn'] = $stocktake_sku;

                $update_data[$stocktake_sku]['stock'] += $stocktake_info[1];
            }
        }

        if (isset($result['error'])) {
            return $result;
        }

        if (empty($stocktake_goods_sns)) {
            return $result['error'] = '資料格式錯誤!';
        }

        // checking product exist
        $sql = "select goods_sn,goods_id from ".$ecs->table('goods')." where goods_sn in ('".implode("','", $stocktake_goods_sns)."' )" ;
        $res = $db->getAll($sql);
        $goods_info_by_sn = [];
        foreach ($res as $goods) {
            $goods_info_by_sn[$goods['goods_sn']] = $goods['goods_id'];
            $update_data[$goods['goods_sn']]['goods_id'] = $goods['goods_id'];
        }

        foreach ($stocktake_goods_sns as $stocktake_goods_sn) {
            if (!array_key_exists($stocktake_goods_sn, $goods_info_by_sn)) {
                $result['error'][] = '貨號['.$stocktake_goods_sn.'] 在系統找不到!';
            }
        }

        // if (isset($result['error'])) {
        //     return $result;
		// }
		
		$update_data_temp = [];
		foreach ($update_data as $goods) {
			$update_data_temp[] = $goods;
		}
		$update_data = $update_data_temp;

		$resAr = [];
		$resAr['stocktake'] = $update_data;
		$resAr['error'] = implode('<br>',$result['error']);
		$resAr['import_time'] = local_date('Y-m-d H:i:s');
		// echo '<pre>';
		// print_R($stocktake_goods_sns);
		// print_R($goods_info_by_sn);
		// print_r($update_data);
		// print_r($resAr);
		
		//$erp_order_id = $_REQUEST['erp_order_id'];
		//$passed = 
		//$this->save_erp_order_cross_check_error_history($erp_order_id);

		return $resAr;
	}

	public function save_erp_order_cross_check_history()
    {
        global $db, $ecs, $_LANG;

        $sql="insert into ".$GLOBALS['ecs']->table('erp_order_cross_check_log')." (erp_order_id,passed,operator,log,add_time) values ( 
            '".$_REQUEST['erp_order_id'].
            "','".$_REQUEST['passed'].
			"','".$_REQUEST['operator'].
            "','".$_REQUEST['log']."','".gmtime()."')";
        
        //echo $sql;

        $db->query($sql);
        
        return true;
	}
	
	public function update_erp_order_cross_check_history()
    {
        global $db, $ecs, $_LANG;

        $sql="update ".$GLOBALS['ecs']->table('erp_order_cross_check_log')." set warehousing_id = ".$_REQUEST['warehousing_id']." where erp_order_id = ".$_REQUEST['erp_order_id']." and warehousing_id is null ";
       
        $db->query($sql);
        
        return true;
    }
	
	function get_goods_info($goods_id)
	{
		global $db, $ecs, $userController, $_CFG;

		$sql = "select goods_id,goods_sn,goods_name,goods_thumb,goods_width,goods_height,goods_length,goods_weight from ".$ecs->table("goods")." where goods_id = ".$goods_id." ";
		$result = $db->getRow($sql);

		return $result;
	}

	function save_goods_info()
	{
		global $db, $ecs, $userController, $_CFG;

		$goods_length = !empty($_POST['goods_length']) ? $_POST['goods_length'] : 0;
		$goods_width  = !empty($_POST['goods_width']) ? $_POST['goods_width'] : 0;
		$goods_height = !empty($_POST['goods_height']) ? $_POST['goods_height']: 0;
		$goods_weight = !empty($_POST['goods_weight']) ? $_POST['goods_weight']: 0;
		if ($_POST['goods_id']) {
			$sql = "update ".$ecs->table("goods")." set goods_length = '".$goods_length."', goods_width = '".$goods_width."', goods_height = '".$goods_height."',  goods_weight = '".$goods_weight."' where goods_id = ".$_POST['goods_id']." ";
			$db->query($sql);
		}

		return true;
	}

	function get_erp_order_cross_check_history($warehousing_id)
	{
		global $db, $ecs, $userController, $_CFG;

		$sql = "select eoccl.*, eo.order_sn, au.avatar,au.user_name, ew.warehousing_sn 
				from ".$ecs->table('erp_order_cross_check_log')." eoccl 
				left join ".$ecs->table('admin_user')." au on (eoccl.operator = au.user_id) 
				left join ".$ecs->table('erp_warehousing')." ew on (eoccl.warehousing_id = ew.warehousing_id) 
				left join ".$ecs->table('erp_order')." eo on (eo.order_id = eoccl.erp_order_id) 
				where eoccl.warehousing_id = '".$warehousing_id."' ";
		$histories = $db->getAll($sql);

		foreach ($histories as &$history){
			$history['add_time'] = local_date($_CFG['time_format'], $history['add_time']);
		}

		return $histories;
	}

	function update_credit_note($param)
	{
		if (empty($param['cn_id']) || empty($param['txn_info'])) return false;
		$cn_id = $param['cn_id'];
		$txn_info = $param['txn_info'];
		unset($param['cn_id']);
		unset($param['txn_info']);
		if (empty($param)) return false;

		$GLOBALS['db']->query('START TRANSACTION');
		if($GLOBALS['db']->autoExecute($GLOBALS['ecs']->table('erp_credit_note'), $param, 'UPDATE', "cn_id = '" . $cn_id . "'"))
		{
			$GLOBALS['db']->query('COMMIT');
			$txn_info['cn_id'] = $cn_id;
			return self::insert_credit_note_transaction($txn_info);
		}
		else
		{
			$GLOBALS['db']->query('ROLLBACK');
			return false;
		}
	}

	function insert_credit_note_transaction($param)
	{
		if (empty($param)) return false;
		if (!erp_param_contains_keys($param, array(
			'cn_id', 'amount', 'remark'
		))) return false;

		if (empty($param['related_cn_txn_id'])) {
			$accountingController = new AccountingController();
			$doubleEntry = [
				'from_actg_type_id'	=> $accountingController->getAccountingType(['actg_type_code' => '43002', 'enabled' => 1], 'actg_type_id'), // Rebates from Supplier
				'to_actg_type_id' 	=> $accountingController->getAccountingType(['actg_type_code' => '23101', 'enabled' => 1], 'actg_type_id'), // AP - Supplier
				// TODO: Credit note base on foreign currency
				// 'currency_code' => $param['currency_code'],
				'currency_code' 	=> 'HKD',
				'amount' 			=> $param['amount'],
				'txn_date' 			=> $param['txn_date'],
				'created_by' 		=> $param['created_by'],
				'erp_order_id' 		=> $param['erp_order_id'],
				'supplier_id' 		=> $param['supplier_id'],
				'remark'			=> $param['txn_remark'],
				'related_txn_id'	=> $param['actg_txn_id'],
			];
			$related_txn_id = $accountingController->insertDoubleEntryTransactions($doubleEntry);
			if (!$related_txn_id) {
				return false;
			}
			if (empty($param['actg_txn_id'])) {
				$param['actg_txn_id'] = $related_txn_id;
			}
		}
		if ($GLOBALS['db']->autoExecute('actg_credit_note_transactions', $param, 'INSERT')) {
			$cn_txn_id = $GLOBALS['db']->insert_id();
			if (empty($param['related_cn_txn_id'])) {
				$GLOBALS['db']->query("UPDATE `actg_accounting_transactions` SET cn_txn_id = $cn_txn_id WHERE related_txn_id = $related_txn_id OR actg_txn_id = $related_txn_id");
			}
			return $cn_txn_id;
		}

		return false;
	}

	function create_erp_order_revise($order_id, $items, $submit = false)
	{
		global $db, $ecs;

		$revise_id = false;
		$revise = [
			'order_id' 	=> $order_id,
			'create_by'	=> $_SESSION['admin_id'],
			'status'	=> $submit ? PO_REVISE_SUBMITTED : PO_REVISE_CREATED,
		];

		if ($db->autoExecute($ecs->table('erp_order_revise'), $revise, 'INSERT')) {
			$revise_id = $db->insert_id();
			$db->query("UPDATE " . $ecs->table('erp_order') . " SET revise_id = $revise_id WHERE order_id = $order_id");
			$res = $db->getAll("SELECT order_item_id, goods_id, original_price, original_shipping_price, order_qty FROM " . $ecs->table('erp_order_item') . " WHERE order_id = $order_id");
			foreach ($res as $row) {
				$old_items[$row['order_item_id']] = [
					'price'		=> $row['original_price'],
					'ship' 		=> $row['original_shipping_price'],
					'qty' 		=> $row['order_qty'],
					'goods_id'	=> $row['goods_id'],
				];
			}
			foreach ($items as $order_item_id => $data) {
				$item = [
					'revise_id'				=> $revise_id,
					'order_item_id' 		=> $order_item_id,
					'old_price' 			=> $old_items[$order_item_id]['price'],
					'old_shipping_price'	=> $old_items[$order_item_id]['ship'],
					'old_order_qty' 		=> $old_items[$order_item_id]['qty'],
					'goods_id'				=> $old_items[$order_item_id]['goods_id'],
					'new_price' 			=> $data['price'],
					'new_shipping_price'	=> $data['ship'],
					'new_order_qty' 		=> $data['qty'],
				];
				$db->autoExecute($ecs->table('erp_order_revise_item'), $item, 'INSERT');
			}
		}

		return $revise_id;
	}

	function get_erp_order_revise_info($revise_id)
	{
		global $db, $ecs;
		if (empty($revise_id)) return false;

		$revise = $db->getRow("SELECT * FROM " . $ecs->table('erp_order_revise') . " WHERE revise_id =  $revise_id");
		if (empty($revise)) return false;

		$revise_items = $db->getAll("SELECT * FROM " . $ecs->table('erp_order_revise_item') . " WHERE revise_id = $revise_id ORDER BY order_item_id DESC");
		foreach ($revise_items as $item) {
			$revise['items'][$item['order_item_id']] = [
				'price'             	=> $item['new_price'],
				'goods_id'				=> $item['goods_id'],
				'shipping_price'    	=> $item['new_shipping_price'],
				'order_qty'         	=> $item['new_order_qty'],
				'old_price'         	=> $item['old_price'],
				'old_shipping_price'	=> $item['old_shipping_price'],
				'old_order_qty'         => $item['old_order_qty'],
			];
		}
		return $revise;
	}
}
