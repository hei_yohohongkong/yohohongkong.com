<?php

/***
* VipController.php
* by Anthony 20170720
*
* Vip controller
***/

namespace Yoho\cms\Controller;
use Yoho\cms\Model;
if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

class VipController extends YohoBaseController
{
    protected $vip_id = 2; //TODO: Default vip is id 2.

    public function __construct($vip_id){
		parent::__construct();
        if(!empty($vip_id)) $this->vip_id  = $vip_id;
	}

    function get_user_ranks_list()
    {
         $sql = "SELECT rank_id, rank_name, min_points " .
               " FROM " . $GLOBALS['ecs']->table('user_rank') .
               " ORDER BY min_points ASC";
        $all_ranks = $GLOBALS['db']->getAll($sql);

        return $all_ranks;
    }

    function get_vip_info_with_user($user_id, $user_rank = 0, $rank_points = 0)
    {
        $v_id = $this->vip_id;

        $sql = "SELECT * FROM".$this->ecs->table('user_rank')." WHERE rank_id = $v_id LIMIT 1";
        $vip = $this->db->getRow($sql);

        if(!$vip) return false;

        // Handle user point rate
        if($user_rank == $v_id || $rank_points >= $vip['min_points'])
        {
            $vip['point_rate'] = "100%";
            $vip['point_diff'] = "0";
        } else {
            $vip['point_rate'] = (intval($rank_points) / intval($vip['min_points']) * 100)."%";
            $vip['point_diff'] = intval($vip['min_points']) - intval($rank_points);
        }
        $vip['display_process'] = empty($user_id) ? false : true;

        return $vip;
    }

    function get_vip_id()
    {
        return $this->vip_id;
    }

    function get_vip_goods_sql()
    {
        $sql = " AND g.goods_id IN (SELECT goods_id FROM ".$this->ecs->table('member_price')." WHERE user_rank = '".$this->vip_id."' ) ";

        return $sql;
    }

    function get_goods_page_vip_info()
    {
        $is_vip = false;
        $vip_banner = array();
        if($_SESSION['user_id']>0 && $_SESSION['user_rank'] == 2) $is_vip = true;
        if(!$is_vip)
        {
            /* Is not login */
            if(empty($_SESSION['user_id']))
            {
                $vip_banner['img'] = '/yohohk/img/goods_vip_banner.png';
                $vip_banner['url'] = '/user.php?act=login';
            } else {
                $vip_banner['img'] = '/yohohk/img/goods_vip_banner.png';
                $vip_banner['url'] = '/vip';
            }
        }
        $vip = array(
            'is_vip'     => $is_vip,
            'vip_banner' => $vip_banner
        );
        return $vip;
    }

    function get_vip_goods($cat_id, $brand_id, $min, $max, $ext, $size, $page, $sort, $order, $is_promote = 0, $display = 'grid',$tags_goods = array())
    {
        global $ecs, $db;

        /* Get SQL format */
        $ext = $this->get_vip_goods_sql();
        $children = get_children($cat_id);
        $categories = ($cat_id > 0) ? ' AND ' . get_children($cat_id) : '';
        $brand      = $brand_id ? " AND g.brand_id = '$brand_id'" : '';
        $min_price  = $min != 0  ? " AND IFNULL(mp.user_price, g.shop_price * '$_SESSION[discount]') >= '$min'" : '';
        $max_price  = $max != 0 || $min < 0  ? " AND IFNULL(mp.user_price, g.shop_price * '$_SESSION[discount]') <= '$max'" : '';
        $where      = " g.is_delete = 0 AND g.is_hidden = 0 AND g.is_on_sale = 1 AND g.is_alone_sale = 1 $ext ";
        $categoryController = new CategoryController();

        /* Get VIP goods category */
        $cat_menu = $categoryController->getLeftCatMenu($cat_id, "vip", $where, 0);
        $cperma = $categoryController->getCategoryPermaLink($cat_id, $cat_menu);

        /* Get VIP price range */
        hw_assign_price_range("g.is_delete = 0 AND g.is_hidden = 0 AND g.is_on_sale = 1 AND g.is_alone_sale = 1 $ext ".
                    "AND (( 1 " . $categories . $brand. " )) ", $_SESSION['user_rank']);

        /* Get VIP goods brands */
        // 1. Get brand
        $fsql = "SELECT b.brand_logo, b.brand_id, IFNULL(bl.brand_name, b.brand_name) as brand_name, count(b.brand_id) as num, bperma " .
                "FROM " . $ecs->table('goods') . " AS g " .
                "LEFT JOIN " . $ecs->table('brand') . " AS b ON g.brand_id = b.brand_id " .
                "LEFT JOIN " . $ecs->table('brand_lang') . "as bl ON bl.brand_id = b.brand_id AND bl.lang = '" . $_CFG['lang'] . "' " .
                "LEFT JOIN (SELECT id, table_name, lang, perma_link as bperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") bpl ON bpl.id = b.brand_id  AND bpl.table_name = 'brand' AND bpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
                "WHERE $where " . //判断总数  $where
                "AND (( 1 " . $categories ." ) ) " .
                "AND b.is_show = 1 " .
                "GROUP BY b.brand_id ORDER BY b.sort_order ASC, b.brand_id ASC";
        $brands = $db->getAll($fsql);

        // 2. Create brands filter data
        foreach($brands as $key => $val)
        {
            $brands[$key]['brand_id'] = $val['brand_id'];
            $brands[$key]['brand_name'] = $val['brand_name'];
            $brands[$key]['num'] = $val['num'];
            $brands[$key]['brand_logo'] = empty($val['brand_logo']) ? null: '/' . DATA_DIR . '/brandlogo/' . $val['brand_logo'];
            $brands[$key]['url'] = build_uri('vip', array('cid' => $cat_id, 'cperma' => $cperma, 'bid' => $val['brand_id'], 'bperma' => $val['bperma']));

            if ($brand_id == $val['brand_id'])
            {
                $brands[$key]['selected'] = 1;
                $brands[$key]['url'] = build_uri('vip', array('cid' => $cat_id, 'cperma' => $cperma, 'bid' => 0));
            }
            else
            {
                $brands[$key]['selected'] = 0;
            }
        }

        /* Get VIP goods count */
        $sql_where = "g.is_delete = 0 AND g.is_hidden = 0 AND g.is_on_sale = 1 AND g.is_alone_sale = 1 $ext ".
                    "AND (( 1 " . $categories . $brand . $min_price . $max_price. " )) ";
        $sql   = "SELECT COUNT(*) FROM " .$ecs->table('goods'). " AS g ".
            " LEFT JOIN  " . $ecs->table('member_price'). " AS mp ON g.goods_id = mp.goods_id AND mp.user_rank = $_SESSION[user_rank] ".
            "WHERE $sql_where ";

        $count = $db->getOne($sql);

        $max_page = ($count> 0) ? ceil($count / $size) : 1;
        if ($page > $max_page)
        {
            $page = $max_page;
        }

        /* Get VIP goods */
        $sql = hw_goods_list_sql($sql_where, $sort, $order, $size, ($page - 1) * $size);
        $res = $db->getAll($sql);

        $goodslist = hw_process_goods_rows($res);

        return array(
            'goodslist'           => $goodslist,
            'count'               => $count,
            'left_cat_menu'       => $cat_menu,
            'left_cat_menu_count' => count($fenlei),
            'brands'              => $brands,
            'brand_id'            => $brand_id
        );
    }
}

        /* Filter Attribute */
        // $attr_data = $attrbuteController->get_attr_data($category, $filter_attr, 'vip', $search_parameters);
        // $attr_goods = $attr_data['ext']; //商品查询条件扩展
        // $all_attr_list = $attr_data['all_attr_list'];

