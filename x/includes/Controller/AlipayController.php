<?php
# @Date:   2019-05-16T14:33:54+08:00
# @Filename: AlipayController.php
# @Last modified time: 
# @author: Billy@YOHO

namespace Yoho\cms\Controller;

require_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_accounting.php');
require_once(ROOT_PATH . '/includes/lib_time.php');
require_once(ROOT_PATH . '/includes/lib_order.php');
require_once(ROOT_PATH . '/includes/lib_payment.php');
use Yoho\cms\Model;

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}


class AlipayController extends YohoBaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    private function insertAlipayPaymentRecord($param)
    {
        if (empty($param)) {
            return false;
        }

        $insert_sql_statement = 'INSERT INTO ' . $GLOBALS['ecs']->table('alipay_payment_log') . ' (log_id, order_id, gateway_transaction_create_datetime, gateway_transaction_payment_datetime, gateway_transaction_last_modified_datetime,currency_code, out_trade_no, gateway_transaction_sn, gateway_transaction_amount, gateway_transaction_status_code, raw_text, wallet_type) VALUES ('
            . "'".$param["log_id"]."', "
            . "'".$param["order_id"]."', "
            . "'".$param["gateway_transaction_create_datetime"]."', "
            . "'".$param["gateway_transaction_payment_datetime"]."', "
            . "'".$param["gateway_transaction_last_modified_datetime"]."', "
            . "'".$param["currency_code"]."', "
            . "'".$param["out_trade_no"]."', "
            . "'".$param["gateway_transaction_sn"]."', "
            . "'".$param["gateway_transaction_amount"]."', "
            . "'".$param["gateway_transaction_status_code"]."', "
            . "'".$param["raw_text"]."', "
            . "'".$param["wallet_type"]."'"
            .')';

            // dd($insert_sql_statement);
        $GLOBALS['db']->query($insert_sql_statement);
        return $GLOBALS['db']->affected_rows();
    }

    private function updateAlipayPaymentRecord($data, $existingEntry = null)
    {
        $update_column_value = '';

        $param_count = count($data);
        if ($param_count == 0 || (empty($data['gateway_transaction_sn']) && empty($existingEntry['gateway_transaction_sn'])) ) {
            return false;
        }

        $transactionSn = $data['gateway_transaction_sn'] ?: $existingEntry['gateway_transaction_sn'];

        // Fetch the current transaction record
        $sql = "SELECT * FROM " . $GLOBALS['ecs']->table('alipay_payment_log') .
                " WHERE gateway_transaction_sn = '$transactionSn'";

        $alipay_payment_log = $GLOBALS['db']->getRow($sql);
        if (empty($alipay_payment_log)) {;
            return false;
        }

        $fields = [
            'log_id',
            'order_id',
            'gateway_transaction_create_datetime',
            'gateway_transaction_payment_datetime',
            'currency_code',
            'out_trade_no',
            'gateway_transaction_sn',
            'gateway_transaction_amount',
            'gateway_transaction_status_code',
            'wallet_type',
            'out_return_no',
        ];

        // Prepare and determine if update is required
        foreach($fields as $field) {
            if (!empty($data[$field]) && $alipay_payment_log[$field] !== $data[$field]) {
                if (!empty($update_column_value)) {
                    $update_column_value .= ', ';
                }
                if ($field === 'out_return_no' && !empty($alipay_payment_log[$field])) {
                    $update_column_value .= "$field = concat($field, ',' , '$data[$field]')";
                } else {
                    $update_column_value .= $field . ' = "' . $data[$field] . '"';
                }
            }
        }

        if (empty($update_column_value)) {
            return false;
        } else {
            // Append the raw string into table raw_text field 
            $update_column_value .= ', raw_text = concat(raw_text, ", ", \'' . $data['raw_text'] . '\')';
        }

        // Update corresponding database record with concatenation of raw string
        $update_sql_query = 'UPDATE ' . $GLOBALS['ecs']->table('alipay_payment_log') . ' SET ' . $update_column_value . " WHERE `gateway_transaction_sn` = '$transactionSn'";

        $GLOBALS['db']->query($update_sql_query);
        return $GLOBALS['db']->affected_rows();
    }

    private function searchAlipayPaymentRecord($out_trade_no = null, $trade_no = null)
    {
        if (empty($out_trade_no) && empty($trade_no)) {
            return false;
        }

        if (!empty($out_trade_no)) {
            $query = "SELECT * FROM ".  $GLOBALS['ecs']->table('alipay_payment_log') . " WHERE out_trade_no = '$out_trade_no'";
        }

        if (!empty($trade_no)) {
            $query = "SELECT * FROM ".  $GLOBALS['ecs']->table('alipay_payment_log') . " WHERE gateway_transaction_sn = '$trade_no'";
        }

        // dd($query);
        $result = $GLOBALS['db']->getRow($query);
        return $result;

    }

    private function get_payment_log($log_id)
    {
        $sql = "SELECT * FROM " . $GLOBALS['ecs']->table('pay_log') .
                " WHERE log_id = '$log_id'";
        $pay_log = $GLOBALS['db']->getRow($sql);
        return $pay_log;
    }

    private function getPayLogIdByTransactionId($transactionId)
    {
        if (!$this->checkIfPayLogExists($transactionId)) {
            return false;
        }
        $payLog = $this->getPayLogByTransactionId($transactionId);
        return $payLog['log_id'];
    }

    private function checkIfPayLogExists($transactionId)
    {
        global $db, $ecs;
        return ($db->getOne('SELECT COUNT(*) FROM ' . $ecs->table('alipay_payment_log') . " WHERE gateway_transaction_sn = '$transactionId'") > 0);
    }

    private function getPayLogByTransactionId($gatewayTransactionId)
    {
        global $db, $ecs;
        return $db->getRow('SELECT * FROM ' . $ecs->table('alipay_payment_log') . " WHERE gateway_transaction_sn = '$gatewayTransactionId'");
    }

    private function refundTransaction($logId, $amount, $gatewayTransactionId)
    {
        $payment_log = $this->get_payment_log($logId);
        if (empty($payment_log)) {
            return json_encode([
                'error' => '1',
                'message' => 'Payment Log Not Found',
                'content' => ''
            ]);
        }

        $paymentRequestRecordQuery = 'SELECT gateway_transaction_sn, wallet_type, out_trade_no FROM ' . $GLOBALS['ecs']->table('alipay_payment_log') . " WHERE log_id = '" . $logId . "'";
        $paymentRequestRecordQueryResult = $GLOBALS['db']->getRow($paymentRequestRecordQuery);
        if (empty($paymentRequestRecordQueryResult)) {
            return json_encode([
                'error' => '1',
                'message' => 'AliPay Pay Log Entry Not Found',
                'content' => ''
            ]);
        }

        $wallet = (!empty($paymentRequestRecordQueryResult['wallet_type'])) ? $paymentRequestRecordQueryResult['wallet_type'] : 'alipayhk';

        $paymentInfo = get_payment($wallet);

        $refundTime = local_getdate();
        $refundTime = sprintf(
            "%s-%s-%s %s:%s:%s",
            $refundTime['year'],
            str_pad($refundTime['mon'], 2, '0', STR_PAD_LEFT),
            str_pad($refundTime['mday'], 2, '0', STR_PAD_LEFT),
            str_pad($refundTime['hours'], 2, '0', STR_PAD_LEFT),
            str_pad($refundTime['minutes'], 2, '0', STR_PAD_LEFT),
            str_pad($refundTime['seconds'], 2, '0', STR_PAD_LEFT)
        );

        $param = [
            'service' => 'forex_refund',
            'partner' => $paymentInfo[$wallet.'_merchant_pid'],
            '_input_charset' => 'UTF-8',
            'notify_url' => 'https://' . ((strpos($_SERVER['SERVER_NAME'], 'beta.yohohongkong.com') === false) ? 'www.yohohongkong.com' : 'beta.yohohongkong.com') . '/api/receiveAlipayPaymentNotification.php',
            'out_return_no' => $paymentRequestRecordQueryResult['out_trade_no'].'_r_'.gmtime(),
            'out_trade_no' => $paymentRequestRecordQueryResult['out_trade_no'],
            'return_amount' => $amount,
            'currency' => 'HKD',
            'gmt_return' => $refundTime,
            'reason' => 'others',
            'product_code' => 'NEW_OVERSEAS_SELLER',
            'is_sync' => 'Y'
        ];

        ksort($param);
        reset($param);

        $sign = '';
        foreach ($param AS $key=>$val)
        {
            if (!in_array($key, ['sign', 'sign_type']))
            {
                $sign .= "$key=$val&";
            }
        }

        $signString = substr($sign, 0, -1) . $paymentInfo[$wallet . '_md5_signature_key'];

        $requestString = $sign . 'sign_type=MD5&sign=' . md5($signString);

        $ch = curl_init($paymentInfo[$wallet . '_api_base_gateway']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $requestString);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            "Content-Type: application/x-www-form-urlencoded",
            "Content-Length: " . strlen($requestString),
        ]);

        $refundResult = curl_exec($ch);
        if (curl_errno($ch) != 0) // cURL error
        {
            hw_error_log('Cannot connect to Alipay: ' . curl_error($ch));
            curl_close($ch);
            return json_encode([
                'error' => '1',
                'message' => 'Unable to connect to Alipay Service',
                'content' => ''
            ]);
        }
        else
        {
            curl_close($ch);
            $refundResult = simplexml_load_string($refundResult);
            $refundResultData = json_decode(json_encode($refundResult, JSON_UNESCAPED_UNICODE),  false);

            if (!empty($refundResultData->error) && $refundResultData->is_success === 'F') {
                return json_encode([
                    'error' => '1',
                    'message' => 'Refund failed: ' . $refundResultData->error,
                    'content' => ''
                ]);
            }

            if ($refundResultData->is_success !== 'T') {
                return json_encode([
                    'error' => '1',
                    'message' => 'Refund failed: UNKNOWN PARSE ERROR',
                    'content' => ''
                ]);
            }

            // Log refund successful
            $this->insertSyncNotification($param);

            // Then return
            return json_encode([
                'error' => '0',
                'message' => $refundResultData,
                'content' => ''
            ]);
        }
    }

    public function insertQueryResult($data, $raw_text)
    {
        $log_id = substr(explode('_', trim($data->out_trade_no))[0], 13);
        $payment_log = $this->get_payment_log($log_id);
        if (empty($payment_log)) {
            return false;
        }
        $param = [
            'log_id' => intval($log_id),
            'order_id' => intval($payment_log['order_id']),
            'gateway_transaction_create_datetime' => (!empty($data->gmt_create) ? gmstr2time((string) $data->gmt_create[0]) : null),
            'gateway_transaction_payment_datetime' => (!empty($data->gmt_payment) ? gmstr2time((string) $data->gmt_payment[0]) : null),
            'gateway_transaction_last_modified_datetime' => (!empty($data->gmt_last_modified_time) ? gmstr2time((string)$data->gmt_last_modified_time[0]) : null),
            'currency_code' => (!empty($data->currency) ? (string)$data->currency[0] : $payment_log['currency_code']),
            'out_trade_no' => (string) $data->out_trade_no[0],
            'gateway_transaction_sn' => (string) $data->trade_no[0],
            'gateway_transaction_amount' => (string) $data->total_fee[0],
            'gateway_transaction_status_code' => (string) $data->trade_status[0],
            'raw_text' => $raw_text,
            'wallet_type' => (string) $data->wallet_type[0]
        ];


        $existing_entry = $this->searchAlipayPaymentRecord(null, (string) $data->trade_no[0]);

        if (!empty($existing_entry)) {
            return $this->updateAlipayPaymentRecord($param);
        } else {
            return $this->insertAlipayPaymentRecord($param);
        }
    }

    public function insertAsyncNotification($data, $wallet_type = null)
    {
        $raw_text = json_encode($data, JSON_UNESCAPED_UNICODE);

        $log_id = substr(explode('_', trim($data['out_trade_no']))[0], 13);
        $payment_log = $this->get_payment_log($log_id);
        if (empty($payment_log)) {
            return false;
        }
        $param = [
            'log_id' => intval($log_id),
            'order_id' => intval($payment_log['order_id']),
            'gateway_transaction_create_datetime' => null,
            'gateway_transaction_payment_datetime' => null,
            'gateway_transaction_last_modified_datetime' => null,
            'currency_code' => $data['currency'],
            'out_trade_no' => $data['out_trade_no'],
            'gateway_transaction_sn' => $data['trade_no'],
            'gateway_transaction_amount' => $data['total_fee'],
            'gateway_transaction_status_code' => $data['trade_status'],
            'raw_text' => $raw_text,
            'wallet_type' => $wallet_type,
            'out_return_no' => $data['out_return_no'] ?: null
        ];

        $existing_entry = $this->searchAlipayPaymentRecord($data['trade_no']);
        if (!empty($existing_entry)) {
            return $this->updateAlipayPaymentRecord($param);
        } else {
            return $this->insertAlipayPaymentRecord($param);
        }
    }

    public function insertSyncNotification($data)
    {
        $raw_text = json_encode($data, JSON_UNESCAPED_UNICODE);

        $log_id = substr(explode('_', trim($data['out_trade_no']))[0], 13);
        $payment_log = $this->get_payment_log($log_id);
        if (empty($payment_log)) {
            return false;
        }
        $param = [
            'log_id' => intval($log_id),
            'order_id' => intval($payment_log['order_id']),
            'gateway_transaction_create_datetime' => null,
            'gateway_transaction_payment_datetime' => null,
            'gateway_transaction_last_modified_datetime' => null,
            'currency_code' => $data['currency'],
            'out_trade_no' => $data['out_trade_no'],
            'gateway_transaction_sn' => $data['trade_no'],
            'gateway_transaction_amount' => $data['total_fee'],
            'gateway_transaction_status_code' => $data['trade_status'],
            'raw_text' => $raw_text,
            'wallet_type' => $data['code'],
            'out_return_no' => $data['out_return_no'],
        ];

        if (!empty($data['out_return_no'])) {
            $existing_entry = $this->searchAlipayPaymentRecord($data['out_trade_no']);
        } else {
            $existing_entry = $this->searchAlipayPaymentRecord(null, $data['trade_no']);
        }

        if (!empty($existing_entry)) {
            return $this->updateAlipayPaymentRecord($param, $existing_entry);
        } else {
            return $this->insertAlipayPaymentRecord($param);
        }
    }

    public function getPaymentLogByLogId($log_id)
    {
        return $this->get_payment_log($log_id);
    }

    public function refund_by_transaction_id_and_amount($gateway_transaction_id, $amount)
    {
        if (empty($gateway_transaction_id) || empty($amount)) {
            return false;
        }

        $log_id = $this->getPayLogIdByTransactionId($gateway_transaction_id);

        $refund_result = json_decode($this->refundTransaction($log_id, $amount, $gateway_transaction_id), false);

        return ($refund_result->error === '0');
    }
}

?>
