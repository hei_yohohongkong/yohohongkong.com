<?php

/***
* FlashdealController.php
* by Anthony 20171020
*
* Flashdeal controller
***/

namespace Yoho\cms\Controller;
use Yoho\cms\Model;
if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

class FlashdealController extends YohoBaseController{

    public $tableName = 'flashdeals';
    public $admin_priv = 'bonus_manage';
    const FLASH_GOODS_STATUS_UNUSE = 0;
    const FLASH_GOODS_STATUS_USING = 1;
    const FLASH_GOODS_STATUS_USED = 2;
    const FLASH_OFFLINE_ORDER_EXPIRED = "+3 days";
    const FLASH_ONLINE_ORDER_EXPIRED = "+20 minutes";
    const FLASH_PAY_ORDER_EXPIRED = "+15 minutes";
    const JSON_PATH = ROOT_PATH . '/data/flashdeal_date.json'; // Using for js file get date

    public function get_countdown($endtime){

        if(empty($endtime)) {
            $endtime = local_strtotime(local_date('Y-m-d'));
            $gmt_end_time = $endtime + 86400;
        }
        else {
            intval($endtime);
            $gmt_end_time = $endtime;
        }
        $gmt_now_time = gmtime();
        $remaining_time = $gmt_end_time - $gmt_now_time;
        $countdown = '';

        if ($remaining_time > 86400)
        {
            $countdown .= floor($remaining_time / 86400) . '天';
            $remaining_time = $remaining_time % 86400;
        }
        if ($remaining_time > 3600)
        {
            $countdown .= floor($remaining_time / 3600) . ':';
            $remaining_time = $remaining_time % 3600;
        }
        if ($remaining_time > 60)
        {
            $countdown .= sprintf("%02d", floor($remaining_time / 60)) . ':';
            $remaining_time = $remaining_time % 60;
        }
        if ($remaining_time > 0)
        {
            $countdown .= sprintf("%02d", $remaining_time) . '';
        }
        return [
            'end_time'     => $endtime,
            'gmt_now_time' => $gmt_now_time,
            'gmt_end_time' => $gmt_end_time,
            'countdown'    => $countdown
        ];
    }

    function get_flashdeal_config($get_all = false){

        global $db, $ecs, $_CFG;
        $sql = "SELECT * FROM " . $ecs->table('shop_config') .
            " WHERE parent_id = (SELECT id FROM " . $ecs->table('shop_config') . " WHERE code = 'flashdeal' and type = 'group' )";
        $config_list = $db->getAll($sql);
        $config = array();
        if($get_all){ //CMS
            require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/admin/shop_config.php');
            foreach ($config_list as $key => $item) {

                $item['name'] = isset($_LANG['cfg_name'][$item['code']]) ? $_LANG['cfg_name'][$item['code']] : $item['code'];
                $item['desc'] = isset($_LANG['cfg_desc'][$item['code']]) ? $_LANG['cfg_desc'][$item['code']] : '';
                $config[] = $item;
            }
        } else { //flashdeal page

            foreach ($config_list as $key => $value) {
                $config[$value['code']] = $value['value'];
            }
            $config['flashdeal_banner'] = str_replace('..', '', empty($config['flashdeal_banner']) ? "/images/flashdeal_banner.png" : $config['flashdeal_banner']);
            $config['mobile_flashdeal_banner'] = str_replace('..', '', empty($config['mobile_flashdeal_banner']) ? "/images/flashdeal_banner.png" : $config['mobile_flashdeal_banner']);
            $config['flashdeal_guide'] = str_replace('..', '', empty($config['flashdeal_guide']) ? "/images/flashdeal_guide.png" : $config['flashdeal_guide']);
            $config['mobile_flashdeal_guide'] = str_replace('..', '', empty($config['mobile_flashdeal_guide']) ? "/dist/img/flashdeal_guide.png" : $config['mobile_flashdeal_guide']);
            $config['flashdeal_thanks'] = str_replace('..', '', empty($config['flashdeal_thanks']) ? "/images/flashdeal_thanks.png" : $config['flashdeal_thanks']);
            $config['mobile_flashdeal_thanks'] = str_replace('..', '', empty($config['mobile_flashdeal_thanks']) ? "/dist/img/flashdeal_thanks.png" : $config['mobile_flashdeal_thanks']);
            $config['flashdeal_timer_css_left'] = $config['flashdeal_timer_css_left'] == '' ? NULL : $config['flashdeal_timer_css_left'];
            $config['flashdeal_thanks_url'] = empty($config['flashdeal_thanks_url']) ? "/sale" : $config['flashdeal_thanks_url'];
        }

        return $config;
    }

    function update_flashdeal_config($request){

        global $db, $ecs, $_CFG, $_LANG;
        require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/admin/shop_config.php');
        $type = empty($request['type']) ? '' : $request['type'];
        /* 允许上传的文件类型 */
        $allow_file_types = '|GIF|JPG|PNG|BMP|';

        /* 保存变量值 */
        $count = count($request['value']);

        $arr = array();
        $sql = 'SELECT id, value FROM ' . $ecs->table('shop_config').
                " WHERE parent_id = (SELECT id FROM " . $ecs->table('shop_config') . " WHERE code = 'flashdeal' and type = 'group' )";
        $res= $db->query($sql);
        while($row = $db->fetchRow($res))
        {
            $arr[$row['id']] = $row['value'];
        }
        foreach ($request['value'] AS $key => $val)
        {
            if($arr[$key] != $val)
            {
                $sql = "UPDATE " . $ecs->table('shop_config') . " SET value = '" . trim($val) . "' WHERE id = '" . $key . "'";
                $db->query($sql);
            }
        }

        /* 处理上传文件 */
        $file_var_list = array();
        $sql = "SELECT * FROM " . $ecs->table('shop_config') . " WHERE parent_id > 0 AND type = 'file' AND parent_id = (SELECT id FROM " . $ecs->table('shop_config') . " WHERE code = 'flashdeal' and type = 'group' )";
        $res = $db->query($sql);
        while ($row = $db->fetchRow($res))
        {
            $file_var_list[$row['code']] = $row;
        }

        foreach ($_FILES AS $code => $file)
        {
            /* 判断用户是否选择了文件 */
            if ((isset($file['error']) && $file['error'] == 0) || (!isset($file['error']) && $file['tmp_name'] != 'none'))
            {
                /* 检查上传的文件类型是否合法 */
                if (!check_file_type($file['tmp_name'], $file['name'], $allow_file_types))
                {
                    sys_msg(sprintf($_LANG['msg_invalid_file'], $file['name']));
                }
                else
                {
                    if (!make_dir($file_var_list[$code]['store_dir']))
                    {
                        /* 创建目录失败 */
                        return false;
                    }
                    $ext = array_pop(explode('.', $file['name']));
                    $file_name = $file_var_list[$code]['store_dir'] . $code .".". $ext;

                    /* 判断是否上传成功 */
                    if (move_upload_file($file['tmp_name'], $file_name))
                    {
                        $file_name = str_replace('..', '', $file_name);
                        $sql = "UPDATE " . $ecs->table('shop_config') . " SET value = '$file_name' WHERE code = '$code'";
                        $db->query($sql);
                    }
                    else
                    {
                        sys_msg(sprintf($_LANG['msg_upload_failed'], $file['name'], $file_var_list[$code]['store_dir']));
                    }
                }
            }
        }
        /* 记录日志 */
        admin_log('', 'edit', 'flashdeal_config');
        /* 清除缓存 */
        clear_all_files();
        $_CFG = load_config();
    }

    function getFlashdealInfo($goods_id, $fid = 0)
    {
        global $db, $ecs;
        $now = local_strtotime('now');
        if(!empty($fid))  {
            // $check_goods_id = $db->getOne(
            //     "SELECT fd.goods_id ".
            //     "FROM ".$ecs->table('flashdeals')." AS fd ".
            //     "LEFT JOIN ".$ecs->table('flashdeal_event')." AS fde ON fde.fd_event_id = fd.fd_event_id ".
            //     "WHERE fde.status = 1 AND fd.flashdeal_id = ".intval($fid)." ".
            //     "AND fd.deal_date BETWEEN '".local_strtotime(local_date('Y-m-d 00:00:00'))."' AND '" . gmtime() . "' Limit 1 "
            //     );

            $check_goods_id = $db->getOne(
                    "SELECT fd.goods_id ".
                    "FROM ".$ecs->table('flashdeals')." AS fd ".
                    "LEFT JOIN ".$ecs->table('flashdeal_event')." AS fde ON fde.fd_event_id = fd.fd_event_id ".
                    "WHERE fde.status = 1 AND fd.flashdeal_id = ".intval($fid)." ".
                    " Limit 1 "
            );

            if($check_goods_id != $goods_id)$fid = $this->check_goods_is_flashdeal(intval($goods_id));
        } else {
            $fid = $this->check_goods_is_flashdeal(intval($goods_id));
        }
        if(empty($fid)) return false;

        $sql = "SELECT fd.flashdeal_id, fd.deal_date, fd.deal_price, fd.deal_quantity, fd.detail_visible, fd.sort_order as up_sort_order, fd.is_online_pay, fd.payme_only, fd.alipay_only, fde.user_rank ".
                "FROM " . $ecs->table('flashdeals') . " as fd " .
                "LEFT JOIN " . $ecs->table('flashdeal_event') . " as fde ON fde.fd_event_id = fd.fd_event_id " .
                "WHERE fde.status = 1 ".
                "AND fd.flashdeal_id = ".intval($fid)." Limit 1 ";

        $deal = $db->getRow($sql);
        // Get 00:00
        $startdate = local_strtotime(local_date('Y-m-d 00:00:00', $deal['deal_date']));
        // Get 23:59
        $enddate   = local_strtotime(local_date('Y-m-d 23:59:59',  $deal['deal_date']));

        if (!empty($_REQUEST['c_fdid'])) {
            if($enddate < $now) return false;    
        } else {
            if($now < $deal['deal_date'] || $enddate < $now) return false;
        }

        $sql = "SELECT IFNULL(COUNT(*),0) " .
            "FROM " . $ecs->table('flashdeal_goods') .
            "WHERE flashdeal_id = '" . intval($fid) . "' " .
            " AND status = " . self::FLASH_GOODS_STATUS_UNUSE;
//            $flashdeal['dealt_quantity'] = $db->getOne($sql);
        $deal['remaining_qty'] = $db->getOne($sql);
        $deal['url'] = "/flashdeal/".local_date('Ymd', $deal['deal_date'])."/".local_date('Hi', $deal['deal_date']);
        $deal = array_merge($deal, $this->get_countdown($enddate));
        /* check session have this timer key */
        if(!isset($_SESSION['flashdeal'][$deal['deal_date']])) {
            $_SESSION['flashdeal'][$deal['deal_date']] = substr(md5(rand()),0,10);
        }
        
        $deal['cart_key'] = $_SESSION['flashdeal'][$deal['deal_date']].$deal['flashdeal_id'];
        $obj = [];
        $obj['k'] = $deal['cart_key'];
        $obj['f'] = $deal['flashdeal_id'];
        $obj['id'] = $goods_id;
        require_once ROOT_PATH . 'core/lib_payload.php';
        $payload = base64_encode(encode_payload($obj));
        //$payload = rawurlencode($payload);
        $deal['payload'] = $payload;
        $rand = substr(md5(microtime()),rand(0,26),5);
        $deal['auto_position'] = (local_strtotime('now')>=local_strtotime("11-11-2018 00:00:00"));
        $deal['random'] = $rand;
        return $deal;
    }

    function getFlashdealRemainingQty($goods_id, $fid = 0)
    {
        global $db, $ecs;
        
        $sql = "SELECT IFNULL(COUNT(*),0) " .
            "FROM " . $ecs->table('flashdeal_goods') .
            "WHERE flashdeal_id = '" . intval($fid) . "' " .
            " AND status = " . self::FLASH_GOODS_STATUS_UNUSE;
        $remaining_qty = $db->getOne($sql);

        return $remaining_qty;
    }
    /**
     * @param int $num
     * @param bool $widget
     * @param int $page
     * @param string $date Get flashdeal date
     * @param string $datetime A datetime to get flashdeal time session
     */
    function get_flashdeals_list($num = 10, $widget = false, $page = 1, $date = '', $datetime = '', $goods = [], $next = false, $cid = 0)
    {
        global $ecs, $db;
        $categoryController = new CategoryController();
        $res = [];
        $now = local_strtotime('now');
        
        if(empty($date) || !ctype_digit(strval($date))) $date = $now;
        else $date = local_strtotime($date);

        // Get 00:00
        $startdate = local_strtotime(local_date('Y-m-d 00:00:00', $date));
        // Get 23:59
        $enddate   = local_strtotime(local_date('Y-m-d 23:59:59', $date));

        if(!$next) {
            /* We get the flashdeal time list */
            $time_list = $db->getCol("SELECT fd.deal_date FROM " . $ecs->table('flashdeals') . " AS fd LEFT JOIN " . $ecs->table('flashdeal_event') . " AS fde ON fde.fd_event_id = fd.fd_event_id WHERE fde.status = 1 AND fd.deal_date BETWEEN '" . $startdate . "' AND '" . $enddate . "' GROUP BY fd.deal_date ORDER BY fd.deal_date ASC ");
        } else {
            $start_time = ($startdate > local_strtotime("midnight")) ? local_strtotime("midnight") : $startdate;
            $time_list = $db->getCol("SELECT fd.deal_date FROM " . $ecs->table('flashdeals') . " AS fd LEFT JOIN " . $ecs->table('flashdeal_event') . " AS fde ON fde.fd_event_id = fd.fd_event_id WHERE fde.status = 1 AND fd.deal_date >= '" . $start_time . "' GROUP BY fd.deal_date ORDER BY fd.deal_date ASC ");
        }
        // This day don't have flashdeals, return.
        if(empty($time_list)) return false;
        /**
         * If empty $datetime, we get the time session is closest time by now, else we get the closest time by datetime.
        */
        if(!empty($datetime) && ctype_digit(strval($datetime))) {
            $date = \DateTime::createFromFormat('YmdHi', local_date('Ymd',$date).$datetime);
            if($date === false) $date = local_strtotime(local_date('Y-m-d H:i:s'));
            else $date = local_strtotime($date->format('Y-m-d H:i:s'));
        } else {
            $date = local_date('Ymd',$date).local_date('Hi');
            if($date === false) $date = local_strtotime(local_date('Y-m-d H:i:s'));
            else $date = local_strtotime($date);
        }
        $closest_time = null;
        /* If have closest time < $date , get this time, else get closest time > date */
        if(!$widget) {
            foreach($time_list as $time) {
                if ($closest_time === null || abs($date - $closest_time) > abs($time - $date)) {
                    $closest_time = $time;
                }
            }
        } else { // $widget only get time < now
            foreach($time_list as $time) {
                if($time > $now) continue;
                if ($closest_time === null || abs($date - $closest_time) > abs($time - $date)) {
                    $closest_time = $time;
                }
            }
        }

        $current_time_session = false;
        $time_session = [];
        foreach($time_list as $key => $time) {
            $time_session[$key]['time'] = $time;
            $time_session[$key]['time_format'] = ($next) ? local_date('Y-m-d', $time).'<br>'.local_date('H:i', $time).'' : local_date('H:i', $time);
            $time_session[$key]['url'] = "/flashdeal/".local_date('Ymd', $time)."/".local_date('Hi', $time).'/';
            $time_session[$key]['in_cat'] = ($cid == 0 || in_array($time, $have_cat_list));
            if($next) $time_session[$key]['url'] .= 'upcoming/';
            if ($now < $time) {
                $time_session[$key] = array_merge($time_session[$key], $this->get_countdown($time));
                $time_session[$key]['coming_soon'] = true;
            } elseif ($now >= $time && $now > local_strtotime(local_date('Y-m-d 23:59:59', $time))) {
                $time_session[$key] = array_merge($time_session[$key], $this->get_countdown($enddate));
                if($time_session[$key]['end_time'] >= $now) $time_session[$key]['now'] = true;
                else $time_session[$key]['expired'] = true;
            } elseif ($time <= $now && local_strtotime(local_date('Y-m-d 23:59:59', $time)) > $now) {
                $time_session[$key] = array_merge($time_session[$key], $this->get_countdown($enddate));
                $time_session[$key]['now'] = true;
            }
            if($closest_time == $time) {
//                $res['prev_url'] = $time_session[$key - 1]['url'];
                $res['current_time_session'] = $time_session[$key];
                $current_time_session = true;
//                if($time_session[$key + 1])
//                $res['next_url'] = $time_session[$key + 1]['url'];
            }
        }

        /* Get date flashdeals list */
        $commentController = new CommentController();
        $comment_type = $commentController::COMMENT_TYPE_GOODS;

        // $sql = "SELECT IFNULL(fs.`sold_count`,0) as `sold_count`,IFNULL(fg.`remaining_qty`,0) as `remaining_qty`, fd.is_online_pay, fd.flashdeal_id, fd.deal_date, fd.deal_price, fd.deal_quantity, fd.detail_visible, fd.sort_order as up_sort_order, g.*, AVG(cm.comment_rank) as rating, COUNT(cm.comment_rank) as rating_total FROM " . $ecs->table('flashdeals') . " as fd " .
        //     "LEFT JOIN " . $ecs->table('goods') . " as g ON g.goods_id = fd.goods_id " .
        //     "LEFT JOIN " . $ecs->table('comment') . " AS cm ON cm.id_value = g.goods_id AND cm.comment_type = $comment_type AND cm.status = 1 ".
        //     "LEFT JOIN (" . // remaining_qty: How many flashdeal goods is added to cart
        //     "SELECT IFNULL(COUNT(*),0) as remaining_qty, flashdeal_id " .
        //     "FROM " . $ecs->table('flashdeal_goods') .
        //     " WHERE status = " . self::FLASH_GOODS_STATUS_UNUSE." GROUP BY flashdeal_id) as fg ON fd.flashdeal_id = fg.flashdeal_id ". // end .remaining_qty
        //     "LEFT JOIN (" . // sold_count
        //     "SELECT og.`flashdeal_id`, sum(og.`goods_number`) as `sold_count` " .
        //     "FROM " . $ecs->table('order_goods') . " as og ".
        //     "LEFT JOIN " . $ecs->table('order_info') . " as oi ON og.order_id = oi.order_id ".
        //     "WHERE og.flashdeal_id > 0 ".
        //     "AND oi.order_status " . db_create_in(array(OS_CONFIRMED, OS_SPLITED, OS_SPLITING_PART)) . " " .
        //     "AND oi.pay_status " . db_create_in(array(PS_PAYED, PS_PAYING)) . " " .
        //     "GROUP BY og.flashdeal_id " .
        //     ") as fs ON fd.flashdeal_id = fs.flashdeal_id "; // end .sold_count
        // $where = " WHERE g.is_hidden = 0 ";

        if ($current_time_session == false) {
            $sql = "SELECT fd.is_online_pay, fd.payme_only, fd.alipay_only, fd.flashdeal_id, fd.deal_date, fd.deal_price, fd.deal_quantity, fd.detail_visible, fde.user_rank, fd.sort_order as up_sort_order, 
            g.goods_id,g.goods_sn,g.market_price,g.shop_price,g.promote_price,g.goods_img,g.is_real,g.goods_name,
             AVG(cm.comment_rank) as rating, COUNT(cm.comment_rank) as rating_total FROM " . $ecs->table('flashdeals') . " as fd " .
            "LEFT JOIN " . $ecs->table('goods') . " as g ON g.goods_id = fd.goods_id " .
            "LEFT JOIN " . $ecs->table('flashdeal_event') . " as fde ON fde.fd_event_id = fd.fd_event_id " .
            "LEFT JOIN " . $ecs->table('comment') . " AS cm ON cm.id_value = g.goods_id AND cm.comment_type = $comment_type AND cm.status = 1 ";
        } else {
            $sql = "SELECT IFNULL(fg.`remaining_qty`,0) as `remaining_qty`, fd.is_online_pay, fd.payme_only, fd.alipay_only, fd.flashdeal_id, fd.deal_date, fd.deal_price, fd.deal_quantity, fd.detail_visible, fde.user_rank, fd.sort_order as up_sort_order, 
            g.goods_id,g.goods_sn,g.market_price,g.shop_price,g.promote_price,g.goods_img,g.is_real,g.goods_name
            FROM " . $ecs->table('flashdeals') . " as fd " .
            "LEFT JOIN " . $ecs->table('goods') . " as g ON g.goods_id = fd.goods_id " .
            "LEFT JOIN " . $ecs->table('flashdeal_event') . " as fde ON fde.fd_event_id = fd.fd_event_id " .
            "LEFT JOIN (" . // remaining_qty: How many flashdeal goods is added to cart
            "SELECT IFNULL(COUNT(*),0) as remaining_qty, flashdeal_id " .
            "FROM " . $ecs->table('flashdeal_goods') .
            " WHERE status = " . self::FLASH_GOODS_STATUS_UNUSE." GROUP BY flashdeal_id) as fg ON fd.flashdeal_id = fg.flashdeal_id "; // end .remaining_qty
        }

        $where = " WHERE g.is_hidden = 0 AND fde.status = 1 ";
        $filter_where = '';
        if(!empty($goods)) {
            if(!$closest_time) return false;
            $cat_list = $categoryController->getParentLevel($goods['cat_id'],false);
            // Default: Use level 2 cat
            $cat_id =  $cat_list[1]['cat_id'];
            $children_ids = get_children($cat_id);
            $where .= " AND fd.deal_date BETWEEN '" . $startdate . "' AND '" . $closest_time . "' AND $children_ids AND g.goods_id != $goods[goods_id] ";
        } else {
            if(!$closest_time) return false;
            $where .= " AND fd.deal_date = $closest_time ";
            if($cid > 0) {
                $children_ids = get_children($cid);
                $filter_where = " AND $children_ids ";
            }
        }
        
        $sql .= $where.$filter_where;
        if ($widget) {
            if ($current_time_session == false) {
                $sql .= "GROUP BY flashdeal_id ORDER BY RAND()";
            } else {
                $sql .= "ORDER BY RAND()";
            }
        } else {
            if ($current_time_session == false) {
                $sql .= "GROUP BY flashdeal_id ORDER BY up_sort_order ASC, fd.`goods_id` ASC";
            } else {
                $sql .= "ORDER BY up_sort_order ASC, fd.`goods_id` ASC";
            }
        }
        if ($num > 0) {
            $sql .= " LIMIT " . (($page - 1) * $num) . ", " . $num;
        }

        $count = $db->getOne(
            "SELECT count(g.goods_id) " . 
            "FROM " . $ecs->table('flashdeals') . " as fd " .
            "LEFT JOIN " . $ecs->table('goods') . " as g ON g.goods_id = fd.goods_id " . 
            "LEFT JOIN " . $ecs->table('flashdeal_event') . " as fde ON fde.fd_event_id = fd.fd_event_id " . 
            $where.$filter_where
        );
        $max_page = ceil($count/$num);
        $flashdeals = $db->getAll($sql);

        // Don't have flashdeals, return.
        if(empty($flashdeals) && $widget) return false;
        if(count($flashdeals) == 1 && empty($flashdeals[0]['goods_id'])) return false;
        if(!$widget || $cid > 0) {
            // Get cat menu
            $sql = "SELECT c.cat_id, IFNULL(cl.cat_name, c.cat_name) as cat_name, c.parent_id, c.is_show, " .
                "p.parent_id as p_id, IFNULL(pcl.cat_name, p.cat_name) as parent_name, p.sort_order as parent_sort_order " .
                "FROM " . $ecs->table('goods') . " as g " .
                "LEFT JOIN " . $ecs->table('flashdeals') . " AS fd ON fd.goods_id = g.goods_id " .
                "LEFT JOIN " . $ecs->table('category') . " AS c ON g.cat_id = c.cat_id " .
                "LEFT JOIN " . $ecs->table('category') . " as p ON c.parent_id  = p.cat_id AND p.is_show = 1 " .
                "LEFT JOIN " . $ecs->table('category_lang') . " as cl ON c.cat_id = cl.cat_id AND cl.lang = '" . $_CFG['lang'] . "' " .
                "LEFT JOIN " . $ecs->table('category_lang') . " as pcl ON pcl.cat_id = p.cat_id AND pcl.lang = '" . $_CFG['lang'] . "' " .
                " WHERE g.is_hidden = 0 ".
                ($next ? " AND fd.deal_date > '" . local_strtotime("midnight") . "' " : " AND fd.deal_date BETWEEN '" . $startdate . "' AND '" . $enddate . "' ").
                "AND c.parent_id != 0 AND c.is_show = 1 " .
                "GROUP BY c.cat_id ORDER BY c.sort_order ASC, c.cat_id ASC";
            $goodsCat = $db->getAll($sql);
            // Group by parent category
            $group_parent = array();
            foreach ($goodsCat as $value) {
                // Build parent category array
                $group_parent[$value['parent_id']]['cat_id']   = $value['parent_id'];
                $group_parent[$value['parent_id']]['cat_name']   = $value['parent_name'];
                $group_parent[$value['parent_id']]['parent_id']   = $value['p_id'];
                $group_parent[$value['parent_id']]['sort_order']   = $value['parent_sort_order'];
                // merge children category into parent category array
                $group_parent[$value['parent_id']]['children'][$value['cat_id']] = $value;
                $group_parent[$value['parent_id']]['count'] = count($group_parent[$value['parent_id']]['children']);
            }
            // Get all parent category
            $ungroupCat = cat_list(0, 0, false, 0, false, false);
            foreach ($goodsCat as $cat_key => $cat) {
                $cat_id = $cat['cat_id'];
                if($ungroupCat[$cat_id]) {
                    $ungroupCat[$cat_id]['show'] = true;
                    $category = $ungroupCat[$cat_id];
                    if($cid > 0 && $cat_id == $cid) $res['selected_cat'] = $ungroupCat[$cat_id];
                    while($category['parent_id'] > 0 && isset($ungroupCat[$category['parent_id']])) {
                        if($cid > 0 && $category['parent_id'] == $cid) $res['selected_cat'] = $ungroupCat[$category['parent_id']];
                        if($res['selected_cat']['parent_id'] == $category['cat_id']) $res['selected_cat']['parent_id'] = $category['parent_id'];
                        $ungroupCat[$category['parent_id']]['show'] = true;
                        $category = $ungroupCat[$category['parent_id']];
                    }
                }
            }
            foreach ($ungroupCat as $cat_key => $cat) {
                if(!$cat['show']) {
                    unset($ungroupCat[$cat_key]);
                }
            }
            // Group category by level
            $category_list = $categoryController->groupCat($ungroupCat);
        } else {
            $category_list = null;
        }

        $userRankController = new UserRankController();
        $arr_user_rank_list = $userRankController->getUserRankProgramName();
        $user_rank = [];
        foreach ($arr_user_rank_list as $index => $rank){
            $user_rank[$rank['rank_id']] = $rank['rank_name'];
        }

        $flashdeals = localize_db_result('goods', $flashdeals);

        foreach ($flashdeals as $key => $flashdeal) {
            $goods = $flashdeal;
            /* TODO: Hardcode update http to https with product image */
            if (!isset($goods['goods_desc'])) {
                $goods['goods_desc'] = '';
            }
            $goods['goods_desc'] = str_replace('http://www.yohohongkong.com/images/upload/Proddb/', 'https://www.yohohongkong.com/images/upload/Proddb/', $goods['goods_desc']);
            $goods['goods_desc'] = str_replace('http://blog.yohohongkong.com/wp-content/uploads/', 'https://blog.yohohongkong.com/wp-content/uploads/', $goods['goods_desc']);

            $flashdeal['goods'] = $goods;

            $flashdeal['rank_filter'] = 0;
            if (empty($flashdeal['user_rank'])){
                $flashdeal['user_rank'] = 0;

            } else {
                $flashdeal['user_rank'] = explode("^", $flashdeal['user_rank']);
                if (!in_array("1", $flashdeal['user_rank'])){
                    $flashdeal['rank_filter'] = 1;
                }
                foreach ($flashdeal['user_rank'] as $index => $rank){
                    $flashdeal['user_rank'][$index] = array("id"=>$rank, "name"=>$user_rank[$rank]);
                }
            }

            if ($flashdeal['remaining_qty'] < 0) $flashdeal['remaining_qty'] = 0;
            $flashdeal['deal_price_formated'] = price_format($flashdeal['deal_price']);
            $flashdeal['market_price_formated'] = price_format($flashdeal['market_price']);

            // YOHO: lowest price = 3% discount, have to pay with cash / ATM / ebanking
            $flashdeal['lowest_pirce'] = ceil(doubleval($flashdeal['deal_price']) * 0.97);
            $flashdeal['lowest_price_formated'] = price_format($flashdeal['lowest_pirce']);

            // Saved price
            $flashdeal['saved_price'] = $goods['market_price'] - $flashdeal['lowest_pirce'];
            $flashdeal['saved_price_formated'] = price_format($flashdeal['saved_price']);
            $flashdeal['saved_percentage'] = round($flashdeal['saved_price'] / $goods['market_price'] * 100.0, 2) . '%';
            $flashdeal['saved_percentage_rounded'] = round($flashdeal['saved_price'] / $goods['market_price'] * 100.0) . '%';

            // Countdown
            $flashdeal['gmt_now_time'] = $time;
            $flashdeal['gmt_end_time'] = local_strtotime(local_date('Y-m-d 23:59:59', $time));;

            $remaining_time = $flashdeal['gmt_end_time'] - $time;
            $flashdeal['countdown'] = '';
            if ($remaining_time > 86400) {
                $flashdeal['countdown'] .= floor($remaining_time / 86400) . '天';
                $remaining_time = $remaining_time % 86400;
            }
            if ($remaining_time > 3600) {
                $flashdeal['countdown'] .= floor($remaining_time / 3600) . '小時';
                $remaining_time = $remaining_time % 3600;
            }
            if ($remaining_time > 60) {
                $flashdeal['countdown'] .= sprintf("%02d", floor($remaining_time / 60)) . '分';
                $remaining_time = $remaining_time % 60;
            }
            if ($remaining_time > 0) {
                $flashdeal['countdown'] .= sprintf("%02d", $remaining_time) . '秒';
            }
            //if(isset($flashdeal['sold_count'])) {
                $flashdeal['sold_out'] = ($flashdeal['remaining_qty'] <= 0);
            //}
            if (!isset($flashdeal['rating_total'])){
                $flashdeal['rating_total'] = 0;
            }
            if($flashdeal['rating_total'] > 0) {
                if($flashdeal['rating_total'] < 3 && $flashdeal['rating'] <= 3) {
                    $flashdeal['rating'] = 0;
                }
                if($flashdeal['rating'] > 0) {
                    $flashdeal['rating'] = $flashdeal['rating'];
                    $flashdeal['rating_code'] = $this->genRatingSvg($flashdeal['rating']);
                }
            } else {
    //            $flashdeal['rating'] = 0;
                if(strpos($_SERVER['SERVER_NAME'], 'beta.yohohongkong.com') !== false) {
                    $flashdeal['rating'] = rand(1, 5);
                    $flashdeal['rating_code'] = $this->genRatingSvg($flashdeal['rating']);
                }
            }
            $flashdeals[$key] = $flashdeal;
        }

        /* If have cid, we get cid and their childs have product time_session */
        if($cid > 0) {
            $children_ids = $children_ids ? $children_ids : get_children($cid);
            $sql = "SELECT fd.deal_date " .
                "FROM " . $ecs->table('goods') . " as g " .
                "LEFT JOIN " . $ecs->table('flashdeals') . " AS fd ON fd.goods_id = g.goods_id " .
                " WHERE g.is_hidden = 0 ".
                ($next ? " AND fd.deal_date > '" . local_strtotime("midnight") . "' " : " AND fd.deal_date BETWEEN '" . $startdate . "' AND '" . $enddate . "' ").
                " AND ". $children_ids .
                " GROUP BY fd.deal_date";
            $have_cat_list = $db->getCol($sql);
        }

        foreach($time_list as $key => $time) {
            $time_session[$key]['in_cat'] = ($cid == 0 || in_array($time, $have_cat_list));
        }

        $res['time_session'] = $time_session;
        $res['flashdeals']   = $flashdeals;
        $res['max_page'] = $max_page;
        $res['category_list'] = $category_list;

        if($max_page > $page) {
            $next_page = parse_url($res['current_time_session']['url'], PHP_URL_PATH);
            $query     = parse_url($res['current_time_session']['url'], PHP_URL_QUERY).($cid>0?"&cid=".$cid:"")."&page=".($page+1);
            $res['next_page'] = $next_page."?".$query;
        }
        return $res;
    }

    /**
     * @return array
     */
    function get_cms_flashdeals()
    {
        global $db, $ecs;
        $timezone = isset($_SESSION['timezone']) ? $_SESSION['timezone'] : $GLOBALS['_CFG']['timezone'];
        $_REQUEST['sort_by']      = empty($_REQUEST['sort_by']) ? 'deal_date' : trim($_REQUEST['sort_by']);
        $_REQUEST['sort_order']   = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
        $_REQUEST['page_size']    = empty($_REQUEST['page_size'])  ? 50 : intval($_REQUEST['page_size']);
        $_REQUEST['start']        = empty($_REQUEST['start'])      ? 0 : intval($_REQUEST['start']);
        $where = "";
        if(!empty($_REQUEST['search_type'])){
            if($_REQUEST['search_type'] != '0' && !empty($_REQUEST['search_str'])){
                if($_REQUEST['search_type'] == "gn")
                    $where = " where g.`goods_name` LIKE '%" . $_REQUEST['search_str'] . "%' ";
                else if($_REQUEST['search_type'] == "fid")
                    $where = " where fd.`flashdeal_id` = '" . $_REQUEST['search_str'] . "' ";
                else
                    $where = " where g.`goods_id` = '" . $_REQUEST['search_str'] . "' ";
            }
        }
        if(!empty($_REQUEST['start_date'])){
            $date = trim($_REQUEST['start_date']);
            $time = local_strtotime($date);
//            // Get 00:00
//            $startdate = local_strtotime($date.' 00:00:00');
//            // Get 23:59
//            $enddate   = local_strtotime($date.' 23:59:59');
            $where .= $where == ""
                ? " where  fd.deal_date = '$time' "
                : " AND  fd.deal_date = '$time' ";
        }
        $sql = "SELECT count(fd.flashdeal_id) " .
            "FROM " . $ecs->table('flashdeals') . " as fd " .
            "LEFT JOIN " . $ecs->table('goods') . " as g ON g.goods_id = fd.goods_id " .
            $where;
        $_REQUEST['record_count'] = $db->getOne($sql);
        /* 查询 */
        // $sql = "SELECT fd.*, g.`goods_name`, g.`goods_sn`, g.`goods_thumb`, g.`market_price`, g.`shop_price`, " .
        //             "COUNT(fn.`notify_id`) as notify_count, " .
        //             "IFNULL((SELECT SUM(og.`goods_number`) " .
        //                 "FROM " . $ecs->table('order_goods') . " as og " .
        //                 "LEFT JOIN " . $ecs->table('order_info') . " as oi ON og.order_id = oi.order_id " .
        //                 "WHERE og.flashdeal_id = fd.flashdeal_id " .
        //                 "AND oi.order_status " . db_create_in(array(OS_CONFIRMED, OS_SPLITED, OS_SPLITING_PART)) . " " .
        //                 "AND oi.pay_status " . db_create_in(array(PS_PAYED, PS_PAYING)) . " " .
        //             "),0) as sold_count " .
        //         "FROM " . $ecs->table('flashdeals') . " as fd " .
        //         "LEFT JOIN " . $ecs->table('goods') . " as g ON g.goods_id = fd.goods_id " .
        //         "LEFT JOIN " . $ecs->table('flashdeal_notify') . " as fn ON fn.flashdeal_id = fd.flashdeal_id " .
        //         "GROUP BY fd.flashdeal_id " .
        //         "ORDER BY " . $_REQUEST['sort_by'] . ' ' . $_REQUEST['sort_order'] . ' ' .
        //         "LIMIT " . $_REQUEST['start'] . "," . $_REQUEST['page_size'];
        $sql = "SELECT fd.*, g.`goods_name`, g.`goods_sn`, g.`goods_thumb`, g.`market_price`, g.`shop_price`, " .
            "IFNULL(fs.`sold_count`,0) as `sold_count`, COUNT(fn.`notify_id`) as `notify_count`,IFNULL(fg.`remaining_qty`,0) as `remaining_qty` " .
            "FROM " . $ecs->table('flashdeals') . " as fd " .
            "LEFT JOIN " . $ecs->table('goods') . " as g ON g.goods_id = fd.goods_id " .
            "LEFT JOIN " . $ecs->table('flashdeal_notify') . " as fn ON fn.flashdeal_id = fd.flashdeal_id " .
            "LEFT JOIN (" . // remaining_qty: How many flashdeal goods is added to cart
            "SELECT IFNULL(COUNT(*),0) as remaining_qty, flashdeal_id " .
            "FROM " . $ecs->table('flashdeal_goods') .
            " WHERE status = " . self::FLASH_GOODS_STATUS_UNUSE." GROUP BY flashdeal_id) as fg ON fd.flashdeal_id = fg.flashdeal_id ". // end .remaining_qty
            "LEFT JOIN (" .
            "SELECT og.`flashdeal_id`, sum(og.`goods_number`) as `sold_count` " .
            "FROM " . $ecs->table('order_goods') . " as og ".
            "LEFT JOIN " . $ecs->table('order_info') . " as oi ON og.order_id = oi.order_id ".
            "WHERE og.flashdeal_id > 0 ".
            "AND oi.order_status " . db_create_in(array(OS_CONFIRMED, OS_SPLITED, OS_SPLITING_PART)) . " " .
            "AND oi.pay_status " . db_create_in(array(PS_PAYED, PS_PAYING)) . " " .
            "GROUP BY og.flashdeal_id " .
            ") as fs ON fd.flashdeal_id = fs.flashdeal_id " . $where .
            "GROUP BY fd.flashdeal_id " .
            "ORDER BY " . $_REQUEST['sort_by'] . ' ' . $_REQUEST['sort_order'] . ' ' .
            "LIMIT " . $_REQUEST['start'] . "," . $_REQUEST['page_size'];
        $data = $db->getAll($sql);

        foreach ($data as $key => $row)
        {
            $data[$key]['market_price_formatted'] = price_format($row['market_price'], false);
            $data[$key]['shop_price_formatted'] = price_format($row['shop_price'], false);
            $data[$key]['deal_price_formatted'] = price_format($row['deal_price'], false);
            $data[$key]['deal_date_formatted'] = local_date('Y-m-d H:i', $row['deal_date']);
            $search_btn = [
                'link'  => 'order.php?act=list&amp;flashdeal_id='.$row['flashdeal_id'],
                'title' => '搜尋訂單',
                'label' => '訂單',
                'btn_class' => 'btn btn-default',
                'icon_class' => 'fa fa-eye'
            ];
            $notice_btn = [
                'link'  => 'flashdeal.php?act=list_notify&amp;flashdeal_id='.$row['flashdeal_id'],
                'title' => '通知列表',
                'label' => '通知',
                'btn_class' => 'btn btn-default',
                'icon_class' => 'fa fa-book'
            ];
            $custom_actions = ['search' => $search_btn, 'notice' => $notice_btn];
            $data[$key]['_action'] = $this->generateCrudActionBtn($row['flashdeal_id'], 'id', null, null, $custom_actions);
        }

        $arr = array('data' => $data, 'record_count' => $_REQUEST['record_count']);

        return $arr;
    }

    function update_flashdeal_goods($flashdeal_id = 0, $deal_quantity = 0)
    {
        global $db, $ecs;

        if (empty($flashdeal_id)) {
            return false;
        }

        if ($db->getOne("SELECT 1 FROM " . $ecs->table("flashdeals") . " WHERE flashdeal_id = $flashdeal_id")) {
            $deal_quantity = empty($deal_quantity) ? $db->getOne("SELECT deal_quantity FROM " . $ecs->table("flashdeals") . " WHERE flashdeal_id = $flashdeal_id") : $deal_quantity;
            $goods_record = $db->getCol("SELECT fg_id FROM " . $ecs->table("flashdeal_goods") . " WHERE flashdeal_id = $flashdeal_id ORDER BY fg_id ASC");
            if (intval($deal_quantity) != count($goods_record)) {
                if (count($goods_record) > intval($deal_quantity)) {
                    /* Anthony: Check buyer count. */
                    $used_goods = $db->getCol("SELECT fg_id FROM " . $ecs->table("flashdeal_cart") . " WHERE flashdeal_id = $flashdeal_id ");
                    $can_delete = count($goods_record) - count($used_goods);
                    $need_delete = count($goods_record) - intval($deal_quantity);
                    if($can_delete < $need_delete) return false;
                    foreach ($goods_record as $key => $fg_id) {
                        if(count($goods_record) == intval($deal_quantity)) break;
                        if(in_array($fg_id, $used_goods)) {
                            continue;
                        } else {
                            $db->query("DELETE FROM " . $ecs->table("flashdeal_goods") . " WHERE fg_id = " . $fg_id);
                            unset($goods_record[$key]);
                        }

                    }
                } elseif (count($goods_record) < intval($deal_quantity)) {
                    $timestamp = local_strtotime("now");
                    while (count($goods_record) < intval($deal_quantity)) {
                        $db->query("INSERT INTO " . $ecs->table("flashdeal_goods") . " (flashdeal_id, create_at, update_time) VALUES ($flashdeal_id, $timestamp, $timestamp)");
                        $goods_record[] = $db->insert_id();
                    }
                }
                clear_cache_files("flashdeal");
                
                return true;
            } else {
                return true;
            }
        }
        return false;
    }

    function mass_update_flashdeal_goods($fs_id = 0, $fe_id = 0)
    {
        $process = 0;
        if (!empty($fs_id) && !empty($fe_id)) {
            $flashdeal_id = $fs_id;
            while ($flashdeal_id <= $fe_id) {
                if (self::update_flashdeal_goods($flashdeal_id)) {
                    $process++;
                }
                $flashdeal_id++;
            }
        }
        echo "Processed: $process";
    }

    function check_goods_is_flashdeal($goods_id)
    {
        global $ecs, $db;
        $sql = "SELECT fd.flashdeal_id, fd.deal_date, fd.deal_price ".
                "FROM ".$ecs->table('flashdeals')." AS fd ".
                "LEFT JOIN ".$ecs->table('flashdeal_event')." AS fde ON fde.fd_event_id = fd.fd_event_id ".
                "WHERE fde.status = 1 AND fd.goods_id = '" . $goods_id . "' ".
                "AND fd.deal_date BETWEEN '" . local_strtotime(local_date('Y-m-d 00:00:00')) . "' AND '" . gmtime() . "' ".
                "ORDER BY fd.deal_price ASC ";
        $flashdeals = $db->getAll($sql);
        $fid = '';
        if(count($flashdeals) == 1) {
            return $flashdeals[0]['flashdeal_id'];
        } else {
            foreach ($flashdeals as $key => $flashdeal) {
                $fid = $flashdeal['flashdeal_id'];
                $sql = "SELECT IFNULL(COUNT(*),0) " .
                    "FROM " . $ecs->table('flashdeal_goods') .
                    "WHERE flashdeal_id = '" . intval($fid) . "' " .
                    " AND status = " . self::FLASH_GOODS_STATUS_UNUSE;
//            $flashdeal['dealt_quantity'] = $db->getOne($sql);
                $remaining_qty = $db->getOne($sql);
                if($remaining_qty > 0) {
                    return $fid;
                }
            }
            // return last;
            return $fid;
        }
        return false;
    }

    /**
     * @param $flashdeal_id
     * @param null $status
     * @return array|bool
     */
    public function getFlashdealGoodsList($flashdeal_id, $status = null)
    {
        global $db, $ecs;

        $sql = "SELECT fg_id FROM " . $ecs->table('flashdeal_goods') . " WHERE flashdeal_id = $flashdeal_id ";
        if (isset($status)) {
            $sql .= " AND status = " . $status;
        }

        $result = $db->getCol($sql);
        return $result;
    }

    /**
     * Check user can buy this flashdeal goods, and insert into flashdeal cart table
     * @param $flashdeal_id
     * @return array|bool
     */
    public function insertFlashdealCart($flashdeal_id)
    {
        global $ecs, $db;

        $list = $this->getFlashdealGoodsList($flashdeal_id, 0);
        $total = count($list);
        $i = 0;
        do {
            $fg_id = $list[$i];
            if(empty($fg_id)) break;
            $user_id = $_SESSION['user_id'];
            $create_at = gmtime();
            $expired = local_strtotime(self::FLASH_ONLINE_ORDER_EXPIRED, $create_at);
            $result = $db->query("INSERT INTO " . $ecs->table('flashdeal_cart') . " (fg_id, user_id, create_at, expired, order_id, cart_id, flashdeal_id, session_id) VALUES ($fg_id, $user_id, $create_at, $expired, 0, 0, $flashdeal_id, '" . SESS_ID . "')", 'SILENT');
            if ($result == false) { // Have fg_id
                $i++;
            } else {
                /* Update flashdeal_goods table status */
                $db->query("UPDATE " . $ecs->table('flashdeal_goods') . " SET status = " . self::FLASH_GOODS_STATUS_USING . " WHERE fg_id = " . $fg_id);
                return array('fg_id' => $fg_id, 'now_time' => gmtime(), 'expired' => $expired);
            }
        } while ($i < $total);

        return false;
    }

    /**
     * @return bool
     */
    public function updateFlashdealCartByECSCart()
    {
        global $ecs, $db;
        $now = gmtime();
        $db->query('START TRANSACTION');
        $cart_list = $db->getCol("SELECT rec_id FROM ". $ecs->table('cart') ." WHERE user_id = " . $_SESSION['user_id'] . " AND session_id = '" . SESS_ID."' AND flashdeal_id > 0 ");
        /* If user is login/change account in this time, we update user_id into flashdeal cart */
        $db->query("UPDATE " . $ecs->table('flashdeal_cart') . " SET user_id = " . $_SESSION['user_id'] . " WHERE order_id = 0 AND session_id = '" . SESS_ID."'");
        /* Delete flashdeal_cart goods, cart goods rule: Is this user flashdeal goods is expired OR user have other session_id (remove other session_id goods) OR cart is remove this goods */
        $where = $_SESSION['user_id'] != 0 ? (" WHERE user_id = " . $_SESSION['user_id'] . " AND (session_id <> '" . SESS_ID . "' OR expired < $now OR cart_id NOT ".db_create_in($cart_list)." ) AND order_id = 0") : (" WHERE session_id = '" . SESS_ID . "' AND (expired < $now OR cart_id NOT ".db_create_in($cart_list) . ") AND order_id = 0");
        $remove_goods = $db->getAll("SELECT * FROM " . $ecs->table('flashdeal_cart') . $where);
        $remove_goods_cart = array_column($remove_goods, 'cart_id');
        $remove_fg_ids = array_column($remove_goods, 'fg_id');
        $check = array_intersect($cart_list, $remove_goods_cart);
        if(count($check) > 0) {
            $remove_goods_cart = array_merge($remove_goods_cart, $cart_list);
            $remove_fg_ids = $db->getCol("SELECT fg_id FROM ". $ecs->table('flashdeal_cart') ." WHERE cart_id " .db_create_in($remove_goods_cart));
        }
        // Delete cart:
        if(!empty($remove_goods_cart) && !empty($remove_fg_ids)) {
            $db->query("DELETE FROM " . $ecs->table('cart') . " WHERE (rec_id " . db_create_in($remove_goods_cart).") OR (session_id = '" . SESS_ID . "' AND is_gift <> 0) ");
            $db->query("DELETE FROM " . $ecs->table('flashdeal_cart') . " WHERE fg_id " . db_create_in($remove_fg_ids));
            // Update flashdeal_goods table:
            $db->query("UPDATE " . $ecs->table('flashdeal_goods') . " SET status = " . self::FLASH_GOODS_STATUS_UNUSE . " , update_time = $now WHERE fg_id " . db_create_in($remove_fg_ids));
        }
        $db->query('COMMIT');
//        clear_cache_files("flashdeal");
        return true;
    }

    /**
     * @param $order_id
     * @return bool
     */
    public function updateFlashdealCartToOrder($order_id)
    {
        global $ecs, $db;
        $now = gmtime();
//        $cart_list = $db->getCol("SELECT rec_id FROM ". $ecs->table('cart') ." WHERE user_id = " . $_SESSION['user_id'] . " AND session_id = '" . SESS_ID."' AND flashdeal_id > 0 ");
        $db->query('START TRANSACTION');
        $ids = $db->getCol("SELECT fg_id FROM " . $ecs->table('flashdeal_cart') . " WHERE order_id = 0 AND session_id = '" . SESS_ID."' AND user_id = ".$_SESSION['user_id'].' FOR UPDATE');
        $db->query("UPDATE " . $ecs->table('flashdeal_cart') . " SET order_id = $order_id, cart_id = 0 WHERE order_id = 0 AND session_id = '" . SESS_ID."' AND user_id = ".$_SESSION['user_id']);
        $db->query('COMMIT');
        $db->query("UPDATE " . $ecs->table('flashdeal_goods') . " SET status = " . self::FLASH_GOODS_STATUS_USED . " , update_time = $now WHERE fg_id ".db_create_in($ids) );
        /* Order flashdeal must online pay? If no, we update the flashdeal_cart expired to 3 days */
        $sql = "SELECT og.flashdeal_id FROM ".$ecs->table('order_goods')." as og LEFT JOIN ".$ecs->table('flashdeals')." as f ON f.flashdeal_id = og.flashdeal_id WHERE f.is_online_pay = 0 AND order_id = ".$order_id." GROUP BY og.flashdeal_id ";
        $fids = $db->getCol($sql);
        $new_expired = local_strtotime(self::FLASH_OFFLINE_ORDER_EXPIRED);
        $db->query("UPDATE " . $ecs->table('flashdeal_cart') . " SET expired = " . $new_expired . " WHERE order_id = ".$order_id." AND flashdeal_id ".db_create_in($fids) );
        return true;
    }

    public function getCartFisrtExpired()
    {
        global $ecs, $db;
        if($_SESSION['user_id'] > 0) {
            $now = gmtime();
            $time = $db->getOne("SELECT expired FROM " . $ecs->table('flashdeal_cart') . " WHERE order_id = 0 AND session_id = '" . SESS_ID."' AND user_id = ".$_SESSION['user_id'] ." ORDER BY expired ASC LIMIT 1");
            return ['expired' => $time, 'now_time' => gmtime()];
        } else {
            $now = gmtime();
            $time = $db->getOne("SELECT expired FROM " . $ecs->table('flashdeal_cart') . " WHERE order_id = 0 AND session_id = '" . SESS_ID."'  ORDER BY expired ASC LIMIT 1");
            return ['expired' => $time, 'now_time' => gmtime()];
        }

    }

    public function getOrderFisrtExpired($order_id)
    {
        global $ecs, $db;
        if($order_id > 0) {
            $now = gmtime();
            $time = $db->getOne("SELECT expired FROM " . $ecs->table('flashdeal_cart') . " WHERE cart_id = 0 AND order_id = $order_id ORDER BY expired ASC LIMIT 1");
            return $time;
        } else {
            return 0;
        }

    }

    public function cancelFlashdealOrder($order_id)
    {
        global $ecs, $db;
        if(empty($order_id)) return false;
        $now = gmtime();
        $ids = $db->getCol("SELECT fg_id FROM " . $ecs->table('flashdeal_cart') . " WHERE order_id = ".$order_id);
        $db->query("DELETE FROM " . $ecs->table('flashdeal_cart') . " WHERE order_id = ".$order_id);
        $db->query("UPDATE " . $ecs->table('flashdeal_goods') . " SET status = " . self::FLASH_GOODS_STATUS_UNUSE . " , update_time = $now WHERE fg_id ".db_create_in($ids) );
//        clear_cache_files("flashdeal");
        return true;
    }

    /** Check and add flashdeal in cart
     * @param $goods_id
     */
    function flashdealAddToCart($payload = '')
    {
        global $ecs, $db, $_LANG;
        require ROOT_PATH . 'core/lib_payload.php';
        if (empty($payload)) {
            return $output = array('status' => 0, 'text' => '非法路徑');
        }
        $obj = decode_payload(base64_decode($payload));
        if (!is_array($obj))  {
            $payload = rawurldecode($payload);
            $obj = decode_payload(base64_decode($payload));
            if (!is_array($obj)) {
                $obj = $_POST;
                $obj['k'] = substr($obj['k'], 0, -5);
                $info['info'] = 'User: ' . $_SESSION['user_id'] . ', IP： ' . real_ip() . ', Goods_id: ' . intval($obj['id']) . 'pd:' . $payload;
                $info['file'] = basename(__FILE__, '.php');
                $info['time'] = gmtime();
                $GLOBALS['db']->autoExecute($GLOBALS['ecs']->table('error_log'), $info, 'INSERT');
            }
        }
        $goods_id = intval($obj['id']);
        $flashdeal_id = intval($obj['f']);
        $key = $obj['k'];
        if(empty($goods_id)) return $output = array('status' => 0, 'text' => _L('flashdeal_addtocart_error_sold_out', '您所選擇的閃購已經售罄'));
        if(empty($flashdeal_id)) $flashdeal_id = $this->check_goods_is_flashdeal($goods_id);
        if(empty($flashdeal_id)) return $output = array('status' => 0, 'text' => _L('flashdeal_addtocart_error_sold_out', '您所選擇的閃購已經售罄'));
        $sql = "SELECT fd.*, fde.user_rank ".
                "FROM " . $ecs->table('flashdeals') . " AS fd " . 
                "LEFT JOIN " . $ecs->table('flashdeal_event') . " AS fde ON fde.fd_event_id = fd.fd_event_id " . 
                "WHERE flashdeal_id = '" . $flashdeal_id . "'";
        $flashdeal = $db->getRow($sql);

        $flashdeal['rank_filter'] = 0;
        if (empty($flashdeal['user_rank'])){
            $flashdeal['user_rank'] = [];
            $flashdeal['rank_filter'] = 1;
        } else {
            $flashdeal['user_rank'] = explode("^", $flashdeal['user_rank']);
            if (!in_array("1", $flashdeal['user_rank'])){
                $flashdeal['rank_filter'] = 1;
            }
        }

        if ($flashdeal['rank_filter']){
            $all_user_rank = array();
            array_push($all_user_rank, $_SESSION['user_rank']);
            if (isset($_SESSION['user_rank_program'])){
                foreach ($_SESSION['user_rank_program'] as $program){
                    array_push($all_user_rank, $program);
                }
            }
            $match_user_rank = FALSE;
            foreach ($all_user_rank as $rank){
                if (in_array($rank, $flashdeal['user_rank'])){
                    $match_user_rank = TRUE;
                    continue;
                }
            }
            if (!$match_user_rank){
                return $output = array('status' => 0, 'text' => _L('flashdeal_addtocart_error_user_rank', '您所選擇的閃購需要特定會員等級'));
            }
        }

        $session_key = $_SESSION['flashdeal'][$flashdeal['deal_date']];
        if(empty($session_key) || $session_key.$flashdeal_id !== $key) {
            $info['info'] = 'User: '.$_SESSION['user_id'].', IP： '.real_ip().', Goods_id: '.intval($obj['id']).'session_key:'.$session_key.'k:'.$key;
            $info['file'] = basename(__FILE__, '.php');
            $info['time'] = gmtime();
            $GLOBALS['db']->autoExecute($GLOBALS['ecs']->table('error_log'), $info, 'INSERT');
            return $output = array('status' => 0, 'text' => '非法路徑');
        }
        $now_time = local_strtotime(local_date('Y-m-d H:i:s'));
        $end_time = local_strtotime(local_date('Y-m-d 23:59:59'));
        if ($flashdeal['deal_date'] > $now_time)
        {
            return $output = array('status' => 0, 'text' => _L('flashdeal_addtocart_error_not_started', '您所選擇的閃購還未開始'));
        }
        elseif ($now_time > $end_time)
        {
            return $output = array('status' => 0, 'text' => _L('flashdeal_addtocart_error_ended', '您所選擇的閃購已經結束'));
        }
        else
        {
            $goods_id = $flashdeal['goods_id'];


            $sql = "SELECT IFNULL(SUM(`goods_number`),0) ".
                    "FROM ".$ecs->table('cart')." ".
                    "WHERE session_id = '" . SESS_ID . "' ".
                    "AND goods_id = '$goods_id' ".
                    "AND parent_id = 0 ".
                    "AND extension_code <> 'package_buy' ".
                    "AND flashdeal_id > 0";
        
            $cart_good_flashdeal_quantity = $db->getOne($sql);
        
            if ($cart_good_flashdeal_quantity > 0){
                return $output = array('status' => 0, 'text' => _L('flashdeal_addtocart_error_flashdeal_good_existed', '購物車內已有同款閃購產品'));
            }


            $sql = "SELECT IFNULL(SUM(og.`goods_number`),0) " .
                "FROM " . $ecs->table('order_goods') . " as og " .
                "LEFT JOIN " . $ecs->table('order_info') . " as oi ON og.order_id = oi.order_id " .
                "WHERE og.goods_id = '" . $goods_id . "' " .
                "AND og.flashdeal_id = '" . $flashdeal['flashdeal_id'] . "' " .
                "AND oi.order_status " . db_create_in(array(OS_CONFIRMED, OS_SPLITED, OS_SPLITING_PART)) . " " .
                "AND oi.pay_status " . db_create_in(array(PS_PAYED, PS_PAYING)) . " ";
            $dealt_quantity = $db->getOne($sql);

            $sql = "SELECT IFNULL(SUM(`goods_number`),0) " .
                "FROM " . $ecs->table('cart') .
                "WHERE session_id = '" . SESS_ID . "' " .
                "AND goods_id = '$goods_id' " .
                "AND parent_id = 0 " .
                "AND extension_code <> 'package_buy' " .
                "AND rec_type = 'CART_GENERAL_GOODS' " .
                "AND flashdeal_id = '" . $flashdeal['flashdeal_id'] . "'";
            $bought_quantity = $db->getOne($sql);

            $flashdeal_payment_method_query = "SELECT fd.is_online_pay as is_online_pay, fd.payme_only as payme_only, fd.alipay_only as alipay_only " .
                                                "FROM " . $ecs->table('cart') . " AS c, " . $ecs->table('flashdeals') . " AS fd " .
                                                "WHERE c.flashdeal_id = fd.flashdeal_id " . 
                                                "AND c.session_id = '" . SESS_ID . "'";
                                             
            $cart_flashdeal_summary = $db->getAll($flashdeal_payment_method_query);
            if (!$this->checkIfFlashDealItemListHaveCommonPaymentMethod($cart_flashdeal_summary)) {
                return $output = array('status' => 0, 'text' => _L('flashdeal_addtocart_error_only_single_payment_method_allowed', '您所選擇的閃購無法使用其他支付方法優惠'));
            };

            $flashdeal_item_payment_method_summary = array_intersect_key($flashdeal, ['is_online_pay' => '', 'payme_only' => '', 'alipay_only' => '']);
            
            $new_cart_item = [];
            foreach($flashdeal_item_payment_method_summary as $method => $count) {
                $new_cart_item[$method] = $count;
            }
            $cart_flashdeal_summary[] = $new_cart_item;

            if (!$this->checkIfFlashDealItemListHaveCommonPaymentMethod($cart_flashdeal_summary)) {
                return $output = array('status' => 0, 'text' => _L('flashdeal_addtocart_error_only_single_payment_method_allowed', '您所選擇的閃購無法使用其他支付方法優惠'));
            };

            if ($dealt_quantity >= $flashdeal['deal_quantity'])
            {
                return $output = array('status' => 0, 'text' => _L('flashdeal_addtocart_error_sold_out', '您所選擇的閃購已經售罄'));
            }
            else
            {
                if($bought_quantity > 0) //如果购物车已经有此物品，失敗
                {
                    return $output = array('status' => 0, 'text' => sprintf(_L('flashdeal_addtocart_error_exceeded', '加入購物車失敗：超過限購數量 %d'), 1));
                }
                else //购物车没有此物品，插入
                {
                    // Try to insert into flashdeal goods.
                    $fg_id = $this->insertFlashdealCart($flashdeal['flashdeal_id']);

                    if($fg_id == false) {
                        return $output = array('status' => 0, 'text' => _L('flashdeal_addtocart_error_sold_out', '您所選擇的閃購已經售罄'));
                    } else {
                        $sql = "SELECT * FROM " . $ecs->table('goods') . " WHERE goods_id = '" . $goods_id . "' LIMIT 1";
                        $goods = $db->getRow($sql);

                        $cart_item = array(
                            'user_id'       => $_SESSION['user_id'],
                            'session_id'    => SESS_ID,
                            'goods_id'      => $goods_id,
                            'goods_sn'      => addslashes($goods['goods_sn']),
                            'product_id'    => 0,
                            'goods_name'    => addslashes($goods['goods_name']),
                            'market_price'  => $goods['market_price'],
                            'goods_price'   => $flashdeal['deal_price'],
                            'goods_number'  => 1,
                            'goods_attr'    => '',
                            'goods_attr_id' => '',
                            'is_real'       => $goods['is_real'],
                            'extension_code'=> $goods['extension_code'],
                            'parent_id'     => 0,
                            'rec_type'      => CART_GENERAL_GOODS,
                            'is_gift'       => 0,
                            'is_shipping'   => $goods['is_shipping'],
                            'flashdeal_id'  => $flashdeal['flashdeal_id']
                        );
                        $db->autoExecute($GLOBALS['ecs']->table('cart'), $cart_item, 'INSERT');
                        $cart_id = $db->insert_id();
                        $db->query("UPDATE ".$ecs->table('flashdeal_cart')." SET cart_id = ".$cart_id." WHERE fg_id = ".$fg_id['fg_id']);
                        // clear product page cache
                        clear_cache_files("goods_".$goods_id);
                        return $output = array('status' => 1, 'count' => insert_cart_info(), 'totalPrice' => insert_cart_infoamount(), 'fg_info' => $fg_id);
                    }
                }
            }
        }
    }
    public function blacklist()
    {
        global $smarty, $db, $ecs;
        if(isset($_SESSION['black_user'])) {
            header("HTTP/1.0 400 Not Found");
            $smarty->template_dir   = ROOT_PATH . 'error';
            $smarty->display('400.html');
            die();
        }
        $ip = real_ip();
        $now = local_strtotime('now');
        $blacklist_ip = [];
        $blacklist_user = [];
        $blacklist_ip[] = '112.118.71.46';
        $blacklist_ip[] = '1.64.113.183';
        $blacklist_user[] = '154956';
        $blacklist_user[] = '221924';
        $now = local_strtotime('now');
        $today_hour = local_date('Y-m-d-H'); //We cache this every hour
        $todaystarttime = local_strtotime(local_date('Y-m-d 00:00:00'));
        $todayendtime   = local_strtotime(local_date('Y-m-d 23:59:59'));
        $flashdeal_time = $this->memcache->get('flashdeal_time_'.$today_hour);
//        if ($flashdeal_time === false) {
        if (empty($flashdeal_time)) { //  Use empty but not false in this case.
            $flashdeal_time = $db->getCol(
                'SELECT fd.deal_date '.
                'FROM '.$ecs->table('flashdeals')."AS fd ".
                "LEFT JOIN ".$ecs->table('flashdeal_event')." AS fde ON fde.fd_event_id = fd.fd_event_id ".
                "WHERE fde.status = 1 AND fd.deal_date BETWEEN '" . $todaystarttime . "' AND '" . $todayendtime . "' "
            );
            $this->memcache->set('flashdeal_time_'.$today_hour, $flashdeal_time);
        }
        foreach ($flashdeal_time as $time) {
            if($now > ($time - 60) && $now <= ($time + 1800) && (in_array($ip, $blacklist_ip) || in_array($_SESSION['user_id'], $blacklist_user))) {
                    $_SESSION['black_user'] = true;
                    header("HTTP/1.0 400 Not Found");
                    $smarty->template_dir   = ROOT_PATH . 'error';
                    $smarty->display('400.html');
                    die();
            }
        }
        return true;
    }

    public function update_date_json()
    {
        global $ecs, $db;
        $time_list = $db->getCol(
            "SELECT fd.deal_date ".
            "FROM " . $ecs->table('flashdeals') . " AS fd ".
            "LEFT JOIN ".$ecs->table('flashdeal_event')." AS fde ON fde.fd_event_id = fd.fd_event_id ".
            "WHERE fde.status = 1 AND fd.deal_date >= '" . local_strtotime("midnight") . "' GROUP BY fd.deal_date ORDER BY fd.deal_date ASC ");
        $first = local_date('Y-m-d 00:00:00', reset($time_list));
        $last = local_date('Y-m-d 23:59:59', end($time_list));
        if(!empty($time_list)) {
            $json = [
                'event_start' => $first,
                'event_end' => $last,
            ];
            $file = self::JSON_PATH;
            file_put_contents($file, json_encode($json));
        }
        return true;
    }

    public function read_date_json()
    {
        $file = self::JSON_PATH;
        $data = file_get_contents($file);
        echo $data;
        return true;
    }

    public function is_flashdeal_today() {
        global $ecs, $db;

        // Get 00:00
        $startdate = local_strtotime(local_date('Y-m-d 00:00:00'));
        
        // Get 23:59
        $enddate   = local_strtotime(local_date('Y-m-d 23:59:59'));

        $time_list = $db->getCol(
            "SELECT fd.deal_date ".
            "FROM ".$ecs->table('flashdeals')." AS fd ".
            "LEFT JOIN ".$ecs->table('flashdeal_event')." AS fde ON fde.fd_event_id = fd.fd_event_id ".
            "WHERE fd.deal_date BETWEEN '".$startdate."' AND '".$enddate."' ".
            "AND fde.status = 1 ".
            "GROUP BY fd.deal_date ORDER BY fd.deal_date ASC "
        );

        if (sizeof($time_list) > 0) {
            return true;
        } else {
            return false;
        }

        exit;
    }

    // NEW flashdeal_event

    public function getFlashdealEventList() {
        global $ecs, $db;

        $sql = "SELECT fd_event_id, count(*) as price_abnormal_num ".
                "FROM ".$ecs->table('flashdeals')." AS f ".
                "LEFT JOIN ".$ecs->table('goods')." AS g ON f.goods_id = g.goods_id ".
                "WHERE  f.deal_price > g.shop_price and f.fd_event_id > 0 ".
                "GROUP BY f.fd_event_id ";
        $price_abnormal_flashdeals = $db->getAll($sql);

        $price_abnormal = array();
        foreach($price_abnormal_flashdeals as $price_abnormal_flashdeal){
            $price_abnormal[$price_abnormal_flashdeal['fd_event_id']] = $price_abnormal_flashdeal['price_abnormal_num'];
        }
        //print_r($price_abnormal);die();

        $sql = "SELECT fe.* , COUNT(f.flashdeal_id) AS no_of_goods ".
                "FROM ".$ecs->table('flashdeal_event')." AS fe ".
                "LEFT JOIN ".$ecs->table('flashdeals')." AS f ON f.fd_event_id = fe.fd_event_id ".
                "GROUP BY fe.fd_event_id ".
                "ORDER BY fe.fd_event_id DESC ";
        $res = $GLOBALS['db']->query($sql);
        
        while ($row = $GLOBALS['db']->fetchRow($res))
        {
            $row['price_abnormal_num']  = $price_abnormal[$row['fd_event_id']] > 0? $price_abnormal[$row['fd_event_id']]:0;
    
            $list[] = $row;
        }
        //print_r($list);
        if (sizeof($list) > 0){
            return $list;
        } else {
            return array();
        }

    }

    public function getFlashdealEventById($id) {
        global $ecs, $db;
        $sql = "SELECT fe.* ".
                "FROM ".$ecs->table('flashdeal_event')." AS fe ".
                "WHERE fe.fd_event_id = $id ".
                "ORDER BY fe.last_update_time DESC ";

        $res = $db->getRow($sql);

        if (!empty($res)){
            return $res;
        } else {
            return FALSE;
        }
    }

    public function updateFlashdealEventStatusById($id, $status) {
        global $ecs, $db;
        $sql = "UPDATE ".$ecs->table('flashdeal_event')." SET status = '$status' WHERE fd_event_id = '$id'";
        $res = $db->query($sql);
    }

    public function updateFlashdealEvent($event_id, $event, $goods) {
        if (empty($event_id)){
            return FALSE;
        }
        global $ecs, $db;

        if (sizeof($event['sql_update']) > 0){
            $str_set_action = implode(", ",$event['sql_update']);

            $sql = "UPDATE ".$ecs->table('flashdeal_event')." ";
            $sql .= "SET ".$str_set_action." ";
            $sql .= "WHERE fd_event_id = '".$event_id."' ";
            $res = $db->query($sql);

            $current_goods_id = $this->getEventGoodId($event_id);

            $new_goods_id = $event['goods_id'];

            if (!$current_goods_id){
                $this -> insertEventGoods($event_id, $goods);
            } else {
                $good_id_delete = array_diff($current_goods_id, $new_goods_id);
                $good_id_insert = array_diff($new_goods_id, $current_goods_id);
                $good_id_update = array_diff(array_diff($current_goods_id, $good_id_delete), $good_id_insert);

                foreach($good_id_delete as $good_id){
                    $this -> deleteEventGoods($event_id, $good_id);
                }

                $to_be_insert = array();

                foreach($good_id_insert as $good_id){
                    array_push($to_be_insert, $goods[$good_id]);
                }

                if (sizeof($to_be_insert) > 0){
                    $this -> insertEventGoods($event_id, $to_be_insert);
                }

                $to_be_update = array();

                foreach($good_id_update as $good_id){
                    array_push($to_be_update, $goods[$good_id]);
                }

                if (sizeof($to_be_update) > 0){
                    $this -> updateEventGoods($event_id, $to_be_update);
                }

    
            }

        } else {
            return FALSE;
        }
    }

    public function getEventGoodId($id) {
        global $ecs, $db;
        $sql = "SELECT f.goods_id ".
                "FROM ".$ecs->table('flashdeals')." AS f ".
                "WHERE f.fd_event_id = $id ".
                "ORDER BY f.goods_id ASC ";

        $res = $db->getAll($sql);

        if (!empty($res)){
            $arr_list = array();
            foreach ($res as $id){
                array_push($arr_list, $id['goods_id']);
            }
            return $arr_list;
        } else {
            return FALSE;
        }
    }

    public function insertEventGoods($event_id, $event_goods) {
        global $ecs, $db;
        if ($event_id){
            foreach ($event_goods as $good){
                if($good['deal_quantity'] < 1){
                    continue;
                }
                $good['fd_event_id'] = $event_id;
                $fd_event_good_id = -1;
                $db->autoExecute($ecs->table('flashdeals'), $good, 'INSERT');
                $fd_event_good_id = $db->insert_id();
                if ($fd_event_good_id){
                    $timestamp_now = local_strtotime(date('Y-m-d H:i:s'));
                    for ($i = 0; $i < $good['deal_quantity']; $i++){
                        $db->autoExecute($ecs->table('flashdeal_goods'), array("flashdeal_id"=>$fd_event_good_id, "status"=>"0", "create_at"=>$timestamp_now, "update_time"=>$timestamp_now), 'INSERT');
                    }
                }
            }
        }
    }

    public function deleteEvent($event_id) {
        global $ecs, $db;
        if ($event_id){
            $sql = "DELETE FROM ".$ecs->table('flashdeal_event')." WHERE fd_event_id = ".$event_id;
            $db->query($sql);
            $flashdeal_id = $this->getFlashdealIdByEvent($event_id);
            if ($flashdeal_id){
                foreach ($flashdeal_id as $flashdeal){
                    $sql = "DELETE FROM ".$ecs->table('flashdeal_goods')." WHERE flashdeal_id = ".$flashdeal['flashdeal_id']." AND status = 0";
                    $db->query($sql);
                    $sql = "DELETE FROM ".$ecs->table('flashdeals')." WHERE flashdeal_id = ".$flashdeal['flashdeal_id']." AND fd_event_id = ".$event_id;
                    $db->query($sql);
                }
            }
        }
    }

    public function deleteEventGoods($event_id, $good_id, $fd_id = -1) {
        global $ecs, $db;
        if ($fd_id <= 0 && $event_id && $good_id){
            $flashdeal_id = $this->getFlashdealIdByEventAndGood($event_id, $good_id);
        } else {
            $flashdeal_id = $fd_id;
        }
        if ($flashdeal_id > 0){
            $sql = "DELETE FROM ".$ecs->table('flashdeal_goods')." WHERE flashdeal_id = ".$flashdeal_id." AND status = 0";
            $db->query($sql);
            $sql = "DELETE FROM ".$ecs->table('flashdeals')." WHERE flashdeal_id = ".$flashdeal_id." ";
            $db->query($sql);
        }
    }

    public function updateEventGoods($event_id, $event_goods) {
        global $ecs, $db;
        if ($event_id){
            foreach ($event_goods as $good){
                if($good['deal_quantity'] < 1){
                    $this -> deleteEventGoods($event_id, $good['goods_id']);
                    continue;
                }
                $flashdeal_id = $this->getFlashdealIdByEventAndGood($event_id, $good['goods_id']);
                if ($flashdeal_id){
                    $sql = "SELECT COUNT(*) FROM ".$ecs->table('flashdeal_goods')." WHERE flashdeal_id = $flashdeal_id GROUP BY flashdeal_id";
                    $no_flashdeal_goods = $db->getOne($sql);
                    $sql = "SELECT COUNT(*) FROM ".$ecs->table('flashdeal_goods')." WHERE flashdeal_id = $flashdeal_id AND status > 0 GROUP BY flashdeal_id";
                    $no_bought_flashdeal_goods = $db->getOne($sql);
                    if (!$no_bought_flashdeal_goods){
                        $no_bought_flashdeal_goods = 0;
                    }
                    if ($good['deal_quantity'] < $no_bought_flashdeal_goods){
                        $good['deal_quantity'] = $no_bought_flashdeal_goods;
                    }

                    //$sql = "UPDATE ".$ecs->table('flashdeals')." SET deal_price = '".$good['deal_price']."', deal_quantity = '".$good['deal_quantity']."', sort_order = '".$good['sort_order']."', detail_visible = '".$good['detail_visible']."', is_online_pay = '".$good['is_online_pay']."', is_mobile_pay = '".$good['is_mobile_pay']."', deal_date = '".$good['deal_date']."' WHERE goods_id = '".$good['goods_id']."' AND fd_event_id = '".$good['fd_event_id']."'";
                    $sql = "UPDATE ".$ecs->table('flashdeals')." SET deal_price = '".$good['deal_price']."', deal_quantity = '".$good['deal_quantity']."', sort_order = '".$good['sort_order']."', detail_visible = '".$good['detail_visible']."', is_online_pay = '".$good['is_online_pay']."', deal_date = '".$good['deal_date']."', payme_only = '".$good['payme_only']."', alipay_only='".$good['alipay_only']."' WHERE goods_id = '".$good['goods_id']."' AND fd_event_id = '".$good['fd_event_id']."'";
                    $db->query($sql);

                    if ($good['deal_quantity'] > $no_flashdeal_goods){
                        $quantity_add = $good['deal_quantity'] - $no_flashdeal_goods;
                        $timestamp_now = local_strtotime(date('Y-m-d H:i:s'));
                        for ($i = 0; $i < $quantity_add; $i++){
                            $db->autoExecute($ecs->table('flashdeal_goods'), array("flashdeal_id"=>$flashdeal_id, "status"=>"0", "create_at"=>$timestamp_now, "update_time"=>$timestamp_now), 'INSERT');
                        }
                    } elseif ($good['deal_quantity'] < $no_flashdeal_goods) {
                        $quantity_del = $no_flashdeal_goods - $good['deal_quantity'];
                        $sql = "SELECT fg_id FROM ".$ecs->table('flashdeal_goods')." WHERE flashdeal_id = $flashdeal_id AND status = 0 ORDER BY fg_id DESC";
                        $arr_goods_not_sell = $db->getAll($sql);

                        for ($i = 0; $i < $quantity_del; $i++){
                            $sql = "DELETE FROM ".$ecs->table('flashdeal_goods')." WHERE fg_id = ".$arr_goods_not_sell[$i]['fg_id']." AND status = 0";
                            $db->query($sql);
                        }
                    }
                }
            }
        }
    }

    public function convertFlashdeal($event_id, $fd_id) {
        global $ecs, $db;
        if ($event_id){
            foreach ($fd_id as $id){
                if ($id){
                    $sql = "UPDATE ".$ecs->table('flashdeals')." SET fd_event_id = '".$event_id."' WHERE flashdeal_id = '".$id['flashdeal_id']."'";
                    $db->query($sql);
                }
            }
        }
    }

    public function getFlashdealIdByEventAndGood($event_id, $good_id) {
        global $ecs, $db;
        $sql = "SELECT flashdeal_id FROM ".$ecs->table('flashdeals')."  WHERE fd_event_id = $event_id AND goods_id = $good_id ORDER BY `flashdeal_id` DESC";
        $flashdeal_id = $db->getOne($sql);
        if (!empty($flashdeal_id)){
            return $flashdeal_id;
        } else {
            return FALSE;
        }
    }

    public function getFlashdealIdByEvent($event_id) {
        global $ecs, $db;
        $sql = "SELECT flashdeal_id FROM ".$ecs->table('flashdeals')."  WHERE fd_event_id = $event_id ORDER BY `flashdeal_id` DESC";
        $flashdeal_id = $db->getAll($sql);
        if (!empty($flashdeal_id)){
            return $flashdeal_id;
        } else {
            return FALSE;
        }
    }

    public function getFlashdealGoodsById($id, $order = 'flashdeal_id', $sort = 'ASC', $where = array()) {
        global $ecs, $db;
        if (in_array($order, ['no_good_sellable', 'no_good_sold'])){
            $order = 'flashdeal_id';
            $sort = 'ASC';
        }
        //$sql = "SELECT f.flashdeal_id, f.goods_id, f.deal_price, f.deal_quantity, g.goods_name, g.shop_price, g.cost, f.sort_order, f.detail_visible, f.is_online_pay, f.is_mobile_pay ".
        $sql = "SELECT f.flashdeal_id, f.goods_id, f.deal_price, f.deal_quantity, g.goods_name, g.shop_price, g.cost, GROUP_CONCAT(s.supplier_id) as supplier_ids, f.sort_order, f.detail_visible, f.is_online_pay, f.alipay_only, f.payme_only, COUNT(fn.notify_id) AS notify_count ".
                "FROM ".$ecs->table('flashdeals')." AS f ".
                "LEFT JOIN ".$ecs->table('goods')." AS g ON g.goods_id = f.goods_id ".
                "LEFT JOIN ".$ecs->table('erp_goods_supplier')." AS s ON s.goods_id = g.goods_id ".
                "LEFT JOIN ". $ecs->table('flashdeal_notify')." AS fn ON fn.flashdeal_id = f.flashdeal_id " .
                "WHERE f.fd_event_id = $id ";
        
        if (!empty($where)){
            $sql .= " AND ".implode(' AND ', $where)." ";
        }

        $sql .= "GROUP BY f.flashdeal_id " .
                "ORDER BY " . $order . " " . $sort;
        if ($order != 'flashdeal_id'){
            $sql .= ", flashdeal_id ASC";
        }
        $res = $db->getAll($sql);
        if (!empty($res)){
            return $res;
        } else {
            return FALSE;
        }
    }

    public function getFlashdealGroupDate() {
        global $ecs, $db;

        $sql = "SELECT f.deal_date ".
                "FROM ".$ecs->table('flashdeals')." AS f ".
                "WHERE f.fd_event_id IS NULL ".
                "GROUP BY f.deal_date ";

        $res = $db->getAll($sql);

        if (sizeof($res) > 0){
            return $res;
        } else {
            return array();
        }
    }

    public function getFlashdealIdByDate($str_date_time) {
        global $ecs, $db;

        $sql = "SELECT f.flashdeal_id ".
                "FROM ".$ecs->table('flashdeals')." AS f ".
                "WHERE f.deal_date = $str_date_time ".
                "ORDER BY f.flashdeal_id DESC ";

        $res = $db->getAll($sql);

        if (sizeof($res) > 0){
            return $res;
        } else {
            return array();
        }
    }

    public function getSuppliersList(){
        global $ecs, $db;
        $sql = 'SELECT supplier_id, code, name FROM '.$ecs->table('erp_supplier').' WHERE is_valid = 1 ORDER BY name ASC';
        $res = $GLOBALS['db']->getAll($sql);
    
        if (!is_array($res)){
            return FALSE;
        } else {
            return $res;
        }
    }

    public function getTopStockGoods($id, $no, $exclude_id){
        $erpController = new ErpController();
        global $ecs, $db;
        $sql = "SELECT g.goods_id, g.goods_name, g.shop_price, g.cost, IFNULL((SUM(gas.goods_qty)), 0) AS stock ".
                "FROM ".$ecs->table('goods')." AS g ".
                "LEFT JOIN ".$ecs->table('erp_goods_attr_stock')." AS gas ON gas.goods_id = g.goods_id ".
                "LEFT JOIN ".$ecs->table('erp_goods_supplier')." AS gs ON gs.goods_id = g.goods_id ".
                "WHERE gas.warehouse_id IN (".$erpController->getSellableWarehouseIds(true).") ".
                "AND g.is_real = 1 ".
                "AND g.is_on_sale = 1 ";
        if ($id > 0){
            $sql .= "AND gs.supplier_id = ".$id." ";
        }
        if ($exclude_id){
            $sql .= "AND g.goods_id NOT IN (".$exclude_id.") ";
        }
        $sql .= "GROUP BY g.goods_id ".
                "ORDER BY stock DESC ".
                "LIMIT ".$no." ";
        $res = $GLOBALS['db']->getAll($sql);
    
        if (sizeof($res) == 0){
            return FALSE;
        } else {
            return $res;
        }
    }

    public function getGoodsBykeyword($key, $arr_where){
        $erpController = new ErpController();

        global $ecs, $db;
        $sql = "SELECT g.goods_id, g.goods_name, g.shop_price, g.cost, IFNULL(SUM(gas.goods_qty), 0) AS stock  ".
                "FROM ".$ecs->table('goods')." AS g ".
                "LEFT JOIN ".$ecs->table('erp_goods_attr_stock')." AS gas ON gas.goods_id = g.goods_id ".
                "WHERE gas.warehouse_id IN (".$erpController->getSellableWarehouseIds(true).") ".
                "AND g.is_real = 1 ".
                "AND g.is_on_sale = 1 ";
        if ($arr_where){
            $sql .= "AND ".implode(" AND ", $arr_where)." ";
        }
        if ($key != ''){
            $sql .= "AND (g.goods_name LIKE '%".$key."%' OR g.goods_id LIKE '%".$key."%' OR g.goods_sn LIKE '%".$key."%') ";
        }
        $sql .= "GROUP BY g.goods_id ".
                "ORDER BY g.goods_name ASC ";

        $res = $GLOBALS['db']->getAll($sql);

        return $res;
    }

    public function getGoodsBykeywordIncludeGoods($key, $arr_where){
        $erpController = new ErpController();

        global $ecs, $db;
        $sql = "SELECT g.goods_id, g.goods_name, g.shop_price, g.cost, GROUP_CONCAT(s.supplier_id) as supplier_ids, IFNULL(SUM(gas.goods_qty), 0) AS stock  ".
                "FROM ".$ecs->table('goods')." AS g ".
                "LEFT JOIN ".$ecs->table('erp_goods_attr_stock')." AS gas ON gas.goods_id = g.goods_id ".
                "LEFT JOIN ".$ecs->table('erp_goods_supplier')." AS s ON s.goods_id = g.goods_id ".
                "WHERE gas.warehouse_id IN (".$erpController->getSellableWarehouseIds(true).") ".
                "AND g.is_real = 1 ".
                "AND g.is_on_sale = 1 ";
        if ($arr_where){
            $sql .= "AND ".implode(" AND ", $arr_where)." ";
        }
        if ($key != ''){
            if(strpos($key, ",") > 0){
                $include_goods = explode(",", $key);
                
                $include_goods_like_str = array_map(function ($goods_name) { return "g.goods_name like '%".trim(mysql_like_quote($goods_name))."%'"; }, $include_goods);
                $include_goods_sn_like_str = array_map(function ($goods_sn) { return "g.goods_sn like '%".trim(mysql_like_quote($goods_sn))."%'"; }, $include_goods);
                $include_goods_id_str = array_map(function ($goods_id) { return "g.goods_id = '".trim(mysql_like_quote($goods_id))."'"; }, $include_goods);

                $sql .= "AND " .implode(" OR ", $include_goods_like_str)." OR ".implode(" OR ", $include_goods_sn_like_str)." OR ".implode(" OR ", $include_goods_id_str);
            }
            else{
                $sql .= " AND (g.goods_sn LIKE '%" . mysql_like_quote($key) . "%' ";
                $sql .= " OR g.goods_name LIKE '%" . mysql_like_quote($key) . "%' ";
                $tmp = preg_split('/\s+/', $key);
                if (count($tmp) > 1)
                {
                    $sql .= " OR ( 1";
                    foreach ($tmp as $kw)
                    {
                        $sql .= " AND g.goods_name LIKE '%" . mysql_like_quote($kw) . "%' ";
                    }
                    $sql .= ") ";
                }
                $sql .= " OR g.goods_id = '" . mysql_like_quote($key) . "')";
            }
        }
        $sql .= "GROUP BY g.goods_id ".
                "ORDER BY g.goods_name ASC ";
        $res = $GLOBALS['db']->getAll($sql);

        return $res;
    }

    public function getSellableFlashdealGoodQty($flashdeal_id){
        global $ecs, $db;
        $sql = "SELECT COUNT(flashdeal_id) AS sellable_qty FROM ".$ecs->table('flashdeal_goods')." WHERE flashdeal_id = $flashdeal_id AND status = 0";
        $res = $GLOBALS['db']->getOne($sql);
        
        return $res;
    }

    public function getSoldFlashdealGoodQty($flashdeal_id){
        global $ecs, $db;
        $sql = "SELECT COUNT(flashdeal_id) AS sellable_qty FROM ".$ecs->table('flashdeal_goods')." WHERE flashdeal_id = $flashdeal_id AND status = 2";
        $res = $GLOBALS['db']->getOne($sql);

        return $res;
    }

    public function getFlashdealCommonPaymentMethod()
    {
        global $db, $ecs;
        $flashdeal_payment_method_query = "SELECT fd.is_online_pay as is_online_pay, fd.payme_only as payme_only, fd.alipay_only as alipay_only " .
                                                "FROM " . $ecs->table('cart') . " AS c, " . $ecs->table('flashdeals') . " AS fd " .
                                                "WHERE c.flashdeal_id = fd.flashdeal_id " . 
                                                "AND c.session_id = '" . SESS_ID . "'";
        $cart_flashdeal_summary = $db->getAll($flashdeal_payment_method_query);

        $max_count = 0;
        $payment_method_count = [];
        $restricted_goods_total = 0;
        if (!isset($cart_flashdeal_summary) || sizeof($cart_flashdeal_summary) == 0) { // No flashdeal item -> allow to add
            return null;
        }

        $available_payment_method = [];
        foreach($cart_flashdeal_summary as $item) {
            if (array_sum($item) == 0) { // Unrestricted Flashdeal item
                continue;
            }
            $restricted_goods_total += 1;
            foreach($item as $method => $count) {
                $payment_method_count[$method] += $count;
            }
        }

        if ($restricted_goods_total == 0) {
            return null;
        }

        $max_payment_method_count = max($payment_method_count);

        foreach($payment_method_count as $method => $count)
        {
            if ($count == $restricted_goods_total) {
                switch($method) {
                    case 'payme_only':
                        array_push($available_payment_method, 'payme');
                        break;
                    case 'alipay_only':
                        array_push($available_payment_method, 'alipayhk');
                        array_push($available_payment_method, 'alipaycn');
                        break;
                    case 'is_online_pay':
                        array_push($available_payment_method, 'braintree');
                        array_push($available_payment_method, 'braintreepaypal');
                        array_push($available_payment_method, 'paypal');
                        array_push($available_payment_method, 'stripe');
                        break;
                    default:
                        break;
                }
            }
        }
        return $available_payment_method;
    }

    private function checkIfFlashDealItemListHaveCommonPaymentMethod($flashdeal_item_list)
    {
        $max_count = 0;
        $payment_method_count = [];
        $restricted_goods_total = 0;
        if (!isset($flashdeal_item_list) || sizeof($flashdeal_item_list) == 0) { // No flashdeal item -> allow to add
            return true;
        }

        foreach($flashdeal_item_list as $item) {
            if (array_sum($item) == 0) { // Unrestricted Flashdeal item
                continue;
            }
            $restricted_goods_total += 1;
            foreach($item as $method => $count) {
                $payment_method_count[$method] += $count;
            }
        }
        $max_count = max($payment_method_count);

        return $max_count == $restricted_goods_total;
    }

    public function preCreateFlashDealCache() {
        global $ecs, $db,$smarty;

        $domain = $ecs->url();
        $domain = substr($domain, 0, -1);
        $m_domain = str_replace('//beta', '//m.beta',str_replace('//www', '//m', $domain));

        // 1. get flashdeal goods 
        //$sql = "select * from ".$ecs->table('flashdeals')." where flashdeal_id = 25";  
        // by flashdeal event id 
        if (isset($_REQUEST['c_fd_event_id']) && $_REQUEST['c_fd_event_id']!='') {
            $sql = "select * from ".$ecs->table('flashdeals')." where fd_event_id = '".$_REQUEST['c_fd_event_id']."' ";
        }else {
            $now = gmtime();
            $time_buffer = gmtime() + 15 * 60 + 1;
            $sql = "select * from ".$ecs->table('flashdeals')." where deal_date >= ".$now." and deal_date <= ".$time_buffer." ";
        }

        $flashdeals = $db->getAll($sql);
        // get all ranking
        // $sql = "SELECT rank_id FROM  ".$ecs->table('user_rank')." where 1";
        // $rankings = $db->getCol($sql);
        $rankings = ['0','1','2','3']; // no login,會員 VIP會員 大專生優惠計劃
        // get all language
        $langs = ['zh_tw','en_us']; // 'zh_tw','zh_cn','en_us'
        $urls = [];
        foreach ($flashdeals as $flashdeal) {
            //dd($flashdeal);
            // 2. build cache combination $cache_id = $goods_id . '-' . $_SESSION['user_rank'] . '-' . $_CFG['lang'].'-'.$flashdeal_id.'-'.$is_now.'-'.$is_testing;
            // 3. build goods url
            foreach ($rankings as $ranking) {
                foreach ($langs as $lang) {
                    $urls[] = $domain.'/product/'.$flashdeal['goods_id'].'?c_ur='.$ranking.'&c_lang='.$lang.'&c_fdid='.$flashdeal['flashdeal_id'];
                    $urls[] = $m_domain.'/product/'.$flashdeal['goods_id'].'?c_ur='.$ranking.'&c_lang='.$lang.'&c_fdid='.$flashdeal['flashdeal_id'];
                }
            }
        }

        if (sizeof($urls)>0) {
            require_once("includes/rolling-curl/RollingCurl.php");
            $rc = new \RollingCurl("flashdeal_request_callback");
            $rc->window_size = 20;
            foreach ($urls as $url) {
                //$url, $method = "GET", $post_data = null, $headers = null, $options = null
                // RollingCurlRequest($url, $method, $post_data, $headers, $options);
                if (strpos( $url, 'm.beta') !== false) {
                    $op = [CURLOPT_COOKIE => 'use_mobile_view=1;'];
                } else {
                    $op = [];
                }
                
                $request = new \RollingCurlRequest($url,"GET",null,null,$op);
                $rc->add($request);
            }
            $rc->execute();
        } else {
            if ($_REQUEST['force'] == 1) {
                echo 'no goods need to generate.';
            }
        }
        exit;
        // $ch = curl_init('https://m.beta.yohohongkong.com/product/2349?c_ur=0&c_lang=en_us&c_fdid=25');
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        // // curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        // curl_setopt($ch, CURLOPT_COOKIE, 'use_mobile_view=1;');
        // $result = curl_exec($ch);
        // curl_close($ch);
        // exit;

        //  var_dump( file_get_contents("https://beta.yohohongkong.com/product/1935"));
    }

    function checkFlashdealEventIsGoingToStart(){
        global $ecs, $db;

        $now_time = date("Y-m-d H:i:s");
        
        $sql = "SELECT count(*) FROM `ecs_flashdeal_event` where status = 1 and '". $now_time."' >= start_date - INTERVAL 10 MINUTE and '". $now_time."' <= start_date + INTERVAL 15 MINUTE  ";

        if ($db->getOne($sql) > 0) {
            return true;
        } else {
            return false;
        }
    }
}

