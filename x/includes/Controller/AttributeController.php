<?php

/***
* AttributeController.php
* by Anthony 20170707
*
* Attribute controller
***/

namespace Yoho\cms\Controller;
use Yoho\cms\Model;
if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

class AttributeController extends YohoBaseController
{

    /**
    * This function is for cms attribute.php update attribute of goods list when the attribute is changed.
    * When user update attribute, auto update the table "goods_att".
    * @param     string  $attr_id
    * @param     string  $attr_values
    * @return    bool
    **/

    function auto_update_goods_attr($attr_id, $attr_values, $old_attr_values)
    {
        if(empty($attr_id)) return false;
        $attr = explode("\n", $attr_values);
        $old_attr = explode("\n", $old_attr_values);
        if(!empty($attr) && !empty($old_attr))
        {
            for($i=0; $i<count($old_attr); $i++){
                $sql = "UPDATE ".$this->ecs->table('goods_attr')." SET attr_value = '".trim($attr[$i])."'  WHERE attr_value = '".trim($old_attr[$i])."' and attr_id = ".$attr_id;
                $this->db->query($sql);
            }
        }
        elseif(empty($attr))
        {
            $sql = "DELETE FROM ".$this->ecs->table('goods_attr')." WHERE attr_id = ".$attr_id;
            $this->db->query($sql);
        }
    }


    /**
    * This function is for cms attribute.php insert/update attribute.
    * When user insert/update attribute, auto add in category.
    * @param     string  $cat_id
    * @param     string  $attr_id
    * @return    bool
    **/
    function auto_update_cat_filter_attr($cat_id)
    {
        if(empty($cat_id)) return false;

        // Get all attr_id
        $sql = "SELECT attr_id FROM ".$this->ecs->table('attribute')." WHERE cat_id = ".$cat_id;
        $filter_attr   = $this->db->getCol($sql);

        $new_filter_attr = implode(',', array_unique($filter_attr));

        //UPDATE category filter_attr
        $sql = "UPDATE ".$this->ecs->table('category')."SET `filter_attr` = '$new_filter_attr' WHERE cat_id = ".$cat_id;
        return $this->db->query($sql);

    }

    /**
    * This function is get cat related attr.
    * @param string $cat_id
    * @return array
    **/
    function get_cat_related_attr($cat_id)
    {
        global $smarty, $db, $ecs;

        if(empty($cat_id)) return array();

        $sql = "SELECT a.attr_id from ".$ecs->table('attribute')." as a ".
        "left join  ".$ecs->table('goods')." as g on g.cat_id = ".$cat_id." ".
        "left join ".$ecs->table('goods_attr')." as ga on ga.goods_id = g.goods_id ".
        "WHERE a.attr_id = ga.attr_id group by a.attr_id order by a.sort_order ";

        return $db->getCol($sql);
    }

    /**
    * This function is for search/brand/category online shop page, if have category selected, add filter list on left menu.
    * @param string $cat_id category id.
    * @param array $filter_attr         $_REQUEST['filter_attr']
    * @param string $page               Page name(e.g.: search, brand, category)
    * @param array $search_parameters   Url request
    * @return array                     Return attr_list and 查出符合所有筛选属性条件的商品id的SQL
    **/
    function get_attr_data($cat_id, $filter_attr, $page, $search_parameters)
    {
        global $smarty, $db, $ecs, $_LANG, $_CFG;
        $cat_filter_attr = $this->get_cat_related_attr($cat_id);
        $children = get_children($cat_id);

        $res = array(
            'all_attr_list'  => array(),
            'ext'            => '',
            'selected_value' => array()
        );
        $selected_value = array();
        if (empty($cat_filter_attr)) return $res;
        // Get attribute id
        $all_attr_list = array();

        foreach ($cat_filter_attr AS $key => $value)
        {
            $sql = "SELECT IFNULL(al.attr_name, a.attr_name) as attr_name, g.goods_id FROM " .
                    $ecs->table('attribute') . " AS a ".
                    " LEFT JOIN " . $ecs->table('attribute_lang') . " as al ON al.attr_id = a.attr_id AND al.lang = '" . $_CFG['lang'] . "' , " .
                    $ecs->table('goods_attr') . " AS ga, " .
                    $ecs->table('goods') . " AS g ".
                    "WHERE ($children OR " . get_extension_goods($children) . ")".
                    "AND a.attr_id = ga.attr_id AND g.goods_id = ga.goods_id AND g.is_delete = 0 AND g.is_on_sale = 1 AND g.is_alone_sale = 1 AND a.attr_id='$value'";

            if($temp_name = $db->getOne($sql))
            {
                $attr_values = array();
                /* Anthony: for multiple language */
                $sql = "SELECT attr_values FROM ".$ecs->table('attribute') ." WHERE attr_id = ".$value;
                $attr_values = $db->getOne($sql);
                $attr_values = explode("\n", $attr_values);
                $attr_values = array_map('trim', $attr_values);

                $all_attr_list[$key]['filter_attr_name'] = $temp_name;
                $all_attr_list[$key]['filter_attr_id'] = $value;
                $sql = "SELECT a.attr_id, MIN(a.goods_attr_id ) AS goods_id,  IFNULL(al.attr_value, a.attr_value) as attr_value FROM " . $ecs->table('goods_attr') . " AS a ".
                    " LEFT JOIN " . $ecs->table('goods_attr_lang') . " as al ON al.goods_attr_id = a.goods_attr_id AND al.lang = '" . $_CFG['lang'] . "' , " .
                    $ecs->table('goods') ." AS g" .
                    " WHERE ($children OR " . get_extension_goods($children) . ') AND g.goods_id = a.goods_id AND g.is_delete = 0 AND g.is_on_sale = 1 AND g.is_alone_sale = 1 '.
                    " AND a.attr_id='$value' ".
                    " GROUP BY a.attr_value";
                $tmp_attr_list = $db->getAll($sql);

                /* Sorting by attr_values */
                $attr_list = array();
                foreach ($tmp_attr_list as $al_k => $al_v) {
                    $sort_key = array_search($al_v['attr_value'], $attr_values);
                    $attr_list[$sort_key] = $al_v;
                }
                ksort($attr_list);

                $temp_arrt_url_arr = array();

                for ($i = 0; $i < count($cat_filter_attr); $i++)        //获取当前url中已选择属性的值，并保留在数组中
                {
                    $temp_arrt_url_arr[$i] = !empty($filter_attr[$i]) ? $filter_attr[$i] : 0;
                }

                $temp_arrt_url_arr[$key] = 0;                           //“全部”的信息生成
                $temp_arrt_url = implode('.', $temp_arrt_url_arr);
                $all_attr_list[$key]['attr_list'][0]['attr_value'] = $_LANG['all_attribute'];
                $all_attr_list[$key]['attr_list'][0]['url'] = build_uri($page, array_merge($search_parameters, array('filter_attr'=>$temp_arrt_url)));
                $all_attr_list[$key]['attr_list'][0]['selected'] = empty($filter_attr[$key]) ? 1 : 0;

                foreach ($attr_list as $k => $v)
                {
                    $temp_key = $k + 1;
                    $temp_arrt_url_arr[$key] = $v['goods_id'];       //为url中代表当前筛选属性的位置变量赋值,并生成以‘.’分隔的筛选属性字符串
                    $temp_arrt_url = implode('.', $temp_arrt_url_arr);

                    /* multiple language */
                    $tmp_key = array_search($v['attr_value'], $attr_values);
                    if(isset($tmp_key) && $tmp_key != false && $_CFG['lang'] != 'zh_tw') // If find key, check attr other lang (TODO: default:zh_tw)
                    {
                        $sql = "SELECT attr_values FROM ".$ecs->table('attribute_lang') ." WHERE attr_id = ".$value." AND lang = '".$_CFG['lang']."' ";
                        $attr_values_lang = $db->getOne($sql);
                        if(!empty($attr_values_lang))
                        {
                            $attr_values_lang = explode("\n", $attr_values_lang);
                            $attr_values_lang = array_map('trim', $attr_values_lang);
                            $v['attr_value']  = isset($attr_values_lang[$tmp_key]) ? $attr_values_lang[$tmp_key] : $v['attr_value'];
                        }
                    }
                    $all_attr_list[$key]['attr_list'][$temp_key]['attr_value'] = $v['attr_value'];
                    $all_attr_list[$key]['attr_list'][$temp_key]['url'] = build_uri($page, array_merge($search_parameters, array('filter_attr'=>$temp_arrt_url)));

                    if (!empty($filter_attr[$key]) AND $filter_attr[$key] == $v['goods_id'])
                    {
                        $all_attr_list[$key]['attr_list'][$temp_key]['selected'] = 1;
                        $selected_value[] = $all_attr_list[$key]['attr_list'][$temp_key]['attr_value'];
                    }
                    else
                    {
                        $all_attr_list[$key]['attr_list'][$temp_key]['selected'] = 0;
                    }
                }
            }
        }

        $ext = '';
        $ext_sql = "SELECT DISTINCT(b.goods_id) FROM " . $ecs->table('goods_attr') . " AS a, " . $ecs->table('goods_attr') . " AS b " .  "WHERE ";
        $ext_group_goods = array();

        foreach ($filter_attr AS $k => $v)                      // 查出符合所有筛选属性条件的商品id */
        {
            if (is_numeric($v) && $v !=0 &&isset($cat_filter_attr[$k]))
            {
                $sql = $ext_sql . "b.attr_value = a.attr_value AND b.attr_id = " . $cat_filter_attr[$k] ." AND a.goods_attr_id = " . $v;
                $ext_group_goods = $db->getColCached($sql);
                $ext .= ' AND ' . db_create_in($ext_group_goods, 'g.goods_id');
            }
        }

        /* Return data */
        $res['all_attr_list']  = $all_attr_list;
        $res['ext']            = $ext;
        $res['ext_goods']      = $ext_group_goods;
        $res['selected_value'] = $selected_value;

        return $res;
    }

    /**
    * This function is for cms goods_attr_by_attr page, get goods list by attribute.
    * @param string $attr_id
    * @param array  $filter
    * @return array
    **/
    function get_attr_goods_list($attr_id, $filter)
    {
        if(empty($attr_id)) return false;

        $attr_data  = array();
        $keyword    = $filter['keyword'];
        $sort_by    = $filter['sort_by'];
        $sort_order = $filter['sort_order'];
        $filter['start'] = $filter['start'] ? $filter['start'] : 0;
        $filter['page_size']  = $filter['page_size'] ? $filter['page_size'] : 10;

        /* Get attribute data */
        $attr = new Model\Attribute($attr_id);
        $data = $attr->data;
        if(empty($data)) return false;

        $cat_id   = $data['cat_id'];
        $data['cat_name'] = $this->db->getOne("SELECT cat_name FROM ".$this->ecs->table('category')." WHERE cat_id = $cat_id LIMIT 1 ");

        $where = " WHERE g.cat_id = $cat_id AND g.is_delete = 0 AND g.is_on_sale = 1 ";
        /* Keyword where */
        if (!empty($keyword))
        {
            $where .= " AND (g.goods_sn LIKE '%" . mysql_like_quote($keyword) . "%' ";
            $where .= " OR g.goods_name LIKE '%" . mysql_like_quote($keyword) . "%' ";
            $tmp = preg_split('/\s+/', $keyword);
            if (count($tmp) > 1)
            {
                $where .= " OR ( 1";
                foreach ($tmp as $kw)
                {
                    $where .= " AND g.goods_name LIKE '%" . mysql_like_quote($kw) . "%' ";
                }
                $where .= ") ";
            }
            $where .= " OR g.goods_id = '" . mysql_like_quote($keyword) . "')";
        }

        $count_sql = "SELECT COUNT(*) FROM " . $this->ecs->table('goods') . " AS g $where";
        $filter['record_count'] = $this->db->getOne($count_sql);

        $sql = "SELECT g.goods_id, g.goods_name, ag.attr_value FROM ".$this->ecs->table('goods')." as g ".
            " LEFT JOIN ".$this->ecs->table('goods_attr')." as ag ON ag.goods_id = g.goods_id AND ag.attr_id = $attr_id ";

        $sql .= $where." ORDER BY $sort_by $sort_order LIMIT " . $filter['start'] .", $filter[page_size]";

        $goods_list = $this->db->getAll($sql);
        $attr_values = $this->attr_values_to_array($data['attr_values']);

        $attr_values_input = array();
        foreach ($attr_values as $key => $value) {
            $attr_values_input[$value] = $value;
        }
        $attr_data['attribute']    = $data;
        $attr_data['goods_list']   = $goods_list;
        $attr_data['attr_values']  = $attr_values_input;
        $attr_data['page_count']   = $filter['page_count'];
        $attr_data['record_count'] = $filter['record_count'];
        $attr_data['filter']       = $filter;

        return $attr_data;
    }

    /**
    * This function is for cms goods_attr_by_attr page, batch update or insert goods_attr with multipe language for same attr_value.
    * @param array $request         $_REQUEST
    * @return bool
    **/
    function update_goods_attr_by_attr_value($request)
    {
        /* Handle request value */
        $attr_id    = $request['attr_id'];
        $goods_ids  = $request['goods_ids'];
        if(!is_array($goods_ids)) $goods_ids  = explode(",", $request['goods_ids']);
        $attr_value = $request['attr_value'];

        if(empty($attr_id) || empty($goods_ids) || !isset($attr_value)) return false;

        if($attr_value == "attr_remove") // remove goods_attr when attr_value is 'attr_remove'
        {
            return $this->remove_goods_attr_by_attr_value($goods_ids, $attr_id);
        }

        /* Get need update goods ids */
        $sql = "SELECT goods_id FROM ".$this->ecs->table('goods_attr').
            " WHERE goods_id ". \db_create_in($goods_ids).
            " AND attr_id = $attr_id ";
        $update_goods = $this->db->getCol($sql);

        /* Get new goods ids */
        $insert_ids = array_diff($goods_ids, $update_goods);

        /* Insert goods attr */
        $sql = "INSERT INTO ".$this->ecs->table('goods_attr')." (goods_id, attr_id, attr_value)".
                    " SELECT goods_id, '$attr_id', '".$attr_value."' ".
                    " FROM ".$this->ecs->table('goods').
                    " WHERE goods_id ". \db_create_in($insert_ids);

        $this->db->query($sql);

        /* Update goods attr */
        $sql = "UPDATE ".$this->ecs->table('goods_attr')." SET  attr_value = '".$attr_value."' ".
                    " WHERE goods_id ". \db_create_in($update_goods)." AND attr_id = $attr_id ";

        $this->db->query($sql);

        /* Get attr_value lang */
        $sql = "SELECT al.*, a.attr_values as origin_attr_values FROM ".$this->ecs->table('attribute_lang')."as al ".
            " LEFT JOIN ".$this->ecs->table('attribute')." as a ON a.attr_id = al.attr_id ".
            " WHERE a.attr_id = ".$attr_id;
        $lang_list = $this->db->getAll($sql);

        /* Insert/Update for every attribute lang */
        foreach ($lang_list as $k => $attr)
        {
            $lang = $attr['lang'];
            /* Find attr value index in attrgit _value list */
            $origin_attr_values = $this->attr_values_to_array($attr['origin_attr_values']);
            $key = array_search($attr_value, $origin_attr_values);

            /* Match if have same index attr value */
            $attr_values = $this->attr_values_to_array($attr['attr_values']);
            $attr_value_lang = isset($attr_values[$key]) ? $attr_values[$key] : $attr_value;

            $sql = "INSERT INTO ".$this->ecs->table('goods_attr_lang')." (goods_attr_id, lang, attr_value)".
                    " SELECT goods_attr_id, '$lang', '".$attr_value_lang."' ".
                    " FROM ".$this->ecs->table('goods_attr').
                    " WHERE goods_id ". \db_create_in($goods_ids)." AND attr_id = $attr_id ".
                    " ON DUPLICATE KEY UPDATE attr_value = '$attr_value_lang' ";

            $this->db->query($sql);
        }

        /* Handle attribute index */
        $this->handle_attribute_index($attr_id);

        return true;
    }

     /**
    * This function is for cms goods_attr_by_cat page, get goods list by category.
    * @param string $attr_id
    * @param array  $filter
    * @return array
    **/
    function get_cat_goods_list($cat_id, $filter)
    {
        if(empty($cat_id)) return false;

        $keyword    = $filter['keyword'];
        $sort_by    = $filter['sort_by'];
        $sort_order = $filter['sort_order'];
        $filter['start'] = $filter['start'] ? $filter['start'] : 0;
        $filter['page_size']  = $filter['page_size'] ? $filter['page_size'] : 10;
        if($sort_by == 'goods_name') $sort_by = 'goods_id';
        /* Get attribute list */
        $sql = "SELECT * FROM ".$this->ecs->table('attribute')." WHERE cat_id = $cat_id ";
        $attr_list = $this->db->getAll($sql);
        foreach ($attr_list as $key => $attr) {
            $attr_values = $this->attr_values_to_array($attr['attr_values']);
            $attr_list[$key]['attr_values'] = array_combine($attr_values, $attr_values);
        }
        $col_num = count($attr_list);

        $where = " WHERE g.cat_id = $cat_id  AND g.is_delete = 0 AND g.is_on_sale = 1 ";
        /* Keyword where */
        if (!empty($keyword))
        {
            $where .= " AND (g.goods_sn LIKE '%" . mysql_like_quote($keyword) . "%' ";
            $where .= " OR g.goods_name LIKE '%" . mysql_like_quote($keyword) . "%' ";
            $tmp = preg_split('/\s+/', $keyword);
            if (count($tmp) > 1)
            {
                $where .= " OR ( 1";
                foreach ($tmp as $kw)
                {
                    $where .= " AND g.goods_name LIKE '%" . mysql_like_quote($kw) . "%' ";
                }
                $where .= ") ";
            }
            $where .= " OR g.goods_id = '" . mysql_like_quote($keyword) . "')";
        }

        $count_sql = "SELECT COUNT(*) FROM " . $this->ecs->table('goods') . " AS g $where";
        $filter['record_count'] = $this->db->getOne($count_sql);

        /* 分页大小 */
        // $filter = page_and_size($filter);

        $sql = "SELECT GROUP_CONCAT(ga.attr_value separator '\\n') as attr_value, ".
                "GROUP_CONCAT(ga.goods_attr_id  separator '\\n') as goods_attr_id, ".
                "GROUP_CONCAT(ga.attr_id  separator '\\n') as attr_id, ".
                "g.goods_name, g.goods_id ".
                " FROM ".$this->ecs->table('goods')." as g ".
                " LEFT JOIN ".$this->ecs->table('attribute')." as a ON a.cat_id = $cat_id".
                " LEFT JOIN ".$this->ecs->table('goods_attr')." as ga ON ga.goods_id = g.goods_id ".
                " $where GROUP BY g.goods_id ";

        $sql .= " ORDER BY $sort_by $sort_order LIMIT " . $filter['start'] .", $filter[page_size]";

        $arr = $this->db->getAll($sql);
        $goods_list = array();
        foreach ($arr as $key => $value) {
            $goods_id   = $value['goods_id'];
            $attr_value = $this->attr_values_to_array($value['attr_value']);
            $attr_id    = $this->attr_values_to_array($value['attr_id']);
            $goods_attr = array();
            foreach ($attr_value as $k => $v) {
                $goods_attr[$attr_id[$k]] = $v;
            }

            $goods_list[$goods_id]['goods_name'] = $value['goods_name'];
            $goods_list[$goods_id]['goods_attr'] = $goods_attr;
            $goods_list[$goods_id]['goods_id']   = $goods_id;
        }
        $goods_list = array_values($goods_list);
        $attr_data['cat_name'] = $this->db->getOne("SELECT cat_name FROM ".$this->ecs->table('category')." WHERE cat_id = $cat_id LIMIT 1 ");
        $attr_data['attr_list']    = $attr_list;
        $attr_data['col_num']      = $col_num + 1; //add with goods_name col
        $attr_data['goods_list']   = $goods_list;
        $attr_data['record_count'] = $filter['record_count'];

        return $attr_data;
    }

    /**
    * This function is for cms goods_attr_by_cat page, batch update or insert goods_attr with multipe language for same category.
    * @param array $request         $_REQUEST
    * @return bool
    **/
    function update_goods_attr_by_cat($request)
    {
        /* Handle request value */
        $cat_id     = $request['cat_id'];
        $goods_attr = $request['goods_attr'];

        if(empty($cat_id) || empty($goods_attr)) return false;

        foreach ($goods_attr as $attr_id => $goods_list)
        {
            /* Remove empty goods */
            $goods_list = array_filter($goods_list);

            /* Get goods_id list group by attr_value */
            $attr_values = array();
            foreach ($goods_list as $goods_id => $attr_value)
            {
                $attr_values[$attr_value][] = $goods_id;
            }

            /* Update goods_attr by attr_value */
            foreach ($attr_values as $attr_value => $goods_ids)
            {
                $data = array(
                    'attr_id'    => $attr_id,
                    'goods_ids'  => $goods_ids,
                    'attr_value' => $attr_value
                );
                $res = $this->update_goods_attr_by_attr_value($data);
            }
        }

        return true;
    }

    /**
    * This function is remove goods_attr
    * @param array  $goods_ids
    * @param string $attr_id (default empty)
    * @return bool
    **/
    function remove_goods_attr_by_attr_value($goods_ids, $attr_id = "")
    {
        $sql = "DELETE ga.*, gal.* FROM ".$this->ecs->table('goods_attr')." AS ga ".
            " LEFT JOIN ".$this->ecs->table('goods_attr_lang')." AS gal ON ga.goods_attr_id = gal.goods_attr_id ".
            " WHERE ga.goods_id ".db_create_in($goods_ids);
        if($attr_id != ""){
            $sql .= " AND ga.attr_id = $attr_id";
        }

        return $this->db->query($sql);
    }

    /**
    * Attribute values list String to Array
    * @param string $attr_values
    * @return array
    */
    function attr_values_to_array($attr_values)
    {
        if(!is_string($attr_values)) return array();

        $attr_values = explode("\n", $attr_values);
        $attr_values = array_map('trim', $attr_values);

        return $attr_values;
    }

    function handle_attribute_index($attr_id)
    {
        /* Get attribute data */
        $attr = new Model\Attribute($attr_id);
        $data = $attr->data;
        if(empty($data)) return false;

        /* If attr_index is 1 : 關鍵字檢索, add attr_value into goods keywords */
        if($data['attr_index'] == 1 )
        {
            $sql = "SELECT goods_id, attr_value, goods_attr_id FROM ".$this->ecs->table('goods_attr')." WHERE attr_id = $attr_id ";
            $goods_list = $this->db->getAll($sql);

            foreach ($goods_list as $key => $value) {
                $goods_id      = $value['goods_id'];
                $attr_value    = $value['attr_value'];
                $goods_attr_id = $value['goods_attr_id'];

                $sql = "SELECT keywords FROM ".$this->ecs->table('goods')." WHERE goods_id = $goods_id ";
                $keywords =  $this->db->getOne($sql);

                /* Add attrbute in keywords */
                $keyword_list = explode(" ", $keywords);
                $keyword_list[] = $attr_value;
                $keyword_list = array_unique($keyword_list);

                $keywords = implode(" ", $keyword_list);
                $keywords = str_replace("'", "", $keywords);

                $sql = "update ".$this->ecs->table('goods')." SET keywords = '".$keywords."' WHERE goods_id = $goods_id ";
                $this->db->query($sql);

                /* Multiple language keywords handle */
                $sql = "SELECT lang, attr_value FROM ".$this->ecs->table('goods_attr_lang')." WHERE goods_attr_id = $goods_attr_id ";
                $langs =  $this->db->getAll($sql);

                if(empty($langs)) continue;

                $sql = "SELECT keywords, lang FROM ".$this->ecs->table('goods_lang')." WHERE goods_id = $goods_id ";
                $goods_langs =  $this->db->getAll($sql);

                if(empty($goods_langs)) continue;

                foreach ($goods_langs as $k => $goods_lang){
                    foreach($langs as $k2 => $lang){
                        if($lang['lang'] == $goods_lang['lang'])
                        {
                            /* Add attrbute in keywords */
                            $keyword_list = explode(" ", $goods_lang['keywords']);
                            $keyword_list[] = $attr_value;
                            $keyword_list = array_unique($keyword_list);

                            $keywords = implode(" ", $keyword_list);
                            $keywords = str_replace("'", "", $keywords);

                            $sql = "update ".$this->ecs->table('goods_lang')." SET keywords = '".$keywords."' WHERE goods_id = $goods_id and lang = '$goods_lang[lang]' ";
                            $this->db->query($sql);
                        }
                    }
                }
            }
        }
    }

    function ajaxEditAction()
    {
        $col   = $_REQUEST['col'];
		$value = $_REQUEST['value'];
		$key   = $_REQUEST['id'];
		$type  = $_REQUEST['type'];
        global $db, $ecs;
        $exc = new \exchange($ecs->table("attribute"), $db, 'attr_id', 'attr_name');
        if($_REQUEST['col'] == "attr_name") {
            /* 取得该属性所属商品类型id */
            $cat_id = $exc->get_name($key, 'cat_id');
            /* 检查名称是否重复 */
            if (!$exc->is_only('attr_name', $value, $key, " cat_id = '$cat_id'"))
            {
                make_json_error(_L('name_exist', ''));
            }
        }
        parent::ajaxEditAction();
    }
    function ajaxQueryAction($list, $record_count)
    {
        $type = $_REQUEST['type'];
        if ($type == 'goods_attr_by_attr') {
            $attr = $this->get_attr_goods_list($_REQUEST['attr_id'], $_REQUEST);
            $list = $attr['goods_list'];
        } else if($type == 'goods_attr_by_cat') {
            $attr = $this->get_cat_goods_list($_REQUEST['cat_id'], $_REQUEST);
            $list = $attr['goods_list'];
        }
        else {
            $attr = $this->get_attrlist();
            $list = $attr['item'];
            foreach($list as $key => $attr) {
                $custom_actions = ['batch_add_goods_attr' => [
                    'link'       => '?act=attr_goods_list&amp;attr_id='.$attr['attr_id'],
                    'icon_class' => 'fa fa-plus'
                ]];
                $action = $this->generateCrudActionBtn($attr['attr_id'], 'id', null, NULL, $custom_actions);
                $list[$key]['_action'] = $action;
            }
        }
        $record_count = $attr['record_count'];
        parent::ajaxQueryAction($list, $record_count, false);
    }

    /**
     * 获取属性列表
     *
     * @return  array
     */
    function get_attrlist()
    {
        /* 查询条件 */
        $_REQUEST['goods_type'] = empty($_REQUEST['goods_type']) ? 0 : intval($_REQUEST['goods_type']);
        $_REQUEST['sort_by']    = empty($_REQUEST['sort_by']) ? 'sort_order' : trim($_REQUEST['sort_by']);
        $_REQUEST['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);

        $where = (!empty($_REQUEST['goods_type'])) ? " WHERE a.cat_id = '$_REQUEST[goods_type]' " : '';

        $sql = "SELECT COUNT(*) FROM " . $GLOBALS['ecs']->table('attribute') . " AS a $where";
        $record_count = $GLOBALS['db']->getOne($sql);


        /* 查询 */
        $sql = "SELECT a.*, t.cat_name " .
                " FROM " . $GLOBALS['ecs']->table('attribute') . " AS a ".
                " LEFT JOIN " . $GLOBALS['ecs']->table('category') . " AS t ON a.cat_id = t.cat_id " . $where .
                " ORDER BY $_REQUEST[sort_by] $_REQUEST[sort_order] ".
                " LIMIT " . $_REQUEST['start'] .", $_REQUEST[page_size]";
        $row = $GLOBALS['db']->getAll($sql);

        foreach ($row AS $key => $val)
        {
            $row[$key]['attr_input_type_desc'] = $GLOBALS['_LANG']['value_attr_input_type'][$val['attr_input_type']];
            $row[$key]['attr_values']      = str_replace("\n", ", ", $val['attr_values']);
        }

        $arr = array('item' => $row, 'record_count' => $record_count);

        return $arr;
    }
}
