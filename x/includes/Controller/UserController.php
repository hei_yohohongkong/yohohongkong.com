<?php

namespace Yoho\cms\Controller;
use Yoho\cms\Model;
if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

class UserController extends YohoBaseController{
	private $tablename='users',$cacheKey=['users','wholesale','ids'];

	public function __construct(){
		parent::__construct();
		$this->tablename=$this->ecs->table($this->tablename);
		$this->flushWholesaleUserIds();
	}

	public function getWholesaleUserIds(){
		if($this->memcache->get($this->genCacheKey(['users','wholesale','ids']))!==false)
			return $this->memcache->get($this->genCacheKey($this->cacheKey));
		$q = "SELECT user_id FROM ".$this->tablename." WHERE is_wholesale=TRUE";
		$result = $this->db->getAll($q);
		$out=[];
		foreach ($result as $key => $value) {
			$out[]=$value['user_id'];
		}
		$this->memcache->set('s',$out);
		return $out;
	}

	public function getWholesaleUsers(){
		//TODO:: memcache
		$q = "SELECT user_id,user_name,email FROM ".$this->tablename." WHERE is_wholesale=TRUE";
		$result = $this->db->getAll($q);
		$out=[];
		foreach ($result as $key => $value) {
			$out[$value['user_id']]=$value['user_name']."\t".$value['email'];
		}
		return $out;
	}

	public function flushWholesaleUserIds(){

		if($this->memcache->get($this->genCacheKey($this->cacheKey))!==false)
		return $this->memcache->flush($this->genCacheKey($this->cacheKey));
		else return true;
	}

	/* Using for cms coupon/favourable to search user */
	public function findUserByType($type, $value, $user_list){
		$user_list = empty($user_list) ? '' : $user_list;
		$add_user_list = array();
		$sql = '';
		switch ($type) {
			case 'goods': // Search by order goods
				$goods_id = $value;
				if(!empty($goods_id)){
					$sql = "SELECT o.user_id FROM ".$GLOBALS['ecs']->table('order_goods')." as og ".
							" LEFT JOIN ".$GLOBALS['ecs']->table('order_info')." as o ON o.order_id = og.order_id ".
							" WHERE og.goods_id = ".$goods_id;
				}
				break;
			case 'birthday': // Search by users birthday month
				$month = $value;
				if(!empty($month)){
					$sql = "SELECT user_id FROM ".$GLOBALS['ecs']->table('users').
							" WHERE MONTH(birthday) = ".$month;
				}
				break;
			case 'rank': // Search by users rank_points
				$rank = $value;
				$sql = "SELECT user_id FROM ".$GLOBALS['ecs']->table('users').
						" WHERE rank_points >= ".(int)$rank;
				break;
			default:
				break;
		}
		if(!empty($sql))$add_user_list = $GLOBALS['db']->getCol($sql);
		$tmp_user_list = empty($user_list)? array() :  json_decode($user_list, true);
		$tmp_user_list = array_unique (array_merge($tmp_user_list, $add_user_list));
		// $user_list = json_encode($tmp_user_list);
		//json_encode can't handle max array
		$user_list = implode(",",$tmp_user_list);
		$user_list = "[".str_replace('"',"", $user_list)."]";

		return $user_list;
	}

	/* Get logged in user's address list */
	public function get_user_address_list($user_id){

		global $ecs, $db;

		$sql = "SELECT ua.*, " .
			"r0.`region_name` as country_name " .
			// "r1.`region_name` as province_name, " . // TODO: Is not using in this flow
			// "r2.`region_name` as city_name, " .     // TODO: Is not using in this flow
			// "r3.`region_name` as district_name " .  // TODO: Is not using in this flow
		"FROM " . $ecs->table('user_address') . " as ua " .
		"LEFT JOIN " . $ecs->table('region') . " as r0 ON r0.region_id = ua.country " .
		// "LEFT JOIN " . $ecs->table('region') . " as r1 ON r1.region_id = ua.province " .
		// "LEFT JOIN " . $ecs->table('region') . " as r2 ON r2.region_id = ua.city " .
		// "LEFT JOIN " . $ecs->table('region') . " as r3 ON r3.region_id = ua.district " .
		"WHERE user_id = '" . intval($user_id) . "' AND address_type != '".AddressController::ADDRESS_TYPE_LOCKER."' LIMIT 8";

		$res = $db->getAll($sql);

		foreach ($res as $key => $address) {
			$address['id']             = $address['address_id'];
			$address['name']           = $address['consignee'];
			$address['country_id']     = $address['country'];
			$address['phone']          = $address['tel'];
			$address['building']       = $address['sign_building'];
			$address['format_address'] = $address['address'];
			$address['country_name']   = _L('country_' . $address['country'], $address['country_name']);
			if ($address['id_doc'] != ""){
				$address['id_doc'] = decrypt_str($address['id_doc']);
				if (preg_match('/[^A-Za-z0-9]/', $address['id_doc'])){
					$address['id_doc'] = '';
				}
			}
			$res[$key] = $address;
		}

		return $res;
	}

	public function get_user_id($user_name, $email) {
		global $db, $ecs;
		$sql = "SELECT user_id FROM ". $ecs->table('users')."  WHERE user_name = '$user_name' OR email = '$email' LIMIT 1";
		$user_id = $db->getOne($sql);

		return $user_id;
	}

	function write_affiliate_log($oid, $uid, $username, $money, $point, $separate_by)
	{
		$time = gmtime();
		$sql = "INSERT INTO " . $GLOBALS['ecs']->table('affiliate_log') . "( order_id, user_id, user_name, time, money, point, separate_type)".
																" VALUES ( '$oid', '$uid', '$username', '$time', '$money', '$point', $separate_by)";
		if ($oid)
		{
			$GLOBALS['db']->query($sql);
		}
	}

	public function get_user_name($user_id)
	{
		global $db, $ecs;

		$sql = "select user_name from ".$ecs->table('admin_user')." where user_id = ".$user_id." ";
		return $db->getOne($sql);
	}

	public function get_user_info($user_id)
	{
		global $db, $ecs;
		$sql = "SELECT u.user_id, u.sex, u.email, u.birthday, u.pay_points, u.rank_points, u.user_rank , u.user_money, u.frozen_money, u.credit_line, u.parent_id, u.user_name, u2.user_name as parent_username, u.qq, u.msn,
		u.office_phone, u.home_phone, u.mobile_phone, u.is_wholesale ".
			" FROM " .$ecs->table('users'). " u LEFT JOIN " . $ecs->table('users') . " u2 ON u.parent_id = u2.user_id WHERE u.user_id='$user_id'";

		$row = $db->GetRow($sql);

		if ($row)
		{
			$user['user_id']        = $row['user_id'];
			$user['sex']            = $row['sex'];
			$user['birthday']       = date($row['birthday']);
			$user['pay_points']     = $row['pay_points'];
			$user['email']    = $row['email'];
			$user['rank_points']    = $row['rank_points'];
			$user['user_rank']      = $this->get_user_rank($row['user_rank'], $row['rank_points']);
			$user['user_money']     = $row['user_money'];
			$user['user_name']      = $row['user_name'];
			$user['frozen_money']   = $row['frozen_money'];
			$user['credit_line']    = $row['credit_line'];
			$user['formated_user_money'] = price_format($row['user_money']);
			$user['formated_frozen_money'] = price_format($row['frozen_money']);
			$user['parent_id']      = $row['parent_id'];
			$user['parent_username']= $row['parent_username'];
			$user['qq']             = $row['qq'];
			$user['msn']            = $row['msn'];
			$user['office_phone']   = $row['office_phone'];
			$user['home_phone']     = $row['home_phone'];
			$user['mobile_phone']   = $row['mobile_phone'];
			$user['is_wholesale']   = $row['is_wholesale'];
			if($user['birthday'] == '0000-00-00') {
				$date = local_strtotime("-30 year");
				$user['birthday'] = date("Y-m-d", $date);
			}
		}
		else
		{
			$user['sex']            = 0;
			$user['pay_points']     = 0;
			$user['rank_points']    = 0;
			$user['user_money']     = 0;
			$user['frozen_money']   = 0;
			$user['credit_line']    = 0;
			$user['formated_user_money'] = price_format(0);
			$user['formated_frozen_money'] = price_format(0);
		}

		/* 取出注册扩展字段 */
		$sql = 'SELECT * FROM ' . $ecs->table('reg_fields') . ' WHERE type < 2 AND display = 1 AND id != 6 ORDER BY dis_order, id';
		$extend_info_list = $db->getAll($sql);

		$sql = 'SELECT reg_field_id, content ' .
			'FROM ' . $ecs->table('reg_extend_info') .
			" WHERE user_id = $user[user_id]";
		$extend_info_arr = $db->getAll($sql);

		$temp_arr = array();
		foreach ($extend_info_arr AS $val)
		{
			$temp_arr[$val['reg_field_id']] = $val['content'];
		}

		foreach ($extend_info_list AS $key => $val)
		{
			switch ($val['id'])
			{
				case 1:     $extend_info_list[$key]['content'] = $user['msn']; break;
				case 2:     $extend_info_list[$key]['content'] = $user['qq']; break;
				case 3:     $extend_info_list[$key]['content'] = $user['office_phone']; break;
				case 4:     $extend_info_list[$key]['content'] = $user['home_phone']; break;
				case 5:     $extend_info_list[$key]['content'] = $user['mobile_phone']; break;
				case 10:    $extend_info_list[$key]['content'] = empty($temp_arr[$val['id']]) ? '' : $temp_arr[$val['id']]; $user['display_name'] = $extend_info_list[$key]['content']; break;
				default:    $extend_info_list[$key]['content'] = empty($temp_arr[$val['id']]) ? '' : $temp_arr[$val['id']] ;
			}
		}
		return $user;
	}

	public function is_user_sms_verified($user_id)
	{
		global $db, $ecs;
		//$sql = "select is_sms_validated from ".$ecs->table('users') ." where user_id = '".$user_id."' ";
		// only for HK
		$sql = "select is_sms_validated from ".$ecs->table('users') ." where user_id = '".$user_id."' and user_name like '+852-%' ";
		$result = $db->getOne($sql);

		return $result;
	}

	function get_user_rankname($user_rank, $rank_points)
	{
		$all_ranks = $this->get_all_user_ranks();

		if ($user_rank > 0)
		{
			foreach ($all_ranks as $rank)
			{
				if ($rank['rank_id'] == $user_rank)
				{
					return $rank['rank_name'];
				}
			}
		}
		else
		{
			$max_rank = '';
			$max_mp = 0;
			foreach ($all_ranks as $rank)
			{
				if (($rank['min_points'] <= $rank_points) && ($rank['min_points'] >= $max_mp))
				{
					$max_rank = $rank['rank_name'];
					$max_mp = $rank['min_points'];
				}
			}
			if (!empty($max_rank))
			{
				return $max_rank;
			}
		}

		return $GLOBALS['_LANG']['undifine_rank'];
	}

	function get_user_rank($user_rank, $rank_points)
	{
		$all_ranks = $this->get_all_user_ranks();

		if ($user_rank > 0)
		{
			foreach ($all_ranks as $rank)
			{
				if ($rank['rank_id'] == $user_rank)
				{
					return $rank['rank_id'];
				}
			}
		}
		else
		{
			$max_rank = '';
			$max_mp = 0;
			foreach ($all_ranks as $rank)
			{
				if (($rank['min_points'] <= $rank_points) && ($rank['min_points'] >= $max_mp))
				{
					$max_rank = $rank['rank_id'];
					$max_mp = $rank['min_points'];
				}
			}
			if (!empty($max_rank))
			{
				return $max_rank;
			}
		}

		return 0;
	}

	function get_all_user_ranks()
	{
		static $all_ranks = null;
		if ($all_ranks === null)
		{
			$sql = "SELECT rank_id, rank_name, min_points " .
				" FROM " . $GLOBALS['ecs']->table('user_rank') .
				" ORDER BY min_points ASC";
			$all_ranks = $GLOBALS['db']->getAll($sql);
			if ($all_ranks === false) $all_ranks = array();
		}
		return $all_ranks;
	}


	/**
	 * @param int $user_id
	 * @param int $id
	 * @return array|bool
	 */
	public function set_user_address_list($user_id = 0, $id = 0)
	{
		global $ecs, $db;

		// 取得地址
		$sql = "SELECT ua.* " .
			"FROM " . $ecs->table('user_address') . " as ua " .
			"WHERE ua.address_id = '$id'";
		$consignee = $db->getRow($sql);
		// 檢查地址是否屬於該用戶
		if ($consignee['user_id'] != $user_id) {
			return false;
		}
		// 設定為用戶的預設收貨地址
		$sql = "UPDATE " . $ecs->table('users') .
			" SET address_id = '$id' WHERE user_id = '$user_id'";
		$res = $db->query($sql);
		// 保存到 session
		$_SESSION['flow_consignee'] = $consignee;

		return true;
	}

	/**
	 * @param int $user_id
	 * @param int $id
	 * @return bool
	 */
	public function isFraudUser($user_id = 0)
	{
		global $ecs, $db;

		return $db->getOne("SELECT 1 FROM " . $ecs->table('users') . " WHERE user_id = '$user_id' AND is_fraud != 0");
	}

	public function getPosRegEmailRate($date)
	{
		global $ecs, $db;
		include_once(ROOT_PATH . 'includes/lib_order.php');
		$where_u = '';
		if(!empty($date['start_date'])){
			$where_u .= " AND u.reg_time >= " . local_strtotime($date['start_date']);
		}
		if(!empty($date['end_date'])){
			$where_u .= " AND u.reg_time < " . (local_strtotime($date['end_date'].' 23:59:59'));
		}
		//門市註冊人數
		$sa_sql = 'SELECT u2.user_id FROM '.$ecs->table('order_info') . ' oi2 '.
			'LEFT JOIN '.$ecs->table('users') . ' u2 ON u2.user_id = oi2.user_id '.
			'WHERE oi2.order_id IN (SELECT MIN(order_id) FROM '.$ecs->table('order_info') . ' GROUP BY user_id) ' .
			'AND oi2.order_type = "'.OrderController::DB_ORDER_TYPE_SALESAGENT.'" AND oi2.add_time - u2.reg_time < 120' . str_replace('u.','u2.',$where_u);
		$sql = 'SELECT COUNT(DISTINCT oi.user_id) as user_num FROM '.$ecs->table('order_info') . ' oi '.
			'LEFT JOIN '.$ecs->table('users') . ' u ON u.user_id = oi.user_id '.
			'WHERE (u.reg_time = oi.add_time OR u.email REGEXP "^\\\+?[0-9-]+(@yohohongkong.com)$") AND oi.platform = "pos" ' .
			' AND u.user_id NOT IN (' . $sa_sql . ') ' . $where_u .where_exclude('oi.',true);
		$pos_register = $db->getOne($sql);
		$sql = 'SELECT COUNT(DISTINCT oi.user_id) as user_num FROM '.$ecs->table('order_info') . ' oi '.
			'LEFT JOIN '.$ecs->table('users') . ' u ON u.user_id = oi.user_id '.
			'WHERE (u.reg_time = oi.add_time AND u.email REGEXP "^\\\+?[0-9-]+(@yohohongkong.com)$") AND oi.platform = "pos"' .
			' AND u.user_id NOT IN (' . $sa_sql . ') ' . $where_u .where_exclude('oi.',true);
		$pos_register_no_email = $db->getOne($sql);

		// cal rate
		$no_mail_rate = round($pos_register_no_email / $pos_register, 2) * 100 .'%';
		$mail_rate    = round(($pos_register - $pos_register_no_email) / $pos_register, 2) * 100 .'%';
		return [
			'mail' => ($pos_register - $pos_register_no_email),
			'no_mail' => $pos_register_no_email,
			'mail_rate' => $mail_rate,
			'no_mail_rate' => $no_mail_rate,
			'pos_register' => $pos_register
		];
	}
}