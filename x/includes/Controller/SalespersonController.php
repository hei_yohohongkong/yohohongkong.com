<?php

/***
* ctrl_salesperson.php
* by MichaelHui 20170328
*
* Salesperson system - the controller
***/
namespace Yoho\cms\Controller;
use Yoho\cms\Model;
if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

class SalespersonController extends YohoBaseController{
	private $tablename='salesperson';

	public function __construct(){
		parent::__construct();
		$this->tablename=$GLOBALS['ecs']->table($this->tablename);
	}

	public function getAll($forOutput=false,$sort='sales_id',$order='ASC'){
		$res = $this->db->query('SELECT a.*,s.* FROM '.$this->tablename." s LEFT JOIN ".Adminuser::getTableName()." a ON a.user_id=s.admin_id ORDER BY ".$sort." ".$order);
		$output=[];
		while($r = mysql_fetch_assoc($res)){
			if($forOutput==true){
				$r['avatar'] = ($r['avatar']!=''?FileController::getImgUrl($r['avatar'],true):'');
			}
			$output[]=$r;
		}

		return $output;
	}

	public function getByAdmin($adminId){
		$res = $this->db->query("SELECT * FROM ".$this->tablename." WHERE admin_id<>0 AND admin_id IS NOT NULL AND admin_id=".$adminId);

		if(mysql_num_rows($res)>0){
			$res = mysql_fetch_assoc($res);
			return new \Yoho\cms\Model\Salesperson($res['sales_id']);
		}else{
			return false;
		}
	}

	public function createFromAdmin($u){
		admin_priv('admin_manage');
		if($u->getId()>0){
			
			$dedup = $this->db->getAll("SELECT * FROM ".$this->tablename." WHERE sales_name='".$u->data['user_name']."'");
			if(count($dedup)>0)
				return false;

			$params = [
				'sales_name'=>"'".$u->data['user_name']."'",
				'display_name'=>"'".$u->data['user_name']."'",
				'user_id'=>0,
				'admin_id'=>$u->getId(),
				'intro'=>"''",
				'available'=>TRUE,
				'allow_pos'=>TRUE
			];
			$q = "INSERT INTO ".$this->tablename." (".implode(',', array_keys($params)).") VALUE (".implode(',', $params).")";
			if($this->db->query($q)){
				admin_log($u->getId().'-'.$u->data['user_name'],'salesperson_action','create_from_admin');
				return $this->db->Insert_ID();
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	public function archive($salesId){
		if(intval($salesId)<=0)
			return false;
		admin_priv('admin_manage');
		admin_log('ID:'.$salesId,'salesperson_status','archive');
		$q="UPDATE ".$this->tablename." SET available=0, admin_id=0 WHERE sales_id=".$salesId;
		return $this->db->query($q);
	}

	public function deactivate($salesId){
		admin_priv('admin_manage');
		
		if(intval($salesId)>0 && $this->db->query("UPDATE ".$this->tablename." SET available=0 WHERE sales_id=".$salesId)){
			admin_log('ID:'.$salesId,'salesperson_status','deactivate');
			return true;
		}else{
			return false;
		}
	}


	public function activate($salesId){
		admin_priv('admin_manage');
		
		if(intval($salesId)>0 && $this->db->query("UPDATE ".$this->tablename." SET available=1 WHERE admin_id<>0 AND admin_id IS NOT NULL AND sales_id=".$salesId)){
			admin_log('ID'.$salesId,'salesperson_status','activate');
			return true;
		}else{
			return false;
		}
	}



}