<?php

/***
* LuckDrawController.php
*
*
*
***/
namespace Yoho\cms\Controller;

use Yoho\cms\Model;

use \Exception;

use Yoho\cms\Controller\ErpController;

if (!defined('IN_ECS')) {
    die('Hacking attempt');
}
class LuckyDrawController extends YohoBaseController
{
    const GIFT_TYPE_POINT = 0;
    const GIFT_TYPE_COUPON = 1;
    const GIFT_TYPE_PRODUCT = 2;

    const TYPE_ARCHIVED = 0;
    const TYPE_ACTIVE = 1;
    // const TYPE_DELETED = 2;

    private static $typeValues=['已封存','未封存'];
    
    private static $giftTypeValues=['積分','優惠券','免費禮物'];

    private static $ticketOpenValues=['未抽獎','己抽獎'];
 
    private $tablename='erp_goods_stocktake';
    
    public function __construct()
    {
        parent::__construct();
       
        $this->adminuserController = new AdminuserController();
        $this->erpController =  new ErpController();
        $this->userController =  new UserController();
    }

    public function getStatusOptions()
    {
        return self::$statusValues;
    }

    public function getGiftTypeOptions()
    {
        return self::$giftTypeValues;
    }

    public function getTicketOpenOptions()
    {
        return self::$ticketOpenValues;
    }

    public function checkWithSet(array $set, $length=10000)
    {
        $left = 0;
        foreach ($set as $num=>$right) {
            $set[$num] = $left + $right*$length;
            $left = $set[$num];
        }
        $test = mt_rand(1, $length);
        $left = 1;
        foreach ($set as $num=>$right) {
            if ($test>=$left && $test<=$right) {
                return $num;
            }
            $left = $right;
        }
        return null;//debug, no event realized
    }

    public function getProbabilityRate()
    {
        global $db, $ecs ,$_CFG;
        
        // get rate
        $sql = "select * from ".$ecs->table('lucky_draw_levels')." ";
        $rates = $db->getAll($sql);

        $ar = [];
        foreach ($rates as $rate) {
            $ar[$rate['level_id']] = $rate['probability'];
        }

        return $ar;
    }

    public function findLevelByProbability()
    {
        global $db, $ecs ,$_CFG;

        // get rate
        $rates = $this->getProbabilityRate();

        // get level 1 rate
        $length = round(100/$rates[1]);
        if (empty($length)) {
            $length = 30000;
        }

        $left = 0;
        foreach ($rates as $num=>$right) {
            $rates[$num] = $left + round($right*$length/100);
            $left = $rates[$num];
        }

        $rand_num = mt_rand(1, $length);

        $left = 1;
        foreach ($rates as $num=>$right) {
            // echo $left . '-' . $right;
            // echo '<br>';
            if ($rand_num == 1) {
                return $num;
            }

            if ($rand_num > $left && $rand_num<=$right) {
                // echo $num;
                // echo '<br>';
                // echo $rand_num;
                // exit;
                return $num;
            }
            $left = $right;
        }
     
        return null;//debug, no event realized

        //echo $number = rand(1,10000);
        //echo '<br>';
        // if ($number == 1) { // 0.01%
        //     return $level = 1;
        // } else if ($number >= 2 && $number <= 100) { // 0.99%
        //     return $level = 2;
        // } else if ($number >= 101 && $number <= 500) { // 4%
        //     return $level = 3;
        // } else if ($number >= 501 && $number <= 1500) { // 10%
        //     return $level = 4;
        // } else if ($number >= 1501 && $number <= 3000) { // 15%
        //     return $level = 5;
        // } else if ($number >= 3001 && $number <= 5000) { // 20%
        //     return $level = 6;
        // } else if ($number >= 5001 && $number <= 10000) { // 50%
        //     return $level = 7;
        // }

        // $number = rand(1,100000);
        // //echo '<br>';
        // if ($number == 1) { // 0.001%
        //     return $level = 1;
        // } else if ($number >= 2 && $number <= 1000) { // 0.99%
        //     return $level = 2;
        // } else if ($number >= 1001 && $number <= 5000) { // 4%
        //     return $level = 3;
        // } else if ($number >= 5001 && $number <= 15000) { // 10%
        //     return $level = 4;
        // } else if ($number >= 15001 && $number <= 30000) { // 15%
        //     return $level = 5;
        // } else if ($number >= 30001 && $number <= 50000) { // 20%
        //     return $level = 6;
        // } else if ($number >= 50001 && $number <= 100000) { // 50%
        //     return $level = 7;
        // }
    }

    public function checkGiftEnough($lucky_draw_id)
    {
        global $db, $ecs ,$_CFG;

        $sql = "select count(*) from ".$ecs->table('lucky_draw_gifts')." where lucky_draw_id = '".$lucky_draw_id."' and status = 1 and probability_level is not null and qty > 0 ";
        $count = $db->getOne($sql);
        
        if ($count >= 8) {
            return true;
        } else {
            return false;
        }
    }

    public function isHoldLuckyDraw($lucky_draw_id)
    {
        global $db, $ecs ,$_CFG;

        $sql = "select count(*) from ".$ecs->table('lucky_draw')." where hold_draw_status = 1 and lucky_draw_id = '".$lucky_draw_id."' ";
        $count = $db->getOne($sql);
        
        if ($count > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function luckyDrawOrderAwards($order_id)
    {
        global $db, $ecs ,$_CFG;

        require_once(ROOT_PATH . 'includes/lib_order.php');
        // check lucky draw if it is active
        $lucky_draw_active = $this->getActiveLuckyDrawEvent();
        
        if (empty($lucky_draw_active)) {
            return true;
        }

        $lucky_draw_id = $lucky_draw_active[0];
        $lucky_draw = $this->getLuckyDrawEventInfo($lucky_draw_id);
        $order = order_info($order_id);

        $whole_sale_user_ids = $this->userController->getWholesaleUserIds();

        if (empty($whole_sale_user_ids)) {
            $whole_sale_user_ids = [];
        }

        if ($order['order_type'] != 'ws' && $order['order_type'] != 'sa' && !in_array($order['user_id'],$whole_sale_user_ids) && $order['replace_order_id'] == 0) {
            if ($lucky_draw_id > 0 && $lucky_draw['purchase_amount'] > 0) {
                if ($lucky_draw['purchase_amount'] <= $order['goods_amount'] && $order['pay_status'] == 2) { // 2 : paid
                    $remark = '產品金額-滿'.$lucky_draw['purchase_amount'];
                    $this->luckyDrawTicketCreate($lucky_draw_id, $order['user_id'], $remark, $order_id);
                }
            }
        }
    }

    public function bulkTicketFreeGiftNotify()
    {
        global $db, $ecs ,$_CFG;

        $total = 0;
        $sent = 0;
        if (!empty($_REQUEST['selected_ticket_ids'])) {
            foreach ($_REQUEST['selected_ticket_ids'] as $ticket_id) {
                $total++;
                $sent += $this->sendLuckyDrawFreeGiftPickupNotice($ticket_id) ? 1 : 0;
            }
        }

        $arr = [
            'total' => $total,
            'sent' => $sent,
        ];

        return $arr;
    }

    public function updateUserInfo($user_id, $email, $first_name, $last_name)
    {
        global $db, $ecs ,$_CFG;

        if (!empty($email)) {
            $sql = "update ".$ecs->table('users')." set email = '".$email."' where user_id = ".$user_id." ";
            $db->query($sql);
        }
        
        if (!empty($first_name)) {
            $sql = "select count(*) from " . $ecs->table('reg_extend_info'). " where user_id = ".$user_id." and reg_field_id = 8 ";
            $count = $db->getOne($sql);

            if ($count > 0) {
               $sql = "update ".$ecs->table('reg_extend_info')." set content = '".$first_name."' where user_id = ".$user_id." and reg_field_id = 8 ";
                $db->query($sql);
            } else {
                $sql = "insert into ".$ecs->table('reg_extend_info')." (user_id,reg_field_id,content) value (".$user_id.",8,'".$first_name."') ";
                $db->query($sql);
            }
        }

        if (!empty($last_name)) {
            $sql = "select count(*) from " . $ecs->table('reg_extend_info'). " where user_id = ".$user_id." and reg_field_id = 9 ";
            $count = $db->getOne($sql);

            if ($count > 0) {
                $sql = "update ".$ecs->table('reg_extend_info')." set content = '".$last_name."' where user_id = ".$user_id." and reg_field_id = 9 ";
                $db->query($sql);
            } else {
                $sql = "insert into ".$ecs->table('reg_extend_info')." (user_id,reg_field_id,content) value (".$user_id.",9,'".$last_name."') ";
                $db->query($sql);
            }
        }

        return true;
    }

    public function findGiftInLevel($level, $lucky_draw_id)
    {
        global $db, $ecs ,$_CFG;

        $sql = "select gift_id from ".$ecs->table('lucky_draw_gifts')." where lucky_draw_id = ".$lucky_draw_id." and probability_level = ".$level." and qty > 0 order by rand() ";
        $gift_id = $db->getOne($sql);
        
        return $gift_id;
    }

    public function randomFindOtherGifts($lucky_draw_id, $result, $qty)
    {
        global $db, $ecs ,$_CFG;
        
        $sql = "select gift_id from ".$ecs->table('lucky_draw_gifts')." where lucky_draw_id = ".$lucky_draw_id." and qty > 0 and probability_level is not null and gift_id not in (".implode(',', $result).") order by rand() limit ".$qty." ";
        return $gift_ids = $db->getCol($sql);
    }

    public function findMustShowItems($lucky_draw_id, $exclude_gift_id, $qty = 7)
    {
        global $db, $ecs ,$_CFG;

        $sql = "select gift_id from ".$ecs->table('lucky_draw_gifts')." where lucky_draw_id = ".$lucky_draw_id." and qty > 0  and probability_level is not null and must_show = 1 and gift_id != ".$exclude_gift_id."  order by rand() limit ".$qty." ";
        return $db->getCol($sql);
    }

    public function assign_user_to_coupon($coupon_code, $user_id)
    {
        global $db, $ecs ,$_CFG;
        
        $sql = "select coupon_id, users_list from ".$ecs->table('coupons')." where coupon_code = '".$coupon_code."' ";
        $coupon = $db->getRow($sql);
        if (!empty($coupon['users_list'])) {
            $coupon['users_list'] = json_decode($coupon['users_list'], true);
        }
        //print_R($coupon);
        $coupon['users_list'][] = (int)$user_id;
        //print_R($coupon);
        $coupon_json = json_encode($coupon['users_list']);

        $sql = 'update '.$ecs->table('coupons').' set users_list = "'.$coupon_json.'" where coupon_id = "'.$coupon['coupon_id'].'" ';
        $db->query($sql);
    }

    public function giftDeleteSubmit()
    {
        global $db, $ecs, $_CFG;

        // check is using
        $sql = "select count(*) from ".$ecs->table('lucky_draw_ticket')." where gift_id = ".$_REQUEST['gift_id']." ";
        $gift_count = $db->getOne($sql);
        
        if ($gift_count > 0) {
            $result['error'] = '這獎品已經有人抽中,不能删除';
            return $result;
        } else {
            $sql = "delete from ".$ecs->table('lucky_draw_gifts')." where gift_id = ".$_REQUEST['gift_id']."  ";
            $db->query($sql);
        }
        
        return true;
    }

    public function luckyDrawTicketCreate($lucky_draw_id, $user_id, $remark, $order_id = 'null')
    {
        global $db, $ecs, $_CFG;

        if ($order_id != 'null') {
            // check ticket exist for the order
            $sql = "select count(*) from ".$ecs->table('lucky_draw_ticket')." where for_order_id = ".$order_id." ";
            $count = $db->getOne($sql);

            $created_by = 1 ;

            if ($count > 0) {
                return true;
            }
        }else{
            $created_by = erp_get_admin_id();
        }

        $sql = "insert into ".$ecs->table('lucky_draw_ticket')." (lucky_draw_id,user_id,for_order_id,remark,created_by,add_date) 
           values ('".$lucky_draw_id."',".$user_id.",".$order_id.",'".$remark."','".$created_by."','".gmtime()."') ";
        $db->query($sql);

        return true;
    }

    public function addTicketForVipSubmit()
    {
        global $db, $ecs, $_CFG;
 
        $lucky_draw_id = $_REQUEST['lucky_draw_id'];
        $sql = "SELECT min_points FROM " . $ecs->table('user_rank') . " WHERE rank_id = 2";
        $min_points = intval($db->getOne($sql));

        $sql = "SELECT count(*) FROM " . $ecs->table('users') . " WHERE rank_points >= ".$min_points." " ;
        
        $users_total = $db->getOne($sql);

        for ($i = 0 ; $i * 5000 < $users_total ; $i++ ) {
            $sql = "SELECT email, rank_points, user_id FROM " . $ecs->table('users') . " WHERE rank_points >= ".$min_points." limit  ".($i*5000)." , 5000 " ;
            $data = $db->getAll($sql);

            $ticket_sql = "select user_id from ".$ecs->table('lucky_draw_ticket') ." where lucky_draw_id = ".$lucky_draw_id." and remark = 'VIP' ";
            $ticket_data = $db->getCol($ticket_sql);

            $ticket_user_ar = [];
            foreach ($ticket_data as $user) {
                $ticket_user[$user] = 1;
            }

            $ticket_count = 0;
            foreach ($data as $row) {
                if (!isset($ticket_user[$row['user_id']])) {
                    // add a ticket for them
                    $remark = 'VIP';
                    $this->luckyDrawTicketCreate($_REQUEST['lucky_draw_id'], $row['user_id'], $remark);
                    $ticket_count++;
                }
            }
        }
        
        

        return $ticket_count;
    }

    public function checkUserTicketTotal($lucky_draw_id, $user_id, $active = true)
    {
        global $db, $ecs, $_CFG;

        $sql = "SELECT count(*) FROM " . $ecs->table('lucky_draw_ticket') . " WHERE lucky_draw_id = ".$lucky_draw_id." ". ($active ? " and opened = 0 " : "")." and user_id = ".$user_id." " ;
        return $count = $db->getOne($sql);
    }

    public function addTicketForUserSubmit()
    {
        global $db, $ecs, $_CFG;

        $lucky_draw_id = $_REQUEST['lucky_draw_id'];
        $user_id = $_REQUEST['user_id'];

        $sql = "SELECT user_id FROM " . $ecs->table('users') . " WHERE  user_name = '".$user_id."' " ;
        $user_id = $db->getOne($sql);
        
        if ($user_id > 0) {
            // find user id 
            $remark = '指定客戶額外獎賞';
            $this->luckyDrawTicketCreate($lucky_draw_id, $user_id, $remark);
            // check user total ticket
            return $this->checkUserTicketTotal($lucky_draw_id, $user_id);
        } else {
            $result['error'] = '找不到會員['.$user_id.']';
            return $result;
        }
    }

    public function ticketFollowUpRemarkSubmit()
    {
        global $db, $ecs ,$_CFG;

        if (!empty($_REQUEST['ticket_id'])) {
            $follow_up_remark = mysql_real_escape_string($_REQUEST['follow_up_remark']);
            $sql = 'update '.$ecs->table('lucky_draw_ticket').' set follow_up_remark = "'.$follow_up_remark.'" where ticket_id = "'.$_REQUEST['ticket_id'].'" ';
            $db->query($sql);
            return true;
        }
    }

    public function confirmPickup()
    {
        global $db, $ecs ,$_CFG;

        $sql = 'update '.  $ecs->table('lucky_draw_ticket') .' set pickup_time = "'.gmtime().'" where ticket_id = "'.$_REQUEST['ticket_id'].'" ';
        $db->query($sql);
        
        return true;
    }

    public function getLuckyDrawTicketList($lucky_draw_id, $is_pagination = true)
    {
        global $db, $ecs ,$_CFG;
        
        $_REQUEST['type'] = !isset($_REQUEST['type']) ? '' : $_REQUEST['type'];
        $_REQUEST['opened'] = !isset($_REQUEST['opened']) ? '1' : $_REQUEST['opened'];
        $_REQUEST['user_name'] = !isset($_REQUEST['user_name']) ? '' : $_REQUEST['user_name'];
        $_REQUEST['show_not_notify'] = !isset($_REQUEST['show_not_notify']) ? '' : $_REQUEST['show_not_notify'];
    
        // total record
        $sql = "SELECT count(*) FROM ".$ecs->table('lucky_draw_ticket') .' ldt ';
        $sql .= " left join ".$ecs->table('lucky_draw_gifts')." ldg on (ldt.gift_id = ldg.gift_id) ";
        $sql .= " left join ".$ecs->table('users')." u on (ldt.user_id = u.user_id) ";
        $sql .= " left join ".$ecs->table('lucky_draw_goods_redeem')." dgr on (ldt.ticket_id = dgr.ticket_id) ";
        
        $sql .= ' where ldt.lucky_draw_id = '.$lucky_draw_id.'';
        $sql .= (isset($_REQUEST['type']) && $_REQUEST['type'] != '') ? " and type = '".$_REQUEST['type']."'" : '' ;
        $sql .= (isset($_REQUEST['opened']) && $_REQUEST['opened'] != '') ? " and opened = '".$_REQUEST['opened']."'" : '' ;
        $sql .= (isset($_REQUEST['user_name']) && $_REQUEST['user_name'] != '') ? " and user_name like '%".$_REQUEST['user_name']."%'" : '' ;
        $sql .= (isset($_REQUEST['show_not_notify']) && $_REQUEST['show_not_notify'] == true) ? " and (notified_time is null or notified_time = 0) and ldg.type = '".self::GIFT_TYPE_PRODUCT."'  " : '' ;
        
        $total_count = $db->getOne($sql);
        
        $_REQUEST['record_count'] = $total_count;
        $_REQUEST['sort_by']    = empty($_REQUEST['sort_by']) ? 'type' : trim($_REQUEST['sort_by']);
        $_REQUEST['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
        
        // pagination
        $sql = "select ldt.*, ldg.*, u.user_name, dgr.goods_id,dgr.order_id,dgr.notified_time,oi.order_sn as for_order_sn, oi2.order_sn, do.delivery_status from ".$ecs->table('lucky_draw_ticket')." ldt ";
        $sql .= " left join ".$ecs->table('lucky_draw_gifts')." ldg on (ldt.gift_id = ldg.gift_id) ";
        $sql .= " left join ".$ecs->table('users')." u on (ldt.user_id = u.user_id) ";
        $sql .= " left join ".$ecs->table('lucky_draw_goods_redeem')." dgr on (ldt.ticket_id = dgr.ticket_id) ";
        $sql .= " left join (SELECT order_id, order_sn FROM ".$ecs->table('order_info').") oi on (ldt.for_order_id = oi.order_id) ";
        $sql .= " left join (SELECT order_id, order_sn FROM ".$ecs->table('order_info').") oi2 on (dgr.order_id = oi2.order_id) ";
        $sql .= " left join (SELECT status as delivery_status, order_id FROM ".$ecs->table('delivery_order').") do on (do.order_id = oi2.order_id) ";

        $sql .= " where ldt.lucky_draw_id = ".$lucky_draw_id." ";
        $sql .= (isset($_REQUEST['type']) && $_REQUEST['type'] != '') ? " and type = '".$_REQUEST['type']."'" : '' ;
        $sql .= (isset($_REQUEST['opened']) && $_REQUEST['opened'] != '') ? " and opened = '".$_REQUEST['opened']."'" : '' ;
        $sql .= (isset($_REQUEST['user_name']) && $_REQUEST['user_name'] != '') ? " and user_name like '%".$_REQUEST['user_name']."%'" : '' ;
        $sql .= (isset($_REQUEST['show_not_notify']) && $_REQUEST['show_not_notify'] == true) ? " and (notified_time is null or notified_time = 0) and ldg.type = '".self::GIFT_TYPE_PRODUCT."' " : '' ;


        $sql .= ' order by '. $_REQUEST['sort_by'] . ' ' .  $_REQUEST['sort_order'];
        $sql .= ($is_pagination ? " LIMIT " . $_REQUEST['start'] . ", " . $_REQUEST['page_size'] : '');
        //exit;
       
        $res = $db->getAll($sql);

        foreach ($res as &$ticket) {
            $ticket['add_date'] = local_date($_CFG['time_format'], $ticket['add_date']);
            $ticket['status'] = $ticket['opened']? '已抽獎':'未抽獎';

            // get user name
            $user_info = $this->getUserInfoConfirm($ticket['user_id'] );

            if (!empty($ticket['for_order_id'])) {
                $ticket['remark'] = $ticket['remark'] .' (<a target="_blank" href="order.php?act=info&amp;order_id='.$ticket['for_order_id'].'">'. $ticket['for_order_sn'].'</a>)';
            }

            if ($ticket['type']==2) {
                // if (!empty($ticket['delivered_time'])) {
                //     $ticket['pickup_status'] = local_date($_CFG['time_format'], $ticket['delivered_time']);
                // } else {
                //     $ticket['pickup_status'] = '未取貨';
                // }
                $ticket['pickup_status'] = '';
                
                if (empty($ticket['order_id'])) {
                    $ticket['notified_status'] = '<span class="red">未開單</span>';
                } else {
                    if (!empty($ticket['notified_time'])) {
                        $ticket['notified_status'] = '<span title="' . local_date($_CFG['time_format'], $ticket['notified_time']) . '">已通知</span>';
                    } else {
                        $ticket['notified_status'] = '<span class="green">未通知</span>';
                    }
                    $ticket['notified_status'] .= ' (<a target="_blank" href="order.php?act=info&amp;order_id='.$ticket['order_id'].'">'. $ticket['order_sn'].'</a>)';
                    $ticket['pickup_status'] = $ticket['delivery_status'] == DO_RECEIVED ? '<span class="green">已取貨</span>' : '<span class="red">未取貨</span>';
                }

                $domain = $ecs->url();
                $domain = substr($domain, 0, -1);
                if (!empty($ticket['goods_id'])) {
                    $goods_url = $domain . build_uri('goods', array('gid' => $ticket['goods_id']));
                }

                $ticket['gift_data'] = '產品ID:<a href="'.$goods_url.'" target="_blank" >'.$ticket['goods_id'].'</a>';
            } else if ($ticket['type']==1) {
                $ticket['pickup_status'] = '';
                $ticket['notified_status'] = '';
                $ticket['gift_data'] = '優惠券:'.$ticket['coupon_code'];
            } else if ($ticket['type']==0) {
                $ticket['pickup_status'] = '';
                $ticket['notified_status'] = '';
                $ticket['gift_data'] = '積分:'.$ticket['points'];

            }
           
            $ticket['name'] = $user_info['first_name'] . ' ' . $user_info['last_name'];
            $ticket['actions'] = '';
            $ticket['image'] = empty($ticket['image'])? '':'<img width=50px src="../'. $ticket['image'].'" />';
            $ticket['follow_up_remark'] = '<span class="follow_up_remark_content" data-ticket-id="'.$ticket['ticket_id'].'" data-toggle="modal" data-target="#popup_follow_up_remark_modal" >'.(empty($ticket['follow_up_remark'])? '請輸入' : $ticket['follow_up_remark']).'</span>';

            if ($ticket['notified'] == 1) {
               $ticket['notified'] = '己通知';
            } else {
               $ticket['notified'] = '未通知';
            }

            if ($ticket['type'] == '2' ) {
                // if ($ticket['pickup_time'] == 0) {
                //     $ticket['pickup_status'] = '待取貨 <button type="button" data-ticket-id="'.$ticket['ticket_id'].'" class="btn btn-success btn-xs confirm-pickup">確定客戶取貨</button>';
                // } else {
                //     $ticket['pickup_status'] = '客人已取貨<br>('.local_date($_CFG['time_format'], $ticket['pickup_time']).')';
                // }
            }

            if ($ticket['opened'] == 1) {
                $ticket['opened_time'] = local_date($_CFG['time_format'], $ticket['opened_time']);
            } else {
                $ticket['opened_time'] = '';
            }
            
            $ticket['type'] = self::$giftTypeValues[$ticket['type']];
        }

        $arr = array(
          'data' => $res,
          'record_count' => $_REQUEST['record_count']
        );

        return $arr;
    }

    public function createGoodsRedeem($ticket_id, $gift_id, $user_id, $goods_id)
    {
        global $db, $ecs ,$_CFG;

        $sql = "select count(*) from ".$ecs->table('lucky_draw_goods_redeem')." where ticket_id = '".$ticket_id."' ";
        $count = $db->getOne($sql);

        if ($count == 0) {
            $sql = "insert into ".$ecs->table('lucky_draw_goods_redeem')." (ticket_id,gift_id,user,goods_id) value (".$ticket_id.",".$gift_id.",".$user_id.",".$goods_id.") ";
            $db->query($sql);
        }
    }
    
    public function getUserName($user_id)
    {
        global $db, $ecs ,$_CFG;
        
        $sql = 'SELECT  content ' .
    		   'FROM ' . $ecs->table('reg_extend_info') .
    		   " WHERE user_id = ".$user_id." and reg_field_id = 8  " ;
    	$extend_info_arr = $db->getRow($sql);
        
        return $extend_info_arr['content'] ;
    }

    public function getUserInfoConfirm($user_id)
    {
        global $db, $ecs ,$_CFG;
        
        $ar = [];
        $sql = 'SELECT  content ' .
    		   'FROM ' . $ecs->table('reg_extend_info') .
    		   " WHERE user_id = ".$user_id." and reg_field_id = 8  " ;
    	$extend_info_arr = $db->getRow($sql);
        $ar['first_name'] = $extend_info_arr['content'] ;

        $sql = 'SELECT  content ' .
    		   'FROM ' . $ecs->table('reg_extend_info') .
    		   " WHERE user_id = ".$user_id." and reg_field_id = 9  " ;
    	$extend_info_arr = $db->getRow($sql);
        $ar['last_name'] = $extend_info_arr['content'] ;

        $sql = "select email from ".$ecs->table('users')." where user_id = ".$user_id." ";
        $extend_info_arr = $db->getRow($sql);
        $ar['email'] = $extend_info_arr['email'] ;
    
        preg_match('/[0-9]+\@yohohongkong.com/', $ar['email'], $matches, PREG_OFFSET_CAPTURE);     
        if (sizeof($matches) > 0) {
            $ar['email'] = '';
        }

        return $ar;
    }

    public function getUserLuckyDrawOpen($ticket_id,$user_id)
    {
        global $db, $ecs ,$_CFG, $smarty;

        // check if opened
        $ticket_info = $this->getLuckyDrawTicketInfo($ticket_id);

        // check if the ticket is yourself
        if ($ticket_info['user_id'] != $user_id) {
            //$result['error'] = '錯誤,你不是抽獎本人';
            $result['error'] = 'lucky_draw_invalid_user';
            return $result;
        }

        if ($ticket_info['opened']) {
            //$result['error'] = '此抽獎已經使用';
            $result['error'] = 'lucky_draw_already_redeemed';
            return $result;
        } else {
            $db->query('START TRANSACTION');

            $sql = "update ".$ecs->table('lucky_draw_ticket')." set opened = 1, opened_time = ".gmtime()." where ticket_id = ".$ticket_id." ";
            $db->query($sql);

            // get gift type;
            $gift_info = $this->getGiftInfo($ticket_info['gift_id']);
            $lucky_draw_info = $this->getLuckyDrawEventInfo($ticket_info['lucky_draw_id']);
            switch ($gift_info['type']) {
                case self::GIFT_TYPE_POINT:
                    if ($gift_info['points'] > 0) {
                        $pay_point = $gift_info['points'];
                        $user_id = $ticket_info['user_id'];
                        $desc = $lucky_draw_info['event_name'] . ' ['. self::$giftTypeValues[self::GIFT_TYPE_POINT] . ':' . $pay_point . '](ref_id:'.$ticket_id.')';
                        log_account_change($user_id, 0, 0, 0, $pay_point, $desc);
                    }
                    break;
                case self::GIFT_TYPE_COUPON:
                    // assign user to the user list of the coupon
                    $coupon_code = $gift_info['coupon_code'];
                    $user_id = $ticket_info['user_id'];
                    if (!empty($coupon_code) && !empty($user_id)) {
                        $this->assign_user_to_coupon($coupon_code, $user_id);
                    }
                    break;
                case self::GIFT_TYPE_PRODUCT:
                    // table log for product
                    $this->createGoodsRedeem($ticket_id, $ticket_info['gift_id'], $ticket_info['user_id'], $gift_info['goods_id']);
                    break;
            }

            $db->query('COMMIT');
        }

        $sql = "select ldt.gift_id, ldg.title, ldg.image, ldg.image2 from ".$ecs->table('lucky_draw_ticket')." ldt left join ".$ecs->table('lucky_draw_gifts')." ldg on (ldt.gift_id = ldg.gift_id) where ticket_id = ".$ticket_id." ";
        return $db->getRow($sql);
    }

    public function sendLuckyDrawResultNotice($ticket_id)
    {
        global $db, $ecs ,$_CFG, $smarty;

        $ticket_id = intval($ticket_id);

        $ticket_info = $this->getLuckyDrawTicketInfo($ticket_id); 
        $gift_info = $this->getGiftInfo($ticket_info['gift_id']);
        $user_id = $ticket_info['user_id'];

        if ($ticket_info['notified'] == true || $ticket_info['opened'] == false) {
            return true;
        }

        $user_info = $this->userController->get_user_info($user_id);
        $user_email = $user_info['email'];
        //echo '<pre>';
        //print_r($user_info);
        $user_name = $this->getUserName($user_id);

        if ($gift_info['type'] == self::GIFT_TYPE_COUPON) {
            $coupon_goods_url = $gift_info['share_link'];
            // $sql = "select goods_list from ".$ecs->table('coupons')." where coupon_code = '".$gift_info['coupon_code'] ."' ";
            // $gift_coupon_goods = $db->getOne($sql);
            // $gift_coupon_goods = json_decode($gift_coupon_goods);
            // 
            // if (!empty($gift_coupon_goods)) {
            //     $gift_coupon_goods_first = $gift_coupon_goods[0][0];
            // 
            //     $domain = $ecs->url();
            //     $domain = substr($domain, 0, -1);
            //     if (!empty($gift_coupon_goods_first)) {
            //         $coupon_goods_url = $domain . build_uri('goods', array('gid' => $gift_coupon_goods_first));
            //     }
            // 
            // }
        }

        $domain = $ecs->url();
        $domain = substr($domain, 0, -1);
        if (!empty($gift_info['goods_id'])) {
            $free_gift_goods_url = $domain . build_uri('goods', array('gid' => $gift_info['goods_id']));
        }

        assign_template();
        $tpl = get_mail_template('lucky_draw_result_notice');
        $smarty->assign('ref_id',$ticket_id);
        $smarty->assign('user_name', $user_name);
		$smarty->assign('type', $gift_info['type']);
        $smarty->assign('prize', $gift_info['title']);
        $smarty->assign('shop_name', $_CFG['shop_name']);
        $smarty->assign('send_date', date($_CFG['time_format']));
        $smarty->assign('points', number_format($gift_info['points']));
        $smarty->assign('coupon_code', $gift_info['coupon_code']);
        $smarty->assign('goods_url', $coupon_goods_url);
        $smarty->assign('free_gift_goods_url', $free_gift_goods_url);

        $content = $smarty->fetch('str:' . $tpl['template_content']);

        send_mail($_CFG['shop_name'], $user_email, $tpl['template_subject'], $content, $tpl['is_html']);

        $sql = "update ".$ecs->table('lucky_draw_ticket')." set notified = 1 where ticket_id = '".$ticket_id."' ";
        $db->query($sql);

        return true;
    }

    public function sendLuckyDrawFreeGiftPickupNotice($ticket_id)
    {
        global $db, $ecs ,$_CFG, $smarty;

        $ticket_id = intval($ticket_id);

        $ticket_info = $this->getLuckyDrawTicketInfo($ticket_id);
        $gift_info = $this->getGiftInfo($ticket_info['gift_id']);

        if ($ticket_info['opened'] == 0 || empty($ticket_info['order_id'])) {
            return false;
        }

        // only for free gift
        if ($gift_info['type'] == self::GIFT_TYPE_PRODUCT) {
           
            $user_id = $ticket_info['user_id'];

            if (!empty($ticket_info['notified_time'])) {
                return false;
            }

            $user_info = $this->userController->get_user_info($user_id);
            $user_email = $user_info['email'];
            //echo '<pre>';
            //print_r($user_info);
            $user_name = $this->getUserName($user_id);

            if (!empty($ticket_info['order_id'])) {
                $sql = "select order_sn from ". $ecs->table('order_info') ." where order_id = '".$ticket_info['order_id']."' ";
                $order_sn = $db->getOne($sql);
            }
            
            //assign_template();
            $tpl = get_mail_template('lucky_draw_gift_pickup_notice');
            $smarty->assign('user_name', $user_name);
            $smarty->assign('order_sn', $order_sn);
            $smarty->assign('type', $gift_info['type']);
            $smarty->assign('prize', $gift_info['title']);
            $smarty->assign('ref_id', $ticket_id);
            $smarty->assign('shop_name', $_CFG['shop_name']);
            $smarty->assign('send_date', date($_CFG['time_format']));
            $content = $smarty->fetch('str:' . $tpl['template_content']);

            send_mail($_CFG['shop_name'], $user_email, $tpl['template_subject'], $content, $tpl['is_html']);

            $sql = "update ".$ecs->table('lucky_draw_goods_redeem')." set notified_time = ".gmtime()." where ticket_id = '".$ticket_id."' ";
            $db->query($sql);
            return true;
        } else {
            return false;
        }
    }

    public function getGiftDetail($gift_id)
    {
        global $db, $ecs ,$_CFG;

        $sql = "select * from ".$ecs->table('lucky_draw_gifts')." where gift_id = ".$gift_id." ";
        return $db->getRow($sql);
    }

    public function getDrawResultsDetail($draw_results)
    {
        global $db, $ecs ,$_CFG;
        
        $res = [];
        foreach ($draw_results as $gift_id) {
            $res[] = $this->getGiftDetail($gift_id);
        }
        
        return $res;
    }

    public function drawStart($ticket_id)
    {
        global $db, $ecs ,$_CFG;

        $draw_result = [];

        // get ticket Info
        $ticket_info = $this->getLuckyDrawTicketInfo($ticket_id);

        if (!empty($ticket_info['draw_results'])) {
            $draw_results = explode(',', $ticket_info['draw_results']);
            return $draw_results;
        }
        
        // step 1 find level
        $level = $this->findLevelByProbability();

        // step 2 find goods into that level
        $gift_id = $this->findGiftInLevel($level, $ticket_info['lucky_draw_id']);
      
        if (empty($gift_id)) {
            $probabilityRate = $this->getProbabilityRate();
            $levelSize = sizeof($probabilityRate);
            $level = $levelSize;
            $gift_id = $this->findGiftInLevel($level, $ticket_info['lucky_draw_id']);
        }

        if (empty($gift_id)) {
            return false;
        }

        $draw_result[] = $gift_id;

        $db->query('START TRANSACTION');

        // reduct the gift qty
        $sql = "update ".$ecs->table('lucky_draw_gifts')." set qty = qty -1 , won = won +1  where gift_id = '".$gift_id."' ";
        $db->query($sql);

        // save to ticket record
        $sql = "update ".$ecs->table('lucky_draw_ticket')." set gift_id = ".$gift_id." where ticket_id = '".$ticket_id."' ";
        $db->query($sql);

        // step 3 find must show items
        $must_show_items = $this->findMustShowItems($ticket_info['lucky_draw_id'],$gift_id);
        
        $draw_result = array_merge($draw_result, $must_show_items);

        $must_show_items_count = sizeof($must_show_items);
        
        // not enough gifts
        $remain = 7-$must_show_items_count;
        if ($remain > 0) {
            // step 4 find other goods
            $gifts = $this->randomFindOtherGifts($ticket_info['lucky_draw_id'], $draw_result, $remain);
            $draw_result = array_merge($draw_result, $gifts);
        }

        shuffle($draw_result);

        // save to ticket records
        $sql = "update ".$ecs->table('lucky_draw_ticket')." set draw_results = '".implode(',', $draw_result)."', draw_results_time = ".gmtime()." where ticket_id = '".$ticket_id."' ";
        $db->query($sql);

        $db->query('COMMIT');

        return $draw_result;
    }

    public function getLuckyDrawTicketInfo($ticket_id)
    {
        global $db, $ecs ,$_CFG;

        $sql = "select ldt.*, ldgr.goods_id, ldgr.order_id, ldgr.notified_time from ".$ecs->table('lucky_draw_ticket')." ldt left join ".$ecs->table('lucky_draw_goods_redeem')." ldgr on (ldt.ticket_id = ldgr.ticket_id) where ldt.ticket_id = ".$ticket_id." ";
        return $db->getRow($sql);
    }

    public function getUserLuckyDrawTicket($user_id, $lucky_draw_id)
    {
        global $db, $ecs ,$_CFG;

        $sql = "select ticket_id from ".$ecs->table('lucky_draw_ticket')." where user_id = ".$user_id." and opened = 0 and lucky_draw_id = '".$lucky_draw_id."' order by ticket_id limit 1";
        return $db->getOne($sql);
    }
    
    public function getActiveLuckyDrawEvent()
    {
        global $db, $ecs ,$_CFG;
        
        $today = gmtime();
        $sql = "select lucky_draw_id from ".$ecs->table('lucky_draw')." where status = 1 and start_date <= ".$today." and end_date >= ".$today." order by lucky_draw_id ";

        return $db->getCol($sql);
    }

    public function getTheFirstActiveLuckyDrawEvent()
    {
        global $db, $ecs ,$_CFG;

        $today = gmtime();
        $sql = "select lucky_draw_id from ".$ecs->table('lucky_draw')." where status = 1 and start_date <= ".$today." and end_date >= ".$today." order by lucky_draw_id limit 1 ";
        return $db->getOne($sql);
    }

    public function getTheLastFinishedLuckyDrawEvent()
    {
        global $db, $ecs ,$_CFG;

        $today = gmtime();
        $sql = "select lucky_draw_id from ".$ecs->table('lucky_draw')." where status = 0 order by lucky_draw_id desc limit 1 ";
        return $db->getOne($sql);
    }

    public function isLuckyDrawEventActive()
    {
        global $db, $ecs ,$_CFG;
        
        $today = gmtime();
        $sql = "select count(*) from ".$ecs->table('lucky_draw')." where status = 1 and start_date <= ".$today." and end_date >= ".$today." order by lucky_draw_id ";
        $count = $db->getOne($sql);

        if ($count > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function isActiveEventShowOnMenu()
    {
        global $db, $ecs ,$_CFG;
        
        $today = gmtime();
        $sql = "select show_on_menu from ".$ecs->table('lucky_draw')." where status = 1 and start_date <= ".$today." and end_date >= ".$today." order by lucky_draw_id limit 1";
        $show = $db->getOne($sql);
        
        if ($show == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function getDrawTicketCount($user_id, $lucky_draw_id)
    {
        global $db, $ecs ,$_CFG;
        
        if ($user_id == 0) {
            return 0;
        }
        $sql = "select count(*) from ".$ecs->table('lucky_draw_ticket')." where user_id= ".$user_id." and opened = 0 and lucky_draw_id = ".$lucky_draw_id." ";
        
        return $db->getOne($sql);
    }

    public function checkLuckyDrawId()
    {
        global $db, $ecs ,$_CFG;

        if (empty($_REQUEST['lucky_draw_id'])) {
            header("Location: lucky_draw.php?act=list");
            die();
        } else {
            return true;
        }
    }

    public function saveLuckyDrawSubmit()
    {
        global $db, $ecs ,$_CFG;

        $start_date = local_strtotime($_REQUEST['start_date'] . ' 00:00:00');
        $end_date = local_strtotime($_REQUEST['end_date'] . ' 23:59:59');
        
        $purchase_amount = $_REQUEST['purchase_amount'];
        
        $sql = "update ".$ecs->table('lucky_draw')." set share_link_1 = '".$_REQUEST['share_link_1']."', share_link_2 = '".$_REQUEST['share_link_2']."', hold_draw_status = '".$_REQUEST['hold_draw_status']."', lucky_draw_license = '".$_REQUEST['lucky_draw_license']."',  event_name = '".$_REQUEST['event_name']."', purchase_amount = '".$_REQUEST['purchase_amount']."', status = '".$_REQUEST['status']."', show_on_menu = '".$_REQUEST['show_on_menu']."', start_date = '".$start_date."', end_date = '".$end_date."', coupon_code = '".$_REQUEST['coupon_code']."' where lucky_draw_id = '".$_REQUEST['lucky_draw_id']."' ";
 
        $db->query($sql);
        
        clear_cache_files();

        return true;
    }

    public function createLuckyDrawSubmit()
    {
        global $db, $ecs ,$_CFG;

        $start_date = local_strtotime($_REQUEST['start_date'] . ' 00:00:00');
        $end_date = local_strtotime($_REQUEST['end_date'] . ' 23:59:59');
        
        $purchase_amount = $_REQUEST['purchase_amount'];
        
        $sql = "insert into ".$ecs->table('lucky_draw')." (event_name,start_date,end_date,purchase_amount) values ('".$_REQUEST['event_name']."','".$start_date."','".$end_date."','".$purchase_amount."')" ;
        $db->query($sql);
        $lucky_draw_id = $db->Insert_ID();

        $sql = "insert into ".$ecs->table('lucky_draw_desc')." (lucky_draw_id,lang,update_date) 
               values ('".$lucky_draw_id."','en_us','".gmtime()."') ";
        $db->query($sql);
       
        return true;
    }

    public function saveLuckyDrawIntroSubmit()
    {
        global $db, $ecs ,$_CFG;

        $lucky_draw_intro = ($_REQUEST['lucky_draw_intro']);
        $lucky_draw_intro_mobile = ($_REQUEST['lucky_draw_intro_mobile']);
        
        $sql = "update ".$ecs->table('lucky_draw_desc')." set intro = '".$lucky_draw_intro."', intro_mobile = '".$lucky_draw_intro_mobile."', update_date = '".gmtime()."'  where lucky_draw_id = ".$_REQUEST['lucky_draw_id']." and lang = 'en_us' ";
        
        $db->query($sql);
      
        return true;
    }

    public function saveLuckyDrawFinishedSubmit()
    {
        global $db, $ecs ,$_CFG;
        
        //mysql_escape_string
        $lucky_draw_finished = ($_REQUEST['lucky_draw_finished']);
        $lucky_draw_finished_mobile = ($_REQUEST['lucky_draw_finished_mobile']);
        
        $sql = "update ".$ecs->table('lucky_draw_desc')." set finished = '".$lucky_draw_finished."', finished_mobile = '".$lucky_draw_finished_mobile."', update_date = '".gmtime()."'  where lucky_draw_id = ".$_REQUEST['lucky_draw_id']." and lang = 'en_us' ";
        $db->query($sql);
      
        return true;
    }

    public function saveLuckyDrawTncSubmit()
    {
        global $db, $ecs ,$_CFG;

        $lucky_draw_tnc = ($_REQUEST['lucky_draw_tnc']);
        
        $sql = "update ".$ecs->table('lucky_draw_desc')." set tnc = '".$lucky_draw_tnc."', update_date = '".gmtime()."'  where lucky_draw_id = ".$_REQUEST['lucky_draw_id']." and lang = 'en_us' ";
        $db->query($sql);
      
        return true;
    }

    public function getLuckyDrawEventInfo($lucky_draw_id)
    {
        global $db, $ecs ,$_CFG;

        $sql = "select * from ".$ecs->table('lucky_draw')." ld left join ".$ecs->table('lucky_draw_desc')." ldd on (ld.lucky_draw_id = ldd.lucky_draw_id and lang = 'en_us') where ld.lucky_draw_id = '".$lucky_draw_id."' ";
        $lucky_draw_info = $db->getRow($sql);
        $lucky_draw_info['start_date'] = local_date('Y-m-d H:i:s', $lucky_draw_info['start_date']);
        $lucky_draw_info['end_date'] = local_date('Y-m-d H:i:s', $lucky_draw_info['end_date']);

        //$lucky_draw_info['intro_mobile'] = htmlspecialchars($lucky_draw_info['intro_mobile']);


        return $lucky_draw_info;
    }

    public function getLuckyDrawEventList($is_pagination = true)
    {
        global $db, $ecs ,$_CFG;

        $sql = "select count(*) from ".$ecs->table('lucky_draw')." where 1";
        $total_count = $db->getOne($sql);
        
        $_REQUEST['record_count'] = $total_count;
        $_REQUEST['sort_by']    = empty($_REQUEST['sort_by']) ? 'type' : trim($_REQUEST['sort_by']);
        $_REQUEST['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);

        $sql = "select * from ".$ecs->table('lucky_draw')." where 1";
        $sql .= ' order by '. $_REQUEST['sort_by'] . ' ' .  $_REQUEST['sort_order'];
        $sql .= ($is_pagination ? " LIMIT " . $_REQUEST['start'] . ", " . $_REQUEST['page_size'] : '');
        $res = $db->getAll($sql);

        foreach ($res as $key => &$row) {
            $row['start_date'] = local_date($_CFG['time_format'], $row['start_date']);
            $row['end_date'] = local_date($_CFG['time_format'], $row['end_date']);
            $actions_html = '<a class="collapse-link" href="lucky_draw_detail.php?act=list&amp;lucky_draw_id='.$row['lucky_draw_id'].'"><i class="fa fa-eye green" style="font-size: 20px;"></i></a> ';
            $actions_html .= '<a class="collapse-link" href="lucky_draw_edit.php?act=list&amp;lucky_draw_id='.$row['lucky_draw_id'].'"><i class="fa fa-cog" style="font-size: 20px;"></i></a>';
            $row['actions'] = $actions_html;
            $row['status'] = ($row['status']? 'Enabled':'Disabled');
        }

        $arr = array(
            'data' => $res,
            'record_count' => $_REQUEST['record_count']
        );

        return $arr;
    }

    public function getGiftInfo($gift_id)
    {
        global $db, $ecs ,$_CFG;
        
        $sql = "select * from ".$ecs->table('lucky_draw_gifts')." where gift_id = '".$gift_id."'";
        $gift = $db->getRow($sql);

        if ($gift['type'] == self::GIFT_TYPE_PRODUCT) {
            $gift['free_gift_coupon'] = $db->getOne("SELECT coupon_code FROM " . $ecs->table('lucky_draw') . " WHERE lucky_draw_id = $gift[lucky_draw_id]");
        }

        return $gift;
    }

    public function getLuckyDrawLevelOptions()
    {
        global $db, $ecs ,$_CFG;

        $sql = "select * from ".$ecs->table('lucky_draw_levels')." where 1";
        $levels = $db->getAll($sql);

        $res = [];
        $res[] = array('name' => '請選擇', 'value' => '');
        foreach ($levels as $level) {
            $res[] = array('name'=> 'Level '.$level['level_id'] . ' ('.$level['probability'].'%)', 'value' => $level['level_id']);
        }

        return $res;
    }

    public function getLuckyDrawLevelProbability()
    {
        global $db, $ecs ,$_CFG;

        $sql = "select * from ".$ecs->table('lucky_draw_levels')." where 1";
        $levels = $db->getAll($sql);

        $res = [];
        $res[] = array('name' => '請選擇', 'value' => '');
        foreach ($levels as $level) {
            $res[$level['level_id']] = $level['probability'];
        }

        return $res;
    }

    public function giftCreatePointSubmit()
    {
        global $db, $ecs ,$_CFG;

        if (!empty($_FILES['file'])) {
            include_once(ROOT_PATH . 'includes/cls_image.php');
            $image = new \cls_image($_CFG['bgcolor']);
            $img_name = basename($image->upload_image($_FILES['file'],'luckydraw/products'));
            $img_path = 'data/luckydraw/products/'.$img_name;
        }

        if (!empty($_FILES['file2'])) {
            include_once(ROOT_PATH . 'includes/cls_image.php');
            $image = new \cls_image($_CFG['bgcolor']);
            $img_name = basename($image->upload_image($_FILES['file2'],'luckydraw/products'));
            $img_path2 = 'data/luckydraw/products/'.$img_name;
        }

        if ($_REQUEST['point_qty'] == ''){
            $_REQUEST['point_qty'] = 0;
        }

        if (!is_numeric($_REQUEST['point_qty'])) {
            $result['error'] = '請輸入正確數量';
            return $result;
        }

        if (!is_numeric($_REQUEST['point_value'])) {
            $result['error'] = '請輸入正確績分';
            return $result;
        }

        if (empty($img_path)) {
            $img_path = '';
        }

        if (empty($img_path2)) {
            $img_path2 = '';
        }

        if (!empty($_REQUEST['gift_id'])) {
            $sql = "update ".$ecs->table('lucky_draw_gifts')." set must_show = '".$_REQUEST['must_show']."', qty = qty + '".$_REQUEST['point_qty']."', ". (empty($img_path)? '' : " image = '".$img_path."',") . (empty($img_path2)? '' : " image2 = '".$img_path2."',") . " title = '".$_REQUEST['point_title']."' where gift_id = '".$_REQUEST['gift_id']."' ";
            $db->query($sql);
        } else {
            $gift_type = self::GIFT_TYPE_POINT;
            $sql = "insert into ".$ecs->table('lucky_draw_gifts')." (lucky_draw_id,type,must_show,title,points,qty,probability_level,image,image2,create_time) 
                  values ('".$_REQUEST['lucky_draw_id']."',".$gift_type.",'".$_REQUEST['must_show']."','".$_REQUEST['point_title']."','".$_REQUEST['point_value']."','".$_REQUEST['point_qty']."',null,'".$img_path."','".$img_path2."','".gmtime()."') ";
            $db->query($sql);
        }
 
        return true;
    }

    public function giftCreateProductSubmit()
    {
        global $db, $ecs ,$_CFG;

        if (!empty($_FILES['file'])) {
            include_once(ROOT_PATH . 'includes/cls_image.php');
            $image = new \cls_image($_CFG['bgcolor']);
            $img_name = basename($image->upload_image($_FILES['file'],'luckydraw/products'));
            $img_path = 'data/luckydraw/products/'.$img_name;
        }

        if (!empty($_FILES['file2'])) {
            include_once(ROOT_PATH . 'includes/cls_image.php');
            $image = new \cls_image($_CFG['bgcolor']);
            $img_name = basename($image->upload_image($_FILES['file2'],'luckydraw/products'));
            $img_path2 = 'data/luckydraw/products/'.$img_name;
        }

        if ($_REQUEST['product_qty'] == ''){
            $_REQUEST['product_qty'] = 0;
        }

        if (!is_numeric($_REQUEST['product_qty'])) {
            $result['error'] = '請輸入正確數量';
            return $result;
        }

        // check product
        if (!empty($_REQUEST['gift_id'])) {
            $sql = "update ".$ecs->table('lucky_draw_gifts')." set must_show = '".$_REQUEST['must_show']."',  qty = qty + '".$_REQUEST['product_qty']."', ". (empty($img_path)? '' : " image = '".$img_path."',") . (empty($img_path2)? '' : " image2 = '".$img_path2."',") . "  title = '".$_REQUEST['product_title']."' where gift_id = '".$_REQUEST['gift_id']."' ";
            $db->query($sql);
        } else {
            
            //// code to id
            //$sql = "select goods_id from ".$ecs->table('goods')." where goods_sn = '".$_REQUEST['product_code']."'";
            //$goods_id = $db->getOne($sql);

            $goods_id = $_REQUEST['product_code'];
            $sql = "select count(*) from ".$ecs->table('goods')." where goods_id = '".$goods_id."'";
            $product_exist = $db->getOne($sql);

            if ($product_exist == 0) {
                $result['error'] = '找不到產品';
                return $result;
            }

            // check product used already
            $sql = "select count(*) from ".$ecs->table('lucky_draw_gifts')." where goods_id = '".$goods_id."' and lucky_draw_id= ".$_REQUEST['lucky_draw_id']." ";
            $product_exist = $db->getOne($sql);
            if ($product_exist == 1) {
                $result['error'] = '產品重覆在獎品列表';
                return $result;
            }

            if (empty($img_path)) {
                $img_path = '';
            }

            if (empty($img_path2)) {
                $img_path2 = '';
            }

            $gift_type = self::GIFT_TYPE_PRODUCT;
            $sql = "insert into ".$ecs->table('lucky_draw_gifts')." (lucky_draw_id,type,must_show,title,goods_id,qty,probability_level,image,image2,create_time) 
                  values ('".$_REQUEST['lucky_draw_id']."',".$gift_type.",'".$_REQUEST['must_show']."','".$_REQUEST['product_title']."','".$goods_id."','".$_REQUEST['product_qty']."',null,'".$img_path."','".$img_path2."','".gmtime()."') ";

            $db->query($sql);
        }
        
        return true;
    }

    public function giftCreateCouponSubmit()
    {
        global $db, $ecs ,$_CFG;

        if ($_REQUEST['coupon_qty'] == ''){
            $_REQUEST['coupon_qty'] = 0;
        }

        if (!is_numeric($_REQUEST['coupon_qty'])) {
            $result['error'] = '請輸入正確數量';
            return $result;
        }

        if (!empty($_FILES['file'])) {
            include_once(ROOT_PATH . 'includes/cls_image.php');
            $image = new \cls_image($_CFG['bgcolor']);
            $img_name = basename($image->upload_image($_FILES['file'],'luckydraw/products'));
            $img_path = 'data/luckydraw/products/'.$img_name;
        }

        if (!empty($_FILES['file2'])) {
            include_once(ROOT_PATH . 'includes/cls_image.php');
            $image = new \cls_image($_CFG['bgcolor']);
            $img_name = basename($image->upload_image($_FILES['file2'],'luckydraw/products'));
            $img_path2 = 'data/luckydraw/products/'.$img_name;
        }

        // check coupon code exist
        $sql = "select count(*) from ".$ecs->table('coupons')." where coupon_code = '".$_REQUEST['coupon_code']."' ";
        $coupon_exist = $db->getOne($sql);

        if ($coupon_exist == 0) {
            $result['error'] = '優惠券不存在';
            return $result;
        }

        if (!empty($_REQUEST['gift_id'])) {
            $sql = "update ".$ecs->table('lucky_draw_gifts')." set share_link = '".$_REQUEST['share_link']."',  must_show = '".$_REQUEST['must_show']."', qty = qty + '".$_REQUEST['coupon_qty']."', ". (empty($img_path)? '' : " image = '".$img_path."',") .  (empty($img_path2)? '' : " image2 = '".$img_path2."',") ." title = '".$_REQUEST['coupon_title']."' where gift_id = '".$_REQUEST['gift_id']."' ";
            $db->query($sql);
        } else {
            // check coupon used already
            $sql = "select count(*) from ".$ecs->table('lucky_draw_gifts')." where coupon_code = '".$_REQUEST['coupon_code']."' and lucky_draw_id= ".$_REQUEST['lucky_draw_id']." ";
            $coupon_exist = $db->getOne($sql);

            if ($coupon_exist > 0) {
                $result['error'] = '優惠券已經在獎品列表';
                return $result;
            }

            if (empty($img_path)) {
                $img_path = '';
            }

            if (empty($img_path2)) {
                $img_path2 = '';
            }

            $gift_type = self::GIFT_TYPE_COUPON;
            $sql = "insert into ".$ecs->table('lucky_draw_gifts')." (lucky_draw_id,type,must_show,title,coupon_code,qty,probability_level,image,image2,create_time) 
               values ('".$_REQUEST['lucky_draw_id']."',".$gift_type.",'".$_REQUEST['must_show']."','".$_REQUEST['coupon_title']."','".$_REQUEST['coupon_code']."','".$_REQUEST['coupon_qty']."',null,'".$img_path."','".$img_path2."','".gmtime()."') ";
            $db->query($sql);
        }

        // check if the admin user is added to user list of coupon
        $sql = "select users_list from ".$ecs->table('coupons')." where coupon_code = '".$_REQUEST['coupon_code']."' ";
        $coupon_user_list = $db->getOne($sql);
        
        if (empty($coupon_user_list)) {
            $this->assign_user_to_coupon($_REQUEST['coupon_code'], 1); // id:1 is admin
        }
        
        return true;
    }
    
    public function saveLuckyDrawImageSubmit()
    {
        global $db, $ecs ,$_CFG;
        
        if (!empty($_FILES['file'])) {
            include_once(ROOT_PATH . 'includes/cls_image.php');
            $image = new \cls_image($_CFG['bgcolor']);
            $img_name = basename($image->upload_image($_FILES['file'],'luckydraw/images'));
            $img_path = 'data/luckydraw/images/'.$img_name;
        }

        if (!empty($_FILES['file2'])) {
            include_once(ROOT_PATH . 'includes/cls_image.php');
            $image = new \cls_image($_CFG['bgcolor']);
            $img_name = basename($image->upload_image($_FILES['file2'],'luckydraw/images'));
            $img_path2 = 'data/luckydraw/images/'.$img_name;
        }

        if (!empty($img_path)) {
            $sql = "update ".$ecs->table('lucky_draw')." set game_image = '".$img_path."' where lucky_draw_id = '".$_REQUEST['lucky_draw_id']."' ";
            $db->query($sql);
        }

        if (!empty($img_path2)) {
            $sql = "update ".$ecs->table('lucky_draw')." set game_image_mobile = '".$img_path2."' where lucky_draw_id = '".$_REQUEST['lucky_draw_id']."' ";
            $db->query($sql);
        }
        return true;
    }

    public function changeGiftLevelSubmit()
    {
        global $db, $ecs ,$_CFG;

        if (!empty($_REQUEST['gift_id']) && !empty($_REQUEST['gift_level'])) {
            $sql = 'update '.$ecs->table('lucky_draw_gifts').' set probability_level = "'.$_REQUEST['gift_level'].'" where gift_id = '.$_REQUEST['gift_id'].' ';
            $db->query($sql);
        }

        if (!empty($_REQUEST['gift_id']) && empty($_REQUEST['gift_level'])) {
            $sql = 'update '.$ecs->table('lucky_draw_gifts').' set probability_level = null where gift_id = '.$_REQUEST['gift_id'].' ';
            $db->query($sql);
        }

        return true;
    }

    public function getLuckyDrawGiftList($is_pagination = true)
    {
        global $db, $ecs ,$_CFG;

        // total record
        $sql = "SELECT count(*) FROM ".$ecs->table('lucky_draw_gifts') . ' where lucky_draw_id = '.$_REQUEST['lucky_draw_id'].'';
        $total_count = $db->getOne($sql);

        $_REQUEST['record_count'] = $total_count;

        if ($_REQUEST['sort_by'] == 'probability') {
            $_REQUEST['sort_by'] = 'probability_level';
        }

        $_REQUEST['sort_by']    = empty($_REQUEST['sort_by']) ? 'type' : trim($_REQUEST['sort_by']);
        $_REQUEST['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);

        // pagination
        $sql = "SELECT * FROM ".$ecs->table('lucky_draw_gifts') . ' where lucky_draw_id = '.$_REQUEST['lucky_draw_id'].' ';
        $sql .= ' order by '. $_REQUEST['sort_by'] . ' ' .  $_REQUEST['sort_order'];
        $sql .= ($is_pagination ? " LIMIT " . $_REQUEST['start'] . ", " . $_REQUEST['page_size'] : '');
        //exit;
        $res = $db->getAll($sql);
        $probability = $this->getLuckyDrawLevelProbability();
        foreach ($res as $key => &$row) {
            $row['remain'] = $row['qty'] - $row['won'];
            if ($row['type'] == self::GIFT_TYPE_POINT) {
                $row['edit_link'] = '<a href="lucky_draw_gift_point.php?act=edit&lucky_draw_id='.$row['lucky_draw_id'].'&gift_id='.$row['gift_id'].'">'.$row['title'].'</a>';
            } elseif ($row['type'] == self::GIFT_TYPE_COUPON) {
                $row['edit_link'] = '<a href="lucky_draw_gift_coupon.php?act=edit&lucky_draw_id='.$row['lucky_draw_id'].'&gift_id='.$row['gift_id'].'">'.$row['title'].'</a>';
            } elseif ($row['type'] == self::GIFT_TYPE_PRODUCT) {
                $row['edit_link'] = '<a href="lucky_draw_gift_product.php?act=edit&lucky_draw_id='.$row['lucky_draw_id'].'&gift_id='.$row['gift_id'].'">'.$row['title'].'</a>';
            }
            
            $row['image'] = $row['image'] ? '<img width="50px" src="../'.$row['image'].'" />' : '';
            
            $select_level = $this->getLuckyDrawLevelOptions();
            if (empty($probability[$row['probability_level']])) {
                $row['probability'] = '';
            } else {
                $row['probability'] = $probability[$row['probability_level']].'%';
            }
            
            $html = '';
            $html .= '<select class="select_level" data-gift-id = '.$row['gift_id'].' >';
            foreach ($select_level as $level) {
                $html .= '<option  '.($level['value'] == $row['probability_level']? 'selected':'') .' value="'.$level['value'].'" >'.$level['name'].'</option>';
            }
            $html .= '</select>';
            $row['select_level'] = $html ;

            $actions_html = '<ul class="nav navbar-right panel_toolbox">';

            if ($row['type'] == self::GIFT_TYPE_POINT) {
                $actions_html .= '<li><a class="collapse-link" href="lucky_draw_gift_point.php?act=edit&amp;lucky_draw_id='.$row['lucky_draw_id'].'&gift_id='.$row['gift_id'].'"><i class="fa fa-eye green" style="font-size: 20px;"></i></a></li>';
            } elseif ($row['type'] == self::GIFT_TYPE_COUPON) {
                $actions_html .= '<li><a class="collapse-link" href="lucky_draw_gift_coupon.php?act=edit&amp;lucky_draw_id='.$row['lucky_draw_id'].'&gift_id='.$row['gift_id'].'"><i class="fa fa-eye green" style="font-size: 20px;"></i></a></li>';
            } elseif ($row['type'] == self::GIFT_TYPE_PRODUCT) {
                $actions_html .= '<li><a class="collapse-link" href="lucky_draw_gift_product.php?act=edit&amp;lucky_draw_id='.$row['lucky_draw_id'].'&gift_id='.$row['gift_id'].'"><i class="fa fa-eye green" style="font-size: 20px;"></i></a></li>';
            }
            
            $row['type'] = self::$giftTypeValues[$row['type']];
            $actions_html .= '<li><a class="close-link"><i class="fa fa-close"></i></a></li>';
            $actions_html .= '</ul>';

            $row['actions'] = $actions_html;
        }

        $arr = array(
            'data' => $res,
            'record_count' => $_REQUEST['record_count']
        );

        return $arr;
    }

    public function getLuckyDrawAllGiftsChartData($lucky_draw_id)
    {
        global $db, $ecs ,$_CFG;
        
        $sql = "select * from ".$ecs->table('lucky_draw_gifts')." where lucky_draw_id = '".$lucky_draw_id."' and status = 1 and probability_level is not null order by probability_level, gift_id ";        
        $res = $db->getAll($sql);

        $rate = $this->getProbabilityRate();

        $level_group = [];


        foreach ($res as $key => &$row) {
            $row['gift_total'] = $row['qty'] + $row['won']; 
            $row['gift_remain_percentage'] =  floor($row['qty'] / $row['gift_total'] * 100);
            $row['gift_remain_percentage_html'] = '<div class="progress-bar '.($row['gift_remain_percentage'] < 20 ? 'bg-red' : 'bg-green' ).' " role="progressbar" data-transitiongoal="'.$row['gift_remain_percentage'].'" aria-valuenow="79" style="width: '.$row['gift_remain_percentage'].'%;"></div>';
            $level_group[$row['probability_level']]['gifts'][] = $row;
            $level_group[$row['probability_level']]['rate'] = $rate[$row['probability_level']];
            $level_group[$row['probability_level']]['won_total'] += $row['won'];
            $level_group[$row['probability_level']]['predict_won_total'] = round($rate[$row['probability_level']] * 4000 / 100);
        }
        //echo '<pre>';
        //print_r($level_group);
        //exit;

        $sql = "select count(*) from ".$ecs->table('lucky_draw_gifts')." where lucky_draw_id = '".$lucky_draw_id."' and status = 1 and probability_level is not null and qty >0 order by probability_level, gift_id ";        
        $gifts_amount = $db->getOne($sql);

        $sql = "select sum(qty) from ".$ecs->table('lucky_draw_gifts')." where lucky_draw_id = '".$lucky_draw_id."' and status = 1 and probability_level is not null and qty >0 order by probability_level, gift_id ";        
        $gifts_qty_total = $db->getOne($sql);

        $data['gift_amount'] = $gifts_amount;
        $data['gift_qty_total'] = $gifts_qty_total;
        $data['level_group'] = $level_group;

        return $data;

    }

    public function getLuckyDrawEventOptions()
    {
        return $lucky_draw_event = array('1'=> '2018 818 lucky draw');
    }
    
    public function getTypeOptions()
    {
        return self::$typeValues;
    }

    public function personInChargeOption()
    {
        global $db, $ecs ,$_CFG;
        $sql = "SELECT user_id as value,user_name as name FROM ".$ecs->table('admin_user') . ' where is_disable = 0';
        $stocktake_person = $db->getAll($sql);
        array_unshift($stocktake_person, array('name'=>'請選擇...','value'=>''));
        return $stocktake_person;
    }

    public function personInChargeList()
    {
        global $db, $ecs ,$_CFG;
        $sql = "SELECT user_id,user_name FROM ".$ecs->table('admin_user'). ' where is_disable = 0';
        $stocktake_person = $db->getAll($sql);
        $person_list = [];
        foreach ($stocktake_person as $person) {
            $person_list[$person['user_id']] = $person['user_name'];
        }
        return $person_list;
    }

    public function bulkTicketFreeGiftCreateOrder()
    {
        $total = 0;
        $proccessed = 0;
        if (!empty($_REQUEST['selected_ticket_ids'])) {
            foreach ($_REQUEST['selected_ticket_ids'] as $ticket_id) {
                $total++;
                $proccessed += $this->createLuckyDrawFreeGiftPickupOrder($ticket_id) ? 1 : 0;
            }
        }

        $arr = [
            "total" => $total,
            "proccessed" => $proccessed
        ];

        return $arr;
    }

    public function createLuckyDrawFreeGiftPickupOrder($ticket_id)
    {
        global $db, $ecs ,$_CFG, $smarty;
        $orderController = new OrderController();
        $deliveryOrderController = new DeliveryOrderController();

        $ticket_id = intval($ticket_id);
        include_once ROOT_PATH . 'includes/lib_howang.php';
        include_once ROOT_PATH . 'includes/lib_order.php';
        include_once ROOT_PATH . 'includes/lib_order_1.php';
        include_once ROOT_PATH . 'includes/lib_goods.php';

        $ticket_info = $this->getLuckyDrawTicketInfo($ticket_id);
        $gift_info = $this->getGiftInfo($ticket_info['gift_id']);
        $admin = [
            'user_id' => 1,
            'user_name' => 'admin'
        ];
        $shipping_id = 2;               // 門市購買
        $payment = 3;                   // 現金付款
        $consignee = [
            "country" => 3409,          // 香港
            "district" => 0,
            "province" => 0,
            "city" => 0,
        ];
        $coupon_code = empty($gift_info['free_gift_coupon']) ? "" : $gift_info['free_gift_coupon'];

        if (!empty($ticket_info['user_id']) && $gift_info['type'] == self::GIFT_TYPE_PRODUCT && !empty($coupon_code)) {
            $user = $db->getRow("SELECT * FROM " . $ecs->table("users") . " WHERE user_id = $ticket_info[user_id]");
            if (!empty($user)) {
                if (empty($ticket_info['order_id'])) {
                    $sql = 'SELECT rank_id FROM ' . $ecs->table('user_rank') . " WHERE special_rank = '0' AND min_points <= " . intval($user['rank_points']) . ' AND max_points > ' . intval($user['rank_points']);
                    if ($row = $db->getOne($sql)) {
                        $user_rank = $row;
                    } else {
                        $user_rank = 0;
                    }
                    $session_id = md5("$ticket_info[ticket_id]_$ticket_info[user_id]_$ticket_info[goods_id]");
                    $_SESSION['discount'] = 1;

                    // Start of transaction
                    $db->query("START TRANSACTION");

                    // Step 1: add gift to cart
                    $sql = "SELECT g.goods_id, g.goods_name, g.goods_sn, g.give_integral, g.is_on_sale, g.is_real,g.integral, ".
                        "g.market_price, g.shop_price AS org_price, g.promote_price, g.promote_start_date, ".
                        "g.promote_end_date, g.goods_weight, g.integral, g.extension_code, ".
                        "g.goods_number, g.is_alone_sale, g.is_shipping,".
                        "IFNULL(mp.user_price, g.shop_price) AS shop_price ".
                    " FROM " . $ecs->table('goods'). " AS g ".
                    " LEFT JOIN " . $ecs->table('member_price') . " AS mp ".
                            "ON mp.goods_id = g.goods_id AND mp.user_rank = '$user_rank' ".
                    " WHERE g.goods_id = '$ticket_info[goods_id]'" .
                    " AND g.is_delete = 0";
                    $goods = $db->getRow($sql);

                    // Check empty stock - do not create order if empty stock
                    if ($this->erpController->getSellableProductQty($ticket_info['goods_id']) < 1) {
                        return false;
                    }

                    $goods['user_id']       = $ticket_info['user_id'];
                    $goods['goods_price']   = get_final_price($ticket_info["goods_id"], 1, false, [], $user_rank);
                    $goods['goods_price']   = $goods['goods_price']['final_price'];
                    $goods['parent_id']     = 0;
                    $goods['is_gift']       = 0;
                    $goods['is_shipping']   = 0;
                    $goods['goods_number']  = 1;
                    $goods['subtotal']      = $goods['goods_price'];
                    $goods['session_id']    = $session_id;

                    // Check preorder - do not create order if preorder
                    $goods_preorder = check_goods_preorder([$goods]);
                    if (!empty($goods_preorder)) {
                        return false;
                    }

                    // Update cart
                    $db->query("DELETE FROM " . $ecs->table('cart1') . " WHERE goods_id=$ticket_info[goods_id] AND session_id='$session_id'");
                    if ($db->autoExecute($ecs->table('cart1'), addslashes_deep($goods), 'INSERT') === false) {
                        return false;
                    }

                    // Step 2: generate order info
                    $order = array(
                        'shipping_id'     => $shipping_id,
                        'pay_id'          => $payment,
                        'pack_id'         => 0,
                        'card_id'         => 0,
                        'card_message'    => '',
                        'surplus'         => 0.00,
                        'integral'        => 0,
                        'bonus_id'        => 0,
                        'coupon_code'     => strtoupper($coupon_code),
                        'coupon_id'       => [],
                        'need_inv'        => 0,
                        'inv_type'        => '',
                        'inv_payee'       => '',
                        'inv_content'     => '',
                        'postscript'      => '抽獎活動免費禮物',
                        'order_remarks'   => '',
                        'serial_numbers'  => '',
                        'how_oos'         => '',
                        'need_insure'     => 0,
                        'user_id'         => $ticket_info['user_id'],
                        'add_time'        => gmtime(),
                        'order_status'    => OS_CONFIRMED,
                        'shipping_status' => SS_UNSHIPPED,
                        'pay_status'      => PS_PAYED,
                        'agency_id'       => get_agency_by_regions(array($consignee['country'], $consignee['province'], $consignee['city'], $consignee['district'])),
                        'user_agency_id'  => $user_agency_id,
                        'extension_code'  => '',
                        'extension_id'    => 0,
                        'from_ad'         => 0,
                        'referer'         => '',
                        'is_separate'     => $orderController::ORDER_SEPARATE_IGNORE,
                        'salesperson'     => $admin['user_name'],
                        'platform'        => $orderController::ORDER_FEE_PLATFORM_POS,
                        'address_id'      => 0,
                        'confirm_time'    => gmtime(),
                        'pay_time'        => gmtime(),
                        'shipping_time'   => gmtime(),
                    );
                    foreach ($consignee as $key => $value) {
                        $order[$key] = addslashes($value);
                    }

                    // Apply coupon
                    $coupon = coupon_info(0, $order['coupon_code']);
                    if (!empty($coupon)) {
                        $order['coupon_id'][] = $coupon['coupon_id'];
                    }

                    // Update order amount
                    $total = $orderController->order_fee_flow($order, [$goods], $consignee, $orderController::ORDER_FEE_PLATFORM_POS, 0);
                    $order['bonus']             = $total['bonus'];
                    $order['coupon']            = $total['coupon'];
                    $order['goods_amount']      = $total['goods_price'];
                    $order['discount']          = $total['discount'];
                    $order['surplus']           = $total['surplus'];
                    $order['tax']               = $total['tax'];
                    $order['cp_price']          = !empty($total['cp_price']) ? $total['cp_price'] : '0';
                    $order['pack_fee']          = $total['pack_fee'];
                    $order['shipping_fee']      = $total['shipping_fee'];
                    $order['insure_fee']        = $total['shipping_insure'];
                    $order['pay_fee']           = $total['pay_fee'];
                    $order['cod_fee']           = $total['cod_fee'];
                    $order['pack_fee']          = $total['pack_fee'];
                    $order['card_fee']          = $total['card_fee'];
                    $order['integral_money']    = $total['integral_money'];
                    $order['integral']          = $total['integral'];
                    $order['money_paid']        = number_format($total['amount'], 2, '.', '');

                    if ($order['shipping_id'] > 0) {
                        $shipping = shipping_info($order['shipping_id']);
                        $order['shipping_name'] = addslashes($shipping['shipping_name']);
                    }
                    if ($order['pay_id'] > 0) {
                        $payment = payment_info($order['pay_id']);
                        // $order['pay_name'] = addslashes($payment['pay_name']);
                        $order['pay_name'] = addslashes($payment['display_name']);
                    }

                    // Step 3: create order
                    $error_no = 0;
                    do {
                        $order['order_sn'] = get_order_sn(); //获取新订单号
                        $db->autoExecute($ecs->table('order_info'), $order, 'INSERT', '', 'SILENT');

                        $error_no = $db->errno();

                        if ($error_no > 0 && $error_no != 1062) {
                            return false;
                        }
                    } while ($error_no == 1062);

                    // Step 4: insert order goods
                    $new_order_id = $db->insert_id();
                    $order['order_id'] = $new_order_id;
                    $sql = "INSERT INTO " . $ecs->table('order_goods') . "( " .
                                "order_id, goods_id, goods_name, goods_sn, product_id, goods_number, market_price, ".
                                "goods_price, goods_attr, is_real, extension_code, parent_id, is_gift, goods_attr_id, cost) ".
                            " SELECT '$new_order_id', c.goods_id, c.goods_name, c.goods_sn, c.product_id, c.goods_number, c.market_price, ".
                            "CASE WHEN c.is_gift > 0 THEN c.goods_price ELSE LEAST(IFNULL(mp.user_price, c.goods_price),c.goods_price) END, c.goods_attr, c.is_real, c.extension_code, c.parent_id, c.is_gift, c.goods_attr_id, g.cost ".
                            " FROM " . $ecs->table('cart1') . "as c ".
                            " LEFT JOIN " . $ecs->table('goods') . " as g ON c.goods_id = g.goods_id ".
                            " LEFT JOIN " . $ecs->table('member_price') . " AS mp ON mp.goods_id = c.goods_id AND mp.user_rank = '$user_rank' ".
                            " WHERE session_id = '$session_id' AND rec_type = '0'";
                    $db->query($sql);
                    $sql = "SELECT og.rec_id, og.goods_id, og.goods_price, u.user_id FROM " . $ecs->table('order_goods') . ' og' .
                            " LEFT JOIN " . $ecs->table('order_info') . " oi ON og.order_id = oi.order_id" .
                            " LEFT JOIN " . $ecs->table('users') . " u ON oi.user_id = u.user_id" .
                            " WHERE og.order_id = " . $new_order_id;
                    $goods_list = $db->getAll($sql);
                    // Mark vip price
                    foreach ($goods_list as $goods) {
                        $is_vip = intval($orderController->is_vip_price($goods['goods_id'], $goods['user_id'], $goods['goods_price']));

                        if ($is_vip) { // Anthony: 如果購物車價格為特殊價格, 但等同VIP價, 預設為VIP價.
                            $sql = "UPDATE " . $ecs->table('order_goods') . " SET user_rank = 2 " .
                                " WHERE is_gift = 0 AND order_id = " . $new_order_id . " AND goods_id = " . $goods['goods_id'] . " AND rec_id = " . $goods['rec_id'];
                            $db->query($sql);
                        }
                    }
                    // Use coupon
                    if (!empty($order['coupon_id'])) {
                        foreach ($order['coupon_id'] as $coupon_id) {
                            use_coupon($coupon_id, $new_order_id, $order['user_id']);
                        }
                    }
                    // Update stock
                    $this->erpController->pos_erp_delivery($new_order_id, $admin, 'gift');

                    // Step 5: Clear cart
                    $sql = "DELETE FROM " . $ecs->table('cart1') . " WHERE session_id = '$session_id' AND rec_type = '0'";
                    $db->query($sql);
                    clear_all_files();

                    // Step 6: update ticket order_id
                    $sql = "UPDATE " . $ecs->table('lucky_draw_goods_redeem') . " SET order_id = $new_order_id WHERE ticket_id = $ticket_info[ticket_id]";
                    $db->query($sql);

                    // Step 7: create delivery order
                    $request =  [
                        'logistics_id'  => 7,
                        'operator'      => $admin['user_name']
                    ];
                    $deliveryOrderController->quick_create_delivery_order($new_order_id, $request, true);

                    // End of transaction
                    $db->query("COMMIT");
                    return true;
                }
            }
        }
        return false;
    }
}
