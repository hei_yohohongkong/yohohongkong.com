<?php

/***
* ctrl_base.php
* by MichaelHui 20170328
*
* Yoho base controller
***/

namespace Yoho\cms\Controller;
use Yoho\cms\Model;
if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}
require_once(ROOT_PATH . 'core/lib_mobile.php');

class YohoBaseController{
    protected $returnObj,$db,$ecs,$memcache,$lang;

    // Ajax query
    public $tableList = [];
    public $tableTotalCount = 0;
    const VERIFY_TYPE_SMS  = "SMS";
    const VERIFY_TYPE_MAIL = "MAIL";
    static $onWhite;

    public function __construct($tableName = ''){
        $this->returnObj=[
            'status'=>false,
            'data'=>''
        ];
        if(!empty($tableName))$this->tableName = $tableName;
        global $db,$ecs,$_LANG;
        $this->db = $db;
        $this->ecs = $ecs;
        $this->lang=$_LANG;
        $this->memcache = new \Memcached();
        $this->memcache->addServer('localhost',11211);

        // Get Class name
        $class = get_class($this);
        $class = explode("\\", $class);
        $class = end($class);
        $class = str_replace('Controller', '', $class);
        $class = trim(preg_replace('/([A-Z])/', ' $1', $class));
        $class = strtolower(str_replace(" ","_",$class));

        if(empty($this->tableName))$this->tableName = $class;
        $this->className = $class;
    }

    public function genCacheKey($params){
        return 'yoho-'.implode('-', $params);
    }

    public function getFullUrl($url){

        //Get full path
        define('SERVER_DOMAIN', implode('.', array_slice(explode('.', $_SERVER['SERVER_NAME']), -2)));
        if (strpos($_SERVER['SERVER_NAME'], 'beta.') !== false)
        {
            define('DESKTOP_HOST', 'beta.' . SERVER_DOMAIN);
            define('MOBILE_HOST', 'm.beta.' . SERVER_DOMAIN);
        }
        else
        {
            define('DESKTOP_HOST', 'www.' . SERVER_DOMAIN);
            define('MOBILE_HOST', 'm.' . SERVER_DOMAIN);
        }

        $new_url = 'https://' . DESKTOP_HOST . (substr($url, 0, 1) == '/' ? '' : '/') . $url;

        return $new_url;
    }

    public function trim_array_walk(&$array_value)
    {
        if (is_array($array_value))
        {
            array_walk($array_value, 'trim_array_walk');
        }else{
            $array_value = trim($array_value);
        }
    }

    public function intval_array_walk(&$array_value)
    {
        if (is_array($array_value))
        {
            array_walk($array_value, 'intval_array_walk');
        }else{
            $array_value = intval($array_value);
        }
    }

    public function escape_string($value)
    {
        // Add escape
        // case 1: "a\'s" -> "a\'s"
        // case 2: "a's"  -> "a\'s"
        if(is_string($value))
        {
            // If value have escape, remove first(magic_quotes_gpc is on, so if using html<input>: ' change to \', but other cases just only: ' to ' ).
            $value = stripslashes($value);
            // All value add escape if need.
            $value = mysql_real_escape_string($value);
        }

        return $value;
    }

    /** Get YOHO holidays(with Saturday and Sunday)
     * We're using 1823.gov.hk HongKong public hoilday ics, will auto update in every year.
     * 繁體中文: http://www.1823.gov.hk/common/ical/tc.ics
        * 簡體中文: http://www.1823.gov.hk/common/ical/sc.ics
       * 英文: http://www.1823.gov.hk/common/ical/en.ics
     */
    public function get_yoho_holidays($year = '', $clear = false, $without = [])
    {

        $count_sat = true;
        if(in_array('saturday', $without)) $count_sat = false;
        $count_sun = true;
        if(in_array('sunday', $without)) $count_sun = false;
        if(!empty($without)) $without_str = 'without_'.implode('_', $without);
        // Default: get this year
        global $db,$ecs;
        if(empty($year)){
            $year      = local_date("Y");
            $start_day = local_date('Y-01-01');
            $end_day   = local_date('Y-12-31');
        } else {
            $start_day = "$year-01-01";
            $end_day   = "$year-12-31";
        }
        $cacheKey = isset($without_str) ? $this->genCacheKey(['holidays', $year, $without_str]) : $this->genCacheKey(['holidays', $year]);

        if($clear){
            $this->memcache->delete($cacheKey);
        }
        $holidays = $this->memcache->get($cacheKey);

        if(!$holidays){
            $holidays           = array();
            $summary            = array();
            $hk_public_holidays = array();
            // ics file to array
            $sql = "SELECT * FROM " . $ecs->table('holidays') . " WHERE year >= " . $year;
            $res = $db->getAll($sql);
            if(count($res) > 0){
                foreach($res as $day){
                    $holidays[] = array('date' => $day['date'], 'info' => $day['info']);
                }
            } else {
                require_once(ROOT_PATH . 'includes/Icalparser_ics/src/IcalParser.php');
                $icalParser = new \om\IcalParser();
                $icalParser->parseFile(
                    'http://www.1823.gov.hk/common/ical/tc.ics'
                );
                // Get Hong Kong Public Holiday
                foreach ($icalParser->getSortedEvents() as $holiday){
                    $hk_public_holidays[] = $holiday['DTSTART']->format('Y-m-d');
                    $summary[]            = $holiday['SUMMARY'];
                    if($holiday['DTSTART']->format('Y') == $year){
                        $sql = "INSERT INTO " . $ecs->table('holidays') . " (year,date,info) VALUES ('".$year."','".$holiday['DTSTART']->format('Y-m-d')."','".$holiday['SUMMARY']."')";
                        $db->query($sql);
                    }
                }
            }

            while(strtotime($start_day) <= strtotime($end_day)) {
                // Get date week
                $day_week = local_date("N", strtotime($start_day));
                // If is Saturday or Sunday or is Hong Kong Public Holiday, is yoho holidays.
                if($day_week == 6 || $day_week == 7 || (in_array($start_day, $hk_public_holidays))){
                    $holiday = [];
                    if(in_array($start_day, $hk_public_holidays)) $holiday = ['date'=>$start_day,'info'=>$summary[array_search($start_day, $hk_public_holidays)]];
                    elseif($day_week == 6 && $count_sat) $holiday = ['date'=>$start_day,'info'=>'星期六'];
                    elseif($day_week == 7 && $count_sun) $holiday = ['date'=>$start_day,'info'=>'星期日'];
                    $holidays[] = $holiday;
                }
                $start_day = local_date("Y-m-d", strtotime("+1 day", strtotime($start_day)));
            }
            $holidays = array_filter($holidays);
            $this->memcache->set($cacheKey,$holidays);
        }

        return $holidays;
    }

    /** Get YOHO next work day
     * $gmtime integer (Start cal date GMT_format)
     * $need_day integer (How many min work days)
     */
    public function get_yoho_next_work_day($gmtime, $need_day = 1, $without = []){
        if(empty($gmtime))$gmtime = gmtime();

        $date          = local_date('Y-m-d', $gmtime);
        $yoho_holidays = $this->get_yoho_holidays('', false, $without);

        foreach ($yoho_holidays as $key => $value) {
            $holidays[] = $value['date'];
        }

        // $i = 1;
        // $nextBusinessDay = local_date('Y-m-d', strtotime($date . ' +' . $i . ' Weekday'));
        
        // while (in_array($nextBusinessDay, $holidays) || $i < $need_day) {
        //     $i++;
        //     $nextBusinessDay = local_date('Y-m-d', strtotime($date . ' +' . $i . ' Weekday'));
        // }

        $i = 0;
        $counted_days = 0; 
        while ($counted_days < $need_day) {
            $i++;
            $nextBusinessDay = local_date('Y-m-d', strtotime($date . ' +' . $i . ' days'));
            
            if (!in_array($nextBusinessDay, $holidays)) {
                $counted_days++; 
            }
        }

        if (empty($nextBusinessDay)) {
            $nextBusinessDay = local_date('Y-m-d', strtotime($date . ' +' . $i . ' Weekday'));
        }
        
        return local_strtotime($nextBusinessDay);
    }

    /** Count YOHO work day number
     *  basic concept: Calculation from starts today to $gmtime, deduction the holidays/weekends, count total work days.
     */
    public function count_yoho_work_day($gmtime, $now = NULL, $actual = false){
        if(!$gmtime) $gmtime	= gmtime();
        if(!$now) $now			= gmtime();
        $date     = local_date('Y-m-d', $gmtime);
        $now_date = local_date('Y-m-d', $now);
        $yoho_holidays = $this->get_yoho_holidays();
        foreach ($yoho_holidays as $key => $value) {
            $holidays[] = $value['date'];
        }
        if ($actual && in_array($date, $holidays)) {
            unset($holidays[array_search($date, $holidays)]);
        }
        $i = 1;
        $nextBusinessDay = local_date('Y-m-d', strtotime($now_date . ' +' . $i . ' Weekday'));
        while (in_array($nextBusinessDay, $holidays) ||strtotime($nextBusinessDay) < strtotime($date)) {
            $i++;
            $nextBusinessDay = local_date('Y-m-d', strtotime($now_date . ' +' . $i . ' Weekday'));
        }

        return $i;

    }

    public function is_holiday($gmtime, $without = []){
        if(!$gmtime) $gmtime = gmtime();
        $date          = local_date('Y-m-d', $gmtime);
        $yoho_holidays = $this->get_yoho_holidays('', false, $without);
        foreach ($yoho_holidays as $key => $value) {
            $holidays[] = $value['date'];
        }

        return in_array($date, $holidays);
    }

    /**
     * 檢查是否全大寫, 無中文.同時轉成首字大寫.
     */
    function validate($string){
        $pattern = "/[^A-Z0-9\s]/";
        if (preg_match($pattern, $string)) 
        {
            return $string;
        } else {
            $string = strtolower($string);
            $string = ucwords($string);
            return $string;
        }
    }

    /* https://github.com/TheGeekExplorer/GTIN-Validation */
    /* Check length of barcode for validity via the checkdigit calculation
     * We split the barcode into it's constituent digits, offset them into the GTIN
     * calculation tuple (x1, x3, x1, x3, x1, etc, etc), multiply the digits and add
     * them together, then modulo them on 10, and you get the calculated check digit.
     * For more information see GS1 website: https://www.gs1.org/check-digit-calculator
     * @param string gtin
     * @return bool */
    public function CheckGTIN ($gtin) {
        
        # Check that GTIN provided is a certain length
        if (!$this->CheckBasics($gtin))
            return false;
        
        # Define fixed variables
        $CheckDigitArray = [];
        $gtinMaths = [3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3];
        $modifier = 17 - (strlen($gtin) - 1);  // Gets the position to place first digit in array
        $gtinCheckDigit = substr($gtin, -1); // Get provided check digit
        $BarcodeArray = str_split($gtin);  // Split barcode at each digit into array
        $gtinLength = strlen($gtin);
        $tmpCheckDigit = 0;
        $tmpCheckSum = 0;
        $tmpMath = 0;
        
        # Run through and put digits into multiplication table
        for ($i=0; $i < ($gtinLength - 1); $i++) {
            $CheckDigitArray[$modifier + $i] = $BarcodeArray[$i];  // Add barcode digits to Multiplication Table
        }
        
        # Calculate "Sum" of barcode digits
        for ($i=$modifier; $i < 17; $i++) {
            $tmpCheckSum += ($CheckDigitArray[$i] * $gtinMaths[$i]);
        }
        
        # Difference from Rounded-Up-To-Nearest-10 - Fianl Check Digit Calculation
        $tmpCheckDigit = (ceil($tmpCheckSum / 10) * 10) - $tmpCheckSum;
        
        # Check if last digit is same as calculated check digit
        if ($gtinCheckDigit == $tmpCheckDigit)
            return true;
        return false;
    }

    /* Checks the length of the GTIN
        * @param string gtin
        * @return bool */
    public function CheckBasics ($gtin) {
        # Check length is ok
        if (strlen($gtin) < 8 || strlen($gtin) > 14)
            return false;
        
        # Check whether is a number
        preg_match("/\d+/", $gtin, $m, PREG_OFFSET_CAPTURE, 0);
        if (empty($m))
            return false;
        
        # Is valid, return true
        return true;
    }

    public function aync_get_curl($url_array, $threads = 10, $json_decode = true)
    {
        if (!is_array($url_array))
        return false;
        $total = count($url_array);
        $tmp_url = $url_array;
        $mh_pool = array();
        $total_time = 0;

        $mh = curl_multi_init(); // multi curl handler

        $opt = array ();
        $opt[CURLOPT_RETURNTRANSFER] = true;
        $opt[CURLOPT_TIMEOUT] = 10;
        $opt[CURLOPT_AUTOREFERER] = true;
        $opt[CURLOPT_HTTPHEADER] = array('Accept:application/json');
        $opt[CURLOPT_FOLLOWLOCATION] = true;
        $opt[CURLOPT_MAXREDIRS] = 10;

        if($total < $threads) $threads = $total;

        for($i=0;$i<$threads;$i++){
            $task = curl_init(array_pop($url_array));
            curl_setopt_array($task, $opt);
            curl_multi_add_handle($mh, $task);
            unset($task);
        }

        $index = 1;

        do{
            do{
                curl_multi_exec($mh, $running);
                if(curl_multi_select($mh, 1.0) > 0)
                    break;
            }while($running);

            while($info = curl_multi_info_read($mh)){
                $ch = $info['handle'];
                $url = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);

                $total_time += curl_getinfo($ch, CURLINFO_TOTAL_TIME);

                if($info['result'] == CURLE_OK){
                    $content = curl_multi_getcontent($ch);
                }
                $keys = array_keys($tmp_url, $url);
                foreach ($keys as $key) {
                    $data[$key] = ($json_decode)?json_decode($content, true):$content;
                }

                curl_multi_remove_handle($mh, $ch);
                curl_close($ch);
                unset($ch);

                if($url_array){
                    $new_task = curl_init(array_pop($url_array));
                    curl_setopt_array($new_task, $opt);
                    curl_multi_add_handle($mh, $new_task);
                    curl_multi_exec($mh, $running);
                }
                $index++;
            }

        }while($running);

        curl_multi_close($mh);
        ksort($data);
        return $data;
    }
    /*
     * CRUD Function
     */


    function generateActionTemplate($assign = [], $template = null)
    {
        global $smarty, $_LANG;

        $e = new \Exception();
        $trace = $e->getTrace();

        // Get action
        $action = strtolower(str_replace('Action', '', $trace[1]['function']));
        if(empty($assign)) $assign = $_GET;
        $assign['full_page']  = 1;
        $assign['act']        = $action ? $action : $_GET['act'];
        $assign['lang']       = $_LANG;
        if(empty($assign['ur_here'])) {
            $assign['ur_here'] = _L($this->className.'_'.$action, $action." ".$this->className);
        }
        if(empty($assign['action_link']) && empty($assign['action_link_list'])) {
            $file = end($trace)['file'];
            $file = end(explode("/", $file));
            $href = $file.'?act=add';
            $text = _L($this->className.'_add', '新增'.$this->className);
            $assign['action_link'] = array('href' => $href, 'text' => $text);
        }
        foreach($assign as $key => $value) {
            $smarty->assign($key, $value);
        }
        if(!$template) {
            $template = $this->className.'_'.$action.'.htm';
        }
        /* 显示商品列表页面 */
        assign_query_info();

        $smarty->display($template);
    }

    public function ajaxQueryAction($list, $totalCount, $action_btn = ['edit', 'remove'], $extraData = [])
    {
        @ini_set("memory_limit","256M");
        // We're using dataTables Ajax to query.
        if(empty($list)) $list = $this->tableList;

        $fields_mapping = $extraData['fields_mapping'];
        unset($extraData['fields_mapping']);
        
        if (!empty($_REQUEST['edit_lang']))
        {
            $list = localize_db_result_lang($_REQUEST['edit_lang'], $this->tableName, $list, $fields_mapping);
        }

        if(empty($totalCount)) $totalCount = $this->tableTotalCount;
        $draw = $_POST['draw'];
        if ($action_btn !== false) {
            $model = new Model\YohoBaseModel($this->tableName);
            $pk = $model->getPkey();
            foreach($list as $key => $value) {
                $value['_action'] = $this->generateCrudActionBtn($value[$pk], 'id', null, $action_btn);
                $list[$key] = $value;
            }
        }

        $info = [
            'draw'=> $draw,                 // ajax請求次數，作為標識符
            'recordsTotal'=>count($list),   // 獲取到的結果數(每頁顯示數量)
            'recordsFiltered'=>$totalCount, // 符合條件的總數據量
            'data'=>$list,                  // 獲取到的數據結果
            'extraData'=>$extraData,  
        ];
        //轉為json返回
        // header('Content-Type: application/json');
        // echo json_encode($info);
        // exit();
        include_once(ROOT_PATH . 'includes/cls_json.php');
        $json = new \JSON;
        $val = $json->encode($info);

        exit($val);
    }

    public function listAction($assign, $template)
    {
        $this->generateActionTemplate($assign, $template);
    }

    public function addAction()
    {
        $this->buildForm();
    }

    public function editAction()
    {
        $model = new Model\YohoBaseModel($this->tableName);
        $pk    = $model->getPkey();
        if($_REQUEST[$pk]) $id = $_REQUEST[$pk];
        else  $id = $_REQUEST['id'];
        $this->buildForm([], $id);
    }

    public function updateAction($need_return = false)
    {
        $act = $_REQUEST['act'];
        $model = new Model\YohoBaseModel($this->tableName);
        if($_REQUEST['_type']) {
            $types = stripslashes($_REQUEST['_type']);
            $types = json_decode(html_entity_decode($types), true);
            foreach($types as $key => $type) {
                $valid = $this->checkValueValid($_REQUEST[$key], $type);
                if($valid === true) {
                    $list[$key] = $this->handleValueType($_REQUEST[$key], $type);
                } else {
                    sys_msg($valid, 1);
                }
            }
        } else {
            $list = $_REQUEST;
        }

        $localizable_fields = $list;
        if($act == 'insert') {
            $id = $model->insert($list);
            if($id > 0) $success = true;
        } elseif($act == 'update') {
            $pk = $model->getPkey();
            $list[$pk] = $id = isset($_REQUEST[$pk]) ? $_REQUEST[$pk] : $_REQUEST['id'];
            if($list['id'] && !isset($list[$pk])) {
                $list[$pk] = $list['id'];
            }
            $localizable_fields = $this->checkRequireSimplied($this->tableName, $localizable_fields, $id);
            $update = $model->update($list);
            if($update > 0) $success = true;
        }

        // handle simplified chinese
        to_simplified_chinese($localizable_fields);
        if(!$success) sys_msg(_L('modify_failure', 'modify_failure'), 1);

        // Insert/Update success
        // Multiple language support
        save_localized_versions($this->tableName, $id);
        /* 清除缓存 */
        clear_cache_files();

        admin_log($this->tableName.': '.$id, ($act=='insert') ? 'add' : 'edit', $this->tableName);

        $e = new \Exception();
        $trace = $e->getTrace();
        $file = end($trace)['file'];
        $file = end(explode("/", $file));
        $href = $file.'?act=list';

        $link[0]['text'] = _L('back_list', '返回列表');
        $link[0]['href'] = $href;
        $note = _L('attradd_succed', '操作成功');

        if ($need_return == true) {
            return $id;
        }

        sys_msg($note, 0, $link);
    }

    public function buildForm($formMapper = [], $id = 0, $act = '', $assign = [], $redirect = '', $jsFile = '')
    {
        global $smarty;

        if($this->admin_priv)admin_priv($this->admin_priv);
        $old_act = $_REQUEST['act'];
        // Create act info
        if(empty($act) && empty($id)) $act = 'insert';
        else if(empty($act)) {
            if($old_act == 'add') $act = 'insert';
            else if($old_act == 'edit') $act = 'update';
        }

        // Default get Model
        if(empty($formMapper)) {
            $model  = new Model\YohoBaseModel($this->tableName);
            $list = $model->getFillable();
            foreach($list as $field) {
                $formMapper[$field] = ['label' =>_L($field, $field), 'type' => 'text', 'notice' => '', 'event' => [], 'required' => true];
            }
        }

        if(!empty($id) && empty($this->editData)) {
            $dataModel       = new Model\YohoBaseModel($this->tableName, $id);
            $this->editData  = $dataModel->getData();
        }

        // Default input setting
        $type = [];
        foreach($formMapper as $field => $input) {
            if(!$input['label'])     $input['label'] = _L($field, $field);
            if(!$input['type'])      $input['type']  = 'text';
            if(!$input['className']) $input['className']  = (in_array($input['type'], ['checkbox', 'radio'])) ? 'form-control flat' : 'form-control';
            else $input['className'] .= (in_array($input['type'], ['checkbox', 'radio'])) ? ' form-control flat' : ' form-control';
            if(!$input['value'] && !empty($this->editData)) $input['value'] = $this->editData[$field];
            if(empty($input['value']) && !empty($input['default'])) $input['value'] = $input['default'];
            $formMapper[$field] = $input;

            // Handle type
            if($input['type'] == 'radio') $type[$field] = 'toggle';
            else if($input['type'] == 'number') $type[$field] = 'number';
            else if(in_array($input['type'], ['daterange', 'date'])) $type[$field] = 'date';
            else $type[$field] = 'string';
        }
        $formMapper['_type'] = ['label' =>'', 'type' => 'hidden', 'value' => json_encode($type)];
        if(empty($assign['action_link']) && empty($assign['action_link_list'])) {
            $file = end($trace)['file'];
            $file = end(explode("/", $file));
            $href = $file.'?act=list';
            $text = _L($this->className.'_list', strtoupper($this->className)."列表");
            $assign['action_link'] = array('href' => $href, 'text' => $text);
        }
        $assign['ur_here'] = _L($old_act.'_'.$this->className, strtoupper($old_act." ".$this->className));
        $assign['form_action'] = $act;
        $assign['form_redirect'] = $redirect;
        $assign['form'] = $formMapper;

        // Multiple language support
        $localizable_fields = localizable_fields_for_table($this->tableName);
        $assign['localizable_fields'] = $localizable_fields;

        $assign['jsFile'] = $jsFile;

        if($id > 0) {
            $assign['id'] = $id;
            $assign['localized_versions'] = get_localized_versions($this->tableName, $id, $localizable_fields);
        }
        $assign['table_name'] = $this->tableName;

        $this->generateActionTemplate($assign, 'library/yoho_base_form.htm');
    }

    /**
     * $id  ID
     * $key id parm
     * $url PHP filename
     * $action default action
     * $custom_actions [action_name => ['icon_class','link', 'onclick', 'data', 'btn_class', 'btn_size', 'label', 'title', 'target']]
     */
    public function generateCrudActionBtn($id = 0, $key = 'id', $url = null, $actions = NULL, $custom_actions = [])
    {
        //require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/admin/common.php');
        if($actions === NULL) $actions = ['edit', 'remove'];
        if(empty($url)) $url = basename($_SERVER["PHP_SELF"]);

        $html = '';
        //$html .="<div class='btn-group  btn-group-sm'>";

        // Normal action:
        if(!empty($actions)) {
            foreach ($actions as $action) {
                $icon = "<i class='fa fa-".$action."'> </i>";
                if($action == 'view') $icon = "<i class='fa fa-eye'></i>";
                if($action == 'remove') $icon = "<i class='fa fa-trash'></i>";
                $link = $url."?act=".$action."&".$key."=".$id;
                $btn_class = 'btn-primary';
                if($action == 'edit')$btn_class = 'btn-info';
                else if($action == 'remove')$btn_class = 'btn-danger';
                $html .=
                    (($action != 'remove') ?"<a href='$link' class='btn $btn_class act_$action btn-xs' >$icon "._L($action, $action)."</a>":"<a href='#' class='btn $btn_class act_$action btn-xs' data-url='$link' onClick=\"cellRemove(this);\">$icon 刪除</a>"); //"._L($action, $action).";
            }
        }

        // Custom Action:
        foreach ($custom_actions as $action_name => $action) {

            // Icon setting
            if(empty($action['icon_class'])) $action['icon_class'] = 'fa fa-cog';
            $icon = "<i class='".$action['icon_class']."'> </i>";
            $data = '';
            // Href setting
            if(empty($action['link'])) {
                $link = $url."?act=".$action_name."&".$key."=".$id;
            } else {
                $link = $action['link'];
            }

            $onclick = '';
            if(!empty($action['onclick'])) {
                $onclick = $action['onclick'];
            }
            if($action['data']) {

                foreach($action['data'] as $dname => $dvalue)$data.="data-$dname=\"$dvalue\" ";
            }
            $target = (empty($action['target'])) ? '' : $action['target'];
            // Button class setting
            $btn_class = 'btn-default';
            if(!empty($action['btn_class'])) $btn_class = $action['btn_class'];
            if(empty($action['btn_size'])) $btn_size = 'btn-xs'; else $btn_size = $action['btn_size'];
            $html .= "<a title='$action[title]' target='$target' href='$link'".($onclick ? "onClick='$onclick'" : "").$data." class='btn $btn_size $btn_class act_$action_name'>$icon ".($action['label']?$action['label']:_L($action_name, ''))."</a>";
        }
        //$html .="</div>";

        return $html;
    }

    public function ajaxEditAction($tableName)
    {
        check_authz_json($this->admin_priv);
        $col   = $_REQUEST['col'];
        $value = $_REQUEST['value'];
        $key   = $_REQUEST['id'];
        $type  = $_REQUEST['type'];
        $edit_lang = empty($_REQUEST['edit_lang']) ? '' : trim($_REQUEST['edit_lang']);
        if(empty($tableName)) $tableName = $this->tableName;
        $valid = $this->checkValueValid($value, $type);

        if($valid === true) {
            $newValue = $this->handleValueType($value, $type);

            $model  = new Model\YohoBaseModel($tableName);

            $localizable_fields = localizable_fields_for_table($tableName);

            if (!$edit_lang || $edit_lang == default_language() || !in_array($col, $localizable_fields))
            {
                if (in_array($col, $localizable_fields)) {
                    localized_update($tableName, $key, array($col => translate_to_simplified_chinese($newValue)), "zh_cn");
                }
                $result = $model->update([$col=>$newValue], $key);
            }
            else
            {
                $result = localized_update($tableName, $key, array($col => $newValue), $edit_lang);
            }
            if($result != false) {
                clear_cache_files();
                admin_log($this->tableName.' PRIMARY KEY('.$key.") - ".$col.': '.$value, 'edit', $tableName);
                if($type == "price") $value = number_format($newValue, 2, '.', '');

                $value = stripslashes($value);
              
                make_json_result($value);
            } else {
                make_json_error('Error.');
            }

        } else {
            make_json_error($valid);
        }

    }

    public function checkValueValid($value, $type)
    {
        switch ($type) {
            case "date":
                $format='Y-m-d';
                $unixTime_1=local_strtotime($value);
                if(!is_numeric($unixTime_1)) return '日期格式錯誤';//如果不是数字格式，则直接返回
                $checkDate=local_date($format,$unixTime_1);
                $unixTime_2=local_strtotime($checkDate);
                if($unixTime_1==$unixTime_2){
                    return true;
                }else{
                    return '日期格式錯誤';
                }
                break;
            case "datetime":
                $format='Y-m-d H:i:s';
                $unixTime_1=local_strtotime($value);
                if(!is_numeric($unixTime_1)) return '日期格式錯誤';//如果不是数字格式，则直接返回
                $checkDate=local_date($format,$unixTime_1);
                $unixTime_2=local_strtotime($checkDate);
                if($unixTime_1==$unixTime_2){
                    return true;
                }else{
                    return '日期格式錯誤';
                }
                break;
            case "toggle":
                if($value != '0' && $value != '1') return '格式錯誤';
                return true;
                break;
            case "price":
                $value = $this->priceToFloat($value);
                if(!is_numeric($value) || floatval($value) < 0) return '您輸入了一個非法的數值';
                return true;
                break;
            case "integer":
                if(!is_numeric($value) || floatval($value) < 0) return '您輸入了一個非法的數值';
                return true;
                break;
            case "number":
                if(!is_numeric($value)) return '您輸入了一個非法的數值';
                return true;
                break;
            default:
                return true;
                break;
        }
    }

    public function handleValueType($value, $type)
    {
        switch ($type) {
            case "date":
                return local_strtotime($value);
                break;
            case "datetime":
                return local_strtotime($value);
                break;
            case "price":
                $value = $this->priceToFloat($value);
                return number_format($value, 2, '.', '');
                break;
            case "integer":
                return intval($value);
                break;
            case "number":
                return intval($value);
                break;
            default:
                return $value;
                break;
        }
    }

    function priceToFloat($s)
    {
        // convert "," to ""
        $s = str_replace(',', '', $s);
        // remove all but numbers "."
        $s = preg_replace("/[^0-9\.]/", "", $s);

        // return float
        return (float) $s;
    }

    function get_today_info()
    {
        $yoho_holidays = $this->get_yoho_holidays();
        foreach ($yoho_holidays as $key => $value) {
            $holiday_list[] = $value['date'];
        }
        $now = time();
        $today = date('Y-m-d', $now);
        $today_dow = date('w', $now); // 0 = Sunday, 6 = Saturday
        $tomorrow = date('Y-m-d', $now+86400);
        $tomorrow_dow = date('w', $now+86400); // 0 = Sunday, 6 = Saturday
        $holiday = (in_array($today, $holiday_list));
        $before_holiday = (in_array($tomorrow, $holiday_list));
        $hour = date('H', $now);
        return compact('holiday','before_holiday','hour');
    }


    /**
     * Using to generate verify code
     */
    public function generateVerifyCode($verify_info)
    {
        global $ecs, $db;

        $count = -1;
        do {
            $code = substr(md5(rand()),0,6);
            $sql = "SELECT count(*) FROM ".$ecs->table('verify_code')." WHERE verify_code = '$code' AND status NOT IN ('".VERIFY_CODE_FINISH."') ";
            $count = $db->getOne($sql);

        } while ($count != 0);

        $verify_info['verify_code'] = $code;
        $verify_info['create_at']   = gmtime();
        $verify_info['status']      = VERIFY_CODE_UNVERIFY;
        $model = new Model\YohoBaseModel('verify_code');
        $id = $model->insert($verify_info);
        if($id > 0) return ['id' => $id, 'code' => $code];
        else return false;
    }

    /**
     * Using to verify code
     */
    public function verifyCode($code, $target, $user_id)
    {
        global $ecs, $db;
        $now = gmtime();
        $sql = "SELECT * FROM ".$ecs->table('verify_code')." WHERE verify_target = '$target' AND verify_code = '$code' AND status NOT IN ('".VERIFY_CODE_FINISH."') LIMIT 1 ";
        $verify_info = $db->getRow($sql);
        if(!$code)return ['error' => 1, 'message' => 'code_error' ];
        if($verify_info && $verify_info['user_id'] != $user_id) { // session[user_id] != db user_id
            return ['error' => 1, 'message' => 'user_not_match' ];
        } elseif($verify_info && $verify_info['verify_code'] == $code) { // Verify is true
            if($verify_info['verify_type'] == self::VERIFY_TYPE_SMS) {
                $expire = $verify_info['create_at'] + 180;
                if($expire > $now) {
                    $db->query("UPDATE ".$ecs->table('verify_code')." SET status = ".VERIFY_CODE_FINISH." WHERE verify_id = ".$verify_info['verify_id']);
                    return ['error' => 0 ];
                } else {
                    return ['error' => 1, 'message' => 'over_time' ];
                }
            } else {
                $db->query("UPDATE ".$ecs->table('verify_code')." SET status = ".VERIFY_CODE_FINISH." WHERE verify_id = ".$verify_info['verify_id']);
                return ['error' => 0 ];
            }
        } else {
            return ['error' => 1, 'message' => 'code_error' ];
        }
    }

    /**
     * @param $path
     * @param string $name
     * @return string
     */
    public function svgIconContent($path, $name  = '')
    {
        $path = str_replace('../', ROOT_PATH, $path);
        $svg = file_get_contents($path);
        if(empty($svg)) return  '';

        $svgTemplate = new \SimpleXMLElement($svg);
        $svgTemplate->registerXPathNamespace('svg', 'http://www.w3.org/2000/svg');

        // How to retrieve the style string
        $style = $svgTemplate->xpath('svg:style');
        $tmp = explode("\n", $style[0]->asXml());
//        var_dump($tmp);
        unset($tmp[0]);
        array_pop($tmp);
        foreach ($tmp as $key => $style) {
            $st = strpos($style, '.st');
            if($st === false) unset($tmp[$key]);
        }
        $new_style = implode("\n", $tmp);
        $new_style = preg_replace("/.st[0-9]+/", "#$name $0",$new_style);
        // How to set the style string
        $children = $svgTemplate->children('svg', true);
        $svgTemplate->attributes()->id = $name;
        $children->style = $new_style;
        if(count($tmp) >= 3) {
            foreach ($svgTemplate->xpath("//*[@class='st0']") as $value) {
                $value->attributes()->class = 'st0 background';
            }
            foreach ($svgTemplate->xpath("//*[@class='st1']") as $value) {
                $value->attributes()->class = 'st1 background';
            }
            foreach ($svgTemplate->xpath("//*[@class='st2']") as $value) {
                $value->attributes()->class = 'st2 border';
            }
        } elseif (count($tmp) == 2 ) {
            foreach ($svgTemplate->xpath("//*[@class='st0']") as $value) {
                $value->attributes()->class = 'st0 border';
            }
            foreach ($svgTemplate->xpath("//*[@class='st1']") as $value) {
                $value->attributes()->class = 'st1 border';
            }
        }
        $svg_icon = $svgTemplate->asXML();
        return $svg_icon;
    }
    
    function display_xml_error($error, $xml)
    {
        $return  = $xml[$error->line - 1] . "\n";
        $return .= str_repeat('-', $error->column) . "^\n";
        
        switch ($error->level) {
            case LIBXML_ERR_WARNING:
                $return .= "Warning $error->code: ";
                break;
            case LIBXML_ERR_ERROR:
                $return .= "Error $error->code: ";
                break;
            case LIBXML_ERR_FATAL:
                $return .= "Fatal Error $error->code: ";
                break;
        }
        
        $return .= trim($error->message) .
            "\n  Line: $error->line" .
            "\n  Column: $error->column";
        
        if ($error->file) {
            $return .= "\n  File: $error->file";
        }
        
        return "$return\n\n--------------------------------------------\n\n";
    }
    
    function genRatingSvg($star = 0)
    {
        if(!$star) return '';
        $width = ($star / 5) * 100;

        $html = '<div class="goodsColRating jq-ry-container"><div class="jq-ry-group-wrapper">';
        $html .= '<div class="jq-ry-normal-group jq-ry-group">
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" style="enable-background:new 0 0 40 40;" xml:space="preserve" width="15px" height="15px"><style type="text/css"></style><path class="st0" d="M22.8,4.6l4.1,8.4c0.3,0.6,0.8,1,1.5,1.1l9.2,1.3c1.6,0.2,2.3,2.2,1.1,3.4L32,25.3c-0.5,0.5-0.7,1.1-0.6,1.7 l1.6,9.2c0.3,1.6-1.4,2.8-2.9,2.1l-8.3-4.3c-0.6-0.3-1.3-0.3-1.8,0l-8.3,4.3C10.4,39,8.7,37.8,9,36.2l1.6-9.2 c0.1-0.6-0.1-1.3-0.6-1.7l-6.7-6.5c-1.2-1.1-0.5-3.1,1.1-3.4l9.2-1.3c0.6-0.1,1.2-0.5,1.5-1.1l4.1-8.4C20,3.1,22,3.1,22.8,4.6z" fill="#E7E3E1"></path><path class="st1" d="M10.9,40c-0.7,0-1.4-0.2-2-0.7c-1.1-0.8-1.6-2.1-1.4-3.4l1.6-9.2c0-0.2,0-0.3-0.1-0.4l-6.7-6.5 c-1-0.9-1.3-2.3-0.9-3.6c0.4-1.3,1.5-2.2,2.8-2.4l9.2-1.3c0.2,0,0.3-0.1,0.4-0.3l4.1-8.4C18.5,2.7,19.7,2,21,2 c1.3,0,2.5,0.7,3.1,1.9l4.1,8.4c0.1,0.1,0.2,0.2,0.4,0.3l9.2,1.3c1.3,0.2,2.4,1.1,2.8,2.4c0.4,1.3,0.1,2.6-0.9,3.6l-6.7,6.5 c-0.1,0.1-0.2,0.3-0.1,0.4l1.6,9.2c0.2,1.3-0.3,2.6-1.4,3.4c-1.1,0.8-2.5,0.9-3.7,0.3l-8.3-4.3c-0.1-0.1-0.3-0.1-0.4,0l-8.3,4.3 C12,39.9,11.5,40,10.9,40z M21,32.2c0.6,0,1.1,0.1,1.6,0.4l8.3,4.3c0.2,0.1,0.4,0,0.5,0c0.1-0.1,0.2-0.2,0.2-0.5L30,27.3 c-0.2-1.1,0.2-2.3,1-3.1l6.7-6.5c0.2-0.2,0.2-0.4,0.1-0.5c0-0.1-0.1-0.3-0.4-0.3l-9.2-1.3c-1.1-0.2-2.1-0.9-2.6-1.9l-4.1-8.4 C21.3,5,21.1,5,21,5c-0.1,0-0.3,0-0.4,0.3l-4.1,8.4c-0.5,1-1.5,1.7-2.6,1.9l-9.2,1.3c-0.3,0-0.3,0.2-0.4,0.3c0,0.1-0.1,0.3,0.1,0.5 l6.7,6.5c0.8,0.8,1.2,1.9,1,3.1l-1.6,9.2c0,0.3,0.1,0.4,0.2,0.5c0.1,0.1,0.3,0.2,0.5,0l8.3-4.3C19.9,32.3,20.4,32.2,21,32.2z" fill="#CECAC8"></path></svg>
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" style="margin-left: 5px;" xml:space="preserve" width="15px" height="15px"><style type="text/css"></style><path class="st0" d="M22.8,4.6l4.1,8.4c0.3,0.6,0.8,1,1.5,1.1l9.2,1.3c1.6,0.2,2.3,2.2,1.1,3.4L32,25.3c-0.5,0.5-0.7,1.1-0.6,1.7 l1.6,9.2c0.3,1.6-1.4,2.8-2.9,2.1l-8.3-4.3c-0.6-0.3-1.3-0.3-1.8,0l-8.3,4.3C10.4,39,8.7,37.8,9,36.2l1.6-9.2 c0.1-0.6-0.1-1.3-0.6-1.7l-6.7-6.5c-1.2-1.1-0.5-3.1,1.1-3.4l9.2-1.3c0.6-0.1,1.2-0.5,1.5-1.1l4.1-8.4C20,3.1,22,3.1,22.8,4.6z" fill="#E7E3E1"></path><path class="st1" d="M10.9,40c-0.7,0-1.4-0.2-2-0.7c-1.1-0.8-1.6-2.1-1.4-3.4l1.6-9.2c0-0.2,0-0.3-0.1-0.4l-6.7-6.5 c-1-0.9-1.3-2.3-0.9-3.6c0.4-1.3,1.5-2.2,2.8-2.4l9.2-1.3c0.2,0,0.3-0.1,0.4-0.3l4.1-8.4C18.5,2.7,19.7,2,21,2 c1.3,0,2.5,0.7,3.1,1.9l4.1,8.4c0.1,0.1,0.2,0.2,0.4,0.3l9.2,1.3c1.3,0.2,2.4,1.1,2.8,2.4c0.4,1.3,0.1,2.6-0.9,3.6l-6.7,6.5 c-0.1,0.1-0.2,0.3-0.1,0.4l1.6,9.2c0.2,1.3-0.3,2.6-1.4,3.4c-1.1,0.8-2.5,0.9-3.7,0.3l-8.3-4.3c-0.1-0.1-0.3-0.1-0.4,0l-8.3,4.3 C12,39.9,11.5,40,10.9,40z M21,32.2c0.6,0,1.1,0.1,1.6,0.4l8.3,4.3c0.2,0.1,0.4,0,0.5,0c0.1-0.1,0.2-0.2,0.2-0.5L30,27.3 c-0.2-1.1,0.2-2.3,1-3.1l6.7-6.5c0.2-0.2,0.2-0.4,0.1-0.5c0-0.1-0.1-0.3-0.4-0.3l-9.2-1.3c-1.1-0.2-2.1-0.9-2.6-1.9l-4.1-8.4 C21.3,5,21.1,5,21,5c-0.1,0-0.3,0-0.4,0.3l-4.1,8.4c-0.5,1-1.5,1.7-2.6,1.9l-9.2,1.3c-0.3,0-0.3,0.2-0.4,0.3c0,0.1-0.1,0.3,0.1,0.5 l6.7,6.5c0.8,0.8,1.2,1.9,1,3.1l-1.6,9.2c0,0.3,0.1,0.4,0.2,0.5c0.1,0.1,0.3,0.2,0.5,0l8.3-4.3C19.9,32.3,20.4,32.2,21,32.2z" fill="#CECAC8"></path></svg>
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" style="margin-left: 5px;" xml:space="preserve" width="15px" height="15px"><style type="text/css"></style><path class="st0" d="M22.8,4.6l4.1,8.4c0.3,0.6,0.8,1,1.5,1.1l9.2,1.3c1.6,0.2,2.3,2.2,1.1,3.4L32,25.3c-0.5,0.5-0.7,1.1-0.6,1.7 l1.6,9.2c0.3,1.6-1.4,2.8-2.9,2.1l-8.3-4.3c-0.6-0.3-1.3-0.3-1.8,0l-8.3,4.3C10.4,39,8.7,37.8,9,36.2l1.6-9.2 c0.1-0.6-0.1-1.3-0.6-1.7l-6.7-6.5c-1.2-1.1-0.5-3.1,1.1-3.4l9.2-1.3c0.6-0.1,1.2-0.5,1.5-1.1l4.1-8.4C20,3.1,22,3.1,22.8,4.6z" fill="#E7E3E1"></path><path class="st1" d="M10.9,40c-0.7,0-1.4-0.2-2-0.7c-1.1-0.8-1.6-2.1-1.4-3.4l1.6-9.2c0-0.2,0-0.3-0.1-0.4l-6.7-6.5 c-1-0.9-1.3-2.3-0.9-3.6c0.4-1.3,1.5-2.2,2.8-2.4l9.2-1.3c0.2,0,0.3-0.1,0.4-0.3l4.1-8.4C18.5,2.7,19.7,2,21,2 c1.3,0,2.5,0.7,3.1,1.9l4.1,8.4c0.1,0.1,0.2,0.2,0.4,0.3l9.2,1.3c1.3,0.2,2.4,1.1,2.8,2.4c0.4,1.3,0.1,2.6-0.9,3.6l-6.7,6.5 c-0.1,0.1-0.2,0.3-0.1,0.4l1.6,9.2c0.2,1.3-0.3,2.6-1.4,3.4c-1.1,0.8-2.5,0.9-3.7,0.3l-8.3-4.3c-0.1-0.1-0.3-0.1-0.4,0l-8.3,4.3 C12,39.9,11.5,40,10.9,40z M21,32.2c0.6,0,1.1,0.1,1.6,0.4l8.3,4.3c0.2,0.1,0.4,0,0.5,0c0.1-0.1,0.2-0.2,0.2-0.5L30,27.3 c-0.2-1.1,0.2-2.3,1-3.1l6.7-6.5c0.2-0.2,0.2-0.4,0.1-0.5c0-0.1-0.1-0.3-0.4-0.3l-9.2-1.3c-1.1-0.2-2.1-0.9-2.6-1.9l-4.1-8.4 C21.3,5,21.1,5,21,5c-0.1,0-0.3,0-0.4,0.3l-4.1,8.4c-0.5,1-1.5,1.7-2.6,1.9l-9.2,1.3c-0.3,0-0.3,0.2-0.4,0.3c0,0.1-0.1,0.3,0.1,0.5 l6.7,6.5c0.8,0.8,1.2,1.9,1,3.1l-1.6,9.2c0,0.3,0.1,0.4,0.2,0.5c0.1,0.1,0.3,0.2,0.5,0l8.3-4.3C19.9,32.3,20.4,32.2,21,32.2z" fill="#CECAC8"></path></svg>
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" style="margin-left: 5px;" xml:space="preserve" width="15px" height="15px"><style type="text/css"></style><path class="st0" d="M22.8,4.6l4.1,8.4c0.3,0.6,0.8,1,1.5,1.1l9.2,1.3c1.6,0.2,2.3,2.2,1.1,3.4L32,25.3c-0.5,0.5-0.7,1.1-0.6,1.7 l1.6,9.2c0.3,1.6-1.4,2.8-2.9,2.1l-8.3-4.3c-0.6-0.3-1.3-0.3-1.8,0l-8.3,4.3C10.4,39,8.7,37.8,9,36.2l1.6-9.2 c0.1-0.6-0.1-1.3-0.6-1.7l-6.7-6.5c-1.2-1.1-0.5-3.1,1.1-3.4l9.2-1.3c0.6-0.1,1.2-0.5,1.5-1.1l4.1-8.4C20,3.1,22,3.1,22.8,4.6z" fill="#E7E3E1"></path><path class="st1" d="M10.9,40c-0.7,0-1.4-0.2-2-0.7c-1.1-0.8-1.6-2.1-1.4-3.4l1.6-9.2c0-0.2,0-0.3-0.1-0.4l-6.7-6.5 c-1-0.9-1.3-2.3-0.9-3.6c0.4-1.3,1.5-2.2,2.8-2.4l9.2-1.3c0.2,0,0.3-0.1,0.4-0.3l4.1-8.4C18.5,2.7,19.7,2,21,2 c1.3,0,2.5,0.7,3.1,1.9l4.1,8.4c0.1,0.1,0.2,0.2,0.4,0.3l9.2,1.3c1.3,0.2,2.4,1.1,2.8,2.4c0.4,1.3,0.1,2.6-0.9,3.6l-6.7,6.5 c-0.1,0.1-0.2,0.3-0.1,0.4l1.6,9.2c0.2,1.3-0.3,2.6-1.4,3.4c-1.1,0.8-2.5,0.9-3.7,0.3l-8.3-4.3c-0.1-0.1-0.3-0.1-0.4,0l-8.3,4.3 C12,39.9,11.5,40,10.9,40z M21,32.2c0.6,0,1.1,0.1,1.6,0.4l8.3,4.3c0.2,0.1,0.4,0,0.5,0c0.1-0.1,0.2-0.2,0.2-0.5L30,27.3 c-0.2-1.1,0.2-2.3,1-3.1l6.7-6.5c0.2-0.2,0.2-0.4,0.1-0.5c0-0.1-0.1-0.3-0.4-0.3l-9.2-1.3c-1.1-0.2-2.1-0.9-2.6-1.9l-4.1-8.4 C21.3,5,21.1,5,21,5c-0.1,0-0.3,0-0.4,0.3l-4.1,8.4c-0.5,1-1.5,1.7-2.6,1.9l-9.2,1.3c-0.3,0-0.3,0.2-0.4,0.3c0,0.1-0.1,0.3,0.1,0.5 l6.7,6.5c0.8,0.8,1.2,1.9,1,3.1l-1.6,9.2c0,0.3,0.1,0.4,0.2,0.5c0.1,0.1,0.3,0.2,0.5,0l8.3-4.3C19.9,32.3,20.4,32.2,21,32.2z" fill="#CECAC8"></path></svg>
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" style="margin-left: 5px;" xml:space="preserve" width="15px" height="15px"><style type="text/css"></style><path class="st0" d="M22.8,4.6l4.1,8.4c0.3,0.6,0.8,1,1.5,1.1l9.2,1.3c1.6,0.2,2.3,2.2,1.1,3.4L32,25.3c-0.5,0.5-0.7,1.1-0.6,1.7 l1.6,9.2c0.3,1.6-1.4,2.8-2.9,2.1l-8.3-4.3c-0.6-0.3-1.3-0.3-1.8,0l-8.3,4.3C10.4,39,8.7,37.8,9,36.2l1.6-9.2 c0.1-0.6-0.1-1.3-0.6-1.7l-6.7-6.5c-1.2-1.1-0.5-3.1,1.1-3.4l9.2-1.3c0.6-0.1,1.2-0.5,1.5-1.1l4.1-8.4C20,3.1,22,3.1,22.8,4.6z" fill="#E7E3E1"></path><path class="st1" d="M10.9,40c-0.7,0-1.4-0.2-2-0.7c-1.1-0.8-1.6-2.1-1.4-3.4l1.6-9.2c0-0.2,0-0.3-0.1-0.4l-6.7-6.5 c-1-0.9-1.3-2.3-0.9-3.6c0.4-1.3,1.5-2.2,2.8-2.4l9.2-1.3c0.2,0,0.3-0.1,0.4-0.3l4.1-8.4C18.5,2.7,19.7,2,21,2 c1.3,0,2.5,0.7,3.1,1.9l4.1,8.4c0.1,0.1,0.2,0.2,0.4,0.3l9.2,1.3c1.3,0.2,2.4,1.1,2.8,2.4c0.4,1.3,0.1,2.6-0.9,3.6l-6.7,6.5 c-0.1,0.1-0.2,0.3-0.1,0.4l1.6,9.2c0.2,1.3-0.3,2.6-1.4,3.4c-1.1,0.8-2.5,0.9-3.7,0.3l-8.3-4.3c-0.1-0.1-0.3-0.1-0.4,0l-8.3,4.3 C12,39.9,11.5,40,10.9,40z M21,32.2c0.6,0,1.1,0.1,1.6,0.4l8.3,4.3c0.2,0.1,0.4,0,0.5,0c0.1-0.1,0.2-0.2,0.2-0.5L30,27.3 c-0.2-1.1,0.2-2.3,1-3.1l6.7-6.5c0.2-0.2,0.2-0.4,0.1-0.5c0-0.1-0.1-0.3-0.4-0.3l-9.2-1.3c-1.1-0.2-2.1-0.9-2.6-1.9l-4.1-8.4 C21.3,5,21.1,5,21,5c-0.1,0-0.3,0-0.4,0.3l-4.1,8.4c-0.5,1-1.5,1.7-2.6,1.9l-9.2,1.3c-0.3,0-0.3,0.2-0.4,0.3c0,0.1-0.1,0.3,0.1,0.5 l6.7,6.5c0.8,0.8,1.2,1.9,1,3.1l-1.6,9.2c0,0.3,0.1,0.4,0.2,0.5c0.1,0.1,0.3,0.2,0.5,0l8.3-4.3C19.9,32.3,20.4,32.2,21,32.2z" fill="#CECAC8"></path></svg>
        </div>
        <div class="jq-ry-rated-group jq-ry-group" style="width:'.$width.'%;z-index: 11;position: absolute;top: 0;left: 0;overflow: hidden;">
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" style="enable-background:new 0 0 40 40;" xml:space="preserve" width="15px" height="15px"><style type="text/css"></style><path class="st0" d="M22.8,4.6l4.1,8.4c0.3,0.6,0.8,1,1.5,1.1l9.2,1.3c1.6,0.2,2.3,2.2,1.1,3.4L32,25.3c-0.5,0.5-0.7,1.1-0.6,1.7 l1.6,9.2c0.3,1.6-1.4,2.8-2.9,2.1l-8.3-4.3c-0.6-0.3-1.3-0.3-1.8,0l-8.3,4.3C10.4,39,8.7,37.8,9,36.2l1.6-9.2 c0.1-0.6-0.1-1.3-0.6-1.7l-6.7-6.5c-1.2-1.1-0.5-3.1,1.1-3.4l9.2-1.3c0.6-0.1,1.2-0.5,1.5-1.1l4.1-8.4C20,3.1,22,3.1,22.8,4.6z" fill="#FEDA00"></path><path class="st1" d="M10.9,40c-0.7,0-1.4-0.2-2-0.7c-1.1-0.8-1.6-2.1-1.4-3.4l1.6-9.2c0-0.2,0-0.3-0.1-0.4l-6.7-6.5 c-1-0.9-1.3-2.3-0.9-3.6c0.4-1.3,1.5-2.2,2.8-2.4l9.2-1.3c0.2,0,0.3-0.1,0.4-0.3l4.1-8.4C18.5,2.7,19.7,2,21,2 c1.3,0,2.5,0.7,3.1,1.9l4.1,8.4c0.1,0.1,0.2,0.2,0.4,0.3l9.2,1.3c1.3,0.2,2.4,1.1,2.8,2.4c0.4,1.3,0.1,2.6-0.9,3.6l-6.7,6.5 c-0.1,0.1-0.2,0.3-0.1,0.4l1.6,9.2c0.2,1.3-0.3,2.6-1.4,3.4c-1.1,0.8-2.5,0.9-3.7,0.3l-8.3-4.3c-0.1-0.1-0.3-0.1-0.4,0l-8.3,4.3 C12,39.9,11.5,40,10.9,40z M21,32.2c0.6,0,1.1,0.1,1.6,0.4l8.3,4.3c0.2,0.1,0.4,0,0.5,0c0.1-0.1,0.2-0.2,0.2-0.5L30,27.3 c-0.2-1.1,0.2-2.3,1-3.1l6.7-6.5c0.2-0.2,0.2-0.4,0.1-0.5c0-0.1-0.1-0.3-0.4-0.3l-9.2-1.3c-1.1-0.2-2.1-0.9-2.6-1.9l-4.1-8.4 C21.3,5,21.1,5,21,5c-0.1,0-0.3,0-0.4,0.3l-4.1,8.4c-0.5,1-1.5,1.7-2.6,1.9l-9.2,1.3c-0.3,0-0.3,0.2-0.4,0.3c0,0.1-0.1,0.3,0.1,0.5 l6.7,6.5c0.8,0.8,1.2,1.9,1,3.1l-1.6,9.2c0,0.3,0.1,0.4,0.2,0.5c0.1,0.1,0.3,0.2,0.5,0l8.3-4.3C19.9,32.3,20.4,32.2,21,32.2z" fill="#EC9022"></path></svg>
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" style="margin-left: 5px;" xml:space="preserve" width="15px" height="15px"><style type="text/css"></style><path class="st0" d="M22.8,4.6l4.1,8.4c0.3,0.6,0.8,1,1.5,1.1l9.2,1.3c1.6,0.2,2.3,2.2,1.1,3.4L32,25.3c-0.5,0.5-0.7,1.1-0.6,1.7 l1.6,9.2c0.3,1.6-1.4,2.8-2.9,2.1l-8.3-4.3c-0.6-0.3-1.3-0.3-1.8,0l-8.3,4.3C10.4,39,8.7,37.8,9,36.2l1.6-9.2 c0.1-0.6-0.1-1.3-0.6-1.7l-6.7-6.5c-1.2-1.1-0.5-3.1,1.1-3.4l9.2-1.3c0.6-0.1,1.2-0.5,1.5-1.1l4.1-8.4C20,3.1,22,3.1,22.8,4.6z" fill="#FEDA00"></path><path class="st1" d="M10.9,40c-0.7,0-1.4-0.2-2-0.7c-1.1-0.8-1.6-2.1-1.4-3.4l1.6-9.2c0-0.2,0-0.3-0.1-0.4l-6.7-6.5 c-1-0.9-1.3-2.3-0.9-3.6c0.4-1.3,1.5-2.2,2.8-2.4l9.2-1.3c0.2,0,0.3-0.1,0.4-0.3l4.1-8.4C18.5,2.7,19.7,2,21,2 c1.3,0,2.5,0.7,3.1,1.9l4.1,8.4c0.1,0.1,0.2,0.2,0.4,0.3l9.2,1.3c1.3,0.2,2.4,1.1,2.8,2.4c0.4,1.3,0.1,2.6-0.9,3.6l-6.7,6.5 c-0.1,0.1-0.2,0.3-0.1,0.4l1.6,9.2c0.2,1.3-0.3,2.6-1.4,3.4c-1.1,0.8-2.5,0.9-3.7,0.3l-8.3-4.3c-0.1-0.1-0.3-0.1-0.4,0l-8.3,4.3 C12,39.9,11.5,40,10.9,40z M21,32.2c0.6,0,1.1,0.1,1.6,0.4l8.3,4.3c0.2,0.1,0.4,0,0.5,0c0.1-0.1,0.2-0.2,0.2-0.5L30,27.3 c-0.2-1.1,0.2-2.3,1-3.1l6.7-6.5c0.2-0.2,0.2-0.4,0.1-0.5c0-0.1-0.1-0.3-0.4-0.3l-9.2-1.3c-1.1-0.2-2.1-0.9-2.6-1.9l-4.1-8.4 C21.3,5,21.1,5,21,5c-0.1,0-0.3,0-0.4,0.3l-4.1,8.4c-0.5,1-1.5,1.7-2.6,1.9l-9.2,1.3c-0.3,0-0.3,0.2-0.4,0.3c0,0.1-0.1,0.3,0.1,0.5 l6.7,6.5c0.8,0.8,1.2,1.9,1,3.1l-1.6,9.2c0,0.3,0.1,0.4,0.2,0.5c0.1,0.1,0.3,0.2,0.5,0l8.3-4.3C19.9,32.3,20.4,32.2,21,32.2z" fill="#EC9022"></path></svg>
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" style="margin-left: 5px;" xml:space="preserve" width="15px" height="15px"><style type="text/css"></style><path class="st0" d="M22.8,4.6l4.1,8.4c0.3,0.6,0.8,1,1.5,1.1l9.2,1.3c1.6,0.2,2.3,2.2,1.1,3.4L32,25.3c-0.5,0.5-0.7,1.1-0.6,1.7 l1.6,9.2c0.3,1.6-1.4,2.8-2.9,2.1l-8.3-4.3c-0.6-0.3-1.3-0.3-1.8,0l-8.3,4.3C10.4,39,8.7,37.8,9,36.2l1.6-9.2 c0.1-0.6-0.1-1.3-0.6-1.7l-6.7-6.5c-1.2-1.1-0.5-3.1,1.1-3.4l9.2-1.3c0.6-0.1,1.2-0.5,1.5-1.1l4.1-8.4C20,3.1,22,3.1,22.8,4.6z" fill="#FEDA00"></path><path class="st1" d="M10.9,40c-0.7,0-1.4-0.2-2-0.7c-1.1-0.8-1.6-2.1-1.4-3.4l1.6-9.2c0-0.2,0-0.3-0.1-0.4l-6.7-6.5 c-1-0.9-1.3-2.3-0.9-3.6c0.4-1.3,1.5-2.2,2.8-2.4l9.2-1.3c0.2,0,0.3-0.1,0.4-0.3l4.1-8.4C18.5,2.7,19.7,2,21,2 c1.3,0,2.5,0.7,3.1,1.9l4.1,8.4c0.1,0.1,0.2,0.2,0.4,0.3l9.2,1.3c1.3,0.2,2.4,1.1,2.8,2.4c0.4,1.3,0.1,2.6-0.9,3.6l-6.7,6.5 c-0.1,0.1-0.2,0.3-0.1,0.4l1.6,9.2c0.2,1.3-0.3,2.6-1.4,3.4c-1.1,0.8-2.5,0.9-3.7,0.3l-8.3-4.3c-0.1-0.1-0.3-0.1-0.4,0l-8.3,4.3 C12,39.9,11.5,40,10.9,40z M21,32.2c0.6,0,1.1,0.1,1.6,0.4l8.3,4.3c0.2,0.1,0.4,0,0.5,0c0.1-0.1,0.2-0.2,0.2-0.5L30,27.3 c-0.2-1.1,0.2-2.3,1-3.1l6.7-6.5c0.2-0.2,0.2-0.4,0.1-0.5c0-0.1-0.1-0.3-0.4-0.3l-9.2-1.3c-1.1-0.2-2.1-0.9-2.6-1.9l-4.1-8.4 C21.3,5,21.1,5,21,5c-0.1,0-0.3,0-0.4,0.3l-4.1,8.4c-0.5,1-1.5,1.7-2.6,1.9l-9.2,1.3c-0.3,0-0.3,0.2-0.4,0.3c0,0.1-0.1,0.3,0.1,0.5 l6.7,6.5c0.8,0.8,1.2,1.9,1,3.1l-1.6,9.2c0,0.3,0.1,0.4,0.2,0.5c0.1,0.1,0.3,0.2,0.5,0l8.3-4.3C19.9,32.3,20.4,32.2,21,32.2z" fill="#EC9022"></path></svg>
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" style="margin-left: 5px;" xml:space="preserve" width="15px" height="15px"><style type="text/css"></style><path class="st0" d="M22.8,4.6l4.1,8.4c0.3,0.6,0.8,1,1.5,1.1l9.2,1.3c1.6,0.2,2.3,2.2,1.1,3.4L32,25.3c-0.5,0.5-0.7,1.1-0.6,1.7 l1.6,9.2c0.3,1.6-1.4,2.8-2.9,2.1l-8.3-4.3c-0.6-0.3-1.3-0.3-1.8,0l-8.3,4.3C10.4,39,8.7,37.8,9,36.2l1.6-9.2 c0.1-0.6-0.1-1.3-0.6-1.7l-6.7-6.5c-1.2-1.1-0.5-3.1,1.1-3.4l9.2-1.3c0.6-0.1,1.2-0.5,1.5-1.1l4.1-8.4C20,3.1,22,3.1,22.8,4.6z" fill="#FEDA00"></path><path class="st1" d="M10.9,40c-0.7,0-1.4-0.2-2-0.7c-1.1-0.8-1.6-2.1-1.4-3.4l1.6-9.2c0-0.2,0-0.3-0.1-0.4l-6.7-6.5 c-1-0.9-1.3-2.3-0.9-3.6c0.4-1.3,1.5-2.2,2.8-2.4l9.2-1.3c0.2,0,0.3-0.1,0.4-0.3l4.1-8.4C18.5,2.7,19.7,2,21,2 c1.3,0,2.5,0.7,3.1,1.9l4.1,8.4c0.1,0.1,0.2,0.2,0.4,0.3l9.2,1.3c1.3,0.2,2.4,1.1,2.8,2.4c0.4,1.3,0.1,2.6-0.9,3.6l-6.7,6.5 c-0.1,0.1-0.2,0.3-0.1,0.4l1.6,9.2c0.2,1.3-0.3,2.6-1.4,3.4c-1.1,0.8-2.5,0.9-3.7,0.3l-8.3-4.3c-0.1-0.1-0.3-0.1-0.4,0l-8.3,4.3 C12,39.9,11.5,40,10.9,40z M21,32.2c0.6,0,1.1,0.1,1.6,0.4l8.3,4.3c0.2,0.1,0.4,0,0.5,0c0.1-0.1,0.2-0.2,0.2-0.5L30,27.3 c-0.2-1.1,0.2-2.3,1-3.1l6.7-6.5c0.2-0.2,0.2-0.4,0.1-0.5c0-0.1-0.1-0.3-0.4-0.3l-9.2-1.3c-1.1-0.2-2.1-0.9-2.6-1.9l-4.1-8.4 C21.3,5,21.1,5,21,5c-0.1,0-0.3,0-0.4,0.3l-4.1,8.4c-0.5,1-1.5,1.7-2.6,1.9l-9.2,1.3c-0.3,0-0.3,0.2-0.4,0.3c0,0.1-0.1,0.3,0.1,0.5 l6.7,6.5c0.8,0.8,1.2,1.9,1,3.1l-1.6,9.2c0,0.3,0.1,0.4,0.2,0.5c0.1,0.1,0.3,0.2,0.5,0l8.3-4.3C19.9,32.3,20.4,32.2,21,32.2z" fill="#EC9022"></path></svg>
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" style="margin-left: 5px;" xml:space="preserve" width="15px" height="15px"><style type="text/css"></style><path class="st0" d="M22.8,4.6l4.1,8.4c0.3,0.6,0.8,1,1.5,1.1l9.2,1.3c1.6,0.2,2.3,2.2,1.1,3.4L32,25.3c-0.5,0.5-0.7,1.1-0.6,1.7 l1.6,9.2c0.3,1.6-1.4,2.8-2.9,2.1l-8.3-4.3c-0.6-0.3-1.3-0.3-1.8,0l-8.3,4.3C10.4,39,8.7,37.8,9,36.2l1.6-9.2 c0.1-0.6-0.1-1.3-0.6-1.7l-6.7-6.5c-1.2-1.1-0.5-3.1,1.1-3.4l9.2-1.3c0.6-0.1,1.2-0.5,1.5-1.1l4.1-8.4C20,3.1,22,3.1,22.8,4.6z" fill="#FEDA00"></path><path class="st1" d="M10.9,40c-0.7,0-1.4-0.2-2-0.7c-1.1-0.8-1.6-2.1-1.4-3.4l1.6-9.2c0-0.2,0-0.3-0.1-0.4l-6.7-6.5 c-1-0.9-1.3-2.3-0.9-3.6c0.4-1.3,1.5-2.2,2.8-2.4l9.2-1.3c0.2,0,0.3-0.1,0.4-0.3l4.1-8.4C18.5,2.7,19.7,2,21,2 c1.3,0,2.5,0.7,3.1,1.9l4.1,8.4c0.1,0.1,0.2,0.2,0.4,0.3l9.2,1.3c1.3,0.2,2.4,1.1,2.8,2.4c0.4,1.3,0.1,2.6-0.9,3.6l-6.7,6.5 c-0.1,0.1-0.2,0.3-0.1,0.4l1.6,9.2c0.2,1.3-0.3,2.6-1.4,3.4c-1.1,0.8-2.5,0.9-3.7,0.3l-8.3-4.3c-0.1-0.1-0.3-0.1-0.4,0l-8.3,4.3 C12,39.9,11.5,40,10.9,40z M21,32.2c0.6,0,1.1,0.1,1.6,0.4l8.3,4.3c0.2,0.1,0.4,0,0.5,0c0.1-0.1,0.2-0.2,0.2-0.5L30,27.3 c-0.2-1.1,0.2-2.3,1-3.1l6.7-6.5c0.2-0.2,0.2-0.4,0.1-0.5c0-0.1-0.1-0.3-0.4-0.3l-9.2-1.3c-1.1-0.2-2.1-0.9-2.6-1.9l-4.1-8.4 C21.3,5,21.1,5,21,5c-0.1,0-0.3,0-0.4,0.3l-4.1,8.4c-0.5,1-1.5,1.7-2.6,1.9l-9.2,1.3c-0.3,0-0.3,0.2-0.4,0.3c0,0.1-0.1,0.3,0.1,0.5 l6.7,6.5c0.8,0.8,1.2,1.9,1,3.1l-1.6,9.2c0,0.3,0.1,0.4,0.2,0.5c0.1,0.1,0.3,0.2,0.5,0l8.3-4.3C19.9,32.3,20.4,32.2,21,32.2z" fill="#EC9022"></path></svg>';
        $html .= '</div></div></div>';
        return $html;
    }

    public function getNextYear($limit = 20, $pass = false)
    {
        $this_year = local_date('Y');
        $list = [];
        if($pass) {
            $y = $this_year;
            for($i = $limit; $i > 0 ;$i--){
                $y--;
                $list[] = $y;
            }
            $list = array_reverse($list);
        }
        for($i = 0; $i <$limit ;$i++){
            $list[] = $this_year;
            $this_year++;
        }

        return $list;
    }

    public function time_to_seconds($str_time) {

        sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);

        $time_seconds = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;

        return $time_seconds;
    }

    public function seconds_to_time($time_seconds) {
        return sprintf('%02d:%02d:%02d', ($time_seconds / 3600), ($time_seconds / 60 % 60), $time_seconds % 60);
    }


    function getQarter($year, $q, $last = false)
    {
        if($last == 'year') $year = $year - 1 ;
        else if($last == 'month') {
            if($q == 1) {
                $year = $year - 1 ;
                $q = 12;
            } elseif(is_numeric($q)) {
                $q = $q - 1;
            } else {
                if($q == 'Q1') {
                    $year = $year - 1 ;
                    $q = 'Q4';
                } elseif($q == 'Q2') {
                    $q = 'Q1';
                } elseif($q == 'Q3') {
                    $q = 'Q2';
                } elseif($q == 'Q4') {
                    $q = 'Q3';
                }
            }
        }
        if(is_numeric($q) && $q > 0 && $q <= 12) {
            $start_date = $year . '-' . $q . '-' . '01';
            $end_date = $year . '-' . $q . '-' . cal_days_in_month(CAL_GREGORIAN, $q, $year);
        } elseif($q === 0) {
            $start_date = $year.'-01-01';
            $end_date   = $year.'-12-31';
        } elseif($q == 'Q1') {
            $start_date = $year.'-01-01';
            $end_date   = $year.'-03-31';
        } elseif($q == 'Q2') {
            $start_date = $year.'-04-01';
            $end_date   = $year.'-06-30';
        } elseif($q == 'Q3') {
            $start_date = $year.'-07-01';
            $end_date   = $year.'-09-30';
        } elseif($q == 'Q4') {
            $start_date = $year.'-10-01';
            $end_date   = $year.'-12-31';
        }

        return ['start_date'=>$start_date, 'end_date'=>$end_date, 'year'=>$year, 'month'=>$q];
    }

    function cutWords($str)
    {
        ini_set('memory_limit', '1024M');
        require ROOT_PATH . 'includes/scws/pscws4.class.php';

        $pscws = new \PSCWS4();
        $pscws->set_charset('utf8');
        $pscws->set_rule(ROOT_PATH . 'includes/scws/rules_cht.utf8.ini');
        $pscws->set_dict(ROOT_PATH . 'includes/scws/etc/dict_cht.utf8.xdb');

        $pscws->set_ignore(true);
        if (get_magic_quotes_gpc())
            $str = stripslashes($str);
//        $str = rawurldecode('第三方中文分詞');
        $pscws->send_text(make_semiangle($str));
        $arr = [];
        while ($res = $pscws->get_result())
        {
            foreach ($res as $tmp)
            {
                $arr[] =  $tmp['word'];
            }
        }

        $pscws->close();
        if(empty($arr)) $arr[] = $str;
        return $arr;
    }
    public function ipConnection()
    {
        global $db, $ecs, $_CFG;
        $url =  $_SERVER['REQUEST_URI'];
        if(!$_CFG['open_jail']) return true;
        if(strpos($url, 'api') !== false) return true;
        $user_ip = real_ip();
        $now = local_strtotime('now');
        if(self::$onWhite) {
            return true;
        }
        $connection = $db->getRow('SELECT ip, create_at, count FROM '.$ecs->table('connections')." WHERE ip = '{$user_ip}' ");
        if(empty($connection)) {
            $this->db->query('INSERT INTO '.$ecs->table('connections')." (ip, create_at) VALUES ('{$user_ip}', '{$now}')  ON DUPLICATE KEY UPDATE ip = '{$user_ip}', create_at = '{$now}', count = count + 1 ", 'SILENT');
        } else if ($connection['create_at'] == $now) {
                $this->db->query('UPDATE '.$ecs->table( 'connections')." SET count = count + 1 WHERE ip = '{$user_ip}' ", 'SILENT');
                if($connection['count'] >= $_CFG['connection_limit']) {
                    $this->db->query("INSERT INTO ".$ecs->table( 'jail')." (ip, create_at, remark) VALUES ('{$user_ip}', '{$now}', 'connection time > ".$connection['count']." ') ON DUPLICATE KEY UPDATE ip = '{$user_ip}' ", 'SILENT');
                }
        } else {
            $this->db->query('UPDATE '.$ecs->table('connections')." SET create_at = {$now}, count = 0 WHERE ip = '{$user_ip}' ", 'SILENT');
        }
        if (($now % 60) == 0)
        {
            $this->db->query('DELETE FROM ' . $ecs->table('connections') . ' WHERE create_at < ' . ($now - 180), 'SILENT');
        }
        return true;
    }

    public function onJail()
    {
        $url =  $_SERVER['REQUEST_URI'];
        if(strpos($url, 'api') !== false) return false;
        global $db, $ecs, $_CFG;
        if(!$_CFG['open_jail']) return false;
        $user_ip = real_ip();
        /* We check white list first */
        if( !isset(self::$onWhite) || self::$onWhite == null)self::$onWhite = $db->getOne('SELECT 1 FROM '.$ecs->table('whitelist')." WHERE ip = '{$user_ip}' ");
        if(self::$onWhite) {
            return false;
        }
        $jail = $db->getOne('SELECT 1 FROM '.$ecs->table('jail')." WHERE ip = '{$user_ip}' ");
        return  $jail;

    }

    public function checkRequireSimplied($table, $fields, $id)
    {
        global $ecs, $db;

        if (empty($table) || empty($id) || empty($fields)) {
            return [];
        }

	    $table_info = localization_table_info($table);
        if ($table_info !== false) {
            extract($table_info);
        } else {
            return [];
        }

        $require_translate = [];
        $localizable_fields = localizable_fields_for_table($table);
        foreach ($fields as $key => $val) {
            if (!in_array($key, $localizable_fields)) continue;
            if (empty($val)) {
                $require_translate[$key] = "";
            } elseif ($db->getOne("SELECT 1 FROM " . $ecs->table($table) . " WHERE $key = '$val' AND $primary_key = $id") != "1") { // field updated
                $require_translate[$key] = $val;
            } else {
                $simplified_language = $db->getOne("SELECT $key FROM " . $ecs->table($lang_table) . " WHERE $primary_key = $id AND lang = 'zh_cn'");
                if (empty($simplified_language)) { // no simplified lang
                    $require_translate[$key] = $val;
                } elseif (stripslashes($simplified_language) == stripslashes($val)) { // do translate again if simplified and traditional are same
                    $require_translate[$key] = $val;
                } elseif (stripslashes($simplified_language) != translate_to_simplified_chinese(stripslashes($val))) { // simplified has updated
                    $require_translate[$key] = $val;
                }
            }
        }

        return $require_translate;
    }

    function get_permanent_link_cache($table, $id = 0, $lang = '')
    {
        if (empty($id)) {
            $links = [];
            $count = $this->db->getOne("SELECT COUNT(*) FROM " . $this->ecs->table('perma_link') . " WHERE table_name = '$table'");
            $i = 0;
            $limit = 5000;
            while ($i < $count) {
                $res = $this->db->getAll("SELECT id, lang, perma_link FROM " . $this->ecs->table('perma_link') . " WHERE table_name = '$table' ORDER BY id ASC LIMIT $i, $limit");
                foreach ($res as $row) {
                    $links[$row['id']][$row['lang']] = $row['perma_link'];
                }
                $i += $limit;
            }
            return $links;
        } else {
            $lang = empty($lang) ? $GLOBALS['_CFG']['lang'] : $lang;
            $link = $this->db->getOne("SELECT perma_link FROM " . $this->ecs->table('perma_link') . " WHERE table_name = '$table' AND id = $id AND lang = '$lang'");
            if (empty($link)) {
                $link = $this->db->getOne("SELECT perma_link FROM " . $this->ecs->table('perma_link') . " WHERE table_name = '$table' AND id = $id AND lang = '" . default_language() . "'");
            }
            return $link;
        }

        return false;
    }

    function maskStr($str){
        $arr_str = str_split($str);
        $arr_str_size = sizeof($arr_str);
        $masked_str = "";
        for ($i = 0; $i < $arr_str_size; $i++){
            if($i == 0 || $i == ($arr_str_size - 1)){
                $masked_str .= $arr_str[$i];
            } else {
                $masked_str .= "*";
            }
        }
        return $masked_str;
    }
    
    function is_testing() {
        global $db, $ecs, $_CFG, $_LANG;
        if (!empty($_CFG['testing_user_id'])) {
            $testing_user_ids = explode(',',$_CFG['testing_user_id']);
            if (!in_array($_SESSION['user_id'],$testing_user_ids)) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }
}
