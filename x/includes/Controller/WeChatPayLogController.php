<?php
# @Date:   2019-05-16T14:33:54+08:00
# @Filename: WeChatPayController.php
# @Last modified time: 
# @author: Billy@YOHO

namespace Yoho\cms\Controller;

require_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_accounting.php');
require_once(ROOT_PATH . '/includes/lib_time.php');
require_once(ROOT_PATH . '/includes/lib_order.php');
require_once(ROOT_PATH . '/includes/lib_payment.php');
use Yoho\cms\Model;

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}


class WeChatPayLogController extends YohoBaseController
{
    private $tablename = 'wechat_pay_log';
    private $payment_info = null;

    public function __construct()
    {
        $this->payment_info = get_payment('qfwechatpay');
        parent::__construct();
    }

    public function insert_wechat_pay_log($data, $payment_log_id)
    {
        $pay_log = $this->get_payment_log($payment_log_id);
        if (empty($pay_log)) {
            return false;
        }

        // Prepare parameters
        $param = [
            'log_id' => intval($payment_log_id),
            'order_id' => intval($pay_log['order_id']),
            'gateway_transaction_datetime' => gmstr2time($data->sysdtm),
            'gateway_payment_datetime' => gmstr2time($data->paydtm),
            'error_msg' => $data->errmsg,
            'currency_code' => $data->txcurrcd,
            'gateway_request_datetime' => gmstr2time($data->txdtm),
            'out_trade_no' => $data->out_trade_no,
            'gateway_transaction_sn' => $data->syssn,
            'cancel' => $data->cancel,
            'order_type' => $data->order_type,
            'gateway_transaction_amount' => bcdiv($data->txamt, '100', 2),
            'gateway_transaction_status_code' => $data->respcd
        ];

        if(!$this->check_if_wechat_pay_log_exists($data->syssn)) {
            // Insert the record since no entry is found
            $insert_sql_statement = 'INSERT INTO ' . $GLOBALS['ecs']->table('wechat_pay_log') . ' (log_id, order_id, gateway_transaction_datetime, gateway_payment_datetime, error_msg, currency_code, gateway_request_datetime, out_trade_no, gateway_transaction_sn, cancel, order_type, gateway_transaction_amount, gateway_transaction_status_code, raw_text) VALUES ('
            . "'".$param["log_id"]."', "
            . "'".$param["order_id"]."', "
            . "'".$param["gateway_transaction_datetime"]."', "
            . "'".$param["gateway_payment_datetime"]."', "
            . "'".$param["error_msg"]."', "
            . "'".$param["currency_code"]."', "
            . "'".$param["gateway_request_datetime"]."', "
            . "'".$param["out_trade_no"]."', "
            . "'".$param["gateway_transaction_sn"]."', "
            . "'".$param["cancel"]."', "
            . "'".$param["order_type"]."', "
            . "'".$param["gateway_transaction_amount"]."', "
            . "'".$param["gateway_transaction_status_code"]."', "
            . "'".json_encode($data)."' "
            .')';

            $GLOBALS['db']->query($insert_sql_statement);
            return $GLOBALS['db']->affected_rows();
        }
        // Uncomment the else case in case of update wechat payment log
        else
        {
            // TODO: Update the existing log in case of cancel or refund in place
            return $this->update_wechat_pay_log_data($param, json_encode($data));
        }

    }

    public function insert_wechat_pay_payment_notification($data)
    {
        $log_id = substr(explode('_', trim($data->out_trade_no))[0], 13);
        $payment_log = $this->get_payment_log($log_id);

        if (!$payment_log) {
            return false;
        }

        if (!check_money($log_id, bcdiv($data->txamt , 100, 2))){ // Case of unmatched amount
            return false;
        };

        if ($payment_log['currency_code'] != $data->txcurrcd) {
            return false;
        }

        // Convert notification type into order_type
        $data = (array)$data;
        $data['order_type'] = $data['notify_type'];
        $data = (object)$data;
        
        return $this->insert_wechat_pay_log($data, $log_id);
        
    }

    protected function check_if_wechat_pay_log_exists($syssn)
    {
        $wechat_pay_log = $this->get_pay_log_by_transaction_id($syssn);
        return !empty($wechat_pay_log);
    }

    protected function get_pay_log_id_by_transaction_id($gateway_transaction_id)
    {
        if (!$this->check_if_wechat_pay_log_exists($gateway_transaction_id)) {
            return false;
        }

        $wechat_pay_log = $this->get_pay_log_by_transaction_id($gateway_transaction_id);
        return $wechat_pay_log['log_id'];
    }

    private function get_pay_log_by_transaction_id($gateway_transaction_id)
    {
        $sql = "SELECT * FROM " . $GLOBALS['ecs']->table('wechat_pay_log') .
                " WHERE gateway_transaction_sn = '$gateway_transaction_id'";
                
        $wechat_pay_log = $GLOBALS['db']->getRow($sql);
        return $wechat_pay_log;
    }

    public function check_if_payment_exists($data, $payment_log_id)
    {
        $pay_log = $this->get_payment_log($payment_log_id);
        if (empty($pay_log)) {
            return false;
        } else {
            return true;
        }
    }

    private function get_payment_log($log_id)
    {
        $sql = "SELECT * FROM " . $GLOBALS['ecs']->table('pay_log') .
                " WHERE log_id = '$log_id'";
        $pay_log = $GLOBALS['db']->getRow($sql);
        return $pay_log;
    }

    public function get_all_payment_log($order_id)
    {
        $sql = "SELECT * FROM " . $GLOBALS['ecs']->table('wechat_pay_log') .
                " WHERE order_id = '$order_id'";
        $pay_log = $GLOBALS['db']->getAll($sql);
        return $pay_log;
    }

    /**
     *  Refund a transaction with designated amount
     */
    public function refund_transaction($log_id, $amount)
    {
        $payment_log = $this->get_payment_log($log_id);
        if (empty($payment_log)) {
            return json_encode([
                'error' => '1',
                'message' => 'Payment Log Not Found',
                'content' => ''
                ]);
        }

        // Get wechat pay log for original transaction sn and proposed refund sn
        $wechat_pay_record_query = "SELECT gateway_transaction_sn, out_trade_no FROM " . $GLOBALS['ecs']->table($this->tablename) . " WHERE log_id = '" . $log_id . "'";
        $record_query_result = $GLOBALS['db']->getRow($wechat_pay_record_query);
        if (empty($record_query_result)) {
            return json_encode([
                'error' => '1',
                'message' => 'WeChat Pay Log Entry Not Found',
                'content' => ''
                ]);
        }

        // Loop no more than 10 times for recurssive query to find nearest vacant refund order number since it is extremely rare to refund 10 times for a particular order
        $refund_identifier = '';

        for ($i = 0; $i < 10; $i++) {
            $insert_identifier = '';
            if ($i > 0) {
                $insert_identifier = $i;
            }
            $query_trade_no = $record_query_result['out_trade_no'] . "_r" . $insert_identifier;
            $query_param = [
                "order_payment_id" => $query_trade_no
            ];

            $result = $this->get_qfwechaypay_transaction_record($query_param);
            if (!isset($result)) {
                return json_encode([
                    'error' => '1',
                    'message' => 'WeChat Pay Transaction Record Not Found',
                    'content' => ''
                    ]);
            }
            $result = json_decode($result);
            if (empty($result->data[0])) {
                $refund_identifier = $query_trade_no;
                break;
            }
        }

        $local_datetime = local_getdate();
        $local_datetime = sprintf(
            "%s-%s-%s %s:%s:%s", 
            $local_datetime['year'], 
            str_pad($local_datetime['mon'], 2, '0', STR_PAD_LEFT),
            str_pad($local_datetime['mday'], 2, '0', STR_PAD_LEFT),
            str_pad($local_datetime['hours'], 2, '0', STR_PAD_LEFT),
            str_pad($local_datetime['minutes'], 2, '0', STR_PAD_LEFT), 
            str_pad($local_datetime['seconds'], 2, '0', STR_PAD_LEFT)
        );

        $parameter = [
            'txamt' => bcmul($amount, 100, 0),
            'txdtm' => $local_datetime,
            'syssn' => $record_query_result['gateway_transaction_sn'],
            'out_trade_no' => $refund_identifier,
        ];

        ksort($parameter);
        reset($parameter);

        $sign  = '';

        foreach ($parameter AS $key => $val)
        {
            $sign  .= ecs_iconv(EC_CHARSET, 'utf-8', $key) . "=" . ecs_iconv(EC_CHARSET, 'utf-8', $val). "&";
        }

        $sign  = substr($sign, 0, -1). ecs_iconv(EC_CHARSET, 'utf-8', $this->payment_info['qfwechatpay_key']);

        $qfpay_host = (strpos($_SERVER['SERVER_NAME'], 'beta.yohohongkong.com') === false) ? "https://openapi.qfpay.com" : "https://openapi-test.qfpay.com";
        $ch = curl_init($qfpay_host . '/trade/v1/refund');
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($parameter));
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            "X-QF-APPCODE: " . $this->payment_info['qfwechatpay_code'],
            "X-QF-SIGN: " . strtoupper(md5($sign)),
            "Content-Type: application/x-www-form-urlencoded",
            "Content-Length: " . strlen(http_build_query($parameter)),
        ]);

        $wechat_pay_creation_result = curl_exec($ch);

        if (curl_errno($ch) != 0) // cURL error
        {
            hw_error_log('Cannot connect to QFPay: ' . curl_error($ch));
            curl_close($ch);
            return json_encode([
                'error' => '1',
                'message' => 'Unable to connect Payment Gateway',
                'content' => ''
                ]);
        }
        else
        {
            // Begin parse sync response
            // Serarate response headers and body
            $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            $wechat_pay_creation_result_header = substr($wechat_pay_creation_result, 0, $header_size);
            $wechat_pay_creation_result = substr($wechat_pay_creation_result, $header_size);

            // Parse and compare the signature
            if (!$this->compareResponseSignature($wechat_pay_creation_result_header, $wechat_pay_creation_result, $this->payment_info['qfwechatpay_key'])) {
                curl_close($ch);
                return json_encode([
                    'error' => '0',
                    'message' => 'Response Signature Error',
                    'content' => ''
                    ]);
            }
            
            curl_close($ch);
        }

        $wechat_pay_creation_result = json_decode($wechat_pay_creation_result);
        if ($wechat_pay_creation_result->respcd != '0000') {
            return json_encode([
                'error' => '1',
                'message' => $wechat_pay_creation_result,
                'content' => ''
                ]);
        }

        // Convert data into insertable format
        $wechat_pay_creation_result->order_type = 'refund';
        $wechat_pay_creation_result->errmsg = $wechat_pay_creation_result->resperr;

        $insert_result = $this->insert_wechat_pay_log($wechat_pay_creation_result, $log_id);
        return json_encode([
            'error' => '0',
            'message' => $wechat_pay_creation_result,
            'content' => ''
            ]);
    }

    private function compareResponseSignature($response_header, $response_body, $key)
    {
        // Get headers for sign checking
        $headers = [];
        $data = explode("\n",$response_header);
        $headers['status'] = $data[0];
        array_shift($data);

        foreach($data as $part){
            $middle = explode(":",$part);
            $headers[trim($middle[0])] = trim($middle[1]);
        }

        $result_sign  = strtoupper(md5($response_body . ecs_iconv(EC_CHARSET, 'utf-8', $key)));
        
        return (!empty($headers['X-QF-SIGN']) && ($headers['X-QF-SIGN'] == $result_sign));
    }

    // TODO: Uncomment out the function update_wechat_pay_log_data in case need of update saved wechat payment records
    private function update_wechat_pay_log_data($param, $data_string)
    {
        $update_column_value = '';

        $param_count = count($param);
        if ($param_count == 0 || empty($param['gateway_transaction_sn'])) {
            return false;
        }

        // Fetch the current transaction record
        $sql = "SELECT * FROM " . $GLOBALS['ecs']->table($this->tablename) .
                " WHERE gateway_transaction_sn = '" . $param['gateway_transaction_sn'] . "'";
        $wechat_payment_log = $GLOBALS['db']->getRow($sql);
        if (empty($wechat_payment_log)) {;
            return false;
        }

        $fields = [
            'log_id',
            'order_id',
            'gateway_transaction_datetime',
            'gateway_payment_datetime',
            'currency_code',
            'gateway_request_datetime',
            'out_trade_no',
            'gateway_transaction_sn',
            'cancel',
            'order_type',
            'gateway_transaction_amount',
            'gateway_transaction_status_code'
        ];

        // Prepare and determine if update is required
        foreach($fields as $field) {
            if (!empty($param[$field]) && $wechat_payment_log[$field] != $param[$field]) {
                if (!empty($update_column_value)) {
                    $update_column_value .= ', ';
                }
                $update_column_value .= $field . ' = "' . $param[$field] . '"';
            }
        }

        if (empty($update_column_value)) {
            return false;
        } else {
            // Append the raw string into table raw_text field 
            $update_column_value .= ', raw_text = concat(raw_text, ", ", \'' . $data_string . '\')';
        }

        // Update corresponding database record with concatenation of raw string
        $update_sql_query = 'UPDATE ' . $GLOBALS['ecs']->table('wechat_pay_log') . ' SET ' . $update_column_value . ' WHERE `gateway_transaction_sn` = "' . $param['gateway_transaction_sn'] . '"';

        $GLOBALS['db']->query($update_sql_query);
        return $GLOBALS['db']->affected_rows();
    }

    public function get_qfwechaypay_transaction_record($param)
    {
        $host_name = (strpos($_SERVER['SERVER_NAME'], 'beta.yohohongkong.com') === false) ? "https://openapi.qfpay.com" : "https://openapi-test.qfpay.com";

        if ( (empty($param['order_payment_id']) && empty($param['gateway_transaction_sn']))
            || (!empty($param['order_payment_id']) && !empty($param['gateway_transaction_sn'])) ) {
            return false;
        }

        if (!empty($param['order_payment_id']) && empty($param['gateway_transaction_sn'])) {
            $parameter['out_trade_no'] = $param['order_payment_id'];
        }
        
        if (empty($param['order_payment_id']) && !empty($param['gateway_transaction_sn'])) {
            $parameter['syssn'] = $param['gateway_transaction_sn'];
        }
        
        $sign  = '';
        
        foreach ($parameter AS $key => $val)
        {
            $sign  .= ecs_iconv(EC_CHARSET, 'utf-8', $key) . "=" . ecs_iconv(EC_CHARSET, 'utf-8', $val). "&";
        }
        
        $sign  = substr($sign, 0, -1). ecs_iconv(EC_CHARSET, 'utf-8', $this->payment_info['qfwechatpay_key']);
        
        $ch = curl_init($host_name.'/trade/v1/query');
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($parameter));
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            "X-QF-APPCODE: " . $this->payment_info['qfwechatpay_code'],
            "X-QF-SIGN: " . md5($sign),
            "Content-Type: application/x-www-form-urlencoded",
            "Content-Length: " . strlen(http_build_query($parameter)),
        ]);
        
        $wechat_pay_query_result = curl_exec($ch);
        
        if (curl_errno($ch) != 0) // cURL error
        {
            hw_error_log('Cannot connect to QFPay: ' . curl_error($ch));
            curl_close($ch);
            echo json_encode([
                'status' => 0,
                'error' => 'Query failed: Unable to connect to Payment Service'
            ]);
            exit;
        }
        else
        {
            // Serarate response headers and body
            $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            $wechat_pay_query_result_header = substr($wechat_pay_query_result, 0, $header_size);
            $wechat_pay_query_result = substr($wechat_pay_query_result, $header_size);
        
            // Get headers for sign checking
            $headers = [];
            $data = explode("\n",$wechat_pay_query_result_header);
            $headers['status'] = $data[0];
            array_shift($data);
        
            foreach($data as $part){
                $middle = explode(":",$part);
                $headers[trim($middle[0])] = trim($middle[1]);
            }
        
            $result_sign  = strtoupper(md5($wechat_pay_query_result . ecs_iconv(EC_CHARSET, 'utf-8', $this->payment_info['qfwechatpay_key'])));
        
            // Parse and compare the signature
            if (!(!empty($headers['X-QF-SIGN']) && $headers['X-QF-SIGN'] == $result_sign)) {
                curl_close($ch);
                return false;
            }
            curl_close($ch);
        }
       return $wechat_pay_query_result;
    }

    public function refund_by_transaction_id_and_amount($gateway_transaction_id, $amount)
    {
        if (empty($gateway_transaction_id) || empty($amount)) {
            return false;
        }

        $log_id = $this->get_pay_log_id_by_transaction_id($gateway_transaction_id);

        $refund_result = json_decode($this->refund_transaction($log_id, $amount));
        if ($refund_result->error == '0') {
            return true;
        } else {
            return false;
        }
    }
}

?>