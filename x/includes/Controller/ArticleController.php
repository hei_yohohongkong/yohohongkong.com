<?php
/**
 * Created by Anthony.
 * User: Anthony
 * Date: 2018/10/4
 * Time: 下午 04:33
 */

namespace Yoho\cms\Controller;
use Yoho\cms\Model;
if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

class ArticleController extends YohoBaseController
{
    public $admin_priv = 'article_manage';
    
    /**
     * @param $list
     * @param $totalCount
     * @param array $action_btn
     * @param array $extraData
     */
    public function ajaxQueryAction($list, $totalCount, $action_btn = ['edit', 'remove'], $extraData = [])
    {
        check_authz_json($this->admin_priv);
    
        $list = $this->getArticlesList();
        $article_list = $list['arr'];
        $totalCount = $list['record_count'];

        parent::ajaxQueryAction($article_list, $totalCount, false);
    }

    public function ajaxEditAction()
    {
        global $db, $ecs;
        if((!empty($_REQUEST['col'])) && $_REQUEST['col'] == 'sort_order'){
            if ($_REQUEST['value'] >= 0 && $_REQUEST['value'] <= 255){
                $exc = new \exchange($ecs->table("article"), $db, 'article_id', 'sort_order');
            } else {
                make_json_error("請輸入0-255");
            }
        }
        parent::ajaxEditAction();
    }

    public function getArticlesList()
    {
        $filter = array();
        $filter['keyword'] = empty($_REQUEST['keyword']) ? '' : json_str_iconv(trim($_REQUEST['keyword']));
        $filter['cat_id'] = empty($_REQUEST['cat_id']) ? 0 : intval($_REQUEST['cat_id']);
        $filter['sort_by'] = empty($_REQUEST['sort_by']) ? 'a.cat_id' : trim($_REQUEST['sort_by']);
        $filter['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
        $filter['page_size'] = empty($_REQUEST['page_size']) ? 50 : intval($_REQUEST['page_size']);
        $filter['start'] = empty($_REQUEST['start']) ? 0 : intval($_REQUEST['start']);
        $filter['type'] = empty($_REQUEST['type']) ? 'post' : trim($_REQUEST['type']);
        $filter['support'] = empty($_REQUEST['support']) ? 0 : intval($_REQUEST['support']);
    
        $where = '';
        if (!empty($filter['keyword'])) {
            $where = " AND a.title LIKE '%" . mysql_like_quote($filter['keyword']) . "%'";
        }
        if ($filter['cat_id']) {
            $where .= " AND a." . get_article_children($filter['cat_id']);
        }
        if($filter['support']) {
            $where .= " AND ac.cat_type = ".SUPPORT_CAT." ";
        } else if ($filter['type'] == 'post') {
            $where .= " AND ac.cat_type = ".COMMON_CAT." ";
        } else if($filter['type'] == 'page') {
            $where .= " AND ac.cat_type NOT IN( ".COMMON_CAT.", ".SUPPORT_CAT.") ";
        }
    
        /* 文章总数 */
        $sql = 'SELECT COUNT(*) FROM ' . $GLOBALS['ecs']->table('article') . ' AS a ' .
            'LEFT JOIN ' . $GLOBALS['ecs']->table('article_cat') . ' AS ac ON ac.cat_id = a.cat_id ' .
            'WHERE 1 ' . $where;
        $record_count = $GLOBALS['db']->getOne($sql);

        /* 获取文章数据 */
        $sql = 'SELECT a.* , ac.cat_name, ac.cat_type ' .
            'FROM ' . $GLOBALS['ecs']->table('article') . ' AS a ' .
            'LEFT JOIN ' . $GLOBALS['ecs']->table('article_cat') . ' AS ac ON ac.cat_id = a.cat_id ' .
            'WHERE 1 ' . $where . ' ORDER by ' . $filter['sort_by'] . ' ' . $filter['sort_order']." , a.sort_order ASC, a.article_id DESC ";
    
        $filter['keyword'] = stripslashes($filter['keyword']);
    
        $arr = array();
        $res = $GLOBALS['db']->selectLimit($sql, $filter['page_size'], $filter['start']);
    
        while ($rows = $GLOBALS['db']->fetchRow($res)) {
            $rows['date'] = date($GLOBALS['_CFG']['time_format'], $rows['add_time']);
            if($rows['cat_type'] == SUPPORT_CAT) {
                $rows['_action'] = $this->generateCrudActionBtn($rows['article_id'], 'id', null, null, ['view' => ['icon_class'=> 'fa fa-eye', 'link' => '/support.php?id='.$rows['article_id'], 'target' => '_blank']]);
            } else {
                $rows['_action'] = $this->generateCrudActionBtn($rows['article_id'], 'id', null, null, ['view' => ['icon_class'=> 'fa fa-eye', 'link' => '/article.php?id='.$rows['article_id'], 'target' => '_blank']]);
            }

            $rows['sort_order'] = "<a class='bg-green' style='border-radius: 20%; padding: 1px 2px;' href='javascript:;' onclick='orderPopup(".$rows['cat_id'].")'>".$rows['sort_order']."</a>";

            $arr[] = $rows;
        }
    
        // Multiple language support
        if (!empty($_REQUEST['edit_lang'])) {
            $arr = localize_db_result_lang($_REQUEST['edit_lang'], 'article', $arr);
            $arr = localize_db_result_lang($_REQUEST['edit_lang'], 'article_cat', $arr);
        }
    
        return array('arr' => $arr, 'record_count' => $record_count);
    }

    public function updateCatParentStyle($cat_id = 0, $post)
    {
        global $ecs, $db;
        $img_path = '../' . DATA_DIR . '/afficheimg/cat_banner/';
        if(empty($cat_id)) return false;
        $style = [];
        $style['desktop'] = $post['desktop'];
        $style['mobile']  = $post['mobile'];
        foreach($style as $platform => $config) {
            if(empty($config)) continue;
            $column_name = $platform.'_config';
            $config['grid'] = empty($config['grid']) ? 0 : $config['grid'];
            $config['old_background_img'] = empty($config['old_background_img']) ? '' : $config['old_background_img'];
            $config['background_img'] = empty($config['background_img']) ? $config['old_background_img'] : $config['background_img'];
            if(isset($config['remove_background_img'])) {
                $config['background_img'] = '';
                if (is_file( $img_path. $config['background_img']))
                {
                    @unlink( $img_path. $config['background_img']);
                }
                $config['background_img'] = '';
            } elseif (isset($_FILES[$platform]) && ($_FILES[$platform]['error']['background_img'] == 0 && $_FILES[$platform]['tmp_name']['background_img'] !== 'none')) {
                $filename = $platform.'_background_img' . substr($_FILES[$platform]['name']['background_img'], strpos($_FILES[$platform]['name']['background_img'], '.'));
                $path     = $img_path . $filename;
                if (move_upload_file($_FILES[$platform]['tmp_name']['background_img'], $path)) $config['background_img'] = $filename;
            }

            $config = serialize($config);
            $db->query("UPDATE " . $ecs->table('article_cat') . " SET $column_name = '".$config."' WHERE cat_id = '$cat_id' ");
        }
        return true;
    }
}