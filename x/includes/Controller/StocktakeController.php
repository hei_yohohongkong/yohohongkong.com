<?php

/***
* StocktakeController.php
*
*
*
***/
namespace Yoho\cms\Controller;

use Yoho\cms\Model;

use \Exception;

use Yoho\cms\Controller\ErpController;

if (!defined('IN_ECS')) {
    die('Hacking attempt');
}
class StocktakeController extends YohoBaseController
{
    const PENDING_FOR_STOCKTAKE = '0';
    const STOCKTAKE_PROGRESSING = '1';
    const WAITING_FOR_REVIEW = '2';
    const REVIEW_PROGRESSING = '3';
    const FINISHED = '4';

    const GOODS_STOCKTAKE_PENDING = '0';
    const GOODS_STOCKTAKE_FINISHED = '1';
    const GOODS_REVIEW_PENDING = '2';
    const GOODS_REVIEW_FINISHED = '3';
    const GOODS_REVIEW_RESTOCKTAKE = '4';

    const TYPE_ARCHIVED = 0;
    const TYPE_ACTIVE = 1;
    const TYPE_DELETED = 2;

    private static $statusValues=['等待盤點','盤點進行中','等待覆核','覆核進行中','已完成'];
    private static $goodsStatusValues=['等待盤點','已盤點','等待覆核','已完成覆核','重新盤點'];
    private static $typeValues=['已封存','未封存','已刪除'];

    private $tablename='erp_goods_stocktake';
    
    public function __construct()
    {
        parent::__construct();
       
        $this->adminuserController = new AdminuserController();
    }
    
    public function getPeriodOptions()
    {
        global $db, $ecs, $userController ,$_CFG;

        $period =  array ('30' => '近30日未盤點', '60' => '近60日未盤點', '90' => '近90日未盤點');

        return $period;
    }

    public function getStockTakeWarehouse($stocktake_id) {
        global $db, $ecs, $userController ,$_CFG;

        $sql = "select warehouse_id from ".$ecs->table('erp_goods_stocktake_item')." where stocktake_id = ".$stocktake_id." limit 1 ";
        $warehouse_id = $db->getOne($sql);

        return $warehouse_id;
    }

    public function getStocktakeList($is_pagination = true)
    {
        global $db, $ecs, $userController ,$_CFG;

        // total record
        $sql = 'SELECT count(*) FROM ' . $ecs->table('erp_goods_stocktake') . " WHERE 1";
        $sql .= (!isset($_REQUEST['status']) || isset($_REQUEST['status']) && $_REQUEST['status'] == '-1') ? '' : " and status = '".$_REQUEST['status']."'";
        $sql .= (!empty($_REQUEST['title'])) ? " and title like '%".$_REQUEST['title']."%'" : '' ;
        $row = $db->GetAll($sql);
        $total_count = $db->getOne($sql);

        $_REQUEST['record_count'] = $total_count;
        $_REQUEST['sort_by']    = empty($_REQUEST['sort_by']) ? 'stocktake_id' : trim($_REQUEST['sort_by']);
        $_REQUEST['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
        $_REQUEST['status'] = (!isset($_REQUEST['status']) || isset($_REQUEST['status']) && $_REQUEST['status'] == '-1') ? '-1' : $_REQUEST['status'];
        $_REQUEST['type'] = !isset($_REQUEST['type']) ? '1' : $_REQUEST['type'];
        $_REQUEST['title'] = $_REQUEST['title'];

        // pagination
        $sql = 'SELECT egsp.*, au.avatar, au.user_name FROM ' . $ecs->table('erp_goods_stocktake') . " egsp left join ". $ecs->table('admin_user')." au on (egsp.stocktake_person = au.user_id) WHERE 1 ";
        $sql .= (!isset($_REQUEST['status']) || isset($_REQUEST['status']) && $_REQUEST['status'] == '-1') ? '' : " and status = '".$_REQUEST['status']."'";
        $sql .= " and type = '".$_REQUEST['type']."'";
        $sql .= (!empty($_REQUEST['title'])) ? " and title like '%".$_REQUEST['title']."%'" : '' ;
        $sql .= ' order by '. $_REQUEST['sort_by'] . ' ' .  $_REQUEST['sort_order'];
        $sql .= ($is_pagination ? " LIMIT " . $_REQUEST['start'] . ", " . $_REQUEST['page_size'] : '');
        $res = $db->getAll($sql);
       
        $statusOptions = $this->getStocktakeStatusOptions();
        $warehouse_info = get_warehouse_list(0, 0, 1);
        foreach ($res as $key => &$row) {
            $warehouse_id = $this->getStockTakeWarehouse($row['stocktake_id']);

          
            $warehouse_name = $warehouse_info[$warehouse_id]['name'];

            $stocktake_progress_html = '<div class="widget_summary">
                    <div class="w_center w_55" style="width: 100%;">
                      <div class="progress">
                        <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" title = "'.$row['stocktake_finished_rate'].'%" style="width: '.$row['stocktake_finished_rate'].'%;">
                          <span class="sr-only">60% Complete</span>
                        </div>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                  </div>';
            $reviw_progress_html = '<div class="widget_summary">
                    <div class="w_center w_55" style="width: 100%;">
                      <div class="progress">
                        <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" title = "'.$row['review_finished_rate'].'%" style="width: '.$row['review_finished_rate'].'%;">
                          <span class="sr-only">60% Complete</span>
                        </div>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                  </div>';
            $actions_html = '<ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link" href="erp_stocktake_process.php?act=list&amp;stocktake_id='.$row['stocktake_id'].'"><i class="fa fa-eye green" style="font-size: 20px;"></i></a>
                      </li>
                      <li><a class="collapse-link confirm_action" data-stocktake-id="'.$row['stocktake_id'].'" data-change-type="0" href="javascript:void(0)"><i class="fa fa-archive" style="font-size: 20px;color:#f0ad4e"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true"><i class="fa fa-chevron-down" style="font-size: 20px;"></i></a>
                        <ul class="dropdown-menu" role="menu" style="min-width: 100px;">
                        
                          <li><a class="confirm_action" data-stocktake-id="'.$row['stocktake_id'].'" data-change-type="2" href="javascript:void(0)">刪除</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>';

            $row['create_date'] = local_date($_CFG['time_format'], $row['create_date']);
            $row['status'] = $statusOptions[$row['status']];
            $row['avatar'] = '<img title="'.$row['user_name'].'" src="'.(empty($row['avatar']) ? '/uploads/avatars/default_60x60.png' : '/'.$row['avatar']).'" >';
            $row['stocktake_progress'] = $stocktake_progress_html;
            $row['review_progress'] = $reviw_progress_html;
            $row['actions'] = $actions_html;
            $row['warehouse'] = $warehouse_name;
        }
        // $sort_order  = $_REQUEST['sort_order'] == 'ASC' ? SORT_ASC : SORT_DESC;
        // array_multisort($sort, $sort_order, $res);

        $arr = array(
            'data' => $res,
            'record_count' => $_REQUEST['record_count']
        );

        return $arr;
    }

    public function bulkStocktakeTypeChange() 
    {
        global $db, $ecs, $userController ,$_CFG;

        if (!empty($_REQUEST['selected_stocktake_ids']) && $_REQUEST['change_type'] != '' ) {
            foreach ($_REQUEST['selected_stocktake_ids'] as $stocktake_id) {
                $sql = "update ".$ecs->table('erp_goods_stocktake')." set type = '".$_REQUEST['change_type']."' where stocktake_id = '".$stocktake_id."' ";
                $db->query($sql);
            }
        }

        return true;
    }

    public function stocktakeTypeChange() 
    {
        global $db, $ecs, $userController ,$_CFG;

        if (!empty($_REQUEST['stocktake_id']) && $_REQUEST['change_type'] != '' ) {
            $sql = "update ".$ecs->table('erp_goods_stocktake')." set type = '".$_REQUEST['change_type']."' where stocktake_id = '".$_REQUEST['stocktake_id']."' ";
            $db->query($sql);
        }

        return true;
    }

    public function getStocktakeStatusOptions()
    {
        return self::$statusValues;
    }

    public function getTypeOptions()
    {
        return self::$typeValues;
    }

    public function getGoodsStatusOptions($stage = 'all', $return_key = false)
    {
        $process_status = [self::GOODS_STOCKTAKE_PENDING,self::GOODS_STOCKTAKE_FINISHED,self::GOODS_REVIEW_RESTOCKTAKE];
        $review_status = [self::GOODS_REVIEW_PENDING,self::GOODS_REVIEW_FINISHED,self::GOODS_REVIEW_RESTOCKTAKE];
        if ($stage == 'all' || $stage == 'finished') {
            return self::$goodsStatusValues;
        } elseif ($stage == 'process') {
            //return self::$goodsStatusValues;
            $status_ar = [];
            foreach (self::$goodsStatusValues as $key => $status) {
                if (in_array($key, $process_status)) {
                    if ($return_key) {
                        $status_ar[] = $key;
                    } else {
                        $status_ar[$key] = $status;
                    }
                }
            }
            return $status_ar;
        } elseif ($stage == 'review') {
            $status_ar = [];
            foreach (self::$goodsStatusValues as $key => $status) {
                if (in_array($key, $review_status)) {
                    if ($return_key) {
                        $status_ar[] = $key;
                    } else {
                        $status_ar[$key] = $status;
                    }
                }
            }
            return $status_ar;
        }
    }

    public function stocktakePersonList()
    {
        global $db, $ecs, $userController ,$_CFG;
        $sql = "SELECT user_id,user_name FROM ".$ecs->table('admin_user');
        $stocktake_person = $db->getAll($sql);
        $person_list = [];
        foreach ($stocktake_person as $person) {
            $person_list[$person['user_id']] = $person['user_name'];
        }
        return $person_list;
    }

    public function stocktakePersonOption()
    {
        global $db, $ecs, $userController ,$_CFG;
        $sql = "SELECT user_id as value,user_name as name FROM ".$ecs->table('admin_user');
        $stocktake_person = $db->getAll($sql);
        array_unshift($stocktake_person, array('name'=>'請選擇...','value'=>''));
        return $stocktake_person;
    }

    public function create()
    {
        global $db, $ecs, $userController ,$_CFG;
        $warehouse_list = get_warehouse_list(0, 0, 1);
        $brand_list = brand_list();
    
        // stocktake person
        $stocktake_person_list = $this->stocktakePersonList();
        $stocktake_person_option = $this->stocktakePersonOption();
        $arr = array(
            'stocktake_person_option' => $stocktake_person_option,
            'stocktake_person' => $stocktake_person,
            'brand_list' => $brand_list,
            'warehouse_list' => $warehouse_list,
            'stocktake_person' => $stocktake_person,
        );
    
        return $arr;
    }

    public function getStocktakeInfo($stocktake_id)
    {
        global $db, $ecs, $userController ,$_CFG;

        $sql = "select * from ".$ecs->table('erp_goods_stocktake')." where  stocktake_id = '".$stocktake_id."'";
        $stocktake = $db->getRow($sql);
        return $stocktake;
    }

    public function updateStocktakeReset($stocktake_id)
    {
        global $db, $ecs, $userController ,$_CFG;
        $stocktake_id = $_REQUEST['stocktake_id'];
        $goods_id = $_REQUEST['goods_id'];
        $warehouse_id = $_REQUEST['warehouse_id'];
        // stocktake status is REVIEW_PROGRESSING
        $result = $this->changeStocktakeStatus($stocktake_id, self::REVIEW_PROGRESSING);
        if ($result) {
            // change product status 1
            $this->changeStocktakeProductStatus($stocktake_id, $goods_id, $warehouse_id, self::GOODS_REVIEW_PENDING);

            return true;
        } else {
            return false;
        }
    }

    public function detail($stage = 'process', $is_pagination = true)
    {
        global $db, $ecs, $userController ,$_CFG;

        $warehouse_list = get_warehouse_list(0, 0, 1);
        $brand_list = brand_list();
        $stocktake_id = $_REQUEST['stocktake_id'];
        
        // stocktake person
        $stocktake_person_option = $this->stocktakePersonOption();
        $stocktake_person_list = $this->stocktakePersonList();
        
        $stocktake_status_option = $this->getStocktakeStatusOptions();
        $goods_status_options = $this->getGoodsStatusOptions($stage);
        $goods_status_option_keys = $this->getGoodsStatusOptions($stage, true);
        
        // total recode
        $sql = "select count(*) from ".$ecs->table('erp_goods_stocktake_item')." where stocktake_id = '".$stocktake_id."'";
        $sql .= (!isset($_REQUEST['status']) || isset($_REQUEST['status']) && $_REQUEST['status'] == '-1') ? '' : " and status = '".$_REQUEST['status']."'";

        if ($stage == 'process') {
            $sql .= " and status in (".implode(",", $goods_status_option_keys).") ";
        }

        $total_count = $db->getOne($sql);
        
        $_REQUEST['record_count'] = $total_count;
        //$filter = page_and_size($filter);
        $_REQUEST['status'] = !isset($_REQUEST['status'])? '-1': $_REQUEST['status'] ;
        $_REQUEST['stocktake_id'] = $stocktake_id;

        $warehouse_name_list = [];
        foreach ($warehouse_list as $key => $warehouse) {
            $warehouse_name_list[$key] =	$warehouse['name'];
        }

        // load stocktake goods
        $sql = "select * from ".$ecs->table('erp_goods_stocktake')." where  stocktake_id = '".$stocktake_id."'";
        $stocktake = $db->getRow($sql);

        $sql = "select egsp.*, g.goods_name, g.last_stocktake_time from ".$ecs->table('erp_goods_stocktake_item')." egsp left join ".$ecs->table('goods')." g on (egsp.goods_id = g.goods_id) where egsp.stocktake_id = '".$stocktake_id."'";
        $sql .= (!isset($_REQUEST['status']) || isset($_REQUEST['status']) && $_REQUEST['status'] == '-1') ? '' : " and status = '".$_REQUEST['status']."'";
        if ($stage == 'process') {
            $sql .= " and status in (".implode(",", $goods_status_option_keys).") ";
        }
        $sql .= ($is_pagination ? " LIMIT " . $_REQUEST['start'] . ", " . $_REQUEST['page_size'] : '');
        $stocktake_goods = $db->getAll($sql);

        foreach ($stocktake_goods as &$goods) {
            if (!empty($goods['stocktake_start_time'])) {
                $goods['stocktake_start_time'] = local_date($_CFG['time_format'], $goods['stocktake_start_time']);
            }

            if ($goods['stock'] != $goods['end_stock']) {
                $goods['stock_html'] = '<span style="color:red">'.$goods['stock'].'</span>';
                $goods['end_stock_html'] = '<span style="color:red">'.$goods['end_stock'].'</span>';
            } else {
                $goods['stock_html'] = '<span style="color:green">'.$goods['stock'].'</span>';
                $goods['end_stock_html'] = '<span style="color:green">'.$goods['end_stock'].'</span>';
            }

            if (!empty($goods['stocktake_end_time'])) {
                $goods['stocktake_end_time'] = local_date($_CFG['time_format'], $goods['stocktake_end_time']);
            }
            
            $goods['need_to_adjust'] = (int)$goods['stock'] + (int)$goods['changed_stock'] - (int)$goods['end_stock'];
           
            $goods['need_to_adjust_html'] = '<input class="form-control" style="width:50px" type="input" id="'.$goods['goods_id'].'_adjust_stock" value="'.($goods['adjust_stock'] == ''? $goods['need_to_adjust'] : $goods['adjust_stock']).'" />('.$goods['need_to_adjust'].')';
            
            if ($goods['status'] == 2) {
                $goods['review_action_html'] = '<input type="button" class="confirm_adjust_stock btn btn-success" data-goods="'.$goods['goods_id'].'" data-warehouse="'.$goods['warehouse_id'].'" value="確定盤點"> | <input type="button" class="reset_stocktake_review btn btn-danger" data-status="'.self::GOODS_REVIEW_RESTOCKTAKE.'" data-item-id="'.$goods['goods_stocktake_item_id'].'" data-goods="'.$goods['goods_id'].'" data-warehouse="'.$goods['warehouse_id'].'" value="重新盤點">';
            } else {
                $goods['review_action_html'] = $goods_status_options[$goods['status']].'<input type="button" class="reset_stocktake_review btn btn-warning" data-status="'.self::GOODS_REVIEW_PENDING.'" data-item-id="'.$goods['goods_stocktake_item_id'].'" data-goods="'.$goods['goods_id'].'" data-warehouse="'.$goods['warehouse_id'].'" value="重置">';
            }
            
            // $goods['action_html'] = '';
            $goods['warehouse_name'] = $warehouse_name_list[$goods['warehouse_id']];
            $goods['status_name'] = $goods_status_options[$goods['status']];
        }

        $arr = array(
            'stocktake' => $stocktake,
            'data' => $stocktake_goods,
            'brand_list' => $brand_list,
            'goods_status_options' => $goods_status_options,
            'stocktake_status_option' => $stocktake_status_option,
            'stocktake_person_option' => $stocktake_person_option,
            'stocktake_person_list' => $stocktake_person_list,
            'warehouse_name_list' => $warehouse_name_list,
            'filter' => $filter,
           // 'page_count' => $_REQUEST['page_count'],
            'record_count' => $_REQUEST['record_count']
            );
    
        return $arr;
    }

    public function checkStatusRedirect($stocktake_id, $stage)
    {
        global $db, $ecs, $userController ,$_CFG;

        $process_stage = array(self::PENDING_FOR_STOCKTAKE,self::STOCKTAKE_PROGRESSING);
        $review_stage = array(self::WAITING_FOR_REVIEW,self::REVIEW_PROGRESSING);
        $finished_stage = array(self::FINISHED);
        
        // get status
        $sql = "select * from ".$ecs->table('erp_goods_stocktake')." where  stocktake_id = '".$stocktake_id."'";
        $stocktake = $db->getRow($sql);

        if (in_array($stocktake['status'], $process_stage)) {
            // redirect
            if ($stage != 'process') {
                header("Location: erp_stocktake_process.php?act=list&stocktake_id=".$stocktake_id);
                die();
            }
        } elseif (in_array($stocktake['status'], $review_stage)) {
            // redirect
            if ($stage != 'review') {
                header("Location: erp_stocktake_review.php?act=list&stocktake_id=".$stocktake_id);
                die();
            }
        } elseif (in_array($stocktake['status'], $finished_stage)) {
            // redirect
            if ($stage != 'finished') {
                header("Location: erp_stocktake_finished.php?act=list&stocktake_id=".$stocktake_id);
                die();
            }
        }
    }

    public function updateStocktakeBasicInfo()
    {
        global $db, $ecs, $userController ,$_CFG;
        $stocktake_id = $_REQUEST['stocktake_id'];
        $title = $_REQUEST['title'];
        $stocktake_person = $_REQUEST['stocktake_person'];

        if (!empty($stocktake_id) && !empty($title) && !empty($stocktake_person)) {
            $sql = "update ".$ecs->table("erp_goods_stocktake")." set title = '".$title."', stocktake_person = '".$stocktake_person."' ".(!empty($review_person)? ", review_person = '".$review_person."' " : '')." where stocktake_id = '".$stocktake_id."'";
            $result = $db->query($sql);
    
            if ($result) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function updateStocktakeRestocktake()
    {
        global $db, $ecs, $userController ,$_CFG;
        $stocktake_id = $_REQUEST['stocktake_id'];
        $goods_id = $_REQUEST['goods_id'];
        $warehouse_id = $_REQUEST['warehouse_id'];
        // stocktake status is REVIEW_PROGRESSING
        $result = $this->changeStocktakeStatus($stocktake_id, self::REVIEW_PROGRESSING);
        if ($result) {
            // change product status 1
            $this->changeStocktakeProductStatus($stocktake_id, $goods_id, $warehouse_id, self::GOODS_REVIEW_RESTOCKTAKE);

            return true;
        } else {
            return false;
        }
    }

    public function updateConfirmAdjustStock()
    {
        global $db, $ecs, $userController ,$_CFG;
        $stocktake_id = $_REQUEST['stocktake_id'];
        $adjust_stock = $_REQUEST['adjust_stock'];
        $review_person = $_REQUEST['review_person'];
        $goods_id = $_REQUEST['goods_id'];
        $warehouse_id = $_REQUEST['warehouse_id'];

        // stocktake status is REVIEW_PROGRESSING
        $this->changeStocktakeStatus($stocktake_id, self::REVIEW_PROGRESSING);

        $sql = "update ".$ecs->table("erp_goods_stocktake_item")." set adjust_stock = '".$adjust_stock."' where stocktake_id = '".$stocktake_id."' and goods_id = '".$goods_id."' and warehouse_id = '".$warehouse_id."' ";
        $result = $db->query($sql);

        if ($result) {
            $this->changeStocktakeProductStatus($stocktake_id, $goods_id, $warehouse_id, self::GOODS_REVIEW_FINISHED);
            return true;
        } else {
            return false;
        }
    }

    public function updateStocktakeAdjust()
    {
        global $db, $ecs, $userController ,$_CFG;
        $stocktake_id = $_REQUEST['stocktake_id'];
        $adjust_stock = $_REQUEST['adjust_stock'];
        $review_person = $_REQUEST['review_person'];
        $goods_id = $_REQUEST['goods_id'];
        $warehouse_id = $_REQUEST['warehouse_id'];

        // stocktake status is REVIEW_PROGRESSING
        $this->changeStocktakeStatus($stocktake_id, self::REVIEW_PROGRESSING);
       
        //$result = $this->adjustProductStock($sku, $warehouse_id, $adjust_stock, $review_person);
        //if ($result) {
        // change product status 1
        $this->changeStocktakeProductStatus($stocktake_id, $goods_id, $warehouse_id, self::REVIEW_FINISHED);

        return true;
        //} else {
        //    return false;
        //}
    }

    public function submitAdjustProductStock($stocktake_id)
    {
        global $db, $ecs, $userController ,$_CFG;
        
        // check all is GOODS_REVIEW_FINISHED
        $sql = "select count(*) from ". $ecs->table("erp_goods_stocktake_item") ." egsp where stocktake_id = '".$stocktake_id."' and status != '".self::GOODS_REVIEW_FINISHED."'";
        $count = $db->getOne($sql);
        $result = [];
        if ($count > 0) {
            $result['error'] = '有部分產品沒有完成覆核';
            return $result;
        } else {
            $db->query('START TRANSACTION');

            $sql = "select * from ". $ecs->table("erp_goods_stocktake_item") ." egsp where stocktake_id = '".$stocktake_id."' and adjust_stock > 0 ";
            $stocktake_in_goods = $db->getAll($sql);

            if (sizeof($stocktake_in_goods) > 0) {
                $warehouse_id = $stocktake_in_goods[0]['warehouse_id'];
                //add warehousing  (in)
                $warehousing_id=add_warehousing($warehouse_id);
                if ($warehousing_id==-1) {
                    $result['error'] = $GLOBALS['_LANG']['erp_warehousing_without_valid_warehouse'];
                    $db->query('ROLLBACK');
                    return $result;
                } else {
                    // update warehousing_style_id 4
                    $sql = "select egs.*, au.user_name from ". $ecs->table("erp_goods_stocktake") ." egs left join ".$ecs->table("admin_user")." au on (egs.review_person = au.user_id) where stocktake_id = '".$stocktake_id."' ";
                    $stocktake_info = $db->getRow($sql);
                    $user_id=erp_get_admin_id();
                    $lock_time=gmtime();
                    $last_act_time=gmtime();
                    $warehousing_from = '存貨盤點('.$stocktake_id.') '.$stocktake_info['user_name'];
    
                    $sql="update ". $ecs->table("erp_warehousing")." set locked_by='".$user_id."'";
                    $sql.=", locked_time='".$lock_time."'";
                    $sql.=", last_act_time='".$last_act_time."'";
                    $sql.=", is_locked='1'";
                    $sql.=", warehousing_style_id='4'";
                    $sql.=", warehousing_from='".$warehousing_from."'";
                    $sql.=" where warehousing_id='".$warehousing_id."'";
                    $res = $db->query($sql);
                    if ($res) {
                        // add stocktake goods
                        foreach ($stocktake_in_goods as $goods) {
                            $goods_id = $goods['goods_id'];
                            $goods_qty = $goods['adjust_stock'];
                            $sql="select warehousing_item_id from ".$ecs->table('erp_warehousing_item')." where warehousing_id='".$warehousing_id."' and goods_id='".$goods_id."'";
                            $warehousing_item_id=$db->getOne($sql);
                            if (empty($warehousing_item_id)) {
                                $attr_id=get_attr_id($goods_id);
                                $sql="insert into ".$ecs->table('erp_warehousing_item')." set warehousing_id='".$warehousing_id."',goods_id='".$goods_id."',attr_id='".$attr_id."', expected_qty='0',received_qty='".$goods_qty."'";
                                $db->query($sql);
                            } else {
                                $sql="update ".$ecs->table('erp_warehousing_item')." set received_qty=received_qty+'".$goods_qty."' where warehousing_item_id='".$warehousing_item_id."'";
                                $db->query($sql);
                            }
                        }
                
                        $sql="update ".$ecs->table('erp_warehousing')." set status='2',post_by='".erp_get_admin_id()."',post_time='".gmtime()."' where warehousing_id='".$warehousing_id."'";
                        if ($db->query($sql)) {
                        } else {
                            $db->query('ROLLBACK');
                            $result['error'] = '發生錯誤';
                            return $result;
                        }
                    } else {
                        $db->query('ROLLBACK');
                        $result['error'] = '發生錯誤';
                        return $result;
                    }
                }
            }

            $sql = "select * from ". $ecs->table("erp_goods_stocktake_item") ." egsp where stocktake_id = '".$stocktake_id."' and adjust_stock < 0 ";
            $stocktake_out_goods = $db->getAll($sql);

            if (sizeof($stocktake_out_goods) > 0) {
                $warehouse_id = $stocktake_in_goods[0]['warehouse_id'];
                //add warehousing  (out)
                $delivery_id=add_delivery($warehouse_id);
                if ($delivery_id==-1) {
                    $result['error']=$GLOBALS['_LANG']['erp_delivery_without_valid_warehouse'];
                    $db->query('ROLLBACK');
                    return $result;
                } else {
                    $sql = "select egs.*, au.user_name from ". $ecs->table("erp_goods_stocktake") ." egs left join ".$ecs->table("admin_user")." au on (egs.review_person = au.user_id) where stocktake_id = '".$stocktake_id."' ";
                    $stocktake_info = $db->getRow($sql);
                    $user_id=erp_get_admin_id();
                    $lock_time=gmtime();
                    $last_act_time=gmtime();
                    $delivery_to = '存貨盤點('.$stocktake_id.') '.$stocktake_info['user_name'];

                    $sql="update ". $ecs->table("erp_delivery")." set locked_by='".$user_id."'";
                    $sql.=", locked_time='".$lock_time."'";
                    $sql.=", last_act_time='".$last_act_time."'";
                    $sql.=", is_locked='1'";
                    $sql.=", delivery_style_id='4'";
                    $sql.=", delivery_to='".$delivery_to."'";
                    $sql.=" where delivery_id='".$delivery_id."'";
                    $res = $db->query($sql);
                    if ($res) {
                        // add stocktake goods
                        foreach ($stocktake_out_goods as $goods) {
                            $goods_id = $goods['goods_id'];
                            $goods_sn = $goods['goods_sn'];
                            $goods_qty = $goods['adjust_stock'];
                            $goods_qty = $goods_qty * -1;
                            $warehouse_id = $goods['warehouse_id'];

                            $stock=get_goods_stock_by_warehouse($warehouse_id, $goods_id);
                            
                            $sql="select delivery_item_id,delivered_qty from ".$ecs->table('erp_delivery_item')." where delivery_id='".$delivery_id."' and goods_id='".$goods_id."'";
                            $delivery_item_info=$db->getRow($sql);
                            if (empty($delivery_item_info)) {
                                $attr_id=get_attr_id($goods_id);
                                if ($goods_qty>$stock) {
                                    $db->query('ROLLBACK');
                                    $result['error'] = '['.$goods_sn.']'.sprintf($_LANG['erp_delivery_stock_not_enough'], $stock);
                                    return $result;
                                }
                                $sql="insert into ".$ecs->table('erp_delivery_item')." set delivery_id='".$delivery_id."',goods_id='".$goods_id."',attr_id='".$attr_id."', expected_qty='0',delivered_qty='".$goods_qty."'";
                                $db->query($sql);
                            } else {
                                if ($goods_qty+$delivery_item_info['delivered_qty'] >$stock) {
                                    $db->query('ROLLBACK');
                                    $result['error'] = '['.$goods_sn.']'.sprintf($_LANG['erp_delivery_stock_not_enough'], $stock);
                                    return $result;
                                }
                                $sql="update ".$ecs->table('erp_delivery_item')." set delivered_qty=delivered_qty+'".$goods_qty."' where delivery_item_id='".$delivery_item_info['delivery_item_id']."'";
                                $db->query($sql);
                            }
                        }
                
                        $sql="update ".$ecs->table('erp_delivery')." set status='2',post_by='".erp_get_admin_id()."',post_time='".gmtime()."' where delivery_id='".$delivery_id."'";
                        if ($db->query($sql)) {
                            $db->query('COMMIT');
                            return true;
                        } else {
                            $db->query('ROLLBACK');
                            $result['error'] = '發生錯誤';
                            return $result;
                        }
                    } else {
                        $db->query('ROLLBACK');
                        $result['error'] = '發生錯誤';
                        return $result;
                    }
                }
            }

            $db->query('COMMIT');
            return true;
        }
    }

    public function submit_review_finished()
    {
        global $db, $ecs, $userController ,$_CFG;
        $stocktake_id = $_REQUEST['stocktake_id'];
        $review_person = $_REQUEST['review_person'];

        $this->updateStocktakeFinishedCount($stocktake_id);

        $sql = "select count(*) from ". $ecs->table("erp_goods_stocktake") ." egs where stocktake_id = '".$stocktake_id."' and status = '".self::FINISHED ."' ";
        $count = $db->getOne($sql);
        if ($count == 1) {
            $result['error'] = '盤點早經完成覆核';
            return $result;
        }

        // check if all product is reviewed
        $sql = "select count(*) from ". $ecs->table("erp_goods_stocktake_item") ." egsp where stocktake_id = '".$stocktake_id."' and status in ('".self::GOODS_REVIEW_PENDING."' )";
        $count = $db->getOne($sql);
        $result = [];
        if ($count > 0) {
            $result['error'] = '有部分產品沒有完成覆核';
            return $result;
        } else {
            // update review persion
            $user_id=erp_get_admin_id();
            $sql="update ". $ecs->table("erp_goods_stocktake")." set review_person='".$user_id."' where stocktake_id = '".$stocktake_id."' ";
            $db->query($sql);

            $sql = "select count(*) from ". $ecs->table("erp_goods_stocktake_item") ." egsp where stocktake_id = '".$stocktake_id."' and status = '".self::GOODS_REVIEW_RESTOCKTAKE."' ";
            $count = $db->getOne($sql);
            $result = [];
            if ($count > 0) {
                // becasue having re-stocktake, so change status to "PENDING_FOR_STOCKTAKE"
                $this->changeStocktakeStatus($stocktake_id, self::PENDING_FOR_STOCKTAKE);
                return self::PENDING_FOR_STOCKTAKE;
            } else {
                $result2 = $this->submitAdjustProductStock($stocktake_id);

                if (isset($result2['error'])) {
                    return $result2;
                } else {

                    // record last stocktake time (finished time)
                    $this->saveStocktaketime($stocktake_id);

                    // change status to "FINISHED"
                    $this->changeStocktakeStatus($stocktake_id, self::FINISHED);
                    
                    return self::FINISHED;
                }
            }
        }
    }

    public function saveStocktaketime($stocktake_id)
    {
        global $db, $ecs, $userController ,$_CFG;
        $sql = "select * from ".$ecs->table("erp_goods_stocktake_item")." where stocktake_id = '".$stocktake_id."' " ;
        $stocktakeProducts = $db->getAll($sql);
        
        foreach ($stocktakeProducts as $product) {
            // get stocktake_end_time
            if ($product['stocktake_end_time'] != '' && $product['goods_id'] != '') {
                $sql = "update ".$ecs->table("goods")." set last_stocktake_time = '".$product['stocktake_end_time']."' where goods_id = '".$product['goods_id']."' ";
                $db->query($sql);
            }
        }
    }

    public function searchProductByBrand($is_pagination = true)
    {
        global $db, $ecs, $userController ,$_CFG;
    
        $warehouse_id = $_REQUEST['warehouse_id'];
       
        if (empty($warehouse_id)) {
            return false;
        }

        $brand_ids = $_REQUEST['brand_ids'];
        $period = $_REQUEST['period'];
        $is_on_sale = $_REQUEST['is_on_sale'];
        $in_stock = $_REQUEST['in_stock'];
        $cat_id = $_REQUEST['cat_id'];

        if (empty($brand_ids) && empty($period) && empty($cat_id)) {
            return false;
        }

        // $brand_ids = explode(',',$brand_ids);
        // echo '<pre>';
        // print_r($brand_ids);

        if (!is_array($brand_ids) && !empty($brand_ids) ) {
            $temp[] = $brand_ids;
            $brand_ids = $temp;
        }

        $warehouse_info = get_warehouse_list(0, 0, 1);
        $warehouse_name = $warehouse_info[$warehouse_id]['name'];
   
        // total record
        $sql = "select g.goods_id, g.goods_sn, update_time from  ".$ecs->table('goods')." g left join ".$ecs->table('erp_goods_attr_stock')." egas on (g.goods_id = egas.goods_id and egas.warehouse_id = ".$warehouse_id.")
        left join (". "select * from ( SELECT DISTINCT warehouse_id, goods_id, update_time FROM  ".$ecs->table('erp_goods_stocktake_item')." egsti_t left join ".$ecs->table('erp_goods_stocktake')."  egs_t  on( egsti_t.stocktake_id = egs_t.stocktake_id) where egs_t.status = ".self::FINISHED." and warehouse_id = ".$warehouse_id." and update_time is not null  order by update_time desc ) as t  group by warehouse_id, goods_id" .") t2 on (egas.goods_id = t2.goods_id)  
        where 1 " ;
        if (isset($brand_ids) &&!empty($brand_ids) && sizeof($brand_ids) > 0  ) {
            $sql.=" AND g.brand_id IN ('".implode("','", $brand_ids)."')";
        }

        if (!empty($_REQUEST['period'])) {
            $end_time_period= local_strtotime(date("Y-m-d", strtotime( date( 'Y-m-d' )." -0 months")));
            $target_time_period  = local_strtotime(date("Y-m-d", strtotime( date( 'Y-m-d' )." -".$_REQUEST['period']." days")));           
            $sql.=" AND ( update_time <= ".$target_time_period."  or update_time is null)  ";
        }

        if (isset($is_on_sale) && $is_on_sale != '') {
            $sql.=" AND g.is_on_sale = ".$is_on_sale." ";
        }

        if (isset($in_stock) && $in_stock > 0) {
            $sql.=" AND egas.goods_qty > 0 ";
        }

        if (isset($cat_id) && $cat_id > 0) {
            $sql.=" AND g.cat_id = '".$cat_id."' ";
        }

        $sql.=" AND g.is_delete = '0'";

        $total_goods = $db->getAll($sql);
        $total_goods_ids = [];
        $total_goods_sns = [];

        // echo '<pre>';
        // echo sizeof($total_goods);
        // exit;

        foreach ($total_goods as $good) {
            $total_goods_ids[] = $good['goods_id'];
            $total_goods_sns[] = $good['goods_sn'];
        }

        $total_count = sizeof($total_goods_ids);

        $_REQUEST['record_count'] = $total_count;
    
        $sql = "select g.goods_id, g.goods_sn, g.goods_name, g.goods_thumb, g.goods_img, g.original_img, g.last_stocktake_time, update_time , IFNULL(egas.goods_qty, 0) as goods_qty from  ".$ecs->table('goods')." g left join ".$ecs->table('erp_goods_attr_stock')." egas on (g.goods_id = egas.goods_id and egas.warehouse_id = ".$warehouse_id.") 
        left join (". "select * from ( SELECT DISTINCT warehouse_id, goods_id, update_time FROM  ".$ecs->table('erp_goods_stocktake_item')." egsti_t left join ".$ecs->table('erp_goods_stocktake')."  egs_t  on( egsti_t.stocktake_id = egs_t.stocktake_id) where egs_t.status = ".self::FINISHED." and warehouse_id = ".$warehouse_id." and update_time is not null  order by update_time desc ) as t  group by warehouse_id, goods_id" .") t2 on (egas.goods_id = t2.goods_id)  
        where 1 " ;
        
        if (isset($brand_ids) &&!empty($brand_ids)) {
            $sql.=" AND g.brand_id IN ('".implode("','", $brand_ids)."')";
        }

        if (!empty($_REQUEST['period'])) {
            $end_time_period= local_strtotime(date("Y-m-d", strtotime( date( 'Y-m-d' )." -0 months")));
            $target_time_period  = local_strtotime(date("Y-m-d", strtotime( date( 'Y-m-d' )." -".$_REQUEST['period']." days")));           
            $sql.=" AND ( update_time <= ".$target_time_period."  or update_time is null)  ";
        }

        if (isset($is_on_sale) && $is_on_sale != '') {
            $sql.=" AND g.is_on_sale = ".$is_on_sale." ";
        }

        if (isset($in_stock) && $in_stock > 0) {
            $sql.=" AND egas.goods_qty > 0 ";
        }

        if (isset($cat_id) && $cat_id > 0) {
            $sql.=" AND g.cat_id = '".$cat_id."' ";
        }

        $sql.=" AND g.is_delete = '0'";
        $sql.=" ORDER BY goods_id ASC";
        
        $sql .= ($is_pagination ? " LIMIT " . $_REQUEST['start'] . ", " . $_REQUEST['page_size'] : '');
    
        $result = $db->getAll($sql);

        foreach ($result as &$goods) {
            $goods['warehouse_name'] = $warehouse_name;
            if (!empty($goods['update_time'])) {
                $goods['last_stocktake_date'] = local_date($_CFG['date_format'], $goods['update_time']);
            } else {
                $goods['last_stocktake_date'] = null;
            }
        }

        $extra_data['total_goods_ids'] = implode(',', $total_goods_ids);
        $extra_data['total_goods_sns'] = implode(',', $total_goods_sns);

        $arr = array(
            'data' => $result,
            'extra_data' => $extra_data,
            'record_count' => $_REQUEST['record_count']
            );
        return $arr;
    }

    public function searchProductBySkuCheckExist()
    {
        global $db, $ecs, $userController ,$_CFG;

        $sku_data = $_REQUEST['sku_data'];
        $sku_data = explode(PHP_EOL, $sku_data);

        if (!is_array($sku_data)) {
            $temp[] = $sku_data;
            $sku_data = $temp;
        }

        // total record
        $sql = "select g.goods_id, g.goods_sn from  ".$ecs->table('goods')." g where 1 " ;
        if (isset($sku_data) && !empty($sku_data)) {
            $sql.=" AND g.goods_sn IN ('".implode("','", $sku_data)."')";
        }
        $sql.=" AND g.is_delete = '0'";

        $total_goods = $db->getAll($sql);
        $total_goods_sns = [];
        foreach ($total_goods as $good) {
            $total_goods_sns[] = $good['goods_sn'];
        }

        $result = [];
        foreach ($sku_data as $sku) {
            if (trim($sku) != '') {
                if (!in_array($sku,$total_goods_sns)) {
                    $result['error'][] = '貨號['.$sku.'] 在系統找不到!';
                }
            }
        }

        return $result;
    }

    public function searchProductBySku($is_pagination = true)
    {
        global $db, $ecs, $userController ,$_CFG;
    
        $warehouse_id = $_REQUEST['warehouse_id'];

        if (empty($warehouse_id)) {
            return false;
        }

        $sku_data = $_REQUEST['sku_data'];
        $sku_data = explode(PHP_EOL, $sku_data);

        if (!is_array($sku_data)) {
            $temp[] = $sku_data;
            $sku_data = $temp;
        }

        $warehouse_info = get_warehouse_list(0, 0, 1);
        $warehouse_name = $warehouse_info[$warehouse_id]['name'];

        // total record
        $sql = "select g.goods_id, g.goods_sn from ".$ecs->table('goods')." g where 1 " ;
        if (isset($sku_data) && !empty($sku_data)) {
            $sql.=" AND g.goods_sn IN ('".implode("','", $sku_data)."')";
        }
        $sql.=" AND g.is_delete = '0'";
    
        $total_goods = $db->getAll($sql);
        $total_goods_ids = [];
        $total_goods_sns = [];
        foreach ($total_goods as $good) {
            $total_goods_ids[] = $good['goods_id'];
            $total_goods_sns[] = $good['goods_sn'];
        }

        $total_count = sizeof($total_goods_ids);

        $_REQUEST['record_count'] = $total_count;
     
        // foreach ($sku_data as $sku) {
        //     if (!in_array($sku,$total_goods_sns)) {
        //         $result['error'][] = '貨號['.$sku.'] 在系統找不到!';
        //     }
        // }

        if (isset($result['error'])) {
            return $result;
        }
        
        $sql = "select g.goods_id, g.goods_sn, g.goods_name, g.goods_thumb, g.goods_img, g.original_img, g.last_stocktake_time, egas.goods_qty ";
        $sql .= "from  ".$ecs->table('goods')." g left join ".$ecs->table('erp_goods_attr_stock')." egas on (g.goods_id = egas.goods_id and egas.warehouse_id = ".$warehouse_id.") where 1 " ;
        if (isset($sku_data) &&!empty($sku_data)) {
            $sql.=" AND g.goods_sn IN ('".implode("','", $sku_data)."')";
        }

        $sql.=" AND g.is_delete = '0'";
        $sql.=" ORDER BY goods_id ASC";
        $sql .= ($is_pagination ? " LIMIT " . $_REQUEST['start'] . ", " . $_REQUEST['page_size'] : '');
    
        $result = $db->getAll($sql);

        foreach ($result as &$goods) {
            $goods['warehouse_name'] = $warehouse_name;
            if (!empty($goods['last_stocktake_time'])){
                $goods['last_stocktake_date'] = local_date($_CFG['date_format'], $goods['last_stocktake_time']);    
            } else {
                $goods['last_stocktake_date'] = null;
            }
        }

        $extra_data['total_goods_ids'] = implode(',', $total_goods_ids);
        $extra_data['total_goods_sns'] = implode(',', $total_goods_sns);

        $arr = array(
            'data' => $result,
            'extra_data' => $extra_data,
            'record_count' => $_REQUEST['record_count']
            );
        return $arr;
    }

    public function bulkStocktakeGoodsStatusChange()
    {
        global $db, $ecs, $userController ,$_CFG;

        if (!empty($_REQUEST['goods_ids']) && $_REQUEST['change_status']) {
            foreach ($_REQUEST['goods_ids'] as $goods_id) {
                $sql = "update ".$ecs->table('erp_goods_stocktake_item')." set status = '".$_REQUEST['change_status']."' where goods_stocktake_item_id = '".$goods_id."' ";
                $db->query($sql);
            }
        }

        return true;
    }

    public function stocktakeGoodsStatusChange()
    {
        global $db, $ecs, $userController ,$_CFG;

        // stocktake status is REVIEW_PROGRESSING
        $stocktake_id = $_REQUEST['stocktake_id'];
        $this->changeStocktakeStatus($stocktake_id, self::REVIEW_PROGRESSING);
        
        if (!empty($_REQUEST['item_id']) && $_REQUEST['change_status']) {
            $sql = "update ".$ecs->table('erp_goods_stocktake_item')." set status = '".$_REQUEST['change_status']."' where goods_stocktake_item_id = '".$_REQUEST['item_id']."' ";
            $db->query($sql);
        }

        return true;
    }

    public function get_goods_stock_druing_stocktake($stocktake_id, $stocktake_start_time, $stocktake_end_time)
    {
        global $db, $ecs, $userController ,$_CFG;
        
        // get the qty at the start of stocktake
        $sql="select egsp.goods_id, egsp.warehouse_id, es.act_date, IF(es.stock IS NULL or es.stock = '', '0', es.stock) as stock  from ". $ecs->table('erp_goods_stocktake_item')." egsp left join ".$ecs->table('erp_stock')." es on ( egsp.goods_id = es.goods_id and egsp.warehouse_id = es.warehouse_id and act_time < ".$stocktake_start_time." ) where egsp.stocktake_id = '".$stocktake_id."' and egsp.status in ( ".self::GOODS_STOCKTAKE_PENDING." , ".self::GOODS_REVIEW_RESTOCKTAKE.", ".self::GOODS_STOCKTAKE_FINISHED." ) order by stock_id desc ";
        $goods_start_qtys=$db->getAll($sql);

        //	print_r($goods_start_qtys);
        $goods_start_qty_ar = [];
        foreach ($goods_start_qtys as $goods_start_qty) {
            if (!isset($goods_start_qty_ar[$goods_start_qty['goods_id'].'_'.$goods_start_qty['warehouse_id']])) {
                $goods_start_qty_ar[$goods_start_qty['goods_id'].'_'.$goods_start_qty['warehouse_id']] = $goods_start_qty['stock'];
            }
        }

        // get the qty change at the during stocktake
        $sql="select egsp.goods_id, egsp.warehouse_id, es.act_date, IF(es.qty IS NULL or es.qty = '', '0', es.qty) as qty , IF(es.stock IS NULL or es.stock = '', '0', es.stock) as stock  from ". $ecs->table('erp_goods_stocktake_item')." egsp left join ".$ecs->table('erp_stock')." es on ( egsp.goods_id = es.goods_id and egsp.warehouse_id = es.warehouse_id and act_time >= ".$stocktake_start_time." and act_time <= ".$stocktake_end_time." ) where egsp.stocktake_id = '".$stocktake_id."' and egsp.status in ( ".self::GOODS_STOCKTAKE_PENDING." , ".self::GOODS_REVIEW_RESTOCKTAKE.", ".self::GOODS_STOCKTAKE_FINISHED." )  order by stock_id desc ";

        $goods_changed_qtys=$db->getAll($sql);

        //print_r($goods_changed_qtys);
        $goods_changed_qty_ar = [];
        foreach ($goods_changed_qtys as $goods_changed_qty) {
            $goods_changed_qty_ar[$goods_changed_qty['goods_id'].'_'.$goods_changed_qty['warehouse_id']] += $goods_changed_qty['qty'];
        }

        // get the qty at the end of stocktake
        $sql="select egsp.goods_id, egsp.warehouse_id, es.act_date, IF(es.stock IS NULL or es.stock = '', '0', es.stock) as stock  from ". $ecs->table('erp_goods_stocktake_item')." egsp left join ".$ecs->table('erp_stock')." es on ( egsp.goods_id = es.goods_id and egsp.warehouse_id = es.warehouse_id and act_time <= ".$stocktake_end_time." ) where egsp.stocktake_id = '".$stocktake_id."' and egsp.status in ( ".self::GOODS_STOCKTAKE_PENDING." , ".self::GOODS_REVIEW_RESTOCKTAKE.", ".self::GOODS_STOCKTAKE_FINISHED." )  order by stock_id desc ";
        $goods_end_qtys=$db->getAll($sql);

        //	print_r($goods_start_qtys);
        $goods_end_qty_ar = [];
        foreach ($goods_end_qtys as $goods_end_qty) {
            if (!isset($goods_end_qty_ar[$goods_end_qty['goods_id'].'_'.$goods_end_qty['warehouse_id']])) {
                $goods_end_qty_ar[$goods_end_qty['goods_id'].'_'.$goods_end_qty['warehouse_id']] = $goods_end_qty['stock'];
            }
        }

        return array('goods_start_qty_ar' => $goods_start_qty_ar,
                     'goods_changed_qty_ar' => $goods_changed_qty_ar,
                     'goods_end_qty_ar' => $goods_end_qty_ar
                );
    }

    public function stocktake_create_by_import()
    {
        global $db, $ecs, $userController ,$_CFG;
        $stocktake_data = $_REQUEST['stocktake_data'];
        $stocktake_start_time = local_strtotime($_REQUEST['stocktake_start_time']);
        $stocktake_end_time = local_strtotime($_REQUEST['stocktake_end_time']);

        if ($stocktake_end_time < $stocktake_start_time) {
            return $result['error'] = '盤點時間不正確!';
        }

        $warehouse_id = $_REQUEST['warehouse_id'];
        $title = $_REQUEST['title'];
        $stocktake_person = $_REQUEST['stocktake_person'];
        $stocktake_data = explode(PHP_EOL, $stocktake_data);
       
        if (empty($stocktake_data) || empty($stocktake_start_time) || empty($stocktake_end_time) || empty($warehouse_id) || empty($stocktake_person) || empty($title)) {
            return false;
        }

        // combine product for the duplicateion
        // get all goods sn
        $result = [];
        $update_data = [];
        $stocktake_goods_sns = [];
        foreach ($stocktake_data as $stocktake) {
            if (trim($stocktake) != '') {
                $stocktake_info = explode(',', $stocktake);
                if (sizeof($stocktake_info) != 2) {
                    $result['error'][] = '['.$stocktake.']資料格式錯誤!';
                }
                
                $stocktake_sku = $stocktake_info[0];
                $theFirstChar = substr($stocktake_sku, 0, 1);
                if ($theFirstChar == '0') {
                    $stocktake_sku_temp = substr($stocktake_sku, 1);
                    // try to search whitout 0
                    // checking product exist
                    $sql = "select count(*) from ".$ecs->table('goods')." where goods_sn = '".$stocktake_sku_temp."' " ;
                    $count = $db->getOne($sql);
                    
                    if ($count == 1) {
                        $stocktake_sku = $stocktake_sku_temp;
                    }
                }
               
                if (!in_array($stocktake_sku, $stocktake_goods_sns)) {
                    $stocktake_goods_sns[] = $stocktake_sku;
                }

                $update_data[$stocktake_sku]['goods_sn'] = $stocktake_sku;

                $update_data[$stocktake_sku]['stock'] += $stocktake_info[1];
            }
        }

        if (isset($result['error'])) {
            return $result;
        }

        if (empty($stocktake_goods_sns)) {
            return $result['error'] = '資料格式錯誤!';
        }

        // checking product exist
        $sql = "select goods_sn,goods_id from ".$ecs->table('goods')." where goods_sn in ('".implode("','", $stocktake_goods_sns)."' )" ;
        $res = $db->getAll($sql);
        $goods_info_by_sn = [];
        foreach ($res as $goods) {
            $goods_info_by_sn[$goods['goods_sn']] = $goods['goods_id'];
            $update_data[$goods['goods_sn']]['goods_id'] = $goods['goods_id'];
        }

        foreach ($stocktake_goods_sns as $stocktake_goods_sn) {
            if (!array_key_exists($stocktake_goods_sn, $goods_info_by_sn)) {
                $result['error'][] = '貨號['.$stocktake_goods_sn.'] 在系統找不到!';
            }
        }

        if (isset($result['error'])) {
            return $result;
        }

        // create erp_goods_stocktake
        $q = "INSERT INTO ".$ecs->table('erp_goods_stocktake')." (	create_date ,status, title, stocktake_person, stocktake_finished_rate ) VALUE ('".gmtime()."','0','".$title."', '".$stocktake_person."', 100)";
        
        if ($this->db->query($q)) {
            $new_insert_id = $this->db->Insert_ID();
            // create erp_goods_stocktake_item
            foreach ($goods_info_by_sn as $goods_sn => $goods_id) {
                $q = "INSERT INTO ".$ecs->table('erp_goods_stocktake_item')." ( stocktake_id, warehouse_id, goods_id, goods_sn ) VALUE ('".$new_insert_id."','".$warehouse_id."','".$goods_id."','".$goods_sn."')";
                $this->db->query($q);
            }
        } else {
            return false;
        }

        $stocktake_id = $new_insert_id;
        $goods_stock_druing_stocktake = $this->get_goods_stock_druing_stocktake($stocktake_id, $stocktake_start_time, $stocktake_end_time);
        $success_count = 0;
        foreach ($update_data as $data) {
            // update stork's history of during the stocktake
            $start_stock = $goods_stock_druing_stocktake['goods_start_qty_ar'][$data['goods_id'].'_'.$warehouse_id];
            $changed_stock = $goods_stock_druing_stocktake['goods_changed_qty_ar'][$data['goods_id'].'_'.$warehouse_id];
            $end_stock = $goods_stock_druing_stocktake['goods_end_qty_ar'][$data['goods_id'].'_'.$warehouse_id];

            $sql = 'update ' . $ecs->table('erp_goods_stocktake_item') .' set stocktake_start_time = "'.$stocktake_start_time.'", stocktake_end_time = "'.$stocktake_end_time.'", start_stock = "'.$start_stock.'" , changed_stock = "'.$changed_stock.'", end_stock = "'.$end_stock.'", stock = "'.$data['stock'].'" , update_time = "'.gmtime().'", status = "'.self::GOODS_STOCKTAKE_FINISHED.'" where goods_sn = "'.$data['goods_sn'].'" and stocktake_id = "'.$stocktake_id.'" and (status = "'.self::GOODS_STOCKTAKE_PENDING.'" or status = "'.self::GOODS_REVIEW_RESTOCKTAKE.'"  or status = "'.self::GOODS_STOCKTAKE_FINISHED.'" ) ';

            $db->query($sql);
            $success_count ++;
        }

        //$sql = "update erp_goods_stocktake set ";

        return $new_insert_id;
    }

    public function stocktake_create()
    {
        global $db, $ecs, $userController ,$_CFG;
        $all_goods_ids = $_REQUEST['all_goods_ids'];
        $all_goods_ids_ar = explode(',', $all_goods_ids);
        $all_goods_sns = $_REQUEST['all_goods_sns'];
        $all_goods_sns_ar = explode(',', $all_goods_sns);
        $warehouse_id = $_REQUEST['warehouse_id'];
        $title = $_REQUEST['title'];
        $stocktake_person = $_REQUEST['stocktake_person'];

        // create erp_goods_stocktake
        $this->db->query('SET NAMES utf8');
        $q = "INSERT INTO ".$ecs->table('erp_goods_stocktake')." (	create_date ,status, title, stocktake_person ) VALUE ('".gmtime()."','0','".$title."', '".$stocktake_person."')";
    
        if ($this->db->query($q)) {
            $new_insert_id = $this->db->Insert_ID();
            // create erp_goods_stocktake_item
            foreach ($all_goods_ids_ar as $goods_key => $goods_id) {
                $goods_sn = $all_goods_sns_ar[$goods_key];
                $q = "INSERT INTO ".$ecs->table('erp_goods_stocktake_item')." ( stocktake_id, warehouse_id, goods_id, goods_sn ) VALUE ('".$new_insert_id."','".$warehouse_id."','".$goods_id."','".$goods_sn."')";
                $this->db->query($q);
            }
        } else {
            return false;
        }

        return $new_insert_id;
    }

    public function changeStocktakeStatus($stocktake_id, $status_id)
    {
        global $db, $ecs, $userController ,$_CFG;
        $sql = "update ".$ecs->table('erp_goods_stocktake')." set status = '".$status_id."' where stocktake_id = '".$stocktake_id."' ";
        $db->query($sql);

        return true;
    }

    public function changeStocktakeProductStatus($stocktake_id, $goods_id, $warehouse_id, $status_id)
    {
        global $db, $ecs, $userController ,$_CFG;
        $sql = "update ".$ecs->table('erp_goods_stocktake_item')." set status = '".$status_id."' where stocktake_id = '".$stocktake_id."' and goods_id = '".$goods_id."' and warehouse_id = '".$warehouse_id."' ";
        $db->query($sql);
    }

    public function submitReviewRequest()
    {
        global $db, $ecs, $userController ,$_CFG;
        $stocktake_id = $_REQUEST['stocktake_id'];
        $force_submit = $_REQUEST['force_submit'];
        // check if all product is stocktake
        $sql = "select count(*) from ". $ecs->table("erp_goods_stocktake_item") ." egsp where stocktake_id = '".$stocktake_id."' and status in ('".self::GOODS_STOCKTAKE_PENDING."', '".self::GOODS_REVIEW_RESTOCKTAKE."' )";
        $count = $db->getOne($sql);
        $result = [];
        if ($count > 0 && $force_submit == 0 ) {
            $result['error'] = '有部分產品沒有完成盤點<br>如果繼續提交,沒有盤點數目的會變 0 <input style="margin-top: -1px;" class="btn btn-warning form-btn  force_submit_review" type="button" id="force_submit_review" name="force_submit_review" value="繼續提交">';
            return $result;
        } else {

            // get the qty at the start of stocktake
            $stocktake_start_time = gmtime();
            $stocktake_end_time = gmtime();
            $sql="select egsp.goods_id, egsp.warehouse_id, es.act_date, IF(es.stock IS NULL or es.stock = '', '0', es.stock) as stock  from ". $ecs->table('erp_goods_stocktake_item')." egsp left join ".$ecs->table('erp_stock')." es on ( egsp.goods_id = es.goods_id and egsp.warehouse_id = es.warehouse_id and act_time < ".$stocktake_start_time." ) where egsp.stocktake_id = '".$stocktake_id."' and egsp.status in ( ".self::GOODS_STOCKTAKE_PENDING." , ".self::GOODS_REVIEW_RESTOCKTAKE.", ".self::GOODS_STOCKTAKE_FINISHED." ) order by stock_id desc ";
            $goods_start_qtys=$db->getAll($sql);
    
            //	print_r($goods_start_qtys);
            $goods_start_qty_ar = [];
            foreach ($goods_start_qtys as $goods_start_qty) {
                if (!isset($goods_start_qty_ar[$goods_start_qty['goods_id']])) {
                    $goods_start_qty_ar[$goods_start_qty['goods_id']] = $goods_start_qty['stock'];
                }
            }
        
            foreach ($goods_start_qty_ar as $goods_id => $goods_start_qty) {
                $sql = "update ".$ecs->table('erp_goods_stocktake_item')." set stocktake_start_time = ".gmtime().", stocktake_end_time = ".gmtime().", local_pick_up_stock = 0, start_stock = '".$goods_start_qty."', changed_stock=0, end_stock = '".$goods_start_qty."', update_time = ".gmtime().", stock=0, status = '".self::GOODS_STOCKTAKE_FINISHED."'  where stocktake_id = '".$stocktake_id."' and goods_id = ".$goods_id." and status in ('".self::GOODS_STOCKTAKE_PENDING."', '".self::GOODS_REVIEW_RESTOCKTAKE."' ) ";
                $db->query($sql);     
            }

           
            $this->changeStocktakeStatus($stocktake_id, self::WAITING_FOR_REVIEW);
          
            // if stocktake qty = end stock + changed_stock - local_pick_up_stock -> GOODS_REVIEW_FINISHED
            $sql = "update ".$ecs->table('erp_goods_stocktake_item')." set status = '".self::GOODS_REVIEW_FINISHED."', adjust_stock = 0 where stocktake_id = '".$stocktake_id."' and status in ('".self::GOODS_STOCKTAKE_FINISHED."', '".self::GOODS_REVIEW_PENDING."' ) and end_stock = stock + changed_stock ";
            
            $db->query($sql);

            // GOODS_STOCKTAKE_PENDING to GOODS_REVIEW_PENDING
            $sql = "update ".$ecs->table('erp_goods_stocktake_item')." set status = '".self::GOODS_REVIEW_PENDING."' where stocktake_id = '".$stocktake_id."' and status = '".self::GOODS_STOCKTAKE_FINISHED."' ";
            $db->query($sql);
            
            return true;
        }
    }

    public function updateStocktakeFinishedCount($stocktake_id)
    {
        global $db, $ecs, $userController ,$_CFG;

        $sql = "select count(*) from ".$ecs->table('erp_goods_stocktake_item')." where stocktake_id = '".$stocktake_id."' ";
        $total_items = $db->getOne($sql);
        
        // stocktake
        $stocktake_finished_status = array(self::GOODS_STOCKTAKE_FINISHED,self::GOODS_REVIEW_PENDING,self:: GOODS_REVIEW_FINISHED);

        $sql = "select count(*) from ".$ecs->table('erp_goods_stocktake_item')." where stocktake_id = '".$stocktake_id."' and status in (".implode(',', $stocktake_finished_status).") ";
        $count = $db->getOne($sql);

        $rate = floor($count / $total_items * 100);
        $sql = "update ".$ecs->table('erp_goods_stocktake')." set stocktake_finished_rate = '".$rate."' where stocktake_id = '".$stocktake_id."' ";

        $db->query($sql);

        // review
        $stocktake_review_status = array(self:: GOODS_REVIEW_FINISHED);

        $sql = "select count(*) from ".$ecs->table('erp_goods_stocktake_item')." where stocktake_id = '".$stocktake_id."' and status in (".implode(',', $stocktake_review_status).") ";
        $count = $db->getOne($sql);

        $rate = floor($count / $total_items * 100);
        $sql = "update ".$ecs->table('erp_goods_stocktake')." set review_finished_rate = '".$rate."' where stocktake_id = '".$stocktake_id."' ";
        $count = $db->getOne($sql);
    }

    public function get_goods_stock_local_pick_up_before_stocktake($stocktake_id, $stocktake_start_time)
    {
        global $db, $ecs, $userController ,$_CFG;

        $thirty_days_ago = strtotime("-30 day", $stocktake_start_time);;
        //echo local_date($_CFG['time_format'], $thirty_days_ago);
        $sql = "select egsi.goods_id, sum(dg.send_number) as total ";
        $sql .= "from ".$ecs->table("erp_goods_stocktake_item")." egsi ";
        $sql .= "inner join ".$ecs->table("delivery_goods")." dg on (egsi.goods_id = dg.goods_id and egsi.stocktake_id = ".$stocktake_id." ) ";
        $sql .= "left join ".$ecs->table("delivery_order")." do on (dg.delivery_id = do.delivery_id) ";
        //$sql .= "and (do.shipping_id = 5 or do.shipping_id = 2) and do.status = 0 and do.add_time < ".$stocktake_start_time." and do.add_time >= ".$thirty_days_ago." ";
        $sql .= "and (logistics_id = 7) and do.status = 0 and do.add_time < ".$stocktake_start_time." and do.add_time >= ".$thirty_days_ago." ";
        $sql .= "group by goods_id ";
        $res = $db->getAll($sql);
        
        $localt_pick_up_stock = [];
        foreach ($res as $good) {
            $localt_pick_up_stock[$good['goods_id']] = $good['total'];
        }
        
        return $localt_pick_up_stock;
    }

    public function importStocktakeData()
    {
        global $db, $ecs, $userController ,$_CFG;
        //	check exist
        //  update product stock
        $stocktake_data = $_REQUEST['stocktake_data'];
        $stocktake_start_time = local_strtotime($_REQUEST['stocktake_start_time']);
        $stocktake_end_time = local_strtotime($_REQUEST['stocktake_end_time']);
        $stocktake_id = $_REQUEST['stocktake_id'];
        $stocktake_data = explode(PHP_EOL, $stocktake_data);
        
        // only count still haven't picked order.
        $goods_local_pick_up_qty_ar = $this->get_goods_stock_local_pick_up_before_stocktake($stocktake_id, $stocktake_start_time);

        $error = [];
        $erpController = new ErpController();
        $warehouse_ids = $erpController->getSellableWarehouseIds(true);
        $goods_stock_druing_stocktake = $this->get_goods_stock_druing_stocktake($stocktake_id, $stocktake_start_time, $stocktake_end_time);
        $goods_start_qty_ar = $goods_stock_druing_stocktake['goods_start_qty_ar'];
        $goods_changed_qty_ar = $goods_stock_druing_stocktake['goods_changed_qty_ar'];
        $goods_end_qty_ar = $goods_stock_druing_stocktake['goods_end_qty_ar'];

        //echo '<pre>';
        //print_r($goods_start_qty_ar);
        //print_r($goods_changed_qty_ar);
        //print_r($goods_end_qty_ar);

        // get all goods sn
        $stocktake_goods_sns = [];
        foreach ($stocktake_data as $stocktake) {
            $stocktake_info = explode(',', $stocktake);
            $stocktake_sku = $stocktake_info[0];
            $theFirstChar = substr($stocktake_sku, 0, 1);
            if ($theFirstChar == '0') {
                $stocktake_sku_temp = substr($stocktake_sku, 1);
                // checking product exist
                $sql = "select count(*) from ".$ecs->table('goods')." where goods_sn = '".$stocktake_sku_temp."' " ;
                $count = $db->getOne($sql);

                if ($count == 1) {
                    $stocktake_sku = $stocktake_sku_temp;
                }
            }

            if (!in_array($stocktake_sku,$stocktake_goods_sns)) {
                $stocktake_goods_sns[] = $stocktake_sku;
            }
        }

        // check stocktake list exist
        $sql = 'select goods_id, goods_sn, warehouse_id from '. $ecs->table('erp_goods_stocktake_item') .' egsp where egsp.stocktake_id = "'.$stocktake_id.'" ';

        $result = $db->getAll($sql);
        $stocktake_info_by_sn = [];
        foreach ($result as $stocktake) {
            $stocktake_info_by_sn[$stocktake['goods_sn']]['goods_id'] = $stocktake['goods_id'];
            $stocktake_info_by_sn[$stocktake['goods_sn']]['warehouse_id'] = $stocktake['warehouse_id'];
        }
        
        // get stocktake
        $success_count = 0;
        $update_data = [];
        foreach ($stocktake_data as $stocktake) {
            if (empty(trim($stocktake))) {
                continue;
            }

            $stocktake_info = explode(',', $stocktake);
            $stocktake_sku = $stocktake_info[0];
            $theFirstChar = substr($stocktake_sku, 0, 1);
            if ($theFirstChar == '0') {
                $stocktake_sku_temp = substr($stocktake_sku, 1);
                // checking product exist
                $sql = "select count(*) from ".$ecs->table('goods')." where goods_sn = '".$stocktake_sku_temp."' " ;
                $count = $db->getOne($sql);

                if ($count == 1) {
                    $stocktake_sku = $stocktake_sku_temp;
                }
            }

            $stocktake_goods_sn = $stocktake_sku;
            $stocktake_goods_stock = $stocktake_info[1];

            $update_data[$stocktake_goods_sn]['goods_sn'] = $stocktake_goods_sn;
            $update_data[$stocktake_goods_sn]['stock'] += $stocktake_goods_stock;

            // if (array_key_exists($stocktake_goods_sn, $stocktake_info_by_sn)) {
            //     // update stork's history of during the stocktake
            //     $start_stock = $goods_start_qty_ar[$stocktake_info_by_sn[$stocktake_goods_sn]['goods_id'].'_'.$stocktake_info_by_sn[$stocktake_goods_sn]['warehouse_id']];
            //     $changed_stock = $goods_changed_qty_ar[$stocktake_info_by_sn[$stocktake_goods_sn]['goods_id'].'_'.$stocktake_info_by_sn[$stocktake_goods_sn]['warehouse_id']];
            //     $end_stock = $goods_end_qty_ar[$stocktake_info_by_sn[$stocktake_goods_sn]['goods_id'].'_'.$stocktake_info_by_sn[$stocktake_goods_sn]['warehouse_id']];
            // 
            //     $sql = 'update ' . $ecs->table('erp_goods_stocktake_item') .' set stocktake_start_time = "'.$stocktake_start_time.'", stocktake_end_time = "'.$stocktake_end_time.'", start_stock = "'.$start_stock.'" , changed_stock = "'.$changed_stock.'", end_stock = "'.$end_stock.'", stock = "'.$stocktake_info[1].'" , update_time = "'.gmtime().'", status = "'.self::GOODS_STOCKTAKE_FINISHED.'" where goods_sn = "'.$stocktake_goods_sn.'" and stocktake_id = "'.$stocktake_id.'" and status in ( "'.self::GOODS_STOCKTAKE_PENDING.'" ,  "'.self::GOODS_REVIEW_RESTOCKTAKE.'" , "'.self::GOODS_STOCKTAKE_FINISHED.'"  ) ';
            //     $db->query($sql);
            // 
            //     $success_count ++;
            // } else {
            //     $error[] = '在盤點清單找不到貨號['.$stocktake_goods_sn.']';
            // }
        }

        foreach ($update_data as $data) {
            if (array_key_exists($data['goods_sn'], $stocktake_info_by_sn)) {
                // update stork's history of during the stocktake
                if (!isset($goods_local_pick_up_qty_ar[$data['goods_sn']])) {
                    $local_pick_up_stock = 0;
                } else {
                    $local_pick_up_stock = $goods_local_pick_up_qty_ar[$stocktake_info_by_sn[$data['goods_sn']]['goods_id']];
                }
            
                $start_stock = $goods_start_qty_ar[$stocktake_info_by_sn[$data['goods_sn']]['goods_id'].'_'.$stocktake_info_by_sn[$data['goods_sn']]['warehouse_id']];
                $changed_stock = $goods_changed_qty_ar[$stocktake_info_by_sn[$data['goods_sn']]['goods_id'].'_'.$stocktake_info_by_sn[$data['goods_sn']]['warehouse_id']];
                $end_stock = $goods_end_qty_ar[$stocktake_info_by_sn[$data['goods_sn']]['goods_id'].'_'.$stocktake_info_by_sn[$data['goods_sn']]['warehouse_id']];
                
                $sql = 'update ' . $ecs->table('erp_goods_stocktake_item') .' set stocktake_start_time = "'.$stocktake_start_time.'", stocktake_end_time = "'.$stocktake_end_time.'", local_pick_up_stock = "'.$local_pick_up_stock.'", start_stock = "'.$start_stock.'" , changed_stock = "'.$changed_stock.'", end_stock = "'.$end_stock.'", stock = "'.$data['stock'].'" , update_time = "'.gmtime().'", status = "'.self::GOODS_STOCKTAKE_FINISHED.'" where goods_sn = "'.$data['goods_sn'].'" and stocktake_id = "'.$stocktake_id.'" and status in ( "'.self::GOODS_STOCKTAKE_PENDING.'" ,  "'.self::GOODS_REVIEW_RESTOCKTAKE.'" , "'.self::GOODS_STOCKTAKE_FINISHED.'"  ) ';
                $db->query($sql);
          
                $success_count ++;
            } else {
                $error[] = '在盤點清單找不到貨號['.$data['goods_sn'].']';
            }
        }

        // change status to "stocktake processing"
        $this->changeStocktakeStatus($stocktake_id, self::STOCKTAKE_PROGRESSING);

        // update stocktake finished
        $this->updateStocktakeFinishedCount($stocktake_id);

        $result = array(
            'success_message' => '成功匯入'.$success_count.'個記錄',
            'error_message' => implode('<br>', $error)
            );

        return $result;
    }
}
