<?php

namespace Yoho\cms\Controller;

use Yoho\cms\Model;

if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

class SalesagentController extends YohoBaseController {

	const DB_ACTION_CODE_SALES_AGENT = 'sales_agent_manage';
	const DB_ACTION_CODE_MENU_SALES_AGENT = 'menu_sales_agent_manage';

	public function __construct() {
		parent::__construct();
	}

	public function get_sales_agent_list() {
		global $db, $ecs;
		$filter['sort_by'] = empty($_REQUEST['sort_by']) ? 'sa.sa_id' : trim($_REQUEST['sort_by']);
		$filter['sort_order'] = empty($_REQUEST['sort_order']) ? 'ASC' : trim($_REQUEST['sort_order']);
		$sql = "SELECT count(*) FROM " . $ecs->table('sales_agent');
		$filter['record_count'] = $db->getOne($sql);
		/* Paging */
		$filter = page_and_size($filter);
		$sql = "SELECT sa.*, (SELECT COUNT(*) FROM " . $ecs->table('sales_agent_goods') . " AS sag WHERE sag.sa_id = sa.sa_id ) AS goods " .
				"FROM " . $ecs->table('sales_agent') . " AS sa " .
				"ORDER BY " . $filter['sort_by'] . ' ' . $filter['sort_order'] . ' ' .
				"LIMIT " . $filter['start'] . "," . $filter['page_size'];
		$data = $db->getAll($sql);
		foreach ($data as $key => $row) {
			$data[$key]['created_at_formatted'] = local_date($GLOBALS['_CFG']['date_format'], $row['created_at']);
		}
		$arr = array('data' => $data, 'filter' => $filter, 'page_count' => $filter['page_count'], 'record_count' => $filter['record_count']);
		return $arr;
	}

	public function get_all_sales_agents() {
		global $db, $ecs;
		$sql = "SELECT sa_id, name  FROM " . $ecs->table('sales_agent') . " ";
		$data = $db->getAll($sql);

		return $data;
	}

	public function get_active_sales_agents() {
		global $db, $ecs;
		$sql = "SELECT sa_id, name  FROM " . $ecs->table('sales_agent') .
						" WHERE active = 1";
		$data = $db->getAll($sql);

		return $data;
	}

	/* ------------------------------------------------------ */

	//-- Insert Sale agent
	/* ------------------------------------------------------ */
	public function insert_sale_agent($request) {
		$data = array(
				'name' => $request['sale_agent_name'],
				'active' => $request['active'],
				'pay_type' => $request['pay_type']
		);

		$is_default = '';
		$rates = array();
		foreach ($request['sa_rate'] as $key => $val) {
			if (!empty($val)){
				$rates[$key] = array(
					'sa_rate' => $val,
					'rate_active' => $request['rate_active'][$key],
					'is_default' => $request['is_default'][$key]
				);
			}
		}

		// Insert sale agent
		$new_salesagent = new Model\Salesagent(null, $data);

		// Add sale agent rate
		if ($new_salesagent != false)
		{
			foreach ($rates as $key => $val) {
				$rate_id = $new_salesagent->addRate($val);
				if ($val['is_default'] == true ){
					$is_default = $rate_id;
				}
				if ($rate_id == false)
					return false;
			}
		}

		if (!$request['sale_agent_id'] && sizeof($request['item-id']) > 0 && sizeof($request['item-id']) == sizeof($request['item-ag-price'])){
			$arr_item_id = $request['item-id'];
			$arr_ag_price = $request['item-ag-price'];

			$new_salesagent->addGoods($arr_item_id, $arr_ag_price);
		}

		//  Set rate default
		$new_salesagent->setDefaultRate($is_default);
	}

	/* ------------------------------------------------------ */

	//-- Update Sale agent
	/* ------------------------------------------------------ */
	public function update_sale_agent($request) {
		global $db, $ecs;
		$rates = array();

		$is_default = '';
		foreach ($request['sa_rate'] as $key => $val) {
			if ($val != ""){
				$rates[$key] = array(
						'rate_id' => $request['rate_id'][$key],
						'sa_rate' => $val,
						'rate_active' => $request['rate_active'][$key],
						'is_default'  => $request['is_default'][$key]
				);
				if ($request['is_default'][$key] == true){
					$is_default = $request['rate_id'][$key];
				}
			}
		}

		$salesagent = new Model\Salesagent($_REQUEST['sale_agent_id']);
		$data = $salesagent->data;

		// If name change, update it.
		if ($request['sale_agent_name'] != $data['name'])
		{
			$update_name = $salesagent->updateName($request['sale_agent_name']);
			//if (!$update_name)
				//return false;
		}

		if (isset($request['pay_type']))
		{
			$pay_type = $salesagent->setPayType($request['pay_type']);
		}
		//if (!$pay_type)
			//return false;

		//  If active change, update it.
		if ($request['active'])
		{
			$active = $salesagent->activate();
		} else
		{
			$active = $salesagent->deactivate();
		}
		//if (!$active)
			//return false;

		// Rate function

		foreach ($rates as $key => $val) {
			/*
			if ($val['rate_id'] != 0 && $val['sa_rate'] == "")
			{
				$salesagent->delRate($val['rate_id']);
				continue;
			}
			*/
			if ($val['rate_id'] != 0 && $val['rate_active'] == true)
			{
				// Have id : set to active
				$update_rate = $salesagent->activateRate($val['rate_id']);
			}
			if ($val['rate_id'] != 0 && $val['rate_active'] == false)
			{
				// Have id : set to deactive
				$update_rate = $salesagent->deactivateRate($val['rate_id']);
			}
			if ($val['rate_id'] == 0 && $val['sa_rate'] != '')
			{
				// No id: insert new rate
				$rate_id = $salesagent->addRate($val);
				if ($val['is_default'] == true && $is_default == 0 ){
					$is_default = $rate_id;
				}
			}
			if ($val['rate_id'] != 0 && $val['sa_rate'] != '')
			{
				$rate_id = $salesagent->updateRate($val['rate_id'], $val['sa_rate']);
			}
			if (!$update_rate)
				return false;
		}

		$arr_ag_current_goods = $db->getAll("SELECT goods_id FROM " . $ecs->table('sales_agent_goods') . " WHERE sa_id = '" . $_REQUEST['sale_agent_id'] . "' ");
		if ($request['sale_agent_id'] && sizeof($request['item-id']) == sizeof($request['item-ag-price'])){
			$arr_item_id = (empty($request['item-id'])?array():$request['item-id']);
			$arr_ag_price = (empty($request['item-ag-price'])?array():$request['item-ag-price']);

			if(empty($arr_ag_current_goods)){
				if (sizeof($request['item-id']) > 0){
					$salesagent->addGoods($arr_item_id, $arr_ag_price);
				}
			} else {
				$arr_ag_goods = array();
				foreach ($arr_ag_current_goods as $good){
					array_push($arr_ag_goods, $good['goods_id']);
				}

				if (empty($arr_item_id) && !empty($arr_ag_goods)){
					$arr_item_id_delete = $arr_ag_goods;
				} else {
					$arr_item_id_delete = array_diff($arr_ag_goods, $arr_item_id);
				}

				if (!empty($arr_item_id_delete)){
					$salesagent->deleteGoods($arr_item_id_delete);
				}

				$arr_item_id_add = array_diff($arr_item_id, $arr_ag_goods);
				$arr_item_price_add = [];
				foreach ($arr_item_id_add as $index => $item){
					$arr_item_price_add[$index] = $arr_ag_price[$index];
				}
				if (!empty($arr_item_id_add) && sizeof($arr_item_id_add) == sizeof($arr_item_price_add)){
					$salesagent->addGoods($arr_item_id_add, $arr_item_price_add);
				}

				$arr_item_id_update = array_diff($arr_item_id, $arr_item_id_delete, $arr_item_id_add);
				$arr_item_price_update = [];
				foreach ($arr_item_id_update as $index => $item){
					$arr_item_price_update[$index] = $arr_ag_price[$index];
				}

				if (!empty($arr_item_id_update) && sizeof($arr_item_id_update) == sizeof($arr_item_price_update)){
					$salesagent->updateGoods($arr_item_id_update, $arr_item_price_update);
				}
			}

		}

		//  Set rate default
		$salesagent->setDefaultRate($is_default);
	}

	function change_is_valid($sales_agent_id) {

		if (!admin_priv('erp_sys_manage', '', false))
		{
			$result['error'] = 1;
			$result['message'] = $_LANG['erp_no_permit'];
			return $result;
		}

		if (empty($sales_agent_id))
		{
			$result['error'] = 2;
			$result['message'] = $_LANG['erp_wrong_parameter'];
			return $result;
		}
		$salesagent = new Model\Salesagent($sales_agent_id);
		if (!$salesagent)
		{
			$result['error'] = 3;
			$result['message'] = $_LANG['erp_sales_agent_not_exists'];
			return $result;
		} else
		{
			$is_valid = $salesagent->data['active'];
			if ($is_valid == 1)
			{
				$salesagent->deactivate();
				$result['error'] = 0;
				$result['is_valid'] = 0;
				return $result;
			} elseif ($is_valid == 0)
			{
				$salesagent->activate();
				$result['error'] = 0;
				$result['is_valid'] = 1;
				return $result;
			}
		}
	}

	/** Asis Miles Excel import and data analytics.
	 *
	 */
	public function import_asiamiles_excel()
    {
        global $db,$ecs;

		$sa_model = new Model\Salesagent($_REQUEST['sa_select']);
		$salesagent = $sa_model->data;
		if(empty($salesagent)) return json_encode(['error' => '沒有此代理銷售資料', 'status' => 0]);
		$sa_rate = $sa_model->getAllRates();
		$default_sa_rate = 0;
		$rate_list = [];
		foreach($sa_rate as $v) {
			if(!$v['active']) continue;
			if($v['is_default'] == true) {
				$default_sa_rate = $v['sa_rate_id'];
			}
			$rate_list[$v['sa_rate_id']] = $v['sa_rate'];
		}
		// init data
        define( 'PCLZIP_TEMPORARY_DIR', ROOT_PATH . 'temp/');

		// Get excel data
        require_once ROOT_PATH . 'includes/excel/PHPExcel.php';
        require_once ROOT_PATH . 'includes/excel/PHPExcel/IOFactory.php';
        \PHPExcel_Settings::setZipClass(\PHPExcel_Settings::PCLZIP);
        ini_set('memory_limit', '512M');
        $fileName     = $_FILES["import_excel"]["tmp_name"];
        $fileType     = \PHPExcel_IOFactory::identify($fileName);
        $objReader    = \PHPExcel_IOFactory::createReader($fileType);
        $objPHPExcel  = $objReader->load($fileName);
		$objWorksheet = $objPHPExcel->getSheet(0);

		/** Get goods list:
		 * Desciption: Using to match Asis Miles product.
		 * Price: sales agent custom price
		 * Item Code: YOHO's goods_id
		 * Commission rate: sa_rate
		*/
		$goods_list   = $objPHPExcel->getSheetByName('Price');
		if($goods_list == NULL)return json_encode(['error' => 'Excel格式錯誤, 沒有產品清單', 'status' => 0]);
		$goods        = [];
		foreach ($goods_list->getRowIterator() as $g_key => $g_row) {
			$g_cellIterator = $g_row->getCellIterator();
			$g_cellIterator->setIterateOnlyExistingCells(false);
			foreach ($g_cellIterator as $g_c_key => $g_cell) {
				if($g_key == 1) {
					if(!empty($g_cell->getValue()))$goods_Key[$g_c_key] = trim($g_cell->getValue());
				} else {
					if(!empty($goods_Key[$g_c_key]))$g_e_row[$goods_Key[$g_c_key]]  = trim($g_cell->getValue());
				}
			}
			// 取得商品信息
			$yoho_goods = null;
			$sql = "SELECT g.goods_id, g.goods_name, g.goods_sn, g.give_integral, g.is_on_sale, g.is_real,g.integral, ".
				"g.market_price, g.shop_price AS org_price, g.promote_price, g.promote_start_date, ".
				"g.promote_end_date, g.goods_weight, g.integral, g.extension_code, ".
				"g.goods_number, g.is_alone_sale, g.is_shipping".
			" FROM " .$GLOBALS['ecs']->table('goods'). " AS g ".
			" WHERE g.goods_id = '".$g_e_row['Item Code']."'" .
			" AND g.is_delete = 0";

			$yoho_goods = $GLOBALS['db']->getRow($sql);
			if($yoho_goods) $g_e_row = array_merge($g_e_row, $yoho_goods);
			$g_e_row['custom_price'] = $g_e_row['Price'];

			// Get goods rate
			$goods_rate = $g_e_row['commission rate'];
			$rate_id = array_search($goods_rate, $rate_list);
			if(empty($rate_id)) $rate_id = $default_sa_rate;
			$g_e_row['rate_id'] = $rate_id;
			$goods[$g_e_row['Item Code']] = $g_e_row;

			unset($g_e_row);
		}

		$goods_list = array_filter($goods);
		unset($goods);

		/** Get order list: **Important field**
		 * Desciption: Using to match Yoho product.
		 * Claim No.: Using to add into sa_sn
		 * Quantity: Goods qty
		 * Name: Order consignee
		 * International Dailing Code: Tel region code
		 * Phone No.: tel, mobile
		 * ADDRESS 1, ADDRESS 2, ADDRESS 3 : address
		 * Postal Code: zip_code
		 * Country Code: Using to find country
		 * Email Address: email
		*/
		foreach ($objWorksheet->getRowIterator() as $key => $row) {
			$cellIterator = $row->getCellIterator();
			$cellIterator->setIterateOnlyExistingCells(false);
			// This loops all cells,
			// even if it is not set.
			// By default, only cells
			// that are set will be
			// iterated.
			foreach ($cellIterator as $c_key => $cell) {

				if($key == 1) {
					if(!empty($cell->getValue()))$excelKey[$c_key] = trim($cell->getValue());
				} else {
					if(!empty($excelKey[$c_key]))$e_row[$excelKey[$c_key]]  = trim($cell->getValue());
				}
			}
			if(array_filter($e_row))$excelData[] = $e_row;
		}
		$check_title = ['Desciption','Claim No.','Quantity','Name','International Dailing Code','Phone No.','ADDRESS 1', 'ADDRESS 2', 'ADDRESS 3','Postal Code','Country Code','Email Address'];
		foreach ($check_title as $key => $value) {
			if(!in_array($value, $excelKey)) return json_encode(['error' => 'Excel格式錯誤, 請確定有以下欄位: '.$value, 'status' => 0]);
		}
		$excelData = array_filter($excelData);
		$order_list = [];
		foreach ($excelData as $key => $value) {

			// Init order
			$order = [];
			$order['consignee'] = $value['Name'];
			$order['phone'] = '+'.$value['International Dailing Code'].'-'.$value['Phone No.'];
			$order['not_show']['phone'] = $order['phone'];
			// Merge Address1, 2, 3 into Address field
			$address = [];
			$address[] = trim($value['ADDRESS 1']);
			if(!empty($value['ADDRESS 2']))$address[] = trim($value['ADDRESS 2']);
			if(!empty($value['ADDRESS 3']))$address[] = trim($value['ADDRESS 3']);

			$order['country_code'] = $value['Country Code'];
			$order['zip_code']  = $value['Postal Code'];
			$order['address'] = implode(', ', $address);

			$order['email'] = $value['Email Address'];

			// Find user exist
			$userController = new UserController();
			$user_id = $userController->get_user_id($order['phone'], $order['email']);
			if(!$user_id) {
				$order['phone'] = '<span style="color:green;">'.$order['phone'].'</span>';
				$order['not_show']['user_id'] = false;
			} else {
				$order['phone'] = '<span style="color:blue;">'.$order['phone'].'</span>';
				$order['not_show']['user_id'] = $user_id;
			}

			// Add order goods and find goods info in our DB
			$goods_id = array_search( $value['Desciption'], array_column($goods_list, 'Desciption', 'Item Code'));
			$goods = $goods_list[$goods_id];
			if(empty($goods['goods_name'])) {
				$goods['goods_name'] = '<span style="color:red;">無法找到相關產品</span>';
				$goods['is_goods']   = false;
				$order['error'] = true;
			}
			$goods['qty']   = $value['Quantity'];
			$goods['goods_number']   = $value['Quantity'];
			$order['goods'][$goods_id] = $goods;

			$order['sa_number'] = $_REQUEST['sa_header'].$value['Claim No.'];

			$order_list[] = $order;
		}

		// Handle same sa sn
		$sa_number_list = array_column($order_list, 'sa_number');
		$cnt_array = array_count_values($sa_number_list);
		foreach ($order_list as $key => $order) {
			if(isset($cnt_array[$order['sa_number']]) && $cnt_array[$order['sa_number']] > 1) {
				$order['not_show']['sa_number'] = $order['sa_number'];
				$order['sa_number'] = '<span style="color:red;">'.$order['sa_number'].'</span>';
				$order['not_show']['duplicate_sa_number'] = true;
				$order['error'] = true;
				$order_list[$key] = $order;
			}
		}
        return json_encode(['format_data' => $order_list, 'status' => 1]);
	}

	/**
	 * Yahoo store list action
	 */
	public function yahooListAction()
	{
		require_once ROOT_PATH . 'includes/lib_order.php';
		require_once ROOT_PATH . 'includes/lib_howang.php';
		global $_LANG, $smarty, $ecs, $db;
		/* 检查权限 */
		admin_priv('yahoo_store');
		// title
		$assign['ur_here'] =$_LANG['yahoo_store'];
		$assign['full_page'] = 1;
		//function button
		$assign['action_link'] = array('text' => $_LANG['add_yahoo_item'], 'href' => 'yahoo_store.php?act=add');
		/* Get updated price YAHOO STORE product count */
		$sql = "SELECT count(h.yahoo_id) ".
			"FROM ". $ecs->table('yahoo_store') ."as h ".
			"LEFT JOIN " . $ecs->table('goods') . " AS g ON g.goods_id = h.goods_id ".
			"WHERE  h.yahoo_price <> g.shop_price ";
		$update_count = $db->getOne($sql);

		/* Get out of stock YAHOO STORE product count */
		$sql = "SELECT count(*) FROM ". $ecs->table('yahoo_store') ."as h ".
			" LEFT JOIN (SELECT  h.goods_id, ".hw_goods_number_subquery('g.', 'real_goods_number') . ", " .hw_reserved_number_subquery('g.') . " " .
			"FROM ". $ecs->table('yahoo_store') ."as h ".
			"LEFT JOIN " . $ecs->table('goods') . " AS g ON g.goods_id = h.goods_id ".
			"WHERE  1 ) as tmp ON tmp.goods_id = h.goods_id WHERE tmp.real_goods_number - tmp.reserved_number = 0 AND h.is_stock = 1";
		$no_stock_count = $db->getOne($sql);

		/* Get re-stock YAHOO STORE product count */
		$sql = "SELECT count(*) FROM ". $ecs->table('yahoo_store') ."as h ".
			" LEFT JOIN (SELECT  h.goods_id, ".hw_goods_number_subquery('g.', 'real_goods_number') . ", " .hw_reserved_number_subquery('g.') . " " .
			"FROM ". $ecs->table('yahoo_store') ."as h ".
			"LEFT JOIN " . $ecs->table('goods') . " AS g ON g.goods_id = h.goods_id ".
			"WHERE  1 ) as tmp ON tmp.goods_id = h.goods_id WHERE tmp.real_goods_number - tmp.reserved_number != 0 AND h.is_stock = 0";
		$re_stock_count = $db->getOne($sql);

		/* Get not sold YAHOO STORE product count */
		$sql = "SELECT count(*) ".
			"FROM ". $ecs->table('yahoo_store') ."as h ".
			"LEFT JOIN " . $ecs->table('goods') . " AS g ON g.goods_id = h.goods_id ".
			"WHERE g.is_on_sale = 0 AND h.is_sale = 1 ";

		$no_sale_count = $db->getOne($sql);

		/* Create action link */
		$assign['action_link_list'][] =  array(
			'text' => $_LANG['export_csv'].($update_count > 0 ? "<span class='badge bg-green update_count'>$update_count</span>" : ""),
			'href' => 'yahoo_store.php?act=export&type=update_price'
		);
		$assign['action_link_list'][] = array(
			'text' => '匯出無庫存產品'.($no_stock_count > 0 ? "<span class='badge bg-green update_count'>$no_stock_count</span>" : ""),
			'href' => 'yahoo_store.php?act=export&type=out_of_stock'
		);
		$assign['action_link_list'][] = array(
			'text' => '匯出重新有庫存產品'.($re_stock_count > 0 ? "<span class='badge bg-green update_count'>$re_stock_count</span>" : ""),
			'href' => 'yahoo_store.php?act=export&type=re_stock'
		);
		$assign['action_link_list'][] = array(
			'text' => '匯出已下架產品'.($no_sale_count > 0 ? "<span class='badge bg-green update_count'>$no_sale_count</span>" : ""),
			'href' => 'yahoo_store.php?act=export&type=not_on_sale'
		);
		parent::listAction($assign, 'yahoo_store_list.htm');
	}

	/*------------------------------------------------------ */
	//-- EXPORT Yahoo CSV/EXCEL
	/*------------------------------------------------------ */
	/**
	 * @param array $ids
	 * @param string $type
	 * @return bool
	 * @throws \PHPExcel_Reader_Exception
	 * @throws \PHPExcel_Writer_Exception
	 */
	public function export_csv_yahoo($ids = array(), $type = "normal")
	{
		global $db, $ecs;

		/* Init PHPExcel */
		ini_set('memory_limit', '256M');
		define('PCLZIP_TEMPORARY_DIR', ROOT_PATH . 'temp/');
		require_once ROOT_PATH . 'includes/excel/PHPExcel.php';
		require_once ROOT_PATH . 'includes/excel/PHPExcel/IOFactory.php';
		require_once ROOT_PATH . 'includes/lib_order.php';
		require_once ROOT_PATH . 'includes/lib_howang.php';
		\PHPExcel_Settings::setZipClass(\PHPExcel_Settings::PCLZIP);
		// Create new PHPExcel object
		$objPHPExcel = new \PHPExcel();
		/* Export excel/csv by type */
		switch ($type) {
			case 'update_price':
				/* Get items */
				$sql = "SELECT y.*, g.market_price as goods_original_price, g.goods_name, g.`shipping_price`,  g.`shop_price`, g.is_shipping " .
					"FROM " . $ecs->table('yahoo_store') . "as y " .
					"LEFT JOIN " . $ecs->table('goods') . " AS g ON g.goods_id = y.goods_id " .
					"WHERE  y.yahoo_price <> g.shop_price ";
				$data = $db->getAll($sql);
				$content = array();
				/* Excel title */
				// Product ID	Spec ID	Product Title	Sale Price	Original Price	Spec 1	Spec 2	Available Quantity	Sold Quantity	Lock Quantity	Start Time	End Time
				$title_list = ['Product ID', 'Spec ID', 'Title', 'Sale Price', 'Original Price', 'Spec 1', 'Spec 2', 'Available Quantity', 'Sold Quantity', 'Lock Quantity', 'Start Time', 'End Time'];
				$content[] = $title_list;
				/* File name */
				$filename = date("F-j-Y-g-i-s") . '-yahoo-update-price';

				if ($data) {
					foreach ($data as $key => $row) {
						/* Cal shipping price */
						$shipping_price = 0;
						if ($row['is_shipping'] == 1) $shipping_price = 0;
						elseif ($row['shipping_price'] > 0) $shipping_price = $row['shipping_price'];
//						elseif ($row['shop_price'] < 1000) $shipping_price = 33;
						$row['shop_price'] = $row['shop_price'] + $shipping_price;
						$row['goods_original_price'] = $row['goods_original_price'] + $shipping_price;
						$content[] = array(
							$row['yahoo_sku'],'', $row['goods_name'], $row['shop_price'], $row['goods_original_price'], '', '', $row['yahoo_quantity'], '', '', '' , ''
						);
					}
				}
				//Set CSV Header
				header("Content-Type: text/csv");
				header("Content-Disposition: attachment; filename=$filename.csv");

				// Disable caching
				header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
				header("Pragma: no-cache"); // HTTP 1.0
				header("Expires: 0"); // Proxies

				$fp = fopen("php://output", "w");

				//Output CSV
				foreach ($content as $fields) {
					fputcsv($fp, $fields);
				}
				fclose($fp);

				/* Update yahoo table when success to create excel */
				$update_sql = "UPDATE " . $this->ecs->table('yahoo_store') . "as y " .
					"LEFT JOIN " . $this->ecs->table('goods') . " AS g ON g.goods_id = y.goods_id " .
					"SET y.yahoo_price = g.shop_price " .
					"WHERE y.yahoo_price <> g.shop_price ";
				if ($this->db->query($update_sql)) {
					clear_cache_files();
				}
				break;
			case 'out_of_stock':
				/* Get items */
				$sql = "SELECT h.*, tmp.goods_name, tmp.goods_sn FROM ". $ecs->table('yahoo_store') ."AS h ".
					"LEFT JOIN (SELECT  g.goods_sn, g.goods_name, h.goods_id, ".hw_goods_number_subquery('g.', 'real_goods_number') . ", " .hw_reserved_number_subquery('g.') . " " .
					"FROM ". $ecs->table('yahoo_store') ."AS h ".
					"LEFT JOIN " . $ecs->table('goods') . " AS g ON g.goods_id = h.goods_id ".
					"WHERE  1 ) as tmp ON tmp.goods_id = h.goods_id WHERE tmp.real_goods_number - tmp.reserved_number = 0 AND h.is_stock = 1";
				$data = $db->getAll($sql);

				/* Excel title */
				$title_list = ['產品貨號', 'Yahoo SKU ID', '產品名稱'];
				foreach (range('A', 'K') as $char_key => $column) {
					$objPHPExcel->getActiveSheet()->SetCellValue($column . '1', $title_list[$char_key]);
				}
				/* File name */
				$filename = date("F-j-Y-g-i-s") . '-yahoo_store-out-of-stock.xlsx';
				$content = array();
				$need_update_ids = [];
				if ($data) {
					foreach ($data as $key => $row) {
						$need_update_ids[] = $row['goods_id'];
						/* Cal shipping price */
						$content[] = array(
							$row['goods_sn'], $row['yahoo_sku'], $row['goods_name']
						);
					}
				}
				/* Fill worksheet from values in array */
				try {
					$objPHPExcel->getActiveSheet()->fromArray($content, null, 'A2');
				} catch (\PHPExcel_Exception $e) {
				}
				/* Save Excel 2007 file */
				$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
				header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.‌​sheet; charset=utf-8');
				header('Content-Disposition: attachment; filename=' . $filename);
				header('Cache-Control: max-age=0');
				$objWriter->save('php://output');

				/* Update yahoo table when success to create excel */
				$update_sql = "UPDATE " . $this->ecs->table('yahoo_store') . "as h " .
					"SET h.is_stock = 0 " .
					"WHERE h.goods_id ".db_create_in($need_update_ids);
				if ($this->db->query($update_sql)) {
					clear_cache_files();
				}
				break;
			case 're_stock':
				/* Get items */
				$sql = "SELECT h.*, tmp.goods_name, tmp.goods_sn FROM ". $ecs->table('yahoo_store') ."AS h ".
					"LEFT JOIN (SELECT  g.goods_sn, g.goods_name, h.goods_id, ".hw_goods_number_subquery('g.', 'real_goods_number') . ", " .hw_reserved_number_subquery('g.') . " " .
					"FROM ". $ecs->table('yahoo_store') ."AS h ".
					"LEFT JOIN " . $ecs->table('goods') . " AS g ON g.goods_id = h.goods_id ".
					"WHERE  1 ) as tmp ON tmp.goods_id = h.goods_id WHERE tmp.real_goods_number - tmp.reserved_number != 0 AND h.is_stock = 0";
				$data = $db->getAll($sql);

				/* Excel title */
				$title_list = ['產品貨號', 'yahoo_store SKU ID', '產品名稱'];
				foreach (range('A', 'K') as $char_key => $column) {
					$objPHPExcel->getActiveSheet()->SetCellValue($column . '1', $title_list[$char_key]);
				}
				/* File name */
				$filename = date("F-j-Y-g-i-s") . '-yahoo_store-re-stock.xlsx';
				$content = array();
				$need_update_ids = [];
				if ($data) {
					foreach ($data as $key => $row) {
						$need_update_ids[] = $row['goods_id'];
						/* Cal shipping price */
						$content[] = array(
							$row['goods_sn'], $row['yahoo_sku'], $row['goods_name']
						);
					}
				}
				/* Fill worksheet from values in array */
				try {
					$objPHPExcel->getActiveSheet()->fromArray($content, null, 'A2');
				} catch (\PHPExcel_Exception $e) {
				}
				/* Save Excel 2007 file */
				$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
				header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.‌​sheet; charset=utf-8');
				header('Content-Disposition: attachment; filename=' . $filename);
				header('Cache-Control: max-age=0');
				$objWriter->save('php://output');

				/* Update yahoo table when success to create excel */
				$update_sql = "UPDATE " . $this->ecs->table('yahoo_store') . "as h " .
					"SET h.is_stock = 1 " .
					"WHERE h.goods_id ".db_create_in($need_update_ids);
				if ($this->db->query($update_sql)) {
					clear_cache_files();
				}
				break;
			case 'not_on_sale':
				/* Get items */
				$sql = "SELECT h.*, g.goods_sn, g.goods_name FROM ". $ecs->table('yahoo_store') ."AS h ".
					"LEFT JOIN " . $ecs->table('goods') . " AS g ON g.goods_id = h.goods_id ".
					"WHERE g.is_on_sale = 0 AND h.is_sale = 1";
				$data = $db->getAll($sql);

				/* Excel title */
				$title_list = ['產品貨號', 'Yahoo SKU ID', '產品名稱'];
				foreach (range('A', 'K') as $char_key => $column) {
					$objPHPExcel->getActiveSheet()->SetCellValue($column . '1', $title_list[$char_key]);
				}
				/* File name */
				$filename = date("F-j-Y-g-i-s") . '-yahoo_store-not-sale.xlsx';
				$content = array();
				$need_update_ids = [];
				if ($data) {
					foreach ($data as $key => $row) {
						$need_update_ids[] = $row['goods_id'];
						/* Cal shipping price */
						$content[] = array(
							$row['goods_sn'], $row['yahoo_sku'], $row['goods_name']
						);
					}
				}
				/* Fill worksheet from values in array */
				try {
					$objPHPExcel->getActiveSheet()->fromArray($content, null, 'A2');
				} catch (\PHPExcel_Exception $e) {
				}
				/* Save Excel 2007 file */
				$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
				header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.‌​sheet; charset=utf-8');
				header('Content-Disposition: attachment; filename=' . $filename);
				header('Cache-Control: max-age=0');
				$objWriter->save('php://output');

				/* Update yahoo table when success to create excel */
				$update_sql = "UPDATE " . $this->ecs->table('yahoo_store') . "as h " .
					"SET h.is_sale = 0 " .
					"WHERE h.goods_id ".db_create_in($need_update_ids);
				if ($this->db->query($update_sql)) {
					clear_cache_files();
				}
				break;
			default: // Normal
				break;
		}
		return true;
	}

	public function searchGoods($keyword){
		global $db, $ecs;
		$sql = "SELECT goods_id, goods_sn, goods_name, shop_price, goods_thumb " .
			"FROM " . $ecs->table('goods') . " " .
			"WHERE goods_name LIKE '%" . $keyword . "%' " . 
			"OR goods_sn LIKE '%" . $keyword . "%' " . 
			"OR goods_id LIKE '%" . $keyword . "%' " . 
			"ORDER BY goods_name ASC ";
		$data = $db->getAll($sql);
		if (empty($data)){
			return FALSE;
		} else {
			return $data;
		}
		
	}

	public function getSalesAgentIdName(){
		global $db, $ecs;
		$sql = "SELECT sa_id, name FROM " . $ecs->table('sales_agent') . " WHERE active = 1 ORDER BY `sa_id` ASC ";
		$data = $db->getAll($sql);
		if (empty($data)){
			return FALSE;
		} else {
			return $data;
		}
		
	}
}
