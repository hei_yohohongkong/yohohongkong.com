<?php

/***
* AdminTaskTemplatesController.php
* by Eric 20180410
*
* AdminTaskTemplates controller
***/

namespace Yoho\cms\Controller;
use Yoho\cms\Model;
if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

class AdminTaskTemplatesController extends YohoBaseController
{
    private $tablename = 'admin_task_templates';
    
    public function __construct()
    {
        parent::__construct();
    }
    public function getTemplateList()
    {
        return $this->db->getAll("SELECT template_id, title FROM " . $this->ecs->table('admin_task_templates') . " ORDER BY title ASC");
    }

    public function getAdminTaskTemplates()
    {
        $filter['sort_by']      = empty($_REQUEST['sort_by']) ? 'template_id' : trim($_REQUEST['sort_by']);
        $filter['sort_order']   = empty($_REQUEST['sort_order']) ? 'ASC' : trim($_REQUEST['sort_order']);
    
        $sql = "SELECT count(*) FROM " . $GLOBALS['ecs']->table('admin_task_templates');
        $filter['record_count'] = $GLOBALS['db']->getOne($sql);
    
        /* 分页大小 */
        $filter = page_and_size($filter);
        
        /* 查询 */
        $sql = "SELECT * FROM " . $GLOBALS['ecs']->table('admin_task_templates') . " as att " .
            "ORDER BY " . $filter['sort_by'] . " " . $filter['sort_order'] . " " .
            "LIMIT " . $filter['start'] . "," . $filter['page_size'];
        $data = $GLOBALS['db']->getAll($sql);
        
        $arr = array('data' => $data, 'filter' => $filter, 'page_count' => $filter['page_count'], 'record_count' => $filter['record_count']);
    
        return $arr;
    }

    public function getAdminTaskTemplate($template_id)
    {
        $q = "SELECT * FROM ". $this->ecs->table('admin_task_templates')." where template_id = '".$template_id."'";
        return $res = $this->db->fetchRow($this->db->query($q));
    }

    public function saveAdminTaskTemplate($request)
    {
        $data = array(
            'title' => stripslashes($request['title']),
            'message' => stripslashes($request['message']),
            'estimated_time' => stripslashes($request['estimated_time']),
            'priority' => stripslashes($request['priority']),
        );
        $template_id = $request['template_id'];
        if (empty($request['template_id'])) {
            // create new
            $q = "INSERT INTO ".$this->ecs->table('admin_task_templates')." (title,message,priority,estimated_time) VALUE ('".mysql_real_escape_string($data['title'])."','".mysql_real_escape_string($data['message'])."','".$data['priority']."','".$data['estimated_time']."')";
            if ($this->db->query($q)) {
                return true;
            } else {
                return false;
            }
        } else {
            // update
            $q = "UPDATE ".$this->ecs->table('admin_task_templates')." set title = '".mysql_real_escape_string($data['title'])."', message = '".mysql_real_escape_string($data['message'])."', priority = '".$data['priority']."' , estimated_time = '".$data['estimated_time']."' where template_id= ".$template_id." ";
            if ($this->db->query($q)) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function removeAdminTaskTemplate($template_id)
    {
        if (!empty($template_id)) {
            $sql = "DELETE FROM " . $this->ecs->table('admin_task_templates') .
                                " WHERE `template_id` = '" . $template_id . "' ";
            if ($this->db->query($sql)) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function buildForm($formMapper = [], $id = 0, $act = '', $assign = [], $redirect = '')
    {
        $ctrlTask = new TaskController();
        $priority_options = $ctrlTask->getPriorityMappings();
        $estimated_time_options = $ctrlTask->getEstimatedTimeMappings();
        if (empty($formMapper)) {
            $formMapper = [
            'priority' => ['type' => 'select', 'options'=> $priority_options],
            'estimated_time' => ['type' => 'select', 'options'=> $estimated_time_options],
            'title' => ['required' => true],
            'message' => ['type' => 'textarea']
        ];
        }
        parent::buildForm($formMapper, $id, $act, $assign, $redirect);
    }
    
    // public function updateAction()
    // {

    // }
}