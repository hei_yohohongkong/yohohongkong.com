<?php

/**
 * ECSHOP 管理中心菜单数组
 * ============================================================================
 * 版权所有 2005-2010 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liuhui $
 * $Id: inc_menu.php 17063 2010-03-25 06:35:46Z liuhui $
*/

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

$modules['02_cat_and_goods']['01_goods_list']       = 'goods.php?act=list';         // 商品列表
// $modules['02_cat_and_goods']['02_goods_add']        = 'goods.php?act=add';          // 添加商品
$modules['02_cat_and_goods']['03_category_list']    = 'category.php?act=list';
$modules['02_cat_and_goods']['06_goods_brand_list'] = 'brand.php?act=list';
//$modules['02_cat_and_goods']['08_goods_type']       = 'goods_type.php?act=manage';                           // Temporary disable unused feature
$modules['02_cat_and_goods']['08_attribute']       = 'attribute.php?act=list';
$modules['02_cat_and_goods']['11_goods_trash']      = 'goods.php?act=trash';        // 商品回收站
$modules['02_cat_and_goods']['12_batch_pic']        = 'picture_batch.php';
// $modules['02_cat_and_goods']['13_batch_add']        = 'goods_batch.php?act=add';    // 商品批量上传
// $modules['02_cat_and_goods']['14_goods_export']     = 'goods_export.php?act=goods_export';
// $modules['02_cat_and_goods']['15_batch_edit']       = 'goods_batch.php?act=select'; // 商品批量修改
//$modules['02_cat_and_goods']['16_goods_script']     = 'gen_goods_script.php?act=setup';                      // Temporary disable unused feature
$modules['02_cat_and_goods']['17_tag_manage']       = 'tag_manage.php?act=list';
//$modules['02_cat_and_goods']['50_virtual_card_list']   = 'goods.php?act=list&extension_code=virtual_card';   // Temporary disable unused feature
//$modules['02_cat_and_goods']['51_virtual_card_add']    = 'goods.php?act=add&extension_code=virtual_card';    // Temporary disable unused feature
//$modules['02_cat_and_goods']['52_virtual_card_change'] = 'virtual_card.php?act=change';                      // Temporary disable unused feature
//$modules['02_cat_and_goods']['goods_auto']             = 'goods_auto.php?act=list';                          // Temporary disable unused feature
$modules['02_cat_and_goods']['goods_cost_log']      = 'goods_cost_log.php?act=list';
$modules['02_cat_and_goods']['goods_price_log']     = 'goods_price_log.php?act=list';
$modules['02_cat_and_goods']['auto_pricing']     = 'auto_pricing.php?act=list';
$modules['02_cat_and_goods']['warranty_templates']  = 'warranty_templates.php?act=list';
$modules['02_cat_and_goods']['shipping_templates']  = 'shipping_templates.php?act=list';
$modules['02_cat_and_goods']['image_compression_product']   = 'image_compression_product.php?act=list';
$modules['02_cat_and_goods']['goods_shipping_dimension_import'] = 'goods_shipping_dimension_import.php?act=main';
$modules['02_cat_and_goods']['esl_operation'] = 'esl_operation.php?act=main';

$modules['01_richang']['goal_setting']       = 'goal_setting.php';
$modules['01_richang']['team_kpi']       = 'team_kpi.php';

$modules['01_richang']['02_book']                   = 'goods_book.php?act=list';
$modules['01_richang']['02_dai']                    = 'goods_dai.php?act=list';
// $modules['01_richang']['031_plans']                 = 'admin_plan.php?act=list';
$modules['01_richang']['03_taskes']                 = 'tasklist.php?act=list';
// $modules['01_richang']['04_redmine']                = '//redmine.yohohongkong.com/projects/yoho-beta/issues';
$modules['01_richang']['05_refund_requests']        = 'refund_requests.php?act=list';
$modules['01_richang']['06_pending_receive']        = 'pending_receive.php?act=list';
$modules['01_richang']['07_repair_orders']          = 'repair_orders.php?act=list';
$modules['01_richang']['07_replace_orders']         = 'order.php?act=replacement_list&replacement=1';
$modules['01_richang']['08_goods_feedback_problem'] = 'goods_feedback.php?act=list_problem';
$modules['01_richang']['09_goods_feedback_price']   = 'goods_feedback.php?act=list_price_suggestion';
$modules['01_richang']['10_abnormal_cost_log']      = 'abnormal_cost_log.php?act=list';
// $modules['01_richang']['11_suggested_promotions']   = 'suggested_promotions.php?act=list';
$modules['01_richang']['sales_agent_manage']        = 'sa_batch_order.php?act=info';
$modules['01_richang']['deal_log']                  = 'deal_log.php?act=list';
$modules['01_richang']['yahoo_store']               = 'yahoo_store.php?act=list';
$modules['01_richang']['quotation']                 = 'quotation.php?act=list';
$modules['01_richang'][Yoho\cms\Controller\HktvmallController::DB_ACTION_CODE_HKTVMALL] = 'hktvmall.php?act=list';
$modules['01_richang']['sale_cross_check']          = 'sale_cross_check.php?act=list';
$modules['01_richang']['order_picker_packer_import']  = 'order_picker_packer_import.php';
$modules['01_richang']['wholesale_user_info']       = 'wholesale_invoice.php?act=list';
$modules['01_richang']['rma']                       = 'erp_rma.php?act=list';
$modules['01_richang']['rsp_record']                = 'rsp_record.php?act=list';
$modules['01_richang']['staff_purchase_list']       = 'staff_purchase.php?act=list';
$modules['01_richang']['shop_restore']              = 'erp_shop_restore.php?act=list';
$modules['01_richang']['wechatpay_force_refund']       = 'erp_wechatpay_force_refund.php?act=search';

$modules['03_order']['02_order_list']               = 'order.php?act=list';
$modules['03_order']['order_ship']                  = 'order_ship.php?act=list';

$modules['03_order']['03_order_query']              = 'order.php?act=order_query';
//$modules['03_order']['04_merge_order']              = 'order.php?act=merge';                                 // Temporary disable unused feature
$modules['03_order']['05_edit_order_print']         = 'order.php?act=templates';
//$modules['03_order']['06_undispose_booking']        = 'goods_booking.php?act=list_all';                      // Temporary disable unused feature
//$modules['03_order']['07_repay_application']        = 'repay.php?act=list_all';
$modules['03_order']['08_add_order']                = 'order.php?act=add';
$modules['03_order']['09_delivery_order']           = 'order.php?act=delivery_list';
$modules['03_order']['10_back_order']               = 'order.php?act=back_list';
$modules['03_order']['order_goods_sn']              = 'order_goods_sn.php?act=list';
$modules['03_order']['order_goods_cross_check']     = 'order_goods_cross_check.php?act=list';
$modules['03_order']['stock_transfer_cross_check']     = 'stock_transfer_cross_check.php?act=list';
$modules['03_order']['order_express_collected_import'] = 'order_express_collected_import.php?act=list';
$modules['03_order']['order_wholesale_delivery']   = 'erp_order_wholesale_delivery.php?act=list';
$modules['03_order']['order_list_rank_program']   = 'order_list_rank_program.php?act=list';

//$modules['04_market']['02_snatch_list']          = 'snatch.php?act=list';                                 // Temporary disable unused feature
//$modules['04_market']['04_bonustype_list']       = 'bonus.php?act=list';                                  // Temporary disable unused feature
//$modules['04_market']['06_pack_list']            = 'pack.php?act=list';                                   // Temporary disable unused feature
//$modules['04_market']['07_card_list']            = 'card.php?act=list';                                   // Temporary disable unused feature
//$modules['04_market']['08_group_buy']            = 'group_buy.php?act=list';                              // Temporary disable unused feature
//$modules['04_market']['09_topic']                = 'topic.php?act=list';                                  //combine to 19_mkt_method
//$modules['04_market']['10_auction']              = 'auction.php?act=list';                                // Temporary disable unused feature
//$modules['04_market']['12_favourable']           = 'favourable.php?act=list';                             //combine to 19_mkt_method
//$modules['04_market']['13_wholesale']            = 'wholesale.php?act=list';                              // Temporary disable unused feature
//$modules['04_market']['14_package_list']         = 'package.php?act=list';                                //combine to 19_mkt_method
//$modules['04_market']['ebao_commend']            = 'ebao_commend.php?act=list';                           // Temporary disable unused feature
//$modules['04_market']['15_exchange_goods']       = 'exchange_goods.php?act=list';                         // Temporary disable unused feature
//$modules['04_market']['16_coupons']              = 'coupon.php?act=list';                                 //combine to 19_mkt_method
//$modules['04_market']['17_flashdeals']           = 'flashdeal.php?act=list';                              //combine to 19_mkt_method
//$modules['04_market']['flashdeal_events']           = 'flashdeal_event.php?act=list';                     //combine to 19_mkt_method
//$modules['04_market']['18_package_list']              = 'package.php?act=list';
$modules['04_market']['mkt_method']                 = 'mkt.php';
$modules['04_market']['ad_position']                = 'ad_position.php?act=list';
$modules['04_market']['ad_list']                    = 'ads.php?act=list';
// $modules['04_market']['ad_campaign']                = 'ad_campaign.php?act=list';
$modules['04_market']['ad_pospromote']                     = 'ad_pos.php?act=list';
$modules['04_market']['homepage_carousel']          = 'homepage_carousel.php?act=list';
$modules['04_market']['support_manage']           = 'articlecat.php?act=support_list';
$modules['04_market']['global_alert']           = 'global_alert.php?act=list';
$modules['04_market']['03_article_list']           = 'article.php?act=list';
$modules['04_market']['02_articlecat_list']        = 'articlecat.php?act=list';
$modules['04_market']['05_comment_manage']   = 'comment_manage.php?act=list';
//$modules['04_market']['lucky_draw']             = 'lucky_draw.php?act=list';
// $modules['04_market']['vote_list']                 = 'vote.php?act=list';
// $modules['04_market']['article_auto']              = 'article_auto.php?act=list';
//$modules['04_market']['shop_help']                 = 'shophelp.php?act=list_cat';
//$modules['04_market']['shop_info']                 = 'shopinfo.php?act=list';

$modules['06_stats']['employee_evaluation']         = 'employee_evaluation.php?act=list';
$modules['06_stats']['flow_stats']                  = 'flow_stats.php?act=view';
$modules['06_stats']['report_guest']                = 'guest_stats.php?act=list';
$modules['06_stats']['report_order']                = 'order_stats.php?act=list';
$modules['06_stats']['report_sell']                 = 'sale_general.php?act=list';
$modules['06_stats']['report_users']                = 'users_order.php?act=order_num';
$modules['06_stats']['report_kpi']                  = 'key_performance.php?act=list&market=0';
$modules['06_stats']['report_kpi_market']           = 'key_performance.php?act=list&market=1';
$modules['06_stats']['report_logistics']           = 'logistics_report.php?act=list';
$modules['06_stats']['report_ex_pos']               = 'exhibition_pos_report.php?act=list';
$modules['06_stats']['review_shop']                 = 'review_shop.php?act=list';
$modules['06_stats']['review_sales']                = 'review_sales.php?act=list';
$modules['06_stats']['review_cs']                   = 'review_cs.php?act=list';
$modules['06_stats']['sale_commission']             = 'sale_commission.php?act=list';
//$modules['06_stats']['sale_list']                   = 'sale_list.php?act=list';                              // Temporary disable unused feature
$modules['06_stats']['sale_payment']                = 'sale_payment.php?act=list';
$modules['06_stats']['sale_profit']                 = 'sale_profit.php?act=list';
//$modules['06_stats']['searchengine_stats']          = 'searchengine_stats.php?act=view';                     // Temporary disable unused feature
$modules['06_stats']['sell_stats']                  = 'sale_order.php?act=goods_num';
$modules['06_stats']['stock_cost']                  = 'stock_cost.php?act=list';
$modules['06_stats']['stock_cost_brand']            = 'sale_brand.php?act=list';
$modules['06_stats']['stock_cost_supplier']         = 'sale_supplier.php?act=list';
$modules['06_stats']['supplier_turnover']           = 'supplier_turnover.php?act=list';
$modules['06_stats']['supplier_stock_report']           = 'supplier_stock_report.php?act=list';
$modules['06_stats']['warehouse_turnover']          = 'warehouse_turnover.php?act=list';
$modules['06_stats']['stock_cost_log']              = 'stock_cost_log.php?act=list';
$modules['06_stats']['visit_buy_per']               = 'visit_sold.php?act=list';
// $modules['06_stats']['z_clicks_stats']              = 'adsense.php?act=list';
$modules['06_stats']['trends_keywords']             = 'trends.php?act=list';
$modules['06_stats']['report_order_address']        = 'order_address.php?act=list';
$modules['06_stats']['image_compression_usages']    = 'image_compression_usages.php?act=list';
$modules['06_stats']['write_off']                   = 'write_off.php';
$modules['06_stats']['rma_report']                  = 'rma_report.php?act=list';
$modules['06_stats']['order_goods_cross_check_report'] = 'order_goods_cross_check_report.php?act=list';
$modules['06_stats']['goods_stats']                 = 'goods_statistics.php?act=list';
$modules['06_stats']['order_progress_report']       = 'order_progress_report.php?act=list';
$modules['06_stats']['sql_report']                 = 'sql_reports.php?act=main';

$modules['08_members']['03_users_list']             = 'users.php?act=list';
$modules['08_members']['04_users_add']              = 'users.php?act=add';
$modules['08_members']['05_user_rank_list']         = 'user_rank.php?act=list';
$modules['08_members']['06_user_rank_program']     = 'user_rank_program.php?act=list';
//$modules['08_members']['06_list_integrate']         = 'integrate.php?act=list';                              // Temporary disable unused feature
//$modules['08_members']['08_unreply_msg']            = 'user_msg.php?act=list_all';                           // Temporary disable unused feature
//$modules['08_members']['09_user_account']           = 'user_account.php?act=list';                           // Temporary disable unused feature
$modules['08_members']['10_user_account_manage']    = 'user_account_manage.php?act=list';
$modules['08_members']['11_user_export']            = 'users.php?act=emailcsv';
$modules['08_members']['12_upcoming_birthdays']     = 'upcoming_birthdays.php?act=list';
$modules['08_members']['13_wishlist']               = 'wishlist.php?act=list';
$modules['08_members']['account_log']               = 'account_log.php?act=log_list';

$modules['09_priv_admin']['admin_logs']             = 'admin_logs.php?act=list';
$modules['09_priv_admin']['admin_list']             = 'privilege.php?act=list';
$modules['09_priv_admin']['admin_role']             = 'role.php?act=list';
//$modules['09_priv_admin']['agency_list']            = 'agency.php?act=list';                                 // Temporary disable unused feature
//$modules['09_priv_admin']['suppliers_list']         = 'suppliers.php?act=list'; // 供货商

$modules['11_system']['01_shop_config']             = 'shop_config.php?act=list_edit';
$modules['11_system']['021_reg_fields']             = 'reg_fields.php?act=list';
$modules['11_system']['022_insurance_service']      = 'insurance.php?act=list';
$modules['11_system']['02_payment_list']            = 'payment.php?act=list';
$modules['11_system']['03_shipping_list']           = 'shipping.php?act=list';
$modules['11_system']['04_mail_settings']           = 'shop_config.php?act=mail_settings';
$modules['11_system']['05_area_list']               = 'area_manage.php?act=list';
//$modules['11_system']['06_plugins']                 = 'plugins.php?act=list';
$modules['11_system']['07_cron_schcron']            = 'cron.php?act=list';
$modules['11_system']['09_logistics_list']          = 'logistics.php?act=list';
//$modules['11_system']['08_friendlink_list']         = 'friend_link.php?act=list';                            // Temporary disable unused feature
$modules['11_system']['captcha_manage']             = 'captcha_manage.php?act=main';
$modules['11_system']['check_file_priv']            = 'check_file_priv.php?act=check';
//$modules['11_system']['fckfile_manage']             = 'fckfile_manage.php?act=list';
//$modules['11_system']['file_check']                 = 'filecheck.php';                                       // Temporary disable unused feature
//$modules['11_system']['flashplay']                  = 'flashplay.php?act=list';                              // Temporary disable unused feature
$modules['11_system']['navigator']                  = 'navigator.php?act=list';
//$modules['11_system']['shop_authorized']            = 'license.php?act=list_edit';                           // Temporary disable unused feature
//$modules['11_system']['shp_webcollect']             = 'webcollect.php';                                      // Temporary disable unused feature
// $modules['11_system']['sitemap']                    = 'sitemap.php';
$modules['11_system']['ucenter_setup']              = 'integrate.php?act=setup&code=ucenter';
$modules['11_system']['holidays']                   = 'holidays.php?act=list';

//$modules['11_system']['02_template_select']       = 'template.php?act=list';                               // Temporary disable unused feature
$modules['11_system']['03_template_setup']        = 'template.php?act=setup';
$modules['11_system']['04_template_library']      = 'template.php?act=library';
$modules['11_system']['05_edit_languages']        = 'edit_languages.php?act=list';
//$modules['11_system']['06_template_backup']       = 'template.php?act=backup_setting';                     // Temporary disable unused feature
$modules['11_system']['mail_template_manage']     = 'mail_template.php?act=list';

//$modules['13_backup']['02_db_manage']               = 'database.php?act=backup';                             // Temporary disable unused feature
//$modules['13_backup']['03_db_optimize']             = 'database.php?act=optimize';                           // Temporary disable unused feature
//$modules['13_backup']['04_sql_query']               = 'sql.php?act=main';                                    // Temporary disable unused feature
//$modules['13_backup']['05_synchronous']             = 'integrate.php?act=sync';
//$modules['13_backup']['convert']                    = 'convert.php?act=main';                                // Temporary disable unused feature

//$modules['14_sms']['02_sms_my_info']                = 'sms.php?act=display_my_info';
//$modules['14_sms']['03_sms_send']                   = 'sms.php?act=display_send_ui';
//$modules['14_sms']['04_sms_charge']                 = 'sms.php?act=display_charge_ui';
//$modules['14_sms']['05_sms_send_history']           = 'sms.php?act=display_send_history_ui';
//$modules['14_sms']['06_sms_charge_history']         = 'sms.php?act=display_charge_history_ui';

$modules['04_market']['affiliate']                     = 'affiliate.php?act=list';                              // Temporary disable unused feature
$modules['04_market']['affiliate_ck']                  = 'affiliate_ck.php?act=list';                           // Temporary disable unused feature
$modules['04_market']['affiliateyoho']                 = 'affiliateyoho.php?act=list';
$modules['04_market']['affiliateyoho_orders']          = 'affiliateyoho.php?act=list_orders';
$modules['04_market']['affiliateyoho_rewards']         = 'affiliateyoho.php?act=list_rewards';

//$modules['11_system']['attention_list']       = 'attention_list.php?act=list';                         // Temporary disable unused feature
//$modules['11_system']['email_list']           = 'email_list.php?act=list';                             // Temporary disable unused feature
//$modules['11_system']['magazine_list']        = 'magazine_list.php?act=list';                          // Temporary disable unused feature
$modules['11_system']['mail_sent_log']        = 'mail_sent_log.php?act=list';
//$modules['11_system']['view_sendlist']        = 'view_sendlist.php?act=list';                          // Temporary disable unused feature
$modules['11_system']['edm_generator']        = 'edmgen.php?act=main';

/******************************以下为进销售存部分****************************************/

//采购订单管理
$modules['05_erp_order_manage']['erp_order_list']                            = 'erp_order_manage.php?act=order_list';
$modules['05_erp_order_manage']['erp_order_list_pending_receive']            = 'erp_order_manage.php?act=order_list&pending_receive=1';
$modules['05_erp_order_manage']['erp_order_list_pending_payment']            = 'erp_order_manage.php?act=order_payment_list';

//库存管理
$modules['06_erp_stock_manage']['erp_goods_warehousing_list']                = 'erp_warehousing_manage.php?act=list';//入库单列表
$modules['06_erp_stock_manage']['erp_goods_delivery_list']                   = 'erp_delivery_manage.php?act=list';//出库单列表
$modules['06_erp_stock_manage']['erp_stock_inquiry']                         = 'erp_stock_inquiry.php?act=list';
$modules['06_erp_stock_manage']['erp_goods_warehousing_and_delivery_record'] = 'erp_stock_record.php?act=list';//出入库序时簿,出入库记录查询
$modules['06_erp_stock_manage']['erp_stock_abnormal'] = 'erp_stock_abnormal.php?act=list';
$modules['06_erp_stock_manage']['erp_stock_check']                           = 'erp_stocktake.php?act=list';//库存盘点
$modules['06_erp_stock_manage']['erp_stock_transfer']                        = 'erp_warehouse_transfer.php?act=list';//转拨单
$modules['06_erp_stock_manage']['erp_stock_daily']                           = 'erp_stock_daily.php?act=list';

//进销存系统设置
$modules['11_system']['erp_supplier_list']                        = 'erp_supplier.php?act=list';
$modules['11_system']['erp_supplier_group']                       = 'erp_supplier_group.php?act=list';
$modules['11_system']['erp_warehouse_list']                       = 'erp_warehouse.php?act=list';
$modules['11_system']['erp_warehousing_style_setting']            = 'erp_warehousing_style.php?act=list';
$modules['11_system']['erp_delivery_style_setting']               = 'erp_delivery_style.php?act=list';
$modules['11_system']['erp_sales_agent_list']               = 'erp_sales_agent.php?act=list';
// $modules['11_system']['erp_account_setting']                      = 'erp_finance_manage.php?act=bank_account_setting';
//$modules['11_system']['erp_goods_barcode_setting']                = 'erp_goods_barcode.php?act=list';

// //财务管理
// $modules['20_erp_finance_manage']['erp_account_payable_list']                = 'erp_finance_manage.php?act=account_payable';
// $modules['20_erp_finance_manage']['erp_account_receivable_list']             = 'erp_finance_manage.php?act=receivable_list';
// $modules['20_erp_finance_manage']['erp_payment_list']                        = 'erp_finance_manage.php?act=payment_list';
// $modules['20_erp_finance_manage']['erp_gathering_list']                      = 'erp_finance_manage.php?act=gathering_list';
// $modules['20_erp_finance_manage']['erp_account_list']                        = 'erp_finance_manage.php?act=bank_account_list';

//------------------------------ Accounting System ------------------------------
$modules['10_accounting_system']['actg_account_summary']      = 'accounting_report.php?act=summary';
$modules['10_accounting_system']['actg_transactions']         = 'accounting_report.php?act=list';
$modules['10_accounting_system']['actg_payment_wholesale']    = 'wholesale_payments.php?act=list';
$modules['10_accounting_system']['actg_payment_purchase']     = 'purchase_payments.php?act=list';
$modules['10_accounting_system']['actg_wholesale_orders']     = 'order.php?act=wholesale_list&is_wholesale=1';//user_ids=4940,5022,5023';
$modules['10_accounting_system']['actg_xaccount_receivable']  = 'order.php?act=wholesale_list&wholesale_receivable=1&start_time=2015-01-01';
$modules['10_accounting_system']['actg_xaccount_payable']     = 'erp_order_manage.php?act=order_list&payable=1&s_date=2015-01-01';
$modules['10_accounting_system']['balance_sheet']             = 'balance_sheet.php?act=list';
$modules['10_accounting_system']['credit_note_summary']       = 'credit_note_report.php?act=summary';
$modules['10_accounting_system']['income_statement']          = 'income_statement.php?act=list';
$modules['10_accounting_system']['cash_flow_statement']       = 'income_statement.php?act=cash_list';
$modules['10_accounting_system']['bank_reconciliation']       = 'bank_reconciliation.php?act=list';
$modules['10_accounting_system']['bank_reconciliation_match'] = 'bank_reconciliation.php?act=match';
$modules['10_accounting_system']['accounting_system']         = 'accounting_system.php?act=list';
$modules['10_accounting_system']['trial_balance']             = 'trial_balance.php?act=list';
//------------------------------ Shop Management ------------------------------
$modules['12_shop']['product_commission']      = 'commission.php?act=report&tab=1';
$modules['12_shop']['shop_commission']         = 'commission.php?act=shop_commission';
$modules['12_shop']['shop_salary']    = 'commission.php?act=shop_salary';
$modules['12_shop']['sale_cash_check']    = 'sale_cash_check.php?act=list';
//------------------------------ CRM System ------------------------------
$modules['section_crm_system']['crm_system']     = 'crm.php?act=list';
$modules['section_crm_system']['ivr_system']     = 'ivr.php?act=list';
$modules['section_crm_system']['op_system']      = 'offline_payment.php?act=list';

//------------------------------ Danger Zone ------------------------------
$modules['22_danger_zone']['danger_zone']      = 'danger_zone.php?act=main';

//------------------------------ SECTION SORTING ------------------------------
$ssort = [
    '01_richang',
    'section_crm_system',
    '02_cat_and_goods',
    '03_order',
    '12_shop',
    '04_market',
    '05_erp_order_manage',
    '06_erp_stock_manage',
    '06_stats',
    '08_members',
    '09_priv_admin',
    '10_accounting_system',
    '11_system',
    '22_danger_zone'
];

//------------------------------ MENU SORTING ------------------------------
$sort['10_accounting_system'] = [
    'actg_account_summary',
    'actg_transactions',
    'bank_reconciliation',
    'bank_reconciliation_match',
    'credit_note_summary',
    'actg_wholesale_orders',
    'actg_xaccount_payable',
    'actg_xaccount_receivable',
    'actg_payment_purchase',
    'actg_payment_wholesale',
    'income_statement',
    'cash_flow_statement',
    'balance_sheet'
];

$sort['03_order'] = [
    '02_order_list',
    'order_ship',
];

?>
