<?php

/***
* lib_accounting.php
* by howang 2014-10-31
*
* YOHO Hong Kong Accounting System
***/

if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

class Accounting
{
	private $db;

	public function __construct($db)
	{
		bcscale(6); // Calculate with 6 d.p. (storage is 4 d.p.)
		$this->db = $db;

		// register_shutdown_function(function ($db) {
		// 	error_log(print_r($db->queryLog, true), 1, 'howang@yohohongkong.com');
		// }, $this->db);
	}

	private function build_where($param, $prefix = '', $mode = 'AND')
	{
		if (is_string($param) && !empty($param)) return $param . ' ';
		if (!is_array($param)) return '';
		$where = '1 ';
		foreach ($param as $key => $value)
		{
			$pf = '';
			if (is_string($prefix))
			{
				$pf = $prefix;
			}
			elseif (is_array($prefix))
			{
				if (array_key_exists($key, $prefix))
				{
					$pf = $prefix[$key];
				}
				elseif (array_key_exists('_', $prefix))
				{
					$pf = $prefix['_'];
				}
			}
			$where .= $mode . " " . $pf . "`" . $key . "` = '" . $value . "' ";
		}
		return $where;
	}

	private function param_contains_keys($param, $required_keys)
	{
		// return (count(array_intersect_key(array_flip($required_keys), $param)) === count($required_keys));
		// return (count(array_diff($required_keys, array_keys($param))) === 0);
		foreach($required_keys as $key)
		{
			if (!array_key_exists($key, $param))
			{
				return false;
			}
		}
		return true;
	}

	private function param_filter_keys($param, $allowed_keys)
	{
		foreach(array_keys($param) as $key)
		{
			if (!in_array($key, $allowed_keys))
			{
				unset($param[$key]);
			}
		}
		return $param;
	}

	public function start_transaction()
	{
		return $this->db->query('START TRANSACTION');
	}

	public function commit_transaction()
	{
		return $this->db->query('COMMIT');
	}

	public function rollback_transaction()
	{
		return $this->db->query('ROLLBACK');
	}

	public function money_format($amount, $format = '')
	{
		if (empty($format)) $format = '%s';
		$negative = (bccomp($amount, 0) < 0);
		if ($negative)
		{
			$amount = bcmul($amount, -1);
		}
		return ($negative ? '-' : '') . sprintf($format, number_format($amount, 2, '.', ','));
	}

	public function dbdate($time = null)
	{
		if (is_null($time))
		{
			$time = gmtime();
		}
		return date('Y-m-d H:i:s', $time);
	}

	/**
	 * Currencies
	 **/

	public function getCurrencies($param = array(), $opt = array())
	{
		return $this->db->getAll("SELECT * " .
			"FROM `actg_currencies` " .
			"WHERE " . $this->build_where($param) .
			"ORDER BY is_default DESC " . (isset($opt['orderby']) ? " , " . $opt['orderby'] . " " : '') .
			(isset($opt['limit']) ? "LIMIT " . $opt['limit'] : ''));
	}

	public function getCurrency($param)
	{
		if (empty($param)) return false;
		return $this->db->getRow("SELECT * " .
			"FROM `actg_currencies` " .
			"WHERE " . $this->build_where($param) . " LIMIT 1");
	}

	public function addCurrency($param)
	{
		if (empty($param)) return false;
		if (!$this->param_contains_keys($param, array(
			'currency_code', 'currency_name', 'currency_rate', 'currency_format'
		))) return false;
		return $this->db->autoExecute('actg_currencies', $param, 'INSERT') ? true : false;
	}

	public function updateCurrency($param)
	{
		if (empty($param['currency_code'])) return false;
		$currency_code = $param['currency_code'];
		unset($param['currency_code']);
		if (empty($param)) return false;
		return $this->db->autoExecute('actg_currencies', $param, 'UPDATE', "currency_code = '" . $currency_code . "'");
	}

	public function deleteCurrency($currency_code)
	{
		if (empty($currency_code)) return false;
		return $this->db->query("DELETE FROM `actg_currencies` WHERE currency_code = '" . $currency_code . "' LIMIT 1");
	}

	/**
	 * Payment Types
	 **/

	public function getPaymentTypes($param = array(), $opt = array())
	{
		return $this->db->getAll("SELECT * " .
			"FROM `actg_payment_types` " .
			"WHERE " . $this->build_where($param) .
			(isset($opt['where']) ? $opt['where'] . " " : '') .
			(isset($opt['orderby']) ? "ORDER BY " . $opt['orderby'] . " " : '') .
			(isset($opt['limit']) ? "LIMIT " . $opt['limit'] : ''));
	}

	public function getPaymentType($param)
	{
		if (empty($param)) return false;
		return $this->db->getRow("SELECT * " .
			"FROM `actg_payment_types` " .
			"WHERE " . $this->build_where($param) . " LIMIT 1");
	}

	public function addPaymentType($param)
	{
		if (empty($param)) return false;
		if (!$this->param_contains_keys($param, array(
			'payment_type_name'
		))) return false;
		return $this->db->autoExecute('actg_payment_types', $param, 'INSERT') ? $this->db->insert_id() : false;
	}

	public function updatePaymentType($param)
	{
		if (empty($param['payment_type_id'])) return false;
		$payment_type_id = $param['payment_type_id'];
		unset($param['payment_type_id']);
		if (empty($param)) return false;
		return $this->db->autoExecute('actg_payment_types', $param, 'UPDATE', "payment_type_id = '" . $payment_type_id . "'");
	}

	public function deletePaymentType($payment_type_id)
	{
		if (empty($payment_type_id)) return false;
		return $this->db->query("DELETE FROM `actg_payment_types` WHERE payment_type_id = '" . $payment_type_id . "' LIMIT 1");
	}

	/**
	 * CSV Bank codes
	 */
	public function getBankCodes($param)
	{
		$param = empty($param) ? array() : $param;
		return $this->db->getAll(
			"SELECT * FROM `actg_bank_codes` bc " .
			"WHERE " . $this->build_where($param)
		);
	}

	/**
	 * Accounts with CSV Bank code
	 */
	public function getAccountBankCodes($param)
	{
		$param = empty($param) ? array() : $param;
		return $this->db->getAll(
			"SELECT * FROM `actg_bank_codes` bc " .
			"LEFT JOIN `actg_accounts` a ON a.bank_id = bc.bank_id " .
			"WHERE a.bank_id != 0"
		);
	}

	/**
	 * Accounts
	 **/

	public function getAccounts($param = array(), $opt = array())
	{
		$data =  $this->db->getAll(
			"SELECT a.*, c.`currency_name`, c.`currency_rate`, c.`currency_format`, bc.`bank_code` " .
			"FROM `actg_accounts` as a " .
			"LEFT JOIN `actg_currencies` as c ON a.currency_code = c.currency_code " .
			"LEFT JOIN `actg_bank_codes` as bc ON a.bank_id = bc.bank_id " .
			"WHERE " . $this->build_where($param, 'a.') .
			(isset($opt['orderby']) ? "ORDER BY " . $opt['orderby'] . " " : '') .
			(isset($opt['limit']) ? "LIMIT " . $opt['limit'] : ''));

		// get book record from accounting entries.
		foreach ($data as &$account_item) {
			$sql = "select sum(amount) as amount from actg_accounting_transactions where actg_type_id = '".$account_item['actg_type_id']."' ";
			$acc_tran_total = $this->db->getOne($sql);
			$account_item['account_balance'] = $acc_tran_total;
		}

		return $data;
	}

	public function getAccount($param)
	{
		if (empty($param)) return false;
		
		$account_item = $this->db->getRow(
			"SELECT a.*, c.`currency_name`, c.`currency_rate`, c.`currency_format` " .
			"FROM `actg_accounts` as a " .
			"LEFT JOIN `actg_currencies` as c ON a.currency_code = c.currency_code " .
			"WHERE " . $this->build_where($param, 'a.') . " LIMIT 1"); 
		
		$sql = "select sum(amount) as amount from actg_accounting_transactions where actg_type_id = '".$account_item['actg_type_id']."' ";
		$acc_tran_total = $this->db->getOne($sql);
		$account_item['account_balance'] = $acc_tran_total;
		return $account_item;
	}

	public function addAccount($param)
	{
		if (empty($param)) return false;
		if (!$this->param_contains_keys($param, array(
			'account_name', 'account_number', 'currency_code', 'actg_type_id'
		))) return false;
		$param = $this->param_filter_keys($param, array(
			'account_name', 'account_number', 'currency_code', 'actg_type_id'
		));
		return $this->db->autoExecute('actg_accounts', $param, 'INSERT') ? $this->db->insert_id() : false;
	}

	public function updateAccount($param)
	{
		if (empty($param['account_id'])) return false;
		$account_id = $param['account_id'];
		unset($param['account_id']);
		if (empty($param)) return false;
		return $this->db->autoExecute('actg_accounts', $param, 'UPDATE', "account_id = '" . $account_id . "'");
	}

	public function deleteAccount($account_id)
	{
		if (empty($account_id)) return false;
		return $this->db->query("DELETE FROM `actg_accounts` WHERE account_id = '" . $account_id . "' LIMIT 1");
	}

	/**
	 * Transactions
	 **/

	public function getTransactions($param = array(), $opt = array())
	{
		return $this->db->getAll(
			"SELECT t.*, c.`currency_name`, c.`currency_rate`, c.`currency_format`, p.`payment_type_name`, " .
				"a.`account_name`, a.`account_number`, tt.`txn_type_name`, au.user_name " .
			"FROM `actg_account_transactions` as t " .
			"LEFT JOIN `actg_currencies` as c ON t.currency_code = c.currency_code " .
			"LEFT JOIN `actg_payment_types` as p ON t.payment_type_id = p.payment_type_id " .
			"LEFT JOIN `actg_accounts` as a ON t.account_id = a.account_id " .
			"LEFT JOIN `actg_transaction_types` as tt ON t.txn_type_id = tt.txn_type_id " .
			"LEFT JOIN (SELECT user_id, user_name FROM " . $GLOBALS['ecs']->table('admin_user') . ") as au ON au.user_id = t.admin_id " .
			"WHERE " . $this->build_where($param, 't.') .
			(isset($opt['orderby']) ? "ORDER BY " . $opt['orderby'] . " " : '') .
			(isset($opt['limit']) ? "LIMIT " . $opt['limit'] : ''));
	}

	public function getTransaction($param)
	{
		if (empty($param)) return false;
		return $this->db->getRow(
			"SELECT t.*, c.`currency_name`, c.`currency_rate`, c.`currency_format`, p.`payment_type_name`, " .
				"a.`account_name`, a.`account_number`, tt.`txn_type_name` " .
			"FROM `actg_account_transactions` as t " .
			"LEFT JOIN `actg_currencies` as c ON t.currency_code = c.currency_code " .
			"LEFT JOIN `actg_payment_types` as p ON t.payment_type_id = p.payment_type_id " .
			"LEFT JOIN `actg_accounts` as a ON t.account_id = a.account_id " .
			"LEFT JOIN `actg_transaction_types` as tt ON t.txn_type_id = tt.txn_type_id " .
			"WHERE " . $this->build_where($param, 't.') . " LIMIT 1");
	}

	public function addTransaction($param)
	{
		if (empty($param)) return false;

		if (!$this->param_contains_keys($param, array(
			'add_date', 'currency_code', 'amount', 'payment_type_id', 'account_id'
		))) return false;

		$param = $this->param_filter_keys($param, array(
			'add_date', 'complete_date', 'bank_date', 'currency_code', 'amount', 'payment_type_id', 'account_id', 'related_txn_id', 'remark', 'txn_type_id', 'expense_year', 'expense_month', 'txn_group_id', 'admin_id'
		));

		$transaction_started = false;
		if (!$this->db->in_transaction)
		{
			$this->start_transaction();
			$transaction_started = true;
		}

		// Completed transaction, process account balance
		if (!empty($param['complete_date']))
		{
			$sql = "SELECT IFNULL(max(`complete_seq`),0)+1 FROM `actg_account_transactions` WHERE `complete_seq` IS NOT NULL FOR UPDATE";
			$param['complete_seq'] = $this->db->getOne($sql);

			$sql = "SELECT `account_balance` FROM `actg_accounts` WHERE `account_id` = '" . $param['account_id'] . "' LIMIT 1 FOR UPDATE";
			$param['opening_balance'] = $this->db->getOne($sql);
			$param['closing_balance'] = bcadd($param['opening_balance'], $param['amount']);

			// Assign bank_date = complete_date if not provided
			if (empty($param['bank_date']))
			{
				$param['bank_date'] = $param['complete_date'];
			}
		}

		// Remove complete_date from param if it's set but empty (null, 0, empty string etc)
		if (array_key_exists('complete_date', $param) && empty($param['complete_date']))
		{
			unset($param['complete_date']);
		}
		// Remove bank_date from param if it's set but empty (null, 0, empty string etc)
		if (array_key_exists('bank_date', $param) && empty($param['bank_date']))
		{
			unset($param['bank_date']);
		}

		$param['expense_year'] =$param['expense_year'] = empty($param['expense_year']) ? local_date('Y') : $param['expense_year'];
		$param['expense_month'] = empty($param['expense_month']) ? local_date('m') : $param['expense_month'];

		if (!$this->db->autoExecute('actg_account_transactions', $param, 'INSERT'))
		{
			if ($transaction_started)
			{
				$this->rollback_transaction();
			}
			return false;
		}
		$txn_id = $this->db->insert_id();

		// Completed transaction, process account balance
		if (!empty($param['complete_date']))
		{
			$sql = "UPDATE `actg_accounts` " .
				"SET `account_balance` = '" . $param['closing_balance'] . "' " .
				"WHERE `account_id` = '" . $param['account_id'] . "' LIMIT 1";
			if (!$this->db->query($sql))
			{
				if ($transaction_started)
				{
					$this->rollback_transaction();
				}
				return false;
			}
		}

		if ($transaction_started)
		{
			$this->commit_transaction();
		}
		return $txn_id;
	}

	public function updateTransaction($param)
	{
		if (empty($param['txn_id'])) return false;
		$txn_id = $param['txn_id'];
		unset($param['txn_id']);

		// Only allow update bank_date, txn_type_id, expense_year, expense_month, remark, complete_date
		$param = $this->param_filter_keys($param, array(
			'bank_date', 'txn_type_id', 'expense_year', 'expense_month', 'remark', 'complete_date'
		));

		if (empty($param)) return false;
		return $this->db->autoExecute('actg_account_transactions', $param, 'UPDATE', "txn_id = '" . $txn_id . "'");
	}

	public function completeTransaction($txn_id)
	{
		if (empty($txn_id)) return false;

		$transaction_started = false;
		if (!$this->db->in_transaction)
		{
			$this->start_transaction();
			$transaction_started = true;
		}

		$sql = "SELECT * FROM `actg_account_transactions` WHERE txn_id = '" . $txn_id . "' LIMIT 1 FOR UPDATE";
		$txn = $this->db->getRow($sql);
		if (!$txn)
		{
			if ($transaction_started)
			{
				$this->rollback_transaction();
			}
			return false;
		}

		// Transaction already completed
		if ($txn['complete_seq'])
		{
			if ($transaction_started)
			{
				$this->rollback_transaction();
			}
			return false;
		}

		$update_txn = array();
		$update_txn['complete_date'] = $this->dbdate();

		// Get completion sequence number
		$sql = "SELECT IFNULL(max(`complete_seq`),0)+1 FROM `actg_account_transactions` WHERE `complete_seq` IS NOT NULL FOR UPDATE";
		$update_txn['complete_seq'] = $this->db->getOne($sql);

		// Calculate
		$sql = "SELECT `account_balance` FROM `actg_accounts` WHERE `account_id` = '" . $txn['account_id'] . "' LIMIT 1 FOR UPDATE";
		$update_txn['opening_balance'] = $this->db->getOne($sql);
		$update_txn['closing_balance'] = bcadd($update_txn['opening_balance'], $txn['amount']);

		// If bank_date not set, default to complete_date
		if (empty($txn['bank_date']))
		{
			$update_txn['bank_date'] = $update_txn['complete_date'];
		}

		if (!$this->db->autoExecute('actg_account_transactions', $update_txn, 'UPDATE', "txn_id = '" . $txn_id . "'"))
		{
			if ($transaction_started)
			{
				$this->rollback_transaction();
			}
			return false;
		}

		$sql = "UPDATE `actg_accounts` " .
			"SET `account_balance` = '" . $update_txn['closing_balance'] . "' " .
			"WHERE `account_id` = '" . $txn['account_id'] . "' LIMIT 1";
		if (!$this->db->query($sql))
		{
			if ($transaction_started)
			{
				$this->rollback_transaction();
			}
			return false;
		}

		if ($transaction_started)
		{
			$this->commit_transaction();
		}
		return true;
	}

	public function clearTransactions($account_id = 0, $since_complete_seq = 0)
	{
		$transaction_started = false;
		if (!$this->db->in_transaction)
		{
			$this->start_transaction();
			$transaction_started = true;
		}

		// Get reversing transactions and their reversed transactions
		$sql = "SELECT txn_id, related_txn_id " .
				"FROM `actg_account_transactions` " .
				"WHERE remark LIKE 'Reverse transaction%' " .
				"AND complete_seq IS NOT NULL " .
				"AND complete_seq > '" . $since_complete_seq . "' " .
				($account_id > 0 ? "AND account_id = '" . $account_id . "' " : '');
		$res = $this->db->getAll($sql);
		$reversed_txns = array();
		$reversing_txns = array();
		foreach ($res as $row)
		{
			$reversed_txns[] = $row['related_txn_id'];
			$reversing_txns[] = $row['txn_id'];
		}

		// Get all transactions
		$sql = "SELECT * FROM `actg_account_transactions` " .
				"WHERE complete_seq IS NOT NULL " .
				"AND complete_seq > '" . $since_complete_seq . "' " .
				($account_id > 0 ? "AND account_id = '" . $account_id . "' " : '') .
				"ORDER BY complete_seq ASC FOR UPDATE";
		$txns = $this->db->getAll($sql);
		if (empty($txns))
		{
			if ($transaction_started)
			{
				$this->rollback_transaction();
			}
			return true;
		}

		// Get account balance for all acccounts concerned
		$account_ids = array_unique(array_map(function ($row) { return $row['account_id']; }, $txns));
		$sql = "SELECT `account_id`, `account_balance` " .
				"FROM `actg_accounts` " .
				"WHERE `account_id` IN (" . implode(',',$account_ids) . ") " .
				"FOR UPDATE";
		$res = $this->db->getAll($sql);

		// Get opening balance for all accounts concerned
		$account_balances = array();
		$num_of_accounts = count($account_ids);
		$account_with_balance_found = 0;
		foreach($txns as $txn)
		{
			$aid = $txn['account_id'];
			if (!isset($account_balances[$aid]))
			{
				$account_balances[$aid] = $txn['opening_balance'];

				$account_with_balance_found = $account_with_balance_found + 1;
				if ($account_with_balance_found == $num_of_accounts)
				{
					break;
				}
			}
		}

		// Recalculate the opening / closing balance for each transaction
		foreach($txns as $txn)
		{
			if ((in_array($txn['txn_id'], $reversed_txns)) || (in_array($txn['txn_id'], $reversing_txns)))
			{
				continue;
			}

			$opening_balance = $account_balances[$txn['account_id']];
			$closing_balance = bcadd($opening_balance, $txn['amount']);
			$account_balances[$txn['account_id']] = $closing_balance;

			$sql = "UPDATE `actg_account_transactions` SET " .
					"`opening_balance` = '" . $opening_balance . "', " .
					"`closing_balance` = '" . $closing_balance . "' " .
					"WHERE `txn_id` = '" . $txn['txn_id'] . "' LIMIT 1";
			if (!$this->db->query($sql))
			{
				if ($transaction_started)
				{
					$this->rollback_transaction();
				}
				return false;
			}
		}

		// Update account balance
		foreach ($account_balances as $aid => $balance)
		{
			$sql = "UPDATE `actg_accounts` " .
					"SET `account_balance` = '" . $balance . "' " .
					"WHERE `account_id` = '" . $aid . "' LIMIT 1";
			if (!$this->db->query($sql))
			{
				if ($transaction_started)
				{
					$this->rollback_transaction();
				}
				return false;
			}
		}

		// Remove reversing transactions and there reversed transactions
		$sql = "DELETE FROM `actg_account_transactions` " .
				"WHERE txn_id " . db_create_in($reversing_txns);
		if (!$this->db->query($sql))
		{
			if ($transaction_started)
			{
				$this->rollback_transaction();
			}
			return false;
		}
		$sql = "DELETE FROM `actg_account_transactions` " .
				"WHERE txn_id " . db_create_in($reversed_txns);
		if (!$this->db->query($sql))
		{
			if ($transaction_started)
			{
				$this->rollback_transaction();
			}
			return false;
		}

		if ($transaction_started)
		{
			$this->commit_transaction();
		}
		return true;
	}

	public function deleteTransaction($txn_id,$account_id = 0, $payment_type_id = 0, $erp_order_id = 0)
	{
		if (empty($txn_id)) return false;

		$transaction_started = false;
		if (!$this->db->in_transaction)
		{
			$this->start_transaction();
			$transaction_started = true;
		}

		$txn = $this->getTransaction(compact('txn_id'));
		if (!empty($txn))
		{
			// Is transaction completed?
			if ($txn['complete_seq'])
			{
				// Is transaction not yet reversed?
				$sql = "SELECT '1' FROM `actg_account_transactions` WHERE related_txn_id = '" . $txn_id . "' AND remark LIKE 'Reverse transaction%' LIMIT 1";
				if (!$this->db->getOne($sql))
				{
					// Completed transaction cannot be deleted, but can be reversed
					$now = $this->dbdate();
					$reverse_txn = array();
					$reverse_txn['add_date'] = $now;
					$reverse_txn['complete_date'] = $now;
					$reverse_txn['bank_date'] = $now;
					$reverse_txn['currency_code'] = $txn['currency_code'];
					$reverse_txn['amount'] = bcmul($txn['amount'], -1);
					$reverse_txn['payment_type_id'] = $payment_type_id ? $payment_type_id : $txn['payment_type_id'];
					$reverse_txn['account_id'] = $account_id ? $account_id : $txn['account_id'];
					$reverse_txn['remark'] = 'Reverse transaction #' . $txn_id;
					$reverse_txn['related_txn_id'] = $txn_id;

					$reverse_txn_id = $this->addTransaction($reverse_txn);
					if (!$reverse_txn_id)
					{
						if ($transaction_started)
						{
							$this->rollback_transaction();
						}
						return false;
					}
					else
					{
						$sql = "SELECT * FROM `actg_credit_note_transactions` acnt LEFT JOIN `ecs_erp_credit_note` ecn ON ecn.cn_id = acnt.cn_id WHERE txn_id = $txn_id";
						$reverse_cn = $this->db->getAll($sql);
						if(count($reverse_cn) > 0)
						{
							$reduce_amount = 0;
							foreach($reverse_cn as $cn_txn)
							{
								$reverse_cn_txn = array(
									'txn_id' => $reverse_txn_id,
									'related_cn_txn_id' => $cn_txn['cn_txn_id'],
									'amount' => -$cn_txn['amount'],
									'remark' => 'Reverse transaction #' . $cn_txn['cn_txn_id']
								);
								$param = array(
									'cn_id' => $cn_txn['cn_id'],
									'curr_amount' => floatval($cn_txn['curr_amount']) - $cn_txn['amount'],
									'txn_info' => $reverse_cn_txn
								);
								$reduce_amount = bcadd($reduce_amount, $cn_txn['amount']);
								$erpController = new Yoho\cms\Controller\ErpController();
								$erpController->update_credit_note($param);
							}
							if (!empty($erp_order_id)) {
								$this->db->query("UPDATE " . $GLOBALS['ecs']->table('erp_order') . " SET `actg_paid_amount` = `actg_paid_amount` + $reduce_amount, `fully_paid` = 0 WHERE order_id = '$erp_order_id'");
							}
						}
					}
				}
			}
			else
			{
				// Transaction not completed yet, just delete it from transaction table
				$sql = "DELETE FROM `actg_account_transactions` WHERE txn_id = '" . $txn['txn_id'] . "' LIMIT 1";
				if (!$this->db->query($sql))
				{
					if ($transaction_started)
					{
						$this->rollback_transaction();
					}
					return false;
				}
			}
		}

		if ($transaction_started)
		{
			$this->commit_transaction();
		}
		return true;
	}

	/**
	 * Expense Types
	 **/

	public function getShopExpenseTypes($param = array(), $opt = array())
	{
		return $this->db->getAll("SELECT * " .
			"FROM `actg_shop_expense_types` " .
			"WHERE " . $this->build_where($param) .
			(isset($opt['orderby']) ? "ORDER BY " . $opt['orderby'] . " " : '') .
			(isset($opt['limit']) ? "LIMIT " . $opt['limit'] : ''));
	}

	public function getShopExpenseType($param)
	{
		if (empty($param)) return false;
		return $this->db->getRow("SELECT * " .
			"FROM `actg_shop_expense_types` " .
			"WHERE " . $this->build_where($param) . " LIMIT 1");
	}

	public function addShopExpenseType($param)
	{
		if (empty($param)) return false;
		if (!$this->param_contains_keys($param, array(
			'expense_type_name'
		))) return false;
		$param = $this->param_filter_keys($param, array(
			'expense_type_name'
		));
		return $this->db->autoExecute('actg_shop_expense_types', $param, 'INSERT') ? $this->db->insert_id() : false;
	}

	public function updateShopExpenseType($param)
	{
		if (empty($param['expense_type_id'])) return false;
		$expense_type_id = $param['expense_type_id'];
		unset($param['expense_type_id']);
		if (empty($param)) return false;
		return $this->db->autoExecute('actg_shop_expense_types', $param, 'UPDATE', "expense_type_id = '" . $expense_type_id . "'");
	}

	public function deleteShopExpenseType($expense_type_id)
	{
		if (empty($expense_type_id)) return false;
		return $this->db->query("DELETE FROM `actg_shop_expense_types` WHERE expense_type_id = '" . $expense_type_id . "' LIMIT 1");
	}

	/**
	 * Shop Expenses
	 **/

	public function getShopExpenses($param = array(), $opt = array())
	{
		return $this->db->getAll(
			"SELECT e.*, et.`expense_type_name`, c.`currency_name`, c.`currency_rate`, c.`currency_format` " .
			"FROM `actg_shop_expenses` as e " .
			"LEFT JOIN `actg_shop_expense_types` as et ON e.expense_type_id = et.expense_type_id " .
			"LEFT JOIN `actg_currencies` as c ON e.currency_code = c.currency_code " .
			"WHERE " . $this->build_where($param, 'e.') .
			(isset($opt['orderby']) ? "ORDER BY " . $opt['orderby'] . " " : '') .
			(isset($opt['limit']) ? "LIMIT " . $opt['limit'] : '')
		);
	}

	public function getShopExpense($param)
	{
		if (empty($param)) return false;
		return $this->db->getRow(
			"SELECT e.*, et.`expense_type_name`, c.`currency_name`, c.`currency_rate`, c.`currency_format` " .
			 "FROM `actg_shop_expenses` as e " .
			 "LEFT JOIN `actg_shop_expense_types` as et ON e.expense_type_id = et.expense_type_id " .
			 "LEFT JOIN `actg_currencies` as c ON e.currency_code = c.currency_code " .
			 "WHERE " . $this->build_where($param, 'e.') . " LIMIT 1");
	}

	public function addShopExpense($param)
	{
		if (empty($param)) return false;

		if (!$this->param_contains_keys($param, array(
			'expense_date', 'expense_type_id', 'currency_code', 'amount', 'operator'
		))) return false;

		$param = $this->param_filter_keys($param, array(
			'expense_date', 'expense_type_id', 'expense_desc', 'currency_code', 'amount', 'operator', 'remark', 'expense_year', 'expense_month'
		));

		$transaction_started = false;
		if (!$this->db->in_transaction)
		{
			$this->start_transaction();
			$transaction_started = true;
		}

		$expense_type = $this->getShopExpenseType(array('expense_type_id' => $param['expense_type_id']));
		$expense_type_name = $expense_type['expense_type_name'];

		$txn_remark = $expense_type_name .
			(empty($param['expense_desc']) ? '' : ': ' . $param['expense_desc']) .
			(empty($param['remark'])       ? '' : ' ' . $param['remark']);

		$param['expense_year'] =$param['expense_year'] = empty($param['expense_year']) ? local_date('Y') : $param['expense_year'];
		$param['expense_month'] = empty($param['expense_month']) ? local_date('m') : $param['expense_month'];

		$txn = array(
			'add_date' => $param['expense_date'],
			'complete_date' => $param['expense_date'],
			'currency_code' => $param['currency_code'],
			'amount' => bcmul($param['amount'], -1), // 支出
			'payment_type_id' => 1, // 現金
			'account_id' => 1, // 門店現金
			'remark' => $txn_remark,
			'admin_id' => $param['operator'],
			'expense_year' => $param['expense_year'],
			'expense_month' => $param['expense_month']
		);
		if($param['expense_type_id'] == 2) $txn['txn_type_id'] = 44;
		$param['txn_id'] = $this->addTransaction($txn);
		if (!$param['txn_id'])
		{
			if ($transaction_started)
			{
				$this->rollback_transaction();
			}
			return false;
		}

		if (!$this->db->autoExecute('actg_shop_expenses', $param, 'INSERT'))
		{
			if ($transaction_started)
			{
				$this->rollback_transaction();
			}
			return false;
		}
		$expense_id = $this->db->insert_id();
		if ($transaction_started)
		{
			$this->commit_transaction();
		}
		return $expense_id;
	}

	// public function updateShopExpense($param)
	// {
	// 	if (empty($param['expense_id'])) return false;
	// 	$expense_id = $param['expense_id'];
	// 	unset($param['expense_id']);
	// 	if (empty($param)) return false;
	// 	return $this->db->autoExecute('actg_shop_expenses', $param, 'UPDATE', "expense_id = '" . $expense_id . "'");
	// }

	public function deleteShopExpense($expense_id)
	{
		if (empty($expense_id)) return false;

		$expense = $this->getShopExpense(compact('expense_id'));
		if (empty($expense)) return false;

		$transaction_started = false;
		if (!$this->db->in_transaction)
		{
			$this->start_transaction();
			$transaction_started = true;
		}

		if (!$this->deleteTransaction($expense['txn_id']))
		{
			if ($transaction_started)
			{
				$this->rollback_transaction();
			}
			return false;
		}

		if (!$this->db->query("DELETE FROM `actg_shop_expenses` WHERE expense_id = '" . $expense_id . "' LIMIT 1"))
		{
			if ($transaction_started)
			{
				$this->rollback_transaction();
			}
			return false;
		}
		if ($transaction_started)
		{
			$this->commit_transaction();
		}
		return true;
	}

	/**
	 * Account Payment Methods
	 **/

	public function getAccountPaymentMethods($param = array(), $opt = array())
	{
		return $this->db->getAll(
			"SELECT apm.*, a.*, c.`currency_name`, c.`currency_rate`, c.`currency_format`, p.`payment_type_name` " .
			"FROM `actg_account_payment_methods` as apm " .
			"LEFT JOIN `actg_accounts` as a ON apm.account_id = a.account_id " .
			"LEFT JOIN `actg_currencies` as c ON a.currency_code = c.currency_code " .
			"LEFT JOIN `actg_payment_types` as p ON apm.payment_type_id = p.payment_type_id " .
			"WHERE " . $this->build_where($param, 'apm.') .
			(isset($opt['orderby']) ? "ORDER BY " . $opt['orderby'] . " " : '') .
			(isset($opt['limit']) ? "LIMIT " . $opt['limit'] : '')
		);
	}

	public function getAccountPaymentMethod($param)
	{
		if (empty($param)) return false;
		return $this->db->getRow(
			"SELECT apm.*, a.*, c.`currency_name`, c.`currency_rate`, c.`currency_format`, p.`payment_type_name` " .
			"FROM `actg_account_payment_methods` as apm " .
			"LEFT JOIN `actg_accounts` as a ON apm.account_id = a.account_id " .
			"LEFT JOIN `actg_currencies` as c ON a.currency_code = c.currency_code " .
			"LEFT JOIN `actg_payment_types` as p ON apm.payment_type_id = p.payment_type_id " .
			"WHERE " . $this->build_where($param, 'apm.') . " LIMIT 1"
		);
	}

	public function addAccountPaymentMethod($param)
	{
		if (empty($param)) return false;
		if (!$this->param_contains_keys($param, array(
			'account_id', 'pay_code', 'payment_type_id'
		))) return false;
		$param = $this->param_filter_keys($param, array(
			'account_id', 'pay_code', 'payment_type_id'
		));
		return $this->db->autoExecute('actg_account_payment_methods', $param, 'INSERT') ? $this->db->insert_id() : false;
	}

	public function activeAccountPaymentMethod($payment_method_id, $type_id)
	{
		if (empty($payment_method_id)) return false;
		return $this->db->autoExecute('actg_account_payment_methods', array('payment_type_id' => $type_id, 'active' => 1),
		'UPDATE', "payment_method_id = '" . $payment_method_id . "'");
	}

	public function unactiveAccountPaymentMethod($payment_method_id)
	{
		if (empty($payment_method_id)) return false;
		return $this->db->autoExecute('actg_account_payment_methods', array('active' => 0), 'UPDATE', "payment_method_id = '" . $payment_method_id . "'");
	}

	public function deleteAccountPaymentMethod($payment_method_id)
	{
		if (empty($payment_method_id)) return false;
		return $this->db->query("DELETE FROM `actg_account_payment_methods` WHERE payment_method_id = '" . $payment_method_id . "' LIMIT 1");
	}

	/**
	 * Order Payments
	 **/

	public function getOrderPayments($param = array(), $opt = array())
	{
		return $this->db->getAll(
			"SELECT op.*, c.`currency_name`, c.`currency_rate`, c.`currency_format`, p.`payment_type_name`, a.`account_name`, a.`account_number` " .
			"FROM `actg_order_payments` as op " .
			"LEFT JOIN `actg_currencies` as c ON op.currency_code = c.currency_code " .
			"LEFT JOIN `actg_payment_types` as p ON op.payment_type_id = p.payment_type_id " .
			"LEFT JOIN `actg_accounts` as a ON op.account_id = a.account_id " .
			"WHERE " . $this->build_where($param, 'op.') .
			(isset($opt['orderby']) ? "ORDER BY " . $opt['orderby'] . " " : '') .
			(isset($opt['limit']) ? "LIMIT " . $opt['limit'] : '')
		);
	}

	public function getOrderPayment($param)
	{
		if (empty($param)) return false;
		return $this->db->getRow(
			"SELECT op.*, c.`currency_name`, c.`currency_rate`, c.`currency_format`, p.`payment_type_name`, a.`account_name`, a.`account_number` " .
			"FROM `actg_order_payments` as op " .
			"LEFT JOIN `actg_currencies` as c ON op.currency_code = c.currency_code " .
			"LEFT JOIN `actg_payment_types` as p ON op.payment_type_id = p.payment_type_id " .
			"LEFT JOIN `actg_accounts` as a ON op.account_id = a.account_id " .
			"WHERE " . $this->build_where($param, 'op.') . " LIMIT 1");
	}

	public function addOrderPayment($param, $created_by)
	{
		if (empty($param)) return false;

			$is_sa_redeem = $this->idOrderSaRedeem($param['order_id']);

			if (!$is_sa_redeem){

				if (!$this->param_contains_keys($param, array(
					'order_id', 'payment_date', 'currency_code', 'amount', 'payment_type_id', 'account_id'
				))) return false;

				$param = $this->param_filter_keys($param, array(
					'order_id', 'payment_date', 'currency_code', 'amount', 'payment_type_id', 'account_id', 'remark', 'refund_id'
				));

				$transaction_started = false;

				if (!$this->db->in_transaction)
				{
					$this->start_transaction();
					$transaction_started = true;
				}

			}

			$order_sn = $this->db->getOne("SELECT order_sn FROM " . $GLOBALS['ecs']->table('order_info') . " WHERE order_id = '" . $param['order_id'] . "' LIMIT 1");

			$sql_get_ins_goods = "SELECT g.goods_id, g.goods_name, SUM(og.goods_number) AS goods_number ".
								"FROM ".$GLOBALS['ecs']->table('order_goods')." AS og ".
								"LEFT JOIN ".$GLOBALS['ecs']->table('order_info')." AS oi ON oi.order_id = og.order_id ".
								"LEFT JOIN ".$GLOBALS['ecs']->table('goods')." AS g ON g.goods_id = og.goods_id ".
								"WHERE oi.order_sn = '".$order_sn."' ".
								"AND g.is_insurance = '1' ".
								"AND og.insurance_of != '0' ".
								"AND og.goods_number != '0' ".
								"GROUP BY g.goods_id ";

			$ins_goods = $this->db->getAll($sql_get_ins_goods);

			$is_refund = (bccomp($param['amount'], 0) < 0);

			$txn_remark = '訂單 ' . $order_sn . ' ' . ($is_refund ? '退款' : '付款') .
				(empty($param['remark']) ? '' : ' ' . $param['remark']);


			if (!empty($ins_goods)){
				$txn_remark .= " - <span class='blue'>附保險: </span>";
				foreach ($ins_goods as $key => $good){
					if ($key > 0){$txn_remark .= ", ";}
					$txn_remark .= str_replace(array("額外保養期"," - "), array("", ""), $good['goods_name'])."x".$good['goods_number'];
				}
				$txn_remark = mysql_real_escape_string($txn_remark);
			}

			if (!$is_sa_redeem){

				$txn = array(
					'add_date' => $param['payment_date'],
					'complete_date' => $param['payment_date'],
					'currency_code' => $param['currency_code'],
					'amount' => $param['amount'],
					'payment_type_id' => $param['payment_type_id'],
					'account_id' => $param['account_id'],
					'remark' => $txn_remark,
					'txn_type_id' => ($is_refund ? 46 /* Refund */ : 1 /* Sales */),
					'expense_year' =>  date("Y"),
					'expense_month' => date("m"),
				);
			}

		$accountingController = new Yoho\cms\Controller\AccountingController();

		if ($is_refund) {
			$refundTxn = [
				'account_id' 		=> $param['account_id'],
				'rec_id'            => $param['refund_id'],
                'sales_order_id'    => $param['order_id'],
                'order_sn'          => $order_sn,
                'amount'            => bcmul($param['amount'], -1, 2),
            ];
			$param['actg_txn_id'] = $accountingController->insertRefundUpdateTransactions($refundTxn, 2, 0);
		} else {

			$sales = [
				'sales_order_id'	=> $param['order_id'],
				'payment_type_id' 	=> $param['payment_type_id'],
				'account_id'	 	=> $param['account_id'],
				'amount' 			=> $param['amount'],
				'remark' 			=> $txn_remark,
				'created_by' 		=> $created_by,
			];

			$param['actg_txn_id'] = $accountingController->insertSalesTransactions($sales);
		}
		

		// $param['txn_id'] = $this->addTransaction($txn);
		if (!$param['actg_txn_id'] && !$is_refund)
		{
			if ($transaction_started)
			{
				$this->rollback_transaction();
			}
			return false;
		}

		if (!$is_sa_redeem){
			if (!$this->db->autoExecute('actg_order_payments', $param, 'INSERT'))
			{
				if ($transaction_started)
				{
					$this->rollback_transaction();
				}
				return false;
			}
			$order_payment_id = $this->db->insert_id();
		}
		if ($transaction_started)
		{
			$this->commit_transaction();
		}
		return $order_payment_id;
	}

	// public function updateOrderPayment($param)
	// {
	// 	if (empty($param['order_payment_id'])) return false;
	// 	$order_payment_id = $param['order_payment_id'];
	// 	unset($param['order_payment_id']);
	// 	if (empty($param)) return false;
	// 	return $this->db->autoExecute('actg_order_payments', $param, 'UPDATE', "order_payment_id = '" . $order_payment_id . "'");
	// }

	public function deleteOrderPayment($order_payment_id)
	{
		if (empty($order_payment_id)) return false;

		$orderPayment = $this->getOrderPayment(compact('order_payment_id'));
		if (empty($orderPayment)) return false;

		$transaction_started = false;
		if (!$this->db->in_transaction)
		{
			$this->start_transaction();
			$transaction_started = true;
		}

		if (!$this->db->query("DELETE FROM `actg_order_payments` WHERE order_payment_id = '" . $order_payment_id . "' LIMIT 1"))
		{
			if ($transaction_started)
			{
				$this->rollback_transaction();
			}
			return false;
		}
		if (!empty($orderPayment['txn_id'])) {
			if (!$this->deleteTransaction($orderPayment['txn_id']))
			{
				if ($transaction_started)
				{
					$this->rollback_transaction();
				}
				return false;
			}
		} elseif (!empty($orderPayment['actg_txn_id'])) {
			$accountingController = new Yoho\cms\Controller\AccountingController();
			$accountingController->reverseAccountingTransaction([
				'actg_txn_id' 	=> $orderPayment['actg_txn_id'],
				'created_by' 	=> $_SESSION['admin_id'],
			], true);
		}

		if ($transaction_started)
		{
			$this->commit_transaction();
		}
		return true;
	}

	/**
	 * Currency Exchanges
	 **/

	public function getCurrencyExchanges($param = array(), $opt = array())
	{
		return $this->db->getAll(
			"SELECT ce.*, " .
				"au.user_name as admin_name, ".
				"f_c.`currency_name` as from_currency_name, " .
				"f_c.`currency_rate` as from_currency_rate, " .
				"f_c.`currency_format` as from_currency_format, " .
				"f_p.`payment_type_name` as from_payment_type_name, " .
				"f_a.`account_name` as from_account_name, " .
				"f_a.`account_number` as from_account_number, " .
				"t_c.`currency_name` as to_currency_name, " .
				"t_c.`currency_rate` as to_currency_rate, " .
				"t_c.`currency_format` as to_currency_format, " .
				"t_p.`payment_type_name` as to_payment_type_name, " .
				"t_a.`account_name` as to_account_name, " .
				"t_a.`account_number` as to_account_number " .
			"FROM `actg_currency_exchanges` as ce " .
			"LEFT JOIN `actg_currencies` as f_c ON ce.from_currency = f_c.currency_code " .
			"LEFT JOIN `actg_payment_types` as f_p ON ce.from_payment_type = f_p.payment_type_id " .
			"LEFT JOIN `actg_accounts` as f_a ON ce.from_account = f_a.account_id " .
			"LEFT JOIN `actg_currencies` as t_c ON ce.to_currency = t_c.currency_code " .
			"LEFT JOIN `actg_payment_types` as t_p ON ce.to_payment_type = t_p.payment_type_id " .
			"LEFT JOIN `actg_accounts` as t_a ON ce.to_account = t_a.account_id " .
			"LEFT JOIN (SELECT user_id, user_name FROM " . $GLOBALS['ecs']->table('admin_user') . ") as au ON ce.operator = au.user_id " .
			"WHERE " . $this->build_where($param, 'ce.') .
			(isset($opt['orderby']) ? "ORDER BY " . $opt['orderby'] . " " : '') .
			(isset($opt['limit']) ? "LIMIT " . $opt['limit'] : '')
		);
	}

	public function getCurrencyExchange($param)
	{
		if (empty($param)) return false;
		return $this->db->getRow(
			"SELECT ce.*, " .
				"au.user_name as admin_name, ".
				"f_c.`currency_name` as from_currency_name, " .
				"f_c.`currency_rate` as from_currency_rate, " .
				"f_c.`currency_format` as from_currency_format, " .
				"f_p.`payment_type_name` as from_payment_type_name, " .
				"f_a.`account_name` as from_account_name, " .
				"f_a.`account_number` as from_account_number, " .
				"t_c.`currency_name` as to_currency_name, " .
				"t_c.`currency_rate` as to_currency_rate, " .
				"t_c.`currency_format` as to_currency_format, " .
				"t_p.`payment_type_name` as to_payment_type_name, " .
				"t_a.`account_name` as to_account_name, " .
				"t_a.`account_number` as to_account_number " .
			"FROM `actg_currency_exchanges` as ce " .
			"LEFT JOIN `actg_currencies` as f_c ON ce.from_currency = f_c.currency_code " .
			"LEFT JOIN `actg_payment_types` as f_p ON ce.from_payment_type = f_p.payment_type_id " .
			"LEFT JOIN `actg_accounts` as f_a ON ce.from_account = f_a.account_id " .
			"LEFT JOIN `actg_currencies` as t_c ON ce.to_currency = t_c.currency_code " .
			"LEFT JOIN `actg_payment_types` as t_p ON ce.to_payment_type = t_p.payment_type_id " .
			"LEFT JOIN `actg_accounts` as t_a ON ce.to_account = t_a.account_id " .
			"LEFT JOIN (SELECT user_id, user_name FROM " . $GLOBALS['ecs']->table('admin_user') . ") as au ON ce.operator = au.user_id " .
			"WHERE " . $this->build_where($param, 'ce.') . " LIMIT 1");
	}

	public function addCurrencyExchange($param)
	{
		if (empty($param)) return false;

		if (!$this->param_contains_keys($param, array(
			'exchange_date',
			'from_account', 'from_currency', 'from_amount', 'from_payment_type',
			'to_account', 'to_currency', 'to_amount', 'to_payment_type',
		))) return false;

		$param = $this->param_filter_keys($param, array(
			'exchange_date',
			'from_account', 'from_currency', 'from_amount', 'from_payment_type',
			'to_account', 'to_currency', 'to_amount', 'to_payment_type',
			'operator', 'remark'
		));

		$from_curr = $this->getCurrency(array('currency_code' => $param['from_currency']));
		$to_curr = $this->getCurrency(array('currency_code' => $param['to_currency']));
		if (empty($from_curr) || empty($to_curr))
		{
			return false;
		}
		$from_acc = $this->getAccount(array('account_id' => $param['from_account']));
		$to_acc = $this->getAccount(array('account_id' => $param['to_account']));

		$transaction_started = false;
		if (!$this->db->in_transaction)
		{
			$this->start_transaction();
			$transaction_started = true;
		}

		$from_txn_remark = '轉帳至 '. $to_acc['account_name'] . ' ' .
			$this->money_format($param['from_amount'], $from_curr['currency_format']) .
			' → ' .
			$this->money_format($param['to_amount'], $to_curr['currency_format']) .
			(empty($param['remark']) ? '' : ' ' . $param['remark']);

		$from_txn = array(
			'add_date' => $param['exchange_date'],
			'complete_date' => $param['exchange_date'],
			'currency_code' => $param['from_currency'],
			'amount' => bcmul($param['from_amount'], -1),
			'payment_type_id' => $param['from_payment_type'],
			'account_id' => $param['from_account'],
			'remark' => $from_txn_remark,
			'admin_id' => $param['operator'],
			'txn_type_id' => 44 /* Internal Transfer */
		);
		$param['from_txn_id'] = $this->addTransaction($from_txn);
		if (!$param['from_txn_id'])
		{
			if ($transaction_started)
			{
				$this->rollback_transaction();
			}
			return false;
		}

		$to_txn_remark = '轉帳自 '. $from_acc['account_name'] . ' ' .
			$this->money_format($param['from_amount'], $from_curr['currency_format']) .
			' → ' .
			$this->money_format($param['to_amount'], $to_curr['currency_format']) .
			(empty($param['remark']) ? '' : ' ' . $param['remark']);

		$to_txn = array(
			'add_date' => $param['exchange_date'],
			'complete_date' => $param['exchange_date'],
			'currency_code' => $param['to_currency'],
			'amount' => $param['to_amount'],
			'payment_type_id' => $param['to_payment_type'],
			'account_id' => $param['to_account'],
			'remark' => $to_txn_remark,
			'admin_id' => $param['operator'],
			'txn_type_id' => 44 /* Internal Transfer */
		);
		$param['to_txn_id'] = $this->addTransaction($to_txn);
		if (!$param['to_txn_id'])
		{
			if ($transaction_started)
			{
				$this->rollback_transaction();
			}
			return false;
		}

		if (!$this->db->autoExecute('actg_currency_exchanges', $param, 'INSERT'))
		{
			if ($transaction_started)
			{
				$this->rollback_transaction();
			}
			return false;
		}
		$exchange_id = $this->db->insert_id();
		if ($transaction_started)
		{
			$this->commit_transaction();
		}
		return $exchange_id;
	}

	// public function updateCurrencyExchange($param)
	// {
	// 	if (empty($param['exchange_id'])) return false;
	// 	$exchange_id = $param['exchange_id'];
	// 	unset($param['exchange_id']);
	// 	if (empty($param)) return false;
	// 	return $this->db->autoExecute('actg_currency_exchanges', $param, 'UPDATE', "exchange_id = '" . $exchange_id . "'");
	// }

	public function deleteCurrencyExchange($exchnage_id)
	{
		if (empty($exchnage_id)) return false;

		$exchange = $this->getCurrencyExchange(compact('exchnage_id'));
		if (empty($exchange)) return false;

		$transaction_started = false;
		if (!$this->db->in_transaction)
		{
			$this->start_transaction();
			$transaction_started = true;
		}

		// Delete the currency exchange record
		if (!$this->db->query("DELETE FROM `actg_currency_exchanges` WHERE exchnage_id = '" . $exchnage_id . "' LIMIT 1"))
		{
			if ($transaction_started)
			{
				$this->rollback_transaction();
			}
			return false;
		}

		// Delete "from" transaction
		if (!$this->deleteTransaction($exchange['from_txn_id']))
		{
			if ($transaction_started)
			{
				$this->rollback_transaction();
			}
			return false;
		}

		// Delete "to" transaction
		if (!$this->deleteTransaction($exchange['to_txn_id']))
		{
			if ($transaction_started)
			{
				$this->rollback_transaction();
			}
			return false;
		}

		if ($transaction_started)
		{
			$this->commit_transaction();
		}
		return true;
	}

	/**
	 * Wholesale Payments
	 **/

	public function getWholesalePayments($param = array(), $opt = array())
	{
		// Define table prefixes to use with ambiguous columns
		$prefixes = array(
			'order_id' => 'wp.',
			'txn_id' => 'wp.',
			'currency_code' => 'c.',
			'payment_type_id' => 't.',
			'account_id' => 'a.',
		);
		return $this->db->getAll(
			"SELECT wp.*, oi.`order_sn`, oi.`order_amount`, oi.`pay_status`, " .
				"IFNULL(t.`add_date`, FROM_UNIXTIME(UNIX_TIMESTAMP(at.`created_at`) - 8 * 3600)) as add_date, " .
				"IFNULL(t.`complete_date`, FROM_UNIXTIME(UNIX_TIMESTAMP(at.`txn_date`) - 8 * 3600)) as complete_date, " .
				"IFNULL(t.`amount`, at.`amount`) as amount, " .
				"c.`currency_code`, c.`currency_name`, c.`currency_rate`, c.`currency_format`, " .
				"p.`payment_type_id`, p.`payment_type_name`, " .
				"a.`account_id`, a.`account_name`, a.`account_number` " .
			"FROM `actg_wholesale_payments` as wp " .
			"LEFT JOIN" . $GLOBALS['ecs']->table('order_info') . " as oi ON wp.order_id = oi.order_id " .
			"LEFT JOIN `actg_account_transactions` as t ON wp.txn_id = t.txn_id AND wp.actg_txn_id IS NULL " .
			"LEFT JOIN `actg_accounting_transactions` as at ON wp.actg_txn_id = at.actg_txn_id AND wp.txn_id IS NULL " .
			"LEFT JOIN `actg_currencies` as c ON ((t.currency_code = c.currency_code AND wp.actg_txn_id IS NULL) OR (at.currency_code = c.currency_code AND wp.txn_id IS NULL)) " .
			"LEFT JOIN `actg_payment_types` as p ON t.payment_type_id = p.payment_type_id AND wp.actg_txn_id IS NULL " .
			"LEFT JOIN `actg_accounts` as a ON ((t.account_id = a.account_id AND wp.actg_txn_id IS NULL) OR (at.actg_type_id = a.actg_type_id AND wp.txn_id IS NULL)) " .
			"WHERE " . $this->build_where($param, $prefixes) . " GROUP BY wholesale_payment_id " .
			(isset($opt['orderby']) ? "ORDER BY " . $opt['orderby'] . " " : '') .
			(isset($opt['limit']) ? "LIMIT " . $opt['limit'] : '')
		);
	}

	public function getWholesalePayment($param)
	{
		if (empty($param)) return false;
		// Define table prefixes to use with ambiguous columns
		$prefixes = array(
			'order_id' => 'wp.',
			'txn_id' => 'wp.',
			'currency_code' => 'c.',
			'payment_type_id' => 't.',
			'account_id' => 'a.',
		);
		return $this->db->getRow(
			"SELECT wp.*, oi.`order_sn`, " .
				"IFNULL(t.`add_date`, FROM_UNIXTIME(UNIX_TIMESTAMP(at.`created_at`) - 8 * 3600)) as add_date, " .
				"IFNULL(t.`complete_date`, FROM_UNIXTIME(UNIX_TIMESTAMP(at.`txn_date`) - 8 * 3600)) as complete_date, " .
				"IFNULL(t.`amount`, at.`amount`) as amount, " .
				"c.`currency_code`, c.`currency_name`, c.`currency_rate`, c.`currency_format`, " .
				"p.`payment_type_id`, p.`payment_type_name`, " .
				"a.`account_id`, a.`account_name`, a.`account_number` " .
			"FROM `actg_wholesale_payments` as wp " .
			"LEFT JOIN" . $GLOBALS['ecs']->table('order_info') . " as oi ON wp.order_id = oi.order_id " .
			"LEFT JOIN `actg_account_transactions` as t ON wp.txn_id = t.txn_id AND wp.actg_txn_id IS NULL " .
			"LEFT JOIN `actg_accounting_transactions` as at ON wp.actg_txn_id = at.actg_txn_id AND wp.txn_id IS NULL " .
			"LEFT JOIN `actg_currencies` as c ON ((t.currency_code = c.currency_code AND wp.actg_txn_id IS NULL) OR (at.currency_code = c.currency_code AND wp.txn_id IS NULL)) " .
			"LEFT JOIN `actg_payment_types` as p ON t.payment_type_id = p.payment_type_id AND wp.actg_txn_id IS NULL " .
			"LEFT JOIN `actg_accounts` as a ON t.account_id = a.account_id " .
			"WHERE " . $this->build_where($param, $prefixes) . " LIMIT 1");
	}

	public function addWholesalePayment($param)
	{
		if (empty($param)) return false;

		if (!$this->param_contains_keys($param, array(
			'order_id', 'add_date', 'currency_code', 'amount', 'payment_type_id', 'account_id', 'operator', 'exchanged_amount'
		))) return false;

		$param = $this->param_filter_keys($param, array(
			'order_id', 'add_date', 'complete_date', 'currency_code', 'amount', 'payment_type_id', 'account_id', 'operator', 'remark', 'type_sn', 'exchanged_amount'
		));

		$transaction_started = false;
		if (!$this->db->in_transaction)
		{
			$this->start_transaction();
			$transaction_started = true;
		}

		$order_sn = $this->db->getOne("SELECT order_sn FROM " . $GLOBALS['ecs']->table('order_info') . " WHERE order_id = '" . $param['order_id'] . "' LIMIT 1");

		$txn_remark = '批發訂單 ' . $order_sn . ' 付款' .
			(empty($param['type_sn']) ? '' : ' (' . $param['type_sn'] . ')') .
			(empty($param['remark']) ? '' : ' ' . $param['remark']);

		$txn = array(
			'add_date' => $param['add_date'],
			'complete_date' => $param['complete_date'],
			'currency_code' => $param['currency_code'],
			'amount' => $param['amount'],
			'exchanged_amount' => $param['exchanged_amount'],
			'payment_type_id' => $param['payment_type_id'],
			'account_id' => $param['account_id'],
			'remark' => $txn_remark,
			'admin_id' => $param['operator'],
			'txn_type_id' => 1 // Sales
		);
		$sales = [
			'sales_order_id'	=> $param['order_id'],
			'payment_type_id' 	=> $param['payment_type_id'],
			'account_id'	 	=> $param['account_id'],
			'amount' 			=> $param['amount'],
			'currency_code' 	=> $param['currency_code'],
			'exchanged_amount' => $param['exchanged_amount'],
			'remark' 			=> $txn_remark,
		];
		// $param['txn_id'] = $this->addTransaction($txn);
		$accountingController = new Yoho\cms\Controller\AccountingController();
		$param['actg_txn_id'] = $accountingController->insertSalesTransactions($sales);
		if (!$param['actg_txn_id'])
		{
			if ($transaction_started)
			{
				$this->rollback_transaction();
			}
			return false;
		}

		if (!$this->db->autoExecute('actg_wholesale_payments', $param, 'INSERT'))
		{
			if ($transaction_started)
			{
				$this->rollback_transaction();
			}
			return false;
		}
		$wholesale_payment_id = $this->db->insert_id();
		if ($transaction_started)
		{
			$this->commit_transaction();
		}
		return $wholesale_payment_id;
	}

	// public function updateWholesalePayment($param)
	// {
	// 	if (empty($param['wholesale_payment_id'])) return false;
	// 	$wholesale_payment_id = $param['wholesale_payment_id'];
	// 	unset($param['wholesale_payment_id']);
	// 	if (empty($param)) return false;
	// 	return $this->db->autoExecute('actg_wholesale_payments', $param, 'UPDATE', "wholesale_payment_id = '" . $wholesale_payment_id . "'");
	// }

	public function deleteWholesalePayment($wholesale_payment_id)
	{
		if (empty($wholesale_payment_id)) return false;

		$wholesalePayment = $this->getWholesalePayment(compact('wholesale_payment_id'));
		if (empty($wholesalePayment)) return false;

		$transaction_started = false;
		if (!$this->db->in_transaction)
		{
			$this->start_transaction();
			$transaction_started = true;
		}

		if (!$this->db->query("DELETE FROM `actg_wholesale_payments` WHERE wholesale_payment_id = '" . $wholesale_payment_id . "' LIMIT 1"))
		{
			if ($transaction_started)
			{
				$this->rollback_transaction();
			}
			return false;
		}

		if (!$this->deleteTransaction($wholesalePayment['txn_id']))
		{
			if ($transaction_started)
			{
				$this->rollback_transaction();
			}
			return false;
		}

		if ($transaction_started)
		{
			$this->commit_transaction();
		}
		return true;
	}

	/**
	 * Wholesale Payments
	 **/

	public function getPurchasePayments($param = array(), $opt = array())
	{
		// Define table prefixes to use with ambiguous columns
		$prefixes = array(
			'order_id' => 'pp.',
			'txn_id' => 'pp.',
			'currency_code' => 'c.',
			'payment_type_id' => 't.',
			'account_id' => 'a.',
		);
		return $this->db->getAll(
			"SELECT pp.*, eo.`order_sn`, " .
				"IFNULL(t.`add_date`, FROM_UNIXTIME(UNIX_TIMESTAMP(at.`created_at`) - 8 * 3600)) as add_date, " .
				"IFNULL(FROM_UNIXTIME(UNIX_TIMESTAMP(t.`complete_date`) + 8 * 3600), at.`txn_date`) as complete_date, " .
				"IFNULL(t.`amount`, at.`amount`) * -1 as amount, SUM(tcn.`amount`) * -1 as cn_amount, " .
				"c.`currency_code`, c.`currency_name`, c.`currency_rate`, c.`currency_format`, " .
				"p.`payment_type_id`, p.`payment_type_name`, " .
				"a.`account_id`, a.`account_name`, a.`account_number` " .
			"FROM `actg_purchase_payments` as pp " .
			"LEFT JOIN" . $GLOBALS['ecs']->table('erp_order') . " as eo ON pp.order_id = eo.order_id " .
			"LEFT JOIN `actg_account_transactions` as t ON pp.txn_id = t.txn_id AND pp.actg_txn_id IS NULL " .
			"LEFT JOIN `actg_accounting_transactions` as at ON pp.actg_txn_id = at.actg_txn_id AND pp.txn_id IS NULL AND at.`related_txn_id` IS NULL AND reversed = 0 AND reversed_txn_id IS NULL AND at.posted != '" . ACTG_POST_REJECTED . "' " .
			"LEFT JOIN `actg_credit_note_transactions` as tcn ON ((tcn.txn_id = t.txn_id AND pp.actg_txn_id IS NULL) OR (tcn.actg_txn_id = at.actg_txn_id AND pp.txn_id IS NULL)) " .
			"LEFT JOIN `actg_currencies` as c ON ((t.currency_code = c.currency_code AND pp.actg_txn_id IS NULL) OR (at.currency_code = c.currency_code AND pp.txn_id IS NULL)) " .
			"LEFT JOIN `actg_payment_types` as p ON t.payment_type_id = p.payment_type_id AND pp.actg_txn_id IS NULL " .
			"LEFT JOIN `actg_accounts` as a ON ((t.account_id = a.account_id AND pp.actg_txn_id IS NULL) OR (at.actg_type_id = a.actg_type_id AND pp.txn_id IS NULL)) " .
			"WHERE " . $this->build_where($param, $prefixes) . " GROUP BY purchase_payment_id " .
			(isset($opt['orderby']) ? "ORDER BY " . $opt['orderby'] . " " : '') .
			(isset($opt['limit']) ? "LIMIT " . $opt['limit'] : '')
		);
	}

	public function getPurchasePayment($param)
	{
		if (empty($param)) return false;
		// Define table prefixes to use with ambiguous columns
		$prefixes = array(
			'order_id' => 'pp.',
			'txn_id' => 'pp.',
			'currency_code' => 't.',
			'payment_type_id' => 't.',
			'account_id' => 't.',
		);
		return $this->db->getRow(
			"SELECT pp.*, eo.`order_sn`, " .
				"IFNULL(t.`add_date`, FROM_UNIXTIME(UNIX_TIMESTAMP(at.`created_at`) - 8 * 3600)) as add_date, " .
				"IFNULL(FROM_UNIXTIME(UNIX_TIMESTAMP(t.`complete_date`) + 8 * 3600), at.`txn_date`) as complete_date, " .
				"IFNULL(t.`amount`, at.`amount`) * -1 as amount, " .
				"c.`currency_code`, c.`currency_name`, c.`currency_rate`, c.`currency_format`, " .
				"p.`payment_type_id`, p.`payment_type_name`, " .
				"a.`account_id`, a.`account_name`, a.`account_number` " .
			"FROM `actg_purchase_payments` as pp " .
			"LEFT JOIN" . $GLOBALS['ecs']->table('erp_order') . " as eo ON pp.order_id = eo.order_id " .
			"LEFT JOIN `actg_account_transactions` as t ON pp.txn_id = t.txn_id AND pp.actg_txn_id IS NULL " .
			"LEFT JOIN `actg_accounting_transactions` as at ON pp.actg_txn_id = at.actg_txn_id AND pp.txn_id IS NULL AND at.`related_txn_id` IS NULL AND reversed = 0 AND reversed_txn_id IS NULL AND at.posted != '" . ACTG_POST_REJECTED . "' " .
			"LEFT JOIN `actg_currencies` as c ON ((t.currency_code = c.currency_code AND pp.actg_txn_id IS NULL) OR (at.currency_code = c.currency_code AND pp.txn_id IS NULL)) " .
			"LEFT JOIN `actg_payment_types` as p ON t.payment_type_id = p.payment_type_id AND pp.actg_txn_id IS NULL " .
			"LEFT JOIN `actg_accounts` as a ON ((t.account_id = a.account_id AND pp.actg_txn_id IS NULL) OR (at.actg_type_id = a.actg_type_id AND pp.txn_id IS NULL)) " .
			"WHERE " . $this->build_where($param, $prefixes) . " LIMIT 1");
	}

	// Deprecate in new accounting flow, function migrated to AccountingController -> insertPurchasePaymentTransactions
	public function addPurchasePayment($param)
	{
		if (empty($param)) return false;

		if (!$this->param_contains_keys($param, array(
			'order_id', 'add_date', 'currency_code', 'amount', 'payment_type_id', 'account_id', 'operator'
		))) return false;

		$param = $this->param_filter_keys($param, array(
			'order_id', 'add_date', 'bank_date', 'complete_date', 'currency_code', 'amount', 'payment_type_id', 'account_id', 'operator', 'remark', 'cn_ids', 'cn_amount',
			'expense_year', 'expense_month'
		));

		$transaction_started = false;
		if (!$this->db->in_transaction)
		{
			$this->start_transaction();
			$transaction_started = true;
		}

		if(!empty($param['cn_ids']))
		{
			$cns = get_credit_note($param['cn_ids'], "", " ORDER BY curr_amount ASC");
			$remain_credit = $param['cn_amount'];
			foreach($cns as $cn){
				if($remain_credit > 0){
					$curr_amount = doubleval($cn['curr_amount']);
					$used_credit = $curr_amount > $remain_credit ? $remain_credit : $curr_amount;
					$params[] = array(
						'cn_id' => $cn['cn_id'],
						'curr_amount' => $curr_amount - $used_credit,
						'txn_info' => array(
							'amount' => -$used_credit
						)
					);
					$param['cn_sn'][] = $cn['cn_sn'];
					$remain_credit -= $curr_amount;
				}
			}
		}

		$order_sn = $this->db->getOne("SELECT order_sn FROM " . $GLOBALS['ecs']->table('erp_order') . " WHERE order_id = '" . $param['order_id'] . "' LIMIT 1");
		$supplier = $this->db->getOne("SELECT name FROM " . $GLOBALS['ecs']->table('erp_order') . " eo " .
						"LEFT JOIN " . $GLOBALS['ecs']->table('erp_supplier') . " es ON eo.supplier_id = es.supplier_id " .
						"WHERE eo.order_sn = '$order_sn'");

		$txn_remark = '採購訂單 ' . $order_sn . ' (' . mysql_escape_string($supplier) . ') 付款' .
			(empty($param['cn_sn']) ? '' : ' (' . implode(', ', $param['cn_sn']) . ' 共扣減 $' . $param['cn_amount'] .')') .
			(empty($param['remark']) ? '' : ' ' . $param['remark']);

		$param['expense_year'] = empty($param['expense_year']) ? local_date('Y') : $param['expense_year'];
		$param['expense_month'] = empty($param['expense_month']) ? local_date('m') : $param['expense_month'];

		$txn = array(
			'add_date' => $param['add_date'],
			'bank_date' => $param['bank_date'],
			'complete_date' => $param['complete_date'],
			'currency_code' => $param['currency_code'],
			'amount' => bcmul($param['amount'], -1), // Debit
			'payment_type_id' => $param['payment_type_id'],
			'account_id' => $param['account_id'],
			'remark' => $txn_remark,
			'txn_type_id' => 4, // Purchase
			'expense_year' => $param['expense_year'],
			'expense_month' => $param['expense_month'],
			'admin_id' => $param['operator'],
		);
		$param['txn_id'] = $this->addTransaction($txn);
		if (!$param['txn_id'])
		{
			if ($transaction_started)
			{
				$this->rollback_transaction();
			}
			return false;
		}
		else
		{
			if(!empty($param['cn_sn'])){
				foreach($params as $p){
					$p['txn_info']['txn_id'] = $param['txn_id'];
					$p['txn_info']['remark'] = "支付: " . $order_sn;
					$erpController = new Yoho\cms\Controller\ErpController();
					$erpController->update_credit_note($p);
				}
			}
		}

		// bank_date is only available in transaction table, not in purchase_payments table
		if (array_key_exists('bank_date', $param))
		{
			unset($param['bank_date']);
		}

		if (!$this->db->autoExecute('actg_purchase_payments', $param, 'INSERT'))
		{
			if ($transaction_started)
			{
				$this->rollback_transaction();
			}
			return false;
		}
		$purchase_payment_id = $this->db->insert_id();
		if ($transaction_started)
		{
			$this->commit_transaction();
		}
		return $purchase_payment_id;
	}

	// public function updatePurchasePayment($param)
	// {
	// 	if (empty($param['purchase_payment_id'])) return false;
	// 	$purchase_payment_id = $param['purchase_payment_id'];
	// 	unset($param['purchase_payment_id']);
	// 	if (empty($param)) return false;
	// 	return $this->db->autoExecute('actg_purchase_payments', $param, 'UPDATE', "purchase_payment_id = '" . $purchase_payment_id . "'");
	// }

	public function deletePurchasePayment($purchase_payment_id)
	{
		if (empty($purchase_payment_id)) return false;

		$purchasePayment = $this->getPurchasePayment(compact('purchase_payment_id'));
		if (empty($purchasePayment)) return false;

		$transaction_started = false;
		if (!$this->db->in_transaction)
		{
			$this->start_transaction();
			$transaction_started = true;
		}

		if (!$this->db->query("DELETE FROM `actg_purchase_payments` WHERE purchase_payment_id = '" . $purchase_payment_id . "' LIMIT 1"))
		{
			if ($transaction_started)
			{
				$this->rollback_transaction();
			}
			return false;
		}

		$actgHook = new AccountingHooks($this, $this->db);
		$paid_amounts = $actgHook->getPurchaseOrderPaidAmounts($purchasePayment['order_id']);
		$total_paid = $actgHook->calculateTotalPaidAmount($paid_amounts);
		$this->db->query("UPDATE " . $GLOBALS['ecs']->table('erp_order') . " SET `actg_paid_amount` = $total_paid, `fully_paid` = 0 WHERE order_id = '$purchasePayment[order_id]'");

		if (!empty($purchasePayment['txn_id'])) {
			if (!$this->deleteTransaction($purchasePayment['txn_id'], 0, 0, $purchasePayment['order_id']))
			{
				if ($transaction_started)
				{
					$this->rollback_transaction();
				}
				return false;
			}
		} elseif (!empty($purchasePayment['actg_txn_id'])) {
			$accountingController = new Yoho\cms\Controller\AccountingController();
			$accountingController->reverseAccountingTransaction([
				'actg_txn_id' 	=> $purchasePayment['actg_txn_id'],
				'created_by' 	=> $_SESSION['admin_id'],
			], true);
		}

		if ($transaction_started)
		{
			$this->commit_transaction();
		}
		return true;
	}

	/**
	 * Payment Currencies
	 **/

	public function getPaymentCurrencies($param = array(), $opt = array())
	{
		return $this->db->getAll(
			"SELECT pc.*, c.`currency_name`, c.`currency_rate`, c.`currency_format` " .
			"FROM `actg_payment_currencies` as pc " .
			"LEFT JOIN `actg_currencies` as c ON pc.currency_code = c.currency_code " .
			"WHERE " . $this->build_where($param, 'pc.') .
			(isset($opt['orderby']) ? "ORDER BY " . $opt['orderby'] . " " : '') .
			(isset($opt['limit']) ? "LIMIT " . $opt['limit'] : '')
		);
	}

	public function getPaymentCurrency($param)
	{
		if (empty($param)) return false;
		return $this->db->getRow(
			"SELECT pc.*, c.`currency_name`, c.`currency_rate`, c.`currency_format` " .
			"FROM `actg_payment_currencies` as pc " .
			"LEFT JOIN `actg_currencies` as c ON pc.currency_code = c.currency_code " .
			"WHERE " . $this->build_where($param, 'pc.') . " LIMIT 1"
		);
	}

	public function addPaymentCurrency($param)
	{
		if (empty($param)) return false;
		if (!$this->param_contains_keys($param, array(
			'pay_code', 'currency_code'
		))) return false;
		$param = $this->param_filter_keys($param, array(
			'pay_code', 'currency_code', 'default_currency'
		));
		return $this->db->autoExecute('actg_payment_currencies', $param, 'INSERT') ? $this->db->insert_id() : false;
	}

	public function deletePaymentCurrency($payment_currency_id)
	{
		if (empty($payment_currency_id)) return false;
		return $this->db->query("DELETE FROM `actg_payment_currencies` WHERE payment_currency_id = '" . $payment_currency_id . "' LIMIT 1");
	}

	/**
	 * Transaction Types
	 **/

	public function getTransactionTypes($param = array(), $opt = array())
	{
		return $this->db->getAll(
			"SELECT tt.*, ttp.`txn_type_name` as txn_type_parent_name " .
			"FROM `actg_transaction_types` as tt " .
			"LEFT JOIN `actg_transaction_types` as ttp ON tt.`txn_type_parent` = ttp.`txn_type_id` " .
			"WHERE " . $this->build_where($param, 'tt.') .
			(isset($opt['orderby']) ? "ORDER BY " . $opt['orderby'] . " " : '') .
			(isset($opt['limit']) ? "LIMIT " . $opt['limit'] : '')
		);
	}

	public function getTransactionType($param)
	{
		if (empty($param)) return false;
		return $this->db->getRow(
			"SELECT tt.*, ttp.`txn_type_name` as txn_type_parent_name " .
			"FROM `actg_transaction_types` as tt " .
			"LEFT JOIN `actg_transaction_types` as ttp ON tt.`txn_type_parent` = ttp.`txn_type_id` " .
			"WHERE " . $this->build_where($param, 'tt.') . " LIMIT 1"
		);
	}

	public function addTransactionType($param)
	{
		if (empty($param)) return false;
		if (!$this->param_contains_keys($param, array(
			'txn_type_name'
		))) return false;
		$param = $this->param_filter_keys($param, array(
			'txn_type_name', 'txn_type_parent'
		));
		return $this->db->autoExecute('actg_transaction_types', $param, 'INSERT') ? $this->db->insert_id() : false;
	}

	public function updateTransactionType($param)
	{
		if (empty($param['txn_type_id'])) return false;
		$txn_type_id = $param['txn_type_id'];
		unset($param['txn_type_id']);
		if (empty($param)) return false;
		return $this->db->autoExecute('actg_transaction_types', $param, 'UPDATE', "txn_type_id = '" . $txn_type_id . "'");
	}

	public function deleteTransactionType($txn_type_id)
	{
		if (empty($txn_type_id)) return false;
		return $this->db->query("DELETE FROM `actg_transaction_types` WHERE txn_type_id = '" . $txn_type_id . "' LIMIT 1");
	}

	public function getCreditNoteTransaction($param = array(), $opt = array())
	{
		return $this->db->getAll(
			"SELECT *, acnt.create_at as create_at, ecn.create_at as e_create_at FROM `actg_credit_note_transactions` acnt " .
			"LEFT JOIN `ecs_erp_credit_note` ecn ON ecn.cn_id = acnt.cn_id " .
			"LEFT JOIN `ecs_erp_supplier` es ON es.supplier_id = ecn.supplier_id " .
			"WHERE " . $this->build_where($param) .
			(isset($opt['orderby']) ? "ORDER BY " . $opt['orderby'] . " " : '') .
			(isset($opt['limit']) ? "LIMIT " . $opt['limit'] : ''));
	}

	public function idOrderSaRedeem($order_id){

		$sql = "SELECT order_type, type_id FROM " . $GLOBALS['ecs']->table('order_info') . " WHERE order_id = " . $order_id;

		$res = $this->db->getRow($sql);

		if (!empty($res) && $res['order_type'] == 'sa' && $res['type_id'] != ""){
			return TRUE;
		} else {
			return FALSE;
		}
	}
}

class AccountingHooks
{
	private $actg;
	private $db;

	public function __construct($actg, $db)
	{
		$this->actg = $actg;
		$this->db = $db;
	}

	public function orderPayPreprocess($order_id)
	{
		$sql = "SELECT p.`pay_code`, oi.order_amount " .
				"FROM " . $GLOBALS['ecs']->table('order_info') . " as oi " .
				"LEFT JOIN " . $GLOBALS['ecs']->table('payment') . " as p ON p.pay_id = oi.pay_id " .
				"WHERE oi.order_id = '" . $order_id . "' LIMIT 1";
		$order_info = $this->db->getRow($sql);
		$pay_code = $order_info['pay_code'];
		$pay_methods = $this->actg->getAccountPaymentMethods(array('pay_code' => $pay_code, 'active' => 1));
		return array(
			'show_actg_pay' => (count($pay_methods) > 1),
			'actg_payment_methods' => $pay_methods,
			'max_pay_amount' => $order_info['order_amount'],
		);
	}

	public function orderPaid($order_id, $amount, $remark = '', $payment_method_id = 0, $is_pos = false, $admin_id = 1)
	{
		$sql = "SELECT user_id, order_type, is_separate, platform, type_id, pay_id FROM " . $GLOBALS['ecs']->table('order_info') . " WHERE order_id = '" . $order_id . "' LIMIT 1";
		$userController = new Yoho\cms\Controller\UserController();
		$luckyDrawController = new Yoho\cms\Controller\LuckyDrawController();

		$order = $this->db->getRow($sql);
		// Exclude orders for the followiung user accounts: 11111111, 88888888, 99999999
		if (in_array($order['user_id'], $userController->getWholesaleUserIds()) || $order['order_type']==Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE) return true;

		$sa_offline_redeem = FALSE;
		if ($order['order_type'] == "sa" || $order['pay_id'] == "11"){
			$sa_offline_redeem = TRUE;
		}

		if (!$payment_method_id)
		{
			// Get ECShop payment method
			$sql = "SELECT p.`pay_code` " .
					"FROM " . $GLOBALS['ecs']->table('order_info') . "as oi " .
					"LEFT JOIN " . $GLOBALS['ecs']->table('payment') . " as p ON p.pay_id = oi.pay_id " .
					"WHERE oi.order_id = '" . $order_id . "' LIMIT 1";
			$pay_code = $this->db->getOne($sql);

			// Determinate account / payment type from ECShop payment method
			$pay_methods = $this->actg->getAccountPaymentMethods(array('pay_code' => $pay_code, 'active' => 1));

			if (count($pay_methods) == 1)
			{
				$pay_method = $pay_methods[0];
			} else if (count($pay_methods) > 1 && $is_pos == true) {
				// If is in pos pay, but have some error to setting the pay_methods. Default set the first pay_methods.
				$pay_method = $pay_methods[0];
			}
			// else none or multiple payment method available...
			else
			{
				$pay_method = false;
			}
		}
		else
		{
			$pay_method = $this->actg->getAccountPaymentMethod(compact('payment_method_id'));
		}

		// If payment method not find... skip this
		if (!$pay_method && !$sa_offline_redeem) return false;

		$order_payment_id = $this->actg->addOrderPayment(array(
			'order_id' => $order_id,
			'payment_date' => $this->actg->dbdate(),
			'currency_code' => (!empty($pay_method['currency_code']))?$pay_method['currency_code']:'HKD',
			'amount' => $amount,
			'payment_type_id' => $pay_method['payment_type_id'],
			'account_id' => $pay_method['account_id'],
			'remark' => $remark,
		), $admin_id);

		if ($order_payment_id)
		{
			$acc = $this->actg->getAccount(array('account_id' => $pay_method['account_id']));
			$sql = "UPDATE " . $GLOBALS['ecs']->table('order_info') .
					"SET pay_note = '" . $acc['account_name'] . "' " .
					"WHERE order_id = '" . $order_id . "'";
			$this->db->query($sql);
		}
		// Update order goods pre_sale date
		$orderController = new Yoho\cms\Controller\OrderController();
		$orderController->order_goods_planned_delivery_date($order_id);

		$luckyDrawController->luckyDrawOrderAwards($order_id);
		
		$orderController->orderAssignWarehouseId($order_id);
		
		$orderController->orderSetShipmentEstimatedTime($order_id);

		// only clear sold product
		$sql = 'SELECT goods_id  FROM ' . $GLOBALS['ecs']->table('order_goods') . " WHERE order_id = '".$order_id ."'";
		$all_order_goods = $GLOBALS['db']->getAll($sql);
		foreach ($all_order_goods as $order_goods) {
			clear_cache_files('goods_'.$order_goods['goods_id']);
		}

		// Auto order separate
		if($order['is_separate'] == 0)$orderController->auto_order_separate($order_id);

		return $order_payment_id;
	}

	public function orderUnpaid($order_id, $amount = null)
	{
		$transaction_started = false;
		if (!$this->db->in_transaction)
		{
			$this->actg->start_transaction();
			$transaction_started = true;
		}

		if (is_null($amount))
		{
			// Fully unpay, just remove all order payments
			$order_payments = $this->actg->getOrderPayments(compact('order_id'));
			foreach ($order_payments as $order_payment)
			{
				if (!$this->actg->deleteOrderPayment($order_payment['order_payment_id']))
				{
					if ($transaction_started)
					{
						$this->actg->rollback_transaction();
					}
					return false;
				}
			}
		}
		else
		{
			// Partial refund, get last payment info, and issue a negative amount order payment
			$order_payments = $this->actg->getOrderPayments(compact('order_id'), array('orderby' => 'payment_date DESC'));
			if (empty($order_payments)) return false;

			$order_payment = $order_payments[0];

			$order_payment_id = $this->actg->addOrderPayment(array(
				'order_id' => $order_id,
				'payment_date' => $this->actg->dbdate(),
				'currency_code' => $order_payment['currency_code'],
				'amount' => bcmul($amount, -1),
				'payment_type_id' => $order_payment['payment_type_id'],
				'account_id' => $order_payment['account_id'],
				'remark' => 'Partial refund',
			));

			if (!$order_payment_id)
			{
				if ($transaction_started)
				{
					$this->actg->rollback_transaction();
				}
				return false;
			}
		}

		if ($transaction_started)
		{
			$this->actg->commit_transaction();
		}
		return true;
	}

	public function orderRefunded($order_id, $amount = null, $payment_method_id = 0, $remark = '', $refund_id = 0)
	{
		$transaction_started = false;
		if (!$this->db->in_transaction)
		{
			$this->actg->start_transaction();
			$transaction_started = true;
		}

		if (is_null($amount))
		{
			// Full refund, obtain paid amount
			$sql = "SELECT money_paid FROM " . $GLOBALS['ecs']->table('order_info') . " WHERE `order_id` = '" . $order_id . "'";
			$amount = $this->db->getOne($sql);
			if (!$amount) return false;
		}

		if (!$payment_method_id)
		{
			// Get ECShop payment method
			$sql = "SELECT p.`pay_code` " .
					"FROM " . $GLOBALS['ecs']->table('order_info') . " as oi " .
					"LEFT JOIN " . $GLOBALS['ecs']->table('payment') . " as p ON p.pay_id = oi.pay_id " .
					"WHERE oi.order_id = '" . $order_id . "' LIMIT 1";
			$pay_code = $this->db->getOne($sql);

			// Determinate account / payment type from ECShop payment method
			$pay_methods = $this->actg->getAccountPaymentMethods(compact('pay_code'));

			if (count($pay_methods) == 1)
			{
				$pay_method = $pay_methods[0];
			}
			// else none or multiple payment method available...
			else
			{
				$pay_method = false;
			}
		}
		else
		{
			$pay_method = $this->actg->getAccountPaymentMethod(compact('payment_method_id'));
		}

		// If payment method not find... skip this
		if (!$pay_method) return false;

		$order_payment_id = $this->actg->addOrderPayment(array(
			'order_id' => $order_id,
			'payment_date' => $this->actg->dbdate(),
			'currency_code' => $pay_method['currency_code'],
			'amount' => bcmul($amount, -1),
			'payment_type_id' => $pay_method['payment_type_id'],
			'account_id' => $pay_method['account_id'],
			'remark' => $remark,
			'refund_id' => $refund_id
		));

		if (!$order_payment_id)
		{
			if ($transaction_started)
			{
				$this->actg->rollback_transaction();
			}
			return false;
		}

		if ($transaction_started)
		{
			$this->actg->commit_transaction();
		}
		return true;
	}

	public function getWholesaleOrderPaidAmounts($order_id)
	{
		$wholesalePayments = $this->actg->getWholesalePayments(compact('order_id'));
		$amounts = array();
		foreach ($wholesalePayments as $row)
		{
			// if (($row['complete_date']) && ($row['complete_date'] != '0000-00-00 00:00:00'))
			// {
				if (!isset($amounts[$row['currency_code']]))
				{
					$amounts[$row['currency_code']] = 0;
				}
				$amounts[$row['currency_code']] = bcadd($amounts[$row['currency_code']], $row['amount']);
			// }
		}
		return $amounts;
	}

	public function getPurchaseOrderPaidAmounts($order_id)
	{
		$purchasePayments = $this->actg->getPurchasePayments(compact('order_id'));
		$amounts = array();
		foreach ($purchasePayments as $row)
		{
			if (empty($row['amount']) && empty($row['cn_amount'])) {
				continue;
			}
			// if (($row['complete_date']) && ($row['complete_date'] != '0000-00-00 00:00:00'))
			// {
				if (!isset($amounts[$row['currency_code']]))
				{
					$amounts[$row['currency_code']] = 0;
				}
				$amounts[$row['currency_code']] = bcadd($amounts[$row['currency_code']], $row['amount'] + $row['cn_amount']);
			// }
		}
		return $amounts;
	}

	public function calculateTotalPaidAmount($amounts)
	{
		if (empty($amounts))
		{
			return 0;
		}

		$currency_rates = array();
		foreach ($this->actg->getCurrencies() as $row)
		{
			$currency_rates[$row['currency_code']] = $row['currency_rate'];
		}
		$total = array_reduce(array_map(function ($amount, $currency_code) use ($currency_rates) {
			return bcmul($amount, $currency_rates[$currency_code]);
		}, $amounts, array_keys($amounts)), 'bcadd', 0);
		return $total;
	}

	/**
	 * Convert amount from a currency to another currency
	 *
	 * @param $from_amount decimal
	 * @param $from_currency string currency_code or array with currency_rate element
	 * @param $to_currency string currency_code or array with currency_rate element
	 * @return decimal
	 **/
	public function convertCurrency($from_amount, $from_currency, $to_currency)
	{
		$from_curr = is_array($from_currency) ? $from_currency : $this->actg->getCurrency(array('currency_code' => $from_currency));
		if (empty($from_curr['currency_rate']))
		{
			return false;
		}
		$to_curr = is_array($to_currency) ? $to_currency : $this->actg->getCurrency(array('currency_code' => $to_currency));
		if (empty($to_curr['currency_rate']))
		{
			return false;
		}

		return bcdiv(bcmul($from_amount, $from_curr['currency_rate']), $to_curr['currency_rate']);
	}

	private function updatePayLogCurrency($pay_log, $currency)
	{
		$currency = is_array($currency) ? $currency : $this->actg->getCurrency(array('currency_code' => $currency));
		if ((empty($currency['currency_code'])) ||
			(empty($currency['currency_rate'])))
		{
			return false;
		}

		// Calculate amount in new currency
		$pay_log['order_amount'] = $this->convertCurrency($pay_log['order_amount'], $pay_log['currency_code'], $currency);
		$pay_log['order_amount'] = round($pay_log['order_amount'], 2);

		// Update currency code in pay_log
		$pay_log['currency_code'] = $currency['currency_code'];

		// Save to database
		$sql = "UPDATE " . $GLOBALS['ecs']->table('pay_log') .
				"SET `order_amount` = '" . $pay_log['order_amount'] . "', `currency_code` = '" . $pay_log['currency_code'] . "'" .
				"WHERE log_id = '" . $pay_log['log_id'] . "'";
		$this->db->query($sql);

		// Return updated pay_log
		return $pay_log;
	}

	public function paymentModuleGetPayLog($pay_code, $log_id, $preferred_currency = null)
	{
		if (!empty($preferred_currency))
		{
			$preferred_currency = is_array($preferred_currency) ? $preferred_currency : $this->actg->getCurrency(array('currency_code' => $preferred_currency));
			if ((empty($preferred_currency['currency_code'])) ||
				(empty($preferred_currency['currency_rate'])) ||
				(empty($preferred_currency['currency_format'])))
			{
				$preferred_currency = null;
			}
		}

		$sql = "SELECT * FROM " . $GLOBALS['ecs']->table('pay_log') . " WHERE log_id = '" . $log_id . "'";
		$pay_log = $this->db->getRow($sql);

		// Pay_log not found!?
		if (empty($pay_log))
		{
			return $pay_log;
		}

		// Get allowed currencies for this payment method
		$currencies = $this->actg->getPaymentCurrencies(compact('pay_code'));

		// No Currency restriction defined, assume all currencies allowed
		if (empty($currencies))
		{
			// If $preferred_currency is provided
			if (!empty($preferred_currency))
			{
				// If pay_log currency didn't match $preferred_currency, update it
				if ($preferred_currency['currency_code'] != $pay_log['currency_code'])
				{
					$pay_log = $this->updatePayLogCurrency($pay_log, $preferred_currency);
				}
			}
			return $pay_log;
		}

		// If $preferred_currency is provided...
		if (!empty($preferred_currency))
		{
			// If $preferred_currency is allowed...
			foreach ($currencies as $currency)
			{
				if ($currency['currency_code'] == $preferred_currency['currency_code'])
				{
					// If pay_log currency didn't match $preferred_currency, update it
					if ($preferred_currency['currency_code'] != $pay_log['currency_code'])
					{
						$pay_log = $this->updatePayLogCurrency($pay_log, $preferred_currency);
					}
					return $pay_log;
				}
			}
		}

		// Is the pay_log currency allowed?
		foreach ($currencies as $currency)
		{
			if ($currency['currency_code'] == $pay_log['currency_code'])
			{
				return $pay_log;
			}
		}

		// Find default currency for this payment method
		$currency = null;
		foreach ($currencies as $curr)
		{
			if ($curr['default_currency'])
			{
				$currency = $curr;
			}
		}

		// Error: No default currency defined
		if (!$currency)
		{
			return false;
		}

		// Update pay_log with default currency, and calculate new amount
		$pay_log = $this->updatePayLogCurrency($pay_log, $currency);

		return $pay_log;
	}

	public function getCurrencyForPaymentModule($pay_code, $preferred_currency = null)
	{
		if (!empty($preferred_currency))
		{
			$preferred_currency = is_array($preferred_currency) ? $preferred_currency : $this->actg->getCurrency(array('currency_code' => $preferred_currency));
			if ((empty($preferred_currency['currency_code'])) ||
				(empty($preferred_currency['currency_rate'])) ||
				(empty($preferred_currency['currency_format'])))
			{
				$preferred_currency = null;
			}
		}

		$default_currency = $this->actg->getCurrency(array('is_default' => 1));

		// Get allowed currencies for this payment method
		$currencies = $this->actg->getPaymentCurrencies(compact('pay_code'));

		// No Currency restriction defined, assume all currencies allowed
		if (empty($currencies))
		{
			if (empty($preferred_currency))
			{
				return $default_currency;
			}
			else
			{
				return $preferred_currency;
			}
		}

		if (!empty($preferred_currency))
		{
			// If $preferred_currency is allowed, use it
			foreach ($currencies as $curr)
			{
				if ($curr['currency_code'] == $preferred_currency['currency_code'])
				{
					return $preferred_currency;
				}
			}
		}

		// Find default currency for this payment method
		foreach ($currencies as $curr)
		{
			if ($curr['default_currency'])
			{
				return $curr;
			}
		}

		// Error: No default currency defined
		return $default_currency;
	}

	public function getPaymentAmountInTargetCurrency($order_amount, $pay_code, $preferred_currency = null)
	{
		$currency = $this->getCurrencyForPaymentModule($pay_code, $preferred_currency);
		$order_amount = bcdiv($order_amount, $currency['currency_rate']);
		return $this->actg->money_format($order_amount, $currency['currency_format']);
	}

	public function getOrderPaymentAmount($order, $preferred_currency = null)
	{
		// If order is invalid, return 0 with default currency format
		if (empty($order) || empty($order['order_id']))
		{
			$default_currency = $this->actg->getCurrency(array('is_default' => 1));
			return $this->actg->money_format(0, $default_currency['currency_format']);
		}

		// Get order pay_code
		$sql = "SELECT p.`pay_code` " .
				"FROM " . $GLOBALS['ecs']->table('order_info') . " as oi " .
				"LEFT JOIN " . $GLOBALS['ecs']->table('payment') . " as p ON p.pay_id = oi.pay_id " .
				"WHERE oi.order_id = '" . $order['order_id'] . "' LIMIT 1";
		$pay_code = $this->db->getOne($sql);

		return $this->getPaymentAmountInTargetCurrency($order['order_amount'], $pay_code, $preferred_currency);
	}

	public function getTransactionTypesTree()
	{
		$res = $this->actg->getTransactionTypes();
		$types = array();
		foreach ($res as $row)
		{
			if (is_null($row['txn_type_parent']))
			{
				if (!isset($types[$row['txn_type_id']]))
				{
					$types[$row['txn_type_id']] = array();
					$types[$row['txn_type_id']]['txn_type_id'] = $row['txn_type_id'];
					$types[$row['txn_type_id']]['txn_type_name'] = $row['txn_type_name'];
					$types[$row['txn_type_id']]['children'] = array();
				}
			}
			else
			{
				if (!isset($types[$row['txn_type_parent']]))
				{
					$types[$row['txn_type_parent']] = array();
					$types[$row['txn_type_parent']]['txn_type_id'] = $row['txn_type_parent'];
					$types[$row['txn_type_parent']]['txn_type_name'] = $row['txn_type_parent_name'];
					$types[$row['txn_type_parent']]['children'] = array();
				}

				$types[$row['txn_type_parent']]['children'][$row['txn_type_id']] = array();
				$types[$row['txn_type_parent']]['children'][$row['txn_type_id']]['txn_type_id'] = $row['txn_type_id'];
				$types[$row['txn_type_parent']]['children'][$row['txn_type_id']]['txn_type_name'] = $row['txn_type_name'];
			}
		}

		return $types;
	}

}

$actg = new Accounting($db);
$actgHooks = new AccountingHooks($actg, $db);

?>
