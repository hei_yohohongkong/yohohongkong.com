<?php

require_once(dirname(__FILE__) .'/lib_erp_common.php');
function get_agency_suppliers($agency_id=0)
{
	if($agency_id >0)
	{
		$sql="select s.supplier_id from ".$GLOBALS['ecs']->table('erp_supplier')." as s,".
		$GLOBALS['ecs']->table('erp_supplier_group')." as sg,".
		$GLOBALS['ecs']->table('erp_supplier_group_admin')." as sga,".
		$GLOBALS['ecs']->table('admin_user')." as u ".
		"where u.agency_id='".$agency_id."' and u.user_id=sga.admin_id and sga.group_id=sg.group_id and sg.group_id=s.supplier_group_id ".
		" and sg.is_valid='1' and s.is_valid='1'";
		$s1=$GLOBALS['db']->getCol($sql);
		$sql="select s.supplier_id from ".$GLOBALS['ecs']->table('erp_supplier')." as s,".
		$GLOBALS['ecs']->table('erp_supplier_admin')." as sa,".
		$GLOBALS['ecs']->table('admin_user')." as u ".
		"where u.agency_id='".$agency_id."' and u.user_id=sa.admin_id and sa.supplier_id=s.supplier_id ".
		" and s.is_valid='1'";
		$s2=$GLOBALS['db']->getCol($sql);
		$s=array_unique(array_merge($s1,$s2));
		if(!empty($s))
		{
			$sql="select s.supplier_id,s.name from ".$GLOBALS['ecs']->table('erp_supplier')." as s where s.supplier_id in (".implode(',',$s).")";
			return $GLOBALS['db']->getAll($sql);
		}
		return array();
	}
	else
	{
		$sql="select s.supplier_id,s.name from ".$GLOBALS['ecs']->table('erp_supplier')." as s where s.is_valid='1'";
		return $GLOBALS['db']->getAll($sql);
	}
}
function supplier_group_count()
{
	$sql="select count(*) from ".$GLOBALS['ecs']->table('erp_supplier_group')." where 1";
	return $GLOBALS['db']->getOne($sql);
}
function supplier_group_list($start=-1,$num=-1)
{
	$sql="select * from ".$GLOBALS['ecs']->table('erp_supplier_group')." where 1 order by group_id desc";
	if($start!=-1 &&$num!=-1)
	{
		if($start<=0)
		{
			$start=0;
		}
		$sql.=" limit ".$start.",".$num;
	}
	$group_list=$GLOBALS['db']->getAll($sql);
	foreach($group_list as $key =>$group)
	{
		$sql="select count(*) from ".$GLOBALS['ecs']->table('erp_supplier')." where supplier_group_id='".$group['group_id']."'";
		$group_list[$key]['count']=$GLOBALS['db']->getOne($sql);
	}
	return $group_list;
}
function supplier_group_info($id)
{
	$sql="select * from ".$GLOBALS['ecs']->table('erp_supplier_group')." where group_id='".$id."'";
	return $GLOBALS['db']->getRow($sql);
}
function delete_supplier_group($id)
{
	$sql="select count(*) as num from ".$GLOBALS['ecs']->table('erp_supplier')." where supplier_group_id='".$id."'";
	$num=$GLOBALS['db']->getOne($sql);
	if(!empty($num))
	{
		return false;
	}
	else
	{
		$sql="delete from ".$GLOBALS['ecs']->table('erp_supplier_group')." where group_id='".$id."' limit 1";
		$GLOBALS['db']->query($sql);
		return true;
	}
}
function get_admin_supplier($admin_id=0, $secondary = false)
{
	// if($admin_id >0)
	// {
	// 	$sql="select su.*,sg.group_name from ".$GLOBALS['ecs']->table('erp_supplier')." as su,".$GLOBALS['ecs']->table('erp_supplier_group')." as sg,".$GLOBALS['ecs']->table('erp_supplier_group_admin')." as sga ";
	// 	$sql.=" where sga.admin_id='".$admin_id."' and su.supplier_group_id=sg.group_id and sg.group_id=sga.group_id and su.is_valid='1' and sg.is_valid='1'";
	// 	$supplier_info1=$GLOBALS['db']->getAll($sql);
	// 	$sql="select su.*,sg.group_name from ".$GLOBALS['ecs']->table('erp_supplier')." as su,".$GLOBALS['ecs']->table('erp_supplier_group')." as sg, ".$GLOBALS['ecs']->table('erp_supplier_admin')." as sa ";
	// 	$sql.=" where sa.admin_id='".$admin_id."' and su.supplier_group_id=sg.group_id and su.supplier_id=sa.supplier_id  and su.is_valid='1' and sg.is_valid='1'";
	// 	$supplier_info2=$GLOBALS['db']->getAll($sql);
	// 	$supplier_id_array=array();
	// 	$supplier_info=array();
	// 	if(!empty($supplier_info1))
	// 	{
	// 		foreach($supplier_info1 as $key =>$item)
	// 		{
	// 			$supplier_id=$item['supplier_id'];
	// 			if(!in_array($supplier_id,$supplier_id_array))
	// 			{
	// 				array_push($supplier_info,$supplier_info1[$key]);
	// 				array_push($supplier_id_array,$supplier_id);
	// 			}
	// 		}
	// 	}
	// 	if(!empty($supplier_info2))
	// 	{
	// 		foreach($supplier_info2 as $key =>$item)
	// 		{
	// 			$supplier_id=$item['supplier_id'];
	// 			if(!in_array($supplier_id,$supplier_id_array))
	// 			{
	// 				array_push($supplier_info,$supplier_info2[$key]);
	// 				array_push($supplier_id_array,$supplier_id);
	// 			}
	// 		}
	// 	}
	// }
	// else
	// {
	// 	$sql="select su.*,sg.group_name from ".$GLOBALS['ecs']->table('erp_supplier')." as su,".$GLOBALS['ecs']->table('erp_supplier_group')." as sg ";
	// 	$sql.=" where su.supplier_group_id=sg.group_id and su.is_valid='1' and sg.is_valid='1'";
	// 	$supplier_info=$GLOBALS['db']->getAll($sql);
	// }
	
	// Rewritten on 2015-10-06 with cleaner logic and better readibility
	if($admin_id > 0){
		if(check_authz(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ONLYSELFSUPPLIERS) && ((!$_SESSION['manage_cost'] && !$_SESSION['is_accountant']))){
			//only self suppliers
			$sql = "SELECT su.*, sg.group_name " .
			"FROM " . $GLOBALS['ecs']->table('erp_supplier') . " as su " .
			"LEFT JOIN " . $GLOBALS['ecs']->table('erp_supplier_admin')." as sa " .
				"ON su.supplier_id = sa.supplier_id " .
			($secondary ? "LEFT JOIN " . $GLOBALS['ecs']->table('erp_supplier_secondary_admin')." as ssa " .
				"ON su.supplier_id = ssa.supplier_id " : "") .
			"LEFT JOIN " . $GLOBALS['ecs']->table('erp_supplier_group') . " as sg " .
				"ON su.supplier_group_id = sg.group_id AND sg.is_valid = '1' " .
			"LEFT JOIN " . $GLOBALS['ecs']->table('erp_supplier_group_admin') . " as sga " .
				"ON sg.group_id = sga.group_id " .
			"WHERE su.is_valid = '1' " .
			"AND (sga.admin_id = '" . $admin_id . "' OR sa.admin_id = '" . $admin_id . "' ".($secondary ? " OR ssa.admin_id = '" . $admin_id . "'" : "").") " .
			"GROUP BY su.supplier_id";
			$supplier_info = $GLOBALS['db']->getAll($sql);
		}elseif(($_SESSION['manage_cost'] || $_SESSION['is_accountant'])){
			//super admin
			$sql = "SELECT su.*, sg.group_name " .
			"FROM " . $GLOBALS['ecs']->table('erp_supplier') . " as su " .
			"LEFT JOIN " . $GLOBALS['ecs']->table('erp_supplier_group') . " as sg " .
				"ON su.supplier_group_id = sg.group_id " .
			"WHERE su.is_valid = '1' AND sg.is_valid = '1' ";
			$supplier_info = $GLOBALS['db']->getAll($sql);
			
		}
	}
	usort($supplier_info, function ($supplier1, $supplier2) {
		return strcasecmp($supplier1['name'], $supplier2['name']);
	});
	return $supplier_info;
}
function supplier_count($paras=array())
{
	$where=' where 1 ';
	if(isset($paras['group_id']) &&intval($paras['group_id'])>0)
	{
		$where.=" and supplier_group_id='".intval($paras['group_id'])."'";
	}
	if(isset($paras['keyword']) &&trim($paras['keyword'])!='')
	{
		$keyword=trim($paras['keyword']);
		$where.=" and (code like '%".$keyword."%' or name like '%".$keyword."%' or address like '%".$keyword."%' or contact like '%".$keyword."%' or tel like '%".$keyword."%' or fax like '%".$keyword."%')";
	}
	if(!empty($paras['admin_id']))
	{
		$where.=" and esa.admin_id='".$paras['admin_id']."'";
	}
	$sql="select count(DISTINCT(es.supplier_id)) from ".$GLOBALS['ecs']->table('erp_supplier')." as es ".
	"left join ".$GLOBALS['ecs']->table('erp_supplier_admin')." as esa ON esa.supplier_id = es.supplier_id "."$where";

	return $GLOBALS['db']->getOne($sql);
}
function supplier_list($paras=array(),$start=-1,$num=-1)
{
	global $_CFG, $_LANG;
	require_once(ROOT_PATH . 'languages/' . $_CFG['lang'] . '/admin/ERP.php');
	$where=' where 1 ';
	if(isset($paras['group_id']) &&intval($paras['group_id'])>0)
	{
		$where.=" and supplier_group_id='".intval($paras['group_id'])."'";
	}
	if(isset($paras['is_valid']) &&intval($paras['is_valid'])>=0)
	{
		$where.=" and is_valid='".intval($paras['is_valid'])."'";
	}
	if(isset($paras['keyword']) &&trim($paras['keyword'])!='')
	{
		$keyword=trim($paras['keyword']);
		$where.=" and (code like '%".$keyword."%' or name like '%".$keyword."%' or address like '%".$keyword."%' or contact like '%".$keyword."%' or tel like '%".$keyword."%' or fax like '%".$keyword."%')";
	}
	if(!empty($paras['admin_id']))
	{
		$where.=" and esa.admin_id='".$paras['admin_id']."'";
	}
	$where.=" GROUP BY supplier_id order by supplier_id desc ";
	if($start!=-1 &&$num!=-1)
	{
		if($start<=0)
		{
			$start=0;
		}
		$where.=" limit ".$start.",".$num;
	}
	$sql="select es.* from ".$GLOBALS['ecs']->table('erp_supplier')." as es ".
	"left join ".$GLOBALS['ecs']->table('erp_supplier_admin')." as esa ON esa.supplier_id = es.supplier_id "."$where";

	$supplier_info=$GLOBALS['db']->getAll($sql);
	$corresponding_company_list = get_corresponding_company_list();
	if(!empty($supplier_info))
	{
		foreach($supplier_info as $key=>$item)
		{
			$corresponding_company_id = array_search($item['corresponding_company_id'], array_column($corresponding_company_list, 'corresponding_company_id'));

			$sql="select group_name from ".$GLOBALS['ecs']->table('erp_supplier_group')." where group_id='".$item['supplier_group_id']."'";
			$supplier_info[$key]['supplier_group_name']=$GLOBALS['db']->getOne($sql);
			$sql="select count(*) from ".$GLOBALS['ecs']->table('erp_goods_supplier')." where supplier_id='".$item['supplier_id']."'";
			$supplier_info[$key]['goods_count']=$GLOBALS['db']->getOne($sql);
			$sql="select count(*) from ".$GLOBALS['ecs']->table('erp_order')." where supplier_id='".$item['supplier_id']."'";
			$supplier_info[$key]['order_count']=$GLOBALS['db']->getOne($sql);
			$supplier_info[$key]['converted_payment_term'] = $_LANG['erp_supplier_payment_term_'.$supplier_info[$key]['payment_term']];
			$supplier_info[$key]['corresponding_company'] = (strlen($corresponding_company_id) > 0) ? $corresponding_company_list[$corresponding_company_id]['company_code'] : '';
		}
	}
	return 	$supplier_info;
}
function supplier_info($id)
{
	$sql="select * from ".$GLOBALS['ecs']->table('erp_supplier')." where supplier_id='".$id."'";
	return $GLOBALS['db']->getRow($sql);
}
function suppliers_list_name($supplier_ids=array())
{
	$result=array();
	$sql="select supplier_id,name from ".$GLOBALS['ecs']->table('erp_supplier')." where is_valid='1'";
	$res=$GLOBALS['db']->query($sql);
	while($row=mysql_fetch_assoc($res))
	{
		if(!empty($supplier_ids))
		{
			if(in_array($row['supplier_id'],$supplier_ids))
			{
				$row['selected']=1;
			}
			else
			{
				$row['selected']=0;
			}
		}
		else
		{
			$row['selected']=0;
		}
		$result[$row['supplier_id']]=$row;
	}
	uasort($result, function ($a, $b) {
		return strcasecmp($a['name'],$b['name']);
	});
	return $result;
}
function suppliers_list_name_by_admin($supplier_ids=array())
{
	$result=array();
	$res = array_map(function ($supplier_info) {
        return array(
            'supplier_id' => $supplier_info['supplier_id'],
            'name' => $supplier_info['name']
        );
    }, get_admin_supplier($_SESSION['admin_id'], 1));
	uasort($res, function ($a, $b) {
		return strcasecmp($a['name'],$b['name']);
	});
	foreach ($res as $row)
	{
		if(!empty($supplier_ids))
		{
			if(in_array($row['supplier_id'],$supplier_ids))
			{
				$row['selected']=1;
			}
			else
			{
				$row['selected']=0;
			}
		}
		else
		{
			$row['selected']=0;
		}
		$result[$row['supplier_id']]=$row;
	}
	return $result;
}
function gen_supplier_code()
{
	$sql="select code from ".$GLOBALS['ecs']->table('erp_supplier')." order by code desc limit 1";
	$result=$GLOBALS['db']->getOne($sql);
	$result=intval($result)+1;
	return format_code($result);
}
function is_supplier_exist($supplier_id='',$code='')
{
	if(empty($supplier_id) &&empty($code))
	{
		return false;
	}
	elseif(!empty($supplier_id) &&empty($code))
	{
		$sql="select * from ".$GLOBALS['ecs']->table('erp_supplier')." where supplier_id='".$supplier_id."'";
	}
	elseif(empty($supplier_id) &&!empty($code))
	{
		$sql="select * from ".$GLOBALS['ecs']->table('erp_supplier')." where code ='".$code."'";
	}
	$result=$GLOBALS['db']->getRow($sql);
	return !empty($result)?$result : false;
}
function is_supplier_admin($supplier_id, $admin_id=0)
{
	$sql = "SELECT '1' " .
		"FROM " . $GLOBALS['ecs']->table('erp_supplier') . " as su " .
		"LEFT JOIN " . $GLOBALS['ecs']->table('erp_supplier_admin')." as sa " .
			"ON su.supplier_id = sa.supplier_id " .
		"LEFT JOIN " . $GLOBALS['ecs']->table('erp_supplier_group') . " as sg " .
			"ON su.supplier_group_id = sg.group_id AND sg.is_valid = '1' " .
		"LEFT JOIN " . $GLOBALS['ecs']->table('erp_supplier_group_admin') . " as sga " .
			"ON sg.group_id = sga.group_id " .
		"WHERE su.is_valid = '1' AND su.supplier_id = '" . $supplier_id . "' " .
		"AND (sga.admin_id = '" . $admin_id . "' OR sa.admin_id = '".$admin_id."') " .
		"GROUP BY su.supplier_id";
	$res = $GLOBALS['db']->getOne($sql);
	return !empty($res) ? true : false;
}
function is_supplier_secondary_admin($supplier_id, $admin_id=0)
{
	$sql = "SELECT '1' " .
		"FROM " . $GLOBALS['ecs']->table('erp_supplier') . " as su " .
		"LEFT JOIN " . $GLOBALS['ecs']->table('erp_supplier_secondary_admin')." as sa " .
			"ON su.supplier_id = sa.supplier_id " .
		"LEFT JOIN " . $GLOBALS['ecs']->table('erp_supplier_group') . " as sg " .
			"ON su.supplier_group_id = sg.group_id AND sg.is_valid = '1' " .
		"LEFT JOIN " . $GLOBALS['ecs']->table('erp_supplier_group_admin') . " as sga " .
			"ON sg.group_id = sga.group_id " .
		"WHERE su.is_valid = '1' AND su.supplier_id = '" . $supplier_id . "' " .
		"AND (sga.admin_id = '" . $admin_id . "' OR sa.admin_id = '".$admin_id."') " .
		"GROUP BY su.supplier_id";
	$res = $GLOBALS['db']->getOne($sql);
	return !empty($res) ? true : false;
}
/**
 * Return goods_id from selected supplier
 * @param array $supplier_ids List of supplier_id
 * @return array List of goods_id
*/
function get_supplier_goods_ids($supplier_ids=array()){
	$sql = "SELECT goods_id FROM " .  $GLOBALS['ecs']->table('erp_goods_supplier') .
				" WHERE supplier_id IN (" . implode(",",$supplier_ids) . ")";
	return $GLOBALS['db']->getCol($sql);
}
/**
 * Return supplier_id of self supplier
 * @return array List of supplier_id
*/
function get_self_supplier_id($secondary = false){
    $self_supplier_info = get_admin_supplier($_SESSION['admin_id'], $secondary);
    $self_supplier_id = [];
    foreach($self_supplier_info as $self_supplier){
        $self_supplier_id[] = $self_supplier['supplier_id'];
    }
    return $self_supplier_id;
}
function get_corresponding_company_list()
{
	$controller = new Yoho\cms\Controller\SupplierController();
	return $controller->getCorrespondingCompanyList();
}
?>
