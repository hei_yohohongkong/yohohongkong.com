<?php

if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}
function brand_list()
{
	$sql="select brand_id,brand_name from ".$GLOBALS['ecs']->table('brand')." order by brand_name asc";
	return $GLOBALS['db']->getAll($sql);
}
function add_stock($delivery_id,$order_id,$warehousing_style_id=2)
{
	global $db,$_LANG;
	
	$packageController = new Yoho\cms\Controller\PackageController();

	if ($delivery_id > 0)
	{
		$sql="select rec_id,goods_id,goods_name,send_number,goods_attr_id,goods_price,order_item_id,is_real from ".$GLOBALS['ecs']->table('delivery_goods')."  where delivery_id='".$delivery_id."'";
		// $res=$db->query($sql);
		// while($row=mysql_fetch_assoc($res))
		$res = $db->getAll($sql);
	}
	else
	{
		// hacked by howang: return stock without 發貨單
		
		// First, check if the order have associated ERP出庫單
		$sql = "SELECT delivery_id, status FROM " . $GLOBALS['ecs']->table('erp_delivery') . " WHERE delivery_style_id IN (1,3) AND order_id = '$order_id'";
		$erp_deliveries = $db->getAll($sql);
		
		// The order have no associated ERP出庫單, we don't need to return stock
		if (empty($erp_deliveries))
		{
			return true;
		}

		// Two cases to be handled here:
		// 1. There are deliveries not approved yet, just delete the delivery record
		$need_add_stock = true;
		foreach ($erp_deliveries as $erp_delivery)
		{
			if ($erp_delivery['status'] == 1 || $erp_delivery['status'] == 2)
			{
				$sql="delete from ".$GLOBALS['ecs']->table('erp_delivery_item')." where delivery_id='".$erp_delivery['delivery_id']."'";
				$db->query($sql);
				$sql="delete from ".$GLOBALS['ecs']->table('erp_delivery')." where delivery_id='".$erp_delivery['delivery_id']."'";
				$db->query($sql);
				
				$need_add_stock = false;
			}
		}
		if (!$need_add_stock)
		{
			return true;
		}
		
		// 2. All deliveries are approved, need to return stock
		$goods_list = array();
		
		// Get list of goods in the order
		$sql = "SELECT o.*, o.goods_attr, IFNULL(b.brand_name, '') AS brand_name " .
				"FROM " . $GLOBALS['ecs']->table('order_goods') . " AS o ".
				"LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " AS g ON o.goods_id = g.goods_id " .
				"LEFT JOIN " . $GLOBALS['ecs']->table('brand') . " AS b ON g.brand_id = b.brand_id " .
				"WHERE o.order_id = '$order_id' ";    
		$res = $GLOBALS['db']->query($sql);
		
		$sql="select agency_id from ".$GLOBALS['ecs']->table('order_info')." where order_id='".$order_id."'";
		$agency_id=$GLOBALS['db']->getOne($sql);
		
		while ($row = $GLOBALS['db']->fetchRow($res))
		{
			// 虚拟商品支持
			if ($row['is_real'] == 0)
			{
				/* 取得语言项 */
				$filename = ROOT_PATH . 'plugins/' . $row['extension_code'] . '/languages/common_' . $GLOBALS['_CFG']['lang'] . '.php';
				if (file_exists($filename))
				{
					include_once($filename);
					if (!empty($GLOBALS['_LANG'][$row['extension_code'].'_link']))
					{
						$row['goods_name'] = $row['goods_name'] . sprintf($GLOBALS['_LANG'][$row['extension_code'].'_link'], $row['goods_id'], $order['order_sn']);
					}
				}
			}
		
			$row['formated_subtotal']       = price_format($row['goods_price'] * $row['goods_number']);
			$row['formated_goods_price']    = price_format($row['goods_price']);
		
			if ($row['extension_code'] == 'package_buy')
			{
				$row['brand_name'] = '';
				$row['package_goods_list'] = $packageController->get_package_goods_list($row['goods_id']);
			}
		
			//处理货品id
			$row['product_id'] = empty($row['product_id']) ? 0 : $row['product_id'];
		
			$goods_list[] = $row;
		}
		
		// Simulate the result set from querying the delivery_goods table
		$res = array();
		if (!empty($goods_list))
		{
			foreach ($goods_list as $value)
			{
				// 商品（实货）（虚货）
				if (empty($value['extension_code']) || $value['extension_code'] == 'virtual_card')
				{
					$delivery_goods = array(
						'rec_id' => $delivery_id,
						'goods_id' => $value['goods_id'],
						'product_id' => $value['product_id'],
						'product_sn' => $value['product_sn'],
						'goods_id' => $value['goods_id'],
						'goods_name' => $value['goods_name'],
						'brand_name' => $value['brand_name'],
						'goods_sn' => $value['goods_sn'],
						'send_number' => $value['goods_number'],
						'parent_id' => 0,
						'is_real' => $value['is_real'],
						'goods_attr' => $value['goods_attr'],
						'goods_attr_id' => $value['goods_attr_id'],
						'goods_price' => $value['goods_price'],
						'order_item_id' => $value['rec_id']
					);
					$res[] = $delivery_goods;
				}
				// 商品（超值礼包）
				elseif ($value['extension_code'] == 'package_buy')
				{
					foreach ($value['package_goods_list'] as $pg_key => $pg_value)
					{
						$delivery_pg_goods = array(
							'rec_id' => $delivery_id,
							'goods_id' => $pg_value['goods_id'],
							'product_id' => $pg_value['product_id'],
							'product_sn' => $pg_value['product_sn'],
							'goods_name' => $pg_value['goods_name'],
							'brand_name' => '',
							'goods_sn' => $pg_value['goods_sn'],
							'send_number' => $pg_value['goods_number'],
							'parent_id' => $value['goods_id'], // 礼包ID
							'extension_code' => $value['extension_code'], // 礼包
							'is_real' => $pg_value['is_real']
						);
						$res[] = $delivery_pg_goods;
					}
				}
			}
		}
	}
	
	foreach ($res as $row)
	{
		$attr_id=get_attr_id($row['goods_id'],$row['goods_attr_id']);
		$sql="select ed.warehouse_id from ".$GLOBALS['ecs']->table('erp_delivery')." as ed,".$GLOBALS['ecs']->table('erp_delivery_item')." as edi ".
		" where ed.delivery_id=edi.delivery_id and edi.sys_delivery_item_id='".$row['rec_id']."'";
		$warehouse_id=$db->getOne($sql);

		if(empty($delivery_id) && $order_id > 0 && $row['rec_id'] == 0)
		{
			$sql="select ed.warehouse_id from ".$GLOBALS['ecs']->table('erp_delivery')." as ed,".$GLOBALS['ecs']->table('erp_delivery_item')." as edi ".
		" where ed.delivery_id=edi.delivery_id and edi.sys_delivery_item_id='".$row['rec_id']."' and ed.order_id = ".$order_id." and edi.goods_id = ".$row['goods_id']."";
			$warehouse_id=$db->getOne($sql);	
		}

		if(empty($warehouse_id))
		{
			// $db->query('ROLLBACK');
			// return false;

			$sql="select shipping_id from ".$GLOBALS['ecs']->table('order_info')." where order_id='".$order_id."'";
			$shipping_id=$GLOBALS['db']->getOne($sql);

			$erpController=new Yoho\cms\Controller\ErpController();

			if ($shipping_id == 2 || $shipping_id == 5) {  // 門市購買 or 觀塘門店自取
				$shop_warehouse_id = $erpController->getShopWarehouseId();
				$warehouse_id = $shop_warehouse_id;
			} else {
				$main_warehouse_id = $erpController->getMainWarehouseId();
				$warehouse_id = $main_warehouse_id;
			}
		}

		$warehousing_id=add_warehousing();
		if($warehousing_id >0)
		{
			$sql="update ".$GLOBALS['ecs']->table('erp_warehousing')." set warehousing_from='".$_SESSION['admin_name'].
			"',warehousing_style_id ='".$warehousing_style_id.
			"',warehouse_id ='".$warehouse_id.
			"',order_id='".$order_id.
			"',post_by='".$_SESSION['admin_id'].
			"',post_time='".gmtime().
			"',approved_by='".$_SESSION['admin_id'].
			"',approve_time='".gmtime().
			"',last_act_time='".gmtime().
			"',approve_remark ='".$_LANG['erp_warehousing_auto_on_cancel_ship'].
			"',status='3' where warehousing_id='".$warehousing_id."'";
			if(!$db->query($sql))
			{
				$db->query('ROLLBACK');
				return false;
			}
			$sql="insert into ".$GLOBALS['ecs']->table('erp_warehousing_item')." set warehousing_id='".$warehousing_id.
			"',goods_id='".$row['goods_id'].
			"',attr_id='".$attr_id.
			"',expected_qty='".$row['send_number'].
			"',received_qty='".$row['send_number'].
			"',price='".$row['goods_price'].
			"',account_payable='".($row['goods_price']*$row['send_number']).
			"',sys_delivery_item_id ='".$row['rec_id'].
			"',order_item_id=".$row['order_item_id']."";
			if(!$db->query($sql))
			{
				$db->query('ROLLBACK');
				return false;
			}
		}
		else
		{
			$db->query('ROLLBACK');
			return false;
		}
		if(!add_stock_by_warehousing($warehousing_id))
		{
			$db->query('ROLLBACK');
			return false;
		}
	}
	return true;
}
function add_stock_by_warehousing($warehousing_id)
{
	global $db,$ecs;
	$erpController=new Yoho\cms\Controller\ErpController();

	$sql="select * from ".$GLOBALS['ecs']->table('erp_warehousing')." where warehousing_id='".$warehousing_id."'";
	$warehousing_info=$db->getRow($sql);
	$warehousing_style_id=$warehousing_info['warehousing_style_id'];
	$warehouse_id=$warehousing_info['warehouse_id'];
	$order_id=$warehousing_info['order_id'];
	$warehousing_sn=$warehousing_info['warehousing_sn'];
	$warehousing_from=$warehousing_info['warehousing_from'];
	if(!empty($order_id))
	{
		$table=$warehousing_style_id==1?$ecs->table('erp_order') : $ecs->table('order_info');
		//TODO:: change to validating function
		if($warehousing_from == Yoho\cms\Controller\ErpController::PREFIX_REPAIR_PO)
			$sql="select repair_sn from ".$ecs->table('repair_orders')." where repair_id='".$order_id."'";
		else
			$sql="select order_sn from ".$table." where order_id='".$order_id."'";
		$order_sn=$db->getOne($sql);
	}
	else
	{
		$order_sn='';
	}
	$warehousing_item_info=get_warehousing_item_info($warehousing_id);
	if($warehousing_style_id==1)
	{
		foreach($warehousing_item_info as $warehousing_item)
		{
			$order_item_id=$warehousing_item['order_item_id'];
			$received_qty=$warehousing_item['received_qty'];
			$sql="update ".$ecs->table('erp_order_item')." set warehousing_qty=warehousing_qty+".$received_qty." where order_item_id='".$order_item_id."'";
			if(!$db->query($sql))
			{
				return false;
			}
		}
	}
	elseif($warehousing_style_id==2 ||$warehousing_style_id==3)
	{
		foreach($warehousing_item_info as $warehousing_item)
		{
			$order_item_id=$warehousing_item['order_item_id'];
			$received_qty=$warehousing_item['received_qty'];
			$sql="update ".$ecs->table('order_goods')." set delivery_qty=delivery_qty-".$received_qty." where rec_id='".$order_item_id."' and delivery_qty >= ".$received_qty.""; // "and delivery_qty >= $received_qty" added by howang so MySQL won't report error when delivery_qty goes below 0
			if(!$db->query($sql))
			{
				return false;
			}
			$sql="select order_id from ".$ecs->table('order_goods')." where rec_id='".$order_item_id."'";
			$shop_order_id=$db->getOne($sql);
			$sql="select sum(goods_price*delivery_qty) as delivery_goods_amount from ".$GLOBALS['ecs']->table('order_goods')." where order_id='".$shop_order_id."'";
			$delivery_goods_amount=$GLOBALS['db']->getOne($sql);
			$sql="update ".$GLOBALS['ecs']->table('order_info')." set delivery_goods_amount='".$delivery_goods_amount."' where order_id='".$shop_order_id."'";
			if(!$db->query($sql))
			{
				return false;
			}
		}
		//Supplier inventory consumption sequence
		$baseTxn = [
			'sales_order_id'	=> $order_id,
			'warehousing_id'	=> $warehousing_id,
			'remark'			=> "訂單 $order_sn 取消發貨 $warehousing_sn"
		];
		$erpController->processConsumptionSequence($warehousing_item_info,false,$baseTxn);
	}
	foreach($warehousing_item_info as $warehousing_item)
	{
		$goods_id=$warehousing_item['goods_id'];
		$attr_id=$warehousing_item['attr_id'];
		$qty=$warehousing_item['received_qty'];
		$price=$warehousing_item['price'];
		$sql="select stock from ".$ecs->table('erp_stock')." where goods_id='".$goods_id."' and goods_attr_id='".$attr_id."' and warehouse_id='".$warehouse_id."' order by stock_id desc limit 1";
		$stock=$db->getOne($sql);
		$stock=!empty($stock)?$stock: 0;
		$sql="insert into ".$ecs->table('erp_stock')." set goods_id='".$goods_id."',goods_attr_id='".$attr_id."',qty='".$qty."',price='".$price."',amount=qty*price,act_time='".gmtime()."',warehouse_id='".$warehouse_id."'";
		$sql.=", w_d_name='".$warehousing_from."', w_d_style_id='".$warehousing_style_id."',order_sn='".$order_sn."',stock_style='w',w_d_sn='".$warehousing_sn."',stock=".($stock+$qty).",act_date='".local_date('Y-m-d',gmtime())."'";
		if(!$db->query($sql))
		{
			return false;
		}
	}
	foreach($warehousing_item_info as $warehousing_item)
	{
		$goods_id=$warehousing_item['goods_id'];
		$attr_id=$warehousing_item['attr_id'];
		$qty=$warehousing_item['received_qty'];
		if(empty($attr_id))
		{
			$sql="select attr_id from ".$ecs->table('erp_goods_attr')." where goods_id='".$goods_id."' and goods_attr_id_list='$$'";
			$attr_id=$db->getOne($sql);
		}
		$sql="update ".$ecs->table('erp_goods_attr')." set goods_qty=goods_qty+".$qty." where goods_id='".$goods_id."' and attr_id='".$attr_id."'";
		if(!$db->query($sql))
		{
			return false;
		}

	    $sql="select count(*) from ".$ecs->table('erp_goods_attr_stock')." where goods_id='".$goods_id."' and attr_id='".$attr_id."' and warehouse_id='".$warehouse_id."' ";
        $item_count = $db->getOne($sql);
        if ($item_count == 0) {
            $sql="insert into ".$ecs->table('erp_goods_attr_stock')." (`attr_id`,`goods_id`,`warehouse_id`, `goods_qty`, `begin_qty`, `delivery_qty`, `warehousing_qty`, `end_qty`) values ( ".$attr_id.",".$goods_id.",".$warehouse_id.",".$qty.",0,0,0,0 )";
        } else {
            $sql="update ".$ecs->table('erp_goods_attr_stock')." set goods_qty =goods_qty +".$qty." where goods_id='".$goods_id."' and attr_id='".$attr_id."' and warehouse_id='".$warehouse_id."'";
        }

		if(!$db->query($sql))
		{
			return false;
		}
	}
	foreach($warehousing_item_info as $warehousing_item)
	{
		$goods_id = $warehousing_item['goods_id'];
		$received_quantity = intval($warehousing_item['received_qty']);
		// Back in stock notification
		//TODO: why compare total qty with received qty?
		$new_quantity = $db->getOne("SELECT SUM(goods_qty) FROM " . $ecs->table('erp_goods_attr') . " WHERE goods_id='".$goods_id."'");
		if ($new_quantity == $received_quantity)
		{
			if (@include_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_notifications.php'))
			{
				notify_goods_stock($db->getRow("SELECT goods_id, goods_name, goods_sn FROM " . $ecs->table('goods') . " WHERE goods_id = '$goods_id'"), true);
			}
			else
			{
				error_log(__FILE__.':'.__LINE__.' Cannot include lib_notifications.php');
			}
		}
		// Update sales count
		if ((!empty($order_id)) && (($warehousing_style_id == 2) || ($warehousing_style_id == 3)))
		{
			$sql = "UPDATE " . $ecs->table('goods') .
				   " SET sales = sales - " . $received_quantity .
				   " WHERE goods_id = '" . $goods_id . "'";
			if(!$db->query($sql))
			{
				return false;
			}
		}
	}
	return true;
}
function deduct_stock_by_delivery($erp_delivery_id)
{
	global $db,$ecs;
	$erpController = new Yoho\cms\Controller\ErpController();
	$sql="select * from ".$GLOBALS['ecs']->table('erp_delivery')." where delivery_id='".$erp_delivery_id."'";
	$delivery_info=$db->getRow($sql);
	$delivery_style_id=$delivery_info['delivery_style_id'];
	$warehouse_id=$delivery_info['warehouse_id'];
	$order_id=$delivery_info['order_id'];
	$delivery_sn=$delivery_info['delivery_sn'];
	$delivery_to=$delivery_info['delivery_to'];
	if(!empty($order_id))
	{
		$sql="select order_sn from ".$ecs->table('order_info')." where order_id='".$order_id."'";
		$order_sn=$db->getOne($sql);
	}
	else
	{
		$order_sn='';
	}
	$delivery_item_info=get_delivery_item_info($erp_delivery_id);
	if($delivery_style_id==1)
	{
		foreach($delivery_item_info as $delivery_item)
		{
			$order_item_id=$delivery_item['order_item_id'];
			$delivered_qty=$delivery_item['delivered_qty'];
			$sql="update ".$ecs->table('order_goods')." set delivery_qty=delivery_qty+".$delivered_qty." where rec_id='".$order_item_id."'";
			if(!$db->query($sql))
			{
				$db->query('ROLLBACK');
				return false;
			}
			$sql="select order_id from ".$ecs->table('order_goods')." where rec_id='".$order_item_id."'";
			$shop_order_id=$db->getOne($sql);
			$sql="select sum(goods_price*delivery_qty) as delivery_goods_amount from ".$GLOBALS['ecs']->table('order_goods')." where order_id='".$shop_order_id."'";
			$delivery_goods_amount=$GLOBALS['db']->getOne($sql);
			$sql="update ".$GLOBALS['ecs']->table('order_info')." set delivery_goods_amount='".$delivery_goods_amount."' where order_id='".$shop_order_id."'";
			if(!$db->query($sql))
			{
				return false;
			}
		}
		//Supplier inventory consumption sequence
		$baseTxn = [
			'sales_order_id'	=> $order_id,
			'delivery_id'		=> $erp_delivery_id,
			'remark'			=> "訂單 $order_sn 出貨 $delivery_sn"
		];
		$erpController->processConsumptionSequence($delivery_item_info,true,$baseTxn);
	}
	foreach($delivery_item_info as $delivery_item)
	{
		$goods_id=$delivery_item['goods_id'];
		$attr_id=$delivery_item['attr_id'];
		$qty=$delivery_item['delivered_qty'];
		$price=$delivery_item['price'];
		$sql="select stock from ".$ecs->table('erp_stock')." where goods_id='".$goods_id."' and goods_attr_id='".$attr_id."' and warehouse_id='".$warehouse_id."' order by stock_id desc limit 1";
		$stock=$db->getOne($sql);
		$stock=!empty($stock)?$stock: 0;
		if($stock <$qty)
		{
			$db->query('ROLLBACK');
			return false;
		}
		$sql="insert into ".$ecs->table('erp_stock')." set goods_id='".$goods_id."',goods_attr_id='".$attr_id."',qty='".(0-$qty)."',price='".$price."',amount=qty*price,act_time='".gmtime()."',warehouse_id='".$warehouse_id."'";
		$sql.=", w_d_name='".$delivery_to."', w_d_style_id='".$delivery_style_id."',order_sn='".$order_sn."',stock_style='d',w_d_sn='".$delivery_sn."',stock=".($stock-$qty).",act_date='".local_date('Y-m-d',gmtime())."'";
		if(!$db->query($sql))
		{
			$db->query('ROLLBACK');
			return false;
		}
	}
	foreach($delivery_item_info as $delivery_item)
	{
		$goods_id=$delivery_item['goods_id'];
		$attr_id=$delivery_item['attr_id'];
		$qty=$delivery_item['delivered_qty'];
		if(empty($attr_id))
		{
			$sql="select attr_id from ".$ecs->table('erp_goods_attr')." where goods_id='".$goods_id."' and goods_attr_id_list='$$'";
			$attr_id=$db->getOne($sql);
		}
		$sql="update ".$ecs->table('erp_goods_attr')." set goods_qty=goods_qty-".$qty." where goods_id='".$goods_id."' and attr_id='".$attr_id."'";
		if(!$db->query($sql))
		{
			$db->query('ROLLBACK');
			return fasle;
		}

	    $sql="select count(*) from ".$ecs->table('erp_goods_attr_stock')." where goods_id='".$goods_id."' and attr_id='".$attr_id."' and warehouse_id='".$warehouse_id."' ";
        $item_count = $db->getOne($sql);
        if ($item_count == 0) {
            $sql="insert into ".$ecs->table('erp_goods_attr_stock')." (`attr_id`,`goods_id`,`warehouse_id`, `goods_qty`, `begin_qty`, `delivery_qty`, `warehousing_qty`, `end_qty`) values ( ".$attr_id.",".$goods_id.",".$warehouse_id.",".$qty.",0,0,0,0 )";
        } else {
            $sql="update ".$ecs->table('erp_goods_attr_stock')." set goods_qty =goods_qty-".$qty." where goods_id='".$goods_id."' and attr_id='".$attr_id."' and warehouse_id='".$warehouse_id."'";
        }

		//$sql="update ".$ecs->table('erp_goods_attr_stock')." set goods_qty =goods_qty-".$qty." where goods_id='".$goods_id."' and attr_id='".$attr_id."' and warehouse_id='".$warehouse_id."'";
		if(!$db->query($sql))
		{
			$db->query('ROLLBACK');
			return false;
		}
	}
	foreach($delivery_item_info as $delivery_item)
	{
		$goods_id = $delivery_item['goods_id'];
		$qty = intval($delivery_item['delivered_qty']);
		// Out of stock notification
		$new_quantity = $erpController->getSellableProductQty($goods_id);
		if ($new_quantity == 0)
		{
			if (@include_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_notifications.php'))
			{
				notify_goods_stock($db->getRow("SELECT goods_id, goods_name, goods_sn FROM " . $ecs->table('goods') . " WHERE goods_id = '$goods_id'"), false);
			}
			else
			{
				error_log(__FILE__.':'.__LINE__.' Cannot include lib_notifications.php');
			}
		}
		// Update sales count
		if (!empty($order_id))
		{
			$sql = "UPDATE " . $ecs->table('goods') .
				   " SET sales = sales + " . $qty .
				   " WHERE goods_id = '" . $goods_id . "'";
			if(!$db->query($sql))
			{
				$db->query('ROLLBACK');
				return false;
			}
		}
	}
	return true;
}
function check_transfer_integrity($transfer_id='')
{
	if(empty($transfer_id))
	{
		return false;
	}
	$sql="select warehouse_from,warehouse_to from ".$GLOBALS['ecs']->table('erp_stock_transfer')." where transfer_id='".$transfer_id."'";
	$row=$GLOBALS['db']->getRow($sql);
	if($row['warehouse_from']==$row['warehouse_to'])
	{
		return 1;
	}
	$sql="select transfer_qty from ".$GLOBALS['ecs']->table('erp_stock_transfer_item')." where transfer_id=".$transfer_id."";
	$transfer_item=$GLOBALS['db']->getAll($sql);
	if(empty($transfer_item))
	{
		return 2;
	}
	else
	{
		foreach($transfer_item as $row)
		{
			if($row['transfer_qty']<=0)
			{
				return 3;
			}
		}
	}
	return 0;
}
function gen_stock_stransfer_sn()
{
	$sql="select transfer_sn from ".$GLOBALS['ecs']->table('erp_stock_transfer')." order by transfer_id desc limit 1";
	$result=$GLOBALS['db']->getOne($sql);
	if(!empty($result))
	{
		$year_date=substr($result,2,4);
		$current_year_date=local_date("ym");
		if($current_year_date==$year_date)
		{
			$sn=substr($result,6,3);
			$sn=intval($sn)+1;
			$sn=gen_zero_str(3-strlen($sn)).$sn;
			$sn='TR'.$year_date.$sn;
			return $sn;
		}
		else
		{
			$sn='TR'.$current_year_date."001";
			return $sn;
		}
	}
	else
	{
		$current_year_date=local_date("ym");
		$sn='TR'.$current_year_date."001";
		return $sn;
	}
}
function add_transfer()
{
	$transfer_sn=gen_stock_stransfer_sn();
	$admin_id=erp_get_admin_id();
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	$warehouse_info=get_warehouse_list($admin_id,$agency_id,1,-1,-1,'asc',false);
	if(empty($warehouse_info))
	{
		return -1;
	}
	else
	{
		$warehouse_id=$warehouse_info[0]['warehouse_id'];
		$sql="insert into ".$GLOBALS['ecs']->table('erp_stock_transfer')." set ";
		$sql.="transfer_sn='".$transfer_sn."',";
		$sql.="warehouse_from ='".$warehouse_id."',";
		$sql.="warehouse_to ='".$warehouse_id."',";
		$sql.="create_time='".gmtime()."',";
		$sql.="admin_from='".$admin_id."',";
		$sql.="admin_to  =0,";
		$sql.="status   ='1',";
		$sql.="agency_id   ='".$agency_id."'";
		if($GLOBALS['db']->query($sql))
		{
			return $GLOBALS['db']->insert_id($sql);
		}
		else
		{
			return false;
		}
	}
}
function is_stock_transfer_exist($transfer_id)
{
	if(empty($transfer_id))
	{
		return false;
	}
	else
	{
		$sql="select * from ".$GLOBALS['ecs']->table('erp_stock_transfer')." where transfer_id='".$transfer_id."'";
		$result=$GLOBALS['db']->getRow($sql);
		return !empty($result) ?$result : false;
	}
}
function is_stock_transfer_item_exist($item_id)
{
	if(empty($item_id))
	{
		return false;
	}
	else
	{
		$sql="select * from ".$GLOBALS['ecs']->table('erp_stock_transfer_item')." where item_id='".$item_id."'";
		$result=$GLOBALS['db']->getRow($sql);
		return !empty($result) ?$result : false;
	}
}
function is_admin_stock_transfer($transfer_id,$admin_id)
{
	$sql="select * from ".$GLOBALS['ecs']->table('erp_stock_transfer')." where transfer_id='".$transfer_id."' and admin_from='".$admin_id."'";
	$row=$GLOBALS['db']->getRow($sql);
	if($row==false)
	{
		return false;
	}
	else
	{
		return true;
	}
}
function get_goods_transfer_item_info($transfer_id)
{
	return get_stock_transfer_item_list(array('transfer_id'=>$transfer_id));
}
function get_goods_transfer_info($transfer_id)
{
	$transfer_info=get_stock_transfer_list(array('transfer_id'=>$transfer_id));
	return !empty($transfer_info) &&isset($transfer_info[0])?$transfer_info[0] : array();
}
function warehouse_admin_list()
{
	$sql="select a.user_id,a.user_name from ".$GLOBALS['ecs']->table('admin_user')." as a where a.user_id in (select distinct admin_id from ".$GLOBALS['ecs']->table('erp_warehouse_admin')." where 1)";
	return $GLOBALS['db']->getAll($sql);
}
function get_goods_transfer_status()
{
	$status[0]['status_no']=1;
	$status[0]['status_style']="轉撥單錄入中";
	$status[1]['status_no']=2;
	$status[1]['status_style']="待確認收貨";
	$status[2]['status_no']=3;
	$status[2]['status_style']="已轉撥";
	$status[3]['status_no']=4;
	$status[3]['status_style']="拒絕收貨";
	return $status;
}
function get_stock_transfer_item_list($paras=array(),$start=-1,$num=-1)
{
	$sql="select * from ".$GLOBALS['ecs']->table('erp_stock_transfer_item')." as i where 1";
	if(isset($paras['transfer_id']) &&!empty($paras['transfer_id']))
	{
		$sql.=" and i.transfer_id='".$paras['transfer_id']."' ";
	}
	if($start!=-1 &&$num!=-1)
	{
		if($start<=0)
		{
			$start=0;
		}
		$sql.=" limit ".$start.",".$num;
	}
	$res=$GLOBALS['db']->query($sql);
	$stock_transfer_item=array();
	while($row=mysql_fetch_assoc($res))
	{
		$sql="select goods_id,cat_id,goods_sn,goods_name,goods_thumb,goods_img,original_img from ".$GLOBALS['ecs']->table('goods')." where goods_id='".$row['goods_id']."'";
		$goods_info=$GLOBALS['db']->getRow($sql);
		if(!empty($goods_info['goods_thumb']))
		{
			$goods_info['goods_thumb']='../'.$goods_info['goods_thumb'];
		}
		if(!empty($goods_info['goods_img']))
		{
			$goods_info['goods_img']='../'.$goods_info['goods_img'];
		}
		if(!empty($goods_info['original_img']))
		{
			$goods_info['original_img']='../'.$goods_info['original_img'];
		}
		$row['goods_info']=$goods_info;
		$row['goods_attr']=get_attr_info($row['attr_id']);
		$stock_transfer_item[]=$row;
	}
	return $stock_transfer_item;
}
function get_stock_transfer_count($paras=array())
{
	$sql="select count(*) from ".$GLOBALS['ecs']->table('erp_stock_transfer')." as s where 1";
	if(isset($paras['status']) &&!empty($paras['status']))
	{
		$sql.=" and s.status='".$paras['status']."' ";
	}
	if(isset($paras['start_time']) &&!empty($paras['start_time']))
	{
		$sql.=" and s.create_time>='".$paras['start_time']."'";
	}
	if(isset($paras['end_time']) &&!empty($paras['end_time']))
	{
		$sql.=" and s.create_time<='".$paras['end_time']."'";
	}
	if(isset($paras['warehouse_from']) &&!empty($paras['warehouse_from']))
	{
		$sql.=" and s.warehouse_from='".$paras['warehouse_from']."'";
	}
	if(isset($paras['warehouse_to']) &&!empty($paras['warehouse_to']))
	{
		$sql.=" and s.warehouse_to='".$paras['warehouse_to']."'";
	}
	if(isset($paras['admin_from']) &&!empty($paras['admin_from']))
	{
		$sql.=" and s.admin_from='".$paras['admin_from']."'";
	}
	if(isset($paras['admin_to']) &&!empty($paras['admin_to']))
	{
		$sql.=" and s.admin_to='".$paras['admin_to']."'";
	}
	if(isset($paras['goods_sn']) &&!empty($paras['goods_sn']))
	{
		$sql.=" and s.transfer_id in (select i.transfer_id from ".$GLOBALS['ecs']->table('erp_stock_transfer_item')." as i,".$GLOBALS['ecs']->table('goods')." as g where g.goods_sn like '%".$paras['goods_sn']."%' and g.goods_id=i.goods_id)";
	}
	if(isset($paras['goods_name']) &&!empty($paras['goods_name']))
	{
		$sql.=" and s.transfer_id in (select i.transfer_id from ".$GLOBALS['ecs']->table('erp_stock_transfer_item')." as i,".$GLOBALS['ecs']->table('goods')." as g where g.goods_name like '%".$paras['goods_name']."%' and g.goods_id=i.goods_id)";
	}
	if(isset($paras['agency_id']) &&intval($paras['agency_id']) >0)
	{
		$sql.=" and (warehouse_from in (select warehouse_id from ".$GLOBALS['ecs']->table('erp_warehouse')." where agency_id='".$paras['agency_id']."') or warehouse_to in (select warehouse_id from ".$GLOBALS['ecs']->table('erp_warehouse')." where agency_id='".$paras['agency_id']."'))";
	}
	return $GLOBALS['db']->getOne($sql);
}
function get_stock_transfer_list($paras=array(),$start=-1,$num=-1)
{
	$sql="select * from ".$GLOBALS['ecs']->table('erp_stock_transfer')." as s where 1";
	if(isset($paras['transfer_id']) &&!empty($paras['transfer_id']))
	{
		$sql.=" and s.transfer_id='".$paras['transfer_id']."' ";
	}
	if(isset($paras['status']) &&!empty($paras['status']))
	{
		$sql.=" and s.status='".$paras['status']."' ";
	}
	if(isset($paras['start_time']) &&!empty($paras['start_time']))
	{
		$sql.=" and s.create_time>='".$paras['start_time']."'";
	}
	if(isset($paras['end_time']) &&!empty($paras['end_time']))
	{
		$sql.=" and s.create_time<='".$paras['end_time']."'";
	}
	if(isset($paras['warehouse_from']) &&!empty($paras['warehouse_from']))
	{
		$sql.=" and s.warehouse_from='".$paras['warehouse_from']."'";
	}
	if(isset($paras['warehouse_to']) &&!empty($paras['warehouse_to']))
	{
		$sql.=" and s.warehouse_to='".$paras['warehouse_to']."'";
	}
	if(isset($paras['admin_from']) &&!empty($paras['admin_from']))
	{
		$sql.=" and s.admin_from='".$paras['admin_from']."'";
	}
	if(isset($paras['admin_to']) &&!empty($paras['admin_to']))
	{
		$sql.=" and s.admin_to='".$paras['admin_to']."'";
	}
	if(isset($paras['goods_sn']) &&!empty($paras['goods_sn']))
	{
		$sql.=" and s.transfer_id in (select i.transfer_id from ".$GLOBALS['ecs']->table('erp_stock_transfer_item')." as i,".$GLOBALS['ecs']->table('goods')." as g where g.goods_sn like '%".$paras['goods_sn']."%' and g.goods_id=i.goods_id)";
	}
	if(isset($paras['goods_name']) &&!empty($paras['goods_name']))
	{
		$sql.=" and s.transfer_id in (select i.transfer_id from ".$GLOBALS['ecs']->table('erp_stock_transfer_item')." as i,".$GLOBALS['ecs']->table('goods')." as g where g.goods_name like '%".$paras['goods_name']."%' and g.goods_id=i.goods_id)";
	}
	if(isset($paras['agency_id']) &&intval($paras['agency_id']) >0)
	{
		$sql.=" and (warehouse_from in (select warehouse_id from ".$GLOBALS['ecs']->table('erp_warehouse')." where agency_id='".$paras['agency_id']."') or warehouse_to in (select warehouse_id from ".$GLOBALS['ecs']->table('erp_warehouse')." where agency_id='".$paras['agency_id']."'))";
	}
	$sql.=" order by transfer_id desc";
	if($start!=-1 &&$num!=-1)
	{
		if($start<=0)
		{
			$start=0;
		}
		$sql.=" limit ".$start.",".$num;
	}
	$res=$GLOBALS['db']->query($sql);
	$stock_transfer=array();
	while($row=mysql_fetch_assoc($res))
	{
		$sql="select name from ".$GLOBALS['ecs']->table('erp_warehouse')." where warehouse_id='".$row['warehouse_from']."'";
		$row['warehouse_from_name']=$GLOBALS['db']->getOne($sql);
		$sql="select name from ".$GLOBALS['ecs']->table('erp_warehouse')." where warehouse_id='".$row['warehouse_to']."'";
		$row['warehouse_to_name']=$GLOBALS['db']->getOne($sql);
		$sql="select user_name from ".$GLOBALS['ecs']->table('admin_user')." where user_id='".$row['admin_from']."'";
		$row['admin_from_name']=$GLOBALS['db']->getOne($sql);
		$sql="select user_name from ".$GLOBALS['ecs']->table('admin_user')." where user_id='".$row['admin_to']."'";
		$row['admin_to_name']=$GLOBALS['db']->getOne($sql);
		$row['create_time']=local_date('Y-m-d',$row['create_time']);
		$row['transfer_time']=local_date('Y-m-d',$row['transfer_time']);
		$sql="select sum(transfer_qty) as transfer_qty from ".$GLOBALS['ecs']->table('erp_stock_transfer_item')." where transfer_id='".$row['transfer_id']."'";
		$sum=$GLOBALS['db']->getRow($sql);
		$row['transfer_qty']=$sum['transfer_qty'] >0?$sum['transfer_qty']:0;
		$row['available_act']=get_stock_transfer_available_act($row['transfer_id']);
		$sql="select agency_name from ".$GLOBALS['ecs']->table('agency')." where agency_id in (select agency_id from ".$GLOBALS['ecs']->table('erp_warehouse')." where warehouse_id='".$row['warehouse_from']."')";
		$row['agency_from']=$GLOBALS['db']->getOne($sql);
		$sql="select agency_name from ".$GLOBALS['ecs']->table('agency')." where agency_id in (select agency_id from ".$GLOBALS['ecs']->table('erp_warehouse')." where warehouse_id='".$row['warehouse_to']."')";
		$row['agency_to']=$GLOBALS['db']->getOne($sql);
		$stock_transfer[]=$row;
	}
	return $stock_transfer;
}
function lock_stock_transfer_table($id='',$act='')
{
	$GLOBALS['db']->query('START TRANSACTION');
	if(check_stock_transfer_accessibility($id,$act))
	{
		$user_id=erp_get_admin_id();
		$lock_time=gmtime();
		$last_act_time=gmtime();
		$sql="update ".$GLOBALS['ecs']->table('erp_stock_transfer')." set locked_by='".$user_id."'";
		$sql.=", locked_time='".$lock_time."'";
		$sql.=", last_act_time='".$last_act_time."'";
		$sql.=", is_locked='1'";
		$sql.=" where transfer_id='".$id."'";
		if($GLOBALS['db']->query($sql))
		{
			$GLOBALS['db']->query('COMMIT');
			return true;
		}
		else
		{
			$GLOBALS['db']->query('ROLLBACK');
			return false;
		}
	}
	else
	{
		$GLOBALS['db']->query('ROLLBACK');
		return false;
	}
}
function get_stock_transfer_available_act($id='')
{
	$act['view']=check_stock_transfer_accessibility($id,'view');
	$act['edit']=check_stock_transfer_accessibility($id,'edit');
	$act['delete']=check_stock_transfer_accessibility($id,'delete');
	$act['post']=check_stock_transfer_accessibility($id,'post');
	$act['confirm']=check_stock_transfer_accessibility($id,'confirm');
	return $act;
}
function check_stock_transfer_accessibility($id='',$act='')
{
	if(empty($act) ||empty($id))
	{
		return false;
	}
	$sql="select status,is_locked,last_act_time,locked_by from ".$GLOBALS['ecs']->table('erp_stock_transfer')." where transfer_id='".$id."' for update";
	$res=$GLOBALS['db']->getRow($sql);
	if($res==false)
	{
		return false;
	}
	else
	{
		$is_locked=$res['is_locked'];
		$status=$res['status'];
		$last_act_time=$res['last_act_time'];
		$locked_by=$res['locked_by'];
		$current_time=gmtime();
		$if_overtime=(($current_time-$last_act_time)>(10))?1:0;
		$current_user_id=erp_get_admin_id();
		if($act=='view')
		{
			if(admin_priv('erp_warehouse_view','',false) ||admin_priv('erp_warehouse_manage','',false) ||admin_priv('erp_warehouse_approve','',false))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		elseif($act=='edit'||$act=='delete'||$act=='post')
		{
			if($status==1)
			{
				if(admin_priv('erp_warehouse_manage','',false) &&($is_locked==0 ||($is_locked==1 &&$if_overtime==1 &&$current_user_id!=$locked_by) ||($is_locked==1 &&$current_user_id==$locked_by)))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		elseif($act=='confirm')
		{
			if($status==2)
			{
				if(admin_priv('erp_warehouse_manage','',false) &&($is_locked==0 ||($is_locked==1 &&$if_overtime==1 &&$current_user_id!=$locked_by) ||($is_locked==1 &&$current_user_id==$locked_by)))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
}
function get_stock_record_count_type($paras=array()){
	$sql ="select concat(IF(es.w_d_style_id=0,'RMA',IF(es.stock_style='w',ews.warehousing_style,eds.delivery_style)),':',es.w_d_name) as w_d_style,sum(es.qty) as sum_qty from ".$GLOBALS['ecs']->table('erp_stock')." es ";
	$sql.="LEFT JOIN ".$GLOBALS['ecs']->table('erp_warehousing_style')." ews ON es.w_d_style_id=ews.warehousing_style_id ";
	$sql.="LEFT JOIN ".$GLOBALS['ecs']->table('erp_delivery_style')." eds ON es.w_d_style_id=eds.delivery_style_id ";
	$sql.="LEFT JOIN ".$GLOBALS['ecs']->table('goods')." g ON g.goods_id=es.goods_id where 1 ";
	if(isset($paras['goods_id']) &&!empty($paras['goods_id']))
	{
		$sql.=" AND es.goods_id='".$paras['goods_id']."'";
	}
	if(isset($paras['goods_sn']) &&!empty($paras['goods_sn']))
	{
		$sql.=" AND g.goods_sn='".$paras['goods_sn']."'";
	}
	if(isset($paras['goods_name']) &&!empty($paras['goods_name']))
	{
		$sql.=" AND g.goods_name like '%".$paras['goods_name']."%'";
	}
	if(isset($paras['warehouse_id']) &&!empty($paras['warehouse_id']))
	{
		$sql.=" AND es.warehouse_id='".$paras['warehouse_id']."'";
	}
	$sql.="  GROUP BY w_d_style ORDER BY w_d_style";
	return $GLOBALS['db']->getAll($sql);
}
function get_stock_record_count($paras=array())
{
	$sql="select count(*) from ".$GLOBALS['ecs']->table('erp_stock')." where 1 ";
	if(isset($paras['stock_id']) &&!empty($paras['stock_id']))
	{
		$sql.=" and stock_id='".$paras['stock_id']."'";
	}
	if(isset($paras['stock_style']) &&$paras['stock_style']!='a')
	{
		$sql.=" and stock_style='".$paras['stock_style']."'";
	}
	if(isset($paras['goods_id']) &&!empty($paras['goods_id']))
	{
		$sql.=" and goods_id='".$paras['goods_id']."'";
	}
	if(isset($paras['goods_sn']) &&!empty($paras['goods_sn']))
	{
		$sql.=" and goods_id in (select goods_id from ".$GLOBALS['ecs']->table('goods')." where goods_sn='".$paras['goods_sn']."')";
	}
	if(isset($paras['goods_name']) &&!empty($paras['goods_name']))
	{
		$sql.=" and goods_id in (select goods_id from ".$GLOBALS['ecs']->table('goods')." where goods_name like '%".$paras['goods_name']."%')";
	}
	if(isset($paras['goods_barcode']) &&!empty($paras['goods_barcode']))
	{
		$sql.=" and goods_attr_id in (select attr_id from ".$GLOBALS['ecs']->table('erp_goods_attr')." as ga,".$GLOBALS['ecs']->table('erp_goods_barcode')." as gb where gb.barcode like '%".$paras['goods_barcode']."%' and gb.id=ga.barcode_id)";
	}
	if(isset($paras['warehouse_id']) &&!empty($paras['warehouse_id']))
	{
		$sql.=" and warehouse_id='".$paras['warehouse_id']."'";
	}
	if(isset($paras['w_d_style_id']) &&!empty($paras['w_d_style_id']))
	{
		$sql.=" and w_d_style_id='".$paras['w_d_style_id']."'";
	}
	if(isset($paras['start_time']) &&!empty($paras['start_time']))
	{
		$sql.=" and act_time >='".$paras['start_time']."'";
	}
	if(isset($paras['end_time']) &&!empty($paras['end_time']))
	{
		$sql.=" and act_time <='".$paras['end_time']."'";
	}
	if(isset($paras['agency_id']) &&intval($paras['agency_id']) >0)
	{
		$sql.=" and warehouse_id in (select warehouse_id from ".$GLOBALS['ecs']->table('erp_warehouse')." where agency_id='".$paras['agency_id']."')";
	}
	if(isset($paras['supplier_id']) &&!empty($paras['supplier_id']))
	{
		$sql.=" and goods_id in (select goods_id from ".$GLOBALS['ecs']->table('erp_goods_supplier')." where supplier_id='".$paras['supplier_id']."')";
	}
	if(isset($paras['brand_id']) &&!empty($paras['brand_id']))
	{
		$sql.=" and goods_id in (select goods_id from ".$GLOBALS['ecs']->table('goods')." where brand_id='".$paras['brand_id']."')";
	}
	$sql.=" and goods_id in (select goods_id from ".$GLOBALS['ecs']->table('goods')." where is_delete='0')";
	return $GLOBALS['db']->getOne($sql);
}
function get_stock_record($paras=array(),$start=-1,$num=-1)
{
	$sql ="SELECT es.*, ";
	$sql.=" g.cat_id, g.goods_sn, g.goods_name, g.goods_thumb, g.goods_img, g.original_img, ";
	$sql.=" ew.name as warehouse_name ";// , ro.repair_id, est.transfer_id
	$sql.=" FROM ".$GLOBALS['ecs']->table('erp_stock')." as es ";
	$sql.=" LEFT JOIN ".$GLOBALS['ecs']->table('goods')." as g ON g.goods_id = es.goods_id ";
	$sql.=" LEFT JOIN ".$GLOBALS['ecs']->table('erp_warehouse')." as ew ON ew.warehouse_id = es.warehouse_id ";
	// $sql.=" LEFT JOIN ".$GLOBALS['ecs']->table('repair_orders')." as ro ON BINARY es.order_sn = BINARY ro.repair_sn ";
	// $sql.=" LEFT JOIN ".$GLOBALS['ecs']->table('erp_stock_transfer')." as est ON BINARY es.order_sn = BINARY est.transfer_sn ";
	$sql.=" WHERE 1 ";
	if(isset($paras['stock_id']) &&!empty($paras['stock_id']))
	{
		$sql.=" AND es.stock_id='".$paras['stock_id']."'";
	}
	if(isset($paras['stock_style']) &&$paras['stock_style']!='a')
	{
		$sql.=" AND es.stock_style='".$paras['stock_style']."'";
	}
	if(isset($paras['goods_id']) &&!empty($paras['goods_id']))
	{
		$sql.=" AND es.goods_id='".$paras['goods_id']."'";
	}
	if(isset($paras['goods_sn']) &&!empty($paras['goods_sn']))
	{
		$sql.=" AND g.goods_sn='".$paras['goods_sn']."'";
	}
	if(isset($paras['goods_name']) &&!empty($paras['goods_name']))
	{
		$sql.=" AND g.goods_name like '%".$paras['goods_name']."%'";
	}
	if(isset($paras['warehouse_id']) &&!empty($paras['warehouse_id']))
	{
		$sql.=" AND es.warehouse_id='".$paras['warehouse_id']."'";
	}
	if(isset($paras['w_d_style_id']) &&!empty($paras['w_d_style_id']))
	{
		$sql.=" AND es.w_d_style_id='".$paras['w_d_style_id']."'";
	}
	if(isset($paras['start_time']) &&!empty($paras['start_time']))
	{
		$sql.=" AND es.act_time >='".$paras['start_time']."'";
	}
	if(isset($paras['end_time']) &&!empty($paras['end_time']))
	{
		$sql.=" AND es.act_time <='".$paras['end_time']."'";
	}
	if(isset($paras['agency_id']) &&intval($paras['agency_id']) >0)
	{
		$sql.=" AND ew.agency_id='".$paras['agency_id']."'";
	}
	if(isset($paras['supplier_id']) &&!empty($paras['supplier_id']))
	{
		$sql.=" AND es.goods_id in (SELECT goods_id FROM ".$GLOBALS['ecs']->table('erp_goods_supplier')." WHERE supplier_id='".$paras['supplier_id']."')";
	}
	if(isset($paras['brand_id']) &&!empty($paras['brand_id']))
	{
		$sql.=" AND g.brand_id='".$paras['brand_id']."'";
	}
	// if(isset($paras['order_sn']) &&!empty($paras['order_sn']))
	// {
	// 	$sql.=" AND es.order_sn='".$paras['order_sn']."'";
	// }
	$sql.=" AND g.is_delete='0'";
	$sql.=" ORDER BY stock_id DESC ";
	if($start!=-1 &&$num!=-1)
	{
		if($start<=0)
		{
			$start=0;
		}
		$sql.=" LIMIT ".$start.",".$num;
	}
	$res=$GLOBALS['db']->query($sql);
	while($row=mysql_fetch_assoc($res))
	{
		// $goods_id=$row['goods_id'];
		// $sql="select goods_id,cat_id,goods_sn,goods_name,goods_thumb,goods_img,original_img from ".$GLOBALS['ecs']->table('goods')." where goods_id='".$goods_id."'";
		// $goods_info=$GLOBALS['db']->getRow($sql);
		// if(!empty($goods_info['goods_thumb']))
		// {
		// 	$goods_info['goods_thumb']='../'.$goods_info['goods_thumb'];
		// }
		// if(!empty($goods_info['goods_img']))
		// {
		// 	$goods_info['goods_img']='../'.$goods_info['goods_img'];
		// }
		// if(!empty($goods_info['original_img']))
		// {
		// 	$goods_info['original_img']='../'.$goods_info['original_img'];
		// }
		// $row['goods_info']=$goods_info;
		$row['goods_info']=array(
			'goods_id' => $row['goods_id'],
			'cat_id' => $row['cat_id'],
			'goods_sn' => $row['goods_sn'],
			'goods_name' => $row['goods_name'],
			'goods_thumb' => (empty($row['goods_thumb']) ? '' : '../'.$row['goods_thumb']),
			'goods_img' => (empty($row['goods_img']) ? '' : '../'.$row['goods_img']),
			'original_img' => (empty($row['original_img']) ? '' : '../'.$row['original_img'])
		);

        $sql ="SELECT ro.repair_id, est.transfer_id FROM ".$GLOBALS['ecs']->table('repair_orders')." as ro ";
        $sql.=" LEFT JOIN ".$GLOBALS['ecs']->table('erp_stock_transfer')." as est ON ro.repair_sn = est.transfer_sn ";
        $sql.=" WHERE ro.repair_sn = '".$row['order_sn']."'";

        $repair_info = $GLOBALS['db']->getRow($sql);
        $row['repair_id'] = $repair_info['repair_id'];
        $row['transfer_id'] = $repair_info['transfer_id'];


        $sql ="SELECT est.transfer_id, est.remark FROM ".$GLOBALS['ecs']->table('erp_stock_transfer')." as est ";
        $sql.=" WHERE est.transfer_sn = '".$row['order_sn']."'";
		$transfer_info = $GLOBALS['db']->getRow($sql);
		$row['transfer_remark'] = $transfer_info['remark'];
		$row['transfer_id'] = $transfer_info['transfer_id'];

		$row['act_date']=local_date('Y-m-d <br/> H:i:s',$row['act_time']);
		
		// Goods Attr is not used
		//$row['goods_attr']=get_attr_info($row['goods_attr_id']);
		
		// Optimized to the main query
		//$warehouse_id=$row['warehouse_id'];
		//$sql=" select name from ".$GLOBALS['ecs']->table('erp_warehouse')." where warehouse_id='".$warehouse_id."'";
		//$row['warehouse_name']=$GLOBALS['db']->getOne($sql);
		
		$stock_style=$row['stock_style'];
		$w_d_style_id=$row['w_d_style_id'];
		if($w_d_style_id==0)
		{
			$row['w_d_style']=$GLOBALS['_LANG']['erp_stock_transfer_w_d_style'];
		}
		else
		{
			if($stock_style=='w')
			{
				$sql=" select warehousing_style from ".$GLOBALS['ecs']->table('erp_warehousing_style')." where warehousing_style_id ='".$w_d_style_id."'";
			}
			elseif($stock_style=='d')
			{
				$sql=" select delivery_style from ".$GLOBALS['ecs']->table('erp_delivery_style')." where delivery_style_id ='".$w_d_style_id."'";
			}
			$row['w_d_style']=$GLOBALS['db']->getOne($sql);
		}
		
		if (substr($row['w_d_sn'],0,2)=="TR") {
			$row['stock_style'] = 'tr';
		}
		// Agency is not used
		//$sql="select agency_name from ".$GLOBALS['ecs']->table('agency')." where agency_id in (select agency_id from ".$GLOBALS['ecs']->table('erp_warehouse')." where warehouse_id=".$row['warehouse_id'].")";
		//$row['agency_name']=$GLOBALS['db']->getOne($sql);
		
		// Barcode is not used
		//$sql="select gb.barcode from ".$GLOBALS['ecs']->table('erp_goods_barcode')." as gb,".$GLOBALS['ecs']->table('erp_goods_attr')." as ga where ga.attr_id='".$row['goods_attr_id']."' and ga.barcode_id=gb.id limit 1";
		//$row['barcode']=$GLOBALS['db']->getOne($sql);
		$stock_record[]=$row;
	}
	return $stock_record;
}

function get_stock_record_rma($paras=array(),$start=-1,$num=-1)
{
	$sql ="SELECT es.*, ";
	$sql.=" g.cat_id, g.goods_sn, g.goods_name, g.goods_thumb, g.goods_img, g.original_img, ";
	$sql.=" ew.name as warehouse_name, est.transfer_id, est.remark";
	$sql.=" FROM ".$GLOBALS['ecs']->table('erp_stock')." as es ";
	$sql.=" LEFT JOIN ".$GLOBALS['ecs']->table('goods')." as g ON g.goods_id = es.goods_id ";
	$sql.=" LEFT JOIN ".$GLOBALS['ecs']->table('erp_warehouse')." as ew ON ew.warehouse_id = es.warehouse_id ";
	//$sql.=" LEFT JOIN ".$GLOBALS['ecs']->table('repair_orders')." as ro ON BINARY es.order_sn = BINARY ro.repair_sn ";
	$sql.=" LEFT JOIN ".$GLOBALS['ecs']->table('erp_stock_transfer')." as est ON BINARY es.order_sn = BINARY est.transfer_sn ";
	$sql.=" WHERE 1 ";
	if(isset($paras['stock_id']) &&!empty($paras['stock_id']))
	{
		$sql.=" AND es.stock_id='".$paras['stock_id']."'";
	}
	if(isset($paras['stock_style']) &&$paras['stock_style']!='a')
	{
		$sql.=" AND es.stock_style='".$paras['stock_style']."'";
	}
	if(isset($paras['goods_id']) &&!empty($paras['goods_id']))
	{
		$sql.=" AND es.goods_id='".$paras['goods_id']."'";
	}
	if(isset($paras['goods_sn']) &&!empty($paras['goods_sn']))
	{
		$sql.=" AND g.goods_sn='".$paras['goods_sn']."'";
	}
	if(isset($paras['goods_name']) &&!empty($paras['goods_name']))
	{
		$sql.=" AND g.goods_name like '%".$paras['goods_name']."%'";
	}
	if(isset($paras['warehouse_id']) &&!empty($paras['warehouse_id']))
	{
		$sql.=" AND es.warehouse_id='".$paras['warehouse_id']."'";
	}
	if(isset($paras['w_d_style_id']) &&!empty($paras['w_d_style_id']))
	{
		$sql.=" AND es.w_d_style_id='".$paras['w_d_style_id']."'";
	}
	if(isset($paras['start_time']) &&!empty($paras['start_time']))
	{
		$sql.=" AND es.act_time >='".$paras['start_time']."'";
	}
	if(isset($paras['end_time']) &&!empty($paras['end_time']))
	{
		$sql.=" AND es.act_time <='".$paras['end_time']."'";
	}
	if(isset($paras['agency_id']) &&intval($paras['agency_id']) >0)
	{
		$sql.=" AND ew.agency_id='".$paras['agency_id']."'";
	}
	if(isset($paras['supplier_id']) &&!empty($paras['supplier_id']))
	{
		$sql.=" AND es.goods_id in (SELECT goods_id FROM ".$GLOBALS['ecs']->table('erp_goods_supplier')." WHERE supplier_id='".$paras['supplier_id']."')";
	}
	if(isset($paras['brand_id']) &&!empty($paras['brand_id']))
	{
		$sql.=" AND g.brand_id='".$paras['brand_id']."'";
	}
	if(isset($paras['remark']) &&!empty($paras['remark']))
	{
		$sql.=" OR est.remark='".$paras['remark']."'";
	}
	// if(isset($paras['order_sn']) &&!empty($paras['order_sn']))
	// {
	// 	$sql.=" AND es.order_sn='".$paras['order_sn']."'";
	// }
	$sql.=" AND g.is_delete='0'";
	$sql.=" ORDER BY stock_id DESC ";
	
	if($start!=-1 &&$num!=-1)
	{
		if($start<=0)
		{
			$start=0;
		}
		$sql.=" LIMIT ".$start.",".$num;
	}
	$res=$GLOBALS['db']->query($sql);
	while($row=mysql_fetch_assoc($res))
	{
		// $goods_id=$row['goods_id'];
		// $sql="select goods_id,cat_id,goods_sn,goods_name,goods_thumb,goods_img,original_img from ".$GLOBALS['ecs']->table('goods')." where goods_id='".$goods_id."'";
		// $goods_info=$GLOBALS['db']->getRow($sql);
		// if(!empty($goods_info['goods_thumb']))
		// {
		// 	$goods_info['goods_thumb']='../'.$goods_info['goods_thumb'];
		// }
		// if(!empty($goods_info['goods_img']))
		// {
		// 	$goods_info['goods_img']='../'.$goods_info['goods_img'];
		// }
		// if(!empty($goods_info['original_img']))
		// {
		// 	$goods_info['original_img']='../'.$goods_info['original_img'];
		// }
		// $row['goods_info']=$goods_info;
		$row['goods_info']=array(
			'goods_id' => $row['goods_id'],
			'cat_id' => $row['cat_id'],
			'goods_sn' => $row['goods_sn'],
			'goods_name' => $row['goods_name'],
			'goods_thumb' => (empty($row['goods_thumb']) ? '' : '../'.$row['goods_thumb']),
			'goods_img' => (empty($row['goods_img']) ? '' : '../'.$row['goods_img']),
			'original_img' => (empty($row['original_img']) ? '' : '../'.$row['original_img'])
		);

        $sql ="SELECT ro.repair_id, est.transfer_id FROM ".$GLOBALS['ecs']->table('repair_orders')." as ro ";
        $sql.=" LEFT JOIN ".$GLOBALS['ecs']->table('erp_stock_transfer')." as est ON ro.repair_sn = est.transfer_sn ";
        $sql.=" WHERE ro.repair_sn = '".$row['order_sn']."'";

        $repair_info = $GLOBALS['db']->getRow($sql);
        $row['repair_id'] = $repair_info['repair_id'];
        $row['transfer_id'] = $repair_info['transfer_id'];
		
		$row['act_date']=local_date('Y-m-d <br/> H:i:s',$row['act_time']);
		
		// Goods Attr is not used
		//$row['goods_attr']=get_attr_info($row['goods_attr_id']);
		
		// Optimized to the main query
		//$warehouse_id=$row['warehouse_id'];
		//$sql=" select name from ".$GLOBALS['ecs']->table('erp_warehouse')." where warehouse_id='".$warehouse_id."'";
		//$row['warehouse_name']=$GLOBALS['db']->getOne($sql);
		
		$stock_style=$row['stock_style'];
		$w_d_style_id=$row['w_d_style_id'];
		if($w_d_style_id==0)
		{
			$row['w_d_style']=$GLOBALS['_LANG']['erp_stock_transfer_w_d_style'];
		}
		else
		{
			if($stock_style=='w')
			{
				$sql=" select warehousing_style from ".$GLOBALS['ecs']->table('erp_warehousing_style')." where warehousing_style_id ='".$w_d_style_id."'";
			}
			elseif($stock_style=='d')
			{
				$sql=" select delivery_style from ".$GLOBALS['ecs']->table('erp_delivery_style')." where delivery_style_id ='".$w_d_style_id."'";
			}
			$row['w_d_style']=$GLOBALS['db']->getOne($sql);
		}
		
		// Agency is not used
		//$sql="select agency_name from ".$GLOBALS['ecs']->table('agency')." where agency_id in (select agency_id from ".$GLOBALS['ecs']->table('erp_warehouse')." where warehouse_id=".$row['warehouse_id'].")";
		//$row['agency_name']=$GLOBALS['db']->getOne($sql);
		
		// Barcode is not used
		//$sql="select gb.barcode from ".$GLOBALS['ecs']->table('erp_goods_barcode')." as gb,".$GLOBALS['ecs']->table('erp_goods_attr')." as ga where ga.attr_id='".$row['goods_attr_id']."' and ga.barcode_id=gb.id limit 1";
		//$row['barcode']=$GLOBALS['db']->getOne($sql);
		$stock_record[]=$row;
	}
	return $stock_record;
}

function update_tb_stock_check($paras=array(),$start_time='',$end_time='')
{
	$where=" where 1 ";
	if(isset($paras['warehouse_id']) &&!empty($paras['warehouse_id']))
	{
		$where.=" and warehouse_id ='".$paras['warehouse_id']."'";
	}
	if(isset($paras['goods_sn']) &&!empty($paras['goods_sn']))
	{
		$where.=" and goods_id in (select goods_id from ".$GLOBALS['ecs']->table('goods')." where goods_sn like '%".$paras['goods_sn']."%')";
	}
	if(isset($paras['goods_name']) &&!empty($paras['goods_name']))
	{
		$where.=" and goods_id in (select goods_id from ".$GLOBALS['ecs']->table('goods')." where goods_name like '%".$paras['goods_name']."%')";
	}
	if(isset($paras['goods_barcode']) &&!empty($paras['goods_barcode']))
	{
		$where.=" and attr_id in (select attr_id from ".$GLOBALS['ecs']->table('erp_goods_attr')." as ga,".$GLOBALS['ecs']->table('erp_goods_barcode')." as gb where gb.barcode like '%".$paras['goods_barcode']."%' and gb.id=ga.barcode_id)";
	}
	if(isset($paras['supplier_id']) &&!empty($paras['supplier_id']))
	{
		$sql.=" and goods_id in (select goods_id from ".$GLOBALS['ecs']->table('erp_goods_supplier')." where supplier_id='".$paras['supplier_id']."')";
	}
	if(isset($paras['brand_id']) &&!empty($paras['brand_id']))
	{
		$sql.=" and goods_id in (select goods_id from ".$GLOBALS['ecs']->table('goods')." where brand_id='".$paras['brand_id']."')";
	}
	$where.=" and goods_id in (select goods_id from ".$GLOBALS['ecs']->table('goods')." where is_delete='0')";
	$sql="update ".$GLOBALS['ecs']->table('erp_goods_attr_stock')." set begin_qty=0,delivery_qty=0,warehousing_qty=0,end_qty=0".$where;
	$GLOBALS['db']->query($sql);
	$cls_date=new cls_date();
	$current_date=local_date('Y-m-d',gmtime());
	$first_date=$cls_date->get_first_day($current_date);
	$last_date=$cls_date->get_last_day($current_date);
	if(empty($end_time))
	{
		$end_time=$cls_date->date_to_stamp($last_date);
	}
	if(empty($start_time))
	{
		$start_time=$cls_date->date_to_stamp($first_date);;
	}
	$sql="select * from ".$GLOBALS['ecs']->table('erp_goods_attr_stock').$where;
	$res=$GLOBALS['db']->query($sql);
	while($row=mysql_fetch_assoc($res))
	{
		$sql="select stock from ".$GLOBALS['ecs']->table('erp_stock')." where goods_id='".$row['goods_id']."' and goods_attr_id='".$row['attr_id']."' and warehouse_id='".$row['warehouse_id']."' and act_time<".$start_time." order by stock_id desc limit 1";
		$begin_qty=$GLOBALS['db']->getOne($sql);
		$begin_qty=!empty($begin_qty)?$begin_qty : 0;
		$sql="update ".$GLOBALS['ecs']->table('erp_goods_attr_stock')." set begin_qty=".$begin_qty." where goods_id='".$row['goods_id']."' and attr_id='".$row['attr_id']."' and warehouse_id='".$row['warehouse_id']."'";
		$GLOBALS['db']->query($sql);
		$sql="select qty from ".$GLOBALS['ecs']->table('erp_stock')." where goods_id='".$row['goods_id']."' and goods_attr_id='".$row['attr_id']."' and warehouse_id='".$row['warehouse_id']."' and act_time>=".$start_time." and act_time<=".$end_time."";
		$res2=$GLOBALS['db']->query($sql);
		while($row2=mysql_fetch_assoc($res2))
		{
			if($row2['qty'] >0)
			{
				$sql="update ".$GLOBALS['ecs']->table('erp_goods_attr_stock')." set warehousing_qty=warehousing_qty+".$row2['qty']." where goods_id='".$row['goods_id']."' and attr_id='".$row['attr_id']."' and warehouse_id='".$row['warehouse_id']."'";
				$GLOBALS['db']->query($sql);
			}
			elseif($row2['qty'] <0)
			{
				$sql="update ".$GLOBALS['ecs']->table('erp_goods_attr_stock')." set delivery_qty=delivery_qty +".(0-$row2['qty'])." where goods_id='".$row['goods_id']."' and attr_id='".$row['attr_id']."' and warehouse_id='".$row['warehouse_id']."'";
				$GLOBALS['db']->query($sql);
			}
		}
		$sql="select stock from ".$GLOBALS['ecs']->table('erp_stock')." where goods_id='".$row['goods_id']."' and goods_attr_id='".$row['attr_id']."' and warehouse_id='".$row['warehouse_id']."' and act_time<=".$end_time." order by stock_id desc limit 1";
		$end_qty=$GLOBALS['db']->getOne($sql);
		$end_qty=!empty($end_qty)?$end_qty : 0;
		$sql="update ".$GLOBALS['ecs']->table('erp_goods_attr_stock')." set end_qty=".$end_qty." where goods_id='".$row['goods_id']."' and attr_id='".$row['attr_id']."' and warehouse_id='".$row['warehouse_id']."'";
		$GLOBALS['db']->query($sql);
	}
}
function get_stock_check_count($paras=array())
{
	$sql="select count(*) from ".$GLOBALS['ecs']->table('erp_goods_attr_stock')." where 1 ";
	if(isset($paras['warehouse_id']) &&!empty($paras['warehouse_id']))
	{
		$sql.=" and warehouse_id ='".$paras['warehouse_id']."'";
	}
	if(isset($paras['goods_sn']) &&!empty($paras['goods_sn']))
	{
		$sql.=" and goods_id in (select goods_id from ".$GLOBALS['ecs']->table('goods')." where goods_sn like '%".$paras['goods_sn']."%')";
	}
	if(isset($paras['goods_name']) &&!empty($paras['goods_name']))
	{
		$sql.=" and goods_id in (select goods_id from ".$GLOBALS['ecs']->table('goods')." where goods_name like '%".$paras['goods_name']."%')";
	}
	if(isset($paras['goods_barcode']) &&!empty($paras['goods_barcode']))
	{
		$sql.=" and attr_id in (select attr_id from ".$GLOBALS['ecs']->table('erp_goods_attr')." as ga,".$GLOBALS['ecs']->table('erp_goods_barcode')." as gb where gb.barcode like '%".$paras['goods_barcode']."%' and gb.id=ga.barcode_id)";
	}
	if(isset($paras['agency_id']) &&intval($paras['agency_id']) >0)
	{
		$sql.=" and warehouse_id in (select warehouse_id from ".$GLOBALS['ecs']->table('erp_warehouse')." where agency_id='".$paras['agency_id']."')";
	}
	if(isset($paras['supplier_id']) &&!empty($paras['supplier_id']))
	{
		$sql.=" and goods_id in (select goods_id from ".$GLOBALS['ecs']->table('erp_goods_supplier')." where supplier_id='".$paras['supplier_id']."')";
	}
	if(isset($paras['brand_id']) &&!empty($paras['brand_id']))
	{
		$sql.=" and goods_id in (select goods_id from ".$GLOBALS['ecs']->table('goods')." where brand_id='".$paras['brand_id']."')";
	}
	$sql.=" and goods_id in (select goods_id from ".$GLOBALS['ecs']->table('goods')." where is_delete='0')";
	return $GLOBALS['db']->getOne($sql);
}
function get_stock_check_list($paras=array(),$start=-1,$num=-1)
{
	$sql ="SELECT egas.*, ";
	$sql.="g.cat_id, g.goods_sn, g.goods_name, g.goods_thumb, g.goods_img, g.original_img, g.cost, ";
	$sql.="ew.name as warehouse_name, ew.description as warehouse_description, ew.is_valid as warehouse_is_valid, ew.is_on_sale as warehouse_is_on_sale ";
	$sql.="FROM ".$GLOBALS['ecs']->table('erp_goods_attr_stock')." as egas ";
	$sql.="LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON egas.goods_id = g.goods_id ";
	$sql.="LEFT JOIN " . $GLOBALS['ecs']->table('erp_warehouse') . " as ew ON egas.warehouse_id = ew.warehouse_id ";
	$sql.=" WHERE 1 ";
	if(isset($paras['warehouse_id']) &&!empty($paras['warehouse_id']))
	{
		$sql.=" AND egas.warehouse_id ='".$paras['warehouse_id']."'";
	}
	if(isset($paras['goods_sn']) &&!empty($paras['goods_sn']))
	{
		$sql.=" AND g.goods_sn LIKE '%".$paras['goods_sn']."%'";
	}
	if(isset($paras['goods_name']) &&!empty($paras['goods_name']))
	{
		$sql.=" AND g.goods_name LIKE '%".$paras['goods_name']."%'";
	}
	if(isset($paras['agency_id']) &&intval($paras['agency_id']) >0)
	{
		$sql.=" AND ew.agency_id='".$paras['agency_id']."'";
	}
	if(isset($paras['supplier_id']) &&!empty($paras['supplier_id']))
	{
		$sql.=" AND egas.goods_id IN (SELECT goods_id FROM ".$GLOBALS['ecs']->table('erp_goods_supplier')." WHERE supplier_id='".$paras['supplier_id']."')";
	}
	if(isset($paras['brand_id']) &&!empty($paras['brand_id']))
	{
		$sql.=" AND g.brand_id='".$paras['brand_id']."'";
	}
	$sql.=" AND g.is_delete = '0'";
	$sql.=" ORDER BY goods_id ASC";
	if($start!=-1 &&$num!=-1)
	{
		if($start<=0)
		{
			$start=0;
		}
		$sql.=" limit ".$start.",".$num;
	}
	//echo $sql;
	$stock_check_info=$GLOBALS['db']->getAll($sql);
	foreach($stock_check_info as $key=>$item)
	{
		$goods_id=$item['goods_id'];
		$warehouse_id=$item['warehouse_id'];
		$goods_attr_id=$item['attr_id'];
		
		// Goods_attr is not used
		// $attr_info=get_attr_info($goods_attr_id);
		// $stock_check_info[$key]['goods_attr']=isset($attr_info['attr_info'])?$attr_info:'';
		
		// $sql="select goods_id,cat_id,goods_sn,goods_name,goods_thumb,goods_img,original_img from ".$GLOBALS['ecs']->table('goods')." where goods_id='".$goods_id."'";
		// $goods_info=$GLOBALS['db']->getRow($sql);
		// if(!empty($goods_info['goods_thumb']))
		// {
		// 	$goods_info['goods_thumb']='../'.$goods_info['goods_thumb'];
		// }
		// if(!empty($goods_info['goods_img']))
		// {
		// 	$goods_info['goods_img']='../'.$goods_info['goods_img'];
		// }
		// if(!empty($goods_info['original_img']))
		// {
		// 	$goods_info['original_img']='../'.$goods_info['original_img'];
		// }
		// $stock_check_info[$key]['goods_info']=$goods_info;
		$stock_check_info[$key]['goods_info']=array(
			'goods_id' => $item['goods_id'],
			'cat_id' => $item['cat_id'],
			'goods_sn' => $item['goods_sn'],
			'goods_name' => $item['goods_name'],
			'goods_thumb' => (empty($item['goods_thumb']) ? '' : '../'.$item['goods_thumb']),
			'goods_img' => (empty($item['goods_img']) ? '' : '../'.$item['goods_img']),
			'original_img' => (empty($item['original_img']) ? '' : '../'.$item['original_img'])
		);
		
		// $sql="select * from ".$GLOBALS['ecs']->table('erp_warehouse')." where warehouse_id='".$warehouse_id."'";
		// $warehouse_info=$GLOBALS['db']->getRow($sql);
		// $stock_check_info[$key]['warehouse_info']=$warehouse_info;
		$stock_check_info[$key]['warehouse_info']=array(
			'warehouse_id' => $item['warehouse_id'],
			'name' => $item['warehouse_name'],
			'description' => $item['warehouse_description'],
			'is_valid' => $item['warehouse_is_valid'],
			'agency_id' => $item['agency_id'],
			'is_on_sale' => $item['warehouse_is_on_sale']
		);
		
		// Agency is not used
		// $sql="select agency_name from ".$GLOBALS['ecs']->table('agency')." where agency_id in (select agency_id from ".$GLOBALS['ecs']->table('erp_warehouse')." where warehouse_id=".$item['warehouse_id'].")";
		// $stock_check_info[$key]['agency_name']=$GLOBALS['db']->getOne($sql);
		
		// $sql="select cost from ".$GLOBALS['ecs']->table('goods')." where goods_id='".$goods_id."'";
		// $stock_check_info[$key]['cp_price']=round($GLOBALS['db']->getOne($sql),2);
		$stock_check_info[$key]['cp_price']=round($item['cost'],2);
		
		// Barcode is not used
		// $sql="select gb.barcode from ".$GLOBALS['ecs']->table('erp_goods_barcode')." as gb,".$GLOBALS['ecs']->table('erp_goods_attr')." as ga where ga.attr_id='".$item['attr_id']."' and ga.barcode_id=gb.id limit 1";
		// $stock_check_info[$key]['barcode']=$GLOBALS['db']->getOne($sql);
	}
	return $stock_check_info;
}
function get_erp_stock_count($paras=array())
{
	$sql="select count(distinct goods_id) from ".$GLOBALS['ecs']->table('erp_goods_attr_stock')." where 1 ";
	if(isset($paras['warehouse_id']) &&!empty($paras['warehouse_id']))
	{
		$sql.=" and warehouse_id='".$paras['warehouse_id']."' ";
	}
	if(isset($paras['goods_id']) &&!is_array($paras['goods_id']) &&$paras['goods_id'] >0)
	{
		$sql.=" and goods_id='".$paras['goods_id']."' ";
	}
	if(isset($paras['goods_id']) &&is_array($paras['goods_id']) &&!empty($paras['goods_id']))
	{
		$sql.=" and goods_id in (".implode(',',$paras['goods_id']).")";
	}
	if(isset($paras['goods_sn']) &&!empty($paras['goods_sn']))
	{
		$sql.=" and goods_id in (select goods_id from ".$GLOBALS['ecs']->table('goods')." where goods_sn like '%".$paras['goods_sn']."%')";
	}
	if(isset($paras['goods_name']) &&!empty($paras['goods_name']))
	{
		$sql.=" and goods_id in (select goods_id from ".$GLOBALS['ecs']->table('goods')." where goods_name like '%".$paras['goods_name']."%')";
	}
	if(isset($paras['goods_barcode']) &&!empty($paras['goods_barcode']))
	{
		$sql.=" and attr_id in (select attr_id from ".$GLOBALS['ecs']->table('erp_goods_attr')." as ga,".$GLOBALS['ecs']->table('erp_goods_barcode')." as gb where gb.barcode like '%".$paras['goods_barcode']."%' and gb.id=ga.barcode_id)";
	}
	if(isset($paras['agency_id']) &&intval($paras['agency_id']) >0)
	{
		$sql.=" and warehouse_id in (select warehouse_id from ".$GLOBALS['ecs']->table('erp_warehouse')." where agency_id='".$paras['agency_id']."')";
	}
	if(isset($paras['cat_id']) &&is_array($paras['cat_id']) &&!empty($paras['cat_id']))
	{
		$sql.=" and goods_id in (select goods_id from ".$GLOBALS['ecs']->table('goods')." where cat_id in (".implode(',',$paras['cat_id'])."))";
	}
	if(isset($paras['cat_id']) &&!is_array($paras['cat_id']) &&intval($paras['cat_id']) >0)
	{
		$sql.=" and goods_id in (select goods_id from ".$GLOBALS['ecs']->table('goods')." where cat_id='".$paras['cat_id']."')";
	}
	if(isset($paras['brand_id']) &&is_array($paras['brand_id']) &&!empty($paras['brand_id']))
	{
		$sql.=" and goods_id in (select goods_id from ".$GLOBALS['ecs']->table('goods')." where brand_id in (".implode(',',$paras['brand_id'])."))";
	}
	if(isset($paras['brand_id']) &&!is_array($paras['brand_id']) &&intval($paras['brand_id']) >0)
	{
		$sql.=" and goods_id in (select goods_id from ".$GLOBALS['ecs']->table('goods')." where brand_id='".$paras['brand_id']."')";
	}
	if(isset($paras['supplier_id']) &&!is_array($paras['supplier_id']) &&intval($paras['supplier_id']) >0)
	{
		$sql.=" AND goods_id IN (SELECT goods_id FROM ".$GLOBALS['ecs']->table('erp_goods_supplier')." WHERE supplier_id=".$paras['supplier_id'].") ";
	}
	$sql.=" and goods_id in (select goods_id from ".$GLOBALS['ecs']->table('goods')." where is_delete='0')";
	return $GLOBALS['db']->getOne($sql);
}
function get_erp_stock($paras=array(),$start=-1,$num=-1)
{
	require_once(ROOT_PATH . '/includes/lib_howang.php');
	require_once(ROOT_PATH . '/includes/lib_order.php');
	//1)prepare subquery join
	$erpController = new Yoho\cms\Controller\ErpController();
	$query = "SELECT warehouse_id FROM ".$GLOBALS['ecs']->table('erp_warehouse')." ORDER BY warehouse_id ASC";
	$warehouse_list = get_warehouse_list(0, $agency_id, 1,-1,-1,'asc');
	$query=[];
	foreach($warehouse_list as $w){
		$query[0][]="egas_".$w['warehouse_id'].".goods_qty as w".$w['warehouse_id']."_qty";
		$query[1][]="LEFT JOIN ".$GLOBALS['ecs']->table('erp_goods_attr_stock')." egas_".$w['warehouse_id']." ON egas_".$w['warehouse_id'].".goods_id=g.goods_id AND egas_".$w['warehouse_id'].".warehouse_id=".$w['warehouse_id']." ";
	}

	$sql="SELECT g.goods_id, g.goods_name, g.goods_thumb, g.goods_img, g.goods_sn, g.original_img, sum(egas.goods_qty) as sum_goods_qty ,".\hw_reserved_number_subquery('egas.').",egas.attr_id," .implode(',', $query[0]).
		" FROM " . $GLOBALS['ecs']->table('erp_goods_attr_stock') . " as egas " .
		" LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON egas.goods_id = g.goods_id " .
		" LEFT JOIN " . $GLOBALS['ecs']->table('erp_warehouse') . " as ew ON egas.warehouse_id = ew.warehouse_id " .
		implode('', $query[1]).
		" WHERE 1 ";

	/*if(isset($paras['warehouse_id']) &&!empty($paras['warehouse_id']))
	{
		$sql.=" AND egas.warehouse_id = '".$paras['warehouse_id']."' ";
	}*/
	if(isset($paras['goods_id']) &&!is_array($paras['goods_id']) &&$paras['goods_id'] >0)
	{
		$sql.=" AND egas.goods_id = '".$paras['goods_id']."' ";
	}
	if(isset($paras['goods_id']) &&is_array($paras['goods_id']) &&!empty($paras['goods_id']))
	{
		$sql.=" AND egas.goods_id IN (".implode(',',$paras['goods_id']).") ";
	}
	if(isset($paras['goods_sn']) &&!empty($paras['goods_sn']))
	{
		$sql.=" AND g.goods_sn LIKE '%".$paras['goods_sn']."%' ";
	}
	if(isset($paras['goods_name']) &&!empty($paras['goods_name']))
	{
		$sql.=" AND g.goods_name LIKE '%".$paras['goods_name']."%' ";
	}
	if(isset($paras['agency_id']) &&intval($paras['agency_id']) >0)
	{
		$sql.=" AND ew.agency_id = '".$paras['agency_id']."' ";
	}
	if(isset($paras['cat_id']) &&is_array($paras['cat_id']) &&!empty($paras['cat_id']))
	{
		$sql.=" AND g.cat_id IN (".implode(',',$paras['cat_id']).") ";
	}
	if(isset($paras['cat_id']) &&!is_array($paras['cat_id']) &&intval($paras['cat_id']) >0)
	{
		$sql.=" AND g.cat_id = '".$paras['cat_id']."' ";
	}
	if(isset($paras['brand_id']) &&is_array($paras['brand_id']) &&!empty($paras['brand_id']))
	{
		$sql.=" AND g.brand_id IN (".implode(',',$paras['brand_id']).") ";
	}
	if(isset($paras['brand_id']) &&!is_array($paras['brand_id']) &&intval($paras['brand_id']) >0)
	{
		$sql.=" AND g.brand_id = '".$paras['brand_id']."')";
	}
	if(isset($paras['supplier_id']) &&!is_array($paras['supplier_id']) &&intval($paras['supplier_id']) >0)
	{
		$sql.=" AND g.goods_id IN (SELECT goods_id FROM ".$GLOBALS['ecs']->table('erp_goods_supplier')." WHERE supplier_id=".$paras['supplier_id'].") ";
	}
	$sql.=" AND g.is_delete = '0' ";
	$sql.=" GROUP BY egas.`goods_id` ORDER BY ".(isset($paras['orderby'])?'w'.$paras['orderby'].'_qty':'sum_goods_qty')." ".(isset($paras['order'])?$paras['order']:'DESC');
	if($start!=-1 &&$num!=-1)
	{
		if($start<=0)
		{
			$start=0;
		}
		$sql.=" LIMIT ".$start.",".$num;
	}
	$res = $GLOBALS['db']->getAll($sql);
	foreach ($res as $row)
	{
		$goods_id = $row['goods_id'];
		$goods_info = array_intersect_key($row, array_flip(array('goods_id', 'cat_id', 'goods_sn', 'goods_name', 'goods_thumb', 'goods_img', 'original_img','attr_id')));
		if(!empty($goods_info['goods_thumb']))
		{
			$goods_info['goods_thumb']='../'.$goods_info['goods_thumb'];
		}
		if(!empty($goods_info['goods_img']))
		{
			$goods_info['goods_img']='../'.$goods_info['goods_img'];
		}
		if(!empty($goods_info['original_img']))
		{
			$goods_info['original_img']='../'.$goods_info['original_img'];
		}
		//loop through inventory
		$stock_info[$goods_id]['stock']=[];
		$sellableSum=0;
		foreach($row as $k=>$v){
			preg_match("/w(\d*)_qty/", $k, $ids);
			if($ids[1]>0){
				$stock_info[$goods_id]['stock'][$ids[1]]=$v;

				foreach($warehouse_list as $w){
					if($w['warehouse_id']==$ids[1] && $w['is_on_sale']){
						$sellableSum+=$v;
						break;
					}
				}
			}
		}
		$stock_info[$goods_id]['sellableSum'] = $sellableSum-$row['reserved_number'];
		$stock_info[$goods_id]['goods_info']=$goods_info;
		/*
		$sql="SELECT egas.`warehouse_id`, ew.`name`, ew.is_on_sale, egas.`attr_id`, egas.`goods_qty`, a.`agency_name` " .
			" FROM " . $GLOBALS['ecs']->table('erp_goods_attr_stock') . " as egas " .
			" LEFT JOIN " . $GLOBALS['ecs']->table('erp_warehouse') . " as ew ON egas.warehouse_id = ew.warehouse_id " .
			" LEFT JOIN " . $GLOBALS['ecs']->table('agency') . " as a ON a.agency_id = ew.agency_id " .
			" WHERE egas.goods_id = '".$goods_id."'";
		if(isset($paras['warehouse_id']) && !empty($paras['warehouse_id']))
		{
			$sql.=" AND egas.warehouse_id = '".$paras['warehouse_id']."' ";
		}
		if(isset($paras['agency_id']) && intval($paras['agency_id']) > 0)
		{
			$sql.=" AND ew.agency_id = '".$paras['agency_id']."' ";
		}
		$sql.=" GROUP BY egas.`warehouse_id`, egas.`attr_id`, egas.`goods_qty` ";
		$res2 = $GLOBALS['db']->query($sql);
		while(list($warehouse_id, $warehouse_name, $is_on_sale, $goods_attr_id, $goods_qty, $agency_name)=mysql_fetch_row($res2))
		{
			$stock_info[$goods_id]['goods_info']=$goods_info;
			$stock_info[$goods_id]['warehouse'][$warehouse_id]['warehouse_info']['warehouse_id']=$warehouse_id;
			$stock_info[$goods_id]['warehouse'][$warehouse_id]['warehouse_info']['warehouse_name']=$warehouse_name;
			$stock_info[$goods_id]['warehouse'][$warehouse_id]['warehouse_info']['is_on_sale']=$is_on_sale;
			$stock_info[$goods_id]['warehouse'][$warehouse_id]['warehouse_info']['agency_name']=$agency_name;
			$attr_info['goods_attr_id']=$goods_attr_id;
			$attr_info['qty']=$goods_qty;
			$attr_info['qty']=$goods_qty;
			$attr=get_attr_info($goods_attr_id);
			$attr_info['attr_value']=isset($attr['attr_info'][0]) &&!empty($attr['attr_info'][0]) ?$attr:'';
			$stock_info[$goods_id]['warehouse'][$warehouse_id]['attr'][]=$attr_info;
		}*/
	}
	return $stock_info;
}
function get_erp_stock2($paras=array(),$start=-1,$num=-1)
{
	$sql="select distinct goods_id from ".$GLOBALS['ecs']->table('erp_goods_attr_stock')." where 1 ";
	if(isset($paras['warehouse_id']) &&!empty($paras['warehouse_id']))
	{
		$sql.=" and warehouse_id='".$paras['warehouse_id']."' ";
	}
	if(isset($paras['goods_id']) &&!is_array($paras['goods_id']) &&$paras['goods_id'] >0)
	{
		$sql.=" and goods_id='".$paras['goods_id']."' ";
	}
	if(isset($paras['goods_id']) &&is_array($paras['goods_id']) &&!empty($paras['goods_id']))
	{
		$sql.=" and goods_id in (".implode(',',$paras['goods_id']).")";
	}
	if(isset($paras['goods_sn']) &&!empty($paras['goods_sn']))
	{
		$sql.=" and goods_id in (select goods_id from ".$GLOBALS['ecs']->table('goods')." where goods_sn like '%".$paras['goods_sn']."%')";
	}
	if(isset($paras['goods_name']) &&!empty($paras['goods_name']))
	{
		$sql.=" and goods_id in (select goods_id from ".$GLOBALS['ecs']->table('goods')." where goods_name like '%".$paras['goods_name']."%')";
	}
	if(isset($paras['goods_barcode']) &&!empty($paras['goods_barcode']))
	{
		$sql.=" and attr_id in (select attr_id from ".$GLOBALS['ecs']->table('erp_goods_attr')." as ga,".$GLOBALS['ecs']->table('erp_goods_barcode')." as gb where gb.barcode like '%".$paras['goods_barcode']."%' and gb.id=ga.barcode_id)";
	}
	if(isset($paras['agency_id']) &&intval($paras['agency_id']) >0)
	{
		$sql.=" and warehouse_id in (select warehouse_id from ".$GLOBALS['ecs']->table('erp_warehouse')." where agency_id='".$paras['agency_id']."')";
	}
	if(isset($paras['cat_id']) &&is_array($paras['cat_id']) &&!empty($paras['cat_id']))
	{
		$sql.=" and goods_id in (select goods_id from ".$GLOBALS['ecs']->table('goods')." where cat_id in (".implode(',',$paras['cat_id'])."))";
	}
	if(isset($paras['cat_id']) &&!is_array($paras['cat_id']) &&intval($paras['cat_id']) >0)
	{
		$sql.=" and goods_id in (select goods_id from ".$GLOBALS['ecs']->table('goods')." where cat_id='".$paras['cat_id']."')";
	}
	if(isset($paras['brand_id']) &&is_array($paras['brand_id']) &&!empty($paras['brand_id']))
	{
		$sql.=" and goods_id in (select goods_id from ".$GLOBALS['ecs']->table('goods')." where brand_id in (".implode(',',$paras['brand_id'])."))";
	}
	if(isset($paras['brand_id']) &&!is_array($paras['brand_id']) &&intval($paras['brand_id']) >0)
	{
		$sql.=" and goods_id in (select goods_id from ".$GLOBALS['ecs']->table('goods')." where brand_id='".$paras['brand_id']."')";
	}
	$sql.=" and goods_id in (select goods_id from ".$GLOBALS['ecs']->table('goods')." where is_delete='0')";
	if($start!=-1 &&$num!=-1)
	{
		if($start<=0)
		{
			$start=0;
		}
		$sql.=" limit ".$start.",".$num;
	}
	$res=$GLOBALS['db']->query($sql);
	$stock_info=array();
	$cache_goods_id=0;
	$cache_warehouse_id=0;
	$cache_attr_id=0;
	while(list($goods_id)=mysql_fetch_row($res))
	{
		if($goods_id!=$cache_goods_id)
		{
			$cache_goods_id=$goods_id;
			$sql="select goods_id,cat_id,goods_sn,goods_name,goods_thumb,goods_img,original_img from ".$GLOBALS['ecs']->table('goods')." where goods_id='".$goods_id."'";
			$goods_info=$GLOBALS['db']->getRow($sql);
			if(!empty($goods_info['goods_thumb']))
			{
				$goods_info['goods_thumb']='../'.$goods_info['goods_thumb'];
			}
			if(!empty($goods_info['goods_img']))
			{
				$goods_info['goods_img']='../'.$goods_info['goods_img'];
			}
			if(!empty($goods_info['original_img']))
			{
				$goods_info['original_img']='../'.$goods_info['original_img'];
			}
		}
		$sql="select id,attr_id,goods_id,warehouse_id,goods_qty from ".$GLOBALS['ecs']->table('erp_goods_attr_stock')." where goods_id='".$goods_id."'";
		if(isset($paras['goods_barcode']) &&!empty($paras['goods_barcode']))
		{
			$sql.=" and attr_id in (select attr_id from ".$GLOBALS['ecs']->table('erp_goods_attr')." as ga,".$GLOBALS['ecs']->table('erp_goods_barcode')." as gb where gb.barcode like '%".$paras['goods_barcode']."%' and gb.id=ga.barcode_id)";
		}
		if(isset($paras['warehouse_id']) &&!empty($paras['warehouse_id']))
		{
			$sql.=" and warehouse_id='".$paras['warehouse_id']."' ";
		}
		if(isset($paras['agency_id']) &&intval($paras['agency_id']) >0)
		{
			$sql.=" and warehouse_id in (select warehouse_id from ".$GLOBALS['ecs']->table('erp_warehouse')." where agency_id='".$paras['agency_id']."')";
		}
		$sql.=" order by warehouse_id";
		$res2=$GLOBALS['db']->query($sql);
		while($row=mysql_fetch_assoc($res2))
		{
			$row['goods_info']=$goods_info;
			if($row['warehouse_id']!=$cache_warehouse_id)
			{
				$cache_warehouse_id=$row['warehouse_id'];
				$sql="select name from ".$GLOBALS['ecs']->table('erp_warehouse')." where warehouse_id='".$row['warehouse_id']."'";
				$warehouse_name=$GLOBALS['db']->getOne($sql);
				$sql="select agency_name from ".$GLOBALS['ecs']->table('agency')." where agency_id in (select agency_id from ".$GLOBALS['ecs']->table('erp_warehouse')." where warehouse_id=".$row['warehouse_id'].")";
				$agency_name=$GLOBALS['db']->getOne($sql);
			}
			$row['warehouse_name']=	$warehouse_name;
			$row['agency_name']= $agency_name;
			if($row['attr_id']!=$cache_attr_id)
			{
				$cache_attr_id=$row['attr_id'];
				$attr=get_attr_info($row['attr_id']);
			}
			$row['attr_value']=isset($attr['attr_info'][0]) &&!empty($attr['attr_info'][0]) ?$attr:'';
			$sql="select gb.barcode from ".$GLOBALS['ecs']->table('erp_goods_barcode')." as gb,".$GLOBALS['ecs']->table('erp_goods_attr')." as ga where ga.attr_id='".$row['attr_id']."' and ga.barcode_id=gb.id limit 1";
			$row['barcode']=$GLOBALS['db']->getOne($sql);
			$stock_info[]=$row;
		}
	}
	return $stock_info;
}
function is_delivery_style_exist($delivery_style_id='')
{
	if(empty($delivery_style_id))
	{
		return false;
	}
	else
	{
		$sql="select * from ".$GLOBALS['ecs']->table('erp_delivery_style')." where delivery_style_id='".$delivery_style_id."'";
		$result=$GLOBALS['db']->getRow($sql);
		return !empty($result) ?$result : false;
	}
}
function is_agency_delivery($delivery_id,$agency_id=0)
{
	if(empty($agency_id))
	{
		return true;
	}
	$sql="select warehouse_id from ".$GLOBALS['ecs']->table('erp_delivery')." where delivery_id='".$delivery_id."' and agency_id='".$agency_id."'";
	$warehouse_id=$GLOBALS['db']->getOne($sql);
	if($warehouse_id>0)
	{
		$sql="select * from ".$GLOBALS['ecs']->table('erp_warehouse')." where agency_id='".$agency_id."' and warehouse_id='".$warehouse_id."'";
		$result=$GLOBALS['db']->getRow($sql);
		return !empty($result)?true : false;
	}
	else
	{
		return false;
	}
}
function is_agency_warehousing($warehousing_id,$agency_id=0)
{
	if(empty($agency_id))
	{
		return true;
	}
	$sql="select warehouse_id from ".$GLOBALS['ecs']->table('erp_warehousing')." where warehousing_id='".$warehousing_id."' and agency_id='".$agency_id."'";
	$warehouse_id=$GLOBALS['db']->getOne($sql);
	if($warehouse_id>0)
	{
		$sql="select * from ".$GLOBALS['ecs']->table('erp_warehouse')." where agency_id='".$agency_id."' and warehouse_id='".$warehouse_id."'";
		$result=$GLOBALS['db']->getRow($sql);
		return !empty($result)?true : false;
	}
	else
	{
		return false;
	}
}
function is_agency_transfer($transfer_id,$agency_id=0)
{
	if(empty($agency_id))
	{
		return true;
	}
	$sql="select warehouse_from,warehouse_to from ".$GLOBALS['ecs']->table('erp_stock_transfer')." where transfer_id='".$transfer_id."'";
	$transfer=$GLOBALS['db']->getRow($sql);
	if(!empty($transfer))
	{
		$sql="select * from ".$GLOBALS['ecs']->table('erp_warehouse')." where agency_id='".$agency_id."' and warehouse_id in (".$transfer['warehouse_from'].",".$transfer['warehouse_to'].")";
		$result=$GLOBALS['db']->getRow($sql);
		return !empty($result)?true : false;
	}
	else
	{
		return false;
	}
}
function get_delivery_count($paras=array(),$start=-1,$num=-1)
{
	$sql="select count(*) from ".$GLOBALS['ecs']->table('erp_delivery')." where 1";
	if(isset($paras['status']) &&!empty($paras['status']))
	{
		$sql.=" and status='".$paras['status']."' ";
	}
	if(isset($paras['start_time']) &&!empty($paras['start_time']))
	{
		$sql.=" and create_time>='".$paras['start_time']."'";
	}
	if(isset($paras['end_time']) &&!empty($paras['end_time']))
	{
		$sql.=" and create_time<='".$paras['end_time']."'";
	}
	if(isset($paras['delivery_style_id']) &&!empty($paras['delivery_style_id']))
	{
		$sql.=" and delivery_style_id='".$paras['delivery_style_id']."'";
	}
	if(isset($paras['warehouse_id']) &&!empty($paras['warehouse_id']))
	{
		$sql.=" and warehouse_id='".$paras['warehouse_id']."'";
	}
	if(isset($paras['delivery_to']) &&!empty($paras['delivery_to']))
	{
		$sql.=" and delivery_to='".$paras['delivery_to']."'";
	}
	if(isset($paras['order_id']) &&!empty($paras['order_id']))
	{
		$sql.=" and order_id='".$paras['order_id']."'";
	}
	if(isset($paras['delivery_sn']) &&!empty($paras['delivery_sn']))
	{
		$sql.=" and delivery_sn like '%".$paras['delivery_sn']."%'";
	}
	if(isset($paras['agency_id']) &&intval($paras['agency_id']) >0)
	{
		$sql.=" and warehouse_id in (select warehouse_id from ".$GLOBALS['ecs']->table('erp_warehouse')." where agency_id='".$paras['agency_id']."')";
		$sql.=" and agency_id='".$paras['agency_id']."'";
	}
	return $GLOBALS['db']->getOne($sql);
}
function get_delivery_list($paras=array(),$start=-1,$num=-1)
{
	$sql="SELECT ed.*, ".
		"(SELECT user_name FROM ".$GLOBALS['ecs']->table('admin_user')." WHERE user_id=ed.create_by) as admin_name, ".
		"(SELECT user_name FROM ".$GLOBALS['ecs']->table('admin_user')." WHERE user_id=ed.post_by) as post_by_name, ".
		"(SELECT user_name FROM ".$GLOBALS['ecs']->table('admin_user')." WHERE user_id=ed.approved_by) as approved_by_name, ".
		"ew.name as warehouse_name, ew.description as warehouse_description, ew.is_valid as warehouse_is_valid, ew.agency_id as warehouse_agency_id, ew.is_on_sale as warehouse_is_on_sale, ".
		"eds.delivery_style as delivery_style_name, eds.is_valid as delivery_style_is_valid, ".
		"oi.order_sn, oi.user_id as order_user_id ".
		" FROM " . $GLOBALS['ecs']->table('erp_delivery') . "as ed ".
		" LEFT JOIN " . $GLOBALS['ecs']->table('erp_warehouse') . " as ew ON ed.warehouse_id = ew.warehouse_id ".
		" LEFT JOIN " . $GLOBALS['ecs']->table('erp_delivery_style') . " as eds ON ed.delivery_style_id = eds.delivery_style_id ".
		" LEFT JOIN " . $GLOBALS['ecs']->table('order_info') . " as oi ON ed.order_id = oi.order_id ".
		" WHERE 1";
	if(isset($paras['status']) &&!empty($paras['status']))
	{
		$sql.=" AND ed.status='".$paras['status']."' ";
	}
	if(isset($paras['start_time']) &&!empty($paras['start_time']))
	{
		$sql.=" AND ed.create_time>='".$paras['start_time']."'";
	}
	if(isset($paras['end_time']) &&!empty($paras['end_time']))
	{
		$sql.=" AND ed.create_time<='".$paras['end_time']."'";
	}
	if(isset($paras['delivery_style_id']) &&!empty($paras['delivery_style_id']))
	{
		$sql.=" AND ed.delivery_style_id='".$paras['delivery_style_id']."'";
	}
	if(isset($paras['warehouse_id']) &&!empty($paras['warehouse_id']))
	{
		$sql.=" AND ed.warehouse_id='".$paras['warehouse_id']."'";
	}
	if(isset($paras['delivery_to']) &&!empty($paras['delivery_to']))
	{
		$sql.=" AND ed.delivery_to LIKE '%".$paras['delivery_to']."%'";
	}
	if(isset($paras['order_id']) &&!empty($paras['order_id']))
	{
		$sql.=" AND ed.order_id='".$paras['order_id']."'";
	}
	if(isset($paras['delivery_sn']) &&!empty($paras['delivery_sn']))
	{
		$sql.=" AND ed.delivery_sn LIKE '%".$paras['delivery_sn']."%'";
	}
	if(isset($paras['agency_id']) &&intval($paras['agency_id']) >0)
	{
		$sql.=" AND ed.warehouse_id in (SELECT warehouse_id FROM ".$GLOBALS['ecs']->table('erp_warehouse')." WHERE agency_id='".$paras['agency_id']."')";
		$sql.=" AND agency_id='".$paras['agency_id']."'";
	}
	$sql.=" ORDER BY delivery_id DESC";
	if($start!=-1 &&$num!=-1)
	{
		if($start<=0)
		{
			$start=0;
		}
		$sql.=" LIMIT ".$start.",".$num;
	}
	$res=$GLOBALS['db']->query($sql);
	while($row=mysql_fetch_assoc($res))
	{
		$create_time=$row['create_time'];
		$row['delivery_date']=local_date('Y-m-d',$create_time);
		$delivery_id=$row['delivery_id'];
		//$row['available_act']=get_available_act($delivery_id,'delivery');
		$row['available_act']=get_available_act($row,'delivery');
		
		$delivery_style_id=$row['delivery_style_id'];
		// $sql="select * from ".$GLOBALS['ecs']->table('erp_delivery_style')." where delivery_style_id='".$delivery_style_id."'";
		// $row['delivery_style_info']=$GLOBALS['db']->getRow($sql);
		$row['delivery_style_info']=array(
			'delivery_style_id' => $delivery_style_id,
			'delivery_style' => $row['delivery_style_name'],
			'is_valid' => $row['delivery_style_is_valid']
		);
		
		// $warehouse_id=$row['warehouse_id'];
		// $sql="select * from ".$GLOBALS['ecs']->table('erp_warehouse')." where warehouse_id='".$warehouse_id."'";
		// $row['warehouse_info']=$GLOBALS['db']->getRow($sql);
		$row['warehouse_info']=array(
			'warehouse_id' => $row['warehouse_id'],
			'name' => $row['warehouse_name'],
			'description' => $row['warehouse_description'],
			'is_valid' => $row['warehouse_is_valid'],
			'agency_id' => $row['warehouse_agency_id'],
			'is_on_sale' => $row['warehouse_is_on_sale']
		);
		
		// $operator_id=$row['create_by'];
		// $sql="select user_name from ".$GLOBALS['ecs']->table('admin_user')." where user_id='".$operator_id."'";
		// $row['admin_name']=$GLOBALS['db']->getOne($sql);
		// if(!empty($row['post_by']))
		// {
		// 	$post_by=$row['post_by'];
		// 	$sql="select user_name from ".$GLOBALS['ecs']->table('admin_user')." where user_id='".$post_by."'";
		// 	$row['post_by_name']=$GLOBALS['db']->getOne($sql);
		// }
		// if(!empty($row['approved_by']))
		// {
		// 	$approved_by=$row['approved_by'];
		// 	$sql="select user_name from ".$GLOBALS['ecs']->table('admin_user')." where user_id='".$approved_by."'";
		// 	$row['approved_by_name']=$GLOBALS['db']->getOne($sql);
		// }
		
		if(!empty($row['order_id']))
		{
			// $order_id=$row['order_id'];
			// $sql="select order_id,order_sn,user_id from ".$GLOBALS['ecs']->table('order_info')." where order_id='".$order_id."'";
			// $row['order_info']=$GLOBALS['db']->getRow($sql);
			$row['order_info'] = array(
				'order_id' => $row['order_id'],
				'order_sn' => $row['order_sn'],
				'user_id' => $row['order_user_id']
			);
		}
		
		// Agency is not used
		// $sql="select agency_name from ".$GLOBALS['ecs']->table('agency')." where agency_id='".$row['agency_id']."'";
		// $row['agency_name']=$GLOBALS['db']->getOne($sql);
		
		$delivery_info[]=$row;
	}
	return $delivery_info;
}
function get_delivery_info($delivery_id)
{
	$sql="select * from ".$GLOBALS['ecs']->table('erp_delivery')." where delivery_id='".$delivery_id."'";
	$delivery_info=$GLOBALS['db']->getRow($sql);
	$create_time=$delivery_info['create_time'];
	$delivery_info['delivery_date']=local_date('Y-m-d',$create_time);
	$delivery_id=$delivery_info['delivery_id'];
	//$delivery_info['available_act']=get_available_act($delivery_id,'delivery');
	$delivery_info['available_act']=get_available_act($delivery_info,'delivery');
	$delivery_style_id=$delivery_info['delivery_style_id'];
	$sql="select * from ".$GLOBALS['ecs']->table('erp_delivery_style')." where delivery_style_id='".$delivery_style_id."'";
	$delivery_info['delivery_style_info']=$GLOBALS['db']->getRow($sql);
	$warehouse_id=$delivery_info['warehouse_id'];
	$sql="select * from ".$GLOBALS['ecs']->table('erp_warehouse')." where warehouse_id='".$warehouse_id."'";
	$delivery_info['warehouse_info']=$GLOBALS['db']->getRow($sql);
	$operator_id=$delivery_info['create_by'];
	$sql="select user_name from ".$GLOBALS['ecs']->table('admin_user')." where user_id='".$operator_id."'";
	$delivery_info['admin_name']=$GLOBALS['db']->getOne($sql);
	if(!empty($delivery_info['post_by']))
	{
		$post_by=$delivery_info['post_by'];
		$sql="select user_name from ".$GLOBALS['ecs']->table('admin_user')." where user_id='".$post_by."'";
		$delivery_info['post_by_name']=$GLOBALS['db']->getOne($sql);
	}
	if(!empty($delivery_info['approved_by']))
	{
		$approved_by=$delivery_info['approved_by'];
		$sql="select user_name from ".$GLOBALS['ecs']->table('admin_user')." where user_id='".$approved_by."'";
		$delivery_info['approved_by_name']=$GLOBALS['db']->getOne($sql);
	}
	if(!empty($delivery_info['order_id']))
	{
		$order_id=$delivery_info['order_id'];
		$sql="select order_id,order_sn,user_id from ".$GLOBALS['ecs']->table('order_info')." where order_id='".$order_id."'";
		$delivery_info['order_info']=$GLOBALS['db']->getRow($sql);
	}
	return $delivery_info;
}
function get_cat_info($cat_id='')
{
	if(empty($cat_id))
	{
		$sql="select * from ".$GLOBALS['ecs']->table('category')." where 1";
		return $GLOBALS['db']->getAll($sql);
	}
	else
	{
		$sql="select * from ".$GLOBALS['ecs']->table('category')." where cat_id='".$cat_id."'";
		return $GLOBALS['db']->getAll($sql);
	}
}
function check_barcode($barcode='')
{
	if(empty($barcode))
	{
		return 1;
	}
	else
	{
		$barcode=trim($barcode);
		$first_char=substr($barcode,0,1);
		$last_char=substr($barcode,-1,1);
		if($first_char!='$')
		{
			return 1;
		}
		if($last_char!='*')
		{
			return 1;
		}
		$pos=strripos($barcode,'-');
		if($pos==false ||$pos==1)
		{
			return 1;
		}
		else
		{
			$goods_sn=substr($barcode,1,$pos-1);
			$goods_size=substr($barcode,$pos+1,strlen($barcode)-$pos-2);
			if(empty($goods_size))
			{
				return 1;
			}
		}
		if(!check_goods_sn($goods_sn))
		{
			return 2;
		}
		if(!check_goods_size('',$goods_sn,$goods_size))
		{
			return 3;
		}
		$goods_info['goods_sn']=$goods_sn;
		$goods_info['goods_size']=$goods_size;
		return $goods_info;
	}
}
function check_file($souce_file)
{
	if(empty($souce_file))
	{
		return 1;
	}
	else
	{
		$souce_file=str_replace("\\","/",$souce_file);
		if(filesize($souce_file)==0)
		{
			return 1;
		}
		$file_handle=fopen($souce_file,'r');
		$contents = fread($file_handle,filesize($souce_file));
		$contents=preg_replace("/([\r\n]+)/",'|',$contents);
		$contents_array=explode('|',$contents);
		foreach($contents_array as $key=>$array_item)
		{
			if(!empty($array_item))
			{
				$array_item=explode(',',$array_item);
				if(count($array_item)!=2)
				{
					return 1;
				}
				else
				{
					$qty=$array_item[1];
					if(!ereg('^[0-9]*[1-9][0-9]*$',$qty))
					{
						return 1;
					}
					$barcode=$array_item[0];
					$goods_info=check_barcode($barcode);
					if(!is_array($goods_info))
					{
						return $goods_info;
					}
					$warehousing_info[$key]['goods_info']=$goods_info;
					$warehousing_info[$key]['qty']=$qty;
				}
			}
		}
		return $warehousing_info;
	}
}
function format_file($souce_file)
{
	$file_ext = 'txt';
	$file_name=get_current_date().'-'.mt_rand(0,10000).'.'.$file_ext;
	$dir='./upload_files/batch_warehousing/';
	if (!make_dir($dir))
	{
		return false;
	}
	if(!move_uploaded_file($souce_file,$dir.$file_name))
	{
		return false;
	}
	else
	{
		return $dir.$file_name;
	}
}
function check_delivery_integrity($delivery_id='')
{
	if(empty($delivery_id))
	{
		return false;
	}
	$sql="select delivery_style_id,delivery_to,order_id from ".$GLOBALS['ecs']->table('erp_delivery')." where delivery_id='".$delivery_id."'";
	$res=$GLOBALS['db']->getRow($sql);
	$delivery_style_id=$res['delivery_style_id'];
	$delivery_to=$res['delivery_to'];
	$order_id=$res['order_id'];
	if($delivery_style_id==1)
	{
		if(empty($order_id))
		{
			return 1;
		}
	}
	if(empty($delivery_to))
	{
		return 2;
	}
	$sql="select delivery_item_id,delivered_qty,attr_id from ".$GLOBALS['ecs']->table('erp_delivery_item')." where delivery_id=".$delivery_id."";
	$delivery_item_info=$GLOBALS['db']->getAll($sql);
	if(empty($delivery_item_info))
	{
		return 3;
	}
	else
	{
		foreach($delivery_item_info as $delivery_item)
		{
			if($delivery_item['delivered_qty']<=0)
			{
				return 4;
			}
			else
			{
				$sql="select goods_qty from ".$GLOBALS['ecs']->table('erp_goods_attr_stock')." where attr_id='".$delivery_item['attr_id']."' and warehouse_id in (select warehouse_id from ".$GLOBALS['ecs']->table('erp_delivery')." where  delivery_id='".$delivery_id."')";
				$stock=	$GLOBALS['db']->getOne($sql);
				if($stock <$delivery_item['delivered_qty'])
				{
					return 5;
				}
			}
		}
	}
	return 0;
}
function check_warehousing_integrity($warehousing_id='')
{
	if(empty($warehousing_id))
	{
		return false;
	}
	$sql="select warehousing_style_id,warehousing_from,order_id from ".$GLOBALS['ecs']->table('erp_warehousing')." where warehousing_id='".$warehousing_id."'";
	$res=$GLOBALS['db']->getRow($sql);
	$warehousing_style_id=$res['warehousing_style_id'];
	$warehousing_from=$res['warehousing_from'];
	$order_id=$res['order_id'];
	if($warehousing_style_id==1)
	{
		if(empty($order_id))
		{
			return 1;
		}
	}
	if(empty($warehousing_from))
	{
		return 2;
	}
	$sql="select warehousing_item_id,received_qty  from ".$GLOBALS['ecs']->table('erp_warehousing_item')." where warehousing_id=".$warehousing_id."";
	$warehousing_item_info=$GLOBALS['db']->getAll($sql);
	if(empty($warehousing_item_info))
	{
		return 3;
	}
	else
	{
		foreach($warehousing_item_info as $warehousing_item)
		{
			if($warehousing_item['received_qty']<=0)
			{
				return 4;
			}
		}
	}
	return 0;
}
function add_delivery($warehouse_id = null, $style_id=-1)
{
	$delivery_sn=gen_delivery_sn();
	$admin_id=erp_get_admin_id();
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	$warehouse_info=get_warehouse_list($admin_id,$agency_id,1,-1,-1,'asc',false);
	$delivery_style_list=get_delivery_style_list(1);
	global $_CFG;
	if($_CFG['stock_dec_time']==0)
	{
		foreach($delivery_style_list as $k =>$delivery_style)
		{
			if($delivery_style['delivery_style_id']==1)
			{
				unset($delivery_style_list[$k]);
			}
		}
	}
	if(empty($warehouse_info) || empty($delivery_style_list))
	{
		return -1;
	}
	else
	{
		if (empty($warehouse_id)) {
			$warehouse_id=$warehouse_info[0]['warehouse_id'];	
		}

		$sql="insert into ".$GLOBALS['ecs']->table('erp_delivery')." set ";
		$sql.="delivery_sn='".$delivery_sn."',";
		if ($style_id > 0){
			$sql.="delivery_style_id='".$style_id."',";
		} else {
			$sql.="delivery_style_id='1',";
		}
		$sql.="warehouse_id ='".$warehouse_id."',";
		$sql.="create_time='".gmtime()."',";
		$sql.="create_by='".$admin_id."',";
		$sql.="delivery_to  ='',";
		$sql.="order_id  ='',";
		$sql.="status   ='1',";
		$sql.="agency_id   ='".$agency_id."'";
		if($GLOBALS['db']->query($sql))
		{
			return $GLOBALS['db']->insert_id($sql);
		}
		else
		{
			return false;
		}
	}
}
function add_warehousing($warehouse_id = null)
{
	$warehousing_sn=gen_warehousing_sn();
	$admin_id=erp_get_admin_id();
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	$warehouse_info=get_warehouse_list($admin_id,$agency_id,1,-1,-1,'asc',false);

	if(empty($warehouse_info))
	{
		return -1;
	}
	else
	{
		if ($warehouse_id == null || $warehouse_id <=0) {
			$warehouse_id=$warehouse_info[0]['warehouse_id'];
		} else {
			$warehouse_id_list = [];
			foreach ($warehouse_info as $warehouse) {
				$warehouse_id_list[] = $warehouse['warehouse_id'];
			}

			if (!in_array($warehouse_id,$warehouse_id_list)) {
				return -1;
			}
		}

		$sql="insert into ".$GLOBALS['ecs']->table('erp_warehousing')." set ";
		$sql.="warehousing_sn='".$warehousing_sn."',";
		$sql.="warehousing_style_id='1',";
		$sql.="warehouse_id ='".$warehouse_id."',";
		$sql.="create_time='".gmtime()."',";
		$sql.="create_by='".$admin_id."',";
		$sql.="warehousing_from  ='',";
		$sql.="order_id  ='',";
		$sql.="status   ='1',";
		$sql.="agency_id   ='".$agency_id."'";
		// As the admin account #4 is shared by all employees in sales team, we don't want it to fill in automatically
		if ($_SESSION['admin_id'] != 4)
		{
			$sql.=",operator = '".$_SESSION['admin_name']."'";
		}
		if($GLOBALS['db']->query($sql))
		{
			return $GLOBALS['db']->insert_id($sql);
		}
		else
		{
			return false;
		}
	}
}
function create_delivery_item_by_order($order_id,$delivery_id)
{
	if(empty($order_id)||empty($delivery_id))
	{
		return false;
	}
	else
	{
		$sql="select warehouse_id from ".$GLOBALS['ecs']->table('erp_delivery')." where delivery_id='".$delivery_id."'";
		$warehouse_id=$GLOBALS['db']->getOne($sql);
		$sql="select rec_id,goods_id,goods_price,(goods_number-delivery_qty) as expected_qty,goods_attr_id from ".$GLOBALS['ecs']->table('order_goods');
		$sql.=" where order_id='".$order_id."' and goods_number>delivery_qty";
		$res=$GLOBALS['db']->query($sql);
		while(list($rec_id,$goods_id,$goods_price,$expected_qty,$goods_attr_id)=mysql_fetch_row($res))
		{
			$attr_id=get_attr_id($goods_id,$goods_attr_id);
			$sql="insert into ".$GLOBALS['ecs']->table('erp_delivery_item')." set delivery_id='".$delivery_id."'";
			$sql.=",goods_id='".$goods_id."',attr_id='".$attr_id."',expected_qty='".$expected_qty."',price='".$goods_price."',order_item_id='".$rec_id."'";
			$GLOBALS['db']->query($sql);
		}
	}
	return true;
}
function create_warehousing_item_by_order($order_id,$warehousing_id,$orderType='')
{
	if(empty($order_id)||empty($warehousing_id))
	{
		return false;
	}
	else
	{
		if($orderType=='repair'){
			$sql="select order_item_id,goods_id,attr_id,(order_qty-warehousing_qty) as expected_qty,price from ".$GLOBALS['ecs']->table('repair_orders')." where repair_id='".$order_id."' and order_qty>warehousing_qty";
		}else{
			$sql="select order_item_id,goods_id,attr_id,(order_qty-warehousing_qty) as expected_qty,(price * order_qty - cn_amount) / order_qty as price from ".$GLOBALS['ecs']->table('erp_order_item')." where order_id='".$order_id."' and order_qty>warehousing_qty";
		}
		
		$res=$GLOBALS['db']->query($sql);
		while(list($order_item_id,$goods_id,$attr_id,$expected_qty,$price)=mysql_fetch_row($res))
		{
			$sql="insert into ".$GLOBALS['ecs']->table('erp_warehousing_item')." set warehousing_id='".$warehousing_id."'";
			$sql.=",goods_id='".$goods_id."',attr_id='".$attr_id."',expected_qty='".$expected_qty."',received_qty='".$expected_qty."',price='".$price."',account_payable=0,paid_amount=0,order_item_id='".$order_item_id."'";
			$GLOBALS['db']->query($sql);
		}
		return true;
	}
}
function get_warehousing_item_info_by_order($order_id,$warehousing_style_id = 1)
{
	$sql="select order_item_id,goods_id,attr_id,(order_qty-warehousing_qty) as expected_qty,(price * order_qty - cn_amount) / order_qty as price from ".$GLOBALS['ecs']->table('erp_order_item')." where order_id='".$order_id."' and order_qty>warehousing_qty";
	$res=$GLOBALS['db']->getAll($sql);

	foreach ($res as &$item) {
		$item['received_qty'] = $item['expected_qty'];

		$goods_id=$item['goods_id'];
		$sql="select cat_id,goods_id,goods_sn,goods_name,goods_thumb,goods_img, goods_length, goods_width, goods_height, goods_weight from ";
		$sql.=$GLOBALS['ecs']->table('goods')." where goods_id='".$goods_id."'";
		$goods_details=$GLOBALS['db']->getRow($sql);

		if ($goods_details['goods_length'] <= 0 || $goods_details['goods_width'] <= 0 || $goods_details['goods_height'] <= 0 || $goods_details['goods_weight'] <= 0 ) {
			$goods_details['basic_info_need_to_update'] = true;
		} else {
			$goods_details['basic_info_need_to_update'] = false;
		}

		if(!empty($goods_details['goods_thumb']))
		{
			$goods_details['goods_thumb']='../'.$goods_details['goods_thumb'];
		}
		if(!empty($goods_details['goods_img']))
		{
			$goods_details['goods_img']='../'.$goods_details['goods_img'];
		}
		$item['goods_details']=$goods_details;
		$item['goods_attr']=goods_attr($item['goods_id']);
		$attr_info=get_attr_info($item['attr_id']);
		if(!empty($attr_info['attr_info']))
		{
			foreach($attr_info['attr_info'] as $k =>$attr)
			{
				$attrs=$warehousing_item_info[$key]['goods_attr'][$attr['attr_id']];
				foreach($attrs as $i =>$a)
				{
					if($a['goods_attr_id']==$attr['goods_attr_id'])
					{
						$attrs[$i]['selected']=1;
					}
					else
					{
						$attrs[$i]['selected']=0;
					}
				}
				$attr_info['attr_info'][$k]['attrs']=$attrs;
			}
		}
		$item['selected_attr']=$attr_info['attr_info'];
		$sql="select gb.barcode from ".$GLOBALS['ecs']->table('erp_goods_barcode')." as gb,".$GLOBALS['ecs']->table('erp_goods_attr')." as ga where ga.attr_id='".$item['attr_id']."' and ga.barcode_id=gb.id limit 1";
		$item['barcode']=$GLOBALS['db']->getOne($sql);
	}

	return $res;
}
function is_sales_order_exist($order_id='',$order_sn='')
{
	if(empty($order_id) &&empty($order_sn))
	{
		return false;
	}
	elseif(!empty($order_id) &&empty($order_sn))
	{
		$sql="select * from ".$GLOBALS['ecs']->table('order_info')." where order_id='".$order_id."'";
		$res=$GLOBALS['db']->getRow($sql);
		if($res==false ||$res['is_deleted']==1)
		{
			return false;
		}
		else
		{
			return $res;
		}
	}
	elseif(empty($order_id) &&!empty($order_sn))
	{
		$sql="select * from ".$GLOBALS['ecs']->table('order_info')." where order_sn ='".$order_sn."'";
		$res=$GLOBALS['db']->getRow($sql);
		if($res==false ||$res['is_deleted']==1)
		{
			return false;
		}
		else
		{
			return $res;
		}
	}
}
function is_delivery_item_exist($delivery_item_id='')
{
	if(empty($delivery_item_id))
	{
		return false;
	}
	else
	{
		$sql="select * from ".$GLOBALS['ecs']->table('erp_delivery_item')." where delivery_item_id='".$delivery_item_id."'";
		$result=$GLOBALS['db']->getRow($sql);
		return !empty($result)?$result : false;
	}
}
function is_warehousing_item_exist($warehousing_item_id='')
{
	if(empty($warehousing_item_id))
	{
		return false;
	}
	else
	{
		$sql="select * from ".$GLOBALS['ecs']->table('erp_warehousing_item')." where warehousing_item_id='".$warehousing_item_id."'";
		$result=$GLOBALS['db']->getRow($sql);
		return !empty($result)?$result : false;
	}
}
function is_delivery_exist($delivery_id='')
{
	if(empty($delivery_id))
	{
		return false;
	}
	else
	{
		$sql="select * from ".$GLOBALS['ecs']->table('erp_delivery')." where delivery_id='".$delivery_id."'";
		$result=$GLOBALS['db']->getRow($sql);
		return !empty($result)?$result : false;
	}
}
function is_warehousing_exist($warehousing_id='')
{
	if(empty($warehousing_id))
	{
		return false;
	}
	else
	{
		$sql="select * from ".$GLOBALS['ecs']->table('erp_warehousing')." where warehousing_id='".$warehousing_id."'";
		$result=$GLOBALS['db']->getRow($sql);
		return !empty($result)?$result : false;
	}
}
function get_delivery_item_info($delivery_id='')
{
	if(empty($delivery_id))
	{
		return false;
	}
	else
	{
		$sql="select * from ".$GLOBALS['ecs']->table('erp_delivery_item')." where delivery_id='".$delivery_id."' for update";
		$delivery_item_info=$GLOBALS['db']->getAll($sql);
		if(!empty($delivery_item_info))
		{
			foreach($delivery_item_info as $key =>$item)
			{
				$goods_id=$item['goods_id'];
				$sql="select cat_id,goods_id,goods_sn,goods_name,goods_thumb,goods_img from ";
				$sql.=$GLOBALS['ecs']->table('goods')." where goods_id='".$goods_id."'";
				$goods_details=$GLOBALS['db']->getRow($sql);
				if(!empty($goods_details['goods_thumb']))
				{
					$goods_details['goods_thumb']='../'.$goods_details['goods_thumb'];
				}
				if(!empty($goods_details['goods_img']))
				{
					$goods_details['goods_img']='../'.$goods_details['goods_img'];
				}
				$delivery_item_info[$key]['goods_details']=$goods_details;
				$delivery_item_info[$key]['goods_attr']=goods_attr($item['goods_id']);
				/*UUECS 纠正错误 */
				if($item['attr_id']=='0'){
					$sql="select attr_id from ".$GLOBALS['ecs']->table('erp_goods_attr')." Where goods_id = '".$goods_id."'";
					$item['attr_id']=$GLOBALS['db']->getOne($sql);
				}
				$attr_info=get_attr_info($item['attr_id']);
				$delivery_item_info[$key]['selected_attr']=$attr_info['attr_info'];
				$sql="select goods_qty from ".$GLOBALS['ecs']->table('erp_goods_attr_stock')." where attr_id='".$item['attr_id']."' and warehouse_id in (select warehouse_id from ".$GLOBALS['ecs']->table('erp_delivery')." where  delivery_id='".$delivery_id."')";
				$goods_qty=$GLOBALS['db']->getOne($sql);
				$delivery_item_info[$key]['stock']=	$goods_qty>0?$goods_qty:0;
				$sql="select gb.barcode from ".$GLOBALS['ecs']->table('erp_goods_barcode')." as gb,".$GLOBALS['ecs']->table('erp_goods_attr')." as ga where ga.attr_id='".$item['attr_id']."' and ga.barcode_id=gb.id limit 1";
				$delivery_item_info[$key]['barcode']=$GLOBALS['db']->getOne($sql);
			}
		}
		return $delivery_item_info;
	}
}
function get_warehousing_item_info($warehousing_id='')
{
	if(empty($warehousing_id))
	{
		return false;
	}
	else
	{
		$sql="select * from ".$GLOBALS['ecs']->table('erp_warehousing_item')." where warehousing_id='".$warehousing_id."'";
		$warehousing_item_info=$GLOBALS['db']->getAll($sql);
		if(!empty($warehousing_item_info))
		{
			foreach($warehousing_item_info as $key =>$item)
			{
				$goods_id=$item['goods_id'];
				$sql="select cat_id,goods_id,goods_sn,goods_name,goods_thumb,goods_img from ";
				$sql.=$GLOBALS['ecs']->table('goods')." where goods_id='".$goods_id."'";
				$goods_details=$GLOBALS['db']->getRow($sql);
				if(!empty($goods_details['goods_thumb']))
				{
					$goods_details['goods_thumb']='../'.$goods_details['goods_thumb'];
				}
				if(!empty($goods_details['goods_img']))
				{
					$goods_details['goods_img']='../'.$goods_details['goods_img'];
				}
				$warehousing_item_info[$key]['goods_details']=$goods_details;
				$warehousing_item_info[$key]['goods_attr']=goods_attr($item['goods_id']);
				$attr_info=get_attr_info($item['attr_id']);
				if(!empty($attr_info['attr_info']))
				{
					foreach($attr_info['attr_info'] as $k =>$attr)
					{
						$attrs=$warehousing_item_info[$key]['goods_attr'][$attr['attr_id']];
						foreach($attrs as $i =>$a)
						{
							if($a['goods_attr_id']==$attr['goods_attr_id'])
							{
								$attrs[$i]['selected']=1;
							}
							else
							{
								$attrs[$i]['selected']=0;
							}
						}
						$attr_info['attr_info'][$k]['attrs']=$attrs;
					}
				}
				$warehousing_item_info[$key]['selected_attr']=$attr_info['attr_info'];
				$sql="select gb.barcode from ".$GLOBALS['ecs']->table('erp_goods_barcode')." as gb,".$GLOBALS['ecs']->table('erp_goods_attr')." as ga where ga.attr_id='".$item['attr_id']."' and ga.barcode_id=gb.id limit 1";
				$warehousing_item_info[$key]['barcode']=$GLOBALS['db']->getOne($sql);
			}
		}
		return $warehousing_item_info;
	}
}
function is_admin_warehouse($warehouse_id,$admin_id)
{
	$sql="select * from ".$GLOBALS['ecs']->table('erp_warehouse_admin')." where warehouse_id='".$warehouse_id."' and admin_id='".$admin_id."'";
	$row=$GLOBALS['db']->getRow($sql);
	if($row==false)
	{
		return false;
	}
	else
	{
		return true;
	}
}
function is_admin_delivery($delivery_id,$admin_id)
{
	$sql="select create_by from ".$GLOBALS['ecs']->table('erp_delivery')." where delivery_id='".$delivery_id."'";
	$create_by=$GLOBALS['db']->getOne($sql);
	if(($create_by == false && $admin_id != 1) || ($create_by > 0 && $create_by != $admin_id))
	{
		return false;
	}
	else
	{
		return true;
	}
}
function is_admin_warehousing($warehousing_id,$admin_id)
{
	$sql="select create_by from ".$GLOBALS['ecs']->table('erp_warehousing')." where warehousing_id='".$warehousing_id."'";
	$create_by=$GLOBALS['db']->getOne($sql);
	if(($create_by == false && $admin_id != 1) || ($create_by > 0 && $create_by != $admin_id))
	{
		return false;
	}
	else
	{
		return true;
	}
}
function get_warehousing_count($paras=array())
{
	$sql="select count(*) from ".$GLOBALS['ecs']->table('erp_warehousing')." where 1";
	if(isset($paras['status']) &&!empty($paras['status']))
	{
		$sql.=" and status='".$paras['status']."' ";
	}
	if(isset($paras['start_time']) &&!empty($paras['start_time']))
	{
		$sql.=" and create_time>='".$paras['start_time']."'";
	}
	if(isset($paras['end_time']) &&!empty($paras['end_time']))
	{
		$sql.=" and create_time<='".$paras['end_time']."'";
	}
	if(isset($paras['warehousing_style_id']) &&!empty($paras['warehousing_style_id']))
	{
		$sql.=" and warehousing_style_id='".$paras['warehousing_style_id']."'";
	}
	if(isset($paras['warehouse_id']) &&!empty($paras['warehouse_id']))
	{
		$sql.=" and warehouse_id='".$paras['warehouse_id']."'";
	}
	if(isset($paras['warehousing_from']) &&!empty($paras['warehousing_from']))
	{
		$sql.=" and warehousing_from like '%".$paras['warehousing_from']."%'";
	}
	if(isset($paras['order_id']) &&!empty($paras['order_id']))
	{
		$sql.=" and order_id='".$paras['order_id']."'";
	}
	if(isset($paras['warehousing_sn']) &&!empty($paras['warehousing_sn']))
	{
		$sql.=" and warehousing_sn like '%".$paras['warehousing_sn']."%'";
	}
	if(isset($paras['agency_id']) &&intval($paras['agency_id']) >0)
	{
		$sql.=" and warehouse_id in (select warehouse_id from ".$GLOBALS['ecs']->table('erp_warehouse')." where agency_id='".$paras['agency_id']."')";
		$sql.=" and agency_id='".$paras['agency_id']."'";
	}
	return $GLOBALS['db']->getOne($sql);
}
function get_warehousing_list($paras=array(),$start=-1,$num=-1)
{
	$sql="SELECT ewi.*, ".
		"(SELECT user_name FROM ".$GLOBALS['ecs']->table('admin_user')." WHERE user_id=ewi.create_by) as admin_name, ".
		"(SELECT user_name FROM ".$GLOBALS['ecs']->table('admin_user')." WHERE user_id=ewi.post_by) as post_by_name, ".
		"(SELECT user_name FROM ".$GLOBALS['ecs']->table('admin_user')." WHERE user_id=ewi.approved_by) as approved_by_name, ".
		"ew.name as warehouse_name, ew.description as warehouse_description, ew.is_valid as warehouse_is_valid, ew.agency_id as warehouse_agency_id, ew.is_on_sale as warehouse_is_on_sale, ".
		"ews.warehousing_style as warehousing_style_name, ews.is_valid as warehousing_style_is_valid, ".
		"eo.order_sn as eo_order_sn, eo.create_time as eo_create_time, eo.description as eo_description, eo.order_status as eo_order_status, ".
		"oi.order_sn as oi_order_sn, ".
		"(SELECT sum(expected_qty) FROM ".$GLOBALS['ecs']->table('erp_warehousing_item')." WHERE warehousing_id=ewi.warehousing_id) as expected_qty, ".
		"(SELECT sum(received_qty) FROM ".$GLOBALS['ecs']->table('erp_warehousing_item')." WHERE warehousing_id=ewi.warehousing_id) as received_qty, ".
		" ro.repair_sn as ro_repair_sn ".
		" FROM " . $GLOBALS['ecs']->table('erp_warehousing') . "as ewi ".
		" LEFT JOIN " . $GLOBALS['ecs']->table('erp_warehouse') . " as ew ON ewi.warehouse_id = ew.warehouse_id ".
		" LEFT JOIN " . $GLOBALS['ecs']->table('erp_warehousing_style') . " as ews ON ewi.warehousing_style_id = ews.warehousing_style_id ".
		" LEFT JOIN " . $GLOBALS['ecs']->table('erp_order') . " as eo ON ewi.order_id = eo.order_id ".
		" LEFT JOIN " . $GLOBALS['ecs']->table('order_info') . " as oi ON ewi.order_id = oi.order_id ".
		" LEFT JOIN " . $GLOBALS['ecs']->table('repair_orders') . " as ro ON ewi.order_id = ro.repair_id ".
		" WHERE 1";
	if(isset($paras['status']) &&!empty($paras['status']))
	{
		$sql.=" AND ewi.status='".$paras['status']."' ";
	}
	if(isset($paras['start_time']) &&!empty($paras['start_time']))
	{
		$sql.=" AND ewi.create_time>='".$paras['start_time']."'";
	}
	if(isset($paras['end_time']) &&!empty($paras['end_time']))
	{
		$sql.=" AND ewi.create_time<='".$paras['end_time']."'";
	}
	if(isset($paras['warehousing_style_id']) &&!empty($paras['warehousing_style_id']))
	{
		$sql.=" AND ewi.warehousing_style_id='".$paras['warehousing_style_id']."'";
	}
	if(isset($paras['warehouse_id']) &&!empty($paras['warehouse_id']))
	{
		$sql.=" AND ewi.warehouse_id='".$paras['warehouse_id']."'";
	}
	if(isset($paras['warehousing_from']) &&!empty($paras['warehousing_from']))
	{
		$sql.=" AND ewi.warehousing_from='".$paras['warehousing_from']."'";
	}
	if(isset($paras['order_id']) &&!empty($paras['order_id']))
	{
		$sql.=" AND ewi.order_id='".$paras['order_id']."'";
	}
	if(isset($paras['warehousing_sn']) &&!empty($paras['warehousing_sn']))
	{
		$sql.=" AND ewi.warehousing_sn LIKE '%".$paras['warehousing_sn']."%'";
	}
	if(isset($paras['agency_id']) &&intval($paras['agency_id']) >0)
	{
		$sql.=" AND ewi.warehouse_id IN (SELECT warehouse_id FROM ".$GLOBALS['ecs']->table('erp_warehouse')." WHERE agency_id='".$paras['agency_id']."')";
		$sql.=" AND ewi.agency_id='".$paras['agency_id']."'";
	}
	$sql.=" ORDER BY ewi.warehousing_id DESC";
	if($start!=-1 &&$num!=-1)
	{
		if($start<=0)
		{
			$start=0;
		}
		$sql.=" LIMIT ".$start.",".$num;
	}
	$res=$GLOBALS['db']->query($sql);
	$warehousing_list=array();
	while($row=mysql_fetch_assoc($res))
	{
		$create_time=$row['create_time'];
		$row['warehousing_date']=local_date('Y-m-d',$create_time);
		$warehousing_id=$row['warehousing_id'];
		//$row['available_act']=get_available_act($warehousing_id,'warehousing');
		$row['available_act']=get_available_act($row,'warehousing');
		$warehousing_style_id=$row['warehousing_style_id'];
		
		// $sql="select * from ".$GLOBALS['ecs']->table('erp_warehousing_style')." where warehousing_style_id='".$warehousing_style_id."'";
		// $row['warehousing_style_info']=$GLOBALS['db']->getRow($sql);
		$row['warehousing_style_info']=array(
			'warehousing_style_id' => $warehousing_style_id,
			'warehousing_style' => $row['warehousing_style_name'],
			'is_valid' => $row['warehousing_style_is_valid']
		);
		
		// $warehouse_id=$row['warehouse_id'];
		// $sql="select * from ".$GLOBALS['ecs']->table('erp_warehouse')." where warehouse_id='".$warehouse_id."'";
		// $row['warehouse_info']=$GLOBALS['db']->getRow($sql);
		$row['warehouse_info']=array(
			'warehouse_id' => $row['warehouse_id'],
			'name' => $row['warehouse_name'],
			'description' => $row['warehouse_description'],
			'is_valid' => $row['warehouse_is_valid'],
			'agency_id' => $row['warehouse_agency_id'],
			'is_on_sale' => $row['warehouse_is_on_sale']
		);
		
		// $create_by=$row['create_by'];
		// $sql="select user_name from ".$GLOBALS['ecs']->table('admin_user')." where user_id='".$create_by."'";
		// $row['admin_name']=$GLOBALS['db']->getOne($sql);
		// if(!empty($row['post_by']))
		// {
		// 	$post_by=$row['post_by'];
		// 	$sql="select user_name from ".$GLOBALS['ecs']->table('admin_user')." where user_id='".$post_by."'";
		// 	$row['post_by_name']=$GLOBALS['db']->getOne($sql);
		// }
		// if(!empty($row['approved_by']))
		// {
		// 	$approved_by=$row['approved_by'];
		// 	$sql="select user_name from ".$GLOBALS['ecs']->table('admin_user')." where user_id='".$approved_by."'";
		// 	$row['approved_by_name']=$GLOBALS['db']->getOne($sql);
		// }
		
		if(!empty($row['order_id']))
		{
			// $order_id=$row['order_id'];
			// if($row['warehousing_style_id']==1)
			// {
			// 	$sql="select order_sn,create_time,description,order_status from ".$GLOBALS['ecs']->table('erp_order')." where order_id='".$order_id."'";
			// 	$row['order_info']=$GLOBALS['db']->getRow($sql);
			// }
			// elseif($row['warehousing_style_id']==2 ||$row['warehousing_style_id']==3)
			// {
			// 	$sql="select order_sn from ".$GLOBALS['ecs']->table('order_info')." where order_id='".$order_id."'";
			// 	$row['order_info']=$GLOBALS['db']->getRow($sql);
			// }
			if($row['warehousing_style_id']==1)
			{
				$row['order_info'] = array(
					'order_sn' => $row['eo_order_sn'],
					'create_time' => $row['eo_create_time'],
					'description' => $row['eo_description'],
					'order_status' => $row['order_status']
				);
			}
			elseif($row['warehousing_style_id']==2 || $row['warehousing_style_id']==3)
			{
				$row['order_info'] = array('order_sn'  => $row['oi_order_sn']);
			}elseif($row['ro_repair_sn']!=''){
				$row['order_info'] = array('repair_sn' => $row['ro_repair_sn']);
			}
		}
		
		// Agency is not used
		// $sql="select agency_name from ".$GLOBALS['ecs']->table('agency')." where agency_id='".$row['agency_id']."'";
		// $row['agency_name']=$GLOBALS['db']->getOne($sql);
		
		// $sql="select sum(expected_qty) as expected_qty,sum(received_qty) as received_qty from ".$GLOBALS['ecs']->table('erp_warehousing_item')." where warehousing_id='".$row['warehousing_id']."'";
		// $tmp=$GLOBALS['db']->getRow($sql);
		// $row['expected_qty']=$tmp['expected_qty'];
		// $row['received_qty']=$tmp['received_qty'];
		
		$warehousing_list[]=$row;
	}
	return $warehousing_list;
}
function get_warehousing_info($warehousing_id)
{
	$sql="select * from ".$GLOBALS['ecs']->table('erp_warehousing')." where warehousing_id='".$warehousing_id."'";
	$warehousing_info=$GLOBALS['db']->getRow($sql);
	$warehousing_info['warehousing_date']=local_date('Y-m-d',$warehousing_info['create_time']);
	//$warehousing_info['available_act']=get_available_act($warehousing_info['warehousing_id'],'warehousing');
	$warehousing_info['available_act']=get_available_act($warehousing_info,'warehousing');
	$sql="select * from ".$GLOBALS['ecs']->table('erp_warehousing_style')." where warehousing_style_id='".$warehousing_info['warehousing_style_id']."'";
	$warehousing_info['warehousing_style_info']=$GLOBALS['db']->getRow($sql);
	$sql="select * from ".$GLOBALS['ecs']->table('erp_warehouse')." where warehouse_id='".$warehousing_info['warehouse_id']."'";
	$warehousing_info['warehouse_info']=$GLOBALS['db']->getRow($sql);
	$sql="select user_name from ".$GLOBALS['ecs']->table('admin_user')." where user_id='".$warehousing_info['create_by']."'";
	$warehousing_info['admin_name']=$GLOBALS['db']->getOne($sql);
	if(!empty($warehousing_info['post_by']))
	{
		$sql="select user_name from ".$GLOBALS['ecs']->table('admin_user')." where user_id='".$warehousing_info['post_by']."'";
		$warehousing_info['post_by_name']=$GLOBALS['db']->getOne($sql);
	}
	if(!empty($warehousing_info['approved_by']))
	{
		$sql="select user_name from ".$GLOBALS['ecs']->table('admin_user')." where user_id='".$warehousing_info['approved_by']."'";
		$warehousing_info['approved_by_name']=$GLOBALS['db']->getOne($sql);
	}
	if(!empty($warehousing_info['order_id']))
	{
		if($warehousing_info['warehousing_style_id']==1)
		{
			$sql="select order_sn,create_time,description,order_status from ".$GLOBALS['ecs']->table('erp_order')." where order_id='".$warehousing_info['order_id']."'";
			$warehousing_info['order_info']=$GLOBALS['db']->getRow($sql);
		}
		elseif($warehousing_info['warehousing_style_id']==2 ||$warehousing_info['warehousing_style_id']==3)
		{
			$sql="select order_sn from ".$GLOBALS['ecs']->table('order_info')." where order_id='".$warehousing_info['order_id']."'";
			$warehousing_info['order_info']=$GLOBALS['db']->getRow($sql);
		}elseif($warehousing_info['warehousing_style_id']==6){
			$sql="select repair_sn from ".$GLOBALS['ecs']->table('repair_orders')." where repair_id='".$warehousing_info['order_id']."'";
			$warehousing_info['order_info']=$GLOBALS['db']->getRow($sql);
		}
	}
	return $warehousing_info;
}
function lock_table($id='',$tb='',$act='')
{
	$GLOBALS['db']->query('START TRANSACTION');
	if(check_accessibility($id,$tb,$act))
	{
		$user_id=erp_get_admin_id();
		$lock_time=gmtime();
		$last_act_time=gmtime();
		if($tb=='warehousing')
		{
			$sql="update ".$GLOBALS['ecs']->table('erp_warehousing')." set locked_by='".$user_id."'";
			$sql.=", locked_time='".$lock_time."'";
			$sql.=", last_act_time='".$last_act_time."'";
			$sql.=", is_locked='1'";
			$sql.=" where warehousing_id='".$id."'";
		}
		elseif($tb=='delivery')
		{
			$sql="update ".$GLOBALS['ecs']->table('erp_delivery')." set locked_by='".$user_id."'";
			$sql.=", locked_time='".$lock_time."'";
			$sql.=", last_act_time='".$last_act_time."'";
			$sql.=", is_locked='1'";
			$sql.=" where delivery_id='".$id."'";
		}
		if($GLOBALS['db']->query($sql))
		{
			$GLOBALS['db']->query('COMMIT');
			return true;
		}
		else
		{
			$GLOBALS['db']->query('ROLLBACK');
			return false;
		}
	}
	else
	{
		$GLOBALS['db']->query('ROLLBACK');
		return false;
	}
}
function get_available_act($id='',$tb='')
{
	$act['view']=check_accessibility($id,$tb,'view');
	$act['edit']=check_accessibility($id,$tb,'edit');
	$act['delete']=check_accessibility($id,$tb,'delete');
	$act['post_to_approve']=check_accessibility($id,$tb,'post_to_approve');
	$act['approve']=check_accessibility($id,$tb,'approve');
	return $act;
}
function check_accessibility($id='',$tb='',$act='')
{
	if(empty($act) ||empty($tb) ||empty($id))
	{
		return false;
	}
	if (is_array($id))
	{
		// howang: Allow passing database row here to avoid querying multiple times
		$res = $id;
		if($tb=='warehousing')
		{
			$id = $res['warehousing_id'];
		}
		elseif($tb=='delivery')
		{
			$id = $res['delivery_id'];
		}
	}
	else
	{
		if($tb=='warehousing')
		{
			$sql="select status,is_locked,last_act_time,locked_by from ".$GLOBALS['ecs']->table('erp_warehousing')." where warehousing_id='".$id."' for update";
		}
		elseif($tb=='delivery')
		{
			$sql="select status,is_locked,last_act_time,locked_by from ".$GLOBALS['ecs']->table('erp_delivery')." where delivery_id='".$id."' for update";
		}
		else
		{
			return false;
		}
		$res=$GLOBALS['db']->getRow($sql);
	}
	if($res==false)
	{
		return false;
	}
	else
	{
		$is_locked=$res['is_locked'];
		$status=$res['status'];
		$last_act_time=$res['last_act_time'];
		$locked_by=$res['locked_by'];
		$current_time=gmtime();
		$if_overtime=(($current_time-$last_act_time)>(10))?1:0;
		$current_user_id=erp_get_admin_id();
		if($act=='view')
		{
			if(admin_priv('erp_warehouse_view','',false) ||admin_priv('erp_warehouse_manage','',false) ||admin_priv('erp_warehouse_approve','',false))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		elseif($act=='edit'||$act=='delete'||$act=='post_to_approve')
		{
			if($status==1)
			{
				if(admin_priv('erp_warehouse_manage','',false) &&($is_locked==0 ||($is_locked==1 &&$if_overtime==1 &&$current_user_id!=$locked_by) ||($is_locked==1 &&$current_user_id==$locked_by)))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		elseif($act=='approve')
		{
			if($status==2)
			{
				if(admin_priv('erp_warehouse_approve','',false) &&($is_locked==0 ||($is_locked==1 &&$if_overtime==1 &&$current_user_id!=$locked_by) ||($is_locked==1 &&$current_user_id==$locked_by)))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
}
function get_order_not_delivered()
{
	$sql="select distinct order_id from ".$GLOBALS['ecs']->table('order_goods')."where delivery_qty<goods_number";
	$res=$GLOBALS['db']->query($sql);
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	while(list($order_id)=mysql_fetch_row($res))
	{
		$sql="select order_id,order_sn from ".$GLOBALS['ecs']->table('order_info')." where order_id='".$order_id."' and (shipping_status='".SS_PREPARING."' or shipping_status='".SS_SHIPPED_PART."')";
		if($agency_id>0)
		{
			$sql.=" and agency_id='".$agency_id."'";
		}
		$sql.=" order by confirm_time ";
		$result=$GLOBALS['db']->getRow($sql);
		if(!empty($result))
		{
			$order_info[]=$result;
		}
	}
	return $order_info;
}
function get_order_not_warehoused()
{
	$sql="select distinct order_id from ".$GLOBALS['ecs']->table('erp_order_item')."where warehousing_qty<order_qty";
	$res=$GLOBALS['db']->query($sql);
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	while(list($order_id)=mysql_fetch_row($res))
	{
		$sql="select order_id,order_sn,create_time,description,supplier_id from ".$GLOBALS['ecs']->table('erp_order')." where order_id='".$order_id."' and order_status='4'";
		if($agency_id>0)
		{
			$sql.=" and agency_id='".$agency_id."'";
		}
		$result=$GLOBALS['db']->getRow($sql);
		if(!empty($result))
		{
			$order_info[]=$result;
		}
	}
	return $order_info;
}
function get_warehouse_count()
{
	$sql="select count(*) from ".$GLOBALS['ecs']->table('erp_warehouse')." where 1 ";
	return $GLOBALS['db']->getOne($sql);
}
function get_warehouse_list($admin_id=0,$agency_id=0,$is_valid=-1,$start=-1,$num=-1,$order='desc',$key=true)
{
	$sql="select * from ".$GLOBALS['ecs']->table('erp_warehouse')." where 1";
	if($admin_id >0)
	{
		$sql.=" and warehouse_id in (select warehouse_id from ".$GLOBALS['ecs']->table('erp_warehouse_admin')." where admin_id='".$admin_id."')";
	}
	if($agency_id >0)
	{
		$sql.=" and warehouse_id in (select warehouse_id from ".$GLOBALS['ecs']->table('erp_warehouse')." where agency_id='".$agency_id."')";
	}
	if($is_valid >= 0)
	{
		$sql.=" and is_valid='".$is_valid."'";
	}
	$sql.=" order by warehouse_id ".$order;
	if($start!=-1 &&$num!=-1)
	{
		if($start<=0)
		{
			$start=0;
		}
		$sql.=" limit ".$start.",".$num;
	}
	$res=$GLOBALS['db']->query($sql);
	$list=array();
	while($row=mysql_fetch_assoc($res))
	{
		$sql="select count(*) as warehousing_number from ".$GLOBALS['ecs']->table('erp_warehousing')." where warehouse_id='".$row['warehouse_id']."'";
		$row['warehousing_number']=$GLOBALS['db']->getOne($sql);
		$sql="select count(*) as delivery_number from ".$GLOBALS['ecs']->table('erp_delivery')." where warehouse_id='".$row['warehouse_id']."'";
		$row['delivery_number']=$GLOBALS['db']->getOne($sql);
		$sql="select agency_name from ".$GLOBALS['ecs']->table('agency')." where agency_id='".$row['agency_id']."'";
		$row['agency_name']=$GLOBALS['db']->getOne($sql);
		if($key==true)
		{
			$list[$row['warehouse_id']]=$row;
		}
		else
		{
			$list[]=$row;
		}
	}
	return $list;
}
function warehouse_info($warehouse_id)
{
	$sql="select * from ".$GLOBALS['ecs']->table('erp_warehouse')." where warehouse_id='".$warehouse_id."'";
	$warehouse_info=$GLOBALS['db']->getRow($sql);
	return $warehouse_info;
}
function is_warehouse_exist($warehouse_id='')
{
	if(empty($warehouse_id))
	{
		return false;
	}
	else
	{
		$sql="select * from ".$GLOBALS['ecs']->table('erp_warehouse')." where warehouse_id='".$warehouse_id."'";
		$result=$GLOBALS['db']->getRow($sql);
		return !empty($result)?$result : false;
	}
}
function get_delivery_status()
{
	$delivery_status[0]['status_no']=1;
	$delivery_status[0]['status_style']="出貨單錄入中";
	$delivery_status[2]['status_no']=2;
	$delivery_status[2]['status_style']="主管審核中";
	$delivery_status[3]['status_no']=3;
	$delivery_status[3]['status_style']="審核已通過";
	$delivery_status[4]['status_no']=4;
	$delivery_status[4]['status_style']="審核未通過";
	return $delivery_status;
}
function get_warehousing_status()
{
	$warehousing_status[0]['status_no']=1;
	$warehousing_status[0]['status_style']="入貨單錄入中";
	$warehousing_status[1]['status_no']=2;
	$warehousing_status[1]['status_style']="財務處理中";
	$warehousing_status[3]['status_no']=3;
	$warehousing_status[3]['status_style']="審核已通過";
	$warehousing_status[4]['status_no']=4;
	$warehousing_status[4]['status_style']="審核未通過";
	return $warehousing_status;
}
function get_delivery_style_count()
{
	$sql="select count(*) from ".$GLOBALS['ecs']->table('erp_delivery_style')." where 1 ";
	return $GLOBALS['db']->getOne($sql);
}
function get_delivery_style_list($is_valid=-1,$start=-1,$num=-1,$only=-1)
{
	$sql="select * from ".$GLOBALS['ecs']->table('erp_delivery_style')." where 1 ";
	if($is_valid >= 0)
	{
		$sql.=" and is_valid='".$is_valid."'";
	}
	if($only >= 0)
	{
		$sql.=" and delivery_style_id='".$only."'";
	}
	$sql.=" order by delivery_style_id desc";
	if($start!=-1 &&$num!=-1)
	{
		if($start<=0)
		{
			$start=0;
		}
		$sql.=" limit ".$start.",".$num;
	}
	$list=$GLOBALS['db']->getAll($sql);
	return $list;
}
function delivery_style_info($delivery_style_id)
{
	$sql="select * from ".$GLOBALS['ecs']->table('erp_delivery_style')." where delivery_style_id='".$delivery_style_id."'";
	return $GLOBALS['db']->getRow($sql);
}
function get_warehousing_style_count()
{
	$sql="select count(*) from ".$GLOBALS['ecs']->table('erp_warehousing_style')." where 1 ";
	return $GLOBALS['db']->getOne($sql);
}
function get_warehousing_style_list($is_valid=-1,$start=-1,$num=-1,$only="-1")
{
	$sql="select * from ".$GLOBALS['ecs']->table('erp_warehousing_style')." where 1 ";
	if($is_valid >= 0)
	{
		$sql.=" and is_valid='".$is_valid."'";
	}
	if($only != "-1")
	{
		$sql.=" and warehousing_style_id in (".$only.")";
	}
	$sql.=" order by warehousing_style_id desc";
	if($start!=-1 &&$num!=-1)
	{
		if($start<=0)
		{
			$start=0;
		}
		$sql.=" limit ".$start.",".$num;
	}
	$list=$GLOBALS['db']->getAll($sql);
	return $list;
}
function warehousing_style_info($warehousing_style_id)
{
	$sql="select * from ".$GLOBALS['ecs']->table('erp_warehousing_style')." where warehousing_style_id='".$warehousing_style_id."'";
	return $GLOBALS['db']->getRow($sql);
}
function is_warehousing_style_exist($warehousing_style_id='')
{
	if(empty($warehousing_style_id))
	{
		return false;
	}
	else
	{
		$sql="select * from ".$GLOBALS['ecs']->table('erp_warehousing_style')." where warehousing_style_id='".$warehousing_style_id."'";
		$result=$GLOBALS['db']->getRow($sql);
		return !empty($result)?$result : false;
	}
}
function is_order_exist($order_id='',$order_sn='')
{
	if(empty($order_id) &&empty($order_sn))
	{
		return false;
	}
	elseif(!empty($order_id) &&empty($order_sn))
	{
		$sql="select * from ".$GLOBALS['ecs']->table('erp_order')." where order_id='".$order_id."' limit 1";
		$result=$GLOBALS['db']->getRow($sql);
	}
	elseif(empty($order_id) &&!empty($order_sn))
	{
		$sql="select * from ".$GLOBALS['ecs']->table('erp_order')." where order_sn ='".$order_sn."' limit 1";
		$result=$GLOBALS['db']->getRow($sql);
	}
	return !empty($result)?true : false;
}
function gen_delivery_sn()
{
	$sql="select delivery_sn from ".$GLOBALS['ecs']->table('erp_delivery')." where delivery_sn like 'DE%' order by delivery_id  desc limit 1";
	$result=$GLOBALS['db']->getOne($sql);
	if(!empty($result))
	{
		$year_date=substr($result,2,4);
		$current_year_date=local_date("ym");
		if($current_year_date==$year_date)
		{
			$sn=substr($result,6,3);
			$sn=intval($sn)+1;
			$sn=gen_zero_str(3-strlen($sn)).$sn;
			$sn='DE'.$year_date.$sn;
			return $sn;
		}
		else
		{
			$sn='DE'.$current_year_date."001";
			return $sn;
		}
	}
	else
	{
		$current_year_date=local_date("ym");
		$sn='DE'.$current_year_date."001";
		return $sn;
	}
}
function gen_warehousing_sn()
{
	$sql="select warehousing_sn from ".$GLOBALS['ecs']->table('erp_warehousing')." where warehousing_sn like 'WA%' order by warehousing_id desc limit 1";
	$result=$GLOBALS['db']->getOne($sql);
	if(!empty($result))
	{
		$year_date=substr($result,2,4);
		$current_year_date=local_date("ym");
		if($current_year_date==$year_date)
		{
			$sn=substr($result,6,3);
			$sn=intval($sn)+1;
			$sn=gen_zero_str(3-strlen($sn)).$sn;
			$sn='WA'.$year_date.$sn;
			return $sn;
		}
		else
		{
			$sn='WA'.$current_year_date."001";
			return $sn;
		}
	}
	else
	{
		$current_year_date=local_date("ym");
		$sn='WA'.$current_year_date."001";
		return $sn;
	}
}
function random_string($type = 'alnum',$len = 8)   
{
	switch($type)   
	{
		case 'alnum':   
		case 'numeric':   
		case 'nozero':   
			switch ($type)   
		{
			case 'alnum':   $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
				break;
			case 'numeric':   $pool = '0123456789';
				break;
			case 'nozero':   $pool = '123456789';
				break;
		}
			$str = '';
			for ($i=0;$i <$len;$i++)   
			{
				$str .= substr($pool,mt_rand(0,strlen($pool) -1),1);
			}
			return $str;
			break;
		case 'unique': return md5(uniqid(mt_rand()));
			break;
	}
}
?>
