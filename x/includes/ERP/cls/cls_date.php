<?php

class cls_date
{
    public function cls_date()
    {
        ;
    }
    public function __construct()
    {
        ;
    }
    public function time_to_array($time)
    {
        $time=preg_replace('/\s+/',' ',$time);
        $time_array=explode(' ',$time);
        $date=explode('-',$time_array[0]);
        $time=explode(':',$time_array[1]);
        $result['year']=intval($date[0]);
        $result['month']=intval($date[1]);
        $result['day']=intval($date[2]);
        $result['hour']=intval($time[0]);
        $result['minute']=intval($time[1]);
        $result['second']=intval($time[2]);
        return $result;
    }
    public function date_to_array($date)
    {
        $date=explode('-',$date);
        $result['year']=intval($date[0]);
        $result['month']=intval($date[1]);
        $result['day']=intval($date[2]);
        $result['hour']=0;
        $result['minute']=0;
        $result['second']=0;
        return $result;
    }
    public function time_to_stamp($time)
    {
        $time_array=$this->time_to_array($time);
        return gmmktime($time_array['hour'],$time_array['minute'],$time_array['second'],$time_array['month'],$time_array['day'],$time_array['year']);
    }
    public function date_to_stamp($date)
    {
        $date_array=$this->date_to_array($date);
        return gmmktime($date_array['hour'],$date_array['minute'],$date_array['second'],$date_array['month'],$date_array['day'],$date_array['year']);
    }
    public function get_time($timestamp='')
    {
        if(empty($timestamp))
        {
            return gmdate('Y-m-d : H:i:s');
        }
        else
        {
            return gmdate('Y-m-d : H:i:s',$timestamp);
        }
    }
    public function get_date($timestamp='')
    {
        if(empty($timestamp))
        {
            return gmdate('Y-m-d');
        }
        else
        {
            return gmdate('Y-m-d',$timestamp);
        }
    }
    public function format_date($year,$month,$day)
    {
        return $year.'-'.$month.'-'.$day;
    }
    public function get_first_day($date)
    {
        $date_array=$this->date_to_array($date);
        $year=$date_array['year'];
        $month=$date_array['month'];
        $day='01';
        $timestamp=$this->date_to_stamp($this->format_date($year,$month,$day));
        return $this->get_date($timestamp);
    }
    public function get_last_day($date)
    {
        $date_array=$this->date_to_array($date);
        $year=$date_array['year'];
        $month=$date_array['month'];
        $day='01';
        if($month==12)
        {
            $month=1;
            $year=$year+1;
        }
        else
        {
            $month=$month+1;
        }
        $timestamp=$this->date_to_stamp($this->format_date($year,$month,$day));
        $timestamp=$timestamp-1;
        return $this->get_date($timestamp);
    }
    public function get_dates($start_date,$end_date)
    {
        $start_timestamp=!empty($start_date)?$this->date_to_stamp($start_date):time();
        $end_timestamp=!empty($end_date)?$this->date_to_stamp($end_date):time();
        if($end_timestamp<$start_timestamp)
        {
            $tmp=$end_timestamp;
            $end_timestamp=$start_timestamp;
            $start_timestamp=$tmp;
        }
        for($stamp=$start_timestamp;$stamp<=$end_timestamp;$stamp=$stamp+24*60*60)
        {
            $result[]=$this->get_date($stamp);
        }
        return $result;
    }
    public function get_day_number($start_date,$end_date)
    {
        return count($this->get_dates($start_date,$end_date));
    }
    public function get_timestamps($start_date,$end_date)
    {
        $dates=$this->get_dates($start_date,$end_date);
        $result=array();
        if(!empty($dates))
        {
            foreach($dates as $key=>$date)
            {
                $stamps=array();
                $stamps['start_stamp']=$this->date_to_stamp($date);
                $stamps['end_stamp']=$this->date_to_stamp($date)+24*60*60-1;
                $result[$date]=$stamps;
            }
        }
        return $result;
    }
    public function get_microtime($microtime='')
    {
        if(empty($microtime))
        {
            $microtime=microtime();
        }
        $time=explode(' ',$microtime);
        $sec=$time[1];
        $micro_sec=$time[0];
        $micro_sec=substr($micro_sec,1,strlen($micro_sec));
        return $sec.$micro_sec;
    }
    public function get_microtime_diff($microtime1,$microtime2)
    {
        if(empty($microtime1) ||empty($microtime2))
        {
            return false;
        }
        else
        {
            $t1=$this->get_microtime($microtime1);
            $t2=$this->get_microtime($microtime2);
            if($t2>=$t1)
            {
                return floatval($t2-$t1);
            }
            else
            {
                return floatval($t1-$t2);
            }
        }
    }
}
?>