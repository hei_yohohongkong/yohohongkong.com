<?php

require_once(dirname(__FILE__) .'/cls_base.php');
class cls_csv extends cls_base{
private $str_coding='utf-8';
public function cls_csv($str_coding='utf-8')
{
$this->str_coding=$str_coding;
}
public function __construct($str_coding='utf-8')
{
$this->cls_csv($str_coding);
}
private function trim_quote($str)
{
if($this->get_substr($str,0,1)=='"')
{
$str=$this->get_substr($str,1,$this->get_strlen($str)-1);
}
if($this->get_substr($str,-1,1)=='"')
{
$str=$this->get_substr($str,0,$this->get_strlen($str)-1);
}
return $str;
}
private function get_quote_number($str)
{
$k=0;
for($i=0;$i<=$this->get_strlen($str)-1;$i++)
{
if($this->get_substr($str,$i,1)=='"')
{
$k++;
}
}
return $k;
}
private function is_even($num)
{
if($num%2==0)
{
return true;
}
else{
return false;
}
}
private function trim_dbl_quote($str)
{
$has_beginning_quote=0;
$has_endding_quote=0;
if($this->get_substr($str,0,1)=='"')
{
$str=$this->get_substr($str,1,$this->get_strlen($str)-1);
$has_beginning_quote=1;
}
if($this->get_substr($str,-1,1)=='"')
{
$str=$this->get_substr($str,0,$this->get_strlen($str)-1);
$has_endding_quote=1;
}
$str=preg_replace('/""/','"',$str);
if($has_beginning_quote==1)
{
$str='"'.$str;
}
if($has_endding_quote==1)
{
$str=$str.'"';
}
return 	$str;
}
private function explode_csv_row($str)
{
if(empty($str))
{
return array();
}
$first_char=$this->get_substr($str,0,1);
if($first_char==',')
{
$result[]='';
$result[]=$this->get_substr($str,1,$this->get_strlen($str)-1);
}
elseif($first_char=='"')
{
for($i=1;$i<=$this->get_strlen($str)-1;$i++)
{
if($this->get_substr($str,$i,1)=='"')
{
$next_char=$this->get_substr($str,$i+1,1);
if($next_char==',')
{
$quote_number=$this->get_quote_number($this->get_substr($str,0,$i+1));
if($this->is_even($quote_number))
{
$result[0]=$this->get_substr($str,0,$i+1);
$result[1]=$this->get_substr($str,$i+2,$this->get_strlen($str)-$i-2);
break;
}
else{
continue;
}
}
else{
continue;
}
}
else{
continue;
}
}
}
else{
$comma_pos=$this->get_strpos($str,',');
if($comma_pos==false)
{
$result[0]=$str;
}
else{
$result[0]=$this->get_substr($str,0,$this->get_strpos($str,','));
$result[1]=$this->get_substr($str,$this->get_strpos($str,',')+1,$this->get_strlen($str)-$this->get_strpos($str,',')-1);
}
}
if((empty($result) ||(empty($result[0]) &&empty($result[1])))&&$str!=',')
{
$result[0]=$str;
}
return $result;
}
private function explode_csv($str)
{
if(!is_array($str))
{
$str=$this->explode_csv_row($str);
}
if(empty($str))
{
return array();
}
else{
$count=count($str);
$last_item=$str[$count-1];
$explode_result=$this->explode_csv_row($last_item);
if(!empty($explode_result) &&count($explode_result)==2)
{
$str[$count-1]=$explode_result[0];
$str[$count]=$explode_result[1];
$str=$this->explode_csv($str);
}
}
if(!empty($str))
{
foreach($str as $key =>$item)
{
$str[$key]=$this->trim_quote($this->trim_dbl_quote($item));
}
}
return $str;
}
public function decode($csv_stream)
{
$csv_stream=preg_replace("/([\r\n]+)/",'|',$csv_stream);
$csv_stream_array=explode('|',$csv_stream);
if(!empty($csv_stream_array))
{
foreach($csv_stream_array as $key=>$row)
{
if(!empty($row))
{
$result_array[]=$this->explode_csv($row);
}
}
}
return $result_array;
}
private function add_quote($str)
{
return '"'.$str.'"';
}
private function add_dbl_quote($str)
{
$str=preg_replace('/"/','""',$str);
return 	$str;
}
public function encode($csv_array)
{
$str="";
if(!empty($csv_array))
{
foreach($csv_array as $key=>$row)
{
if(is_array($row) &&!empty($row))
{
$new_row=array();
foreach($row as $k=>$v)
{
$new_row[]=$v;
}
$count=count($row);
foreach($new_row as $k=>$v)
{
$str.=$this->add_quote($this->add_dbl_quote($v));
if($k!=$count-1)
{
$str.=",";
}
}
}
$str.="\r\n";
}
}
return $str;
}
}
?>
