<?php

require_once(dirname(__FILE__) .'/cls_base.php');
class cls_array extends cls_base{
public $coding='utf-8';
public function cls_array($coding='utf-8')
{
$this->coding=$coding;
}
public function __construct($coding='utf-8')
{
$this->cls_array($coding);
}
public function trim_empty($array)
{
if(!is_array($array))
{
return false;
}
else{
if(empty($array))
{
return array();
}
else{
foreach($array as $k=>$v)
{
if(!empty($v))
{
$new_array[$k]=$v;
}
}
}
}
return $new_array;
}
public function trim_repeat($array)
{
if(!is_array($array))
{
return false;
}
else{
if(empty($array))
{
return array();
}
else{
$new_array=array();
foreach($array as $k=>$v)
{
if(!in_array($v,$new_array))
{
$new_array[$k]=$v;
}
}
return $new_array;
}
}
}
public function remove_key($array)
{
if(!is_array($array))
{
return false;
}
else{
if(empty($array))
{
return array();
}
else{
$new_array=array();
foreach($array as $k=>$v)
{
$new_array[]=$v;
}
return $new_array;
}
}
}
public function sub_array($array,$start,$num,$keep_key=true)
{
if(!is_array($array))
{
return false;
}
else{
if(empty($array))
{
return array();
}
else{
if($start>=0)
{
reset($array);
while($tmp[]=each($array))
{
;
}
$new_array=$this->trim_empty($tmp);
foreach($new_array as $key=>$v)
{
if($key>=$start &&$key<$start+$num)
{
$result[$v['key']]=$v['value'];
}
}
if($keep_key==true)
{
return $result;
}
else{
return $this->remove_key($result);
}
}
else{
$array_len=count($array);
return $this->sub_array($array,$array_len+$start,$num,$keep_key);
}
}
}
}
public function traverse_array($var,$func)
{
if(is_array($var))
{
if(!empty($var))
{
foreach($var as $key=>$item)
{
$var[$key]=$this->traverse_array($item,$func);
}
}
}
else{
$var=$func($var);
}
return $var;
}
public function encode($var)
{
return $this->traverse_array($var,'base64_encode');
}
public function decode($var)
{
return $this->traverse_array($var,'base64_decode');
}
public function flat_array($array)
{
while(!empty($array))
{
foreach($array as $key=>$item)
{
if(is_array($item))
{
foreach($item as $key2=>$item2)
{
$array[]=$item2;
}
unset($array[$key]);
}
else{
$new_array[]=$item;
unset($array[$key]);
}
}
}
return $new_array;
}
public function utf2gbk($var)
{
if(is_array($var))
{
if(!empty($var))
{
foreach($var as $key=>$item)
{
$var[$key]=$this->utf2gbk($item);
}
}
}
else{
if($this->is_utf8($var))
{
return iconv('utf-8','gbk',$var);
}
else{
return $var;
}
}
return $var;
}
public function gbk2utf($var)
{
if(is_array($var))
{
if(!empty($var))
{
foreach($var as $key=>$item)
{
$var[$key]=$this->gbk2utf($item);
}
}
}
else{
if(!$this->is_utf8($var))
{
return iconv('gbk','utf-8',$var);
}
else{
return $var;
}
}
return $var;
}
}
?>
