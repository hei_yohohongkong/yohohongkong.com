<?php

class cls_base{
public $coding='utf-8';
public function cls_base($coding='utf-8')
{
$this->coding=$coding;
}
public function __construct($coding='utf-8')
{
$this->cls_base($coding);
}
public function get_substr($str,$start,$lengh)
{
return mb_substr($str,$start,$lengh,$this->coding);
}
public function get_strlen($str)
{
return mb_strlen($str,$this->coding);
}
public function get_strpos($str,$substr,$pos=0)
{
return mb_strpos($str,$substr,$pos,$this->coding);
}
public function get_strrpos($str,$substr)
{
return mb_strrpos($str,$substr,$this->coding);
}
public function str_encoding($str)
{
$coding=array('utf-8','gbk','gb2312');
return mb_detect_encoding($str,$coding);
}
public function split_string($str)
{
$str_len=$this->get_strlen($str);
for($i=0;$i<=$str_len-1;$i++)
{
$result[]=$this->get_substr($str,$i,1);
}
return $result;
}
public function is_utf8($str) 
{
if (preg_match("/^([".chr(228)."-".chr(233)."]{1}[".chr(128)."-".chr(191)."]{1}[".chr(128)."-".chr(191)."]{1}){1}/",$str) == true ||preg_match("/([".chr(228)."-".chr(233)."]{1}[".chr(128)."-".chr(191)."]{1}[".chr(128)."-".chr(191)."]{1}){1}$/",$str) == true ||preg_match("/([".chr(228)."-".chr(233)."]{1}[".chr(128)."-".chr(191)."]{1}[".chr(128)."-".chr(191)."]{1}){2,}/",$str) == true) 
{
return true;
}
else 
{
if($str===iconv('gbk','utf-8',iconv('utf-8','gbk',$str)))
{
return true;
}
else{
return false;
}
}
}
public function random_string($type = 'alnum',$len = 8)   
{
switch($type)   
{
case 'alnum':   
case 'numeric':   
case 'nozero': 
case 'letter': 
case 'lower_letter':
case 'upper_letter':
switch ($type)   
{
case 'alnum':   $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
break;
case 'numeric':   $pool = '0123456789';
break;
case 'nozero':   $pool = '123456789';
break;
case 'letter':   $pool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
break;
case 'lower_letter':   $pool = 'abcdefghijklmnopqrstuvwxyz';
break;
case 'upper_letter':   $pool = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
break;
}
$str = '';
for ($i=0;$i <$len;$i++)   
{
$str .= substr($pool,mt_rand(0,strlen($pool) -1),1);
}
return $str;
break;
case 'unique': return md5(uniqid(mt_rand()));
break;
}
}
}
?>
