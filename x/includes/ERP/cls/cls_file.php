<?php

require_once(dirname(__FILE__) .'/cls_base.php');
class cls_file extends cls_base{
private $path='';
public function cls_file($coding='utf-8')
{
$this->coding=$coding;
}
public function __construct($coding='utf-8')
{
$this->cls_file($coding);
}
private function dir_list($dir)
{
$dir=preg_replace('/\\\/','/',$dir);
$dir=preg_replace('|\/+|','/',$dir);
if($this->get_substr($dir,-1,1)=='/')
{
$dir=$this->get_substr($dir,0,$this->get_strlen($dir)-1);
}
if($this->is_utf8($dir))
{
$dir=iconv('utf-8','gbk',$dir);
}
$dir_array=explode('/',$dir);
if(!empty($dir_array))
{
foreach($dir_array as $k=>$v)
{
$temp.=$v.'/';
$dir_list[]=$temp;
}
}
return $dir_list;
}
public function create_dir($dir)
{
$dir=$this->transact_path($dir);
if($this->is_utf8($dir))
{
$dir=iconv('utf-8','gbk',$dir);
}
if(!empty($dir))
{
$dir_list=$this->dir_list($dir);
foreach($dir_list as $k=>$v)
{
if(!file_exists($v))
{
if(!mkdir($v))
{
return false;
}
}
}
}
}
public function is_dir_empty($dir)
{
$dir=$this->transact_path($dir);
if($this->is_utf8($dir))
{
$dir=iconv('utf-8','gbk',$dir);
}
$is_empty=true;
$files=scandir($dir);
if(!empty($files))
{
foreach($files as $key=>$item)
{
if($item!='.'&&$item!='..')
{
$is_empty=false;
}
}
}
return $is_empty;
}
public function delete_dir($dir,$delete_self=false)
{
$this->clear_dir($dir);
$dirs=$this->get_dirs($dir);
if(!empty($dirs))
{
krsort($dirs);
foreach($dirs as $dir)
{
@rmdir($dir);
}
}
if($delete_self===true)
{
if($this->is_utf8($dir))
{
$dir=iconv('utf-8','gbk',$dir);
}
@rmdir($dir);
}
}
public function clear_dir($dir,$file_ext='')
{
$file_list=$this->get_files($dir,$file_ext);
if(!empty($file_list))
{
foreach($file_list as $key=>$item)
{
@unlink($item);
}
}
}
public function transact_path($path)
{
$path=preg_replace('/\\\/','/',$path);
$path=preg_replace('|\/+|','/',$path);
if($this->get_substr($path,-1,1)!='/')
{
$path=$path.'/';
}
if($this->is_utf8($path))
{
$path=iconv('utf-8','gbk',$path);
}
return $path;
}
public function write_file($path,$file,$str,$op='rewrite')
{
$path=$this->transact_path($path);
if($this->is_utf8($path))
{
$path=iconv('utf-8','gbk',$path);
}
if($this->is_utf8($file))
{
$file=iconv('utf-8','gbk',$file);
}
if(!file_exists($path))
{
if(false===($this->create_dir($path)))
{
return false;
}
}
if($op=='append')
{
$f_handle=fopen($path.$file,'ab');
if(!fwrite($f_handle,$str))
{
return false;
}
fclose($f_handle);
}
elseif($op=='rewrite')
{
$f_handle=fopen($path.$file,'wb');
if(!fwrite($f_handle,$str))
{
return false;
}
fclose($f_handle);
}
if(!file_exists($path.$file))
{
return false;
}
else{
return true;
}
}
public function read_file($path,$file)
{
$path=$this->transact_path($path);
if($this->is_utf8($path))
{
$path=iconv('utf-8','gbk',$path);
}
if($this->is_utf8($file))
{
$file=iconv('utf-8','gbk',$file);
}
if(!file_exists($path.$file))
{
return false;
}
$f_handle=fopen($path.$file,'r');
if($f_handle===false)
{
return false;
}
$contents = "";
while (!feof($f_handle))
{
$contents .= fread($f_handle,8192);
}
fclose($f_handle);
return $contents;
}
public function file_a_time($path,$file)
{
$path=$this->transact_path($path);
if($this->is_utf8($path))
{
$path=iconv('utf-8','gbk',$path);
}
if($this->is_utf8($file))
{
$file=iconv('utf-8','gbk',$file);
}
return fileatime($path.$file);
}
public function file_m_time($path,$file='')
{
$path=$this->transact_path($path);
if($this->is_utf8($path))
{
$path=iconv('utf-8','gbk',$path);
}
if($this->is_utf8($file))
{
$file=iconv('utf-8','gbk',$file);
}
return filemtime($path.$file);
}
private function get_dirs_cascade($path)
{
$files=scandir($path);
if(!empty($files))
{
foreach($files as $key=>$item)
{
if($item=='.'||$item=='..')
{
unset($files[$key]);
continue;
}
else
{
if(mb_substr($path,-1,1,'gbk')=='/')
{
$tmp=$path.$item;
}
else{
$tmp=$path.'/'.$item;
}
if(is_dir($tmp))
{
$files[$key]=$tmp;
$sub_files=$this->get_dirs_cascade($tmp);
if(!empty($sub_files))
{
$files[]=$sub_files;
}
}
else{
unset($files[$key]);
}
}
}
}
return $files;
}
private function get_files_cascade($path,$file_ext='')
{
$files=scandir($path);
if(!empty($files))
{
foreach($files as $key=>$item)
{
if($item=='.'||$item=='..')
{
unset($files[$key]);
continue;
}
else
{
if(mb_substr($path,-1,1,'gbk')=='/')
{
$tmp=$path.$item;
}
else{
$tmp=$path.'/'.$item;
}
if(is_dir($tmp))
{
$files[$key]=$this->get_files_cascade($tmp,$file_ext);
}
else
{
if(!empty($file_ext))
{
if($this->get_file_ext($tmp)==$file_ext)
{
$files[$key]=$tmp;
}
else{
unset($files[$key]);
}
}
else{
$files[$key]=$tmp;
}
}
}
}
}
return $files;
}
public function get_files($path,$file_ext='')
{
$path=$this->transact_path($path);
if($this->is_utf8($path))
{
$path=iconv('utf-8','gbk',$path);
}
if(!file_exists($path))
{
return array();
}
else{
$files_array=$this->get_files_cascade($path,$file_ext);
require_once(dirname(__FILE__) .'/cls_array.php');
$cls_array=new cls_array();
return $cls_array->flat_array($files_array);
}
}
public function get_dirs($path)
{
$path=$this->transact_path($path);
if($this->is_utf8($path))
{
$path=iconv('utf-8','gbk',$path);
}
if(!file_exists($path))
{
return array();
}
else{
$dirs=$this->get_dirs_cascade($path);
require_once(dirname(__FILE__) .'/cls_array.php');
$cls_array=new cls_array();
return $cls_array->flat_array($dirs);
}
}
public function get_file_ext($filename)
{
$filename=$this->transact_path($filename);
$filename=basename($filename);
$last_dot_pos=$this->get_strrpos($filename,'.');
if($last_dot_pos===false)
{
return false;
}
else{
return $this->get_substr($filename,$last_dot_pos+1,$this->get_strlen($filename)-$last_dot_pos);
}
}
}
?>
