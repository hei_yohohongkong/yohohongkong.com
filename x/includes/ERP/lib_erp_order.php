<?php
const ORDER_STATUS = ['','訂單錄入中','等待財務處理','等待主管審核訂單','審核已通過','審核未通過'];

if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}
function order_has_supplier($order_id)
{
	$sql="select supplier_id from ".$GLOBALS['ecs']->table('erp_order')." where order_id='".$order_id."'";
	$supplier_id=$GLOBALS['db']->getOne($sql);
	if(empty($supplier_id))
	{
		return false;
	}
	else
	{
		return true;
	}
}
function is_admin_order($order_id,$admin_id)
{
	$sql="select create_by from ".$GLOBALS['ecs']->table('erp_order')." where order_id='".$order_id."'";
	$create_by=$GLOBALS['db']->getOne($sql);
	if($create_by==false ||$create_by!=$admin_id)
	{
		return false;
	}
	else
	{
		return true;
	}
}
function is_order_exist($order_id='',$order_sn='')
{
	if(empty($order_id) &&empty($order_sn))
	{
		return false;
	}
	elseif(!empty($order_id) &&empty($order_sn))
	{
		$sql="select * from ".$GLOBALS['ecs']->table('erp_order')." where order_id='".$order_id."'";
		$result=$GLOBALS['db']->getRow($sql);
	}
	elseif(empty($order_id) &&!empty($order_sn))
	{
		$sql="select * as num from ".$GLOBALS['ecs']->table('erp_order')." where order_sn ='".$order_sn."'";
		$result=$GLOBALS['db']->getRow($sql);
	}
	return !empty($result)?$result : false;
}
function is_client_order_exist($order_id='',$order_sn='')
{
	if(empty($order_id) &&empty($order_sn))
	{
		return false;
	}
	elseif(!empty($order_id) &&empty($order_sn))
	{
		$sql="select * from ".$GLOBALS['ecs']->table('order_info')." where order_id='".$order_id."'";
		$result=$GLOBALS['db']->getRow($sql);
	}
	elseif(empty($order_id) &&!empty($order_sn))
	{
		$sql="select * from ".$GLOBALS['ecs']->table('order_info')." where order_sn ='".$order_sn."'";
		$result=$GLOBALS['db']->getRow($sql);
	}
	return !empty($result)?$result : false;
}
function get_order_count($paras=array(),$start=-1,$num=-1)
{
	$sql="select count(*) from ".$GLOBALS['ecs']->table('erp_order')." where 1 ";
	if(isset($paras['order_sn']) &&!empty($paras['order_sn']))
	{
		$sql.=" and order_sn like '%".$paras['order_sn']."%'";
	}
	if(isset($paras['order_status']) &&!empty($paras['order_status']))
	{
		$sql.=" and order_status='".$paras['order_status']."'";
	}
	if(isset($paras['start_time']) &&!empty($paras['start_time']))
	{
		$sql.=" and create_time >='".$paras['start_time']."'";
	}
	if(isset($paras['end_time']) &&!empty($paras['end_time']))
	{
		$sql.=" and create_time <='".$paras['end_time']."'";
	}
	if(isset($paras['supplier_id']) &&!empty($paras['supplier_id']))
	{
		$sql.=" and supplier_id='".$paras['supplier_id']."'";
	}
	if(isset($paras['purchaser_id']) &&!empty($paras['purchaser_id']))
	{
		$sql.=" and create_by ='".$paras['purchaser_id']."'";
	}
	if(isset($paras['agency_id']) &&intval($paras['agency_id']) >0)
	{
		$sql.=" and agency_id='".$paras['agency_id']."'";
	}
	if(isset($paras['delivery_status']) &&!empty($paras['delivery_status']))
	{
		$sql.=" and order_status='4' and warehouse_id is not null";
	}
	if(isset($paras['order_paid']) && !empty($paras['order_paid']))
	{
		if ($paras['order_paid'] == 'paid') {
			$sql.=" and fully_paid = true";
		} elseif ($paras['order_paid'] == 'not_paid') {
			$sql.=" and fully_paid = false";
		}
	}
	return $GLOBALS['db']->getOne($sql);
}
function get_order_list($paras=array(),$start=-1,$num=-1)
{
	global $_LANG, $_CFG;
	require_once(ROOT_PATH . 'languages/' . $_CFG['lang'] . '/admin/ERP.php');
	$sql = "SELECT o.*, s.`name` as supplier_name, s.`code` as supplier_code, IFNULL(o.`payment_term`, s.`payment_term`) as payment_term, au.`user_name` as purchaser, ag.`agency_name` " .
			" , group_concat(w.warehousing_id SEPARATOR ',') as warehousing_ids, group_concat(w.warehousing_sn SEPARATOR ',') as warehousing_sns " .
				", IF(o.order_status = 4, (SELECT count(*) FROM " . $GLOBALS['ecs']->table('erp_order_item') . " as eoi " .
					"WHERE eoi.order_id = o.order_id AND eoi.warehousing_qty < eoi.order_qty" .
				"), 0) as can_warehousing, GROUP_CONCAT(g.goods_name) as goods_name, GROUP_CONCAT(oi.goods_id) as goods_id, GROUP_CONCAT(g.goods_sn) as goods_sn " .
			"FROM " . $GLOBALS['ecs']->table('erp_order') . " as o " .
			"LEFT JOIN " . $GLOBALS['ecs']->table('erp_order_item') . " as oi ON oi.order_id = o.order_id " .
			"LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON g.goods_id = oi.goods_id " .
			"LEFT JOIN " . $GLOBALS['ecs']->table('erp_supplier') . " as s ON s.supplier_id = o.supplier_id " .
			"LEFT JOIN " . $GLOBALS['ecs']->table('admin_user') . " as au ON au.user_id = o.create_by " .
			"LEFT JOIN " . $GLOBALS['ecs']->table('agency') . " as ag ON ag.agency_id = o.agency_id " .
			"LEFT JOIN " . $GLOBALS['ecs']->table('erp_warehousing') . " as w ON w.order_id = o.order_id AND w.warehousing_style_id = 1 AND w.status = 3 " .
			" WHERE 1 ";
	$sort_by = 'o.order_id';
	$sort_order = 'DESC';
	if(isset($paras['order_sn']) &&!empty($paras['order_sn']))
	{
		$sql.=" AND o.order_sn like '%".$paras['order_sn']."%'";
	}
	if(isset($paras['order_status']) &&!empty($paras['order_status']))
	{
		$sql.=" AND o.order_status='".$paras['order_status']."'";
	}
	if(isset($paras['start_time']) &&!empty($paras['start_time']))
	{
		$sql.=" AND o.create_time >='".$paras['start_time']."'";
	}
	if(isset($paras['end_time']) &&!empty($paras['end_time']))
	{
		$sql.=" AND o.create_time <='".$paras['end_time']."'";
	}
	if(isset($paras['supplier_id']) &&!empty($paras['supplier_id']))
	{
		$sql.=" AND o.supplier_id='".$paras['supplier_id']."'";
	}
	if(isset($paras['purchaser_id']) &&!empty($paras['purchaser_id']))
	{
		$sql.=" AND o.create_by ='".$paras['purchaser_id']."'";
	}
	if($paras['type']!='')
	{
		$sql.=" AND o.type ='".$paras['type']."'";
	}
	if(isset($paras['agency_id']) &&intval($paras['agency_id']) >0)
	{
		$sql.=" AND o.agency_id='".$paras['agency_id']."'";
	}
	if(isset($paras['payable']) &&!empty($paras['payable']))
	{
		$sql.=" AND o.fully_paid = 0";
	}
	if(isset($paras['good_key']) &&!empty($paras['good_key']))
	{
		$sql.=" AND (oi.goods_id REGEXP '".$paras['good_key']."' OR g.goods_sn REGEXP '".$paras['good_key']."' OR g.goods_name REGEXP '".$paras['good_key']."') ";
	}
	if(isset($paras['pending_receive']) &&!empty($paras['pending_receive']))
	{
		// 已審核, 未有入貨單, 有預計送貨日期
		$sql.=" AND o.order_status = 4 AND w.warehousing_id IS NULL AND o.expected_delivery_time > 0";
		$sort_by = 'o.expected_delivery_time';
		$sort_order = 'ASC';
	}
	if(isset($paras['delivery_status']) &&!empty($paras['delivery_status']))
	{
		if ($paras['delivery_status'] == 'arrived'){
			$sql.=" and o.order_status='4' and w.warehousing_id is not null";
		} elseif($paras['delivery_status'] == 'in_transit') {
			// 已審核, 未有入貨單, 有預計送貨日期
			$sql.=" AND o.order_status = 4 AND w.warehousing_id IS NULL AND o.expected_delivery_time > 0";
			$sort_by = 'o.expected_delivery_time';
			$sort_order = 'ASC';
		}
	}
	if(isset($paras['order_paid']) && !empty($paras['order_paid']))
	{
		if ($paras['order_paid'] == 'paid') {
			$sql.=" and fully_paid = true";
		} elseif ($paras['order_paid'] == 'not_paid') {
			$sql.=" and fully_paid = false";
		}
	}
	if(isset($paras['pending_payment']) &&!empty($paras['pending_payment']))
	{
		// 已審核, 有入貨單
		$sql.=" AND o.order_status = 4 AND w.warehousing_id IS NOT NULL";
		$sort_by = 'o.locked_time';
		$sort_order = 'ASC';
	}
	$sql.=" GROUP BY o.order_id ";
	$sql.=" ORDER BY ";
	$sql.=" can_warehousing DESC, $sort_by $sort_order ";
	if(isset($paras['sort_by']) && !empty($paras['sort_by']) && isset($paras['sort_order']) && !empty($paras['sort_order']))
	{	
		$temp_sort_by = $paras['sort_by'];
		if ($paras['sort_by'] == "create_date"){$temp_sort_by = "create_time";}
		if ($paras['sort_by'] == "expected_delivery_date"){$temp_sort_by = "expected_delivery_time";}
		$sql.= ", ".$temp_sort_by." ".$paras['sort_order']." ";
	}
	if($start!=-1 &&$num!=-1)
	{
		if($start<=0)
		{
			$start=0;
		}
		$sql.=" LIMIT ".$start.",".$num;
	}
	$order_info=$GLOBALS['db']->getAll($sql);

	if(!empty($order_info))
	{
		// Order goods list
		$order_ids = array_map(function ($item) { return $item['order_id']; }, $order_info);
		$sql = "SELECT eoi.*, g.goods_thumb " .
				"FROM " . $GLOBALS['ecs']->table('erp_order_item') . " as eoi " .
				"LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON eoi.goods_id = g.goods_id " .
				"WHERE eoi.order_id " . db_create_in($order_ids);
		$order_goods = $GLOBALS['db']->getAll($sql);
		
		// Supplier list
		$available_supplier_ids = array_map(function ($supplier) { return $supplier['supplier_id']; }, suppliers_list_name_by_admin());
		
		foreach($order_info as $key => $item)
		{
			$order_id=$item['order_id'];
			$order_info[$key]['available_act']=get_order_available_act('','',$item);
			$order_summary=get_order_summary($order_id);
			$order_info[$key]['order_qty']=$order_summary['order_qty'];
			$order_info[$key]['order_amount']=$order_summary['order_amount'];
			$order_info[$key]['shipping_cost']=$order_summary['shipping_cost'];
			$order_info[$key]['create_date']=local_date('Y-m-d',$item['create_time']);
			$order_info[$key]['post_date']=local_date('Y-m-d',$item['post_time']);
			$order_info[$key]['rate_date']=local_date('Y-m-d',$item['rate_time']);
			$order_info[$key]['approve_date']=local_date('Y-m-d',$item['approve_time']);
			$order_info[$key]['expected_delivery_date']=($item['expected_delivery_time'])?local_date('Y-m-d',$item['expected_delivery_time']): 'N/A';
			$order_info[$key]['delivery_date'] = ($item['warehousing_ids']) ? getPurchaseOrderDeliveryDate($item['warehousing_ids']) : "N/A";
			$order_info[$key]['converted_payment_term'] = $_LANG['erp_supplier_payment_term_'.$item['payment_term']];
			$order_info[$key]['payment_date'] = getPurchaseOrderPaymentDate($item['warehousing_ids'], $item['payment_term'], $item['can_warehousing']);
			
			// Extract order_goods for this order, and find most expensive goods
			$order_info[$key]['goods_list'] = array();
			$most_expensive = -1;
			foreach ($order_goods as $og)
			{
				if ($og['order_id'] == $order_id)
				{
					$order_info[$key]['goods_list'][] = $og;
					if ($og['price'] > $most_expensive)
					{
						$most_expensive = $og['price'];
						$order_info[$key]['most_expensive_goods'] = $og;
						$order_info[$key]['ex_goods_thumb'] = $og['goods_thumb'];
					}
				}
			}
			
			// 2015-02-02 Temp change: Sometimes we may pay more to supplier as "deposit", so allow negative value to be shown even fully_paid
			$order_info[$key]['due_amount'] = $order_info[$key]['order_amount'] + $order_info[$key]['shipping_cost'] - $item['actg_paid_amount'];
			if (($item['fully_paid']) && ($order_info[$key]['due_amount'] > 0)) $order_info[$key]['due_amount'] = 0;
			//$order_info[$key]['due_amount'] = $item['fully_paid'] ? 0 : $order_info[$key]['order_amount'] - $item['actg_paid_amount'];
			$order_info[$key]['order_amount_formatted'] = price_format($order_info[$key]['order_amount'], false);
			$order_info[$key]['due_amount_formatted'] = price_format($order_info[$key]['due_amount'], false);
			$order_info[$key]['shipping_cost_formatted'] = price_format($order_info[$key]['shipping_cost'], false);
			$order_info[$key]['checkbox'] = "<input name='order_check' class='flat' type='checkbox' value='" . round($order_info[$key]['due_amount'], 2) . "' data-id='$order_id'' data-sid='$item[supplier_id]'>";
			
			$order_info[$key]['type_id'] = $order_info[$key]['type'];
			
			if(!$order_info[$key]['type'] || $order_info[$key]['type']==0){
				$order_info[$key]['type'] = Yoho\cms\Controller\ErpController::PO_ORDER_TYPE[0];
			}else{
				$order_info[$key]['type'] = Yoho\cms\Controller\ErpController::PO_ORDER_TYPE[$order_info[$key]['type']];
			}
			
			// If admin is not authorized for that supplier, mask the supplier name
			if (!$_SESSION['manage_cost'] && !$_SESSION['is_accountant'] && !in_array($item['supplier_id'], $available_supplier_ids))
			{
				$order_info[$key]['supplier_name'] = '供應商 #' . $item['supplier_id'];
			}

			$html = '';
			if($order_info[$key]['available_act']['confirm'] == 1 ){
				$html .= '<a href="erp_order_manage.php?act=edit_order&amp;order_id='.$item['order_id'].'">'._L('erp_operation_edit','').'</a>';
				$html .= '<a href="javascript:delete_order('.$item['order_id'].')">'._L('erp_operation_delete','').'</a>';
				$html .= '<a href="javascript:one_key_confirm('.$item['order_id'].')">提交確認</a>';
			} else {
				if($order_info[$key]['available_act']['view'] == 1 ){
					// $html .= '<a href="erp_order_manage.php?act=view_order&amp;order_id='.$item['order_id'].'">'._L('erp_operation_view','').'</a>';
				}
				if($order_info[$key]['available_act']['edit'] == 1){$html .= '<a href="erp_order_manage.php?act=edit_order&amp;order_id='.$item['order_id'].'">'._L('erp_operation_edit','').'</a>';}
				if($order_info[$key]['available_act']['delete'] == 1){$html .= '<a href="javascript:delete_order('.$item['order_id'].')">'._L('erp_operation_delete','').'</a>';}
				if($order_info[$key]['available_act']['post_to_rate'] == 1){$html .= '<a href="javascript:post_to_rate('.$item['order_id'].')">'._L('erp_operation_post_to_finance','').'</a>';}

				if($order_info[$key]['available_act']['rate'] == 1 && check_authz('erp_order_rate')){
					$html .= '<a href="erp_order_manage.php?act=rate_order&amp;order_id='.$item['order_id'].'">'._L('erp_operation_rate','').'</a>';
					$html .= '<a href="javascript:withdrawal_to_edit('.$item['order_id'].')">'._L('erp_operation_withdrawal','').'</a>';
				}

				if($order_info[$key]['available_act']['post_to_approve'] == 1 && check_authz('erp_order_rate')){
					$html .= '<a href="javascript:post_to_approve('.$item['order_id'].')">'._L('erp_operation_post_to_approve','').'</a>';
				}
				if($order_info[$key]['available_act']['approve'] == 1 && check_authz('erp_order_approve')){
					$html .= '<a href="erp_order_manage.php?act=approve_order&amp;order_id='.$item['order_id'].'">'._L('erp_operation_approve','').'</a>';
					$html .= '<a href="javascript:one_key_approve('.$item['order_id'].')">一鍵審核</a>';
					$html .= '<a href="javascript:withdrawal_to_rate('.$item['order_id'].')">'._L('erp_operation_withdrawal','').'</a>';
				}
				if( $item['order_status'] == 4 || $item['order_status'] == 5){
					// $html .= '<a href="erp_order_manage.php?act=view_order&amp;order_id='.$item['order_id'].'">'._L('erp_operation_view','').'</a>';
				}

				if( $item['order_status'] == 4){
					if((check_authz(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ONLYSELFSUPPLIERS,'',false) && in_array($item['supplier_id'], $available_supplier_ids)) || $_SESSION['manage_cost']|| is_supplier_secondary_admin($item['supplier_id'],$_SESSION['admin_id'])) {
						$html .= '<a target="_blank" href="erp_order_manage.php?act=print_order&amp;order_id='.$item['order_id'].'&amp;page=1">'._L('erp_operation_print','').'</a>';
						if (!($item['fully_paid'] && !$item['can_warehousing']) && is_admin_order($item['order_id'],erp_get_admin_id())) {
							$html .= '<a href="javascript:revise('.$item['order_id'].')">修訂</a>';
						}
					}
					if( $item['can_warehousing'] && (in_array($_SESSION['role_id'], [11, 13, 14, 18]) || $_SESSION['manage_cost'])){
						$html .= '<a href="javascript:create_warehousing_by_order('.$item['order_id'].')">立即入貨</a>';
						//$html .= '<a href="javascript:create_warehousing('.$item['order_id'].')">(立即入貨old version)</a>';
					}
					if( $item['warehousing_id']){
						$html .= '<a href="erp_warehousing_manage.php?act=list&amp;order_id='.$item['order_id'].'">入貨單</a>';
					}
					if (check_authz(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_PO_UNAPPROVE)) {
						if ($item['fully_paid'] && !$item['can_warehousing']) {

						} elseif (($item['warehousing_id'] || bccomp($item['actg_paid_amount'], 0) != 0) && $item['revise_id'] > 0) {
							if ($GLOBALS['db']->getOne("SELECT 1 FROM " . $GLOBALS['ecs']->table('erp_order_revise') . " WHERE revise_id = $item[revise_id] AND status = " . PO_REVISE_SUBMITTED)) {
								$html .= '<a href="javascript:revise('.$item['order_id'].')">審核修訂</a>';
							}
						} elseif (!$item['fully_paid']) {
							$html .= '<a href="javascript:unapprove('.$item['order_id'].')">反審核</a>';
						}
					}
					if (!empty($item['revise_id'])) {
						if ((check_authz(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ONLYSELFSUPPLIERS,'',false) && in_array($item['supplier_id'], $available_supplier_ids)) || $_SESSION['manage_cost'] || check_authz(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_PO_UNAPPROVE)) {
							$html .= '<a href="erp_order_revise.php?act=list&order_sn='.$item['order_sn'].'">修訂紀錄</a>';
						}
					}
					if ($_SESSION['is_accountant'] || $_SESSION['manage_cost']) {
						if( $item['fully_paid']){
							if (check_authz('erp_order_finish')) {
								$html .= '<a href="purchase_payments.php?act=list&amp;order_sn='.$item['order_sn'].'">付款紀錄</a>';
							}
							if (check_authz('erp_order_receipt')) {
								if( $item['receipt_location']){
									$html .= '<a href="javascript:retrieve_payment_receipt('.$item['order_id'].');">'._L('erp_operation_check_receipt','').'</a>';
								} else {
									$html .= '<a href="javascript:upload_receipt('.$item['order_id'].');">'._L('erp_operation_send_email','').'</a>';
								}
							}
						} else {
							if (check_authz('erp_order_finish')) {
								$html .= '<a href="purchase_payments.php?act=add&amp;order_id='.$item['order_id'].'">付款</a>';
								$html .= '<a href="javascript:;" onclick="javascript:completeOrderPayment('.$item['order_id'].')">完成</a>';
							}
						}
					}
				}
			}
			$order_info[$key]['_action'] = $html;
		}
	}
	return $order_info;
}
function get_order_info($order_id)
{
	$sql="select * from ".$GLOBALS['ecs']->table('erp_order')." where order_id='".$order_id."'";
	$order_info=$GLOBALS['db']->getRow($sql);
	if(!empty($order_info))
	{
		$order_id=$order_info['order_id'];
		$order_info['available_act']=get_order_available_act($order_id,'');
		$supplier_id=$order_info['supplier_id'];
		$sql="select name,code from ".$GLOBALS['ecs']->table('erp_supplier')." where supplier_id='".$supplier_id."'";
		$supplier_info=$GLOBALS['db']->getRow($sql);
		$order_info['supplier_name']=$supplier_info['name'];
		$order_info['supplier_code']=$supplier_info['code'];

		// get warehouse name
		$sql = "select name,description from ".$GLOBALS['ecs']->table('erp_warehouse')." where warehouse_id = '".$order_info['warehouse_id']."' " ;
		$warehouse_info=$GLOBALS['db']->getRow($sql);
		$order_info['warehouse_name'] = $warehouse_info['name'];

		if(!$order_info['type'] || $order_info['type']==0){
			$order_info['type_name'] = Yoho\cms\Controller\ErpController::PO_ORDER_TYPE[0];
		}else{
			$order_info['type_name'] = Yoho\cms\Controller\ErpController::PO_ORDER_TYPE[$order_info['type']];
		}
		$order_summary=get_order_summary($order_id);
		$order_info['order_qty']=$order_summary['order_qty'];
		$order_info['order_amount']=$order_summary['order_amount']+$order_summary['shipping_cost'];
		$order_info['shipping_cost']=$order_summary['shipping_cost'];
		$order_info['original_order_amount']=$order_summary['original_order_amount']+$order_summary['original_shipping_cost'];
		$order_info['create_date']=local_date('Y-m-d',$order_info['create_time']);
		$order_info['post_date']=local_date('Y-m-d',$order_info['post_time']);
		$order_info['rate_date']=local_date('Y-m-d',$order_info['rate_time']);
		$order_info['approve_date']=local_date('Y-m-d',$order_info['approve_time']);
		$order_info['expected_delivery_date']=local_date('Y-m-d',$order_info['expected_delivery_time']);
		

		// If admin is not authorized for that supplier, mask the supplier name
		if (!(is_supplier_admin($supplier_id, $_SESSION['admin_id']) || $order_info['create_by']==$_SESSION['admin_id'] || is_supplier_secondary_admin($supplier_id,$_SESSION['admin_id']))) {
			$order_info['supplier_name'] = '供應商 #' . $supplier_id;
		}
	}
	return $order_info;
}
function get_order_item_info($order_id='',$order_item_id='')
{
	if(empty($order_id) && empty($order_item_id))
	{
		return array();
	}
	else if(!empty($order_id) && empty($order_item_id))
	{
		$order_item_info=array();
		$sql="select * from ".$GLOBALS['ecs']->table('erp_order_item')." where order_id='".$order_id."' order by order_item_id desc";
		$order_items=$GLOBALS['db']->getAll($sql);
		if(!empty($order_items))
		{
			foreach($order_items as $key =>$item)
			{
				$goods_id=$item['goods_id'];
				$sql="select cat_id,goods_id,goods_sn,goods_name,goods_thumb,goods_img,shop_price,cost from ";
				$sql.=$GLOBALS['ecs']->table('goods')." where goods_id='".$goods_id."'";
				$goods_details=$GLOBALS['db']->getRow($sql);
				if(!empty($goods_details['goods_thumb']))
				{
					$goods_details['goods_thumb']='../'.$goods_details['goods_thumb'];
				}
				if(!empty($goods_details['goods_img']))
				{
					$goods_details['goods_img']='../'.$goods_details['goods_img'];
				}
				$goods_details['shop_price'] = price_format($goods_details['shop_price'], false);
				$goods_details['cost'] = price_format($goods_details['cost'], false);
				$order_items[$key]['goods_details']=$goods_details;
				$order_items[$key]['goods_attr']=goods_attr($item['goods_id']);
				$attr_info=get_attr_info($item['attr_id']);
				if(isset($attr_info['attr_info']))
				{
					$order_items[$key]['selected_attr']=$attr_info['attr_info'];
				}
			}
			return $order_items;
		}
		else
		{
			return array();
		}
	}
	else if(empty($order_id) && !empty($order_item_id))
	{
		$sql = "SELECT * FROM " . $GLOBALS['ecs']->table('erp_order_item') . " eoi" .
				" LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " g ON g.goods_id = eoi.goods_id" .
				" WHERE order_item_id = " . $order_item_id;
		return $GLOBALS['db']->getRow($sql);
	}
}
function delete_order_item($oder_id='',$order_item_id='')
{
	if(!empty($oder_id) &&empty($order_item_id))
	{
		$sql = "SELECT order_item_id FROM " . $GLOBALS['ecs']->table('erp_order_item') . " WHERE order_id = " . $oder_id;
		$order_item_ids = $GLOBALS['db']->getCol($sql);
		$order_info = get_order_info($oder_id);
		if(count($order_item_ids) > 0){
			$action_remark = "編輯訂單 $order_info[order_sn] 產品 | 移除 ";
			$gn = [];
			foreach($order_item_ids as $order_item_id){
				$gn[] = get_order_item_info("",$order_item_id)['goods_name'];
			}
			$sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
					"VALUES(" . $oder_id . ", " . ERP_UPDATE . "," . $_SESSION['admin_id'] . ",'" . mysql_escape_string($action_remark . implode(', ',$gn)) . "')";
			$GLOBALS['db']->query($sql);
			admin_log($order_info['order_sn'],'remove','erp_order_item');
		}
		$sql="delete from ".$GLOBALS['ecs']->table('erp_order_item')." where order_id='".$oder_id."'";
	}
	elseif(empty($oder_id) &&!empty($order_item_id))
	{
		$oder_id = get_order_item_info("",$order_item_id)['order_id'];
		$order_info = get_order_info($oder_id);
		$action_remark = "編輯訂單 $order_info[order_sn] 產品 | 移除 " . get_order_item_info("",$order_item_id)['goods_name'];
		$sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
				"VALUES(" . $oder_id . ", " . ERP_UPDATE . "," . $_SESSION['admin_id'] . ",'" . mysql_escape_string($action_remark) . "')";
		$GLOBALS['db']->query($sql);
		admin_log($order_info['order_sn'],'remove','erp_order_item');
		$sql="delete from ".$GLOBALS['ecs']->table('erp_order_item')." where order_item_id='".$order_item_id."'";
	}
	else
	{
		return false;
	}
	$GLOBALS['db']->query($sql);
	return true;
}
function delete_client_details($oder_id='')
{
	if(!empty($oder_id))
	{
		$sql="update ".$GLOBALS['ecs']->table('erp_order')." set client_name='', client_tel='', client_mobile='', client_address='' where order_id='".$oder_id."'";
	}
	else
	{
		return false;
	}
	$GLOBALS['db']->query($sql);
	return true;
}
function get_order_summary($order_id='')
{
	if(empty($order_id))
	{
		return false;
	}
	else
	{
		$sql="select sum(order_qty) as order_qty, sum(amount - cn_amount) as order_amount, sum(shipping_price) as shipping_cost, sum(original_amount - cn_amount) as original_order_amount, sum(original_shipping_price) as original_shipping_cost from ".$GLOBALS['ecs']->table('erp_order_item')." where order_id='".$order_id."' group by order_id";
		return $GLOBALS['db']->getRow($sql);
	}
}
function check_order_integrity($order_id='')
{
	if(empty($order_id))
	{
		return false;
	}
	$order_info=get_order_info($order_id);
	$order_item_info=get_order_item_info($order_id);
	$supplier_id=$order_info['supplier_id'];
	if(empty($supplier_id))
	{
		return 1;
	}
	if(empty($order_item_info))
	{
		return 2;
	}
	foreach($order_item_info as $order_item)
	{
		$goods_id=$order_item['goods_id'];
		if(empty($goods_id))
		{
			return 3;
		}
		else
		{
			$goods_attr=goods_attr($goods_id);
			if(!empty($goods_attr) &&empty($order_item['selected_attr']))
			{
				return 4;
			}
			if(empty($order_item['order_qty']))
			{
				return 5;
			}
		}
	}
	return true;
}
function add_order($supplier_id=0)
{
	$user_id=erp_get_admin_id();
	$order_sn=gen_order_sn();
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	$payment_term = get_supplier_payment_term($supplier_id);
	$sql="insert into ".$GLOBALS['ecs']->table('erp_order')." set ";
	$sql.="order_sn='".$order_sn."',";
	$sql.="supplier_id='".$supplier_id."',";
	$sql.="create_time='".gmtime()."',";
	$sql.="create_by='".$user_id."',";
	$sql.="is_locked ='0',";
	$sql.="order_status ='1',";
	$sql.="agency_id='".$agency_id."',";
	$sql .= "payment_term='" . $payment_term . "'";
	
	// As the admin account #4 is shared by all employees in sales team, we don't want it to fill in automatically
	if ($_SESSION['admin_id'] != 4)
	{
		$sql.=",operator = '".$_SESSION['admin_name']."'";
	}

	if($GLOBALS['db']->query($sql))
	{
		$sql="select order_id from ".$GLOBALS['ecs']->table('erp_order')." where order_sn='".$order_sn."'";
		$order_id=$GLOBALS['db']->getOne($sql);
		$action_remark = "添加訂單 " . $order_sn;
		$sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
				"VALUES(" . $order_id . ", " . ERP_CREATE . "," . $_SESSION['admin_id'] . ",'" . $action_remark . "')";
		$GLOBALS['db']->query($sql);
		admin_log($order_sn,'add','erp_order');
		return $order_id;
	}
	else
	{
		return false;
	}
}
function post_order($order_id='')
{
	if($order_info = is_order_exist($order_id))
	{
		$sql="update ".$GLOBALS['ecs']->table('erp_order')." set order_status='2' where order_id='".$order_id."'";
		if($GLOBALS['db']->query($sql))
		{
			$action_remark = "編輯訂單 $order_info[order_sn] 狀態 | 由 " . ORDER_STATUS[$order_info['order_status']] . " 至 " . ORDER_STATUS[2];
			$sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
					"VALUES(" . $order_id . ", " . ERP_UPDATE . "," . $_SESSION['admin_id'] . ",'" . mysql_escape_string($action_remark) . "')";
			$GLOBALS['db']->query($sql);
			admin_log($order_info['order_sn'],'submit_edit','erp_order');
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}
function has_order_item($order_id='')
{
	if(empty($order_id))
	{
		return false;
	}
	else
	{
		$sql="select order_item_id from ".$GLOBALS['ecs']->table('erp_order_item')." where order_id='".$order_id."'";
		$res=$GLOBALS['db']->query($sql);
		while(list($order_item_id)=mysql_fetch_row($res))
		{
			$order_item_id_array[]=$order_item_id;
		}
		if(empty($order_item_id_array))
		{
			return false;
		}
		else
		{
			return $order_item_id_array;
		}
	}
}
function order_act_record($order_id='',$act='')
{
	if(empty($order_id))
	{
		return false;
	}
	if(!is_order_exist($order_id))
	{
		return false;
	}
	if(empty($act))
	{
		return false;
	}
	$user_id=erp_get_admin_id();
	$current_time=gmtime();
	if($act=='confirm')
	{
		$sql = "UPDATE ".$GLOBALS['ecs']->table('erp_order')." ".
				"SET create_by = '".$user_id."' , create_time = '".$current_time."' ".
				", post_by = '".$user_id."' , post_time = '".$current_time."' ".
				", rate_by = '".$user_id."' , rate_time = '".$current_time."' ".
				", submit_by = '".$user_id."' , submit_time = '".$current_time."' ".
				", approve_by = '".$user_id."' , approve_time = '".$current_time."' ".
				"WHERE order_id='".$order_id."'";
	}
	elseif($act=='create')
	{
		$sql="update ".$GLOBALS['ecs']->table('erp_order')." set create_by='".$user_id."'";
		$sql.=", create_time='".$current_time."' where order_id='".$order_id."'";
	}
	elseif($act=='post')
	{
		$sql="update ".$GLOBALS['ecs']->table('erp_order')." set post_by='".$user_id."'";
		$sql.=", post_time='".$current_time."' where order_id='".$order_id."'";
	}
	elseif($act=='rate')
	{
		$sql="update ".$GLOBALS['ecs']->table('erp_order')." set rate_by ='".$user_id."'";
		$sql.=", rate_time='".$current_time."' where order_id='".$order_id."'";
	}
	elseif($act=='approve')
	{
		$sql="update ".$GLOBALS['ecs']->table('erp_order')." set approve_by ='".$user_id."'";
		$sql.=", approve_time='".$current_time."' where order_id='".$order_id."'";
	}
	elseif($act=='submit')
	{
		$sql="update ".$GLOBALS['ecs']->table('erp_order')." set submit_by  ='".$user_id."'";
		$sql.=", submit_time ='".$current_time."' where order_id='".$order_id."'";
	}
	$GLOBALS['db']->query($sql);
	return true;
}
function delete_order($order_id='')
{
	if(empty($order_id))
	{
		return false;
	}
	$order_info = get_order_info($order_id);
	$sql="delete from ".$GLOBALS['ecs']->table('erp_order')." where order_id='".$order_id."'";
	$GLOBALS['db']->query($sql);
	$action_remark = "移除訂單 " . $order_info['order_sn'];
	$sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
			"VALUES(" . $order_id . ", " . ERP_REMOVE . "," . $_SESSION['admin_id'] . ",'" . $action_remark . "')";
	$GLOBALS['db']->query($sql);
	admin_log($order_info['order_sn'],'remove','erp_order');
	return true;
}
function is_order_item_exist($order_item_id='')
{
	if(empty($order_item_id))
	{
		return false;
	}
	else
	{
		$sql="select * from ".$GLOBALS['ecs']->table('erp_order_item')." where order_item_id ='".$order_item_id."'";
		$result=$GLOBALS['db']->getRow($sql);
		return !empty($result)?$result : false;
	}
}
$expired_time=3600;
function lock_order($order_id='',$act='')
{
	if(empty($order_id))
	{
		return false;
	}
	$GLOBALS['db']->query('START TRANSACTION');
	if(check_accessibility($order_id,$act))
	{
		$user_id=erp_get_admin_id();
		$lock_time=gmtime();
		$last_act_time=gmtime();
		$sql="update ".$GLOBALS['ecs']->table('erp_order')." set locked_by='".$user_id."'";
		$sql.=", locked_time='".$lock_time."'";
		$sql.=", last_act_time='".$last_act_time."'";
		$sql.=", is_locked='1'";
		$sql.=" where order_id='".$order_id."'";
		if($GLOBALS['db']->query($sql))
		{
			$GLOBALS['db']->query('COMMIT');
			return true;
		}
		else
		{
			$GLOBALS['db']->query('ROLLBACK');
			return false;
		}
	}
	else
	{
		return false;
	}
}
function access_order($order_id='',$act='')
{
	if(empty($order_id))
	{
		return false;
	}
	$GLOBALS['db']->query('START TRANSACTION');
	if(check_accessibility($order_id,$act))
	{
		$user_id=!empty($_COOKIE['ECSCP']['admin_id'])?$_COOKIE['ECSCP']['admin_id']:'';
		$user_id=isset($_SESSION['admin_id'])?$_SESSION['admin_id']:'';
		$lock_time=gmtime();
		$last_act_time=gmtime();
		$sql="update ".$GLOBALS['ecs']->table('erp_order')." set last_act_time='".$last_act_time."'";
		$sql.=" where order_id='".$order_id."'";
		if($GLOBALS['db']->query($sql))
		{
			$GLOBALS['db']->query('COMMIT');
			return true;
		}
		else
		{
			$GLOBALS['db']->query('ROLLBACK');
			return false;
		}
	}
	else
	{
		return false;
	}
}
function check_accessibility($order_id='',$act='')
{
	if(empty($act) &&empty($order_id))
	{
		return false;
	}
	else
	{
		$sql="select is_locked,order_status,last_act_time,locked_by,type from ".$GLOBALS['ecs']->table('erp_order')." where order_id='".$order_id."' for update";
		$res=$GLOBALS['db']->getRow($sql);
		if($res==false)
		{
			return false;
		}
		else
		{
			$is_locked=$res['is_locked'];
			$order_status=$res['order_status'];
			$last_act_time=$res['last_act_time'];
			$locked_by=$res['locked_by'];
			$type=$res['type'];
			$current_time=gmtime();
			$if_overtime=(($current_time-$last_act_time)>(10))?1:0;
			$current_user_id=erp_get_admin_id();
			if($act=='view')
			{
				return(admin_priv('erp_order_view','',false));
			}
			elseif($act=='edit'||$act=='delete'||$act=='post_to_rate')
			{
				if($order_status==1)
				{
					if(admin_priv('erp_order_manage','',false) &&($is_locked==0 ||($is_locked==1 &&$if_overtime==1 &&$current_user_id!=$locked_by) ||($is_locked==1 &&$current_user_id==$locked_by)))
					{
						return true;
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
			elseif($act=='rate'||$act=='post_to_approve')
			{
				//if($order_status <= 2)
				//keep process for other type of erp_order
				if($order_status == 2 || ($type == 2 && $order_status <= 2))
				{
					if(admin_priv('erp_order_rate','',false) &&($is_locked==0 ||($is_locked==1 &&$if_overtime==1 &&$current_user_id!=$locked_by) ||($is_locked==1 &&$current_user_id==$locked_by)))
					{
						return true;
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
			elseif($act=='approve' || $act=='to_confirm')
			{
				if($order_status == 3 || ($order_status == 1 && $type == 2))
				{
					if(admin_priv('erp_order_approve','',false) &&($is_locked==0 ||($is_locked==1 &&$if_overtime==1 &&$current_user_id!=$locked_by) ||($is_locked==1 &&$current_user_id==$locked_by)))
					{
						return true;
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
	}
}
function gen_order_sn()
{
	//$sql="select order_sn from ".$GLOBALS['ecs']->table('erp_order')." where order_sn like 'PO%' order by order_sn desc limit 1";
	$sql="select order_sn from ".$GLOBALS['ecs']->table('erp_order')." where order_sn like 'PO%' order by order_id desc limit 1";
	$result=$GLOBALS['db']->getOne($sql);
	if(!empty($result))
	{
		$year_date=substr($result,2,4);
		$current_year_date=local_date("ym");
		if($current_year_date==$year_date)
		{
			$sn=substr($result,6);
			$sn=intval($sn)+1;
			$sn=gen_zero_str(4-strlen($sn)).$sn;
			$sn='PO'.$year_date.$sn;
			return $sn;
		}
		else
		{
			$sn='PO'.$current_year_date."0001";
			return $sn;
		}
	}
	else
	{
		$current_year_date=local_date("ym");
		$sn='PO'.$current_year_date."0001";
		return $sn;
	}
}
function get_order_available_act($order_id='',$order_sn='',$res=false)
{
	$sql='';
	$act=array('view'=>0,'edit'=>0,'delete'=>0,'post_to_rate'=>0,'rate'=>0,'post_to_approve'=>0,'approve'=>0);
	$is_locked='';
	$order_status='';
	if(empty($order_id) && empty($order_sn) && !$res)
	{
		return false;
	}
	if(empty($order_sn) && !empty($order_id))
	{
		$sql="select order_id,is_locked,order_status,last_act_time,locked_by,type from ".$GLOBALS['ecs']->table('erp_order')." where order_id='".$order_id."'";
	}
	elseif(!empty($order_sn) && empty($order_id))
	{
		$sql="select order_id,is_locked,order_status,last_act_time,locked_by,type from ".$GLOBALS['ecs']->table('erp_order')." where order_sn='".$order_sn."'";
	}
	if (!$res)
	{
		$res=$GLOBALS['db']->getRow($sql);
	}
	if($res===false)
	{
		return false;
	}
	else
	{
		$order_id=$res['order_id'];
		$is_locked=$res['is_locked'];
		$order_status=$res['order_status'];
		$last_act_time=$res['last_act_time'];
		$locked_by=$res['locked_by'];
		$type=$res['type'];
	}
	$current_time=gmtime();
	$if_overtime=(($current_time-$last_act_time)>(10))?1:0;
	$current_user_id=!empty($_COOKIE['ECSCP']['admin_id'])?$_COOKIE['ECSCP']['admin_id']:'';
	$current_user_id=isset($_SESSION['admin_id'])?$_SESSION['admin_id']:$current_user_id;
	if($order_status==1)
	{
		if(admin_priv('erp_order_view','',false) ||admin_priv('erp_order_manage','',false) ||admin_priv('erp_order_rate','',false) ||admin_priv('erp_order_approve','',false))
		{
			$act['view']=1;
		}
		else
		{
			$act['view']=0;
		}

		//keep process for other type of erp_order
		if ($type == 2){
			$act['edit']=1;
			$act['delete']=1;
			$act['post_to_rate']=0;
			$act['rate']=1;
			$act['post_to_approve']=0;
			$act['approve']=admin_priv('erp_order_approve','',false)?1:0;
			$act['confirm']=admin_priv('erp_order_approve','',false)?1:0;
			return $act;
		} else {
			if(admin_priv('erp_order_manage','',false) &&($is_locked==0 ||($is_locked==1 &&$if_overtime==1 &&$current_user_id!=$locked_by) ||($is_locked==1 &&$current_user_id==$locked_by)))
			{
				$act['edit']=is_admin_order($order_id,erp_get_admin_id())?1: 0;
				$act['delete']=is_admin_order($order_id,erp_get_admin_id())?1: 0;
				$act['post_to_rate']=is_admin_order($order_id,erp_get_admin_id())?1: 0;
			}
			else
			{
				$act['edit']=0;
				$act['delete']=0;
				$act['post_to_rate']=0;
			}
			$act['rate']=0;
			$act['post_to_approve']=0;
			$act['approve']=0;
			return $act;
		}
	}
	elseif($order_status==2)
	{
		if(admin_priv('erp_order_view','',false) ||admin_priv('erp_order_manage','',false) ||admin_priv('erp_order_rate','',false) ||admin_priv('erp_order_approve','',false))
		{
			$act['view']=1;
		}
		else
		{
			$act['view']=0;
		}
		$act['edit']=0;
		$act['delete']=0;
		$act['post_to_rate']=0;
		if(admin_priv('erp_order_rate','',false) &&($is_locked==0 ||($is_locked==1 &&$if_overtime==1 &&$current_user_id!=$locked_by) ||($is_locked==1 &&$current_user_id==$locked_by)))
		{
			$act['rate']=1;
			$act['post_to_approve']=1;
		}
		else
		{
			$act['rate']=0;
			$act['post_to_approve']=0;
		}
		$act['approve']=0;
		return $act;
	}
	elseif($order_status==3)
	{
		if(admin_priv('erp_order_view','',false) ||admin_priv('erp_order_manage','',false) ||admin_priv('erp_order_rate','',false) ||admin_priv('erp_order_approve','',false))
		{
			$act['view']=1;
		}
		else
		{
			$act['view']=0;
		}
		$act['edit']=0;
		$act['delete']=0;
		$act['post_to_rate']=0;
		$act['rate']=0;
		$act['post_to_approve']=0;
		$act['approve']=admin_priv('erp_order_approve','',false)?1:0;
		return $act;
	}
	elseif($order_status==4 ||$order_status==5)
	{
		$act['view']=0;
		$act['edit']=0;
		$act['delete']=0;
		$act['post_to_rate']=0;
		$act['rate']=0;
		$act['post_to_approve']=0;
		$act['approve']=0;
		return $act;
	}
	else
	{
		return false;
	}
}
function get_order_status()
{
	$order_status[0]['status_no']=1;
	$order_status[0]['status_style']="訂單錄入中";
	$order_status[1]['status_no']=2;
	$order_status[1]['status_style']="財務處理中";
	$order_status[2]['status_no']=3;
	$order_status[2]['status_style']="主管審核中";
	$order_status[3]['status_no']=4;
	$order_status[3]['status_style']="審核已通過";
	$order_status[4]['status_no']=5;
	$order_status[4]['status_style']="審核未通過";
	return $order_status;
}
function is_supplier_code_exist($supplier_id=0,$code='')
{
	$sql="select * from ".$GLOBALS['ecs']->table('erp_supplier')." where code='".$code."' and supplier_id<>'".$supplier_id."'";
	$result=$GLOBALS['db']->getRow($sql);
	if($result==false)
	{
		return false;
	}
	else
	{
		return true;
	}
}
function reformat_image_name($sn,$sub_dir,$source_img)
{
	$img_ext = substr($source_img,strrpos($source_img,'.'));
	$dir = 'images';
	if (!make_dir(ROOT_PATH.$dir.'/'.$sub_dir))
	{
		return false;
	}
	$img_name = $sn.$img_ext;
	if (move_image_file(ROOT_PATH.$source_img,ROOT_PATH.$dir.'/'.$sub_dir.'/'.$img_name))
	{
		return '../'.$dir.'/'.$sub_dir.'/'.$img_name;
	}
	return false;
}
function move_image_file($source,$dest)
{
	if (@copy($source,$dest))
	{
		@unlink($source);
		return true;
	}
	return false;
}
function gen_credit_note_sn()
{
	$sql="select cn_sn from ".$GLOBALS['ecs']->table('erp_credit_note')." where cn_sn like 'CN%' order by cn_id desc limit 1";
	$result=$GLOBALS['db']->getOne($sql);
	if(!empty($result))
	{
		$year_date=substr($result,2,4);
		$current_year_date=local_date("ym");
		if($current_year_date==$year_date)
		{
			$sn=substr($result,6,3);
			$sn=intval($sn)+1;
			$sn=gen_zero_str(3-strlen($sn)).$sn;
			$sn='CN'.$year_date.$sn;
			return $sn;
		}
		else
		{
			$sn='CN'.$current_year_date."001";
			return $sn;
		}
	}
	else
	{
		$current_year_date=local_date("ym");
		$sn='CN'.$current_year_date."001";
		return $sn;
	}
}
function get_credit_note($cn_ids = array(), $supplier_id = "", $sort_by = "")
{
	!empty($cn_ids) ? (!is_array($cn_ids) ? $cn_ids = [$cn_ids] : true) : true;
	if(!empty($cn_ids) && empty($supplier_id)) $where = " WHERE ecn.cn_id IN (" . implode(',', $cn_ids) . ")";
	if(empty($cn_ids) && !empty($supplier_id)) $where = " WHERE es.supplier_id = $supplier_id";
	return $GLOBALS['db']->getAll(
		"SELECT * FROM " . $GLOBALS['ecs']->table('erp_credit_note') . " ecn " .
		"LEFT JOIN " . $GLOBALS['ecs']->table('erp_supplier') . " es ON ecn.supplier_id = es.supplier_id" .
		$where . $sort_by
	);
}
function add_credit_note($param)
{
	if (empty($param)) return false;
	if (!erp_param_contains_keys($param, array(
		'cn_sn', 'supplier_id', 'admin_id', 'curr_amount', 'init_amount'
	))) return false;

	$GLOBALS['db']->query('START TRANSACTION');
	$remark = $param['remark'];
	unset($param['remark']);
	if($GLOBALS['db']->autoExecute($GLOBALS['ecs']->table('erp_credit_note'), $param, 'INSERT'))
	{
		$cn_id = $GLOBALS['db']->insert_id();
		$GLOBALS['db']->query('COMMIT');
		$param2 = array(
			'cn_id' => $cn_id,
			'supplier_id' => $param['supplier_id'],
			'amount' => $param['init_amount'],
			'txn_remark' => "供應商 Rebate $param[cn_sn]",
			'remark' => "新增: ". $param['cn_sn'] . (empty($remark) ? "" : " $remark")
		);
		$erpController = new Yoho\cms\Controller\ErpController();
		return $erpController->insert_credit_note_transaction($param2);
	}
	else
	{
		$GLOBALS['db']->query('ROLLBACK');
		return false;
	}
}
function erp_param_contains_keys($param, $required_keys)
{
	foreach($required_keys as $key)
	{
		if (!array_key_exists($key, $param))
		{
			return false;
		}
	}
	return true;
}
function sum_curr_credit_note_amount($cn_ids = array(), $supplier_id = "")
{
	if(!empty($cn_ids) && empty($supplier_id)) $where = " WHERE cn_id IN (" . implode(',', $cn_ids) . ")";
	if(empty($cn_ids) && !empty($supplier_id)) $where = " WHERE supplier_id = $supplier_id";
	if(!empty($cn_ids) || !empty($supplier_id))
	{
		return $GLOBALS['db']->getOne(
			"SELECT SUM(curr_amount) FROM " . $GLOBALS['ecs']->table('erp_credit_note') . $where
		);
	}
	return 0.0;
}
function getPurchaseOrderDeliveryDate($warehousing_ids)
{
	if (empty($warehousing_ids)) {
		return "N/A";
	}

	$warehousing_ids = explode(',', $warehousing_ids);
	$warehousing_ids = implode("','", $warehousing_ids);
	$warehousing_ids = "'$warehousing_ids'";

	$sql = "SELECT max(approve_time) FROM " . $GLOBALS['ecs']->table('erp_warehousing') . " WHERE warehousing_id in ($warehousing_ids)";

	$max_date = $GLOBALS['db']->getOne($sql);
	$result = "<span>" . ($max_date > 0 ? local_date("Y-m-d", $max_date) : "N/A") . "</span><br/>";
	return $result;
}
function getPurchaseOrderPaymentDate($warehousing_ids, $payment_term = null, $can_warehousing = false)
{
	if (empty($warehousing_ids) || $can_warehousing) {
		return "N/A";
	}

	$warehousing_ids = explode(',', $warehousing_ids);
	$warehousing_ids = implode("','", $warehousing_ids);
	$warehousing_ids = "'$warehousing_ids'";

	$sql = "SELECT max(approve_time) FROM " . $GLOBALS['ecs']->table('erp_warehousing') . " WHERE warehousing_id in ($warehousing_ids)";

	$last_delivery_datetime = $GLOBALS['db']->getOne($sql);

	$result = "<span>" . (!empty($last_delivery_datetime) ? calculateOrderPaymentDate($last_delivery_datetime, $payment_term) : "N/A") . "</span>";
	return $result;
}
function calculateOrderPaymentDate($locked_time, $payment_term)
{
	$format = "Y-m-d";
	$datetime = \DateTime::createFromFormat($format, local_date($format, $locked_time));
	if ($payment_term == 'month_statement') {
		return $datetime->modify('first day of next month')->modify('+9 day')->format($format);
	}elseif ($payment_term == '7_day') {
		return $datetime->add(new \DateInterval('P7D'))->format($format);
	} elseif ($payment_term == 'immediate') {
		return $datetime->format($format);
	} else {
		return $datetime->format($format) . "<br/>(付款期限不明)";
	}
}
function get_supplier_payment_term($supplier_id = 0)
{
	if ($supplier_id == 0) {
		return 'immediate';
	}

	$sql = "SELECT payment_term FROM" . $GLOBALS['ecs']->table('erp_supplier') . " WHERE supplier_id = '$supplier_id'";

	$result = $GLOBALS['db']->getOne($sql);
	return $result;
}
?>
