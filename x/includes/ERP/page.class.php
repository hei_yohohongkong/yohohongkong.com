<?php
 
class page 
{
var $page_name="page";
var $next_page='下一頁';
var $pre_page='上一頁';
var $first_page='首頁';
var $last_page='尾頁';
var $pre_bar='<<';
var $next_bar='>>';
var $format_left='[';
var $format_right=']';
var $page_style='';
var $current_page_style='';
var $total_data=0;
var $data_per_page=10;
var $total_page=1;
var $current_page=1;
var $url="";
var $mode=1;
var $page_bar_num=10;
function set_page_style($style='')
{
$this->page_style=$style;
}
function set_current_page_style($style='')
{
$this->current_page_style=$style;
}
function set_total_page()
{
$this->total_page=ceil($this->total_data/$this->data_per_page);
}
function set_current_page()
{
if(isset($_GET[$this->page_name]))
{
$this->current_page=intval($_GET[$this->page_name]);
}
else{
$this->current_page=1;
}
}
function set_total_data($total_data=1)
{
$this->total_data=$total_data;
}
function set_data_per_page($data_per_page=1)
{
$this->data_per_page=$data_per_page;
}
function set_url($url="")
{
$this->url=$url;
}
function set_mode($mode=1)
{
if(empty($mode))
{
$this->mode=1;
}
else{
$this->mode=intval($mode);
}
}
function set_page_bar_num($page_bar_num)
{
$this->page_bar_num=$page_bar_num;
}
function get_total_page()
{
return $this->total_page;
}
function get_current_page()
{
return $this->current_page;
}
function get_link($url,$text,$style='')
{
$style=(empty($style))?'':'class="'.$style.'"';
return '<a '.$style.' href="'.$url.'">'.$text.'</a>';
}
function get_url($page_no=1)
{
if(strpos($this->url,'='))
{
	//handle when page parameter is not right after ?
	if(array_key_exists($this->page_name, $_REQUEST)){
		return preg_replace("/".$this->page_name."\=([0-9]*)/i", $this->page_name.'='.$page_no, $_SERVER['REQUEST_URI']);
	}else{
		return $this->url."&".$this->page_name."=".$page_no;
	}
}
else{
return $this->url."?".$this->page_name."=".$page_no;
}
}
function page($array)
{
if(is_array($array))
{
if(!array_key_exists('total_data',$array)||!array_key_exists('data_per_page',$array)||!array_key_exists('url',$array)||!array_key_exists('mode',$array)||!array_key_exists('page_bar_num',$array) ||!array_key_exists('page_style',$array) ||!array_key_exists('current_page_style',$array))
{
$this->error(__FUNCTION__,'The param is not provided.');
}
$this->set_total_data(intval($array['total_data']));
$this->set_data_per_page(intval($array['data_per_page']));
$this->set_url($array['url']);
$this->set_mode($array['mode']);
$this->set_page_bar_num($array['page_bar_num']);
$this->page_name=((array_key_exists('page_name', $array) && $array['page_name']!='')?preg_replace('/[^A-Za-z0-9\-]/', '', $array['page_name']):$this->page_name);
$this->set_page_style($array['page_style']);
$this->set_current_page_style($array['current_page_style']);
}
$this->set_total_page();
$this->set_current_page();
}
function total_page()
{
return '共有 '.$this->total_page." 頁 ";
}
function next_page($style='')
{
if($this->current_page<$this->total_page)
{
return $this->get_link($this->get_url($this->current_page+1),$this->next_page,$style);
}
else
{
return '<span class="'.$style.'">'.$this->next_page.'</span>';
}
}
function pre_page($style='')
{
if($this->current_page>1)
{
return $this->get_link($this->get_url($this->current_page-1),$this->pre_page,$style);
}
else
{
return '<span class="'.$style.'">'.$this->pre_page.'</span>';
}
}
function first_page($style='')
{
if($this->current_page==1)
{
return '<span class="'.$style.'">'.$this->first_page.'</span>';
}
else
{
return $this->get_link($this->get_url(1),$this->first_page,$style);
}
}
function last_page($style='')
{
if($this->current_page==$this->total_page)
{
return '<span class="'.$style.'">'.$this->last_page.'</span>';
}
else{
return $this->get_link($this->get_url($this->total_page),$this->last_page,$style);
}
}
function nowbar($style='',$current_page_style='')
{
$plus=ceil($this->page_bar_num/2);
if($this->page_bar_num-$plus+$this->current_page>$this->total_page)
{
$plus=($this->page_bar_num-$this->total_page+$this->current_page);
}
$begin=$this->current_page-$plus+1;
$begin=($begin>=1)?$begin:1;
$return='';
for($i=$begin;$i<$begin+$this->page_bar_num;$i++)
{
if($i<=$this->total_page)
{
if($i!=$this->current_page)
{
$return.=$this->get_text($this->get_link($this->get_url($i),$i,$style));
}
else
{
$return.=$this->get_text('<span class="'.$current_page_style.'">'.$i.'</span>');
}
}
else
{
break;
}
$return.="\n";
}
unset($begin);
return $return;
}
function select()
{
$return='<select name="PB_Page_Select" >';
for($i=1;$i<=$this->total_page;$i++)
{
if($i==$this->current_page)
{
$return.='<option value="'.$i.'" selected>'.$i.'</option>';
}
else
{
$return.='<option value="'.$i.'">'.$i.'</option>';
}
}
unset($i);
$return.='</select>';
return $return;
}
function show()
{
switch ($this->mode)
{
case '1':
return '共'.$this->total_data.'項紀錄 '.$this->total_page().$this->first_page().$this->pre_page().$this->nowbar($this->page_style,$this->current_page_style).$this->next_page().$this->last_page();
break;
case '2':
return $this->first_page().$this->pre_page().' [第'.$this->current_page.'頁] '.$this->next_page().$this->last_page();
break;
case '3':
return $this->first_page().$this->pre_page().$this->next_page().$this->last_page();
break;
case '4':
return $this->pre_page().$this->nowbar().$this->next_page();
break;
}
}
function get_text($str)
{
return $this->format_left.$str.$this->format_right;
}
function error($function,$errormsg)
{
die('Error in file <b>'.__FILE__.'</b> ,Function <b>'.$function.'()</b> :'.$errormsg);
}
}
;echo ' 
';?>
