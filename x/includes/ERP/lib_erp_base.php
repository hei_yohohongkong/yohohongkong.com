<?php

function fix_url($url,$paras=array())
{
	$url_info=parse_url($url);
	$querys='';
	if(!empty($url_info['query']))
	{
		$querys=explode('&',$url_info['query']);
		foreach($querys as $key =>$query)
		{
			$query=explode('=',$query);
			$querys[$query[0]]=$query[1];
			unset($querys[$key]);
		}
		$querys=array_merge($querys,$paras);
		foreach($querys as $key =>$value)
		{
			if($value!=='')
			{
				$querys[$key]=$key.'='.$value;
			}
			else
			{
				unset($querys[$key]);
			}
		}
		$querys=implode('&',$querys);
	}
	else
	{
		if(!empty($paras))
		{
			foreach($paras as $key =>$value)
			{
				if($value!='')
				{
					$querys[$key]=$key.'='.$value;
				}
			}
			$querys=!empty($querys)?implode('&',$querys) : '';
		}
	}
	return empty($querys)?$url_info['path'] : $url_info['path'].'?'.$querys;
}

function get_current_time()
{
	return date("Y-m-d H:i:s");
}

function get_current_date()
{
	return date("Y-m-d");
}

?>