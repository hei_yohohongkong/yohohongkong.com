<?php

require_once(dirname(__FILE__) .'/lib_erp_supplier.php');
function auto_fix_barcode($goods_id)
{
$sql="select attr_id,attr_id_no_dynamic from ".$GLOBALS['ecs']->table('erp_goods_attr')." where goods_id='".$goods_id."' and barcode_id='0'";
$attr_ids=$GLOBALS['db']->getAll($sql);
if(!empty($attr_ids))
{
foreach($attr_ids as $attr)
{
$sql="select barcode_id from ".$GLOBALS['ecs']->table('erp_goods_attr')." where goods_id='".$goods_id."' and attr_id_no_dynamic='".$attr['attr_id_no_dynamic']."' and barcode_id<>0 limit 1";
$barcode_id=$GLOBALS['db']->getOne($sql);
if($barcode_id >0)
{
$sql="update ".$GLOBALS['ecs']->table('erp_goods_attr')." set barcode_id='".$barcode_id."' where attr_id='".$attr['attr_id']."'";
$GLOBALS['db']->query($sql);
}
}
}
}
function get_attr_id_list_by_barcode_id($barcode_id,$require_stock=false)
{
$sql="select goods_attr_id_list from ".$GLOBALS['ecs']->table('erp_goods_attr')." where barcode_id='".$barcode_id."' ";
if($require_stock==true)
{
$sql.=" and goods_qty > 0";
}
$sql.=" order by attr_id asc limit 1";
return $GLOBALS['db']->getOne($sql);
}
function get_goods_id_by_barcode_id($barcode_id)
{
$sql="select goods_id from ".$GLOBALS['ecs']->table('erp_goods_attr')." where barcode_id='".$barcode_id."' limit 1";
return $GLOBALS['db']->getOne($sql);
}
function get_goods_attr_by_barcode($barcode_id)
{
$sql="select goods_id,attr_id_no_dynamic from ".$GLOBALS['ecs']->table('erp_goods_attr')." where barcode_id='".$barcode_id."' limit 1";
$info=$GLOBALS['db']->getRow($sql);
$goods_attr=goods_attr($info['goods_id']);
if(!empty($goods_attr) &&$info['attr_id_no_dynamic']!='0')
{
$attr_id_no_dynamic=explode(',',$info['attr_id_no_dynamic']);
foreach($goods_attr as $key =>$attr)
{
foreach($attr as $k =>$a)
{
if(!in_array($a['goods_attr_id'],$attr_id_no_dynamic) &&$a['is_dynamic']==0)
{
unset($attr[$k]);
}
}
$goods_attr[$key]=$attr;
}
}
return $goods_attr;
}
function check_goods_barcode($goods_barcode='')
{
if(empty($goods_barcode))
{
return false;
}
else{
$sql="select id from ".$GLOBALS['ecs']->table('erp_goods_barcode')." where barcode='".$goods_barcode."'";
$barcode_id=$GLOBALS['db']->getOne($sql);
return !empty($barcode_id)?$barcode_id : false;
}
}
function get_erp_goods_barcode($paras=array(),$start=-1,$num=-1)
{
$sql="select distinct goods_id from ".$GLOBALS['ecs']->table('erp_goods_attr')." where 1 ";
if(isset($paras['goods_sn']) &&!empty($paras['goods_sn']))
{
$sql.=" and goods_id in (select goods_id from ".$GLOBALS['ecs']->table('goods')." where goods_sn like '%".$paras['goods_sn']."%')";
}
if(isset($paras['goods_name']) &&!empty($paras['goods_name']))
{
$sql.=" and goods_id in (select goods_id from ".$GLOBALS['ecs']->table('goods')." where goods_name like '%".$paras['goods_name']."%')";
}
if(isset($paras['goods_barcode']) &&!empty($paras['goods_barcode']))
{
$sql.=" and barcode_id in (select id from ".$GLOBALS['ecs']->table('erp_goods_barcode')." where barcode like '%".$paras['goods_barcode']."%')";
}
if($start!=-1 &&$num!=-1)
{
if($start<=0)
{
$start=0;
}
$sql.=" limit ".$start.",".$num;
}
$result=array();
$res=$GLOBALS['db']->query($sql);
while(list($goods_id)=mysql_fetch_row($res))
{
$sql="select goods_id,cat_id,goods_sn,goods_name,goods_thumb,goods_img,original_img from ".$GLOBALS['ecs']->table('goods')." where goods_id='".$goods_id."'";
$goods_info=$GLOBALS['db']->getRow($sql);
if(!empty($goods_info['goods_thumb']))
{
$goods_info['goods_thumb']='../'.$goods_info['goods_thumb'];
}
if(!empty($goods_info['goods_img']))
{
$goods_info['goods_img']='../'.$goods_info['goods_img'];
}
if(!empty($goods_info['original_img']))
{
$goods_info['original_img']='../'.$goods_info['original_img'];
}
$sql="select distinct attr_id_no_dynamic,barcode_id from ".$GLOBALS['ecs']->table('erp_goods_attr')." where goods_id='".$goods_id."' ";
if(isset($paras['goods_barcode']) &&!empty($paras['goods_barcode']))
{
$sql.=" and barcode_id in (select id from ".$GLOBALS['ecs']->table('erp_goods_barcode')." where barcode like '%".$paras['goods_barcode']."%')";
}
$sql.=" order by attr_id_no_dynamic";
$attr=array();
$res2=$GLOBALS['db']->query($sql);
while($row=mysql_fetch_assoc($res2))
{
if($row['barcode_id'] >0)
{
$sql="select barcode from ".$GLOBALS['ecs']->table('erp_goods_barcode')." where id='".$row['barcode_id']."'";
$row['barcode']=$GLOBALS['db']->getOne($sql);
}
else{
$row['barcode']='';
}
if($row['attr_id_no_dynamic']!='0')
{
$row['attr']=get_attr_info_by_attr_ids($row['attr_id_no_dynamic']);
$row['attr_id_no_dynamic']=str_replace(',','_',$row['attr_id_no_dynamic']);
$attr[]=$row;
}
else{
$attr='';
$goods_info['barcode_id']=$row['barcode_id'];
$goods_info['barcode']=$row['barcode'];
$goods_info['no_attr']=1;
}
}
$result[$goods_id]=array('goods_info'=>$goods_info,'attr'=>$attr);
}
return $result;
}
function get_erp_goods_barcode_count($paras=array())
{
$sql="select count(distinct goods_id) from ".$GLOBALS['ecs']->table('erp_goods_attr')." where 1 ";
if(isset($paras['goods_sn']) &&!empty($paras['goods_sn']))
{
$sql.=" and goods_id in (select goods_id from ".$GLOBALS['ecs']->table('goods')." where goods_sn like '%".$paras['goods_sn']."%')";
}
if(isset($paras['goods_name']) &&!empty($paras['goods_name']))
{
$sql.=" and goods_id in (select goods_id from ".$GLOBALS['ecs']->table('goods')." where goods_name like '%".$paras['goods_name']."%')";
}
if(isset($paras['goods_barcode']) &&!empty($paras['goods_barcode']))
{
$sql.=" and barcode_id in (select id from ".$GLOBALS['ecs']->table('erp_goods_barcode')." where barcode like '%".$paras['goods_barcode']."%')";
}
return $GLOBALS['db']->getOne($sql);
}
function check_goods_sn($goods_sn='')
{
if(empty($goods_sn))
{
return false;
}
else{
$sql="select goods_id from ".$GLOBALS['ecs']->table('goods')." where goods_sn='".$goods_sn."' and is_delete='0'";
$goods_id=$GLOBALS['db']->getOne($sql);
return !empty($goods_id)?$goods_id : false;
}
}
function check_goods_name($goods_name='')
{
if(empty($goods_name))
{
return false;
}
else{
$sql="select goods_id from ".$GLOBALS['ecs']->table('goods')." where goods_name='".$goods_name."' and is_delete='0'";
$goods_id=$GLOBALS['db']->getOne($sql);
return !empty($goods_id)?$goods_id : false;
}
}
function is_admin_goods($admin_id,$goods_id='',$goods_sn='')
{
$supplier_id_array=array();
$supplier_info=get_admin_supplier($admin_id);
if(!empty($supplier_info))
{
foreach($supplier_info as $key =>$item)
{
array_push($supplier_id_array,$item['supplier_id']);
}
}
else{
return false;
}
if(empty($goods_id) &&!empty($goods_sn))
{
$sql="select goods_id from ".$GLOBALS['ecs']->table('goods')." where goods_sn='".$goods_sn."'";
$goods_id=$GLOBALS['db']->getOne($sql);
if(empty($goods_id))
{
return false;
}
}
$sql="select * from ".$GLOBALS['ecs']->table('erp_goods_supplier')." where goods_id='".$goods_id."' and supplier_id in (".implode(',',$supplier_id_array).")";
$result=$GLOBALS['db']->getRow($sql);
if($result==false)
{
return false;
}
else{
return true;
}
}
function is_supplier_goods($supplier_id,$goods_id='',$goods_sn='')
{
if(empty($supplier_id))
{
return false;
}
if(empty($goods_id) &&!empty($goods_sn))
{
$sql="select goods_id from ".$GLOBALS['ecs']->table('goods')." where goods_sn='".$goods_sn."'";
$goods_id=$GLOBALS['db']->getOne($sql);
if(empty($goods_id))
{
return false;
}
}
$sql="select * from ".$GLOBALS['ecs']->table('erp_goods_supplier')." where goods_id='".$goods_id."' and supplier_id='".$supplier_id."'";
$result=$GLOBALS['db']->getRow($sql);
if($result==false)
{
return false;
}
else{
return true;
}
}
function is_goods_exist($goods_id='',$goods_sn='')
{
if(!empty($goods_id) &&empty($goods_sn))
{
$sql="select * from ".$GLOBALS['ecs']->table('goods')." where goods_id='".$goods_id."'";
}
elseif(empty($goods_id) &&!empty($goods_sn))
{
$sql="select * from ".$GLOBALS['ecs']->table('goods')." where goods_sn='".$goods_sn."'";
}
else{
return false;
}
$result=$GLOBALS['db']->getRow($sql);
return !empty($result)?$result : false;
}
function get_goods_qty($goods_id)
{
$sql="select sum(goods_qty) as goods_number from ".$GLOBALS['ecs']->table('erp_goods_attr')." where goods_id='".$goods_id."'";
$goods_number=$GLOBALS['db']->getOne($sql);
return $goods_number>0 ?$goods_number : 0;
}
function get_goods_stock_by_warehouse($warehouse_id,$goods_id,$attr_id=0)
{
if(!empty($attr_id))
{
$sql="select goods_qty from ".$GLOBALS['ecs']->table('erp_goods_attr_stock')." where goods_id='".$goods_id."' and attr_id='".$attr_id."' and warehouse_id='".$warehouse_id."'";
return $GLOBALS['db']->getOne($sql);
}
else{
$sql="select sum(goods_qty) from ".$GLOBALS['ecs']->table('erp_goods_attr_stock')." where goods_id='".$goods_id."' and warehouse_id='".$warehouse_id."'";
return $GLOBALS['db']->getOne($sql);
}
}
function goods_stock($warehouse_id,$goods_id,$goods_attr_id_list='')
{
$attr_id=get_attr_id($goods_id,$goods_attr_id_list);
$sql="select goods_qty from ".$GLOBALS['ecs']->table('erp_goods_attr_stock')." where attr_id='".$attr_id."' and warehouse_id='".$warehouse_id."' limit 1";
return $GLOBALS['db']->getOne($sql);
}
?>
