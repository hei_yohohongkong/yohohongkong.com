<?php

if (!defined('IN_ECS'))
{
die('Hacking attempt');
}
function get_receivable_order_count($paras=array())
{
$sql="select count(*) from ".
$GLOBALS['ecs']->table('order_info').
"where 1 and shipping_status in (1,2,4) ".
"and gathered_amount<(delivery_goods_amount+tax+shipping_fee+insure_fee+pay_fee+pack_fee+card_fee-surplus-integral_money-bonus-discount)";
if(isset($paras['order_id']) &&intval($paras['order_id']>0))
{
$sql.=" and order_id='".$paras['order_id']."' ";
}
if(isset($paras['order_sn']) &&!empty($paras['order_sn']))
{
$sql.=" and order_sn like '%".$paras['order_sn']."%'";
}
if(isset($paras['agency_id']) &&intval($paras['agency_id']) >0)
{
$sql.=" and agency_id='".$paras['agency_id']."'";
}
return $GLOBALS['db']->getOne($sql);
}
function get_receivable_order_list($paras=array(),$start=-1,$num=-1)
{
$sql="select order_id,order_sn,user_id,consignee,pay_name,delivery_goods_amount,".
"order_amount,money_paid,gathered_amount,add_time,confirm_time,pay_time,shipping_time,agency_id,".
"(delivery_goods_amount+tax+shipping_fee+insure_fee+pay_fee+pack_fee+card_fee-surplus-integral_money-bonus-discount) as receivable_amount,money_paid+order_amount as total_amount from ".
$GLOBALS['ecs']->table('order_info').
"where 1 and shipping_status in (1,2,4) ".
"and gathered_amount<(delivery_goods_amount+tax+shipping_fee+insure_fee+pay_fee+pack_fee+card_fee-surplus-integral_money-bonus-discount)";
if(isset($paras['order_id']) &&intval($paras['order_id']>0))
{
$sql.=" and order_id='".$paras['order_id']."' ";
}
if(isset($paras['order_sn']) &&!empty($paras['order_sn']))
{
$sql.=" and order_sn like '%".$paras['order_sn']."%'";
}
if(isset($paras['agency_id']) &&intval($paras['agency_id']) >0)
{
$sql.=" and agency_id='".$paras['agency_id']."'";
}
$sql.=" order by order_id desc ";
if($start!=-1 &&$num!=-1)
{
if($start<=0)
{
$start=0;
}
$sql.=" limit ".$start.",".$num;
}
$order_info=$GLOBALS['db']->getAll($sql);
foreach($order_info as $key =>$item)
{
$order_info[$key]['add_date']=local_date("Y-m-d",$item['add_time']);
$order_info[$key]['confirm_date']=local_date("Y-m-d",$item['confirm_time']);
$order_info[$key]['pay_date']=local_date("Y-m-d",$item['pay_time']);
$order_info[$key]['shipping_date']=local_date("Y-m-d",$item['shipping_time']);
$order_info[$key]['receivable_amount']=$order_info[$key]['receivable_amount'];
$order_info[$key]['receivable']=floatval($order_info[$key]['receivable_amount']-$order_info[$key]['gathered_amount']);
$sql="select user_name from ".$GLOBALS['ecs']->table('users')." where user_id='".$item['user_id']."'";
$order_info[$key]['username']=$GLOBALS['db']->getOne($sql);
$sql="select * from ".$GLOBALS['ecs']->table('order_goods')." where order_id='".$item['order_id']."'";
$order_goods=$GLOBALS['db']->getAll($sql);
$goods_amount_subtotal=0;
foreach($order_goods as $key2 =>$item2)
{
$order_goods[$key2]['goods_amount']=$item2['goods_price']*$item2['delivery_qty'];
$goods_amount_subtotal+=$order_goods[$key2]['goods_amount'];
$sql="select goods_id,cat_id,goods_sn,goods_name,brand_id,goods_thumb,goods_img,original_img from ".$GLOBALS['ecs']->table('goods')." where goods_id='".$item2['goods_id']."'";
$goods_info=$GLOBALS['db']->getRow($sql);
if(!empty($goods_info['goods_thumb']))
{
$goods_info['goods_thumb']='../'.$goods_info['goods_thumb'];
}
if(!empty($goods_info['goods_img']))
{
$goods_info['goods_img']='../'.$goods_info['goods_img'];
}
if(!empty($goods_info['original_img']))
{
$goods_info['original_img']='../'.$goods_info['original_img'];
}
$order_goods[$key2]['goods_info']=$goods_info;
}
$order_info[$key]['order_goods']=$order_goods;
$order_info[$key]['goods_amount']=$goods_amount_subtotal;
$sql="select agency_name from ".$GLOBALS['ecs']->table('agency')." where agency_id='".$item['agency_id']."'";
$order_info[$key]['agency_name']=$GLOBALS['db']->getOne($sql);
}
return $order_info;
}
function approve_payment_pass($payment_id='',$approve_remark='')
{
if(empty($payment_id))
{
return false;
}
else{
$payment_info=get_payment_info($payment_id);
$payment_info=$payment_info[0];
$account_id=$payment_info['account_id'];
$payment_style_id=$payment_info['payment_style_id'];
$pay_amount=$payment_info['pay_amount'];
$bill_id=$payment_info['bill_id'];
$payment_remark=$payment_info['payment_remark'];
$account_info=get_bank_account_info($account_id);
$sql="update ".$GLOBALS['ecs']->table('erp_bank_account')." set account_balance=account_balance-".$pay_amount." where account_id='".$account_id."'";
$GLOBALS['db']->query($sql);
$account_info=get_bank_account_info($account_id);
$pay_amount=0-$pay_amount;
$sql="insert into ".$GLOBALS['ecs']->table('erp_bank_account_record')." set account_id='".$account_id."',income_expenses=".$pay_amount.",balance=".$account_info['account_balance'].",record_time='".gmtime()."',bill_id='".$bill_id."',remark='".$payment_remark."',payment_gathering_id='".$payment_id."'";
$GLOBALS['db']->query($sql);
if($payment_info['payment_style_id']==1)
{
$payment_details_info=get_payment_details($payment_id);
foreach($payment_details_info as $payment_details)
{
$sql="update ".$GLOBALS['ecs']->table('erp_warehousing_item')." set paid_amount=paid_amount+".$payment_details['pay_amount']." where warehousing_item_id='".$payment_details['warehousing_item_id']."'";
$GLOBALS['db']->query($sql);
}
}
$sql="update ".$GLOBALS['ecs']->table('erp_payment')." set ";
$sql.="payment_status ='3', approve_remark='".$approve_remark."' ";
$sql.="where payment_id='".$payment_id."'";
$GLOBALS['db']->query($sql);
return true;
}
}
function approve_gathering_pass($gathering_id='',$approve_remark='')
{
if(empty($gathering_id))
{
return false;
}
else{
$gathering_info=get_gathering_info($gathering_id);
$gathering_info=$gathering_info[0];
$account_id=$gathering_info['account_id'];
$gathering_style_id=$gathering_info['gathering_style_id'];
$gather_amount=$gathering_info['gather_amount'];
$bill_id=$gathering_info['bill_id'];
$gathering_remark=$gathering_info['gathering_remark'];
$account_info=get_bank_account_info($account_id);
$sql="update ".$GLOBALS['ecs']->table('erp_bank_account')." set account_balance=account_balance+".$gather_amount." where account_id='".$account_id."'";
$GLOBALS['db']->query($sql);
$account_info=get_bank_account_info($account_id);
$sql="insert into ".$GLOBALS['ecs']->table('erp_bank_account_record')." set account_id='".$account_id."',income_expenses=".$gather_amount.",balance=".$account_info['account_balance'].",record_time ='".gmtime()."',bill_id='".$bill_id."',remark='".$gathering_remark."',payment_gathering_id='".$gathering_id."'";
$GLOBALS['db']->query($sql);
if($gathering_info['gathering_style_id']==1)
{
$sql="update ".$GLOBALS['ecs']->table('order_info')." set gathered_amount=gathered_amount+".$gather_amount." where order_id='".$bill_id."'";
$GLOBALS['db']->query($sql);
}
$sql="update ".$GLOBALS['ecs']->table('erp_gathering')." set ";
$sql.="gathering_status ='3', approve_remark='".$approve_remark."' ";
$sql.="where gathering_id='".$gathering_id."'";
$GLOBALS['db']->query($sql);
return true;
}
}
function approve_payment_reject($payment_id='',$approve_remark='')
{
if(empty($payment_id))
{
return false;
}
else{
$sql="update ".$GLOBALS['ecs']->table('erp_payment')." set ";
$sql.="payment_status ='4', approve_remark='".$approve_remark."' ";
$sql.="where payment_id='".$payment_id."'";
if($GLOBALS['db']->query($sql))
{
return true;
}
else{
return false;
}
}
}
function approve_gathering_reject($gathering_id='',$approve_remark='')
{
if(empty($gathering_id))
{
return false;
}
else{
$sql="update ".$GLOBALS['ecs']->table('erp_gathering')." set ";
$sql.="gathering_status ='4', approve_remark='".$approve_remark."' ";
$sql.="where gathering_id='".$gathering_id."'";
if($GLOBALS['db']->query($sql))
{
return true;
}
else{
return false;
}
}
}
function update_payment($payment_id='',$order_id='',$pay_amount)
{
@delete_payment_details($payment_id);
$order_info=get_payable_order_list(array('order_id'=>$order_id));
$pay_to=$order_info[$order_id]['order_info']['supplier_id'];
$bill_id=$order_id;
$bill_amount=$order_info[$order_id]['order_info']['order_amount'];
$bill_total_payable=$order_info[$order_id]['total_payable_amount'];
$bill_paid_payable=$order_info[$order_id]['total_paid_amount'];
$bill_payable_balance=$order_info[$order_id]['payable'];
if($pay_amount>$bill_payable_balance)
{
$pay_amount=$bill_payable_balance;
}
$sql="update ".$GLOBALS['ecs']->table('erp_payment')." set pay_to='".$pay_to."',bill_id='".$bill_id."',bill_amount='".$bill_amount."',";
$sql.="bill_total_payable='".$bill_total_payable."',bill_paid_payable='".$bill_paid_payable."',bill_payable_balance='".$bill_payable_balance."',pay_amount='".$pay_amount."'";
$sql.=" where payment_id='".$payment_id."'";
$GLOBALS['db']->query($sql);
foreach($order_info[$order_id]['warehousing_info'] as $warehousing_info)
{
if($pay_amount>0)
{
foreach($warehousing_info['wareshouing_item_info'] as $warehousing_item_info)
{
if($warehousing_item_info['payable']!=0 &&$pay_amount>0 &&$warehousing_item_info['payable']<=$pay_amount)
{
$pay_details_item['warehousing_item_id']=$warehousing_item_info['warehousing_item_id'];
$pay_details_item['pay_amount']=$warehousing_item_info['payable'];
$pay_details_array[]=$pay_details_item;
$pay_amount=$pay_amount-$warehousing_item_info['payable'];
}
elseif($warehousing_item_info['payable']!=0 &&$pay_amount>0 &&$warehousing_item_info['payable'] >$pay_amount)
{
$pay_details_item['warehousing_item_id']=$warehousing_item_info['warehousing_item_id'];
$pay_details_item['pay_amount']=$pay_amount;
$pay_details_array[]=$pay_details_item;
$pay_amount=0;
}
}
}
}
foreach($pay_details_array as $pay_details_item)
{
$sql="insert into ".$GLOBALS['ecs']->table('erp_payment_details')." set payment_id='".$payment_id."',warehousing_item_id='".$pay_details_item['warehousing_item_id']."',pay_amount='".$pay_details_item['pay_amount']."'";
$GLOBALS['db']->query($sql);
}
}
function update_gathering($gathering_id='',$order_id='',$gather_amount)
{
$order_info=get_receivable_order_list(array('order_id'=>$order_id));
$pay_from=$order_info[0]['user_id'];
$bill_id=$order_id;
$bill_time=$order_info[0]['add_time'];
$bill_amount=$order_info[0]['money_paid']+$order_info[0]['order_amount'];
$bill_total_receivable=$order_info[0]['receivable_amount'];
$bill_gathered_receivable=$order_info[0]['gathered_amount'];
$bill_receivable_balance=$order_info[0]['receivable'];
if($gather_amount>$bill_receivable_balance)
{
$gather_amount=$bill_receivable_balance;
}
$sql="update ".$GLOBALS['ecs']->table('erp_gathering')."set ";
$sql.="pay_from='".$pay_from."',bill_id='".$bill_id."',";
$sql.="bill_time='".$bill_time."',bill_amount='".$bill_amount."',bill_total_receivable='".$bill_total_receivable."',bill_gathered_receivable='".$bill_gathered_receivable."',";
$sql.="bill_receivable_balance='".$bill_receivable_balance."',gather_amount='".$gather_amount."' where gathering_id='".$gathering_id."'";
$GLOBALS['db']->query($sql);
}
function create_payment($order_id='',$pay_amount=0)
{
if(empty($order_id))
{
return false;
}
else{
$agency_id=get_admin_agency_id($_SESSION['admin_id']);
$order_info=get_payable_order_list(array('order_id'=>$order_id,'agency_id'=>$agency_id));
if(empty($order_info))
{
return -1;
}
$payment_sn=gen_payment_sn();
$bank_account=bank_account_list(array('is_valid'=>1,'agency_id'=>$agency_id));
if(empty($bank_account))
{
return -2;
}
else
{
$account_id=$bank_account[0]['account_id'];
$pay_to=$order_info[$order_id]['order_info']['supplier_id'];
$payment_style_id=1;
$bill_id=$order_id;
$bill_time=$order_info[$order_id]['order_info']['create_time'];
$bill_amount=$order_info[$order_id]['order_info']['order_amount'];
$bill_total_payable=$order_info[$order_id]['total_payable_amount'];
$bill_paid_payable=$order_info[$order_id]['total_paid_amount'];
$bill_payable_balance=$order_info[$order_id]['payable'];
$create_user_id=erp_get_admin_id();
$create_time=gmtime();
$payment_status=1;
$sql="insert into ".$GLOBALS['ecs']->table('erp_payment')."set payment_sn='".$payment_sn."',";
$sql.="account_id='".$account_id."',pay_to='".$pay_to."',payment_style_id='".$payment_style_id."',bill_id='".$bill_id."',";
$sql.="bill_time='".$bill_time."',bill_amount='".$bill_amount."',bill_total_payable='".$bill_total_payable."',bill_paid_payable='".$bill_paid_payable."',";
$sql.="bill_payable_balance='".$bill_payable_balance."',pay_amount='".$pay_amount."',create_user_id='".$create_user_id."',create_time='".$create_time."',";
$sql.="payment_status='".$payment_status."',agency_id='".$agency_id."'";
$GLOBALS['db']->query($sql);
$sql="select payment_id from ".$GLOBALS['ecs']->table('erp_payment')." where payment_sn='".$payment_sn."'";
$payment_id=$GLOBALS['db']->getOne($sql);
update_payment($payment_id,$order_id,$pay_amount);
return $payment_id;
}
}
}
function create_gathering($order_id='',$gather_amount=0)
{
if(empty($order_id))
{
return false;
}
else{
$agency_id=get_admin_agency_id($_SESSION['admin_id']);
$order_info=get_receivable_order_list(array('order_id'=>$order_id,'agency_id'=>$agency_id));
if(empty($order_info))
{
return -1;
}
$gathering_sn=gen_gathering_sn();
$bank_account=bank_account_list(array('is_valid'=>1,'agency_id'=>$agency_id));
if(empty($bank_account))
{
return -2;
}
else
{
$account_id=$bank_account[0]['account_id'];
$pay_from=$order_info[0]['user_id'];
$gathering_style_id=1;
$bill_id=$order_id;
$bill_time=$order_info[0]['add_time'];
$bill_amount=$order_info[0]['money_paid'];
$bill_total_receivable=$order_info[0]['receivable_amount'];
$bill_gathered_receivable=$order_info[0]['gathered_amount'];
$bill_receivable_balance=$order_info[0]['receivable'];
$create_user_id=erp_get_admin_id();
$create_time=gmtime();
$gathering_status=1;
$sql="insert into ".$GLOBALS['ecs']->table('erp_gathering')."set gathering_sn='".$gathering_sn."', ";
$sql.="account_id='".$account_id."',pay_from='".$pay_from."',gathering_style_id='".$gathering_style_id."',bill_id='".$bill_id."',";
$sql.="bill_time='".$bill_time."',bill_amount='".$bill_amount."',bill_total_receivable='".$bill_total_receivable."',bill_gathered_receivable='".$bill_gathered_receivable."',";
$sql.="bill_receivable_balance='".$bill_receivable_balance."',gather_amount='".$gather_amount."',create_user_id='".$create_user_id."',create_time='".$create_time."',";
$sql.="gathering_status='".$gathering_status."',agency_id='".$agency_id."'";
$GLOBALS['db']->query($sql);
$sql="select gathering_id from ".$GLOBALS['ecs']->table('erp_gathering')." where gathering_sn='".$gathering_sn."'";
$gathering_id=$GLOBALS['db']->getOne($sql);
return $gathering_id;
}
}
}
function get_payment_style_info($payment_style_id='')
{
if(!empty($payment_style_id))
{
$sql="select * from ".$GLOBALS['ecs']->table('erp_payment_style')." where payment_style_id='".$payment_style_id."'";
}
else{
$sql="select * from ".$GLOBALS['ecs']->table('erp_payment_style')." where 1 order by payment_style_id";
}
return $GLOBALS['db']->getAll($sql);
}
function get_gathering_style_info($gathering_style_id='')
{
if(!empty($gathering_style_id))
{
$sql="select * from ".$GLOBALS['ecs']->table('erp_gathering_style')." where gathering_style_id='".$gathering_style_id."'";
}
else{
$sql="select * from ".$GLOBALS['ecs']->table('erp_gathering_style')." where 1 order by gathering_style_id";
}
return $GLOBALS['db']->getAll($sql);
}
function get_payment_status()
{
$payment_status[0]['status_no']=1;
$payment_status[0]['status_style']="付款单录入中";
$payment_status[2]['status_no']=2;
$payment_status[2]['status_style']="主管审核中";
$payment_status[3]['status_no']=3;
$payment_status[3]['status_style']="审核已通过";
$payment_status[4]['status_no']=4;
$payment_status[4]['status_style']="审核未通过";
return $payment_status;
}
function get_gathering_status()
{
$gathering_status[0]['status_no']=1;
$gathering_status[0]['status_style']="收款单录入中";
$gathering_status[2]['status_no']=2;
$gathering_status[2]['status_style']="主管审核中";
$gathering_status[3]['status_no']=3;
$gathering_status[3]['status_style']="审核已通过";
$gathering_status[4]['status_no']=4;
$gathering_status[4]['status_style']="审核未通过";
return $gathering_status;
}
function get_payment_details($payment_id='')
{
if(empty($payment_id))
{
return false;
}
else{
$sql="select * from ".$GLOBALS['ecs']->table('erp_payment_details')." where payment_id='".$payment_id."'";
return $GLOBALS['db']->getAll($sql);
}
}
function delete_payment($payment_id='')
{
if(empty($payment_id))
{
return false;
}
$sql="delete from ".$GLOBALS['ecs']->table('erp_payment')."  where payment_id='".$payment_id."'";
$GLOBALS['db']->query($sql);
return true;
}
function delete_gathering($gathering_id='')
{
if(empty($gathering_id))
{
return false;
}
$sql="delete from ".$GLOBALS['ecs']->table('erp_gathering')."  where gathering_id='".$gathering_id."'";
$GLOBALS['db']->query($sql);
return true;
}
function delete_payment_details($payment_id='')
{
if(empty($payment_id))
{
return false;
}
$sql="delete from ".$GLOBALS['ecs']->table('erp_payment_details')." where payment_id='".$payment_id."'";
$GLOBALS['db']->query($sql);
return true;
}
function is_payment_exists($payment_id='',$payment_sn='')
{
if(empty($payment_id) &&empty($payment_sn))
{
return false;
}
elseif(!empty($payment_id) &&empty($payment_sn))
{
$sql="select * from ".$GLOBALS['ecs']->table('erp_payment')." where payment_id='".$payment_id."'";
$result=$GLOBALS['db']->getRow($sql);
return !empty($result) ?$result : false;
}
elseif(empty($payment_id) &&!empty($payment_sn))
{
$sql="select * from ".$GLOBALS['ecs']->table('erp_payment')." where payment_sn ='".$payment_sn."'";
$result=$GLOBALS['db']->getRow($sql);
return !empty($result) ?$result : false;
}
}
function is_gathering_exists($gathering_id='',$gathering_sn='')
{
if(empty($gathering_id) &&empty($gathering_sn))
{
return false;
}
elseif(!empty($gathering_id) &&empty($gathering_sn))
{
$sql="select * from ".$GLOBALS['ecs']->table('erp_gathering')." where gathering_id='".$gathering_id."'";
$result=$GLOBALS['db']->getRow($sql);
return !empty($result) ?$result : false;
}
elseif(empty($gathering_id) &&!empty($gathering_sn))
{
$sql="select * from ".$GLOBALS['ecs']->table('erp_gathering')." where gathering_sn ='".$gathering_sn."'";
$result=$GLOBALS['db']->getRow($sql);
return !empty($result) ?$result : false;
}
}
function payment_act_record($payment_id='',$act='')
{
if(empty($payment_id))
{
return false;
}
if(!is_payment_exists($payment_id))
{
return false;
}
if(empty($act))
{
return false;
}
$user_id=erp_get_admin_id();
$current_time=gmtime();
if($act=='create')
{
$sql="update ".$GLOBALS['ecs']->table('erp_payment')." set create_user_id='".$user_id."'";
$sql.=", create_time='".$current_time."' where payment_id='".$payment_id."'";
}
elseif($act=='modify')
{
$sql="update ".$GLOBALS['ecs']->table('erp_payment')." set modify_user_id='".$user_id."'";
$sql.=", modify_time='".$current_time."' where payment_id='".$payment_id."'";
}
elseif($act=='delete')
{
$sql="update ".$GLOBALS['ecs']->table('erp_payment')." set delete_user_id  ='".$user_id."'";
$sql.=", delete_time ='".$current_time."' where payment_id='".$payment_id."'";
}
elseif($act=='post')
{
$sql="update ".$GLOBALS['ecs']->table('erp_payment')." set post_user_id='".$user_id."'";
$sql.=", post_time='".$current_time."' where payment_id='".$payment_id."'";
}
elseif($act=='approve')
{
$sql="update ".$GLOBALS['ecs']->table('erp_payment')." set approve_user_id ='".$user_id."'";
$sql.=", approve_time='".$current_time."' where payment_id='".$payment_id."'";
}
$GLOBALS['db']->query($sql);
return true;
}
function gathering_act_record($gathering_id='',$act='')
{
if(empty($gathering_id))
{
return false;
}
if(!is_gathering_exists($gathering_id))
{
return false;
}
if(empty($act))
{
return false;
}
$user_id=erp_get_admin_id();
$current_time=gmtime();
if($act=='create')
{
$sql="update ".$GLOBALS['ecs']->table('erp_gathering')." set create_user_id='".$user_id."'";
$sql.=", create_time='".$current_time."' where gathering_id='".$gathering_id."'";
}
elseif($act=='modify')
{
$sql="update ".$GLOBALS['ecs']->table('erp_gathering')." set modify_user_id='".$user_id."'";
$sql.=", modify_time='".$current_time."' where gathering_id='".$gathering_id."'";
}
elseif($act=='delete')
{
$sql="update ".$GLOBALS['ecs']->table('erp_gathering')." set delete_user_id  ='".$user_id."'";
$sql.=", delete_time ='".$current_time."' where gathering_id='".$gathering_id."'";
}
elseif($act=='post')
{
$sql="update ".$GLOBALS['ecs']->table('erp_gathering')." set post_user_id='".$user_id."'";
$sql.=", post_time='".$current_time."' where gathering_id='".$gathering_id."'";
}
elseif($act=='approve')
{
$sql="update ".$GLOBALS['ecs']->table('erp_gathering')." set approve_user_id ='".$user_id."'";
$sql.=", approve_time='".$current_time."' where gathering_id='".$gathering_id."'";
}
$GLOBALS['db']->query($sql);
return true;
}
function get_payment_count($paras=array())
{
$sql="select count(*) from ".$GLOBALS['ecs']->table('erp_payment')." where 1";
if(isset($paras['status']) &&intval($paras['status']>0))
{
$sql.=" and payment_status='".$paras['status']."' ";
}
if(isset($paras['start_time']) &&intval($paras['start_time']>0))
{
$sql.=" and create_time>='".$paras['start_time']."' ";
}
if(isset($paras['end_time']) &&intval($paras['end_time']>0))
{
$sql.=" and create_time<='".$paras['end_time']."' ";
}
if(isset($paras['payment_sn']) &&!empty($paras['payment_sn']))
{
$sql.=" and payment_sn like '%".$paras['payment_sn']."%'";
}
if(isset($paras['pay_to']) &&!empty($paras['pay_to']))
{
$sql.=" and pay_to in (select supplier_id from ".$GLOBALS['ecs']->table('erp_supplier')." where name like '%".$paras['pay_to']."%')";
}
if(isset($paras['bill_sn']) &&!empty($paras['bill_sn']))
{
$sql.=" and bill_id in (select order_id from ".$GLOBALS['ecs']->table('erp_order')." where order_sn like '%".$paras['bill_sn']."%')";
}
if(isset($paras['agency_id']) &&intval($paras['agency_id']) >0)
{
$sql.=" and agency_id='".$paras['agency_id']."'";
}
return $GLOBALS['db']->getOne($sql);
}
function get_payment_list($paras=array(),$start=-1,$num=-1)
{
$sql="select * from ".$GLOBALS['ecs']->table('erp_payment')." where 1";
if(isset($paras['payment_id']) &&intval($paras['payment_id']>0))
{
$sql.=" and payment_id='".$paras['payment_id']."' ";
}
if(isset($paras['status']) &&intval($paras['status']>0))
{
$sql.=" and payment_status='".$paras['status']."' ";
}
if(isset($paras['start_time']) &&intval($paras['start_time']>0))
{
$sql.=" and create_time>='".$paras['start_time']."' ";
}
if(isset($paras['end_time']) &&intval($paras['end_time']>0))
{
$sql.=" and create_time<='".$paras['end_time']."' ";
}
if(isset($paras['payment_sn']) &&!empty($paras['payment_sn']))
{
$sql.=" and payment_sn like '%".$paras['payment_sn']."%'";
}
if(isset($paras['pay_to']) &&!empty($paras['pay_to']))
{
$sql.=" and pay_to in (select supplier_id from ".$GLOBALS['ecs']->table('erp_supplier')." where name like '%".$paras['pay_to']."%')";
}
if(isset($paras['bill_sn']) &&!empty($paras['bill_sn']))
{
$sql.=" and bill_id in (select order_id from ".$GLOBALS['ecs']->table('erp_order')." where order_sn like '%".$paras['bill_sn']."%')";
}
if(isset($paras['agency_id']) &&intval($paras['agency_id']) >0)
{
$sql.=" and agency_id='".$paras['agency_id']."'";
}
$sql.=" order by payment_id  desc ";
if($start!=-1 &&$num!=-1)
{
if($start<=0)
{
$start=0;
}
$sql.=" limit ".$start.",".$num;
}
$res=$GLOBALS['db']->query($sql);
while($row=mysql_fetch_assoc($res))
{
$row['create_date']=local_date('Y-m-d',$row['create_time']);
$row['bill_date']=local_date('Y-m-d',$row['bill_time']);
$row['post_date']=local_date('Y-m-d',$row['post_time']);
$row['approve_date']=local_date('Y-m-d',$row['approve_time']);
$payment_id=$row['payment_id'];
$row['available_act']=get_payment_available_act($payment_id,'');
$account_id=$row['account_id'];
$row['account_info']=get_bank_account_info($account_id);
if($row['payment_style_id']==1)
{
$order_id=$row['bill_id'];
$row['order_info']=get_purchase_order_info($order_id);
$supplier_id=$row['pay_to'];
$row['supplier_info']=supplier_info($supplier_id);
$row['payment_details']=get_payment_details($payment_id);
}
$sql="select agency_name from ".$GLOBALS['ecs']->table('agency')." where agency_id='".$row['agency_id']."'";
$row['agency_name']=$GLOBALS['db']->getOne($sql);
$payment_array[]=$row;
}
return $payment_array;
}
function get_payment_info($payment_id='',$payment_sn='',$status='',$start_time='',$end_time='',$pay_to='',$start=-1,$num=-1)
{
if(empty($payment_id) &&empty($payment_sn))
{
$sql="select * from ".$GLOBALS['ecs']->table('erp_payment')." where 1";
if(!empty($status))
{
$sql.=" and payment_status='".$status."'";
}
if(!empty($start_time) &&empty($end_time))
{
$sql.=" and create_time >='".$start_time."'";
}
elseif(empty($start_time) &&!empty($end_time))
{
$sql.=" and create_time  <='".$end_time."'";
}
elseif(!empty($start_time) &&!empty($end_time))
{
$sql.=" and create_time  >='".$start_time."' and create_time  <='".$end_time."'";
}
if(!empty($pay_to))
{
$sql.=" and pay_to='".$pay_to."'";
}
$sql.=" order by payment_sn desc ";
if($start!=-1 &&$num!=-1)
{
if($start<=0)
{
$start=0;
}
$sql.=" limit ".$start.",".$num;
}
}
elseif(!empty($payment_id) &&empty($payment_sn))
{
$sql="select * from ".$GLOBALS['ecs']->table('erp_payment')." where payment_id=".$payment_id."";
}
elseif(empty($payment_id) &&!empty($payment_sn))
{
$sql="select * from ".$GLOBALS['ecs']->table('erp_payment')." where payment_sn='".$payment_sn."'";
}
$res=$GLOBALS['db']->query($sql);
while($row=mysql_fetch_assoc($res))
{
$row['create_date']=local_date('Y-m-d',$row['create_time']);
$row['bill_date']=local_date('Y-m-d',$row['bill_time']);
$row['post_date']=local_date('Y-m-d',$row['post_time']);
$row['approve_date']=local_date('Y-m-d',$row['approve_time']);
$payment_id=$row['payment_id'];
$row['available_act']=get_payment_available_act($payment_id,'');
$account_id=$row['account_id'];
$row['account_info']=get_bank_account_info($account_id);
if($row['payment_style_id']==1)
{
$order_id=$row['bill_id'];
$row['order_info']=get_purchase_order_info($order_id);
$supplier_id=$row['pay_to'];
$row['supplier_info']=supplier_info($supplier_id);
$row['payment_details']=get_payment_details($payment_id);
}
$payment_array[]=$row;
}
return $payment_array;
}
function get_gathering_count($paras=array(),$start=-1,$num=-1)
{
$sql="select count(*) from ".$GLOBALS['ecs']->table('erp_gathering')." where 1 ";
if(isset($paras['gathering_id']) &&intval($paras['gathering_id']>0))
{
$sql.=" and gathering_id='".$paras['gathering_id']."' ";
}
if(isset($paras['status']) &&intval($paras['status']>0))
{
$sql.=" and gathering_status='".$paras['status']."' ";
}
if(isset($paras['start_time']) &&intval($paras['start_time']>0))
{
$sql.=" and create_time>='".$paras['start_time']."' ";
}
if(isset($paras['end_time']) &&intval($paras['end_time']>0))
{
$sql.=" and create_time<='".$paras['end_time']."' ";
}
if(isset($paras['gathering_sn']) &&!empty($paras['gathering_sn']))
{
$sql.=" and gathering_sn like '%".$paras['gathering_sn']."%'";
}
if(isset($paras['bill_sn']) &&!empty($paras['bill_sn']))
{
$sql.=" and bill_id in (select order_id from ".$GLOBALS['ecs']->table('order_info')." where order_sn like '%".$paras['bill_sn']."%')";
}
if(isset($paras['agency_id']) &&intval($paras['agency_id']) >0)
{
$sql.=" and agency_id='".$paras['agency_id']."'";
}
return $GLOBALS['db']->getOne($sql);
}
function get_gathering_list($paras=array(),$start=-1,$num=-1)
{
$sql="select * from ".$GLOBALS['ecs']->table('erp_gathering')." where 1 ";
if(isset($paras['gathering_id']) &&intval($paras['gathering_id']>0))
{
$sql.=" and gathering_id='".$paras['gathering_id']."' ";
}
if(isset($paras['status']) &&intval($paras['status']>0))
{
$sql.=" and gathering_status='".$paras['status']."' ";
}
if(isset($paras['start_time']) &&intval($paras['start_time']>0))
{
$sql.=" and create_time>='".$paras['start_time']."' ";
}
if(isset($paras['end_time']) &&intval($paras['end_time']>0))
{
$sql.=" and create_time<='".$paras['end_time']."' ";
}
if(isset($paras['gathering_sn']) &&!empty($paras['gathering_sn']))
{
$sql.=" and gathering_sn like '%".$paras['gathering_sn']."%'";
}
if(isset($paras['bill_sn']) &&!empty($paras['bill_sn']))
{
$sql.=" and bill_id in (select order_id from ".$GLOBALS['ecs']->table('order_info')." where order_sn like '%".$paras['bill_sn']."%')";
}
if(isset($paras['agency_id']) &&intval($paras['agency_id']) >0)
{
$sql.=" and agency_id='".$paras['agency_id']."'";
}
$sql.=" order by gathering_id desc ";
if($start!=-1 &&$num!=-1)
{
if($start<=0)
{
$start=0;
}
$sql.=" limit ".$start.",".$num;
}
$res=$GLOBALS['db']->query($sql);
while($row=mysql_fetch_assoc($res))
{
$row['create_date']=local_date('Y-m-d',$row['create_time']);
$row['bill_date']=local_date('Y-m-d',$row['bill_time']);
$gathering_id=$row['gathering_id'];
$row['available_act']=get_gathering_available_act($gathering_id,'');
$account_id=$row['account_id'];
$row['account_info']=get_bank_account_info($account_id);
if($row['gathering_style_id']==1)
{
$order_id=$row['bill_id'];
$row['order_info']=get_sales_order_info($order_id);
$user_id=$row['pay_from'];
$row['user_info']=get_user_info($user_id);
}
$sql="select agency_name from ".$GLOBALS['ecs']->table('agency')." where agency_id='".$row['agency_id']."'";
$row['agency_name']=$GLOBALS['db']->getOne($sql);
$gathering_array[]=$row;
}
return $gathering_array;
}
function get_gathering_info($gathering_id='',$gathering_sn='',$status='',$start_time='',$end_time='',$pay_from='',$start=-1,$num=-1)
{
if(empty($gathering_id) &&empty($gathering_sn))
{
$sql="select * from ".$GLOBALS['ecs']->table('erp_gathering')." where 1 ";
if(!empty($status))
{
$sql.=" and gathering_status='".$status."'";
}
if(!empty($start_time) &&empty($end_time))
{
$sql.=" and create_time >='".$start_time."'";
}
elseif(empty($start_time) &&!empty($end_time))
{
$sql.=" and create_time <='".$end_time."'";
}
elseif(!empty($start_time) &&!empty($end_time))
{
$sql.=" and create_time >='".$start_time."' and create_time <='".$end_time."'";
}
if(!empty($pay_from))
{
$sql.=" and pay_from='".$pay_from."'";
}
$sql.=" order by gathering_sn desc ";
if($start!=-1 &&$num!=-1)
{
if($start<=0)
{
$start=0;
}
$sql.=" limit ".$start.",".$num;
}
}
elseif(!empty($gathering_id) &&empty($gathering_sn))
{
if(is_gathering_exists($gathering_id))
{
$sql="select * from ".$GLOBALS['ecs']->table('erp_gathering')." where gathering_id=".$gathering_id."";
}
else{
return false;
}
}
elseif(empty($gathering_id) &&!empty($gathering_sn))
{
$sql="select * from ".$GLOBALS['ecs']->table('erp_gathering')." where gathering_sn='".$gathering_sn."'";
}
$res=$GLOBALS['db']->query($sql);
while($row=mysql_fetch_assoc($res))
{
$row['create_date']=local_date('Y-m-d',$row['create_time']);
$row['bill_date']=local_date('Y-m-d',$row['bill_time']);
$gathering_id=$row['gathering_id'];
$row['available_act']=get_gathering_available_act($gathering_id,'');
$account_id=$row['account_id'];
$row['account_info']=get_bank_account_info($account_id);
if($row['gathering_style_id']==1)
{
$order_id=$row['bill_id'];
$row['order_info']=get_sales_order_info($order_id);
$user_id=$row['pay_from'];
$row['user_info']=get_user_info($user_id);
}
$gathering_array[]=$row;
}
return $gathering_array;
}
function get_sales_order_info($order_id='')
{
if(empty($order_id))
{
return false;
}
else
{
$sql="select order_id,order_sn,user_id,consignee,pay_name,goods_amount,order_amount,money_paid,gathered_amount,add_time,confirm_time,pay_time,shipping_time from ".$GLOBALS['ecs']->table('order_info')."where order_id='".$order_id."'";
$order_info=$GLOBALS['db']->getRow($sql);
$order_info['add_time']=local_date("Y-m-d",$order_info['add_time']);
$order_info['confirm_time']=local_date("Y-m-d",$order_info['confirm_time']);
$order_info['pay_time']=local_date("Y-m-d",$order_info['pay_time']);
$order_info['shipping_time']=local_date("Y-m-d",$order_info['shipping_time']);
$order_info['receivable_amount']=$order_info['money_paid']+$order_info['order_amount'];
$order_info['receivable']=floatval($order_info['money_paid']+$order_info['order_amount']-$order_info['gathered_amount']);
$sql="select user_name from ".$GLOBALS['ecs']->table('users')." where user_id='".$order_info['user_id']."'";
$order_info['username']=$GLOBALS['db']->getOne($sql);
$sql="select * from ".$GLOBALS['ecs']->table('order_goods')." where order_id='".$order_info['order_id']."'";
$order_goods=$GLOBALS['db']->getAll($sql);
foreach($order_goods as $key=>$item)
{
$order_goods[$key]['goods_amount']=$item['goods_price']*$item['goods_number'];
$sql="select goods_id,cat_id,goods_sn,goods_name,brand_id,goods_thumb,goods_img,original_img from ".$GLOBALS['ecs']->table('goods')." where goods_id='".$item['goods_id']."'";
$goods_info=$GLOBALS['db']->getRow($sql);
if(!empty($goods_info['goods_thumb']))
{
$goods_info['goods_thumb']='../'.$goods_info['goods_thumb'];
}
if(!empty($goods_info['goods_img']))
{
$goods_info['goods_img']='../'.$goods_info['goods_img'];
}
if(!empty($goods_info['original_img']))
{
$goods_info['original_img']='../'.$goods_info['original_img'];
}
$order_goods[$key]['goods_info']=$goods_info;
}
$order_info['order_goods']=$order_goods;
return $order_info;
}
}
function get_user_info($user_id='')
{
if(empty($user_id))
{
return false;
}
else
{
$sql="select user_id,user_name from ".$GLOBALS['ecs']->table('users')." where user_id='".$user_id."'";
return $GLOBALS['db']->getRow($sql);
}
}
function lock_payment($payment_id='',$act='')
{
if(empty($payment_id))
{
return false;
}
if(check_payment_accessibility($payment_id,$act))
{
$user_id=erp_get_admin_id();
$lock_time=gmtime();
$last_act_time=gmtime();
$sql="update ".$GLOBALS['ecs']->table('erp_payment')." set locked_by='".$user_id."'";
$sql.=", locked_time='".$lock_time."'";
$sql.=", last_act_time='".$last_act_time."'";
$sql.=", is_locked='1'";
$sql.=" where payment_id='".$payment_id."'";
$GLOBALS['db']->query($sql);
return true;
}
else{
return false;
}
}
function lock_gathering($gathering_id='',$act='')
{
if(empty($gathering_id))
{
return false;
}
if(check_gathering_accessibility($gathering_id,$act))
{
$user_id=erp_get_admin_id();
$lock_time=gmtime();
$last_act_time=gmtime();
$sql="update ".$GLOBALS['ecs']->table('erp_gathering')." set locked_by='".$user_id."'";
$sql.=", locked_time='".$lock_time."'";
$sql.=", last_act_time='".$last_act_time."'";
$sql.=", is_locked='1'";
$sql.=" where gathering_id='".$gathering_id."'";
$GLOBALS['db']->query($sql);
return true;
}
else{
return false;
}
}
function check_payment_accessibility($payment_id='',$act='')
{
if(empty($act) &&empty($payment_id))
{
return false;
}
else
{
$sql="select is_locked,payment_status,last_act_time,locked_by from ".$GLOBALS['ecs']->table('erp_payment')." where payment_id='".$payment_id."'";
$res=$GLOBALS['db']->getRow($sql);
if($res==false)
{
return false;
}
else{
$is_locked=$res['is_locked'];
$payment_status=$res['payment_status'];
$last_act_time=$res['last_act_time'];
$locked_by=$res['locked_by'];
$current_time=gmtime();
$if_overtime=(($current_time-$last_act_time)>(10))?1:0;
$current_user_id=erp_get_admin_id();
if($act=='view')
{
if(admin_priv('erp_finance_view','',false) ||admin_priv('erp_finance_manage','',false) ||admin_priv('erp_finance_approve','',false))
{
return true;
}
}
elseif($act=='edit'||$act=='delete'||$act=='post_to_approve')
{
if($payment_status==1)
{
if(admin_priv('erp_finance_manage','',false) &&($is_locked==0 ||($is_locked==1 &&$if_overtime==1 &&$current_user_id!=$locked_by) ||($is_locked==1 &&$current_user_id==$locked_by)))
{
return true;
}
else{
return false;
}
}
else{
return false;
}
}
elseif($act=='approve')
{
if($payment_status==2)
{
if(admin_priv('erp_finance_approve','',false) &&($is_locked==0 ||($is_locked==1 &&$if_overtime==1 &&$current_user_id!=$locked_by) ||($is_locked==1 &&$current_user_id==$locked_by)))
{
return true;
}
else{
return false;
}
}
else{
return false;
}
}
else{
return false;
}
}
}
}
function check_gathering_accessibility($gathering_id='',$act='')
{
if(empty($act) &&empty($gathering_id))
{
return false;
}
else
{
$sql="select is_locked,gathering_status,last_act_time,locked_by from ".$GLOBALS['ecs']->table('erp_gathering')." where gathering_id='".$gathering_id."'";
$res=$GLOBALS['db']->getRow($sql);
if($res==false)
{
return false;
}
else{
$is_locked=$res['is_locked'];
$gathering_status=$res['gathering_status'];
$last_act_time=$res['last_act_time'];
$locked_by=$res['locked_by'];
$current_time=gmtime();
$if_overtime=(($current_time-$last_act_time)>(10))?1:0;
$current_user_id=erp_get_admin_id();
if($act=='view')
{
return(admin_priv('erp_finance_view','',false));
}
elseif($act=='edit'||$act=='delete'||$act=='post_to_approve')
{
if($gathering_status==1)
{
if(admin_priv('erp_finance_manage','',false) &&($is_locked==0 ||($is_locked==1 &&$if_overtime==1 &&$current_user_id!=$locked_by) ||($is_locked==1 &&$current_user_id==$locked_by)))
{
return true;
}
else{
return false;
}
}
else{
return false;
}
}
elseif($act=='approve')
{
if($gathering_status==2)
{
if(admin_priv('erp_finance_approve','',false) &&($is_locked==0 ||($is_locked==1 &&$if_overtime==1 &&$current_user_id!=$locked_by) ||($is_locked==1 &&$current_user_id==$locked_by)))
{
return true;
}
else{
return false;
}
}
else{
return false;
}
}
else{
return false;
}
}
}
}
function get_payment_available_act($payment_id='',$payment_sn='')
{
$sql='';
$act=array('view'=>0,'edit'=>0,'delete'=>0,'post_to_approve'=>0,'approve'=>0);
$is_locked='';
$payment_status='';
if(empty($payment_id) &&empty($payment_sn))
{
return false;
}
if(empty($payment_sn) &&!empty($payment_id))
{
$sql="select is_locked,payment_status,last_act_time,locked_by from ".$GLOBALS['ecs']->table('erp_payment')." where payment_id='".$payment_id."'";
}
elseif(!empty($payment_sn) &&empty($payment_id))
{
$sql="select is_locked,payment_status,last_act_time,locked_by from ".$GLOBALS['ecs']->table('erp_payment')." where payment_sn='".$payment_sn."'";
}
$res=$GLOBALS['db']->getRow($sql);
if($res==false)
{
return false;
}
else{
$is_locked=$res['is_locked'];
$payment_status=$res['payment_status'];
$last_act_time=$res['last_act_time'];
$locked_by=$res['locked_by'];
}
$current_time=gmtime();
$if_overtime=(($current_time-$last_act_time)>(10))?1:0;
$current_user_id=!empty($_COOKIE['ECSCP']['admin_id'])?$_COOKIE['ECSCP']['admin_id']:'';
$current_user_id=isset($_SESSION['admin_id'])?$_SESSION['admin_id']:'';
if($payment_status==1)
{
if(admin_priv('erp_finance_view','',false) ||admin_priv('erp_finance_manage','',false) ||admin_priv('erp_finance_approve','',false))
{
$act['view']=1;
}
else{
$act['view']=0;
}
if(admin_priv('erp_finance_manage','',false) &&($is_locked==0 ||($is_locked==1 &&$if_overtime==1 &&$current_user_id!=$locked_by) ||($is_locked==1 &&$current_user_id==$locked_by)))
{
$act['edit']=1;
$act['delete']=1;
$act['post_to_approve']=1;
}
else{
$act['edit']=0;
$act['delete']=0;
$act['post_to_approve']=0;
}
$act['approve']=0;
return $act;
}
elseif($payment_status==2)
{
if(admin_priv('erp_finance_view','',false) ||admin_priv('erp_finance_manage','',false) ||admin_priv('erp_finance_approve','',false))
{
$act['view']=1;
}
else{
$act['view']=0;
}
$act['edit']=0;
$act['delete']=0;
$act['post_to_approve']=0;
if(admin_priv('erp_finance_approve','',false) &&($is_locked==0 ||($is_locked==1 &&$if_overtime==1 &&$current_user_id!=$locked_by) ||($is_locked==1 &&$current_user_id==$locked_by)))
{
$act['approve']=1;
}
else
{
$act['approve']=0;
}
return $act;
}
elseif($payment_status==3 ||$payment_status==4)
{
$act['view']=0;
$act['edit']=0;
$act['delete']=0;
$act['post_to_approve']=0;
$act['approve']=0;
return $act;
}
else
{
return false;
}
}
function get_gathering_available_act($gathering_id='',$gathering_sn='')
{
$act=array('view'=>0,'edit'=>0,'delete'=>0,'post_to_approve'=>0,'approve'=>0);
$is_locked='';
$gathering_status='';
if(empty($gathering_id) &&empty($gathering_sn))
{
return false;
}
if(empty($gathering_sn) &&!empty($gathering_id))
{
$sql="select is_locked,gathering_status,last_act_time,locked_by from ".$GLOBALS['ecs']->table('erp_gathering')." where gathering_id='".$gathering_id."'";
}
elseif(!empty($gathering_sn) &&empty($gathering_id))
{
$sql="select is_locked,gathering_status,last_act_time,locked_by from ".$GLOBALS['ecs']->table('erp_gathering')." where gathering_sn='".$gathering_sn."'";
}
$res=$GLOBALS['db']->getRow($sql);
if($res==false)
{
return false;
}
else{
$is_locked=$res['is_locked'];
$gathering_status=$res['gathering_status'];
$last_act_time=$res['last_act_time'];
$locked_by=$res['locked_by'];
}
$current_time=gmtime();
$if_overtime=(($current_time-$last_act_time)>(10))?1:0;
$current_user_id=!empty($_COOKIE['ECSCP']['admin_id'])?$_COOKIE['ECSCP']['admin_id']:'';
$current_user_id=isset($_SESSION['admin_id'])?$_SESSION['admin_id']:'';
if($gathering_status==1)
{
if(admin_priv('erp_finance_view','',false) ||admin_priv('erp_finance_manage','',false) ||admin_priv('erp_finance_approve','',false))
{
$act['view']=1;
}
else{
$act['view']=0;
}
if(admin_priv('erp_finance_manage','',false) &&($is_locked==0 ||($is_locked==1 &&$if_overtime==1 &&$current_user_id!=$locked_by) ||($is_locked==1 &&$current_user_id==$locked_by)))
{
$act['edit']=1;
$act['delete']=1;
$act['post_to_approve']=1;
}
else{
$act['edit']=0;
$act['delete']=0;
$act['post_to_approve']=0;
}
$act['approve']=0;
return $act;
}
elseif($gathering_status==2)
{
if(admin_priv('erp_finance_view','',false) ||admin_priv('erp_finance_manage','',false) ||admin_priv('erp_finance_approve','',false))
{
$act['view']=1;
}
else{
$act['view']=0;
}
$act['edit']=0;
$act['delete']=0;
$act['post_to_approve']=0;
if(admin_priv('erp_finance_approve','',false) &&($is_locked==0 ||($is_locked==1 &&$if_overtime==1 &&$current_user_id!=$locked_by) ||($is_locked==1 &&$current_user_id==$locked_by)))
{
$act['approve']=1;
}
else
{
$act['approve']=0;
}
return $act;
}
elseif($gathering_status==3 ||$gathering_status==4)
{
$act['view']=0;
$act['edit']=0;
$act['delete']=0;
$act['post_to_approve']=0;
$act['approve']=0;
return $act;
}
else
{
return false;
}
}
function gen_gathering_sn()
{
$sql="select gathering_sn from ".$GLOBALS['ecs']->table('erp_gathering')." order by gathering_sn desc limit 1";
$result=$GLOBALS['db']->getOne($sql);
if(!empty($result))
{
$year_date=substr($result,2,4);
$current_year_date=local_date("ym");
if($current_year_date==$year_date)
{
$sn=substr($result,6,3);
$sn=intval($sn)+1;
$sn=gen_zero_str(3-strlen($sn)).$sn;
$sn='GA'.$year_date.$sn;
return $sn;
}
else
{
$sn='GA'.$current_year_date."001";
return $sn;
}
}
else{
$current_year_date=local_date("ym");
$sn='GA'.$current_year_date."001";
return $sn;
}
}
function gen_payment_sn()
{
$sql="select payment_sn from ".$GLOBALS['ecs']->table('erp_payment')." order by payment_sn desc limit 1";
$result=$GLOBALS['db']->getOne($sql);
if(!empty($result))
{
$year_date=substr($result,2,4);
$current_year_date=local_date("ym");
if($current_year_date==$year_date)
{
$sn=substr($result,6,3);
$sn=intval($sn)+1;
$sn=gen_zero_str(3-strlen($sn)).$sn;
$sn='PY'.$year_date.$sn;
return $sn;
}
else
{
$sn='PY'.$current_year_date."001";
return $sn;
}
}
else{
$current_year_date=local_date("ym");
$sn='PY'.$current_year_date."001";
return $sn;
}
}
function get_warehousing_info($warehousing_id='')
{
$sql="select warehousing_id,warehousing_sn,create_time,warehousing_style_id,warehouse_id from ".$GLOBALS['ecs']->table('erp_warehousing')." where warehousing_id='".$warehousing_id."'";
$warehousing_info= $GLOBALS['db']->getRow($sql);
$warehousing_info['create_date']=local_date('Y-m-d',$warehousing_info['create_time']);
return $warehousing_info;
}
function get_warehousing_item_info($warehousing_id='')
{
if(empty($warehousing_id))
{
return false;
}
else
{
$sql="select * from ".$GLOBALS['ecs']->table('erp_warehousing_item')." where warehousing_id='".$warehousing_id."'";
$res=$GLOBALS['db']->query($sql);
while($row=mysql_fetch_assoc($res))
{
$goods_id=$row['goods_id'];
$sql="select cat_id,goods_sn,goods_name,goods_thumb,goods_img from ".$GLOBALS['ecs']->table('goods')." where goods_id='".$goods_id."'";
$goods_info=$GLOBALS['db']->getRow($sql);
if(!empty($goods_info['goods_thumb']))
{
$goods_info['goods_thumb']='../'.$goods_info['goods_thumb'];
}
if(!empty($goods_info['goods_img']))
{
$goods_info['goods_img']='../'.$goods_info['goods_img'];
}
$row['goods_info']=$goods_info;
$attr_id=$row['attr_id'];
$row['goods_attr']=get_attr_info($attr_id);
$warehousing_item_info[]=$row;
}
return $warehousing_item_info;
}
}
function get_purchase_order_info($order_id='')
{
if(empty($order_id))
{
return false;
}
$sql="select order_id,order_sn,description,supplier_id,create_time from ".$GLOBALS['ecs']->table('erp_order')." where order_id=".$order_id."";
$order_info=$GLOBALS['db']->getRow($sql);
$order_info['create_date']=local_date('Y-m-d',$order_info['create_time']);
$supplier_id=$order_info['supplier_id'];
$sql="select name,code from ".$GLOBALS['ecs']->table('erp_supplier')." where supplier_id='".$supplier_id."'";
$row=$GLOBALS['db']->getRow($sql);
$order_info['supplier_name']=$row['name'];
$order_info['supplier_code']=$row['code'];
$sql="select sum(order_qty) as order_qty, sum(amount) as order_amount from ".$GLOBALS['ecs']->table('erp_order_item')." where order_id='".$order_id."' group by order_id";
$row=$GLOBALS['db']->getRow($sql);
$order_info['order_qty']=$row['order_qty'];
$order_info['order_amount']=$row['order_amount'];
return $order_info;
}
function get_payable_order_count($paras=array())
{
$sql="select count(distinct order_id) from ".$GLOBALS['ecs']->table('erp_warehousing')." as w where 1 and w.warehousing_id in (";
$sql.=" select distinct wi.warehousing_id from ".$GLOBALS['ecs']->table('erp_warehousing_item')." as wi where wi.paid_amount<wi.account_payable )";
if(isset($paras['order_id']) &&intval($paras['order_id']>0))
{
$sql.=" and w.order_id='".$paras['order_id']."' ";
}
if(isset($paras['order_sn']) &&!empty($paras['order_sn']))
{
$sql.=" and w.order_id in (select order_id from ".$GLOBALS['ecs']->table('erp_order')." as o where o.order_sn like '%".$paras['order_sn']."%')";
}
if(isset($paras['supplier_name']) &&!empty($paras['supplier_name']))
{
$sql.=" and w.order_id in (select order_id from ".$GLOBALS['ecs']->table('erp_order')." as o,".$GLOBALS['ecs']->table('erp_supplier')." as s where o.supplier_id=s.supplier_id and s.name like '%".$paras['supplier_name']."%')";
}
if(isset($paras['agency_id']) &&intval($paras['agency_id']) >0)
{
$sql.=" and w.order_id in (select order_id from ".$GLOBALS['ecs']->table('erp_order')." as o where o.agency_id='".$paras['agency_id']."')";
}
return $GLOBALS['db']->getOne($sql);
}
function get_payable_order_list($paras=array(),$start=-1,$num=-1)
{
$sql="select distinct order_id from ".$GLOBALS['ecs']->table('erp_warehousing')." as w where 1 and w.warehousing_id in (";
$sql.=" select distinct wi.warehousing_id from ".$GLOBALS['ecs']->table('erp_warehousing_item')." as wi where wi.paid_amount<wi.account_payable )";
if(isset($paras['order_id']) &&intval($paras['order_id']>0))
{
$sql.=" and w.order_id='".$paras['order_id']."' ";
}
if(isset($paras['order_sn']) &&!empty($paras['order_sn']))
{
$sql.=" and w.order_id in (select order_id from ".$GLOBALS['ecs']->table('erp_order')." as o where o.order_sn like '%".$paras['order_sn']."%')";
}
if(isset($paras['supplier_name']) &&!empty($paras['supplier_name']))
{
$sql.=" and w.order_id in (select order_id from ".$GLOBALS['ecs']->table('erp_order')." as o,".$GLOBALS['ecs']->table('erp_supplier')." as s where o.supplier_id=s.supplier_id and s.name like '%".$paras['supplier_name']."%')";
}
if(isset($paras['agency_id']) &&intval($paras['agency_id']) >0)
{
$sql.=" and w.order_id in (select order_id from ".$GLOBALS['ecs']->table('erp_order')." as o where o.agency_id='".$paras['agency_id']."')";
}
$sql.=" order by w.warehousing_id desc ";
if($start!=-1 &&$num!=-1)
{
if($start<=0)
{
$start=0;
}
$sql.=" limit ".$start.",".$num;
}
$res=$GLOBALS['db']->query($sql);
while(list($order_id)=mysql_fetch_row($res))
{
$payable_order[$order_id]['order_id']=$order_id;
$payable_order[$order_id]['order_info']=get_purchase_order_info($order_id);
$sql2="select warehousing_id from ".$GLOBALS['ecs']->table('erp_warehousing')." where order_id='".$order_id."'";
$res2=$GLOBALS['db']->query($sql2);
while(list($warehousing_id)=mysql_fetch_row($res2))
{
$payable_order[$order_id]['warehousing_info'][$warehousing_id]=get_warehousing_info($warehousing_id);
$payable_order[$order_id]['warehousing_info'][$warehousing_id]['wareshouing_item_info']=get_warehousing_item_info($warehousing_id);
}
$sql="select agency_name from ".$GLOBALS['ecs']->table('agency')." where agency_id in (select agency_id from ".$GLOBALS['ecs']->table('erp_order')." where order_id='".$order_id."')";
$payable_order[$order_id]['agency_name']=$GLOBALS['db']->getOne($sql);
}
if(!empty($payable_order))
{
foreach($payable_order as $d_order)
{
$order_id=$d_order['order_id'];
$total_payable_amount=0;
$total_paid_amount=0;
if(!empty($d_order['warehousing_info']))
{
foreach($d_order['warehousing_info'] as $key1 =>$w_info)
{
if(!empty($w_info['wareshouing_item_info']))
{
foreach($w_info['wareshouing_item_info'] as $key2=>$w_i_info)
{
$total_payable_amount+=$w_i_info['account_payable'];
$total_paid_amount+=$w_i_info['paid_amount'];
$payable_order[$order_id]['warehousing_info'][$key1]['wareshouing_item_info'][$key2]['payable']=$w_i_info['account_payable']-$w_i_info['paid_amount'];
}
}
}
}
$payable_order[$order_id]['total_payable_amount']=$total_payable_amount;
$payable_order[$order_id]['total_paid_amount']=$total_paid_amount;
$payable_order[$order_id]['payable']=$total_payable_amount-$total_paid_amount;
}
}
return $payable_order;
}
function bank_account_count($paras=array())
{
$sql="select count(*) from ".$GLOBALS['ecs']->table('erp_bank_account')." where 1";
if(isset($paras['account_name']) &&!empty($paras['account_name']))
{
$sql.=" and account_name like '%".$paras['account_name']."%' ";
}
if(isset($paras['account_no']) &&!empty($paras['account_no']))
{
$sql.=" and account_no like '%".$paras['account_no']."%' ";
}
if(isset($paras['is_valid']) &&intval($paras['is_valid']>=0))
{
$sql.=" and is_valid='".$paras['is_valid']."' ";
}
if(isset($paras['agency_id']) &&intval($paras['agency_id']) >0)
{
$sql.=" and agency_id='".$paras['agency_id']."'";
}
return $GLOBALS['db']->getOne($sql);
}
function bank_account_list($paras=array(),$start=-1,$num=-1)
{
$sql="select * from ".$GLOBALS['ecs']->table('erp_bank_account')." where 1";
if(isset($paras['account_name']) &&!empty($paras['account_name']))
{
$sql.=" and account_name like '%".$paras['account_name']."%' ";
}
if(isset($paras['account_no']) &&!empty($paras['account_no']))
{
$sql.=" and account_no like '%".$paras['account_no']."%' ";
}
if(isset($paras['is_valid']) &&intval($paras['is_valid']>=0))
{
$sql.=" and is_valid='".$paras['is_valid']."' ";
}
if(isset($paras['agency_id']) &&intval($paras['agency_id']) >0)
{
$sql.=" and agency_id='".$paras['agency_id']."'";
}
if($start!=-1 &&$num!=-1)
{
if($start<=0)
{
$start=0;
}
$sql.=" limit ".$start.",".$num;
}
$account_list=$GLOBALS['db']->getAll($sql);
if(!empty($account_list))
{
foreach($account_list as $key =>$account)
{
if($account['agency_id'] >0)
{
$sql="select agency_name from ".$GLOBALS['ecs']->table('agency')." where agency_id='".$account['agency_id']."'";
$account_list[$key]['agency_name']=$GLOBALS['db']->getOne($sql);
}
$sql="select * from ".$GLOBALS['ecs']->table('erp_gathering')." where account_id='".$account['account_id']."' limit 1";
$record=$GLOBALS['db']->getRow($sql);
$account_list[$key]['has_gathering']=!empty($record)?true : false;
$sql="select * from ".$GLOBALS['ecs']->table('erp_payment')." where account_id='".$account['account_id']."' limit 1";
$record=$GLOBALS['db']->getRow($sql);
$account_list[$key]['has_payment']=!empty($record)?true : false;
$sql="select * from ".$GLOBALS['ecs']->table('erp_bank_account_record')." where account_id='".$account['account_id']."' limit 1";
$record=$GLOBALS['db']->getRow($sql);
$account_list[$key]['has_record']=!empty($record)?true : false;
}
}
return $account_list;
}
function is_agency_bank_account($account_id,$agency_id=0)
{
if(empty($agency_id))
{
return true;
}
$sql="select * from ".$GLOBALS['ecs']->table('erp_bank_account')." where account_id='".$account_id."' and agency_id='".$agency_id."'";
$result=$GLOBALS['db']->getRow($sql);
return !empty($result)?true : false;
}
function get_bank_account_info($account_id)
{
$sql="select * from ".$GLOBALS['ecs']->table('erp_bank_account')." where account_id='".$account_id."'";
$bank_account=$GLOBALS['db']->getRow($sql);
return $bank_account;
}
function get_bank_account_details_count($paras=array(),$start=-1,$num=-1)
{
$sql="select count(*) from ".$GLOBALS['ecs']->table('erp_bank_account_record')." where 1 ";
if(isset($paras['account_id']) &&intval($paras['account_id']>0))
{
$sql.=" and account_id='".$paras['account_id']."' ";
}
if(isset($paras['start_time']) &&intval($paras['start_time']>0))
{
$sql.=" and record_time>='".$paras['start_time']."' ";
}
if(isset($paras['end_time']) &&intval($paras['end_time']>0))
{
$sql.=" and record_time<='".$paras['end_time']."' ";
}
if(isset($paras['agency_id']) &&intval($paras['agency_id']) >0)
{
$sql.=" and account_id in (select account_id from ".$GLOBALS['ecs']->table('erp_bank_account')." where agency_id='".$paras['agency_id']."')";
}
return $GLOBALS['db']->getOne($sql);
}
function get_bank_account_details_list($paras=array(),$start=-1,$num=-1)
{
$sql="select * from ".$GLOBALS['ecs']->table('erp_bank_account_record')." where 1 ";
if(isset($paras['account_id']) &&intval($paras['account_id']>0))
{
$sql.=" and account_id='".$paras['account_id']."' ";
}
if(isset($paras['start_time']) &&intval($paras['start_time']>0))
{
$sql.=" and record_time>='".$paras['start_time']."' ";
}
if(isset($paras['end_time']) &&intval($paras['end_time']>0))
{
$sql.=" and record_time<='".$paras['end_time']."' ";
}
if(isset($paras['agency_id']) &&intval($paras['agency_id']) >0)
{
$sql.=" and account_id in (select account_id from ".$GLOBALS['ecs']->table('erp_bank_account')." where agency_id='".$paras['agency_id']."')";
}
$sql.=" order by account_record_id desc ";
if($start!=-1 &&$num!=-1)
{
if($start<=0)
{
$start=0;
}
$sql.=" limit ".$start.",".$num;
}
$bank_account_details=$GLOBALS['db']->getAll($sql);
foreach($bank_account_details as $key=>$item)
{
$income_expenses=$item['income_expenses'];
$order_id=$item['bill_id'];
$payment_gathering_id=$item['payment_gathering_id'];
$bank_account_details[$key]['record_date']=local_date('Y-m-d',$item['record_time']);
if(!empty($order_id))
{
if($income_expenses<0)
{
$sql="select order_sn from ".$GLOBALS['ecs']->table('erp_order')." where order_id='".$order_id."'";
$bank_account_details[$key]['order_sn']=$GLOBALS['db']->getOne($sql);
$sql="select payment_id,payment_sn from ".$GLOBALS['ecs']->table('erp_payment')." where payment_id='".$payment_gathering_id."'";
$bank_account_details[$key]['payment_gathering_info']=$GLOBALS['db']->getRow($sql);
$bank_account_details[$key]['payment_gathering_info']['style']='payment';
}
elseif($income_expenses>0)
{
$sql="select order_sn from ".$GLOBALS['ecs']->table('order_info')." where order_id='".$order_id."'";
$bank_account_details[$key]['order_sn']=$GLOBALS['db']->getOne($sql);
$sql="select gathering_id,gathering_sn from ".$GLOBALS['ecs']->table('erp_gathering')." where gathering_id='".$payment_gathering_id."'";
$bank_account_details[$key]['payment_gathering_info']=$GLOBALS['db']->getRow($sql);
$bank_account_details[$key]['payment_gathering_info']['style']='gathering';
}
}
}
return $bank_account_details;
}
function get_bank_account_details($account_id='',$start_time='',$end_time='',$start=-1,$num=-1)
{
if(empty($account_id))
{
return false;
}
else{
$sql="select * from ".$GLOBALS['ecs']->table('erp_bank_account_record')." where account_id='".$account_id."' ";
if(!empty($start_time) &&empty($end_time))
{
$sql.=" and record_time >='".$start_time."'";
}
elseif(empty($start_time) &&!empty($end_time))
{
$sql.=" and record_time <='".$end_time."'";
}
elseif(!empty($start_time) &&!empty($end_time))
{
$sql.=" and record_time >='".$start_time."' and record_time <='".$end_time."'";
}
$sql.=" order by record_time ";
if($start!=-1 &&$num!=-1)
{
if($start<=0)
{
$start=0;
}
$sql.=" limit ".$start.",".$num;
}
$bank_account_details=$GLOBALS['db']->getAll($sql);
foreach($bank_account_details as $key=>$item)
{
$income_expenses=$item['income_expenses'];
$order_id=$item['bill_id'];
$payment_gathering_id=$item['payment_gathering_id'];
$bank_account_details[$key]['record_date']=local_date('Y-m-d',$item['record_time']);
if(!empty($order_id))
{
if($income_expenses<0)
{
$sql="select order_sn from ".$GLOBALS['ecs']->table('erp_order')." where order_id='".$order_id."'";
$bank_account_details[$key]['order_sn']=$GLOBALS['db']->getOne($sql);
$sql="select payment_id,payment_sn from ".$GLOBALS['ecs']->table('erp_payment')." where payment_id='".$payment_gathering_id."'";
$bank_account_details[$key]['payment_gathering_info']=$GLOBALS['db']->getRow($sql);
$bank_account_details[$key]['payment_gathering_info']['style']='payment';
}
elseif($income_expenses>0)
{
$sql="select order_sn from ".$GLOBALS['ecs']->table('order_info')." where order_id='".$order_id."'";
$bank_account_details[$key]['order_sn']=$GLOBALS['db']->getOne($sql);
$sql="select gathering_id,gathering_sn from ".$GLOBALS['ecs']->table('erp_gathering')." where gathering_id='".$payment_gathering_id."'";
$bank_account_details[$key]['payment_gathering_info']=$GLOBALS['db']->getRow($sql);
$bank_account_details[$key]['payment_gathering_info']['style']='gathering';
}
}
}
return $bank_account_details;
}
}
?>
