<?php

function gen_zero_str($num=0)
{
	$res="";
	if($num==0)
	{
		return $res;
	}
	for($i=1;$i<=$num;$i++)
	{
		$res.="0";
	}
	return $res;
}
function format_code($code)
{
	if(!is_int($code))
	{
		return false;
	}
	else
	{
		$code=strval($code);
		if(strlen($code)<5)
		{
			$code=gen_zero_str(5-strlen($code)).$code;
		}
	}
	return $code;
}
function get_admin($user_id='')
{
	if(empty($user_id))
	{
		$sql="select user_id,user_name from ".$GLOBALS['ecs']->table('admin_user')." where 1";
		return $GLOBALS['db']->getAll($sql);
	}
	else
	{
		$sql="select user_id,user_name from ".$GLOBALS['ecs']->table('admin_user')." where user_id='".$user_id."'";
		return $GLOBALS['db']->getRow($sql);
	}
}
function get_admin_agency_id($user_id)
{
	if(!isset($_SESSION['agency_id']))
	{
		$sql="select agency_id from ".$GLOBALS['ecs']->table('admin_user')." where user_id='".$user_id."'";
		$_SESSION['agency_id']=$GLOBALS['db']->getOne($sql);
		$_SESSION['agency_id']=empty($_SESSION['agency_id'])?0 : $_SESSION['agency_id'];
	}
	return $_SESSION['agency_id'];
}
function agency_list($agency_ids=array())
{
	$result=array();
	$sql="select * from ".$GLOBALS['ecs']->table('agency')." where 1";
	$res=$GLOBALS['db']->query($sql);
	while($row=mysql_fetch_assoc($res))
	{
		if(!empty($agency_ids))
		{
			if(in_array($row['agency_id'],$agency_ids))
			{
				$row['selected']=1;
			}
			else
			{
				$row['selected']=0;
			}
		}
		else
		{
			$row['selected']=0;
		}
		$result[]=$row;
	}
	return $result;
}
function admin_list($admin_ids=array())
{
	$result=array();
	$sql="select user_id,user_name from ".$GLOBALS['ecs']->table('admin_user')." where is_disable = 0 order by user_name";
	$res=$GLOBALS['db']->query($sql);
	while($row=mysql_fetch_assoc($res))
	{
		if(!empty($admin_ids))
		{
			if(in_array($row['user_id'],$admin_ids))
			{
				$row['selected']=1;
			}
			else
			{
				$row['selected']=0;
			}
		}
		else
		{
			$row['selected']=0;
		}
		$result[]=$row;
	}
	return $result;
}
function erp_get_admin_id()
{
	$user_id=!empty($_COOKIE['ECSCP']['admin_id'])?$_COOKIE['ECSCP']['admin_id']:'-1';
	$user_id=isset($_SESSION['admin_id'])?$_SESSION['admin_id'] : $user_id;
	return $user_id;
}

?>