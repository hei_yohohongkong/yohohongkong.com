<?php

require_once(dirname(__FILE__) .'/lib_erp_common.php');
function update_attr_stock($goods_id)
{
    $warehouse_ids=array();
    $sql="select warehouse_id from ".$GLOBALS['ecs']->table('erp_warehouse')." where is_valid='1'";
    $res=$GLOBALS['db']->query($sql);
    if(!$res)
    {
        return false;
    }
    while($row=mysql_fetch_assoc($res))
    {
        $warehouse_ids[]=$row['warehouse_id'];
    }
    $sql="select attr_id from ".$GLOBALS['ecs']->table('erp_goods_attr')." where goods_id='".$goods_id."'";
    $res=$GLOBALS['db']->query($sql);
    if(!$res)
    {
        return false;
    }
    while($row=mysql_fetch_assoc($res))
    {
        foreach($warehouse_ids as $key =>$warehouse_id)
        {
            $sql="select attr_id from ".$GLOBALS['ecs']->table('erp_goods_attr_stock')." where attr_id='".$row['attr_id']."' and warehouse_id='".$warehouse_id."'";
            $attr_id=$GLOBALS['db']->getOne($sql);
            if(empty($attr_id))
            {
                $sql="insert into ".$GLOBALS['ecs']->table('erp_goods_attr_stock')." set attr_id='".$row['attr_id']."',goods_id='".$goods_id."',warehouse_id='".$warehouse_id."'";
                if(!$GLOBALS['db']->query($sql))	
                {
                    return false;
                }
            }
        }
    }
    $sql="select distinct attr_id from ".$GLOBALS['ecs']->table('erp_goods_attr_stock')." where goods_id='".$goods_id."'";
    $res=$GLOBALS['db']->query($sql);
    while($row=mysql_fetch_assoc($res))
    {
        $sql="select attr_id from ".$GLOBALS['ecs']->table('erp_goods_attr')." where attr_id='".$row['attr_id']."'";
        $attr_id=$GLOBALS['db']->getOne($sql);
        if(empty($attr_id))
        {
            $sql="delete from ".$GLOBALS['ecs']->table('erp_goods_attr_stock')." where attr_id='".$row['attr_id']."' and goods_qty='0'";
            if(!$GLOBALS['db']->query($sql))
            {
                return false;
            }
        }
    }
    return true;
}
function goods_attr($goods_id,$attr_id=0)
{
    if($attr_id >0)
    {
        $sql="select goods_attr_id_list from ".$GLOBALS['ecs']->table('erp_goods_attr')." where attr_id='".$attr_id."'";
        $goods_attr_id_list=$GLOBALS['db']->getOne($sql);
        if($goods_attr_id_list!='$$')
        {
            $goods_attr_id_list=explode(',',$goods_attr_id_list);
            sort($goods_attr_id_list);
        }
        else
        {
            $goods_attr_id_list=array();
        }
    }
    else
    {
        $goods_attr_id_list=array();
    }
    $sql="select ga.goods_attr_id,ga.attr_id,ga.attr_value,a.attr_name,a.attr_id,a.is_dynamic from ".$GLOBALS['ecs']->table('goods_attr')." as ga ";
    $sql.=" join ".$GLOBALS['ecs']->table('attribute')." as a ";
    $sql.=" on ga.attr_id=a.attr_id and a.attr_type='1' ";
    $sql.=" where ga.goods_id='".$goods_id."'";
    $goods_attrs=$GLOBALS['db']->getAll($sql);
    $result=array();
    if(!empty($goods_attrs))
    {
        foreach($goods_attrs as $key =>$attr)
        {
            $tmp['attr_id']=$attr['attr_id'];
            $tmp['attr_name']=$attr['attr_name'];
            $tmp['goods_attr_id']=$attr['goods_attr_id'];
            $tmp['attr_value']=$attr['attr_value'];
            $tmp['is_dynamic']=$attr['is_dynamic'];
            if(!empty($goods_attr_id_list) &&in_array($attr['goods_attr_id'],$goods_attr_id_list))
            {
                $tmp['selected']=1;
            }
            else
            {
                $tmp['selected']=0;
            }
            $result[$attr['attr_id']][]=$tmp;
        }
    }
    return $result;
}
function update_attr($goods_id)
{
    $goods_attrs=goods_attr($goods_id);
    $goods_attr_list=array();
    if(!empty($goods_attrs))
    {
        foreach($goods_attrs as $key =>$item)
        {
            $tmp=array();
            if(!empty($item))
            {
                foreach($item as $k =>$i)
                {
                    $tmp[]=$i['goods_attr_id'];
                }
            }
            $goods_attr_list[]=$tmp;
        }
    }
    $goods_attr_list=get_array_combination($goods_attr_list);
    if(!empty($goods_attr_list))
    {
        foreach($goods_attr_list as $key =>$item)
        {
            if(!is_attr_exists($goods_id,$item))
            {
                if(!insert_attr_item($goods_id,$item))
                {
                    return false;
                }
            }
        }
        $sql="select attr_id,goods_qty from ".$GLOBALS['ecs']->table('erp_goods_attr')." where goods_id='".$goods_id."' and goods_attr_id_list='$$'";
        $attr_info=$GLOBALS['db']->getRow($sql);
        if($attr_info['goods_qty']==0)	
        {
            $sql="delete from ".$GLOBALS['ecs']->table('erp_goods_attr')." where attr_id='".$attr_info['attr_id']."'";
            if(!$GLOBALS['db']->query($sql))
            {
                return false;
            }
        }
    }
    else
    {
        $goods_attr=goods_attr($goods_id);
        if(empty($goods_attr))
        {
            $sql="select attr_id from ".$GLOBALS['ecs']->table('erp_goods_attr')." where goods_id='".$goods_id."' and goods_attr_id_list='$$'";
            $attr_id=$GLOBALS['db']->getOne($sql);
            if(empty($attr_id))
            {
                $sql="insert into ".$GLOBALS['ecs']->table('erp_goods_attr')." set goods_attr_id_list='$$', goods_id='".$goods_id."'";
                if(!$GLOBALS['db']->query($sql))
                {
                    return false;
                }
            }
        }
    }
    $sql="select attr_id,goods_attr_id_list from ".$GLOBALS['ecs']->table('erp_goods_attr')." where goods_id='".$goods_id."'";
    $res=$GLOBALS['db']->query($sql);
    while($row=mysql_fetch_assoc($res))
    {
        $sql="update ".$GLOBALS['ecs']->table('erp_goods_attr')." set attr_id_no_dynamic='".remove_dynamic_attr($row['goods_attr_id_list'])."' where attr_id='".$row['attr_id']."'";
        if(!$GLOBALS['db']->query($sql))
        {
            return false;
        }
    }
    return true;
}
function get_array_combination($array=array())
{
    $count_total=count($array);
    $t=1;
    if($count_total>=2)
    {
        $result=array();
        for($i=0;$i<=$count_total-1;$i++)
        {
            $count[$i]=count($array[$i]);
        }
        foreach($count as $c)
        {
            $t=$t*$c;
        }
        for($j=1;$j<=$t;$j++)
        {
            for($i=0;$i<=$count_total-1;$i++)
            {
                $pos=rand(0,$count[$i]-1);
                $item[$i]=$array[$i][$pos];
            }
            $tmp='';
            for($i=0;$i<=$count_total-1;$i++)
            {
                if($i!=$count_total-1)
                {
                    $tmp.=$item[$i].',';
                }
                else
                {
                    $tmp.=$item[$i];
                }
            }
            while(in_array($tmp,$result))
            {
                for($i=0;$i<=$count_total-1;$i++)
                {
                    $pos=rand(0,$count[$i]-1);
                    $item[$i]=$array[$i][$pos];
                }
                $tmp='';
                for($i=0;$i<=$count_total-1;$i++)
                {
                    if($i!=$count_total-1)
                    {
                        $tmp.=$item[$i].',';
                    }
                    else
                    {
                        $tmp.=$item[$i];
                    }
                }
            }
            array_push($result,$tmp);
        }
        return $result;
    }
    else
    {
        return $array[0];
    }
}
function delete_attr($goods_id,$goods_attr_id)
{
    $sql="select * from ".$GLOBALS['ecs']->table('erp_goods_attr')." where goods_id='".$goods_id."'";
    $attr_infos=$GLOBALS['db']->getAll($sql);
    if(!attr_has_qty($goods_id,$goods_attr_id))
    {
        if(!empty($attr_infos))
        {
            foreach($attr_infos as $key=>$item)
            {
                $attr_id=$item['attr_id'];
                $goods_attr_id_list=$item['goods_attr_id_list'];
                $goods_attr_id_array=explode(',',$goods_attr_id_list);
                if(in_array($goods_attr_id,$goods_attr_id_array))
                {
                    $sql="delete from ".$GLOBALS['ecs']->table('erp_goods_attr')." where attr_id='".$attr_id."' and barcode_id='0' limit 1";
                    $GLOBALS['db']->query($sql);
                }
            }
        }
    }
}
function attr_has_qty($goods_id,$goods_attr_id)
{
    $has_qty=false;
    $sql="select * from ".$GLOBALS['ecs']->table('erp_goods_attr')." where goods_id='".$goods_id."'";
    $attr_infos=$GLOBALS['db']->getAll($sql);
    if(!empty($attr_infos))
    {
        foreach($attr_infos as $key=>$item)
        {
            $goods_attr_id_list=$item['goods_attr_id_list'];
            $goods_qty=$item['goods_qty'];
            $goods_attr_id_array=explode(',',$goods_attr_id_list);
            if(in_array($goods_attr_id,$goods_attr_id_array) &&$goods_qty>0)
            {
                $has_qty=true;
                break;
            }
        }
    }
    return $has_qty;
}
function is_attr_exists($goods_id,$goods_attr_id_list)
{
    $exists=false;
    $goods_attr_id_list=explode(',',$goods_attr_id_list);
    $sql="select goods_attr_id_list from ".$GLOBALS['ecs']->table('erp_goods_attr')." where goods_id='".$goods_id."'";
    $attrs=$GLOBALS['db']->getAll($sql);
    if(!empty($attrs))
    {
        foreach($attrs as $key =>$item)
        {
            $attr_id_list=explode(',',$item['goods_attr_id_list']);
            $tmp1=array_diff($goods_attr_id_list,$attr_id_list);
            $tmp2=array_diff($attr_id_list,$goods_attr_id_list);
            if(empty($tmp1) &&empty($tmp2))
            {
                $exists=true;
            }
        }
    }
    return $exists;
}
function insert_attr_item($goods_id,$goods_attr_id_list)
{
    $sql="insert into ".$GLOBALS['ecs']->table('erp_goods_attr')." set goods_id='".$goods_id."', goods_attr_id_list='".sort_attr($goods_attr_id_list)."',goods_qty=0";
    return $GLOBALS['db']->query($sql);
}
function delete_attr_item($goods_id,$goods_attr_id_list)
{
    $goods_attr_id_list=explode(',',$goods_attr_id_list);
    $sql="select attr_id,goods_id,goods_qty,goods_attr_id_list from ".$GLOBALS['ecs']->table('erp_goods_attr')." where goods_id='".$goods_id."'";
    $attrs=$GLOBALS['db']->getAll($sql);
    if(!empty($attrs))
    {
        foreach($attrs as $key =>$item)
        {
            $attr_id=$item['attr_id'];
            $attr_id_list=explode(',',$item['goods_attr_id_list']);
            $tmp1=array_diff($goods_attr_id_list,$attr_id_list);
            $tmp2=array_diff($attr_id_list,$goods_attr_id_list);
            if(empty($tmp1) &&empty($tmp2))
            {
                $sql="delete from ".$GLOBALS['ecs']->table('erp_goods_attr')."  where  attr_id='".$attr_id."' limit 1";
                $GLOBALS['db']->query($sql);
                break;
            }
        }
    }
}
function set_attr_qty($goods_id,$goods_attr_id_list,$goods_qty,$act='update')
{
    if(!is_array($goods_attr_id_list))
    {
        $goods_attr_id_list=explode(',',$goods_attr_id_list);
    }
    $sql="select attr_id,goods_id,goods_qty,goods_attr_id_list from ".$GLOBALS['ecs']->table('erp_goods_attr')." where goods_id='".$goods_id."'";
    $attrs=$GLOBALS['db']->getAll($sql);
    if(!empty($attrs))
    {
        foreach($attrs as $key =>$item)
        {
            $attr_id=$item['attr_id'];
            $attr_id_list=explode(',',$item['goods_attr_id_list']);
            $tmp1=array_diff($goods_attr_id_list,$attr_id_list);
            $tmp2=array_diff($attr_id_list,$goods_attr_id_list);
            if(empty($tmp1) &&empty($tmp2))
            {
                if($act=='update')
                {
                    $sql="update ".$GLOBALS['ecs']->table('erp_goods_attr')." set goods_qty='".$goods_qty."' where  attr_id='".$attr_id."'";
                }
                elseif($act=='add')
                {
                    $sql="update ".$GLOBALS['ecs']->table('erp_goods_attr')." set goods_qty=goods_qty+'".$goods_qty."' where  attr_id='".$attr_id."'";
                }
                $GLOBALS['db']->query($sql);
                break;
            }
        }
    }
}
if(!function_exists('get_attr_id'))
{
    function get_attr_id($goods_id,$goods_attr_id_list='')
    {
        $attr_id=0;
        if(empty($goods_attr_id_list))
        {
            $sql="select attr_id from ".$GLOBALS['ecs']->table('erp_goods_attr')." where goods_id='".$goods_id."' and goods_attr_id_list='$$'";
            return $GLOBALS['db']->getOne($sql);
        }
        else
        {
            if(!is_array($goods_attr_id_list))
            {
                $goods_attr_id_list=explode(',',$goods_attr_id_list);
            }
            $sql="select attr_id,goods_attr_id_list from ".$GLOBALS['ecs']->table('erp_goods_attr')." where goods_id='".$goods_id."'";
            $attrs=$GLOBALS['db']->getAll($sql);
            if(!empty($attrs))
            {
                foreach($attrs as $key =>$item)
                {
                    $attr_id=$item['attr_id'];
                    $attr_id_list=explode(',',$item['goods_attr_id_list']);
                    $tmp1=array_diff($goods_attr_id_list,$attr_id_list);
                    $tmp2=array_diff($attr_id_list,$goods_attr_id_list);
                    if(empty($tmp1) &&empty($tmp2))
                    {
                        return $attr_id;
                    }
                }
            }
        }
    }
}
function get_attr_qty($goods_id,$goods_attr_id_list)
{
    $goods_qty=0;
    if(!is_array($goods_attr_id_list))
    {
        $goods_attr_id_list=explode(',',$goods_attr_id_list);
    }
    $sql="select goods_qty,goods_attr_id_list from ".$GLOBALS['ecs']->table('erp_goods_attr')." where goods_id='".$goods_id."'";
    $attrs=$GLOBALS['db']->getAll($sql);
    if(!empty($attrs))
    {
        foreach($attrs as $key =>$item)
        {
            $goods_qty=$item['goods_qty'];
            $attr_id_list=explode(',',$item['goods_attr_id_list']);
            $tmp1=array_diff($goods_attr_id_list,$attr_id_list);
            $tmp2=array_diff($attr_id_list,$goods_attr_id_list);
            if(empty($tmp1) &&empty($tmp2))
            {
                return $goods_qty;
            }
        }
    }
}
function get_attr_info($attr_id)
{
    $sql="select goods_id,goods_attr_id_list,goods_qty from ".$GLOBALS['ecs']->table('erp_goods_attr')." where attr_id='".$attr_id."'";
    $attr_info=$GLOBALS['db']->getRow($sql);
    if(!empty($attr_info))
    {
        if($attr_info['goods_attr_id_list']!='$$')
        {
            $goods_attr_id_list=explode(',',$attr_info['goods_attr_id_list']);
            sort($goods_attr_id_list);
            foreach($goods_attr_id_list as $key =>$item)
            {
                $goods_attr_id=$item;
                $sql="select a.attr_id,a.attr_name,a.is_dynamic,ga.goods_attr_id,ga.attr_value from ".$GLOBALS['ecs']->table('goods_attr')." as ga,";
                $sql.=$GLOBALS['ecs']->table('attribute')." as a where ";
                $sql.="ga.goods_attr_id='".$goods_attr_id."' and ga.attr_id=a.attr_id";
                $info=$GLOBALS['db']->getRow($sql);
                if(!empty($info))
                {
                    $result['attr_info'][]=$info;
                }
            }
        }
        else
        {
            $result['attr_info']='';
        }
        $result['goods_id']=$attr_info['goods_id'];
        $result['goods_qty']=$attr_info['goods_qty'];
        return $result;
    }
    else
    {
        return array();
    }
}
function get_attr_info_by_attr_ids($goods_attr_id_list)
{
    $result=array();
    if($goods_attr_id_list!='$$')
    {
        $goods_attr_id_list=explode(',',$goods_attr_id_list);
        sort($goods_attr_id_list);
        foreach($goods_attr_id_list as $key =>$goods_attr_id)
        {
            $sql="select a.attr_id,a.attr_name,a.is_dynamic,ga.goods_attr_id,ga.attr_value from ".$GLOBALS['ecs']->table('goods_attr')." as ga,";
            $sql.=$GLOBALS['ecs']->table('attribute')." as a where ";
            $sql.="ga.goods_attr_id='".$goods_attr_id."' and ga.attr_id=a.attr_id";
            $info=$GLOBALS['db']->getRow($sql);
            if(!empty($info))
            {
                $result[]=$info;
            }
        }
    }
    return $result;
}
function sort_attr($attr_id_list)
{
    if($attr_id_list!='$$')
    {
        $attr_id_list=explode(',',$attr_id_list);
        sort($attr_id_list);
        return implode(',',$attr_id_list);
    }
    else
    {
        return '$$';
    }
}
function remove_dynamic_attr($attr_id_list)
{
    if($attr_id_list!='$$')
    {
        $attr_id_list=explode(',',$attr_id_list);
        sort($attr_id_list);
        foreach($attr_id_list as $key =>$attr_id)
        {
            $sql="select a.is_dynamic from ".$GLOBALS['ecs']->table('goods_attr')." as ga ";
            $sql.=" join ".$GLOBALS['ecs']->table('attribute')." as a ";
            $sql.=" on ga.attr_id=a.attr_id and a.attr_type='1' ";
            $sql.=" where ga.goods_attr_id='".$attr_id."'";
            $is_dynamic=$GLOBALS['db']->getOne($sql);
            if($is_dynamic==1)
            {
                unset($attr_id_list[$key]);
            }
        }
        if(count($attr_id_list) >0)
        {
            return implode(',',$attr_id_list);
        }
        else
        {
            return '0';
        }
    }
    else
    {
        return '0';
    }
}
?>
