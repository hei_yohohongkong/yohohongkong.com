<?php

/**
 * ECSHOP 管理中心公用文件
 * ============================================================================
 * 版权所有 2005-2010 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liuhui $
 * $Id: init.php 17063 2010-03-25 06:35:46Z liuhui $
*/

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

define('ECS_ADMIN', true);

if (extension_loaded('newrelic'))
{
    newrelic_disable_autorum();
}

define('FORCE_HTTPS', true);

error_reporting(E_ALL);

if (__FILE__ == '')
{
    die('Fatal error code: 0');
}

/* 初始化设置 */
@ini_set('memory_limit',          '64M');
@ini_set('session.cache_expire',  180);
@ini_set('session.use_trans_sid', 0);
@ini_set('session.use_cookies',   1);
@ini_set('session.auto_start',    0);
@ini_set('display_errors',        1);

if (DIRECTORY_SEPARATOR == '\\')
{
    @ini_set('include_path',      '.;' . ROOT_PATH);
}
else
{
    @ini_set('include_path',      '.:' . ROOT_PATH);
}

if (file_exists('../data/config.php'))
{
    include('../data/config.php');
}
else
{
    include('../includes/config.php');
}


/* 取得当前ecshop所在的根目录 */
if(!defined('ADMIN_PATH'))
{
    define('ADMIN_PATH','admin');
}
define('ROOT_PATH', str_replace(ADMIN_PATH . '/includes/init.php', '', str_replace('\\', '/', __FILE__)));

/*
require(ROOT_PATH . 'includes/cls_combat_mysql_injection.php');
$cls_combat_injection=new cls_combat_mysql_injection();
if($cls_combat_injection->check_vars())
{
	die('Hacking attempt');
}
*/

if (defined('DEBUG_MODE') == false)
{
    define('DEBUG_MODE', 0);
}

if (PHP_VERSION >= '5.1' && !empty($timezone))
{
    date_default_timezone_set($timezone);
}

if (isset($_SERVER['PHP_SELF']))
{
    define('PHP_SELF', $_SERVER['PHP_SELF']);
}
else
{
    define('PHP_SELF', $_SERVER['SCRIPT_NAME']);
}

/* PSR-4 class autoloading */
spl_autoload_register(function ($class) {
    $prefix = 'Yoho\\cms\\';
    $base_dir = ROOT_PATH . 'x/includes/';
    $len = strlen($prefix);
    if (strncmp($prefix, $class, $len) !== 0) {
        return;
    }

    $relative_class = substr($class, $len);
    $file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';
    if (file_exists($file)) {
        require $file;
    }
});

require(ROOT_PATH . 'core/lib_https.php');
require(ROOT_PATH . 'includes/inc_constant.php');
require(ROOT_PATH . 'includes/cls_ecshop.php');
require(ROOT_PATH . 'includes/cls_error.php');
require(ROOT_PATH . 'includes/lib_time.php');
require(ROOT_PATH . 'includes/lib_base.php');
require(ROOT_PATH . 'includes/lib_common.php');
require(ROOT_PATH . 'includes/lib_i18n.php');
require(ROOT_PATH . ADMIN_PATH . '/includes/lib_main.php');
require(ROOT_PATH . ADMIN_PATH . '/includes/cls_exchange.php');

/* 对用户传入的变量进行转义操作。*/
if (!get_magic_quotes_gpc())
{
    if (!empty($_GET))
    {
        $_GET  = addslashes_deep($_GET);
    }
    if (!empty($_POST))
    {
        $_POST = addslashes_deep($_POST);
    }

    $_COOKIE   = addslashes_deep($_COOKIE);
    $_REQUEST  = addslashes_deep($_REQUEST);
}

/* 对路径进行安全处理 */
if (strpos(PHP_SELF, '.php/') !== false)
{
    ecs_header("Location:" . substr(PHP_SELF, 0, strpos(PHP_SELF, '.php/') + 4) . "\n");
    exit();
}

/* 创建 ECSHOP 对象 */
$ecs = new ECS($db_name, $prefix);
define('DATA_DIR', $ecs->data_dir());
define('IMAGE_DIR', $ecs->image_dir());

/* 初始化数据库类 */
require(ROOT_PATH . 'includes/cls_mysql.php');
$db = new cls_mysql($db_host, $db_user, $db_pass, $db_name);
$db_host = $db_user = $db_pass = $db_name = NULL;

/* 创建错误处理对象 */
$err = new ecs_error('message.htm');

/* 初始化session */
require(ROOT_PATH . 'includes/cls_session.php');
$sess = new cls_session($db, $ecs->table('sessions'), $ecs->table('sessions_data'), 'ECSCP_ID');

/* 初始化 action */
if (!isset($_REQUEST['act']))
{
    $_REQUEST['act'] = '';
}
elseif (($_REQUEST['act'] == 'login' || $_REQUEST['act'] == 'logout' || $_REQUEST['act'] == 'signin') &&
    strpos(PHP_SELF, '/privilege.php') === false)
{
    $_REQUEST['act'] = '';
}
elseif (($_REQUEST['act'] == 'forget_pwd' || $_REQUEST['act'] == 'reset_pwd' || $_REQUEST['act'] == 'get_pwd') &&
    strpos(PHP_SELF, '/get_password.php') === false)
{
    $_REQUEST['act'] = '';
}

/* 载入系统参数 */
$_CFG = load_config();

// TODO : 登录部分准备拿出去做，到时候把以下操作一起挪过去
if ($_REQUEST['act'] == 'captcha')
{
    include_once(ROOT_PATH . 'includes/cls_captcha.php');

    $img = new captcha('../data/captcha/');
    @ob_end_clean(); //清除之前出现的多余输入
    $img->generate_image();

    exit;
}

$_CFG['lang'] = "zh_tw";
require(ROOT_PATH . 'languages/' .$_CFG['lang']. '/admin/common.php');
require(ROOT_PATH . 'languages/' .$_CFG['lang']. '/admin/log_action.php');
require(ROOT_PATH . 'languages/' .$_CFG['lang']. '/admin/ERP.php');

if (file_exists(ROOT_PATH . 'languages/' . $_CFG['lang'] . '/admin/' . basename(PHP_SELF)))
{
    include(ROOT_PATH . 'languages/' . $_CFG['lang'] . '/admin/' . basename(PHP_SELF));
}

if (!file_exists('../temp/caches'))
{
    @mkdir('../temp/caches', 0777);
    @chmod('../temp/caches', 0777);
}

if (!file_exists('../temp/compiled/admin'))
{
    @mkdir('../temp/compiled/admin', 0777);
    @chmod('../temp/compiled/admin', 0777);
}

clearstatcache();

/* 如果有新版本，升级 */
if (!isset($_CFG['ecs_version']))
{
    $_CFG['ecs_version'] = 'v2.0.5';
}

if (preg_replace('/(?:\.|\s+)[a-z]*$/i', '', $_CFG['ecs_version']) != preg_replace('/(?:\.|\s+)[a-z]*$/i', '', VERSION)
        && file_exists('../upgrade/index.php'))
{
    // 转到升级文件
    ecs_header("Location: ../upgrade/index.php\n");

    exit;
}

/* 创建 Smarty 对象。*/
require(ROOT_PATH . 'core/cls_template.php');
$smarty = new cls_template;

$smarty->template_dir  = ROOT_PATH . ADMIN_PATH . '/templates';
$smarty->compile_dir   = ROOT_PATH . 'temp/compiled/admin';
if ((DEBUG_MODE & 2) == 2)
{
    $smarty->force_compile = true;
}


$smarty->assign('lang', $_LANG);
$smarty->assign('help_open', $_CFG['help_open']);

if(isset($_CFG['enable_order_check']))  // 为了从旧版本顺利升级到2.5.0
{
    $smarty->assign('enable_order_check', $_CFG['enable_order_check']);
}
else
{
    $smarty->assign('enable_order_check', 0);
}

/* 验证管理员身份 */
if ((!isset($_SESSION['admin_id']) || intval($_SESSION['admin_id']) <= 0) &&
    $_REQUEST['act'] != 'login' && $_REQUEST['act'] != 'signin' &&
    $_REQUEST['act'] != 'forget_pwd' && $_REQUEST['act'] != 'reset_pwd' && $_REQUEST['act'] != 'check_order')
{
    /* session 不存在，检查cookie */
    if (!empty($_COOKIE['ECSCP']['admin_id']) && !empty($_COOKIE['ECSCP']['admin_pass']))
    {
        // 找到了cookie, 验证cookie信息
        $sql = 'SELECT user_id, user_name, password, action_list, last_login, role_id ' .
                ' FROM ' .$ecs->table('admin_user') .
                " WHERE user_id = '" . intval($_COOKIE['ECSCP']['admin_id']) . "'";
        $row = $db->GetRow($sql);

        if (!$row)
        {
            // 没有找到这个记录
            setcookie('ECSCP[admin_id]',   '', 1, '/');
            setcookie('ECSCP[admin_pass]', '', 1, '/');

            if (!empty($_REQUEST['is_ajax']))
            {
                make_json_error($_LANG['priv_error']);
            }
            else
            {
                ecs_header("Location: privilege.php?act=login\n");
            }

            exit;
        }
        else
        {
            // 检查密码是否正确
            if (md5($row['password'] . $_CFG['hash_code']) == $_COOKIE['ECSCP']['admin_pass'])
            {
                !isset($row['last_time']) && $row['last_time'] = '';
                set_admin_session($row['user_id'], $row['user_name'], $row['action_list'], $row['last_time'], $row['role_id']);

                // 更新最后登录时间和IP
                $db->query('UPDATE ' . $ecs->table('admin_user') .
                            " SET last_login = '" . gmtime() . "', last_ip = '" . real_ip() . "'" .
                            " WHERE user_id = '" . $_SESSION['admin_id'] . "'");
                // If admin login, we add ip into whitelist
                $db->query("INSERT INTO ".$ecs->table( 'whitelist')." (ip, remark) VALUES ('".real_ip()."', 'Admin login on CMS') ON DUPLICATE KEY UPDATE ip = '".real_ip()."' ", 'SILENT');
            }
            else
            {
                setcookie('ECSCP[admin_id]',   '', 1, '/');
                setcookie('ECSCP[admin_pass]', '', 1, '/');

                if (!empty($_REQUEST['is_ajax']))
                {
                    make_json_error($_LANG['priv_error']);
                }
                else
                {
                    ecs_header("Location: privilege.php?act=login\n");
                }

                exit;
            }
        }
    }
    else
    {
        if (!empty($_REQUEST['is_ajax']))
        {
            make_json_error($_LANG['priv_error']);
        }
        else
        {
            ecs_header("Location: privilege.php?act=login\n");
        }

        exit;
    }
}

if ($_REQUEST['act'] != 'login' && $_REQUEST['act'] != 'signin' &&
    $_REQUEST['act'] != 'forget_pwd' && $_REQUEST['act'] != 'reset_pwd' && $_REQUEST['act'] != 'check_order')
{
    $admin_path = preg_replace('/:\d+/', '', $ecs->url()) . ADMIN_PATH;
    // howang: Also allow refer from POS pages
    $pos_path = preg_replace('/:\d+/', '', $ecs->url()) . 'pos';
    $referer_path = empty($_SERVER['HTTP_REFERER']) ? $admin_path : preg_replace('/:\d+/', '', $_SERVER['HTTP_REFERER']);
    // if (!empty($_SERVER['HTTP_REFERER']) &&
    //     strpos(preg_replace('/:\d+/', '', $_SERVER['HTTP_REFERER']), $admin_path) === false)
    if (strpos($referer_path, $admin_path) === false && strpos($referer_path, $pos_path) === false)
    {
        if (!empty($_REQUEST['is_ajax']))
        {
            make_json_error($_LANG['priv_error']);
        }
        else
        {
            ecs_header("Location: privilege.php?act=login\n");
        }

        exit;
    }
}

// admin, howang, franz and kathy can manage 成本價
// 2017April, removing hard code, insert new 'admin_action'
if ((!empty($_SESSION['admin_name'])) && (in_array($_SESSION['admin_name'], array('admin','howang','franz','kathy')) || check_authz(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_SUPERADMIN)))
{
    $_SESSION['manage_cost'] = 1;
    
    /* 管理员登录后可在任何页面使用 act=phpinfo 显示 phpinfo() 信息 */
    if ($_REQUEST['act'] == 'phpinfo' && function_exists('phpinfo'))
    {
        phpinfo();
        exit;
    }
}
else
{
    if (isset($_SESSION['manage_cost']))
    {
        $_SESSION['manage_cost'] = 0;
        unset($_SESSION['manage_cost']);
    }
}
// Accountant permission
$accountant = $db->getCol("SELECT user_name FROM " . $ecs->table('admin_user') . " WHERE role_id " . db_create_in(Yoho\cms\Controller\AdminuserController::ACCOUNTING_TEAM_ROLE_IDS));/* Role: Accounting Team */
if ((!empty($_SESSION['admin_name'])) && ((in_array($_SESSION['admin_name'], $accountant))))
{
    $_SESSION['is_accountant'] = 1;
}
else
{
    if (isset($_SESSION['is_accountant']))
    {
        $_SESSION['is_accountant'] = 0;
        unset($_SESSION['is_accountant']);
    }
}

//header('Cache-control: private');
header('content-type: text/html; charset=' . EC_CHARSET);
header('Expires: Fri, 14 Mar 1980 20:53:00 GMT');
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: no-cache, must-revalidate');
header('Pragma: no-cache');

if ((DEBUG_MODE & 1) == 1)
{
    error_reporting(E_ALL);
}
else
{
    error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING & ~E_STRICT & ~E_DEPRECATED);
}
if ((DEBUG_MODE & 4) == 4)
{
    include(ROOT_PATH . 'includes/lib.debug.php');
}

/* 判断是否支持gzip模式 */
if (gzip_enabled())
{
    ob_start('ob_gzhandler');
}
else
{
    ob_start();
}

/* Init topbar & menu */
// require_once(ROOT_PATH . '/includes/lib_order.php');
require_once(ROOT_PATH . '/includes/lib_insert.php');
$orderController = new Yoho\cms\Controller\OrderController();
$taskController = new Yoho\cms\Controller\TaskController();
$erpController = new Yoho\cms\Controller\ErpController();
$userController = new Yoho\cms\Controller\UserController();
$smarty->assign('shop_url', urlencode($ecs->url()));
include_once('includes/inc_menu.php');

// 权限对照表
include_once('includes/inc_priv.php');

foreach ($modules AS $key => $value)
{
    if (empty($sort[$key])) {
        ksort($modules[$key]);
    } else {
        $temp_sort = $sort[$key];
        uksort($modules[$key], function ($a, $b) use ($temp_sort) {
            return array_search($a, $temp_sort) !== false && array_search($b, $temp_sort) === false
                ? -1
                : (array_search($a, $temp_sort) === false && array_search($b, $temp_sort) !== false
                    ? 1
                    : (array_search($a, $temp_sort) > array_search($b, $temp_sort)
                        ? 1
                        : -1
                    )
                );
        });
    }
}
uksort($modules, function ($a, $b) use ($ssort) {
    return array_search($a, $ssort) !== false && array_search($b, $ssort) === false
    ? -1
    : (array_search($a, $ssort) === false && array_search($b, $ssort) !== false
        ? 1
        : (array_search($a, $ssort) > array_search($b, $ssort)
            ? 1
            : -1
        )
    );
});

foreach ($modules AS $key => $val)
{
    $menus[$key]['label'] = $_LANG[$key];
    if (is_array($val))
    {
        foreach ($val AS $k => $v)
        {
            /*
            if ( isset($purview[$k]))
            {
                if (is_array($purview[$k]))
                {
                    $boole = false;
                    foreach ($purview[$k] as $action)
                    {
                            $boole = $boole || admin_priv($action, '', false);
                    }
                    if (!$boole)
                    {
                        continue;
                    }

                }
                else
                {
                    if (! admin_priv($purview[$k], '', false))
                    {
                        continue;
                    }
                }
            }
            */
            if (! admin_priv('menu_' . $k, '', false))
            {
                continue;
            }
            if ($k == 'ucenter_setup' && $_CFG['integrate_code'] != 'ucenter')
            {
                continue;
            }
            $menus[$key]['children'][$k]['label']  = $_LANG[$k];
            $menus[$key]['children'][$k]['action'] = $v;
        }
    }
    else
    {
        $menus[$key]['action'] = $val;
    }

    // 如果children的子元素长度为0则删除该组
    if(empty($menus[$key]['children']))
    {
        unset($menus[$key]);
    }

}
unset($modules);
/* Top bar */
// 获得管理员设置的菜单
$lst = array();
$nav = $db->GetOne('SELECT nav_list FROM ' . $ecs->table('admin_user') . " WHERE user_id = '" . $_SESSION['admin_id'] . "'");
$TaskController = new Yoho\cms\Controller\TaskController();
if($TaskController->getCacheUnreadCount($_SESSION['admin_id'])>0){
    $smarty->assign('topmsg',['url'=>'tasklist.php?act=list','text'=>'您有 <b>'.$TaskController->getCacheUnreadCount($_SESSION['admin_id']).'</b> 個更新的工作紀錄']);
}

if (!empty($nav))
{
    $arr = explode(',', $nav);

    foreach ($arr AS $val)
    {
        $tmp = explode('|', $val);
        $lst[$tmp[1]] = $tmp[0];
    }
}

// 获得管理员设置的菜单

// 获得管理员ID
$smarty->assign('send_mail_on',$_CFG['send_mail_on']);
$smarty->assign('nav_list', $lst);
$smarty->assign('admin_id', $_SESSION['admin_id']);
$smarty->assign('admin_name', $_SESSION['admin_name']);
$smarty->assign('session_manage_cost', $_SESSION['manage_cost']);
$smarty->assign('session_is_accountant', $_SESSION['is_accountant']);
$smarty->assign('certi', $_CFG['certi']);
$adminUser = new Yoho\cms\Model\Adminuser($_SESSION['admin_id']);
$adminUser = $adminUser->data;
$smarty->assign('admin', $adminUser);
$smarty->assign('is_beta', (strpos($_SERVER['SERVER_NAME'], 'beta.yohohongkong.com') !== false));
$smarty->assign('menus',     $menus);
$smarty->assign('no_help',   $_LANG['no_help']);
$smarty->assign('help_lang', $_CFG['lang']);
$smarty->assign('charset', EC_CHARSET);
$smarty->assign('edit_perma_link', check_authz('edit_perma_link'));
/* 工作清單 - 未完成的工作 */
$sql = 'SELECT a.*, b.user_name, ' .
"IFNULL((SELECT s.user_id FROM " . $ecs->table('salesperson') . " as s WHERE s.sales_name = a.pic), 0) as pic_user_id, ".
"(SELECT avatar  FROM " . $GLOBALS['ecs']->table('admin_user') . " WHERE user_id = a.sender_id) as avatar ".
'FROM ' . $ecs->table('admin_taskes') . ' AS a, ' . $ecs->table('admin_user') . ' AS b ' .
"WHERE a.sender_id = b.user_id AND a.receiver_id = '$_SESSION[admin_id]' AND ".
"a.status IN (".$taskController::STATUS_NEW.','.$taskController::STATUS_INPROGRESS.") AND deleted = 0 ORDER BY a.sent_time DESC";
$unfinished_taskes = $db->GetAll($sql);

foreach ($unfinished_taskes as $key => $task)
{
    $unfinished_taskes[$key]['send_date'] = local_date('d M H:i', $task['sent_time']);
    $unfinished_taskes[$key]['avatar']    = !empty($task['avatar']) ? '../'.$task['avatar'] : NULL;
    $unfinished_taskes[$key]['message']    = strip_tags($task['message']);
}
$smarty->assign('unfinished_taskes', $unfinished_taskes);
$smarty->assign('unfinished_taskes_count', count($unfinished_taskes));

if($_REQUEST['ajax_global_task']) {

    /* 工作清單 - 未完成的工作 */
    $sql = 'SELECT a.*, b.user_name, ' .
    "IFNULL((SELECT s.user_id FROM " . $ecs->table('salesperson') . " as s WHERE s.sales_name = a.pic), 0) as pic_user_id, ".
    "(SELECT avatar  FROM " . $GLOBALS['ecs']->table('admin_user') . " WHERE user_id = a.sender_id) as avatar ".
    'FROM ' . $ecs->table('admin_taskes') . ' AS a, ' . $ecs->table('admin_user') . ' AS b ' .
    "WHERE a.sender_id = b.user_id AND a.receiver_id = '$_SESSION[admin_id]' AND ".
    "a.status IN (".$taskController::STATUS_NEW.','.$taskController::STATUS_INPROGRESS.") AND deleted = 0 ORDER BY a.sent_time DESC";
    $unfinished_taskes = $db->GetAll($sql);

    foreach ($unfinished_taskes as $key => $task)
    {
        $unfinished_taskes[$key]['send_date'] = local_date($_CFG['time_format'], $task['sent_time']);
        $unfinished_taskes[$key]['avatar']    = !empty($task['avatar']) ? '../'.$task['avatar'] : NULL;
        $unfinished_taskes[$key]['message']    = strip_tags($task['message']);
    }
    $result = ['unfinished_taskes'=>$unfinished_taskes];
    $topmsg = [];
    if($TaskController->getCacheUnreadCount($_SESSION['admin_id'])>0){
        $topmsg =  ['url'=>'tasklist.php?act=list','text'=>'您有 <b>'.$TaskController->getCacheUnreadCount($_SESSION['admin_id']).'</b> 個更新的工作紀錄'];
        $result['topmsg'] = $topmsg;
    }

    make_json_result('','',$result);
}
/* End */
/*
$sql = "SELECT user_id, user_name, action_list, last_login".
        " FROM " . $GLOBALS['ecs']->table('admin_user') .
        " WHERE user_id = '" . $_SESSION['admin_id']. "'";
//die($sql);
$row = $GLOBALS['db']->getRow($sql);
set_admin_session($row['user_id'], $row['user_name'], $row['action_list'], $row['last_login']);
*/

?>
