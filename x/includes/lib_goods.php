<?php

/**
 * ECSHOP 管理中心商品相关函数
 * ============================================================================
 * 版权所有 2005-2010 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liuhui $
 * $Id: lib_goods.php 17063 2010-03-25 06:35:46Z liuhui $
 */

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}
if(!$erpController)
    $erpController = new Yoho\cms\Controller\ErpController();

if(!$goodsController)
    $goodsController = new Yoho\cms\Controller\GoodsController();
/**
 * 取得推荐类型列表
 * @return  array   推荐类型列表
 */
function get_intro_list()
{
    return array(
        'is_best'    => $GLOBALS['_LANG']['is_best'],
        'is_new'     => $GLOBALS['_LANG']['is_new'],
        'is_hot'     => $GLOBALS['_LANG']['is_hot'],
        'is_promote' => $GLOBALS['_LANG']['is_promote'],
        'all_type' => $GLOBALS['_LANG']['all_type'],
    );
}

/**
 * 取得重量单位列表
 * @return  array   重量单位列表
 */
function get_unit_list()
{
    return array(
        '1'     => $GLOBALS['_LANG']['unit_kg'],
        '0.001' => $GLOBALS['_LANG']['unit_g'],
    );
}

/**
 * 取得会员等级列表
 * @return  array   会员等级列表
 */
function get_user_rank_list()
{
    $sql = "SELECT * FROM " . $GLOBALS['ecs']->table('user_rank') .
           " ORDER BY min_points";

    return $GLOBALS['db']->getAll($sql);
}

/**
 * 取得某商品的会员价格列表
 * @param   int     $goods_id   商品编号
 * @return  array   会员价格列表 user_rank => user_price
 */
function get_member_price_list($goods_id)
{
    /* 取得会员价格 */
    $price_list = array();
    $sql = "SELECT user_rank, user_price FROM " .
           $GLOBALS['ecs']->table('member_price') .
           " WHERE goods_id = '$goods_id'";
    $res = $GLOBALS['db']->query($sql);
    while ($row = $GLOBALS['db']->fetchRow($res))
    {
        $price_list[$row['user_rank']] = $row['user_price'];
    }

    return $price_list;
}

/**
 * 插入或更新商品属性
 *
 * @param   int     $goods_id           商品编号
 * @param   array   $id_list            属性编号数组
 * @param   array   $is_spec_list       是否规格数组 'true' | 'false'
 * @param   array   $value_price_list   属性值数组
 * @return  array                       返回受到影响的goods_attr_id数组
 */
function handle_goods_attr($goods_id, $id_list, $is_spec_list, $value_price_list)
{
    $goods_attr_id = array();

    /* 循环处理每个属性 */
    foreach ($id_list AS $key => $id)
    {
        $is_spec = $is_spec_list[$key];
        if ($is_spec == 'false')
        {
            $value = $value_price_list[$key];
            $price = '';
        }
        else
        {
            $value_list = array();
            $price_list = array();
            if ($value_price_list[$key])
            {
                $vp_list = explode(chr(13), $value_price_list[$key]);
                foreach ($vp_list AS $v_p)
                {
                    $arr = explode(chr(9), $v_p);
                    $value_list[] = $arr[0];
                    $price_list[] = $arr[1];
                }
            }
            $value = join(chr(13), $value_list);
            $price = join(chr(13), $price_list);
        }

        // 插入或更新记录
        $sql = "SELECT goods_attr_id FROM " . $GLOBALS['ecs']->table('goods_attr') . " WHERE goods_id = '$goods_id' AND attr_id = '$id' AND attr_value = '$value' LIMIT 0, 1";
        $result_id = $GLOBALS['db']->getOne($sql);
        if (!empty($result_id))
        {
            $sql = "UPDATE " . $GLOBALS['ecs']->table('goods_attr') . "
                    SET attr_value = '$value'
                    WHERE goods_id = '$goods_id'
                    AND attr_id = '$id'
                    AND goods_attr_id = '$result_id'";

            $goods_attr_id[$id] = $result_id;
        }
        else
        {
            $sql = "INSERT INTO " . $GLOBALS['ecs']->table('goods_attr') . " (goods_id, attr_id, attr_value, attr_price) " .
                    "VALUES ('$goods_id', '$id', '$value', '$price')";
        }

        $GLOBALS['db']->query($sql);

        if ($goods_attr_id[$id] == '')
        {
            $goods_attr_id[$id] = $GLOBALS['db']->insert_id();
        }
    }

    return $goods_attr_id;
}

/**
 * 保存某商品的会员价格
 * @param   int     $goods_id   商品编号
 * @param   array   $rank_list  等级列表
 * @param   array   $price_list 价格列表
 * @return  void
 */
function handle_member_price($goods_id, $rank_list, $price_list)
{
    /* 循环处理每个会员等级 */
    foreach ($rank_list AS $key => $rank)
    {
        /* 会员等级对应的价格 */
        $price = $price_list[$key];

        // 插入或更新记录
        $sql = "SELECT COUNT(*) FROM " . $GLOBALS['ecs']->table('member_price') .
               " WHERE goods_id = '$goods_id' AND user_rank = '$rank'";
        if ($GLOBALS['db']->getOne($sql) > 0)
        {
            /* 如果会员价格是小于0则删除原来价格，不是则更新为新的价格 */
            if ($price < 0 || $price == '')
            {
                $sql = "DELETE FROM " . $GLOBALS['ecs']->table('member_price') .
                       " WHERE goods_id = '$goods_id' AND user_rank = '$rank' LIMIT 1";
            }
            else
            {
                $sql = "UPDATE " . $GLOBALS['ecs']->table('member_price') .
                       " SET user_price = '$price' " .
                       "WHERE goods_id = '$goods_id' " .
                       "AND user_rank = '$rank' LIMIT 1";
            }
        }
        else
        {
            if ($price < 0 || $price == "")
            {
                $sql = '';
            }
            else
            {
                $sql = "INSERT INTO " . $GLOBALS['ecs']->table('member_price') . " (goods_id, user_rank, user_price) " .
                       "VALUES ('$goods_id', '$rank', '$price')";
            }
        }

        if ($sql)
        {
            $GLOBALS['db']->query($sql);
        }
    }
}

/**
 * 保存某商品的扩展分类
 * @param   int     $goods_id   商品编号
 * @param   array   $cat_list   分类编号数组
 * @return  void
 */
function handle_other_cat($goods_id, $cat_list)
{
    /* 查询现有的扩展分类 */
    $sql = "SELECT cat_id FROM " . $GLOBALS['ecs']->table('goods_cat') .
            " WHERE goods_id = '$goods_id'";
    $exist_list = $GLOBALS['db']->getCol($sql);

    /* 删除不再有的分类 */
    $delete_list = array_diff($exist_list, $cat_list);
    if ($delete_list)
    {
        $sql = "DELETE FROM " . $GLOBALS['ecs']->table('goods_cat') .
                " WHERE goods_id = '$goods_id' " .
                "AND cat_id " . db_create_in($delete_list);
        $GLOBALS['db']->query($sql);
    }

    /* 添加新加的分类 */
    $add_list = array_diff($cat_list, $exist_list, array(0));
    foreach ($add_list AS $cat_id)
    {
        // 插入记录
        $sql = "INSERT INTO " . $GLOBALS['ecs']->table('goods_cat') .
                " (goods_id, cat_id) " .
                "VALUES ('$goods_id', '$cat_id')";
        $GLOBALS['db']->query($sql);
    }
}

/**
 * 保存某商品的关联商品
 * @param   int     $goods_id
 * @return  void
 */
function handle_link_goods($goods_id)
{
    $sql = "UPDATE " . $GLOBALS['ecs']->table('link_goods') . " SET " .
            " goods_id = '$goods_id' " .
            " WHERE goods_id = '0'" .
            " AND admin_id = '$_SESSION[admin_id]'";
    $GLOBALS['db']->query($sql);

    $sql = "UPDATE " . $GLOBALS['ecs']->table('link_goods') . " SET " .
            " link_goods_id = '$goods_id' " .
            " WHERE link_goods_id = '0'" .
            " AND admin_id = '$_SESSION[admin_id]'";
    $GLOBALS['db']->query($sql);
}

/**
 * 保存某商品的配件
 * @param   int     $goods_id
 * @return  void
 */
function handle_group_goods($goods_id)
{
    $sql = "UPDATE " . $GLOBALS['ecs']->table('group_goods') . " SET " .
            " parent_id = '$goods_id' " .
            " WHERE parent_id = '0'" .
            " AND admin_id = '$_SESSION[admin_id]'";
    $GLOBALS['db']->query($sql);
}

/**
 * 保存某商品的关联文章
 * @param   int     $goods_id
 * @return  void
 */
function handle_goods_article($goods_id)
{
    $sql = "UPDATE " . $GLOBALS['ecs']->table('goods_article') . " SET " .
            " goods_id = '$goods_id' " .
            " WHERE goods_id = '0'" .
            " AND admin_id = '$_SESSION[admin_id]'";
    $GLOBALS['db']->query($sql);
}

/**
 * 保存某商品的自訂欄位
 * @param   int     $goods_id
 * @param   array   $name_arr
 * @param   array   $value_arr
 * @param   array   $url_arr
 * @return  void
 */
function handle_goods_extra_info($goods_id, $name_arr, $value_arr, $url_arr, $newtab_arr)
{
    global $db, $ecs;
    
    $names = array();
    $c = 0;
    for($i = 0; (!empty($name_arr[$i]) && !empty($value_arr[$i])); $i++)
    {
        $name = $name_arr[$i];
        $value = $value_arr[$i];
        $url = $url_arr[$i];
        $newtab = !empty($newtab_arr[$i]) ? '1' : '0';
        
        $sql = "SELECT * FROM " . $ecs->table('goods_extra_info') . "  WHERE `info_name` = '" . $name . "' AND `goods_id` = '" . $goods_id . "'";
        if ($db->getOne($sql)) // Already exists?
        {
            $sql = "UPDATE " . $ecs->table('goods_extra_info') . 
                    " SET `info_value` = '" . $value . "', `info_url` = '" . $url . "', `info_url_newtab` = '" . $newtab . "', `info_order` = '" . $c . "' " .
                    " WHERE `goods_id` = '" . $goods_id . "' AND `info_name` = '" . $name . "'";
        }
        else
        {
            $sql = "INSERT INTO " . $ecs->table('goods_extra_info') .
                    " (`goods_id`, `info_name`, `info_value`, `info_url`, `info_url_newtab`, `info_order`)" .
                    " VALUES ('" . $goods_id . "', '" . $name . "', '" . $value . "', '" . $url . "', '" . $newtab . "', '" . $c . "')";
        }
        $db->query($sql);
        
        $names[] = $name;
        $c++;
    }
    
    // Remove all old values not included in this update
    $sql = "DELETE FROM " . $ecs->table('goods_extra_info') . 
           " WHERE goods_id = '$goods_id' " .
           " AND info_name NOT " . db_create_in($names);
    $db->query($sql);
}

/**
 * 保存某商品的自訂欄位
 * @param   int     $goods_id
 * @param   array   $name_arr
 * @param   array   $value_arr
 * @param   array   $url_arr
 * @return  void
 */
function handle_goods_promo_links($goods_id, $title_arr, $url_arr, $newtab_arr, $img_arr, $desc_arr)
{
    global $db, $ecs;

    $titles = array();
    $c = 0;
    for($i = 0; (!empty($title_arr[$i]) && !empty($url_arr[$i])); $i++)
    {
        $title = $title_arr[$i];
        $url = $url_arr[$i];
        $newtab = !empty($newtab_arr[$i]) ? '1' : '0';
        $img = $img_arr[$i];
        $desc = $desc_arr[$i];

        $sql = "SELECT * FROM " . $ecs->table('goods_promo_links') . "  WHERE `link_title` = '" . $title . "' AND `goods_id` = '" . $goods_id . "'";
        if ($db->getOne($sql)) // Already exists?
        {
            $sql = "UPDATE " . $ecs->table('goods_promo_links') . 
                    " SET `link_url` = '" . $url . "', `link_newtab` = '" . $newtab . "', `link_img` = '" . $img . "', `link_desc` = '" . $desc . "', `link_order` = '" . $c . "' " .
                    " WHERE `goods_id` = '" . $goods_id . "' AND `link_title` = '" . $title . "'";
        }
        else
        {
            $sql = "INSERT INTO " . $ecs->table('goods_promo_links') .
                    " (`goods_id`, `link_title`, `link_url`, `link_newtab`, `link_img`, `link_desc`, `link_order`)" .
                    " VALUES ('" . $goods_id . "', '" . $title . "', '" . $url . "', '" . $newtab . "', '" . $img . "', '" . $desc . "', '" . $c . "')";
        }
        $db->query($sql);

        $titles[] = $title;
        $c++;
    }

    // Remove all old values not included in this update
    $sql = "DELETE FROM " . $ecs->table('goods_promo_links') . 
           " WHERE goods_id = '$goods_id' " .
           " AND link_title NOT " . db_create_in($titles);
    $db->query($sql);
}

/**
 * 保存某商品的相册图片
 * @param   int     $goods_id
 * @param   array   $image_files
 * @param   array   $image_descs
 * @return  void
 */
function handle_gallery_image($goods_id, $image_files, $image_descs, $image_urls)
{
    /* 是否处理缩略图 */
    $proc_thumb = (isset($GLOBALS['shop_id']) && $GLOBALS['shop_id'] > 0)? false : true;
    foreach ($image_descs AS $key => $img_desc)
    {
        /* 是否成功上传 */
        $flag = false;
        if (isset($image_files['error']))
        {
            if ($image_files['error'][$key] == 0)
            {
                $flag = true;
            }
        }
        else
        {
            if ($image_files['tmp_name'][$key] != 'none')
            {
                $flag = true;
            }
        }

        if ($flag)
        {
            // 生成缩略图
            if ($proc_thumb)
            {
                $thumb_url = $GLOBALS['image']->make_thumb($image_files['tmp_name'][$key], $GLOBALS['_CFG']['thumb_width'],  $GLOBALS['_CFG']['thumb_height']);
                $thumb_url = is_string($thumb_url) ? $thumb_url : '';
            }

            $upload = array(
                'name' => $image_files['name'][$key],
                'type' => $image_files['type'][$key],
                'tmp_name' => $image_files['tmp_name'][$key],
                'size' => $image_files['size'][$key],
            );
            if (isset($image_files['error']))
            {
                $upload['error'] = $image_files['error'][$key];
            }
            $img_original = $GLOBALS['image']->upload_image($upload);
            if ($img_original === false)
            {
                sys_msg($GLOBALS['image']->error_msg(), 1, array(), false);
            }
            $img_url = $img_original;

            if (!$proc_thumb)
            {
                $thumb_url = $img_original;
            }
            // 如果服务器支持GD 则添加水印
            if ($proc_thumb && gd_version() > 0)
            {
                $pos        = strpos(basename($img_original), '.');
                $newname    = dirname($img_original) . '/' . $GLOBALS['image']->random_filename() . substr(basename($img_original), $pos);
                copy('../' . $img_original, '../' . $newname);
                $img_url    = $newname;

                $GLOBALS['image']->add_watermark('../'.$img_url,'',$GLOBALS['_CFG']['watermark'], $GLOBALS['_CFG']['watermark_place'], $GLOBALS['_CFG']['watermark_alpha']);
            }

            /* 重新格式化图片名称 */
            $img_original = reformat_image_name('gallery', $goods_id, $img_original, 'source');
            $img_url = reformat_image_name('gallery', $goods_id, $img_url, 'goods');
            $thumb_url = reformat_image_name('gallery_thumb', $goods_id, $thumb_url, 'thumb');
            /* 取得 sort_order */
            $sql = "SELECT IFNULL(MAX(sort_order),-1)+1 FROM " . $GLOBALS['ecs']->table('goods_gallery') . " WHERE goods_id = '$goods_id'";
            $sort_order = $GLOBALS['db']->getOne($sql);
            $sql = "INSERT INTO " . $GLOBALS['ecs']->table('goods_gallery') .
                    " (goods_id, img_url, img_desc, thumb_url, img_original, sort_order) " .
                    "VALUES ('$goods_id', '$img_url', '$img_desc', '$thumb_url', '$img_original', '$sort_order')";
            $GLOBALS['db']->query($sql);
            /* 不保留商品原图的时候删除原图 */
            if ($proc_thumb && !$GLOBALS['_CFG']['retain_original_img'] && !empty($img_original))
            {
                $GLOBALS['db']->query("UPDATE " . $GLOBALS['ecs']->table('goods_gallery') . " SET img_original='' WHERE `goods_id`='{$goods_id}'");
                @unlink('../' . $img_original);
            }
        }
        elseif (!empty($image_urls[$key]) && ($image_urls[$key] != $GLOBALS['_LANG']['img_file']) && ($image_urls[$key] != 'http://') && copy(trim($image_urls[$key]), ROOT_PATH . 'temp/' . basename($image_urls[$key])))
        {
            $image_url = trim($image_urls[$key]);

            //定义原图路径
            $down_img = ROOT_PATH . 'temp/' . basename($image_url);

            // 生成缩略图
            if ($proc_thumb)
            {
                $thumb_url = $GLOBALS['image']->make_thumb($down_img, $GLOBALS['_CFG']['thumb_width'],  $GLOBALS['_CFG']['thumb_height']);
                $thumb_url = is_string($thumb_url) ? $thumb_url : '';
                $thumb_url = reformat_image_name('gallery_thumb', $goods_id, $thumb_url, 'thumb');
            }

            if (!$proc_thumb)
            {
                $thumb_url = htmlspecialchars($image_url);
            }

            /* 重新格式化图片名称 */
            $img_url = $img_original = htmlspecialchars($image_url);
            $sql = "INSERT INTO " . $GLOBALS['ecs']->table('goods_gallery') . " (goods_id, img_url, img_desc, thumb_url, img_original) " .
                    "VALUES ('$goods_id', '$img_url', '$img_desc', '$thumb_url', '$img_original')";
            $GLOBALS['db']->query($sql);

            @unlink($down_img);
        }
    }
}

/**
 * 修改商品某字段值
 * @param   string  $goods_id   商品编号，可以为多个，用 ',' 隔开
 * @param   string  $field      字段名
 * @param   string  $value      字段值
 * @return  bool
 */
function update_goods($goods_id, $field, $value)
{
    if ($goods_id)
    {
        /* 清除缓存 */
        clear_cache_files();

        $sql = "UPDATE " . $GLOBALS['ecs']->table('goods') .
                " SET $field = '$value' , last_update = '". gmtime() ."' " .
                "WHERE goods_id " . db_create_in($goods_id);
        
        // Flush Category Thumb memcached
        $categoryController = new Yoho\cms\Controller\CategoryController();
        $goods_ids = explode(",", $goods_id);
        foreach ($goods_ids as $id) {
            $categoryController->flushCatThumbMemcache(0, $id);
        }
        return $GLOBALS['db']->query($sql);
    }
    else
    {
        return false;
    }
}

/**
 * 从回收站删除多个商品
 * @param   mix     $goods_id   商品id列表：可以逗号格开，也可以是数组
 * @return  void
 */
function delete_goods($goods_id)
{
    if (empty($goods_id))
    {
        return;
    }

    /* 取得有效商品id */
    $sql = "SELECT DISTINCT goods_id FROM " . $GLOBALS['ecs']->table('goods') .
            " WHERE goods_id " . db_create_in($goods_id) . " AND is_delete = 1";
    $goods_id = $GLOBALS['db']->getCol($sql);
    if (empty($goods_id))
    {
        return;
    }

    /* 删除商品图片和轮播图片文件 */
    $sql = "SELECT goods_thumb, goods_img, original_img " .
            "FROM " . $GLOBALS['ecs']->table('goods') .
            " WHERE goods_id " . db_create_in($goods_id);
    $res = $GLOBALS['db']->query($sql);
    while ($goods = $GLOBALS['db']->fetchRow($res))
    {
        if (!empty($goods['goods_thumb']))
        {
            @unlink('../' . $goods['goods_thumb']);
        }
        if (!empty($goods['goods_img']))
        {
            @unlink('../' . $goods['goods_img']);
        }
        if (!empty($goods['original_img']))
        {
            @unlink('../' . $goods['original_img']);
        }
    }

    /* 删除商品 */
    $sql = "DELETE FROM " . $GLOBALS['ecs']->table('goods') .
            " WHERE goods_id " . db_create_in($goods_id);
    $GLOBALS['db']->query($sql);

    /* 删除商品的货品记录 */
    $sql = "DELETE FROM " . $GLOBALS['ecs']->table('products') .
            " WHERE goods_id " . db_create_in($goods_id);
    $GLOBALS['db']->query($sql);

    /* 删除商品相册的图片文件 */
    $sql = "SELECT img_url, thumb_url, img_original " .
            "FROM " . $GLOBALS['ecs']->table('goods_gallery') .
            " WHERE goods_id " . db_create_in($goods_id);
    $res = $GLOBALS['db']->query($sql);
    while ($row = $GLOBALS['db']->fetchRow($res))
    {
        if (!empty($row['img_url']))
        {
            @unlink('../' . $row['img_url']);
        }
        if (!empty($row['thumb_url']))
        {
            @unlink('../' . $row['thumb_url']);
        }
        if (!empty($row['img_original']))
        {
            @unlink('../' . $row['img_original']);
        }
    }

    /* 删除商品相册 */
    $sql = "DELETE FROM " . $GLOBALS['ecs']->table('goods_gallery') . " WHERE goods_id " . db_create_in($goods_id);
    $GLOBALS['db']->query($sql);

    /* 删除相关表记录 */
    $sql = "DELETE FROM " . $GLOBALS['ecs']->table('collect_goods') . " WHERE goods_id " . db_create_in($goods_id);
    $GLOBALS['db']->query($sql);
    $sql = "DELETE FROM " . $GLOBALS['ecs']->table('goods_article') . " WHERE goods_id " . db_create_in($goods_id);
    $GLOBALS['db']->query($sql);
    $sql = "DELETE FROM " . $GLOBALS['ecs']->table('goods_attr') . " WHERE goods_id " . db_create_in($goods_id);
    $GLOBALS['db']->query($sql);
    $sql = "DELETE FROM " . $GLOBALS['ecs']->table('goods_cat') . " WHERE goods_id " . db_create_in($goods_id);
    $GLOBALS['db']->query($sql);
    $sql = "DELETE FROM " . $GLOBALS['ecs']->table('member_price') . " WHERE goods_id " . db_create_in($goods_id);
    $GLOBALS['db']->query($sql);
    $sql = "DELETE FROM " . $GLOBALS['ecs']->table('group_goods') . " WHERE parent_id " . db_create_in($goods_id);
    $GLOBALS['db']->query($sql);
    $sql = "DELETE FROM " . $GLOBALS['ecs']->table('group_goods') . " WHERE goods_id " . db_create_in($goods_id);
    $GLOBALS['db']->query($sql);
    $sql = "DELETE FROM " . $GLOBALS['ecs']->table('link_goods') . " WHERE goods_id " . db_create_in($goods_id);
    $GLOBALS['db']->query($sql);
    $sql = "DELETE FROM " . $GLOBALS['ecs']->table('link_goods') . " WHERE link_goods_id " . db_create_in($goods_id);
    $GLOBALS['db']->query($sql);
    $sql = "DELETE FROM " . $GLOBALS['ecs']->table('tag') . " WHERE goods_id " . db_create_in($goods_id);
    $GLOBALS['db']->query($sql);
    $sql = "DELETE FROM " . $GLOBALS['ecs']->table('comment') . " WHERE comment_type = 0 AND id_value " . db_create_in($goods_id);
    $GLOBALS['db']->query($sql);
    $sql = "DELETE FROM " . $GLOBALS['ecs']->table('booking_goods') . " WHERE goods_id " . db_create_in($goods_id);
    $GLOBALS['db']->query($sql);
    $sql = "DELETE FROM " . $GLOBALS['ecs']->table('goods_activity') . " WHERE goods_id " . db_create_in($goods_id);
    $GLOBALS['db']->query($sql);
    
    // 刪除相關預售登記
    $sql = "DELETE FROM " . $GLOBALS['ecs']->table('goods_book') . " WHERE goods_id " . db_create_in($goods_id);
    $GLOBALS['db']->query($sql);
    // 刪除相關缺貨登記
    $sql = "DELETE FROM " . $GLOBALS['ecs']->table('goods_dai') . " WHERE goods_id " . db_create_in($goods_id);
    $GLOBALS['db']->query($sql);
    // 刪除相關自訂欄位
    $sql = "DELETE FROM " . $GLOBALS['ecs']->table('goods_extra_info') . " WHERE goods_id " . db_create_in($goods_id);
    $GLOBALS['db']->query($sql);
    // 刪除相關推廣鏈結
    $sql = "DELETE FROM " . $GLOBALS['ecs']->table('goods_promo_links') . " WHERE goods_id " . db_create_in($goods_id);
    $GLOBALS['db']->query($sql);

    /* 删除相应虚拟商品记录 */
    $sql = "DELETE FROM " . $GLOBALS['ecs']->table('virtual_card') . " WHERE goods_id " . db_create_in($goods_id);
    if (!$GLOBALS['db']->query($sql, 'SILENT') && $GLOBALS['db']->errno() != 1146)
    {
        die($GLOBALS['db']->error());
    }

    // Flush Category Thumb memcached
    $categoryController = new Yoho\cms\Controller\CategoryController();
    $categoryController->flushCatThumbMemcache(0, $goods_id);

    /* 清除缓存 */
    clear_cache_files();
}

/**
 * 为某商品生成唯一的货号
 * @param   int     $goods_id   商品编号
 * @return  string  唯一的货号
 */
function generate_goods_sn($goods_id)
{
    $goods_sn = $GLOBALS['_CFG']['sn_prefix'] . str_repeat('0', 4 - strlen($goods_id)) . $goods_id;

    $sql = "SELECT goods_sn FROM " . $GLOBALS['ecs']->table('goods') .
            " WHERE goods_sn LIKE '" . mysql_like_quote($goods_sn) . "%' AND goods_id <> '$goods_id' " .
            " ORDER BY LENGTH(goods_sn) DESC";
    $sn_list = $GLOBALS['db']->getCol($sql);
    if (in_array($goods_sn, $sn_list))
    {
        $max = pow(10, strlen($sn_list[0]) - strlen($goods_sn) + 1) - 1;
        $new_sn = $goods_sn . mt_rand(0, $max);
        while (in_array($new_sn, $sn_list))
        {
            $new_sn = $goods_sn . mt_rand(0, $max);
        }
        $goods_sn = $new_sn;
    }

    return $goods_sn;
}

/**
 * 商品货号是否重复
 *
 * @param   string     $goods_sn        商品货号；请在传入本参数前对本参数进行SQl脚本过滤
 * @param   int        $goods_id        商品id；默认值为：0，没有商品id
 * @return  bool                        true，重复；false，不重复
 */
function check_goods_sn_exist($goods_sn, $goods_id = 0)
{
    $goods_sn = trim($goods_sn);
    $goods_id = intval($goods_id);
    if (strlen($goods_sn) == 0)
    {
        return true;    //重复
    }

    if (empty($goods_id))
    {
        $sql = "SELECT goods_id FROM " . $GLOBALS['ecs']->table('goods') ."
                WHERE goods_sn = '$goods_sn'";
    }
    else
    {
        $sql = "SELECT goods_id FROM " . $GLOBALS['ecs']->table('goods') ."
                WHERE goods_sn = '$goods_sn'
                AND goods_id <> '$goods_id'";
    }

    $res = $GLOBALS['db']->getOne($sql);

    if (empty($res))
    {
        return false;    //不重复
    }
    else
    {
        return true;    //重复
    }

}

/**
 * 取得通用属性和某分类的属性，以及某商品的属性值
 * @param   int     $cat_id     分类编号
 * @param   int     $goods_id   商品编号
 * @return  array   规格与属性列表
 */
function get_attr_list($cat_id, $goods_id = 0)
{
    if (empty($cat_id))
    {
        return array();
    }

    // 查询属性值及商品的属性值
    $sql = "SELECT a.attr_id, a.attr_name, a.attr_input_type, a.attr_type, a.attr_values, v.attr_value, v.attr_price ".
            "FROM " .$GLOBALS['ecs']->table('attribute'). " AS a ".
            "LEFT JOIN " .$GLOBALS['ecs']->table('goods_attr'). " AS v ".
            "ON v.attr_id = a.attr_id AND v.goods_id = '$goods_id' ".
            "WHERE a.cat_id = " . intval($cat_id) ." OR a.cat_id = 0 ".
            "ORDER BY a.sort_order, a.attr_type, a.attr_id, v.attr_price, v.goods_attr_id";

    $row = $GLOBALS['db']->GetAll($sql);

    return $row;
}

/**
 * 获取商品类型中包含规格的类型列表
 *
 * @access  public
 * @return  array
 */
function get_goods_type_specifications()
{
    // 查询
    $sql = "SELECT DISTINCT cat_id
            FROM " .$GLOBALS['ecs']->table('attribute'). "
            WHERE attr_type = 1";
    $row = $GLOBALS['db']->GetAll($sql);

    $return_arr = array();
    if (!empty($row))
    {
        foreach ($row as $value)
        {
            $return_arr[$value['cat_id']] = $value['cat_id'];
        }
    }
    return $return_arr;
}

/**
 * 根据属性数组创建属性的表单
 *
 * @access  public
 * @param   int     $cat_id     分类编号
 * @param   int     $goods_id   商品编号
 * @return  string
 */
function build_attr_html($cat_id, $goods_id = 0)
{
    $attr = get_attr_list($cat_id, $goods_id);
    $html = '<div id="attrTable">';
    $spec = 0;

    foreach ($attr AS $key => $val)
    {
        $attr_id = $val['attr_id'];
        $sql = "SELECT count(*) FROM ".$GLOBALS['ecs']->table('attribute_lang'). " WHERE attr_id = $attr_id";
        $have_lang = false;
        if($GLOBALS['db']->getOne($sql)> 0) $have_lang = true;
        $html .= "<div class='row'><label class='control-label col-md-3 col-sm-3 col-xs-12'>";
        if ($val['attr_type'] == 1 || $val['attr_type'] == 2)
        {
            $html .= ($spec != $val['attr_id']) ?
                "<a href='javascript:;' onclick='addSpec(this)'>[+]</a>" :
                "<a href='javascript:;' onclick='removeSpec(this)'>[-]</a>";
            $spec = $val['attr_id'];
        }

        $html .= "$val[attr_name]</label><div class='col-md-6 col-sm-6 col-xs-12'><input class='form-control' type='hidden' name='attr_id_list[]' value='$val[attr_id]' />";

        if ($val['attr_input_type'] == 0)
        {
            $html .= '<input class="form-control" name="attr_value_list[]" type="text" value="' .htmlspecialchars($val['attr_value']). '" size="40" /> ';
        }
        elseif ($val['attr_input_type'] == 2)
        {
            $html .= '<textarea class="form-control" name="attr_value_list[]" rows="3" cols="40">' .htmlspecialchars($val['attr_value']). '</textarea>';
        }
        else
        {
            $html .= '<select class="form-control" name="attr_value_list[]">';
            $html .= '<option value="">' .$GLOBALS['_LANG']['select_please']. '</option>';

            $attr_values = explode("\n", $val['attr_values']);

            foreach ($attr_values AS $opt)
            {
                $opt    = trim(htmlspecialchars($opt));

                $html   .= ($val['attr_value'] != $opt) ?
                    '<option value="' . $opt . '">' . $opt . '</option>' :
                    '<option value="' . $opt . '" selected="selected">' . $opt . '</option>';
            }
            $html .= '</select> ';
            if(!$have_lang) $html.="<span class='require-field'>* 此屬性未有其他語言選項(.e.g. : en_us)</span>";
        }

        $html .= ($val['attr_type'] == 1 || $val['attr_type'] == 2) ?
            $GLOBALS['_LANG']['spec_price'].' <input type="text" name="attr_price_list[]" value="' . $val['attr_price'] . '" size="5" maxlength="10" />' :
            ' <input type="hidden" name="attr_price_list[]" value="0" />';

        $html .= '</div></div>';
    }

    $html .= '</div>';

    return $html;
}

/**
 * 获得指定商品相关的商品
 *
 * @access  public
 * @param   integer $goods_id
 * @return  array
 */
function get_linked_goods($goods_id)
{
    $sql = "SELECT lg.link_goods_id AS goods_id, g.goods_name, lg.is_double " .
            "FROM " . $GLOBALS['ecs']->table('link_goods') . " AS lg, " .
                $GLOBALS['ecs']->table('goods') . " AS g " .
            "WHERE lg.goods_id = '$goods_id' " .
            "AND lg.link_goods_id = g.goods_id ";
    if ($goods_id == 0)
    {
        $sql .= " AND lg.admin_id = '$_SESSION[admin_id]'";
    }
    $row = $GLOBALS['db']->getAll($sql);

    foreach ($row AS $key => $val)
    {
        $linked_type = $val['is_double'] == 0 ? $GLOBALS['_LANG']['single'] : $GLOBALS['_LANG']['double'];

        $row[$key]['goods_name'] = $val['goods_name'] . " -- [$linked_type]";

        unset($row[$key]['is_double']);
    }

    return $row;
}

/**
 * 获得指定商品的配件
 *
 * @access  public
 * @param   integer $goods_id
 * @return  array
 */
function get_group_goods($goods_id)
{
    $sql = "SELECT gg.goods_id, CONCAT(g.goods_name, ' -- [', gg.goods_price, ']') AS goods_name " .
            "FROM " . $GLOBALS['ecs']->table('group_goods') . " AS gg, " .
                $GLOBALS['ecs']->table('goods') . " AS g " .
            "WHERE gg.parent_id = '$goods_id' " .
            "AND gg.goods_id = g.goods_id ";
    if ($goods_id == 0)
    {
        $sql .= " AND gg.admin_id = '$_SESSION[admin_id]'";
    }
    $row = $GLOBALS['db']->getAll($sql);

    return $row;
}

/**
 * 获得商品的关联文章
 *
 * @access  public
 * @param   integer $goods_id
 * @return  array
 */
function get_goods_articles($goods_id)
{
    $sql = "SELECT g.article_id, a.title " .
            "FROM " .$GLOBALS['ecs']->table('goods_article') . " AS g, " .
                $GLOBALS['ecs']->table('article') . " AS a " .
            "WHERE g.goods_id = '$goods_id' " .
            "AND g.article_id = a.article_id ";
    if ($goods_id == 0)
    {
        $sql .= " AND g.admin_id = '$_SESSION[admin_id]'";
    }
    $row = $GLOBALS['db']->getAll($sql);

    return $row;
}

/**
 * 获得指定商品的自訂欄位
 *
 * @access  public
 * @param   integer $goods_id
 * @return  array
 */
function get_goods_extra_info($goods_id)
{
    $sql = "SELECT * " .
            "FROM " . $GLOBALS['ecs']->table('goods_extra_info') .
            "WHERE goods_id = '$goods_id' " .
            "ORDER BY `info_order` ASC ";
    $row = $GLOBALS['db']->getAll($sql);

    return $row;
}

/**
 * 获得指定商品的推廣鏈結
 *
 * @access  public
 * @param   integer $goods_id
 * @return  array
 */
function get_goods_promo_links($goods_id)
{
    $sql = "SELECT * " .
            "FROM " . $GLOBALS['ecs']->table('goods_promo_links') .
            "WHERE goods_id = '$goods_id' " .
            "ORDER BY `link_order` ASC ";
    $row = $GLOBALS['db']->getAll($sql);

    return $row;
}

/**
 * 获得商品列表
 *
 * @access  public
 * @params  integer $isdelete
 * @params  integer $real_goods
 * @params  integer $conditions
 * @return  array
 */
function goods_list($is_delete, $real_goods=1, $conditions = '')
{
    global $erpController, $goodsController;
    /* 过滤条件 */
    $param_str = '-' . $is_delete . '-' . $real_goods;
    $result = get_filter($param_str);
    if ($result === false)
    {
        $day = getdate();
        $today = local_mktime(23, 59, 59, $day['mon'], $day['mday'], $day['year']);

        $filter['cat_id']           = empty($_REQUEST['cat_id']) ? 0 : intval($_REQUEST['cat_id']);
        $filter['vip_abnormal']     = empty($_REQUEST['vip_abnormal']) ? 0 : intval($_REQUEST['vip_abnormal']);
        $filter['intro_type']       = empty($_REQUEST['intro_type']) ? '' : trim($_REQUEST['intro_type']);
        $filter['is_promote']       = empty($_REQUEST['is_promote']) ? 0 : intval($_REQUEST['is_promote']);
        $filter['stock_warning']    = empty($_REQUEST['stock_warning']) ? 0 : intval($_REQUEST['stock_warning']);
        $filter['brand_id']         = empty($_REQUEST['brand_id']) ? 0 : intval($_REQUEST['brand_id']);
        $filter['keyword']          = empty($_REQUEST['keyword']) ? '' : trim($_REQUEST['keyword']);
        $filter['include_goods']    = empty($_REQUEST['include_goods']) ? '' : trim($_REQUEST['include_goods']);
        $filter['suppliers_id']     = isset($_REQUEST['suppliers_id']) ? (empty($_REQUEST['suppliers_id']) ? '' : trim($_REQUEST['suppliers_id'])) : '';
        $filter['is_on_sale']       = isset($_REQUEST['is_on_sale']) ? ((empty($_REQUEST['is_on_sale']) && $_REQUEST['is_on_sale'] === 0) ? '' : trim($_REQUEST['is_on_sale'])) : '';
        $filter['goods_warehouse']  = empty($_REQUEST['goods_warehouse']) ? 0 : $_REQUEST['goods_warehouse'];
        if (isset($_REQUEST['is_ajax']) && $_REQUEST['is_ajax'] == 1)
        {
            $filter['keyword'] = json_str_iconv($filter['keyword']);
        }
        $filter['sort_by']          = empty($_REQUEST['sort_by']) ? 'goods_id' : trim($_REQUEST['sort_by']);
        $filter['sort_order']       = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
        $filter['extension_code']   = empty($_REQUEST['extension_code']) ? '' : trim($_REQUEST['extension_code']);
        $filter['is_delete']        = $is_delete;
        $filter['real_goods']       = $real_goods;
        
        $filter['preorder_warning'] = empty($_REQUEST['preorder_warning']) ? 0 : intval($_REQUEST['preorder_warning']);
        $filter['no_desc'] = empty($_REQUEST['no_desc']) ? 0 : intval($_REQUEST['no_desc']);
        $filter['no_brief'] = empty($_REQUEST['no_brief']) ? 0 : intval($_REQUEST['no_brief']);
        $filter['proddb_not_linked'] = empty($_REQUEST['proddb_not_linked']) ? 0 : intval($_REQUEST['proddb_not_linked']);
        $filter['no_barcode'] = empty($_REQUEST['no_barcode']) ? 0 : intval($_REQUEST['no_barcode']);
        $filter['warranty_template_id'] = isset($_REQUEST['warranty_template_id']) ? intval($_REQUEST['warranty_template_id']) : -1;
        $filter['goods_ids'] = empty($_REQUEST['goods_ids']) ? '' : array_filter(array_map('intval', explode(',', $_REQUEST['goods_ids'])));
        $filter['no_eng_name'] = !empty($_REQUEST['no_eng_name']) ? 1 : 0;
        $filter['no_lwh'] = empty($_REQUEST['no_lwh']) ? 0 : intval($_REQUEST['no_lwh']);

        $filter['show_pricecomhk'] = !empty($_REQUEST['show_pricecomhk']) ? 1 : 0;
        $filter['show_slowsoldgoods'] = !empty($_REQUEST['show_slowsoldgoods']) ? 1 : 0;
        $filter['start']  = empty($_REQUEST['start']) ? 0 : intval($_REQUEST['start']);
        $filter['page_size']  = empty($_REQUEST['page_size']) ? 0 : intval($_REQUEST['page_size']);
        $where = $filter['cat_id'] > 0 ? " AND " . get_children($filter['cat_id']) : '';

        /* 推荐类型 */
        switch ($filter['intro_type'])
        {
            case 'is_best':
                $where .= " AND is_best=1";
                break;
            case 'is_hot':
                $where .= ' AND is_hot=1';
                break;
            case 'is_new':
                $where .= ' AND is_new=1';
                break;
            case 'is_promote':
                $where .= " AND is_promote = 1 AND promote_price > 0 AND promote_start_date <= '$today' AND promote_end_date >= '$today'";
                break;
            case 'all_type';
                $where .= " AND (is_best=1 OR is_hot=1 OR is_new=1 OR (is_promote = 1 AND promote_price > 0 AND promote_start_date <= '" . $today . "' AND promote_end_date >= '" . $today . "'))";
        }

        if (!empty($filter['goods_warehouse'])) {
            switch ($filter['goods_warehouse'])
            {
                case 'FLS':
                    $where .= " AND is_fls_warehouse = 1";
                    break;
            }
        }
        
        /* 品牌 */
        if ($filter['brand_id'])
        {
            if ($filter['brand_id'] < 0) $filter['brand_id'] = 0;
            $where .= " AND brand_id='$filter[brand_id]'";
        }

        /* 扩展 */
        if ($filter['extension_code'])
        {
            $where .= " AND extension_code='$filter[extension_code]'";
        }

        /* 关键字 */
        if (!empty($filter['keyword']))
        {
            $where .= " AND (goods_sn LIKE '%" . mysql_like_quote($filter['keyword']) . "%' ";
            $where .= " OR goods_name LIKE '%" . mysql_like_quote($filter['keyword']) . "%' ";
            $tmp = preg_split('/\s+/', $filter['keyword']);
            if (count($tmp) > 1)
            {
                $where .= " OR ( 1";
                foreach ($tmp as $kw)
                {
                    $where .= " AND goods_name LIKE '%" . mysql_like_quote($kw) . "%' ";
                }
                $where .= ") ";
            }
            $where .= " OR goods_id = '" . mysql_like_quote($filter['keyword']) . "')";
        }

        /* 產品名稱/ID */
        if (!empty($filter['include_goods']))
        {
            if(strpos($filter['include_goods'], ",") > 0){
                $include_goods = explode(",", $filter['include_goods']);
        
                $include_goods_like_str = array_map(function ($goods_name) { return "goods_name like '%".trim(mysql_like_quote($goods_name))."%'"; }, $include_goods);
                $include_goods_sn_like_str = array_map(function ($goods_sn) { return "goods_name like '%".trim(mysql_like_quote($goods_sn))."%'"; }, $include_goods);
                $include_goods_id_str = array_map(function ($goods_id) { return "goods_id = '".trim(mysql_like_quote($goods_id))."'"; }, $include_goods);

                $where .= " AND (".implode(" OR ", $include_goods_like_str)." OR ".implode(" OR ", $include_goods_sn_like_str)." OR ".implode(" OR ", $include_goods_id_str).")";
            }
            else{
                $where .= " AND (goods_sn LIKE '%" . mysql_like_quote($filter['include_goods']) . "%' ";
                $where .= " OR goods_name LIKE '%" . mysql_like_quote($filter['include_goods']) . "%' ";
                $tmp = preg_split('/\s+/', $filter['include_goods']);
                if (count($tmp) > 1)
                {
                    $where .= " OR ( 1";
                    foreach ($tmp as $kw)
                    {
                        $where .= " AND goods_name LIKE '%" . mysql_like_quote($kw) . "%' ";
                    }
                    $where .= ") ";
                }
                $where .= " OR goods_id = '" . mysql_like_quote($filter['include_goods']) . "')";
            }
            
        }

        if ($real_goods > -1)
        {
            $where .= " AND is_real='$real_goods'";
        }

        /* 上架 */
        if ($filter['is_on_sale'] !== '')
        {
            $where .= " AND (is_on_sale = '" . $filter['is_on_sale'] . "')";
        }

        /* 供货商 */
        if (!empty($filter['suppliers_id']))
        {
            $where .= " AND goods_id in (select goods_id from ".$GLOBALS['ecs']->table('erp_goods_supplier')." where supplier_id='".$filter['suppliers_id']."')";
        }
        
        if ($filter['preorder_warning'])
        {
            $where .= " AND (SELECT sum(ega_po.goods_qty) FROM " . $GLOBALS['ecs']->table('erp_goods_attr_stock') . " as ega_po WHERE ega_po.goods_id = g.goods_id AND ega_po.warehouse_id IN (".$erpController->getSellableWarehouseIds(true).")) > 0 AND g.pre_sale_days > 0 ";
        }
        
        if ($filter['no_desc'])
        {
            $where .= " AND trim(g.goods_desc) = '' ";
        }
        
        if ($filter['no_brief'])
        {
            $where .= " AND trim(g.goods_brief) = '' ";
        }
        
        if ($filter['proddb_not_linked'])
        {
            $where .= " AND g.proddb_id = 0 ";
        }
        
        if ($filter['no_barcode'])
        {
            $where .= " AND g.goods_sn = g.goods_id ";
        }

        if ($filter['no_lwh'])
        {
            $where .= " AND (g.goods_height <= 0 or g.goods_width <= 0 or g.goods_length <= 0) ";
        }
        
        if ($filter['warranty_template_id'] != -1)
        {
            $where .= " AND g.warranty_template_id = '" . $filter['warranty_template_id'] . "' ";
        }

        if (!empty($filter['goods_ids']) && is_array($filter['goods_ids']))
        {
            $where .= " AND g.goods_id " . db_create_in($filter['goods_ids']);
        }
        
        if ($filter['no_eng_name'])
        {
            $where .= " AND g.goods_id NOT IN (SELECT nge.goods_id FROM `ecs_goods_lang` as nge WHERE nge.lang = 'en_us' AND nge.goods_name IS NOT NULL) ";
        }
        $where .= $conditions;
        
        /* 记录总数 */
        // howang: handle 庫存警告
        $stock_warning_where = ($filter['stock_warning']) ? "AND (SELECT sum(ega.goods_qty) FROM " . $GLOBALS['ecs']->table('erp_goods_attr_stock') . " as ega WHERE ega.goods_id = g.goods_id AND ega.warehouse_id IN (".$erpController->getSellableWarehouseIds(true).")) <= warn_number" : "";
        $sql = "SELECT COUNT(*) FROM " .$GLOBALS['ecs']->table('goods'). " AS g WHERE is_delete='$is_delete' $where $stock_warning_where";
        $filter['record_count'] = $GLOBALS['db']->getOne($sql);

        /* 分页大小 */
        // $filter = page_and_size($filter);
        
        // howang: Prioritize exact matches
        if($filter['sort_by'] == 'vip_price') $orderby = 'CAST(vip_price AS DECIMAL(10,2))' . ' ' . $filter['sort_order'] . ' ';
        else $orderby = $filter['sort_by'] . ' ' . $filter['sort_order'] . ' ';
        if (!empty($filter['keyword']))
        {
            $orderby = "CASE " . 
                       "WHEN goods_id = '" . $filter['keyword'] . "' THEN 0 " .
                       "WHEN goods_sn = '" . $filter['keyword'] . "' THEN 1 " .
                       "WHEN goods_name = '" . $filter['keyword'] . "' THEN 2 " .
                       "ELSE 3 " .
                       "END ASC, " . $orderby;
        }
        
        $time_yesterday = local_strtotime('today'); // 23:59:59 yesterday
        $time_30days_before = local_strtotime('-30 days midnight'); // 00:00:00 30 days ago
        $stock_cost_log = false;
        $max_cost_log_time = $GLOBALS['db']->getOne("SELECT MAX(report_date) FROM " . $GLOBALS['ecs']->table('stock_cost_log') . " WHERE type = 4");
        if (!empty($max_cost_log_time))  {
            $stock_cost_log = true;
        }
        if ($filter['stock_warning']) // howang: handle 庫存警告
        {
            $sql = ($filter['show_slowsoldgoods'] ? "SELECT *, IFNULL((goods_number/thirtydayssales*30), 99999999) AS soldoutdays FROM (" : 'select * from (') .
                    "SELECT goods_id, goods_name, goods_type, goods_sn, shop_price, market_price, is_best_sellers, is_on_sale, is_best, is_new, is_hot, is_promote_cart, is_hidden, sort_order, give_integral, is_pre_sale, pre_sale_date, pre_sale_days, is_purchasing, warn_number, commission, cost, sales, sales_adjust, proddb_id, " .
                    " (SELECT sum(ega.goods_qty) FROM " . $GLOBALS['ecs']->table('erp_goods_attr_stock') . " as ega WHERE ega.goods_id = g.goods_id AND ega.warehouse_id IN (".$erpController->getSellableWarehouseIds(true).")) as goods_number, ".
                    " IFNULL((SELECT mp.user_price FROM " . $GLOBALS['ecs']->table('member_price') . " as mp WHERE mp.goods_id = g.goods_id AND mp.user_rank = '2'),'無') as vip_price, ".
                    " IF((SELECT y.goods_id FROM " . $GLOBALS['ecs']->table('yahoo_store') . " as y WHERE y.goods_id = g.goods_id),true,false) as is_yahoo, ".
                    " IF((SELECT h.goods_id FROM " . $GLOBALS['ecs']->table('hktvmall') . " as h WHERE h.goods_id = g.goods_id),true,false) as is_hktv, ".
                    " (promote_price > 0 AND promote_start_date <= '$today' AND promote_end_date >= '$today') AS is_promote, display_discount ".
                    ($filter['show_pricecomhk'] ? ", (SELECT avg_price FROM " . $GLOBALS['ecs']->table('goods_pricecomhk') . " as gpch WHERE gpch.goods_id = g.goods_id) as pricecomhk_avg_price " : '') .
                    ($filter['show_slowsoldgoods'] ? (
                        $stock_cost_log
                            ? (", IFNULL((SELECT ndayssales_30 FROM " . $GLOBALS['ecs']->table('stock_cost_log') . " as scl WHERE scl.type = 4 AND report_date = $max_cost_log_time AND scl.type_id = g.goods_id), 0) as thirtydayssales ")
                            : (", (SELECT SUM(og.goods_number) FROM " . $GLOBALS['ecs']->table('order_goods') . " as og LEFT JOIN " . $GLOBALS['ecs']->table('order_info') . " as oi ON oi.order_id = og.order_id WHERE og.goods_id = g.goods_id AND oi.order_status IN ('1','5') AND oi.shipping_status IN ('1','2') AND oi.pay_status IN ('2','1') AND (oi.shipping_time BETWEEN " . $time_30days_before . " AND " . $time_yesterday . ") ) as thirtydayssales ")
                        ) : '') .
                       " FROM " . $GLOBALS['ecs']->table('goods') . " AS g WHERE is_delete='$is_delete' $where " .
                ") as tmp WHERE goods_number <= warn_number " .
                ($filter['show_slowsoldgoods'] ? " AND goods_number > 0 AND (thirtydayssales IS NULL OR (thirtydayssales > 0 AND goods_number * 3 >= thirtydayssales * 5))" : '') .
                " ORDER BY $orderby " .
                ($filter['vip_abnormal']==0 ? " LIMIT " . $filter['start'] . ",$filter[page_size]" : "");
        }
        else
        {
            $sql =  ($filter['show_slowsoldgoods'] ? "SELECT *, IFNULL((goods_number/thirtydayssales*30), 99999999) AS soldoutdays FROM (" : '') .
                    "SELECT goods_id, goods_name, goods_type, goods_sn, shop_price, market_price, is_best_sellers, is_on_sale, is_best, is_new, is_hot, is_promote_cart, is_hidden, sort_order, give_integral, is_pre_sale, pre_sale_date, pre_sale_days, is_purchasing, warn_number, commission, cost, sales, sales_adjust, proddb_id, " .
                    " (SELECT sum(ega.goods_qty) FROM " . $GLOBALS['ecs']->table('erp_goods_attr_stock') . " as ega WHERE ega.goods_id = g.goods_id AND ega.warehouse_id IN (".$erpController->getSellableWarehouseIds(true).")) as goods_number, ".
                    " IFNULL((SELECT mp.user_price FROM " . $GLOBALS['ecs']->table('member_price') . " as mp WHERE mp.goods_id = g.goods_id AND mp.user_rank = '2'),'無') as vip_price, ".
                    " IF((SELECT y.goods_id FROM " . $GLOBALS['ecs']->table('yahoo_store') . " as y WHERE y.goods_id = g.goods_id),true,false) as is_yahoo, ".
                    " IF((SELECT h.goods_id FROM " . $GLOBALS['ecs']->table('hktvmall') . " as h WHERE h.goods_id = g.goods_id),true,false) as is_hktv, ".
                    " (promote_price > 0 AND promote_start_date <= '$today' AND promote_end_date >= '$today') AS is_promote, display_discount ".
                    ($filter['show_pricecomhk'] ? ", (SELECT avg_price FROM " . $GLOBALS['ecs']->table('goods_pricecomhk') . " as gpch WHERE gpch.goods_id = g.goods_id) as pricecomhk_avg_price " : '') .
                    ($filter['show_slowsoldgoods'] ? (
                        $stock_cost_log
                            ? (", IFNULL((SELECT ndayssales_30 FROM " . $GLOBALS['ecs']->table('stock_cost_log') . " as scl WHERE scl.type = 4 AND report_date = $max_cost_log_time AND scl.type_id = g.goods_id), 0) as thirtydayssales ")
                            : (", (SELECT SUM(og.goods_number) FROM " . $GLOBALS['ecs']->table('order_goods') . " as og LEFT JOIN " . $GLOBALS['ecs']->table('order_info') . " as oi ON oi.order_id = og.order_id WHERE og.goods_id = g.goods_id AND oi.order_status IN ('1','5') AND oi.shipping_status IN ('1','2') AND oi.pay_status IN ('2','1') AND (oi.shipping_time BETWEEN " . $time_30days_before . " AND " . $time_yesterday . ") ) as thirtydayssales ")
                        ) : '') .
                    " FROM " . $GLOBALS['ecs']->table('goods') . " AS g WHERE is_delete='$is_delete' $where " .
                    ($filter['show_slowsoldgoods'] ? ") AS thirtydays_table WHERE (thirtydayssales IS NULL OR (thirtydayssales > 0 AND goods_number * 3 >= thirtydayssales * 5)) AND goods_number > 0" : '') .            
                    " ORDER BY $orderby " .
                    ($filter['vip_abnormal']==0 ? " LIMIT " . $filter['start'] . ",$filter[page_size]" : "");
        }
        $vip_price_abnormal_count =  "SELECT count(*) FROM (" . $sql . ") AS a where a.vip_price != '無' AND a.vip_price > a.shop_price";
        $vip_price_abnormal = "SELECT * FROM (" . $sql . ") AS a where a.vip_price != '無' AND a.vip_price > a.shop_price";
        $filter['keyword'] = stripslashes($filter['keyword']);
        set_filter($filter, $sql, $param_str);
    }
    else
    {
        $sql    = $result['sql'];
        $filter = $result['filter'];
    }
    if($filter['vip_abnormal']==0){
        $row = $GLOBALS['db']->getAll($sql);
    }else{
        $row = $GLOBALS['db']->getAll($vip_price_abnormal);
        $filter['record_count'] = $GLOBALS['db']->getOne($vip_price_abnormal_count);
    }
    // $row = $GLOBALS['db']->getAll($sql);
    $res_vip_price_abnormal_count = $GLOBALS['db']->getOne($vip_price_abnormal_count);
    // Multiple language support
    if (!empty($_REQUEST['edit_lang']))
    {
        $row = localize_db_result_lang($_REQUEST['edit_lang'], 'goods', $row);
    }
    $self_supplier_id = get_self_supplier_id(1);
    foreach($row as $key => $item)
    {
        $row[$key]['pre_sale_date'] = $item['pre_sale_date'] ? local_date('Y-m-d',$item['pre_sale_date']) : '2013-00-00';
        $row[$key]['cost'] = number_format($item['cost'], 2, '.', '');
        if ($row[$key]['vip_price'] >= $row[$key]['shop_price'] && $row[$key]['vip_price'] != '無') {
            $row[$key]['vip_price'] = '<span class="red">' . $row[$key]['vip_price'] . '</span>';
        }
        if ($filter['show_slowsoldgoods'])
        {
            //$row[$key]['soldoutdays'] = $item['thirtydayssales'] ? ceil($item['goods_number'] / $item['thirtydayssales'] * 30) : '∞';
            $row[$key]['soldoutdays'] = $item['soldoutdays'] && $item['soldoutdays']!=99999999 ? $item['soldoutdays'] : '∞';
        }

        if(!$_SESSION['manage_cost'] && check_authz(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ONLYSELFSUPPLIERS)){
            if(count($self_supplier_id) > 0){
                $self_goods_id = get_supplier_goods_ids($self_supplier_id);
            }
            $row[$key]['cost']  = isset($item['cost']) && in_array($item['goods_id'],$self_goods_id) ? number_format($item['cost'], 2, '.', '') : "--";
            $row[$key]['sales'] = isset($item['sales']) && in_array($item['goods_id'],$self_goods_id) ? $item['sales'] : "--";
        }
        $custom_action = [
            'c_edit' => ['icon_class'=> 'fa fa-edit','btn_class'=> 'btn-info', 'link' => './goods.php?act=edit&id='.$item['goods_id'], 'label' =>'', 'target' => '_blank'],
            'c_remove' => ['icon_class'=> 'fa fa-trash','btn_class'=> 'btn-danger', 'link' => '#', 'data' => ['url' => 'goods.php?act=remove&id='.$item['goods_id']], 'onclick' =>'cellRemove(this);', 'label' =>''],
            'c_view' => ['icon_class'=> 'fa fa-eye', 'link' => '../goods.php?id='.$item['goods_id'], 'target' => '_blank', 'label' =>''],
            'c_copy' => ['icon_class'=> 'fa fa-copy', 'link' => 'goods.php?act=copy&amp;id='.$item['goods_id'], 'label' =>''],
            //隱藏特殊價格btn
            // 'user_price' => ['icon_class'=> 'fa fa-dollar-sign', 'link' => 'goods.php?act=user_rank_price&amp;id='.$item['goods_id'], 'label'=>'特殊價格']
        ];
        $row[$key]['_action'] = $goodsController->generateCrudActionBtn($item['goods_id'], 'id', null, [], $custom_action);
    }

    return array('goods' => $row, 'filter' => $filter, 'page_count' => $filter['page_count'], 'record_count' => $filter['record_count'], 'vip_price_abnormal_count' => $res_vip_price_abnormal_count);
}
//VIP價格異常的數量
function vip_abnormal_count($is_delete, $real_goods=1, $conditions = '')
{
    $sql = "SELECT count(*) FROM ( SELECT goods_id, shop_price," .
                " IFNULL((SELECT mp.user_price FROM " . $GLOBALS['ecs']->table('member_price') . " as mp WHERE mp.goods_id = g.goods_id AND mp.user_rank = '2'),'無') as vip_price ".  
                " FROM " . $GLOBALS['ecs']->table('goods') . " AS g WHERE is_delete='$is_delete' AND is_on_sale=1) AS a WHERE a.vip_price != '無' AND a.vip_price > a.shop_price";         
        return $GLOBALS['db']->getOne($sql);
}
/**
 * 检测商品是否有货品
 *
 * @access      public
 * @params      integer     $goods_id       商品id
 * @params      string      $conditions     sql条件，AND语句开头
 * @return      string number               -1，错误；1，存在；0，不存在
 */
function check_goods_product_exist($goods_id, $conditions = '')
{
    if (empty($goods_id))
    {
        return -1;  //$goods_id不能为空
    }

    $sql = "SELECT goods_id
            FROM " . $GLOBALS['ecs']->table('products') . "
            WHERE goods_id = '$goods_id'
            " . $conditions . "
            LIMIT 0, 1";
    $result = $GLOBALS['db']->getRow($sql);

    if (empty($result))
    {
        return 0;
    }

    return 1;
}

/**
 * 获得商品的货品总库存
 *
 * @access      public
 * @params      integer     $goods_id       商品id
 * @params      string      $conditions     sql条件，AND语句开头
 * @return      string number
 */
function product_number_count($goods_id, $conditions = '')
{
    if (empty($goods_id))
    {
        return -1;  //$goods_id不能为空
    }

    $sql = "SELECT SUM(product_number)
            FROM " . $GLOBALS['ecs']->table('products') . "
            WHERE goods_id = '$goods_id'
            " . $conditions;
    $nums = $GLOBALS['db']->getOne($sql);
    $nums = empty($nums) ? 0 : $nums;

    return $nums;
}

/**
 * 获得商品的规格属性值列表
 *
 * @access      public
 * @params      integer         $goods_id
 * @return      array
 */
function product_goods_attr_list($goods_id)
{
    if (empty($goods_id))
    {
        return array();  //$goods_id不能为空
    }

    $sql = "SELECT goods_attr_id, attr_value FROM " . $GLOBALS['ecs']->table('goods_attr') . " WHERE goods_id = '$goods_id'";
    $results = $GLOBALS['db']->getAll($sql);

    $return_arr = array();
    foreach ($results as $value)
    {
        $return_arr[$value['goods_attr_id']] = $value['attr_value'];
    }

    return $return_arr;
}

/**
 * 获得商品已添加的规格列表
 *
 * @access      public
 * @params      integer         $goods_id
 * @return      array
 */
function get_goods_specifications_list($goods_id)
{
    if (empty($goods_id))
    {
        return array();  //$goods_id不能为空
    }

    $sql = "SELECT g.goods_attr_id, g.attr_value, g.attr_id, a.attr_name
            FROM " . $GLOBALS['ecs']->table('goods_attr') . " AS g
                LEFT JOIN " . $GLOBALS['ecs']->table('attribute') . " AS a
                    ON a.attr_id = g.attr_id
            WHERE goods_id = '$goods_id'
            AND a.attr_type = 1
            ORDER BY g.attr_id ASC";
    $results = $GLOBALS['db']->getAll($sql);

    return $results;
}


/**
 * 检查单个商品是否存在规格
 *
 * @param   int        $goods_id          商品id
 * @return  bool                          true，存在；false，不存在
 */
function check_goods_specifications_exist($goods_id)
{
    $goods_id = intval($goods_id);

    $sql = "SELECT COUNT(a.attr_id)
            FROM " .$GLOBALS['ecs']->table('attribute'). " AS a, " .$GLOBALS['ecs']->table('goods'). " AS g
            WHERE a.cat_id = g.goods_type
            AND g.goods_id = '$goods_id'";

    $count = $GLOBALS['db']->getOne($sql);

    if ($count > 0)
    {
        return true;    //存在
    }
    else
    {
        return false;    //不存在
    }
}

/**
 * 商品的货品规格是否存在
 *
 * @param   string     $goods_attr        商品的货品规格
 * @param   string     $goods_id          商品id
 * @param   int        $product_id        商品的货品id；默认值为：0，没有货品id
 * @return  bool                          true，重复；false，不重复
 */
function check_goods_attr_exist($goods_attr, $goods_id, $product_id = 0)
{
    $goods_id = intval($goods_id);
    if (strlen($goods_attr) == 0 || empty($goods_id))
    {
        return true;    //重复
    }

    if (empty($product_id))
    {
        $sql = "SELECT product_id FROM " . $GLOBALS['ecs']->table('products') ."
                WHERE goods_attr = '$goods_attr'
                AND goods_id = '$goods_id'";
    }
    else
    {
        $sql = "SELECT product_id FROM " . $GLOBALS['ecs']->table('products') ."
                WHERE goods_attr = '$goods_attr'
                AND goods_id = '$goods_id'
                AND product_id <> '$product_id'";
    }

    $res = $GLOBALS['db']->getOne($sql);

    if (empty($res))
    {
        return false;    //不重复
    }
    else
    {
        return true;    //重复
    }
}


/**
 * 格式化商品图片名称（按目录存储）
 *
 */
function reformat_image_name($type, $goods_id, $source_img, $position='')
{
    $rand_name = gmtime() . sprintf("%03d", mt_rand(1,999));
    $img_ext = substr($source_img, strrpos($source_img, '.'));
    $dir = 'images';
    if (defined('IMAGE_DIR'))
    {
        $dir = IMAGE_DIR;
    }
    $sub_dir = date('Ym', gmtime());
    if (!make_dir(ROOT_PATH.$dir.'/'.$sub_dir))
    {
        return false;
    }
    if (!make_dir(ROOT_PATH.$dir.'/'.$sub_dir.'/source_img'))
    {
        return false;
    }
    if (!make_dir(ROOT_PATH.$dir.'/'.$sub_dir.'/goods_img'))
    {
        return false;
    }
    if (!make_dir(ROOT_PATH.$dir.'/'.$sub_dir.'/thumb_img'))
    {
        return false;
    }
    switch($type)
    {
        case 'goods':
            $img_name = $goods_id . '_G_' . $rand_name;
            break;
        case 'goods_thumb':
            $img_name = $goods_id . '_thumb_G_' . $rand_name;
            break;
        case 'gallery':
            $img_name = $goods_id . '_P_' . $rand_name;
            break;
        case 'gallery_thumb':
            $img_name = $goods_id . '_thumb_P_' . $rand_name;
            break;
    }
    if ($position == 'source')
    {
        if (move_image_file(ROOT_PATH.$source_img, ROOT_PATH.$dir.'/'.$sub_dir.'/source_img/'.$img_name.$img_ext))
        {
            return $dir.'/'.$sub_dir.'/source_img/'.$img_name.$img_ext;
        }
    }
    elseif ($position == 'thumb')
    {
        if (move_image_file(ROOT_PATH.$source_img, ROOT_PATH.$dir.'/'.$sub_dir.'/thumb_img/'.$img_name.$img_ext))
        {
            image_compression(ROOT_PATH.$dir.'/'.$sub_dir.'/thumb_img/'.$img_name.$img_ext,null,false,true);
            return $dir.'/'.$sub_dir.'/thumb_img/'.$img_name.$img_ext;
        }
    }
    else
    {
        if (move_image_file(ROOT_PATH.$source_img, ROOT_PATH.$dir.'/'.$sub_dir.'/goods_img/'.$img_name.$img_ext))
        {
            if ($type == 'gallery') {
                image_compression(ROOT_PATH.$dir.'/'.$sub_dir.'/goods_img/'.$img_name.$img_ext);
            }
            
            return $dir.'/'.$sub_dir.'/goods_img/'.$img_name.$img_ext;
        }
    }
    return false;
}

function move_image_file($source, $dest)
{
    if (@copy($source, $dest))
    {
        @unlink($source);
        return true;
    }
    return false;
}

?>
