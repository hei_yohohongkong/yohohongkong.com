<?php

/***
* lib_proddb.php
* by howang 2015-09-24
*
* Communicate with YOHO Product Database
***/

if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

if (!defined('PRODDB_HOST') || !defined('PRODDB_APP_ID') || !defined('PRODDB_APP_KEY'))
{
	die('Error: PRODDB_HOST, PRODDB_APP_ID or PRODDB_APP_KEY not found');
}

require_once ROOT_PATH . 'core/lib_payload.php';

// getimagesizefromstring() polyfill for PHP 5.2+
if (!function_exists('getimagesizefromstring'))
{
	function getimagesizefromstring($image_data)
	{
		$uri = 'data://application/octet-stream;base64,'  . base64_encode($image_data);
		return getimagesize($uri);
	}
}

class ProdDB
{
	public function search_product($keyword)
	{
		$url = 'http://' . PRODDB_HOST . '/api/search?app=' . PRODDB_APP_ID . '&keyword=' . rawurlencode($keyword);
		$payload = file_get_contents($url);
		$data = decode_payload($payload, PRODDB_APP_KEY);
		if ($data['status'] == 1)
		{
			return $data['results'];
		}
		else
		{
			return false;
		}
	}
	
	public function get_product_name_by_id($product_id)
	{
		$url = 'http://' . PRODDB_HOST . '/api/name/' . intval($product_id) . '?app=' . PRODDB_APP_ID;
		$payload = file_get_contents($url);
		$data = decode_payload($payload, PRODDB_APP_KEY);
		
		if ($data['status'] == 1)
		{
			return $data['product_name'];
		}
		else
		{
			return false;
		}
	}
	
	public function get_product_by_id($product_id)
	{
		$url = 'http://' . PRODDB_HOST . '/api/export/' . intval($product_id) . '?app=' . PRODDB_APP_ID;
		$payload = file_get_contents($url);
		$data = decode_payload($payload, PRODDB_APP_KEY);
		
		if ($data['status'] == 1)
		{
			unset($data['status']);
			
			// Fetch images in product_desc, store them locally, and rewrite HTML to link local image copies
			$data['product']['product_desc'] = $this->fetch_html_images($data['product']['product_desc']);
			foreach ($data['localized_versions'] as $key => $version)
			{
				if (!empty($version['product_desc']))
				{
					$data['localized_versions'][$key]['product_desc'] = $this->fetch_html_images($version['product_desc']);
				}
			}
			
			return $data;
		}
		else
		{
			return false;
		}
	}
	
	public function fetch_html_images($html)
	{
		global $ecs;
		
		$doc = new DOMDocument();
		//$html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');
		$html = '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head><body>' . $html . '</body></html>';
		@$doc->loadHTML($html);
		
		$tags = $doc->getElementsByTagName('img');
		
		foreach ($tags as $tag)
		{
			$url = $tag->getAttribute('src');
			
			// Fetch the image if not from yohohongkong.com or from beta
			$url_host = strtolower(parse_url($url, PHP_URL_HOST));
			if ((substr($url_host, -16) != 'yohohongkong.com') || (strpos($url_host, 'beta') !== false))
			{
				$save_path = $this->download_image($url);
				if ($save_path)
				{
					$url = $ecs->url() . $save_path;
				}
				else
				{
					error_log('fetch_html_images: Failed to fetch URL: ' . $url);
				}
			}
			
			$tag->setAttribute('src', $url);
		}
		
		$html = $doc->saveHTML($doc->getElementsByTagName('body')->item(0));
		$html = str_replace(array('<body>', '</body>'), array('',''), $html);
		
		return $html;
	}

	private function download_image($url)
	{
		global $db, $ecs;
		
		// curl cannot handle URL with space, so we encode them first
		$url = str_replace(' ', '%20', $url);
		
		// Downloaded already?
		$sql = "SELECT `image_path` FROM " . $ecs->table('proddb_image_mapping') . "WHERE `url` = '" . addslashes($url) . "'";
		$image_path = $db->getOne($sql);
		if ($image_path)
		{
			return $image_path;
		}
		
		$save_path = 'images/upload/Proddb/';
		
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
		$image_data = curl_exec($ch);
		if($image_data === false)
		{
			error_log('download_image: curl error: ' . curl_error($ch) . ', url = ' . $url);
		}
		curl_close($ch);
		
		$filename = str_replace('.', '', array_sum(explode(' ', microtime()))) . mt_rand(10000,99999);
		$size = getimagesizefromstring($image_data);
		if ($size['mime'] == 'image/jpeg')
		{
			$filename .= '.jpg';
		}
		else if ($size['mime'] == 'image/gif')
		{
			$filename .= '.gif';
		}
		else if ($size['mime'] == 'image/png')
		{
			$filename .= '.png';
		}
		else // Not an image, don't save it and return now
		{
			return '';
		}
		
		$image_path = $save_path . $filename;
		file_put_contents(ROOT_PATH . $image_path, $image_data);
		
		$sql = "REPLACE INTO " . $ecs->table('proddb_image_mapping') .
				"(`url`, `image_path`, `last_fetch`) " .
				"VALUES " .
				"('" . addslashes($url) . "', '" . addslashes($image_path) . "', '" . gmtime() . "')";
		$db->query($sql);
		
		return $image_path;
	}
	
	public function get_product_updates($products)
	{
		$url = 'http://' . PRODDB_HOST . '/api/update?app=' . PRODDB_APP_ID;
		$postdata = array('products' => $products);
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('payload' => base64_encode(encode_payload($postdata, PRODDB_APP_KEY))))); 
		$payload = curl_exec($ch);
		curl_close($ch);
		$data = decode_payload($payload, PRODDB_APP_KEY);
		
		if ($data['status'] == 1)
		{
			$updates = $data['updates'];
			
			// Fetch images in product_desc, store them locally, and rewrite HTML to link local image copies
			foreach ($updates as $key => $update)
			{
				$update['product']['product_desc'] = $this->fetch_html_images($update['product']['product_desc']);
				foreach ($update['localized_versions'] as $k => $version)
				{
					if (!empty($version['product_desc']))
					{
						$update['localized_versions'][$k]['product_desc'] = $this->fetch_html_images($version['product_desc']);
					}
				}
				$updates[$key] = $update;
			}
			
			return $updates;
		}
		else
		{
			return false;
		}
	}
}

$proddb = new ProdDB();

?>