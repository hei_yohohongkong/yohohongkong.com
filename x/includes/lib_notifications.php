<?php

/***
* lib_notifications.php
* by howang 2014-07-09
*
* Admin Notifications for YOHO Hong Kong
***/

if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

function notify_price_changed($goods, $org_price, $new_price)
{
	$data = array(
		'goods_id' => $goods['goods_id'],
		'goods_name' => $goods['goods_name'],
		'goods_sn' => $goods['goods_sn'],
		'org_price' => price_format($org_price, false),
		'new_price' => price_format($new_price, false)
	);
	
	$tpl = get_mail_template('admin_price_changed');
	$subject = $tpl['template_subject'] . ' - ' . $goods['goods_name'] . ' ' . price_format($new_price, false);
	$GLOBALS['smarty']->assign('email', $data);
	$GLOBALS['smarty']->assign('shop_name', $GLOBALS['_CFG']['shop_name']);
	$GLOBALS['smarty']->assign('send_date', local_date($GLOBALS['_CFG']['date_format']));
	$GLOBALS['smarty']->assign('sent_date', local_date($GLOBALS['_CFG']['date_format']));
	$GLOBALS['smarty']->assign('shop_url', $GLOBALS['ecs']->url());
	$content = $GLOBALS['smarty']->fetch('str:' . $tpl['template_content']);
	notify_all_admins($subject, $content, $tpl['is_html']);
}

function notify_goods_stock($goods, $have_stock)
{
	$data = array(
		'goods_id' => $goods['goods_id'],
		'goods_name' => $goods['goods_name'],
		'goods_sn' => $goods['goods_sn']
	);
	
	$tpl = get_mail_template($have_stock ? 'admin_back_in_stock' : 'admin_out_of_stock');
	$subject = $tpl['template_subject'] . ' - ' . $goods['goods_name'];
	$GLOBALS['smarty']->assign('email', $data);
	$GLOBALS['smarty']->assign('shop_name', $GLOBALS['_CFG']['shop_name']);
	$GLOBALS['smarty']->assign('send_date', local_date($GLOBALS['_CFG']['date_format']));
	$GLOBALS['smarty']->assign('sent_date', local_date($GLOBALS['_CFG']['date_format']));
	$GLOBALS['smarty']->assign('shop_url', $GLOBALS['ecs']->url());
	$content = $GLOBALS['smarty']->fetch('str:' . $tpl['template_content']);
	notify_all_admins($subject, $content, $tpl['is_html']);
}

function notify_all_admins($subject, $content, $is_html)
{
	$func = 'notify_all_admins_real';
	$params = base64_encode(json_encode(func_get_args()));
	$calltime = gmtime();
	$hash = substr(sha1($func . $params . $GLOBALS['_CFG']['hash_code'] . $calltime), 34);
	$sig = base64_encode(json_encode(array('t'=>$calltime,'c'=>$hash)));
	
	$api_url = $GLOBALS['ecs']->url() . 'api/howang/admin_notifications.php';
	$post_data = 'func=' . rawurlencode($func) . '&params=' . rawurlencode($params) . '&sig=' . rawurlencode($sig);
	exec('curl -d "' . $post_data . '" ' . $api_url . ' > /dev/null 2>&1 &');
}

function notify_all_admins_real($subject, $content, $is_html)
{
	define('BYPASS_MAIL_LOG', true);
	// Only send to howang on beta website
	if (substr($_SERVER['HTTP_HOST'],0,5) == 'beta.')
	{
		$admin_name = 'Wong Ho Wang';
		$admin_email = 'howang@yohohongkong.com';
	}
	else
	{
		$admin_name = 'All YOHO Hong Kong Staffs';
		$admin_email = 'allyohohongkongstaffs@yohohongkong.com';
	}
	send_mail($admin_name, $admin_email, $subject, $content, $is_html);
}

?>