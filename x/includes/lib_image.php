<?php

if (!defined('IN_ECS')) {
    die('Hacking attempt');
}

require_once ROOT_PATH  . '/api/includes/config.php';
require_once ROOT_PATH  . '/includes/tinify/lib/Tinify/Exception.php';
require_once ROOT_PATH  . '/includes/tinify/lib/Tinify/ResultMeta.php';
require_once ROOT_PATH  . '/includes/tinify/lib/Tinify/Result.php';
require_once ROOT_PATH  . '/includes/tinify/lib/Tinify/Source.php';
require_once ROOT_PATH  . '/includes/tinify/lib/Tinify/Client.php';
require_once ROOT_PATH  . '/includes/tinify/lib/Tinify.php';

function get_tiny_usages($api_key, $year, $month)
{
    global $db, $ecs, $_CFG;
    
    $sql = "SELECT * 
        FROM " .$ecs->table('tiny_usages'). "
        WHERE api_key = '".$api_key."'
        AND year = '".$year."'
        AND month = '".$month."' ";
    
    $tinyUsage = $db->getRow($sql);
    
    if (empty($tinyUsage)) {
        $sql = 'INSERT INTO ' . $ecs->table('tiny_usages') . ' (year, month, api_key) ' .
              " VALUES ( $year, '" . $month . "', '" . $api_key . "')";
        $db->query($sql);
        $tiny_usage_id = $db->Insert_ID();
        $used_total = 0;
    } else {
        $used_total = $tinyUsage['used_total'];
    }

    \Tinify\setKey($api_key);
    \Tinify\validate();
    $compressionsThisMonth = \Tinify\compressionCount();
    
    if ($compressionsThisMonth > $used_total) {
        $sql = "update " . $ecs->table('tiny_usages') . " set used_total = '".$compressionsThisMonth."' where api_key = '".$api_key."' and year = '".$year."' and month = '".$month."' ";
        $db->query($sql);
    }

    return $compressionsThisMonth;
}

function image_compression($image_path, $save_path = null, $wait_to_finish = false, $use_imagemagick_jpg_compression = false)
{
    global $db, $ecs, $_CFG;
    usleep(0.01); //1 (S)

    if ($save_path == null){
        $save_path = $image_path;
    }

    // use_imagemagick_compression only for jpg format
    if ($use_imagemagick_jpg_compression == true){
        $path_parts = pathinfo($image_path);
        $file_name = $path_parts['filename'];
        $extension = $path_parts['extension'];

        if (strtolower($extension) == 'jpg') {
         // use imagemacgick lib to compress
         exec('/usr/bin/convert -quality 90% '.escapeshellarg($image_path) . ' ' .escapeshellarg($save_path));
        }
        return true;
    }

    // check usage
    $current_year = local_date('Y');
    $current_month = local_date('m');
    
    $tiny_used_total_1 = get_tiny_usages($_CFG['tiny_api_key_1'], $current_year, $current_month);
    if (!empty($_CFG['tiny_api_key_2'])) {
        $tiny_used_total_2 = get_tiny_usages($_CFG['tiny_api_key_2'], $current_year, $current_month);
    }

    // if account 1 reaches the limit, try to use second account
    $default_limit = 500;
    if (!empty($_CFG['tiny_api_key_2']) && $tiny_used_total_1 >= $default_limit && $tiny_used_total_2 < $default_limit) {
        $api_key = $_CFG['tiny_api_key_2'];
        $used_total = $tiny_used_total_2;
    } else {
        $api_key = $_CFG['tiny_api_key_1'];
        $used_total = $tiny_used_total_1;
    }

    // echo 'used_total:'.$used_total.'<br>';
    // echo 'tiny_limit:'.$_CFG['tiny_limit'].'<br>';
    // echo 'api key:' .$api_key.'<br>';

    if ($_CFG['tiny_enable'] && $used_total <= $_CFG['tiny_limit']) {
        // $sql = 'update ' . $ecs->table('tiny_usages') . ' set used_total = used_total +1 ' .
        //    " where api_key = '".$api_key."' and year = '".$current_year."' and month = '".$current_month."' ";
        // $db->query($sql);
  
        if (empty($image_path)) {
            return false;
        }
        if (empty($save_path)) {
            $save_path = $image_path;
        }

        if (!$wait_to_finish) {
            $data = array('api_key' => $api_key,
                          'image_path' => $image_path,
                          'save_path' => $save_path,
                          'auth_image_compression_token_1' => AUTH_IMAGE_COMPRESSION_TOKEN_1,
                          'auth_image_compression_token_2' => AUTH_IMAGE_COMPRESSION_TOKEN_2,
                    );
            $ch = curl_init();
            if (strpos($_SERVER['SERVER_NAME'], 'beta.') !== false) {
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Host: beta.yohohongkong.com'));
                curl_setopt($ch, CURLOPT_URL, "http://127.0.0.1/api/imageCompress.php");
            } else {
                curl_setopt($ch, CURLOPT_URL, "https://www.yohohongkong.com/api/imageCompress.php");
            }
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt(
                $ch,
                CURLOPT_POSTFIELDS,
                http_build_query($data)
            );
            // receive server response ...
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_USERAGENT, 'api');
            curl_setopt($ch, CURLOPT_TIMEOUT_MS, 600);
            curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            //curl_setopt($ch, CURLOPT_TIMEOUT, 1);
            curl_setopt($ch, CURLOPT_FORBID_REUSE, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
            curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 10); 
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_exec($ch);
            curl_close($ch);

            //exit;
        } else {
            send_to_tiny_compress($api_key,$image_path,$save_path);
        }
    }
}

function send_to_tiny_compress($api_key,$image_path,$save_path){
    global $db, $ecs, $_CFG;

    // status 0  待處理
    // status 1  處理中
    // status 2  完成
    // status 3  錯誤

    $current_year = local_date('Y');
    $current_month = local_date('m');
    
    try {
        if (strpos($_SERVER['SERVER_NAME'], 'beta.') !== false) {
            // log
            $sql = "INSERT INTO " . $ecs->table('tiny_usages_log') . ' (api_key, year, month, image_path, save_path, add_time, status) ' .
                   " VALUES ( '".$api_key."', '".$current_year."', '".$current_month."', '".$image_path."', '".$save_path."', '".gmtime()."', '1' )";
            $db->query($sql);
            $new_insert_id = $db->Insert_ID();

            \Tinify\setKey($api_key);
            \Tinify\validate();
            $source = \Tinify\fromFile($image_path);
            $source->toFile($save_path);
            $sql = "update ".$ecs->table('tiny_usages_log')." set status = '2' where log_id = '".$new_insert_id."' ";
            $db->query($sql);
        } else {
            // prevent from caching by CDN, we make a temp image instead.
            $path_parts = pathinfo($image_path);
            $file_name = $path_parts['filename'];
            $extension = $path_parts['extension'];
            $new_image_path = $path_parts['dirname'].'/'.$file_name.'_cdn.'.$path_parts['extension'];
            copy($image_path,$new_image_path);
            $new_image_path_http =  str_replace(ROOT_PATH,"https://www.yohohongkong.com/",$new_image_path);

            // log     
            $sql = "INSERT INTO " . $ecs->table('tiny_usages_log') . ' (api_key, year, month, image_path, save_path, add_time, status) ' .
                   " VALUES ( '".$api_key."', '".$current_year."', '".$current_month."', '".$new_image_path_http."', '".$save_path."', '".gmtime()."', '1' )";
            $db->query($sql);
            $new_insert_id = $db->Insert_ID();
            
            // for testing;
            //$new_image_path_http = 'http://www.yohohongkong.com/data/brandlogo/1530068923572728767.png';
            
            \Tinify\setKey($api_key);
            \Tinify\validate();
            $source = \Tinify\fromUrl($new_image_path_http);
            $source->toFile($save_path);

            @unlink($new_image_path);

            $sql = "update ".$ecs->table('tiny_usages_log')." set status = '2' where log_id = '".$new_insert_id."' ";
            $db->query($sql);
        }
    } catch(\Tinify\AccountException $e) {
         // Verify your API key and account limit.
        $sql = "update ".$ecs->table('tiny_usages_log')." set  status = '3',remark = '". 'Verify your API key and account limit- ' . mysql_escape_string($e->getMessage()) ."' where log_id = '".$new_insert_id."' ";
        $db->query($sql);
    } catch(\Tinify\ClientException $e) {
        // Check your source image and request options.
        $sql = "update ".$ecs->table('tiny_usages_log')." set status = '3', remark = '". 'Check your source image and request options- ' . mysql_escape_string($e->getMessage()) ."' where log_id = '".$new_insert_id."' ";
        $db->query($sql);  
    } catch(\Tinify\ServerException $e) {
        // Temporary issue with the Tinify API.
        $sql = "update ".$ecs->table('tiny_usages_log')." set status = '3', remark = '". 'Temporary issue with the Tinify API- ' . mysql_escape_string($e->getMessage()) ."' where log_id = '".$new_insert_id."' ";
        $db->query($sql);
    } catch(\Tinify\ConnectionException $e) {
        // A network connection error occurred.
        $sql = "update ".$ecs->table('tiny_usages_log')." set status = '3', remark = '". 'A network connection error occurred- ' . mysql_escape_string($e->getMessage()) ."' where log_id = '".$new_insert_id."' ";
        $db->query($sql);
    } catch(Exception $e) {
        // Something else went wrong, unrelated to the Tinify API.
        $sql = "update ".$ecs->table('tiny_usages_log')." set status = '3', remark = '". 'Something else went wrong, unrelated to the Tinify API- ' . mysql_escape_string($e->getMessage()) ."' where log_id = '".$new_insert_id."' ";
        $db->query($sql);
    }

    $sql = "select used_total from ".$ecs->table('tiny_usages')." where api_key = '".$api_key."' and year = '".$current_year."' and month = '".$current_month."' ";
    $total = $db->getOne($sql);
    $compressionsThisMonth = \Tinify\compressionCount();

    if ($compressionsThisMonth > $total){
        $sql = "update " . $ecs->table('tiny_usages') . " set used_total = '".$compressionsThisMonth."' where api_key = '".$api_key."' and year = '".$current_year."' and month = '".$current_month."' ";
        $db->query($sql);
    }

    return true;
}
