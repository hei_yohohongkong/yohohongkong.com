<?php

/**
 * ECSHOP Google sitemap 类
 * ============================================================================
 * 版权所有 2005-2010 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liuhui $
 * $Id: cls_google_sitemap.php 17063 2010-03-25 06:35:46Z liuhui $
*/

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

class google_sitemap
{
    const SITEMAP_SECTION_BRAND='brand';
    const SITEMAP_SECTION_CAT='cat';
    const SITEMAP_SECTION_PRODUCT='product';
    const SITEMAP_SECTION_ARTICLECAT='article_cat';
    const SITEMAP_SECTION_ARTICLE='article';
    const SITEMAP_SECTION_TOPIC='topic';
    const SITEMAP_SECTION_TAG='tag';
    const SITEMAP_SECTION_OTHERS='other';

    var $header = "<\x3Fxml version=\"1.0\" encoding=\"UTF-8\"\x3F>\n\t<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" xmlns:xhtml=\"http://www.w3.org/1999/xhtml\" >";
    var $charset = "UTF-8";
    var $footer = "\t</urlset>\n";
    var $items = array();

    /**
     * 增加一个新的子项
     *@access   public
     *@param    google_sitemap  item    $new_item
     */
    function add_item($key, $new_item)
    {
        if(!isset($this->items[$key])) $this->items[$key]=array();
        $this->items[$key][] = $new_item;
    }

    /**
     * 生成XML文档
     *@access    public
     *@param     string  $file_name  如果提供了文件名则生成文件，否则返回字符串.
     *@return [void|string]
     */
    /*
    <?xml version="1.0" encoding="UTF-8"?>
   <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
   <sitemap>
      <loc>http://www.example.com/sitemap1.xml.gz</loc>
      <lastmod>2004-10-01T18:23:17+00:00</lastmod>
   </sitemap>
   <sitemap>
      <loc>http://www.example.com/sitemap2.xml.gz</loc>
      <lastmod>2005-01-01</lastmod>
   </sitemap>
   </sitemapindex>
    */
    function build( $file_name = null, $hostUrl )
    {
        try{
            $smid=1;
            foreach($this->items as $section=>$sectionItems){
                $map = $this->header . "\n";
                $sectionFileName = str_replace(".xml",$smid.".xml",$file_name);
                foreach ($sectionItems AS $item)
                {
                    $item->loc = htmlentities($item->loc, ENT_QUOTES);
                    $map .= "\t\t<url>\n\t\t\t<loc>$item->loc</loc>\n";

                    // lastmod
                    if ( !empty( $item->lastmod ) )
                        $map .= "\t\t\t<lastmod>$item->lastmod</lastmod>\n";

                    // changefreq
                    if ( !empty( $item->changefreq ) )
                        $map .= "\t\t\t<changefreq>$item->changefreq</changefreq>\n";

                    // priority
                    if ( !empty( $item->priority ) )
                        $map .= "\t\t\t<priority>$item->priority</priority>\n";
                    $map .= "\t\t\t".'<xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="'.str_replace('//beta', '//m.beta',str_replace('//www', '//m', $item->loc)).'" />'."\n";
                    $map .= "\t\t\t".'<xhtml:link rel="alternate" hreflang="en" href="'.str_replace('yohohongkong.com/', 'yohohongkong.com/en_us/', $item->loc).'"/>'."\n";
                    $map .= "\t\t\t".'<xhtml:link rel="alternate" hreflang="zh-Hans" href="'.str_replace('yohohongkong.com/', 'yohohongkong.com/zh_cn/', $item->loc).'"/>'."\n";
                    $map .= "\t\t</url>\n";
                }
                $map .= $this->footer . "\n";

                if (!is_null($sectionFileName))
                {
                    if(!file_put_contents($sectionFileName, $map)){
                        throw new Exception('Error while creating file'.$sectionFileName,501);
                    }
                }
                else
                {
                    return $map;
                }

                $smid++;
            }

            //construct sitemap index
            $map='<?xml version="1.0" encoding="UTF-8"?>'."\n";
            $map.='<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'."\n";
            $smid=1;
            foreach($this->items as $section=>$sectionItems){
                $map.="\t\t".'<sitemap>'."\n";
                $map.="\t\t\t".'<loc>'.$hostUrl.str_replace('../', '', str_replace('.xml', $smid.'.xml', basename($file_name))).'</loc>'."\n";
                $map.="\t\t\t".'<lastmod>'.date('Y-m-d').'</lastmod>'."\n";
                $map.="\t\t".'</sitemap>';
                $smid++;
            }
            $map.='</sitemapindex>';
            if(!file_put_contents($file_name, $map)){
                throw new Exception(401,'Error while creating file'.$file_name);
            }
        }catch(Exception $e){
            //revert sitemap? copy to new?
            return $e->getMessage();
        }

        return true;
    }
}

class google_sitemap_item
{
    /**
     *@access   public
     *@param    string  $loc        位置
     *@param    string  $lastmod    日期格式 YYYY-MM-DD
     *@param    string  $changefreq 更新频率的单位 (always, hourly, daily, weekly, monthly, yearly, never)
     *@param    string  $priority   更新频率 0-1
     */
    function google_sitemap_item($loc, $lastmod = '', $changefreq = '', $priority = '')
    {
        $this->loc = $loc;
        $this->lastmod = $lastmod;
        $this->changefreq = $changefreq;
        $this->priority = $priority;
    }
}

?>