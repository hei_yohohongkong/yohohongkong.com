<?php

/**
 * Anthony
 * 20170410
 */

namespace Yoho\cms\Model;

use Yoho\cms\Controller;

if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

class Quotation extends YohoBaseModel {

	const fillable = [
			'quotation_sn',
			'company_name',
			'contact_name',
			'contact_email',
			'contact_tel',
			'shipping_id',
			'shipping_fee',
			'pay_id',
			'pay_fee',
			'quotation_fee',
			'created_at'];

	public $data;
	protected $id, $fillable;
	private $tablename = 'quotation_info';

	public function getFillable() {
		return $this->fillable;
	}

	public static function getTableName() {
		return $GLOBALS['ecs']->table('quotation_info');
	}

	public function __construct($qId, $data = null) {
		parent::__construct();
		$qId = intval($qId);
		$this->fillable = self::fillable;
		$this->tablename = $this->ecs->table($this->tablename);

		if ($qId <= 0 && $data != null)
		{
			return false;
		} else
		{
			$q = "SELECT * FROM " . $this->tablename . " WHERE quotation_id=" . $qId;
			$res = $this->db->query($q);
			if (mysql_num_rows($res) > 0)
			{
				$res = mysql_fetch_assoc($res);
				$this->id = intval($res['quotation_id']);
				$this->_fetchData();
			} else
			{
				return false;
			}
		}
	}

	public function getId() {
		return $this->id;
	}

	private function _fetchData() {
		if (intval($this->id) <= 0)
			return false;

		$q = "SELECT " . implode(',', $this->fillable) . " FROM " . $this->tablename . " WHERE quotation_id=" . $this->id;
		$res = mysql_fetch_assoc($this->db->query($q));

		foreach ($res as $k => $v) {
			$this->data[$k] = $v;
		}

		if (count($this->data) > 0)
			return true;
		else
			return false;
	}

}
