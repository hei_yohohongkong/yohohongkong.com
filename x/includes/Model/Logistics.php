<?php
/**
* Anthony@yoho
* 20171009
*/
namespace Yoho\cms\Model;
use Yoho\cms\Controller;

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

class Logistics extends YohoBaseModel{
	const fillable=[
        'logistics_code',
        'logistics_name',
        'logistics_desc',
        'logistics_url',
        'logistics_logo',
        'invoice_template',
		'enabled',
		'created_at'
    ];

	public $data;

	private $id,$tablename='logistics';

	public function getFillable(){
		return $this->fillable;
	}

	public static function getTableName(){
		return $GLOBALS['ecs']->table('logistics');
	}

	public function __construct($id=null,$data=null){
		parent::__construct();

		$id = intval($id);
		$this->fillable=self::fillable;
		$this->tablename=$this->ecs->table('logistics');

		if($id<=0){
			if(is_array($data) && count($data)>0){
				if($this->_create($data))
					$id = $this->db->Insert_ID();
				else
					return false;
			}else{
				return false;
			}
		} else {
            $q = "SELECT * FROM " . $this->tablename . " WHERE logistics_id = '" . $id . "'" ;
            $res = $this->db->query($q);
            if (mysql_num_rows($res) > 0)
            {
                $res = mysql_fetch_assoc($res);
                $this->id = intval($res['logistics_id']);
                $this->_fetchData();
            } else
            {
                return false;
            }
        }


	}

	private function _fetchData() {
		if (intval($this->id) <= 0)
			return false;
		$q = "SELECT " . implode(',', $this->fillable) . " FROM " . $this->tablename . " WHERE logistics_id=" . $this->id;
		$res = mysql_fetch_assoc($this->db->query($q));

		foreach ($res as $k => $v) {
            $this->data[$k] = $v;
            if($k == 'invoice_template') {
                $invoice_template = $v;
                $length           = strlen($invoice_template);
                $first_word       = (strlen($invoice_template) > 2)?substr($invoice_template, 0, 2):'';
                $this->data['template_length'] = $length;
                $this->data['first_word']      = $first_word;
            }
		}
		if (count($this->data) > 0)
			return true;
		else
			return false;
	}

	private function _create($data)
    {
        if(!isset($data['logistics_name'])){
            return false;
        }
		
        $sql = "INSERT INTO " . $this->getTableName() .
               " ( ";
        $last_key = end(array_keys($data));
        foreach ($data as $key => $value) {
            $sql .=" ".$key." ";
            if ($key != $last_key){
                $sql .=", ";
            }
        }
        $sql .= ") VALUE ( ";
        foreach ($data as $key => $value) {
            if(is_string($value)) $value = "'".$value."'";
            $sql .=" ".$value." ";
            if ($key != $last_key){
                $sql .=", ";
            }
        }
		$sql .= ")";
        return ($this->db->query($sql));
    }

    public function update($data)
    {
        $sql = "UPDATE " . $this->getTableName() .
               " SET ";

        $last_key = end(array_keys($data));
        foreach ($data as $key => $value) {
            if(is_string($value)) $value = "'".$value."'";
            $sql .=" ".$key." = ".$value." ";
            if ($key != $last_key){
                $sql .=", ";
            }
        }
        $sql .= " WHERE logistics_id = ".$this->id." LIMIT 1";
        return ($this->db->query($sql));
	}

	public function getId()
    {
		return $this->id;
	}
}