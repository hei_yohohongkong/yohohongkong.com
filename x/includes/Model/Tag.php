<?php

/***
* Tag.php
* by Anthony 20170628
*
***/

namespace Yoho\cms\Model;

if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}
require_once(ROOT_PATH . 'core/lib_mobile.php');

class Tag extends YohoBaseModel{

	const fillable = [
            'user_id',
            'goods_id',
			'cat_id',
			'brand_id',
			'article_id',
            'tag_words'
        ];

	public $data;
	protected $id, $fillable;
	private $tablename = 'tag';

	public function getFillable()
    {
		return $this->fillable;
	}

	public static function getTableName()
    {
		return $GLOBALS['ecs']->table('tag');
	}

	public function __construct($id, $data = null)
    {
		parent::__construct();
		$id = intval($id);
		$this->fillable = self::fillable;
		$this->tablename = $this->ecs->table('tag');

        if(empty($id)){
            if(is_array($data) && count($data) > 0){
                if($this->_create($data))
                {
                    $id = $this->db->Insert_ID();
                    $this->id = $id;
                }
                else
                return false;
            }else{
                return false;
            }

		} else
		{
			$q = "SELECT * FROM " . $this->tablename . " WHERE tag_id =" . $id;
			$res = $this->db->query($q);
			if (mysql_num_rows($res) > 0)
			{
				$res = mysql_fetch_assoc($res);
				$this->id = intval($res['tag_id']);
				$this->_fetchData();
			} else
			{
				return false;
			}
		}
	}

	public function getId()
    {
		return $this->id;
	}

	private function _fetchData()
    {
		if (intval($this->id) <= 0)
			return false;
		$q = "SELECT " . implode(',', $this->fillable) . " FROM " . $this->tablename . " WHERE tag_id =" . $this->id;
		$res = mysql_fetch_assoc($this->db->query($q));
		foreach ($res as $k => $v) {
			$this->data[$k] = $v;
		}

		if (count($this->data) > 0)
			return true;
		else
			return false;
	}

    private function _create($data)
    {
        $sql = "INSERT INTO " . $this->getTableName() .
               " ( ";
        $last_key = end(array_keys($data));
        foreach ($data as $key => $value) {
            $sql .=" ".$key." ";
            if ($key != $last_key){
                $sql .=", ";
            }
        }
        $sql .= ") VALUE ( ";
        foreach ($data as $key => $value) {
            if(is_string($value))
            {
                // $value = mysql_real_escape_string($value);
                $value = "'".$value."'";
            }
            $sql .=" ".$value." ";
            if ($key != $last_key){
                $sql .=", ";
            }
        }
        $sql .= ")";
        return ($this->db->query($sql));
    }

    public function update($data)
    {
        $sql = "UPDATE " . $this->getTableName() .
               " SET ";

        $last_key = end(array_keys($data));
        foreach ($data as $key => $value) {
            if(is_string($value))
            {
                // $value = mysql_real_escape_string($value);
                $value = "'".$value."'";
            }
            $sql .=" ".$key." = ".$value." ";
            if ($key != $last_key){
                $sql .=", ";
            }
        }
        $sql .= " WHERE tag_id = '$this->id' LIMIT 1";
        return ($this->db->query($sql));
    }
}
