<?php

/***
* cls_base.php
* by MichaelHui 20170328
*
* Yoho base model
***/

namespace Yoho\cms\Model;

if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}
require_once(ROOT_PATH . 'core/lib_mobile.php');

class YohoBaseModel{
	protected $db,$ecs,$memcache;
	protected $table; //table name
	protected $fields = array();  //fields list
    protected $filter = array();
    protected $fillable = [];
    public $data = [];

	public function __construct($tableName = null, $id = 0){
		global $db,$ecs;
		$this->db = $db;
		$this->ecs = $ecs;
		$this->memcache = new \Memcached();
        $this->memcache->addServer('localhost',11211);
        if(!empty($tableName)) {
            $this->table = (strpos($tableName, 'actg_') !== false) ? $tableName : $ecs->table($tableName);
            $this->getFields();

            foreach($this->fields as $field) {
                if(!in_array($field, $this->filter) && $field != $this->fields['_pk']) $this->fillable[] = $field;
            }
            if(!empty($id)) $this->data = $this->selectAllByKey($id);
        }
	}

	public function genCacheKey($params){
		return 'yoho-m-'.implode('-', $params);
	}

	/**
     * Get the list of table fields
     *
     */
	private function getFields()
	{

        $sql = "DESC ". $this->table;

        $result = $this->db->getAll($sql);

        foreach ($result as $v) {

            $this->fields[] = $v['Field'];
            if ($v['Key'] == 'PRI') {
                // If there is PK, save it in $pk
                $pk = $v['Field'];
            }
        }

        // If there is PK, add it into fields list
        if (isset($pk)) {
            $this->fields['_pk'] = $pk;
        }
	}

	/**
     * Insert records
     * @access public
     * @param $list array associative array
     * @return mixed If succeed return inserted record id, else return false
     */
	public function insert($list)
	{

        $field_list = '';  //field list string
        $value_list = '';  //value list string
        foreach ($list as $k => $v) {

            if (in_array($k, $this->fields)) {
				$v = $this->model_escape_string($v);
                $field_list .= "`".$k."`" . ',';
                $value_list .= "'".$v."'" . ',';
            }

        }

        // Trim the comma on the right
        $field_list = rtrim($field_list,',');
        $value_list = rtrim($value_list,',');

        // Construct sql statement
        $sql = "INSERT INTO {$this->table} ({$field_list}) VALUES ($value_list)";
        if ($this->db->query($sql)) {
            // Insert succeed, return the last record’s id
            return $this->db->Insert_ID();
            //return true;
        } else {
            // Insert fail, return false
            return false;
        }
	}

	/**
     * Update records
     * @access public
     * @param $list array associative array needs to be updated
     * @return mixed If succeed return the count of affected rows, else return false
     */
	public function update($list, $pk_id = null, $where = 0)
	{

        $uplist = ''; //update fields
        if($pk_id) {
            $list[$this->fields['_pk']] = $pk_id;
        }
        if($this->fields['last_update']) {
            $list['last_update'] = gmtime();
        }
        foreach ($list as $k => $v) {

            if (in_array($k,$this->fillable)) {
                $v = $this->model_escape_string($v);
                $uplist .= "`$k`='$v'".",";
            } else if ($k == $this->fields['_pk'] && $where == 0) {
                // If it’s PK, construct where condition
                $where = "`$k`=$v";
            }
        }
        // Trim comma on the right of update list
        $uplist = rtrim($uplist,',');
        // Construct SQL statement
		$sql = "UPDATE {$this->table} SET {$uplist} WHERE {$where}";

        if ($this->db->query($sql)) {
            // If succeed, return the count of affected rows
            if ($rows = $this->db->affected_rows()) {
                // Has count of affected rows
                return ($rows);
            } else {
                // No count of affected rows, hence no update operation
                return (true);
            }
        } else {
            // If fail, return false
            return false;
        }
    }

    /**
     * Delete records
     * @access public
     * @param $pk mixed could be an int or an array
     * @return mixed If succeed, return the count of deleted records, if fail, return false
     */
	public function delete($pk)
	{

		$where = 0; //condition string
        //Check if $pk is a single value or array, and construct where condition accordingly
        if (is_array($pk)) {
            // array
            $where = "`{$this->fields['_pk']}` in (".implode(',', $pk).")";
        } else {
            // single value
            $where = "`{$this->fields['_pk']}`=$pk";
        }

        // Construct SQL statement
		$sql = "DELETE FROM {$this->table} WHERE $where";

        if ($this->db->query($sql)) {
            // If succeed, return the count of affected rows
            if ($rows = $this->db->affected_rows()) {
                // Has count of affected rows
                return $rows;
            } else {
                // No count of affected rows, hence no delete operation
                return false;
            }
        } else {
            // If fail, return false
            return false;
        }
    }

    /**
     * Get info based on PK
     * @param $pk int Primary Key
     * @return array an array of single record
     */
	public function selectAllByKey($pk)
	{
        $sql = "select * from {$this->table} where `{$this->fields['_pk']}`=$pk";
        return $this->db->getRow($sql);
    }

    public function getData($need_filter = false)
    {
        $data = $this->data;
        if($need_filter) {
            $data = array_diff_key($data, array_flip($this->fillable));
        }

        return $data;
    }

    /**
     * Get the count of all records
     *
     */
	public function total()
	{
        $sql = "select count(*) from {$this->table}";
        return $this->db->getOne($sql);
    }

    /**
     * Get info of pagination
     * @param $offset int offset value
     * @param $limit int number of records of each fetch
     * @param $where string where condition,default is empty
     */
    public function pageRows($offset, $limit,$where = ''){

        if (empty($where)){
            $sql = "select * from {$this->table} limit $offset, $limit";
        } else {
            $sql = "select * from {$this->table}  where $where limit $offset, $limit";
        }
        return $this->db->getAll($sql);
	}

	public function model_escape_string($value)
	{
		// Add escape
		// case 1: "a\'s" -> "a\'s"
		// case 2: "a's"  -> "a\'s"
		if(is_string($value))
        {
            // If value have escape, remove first(magic_quotes_gpc is on, so if using html<input>: ' change to \', but other cases just only: ' to ' ).
            $value = stripslashes($value);
            // All value add escape if need.
            $value = mysql_real_escape_string($value);
		}

		return $value;
    }

    public function setFillable($fillable)
    {
        $this->fillable = $fillable;
    }

    public function getFillable()
    {
		return $this->fillable;
    }

    public function getPkey()//PRIMARY KEY
    {
		return $this->fields['_pk'];
    }
}



