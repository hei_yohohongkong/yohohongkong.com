<?php
# @Date:   2017-05-24T10:50:00+08:00
# @Filename: Payment.php
# @Last modified time: 2017-06-02T15:21:03+08:00

namespace Yoho\cms\Model;

use Yoho\cms\Controller;

if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

class Payment extends YohoBaseModel
{

    const fillable = [
        'pay_code',
        'pay_name',
        'display_name',
        'pay_fee',
        'pay_desc',
        'pay_order',
        'pay_config',
        'enabled',
        'is_cod',
        'is_online',
        'is_pos',
        'is_online_pay',
        'is_debug_mode',
        'platform',
        'payment_logo'
    ];

	public $data;
	protected $id, $fillable, $pay_code;
	private $tablename = 'payment';

	public function getFillable()
    {
		return $this->fillable;
	}

	public static function getTableName()
    {
		return $GLOBALS['ecs']->table('payment');
	}

    public function __construct($pay_code, $data = null)
    {
        parent::__construct();
        $this->fillable = self::fillable;
        $this->tablename = $this->ecs->table($this->tablename);
        
        if(!isset($pay_code) && $pay_code == ''){
            if(is_array($data) && count($data) > 0){
                if($this->_create($data))
                $id = $this->db->Insert_ID();
                else
                return false;
            }else{
                return false;
            }
        } else
        {
            $this->pay_code = $pay_code;
            $q = "SELECT * FROM " . $this->tablename . " WHERE pay_code = '" . $pay_code . "'" ;
            $res = $this->db->query($q);
            if (mysql_num_rows($res) > 0)
            {
                $res = mysql_fetch_assoc($res);
                $this->id = intval($res['pay_id']);
                $this->_fetchData();
            } else
            {
                return false;
            }
        }
    }

	public function getId()
    {
		return $this->id;
	}

	private function _fetchData()
    {
		if (intval($this->id) <= 0)
			return false;

		$q = "SELECT " . implode(',', $this->fillable) . " FROM " . $this->tablename . " WHERE pay_id =" . $this->id;
		$res = mysql_fetch_assoc($this->db->query($q));

		foreach ($res as $k => $v) {
			$this->data[$k] = $v;
		}

		if (count($this->data) > 0)
			return true;
		else
			return false;
	}
    
    private function _create($data)
    {
        if(!isset($data['pay_code'])){
            return false;
        }

        $sql = "INSERT INTO " . $this->getTableName() .
               " ( ";
        $last_key = end(array_keys($data));
        foreach ($data as $key => $value) {
            $sql .=" ".$key." ";
            if ($key != $last_key){
                $sql .=", ";
            }
        }
        $sql .= ") VALUE ( ";
        foreach ($data as $key => $value) {
            if(is_string($value)) $value = "'".$value."'";
            $sql .=" ".$value." ";
            if ($key != $last_key){
                $sql .=", ";
            }
        }
        $sql .= ")";
    
        return ($this->db->query($sql));
    }
    
    public function update($data)
    {
        $sql = "UPDATE " . $this->getTableName() .
               " SET ";

        $last_key = end(array_keys($data));
        foreach ($data as $key => $value) {
            if(is_string($value)) $value = "'".$value."'";
            $sql .=" ".$key." = ".$value." ";
            if ($key != $last_key){
                $sql .=", ";
            }
        }
        $sql .= " WHERE pay_code = '$this->pay_code' LIMIT 1";
        return ($this->db->query($sql));
    }

    public function enablePayment()
    {
        $sql = "UPDATE " . $this->getTableName() .
               "SET enabled = 1 " .
               "WHERE pay_code = '$this->pay_code' LIMIT 1";
        return ($this->db->query($sql));
    }
    
    public function disablePayment()
    {
        $sql = "UPDATE " . $this->getTableName() .
               "SET enabled = 0 " .
               "WHERE pay_code = '$this->pay_code' LIMIT 1";
        return ($this->db->query($sql));
    }
    
    public function isenable()
    {
        if(isset($this->data['enabled'])) return $this->data['enabled'];
        else return false;
    }

}
