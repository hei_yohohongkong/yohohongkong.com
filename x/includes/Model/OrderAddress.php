<?php
/**
* Anthony@yoho
* 20180110
*/
namespace Yoho\cms\Model;
use Yoho\cms\Controller;

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

class OrderAddress extends YohoBaseModel{

	public $data;

    private $id, $field = [];
    
    private static $tablename='order_address';

    const update_fillable=[
        'consignee',
        'country',
        'province',
        'city',
		'district',
		'address',
        'zipcode',
        'sign_building',
        'street_name',
        'street_num',
        'locality',
        'administrative_area',
        'floor',
        'block',
        'phase',
        'room',
        'hk_area',
        'auto_break',
        'staying_time',
        'address_type',
        'type_code',
        'dest_type',
        'lift',
        'area_type'
    ];

	public static function getTableName(){
		return $GLOBALS['ecs']->table(self::$tablename);
	}

	public function __construct($id=null,$data=null){
		parent::__construct();

		$id = intval($id);

		if($id<=0){
			if(is_array($data) && count($data)>0){
				if($this->_create($data)){
                    $id = $this->db->Insert_ID();
                    $this->id = $id;
                }else
					return false;
			}else{
				return false;
			}
		} else {
            $q = "SELECT * FROM " . $this->getTableName() . " WHERE order_address_id = '" . $id . "'" ;
            $res = $this->db->query($q);
            if (mysql_num_rows($res) > 0)
            {
                $res = mysql_fetch_assoc($res);
                $this->id = intval($res['order_address_id']);
                $this->_fetchData();
            } else
            {
                return false;
            }
        }
	}

	private function _fetchData() {
		if (intval($this->id) <= 0)
			return false;
		$q = "SELECT * FROM " . $this->getTableName() . " WHERE order_address_id=" . $this->id;
		$res = mysql_fetch_assoc($this->db->query($q));

		foreach ($res as $k => $v) {
			$this->data[$k] = $v;
		}
		if (count($this->data) > 0)
			return true;
		else
			return false;
	}

	private function _create($data)
    {
        if(!isset($data['order_id'])){
            return false;
        }

        return $this->db->autoExecute($this->getTableName(), $data, $mode = 'INSERT');
    }

    public function update($data, $pk_id = null, $where = 0)
    {
        $sql = "UPDATE " . $this->getTableName() .
               " SET ";
        $fill = self::update_fillable;
        $last_key = end(array_keys($data));
        $tmp = [];
        foreach ($data as $key => $value) {
            if(!in_array($key,$fill)) continue;
            if(is_string($value)) $value = "'".$value."'";
            if($value == null) $value = "''";
            $tmp[] = $key."=".$value;
        }
        $sql .= implode(",", $tmp);
        $sql .= " WHERE order_address_id = ".$this->id." LIMIT 1";
        return ($this->db->query($sql));
	}

	public function getId()
    {
		return $this->id;
    }
}