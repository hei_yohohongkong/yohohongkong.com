<?php
# @Author: Anthony
# @Date:   2017-05-16T15:55:22+08:00
# @Filename: Hktvmall.php

namespace Yoho\cms\Model;

use Yoho\cms\Controller;

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

class Hktvmall extends YohoBaseModel {
    
    const fillable = [
        'goods_id',
        'hktv_qty',
        'hktv_price',
        'hktv_rate',
        'hktv_brand',
        'created_at'
    ];
    
    public $data;
    protected $id, $fillable;
    private $tablename = 'hktvmall';
    
    public function getFillable() {
        return $this->fillable;
    }
    
    public function getTableName() {
        return $GLOBALS['ecs']->table($this->tablename);
    }
    
    public function __construct($hId, $data = null) {
        parent::__construct();
        $hId = intval($hId);
        $this->fillable = self::fillable;
        $this->tablename = $this->ecs->table($this->tablename);
        
        if($hId<=0){
            if(is_array($data) && count($data)>0){
                if($this->_create($data))
                $hId = $this->db->Insert_ID();
                else
                return false;
            }else{
                return false;
            }
        } else {
            $q = "SELECT * FROM " . $this->tablename . " WHERE hktv_id=" . $hId;
            $res = $this->db->query($q);
            if (mysql_num_rows($res) > 0)
            {
                $res = mysql_fetch_assoc($res);
                $this->id = intval($res['hktv_id']);
                $this->_fetchData();
            } else
            {
                return false;
            }
        }
    }
    
    public function getId() {
        return $this->id;
    }
    
    public function delHktv(){
        if(!isset($this->id)){
			return false;
		}
        
        $sql = "DELETE FROM "  .$this->tablename. " WHERE " . "hktv_id = $this->id";
        
        return $this->db->query($sql);
    }
    
    private function _fetchData() {
        if (intval($this->id) <= 0)
        return false;
        
        $q = "SELECT " . implode(',', $this->fillable) . " FROM " . $this->tablename . " WHERE hktv_id =" . $this->id;
        $res = mysql_fetch_assoc($this->db->query($q));
        
        foreach ($res as $k => $v) {
            $this->data[$k] = $v;
        }
        
        if (count($this->data) > 0){
            return true;
        } else {
            return false;
        }
        
    }
    
    private function _create($data){
		if(!isset($data['goods_id'])){
			return false;
		}
        // Data handle
        $creator = isset($data['creator']) ? $data['creator'] : $_SESSION['admin_name'];
		$hktv_rate = isset($data['hktv_rate']) ? $data['hktv_rate'] : Yoho\cms\Controller\HktvmallController::DEFAULT_RATE;
        
		$q = "INSERT INTO ".$this->tablename." (goods_id, hktv_price, hktv_rate, hktv_brand, creator, created_at) SELECT ".
        " '".$data['goods_id']."', g.shop_price, '".$hktv_rate."', b.brand_name, '".$creator."', '".gmtime()."' ".
        " FROM " . $this->ecs->table('goods') . "as g ".
        " LEFT JOIN " . $this->ecs->table('brand') . " as b ON b.brand_id = g.brand_id ".
        " WHERE g.goods_id = ".$data['goods_id'];
        
		return $this->db->query($q);
	}

    
}
