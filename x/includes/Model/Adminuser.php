<?php
/**
* MichaelHui@yoho
* 20170328
*/
namespace Yoho\cms\Model;
use Yoho\cms\Controller;

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

class Adminuser extends YohoBaseModel{
	const fillable=[
		'user_name',
		'email',
		'avatar',
		'first_name',
		'last_name',
		'joined_date',
		'emergency_contact',
		'address',
		'mobile',
		'id_number',
		'dob',
		'position',
		'role_id',
		'is_receive_stock_alert',
		'is_disable',
	];

	public $data;
	public $id,$fillable;

	private $tablename='admin_user';

	public function getFillable(){
		return $this->fillable;
	}

	public static function getTableName(){
		return $GLOBALS['ecs']->table('admin_user');
	}

	public function __construct($uId,$data=null){
		parent::__construct();
		$uId = intval($uId);
		$this->fillable=self::fillable;
		$this->tablename=$this->ecs->table($this->tablename);
		
		if($uId<=0 && $data!=null){
			return $this->_create($data);
		}else{
			$q = "SELECT * FROM ".$this->tablename." WHERE user_id=".$uId;
			$res = $this->db->query($q);
			if(mysql_num_rows($res)>0){
				$res = mysql_fetch_assoc($res);
				$this->id=intval($res['user_id']);
				$this->_fetchData();
			}else{
				return false;
			}
		}
	}

	public function getId(){
		return $this->id;
	}

	private function _create($data){
		/* 获取添加日期及密码 */
		$add_time = gmtime();

		$password  = md5($data['password']);
		$role_id = '';
		$action_list = '';
		if (!empty($data['select_role'])){
			$sql = "SELECT action_list,is_autocreate_salesperson FROM " . $this->ecs->table('role') . " WHERE role_id = '".$data['select_role']."'";
			$row = $this->db->getRow($sql);
			$action_list = $row['action_list'];
			$role_id = $data['select_role'];
			$shouldCreateSp=$row['is_autocreate_salesperson'];
		}

		$sql = "SELECT nav_list FROM " . $this->ecs->table('admin_user') . " WHERE action_list = 'all'";
		$row = $this->db->getRow($sql);
		$sql = "INSERT INTO ".$this->ecs->table('admin_user')." (user_name, email, password, add_time, nav_list, action_list, role_id) ".
		"VALUES ('".trim($data['user_name'])."', '".trim($data['email'])."', '$password', '$add_time', '$row[nav_list]', '$action_list', '$role_id')";
		$this->db->query($sql);
		$this->id = $this->db->Insert_ID();
		
		$adminuserController= new Controller\AdminuserController();
		$adminuserController->updateFormData($this->id,$data);

		if($shouldCreateSp==1){
			$spController= new Controller\SalespersonController();
			$this->_fetchData();
			$spController->createFromAdmin($this);
		}
		return true;
	}

	private function _fetchData(){
		if(intval($this->id)<=0)
			return false;

		$q = "SELECT ".implode(',',$this->fillable)." FROM ".$this->tablename." WHERE user_id=".$this->id;
		$res = mysql_fetch_assoc($this->db->query($q));

		foreach($res as $k=>$v){
			$this->data[$k]=$v;
		}

		if(count($this->data)>0)
			return true;
		else
			return false;
	}
}