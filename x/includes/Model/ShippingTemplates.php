<?php
namespace Yoho\cms\Model;

use Yoho\cms\Controller;

if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

class ShippingTemplates extends YohoBaseModel {

	const fillable = [
            'template_name',
            'template_content'
        ];
    
	public $data;
	protected $id, $fillable;
	private $tablename = 'shipping_templates';
    
	public function getFillable()
    {
		return $this->fillable;
	}
    
	public static function getTableName()
    {
		return $GLOBALS['ecs']->table($tablename);
	}
    
	public function __construct($id, $data = null)
    {
		parent::__construct();
		$id = intval($id);
		$this->fillable = self::fillable;
        
        if(empty($id)){
            if(is_array($data) && count($data) > 0){
                if($this->_create($data))
                {
                    $id = $this->db->Insert_ID();
                    $this->id = $id;
                    $sql = "INSERT INTO " . $this->ecs->table($this->tablename . "_lang") . " ( template_id, lang ) VALUE ( $id, 'en_us' )";
                    $this->db->query($sql);
                }
                else
                return false;
            }else{
                return false;
            }
		} else
		{
			$q = "SELECT * FROM " . $this->ecs->table($this->tablename) . " WHERE template_id =" . $id;
			$res = $this->db->query($q);
			if (mysql_num_rows($res) > 0)
			{
				$res = mysql_fetch_assoc($res);
				$this->id = intval($res['template_id']);
				$this->_fetchData();
			} else
			{
				return false;
			}
		}
	}
    
	public function getId() {
		return $this->id;
	}
    
	private function _fetchData() {
		if (intval($this->id) <= 0)
			return false;
		$q = "SELECT " . implode(',', $this->fillable) . " FROM " . $this->ecs->table($this->tablename) . " WHERE template_id =" . $this->id;
		$res = mysql_fetch_assoc($this->db->query($q));
		foreach ($res as $k => $v) {
			$this->data[$k] = $v;
		}
    
		if (count($this->data) > 0)
			return true;
		else
			return false;
	}
    private function _create($data)
    {
        $sql = "INSERT INTO " . $this->ecs->table($this->tablename) .
               " ( ";
        $last_key = end(array_keys($data));
        foreach ($data as $key => $value) {
            $sql .=" ".$key." ";
            if ($key != $last_key){
                $sql .=", ";
            }
        }
        $sql .= ") VALUE ( ";
        foreach ($data as $key => $value) {
            if(is_string($value))
            {
                $value = mysql_real_escape_string($value);
                $value = "'".$value."'";
            }
            $sql .=" ".$value." ";
            if ($key != $last_key){
                $sql .=", ";
            }
        }
        $sql .= ")";
        return ($this->db->query($sql));
    }
    
    function last_key($table, $data, $lang = ""){
        $sql = "UPDATE " . $this->ecs->table($table) .
        " SET ";
        $last_key = end(array_keys($data));
        foreach ($data as $key => $value) {
            if(is_string($value))
            {
                $value = mysql_real_escape_string($value);
                $value = "'".$value."'";
            }
            $sql .=" ".$key." = ".$value." ";
            if ($key != $last_key){
                $sql .=", ";
            }
        }
        if(!strpos($table,'lang')){
            $sql .= " WHERE template_id = '$this->id' LIMIT 1";
        }else{
            $sql .= " WHERE template_id = '$this->id' AND lang = '$lang' LIMIT 1";
        }
        return $sql;
    }

    public function update($data)
    {
        $sql = $this->last_key($this->tablename,$data);
        return ($this->db->query($sql));
    }
    public function update_lang($data)
    {
        $lv = json_decode(stripslashes($data['localized_versions']));
        foreach($lv as $key=>$value){
            $sql = $this->last_key($this->tablename."_lang", (array) $value, $key);
        }
        return ($this->db->query($sql));
    }
}
