<?php

/**
 * Anthony
 * 20170620
 */

namespace Yoho\cms\Model;

use Yoho\cms\Controller;

if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

class AdCampaign extends YohoBaseModel {

	const fillable = [
            'ad_campaign_name',
            'ad_campaign_desc',
			'start_time',
			'end_time',
			'created_at',
            'enabled'
        ];
    
	public $data;
	protected $id, $fillable;
	private $tablename = 'ad_campaign';
    
	public function getFillable()
    {
		return $this->fillable;
	}
    
	public static function getTableName()
    {
		return $GLOBALS['ecs']->table('ad_campaign');
	}
    
	public function __construct($id, $data = null)
    {
		parent::__construct();
		$id = intval($id);
		$this->fillable = self::fillable;
		$this->tablename = $this->ecs->table('ad_campaign');
        
        if(empty($id)){
            if(is_array($data) && count($data) > 0){
                if($this->_create($data))
                {
                    $id = $this->db->Insert_ID();
                    $this->id = $id;
                }
                else
                return false;
            }else{
                return false;
            }

		} else
		{
			$q = "SELECT * FROM " . $this->tablename . " WHERE ad_campaign_id =" . $id;
			$res = $this->db->query($q);
			if (mysql_num_rows($res) > 0)
			{
				$res = mysql_fetch_assoc($res);
				$this->id = intval($res['ad_campaign_id']);
				$this->_fetchData();
			} else
			{
				return false;
			}
		}
	}
    
	public function getId() {
		return $this->id;
	}
    
	private function _fetchData() {
		if (intval($this->id) <= 0)
			return false;
		$q = "SELECT " . implode(',', $this->fillable) . " FROM " . $this->tablename . " WHERE ad_campaign_id =" . $this->id;
		$res = mysql_fetch_assoc($this->db->query($q));
		foreach ($res as $k => $v) {
			$this->data[$k] = $v;
		}
    
		if (count($this->data) > 0)
			return true;
		else
			return false;
	}
    
    private function _create($data)
    {
        $sql = "INSERT INTO " . $this->getTableName() .
               " ( ";
        $last_key = end(array_keys($data));
        foreach ($data as $key => $value) {
            $sql .=" ".$key." ";
            if ($key != $last_key){
                $sql .=", ";
            }
        }
        $sql .= ") VALUE ( ";
        foreach ($data as $key => $value) {
            if(is_string($value))
            {
                $value = mysql_real_escape_string($value);
                $value = "'".$value."'";
            }
            $sql .=" ".$value." ";
            if ($key != $last_key){
                $sql .=", ";
            }
        }
        $sql .= ")";
        return ($this->db->query($sql));
    }
    
    public function update($data)
    {
        $sql = "UPDATE " . $this->getTableName() .
               " SET ";

        $last_key = end(array_keys($data));
        foreach ($data as $key => $value) {
            if(is_string($value))
            {
                $value = mysql_real_escape_string($value);
                $value = "'".$value."'";
            }
            $sql .=" ".$key." = ".$value." ";
            if ($key != $last_key){
                $sql .=", ";
            }
        }
        $sql .= " WHERE ad_campaign_id = '$this->id' LIMIT 1";
        return ($this->db->query($sql));
    }


}
