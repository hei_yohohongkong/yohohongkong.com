<?php
/**
* MichaelHui@yoho
* 20170328
*/
namespace Yoho\cms\Model;
use Yoho\cms\Controller;

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

class Salesagent extends YohoBaseModel{
	const fillable=[
		'name','active'];

	public $data;

	private $id,$tablename='sales_agent',$ratesCacheKey,$ratesCacheKeyAll;

	public function getFillable(){
		return $this->fillable;
	}

	public static function getTableName(){
		return $GLOBALS['ecs']->table('sales_agent');
	}

	public function __construct($saId=null,$data=null){
		parent::__construct();

		$saId = intval($saId);
		$this->fillable=self::fillable;
		$this->tablename=$this->ecs->table($this->tablename);
		
		if($saId<=0){
			if(is_array($data) && count($data)>0){
				if($this->_create($data))
					$saId = $this->db->Insert_ID();
				else
					return false;
			}else{
				return false;
			}
		}

		$q = "SELECT name,active,pay_type FROM ".$this->tablename." WHERE sa_id=".$saId;
		$res = $this->db->getAll($q);

		if(count($res)<=0)
			return false;
		$this->data=$res[0];
		$this->id = $saId;

		$this->ratesCacheKey=$this->genCacheKey(['sales_agent',$this->id,'rates']);
		$this->ratesCacheKeyAll=$this->genCacheKey(['sales_agent',$this->id,'ratesAll']);
		$this->_flushRateCache();
	}

	public function getRates($includeArchived=false){
		$cacheKey=($includeArchived?$this->ratesCacheKeyAll:$this->ratesCacheKey);

		if($this->memcache->get($cacheKey)==false){
			$cond = ($includeArchivedRate==false?" AND active=TRUE ":" ");
			$q = "SELECT sa_rate_id, sa_rate, is_default FROM ".$this->ecs->table('sales_agent_rate')." WHERE sa_id=".$this->id.$cond;
//			$rates=[];
//			foreach($this->db->getAll($q) as $val)
//				$rates[]=floatval($val['sa_rate']);
			$rates = $this->db->getAll($q);
			return $rates;
		}else{
			return $this->memcache->get($cacheKey);
		}
	}

	public function getAllRates(){
			$q = "SELECT sa_rate_id, sa_rate, is_default, active FROM ".$this->ecs->table('sales_agent_rate')." WHERE sa_id=".$this->id." ";
			$rates = $this->db->getAll($q);
			return $rates;
	}

	public function getAgentGoods(){
			$q = "SELECT sag.*, g.goods_sn, g.goods_name, g.shop_price, g.goods_thumb " .
					"FROM ".$this->ecs->table('sales_agent_goods')." AS sag " .
					"LEFT JOIN ".$this->ecs->table('goods')." AS g ON g.goods_id = sag.goods_id " .
					"WHERE sag.sa_id = '" . $this->id . "' " .
					"ORDER BY sag.updated_at DESC, sag.goods_id DESC ";
			$list = $this->db->getAll($q);
			return $list;
	}

	public function addRate($data){
		if(!isset($data['sa_rate']) || !isset($data['rate_active']) || !$this->id)
			return false;

		$rate = $data['sa_rate'];
		$active = $data['rate_active'];
		$is_default = isset($data['$is_default']) ? $data['$is_default'] : 0;

		try{
			$q="INSERT INTO ".$this->ecs->table('sales_agent_rate')." (sa_id,sa_rate,active,is_default,created_at) VALUE (".$this->id.",".$rate.",".$active.", '".$is_default."', '".date('Y-m-d H:i:s')."')";
			if($this->db->query($q,null,true)){
				$this->_flushRateCache();
				return $this->db->Insert_ID();
			}
		}catch(\Exception $e){
			return false;
		}

		return false;
	}

	public function delRate($rate_id){
		$sql = "DELETE FROM ".$this->ecs->table('sales_agent_rate')." WHERE sa_rate_id = '" . $rate_id . "' ";
		$this->db->query($sql,null,true);
	}

	public function updateRate($id, $rate){
		$q = "UPDATE ".$this->ecs->table('sales_agent_rate')." SET sa_rate = '" . $rate . "' WHERE sa_rate_id = '" . $id . "' ";
		if($this->db->query($q)){
			$this->_flushRateCache();
			return true;
		}
		
		return false;
	}

	public function addGoods($arr_item_id, $arr_ag_price){
		$str_data_good_price = "";
		$i = 0;
		foreach ($arr_item_id as $index => $item){
			if ($i != 0){
				$str_data_good_price .= ",";
			}
			$str_data_good_price .= "(".$this->id.",".$item.",".$arr_ag_price[$index].")";
			$i++;
		}
		try{
			$sql = "INSERT INTO ".$this->ecs->table('sales_agent_goods')." (sa_id,goods_id,sa_price) VALUE ".$str_data_good_price;
			//echo "<br>".$sql;
			if($this->db->query($sql,null,true)){
				$this->_flushRateCache();
				return $this->db->Insert_ID();
			}
		}catch(\Exception $e){
			return false;
		}
		return false;
	}

	public function deleteGoods($arr_item_id){
		$str_item_id = implode(',', $arr_item_id);
		$sql = "DELETE FROM ".$this->ecs->table('sales_agent_goods')." WHERE goods_id IN (" . $str_item_id . ") AND sa_id = '" . $this->id . "'";
		$this->db->query($sql,null,true);
	}

	public function updateGoods($arr_item_id, $arr_ag_price){
		$str_data_good_price = "";
		$i = 0;
		foreach ($arr_item_id as $index => $item){
			if ($i != 0){
				$str_data_good_price .= ",";
			}
			$str_data_good_price .= "(".$this->id.",".$item.",".$arr_ag_price[$index].")";
			$i++;
		}

		foreach ($arr_item_id as $index => $item){
			$sql = "UPDATE ".$this->ecs->table('sales_agent_goods')." SET sa_price = '" . $arr_ag_price[$index] . "' WHERE sa_id = '" . $this->id . "' AND goods_id = '" . $item . "' ";
			$this->db->query($sql,null,true);
		}

	}

	public function deactivateRate($rateId){
		if(intval($rateId)<=0 || !$this->id)
			return false;
		$q = "UPDATE ".$this->ecs->table('sales_agent_rate')." SET active=false WHERE active=true AND sa_id=".$this->id."  AND sa_rate_id=".$rateId;
		if($this->db->query($q)){
			$this->_flushRateCache();
			return true;
		}
		
		return false;
	}

	public function activateRate($rateId){
		if(intval($rateId)<=0 || !$this->id)
			return false;
		$q = "UPDATE ".$this->ecs->table('sales_agent_rate')." SET active=true WHERE active=false AND sa_id=".$this->id." AND sa_rate_id=".$rateId;
		if($this->db->query($q)){
			$this->_flushRateCache();
			return true;
		}
		
		return false;
	}

	public function deactivate(){
		if(!$this->id)
			return false;
		$q = "UPDATE ".$this->tablename." SET active=false WHERE active=true AND sa_id=".$this->id;
		return $this->db->query($q);
	}

	public function activate(){
		if(!$this->id)
			return false;

		$q = "UPDATE ".$this->tablename." SET active=true WHERE active=false AND sa_id=".$this->id;
		return $this->db->query($q);
	}

	public function updateName($name){
		if(!$this->id)
			return false;

		$q = "UPDATE ".$this->tablename." SET name='".$name."' WHERE sa_id=".$this->id;
		return $this->db->query($q);
	}

	public function setPayType($type_id){
		if(!$this->id)
			return false;

		$q = "UPDATE ".$this->tablename." SET pay_type='" . $type_id . "' WHERE sa_id=".$this->id;

		return $this->db->query($q);
	}

	public function setDefaultRate($rate_id){
		if(!$this->id)
			return false;
		if(!$rate_id || $rate_id == 0 || $rate_id == '')
			return false;

		$q = "UPDATE ".$this->ecs->table('sales_agent_rate')." SET is_default = (CASE WHEN sa_rate_id = '".$rate_id."' THEN 1 ELSE 0 END ) ".
						" WHERE sa_id=".$this->id;
		
		if($this->db->query($q)){
			$this->_flushRateCache();
			return true;
		}

		return false;
	}

	private function _fetchData() {
		if (intval($this->id) <= 0)
			return false;
		$q = "SELECT " . implode(',', $this->fillable) . " FROM " . $this->tablename . " WHERE sa_id=" . $this->id;
		$res = mysql_fetch_assoc($this->db->query($q));
		foreach ($res as $k => $v) {
			$this->data[$k] = $v;
		}
		if (count($this->data) > 0)
			return true;
		else
			return false;
	}

	private function _create($data){
		if(!isset($data['name'])){
			return false;
		}
		$active = isset($data['active']) ? $data['active'] : true;
		
		$q = "INSERT INTO ".$this->tablename." (name,active,created_at) VALUE ('".$data['name']."', '".$active."', '".date('Y-m-d H:i:s')."')";
		return $this->db->query($q);
	}

	private function _flushRateCache(){
		if($this->memcache->get($this->ratesCacheKey)!==false)
		$this->memcache->flush($this->ratesCacheKey);
		if($this->memcache->get($this->ratesCacheKeyAll)!==false)
		$this->memcache->flush($this->ratesCacheKeyAll);
	}
}