<?php
/**
* MichaelHui@yoho
* 20170328
*/
namespace Yoho\cms\Model;

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

/*------------------------------------------------------ */
//-- 该类用于与数据库数据进行交换
/*------------------------------------------------------ */
class Salesperson extends YohoBaseModel
{
	public $data;
	public $id,$fillable=[
		'sales_name',
		'display_name'
	];
	private $tablename='salesperson';

	public function __construct($id){
		parent::__construct();
		$id=intval($id);

		$this->tablename=$this->ecs->table($this->tablename);

		$res = $this->db->query("SELECT * FROM ".$this->tablename." WHERE sales_id=".$id);
		if(mysql_num_rows($res)>0){
			$this->id=$id;
			$this->_fetchData();
		}else{
			return false;
		}
		return true;
	}
/*
	public function attachAdminUser($adminId){
		$adminId=intval($adminId);
		$query="SELECT * FROM ".$GLOBALS['ecs']->table('admin_user')." WHERE user_id=".$adminId;
		$query2="SELECT * FROM ".$this->tablename." WHERE admin_id=".$adminId;
		if($adminId>0 && $this->id>0){
			if(mysql_num_rows($this->db->query($query))>0){
				return false;
			}else{
				if(mysql_num_rows($this->db->query($query2))>0){
					return false;
				}else{
					return $this->db->query("UPDATE ".$this->tablename." SET admin_id=".$adminId." WHERE sales_id=".$this->id);
				}
			}
		}else{
			return false;
		}
	}
	*/

	public function getId(){
		return $this->id;
	}

	public function getFillable(){
		return $this->fillable;
	}

	private function _fetchData(){
		$adminuser = new Adminuser();
		$this->data=[];
		$res = $this->db->query("SELECT sp.".implode(',sp.', $this->fillable).", au.".implode(',au.', $adminuser->getFillable())." FROM ".$this->tablename." sp, ".$adminuser->getTableName()." au WHERE sp.admin_id=au.user_id AND sp.sales_id=".$this->id);
		if(mysql_num_rows($res)>0){
			$res=mysql_fetch_assoc($res);
			foreach($res as $k=>$v){
				$this->data[$k]=$v;
			}
		}
	}
}