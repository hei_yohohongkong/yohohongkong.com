<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_base.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_finance.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_supplier.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
require(dirname(__FILE__) .'/includes/ERP/cls/cls_date.php');
if($_REQUEST['act'] == 'bank_account_setting'||$_REQUEST['act'] == 'bank_account_list')
{
	if($_REQUEST['act'] == 'bank_account_setting'&&!admin_priv('erp_sys_manage','',false))
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
	if($_REQUEST['act'] == 'bank_account_list'&&!admin_priv('erp_finance_manage','',false)  &&!admin_priv('erp_finance_approve','',false))
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
	$page=isset($_REQUEST['page']) &&intval($_REQUEST['page']) >0 ?intval($_REQUEST['page']):1;
	$account_name=isset($_REQUEST['account_name'])?trim($_REQUEST['account_name']):'';
	$account_no=isset($_REQUEST['account_no'])?trim($_REQUEST['account_no']):'';
	include('./includes/ERP/page.class.php');
	$num_per_page=20;
	$mode=1;
	$page_bar_num=6;
	$page_style="page_style";
	$current_page_style="current_page_style";
	$start=$num_per_page*($page-1);
	$agency_id=$_REQUEST['act'] == 'bank_account_setting' ? 0 : get_admin_agency_id($_SESSION['admin_id']);
	$paras=array(
		'account_name'=>$account_name,
		'account_no'=>$account_no,
		'agency_id'=>$agency_id
	);
	$total_num=bank_account_count($paras);
	$account_list=bank_account_list($paras,$start,$num_per_page);
	$smarty->assign('account_list',$account_list);
	$url=fix_url($_SERVER['REQUEST_URI'],array(
		'page'=>'',
		'account_name'=>$account_name,
		'account_no'=>$account_no
	));
	$smarty->assign('account_name',$account_name);
	$smarty->assign('account_no',$account_no);
	$pager=new page(array('total_data'=>$total_num,'data_per_page'=>$num_per_page,'url'=>$url,'mode'=>$mode,'page_bar_num'=>$page_bar_num,'page_style'=>$page_style,'current_page_style'=>$current_page_style));
	$smarty->assign('pager',$pager->show());
	$smarty->assign('act',$_REQUEST['act']);
	if($_REQUEST['act'] == 'bank_account_setting')
	{
		$smarty->assign('ur_here',$GLOBALS['_LANG']['erp_account_setting']);
		$href="erp_finance_manage.php?act=add_bank_account";
		$text=$_LANG['erp_bank_account_add'];
		$action_link = array('href'=>$href,'text'=>$text);
		$smarty->assign('action_link',$action_link);
		assign_query_info();
		$smarty->display('erp_bank_account.htm');
	}
	else
	{
		$smarty->assign('ur_here',$GLOBALS['_LANG']['erp_account_list']);
		assign_query_info();
		$smarty->display('erp_bank_account_list.htm');
	}
}
if($_REQUEST['act'] == 'edit_bank_account')
{
	if((admin_priv('erp_sys_manage','',false)))
	{
		$account_id=isset($_REQUEST['id']) &&intval($_REQUEST['id'])>0 ?intval($_REQUEST['id']):0;
		$sql="select * from ".$GLOBALS['ecs']->table('erp_gathering')." where account_id='".$account_id."' limit 1";
		$record=$GLOBALS['db']->getRow($sql);
		if(!empty($record))
		{
			$href="erp_finance_manage.php?act=bank_account_setting";
			$text=$_LANG['erp_bank_account_return_to_list'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_bank_account_has_gathering'],0,$link);
		}
		$sql="select * from ".$GLOBALS['ecs']->table('erp_payment')." where account_id='".$account_id."' limit 1";
		$record=$GLOBALS['db']->getRow($sql);
		if(!empty($record))
		{
			$href="erp_finance_manage.php?act=bank_account_setting";
			$text=$_LANG['erp_bank_account_return_to_list'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_bank_account_has_payment'],0,$link);
		}
		$sql="select * from ".$GLOBALS['ecs']->table('erp_bank_account_record')." where account_id='".$account_id."' limit 1";
		$record=$GLOBALS['db']->getRow($sql);
		if(!empty($record))
		{
			$href="erp_finance_manage.php?act=bank_account_setting";
			$text=$_LANG['erp_bank_account_return_to_list'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_bank_account_has_bank_record'],0,$link);
		}
		$smarty->assign('bank_account',get_bank_account_info($account_id));
		$smarty->assign('agency_list',agency_list());
		$smarty->assign('act','update_bank_account');
		$smarty->assign('ur_here',$GLOBALS['_LANG']['erp_bank_account_edit']);
		assign_query_info();
		$smarty->display('erp_bank_account_info.htm');
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
if($_REQUEST['act'] == 'add_bank_account')
{
	if((admin_priv('erp_sys_manage','',false)))
	{
		$smarty->assign('agency_list',agency_list());
		$smarty->assign('act','add_bank_account');
		$smarty->assign('ur_here',$GLOBALS['_LANG']['erp_bank_account_add']);
		assign_query_info();
		$smarty->display('erp_bank_account_info.htm');
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif($_REQUEST['act'] == 'payment_style_setting')
{
	$payment_style_info=get_payment_style_info();
	$smarty->assign('payment_style_info',$payment_style_info);
	assign_query_info();
	$smarty->display('erp_payment_style.htm');
}
elseif($_REQUEST['act'] == 'print_bank_details')
{
	if(admin_priv('erp_finance_view','',false) ||admin_priv('erp_finance_manage','',false) ||admin_priv('erp_finance_approve','',false))
	{
		$account_id=isset($_REQUEST['account_id']) &&intval($_REQUEST['account_id']) >0 ?intval($_REQUEST['account_id']):0;
		$start_date=isset($_REQUEST['start_date']) ?trim($_REQUEST['start_date']):'';
		$end_date=isset($_REQUEST['end_date']) ?trim($_REQUEST['end_date']):'';
		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		if(!is_agency_bank_account($account_id,$agency_id))
		{
			$href="erp_finance_manage.php?act=bank_account_list";
			$text=$_LANG['erp_bank_account_return_to_list'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_bank_account_no_agency_access'],0,$link);
		}
		$cls_date=new cls_date();
		$start_time=!empty($start_date)?$cls_date->date_to_stamp($start_date): 0;
		$end_time=!empty($end_date)?$cls_date->date_to_stamp($end_date):0;
		$paras=array(
			'account_id'=>$account_id,
			'start_time'=>$start_time,
			'end_time'=>$end_time,
			'agency_id'=>$agency_id
		);
		$bank_account_info=get_bank_account_info($account_id);
		$bank_details=get_bank_account_details_list($paras);
		$smarty->assign('bank_account_info',$bank_account_info);
		$smarty->assign('bank_details',$bank_details);
		assign_query_info();
		$smarty->display('erp_bank_account_details_print.htm');
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif($_REQUEST['act'] == 'view_bank_details')
{
	if(admin_priv('erp_finance_view','',false) ||admin_priv('erp_finance_manage','',false) ||admin_priv('erp_finance_approve','',false))
	{
		$page=isset($_REQUEST['page']) &&intval($_REQUEST['page']) >0 ?intval($_REQUEST['page']):1;
		$account_id=isset($_REQUEST['account_id']) &&intval($_REQUEST['account_id']) >0 ?intval($_REQUEST['account_id']):0;
		$start_date=isset($_REQUEST['start_date']) ?trim($_REQUEST['start_date']):'';
		$end_date=isset($_REQUEST['end_date']) ?trim($_REQUEST['end_date']):'';
		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		if(!is_agency_bank_account($account_id,$agency_id))
		{
			$href="erp_finance_manage.php?act=bank_account_list";
			$text=$_LANG['erp_bank_account_return_to_list'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_bank_account_no_agency_access'],0,$link);
		}
		include('./includes/ERP/page.class.php');
		$num_per_page=20;
		$mode=1;
		$page_bar_num=6;
		$page_style="page_style";
		$current_page_style="current_page_style";
		$start=$num_per_page*($page-1);
		$cls_date=new cls_date();
		$start_time=!empty($start_date)?$cls_date->date_to_stamp($start_date): 0;
		$end_time=!empty($end_date)?$cls_date->date_to_stamp($end_date):0;
		$paras=array(
			'account_id'=>$account_id,
			'start_time'=>$start_time,
			'end_time'=>$end_time,
			'agency_id'=>$agency_id
		);
		$total_num=get_bank_account_details_count($paras);
		$account_details_list=get_bank_account_details_list($paras,$start,$num_per_page);
		$smarty->assign('bank_details',$account_details_list);
		$url=fix_url($_SERVER['REQUEST_URI'],array(
			'page'=>'',
			'account_id'=>$account_id,
			'start_date'=>$start_date,
			'end_date'=>$end_date
		));
		$smarty->assign('account_id',$account_id);
		$smarty->assign('start_date',$start_date);
		$smarty->assign('end_date',$end_date);
		$pager=new page(array('total_data'=>$total_num,'data_per_page'=>$num_per_page,'url'=>$url,'mode'=>$mode,'page_bar_num'=>$page_bar_num,'page_style'=>$page_style,'current_page_style'=>$current_page_style));
		$smarty->assign('pager',$pager->show());
		$bank_account_info=get_bank_account_info($account_id);
		$smarty->assign('bank_account_info',$bank_account_info);
		$smarty->assign('ur_here',$GLOBALS['_LANG']['erp_bank_account_details']);
		assign_query_info();
		$smarty->display('erp_bank_account_details.htm');
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif($_REQUEST['act'] == 'account_payable')
{
	if(admin_priv('erp_finance_view','',false) ||admin_priv('erp_finance_manage','',false) ||admin_priv('erp_finance_approve','',false))
	{
		$page=isset($_REQUEST['page']) &&intval($_REQUEST['page']) >0 ?intval($_REQUEST['page']):1;
		$order_sn=isset($_REQUEST['order_sn'])?trim($_REQUEST['order_sn']):'';
		$supplier_name=isset($_REQUEST['supplier_name'])?trim($_REQUEST['supplier_name']):'';
		include('./includes/ERP/page.class.php');
		$num_per_page=20;
		$mode=1;
		$page_bar_num=6;
		$page_style="page_style";
		$current_page_style="current_page_style";
		$start=$num_per_page*($page-1);
		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		$paras=array(
			'order_sn'=>$order_sn,
			'supplier_name'=>$supplier_name,
			'agency_id'=>$agency_id
		);
		$total_num=get_payable_order_count($paras);
		$payables_list=get_payable_order_list($paras,$start,$num_per_page);
		$smarty->assign('payables_list',$payables_list);
		$url=fix_url($_SERVER['REQUEST_URI'],array(
			'page'=>'',
			'order_sn'=>$order_sn,
			'supplier_name'=>$supplier_name
		));
		$smarty->assign('order_sn',$order_sn);
		$smarty->assign('supplier_name',$supplier_name);
		$pager=new page(array('total_data'=>$total_num,'data_per_page'=>$num_per_page,'url'=>$url,'mode'=>$mode,'page_bar_num'=>$page_bar_num,'page_style'=>$page_style,'current_page_style'=>$current_page_style));
		$smarty->assign('pager',$pager->show());
		if((admin_priv('erp_finance_manage','',false)))
		{
			$smarty->assign('act','edit');
		}
		$smarty->assign('ur_here',$GLOBALS['_LANG']['erp_account_payable_list']);
		assign_query_info();
		$smarty->display('erp_account_payable_list.htm');
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif($_REQUEST['act'] == 'view_payable')
{
	if(admin_priv('erp_finance_view','',false) ||admin_priv('erp_finance_manage','',false) ||admin_priv('erp_finance_approve','',false))
	{
		$order_id=isset($_REQUEST['order_id']) &&intval($_REQUEST['order_id'])>0?intval($_REQUEST['order_id']):0;
		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		$paras=array(
			'order_id'=>$order_id,
			'agency_id'=>$agency_id
		);
		$payables_info=get_payable_order_list($paras);
		if((admin_priv('erp_finance_manage','',false)))
		{
			$smarty->assign('act','edit');
		}
		$smarty->assign('payable_info',$payables_info[$order_id]);
		$smarty->assign('ur_here',$GLOBALS['_LANG']['erp_payable_details']);
		assign_query_info();
		$smarty->display('erp_payable.htm');
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif($_REQUEST['act'] == 'view_receivable')
{
	if(admin_priv('erp_finance_view','',false) ||admin_priv('erp_finance_manage','',false) ||admin_priv('erp_finance_approve','',false))
	{
		$order_id=isset($_REQUEST['order_id']) &&intval($_REQUEST['order_id'])>0 ?intval($_REQUEST['order_id']):0;
		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		$paras=array(
			'order_id'=>$order_id,
			'agency_id'=>$agency_id
		);
		$receivable_info=get_receivable_order_list($paras);
		if((admin_priv('erp_finance_manage','',false)))
		{
			$smarty->assign('act','edit');
		}
		$smarty->assign('receivable_info',$receivable_info[0]);
		$smarty->assign('ur_here',$GLOBALS['_LANG']['erp_receivable_details']);
		assign_query_info();
		$smarty->display('erp_receivable.htm');
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif($_REQUEST['act'] == 'payment_list')
{
	if(admin_priv('erp_finance_view','',false) ||admin_priv('erp_finance_manage','',false) ||admin_priv('erp_finance_approve','',false))
	{
		$page=isset($_REQUEST['page']) &&intval($_REQUEST['page']) >0 ?intval($_REQUEST['page']):1;
		$status=isset($_REQUEST['status']) &&intval($_REQUEST['status'])>0 ?intval($_REQUEST['status']):0;
		$start_date=isset($_REQUEST['start_date'])?trim($_REQUEST['start_date']):'';
		$end_date=isset($_REQUEST['end_date'])?trim($_REQUEST['end_date']):'';
		$payment_sn=isset($_REQUEST['payment_sn'])?trim($_REQUEST['payment_sn']):'';
		$pay_to=isset($_REQUEST['pay_to'])?trim($_REQUEST['pay_to']):'';
		$bill_sn=isset($_REQUEST['bill_sn'])?trim($_REQUEST['bill_sn']):'';
		include('./includes/ERP/page.class.php');
		$num_per_page=20;
		$mode=1;
		$page_bar_num=6;
		$page_style="page_style";
		$current_page_style="current_page_style";
		$start=$num_per_page*($page-1);
		$cls_date=new cls_date();
		$start_time=!empty($start_date)?$cls_date->date_to_stamp($start_date): 0;
		$end_time=!empty($end_date)?$cls_date->date_to_stamp($end_date):0;
		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		$paras=array(
			'status'=>$status,
			'start_time'=>$start_time,
			'end_time'=>$end_time,
			'payment_sn'=>$payment_sn,
			'pay_to'=>$pay_to,
			'bill_sn'=>$bill_sn,
			'agency_id'=>$agency_id
		);
		$total_num=get_payment_count($paras);
		$payment_list=get_payment_list($paras,$start,$num_per_page);
		$smarty->assign('payment_list',$payment_list);
		$url=fix_url($_SERVER['REQUEST_URI'],array('page'=>''));
		$smarty->assign('status',$status);
		$smarty->assign('start_date',$start_date);
		$smarty->assign('end_date',$end_date);
		$smarty->assign('payment_sn',$payment_sn);
		$smarty->assign('pay_to',$pay_to);
		$smarty->assign('bill_sn',$bill_sn);
		$pager=new page(array('total_data'=>$total_num,'data_per_page'=>$num_per_page,'url'=>$url,'mode'=>$mode,'page_bar_num'=>$page_bar_num,'page_style'=>$page_style,'current_page_style'=>$current_page_style));
		$smarty->assign('pager',$pager->show());
		$payment_status=get_payment_status();
		$smarty->assign('payment_status',$payment_status);
		$supplier_info=supplier_list(array('is_valid'=>1));
		$smarty->assign('supplier_info',$supplier_info);
		$bank_account=bank_account_list(array('is_valid'=>1,'agency_id'=>$agency_id));
		$smarty->assign('bank_account',$bank_account);
		$payable_orders=get_payable_order_list(array('agency_id'=>$agency_id));
		$smarty->assign('payable_orders',$payable_orders);
		$smarty->assign('ur_here',$GLOBALS['_LANG']['erp_payment_list']);
		assign_query_info();
		$smarty->display('erp_payment_list.htm');
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif($_REQUEST['act'] == 'edit_payment')
{
	if((admin_priv('erp_finance_manage','',false)))
	{
		$payment_id=$_REQUEST['payment_id'];
		if(lock_payment($payment_id,'edit'))
		{
			$payment_info=get_payment_info($payment_id);
			$smarty->assign('payment_info',$payment_info[0]);
			$smarty->assign('ur_here',$GLOBALS['_LANG']['erp_payment_list']);
			assign_query_info();
			$smarty->display('erp_payment_info.htm');
		}
		else
		{
			$href="./erp_finance_manage.php?act=payment_list";
			$text=$_LANG['erp_payment_return_to_payment_list'];
			$link[] = array('href'=>$href,'text'=>$text);
			$href="./erp_finance_manage.php?act=view_payment&payment_id=".$payment_id;
			$text=$_LANG['erp_payment_view_payment'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_order_no_accessibility'],0,$link);
		}
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif($_REQUEST['act'] == 'receivable_list')
{
	if(admin_priv('erp_finance_view','',false) ||admin_priv('erp_finance_manage','',false) ||admin_priv('erp_finance_approve','',false))
	{
		$page=isset($_REQUEST['page']) &&intval($_REQUEST['page']) >0 ?intval($_REQUEST['page']):1;
		$order_sn=isset($_REQUEST['order_sn'])?trim($_REQUEST['order_sn']) : '';
		include('./includes/ERP/page.class.php');
		$num_per_page=20;
		$mode=1;
		$page_bar_num=6;
		$page_style="page_style";
		$current_page_style="current_page_style";
		$start=$num_per_page*($page-1);
		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		$paras=array(
			'order_sn'=>$order_sn,
			'agency_id'=>$agency_id
		);
		$total_num=get_receivable_order_count($paras);
		$receivable_list=get_receivable_order_list($paras,$start,$num_per_page);
		$smarty->assign('receivable_list',$receivable_list);
		$url=fix_url($_SERVER['REQUEST_URI'],array(
			'page'=>'',
			'order_sn'=>$order_sn
		));
		$smarty->assign('order_sn',$order_sn);
		$pager=new page(array('total_data'=>$total_num,'data_per_page'=>$num_per_page,'url'=>$url,'mode'=>$mode,'page_bar_num'=>$page_bar_num,'page_style'=>$page_style,'current_page_style'=>$current_page_style));
		$smarty->assign('pager',$pager->show());
		if((admin_priv('erp_finance_manage','',false)))
		{
			$smarty->assign('act','edit');
		}
		$smarty->assign('ur_here',$GLOBALS['_LANG']['erp_account_receivable_list']);
		assign_query_info();
		$smarty->display('erp_receivable_list.htm');
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif($_REQUEST['act'] == 'gathering_list')
{
	if(admin_priv('erp_finance_view','',false) ||admin_priv('erp_finance_manage','',false) ||admin_priv('erp_finance_approve','',false))
	{
		$page=isset($_REQUEST['page']) &&intval($_REQUEST['page']) >0 ?intval($_REQUEST['page']):1;
		$status=isset($_REQUEST['status']) &&intval($_REQUEST['status'])>0 ?intval($_REQUEST['status']):0;
		$start_date=isset($_REQUEST['start_date'])?trim($_REQUEST['start_date']):'';
		$end_date=isset($_REQUEST['end_date'])?trim($_REQUEST['end_date']):'';
		$gathering_sn=isset($_REQUEST['gathering_sn'])?trim($_REQUEST['gathering_sn']):'';
		$bill_sn=isset($_REQUEST['bill_sn'])?trim($_REQUEST['bill_sn']):'';
		include('./includes/ERP/page.class.php');
		$num_per_page=20;
		$mode=1;
		$page_bar_num=6;
		$page_style="page_style";
		$current_page_style="current_page_style";
		$start=$num_per_page*($page-1);
		$cls_date=new cls_date();
		$start_time=!empty($start_date)?$cls_date->date_to_stamp($start_date): 0;
		$end_time=!empty($end_date)?$cls_date->date_to_stamp($end_date):0;
		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		$paras=array(
			'status'=>$status,
			'start_time'=>$start_time,
			'end_time'=>$end_time,
			'gathering_sn'=>$gathering_sn,
			'bill_sn'=>$bill_sn,
			'agency_id'=>$agency_id
		);
		$total_num=get_gathering_count($paras);
		$gathering_list=get_gathering_list($paras,$start,$num_per_page);
		$smarty->assign('gathering_list',$gathering_list);
		$url=fix_url($_SERVER['REQUEST_URI'],array('page'=>''));
		$smarty->assign('status',$status);
		$smarty->assign('start_date',$start_date);
		$smarty->assign('end_date',$end_date);
		$smarty->assign('gathering_sn',$gathering_sn);
		$smarty->assign('bill_sn',$bill_sn);
		$pager=new page(array('total_data'=>$total_num,'data_per_page'=>$num_per_page,'url'=>$url,'mode'=>$mode,'page_bar_num'=>$page_bar_num,'page_style'=>$page_style,'current_page_style'=>$current_page_style));
		$smarty->assign('pager',$pager->show());
		$gathering_status=get_gathering_status();
		$smarty->assign('gathering_status',$gathering_status);
		$smarty->assign('bank_account',bank_account_list(array('is_valid'=>1,'agency_id'=>$agency_id)));
		$smarty->assign('receivable_order',get_receivable_order_list(array('agency_id'=>$agency_id)));
		$smarty->assign('ur_here',$GLOBALS['_LANG']['erp_gathering_list']);
		assign_query_info();
		$smarty->display('erp_gathering_list.htm');
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif($_REQUEST['act'] == 'print_gathering')
{
	echo "print_gathering";
}
elseif($_REQUEST['act'] == 'print_payment')
{
	echo "print_payment";
}
?>
