<?php

/***
* sale_commission.php
* by howang 2014-06-24
*
* Sales Commission Calculator for YOHO Hong Kong
***/


define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require_once(ROOT_PATH . 'includes/lib_order.php');
require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/admin/statistic.php');
$smarty->assign('lang', $_LANG);

if (isset($_REQUEST['act']) && $_REQUEST['act'] == 'adjust')
{
    /* 权限判断 */
    admin_priv('sale_order_stats');

    $start_date = empty($_REQUEST['start_date']) ? local_strtotime(month_start()) : local_strtotime($_REQUEST['start_date']);
    $end_date = empty($_REQUEST['end_date']) ? local_strtotime(month_end()) : local_strtotime($_REQUEST['end_date']);

    $data = get_sale_commission_detail();

    $smarty->assign('filter',           $data['filter']);
    $smarty->assign('record_count',     $data['record_count']);
    $smarty->assign('page_count',       $data['page_count']);
    $smarty->assign('commission_list',  $data['data']);
    $smarty->assign('ur_here',          '佣金修正');
    $smarty->assign('full_page',        1);
    $smarty->assign('start_date',       local_date('Y-m-d', $start_date));
    $smarty->assign('end_date',         local_date('Y-m-d', $end_date));
    $smarty->assign('have_discount',    (empty($_REQUEST['have_discount']) ? 0 : 1));
    $smarty->assign('cfg_lang',         $_CFG['lang']);
    $smarty->assign('action_link',  array('text' => '訂單自動修正佣金','href'=>'#auto_adjust_order'));

    /* 显示页面 */
    assign_query_info();
    $smarty->display('sale_commission_adjust.htm');
}
else if (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'query_adjust'))
{
    /* 检查权限 */
    check_authz_json('sale_order_stats');
    if (strstr($_REQUEST['start_date'], '-') === false)
    {
        $_REQUEST['start_date'] = local_date('Y-m-d', $_REQUEST['start_date']);
        $_REQUEST['end_date'] = local_date('Y-m-d', $_REQUEST['end_date']);
    }
    
    $data = get_sale_commission_detail();

    $smarty->assign('commission_list', $data['data']);
    $smarty->assign('filter',          $data['filter']);
    $smarty->assign('record_count',    $data['record_count']);
    $smarty->assign('page_count',      $data['page_count']);

    make_json_result($smarty->fetch('sale_commission_adjust.htm'), '', array('filter' => $data['filter'], 'page_count' => $data['page_count']));
}
/*------------------------------------------------------ */
//-- 编辑訂單佣金修正
/*------------------------------------------------------ */
else if ($_REQUEST['act'] == 'edit_commission_adjust')
{
    /* 检查权限 */
    check_authz_json('sale_order_stats');

    $commission_adjust = empty($_POST['val']) ? 0 : doubleval($_POST['val']);
    $order_id = empty($_POST['id']) ? 0 : intval($_POST['id']);

    if ($order_id == 0)
    {
        make_json_error('NO ORDER ID');
        exit;
    }

    $sql = 'UPDATE ' . $GLOBALS['ecs']->table('order_info') . " SET commission_adjust = '$commission_adjust' WHERE order_id = '$order_id'";
    if ($GLOBALS['db']->query($sql))
    {
        make_json_result($commission_adjust);
    }
    else
    {
        make_json_error($GLOBALS['db']->errorMsg());
    }
}
else if (isset($_REQUEST['act']) && $_REQUEST['act'] == 'detail')
{
    /* 权限判断 */
    admin_priv('sale_order_stats');
    
    $start_date = empty($_REQUEST['start_date']) ? local_strtotime(month_start()) : local_strtotime($_REQUEST['start_date']);
    $end_date = empty($_REQUEST['end_date']) ? local_strtotime(month_end()) : local_strtotime($_REQUEST['end_date']);
    $salesperson = empty($_REQUEST['salesperson']) ? '' : $_REQUEST['salesperson'];

    $data = get_sale_commission_detail();

    $smarty->assign('filter',           $data['filter']);
    $smarty->assign('record_count',     $data['record_count']);
    $smarty->assign('page_count',       $data['page_count']);
    $smarty->assign('commission_list',  $data['data']);
    $smarty->assign('ur_here',          '佣金統計詳情');
    $smarty->assign('full_page',        1);
    $smarty->assign('start_date',       local_date('Y-m-d', $start_date));
    $smarty->assign('end_date',         local_date('Y-m-d', $end_date));
    $smarty->assign('salesperson',      $salesperson);
    $smarty->assign('cfg_lang',         $_CFG['lang']);
    $smarty->assign('action_link',      array('text' => '下載報表','href'=>'#download'));

    /* 显示页面 */
    assign_query_info();
    $smarty->display('sale_commission_detail.htm');
}
else if (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'query_detail' ||  $_REQUEST['act'] == 'download_detail'))
{
    /* 检查权限 */
    check_authz_json('sale_order_stats');
    if (strstr($_REQUEST['start_date'], '-') === false)
    {
        $_REQUEST['start_date'] = local_date('Y-m-d', $_REQUEST['start_date']);
        $_REQUEST['end_date'] = local_date('Y-m-d', $_REQUEST['end_date']);
    }
    
    /*------------------------------------------------------ */
    //--Excel文件下载
    /*------------------------------------------------------ */
    if ($_REQUEST['act'] == 'download_detail')
    {
        $file_name = $_REQUEST['start_date'].'_'.$_REQUEST['end_date'].'_'.$_REQUEST['salesperson'] . '_commission_detail';
        $data = get_sale_commission_detail(false);
        header("Content-type: application/vnd.ms-excel; charset=utf-8");
        header("Content-Disposition: attachment; filename=$file_name.xls");
        
        require_once ROOT_PATH . 'includes/excel/PHPExcel.php';
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        
        $objPHPExcel->getActiveSheet()->setTitle('佣金統計詳情');
        
        /* 文件标题 */
        $objPHPExcel->getActiveSheet()
        ->setCellValue('A1', ecs_iconv(EC_CHARSET, 'UTF8', $_REQUEST['start_date']. $_LANG['to'] .$_REQUEST['end_date']. ' 佣金統計詳情'))
        ->setCellValue('A2', ecs_iconv(EC_CHARSET, 'UTF8', '銷售人員'))
        ->setCellValue('B2', ecs_iconv(EC_CHARSET, 'UTF8', '訂單編號'))
        ->setCellValue('C2', ecs_iconv(EC_CHARSET, 'UTF8', '訂單日期'))
        ->setCellValue('D2', ecs_iconv(EC_CHARSET, 'UTF8', '產品名稱'))
        ->setCellValue('E2', ecs_iconv(EC_CHARSET, 'UTF8', '產品售價'))
        ->setCellValue('F2', ecs_iconv(EC_CHARSET, 'UTF8', '售出數量'))
        ->setCellValue('G2', ecs_iconv(EC_CHARSET, 'UTF8', '產品佣金'))
        ->setCellValue('H2', ecs_iconv(EC_CHARSET, 'UTF8', '佣金'))
        ->setCellValue('I2', ecs_iconv(EC_CHARSET, 'UTF8', '按比例扣除支付折扣'))
        ->setCellValue('J2', ecs_iconv(EC_CHARSET, 'UTF8', '可得佣金'));
        $objPHPExcel->getActiveSheet()->mergeCells('A1:J1');
        
        $i = 3;
        foreach ($data['data'] AS $key => $value)
        {
            $objPHPExcel->getActiveSheet()
            ->setCellValue('A'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['salesperson']))
            ->setCellValueExplicit('B'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['order_sn']), PHPExcel_Cell_DataType::TYPE_STRING)
            ->setCellValue('C'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['order_date']))
            ->setCellValue('D'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['goods_name']))
            ->setCellValue('E'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['goods_price']))
            ->setCellValue('F'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['goods_number']))
            ->setCellValue('G'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['goods_commission']))
            ->setCellValue('H'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['original_commission']))
            ->setCellValue('I'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['pay_discount']))
            ->setCellValue('J'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['commission_amount']));
            $objPHPExcel->getActiveSheet()->getStyle('H'.$i)->getNumberFormat()->setFormatCode('$0.00');
            $objPHPExcel->getActiveSheet()->getStyle('I'.$i)->getNumberFormat()->setFormatCode('$0.00');
            $objPHPExcel->getActiveSheet()->getStyle('J'.$i)->getNumberFormat()->setFormatCode('$0.00');
            $i++;
        }
        
        for ($col = 'A'; $col <= 'J'; $col++)
        {
            $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
        }
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        
        exit;
    }

    $data = get_sale_commission_detail();

    $smarty->assign('commission_list', $data['data']);
    $smarty->assign('filter',          $data['filter']);
    $smarty->assign('record_count',    $data['record_count']);
    $smarty->assign('page_count',      $data['page_count']);

    make_json_result($smarty->fetch('sale_commission_detail.htm'), '', array('filter' => $data['filter'], 'page_count' => $data['page_count']));
}
else if (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'query' ||  $_REQUEST['act'] == 'download'))
{
    /* 检查权限 */
    check_authz_json('sale_order_stats');
    if (strstr($_REQUEST['start_date'], '-') === false)
    {
        $_REQUEST['start_date'] = local_date('Y-m-d', $_REQUEST['start_date']);
        $_REQUEST['end_date'] = local_date('Y-m-d', $_REQUEST['end_date']);
    }
    /*------------------------------------------------------ */
    //--Excel文件下载
    /*------------------------------------------------------ */
    if ($_REQUEST['act'] == 'download')
    {
        $file_name = $_REQUEST['start_date'].'_'.$_REQUEST['end_date'] . '_commission';
        $goods_sales_list = get_sale_commission(false);
        header("Content-type: application/vnd.ms-excel; charset=utf-8");
        header("Content-Disposition: attachment; filename=$file_name.xls");

        require_once ROOT_PATH . 'includes/excel/PHPExcel.php';
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        
        $objPHPExcel->getActiveSheet()->setTitle('佣金統計');
        
        /* 文件标题 */
        $objPHPExcel->getActiveSheet()
        ->setCellValue('A1', ecs_iconv(EC_CHARSET, 'UTF8', $_REQUEST['start_date']. $_LANG['to'] .$_REQUEST['end_date']. ' 佣金統計'))
        ->setCellValue('A2', ecs_iconv(EC_CHARSET, 'UTF8', '銷售人員'))
        ->setCellValue('B2', ecs_iconv(EC_CHARSET, 'UTF8', '訂單總數'))
        ->setCellValue('C2', ecs_iconv(EC_CHARSET, 'UTF8', '銷售總額'))
        ->setCellValue('D2', ecs_iconv(EC_CHARSET, 'UTF8', '可得佣金'));
        $objPHPExcel->getActiveSheet()->mergeCells('A1:D1');
        
        $i = 3;
        foreach ($goods_sales_list['sale_commission_data'] AS $key => $value)
        {
            $objPHPExcel->getActiveSheet()
            ->setCellValue('A'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['salesperson']))
            ->setCellValue('B'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['orders']))
            ->setCellValue('C'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['volume']))
            ->setCellValue('D'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['commission']));
            $objPHPExcel->getActiveSheet()->getStyle('C'.$i)->getNumberFormat()->setFormatCode('$0.00');
            $objPHPExcel->getActiveSheet()->getStyle('D'.$i)->getNumberFormat()->setFormatCode('$0.00');
            $i++;
        }
        
        for ($col = 'A'; $col <= 'D'; $col++)
        {
            $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
        }
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        
        exit;
    }
    $sale_commission_data = get_sale_commission();
    $smarty->assign('commission_list', $sale_commission_data['sale_commission_data']);
    $smarty->assign('filter',          $sale_commission_data['filter']);
    $smarty->assign('record_count',    $sale_commission_data['record_count']);
    $smarty->assign('page_count',      $sale_commission_data['page_count']);
    $smarty->assign('start_date',      $_REQUEST['start_date']);
    $smarty->assign('end_date',        $_REQUEST['end_date']);

    make_json_result($smarty->fetch('sale_commission.htm'), '', array('filter' => $sale_commission_data['filter'], 'page_count' => $sale_commission_data['page_count']));
}
else if (isset($_REQUEST['act']) && $_REQUEST['act'] == 'auto_udpate_commission_adjust'){
    
    /* 检查权限 */
    check_authz_json('sale_order_stats');

    // Get all order
    $order_list = get_sale_commission_detail(false)['data'];

    $sql_list = [];
    foreach ($order_list as $order_id => $order) {
        $auto_adjust_amount = $order['auto_adjust_amount'];
        $commission_adjust  = intval($order['commission_adjust']);

        if(empty($commission_adjust)){
            $order_ids[] = $order_id;
            $sql = "('$order_id','$auto_adjust_amount')";
            $sql_list[] = $sql;
        }
    }
    $order_sql = implode(",", $sql_list);

    $sql = "INSERT INTO ". $GLOBALS['ecs']->table('order_info') . " (order_id, commission_adjust) VALUES ".$order_sql.
           "ON DUPLICATE KEY UPDATE commission_adjust=VALUES(commission_adjust)";

    if(strlen($order_sql) <= 0 ) {
        make_json_error('沒有可自動修正的訂單');
        exit;
    } else if ($GLOBALS['db']->query($sql))
    {
        make_json_result(['status' => 1]);
    }
    else
    {
        make_json_error($GLOBALS['db']->errorMsg());
    }
    

}
/*------------------------------------------------------ */
//--商品明细列表
/*------------------------------------------------------ */
else
{
    /* 权限判断 */
    admin_priv('sale_order_stats');
    /* 时间参数 */
    if (!isset($_REQUEST['start_date']))
    {
        $start_date = local_strtotime(month_start());
    }
    if (!isset($_REQUEST['end_date']))
    {
        $end_date = local_strtotime(month_end());
    }
    
    $sale_commission_data = get_sale_commission();
    /* 赋值到模板 */
    $smarty->assign('filter',       $sale_commission_data['filter']);
    $smarty->assign('record_count', $sale_commission_data['record_count']);
    $smarty->assign('page_count',   $sale_commission_data['page_count']);
    $smarty->assign('commission_list', $sale_commission_data['sale_commission_data']);
    $smarty->assign('ur_here',          $_LANG['sale_commission']);
    $smarty->assign('full_page',        1);
    $smarty->assign('start_date',       local_date('Y-m-d', $start_date));
    $smarty->assign('end_date',         local_date('Y-m-d', $end_date));
    $smarty->assign('cfg_lang',     $_CFG['lang']);
    $smarty->assign('action_link',  array('text' => '下載報表','href'=>'#download'));
    $smarty->assign('action_link2',  array('text' => '佣金修正','href'=>'sale_commission.php?act=adjust&have_discount=1'));

    /* 显示页面 */
    assign_query_info();
    $smarty->display('sale_commission.htm');
}

function month_start()
{
    return local_date('Y-m-01');
}

function month_end()
{
    return local_date('Y-m-t');
}

function get_sale_commission($is_pagination = true){

    /* 时间参数 */
    $filter['start_date'] = empty($_REQUEST['start_date']) ? local_strtotime(month_start()) : local_strtotime($_REQUEST['start_date']);
    $filter['end_date'] = empty($_REQUEST['end_date']) ? local_strtotime(month_end()) : local_strtotime($_REQUEST['end_date']);
  
    /* 查询数据的条件 */
    $where = order_query_sql('finished', 'oi.') .
		   " AND (oi.order_type <> '" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT . "' OR oi.order_type IS NULL) ".
             " AND oi.shipping_time >= '".$filter['start_date']."' AND oi.shipping_time <= '" . ($filter['end_date'] + 86399) . "' ";
    
    $sql = 'SELECT COUNT(*) FROM ( SELECT DISTINCT oi.`salesperson` '.
           'FROM ' . $GLOBALS['ecs']->table('order_info') . ' AS oi '.
           'WHERE 1 ' . $where .
           ') as tmp';
    $filter['record_count'] = $GLOBALS['db']->getOne($sql);

    /* 分页大小 */
    $filter = page_and_size($filter);

    $sql = '('.
           'SELECT salesperson, COUNT(*) as orders, SUM(total_amount) as volume, SUM(commission + commission_adjust) as commission FROM (' .
               'SELECT oi.`salesperson`, (oi.`money_paid` + oi.`order_amount`) as total_amount, oi.`commission_adjust`, '.
               'SUM(IF(RIGHT(g.`commission`, 1) = \'%\', (og.goods_price * og.goods_number - IF(oi.`pay_fee` < 0, oi.`pay_fee` * -1 * og.goods_price * og.goods_number / oi.goods_amount, 0)) * g.commission / 100, g.commission * og.goods_number - IF(oi.`pay_fee` < 0 AND g.commission > 0, oi.`pay_fee` * -1 * og.goods_price * og.goods_number / oi.goods_amount, 0))) as commission '.
               'FROM ' . $GLOBALS['ecs']->table('order_info') . ' as oi '.
               'LEFT JOIN ' . $GLOBALS['ecs']->table('order_goods') . ' as og ON oi.`order_id` = og.`order_id` '.
               'LEFT JOIN ' . $GLOBALS['ecs']->table('goods') . ' as g ON og.`goods_id` = g.`goods_id` '.
               'WHERE 1 ' . $where .
               'GROUP BY oi.`order_id` '.
               ') as tmp '.
           'GROUP BY `salesperson` '.
           'ORDER BY `salesperson` ASC'.
           ') UNION ('.
           'SELECT \'Payable\' as salesperson, COUNT(*) as orders, SUM(total_amount) as volume, SUM(commission + commission_adjust) as commission FROM (' .
           'SELECT oi.`salesperson`, (oi.`money_paid` + oi.`order_amount`) as total_amount, oi.`commission_adjust`, '.
           'SUM(IF(RIGHT(g.`commission`, 1) = \'%\', (og.goods_price * og.goods_number - IF(oi.`pay_fee` < 0, oi.`pay_fee` * -1 * og.goods_price * og.goods_number / oi.goods_amount, 0)) * g.commission / 100, g.commission * og.goods_number - IF(oi.`pay_fee` < 0 AND g.commission > 0, oi.`pay_fee` * -1 * og.goods_price * og.goods_number / oi.goods_amount, 0))) as commission '.
           'FROM ' . $GLOBALS['ecs']->table('order_info') . ' as oi '.
           'LEFT JOIN ' . $GLOBALS['ecs']->table('order_goods') . ' as og ON oi.`order_id` = og.`order_id` '.
           'LEFT JOIN ' . $GLOBALS['ecs']->table('goods') . ' as g ON og.`goods_id` = g.`goods_id` '.
           'WHERE 1 ' . $where . " AND oi.`salesperson` IN ('kanyiukwong','online','yen','natalie','wai','him')".
           'GROUP BY oi.`order_id` '.
           ') as tmp2 '.
           ')';
           
    if ($is_pagination)
    {
        $sql .= " LIMIT " . $filter['start'] . ', ' . $filter['page_size'];
    }

    $sale_commission_data = $GLOBALS['db']->getAll($sql);

    foreach ($sale_commission_data as $key => $item)
    {
        $sale_commission_data[$key]['volume_formatted'] = price_format($sale_commission_data[$key]['volume']);
        $sale_commission_data[$key]['commission_formatted'] = price_format($sale_commission_data[$key]['commission']);
    }
    $arr = array(
        'sale_commission_data' => $sale_commission_data,
        'filter' => $filter,
        'page_count' => $filter['page_count'],
        'record_count' => $filter['record_count']
    );
    return $arr;
}

function get_sale_commission_detail($is_pagination = true)
{
    ini_set('memory_limit', '512M');
    $userController = new Yoho\cms\Controller\UserController();
	$is_adjust = (strpos($_REQUEST['act'],'adjust') !== false || strpos($_REQUEST['act'],'auto_udpate_commission_adjust') !== false );    
    $filter['start_date'] = empty($_REQUEST['start_date']) ? local_strtotime(month_start()) : local_strtotime($_REQUEST['start_date']);
    $filter['end_date'] = empty($_REQUEST['end_date']) ? local_strtotime(month_end()) : local_strtotime($_REQUEST['end_date']);
    $filter['salesperson'] = empty($_REQUEST['salesperson']) ? '' : $_REQUEST['salesperson'];
    $filter['have_discount'] = empty($_REQUEST['have_discount']) ? 0 : 1;
    
    $where = order_query_sql('finished', 'oi.') .
    " AND oi.shipping_time >= '".$filter['start_date']."' AND oi.shipping_time < '" . ($filter['end_date'] + 86399) . "' " .
    " AND (oi.order_type <> '" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT . "' OR oi.order_type IS NULL) ".
    ($is_adjust ? '' : " AND oi.salesperson = '" . $filter['salesperson'] . "' ") .
    ($is_adjust && $filter['have_discount'] ? ' AND (oi.discount > 0 OR oi.bonus > 0 OR oi.coupon > 0) ' : '');
    
    $sql = 'SELECT COUNT(*) '.
           'FROM ' . $GLOBALS['ecs']->table('order_info') . ' as oi ' .
           'LEFT JOIN ' . $GLOBALS['ecs']->table('order_goods') . ' as og ON oi.`order_id` = og.`order_id` ' .
           'WHERE 1 ' . $where;
    $filter['record_count'] = $GLOBALS['db']->getOne($sql);
    
    /* 分页大小 */
    $filter = page_and_size($filter);
    
    $sql = 'SELECT oi.`salesperson`, oi.order_id, oi.order_sn, oi.add_time, oi.postscript, oi.user_id, g.goods_id, g.goods_name, og.goods_price, og.goods_number, g.commission as goods_commission, (oi.discount + oi.bonus + oi.coupon) as discount, oi.commission_adjust, ' .
           'IF(RIGHT(g.`commission`, 1) = \'%\', (og.goods_price * og.goods_number) * g.commission / 100, g.commission * og.goods_number) as original_commission, ' .
           'IF(oi.`pay_fee` < 0, IF(RIGHT(g.`commission`, 1) = \'%\', (oi.`pay_fee` * og.goods_price * og.goods_number / oi.goods_amount) * g.commission / 100, IF(g.commission > 0, oi.`pay_fee` * og.goods_price * og.goods_number / oi.goods_amount, 0)),0) as pay_discount, ' .
           'IF(RIGHT(g.`commission`, 1) = \'%\', (og.goods_price * og.goods_number - IF(oi.`pay_fee` < 0, oi.`pay_fee` * -1 * og.goods_price * og.goods_number / oi.goods_amount, 0)) * g.commission / 100, g.commission * og.goods_number - IF(oi.`pay_fee` < 0 AND g.commission > 0, oi.`pay_fee` * -1 * og.goods_price * og.goods_number / oi.goods_amount, 0)) as commission_amount ' .
           'FROM ' . $GLOBALS['ecs']->table('order_info') . ' as oi ' .
           'LEFT JOIN ' . $GLOBALS['ecs']->table('order_goods') . ' as og ON oi.`order_id` = og.`order_id` ' .
           'LEFT JOIN ' . $GLOBALS['ecs']->table('goods') . ' as g ON og.`goods_id` = g.`goods_id` ' .
           'WHERE 1 ' . $where .
           'ORDER BY oi.`shipping_time` ASC, oi.`order_id` ASC, og.`rec_id` ASC';
    
    if ($is_pagination)
    {
        $sql .= " LIMIT " . $filter['start'] . ', ' . $filter['page_size'];
    }
    
    $data = $GLOBALS['db']->getAll($sql);
    
    foreach ($data as $key => $item)
    {
        $data[$key]['order_date'] = local_date('Y-m-d H:i:s',$data[$key]['add_time']);
        $data[$key]['original_commission_formatted'] = price_format($data[$key]['original_commission']);
        $data[$key]['pay_discount_formatted'] = price_format($data[$key]['pay_discount']);
        $data[$key]['commission_amount_formatted'] = price_format($data[$key]['commission_amount']);
    }
    
    if ($is_adjust)
    {
        $order_commissions = array();
        $adjusted_commissions = array();
        $all_order_ids = array_map(function ($row) { return $row['order_id']; }, $data);
        if (!empty($all_order_ids))
        {
            $sql = 'SELECT oi.`order_id`, oi.`commission_adjust`, ' .
                    'SUM(IF(RIGHT(g.`commission`, 1) = \'%\', (og.goods_price * og.goods_number - IF(oi.`pay_fee` < 0, oi.`pay_fee` * -1 * og.goods_price * og.goods_number / oi.goods_amount, 0)) * g.commission / 100, g.commission * og.goods_number - IF(oi.`pay_fee` < 0 AND g.commission > 0, oi.`pay_fee` * -1 * og.goods_price * og.goods_number / oi.goods_amount, 0))) as commission ' .
                    'FROM ' . $GLOBALS['ecs']->table('order_info') . ' as oi '.
                    'LEFT JOIN ' . $GLOBALS['ecs']->table('order_goods') . ' as og ON oi.`order_id` = og.`order_id` '.
                    'LEFT JOIN ' . $GLOBALS['ecs']->table('goods') . ' as g ON og.`goods_id` = g.`goods_id` '.
                    'WHERE oi.`order_id` IN (' . implode(',',$all_order_ids) . ')'.
                    'GROUP BY oi.`order_id`';
            $res = $GLOBALS['db']->getAll($sql);
            foreach ($res as $row)
            {
                $order_commissions[$row['order_id']] = $row['commission'];
                $adjusted_commissions[$row['order_id']] = $row['commission'] + $row['commission_adjust'];
            }
        }
        
        $res = array();
        foreach ($data as $item)
        {
            if (!isset($res[$item['order_id']]))
            {
                $res[$item['order_id']] = array();
                $res[$item['order_id']]['salesperson'] = $item['salesperson'];
                $res[$item['order_id']]['order_id'] = $item['order_id'];
                $res[$item['order_id']]['order_sn'] = $item['order_sn'];
                $res[$item['order_id']]['add_time'] = $item['add_time'];
                $res[$item['order_id']]['order_date'] = $item['order_date'];
                $res[$item['order_id']]['postscript'] = $item['postscript'];
                $res[$item['order_id']]['user_id'] = $item['user_id'];
                $res[$item['order_id']]['discount'] = $item['discount'];
                $res[$item['order_id']]['discount_formatted'] = price_format($item['discount']);
                $res[$item['order_id']]['commission_adjust'] = $item['commission_adjust'];
                $res[$item['order_id']]['order_commission'] = $order_commissions[$item['order_id']];
                $res[$item['order_id']]['order_commission_formatted'] = price_format($order_commissions[$item['order_id']]);
                $res[$item['order_id']]['adjusted_commission'] = $adjusted_commissions[$item['order_id']];
                $res[$item['order_id']]['adjusted_commission_formatted'] = price_format($adjusted_commissions[$item['order_id']]);
                $res[$item['order_id']]['goods'] = array();
            }
            $res[$item['order_id']]['goods'][] = $item;
        }
        foreach($res as $order_id => $order)
        {
            $discount_amount = doubleval($order['discount']);
            
            if ((in_array($order['user_id'], $userController->getWholesaleUserIds())) || (strpos($order['postscript'], 'no commission') !== false))
            {
                $adjust_amount = doubleval($order['order_commission']) * -1;
            }
            else
            {
                $adjust_amount = 0;
                $goods_sort_commission = $order['goods'];
                usort($goods_sort_commission, function ($goods1, $goods2) {
                    $c1 = doubleval($goods1['goods_commission']);
                    $c2 = doubleval($goods2['goods_commission']);
                    return $c1 == $c2 ? 0 : 
                           $c1 <  $c2 ? 1 : -1;
                });
                foreach ($goods_sort_commission as $goods)
                {
                    $price_amount = doubleval($goods['goods_price']) * intval($goods['goods_number']);
                    $commission_amount = doubleval($goods['original_commission']);
                    if ($discount_amount >= $price_amount)
                    {
                        $adjust_amount -= $commission_amount;
                        $discount_amount -= $price_amount;
                        if ($discount_amount <= 0) break;
                    }
                    else
                    {
                        $adjust_amount -= $commission_amount * $discount_amount / $price_amount;
                        break;
                    }
                }
            }
            $res[$order_id]['auto_adjust_amount'] = $adjust_amount;
        }
        $data = $res;
    }

    $arr = array(
        'data' => $data,
        'filter' => $filter,
        'page_count' => $filter['page_count'],
        'record_count' => $filter['record_count']
    );
    return $arr;
}

?>