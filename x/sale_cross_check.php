<?php

/***
* sale_cross_check.php
* by EricHo 2018-04-27
*
* Sale Cross check Report
***/


define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require(ROOT_PATH . 'includes/lib_order.php');
$userController = new Yoho\cms\Controller\UserController();
$orderController = new Yoho\cms\Controller\OrderController();

if (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'query' ||  $_REQUEST['act'] == 'download')) {
    /* 检查权限 */
    check_authz_json('sale_cross_check');
    if (strstr($_REQUEST['start_date'], '-') === false) {
        $_REQUEST['start_date'] = local_date('Y-m-d', $_REQUEST['start_date']);
        $_REQUEST['end_date'] = local_date('Y-m-d', $_REQUEST['end_date']);
    }

    // YOHO: 當日門店帳目
    $today_paid_order    = $orderController->get_today_order_stats('paid');
    $today_shipped_order = $orderController->get_today_order_stats('shipped');

    // YOHO:現金訂貨
    $cash_preorder['today_amount_order_list_url'] = 'order.php?act=list&pay_id=3&composite_status=' . CS_AWAIT_SHIP . '&pay_start_time=' . local_mktime(0, 0, 0, local_date('m'), local_date('d'), local_date('Y')) . '&pay_end_time=' . local_mktime(23, 59, 59, local_date('m'), local_date('d'), local_date('Y')) . '&have_preorder=1';
    $cash_preorder['today_amount'] = price_format($db->GetOne("SELECT sum(tmp.money_paid) as total FROM " . $GLOBALS['ecs']->table('order_info') . " AS o
    left join (SELECT o.`money_paid`, o.order_id  FROM " . $GLOBALS['ecs']->table('order_info') . " AS o " ." LEFT JOIN " . $GLOBALS['ecs']->table('order_goods') . " AS og on og.order_id = o.order_id ".
        "WHERE o.pay_id = 3 AND (o.postscript LIKE '%訂貨%天%' OR o.order_remarks LIKE '%訂貨%天%' OR o.to_buyer LIKE '%訂貨%天%' OR og.planned_delivery_date > 0) " . order_query_sql('await_ship', 'o.') .
        " AND pay_time BETWEEN " . local_mktime(0, 0, 0, local_date('m'), local_date('d'), local_date('Y')) . " AND " . local_mktime(23, 59, 59, local_date('m'), local_date('d'), local_date('Y'))." GROUP BY o.order_id order by o.order_sn DESC) as tmp on tmp.order_id = o.order_id"), false);
    $cash_preorder['total_amount_order_list_url'] = 'order.php?act=list&pay_id=3&composite_status=' . CS_AWAIT_SHIP . '&have_preorder=1';
    $cash_preorder['total_amount'] = price_format($db->GetOne("SELECT sum(tmp.money_paid) as total FROM " . $GLOBALS['ecs']->table('order_info') . " AS o
    left join (SELECT o.`money_paid`, o.order_id  FROM " . $GLOBALS['ecs']->table('order_info') . " AS o " ." LEFT JOIN " . $GLOBALS['ecs']->table('order_goods') . " AS og on og.order_id = o.order_id ".
        "WHERE o.pay_id = 3 AND (o.postscript LIKE '%訂貨%天%' OR o.order_remarks LIKE '%訂貨%天%' OR o.to_buyer LIKE '%訂貨%天%' OR og.planned_delivery_date > 0) " . order_query_sql('await_ship', 'o.')." GROUP BY o.order_id order by o.order_sn DESC) as tmp on tmp.order_id = o.order_id"), false);
    $smarty->assign('cash_preorder', $cash_preorder);

    $salesman_data = get_salesman_sales_today();
    $smarty->assign('today_paid_order', $today_paid_order);
    $smarty->assign('today_shipped_order', $today_shipped_order);
    $smarty->assign('filter', $salesman_data['filter']);
    $smarty->assign('record_count', $salesman_data['record_count']);
    $smarty->assign('page_count', $salesman_data['page_count']);
    $smarty->assign('salesman_list', $salesman_data['salesman_data']);
    $smarty->assign('sales_total_data', $salesman_data['sales_total_data']);
    $smarty->assign('uncompleted_sales_total', $salesman_data['uncompleted_sales_total']);
    $smarty->assign('today_ship_not_today_paid_total', $salesman_data['today_ship_not_today_paid_total']);
    $smarty->assign('today_ship_not_today_paid_total_url', $salesman_data['tsntpt_url']);
    $smarty->assign('yesterday_shiped_order_url', $salesman_data['yesterday_shiped_order_url']);
    $smarty->assign('uncompleted_sales_total_url', 'order.php?act=list&composite_status='.CS_PAYED_CASH);
    $smarty->assign('today_date', $salesman_data['today_date']);
    $smarty->assign('start_time', $salesman_data['start_time']);
    $smarty->assign('end_time', $salesman_data['end_time']);

    make_json_result($smarty->fetch('sale_cross_check.htm'), '', array('filter' => $salesman_data['filter'], 'page_count' => $salesman_data['page_count']));
}
/*------------------------------------------------------ */
//--商品明细列表
/*------------------------------------------------------ */
else {
    /* 权限判断 */
    admin_priv('sale_cross_check');

    // YOHO: 當日門店帳目
    $today_paid_order    = $orderController->get_today_order_stats('paid');
    $today_shipped_order = $orderController->get_today_order_stats('shipped');
    $salesman_data = get_salesman_sales_today();
    $erp_delivery_pending_approve = intval($db->GetOne('SELECT COUNT(*) FROM ' . $ecs->table('erp_delivery') . " AS ed " ."WHERE ed.status = 2"));
    $smarty->assign('erp_delivery_pending_approve', $erp_delivery_pending_approve);
    $smarty->assign('today_paid_order', $today_paid_order);
    $smarty->assign('today_shipped_order', $today_shipped_order);

    // YOHO:現金訂貨
    $cash_preorder['today_amount_order_list_url'] = 'order.php?act=list&pay_id=3&composite_status=' . CS_AWAIT_SHIP . '&pay_start_time=' . local_mktime(0, 0, 0, local_date('m'), local_date('d'), local_date('Y')) . '&pay_end_time=' . local_mktime(23, 59, 59, local_date('m'), local_date('d'), local_date('Y')) . '&have_preorder=1';
    $cash_preorder['today_amount'] = price_format($db->GetOne("SELECT sum(tmp.money_paid) as total FROM " . $GLOBALS['ecs']->table('order_info') . " AS o
    left join (SELECT o.`money_paid`, o.order_id  FROM " . $GLOBALS['ecs']->table('order_info') . " AS o " ." LEFT JOIN " . $GLOBALS['ecs']->table('order_goods') . " AS og on og.order_id = o.order_id ".
        "WHERE o.pay_id = 3 AND (o.postscript LIKE '%訂貨%天%' OR o.order_remarks LIKE '%訂貨%天%' OR o.to_buyer LIKE '%訂貨%天%' OR og.planned_delivery_date > 0) " . order_query_sql('await_ship', 'o.') .
        " AND pay_time BETWEEN " . local_mktime(0, 0, 0, local_date('m'), local_date('d'), local_date('Y')) . " AND " . local_mktime(23, 59, 59, local_date('m'), local_date('d'), local_date('Y'))." GROUP BY o.order_id order by o.order_sn DESC) as tmp on tmp.order_id = o.order_id"), false);
    $cash_preorder['total_amount_order_list_url'] = 'order.php?act=list&pay_id=3&composite_status=' . CS_AWAIT_SHIP . '&have_preorder=1';
    $cash_preorder['total_amount'] = price_format($db->GetOne("SELECT sum(tmp.money_paid) as total FROM " . $GLOBALS['ecs']->table('order_info') . " AS o
    left join (SELECT o.`money_paid`, o.order_id  FROM " . $GLOBALS['ecs']->table('order_info') . " AS o " ." LEFT JOIN " . $GLOBALS['ecs']->table('order_goods') . " AS og on og.order_id = o.order_id ".
        "WHERE o.pay_id = 3 AND (o.postscript LIKE '%訂貨%天%' OR o.order_remarks LIKE '%訂貨%天%' OR o.to_buyer LIKE '%訂貨%天%' OR og.planned_delivery_date > 0) " . order_query_sql('await_ship', 'o.')." GROUP BY o.order_id order by o.order_sn DESC) as tmp on tmp.order_id = o.order_id"), false);
    $smarty->assign('cash_preorder', $cash_preorder);

    /* 赋值到模板 */
    $smarty->assign('filter', $salesman_data['filter']);
    $smarty->assign('record_count', $salesman_data['record_count']);
    $smarty->assign('page_count', $salesman_data['page_count']);
    $smarty->assign('salesman_list', $salesman_data['salesman_data']);
    $smarty->assign('sales_total_data', $salesman_data['sales_total_data']);
    $smarty->assign('uncompleted_sales_total', $salesman_data['uncompleted_sales_total']);
    $smarty->assign('today_ship_not_today_paid_total', $salesman_data['today_ship_not_today_paid_total']);
    $smarty->assign('today_ship_not_today_paid_total_url', $salesman_data['tsntpt_url']);
    $smarty->assign('yesterday_shiped_order_url', $salesman_data['yesterday_shiped_order_url']);
    $smarty->assign('uncompleted_sales_total_url', 'order.php?act=list&composite_status='.CS_PAYED_CASH);
    $smarty->assign('ur_here', $_LANG['sale_cross_check']);
    $smarty->assign('full_page', 1);
    $smarty->assign('today_date', $salesman_data['today_date']);
    $smarty->assign('start_time', $salesman_data['start_time']);
    $smarty->assign('end_time', $salesman_data['end_time']);
    $smarty->assign('cfg_lang', $_CFG['lang']);

    /* 显示页面 */
    assign_query_info();
    $smarty->display('sale_cross_check.htm');
}


/* 只是計今日出貨 */
function get_salesman_sales_today($is_pagination = false)
{
    global $db, $ecs, $userController;

    $filter['sort_by']          = empty($_REQUEST['sort_by']) ? 'sales_name' : trim($_REQUEST['sort_by']);
    $filter['sort_order']       = empty($_REQUEST['sort_order']) ? 'ASC' : trim($_REQUEST['sort_order']);

    /* 分页大小 */
    $filter = page_and_size($filter);

    $salesperson = array();
    // salesperson list
    $sql = "SELECT IFNULL(sp.sales_name, au.user_name) as sales_name " .
            "FROM " . $ecs->table('admin_user') ." as au ".
            "JOIN " . $ecs->table('salesperson') ." as sp ON sp.admin_id = au.user_id AND sp.available = 1 And sp.allow_pos = 1 ".
            "WHERE 1 AND is_disable = 0 " .
            "ORDER BY sales_name ASC " .
    ($is_pagination ? "LIMIT " . $filter['start'] . ", " . $filter['page_size'] : '');
    $sales_name = $db->getCol($sql);
    // We use lower_sales_name to find key
    $lower_sales_name = array_map('strtolower', $sales_name);

    $filter['record_count'] = sizeof($sales_name);

    /* get sub total 已付款,已出貸 */
    // 銷售金額
    $sales_sql = '';
    // Exclude orders for the followiung user accounts: 11111111, 88888888, 99999999
    $where_exclude = " AND (order_type<>'".Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE."' OR (order_type IS NULL AND user_id NOT IN (".implode(',', $userController->getWholesaleUserIds())."))) ";
    // Exclude sales agent
    $where_exclude .= " AND (order_type <> '" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT . "' OR order_type IS NULL) ";
    // Exclude 展場POS
    $where_exclude .= " AND (platform <> 'pos_exhibition') ";

    $where_status = " AND order_status NOT IN (" . OS_CANCELED . ',' . OS_INVALID . ',' . OS_RETURNED . ") ";
    $where_status .= " AND shipping_status IN (" . SS_SHIPPED . ',' . SS_RECEIVED . ") ";
    // today
    $today_date = local_date('Y') .'-'. local_date('m') .'-'. local_date('d');
    $start_time = local_mktime(0, 0, 0, local_date('m'), local_date('d'), local_date('Y'));
    $end_time = local_mktime(23, 59, 59, local_date('m'), local_date('d'), local_date('Y'));

    $where_today = " AND shipping_time BETWEEN " . $start_time . " AND " . $end_time;
    $where_today .= " AND pay_time BETWEEN " . $start_time . " AND " . $end_time;

    $sales_sql  = "SELECT COUNT(*) as order_count, pay_id, salesperson, ";
    $sales_sql .= " SUM(`money_paid` + `order_amount`) as total_amount ";
    $sales_sql .= " FROM " .$ecs->table('order_info');
    $sales_sql .= " WHERE 1 " . $where_exclude . $where_status . $where_today;
    $sales_sql .= " And `salesperson` " . db_create_in($sales_name) ;
    $sales_sql .= " GROUP BY salesperson,pay_id  ";

    $res = $db->GetAll($sales_sql);

    $sales_data = []; //  by salesman
    foreach ($res as $row) {
        $sales_key = strtolower($row['salesperson']);
        $sales_data[$sales_key]['order_count'] += $row['order_count'];
        if ($row['pay_id'] == 3) {
            $sales_data[$sales_key]['cash'] = $row['total_amount'];
        } elseif ($row['pay_id'] == 2) {
            $sales_data[$sales_key]['eps'] = $row['total_amount'];
        } elseif (($row['pay_id'] == 5) || ($row['pay_id'] == 9)) {
            $sales_data[$sales_key]['bank'] += $row['total_amount'];
        } elseif ($row['pay_id'] == 17) {
            $sales_data[$sales_key]['paypal'] = $row['total_amount'];
        } elseif ($row['pay_id'] == 10) {
            $sales_data[$sales_key]['unionpay'] = $row['total_amount'];
        } elseif ($row['pay_id'] == 11) {
            $sales_data[$sales_key]['franzpay'] = $row['total_amount'];
        } elseif ($row['pay_id'] == 12) {
            $sales_data[$sales_key]['visamaster'] = $row['total_amount'];
        } elseif ($row['pay_id'] == 13) {
            $sales_data[$sales_key]['americanexpress'] = $row['total_amount'];
        } elseif ($row['pay_id'] == 14) {
            $sales_data[$sales_key]['alipay'] = $row['total_amount'];
        } elseif ($row['pay_id'] == 15) {
            $sales_data[$sales_key]['stripe'] = $row['total_amount'];
        } elseif ($row['pay_id'] == 16) {
            $sales_data[$sales_key]['braintree'] = $row['total_amount'];
        } elseif ($row['pay_id'] == 22) {
            $sales_data[$sales_key]['qfpay'] = $row['total_amount'];
        }
    }

    /* get sub total cash 己付款,未出貸 */
    // 銷售金額
    $sales_sql = '';
    // Exclude orders for the followiung user accounts: 11111111, 88888888, 99999999
    $where_exclude = " AND (order_type<>'".Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE."' OR (order_type IS NULL AND user_id NOT IN (".implode(',', $userController->getWholesaleUserIds())."))) ";
    // Exclude sales agent
    $where_exclude .= " AND (order_type <> '" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT . "' OR order_type IS NULL) ";
    // Exclude 展場POS
    $where_exclude .= " AND (platform <> 'pos_exhibition') ";

    $where_status = " AND order_status NOT IN (" . OS_CANCELED . ',' . OS_INVALID . ',' . OS_RETURNED . ") " ;
    $where_status .= " AND pay_status = " . PS_PAYED . " "; // paid
    $where_status .= " AND shipping_status NOT IN (" . SS_SHIPPED . ',' . SS_RECEIVED . ") "; // SS_UNSHIPPED,SS_PREPARING,SS_SHIPPED_PART,SS_SHIPPED_ING,OS_SHIPPED_PART
    //$where_status .= " AND shipping_id = 3 "; // express
    $where_status .= " AND pay_id = 3 "; // cash

    // today
    $today_date = local_date('Y') .'-'. local_date('m') .'-'. local_date('d');
    $start_time = local_mktime(0, 0, 0, local_date('m'), local_date('d'), local_date('Y'));
    $end_time = local_mktime(23, 59, 59, local_date('m'), local_date('d'), local_date('Y'));

    $where_today = " AND pay_time BETWEEN " . $start_time . " AND " . $end_time;

    $sales_sql  = "SELECT COUNT(*) as order_count, salesperson, ";
    $sales_sql .= " SUM(`money_paid` + `order_amount`) as total_amount ";
    $sales_sql .= " FROM " .$ecs->table('order_info');
    $sales_sql .= " WHERE 1 " . $where_exclude . $where_status . $where_today;
    $sales_sql .= " And `salesperson` " . db_create_in($sales_name) ;
    $sales_sql .= " GROUP BY salesperson  ";

    $res = $db->GetAll($sales_sql);
    foreach ($res as $row) {
        $sales_key = strtolower($row['salesperson']);
        $sales_data[$sales_key]['cash_unshipped'] = $row['total_amount'];
        $sales_data[$sales_key]['order_count'] += $row['order_count'] ;
    }

    /* get sub total cash 己付款,未出貸 */
    // 銷售金額
    $uncompleted_sales_total_sql = '';
    // Exclude orders for the followiung user accounts: 11111111, 88888888, 99999999
    $where_exclude = " AND (order_type<>'".Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE."' OR (order_type IS NULL AND user_id NOT IN (".implode(',', $userController->getWholesaleUserIds())."))) ";
    // Exclude sales agent
    $where_exclude .= " AND (order_type <> '" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT . "' OR order_type IS NULL) ";
    // Exclude 展場POS
    $where_exclude .= " AND (platform <> 'pos_exhibition') ";

    $where_status = " AND order_status IN (" . OS_CONFIRMED . ',' . OS_SPLITED . ',' . OS_SPLITING_PART . ") " ;
    $where_status .= "  AND money_paid > 0 "; // paid
    $where_status .= " AND shipping_status NOT IN (" . SS_SHIPPED . ',' . SS_RECEIVED . ") "; // SS_UNSHIPPED,SS_PREPARING,SS_SHIPPED_PART,SS_SHIPPED_ING,OS_SHIPPED_PART
    //$where_status .= " AND shipping_id = 3 "; // express
    $where_status .= " AND pay_id = 3 "; // cash

    // today
    // $today_date = local_date('Y') .'-'. local_date('m') .'-'. local_date('d');
    // $start_time = local_mktime(0, 0, 0, local_date('m'), local_date('d'), local_date('Y'));
    // $end_time = local_mktime(23, 59, 59, local_date('m'), local_date('d'), local_date('Y'));
    //
    // $where_today = " AND pay_time BETWEEN " . $start_time . " AND " . $end_time;

    $uncompleted_sales_total_sql .= "select SUM(`money_paid`) as total_amount ";
    $uncompleted_sales_total_sql .= " FROM " .$ecs->table('order_info');
    $uncompleted_sales_total_sql .= " WHERE 1 " . $where_exclude . $where_status;
    $res = $db->getAll($uncompleted_sales_total_sql);
    foreach ($res as $row) {
        $uncompleted_sales_total = price_format($row['total_amount'],false);
    }

    /* get sub total cash 己付款,已出貸, 不是今日的訂單 */
    // 銷售金額
    $today_ship_not_today_paid_total_sql = '';
    // Exclude orders for the followiung user accounts: 11111111, 88888888, 99999999
    $where_exclude = " AND (order_type<>'".Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE."' OR (order_type IS NULL AND user_id NOT IN (".implode(',', $userController->getWholesaleUserIds())."))) ";
    // Exclude sales agent
    $where_exclude .= " AND (order_type <> '" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT . "' OR order_type IS NULL) ";
    // Exclude 展場POS
    $where_exclude .= " AND (platform <> 'pos_exhibition') ";

    $where_status = " AND order_status IN (" . OS_CONFIRMED . ") " ;
    $where_status .= " AND pay_status = " . PS_PAYED . " "; // paid
    $where_status .= " AND shipping_status IN (" . SS_SHIPPED . ',' . SS_RECEIVED . ") "; // SS_UNSHIPPED,SS_PREPARING,SS_SHIPPED_PART,SS_SHIPPED_ING,OS_SHIPPED_PART
    //$where_status .= " AND shipping_id = 3 "; // express
    $where_status .= " AND pay_id = 3 "; // cash
    $where_today  = " AND shipping_time BETWEEN " . $start_time . " AND " . $end_time;
    $where_today  .= " AND add_time NOT BETWEEN " . $start_time . " AND " . $end_time;

    // today
    // $today_date = local_date('Y') .'-'. local_date('m') .'-'. local_date('d');
    // $start_time = local_mktime(0, 0, 0, local_date('m'), local_date('d'), local_date('Y'));
    // $end_time = local_mktime(23, 59, 59, local_date('m'), local_date('d'), local_date('Y'));
    //
    // $where_today = " AND pay_time BETWEEN " . $start_time . " AND " . $end_time;

    $today_ship_not_today_paid_total_sql .= "select SUM(`money_paid`) as total_amount ";
    $today_ship_not_today_paid_total_sql .= " FROM " .$ecs->table('order_info');
    $today_ship_not_today_paid_total_sql .= " WHERE 1 " . $where_exclude . $where_status . $where_today;
    $res = $db->GetAll($today_ship_not_today_paid_total_sql);
    foreach ($res as $row) {
        $today_ship_not_today_paid_total = price_format($row['total_amount'],false);
    }

    $sales_total_data = [];
    $salesman_data = [];

    foreach ($sales_name as $key => $row) {
        $row = strtolower($row);
        // calculate sales total
        $sales_total_data['order_count'] += $sales_data[$row]['order_count'];
        $sales_total_data['cash_unshipped'] += $sales_data[$row]['cash_unshipped'];
        $sales_total_data['cash'] += $sales_data[$row]['cash'];
        $sales_total_data['eps']  += $sales_data[$row]['eps'];
        $sales_total_data['bank'] += $sales_data[$row]['bank'];
        $sales_total_data['paypal'] += $sales_data[$row]['paypal'];
        $sales_total_data['alipay'] += $sales_data[$row]['alipay'];
        $sales_total_data['unionpay'] += $sales_data[$row]['unionpay'];
        $sales_total_data['franzpay'] += $sales_data[$row]['franzpay'];
        $sales_total_data['visamaster'] += $sales_data[$row]['visamaster'];
        $sales_total_data['americanexpress'] += $sales_data[$row]['americanexpress'];
        $sales_total_data['stripe'] += $sales_data[$row]['stripe'];
        $sales_total_data['braintree'] += $sales_data[$row]['braintree'];
        $sales_total_data['qfpay'] += $sales_data[$row]['qfpay'];

        $salesman_data[$key]  = array(
            'sales_name' => ucfirst($row),
            'order_count' => empty($sales_data[$row]['order_count'])? 0 :$sales_data[$row]['order_count'],
            'cash' => price_format($sales_data[$row]['cash'], false),
            'cash_unshipped' => price_format($sales_data[$row]['cash_unshipped'], false),
            'eps' =>  price_format($sales_data[$row]['eps'], false),
            'bank' =>  price_format($sales_data[$row]['bank'], false),
            'paypal' =>  price_format($sales_data[$row]['paypal'], false),
            'alipay' =>  price_format($sales_data[$row]['alipay'], false),
            'unionpay' => price_format($sales_data[$row]['unionpay'], false),
            'franzpay' => price_format($sales_data[$row]['franzpay'], false),
            'visamaster' =>  price_format($sales_data[$row]['visamaster'], false),
            'americanexpress' =>  price_format($sales_data[$row]['americanexpress'], false),
            'stripe' => price_format($sales_data[$row]['stripe'], false),
            'braintree' => price_format($sales_data[$row]['braintree'], false),
            'qfpay' => price_format($sales_data[$row]['qfpay'], false)
      );
    }

    // change price format
    foreach ($sales_total_data as $key => &$sales_total) {
        if ($key != 'order_count') {
            $sales_total = price_format($sales_total, false);
        }
    }

    $sort = array();
    foreach ($salesman_data as $key => $row) {
        $sort[$key] = $row[$filter['sort_by']];
    }
    $sort_order  = $filter['sort_order'] == 'ASC' ? SORT_ASC : SORT_DESC;
    array_multisort($sort, $sort_order, $salesman_data);

    $yesterday = local_strtotime("-1 day");
    $yesterday_date = local_date('Y', $yesterday) .'-'. local_date('m', $yesterday) .'-'. local_date('d', $yesterday);
    $y_start_time = local_mktime(0, 0, 0, local_date('m', $yesterday), local_date('d', $yesterday), local_date('Y', $yesterday));
    $y_end_time = local_mktime(23, 59, 59, local_date('m', $yesterday), local_date('d', $yesterday), local_date('Y', $yesterday));
    $arr = array(
        'today_date' => $today_date,
        'start_time' => $start_time,
        'end_time' => $end_time,
        'salesman_data' => $salesman_data,
        'sales_total_data' => $sales_total_data,
        'uncompleted_sales_total' => $uncompleted_sales_total,
        'today_ship_not_today_paid_total' => $today_ship_not_today_paid_total,
        'tsntpt_url' => 'order.php?act=list&ship_start_time='.$start_time.'&ship_end_time='.$end_time.'&end_time='.$start_time.'&composite_status='.CS_SHIPPED.'&display_normal_order=1&pay_id=3',
        'yesterday_shiped_order_url' => 'order.php?act=list&ship_start_time='.$y_start_time.'&ship_end_time='.$y_end_time.'&composite_status='.CS_SHIPPED.'&display_normal_order=1&pay_id=3',
        'filter' => $filter,
        'page_count' => $filter['page_count'],
        'record_count' => $filter['record_count']
    );

    return $arr;
}
