<?php

/**
 * ECSHOP 管理员信息以及权限管理程序
 * ============================================================================
 * 版权所有 2005-2010 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liuhui $
 * $Id: privilege.php 17063 2010-03-25 06:35:46Z liuhui $
*/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

$roleController = new Yoho\cms\Controller\RoleController();
$adminuserController = new Yoho\cms\Controller\AdminuserController();
$spController = new Yoho\cms\Controller\SalespersonController();

/* act操作项的初始化 */
if (empty($_REQUEST['act']))
{
    $_REQUEST['act'] = 'login';
}
else
{
    $_REQUEST['act'] = trim($_REQUEST['act']);
}

//$one_time_array = array("01_richang"=>"日常工作","02_book"=>"預售登記","02_dai"=>"缺貨登記","03_taskes"=>"工作清單","05_refund_requests"=>"退款及積分申請","06_pending_receive"=>"即將派送貨物清單","07_repair_orders"=>"維修單列表","07_replace_orders"=>"換貨單列表","08_goods_feedback_problem"=>"產品問題回報","09_goods_feedback_price"=>"產品售價提議","10_abnormal_cost_log"=>"成本異常","deal_log"=>"保養登記","goal_setting"=>"KPI輔助","hktvmall"=>"HKTVMALL","order_picker_packer_import"=>"匯入出貨人員資料","quotation"=>"報價單","rma"=>"RMA列表","rsp_record"=>"WEEE回收記錄","sale_cross_check"=>"門店對數","sales_agent_manage"=>"B2B2C批量入單","shop_restore"=>"門市補貨","staff_purchase_list"=>"員工優惠列表","team_kpi"=>"團隊KPI","wechatpay_force_refund"=>"微信支付強制退款","wholesale_user_info"=>"批發訂單顧客資料","yahoo_store"=>"Yahoo商店","section_crm_system"=>"CRM系統","crm_system"=>"客戶服務","ivr_system"=>"IVR","op_system"=>"線下支付","02_cat_and_goods"=>"產品管理","01_goods_list"=>"產品列表","03_category_list"=>"產品分類","06_goods_brand_list"=>"產品品牌","08_attribute"=>"產品屬性","11_goods_trash"=>"產品回收站","12_batch_pic"=>"圖片批量處理","17_tag_manage"=>"標籤管理","auto_pricing"=>"產品自動定價","goods_cost_log"=>"產品成本變動紀錄","goods_price_log"=>"產品價格變動紀錄","goods_shipping_dimension_import"=>"匯入產品尺寸資料","image_compression_product"=>"舊產品圖片處理","shipping_templates"=>"安裝條款模版","warranty_templates"=>"保養條款模版","03_order"=>"訂單管理","02_order_list"=>"訂單列表","order_ship"=>"出貨列表","stock_transfer_cross_check"=>"調貨產品核對","order_express_collected_import"=>"匯入訂單速遞已收件","order_wholesale_delivery"=>"批發單執貨列表","order_list_rank_program"=>"會員計劃訂單列表","order_goods_cross_check"=>"訂單產品核對","10_back_order"=>"退貨單列表","03_order_query"=>"訂單查詢","05_edit_order_print"=>"訂單打印","08_add_order"=>"添加訂單","09_delivery_order"=>"物流單列表","order_goods_sn"=>"輸入產品序號","04_market"=>"市場推廣","02_articlecat_list"=>"資訊分類","03_article_list"=>"資訊列表","05_comment_manage"=>"用家評價","ad_list"=>"廣告列表","ad_position"=>"廣告位置","affiliate"=>"會員推薦計劃設置","affiliate_ck"=>"會員推薦回贈管理","affiliateyoho"=>"推廣大使列表","affiliateyoho_orders"=>"推廣訂單列表","affiliateyoho_rewards"=>"推廣獎勵列表","global_alert"=>"公告管理","homepage_carousel"=>"首頁廣告輪播","mkt_method"=>"營銷玩法","support_manage"=>"Q&A管理","05_erp_order_manage"=>"採購訂單","erp_order_list"=>"採購訂單列表","erp_order_list_pending_payment"=>"等待付款已送貨採購訂單","erp_order_list_pending_receive"=>"即將送貨採購訂單","06_erp_stock_manage"=>"庫存系統","erp_goods_delivery_list"=>"出貨訂單","erp_goods_warehousing_and_delivery_record"=>"出入貨流水帳","erp_goods_warehousing_list"=>"入貨訂單","erp_stock_abnormal"=>"庫存異常","erp_stock_check"=>"存貨盤點","erp_stock_daily"=>"存貨變動圖表","erp_stock_inquiry"=>"即時存貨查詢","erp_stock_transfer"=>"貨倉調貨","06_stats"=>"報表統計","employee_evaluation"=>"員工評核","flow_stats"=>"流量分析","goods_stats"=>"Big Data - 產品","image_compression_usages"=>"Tiny使用量報告","order_goods_cross_check_report"=>"訂單產品核對報告","order_progress_report"=>"出貨進度","report_ex_pos"=>"展場報告","report_guest"=>"客戶統計","report_kpi"=>"KPI(Other)","report_kpi_market"=>"KPI(Market Team)","report_logistics"=>"物流報告","report_order"=>"訂單統計","report_order_address"=>"Big Data - 地址","report_sell"=>"銷售概況","report_users"=>"會員排行","review_cs"=>"客服評價","review_sales"=>"員工評價","review_shop"=>"友和評價","rma_report"=>"RMA報告","sale_commission"=>"佣金統計","sale_payment"=>"B2C對數","sale_profit"=>"盈利統計","sell_stats"=>"銷售排行","sql_report"=>"Data Export","stock_cost"=>"分類報告","stock_cost_brand"=>"品牌報告","stock_cost_log"=>"庫存成本紀錄","stock_cost_supplier"=>"供應商報告","supplier_turnover"=>"供應商Turnover","trends_keywords"=>"Big Data - 關鍵字趨勢","visit_buy_per"=>"Big Data - 瀏覽轉換率","warehouse_turnover"=>"貨倉Turnover","write_off"=>"產品報廢","08_members"=>"會員管理","03_users_list"=>"會員列表","04_users_add"=>"添加會員","05_user_rank_list"=>"會員等級","06_user_rank_program"=>"特殊會員計劃","10_user_account_manage"=>"資金管理","11_user_export"=>"導出會員電郵","12_upcoming_birthdays"=>"即將生日的會員","13_wishlist"=>"願望清單","account_log"=>"會員積分變動紀錄","09_priv_admin"=>"HRM系統","admin_list"=>"管理員列表","admin_logs"=>"管理員日誌","admin_role"=>"角色管理","10_accounting_system"=>"會計系統","actg_account_summary"=>"帳戶概覽","actg_transactions"=>"Book Records","bank_reconciliation"=>"Bank Records","bank_reconciliation_match"=>"Reconciliation","credit_note_summary"=>"Credit Note概覽","actg_wholesale_orders"=>"批發訂單列表","actg_xaccount_payable"=>"採購單應付賬款","actg_xaccount_receivable"=>"批發單應收賬款","actg_payment_purchase"=>"採購單付款紀錄","actg_payment_wholesale"=>"批發單付款紀錄","income_statement"=>"Income Statement","cash_flow_statement"=>"Cash Flow Statement","balance_sheet"=>"Balance Sheet","trial_balance"=>"Trial Balance","accounting_system"=>"會計紀錄","11_system"=>"系統設置","01_shop_config"=>"商店設置","021_reg_fields"=>"會員註冊項設置","02_payment_list"=>"支付方式","03_shipping_list"=>"配送方式","03_template_setup"=>"設置模板","04_mail_settings"=>"郵件服務器設置","04_template_library"=>"庫項目管理","05_area_list"=>"地區列表","05_edit_languages"=>"語言項編輯","07_cron_schcron"=>"計劃任務","09_logistics_list"=>"物流列表","captcha_manage"=>"驗證碼管理","check_file_priv"=>"文件權限檢測","edm_generator"=>"促銷郵件產生器","erp_delivery_style_setting"=>"出貨類型設置","erp_sales_agent_list"=>"代理銷售","erp_supplier_group"=>"供應商類別","erp_supplier_list"=>"供應商列表","erp_warehouse_list"=>"貨倉列表","erp_warehousing_style_setting"=>"入貨類型設置","holidays"=>"公眾假期列表","mail_sent_log"=>"已發送郵件紀錄","mail_template_manage"=>"郵件模板","navigator"=>"自定義導航欄","22_danger_zone"=>"Danger Zone","danger_zone"=>"Danger Zone™");

/* 初始化 $exc 对象 */
$exc = new exchange($ecs->table("admin_user"), $db, 'user_id', 'user_name');

/*------------------------------------------------------ */
//-- 退出登录
/*------------------------------------------------------ */
if ($_REQUEST['act'] == 'logout')
{
    /* 清除cookie */
    setcookie('ECSCP[admin_id]',   '', 1, '/');
    setcookie('ECSCP[admin_pass]', '', 1, '/');

    $sess->destroy_session();

    $_REQUEST['act'] = 'login';
}

/*------------------------------------------------------ */
//-- 登陆界面
/*------------------------------------------------------ */
if ($_REQUEST['act'] == 'login')
{
    header("Expires: Mon, 30 Jun 1997 16:00:00 GMT");
    header("Cache-Control: no-cache, must-revalidate");
    header("Pragma: no-cache");

    if ((intval($_CFG['captcha']) & CAPTCHA_ADMIN) && gd_version() > 0)
    {
        $smarty->assign('gd_version', gd_version());
        $smarty->assign('random',     mt_rand());
    }

    $smarty->display('login.htm');
}

/*------------------------------------------------------ */
//-- 验证登陆信息
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'signin')
{
    if (!empty($_SESSION['captcha_word']) && (intval($_CFG['captcha']) & CAPTCHA_ADMIN))
    {
        include_once(ROOT_PATH . 'includes/cls_captcha.php');

        /* 检查验证码是否正确 */
        $validator = new captcha();
        if (!empty($_POST['captcha']) && !$validator->check_word($_POST['captcha']))
        {
            sys_msg($_LANG['captcha_error'], 1);
        }
    }

    $_POST['username'] = isset($_POST['username']) ? trim($_POST['username']) : '';
    $_POST['password'] = isset($_POST['password']) ? trim($_POST['password']) : '';

    /* 检查密码是否正确 */
    $sql = "SELECT user_id, user_name, password, last_login, action_list, last_login, suppliers_id, is_disable, role_id".
            " FROM " . $ecs->table('admin_user') .
            " WHERE user_name = '" . $_POST['username']. "' AND password = '" . md5($_POST['password']) . "'";
    $row = $db->getRow($sql);

    if ($row)
    {
        if ($row['is_disable']) {
            sys_msg($_LANG['login_disable'], 1);
        }
        // 检查是否为供货商的管理员 所属供货商是否有效
        if (!empty($row['suppliers_id']))
        {
            $supplier_is_check = suppliers_list_info(' is_check = 1 AND suppliers_id = ' . $row['suppliers_id']);
            if (empty($supplier_is_check))
            {
                sys_msg($_LANG['login_disable'], 1);
            }
        }

        // 登录成功
        set_admin_session($row['user_id'], $row['user_name'], $row['action_list'], $row['last_login'], $row['role_id']);
        $_SESSION['suppliers_id'] = $row['suppliers_id'];

        if($row['action_list'] == 'all' && empty($row['last_login']))
        {
            $_SESSION['shop_guide'] = true;
        }

        // 更新最后登录时间和IP
        $db->query("UPDATE " .$ecs->table('admin_user').
                 " SET last_login='" . gmtime() . "', last_ip='" . real_ip() . "'".
                 " WHERE user_id='$_SESSION[admin_id]'");

        if (isset($_POST['remember']))
        {
            $time = gmtime() + 3600 * 24 * 365;
            setcookie('ECSCP[admin_id]',   $row['user_id'],                            $time, '/');
            setcookie('ECSCP[admin_pass]', md5($row['password'] . $_CFG['hash_code']), $time, '/');
        }

        // 清除购物车中过期的数据
        clear_cart();

        // If login from POS, send the user back instead of heading to admin panel
        if (!empty($_COOKIE['login_from_pos']))
        {
            setcookie('login_from_pos', '', 1, '/', false, true, true);
            header('Location: /pos.php');
            exit;
        }

        ecs_header("Location: ./index.php\n");

        exit;
    }
    else
    {
        sys_msg($_LANG['login_faild'], 1);
    }
}

/*------------------------------------------------------ */
//-- 管理员列表页面
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'list')
{
    /* 模板赋值 */
    $smarty->assign('ur_here',     $_LANG['admin_list']);
    $smarty->assign('action_link', array('href'=>'privilege.php?act=add', 'text' => $_LANG['admin_add']));
    $smarty->assign('full_page',   1);
    //$smarty->assign('admin_list',  get_admin_userlist());

    /* 显示页面 */
    assign_query_info();
    $smarty->display('privilege_list.htm');
}

/*------------------------------------------------------ */
//-- 查询
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'query')
{
    $list = get_admin_userlist();
    $adminuserController->ajaxQueryAction($list["data"], $list["record_count"], false);
}

/*------------------------------------------------------ */
//-- 添加管理员页面
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'add')
{
    /* 检查权限 */
    admin_priv('admin_manage');

     /* 模板赋值 */
    $smarty->assign('ur_here',     $_LANG['admin_add']);
    $smarty->assign('action_link', array('href'=>'privilege.php?act=list', 'text' => $_LANG['admin_list']));
    $smarty->assign('form_act',    'insert');
    $smarty->assign('action',      'add');
    $smarty->assign('select_role',  $roleController->get_role_list());
    $smarty->assign('formData',$adminuserController->prepareFormData());
    $smarty->assign('preselectedRoleId',  (isset($_REQUEST['role'])?$_REQUEST['role']:null));

    /* 显示页面 */
    assign_query_info();
    $smarty->display('privilege_info.htm');
}

/*------------------------------------------------------ */
//-- 添加管理员的处理
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'insert')
{
    admin_priv('admin_manage');

    /* 判断管理员是否已经存在 */
    if (!empty($_POST['user_name']))
    {
        $is_only = $exc->is_only('user_name', stripslashes($_POST['user_name']));

        if (!$is_only)
        {
            sys_msg(sprintf($_LANG['user_name_exist'], stripslashes($_POST['user_name'])), 1);
        }
    }

    /* Email地址是否有重复 */
    if (!empty($_POST['email']))
    {
        $is_only = $exc->is_only('email', stripslashes($_POST['email']));

        if (!$is_only)
        {
            sys_msg(sprintf($_LANG['email_exist'], stripslashes($_POST['email'])), 1);
        }
    }

    $new_id = new Yoho\cms\Model\Adminuser(null,$_POST);
    
    if($new_id->getId()>0){
        $new_id = $new_id->getId();
    }else{
        sys_msg('Error while creating Admin User',1,'privilege.php?act=list');
    }

    /*添加链接*/
    $link[0]['text'] = $_LANG['go_allot_priv'];
    $link[0]['href'] = 'privilege.php?act=allot&id='.$new_id.'&user='.$_POST['user_name'].'';

    $link[1]['text'] = $_LANG['continue_add'];
    $link[1]['href'] = 'privilege.php?act=add';

    sys_msg($_LANG['add'] . "&nbsp;" .$_POST['user_name'] . "&nbsp;" . $_LANG['action_succeed'],0, $link);

    /* 记录管理员操作 */
    admin_log($_POST['user_name'], 'add', 'privilege');
 }

/*------------------------------------------------------ */
//-- 编辑管理员信息
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'edit')
{
    /* 不能编辑demo这个管理员 */
    if ($_SESSION['admin_name'] == 'demo')
    {
       $link[] = array('text' => $_LANG['back_list'], 'href'=>'privilege.php?act=list');
       sys_msg($_LANG['edit_admininfo_cannot'], 0, $link);
    }

    $_REQUEST['id'] = !empty($_REQUEST['id']) ? intval($_REQUEST['id']) : 0;

    /* 查看是否有权限编辑其他管理员的信息 */
    if ($_SESSION['admin_id'] != $_REQUEST['id'])
    {
        admin_priv('admin_manage');
    }

    /* 获取管理员信息 */
    $sql = "SELECT u.user_id, u.user_name, u.email, u.password, u.agency_id, u.role_id, sp.available FROM " .$ecs->table('admin_user')." u "." LEFT JOIN ".$ecs->table('salesperson')." sp ON sp.admin_id=u.user_id AND sp.sales_name=u.user_name WHERE u.user_id = '".$_REQUEST['id']."' ";
    $user_info = $db->getRow($sql);


    /* 取得该管理员负责的办事处名称 */
    if ($user_info['agency_id'] > 0)
    {
        $sql = "SELECT agency_name FROM " . $ecs->table('agency') . " WHERE agency_id = '$user_info[agency_id]'";
        $user_info['agency_name'] = $db->getOne($sql);
    }

    /* 模板赋值 */
    $smarty->assign('ur_here',     $_LANG['admin_edit']);
    $smarty->assign('action_link', array('text' => $_LANG['admin_list'], 'href'=>'privilege.php?act=list'));
    $smarty->assign('action_link2', array('text' => $_LANG['admin_edit_nav_list'], 'href'=>'privilege.php?act=nav_modif&id='.$_REQUEST['id']));
    $smarty->assign('user',        $user_info);
    $smarty->assign('formData',$adminuserController->prepareFormData($user_info['user_id']));

    /* 获得该管理员的权限 */
    $priv_str = $db->getOne("SELECT action_list FROM " .$ecs->table('admin_user'). " WHERE user_id = '$_GET[id]'");

    /* 如果被编辑的管理员拥有了all这个权限，将不能编辑 */
    if ($priv_str != 'all')
    {
       $smarty->assign('select_role',  $roleController->get_role_list());
    }
    $smarty->assign('sp_role_ids',implode(',', $roleController->getRoleIdAutocreatesp()));
    $smarty->assign('form_act',    'update');
    $smarty->assign('action',      'edit');

    assign_query_info();
    $smarty->display('privilege_info.htm');
}

/*------------------------------------------------------ */
//-- 更新管理员信息
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'update' || $_REQUEST['act'] == 'update_self')
{

    /* 变量初始化 */
    $admin_id    = !empty($_REQUEST['id'])        ? intval($_REQUEST['id'])      : 0;
    $admin_name  = !empty($_REQUEST['user_name']) ? trim($_REQUEST['user_name']) : '';
    $admin_email = !empty($_REQUEST['email'])     ? trim($_REQUEST['email'])     : '';

    $password = !empty($_POST['new_password']) ? ", password = '".md5($_POST['new_password'])."'"    : '';
    if ($_REQUEST['act'] == 'update')
    {
        /* 查看是否有权限编辑其他管理员的信息 */
        if ($_SESSION['admin_id'] != $_REQUEST['id'])
        {
            admin_priv('admin_manage');
        }
        $g_link = 'privilege.php?act=list';
    }
    else
    {
        $admin_id = $_SESSION['admin_id'];
        $g_link = 'privilege.php?act=modif';
    }

    $nav_arr = [];
    $nav_list = "";
    foreach (explode(",", $_POST['nav_list'][0]) as $nav_item) {
        if (!in_array($nav_item, $nav_arr)) {
            $nav_arr[] = $nav_item;
        }
    }
    if (!empty($nav_arr)) {
        $nav_list = ", nav_list = '" . implode(",", $nav_arr) . "'";
    }

    /* 判断管理员是否已经存在 */
    if (!empty($admin_name))
    {
        $is_only = $exc->num('user_name', stripslashes($admin_name), $admin_id);
        if ($is_only == 1)
        {
            sys_msg(sprintf($_LANG['user_name_exist'], stripslashes($admin_name)), 1);
        }
    }

    /* Email地址是否有重复 */
    if (!empty($admin_email))
    {
        $is_only = $exc->num('email', stripslashes($admin_email), $admin_id);

        if ($is_only == 1)
        {
            sys_msg(sprintf($_LANG['email_exist'], stripslashes($admin_email)), 1);
        }
    }

    //如果要修改密码
    $pwd_modified = false;

    if (!empty($_POST['new_password']))
    {
        /* 查询旧密码并与输入的旧密码比较是否相同 */
        $sql = "SELECT password FROM ".$ecs->table('admin_user')." WHERE user_id = '$admin_id'";
        $old_password = $db->getOne($sql);
        if ($old_password <> (md5($_POST['old_password'])))
        {
           $link[] = array('text' => $_LANG['go_back'], 'href'=>'javascript:history.back(-1)');
           sys_msg($_LANG['pwd_error'], 0, $link);
        }

        /* 比较新密码和确认密码是否相同 */
        if ($_POST['new_password'] <> $_POST['pwd_confirm'])
        {
           $link[] = array('text' => $_LANG['go_back'], 'href'=>'javascript:history.back(-1)');
           sys_msg($_LANG['js_languages']['password_error'], 0, $link);
        }
        else
        {
            $pwd_modified = true;
        }
    }

    $role_id = '';
    $action_list = '';
    $updateAdminRole=true;
    if (!empty($_POST['select_role']))
    {
        $sql = "SELECT action_list FROM " . $ecs->table('role') . " WHERE role_id = '".$_POST['select_role']."'";
        $row = $db->getRow($sql);
        $action_list = ', action_list = \''.$row['action_list'].'\'';
        $role_id = ', role_id = '.$_POST['select_role'].' ';
        $updateAdminRole = $adminuserController->updateRole($admin_id,$_POST['select_role']);
    }
    

    //更新管理员信息
    $sql = "UPDATE " .$ecs->table('admin_user'). " SET ".
           "user_name = '$admin_name', ".
           "email = '$admin_email' ".
           $action_list.
           $role_id.
           $password.
           $nav_list.
           "WHERE user_id = '$admin_id'";

   if($adminuserController->updateFormData($admin_id,$_POST)===false)
    sys_msg("Core information updated, but error occured while updating additional information.",1);
   
   if($updateAdminRole===false)
    sys_msg("Error while attempting to update Admin-Salesperson role.",1);

   $db->query($sql);

   /* 记录管理员操作 */
   admin_log($_POST['user_name'], 'edit', 'privilege');

   /* 如果修改了密码，则需要将session中该管理员的数据清空 */
   if ($pwd_modified && $_REQUEST['act'] == 'update_self')
   {
       $sess->delete_spec_admin_session($_SESSION['admin_id']);
       $msg = $_LANG['edit_password_succeed'];
   }
   else
   {
       $msg = $_LANG['edit_profile_succeed'];
   }

   /* 提示信息 */
   $link[] = array('text' => strpos($g_link, 'list') ? $_LANG['back_admin_list'] : $_LANG['modif_info'], 'href'=>$g_link);
   sys_msg("$msg<script>parent.document.getElementById('header-frame').contentWindow.document.location.reload();</script>", 0, $link);

}

/*------------------------------------------------------ */
//-- 编辑个人资料
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'modif' || $_REQUEST['act'] == 'nav_modif')
{

    /* 不能编辑demo这个管理员 */
    if ($_SESSION['admin_name'] == 'demo')
    {
       $link[] = array('text' => $_LANG['back_admin_list'], 'href'=>'privilege.php?act=list');
       sys_msg($_LANG['edit_admininfo_cannot'], 0, $link);
    }

    if ($_REQUEST['act'] == 'nav_modif' && empty($_REQUEST['id'])) {
       $link[] = array('text' => $_LANG['back_admin_list'], 'href'=>'privilege.php?act=list');
       sys_msg($_LANG['edit_admininfo_cannot'], 0, $link);
    }

    include_once('includes/inc_menu.php');

    /* 包含插件菜单语言项 */
    $sql = "SELECT code FROM ".$ecs->table('plugins');
    $rs = $db->query($sql);
    while ($row = $db->FetchRow($rs))
    {
        /* 取得语言项 */
        if (file_exists(ROOT_PATH.'plugins/'.$row['code'].'/languages/common_'.$_CFG['lang'].'.php'))
        {
            include_once(ROOT_PATH.'plugins/'.$row['code'].'/languages/common_'.$_CFG['lang'].'.php');
        }

        /* 插件的菜单项 */
        if (file_exists(ROOT_PATH.'plugins/'.$row['code'].'/languages/inc_menu.php'))
        {
            include_once(ROOT_PATH.'plugins/'.$row['code'].'/languages/inc_menu.php');
        }
    }

    /* 获得当前管理员数据信息 */
    $sql = "SELECT user_id, user_name, email, nav_list ".
           "FROM " .$ecs->table('admin_user'). " WHERE user_id = '". ($_REQUEST['act'] == 'modif' ? $_SESSION['admin_id'] : $_REQUEST['id']) ."'";
    $user_info = $db->getRow($sql);

    /* 获取导航条 */
    $nav_arr = (trim($user_info['nav_list']) == '') ? array() : explode(",", $user_info['nav_list']);
    $nav_lst = array();
    foreach ($nav_arr AS $val)
    {
        $arr              = explode('|', $val);
        $nav_lst[$arr[1]] = $arr[0];
    }

    /* 模板赋值 */
    $smarty->assign('lang',        $_LANG);
    $smarty->assign('ur_here',     $_REQUEST['act'] == 'modif' ? $_LANG['modif_info'] : $_LANG['admin_edit_nav_list']);
    $smarty->assign('action_link', array('text' => $_LANG['admin_list'], 'href'=>'privilege.php?act=list'));
    $smarty->assign('user',        $user_info);

    //block non-super admin 
    if($_SESSION['manage_cost'] || (!$_SESSION['manage_cost'] && check_authz(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_SUPERADMIN)==true) ){
        $smarty->assign('list_menus',  $menus);
        $smarty->assign('nav_arr',     $nav_lst);
    }

    $smarty->assign('form_act',    $_REQUEST['act'] == 'modif' ? 'update_self' : 'update');
    $smarty->assign('action',      'modif');

    /* 显示页面 */
    assign_query_info();
    $smarty->display('privilege_info.htm');
}

/*------------------------------------------------------ */
//-- 为管理员分配权限
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'allot')
{
    include_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/admin/priv_action.php');
    admin_priv('allot_priv');

    if ($_SESSION['admin_id'] == $_GET['id'])
    {
        admin_priv('all');
    }

    $cat = $_GET["cat"];

    /* 获得该管理员的权限 */
    $priv_str = $db->getOne("SELECT action_list FROM " .$ecs->table('admin_user'). " WHERE user_id = '$_GET[id]'");

    /* 如果被编辑的管理员拥有了all这个权限，将不能编辑 */
    if ($priv_str == 'all')
    {
       $link[] = array('text' => $_LANG['back_admin_list'], 'href'=>'privilege.php?act=list');
       sys_msg($_LANG['edit_admininfo_cannot'], 0, $link);
    }


    /* 按权限组查询底级的权限名称 */
    $sql = "SELECT action_id, parent_id, action_code, relevance, IF(parent_cat IS NULL or parent_cat = '', 'unclassified', parent_cat) as parent_cat, remarks FROM " .$ecs->table('admin_action').
           " WHERE parent_id > 0".
           " ORDER BY remarks";
    $result = $db->query($sql);
    while ($priv = $db->FetchRow($result))
    {
        $priv['action_code_alt']=$priv['action_code'].'_alt';
        if($priv['remarks'] == "menu"){
            $priv_arr[$priv["parent_cat"]]["priv_menu"][$priv["action_code"]] = $priv;
        }
        else{
            $priv_arr[$priv["parent_cat"]]["priv"][$priv["action_code"]] = $priv;
        }
    }

    //print_r($priv_arr);
    // 将同一组的权限使用 "," 连接起来，供JS全选
    foreach ($priv_arr AS $parent_cat => $action_group)
    {
        $priv_arr[$parent_cat]['priv_list'] = join(',', @array_keys($action_group['priv']));

        foreach ($action_group['priv'] AS $key => $val)
        {
            $priv_arr[$parent_cat]['priv'][$key]['cando'] = (strpos($priv_str, $val['action_code']) !== false || $priv_str == 'all') ? 1 : 0;
        }

        foreach ($action_group['priv_menu'] AS $key => $val)
        {
            $priv_arr[$parent_cat]['priv_menu'][$key]['cando'] = (strpos($priv_str, $val['action_code']) !== false || $priv_str == 'all') ? 1 : 0;
        }
    }

    /* 赋值 */
    $smarty->assign('lang',        $_LANG);
    $smarty->assign('ur_here',     $_LANG['allot_priv'] . ' [ '. $_GET['user'] . ' ] ');
    $smarty->assign('action_link', array('href'=>'privilege.php?act=list', 'text' => $_LANG['admin_list']));
    $smarty->assign('priv_arr',    $priv_arr);
    $smarty->assign('current_act', 'allot');
    $smarty->assign('form_act',    'update_allot');
    $smarty->assign('user_id',     $_GET['id']);
    $smarty->assign('user_name',   $_GET['user']);
    $smarty->assign('cat',         $cat);

    $smarty->assign('list_menus',  $menus);

    /* 显示页面 */
    assign_query_info();
    $smarty->display('privilege_allot.htm');
}

/*------------------------------------------------------ */
//-- 更新管理员的权限
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'update_allot')
{
    admin_priv('admin_manage');

    /* 取得当前管理员用户名 */
    $admin_name = $db->getOne("SELECT user_name FROM " .$ecs->table('admin_user'). " WHERE user_id = '$_POST[id]'");

    /* 更新管理员的权限 */
    $act_list = @join(",", $_POST['action_code']);
    $sql = "UPDATE " .$ecs->table('admin_user'). " SET action_list = '$act_list', role_id = '' ".
           "WHERE user_id = '$_POST[id]'";

    $db->query($sql);
    /* 动态更新管理员的SESSION */
    if ($_SESSION["admin_id"] == $_POST['id'])
    {
        $_SESSION["action_list"] = $act_list;
    }

    /* 记录管理员操作 */
    admin_log(addslashes($admin_name), 'edit', 'privilege');

    /* 提示信息 */
    $link[] = array('text' => $_LANG['back_admin_list'], 'href'=>'privilege.php?act=list');
    sys_msg($_LANG['edit'] . "&nbsp;" . $admin_name . "&nbsp;" . $_LANG['action_succeed'], 0, $link);

}

/*------------------------------------------------------ */
//-- 删除一个管理员
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'remove')
{
    check_authz_json('admin_drop');

    $id = intval($_GET['id']);

    /* 获得管理员用户名 */
    $admin_name = $db->getOne('SELECT user_name FROM '.$ecs->table('admin_user')." WHERE user_id='$id'");

    /* demo这个管理员不允许删除 */
    if ($admin_name == 'demo')
    {
        make_json_error($_LANG['edit_remove_cannot']);
    }

    /* ID为1的不允许删除 */
    if ($id == 1)
    {
        make_json_error($_LANG['remove_cannot']);
    }

    /* 管理员不能删除自己 */
    if ($id == $_SESSION['admin_id'])
    {
        make_json_error($_LANG['remove_self_cannot']);
    }

    if ($exc->drop($id))
    {
        $sp = $spController->getByAdmin($id);
        if($sp!==false)
            $spController->archive($sp->getId());

        $sess->delete_spec_admin_session($id); // 删除session中该管理员的记录

        admin_log(addslashes($admin_name), 'remove', 'privilege');
        clear_cache_files();
    }

    $url = 'privilege.php?act=query&' . str_replace('act=remove', '', $_SERVER['QUERY_STRING']);

    ecs_header("Location: $url\n");
    exit;
} elseif ($_REQUEST['act'] == 'ajaxEdit') {
    check_authz_json('admin_manage');
    $adminuserController->ajaxEditAction();
}

/* 获取管理员列表 */
function get_admin_userlist()
{
    global $adminuserController;

    $_REQUEST['sort_by']    = empty($_REQUEST['sort_by']) ? 'user_id' : trim($_REQUEST['sort_by']);
    $_REQUEST['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
    
    $list = array();
    if ($_REQUEST['sort_by'] == 'user_id') {
        $_REQUEST['sort_by'] = "u." . $_REQUEST['sort_by'];
    }

    $sql  = 'SELECT count(*) '.
            'FROM ' .$GLOBALS['ecs']->table('admin_user').' u LEFT JOIN '.$GLOBALS['ecs']->table('role').' r ON r.role_id=u.role_id '.
            ' LEFT JOIN '.$GLOBALS['ecs']->table('salesperson').' sp ON sp.admin_id=u.user_id AND sp.sales_name=u.user_name '.
            ' WHERE u.is_disable = 0';
    $record_count = $GLOBALS['db']->getOne($sql);

    $sql  = 'SELECT u.user_id, u.user_name, u.email, u.add_time, u.is_disable, u.last_login,r.role_name, sp.available '.
            'FROM ' .$GLOBALS['ecs']->table('admin_user').' u LEFT JOIN '.$GLOBALS['ecs']->table('role').' r ON r.role_id=u.role_id '.
            ' LEFT JOIN '.$GLOBALS['ecs']->table('salesperson').' sp ON sp.admin_id=u.user_id AND sp.sales_name=u.user_name '.
            ' WHERE u.is_disable = 0' .
            ' ORDER BY is_disable ASC, ' . $_REQUEST['sort_by'] . ' ' . $_REQUEST['sort_order'].
            ' LIMIT ' . $_REQUEST['start'] . ',' . $_REQUEST['page_size'];
    $record = $GLOBALS['db']->getAll($sql);

    foreach ($record AS $key=>$val)
    {
        $record[$key]['add_time']     = local_date($GLOBALS['_CFG']['time_format'], $val['add_time']);
        $record[$key]['last_login']   = local_date($GLOBALS['_CFG']['time_format'], $val['last_login']);
        if (empty($val['role_name'])) {
            $record[$key]['role_name'] = " ";
        }
        if ($val['available'] == 0 && !empty($val['role_name'])) {
            $record[$key]['role_name'] = '<font style="color:grey">' . $record[$key]['role_name'] . '</font>';
        }
        $allot_btn = [
            'link' => 'privilege.php?act=allot&id=' . $val['user_id'] . '&user=' . $val['user_name'],
            'title' => $GLOBALS['_LANG']['allot_priv'],
            'label' => '權限',
            'btn_class' => 'btn btn-success'
        ];
        $log_btn = [
            'link' => 'admin_logs.php?act=list&id=' . $val['user_id'],
            'title' => $GLOBALS['_LANG']['view_log'],
            'label' => '日誌',
            'btn_class' => 'btn btn-default'
        ];
        $custom_actions = ['allot' => $allot_btn, 'log' => $log_btn];
        $record[$key]['_action'] = $adminuserController->generateCrudActionBtn($val['user_id'], 'id', null, ['edit', 'remove'], $custom_actions);
    }
    
    $list["data"] = $record;
    $list["record_count"] = $record_count;

    return $list;
}

/* 清除购物车中过期的数据 */
function clear_cart()
{
    /* 取得有效的session */
    $sql = "SELECT DISTINCT session_id " .
            "FROM " . $GLOBALS['ecs']->table('cart') . " AS c, " .
                $GLOBALS['ecs']->table('sessions') . " AS s " .
            "WHERE c.session_id = s.sesskey ";
    $valid_sess = $GLOBALS['db']->getCol($sql);

    // 删除cart中无效的数据
    $sql = "DELETE FROM " . $GLOBALS['ecs']->table('cart') .
            " WHERE session_id NOT " . db_create_in($valid_sess);
    $GLOBALS['db']->query($sql);
}

?>
