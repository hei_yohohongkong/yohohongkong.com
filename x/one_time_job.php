<?php
error_reporting(E_ALL);

define('IN_ECS', true);
ini_set('memory_limit', '256M');
require(dirname(__FILE__) . '/includes/init.php');

$job = trim($_REQUEST['job']);
if (empty($job)) {
    die("No job selected");
}

$feedController = new Yoho\cms\Controller\FeedController();
if ($job == 'init_all_perma') {
    // Initiate all permanent link
    $reset = empty($_REQUEST['reset']) ? 0 : intval($_REQUEST['reset']);
    $feedController->init_all_permanent_link($reset);
} elseif ($job == 'delete_all_cache') {
    // Clear all permanent link cache
    $reset = empty($_REQUEST['reset']) ? 0 : intval($_REQUEST['reset']);
    $feedController->clear_all_permanent_link($reset);
} elseif ($job == 'get_perma_cache') {
    // Get permanent link cache
    $table = empty($_REQUEST['table']) ? '' : $_REQUEST['table'];
    if (empty($table)) die("No table selected");
    $id = empty($_REQUEST['id']) ? 0 : $_REQUEST['id'];
    $lang = empty($_REQUEST['lang']) ? '' : $_REQUEST['lang'];
    $cache = $feedController->get_permanent_link_cache($table, $id, $lang);
    var_dump($cache);
} elseif ($job == 'delete_perma_cache') {
    // Clear permanent link cache
    $table = empty($_REQUEST['table']) ? '' : $_REQUEST['table'];
    if (empty($table)) die("No table selected");
    $reset = empty($_REQUEST['reset']) ? 0 : intval($_REQUEST['reset']);
    $feedController->clear_permanent_link($table, $reset);
} elseif ($job == 'refresh_perma_cache') {
    // Regenerate permanent link cache
    $table = empty($_REQUEST['table']) ? '' : $_REQUEST['table'];
    if (empty($table)) die("No table selected");
    $reset = empty($_REQUEST['reset']) ? 0 : intval($_REQUEST['reset']);
    $id = empty($_REQUEST['id']) ? 0 : $_REQUEST['id'];
    $feedController->init_permanent_link($table, $id, $reset);
} elseif ($job == 'migrate_perma_cache') {
    // Migrate all permanent link cache to single table
    $feedController->init_permanent_link_table();
} elseif ($job == 'test_sitemap') {
    include_once(ROOT_PATH . ADMIN_PATH . '/includes/cls_phpzip.php');
    include_once(ROOT_PATH . ADMIN_PATH . '/includes/cls_google_sitemap.php');
    $time_start = microtime(true);
    $feedController = new Yoho\cms\Controller\FeedController();
    if ($GLOBALS['_CFG']['perma_link_cache'] || !empty($_REQUEST['test_perma_link'])) $feedController->init_all_permanent_link(1);
    $feedController->addAllProduct();

    $domain = $ecs->url();
    $domain = str_replace('https://', "http://", $domain); // dem: force http
    $domain = substr($domain, 0, -1); // howang: I modified build_uri() to include /, so we need to remove it from the url here
    $today  = local_date('Y-m-d');
    $config = unserialize($_CFG['sitemap']);
    $config['limit'] = 10000;

    $sm  = new google_sitemap();
    $smi = new google_sitemap_item($domain, $today, $config['homepage_changefreq'], $config['homepage_priority']);

    $sm->add_item($smi);

    /* 商品分类 */
    $sql = "SELECT cat_id,cat_name FROM " .$ecs->table('category'). " ORDER BY parent_id";
    $res = $db->getAll($sql);
    $data_res['category'] = $res;

    foreach ($res as $row)
    {
        $smi = new google_sitemap_item($domain . build_uri('category', array('cid' => $row['cat_id'], 'cname' => $row['cat_name']), $row['cat_name']), $today,
            $config['category_changefreq'], $config['category_priority']);
        $sm->add_item(google_sitemap::SITEMAP_SECTION_CAT,$smi);
    }

    /* 商品品牌 */
    $sql = "SELECT brand_id,brand_name FROM " .$ecs->table('brand'). " ";
    $res = $db->getAll($sql);
    $data_res['brand'] = $res;
    foreach ($res as $row)
    {
        $smi = new google_sitemap_item($domain . build_uri('brand', array('bid' => $row['brand_id'], 'bname' => $row['brand_name']), $row['brand_name']), $today,
            $config['category_changefreq'], $config['category_priority']);
        $sm->add_item(google_sitemap::SITEMAP_SECTION_BRAND,$smi);
    }

    /* 文章分类 */
    $sql = "SELECT cat_id,cat_name FROM " .$ecs->table('article_cat'). " WHERE cat_type=1";
    $res = $db->getAll($sql);
    $data_res['article_category'] = $res;

    foreach ($res as $row)
    {
        $smi = new google_sitemap_item($domain . build_uri('article_cat', array('acid' => $row['cat_id'], 'acname' => $row['cat_name']), $row['cat_name']), $today,
            $config['category_changefreq'], $config['category_priority']);
        $sm->add_item(google_sitemap::SITEMAP_SECTION_ARTICLECAT,$smi);
    }

    /* 商品 */
    // 2018-07-01, we only show the product before the date create. The product after the date creates, put it in sitemap with https.
    $https_start_time = local_strtotime('2018-07-01');
    $sql = "SELECT goods_id, goods_name FROM " .$ecs->table('goods'). " WHERE is_delete = 0 AND is_hidden = 0 and add_time <'".$https_start_time."' ";
    $res = $db->query($sql);

    while ($row = $db->fetchRow($res))
    {
        $smi = new google_sitemap_item($domain . build_uri('goods', array('gid' => $row['goods_id'], 'gname' => $row['goods_name']), $row['goods_name']), $today,
            $config['content_changefreq'], $config['content_priority']);
        $sm->add_item(google_sitemap::SITEMAP_SECTION_PRODUCT,$smi);
    }

    /* 文章 */
    $sql = "SELECT article_id,title,file_url,open_type FROM " .$ecs->table('article'). " WHERE is_open=1";
    $res = $db->getAll($sql);
    $data_res['article'] = $res;

    foreach ($res as $row)
    {
        $article_url=$row['open_type'] != 1 ? build_uri('article', array('aid'=>$row['article_id'], 'aname' => $row['title']), $row['title']) : trim($row['file_url']);
        $smi = new google_sitemap_item($domain . $article_url,
            $today, $config['content_changefreq'], $config['content_priority']);
        $sm->add_item(google_sitemap::SITEMAP_SECTION_ARTICLE,$smi);
    }

    clear_cache_files();	// 清除缓存

    if (!file_exists(ROOT_PATH . DATA_DIR . '/test_sitemap')) {
        mkdir(ROOT_PATH . DATA_DIR . '/test_sitemap');
    }
    if (!file_exists(ROOT_PATH . DATA_DIR . '/test_sitemap/data')) {
        mkdir(ROOT_PATH . DATA_DIR . '/test_sitemap/data');
    }

    $sm_file = ROOT_PATH . DATA_DIR . '/test_sitemap/sitemap.xml';
    try{
        $feedController->buildFeed();
        $res=$sm->build($sm_file,$ecs->url());
        if($res!==true){
            $sm_file = ROOT_PATH . DATA_DIR . '/test_sitemap/data/sitemap.xml';
            $res=$sm->build($sm_file,$ecs->url());
            if($res!==true){
                var_dump('Sitemap generation error:'.$res);
            }else{
                $sitemap_url = $ecs->url() . 'sitemap.xml';
                var_dump('Sitemap generated: ' . $sitemap_url);
            }
        }else{
            $sitemap_url = $ecs->url() . 'sitemap.xml';
            var_dump('Sitemap generated: ' . $sitemap_url);
        }

    }catch(Exception $e){
        var_dump('Sitemap generation error:'.$e->getMessage());
    }

    // to generate sitemap with https start  added by eric
    $domain = $ecs->url();
    $domain = str_replace('http://', "https://", $domain);
    $domain = substr($domain, 0, -1); // remove '/' from the url tail
    $today  = local_date('Y-m-d');
    $config = unserialize($_CFG['sitemap']);
    $config['limit'] = 1000;

    $sm  = new google_sitemap();
    $smi = new google_sitemap_item($domain, $today, $config['homepage_changefreq'], $config['homepage_priority']);

    /* 商品 */
    // 2018-07-01, we only show the product before the date create. The product after the date creates, put it in sitemap with https.
    $https_start_time = local_strtotime('2018-07-01');
    $sql = "SELECT goods_id, goods_name FROM " .$ecs->table('goods'). " WHERE is_delete = 0 AND is_hidden = 0 and add_time >='".$https_start_time."' ";
    $res = $db->query($sql);

    while ($row = $db->fetchRow($res))
    {
        $smi = new google_sitemap_item($domain . build_uri('goods', array('gid' => $row['goods_id'], 'gname' => $row['goods_name']), $row['goods_name']), $today,
            $config['content_changefreq'], $config['content_priority']);
        $sm->add_item(google_sitemap::SITEMAP_SECTION_PRODUCT,$smi);
    }

    /* Promotions */
    $sql = "SELECT t.topic_id, IFNULL(tl.title, t.title) as title " .
        "FROM " . $ecs->table('topic') . " as t " .
        "LEFT JOIN " . $ecs->table('topic_lang') . " as tl ON tl.topic_id = t.topic_id AND tl.lang = '" . $_CFG['lang'] . "' " .
        "ORDER BY t.sort_order ASC, t.topic_id DESC";
    $res = $db->getAll($sql);
    $data_res['topic'] = $res;

    foreach ($res as $row)
    {
        $topic_url=build_uri('topic', array('tid' => $row['topic_id'], 'tname' => $row['title']), $row['title']);
        $smi = new google_sitemap_item($domain . $topic_url,
            $today, $config['content_changefreq'], $config['content_priority']);
        $sm->add_item(google_sitemap::SITEMAP_SECTION_TOPIC,$smi);
    }

    /* tags */
    $sql = "SELECT distinct (tag_words) " .
        "FROM " . $ecs->table('tag') . " ";
    $res = $db->getAll($sql);
    $data_res['tag'] = $res;

    foreach ($res as $row)
    {
        $topic_url= "/keyword/".urlencode($row['tag_words']);
        $smi = new google_sitemap_item($domain . $topic_url,
            $today, $config['content_changefreq'], $config['content_priority']);
        $sm->add_item(google_sitemap::SITEMAP_SECTION_TAG,$smi);
    }

    clear_cache_files();	// 清除缓存

    $sm_file = ROOT_PATH . DATA_DIR . '/test_sitemap/sitemap_https.xml';
    try{
        $res=$sm->build($sm_file,$ecs->url());
        if($res!==true){
            $sm_file = ROOT_PATH . DATA_DIR . '/test_sitemap/data/sitemap_https.xml';
            $res=$sm->build($sm_file,$ecs->url());
            if($res!==true){
                var_dump('Sitemap generation error:'.$res);
            }else{
                $sitemap_url = $ecs->url() . 'sitemap_https.xml';
                var_dump('Sitemap generated: ' . $sitemap_url);
            }
        }else{
            $sitemap_url = $ecs->url() . 'sitemap_https.xml';
            var_dump('Sitemap generated: ' . $sitemap_url);
        }

    }catch(Exception $e){
        var_dump('Sitemap generation error:'.$e->getMessage());
    }
    // to generate sitemap with https end


    // to generate sitemap with https start (baidu)  added by eric
    $domain = $ecs->url();
    $domain = str_replace('http://', "https://", $domain);
    $domain = substr($domain, 0, -1); // remove '/' from the url tail
    $today  = local_date('Y-m-d');
    $config = unserialize($_CFG['sitemap']);
    $config['limit'] = 10000;

    $sm  = new google_sitemap();
    $smi = new google_sitemap_item($domain, $today, $config['homepage_changefreq'], $config['homepage_priority']);

    /* 商品 */
    $sql = "SELECT goods_id, goods_name FROM " .$ecs->table('goods'). " WHERE is_delete = 0 AND is_hidden = 0 ";
    $res = $db->getAll($sql);

    $index = 0;
    $limit = $config['limit'];
    foreach ($res as $key => $row)
    {
        $smi = new google_sitemap_item($domain . build_uri('goods', array('gid' => $row['goods_id'], 'gname' => $row['goods_name']), $row['goods_name']), $today,
            $config['content_changefreq'], $config['content_priority']);
        $sm->add_item(google_sitemap::SITEMAP_SECTION_PRODUCT.'_'.$index,$smi);

        if ($key > $limit) {
            $limit += $config['limit'];
            $index++;
        }
    }

    /* tags */
    $index = 0;
    $limit = $config['limit'];
    foreach ($data_res['tag'] as $key => $row)
    {
        $topic_url= "/keyword/".urlencode($row['tag_words']);
        $smi = new google_sitemap_item($domain . $topic_url,
            $today, $config['content_changefreq'], $config['content_priority']);
        $sm->add_item(google_sitemap::SITEMAP_SECTION_TAG.'_'.$index,$smi);

        if ($key > $limit) {
            $limit += $config['limit'];
            $index++;
        }
    }

    /* Promotions */
    $index = 0;
    $limit = $config['limit'];
    foreach ($data_res['topic'] as $key => $row)
    {
        $topic_url=build_uri('topic', array('tid' => $row['topic_id'], 'tname' => $row['title']), $row['title']);
        $smi = new google_sitemap_item($domain . $topic_url,
            $today, $config['content_changefreq'], $config['content_priority']);
        $sm->add_item(google_sitemap::SITEMAP_SECTION_TOPIC.'_'.$index,$smi);

        if ($key > $limit) {
            $limit += $config['limit'];
            $index++;
        }
    }

    /* 文章 */
    $index = 0;
    $limit = $config['limit'];
    foreach ($data_res['article'] as $key => $row)
    {
        $article_url=$row['open_type'] != 1 ? build_uri('article', array('aid'=>$row['article_id'], 'aname' => $row['title']), $row['title']) : trim($row['file_url']);
        $smi = new google_sitemap_item($domain . $article_url,
            $today, $config['content_changefreq'], $config['content_priority']);
        $sm->add_item(google_sitemap::SITEMAP_SECTION_ARTICLE.'_'.$index,$smi);

        if ($key > $limit) {
            $limit += $config['limit'];
            $index++;
        }
    }

    /* 文章分类 */
    $index = 0;
    $limit = $config['limit'];
    foreach ($data_res['article_category'] as $key => $row)
    {
        $smi = new google_sitemap_item($domain . build_uri('article_cat', array('acid' => $row['cat_id'], 'acname' => $row['cat_name']), $row['cat_name']), $today,
            $config['category_changefreq'], $config['category_priority']);
        $sm->add_item(google_sitemap::SITEMAP_SECTION_ARTICLECAT.'_'.$index,$smi);

        if ($key > $limit) {
            $limit += $config['limit'];
            $index++;
        }
    }

    /* 商品品牌 */
    $index = 0;
    $limit = $config['limit'];
    foreach ($data_res['brand'] as $key => $row)
    {
        $smi = new google_sitemap_item($domain . build_uri('brand', array('bid' => $row['brand_id'], 'bname' => $row['brand_name']), $row['brand_name']), $today,
            $config['category_changefreq'], $config['category_priority']);
        $sm->add_item(google_sitemap::SITEMAP_SECTION_BRAND.'_'.$index,$smi);

        if ($key > $limit) {
            $limit += $config['limit'];
            $index++;
        }
    }

    /* 商品分类 */
    $index = 0;
    $limit = $config['limit'];
    foreach ($data_res['category'] as $key => $row)
    {
        $smi = new google_sitemap_item($domain . build_uri('category', array('cid' => $row['cat_id'], 'cname' => $row['cat_name']), $row['cat_name']), $today,
            $config['category_changefreq'], $config['category_priority']);
        $sm->add_item(google_sitemap::SITEMAP_SECTION_CAT.'_'.$index,$smi);

        if ($key > $limit) {
            $limit += $config['limit'];
            $index++;
        }
    }

    clear_cache_files();	// 清除缓存

    $sm_file = ROOT_PATH . DATA_DIR . '/test_sitemap/sitemap_baidu.xml';
    try{
        $res=$sm->build($sm_file,$ecs->url());
        if($res!==true){
            $sm_file = ROOT_PATH . DATA_DIR . '/test_sitemap/data/sitemap_baidu.xml';
            $res=$sm->build($sm_file,$ecs->url());
            if($res!==true){
                var_dump('Sitemap generation error:'.$res);
            }else{
                $sitemap_url = $ecs->url() . 'sitemap_baidu.xml';
                var_dump('Sitemap generated: ' . $sitemap_url);
            }
        }else{
            $sitemap_url = $ecs->url() . 'sitemap_baidu.xml';
            var_dump('Sitemap generated: ' . $sitemap_url);
        }

    }catch(Exception $e){
        var_dump('Sitemap generation error:'.$e->getMessage());
    }

    var_dump("Time Used: " . round(microtime(true) - $time_start, 2));
} elseif ($job == 'update_ga_landing_data') {
    ini_set('max_execution_time', 1800);
    $max_days = empty($_REQUEST['max_days']) ? 10 : intval($_REQUEST['max_days']);
    $custom_date = empty($_REQUEST['date']) ? '' : trim($_REQUEST['date']);
    $dates = $db->getCol("SELECT date FROM " . $ecs->table('google_statistics') . (" WHERE date >= '$custom_date'") . " GROUP BY date HAVING SUM(landing_session + landing_pageview + landing_pageview_unique) = 0 LIMIT $max_days");

    $googleController = new Yoho\cms\Controller\GoogleController();
    $statisticsController = new Yoho\cms\Controller\StatisticsController();
    $client = $googleController->getClient();
    $service = new \Google_Service_Analytics($client);
    foreach ($dates as $date) {
        $metrics = "ga:users,ga:pageviews,ga:uniquePageviews";
        $optParams = [
            'dimensions'    => 'ga:pagePath,ga:landingPagePath,ga:medium',
            'filters'       => 'ga:pagePath=~product',
            'max-results'   => 10000,
        ];
        if ($batched > 0) {
            sleep($batched);
        }
        $batched = 0;
        $updated = 0;
        $target_date = local_date("Y-m-d", local_strtotime($date));
        $optParams['start-index'] = 1;
        $params = [
            'start_day' => $target_date,
            'end_day'   => $target_date,
            'metrics'   => $metrics,
            'optParams' => $optParams
        ];
        $result = $statisticsController->batchGetGoogleAnalyticData($service, $client, $params);
        $batched = $result['batched'];
        if ($result['success']) {
            foreach ($result['insert_data'] as $goods_id => $row) {
                $updated++;
                $db->query("INSERT INTO " . $ecs->table("google_statistics") . " (goods_id, session, pageview, pageview_unique,landing_session, landing_pageview, landing_pageview_unique, date) VALUES ('$goods_id', '$row[session]', '$row[pageview]', '$row[pageview_unique]', '$row[landing_session]', '$row[landing_pageview]', '$row[landing_pageview_unique]', '$target_date') ON DUPLICATE KEY UPDATE session = $row[session], pageview = $row[pageview], pageview_unique = $row[pageview_unique], landing_session = $row[landing_session], landing_pageview = $row[landing_pageview], landing_pageview_unique = $row[landing_pageview_unique]");
            }
        }
        var_dump("$target_date Updated: $updated");
    }
} elseif ($job == 'update_goods_consumption') {
    $erpController = new Yoho\cms\Controller\ErpController();
    $delivery_ids = [];
    $sql = "SELECT edi.order_item_id, edi.goods_id, edi.delivered_qty, ed.create_time, ed.delivery_id, ed.delivery_sn, oi.order_id, oi.order_sn FROM (SELECT delivery_id, create_time, delivery_sn, order_id FROM " . $ecs->table('erp_delivery') . ") ed " .
            "LEFT JOIN (SELECT delivery_id, order_item_id, goods_id, delivered_qty FROM " . $ecs->table('erp_delivery_item') . ") edi ON ed.delivery_id = edi.delivery_id " .
            "LEFT JOIN (SELECT order_id, order_sn FROM " . $ecs->table('order_info') . ") oi ON ed.order_id = oi.order_id " .
            "WHERE ed.delivery_id " . db_create_in($delivery_ids);
    $deliveryInfo = $db->getAll($sql);
    $deliveryItems = [];
    $goods_comsumed = [];
    foreach($deliveryInfo as $item){
        if (empty($goods_comsumed[$item['goods_id']])) {
            $goods_comsumed[$item['goods_id']] = 0;
        }
        $goods_comsumed[$item['goods_id']] += $item['delivered_qty'];
        if (empty($deliveryItems[$item['delivery_id']])) {
            $deliveryItems[$item['delivery_id']] = [
                'order_id'      => $item['order_id'],
                'order_sn'      => $item['order_sn'],
                'delivery_sn'   => $item['delivery_sn'],
                'txn_date'      => local_date('Y-m-d H:i:s', $item['create_time']),
                'items'         => [],
            ];

        }
        $deliveryItems[$item['delivery_id']]['items'][] = $item;
    }
    foreach ($deliveryItems as $delivery_id => $deliveryItemInfo) {
        $accountingInfo = [];
        $baseTxn = [
            'sales_order_id'    => $deliveryItemInfo['order_id'],
            'delivery_id'	    => $delivery_id,
            'remark'			=> "訂單 $deliveryItemInfo[order_sn] 出貨 $deliveryItemInfo[delivery_sn]",
            'txn_date'          => $deliveryItemInfo['txn_date'],
        ];

        foreach($deliveryItemInfo['items'] as $item){
            $consumed = empty($goods_comsumed[$item['goods_id']]) ? 0 : $goods_comsumed[$item['goods_id']];

            $data = $db->getAll("SELECT eo.supplier_id,eoi.warehousing_qty as qty,(eoi.price * eoi.order_qty - eoi.cn_amount) / eoi.order_qty as unit_cost, CASE WHEN eo.type = 0 THEN eoi.price ELSE 0 END as unit_value FROM ".$ecs->table('erp_order_item')." eoi LEFT JOIN ".$ecs->table('erp_order')." eo ON eoi.order_id=eo.order_id WHERE eoi.goods_id = ".$item['goods_id']." ORDER BY eoi.order_item_id DESC");
            $stock = $erpController->getSellableProductQty($item['goods_id'], false);

            $dataTobeCounted=[];
            $stockCounter = $stock + $consumed;
            $goods_comsumed[$item['goods_id']] -= $item['delivered_qty'];
            foreach ($data as $d) {
                if($stockCounter-$d['qty']>0){
                    $dataTobeCounted[]=$d;
                }else{
                    if($stockCounter>0){
                        $d['qty'] = $stockCounter;
                        $dataTobeCounted[]=$d;
                    }
                    break;
                }
                $stockCounter-=$d['qty'];
            }

            $suppliers=[];
            foreach (array_reverse($dataTobeCounted,TRUE) as $d) {
                if($d['qty']-$item['delivered_qty'] >= 0){
                    $suppliers[] = ['supplier_id' => $d['supplier_id'], 'qty'=>intval($item['delivered_qty']),'unit_cost'=>floatval($d['unit_cost']), 'unit_value'=>floatval($d['unit_value'])];
                    break;
                }else{
                    $item['delivered_qty']-=intval($d['qty']);
                    $suppliers[] = ['supplier_id' => $d['supplier_id'], 'qty'=>intval($d['qty']),'unit_cost'=>floatval($d['unit_cost']), 'unit_value'=>floatval($d['unit_value'])];
                }
            }
            $statment = [];
            foreach ($suppliers as $key=>$data) {
                $statment[]="(".$item['order_item_id'].",".$item['goods_id'].",".$data['supplier_id'].",".$data['qty'].",".$data['unit_cost'].",'".local_date('Y-m-d H:i:s', $item['create_time'])."',".$data['unit_value'].")";
            }
            if (!empty($statment))
            $db->query("INSERT IGNORE INTO ".$ecs->table('order_goods_supplier_consumption')." (order_goods_id,goods_id,supplier_id,consumed_qty,po_unit_cost,last_updated,po_unit_value) VALUES ".implode(',', $statment));

            $accountingInfo[] = $suppliers;
        }

        $accountingAmount = 0;
        foreach ($accountingInfo as $info) {
            if(count($info) <= 0) continue;
            foreach ($info as $data) {
                $accountingAmount = bcadd($accountingAmount, bcmul(round($data['unit_value'], 2), $data['qty'], 2), 2);
            }
        }
        if ($accountingAmount != 0) {
			$accountingController = new Yoho\cms\Controller\AccountingController();
			$param = [
				'from_actg_type_id'	=> $accountingController->getAccountingType(['actg_type_code' => '45000', 'enabled' => 1], 'actg_type_id'), // Cost of Sales
				'to_actg_type_id' 	=> $accountingController->getAccountingType(['actg_type_code' => '14001', 'enabled' => 1], 'actg_type_id'), // Inventory (Owned)
				'currency_code' 	=> 'HKD',
                'amount'			=> $accountingAmount,
			];
			foreach ($baseTxn as $key => $val) {
				$param[$key] = $val;
			}
			$accountingController->insertDoubleEntryTransactions($param);
        }
    }
} else {
    die("Job not found");
}

die("Job completed");
?>