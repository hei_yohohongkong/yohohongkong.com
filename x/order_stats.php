<?php

/**
 * ECSHOP 订单统计
 * ============================================================================
 * 版权所有 2005-2010 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liuhui $
 * $Id: order_stats.php 17063 2010-03-25 06:35:46Z liuhui $
*/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require_once(ROOT_PATH . 'includes/lib_order.php');
require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/admin/statistic.php');
$orderController = new Yoho\cms\Controller\OrderController();
$smarty->assign('lang', $_LANG);
$userController = new Yoho\cms\Controller\UserController();
/* act操作项的初始化 */
if (empty($_REQUEST['act']))
{
    $_REQUEST['act'] = 'list';
}
else
{
    $_REQUEST['act'] = trim($_REQUEST['act']);
}

/*------------------------------------------------------ */
//--订单统计
/*------------------------------------------------------ */
if ($_REQUEST['act'] == 'list')
{
    admin_priv('sale_order_stats');
    $type = $_REQUEST['selected'] ? trim($_REQUEST['selected']) : 'normal';
    $smarty->assign('selected',            $type);
    /* 计算订单各种费用之和的语句 */
    $total_fee = " SUM(" . order_amount_field() . ") AS total_turnover ";

    /* 取得订单转化率数据 */
    $sql = "SELECT COUNT(*) AS total_order_num, " .$total_fee.
        " FROM " . $ecs->table('order_info').
        " WHERE 1 " . order_query_sql('finished') . where_exclude();
    $order_general = $db->getRow($sql);
    $order_general['total_turnover'] = floatval($order_general['total_turnover']);

    /* 取得商品总点击数量 */
    $sql = 'SELECT SUM(click_count) FROM ' .$ecs->table('goods') .' WHERE is_delete = 0';
    $click_count = floatval($db->getOne($sql));

    /* 每千个点击的订单数 */
    $click_ordernum = $click_count > 0 ? round(($order_general['total_order_num'] * 1000)/$click_count,2) : 0;

    /* 每千个点击的购物额 */
    $click_turnover = $click_count > 0 ? round(($order_general['total_turnover'] * 1000)/$click_count,2) : 0;

    /* 时区 */
    $timezone = isset($_SESSION['timezone']) ? $_SESSION['timezone'] : $GLOBALS['_CFG']['timezone'];

    /* 时间参数 */
    $is_multi = empty($_POST['is_multi']) ? false : true;

    /* 时间参数 */
    if (isset($_POST['submit2']))
    {
        $sd = new DateTime('first day of last month 00:00:00');     //00:00, First day of last month
        $ed = new DateTime('last day of last month 23:59:59');      //23:59, Last day of last month
        $_POST['start_date'] = $sd->format('Y-m-d');
        $_POST['end_date'] = $ed->format('Y-m-d');
    }
    if (isset($_POST['start_date']) && !empty($_POST['end_date']))
    {
        $start_date = strtotime($_POST['start_date']);                  //00:00, Selected day
        $end_date = strtotime($_POST['end_date']) + 86400 - 1;          //23:59, Selected day
        if ($start_date == $end_date)
        {
            $end_date   =   $start_date + 86400 - 1;
        }
    }
    else
    {
        $today      = local_strtotime('now');     //00:00, Today
        $start_date = strtotime(local_date('Y-m-01'));     //00:00, First day of current month
        $end_date   = strtotime(local_date('Y-m-t'));          //23:59, Today
    }

    $start_date_arr = array();
    $end_date_arr = array();
    if(!empty($_POST['year_month']))
    {
        $tmp = $_POST['year_month'];

        for ($i = 0; $i < count($tmp); $i++)
        {
            if (!empty($tmp[$i]))
            {
                $tmp_time = strtotime($tmp[$i] . '-1');
                $start_date_arr[] = $tmp_time;                                                      //00:00, First day of selected month
                $end_date_arr[]   = strtotime($tmp[$i] . '-' . date('t', $tmp_time)) + 86400 - 1;   //23:59, Last day of selected month
            }
        }
    }
    else
    {
        $tmp_time = strtotime(date('Y-m-d'));
        $start_date_arr[] = strtotime(date('Y-m') . '-1');
        $end_date_arr[]   = strtotime(date('Y-m') . '-31');
    }

    /* 按月份交叉查询 */
    if ($is_multi)
    {
        /* 订单概况 */
        $order_general_xml = "<chart caption='$_LANG[order_circs]' shownames='1' showvalues='0' decimals='0' outCnvBaseFontSize='12' baseFontSize='12' >";
        $order_general_xml .= "<categories><category label='$_LANG[confirmed]' />" .
            "<category label='$_LANG[paid]' />" .
            "<category label='$_LANG[succeed]' />" .
            "<category label='$_LANG[unconfirmed]' />" .
            "<category label='$_LANG[invalid]' /></categories>";
        foreach($start_date_arr AS $k => $val)
        {
            $seriesName = date('Y-m',$val);
            $order_info = get_orderinfo($start_date_arr[$k], $end_date_arr[$k]);
            $order_general_xml .= "<dataset seriesName='$seriesName' color='$color_array[$k]' showValues='0'>";
            $order_general_xml .= "<set value='$order_info[confirmed_num]' />";
            $order_general_xml .= "<set value='$order_info[paid_num]' />";
            $order_general_xml .= "<set value='$order_info[succeed_num]' />";
            $order_general_xml .= "<set value='$order_info[unconfirmed_num]' />";
            $order_general_xml .= "<set value='$order_info[invalid_num]' />";
            $order_general_xml .= "</dataset>";
        }
        $order_general_xml .= "</chart>";

        /* 支付方式 */
        $pay_xml = "<chart caption='$_LANG[pay_method]' shownames='1' showvalues='0' decimals='0' outCnvBaseFontSize='12' baseFontSize='12' >";

        $payment = array();
        $payment_count = array();

        foreach($start_date_arr AS $k => $val)
        {
            $sql = 'SELECT i.pay_id, p.pay_name, i.pay_time, COUNT(i.order_id) AS order_num ' .
                'FROM ' .$ecs->table('payment'). ' AS p, ' .$ecs->table('order_info'). ' AS i '.
                "WHERE p.pay_id = i.pay_id AND i.order_status = '" .OS_CONFIRMED. "' ".
                "AND i.pay_status > '" .PS_UNPAYED. "' AND i.shipping_status > '" .SS_UNSHIPPED. "' ".
                "AND i.shipping_time >= '$start_date_arr[$k]' AND i.shipping_time <= '$end_date_arr[$k]'".
                "GROUP BY i.pay_id ORDER BY order_num DESC";
            $pay_res = $db->query($sql);
            while ($pay_item = $db->FetchRow($pay_res))
            {
                $payment[$pay_item['pay_name']] = null;

                $paydate = date('Y-m', $pay_item['pay_time']);

                $payment_count[$pay_item['pay_name']][$paydate] = $pay_item['order_num'];
            }
        }

        $pay_xml .= "<categories>";
        foreach ($payment AS $k => $val)
        {
            $pay_xml .= "<category label='$k' />";
        }
        $pay_xml .= "</categories>";

        foreach($start_date_arr AS $k => $val)
        {
            $date = date('Y-m', $start_date_arr[$k]);
            $pay_xml .= "<dataset seriesName='$date' color='$color_array[$k]' showValues='0'>";
            foreach ($payment AS $k => $val)
            {
                $count = 0;
                if (!empty($payment_count[$k][$date]))
                {
                    $count = $payment_count[$k][$date];
                }

                $pay_xml .= "<set value='$count' name='$date' />";
            }
            $pay_xml .= "</dataset>";
        }
        $pay_xml .= "</chart>";

        /* 配送方式 */
        $ship = array();
        $ship_count = array();

        $ship_xml = "<chart caption='$_LANG[shipping_method]' shownames='1' showvalues='0' decimals='0' outCnvBaseFontSize='12' baseFontSize='12' >";

        foreach($start_date_arr AS $k => $val)
        {
            $sql = 'SELECT sp.shipping_id, sp.shipping_name AS ship_name, i.shipping_time, COUNT(i.order_id) AS order_num ' .
                'FROM ' .$ecs->table('shipping'). ' AS sp, ' .$ecs->table('order_info'). ' AS i ' .
                'WHERE sp.shipping_id = i.shipping_id ' . order_query_sql('finished') . where_exclude() .
                "AND i.shipping_time >= '$start_date_arr[$k]' AND i.shipping_time <= '$end_date_arr[$k]' " .
                "GROUP BY i.shipping_id ORDER BY order_num DESC";

            $ship_res = $db->query($sql);
            while ($ship_item = $db->FetchRow($ship_res))
            {
                $ship[$ship_item['ship_name']] = null;

                $shipdate = date('Y-m', $ship_item['shipping_time']);

                $ship_count[$ship_item['ship_name']][$shipdate] = $ship_item['order_num'];
            }
        }

        $ship_xml .= "<categories>";
        foreach ($ship AS $k => $val)
        {
            $ship_xml .= "<category label='$k' />";
        }
        $ship_xml .= "</categories>";

        foreach($start_date_arr AS $k => $val)
        {
            $date = date('Y-m', $start_date_arr[$k]);

            $ship_xml .= "<dataset seriesName='$date' color='$color_array[$k]' showValues='0'>";
            foreach ($ship AS $k => $val)
            {
                $count = 0;
                if (!empty($ship_count[$k][$date]))
                {
                    $count = $ship_count[$k][$date];
                }
                $ship_xml .= "<set value='$count' name='$date' />";
            }
            $ship_xml .= "</dataset>";
        }
        $ship_xml .= "</chart>";

        /* 地區分佈 */
        $country = array();
        $country_count = array();

        $country_xml = "<chart caption='$_LANG[country_distribution]' shownames='1' showvalues='0' decimals='0' outCnvBaseFontSize='12' baseFontSize='12' >";

        foreach($start_date_arr AS $k => $val)
        {
            $sql = 'SELECT r.region_name AS country,COUNT(oi.order_id) AS order_num, shipping_time FROM ' .$ecs->table('order_info'). ' oi ' .
                'LEFT JOIN ' .$ecs->table('region'). ' r ON r.region_id = oi.country '.
                'WHERE country > 0' . order_query_sql('finished') . where_exclude() .
                "AND shipping_time >= '$start_date_arr[$k]' AND shipping_time <= '$end_date_arr[$k]' " .
                "GROUP BY country ORDER BY country ASC";

            $country_res = $db->query($sql);
            while ($country_item = $db->FetchRow($country_res))
            {
                $country[$country_item['country']] = null;

                $shipdate = date('Y-m', $country_item['shipping_time']);

                $country_count[$country_item['country']][$shipdate] = $country_item['order_num'];
            }
        }

        $country_xml .= "<categories>";
        foreach ($country AS $k => $val)
        {
            $country_xml .= "<category label='$k' />";
        }
        $country_xml .= "</categories>";

        foreach($start_date_arr AS $k => $val)
        {
            $date = date('Y-m', $start_date_arr[$k]);

            $country_xml .= "<dataset seriesName='$date' color='$color_array[$k]' showValues='0'>";
            foreach ($country AS $k => $val)
            {
                $count = 0;
                if (!empty($country_count[$k][$date]))
                {
                    $count = $country_count[$k][$date];
                }
                $country_xml .= "<set value='$count' name='$date' />";
            }
            $country_xml .= "</dataset>";
        }
        $country_xml .= "</chart>";

        /* 落單設備 */
        $device = array();
        $device_count = array();

        $device_xml = "<chart caption='$_LANG[device_distribution]' shownames='1' showvalues='0' decimals='0' outCnvBaseFontSize='12' baseFontSize='12' >";

        foreach($start_date_arr AS $k => $val)
        {
            $sql = 'SELECT platform,COUNT(order_id) AS order_num, shipping_time FROM ' .$ecs->table('order_info'). ' ' .
                'WHERE platform != "pos"' . order_query_sql('finished') . where_exclude() .
                "AND shipping_time >= '$start_date_arr[$k]' AND shipping_time <= '$end_date_arr[$k]' " .
                "GROUP BY platform ORDER BY platform ASC";

            $device_res = $db->query($sql);
            while ($device_item = $db->FetchRow($device_res))
            {
                $device[$device_item['platform']] = null;

                $shipdate = date('Y-m', $device_item['shipping_time']);

                $device_count[$device_item['platform']][$shipdate] = $device_item['order_num'];
            }
        }

        $device_xml .= "<categories>";
        foreach ($device AS $k => $val)
        {
            $device_xml .= "<category label='".$orderController::DB_SELLING_PLATFORMS[$k]."' />";
        }
        $device_xml .= "</categories>";

        foreach($start_date_arr AS $k => $val)
        {
            $date = date('Y-m', $start_date_arr[$k]);

            $device_xml .= "<dataset seriesName='$date' color='$color_array[$k]' showValues='0'>";
            foreach ($device AS $k => $val)
            {
                $count = 0;
                if (!empty($device_count[$k][$date]))
                {
                    $count = $device_count[$k][$date];
                }
                $device_xml .= "<set value='$count' name='$date' />";
            }
            $device_xml .= "</dataset>";
        }
        $device_xml .= "</chart>";

        /* 訂單來源 */
        $source = array();
        $source_count = array();

        $source_xml = "<chart caption='$_LANG[source_distribution]' shownames='1' showvalues='0' decimals='0' outCnvBaseFontSize='12' baseFontSize='12' >";

        foreach($start_date_arr AS $k => $val)
        {
            $sql = 'SELECT (CASE WHEN (platform = "web" OR platform = "mobile") THEN "網上" WHEN platform = "pos" THEN "門店" ELSE "其他" END) AS platforms, COUNT(order_id) AS order_num, shipping_time FROM ' .$ecs->table('order_info') .
                'WHERE platform != ""' . order_query_sql('finished') .
                "AND shipping_time >= '$start_date_arr[$k]' AND shipping_time <= '$end_date_arr[$k]' " . where_exclude() .
                "GROUP BY platforms ORDER BY platforms ASC";
            $source_res = $db->query($sql);
            while ($source_item = $db->FetchRow($source_res))
            {
                $source[$source_item['platforms']] = null;

                $shipdate = date('Y-m', $source_item['shipping_time']);

                $source_count[$source_item['platforms']][$shipdate] = $source_item['order_num'];
            }
        }
        $source_xml .= "<categories>";
        foreach ($source AS $k => $val)
        {
            $source_xml .= "<category label='$k' />";
        }
        $source_xml .= "</categories>";

        foreach($start_date_arr AS $k => $val)
        {
            $date = date('Y-m', $start_date_arr[$k]);

            $source_xml .= "<dataset seriesName='$date' color='$color_array[$k]' showValues='0'>";
            foreach ($source AS $k => $val)
            {
                $count = 0;
                if (!empty($source_count[$k][$date]))
                {
                    $count = $source_count[$k][$date];
                }
                $source_xml .= "<set value='$count' name='$date' />";
            }
            $source_xml .= "</dataset>";
        }
        $source_xml .= "</chart>";

        /* VIP訂單 */
        $vip = array();
        $vip_count = array();

        $vip_xml = "<chart caption='$_LANG[vip_distribution]' shownames='1' showvalues='0' decimals='0' outCnvBaseFontSize='12' baseFontSize='12' >";
        $vip['VIP訂單'] = null;
        $vip['非VIP訂單'] = null;
        foreach($start_date_arr AS $k => $val)
        {
            $shipdate = date('Y-m', $start_date_arr[$k]);
            $sql = 'SELECT COUNT(oi.order_id) AS order_num, shipping_time FROM ' .$ecs->table('order_info'). ' oi ' .
                'WHERE 1 in (SELECT IF(user_rank = 2, 1, 0) FROM ' .$ecs->table('order_goods'). ' og WHERE og.order_id = oi.order_id)' . order_query_sql('finished') . where_exclude() .
                "AND shipping_time >= '$start_date_arr[$k]' AND shipping_time <= '$end_date_arr[$k]'";
            $vip_count['VIP訂單'][$shipdate] = $db->getOne($sql);
            $sql = 'SELECT COUNT(*) FROM ' .$ecs->table('order_info'). ' WHERE order_id NOT in (' .
                'SELECT oi.order_id FROM ' .$ecs->table('order_info'). ' oi ' .
                'WHERE 1 in (SELECT IF(user_rank = 2, 1, 0) FROM ' .$ecs->table('order_goods'). ' og WHERE og.order_id = oi.order_id)) ' . order_query_sql('finished') . where_exclude() .
                "AND shipping_time >= '$start_date_arr[$k]' AND shipping_time <= '$end_date_arr[$k]'";
            $vip_count['非VIP訂單'][$shipdate] = $db->getOne($sql);
        }
        $vip_xml .= "<categories>";
        foreach ($vip AS $k => $val)
        {
            $vip_xml .= "<category label='$k' />";
        }
        $vip_xml .= "</categories>";

        foreach($start_date_arr AS $k => $val)
        {
            $date = date('Y-m', $start_date_arr[$k]);

            $vip_xml .= "<dataset seriesName='$date' color='$color_array[$k]' showValues='0'>";
            foreach ($vip AS $k => $val)
            {
                $count = 0;
                if (!empty($vip_count[$k][$date]))
                {
                    $count = $vip_count[$k][$date];
                }
                $vip_xml .= "<set value='$count' name='$date' />";
            }
            $vip_xml .= "</dataset>";
        }
        $vip_xml .= "</chart>";

        /* 送貨途徑 */
        $delivery = array();
        $delivery_count = array();

        $delivery_xml = "<chart caption='$_LANG[delivery_distribution]' shownames='1' showvalues='0' decimals='0' outCnvBaseFontSize='12' baseFontSize='12' >";

        foreach($start_date_arr AS $k => $val)
        {
            $sql = 'SELECT COALESCE(l.logistics_name,"無法歸類") AS logistics_name, COUNT(*) AS order_num, shipping_time FROM (' .
                'SELECT do.order_id, do.logistics_id, oi.shipping_time, COUNT(*) FROM ' . $ecs->table('delivery_order') . ' do ' .
                'LEFT JOIN ' . $ecs->table('order_info') . ' oi ON oi.order_id = do.order_id ' .
                'WHERE oi.shipping_time > 0 ' . order_query_sql('finished','oi.') .
                "AND oi.shipping_time >= '$start_date_arr[$k]' AND oi.shipping_time <= '$end_date_arr[$k]' " . where_exclude('oi.') .
                "GROUP BY oi.order_id, do.logistics_id) a " .
                "LEFT JOIN " . $ecs->table('logistics') . ' l ON l.logistics_id = a.logistics_id ' .
                "GROUP BY l.logistics_id ORDER BY l.logistics_id ASC";
            $delivery_res = $db->query($sql);
            while ($delivery_item = $db->FetchRow($delivery_res))
            {
                $delivery[$delivery_item['logistics_name']] = null;

                $shipdate = date('Y-m', $delivery_item['shipping_time']);

                $delivery_count[$delivery_item['logistics_name']][$shipdate] = $delivery_item['order_num'];
            }
        }
        $delivery_xml .= "<categories>";
        foreach ($delivery AS $k => $val)
        {
            $delivery_xml .= "<category label='$k' />";
        }
        $delivery_xml .= "</categories>";

        foreach($start_date_arr AS $k => $val)
        {
            $date = date('Y-m', $start_date_arr[$k]);

            $delivery_xml .= "<dataset seriesName='$date' color='$color_array[$k]' showValues='0'>";
            foreach ($delivery AS $k => $val)
            {
                $count = 0;
                if (!empty($delivery_count[$k][$date]))
                {
                    $count = $delivery_count[$k][$date];
                }
                $delivery_xml .= "<set value='$count' name='$date' />";
            }
            $delivery_xml .= "</dataset>";
        }
        $delivery_xml .= "</chart>";

        /* 送貨途徑(包括SA) */
        $delivery2 = array();
        $delivery2_count = array();

        $delivery2_xml = "<chart caption='$_LANG[delivery2_distribution]' shownames='1' showvalues='0' decimals='0' outCnvBaseFontSize='12' baseFontSize='12' >";

        foreach($start_date_arr AS $k => $val)
        {
            $sql = 'SELECT COALESCE(l.logistics_name,"無法歸類") AS logistics_name, COUNT(*) AS order_num, shipping_time FROM (' .
                'SELECT do.order_id, do.logistics_id, oi.shipping_time, COUNT(*) FROM ' . $ecs->table('delivery_order') . ' do ' .
                'LEFT JOIN ' . $ecs->table('order_info') . ' oi ON oi.order_id = do.order_id ' .
                'WHERE oi.shipping_time > 0 ' . order_query_sql('finished','oi.') .
                "AND oi.shipping_time >= '$start_date_arr[$k]' AND oi.shipping_time <= '$end_date_arr[$k]' " . where_exclude('oi.',true) .
                "GROUP BY oi.order_id, do.logistics_id) a " .
                "LEFT JOIN " . $ecs->table('logistics') . ' l ON l.logistics_id = a.logistics_id ' .
                "GROUP BY l.logistics_id ORDER BY l.logistics_id ASC";
            $delivery2_res = $db->query($sql);
            while ($delivery2_item = $db->FetchRow($delivery2_res))
            {
                $delivery2[$delivery2_item['logistics_name']] = null;

                $shipdate = date('Y-m', $delivery2_item['shipping_time']);

                $delivery2_count[$delivery2_item['logistics_name']][$shipdate] = $delivery2_item['order_num'];
            }
        }
        $delivery2_xml .= "<categories>";
        foreach ($delivery2 AS $k => $val)
        {
            $delivery2_xml .= "<category label='$k' />";
        }
        $delivery2_xml .= "</categories>";

        foreach($start_date_arr AS $k => $val)
        {
            $date = date('Y-m', $start_date_arr[$k]);

            $delivery2_xml .= "<dataset seriesName='$date' color='$color_array[$k]' showValues='0'>";
            foreach ($delivery2 AS $k => $val)
            {
                $count = 0;
                if (!empty($delivery2_count[$k][$date]))
                {
                    $count = $delivery2_count[$k][$date];
                }
                $delivery2_xml .= "<set value='$count' name='$date' />";
            }
            $delivery2_xml .= "</dataset>";
        }
        $delivery2_xml .= "</chart>";
    }
    /* 按时间段查询 */
    else
    {
        /* 订单概况 */
        $order_info = get_orderinfo($start_date, $end_date);
        /* Handle data */
        $order = [];
        $i = 0;
        $data = [];
        $labels = [];
        $datasets = [];
        foreach($order_info as $name => $value) {
            if($name == 'sql') continue;
            $color = '#'.chart_color($i);
            $data[$i] = $value;
            $labels[$i] = $_LANG[$name];
            $backgroundColor[$i] = $color;
            $i++;
        }
        $label = $_LANG['order_circs'];
        $datasets[0]['data'] = $data;
        $datasets[0]['backgroundColor'] = $backgroundColor;
        $datasets[0]['label'] = $label;
        $order = ['datasets' => $datasets, 'labels' => $labels];

        /* 支付方式 */
        $sql = 'SELECT i.pay_id, p.pay_name, COUNT(i.order_id) AS order_num ' .
            'FROM ' .$ecs->table('payment'). ' AS p, ' .$ecs->table('order_info'). ' AS i '.
            "WHERE p.pay_id = i.pay_id " . order_query_sql('finished') .where_exclude('i.') .
            "AND i.shipping_time >= '$start_date' AND i.shipping_time <= '$end_date' ".
            "GROUP BY i.pay_id ORDER BY order_num DESC";
        $pay_res= $db->query($sql);
        $i = 0;
        $data = [];
        $labels = [];
        $datasets = [];
        while ($pay_item = $db->FetchRow($pay_res))
        {
            $color = '#'.chart_color($i);
            $data[$i] = $pay_item['order_num'];
            $labels[$i] = $pay_item['pay_name'];
            $backgroundColor[$i] = $color;
            $i++;
        }
        $label = $_LANG['pay_method'];
        $datasets[0]['data'] = $data;
        $datasets[0]['backgroundColor'] = $backgroundColor;
        $datasets[0]['label'] = $label;
        $pay = ['datasets' => $datasets, 'labels' => $labels];

        /* 配送方式 */
        $where_status = " AND i.order_status NOT IN (" . OS_CANCELED . ',' . OS_INVALID . ',' . OS_RETURNED . ") ".
            " AND i.shipping_status IN (" . SS_SHIPPED . ',' . SS_RECEIVED . ") ";
        $where_exclude = " AND i.shipping_time BETWEEN " . $start_date . " AND " . $end_date;
        // Exclude orders for the followiung user accounts: 11111111, 88888888, 99999999
        // Exclude sales agent and wholesale
        $where_exclude .= " AND (i.order_type NOT IN ('" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT . "','" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE . "') OR (i.order_type IS NULL AND i.user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).") ) ) ";
        // Exclude sales agent
        $where_exclude .= " AND (i.order_type <> '" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT . "' OR i.order_type IS NULL) ";
        // Exclude 展場POS
        $where_exclude .= " AND (i.platform <> 'pos_exhibition') ";
        $sql = 'SELECT sp.shipping_id, sp.shipping_name AS ship_name, COUNT(i.order_id) AS order_num , SUM(i.money_paid + i.order_amount) AS total_turnover ' .
            'FROM ' .$ecs->table('shipping'). ' AS sp, ' .$ecs->table('order_info'). ' AS i ' .
            'WHERE sp.shipping_id = i.shipping_id ' . $where_status .$where_exclude .
            $where_status .
            " GROUP BY i.shipping_id ORDER BY order_num DESC";

        $ship_res = $db->query($sql);
        $i = 0;
        $data = [];
        $data2 = [];
        $labels = [];
        $datasets = [];
        while ($ship_item = $db->fetchRow($ship_res))
        {
            $color = '#'.chart_color($i);
            $data[$i] = $ship_item['order_num'];
            $data2[$i] = $ship_item['total_turnover'];
            $labels[$i] = $ship_item['ship_name'];
            $backgroundColor[$i] = $color;
            $i++;
        }
        $label = $_LANG['shipping_method'];
        $datasets[0]['data'] = $data;
        $datasets[0]['backgroundColor'] = $backgroundColor;
        $datasets[0]['label'] = $label;
        $ship = ['datasets' => $datasets, 'labels' => $labels, 'data2' =>$data2];

        /* 地區分佈 */
        $sql = 'SELECT r.region_name AS country,COUNT(oi.order_id) AS order_num FROM ' .$ecs->table('order_info'). ' oi ' .
            'LEFT JOIN ' .$ecs->table('region'). ' r ON r.region_id = oi.country '.
            'WHERE country > 0' . order_query_sql('finished') .where_exclude() .
            "AND shipping_time >= '$start_date' AND shipping_time <= '$end_date' " .
            "GROUP BY country ORDER BY country ASC";
        $country_res = $db->query($sql);
        $i = 0;
        $data = [];
        $labels = [];
        $datasets = [];
        while ($country_item = $db->fetchRow($country_res))
        {
            $color = '#'.chart_color($i);
            $data[$i] = $country_item['order_num'];
            $labels[$i] = $country_item['country'];
            $backgroundColor[$i] = $color;
            $i++;
        }
        $label = $_LANG['country_distribution'];
        $datasets[0]['data'] = $data;
        $datasets[0]['backgroundColor'] = $backgroundColor;
        $datasets[0]['label'] = $label;
        $country = ['datasets' => $datasets, 'labels' => $labels];

        /* 落單設備 */
        $sql = 'SELECT platform,COUNT(order_id) AS order_num FROM ' .$ecs->table('order_info'). ' ' .
            'WHERE platform != "pos"' . order_query_sql('finished') .
            "AND shipping_time >= '$start_date' AND shipping_time <= '$end_date' " . where_exclude() .
            "GROUP BY platform ORDER BY platform ASC";
        $device_res = $db->query($sql);
        $i = 0;
        $data = [];
        $labels = [];
        $datasets = [];
        while ($device_item = $db->fetchRow($device_res))
        {
            $color = '#'.chart_color($i);
            $data[$i] = $device_item['order_num'];
            $labels[$i] = $device_item['platform'];
            $backgroundColor[$i] = $color;
            $i++;
        }
        $label = $_LANG['device_distribution'];
        $datasets[0]['data'] = $data;
        $datasets[0]['backgroundColor'] = $backgroundColor;
        $datasets[0]['label'] = $label;
        $device = ['datasets' => $datasets, 'labels' => $labels];

        /* 訂單來源 */
        $source_xml = "<graph caption='".$_LANG['source_distribution']."' decimalPrecision='2' showPercentageValues='0' showNames='1' numberPrefix='' showValues='1' showPercentageInLabel='0' pieYScale='45' pieBorderAlpha='40' pieFillAlpha='70' pieSliceDepth='15' pieRadius='100' outCnvBaseFontSize='13' baseFontSize='12'>";
        $sql = 'SELECT (CASE WHEN (platform = "web" OR platform = "mobile") THEN "網上" WHEN platform = "pos" THEN "門店" ELSE "其他" END) AS platforms , COUNT(order_id) AS order_num FROM ' .$ecs->table('order_info') .
            'WHERE platform != ""' . order_query_sql('finished') . where_exclude() .
            "AND shipping_time >= '$start_date' AND shipping_time <= '$end_date' " .
            "GROUP BY platforms ORDER BY platforms ASC";
        $source_res = $db->query($sql);
        $i = 0;
        $data = [];
        $labels = [];
        $datasets = [];
        while ($source_item = $db->FetchRow($source_res))
        {
            $color = '#'.chart_color($i);
            $data[$i] = $source_item['order_num'];
            $labels[$i] = $source_item['platforms'];
            $backgroundColor[$i] = $color;
            $i++;
        }
        $label = $_LANG['source_distribution'];
        $datasets[0]['data'] = $data;
        $datasets[0]['backgroundColor'] = $backgroundColor;
        $datasets[0]['label'] = $label;
        $source = ['datasets' => $datasets, 'labels' => $labels];

        /* VIP訂單 */
        $i = 0;
        $data = [];
        $labels = [];
        $datasets = [];
        $label = $_LANG['vip_distribution'];
        $sql = 'SELECT COUNT(oi.order_id) AS order_num FROM ' .$ecs->table('order_info'). ' oi ' .
            'WHERE 1 in (SELECT IF(user_rank = 2, 1, 0) FROM ' .$ecs->table('order_goods'). ' og WHERE og.order_id = oi.order_id)' . order_query_sql('finished') . where_exclude() .
            "AND shipping_time >= '$start_date' AND shipping_time <= '$end_date'";
        $vip_res = $db->getOne($sql);

        $color = '#'.chart_color(0);
        $data[0] = $vip_res;
        $labels[0] = 'VIP訂單';
        $backgroundColor[0] = $color;

        $sql = 'SELECT COUNT(*) FROM ' .$ecs->table('order_info'). ' WHERE order_id NOT in (' .
            'SELECT oi.order_id FROM ' .$ecs->table('order_info'). ' oi ' .
            'WHERE 1 in (SELECT IF(user_rank = 2, 1, 0) FROM ' .$ecs->table('order_goods'). ' og WHERE og.order_id = oi.order_id)) ' . order_query_sql('finished') . where_exclude() .
            "AND shipping_time >= '$start_date' AND shipping_time <= '$end_date'";
        $vip_res = $db->getOne($sql);

        $color = '#'.chart_color(1);
        $data[1] = $vip_res;
        $labels[1] = '非VIP訂單';
        $backgroundColor[1] = $color;

        $datasets[0]['data'] = $data;
        $datasets[0]['backgroundColor'] = $backgroundColor;
        $datasets[0]['label'] = $label;
        $vip = ['datasets' => $datasets, 'labels' => $labels];

        /* 送貨途徑 */
        $label = $_LANG['delivery_distribution'];
        $sql = 'SELECT COALESCE(l.logistics_name,"無法歸類") AS logistics_name, COUNT(*) AS order_num, shipping_time FROM (' .
            'SELECT do.order_id, do.logistics_id, oi.shipping_time, COUNT(*) FROM ' . $ecs->table('delivery_order') . ' do ' .
            'LEFT JOIN ' . $ecs->table('order_info') . ' oi ON oi.order_id = do.order_id ' .
            'WHERE oi.shipping_time > 0 ' . order_query_sql('finished','oi.') .
            "AND oi.shipping_time >= '$start_date' AND oi.shipping_time <= '$end_date' " . where_exclude('oi.') .
            "GROUP BY oi.order_id, do.logistics_id) a " .
            "LEFT JOIN " . $ecs->table('logistics') . ' l ON l.logistics_id = a.logistics_id ' .
            "GROUP BY l.logistics_id ORDER BY l.logistics_id ASC";
        $delivery_res = $db->query($sql);
        $i = 0;
        $data = [];
        $labels = [];
        $datasets = [];
        while ($delivery_item = $db->FetchRow($delivery_res))
        {
            $color = '#'.chart_color($i);
            $data[$i] = $delivery_item['order_num'];
            $labels[$i] = $delivery_item['logistics_name'];
            $backgroundColor[$i] = $color;
            $i++;
        }
        $datasets[0]['data'] = $data;
        $datasets[0]['backgroundColor'] = $backgroundColor;
        $datasets[0]['label'] = $label;
        $delivery = ['datasets' => $datasets, 'labels' => $labels];

        /* 送貨途徑(包括SA) */
        $label = $_LANG['delivery2_distribution'];
        $sql = 'SELECT COALESCE(l.logistics_name,"無法歸類") AS logistics_name, COUNT(*) AS order_num, shipping_time FROM (' .
            'SELECT do.order_id, do.logistics_id, oi.shipping_time, COUNT(*) FROM ' . $ecs->table('delivery_order') . ' do ' .
            'LEFT JOIN ' . $ecs->table('order_info') . ' oi ON oi.order_id = do.order_id ' .
            'WHERE oi.shipping_time > 0 ' . order_query_sql('finished','oi.') .
            "AND oi.shipping_time >= '$start_date' AND oi.shipping_time <= '$end_date' " . where_exclude('oi.',true) .
            "GROUP BY oi.order_id, do.logistics_id) a " .
            "LEFT JOIN " . $ecs->table('logistics') . ' l ON l.logistics_id = a.logistics_id ' .
            "GROUP BY l.logistics_id ORDER BY l.logistics_id ASC";
        $delivery2_res = $db->query($sql);
        $i = 0;
        $data = [];
        $labels = [];
        $datasets = [];
        while ($delivery2_item = $db->FetchRow($delivery2_res))
        {
            $color = '#'.chart_color($i);
            $data[$i] = $delivery2_item['order_num'];
            $labels[$i] = $delivery2_item['logistics_name'];
            $backgroundColor[$i] = $color;
            $i++;
        }
        $datasets[0]['data'] = $data;
        $datasets[0]['backgroundColor'] = $backgroundColor;
        $datasets[0]['label'] = $label;
        $delivery2 = ['datasets' => $datasets, 'labels' => $labels];
    }
    /* 赋值到模板 */
    $smarty->assign('order_general',       $order_general);
    $smarty->assign('total_turnover',      price_format($order_general['total_turnover']));
    $smarty->assign('click_count',         $click_count);         //商品总点击数
    $smarty->assign('click_ordernum',      $click_ordernum);      //每千点订单数
    $smarty->assign('click_turnover',      price_format($click_turnover));  //每千点购物额

    $smarty->assign('is_multi',            $is_multi);

    $smarty->assign('dataset_group', [
            'order' => $order,
            'ship' => $ship,
            'pay' => $pay,
            'country' => $country,
            'device' => $device,
            'source' => $source,
            'vip' => $vip,
            'delivery' => $delivery,
            'delivery2' => $delivery2,
        ]
    );
    // $smarty->assign('echoRes',             $echoRes);

    $smarty->assign('ur_here',             $_LANG['report_order']);
    $smarty->assign('start_date',          date($_CFG['date_format'], $start_date));
    $smarty->assign('end_date',            date($_CFG['date_format'], $end_date));

    for ($i = 0; $i < 5; $i++)
    {
        if (isset($start_date_arr[$i]))
        {
            $start_date_arr[$i] = date('Y-m', $start_date_arr[$i]);
        }
        else
        {
            $start_date_arr[$i] = null;
        }
    }
    $smarty->assign('start_date_arr', $start_date_arr);

    if (!$is_multi)
    {
        $filename = date('Ymd', $start_date) . '_' . date('Ymd', $end_date);
        $smarty->assign('action_link',  array('text' => $_LANG['down_order_statistics'], 'href' => 'order_stats.php?act=download&start_date=' . $start_date . '&end_date=' . $end_date . '&filename=' . $filename));
    }
    //非香港用戶營業額

    // 線下中國用戶營業額 : 會員手機號碼為+86 / POS 單
    $day = ($end_date - $start_date > 86400 )? 'FROM_UNIXTIME(oi.shipping_time, "%Y-%m-%d")' : 'FROM_UNIXTIME(oi.shipping_time, "%h:00%p")';
    $sql = 'SELECT SUM(' . order_amount_field('oi.') . ') as total, '.$day.' as day FROM ' .$GLOBALS['ecs']->table('order_info')." as oi ".
        " LEFT JOIN ".$GLOBALS['ecs']->table('users')." as u on u.user_id = oi.user_id ".
        " WHERE oi.order_status = '" .OS_CONFIRMED. "' AND shipping_status ". db_create_in(array(SS_SHIPPED, SS_RECEIVED)) . " AND pay_status " . db_create_in(array(PS_PAYED)) ." AND oi.shipping_time >= '$start_date'". where_exclude('oi.') .
        " AND oi.shipping_time < '" . ($end_date) . "' AND oi.platform = 'pos' AND u.user_name LIKE '+86-%' GROUP BY day  ";

    $offline_cn = $db->getAll($sql);

    // 線上中國用戶營業額 : 會員手機號碼為+86 / 非POS 單
    $sql = 'SELECT SUM(' . order_amount_field('oi.') . ') as total, '.$day.' as day FROM ' .$GLOBALS['ecs']->table('order_info')." as oi ".
        " LEFT JOIN ".$GLOBALS['ecs']->table('users')." as u on u.user_id = oi.user_id ".
        " WHERE oi.order_status = '" .OS_CONFIRMED. "' AND shipping_status ". db_create_in(array(SS_SHIPPED, SS_RECEIVED)) . " AND pay_status " . db_create_in(array(PS_PAYED)) ." AND oi.shipping_time >= '$start_date'". where_exclude('oi.') .
        " AND oi.shipping_time < '" . ($end_date) . "' AND oi.platform != 'pos' AND u.user_name LIKE '+86-%' GROUP BY day  ";

    $online_cn = $db->getAll($sql);

    // 海外用戶營業額 : 會員手機號碼不為+86 / +852 單
    $sql = 'SELECT SUM(' . order_amount_field('oi.') . ') as total, '.$day.' as day FROM ' .$GLOBALS['ecs']->table('order_info')." as oi ".
        " LEFT JOIN ".$GLOBALS['ecs']->table('users')." as u on u.user_id = oi.user_id ".
        " WHERE oi.order_status = '" .OS_CONFIRMED. "' AND shipping_status ". db_create_in(array(SS_SHIPPED, SS_RECEIVED)) . " AND pay_status " . db_create_in(array(PS_PAYED)) ." AND oi.shipping_time >= '$start_date'". where_exclude('oi.') .
        " AND oi.shipping_time < '" . ($end_date) . "' AND u.user_name NOT LIKE '+86-%' AND u.user_name NOT LIKE '+852-%' GROUP BY day  ";

    $overseas = $db->getAll($sql);
    $data = [];
    $labels = [];
    $datasets = [];
    $format = ($end_date - $start_date > 86400 )? 'Y-m-d' : "h:iA";
    if($end_date - $start_date > 86400) {
        for ($day = $start_date; $day <= $end_date; $day = $day + 86400 ) {
            $time = local_date($format, $day);
            $labels[$time] = $time;
            $data[$time]   = 0;
        }
    } else {
        for ($i = 0; $i < 24; $i++ ) {
            $time = date("h:iA", strtotime("$i:00"));
            $labels[$time] = $time;
            $data[$time]   = 0;
        }
    }
    /* 線下中國用戶 */
    $offline_cn_data = $data;
    $total = 0;
    foreach ($offline_cn as $value) {
        $date   = local_date($format, strtotime($value['day']));
        $offline_cn_data[$date] = $value['total'];
        $total += $value['total'];
    }
    $offline_cn_data   = array_values($offline_cn_data);
    $labels = array_values($labels);
    $smarty->assign('cn_offline_total',            price_format($total));
    $datasets [] = [
        'borderColor' => '#'.chart_color(0),
        'data' => $offline_cn_data,
        'label'=> '線下中國用戶營業額',
        'backgroundColor' => 'transparent'
    ];
    $bar_labels[] = '非香港用戶營業額';
    $bar_datasets [] = [
        'backgroundColor' => '#'.chart_color(0),
        'data' => [$total],
        'label'=> '線下中國用戶營業額',
    ];
    /* 線上中國用戶 */
    $online_cn_data = $data;
    $total = 0;
    foreach ($online_cn as $value) {
        $date   = local_date($format, strtotime($value['day']));
        $online_cn_data[$date] = $value['total'];
        $total += $value['total'];
    }
    $online_cn_data   = array_values($online_cn_data);
    $smarty->assign('cn_online_total', price_format($total));
    $datasets [] = [
        'borderColor' => '#'.chart_color(2),
        'data' => $online_cn_data,
        'label'=> '線上中國用戶營業額',
        'backgroundColor' => 'transparent'
    ];
//    $bar_labels[] = '線上中國用戶營業額';
    $bar_datasets [] = [
        'backgroundColor' => '#'.chart_color(2),
        'data' => [$total],
        'label'=> '線上中國用戶營業額',
    ];
    /* 海外用戶 */
    $overseas_data = $data;
    $total = 0;
    foreach ($overseas as $value) {
        $date   = local_date($format, strtotime($value['day']));
        $overseas_data[$date] = $value['total'];
        $total += $value['total'];
    }
    $overseas_data   = array_values($overseas_data);
    $smarty->assign('overseas_total', price_format($total));
    $datasets [] = [
        'borderColor' => '#'.chart_color(1),
        'data' => $overseas_data,
        'label'=> '海外用戶營業額',
        'backgroundColor' => 'transparent'
    ];
//    $bar_labels[] = '海外用戶營業額';
    $bar_datasets [] = [
        'backgroundColor' => '#'.chart_color(1),
        'data' => [$total],
        'label'=> '海外用戶營業額',
    ];

    $cn_list = ['datasets' => $datasets, 'labels' => $labels];
    $smarty->assign('cn_data',            $cn_list);
    $non_hk_bar_data = ['datasets' => $bar_datasets, 'labels' => $bar_labels];
    $smarty->assign('non_hk_bar_data',            $non_hk_bar_data);
    assign_query_info();
    $smarty->display('order_stats.htm');
}
elseif ($act == 'download')
{
    $filename = !empty($_REQUEST['filename']) ? trim($_REQUEST['filename']) : '';

    header("Content-type: application/vnd.ms-excel; charset=utf-8");
    header("Content-Disposition: attachment; filename=$filename.xls");
    $start_date = $_REQUEST['start_date'];
    $end_date   = $_REQUEST['end_date'];
    /* 订单概况 */
    $order_info = get_orderinfo($start_date, $end_date);
    $data = $_LANG['order_circs'] . "\n";
    $data .= "$_LANG[confirmed] \t $_LANG[paid] \t $_LANG[succeed] \t $_LANG[unconfirmed] \t $_LANG[invalid] \n";
    $data .= "$order_info[confirmed_num] \t $order_info[paid_num] \t $order_info[succeed_num] \t $order_info[unconfirmed_num] \t $order_info[invalid_num]\n";

    /* 支付方式 */
    $sql = 'SELECT i.pay_id, p.pay_name, COUNT(i.order_id) AS order_num ' .
            'FROM ' .$ecs->table('payment'). ' AS p, ' .$ecs->table('order_info'). ' AS i '.
            "WHERE p.pay_id = i.pay_id " . order_query_sql('finished') .where_exclude() .
            "AND i.shipping_time >= '$start_date' AND i.shipping_time <= '$end_date' ".
            "GROUP BY i.pay_id ORDER BY order_num DESC";
    $data .= "\n$_LANG[pay_method]\n";
    $pay_res= $db->getAll($sql);
    foreach ($pay_res AS $val)
    {
        $data .= $val['pay_name'] . "\t";
    }
    $data .= "\n";
    foreach ($pay_res AS $val)
    {
        $data .= $val['order_num'] . "\t";
    }
    $data .= "\n";

    /* 配送方式 */
    $sql = 'SELECT sp.shipping_id, sp.shipping_name AS ship_name, COUNT(i.order_id) AS order_num ' .
            'FROM ' .$ecs->table('shipping'). ' AS sp, ' .$ecs->table('order_info'). ' AS i ' .
            'WHERE sp.shipping_id = i.shipping_id ' . order_query_sql('finished') .where_exclude() .
            "AND i.shipping_time >= '$start_date' AND i.shipping_time <= '$end_date' " .
            "GROUP BY i.shipping_id ORDER BY order_num DESC";
    $ship_res = $db->getAll($sql);
    $data .= "\n$_LANG[shipping_method]\n";
    foreach ($ship_res AS $val)
    {
        $data .= $val['ship_name'] . "\t";
    }
    $data .= "\n";
    foreach ($ship_res AS $val)
    {
        $data .= $val['order_num'] . "\t";
    }
    $data .= "\n";

    /* 地區分佈 */
    $sql = 'SELECT r.region_name AS country,COUNT(oi.order_id) AS order_num FROM ' .$ecs->table('order_info'). ' oi ' .
      'LEFT JOIN ' .$ecs->table('region'). ' r ON r.region_id = oi.country ' .
      'WHERE country > 0' . order_query_sql('finished') . where_exclude() .
      "AND shipping_time >= '$start_date' AND shipping_time <= '$end_date' " .
      "GROUP BY country ORDER BY country ASC";
    $country_res = $db->getAll($sql);
    $data .= "\n$_LANG[country_distribution]\n";
    foreach ($country_res AS $val)
    {
        $data .= $val['country'] . "\t";
    }
    $data .= "\n";
    foreach ($country_res AS $val)
    {
        $data .= $val['order_num'] . "\t";
    }
    $data .= "\n";

    /* 落單設備 */
    $sql = 'SELECT platform,COUNT(order_id) AS order_num FROM ' .$ecs->table('order_info'). ' ' .
      'WHERE platform != "pos"' . order_query_sql('finished') . where_exclude() .
      "AND shipping_time >= '$start_date' AND shipping_time <= '$end_date' " .
      "GROUP BY platform ORDER BY platform ASC";
    $device_res = $db->getAll($sql);
    $data .= "\n$_LANG[device_distribution]\n";
    foreach ($device_res AS $val)
    {
        $data .= $orderController::DB_SELLING_PLATFORMS[$val['platform']]. "\t";
    }
    $data .= "\n";
    foreach ($device_res AS $val)
    {
        $data .= $val['order_num'] . "\t";
    }
    $data .= "\n";

    /* 訂單來源 */
    $sql = 'SELECT (CASE WHEN (platform = "web" OR platform = "mobile") THEN "網上" WHEN platform = "pos" THEN "門店" ELSE "其他" END) AS platforms , COUNT(order_id) AS order_num FROM ' .$ecs->table('order_info') .
      'WHERE platform != ""' . order_query_sql('finished') . where_exclude() .
      "AND shipping_time >= '$start_date' AND shipping_time <= '$end_date' " .
      "GROUP BY platforms ORDER BY platforms ASC";
    $source_res = $db->getAll($sql);
    $data .= "\n$_LANG[source_distribution]\n";
    foreach ($source_res AS $val)
    {
        $data .= $val['platforms'] . "\t";
    }
    $data .= "\n";
    foreach ($source_res AS $val)
    {
        $data .= $val['order_num'] . "\t";
    }
    $data .= "\n";

    /* VIP訂單 */
    $data .= "\n$_LANG[vip_distribution]\n";
    $data .= "VIP訂單\t非VIP訂單\t\n";
    $sql = 'SELECT COUNT(oi.order_id) AS order_num FROM ' .$ecs->table('order_info'). ' oi ' .
            'WHERE 1 in (SELECT IF(user_rank = 2, 1, 0) FROM ' .$ecs->table('order_goods'). ' og WHERE og.order_id = oi.order_id)' . order_query_sql('finished') . where_exclude() .
            "AND shipping_time >= '$start_date' AND shipping_time <= '$end_date'";
    $vip_res = $db->getOne($sql);
    $data .= $vip_res . "\t";
    $vip_xml .= "<set value='".$vip_res."' name='VIP訂單' color='".$color_array[0]."'/>";
    $sql = 'SELECT COUNT(*) FROM ' .$ecs->table('order_info'). ' WHERE order_id NOT in (' .
            'SELECT oi.order_id FROM ' .$ecs->table('order_info'). ' oi ' .
            'WHERE 1 in (SELECT IF(user_rank = 2, 1, 0) FROM ' .$ecs->table('order_goods'). ' og WHERE og.order_id = oi.order_id)) ' . order_query_sql('finished') . where_exclude() .
            "AND shipping_time >= '$start_date' AND shipping_time <= '$end_date'";
    $vip_res = $db->getOne($sql);
    $data .= $vip_res . "\t\n";

    /* 送貨途徑 */
    $sql = 'SELECT COALESCE(l.logistics_name,"無法歸類") AS logistics_name, COUNT(*) AS order_num, shipping_time FROM (' .
                'SELECT do.order_id, do.logistics_id, oi.shipping_time, COUNT(*) FROM ' . $ecs->table('delivery_order') . ' do ' .
                'LEFT JOIN ' . $ecs->table('order_info') . ' oi ON oi.order_id = do.order_id ' .
                'WHERE oi.shipping_time > 0 ' . order_query_sql('finished','oi.') .
                "AND oi.shipping_time >= '$start_date' AND oi.shipping_time <= '$end_date' " . where_exclude('oi.') .
                "GROUP BY oi.order_id, do.logistics_id) a " .
            "LEFT JOIN " . $ecs->table('logistics') . ' l ON l.logistics_id = a.logistics_id ' .
            "GROUP BY l.logistics_id ORDER BY l.logistics_id ASC";
    $delivery_res = $db->getAll($sql);
    $data .= "\n$_LANG[delivery_distribution]\n";
    foreach ($delivery_res AS $val)
    {
        $data .= $val['logistics_name'] . "\t";
    }
    $data .= "\n";
    foreach ($delivery_res AS $val)
    {
        $data .= $val['order_num'] . "\t";
    }
    $data .= "\n";

    /* 送貨途徑(包括SA) */
    $sql = 'SELECT COALESCE(l.logistics_name,"無法歸類") AS logistics_name, COUNT(*) AS order_num, shipping_time FROM (' .
                'SELECT do.order_id, do.logistics_id, oi.shipping_time, COUNT(*) FROM ' . $ecs->table('delivery_order') . ' do ' .
                'LEFT JOIN ' . $ecs->table('order_info') . ' oi ON oi.order_id = do.order_id ' .
                'WHERE oi.shipping_time > 0 ' . order_query_sql('finished','oi.') .
                "AND oi.shipping_time >= '$start_date' AND oi.shipping_time <= '$end_date' " . where_exclude('oi.',true) .
                "GROUP BY oi.order_id, do.logistics_id) a " .
            "LEFT JOIN " . $ecs->table('logistics') . ' l ON l.logistics_id = a.logistics_id ' .
            "GROUP BY l.logistics_id ORDER BY l.logistics_id ASC";
    $delivery2_res = $db->getAll($sql);
    $data .= "\n$_LANG[delivery2_distribution]\n";
    foreach ($delivery2_res AS $val)
    {
        $data .= $val['logistics_name'] . "\t";
    }
    $data .= "\n";
    foreach ($delivery2_res AS $val)
    {
        $data .= $val['order_num'] . "\t";
    }
    $data .= "\n";

    echo ecs_iconv(EC_CHARSET, 'BIG5', $data) . "\t";
    exit;

}

/*------------------------------------------------------ */
//--订单统计需要的函数
/*------------------------------------------------------ */
 /**
  * 取得订单概况数据(包括订单的几种状态)
  * @param       $start_date    开始查询的日期
  * @param       $end_date      查询的结束日期
  * @return      $order_info    订单概况数据
  */
 function get_orderinfo($start_date, $end_date)
 {
    $order_info = array();

    /* 未确认订单数 */
    $sql = 'SELECT COUNT(*) AS unconfirmed_num FROM ' .$GLOBALS['ecs']->table('order_info').
           " WHERE order_status = '" .OS_UNCONFIRMED. "' AND shipping_time >= '$start_date'". where_exclude() .
           " AND shipping_time < '" . ($end_date) . "'";

    $order_info['unconfirmed_num'] = $GLOBALS['db']->getOne($sql);

    /* 已确认订单数 */
    $sql = 'SELECT COUNT(*) AS confirmed_num FROM ' .$GLOBALS['ecs']->table('order_info').
           " WHERE order_status = '" .OS_CONFIRMED. "' AND shipping_status NOT ". db_create_in(array(SS_SHIPPED, SS_RECEIVED)) . " AND pay_status NOT" . db_create_in(array(PS_PAYED, PS_PAYING)) ." AND shipping_time >= '$start_date'". where_exclude() .
           " AND shipping_time < '" . ($end_date) . "'";
    $order_info['confirmed_num'] = $GLOBALS['db']->getOne($sql);

    /* 已付款订单数 */
    $sql = 'SELECT COUNT(*) AS paid_num FROM ' .$GLOBALS['ecs']->table('order_info').
           " WHERE order_status = '" .OS_CONFIRMED. "' AND shipping_status NOT ". db_create_in(array(SS_SHIPPED, SS_RECEIVED)) . " AND pay_status " . db_create_in(array(PS_PAYED)) ." AND shipping_time >= '$start_date'". where_exclude() .
           " AND shipping_time < '" . ($end_date) . "'";
    $order_info['paid_num'] = $GLOBALS['db']->getOne($sql);
    /* 已成交订单数 */
    $sql = 'SELECT COUNT(*) AS succeed_num FROM ' .$GLOBALS['ecs']->table('order_info').
           " WHERE 1 " . order_query_sql('finished') . where_exclude() .
           " AND shipping_time >= '$start_date' AND shipping_time < '" . ($end_date) . "'";
    $order_info['succeed_num'] = $GLOBALS['db']->getOne($sql);$order_info['sql'] = $sql;

    /* 无效或已取消订单数 */
    $sql = "SELECT COUNT(*) AS invalid_num FROM " .$GLOBALS['ecs']->table('order_info').
           " WHERE order_status > '" .OS_CONFIRMED. "'". where_exclude() .
           " AND shipping_time >= '$start_date' AND shipping_time < '" . ($end_date) . "'";
    $order_info['invalid_num'] = $GLOBALS['db']->getOne($sql);
    return $order_info;
}
?>