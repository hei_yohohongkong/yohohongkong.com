<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_base.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_supplier.php');
if($_REQUEST['act'] == 'list')
{
	if((admin_priv('erp_sys_manage','',false)))
	{
		include('./includes/ERP/page.class.php');
		$num_per_page=20;
		$mode=1;
		$page_bar_num=6;
		$page_style="page_style";
		$current_page_style="current_page_style";
		$page=isset($_REQUEST['page'])?intval($_REQUEST['page']):1;
		$start=$num_per_page*($page-1);
		$total_num=supplier_group_count();
		$supplier_groups=supplier_group_list($start,$num_per_page);
		$url="erp_supplier_group.php?act=list";
		$smarty->assign('url',$_SERVER["REQUEST_URI"].'&page='.$page);
		$pager=new page(array('total_data'=>$total_num,'data_per_page'=>$num_per_page,'url'=>$url,'mode'=>$mode,'page_bar_num'=>$page_bar_num,'page_style'=>$page_style,'current_page_style'=>$current_page_style));
		$smarty->assign('pager',$pager->show());
		$action_link = array('href'=>'erp_supplier_group.php?act=add','text'=>$_LANG['erp_add_supplier_group']);
		$smarty->assign('action_link',$action_link);
		$smarty->assign('supplier_groups',$supplier_groups);
		$smarty->assign('ur_here',$GLOBALS['_LANG']['erp_supplier_group']);
		assign_query_info();
		$smarty->display('erp_supplier_group_list.htm');
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif($_REQUEST['act'] == 'change_is_valid')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_sys_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	else
	{
		$group_id=$_REQUEST['group_id'];
		$group_info=supplier_group_info($group_id);
		$is_valid=$group_info['is_valid'];
		if($is_valid==1)
		{
			$sql="update ".$ecs->table('erp_supplier_group')." set is_valid='0' where group_id='".$group_id."'";
			$db->query($sql);
			$result['error']=0;
			$result['is_valid']=0;
			die($json->encode($result));
		}
		elseif($is_valid==0)
		{
			$sql="update ".$ecs->table('erp_supplier_group')." set is_valid='1' where group_id='".$group_id."'";
			$db->query($sql);
			$result['error']=0;
			$result['is_valid']=1;
			die($json->encode($result));
		}
	}
}
elseif($_REQUEST['act'] == 'edit')
{
	if((admin_priv('erp_sys_manage','',false)))
	{
		$group_id=isset($_REQUEST['id']) &&intval($_REQUEST['id'])>0?intval($_REQUEST['id']):0;
		if(empty($group_id))
		{
			$href="erp_supplier_group.php?act=list";
			$text=$_LANG['erp_supplier_group_return'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_wrong_parameter'],0,$link);
		}
		$smarty->assign('supplier_group',supplier_group_info($group_id));
		$sql="select admin_id from ".$GLOBALS['ecs']->table('erp_supplier_group_admin')." where group_id='".$group_id."'";
		$res=$GLOBALS['db']->query($sql);
		while($row=mysql_fetch_assoc($res))
		{
			$admin_ids[]=$row['admin_id'];
		}
		$smarty->assign('admin_list',admin_list($admin_ids));
		$smarty->assign('ur_here',$GLOBALS['_LANG']['erp_edit_supplier_group']);
		$smarty->assign('act','update');
		assign_query_info();
		$smarty->display('erp_supplier_group.htm');
	}
	else
	{
		$href="erp_supplier_group.php?act=list";
		$text=$_LANG['erp_supplier_group_return'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif($_REQUEST['act'] == 'remove')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_sys_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	else
	{
		$group_id=isset($_REQUEST['group_id'])?intval($_REQUEST['group_id']) : 0;
		if(empty($group_id))
		{
			$result['error']=2;
			$result['message']=$_LANG['erp_wrong_parameter'];
			die($json->encode($result));
		}
		if(delete_supplier_group($group_id))
		{
			$result['error']=0;
			die($json->encode($result));
		}
		else
		{
			$result['error']=3;
			$result['message']=$_LANG['erp_supplier_group_has_supplier'];
			die($json->encode($result));
		}
	}
}
elseif($_REQUEST['act'] == 'add')
{
	if((admin_priv('erp_sys_manage','',false)))
	{
		$smarty->assign('ur_here',$GLOBALS['_LANG']['erp_add_supplier_group']);
		$smarty->assign('act','insert');
		$smarty->assign('admin_list',admin_list());
		assign_query_info();
		$smarty->display('erp_supplier_group.htm');
	}
	else
	{
		$href="erp_supplier_group.php?act=list";
		$text=$_LANG['erp_supplier_group_return'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif($_REQUEST['act'] == 'insert')
{
	if((admin_priv('erp_sys_manage','',false)))
	{
		$is_valid=isset($_REQUEST['is_valid']) &&intval($_REQUEST['is_valid'])>0 ?intval($_REQUEST['is_valid']) : 0;
		$group_name=isset($_REQUEST['group_name']) ?trim($_REQUEST['group_name']) : '';
		if(empty($group_name))
		{
			$href="erp_supplier_group.php?act=list";
			$text=$_LANG['erp_supplier_group_return'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_wrong_parameter'],0,$link);
		}
		$sql="insert into ".$GLOBALS['ecs']->table('erp_supplier_group')." set group_name='".$group_name."',is_valid='".$is_valid."'";
		$GLOBALS['db']->query($sql);
		$group_id=$GLOBALS['db']->insert_id();
		if(isset($_REQUEST['admins']))
		{
			foreach($_REQUEST['admins'] as $key =>$admin_id)
			{
				$admin_id=intval($admin_id);
				if($admin_id >0)
				{
					$sql="insert into ".$GLOBALS['ecs']->table('erp_supplier_group_admin')." set group_id='".$group_id."',admin_id='".$admin_id."'";
					$GLOBALS['db']->query($sql);
				}
			}
		}
		$href="erp_supplier_group.php?act=list";
		$text=$_LANG['erp_supplier_group_return'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_supplier_group_add_success'],0,$link);
	}
	else
	{
		$href="erp_supplier_group.php?act=list";
		$text=$_LANG['erp_supplier_group_return'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif($_REQUEST['act'] == 'update')
{
	if((admin_priv('erp_sys_manage','',false)))
	{
		$group_id=isset($_REQUEST['group_id']) &&intval($_REQUEST['group_id'])>0 ?intval($_REQUEST['group_id']) : 0;
		$is_valid=isset($_REQUEST['is_valid']) &&intval($_REQUEST['is_valid'])>0 ?intval($_REQUEST['is_valid']) : 0;
		$group_name=isset($_REQUEST['group_name']) ?trim($_REQUEST['group_name']) : '';
		if(empty($group_id) ||empty($group_name))
		{
			$href="erp_supplier_group.php?act=list";
			$text=$_LANG['erp_supplier_group_return'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_wrong_parameter'],0,$link);
		}
		$sql="update ".$GLOBALS['ecs']->table('erp_supplier_group')." set group_name='".$group_name."',is_valid='".$is_valid."' where group_id='".$group_id."'";
		$GLOBALS['db']->query($sql);
		$sql="delete from ".$GLOBALS['ecs']->table('erp_supplier_group_admin')." where group_id='".$group_id."'";
		$GLOBALS['db']->query($sql);
		if(isset($_REQUEST['admins']))
		{
			foreach($_REQUEST['admins'] as $key =>$admin_id)
			{
				$admin_id=intval($admin_id);
				if($admin_id >0)
				{
					$sql="insert into ".$GLOBALS['ecs']->table('erp_supplier_group_admin')." set group_id='".$group_id."',admin_id='".$admin_id."'";
					$GLOBALS['db']->query($sql);
				}
			}
		}
		$href="erp_supplier_group.php?act=list";
		$text=$_LANG['erp_supplier_group_return'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_supplier_group_edit_success'],0,$link);
	}
	else
	{
		$href="erp_supplier_group.php?act=list";
		$text=$_LANG['erp_supplier_group_return'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
?>