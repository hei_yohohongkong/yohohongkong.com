<?php

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require(ROOT_PATH . 'includes/lib_order.php');
require_once(ROOT_PATH .'/languages/zh_tw/admin/order.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_base.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_warehouse.php');
require(dirname(__FILE__) .'/includes/ERP/cls/cls_date.php');

$imageCompressionController = new Yoho\cms\Controller\ImageCompressionController();

$rmaController = new Yoho\cms\Controller\RmaController();

if (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'query')) {
    /* 检查权限 */

    $imageCompressionController->ajaxQueryAction($result['data'],$result['record_count'],false);
}else if (isset($_REQUEST['act']) && $_REQUEST['act'] == 'list') {
    $issue_count_list = $rmaController->getIssueCountByPerson(true);
    $monthly_data = $rmaController->getReportDataMonthly();
    $goods_top = $rmaController->getRmaGoodsTop(10);
    $data = $rmaController->getReportData();
    $rma_status_data = $rmaController->getRmaStatusData();

    // echo '<pre>';
    // print_r($rma_status_data);
    // exit;
    $smarty->assign('issue_count_list', $issue_count_list);
    $smarty->assign('goods_top', $goods_top);
    $smarty->assign('ur_here', 'Tiny壓縮圖片使用量報告');
    $smarty->assign('image_compression_limit', $_CFG['lang']);
    $smarty->assign('cfg_tiny_limit', $_CFG['tiny_limit']);
    $smarty->assign('lang', $_LANG);
    $smarty->assign('monthly_data', $monthly_data);
    $smarty->assign('rma_status_data', $rma_status_data);
    $smarty->assign('data', $data);

    /* 显示页面 */
    assign_query_info();
    $smarty->display('rma_report.htm');
}