<?php

/**
 * ECSHOP 后台标签管理
 * ============================================================================
 * 版权所有 2005-2010 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liuhui $
 * $Id: tag_manage.php 17063 2010-03-25 06:35:46Z liuhui $
*/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
$couponPromoteController = new Yoho\cms\Controller\CouponPromoteController();

/* act操作项的初始化 */
$_REQUEST['act'] = trim($_REQUEST['act']);
if (empty($_REQUEST['act']))
{
    $_REQUEST['act'] = 'list';
}

/*------------------------------------------------------ */
//-- 获取标签数据列表
/*------------------------------------------------------ */
if ($_REQUEST['act'] == 'list')
{
    /* 权限判断 */
    //admin_priv($admin_priv);

    /* 模板赋值 */
    $smarty->assign('ur_here',      '信用卡推廣');
    $smarty->assign('action_link2', array('href' => 'coupon_promote_manage.php?act=add', 'text' => '新增信用卡推廣'));
    $smarty->assign('action_link', array('href' => 'coupon.php?act=list', 'text' => '優惠券'));//優惠券
    $smarty->assign('full_page',    1);

    /* 页面显示 */
    assign_query_info();
    $smarty->display('coupon_promote_manage.htm');
}

/*------------------------------------------------------ */
//-- 添加 ,编辑
/*------------------------------------------------------ */

elseif($_REQUEST['act'] == 'add' || $_REQUEST['act'] == 'edit')
{
    //admin_priv($admin_priv);

    $is_add = $_REQUEST['act'] == 'add';
    $smarty->assign('insert_or_update', $is_add ? 'insert' : 'update');
    $localizable_fields = localizable_fields_for_table('coupon_promote');
    $smarty->assign('localizable_fields', $localizable_fields);

    if($is_add)
    {
        $coupon_promote = array(
            'promote_id' => 0,
            'tag_words' => '',
            'bin' => '',
            'promote_name' => ''
        );
        $smarty->assign('ur_here', '新增信用卡推廣');
    }
    else
    {
        $coupon_promote_id = $_GET['id'];
        $coupon_promote = $couponPromoteController->get_coupon_promote_info($coupon_promote_id);
       
        // bin 
        $coupon_promote['bin'] = implode("\n",$coupon_promote['config']['card_bin']);
      
        $smarty->assign('localized_versions', get_localized_versions('coupon_promote', $coupon_promote_id, $localizable_fields));
        $smarty->assign('ur_here', '編輯信用卡推廣');
    }
    $smarty->assign('coupon_promote', $coupon_promote);
    $smarty->assign('action_link', array('href' => 'coupon_promote_manage.php?act=list', 'text' => '返回信用卡優惠' ));
    assign_query_info();
    $smarty->display('coupon_promote_edit.htm');
}

/*------------------------------------------------------ */
//-- 更新
/*------------------------------------------------------ */

elseif($_REQUEST['act'] == 'insert' || $_REQUEST['act'] == 'update')
{
    $is_insert = $_REQUEST['act'] == 'insert';
    $post = $_POST;

    $res = $couponPromoteController->insert_update_promote($post, $is_insert);
    if($res)
    {
        /* 清除缓存 */
        //clear_cache_files();

        $link[0]['text'] = $_LANG['back_list'];
        $link[0]['href'] = 'coupon_promote_manage.php?act=list';

        sys_msg('新增成功', 0, $link);
    } else {
        $link[0]['text'] = $_LANG['back_list'];

        if ($_REQUEST['act'] == 'update') {
            $link[0]['href'] = 'coupon_promote_manage.php?act=edit&id='.$post['id'];
        } else {
            $link[0]['href'] = 'coupon_promote_manage.php?act=list';
        }

        sys_msg('信用卡(BIN) 格式錯誤', 0, $link);
    }
}

/*------------------------------------------------------ */
//-- 翻页，排序
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'query')
{
    //check_authz_json($admin_priv);
    $coupon_promote_list = $couponPromoteController->get_coupon_promote_list();
    $couponPromoteController->ajaxQueryAction($coupon_promote_list['promotes'],  $tag_list['record_count']);
}

/*------------------------------------------------------ */
//-- 删除标签
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'remove')
{
    //check_authz_json($admin_priv);

    include_once(ROOT_PATH . 'includes/cls_json.php');
    $json = new JSON;

    $id = intval($_GET['id']);

    $sql = "select count(*) from ".$ecs->table('coupons')." where promote_id = ".$id." ";
    $coupon_using = $db->getOne($sql);

    if ($coupon_using == 0)
    {
        $sql = "DELETE FROM " .$ecs->table('coupon_promote'). " WHERE promote_id = '$id'";
        $result = $GLOBALS['db']->query($sql);
        exit;
    }
    else
    {
       make_json_error('error');
    }
}

?>
