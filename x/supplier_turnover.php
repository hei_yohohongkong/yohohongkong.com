<?php
define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require_once(ROOT_PATH . 'includes/lib_order.php');
require_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_goods.php');
require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/admin/statistic.php');

$smarty->assign('lang', $_LANG);
$userController = new Yoho\cms\Controller\UserController();
$erpController = new Yoho\cms\Controller\ErpController();
$supplierController = new Yoho\cms\Controller\SupplierController();
if(isset($_REQUEST['act'])){
    $sql = "select au.user_name as admin_name, esa.admin_id from ".$ecs->table('erp_supplier_admin')." as esa ".
    " inner join ".$ecs->table('admin_user')." as au on au.user_id = esa.admin_id ".
    " WHERE esa.admin_id > 0 and au.is_disable = 0 ".
    " group by esa.admin_id";
    $supplier_admin_list = $db->getAll($sql);
    $admin_list = [];
    foreach($supplier_admin_list as $admin) {
        if ($_SESSION['manage_cost']) {
            $admin_list[$admin['admin_id']] = $admin['admin_name'];
        } elseif ($_SESSION['admin_id'] == $admin['admin_id']) {
            $admin_list[$admin['admin_id']] = $admin['admin_name'];
        }
    }
    if ($_SESSION['manage_cost'] || $_SESSION['role_id'] == 4) { //role_id = tech team, using for testing.
        $admin_id = empty($_REQUEST['admin_id']) ? $_SESSION['admin_id'] : intval($_REQUEST['admin_id']);
    } else {
        $admin_id = intval($_SESSION['admin_id']);
    }
    $smarty->assign('admin_id',       $admin_id);
    $smarty->assign('admin_list',     $admin_list);
    if($_REQUEST['act'] == 'query'){
        if (strstr($_REQUEST['start_date'], '-') === false)
        {
            $_REQUEST['start_date'] = local_date('Y-m-d', $_REQUEST['start_date']);
            $_REQUEST['end_date'] = local_date('Y-m-d', $_REQUEST['end_date']);
        }
        
        $_REQUEST['order_type'] = '4';
        $_REQUEST['deprecatedflow'] = 'N';
        $data = $supplierController->get_sale_supplier(false, $admin_id);
        $smarty->assign('data',            $data['data']);
        $smarty->assign('total_discount',  $data['total_discount']);
        $smarty->assign('start_date',      $_REQUEST['start_date']);
        $smarty->assign('end_date',        $_REQUEST['end_date']);

        make_json_result($smarty->fetch('supplier_turnover.htm'), '');
    }else if($_REQUEST['act'] == 'list'){
        if (!isset($_REQUEST['start_date']))
        {
            $start_date = strtotime(month_start());
        }
        if (!isset($_REQUEST['end_date']))
        {
            $end_date = strtotime(month_end());
		}
		$_REQUEST['order_type'] = '4';
        $_REQUEST['deprecatedflow'] = 'N';
        $data = $supplierController->get_sale_supplier(false, $admin_id);
        $smarty->assign('ur_here',        $_LANG['supplier_turnover']);
        $smarty->assign('total_discount', $data['total_discount']);
        $smarty->assign('full_page',      1);
        $smarty->assign('start_date',     local_date('Y-m-d', $start_date));
        $smarty->assign('end_date',       local_date('Y-m-d', $end_date));
        $smarty->assign('data',           $data['data']);
        $smarty->assign('cfg_lang',       $_CFG['lang']);
        assign_query_info();
        $smarty->display('supplier_turnover.htm');
    }
}

function month_start()
{
	return local_date('Y-m-01');
}
function month_end()
{
	return local_date('Y-m-t');
}
?>