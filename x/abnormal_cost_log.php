<?php

use Yoho\cms\Controller\YohoBaseController;

/***
* abnormal_cost_log.php
* by howang 2014-10-09
*
* Display the log of goods cost changes
***/

define('IN_ECS', true);
ini_set('memory_limit', '256M');
require(dirname(__FILE__) . '/includes/init.php');
require_once ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_supplier.php';

$abnormalCostLogController = new Yoho\cms\Controller\YohoBaseController("abnormal_cost_log");
$abnormalPriceController = new Yoho\cms\Controller\YohoBaseController("goods");
$autoPricingController = new Yoho\cms\Controller\AutoPricingController();

if ($_REQUEST['act'] == 'list')
{
    /* 检查权限 */
    admin_priv('goods_manage');
    
    if (!$_SESSION['manage_cost'] && !check_authz(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ONLYSELFSUPPLIERS))
    {
        sys_msg('Admin only!', 1);
    }

    assign_query_info();
    $webs = $autoPricingController::AUTO_PRICING_WEBS;
    $smarty->assign("ur_here", $_LANG['10_abnormal_cost_log']);
    $smarty->assign("webs", $webs);
    $smarty->display('abnormal_cost_log.htm');
}
elseif ($_REQUEST['act'] == 'query')
{
    /* 检查权限 */
    check_authz_json('goods_manage');

    if (!$_SESSION['manage_cost'] && !check_authz(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ONLYSELFSUPPLIERS))
    {
        make_json_error('Admin only!');
    }

    $list = get_abnormal_cost_log();
    $abnormalCostLogController->ajaxQueryAction($list['data'], $list['record_count'], false);

}
// 封存
elseif ($_REQUEST['act'] == 'remove')
{
    /* 检查权限 */
    check_authz_json('goods_manage');
    
    if (!$_SESSION['manage_cost'] && !check_authz(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ONLYSELFSUPPLIERS))
    {
        make_json_error('Admin only!');
    }
    
    $log_id = intval($_GET['id']);
    
    $sql = "UPDATE " .$ecs->table('abnormal_cost_log'). " SET `is_archived` = 1 WHERE log_id = '$log_id'";
    $res = $db->query($sql);
    
    header('Location: abnormal_cost_log.php?' . str_replace('act=remove', 'act=list', $_SERVER['QUERY_STRING']));
    exit;
}
// 還原
elseif ($_REQUEST['act'] == 'recover')
{
    /* 检查权限 */
    check_authz_json('goods_manage');
    
    if (!$_SESSION['manage_cost'] && !check_authz(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ONLYSELFSUPPLIERS))
    {
        make_json_error('Admin only!');
    }
    
    $log_id = intval($_GET['id']);
    
    $sql = "UPDATE " .$ecs->table('abnormal_cost_log'). " SET `is_archived` = 0 WHERE log_id = '$log_id'";
    $res = $db->query($sql);
    
    header('Location: abnormal_cost_log.php?' . str_replace('act=recover', 'act=list', $_SERVER['QUERY_STRING']));
    exit;
}
// ajax query
elseif ($_REQUEST['act'] == 'ajax') {
    $list = get_abnormal_cost_log();
    make_json_response($list['data']);
} elseif ($_REQUEST['act'] == 'price_query') {
    /* 检查权限 */
    check_authz_json('goods_manage');
    
    if (!$_SESSION['manage_cost'] && !check_authz(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ONLYSELFSUPPLIERS))
    {
        make_json_error('Admin only!');
    }
    
    $list = get_abnormal_price();
    $abnormalPriceController->ajaxQueryAction($list['data'], $list['record_count'], false);
}


function get_abnormal_cost_log()
{
    global $abnormalCostLogController;
    $result = get_filter();
    if ($result === false)
    {
        $filter['mode']         = empty($_REQUEST['mode']) ? 'normal' : trim($_REQUEST['mode']);
        
        $filter['sort_by']      = empty($_REQUEST['sort_by']) ? 'log_id' : trim($_REQUEST['sort_by']);
        $filter['sort_order']   = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
        
        $where = " WHERE 1 ";
        if ($filter['mode'] == 'normal')
        {
            $where .= " AND acl.is_archived = '0' ";
        }
        elseif ($filter['mode'] == 'archived')
        {
            $where .= " AND acl.is_archived = '1' ";
        }

        if (!empty($_REQUEST['goods_name'])) {
            $where .= " AND goods_name LIKE '%$_REQUEST[goods_name]%'";
        }

        if (!empty($_REQUEST['log_id'])) {
            $where .= " AND log_id = $_REQUEST[log_id]";
        }

        if (!$_SESSION['manage_cost'] && check_authz(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ONLYSELFSUPPLIERS)) {
            $self_supplier_id = get_self_supplier_id();
            if (count($self_supplier_id) > 0) {
                $where .= " AND g.goods_id IN (SELECT goods_id FROM " . $GLOBALS['ecs']->table('erp_goods_supplier') .
                            " WHERE supplier_id " . db_create_in($self_supplier_id) . ")";
            }
        }
        
        $orderby = $filter['sort_by'] . ' ' . $filter['sort_order'] . ' ';

        $sql = "SELECT count(*) ";
        $sql.= " FROM " . $GLOBALS['ecs']->table('abnormal_cost_log') . " as acl ";
        $sql.= " LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON acl.`goods_id` = g.`goods_id`";
        $sql.= $where;
        $filter['record_count'] = $GLOBALS['db']->getOne($sql);

        /* 查询 */
        $sql = "SELECT acl.*, g.`goods_name`, g.`goods_thumb`, g.`goods_sn`, es.`name` as supplier_name ";
        $sql.= " FROM " . $GLOBALS['ecs']->table('abnormal_cost_log') . " as acl ";
        $sql.= " LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON acl.`goods_id` = g.`goods_id`";
        $sql.= " LEFT JOIN " . $GLOBALS['ecs']->table('erp_supplier') . " as es ON acl.`supplier_id` = es.`supplier_id`";
        $sql.= $where;
        $sql.= " ORDER BY " . $orderby;
        $sql.= " LIMIT " . $_REQUEST['start'] . ",$_REQUEST[page_size]";
        set_filter($filter, $sql);
    }
    else
    {
        $sql    = $result['sql'];
        $filter = $result['filter'];
    }

    $data = $GLOBALS['db']->getAll($sql);
    
    foreach ($data as $key => $row)
    {
        $data[$key]['difference_text'] = "<span class='" . ($row['difference'] > 0 ? "red" : "green") . "'>" . round($row['difference'] * 100.0, 2) . '%</span>';
        $data[$key]['original_cost_rounded'] = round($row['original_cost'], 2);
        $data[$key]['warehousing_cost_rounded'] = round($row['warehousing_cost'], 2);
        $data[$key]['formatted_goods_name'] = "<a href='/goods.php?id=$row[goods_id]' target='_blank'>$row[goods_name]</a>";
        $data[$key]['formatted_goods_sn'] = "<a href='goods.php?act=list&keyword=$row[goods_sn]' target='_blank'>$row[goods_sn]</a>";
        $data[$key]['goods_image'] = "<img src='/$row[goods_thumb]' width='40' height='40'>";
        $data[$key]['formatted_erp_order_sn'] = "<a href='erp_order_manage.php?act=order_list&order_sn=$row[erp_order_sn]' target='_blank'>$row[erp_order_sn]</a>";
        $data[$key]['formatted_warehousing_sn'] = "<a href='erp_warehousing_manage.php?act=list&order_sn=$row[warehousing_sn]' target='_blank'>$row[warehousing_sn]</a>";
        if ($row['is_archived']) {
            $custom_actions = [
                'recover' => [
                    'title' => '還原記錄',
                    'label' => '還原',
                    'btn_class' => 'btn btn-success',
                    'icon_class' => 'fa fa-reply'
                ]
            ];
        } else {
            $custom_actions = [
                'remove' => [
                    'title' => '封存記錄',
                    'label' => '封存',
                    'btn_class' => 'btn btn-danger',
                    'icon_class' => 'fa fa-archive'
                ]
            ];
        }
        $data[$key]["_action"] = $abnormalCostLogController->generateCrudActionBtn($row['log_id'], 'id', null, [], $custom_actions);
    }

    $arr = array('data' => $data, 'filter' => $filter, 'page_count' => $filter['page_count'], 'record_count' => $filter['record_count']);

    return $arr;
}

function get_abnormal_price()
{
    global $db, $ecs, $autoPricingController;
    $sort_by = empty($_REQUEST['sort_by']) ? "suggest_id" : $_REQUEST['sort_by'];
    $sort_order = empty($_REQUEST['sort_order']) ? "asc" : $_REQUEST['sort_order'];
    $start = empty($_REQUEST['start']) ? 0 : intval($_REQUEST['start']);
    $page_size = empty($_REQUEST['page_size']) ? 50 : intval($_REQUEST['page_size']);

    $where = " WHERE 1";

    if (!empty($_REQUEST['goods_name'])) {
        $where .= " AND goods_name LIKE '%$_REQUEST[goods_name]%'";
    }

    if (!$_SESSION['manage_cost']) {
        $self_supplier_id = get_self_supplier_id();
        if (!$_SESSION['manage_cost'] && check_authz(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ONLYSELFSUPPLIERS)) {
            if (count($self_supplier_id) > 0) {
                $self_goods_id = get_supplier_goods_ids($self_supplier_id);
            }
        }
        $where .= " AND g.goods_id " . db_create_in($self_goods_id);
    }

    $price_process = ['cost'];
    $web_columns = [];
    foreach (array_keys($autoPricingController::AUTO_PRICING_TABLES) as $table) {
        $web_columns[] = "(" .$table . "_price > 0 AND " . $table . "_price < g.cost)";
        $price_process[] = $table . "_price";
    }
    if (!empty($web_columns)) {
        $where .= " HAVING (" . implode(" OR ", $web_columns) . ")";
    }

    $web_sql = $autoPricingController->getWebSql(false);

    $sql_base = "SELECT " .
            $web_sql .
            "g.goods_id, g.goods_name, g.goods_sn, g.cost, g.goods_thumb " .
            "FROM " .  $ecs->table("goods") . " g " .
            $where . (empty($sort_by) ? "" : " ORDER BY $sort_by $sort_order");
    $sql = $sql_base . " LIMIT $start, $page_size";
    $data = $db->getAll($sql);
    $count = count($db->getAll($sql_base));

    foreach ($data as $key => $row) {
        foreach ($row as $name => $val) {
            if (in_array($name, $price_process)) {
                if ($val == -1) {
                    $data[$key][$name] = "--";
                } else {
                    $data[$key][$name] = sprintf('%0.2f', $val);
                }
            }
        }
        foreach (array_keys($autoPricingController::AUTO_PRICING_TABLES) as $table) {
            if (floatval($row[$table . "_price"]) > 0 && $data[$key][$table . "_price"] < $data[$key]['cost']) {
                $data[$key][$table . "_price"] = "<span class='red'>" . $data[$key][$table . "_price"] . "</span>";
            }
        }
        $data[$key]['formatted_goods_name'] = "<a href='/goods.php?id=$row[goods_id]' target='_blank'>$row[goods_name]</a>";
        $data[$key]['formatted_goods_sn'] = "<a href='goods.php?act=list&keyword=$row[goods_sn]' target='_blank'>$row[goods_sn]</a>";
        $data[$key]['goods_image'] = "<img src='/$row[goods_thumb]' width='40' height='40'>";
    }
    return [
        'data' => $data,
        'record_count' => $count,
    ];
}
?>