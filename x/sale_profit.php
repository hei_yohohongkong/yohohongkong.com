<?php

/***
* sale_profit.php
* by howang 2014-07-07
*
* Sales Profit Calculator for YOHO Hong Kong
***/


define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require_once(ROOT_PATH . 'includes/lib_order.php');
require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/admin/statistic.php');
$smarty->assign('lang', $_LANG);
$userController = new Yoho\cms\Controller\UserController();
$orderController = new Yoho\cms\Controller\OrderController();
if (isset($_REQUEST['act']) && $_REQUEST['act'] == 'detail')
{
    /* 权限判断 */
    admin_priv('sale_order_stats');
    
    $start_date = empty($_REQUEST['start_date']) ? local_strtotime(month_start()) : local_strtotime($_REQUEST['start_date']);
    $end_date = empty($_REQUEST['end_date']) ? local_strtotime(month_end()) : local_strtotime($_REQUEST['end_date']);
    $salesperson = empty($_REQUEST['salesperson']) ? '' : $_REQUEST['salesperson'];

    $data = get_sale_profit_detail();

    $smarty->assign('filter',           $data['filter']);
    $smarty->assign('record_count',     $data['record_count']);
    $smarty->assign('page_count',       $data['page_count']);
    $smarty->assign('profit_list',      $data['data']);
    $smarty->assign('ur_here',          '盈利統計詳情');
    $smarty->assign('full_page',        1);
    $smarty->assign('start_date',       local_date('Y-m-d', $start_date));
    $smarty->assign('end_date',         local_date('Y-m-d', $end_date));
    $smarty->assign('salesperson',      $salesperson);
    $smarty->assign('cfg_lang',         $_CFG['lang']);
    $smarty->assign('action_link',      array('text' => '下載盈利統計報表','href'=>'#download'));
    $smarty->assign('order_type',        $_REQUEST['order_type']);
    if($_REQUEST['order_type'] == 3){
        $smarty->assign('display_sa_th', true);
    }
    
    /* 显示页面 */
    assign_query_info();
    $smarty->display('sale_profit_detail.htm');
}
else if (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'query_detail' ||  $_REQUEST['act'] == 'download_detail'))
{
    /* 检查权限 */
    check_authz_json('sale_order_stats');
    if (strstr($_REQUEST['start_date'], '-') === false)
    {
        $_REQUEST['start_date'] = local_date('Y-m-d', $_REQUEST['start_date']);
        $_REQUEST['end_date'] = local_date('Y-m-d', $_REQUEST['end_date']);
    }
    
    /*------------------------------------------------------ */
    //--Excel文件下载
    /*------------------------------------------------------ */
    if ($_REQUEST['act'] == 'download_detail')
    {
        $file_name = $_REQUEST['start_date'].'_'.$_REQUEST['end_date'].'_'.$_REQUEST['salesperson'] . '_profit_detail';
        $data = get_sale_profit_detail(false);
        header("Content-type: application/vnd.ms-excel; charset=utf-8");
        header("Content-Disposition: attachment; filename=$file_name.xls");
        
        require_once ROOT_PATH . 'includes/excel/PHPExcel.php';
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        
        $objPHPExcel->getActiveSheet()->setTitle('盈利統計詳情');
        
        /* 文件标题 */
        if($_REQUEST['order_type'] == 3){
            $objPHPExcel->getActiveSheet()
            ->setCellValue('A1', ecs_iconv(EC_CHARSET, 'UTF8', $_REQUEST['start_date']. $_LANG['to'] .$_REQUEST['end_date']. ' 盈利統計詳情'))
            ->setCellValue('A2', ecs_iconv(EC_CHARSET, 'UTF8', '銷售人員'))
            ->setCellValue('B2', ecs_iconv(EC_CHARSET, 'UTF8', '訂單編號'))
            ->setCellValue('C2', ecs_iconv(EC_CHARSET, 'UTF8', '訂單日期'))
            ->setCellValue('D2', ecs_iconv(EC_CHARSET, 'UTF8', '訂單金額'))
            ->setCellValue('E2', ecs_iconv(EC_CHARSET, 'UTF8', '實際訂單總數(扣除手續費)'))
            ->setCellValue('F2', ecs_iconv(EC_CHARSET, 'UTF8', '訂單成本'))
            ->setCellValue('G2', ecs_iconv(EC_CHARSET, 'UTF8', '訂單盈利'))
            ->setCellValue('H2', ecs_iconv(EC_CHARSET, 'UTF8', '實際盈利總額(扣除手續費)'))
            ->setCellValue('I2', ecs_iconv(EC_CHARSET, 'UTF8', '盈利率'))
            ->setCellValue('J2', ecs_iconv(EC_CHARSET, 'UTF8', '實際盈利率(扣除手續費)'));
            $objPHPExcel->getActiveSheet()->mergeCells('A1:J1');
        } else {
            $objPHPExcel->getActiveSheet()
            ->setCellValue('A1', ecs_iconv(EC_CHARSET, 'UTF8', $_REQUEST['start_date']. $_LANG['to'] .$_REQUEST['end_date']. ' 盈利統計詳情'))
            ->setCellValue('A2', ecs_iconv(EC_CHARSET, 'UTF8', '銷售人員'))
            ->setCellValue('B2', ecs_iconv(EC_CHARSET, 'UTF8', '訂單編號'))
            ->setCellValue('C2', ecs_iconv(EC_CHARSET, 'UTF8', '訂單日期'))
            ->setCellValue('D2', ecs_iconv(EC_CHARSET, 'UTF8', '訂單金額'))
            ->setCellValue('E2', ecs_iconv(EC_CHARSET, 'UTF8', '訂單成本'))
            ->setCellValue('F2', ecs_iconv(EC_CHARSET, 'UTF8', '訂單盈利'))
            ->setCellValue('G2', ecs_iconv(EC_CHARSET, 'UTF8', '盈利率'));
            $objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }
        $i = 3;
        foreach ($data['data'] AS $key => $value)
        {
            if($_REQUEST['order_type'] == 3){
                $objPHPExcel->getActiveSheet()
                ->setCellValue('A'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['salesperson']))
                ->setCellValueExplicit('B'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['order_sn']), PHPExcel_Cell_DataType::TYPE_STRING)
                ->setCellValue('C'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['order_date']))
                ->setCellValue('D'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['total_amount']))
                ->setCellValue('E'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['sales_agent_price_format']))
                ->setCellValue('F'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['cp_price']))
                ->setCellValue('G'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['profit']))
                ->setCellValue('H'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['sales_agent_price_profit_format']))
                ->setCellValue('I'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['profit_margin']))
                ->setCellValue('J'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['sales_agent_price_margin_format']));
                $i++;
            } else {
                $objPHPExcel->getActiveSheet()
                ->setCellValue('A'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['salesperson']))
                ->setCellValueExplicit('B'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['order_sn']), PHPExcel_Cell_DataType::TYPE_STRING)
                ->setCellValue('C'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['order_date']))
                ->setCellValue('D'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['total_amount']))
                ->setCellValue('E'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['cp_price']))
                ->setCellValue('F'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['profit']))
                ->setCellValue('G'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['profit_margin']));
                $i++;
            }
        }
        
        for ($col = 'A'; $col <= 'J'; $col++)
        {
            $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
        }
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        
        exit;
    }
    
    $data = get_sale_profit_detail();
    
    $smarty->assign('profit_list',     $data['data']);
    $smarty->assign('filter',          $data['filter']);
    $smarty->assign('record_count',    $data['record_count']);
    $smarty->assign('page_count',      $data['page_count']);
    $smarty->assign('order_type',        $_REQUEST['order_type']);
    if($_REQUEST['order_type'] == 3){
        $smarty->assign('display_sa_th', true);
    }
    
    make_json_result($smarty->fetch('sale_profit_detail.htm'), '', array('filter' => $data['filter'], 'page_count' => $data['page_count']));
}
else if (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'query' ||  $_REQUEST['act'] == 'download'))
{
    /* 检查权限 */
    check_authz_json('sale_order_stats');
    if (strstr($_REQUEST['start_date'], '-') === false)
    {
        $_REQUEST['start_date'] = local_date('Y-m-d', $_REQUEST['start_date']);
        $_REQUEST['end_date'] = local_date('Y-m-d', $_REQUEST['end_date']);
    }
    /*------------------------------------------------------ */
    //--Excel文件下载
    /*------------------------------------------------------ */
    if ($_REQUEST['act'] == 'download')
    {
        $file_name = $_REQUEST['start_date'].'_'.$_REQUEST['end_date'] . '_profit';
        $goods_sales_list = get_sale_profit(false);
        header("Content-type: application/vnd.ms-excel; charset=utf-8");
        header("Content-Disposition: attachment; filename=$file_name.xls");
        
        require_once ROOT_PATH . 'includes/excel/PHPExcel.php';
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        
        $objPHPExcel->getActiveSheet()->setTitle('盈利統計');
        
        /* 文件标题 */
        $objPHPExcel->getActiveSheet()
        ->setCellValue('A1', ecs_iconv(EC_CHARSET, 'UTF8', $_REQUEST['start_date']. $_LANG['to'] .$_REQUEST['end_date']. ' 盈利統計'));
        if($_REQUEST['order_type'] == 3){
            $objPHPExcel->getActiveSheet()
            ->setCellValue('A2', ecs_iconv(EC_CHARSET, 'UTF8', '銷售人員'))
            ->setCellValue('B2', ecs_iconv(EC_CHARSET, 'UTF8', '訂單總數'))
            ->setCellValue('C2', ecs_iconv(EC_CHARSET, 'UTF8', '訂單總額'))
            ->setCellValue('D2', ecs_iconv(EC_CHARSET, 'UTF8', '實際訂單總數(扣除手續費)'))
            ->setCellValue('E2', ecs_iconv(EC_CHARSET, 'UTF8', '成本總額'))
            ->setCellValue('F2', ecs_iconv(EC_CHARSET, 'UTF8', '盈利總額'))
            ->setCellValue('G2', ecs_iconv(EC_CHARSET, 'UTF8', '實際盈利總額(扣除手續費)'))
            ->setCellValue('H2', ecs_iconv(EC_CHARSET, 'UTF8', '盈利率'))
            ->setCellValue('I2', ecs_iconv(EC_CHARSET, 'UTF8', '實際盈利率(扣除手續費)'));
            $objPHPExcel->getActiveSheet()->mergeCells('A1:I1');
        } else {
            $objPHPExcel->getActiveSheet()
            ->setCellValue('A2', ecs_iconv(EC_CHARSET, 'UTF8', '銷售人員'))
            ->setCellValue('B2', ecs_iconv(EC_CHARSET, 'UTF8', '訂單總數'))
            ->setCellValue('C2', ecs_iconv(EC_CHARSET, 'UTF8', '訂單總額'))
            ->setCellValue('D2', ecs_iconv(EC_CHARSET, 'UTF8', '成本總額'))
            ->setCellValue('E2', ecs_iconv(EC_CHARSET, 'UTF8', '盈利總額'))
            ->setCellValue('F2', ecs_iconv(EC_CHARSET, 'UTF8', '盈利率'));
            $objPHPExcel->getActiveSheet()->mergeCells('A1:F1');
        }
        
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $i = 3;
        foreach ($goods_sales_list['sale_profit_data'] AS $key => $value)
        {
            if($_REQUEST['order_type'] == 3){
                $objPHPExcel->getActiveSheet()
                ->setCellValue('A'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['salesperson']))
                ->setCellValue('B'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['orders']))
                ->setCellValue('C'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['volume']))
                ->setCellValue('D'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['sales_agent_price_format']))
                ->setCellValue('E'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['cost']))
                ->setCellValue('F'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['profit']))
                ->setCellValue('G'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['sales_agent_price_profit_format']))
                ->setCellValue('H'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['profit_margin']))
                ->setCellValue('I'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['sales_agent_price_margin_format']));
            } else {
                $objPHPExcel->getActiveSheet()
                ->setCellValue('A'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['salesperson']))
                ->setCellValue('B'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['orders']))
                ->setCellValue('C'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['volume']))
                ->setCellValue('D'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['cost']))
                ->setCellValue('E'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['profit']))
                ->setCellValue('F'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['profit_margin']));
            }
            $i++;
        }
        for ($col = 'A'; $col <= 'I'; $col++)
        {
            $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
        }
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        
        exit;
    }
    $sale_profit_data = get_sale_profit();
    $smarty->assign('profit_list',     $sale_profit_data['sale_profit_data']);
    $smarty->assign('filter',          $sale_profit_data['filter']);
    $smarty->assign('record_count',    $sale_profit_data['record_count']);
    $smarty->assign('page_count',      $sale_profit_data['page_count']);
    $smarty->assign('start_date',      $_REQUEST['start_date']);
    $smarty->assign('end_date',        $_REQUEST['end_date']);
    $smarty->assign('order_type',        $_REQUEST['order_type']);
    if($_REQUEST['order_type'] == 3){
        $smarty->assign('display_sa_th', true);
    }
    
    make_json_result($smarty->fetch('sale_profit.htm'), '', array('filter' => $sale_profit_data['filter'], 'page_count' => $sale_profit_data['page_count']));
}
/*------------------------------------------------------ */
//--商品明细列表
/*------------------------------------------------------ */
else
{
    /* 权限判断 */
    admin_priv('sale_order_stats');
    /* 时间参数 */
    if (!isset($_REQUEST['start_date']))
    {
        $start_date = local_strtotime(month_start());
    }
    if (!isset($_REQUEST['end_date']))
    {
        $end_date = local_strtotime(month_end());
    }
    
    $sale_profit_data = get_sale_profit();
    /* 赋值到模板 */
    $smarty->assign('filter',       $sale_profit_data['filter']);
    $smarty->assign('record_count', $sale_profit_data['record_count']);
    $smarty->assign('page_count',   $sale_profit_data['page_count']);
    $smarty->assign('profit_list', $sale_profit_data['sale_profit_data']);
    $smarty->assign('ur_here',          $_LANG['sale_profit']);
    $smarty->assign('full_page',        1);
    $smarty->assign('start_date',       local_date('Y-m-d', $start_date));
    $smarty->assign('end_date',         local_date('Y-m-d', $end_date));
    $smarty->assign('cfg_lang',     $_CFG['lang']);
    $smarty->assign('action_link',  array('text' => '下載盈利統計報表','href'=>'#download'));

    /* 显示页面 */
    assign_query_info();
    $smarty->display('sale_profit.htm');
}

function month_start()
{
    return local_date('Y-m-01');
}

function month_end()
{
    return local_date('Y-m-t');
}

function get_sale_profit($is_pagination = true){

    global $userController,$orderController;
    /* 时间参数 */
    $filter['start_date'] = empty($_REQUEST['start_date']) ? local_strtotime(month_start()) : local_strtotime($_REQUEST['start_date']);
    $filter['end_date'] = empty($_REQUEST['end_date']) ? local_strtotime(month_end()) : local_strtotime($_REQUEST['end_date']);
    $filter['order_type'] = empty($_REQUEST['order_type']) ? 0 : intval($_REQUEST['order_type']);
  
    /* 查询数据的条件 */
    $where = order_query_sql('finished', 'oi.') .
             " AND oi.shipping_time >= '".$filter['start_date']."' AND oi.shipping_time < '" . ($filter['end_date'] + 86400) . "' ";
    if ($filter['order_type'] == 1) // 批發訂單
    {
        // Only include orders for the followiung user account: 99999999
        //$where .= " AND oi.user_id = 5022 ";
        //Business logic: include all wholesale orders even it's unpaid
        $where = str_replace("oi.pay_status  IN ('", "oi.pay_status  IN ('".PS_UNPAYED."','", $where);
        $where .= " AND (oi.user_id IN (".implode(',',$userController->getWholesaleUserIds()).") OR oi.order_type='".$orderController::DB_ORDER_TYPE_WHOLESALE."')";
    }
    else if ($filter['order_type'] == 2) // 借貨訂單
    {
        // Only include orders for the followiung user accounts: 11111111, 88888888
        // DEPRECATED 2017April
        // TODO: from Franz20170410, we are using coupon code instead of static id to identify this type of order. Further study if any changes required
        $where .= " AND oi.user_id IN (4940,5023) ";
    }
    else if ($filter['order_type'] == 3) // 代理銷售訂單
    {
        $where .= " AND oi.order_type = '".\Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT."' ";
    }
    else if ($filter['order_type'] == 5) // Exhibition POS
    {
        // Exclude orders for the followiung user accounts: 11111111, 88888888, 99999999
        $where .= " AND (oi.order_type NOT IN ('".Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT."','".Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE."') OR (oi.order_type IS NULL AND oi.user_id NOT IN (".implode(',',$userController->getWholesaleUserIds()).")) )";
        $where .= " AND (oi.order_type <> '" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT . "' OR oi.order_type IS NULL) ";
        $where .= " AND (oi.platform = 'pos_exhibition') ";
    }
    else // Normal order
    {
        // Exclude orders for the followiung user accounts: 11111111, 88888888, 99999999
        $where .= " AND (oi.order_type NOT IN ('".Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT."','".Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE."') OR (oi.order_type IS NULL AND oi.user_id NOT IN (".implode(',',$userController->getWholesaleUserIds()).") ) ) ";
        $where .= " AND (oi.order_type <> '" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT . "' OR oi.order_type IS NULL) ";
        $where .= " AND (oi.platform <> 'pos_exhibition') ";
    }
    
    $sql = 'SELECT COUNT(*) FROM ( SELECT DISTINCT oi.`salesperson` '.
           'FROM ' . $GLOBALS['ecs']->table('order_info') . ' AS oi '.
           'WHERE 1 ' . $where .
           ') as tmp';
    $filter['record_count'] = $GLOBALS['db']->getOne($sql);

    /* 分页大小 */
    $filter = page_and_size($filter);

    $sql = '('.
            'SELECT *, (volume - cost) as profit, ((volume - cost) / volume) as profit_margin, (sales_agent_price - cost) as sales_agent_price_profit, ((sales_agent_price - cost) / sales_agent_price) as sales_agent_price_margin FROM (' .
                'SELECT oi.`salesperson`, COUNT(oi.`order_id`) as orders, ' .
                'SUM(oi.`money_paid` + oi.`order_amount`) as volume, '. // howang: money_paid(已付金額) + order_amount(應付金額) = 訂單總金額
                'SUM(oi.`cp_price` + ' .
                'IF(oi.`pay_id` = 2,  (oi.`money_paid` + oi.`order_amount`) * 0.0095, 0) + ' . // EPS: 0.95% fee
                'IF(oi.`pay_id` = 7,  (oi.`money_paid` + oi.`order_amount`) * 0.032,  0) + ' . // PayPal: 3.2% fee
                'IF(oi.`pay_id` = 10, (oi.`money_paid` + oi.`order_amount`) * 0.029,  0) + ' . // UnionPay: 2.9% fee
                'IF(oi.`pay_id` = 12, (oi.`money_paid` + oi.`order_amount`) * 0.023,  0) + ' . // Visa / MasterCard: 2.3% fee
                'IF(oi.`pay_id` = 13, (oi.`money_paid` + oi.`order_amount`) * 0.0296, 0) + ' . // American Express: 2.96% fee
                'IF(oi.`shipping_id` = 3, 30, 0)) as cost, ' . // Delivery cost: $30
                'SUM(oi2.sap) as sales_agent_price '.
                'FROM ' . $GLOBALS['ecs']->table('order_info') . ' as oi '.
                'LEFT JOIN ('.
                'SELECT oi.order_id,'.
                "IF(oi.order_type = 'sa', SUM( (( (100 - IF(og.sa_rate_id > 0 , sa.sa_rate, 0) ) /100 ) * og.goods_price)*og.goods_number ),(oi.`money_paid` + oi.`order_amount`) )as sap ".
                "FROM " . $GLOBALS['ecs']->table('order_info') . " as oi ".
                "LEFT JOIN " . $GLOBALS['ecs']->table('order_goods') . " as og ".
                "ON og.order_id = oi.order_id ".
                "LEFT JOIN " . $GLOBALS['ecs']->table('sales_agent_rate') . " as sa ON sa.sa_rate_id = og.sa_rate_id ".
                "GROUP BY oi.order_id) as oi2 ON oi2.order_id = oi.order_id ".
                "WHERE 1 " . $where .
                'GROUP BY oi.`salesperson` '.
                'ORDER BY oi.`salesperson` ASC '.
                ($is_pagination ? 'LIMIT ' . $filter['start'] . ', ' . $filter['page_size'] : '').
            ') as tmp1'.
           ') UNION ('.
           'SELECT *, (volume - cost) as profit, ((volume - cost) / volume) as profit_margin, (sales_agent_price - cost) as sales_agent_price_profit, ((sales_agent_price - cost) / sales_agent_price) as sales_agent_price_margin FROM (' .
                'SELECT \'Total\' as `salesperson`, COUNT(oi.`order_id`) as orders, ' .
                'SUM(oi.`money_paid` + oi.`order_amount`) as volume, ' . // howang: money_paid(已付金額) + order_amount(應付金額) = 訂單總金額
                'SUM(oi.`cp_price` + ' .
                'IF(oi.`pay_id` = 2,  (oi.`money_paid` + oi.`order_amount`) * 0.0095, 0) + ' . // EPS: 0.95% fee
                'IF(oi.`pay_id` = 7,  (oi.`money_paid` + oi.`order_amount`) * 0.032,  0) + ' . // PayPal: 3.2% fee
                'IF(oi.`pay_id` = 10, (oi.`money_paid` + oi.`order_amount`) * 0.029,  0) + ' . // UnionPay: 2.9% fee
                'IF(oi.`pay_id` = 12, (oi.`money_paid` + oi.`order_amount`) * 0.023,  0) + ' . // Visa / MasterCard: 2.3% fee
                'IF(oi.`pay_id` = 13, (oi.`money_paid` + oi.`order_amount`) * 0.0296, 0) + ' . // American Express: 2.96% fee
                'IF(oi.`shipping_id` = 3, 30, 0)) as cost, ' . // Delivery cost: $30
                'SUM(oi2.sap) as sales_agent_price '.
                'FROM ' . $GLOBALS['ecs']->table('order_info') . ' as oi '.
                'LEFT JOIN ('.
                'SELECT oi.order_id,'.
                "IF(oi.order_type = 'sa', SUM( (( (100 - IF(og.sa_rate_id > 0 , sa.sa_rate, 0) ) /100 ) * og.goods_price)*og.goods_number ),(oi.`money_paid` + oi.`order_amount`) )as sap ".
                "FROM " . $GLOBALS['ecs']->table('order_info') . " as oi ".
                "LEFT JOIN " . $GLOBALS['ecs']->table('order_goods') . " as og ".
                "ON og.order_id = oi.order_id ".
                "LEFT JOIN " . $GLOBALS['ecs']->table('sales_agent_rate') . " as sa ON sa.sa_rate_id = og.sa_rate_id ".
                "GROUP BY oi.order_id) as oi2 ON oi2.order_id = oi.order_id ".
                'WHERE 1 ' . $where .
            ') as tmp2'.
           ')';

    $sale_profit_data = $GLOBALS['db']->getAll($sql);

    foreach ($sale_profit_data as $key => $item)
    {
        $sale_profit_data[$key]['cost'] = price_format($sale_profit_data[$key]['cost']);
        $sale_profit_data[$key]['profit'] = price_format($sale_profit_data[$key]['profit']);
        $sale_profit_data[$key]['sales_agent_price_format'] = price_format($sale_profit_data[$key]['sales_agent_price']);
        $sale_profit_data[$key]['sales_agent_price_margin_format'] = number_format($sale_profit_data[$key]['sales_agent_price_margin'] * 100, 2, '.', '') . '%';
        $sale_profit_data[$key]['sales_agent_price_profit_format'] = price_format($sale_profit_data[$key]['sales_agent_price_profit']);
        $sale_profit_data[$key]['volume'] = price_format($sale_profit_data[$key]['volume']);
        $sale_profit_data[$key]['profit_margin'] = number_format($sale_profit_data[$key]['profit_margin'] * 100, 2, '.', '') . '%';
        if ($filter['order_type'] == 3) {
            $sale_profit_data[$key]['display_sa'] = true;
    }
    }
    $arr = array(
        'sale_profit_data' => $sale_profit_data,
        'filter' => $filter,
        'page_count' => $filter['page_count'],
        'record_count' => $filter['record_count']
    );
    return $arr;
}

function get_sale_profit_detail($is_pagination = true)
{
    global $userController,$orderController;
    $filter['start_date'] = empty($_REQUEST['start_date']) ? local_strtotime(month_start()) : local_strtotime($_REQUEST['start_date']);
    $filter['end_date'] = empty($_REQUEST['end_date']) ? local_strtotime(month_end()) : local_strtotime($_REQUEST['end_date']);
    $filter['salesperson'] = empty($_REQUEST['salesperson']) ? '' : $_REQUEST['salesperson'];
    $filter['order_type'] = empty($_REQUEST['order_type']) ? 0 : intval($_REQUEST['order_type']);
    
    $where = order_query_sql('finished', 'oi.') .
            " AND oi.shipping_time >= '".$filter['start_date']."' AND oi.shipping_time < '" . ($filter['end_date'] + 86400) . "' " .
            " AND oi.salesperson = '" . $filter['salesperson'] . "' ";
    if ($filter['order_type'] == 1) // 批發訂單
    {
        // Only include orders for the followiung user account: 99999999
        //Business logic: include all wholesale orders even it's unpaid
        $where = str_replace("oi.pay_status  IN ('", "oi.pay_status  IN ('".PS_UNPAYED."','", $where);
        $where .= " AND (oi.user_id IN (".implode(',',$userController->getWholesaleUserIds()).") OR oi.order_type='".$orderController::DB_ORDER_TYPE_WHOLESALE."')";
    }
    else if ($filter['order_type'] == 2) // 借貨訂單
    {
        // Only include orders for the followiung user accounts: 11111111, 88888888
        // DEPRECATED 2017April
        $where .= " AND oi.user_id IN (4940,5023) ";
    }
    else if ($filter['order_type'] == 3) // 代理銷售訂單
    {
        $where .= " AND oi.order_type = '".\Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT."' ";
    }
    else if ($filter['order_type'] == 5) // Exhibition POS
    {
        // Exclude orders for the followiung user accounts: 11111111, 88888888, 99999999
        $where .= " AND (oi.order_type NOT IN ('".Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT."','".Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE."') OR (oi.order_type IS NULL AND oi.user_id NOT IN (".implode(',',$userController->getWholesaleUserIds()).")) )";
        $where .= " AND (oi.order_type <> '" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT . "' OR oi.order_type IS NULL) ";
        $where .= " AND (oi.platform = 'pos_exhibition') ";
    }
    else // Normal order
    {
        // Exclude orders for the followiung user accounts: 11111111, 88888888, 99999999
        $where .= " AND (oi.order_type NOT IN ('".Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT."','".Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE."') OR (oi.order_type IS NULL AND oi.user_id NOT IN (".implode(',',$userController->getWholesaleUserIds()).")) )";
        $where .= " AND (oi.order_type <> '" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT . "' OR oi.order_type IS NULL) ";
        $where .= " AND (oi.platform <> 'pos_exhibition') ";
    }
    
    $sql = 'SELECT COUNT(*) '.
           'FROM ' . $GLOBALS['ecs']->table('order_info') . ' as oi ' .
           'WHERE 1 ' . $where;
    $filter['record_count'] = $GLOBALS['db']->getOne($sql);
    
    /* 分页大小 */
    $filter = page_and_size($filter);
    
    $sql = 'SELECT *, (total_amount - cost) as profit, ((total_amount - cost) / total_amount) as profit_margin, (sales_agent_price - cost) as sales_agent_price_profit, ((sales_agent_price - cost) / sales_agent_price) as sales_agent_price_margin FROM (' .
               'SELECT oi.`salesperson`, oi.order_id, oi.order_sn, oi.add_time, ' .
               '(oi.`money_paid` + oi.`order_amount`) as total_amount, ' . // howang: money_paid(已付金額) + order_amount(應付金額) = 訂單總金額
               '(oi.`cp_price` + ' .
               'IF(oi.`pay_id` = 2,  (oi.`money_paid` + oi.`order_amount`) * 0.0095,       0) + ' . // EPS: 0.95% fee
               'IF(oi.`pay_id` = 7,  (oi.`money_paid` + oi.`order_amount`) * 0.032 + 2.35, 0) + ' . // PayPal: 3.2% fee + $2.35 per txn
               'IF(oi.`pay_id` = 10, (oi.`money_paid` + oi.`order_amount`) * 0.029,        0) + ' . // UnionPay: 2.9% fee
               'IF(oi.`pay_id` = 12, (oi.`money_paid` + oi.`order_amount`) * 0.023,        0) + ' . // Visa / MasterCard: 2.3% fee
               'IF(oi.`pay_id` = 13, (oi.`money_paid` + oi.`order_amount`) * 0.0296,       0) + ' . // American Express: 2.96% fee
               'IF(oi.`shipping_id` = 3, 30, 0)) as cost, ' . // Delivery cost: $30
               'oi2.sap as sales_agent_price '.
               'FROM ' . $GLOBALS['ecs']->table('order_info') . ' as oi ' .
               'LEFT JOIN ('.
               'SELECT oi.order_id,'.
               "IF(oi.order_type = 'sa', SUM( (( (100 - IF(og.sa_rate_id > 0 , sa.sa_rate, 0) ) /100 ) * og.goods_price)*og.goods_number ),(oi.`money_paid` + oi.`order_amount`) )as sap ".
               "FROM " . $GLOBALS['ecs']->table('order_info') . " as oi ".
               "LEFT JOIN " . $GLOBALS['ecs']->table('order_goods') . " as og ".
               "ON og.order_id = oi.order_id ".
               "LEFT JOIN " . $GLOBALS['ecs']->table('sales_agent_rate') . " as sa ON sa.sa_rate_id = og.sa_rate_id ".
               "GROUP BY oi.order_id) as oi2 ON oi2.order_id = oi.order_id ".
               'WHERE 1 ' . $where .
               'ORDER BY oi.`order_id` DESC '.
               ($is_pagination ? 'LIMIT ' . $filter['start'] . ', ' . $filter['page_size'] : '').
           ') as tmp';
    
    if ($is_pagination)
    {
        $sql .= " LIMIT " . $filter['start'] . ', ' . $filter['page_size'];
    }
    
    $data = $GLOBALS['db']->getAll($sql);
    
    foreach ($data as $key => $item)
    {
        $data[$key]['order_date'] = local_date('Y-m-d H:i:s',$data[$key]['add_time']);
        $data[$key]['total_amount'] = price_format($data[$key]['total_amount']);
        $data[$key]['cp_price'] = price_format($data[$key]['cost']);
        $data[$key]['sales_agent_price_format'] = price_format($data[$key]['sales_agent_price']);
        $data[$key]['sales_agent_price_margin_format'] = number_format($data[$key]['sales_agent_price_margin'] * 100, 2, '.', '') . '%';
        $data[$key]['sales_agent_price_profit_format'] = price_format($data[$key]['sales_agent_price_profit']);
        if ($_REQUEST['order_type'] == 3) {
            $data[$key]['display_sa'] = true;
        }
        $data[$key]['profit'] = price_format($data[$key]['profit']);
        $data[$key]['profit_margin'] = number_format($data[$key]['profit_margin'] * 100, 2, '.', '') . '%';
    }
    $arr = array(
        'data' => $data,
        'filter' => $filter,
        'page_count' => $filter['page_count'],
        'record_count' => $filter['record_count']
    );
    return $arr;
}

?>
