<?php

/**
 * ECSHOP 订单管理
 * ============================================================================
 * 版权所有 2005-2010 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: yehuaixiao $
 * $Id: order.php 17157 2010-05-13 06:02:31Z yehuaixiao $
 */

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require_once(ROOT_PATH . 'includes/lib_i18n.php');
require_once(ROOT_PATH . 'includes/lib_order.php');
$userController = new Yoho\cms\Controller\UserController();
$adminuserController = new Yoho\cms\Controller\AdminuserController();
$commissionController = new Yoho\cms\Controller\CommissionController();


/*------------------------------------------------------ */
//-- 產品佣金設置頁面
/*------------------------------------------------------ */

if ($_REQUEST['act'] == 'set')
{
    /* 权限检查 */
    admin_priv('product_commission_setting');
    $cats = cat_list();
    $smarty->assign('cats', $cats);
    $brands = get_brand_list();
    $smarty->assign('brands', $brands);
    $form_action = 'insert';
    $smarty->assign('form_action', $form_action);
    $smarty->assign('action_link', array('href' => 'commission.php?act=report&tab=1', 'text' => '產品佣金及銷售情況'));
    assign_query_info();
    $smarty->display('goods_commission.htm');
}
/*------------------------------------------------------ */
//-- 產品佣金列表
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'list')
{
    /* 权限检查 */
    admin_priv('product_commission_select');
    if (check_authz('product_commission_update')) {
        $smarty->assign('priv_product_commission_update', 1);
    }
    $smarty->assign('ur_here', '產品佣金');
    $cats = cat_list();
    $smarty->assign('cats', $cats);
    $brands = get_brand_list();
    $smarty->assign('brands', $brands);
    $goods_commission_list = $commissionController->goods_commission_list();
    $smarty->assign('commission_list',$goods_commission_list['goods_commission']);
    $smarty->assign('record_count',$goods_commission_list['record_count']);
    $smarty->assign('action_link', array('href' => 'commission.php?act=report&tab=1', 'text' => '產品佣金及銷售情況'));
    assign_query_info();
    $smarty->display('goods_commission_list.htm');
}
/*------------------------------------------------------ */
//-- 產品佣金report
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'report')
{
    if (check_authz('commission_statistics')) {
        $smarty->assign('priv_commission_statistics', 1);
    }
    if (check_authz('estimated_sales_manage')) {
        $smarty->assign('priv_estimated_sales_manage', 1);
    }
    /* 模板赋值 */
    $smarty->assign('ur_here', $_LANG['product_commission']);
    $smarty->assign('tab',        1);
    /* 时间参数 */

    if (!isset($_REQUEST['end_date']))
    {
        $end_date = month_end();
    }else {
        $end_date = $_REQUEST['end_date'];
    }
    if($_REQUEST['tab'] == '2'){
        admin_priv('commission_statistics');
        if (!isset($_REQUEST['start_date']))
        {
            $start_date = month_start();
        }
        $smarty->assign('selected_status', 2);
        $smarty->assign('action_link',  array('text' => '下載報表','href'=>'#download'));
        $smarty->assign('action_link2',  array('text' => '佣金修正','href'=>'sale_commission.php?act=adjust&have_discount=1'));
        $sale_commission_data = $commissionController->get_sale_commission(true,$start_date,$end_date);
        $smarty->assign('filter',       $sale_commission_data['filter']);
        $smarty->assign('record_count', $sale_commission_data['record_count']);
        $smarty->assign('page_count',   $sale_commission_data['page_count']);
        $smarty->assign('commission_list', $sale_commission_data['sale_commission_data']);
    } else {
        if (!isset($_REQUEST['start_date']))
        {
            $start_date = last_month_start();
        }else {
            $start_date = $_REQUEST['start_date'];
        }
        if (isset($_REQUEST['time']))
        {  
            $smarty->assign('selected_time', $_REQUEST['time']);
            $selected_date =local_date('Y-m-t',local_strtotime($_REQUEST['time']."-01"));
            $important_goods_list = $commissionController->getImportantGoodsList($selected_date);
            if (isset($_REQUEST['date_range']))
            {
                $smarty->assign('date_range_str', $_REQUEST['date_range']);
                $smarty->assign('date_range', explode(",",$_REQUEST['date_range']));
            }
        } else {
            $important_goods_list = $commissionController->getImportantGoodsList($end_date);
        }
        $important_goods = $important_goods_list['important_goods'];
        $salesperson = $important_goods_list['cursalesperson'];
        $smarty->assign('total_salesperson', count($salesperson));
        $smarty->assign('salesperson', $salesperson);
        $smarty->assign('selected_status', 1);
        if (check_authz('product_commission_setting')) {
            $smarty->assign('action_link', array('href' => 'commission.php?act=set', 'text' => '佣金設置'));
        }
        if (check_authz('product_commission_select')) {
            $smarty->assign('action_link2', array('href' => 'commission.php?act=list', 'text' => '產品佣金'));
        }
        $smarty->assign('tab_start_date',       local_date('Y-m', local_strtotime($start_date)));
        $smarty->assign('tab_end_date',         local_date('Y-m', local_strtotime($end_date)));
        $smarty->assign('important_goods',$important_goods);
        $smarty->assign('record_count',$important_goods_list['record_count']);
    }
    $smarty->assign('start_date',        $start_date);
    $smarty->assign('end_date',         $end_date);
    assign_query_info();
    $smarty->display('goods_commission_report.htm');
}
/*------------------------------------------------------ */
//-- 門市佣金計算
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'shop_commission')
{
    if (check_authz('shop_target_manage')) {
        $smarty->assign('priv_shop_target_manage', 1);
    }
    /* 模板赋值 */
    $smarty->assign('ur_here', $_LANG['shop_commission']);
    /* 时间参数 */
    $smarty->assign('full_page',        1);
    if (!isset($_REQUEST['end_date']))
    {
        $end_date = month_end();
    }else {
        $end_date = $_REQUEST['end_date'];
    }

    if (!isset($_REQUEST['start_date']))
    {
        $start_date = last_month_start();
    }else {
        $start_date = $_REQUEST['start_date'];
    }
    if (isset($_REQUEST['time']))
    {  
        $smarty->assign('selected_time', $_REQUEST['time']);
        $selected_date =local_date('Y-m-t',local_strtotime($_REQUEST['time']."-01"));
        $shop_commission_list = $commissionController->getShopCommissionList($selected_date);
        if (isset($_REQUEST['date_range']))
        {
            $smarty->assign('date_range_str', $_REQUEST['date_range']);
            $smarty->assign('date_range', explode(",",$_REQUEST['date_range']));
        }
    } else {
        $shop_commission_list = $commissionController->getShopCommissionList($end_date);
    }
    $salesperson = $commissionController->getSalesperson();
    $smarty->assign('total_salesperson', count($salesperson));
    $smarty->assign('shop_target', $shop_commission_list['shop_target']);
    $smarty->assign('shop_target_time', $shop_commission_list['shop_target_time']);
    $smarty->assign('meet_target', $shop_commission_list['meet_target']);
    $smarty->assign('manager_grade_sales', $shop_commission_list['manager_grade_sales']);
    $smarty->assign('sales_data', $shop_commission_list['sales']);
    $smarty->assign('wholesale_data', $shop_commission_list['wholesale']);
    $smarty->assign('pickup_data', $shop_commission_list['pickup']);
    $smarty->assign('esd', $shop_commission_list['esd']);
    $smarty->assign('online_except_pickup', $shop_commission_list['online_except_pickup']);
    $smarty->assign('other_sa', $shop_commission_list['other_sa']);
    $smarty->assign('total_sales_volumn', $shop_commission_list['total_sales_volumn']);
    $smarty->assign('total_volumn', $shop_commission_list['total_volumn']);
    $smarty->assign('total_commission', $shop_commission_list['total_commission']);
    $smarty->assign('total_weight_commission', $shop_commission_list['total_weight_commission']);
    $smarty->assign('tab_start_date',       local_date('Y-m', local_strtotime($start_date)));
    $smarty->assign('tab_end_date',         local_date('Y-m', local_strtotime($end_date)));
    $smarty->assign('start_date',       $start_date);
    $smarty->assign('end_date',         $end_date);
    assign_query_info();
    $smarty->display('shop_commission_report.htm');
}
elseif ($_REQUEST['act'] == 'update_shop_target') 
{
    $shop_target_time = $_REQUEST['shop_target_time'];
    $shop_target = $_REQUEST['shop_target'];
    $res = $commissionController->getShopTarget($shop_target_time);
    if($res) {
        $sql = "UPDATE " . $ecs->table('shop_target') . " SET `target_volumn` = '" . $shop_target ."' WHERE `time` like '" . $shop_target_time . "' ";
    } else {
        $sql = "INSERT INTO " . $ecs->table('shop_target') . " (`id`, `time`, `target_volumn`) VALUES (NULL, '" . $shop_target_time ."', '" . $shop_target . "')";
    } 
    $db -> query($sql);
    // make_json_result($res);
}
/*------------------------------------------------------ */
//-- 門市薪資計算
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'shop_salary')
{
    /* 模板赋值 */
    $smarty->assign('ur_here', $_LANG['shop_salary']);
    /* 时间参数 */
    $smarty->assign('full_page',        1);
    if (!isset($_REQUEST['end_date']))
    {
        $end_date = month_end();
    }else {
        $end_date = $_REQUEST['end_date'];
    }

    if (!isset($_REQUEST['start_date']))
    {
        $start_date = last_month_start();
    }else {
        $start_date = $_REQUEST['start_date'];
    }
    if (isset($_REQUEST['time']))
    {  
        $smarty->assign('selected_time', $_REQUEST['time']);
        $selected_date =local_date('Y-m-t',local_strtotime($_REQUEST['time']."-01"));
        $shop_salary_list = $commissionController->getShopSalaryList($selected_date);
        if (isset($_REQUEST['date_range']))
        {
            $smarty->assign('date_range_str', $_REQUEST['date_range']);
            $smarty->assign('date_range', explode(",",$_REQUEST['date_range']));
        }
    } else {
        $shop_salary_list = $commissionController->getShopSalaryList($end_date);
    }
    $smarty->assign('sales_salary', $shop_salary_list);
    $smarty->assign('tab_start_date',       local_date('Y-m', local_strtotime($start_date)));
    $smarty->assign('tab_end_date',         local_date('Y-m', local_strtotime($end_date)));
    $smarty->assign('start_date',       $start_date);
    $smarty->assign('end_date',         $end_date);
    assign_query_info();
    $smarty->display('shop_salary_report.htm');
}
/*------------------------------------------------------ */
//-- 篩選
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'query')
{
    //重點產品tab
    if($_REQUEST['tab'] == '1') {
        if (isset($_REQUEST['end_date'])) {
            $end_date = $_REQUEST['end_date'];
        }
        if (isset($_REQUEST['time'])) {
            $end_date = local_date('Y-m-t',local_strtotime($_REQUEST['time']."-01"));
        }
        if (!isset($_REQUEST['end_date']) && !isset($_REQUEST['time'])) {
            $end_date = local_date('Y-m-t',gmtime());
        }
        $important_goods_list = $commissionController->getImportantGoodsList($end_date);
        $important_goods = $important_goods_list['important_goods'];
        $salesperson = $important_goods_list['cursalesperson'];
        $commissionController->ajaxQueryAction($important_goods, $important_goods_list['record_count'], false, $salesperson);
    } elseif($_REQUEST['is_ajax'] == 1) {
        //門市佣金計算
        if($_REQUEST['obj'] == 'shop_commission'){
            $end_date = local_date('Y-m-t',local_strtotime($_REQUEST['end_date']));
            $start_date = local_date('Y-m-01',local_strtotime($_REQUEST['start_date']));
            $shop_commission_list = $commissionController->getShopCommissionList($end_date);
            $salesperson = $commissionController->getSalesperson();
            $smarty->assign('total_salesperson', count($salesperson));
            $smarty->assign('shop_target', $shop_commission_list['shop_target']);
            $smarty->assign('shop_target_time', $shop_commission_list['shop_target_time']);
            $smarty->assign('meet_target', $shop_commission_list['meet_target']);
            $smarty->assign('manager_grade_sales', $shop_commission_list['manager_grade_sales']);
            $smarty->assign('sales_data', $shop_commission_list['sales']);
            $smarty->assign('wholesale_data', $shop_commission_list['wholesale']);
            $smarty->assign('pickup_data', $shop_commission_list['pickup']);
            $smarty->assign('esd', $shop_commission_list['esd']);
            $smarty->assign('online_except_pickup', $shop_commission_list['online_except_pickup']);
            $smarty->assign('other_sa', $shop_commission_list['other_sa']);
            $smarty->assign('total_volumn', $shop_commission_list['total_volumn']);
            $smarty->assign('total_commission', $shop_commission_list['total_commission']);
            $smarty->assign('total_weight_commission', $shop_commission_list['total_weight_commission']);
            $smarty->assign('tab_start_date',       local_date('Y-m', $start_date));
            $smarty->assign('tab_end_date',         local_date('Y-m', $end_date));
            $smarty->assign('start_date',      $start_date);
            $smarty->assign('end_date',        $end_date);
            make_json_result($smarty->fetch('shop_commission_report.htm'));
            
        }
         elseif($_REQUEST['obj'] == 'sales_commission'){   
            $start_date = $_REQUEST['start_date'];
            $end_date = $_REQUEST['end_date'];
            $sale_commission_data = $commissionController->get_sale_commission(true,$start_date,$end_date);
            $smarty->assign('commission_list', $sale_commission_data['sale_commission_data']);
            $smarty->assign('filter',          $sale_commission_data['filter']);
            $smarty->assign('record_count',    $sale_commission_data['record_count']);
            $smarty->assign('page_count',      $sale_commission_data['page_count']);
            $smarty->assign('start_date',      $start_date);
            $smarty->assign('end_date',        $end_date);
            make_json_result($smarty->fetch('sale_commission.htm'), '', array('filter' => $sale_commission_data['filter'], 'page_count' => $sale_commission_data['page_count']));
        }
        elseif($_REQUEST['obj'] == 'shop_salary'){   
            $end_date = local_date('Y-m-t',local_strtotime($_REQUEST['end_date']));
            $start_date = local_date('Y-m-01',local_strtotime($_REQUEST['start_date']));
            $shop_salary_list = $commissionController->getShopSalaryList($end_date);
            $smarty->assign('sales_salary', $shop_salary_list);
            $smarty->assign('tab_start_date',       local_date('Y-m', $start_date));
            $smarty->assign('tab_end_date',         local_date('Y-m', $end_date));
            $smarty->assign('start_date',      $start_date);
            $smarty->assign('end_date',        $end_date);
            make_json_result($smarty->fetch('shop_salary_report.htm'));
        }
    }else {
        //產品佣金
        $goods_commission_list = $commissionController->goods_commission_list();
        $commissionController->ajaxQueryAction($goods_commission_list['goods_commission'], $goods_commission_list['record_count'], false);
    }
    
}
elseif ($_REQUEST['act'] == 'ajaxEdit')
{
    /* 检查权限 */
    // check_authz_json('order_edit');
    if($_REQUEST['col'] == 'commission') {
        $commission = empty($_POST['value']) ? 0 : $_POST['value'];
        $goods_id = empty($_POST['id']) ? 0 : intval($_POST['id']);

        if ($goods_id == 0)
        {
            make_json_error('NO GOODS ID');
            exit;
        }

        $sql = "UPDATE " . $GLOBALS['ecs']->table('goods') . " SET commission = '".$commission."' WHERE goods_id = '".$goods_id."'";
        if ($GLOBALS['db']->query($sql))
        {
            make_json_result($commission);
        }
        else
        {
            make_json_error($GLOBALS['db']->errorMsg());
        }
    } elseif($_REQUEST['col'] == 'estimated_sales') {
        $estimated_sales = empty($_POST['value']) ? 0 : $_POST['value'];
        $goods_id = empty($_POST['id']) ? 0 : intval($_POST['id']);

        if ($goods_id == 0)
        {
            make_json_error('NO GOODS ID');
            exit;
        }
        $sql = "UPDATE " . $GLOBALS['ecs']->table('important_goods') . " SET estimated_sales = '".$estimated_sales."' WHERE goods_id = '".$goods_id."'";
        if ($GLOBALS['db']->query($sql))
        {
            make_json_result($estimated_sales);
        }
        else
        {
            make_json_error($GLOBALS['db']->errorMsg());
        }
    }
    else {
        $commissionController->ajaxEditAction();
    }
    
}

/*------------------------------------------------------ */
//-- 產品佣金添加
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'insert' )
{
    $arr_good_id = array();
    $arr_goods = array();
    $arr_duplicate_fd_id = array();
    foreach ($_REQUEST['goods-id'] as $key => $val) {
        if (!in_array($val, $arr_good_id)){
            //get estimated sales of goods
            $sales = $commissionController->getEstimatedSales($val);
            $arr_goods[$val] = array(
                    'goods_id' => $val,
                    'is_important' => $_REQUEST['is-important'][$key],
                    'time' => $_REQUEST['time'][$key],
                    'estimated_sales' => $sales
            );
            array_push($arr_good_id, $val);
            $sql = "UPDATE " . $GLOBALS['ecs']->table('goods') . " SET commission = '".$_REQUEST['commission-rate'][$key]."' WHERE goods_id = '".$val."'";
            $GLOBALS['db']->query($sql);
            $db->autoExecute($ecs->table('important_goods'), $arr_goods[$val], 'INSERT');
            
        }
    }
    $link[0]['text'] = '產品佣金及銷售情況';
    $link[0]['href'] = 'commission.php?act=report&tab=1';
    sys_msg(($_REQUEST['act'] == 'update' ? '編輯' : '新增') . '產品佣金成功', 0, $link, true);
}
elseif ($_REQUEST['act'] == 'ajax_get_goods_by_keyword' || $_REQUEST['act'] == 'ajax_get_goods_by_cat')
{
    $arr_sql_where = array();

    if (isset($_REQUEST['cat_id']) && $_REQUEST['cat_id'] > -1){
        $arr_cats = cat_list($_REQUEST['cat_id'], $_REQUEST['cat_id'], FALSE);
        $arr_cats_id = array();
        if (sizeof($arr_cats) > 0){
            foreach($arr_cats as $cat){
                array_push($arr_cats_id, $cat['cat_id']);
            }
        }
        $str_cats_id = implode(",", $arr_cats_id);
        array_push($arr_sql_where, 'g.cat_id IN ('.$str_cats_id.')');
    }
    if (isset($_REQUEST['brand_id']) && $_REQUEST['brand_id'] > -1){
        array_push($arr_sql_where, 'g.brand_id = '.$_REQUEST['brand_id']);
    }

    if (sizeof($arr_sql_where) == 0){
        $arr_sql_where = FALSE;
    }

    if (!$arr_sql_where && $_REQUEST['act'] == 'ajax_get_goods_by_cat'){
        return 'false';
    }

    $search_key = mysql_real_escape_string(trim($_REQUEST['search_key']));

    if ($search_key == '' && $_REQUEST['act'] != 'ajax_get_goods_by_cat'){
        return 'false';
    }

    $goods = $commissionController->getGoodsBykeyword($search_key, $arr_sql_where);

    return make_json_result($goods);
    
}
elseif ($_REQUEST['act'] == 'ajax_goods_commission_exist')
{
    $_REQUEST['goods_id'] = empty($_REQUEST['goods_id']) ? "" : intval($_REQUEST['goods_id']);
    $_REQUEST['time'] = empty($_REQUEST['time']) ? '' : trim($_REQUEST['time']);
    $sql = "SELECT g.goods_name FROM ".$ecs->table('important_goods')." AS gc LEFT JOIN ".$ecs->table('goods')." AS g ON g.goods_id = gc.goods_id WHERE g.goods_id = ".$_REQUEST['goods_id']." AND gc.`time` LIKE '%".$_REQUEST['time']."%'";
    $res   = $db->getOne($sql); 
    return make_json_result($res);
}
/*------------------------------------------------------ */
//-- 轉成Excel
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'download')
{
    // ini_set('memory_limit', '512M');
    // ini_set('max_execution_time', 180);
    $obj = $_REQUEST['obj'];
    $start_date = $_REQUEST['start_date'];
    $end_date = $_REQUEST['end_date'];
    if ($obj=="importantGoods") {
        
        $file_name = $_REQUEST['start_date'].'_'.$_REQUEST['end_date'] . '_重點產品';
        header("Content-type: application/vnd.ms-excel; charset=utf-8");
        header("Content-Disposition: attachment; filename=$file_name.xls");
        require_once ROOT_PATH . 'includes/excel/PHPExcel.php';
        $objPHPExcel = new PHPExcel();
        $month_range = $commissionController->getMonthRange($start_date,$end_date)["month_range"];
        $sheetIndex = 0;
        foreach ($month_range AS $mkey => $mvalue) {
            $sheet_month = $mvalue['month'];
            $sheet_end_date = local_date('Y-m-t',local_strtotime($sheet_month."-01"));

            $important_goods_list = $commissionController->getImportantGoodsList($sheet_end_date,false);
            $important_goods = $important_goods_list['important_goods'];
            if (count($important_goods) != 0) {
                $salesperson = $important_goods_list['cursalesperson'];
                $total_sales = count($salesperson);
                $objPHPExcel->createSheet();
                $objPHPExcel->setActiveSheetIndex($sheetIndex);
                /* sheet标题 */
                $objPHPExcel->getActiveSheet()->setTitle($sheet_month.'重點產品');
                if ($total_sales != 0) {
                    $objPHPExcel->getActiveSheet()
                    ->setCellValue('F1', ecs_iconv(EC_CHARSET, 'UTF8', '實際銷量'));
                }

                $objPHPExcel->getActiveSheet()
                ->setCellValue('A2', ecs_iconv(EC_CHARSET, 'UTF8', '產品ID'))
                ->setCellValue('B2', ecs_iconv(EC_CHARSET, 'UTF8', '產品名稱'))
                ->setCellValue('C2', ecs_iconv(EC_CHARSET, 'UTF8', '預計銷量'))
                ->setCellValue('D2', ecs_iconv(EC_CHARSET, 'UTF8', '每件佣金'))
                ->setCellValue('E2', ecs_iconv(EC_CHARSET, 'UTF8', '預計總佣金'));
                $slaesperson_cell = array('F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T');
                $largest_col = $slaesperson_cell[$total_sales-1];
                $objPHPExcel->getActiveSheet()->mergeCells('A1:E1');
                if ($total_sales > 1) {
                    $objPHPExcel->getActiveSheet()->mergeCells('F1:'.$largest_col.'1');
                }
                foreach ($salesperson AS $skey => $svalue) {
                    $col_no = $slaesperson_cell[$skey];
                    $objPHPExcel->getActiveSheet()
                    ->setCellValue($col_no.'2', ecs_iconv(EC_CHARSET, 'UTF8', $svalue));
                }
                $i = 3;
                foreach ($important_goods AS $key => $value)
                {
                    $objPHPExcel->getActiveSheet()
                    ->setCellValue('A'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['goods_id']))
                    ->setCellValue('B'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['goods_name']))
                    ->setCellValue('C'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['estimated_sales']))
                    ->setCellValue('D'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['commission']))
                    ->setCellValue('E'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['estimated_total_commission']));
                    foreach ($salesperson AS $skey => $svalue) {
                        $col_no = $slaesperson_cell[$skey];
                        $objPHPExcel->getActiveSheet()
                        ->setCellValue($col_no.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value[$svalue]));
                    }
                    $objPHPExcel->getActiveSheet()->getStyle('D'.$i)->getNumberFormat()->setFormatCode('$0.00');
                    $objPHPExcel->getActiveSheet()->getStyle('E'.$i)->getNumberFormat()->setFormatCode('$0.00');
                    $i++;
                }
                for ($col = 'A'; $col <= $largest_col; $col++)
                {
                    $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
                }
    
    
    
                $sheetIndex ++;
            }
        }

        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        
        exit;
    } 
    elseif ($obj=="shopSalary") {
            $file_name = $_REQUEST['start_date'].'_'.$_REQUEST['end_date'] . '_門市薪資';       
            header("Content-type: application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$file_name.xls");
            require_once ROOT_PATH . 'includes/excel/PHPExcel.php';
            $objPHPExcel = new PHPExcel(); 
            $month_range = $commissionController->getMonthRange($start_date,$end_date)["month_range"];
            $sheetIndex = 0;
            foreach ($month_range AS $mkey => $mvalue) {
                $sheet_month = $mvalue['month'];
                $sheet_end_date = local_date('Y-m-t',local_strtotime($sheet_month."-01"));
    
                $shop_salary_list = $commissionController->getShopSalaryList($sheet_end_date,false);  
                if(count($shop_salary_list)!=0) {
                    $objPHPExcel->createSheet();
                    $objPHPExcel->setActiveSheetIndex($sheetIndex);
                    
                    $objPHPExcel->getActiveSheet()->setTitle($sheet_month.'門市薪資');
                    
                    /* 文件标题 */
                    $objPHPExcel->getActiveSheet()
                    ->setCellValue('A1', ecs_iconv(EC_CHARSET, 'UTF8', 'Sales Executives'))
                    ->setCellValue('B1', ecs_iconv(EC_CHARSET, 'UTF8', '角色'))
                    ->setCellValue('C1', ecs_iconv(EC_CHARSET, 'UTF8', '入職日期'))
                    ->setCellValue('D1', ecs_iconv(EC_CHARSET, 'UTF8', '年資(月)'))
                    ->setCellValue('E1', ecs_iconv(EC_CHARSET, 'UTF8', '職位分數'))
                    ->setCellValue('F1', ecs_iconv(EC_CHARSET, 'UTF8', '年資分數'))
                    ->setCellValue('G1', ecs_iconv(EC_CHARSET, 'UTF8', '總分數'))
                    ->setCellValue('H1', ecs_iconv(EC_CHARSET, 'UTF8', '公佣'))
                    ->setCellValue('I1', ecs_iconv(EC_CHARSET, 'UTF8', '私佣'))
                    ->setCellValue('J1', ecs_iconv(EC_CHARSET, 'UTF8', '評分獎'))
                    ->setCellValue('K1', ecs_iconv(EC_CHARSET, 'UTF8', '重點產品獎'));
                    $i = 2;
                    foreach ($shop_salary_list AS $key => $value)
                    {
                        $objPHPExcel->getActiveSheet()
                        ->setCellValue('A'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['salesperson']))
                        ->setCellValue('B'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['position']))
                        ->setCellValue('C'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['joined_date']))
                        ->setCellValue('D'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['month_points']))
                        ->setCellValue('E'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['position_points']))
                        ->setCellValue('F'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['month_points']))
                        ->setCellValue('G'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['private_pontis']))
                        ->setCellValue('H'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['public_commission']))
                        ->setCellValue('I'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['private_commission']))
                        ->setCellValue('J'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['review_award']))
                        ->setCellValue('K'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['important_goods_award']));
                        $objPHPExcel->getActiveSheet()->getStyle('H'.$i)->getNumberFormat()->setFormatCode('$0.00');
                        $objPHPExcel->getActiveSheet()->getStyle('I'.$i)->getNumberFormat()->setFormatCode('$0.00');
                        $objPHPExcel->getActiveSheet()->getStyle('J'.$i)->getNumberFormat()->setFormatCode('$0.00');
                        $objPHPExcel->getActiveSheet()->getStyle('K'.$i)->getNumberFormat()->setFormatCode('$0.00');
                        $i++;
                    }
                    for ($col = 'A'; $col <= 'K'; $col++)
                    {
                        $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
                    }
                    $sheetIndex ++;
                }
            }
    
            
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            
            exit;
        
    }
}




function month_start()
{
    return local_date('Y-m-01');
}

function last_month_start()
{
    return date('Y-m-01', strtotime('first day of last month'));
}

function month_end()
{
    return local_date('Y-m-t');
}

?>
