<?php

define('IN_ECS', true);

require dirname(__FILE__) . '/includes/init.php';

if ($_REQUEST['act'] == 'list') {
    $crmController = new Yoho\cms\Controller\CrmController();
    $smarty->assign("ivr_language", $_CFG['ivr_language']);
    $smarty->assign("ivr_language_enabled", $_CFG['ivr_language_enabled']);
    $smarty->assign("ivr_language_decoded", json_decode($_CFG['ivr_language'], true));
    $smarty->assign("available_languages", $crmController::IVR_LANG_NAME);
    $smarty->display("ivr.htm");
} elseif ($_REQUEST['act'] == 'update') {
    $ivr_language = trim($_REQUEST['ivr_language']);
    $ivr_language_enabled = trim($_REQUEST['ivr_language_enabled']);
    if (empty($ivr_language)) {
        sys_msg("沒有輸入語言");
    }
    $db->query("UPDATE " . $ecs->table("shop_config") . " SET value = '$ivr_language' WHERE code = 'ivr_language'");
    $db->query("UPDATE " . $ecs->table("shop_config") . " SET value = '$ivr_language_enabled' WHERE code = 'ivr_language_enabled'");
    clear_cache_files();
    $_CFG = load_config();
    sys_msg("已更新設定", 0, [['text' => $GLOBALS['_LANG']['go_back'], 'href' => 'ivr.php?act=list']]);
} elseif ($_REQUEST['act'] == 'upload') {
    $ivr_language_decoded = json_decode($_CFG['ivr_language'], true);
    $target = explode("_", trim($_REQUEST['name']));
    $lang = array_shift($target);
    $ext = explode(".", $_FILES['file']['name'])[1];

    $path = ROOT_PATH . 'ajax/voice_recording/';
    if (!file_exists($path)) {
        mkdir($path, 0777, true);
    }

    $filename = "$_REQUEST[name].$ext";
    $new_ivr_language = $ivr_language_decoded;
    if (move_upload_file($_FILES['file']['tmp_name'], $path . $filename)) {
        $new_ivr_language = updateIVRLanguage($ivr_language_decoded, $lang, $target, $filename);
    }
    $db->query("UPDATE " . $ecs->table("shop_config") . " SET value = '" . json_encode($new_ivr_language, JSON_UNESCAPED_UNICODE) . "' WHERE code = 'ivr_language'");
    clear_cache_files();
    $_CFG = load_config();
    header("location: ivr.php?act=list");
} elseif ($_REQUEST['act'] == 'remove') {
    $ivr_language_decoded = json_decode($_CFG['ivr_language'], true);
    $filename = trim($_REQUEST['name']);
    $target = explode("_", explode(".", $filename)[0]);
    $lang = array_shift($target);

    $path = ROOT_PATH . 'ajax/voice_recording/';
    if (file_exists($path . $filename)) {
        unlink($path . $filename);
    }

    make_json_response(1);
}

function updateIVRLanguage($config, $lang, $target, $value)
{
    $new_config = $config;
    if (count($target) == 1) {
        $new_config[$target[0]][$lang] = $value;
        return $new_config;
    } else {
        $new_config[$target[0]] = updateIVRLanguage($config[array_shift($target)], $lang, $target, $value);
    }
    return $new_config;
}
?>