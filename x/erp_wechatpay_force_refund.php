<?php

define('IN_ECS', true);
require(dirname(__FILE__) .'/includes/init.php');

$orderController = new Yoho\cms\Controller\OrderController(); 

if($_SESSION['is_accountant'] != 1 && $_SESSION['role_id'] != 4 && $_SESSION['manage_cost'] != 1)
{
    sys_msg($GLOBALS['_LANG']['priv_error']);
};

if ($_GET['act'] == 'search' || empty($_REQUEST['act']))
{
    $smarty->assign('ur_here', $_LANG['wechatpay_force_refund']);
    $smarty->display('wechatpay_force_refund.htm');
}
elseif($_POST['act'] == 'query')
{
    if (empty($_POST['transaction_sn'])) {
        header('Location: erp_wechatpay_force_refund.php?act=search');
        exit;
    }

    $order_id = $orderController->getOrderIdBySn($_POST['transaction_sn']);
    
    if(isset($order_id) && !empty($order_id)) {
        header('Location: qfwechatpay_force_refund.php?act=add&order_id='.$order_id);
        exit;
    }
    else
    {
        header('Location: erp_wechatpay_force_refund.php?act=search');
        exit;
    }
}

?>
