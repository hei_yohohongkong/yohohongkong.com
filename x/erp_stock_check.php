<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_base.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_warehouse.php');
require(dirname(__FILE__) .'/includes/ERP/cls/cls_date.php');

if($_REQUEST['act'] == 'list'||$_REQUEST['act'] == 'print'||$_REQUEST['act'] == 'export')
{
	if(admin_priv('erp_warehouse_view','',false) ||admin_priv('erp_warehouse_manage','',false) ||admin_priv('erp_warehouse_approve','',false))
	{
		include('./includes/ERP/page.class.php');
		$cls_date=new cls_date();
		$num_per_page=30;
		$mode=1;
		$page_bar_num=6;
		$page_style="page_style";
		$current_page_style="current_page_style";
		$page=isset($_REQUEST['page'])?($_REQUEST['page']):1;
		$start=$num_per_page*($page-1);
		$current_date=local_date('Y-m-d',gmtime());
		$first_date=$cls_date->get_first_day($current_date);
		$last_date=$cls_date->get_last_day($current_date);
		$start_date=isset($_REQUEST['s_date'])?($_REQUEST['s_date']):$first_date;
		$end_date=isset($_REQUEST['e_date'])?($_REQUEST['e_date']):$last_date;
		$warehouse_id=isset($_REQUEST['warehouse_id'])?intval($_REQUEST['warehouse_id']):0;
		$goods_sn=isset($_REQUEST['goods_sn'])?trim($_REQUEST['goods_sn']):'';
		$goods_name=isset($_REQUEST['goods_name'])?trim($_REQUEST['goods_name']):'';
		$goods_barcode=isset($_REQUEST['goods_barcode'])?trim($_REQUEST['goods_barcode']):'';
		$supplier_id=isset($_REQUEST['s_id']) &&intval($_REQUEST['s_id']) >0 ?intval($_REQUEST['s_id']):0;
		$brand_id=isset($_REQUEST['b_id']) &&intval($_REQUEST['b_id']) >0 ?intval($_REQUEST['b_id']):0;
		if(!empty($start_date))
		{
			$start_time=$cls_date->date_to_stamp($start_date);
		}
		if(!empty($end_date))
		{
			$end_time=$cls_date->date_to_stamp($end_date);
		}
		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		$paras=array(
			'warehouse_id'=>$warehouse_id,
			'goods_sn'=>$goods_sn,
			'goods_name'=>$goods_name,
			'goods_barcode'=>$goods_barcode,
			'agency_id'=>$agency_id,
			'supplier_id'=>$supplier_id,
			'brand_id'=>$brand_id
		);
		if($page==1)
		{
			update_tb_stock_check($paras,$start_time,$end_time);
		}
		$total_num=get_stock_check_count($paras);
		if($_REQUEST['act'] == 'list'||$_REQUEST['act'] == 'print')
		{
			$stock_check_info=get_stock_check_list($paras,$start,$num_per_page);
		}
		else
		{
			$stock_check_info=get_stock_check_list($paras);
		}
		$url=fix_url($_SERVER['REQUEST_URI'], array(
			'page'=>'',
			's_date'=>$start_date,
			'e_date'=>$end_date,
			'warehouse_id'=>$warehouse_id,
			'goods_sn'=>$goods_sn,
			'goods_name'=>$goods_name,
			'supplier_id'=>$supplier_id,
			'brand_id'=>$brand_id
		));
		$pager=new page(array('total_data'=>$total_num,'data_per_page'=>$num_per_page,'url'=>$url,'mode'=>$mode,'page_bar_num'=>$page_bar_num,'page_style'=>$page_style,'current_page_style'=>$current_page_style));
		$smarty->assign('start_date',$start_date);
		$smarty->assign('end_date',$end_date);
		$smarty->assign('warehouse_id',$warehouse_id);
		$smarty->assign('goods_sn',$goods_sn);
		$smarty->assign('goods_name',$goods_name);
		$smarty->assign('goods_barcode',$goods_barcode);
		$smarty->assign('page',$page);
		$smarty->assign('supplier_id',$supplier_id);
		$smarty->assign('brand_id',$brand_id);
		$smarty->assign('pager',$pager->show());
		$smarty->assign('stock_check_info',$stock_check_info);
		$warehouse_list=get_warehouse_list(0,$agency_id,1);
		$smarty->assign('warehouse_list',$warehouse_list);
		$smarty->assign('supplier_list',get_agency_suppliers($agency_id));
		$smarty->assign('brand_list',brand_list());
		$smarty->assign('ur_here',$_LANG['erp_stock_check']);
		if($_REQUEST['act'] == 'list'||$_REQUEST['act'] == 'print')
		{
			$template=$_REQUEST['act'] == 'list'?'erp_stock_check.htm': 'erp_stock_check_print.htm';
			assign_query_info();
			$smarty->display($template);
		}
		else
		{
			require(dirname(__FILE__) .'/includes/ERP/cls/cls_csv.php');
			require(dirname(__FILE__) .'/includes/ERP/cls/cls_file.php');
			include(ROOT_PATH.'/includes/cls_json.php');
			$export=array();
			$row['check_id']=$_LANG['erp_stock_check_id'];
			$row['goods_sn']=$_LANG['erp_stock_check_goods_sn'];
			$row['goods_name']=$_LANG['erp_stock_check_goods_name'];
			$row['goods_barcode']=$_LANG['erp_stock_check_goods_barcode'];
			$row['warehouse_name']=$_LANG['erp_stock_check_warehouse_name'];
			$row['agency_name']=$_LANG['erp_stock_check_agency'];
			$row['goods_attr']=$_LANG['erp_stock_check_goods_attr'];
			$row['begin_qty']=$_LANG['erp_stock_check_begin_qty'];
			$row['warehousing_qty']=$_LANG['erp_stock_check_warehousing_qty'];
			$row['delivery_qty']=$_LANG['erp_stock_check_delivery_qty'];
			$row['end_qty']=$_LANG['erp_stock_check_end_qty'];
			$export[]=$row;
			if(!empty($stock_check_info))
			{
				foreach($stock_check_info as $k=>$v)
				{
					$row['check_id']=$v['id'];
					$row['goods_sn']=$v['goods_info']['goods_sn'];
					$row['goods_name']=$v['goods_info']['goods_name'];
					$row['goods_barcode']=$v['barcode'];
					$row['warehouse_name']=$v['warehouse_info']['name'];
					$row['agency_name']=$v['agency_name'];
					$goods_attr_id=$v['attr_id'];
					if(!empty($goods_attr_id))
					{
						$goods_attr=get_attr_info($goods_attr_id);
						$attr='';
						if(isset($goods_attr['attr_info']) &&is_array($goods_attr['attr_info']))
						{
							foreach($goods_attr['attr_info'] as $item)
							{
								$attr.=$item['attr_value'].' ';
							}
						}
						$row['goods_attr']=empty($attr)?'$$': $attr;
					}
					else
					{
						$row['goods_attr']='$$';
					}
					$row['begin_qty']=$v['begin_qty'];
					$row['warehousing_qty']=$v['warehousing_qty'];
					$row['delivery_qty']=$v['delivery_qty'];
					$row['end_qty']=$v['end_qty'];
					$export[]=$row;
				}
			}
			$csv=new cls_csv();
			$file_stream=$csv->encode($export);
			$file_stream=iconv('utf-8','gb2312',$file_stream);
			$file=new cls_file();
			$path=ROOT_PATH.'data/stock_check/';
			$random_name=random_string('numeric',8);
			$file_name=date('Y-m-d').'-'.$random_name.'.csv';
			while(file_exists($path.$file_name))
			{
				$random_name=random_string('numeric',8);
				$file_name=date('Y-m-d').'-'.$random_name.'.csv';
			}
			$file->write_file($path,$file_name,$file_stream,$op='rewrite');
			$json  = new JSON;
			$server_address=$_SERVER["SERVER_NAME"];
			$phpself=$_SERVER["PHP_SELF"];
			$port=$_SERVER["SERVER_PORT"];
			$path=str_replace(ADMIN_PATH.'/erp_stock_check.php','',$phpself);
			$base_url=$server_address.':'.$port.$path;
			$url='http://'.$base_url.'data/stock_check/'.$file_name;
			$result['error']=0;
			$result['url']=$url;
			die($json->encode($result));
		}
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
?>