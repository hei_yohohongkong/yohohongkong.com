<?php

/***
* warranty_templates.php
* by howang 2015-10-30
*
* Manage templates of warranty info
***/

define('IN_ECS', true);
require(dirname(__FILE__) . '/includes/init.php');
$warrantyTemplatesController = new Yoho\cms\Controller\WarrantyTemplatesController();

if ($_REQUEST['act'] == 'list')
{
    $smarty->assign('ur_here',      $_LANG['warranty_templates']);
    $smarty->assign('action_link', array('text' => '新增保養條款模版', 'href' => 'javascript:add_warranty_template();'));

    assign_query_info();
    $smarty->display('warranty_templates_list.htm');
}
elseif ($_REQUEST['act'] == 'query')
{
    $warrantyTemplatesController->ajaxQueryAction();
}
elseif ($_REQUEST['act'] == 'add_warranty_template')
{
    $template_name = isset($_POST['template_name']) ? trim($_POST['template_name']) : '';
    
    if (empty($template_name))
    {
        make_json_error('請輸入保養條款模版名稱');
    }
    
    $data = array(
        'template_name' => $template_name,
        'template_content' => ''
    );
    $db->autoExecute($ecs->table('warranty_templates'), $data, 'INSERT');
    
    make_json_result('新增保養條款模版成功');
}
elseif ($_REQUEST['act'] == 'ajaxEdit')
{
    $warrantyTemplatesController->ajaxEditAction();
}
elseif ($_REQUEST['act'] == 'remove')
{
    $template_id = intval($_REQUEST['id']);
    
    $sql = "DELETE FROM " . $ecs->table('warranty_templates') .
            " WHERE `template_id` = '" . $template_id . "' ";
    $db->query($sql);
    $sql = "DELETE FROM " . $ecs->table('warranty_templates_lang') .
            " WHERE `template_id` = '" . $template_id . "' ";
    $db->query($sql);
    // Reset linked goods to "custom warranty"
    $sql = "UPDATE " . $ecs->table('goods') . " SET `warranty_template_id` = 0 " .
            " WHERE `warranty_template_id` = '" . $template_id . "' ";
    $db->query($sql);
    
    $url = 'warranty_templates.php?act=query&' . str_replace('act=' . $_REQUEST['act'], '', $_SERVER['QUERY_STRING']);
    
    header('Location: ' . $url);
    exit;
}
elseif ($_REQUEST['act'] == 'get_warranty_template')
{
    $template_id = intval($_REQUEST['id']);
    
    $sql = "SELECT * " .
            "FROM " . $ecs->table('warranty_templates') .
            "WHERE `template_id` = '" . $template_id . "' ";
    $data = $db->getRow($sql);
    
    $localized_versions = get_localized_versions('warranty_templates', $template_id);
    
    header('Content-Type: application/json;charset=utf-8');
    echo json_encode(array(
        'data' => $data,
        'localized_versions' => $localized_versions
    ));
    exit;
}
elseif ($_REQUEST['act'] == 'edit')
{
    $warrantyTemplatesController->editAction();
}
elseif ($_REQUEST['act'] == 'update')
{
    $warrantyTemplatesController->updateAction();
}
?>