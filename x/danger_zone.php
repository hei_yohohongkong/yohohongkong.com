<?php

/***
* danger_zone.php
* by howang 2015-03-19
*
* Dangerous admin operations - for super admin only
***/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

if (($_REQUEST['act'] == 'main') || (empty($_REQUEST['act'])))
{
    /* 权限判断 */
    if (!check_authz('danger_zone_base') && !check_authz('danger_zone_edit') && !check_authz('danger_zone_admin'))
    {
        sys_msg($GLOBALS['_LANG']['priv_error']);
    }

    $smarty->assign('ur_here',      'DangerZone™');
    $smarty->assign('zone_2', check_authz('danger_zone_edit'));
    $smarty->assign('zone_3', check_authz('danger_zone_admin'));
    assign_query_info();
    $smarty->display('danger_zone.htm');
}
elseif ($_REQUEST['act'] == 'edit_user_name')
{
    if (!admin_priv('danger_zone_edit'))
    {
        make_json_error($GLOBALS['_LANG']['priv_error']);
    }
    
    $old_name = empty($_POST['old_name']) ? '' : trim($_POST['old_name']);
    $new_name = empty($_POST['new_name']) ? '' : trim($_POST['new_name']);
    
    $sql = "SELECT user_id FROM " . $ecs->table('users') . " WHERE `user_name` = '" . $old_name . "'";
    $user_id = $db->getOne($sql);
    
    if (empty($user_id))
    {
        make_json_error('找不到 ' . stripslashes($old_name));
    }
    
    $sql = "SELECT user_id FROM " . $ecs->table('users') . " WHERE `user_name` = '" . $new_name . "'";
    $res = $db->getOne($sql);
    
    if (!empty($res))
    {
        make_json_error(stripslashes($new_name) . ' 已被使用');
    }
    
    $sql = "UPDATE " . $ecs->table('users') .
            "SET `user_name` = '" . $new_name . "' " .
            "WHERE `user_id` = '" . $user_id . "' " .
            "LIMIT 1";
    $res = $db->query($sql, 'SILENT');
    
    if ($res)
    {
        $sql = "UPDATE " . $ecs->table('order_info') .
                "SET `tel` = '" . $new_name . "' " .
                "WHERE `user_id` = '" . $user_id . "' " .
                "AND `tel` = '" . $old_name . "'";
        $db->query($sql);
        
        $sql = "UPDATE " . $ecs->table('order_info') .
                "SET `mobile` = '" . $new_name . "' " .
                "WHERE `user_id` = '" . $user_id . "' " .
                "AND `mobile` = '" . $old_name . "'";
        $db->query($sql);
        
        clear_cache_files();
        make_json_result('', '修改成功');
    }
    else
    {
        make_json_error('修改失敗');
    }
}
elseif ($_REQUEST['act'] == 'recalc_goods_sales')
{
    if (!admin_priv('danger_zone_admin'))
    {
        sys_msg($GLOBALS['_LANG']['priv_error']);
    }
    
    $sql = "UPDATE " . $ecs->table('goods') . " as g " .
            "SET `sales` = (" .
                "SELECT SUM(goods_number) AS shownum " .
                "FROM " . $ecs->table('order_goods') . " as og " .
                "LEFT JOIN " . $ecs->table('order_info') . " as oi ON oi.order_id = og.order_id " .
                "WHERE oi.order_status IN ('1','5') AND oi.shipping_status IN ('1','2') AND oi.pay_status IN ('2','1') " .
                "AND og.goods_id = g.goods_id" .
            ")";
    $res = $db->query($sql, 'SILENT');
    
    if ($res)
    {
        clear_cache_files();
        $msg = '重新計算成功，更新了 ' . $db->affected_rows() . ' 個產品';
        sys_msg($msg, 0, array(array('text' => '返回 DangerZone™', 'href' => 'danger_zone.php?act=main')), false);
    }
    else
    {
        sys_msg('重新計算失敗');
    }
}
elseif ($_REQUEST['act'] == 'flush_pagespeed_cache')
{
    if (!admin_priv('danger_zone_admin'))
    {
        sys_msg($GLOBALS['_LANG']['priv_error']);
    }
    
    $result = shell_exec('exec curl \'http://127.0.0.1/pagespeed_admin/cache?purge=*\'');
    
    sys_msg($result, 0, array(array('text' => '返回 DangerZone™', 'href' => 'danger_zone.php?act=main')), false);
}
elseif ($_REQUEST['act'] == 'send_verify_email')
{    
    $user_name = empty($_POST['user_name']) ? '' : trim($_POST['user_name']);
    
    $sql = "SELECT user_id, email FROM " . $ecs->table('users') . " WHERE `user_name` = '" . $user_name . "'";
    $res = $db->getRow($sql);
    
    if (empty($res))
    {
        make_json_error('找不到 ' . stripslashes($user_name));
    }
    
    $user_id = $res['user_id'];
    $email = $res['email'];
    
    // howang: don't send if user is using default system-generated email address
    if (empty($email) || is_placeholder_email($email))
    {
        make_json_error('該會員沒有填寫電郵地址');
    }
    
    // check bounce list
    if (!can_send_mail($email, true))
    {
        make_json_error('此會員的電郵地址有電郵被打回頭的紀錄，該電郵地址是錯誤或虛假的');
    }
    
    require_once ROOT_PATH . 'includes/lib_passport.php';
    $ok = send_regiter_hash($user_id);
    
    if ($ok)
    {
        make_json_result('', '發送成功');
    }
    else
    {
        make_json_error('發送失敗');
    }
}
elseif ($_REQUEST['act'] == 'send_order_receipt')
{
    $order_sn = empty($_POST['order_sn']) ? '' : trim($_POST['order_sn']);
    
    if (empty($order_sn))
    {
        make_json_error('請輸入訂單號！');
    }
    
    require_once ROOT_PATH . 'includes/lib_order.php';
    $order = order_info(0, $order_sn);
    
    if (empty($order))
    {
        make_json_error('找不到符合訂單號 ' . stripslashes($order_sn) . ' 的訂單');
    }
    
    if (empty($order['email']))
    {
        make_json_error('該訂單沒有電郵地址');
    }
    
    // howang: don't send if user is using default system-generated email address
    if (is_placeholder_email($order['email']))
    {
        make_json_error('該訂單的會員沒有填寫電郵地址');
    }
    
    // check bounce list
    if (!can_send_mail($order['email'], true))
    {
        make_json_error('此訂單的電郵地址有電郵被打回頭的紀錄，該電郵地址是錯誤或虛假的');
    }
    
    require_once ROOT_PATH . 'core/lib_payload.php';
    $api_url = $ecs->url() . 'api/howang/email_receipts.php';
    $obj = array('act' => 'send_receipt', 'order_id' => $order['order_id']);
    $payload = base64_encode(encode_payload($obj));
    //echo 'curl -d "payload=' . rawurlencode($payload) . '" ' . $api_url . ' > /dev/null 2>&1 &' . "\n";
    exec('curl -d "payload=' . rawurlencode($payload) . '" ' . $api_url . ' > /dev/null 2>&1 &');
    
    make_json_result('', '正在發送訂單 ' . $order['order_sn'] . ' 的收據到 ' . $order['email']);
}
elseif ($_REQUEST['act'] == 'edit_order_email')
{
    if (!admin_priv('danger_zone_edit'))
    {
        make_json_error($GLOBALS['_LANG']['priv_error']);
    }
    
    $order_sn = empty($_POST['order_sn']) ? '' : trim($_POST['order_sn']);
    
    if (empty($order_sn))
    {
        make_json_error('請輸入訂單號！');
    }
    
    $email = empty($_POST['email']) ? '' : trim($_POST['email']);
    
    if (empty($email))
    {
        make_json_error('請輸入電郵地址！');
    }
    
    require_once ROOT_PATH . 'includes/lib_order.php';
    $order = order_info(0, $order_sn);
    
    if (empty($order))
    {
        make_json_error('找不到符合訂單號 ' . stripslashes($order_sn) . ' 的訂單');
    }
    
    if (!empty($order['email']) && ($order['email'] == $email))
    {
        make_json_result('', '該訂單的電郵地址已經是 ' . $email);
    }
    
    $sql = "UPDATE " . $ecs->table('order_info') . " SET email = '" . $email . "' WHERE order_id = '" . $order['order_id'] . "' LIMIT 1";
    $db->query($sql);
    
    make_json_result('', '已更新訂單 ' . $order['order_sn'] . ' 的電郵地址為 ' . $email);
}
elseif ($_REQUEST['act'] == 'reset_order_send_number')
{
    if (!$_SESSION['manage_cost'])
    {
        make_json_error($GLOBALS['_LANG']['priv_error']);
    }
    
    $order_sn = empty($_POST['order_sn']) ? '' : trim($_POST['order_sn']);
    
    if (empty($order_sn))
    {
        make_json_error('請輸入訂單號！');
    }
    
    require_once ROOT_PATH . 'includes/lib_order.php';
    $order = order_info(0, $order_sn);
    
    if (empty($order))
    {
        make_json_error('找不到符合訂單號 ' . stripslashes($order_sn) . ' 的訂單');
    }
    
    $sql = "UPDATE " . $ecs->table('order_goods') . " SET `send_number` = '0' WHERE `order_id` = '" . $order['order_id'] . "'";
    $db->query($sql);
    
    make_json_result('', '已將訂單 ' . $order['order_sn'] . ' 中的所有產品出貨數量歸零');
}
elseif ($_REQUEST['act'] == 'merge_users')
{
    if (!admin_priv('danger_zone_edit'))
    {
        make_json_error($GLOBALS['_LANG']['priv_error']);
    }
    
    $from_name = empty($_POST['from_name']) ? '' : trim($_POST['from_name']);
    $to_name = empty($_POST['to_name']) ? '' : trim($_POST['to_name']);
    
    $sql = "SELECT user_id FROM " . $ecs->table('users') . " WHERE `user_name` = '" . $from_name . "'";
    $from_user_id = $db->getOne($sql);
    
    if (empty($from_user_id))
    {
        make_json_error('找不到 ' . stripslashes($from_name));
    }
    
    $sql = "SELECT user_id FROM " . $ecs->table('users') . " WHERE `user_name` = '" . $to_name . "'";
    $to_user_id = $db->getOne($sql);
    
    if (empty($to_user_id))
    {
        make_json_error('找不到 ' . stripslashes($to_name));
    }
    
    // Merge 訂單
    $sql = "UPDATE " . $ecs->table('order_info') .
            "SET `user_id` = '" . $to_user_id . "' " .
            "WHERE `user_id` = '" . $from_user_id . "' ";
    $res = $db->query($sql);
    
    // Merge 積分紀錄
    $sql = "UPDATE " . $ecs->table('account_log') .
            "SET `user_id` = '" . $to_user_id . "' " .
            "WHERE `user_id` = '" . $from_user_id . "' ";
    $res = $db->query($sql);
    
    // Merge 積分
    $sql = "SELECT rank_points, pay_points FROM " . $ecs->table('users') . " WHERE `user_id` = '" . $from_user_id . "'";
    $res = $db->getRow($sql);
    $sql = "UPDATE " . $ecs->table('users') .
            "SET `rank_points` = `rank_points` + '" . (empty($res['rank_points']) ? 0 : intval($res['rank_points'])) . "', " .
                "`pay_points` = `pay_points` + '" . (empty($res['pay_points']) ? 0 : intval($res['pay_points'])) . "' " .
            "WHERE `user_id` = '" . $to_user_id . "' ";
    $res = $db->query($sql);
    
    // Remove from user
    $users =& init_users();
    $users->remove_user($from_name); //已经删除用户所有数据
    
    clear_cache_files();
    make_json_result('', '合併會員成功');
}
elseif ($_REQUEST['act'] == 'resend_delivery_email')
{
    if (!admin_priv('danger_zone_base'))
    {
        make_json_error($GLOBALS['_LANG']['priv_error']);
    }

    $resend_order_sn = $_REQUEST['order_sn'];
    
    // Find delivery order
    require_once ROOT_PATH . 'includes/lib_order.php';
    $order = order_info(0, $resend_order_sn);
    $sql = "SELECT order_sn,delivery_id FROM ".$ecs->table('delivery_order')." WHERE order_sn = '$resend_order_sn' ORDER BY update_time desc";
    $order_list = $db->getRow($sql);
    
    if (!isset($order) || empty($order) 
    || !isset($order_list['delivery_id']) || empty($order_list['delivery_id'])
    || !isset($order_list['order_sn']) || empty($order_list['order_sn'])
    || ($order_list['order_sn'] != $resend_order_sn)
    )
    {
        make_json_error('找不到訂單 ' . stripslashes($resend_order_sn) . ' 相關寄貨紀錄');
    }

    $deliveryOrderController = new Yoho\cms\Controller\DeliveryOrderController();
    $ok = $deliveryOrderController->delivery_send_mail($order, $order_list['delivery_id'], true);

    make_json_result('', '重發訂單 ' . $resend_order_sn . '寄貨通知成功');
    
}
elseif ($_REQUEST['act'] == 'force_unsubscribe_edm')
{
    
    if(!isset($_REQUEST['email']) || !is_email($_REQUEST['email'])) {
        make_json_error('必須輸入有效電郵');
    }

    $email = $_REQUEST['email'];

    // Local test API route and key
    $base_url = $_SERVER['SERVER_NAME']. "/includes/sendy/";
    $api_key = $_CFG['sendy_api_key'];

    if(strpos($_SERVER['SERVER_NAME'], 'beta.yohohongkong.com') === false){
        $base_url = "https://email.yohohongkong.com/s/";
    }

    // Run cURL for modified sendy api
    $force_unsubscribe_url = $base_url . 'api/subscribers/force-unsubscribe-all.php';

    $curl_client = curl_init();
    $data = http_build_query([
        "email" => $email,
        "api_key" => $api_key
    ]);

    curl_setopt($curl_client, CURLOPT_URL, $force_unsubscribe_url);
    curl_setopt($curl_client, CURLOPT_POST, true);
    curl_setopt($curl_client, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl_client, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl_client, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));


    $result = curl_exec($curl_client);
    curl_close($curl_client);

    $decoded_result = json_decode($result);
    if ($decoded_result->error != 0) {
        make_json_error($email . " 強制取消訂閱電子報失敗: \n".$decoded_result->message);
    } elseif ($decoded_result->error == 0) {
        make_json_result('', $email . " 強制取消訂閱電子報成功");
    }
}

?>