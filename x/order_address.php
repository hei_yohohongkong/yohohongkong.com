<?php
define('IN_ECS', true);
require(dirname(__FILE__) . '/includes/init.php');
require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/admin/statistic.php');

$smarty->assign('lang', $_LANG);
$userController = new Yoho\cms\Controller\UserController();
$addressController    = new Yoho\cms\Controller\AddressController();

if(isset($_REQUEST['act'])){
    if($_REQUEST['act'] == 'query'){
        $page_size = empty($_REQUEST['page_size']) ? 50 : intval($_REQUEST['page_size']);
        $page = empty($_REQUEST['page']) ? 1 : intval($_REQUEST['page']);
        $data_start = empty($_REQUEST['start']) ? 0 : intval($_REQUEST['start']);
        $sort_by = empty($_REQUEST['sort_by']) ? 'order_address_id' : $_REQUEST['sort_by'];
        if ($sort_by == 'formatted_staying_time'){
            $sort_by = 'staying_time';
        }
        $sort_order = empty($_REQUEST['sort_order']) ? 'DESC' : $_REQUEST['sort_order'];
        if(isset($_REQUEST['tab'])){
            $addr_list_all = $addressController->get_full_list($_REQUEST['tab'],$page_size,$page);
            $addressController->address_paging($addr_list_all,$page_size,$page);
            $smarty->assign('tab', $_REQUEST['tab']);            
            assign_query_info();
            $smarty->display('order_address_report_detail.htm');
        } else if (isset($_REQUEST['data-type'])) {
            $smarty->assign('full_page', 0);
            $smarty->assign('ur_here', $_LANG['order_address_list']);
            $smarty->assign('list', $_REQUEST['list']);
            $order = $sort_by." ".$sort_order;
            $offset = $data_start;

            $weekday = date('w');
            if (isset($_REQUEST['start_date'])){
                $start_date = $_REQUEST['start_date'];
            } else if ($weekday == 1){
                $start_date = local_date('Y-m-d', local_strtotime('-2 days'));
            } else {
                $start_date = local_date('Y-m-d', local_strtotime('-1 days'));
            }
            if (isset($_REQUEST['end_date'])){
                $end_date = $_REQUEST['end_date'];
            } else {
                $end_date = local_date('Y-m-d', local_strtotime('today'));
            }
            $smarty->assign('start_date', $start_date);
            $smarty->assign('end_date', $end_date);

            if (isset($_REQUEST['non_verify'])){
                $non_verify = $_REQUEST['non_verify'];
            } else {
                $non_verify = 'false';
            }

            if (isset($_REQUEST['verify'])){
                $verify = $_REQUEST['verify'];
            } else {
                $verify = 'false';
            }

            if (isset($_REQUEST['type']) && $_REQUEST['type'] == 'pickup'){
                $non_pickup = 'false';
            } else {
                if (isset($_REQUEST['non_pickup'])){
                    $non_pickup = $_REQUEST['non_pickup'];
                } else {
                    $non_pickup = 'true';
                }
            }

            if ((isset($_REQUEST['type']) && $_REQUEST['type'] == 'pickup') || isset($_REQUEST['pickup_only'])){
                $pickup_only = true;
            } else {
                $pickup_only = 'false';
            }

            if (isset($_REQUEST['no_city'])){
                $no_city = $_REQUEST['no_city'];
            } else {
                $no_city = 'false';
            }

            if (isset($_REQUEST['unknown_city'])){
                $unknown_city = $_REQUEST['unknown_city'];
            } else {
                $unknown_city = 'false';
            }

            if (isset($_REQUEST['search_type_cat'])){
                $search_type_cat = $_REQUEST['search_type_cat'];
            } else {
                $search_type_cat = false;
            }

            if (isset($_REQUEST['keyword_cat']) && (isset($_REQUEST['keyword_text_1']) || isset($_REQUEST['keyword_text_2']))){
                $search_keyword_cat = $_REQUEST['keyword_cat'];
                if (isset($_REQUEST['keyword_text_1']) && isset($_REQUEST['keyword_text_2']) && trim($_REQUEST['keyword_text_1']) !="" && trim($_REQUEST['keyword_text_2']) !=""){
                    $search_keyword_1 = trim($_REQUEST['keyword_text_1']);
                    $search_keyword_2 = trim($_REQUEST['keyword_text_2']);
                    $keyword_operator = $_REQUEST['keyword_operator'];
                } else if (isset($_REQUEST['keyword_text_1']) && $_REQUEST['keyword_text_1'] !="" ) {
                    $search_keyword_1 = trim($_REQUEST['keyword_text_1']);
                } else {
                    $search_keyword_2 = trim($_REQUEST['keyword_text_2']);
                }
            } else {
                $search_keyword_cat = false;
            }

            $arr_where = array();
            array_push($arr_where, "oa.address IS NOT NULL", "oa.address != 'NULL'", "oa.address !=''", "oa.country = 3409", "oa.verify >= 0");
            if (!empty($start_date)){
                array_push($arr_where, "oa.create_at > '" . $start_date . " 00:00:00'");
            }
            if (!empty($end_date)){
                array_push($arr_where, "oa.create_at <= '" . $end_date . " 23:59:59'");
            }
            if ($non_verify == 'true'){
                array_push($arr_where, "oa.verify = 0");
            } else if ($verify == 'true'){
                array_push($arr_where, "oa.verify = 1");
            }
            if ($non_pickup == 'true'){
                array_push($arr_where, "oa.area_type != 2 AND oa.address_type != 'locker'");
            }
            if ($pickup_only == 'true'){
                array_push($arr_where, "oa.address_type = 'locker'");
            }
            if ($no_city == 'true'){
                $not_empty = ['oa.sign_building','oa.street_name','oa.street_num','oa.google_place','oa.administrative_area','oa.floor','oa.block','oa.phase','oa.room'];
                $not_empty = array_map(function($v){return $v." = ''";}, $not_empty);
                //array_push($arr_where,"oa.country = 3409", "oa.hk_area = 0", "oa.locality = ''", "NOT(".implode(" AND ",$not_empty).")");
                array_push($arr_where,"oa.hk_area = 0", "oa.locality = ''", "NOT(".implode(" AND ",$not_empty).")");
            }
            if ($unknown_city == 'true'){
                //array_push($arr_where, "oa.country = 3409", "oa.hk_area = 0", "oa.locality != ''");
                array_push($arr_where, "oa.hk_area = 0", "oa.locality != ''");
            }
            if (!empty($search_type_cat) && $search_type_cat > 0){
                array_push($arr_where, "oadt.address_dest_type_cat  = '".$search_type_cat."'");
            }
            if (!empty($search_keyword_cat) && $search_keyword_cat != -1){
                if (isset($keyword_operator) && !empty($keyword_operator) && isset($search_keyword_1) && !empty($search_keyword_1) && isset($search_keyword_2) && !empty($search_keyword_2)){
                    array_push($arr_where, "(oa.".$search_keyword_cat." LIKE '%".$search_keyword_1."%' ".$keyword_operator." oa.".$search_keyword_cat." LIKE '%".$search_keyword_2."%')");
                } else if (isset($search_keyword_1) && $search_keyword_1 !=""){
                    array_push($arr_where, "oa.".$search_keyword_cat." LIKE '%".$search_keyword_1."%'");
                } else if (isset($search_keyword_2) && $search_keyword_2 !=""){
                    array_push($arr_where, "oa.".$search_keyword_cat." LIKE '%".$search_keyword_2."%'");
                }
            }
            if ($_REQUEST['data-type'] == 'list'){
                
                $addr_list_count = $addressController->count_address($arr_where);
                $addr_list_all = $addressController->get_address($arr_where, $order, $page_size, $offset);

                if (isset($_REQUEST['cat']) && $_REQUEST['cat'] == 'time'){
                    foreach($addr_list_all as $i => $addr){
                        $addr_list_all[$i]['formatted_staying_time'] = $addressController->format_staying_time($addr['staying_time']);
                    }
                }

                $addr_type = $addressController->get_address_types();
                $lift_type = array(1, 2, 3);
                $total_stay_time = 0;
                
                if (!isset($_REQUEST['cat'])){
                    foreach ($addr_list_all as $i => $addr){
                        $sel_type_col = "<select class='sel_type' data-source='dest_type' data-key='".$addr['order_address_id']."' onchange='sel_change(this)'>";
                        $sel_type_col .= "<option value='0'";
                        if ($addr['dest_type'] == '0'){
                            $sel_type_col .= " selected";
                        }
                        $sel_type_col .= ">未選擇</option>";
                        foreach ($addr_type as $type){
                            $sel_type_col .= "<option value='".$type['type_id']."'";
                            if ($addr['dest_type'] == $type['type_id']){
                                $sel_type_col .= " selected";
                            }
                            $sel_type_col .= ">".$type['type_name']."</option>";
                        }
                        $addr_list_all[$i]['type_select'] = $sel_type_col;
                        
                        $sel_lift_col = "<select class='sel_lift' data-source='lift' data-key='".$addr['order_address_id']."' onchange='sel_change(this)'>";
                        $sel_lift_col .= "<option value='0'";
                        if ($addr['lift'] == '0'){
                            $sel_lift_col .= " selected";
                        }
                        $sel_lift_col .= ">未選擇</option>";
                        foreach ($lift_type as $type){
                            $sel_lift_col .= "<option value='".$type."'";
                            if ($addr['lift'] == $type){
                                $sel_lift_col .= " selected";
                            }
                            $sel_lift_col .= ">".$_LANG['address_type_lift_type_'.$type]."</option>";
                        }
                        $addr_list_all[$i]['lift_select'] = $sel_lift_col;
                    }
                } else if ($_REQUEST['cat'] == 'time'){
                    foreach ($addr_list_all as $i => $addr){
                        $addr_list_all[$i]['lift_name'] = $_LANG['address_type_lift_type_'.$addr['lift']];
                        $total_stay_time += $addr['staying_time'];
                    }
                    $avg_stay_time = ($total_stay_time / sizeof($addr_list_all));
                    $formatted_stay_time = $addressController->format_staying_time($avg_stay_time);
                    array_unshift($addr_list_all, array(
                            "formatted_staying_time"=>"<span style='font-weight: bold;'>平均".$formatted_stay_time."</span>",
                            "order_address_id"=>"-",
                            "address"=>"-",
                            "administrative_area"=>"-",
                            "locality"=>"-",
                            "street_name"=>"-",
                            "street_num"=>"-",
                            "phase"=>"-",
                            "sign_building"=>"-",
                            "block"=>"-",
                            "floor"=>"-",
                            "room"=>"-",
                            "zipcode"=>"-",
                            "address_dest_type_name"=>"-",
                            "lift_name"=>"-"
                    ));
                    $smarty->assign('formatted_stay_time', $formatted_stay_time);
                }
                $smarty->assign('data', $addr_list_all);
                $addressController->ajaxQueryAction($addr_list_all, $addr_list_count, false);
                //assign_query_info();
            } else if ($_REQUEST['data-type'] == 'count_list' && $_REQUEST['cat'] == 'building'){
                $sort_by = empty($_REQUEST['sort_by']) ? 'count' : $_REQUEST['sort_by'];
                $sort_order = empty($_REQUEST['sort_order']) ? 'DESC' : $_REQUEST['sort_order'];
                $order = $sort_by." ".$sort_order;

                $addr_list_count = $addressController->count_building_no($arr_where);

                $addr_list_all = $addressController->count_building($arr_where, $order, $page_size, $offset);

                $smarty->assign('data', $addr_list_all);
                $addressController->ajaxQueryAction($addr_list_all, $addr_list_count, false);
            } else if ($_REQUEST['data-type'] == 'count_list' && $_REQUEST['cat'] == 'google_place'){
                $sort_by = empty($_REQUEST['sort_by']) ? 'count' : $_REQUEST['sort_by'];
                $sort_order = empty($_REQUEST['sort_order']) ? 'DESC' : $_REQUEST['sort_order'];
                $order = $sort_by." ".$sort_order;

                $addr_list_count = $addressController->count_google_place_no($arr_where);

                $addr_list_all = $addressController->count_google_place($arr_where, $order, $page_size, $offset);

                $smarty->assign('data', $addr_list_all);
                $addressController->ajaxQueryAction($addr_list_all, $addr_list_count, false);
            }
        }
    }else if($_REQUEST['act'] == 'list'){
        $page_size = empty($_REQUEST['page_size']) ? 50 : intval($_REQUEST['page_size']);
        $page = empty($_REQUEST['page']) ? 1 : intval($_REQUEST['page']);
        //$addr_list_all = $addressController->get_address();
        $addressController->address_paging($addr_list_all,$page_size,$page);
        if(empty($_REQUEST['ajax']) || $_REQUEST['ajax'] != "1")
            $smarty->assign('full_page', 1);
        $smarty->assign('act', $_REQUEST['act']);
        $smarty->assign('ur_here', $_LANG['order_address_report_title']);
        $weekday = date('w');
        if (isset($_REQUEST['start_date'])){
            $start_date = $_REQUEST['start_date'];
        } else if ($weekday == 1){
            $start_date = local_date('Y-m-d', local_strtotime('-2 days'));
        } else {
            $start_date = local_date('Y-m-d', local_strtotime('-1 days'));
        }
        if (isset($_REQUEST['end_date'])){
            $end_date = $_REQUEST['end_date'];
        } else {
            $end_date = local_date('Y-m-d', local_strtotime('today'));
        }
        $smarty->assign('start_date', $start_date);
        $smarty->assign('end_date', $end_date);
        $smarty->assign('date_today',  local_date('Y-m-d', local_strtotime('today')));
        $smarty->assign('7_date_before',  local_date('Y-m-d', local_strtotime('-7 days')));
        $smarty->assign('30_date_before',  local_date('Y-m-d', local_strtotime('-30 days')));
        $smarty->assign('non_pickup', '1');
        //$smarty->assign('action_link', array('text' => $_LANG['order_address_report'], 'href' => 'order_address.php?act=report'));

        //$addressController->ajaxQueryAction($addr_list_all, 0, false);
        assign_query_info();
        if (isset($_REQUEST['type']) && $_REQUEST['type'] == "time"){
            $smarty->assign('page_type', "time");
            $smarty->display('order_address_time_list.htm');
        } else if (isset($_REQUEST['type']) && $_REQUEST['type'] == "building"){
            $smarty->assign('page_type', "building");
            $smarty->display('order_address_count_list.htm');
        } else if (isset($_REQUEST['type']) && $_REQUEST['type'] == "google_place"){
            $smarty->assign('page_type', "google_place");
            $smarty->display('order_address_count_list.htm');
        } else {
            if (isset($_REQUEST['type']) && $_REQUEST['type'] == "pickup"){
                $smarty->assign('page_type', "pickup");
                //$smarty->display('order_address_pickup_list.htm');
            } else {
                $smarty->assign('page_type', "edit");
            }
            $smarty->display('order_address_list.htm');
        }
    }else if($_REQUEST['act'] == 'edit_list'){
        $page_size = empty($_REQUEST['page_size']) ? 50 : intval($_REQUEST['page_size']);
        $page = empty($_REQUEST['page']) ? 1 : intval($_REQUEST['page']);
        $addr_list_all = $addressController->get_auto_break_list(3409);
        $addressController->address_paging($addr_list_all,$page_size,$page);
        if(empty($_REQUEST['ajax']) || $_REQUEST['ajax'] != "1")
            $smarty->assign('full_page', 1);
        $smarty->assign('act', $_REQUEST['act']);            
        $smarty->assign('ur_here', $_LANG['order_address_edit']);
        $smarty->assign('action_link', array('text' => $_LANG['order_address_report'], 'href' => 'order_address.php?act=report'));
        assign_query_info();
        $smarty->display('order_address_list.htm');
    }else if($_REQUEST['act'] == 'edit'){
        $smarty->assign('addr_list', $addressController->get_address(array("order_address_id = " . $_REQUEST['address_id']), "", 1));
        $smarty->assign('full_page', 1);
        $smarty->assign('ur_here', $_LANG['order_address_edit']);
        $smarty->assign('action_link', array('text' => $_LANG['order_address_return'], 'href' => 'order_address.php?act=edit_list'));
        assign_query_info();
        $smarty->display('order_address_edit.htm');
    }else if($_REQUEST['act'] == 'report'){
        if(empty($_REQUEST['ajax']) || $_REQUEST['ajax'] != "1")
            $smarty->assign('full_page', 1);
        if(!empty($_REQUEST['start_date'])){
            $smarty->assign('start_date',$_REQUEST['start_date']);
        }
        if(!empty($_REQUEST['end_date'])){
            $smarty->assign('end_date',$_REQUEST['end_date']);
        }
        if(!empty($_REQUEST['sorting'])){
            $smarty->assign('sorting',$_REQUEST['sorting']);
        }
        $smarty->assign('action_link', array('text' => $_LANG['order_address_edit'], 'href' => 'order_address.php?act=edit_list'));
        $smarty->assign('action_link2', array('text' => $_LANG['order_address_list'], 'href' => 'order_address.php?act=list'));
        $smarty->assign('ur_here', $_LANG['order_address_report_title']);
        $smarty->display('order_address_report.htm');
        assign_query_info();
    }else if($_REQUEST['act'] == 'update'){
        $addressController->update_address();
        sys_msg("你已成功修改此地址",0,array(array("href"=>"order_address.php?act=edit_list", "text" => $_LANG['order_address_return'])));
    }
    elseif ($_REQUEST['act'] == 'ajaxEdit') {
        if ($_REQUEST['value'] == "N/A"){
            $_REQUEST['value'] = "";
        }
        $return_val = "";
        if ($_REQUEST['value'] != "" && $_REQUEST['value'] != " "){
            $_REQUEST['value'] = trim($_REQUEST['value']);
        }
        if ($_REQUEST['col'] == "address"){
            $full_addr = $addressController->get_order_address_by_id($_REQUEST['id']);
            if ($full_addr){
                $_REQUEST['value'] = $full_addr;
            }
        }
        if ($_REQUEST['col'] == "room" && $_REQUEST['value'] != ""){
            $_REQUEST['value'] = strtoupper($_REQUEST['value']);
        }
        $user_addr_id = $addressController->search_user_address($_REQUEST['id']);
        $order_addr_key = $_REQUEST['mkey'];
        $order_addr_id = $_REQUEST['id'];
        if ($_REQUEST['col'] == "locality" && $_REQUEST['value'] != ""){
            $hk_area_id = $addressController->search_match_area_id($_REQUEST['value']);
            $locality_value = $_REQUEST['value'];
            $locality_col = $_REQUEST['col'];
            $return_val = $locality_value;
            if ($hk_area_id > 0){
                $_REQUEST['value'] = $hk_area_id;
            } else {
                $_REQUEST['value'] = 0;
            }
            $_REQUEST['col'] = "hk_area";
            $addressController->addressEditAction('order_address');
            if ($user_addr_id > 0){
                $_REQUEST['mkey'] = 'address_id';
                $_REQUEST['id'] = $user_addr_id;
                $addressController->addressEditAction('user_address');
                $_REQUEST['value'] = $locality_value;
                $_REQUEST['col'] = $locality_col;
                $addressController->addressEditAction('user_address');
            }
            $_REQUEST['value'] = $locality_value;
            $_REQUEST['col'] = $locality_col;
            $_REQUEST['mkey'] = $order_addr_key;
            $_REQUEST['id'] = $order_addr_id;

            $addressController->addressEditAction('order_address');

        } else {
            if ($user_addr_id > 0 && $_REQUEST['col'] != "verify"){
                $_REQUEST['mkey'] = 'address_id';
                $_REQUEST['id'] = $user_addr_id;
                $addressController->addressEditAction('user_address');
                if ($_REQUEST['col'] == "address"){
                    $_REQUEST['id'] = $order_addr_id;
                    $addressController->addressEditAction('order_info');
                }
                $_REQUEST['mkey'] = $order_addr_key;
                $_REQUEST['id'] = $order_addr_id;
            }
            $addressController->addressEditAction('order_address');
            $return_val = $_REQUEST['value'];
        }
        $return_val = str_replace("\\", "", $return_val);
        if ($return_val == ""){
            $return_val = "N/A";
        }
        make_json_result($return_val);
    }
    elseif ($_REQUEST['act'] == 'ajaxAddressMerge') {
        $addr_id = $_REQUEST['id'];
        /*
        echo "<pre>";
        print_r ($addr_id);
        echo "</pre>";
        */
        $most_recent_addr_id = max($addr_id);
        if (is_numeric($most_recent_addr_id)){
            foreach ($addr_id as $key => $value) {
                if ($value == $most_recent_addr_id) {
                    unset($addr_id[$key]);
                }
                break;
            }
            $res = $addressController->remap_address_to_ordera($addr_id, $most_recent_addr_id);
        } else {
            make_json_result("error: invalid address id");
        }
        make_json_result($res);
    }
}
?>