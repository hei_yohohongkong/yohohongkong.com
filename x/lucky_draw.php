<?php

define('IN_ECS', true);
require(dirname(__FILE__) .'/includes/init.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_base.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_warehouse.php');
require(dirname(__FILE__) .'/includes/ERP/cls/cls_date.php');
$luckyDrawController = new Yoho\cms\Controller\LuckyDrawController();
$adminuserController = new Yoho\cms\Controller\AdminuserController();

if ($_REQUEST['act'] == 'list' || $_REQUEST['act'] == '') {
    include('./includes/ERP/page.class.php');
    
    $smarty->assign('ur_here', '抽獎列表');
    $smarty->assign('full_page', 1);
    $smarty->assign('start_date', $result['start_date']);
    $smarty->assign('end_date', $result['end_date']);
    $smarty->assign('cfg_lang', $_CFG['lang']);
    $smarty->assign('lang', $_LANG);
	$smarty->assign('action_link', array('text' => '建立抽獎活動', 'href' => 'lucky_draw_create.php?act=list'));
    $smarty->display('lucky_draw.htm');
} elseif ($_REQUEST['act'] == 'query') {
    $result = $luckyDrawController->getLuckyDrawEventList();
    $luckyDrawController->ajaxQueryAction($result['data'], $result['record_count'], false);
}
