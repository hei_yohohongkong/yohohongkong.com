<?php

define('IN_ECS', true);

require dirname(__FILE__) . '/includes/init.php';
require_once ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_accounting.php';
$accountingController = new Yoho\cms\Controller\AccountingController();

if ($_REQUEST['act'] == 'list') {
    $pay_methods = $actg->getAccountBankCodes();
    $data = $accountingController->retriveBankRecords();
    $smarty->assign('pay_methods', $pay_methods);
    $smarty->assign('records', $data['records']);
    $smarty->assign('filter', $data['filter']);
    $smarty->assign('page_count', $data['page_count']);
    $smarty->assign('record_count', $data['record_count']);
    $smarty->assign('ur_here', $_LANG['bank_reconciliation']);
    $smarty->assign('full_page', 1);
    $smarty->assign('action_link', array('text' => '上傳銀行紀錄', 'href' => 'bank_reconciliation.php?act=import'));
    $smarty->assign('act', $_REQUEST['act']);
    assign_query_info();
    $smarty->display('bank_reconciliation.htm');
} elseif ($_REQUEST['act'] == 'query') {
    $pay_methods = $actg->getAccountBankCodes();
    $data = $accountingController->retriveBankRecords();
    $smarty->assign('pay_methods', $pay_methods);
    $smarty->assign('records', $data['records']);
    $smarty->assign('filter', $data['filter']);
    $smarty->assign('page_count', $data['page_count']);
    $smarty->assign('record_count', $data['record_count']);
    $smarty->assign('act', $_REQUEST['act']);
    make_json_result($smarty->fetch('bank_reconciliation.htm'), '', array('filter' => $data['filter'], 'page_count' => $data['page_count']));
} elseif ($_REQUEST['act'] == 'import') {
    $pay_methods = $actg->getAccountBankCodes();
    $smarty->assign('pay_methods', $pay_methods);
    $smarty->assign('ur_here', '上傳銀行紀錄');
    $smarty->assign('full_page', 1);
    $smarty->assign('action_link', array('text' => $_LANG['bank_reconciliation'], 'href' => 'bank_reconciliation.php?act=list'));
    $smarty->assign('act', $_REQUEST['act']);
    assign_query_info();
    $smarty->display('bank_reconciliation.htm');
} elseif ($_REQUEST['act'] == 'upload') {
    $accountingController->importBankRecords();
} elseif ($_REQUEST['act'] == 'matched') {
    $pay_methods = $actg->getAccountBankCodes();
    $data = !empty($_REQUEST['transaction_code']) ? $accountingController->searchMatchedRecords() : $accountingController->retriveMatchedRecords();
    $smarty->assign('pay_methods', $pay_methods);
    $smarty->assign('txns', $data['txns']);
    $smarty->assign('filter', $data['filter']);
    $smarty->assign('ur_here', $_LANG['bank_reconciliation_match']);
    $smarty->assign('full_page', 1);
    $smarty->assign('action_link', array('text' => '處理銀行紀錄', 'href' => 'bank_reconciliation.php?act=match'));
    $smarty->assign('act', $_REQUEST['act']);
    assign_query_info();
    $smarty->display('bank_reconciliation.htm');
} elseif ($_REQUEST['act'] == 'match') {
    $pay_methods = $actg->getAccountBankCodes();
    $data = $accountingController->matchTransactionsToBankRecords();
    $smarty->assign('pay_methods', $pay_methods);
    $smarty->assign('result_txns', $data['result_txns']);
    $smarty->assign('banks', $data['banks']);
    $smarty->assign('accounts', $data['accounts']);
    $smarty->assign('account_id', $data['account_id']);
    $smarty->assign('ur_here', '處理銀行紀錄');
    $smarty->assign('full_page', 1);
    $smarty->assign('action_link', array('text' => $_LANG['bank_reconciliation_match'] . " List", 'href' => 'bank_reconciliation.php?act=matched'));
    $smarty->assign('act', $_REQUEST['act']);
    $smarty->assign('unmatched_item_only', $_REQUEST['unmatched_item_only']);
    assign_query_info();
    $smarty->display('bank_reconciliation.htm');
} elseif ($_REQUEST['act'] == 'link') {
    $accountingController->linkBankTransactionRecord();
} elseif ($_REQUEST['act'] == 'merge') {
    $accountingController->mergeTransaction();
} elseif ($_REQUEST['act'] == 'change_account') {
    $accountingController->changeBelongingAccount();
} elseif ($_REQUEST['act'] == 'suggest') {
    $accountingController->getPossibleBankRecords();
} elseif ($_REQUEST['act'] == 'suggest2') {
    $accountingController->getPossibleTxnRecords();
} elseif ($_REQUEST['act'] == 'searchGroup') {
    $accountingController->retriveTxnGroupRecords();
} elseif ($_REQUEST['act'] == 'downloadAudit') {
    $accountingController->generateBankAuditReport();
} elseif ($_REQUEST['act'] == 'unbind') {
    check_authz_json('unlink_bank_reconciliation');
    
    $accountingController->unbindBankReconciliationEntries($_REQUEST['ids']);
} elseif ($_REQUEST['act'] === 'matched_report_export') {
    $data = !empty($_REQUEST['transaction_code']) ? $accountingController->searchMatchedRecords() : $accountingController->retriveMatchedRecords();
    $accountingController->generateBankReconciliationReport($data['txns']);
}