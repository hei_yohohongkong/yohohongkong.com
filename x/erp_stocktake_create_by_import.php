<?php

define('IN_ECS', true);
require(dirname(__FILE__) .'/includes/init.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_base.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_warehouse.php');
require(dirname(__FILE__) .'/includes/ERP/cls/cls_date.php');
$stocktakeController = new Yoho\cms\Controller\StocktakeController();
$adminuserController = new Yoho\cms\Controller\AdminuserController();

if ($_REQUEST['act'] == 'list') {
    $result = $stocktakeController->create();
    $smarty->assign('stocktake_person_option', $result['stocktake_person_option']);
    $smarty->assign('warehouse_list', $result['warehouse_list']);
    $smarty->assign('brand_list', $result['brand_list']);
    $smarty->assign('ur_here', '新增存貨盤點');
    $smarty->assign('full_page', 1);
    $smarty->assign('cfg_lang', $_CFG['lang']);
    $smarty->assign('lang', $_LANG);
    $smarty->assign('action_link', array('text' => '盤點工作清單', 'href' => 'erp_stocktake.php'));

    assign_query_info();
    $smarty->display('erp_stocktake_create_by_import.htm');
} elseif ($_REQUEST['act'] == 'post_stocktake_create') {
    $result = $stocktakeController->stocktake_create_by_import();

    if (isset($result['error'])) {
        make_json_error(implode('<br>', $result['error']));
    } elseif ($result == false) {
        make_json_error('發生錯誤');
    } else {
        make_json_result($result);
    }
}
