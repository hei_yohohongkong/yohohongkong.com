<?php
define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require_once ROOT_PATH . '/includes/braintree/lib/Braintree.php';
require_once ROOT_PATH . '/includes/stripe/init.php';
require_once ROOT_PATH . '/includes/lib_payment.php';
$payment_lang = ROOT_PATH . 'languages/zh_tw/payment/braintree.php';

$risk_status = [
    1 => "詐騙",
    2 => "高風險",
    3 => "中等風險",
    4 => "低風險",
];
$pay_method = [
    "stripe" => "Stripe",
    "braintree" => "Braintree",
    "braintreepaypal" => "Braintree-PAYPAL",
];

$is_sandbox = false;
if ($is_sandbox) {
    // Braintree
    $environment = Braintree\Configuration::environment('sandbox');
    $merchant_id = 'fd3vmpwh4cy7qcv3';
    $public_key  = '38k9tgrwxz324dkd';
    $private_key = '69863db467746b406b5a7e951c35caf8';

    // Stripe
    $publishable_key = 'pk_test_E0Stl002F54obSM0TUWwAgix';
    $secret_key      = 'sk_test_fB3Ene1AWkvnEc0IdfHLTgEH';
} else {
    // Braintree
    $payment = get_payment('braintree');
    $environment = Braintree\Configuration::environment('production');
    $merchant_id = $payment['braintree_merchantId'];
    $public_key  = $payment['braintree_publicKey'];
    $private_key = $payment['braintree_privateKey'];

    // Stripe
    $sql = 'SELECT * FROM ' . $ecs->table('payment').
        " WHERE pay_code = '$pay_code' AND enabled = '1'";
    $payment = $db->getRow($sql);
    $publishable_key = $payment['stripe_publishable_key'];
    $secret_key      = $payment['stripe_secret_key'];
}

$merchantId  = Braintree\Configuration::merchantId($merchant_id);
$publicKey   = Braintree\Configuration::publicKey($public_key);
$privateKey  = Braintree\Configuration::privateKey($private_key);
\Stripe\Stripe::setApiKey($secret_key);

$fsql = "SELECT * FROM " . $GLOBALS["ecs"]->table("order_fraud") . " of " .
        "LEFT JOIN " . $GLOBALS["ecs"]->table("order_info") . " oi ON oi.order_id = of.order_id " .
        "LEFT JOIN " . $GLOBALS["ecs"]->table("payment") . " p ON p.pay_id = of.pay_id " .
        "WHERE of.status != 0 AND NOT(of.pay_id = 17 AND status = 4) ORDER BY status ASC, ofid ASC";
$flist = $db->getAll($fsql);
foreach ($flist as $key => $row) {
    $flist[$key]['risk_status'] = $risk_status[$row['status']];
    $flist[$key]['pay_method'] = $pay_method[$row['pay_code']];
    $flist[$key]['txn_url'] = $row['pay_code'] == "stripe" ? "https://dashboard.stripe.com/payments/$row[txn]" : (in_array($row['pay_code'], ["braintree", "braintreepaypal"]) ? "https://" . ($is_sandbox ? "sandbox" : "www") . ".braintreegateway.com/merchants/$merchant_id/transactions/$row[txn]" : "");
}
$smarty->assign("list", $flist);
$smarty->assign("ur_here", "可疑交易紀錄");
$smarty->assign("full_page", 1);
$smarty->display("order_fraud.htm");



// if ($_REQUEST['stripe']) {
//     $secret_key      = 'sk_live_Jr25LQ8cNf0YHgshsppc5gYV';
//     \Stripe\Stripe::setApiKey($secret_key);
//     $card = \Stripe\Source::retrieve("src_1CNtpRE6zRu3MKlsFymEZLrz");
//     var_dump($card->card->brand);
// } elseif (file_exists($payment_lang)) {
//     global $_LANG;

//     include_once $payment_lang;
    

//     // $search = [Braintree_CreditCardVerificationSearch::creditCardExpirationDate()->is("01/20"),];
//     if (!empty($_REQUEST['id'])) {
//         $search[] = Braintree_CreditCardVerificationSearch::id()->is($_REQUEST['id']);
//     }
//     $collection = Braintree_Transaction::search($search);
//     foreach ($collection as $verification) {
//         if (!empty($verification->riskData)) {
//             if ($verification->riskData->decision != "Approve") {
//                 echo $verification->id . ": " . $verification->cvvResponseCode . " / " . $verification->riskData->decision . "<br>";
//             }
//         } else {
//             echo $verification->id . ": " . $verification->cvvResponseCode . "<br>";
//         }
//         if (!in_array($verification->cvvResponseCode, ["M"])) {
//             echo $verification->id . ": " . $verification->cvvResponseCode . " / " . $verification->riskData->decision . "<br>";
//         }
//         if (!empty($_REQUEST['id'])) {
//             var_dump($verification);
//         }
//     }
// } else {
//     echo "no file with lang: zh_tw";
// }
?>