<?php

/**
 * ECSHOP 支付方式管理程序
 * ============================================================================
 * 版权所有 2005-2010 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liuhui $
 * $Id: payment.php 17063 2010-03-25 06:35:46Z liuhui $
 * $Rewrite: Anthony@YOHO 2017-05-23
*/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_accounting.php');
use Yoho\cms\Model;

$exc = new exchange($ecs->table('payment'), $db, 'pay_code', 'pay_name');
$paymentController = new Yoho\cms\Controller\PaymentController();
$admin_priv = $paymentController::DB_ACTION_CODE_PAYMENT;

/*------------------------------------------------------ */
//-- 支付方式列表 ?act=list
/*------------------------------------------------------ */

if ($_REQUEST['act'] == 'list')
{
    admin_priv($admin_priv);
    
    $data = $paymentController->get_payment_modules($_REQUEST['edit_lang']);
    assign_query_info();
    
    $smarty->assign('ur_here', $_LANG['02_payment_list']);
    $smarty->assign('action_link', array('text' => $_LANG['rule_list'], 'href' => 'payment.php?act=rule_list'));
    $smarty->assign('full_page', 1);
    $smarty->assign('modules', $data['modules']);
    $smarty->assign('tenpayc2c', $data['tenpayc2c']);
    $smarty->display('payment_list.htm');
}
elseif ($_REQUEST['act'] == 'query')
{
    $data = $paymentController->get_payment_modules($_REQUEST['edit_lang']);
    
    $smarty->assign('modules', $data['modules']);
    $smarty->assign('tenpayc2c', $data['tenpayc2c']);
    
    make_json_result($smarty->fetch('payment_list.htm'), '', array('filter' => array(), 'page_count' => 1));
}

/*------------------------------------------------------ */
//-- 安装支付方式 ?act=install&code=".$code."
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'install')
{
    admin_priv($admin_priv);

    $code = $_REQUEST['code'];
    /* 查询该支付方式内容 */
    if (isset($code))
    {
        $code = trim($code);
    }
    else
    {
        die('invalid parameter');
    }
    
    $pay = $paymentController->setup_payment_modules($code);
    $localizable_fields = localizable_fields_for_table('payment');
    $accounts = $actg->getAccounts();
    $payment_types = $actg->getPaymentTypes();
    
    assign_query_info();

    $smarty->assign('accounts', $accounts);
    $smarty->assign('payment_types', $payment_types);
    $smarty->assign('localizable_fields', $localizable_fields);
    $smarty->assign('pay', $pay);
    $smarty->assign('action', 'install');
    $smarty->assign('rule_types', $paymentController->get_type_array());
    $smarty->assign('rule_equivalent', $paymentController->get_equiv_array());
    
    // 取得國家名單
    $smarty->assign('available_countries', available_countries());

    $smarty->assign('action_link',  array('text' => $_LANG['02_payment_list'], 'href' => 'payment.php?act=list'));
    $smarty->display('payment_edit.htm');
}

elseif ($_REQUEST['act'] == 'get_config')
{
    check_authz_json($admin_priv);

    $code = $_REQUEST['code'];

    $config = $paymentController->get_payment_config_template($code);

    make_json_result($config);
}

/*------------------------------------------------------ */
//-- 编辑支付方式 ?act=edit&code={$code}
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'edit')
{
    admin_priv($admin_priv);

    $code = $_REQUEST['code'];
    /* 查询该支付方式内容 */
    if (isset($code))
    {
        $code = trim($code);
    }
    else
    {
        die('invalid parameter');
    }
    
    $pay = $paymentController->get_payment_modules_file($code);
    
    // Get accounts payment methods
    $account_payment_methods = $actg->getAccountPaymentMethods(array('pay_code' => $code, 'active' => 1));
    $localizable_fields = localizable_fields_for_table('payment');
    $accounts = $actg->getAccounts();
    $payment_types = $actg->getPaymentTypes();
    $localized_versions = get_localized_versions('payment', $pay['pay_id'], $localizable_fields);
    foreach($localized_versions as $data => $config){
        $banks = [];
        if(isset($localized_versions[$data]['pay_config'])){
            $store = unserialize($localized_versions[$data]['pay_config']);
            foreach($store as $key => $value){
                if($value['name'] == 'banks'){
                    $banks = $value['value'];
                } elseif ($value['name'] == 'tutorial'){
                    $localized_versions[$data]['conf_tutorial'] = $value['value'];
                }
            }
        }
        foreach ($pay['banks'] as $k => $v) {
            $localized_versions[$data]["bank$k"] = !isset($banks[$k - 1]) ? "" : $banks[$k - 1]["name"];
            if (!in_array("bank$k", $localizable_fields)) {
                $localizable_fields[] = "bank$k";
            }
        }
    }
    foreach ($pay['pay_config'] as $conf) {
        if ($conf['name'] == "tutorial" && !in_array("tutorial", $localizable_fields)) {
            $localizable_fields[] = "conf_tutorial";
        }
    }
    assign_query_info();

    $smarty->assign('account_payment_methods', $account_payment_methods);
    $smarty->assign('accounts', $accounts);
    $smarty->assign('payment_types', $payment_types);
    $smarty->assign('localizable_fields', $localizable_fields);
    $smarty->assign('localized_versions', $localized_versions);
    $smarty->assign('pay', $pay);
    $smarty->assign('action', 'edit');
    $smarty->assign('rule_types', $paymentController->get_type_array());
    $smarty->assign('rule_equivalent', $paymentController->get_equiv_array());
    // 取得國家名單
    $smarty->assign('available_countries', available_countries());
    
    $smarty->assign('payment_rules', $rules);
    $smarty->assign('action_link',  array('text' => $_LANG['02_payment_list'], 'href' => 'payment.php?act=list'));
    $smarty->assign('ur_here', $_LANG['edit'] . $_LANG['payment']);
    $smarty->display('payment_edit.htm');
}

/*------------------------------------------------------ */
//-- 提交支付方式 post
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'submit_payment')
{
    admin_priv($admin_priv);
    $post = $_POST;
    // var_dump($post);
    $res = $paymentController->submit_form_action($post);
    
    if($res){
        $action = $_POST['action'];
        $pay_name = $_POST['pay_name'];
        $tablename ='payment';
        
        // 记录日志
        admin_log($pay_name, $action, $tablename);
        $link[] = array('text' => $_LANG['back_list'], 'href' => 'payment.php?act=list');
        sys_msg($_LANG[$action.'_ok'], 0, $link);
    } else {
        sys_msg($_LANG['payment_not_available']);
    }
    
}

/*------------------------------------------------------ */
//-- 卸载支付方式 ?act=uninstall&code={$code}
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'uninstall')
{
    admin_priv($admin_priv);
    
    $pay_code = $_REQUEST['code'];
    
    /* We not delete payment method, just only disable this. */
    $res = $paymentController->disable_payment($pay_code);
    
    if($res){
        /* 记录日志 */
        admin_log($_REQUEST['code'], 'uninstall', 'payment');
        $link[] = array('text' => $_LANG['back_list'], 'href' => 'payment.php?act=list');
        sys_msg($_LANG['uninstall_ok'], 0, $link);
        
    } else {
        sys_msg($_LANG['payment_not_available']);
    }
}

/*------------------------------------------------------ */
//-- 修改支付方式名称
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'edit_name')
{
    /* 检查权限 */
    check_authz_json($admin_priv);

    /* 取得参数 */
    $pay_code = json_str_iconv(trim($_POST['id']));
    $pay_name = json_str_iconv(trim($_POST['val']));
    $edit_lang = empty($_REQUEST['edit_lang']) ? '' : trim($_REQUEST['edit_lang']);
    
    $res = $paymentController->ajax_mulit_lang_info($pay_name, $pay_code, 'pay_name', $edit_lang);
    
    if($res){
        make_json_result(stripcslashes($pay_name));
    } else {
        return false;
    }
}

/*------------------------------------------------------ */
//-- 修改支付方式描述
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'edit_desc')
{
    /* 检查权限 */
    check_authz_json($admin_priv);

    /* 取得参数 */
    $pay_code = json_str_iconv(trim($_POST['id']));
    $pay_desc = json_str_iconv(trim($_POST['val']));
    $edit_lang = empty($_REQUEST['edit_lang']) ? '' : trim($_REQUEST['edit_lang']);
    
    $res = $paymentController->ajax_mulit_lang_info($pay_desc, $pay_code, 'pay_desc', $edit_lang);
    
    if($res){
        make_json_result(stripcslashes($pay_desc));
    } else {
        return false;
    }
}

/*------------------------------------------------------ */
//-- 修改支付方式排序
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'edit_order')
{
    /* 检查权限 */
    check_authz_json($admin_priv);

    /* 取得参数 */
    $pay_code = json_str_iconv(trim($_POST['id']));
    $pay_order = intval($_POST['val']);

    /* 更新排序 */
    $exc->edit("pay_order = '$pay_order'", $pay_code);
    make_json_result(stripcslashes($pay_order));
}

/*------------------------------------------------------ */
//-- 修改支付方式费用
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'edit_pay_fee')
{
    /* 检查权限 */
    check_authz_json($admin_priv);

    /* 取得参数 */
    $pay_code = json_str_iconv(trim($_POST['id']));
    $pay_fee = json_str_iconv(trim($_POST['val']));
    
    $pay_fee = $paymentController->pay_fee($pay_fee);

    /* 更新支付费用 */
    $exc->edit("pay_fee = '$pay_fee'", $pay_code);
    make_json_result(stripcslashes($pay_fee));
}

/*------------------------------------------------------ */
//-- 修改前台顯示名稱
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'edit_display_name')
{
    /* 检查权限 */
    check_authz_json($admin_priv);

    /* 取得参数 */
    $pay_code = json_str_iconv(trim($_POST['id']));
    $display_name = json_str_iconv(trim($_POST['val']));
    $edit_lang = empty($_REQUEST['edit_lang']) ? '' : trim($_REQUEST['edit_lang']);
    
    $res = $paymentController->ajax_mulit_lang_info($display_name, $pay_code, 'display_name', $edit_lang);
    
    if($res){
        make_json_result(stripcslashes($display_name));
    } else {
        return false;
    }
}

/*------------------------------------------------------ */
//-- 修改POS顯示
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'toggle_is_pos')
{
    check_authz_json($admin_priv);

    $pay_code = json_str_iconv(trim($_POST['id']));
    $is_pos   = intval($_POST['val']);

    if ($exc->edit("is_pos = '$is_pos' ", $pay_code))
    {
        clear_cache_files();
        make_json_result($is_pos);
    }
}

/*------------------------------------------------------ */
//-- 修改網上支付
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'toggle_is_online_pay')
{
    check_authz_json($admin_priv);

    $pay_code = json_str_iconv(trim($_POST['id']));
    $is_online_pay   = intval($_POST['val']);

    if ($exc->edit("is_online_pay = '$is_online_pay' ", $pay_code))
    {
        clear_cache_files();
        make_json_result($is_online_pay);
    }
}

/*------------------------------------------------------ */
//-- 規則方式列表 ?act=rule_list
/*------------------------------------------------------ */

if ($_REQUEST['act'] == 'rule_list')
{
    admin_priv($admin_priv);
    
    $data = $paymentController->get_rule_list();
    assign_query_info();
    
    $smarty->assign('rule_list', $data);
    $smarty->assign('ur_here', $_LANG['rule_type']);
    // $smarty->assign('action_link', array('text' => $_LANG['new_rule'], 'href' => 'payment.php?act=edit_rule'));
    $smarty->assign('action_link2',  array('text' => $_LANG['02_payment_list'], 'href' => 'payment.php?act=list'));
    $smarty->assign('full_page', 1);
    $smarty->display('payment_rule_list.htm');
}

/*------------------------------------------------------ */
//-- 增加/編輯規則
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'edit_rule')
{
    admin_priv($admin_priv);
    
    $id   = $_REQUEST['rule_id'];
    $data = $paymentController->rule_page_data($id);

    $smarty->assign('pay_list', $data['pay_list']);
    $smarty->assign('rule_types', $data['types']);
    $smarty->assign('rule_equivalent', $data['equiv_list']);
    $smarty->assign('rule', $data['rule']);
    $smarty->assign('child_list', $data['child']);
    // 取得國家名單
    $smarty->assign('available_countries', available_countries());
    $smarty->assign('data', $data);
    $smarty->assign('ur_here', $_LANG['edit'] . $_LANG['payment_rule']);
    $smarty->display('payment_rule_edit.htm');
}

/*------------------------------------------------------ */
//-- 移除規則
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'remove_rule')
{
    admin_priv($admin_priv);
    
    $id   = $_REQUEST['rule_id'];
    $res  = $paymentController->remove_rule($id);

    if($res)
    {
        /* 记录日志 */
        admin_log($_REQUEST['rule_id'], 'remove', 'payment_rule');
        $link[] = array('text' => $_LANG['back_rule_list'], 'href' => 'payment.php?act=rule_list');
        sys_msg($_LANG['uninstall_ok'], 0, $link);
    } else {
        return false;
    }
}

/*------------------------------------------------------ */
//-- 提交規則 post
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'sumbit_rule')
{
    admin_priv($admin_priv);
    
    $post = $_POST;
    $res = $paymentController->submit_rule_action($post);
    
    if($res){
        $action = 'add_or_edit_rule';
        $rule_id = $_POST['pay_rule_id'];
        $tablename ='payment_rule';
        
        // 记录日志
        admin_log($rule_id, $action, $tablename);
        $link[] = array('text' => $_LANG['back_rule_list'], 'href' => 'payment.php?act=rule_list');
        sys_msg($_LANG['edit_ok'], 0, $link);
    } else {
        sys_msg($_LANG['rule_not_available']);
    }
    
    
}


?>
