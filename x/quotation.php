<?php

/* * *
 * quotation.php
 * by Anthony 2017-04-05
 *
 * Manage quotation backend function
 * * */
define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

$quotationController = new Yoho\cms\Controller\QuotationController();

use Yoho\cms\Model;

switch ($_REQUEST['act']) {

	/* ------------------------------------------------------ */
	//-- List Quotation
	/* ------------------------------------------------------ */
	case 'list':
		/* 检查权限 */
		admin_priv('quotation');

		$list = $quotationController->get_quotation_list();
		// title
		$smarty->assign('ur_here', $_LANG['quotation']);
		$smarty->assign('full_page', 1);
		// list
		$smarty->assign('list', $list['data']);
		$smarty->assign('filter', $list['filter']);
		$smarty->assign('record_count', $list['record_count']);
		$smarty->assign('page_count', $list['page_count']);

		$sort_flag = sort_flag($list['filter']);
		$smarty->assign($sort_flag['tag'], $sort_flag['img']);

		assign_query_info();

		//var_dump($_REQUEST);
		$smarty->display('quotation_list.htm');
		break;
	/* ------------------------------------------------------ */
	//-- Query Quotation
	/* ------------------------------------------------------ */
	case 'query':

		/* 检查权限 */
		admin_priv('quotation');
		$list = $quotationController->get_quotation_list();
		$smarty->assign('list', $list['data']);
		$smarty->assign('filter', $list['filter']);
		$smarty->assign('record_count', $list['record_count']);
		$smarty->assign('page_count', $list['page_count']);

		$sort_flag = sort_flag($list['filter']);
		$smarty->assign($sort_flag['tag'], $sort_flag['img']);

		make_json_result($smarty->fetch('quotation_list.htm'), '', array('filter' => $list['filter'], 'page_count' => $list['page_count']));
		break;
	/* ------------------------------------------------------ */
	//-- Quotation Info
	/* ------------------------------------------------------ */
	case 'info':

		/* 检查权限 */
		admin_priv('quotation');

		$quotation = new Model\Quotation($_REQUEST['id']);
		$data = $quotation->data;

		$goods = $quotationController->get_quotation($_REQUEST['id']);
		$data['quotation_fee_formatted'] = price_format($data['quotation_fee'], false);

		$smarty->assign('ur_here', $_LANG['quotation']);
		$smarty->assign('quotation', $data);
		$smarty->assign('goods_list', $goods);
		$smarty->display('quotation_info.htm');
		break;

	case 'view_pdf':

		$quotationController->view_pdf($_REQUEST['sn']);
		break;
}
?>
