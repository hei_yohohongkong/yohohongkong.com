<?php
define('IN_ECS', true);
require(dirname(__FILE__) . '/includes/init.php');
require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/admin/statistic.php');

$smarty->assign('lang', $_LANG);
$userController = new Yoho\cms\Controller\UserController();
$addressController    = new Yoho\cms\Controller\AddressController();
$insuranceController    = new Yoho\cms\Controller\InsuranceController();

if(isset($_REQUEST['act'])){
    if($_REQUEST['act'] == 'query'){
        $page_size = empty($_REQUEST['page_size']) ? 50 : intval($_REQUEST['page_size']);
        $page = empty($_REQUEST['page']) ? 1 : intval($_REQUEST['page']);
        $offset = empty($_REQUEST['start']) ? 0 : intval($_REQUEST['start']);
        $sort_order = empty($_REQUEST['sort_order']) ? 'DESC' : $_REQUEST['sort_order'];
        if (isset($_REQUEST['data-type'])) {
            if ($_REQUEST['data-type'] == 'list'){
                $sort_by = (empty($_REQUEST['sort_by']) || in_array($_REQUEST['sort_by'], array("count_goods", "count_bought", "count_claimed"))) ? 'insurance_id' : $_REQUEST['sort_by'];
                $order = $sort_by." ".$sort_order;
                $ins_list_count = $insuranceController->countIns();
                $ins_list = $insuranceController->getIns($arr_where, $order, $page_size, $offset);
                foreach ($ins_list as $key => $ins){
                    $ins_list[$key]['action'] = 
                        "<a href='insurance.php?act=ins_edit&id=".$ins['insurance_id']."' class='fa fa-edit btn btn-primary btn-sm' role='button'>".$_LANG['erp_operation_edit']."</a>";
                        //"<a href='insurance.php?act=ins_remove&id=".$ins['insurance_id']."' class='fa fa-trash btn btn-danger btn-sm delete-btn disabled' role='button'>".$_LANG['erp_operation_delete']."</a>";
                        if (!$ins_list[$key]['count_bought']){
                            $ins_list[$key]['count_bought'] = 0;
                        } else {
                            $ins_list[$key]['count_bought'] = "<a href='insurance.php?act=bought_list&ins_id=".$ins['insurance_id']."'>".$ins['count_bought']."</a>";
                        }
                        if (!$ins_list[$key]['count_claimed']){
                            $ins_list[$key]['count_claimed'] = 0;
                        } else {
                            $ins_list[$key]['count_claimed'] = "<a href='insurance.php?act=claim_list&ins_id=".$ins['insurance_id']."'>".$ins['count_claimed']."</a>";
                        }
                    }
                $smarty->assign('data', $ins_list);
                $insuranceController->ajaxQueryAction($ins_list, $ins_list_count, false);
            } elseif ($_REQUEST['data-type'] == 'ins-list') {
                $sort_by = empty($_REQUEST['sort_by']) ? 'insurance_goods_id' : $_REQUEST['sort_by'];
                $sort_by_stock = FALSE;
                if ($sort_by == 'stock'){
                    $sort_by_stock = TRUE;
                    $sort_by = 'insurance_goods_id';
                }
                $order = $sort_by." ".$sort_order;

                $arr_sql_where = array();

                if (isset($_REQUEST['cat']) && $_REQUEST['cat'] >= 1){
                    $arr_cats = cat_list($_REQUEST['cat'], $_REQUEST['cat'], FALSE);
                    $arr_cats_id = array();
                    if (sizeof($arr_cats) > 0){
                        foreach($arr_cats as $cat){
                            array_push($arr_cats_id, $cat['cat_id']);
                        }
                    }
                    $str_cats_id = implode(",", $arr_cats_id);
                    array_push($arr_sql_where, 'g.cat_id IN ('.$str_cats_id.')');
                }
                if (isset($_REQUEST['brand']) && $_REQUEST['brand'] > -1){
                    array_push($arr_sql_where, 'g.brand_id = '.$_REQUEST['brand']);
                }
                if (isset($_REQUEST['search_key']) && trim($_REQUEST['search_key'] != '')){
                    $search_key = trim($_REQUEST['search_key']);
                    $str_search_key = preg_replace(array('/,/', '/\s+/'), array('', ''), $search_key);
                    if (is_numeric($str_search_key) && strpos($search_key, ',') !== FALSE){
                            array_push($arr_sql_where, "g.goods_id IN (".$search_key.")");
                    } else {
                        array_push($arr_sql_where, "(g.goods_id LIKE '%".$search_key."%' OR g.goods_sn LIKE '%".$search_key."%' OR g.goods_name LIKE '%".$search_key."%') ");
                    }
                }

                $ins_id = empty($_REQUEST['id']) ? 0 : intval($_REQUEST['id']);
                $ins_goods_list_count = $insuranceController->countInsGood($ins_id, $arr_sql_where);
                $ins_goods_list = $insuranceController->getInsGoodList($ins_id, $order, $arr_sql_where, $page_size, $offset);
                $ins_info =  $insuranceController->getInsuranceInfo($ins_id);

                $duration_options = explode(',', $ins_info['duration']);

                $erpController = new Yoho\cms\Controller\ErpController();

                $ins_goods_group_list = [];

                foreach ($ins_goods_list as $key => $good){
                    if (!isset($ins_goods_group_list[$good['goods_id']])){
                        $ins_goods_group_list[$good['goods_id']] = array('insurance_goods_id' => $good['insurance_goods_id'], 'goods_id' => $good['goods_id'], 'cost' => $good['cost'], 'goods_name' => $good['goods_name'], 'shop_price' => $good['shop_price'], 'stock' => $erpController->getSellableProductQty($good['goods_id']), 'delete' => "<span class='del-item glyphicon glyphicon-remove pointer' data-good-id='".$good['goods_id']."' onclick='deleteItem(this)'></span>");
                        foreach ($duration_options as $option){
                            $ins_goods_group_list[$good['goods_id']]['duration_'.$option] = '-';
                        }
                    }
                    $ins_goods_group_list[$good['goods_id']]['duration_'.$good['duration']] = $good['price_percent'];
                    //$ins_goods_list[$key]['duration'] = $ins_info['name'].$good['duration'].$_LANG['no_month'];
                    //$ins_goods_list[$key]['stock'] = $erpController->getSellableProductQty($good['goods_id']);
                    //$ins_goods_list[$key]['delete'] = "<span class='del-item glyphicon glyphicon-remove pointer' data-ins-good-id='".$good['insurance_goods_id']."' onclick='deleteItem(this)'></span>";
                }
 
                unset($ins_goods_list);
                $ins_goods_list = array_values($ins_goods_group_list);
                
                if ($sort_by_stock){
                    if ($sort_order == 'ASC' || $sort_order == 'asc'){
                        usort($ins_goods_list, function($a, $b) {
                            return $a["stock"] - $b["stock"];
                        });
                    } else {
                        usort($ins_goods_list, function($a, $b) {
                            return $b["stock"] - $a["stock"];
                        });
                    }
                }
                //dd($ins_goods_group_list);
                //dd($ins_goods_list);
                $smarty->assign('data', $ins_goods_list);
                $insuranceController->ajaxQueryAction($ins_goods_list, $ins_goods_list_count, false);
            } elseif ($_REQUEST['data-type'] == 'bought-list') {
                $sort_by = empty($_REQUEST['sort_by']) ? 'og.rec_id' : $_REQUEST['sort_by'];
                $sort_by_stock = FALSE;
                $order = $sort_by." ".$sort_order;

                $arr_sql_where = array();

                //if (isset($_REQUEST['brand']) && $_REQUEST['brand'] > -1){
                //    array_push($arr_sql_where, 'g.brand_id = '.$_REQUEST['brand']);
                //}


                $ins_id = empty($_REQUEST['ins_id']) ? 0 : intval($_REQUEST['ins_id']);
                $ins_goods_list_count = $insuranceController->countInsGoodBought($ins_id, $arr_sql_where);
                $ins_goods_list = $insuranceController->getInsGoodBoughtList($ins_id, $order, $arr_sql_where, $page_size, $offset);
                //$ins_info =  $insuranceController->getInsuranceInfo($ins_id);

                $duration_options = explode(',', $ins_info['duration']);

                foreach ($ins_goods_list as $key => $good){
                    $order_link = "<a href='order.php?act=info&order_id=".$good['order_id']."' target='_blank'>".$good['order_sn']."</a>";
                    $ins_goods_list[$key]['order_sn'] = $order_link;
                    $ins_goods_list[$key]['add_time'] = local_date($GLOBALS['_CFG']['time_format'], $good['add_time']);
                }
 
                //unset($ins_goods_list);
                //$ins_goods_list = array_values($ins_goods_group_list);
                
                //if ($sort_by_stock){
                //    if ($sort_order == 'ASC' || $sort_order == 'asc'){
                //        usort($ins_goods_list, function($a, $b) {
               //             return $a["stock"] - $b["stock"];
               //         });
                //    } else {
                //        usort($ins_goods_list, function($a, $b) {
                //            return $b["stock"] - $a["stock"];
                //        });
               //     }
               // }
                //dd($ins_goods_group_list);
                //dd($ins_goods_list);
                $smarty->assign('data', $ins_goods_list);
                $insuranceController->ajaxQueryAction($ins_goods_list, $ins_goods_list_count, false);
            } elseif ($_REQUEST['data-type'] == 'claim-list') {
                $sort_by = empty($_REQUEST['sort_by']) ? 'og.rec_id' : $_REQUEST['sort_by'];
                $sort_by_stock = FALSE;
                $order = $sort_by." ".$sort_order;

                $arr_sql_where = array();

                //if (isset($_REQUEST['brand']) && $_REQUEST['brand'] > -1){
                //    array_push($arr_sql_where, 'g.brand_id = '.$_REQUEST['brand']);
                //}


                $ins_id = empty($_REQUEST['ins_id']) ? 0 : intval($_REQUEST['ins_id']);
                $ins_goods_list_count = $insuranceController->countInsGoodClaim($ins_id, $arr_sql_where);
                $ins_goods_list = $insuranceController->getInsGoodClaimList($ins_id, $order, $arr_sql_where, $page_size, $offset);
                //$ins_info =  $insuranceController->getInsuranceInfo($ins_id);

                $duration_options = explode(',', $ins_info['duration']);

                foreach ($ins_goods_list as $key => $good){
                    $order_link = "<a href='order.php?act=info&order_id=".$good['order_id']."' target='_blank'>".$good['order_sn']."</a>";
                    $ins_goods_list[$key]['order_sn'] = $order_link;
                    $ins_goods_list[$key]['claim_datetime'] = local_date($GLOBALS['_CFG']['time_format'], $good['claim_datetime']);
                }


                $smarty->assign('data', $ins_goods_list);
                $insuranceController->ajaxQueryAction($ins_goods_list, $ins_goods_list_count, false);
            }
        }
        exit;
        if (isset($_REQUEST['data-type'])) {
            $smarty->assign('full_page', 0);

            if (isset($_REQUEST['type']) && $_REQUEST['type'] == 'pickup'){
                $non_pickup = 'false';
            } else {
                if (isset($_REQUEST['non_pickup'])){
                    $non_pickup = $_REQUEST['non_pickup'];
                } else {
                    $non_pickup = 'true';
                }
            }

            if ((isset($_REQUEST['type']) && $_REQUEST['type'] == 'pickup') || isset($_REQUEST['pickup_only'])){
                $pickup_only = true;
            } else {
                $pickup_only = 'false';
            }

            if (isset($_REQUEST['search_type_cat'])){
                $search_type_cat = $_REQUEST['search_type_cat'];
            } else {
                $search_type_cat = false;
            }

            if (isset($_REQUEST['keyword_cat']) && (isset($_REQUEST['keyword_text_1']) || isset($_REQUEST['keyword_text_2']))){
                $search_keyword_cat = $_REQUEST['keyword_cat'];
                if (isset($_REQUEST['keyword_text_1']) && isset($_REQUEST['keyword_text_2']) && trim($_REQUEST['keyword_text_1']) !="" && trim($_REQUEST['keyword_text_2']) !=""){
                    $search_keyword_1 = trim($_REQUEST['keyword_text_1']);
                    $search_keyword_2 = trim($_REQUEST['keyword_text_2']);
                    $keyword_operator = $_REQUEST['keyword_operator'];
                } else if (isset($_REQUEST['keyword_text_1']) && $_REQUEST['keyword_text_1'] !="" ) {
                    $search_keyword_1 = trim($_REQUEST['keyword_text_1']);
                } else {
                    $search_keyword_2 = trim($_REQUEST['keyword_text_2']);
                }
            } else {
                $search_keyword_cat = false;
            }

            $arr_where = array();
            array_push($arr_where, "oa.address IS NOT NULL", "oa.address != 'NULL'", "oa.address !=''", "oa.country = 3409", "oa.verify >= 0");

            if ($non_pickup == 'true'){
                array_push($arr_where, "oa.area_type != 2 AND oa.address_type != 'locker'");
            }
            if ($pickup_only == 'true'){
                array_push($arr_where, "oa.address_type = 'locker'");
            }

            if (!empty($search_type_cat) && $search_type_cat > 0){
                array_push($arr_where, "oadt.address_dest_type_cat  = '".$search_type_cat."'");
            }
            if (!empty($search_keyword_cat) && $search_keyword_cat != -1){
                if (isset($keyword_operator) && !empty($keyword_operator) && isset($search_keyword_1) && !empty($search_keyword_1) && isset($search_keyword_2) && !empty($search_keyword_2)){
                    array_push($arr_where, "(oa.".$search_keyword_cat." LIKE '%".$search_keyword_1."%' ".$keyword_operator." oa.".$search_keyword_cat." LIKE '%".$search_keyword_2."%')");
                } else if (isset($search_keyword_1) && $search_keyword_1 !=""){
                    array_push($arr_where, "oa.".$search_keyword_cat." LIKE '%".$search_keyword_1."%'");
                } else if (isset($search_keyword_2) && $search_keyword_2 !=""){
                    array_push($arr_where, "oa.".$search_keyword_cat." LIKE '%".$search_keyword_2."%'");
                }
            }
            $arr_where = array();

        }

    } else if($_REQUEST['act'] == 'list'){
        $page_size = empty($_REQUEST['page_size']) ? 50 : intval($_REQUEST['page_size']);
        $page = empty($_REQUEST['page']) ? 1 : intval($_REQUEST['page']);
        //$addr_list_all = $addressController->get_address();
        //$addressController->address_paging($addr_list_all,$page_size,$page);
        if(empty($_REQUEST['ajax']) || $_REQUEST['ajax'] != "1")
            $smarty->assign('full_page', 1);
        $smarty->assign('act', $_REQUEST['act']);
        $smarty->assign('ur_here', $_LANG['022_insurance_service']);
        $weekday = date('w');
        if (isset($_REQUEST['start_date'])){
            $start_date = $_REQUEST['start_date'];
        } else if ($weekday == 1){
            $start_date = local_date('Y-m-d', local_strtotime('-2 days'));
        } else {
            $start_date = local_date('Y-m-d', local_strtotime('-1 days'));
        }
        if (isset($_REQUEST['end_date'])){
            $end_date = $_REQUEST['end_date'];
        } else {
            $end_date = local_date('Y-m-d', local_strtotime('today'));
        }
        $smarty->assign('start_date', $start_date);
        $smarty->assign('end_date', $end_date);
        $smarty->assign('date_today',  local_date('Y-m-d', local_strtotime('today')));
        $smarty->assign('7_date_before',  local_date('Y-m-d', local_strtotime('-7 days')));
        $smarty->assign('30_date_before',  local_date('Y-m-d', local_strtotime('-30 days')));
        $smarty->assign('non_pickup', '1');
        //$smarty->assign('action_link', array('text' => $_LANG['order_address_report'], 'href' => 'order_address.php?act=report'));

        $default_ins_list = array(array('insurance_id' => '-1', 'name' => '友和爆屏換新服務', 'name_en' => 'Yoho Screen Proctection Service', 'code' => 'screen_repair'), array('insurance_id' => '-1', 'name' => '友和延長保養服務', 'name_en' => 'Yoho Extended Warranty Service', 'code' => 'warranty_extend'));

        $ins_list = $insuranceController->getInsList();

        foreach ($default_ins_list as $key => $default_ins){
            foreach ($ins_list as $ins){
                if ($ins['code'] ==  $default_ins['code'] && !empty($ins['insurance_id'])){
                    $default_ins_list[$key]['insurance_id'] = $ins['insurance_id'];
                }
            }
        }

        $smarty->assign('ins_list', $default_ins_list);

        //$addressController->ajaxQueryAction($addr_list_all, 0, false);
        assign_query_info();

        $smarty->display('insurance_list.htm');
        
        
    } else if($_REQUEST['act'] == 'bought_list'){
        $ins_id = empty($_REQUEST['ins_id']) ? 0 : intval($_REQUEST['ins_id']);
        if (!$ins_id){
            sys_msg('未能找到有關保險服務', 0, array(array("href"=>"insurance.php?act=list", "text" => $_LANG['return_to_list'])));
        }
        $ins_info =  $insuranceController->getInsuranceInfo($ins_id);
        if (!$ins_info){
            sys_msg('未能找到有關保險服務', 0, array(array("href"=>"insurance.php?act=list", "text" => $_LANG['return_to_list'])));
        }
        
        $page_size = empty($_REQUEST['page_size']) ? 50 : intval($_REQUEST['page_size']);
        $page = empty($_REQUEST['page']) ? 1 : intval($_REQUEST['page']);
        if(empty($_REQUEST['ajax']) || $_REQUEST['ajax'] != "1")
        $smarty->assign('full_page', 1);
        $smarty->assign('act', $_REQUEST['act']);

        $smarty->assign('ur_here', $_LANG['022_insurance_service'].$_LANG['bought_count']." - ".$ins_info['name']);
        $smarty->assign('action_link', array('text' => $_LANG['return_to_list'], 'href' => 'insurance.php?act=list'));

        assign_query_info();

        $smarty->display('insurance_bought_list.htm');
        
        
    } else if($_REQUEST['act'] == 'claim_list'){
        $ins_id = empty($_REQUEST['ins_id']) ? 0 : intval($_REQUEST['ins_id']);
        if (!$ins_id){
            sys_msg('未能找到有關保險服務', 0, array(array("href"=>"insurance.php?act=list", "text" => $_LANG['return_to_list'])));
        }
        $ins_info =  $insuranceController->getInsuranceInfo($ins_id);
        if (!$ins_info){
            sys_msg('未能找到有關保險服務', 0, array(array("href"=>"insurance.php?act=list", "text" => $_LANG['return_to_list'])));
        }
        
        $page_size = empty($_REQUEST['page_size']) ? 50 : intval($_REQUEST['page_size']);
        $page = empty($_REQUEST['page']) ? 1 : intval($_REQUEST['page']);
        if(empty($_REQUEST['ajax']) || $_REQUEST['ajax'] != "1")
        $smarty->assign('full_page', 1);
        $smarty->assign('act', $_REQUEST['act']);

        $smarty->assign('ur_here', $_LANG['022_insurance_service'].$_LANG['claimed_count']." - ".$ins_info['name']);
        $smarty->assign('action_link', array('text' => $_LANG['return_to_list'], 'href' => 'insurance.php?act=list'));

        assign_query_info();

        $smarty->display('insurance_claim_list.htm');
        
        
    } else if($_REQUEST['act'] == 'add_ins'){
        //admin_priv('menu_022_insurance_service');
        $vaild_data = TRUE;
        if (isset($_REQUEST['insurance_id'])){
            $ins_id = $_REQUEST['insurance_id'];
        } else {
            $vaild_data = FALSE;
        }
        if (isset($_REQUEST['name']) && trim($_REQUEST['name']) != ''){
            $ins_name = trim($_REQUEST['name']);
        } else {
            $vaild_data = FALSE;
        }
        if ((isset($_REQUEST['name_en']) && trim($_REQUEST['name_en']) != '') || $ins_id){
            $ins_name_en = trim($_REQUEST['name_en']);
        } else {
            $vaild_data = FALSE;
        }
        if (isset($_REQUEST['code']) && trim($_REQUEST['code']) != ''){
            $ins_code = trim($_REQUEST['code']);
        } else {
            $vaild_data = FALSE;
        }
        //if (isset($_REQUEST['duration_options']) && is_array($_REQUEST['duration_options']) && sizeof($_REQUEST['duration_options']) == 1){
            $ins_duration = $_REQUEST['duration_options'];
        //} else {
            //$vaild_data = FALSE;
        //}
        if ($vaild_data){
            $ins_existed = FALSE;
            if ($ins_id > 0){
                $ins_existed = $insuranceController->checkInsExisted($ins_id, $ins_duration);
            }
            if (!$ins_existed){
                $add_status = $insuranceController->insertInsurance($ins_name, $ins_name_en, $ins_code, $ins_duration, $ins_id, FALSE);
                if ($add_status){
                    sys_msg($_LANG['created_ins'].$ins_name, 0, array(array("href"=>"insurance.php?act=list", "text" => $_LANG['return_to_list'])));
                } else {
                    sys_msg($_LANG['lack_info_cant_create_ins'], 0, array(array("href"=>"insurance.php?act=list", "text" => $_LANG['return_to_list'])));
                }
            } else {
                sys_msg($_LANG['same_ins_existed'], 0, array(array("href"=>"insurance.php?act=list", "text" => $_LANG['return_to_list'])));
            }
        } else {
            sys_msg($_LANG['lack_info_cant_create_ins'], 0, array(array("href"=>"insurance.php?act=list", "text" => $_LANG['return_to_list'])));
        }
        exit;

    } else if($_REQUEST['act'] == 'ins_edit'){
        //admin_priv('menu_022_insurance_service');
        if (!isset($_REQUEST['id']) || $_REQUEST['id'] <= 0){
            sys_msg('連結無效，欠保險服務ID', 0, array(array("href"=>"insurance.php?act=list", "text" => $_LANG['return_to_list'])));
        } else {
            $ins_id = $_REQUEST['id'];
        }
        if ($_REQUEST['tab'] == 'content'){
            $selected_status = 'content';
            exit;
        } else if ($_REQUEST['tab'] == 'info'){
            $selected_status = 'info';
            $ins_info =  $insuranceController->getInsuranceInfo($ins_id);
            if (!$ins_info){
                sys_msg('未能找到有關保險服務', 0, array(array("href"=>"insurance.php?act=list", "text" => $_LANG['return_to_list'])));
            }
            $ins_info['duration_options'] = explode(',', $ins_info['duration']);
            $insurance_duration = array('12' => 0, '24' => 0, '36' => 0, '48' => 0);
            if (in_array(12, $ins_info['duration_options'])){$insurance_duration['12'] = 1;}
            if (in_array(24, $ins_info['duration_options'])){$insurance_duration['24'] = 1;}
            if (in_array(36, $ins_info['duration_options'])){$insurance_duration['36'] = 1;}
            if (in_array(48, $ins_info['duration_options'])){$insurance_duration['48'] = 1;}
            $smarty->assign('insurance_duration', $insurance_duration);
            $smarty->assign('ins_info', $ins_info);
        } else {
            $selected_status = 'goods';
            $ins_info =  $insuranceController->getInsuranceInfo($ins_id);
            if (!$ins_info){
                sys_msg('未能找到有關保險服務', 0, array(array("href"=>"insurance.php?act=list", "text" => $_LANG['return_to_list'])));
            }
            $ins_info['duration_options'] = explode(',', $ins_info['duration']);
            $smarty->assign('ins_info', $ins_info);
        }
        $smarty->assign('selected_status', $selected_status);
        //$smarty->assign('addr_list', $addressController->get_address(array("order_address_id = " . $_REQUEST['address_id']), "", 1));
        $smarty->assign('full_page', 1);
        $smarty->assign('id', $_REQUEST['id']);    
        $smarty->assign('act', $_REQUEST['act']);
        $smarty->assign('ur_here', $_LANG['manage_ins']);
        $smarty->assign('action_link', array('text' => $_LANG['return_to_list'], 'href' => 'insurance.php?act=list'));

        $cats = cat_list(0, 0, false, 0, true, true);
        $smarty->assign('cats', $cats);
        $brands = get_brand_list();
        $smarty->assign('brands', $brands);
        //assign_query_info();
        $smarty->display('insurance_edit.htm');

    } else if($_REQUEST['act'] == 'ins_info_edit'){
        //admin_priv('menu_022_insurance_service');
        $vaild_data = TRUE;
        if (isset($_REQUEST['name']) && trim($_REQUEST['name']) != ''){
            $ins_name = trim($_REQUEST['name']);
        } else {
            $vaild_data = FALSE;
        }
        if (isset($_REQUEST['name_en']) && trim($_REQUEST['name_en']) != ''){
            $ins_name_en = trim($_REQUEST['name_en']);
        } else {
            $vaild_data = FALSE;
        }
        if (isset($_REQUEST['code']) && trim($_REQUEST['code']) != ''){
            $ins_code = trim($_REQUEST['code']);
        } else {
            $vaild_data = FALSE;
        }
        /*
        if (isset($_REQUEST['duration_options']) && !empty($_REQUEST['duration_options'])){
            $ins_duration = $_REQUEST['duration_options'];
        } else {
            $vaild_data = FALSE;
        }
        */
        if (!isset($_REQUEST['id']) || !($_REQUEST['id'])){
           $vaild_data = FALSE;
        }
        $arr_info = array('name' => $ins_name, 'code' => $ins_code, 'status' => $_REQUEST['status']);
        if ($vaild_data){
            $ins_name_cn = translate_to_simplified_chinese($ins_name);
            $arr_name_lang = array('en' => $ins_name_en, 'cn' => $ins_name_cn);
            $add_status = $insuranceController->editInsuranceInfo($_REQUEST['id'], $arr_info, $arr_name_lang);
            sys_msg($_LANG['edited_ins'].$ins_name, 0, array(array("href"=>"insurance.php?act=ins_edit&tab=info&id=".$_REQUEST['id'], "text" => $_LANG['return_to_ins']), array("href"=>"insurance.php?act=list", "text" => $_LANG['return_to_list'])));
        } else {
            sys_msg($_LANG['lack_info_cant_edit_ins'], 0, array(array("href"=>"insurance.php?act=list", "text" => $_LANG['return_to_list'])));
        }

    } else if ($_REQUEST['act'] == 'ajax_get_goods_by_keyword'){
        $arr_sql_where = array();
        if (!$_REQUEST['search_key'] || trim($_REQUEST['search_key']) == "" ){
            return FALSE;
        }
        $search_key = trim($_REQUEST['search_key']);
        $str_search_key = preg_replace(array('/,/', '/\s+/'), array('', ''), $search_key);
        if (is_numeric($str_search_key) && strpos($search_key, ',') !== FALSE){
            $arr_search_key = explode(',', $search_key);
            foreach ($arr_search_key as $keyword){
                if (trim($keyword) !=""){
                    array_push($arr_sql_where, "goods_id = '".$keyword."'");
                }
            }
            //echo 2222;
        } else {
            array_push($arr_sql_where, "goods_id LIKE '%".$search_key."%'", "goods_sn LIKE '%".$search_key."%'", "goods_name LIKE '%".$search_key."%'");
            //echo 1111;
        }
        $where_operator = " OR ";
        //echo "<br>".preg_replace('/[.,]/', '', $search_key);

        $goods = $insuranceController->getGoodsBykeyword($arr_sql_where, $where_operator);

        return make_json_result($goods);
    
    } else if ($_REQUEST['act'] == 'ajax_get_claim_record'){
        if (!$_REQUEST['rec_id'] || !$_REQUEST['order_id']){
            return FALSE;
        }

        $record = $insuranceController->getClaimRecord($_REQUEST['order_id'], $_REQUEST['rec_id']);

        if (!empty($record)){
            foreach ($record as $key => $claim){
                $record[$key]['claim_datetime'] = local_date($_CFG['time_format'], $record[$key]['claim_datetime']);
                $record[$key]['claim_amount'] = $record[$key]['claim_amount'] + 0;
            }
        }

        return make_json_result($record);
    
    } else if ($_REQUEST['act'] == 'insNoItem'){
        if (!empty($_REQUEST['id']) ){
            $no_item = $insuranceController->countInsGood($_REQUEST['id']);
            return make_json_result($no_item);
        }
        exit;
    
    } else if ($_REQUEST['act'] == 'insAddItem'){
        if (!empty($_REQUEST['id']) && !empty($_REQUEST['items']) && is_array($_REQUEST['items']) && !empty($_REQUEST['duration_percent']) && is_array($_REQUEST['duration_percent'])){
            $arr_duration_items = [];
            $arr_existed_duration_items = [];
            $price_percent = [];
            foreach ($_REQUEST['duration_percent'] as $duration_percent){
                $price_percent[$duration_percent['duration']] = $duration_percent['percent'];
                $existed = $insuranceController->getExistedInsGoods($_REQUEST['id'], $duration_percent['duration'], $_REQUEST['items']);
                //dd($existed);
                if (!$existed){
                    $new_items = $_REQUEST['items'];
                } else {
                    $arr_existed_duration_items[$duration_percent['duration']] = array();
                    foreach ($existed as $item){
                        array_push($arr_existed_duration_items[$duration_percent['duration']], $item['goods_id']);
                    }
                    $new_items = array_diff($_REQUEST['items'], $arr_existed_duration_items[$duration_percent['duration']]);
                }
                //$new_items_prices = $insuranceController->getGoodsPrice($new_items);
                if (!empty($new_items)){
                    $arr_duration_items[$duration_percent['duration']] = array();
                    $arr_duration_items[$duration_percent['duration']] = $new_items;
                }
                //$arr_duration_items[$duration] = $_REQUEST['items'];
            }
            //dd($arr_existed_duration_items);
            //dd($arr_duration_items);
            //$goods = $insuranceController->getGoodsBykeyword($arr_sql_where, $where_operator);
            if (!empty($arr_duration_items)){
                $insuranceController->addInsGoods($_REQUEST['id'], $price_percent, $arr_duration_items);
            }
            return make_json_result(array('added' => $arr_duration_items, 'existed' => $arr_existed_duration_items));
        }
        exit;
    
    } else if ($_REQUEST['act'] == 'insDeleteItem'){
        //admin_priv('menu_022_insurance_service');
        if (!empty($_REQUEST['id']) && !empty($_REQUEST['good_id'])){
            $res = $insuranceController->deleteInsGoods($_REQUEST['id'], $_REQUEST['good_id']);
            if ($res){
                make_json_result('true');
            } else {
                make_json_result('false');
            }
        }
        exit;
        
    } else if ($_REQUEST['act'] == 'insDeleteAllItem'){
        //admin_priv('menu_022_insurance_service');
        if (!empty($_REQUEST['id'])){
            $res = $insuranceController->deleteInsAllGoods($_REQUEST['id']);
            if ($res){
                make_json_result('true');
                return ;
            } else {
                make_json_result('false');
            }
        }
        exit;
        
    } elseif ($_REQUEST['act'] == 'ajaxEdit') {

        $_REQUEST['value'] = trim($_REQUEST['value']);
        $inpuut_val = $_REQUEST['value'];
        if ($_REQUEST['value'] == "N/A" || $_REQUEST['value'] == "-"){
            $_REQUEST['value'] = "";
        }
        
        $return_val = "";
        $ins_good_id = "";
        if ($_REQUEST['id'] != ""){
            if (strpos($_REQUEST['col'], 'duration_') !== false) {
                $arr_col = explode('_', $_REQUEST['col']);
                if ($arr_col[0] == 'duration' && !empty($arr_col[1])){
                    $duration = $arr_col[1];
                    $ins_good_id = $insuranceController->getInsGoodIdByInsGoodId($_REQUEST['id'], $duration);
                    if (empty($ins_good_id)){
                        $add_res = $insuranceController->addNewInsGood($_REQUEST['id'], $duration, $_REQUEST['value']);
                        if ($add_res){
                            make_json_result($inpuut_val);
                        } else {
                            make_json_result('ERROR');
                        }
                    } else {
                        $_REQUEST['id'] = $ins_good_id;
                        $_REQUEST['col'] = 'price_percent';
                        $insuranceController->insuranceEditAction('insurance_goods');
                        return $inpuut_val;
                        //$check_res = $insuranceController->checkInsuranceEditAction($target_table);
                    }
                }

            } else{
                $insuranceController->insuranceEditAction($target_table);
            }
        } else {
            return $inpuut_val;
        }
        /*
        $return_val = $_REQUEST['value'];

        $return_val = str_replace("\\", "", $return_val);
        if ($return_val == ""){
            $return_val = "N/A";
        }
        make_json_result($return_val);
        */
    }
}
?>