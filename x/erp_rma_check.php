<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_base.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_warehouse.php');
require(dirname(__FILE__) .'/includes/ERP/cls/cls_date.php');
$rmaController = new Yoho\cms\Controller\RmaController();
$adminuserController = new Yoho\cms\Controller\AdminuserController();

if ($_REQUEST['act'] == 'list' || $_REQUEST['act'] == '') {
    $rma_id = $_REQUEST['rma_id'];
    $rmaController->checkStatusRedirect($rma_id,'check');

    $step = 1;
    //$rmaController->checkStatusRedirect($stocktake_id,'process');
    $person_in_charge_option = $rmaController->personInChargeOption(true,'SaleTeam');

    //$result = $stocktakeController->detail();
    //$goods_status_options = $stocktakeController->getGoodsStatusOptions('process');
    $rma_basic_info = $rmaController->getRmaInfo($rma_id);

    // echo '<pre>';
    // print_r($rma_basic_info);
    // exit;

    $action_log = $rmaController->getActionLog($rma_id);

    if (empty($rma_basic_info['person_in_charge'])) {
        $rmaController->updatePersonInCharge($rma_id,64);
    }

    // get repair centers;
    $repair_center_warehouse_list = $erpController->getWarehouseList(0, -1, -1, -1,1);
    // echo '<pre>';
    // print_r($repair_center_warehouse_list);
    // exit;
    $repair_center_warehouse_ids = $rmaController->getRepairCenterWarehouseIds();

    // $is_not_from_repair_center = 0;
    // if (!in_array($rma_basic_info['warehouse_id'],$repair_center_warehouse_ids)) {
    //     $is_not_from_repair_center = 1;
    // }
    // 
    // if (in_array($rma_basic_info['repair_warehouse_id'],$repair_center_warehouse_ids)) {
    //     $is_not_from_repair_center = 0;
    // }


    //$rma_basic_info['person_in_charge'] = 56;
    // echo '<pre>';
    // print_r($rma_basic_info);
    // exit;
    //$smarty->assign('goods_status_options',$goods_status_options);
    $smarty->assign('repair_center_warehouse_list',$repair_center_warehouse_list);
    $smarty->assign('repair_center_warehouse_ids',$repair_center_warehouse_ids);
    $smarty->assign('action_log',$action_log);
    $smarty->assign('person_in_charge_option',$person_in_charge_option);
    $smarty->assign('rma_basic_info',$rma_basic_info);
    $smarty->assign('channel',$rma_basic_info['channel']);
    $smarty->assign('rma_id',$rma_id);
    $smarty->assign('step',$step);
    $smarty->assign('full_page', 1);
    $smarty->assign('cfg_lang', $_CFG['lang']);
    $smarty->assign('lang', $_LANG);
    $smarty->assign('action_link', array('text' => 'RMA列表', 'href' => 'erp_rma.php'));
    assign_query_info();
    $smarty->display('erp_rma_check.htm');

} else if ($_REQUEST['act'] == 'post_rma_check_submit') {
    $result = $rmaController->rmaCheckSubmit();
    
    if (isset($result['error'])) {
        make_json_error($result['error']);
    }else{
        make_json_result($result);
    }

} else if ($_REQUEST['act'] == 'post_rma_cancel_transfer_submit') {
    $result = $rmaController->rmaCancelRepairTransferSubmit();
    
    if (isset($result['error'])) {
        make_json_error($result['error']);
    }else{
        make_json_result($result);
    }

} else if ($_REQUEST['act'] == 'post_rma_save_note') {
    $result = $rmaController->rmaSaveNote();
    if($result !== false){
        make_json_result($result);
    }else{
        make_json_error('更新時發生錯誤');
    }
} elseif ($_REQUEST['act'] == 'query') {
  	$result = $rmaController->getRmaStockInOutLog();
    $rmaController->ajaxQueryAction($result['data'],$result['record_count'],false);
}
?>