<?php

define('IN_ECS', true);

require dirname(__FILE__) . '/includes/init.php';

$fraudController = new Yoho\cms\Controller\FraudController();
// $fraudController->calculateOverallRiskLevel("39257");

// 39454: stripe verify 3ds
// 39455: stripe not verify 3ds
// 39456: stripe not enable 3ds

// 39257: braintree
// 39256: paypal
if ($_REQUEST['act'] == 'list') {
    admin_priv("order_fraud_detail");
    $smarty->assign("risk_cats", $fraudController->getHighRiskCategory(true));
    $smarty->assign("order", $_REQUEST['order']);
    $smarty->assign("txn", $_REQUEST['txn']);
    $smarty->display("fraud_control.htm");
} elseif ($_REQUEST['act'] == 'query') {
    $order_id = 0;
    if (!empty($_REQUEST["order"])) {
        $order = trim($_REQUEST["order"]);
        $orders = $db->getAll("SELECT order_id, order_sn FROM " . $ecs->table("order_info") . " WHERE order_id = $order OR order_sn LIKE '%$order%'");
    } elseif (!empty($_REQUEST["txn"])) {
        $txn = trim($_REQUEST["txn"]);
        $orders = $db->getAll("SELECT of.order_id, order_sn FROM " . $ecs->table("order_fraud") . " of LEFT JOIN " . $ecs->table("order_info") . " oi ON oi.order_id = of.order_id WHERE txn LIKE '%$txn%'");
    }
    if (count($orders) == 1) {
        $order_id = $orders[0]['order_id'];
    } else {
        make_json_response(
            ['orders' => $orders]
        );
    }
    if (!empty($order_id)) {
        $risk = $fraudController->calculateOverallRiskLevel($order_id);
        make_json_response(
            [
                'risk' => $risk,
                'order_id' => $order_id
            ]
        );
    }
    make_json_error("找不到任何結果");
}
?>