<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_base.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_warehouse.php');
require(dirname(__FILE__) .'/includes/ERP/cls/cls_date.php');
$stocktakeController = new Yoho\cms\Controller\StocktakeController();
$adminuserController = new Yoho\cms\Controller\AdminuserController();

if ($_REQUEST['act'] == 'list' || $_REQUEST['act'] == '') {

    if( admin_priv('erp_warehouse_approve','',false) ) {
        $erp_warehouse_approve = true;
    } else {
        $erp_warehouse_approve = false;
    }

    $stocktake_id = $_REQUEST['stocktake_id'];
    $stocktakeController->checkStatusRedirect($stocktake_id,'review');
    //$result = $stocktakeController->detail('review');
    $stocktake_status_options = $stocktakeController->getStocktakeStatusOptions();

    $stocktake_person_option = $stocktakeController->stocktakePersonOption();
    $goods_status_options = $stocktakeController->getGoodsStatusOptions('review');
    $stocktake_person_list = $stocktakeController->stocktakePersonList();
    $stocktake = $stocktakeController->getStocktakeInfo($stocktake_id);
    
    $smarty->assign('erp_warehouse_approve',$erp_warehouse_approve);
    $smarty->assign('goods_status_options',$goods_status_options);
    $smarty->assign('stocktake_person_list',$stocktake_person_list);
    $smarty->assign('stocktake_person_option',$stocktake_person_option);
    $smarty->assign('stocktake_status_options',$stocktake_status_options);
    $smarty->assign('stocktake',$stocktake);
    $smarty->assign('stocktake_id',$_REQUEST['stocktake_id']);
    $smarty->assign('warehouse_name_list',$result['warehouse_name_list']);
    $smarty->assign('action_link', array('text' => '盤點工作清單', 'href' => 'erp_stocktake.php'));
    $smarty->assign('full_page', 1);
    $smarty->assign('cfg_lang', $_CFG['lang']);
    $smarty->assign('lang', $_LANG);
    //	$smarty->assign('action_link', array('text' => '盤點工作清單', 'href' => 'erp_stocktake.php'));
    assign_query_info();
    $smarty->display('erp_stocktake_review.htm');
} else if ($_REQUEST['act'] == 'post_submit_review_finished') {
    $result = $stocktakeController->submit_review_finished();
    if(isset($result['error'])){
        make_json_error($result['error']);	
    }else{
        make_json_result($result);
    }
} else if ($_REQUEST['act'] == 'post_stocktake_basic_info') {
    $result = $stocktakeController->updateStocktakeBasicInfo();
    if($result !== false){
        make_json_result($result);
    }else{
        make_json_error('更新時發生錯誤');
    }
} else if ($_REQUEST['act'] == 'post_bulk_stocktake_goods_status_change') {
    $result = $stocktakeController->bulkStocktakeGoodsStatusChange();
    if($result !== false){
        make_json_result($result);
    }else{
        make_json_error('更新時發生錯誤');
    }

} else if ($_REQUEST['act'] == 'post_confirm_adjust_stock') {
    $result = $stocktakeController->updateConfirmAdjustStock();
    if($result !== false){
        make_json_result($result);
    }else{
        make_json_error('更新時發生錯誤');
    }
} else if ($_REQUEST['act'] == 'post_stocktake_goods_status_change') {
    $result = $stocktakeController->stocktakeGoodsStatusChange();
    if($result !== false){
        make_json_result($result);
    }else{
        make_json_error('更新時發生錯誤');
    }
// } else if ($_REQUEST['act'] == 'post_stocktake_restocktake') {
//     $result = $stocktakeController->updateStocktakeRestocktake();
//     if($result !== false){
//         make_json_result($result);
//     }else{
//         make_json_error('更新時發生錯誤');
//     }
// }else if ($_REQUEST['act'] == 'post_stocktake_reset') {
//     $result = $stocktakeController->updateStocktakeReset();
//     if($result !== false){
//         make_json_result($result);
//     }else{
//         make_json_error('更新時發生錯誤');
//     }
}elseif ($_REQUEST['act'] == 'query') {
  	$result = $stocktakeController->detail('review');
    $stocktakeController->ajaxQueryAction($result['data'],$result['record_count'],false);
}
?>