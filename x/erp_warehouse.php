<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_base.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_warehouse.php');
$erpController = new Yoho\cms\Controller\ErpController();
if($_REQUEST['act'] == 'list')
{
	if((admin_priv('erp_sys_manage','',false)))
	{
		include('./includes/ERP/page.class.php');
		$page=isset($_REQUEST['page']) &&intval($_REQUEST['page']) >0 ?intval($_REQUEST['page']):1;
		$num_per_page=20;
		$mode=1;
		$page_bar_num=6;
		$page_style="page_style";
		$current_page_style="current_page_style";
		$start=$num_per_page*($page-1);
		$total_num=get_warehouse_count();
		$warehouse_list=get_warehouse_list(0,0,-1,$start,$num_per_page);
		$smarty->assign('warehouse_list',$warehouse_list);
		$url=fix_url($_SERVER['REQUEST_URI'],array('page'=>''));
		$pager=new page(array('total_data'=>$total_num,'data_per_page'=>$num_per_page,'url'=>$url,'mode'=>$mode,'page_bar_num'=>$page_bar_num,'page_style'=>$page_style,'current_page_style'=>$current_page_style));
		$smarty->assign('pager',$pager->show());
		$action_link = array('href'=>'erp_warehouse.php?act=add','text'=>$_LANG['erp_add_warehouse']);
		$smarty->assign('action_link',$action_link);
		$smarty->assign('url',$_SERVER['REQUEST_URI']);
		$smarty->assign('ur_here',$GLOBALS['_LANG']['erp_warehouse_list']);
		assign_query_info();
		$smarty->display('erp_warehouse_list.htm');
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif($_REQUEST['act'] == 'change_is_valid')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_sys_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	if(empty($_REQUEST['warehouse_id']))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	else
	{
		$warehouse_id=intval($_REQUEST['warehouse_id']);
		$sql="select is_valid from ".$ecs->table('erp_warehouse')." where warehouse_id='".$warehouse_id."'";
		$is_valid=$db->getOne($sql);
		if($is_valid==1)
		{
			$result['error']=0;
			$result['is_valid']=0;
			$sql="update ".$ecs->table('erp_warehouse')." set is_valid='0' where warehouse_id='".$warehouse_id."'";
			$db->query($sql);
		}
		else
		{
			$result['error']=0;
			$result['is_valid']=1;
			$sql="update ".$ecs->table('erp_warehouse')." set is_valid='1' where warehouse_id='".$warehouse_id."'";
			$db->query($sql);
		}
		$erpController->clearWarehouseCache();
		die($json->encode($result));
	}
}
elseif($_REQUEST['act'] == 'change_is_on_sale')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_sys_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	if(empty($_REQUEST['warehouse_id']))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	else
	{
		$warehouse_id=intval($_REQUEST['warehouse_id']);
		$sql="select is_on_sale from ".$ecs->table('erp_warehouse')." where warehouse_id='".$warehouse_id."'";
		$is_on_sale=$db->getOne($sql);
		if($is_on_sale==1)
		{
			$result['error']=0;
			$result['is_on_sale']=0;
			$sql="update ".$ecs->table('erp_warehouse')." set is_on_sale='0' where warehouse_id='".$warehouse_id."'";
			$db->query($sql);
		}
		else
		{
			$result['error']=0;
			$result['is_on_sale']=1;
			$sql="update ".$ecs->table('erp_warehouse')." set is_on_sale='1' where warehouse_id='".$warehouse_id."'";
			$db->query($sql);
		}
		$erpController->clearWarehouseCache();
		die($json->encode($result));
	}
}
elseif($_REQUEST['act'] == 'remove')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_sys_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	if(empty($_REQUEST['warehouse_id']))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	else
	{
		$warehouse_id=intval($_REQUEST['warehouse_id']);
		$warehouse_info=is_warehouse_exist($warehouse_id);
		if($warehouse_info!==false)
		{
			$sql="select count(*) as warehousing_number from ".$ecs->table('erp_warehousing')." where warehouse_id='".$warehouse_id."'";
			$warehousing_number=$db->getOne($sql);
			if($warehousing_number>0)
			{
				$result['error']=2;
				$result['message']=$_LANG['erp_warehouse_has_warehousing'];
				die($json->encode($result));
			}
			$sql="select count(*) as delivery_number from ".$ecs->table('erp_delivery')." where warehouse_id='".$warehouse_id."'";
			$delivery_number=$db->getOne($sql);
			if($delivery_number>0)
			{
				$result['error']=2;
				$result['message']=$_LANG['erp_warehouse_has_delivery'];
				die($json->encode($result));
			}
			$sql="select attr_id from ".$ecs->table('erp_goods_attr_stock')." where warehouse_id='".$warehouse_id."' and goods_qty>0 limit 1";
			$attr_id=$db->getOne($sql);
			if(!empty($attr_id))
			{
				$result['error']=2;
				$result['message']=$_LANG['erp_warehouse_has_stock'];
				die($json->encode($result));
			}
			$sql="delete from ".$ecs->table('erp_warehouse')." where warehouse_id='".$warehouse_id."'";
			if($db->query($sql))
			{
				$sql="delete from ".$ecs->table('erp_goods_attr_stock')." where warehouse_id='".$warehouse_id."'";
				$db->query($sql);
				$result['error']=0;
				$erpController->clearWarehouseCache();
				die($json->encode($result));
			}
		}
		else
		{
			$result['error']=3;
			$result['message']=$_LANG['erp_warehouse_not_exist'];
			die($json->encode($result));
		}
	}
}
elseif($_REQUEST['act'] == 'add')
{
	if((admin_priv('erp_sys_manage','',false)))
	{
		$smarty->assign('act','insert');
		$smarty->assign('admin_list',admin_list());
		$smarty->assign('ur_here',$GLOBALS['_LANG']['erp_add_warehouse']);
		$smarty->assign('agency_list',agency_list());
		assign_query_info();
		$smarty->display('erp_warehouse_info.htm');
	}
	else
	{
		$href="erp_warehouse.php?act=list";
		$text=$_LANG['erp_warehouse_return'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif($_REQUEST['act'] == 'insert')
{
	if((admin_priv('erp_sys_manage','',false)))
	{
		$warehouse_name=isset($_REQUEST['name']) ?trim($_REQUEST['name']) : '';
		$warehouse_description=isset($_REQUEST['description']) ?trim($_REQUEST['description']) : '';
		$is_valid=isset($_REQUEST['is_valid']) &&intval($_REQUEST['is_valid'])>0 ?intval($_REQUEST['is_valid']) : 0;
		$is_on_sale=isset($_REQUEST['is_on_sale']) &&intval($_REQUEST['is_on_sale'])>0 ?intval($_REQUEST['is_on_sale']) : 0;
		$agency_id=isset($_REQUEST['agency_id']) &&intval($_REQUEST['agency_id'])>0 ?intval($_REQUEST['agency_id']) : 0;
		if(empty($warehouse_name))
		{
			$href="erp_warehouse.php?act=list";
			$text=$_LANG['erp_warehouse_return'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_wrong_parameter'],0,$link);
		}
		$row=array(
			'name'=>$warehouse_name,
			'description'=>$warehouse_description,
			'is_valid'=>$is_valid,
			'is_on_sale'=>$is_on_sale,
			'agency_id'=>$agency_id
		);
		$GLOBALS['db']->autoExecute($GLOBALS['ecs']->table('erp_warehouse'),$row,'INSERT');
		$erpController->clearWarehouseCache();
		$warehouse_id=$GLOBALS['db']->insert_id();
		if(isset($_REQUEST['admins']))
		{
			foreach($_REQUEST['admins'] as $key =>$admin_id)
			{
				$admin_id=intval($admin_id);
				if($admin_id >0)
				{
					$sql="insert into ".$GLOBALS['ecs']->table('erp_warehouse_admin')." set warehouse_id='".$warehouse_id."',admin_id='".$admin_id."'";
					$GLOBALS['db']->query($sql);
				}
			}
		}
		$sql="select attr_id,goods_id from ".$GLOBALS['ecs']->table('erp_goods_attr')." where 1";
		$res=$GLOBALS['db']->query($sql);
		while($row=mysql_fetch_assoc($res))
		{
			$sql="select attr_id,goods_id from ".$GLOBALS['ecs']->table('erp_goods_attr_stock')." where attr_id='".$row['attr_id']."' and goods_id='".$row['goods_id']."' and warehouse_id='".$warehouse_id."'";
			$record=$GLOBALS['db']->getRow($sql);
			if(empty($record))
			{
				$sql="insert into ".$GLOBALS['ecs']->table('erp_goods_attr_stock')." set attr_id='".$row['attr_id']."',goods_id='".$row['goods_id']."',warehouse_id='".$warehouse_id."'";
				$GLOBALS['db']->query($sql);
			}
		}
		$href="erp_warehouse.php?act=list";
		$text=$_LANG['erp_warehouse_return'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_warehouse_add_success'],0,$link);
	}
	else
	{
		$href="erp_warehouse.php?act=list";
		$text=$_LANG['erp_warehouse_return'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif($_REQUEST['act'] == 'edit')
{
	if((admin_priv('erp_sys_manage','',false)))
	{
		$warehouse_id=isset($_REQUEST['id']) &&intval($_REQUEST['id'])>0 ?intval($_REQUEST['id']):0;
		if(empty($warehouse_id))
		{
			$href="erp_warehouse.php?act=list";
			$text=$_LANG['erp_warehouse_return'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_wrong_parameter'],0,$link);
		}
		$warehouse_info=warehouse_info($warehouse_id);
		$sql="select admin_id from ".$GLOBALS['ecs']->table('erp_warehouse_admin')." where warehouse_id='".$warehouse_id."'";
		$res=$GLOBALS['db']->query($sql);
		while($row=mysql_fetch_assoc($res))
		{
			$admin_ids[]=$row['admin_id'];
		}
		$smarty->assign('admin_list',admin_list($admin_ids));
		$smarty->assign('agency_list',agency_list(array($warehouse_info['agency_id'])));
		$smarty->assign('warehouse_info',$warehouse_info);
		$smarty->assign('ur_here',$GLOBALS['_LANG']['erp_edit_warehouse']);
		$smarty->assign('act','update');
		assign_query_info();
		$smarty->display('erp_warehouse_info.htm');
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif($_REQUEST['act'] == 'update')
{
	if((admin_priv('erp_sys_manage','',false)))
	{
		$warehouse_id=isset($_REQUEST['warehouse_id']) &&intval($_REQUEST['warehouse_id'])>0 ?intval($_REQUEST['warehouse_id']) : 0;
		$warehouse_name=isset($_REQUEST['name']) ?trim($_REQUEST['name']) : '';
		$warehouse_description=isset($_REQUEST['description']) ?trim($_REQUEST['description']) : '';
		$is_valid=isset($_REQUEST['is_valid']) &&intval($_REQUEST['is_valid'])>0 ?intval($_REQUEST['is_valid']) : 0;
		$is_on_sale=isset($_REQUEST['is_on_sale']) &&intval($_REQUEST['is_on_sale'])>0 ?intval($_REQUEST['is_on_sale']) : 0;
		$agency_id=isset($_REQUEST['agency_id']) &&intval($_REQUEST['agency_id'])>0 ?intval($_REQUEST['agency_id']) : 0;
		if($warehouse_id == 0 ||($warehouse_id >1 &&empty($warehouse_name)))
		{
			$href="erp_warehouse.php?act=list";
			$text=$_LANG['erp_warehouse_return'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_wrong_parameter'],0,$link);
		}
		$row=array(
			'description'=>$warehouse_description,
			'is_valid'=>$is_valid,
			'is_on_sale'=>$is_on_sale,
			'agency_id'=>$agency_id
		);
		if($warehouse_id >1)
		{
			$row['name']=$warehouse_name;
		}
		$GLOBALS['db']->autoExecute($GLOBALS['ecs']->table('erp_warehouse'),$row,'UPDATE','warehouse_id='.$warehouse_id);
		$erpController->clearWarehouseCache();
		$sql="delete from ".$GLOBALS['ecs']->table('erp_warehouse_admin')." where warehouse_id='".$warehouse_id."'";
		$GLOBALS['db']->query($sql);
		if(isset($_REQUEST['admins']))
		{
			foreach($_REQUEST['admins'] as $key =>$admin_id)
			{
				$admin_id=intval($admin_id);
				if($admin_id >0)
				{
					$sql="insert into ".$GLOBALS['ecs']->table('erp_warehouse_admin')." set warehouse_id='".$warehouse_id."',admin_id='".$admin_id."'";
					$GLOBALS['db']->query($sql);
				}
			}
		}
		$href="erp_warehouse.php?act=list";
		$text=$_LANG['erp_warehouse_return'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_warehouse_edit_success'],0,$link);
	}
	else
	{
		$href="erp_warehouse.php?act=list";
		$text=$_LANG['erp_warehouse_return'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
?>
