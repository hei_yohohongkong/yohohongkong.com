<?php

/**
 * ECSHOP 用户评论管理程序
 * ============================================================================
 * 版权所有 2005-2010 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liuhui $
 * $Id: comment_manage.php 17123 2010-04-22 07:28:54Z liuhui $
*/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

require_once(ROOT_PATH . 'languages/'. $_CFG['lang'] .'/admin/comment_manage.php');
$goodsController  = new Yoho\cms\Controller\GoodsController();
$adminUserController = new Yoho\cms\Controller\AdminuserController();
$exc   = new exchange($ecs->table("goods_dai"), $db, 'id', 'content');
/* act操作项的初始化 */
if (empty($_REQUEST['act']))
{
    $_REQUEST['act'] = 'list';
}
else
{
    $_REQUEST['act'] = trim($_REQUEST['act']);
}

/*------------------------------------------------------ */
//-- 获取没有回复的评论列表
/*------------------------------------------------------ */
if ($_REQUEST['act'] == 'list')
{
    /* 检查权限 */
    admin_priv('booking');

    $smarty->assign('ur_here',      "缺貨登記");

    $smarty->assign('default_language', default_language());
    $smarty->assign('language_list', available_language_names());
    
    $adminuserJson=[
        Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_OUTOFSTOCK_FIRSTOP=>[],
        Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_OUTOFSTOCK_RECOP=>[]
        ];

    $adminuserJson[Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_OUTOFSTOCK_FIRSTOP] = $adminUserController->getAdminsByPriv(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_OUTOFSTOCK_FIRSTOP, 'name', ['user_name' => 'value']);
    $adminuserJson[Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_OUTOFSTOCK_RECOP] = $adminUserController->getAdminsByPriv(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_OUTOFSTOCK_RECOP, 'name', ['user_name' => 'value']);
    array_unshift($adminuserJson[Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_OUTOFSTOCK_FIRSTOP], array('name'=>'請選擇...','value'=>''));
    $smarty->assign('salespeople_firstop_json', json_encode($adminuserJson[Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_OUTOFSTOCK_FIRSTOP]));

    array_unshift($adminuserJson[Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_OUTOFSTOCK_RECOP], array('name'=>'請選擇...','value'=>''));
    $smarty->assign('salespeople_recop_json', json_encode($adminuserJson[Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_OUTOFSTOCK_RECOP]));
    
    
    $smarty->assign('content_json', json_encode(array(
        array('name'=>'未回覆','value'=>'未回覆'),
        array('name'=>'已回覆到貨期','value'=>'已回覆到貨期'),
        // array('name'=>'已回覆停產','value'=>'已回覆停產'),
        // array('name'=>'已回覆暫沒貨期','value'=>'已回覆暫沒貨期'),
        array('name'=>'暫時沒有貨期，到貨會再通知','value'=>'暫時沒有貨期，到貨會再通知'),
        array('name'=>'暫時沒有貨期，並推介相關產品','value'=>'暫時沒有貨期，並推介相關產品'),
        array('name'=>'已停產，並推介相關產品','value'=>'已停產，並推介相關產品'),
    )));
    
    $smarty->assign('content2_json', json_encode(array(
        array('name'=>'不適用','value'=>'不適用'),
        array('name'=>'已通知，客買','value'=>'已通知，客買'),
        array('name'=>'已通知，客不買','value'=>'已通知，客不買'),
        array('name'=>'已通知，客沒覆','value'=>'已通知，客沒覆'),
        array('name'=>'已通知，沒有貨','value'=>'已通知，沒有貨'),
    )));

    assign_query_info();
    $smarty->display('goods_dai_list.htm');
}

/*------------------------------------------------------ */
//-- 翻页、搜索、排序
/*------------------------------------------------------ */
if ($_REQUEST['act'] == 'query')
{
    $goodsController->goodsDaiAjaxQueryAction();
}

// 封存
elseif ($_REQUEST['act'] == 'remove')
{
    check_authz_json('comment_priv');
    
    $id = intval($_GET['id']);
    
    //$sql = "DELETE FROM " .$ecs->table('goods_dai'). " WHERE id = '$id'";
    $sql = "UPDATE " .$ecs->table('goods_dai'). " SET is_deleted = 1 WHERE id = '$id'";
    $res = $db->query($sql);
    
    $url = 'goods_dai.php?act=list&' . str_replace('act=remove', '', $_SERVER['QUERY_STRING']);
    
    ecs_header("Location: $url\n");
    exit;
}
// 還原
elseif ($_REQUEST['act'] == 'recover')
{
    check_authz_json('comment_priv');
    
    $id = intval($_GET['id']);
    
    //$sql = "DELETE FROM " .$ecs->table('goods_dai'). " WHERE id = '$id'";
    $sql = "UPDATE " .$ecs->table('goods_dai'). " SET is_deleted = 0 WHERE id = '$id'";
    $res = $db->query($sql);
    
    $url = 'goods_dai.php?act=list&' . str_replace('act=recover', '', $_SERVER['QUERY_STRING']);
    
    ecs_header("Location: $url\n");
    exit;
}

if ($_REQUEST['act'] == 'ajaxEdit')
{
    check_authz_json('comment_priv');
    if($_REQUEST['col'] == 'content') {
        $db->query('UPDATE '.$ecs->table('goods_dai').' SET update_time ='.gmtime().' WHERE id = '.$_REQUEST['id']);
    } else if($_REQUEST['col'] == 'content2') {
        $db->query('UPDATE '.$ecs->table('goods_dai').' SET update_time2 ='.gmtime().' WHERE id = '.$_REQUEST['id']);
    }
    $goodsController->ajaxEditAction('goods_dai');
}

/*------------------------------------------------------ */
//-- 批量删除用户评论
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'batch')
{
    admin_priv('booking');
    $action = isset($_POST['sel_action']) ? trim($_POST['sel_action']) : 'remove';

    if (isset($_POST['id']))
    {
        switch ($action)
        {
            case 'remove':
                // $db->query("DELETE FROM " . $ecs->table('comment') . " WHERE " . db_create_in($_POST['checkboxes'], 'comment_id'));
                // $db->query("DELETE FROM " . $ecs->table('comment') . " WHERE " . db_create_in($_POST['checkboxes'], 'parent_id'));
                $db->query("UPDATE " . $ecs->table('goods_dai') . " SET is_deleted = 1 WHERE " . db_create_in($_POST['id'], 'id'));
                break;

           case 'allow' :
               $db->query("UPDATE " . $ecs->table('comment') . " SET status = 1  WHERE " . db_create_in($_POST['id'], 'comment_id'));
               break;

           case 'deny' :
               $db->query("UPDATE " . $ecs->table('comment') . " SET status = 0  WHERE " . db_create_in($_POST['id'], 'comment_id'));
               break;

           default :
               break;
        }

        clear_cache_files();
        $action = ($action == 'remove') ? 'remove' : 'edit';
        admin_log('', $action, 'adminlog');

        $link[] = array('text' => $_LANG['back_list'], 'href' => 'goods_dai.php?act=list');
        sys_msg(sprintf($_LANG['batch_drop_success'], count(explode(",", $_POST['id']))), 0, $link);
    }
    else
    {
        /* 提示信息 */
        $link[] = array('text' => $_LANG['back_list'], 'href' => 'goods_dai.php?act=list');
        sys_msg($_LANG['no_select_comment'], 0, $link);
    }
}

/*------------------------------------------------------ */
//-- 發送電郵
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'email_reply')
{
    check_authz_json('comment_priv');

    $id = empty($_REQUEST['id']) ? 0 : intval($_REQUEST['id']);
    $mode = empty($_REQUEST['val']) ? '' : trim($_REQUEST['val']);
    
    if (empty($id))
    {
        make_json_error('missing id');
    }
    
    if (!in_array($mode, array('received', 'preorder', 'available')))
    {
        make_json_error('wrong mode');
    }
    
    $sql = "SELECT goods_id, pre_name, pre_im, pre_value, language FROM " . $ecs->table('goods_dai') . " WHERE `id` = '" . $id . "'";
    $res = $db->getRow($sql);
    
    if (empty($res))
    {
        make_json_error('wrong id');
    }
    
    if ($res['pre_im'] != 'email')
    {
        make_json_error('該缺貨登記的聯繫方式並不是Email');
    }
    
    if (empty($res['language']) || !in_array($res['language'], available_languages()))
    {
        $res['language'] = default_language();
    }
    
    $name = $res['pre_name'];
    $email = $res['pre_value'];
    
    $sql = "SELECT IFNULL(gl.goods_name, g.goods_name) as goods_name " .
            "FROM " . $ecs->table('goods') . " as g " .
            "LEFT JOIN " . $ecs->table('goods_lang') . " as gl " .
                "ON g.goods_id = gl.goods_id AND gl.lang = '" . $res['language'] . "'" .
            "WHERE g.`goods_id` = '" . $res['goods_id'] . "'";
    $goods = $db->getRow($sql);
    
    $sent = false;
    if (can_send_mail($email, true))
    {
        $old_lang = $_CFG['lang'];
        $_CFG['lang'] = $res['language'];
        
        $tpl = get_mail_template('goods_dai_' . $mode);
        $content = $smarty->fetch('str:' . $tpl['template_content']);
        $sent = send_mail($name, $email, $tpl['template_subject'] . ' - ' . $goods['goods_name'], $content, $tpl['is_html']);
        
        $_CFG['lang'] = $old_lang;
    }
    
    make_json_result('', ($sent ? '已發送電郵到 ' . $email : '發送電郵失敗'));
}

?>