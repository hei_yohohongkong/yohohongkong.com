<?php

define('IN_ECS', true);
require dirname(__FILE__) . '/includes/init.php';
require_once ROOT_PATH . 'includes/lib_order.php';
$crmController = new Yoho\cms\Controller\CrmController();

if ($_REQUEST['act'] == 'list') {
    $paymentController = new Yoho\cms\Controller\PaymentController();
    $methods = $paymentController->getWebOfflinePaymentMethodList();
    foreach ($methods as $method) {
        $pay_list[$method['pay_id']] = $method['pay_name'];
    }
    $smarty->assign('pay_list', $pay_list);
    $smarty->assign('ur_here', $_LANG['op_system']);
    $smarty->display('offline_payment.htm');
} elseif ($_REQUEST['act'] == 'query') {
    $data = $crmController->offline_payment_list();
    $list = $data['data'];
    foreach ($list as $key => $row) {
        $payment = payment_info($row['pay_id']);
        $list[$key]['payment_name'] = $payment['pay_name'];
        if (!empty($payment['pay_config_list']['banks'][$row['pay_account']])) {
            $list[$key]['account_name'] = $payment['pay_config_list']['banks'][$row['pay_account']]['name'];
        }
        if (empty($list[$key]['account_name'])) {
            $list[$key]['account_name'] = "--";
        }
        $custom_actions = [
            'verify' => [
                'title' => $row['status'] >= OFFLINE_PAYTMENT_FINISHED ? '查看' : ($row['status'] == OFFLINE_PAYTMENT_UPLOADED ? '審核' : '修改'),
                'label' => $row['status'] >= OFFLINE_PAYTMENT_FINISHED ? '查看' : ($row['status'] == OFFLINE_PAYTMENT_UPLOADED ? '審核' : '修改'),
                'btn_class' => "btn btn-" . ($row['status'] >= OFFLINE_PAYTMENT_FINISHED ? 'default' : ($row['status'] == OFFLINE_PAYTMENT_UPLOADED ? 'success' : 'info')),
                'icon_class' => "fa fa-" . ($row['status'] >= OFFLINE_PAYTMENT_FINISHED ? 'search' : ($row['status'] == OFFLINE_PAYTMENT_UPLOADED ? 'check' : 'edit'))
            ]
        ];
        $list[$key]["_action"] = $crmController->generateCrudActionBtn($row['op_id'], 'id', null, [], $custom_actions);
        $list[$key]['status'] = $_LANG['offline_payment_status'][$row['status']];
        $list[$key]['order_sn'] = "<a href='order.php?act=info&order_id=$row[order_id]' target='_blank'>$row[order_sn]</a>";
    }
    $crmController->ajaxQueryAction($list, $data['record_count'], false);
} elseif ($_REQUEST['act'] == 'verify') {
    if (empty($_REQUEST['id'])) {
        sys_msg("請選擇要處理的申請");
    }
    $request = $crmController->get_offlinne_payment_request($_REQUEST['id']);
    $record = $request['record'];
    $ref = $request['ref'];
    $record['formatted_status'] = $_LANG['offline_payment_status'][$record['status']];

    if ($record['status'] != OFFLINE_PAYTMENT_PENDING) {
        $files = explode(",", $record['files']);
        foreach ($files as $file) {
            if (empty($file)) {
                continue;
            }
            $full_path = ROOT_PATH . "data/order_receipt/$file";
            if (file_exists($full_path)) {
                $dataUri = "data:" . mime_content_type($full_path) . ";base64," . base64_encode(file_get_contents($full_path));
                if (strpos(mime_content_type($full_path), "image") !== false) {
                    $img_data[] = $dataUri;
                } elseif (strpos(mime_content_type($full_path), "pdf") !== false) {
                    $pdf_data[] = $dataUri;
                } else {
                    $data = [
                        'dataUri' => $dataUri,
                        'size' => round(filesize($full_path) / 1024, 1),
                    ];
                    $file_data[] = $data;
                }
            }
        }
        $smarty->assign("img_data", $img_data);
        $smarty->assign("pdf_data", $pdf_data);
        $smarty->assign("file_data", $file_data);
    }

    $payment = payment_info($record['pay_id']);
    if (strpos($payment['pay_code'], 'bank') !== false) {
        $banks = $payment['pay_config_list']['banks'];
        $account_number = [];
        foreach ($banks as $key => $bank) {
            $account = $db->getRow("SELECT account_id, account_name FROM actg_accounts WHERE account_number = '$bank[account]'");
            $banks[$key]['account_id'] = $account['account_id'];
            $banks[$key]['account_name'] = $account['account_name'];
        }
    }

    $order = order_info($record['order_id']);
    $record['payment_name'] = $payment['pay_name'];
    $action_link_list[] = [
        'href' => 'offline_payment.php?act=list',
        'text' => '線下付款列表',
    ];

    $smarty->assign('ur_here', $_LANG['op_system_verify']);
    $smarty->assign("action_link_list", $action_link_list);
    $smarty->assign("record", $record);
    $smarty->assign("ref", $ref);
    $smarty->assign("banks", $banks);
    $smarty->assign("order_amount", $order['order_amount']);
    $smarty->display('offline_payment_verify.htm');
} elseif ($_REQUEST['act'] == 'update') {
    if (empty($_REQUEST['op_id'])) {
        sys_msg("請選擇要處理的申請");
    }
    $update = [];
    $token_update = false;
    foreach ($_REQUEST as $name => $value) {
        if (!in_array($name, ['act', 'op_id', 'account_id', 'amount']) && !($name == 'status' && $value == OFFLINE_PAYTMENT_FINISHED)) {
            $update[] = "$name = '$value'";
            if ($name == 'status') {
                $token_update = true;
            }
        }
    }
    $record = $db->getRow("SELECT * FROM " . $ecs->table("offline_payment") . " WHERE op_id = $_REQUEST[op_id]");
    if (intval($_REQUEST['status']) == OFFLINE_PAYTMENT_FINISHED) {
        $sql = "SELECT 1 FROM " . $ecs->table("order_info") . " WHERE order_id = $record[order_id] " .
                "AND order_status NOT " . db_create_in([OS_CANCELED, OS_INVALID, OS_WAITING_REFUND]) .
                "AND pay_status != " . PS_PAYED;
        if (empty($db->getOne($sql))) {
            sys_msg("無法為此訂單付款", 1);
        }
        $actg_payment_methods = $db->getCol("SELECT payment_method_id FROM actg_account_payment_methods WHERE " . (!empty($_REQUEST['account_id']) ? "account_id = $_REQUEST[account_id] AND " : "") . "pay_code = (SELECT pay_code FROM " . $ecs-> table("payment") . " WHERE pay_id = $record[pay_id])");
        if (count($actg_payment_methods) > 1 || empty($actg_payment_methods)) {
            sys_msg("未選擇付款方式");
        } else {
            $actg_payment_method = $actg_payment_methods[0];
        }
        $ticket_id = empty(trim($_REQUEST['ticket_id'])) ? 0 : intval(trim($_REQUEST['ticket_id']));
        if (!empty($ticket_id)) {
            $ticket_id = $db->getOne("SELECT ticket_id FROM " . $ecs->table("crm_ticket_info") . " WHERE ticket_id = $ticket_id");
        }

        $crmController->offlinePayment($ticket_id, $record['order_id'], $_REQUEST['amount'], $_REQUEST['op_id'], true, $actg_payment_method);
    }
    if (intval($_REQUEST['status']) == OFFLINE_PAYTMENT_PENDING) {
        $files = explode(",", $record['files']);
        foreach ($files as $file) {
            if (empty($file)) {
                continue;
            }
            $full_path = ROOT_PATH . "data/order_receipt/$file";
            if (file_exists($full_path)) {
                unlink($full_path);
            }
        }
        $update[] = "files = '', upload_time = NULL";
    }
    if (!empty($update)) {
        if ($token_update) {
            $update[] = "token = '" . md5("$record[user_id]-$record[order_id]-$_REQUEST[status]-$record[expire_time]") . "'";
        }
        $sql = "UPDATE " . $ecs->table("offline_payment") . " SET " . implode(", ", $update) . " WHERE op_id = $_REQUEST[op_id]";
        $db->query($sql);
        sys_msg((intval($_REQUEST['status']) == OFFLINE_PAYTMENT_FINISHED ? "付款" : "更新") . "成功", 0, [['href' => 'offline_payment.php?act=list', 'text' => '線下付款列表']]);
    } elseif (intval($_REQUEST['status']) == OFFLINE_PAYTMENT_FINISHED) {
        sys_msg("付款成功", 0, [['href' => 'offline_payment.php?act=list', 'text' => '線下付款列表']]);
    }
    sys_msg((intval($_REQUEST['status']) == OFFLINE_PAYTMENT_FINISHED ? "付款" : "更新") . "失敗");
} elseif ($_REQUEST['act'] == 'get_tickets') {
    include_once ROOT_PATH . 'includes/Google_api_client/vendor/autoload.php';
    $amount = empty($_REQUEST['amount']) ? 0 : floatval(trim($_REQUEST['amount']));
    $pay_id = empty($_REQUEST['pay_id']) ? 0 : intval(trim($_REQUEST['pay_id']));
    $account_id = empty($_REQUEST['account_id']) ? 0 : intval(trim($_REQUEST['account_id']));
    $live_config = [
        "fps轉賬"  => ['pay_ids' => [21], 'account_id' => 4],
        "hsbc轉帳" => ['pay_ids' => [5, 9], 'account_id' => 2],
        "hs轉賬"   => ['pay_ids' => [5, 9], 'account_id' => 4],
    ];
    $beta_config = [
        "dem-test-fps"  => ['pay_ids' => [21], 'account_id' => 4],
        "dem-test-hsbc" => ['pay_ids' => [5, 9], 'account_id' => 2],
        "dem-test-hs"   => ['pay_ids' => [5, 9], 'account_id' => 4],
    ];
    $labelConfig = strpos($_SERVER['SERVER_NAME'], 'beta.') !== false ? $beta_config : $live_config;
    $labels = [];

    foreach ($labelConfig as $label => $config) {
        if (in_array($pay_id, $config['pay_ids']) && $config['account_id'] == $account_id) {
            $labels[] = "label:$label";
        }
    }
    if (empty($labels)) {
        make_json_response("no_match_found");
    } elseif (count($labels) > 1) {
        make_json_response("multiple_labels");
    }

    $gmail_user = $crmController::GMAIL_USER;
    $client = new Google_Client();
    $client->setApplicationName("Yoho Offline Payment API");
    $client->setAuthConfig($crmController::KEY_FILE_LOCATION);
    $client->setScopes(['https://mail.google.com/']);
    $client->setSubject($gmail_user);
    $client->useApplicationDefaultCredentials();
    $client->fetchAccessTokenWithAssertion();
    $client->getAccessToken();
    $service = new Google_Service_Gmail($client);

    $params = [
        'maxResults' => strpos($_SERVER['SERVER_NAME'], 'beta.') !== false ? 5 : 50,
        'q' => $labels[0] . " -label:starred after:" . local_date("Y-m-d", local_strtotime("-1 week midnight")) . " \"HKD" . number_format($amount, 2) . "\""
    ];
    
    $results = $service->users_threads->listUsersThreads($gmail_user, $params);

    $client->setUseBatch(true);
    $batch = new Google_Http_Batch($client);
    foreach ($results->getThreads() as $row) {
        $mail_id = $row->id;
        $email = $service->users_messages->get($gmail_user, $mail_id);
        $batch->add($email, $mail_id);
    }
    $match_mails = [];
    foreach ($batch->execute() as $key => $res) {
        $headers = $res->getPayload()->getHeaders();
        $mail_id = str_replace('response-', '', $key);
        $payload = $res->getPayload();
        $info = $crmController->getUsefulHeaders($headers);
        $ticket_id = $crmController->createTicket($mail_id, $res->getThreadId(), CRM_PLATFORM_GMAIL, $info['From_mail'], $info['Format_Date']);
        if (in_array($info['Payment'], ["HSBC", "HS", "FPS"])) {
            $html = base64_decode(strtr($payload->body->data, '-_', '+/'));
        } elseif ($info['Payment'] == "TEST" && strpos($_SERVER['SERVER_NAME'], 'beta.') !== false) {
            $html = base64_decode(strtr($payload->parts[0]->body->data, '-_', '+/'));
        }
        preg_match("/(HKD[0-9,]+\.[0-9]{2}){1}/", $html, $amount);
        preg_match("/\d\d\d\d-\d\d-\d\d\s\d\d\:\d\d/", $html, $date);
        if (empty($amount)) {
            continue;
        }
        if (empty($date)) {
            $date[0] = substr($info['Format_Date'], 0, -3);
        }
        $match_mails[$ticket_id] = [
            'amount'    => $amount[0],
            'date'      => $date[0],
        ];
    }
    make_json_response($match_mails);
}

?>