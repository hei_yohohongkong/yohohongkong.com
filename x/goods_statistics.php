<?php

define('IN_ECS', true);
require dirname(__FILE__) . '/includes/init.php';
ini_set('memory_limit', '256M');

$statisticsController = new Yoho\cms\Controller\StatisticsController();

if ($_REQUEST['act'] == 'list') {
    $index = empty($_REQUEST['index']) ? 0 : intval($_REQUEST['index']);
    $action_link_list = [[
        'text' => empty($_REQUEST['index']) ? 'Search Session' : 'Session',
        'href' => 'goods_statistics.php?act=list' . (empty($index) ? '&index=3' : '')
    ]];
    $smarty->assign('index', $index);
    $smarty->assign('sort_by', $statisticsController::GOOGLE_STATS_COLUMNS[$index]);
    $smarty->assign('action_link_list', $action_link_list);
    $smarty->assign('ur_here', $_LANG['goods_stats']);
    $smarty->display('goods_statistics.htm');
} elseif ($_REQUEST['act'] == 'query') {
    $res = $statisticsController->getLastestGoodsStats();
    $statisticsController->ajaxQueryAction($res['data'], $res['count'], false);
} elseif (strpos($_REQUEST['act'], 'query_') !== false) {
    preg_match('/query_(\w+)/', $_REQUEST['act'], $match);
    $type = $match[1];
    make_json_response($statisticsController->getLastestBrandsCatsStats($type));
}