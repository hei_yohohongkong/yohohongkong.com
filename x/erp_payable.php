<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require_once(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require_once(dirname(__FILE__) .'/includes/ERP/lib_erp_finance.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
if($_REQUEST['act'] == 'pay')
{
include('../includes/cls_json.php');
$json  = new JSON;
if(!admin_priv('erp_finance_manage','',false))
{
$result['error']=1;
$result['message']=$_LANG['erp_no_permit'];
die($json->encode($result));
}
$order_id=isset($_REQUEST['order_id']) &&intval($_REQUEST['order_id'])>0?intval($_REQUEST['order_id']):0;
$pay_amount=isset($_REQUEST['pay_amount']) &&floatval($_REQUEST['pay_amount'])>0?floatval($_REQUEST['pay_amount']) : 0;
$payment_id=create_payment($order_id,$pay_amount);
if($payment_id==-1)
{
$result['error']=2;
$result['message']=$GLOBALS['_LANG']['erp_payable_no_agency_access'];
die($json->encode($result));
}
if($payment_id==-2)
{
$result['error']=2;
$result['message']=$GLOBALS['_LANG']['erp_payable_no_valid_bank_account'];
die($json->encode($result));
}
if(lock_payment($payment_id,'edit'))	
{
if(payment_act_record($payment_id,'modify'))
{
$result['error']=0;
die($json->encode($result));
}
}
else{
$result['error']=-1;
$result['message']=$_LANG['erp_payment_no_accessibility'];
die($json->encode($result));
}
}
if($_REQUEST['act'] == 'withdrawal_to_edit')
{
include('../includes/cls_json.php');
$json  = new JSON;
if(!admin_priv('erp_finance_approve','',false))
{
$result['error']=1;
$result['message']=$_LANG['erp_no_permit'];
die($json->encode($result));
}
$payment_id=isset($_REQUEST['payment_id']) &&intval($_REQUEST['payment_id'])>0?intval($_REQUEST['payment_id']):0;
$agency_id=get_admin_agency_id($_SESSION['admin_id']);
if($agency_id>0)
{
$sql="select * from ".$GLOBALS['ecs']->table('erp_order')." where order_id='".$order_id."' and agency_id='".$agency_id."'";
$order=$GLOBALS['db']->getRow($sql);
if(empty($order))
{
$result['error']=2;
$result['message']=$GLOBALS['_LANG']['erp_payable_no_agency_access'];
die($json->encode($result));
}
}
$sql="update ".$GLOBALS['ecs']->table('erp_payment')." set payment_status='1' where payment_id='".$payment_id."'";
$GLOBALS['db']->query($sql);
$result['error']=0;
die($json->encode($result));
}
?>
