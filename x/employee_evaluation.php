<?php

/***
* employee_evaluation.php
* by howang 2015-04-27
*
* Employee Evaluation Report
***/


define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require(ROOT_PATH . 'includes/lib_order.php');
$userController = new Yoho\cms\Controller\UserController();

if (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'query' ||  $_REQUEST['act'] == 'download'))
{
    /* 检查权限 */
    check_authz_json('sale_order_stats');
    if (strstr($_REQUEST['start_date'], '-') === false)
    {
        $_REQUEST['start_date'] = local_date('Y-m-d', $_REQUEST['start_date']);
        $_REQUEST['end_date'] = local_date('Y-m-d', $_REQUEST['end_date']);
    }
    /*------------------------------------------------------ */
    //--Excel文件下载
    /*------------------------------------------------------ */
    if ($_REQUEST['act'] == 'download')
    {
        $file_name = $_REQUEST['start_date'].'_'.$_REQUEST['end_date'] . '_employee_evaluation';
        $evaluation_data = get_employee_evaluation(false);
        header("Content-type: application/vnd.ms-excel; charset=utf-8");
        header("Content-Disposition: attachment; filename=$file_name.xls");

        require_once ROOT_PATH . 'includes/excel/PHPExcel.php';
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        
        $objPHPExcel->getActiveSheet()->setTitle('員工評核');
        
        /* 文件标题 */
        $objPHPExcel->getActiveSheet()
        ->setCellValue('A1', ecs_iconv(EC_CHARSET, 'UTF8', $_REQUEST['start_date']. $_LANG['to'] .$_REQUEST['end_date']. ' 員工評核'))
        ->setCellValue('A2', ecs_iconv(EC_CHARSET, 'UTF8', '員工名稱'))
        ->setCellValue('B2', ecs_iconv(EC_CHARSET, 'UTF8', 'POS訂單數目'))
        ->setCellValue('C2', ecs_iconv(EC_CHARSET, 'UTF8', 'POS銷售金額'))
        ->setCellValue('D2', ecs_iconv(EC_CHARSET, 'UTF8', 'POS門市購買數目'))
        ->setCellValue('E2', ecs_iconv(EC_CHARSET, 'UTF8', 'POS門市購買銷售金額'))
        ->setCellValue('F2', ecs_iconv(EC_CHARSET, 'UTF8', 'POS訂單平均產品數'))
        ->setCellValue('G2', ecs_iconv(EC_CHARSET, 'UTF8', '預售登記回覆'))
        ->setCellValue('H2', ecs_iconv(EC_CHARSET, 'UTF8', '缺貨登記回覆'))
        ->setCellValue('I2', ecs_iconv(EC_CHARSET, 'UTF8', '採購'))
        ->setCellValue('J2', ecs_iconv(EC_CHARSET, 'UTF8', '入貨'))
        ->setCellValue('K2', ecs_iconv(EC_CHARSET, 'UTF8', '訂單付款'))
        ->setCellValue('L2', ecs_iconv(EC_CHARSET, 'UTF8', '訂單出貨'))
        ->setCellValue('M2', ecs_iconv(EC_CHARSET, 'UTF8', '訂單備註'))
        ->setCellValue('N2', ecs_iconv(EC_CHARSET, 'UTF8', '處理工作清單數目'))
        ->setCellValue('O2', ecs_iconv(EC_CHARSET, 'UTF8', '處理工作清單時間'))
        ->setCellValue('P2', ecs_iconv(EC_CHARSET, 'UTF8', '服務態度'))
        ->setCellValue('Q2', ecs_iconv(EC_CHARSET, 'UTF8', '產品知識'))
        ->setCellValue('R2', ecs_iconv(EC_CHARSET, 'UTF8', '整體評分'))
        ->setCellValue('S2', ecs_iconv(EC_CHARSET, 'UTF8', '友和寄貨'));
        $objPHPExcel->getActiveSheet()->mergeCells('A1:S1');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $i = 3;
        foreach ($evaluation_data['evaluation_data'] AS $key => $value)
        {
            $objPHPExcel->getActiveSheet()
            ->setCellValue('A'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['sales_name']))
            ->setCellValue('B'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['orders']))
            ->setCellValue('C'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['volume']))
            ->setCellValue('D'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['shop_buy_orders']))
            ->setCellValue('E'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['shop_buy_volume']))
            ->setCellValue('F'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['avg_goods']))
            ->setCellValue('G'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['book_reply']))
            ->setCellValue('H'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['dai_replay']))
            ->setCellValue('I'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['purchase']))
            ->setCellValue('J'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['warehousing']))
            ->setCellValue('K'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['order_pay']))
            ->setCellValue('L'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['order_delivery']))
            ->setCellValue('M'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['order_remark']))
            ->setCellValue('N'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['task_count']))
            ->setCellValue('O'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['task_time']))
            ->setCellValue('P'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['sales_rating1']))
            ->setCellValue('Q'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['sales_rating2']))
            ->setCellValue('R'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['sales_rating3']))
            ->setCellValue('S'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['shop_rating1']));
            $i++;
        }
        
        for ($col = 'A'; $col <= 'S'; $col++)
        {
            $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
        }
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        
        exit;
    }
    $evaluation_data = get_employee_evaluation();
    $smarty->assign('evaluation_list', $evaluation_data['evaluation_data']);
    $smarty->assign('filter',          $evaluation_data['filter']);
    $smarty->assign('record_count',    $evaluation_data['record_count']);
    $smarty->assign('page_count',      $evaluation_data['page_count']);
    $smarty->assign('start_date',      $_REQUEST['start_date']);
    $smarty->assign('end_date',        $_REQUEST['end_date']);

    make_json_result($smarty->fetch('employee_evaluation.htm'), '', array('filter' => $evaluation_data['filter'], 'page_count' => $evaluation_data['page_count']));
}
/*------------------------------------------------------ */
//--商品明细列表
/*------------------------------------------------------ */
else
{
    /* 权限判断 */
    admin_priv('sale_order_stats');
    /* 时间参数 */
    if (!isset($_REQUEST['start_date']))
    {
        $start_date = local_strtotime(month_start());
    }
    if (!isset($_REQUEST['end_date']))
    {
        $end_date = local_strtotime(month_end());
    }

    $evaluation_data = get_employee_evaluation();
    /* 赋值到模板 */
    $smarty->assign('filter',           $evaluation_data['filter']);
    $smarty->assign('record_count',     $evaluation_data['record_count']);
    $smarty->assign('page_count',       $evaluation_data['page_count']);
    $smarty->assign('evaluation_list',  $evaluation_data['evaluation_data']);
    $smarty->assign('ur_here',          $_LANG['employee_evaluation']);
    $smarty->assign('full_page',        1);
    $smarty->assign('start_date',       local_date('Y-m-d', $start_date));
    $smarty->assign('end_date',         local_date('Y-m-d', $end_date));
    $smarty->assign('cfg_lang',         $_CFG['lang']);
    $smarty->assign('action_link',      array('text' => '下載員工評核報表','href'=>'#download'));

    /* 显示页面 */
    assign_query_info();
    $smarty->display('employee_evaluation.htm');
}

function month_start()
{
    return local_date('Y-m-01');
}

function month_end()
{
    return local_date('Y-m-t');
}

function get_employee_evaluation($is_pagination = true)
{
    global $db, $ecs, $userController;
    
    /* 时间参数 */
    $filter['start_date'] = empty($_REQUEST['start_date']) ? local_strtotime(month_start()) : local_strtotime($_REQUEST['start_date']);
    $filter['end_date'] = empty($_REQUEST['end_date']) ? local_strtotime(month_end()) : local_strtotime($_REQUEST['end_date']);
    $filter['sort_by']          = empty($_REQUEST['sort_by']) ? 'sales_name' : trim($_REQUEST['sort_by']);
    $filter['sort_order']       = empty($_REQUEST['sort_order']) ? 'ASC' : trim($_REQUEST['sort_order']);

    $sql = "SELECT COUNT(*) FROM " . $ecs->table('admin_user') ." as au ".
    "LEFT JOIN " . $ecs->table('salesperson') ." as sp ON sp.admin_id = au.user_id AND sp.available = 1 ".
    "WHERE au.user_name != 'admin' AND is_disable = 0";
    $filter['record_count'] = $GLOBALS['db']->getOne($sql);
    
    /* 分页大小 */
    $filter = page_and_size($filter);
    
    $evaluation_data = array();
    
    // 員工名稱
    $sql = "SELECT IFNULL(sp.sales_name, au.user_name) as sales_name " .
            "FROM " . $ecs->table('admin_user') ." as au ".
            "LEFT JOIN " . $ecs->table('salesperson') ." as sp ON sp.admin_id = au.user_id AND sp.available = 1 ".
            "WHERE au.user_name != 'admin' AND is_disable = 0 " .
            "ORDER BY sales_name ASC " .
    ($is_pagination ? "LIMIT " . $filter['start'] . ", " . $filter['page_size'] : '');
    $sales_name = $db->getCol($sql);
    // We use lower_sales_name to find key
    $lower_sales_name = array_map('strtolower', $sales_name);
    foreach ($sales_name as $key => $row)
    {
        $evaluation_data[$key] = array(
            'sales_name' => $row,
            'orders' => 0,
            'volume' => 0,
            'shop_buy_orders' =>0,
            'shop_buy_volume' =>0,
            'avg_goods' => 0,
            'book_reply' => 0,
            'dai_reply' => 0,
            'purchase' => 0,
            'warehousing' => 0,
            'warehousing_item' => 0,
            'order_pay' => 0,
            'order_delivery' => 0,
            'order_remark' => 0,
            'task_count' => 0,
            'task_time' => 0,
            'sales_rating1' => 0,
            'sales_rating2' => 0,
            'sales_rating3' => 0,
            'shop_rating1' => 0,
        );
    }
    
    // 訂單數目, 銷售金額, 訂單平均產品數
    $sql = "SELECT salesperson, COUNT(*) as orders, SUM(total_amount) as volume, AVG(goods_count) as avg_goods " .
            "FROM (" .
                "SELECT oi.`salesperson`, (oi.`money_paid` + oi.`order_amount`) as total_amount, sum(og.goods_number) as goods_count " .
                "FROM " . $ecs->table('order_info') . " as oi " .
                "LEFT JOIN " . $ecs->table('order_goods') . " as og ON oi.`order_id` = og.`order_id` " .
                "WHERE 1 " . order_query_sql('finished', 'oi.') .
                "AND oi.shipping_time >= '" . $filter['start_date'] . "' AND oi.shipping_time < '" . ($filter['end_date'] + 86400) . "' " .
                // Exclude orders for the followiung user accounts: 11111111, 88888888, 99999999
                "AND (oi.user_id NOT IN (".implode(',',$userController->getWholesaleUserIds()).") AND (oi.order_type <> '".Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE."' OR oi.order_type IS NULL))" .
                "GROUP BY oi.`order_id` " .
                ") as tmp " .
            "WHERE `salesperson` " . db_create_in($sales_name) .
            "GROUP BY `salesperson` ";
    $res = $db->getAll($sql);
    foreach ($res as $row)
    {
        $key = array_search(strtolower($row['salesperson']), $lower_sales_name);
        $row['avg_goods'] = round(doubleval($row['avg_goods']), 2);
        $evaluation_data[$key] = array_merge($evaluation_data[$key], $row);
    }
    
    // 門市購買訂單數目, 門市購買銷售金額, 訂單平均產品數
    $sql = "SELECT salesperson, COUNT(*) as shop_buy_orders, SUM(total_amount) as shop_buy_volume " .
        "FROM (" .
        "SELECT oi.`salesperson`, (oi.`money_paid` + oi.`order_amount`) as total_amount, sum(og.goods_number) as goods_count " .
        "FROM " . $ecs->table('order_info') . " as oi " .
        "LEFT JOIN " . $ecs->table('order_goods') . " as og ON oi.`order_id` = og.`order_id` " .
        "WHERE 1 " . order_query_sql('finished', 'oi.') .
        "AND oi.shipping_time >= '" . $filter['start_date'] . "' AND oi.shipping_time < '" . ($filter['end_date'] + 86400) . "' AND oi.shipping_id = 2 AND platform = 'POS' " .
        // Exclude orders for the followiung user accounts: 11111111, 88888888, 99999999
        "AND (oi.user_id NOT IN (".implode(',',$userController->getWholesaleUserIds()).") AND (oi.order_type <> '".Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE."' OR oi.order_type IS NULL))" .
        "GROUP BY oi.`order_id` " .
        ") as tmp " .
        "WHERE `salesperson` " . db_create_in($sales_name) .
        "GROUP BY `salesperson` ";
    $res = $db->getAll($sql);
    foreach ($res as $row)
    {
        $key = array_search(strtolower($row['salesperson']), $lower_sales_name);
        $evaluation_data[$key] = array_merge($evaluation_data[$key], $row);
    }
    
    // 預售登記回覆
    $sql = "SELECT `operator`, COUNT(*) as `count` " .
            "FROM " . $ecs->table('goods_book') .
            "WHERE `update_time2` >= '" . $filter['start_date'] . "' AND `update_time2` < '" . ($filter['end_date'] + 86400) . "' " .
            "AND `content2` != '' " .
            "AND `operator` " . db_create_in($sales_name) .
            "GROUP BY `operator` ";
    $res = $db->getAll($sql);
    foreach ($res as $row)
    {
        $key = array_search(strtolower($row['operator']), $lower_sales_name);
        $evaluation_data[$key]['book_reply'] = intval($row['count']);
    }
    
    // 缺貨登記回覆 (首次回覆)
    $sql = "SELECT `operator`, COUNT(*) as `count` " .
            "FROM " . $ecs->table('goods_dai') .
            "WHERE `update_time` >= '" . $filter['start_date'] . "' AND `update_time` < '" . ($filter['end_date'] + 86400) . "' " .
            "AND `content` != '' " .
            "AND `operator` " . db_create_in($sales_name) .
            "GROUP BY `operator` ";
    $res = $db->getAll($sql);
    foreach ($res as $row)
    {
        $key = array_search(strtolower($row['operator']), $lower_sales_name);
        $evaluation_data[$key]['dai_reply'] += intval($row['count']);
    }
    // 缺貨登記回覆 (到貨通知)
    $sql = "SELECT `operator2`, COUNT(*) as `count` " .
            "FROM " . $ecs->table('goods_dai') .
            "WHERE `update_time2` >= '" . $filter['start_date'] . "' AND `update_time2` < '" . ($filter['end_date'] + 86400) . "' " .
            "AND `content2` != '' " .
            "AND `operator2` " . db_create_in($sales_name) .
            "GROUP BY `operator2` ";
    $res = $db->getAll($sql);
    foreach ($res as $row)
    {
        $key = array_search(strtolower($row['operator2']), $lower_sales_name);
        $evaluation_data[$key]['dai_reply'] += intval($row['count']);
    }
    
    // 採購單
    $sql = "SELECT `operator`, COUNT(*) as `count` " .
            "FROM " . $ecs->table('erp_order') .
            "WHERE `create_time` >= '" . $filter['start_date'] . "' AND `create_time` < '" . ($filter['end_date'] + 86400) . "' " .
            "AND `order_status` = 4 " .
            "AND `operator` " . db_create_in($sales_name) .
            "GROUP BY `operator` ";
    $res = $db->getAll($sql);
    foreach ($res as $row)
    {
        $key = array_search(strtolower($row['operator']), $lower_sales_name);
        $evaluation_data[$key]['purchase'] = intval($row['count']);
    }
    
    // 入貨單
    $sql = "SELECT ew.`operator`, COUNT(DISTINCT(ew.warehousing_id)) as `count`, COUNT(DISTINCT(ewi.warehousing_item_id )) as `count2`  " .
            "FROM " . $ecs->table('erp_warehousing') ." as ew ".
            "LEFT JOIN ". $ecs->table('erp_warehousing_item') ." as ewi ON ewi.warehousing_id = ew.warehousing_id ".
            "WHERE ew.`create_time` >= '" . $filter['start_date'] . "' AND ew.`create_time` < '" . ($filter['end_date'] + 86400) . "' " .
            "AND ew.`status` = 3 " .
            "AND ew.`warehousing_style_id` = 1 " . // 關聯採購訂單入貨
            "AND ew.`operator` " . db_create_in($sales_name) .
            "GROUP BY ew.`operator` ";

    $res = $db->getAll($sql);

    foreach ($res as $row)
    {
        $key = array_search(strtolower($row['operator']), $lower_sales_name);
        $evaluation_data[$key]['warehousing'] = intval($row['count']);
        $evaluation_data[$key]['warehousing_item'] = intval($row['count2']);
    }
    
    // 訂單付款
    $sql = "SELECT `operator`, COUNT(*) as `count` " .
            "FROM (" .
                "SELECT a.* " .
                "FROM " . $ecs->table('admin_operation_log') . " as a " .
                "LEFT JOIN " . $ecs->table('admin_operation_log') . " as b " .
                    "ON a.order_id = b.order_id AND a.log_id < b.log_id AND a.operation = b.operation " .
                "WHERE b.log_id IS NULL" .
            ") aol " .
            "WHERE `log_time` >= '" . $filter['start_date'] . "' AND `log_time` < '" . ($filter['end_date'] + 86400) . "' " .
            "AND `operation` = '訂單付款' " .
            "AND `operator` " . db_create_in($sales_name) .
            "GROUP BY `operator` ";
    $res = $db->getAll($sql);
    foreach ($res as $row)
    {
        $key = array_search(strtolower($row['operator']), $lower_sales_name);
        $evaluation_data[$key]['order_pay'] = intval($row['count']);
    }
    
    // 訂單發貨
    $sql = "SELECT `operator`, COUNT(*) as `count` " .
            "FROM (" .
                "SELECT a.* " .
                "FROM " . $ecs->table('admin_operation_log') . " as a " .
                "LEFT JOIN " . $ecs->table('admin_operation_log') . " as b " .
                    "ON a.order_id = b.order_id AND a.log_id < b.log_id AND a.operation = b.operation " .
                "WHERE b.log_id IS NULL" .
            ") aol " .
            "WHERE `log_time` >= '" . $filter['start_date'] . "' AND `log_time` < '" . ($filter['end_date'] + 86400) . "' " .
            "AND `operation` = '訂單發貨' " .
            "AND `operator` " . db_create_in($sales_name) .
            "GROUP BY `operator` ";
    $res = $db->getAll($sql);
    foreach ($res as $row)
    {
        $key = array_search(strtolower($row['operator']), $lower_sales_name);
        $evaluation_data[$key]['order_delivery'] = intval($row['count']);
    }
    
    // 訂單備註
    $sql = "SELECT `action_user`, COUNT(*) as `count` " .
            "FROM " . $ecs->table('order_action') .
            "WHERE `log_time` >= '" . $filter['start_date'] . "' AND `log_time` < '" . ($filter['end_date'] + 86400) . "' " .
            "AND `action_note` LIKE '[儲存備註]%' " .
            "AND `action_user` " . db_create_in($sales_name) .
            "GROUP BY `action_user` ";
    $res = $db->getAll($sql);
    foreach ($res as $row)
    {
        $key = array_search(strtolower($row['action_user']), $lower_sales_name);
        $evaluation_data[$key]['order_remark'] = intval($row['count']);
    }
    
    // 處理工作清單數目, 處理工作清單時間
    $sql = "SELECT `pic`, COUNT(*) as `count`, SUM(`estimated_time`) as `total_time` " .
            "FROM " . $ecs->table('admin_taskes') .
            "WHERE `finish_time` >= '" . $filter['start_date'] . "' AND `finish_time` < '" . ($filter['end_date'] + 86400) . "' " .
            "AND `status` IN (".Yoho\cms\Controller\TaskController::STATUS_REVIEW.",".Yoho\cms\Controller\TaskController::STATUS_RESOLVED.",".Yoho\cms\Controller\TaskController::STATUS_ARCHIVED.",'Finished')" .
            "AND `pic` " . db_create_in($sales_name) .
            "GROUP BY `pic` ";
    $res = $db->getAll($sql);
    foreach ($res as $row)
    {
        $key = array_search(strtolower($row['pic']), $lower_sales_name);
        $evaluation_data[$key]['task_count'] = intval($row['count']);
        $evaluation_data[$key]['task_time'] = intval($row['total_time']);
    }
    
    // 客戶給員工的評價
    $sql = "SELECT sp.sales_name, AVG(rs.score1) as sales_rating1, AVG(rs.score2) as sales_rating2, AVG(rs.score3) as sales_rating3 " .
            "FROM " . $ecs->table('review_sales') . " as rs " .
            "LEFT JOIN " . $ecs->table('salesperson') . " as sp ON rs.sales_id = sp.sales_id " .
            "WHERE rs.`time` >= '" . $filter['start_date'] . "' AND rs.`time` < '" . ($filter['end_date'] + 86400) . "' " .
            "AND sp.`sales_name` " . db_create_in($sales_name) .
            "GROUP BY rs.`sales_id` ";
    $res = $db->getAll($sql);
    foreach ($res as $row)
    {
        $key = array_search(strtolower($row['sales_name']), $lower_sales_name);
        $evaluation_data[$key]['sales_rating1'] = round(doubleval($row['sales_rating1']), 2);
        $evaluation_data[$key]['sales_rating2'] = round(doubleval($row['sales_rating2']), 2);
        $evaluation_data[$key]['sales_rating3'] = round(doubleval($row['sales_rating3']), 2);
    }
    
    // 客戶給友和的評價
    $sql = "SELECT `operator`, AVG(rs.score1) as shop_rating1 " .
            "FROM " . $ecs->table('review_shop') . " as rs " .
            "LEFT JOIN (" .
                "SELECT a.* " .
                "FROM " . $ecs->table('admin_operation_log') . " as a " .
                "LEFT JOIN " . $ecs->table('admin_operation_log') . " as b " .
                    "ON a.order_id = b.order_id AND a.log_id < b.log_id AND a.operation = b.operation " .
                "WHERE b.log_id IS NULL " .
                "AND a.`operation` = '訂單發貨' " .
            ") aol ON rs.order_id = aol.order_id " .
            "WHERE rs.`time` >= '" . $filter['start_date'] . "' AND rs.`time` < '" . ($filter['end_date'] + 86400) . "' " .
            "AND aol.`operator` " . db_create_in($sales_name) .
            "GROUP BY `operator` ";
    $res = $db->getAll($sql);
    foreach ($res as $row)
    {
        $key = array_search(strtolower($row['operator']), $lower_sales_name);
        $evaluation_data[$key]['shop_rating1'] = round(doubleval($row['shop_rating1']), 2);
    }

    $sort = array();
    foreach ($evaluation_data as $key => $row)
    {
        $sort[$key] = $row[$filter['sort_by']];
    }
    $sort_order  = $filter['sort_order'] == 'ASC' ? SORT_ASC : SORT_DESC;
    array_multisort($sort, $sort_order, $evaluation_data);

    foreach ($evaluation_data as $key => $item)
    {
        $evaluation_data[$key]['volume'] = price_format($item['volume']);
        
        $task_time = intval($item['task_time']);
        $task_time_formatted = '';
        if ($task_time >= 60)
        {
            $task_time_formatted .= floor($task_time / 60) . '小時';
            $task_time = $task_time % 60;
        }
        $task_time_formatted .= $task_time . '分鐘';
        $evaluation_data[$key]['task_time'] = $task_time_formatted;
    }
    $arr = array(
        'evaluation_data' => $evaluation_data,
        'filter' => $filter,
        'page_count' => $filter['page_count'],
        'record_count' => $filter['record_count']
    );
    return $arr;
}

?>