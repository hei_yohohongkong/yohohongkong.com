<?php

/***
* goods_feedback.php
* by howang 2015-06-25
*
* View goods feedback from customers
***/

define('IN_ECS', true);
require(dirname(__FILE__) . '/includes/init.php');


if ($_REQUEST['act'] == 'list_problem')
{
    $list = get_goods_feedbacks('problem');
    $smarty->assign('ur_here',      $_LANG['08_goods_feedback_problem']);
    $smarty->assign('full_page',    1);

    $smarty->assign('list',         $list['data']);
    $smarty->assign('filter',       $list['filter']);
    $smarty->assign('record_count', $list['record_count']);
    $smarty->assign('page_count',   $list['page_count']);
    
    $sql = "SELECT `sales_name` as `name`, `sales_name` as `value` " .
            "FROM " . $ecs->table('salesperson') .
            "WHERE `available` = 1 ";
    $operators = $db->getAll($sql);
    array_unshift($operators, array('name'=>'請選擇...', 'value'=>''));
    $smarty->assign('operators', $operators);
    
    $sort_flag  = sort_flag($list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);
    
    assign_query_info();
    $smarty->display('goods_feedback_problem_list.htm');
}
elseif ($_REQUEST['act'] == 'query_problem')
{
    $list = get_goods_feedbacks('problem');
    $smarty->assign('list',         $list['data']);
    $smarty->assign('filter',       $list['filter']);
    $smarty->assign('record_count', $list['record_count']);
    $smarty->assign('page_count',   $list['page_count']);

    $sort_flag  = sort_flag($list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);

    make_json_result($smarty->fetch('goods_feedback_problem_list.htm'), '', array('filter' => $list['filter'], 'page_count' => $list['page_count']));
}
elseif ($_REQUEST['act'] == 'list_price_suggestion')
{
    $list = get_goods_feedbacks('price');
    $smarty->assign('ur_here',      $_LANG['09_goods_feedback_price']);
    $smarty->assign('full_page',    1);

    $smarty->assign('list',         $list['data']);
    $smarty->assign('filter',       $list['filter']);
    $smarty->assign('record_count', $list['record_count']);
    $smarty->assign('page_count',   $list['page_count']);
    
    $sql = "SELECT `sales_name` as `name`, `sales_name` as `value` " .
            "FROM " . $ecs->table('salesperson') .
            "WHERE `available` = 1 ";
    $operators = $db->getAll($sql);
    array_unshift($operators, array('name'=>'請選擇...', 'value'=>''));
    $smarty->assign('operators', $operators);
    
    $sort_flag  = sort_flag($list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);
    
    assign_query_info();
    $smarty->display('goods_feedback_price_list.htm');
}
elseif ($_REQUEST['act'] == 'query_price_suggestion')
{
    $list = get_goods_feedbacks('price');
    $smarty->assign('list',         $list['data']);
    $smarty->assign('filter',       $list['filter']);
    $smarty->assign('record_count', $list['record_count']);
    $smarty->assign('page_count',   $list['page_count']);

    $sort_flag  = sort_flag($list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);

    make_json_result($smarty->fetch('goods_feedback_price_list.htm'), '', array('filter' => $list['filter'], 'page_count' => $list['page_count']));
}
elseif ($_REQUEST['act'] == 'edit_status')
{
    $feedback_id = intval($_POST['id']);
    $value = trim($_POST['val']);
    
    if (!in_array($value, array('pending','processing','finished','rejected')))
    {
        make_json_error('您輸入了一個非法的狀態');
    }
    else
    {
        $db->query("UPDATE " . $ecs->table('goods_feedback') . " " .
                    "SET `status` = '" . $value . "', " .
                        "`operate_time` = '" . gmtime() . "' " .
                    "WHERE `feedback_id` = '" . $feedback_id . "'");
        make_json_result(stripslashes($value));
    }
}
elseif ($_REQUEST['act'] == 'edit_operator')
{
    $feedback_id = intval($_POST['id']);
    $value = trim($_POST['val']);
    
    $db->query("UPDATE " . $ecs->table('goods_feedback') . " " .
                "SET `operator` = '" . $value . "', " .
                    "`operate_time` = '" . gmtime() . "' " .
                "WHERE `feedback_id` = '" . $feedback_id . "'");
    make_json_result(stripslashes($value));
}
elseif ($_REQUEST['act'] == 'edit_remark')
{
    $feedback_id = intval($_POST['id']);
    $value = trim($_POST['val']);
    
    $db->query("UPDATE " . $ecs->table('goods_feedback') . " " .
                "SET `remark` = '" . $value . "', " .
                    "`operate_time` = '" . gmtime() . "' " .
                "WHERE `feedback_id` = '" . $feedback_id . "'");
    make_json_result(stripslashes($value));
}
elseif (($_REQUEST['act'] == 'remove_problem') || ($_REQUEST['act'] == 'remove_price_suggestion'))
{
    $id = intval($_GET['id']);
    
    $sql = "DELETE FROM " . $ecs->table('goods_feedback') .
            " WHERE feedback_id = '".$id."' ";
    $db->query($sql);
    
    $url = 'goods_feedback.php?' . str_replace('act=remove_', 'act=query_', $_SERVER['QUERY_STRING']);
    
    header('Location: ' . $url);
    exit;
}

function get_goods_feedbacks($feedback_type)
{
    $filter['sort_by']      = empty($_REQUEST['sort_by']) ? 'feedback_id' : trim($_REQUEST['sort_by']);
    $filter['sort_order']   = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);

    $sql = "SELECT count(*) FROM " . $GLOBALS['ecs']->table('goods_feedback') . (empty($feedback_type) ? '' : " WHERE `feedback_type` = '" . $feedback_type . "'");
    $filter['record_count'] = $GLOBALS['db']->getOne($sql);

    /* 分页大小 */
    $filter = page_and_size($filter);
    
    $bottom_finished = "CASE WHEN status = 'finished' THEN 1 WHEN status = 'rejected' THEN 2 ELSE 0 END ASC, ";

    /* 查询 */
    $sql = "SELECT gf.*, g.`goods_sn`, g.`goods_name`, g.`goods_thumb`, g.`shop_price` " .
            "FROM " . $GLOBALS['ecs']->table('goods_feedback') . " as gf " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON g.goods_id = gf.goods_id " .
            (empty($feedback_type) ? '' : "WHERE `feedback_type` = '" . $feedback_type . "' ") .
            "ORDER BY " . $bottom_finished . $filter['sort_by'] . " " . $filter['sort_order'] . " " .
            "LIMIT " . $filter['start'] . "," . $filter['page_size'];
    $data = $GLOBALS['db']->getAll($sql);
    
    foreach ($data as $key => $row)
    {
        $data[$key]['add_date'] = local_date($GLOBALS['_CFG']['time_format'], $row['add_time']);
        $data[$key]['operate_date'] = local_date($GLOBALS['_CFG']['time_format'], $row['operate_time']);
        if ($row['status'] == 'pending')
        {
            $data[$key]['status_text'] = '未處理';
        }
        else if ($row['status'] == 'processing')
        {
            $data[$key]['status_text'] = '處理中';
        }
        else if ($row['status'] == 'finished')
        {
            $data[$key]['status_text'] = '已完成';
        }
        else if ($row['status'] == 'rejected')
        {
            $data[$key]['status_text'] = '不處理';
        }
        if ($row['feedback_type'] == 'price')
        {
            $data[$key]['shop_price_formatted'] = price_format($row['shop_price']);
            $data[$key]['content'] = json_decode($row['content'], true);
        }
    }
    
    $arr = array('data' => $data, 'filter' => $filter, 'page_count' => $filter['page_count'], 'record_count' => $filter['record_count']);
    
    return $arr;
}

?>