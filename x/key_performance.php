<?php

use function GuzzleHttp\json_encode;
use function Tinify\curl_close;
session_start();
define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require_once(ROOT_PATH . 'includes/lib_order.php');
require_once(ROOT_PATH . 'includes/Google_api_client/vendor/autoload.php');
require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/admin/statistic.php');
$baseController = new Yoho\cms\Controller\YohoBaseController();
$smarty->assign('lang', $_LANG);
$userController = new Yoho\cms\Controller\UserController();
$deliveryOrderController = new Yoho\cms\Controller\DeliveryOrderController();
$goodsController = new Yoho\cms\Controller\GoodsController();
/* act操作项的初始化 */
if (empty($_REQUEST['act']))
{
    $_REQUEST['act'] = 'list';
}
else
{
    $_REQUEST['act'] = trim($_REQUEST['act']);
}

$color_array = array('33FF66', 'FF6600', '3399FF', '009966', 'CC3399', 'FFCC33', '6699CC', 'CC3366');
$where =  "";
$where_u = "";
$where_rr = "";
$where_ex_wssa = " AND (oi.order_type NOT IN ('" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT . "','" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE . "') OR (oi.order_type IS NULL AND oi.user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).") ) ) " .
     " AND (oi.order_type <> '" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT . "' OR oi.order_type IS NULL) ";
$where_exclude = " AND oi.postscript NOT LIKE '%換貨，%' AND " . order_amount_field("oi.") . " > 0";
if(!empty($_REQUEST['user_start_date'])){
    $where_u .= " AND u.reg_time >= " . strtotime($_REQUEST['user_start_date']);
    $smarty->assign('user_start_date',$_REQUEST['user_start_date']);
}
if(!empty($_REQUEST['user_end_date'])){
    $where_u .= " AND u.reg_time < " . (strtotime($_REQUEST['user_end_date']) + 86400);
    $smarty->assign('user_end_date',$_REQUEST['user_end_date']);
}
if(!empty($_REQUEST['ship_start_date'])){
    $where .= " AND oi.shipping_time >= " . strtotime($_REQUEST['ship_start_date']);
    $smarty->assign('ship_start_date',$_REQUEST['ship_start_date']);
}
if(!empty($_REQUEST['ship_end_date'])){
    $where .= " AND oi.shipping_time < " . (strtotime($_REQUEST['ship_end_date']) + 86400);
    $where_u .= " AND u.reg_time < " . (strtotime($_REQUEST['ship_end_date']) + 86400);
    $smarty->assign('ship_end_date',$_REQUEST['ship_end_date']);
}
if(!empty($_REQUEST['refund_start_date'])){
    $where_rr .= " AND request_time >= " . strtotime($_REQUEST['refund_start_date']);
    $smarty->assign('refund_start_date',$_REQUEST['refund_start_date']);
}
if(!empty($_REQUEST['refund_end_date'])){
    $where_rr .= " AND request_time < " . (strtotime($_REQUEST['refund_end_date']) + 86400);
    $smarty->assign('refund_end_date',$_REQUEST['refund_end_date']);
}
if($_REQUEST['act'] == 'list'){
    $page = 0;
    $market = 0;
    if(isset($_REQUEST['page'])){
        $page = 1;
    }
    if(isset($_REQUEST['market'])){
        $market = intval($_REQUEST['market']);
    }

    if(isset($_SESSION['ga_access_token']) && isset($_SESSION['expiry']) && intval($_SESSION['expiry']) > gmtime()){
        $access_token = $_SESSION['ga_access_token'];
    } else {
        $KEY_FILE_LOCATION =  ROOT_PATH . 'data/yoho_service_account.json';
        $client = new Google_Client();
        $client->setApplicationName("Yoho GA Report");
        $client->setAuthConfig($KEY_FILE_LOCATION);
        $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
        $client->useApplicationDefaultCredentials();
        $client->fetchAccessTokenWithAssertion();
        $token = $client->getAccessToken();
        $access_token = $token['access_token'];
        $expiry = gmtime() + $token['expires_in'];
        $_SESSION['ga_access_token'] = $access_token;
        $_SESSION['expiry'] = $expiry;
    }
    if(!isset($_SESSION['fb_access_token'])){
        $fb_access_token = $_CFG['fb_access_token'];
        $_SESSION['fb_access_token'] = $fb_access_token;
    }else if(isset($_REQUEST['renew'])){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://graph.facebook.com/oauth/access_token?client_id=2026095777606738&client_secret=58cf3cc1e2ca3b43372d502cdddb87f0&grant_type=fb_exchange_token&fb_exchange_token=$_CFG[fb_access_token]");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        \curl_close($ch);
        if($result){
            $obj = json_decode($result, true);
            if ($obj['error']) {
                unset($_SESSION['fb_access_token']);
            } else {
                $fb_access_token = $obj['access_token'];
                $_SESSION['fb_access_token'] = $fb_access_token;
                $sql = "UPDATE " . $ecs->table('shop_config') . " SET value='$fb_access_token' WHERE code='fb_access_token'";
                $db->query($sql);
            }
        }else{
            // Update access token
            // 1. Go to https://developers.facebook.com/tools/explorer/2026095777606738 to generate new access token
            // 2. Update the access token in shop config
            unset($_SESSION['fb_access_token']);
        }
    }else{
        $fb_access_token = $_SESSION['fb_access_token'];
    }

    $smarty->assign('page', $page);
    $smarty->assign('market', $market);
    $smarty->assign('access_token', $access_token);
    $smarty->assign('fb_access_token', $fb_access_token);
    $smarty->assign('ur_here',$market ? $_LANG['report_kpi_market'] : $_LANG['report_kpi']);
    $smarty->assign('st_pos_email_xml',     $st_pos_email_xml);
    $smarty->display('key_performance.htm');
}else if($_REQUEST['act'] == 'ajax'){
    genData($_REQUEST['div_id']);
    $smarty->display('key_performance_chart.htm');
}
function genData($div_id){
    $xml = "";
    switch($div_id){
        case "pos_email": genEmailXML();break;
        case "refund": genRefund();break;
        case "pre_sale_days_all": genPreSaleDaysAll();break;
        case "pre_sale_days_sold": genLastMonthPreSaleDaysSold();break;
        case "delivery": genDeliveryFromPaid();break;
        case "aircall": genAircallStat();break;
    }
    return $xml;
}
function genEmailXML(){
    global $ecs,$db,$where_u,$color_array,$smarty;
    //門市註冊人數
    $sa_sql = 'SELECT u2.user_id FROM '.$ecs->table('order_info') . ' oi2 '.
    'LEFT JOIN '.$ecs->table('users') . ' u2 ON u2.user_id = oi2.user_id '.
    'WHERE oi2.order_id IN (SELECT MIN(order_id) FROM '.$ecs->table('order_info') . ' GROUP BY user_id) ' .
    'AND oi2.order_type = "sa" AND oi2.add_time - u2.reg_time < 120' . str_replace('u.','u2.',$where_u);
    $sql = 'SELECT COUNT(DISTINCT oi.user_id) as user_num FROM '.$ecs->table('order_info') . ' oi '.
    'LEFT JOIN '.$ecs->table('users') . ' u ON u.user_id = oi.user_id '.
    'WHERE (u.reg_time = oi.add_time OR u.email REGEXP "^\\\+?[0-9-]+(@yohohongkong.com)$") AND oi.platform = "pos" ' .
    ' AND u.user_id NOT IN (' . $sa_sql . ') ' . $where_u .where_exclude('oi.',true);
    $pos_register = $db->getOne($sql);
    $sql = 'SELECT COUNT(DISTINCT oi.user_id) as user_num FROM '.$ecs->table('order_info') . ' oi '.
    'LEFT JOIN '.$ecs->table('users') . ' u ON u.user_id = oi.user_id '.
    'WHERE (u.reg_time = oi.add_time AND u.email REGEXP "^\\\+?[0-9-]+(@yohohongkong.com)$") AND oi.platform = "pos"' .
    ' AND u.user_id NOT IN (' . $sa_sql . ') ' . $where_u .where_exclude('oi.',true);
    $pos_register_no_email = $db->getOne($sql);
    $chart['datasets'][0] = [
        'backgroundColor' => ['#'.chart_color(0), '#'.chart_color(1)],
    ];
    if (empty($pos_register - $pos_register_no_email) && empty($pos_register_no_email)) {
        $chart['datasets'][0]['data'] = [];
    } else {
        $chart['datasets'][0]['data'] = [$pos_register - $pos_register_no_email, $pos_register_no_email];
    }
    $chart['labels'] = ['有填電郵', '沒有電郵'];
    $arr = [
        'input' => 1,
        'start' => $_REQUEST['user_start_date'],
        'end' => $_REQUEST['user_end_date'],
        'start_date' => 'user_start_date',
        'end_date' => 'user_end_date',
        'submit' => 'user_submit',
        'submit_func' => "javascript:ajaxRetrieve('$_REQUEST[div_id]')",
        'title' => '註冊日期',
    ];
    $form['html'] = insert_daterange_group($arr);
    $smarty->assign('form', $form);
    $smarty->assign('chart', $chart);
}
function genRefund(){
    global $ecs,$db,$smarty,$where_rr,$baseController;
    $sql = "SELECT request_time, CASE WHEN update_time = 0 THEN UNIX_TIMESTAMP(NOW()) ELSE update_time END AS update_time, status FROM " . $ecs->table('refund_requests') . " WHERE rec_id > (SELECT MAX(rec_id) FROM " . $ecs->table('refund_requests') . " WHERE status IN(" . REFUND_FINISH . ", " . REFUND_REJECT . ") AND update_time = 0)" . $where_rr;
    $res = $db->getAll($sql);
    $time_required_t = array();
    $time_required_d = array();
    $completed = 0;
    $requested = count($res);
    foreach($res as $item){
        if (in_array($item['status'], array(REFUND_FINISH, REFUND_REJECT))) {
            $completed++;
        }
        $rt = (int) $item['request_time'];
        $ut = (int) $item['update_time'];
        $time_required_t[] = ($ut - $rt);
        $time_required_d[] = $baseController->count_yoho_work_day($ut,$rt,true);
    }
    $avg_rt = round(array_sum($time_required_t) / count($time_required_t) / 86400,1);
    $avg_rd = round(array_sum($time_required_d) / count($time_required_d),1);
    $table = array(
        'title' => '退款處理',
        'data' => array(
            '申請數'=>array($requested),
            '處理數'=>array($completed),
            '日數'=>array($avg_rt . "天"),
            '工作日數'=>array($avg_rd . "天")
        )
    );
    $arr = [
        'input' => 1,
        'start' => $_REQUEST['refund_start_date'],
        'end' => $_REQUEST['refund_end_date'],
        'start_date' => 'refund_start_date',
        'end_date' => 'refund_end_date',
        'submit' => 'refund_submit',
        'submit_func' => "javascript:ajaxRetrieve('$_REQUEST[div_id]')",
        'title' => '申請日期',
    ];
    $form['html'] = insert_daterange_group($arr);
    $smarty->assign('form', $form);
    $smarty->assign('tableArr', array($table));
}
function genPreSaleDaysAll(){
    global $ecs, $db, $smarty, $goodsController;
    $res = $goodsController->getGoodsPreSaleDaysAll();
    $table1 = array(
        'title' => '產品列表平均訂貨日數<small>(只計算訂貨天數, 不計算到貨日期)</small>',
        'data' => array('日數'=>round(($res['total_day']) / ($res['total_product']),1) . "天")
    );
    $sql = "SELECT pre_sale_days, COUNT(*) AS total_product FROM " . $ecs->table('goods') . " WHERE pre_sale_days > 0 GROUP BY pre_sale_days ORDER BY total_product DESC LIMIT 5";
    $res = $db->getAll($sql);
    foreach($res as $row){
        $arr[count($arr) + 1] = array($row['pre_sale_days'] . "天", $row['total_product']);
    }
    $table2 = array(
        'title' => '產品列表訂貨日數排行',
        'head' => array('排行','日數','貨品數'),
        'data' => $arr
    );
    $smarty->assign('tableArr', array($table1,$table2));
}

function genLastMonthPreSaleDaysSold()
{
    global $ecs,$db,$smarty,$baseController,$deliveryOrderController;
    function csort($a, $b)
    {
        if ($a['delivered'] == $b['delivered']) return 0;
        return $a['delivered'] < $b['delivered'] ? 1 : -1;
    }
    function csort2($a, $b)
    {
        if ($a['send_number'] == $b['send_number']) return 0;
        return $a['send_number'] < $b['send_number'] ? 1 : -1;
    }
    function process_data($arr, $step, $key = '', $param = [])
    {
        if ($step == 0) {
            if (!in_array($param[0], $arr[$key]['order'])) {
                $arr[$key]['delivered'] = empty($arr[$key]['delivered']) ? 1 : $arr[$key]['delivered'] + 1;
                $arr[$key]['order'][] = $param[0];
            }
            $delay_day = $param[2] > $param[3] ? $param[2] - $param[3] : 0;
            $before_day = $param[2] < $param[3] ? $param[3] - $param[2] : 0;
            if ($delay_day > 0) {
                $arr[$key]['delay_day'][] = $delay_day;
                if (!in_array($param[0], $arr[$key]['delay_order'])) {
                    if (($key2 = array_search($param[0], $arr[$key]['before_order'])) !== false) {
                        unset($arr[$key]['before_order'][$key2]);
                    }
                    if (($key2 = array_search($param[0], $arr[$key]['on_order'])) !== false) {
                        unset($arr[$key]['on_order'][$key2]);
                    }
                    $arr[$key]['delay_order'][] = $param[0];
                }
            } elseif ($before_day > 0) {
                $arr[$key]['before_day'][] = $before_day;
                if (!in_array($param[0], $arr[$key]['before_order']) && !in_array($param[0], $arr[$key]['delay_order']) && !in_array($param[0], $arr[$key]['on_order'])) {
                    $arr[$key]['before_order'][] = $param[0];
                }
            } else {
                if (!in_array($param[0], $arr[$key]['on_order']) && !in_array($param[0], $arr[$key]['delay_order'])) {
                    if (($key2 = array_search($param[0], $arr[$key]['before_order'])) !== false) {
                        unset($arr[$key]['before_order'][$key2]);
                    }
                    $arr[$key]['on_order'][] = $param[0];
                }
            }
            $arr[$key]['total_day'][] = $param[2];
            $arr[$key]['raw_data'][] = [
                'order_id' => $param[0],
                'goods_id' => $param[1],
                'actual_day' => $param[2],
                'estimate_day' => $param[3],
                'goods_name' => $param[4],
                'send_number' => $param[5],
            ];
        } elseif ($step == 1) {
            foreach ($arr as $k => $row) {
                $goods = [];
                $arr[$k]['delay'] = count($row['delay_order']);
                $arr[$k]['before'] = count($row['before_order']);
                $arr[$k]['on'] = count($row['on_order']);
                $arr[$k]['delay_percent'] = round($arr[$k]['delay'] / $row['delivered'] * 100, 1);
                $arr[$k]['before_percent'] = round($arr[$k]['before'] / $row['delivered'] * 100, 1);
                $arr[$k]['delay_avg'] = round(array_sum($row['delay_day']) / count($row['delay_day']), 1);
                $arr[$k]['before_avg'] = round(array_sum($row['before_day']) / count($row['before_day']), 1);
                $arr[$k]['all_avg'] = round(array_sum($row['total_day']) / $row['delivered'], 1);
                foreach ($row['raw_data'] as $data) {
                    $goods[$data['goods_id']]['delivered'] = empty($goods[$data['goods_id']]['delivered']) ? 1 : $goods[$data['goods_id']]['delivered'] + 1;
                    $goods[$data['goods_id']]['send_number'] = empty($goods[$data['goods_id']]['send_number']) ? $data['send_number'] : $goods[$data['goods_id']]['send_number'] + $data['send_number'];
                    $goods[$data['goods_id']]['goods_name'] = $data['goods_name'];
                }
                uasort($goods, 'csort2');
                $arr[$k]['goods_remark'] = "\"<table class='dataTable table jambo_table table-striped table-bordered bulk_action'><tr><th>產品</th><th>數量</th></tr>";
                $count = 0;
                foreach ($goods as $goods_id => $good) {
                    if ($count < 10) {
                        $g_name = str_replace("\"", "", $good['goods_name']);
                        $arr[$k]['goods_remark'] .= "<tr><td><a target='_blank' href='goods.php?act=list&keyword=$g_name'>$g_name</a></td><td>$good[send_number]</td></tr>";
                        $count++;
                    } else {
                        unset($goods[$goods_id]);
                    }
                }
                $arr[$k]['goods_remark'] .= "</table><div class='clearfix'></div>\"";
                if (count($goods) == 0) {
                    $arr[$k]['goods_remark'] = "";
                }
            }
            uasort($arr, 'csort');
        }
        return $arr;
    }
    $_REQUEST['filter'] = empty($_REQUEST['filter']) ? '較少' : trim($_REQUEST['filter']);
    $filter = $_REQUEST['filter'] == '較多' ? 0 : 1;
    $date_start = strtotime(!empty($_REQUEST['ship_start_date']) ? $_REQUEST['ship_start_date'] : "-7 days midnight");
    $_REQUEST['ship_start_date'] = date("Y-m-d", $date_start);
    $date['start_date'] = $_REQUEST['ship_start_date'];
    $date_end = !empty($_REQUEST['ship_end_date']) ? strtotime($_REQUEST['ship_end_date']) + 86399 : strtotime("today midnight") - 1;
    $_REQUEST['ship_end_date'] = date("Y-m-d", $date_end);
    $date['end_date'] = $_REQUEST['ship_end_date'] . ' 23:59:59';
    $sql_delivered = "SELECT og.order_id, og.goods_id, og.goods_name, oi.pay_time, do.update_time, og.planned_delivery_date, og.rec_id, ogsc.supplier_id, ogsc.admin_id, ogsc.consumed_qty FROM " . $ecs->table('delivery_order') . " do " .
    "INNER JOIN " . $ecs->table('delivery_goods') . " dg ON dg.delivery_id = do.delivery_id " .
    "INNER JOIN " . $ecs->table('order_goods') . " og ON og.rec_id = dg.order_item_id " .
    "INNER JOIN " . $ecs->table('order_info') . " oi ON oi.order_id = og.order_id " .
    "INNER JOIN (SELECT supplier_id, order_goods_id, consumed_qty, MAX(admin_id) AS admin_id FROM (SELECT esa.admin_id, ogsc2.* FROM  " . $ecs->table('erp_supplier_admin') . " esa LEFT JOIN (SELECT supplier_id, order_goods_id, MAX(consumption_id) AS consumption_id, consumed_qty FROM " . $ecs->table('order_goods_supplier_consumption') . " GROUP BY order_goods_id, supplier_id) ogsc2 ON esa.supplier_id = ogsc2.supplier_id) ogsc3 GROUP BY order_goods_id, supplier_id) ogsc ON ogsc.order_goods_id = og.rec_id " .
    "WHERE do.update_time BETWEEN $date_start AND $date_end AND dg.send_number > 0 AND og.planned_delivery_date > 0 AND ogsc.consumed_qty > 0 AND oi.shipping_status != 0 group by order_id, rec_id, supplier_id";
    $res_delivered = $db->getAll($sql_delivered);
    $res = $deliveryOrderController->countAdminDelayShip($date);

    foreach ($res_delivered as $row) {
        $actual_day = $baseController->count_yoho_work_day($row['update_time'], $row['pay_time'], true);
        $estimate_day = $baseController->count_yoho_work_day($row['planned_delivery_date'], $row['pay_time']);

        if ($estimate_day == 1) {
            $pay_time = $db->getOne("SELECT MIN(log_time) FROM " . $ecs->table('order_action') . " WHERE order_id = $row[order_id] AND log_time < $row[pay_time] AND log_time < $row[planned_delivery_date] AND pay_status = 2 GROUP BY order_id");
            if (!empty($pay_time)) {
                $actual_day = $baseController->count_yoho_work_day($row['update_time'], $pay_time, true);
                $estimate_day = $baseController->count_yoho_work_day($row['planned_delivery_date'], $pay_time);
            }
        }

        if ($actual_day > $estimate_day) {
            $day = $actual_day - $estimate_day;
            $over_day[] = $day;
            if (empty($over_day_order[$row['order_id']]) || $over_day_order[$row['order_id']] < $day) {
                $over_day_order[$row['order_id']] = $day;
            }
        }
        $over_day_unique = array_values($over_day_order);
        $supplier = process_data($supplier, 0, $row['supplier_id'], [$row['order_id'], $row['goods_id'], $actual_day, $estimate_day, $row['goods_name'], $row['consumed_qty']]);
        $admin = process_data($admin, 0, $row['admin_id'], [$row['order_id'], $row['goods_id'], $actual_day, $estimate_day, $row['goods_name'], $row['consumed_qty']]);
    }

    $supplier = process_data($supplier, 1);
    $admin = process_data($admin, 1);
    $count = 0;

    foreach ($supplier as $key => $row) {
        if ($filter && $count > 10) {
            unset($supplier[$key]);
        } else {
            $name = $db->getOne("SELECT name FROM ". $ecs->table('erp_supplier') . " WHERE supplier_id = $key");
            $supplier_table[] = [$name, "<a data-tips=$row[goods_remark] onclick='javascript:showRemark(this)' style='cursor: pointer;'>$row[delivered]</a>", "$row[delay] ($row[delay_percent]%)", "$row[on] / $row[before] ($row[before_percent]%)", $row['delay_avg']];
            $count++;
        }
    }
    foreach ($admin as $key => $row) {
        $name = $db->getOne("SELECT user_name FROM ". $ecs->table('admin_user') . " WHERE user_id = $key");
        $admin_table[] = [$name, "<a data-tips=$row[goods_remark] onclick='javascript:showRemark(this)' style='cursor: pointer;'>".$res[$key]['total']."</a>", $res[$key]['delay']." (".$res[$key]['delay_rate']."%)", $res[$key]['on_time']." / ".$res[$key]['early'], $row['all_avg'], $row['delay_avg']];
    }

    $od = array_count_values($over_day);
    $odu = array_count_values($over_day_unique);
    $over_day_sort = array();
    foreach ($od as $k => $i) {
        if ($k < 6) {
            $over_day_sort[$k . "天"][0] = $i;
            $over_day_sort[$k . "天"][1] = empty($odu[$k]) ? 0 : $odu[$k];
        } else {
            $over_day_sort[">5天"][0] += $i;
            $over_day_sort[">5天"][1] += empty($odu[$k]) ? 0 : $odu[$k];
        }
    }
    ksort($over_day_sort);

    $table1 = array(
        'title' => '負責人訂貨表現',
        'head' => array('負責人', '產品數目', '延誤出貨', '準時出貨<br>(準時 / 提前)', '平均訂貨工作日數<br>(所有訂單)', '平均延誤工作日數<br>(只計算延誤訂單)'),
        'table_remark' => array(
            '計算' => '1) 同一訂單含多個供應商/負責人會分別計算<br>2) 除 1) 的情況, 同一訂單含多件需訂貨產品只會計算1次<br>3) 同一訂單含延誤出貨及準時出貨會以延誤出貨計算',
            '誤差' => '1) 無法計算門市購買而需訂貨的產品<br>2) 訂單曾多次付款或出貨較不準確',
        ),
        'data' => $admin_table,
        'no_th' => 1
    );
    $table2 = array(
        'title' => '供應商訂貨表現',
        'head' => array('供應商', '訂單數目', '延誤出貨', '準時出貨<br>(準時 / 提前)', '平均延誤工作日數<br>(只計算延誤訂單)'),
        'data' => $supplier_table,
        'no_th' => 1,
        'button' => array(
            array(
                'name' => 'filter',
                'text' => $_REQUEST['filter'],
                'func'  => '$("input[name=filter]").val($("input[name=filter]").val() == "較多"? "較少" : "較多");ajaxRetrieve("pre_sale_days_sold");'
            ),
        )
    );
    $table3 = array(
        'id' => 'delay_pre_order',
        'title' => '出貨延誤情況',
        'style' => 'float: right; width:30%',
        'head' => array('延誤日數','產品數','訂單數'),
        'table_remark' => array(
            '計算' => '訂單數按出貨延誤最嚴重的一件計算',
        ),
        'data' => $over_day_sort
    );
    $arr = [
        'input' => 1,
        'start' => $_REQUEST['ship_start_date'],
        'end' => $_REQUEST['ship_end_date'],
        'start_date' => 'ship_start_date',
        'end_date' => 'ship_end_date',
        'submit' => 'ship_submit',
        'submit_func' => "javascript:ajaxRetrieve('pre_sale_days_sold-delivery')",
        'title' => '出貨日期',
    ];
    $form['html'] = insert_daterange_group($arr);
    $script = "$('#pre_sale_days_overall').html('');$('#delay_pre_order').appendTo('#pre_sale_days_overall')";
    $smarty->assign('form', $form);
    $smarty->assign('script', $script);
    $smarty->assign('tableArr', array($table1, $table2, $table3));
}
function genDeliveryFromPaid()
{
    global $ecs,$db,$smarty,$baseController;
    function get_total_days ($arr)
    {
        $count = 0;
        foreach ($arr as $key => $value) {
            $count += intval($key) * intval($value);
        }
        return $count;
    }
    $date_start = strtotime(!empty($_REQUEST['ship_start_date']) ? $_REQUEST['ship_start_date'] : "-7 days midnight");
    $_REQUEST['ship_start_date'] = date("Y-m-d", $date_start);
    $date_end = !empty($_REQUEST['ship_end_date']) ? strtotime($_REQUEST['ship_end_date']) + 86399 : strtotime("today midnight") - 1;
    $_REQUEST['ship_end_date'] = date("Y-m-d", $date_end);

    $sql_delivery = "SELECT og.rec_id, og.order_id, og.goods_id, og.goods_name, oi.pay_time, oi.order_sn, do.update_time, dg.send_number, aol.operator, aol.log_id FROM " . $ecs->table('delivery_order') . " do " .
            "INNER JOIN " . $ecs->table('delivery_goods') . " dg ON dg.delivery_id = do.delivery_id " .
            "INNER JOIN " . $ecs->table('order_goods') . " og ON og.rec_id = dg.order_item_id " .
            "INNER JOIN " . $ecs->table('order_info') . " oi ON oi.order_id = og.order_id " .
            "INNER JOIN (SELECT operator, order_id, log_id FROM " . $ecs->table('admin_operation_log') . " WHERE operation = '訂單發貨')" ." aol ON aol.order_id = og.order_id " .
            "WHERE do.update_time BETWEEN $date_start AND $date_end AND dg.send_number > 0 AND oi.shipping_status != 0 AND oi.platform IN ('mobile', 'web') AND (oi.order_type IS NULL OR oi.order_type != 'ws') AND oi.shipping_id != 5 AND (SELECT COUNT(*) FROM " . $ecs->table('order_goods') . " WHERE planned_delivery_date != 0 AND order_id = og.order_id) = 0 ORDER BY og.rec_id DESC, aol.log_id DESC";
    $res_delivery = $db->getAll($sql_delivery);

    foreach ($res_delivery as $key => $row) {
        if (in_array($row['rec_id'], $rec_ids)) {
            unset($res_delivery[$key]);
        } else {
            $rec_ids[] = $row['rec_id'];
        }
    }
    foreach ($res_delivery as $row) {
        $day = $baseController->count_yoho_work_day($row['update_time'], $row['pay_time'], true);
        $pay_time = $row['pay_time'];
        if ($row['update_time'] - $row['pay_time'] < 0) {
            $pay_time = $db->getOne("SELECT MIN(log_time) FROM " . $ecs->table('order_action') . " WHERE order_id = $row[order_id] AND log_time < $row[pay_time] AND pay_status = 2 GROUP BY order_id");
            if (!empty($pay_time)) {
                $day = $baseController->count_yoho_work_day($row['update_time'], $pay_time, true);
            } else {
                $pay_time = $row['pay_time'];
            }
        }

        $order[$row['order_id']] = [
            'order_sn' => $row['order_sn'],
            'send_number' => $row['send_number'],
        ];
        if ($row['update_time'] - $pay_time < 86400) {
            $day = 0;
        }
        if (empty($operator[$row['operator']]['order'][$row['order_id']]) || $day > $operator[$row['operator']]['order'][$row['order_id']]) {
            $operator[$row['operator']]['order'][$row['order_id']] =  $day;
        }
    }
    foreach ($operator as $key => $row) {
        arsort($operator[$key]['order']);
        $operator[$key]['count'] = count($operator[$key]['order']);
        $inspect = [];
        foreach ($operator[$key]['order'] as $order_id => $value) {
            empty($operator[$key]['performance'][$value]) ? $operator[$key]['performance'][$value] = 1 : $operator[$key]['performance'][$value]++;
            empty($overall[$value]) ? $overall[$value] = 1 : $overall[$value]++;
            $overall_product[$value] = empty($overall_product[$value]) ? $order[$order_id]['send_number'] : $overall_product[$value] + $order[$order_id]['send_number'];
            if (count($inspect) < 20 && $value > 1) {
                $inspect[] = "<tr><td><a target='_blank' href='order.php?act=info&order_id=$order_id'>" . $order[$order_id]['order_sn'] . "</a></td><td>$value</td></tr>";
            }
        }
        $operator[$key]['avg_performance'] = round(get_total_days($operator[$key]['performance']) / $operator[$key]['count'], 1);
        $operator[$key]['worst'] = max(array_keys($operator[$key]['performance']));
        $operator[$key]['inspect'] = "\"<table class='table table-stripped'><thead><tr><th>訂單號</th><th>出貨日數</th></tr></thead><tbody>" . implode("", $inspect) . "</tbody></table>\"";
    }
    foreach ($operator as $key => $row) {
        $operator_table[] = [$key, $row['count'], "<a data-tips=$row[inspect] onclick='javascript:showRemark(this, \"md\")' style='cursor: pointer;'>$row[avg_performance]</a>", $row['worst']];
    }

    $sql_pickpack = "SELECT og.rec_id, og.order_id, oi.order_sn, do.update_time, dg.send_number, opp.picker_user_ids, opp.packer_user_ids FROM " . $ecs->table('delivery_order') . " do " .
            "INNER JOIN " . $ecs->table('delivery_goods') . " dg ON dg.delivery_id = do.delivery_id " .
            "INNER JOIN " . $ecs->table('order_goods') . " og ON og.rec_id = dg.order_item_id " .
            "INNER JOIN " . $ecs->table('order_info') . " oi ON oi.order_id = og.order_id " .
            "INNER JOIN " . $ecs->table('order_picker_packer') . " opp ON opp.order_sn = oi.order_sn " .
            "WHERE do.update_time BETWEEN $date_start AND $date_end AND dg.send_number > 0 AND oi.shipping_status != 0 ORDER BY og.rec_id DESC";
    $res_pickpack = $db->getAll($sql_pickpack);

    foreach ($res_pickpack as $row) {
        $picks = [];
        $packs = [];
        if (!empty($row['picker_user_ids'])) {
            $picks = explode(",", $row['picker_user_ids']);
            foreach ($picks as $pick) {
                if (!in_array($pick, $pick_packs)) {
                    $pick_packs[] = $pick;
                }
            }
        }
        if (!empty($row['packer_user_ids'])) {
            $packs = explode(",", $row['packer_user_ids']);
            foreach ($packs as $pack) {
                if (!in_array($pack, $pick_packs)) {
                    $pick_packs[] = $pack;
                }
            }
        }
        $pickpack_order[$row['order_id']] = [
            'order_sn' => $row['order_sn'],
            'send_number' => $row['send_number'],
            'pickers' => $picks,
            'packers' => $packs,
        ];
    }
    foreach ($pickpack_order as $order_id => $row) {
        $order_picks = $row['pickers'];
        $order_packs = $row['packers'];
        if (!empty($order_picks)) {
            foreach ($order_picks as $pick) {
                $picker[$pick]['qty'] = empty($picker[$pick]['qty']) ? $row['send_number'] : $picker[$pick]['qty'] + $row['send_number'];
                if (!in_array($order_id, $picker[$pick]['order'])) {
                    $picker[$pick]['order'][] = $order_id;
                }
            }
        }
        if (!empty($order_packs)) {
            foreach ($order_packs as $pack) {
                $packer[$pack]['qty'] = empty($packer[$pack]['qty']) ? $row['send_number'] : $packer[$pack]['qty'] + $row['send_number'];
                if (!in_array($order_id, $packer[$pack]['order'])) {
                    $packer[$pack]['order'][] = $order_id;
                }
            }
        }
    }

    foreach ($pick_packs as $user) {
        $picker_packer[$user] = $db->getOne("SELECT user_name FROM " . $ecs->table('admin_user') . " WHERE user_id = $user");
    }
    foreach ($picker as $user => $row) {
        $picker_table[] = [$picker_packer[$user], count($row['order']), $row['qty']];
    }
    foreach ($packer as $user => $row) {
        $packer_table[] = [$picker_packer[$user], count($row['order']), $row['qty']];
    }

    foreach ($overall as $k => $i) {
        if ($k == 0) {
            $key = 0;
        } elseif ($k == 1) {
            $key = 1;
        } elseif ($k == 2) {
            $key = 2;
        } elseif ($k <= 5) {
            $key = 3;
        } elseif ($k <= 10) {
            $key = 4;
        } else {
            $key = 5;
        }
        $overall_sort[$key][0] += $i;
        $overall_sort[$key][1] += $overall_product[$k];
    }
    ksort($overall_sort);
    foreach ($overall_sort as $key => $val) {
        switch ($key) {
            case 0: $k = '即日'; break;
            case 3: $k = '3-5天'; break;
            case 4: $k = '6-10天'; break;
            case 5: $k = '>10天'; break;
            default: $k = $key . '天'; break;
        }
        $overall_table[$k] = $val;
    }

    $sql_serial_number = 'select count(*) from '.$ecs->table('order_action').' where log_time BETWEEN '.$date_start.' AND '.$date_end.' and action_note = "[系統備註] 備註更新(產品序號)"  ';
    $serial_number_total = $db->getOne($sql_serial_number);
    $order_product_serial_number = array('總數'=>$serial_number_total);

    $erpController = new \Yoho\cms\Controller\ErpController();
    $warehousing_table = $erpController->incoming_warehousing_report(['start_date'=>$_REQUEST['ship_start_date'], 'end_date'=>$_REQUEST['ship_end_date']], 0);

    $table1 = array(
        'title' => '負責人出貨表現',
        'head' => array('負責人', '訂單數目', '平均出貨工作日數', '最長出貨工作日數'),
        'table_remark' => array(
            '計算' => '1) 只計算網上及手機下的速遞零售單(不包含批發單、需訂貨訂單、門店自取訂單)<br>2) 同一訂單同一產品含多次出貨只計算最新一條紀錄',
            '誤差' => '1) 計算包含系統沒有標示需訂貨/需延遲出貨的訂單<br>2) 訂單曾多次付款或出貨較不準確',
        ),
        'data' => $operator_table,
        'no_th' => 1
    );
    $table2 = array(
        'id' => 'delay_delivery',
        'title' => '訂單平均出貨日數',
        'head' => array('工作日數', '訂單數目', '出貨量'),
        'data' => $overall_table,
    );
    $table3 = array(
        'id' => 'delay_picker',
        'title' => '執貨人表現',
        'head' => array('負責人', '訂單數目', '出貨量'),
        'data' => empty($picker_table) ? [] : $picker_table,
        'no_th' => 1
    );
    $table4 = array(
        'id' => 'delay_packer',
        'title' => 'Pack貨人表現',
        'head' => array('負責人', '訂單數目', '出貨量'),
        'data' => empty($packer_table) ? [] : $packer_table,
        'no_th' => 1
    );
    $table5 = array(
        'id' => 'delay_serial_number',
        'title' => '使用條碼機入產品序號',
        'data' => empty($order_product_serial_number) ? [] : $order_product_serial_number,
        'no_th' => 0
    );
    $table6 = array(
        'id' => 'delay_warehousing',
        'title' => '入貨表現',
        'head' => array('負責人', '入貨單數目', '入貨量'),
        'data' => empty($warehousing_table) ? [] : $warehousing_table,
        'no_th' => 1
    );
    $arr = [
        'input' => 1,
        'start' => $_REQUEST['ship_start_date'],
        'end' => $_REQUEST['ship_end_date'],
        'start_date' => 'ship_start_date',
        'end_date' => 'ship_end_date',
        'submit' => 'ship_submit2',
        'submit_func' => "javascript:ajaxRetrieve('delivery-pre_sale_days_sold')",
        'title' => '出貨日期',
    ];
    $form['html'] = insert_daterange_group($arr);
    $script = "$('#delivery_overall').html('');$('#delay_delivery').appendTo('#delivery_overall');" .
                "$('#picker').html('');$('#delay_picker').appendTo('#picker');" .
                "$('#packer').html('');$('#delay_packer').appendTo('#packer');" .
                "$('#serial_number').html('');$('#delay_serial_number').appendTo('#serial_number');" .
                "$('#warehousing').html('');$('#delay_warehousing').appendTo('#warehousing');";
                $smarty->assign('form', $form);
    $smarty->assign('script', $script);
    $smarty->assign('tableArr', array($table1, $table2, $table3, $table4, $table5, $table6));
}
function genAircallStat()
{
    global $db, $ecs, $smarty;
    $start_time = date("Y-m-d", strtotime("-7 days"));
    $end_time = date("Y-m-d", strtotime("today"));
    $arr = [];
    $arr2 = [];
    $time_section = ["11:00:00", "19:00:00"];
    $datail_time_section = ["00:00 - 11:00", "11:00 - 19:00", "19:00 - 24:00"];

    while (count($arr) < 7) {
        $arr[date("Y-m-d", strtotime("-" . (7 - count($arr)) . " days"))] = [[], [], []];
    }
    $list = $db->getAll(
        "SELECT ticket_id, create_at, " .
        "IFNULL((SELECT 1 FROM " . $ecs->table("crm_aircall_queue") . " WHERE unique_id = call_id), 0) as missed " .
        "FROM " . $ecs->table("crm_list") .
        " WHERE ticket_id IN (SELECT ticket_id FROM " . $ecs->table("crm_ticket_info") . " WHERE platform = " . CRM_PLATFORM_AIRCALL . ") " .
        " AND create_at BETWEEN '$start_time' AND '$end_time' ORDER BY create_at ASC"
    );
    foreach ($list as $row) {
        $create_timestamp = strtotime($row['create_at']);
        $date_section = date("Y-m-d", $create_timestamp);
        if ($create_timestamp < strtotime($date_section . $time_section[0])) {
            $arr[$date_section][0][$row['ticket_id']] = $row['missed'];
        } elseif ($create_timestamp < strtotime($date_section . $time_section[1])) {
            $arr[$date_section][1][$row['ticket_id']] = $row['missed'];
        } else {
            $arr[$date_section][2][$row['ticket_id']] = $row['missed'];
        }
    }
    foreach ($arr as $date => $data) {
        if (!in_array($date, array_keys($arr2))) {
            $arr2[$date] = [
                [
                    'total' => count($data[0]),
                    'answer' => 0,
                ], [
                    'total' => count($data[1]),
                    'answer' => 0,
                ], [
                    'total' => count($data[2]),
                    'answer' => 0,
                ]
            ];
        }
        foreach ($data as $key => $row) {
            foreach ($row as $val) {
                if ($val == 0) {
                    $arr2[$date][$key]['answer']++;
                }
            }
        }
    }
    foreach (array_keys($arr2) as $date) {
        foreach ($arr2[$date] as $key => $data) {
            $arr3[$datail_time_section[$key]][] = "$data[answer] / $data[total]" . ($key == 1 ? (" (" . round($data['answer'] / $data['total'] * 100) . "%)") : "");
        }
    }
    $table1 = array(
        'title' => '過去7日表現 - Aircall',
        'head' => array_merge([""], array_keys($arr2)),
        'data' => $arr3
    );
    $smarty->assign('tableArr', array($table1));
}
function createDateRange($start_name, $end_name, $label = "")
{
    return (empty($label) ? "" : "<span class='input-group-addon alert-info'>$label</span>") .
        "<span class='input-group-addon'>由</span>" .
        "<input type='text' class='form-control bind_date' data-type='date' name='$start_name' value='$_REQUEST[$start_name]'>" .
        "<span class='input-group-addon'>至</span>" .
        "<input type='text' class='form-control bind_date' data-type='date' name='$end_name' value='$_REQUEST[$end_name]'>";
}
?>