<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_base.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_warehouse.php');
require(dirname(__FILE__) .'/includes/ERP/cls/cls_date.php');
$stocktakeController = new Yoho\cms\Controller\StocktakeController();
$adminuserController = new Yoho\cms\Controller\AdminuserController();

if($_REQUEST['act'] == 'list' || $_REQUEST['act'] == ''){
	if( admin_priv('erp_warehouse_view','',false) || admin_priv('erp_warehouse_manage','',false) || admin_priv('erp_warehouse_approve','',false))
	{
        include('./includes/ERP/page.class.php');
		
		$smarty->assign('type_options', $stocktakeController->getTypeOptions());
        $smarty->assign('status_options', $stocktakeController->getStocktakeStatusOptions());
        $smarty->assign('ur_here', '存貨盤點');
        $smarty->assign('full_page', 1);
        $smarty->assign('start_date', $result['start_date']);
        $smarty->assign('end_date', $result['end_date']);
        $smarty->assign('cfg_lang', $_CFG['lang']);
        $smarty->assign('lang', $_LANG);
        $smarty->display('erp_stocktake_list.htm');
    }else{
        $href="index.php";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
    }

} elseif ($_REQUEST['act'] == 'query') {
    $result = $stocktakeController->getStocktakeList();
    $stocktakeController->ajaxQueryAction($result['data'],$result['record_count'],false);
} elseif ($_REQUEST['act'] == 'post_bulk_stocktake_type_change') {
    $result = $stocktakeController->bulkStocktakeTypeChange();
	if($result !== false){
		make_json_result($result);
	}else{
		make_json_error('更新時發生錯誤');
	}
} elseif ($_REQUEST['act'] == 'post_stocktake_type_change') {
    $result = $stocktakeController->stocktakeTypeChange();
	if($result !== false){
		make_json_result($result);
	}else{
		make_json_error('更新時發生錯誤');
	}
}


?>