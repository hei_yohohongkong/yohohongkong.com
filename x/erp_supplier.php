<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_base.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_supplier.php');
if($_REQUEST['act'] == 'list')
{
	if((admin_priv('erp_sys_manage','',false)))
	{
		// include('./includes/ERP/page.class.php');
		// $page=isset($_REQUEST['page']) &&intval($_REQUEST['page']) >0 ?intval($_REQUEST['page']):1;
		// $group_id=isset($_REQUEST['group_id']) &&intval($_REQUEST['group_id']) >0 ?intval($_REQUEST['group_id']):0;
		// $keyword=isset($_REQUEST['keyword'])?trim($_REQUEST['keyword']):'';
		// $num_per_page=20;
		// $mode=1;
		// $page_bar_num=6;
		// $page_style="page_style";
		// $current_page_style="current_page_style";
		// $start=$num_per_page*($page-1);
		// $paras=array('group_id'=>$group_id,'keyword'=>$keyword);
		// $total_num=supplier_count($paras);
		// $supplier_info=supplier_list($paras,$start,$num_per_page);
		// $smarty->assign('supplier_info',$supplier_info);
		// $url=fix_url($_SERVER['REQUEST_URI'],array('page'=>'','group_id'=>$group_id,'keyword'=>$keyword));
		// $pager=new page(array('total_data'=>$total_num,'data_per_page'=>$num_per_page,'url'=>$url,'mode'=>$mode,'page_bar_num'=>$page_bar_num,'page_style'=>$page_style,'current_page_style'=>$current_page_style));
		// $smarty->assign('pager',$pager->show());

		$supplier_list = get_supplier_list();
		$sql="select esa.admin_id, au.user_name from ".$GLOBALS['ecs']->table('erp_supplier_admin')." as esa ".
		"LEFT JOIN ".$GLOBALS['ecs']->table('admin_user')." as au ON au.user_id = esa.admin_id ".
		" where esa.admin_id > 0 AND au.user_name IS NOT NULL group BY admin_id";
		$res=$GLOBALS['db']->query($sql);
		while($row=mysql_fetch_assoc($res))
		{
			$admin_list[]=$row;
		}
		$smarty->assign('admin_list',       $admin_list);
	    $smarty->assign('supplier_info', $supplier_list['supplier_info']);
	    $smarty->assign('filter',        $supplier_list['filter']);
	    $smarty->assign('record_count',  $supplier_list['record_count']);
	    $smarty->assign('page_count',    $supplier_list['page_count']);
		$smarty->assign('group_id',      $_REQUEST['group_id']);
		$smarty->assign('admin_id',      $_REQUEST['admin_id']);
		$smarty->assign('keyword',       $_REQUEST['keyword']);
		$smarty->assign('full_page',     1);
		
		$action_link = array('href'=>'erp_supplier.php?act=add','text'=>$_LANG['erp_add_supplier']);
		$action_link2 = array('href'=>'credit_note_report.php?act=summary','text'=>$_LANG['erp_credit_summary']);
		$smarty->assign('action_link',$action_link);
		$smarty->assign('action_link2',$action_link2);
		$smarty->assign('ur_here',$GLOBALS['_LANG']['erp_supplier_list']);
		$smarty->assign('group_list',supplier_group_list());
		
		assign_query_info();
		$smarty->display('erp_supplier_list.htm');
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif ($_REQUEST['act'] == 'query')
{
	check_authz_json('erp_sys_manage');
	
	$supplier_list = get_supplier_list();
    $smarty->assign('supplier_info', $supplier_list['supplier_info']);
    $smarty->assign('filter',        $supplier_list['filter']);
    $smarty->assign('record_count',  $supplier_list['record_count']);
    $smarty->assign('page_count',    $supplier_list['page_count']);
	$smarty->assign('group_id',      $_REQUEST['group_id']);
	$smarty->assign('admin_id',      $_REQUEST['admin_id']);
	$smarty->assign('keyword',       $_REQUEST['keyword']);

    make_json_result($smarty->fetch('erp_supplier_list.htm'), '', array('filter' => $supplier_list['filter'], 'page_count' => $supplier_list['page_count']));
}
elseif($_REQUEST['act'] == 'change_is_valid')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_sys_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	if(empty($_REQUEST['supplier_id']) && !empty($_REQUEST['id'])) // listTable flow
	{
		$_REQUEST['supplier_id'] = $_REQUEST['id'];
	}
	if(empty($_REQUEST['supplier_id']))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	else
	{
		$supplier_id=intval($_REQUEST['supplier_id']);
		if (isset($_REQUEST['val'])) // listTable flow
		{
			$is_valid = intval($_REQUEST['val']) == 1 ? 1 : 0;
			$result['content'] = $is_valid;
		}
		else // old ERP flow
		{
			$sql="select is_valid from ".$ecs->table('erp_supplier')." where supplier_id='".$supplier_id."'";
			$is_valid = intval($db->getOne($sql)) == 1 ? 0 : 1;
			$result['is_valid'] = $is_valid;
		}
		$sql="update ".$ecs->table('erp_supplier')." set is_valid='".$is_valid."' where supplier_id='".$supplier_id."'";
		$db->query($sql);
		$result['error']=0;
		die($json->encode($result));
	}
}
elseif($_REQUEST['act'] == 'remove')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_sys_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	if(empty($_REQUEST['supplier_id']) && !empty($_REQUEST['id'])) // listTable flow
	{
		$_REQUEST['supplier_id'] = $_REQUEST['id'];
	}
	if(empty($_REQUEST['supplier_id']))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	else
	{
		$supplier_id=intval($_REQUEST['supplier_id']);
		$supplier_info=is_supplier_exist($supplier_id);
		if($supplier_info!==false)
		{
			$sql="select count(*) as order_number from ".$ecs->table('erp_order')." where supplier_id='".$supplier_id."'";
			$order_number=$db->getOne($sql);
			if($order_number>0)
			{
				$result['error']=2;
				$result['message']=$_LANG['erp_supplier_has_order'];
				die($json->encode($result));
			}
			$sql="select count(*) as goods_number from ".$ecs->table('erp_goods_supplier')." where supplier_id='".$supplier_id."'";
			$goods_number=$db->getOne($sql);
			if($goods_number>0)
			{
				$result['error']=2;
				$result['message']=$_LANG['erp_supplier_has_goods'];
				die($json->encode($result));
			}
			$sql="delete from ".$ecs->table('erp_supplier')." where supplier_id='".$supplier_id."'";
			if($db->query($sql))
			{
				if (!empty($_REQUEST['id'])) // listTable flow
				{
					$url = 'erp_supplier.php?act=query&' . str_replace('act=remove', '', $_SERVER['QUERY_STRING']);
				    header('Location: ' . $url);
				    exit;
				}
				else // old ERP flow
				{
					$result['error']=0;
					die($json->encode($result));
				}
			}
		}
		else
		{
			$result['error']=3;
			$result['message']=$_LANG['erp_supplier_not_exist'];
			die($json->encode($result));
		}
	}
}
elseif($_REQUEST['act'] == 'add')
{
	if((admin_priv('erp_sys_manage','',false)))
	{
		$smarty->assign('act','insert');
		$supplier_code=gen_supplier_code();
		$smarty->assign('supplier_code',$supplier_code);
		$smarty->assign('group_list',supplier_group_list());
		$smarty->assign('admin_list',admin_list());
		$smarty->assign('secondary_admin_list',admin_list());
		$smarty->assign('ur_here',$GLOBALS['_LANG']['erp_add_supplier']);
		assign_query_info();
		$smarty->display('erp_supplier.htm');
	}
	else
	{
		$href="erp_supplier.php?act=list";
		$text=$_LANG['erp_supplier_return'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif($_REQUEST['act'] == 'insert')
{
	if((admin_priv('erp_sys_manage','',false)))
	{
		$supplier_name=isset($_REQUEST['supplier_name']) ?trim($_REQUEST['supplier_name']) : '';
		$supplier_address=isset($_REQUEST['supplier_name']) ?trim($_REQUEST['supplier_address']) : '';
		$supplier_contact=isset($_REQUEST['supplier_contact']) ?trim($_REQUEST['supplier_contact']) : '';
		$supplier_tel=isset($_REQUEST['supplier_tel']) ?trim($_REQUEST['supplier_tel']) : '';
		$supplier_email=isset($_REQUEST['supplier_email']) ?trim($_REQUEST['supplier_email']) : '';
		$supplier_other_email=isset($_REQUEST['supplier_other_email']) ?trim($_REQUEST['supplier_other_email']) : '';
		$supplier_fax=isset($_REQUEST['supplier_fax']) ?trim($_REQUEST['supplier_fax']) : '';
		$supplier_bank_name=isset($_REQUEST['supplier_bank_name']) ?trim($_REQUEST['supplier_bank_name']) : '';
		$supplier_bank_account=isset($_REQUEST['supplier_bank_account']) ?trim($_REQUEST['supplier_bank_account']) : '';
		$supplier_bank_contact=isset($_REQUEST['supplier_bank_contact']) ?trim($_REQUEST['supplier_bank_contact']) : '';
		$supplier_group_id=isset($_REQUEST['supplier_group_id']) &&intval($_REQUEST['supplier_group_id'])>0 ?intval($_REQUEST['supplier_group_id']) : 0;
		$is_valid=isset($_REQUEST['is_valid']) &&intval($_REQUEST['is_valid'])>0 ?intval($_REQUEST['is_valid']) : 0;
		if(empty($supplier_name))
		{
			$href="erp_supplier.php?act=list";
			$text=$_LANG['erp_supplier_return'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_wrong_parameter'],0,$link);
		}
		//對方會計部電郵 不能輸入yohohongkong的email， 消除email之間的空格
		if ($supplier_bank_contact != "") {
			$email_no = substr_count($supplier_bank_contact,"@");
			$bank_contact_arr = explode(",",$supplier_bank_contact);
			$email_arr = [];
			//有中文逗號，
			if ($email_no != count($bank_contact_arr)) {
				foreach ($bank_contact_arr as  $value) {
					$value_arr = explode("，",$value);
					foreach ($value_arr as  $val) {
						$pos = strpos($val, "yohohongkong");
						if ($pos === false) {
							$val = trim($val);
							array_push($email_arr,$val);
						}
					}	
				}
			} else {
			//無中文逗號，only remove whitespace
				foreach ($bank_contact_arr as  $val) {
					$pos = strpos($val, "yohohongkong");
					if ($pos === false) {
						$val = trim($val);
						array_push($email_arr,$val);
					}
				}				
			}
			if (count($email_arr)!=0) {
				$email_str = implode(",",$email_arr);
				$supplier_bank_contact = $email_str;
			}
			//email全部是yoho
			else {
				$supplier_bank_contact = '';
			}
		}
		$row=array(
			'name'=>$supplier_name,
			'address'=>$supplier_address,
			'contact'=>$supplier_contact,
			'tel'=>$supplier_tel,
			'email'=>$supplier_email,
			'other_email'=>$supplier_other_email,
			'fax'=>$supplier_fax,
			'bank_name'=>$supplier_bank_name,
			'bank_account'=>$supplier_bank_account,
			'bank_contact'=>$supplier_bank_contact,
			'supplier_group_id'=>$supplier_group_id,
			'is_valid'=>$is_valid,
			'code'=>gen_supplier_code()
		);
		$GLOBALS['db']->autoExecute($GLOBALS['ecs']->table('erp_supplier'),$row,'INSERT');
		$supplier_id=$GLOBALS['db']->insert_id();
		if(isset($_REQUEST['admins']))
		{
			foreach($_REQUEST['admins'] as $key =>$admin_id)
			{
				$admin_id=intval($admin_id);
				if($admin_id >0)
				{
					$sql="insert into ".$GLOBALS['ecs']->table('erp_supplier_admin')." set supplier_id='".$supplier_id."',admin_id='".$admin_id."'";
					$GLOBALS['db']->query($sql);
				}
			}
		}
		if(isset($_REQUEST['secondary_admins']))
		{
			foreach($_REQUEST['secondary_admins'] as $key =>$admin_id)
			{
				$admin_id=intval($admin_id);
				if($admin_id >0)
				{
					$sql="insert into ".$GLOBALS['ecs']->table('erp_supplier_secondary_admin')." set supplier_id='".$supplier_id."',admin_id='".$admin_id."'";
					$GLOBALS['db']->query($sql);
				}
			}
		}
		$href="erp_supplier.php?act=list";
		$text=$_LANG['erp_supplier_return'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_supplier_add_success'],0,$link);
	}
	else
	{
		$href="erp_supplier.php?act=list";
		$text=$_LANG['erp_supplier_return'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif($_REQUEST['act'] == 'edit')
{
	if((admin_priv('erp_sys_manage','',false)))
	{
		$supplier_id=isset($_REQUEST['id']) &&intval($_REQUEST['id'])>0 ?intval($_REQUEST['id']):0;
		if(empty($supplier_id))
		{
			$href="erp_supplier.php?act=list";
			$text=$_LANG['erp_supplier_return'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_wrong_parameter'],0,$link);
		}
		$supplier_info=supplier_info($supplier_id);
		$smarty->assign('group_list',supplier_group_list());
		$sql="select admin_id from ".$GLOBALS['ecs']->table('erp_supplier_admin')." where supplier_id='".$supplier_id."'";
		$res=$GLOBALS['db']->query($sql);
		while($row=mysql_fetch_assoc($res))
		{
			$admin_ids[]=$row['admin_id'];
		}
		$sql="select admin_id from ".$GLOBALS['ecs']->table('erp_supplier_secondary_admin')." where supplier_id='".$supplier_id."'";
		$res=$GLOBALS['db']->query($sql);
		while($row=mysql_fetch_assoc($res))
		{
			$secondary_admin_ids[]=$row['admin_id'];
		}
		$supplierController =  new Yoho\cms\Controller\SupplierController();
		$smarty->assign('payment_term_list',$supplierController->getPaymentTerms());
		$smarty->assign('corresponding_company_list', $supplierController->getCorrespondingCompanyList());
		$smarty->assign('admin_list',admin_list($admin_ids));
		$smarty->assign('secondary_admin_list', admin_list($secondary_admin_ids));
		$smarty->assign('supplier_info',$supplier_info);
		$smarty->assign('ur_here',$GLOBALS['_LANG']['erp_edit_supplier']);
		$smarty->assign('act','update');
		assign_query_info();
		$smarty->display('erp_supplier.htm');
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif($_REQUEST['act'] == 'update')
{
	if((admin_priv('erp_sys_manage','',false)))
	{
		$supplier_id=isset($_REQUEST['supplier_id']) &&intval($_REQUEST['supplier_id'])>0 ?intval($_REQUEST['supplier_id']) : 0;
		$supplier_name=isset($_REQUEST['supplier_name']) ?trim($_REQUEST['supplier_name']) : '';
		$supplier_address=isset($_REQUEST['supplier_name']) ?trim($_REQUEST['supplier_address']) : '';
		$supplier_contact=isset($_REQUEST['supplier_contact']) ?trim($_REQUEST['supplier_contact']) : '';
		$supplier_tel=isset($_REQUEST['supplier_tel']) ?trim($_REQUEST['supplier_tel']) : '';
		$supplier_email=isset($_REQUEST['supplier_email']) ?trim($_REQUEST['supplier_email']) : '';
		$supplier_other_email=isset($_REQUEST['supplier_other_email']) ?trim($_REQUEST['supplier_other_email']) : '';
		$supplier_fax=isset($_REQUEST['supplier_fax']) ?trim($_REQUEST['supplier_fax']) : '';
		$supplier_bank_name=isset($_REQUEST['supplier_bank_name']) ?trim($_REQUEST['supplier_bank_name']) : '';
		$supplier_bank_account=isset($_REQUEST['supplier_bank_account']) ?trim($_REQUEST['supplier_bank_account']) : '';
		$supplier_bank_contact=isset($_REQUEST['supplier_bank_contact']) ?trim($_REQUEST['supplier_bank_contact']) : '';
		$supplier_group_id=isset($_REQUEST['supplier_group_id']) &&intval($_REQUEST['supplier_group_id'])>0 ?intval($_REQUEST['supplier_group_id']) : 0;
		$supplier_payment_term = isset($_REQUEST['supplier_payment_term']) ? trim($_REQUEST['supplier_payment_term']) : "";
		$supplier_corresponding_company = isset($_REQUEST['supplier_corresponding_company']) ? trim($_REQUEST['supplier_corresponding_company']) : "";
		$is_valid=isset($_REQUEST['is_valid']) &&intval($_REQUEST['is_valid'])>0 ?intval($_REQUEST['is_valid']) : 0;
		if(empty($supplier_id) ||empty($supplier_name))
		{
			$href="erp_supplier.php?act=list";
			$text=$_LANG['erp_supplier_return'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_wrong_parameter'],0,$link);
		}
		//對方會計部電郵 不能輸入yohohongkong的email， 消除email之間的空格
		if ($supplier_bank_contact != "") {
			$email_no = substr_count($supplier_bank_contact,"@");
			$bank_contact_arr = explode(",",$supplier_bank_contact);
			$email_arr = [];
			//有中文逗號，
			if ($email_no != count($bank_contact_arr)) {
				foreach ($bank_contact_arr as  $value) {
					$value_arr = explode("，",$value);
					foreach ($value_arr as  $val) {
						$pos = strpos($val, "yohohongkong");
						if ($pos === false) {
							$val = trim($val);
							array_push($email_arr,$val);
						}
					}	
				}
			} else {
			//無中文逗號，only remove whitespace
				foreach ($bank_contact_arr as  $val) {
					$pos = strpos($val, "yohohongkong");
					if ($pos === false) {
						$val = trim($val);
						array_push($email_arr,$val);
					}
				}				
			}
			if (count($email_arr)!=0) {
				$email_str = implode(",",$email_arr);
				$supplier_bank_contact = $email_str;
			}
			//email全部是yoho
			else {
				$supplier_bank_contact = '';
			}
		}
		$row=array(
			'name'=>$supplier_name,
			'address'=>$supplier_address,
			'contact'=>$supplier_contact,
			'tel'=>$supplier_tel,
			'email'=>$supplier_email,
			'other_email'=>$supplier_other_email,
			'fax'=>$supplier_fax,
			'bank_name'=>$supplier_bank_name,
			'bank_account'=>$supplier_bank_account,
			'bank_contact'=>$supplier_bank_contact,
			'supplier_group_id'=>$supplier_group_id,
			'is_valid'=>$is_valid,
			'payment_term'=> $supplier_payment_term,
			'corresponding_company_id' => $supplier_corresponding_company,
		);
		$GLOBALS['db']->autoExecute($GLOBALS['ecs']->table('erp_supplier'),$row,'UPDATE','supplier_id='.$supplier_id);
		$sql="delete from ".$GLOBALS['ecs']->table('erp_supplier_admin')." where supplier_id='".$supplier_id."'";
		$GLOBALS['db']->query($sql);
		$sql="delete from ".$GLOBALS['ecs']->table('erp_supplier_secondary_admin')." where supplier_id='".$supplier_id."'";
		$GLOBALS['db']->query($sql);
		if(isset($_REQUEST['admins']))
		{
			foreach($_REQUEST['admins'] as $key =>$admin_id)
			{
			$admin_id=intval($admin_id);
				if($admin_id >0)
				{
					$sql="insert into ".$GLOBALS['ecs']->table('erp_supplier_admin')." set supplier_id='".$supplier_id."',admin_id='".$admin_id."'";
					$GLOBALS['db']->query($sql);
				}
			}
		}
		if(isset($_REQUEST['secondary_admins']))
		{
			foreach($_REQUEST['secondary_admins'] as $key =>$admin_id)
			{
				$admin_id=intval($admin_id);
				if($admin_id >0)
				{
					$sql="insert into ".$GLOBALS['ecs']->table('erp_supplier_secondary_admin')." set supplier_id='".$supplier_id."',admin_id='".$admin_id."'";
					$GLOBALS['db']->query($sql);
				}
			}
		}
		$href="erp_supplier.php?act=list";
		$text=$_LANG['erp_supplier_return'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_supplier_edit_success'],0,$link);
	}
	else
	{
		$href="erp_supplier.php?act=list";
		$text=$_LANG['erp_supplier_return'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif ($_REQUEST['act'] == 'update_one_time') {
    $sql = "SELECT supplier_id, bank_contact FROM" . $GLOBALS['ecs']->table('erp_supplier');
    $list = $db->getAll($sql);
    foreach ($list as $key => $row) {
        $supplier_id = $row["supplier_id"];
		$bank_contact = $row["bank_contact"];
		if ($bank_contact != "") {
			$email_no = substr_count($bank_contact,"@");
			$bank_contact_arr = explode(",",$bank_contact);
			$email_arr = [];
			//有中文逗號，
			if ($email_no != count($bank_contact_arr)) {
				foreach ($bank_contact_arr as  $value) {
					$value_arr = explode("，",$value);
					foreach ($value_arr as  $val) {
						$pos = strpos($val, "yohohongkong");
						if ($pos === false) {
							$val = trim($val);
							array_push($email_arr,$val);
						}
					}	
				}
			} else {
			//無中文逗號，only remove whitespace
				foreach ($bank_contact_arr as  $val) {
					$pos = strpos($val, "yohohongkong");
					if ($pos === false) {
						$val = trim($val);
						array_push($email_arr,$val);
					}
				}	
				
			}
			if (count($email_arr)!=0) {
				$email_str = implode(",",$email_arr);
				$sql = "UPDATE " . $GLOBALS['ecs']->table('erp_supplier') . " SET `bank_contact` = '" . $email_str . "' WHERE `supplier_id` = '" . $supplier_id ."'";
				$db->query($sql);
			}
			//email全部是yoho
			else {
				$sql = "UPDATE " . $GLOBALS['ecs']->table('erp_supplier') . " SET `bank_contact` = '' WHERE `supplier_id` = '" . $supplier_id ."'";
				$db->query($sql);
			}
		}
    }
}
function get_supplier_list()
{
    $filter['group_id'] = empty($_REQUEST['group_id']) ? 0 : intval($_REQUEST['group_id']);
    $filter['keyword'] = empty($_REQUEST['keyword']) ? '' : trim($_REQUEST['keyword']);
	$filter['admin_id'] = empty($_REQUEST['admin_id']) ? 0 : intval($_REQUEST['admin_id']);
    $filter['record_count'] = supplier_count($filter);
    
    /* 分页大小 */
    $filter = page_and_size($filter);
	
	$supplier_info = supplier_list($filter, $filter['start'], $filter['page_size']);
	
	$arr = array(
        'supplier_info' => $supplier_info,
        'filter' => $filter,
        'page_count' => $filter['page_count'],
        'record_count' => $filter['record_count']
    );
    return $arr;
}
?>