<?php
    session_start();
    define('IN_ECS', true);
    require(dirname(__FILE__) . '/includes/init.php');
    require_once(ROOT_PATH . 'includes/Google_api_client/vendor/autoload.php');

    
    $erpController = new Yoho\cms\Controller\ErpController();
    $page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 0;
    $byyears = isset($_REQUEST['byyears']) ? intval($_REQUEST['byyears']) : 0;
    $geo = isset($_REQUEST['geo']) ? intval($_REQUEST['geo']) : 0;
    $nbyyears = intval(!$byyears);
    $ngeo = intval(!$geo);
    $today = local_date("Y-m-d");
    $lastday = local_date("Y-m-d", strtotime($today . " -3 year"));
    $lastday2 = local_date("Y-m-d", strtotime($today . " -2 year"));
    $lastday3 = local_date("Y-m-d", strtotime($today . " -1 year"));
    $segment = 8;
    
    if(isset($_SESSION['ga_access_token']) && isset($_SESSION['expiry']) && intval($_SESSION['expiry']) > gmtime()){
        $access_token = $_SESSION['ga_access_token'];
    } else {
        $KEY_FILE_LOCATION =  ROOT_PATH . 'data/yoho_service_account.json';
        $client = new Google_Client();
        $client->setApplicationName("Yoho GA Report");
        $client->setAuthConfig($KEY_FILE_LOCATION);
        $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
        $client->useApplicationDefaultCredentials();
        $client->fetchAccessTokenWithAssertion();
        $token = $client->getAccessToken();
        $access_token = $token['access_token'];
        $expiry = gmtime() + $token['expires_in'];
        $_SESSION['ga_access_token'] = $access_token;
        $_SESSION['expiry'] = $expiry;
    }
    
    if($_REQUEST['act'] == 'list'){
        if(!isset($_REQUEST['keys'])){
            // brand info
            $sql = "SELECT brand_name, IFNULL(SUM(gn.goods_number),0) AS total_stock FROM " . $ecs->table('brand') . " b " .
                "LEFT JOIN " . $ecs->table('goods') . " g ON g.brand_id = b.brand_id " .
                "LEFT JOIN (SELECT goods_id, SUM(goods_qty) AS goods_number FROM " . $ecs->table('erp_goods_attr_stock') .
                    " WHERE warehouse_id IN (". $erpController->getSellableWarehouseIds(true). ") GROUP BY goods_id) gn ON gn.goods_id = g.goods_id " .
                "WHERE g.is_hidden = 0 AND b.is_show = 1 GROUP BY b.brand_id HAVING total_stock > 200 ORDER BY total_stock DESC";
            $res = $db->getAll($sql);
            $bpage = ceil(count($res) / $segment);
            for($i = $page * $segment; $i < ($page + 1) * $segment && $i < count($res); $i++){
                $res_en[] = array('name'=>str_replace('/',' ',$res[$i]['brand_name']),'stock'=>$res[$i]['total_stock'],'urlencode'=>urlencode(str_replace('/',' ',$res[$i]['brand_name'])));            
            }
            // cat info
            $sql = "SELECT c.cat_id, cat_name, IFNULL(SUM(gn.goods_number),0) AS total_stock FROM " . $ecs->table('category') . " c " .
                "LEFT JOIN " . $ecs->table('goods') . " g ON g.cat_id = c.cat_id " .
                "LEFT JOIN (SELECT goods_id, SUM(goods_qty) AS goods_number FROM " . $ecs->table('erp_goods_attr_stock') .
                    " WHERE warehouse_id IN (". $erpController->getSellableWarehouseIds(true). ") GROUP BY goods_id) gn ON gn.goods_id = g.goods_id " .
                "WHERE c.cat_name NOT LIKE '%其他%' AND c.cat_name NOT LIKE '%其它%' AND g.is_hidden = 0 AND c.is_show = 1 GROUP BY c.cat_id HAVING total_stock > 200 ORDER BY total_stock DESC";
            $res = $db->getAll($sql);
            $cpage = ceil(count($res) / $segment);
            for($i = $page * $segment; $i < ($page + 1) * $segment && $i < count($res); $i++){
                $res_en2[] = array('id'=>$res[$i]['cat_id'],'name'=>str_replace('/',' ',$res[$i]['cat_name']),'stock'=>$res[$i]['total_stock'],'urlencode'=>urlencode(str_replace('/',' ',$res[$i]['cat_name'])));
            }
            $mpage = max($bpage,$cpage);
            $smarty->assign('res',          $res_en);
            $smarty->assign('res2',         $res_en2);
            $smarty->assign('full_page',    1);
            assign_query_info();
        }else{
            $mpage = 3;
            $all_keys = $_REQUEST['keys'];
            foreach($all_keys as $key){
                $url_keys .= "&keys[]=" . $key;
            }
            for($i = $page * $segment; $i < ($page + 1) * $segment && $i < count($all_keys); $i++){
                $keys[] = $all_keys[$i];
            }
            $smarty->assign('keys',         $keys);
            $smarty->assign('url_keys',     $url_keys);
        }
        
        $smarty->assign('geo',          $geo);
        $smarty->assign('ngeo',         $ngeo);
        $smarty->assign('segment',      $segment / 2);
        $smarty->assign('csssegment',   100 / ($segment / 2));
        $smarty->assign('byyears',      $byyears);
        $smarty->assign('nbyyears',     $nbyyears);
        $smarty->assign('page',         $page);
        $smarty->assign('prev_page',    $page > 0 ? $page - 1 : -1);
        $smarty->assign('next_page',    $page + 1 < $mpage ? $page + 1 : -1);
        $smarty->assign('today',        $today);
        $smarty->assign('lastday',      $lastday);
        $smarty->assign('lastday2',     $lastday2);
        $smarty->assign('lastday3',     $lastday3);
        $smarty->assign('access_token', $access_token);
        $smarty->assign('ur_here',      $_LANG['trends_keywords']);
        $smarty->display('trends.htm');
    }
?>