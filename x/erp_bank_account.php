<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require_once(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require_once(dirname(__FILE__) .'/includes/ERP/lib_erp_finance.php');
if ($_REQUEST['act'] == 'change_is_valid')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_finance_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$account_id=isset($_REQUEST['account_id']) &&intval($_REQUEST['account_id'])>0?intval($_REQUEST['account_id']):0;
	if(empty($account_id))
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$bank_account_info=get_bank_account_info($account_id);
	if(empty($bank_account_info))
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_bank_account_not_exists'];
		die($json->encode($result));
	}
	else
	{
		$is_valid=$bank_account_info['is_valid'];
		if($is_valid==1)
		{
			$sql="update ".$ecs->table('erp_bank_account')." set is_valid='0' where account_id='".$account_id."'";
			$db->query($sql);
			$result['error']=0;
			$result['is_valid']=0;
			die($json->encode($result));
		}
		elseif($is_valid==0)
		{
			$sql="update ".$ecs->table('erp_bank_account')." set is_valid='1' where account_id='".$account_id."'";
			$db->query($sql);
			$result['error']=0;
			$result['is_valid']=1;
			die($json->encode($result));
		}
	}
}
elseif($_REQUEST['act'] == 'add_bank_account'||$_REQUEST['act'] == 'update_bank_account')
{
	if((admin_priv('erp_finance_manage','',false)))	
	{
		$account_name=isset($_POST['account_name'])?trim($_POST['account_name']):'';
		$account_no=isset($_POST['account_no'])?trim($_POST['account_no']):'';
		$is_valid=isset($_POST['is_valid']) &&intval($_POST['is_valid'])>0 ?intval($_POST['is_valid']):0;
		$agency_id=isset($_POST['agency_id']) &&intval($_POST['agency_id'])>0 ?intval($_POST['agency_id']):0;
		$account_id=isset($_POST['account_id']) &&intval($_POST['account_id'])>0 ?intval($_POST['account_id']):0;
		if($_REQUEST['act'] == 'update_bank_account'&&empty($account_id))
		{
			$href="erp_finance_manage.php?act=bank_account_setting";
			$text=$_LANG['erp_bank_account_return'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_wrong_parameter'],0,$link);
		}
		if(empty($account_name) ||empty($account_no))
		{
			$href="erp_finance_manage.php?act=bank_account_setting";
			$text=$_LANG['erp_bank_account_return'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_wrong_parameter'],0,$link);
		}
		if($_REQUEST['act'] == 'add_bank_account')
		{
			$sql="insert into ".$ecs->table('erp_bank_account')." set account_name ='".$account_name."',account_no='".$account_no."',is_valid='".		$is_valid."',agency_id='".$agency_id."'";
			$db->query($sql);
		}
		else
		{
			$sql="update ".$ecs->table('erp_bank_account')." set account_name ='".$account_name."',account_no='".$account_no."',is_valid='".$is_valid."',agency_id='".$agency_id."' where account_id='".$account_id."'";
			$db->query($sql);
		}
		$href="erp_finance_manage.php?act=bank_account_setting";
		$text=$_LANG['erp_bank_account_return'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_bank_account_add_success'],0,$link);
	}
	else
	{
		$href="erp_finance_manage.php?act=bank_account_setting";
		$text=$_LANG['erp_bank_account_return'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif($_REQUEST['act'] == 'delete_bank_account')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_finance_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$account_id=isset($_REQUEST['account_id']) &&intval($_REQUEST['account_id'])>0?intval($_REQUEST['account_id']):0;
	if(empty($account_id))
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$bank_account_info=get_bank_account_info($account_id);
	if(empty($bank_account_info))
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_bank_account_not_exist'];
		die($json->encode($result));
	}
	else
	{
		$sql="select * from ".$GLOBALS['ecs']->table('erp_gathering')." where account_id='".$account_id."'";
		if($db->getRow($sql))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_bank_account_has_gathering'];
			die($json->encode($result));
		}
		$sql="select * from ".$GLOBALS['ecs']->table('erp_payment')." where account_id='".$account_id."' limit 1";
		if($db->getRow($sql))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_bank_account_has_payment'];
			die($json->encode($result));
		}
		$sql="select * from ".$GLOBALS['ecs']->table('erp_bank_account_record')." where account_id='".$account_id."' limit 1";
		if($db->getRow($sql))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_bank_account_has_bank_record'];
			die($json->encode($result));
		}
		$sql="delete from ".$ecs->table('erp_bank_account')." where account_id='".$account_id."'";
		$db->query($sql);
		$result['error']=0;
		die($json->encode($result));
	}
}
?>