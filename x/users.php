<?php

/**
 * ECSHOP 会员管理程序
 * ============================================================================
 * 版权所有 2005-2010 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liuhui $
 * $Id: users.php 17063 2010-03-25 06:35:46Z liuhui $
*/

define('IN_ECS', true);
set_time_limit(0);
ini_set("memory_limit","256M");

require(dirname(__FILE__) . '/includes/init.php');
$userController = new Yoho\cms\Controller\UserController();
/*------------------------------------------------------ */
//-- 用户帐号列表
/*------------------------------------------------------ */
$smarty->assign('wholesale_user_manage', check_authz('wholesale_user_manage'));
if ($_REQUEST['act'] == 'list')
{
    /* 检查权限 */
    admin_priv('users_manage');
    
    $all_ranks = get_all_user_ranks();
    $ranks = array();
    foreach ($all_ranks as $row)
    {
        $ranks[$row['rank_id']] = $row['rank_name'];
    }

    $smarty->assign('user_ranks',   $ranks);
    $smarty->assign('ur_here',      $_LANG['03_users_list']);
    $smarty->assign('action_link',  array('text' => $_LANG['04_users_add'], 'href'=>'users.php?act=add'));
    $smarty->assign('action_link2',  array('text' => '下載 XLSX', 'href'=>'downloadxlsx'));

    $smarty->assign('keywords',     empty($_REQUEST['keywords']) ? '' : trim($_REQUEST['keywords']));
    $smarty->assign('cst',     empty($_REQUEST['cst']) ? 0 : intval($_REQUEST['cst']));
    $smarty->assign('csta',     empty($_REQUEST['csta']) ? 0 : intval($_REQUEST['csta']));
    $smarty->assign('range',     empty($_REQUEST['range']) ? 0 : intval($_REQUEST['range']));
    $smarty->assign('guest_stats_type',     empty($_REQUEST['guest_stats_type']) ? 0 : intval($_REQUEST['guest_stats_type']));
    $smarty->assign('bought_goods', empty($_REQUEST['bought_goods']) ? '' : trim($_REQUEST['bought_goods']));
    $smarty->assign('have_email',   empty($_REQUEST['have_email']) ? 0 : intval($_REQUEST['have_email']));
    $smarty->assign('language_list', available_language_names());
    
    assign_query_info();
    $smarty->display('users_list.htm');
}

/*------------------------------------------------------ */
//-- ajax返回用户列表
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'query')
{
    $user_list = user_list();
    $userController->ajaxQueryAction($user_list['user_list'],$user_list['record_count'], false);
}

/*------------------------------------------------------ */
//-- 添加会员帐号
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'add')
{
    /* 检查权限 */
    admin_priv('users_manage');

    $user = array(  'rank_points'   => $_CFG['register_points'],
                    'pay_points'    => $_CFG['register_points'],
                    'sex'           => 0,
                    'credit_line'   => 0
                    );
    /* 取出注册扩展字段 */
    $sql = 'SELECT * FROM ' . $ecs->table('reg_fields') . ' WHERE type < 2 AND display = 1 AND id != 6 ORDER BY dis_order, id';
    $extend_info_list = $db->getAll($sql);
    $smarty->assign('extend_info_list', $extend_info_list);

    $smarty->assign('is_wholesale',     $_REQUEST['is_wholesale']);
    $smarty->assign('ur_here',          $_LANG['04_users_add']);
    $smarty->assign('action_link',      array('text' => $_LANG['03_users_list'], 'href'=>'users.php?act=list'));
    $smarty->assign('form_action',      'insert');
    $smarty->assign('user',             $user);
    $smarty->assign('special_ranks',    get_rank_list(true));

    assign_query_info();
    $smarty->display('user_info.htm');
}

/*------------------------------------------------------ */
//-- 添加会员帐号
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'insert')
{
    /* 检查权限 */
    admin_priv('users_manage');
    $username = empty($_POST['username']) ? '' : trim($_POST['username']);
    $password = empty($_POST['password']) ? '' : trim($_POST['password']);
    $email = empty($_POST['email']) ? '' : trim($_POST['email']);
    $sex = empty($_POST['sex']) ? 0 : intval($_POST['sex']);
    $sex = in_array($sex, array(0, 1, 2)) ? $sex : 0;
    $birthday = $_POST['birthday'];
    $rank = empty($_POST['user_rank']) ? 0 : intval($_POST['user_rank']);
    $credit_line = empty($_POST['credit_line']) ? 0 : floatval($_POST['credit_line']);

    $users =& init_users();

    if (!$users->add_user($username, $password, $email))
    {
        /* 插入会员数据失败 */
        if ($users->error == ERR_INVALID_USERNAME)
        {
            $msg = $_LANG['username_invalid'];
        }
        elseif ($users->error == ERR_USERNAME_NOT_ALLOW)
        {
            $msg = $_LANG['username_not_allow'];
        }
        elseif ($users->error == ERR_USERNAME_EXISTS)
        {
            $msg = $_LANG['username_exists'];
        }
        elseif ($users->error == ERR_INVALID_EMAIL)
        {
            $msg = $_LANG['email_invalid'];
        }
        elseif ($users->error == ERR_EMAIL_NOT_ALLOW)
        {
            $msg = $_LANG['email_not_allow'];
        }
        elseif ($users->error == ERR_EMAIL_EXISTS)
        {
            $msg = $_LANG['email_exists'];
        }
        else
        {
            //die('Error:'.$users->error_msg());
        }
        sys_msg($msg, 1);
    }

    /* 注册送积分 */
    if (!empty($GLOBALS['_CFG']['register_points']))
    {
        log_account_change($_SESSION['user_id'], 0, 0, $GLOBALS['_CFG']['register_points'], $GLOBALS['_CFG']['register_points'], $_LANG['register_points']);
    }

    /*把新注册用户的扩展信息插入数据库*/
    $sql = 'SELECT id FROM ' . $ecs->table('reg_fields') . ' WHERE type = 0 AND display = 1 ORDER BY dis_order, id';   //读出所有扩展字段的id
    $fields_arr = $db->getAll($sql);

    $extend_field_str = '';    //生成扩展字段的内容字符串
    $user_id_arr = $users->get_profile_by_name($username);
    foreach ($fields_arr AS $val)
    {
        $extend_field_index = 'extend_field' . $val['id'];
        if(!empty($_POST[$extend_field_index]))
        {
            $temp_field_content = strlen($_POST[$extend_field_index]) > 100 ? mb_substr($_POST[$extend_field_index], 0, 99) : $_POST[$extend_field_index];
            $extend_field_str .= " ('" . $user_id_arr['user_id'] . "', '" . $val['id'] . "', '" . $temp_field_content . "'),";
        }
    }
    $extend_field_str = substr($extend_field_str, 0, -1);

    if ($extend_field_str)      //插入注册扩展数据
    {
        $sql = 'INSERT INTO '. $ecs->table('reg_extend_info') . ' (`user_id`, `reg_field_id`, `content`) VALUES' . $extend_field_str;
        $db->query($sql);
    }

    /* 更新会员的其它信息 */
    $other =  array();
    $other['credit_line'] = $credit_line;
    $other['user_rank']  = $rank;
    $other['sex']        = $sex;
    $other['birthday']   = $birthday;
    $other['reg_time'] = local_strtotime(local_date('Y-m-d H:i:s'));
    $other['is_wholesale'] = (isset($_POST['is_wholesale'])&&$_POST['is_wholesale']=='1') ? true:false;

    $other['msn'] = isset($_POST['extend_field1']) ? htmlspecialchars(trim($_POST['extend_field1'])) : '';
    $other['qq'] = isset($_POST['extend_field2']) ? htmlspecialchars(trim($_POST['extend_field2'])) : '';
    $other['office_phone'] = isset($_POST['extend_field3']) ? htmlspecialchars(trim($_POST['extend_field3'])) : '';
    $other['home_phone'] = isset($_POST['extend_field4']) ? htmlspecialchars(trim($_POST['extend_field4'])) : '';
    $other['mobile_phone'] = isset($_POST['extend_field5']) ? htmlspecialchars(trim($_POST['extend_field5'])) : '';

    $db->autoExecute($ecs->table('users'), $other, 'UPDATE', "user_name = '$username'");
    $userController->flushWholesaleUserIds();
    /* 记录管理员操作 */
    admin_log($_POST['username'], 'add', 'users');

    /* 提示信息 */
    if (!empty($_REQUEST['return_ws'])) {
        $link[] = array('text' => $_LANG["wholesale_user_info"], 'href'=>'wholesale_invoice.php?act=list');
    }
    $link[] = array('text' => $_LANG['go_back'], 'href'=>'users.php?act=list');
    sys_msg(sprintf($_LANG['add_success'], htmlspecialchars(stripslashes($_POST['username']))), 0, $link);

}

/*------------------------------------------------------ */
//-- 编辑用户帐号
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'edit')
{
    /* 检查权限 */
    admin_priv('users_manage');

    $sql = "SELECT u.user_name, u.sex, u.birthday, u.pay_points, u.rank_points, u.user_rank , u.user_money, u.frozen_money, u.credit_line, u.parent_id, u2.user_name as parent_username, u.qq, u.msn, u.office_phone, u.home_phone, u.mobile_phone, u.is_wholesale ".
        " FROM " .$ecs->table('users'). " u LEFT JOIN " . $ecs->table('users') . " u2 ON u.parent_id = u2.user_id WHERE u.user_id='$_GET[id]'";

    $row = $db->GetRow($sql);
    $row['user_name'] = addslashes($row['user_name']);
    $users  =& init_users();
    $user   = $users->get_user_info($row['user_name']);

    $sql = "SELECT u.user_id, u.sex, u.birthday, u.pay_points, u.rank_points, u.user_rank , u.user_money, u.frozen_money, u.credit_line, u.parent_id, u2.user_name as parent_username, u.qq, u.msn,
    u.office_phone, u.home_phone, u.mobile_phone, u.is_wholesale, u.is_fraud ".
        " FROM " .$ecs->table('users'). " u LEFT JOIN " . $ecs->table('users') . " u2 ON u.parent_id = u2.user_id WHERE u.user_id='$_GET[id]'";

    $row = $db->GetRow($sql);

    if ($row)
    {
        $user['user_id']        = $row['user_id'];
        $user['sex']            = $row['sex'];
        $user['birthday']       = date($row['birthday']);
        $user['pay_points']     = $row['pay_points'];
        $user['rank_points']    = $row['rank_points'];
        $user['user_rank']      = $row['user_rank'];
        $user['user_money']     = $row['user_money'];
        $user['frozen_money']   = $row['frozen_money'];
        $user['credit_line']    = $row['credit_line'];
        $user['formated_user_money'] = price_format($row['user_money']);
        $user['formated_frozen_money'] = price_format($row['frozen_money']);
        $user['parent_id']      = $row['parent_id'];
        $user['parent_username']= $row['parent_username'];
        $user['qq']             = $row['qq'];
        $user['msn']            = $row['msn'];
        $user['office_phone']   = $row['office_phone'];
        $user['home_phone']     = $row['home_phone'];
        $user['mobile_phone']   = $row['mobile_phone'];
        $user['is_wholesale']   = $row['is_wholesale'];
        $user['is_fraud']       = $row['is_fraud'];
        if($user['birthday'] == '0000-00-00') {
            $date = local_strtotime("-30 year");
            $user['birthday'] = date("Y-m-d", $date);
        }
    }
    else
    {
        $user['sex']            = 0;
        $user['pay_points']     = 0;
        $user['rank_points']    = 0;
        $user['user_money']     = 0;
        $user['frozen_money']   = 0;
        $user['credit_line']    = 0;
        $user['formated_user_money'] = price_format(0);
        $user['formated_frozen_money'] = price_format(0);
     }

    /* 取出注册扩展字段 */
    $sql = 'SELECT * FROM ' . $ecs->table('reg_fields') . ' WHERE type < 2 AND display = 1 AND id != 6 ORDER BY dis_order, id';
    $extend_info_list = $db->getAll($sql);

    $sql = 'SELECT reg_field_id, content ' .
           'FROM ' . $ecs->table('reg_extend_info') .
           " WHERE user_id = $user[user_id]";
    $extend_info_arr = $db->getAll($sql);

    $temp_arr = array();
    foreach ($extend_info_arr AS $val)
    {
        $temp_arr[$val['reg_field_id']] = $val['content'];
    }

    foreach ($extend_info_list AS $key => $val)
    {
        switch ($val['id'])
        {
            case 1:     $extend_info_list[$key]['content'] = $user['msn']; break;
            case 2:     $extend_info_list[$key]['content'] = $user['qq']; break;
            case 3:     $extend_info_list[$key]['content'] = $user['office_phone']; break;
            case 4:     $extend_info_list[$key]['content'] = $user['home_phone']; break;
            case 5:     $extend_info_list[$key]['content'] = $user['mobile_phone']; break;
            default:    $extend_info_list[$key]['content'] = empty($temp_arr[$val['id']]) ? '' : $temp_arr[$val['id']] ;
        }
    }

    $smarty->assign('extend_info_list', $extend_info_list);

    /* 当前会员推荐信息 */
    // $affiliate = unserialize($GLOBALS['_CFG']['affiliate']);
    // $smarty->assign('affiliate', $affiliate);

    empty($affiliate) && $affiliate = array();

    if(empty($affiliate['config']['separate_by']))
    {
        //推荐注册分成
        $affdb = array();
        $num = count($affiliate['item']);
        $up_uid = "'$_GET[id]'";
        for ($i = 1 ; $i <=$num ;$i++)
        {
            $count = 0;
            if ($up_uid)
            {
                $sql = "SELECT user_id FROM " . $ecs->table('users') . " WHERE parent_id IN($up_uid)";
                $query = $db->query($sql);
                $up_uid = '';
                while ($rt = $db->fetch_array($query))
                {
                    $up_uid .= $up_uid ? ",'$rt[user_id]'" : "'$rt[user_id]'";
                    $count++;
                }
            }
            $affdb[$i]['num'] = $count;
        }
        if ($affdb[1]['num'] > 0)
        {
            $smarty->assign('affdb', $affdb);
        }
    }

    $userController->flushWholesaleUserIds();
    assign_query_info();
    $smarty->assign('ur_here',          $_LANG['users_edit']);
    $smarty->assign('action_link',      array('text' => $_LANG['03_users_list'], 'href'=>'users.php?act=list&' . list_link_postfix()));
    $smarty->assign('user',             $user);
    $smarty->assign('form_action',      'update');
    $smarty->assign('special_ranks',    get_rank_list(true));
    $smarty->display('user_info.htm');
}

/*------------------------------------------------------ */
//-- 更新用户帐号
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'update')
{
    /* 检查权限 */
    admin_priv('users_manage');
    $username = empty($_POST['username']) ? '' : trim($_POST['username']);
    $password = empty($_POST['password']) ? '' : trim($_POST['password']);
    $email = empty($_POST['email']) ? '' : trim($_POST['email']);
    $sex = empty($_POST['sex']) ? 0 : intval($_POST['sex']);
    $sex = in_array($sex, array(0, 1, 2)) ? $sex : 0;
    $birthday = $_POST['birthday'];
    $rank = empty($_POST['user_rank']) ? 0 : intval($_POST['user_rank']);
    $credit_line = empty($_POST['credit_line']) ? 0 : floatval($_POST['credit_line']);

    $users  =& init_users();

    if (!$users->edit_user(array('username'=>$username, 'password'=>$password, 'email'=>$email, 'gender'=>$sex, 'bday'=>$birthday ), 1))
    {
        if ($users->error == ERR_EMAIL_EXISTS)
        {
            $msg = $_LANG['email_exists'];
        }
        else
        {
            $msg = $_LANG['edit_user_failed'];
        }
        sys_msg($msg, 1);
    }

    /* 更新用户扩展字段的数据 */
    $sql = 'SELECT id FROM ' . $ecs->table('reg_fields') . ' WHERE type = 0 AND display = 1 ORDER BY dis_order, id';   //读出所有扩展字段的id
    $fields_arr = $db->getAll($sql);
    $user_id_arr = $users->get_profile_by_name($username);
    $user_id = $user_id_arr['user_id'];

    foreach ($fields_arr AS $val)       //循环更新扩展用户信息
    {
        $extend_field_index = 'extend_field' . $val['id'];
        if(isset($_POST[$extend_field_index]))
        {
            $temp_field_content = strlen($_POST[$extend_field_index]) > 100 ? mb_substr($_POST[$extend_field_index], 0, 99) : $_POST[$extend_field_index];

            $sql = 'SELECT * FROM ' . $ecs->table('reg_extend_info') . "  WHERE reg_field_id = '$val[id]' AND user_id = '$user_id'";
            if ($db->getOne($sql))      //如果之前没有记录，则插入
            {
                $sql = 'UPDATE ' . $ecs->table('reg_extend_info') . " SET content = '$temp_field_content' WHERE reg_field_id = '$val[id]' AND user_id = '$user_id'";
            }
            else
            {
                $sql = 'INSERT INTO '. $ecs->table('reg_extend_info') . " (`user_id`, `reg_field_id`, `content`) VALUES ('$user_id', '$val[id]', '$temp_field_content')";
            }
            $db->query($sql);
        }
    }


    /* 更新会员的其它信息 */
    $other =  array();
    $other['credit_line'] = $credit_line;
    $other['user_rank'] = $rank;

    $other['msn'] = isset($_POST['extend_field1']) ? htmlspecialchars(trim($_POST['extend_field1'])) : '';
    $other['qq'] = isset($_POST['extend_field2']) ? htmlspecialchars(trim($_POST['extend_field2'])) : '';
    $other['office_phone'] = isset($_POST['extend_field3']) ? htmlspecialchars(trim($_POST['extend_field3'])) : '';
    $other['home_phone'] = isset($_POST['extend_field4']) ? htmlspecialchars(trim($_POST['extend_field4'])) : '';
    $other['mobile_phone'] = isset($_POST['extend_field5']) ? htmlspecialchars(trim($_POST['extend_field5'])) : '';
    $other['is_wholesale'] = (isset($_POST['is_wholesale'])&&$_POST['is_wholesale']=='1') ? true:false;
    $other['is_fraud'] = isset($_POST['is_fraud']) ? intval($_POST['is_fraud']) : 0;
    
    $db->autoExecute($ecs->table('users'), $other, 'UPDATE', "user_name = '$username'");
    $userController->flushWholesaleUserIds();
    /* 记录管理员操作 */
    admin_log($username, 'edit', 'users');

    /* 提示信息 */
    $links[0]['text']    = $_LANG['goto_list'];
    $links[0]['href']    = 'users.php?act=list&' . list_link_postfix();
    $links[1]['text']    = $_LANG['go_back'];
    $links[1]['href']    = 'javascript:history.back()';

    sys_msg($_LANG['update_success'], 0, $links);

}

/*------------------------------------------------------ */
//-- 批量删除会员帐号
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'batch_remove')
{
    /* 检查权限 */
    admin_priv('users_drop');

    if (isset($_POST['checkboxes']))
    {
        $sql = "SELECT user_name FROM " . $ecs->table('users') . " WHERE user_id " . db_create_in($_POST['checkboxes']);
        $col = $db->getCol($sql);
        $usernames = implode(',',addslashes_deep($col));
        $count = count($col);
        /* 通过插件来删除用户 */
        $users =& init_users();
        $users->remove_user($col);

        admin_log($usernames, 'batch_remove', 'users');

        $lnk[] = array('text' => $_LANG['go_back'], 'href'=>'users.php?act=list');
        sys_msg(sprintf($_LANG['batch_remove_success'], $count), 0, $lnk);
    }
    else
    {
        $lnk[] = array('text' => $_LANG['go_back'], 'href'=>'users.php?act=list');
        sys_msg($_LANG['no_select_user'], 0, $lnk);
    }
}

/* 编辑用户名 */
elseif ($_REQUEST['act'] == 'edit_username')
{
    /* 检查权限 */
    check_authz_json('users_manage');

    $username = empty($_REQUEST['val']) ? '' : json_str_iconv(trim($_REQUEST['val']));
    $id = empty($_REQUEST['id']) ? 0 : intval($_REQUEST['id']);

    if ($id == 0)
    {
        make_json_error('NO USER ID');
        return;
    }

    if ($username == '')
    {
        make_json_error($GLOBALS['_LANG']['username_empty']);
        return;
    }

    $users =& init_users();

    if ($users->edit_user($id, $username))
    {
        if ($_CFG['integrate_code'] != 'ecshop')
        {
            /* 更新商城会员表 */
            $db->query('UPDATE ' .$ecs->table('users'). " SET user_name = '$username' WHERE user_id = '$id'");
        }

        admin_log(addslashes($username), 'edit', 'users');
        make_json_result(stripcslashes($username));
    }
    else
    {
        $msg = ($users->error == ERR_USERNAME_EXISTS) ? $GLOBALS['_LANG']['username_exists'] : $GLOBALS['_LANG']['edit_user_failed'];
        make_json_error($msg);
    }
}

/*------------------------------------------------------ */
//-- 编辑email
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'edit_email')
{
    /* 检查权限 */
    check_authz_json('users_manage');

    $id = empty($_REQUEST['id']) ? 0 : intval($_REQUEST['id']);
    $email = empty($_REQUEST['val']) ? '' : json_str_iconv(trim($_REQUEST['val']));

    $users =& init_users();

    $sql = "SELECT user_name FROM " . $ecs->table('users') . " WHERE user_id = '$id'";
    $username = $db->getOne($sql);


    if (is_email($email))
    {
        if ($users->edit_user(array('username'=>$username, 'email'=>$email)))
        {
            admin_log(addslashes($username), 'edit', 'users');

            make_json_result(stripcslashes($email));
        }
        else
        {
            $msg = ($users->error == ERR_EMAIL_EXISTS) ? $GLOBALS['_LANG']['email_exists'] : $GLOBALS['_LANG']['edit_user_failed'];
            make_json_error($msg);
        }
    }
    else
    {
        make_json_error($GLOBALS['_LANG']['invalid_email']);
    }
}

/*------------------------------------------------------ */
//-- 删除会员帐号
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'remove')
{
    /* 检查权限 */
    admin_priv('users_drop');

    $sql = "SELECT user_name FROM " . $ecs->table('users') . " WHERE user_id = '" . $_GET['id'] . "'";
    $username = $db->getOne($sql);
    /* 通过插件来删除用户 */
    $users =& init_users();
    $users->remove_user($username); //已经删除用户所有数据
    $userController->flushWholesaleUserIds();
    /* 记录管理员操作 */
    admin_log(addslashes($username), 'remove', 'users');

    /* 提示信息 */
    $link[] = array('text' => $_LANG['go_back'], 'href'=>'users.php?act=list');
    sys_msg(sprintf($_LANG['remove_success'], $username), 0, $link);
}

/*------------------------------------------------------ */
//--  收货地址查看
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'address_list')
{
    $id = isset($_GET['id']) ? intval($_GET['id']) : 0;
    $sql = "SELECT a.*, c.region_name AS country_name, p.region_name AS province, ct.region_name AS city_name, d.region_name AS district_name ".
           " FROM " .$ecs->table('user_address'). " as a ".
           " LEFT JOIN " . $ecs->table('region') . " AS c ON c.region_id = a.country " .
           " LEFT JOIN " . $ecs->table('region') . " AS p ON p.region_id = a.province " .
           " LEFT JOIN " . $ecs->table('region') . " AS ct ON ct.region_id = a.city " .
           " LEFT JOIN " . $ecs->table('region') . " AS d ON d.region_id = a.district " .
           " WHERE user_id='$id'";
    $address = $db->getAll($sql);
    $smarty->assign('address',          $address);
    assign_query_info();
    $smarty->assign('ur_here',          $_LANG['address_list']);
    $smarty->assign('action_link',      array('text' => $_LANG['03_users_list'], 'href'=>'users.php?act=list&' . list_link_postfix()));
    $smarty->display('user_address_list.htm');
}

/*------------------------------------------------------ */
//-- 脱离推荐关系
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'remove_parent')
{
    /* 检查权限 */
    admin_priv('users_manage');

    $sql = "UPDATE " . $ecs->table('users') . " SET parent_id = 0 WHERE user_id = '" . $_GET['id'] . "'";
    $db->query($sql);

    /* 记录管理员操作 */
    $sql = "SELECT user_name FROM " . $ecs->table('users') . " WHERE user_id = '" . $_GET['id'] . "'";
    $username = $db->getOne($sql);
    admin_log(addslashes($username), 'edit', 'users');

    /* 提示信息 */
    $link[] = array('text' => $_LANG['go_back'], 'href'=>'users.php?act=list');
    sys_msg(sprintf($_LANG['update_success'], $username), 0, $link);
}

/*------------------------------------------------------ */
//-- 查看用户推荐会员列表
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'aff_list')
{
    /* 检查权限 */
    admin_priv('users_manage');
    $smarty->assign('ur_here',      $_LANG['03_users_list']);

    $auid = $_GET['auid'];
    $user_list['user_list'] = array();

    // $affiliate = unserialize($GLOBALS['_CFG']['affiliate']);
    // $smarty->assign('affiliate', $affiliate);

    empty($affiliate) && $affiliate = array();

    $num = count($affiliate['item']);
    $up_uid = "'$auid'";
    $all_count = 0;
    for ($i = 1; $i<=$num; $i++)
    {
        $count = 0;
        if ($up_uid)
        {
            $sql = "SELECT user_id FROM " . $ecs->table('users') . " WHERE parent_id IN($up_uid)";
            $query = $db->query($sql);
            $up_uid = '';
            while ($rt = $db->fetch_array($query))
            {
                $up_uid .= $up_uid ? ",'$rt[user_id]'" : "'$rt[user_id]'";
                $count++;
            }
        }
        $all_count += $count;

        if ($count)
        {
            $sql = "SELECT user_id, user_name, '$i' AS level, email, is_validated, user_money, frozen_money, rank_points, pay_points, reg_time ".
                    " FROM " . $GLOBALS['ecs']->table('users') . " WHERE user_id IN($up_uid)" .
                    " ORDER by level, user_id";
            $user_list['user_list'] = array_merge($user_list['user_list'], $db->getAll($sql));
        }
    }

    $temp_count = count($user_list['user_list']);
    for ($i=0; $i<$temp_count; $i++)
    {
        $user_list['user_list'][$i]['reg_time'] = local_date($_CFG['date_format'], $user_list['user_list'][$i]['reg_time']);
    }

    $user_list['record_count'] = $all_count;

    $smarty->assign('user_list',    $user_list['user_list']);
    $smarty->assign('record_count', $user_list['record_count']);
    $smarty->assign('full_page',    1);
    $smarty->assign('action_link',  array('text' => $_LANG['back_note'], 'href'=>"users.php?act=edit&id=$auid"));

    assign_query_info();
    $smarty->display('affiliate_list.htm');
}

/*------------------------------------------------------ */
//-- 導出會員資料
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'downloadcsv')
{
    /* 检查权限 */
    admin_priv('users_manage');
    if (!$_SESSION['manage_cost'])
        sys_msg('只有Super Admin權限才能使用此功能');
    
    ini_set('memory_limit', '2048M');

    $user_list = user_list(false);

    if (empty($user_list['user_list'])) die();

    $readable_name_mappings = array
    (
        "user_id" => 'record_id',
        "user_name" => 'username',
        "email" => 'email',
        "is_validated" => 'is_validated',
        "info_completed" => 'info_completed',
        "user_money" => 'user_money',
        "frozen_money" => 'frozen_money',
        "rank_points" => 'rank_points',
        "pay_points" => 'pay_points',
        "reg_time" => 'reg_date',
        "user_rank" => 'user_rank',
        "last_login" => '最後登入',
        "visit_count" => '瀏覽次數',
        "language" => '語言',
        "birthday" => '出生日期',
        "live_area" => '常住地區',
        "work_area" => '工作地區',
        "sex" => '性別',
    );

    $content = '';

    $first_user = $user_list['user_list'][0];
    foreach ($first_user as $key => $value)
    {
        $readable_name = isset($readable_name_mappings[$key]) ? (isset($_LANG[$readable_name_mappings[$key]]) ? $_LANG[$readable_name_mappings[$key]] : $readable_name_mappings[$key]) : $key;
        $content .= replace_special_char($readable_name) . ',';
    }
    $content .= "\n";

    foreach ($user_list['user_list'] as $user)
    {
        foreach ($user as $value)
        {
            $content .= replace_special_char($value) . ',';
        }
        $content{strlen($content) - 1} = "\n";
    }

    $charset = empty($_POST['charset_custom']) ? 'UTF8' : trim($_POST['charset_custom']);
    $content = (EC_CHARSET == $charset) ? $content : ecs_iconv(EC_CHARSET, $charset, $content);

    header("Content-Disposition: attachment; filename=user_list.csv");
    header("Content-Type: text/csv");
    die($content);
}

/*------------------------------------------------------ */
//-- 導出會員資料 Excel
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'downloadxlsx')
{
    /* 检查权限 */
    admin_priv('users_manage');

    if (!$_SESSION['manage_cost'] && !check_authz('download_userslist_xlsx'))
        sys_msg('對不起,您沒有執行此項操作的權限!');   //sys_msg('只有Super Admin權限才能使用此功能');

    ini_set('memory_limit', '1024M');
    // set_time_limit(300);

    $user_list = user_list(false);

    if (empty($user_list['user_list'])) die();
    $file_name = 'user_list_' . local_date('Y-m-d'). '.xlsx';
	define('PCLZIP_TEMPORARY_DIR', ROOT_PATH . 'temp/');
	require_once ROOT_PATH . 'includes/excel/PHPExcel.php';
	require_once ROOT_PATH . 'includes/excel/PHPExcel/IOFactory.php';
	\PHPExcel_Settings::setZipClass(\PHPExcel_Settings::PCLZIP);
	$objPHPExcel = new \PHPExcel();
    /* 文件标题 */
    // $writer->addRow(array(ecs_iconv(EC_CHARSET, 'UTF8', local_date('Y-m-d') . ' 會員列表')));
    if($_SESSION['manage_cost']){
        $export[] = array(
            ecs_iconv(EC_CHARSET, 'UTF8', '編號'),
            ecs_iconv(EC_CHARSET, 'UTF8', '會員名稱'),
            ecs_iconv(EC_CHARSET, 'UTF8', '郵件地址'),
            ecs_iconv(EC_CHARSET, 'UTF8', '是否已驗證'),
            ecs_iconv(EC_CHARSET, 'UTF8', '已填資料'),
            ecs_iconv(EC_CHARSET, 'UTF8', '可用資金'),
            ecs_iconv(EC_CHARSET, 'UTF8', '凍結資金'),
            ecs_iconv(EC_CHARSET, 'UTF8', '等級積分'),
            ecs_iconv(EC_CHARSET, 'UTF8', '消費積分'),
            ecs_iconv(EC_CHARSET, 'UTF8', '註冊日期'),
            ecs_iconv(EC_CHARSET, 'UTF8', '會員等級'),
            ecs_iconv(EC_CHARSET, 'UTF8', '最後登入'),
            ecs_iconv(EC_CHARSET, 'UTF8', '瀏覽次數'),
            ecs_iconv(EC_CHARSET, 'UTF8', '語言'),
            ecs_iconv(EC_CHARSET, 'UTF8', '出生日期'),
            ecs_iconv(EC_CHARSET, 'UTF8', '常住地區'),
            ecs_iconv(EC_CHARSET, 'UTF8', '工作地區'),
            ecs_iconv(EC_CHARSET, 'UTF8', '性別')
        );
    }
    else{
        $export[] = array(
            ecs_iconv(EC_CHARSET, 'UTF8', '編號'),
            ecs_iconv(EC_CHARSET, 'UTF8', '會員名稱'),
            ecs_iconv(EC_CHARSET, 'UTF8', '郵件地址'),
            ecs_iconv(EC_CHARSET, 'UTF8', '是否已驗證'),
            ecs_iconv(EC_CHARSET, 'UTF8', '已填資料'),
            //ecs_iconv(EC_CHARSET, 'UTF8', '可用資金'),
            //ecs_iconv(EC_CHARSET, 'UTF8', '凍結資金'),
            ecs_iconv(EC_CHARSET, 'UTF8', '等級積分'),
            ecs_iconv(EC_CHARSET, 'UTF8', '消費積分'),
            ecs_iconv(EC_CHARSET, 'UTF8', '註冊日期'),
            ecs_iconv(EC_CHARSET, 'UTF8', '會員等級'),
            ecs_iconv(EC_CHARSET, 'UTF8', '最後登入'),
            ecs_iconv(EC_CHARSET, 'UTF8', '瀏覽次數'),
            //ecs_iconv(EC_CHARSET, 'UTF8', '語言'),
            //ecs_iconv(EC_CHARSET, 'UTF8', '出生日期'),
            //ecs_iconv(EC_CHARSET, 'UTF8', '常住地區'),
            //ecs_iconv(EC_CHARSET, 'UTF8', '工作地區'),
            //ecs_iconv(EC_CHARSET, 'UTF8', '性別')
        );
    }
    
    
    foreach ($user_list['user_list'] as $user)
    {
        if($_SESSION['manage_cost']){
            $export[] = array(
                $user['user_id'],
                $user['user_name'],
                $user['email'],
                $user['is_validated'],
                $user['info_completed'],
                $user['user_money'],
                $user['frozen_money'],
                $user['rank_points'],
                $user['pay_points'],
                $user['reg_time'],
                $user['user_rank'],
                $user['last_login'],
                $user['visit_count'],
                $user['language'],
                $user['birthday'],
                $user['live_area'],
                $user['work_area'],
                $user['sex']
            );
        }
        else{
            $export[] = array(
                $user['user_id'],
                $user['user_name'],
                $user['email'],
                $user['is_validated'],
                $user['info_completed'],
                //$user['user_money'],
                //$user['frozen_money'],
                $user['rank_points'],
                $user['pay_points'],
                $user['reg_time'],
                $user['user_rank'],
                $user['last_login'],
                $user['visit_count'],
                //$user['language'],
                //$user['birthday'],
                //$user['live_area'],
                //$user['work_area'],
                //$user['sex']
            );
        }
    }
    $objPHPExcel->setActiveSheetIndex()->fromArray($export,null,'A1');
    header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.‌​sheet; charset=utf-8');
	header('Content-Disposition: attachment; filename=' . $file_name);
	header('Cache-Control: max-age=0');
	$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
}

/*------------------------------------------------------ */
//-- 導出會員電郵
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'emailcsv')
{
    /* 检查权限 */
    admin_priv('users_manage');
    if (!$_SESSION['manage_cost'])
        sys_msg('只有Super Admin權限才能使用此功能');

    ini_set('memory_limit', '512M');

    $ex_where = " WHERE `is_validated` = 1 OR `email` NOT REGEXP '^\\\\+?[0-9-]+@yohohongkong\\\\.com$' ";
    $sql = "SELECT user_id, user_name, email".
           " FROM " . $GLOBALS['ecs']->table('users') . $ex_where.
           " ORDER BY user_id ASC";

    $user_list = $GLOBALS['db']->getAll($sql);

    $readable_name_mappings = array
    (
        "user_id" => 'record_id',
        "user_name" => 'username',
        "email" => 'email'
    );

    $content = '';

    $first_user = $user_list[0];
    foreach ($first_user as $key => $value)
    {
        $readable_name = isset($readable_name_mappings[$key]) ? $_LANG[$readable_name_mappings[$key]] : $key;
        $content .= replace_special_char($readable_name) . ',';
    }
    $content .= "\n";

    foreach ($user_list as $user)
    {
        foreach ($user as $value)
        {
            $content .= replace_special_char($value) . ',';
        }
        $content .= "\n";
    }

    $charset = empty($_POST['charset_custom']) ? 'UTF8' : trim($_POST['charset_custom']);
    $output = ecs_iconv(EC_CHARSET, $charset, $content);

    header("Content-Disposition: attachment; filename=user_email_list.csv");
    header("Content-Type: text/csv");
    die($output);
}

/*------------------------------------------------------ */
//-- 使用會員身分登入
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'loginas')
{
    /* 检查权限 */
    admin_priv('users_manage');
    
    $user_id = empty($_REQUEST['user_id']) ? '' : intval($_REQUEST['user_id']);
    
    $username = $db->getOne("SELECT user_name FROM " . $ecs->table('users') . " WHERE user_id = " . $user_id . " LIMIT 1");
    
    if (empty($username))
    {
        sys_msg('User does not exist');
    }
    
    $admin_pwd = $db->getOne("SELECT password FROM " . $ecs->table('admin_user') . " WHERE user_id = '" . intval($_SESSION['admin_id']) . "'");
    $admin_pwd = md5($admin_pwd . $_CFG['hash_code']);
    $admin_login = base64_encode(json_encode(array('id' => $_SESSION['admin_id'], 'pw' => $admin_pwd)));
    
    echo '<form id="loginForm" action="../user.php" method="post">';
    echo '<input type="hidden" name="act" value="act_login">';
    echo '<input type="hidden" name="username" value="' . $username . '">';
    echo '<input type="hidden" name="admin_login" value="' . $admin_login . '">';
    echo '</form>';
    echo '<script type="text/javascript">';
    echo 'document.getElementById(\'loginForm\').submit();';
    echo '</script>';
    exit;
}

/*------------------------------------------------------ */
//-- AJAX 搜尋會員
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'get_users_list')
{
    /* 检查权限 */
    check_authz_json('users_manage');
    
    header('Content-Type: application/json');
    echo json_encode(user_list());
    exit;
}

/*------------------------------------------------------ */
//-- 修改會員名稱
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'edit_user_name')
{
    /* 检查权限 */
    check_authz_json('users_manage');
    
    $old_name = empty($_POST['old_name']) ? '' : trim($_POST['old_name']);
    $new_name = empty($_POST['new_name']) ? '' : trim($_POST['new_name']);
    
    $sql = "SELECT user_id FROM " . $ecs->table('users') . " WHERE `user_name` = '" . $old_name . "'";
    $user_id = $db->getOne($sql);
    
    if (empty($user_id))
    {
        make_json_error('找不到 ' . stripslashes($old_name));
    }
    
    $sql = "SELECT user_id FROM " . $ecs->table('users') . " WHERE `user_name` = '" . $new_name . "'";
    $res = $db->getOne($sql);
    
    if (!empty($res))
    {
        make_json_error(stripslashes($new_name) . ' 已被使用');
    }
    
    $sql = "UPDATE " . $ecs->table('users') .
            "SET `user_name` = '" . $new_name . "' " .
            "WHERE `user_id` = '" . $user_id . "' " .
            "LIMIT 1";
    $res = $db->query($sql, 'SILENT');
    
    if ($res)
    {
        admin_log(addslashes($new_name), 'edit', 'users');
        
        clear_cache_files();
        make_json_result('', '修改成功');
    }
    else
    {
        make_json_error('修改失敗');
    }
}

/**
 *  返回用户列表数据
 *
 * @access  public
 * @param
 *
 * @return void
 */
function user_list($is_pagination = true)
{
    global $ecs, $db, $_LANG, $userController;
    /* 过滤条件 */
    $_REQUEST['keywords'] = empty($_REQUEST['keywords']) ? '' : trim($_REQUEST['keywords']);
    if (isset($_REQUEST['is_ajax']) && $_REQUEST['is_ajax'] == 1)
    {
        $_REQUEST['keywords'] = json_str_iconv($_REQUEST['keywords']);
    }
    $_REQUEST['rank'] = empty($_REQUEST['rank']) ? 0 : intval($_REQUEST['rank']);
    $_REQUEST['pay_points_gt'] = empty($_REQUEST['pay_points_gt']) ? 0 : intval($_REQUEST['pay_points_gt']);
    $_REQUEST['pay_points_lt'] = empty($_REQUEST['pay_points_lt']) ? 0 : intval($_REQUEST['pay_points_lt']);

    $_REQUEST['bought_goods'] = empty($_REQUEST['bought_goods']) ? '' : trim($_REQUEST['bought_goods']);
    $_REQUEST['have_email'] = empty($_REQUEST['have_email']) ? 0 : intval($_REQUEST['have_email']);

    $_REQUEST['sort_by']    = empty($_REQUEST['sort_by'])    ? 'user_id' : trim($_REQUEST['sort_by']);
    $_REQUEST['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC'    : trim($_REQUEST['sort_order']);
    $_REQUEST['start']      = empty($_REQUEST['start'])    ? '0' : trim($_REQUEST['start']);
    $_REQUEST['cst']      = empty($_REQUEST['cst'])    ? '0' : intval($_REQUEST['cst']);
    $_REQUEST['csta']      = empty($_REQUEST['csta'])    ? '0' : intval($_REQUEST['csta']);
    $_REQUEST['range']      = empty($_REQUEST['range'])    ? '0' : intval($_REQUEST['range']);
    $_REQUEST['guest_stats_type']      = empty($_REQUEST['guest_stats_type'])    ? '0' : intval($_REQUEST['guest_stats_type']);
    $_REQUEST['page_size']  = empty($_REQUEST['page_size']) ? '50'    : trim($_REQUEST['page_size']);

    $ex_where = ' WHERE 1 ';
    if ($_SESSION['manage_cost']) // 如果是超級管理員，可以查看完整會員列表
    {
        if ($_REQUEST['keywords'])
        {
            $ex_where .= " AND (u.user_name LIKE '%" . mysql_like_quote($_REQUEST['keywords']) ."%' OR u.email LIKE '%" . mysql_like_quote($_REQUEST['keywords']) ."%')";
        }
    }
    else
    {
        // Non-admin, only works if user_name / email exact match or user_name postfix match
        if ($_REQUEST['keywords'])
        {
            $ex_where .= " AND (u.user_name LIKE '%" . mysql_like_quote($_REQUEST['keywords']) ."' OR u.email = '" . $_REQUEST['keywords'] ."')";
        } else if(!$_REQUEST['cst'] && !$_REQUEST['csta'] && !$_REQUEST['guest_stats_type']) {
            $ex_where .= " AND 0 ";
        }
    }
    if ($_REQUEST['cst']) {
        $ex_where .=" AND u.user_id IN (" .
        "SELECT DISTINCT(us.user_id) FROM (" .
        "SELECT CASE" .
            " WHEN sum(oi.goods_amount) = 0 THEN 0" .
            " WHEN sum(oi.goods_amount) > 0 AND sum(oi.goods_amount) < 1000 THEN 1" .
            " WHEN sum(oi.goods_amount) >= 1000 AND sum(oi.goods_amount) < 1500 THEN 2" .
            " WHEN sum(oi.goods_amount) >= 1500 AND sum(oi.goods_amount) < 3000 THEN 3" .
            " WHEN sum(oi.goods_amount) >= 3000 AND sum(oi.goods_amount) < 7000 THEN 4" .
            " WHEN sum(oi.goods_amount) >= 7000 THEN 5" .
            " ELSE 6 ".
        " END AS seg, u.user_id ".
        " FROM " . $ecs->table('users') ." as u ".
        " LEFT JOIN " . $ecs->table('order_info') ." as oi ON oi.user_id = u.user_id ".
        " AND order_status NOT IN (" . OS_CANCELED . ',' . OS_INVALID . ',' . OS_RETURNED . ") ".
        " AND pay_status = " . PS_PAYED . " ".
        " WHERE u.user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).") ".
        " GROUP BY u.user_id ) us WHERE us.seg = ".$_REQUEST['range'].
        ") ";
    }else if ($_REQUEST['csta']) {
        $ex_where .=" AND u.user_id IN (" .
        "SELECT DISTINCT(us.user_id) FROM (" .
        "SELECT CASE" .
            " WHEN sum(oi.money_paid+oi.order_amount) / count(oi.order_id) = 0 THEN 0" .
            " WHEN sum(oi.money_paid+oi.order_amount) / count(oi.order_id) > 0 AND sum(oi.money_paid+oi.order_amount) / count(oi.order_id) < 1000 THEN 1" .
            " WHEN sum(oi.money_paid+oi.order_amount) / count(oi.order_id) >= 1000 AND sum(oi.money_paid+oi.order_amount) / count(oi.order_id) < 1500 THEN 2" .
            " WHEN sum(oi.money_paid+oi.order_amount) / count(oi.order_id) >= 1500 AND sum(oi.money_paid+oi.order_amount) / count(oi.order_id) < 3000 THEN 3" .
            " WHEN sum(oi.money_paid+oi.order_amount) / count(oi.order_id) >= 3000 AND sum(oi.money_paid+oi.order_amount) / count(oi.order_id) < 7000 THEN 4" .
            " WHEN sum(oi.money_paid+oi.order_amount) / count(oi.order_id) >= 7000 THEN 5" .
            " ELSE 6 ".
        " END AS seg, u.user_id ".
        " FROM " . $ecs->table('users') ." as u ".
        " LEFT JOIN " . $ecs->table('order_info') ." as oi ON oi.user_id = u.user_id ".
        " AND order_status NOT IN (" . OS_CANCELED . ',' . OS_INVALID . ',' . OS_RETURNED . ") ".
        " AND pay_status = " . PS_PAYED . " ".
        " WHERE u.user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).") ".
        " GROUP BY u.user_id ) us WHERE us.seg = ".$_REQUEST['range'].
        ") ";
    } else if ($_REQUEST['guest_stats_type']) {
        $now  = local_strtotime("now");
        $range = $_REQUEST['range'];
        if($_REQUEST['guest_stats_type'] == 1) {
            $ex_where .=" AND u.user_id IN (" .
            "SELECT DISTINCT(us.user_id) FROM (" .
            "SELECT CASE" .
                " WHEN ($now - oi.pay_time) / 86400 >= 0 AND ($now - oi.pay_time)/86400 < 30 THEN 0 ". // 0 to 1 month
                " WHEN ($now - oi.pay_time)/86400 >= 30 AND ( $now - oi.pay_time)/86400 < (6 * 30 ) THEN 1 ". // 1 to 6 month
                " WHEN ($now - oi.pay_time)/86400 >= (6 * 30) THEN 2 ".
                " ELSE 3 ".
            " END AS buy_total, u.user_id ".
            " FROM " . $ecs->table('users') ." as u ".
            " LEFT JOIN " . $ecs->table('order_info') ." as oi ON oi.user_id = u.user_id ".
            " AND order_status NOT IN (" . OS_CANCELED . ',' . OS_INVALID . ',' . OS_RETURNED . ") ".
            " AND pay_status = " . PS_PAYED . " ".
            " WHERE u.user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).")" .
            " GROUP BY u.user_id HAVING count(oi.order_id) = 1 ) us WHERE us.buy_total = $range".
            ") ";
        } else if ($_REQUEST['guest_stats_type'] == 2) {
            $ex_where .=" AND u.user_id IN (" .
            "SELECT DISTINCT(us.user_id) FROM (" .
            "SELECT CASE" .
                " WHEN (cast(min(oi.pay_time) as signed) - cast(u.reg_time as signed)) / (86400) < 30 THEN 0 ". // 0 to 1 month
                " WHEN (cast(min(oi.pay_time) as signed) - cast(u.reg_time as signed)) / (86400) >= 30 AND (cast(min(oi.pay_time) as signed) - cast(u.reg_time as signed)) / (86400) < (6 * 30 ) THEN 1 ". // 1 to 6 month
                " WHEN (cast(min(oi.pay_time) as signed) - cast(u.reg_time as signed)) / (86400) >= (6 * 30) THEN 2 ".
                " ELSE 3 ".
            " END AS buy_total, u.user_id ".
            " FROM " . $ecs->table('users') ." as u ".
            " LEFT JOIN " . $ecs->table('order_info') ." as oi ON oi.user_id = u.user_id ".
            " AND order_status NOT IN (" . OS_CANCELED . ',' . OS_INVALID . ',' . OS_RETURNED . ") ".
            " AND pay_status = " . PS_PAYED . " ".
            " WHERE u.user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).")".
            " AND oi.platform NOT IN ('pos_exhibition','pos') ".
            " AND (oi.order_type <> '" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT . "' OR oi.order_type IS NULL) ".
            " GROUP BY u.user_id HAVING count(oi.order_id) > 1 ) us WHERE us.buy_total = $range".
            ") ";
        } else if ($_REQUEST['guest_stats_type'] == 3) {
            $ex_where .=" AND u.user_id IN (" .
            "SELECT DISTINCT(us.user_id) FROM (" .
            "SELECT CASE" .
                " WHEN (max(oi.pay_time) -  min(oi.pay_time)) / ((count(oi.order_id) - 1) * 86400) < 30 THEN 0 ". // 0 to 1 month
                " WHEN (max(oi.pay_time) -  min(oi.pay_time)) / ((count(oi.order_id) - 1) * 86400) >= 30 AND (max(oi.pay_time) -  min(oi.pay_time)) / ((count(oi.order_id) - 1) * 86400) < (6 * 30 ) THEN 1 ". // 1 to 6 month
                " WHEN (max(oi.pay_time) -  min(oi.pay_time)) / ((count(oi.order_id) - 1) * 86400) >= (6 * 30) THEN 2 ".
                " ELSE 3 ".
            " END AS buy_total, u.user_id ".
            " FROM " . $ecs->table('users') ." as u ".
            " LEFT JOIN " . $ecs->table('order_info') ." as oi ON oi.user_id = u.user_id ".
            " AND order_status NOT IN (" . OS_CANCELED . ',' . OS_INVALID . ',' . OS_RETURNED . ") ".
            " AND pay_status = " . PS_PAYED . " ".
            " WHERE u.user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).")" .
            " GROUP BY u.user_id HAVING count(oi.order_id) > 1 AND max(oi.pay_time) BETWEEN ($now - (365 * 86400)) AND $now ) us WHERE us.buy_total = $range".
            ") ";
        } else if ($_REQUEST['guest_stats_type'] == 4) {
            $ex_where .=" AND u.user_id IN (" .
            "SELECT DISTINCT(us.user_id) FROM (" .
            "SELECT CASE" .
                " WHEN (max(oi.pay_time) -  min(oi.pay_time)) / ((count(oi.order_id) - 1) * 86400) < 30 THEN 0 ". // 0 to 1 month
                " WHEN (max(oi.pay_time) -  min(oi.pay_time)) / ((count(oi.order_id) - 1) * 86400) >= 30 AND (max(oi.pay_time) -  min(oi.pay_time)) / ((count(oi.order_id) - 1) * 86400) < (6 * 30 ) THEN 1 ". // 1 to 6 month
                " WHEN (max(oi.pay_time) -  min(oi.pay_time)) / ((count(oi.order_id) - 1) * 86400) >= (6 * 30) THEN 2 ".
                " ELSE 3 ".
            " END AS buy_total, u.user_id ".
            " FROM " . $ecs->table('users') ." as u ".
            " LEFT JOIN " . $ecs->table('order_info') ." as oi ON oi.user_id = u.user_id ".
            " AND order_status NOT IN (" . OS_CANCELED . ',' . OS_INVALID . ',' . OS_RETURNED . ") ".
            " AND pay_status = " . PS_PAYED . " ".
            " WHERE u.user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).")" .
            " GROUP BY u.user_id HAVING count(oi.order_id) > 1 AND max(oi.pay_time) < $now - (365 * 86400) ) us WHERE us.buy_total = $range".
            ") ";
        } else if ($_REQUEST['guest_stats_type'] == 11) {// within 365 days with login activity or with purchase
            $ex_where .=" AND u.user_id IN (" .
            "SELECT DISTINCT(u.user_id) FROM ".$ecs->table('users')." as u ".
            " WHERE (u.last_login > (".local_strtotime("now")." - (365*86400) )".
            " OR u.user_id IN ( SELECT oi.user_id FROM " . $ecs->table('order_info') ." as oi ".
            " WHERE oi.pay_time > (".local_strtotime("now")." - (365*86400) )".
            " AND oi.order_status NOT IN (" . OS_CANCELED . ',' . OS_INVALID . ',' . OS_RETURNED . ") ".
            " AND oi.pay_status = " . PS_PAYED . " ".
            ") )AND u.user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).") ".
            ") ";
        } else if ($_REQUEST['guest_stats_type'] == 12) {// within 10 days with login, no purchase
            $ex_where .=" AND u.user_id IN (" .
            "SELECT DISTINCT(u.user_id) FROM ".$ecs->table('users')." as u ".
            " WHERE (u.last_login > (".local_strtotime("now")." - (10 * 86400) ) ".
            " AND u.user_id NOT IN (SELECT oi.user_id FROM ". $ecs->table('order_info') ." as oi WHERE ".
            " oi.pay_time > (".local_strtotime("now")." - (10 * 86400) ) ".
            " AND oi.order_status NOT IN (" . OS_CANCELED . ',' . OS_INVALID . ',' . OS_RETURNED . ") ".
            " AND oi.pay_status = " . PS_PAYED . ") )".
            " AND u.user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).") ".
            ") ";
        } else if ($_REQUEST['guest_stats_type'] == 13) { // over 365 days without login activity or without purchase
            $ex_where .=" AND u.user_id IN (" .
            "SELECT DISTINCT(u.user_id) FROM ".$ecs->table('users')." as u ".
            " WHERE (u.last_login < (".local_strtotime("now")." - (365*86400) )".
            " AND u.user_id NOT IN ( SELECT oi.user_id FROM " . $ecs->table('order_info') ." as oi ".
            " WHERE oi.pay_time > (".local_strtotime("now")." - (365*86400) )".
            " AND oi.order_status NOT IN (" . OS_CANCELED . ',' . OS_INVALID . ',' . OS_RETURNED . ") ".
            " AND oi.pay_status = " . PS_PAYED . " ".
            ") ) AND u.user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).") ".
            ") ";
        } else if ($_REQUEST['guest_stats_type'] == 14) {// over 365 days, only 1 visit count, without purchase
            $ex_where .=" AND u.user_id IN (" .
            "SELECT DISTINCT(u.user_id) FROM ".$ecs->table('users')." as u ".
            " WHERE (u.last_login < (".local_strtotime("now")." - (365 * 86400) ) ".
            " AND u.visit_count = 1 ".
            " AND u.user_id NOT IN (SELECT oi.user_id FROM ". $ecs->table('order_info') ." as oi WHERE ".
            " oi.order_status NOT IN (" . OS_CANCELED . ',' . OS_INVALID . ',' . OS_RETURNED . ") ".
            " AND oi.pay_status = " . PS_PAYED . ") )".
            " AND u.user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).") ".
            ") ";
        } else if ($_REQUEST['guest_stats_type'] == 15) {// Inactive members with strong purchase power: customer segment(客人平均消費力) $1500 above, over 1 year no login or purchase
            $ex_where .=" AND u.user_id IN (" .
            "SELECT DISTINCT(u.user_id) FROM ".$ecs->table('users')." as u ".
            " WHERE u.last_login < (".local_strtotime("now")." - (365*86400) )".
            " AND u.user_id IN (SELECT u.user_id ".
            " FROM " . $ecs->table('users') ." as u ".
            " LEFT JOIN " . $ecs->table('order_info') ." as oi ON oi.user_id = u.user_id ".
            " AND order_status NOT IN (" . OS_CANCELED . ',' . OS_INVALID . ',' . OS_RETURNED . ") ".
            " AND pay_status = " . PS_PAYED . " ".
            " WHERE u.user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).")" .
            " GROUP BY u.user_id HAVING sum(oi.money_paid+oi.order_amount) / count(oi.order_id) >= 1500 AND MAX(oi.pay_time) < (".local_strtotime("now")." - (365*86400) ) )".
            ") ";
        } else if ($_REQUEST['guest_stats_type'] == 16) {// Only with offline purchase record, never login (within 6 months)
            $ex_where .=" AND u.user_id IN (" .
            "SELECT DISTINCT(u.user_id) FROM ".$ecs->table('users')." as u ".
            " WHERE  u.last_login = 0 AND u.is_wholesale = 0 ".
            " AND u.user_id IN (SELECT oi.user_id FROM ". $ecs->table('order_info') ." as oi WHERE ".
            " oi.pay_time > (".local_strtotime("now")." - (180 * 86400)) ".
            " AND oi.order_status NOT IN (" . OS_CANCELED . ',' . OS_INVALID . ',' . OS_RETURNED . ") ".
            " AND (oi.order_type<>'".Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE."' OR oi.order_type IS NULL) ".
            " AND oi.pay_status = " . PS_PAYED . " )".
            ") ";
        } else if ($_REQUEST['guest_stats_type'] == 17) {// Only with offline purchase record, never login (over 6 months)
            $ex_where .=" AND u.user_id IN (" .
            "SELECT DISTINCT(u.user_id) FROM ".$ecs->table('users')." as u ".
            " WHERE  u.last_login = 0 AND u.is_wholesale = 0 ".
            " AND u.user_id NOT IN (SELECT oi.user_id FROM ". $ecs->table('order_info') ." as oi WHERE ".
            " oi.pay_time > (".local_strtotime("now")." - (180 * 86400)) ".
            " AND oi.order_status NOT IN (" . OS_CANCELED . ',' . OS_INVALID . ',' . OS_RETURNED . ") ".
            " AND (oi.order_type<>'".Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE."' OR oi.order_type IS NULL) ".
            " AND oi.pay_status = " . PS_PAYED . " )".
            ") ";
        }
    }
    if ($_REQUEST['rank'])
    {
        $sql = "SELECT min_points, max_points, special_rank FROM ".$GLOBALS['ecs']->table('user_rank')." WHERE rank_id = '$_REQUEST[rank]'";
        $row = $GLOBALS['db']->getRow($sql);
        if ($row['special_rank'] > 0)
        {
            /* 特殊等级 */
            $ex_where .= " AND u.user_rank = '$_REQUEST[rank]' ";
        }
        else
        {
            $ex_where .= " AND u.rank_points >= " . intval($row['min_points']) . " AND u.rank_points < " . intval($row['max_points']);
        }
    }
    if ($_REQUEST['pay_points_gt'])
    {
            $ex_where .=" AND u.pay_points >= '$_REQUEST[pay_points_gt]' ";
    }
    if ($_REQUEST['pay_points_lt'])
    {
        $ex_where .=" AND u.pay_points < '$_REQUEST[pay_points_lt]' ";
    }
    if ($_REQUEST['bought_goods'])
    {
        if (!preg_match('/^([0-9]+,)*[0-9]+$/', $_REQUEST['bought_goods']))
        {
            $error_msg = '「曾購買產品ID」欄位輸入錯誤，請輸入以逗號(,)分隔的產品ID';
            if ($_REQUEST['is_ajax'])
            {
                make_json_error($error_msg);
            }
            else
            {
                sys_msg($error_msg);
            }
        }
        
        require_once ROOT_PATH . 'includes/lib_order.php';
        
        $ex_where .=" AND u.user_id IN (" .
                        "SELECT oi.user_id " . 
                        "FROM " . $GLOBALS['ecs']->table('order_info') . " as oi " .
                        "LEFT JOIN " . $GLOBALS['ecs']->table('order_goods') . " as og ON oi.order_id = og.order_id " . 
                        "WHERE og.goods_id IN ({$_REQUEST['bought_goods']}) " . order_query_sql('finished', 'oi.') .
                    ") ";
    }
    if ($_REQUEST['have_email'])
    {
        $ex_where .=" AND u.`email` != '' AND u.`email` NOT REGEXP '^\\\\+?[0-9-]+@yohohongkong\\\\.com$' ";
    }

    $_REQUEST['record_count'] = $GLOBALS['db']->getOne("SELECT COUNT(*) FROM " . $GLOBALS['ecs']->table('users') . " as u " . $ex_where);

    if ($is_pagination)
    {
        $sql = "SELECT u.user_id, u.user_name, u.email, u.is_validated, u.info_completed, u.user_money, u.frozen_money, u.rank_points, u.pay_points, u.reg_time, u.user_rank, u.last_login, u.visit_count, u.language, u.is_wholesale ".
                " FROM " . $GLOBALS['ecs']->table('users') . " as u " . $ex_where .
                " ORDER by " . $_REQUEST['sort_by'] . ' ' . $_REQUEST['sort_order'];
        
        /* 分页大小 */
        $sql .= " LIMIT " . $_REQUEST['start'] . ',' . $_REQUEST['page_size'];

        $user_list = $GLOBALS['db']->getAll($sql);
    }
    else
    {
        $limit = 5000;
        $user_list = array();
        for($alread_get = 0; $alread_get < $_REQUEST['record_count']; $alread_get+=$limit){
            $sql = "SELECT u.user_id, u.user_name, u.email, u.is_validated, u.info_completed, u.user_money, u.frozen_money, u.rank_points, u.pay_points, u.reg_time, u.user_rank, u.last_login, u.visit_count, u.language, u.birthday, rei.content as live_area, rei2.content as work_area, u.sex, u.is_wholesale " .
                "FROM " . $GLOBALS['ecs']->table('users') . " as u " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('reg_extend_info') . " as rei ON rei.user_id = u.user_id AND rei.reg_field_id = 11 " .
                "LEFT JOIN " . $GLOBALS['ecs']->table('reg_extend_info') . " as rei2 ON rei2.user_id = u.user_id AND rei2.reg_field_id = 12 " .
                $ex_where .
                " ORDER by " . $_REQUEST['sort_by'] . ' ' . $_REQUEST['sort_order'] . " LIMIT " . $alread_get . ", " . $limit;
            $user_list_temp = $GLOBALS['db']->getAll($sql);
            foreach($user_list_temp as $temp){
                $user_list[] = $temp;
            }
        }
    }
    
    
    
    $count = count($user_list);
    for ($i=0; $i<$count; $i++)
    {
        $user_list[$i]['reg_time'] = local_date($GLOBALS['_CFG']['date_format'], $user_list[$i]['reg_time']);
		$user_list[$i]['user_rank'] = get_user_rankname($user_list[$i]['user_rank'],$user_list[$i]['rank_points']);
		$user_list[$i]['last_login'] = ($user_list[$i]['last_login']) ? local_date($GLOBALS['_CFG']['time_format'], $user_list[$i]['last_login']) :  '從未登入';
        if (!$is_pagination)
        {
            $user_list[$i]['sex'] = ($user_list[$i]['sex'] == 1) ? '男' : (($user_list[$i]['sex'] == 2) ? '女' : '未知');
        }
        $view_order = ['icon_class' => 'fa fa-eye','link' => "order.php?act=list&user_id=".$user_list[$i]['user_id'], 'label' => '訂單', 'title' => $_LANG['view_order']];
        $view_deposit = ['icon_class' => 'fa fa-money-check','btn_class'=>'btn btn-success','link' => "account_log.php?act=list&user_id=".$user_list[$i]['user_id'], 'label' => '帳目', 'title' => $_LANG['view_deposit']];
        $custom_actions = ['view_order' => $view_order,'view_deposit' => $view_deposit];
        $user_list[$i]['_action'] = $userController->generateCrudActionBtn($user_list[$i]['user_id'], 'id', null, NULL, $custom_actions);
        if($_SESSION['admin_name'] == 'admin') {
            $html = '<div style="margin-top:5px;"><a href="users.php?act=loginas&user_id='.$user_list[$i]['user_id'].'" title="以會員身份登入" target="_blank">以會員身份登入</a></div>';
            $user_list[$i]['_action'] .= $html;
        }
    }

    $arr = array('user_list' => $user_list, 'record_count' => $_REQUEST['record_count']);

    return $arr;
}

function get_all_user_ranks()
{
    static $all_ranks = null;
    if ($all_ranks === null)
    {
        $sql = "SELECT rank_id, rank_name, min_points, special_rank " .
               " FROM " . $GLOBALS['ecs']->table('user_rank') .
               " ORDER BY min_points ASC";
        $all_ranks = $GLOBALS['db']->getAll($sql);
        if ($all_ranks === false) $all_ranks = array();
    }
    return $all_ranks;
}

// 取得會員等級名稱
function get_user_rankname($user_rank, $rank_points)
{
    $all_ranks = get_all_user_ranks();

    if ($user_rank > 0)
    {
        foreach ($all_ranks as $rank)
        {
            if ($rank['rank_id'] == $user_rank)
            {
                return $rank['rank_name'];
            }
        }
    }
    else
    {
        $max_rank = '';
        $max_mp = 0;
        foreach ($all_ranks as $rank)
        {
            if (($rank['min_points'] <= $rank_points) && ($rank['min_points'] >= $max_mp) && $rank['special_rank'] == 0)
            {
                $max_rank = $rank['rank_name'];
                $max_mp = $rank['min_points'];
            }
        }
        if (!empty($max_rank))
        {
            return $max_rank;
        }
    }
    
    return $GLOBALS['_LANG']['undifine_rank'];
}

function image_path_format($content)
{
    $prefix = 'http://' . $_SERVER['SERVER_NAME'];
    $pattern = '/(background|src)=[\'|\"]((?!http:\/\/).*?)[\'|\"]/i';
    $replace = "$1='" . $prefix . "$2'";
    return preg_replace($pattern, $replace, $content);
}

/**
 * 替换影响csv文件的字符
 *
 * @param $str string 处理字符串
 */
function replace_special_char($str, $replace = true)
{
    $str = str_replace("\r\n", "", image_path_format($str));
    $str = str_replace("\t", "    ", $str);
    $str = str_replace("\n", "", $str);
    if ($replace == true)
    {
        $str = '"' . str_replace('"', '""', $str) . '"';
    }
    return $str;
}

?>