<?php
/**
 * ECSHOP 控制台首页
 * ============================================================================
 * 版权所有 2005-2010 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liuhui $
 * $Id: index.php 17163 2010-05-20 10:13:23Z liuhui $
 * Updated By Anthony@YOHO 2018-03-23
*/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require_once(ROOT_PATH . '/includes/lib_order.php');
require_once(ROOT_PATH . '/includes/lib_insert.php');

$erpController   = new Yoho\cms\Controller\ErpController();
$userController  = new Yoho\cms\Controller\UserController();
$taskController  = new Yoho\cms\Controller\TaskController();
$orderController = new Yoho\cms\Controller\OrderController();
/*------------------------------------------------------ */
//-- 框架
/*------------------------------------------------------ */
if ($_REQUEST['act'] == '')
{

    $gd = gd_version();

    /* 检查文件目录属性 */
    $warning = array();

    if ($_CFG['shop_closed'])
    {
        $warning[] = $_LANG['shop_closed_tips'];
    }

    if (file_exists('../install'))
    {
        $warning[] = $_LANG['remove_install'];
    }

    if (file_exists('../upgrade'))
    {
        $warning[] = $_LANG['remove_upgrade'];
    }

    if (file_exists('../demo'))
    {
        $warning[] = $_LANG['remove_demo'];
    }

    $open_basedir = ini_get('open_basedir');
    if (!empty($open_basedir))
    {
        /* 如果 open_basedir 不为空，则检查是否包含了 upload_tmp_dir  */
        $open_basedir = str_replace(array("\\", "\\\\"), array("/", "/"), $open_basedir);
        $upload_tmp_dir = ini_get('upload_tmp_dir');

        if (empty($upload_tmp_dir))
        {
            if (stristr(PHP_OS, 'win'))
            {
                $upload_tmp_dir = getenv('TEMP') ? getenv('TEMP') : getenv('TMP');
                $upload_tmp_dir = str_replace(array("\\", "\\\\"), array("/", "/"), $upload_tmp_dir);
            }
            else
            {
                $upload_tmp_dir = getenv('TMPDIR') === false ? '/tmp' : getenv('TMPDIR');
            }
        }

        if (!stristr($open_basedir, $upload_tmp_dir))
        {
            $warning[] = sprintf($_LANG['temp_dir_cannt_read'], $upload_tmp_dir);
        }
    }

    $result = file_mode_info('../cert');
    if ($result < 2)
    {
        $warning[] = sprintf($_LANG['not_writable'], 'cert', $_LANG['cert_cannt_write']);
    }

    $result = file_mode_info('../' . DATA_DIR);
    if ($result < 2)
    {
        $warning[] = sprintf($_LANG['not_writable'], 'data', $_LANG['data_cannt_write']);
    }
    else
    {
        $result = file_mode_info('../' . DATA_DIR . '/afficheimg');
        if ($result < 2)
        {
            $warning[] = sprintf($_LANG['not_writable'], DATA_DIR . '/afficheimg', $_LANG['afficheimg_cannt_write']);
        }

        $result = file_mode_info('../' . DATA_DIR . '/brandlogo');
        if ($result < 2)
        {
            $warning[] = sprintf($_LANG['not_writable'], DATA_DIR . '/brandlogo', $_LANG['brandlogo_cannt_write']);
        }

        $result = file_mode_info('../' . DATA_DIR . '/cardimg');
        if ($result < 2)
        {
            $warning[] = sprintf($_LANG['not_writable'], DATA_DIR . '/cardimg', $_LANG['cardimg_cannt_write']);
        }

        $result = file_mode_info('../' . DATA_DIR . '/feedbackimg');
        if ($result < 2)
        {
            $warning[] = sprintf($_LANG['not_writable'], DATA_DIR . '/feedbackimg', $_LANG['feedbackimg_cannt_write']);
        }

        $result = file_mode_info('../' . DATA_DIR . '/packimg');
        if ($result < 2)
        {
            $warning[] = sprintf($_LANG['not_writable'], DATA_DIR . '/packimg', $_LANG['packimg_cannt_write']);
        }
    }

    $result = file_mode_info('../images');
    if ($result < 2)
    {
        $warning[] = sprintf($_LANG['not_writable'], 'images', $_LANG['images_cannt_write']);
    }
    else
    {
        $result = file_mode_info('../' . IMAGE_DIR . '/upload');
        if ($result < 2)
        {
            $warning[] = sprintf($_LANG['not_writable'], IMAGE_DIR . '/upload', $_LANG['imagesupload_cannt_write']);
        }
    }

    $result = file_mode_info('../temp');
    if ($result < 2)
    {
        $warning[] = sprintf($_LANG['not_writable'], 'images', $_LANG['tpl_cannt_write']);
    }

    $result = file_mode_info('../temp/backup');
    if ($result < 2)
    {
        $warning[] = sprintf($_LANG['not_writable'], 'images', $_LANG['tpl_backup_cannt_write']);
    }

    if (!is_writeable('../' . DATA_DIR . '/order_print.html'))
    {
        $warning[] = $_LANG['order_print_canntwrite'];
    }
    clearstatcache();

    $smarty->assign('warning_arr', $warning);

    /* 管理员留言信息 */
    $sql = 'SELECT message_id, sender_id, receiver_id, sent_time, readed, deleted, title, message, user_name ' .
    'FROM ' . $ecs->table('admin_message') . ' AS a, ' . $ecs->table('admin_user') . ' AS b ' .
    "WHERE a.sender_id = b.user_id AND a.receiver_id = '$_SESSION[admin_id]' AND ".
    "a.readed = 0 AND deleted = 0 ORDER BY a.sent_time DESC";
    $admin_msg = $db->GetAll($sql);

    $smarty->assign('admin_msg', $admin_msg);

    /* 取得支持货到付款和不支持货到付款的支付方式 */
    $ids = get_pay_ids();

    $status['finished']    = CS_FINISHED;
    $status['await_ship']  = CS_AWAIT_SHIP;
    $status['await_pay']   = CS_AWAIT_PAY;
    $status['unconfirmed'] = OS_UNCONFIRMED;
    $start_time = local_mktime(0, 0, 0, local_date('m'), local_date('d'), local_date('Y'));
    $end_time = local_mktime(23, 59, 59, local_date('m'), local_date('d'), local_date('Y'));
    $today_paid_url = 'order.php?act=list&pay_start_time=' . $start_time . '&pay_end_time=' . $end_time . '&pay_status=2&display_normal_order=1';

    $smarty->assign('today_paid_url', $today_paid_url);
    $smarty->assign('status', $status);
    // $smarty->assign('virtual_card', $virtual_card);

    // /* 访问统计信息 */
    // $today  = local_getdate();
    // $sql    = 'SELECT COUNT(*) FROM ' .$ecs->table('stats').
    // ' WHERE access_time > ' . (mktime(0, 0, 0, $today['mon'], $today['mday'], $today['year']) - date('Z'));
    // 
    // $today_visit = $db->GetOne($sql);
    // $smarty->assign('today_visit', $today_visit);
    // 
    // $online_users = $sess->get_users_count();
    // $smarty->assign('online_users', $online_users);
    // 
    // /* 最近反馈 */
    // $sql = "SELECT COUNT(f.msg_id) ".
    // "FROM " . $ecs->table('feedback') . " AS f ".
    // "LEFT JOIN " . $ecs->table('feedback') . " AS r ON r.parent_id=f.msg_id " .
    // 'WHERE f.parent_id=0 AND ISNULL(r.msg_id) ' ;
    // $smarty->assign('feedback_number', $db->GetOne($sql));
    // 
    // /* 未审核评论 */
    // $smarty->assign('comment_number', $db->getOne('SELECT COUNT(*) FROM ' . $ecs->table('comment') .
    // ' WHERE status = 0 AND parent_id = 0'));
    // 
    // $mysql_ver = $db->version();   // 获得 MySQL 版本
    // 
    // /* 系统信息 */
    // $sys_info['os']            = PHP_OS;
    // $sys_info['ip']            = $_SERVER['SERVER_ADDR'];
    // $sys_info['web_server']    = $_SERVER['SERVER_SOFTWARE'];
    // $sys_info['php_ver']       = PHP_VERSION;
    // $sys_info['mysql_ver']     = $mysql_ver;
    // $sys_info['zlib']          = function_exists('gzclose') ? $_LANG['yes']:$_LANG['no'];
    // $sys_info['safe_mode']     = (boolean) ini_get('safe_mode') ?  $_LANG['yes']:$_LANG['no'];
    // $sys_info['safe_mode_gid'] = (boolean) ini_get('safe_mode_gid') ? $_LANG['yes'] : $_LANG['no'];
    // $sys_info['timezone']      = function_exists("date_default_timezone_get") ? date_default_timezone_get() : $_LANG['no_timezone'];
    // $sys_info['socket']        = function_exists('fsockopen') ? $_LANG['yes'] : $_LANG['no'];
    // 
    // if ($gd == 0)
    // {
    //     $sys_info['gd'] = 'N/A';
    // }
    // else
    // {
    //     if ($gd == 1)
    //     {
    //         $sys_info['gd'] = 'GD1';
    //     }
    //     else
    //     {
    //         $sys_info['gd'] = 'GD2';
    //     }
    // 
    //     $sys_info['gd'] .= ' (';
    // 
    //     /* 检查系统支持的图片类型 */
    //     if ($gd && (imagetypes() & IMG_JPG) > 0)
    //     {
    //         $sys_info['gd'] .= ' JPEG';
    //     }
    // 
    //     if ($gd && (imagetypes() & IMG_GIF) > 0)
    //     {
    //         $sys_info['gd'] .= ' GIF';
    //     }
    // 
    //     if ($gd && (imagetypes() & IMG_PNG) > 0)
    //     {
    //         $sys_info['gd'] .= ' PNG';
    //     }
    // 
    //     $sys_info['gd'] .= ')';
    // }
    // 
    // /* IP库版本 */
    // $sys_info['ip_version'] = ecs_geoip('255.255.255.0');
    // 
    // /* 允许上传的最大文件大小 */
    // $sys_info['max_filesize'] = ini_get('upload_max_filesize');
    // 
    // $smarty->assign('sys_info', $sys_info);
    // 
    // /* 缺货登记 */
    // $smarty->assign('booking_goods', $db->getOne('SELECT COUNT(*) FROM ' . $ecs->table('booking_goods') . ' WHERE is_dispose = 0'));
    // 
    // /* 退款申请 */
    // $smarty->assign('new_repay', $db->getOne('SELECT COUNT(*) FROM ' . $ecs->table('user_account') . ' WHERE process_type = ' . SURPLUS_RETURN . ' AND is_paid = 0 '));

    
    // YOHO:現金訂貨
    // $cash_preorder['today_amount_order_list_url'] = 'order.php?act=list&pay_id=3&composite_status=' . CS_AWAIT_SHIP . '&pay_start_time=' . local_mktime(0, 0, 0, local_date('m'), local_date('d'), local_date('Y')) . '&pay_end_time=' . local_mktime(23, 59, 59, local_date('m'), local_date('d'), local_date('Y')) . '&have_preorder=1';
    // $cash_preorder['today_amount'] = price_format($db->GetOne("SELECT sum(tmp.money_paid) as total FROM " . $GLOBALS['ecs']->table('order_info') . " AS o
    // left join (SELECT o.`money_paid`, o.order_id  FROM " . $GLOBALS['ecs']->table('order_info') . " AS o " ." LEFT JOIN " . $GLOBALS['ecs']->table('order_goods') . " AS og on og.order_id = o.order_id ".
    //     "WHERE o.pay_id = 3 AND (o.postscript LIKE '%訂貨%天%' OR o.order_remarks LIKE '%訂貨%天%' OR o.to_buyer LIKE '%訂貨%天%' OR og.planned_delivery_date > 0) " . order_query_sql('await_ship', 'o.') .
    //     " AND pay_time BETWEEN " . local_mktime(0, 0, 0, local_date('m'), local_date('d'), local_date('Y')) . " AND " . local_mktime(23, 59, 59, local_date('m'), local_date('d'), local_date('Y'))." GROUP BY o.order_id order by o.order_sn DESC) as tmp on tmp.order_id = o.order_id"), false);
    // $cash_preorder['total_amount_order_list_url'] = 'order.php?act=list&pay_id=3&composite_status=' . CS_AWAIT_SHIP . '&have_preorder=1';
    // $cash_preorder['total_amount'] = price_format($db->GetOne("SELECT sum(tmp.money_paid) as total FROM " . $GLOBALS['ecs']->table('order_info') . " AS o
    // left join (SELECT o.`money_paid`, o.order_id  FROM " . $GLOBALS['ecs']->table('order_info') . " AS o " ." LEFT JOIN " . $GLOBALS['ecs']->table('order_goods') . " AS og on og.order_id = o.order_id ".
    //     "WHERE o.pay_id = 3 AND (o.postscript LIKE '%訂貨%天%' OR o.order_remarks LIKE '%訂貨%天%' OR o.to_buyer LIKE '%訂貨%天%' OR og.planned_delivery_date > 0) " . order_query_sql('await_ship', 'o.')." GROUP BY o.order_id order by o.order_sn DESC) as tmp on tmp.order_id = o.order_id"), false);
    // $smarty->assign('cash_preorder', $cash_preorder);

    assign_query_info();
    $smarty->assign('ecs_version',  VERSION);
    $smarty->assign('ecs_release',  RELEASE);
    $smarty->assign('ecs_lang',     $_CFG['lang']);
    $smarty->assign('ecs_charset',  strtoupper(EC_CHARSET));
    $smarty->assign('install_date', local_date($_CFG['date_format'], $_CFG['install_date']));
    $smarty->display('index.htm');
}

elseif($_REQUEST['act'] == 'ajax_index_chart') {  
    // YOHO: 當日門店帳目
    $today_paid_order    = $orderController->get_today_order_stats('paid');
    $today_shipped_order = $orderController->get_today_order_stats('shipped');

    // TODO: 未諗到點做...
    // // howang: 部份齊貨
    // $order['partially_in_stock'] = intval($db->GetOne("SELECT COUNT(*) FROM " . $GLOBALS['ecs']->table('order_info') . " AS o " .
    //     "WHERE 1 " . order_query_sql('await_ship', 'o.') .
    //     "AND (SELECT IF(min(stock) >= 0, 'Y', 'N') FROM (SELECT og.order_id, (SUM(ega.goods_qty) - og.goods_number + og.send_number) as stock FROM " . $GLOBALS['ecs']->table('erp_goods_attr') . " as ega LEFT JOIN " .$GLOBALS['ecs']->table('order_goods'). " as og ON ega.goods_id = og.goods_id GROUP BY og.order_id, og.goods_id) as gs WHERE gs.order_id = o.order_id) = 'N'"));
    /* 已完成的订单 */
    // $order['finished']     = $db->GetOne('SELECT COUNT(*) FROM ' . $ecs->table('order_info').
    // " WHERE 1 " . order_query_sql('finished')." AND (order_type<>'".Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE."' OR (order_type IS NULL AND user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).") ))");
    /* 待发货的订单： */
    // $order['await_ship']   = $db->GetOne('SELECT COUNT(*)'.
    // ' FROM ' .$ecs->table('order_info') .
    // " WHERE 1 " . order_query_sql('await_ship'));
    // 

    // new await_ship
    $sellableWarehouseIds = $erpController->getSellableWarehouseIds(true); 

    $sql = "SELECT o.order_id,o.pay_time " .
           "FROM " . $GLOBALS['ecs']->table('order_info') . " AS o " .
           "where 1 AND (o.order_type <> '".Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE."' OR o.order_type IS NULL) AND o.user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).")" .
           order_query_sql('await_ship', 'o.')  ;

    $order_rows = $db->getAll($sql);
    $order['await_ship'] = sizeof($order_rows);

    // new ready ship
    $order_ids = array_map(function ($order) { return $order['order_id']; }, $order_rows);
    $order_goods_ids = [];
    // get order goods ids
    $sql = "SELECT distinct og.goods_id " .
           "FROM " . $GLOBALS['ecs']->table('order_goods') . " as og " .
           "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON og.goods_id = g.goods_id " .
           "WHERE order_id " . db_create_in($order_ids);
    $order_goods_ids = $GLOBALS['db']->getCol($sql);

    // for globals goods qty
    $sql = "SELECT ega.goods_id, IFNULL(SUM(ega.goods_qty),0) as storage " .
           "FROM " . $GLOBALS['ecs']->table('erp_goods_attr_stock') . " AS ega " .
           "WHERE ega.goods_id " . db_create_in($order_goods_ids) ." AND ega.warehouse_id IN (".$sellableWarehouseIds.") ".
           "group by goods_id";

    $order_goods = $GLOBALS['db']->getAll($sql);

    $goods_qty = [];
    foreach ($order_goods as $order_goods_info){
       $goods_qty[$order_goods_info['goods_id']] = $order_goods_info['storage'];
    }

    foreach ($order_goods_ids as $order_goods_id) {
       if (!isset($goods_qty[$order_goods_id])) {
           $goods_qty[$order_goods_id] = 0;
       } 
    }

    // order preordered
    $order_preordered = [];
    $sql = "SELECT oa.order_id, IFNULL(max(1),0) as preordered " .
           "FROM " . $GLOBALS['ecs']->table('order_action') . " AS oa " .
           "WHERE oa.order_id " . db_create_in($order_ids) ." AND oa.action_note LIKE '%已向供應商訂貨%' ".
           "group by order_id";
    $order_preorder = $GLOBALS['db']->getAll($sql);
    foreach ($order_preorder as $order_preorder_info){
       $order_preordered[$order_preorder_info['order_id']] = 1;
    }

    $sql = "SELECT og.*, g.goods_thumb " .
        "FROM " . $GLOBALS['ecs']->table('order_goods') . " as og " .
        "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON og.goods_id = g.goods_id " .
        "WHERE order_id " . db_create_in($order_ids);

    $order_goods = $GLOBALS['db']->getAll($sql);

    $order_goods_by_order_id = [];
    foreach ($order_goods as $order_goods_key =>  $order_goods_info){
        // add storage to order_goods ar
        $order_goods[$order_goods_key]['storage'] = $goods_qty[$order_goods_info['goods_id']];
        $order_goods_by_order_id[$order_goods_info['order_id']][] = $order_goods[$order_goods_key];
    }

    $row_temp = $order_rows;
    // sort by paid time
    usort($row_temp, function($a, $b) {
        return $a['pay_time'] - $b['pay_time'];
    });
    //echo '<pre>';
    //print_r($row_temp);

    $orders_can_ship = [];
    $orders_cant_ship = [];

    foreach ($row_temp AS $key => $value) {
        // filter the order can't be shipped
        if (isset($order_goods_by_order_id[$value['order_id']])) {
            $all_order_goods_in_stock = true;
            foreach ($order_goods_by_order_id[$value['order_id']] as $ogs)
            {
                if ($goods_qty[$ogs['goods_id']] - $ogs['goods_number'] + $ogs['send_number'] >= 0) {
                    $goods_qty[$ogs['goods_id']] = $goods_qty[$ogs['goods_id']] -$ogs['goods_number'] + $ogs['send_number'];
                } else {
                    $goods_qty[$ogs['goods_id']] = 0;
                    $all_order_goods_in_stock = false;
                }
            }

            if ($all_order_goods_in_stock == true) {
                $orders_can_ship[] = $value['order_id']; 
            } else {
                $orders_cant_ship[] = $value['order_id']; 
            }
        }
    }

    $order['ready_ship'] = sizeof($orders_can_ship);

    $orders_need_preorder = [];
    foreach ($orders_cant_ship as $orders_cant_ship_id) {
        if (!isset($order_preordered[$orders_cant_ship_id])) {
            $orders_need_preorder[] = $orders_cant_ship_id;  
        }
    }

    $order['need_purchase'] = sizeof($orders_need_preorder);

// $sql = "SELECT eo.client_order_sn, o.order_id, o.order_sn, o.add_time, o.order_status, o.shipping_id, o.shipping_status, o.order_amount, o.money_paid, o.cp_price, " .
//             "o.pay_status, o.order_type, o.type_sn, o.type_id, o.actg_paid_amount,o.pay_id, o.extension_code, o.extension_id, o.postscript, o.serial_numbers,  " .
//             "o.consignee, o.address, o.country, o.email, o.tel, o.pay_time, o.shipping_fod, o.language, p.pay_name, " .
//             "(" . order_amount_field('o.') . ") AS total_fee, o.to_buyer, " .
//             "(o.money_paid + o.order_amount) AS order_total, " . // howang: money_paid(已付金額) + order_amount(應付金額) = 訂單總金額
//             " o.user_id, o.salesperson, o.platform, ".
//             "o.fully_paid, o.actg_paid_amount, ". // Accounting System for wholesale orders
//            // "IFNULL((SELECT count(*) FROM ".$GLOBALS['ecs']->table('order_action')." as oa WHERE oa.order_id = o.order_id), 0) as action_count, " . // Action count
//            // "IFNULL((SELECT max(1) FROM ".$GLOBALS['ecs']->table('order_action')." as oa WHERE oa.order_id = o.order_id AND oa.action_note LIKE '%已向供應商訂貨%'), 0) as ordered_from_supplier " .
//         " FROM " . $GLOBALS['ecs']->table('order_info') . " AS o " .
//         " LEFT JOIN " .$GLOBALS['ecs']->table('erp_order'). " AS eo ON eo.client_order_sn=o.order_sn ".
//         ($_REQUEST['need_purchase'] ? "HAVING ordered_from_supplier = 0 " : '') .
//         ($_REQUEST['display_order_not_pickup'] == 1 ? "Group by o.order_id " : '');


    // new ready ship 

    // howang: 待發貨 && 可出貨
    // $order['ready_ship'] = intval($db->GetOne(
    //     "SELECT SUM(IF(min_stock >= 0, 1, 0)) as ready_ship FROM (" .
    //         "SELECT order_id, min(stock) as min_stock FROM (" .
    //             "SELECT o.order_id, og.goods_id, (SUM(ega.goods_qty) - og.goods_number + og.send_number) as stock " .
    //             "FROM " . $GLOBALS['ecs']->table('order_info') . " as o " .
    //             "LEFT JOIN " . $GLOBALS['ecs']->table('order_goods') . " as og ON og.order_id = o.order_id " .
    //             "LEFT JOIN " . $GLOBALS['ecs']->table('erp_goods_attr_stock') . " as ega ON ega.goods_id = og.goods_id AND ega.warehouse_id IN (".$erpController->getSellableWarehouseIds(true).") " .
    //             "WHERE 1 " . order_query_sql('await_ship', 'o.')." AND (order_type<>'".Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE."' OR (order_type IS NULL AND user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).") ))".
    //             "GROUP BY o.order_id, og.goods_id" .
    //         ") as gs " .
    //         "GROUP BY order_id " .
    //     ") as oms "));
    
    // // howang: 待發貨 && 未齊貨 && 未訂貨
    // $order['need_purchase'] = intval($db->GetOne("SELECT COUNT(*) FROM " . $GLOBALS['ecs']->table('order_info') . " AS o " .
    //     "WHERE 1 " . order_query_sql('await_ship', 'o.') .
    //     "AND (SELECT IF(min(stock) >= 0, 'Y', 'N') FROM (SELECT og.order_id, (SUM(ega.goods_qty) - og.goods_number + og.send_number) as stock FROM " . $GLOBALS['ecs']->table('erp_goods_attr_stock') . " as ega LEFT JOIN " .$GLOBALS['ecs']->table('order_goods'). " as og ON ega.goods_id = og.goods_id WHERE ega.warehouse_id IN (".$erpController->getSellableWarehouseIds(true).") GROUP BY og.order_id, og.goods_id) as gs WHERE gs.order_id = o.order_id) = 'N'" .
    //     "AND IFNULL((SELECT max(1) FROM ".$GLOBALS['ecs']->table('order_action')." as oa WHERE oa.order_id = o.order_id AND oa.action_note LIKE '%已向供應商訂貨%'), 0) = 0"));
    // 
    // $order['stock_incorrect'] = intval($db->GetOne("SELECT COUNT(*) FROM " . $ecs->table('order_info') . " as o " .
    //     "WHERE o.platform != 'pos' " . order_query_sql('finished', 'o.') .
    //     "AND EXISTS (" .
    //         "SELECT og.`order_id` " .
    //         "FROM " . $ecs->table('order_goods') . " as og " .
    //         "WHERE og.order_id = o.order_id AND og.replaced_qty = 0 " .
    //         "GROUP BY og.rec_id " .
    //         "HAVING SUM(cast(og.goods_number as signed) - cast(og.delivery_qty as signed)) > 0" .
    //     ") AND o.order_id NOT IN (" .
    //         "SELECT DISTINCT replace_order_id FROM " . $GLOBALS['ecs']->table('order_info') .
    //         " WHERE replace_order_id != 0" .
    //     ")"));
    // $order['erp_delivery_pending_approve'] = intval($db->GetOne('SELECT COUNT(*) FROM ' . $ecs->table('erp_delivery') . " AS ed " .
    //     "WHERE ed.status = 2"));

    /* 待付款的订单： */
    // $order['await_pay']    = $db->GetOne('SELECT COUNT(*)'.
    // ' FROM ' .$ecs->table('order_info') .
    // " WHERE 1 " . order_query_sql('await_pay')." AND (order_type<>'".Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE."' OR (order_type IS NULL AND user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).") ))");
    

    /* “未确认”的订单 */
    // $order['unconfirmed']  = $db->GetOne('SELECT COUNT(*) FROM ' .$ecs->table('order_info').
    // " WHERE 1 " . order_query_sql('unconfirmed'));
    

//    $today_start = mktime(0,0,0,date('m'),date('d'),date('Y'));
    // $order['stats']        = $db->getRow('SELECT COUNT(*) AS oCount, IFNULL(SUM(order_amount), 0) AS oAmount' .
    // ' FROM ' .$ecs->table('order_info'));

    // $smarty->assign('order', $order);
    // Exclude orders for the followiung user accounts: 11111111, 88888888, 99999999
    $where_exclude = " AND (oi.order_type<>'".Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE."' OR (oi.order_type IS NULL AND oi.user_id NOT IN (".implode(',',$userController->getWholesaleUserIds())."))) ";
    // Exclude sales agent
    $where_exclude .= " AND (oi.order_type <> '" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT . "' OR oi.order_type IS NULL) ";
    // Exclude 展場POS
    $where_exclude .= " AND (oi.platform <> 'pos_exhibition') ";

    $start_time = local_mktime(0, 0, 0, local_date('m'), local_date('d'), local_date('Y'));
    $end_time = local_mktime(23, 59, 59, local_date('m'), local_date('d'), local_date('Y'));
    $where_status = " AND oi.order_status NOT IN (" . OS_CANCELED . ',' . OS_INVALID . ',' . OS_RETURNED . ") ".
                        " AND oi.pay_status = " . PS_PAYED . " ";
    $where_today = " AND oi.pay_time BETWEEN " . $start_time . " AND " . $end_time;
    $sql = "SELECT og.goods_id, og.goods_name, SUM(og.`goods_number`) as total_goods_number ".
    "FROM " . $ecs->table('order_info') ." as oi ".
    "LEFT JOIN " . $ecs->table('order_goods') ." as og ON og.order_id = oi.order_id ".
    " WHERE 1 " . $where_exclude . $where_status . $where_today .
    " GROUP BY og.goods_id ORDER BY SUM(og.`goods_number`) DESC, og.goods_id DESC  LIMIT 5";
    $res = $db->GetAll($sql);

    // 未連 Anyspecs & 未入條碼產品數
    $sql = "SELECT COUNT(*) FROM " . $ecs->table('goods') . " as g WHERE g.proddb_id = 0";
    $goods['proddb_not_linked'] = $db->GetOne($sql);
    $sql = "SELECT COUNT(*) FROM " . $ecs->table('goods') . " as g WHERE (SELECT sum(ega.goods_qty) FROM " . $GLOBALS['ecs']->table('erp_goods_attr_stock') . " as ega WHERE ega.goods_id = g.goods_id AND ega.warehouse_id IN (".$erpController->getSellableWarehouseIds(true).")) > 0 AND (g.goods_sn = '' OR g.goods_sn = g.goods_id)";
    $goods['no_barcode'] = $db->GetOne($sql);

    // haven't set LWH
    $sql = $sql = "SELECT COUNT(*) FROM " . $ecs->table('goods') . " as g WHERE  (SELECT sum(ega.goods_qty) FROM " . $GLOBALS['ecs']->table('erp_goods_attr_stock') . " as ega WHERE ega.goods_id = g.goods_id AND ega.warehouse_id IN (".$erpController->getSellableWarehouseIds(true).")) > 0 and (goods_height <= 0 or goods_width <= 0 or goods_length <= 0) and g.is_on_sale = 1 and g.is_delete = 0 ";
    $goods['no_lwh'] = $db->GetOne($sql);

    // $sql = $sql = "SELECT COUNT(*) FROM " . $ecs->table('goods') . " as g WHERE  goods_weight <= 0 and g.is_on_sale = 1 and g.is_delete = 0 ";
    // $goods['no_weight'] = $db->GetOne($sql);

    make_json_result('', '', array('today_paid_order' => $today_paid_order, 'today_shipped_order' => $today_shipped_order, 'order' => $order, 'top_goods' => $res, 'goods_data' => $goods));
}
elseif($_REQUEST['act'] == 'ajax_goods_chart') {

    /* 商品信息 */
    /* 缺货商品 */
    // if ($_CFG['use_storage'])
    // {
    //     // howang: fix 庫存警告
    //     //$sql = 'SELECT COUNT(*) FROM ' .$ecs->table('goods'). ' WHERE is_delete = 0 AND goods_number <= warn_number AND is_real = 1';
    //     $sql = 'SELECT COUNT(*) FROM ' .$ecs->table('goods'). ' as g WHERE is_delete = 0 AND g.is_on_sale = 1 AND is_real = 1 AND (SELECT sum(ega.goods_qty) FROM ' . $GLOBALS['ecs']->table('erp_goods_attr_stock') . ' as ega WHERE ega.goods_id = g.goods_id AND ega.warehouse_id IN ('.$erpController->getSellableWarehouseIds(true).')) <= warn_number';
    //     $goods['warn'] = $db->GetOne($sql);
    //     // $sql = 'SELECT COUNT(*) FROM ' .$ecs->table('goods'). ' WHERE is_delete = 0 AND is_on_sale = 1 AND goods_number <= warn_number AND is_real=0 AND extension_code=\'virtual_card\'';
    //     // $virtual_card['warn'] = $db->GetOne($sql);
    // }
    // else
    // {
    //     $goods['warn'] = 0;
    //     $virtual_card['warn'] = 0;
    // }
    
    // // 訂貨錯誤警告產品數, "It is 訂貨錯誤 when 存貨 and 訂貨 are both positive figure.", Franz said.
    // $sql = 'SELECT COUNT(*) FROM ' .$ecs->table('goods'). ' as g WHERE g.is_delete = 0 AND (SELECT sum(ega.goods_qty) FROM ' . $GLOBALS['ecs']->table('erp_goods_attr_stock') . ' as ega WHERE ega.goods_id = g.goods_id AND ega.warehouse_id IN ('.$erpController->getSellableWarehouseIds(true).')) > 0 AND g.pre_sale_days > 0 AND g.is_real = 1';
    // $goods['warn_preorder'] = $db->GetOne($sql);
    
    // // 產品無詳情警告產品數 & 產品無簡介警告產品數
    // $sql = 'SELECT COUNT(*) FROM ' .$ecs->table('goods'). ' as g WHERE g.is_delete = 0 AND g.is_real = 1 AND trim(goods_desc) = \'\'';
    // $goods['num_of_no_desc'] = $db->GetOne($sql);
    // $sql = 'SELECT COUNT(*) FROM ' .$ecs->table('goods'). ' as g WHERE g.is_delete = 0 AND g.is_real = 1 AND trim(goods_brief) = \'\'';
    // $goods['num_of_no_brief'] = $db->GetOne($sql);
    
    // // 未處理產品問題回報 & 未處理產品售價提議
    // $sql = "SELECT COUNT(*) FROM " .$ecs->table('goods_feedback'). " as gf WHERE gf.feedback_type = 'problem' AND gf.status = 'pending'";
    // $goods['feedback_problem'] = $db->GetOne($sql);
    // $sql = "SELECT COUNT(*) FROM " .$ecs->table('goods_feedback'). " as gf WHERE gf.feedback_type = 'price' AND gf.status = 'pending'";
    // $goods['feedback_price'] = $db->GetOne($sql);

}

/*------------------------------------------------------ */
//-- 清除缓存
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'clear_cache')
{
    clear_all_files('',1);

    sys_msg($_LANG['caches_cleared']);
}

/*------------------------------------------------------ */
//-- 检查订单
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'check_order')
{
    if (empty($_SESSION['last_check']))
    {
        $_SESSION['last_check'] = gmtime();

        make_json_result('', '', array('new_orders' => 0, 'new_paid' => 0));
    }

    /* 新订单 */
    $sql = 'SELECT COUNT(*) FROM ' . $ecs->table('order_info').
    " WHERE add_time >= '$_SESSION[last_check]'";
    $arr['new_orders'] = $db->getOne($sql);

    /* 新付款的订单 */
    $sql = 'SELECT COUNT(*) FROM '.$ecs->table('order_info').
    ' WHERE pay_time >= ' . $_SESSION['last_check'];
    $arr['new_paid'] = $db->getOne($sql);

    $_SESSION['last_check'] = gmtime();

    if (!(is_numeric($arr['new_orders']) && is_numeric($arr['new_paid'])))
    {
        make_json_error($db->error());
    }
    else
    {
        make_json_result('', '', $arr);
    }
}

/*------------------------------------------------------ */
//-- Totolist操作
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'save_todolist')
{
    $content = json_str_iconv($_POST["content"]);
    $sql = "UPDATE" .$GLOBALS['ecs']->table('admin_user'). " SET todolist='" . $content . "' WHERE user_id = " . $_SESSION['admin_id'];
    $GLOBALS['db']->query($sql);
}

elseif ($_REQUEST['act'] == 'get_todolist')
{
    $sql     = "SELECT todolist FROM " .$GLOBALS['ecs']->table('admin_user'). " WHERE user_id = " . $_SESSION['admin_id'];
    $content = $GLOBALS['db']->getOne($sql);
    echo $content;
}
// 邮件群发处理
elseif ($_REQUEST['act'] == 'send_mail')
{
    if ($_CFG['send_mail_on'] == 'off')
    {
        make_json_result('', $_LANG['send_mail_off'], 0);
        exit();
    }
    $sql = "SELECT * FROM " . $ecs->table('email_sendlist') . " ORDER BY pri DESC, last_send ASC LIMIT 1";
    $row = $db->getRow($sql);

    //发送列表为空
    if (empty($row['id']))
    {
        make_json_result('', $_LANG['mailsend_null'], 0);
    }

    //发送列表不为空，邮件地址为空
    if (!empty($row['id']) && empty($row['email']))
    {
        $sql = "DELETE FROM " . $ecs->table('email_sendlist') . " WHERE id = '$row[id]'";
        $db->query($sql);
        $count = $db->getOne("SELECT COUNT(*) FROM " . $ecs->table('email_sendlist'));
        make_json_result('', $_LANG['mailsend_skip'], array('count' => $count, 'goon' => 1));
    }

    //查询相关模板
    $sql = "SELECT * FROM " . $ecs->table('mail_templates') . " WHERE template_id = '$row[template_id]'";
    $rt = $db->getRow($sql);

    //如果是模板，则将已存入email_sendlist的内容作为邮件内容
    //否则即是杂质，将mail_templates调出的内容作为邮件内容
    if ($rt['type'] == 'template')
    {
        $rt['template_content'] = $row['email_content'];
    }

    if ($rt['template_id'] && $rt['template_content'])
    {
        if (send_mail('', $row['email'], $rt['template_subject'], $rt['template_content'], $rt['is_html']))
        {
            //发送成功

            //从列表中删除
            $sql = "DELETE FROM " . $ecs->table('email_sendlist') . " WHERE id = '$row[id]'";
            $db->query($sql);

            //剩余列表数
            $count = $db->getOne("SELECT COUNT(*) FROM " . $ecs->table('email_sendlist'));

            if($count > 0)
            {
                $msg = sprintf($_LANG['mailsend_ok'],$row['email'],$count);
            }
            else
            {
                $msg = sprintf($_LANG['mailsend_finished'],$row['email']);
            }
            make_json_result('', $msg, array('count' => $count));
        }
        else
        {
            //发送出错

            if ($row['error'] < 3)
            {
                $time = time();
                $sql = "UPDATE " . $ecs->table('email_sendlist') . " SET error = error + 1, pri = 0, last_send = '$time' WHERE id = '$row[id]'";
            }
            else
            {
                //将出错超次的纪录删除
                $sql = "DELETE FROM " . $ecs->table('email_sendlist') . " WHERE id = '$row[id]'";
            }
            $db->query($sql);

            $count = $db->getOne("SELECT COUNT(*) FROM " . $ecs->table('email_sendlist'));
            make_json_result('', sprintf($_LANG['mailsend_fail'],$row['email']), array('count' => $count));
        }
    }
    else
    {
        //无效的邮件队列
        $sql = "DELETE FROM " . $ecs->table('email_sendlist') . " WHERE id = '$row[id]'";
        $db->query($sql);
        $count = $db->getOne("SELECT COUNT(*) FROM " . $ecs->table('email_sendlist'));
        make_json_result('', sprintf($_LANG['mailsend_fail'],$row['email']), array('count' => $count));
    }
}
?>
