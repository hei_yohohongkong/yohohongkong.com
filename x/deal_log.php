<?php

/***
* deal_log.php
* by howang 2014-07-03
*
* List of warranty registration from deal page
***/

define('IN_ECS', true);
require(dirname(__FILE__) . '/includes/init.php');


if ($_REQUEST['act'] == 'list')
{
    /* 检查权限 */
    admin_priv('deal_log');
    
    $list = get_deal_log();
    $smarty->assign('ur_here',      $_LANG['deal_log']);
    $smarty->assign('full_page',    1);

    $smarty->assign('list',         $list['data']);
    $smarty->assign('filter',       $list['filter']);
    $smarty->assign('record_count', $list['record_count']);
    $smarty->assign('page_count',   $list['page_count']);
    
    $smarty->assign('action_link', array('text' => '保養產品型號', 'href' => 'deal_model.php?act=list'));

    $sort_flag  = sort_flag($list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);

    assign_query_info();
    $smarty->display('deal_log.htm');
}
elseif ($_REQUEST['act'] == 'query')
{
    /* 检查权限 */
    check_authz_json('deal_log');
    
    $list = get_deal_log();
    $smarty->assign('list',         $list['data']);
    $smarty->assign('filter',       $list['filter']);
    $smarty->assign('record_count', $list['record_count']);
    $smarty->assign('page_count',   $list['page_count']);

    $sort_flag  = sort_flag($list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);

    make_json_result($smarty->fetch('deal_log.htm'), '', array('filter' => $list['filter'], 'page_count' => $list['page_count']));
}


function get_deal_log()
{
    $filter['sort_by']      = empty($_REQUEST['sort_by']) ? 'reg_id' : trim($_REQUEST['sort_by']);
    $filter['sort_order']   = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);

    $sql = "SELECT count(*) FROM " . $GLOBALS['ecs']->table('warranty_registration');
    $filter['record_count'] = $GLOBALS['db']->getOne($sql);

    /* 分页大小 */
    $filter = page_and_size($filter);

    /* 查询 */
    $sql = "SELECT * ";
    $sql.= " FROM " . $GLOBALS['ecs']->table('warranty_registration');
    $sql.= " ORDER by " . $filter['sort_by'] . ' ' . $filter['sort_order'];
    $sql.= " LIMIT " . $filter['start'] . "," . $filter['page_size'];
    $data = $GLOBALS['db']->getAll($sql);
    
    foreach ($data as $key => $row)
    {
        $data[$key]['reg_date'] = local_date($GLOBALS['_CFG']['time_format'], $row['reg_time']);
    }

    $arr = array('data' => $data, 'filter' => $filter, 'page_count' => $filter['page_count'], 'record_count' => $filter['record_count']);

    return $arr;
}
?>