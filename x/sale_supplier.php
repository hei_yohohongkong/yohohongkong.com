<?php

/***
* sale_supplier.php
* by howang 2015-10-19
*
* Sales stats by supplier
***/


define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require_once(ROOT_PATH . 'includes/lib_order.php');
require_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_goods.php');
require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/admin/statistic.php');

$smarty->assign('lang', $_LANG);
$userController = new Yoho\cms\Controller\UserController();
$erpController = new Yoho\cms\Controller\ErpController();
$supplierController = new Yoho\cms\Controller\SupplierController();
$_REQUEST['order_type'] = isset($_REQUEST['order_type']) ? $_REQUEST['order_type'] : 4;
$_REQUEST['deprecatedflow'] = isset($_REQUEST['deprecatedflow']) ? $_REQUEST['deprecatedflow'] : 'N';
if (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'query' ||  $_REQUEST['act'] == 'download'))
{
	/* 检查权限 */
	check_authz_json('sale_order_stats');
	
	if (strstr($_REQUEST['start_date'], '-') === false)
	{
		$_REQUEST['start_date'] = local_date('Y-m-d', $_REQUEST['start_date']);
		$_REQUEST['end_date'] = local_date('Y-m-d', $_REQUEST['end_date']);
	}
	
	/*------------------------------------------------------ */
	//--Excel文件下载
	/*------------------------------------------------------ */
	if ($_REQUEST['act'] == 'download')
	{
		header("Content-type: application/vnd.ms-excel; charset=utf-8");
		
		require_once ROOT_PATH . 'includes/excel/PHPExcel.php';
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		
		$objPHPExcel->getActiveSheet()->setTitle('供應商報告');
		$column_list = array();
		foreach(range('A', 'Z') as $letter)
		{
			$column_list[] = $letter;
		}
		foreach(range('A', 'Z') as $letter)
		{
			$column_list[] = 'A'.$letter;
		}
		foreach(range('A', 'Z') as $letter)
		{
			$column_list[] = 'B'.$letter;
		}
		
		if (!empty($_REQUEST['period']))
		{
			$period = (int)$_REQUEST['period'];
			$file_name = local_date('Y-m', strtotime('-1 months', strtotime(month_start()))) . '_to_' . local_date('Y-m', strtotime('-' . $period . ' months', strtotime(month_start()))) . '_sale_supplier';
			
			if($_REQUEST['order_type'] == 4){
				$value_type_list = array(
					array('name'=>'平均存貨成本', 'value_key'=>'avg_stock_cost'),
					array('name'=>'Turnover', 'value_key'=>'turnover'),
					array('name'=>'實際銷售總額(扣除手續費)', 'value_key'=>'total_sa_revenue'),
					array('name'=>'實際盈利總額(扣除手續費)', 'value_key'=>'total_sa_profit'),
					array('name'=>'實際盈利率總額(扣除手續費)', 'value_key'=>'sa_profit_margin_formated')
				);
			}else if($_REQUEST['order_type'] == 3){
				$value_type_list = array(
					array('name'=>'銷售總額','value_key'=>'total_revenue'),
					array('name'=>'實際銷售總額(扣除手續費)', 'value_key'=>'total_sa_revenue'),
					array('name'=>'盈利總額','value_key'=>'total_profit'),
					array('name'=>'實際盈利總額(扣除手續費)', 'value_key'=>'total_sa_profit'),
					array('name'=>'盈利率', 'value_key'=>'profit_margin_formated'),
					array('name'=>'實際盈利率總額(扣除手續費)', 'value_key'=>'sa_profit_margin_formated')
				);
			} else {
				$value_type_list = array(
					array('name'=>'銷售總額','value_key'=>'total_revenue'),
					array('name'=>'盈利總額','value_key'=>'total_profit'),
					array('name'=>'盈利率', 'value_key'=>'profit_margin_formated'),
				);
			}
			$vi_num = count($value_type_list);
			for ($vi = 0; $vi < $vi_num; $vi++)
			{
				if ($vi > 0)
				{
					$objWorkSheet = $objPHPExcel->createSheet();
				}
				$objPHPExcel->setActiveSheetIndex($vi);
				$objPHPExcel->getActiveSheet()->setTitle($value_type_list[$vi]['name']);
				
				for ($pi = 0; $pi < $period; $pi++)
				{
					/* 文件标题 */
					$objPHPExcel->getActiveSheet()
					->setCellValue($column_list[$pi+1].'1', ecs_iconv(EC_CHARSET, 'UTF8', local_date('M Y', strtotime('-'.($pi+1).' months', strtotime(month_start()))) ));
				}
				$style = array(
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					)
				);
				$objPHPExcel->getActiveSheet()
				->setCellValue($column_list[0].'2', ecs_iconv(EC_CHARSET, 'UTF8', '供應商'))
				->setCellValue($column_list[1].'2', ecs_iconv(EC_CHARSET, 'UTF8', $value_type_list[$vi]['name']))
				->mergeCells($column_list[1].'2:'.$column_list[$period+1].'2')
				->getStyle($column_list[1].'2:'.$column_list[$period+1].'2')->applyFromArray($style);
				
				$_REQUEST['start_date'] = local_date('Y-m-01', strtotime('-'.($pi+1).' months', strtotime(month_start())));
				$_REQUEST['end_date'] = local_date('Y-m-t', strtotime('-'.($pi+1).' months', strtotime(month_start())));
				
				$data = $supplierController->get_sale_supplier(false);
				$i = 3;
				foreach ($data['data'] as $value)
				{
					$objPHPExcel->getActiveSheet()
					->setCellValue($column_list[0].$i, ecs_iconv(EC_CHARSET, 'UTF8', ($i == 3 ? '' : '　') . $value['name']));
					$i++;
					if (isset($value['children']))
					foreach ($value['children'] as $child)
					{
						$objPHPExcel->getActiveSheet()
						->setCellValue($column_list[0].$i, ecs_iconv(EC_CHARSET, 'UTF8', '　　' . $child['name']));
						$i++;
						if (isset($child['children']))
						foreach ($child['children'] as $grandson)
						{
							$objPHPExcel->getActiveSheet()
							->setCellValue($column_list[0].$i, ecs_iconv(EC_CHARSET, 'UTF8', '　　　' . $grandson['name']));
							$i++;
						}
					}
				}
				
				for ($pi = 0; $pi < $period; $pi++)
				{
					$_REQUEST['start_date'] = local_date('Y-m-01', strtotime('-'.($pi+1).' months', strtotime(month_start())));
					$_REQUEST['end_date'] = local_date('Y-m-t', strtotime('-'.($pi+1).' months', strtotime(month_start())));
					
					$data = $supplierController->get_sale_supplier(false);
					$i = 3;
					foreach ($data['data'] as $value)
					{
						$objPHPExcel->getActiveSheet()
						->setCellValue($column_list[1+$pi].$i, ecs_iconv(EC_CHARSET, 'UTF8', $value[$value_type_list[$vi]['value_key']]));
						if($value_type_list[$vi]['value_key'] != "total_fifo_qty" && $value_type_list[$vi]['value_key'] != "turnover")
							$objPHPExcel->getActiveSheet()->getStyle($column_list[1+$pi].$i)->getNumberFormat()->setFormatCode('$0.00');
						$i++;
						if (isset($value['children']))
						foreach ($value['children'] as $child)
						{
							$objPHPExcel->getActiveSheet()
							->setCellValue($column_list[1+$pi].$i, ecs_iconv(EC_CHARSET, 'UTF8', $child[$value_type_list[$vi]['value_key']]));
							if($value_type_list[$vi]['value_key'] != "total_fifo_qty" && $value_type_list[$vi]['value_key'] != "turnover")
								$objPHPExcel->getActiveSheet()->getStyle($column_list[1+$pi].$i)->getNumberFormat()->setFormatCode('$0.00');
							$i++;
							if (isset($child['children']))
							foreach ($child['children'] as $grandson)
							{
								$objPHPExcel->getActiveSheet()
								->setCellValue($column_list[1+$pi].$i, ecs_iconv(EC_CHARSET, 'UTF8', $grandson[$value_type_list[$vi]['value_key']]));
								if($value_type_list[$vi]['value_key'] != "total_fifo_qty" && $value_type_list[$vi]['value_key'] != "turnover")
									$objPHPExcel->getActiveSheet()->getStyle($column_list[1+$pi].$i)->getNumberFormat()->setFormatCode('$0.00');
								$i++;
							}
						}
					}
				}
				
				for ($s = 0; $s < $period + 1; $s++)
				{
					$objPHPExcel->getActiveSheet()->getColumnDimension($column_list[$s])->setAutoSize(true);
				}
			}
			$objPHPExcel->setActiveSheetIndex(0);
		}
		else
		{
			$file_name = $_REQUEST['start_date'] . '_to_' . $_REQUEST['end_date'] . '_sale_supplier';
			
			$data = $supplierController->get_sale_supplier(false);
			// If is Sales agent type
			if($_REQUEST['order_type'] == 4){
				/* 文件标题 */
				$objPHPExcel->getActiveSheet()
				->setCellValue('A1', ecs_iconv(EC_CHARSET, 'UTF8', $_REQUEST['start_date'] . '至' . $_REQUEST['end_date'] . ' 供應商報告'))
				->setCellValue('A2', ecs_iconv(EC_CHARSET, 'UTF8', '供應商'))
				->setCellValue('B2', ecs_iconv(EC_CHARSET, 'UTF8', '即時存貨總數'))
				->setCellValue('C2', ecs_iconv(EC_CHARSET, 'UTF8', '平均存貨成本'))
				->setCellValue('D2', ecs_iconv(EC_CHARSET, 'UTF8', 'Turnover'))
				->setCellValue('E2', ecs_iconv(EC_CHARSET, 'UTF8', '即時存貨成本(FIFO)'))
				->setCellValue('F2', ecs_iconv(EC_CHARSET, 'UTF8', '平均存貨成本(呆壞貨)'))
				->setCellValue('G2', ecs_iconv(EC_CHARSET, 'UTF8', '銷售總數'))
				->setCellValue('H2', ecs_iconv(EC_CHARSET, 'UTF8', '實際銷售總額(扣除手續費)'))
				->setCellValue('I2', ecs_iconv(EC_CHARSET, 'UTF8', '實際盈利總額(扣除手續費)'))
				->setCellValue('J2', ecs_iconv(EC_CHARSET, 'UTF8', '實際盈利率(扣除手續費)'));
				$objPHPExcel->getActiveSheet()->mergeCells('A1:J1');
				
				$i = 3;
				foreach ($data['data'] as $value)
				{
					$objPHPExcel->getActiveSheet()
					->setCellValue('A'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['name']))
					->setCellValue('B'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['total_fifo_qty']))
					->setCellValue('C'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['avg_stock_cost']))
					->setCellValue('D'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['turnover']))
					->setCellValue('E'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['total_fifo_cost']))
					->setCellValue('F'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['total_dead_cost']))
					->setCellValue('G'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['total_sold_qty']))
					->setCellValue('H'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['total_sa_revenue']))
					->setCellValue('I'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['total_sa_profit']))
					->setCellValue('J'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['sa_profit_margin_formated']));
					$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->getNumberFormat()->setFormatCode('$0.00');
					$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->getNumberFormat()->setFormatCode('$0.00');
					$objPHPExcel->getActiveSheet()->getStyle('F'.$i)->getNumberFormat()->setFormatCode('$0.00');
					$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->getNumberFormat()->setFormatCode('$0.00');
					$objPHPExcel->getActiveSheet()->getStyle('I'.$i)->getNumberFormat()->setFormatCode('$0.00');
					$i++;
				}
			} else if($_REQUEST['order_type'] == 3){
				/* 文件标题 */
				$objPHPExcel->getActiveSheet()
				->setCellValue('A1', ecs_iconv(EC_CHARSET, 'UTF8', $_REQUEST['start_date'] . '至' . $_REQUEST['end_date'] . ' 供應商報告'))
				->setCellValue('A2', ecs_iconv(EC_CHARSET, 'UTF8', '供應商'))
				->setCellValue('B2', ecs_iconv(EC_CHARSET, 'UTF8', '即時存貨總數'))
				->setCellValue('C2', ecs_iconv(EC_CHARSET, 'UTF8', '銷售總數'))
				->setCellValue('D2', ecs_iconv(EC_CHARSET, 'UTF8', '銷售總額'))
				->setCellValue('E2', ecs_iconv(EC_CHARSET, 'UTF8', '實際銷售總額(扣除手續費)'))
				->setCellValue('F2', ecs_iconv(EC_CHARSET, 'UTF8', '盈利總額'))
				->setCellValue('G2', ecs_iconv(EC_CHARSET, 'UTF8', '實際盈利總額(扣除手續費)'))
				->setCellValue('H2', ecs_iconv(EC_CHARSET, 'UTF8', '盈利率'))
				->setCellValue('I2', ecs_iconv(EC_CHARSET, 'UTF8', '實際盈利率(扣除手續費)'));
				$objPHPExcel->getActiveSheet()->mergeCells('A1:I1');
				
				$i = 3;
				foreach ($data['data'] as $value)
				{
					$objPHPExcel->getActiveSheet()
					->setCellValue('A'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['name']))
					->setCellValue('B'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['total_fifo_qty']))
					->setCellValue('C'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['total_sold_qty']))
					->setCellValue('D'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['total_revenue']))
					->setCellValue('E'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['total_sa_revenue']))
					->setCellValue('F'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['total_profit']))
					->setCellValue('G'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['total_sa_profit']))
					->setCellValue('H'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['profit_margin_formated']))
					->setCellValue('I'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['sa_profit_margin_formated']));
					$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->getNumberFormat()->setFormatCode('$0.00');
					$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->getNumberFormat()->setFormatCode('$0.00');
					$objPHPExcel->getActiveSheet()->getStyle('F'.$i)->getNumberFormat()->setFormatCode('$0.00');
					$objPHPExcel->getActiveSheet()->getStyle('G'.$i)->getNumberFormat()->setFormatCode('$0.00');
					$i++;
				}
			} else {
				/* 文件标题 */
				$objPHPExcel->getActiveSheet()
				->setCellValue('A1', ecs_iconv(EC_CHARSET, 'UTF8', $_REQUEST['start_date'] . '至' . $_REQUEST['end_date'] . ' 供應商報告'))
				->setCellValue('A2', ecs_iconv(EC_CHARSET, 'UTF8', '供應商'))
				->setCellValue('B2', ecs_iconv(EC_CHARSET, 'UTF8', '即時存貨總數'))
				->setCellValue('C2', ecs_iconv(EC_CHARSET, 'UTF8', '銷售總數'))
				->setCellValue('D2', ecs_iconv(EC_CHARSET, 'UTF8', '銷售總額'))
				->setCellValue('E2', ecs_iconv(EC_CHARSET, 'UTF8', '盈利總額'))
				->setCellValue('F2', ecs_iconv(EC_CHARSET, 'UTF8', '盈利率'));
				$objPHPExcel->getActiveSheet()->mergeCells('A1:F1');
				
				$i = 3;
				foreach ($data['data'] as $value)
				{
					$objPHPExcel->getActiveSheet()
					->setCellValue('A'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['name']))
					->setCellValue('B'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['total_fifo_qty']))
					->setCellValue('C'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['total_sold_qty']))
					->setCellValue('D'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['total_revenue']))
					->setCellValue('E'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['total_profit']))
					->setCellValue('F'.$i, ecs_iconv(EC_CHARSET, 'UTF8', $value['profit_margin_formated']));
					$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->getNumberFormat()->setFormatCode('$0.00');
					$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->getNumberFormat()->setFormatCode('$0.00');
					$i++;
				}
			}
			
			for ($col = 'A'; $col <= 'J'; $col++)
			{
				$objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
			}
		}
		
		header('Content-type: application/vnd.ms-excel; charset=utf-8');
		header('Content-Disposition: attachment; filename=' . $file_name . '.xls');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		
		exit;
	}
	
	$data = $supplierController->get_sale_supplier();
	$smarty->assign('data',            $data['data']);
	$smarty->assign('total_discount',  $data['total_discount']);
	$smarty->assign('filter',          $data['filter']);
	$smarty->assign('record_count',    $data['record_count']);
	$smarty->assign('page_count',      $data['page_count']);
	$smarty->assign('start_date',      $_REQUEST['start_date']);
	$smarty->assign('end_date',        $_REQUEST['end_date']);
	$smarty->assign('display', $_REQUEST['order_type']);

	make_json_result($smarty->fetch('sale_supplier.htm'), '', array('filter' => $data['filter'], 'page_count' => $data['page_count']));
}elseif($_REQUEST['act']=='processpastorder' && $_REQUEST['order_id']>0){

	$erpController->processPastInventorySequence($_REQUEST['order_id']);
}
else
{
	/* 权限判断 */
	admin_priv('sale_order_stats');
	
	/* 时间参数 */
	if (!isset($_REQUEST['start_date']))
	{
		$start_date = strtotime(month_start());
	}
	if (!isset($_REQUEST['end_date']))
	{
		$end_date = strtotime(month_end());
	}
	
	$data = $supplierController->get_sale_supplier();
	/* 赋值到模板 */
	$smarty->assign('filter',         $data['filter']);
	$smarty->assign('order_type',     4);
	$smarty->assign('record_count',   $data['record_count']);
	$smarty->assign('page_count',     $data['page_count']);
	$smarty->assign('data',           $data['data']);
	$smarty->assign('total_discount', $data['total_discount']);
	$smarty->assign('ur_here',        $_LANG['stock_cost_supplier']);
	$smarty->assign('full_page',      1);
	$smarty->assign('start_date',     local_date('Y-m-d', $start_date));
	$smarty->assign('end_date',       local_date('Y-m-d', $end_date));
	$smarty->assign('cfg_lang',       $_CFG['lang']);
	$smarty->assign('action_link',    array('text' => '下載Excel版本','href'=>'#download'));
	$smarty->assign('action_link2',    array('text' => '下載最近六個月Excel版本','href'=>'#six_months'));
	$smarty->assign('action_link3',    array('text' => '下載最近十二個月Excel版本','href'=>'#twelve_months'));
	$smarty->assign('display', $_REQUEST['order_type']);
	$smarty->assign('deprecated', $_REQUEST['deprecatedflow'] == 'Y');
	/* 显示页面 */
	assign_query_info();
	$smarty->display('sale_supplier.htm');
}

function month_start()
{
	return local_date('Y-m-01');
}

function month_end()
{
	return local_date('Y-m-t');
}


?>
