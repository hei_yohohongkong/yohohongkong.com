<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_base.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_warehouse.php');
require(dirname(__FILE__) .'/includes/ERP/cls/cls_date.php');
$stocktakeController = new Yoho\cms\Controller\StocktakeController();
$adminuserController = new Yoho\cms\Controller\AdminuserController();

if ($_REQUEST['act'] == 'list') {
    $result = $stocktakeController->create();
    $cat_list = cat_list(0, $cat_id);
    $smarty->assign('cat_list',$cat_list);
    $period_options = $stocktakeController->getPeriodOptions();
    $smarty->assign('period_options',$period_options);
    $smarty->assign('stocktake_person_option',$result['stocktake_person_option']);
    $smarty->assign('warehouse_list',$result['warehouse_list']);
    $smarty->assign('brand_list',$result['brand_list']);
    $smarty->assign('ur_here', '新增存貨盤點');
    $smarty->assign('full_page', 1);
    $smarty->assign('cfg_lang', $_CFG['lang']);
    $smarty->assign('lang', $_LANG);
    $smarty->assign('action_link', array('text' => '盤點工作清單', 'href' => 'erp_stocktake.php'));

    assign_query_info();
    $smarty->display('erp_stocktake_create.htm');

} else if ($_REQUEST['act'] == 'post_stocktake_create') {
    $result = $stocktakeController->stocktake_create();
    if($result != false){
        make_json_result($result);
    }else{
     	make_json_error('發生錯誤');
    }
} else if ($_REQUEST['act'] == 'post_search_product_by_sku_check_exist') {
    $result = $stocktakeController->searchProductBySkuCheckExist();
    if(!isset($result['error'])){
        make_json_result($result);
    }else{
     	make_json_error(implode('<br>',$result['error']));
    }
} elseif ($_REQUEST['act'] == 'query') {
    $result = $stocktakeController->searchProductByBrand();
    if ($result != false){
        $stocktakeController->ajaxQueryAction($result['data'],$result['record_count'],false,$result['extra_data']);
    }else{
        $result['data'] = array();
        $result['record_count'] = 0;
        $result['extra_data'] = array();
        $stocktakeController->ajaxQueryAction($result['data'],$result['record_count'],false,$result['extra_data']);
    }
} elseif ($_REQUEST['act'] == 'query_by_sku') {
    $result = $stocktakeController->searchProductBySku();
    $stocktakeController->ajaxQueryAction($result['data'],$result['record_count'],false,$result['extra_data']);
}
?>