<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_order.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_supplier.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');

$erpController = new Yoho\cms\Controller\ErpController();
$smarty->assign('act',  $_REQUEST['act']);
$smarty->assign('manage_cost',  $_SESSION['manage_cost']);
$smarty->assign('is_accountant',  $_SESSION['is_accountant']);

if ($_REQUEST['act'] == 'list') {
    if((admin_priv('erp_order_manage','',false)) &&!(admin_priv('erp_order_rate','',false)) &&!(admin_priv('erp_order_approve','',false))) {
        $smarty->assign('supplier_list',get_admin_supplier(erp_get_admin_id()));
    }
    else {
        if($_SESSION['manage_cost'])
            $smarty->assign('supplier_list',get_admin_supplier(1));
        elseif(check_authz(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ONLYSELFSUPPLIERS))
            $smarty->assign('supplier_list',get_admin_supplier(erp_get_admin_id()));
        else
            $smarty->assign('supplier_list',get_admin_supplier(0));
    }
    $purchaser_list=array();
    $sql="select distinct create_by from ".$GLOBALS['ecs']->table('erp_order')." where 1";
    $res=$GLOBALS['db']->query($sql);
    while($row=mysql_fetch_assoc($res)) {
        $sql="select user_name from ".$GLOBALS['ecs']->table('admin_user')." where user_id='".$row['create_by']."'";
        $purchaser_list[$row['create_by']]=$GLOBALS['db']->getOne($sql);
    }
    $smarty->assign('purchaser_list',$purchaser_list);
    $smarty->assign('po_order_type',Yoho\cms\Controller\ErpController::PO_ORDER_TYPE);

    $smarty->assign('order_sn', trim($_REQUEST['order_sn']));
    $smarty->display('erp_order_revise_list.htm');
} elseif ($_REQUEST['act'] == 'query') {
    $start_date = trim($_REQUEST['start_date']);
    $end_date = trim($_REQUEST['end_date']);
    $order_sn = empty($_REQUEST['order_sn']) ? '' : trim($_REQUEST['order_sn']);
	$supplier_id=isset($_REQUEST['s_id']) &&intval($_REQUEST['s_id']) >0 ?intval($_REQUEST['s_id']) : 0;
	$purchaser_id=isset($_REQUEST['p_id']) &&intval($_REQUEST['p_id']) >0 ?intval($_REQUEST['p_id']) : 0;
    $type_id=isset($_REQUEST['t_id']) ?$_REQUEST['t_id'] : '';

	$start = empty($_REQUEST['start']) ? trim($_REQUEST['start']) : 0;
    $page_size = empty($_REQUEST['page_size']) ? intval($_REQUEST['page_size']) : 50;
    $sort_by = empty($_REQUEST['sort_by']) ? 'revise_id' : trim($_REQUEST['sort_by']);
    $sort_order = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
    
    $where = " WHERE 1";
    if (!empty($start_date)) {
        $where .= " AND create_at >= '$start_date' ";
    }
    if (!empty($end_date)) {
        $where .= " AND create_at <= '$end_date 23:59:59' ";
    }
    if (isset($_REQUEST['status']) && $_REQUEST['status'] != -1) {
        $where .= " AND status = " . intval($_REQUEST['status']);
    }
    if (!empty($order_sn)) {
        $where .= " AND order_sn LIKE '%$order_sn%'";
    }
	if(isset($supplier_id) &&!empty($supplier_id))
	{
		$where .= " AND eo.supplier_id = $supplier_id";
	}
	if(isset($purchaser_id) &&!empty($purchaser_id))
	{
		$where .= " AND eo.create_by = $purchaser_id";
	}
	if($paras['type']!='')
	{
		$where .= " AND eo.type = $type";
	}

    $sql = "SELECT eor.*, eo.order_sn, ca.user_name as create_admin, aa.user_name as approve_admin " .
            "FROM " . $ecs->table('erp_order_revise') . " eor " .
            "LEFT JOIN (SELECT order_id, order_sn, create_by, supplier_id, type FROM " . $ecs->table('erp_order') . ") eo ON eo.order_id = eor.order_id " .
            "LEFT JOIN (SELECT user_id, user_name FROM " . $ecs->table('admin_user') . ") ca ON ca.user_id = eor.create_by " .
            "LEFT JOIN (SELECT user_id, user_name FROM " . $ecs->table('admin_user') . ") aa ON aa.user_id = eor.approve_by " .
            $where . " ORDER BY $sort_by $sort_order, status ASC " .
            "LIMIT $start, $page_size";
    $res = $db->getAll($sql);

    foreach ($res as $key => $row) {
        $res[$key]['status'] = $_LANG['po_revise_status'][$row['status']];

        $view_btn = [
            'link'  => "erp_order_revise.php?act=" . ($row['status'] < PO_REVISE_APPROVED ? 'list' : 'history') . "&revise_id=$row[revise_id]",
            'title' => '查看',
            'label' => '查看',
            'btn_class' => 'btn btn-primary'
        ];
        $custom_actions = [
            'view' => $view_btn,
        ];

        if ($row['status'] == PO_REVISE_CREATED) {
            $edit_btn = [
                'link'  => "erp_order_revise.php?act=edit&revise_id=$row[revise_id]",
                'title' => '編輯',
                'label' => '編輯',
                'btn_class' => 'btn btn-info'
            ];
            $custom_actions['edit'] = $edit_btn;
        } elseif ($row['status'] == PO_REVISE_SUBMITTED && check_authz(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_PO_UNAPPROVE)) {
            $approve_btn = [
                'link'  => "erp_order_revise.php?act=approve&revise_id=$row[revise_id]",
                'title' => '審核',
                'label' => '審核',
                'btn_class' => 'btn btn-danger'
            ];
            $custom_actions['approve'] = $approve_btn;
        }
        $res[$key]['_action'] = $erpController->generateCrudActionBtn($row['revise_id'], 'revise_id', null, [], $custom_actions);
    }

    $count_sql = "SELECT COUNT(DISTINCT revise_id) FROM " . $ecs->table('erp_order_revise') . " eor " .
                    "LEFT JOIN (SELECT order_id, order_sn, create_by, supplier_id, type FROM " . $ecs->table('erp_order') . ") eo ON eo.order_id = eor.order_id " .
                    $where;
    $count = $db->getOne($count_sql);
    
    $erpController->ajaxQueryAction($res, $count, false);
} elseif (in_array($_REQUEST['act'], ['add', 'edit', 'view', 'approve', 'history'])) {
    if (check_authz('erp_order_manage') && check_authz('erp_order_rate')) {
        if (empty($_REQUEST['revise_id'])) {
            $order_id = trim($_REQUEST['order_id']);
            if (empty($order_id)) {
                sys_msg($_LANG['erp_no_permit'],0);
            }
        } else {
            $order_id = $db->getOne("SELECT order_id FROM " . $ecs->table('erp_order_revise') . " WHERE revise_id = $_REQUEST[revise_id]");
            $revise = $erpController->get_erp_order_revise_info($_REQUEST['revise_id']);
            $smarty->assign('revise_status', $revise['status']);
            if ($_REQUEST['act'] == 'history') {
                
            } elseif (in_array($revise['status'], [PO_REVISE_APPROVED, PO_REVISE_DISAPPROVED])) {
                if ($_REQUEST['act'] != 'history') {
                    header('location: erp_order_revise.php?act=history&revise_id=' . $_REQUEST['revise_id']);
                }
            } elseif ($revise['status'] == PO_REVISE_SUBMITTED) {
                if ($_SESSION['manage_cost'] || $_SESSION['is_accountant']) {
                    if ($_REQUEST['act'] != 'approve') {
                        header('location: erp_order_revise.php?act=approve&revise_id=' . $_REQUEST['revise_id']);
                    }
                } elseif ($_REQUEST['act'] != 'view') {
                    header('location: erp_order_revise.php?act=view&revise_id=' . $_REQUEST['revise_id']);
                }
            } elseif ($_REQUEST['act'] != 'edit') {
                header('location: erp_order_revise.php?act=edit&revise_id=' . $_REQUEST['revise_id']);
            }
        }
        
        $order_info=get_order_info($order_id);

        if ($_REQUEST['act'] == 'add') {
            if (!empty($order_info['revise_id']) && $order_info['revise_id'] != '-1') {
                header('location: erp_order_revise.php?act=edit&revise_id=' . $order_info['revise_id']);
            }
        }
        $agency_id=get_admin_agency_id($_SESSION['admin_id']);
        if ($_REQUEST['act'] != 'history') {
            if($agency_id>0 &&$order_info['agency_id']!=$agency_id)
            {
                $href="erp_order_manage.php?act=order_list";
                $text=$_LANG['erp_order_return'];
                $link[] = array('href'=>$href,'text'=>$text);
                sys_msg($_LANG['erp_order_no_agency_access'],0,$link);
            }
        }
		if(!(is_admin_order($order_id,erp_get_admin_id()) || $_SESSION['manage_cost'] || $_SESSION['is_accountant'] || check_authz(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_PO_UNAPPROVE) || ($_SESSION['role_id'] == 4 && $_REQUEST['act'] == 'history')))
		{
            $href="erp_order_manage.php?act=order_list";
            $text=$_LANG['erp_order_return'];
            $link[] = array('href'=>$href,'text'=>$text);
            sys_msg($_LANG['erp_order_no_access'],0,$link);
        }
        $available_supplier_ids = array_map(function ($supplier) { return $supplier['supplier_id']; }, suppliers_list_name_by_admin());
        
        $order_item_info=get_order_item_info($order_id);
        if (!empty($revise['items'])) {
            if ($_REQUEST['act'] == 'history') {
                $order_item_info = [];
                foreach ($revise['items'] as $order_item_id => $item) {
                    $goods_details = $db->getRow("SELECT goods_sn, goods_thumb, cost, shop_price, goods_name FROM " . $ecs->table('goods') . " WHERE goods_id = $item[goods_id]");
                    $goods_details['goods_thumb'] = "../" . $goods_details['goods_thumb'];
                    $goods_details['shop_price'] = price_format($goods_details['shop_price'], false);
                    $goods_details['cost'] = price_format($goods_details['cost'], false);
                    $order_item_info[] = [
                        'order_item_id'             => $order_item_id,
                        'original_price'            => $item['old_price'],
                        'original_shipping_price'   => $item['old_shipping_price'],
                        'order_qty'                 => $item['old_order_qty'],
                        'revise'                    => $item,
                        'goods_details'             => $goods_details
                    ];
                }
            } else {
                foreach ($order_item_info as $key => $item) {
                    $order_item_info[$key]['revise'] = [
                        'price'             => $revise['items'][$item['order_item_id']]['price'],
                        'shipping_price'    => $revise['items'][$item['order_item_id']]['shipping_price'],
                        'order_qty'         => $revise['items'][$item['order_item_id']]['order_qty'],
                    ];
    
                    if ($_REQUEST['act'] == 'history') {
                        $order_item_info[$key]['price'] = $revise['items'][$item['order_item_id']]['old_price'];
                        $order_item_info[$key]['shipping_price'] = $revise['items'][$item['order_item_id']]['old_shipping_price'];
                        $order_item_info[$key]['order_qty'] = $revise['items'][$item['order_item_id']]['old_order_qty'];
                    }
                }
            }
        }
        $smarty->assign('is_self_po', in_array($order_info['supplier_id'], $available_supplier_ids));
        $smarty->assign('order_info', $order_info);
        $smarty->assign('order_item_info', $order_item_info);
        $smarty->display('erp_order_revise_info.htm');
    } else {
		sys_msg($_LANG['erp_no_permit'],0);
    }
} elseif ($_REQUEST['act'] == 'update') {
    $revise_id = empty($_REQUEST['revise_id']) ? 0 : intval($_REQUEST['revise_id']);
    $order_id = empty($_REQUEST['order_id']) ? 0 : intval($_REQUEST['order_id']);
    if (empty($order_id)) {
        make_json_error("未有選擇採購訂單");
    }
    if (empty($revise_id)) {
        if (!$erpController->create_erp_order_revise($order_id, $_REQUEST['items'], !empty($_REQUEST['submit']))) {
            make_json_error("無法建立修訂");
        }
        make_json_response("成功建立修訂");
    } else {
        foreach ($_REQUEST['items'] as $order_item_id => $data) {
            $db->query("UPDATE " . $ecs->table('erp_order_revise_item') . " SET new_price = '$data[price]', new_shipping_price = '$data[ship]', new_order_qty = '$data[qty]' WHERE revise_id = $revise_id AND order_item_id = $order_item_id");
        }
        $db->query("UPDATE " . $ecs->table('erp_order_revise') . " SET update_by = $_SESSION[admin_id], update_at = CURRENT_TIMESTAMP" . (!empty($_REQUEST['submit']) ? ", status = " . PO_REVISE_SUBMITTED : "") . " WHERE revise_id = $revise_id");
        make_json_response("成功更新修訂");
    }
} elseif ($_REQUEST['act'] == 'change_status') {
    $revise_id = empty($_REQUEST['revise_id']) ? 0 : intval($_REQUEST['revise_id']);
    $status = empty($_REQUEST['status']) ? PO_REVISE_CREATED : intval($_REQUEST['status']);
    if (empty($revise_id)) {
        make_json_error("未有選擇修訂");
    }
    if (in_array($status, [PO_REVISE_DISAPPROVED, PO_REVISE_APPROVED, PO_REVISE_SUBMITTED, PO_REVISE_CREATED])) {
        $db->query("UPDATE " . $ecs->table('erp_order_revise') . " SET approve_by = $_SESSION[admin_id], approve_at = CURRENT_TIMESTAMP, status = $status WHERE revise_id = $revise_id");
    }
    if ($status == PO_REVISE_DISAPPROVED || $status == PO_REVISE_APPROVED) {
        $revise = $erpController->get_erp_order_revise_info($revise_id);
        $db->query("UPDATE " . $ecs->table('erp_order') . " SET revise_id = -1 WHERE order_id = $revise[order_id]");

        if ($status == PO_REVISE_APPROVED) {
            include_once ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_accounting.php';

            $order_info=get_order_info($revise['order_id']);
            $revise_qty = 0;
            $currency = $actg->getCurrency(['currency_code' => $order_info['currency_code']]);

            foreach ($revise['items'] as $order_item_id => $item) {
                if (empty($item['order_qty'])) {
                    delete_order_item('', $order_item_id);
                } else {
                    $amount = bcmul($item['order_qty'], $item['price'], 4);
                    $db->query("UPDATE " . $ecs->table('erp_order_item') . " SET original_amount = '$amount', original_price = '$item[price]', original_shipping_price = '$item[shipping_price]', order_qty = '$item[order_qty]' WHERE order_item_id = $order_item_id");
                    $revise_qty += intval($item['order_qty']);
                }
            }
            $db->query("UPDATE " . $ecs->table('erp_order_item') . " SET price = original_price * $currency[currency_rate], amount = original_amount * $currency[currency_rate], shipping_price = original_shipping_price * $currency[currency_rate] WHERE order_id = $revise[order_id]");

            $accountingController = new Yoho\cms\Controller\AccountingController();

            // if no products in this order
            if ($revise_qty == 0) {
                // remove all items not removed in case
                if ($db->getOne("SELECT COUNT(*) FROM " . $ecs->table('erp_order_item') . " WHERE order_id = $revise[order_id]")) {
                    delete_order_item($revise['order_id']);
                }

                // remove all payments that has been made and not deleted
                $purchase_payments = $db->getCol("SELECT purchase_payment_id FROM `actg_purchase_payments` WHERE order_id = $revise[order_id]");
                if (!empty($purchase_payments)) {
                    foreach ($purchase_payments as $purchase_payment_id) {
                        deletePurchasePayment($purchase_payment_id);
                    }
                }
                // remove order
                delete_order($revise['order_id']);

                // reverse previous PO approved transaction
                $accountingController->reverseAccountingTransaction([
                    'actg_txn_id' 	=> $order_info['actg_txn_id'],
                    'created_by' 	=> $_SESSION['admin_id'],
                ]);
                $revise_txn_ids = $db->getCol("SELECT actg_txn_id FROM `actg_accounting_transactions` WHERE related_txn_id IS NULL AND actg_group_id = $order_info[actg_txn_id]");
                foreach ($revise_txn_ids as $revise_txn_id) {
                    $accountingController->reverseAccountingTransaction([
                        'actg_txn_id' 	=> $revise_txn_id,
                        'created_by' 	=> $_SESSION['admin_id'],
                    ]);
                }
            } else {
                $erp_order_info = $erpController->get_erp_order_info($revise['order_id']);
                $shipping_type_id = $accountingController->getAccountingType(['actg_type_code' => '44001', 'enabled' => 1], 'actg_type_id'); // Shipping Cost
                if ($erp_order_info['currency_code'] == "HKD") {
                    $purchase_type_id = $accountingController->getAccountingType(['actg_type_code' => '41100', 'enabled' => 1], 'actg_type_id'); // Local Purchase
                } else {
                    $purchase_type_id = $accountingController->getAccountingType(['actg_type_code' => '41200', 'enabled' => 1], 'actg_type_id'); // Global Purchase (Foreign currency)
                }

                $old_order_amount = $db->getOne("SELECT SUM(amount) FROM `actg_accounting_transactions` WHERE (actg_txn_id = $order_info[actg_txn_id] OR related_txn_id = $order_info[actg_txn_id] OR actg_group_id = $order_info[actg_txn_id]) AND actg_type_id = $purchase_type_id");
                $old_shipping_amount = $db->getOne("SELECT SUM(amount) FROM `actg_accounting_transactions` WHERE (actg_txn_id = $order_info[actg_txn_id] OR related_txn_id = $order_info[actg_txn_id] OR actg_group_id = $order_info[actg_txn_id]) AND actg_type_id = $shipping_type_id");

                if (bccomp($old_order_amount, $erp_order_info['order_amount']) != 0 || bccomp($old_shipping_amount, $erp_order_info['shipping_amount']) != 0) {
                    $param = [
                        'supplier_id'		=> $erp_order_info['supplier_id'],
                        'currency_code'		=> $erp_order_info['currency_code'],
                        'order_amount'		=> round(bcsub($erp_order_info['order_amount'], $old_order_amount, 4), 2),
                        'shipping_amount'	=> round(bcsub($erp_order_info['shipping_amount'], $old_shipping_amount, 4), 2),
                        'erp_order_id'		=> $revise['order_id'],
                        'created_by'		=> $_SESSION['admin_id'],
                        'remark'			=> "採購訂單 $erp_order_info[order_sn] 修訂審核",
                        'actg_group_id' 	=> $order_info['actg_txn_id'],
                    ];
                    $accountingController->insertPurchaseTransactions($param);
                }
            }
        }
        make_json_response("修訂已" . ($status == PO_REVISE_DISAPPROVED ? "拒絕" : "通過"));
    }
    make_json_response("已更新狀態");
}

?>