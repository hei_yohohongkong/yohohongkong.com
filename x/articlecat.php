<?php

/**
 * ECSHOP 文章分类管理程序
 * ============================================================================
 * 版权所有 2005-2010 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liuhui $
 * $Id: articlecat.php 17063 2010-03-25 06:35:46Z liuhui $
*/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

/* Using for cat icon */
include_once(ROOT_PATH . 'includes/cls_image.php');
$image = new cls_image($_CFG['bgcolor']);
$articleController = new \Yoho\cms\Controller\ArticleController();
$exc = new exchange($ecs->table("article_cat"), $db, 'cat_id', 'cat_name');
/* act操作项的初始化 */
$_REQUEST['act'] = trim($_REQUEST['act']);
if (empty($_REQUEST['act']))
{
    $_REQUEST['act'] = 'list';
}

/*------------------------------------------------------ */
//-- 分类列表
/*------------------------------------------------------ */
if ($_REQUEST['act'] == 'list')
{
    // Multiple language support
    $_REQUEST['edit_lang'] = empty($_REQUEST['edit_lang']) ? default_language() : $_REQUEST['edit_lang'];
    if (!empty($_REQUEST['edit_lang']))
    {
        $old_lang = $_CFG['lang'];
        $_CFG['lang'] = $_REQUEST['edit_lang'];
    }

    $articlecat = article_cat_list(0, 0, false);

    // Multiple language support
    if (!empty($old_lang))
    {
        $_CFG['lang'] = $old_lang;
    }
    $pid = 0;
    $pcat_type = 0;
    $group_list = [];
    foreach ($articlecat as $key => &$cat)
    {
        $cat['type_name'] = $_LANG['type_name'][$cat['cat_type']];
        if($cat['parent_id'] == 0) {
            $pid = $cat['cat_id'];
            $pcat_type = (in_array($cat['cat_type'], [COMMON_CAT, UPHELP_CAT, SUPPORT_CAT])) ? $cat['cat_type'] : 0;
        }
        $group_list[$pcat_type][$cat['cat_id']] = $cat;
    }
    unset($articlecat);
    krsort($group_list);
    $smarty->assign('ur_here',     $_LANG['02_articlecat_list']);
    $smarty->assign('action_link', array('text' => $_LANG['articlecat_add'], 'href' => 'articlecat.php?act=add'));
    $smarty->assign('full_page',   1);
    $smarty->assign('articlecat',        $group_list);

    assign_query_info();
    $smarty->display('articlecat_list.htm');
}
/*------------------------------------------------------ */
//-- 分类列表
/*------------------------------------------------------ */
if ($_REQUEST['act'] == 'support_list')
{
    // Multiple language support
    $_REQUEST['edit_lang'] = empty($_REQUEST['edit_lang']) ? default_language() : $_REQUEST['edit_lang'];
    if (!empty($_REQUEST['edit_lang']))
    {
        $old_lang = $_CFG['lang'];
        $_CFG['lang'] = $_REQUEST['edit_lang'];
    }

    $articlecat = article_cat_list(0, 0, false, 0, 1);

    // Multiple language support
    if (!empty($old_lang))
    {
        $_CFG['lang'] = $old_lang;
    }

    $pid = 0;
    $pcat_type = 0;
    $group_list = [];
    foreach ($articlecat as $key => &$cat)
    {
        $cat['type_name'] = $_LANG['type_name'][$cat['cat_type']];
        if($cat['parent_id'] == 0) {
            $pid = $cat['cat_id'];
            $pcat_type = (in_array($cat['cat_type'], [COMMON_CAT, UPHELP_CAT, SUPPORT_CAT])) ? $cat['cat_type'] : 0;
        }
        $group_list[$pcat_type][$cat['cat_id']] = $cat;
    }
    unset($articlecat);
    krsort($group_list);
    $smarty->assign('articlecat',        $group_list);
    $smarty->assign('ur_here',     $_LANG['support_list']);
    $smarty->assign('action_link', array('text' => $_LANG['support_list_add'], 'href' => 'articlecat.php?act=add&support=1'));
    $smarty->assign('full_page',   1);

    assign_query_info();
    $smarty->display('articlecat_list.htm');
}
/*------------------------------------------------------ */
//-- 查询
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'query')
{
    // Multiple language support
    $_REQUEST['edit_lang'] = empty($_REQUEST['edit_lang']) ? default_language() : $_REQUEST['edit_lang'];
    if (!empty($_REQUEST['edit_lang']))
    {
        $old_lang = $_CFG['lang'];
        $_CFG['lang'] = $_REQUEST['edit_lang'];
    }

    $articlecat = article_cat_list(0, 0, false);

    // Multiple language support
    if (!empty($old_lang))
    {
        $_CFG['lang'] = $old_lang;
    }

    foreach ($articlecat as $key => $cat)
    {
        $articlecat[$key]['type_name'] = $_LANG['type_name'][$cat['cat_type']];
    }
    $smarty->assign('articlecat',        $articlecat);

    make_json_result($smarty->fetch('articlecat_list.htm'));
}

/*------------------------------------------------------ */
//-- 添加分类
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'add')
{
    /* 权限判断 */
    admin_priv('article_cat');

    $cat_select = article_cat_list(0, 0, true, 0, $_REQUEST['support']);
    $smarty->assign('cat_select',  $cat_select);
    if( $_REQUEST['support']) {
        $smarty->assign('action_link', array('text' => $_LANG['back_support_list'], 'href' => 'articlecat.php?act=support_list'));
        $smarty->assign('ur_here',     $_LANG['support_list_add']);
    } else {
        $smarty->assign('action_link', array('text' => $_LANG['02_articlecat_list'], 'href' => 'articlecat.php?act=list'));
        $smarty->assign('ur_here',     $_LANG['articlecat_add']);
    }
    $smarty->assign('form_action', 'insert');
    // Multiple language support
    $localizable_fields = localizable_fields_for_table('article_cat');
    $smarty->assign('localizable_fields', $localizable_fields);
    $smarty->assign('support', $_REQUEST['support']);
    assign_query_info();
    $smarty->display('articlecat_info.htm');
}
elseif ($_REQUEST['act'] == 'insert')
{
    /* 权限判断 */
    admin_priv('article_cat');

    /*检查分类名是否重复*/
    $is_only = $exc->is_only('cat_name', $_POST['cat_name']);

    if (!$is_only)
    {
        sys_msg(sprintf($_LANG['catname_exist'], stripslashes($_POST['cat_name'])), 1);
    }

    $cat_type = 1;
    if ($_POST['parent_id'] > 0)
    {
        $sql = "SELECT cat_type FROM " . $ecs->table('article_cat') . " WHERE cat_id = '$_POST[parent_id]'";
        $p_cat_type = $db->getOne($sql);
        if ($p_cat_type == 2 || $p_cat_type == 3 || $p_cat_type == 5)
        {
            sys_msg($_LANG['not_allow_add'], 0);
        }
        else if ($p_cat_type == 4)
        {
            $cat_type = 5;
        }
        else if ($p_cat_type == 6)
        {
            $cat_type = 6;
        }
    }

    /* 2017-2-28 : Upload icon */
    if ((isset($_FILES['cat_icon']['error']) && $_FILES['cat_icon']['error'] == 0) || (!isset($_FILES['cat_icon']['error']) && isset($_FILES['cat_icon']['tmp_name'] ) &&$_FILES['cat_icon']['tmp_name'] != 'none'))
        {
            $cat_icon = basename($image->upload_image($_FILES['cat_icon'], 'afficheimg/cat_icon'));
        }
    if (((isset($_FILES['cat_icon']['error']) && $_FILES['cat_icon']['error'] > 0) || (!isset($_FILES['cat_icon']['error']) && isset($_FILES['cat_icon']['tmp_name']) && $_FILES['cat_icon']['tmp_name'] == 'none')) && empty($_POST['img_url']))
        {
            $link[] = array('text' => $_LANG['go_back'], 'href' => 'javascript:history.back(-1)');
//            sys_msg($_LANG['js_languages']['ad_photo_empty'], 0, $link);
        }

    // handle simplified chinese
    $localizable_fields = [
        "cat_name"  => $_POST["cat_name"],
        "cat_desc"  => $_POST['cat_desc'],
        "keywords"  => $_POST['keywords'],
        "perma_link"  => empty($_POST["perma_link_raw"]) ? $_POST['cat_name'] : $_POST["perma_link_raw"],
    ];
    to_simplified_chinese($localizable_fields);

    /* Insert sql */
    $sql = "INSERT INTO ".$ecs->table('article_cat')."(cat_name, cat_type, cat_desc, cat_icon, keywords, parent_id, sort_order, show_in_nav, show_in_footer, perma_link)
           VALUES ('$_POST[cat_name]', '$cat_type',  '$_POST[cat_desc]',  '$cat_icon', '$_POST[keywords]', '$_POST[parent_id]', '$_POST[sort_order]', '$_POST[show_in_nav]', '$_POST[show_in_footer]', '$_POST[perma_link]')";
    $db->query($sql);
    $id = $db->insert_id();

    if($_POST['show_in_nav'] == 1)
    {
        $vieworder = $db->getOne("SELECT max(vieworder) FROM ". $ecs->table('nav') . " WHERE type = 'middle'");
        $vieworder += 2;
        //显示在自定义导航栏中
        $sql = "INSERT INTO " . $ecs->table('nav') . " (name,ctype,cid,ifshow,vieworder,opennew,url,type) VALUES('" . $_POST['cat_name'] . "', 'a', '$id','1','$vieworder','0', '" . build_uri('article_cat', array('acid'=> $id), $_POST['cat_name']) . "','middle')";
        $db->query($sql);
        $nav_id = $db->insert_id();

        $translated = translate_to_simplified_chinese($_POST['cat_name']);
        if ($translated != $_POST['cat_name']) {
            $db->query(
                "INSERT INTO " . $ecs->table('nav_lang') . " (id,lang,name,url) VALUES('$nav_id', 'zh_cn', '$translated', '" . build_uri('article_cat', array('acid'=> $id), '', 0, '', 0, 'zh_cn') . "')"
            );
        }
    }
    $articleController->updateCatParentStyle($id, $_POST);

    // Multiple language support
    save_localized_versions('article_cat', $id);

    admin_log($_POST['cat_name'],'add','articlecat');

    $link[0]['text'] = $_LANG['continue_add'];
    $link[0]['href'] = 'articlecat.php?act=add'.( $cat_type == 6 ? '&support=1' : '');

    $link[1]['text'] = $_LANG['back_list'];
    $link[1]['href'] = 'articlecat.php?act='.( $cat_type == 6 ? 'support_list' : 'list');
    clear_cache_files();
    sys_msg($_POST['cat_name'].$_LANG['catadd_succed'],0, $link);
}

/*------------------------------------------------------ */
//-- 编辑文章分类
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'edit')
{
    /* 权限判断 */
    admin_priv('article_cat');
    $support = 0;
    $sql = "SELECT * FROM ".
           $ecs->table('article_cat'). " WHERE cat_id='$_REQUEST[id]'";
    $cat = $db->GetRow($sql);
    
    if($cat['cat_type'] == SUPPORT_CAT) $support = 1;
    /* get icon */
    if(isset($cat['cat_icon']) && $cat['cat_icon'] != '' ){
        $src = '../' . DATA_DIR . '/afficheimg/cat_icon/'. $cat['cat_icon'];
        $cat['cat_icon_src'] = $src;
    }
    /* get banner (Mobile) */
    if(isset($cat['cat_banner']) && $cat['cat_banner'] != '' ){
        $src = '../' . DATA_DIR . '/afficheimg/cat_banner/'. $cat['cat_banner'];;
        $cat['cat_banner_src'] = $src;
    }
    if ($cat['cat_type'] == 2 || $cat['cat_type'] == 3 || $cat['cat_type'] ==4)
    {
        $smarty->assign('disabled', 1);
    }
    $cat['desktop'] = unserialize($cat['desktop_config']);
    if(!empty($cat['desktop']) && $cat['desktop']['background_img'] != ''){
        $cat['desktop']['old_background_img'] = $cat['desktop']['background_img'];
        $src = '../' . DATA_DIR . '/afficheimg/cat_banner/'. $cat['desktop']['background_img'];
        $cat['desktop']['background_img'] = $src;
    }
    $cat['mobile']  = unserialize($cat['mobile_config']);
    if(!empty($cat['mobile']) && $cat['mobile']['background_img'] != ''){
        $cat['mobile']['old_background_img'] = $cat['mobile']['background_img'];
        $src = '../' . DATA_DIR . '/afficheimg/cat_banner/'. $cat['mobile']['background_img'];
        $cat['mobile']['background_img'] = $src;
    }
    $options    =   article_cat_list(0, $cat['parent_id'], false, 0, $support);
    $select     =   '';
    $selected   =   $cat['parent_id'];
    foreach ($options as $var)
    {
        if ($var['cat_id'] == $_REQUEST['id'])
        {
            continue;
        }
        $select .= '<option value="' . $var['cat_id'] . '" ';
        $select .= ' cat_type="' . $var['cat_type'] . '" ';
        $select .= ($selected == $var['cat_id']) ? "selected='ture'" : '';
        $select .= '>';
        if ($var['level'] > 0)
        {
            $select .= str_repeat('&nbsp;', $var['level'] * 4);
        }
        $select .= htmlspecialchars($var['cat_name']) . '</option>';
    }
    unset($options);
    $array = [COMMON_CAT, SYSTEM_CAT, INFO_CAT, UPHELP_CAT, HELP_CAT];
    $types = [];
    foreach ($array as $type){
        $value = $type;
        $name = $_LANG['type_name'][$type];
        $types[] = ['value' => $value, 'name' => $name];
    };
    $smarty->assign('cat',         $cat);
    $smarty->assign('cat_select',  $select);
    $smarty->assign('type_select',  $types);
    if($cat['parent_id'] == 0) {
        $smarty->assign('disabled',  1);
    }
    $sql = "SELECT * FROM ".
        $ecs->table('article_cat'). " WHERE cat_id='$cat[parent_id]'";
    $parent = $db->GetRow($sql);
    $smarty->assign('parent', $parent);
    if($support) {
        $smarty->assign('ur_here',     $_LANG['support_cat_edit']);
        $smarty->assign('action_link', array('text' => $_LANG['support_list'], 'href' => 'articlecat.php?act=support_list'));
    } else {
        $smarty->assign('ur_here',     $_LANG['articlecat_edit']);
        $smarty->assign('action_link', array('text' => $_LANG['02_articlecat_list'], 'href' => 'articlecat.php?act=list'));
    }
    $smarty->assign('form_action', 'update');
    $smarty->assign('support', $_REQUEST['support']);

    // Multiple language support
    $localizable_fields = localizable_fields_for_table('article_cat');
    $smarty->assign('localizable_fields', $localizable_fields);
    $smarty->assign('localized_versions', get_localized_versions('article_cat', $_REQUEST['id'], $localizable_fields));

    assign_query_info();
    $smarty->display('articlecat_info.htm');
}
elseif ($_REQUEST['act'] == 'update')
{
    /* 权限判断 */
    admin_priv('article_cat');

    /*检查重名*/
    if ($_POST['cat_name'] != $_POST['old_catname'])
    {
        $is_only = $exc->is_only('cat_name', $_POST['cat_name'], $_POST['id']);

        if (!$is_only)
        {
            sys_msg(sprintf($_LANG['catname_exist'], stripslashes($_POST['cat_name'])), 1);
        }
    }

    if(!isset($_POST['parent_id']))
    {
        $_POST['parent_id'] = 0;
    }

    $row = $db->getRow("SELECT cat_type, parent_id, cat_icon FROM " . $ecs->table('article_cat') . " WHERE cat_id='$_POST[id]'");
    $cat_type = $row['cat_type'];
    if ($cat_type == 3 || $cat_type ==4)
    {
        $_POST['parent_id'] = $row['parent_id'];
    }

    /* 检查设定的分类的父分类是否合法 */
    $child_cat = article_cat_list($_POST['id'], 0, false);
    if (!empty($child_cat))
    {
        foreach ($child_cat as $child_data)
        {
            $catid_array[] = $child_data['cat_id'];
        }
    }
    if (in_array($_POST['parent_id'], $catid_array))
    {
        sys_msg(sprintf($_LANG['parent_id_err'], stripslashes($_POST['cat_name'])), 1);
    }

    if ($cat_type == COMMON_CAT || $cat_type == HELP_CAT)
    {
        if ($_POST['parent_id'] > 0)
        {
            $sql = "SELECT cat_type FROM " . $ecs->table('article_cat') . " WHERE cat_id = '$_POST[parent_id]'";
            $p_cat_type = $db->getOne($sql);
            if ($p_cat_type == UPHELP_CAT)
            {
                $cat_type = HELP_CAT;
            } else if ($p_cat_type == SUPPORT_CAT)
            {
                $cat_type = SUPPORT_CAT;
            }
            else
            {
                $cat_type = COMMON_CAT;
            }
        }
        else
        {
            $cat_type = COMMON_CAT;
        }
    }

    /* update img */
    if ($_FILES['cat_icon']['tmp_name'] != '' && $_FILES['cat_icon']['tmp_name'] != 'none')
    {
        $update_cat_icon = basename($image->upload_image($_FILES['cat_icon'], 'afficheimg/cat_icon'));
        
        /*delete old image*/
        
        if ($row['cat_icon'] != '' && is_file( '../' . DATA_DIR . '/afficheimg/cat_icon/'. $row['cat_icon']))
        {
            @unlink( '../' . DATA_DIR . '/afficheimg/cat_icon/'. $row['cat_icon']);
        }
    }
    else
    {
        $update_cat_icon = $row['cat_icon'];
    }
    if ($_FILES['cat_banner']['tmp_name'] != '' && $_FILES['cat_banner']['tmp_name'] != 'none')
    {
        $update_cat_banner = basename($image->upload_image($_FILES['cat_banner'], 'afficheimg/cat_banner'));
        
        /*delete old image*/
        
        if ($row['cat_banner'] != '' && is_file( '../' . DATA_DIR . '/afficheimg/cat_banner/'. $row['cat_banner']))
        {
            @unlink( '../' . DATA_DIR . '/afficheimg/cat_banner/'. $row['cat_banner']);
        }
    }
    else
    {
        $update_cat_banner = $row['cat_banner'];
    }

    $articleController->updateCatParentStyle(intval($_POST['id']), $_POST);

    $dat = $db->getOne("SELECT cat_name, show_in_nav FROM ". $ecs->table('article_cat') . " WHERE cat_id = '" . $_POST['id'] . "'");

    // handle simplified chinese
    $localizable_fields = [
        "cat_name"  => $_POST["cat_name"],
        "cat_desc"  => $_POST['cat_desc'],
        "keywords"  => $_POST['keywords'],
    ];
    $localizable_fields = $articleController->checkRequireSimplied("article_cat", $localizable_fields, $_POST['id']);
    to_simplified_chinese($localizable_fields);

    if ($exc->edit("cat_name = '$_POST[cat_name]', cat_desc ='$_POST[cat_desc]', cat_icon ='$update_cat_icon', cat_banner ='$update_cat_banner', keywords='$_POST[keywords]',parent_id = '$_POST[parent_id]', cat_type='$cat_type', sort_order='$_POST[sort_order]', show_in_nav = '$_POST[show_in_nav]', show_in_footer = '$_POST[show_in_footer]'",  $_POST['id']))
    {
        if($_POST['cat_name'] != $dat['cat_name'])
        {
            //如果分类名称发生了改变
            $sql = "UPDATE " . $ecs->table('nav') . " SET name = '" . $_POST['cat_name'] . "' WHERE ctype = 'a' AND cid = '" . $_POST['id'] . "' AND type = 'middle'";
            $db->query($sql);
        }
        if($_POST['show_in_nav'] != $dat['show_in_nav'])
        {
            if($_POST['show_in_nav'] == 1)
            {
                //显示
                $nid = $db->getOne("SELECT id FROM ". $ecs->table('nav') . " WHERE ctype = 'a' AND cid = '" . $_POST['id'] . "' AND type = 'middle'");
                if(empty($nid))
                {
                    $vieworder = $db->getOne("SELECT max(vieworder) FROM ". $ecs->table('nav') . " WHERE type = 'middle'");
                    $vieworder += 2;
                    $uri = build_uri('article_cat', array('acid'=> $_POST['id']), $_POST['cat_name']);
                    //不存在
                    $sql = "INSERT INTO " . $ecs->table('nav') .
                        " (name,ctype,cid,ifshow,vieworder,opennew,url,type) ".
                        "VALUES('" . $_POST['cat_name'] . "', 'a', '" . $_POST['id'] . "','1','$vieworder','0', '" . $uri . "','middle')";
                    $insert_nav = true;
                }
                else
                {
                    $sql = "UPDATE " . $ecs->table('nav') . " SET ifshow = 1 WHERE ctype = 'a' AND cid = '" . $_POST['id'] . "' AND type = 'middle'";
                }
                $db->query($sql);
                if ($insert_nav) {
                    $nav_id = $db->insert_id();

                    $translated = translate_to_simplified_chinese($_POST['cat_name']);
                    if ($translated != $_POST['cat_name']) {
                        $db->query(
                            "INSERT INTO " . $ecs->table('nav_lang') . " (id,lang,name,url) VALUES('$nav_id', 'zh_cn', '$translated', '" . build_uri('article_cat', array('acid'=> $_POST['id']), '', 0, '', 0, 'zh_cn') . "')"
                        );
                    }
                }
            }
            else
            {
                //去除
                $db->query("UPDATE " . $ecs->table('nav') . " SET ifshow = 0 WHERE ctype = 'a' AND cid = '" . $_POST['id'] . "' AND type = 'middle'");
            }
        }

        // Multiple language support
        save_localized_versions('article_cat', $_POST['id']);

        $link[0]['text'] = $_LANG['back_list'];
        if($cat_type == SUPPORT_CAT)$link[0]['href'] = 'articlecat.php?act=support_list';
        else $link[0]['href'] = 'articlecat.php?act=list';
        $note = sprintf($_LANG['catedit_succed'], $_POST['cat_name']);
        admin_log($_POST['cat_name'], 'edit', 'articlecat');
        clear_cache_files();
        sys_msg($note, 0, $link);

    }
    else
    {
        die($db->error());
    }
}



/*------------------------------------------------------ */
//-- 编辑文章分类的排序
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'edit_sort_order')
{
    check_authz_json('article_cat');

    $id    = intval($_POST['id']);
    $order = json_str_iconv(trim($_POST['val']));

    /* 检查输入的值是否合法 */
    if (!preg_match("/^[0-9]+$/", $order))
    {
        make_json_error(sprintf($_LANG['enter_int'], $order));
    }
    else
    {
        if ($exc->edit("sort_order = '$order'", $id))
        {
            clear_cache_files();
            make_json_result(stripslashes($order));
        }
        else
        {
            make_json_error($db->error());
        }
    }
}

/*------------------------------------------------------ */
//-- 删除文章分类
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'remove')
{
    check_authz_json('article_cat');

    $id = intval($_GET['id']);

    $sql = "SELECT cat_type, cat_icon, cat_banner FROM " . $ecs->table('article_cat') . " WHERE cat_id = '$id'";
    $row = $db->getRow($sql);
    $cat_type = $row['cat_type'];
    $icon = $row['cat_icon'];
    $banner = $row['cat_banner'];

    /*delete old image*/
    if ($icon != '' && is_file( '../' . DATA_DIR . '/afficheimg/cat_icon/'. $icon))
    {
        @unlink( '../' . DATA_DIR . '/afficheimg/cat_icon/'. $icon);
    }
    if ($banner != '' && is_file( '../' . DATA_DIR . '/afficheimg/cat_banner/'. $banner))
    {
        @unlink( '../' . DATA_DIR . '/afficheimg/cat_banner/'. $banner);
    }

    if ($cat_type == 2 || $cat_type == 3 || $cat_type ==4)
    {
        /* 系统保留分类，不能删除 */
        make_json_error($_LANG['not_allow_remove']);
    }

    $sql = "SELECT COUNT(*) FROM " . $ecs->table('article_cat') . " WHERE parent_id = '$id'";
    if ($db->getOne($sql) > 0)
    {
        /* 还有子分类，不能删除 */
        make_json_error($_LANG['is_fullcat']);
    }

    /* 非空的分类不允许删除 */
    $sql = "SELECT COUNT(*) FROM ".$ecs->table('article')." WHERE cat_id = '$id'";
    if ($db->getOne($sql) > 0)
    {
        make_json_error(sprintf($_LANG['not_emptycat']));
    }
    else
    {
        $exc->drop($id);
        $nav_id = $db->getOne("SELECT id FROM " . $ecs->table('nav') . "WHERE  ctype = 'a' AND cid = '$id' AND type = 'middle'");
        $db->query("DELETE FROM " . $ecs->table('nav') . "WHERE id = '$nav_id'");
        $db->query("DELETE FROM " . $ecs->table('nav_lang') . "WHERE id = '$nav_id'");
        clear_cache_files();
        admin_log($cat_name, 'remove', 'category');
    }

    $url = 'articlecat.php?act=query&' . str_replace('act=remove', '', $_SERVER['QUERY_STRING']);

    ecs_header("Location: $url\n");
    exit;
}
/*------------------------------------------------------ */
//-- 切换是否显示在导航栏
/*------------------------------------------------------ */

if ($_REQUEST['act'] == 'toggle_show_in_nav')
{
    check_authz_json('cat_manage');

    $id = intval($_POST['id']);
    $val = intval($_POST['val']);

    if (cat_update($id, array('show_in_nav' => $val)) != false)
    {
        if($val == 1)
        {
            //显示
            $nid = $db->getOne("SELECT id FROM ". $ecs->table('nav') . " WHERE ctype='a' AND cid='$id' AND type = 'middle'");
            if(empty($nid))
            {
                //不存在
                $vieworder = $db->getOne("SELECT max(vieworder) FROM ". $ecs->table('nav') . " WHERE type = 'middle'");
                $vieworder += 2;
                $catname = $db->getOne("SELECT cat_name FROM ". $ecs->table('article_cat') . " WHERE cat_id = '$id'");
                $uri = build_uri('article_cat', array('acid'=> $id), $_POST['cat_name']);

                $sql = "INSERT INTO " . $ecs->table('nav') . " (name,ctype,cid,ifshow,vieworder,opennew,url,type) ".
                    "VALUES('" . $catname . "', 'a', '$id','1','$vieworder','0', '" . $uri . "','middle')";
                $insert_nav = true;
            }
            else
            {
                $sql = "UPDATE " . $ecs->table('nav') . " SET ifshow = 1 WHERE ctype='a' AND cid='$id' AND type = 'middle'";
            }
            $db->query($sql);
            if ($insert_nav) {
                $nav_id = $db->insert_id();

                $translated = translate_to_simplified_chinese($_POST['cat_name']);
                if ($translated != $_POST['cat_name']) {
                    $db->query(
                        "INSERT INTO " . $ecs->table('nav_lang') . " (id,lang,name,url) VALUES('$nav_id', 'zh_cn', '$translated', '" . build_uri('article_cat', array('acid'=> $id), '', 0, '', 0, 'zh_cn') . "')"
                    );
                }
            }
        }
        else
        {
            //去除
            $db->query("UPDATE " . $ecs->table('nav') . " SET ifshow = 0 WHERE ctype='a' AND cid='$id' AND type = 'middle'");
        }
        clear_cache_files();
        make_json_result($val);
    }
    else
    {
        make_json_error($db->error());
    }
}
if ($_REQUEST['act'] == 'toggle_show_in_footer')
{
    check_authz_json('cat_manage');

    $id = intval($_POST['id']);
    $val = intval($_POST['val']);

    if (cat_update($id, array('show_in_footer' => $val)) != false)
    {
        clear_cache_files();
        make_json_result($val);
    }
    else
    {
        make_json_error($db->error());
    }
}

/**
 * 添加商品分类
 *
 * @param   integer $cat_id
 * @param   array   $args
 *
 * @return  mix
 */
function cat_update($cat_id, $args)
{
    if (empty($args) || empty($cat_id))
    {
        return false;
    }

    return $GLOBALS['db']->autoExecute($GLOBALS['ecs']->table('article_cat'), $args, 'update', "cat_id='$cat_id'");
}
?>
