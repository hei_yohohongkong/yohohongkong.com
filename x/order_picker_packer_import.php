<?php

/***
* order_picker_packer_import.php
* by EricHo 2018-04-23
*
* Order Import Picker Packer
***/


define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require(ROOT_PATH . 'includes/lib_order.php');
 require_once(ROOT_PATH .'/languages/zh_tw/admin/order.php');
$userController = new Yoho\cms\Controller\UserController();
$orderController = new Yoho\cms\Controller\OrderController();

if (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'query')) {
    
    /* 检查权限 */
    check_authz_json('order_picker_packer_import');
    $result = $orderController->ajaxQueryOrderPickerAction();

}else if (isset($_REQUEST['act']) && $_REQUEST['act'] == 'upload') {
    $result = $orderController->import_picker_packer_excel();
    if (!empty($result['error'])){
      make_json_error($result['error']);
    }else{
      make_json_result('',$result['success']);
    }
    exit;
}else if (isset($_REQUEST['act']) && $_REQUEST['act'] == 'download_template') {
    $result = $orderController->export_picker_packer_template();
    if (!empty($result['error'])){
      make_json_error($result['error']);
    }else{
      make_json_result('',$result['success']);
    }
    exit;
}else if (isset($_REQUEST['act']) && $_REQUEST['act'] == 'list') {
    check_authz_json('order_picker_packer_import');
    $smarty->assign('ur_here', $_LANG['order_picker_packer_import']);
    $smarty->assign('start_date', $_REQUEST['start_date']);
    $smarty->assign('end_date', $_REQUEST['end_date']);
    $smarty->assign('cfg_lang', $_CFG['lang']);
    $smarty->assign('lang', $_LANG);
    $smarty->assign('action_link', array('text' => '匯入訂單出貨人', 'href' => 'order_picker_packer_import.php'));
    /* 显示页面 */
    assign_query_info();
    $smarty->display('order_picker_packer_list.htm');
}

/*------------------------------------------------------ */
//--商品明细列表
/*------------------------------------------------------ */
else {  

    /* 权限判断 */
    admin_priv('order_picker_packer_import');
    /* 赋值到模板 */
    $smarty->assign('ur_here', $_LANG['order_picker_packer_import']);
    $smarty->assign('full_page', 1);
    $smarty->assign('cfg_lang', $_CFG['lang']);
    $smarty->assign('action_link', array('text' => '訂單出貨人列表', 'href' => 'order_picker_packer_import.php?act=list'));
    
    /* 显示页面 */
    assign_query_info();
    $smarty->display('order_picker_packer_import.htm');
}
