<?php
define('IN_ECS', true);

require dirname(__FILE__) . '/includes/init.php';
require_once ROOT_PATH . 'includes/cls_image.php';
$kpiController = new Yoho\cms\Controller\KpiController();

if (empty($_REQUEST['act'])) {
    $_REQUEST['act'] = 'menu';
} else {
    $_REQUEST['act'] = trim($_REQUEST['act']);
}

$smarty->assign('lang', $_LANG);

if ($_REQUEST['act'] == 'menu') {
    $kpiController->menuAction();
} elseif ($_REQUEST['act'] == "build_column") {
    $kpiController->buildKpiColumn();
} elseif ($_REQUEST['act'] == "get_team_columns") {
    $kpiController->buildKpiColumn($_GET['team_code']);
} elseif ($_REQUEST['act'] == "update_columns") {
    $kpiController->updateKpiColumns($_REQUEST);
} elseif ($_REQUEST['act'] == 'list') {
    $kpiController->listAction();
} elseif ($_REQUEST['act'] == 'add') {
    $kpiController->addAction();
} elseif ($_REQUEST['act'] == 'ajaxAdd') {
    $kpiController->addAction();
} elseif ($_REQUEST['act'] == 'create_kpi') {
    $kpiController->insertAction();
} elseif ($_REQUEST['act'] == 'remove') {
    $kpiController->ajaxRemoveAction();
} elseif ($_REQUEST['act'] == 'import') {
    $kpiController->importAction($_REQUEST);
}