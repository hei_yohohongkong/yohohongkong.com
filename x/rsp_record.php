<?php

/***
* by Anthony 20180410
* Removal Service Plan
***/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
$rspController = new Yoho\cms\Controller\RspController();
/* act操作项的初始化 */
$_REQUEST['act'] = trim($_REQUEST['act']);
if (empty($_REQUEST['act']))
{
    $_REQUEST['act'] = 'list';
}

if($_REQUEST['act'] == 'list') {
    $rspController->listAction();
} elseif($_REQUEST['act'] == 'query') {
    $rspController->ajaxQueryAction();
} elseif($_REQUEST['act'] == 'ajaxEdit') {
    $rspController->ajaxEditAction();
} elseif(in_array($_REQUEST['act'], ['add', 'edit', 'view', 'insert', 'step', 'cancel'])) {
    $rspController->{$_REQUEST['act'].'Action'}();
} elseif($_REQUEST['act']=='findOrder') {
    $rspController->ajaxfindOrderAction();
}