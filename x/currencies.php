<?php

/***
* currencies.php
* by howang 2014-12-05
*
* Accounting System
***/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_accounting.php');

if (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'query'))
{
    $data = get_currencies();
    $smarty->assign('currencies',      $data['data']);
    $smarty->assign('filter',          $data['filter']);
    $smarty->assign('record_count',    $data['record_count']);
    $smarty->assign('page_count',      $data['page_count']);
    
    $sort_flag  = sort_flag($data['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);
    
    make_json_result($smarty->fetch('currencies_list.htm'), '', array('filter' => $data['filter'], 'page_count' => $data['page_count']));
}
elseif (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'add'))
{
    /* 权限判断 */
    admin_priv('sale_order_stats');
    
    $smarty->assign('ur_here',     '新增貨幣');
    
    $smarty->assign('action_link', array('text' => '返回貨幣列表', 'href' => 'currencies.php?act=list'));
    
    assign_query_info();
    $smarty->display('currency_info.htm');
}
elseif (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'insert'))
{
    /* 权限判断 */
    admin_priv('sale_order_stats');
    
    $currency_code = empty($_POST['currency_code']) ? 0 : trim($_POST['currency_code']);
    if (empty($currency_code))
    {
        sys_msg('必須輸入貨幣代號');
    }
    
    $test = $actg->getCurrency(compact('currency_code'));
    if (!empty($test))
    {
        sys_msg('該貨幣代號已經存在');
    }
    
    $currency_name = empty($_POST['currency_name']) ? '' : trim($_POST['currency_name']);
    if (empty($currency_name))
    {
        sys_msg('必須輸入貨幣名稱');
    }
    
    $currency_rate = empty($_POST['currency_rate']) ? 0 : doubleval($_POST['currency_rate']);
    if (empty($currency_rate))
    {
        sys_msg('必須輸入貨幣滙率');
    }
    
    $currency_format = empty($_POST['currency_format']) ? '%s' . $currency_code : trim($_POST['currency_format']);
    
    $actg->start_transaction();
    
    $currency = array(
        'currency_code' => $currency_code,
        'currency_name' => $currency_name,
        'currency_rate' => $currency_rate,
        'currency_format' => $currency_format,
    );
    $ok = $actg->addCurrency($currency);
    
    if ($ok)
    {
        $actg->commit_transaction();

        $link[0]['text'] = '返回貨幣列表';
        $link[0]['href'] = 'currencies.php?act=list';

        sys_msg('新增貨幣成功', 0, $link, true);
    }
    else
    {
        $actg->rollback_transaction();

        sys_msg('新增貨幣失敗');
    }
}
elseif (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'edit_currency_name'))
{
    /* 权限判断 */
    check_authz_json('sale_order_stats');
    
    $currency_code = json_str_iconv(trim($_POST['id']));
    $currency_name = json_str_iconv(trim($_POST['val']));
    
    $param = compact('currency_code', 'currency_name');
    
    if ($actg->updateCurrency($param))
    {
        make_json_result(stripslashes($currency_name));
    }
    else
    {
        make_json_error('更改貨幣名稱失敗');
    }
}
elseif (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'edit_currency_rate'))
{
    /* 权限判断 */
    check_authz_json('sale_order_stats');

    $currency_code = json_str_iconv(trim($_POST['id']));
    $currency_rate = doubleval($_POST['val']);

    $param = compact('currency_code', 'currency_rate');

    if ($actg->updateCurrency($param))
    {
        $currency = $actg->getCurrency(compact('currency_code'));
        make_json_result($currency['currency_rate']);
    }
    else
    {
        make_json_error('更改貨幣滙率失敗');
    }
}
elseif (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'edit_currency_format'))
{
    /* 权限判断 */
    check_authz_json('sale_order_stats');

    $currency_code = json_str_iconv(trim($_POST['id']));
    $currency_format = json_str_iconv(trim($_POST['val']));

    $param = compact('currency_code', 'currency_format');

    if ($actg->updateCurrency($param))
    {
        make_json_result(stripslashes($currency_format));
    }
    else
    {
        make_json_error('更改貨幣顯示格式失敗');
    }
}
else
{
    $data = get_currencies();
    /* 赋值到模板 */
    $smarty->assign('filter',       $data['filter']);
    $smarty->assign('record_count', $data['record_count']);
    $smarty->assign('page_count',   $data['page_count']);
    $smarty->assign('currencies',   $data['data']);
    $smarty->assign('ur_here',      '貨幣列表');
    $smarty->assign('full_page',    1);
    $smarty->assign('action_link',  array('text' => '貨幣兌換紀錄','href'=>'currency_exchanges.php?act=list'));
    $smarty->assign('action_link2',  array('text' => '新增貨幣','href'=>'currencies.php?act=add'));
    
    $sort_flag  = sort_flag($data['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);
    
    /* 显示页面 */
    assign_query_info();
    $smarty->display('currencies_list.htm');
}

function get_currencies()
{
    global $actg, $db;
    
    /* 排序参数 */
    $filter['sort_by']      = empty($_REQUEST['sort_by']) ? 'currency_code' : trim($_REQUEST['sort_by']);
    $filter['sort_order']   = empty($_REQUEST['sort_order']) ? 'ASC' : trim($_REQUEST['sort_order']);
    
    /* 查询数据的条件 */
    $where = " 1 ";
    
    $sql = "SELECT count(*) FROM `actg_currencies` as a WHERE " . $where;
    $filter['record_count'] = $db->getOne($sql);
    
    /* 分页大小 */
    //$filter = page_and_size($filter);
    $filter['page_size'] = $filter['record_count'];
    $filter['page'] = 1;
    $filter['page_count'] = 1;
    $filter['start'] = 0;
    
    $opt = array();
    $opt['orderby'] = $filter['sort_by'] . ' ' . $filter['sort_order'];

    /* 查询 */
    $data = $actg->getCurrencies($where, $opt);
    
    $arr = array(
        'data' => $data,
        'filter' => $filter,
        'page_count' => $filter['page_count'],
        'record_count' => $filter['record_count']
    );
    return $arr;
}

?>