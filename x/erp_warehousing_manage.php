<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_base.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_warehouse.php');
//require(dirname(__FILE__) .'/includes/ERP/cls/cls_date.php');
$erpController = new Yoho\cms\Controller\ErpController();
$adminuserController = new Yoho\cms\Controller\AdminuserController();
if($_REQUEST['act'] == 'list')
{
	if(admin_priv('erp_warehouse_view','',false) ||admin_priv('erp_warehouse_manage','',false) ||admin_priv('erp_warehouse_approve','',false))
	{
		// $page=isset($_REQUEST['page']) &&intval($_REQUEST['page']) >0 ?intval($_REQUEST['page']):1;
		// $warehouse_id=isset($_REQUEST['w_id']) &&intval($_REQUEST['w_id'])>0 ?intval($_REQUEST['w_id']):0;
		// $warehousing_style_id=isset($_REQUEST['w_s_id']) &&intval($_REQUEST['w_s_id'])>0?intval($_REQUEST['w_s_id']):0;
		// $warehousing_status=isset($_REQUEST['w_s']) &&intval($_REQUEST['w_s'])>0?intval($_REQUEST['w_s']):0;
		// $start_date=isset($_REQUEST['s_date'])?trim($_REQUEST['s_date']):'';
		// $end_date=isset($_REQUEST['e_date'])?trim($_REQUEST['e_date']):'';
		// $warehousing_from=isset($_REQUEST['w_f'])?trim($_REQUEST['w_f']):'';
		// $warehousing_sn=isset($_REQUEST['warehousing_sn'])?trim($_REQUEST['warehousing_sn']):'';
		// include('./includes/ERP/page.class.php');
		// $num_per_page=20;
		// $mode=1;
		// $page_bar_num=6;
		// $page_style="page_style";
		// $current_page_style="current_page_style";
		// $start=$num_per_page*($page-1);
		// $cls_date=new cls_date();
		// $start_time=!empty($start_date)?$cls_date->date_to_stamp($start_date): 0;
		// $end_time=!empty($end_date)?$cls_date->date_to_stamp($end_date):0;
		// $agency_id=get_admin_agency_id($_SESSION['admin_id']);
		// $paras=array(
		// 	'status'=>$warehousing_status,
		// 	'start_time'=>$start_time,
		// 	'end_time'=>$end_time,
		// 	'warehousing_style_id'=>$warehousing_style_id,
		// 	'warehouse_id'=>$warehouse_id,
		// 	'warehousing_from'=>$warehousing_from,
		// 	'warehousing_sn'=>$warehousing_sn,
		// 	'agency_id'=>$agency_id
		// );
		// $total_num=get_warehousing_count($paras);
		// $warehousing_list=get_warehousing_list($paras,$start,$num_per_page);
		// $smarty->assign('warehousing_list',$warehousing_list);
		// $url=fix_url($_SERVER['REQUEST_URI'],array(
		// 	'page'=>'',
		// 	'w_id'=>$warehouse_id,
		// 	'w_s_id'=>$warehousing_style_id,
		// 	'w_s'=>$warehousing_status,
		// 	's_date'=>$start_date,
		// 	'e_date'=>$end_date,
		// 	'w_f'=>$warehousing_from,
		// 	'sn'=>$warehousing_sn,
		// ));
		// $smarty->assign('w_id',$warehouse_id);
		// $smarty->assign('w_s_id',$warehousing_style_id);
		// $smarty->assign('w_s',$warehousing_status);
		// $smarty->assign('s_date',$start_date);
		// $smarty->assign('e_date',$end_date);
		// $smarty->assign('w_f',$warehousing_from);
		// $smarty->assign('sn',$warehousing_sn);
		// $pager=new page(array('total_data'=>$total_num,'data_per_page'=>$num_per_page,'url'=>$url,'mode'=>$mode,'page_bar_num'=>$page_bar_num,'page_style'=>$page_style,'current_page_style'=>$current_page_style));
		// $smarty->assign('pager',$pager->show());

		$warehousing_list = get_warehousing_listtable();
		$smarty->assign('warehousing_list', $warehousing_list['warehousing_list']);
		$smarty->assign('filter',           $warehousing_list['filter']);
		$smarty->assign('record_count',     $warehousing_list['record_count']);
		$smarty->assign('page_count',       $warehousing_list['page_count']);
		$smarty->assign('w_id',             empty($_REQUEST['w_id']) ? 0 : intval($_REQUEST['w_id']));
		$smarty->assign('w_s_id',           empty($_REQUEST['w_s_id']) ? 0 : intval($_REQUEST['w_s_id']));
		$smarty->assign('w_s',              empty($_REQUEST['w_s']) ? 0 : intval($_REQUEST['w_s']));
		$smarty->assign('s_date',           empty($_REQUEST['s_date']) ? '' : trim($_REQUEST['s_date']));
		$smarty->assign('e_date',           empty($_REQUEST['e_date']) ? '' : trim($_REQUEST['e_date']));
		$smarty->assign('w_f',              empty($_REQUEST['w_f']) ? '' : trim($_REQUEST['w_f']));
		$smarty->assign('sn',               empty($_REQUEST['warehousing_sn']) ? '' : trim($_REQUEST['warehousing_sn']));
		$smarty->assign('full_page',        1);
		
		$warehouse_list=get_warehouse_list(0,$agency_id,1);
		$smarty->assign('warehouse_list',$warehouse_list);
		$warehousing_style_list=get_warehousing_style_list(1);
		$smarty->assign('warehousing_style_list',$warehousing_style_list);
		$warehousing_status=get_warehousing_status();
		$smarty->assign('warehousing_status',$warehousing_status);
		if((admin_priv('erp_warehouse_manage','',false)))
		{
			//$action_link = array('href'=>'javascript: add_warehousing();','text'=>$_LANG['erp_goods_warehousing']);
			$action_link = array('href'=>'erp_warehousing_manage.php?act=create','text'=>$_LANG['erp_goods_warehousing']);
			$smarty->assign('action_link',$action_link);
		}
		$smarty->assign('ur_here',$_LANG['erp_goods_warehousing_list']);
		assign_query_info();
		$smarty->display('erp_warehousing_list.htm');
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif ($_REQUEST['act'] == 'query')
{
	if(admin_priv('erp_warehouse_view','',false) ||admin_priv('erp_warehouse_manage','',false) ||admin_priv('erp_warehouse_approve','',false))
	{
		if ((!empty($_REQUEST['s_date'])) && (strstr($_REQUEST['s_date'], '-') === false))
	    {
	        $_REQUEST['s_date'] = local_date('Y-m-d', $_REQUEST['s_date']);
		}
		if ((!empty($_REQUEST['e_date'])) && (strstr($_REQUEST['e_date'], '-') === false))
		{
	        $_REQUEST['e_date'] = local_date('Y-m-d', $_REQUEST['e_date']);
	    }
		
		$warehousing_list = get_warehousing_listtable();
		$smarty->assign('warehousing_list', $warehousing_list['warehousing_list']);
		$smarty->assign('filter',           $warehousing_list['filter']);
		$smarty->assign('record_count',     $warehousing_list['record_count']);
		$smarty->assign('page_count',       $warehousing_list['page_count']);
		$smarty->assign('w_id',             empty($_REQUEST['w_id']) ? 0 : intval($_REQUEST['w_id']));
		$smarty->assign('w_s_id',           empty($_REQUEST['w_s_id']) ? 0 : intval($_REQUEST['w_s_id']));
		$smarty->assign('w_s',              empty($_REQUEST['w_s']) ? 0 : intval($_REQUEST['w_s']));
		$smarty->assign('s_date',           empty($_REQUEST['s_date']) ? '' : trim($_REQUEST['s_date']));
		$smarty->assign('e_date',           empty($_REQUEST['e_date']) ? '' : trim($_REQUEST['e_date']));
		$smarty->assign('w_f',              empty($_REQUEST['w_f']) ? '' : trim($_REQUEST['w_f']));
		$smarty->assign('sn',               empty($_REQUEST['warehousing_sn']) ? '' : trim($_REQUEST['warehousing_sn']));
		
    	make_json_result($smarty->fetch('erp_warehousing_list.htm'), '', array('filter' => $warehousing_list['filter'], 'page_count' => $warehousing_list['page_count']));
	}
	else
	{
		make_json_error($_LANG['erp_no_permit']);
	}
}
elseif($_REQUEST['act'] == 'view')
{
	if(admin_priv('erp_warehouse_view','',false) ||admin_priv('erp_warehouse_manage','',false) ||admin_priv('erp_warehouse_approve','',false))
	{
		$warehousing_id=isset($_REQUEST['id']) &&intval($_REQUEST['id'])?intval($_REQUEST['id']) : 0;
		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		if(!is_agency_warehousing($warehousing_id,$agency_id))
		{
			$href="erp_warehousing_manage.php?act=list";
			$text=$_LANG['erp_warehousing_return_to_warehousing_list'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_warehousing_no_agency_access'],0,$link);
		}
		$warehousing_info=get_warehousing_info($warehousing_id);

		$warehousing_item_info=get_warehousing_item_info($warehousing_id);
		$smarty->assign('warehousing_info',$warehousing_info);
		$smarty->assign('warehousing_item_info',$warehousing_item_info);
		$smarty->assign('act','view');
		//if((admin_priv('erp_warehouse_approve','',false)))
		if ($_SESSION['manage_cost'])
		{
			$smarty->assign('price_visible','true');
		}
		$smarty->assign('ur_here',$_LANG['erp_warehousing_view_warehousing']);
		assign_query_info();

		// get cross check history
		$cross_check_history = $erpController->get_erp_order_cross_check_history($warehousing_id);
		$smarty->assign('cross_check_history',$cross_check_history);

		$smarty->display('erp_warehousing_info.htm');
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif ($_REQUEST['act'] == 'edit' || $_REQUEST['act']=='add_rma_from_po')
{
	if((admin_priv('erp_warehouse_manage','',false)))
	{
		if($_REQUEST['act']=='add_rma_from_po'){
			$warehousing_id = add_warehousing();
			$tmpWarehouseId = null;
			foreach ($erpController->getWarehouseList() as $w) {
				if($w['is_on_sale']!='1'){
					$tmpWarehouseId=$w['warehouse_id'];
					break;
				}
			}
			if($tmpWarehouseId<=0){
				$db->query("DELETE FROM ".$ecs->table('erp_warehousing')." WHERE warehousing_id=".$warehousing_id);
				$href="erp_warehousing_manage.php?act=list";
				$text=$_LANG['erp_warehousing_return_to_warehousing_list'];
				$link[] = array('href'=>$href,'text'=>$text);
				sys_msg("找不到非銷售貨倉作暫存倉, 無法完成操作",1,$link);
			}

			$sql="UPDATE ".$ecs->table('erp_warehousing')." SET warehouse_id=".$tmpWarehouseId." ,warehousing_style_id ='".$erpController->getWarehousingStyleIdByName($erpController::DB_WAREHOUSING_STYLE_REPAIRANDSTOCK)."' WHERE warehousing_id='".$warehousing_id."'";
			$db->query($sql);
		}else{
			$warehousing_id=isset($_REQUEST['id']) &&intval($_REQUEST['id'])?intval($_REQUEST['id']) : 0;	
		}

		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		if(!is_agency_warehousing($warehousing_id,$agency_id))
		{
			$href="erp_warehousing_manage.php?act=list";
			$text=$_LANG['erp_warehousing_return_to_warehousing_list'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_warehousing_no_agency_access'],0,$link);
		}
		if(!is_admin_warehousing($warehousing_id,erp_get_admin_id()))
		{
			$href="erp_warehousing_manage.php?act=list";
			$text=$_LANG['erp_order_return'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_warehousing_no_access'],0,$link);
		}
		if(lock_table($warehousing_id,'warehousing','edit'))
		{
			$warehousing_info=get_warehousing_info($warehousing_id);
			$warehousing_item_info=get_warehousing_item_info($warehousing_id);
			$warehouse_list=get_warehouse_list(erp_get_admin_id(),$agency_id,1);
			$warehousing_style_list=get_warehousing_style_list(1);
			if($warehousing_info['warehousing_style_id']==1)
			{
				$order_info=get_order_not_warehoused();
				$smarty->assign('order_info',$order_info);
			}elseif($warehousing_info['warehousing_style_id']==$erpController->getWarehousingStyleIdByName($erpController::DB_WAREHOUSING_STYLE_REPAIRANDSTOCK)){
				$smarty->assign('supplier_list',supplier_list());
			}
			$smarty->assign('warehousing_info',$warehousing_info);
			$smarty->assign('warehousing_item_info',$warehousing_item_info);
			$smarty->assign('warehouse_list',$warehouse_list);
			$smarty->assign('warehousing_style_list',$warehousing_style_list);
			//$salespeople = $db->getCol("SELECT `sales_name` FROM " . $ecs->table('salesperson') . " WHERE available = 1");
			//$salespeople = $db->getCol("SELECT user_name as sales_name FROM ".$ecs->table('admin_user')." WHERE user_id=".$_SESSION['admin_id']);
			$salespeople = $adminuserController->getAdminsByPriv(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_WAREHOUSE_OP, 'name');
			$smarty->assign('salesperson_list', $salespeople);
			$smarty->assign('act','edit');
			//if(admin_priv('erp_warehouse_approve','',false))
			if ($_SESSION['manage_cost'])
			{
				$smarty->assign('price_visible','true');
			}
			$smarty->assign('ur_here',$_LANG['erp_warehousing_edit_warehousing']);
			assign_query_info();
			$smarty->display('erp_warehousing_info.htm');
		}
		else
		{
			$href="erp_warehousing_manage.php?act=list";
			$text=$_LANG['erp_warehousing_return_to_warehousing_list'];
			$link[] = array('href'=>$href,'text'=>$text);
			$href="erp_warehousing_manage.php?act=view&id=".$warehousing_id;
			$text=$_LANG['erp_warehousing_view_warehousing'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_warehousing_no_accessibility'],0,$link);
		}
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif ($_REQUEST['act'] == 'create')
{
	if((admin_priv('erp_warehouse_manage','',false)))
	{
		$warehousing_id= 0;
		
		$smarty->assign('operator',$_SESSION['admin_id']); 
		$agency_id = get_admin_agency_id($_SESSION['admin_id']);
		
		$order_id = $_REQUEST['order_id'];
		
		if(!empty($order_id)) {
            $erpOrderInfo = $erpController->get_erp_order_info($order_id);
		}
		$smarty->assign('order_sn',$erpOrderInfo['order_sn']);

		$warehousing_style_id = $_REQUEST['warehousing_style_id'];
		$warehousing_info['warehousing_sn'] = '-';

		if ($erpOrderInfo['warehouse_id'] && !empty($order_id)) {
			$warehousing_info['warehouse_id'] = $erpOrderInfo['warehouse_id'];
		} else {
			$warehousing_info['warehouse_id'] = '';
		}
		
		$admin_info = get_admin(erp_get_admin_id());
		$warehousing_info['operator'] = $admin_info['user_name'];

		if ($_REQUEST['order_id'] != '' && $warehousing_style_id == 1) {
			$warehousing_info['warehousing_style_id'] = 1;
			$warehousing_info['order_id'] = $order_id;
		}else{
			$warehousing_info['warehousing_style_id'] = $warehousing_style_id;
		}

		$warehousing_info['warehousing_date'] = local_date('Y-m-d');	

		if(admin_priv('erp_warehouse_approve','',false)){
			$warehousing_info['available_act']['approve'] = 1;
		}else{
			$warehousing_info['available_act']['approve'] = 0;
		}

		if(admin_priv('erp_warehouse_approve','',false) || admin_priv('erp_warehouse_manage','',false)){
			$warehousing_info['available_act']['edit'] = 1;
			$warehousing_info['available_act']['delete'] = 1;
		}

		if ($warehousing_style_id == 1 && $order_id != '') {
			$warehousing_item_info=get_warehousing_item_info_by_order($order_id);
		}

		$warehouse_list=get_warehouse_list(erp_get_admin_id(),$agency_id,1);
		if($_REQUEST['warehousing_style_id'] && $_REQUEST['order_id']){
			$warehousing_style_list=get_warehousing_style_list(1,-1,-1,"1,4");
		} else {
			$warehousing_style_list=get_warehousing_style_list(1,-1,-1,"4");
		}
		if($warehousing_info['warehousing_style_id']==1)
		{
			$order_info=get_order_not_warehoused();
			$smarty->assign('order_info',$order_info);
		}elseif($warehousing_info['warehousing_style_id']==$erpController->getWarehousingStyleIdByName($erpController::DB_WAREHOUSING_STYLE_REPAIRANDSTOCK)){
			$smarty->assign('supplier_list',supplier_list());
		}
		$smarty->assign('warehousing_info',$warehousing_info);
		$smarty->assign('warehousing_item_info',$warehousing_item_info);
		$smarty->assign('warehouse_list',$warehouse_list);
		$smarty->assign('warehousing_style_list',$warehousing_style_list);

		if (empty($order_id)) {
			$order_id = 0;
		}
		$smarty->assign('order_id',$order_id);
		//$salespeople = $db->getCol("SELECT `sales_name` FROM " . $ecs->table('salesperson') . " WHERE available = 1");
		//$salespeople = $db->getCol("SELECT user_name as sales_name FROM ".$ecs->table('admin_user')." WHERE user_id=".$_SESSION['admin_id']);
		$salespeople = $adminuserController->getAdminsByPriv(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_WAREHOUSE_OP, 'name');
	
		$smarty->assign('salesperson_list', $salespeople);
		$smarty->assign('act','create');
		
		if ($_SESSION['manage_cost'])
		{
			$smarty->assign('price_visible','true');
		}

	

		$smarty->assign('ur_here',$_LANG['erp_warehousing_edit_warehousing']);
		assign_query_info();
		$smarty->display('erp_warehousing_create.htm');
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif ($_REQUEST['act'] == 'approve')
{
	if((admin_priv('erp_warehouse_approve','',false)))
	{
		$warehousing_id=isset($_REQUEST['id']) &&intval($_REQUEST['id'])>0?intval($_REQUEST['id']):0;
		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		if(!is_agency_warehousing($warehousing_id,$agency_id))
		{
			$href="erp_warehousing_manage.php?act=list";
			$text=$_LANG['erp_warehousing_return_to_warehousing_list'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_warehousing_no_agency_access'],0,$link);
		}
		if(lock_table($warehousing_id,'warehousing','approve'))
		{
			$warehousing_info=get_warehousing_info($warehousing_id);
			$warehousing_item_info=get_warehousing_item_info($warehousing_id);
			$smarty->assign('warehousing_info',$warehousing_info);
			$smarty->assign('warehousing_item_info',$warehousing_item_info);
			$smarty->assign('act','approve');
			//if((admin_priv('erp_warehouse_approve','',false)))
			if ($_SESSION['manage_cost'])
			{
				$smarty->assign('price_visible','true');
			}
			$smarty->assign('ur_here',$_LANG['erp_warehousing_approve_warehousing']);
			assign_query_info();
			$smarty->display('erp_warehousing_info.htm');
		}
		else
		{
			$href="erp_warehousing_manage.php?act=list";
			$text=$_LANG['erp_warehousing_return_to_warehousing_list'];
			$link[] = array('href'=>$href,'text'=>$text);
			$href="erp_warehousing_manage.php?act=view&id=".$warehousing_id;
			$text=$_LANG['erp_warehousing_view_warehousing'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_warehousing_no_accessibility'],0,$link);
		}
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif ($_REQUEST['act'] == 'print')
{
	if(admin_priv('erp_warehouse_view','',false) ||admin_priv('erp_warehouse_manage','',false) ||admin_priv('erp_warehouse_approve','',false))
	{
		$warehousing_id=isset($_REQUEST['id']) &&intval($_REQUEST['id'])?intval($_REQUEST['id']) : 0;
		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		if(!is_agency_warehousing($warehousing_id,$agency_id))
		{
			$href="erp_warehousing_manage.php?act=list";
			$text=$_LANG['erp_warehousing_return_to_warehousing_list'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_warehousing_no_agency_access'],0,$link);
		}
		$warehousing_info=get_warehousing_info($warehousing_id);
		$warehousing_item_info=get_warehousing_item_info($warehousing_id);
		$smarty->assign('warehousing_info',$warehousing_info);
		$smarty->assign('warehousing_item_info',$warehousing_item_info);
		$smarty->assign('act','view');
		assign_query_info();
		$smarty->display('erp_warehousing_print.htm');
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif ($_REQUEST['act'] == 'post_po_stock_in_import_data')
{
	$result = $erpController->po_stock_in_import_data();
    make_json_result($result);
}
elseif ($_REQUEST['act'] == 'post_get_goods_info')
{
	$goods_id = $_REQUEST['goods_id'];
	$result = $erpController->get_goods_info($goods_id);
    make_json_result($result);
}
elseif ($_REQUEST['act'] == 'post_save_goods_info')
{
	$goods_id = $_REQUEST['goods_id'];
	$result = $erpController->save_goods_info();
    make_json_result($result);
}
elseif ($_REQUEST['act'] == 'post_erp_order_cross_check_history')
{
	$result = $erpController->save_erp_order_cross_check_history();
    make_json_result($result);
}
elseif ($_REQUEST['act'] == 'post_update_erp_order_cross_check_history')
{
	$result = $erpController->update_erp_order_cross_check_history();
    make_json_result($result);
}
function get_warehousing_listtable()
{
	$warehouse_id = isset($_REQUEST['w_id']) &&intval($_REQUEST['w_id'])>0 ?intval($_REQUEST['w_id']):0;
	$warehousing_style_id = isset($_REQUEST['w_s_id']) &&intval($_REQUEST['w_s_id'])>0 ?intval($_REQUEST['w_s_id']):0;
	$warehousing_status = isset($_REQUEST['w_s']) &&intval($_REQUEST['w_s'])>0 ?intval($_REQUEST['w_s']):0;
	$s_date=isset($_REQUEST['s_date'])?trim($_REQUEST['s_date']):'';
	$e_date=isset($_REQUEST['e_date'])?trim($_REQUEST['e_date']):'';
	$warehousing_from=isset($_REQUEST['w_f'])?trim($_REQUEST['w_f']):'';
	$warehousing_sn=isset($_REQUEST['warehousing_sn'])?trim($_REQUEST['warehousing_sn']):'';
	$order_id=isset($_REQUEST['order_id'])?trim($_REQUEST['order_id']):'';
	$start_time=!empty($s_date)?local_strtotime($s_date): 0;
	$end_time=!empty($e_date)?local_strtotime($e_date):0;
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	
	$filter = array(
		// Search parameters
		'status' => $warehousing_status,
		'start_time' => $start_time,
		'end_time' => $end_time,
		'warehousing_style_id' => $warehousing_style_id,
		'warehouse_id' => $warehouse_id,
		'warehousing_from' => $warehousing_from,
		'warehousing_sn' => $warehousing_sn,
		'order_id' => $order_id,
		'agency_id' => $agency_id,
		// Parameters with different name
		'w_id' => $warehouse_id,
		'w_s_id' => $warehousing_style_id,
		'w_s' => $warehousing_status,
		's_date' => $s_date,
		'e_date' => $e_date,
		'w_f' => $warehousing_from
	);
	
	$filter['record_count'] = get_warehousing_count($filter);
    
    /* 分页大小 */
    $filter = page_and_size($filter);
	
	$warehousing_list = get_warehousing_list($filter, $filter['start'], $filter['page_size']);
	
	$arr = array(
        'warehousing_list' => $warehousing_list,
        'filter' => $filter,
        'page_count' => $filter['page_count'],
        'record_count' => $filter['record_count']
    );
    return $arr;
}
?>