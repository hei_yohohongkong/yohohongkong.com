<?php

define('IN_ECS',true);
require_once(dirname(__FILE__) .'/includes/init.php');
require_once(dirname(__FILE__) .'/includes/ERP/lib_erp_base.php');
require_once(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require_once(dirname(__FILE__) .'/includes/ERP/lib_erp_goods.php');
require_once(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
require_once(dirname(__FILE__) .'/includes/ERP/lib_erp_warehouse.php');
require_once(dirname(__FILE__) .'/includes/ERP/cls/cls_date.php');
$warehouseTransferController = new Yoho\cms\Controller\WarehouseTransferController();
$erpController = new Yoho\cms\Controller\ErpController();
$adminuserController = new Yoho\cms\Controller\AdminuserController();

if ($_REQUEST['act'] == 'list') {

	if (empty($_REQUEST['transfer_id'])) {
		header("Location: erp_warehouse_transfer.php?act=list");
		die();
	}
    
	$warehouseTransferController->checkStatusRedirect($_REQUEST['transfer_id'], 'create');
	$transfer_info = $warehouseTransferController->getTransferInfo($_REQUEST['transfer_id']);
	
	if ($transfer_info['reason_type'] == $warehouseTransferController::REASON_TYPE_NORMAL || $transfer_info['reason_type'] == $warehouseTransferController::REASON_TYPE_SHOP_RESTOCK || $transfer_info['reason_type'] == $warehouseTransferController::REASON_TYPE_ORDER_OUT_OF_STOCK || $transfer_info['reason_type'] == $warehouseTransferController::REASON_TYPE_STORE_PICK_UP || $transfer_info['reason_type'] == $warehouseTransferController::REASON_TYPE_OTHERS ) {
		$warehouse_list = $erpController->getWarehouseList(erp_get_admin_id(),0);
	} else if ($transfer_info['reason_type'] == $warehouseTransferController::REASON_TYPE_BAD_GOODS_TO_REPAIR_CENTER || $transfer_info['reason_type'] == $warehouseTransferController::REASON_TYPE_BAD_GOODS_TO_SHOP) {
		$warehouse_list = $erpController->getWarehouseList(erp_get_admin_id(),-1,2);
	}


	$reason_type_options = $warehouseTransferController->getReasonTypeOptions();
	
	$smarty->assign('step', 1);
	$smarty->assign('transfer_info', $transfer_info);
	$smarty->assign('transfer_id', $_REQUEST['transfer_id']);
    $smarty->assign('warehouse_list',$warehouse_list);
	$smarty->assign('reason_type_options',$reason_type_options);
    $smarty->assign('brand_list',$result['brand_list']);
    $smarty->assign('ur_here', '新增調貨');
    $smarty->assign('full_page', 1);
    $smarty->assign('cfg_lang', $_CFG['lang']);
    $smarty->assign('lang', $_LANG);
    $smarty->assign('action_link', array('text' => '調貨清單', 'href' => 'erp_warehouse_transfer.php'));

    assign_query_info();
    $smarty->display('erp_warehouse_transfer_create.htm');

} else if ($_REQUEST['act'] == 'post_search_product_by_sku') {
    $result = $warehouseTransferController->searchProductBySku();
    if(!isset($result['error'])){
        make_json_result($result);
    }else{
     	make_json_error($result['error']);
    }
} elseif ($_REQUEST['act'] == 'query') {
    $result = $warehouseTransferController->getTransferProducts();
    if ($result != false){
        $warehouseTransferController->ajaxQueryAction($result['data'],$result['record_count'],false,$result['extra_data']);
    }else{
        $result['data'] = array();
        $result['record_count'] = 0;
        $result['extra_data'] = array();
        $stocktakeController->ajaxQueryAction($result['data'],$result['record_count'],false,$result['extra_data']);
    }
} elseif ($_REQUEST['act'] == 'post_transfer_item_delete') {
    $result = $warehouseTransferController->transferItemDelete();
	if(isset($result['error'])){
		make_json_error($result['error']);
	}else{
		make_json_result($result);
	}
} elseif ($_REQUEST['act'] == 'post_transfer_info_change') {
    $result = $warehouseTransferController->transferInfoChange();
	if(isset($result['error'])){
		make_json_error($result['error']);
	}else{
		make_json_result($result);
	}
} elseif ($_REQUEST['act'] == 'post_transfer_info_change_reason_type') {
    $result = $warehouseTransferController->transferInfoChangeReasonType();
	if(isset($result['error'])){
		make_json_error($result['error']);
	}else{
		make_json_result($result);
	}
} elseif ($_REQUEST['act'] == 'post_transfer_qty_update') {
    $result = $warehouseTransferController->transferQtyUpdate();
	if(isset($result['error'])){
		make_json_error($result['error']);
	}else{
		make_json_result($result);
	}
} elseif ($_REQUEST['act'] == 'post_requested_qty_update') {
    $result = $warehouseTransferController->requestedQtyUpdate();
	if(isset($result['error'])){
		make_json_error($result['error']);
	}else{
		make_json_result($result);
	}
} elseif ($_REQUEST['act'] == 'post_transfer_qty_check') {
    $result = $warehouseTransferController->transferQtyCheck();
	if(isset($result['error'])){
		make_json_error(implode('<br>',$result['error']));
	}else{
		make_json_result($result);
	}
}
?>