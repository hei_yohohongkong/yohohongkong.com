<?php

define('IN_ECS',true);

require(dirname(__FILE__) .'/includes/init.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_base.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_warehouse.php');
require(dirname(__FILE__) .'/includes/ERP/cls/cls_date.php');
$erpController = new Yoho\cms\Controller\ErpController();
if ($_REQUEST['act'] == 'list' || $_REQUEST['act'] == 'print' || $_REQUEST['act'] == 'export')
{
	/* 检查权限 */
	if ((!admin_priv('erp_warehouse_view',    '', false)) && 
		(!admin_priv('erp_warehouse_manage',  '', false)) && 
		(!admin_priv('erp_warehouse_approve', '', false)))
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href, 'text'=>$text);
		sys_msg($_LANG['erp_no_permit'], 0, $link);
	}
	
	$page = isset($_REQUEST['page']) && intval($_REQUEST['page']) > 0 ? intval($_REQUEST['page']) : 1;
	$goods_sn = isset($_REQUEST['goods_sn']) ? trim($_REQUEST['goods_sn']) : '';
	$goods_name = isset($_REQUEST['goods_name']) ? trim($_REQUEST['goods_name']) : '';
	$supplier_id = isset($_REQUEST['supplier_id']) ? trim($_REQUEST['supplier_id']) : '';
	$goods_barcode = isset($_REQUEST['goods_barcode']) ? trim($_REQUEST['goods_barcode']) : '';
	$warehouse_id = isset($_REQUEST['warehouse_id']) && intval($_REQUEST['warehouse_id']) > 0 ? intval($_REQUEST['warehouse_id']) : 0;
	include('./includes/ERP/page.class.php');
	$num_per_page = 10;
	$mode = 1;
	$page_bar_num = 6;
	$page_style = "page_style";
	$current_page_style = "current_page_style";
	$start = $num_per_page * ($page - 1);
	$agency_id = get_admin_agency_id($_SESSION['admin_id']);
	$paras = array(
		'goods_sn' => $goods_sn,
		'goods_name' => $goods_name,
		'goods_barcode' => $goods_barcode,
		'warehouse_id' => $warehouse_id,
		'supplier_id'=>$supplier_id,
		'agency_id' => $agency_id
	);
	if(isset($_REQUEST['order_by'])) $paras['orderby']=intval($_REQUEST['order_by']);
	if(isset($_REQUEST['order'])) $paras['order']=$_REQUEST['order'];

	$total_num = get_erp_stock_count($paras);
	if ($_REQUEST['act'] == 'list')
	{
		$stock_info=get_erp_stock($paras, $start, $num_per_page);
	}

	elseif ($_REQUEST['act'] == 'print')
	{
		$stock_info=get_erp_stock2($paras, $start, $num_per_page);
	}
	else //if($_REQUEST['act'] == 'export')
	{
		$stock_info=get_erp_stock2($paras);
	}
	$smarty->assign('stock_info',$stock_info);
	$url = fix_url($_SERVER['REQUEST_URI'], array(
		'page' => '',
		'goods_sn' => $goods_sn,
		'goods_name' => $goods_name,
		'goods_barcode' => $goods_barcode,
		'warehouse_id' => $warehouse_id
	));
	$pager = new page(array(
		'total_data' => $total_num,
		'data_per_page' => $num_per_page,
		'url' => $url,
		'mode' => $mode,
		'page_bar_num' => $page_bar_num,
		'page_style' => $page_style,
		'current_page_style' => $current_page_style
	));
	$smarty->assign('pager', $pager->show());
	$warehouse_list = get_warehouse_list(0, $agency_id, 1,-1,-1,'asc');

	$smarty->assign('warehouse_list', $warehouse_list);
	$smarty->assign('warehouse_options', $warehouse_list);
	$smarty->assign('warehouse_id', $warehouse_id);
	$smarty->assign('goods_sn', $goods_sn);
	$smarty->assign('goods_name', $goods_name);
	$smarty->assign('goods_barcode', $goods_barcode);
	$smarty->assign('page', $page);
	$smarty->assign('ur_here', $_LANG['erp_stock_inquiry']);
	$smarty->assign('supplier_list',suppliers_list_name_by_admin());
	$smarty->assign('supplier_id',$supplier_id);
	if ($_REQUEST['act'] == 'list'||$_REQUEST['act'] == 'print')
	{
		assign_query_info();
		$template = ($_REQUEST['act'] == 'list') ? 'erp_stock_inquiry.htm' : 'erp_stock_inquiry_print.htm';
		$smarty->display($template);
	}
	else //if($_REQUEST['act'] == 'export')
	{
		require(dirname(__FILE__) .'/includes/ERP/cls/cls_csv.php');
		require(dirname(__FILE__) .'/includes/ERP/cls/cls_file.php');
		include(ROOT_PATH.'/includes/cls_json.php');
		$export = array();
		$export[] = array(
			'stock_id'       => $_LANG['erp_stock_id'],
			'stock_goods_id' => $_LANG['erp_stock_goods_id'],
			'goods_sn'       => $_LANG['erp_stock_goods_sn'],
			'goods_name'     => $_LANG['erp_stock_goods_name'],
			'goods_barcode'  => $_LANG['erp_stock_goods_barcode'],
			'warehouse_name' => $_LANG['erp_stock_warehouse_name'],
			'stock_agency'   => $_LANG['erp_stock_agency'],
			'stock_attr'     => $_LANG['erp_stock_attr'],
			'stock_qty'      => $_LANG['erp_stock_qty']
		);
		if (!empty($stock_info))
		{
			foreach ($stock_info as $k => $v)
			{
				$row = array(
					'stock_id' => $v['id'],
					'stock_goods_id' => $v['goods_id'],
					'goods_sn' => $v['goods_info']['goods_sn'],
					'goods_name' => $v['goods_info']['goods_name'],
					'goods_barcode' => $v['barcode'],
					'warehouse_name' => $v['warehouse_name'],
					'stock_agency' => $v['agency_name']
				);
				if (isset($v['attr_value']['attr_info']))
				{
					$attr = '';
					foreach($v['attr_value']['attr_info'] as $item)
					{
						$attr.=$item['attr_value'].' ';
					}
					$row['stock_attr'] = empty($attr) ? '$$' : $attr;
				}
				else
				{
					$row['stock_attr'] = '$$';
				}
				$row['stock_qty'] = $v['goods_qty'];
				$export[] = $row;
			}
		}
		$csv = new cls_csv();
		$file_stream = $csv->encode($export);
		$file_stream = iconv('utf-8','gb2312',$file_stream);
		$file = new cls_file();
		$path = ROOT_PATH.'data/stock_inquiry/';
		$random_name = random_string('numeric',8);
		$file_name = date('Y-m-d').'-'.$random_name.'.csv';
		while (file_exists($path.$file_name))
		{
			$random_name = random_string('numeric',8);
			$file_name = date('Y-m-d').'-'.$random_name.'.csv';
		}
		$file->write_file($path,$file_name,$file_stream,$op='rewrite');
		$json = new JSON;
		$server_address = $_SERVER["SERVER_NAME"];
		$phpself = $_SERVER["PHP_SELF"];
		$port = $_SERVER["SERVER_PORT"];
		$path = str_replace(ADMIN_PATH.'/erp_stock_inquiry.php','',$phpself);
		$base_url = $server_address.':'.$port.$path;
		$url = 'http://'.$base_url.'data/stock_inquiry/'.$file_name;
		$result['error'] = 0;
		$result['url'] = $url;
		die($json->encode($result));
	}
}elseif($_REQUEST['act']=='warehouse_stock_detail'){
	$wId=intval($_REQUEST['wid']);
	$pAttrId=0;
	$pId=intval($_REQUEST['pid']);
	$agency_id = get_admin_agency_id($_SESSION['admin_id']);
	$warehouse_list = get_warehouse_list(0, $agency_id, 1,-1,-1,'asc');

	unset($warehouse_list[$wId]);

	$smarty->assign('warehouse_options', $warehouse_list);
	$supplier_list = $erpController->getSupplierByProductId($pId);

	$smarty->assign('supplier_options', $supplier_list);
	foreach ($erpController->getWarehouseList() as $v) {
		if($v['warehouse_id']==$wId){
			$wInfo=$v;
			break;
		}
	}
	$productInfo=get_erp_stock(['goods_id'=>$pId,'warehouse_id'=>$wId], 0,10);
	/*paging*/
	include('./includes/ERP/page.class.php');

	$pageA = isset($_REQUEST['pagea']) && intval($_REQUEST['pagea']) > 0 ? intval($_REQUEST['pagea']) : 1;
	$pageB = isset($_REQUEST['pageb']) && intval($_REQUEST['pageb']) > 0 ? intval($_REQUEST['pageb']) : 1;
	$num_per_page = 10;
	$page_bar_num = 6;
	$page_style = "page_style";
	$current_page_style = "current_page_style";
	$startA = $num_per_page * ($pageA - 1);
	$startB = $num_per_page * ($pageB - 1);
	$pagerA = new page(array(
		'total_data' => get_stock_record_count(['goods_sn'=>$productInfo[$pId]['goods_info']['goods_sn']]),//$total_num,
		'data_per_page' => $num_per_page,
		'url' => $_SERVER['REQUEST_URI'],
		'page_name'=>'pagea',
		'mode' => $mode,
		'page_bar_num' => $page_bar_num,
		'page_style' => $page_style,
		'current_page_style' => $current_page_style
	));

	$pagerB = new page(array(
		'total_data' => 100,//$total_num,
		'data_per_page' => $num_per_page,
		'url' => $startA, $num_per_page,
		'page_name'=>'pageb',
		'mode' => $mode,
		'page_bar_num' => $page_bar_num,
		'page_style' => $page_style,
		'current_page_style' => $current_page_style
	));
	$smarty->assign('pagerA', $pagerA->show());
	$smarty->assign('pagerB', $pagerB->show());
	$smarty->assign('pageA', $pageA);
	$smarty->assign('pageB', $pageB);
	/*-- paging --*/

	
	$repairOrders = $GLOBALS['db']->getAll("SELECT ro.repair_sn, FROM_UNIXTIME(ro.add_time) as add_time, oi.order_sn as order_sn,ro.order_id,IF(ro.repair_order_status=0,1,ro.repair_order_status) as status,ro.operator,ro.remark,ro.operator2,ro.remark2,FROM_UNIXTIME(ro.pickup_time) as pickup_time FROM ".$GLOBALS['ecs']->table('repair_orders')." ro LEFT JOIN ".$GLOBALS['ecs']->table('order_info')." oi ON oi.order_id=ro.order_id LEFT JOIN ".$GLOBALS['ecs']->table('erp_stock')." es ON BINARY es.order_sn=BINARY ro.repair_sn WHERE ro.goods_id=".$pId." AND ro.is_deleted=0 AND ro.repair_order_status<>".Yoho\cms\Controller\OrderController::DB_REPAIR_ORDER_STATUS_RESOLVED." AND es.warehouse_id=".$wId." ORDER by repair_id DESC LIMIT ".$startB.','.$num_per_page);

	$smarty->assign('actualSellable',$erpController->getSellableProductQty($pId));
	$smarty->assign('repairOrders',$repairOrders);
	$smarty->assign('repairOrderStatus',Yoho\cms\Controller\OrderController::DB_REPAIR_ORDER_STATUS);
	$smarty->assign('productInfo',$productInfo);
	$smarty->assign('warehouseInfo',$wInfo);

	$stockRecords = get_stock_record(['goods_sn'=>$productInfo[$pId]['goods_info']['goods_sn']],$startA, $num_per_page);
	$stockRecordsByTypes = get_stock_record_count_type(['goods_sn'=>$productInfo[$pId]['goods_info']['goods_sn'],'warehouse_id'=>$wId]);
	$smarty->assign('stockRecordsByTypes',$stockRecordsByTypes);
	$smarty->assign('stockRecords',$stockRecords);

	$smarty->assign('ur_here', $_LANG['erp_stock_inquiry']." > ".$wInfo['name']."(".($wInfo['is_on_sale']==false?'不':'')."可銷售庫存) - ".$productInfo[$pId]['goods_info']['goods_name']);
	$smarty->assign('action_link',['text'=>'返回 即時存貨查詢 列表','href'=>'erp_stock_inquiry.php?act=list&goods_sn='.$productInfo[$pId]['goods_info']['goods_sn'].'&goods_name=']);
	$smarty->display('erp_stock_inquiry_warehouse.htm');
		
}elseif($_REQUEST['act']=='rma_to_warehouse'){
	if(!is_numeric($_POST['val']))
		make_json_result(null,'請選擇貨倉',['error'=>1]);
	preg_match('/fw:(.*)_p:(.*)/', $_POST['id'], $ids);
	$fromWId=$ids[1];
	$pId=$ids[2];
	$toWId=$_POST['val'];
	$pName = $GLOBALS['db']->getOne("SELECT goods_name FROM ".$GLOBALS['ecs']->table('goods')." WHERE goods_id=".$pId);
	$wNames = $GLOBALS['db']->getAll("SELECT fw.name as fwname,tw.name as twname FROM ".$GLOBALS['ecs']->table('erp_warehouse')." fw LEFT JOIN ".$GLOBALS['ecs']->table('erp_warehouse')." tw ON tw.warehouse_id=".$toWId." WHERE fw.warehouse_id=".$fromWId);
	make_json_result('RMA轉倉至'.$matches[0].'<!--'.time().'-->',null,['fwid'=>$fromWId,'twid'=>$toWId,'pid'=>$pId,'pname'=>$pName,'twname'=>$wNames[0]['twname'],'fwname'=>$wNames[0]['fwname']]);
}elseif($_REQUEST['act']=='rma_to_warehouse_proceed'){
	$errmsg = $erpController->stockTransfer($_REQUEST['fwid'],$_REQUEST['twid'],[$_REQUEST['pid']=>$_REQUEST['qty']]);
	make_json_result(true,$errmsg,['error'=>($errmsg===true?0:1)]);
}elseif($_REQUEST['act']=='warehouse_transfer_proceed'){
	$errmsg = $erpController->stockWarehouseTransfer($_REQUEST['warehouse_from'],$_REQUEST['warehouse_to'],[$_REQUEST['goods_id']=>$_REQUEST['warehouse_transfer_qty']],$_REQUEST['remark']);
	make_json_result(true,$errmsg,['error'=>($errmsg===true?0:1)]);
}elseif ($_REQUEST['act']=='rma_to_supplier') {
	if(!is_numeric($_POST['val']))
		make_json_result(null,'請選擇供應商',['error'=>1]);

	$pId=intval($_REQUEST['id']);
	$supplierId = intval($_REQUEST['val']);
	$sName=$GLOBALS['db']->getOne("SELECT name FROM ".$GLOBALS['ecs']->table('erp_supplier')." WHERE supplier_id=".$supplierId);
	make_json_result('出貨至供應商維修'.'<!--'.time().'-->',null,['error'=>0,'sname'=>$sName,'pid'=>$pId,'supplier_id'=>$supplierId]);

}elseif($_REQUEST['act']=='rma_to_supplier_proceed'){
	$errmsg = $erpController->makeRMADeliveryOrder($_REQUEST['wid'],$_REQUEST['supplier_id'],$_REQUEST['pid'],$_REQUEST['qty']);
	make_json_result(true,$errmsg,['error'=>($errmsg===true?0:1)]);
}elseif($_REQUEST['act']=='stock_breakdown_query'){
	$goods_ids = explode(",", trim($_REQUEST['gid']));
	$sellable = !empty($_REQUEST['pos']);
	$sql = "SELECT goods_id, goods_name FROM " . $ecs->table("goods") . " WHERE goods_id " . db_create_in($goods_ids);
	$temp_goods = $db->getAll($sql);
	$temp_warehouses = $erpController->getSellableWarehouseInfo();
	foreach ($temp_goods as $row) {
		$goods[$row['goods_id']] = $row['goods_name'];
	}
	foreach ($temp_warehouses as $row) {
		$warehouses[$row['warehouse_id']] = $row['name'];
	}
	$stocks = $erpController->getSellableProductsQtyByWarehouses($goods_ids, "", $sellable);
	make_json_response(["stocks" => $stocks, "warehouses" => $warehouses, "goods" => $goods]);
}
?>