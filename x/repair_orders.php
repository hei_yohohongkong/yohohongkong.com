<?php

/***
* repair_orders.php
* by howang 2015-05-21
*
* Manage repair orders
***/

define('IN_ECS', true);
require(dirname(__FILE__) . '/includes/init.php');
$controller = new Yoho\cms\Controller\YohoBaseController(basename(PHP_SELF, ".php"));
if ($_REQUEST['act'] == 'list')
{
    $smarty->assign('ur_here',      $_LANG['07_repair_orders']);

    $smarty->assign('ro_sn', empty($_REQUEST['ro_sn']) ? '' : $_REQUEST['ro_sn']);
    $smarty->assign('action_link', array('text' => '開維修單', 'href' => 'javascript:newRepairOrder();'));
    $operators = $db->getAll("SELECT `sales_name` as `name`, `sales_name` as `value` FROM " . $ecs->table('salesperson') . " WHERE `user_id` > 0 AND `available` = 1");
    $smarty->assign('operators', $operators);
    $smarty->assign('statuslist', Yoho\cms\Controller\OrderController::DB_REPAIR_ORDER_STATUS);
    $statusoptions = [];
    foreach(Yoho\cms\Controller\OrderController::DB_REPAIR_ORDER_STATUS as $k=>$v){
        if(!in_array($k, [Yoho\cms\Controller\OrderController::DB_REPAIR_ORDER_STATUS_PENDING,Yoho\cms\Controller\OrderController::DB_REPAIR_ORDER_STATUS_RESOLVED]))
            continue;
        $statusoptions[]=['name'=>$v,'value'=>$k];
    }
    $statusArr=(Yoho\cms\Controller\OrderController::DB_REPAIR_ORDER_STATUS);
    for($i=0;$i<count($list['data']);$i++){
        $list['data'][$i]['status_key']=$statusArr[intval($list['data'][$i]['repair_order_status'])];
    }
    $smarty->assign('statusoptions', $statusoptions);
    assign_query_info();
    $smarty->display('repair_order_list.htm');
}
elseif ($_REQUEST['act'] == 'query')
{
    $list = get_repair_orders();
    $controller->ajaxQueryAction($list['data'], $list['record_count'], false);
}
elseif ($_REQUEST['act'] == 'ajaxEdit')
{
    $repair_id = intval($_POST['id']);
    $value = $_POST['value'];
    $col = trim($_POST['col']);
    $pkey = trim($_POST['mkey']);
    $edit_lang = trim($_POST['edit_lang']);
    $type = trim($_POST['type']);
    if($col == 'repair_fee') {
        $value = $controller->priceToFloat($value);
        if ($value < 0)
        {
            make_json_error('您輸入了一個非法的數值');
        }
        else
        {
            $db->query("UPDATE " . $ecs->table('repair_orders') . " SET `repair_fee` = '" . $value . "' WHERE `repair_id` = '" . $repair_id . "'");
            make_json_result(price_format($value));
        }
    } elseif($col == 'remark') {
        $value = trim($value);
        $db->query("UPDATE " . $ecs->table('repair_orders') . " SET `remark` = '" . $value . "' WHERE `repair_id` = '" . $repair_id . "'");
        make_json_result(stripslashes($value));
    } elseif($col == 'operator2') {
        $value = trim($value);
        $db->query("UPDATE " . $ecs->table('repair_orders') . " SET `operator2` = '" . $value . "' WHERE `repair_id` = '" . $repair_id . "'");
        make_json_result(stripslashes($value));
    } elseif ($col == 'remark2') {
        $value = trim($value);

        $db->query("UPDATE " . $ecs->table('repair_orders') . " SET `remark2` = '" . $value . "' WHERE `repair_id` = '" . $repair_id . "'");
        make_json_result(stripslashes($value));
    } elseif ($col == 'repair_order_status') {
        $value = intval($value);

        $orderController = new Yoho\cms\Controller\OrderController();
        if($value==$orderController::DB_REPAIR_ORDER_STATUS_RESOLVED){
            $repair_sn = $db->getOne("SELECT ro.repair_sn FROM ".$ecs->table('repair_orders')." ro WHERE ro.repair_id=".$repair_id);
            //this query supposed to be executed inside makeRODelivery, however the db table definiation was bad so we could not compare erp_stock.order_sn and repair_orders.repair_sn, in order to see if there's any existing stock record
            $stock_id= $db->getOne("SELECT stock_id FROM ".$ecs->table('erp_stock')." WHERE order_sn='".$repair_sn."' ORDER BY stock_id DESC LIMIT 1");
            if($stock_id!=''){
                $erpController = new Yoho\cms\Controller\ErpController();
                $result = $erpController->makeRODeliveryOrder($repair_sn);
                if($result!==true){
                    make_json_error($result);
                }
            }
        }
        $result = $orderController->updateRepairOrderStatus($repair_id,$value);
        if($result!==true){
            make_json_error($result);
        }
        $value = Yoho\cms\Controller\OrderController::DB_REPAIR_ORDER_STATUS[$value];
        make_json_result(stripslashes($value));
    } elseif ($col == 'pickup_time') {
        $value = trim($value);
        $value_time = local_strtotime($value);

        if (local_date('Y-m-d', $value_time) != $value)
        {
            make_json_error('您輸入了一個非法的日期');
        }
        else
        {
            $db->query("UPDATE " . $ecs->table('repair_orders') . " SET `pickup_time` = '" . $value_time . "' WHERE `repair_id` = '" . $repair_id . "'");
            make_json_result(stripslashes($value));
        }
    }
}

// 封存
elseif ($_REQUEST['act'] == 'remove')
{
    $repair_id = intval($_GET['id']);

    $sql = "UPDATE " .$ecs->table('repair_orders'). " SET `is_deleted` = 1 WHERE `repair_id` = '" . $repair_id . "'";
    $res = $db->query($sql);
    
    $url = 'repair_orders.php?act=query&' . str_replace('act=remove', '', $_SERVER['QUERY_STRING']);
    
    header('Location: ' . $url);
    exit;
}
// 還原
elseif ($_REQUEST['act'] == 'restore')
{
    $repair_id = intval($_GET['id']);
    
    $sql = "UPDATE " .$ecs->table('repair_orders'). " SET `is_deleted` = 0 WHERE `repair_id` = '" . $repair_id . "'";
    $res = $db->query($sql);
    
    //$url = 'repair_orders.php?act=query&' . str_replace('act=restore', '', $_SERVER['QUERY_STRING']);
    $url = 'repair_orders.php?act=list';
    header('Location: ' . $url);
    exit;
}

function get_repair_orders()
{
    global $controller;
    $erpController = new Yoho\cms\Controller\ErpController();
    $_REQUEST['mode']         = empty($_REQUEST['mode']) ? 'normal' : trim($_REQUEST['mode']);
    $_REQUEST['ro_sn']         = empty($_REQUEST['ro_sn']) ? '' : trim($_REQUEST['ro_sn']);
    $_REQUEST['sort_by']      = empty($_REQUEST['sort_by']) ? 'repair_id' : trim($_REQUEST['sort_by']);
    $_REQUEST['sort_order']   = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
    
    $where = '';
    if ($_REQUEST['mode'] == 'normal')
    {
        $where = " WHERE ro.is_deleted = '0' ";
    }
    elseif ($_REQUEST['mode'] == 'deleted')
    {
        $where = " WHERE ro.is_deleted = '1' ";
    }
    if($_REQUEST['ro_sn']!=''){
        $where .= " AND repair_sn LIKE '%".$_REQUEST['ro_sn']."'";
    }
    $sql = "SELECT count(*) FROM " . $GLOBALS['ecs']->table('repair_orders') . " as ro $where";
    $record_count = $GLOBALS['db']->getOne($sql);

    /* 查询 */
    $sql = "SELECT ro.*, oi.`order_sn`, g.`goods_sn`, g.`goods_name`, g.`goods_thumb`, IFNULL(sum(es.qty),0) as sum_qty " .
            "FROM " . $GLOBALS['ecs']->table('repair_orders') . " as ro " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('order_info') . " as oi ON oi.order_id = ro.order_id " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON g.goods_id = ro.goods_id " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('erp_stock')." as es ON es.goods_id=ro.goods_id AND es.warehouse_id NOT IN (".$erpController->getSellableWarehouseIds(true).") ".
            $where .
            "GROUP BY ro.repair_id ORDER BY " . $_REQUEST['sort_by'] . " " . $_REQUEST['sort_order'] . " " .
            "LIMIT " . $_REQUEST['start'] . "," . $_REQUEST['page_size'];
    $data = $GLOBALS['db']->getAll($sql);
    
    foreach ($data as $key => $row)
    {
        $data[$key]['add_date'] = local_date($GLOBALS['_CFG']['time_format'], $row['add_time']);
        $data[$key]['pickup_date'] = $row['pickup_time'] ? local_date($GLOBALS['_CFG']['date_format'], $row['pickup_time']) : '--';
        $data[$key]['repair_fee_formatted'] = price_format($row['repair_fee'], false);
        $data[$key]['operator2'] = $row['operator2'] ? $row['operator2'] : "N/A";
        $btn_1 = ['icon_class' => 'fa fa-eye','title' => '查看庫存紀錄', 'link' => "erp_stock_inquiry.php?act=list&goods_sn=$row[goods_sn]"];
        if($row['is_deleted']) {
            $btn_2 = [
                'icon_class' => 'fa fa-repeat',
                'link'       => "repair_orders.php?act=restore&amp;id=".$row['repair_id'],
                'title'      => '復原',
                'label'      => '復原',
            ];
        } else {
            $btn_2 = [
                'icon_class' => 'fa fa-trash',
                'link'       => 'javascript:;',
                'label'      => '封存',
                'title'      => '封存',
                'data'       => ['url'=>"repair_orders.php?act=remove&amp;id=".$row['repair_id']],
                'onclick'    => 'cellArchive(this);',
                'btn_class'  => 'btn-danger'
            ];
        }
        $btn_3 = [
            'icon_class' => 'fa fa-print',
            'link'       => '/pos_repair.php?act=print&amp;repair_id='.$row['repair_id'],
            'label'      => '列印',
            'title'      => '列印',
            'btn_class'  => 'btn-success',
            'target'     => '_blank'

        ];
        $btn_list = ['view' => $btn_1, 'btn_2' => $btn_2, 'print' => $btn_3];
        $data[$key]['_action'] = $controller->generateCrudActionBtn(0, '', '', [], $btn_list);
        // if ($row['status'] == 'pending')
        // {
        //     $data[$key]['status_text'] = '未處理';
        // }
        // else if ($row['status'] == 'processing')
        // {
        //     $data[$key]['status_text'] = '處理中';
        // }
        // else if ($row['status'] == 'finished')
        // {
        //     $data[$key]['status_text'] = '已完成';
        // }
        // else if ($row['status'] == 'rejected')
        // {
        //     $data[$key]['status_text'] = '不處理';
        // }
    }
    
    $arr = array('data' => $data, 'record_count' => $record_count);
    
    return $arr;
}

?>