<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_base.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_warehouse.php');
require(dirname(__FILE__) .'/includes/ERP/cls/cls_date.php');
$stocktakeController = new Yoho\cms\Controller\StocktakeController();
$adminuserController = new Yoho\cms\Controller\AdminuserController();
$deliveryOrderController = new Yoho\cms\Controller\DeliveryOrderController();

if($_REQUEST['act'] == 'list' || $_REQUEST['act'] == ''){
	if( admin_priv('erp_warehouse_view','',false) || admin_priv('erp_warehouse_manage','',false) || admin_priv('erp_warehouse_approve','',false))
	{
        include('./includes/ERP/page.class.php');

        if (!empty($_REQUEST['date'])) {
            $date = $_REQUEST['date'];
        } else {
            $date = date( 'Y-m-d' );
        }

		if (isset($_REQUEST['status'])) {
			$status = $_REQUEST['status'];
		} else {
			$status = -1;
		}

		if (isset($_REQUEST['passed'])) {
			$passed = $_REQUEST['passed'];
		} else {
			$passed = -1;
		}

		if (isset($_REQUEST['logistics_id']) && $_REQUEST['logistics_id'] !='' ) {
			$logistics_id = $_REQUEST['logistics_id'];
		} else {
			$logistics_id = -1;
		}

		$logisticMethod = $deliveryOrderController->getLogisticsMethod();
		// echo '<pre>';
		// print_r($logisticMethod);
		// exit;
		$orderResult = $deliveryOrderController->getOrderGoodsCrossCheckListByDate($date);
		
		$smarty->assign('logistics_id', $logistics_id);
		$smarty->assign('logistic_method', $logisticMethod);
		$smarty->assign('order_result', $orderResult);
		$smarty->assign('action_link', array('text' => '返回核對報告列表', 'href' => 'order_goods_cross_check_report.php'));
	    
        $smarty->assign('full_page', 1);
		$smarty->assign('order_sn', $_REQUEST['order_sn']);
        $smarty->assign('status', $status);
		$smarty->assign('passed', $passed);
        $smarty->assign('date', $date);
        $smarty->assign('cfg_lang', $_CFG['lang']);
        $smarty->assign('lang', $_LANG);
        $smarty->display('order_goods_cross_check_report_detail.htm');
    }else{
        $href="index.php";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
    }

} elseif ($_REQUEST['act'] == 'get_cross_check_detail_by_delivery_id') {
	$delivery_id = $_REQUEST['delivery_id'];
	$order_sn = $_REQUEST['order_sn'];
	$result = $deliveryOrderController->getCrossCheckDetailByDeliveryId($delivery_id,$order_sn);

	if($result !== false){
		make_json_result($result);
	}else{
		make_json_error('發生錯誤');
	}

   // $deliveryOrderController->ajaxQueryAction($responseData,0,false);
}
?>