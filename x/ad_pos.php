<?php
/**
 * ad_pos.php
 * $Author: Yan@YOHO
 * $date: 10/10/2019
*/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/admin/ads.php');

// define controller
$adPosController = new Yoho\cms\Controller\AdPosController();
$controller = new Yoho\cms\Controller\YohoBaseController('ad_pos');

$act  = empty($_REQUEST['act']) ? 'list' : $_REQUEST['act'];
$tablename ='ad_pos';
$smarty->assign('lang', $_LANG);
$exc = new exchange( $ecs->table($tablename), $db, 'ad_pos_id', 'ad_name');


switch($act) {
    /* ------------------------------------------------------ */
    //-- List pos ad
    /* ------------------------------------------------------ */
    case 'list':
        $smarty->assign('ur_here', $_LANG['ad_pospromote']);
        $smarty->assign('action_link', array('text' => $_LANG['add_pos_ad'], 'href' => 'ad_pos.php?act=add'));

        assign_query_info();
        $smarty->display('ad_pos_list.htm');
        break;

    /* ------------------------------------------------------ */
    //-- Query pos ad
    /* ------------------------------------------------------ */
    case 'query':
        $list = $adPosController->get_ad_pos_list();
        $controller->ajaxQueryAction($list['data'], $list['record_count']);
        break;
    /* ------------------------------------------------------ */
    //-- Add pos ad
    /* ------------------------------------------------------ */
    case 'add':
        $ad_name = empty($_GET['ad_name']) ? '' : trim($_GET['ad_name']);

        $start_time = local_date('Y-m-d');
        $end_time   = local_date('Y-m-d', gmtime() + 3600 * 24 * 30);  // 默认结束时间为1个月以后
        
        $smarty->assign('ad_pos', array(
                            'ad_name' => $ad_name, 
                            'start_time' => $start_time,
                            'end_time' => $end_time, 
                            'enabled' => 1,
                        ));
    
        $smarty->assign('ur_here',       $_LANG['add_pos_ad']);
        $smarty->assign('action_link',   array('href' => 'ad_pos.php?act=list', 'text' => $_LANG['ad_pos_list']));

        $smarty->assign('form_act', 'insert');
        $smarty->assign('action',   $act);

        assign_query_info();
        $smarty->display('ad_pos_info.htm');
        break;
    /* ------------------------------------------------------ */
    //-- Insert pos ad
    /* ------------------------------------------------------ */
    case 'insert':
        if ($exc->num('ad_name', trim($_POST['ad_name'])) == 0) 
        {
            $data = $adPosController->parse_ad_pos_request($_POST);
            if($data['status']) {
                $data = $data['data'];
                if(empty($data['ad_file'])) {
                    if($data['media_type'] == 0) {
                        sys_msg($_LANG['js_languages']['ad_photo_empty']);
                    } else if($data['media_type'] == 4) {
                        sys_msg($_LANG['js_languages']['ad_video_empty']);
                    }
                }

                $result = $adPosController->update($data);
                if($result)
                {
                    $action = $_POST['action'];
                    $name = $data['ad_name'];
        
                    // 记录日志
                    admin_log($name, $action, $tablename);

                    clear_cache_files();

                    $link[] = array('text' => $_LANG['back_ad_pos_list'], 'href' => 'ad_pos.php?act=list');
                    sys_msg($_LANG['add'] . "&nbsp;" .$_POST['ad_name'] . "&nbsp;" . $_LANG['attradd_succed'], 0, $link);
                } else {
                    $link[] = array('text' => $_LANG['back_ad_pos_list'], 'href' => 'ad_pos.php?act=list');
                    sys_msg($_LANG['ad_pos_not_available'], 0, $link);
                }
            } else {
                $link[] = array('text' => $_LANG['go_back'], 'href' => 'ad_pos.php?act=add');
                sys_msg($data['error_msg'], 0, $link);
            }
        } else {
            $link[] = array('text' => $_LANG['go_back'], 'href' => 'ad_pos.php?act=add');
            sys_msg($_LANG['ad_name_exist'], 0, $link);
        }

        break;
    /* ------------------------------------------------------ */
    //-- Edit pos ad
    /* ------------------------------------------------------ */
    case 'edit':
        
        $ad_pos = $adPosController->get_ad_pos($_REQUEST['id']);
        if($ad_pos['status']) 
        {
            $smarty->assign('ad_pos'      , $ad_pos['data']);
            $smarty->assign('ur_here'     , $_LANG['edit_pos_ad']);
            $smarty->assign('action_link' , array('href' => 'ad_pos.php?act=list', 'text' => $_LANG['ad_pos_list']));

            $smarty->assign('form_act', 'update');
            $smarty->assign('action',   $act);

            assign_query_info();
            $smarty->display('ad_pos_info.htm');
        } 
        else 
        {
            sys_msg($_LANG['ad_pos_not_available']);
        }

        break;

    /* ------------------------------------------------------ */
    //-- Update pos ad
    /* ------------------------------------------------------ */
    case 'update':
        if(!empty(trim($_POST['id']))) {
            if ($exc->is_only('ad_name', trim($_POST['ad_name']), $_POST['id'])) 
            {
                $data = $adPosController->parse_ad_pos_request($_POST);
                if($data['status']) {
                    $data = $data['data'];
                    if(empty($data['ad_file'])) {
                        unset($data['ad_file']);
                    }

                    $result = $adPosController->update($data);

                    if($result)
                    {
                        $action = $_POST['action'];
                        $name = $data['ad_name'];
            
                        // 记录日志
                        admin_log($name, $action, $tablename);
        
                        clear_cache_files();
        
                        $link[] = array('text' => $_LANG['back_ad_pos_list'], 'href' => 'ad_pos.php?act=list');
                        sys_msg($_LANG['edit'] . "&nbsp;" .$_POST['ad_name'] . "&nbsp;" . $_LANG['attradd_succed'], 0, $link);
                    } else {
                        $link[] = array('text' => $_LANG['back_ad_pos_list'], 'href' => 'ad_pos.php?act=list');
                        sys_msg($_LANG['ad_pos_not_available'], 0, $link);
                    }
                } else {
                    $error_msg = $data['error_msg'];
                }

            } else {
                $error_msg = $_LANG['ad_name_exist'];
            }
            $link[] = array('text' => $_LANG['go_back'], 'href' => 'ad_pos.php?act=edit&id='.$_POST['id']);
        } else {
            $error_msg = $_LANG['ad_pos_not_available'];
            $link[] = array('text' => $_LANG['back_ad_pos_list'], 'href' => 'ad_pos.php?act=list');
        }

        sys_msg($error_msg, 0, $link);

        break;
    /* ------------------------------------------------------ */
    //-- Remove pos ad
    /* ------------------------------------------------------ */
    case 'remove':
        
        check_authz_json('ad_manage');

        $id = intval($_GET['id']);
        $img = $exc->get_name($id, 'ad_file');

        $exc->drop($id);

        if ((strpos($img, 'http://') === false) && (strpos($img, 'https://') === false))
        {
            $img_name = basename($img);
            @unlink(ROOT_PATH. DATA_DIR . '/afficheimg/'.$img_name);
        }

        admin_log('', 'remove', $tablename);

        $url = 'ad_pos.php?act=query&' . str_replace('act=remove', '', $_SERVER['QUERY_STRING']);

        ecs_header("Location: $url\n");
        exit;


        break;
}

?>