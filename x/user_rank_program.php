<?php
define('IN_ECS', true);
require dirname(__FILE__) . '/includes/init.php';
$controller = new Yoho\cms\Controller\YohoBaseController('user_rank_program');
$userRankController = new Yoho\cms\Controller\UserRankController();

if ($_REQUEST['act'] == "list") {
    $assign['ur_here'] = $_LANG['06_user_rank_program'];
    $assign['action_link'] = array('text' => $_LANG['add_rank_program'], 'href'=>'user_rank_program.php?act=add');
    $assign['action_link2'] = array('text' => $_LANG['list_organization'], 'href'=>'user_rank_program.php?act=list_organization');
    $assign['action_link3'] = array('text' => $_LANG['list_pricing_profile'], 'href'=>'user_rank_program.php?act=list_pricing_profile');

    // get wrong price
    $wrong_price = $userRankController->getProfileItemWrongPrice();

    //dd($wrong_price);
    $wrong_alert_str = "";
    foreach ($wrong_price as $item) {
       $wrong_alert_str .= '[<a href="user_rank_program.php?act=edit_pricing_profile&pricing_profile_id='.$item['id'].'">'.$item['name'].'</a>] ';
    }
    $assign['wrong_alert_str'] = $wrong_alert_str; 
    $userRankController->listAction($assign, "user_rank_program.htm");
} elseif ($_REQUEST['act'] == "query") {
    $program_id = empty($_REQUEST['program_id']) ? 0 : intval($_REQUEST['program_id']);
    $sql = "SELECT ur.rank_name, urp.*, urppp.profile_name, count(urpppi.pricing_profile_items_id) as total_goods_qty FROM " . $ecs->table("user_rank_program") . " urp " .
            "LEFT JOIN " . $ecs->table("user_rank") . " ur ON ur.rank_id = urp.rank_id " .
            "LEFT JOIN " . $ecs->table("user_rank_program_pricing_profile") . " urppp ON urp.pricing_profile_id = urppp.pricing_profile_id " .
            "LEFT JOIN " . $ecs->table("user_rank_program_pricing_profile_items") . " urpppi ON urppp.pricing_profile_id = urpppi.pricing_profile_id " .
            (empty($program_id) ? "WHERE 1 " : "WHERE program_id = $program_id ") .
            "group by urp.program_id, urp.pricing_profile_id   ORDER BY  $_REQUEST[sort_by] $_REQUEST[sort_order]"." LIMIT " . $_REQUEST['start'] . ",$_REQUEST[page_size] ";

    $list = $db->getAll($sql);
    $sql = "SELECT count(*) FROM " . $ecs->table("user_rank_program") .
            (empty($program_id) ? "WHERE 1 " : "WHERE program_id = $program_id ");
    $count = $db->getOne($sql);

    // profile qty
    //$sql = "";

    $sql = "select count(user_id) as count,program_id from ".$ecs->table("user_rank_program_application")." where verified = 1 group by program_id  ";
    $count_res = $db->getAll($sql);

    $joined_count = [];
    foreach ($count_res as $count) {
        $joined_count[$count['program_id']] = $count['count']; 
    }

    foreach ($list as $key => $row) {
        $list[$key]["status"] = $_LANG['formatted_rank_program_status'][$row['status']];

        if ($row['show_on_menu'] == 1) {
            $list[$key]["formatted_status"] .= '<br>(公開顯示)'; 
        }

        if ($list[$key]["verifiy_type"] ==  1) {
            $list[$key]["verify_type_name"] = '電郵'; 
        } else if ($list[$key]["verifiy_type"] ==  2 || $list[$key]["verifiy_type"] ==  3 ) {
            $list[$key]["verify_type_name"] = '特殊連結';
        } else {
            $list[$key]["verify_type_name"] = '其他';
        }

        if ($row['status'] == RANK_PROGRAM_SHOW || $row['status'] == RANK_PROGRAM_HIDE) {
            $btn = [
                'link'  => "user_rank_validate.php?act=list&program_id=$row[program_id]",
                'title' => "審核申請",
                'label' => "審核",
                'btn_class' => "btn btn-success"
            ];
            $list[$key]["_action"] = $controller->generateCrudActionBtn($row['program_id'], 'id', null, ['edit'], ['verify' => $btn]);
        } else {
            $list[$key]["_action"] = $controller->generateCrudActionBtn($row['program_id'], 'id', null, ['edit']);
        }
        $default_expiry_ar = explode(" ",$list[$key]['default_expiry']);
        $list[$key]['default_expiry'] = $default_expiry_ar[0];
        $list[$key]['joined_count'] = isset($joined_count[$list[$key]['program_id']]) ? $joined_count[$list[$key]['program_id']] : 0;

        $list[$key]['profile_name'] = '<a href="user_rank_program.php?act=edit_pricing_profile&pricing_profile_id='.$row['pricing_profile_id'].'">'.$list[$key]['profile_name'].'</a>';


    }
    $controller->ajaxQueryAction($list, count($list), false);
} elseif ($_REQUEST['act'] == "edit" || $_REQUEST['act'] == "add") {
    $userRankController->editAction();
} elseif ($_REQUEST['act'] == "update") {
    $_REQUEST["program_name"] = trim($_REQUEST["program_name"]);

    $_REQUEST["validate"] = 1;
    if (empty($_REQUEST['validate'])) {
        $_REQUEST['validate'] = 0;
    }
    if (empty($_REQUEST['program_id'])) {
        $insert = true;
        $_REQUEST['act'] = "insert";
    }
    if (preg_match('/[^A-Za-z0-9\-]+/', $_REQUEST['program_code'])) {
        sys_msg('計劃路徑名只能為英數及橫線', 1);
    }
    if($insert) { // If insert, check program_code unique
        $count_code = $db->getOne("SELECT COUNT(*) FROM " . $ecs->table("user_rank_program") . " WHERE program_code = '$_REQUEST[program_code]' ");
    } else {
        $count_code = $db->getOne("SELECT COUNT(*) FROM " . $ecs->table("user_rank_program") . " WHERE program_code = '$_REQUEST[program_code]' AND program_id != ".$_REQUEST['program_id']);
    }
    if($count_code > 0) {
        sys_msg('計劃路徑名已存在', 1);
    }

    if($insert) { // If insert, check program_code unique
        $count_code = $db->getOne("SELECT COUNT(*) FROM " . $ecs->table("user_rank_program") . " WHERE program_name = '$_REQUEST[program_name]' ");
    } else {
        $count_code = $db->getOne("SELECT COUNT(*) FROM " . $ecs->table("user_rank_program") . " WHERE program_name = '$_REQUEST[program_name]' AND program_id != ".$_REQUEST['program_id']);
    }
    if($count_code > 0) {
        sys_msg('計劃名稱['.$_REQUEST['program_name'].']已存在', 1);
    } 

    if (!empty($_REQUEST['rank_id'])) {
        $valid = $db->getOne("SELECT COUNT(*) FROM " . $ecs->table("user_rank") . " WHERE special_rank = 1 AND rank_id = $_REQUEST[rank_id]");
        if (!empty($valid)) {
            $_REQUEST["rank_id"] = $_REQUEST['rank_id'];
        }
    }
    
    if (!empty($_REQUEST['default_expiry'])) {
        $_REQUEST["default_expiry"] = "$_REQUEST[default_expiry] 23:59:59";
    } else {
        sys_msg('請設定預設有效日期', 1);  
    }

    if(!empty($_REQUEST['organization_ids']) && is_array($_REQUEST['organization_ids'])) {
        $_REQUEST['organization_ids'] = array_unique($_REQUEST['organization_ids']);
        $_REQUEST['organization_ids'] = serialize($_REQUEST['organization_ids']);
    } else {
        $_REQUEST['organization_ids'] = '';
    }
    if (!empty($_REQUEST['expiry_update'])) {
        $sql = "update  ". $ecs->table("user_rank_program_application"). " set expiry = '".$_REQUEST["default_expiry"]."'  where verified = 1 and program_id = ".$_REQUEST['program_id']." ";
        $db->query($sql);
    }

    if ($_REQUEST['verifiy_type'] == 2 && isset($_REQUEST['foreign_id_req'])){
        $_REQUEST['verifiy_type'] = 3;
    }

    if (empty($_REQUEST['require_proof'])) {
        $_REQUEST['require_proof'] = 0;
    }

    if (empty($_REQUEST['id'])) {
        if (!empty(trim($_REQUEST["program_name"]))) {
            $new_rank_id = $userRankController->creatNewUserRank(trim($_REQUEST["program_name"]));
            $_REQUEST['rank_id'] = $new_rank_id;
        }
    }

    // get current user_rank
    $program_basic_info = $userRankController->getUserRankProgramBasicInfo($_REQUEST['id']);

    if ($program_basic_info['pricing_profile_id'] != $_REQUEST['pricing_profile_id']) {
        if (!isset($_REQUEST['rank_id'])) {
            $rank_id = $program_basic_info['rank_id'];
        } else {
            $rank_id = $_REQUEST['rank_id'];
        }

        // update user_rank and member price
        //$pricingProfileInfo = $userRankController->getPricingProfileInfo($_REQUEST['pricing_profile_id']);
        //$userRankController->updateProgramRankId($_REQUEST['program_id'],$pricingProfileInfo['user_rank_id']);

        // update member price by prcing profile
        $userRankController->updateMemberPriceByRankIdAndProfileId($rank_id, $_REQUEST['pricing_profile_id']);
    }

    // update user rank name 
    if (!empty($_REQUEST['program_id']) && !empty($program_basic_info['rank_id'])) {
        $sql = "update ". $ecs->table("user_rank") ." set rank_name = '".$_REQUEST['program_name']."' where rank_id = ".$program_basic_info['rank_id']." ";
        $db->query($sql);    
    }

    if ($_REQUEST['act'] == 'insert') {
        $new_program_id = $controller->updateAction(true);
        header("Location: user_rank_program.php?act=edit&id=".$new_program_id."&tab=organization");
        die();
    } else {
        $controller->updateAction();
    }
} elseif ($_REQUEST['act'] == "list_organization") {
    $assign['ur_here'] = $_LANG['list_organization'];
    $assign['action_link'] = array('text' => $_LANG['06_user_rank_program'], 'href'=>'user_rank_program.php?act=list');
    $assign['action_link2'] = array('text' => $_LANG['add_organization'], 'href'=>'user_rank_program.php?act=add_organization');
    $userRankController->listAction($assign, "program_organization_list.htm");
} elseif ($_REQUEST['act'] == "query_o") {
    $organization_id = empty($_REQUEST['organization_id']) ? 0 : intval($_REQUEST['organization_id']);
    $sql = "SELECT * FROM " . $ecs->table("program_organization").
            (empty($organization_id) ? "WHERE 1 " : "WHERE organization_id = $organization_id ") .
            "ORDER BY $_REQUEST[sort_by] $_REQUEST[sort_order]"." LIMIT " . $_REQUEST['start'] . ",$_REQUEST[page_size] ";
    $list = $db->getAll($sql);
    $sql = "SELECT count(*) FROM " . $ecs->table("program_organization") .
            (empty($organization_id) ? "WHERE 1 " : "WHERE organization_id = $organization_id ");
    $count = $db->getOne($sql);
    foreach ($list as $key => $row) {

        $list[$key]["type_string"] = $userRankController::ORGANIZATION_TYPE_STRING_ARRAY[$row['organization_type']];
        $list[$key]["_action"] = $controller->generateCrudActionBtn($row['organization_id'], 'id', '', ['edit_organization']);

    }
    $controller->ajaxQueryAction($list, count($list), false);
} elseif ($_REQUEST['act'] == "edit_organization" || $_REQUEST['act'] == "add_organization") {
    $userRankController->editOrganizationAction();
} elseif ($_REQUEST['act'] == "update_o") {
    if (empty($_REQUEST['organization_id'])) {
        $insert = true;
        $_REQUEST['act'] = "insert";
    } else {
        $_REQUEST['act'] = "update";
    }
    $_REQUEST['create_at'] = gmtime();
    $_REQUEST['enabled']   = 1;
    $controller = new Yoho\cms\Controller\YohoBaseController('program_organization');
    $controller->updateAction();
} elseif ($_REQUEST['act'] == "list_pricing_profile") {
    $assign['ur_here'] = $_LANG['list_pricing_profile'];
    $assign['action_link'] = array('text' => '返回特殊會員計劃', 'href'=>'user_rank_program.php?act=list');
    $assign['action_link2'] = array('text' => $_LANG['add_pricing_profile'], 'href'=>'user_rank_program.php?act=add_pricing_profile');
    $userRankController->listAction($assign, "program_pricing_profile_list.htm");
}elseif ($_REQUEST['act'] == "query_pricing_profile") {
    $result = $userRankController->getPricingProfileList();
    $controller->ajaxQueryAction($result['data'],$result['record_count'],false);
} elseif ($_REQUEST['act'] == "query_pricing_profile_products") {
    $result = $userRankController->searchPricingProfileProductsList();
    $controller->ajaxQueryAction($result['data'],$result['record_count'],false);
} elseif ($_REQUEST['act'] == "add_pricing_profile") {
    $userRankController->addPricingProfile();
} elseif ($_REQUEST['act'] == "edit_pricing_profile") {
    $userRankController->editPricingProfile();
} elseif ($_REQUEST['act'] == "insert_pricing_profile") {
    $userRankController->insertPricingProfile();
} elseif ($_REQUEST['act'] == "update_pricing_profile") {
   // $userRankController->updatePricingProfile();
} elseif ($_REQUEST['act'] == 'search_and_insert_goods') {
    $result = $userRankController->searchAndInsertGoods();
    if(isset($result['error'])){
        make_json_error($result['message']);
    }else{
        make_json_result($result);
    }
} elseif ($_REQUEST['act'] == 'query_pricing_profile_items') {
    $result = $userRankController->getPricingProfileItems($_REQUEST['pricing_profile_id']);
    $controller->ajaxQueryAction($result['data'],$result['record_count'],false);
}  elseif ($_REQUEST['act'] == 'update_user_price') {
    $result = $userRankController->updateUserPrice($_REQUEST['pricing_profile_id'],$_REQUEST['goods_id'],$_REQUEST['update_price']);
    make_json_result($result);
}  elseif ($_REQUEST['act'] == 'delete_user_price') {
    $result = $userRankController->deleteUserPrice($_REQUEST['pricing_profile_id'],$_REQUEST['goods_id']);
    make_json_result($result);
}  elseif ($_REQUEST['act'] == 'delete_pricing_profile') {
    $result = $userRankController->deletePricingProfile($_REQUEST['pricing_profile_id']);
    if(isset($result['error'])){
        make_json_error($result['message']);
    }else{
        make_json_result($result);
    }
} elseif ($_REQUEST['act'] == 'goods_name_tips') {
	include('../includes/cls_json.php');
	$json  = new JSON;
	$goods_name=trim($_REQUEST['goods_name']);
	$fetch_goods_name=array();
	$sql="select g.goods_name from ".$GLOBALS['ecs']->table('goods')." as g ";
	$sql.=" where g.goods_name like '%".$goods_name."%' order by goods_id desc limit 10";
	$res=$GLOBALS['db']->query($sql);
	while($row=mysql_fetch_assoc($res))
	{
		$fetch_goods_name[]=$row['goods_name'];
	}
	$smarty->assign('goods_name',$fetch_goods_name);
	$content=$smarty->fetch('erp_goods_name_tips.htm');
	$result['error']=0;
	$result['content']=$content;
	$result['num']=count($fetch_goods_name);
	die($json->encode($result));
}
elseif ($_REQUEST['act'] == 'check_goods_sn'||$_REQUEST['act'] == 'check_goods_name')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if($_REQUEST['act'] == 'check_goods_sn')
	{
		$goods_sn=trim($_REQUEST['goods_sn']);
		$goods_id=check_goods_sn($goods_sn);
		if(empty($goods_id))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_warehousing_wrong_goods_sn'];
			die($json->encode($result));
		}
	}
	else
	{
		$goods_name=trim($_REQUEST['goods_name']);
		$goods_id=check_goods_name($goods_name);
		if(empty($goods_id))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_warehousing_wrong_goods_name'];
			die($json->encode($result));
		}
	}
	$goods_attr=goods_attr($goods_id);
	$smarty->assign('goods_attr',$goods_attr);
	$smarty->assign('is_dynamic',1);
	$attr_info=$smarty->fetch('erp_select_goods_attr.htm');
	$result['attr_info']=$attr_info;
	$result['attr_num']=count($goods_attr);
	$result['error']=0;
	die($json->encode($result));
} else if ($_REQUEST['act'] == 'update_pricing_profile_name') {
    $userRankController->updatePricingProfileName($_REQUEST['pricing_profile_id'],$_REQUEST['profile_name']);
    make_json_result($result);
} elseif ($_REQUEST['act'] == 'update_pricing_profile_item_status') {
    $userRankController->updatePricingProfileItemStatus($_REQUEST['active'],$_REQUEST['pricing_profile_id'],$_REQUEST['goods_id'],$_REQUEST['update_price']);
    make_json_result($result);
} else if ($_REQUEST['act'] == 'convert_pricing_profile') {
    $userRankController->convert_pricing_profile();
}

?>