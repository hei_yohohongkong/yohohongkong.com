<?php

/***
* admin_plan.php
* by howang 2015-01-14
*
* Admin work plan share
***/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

if ($_REQUEST['act'] == 'list')
{
    /* 权限判断 */
    if (!$_SESSION['manage_cost'])
    {
        sys_msg($GLOBALS['_LANG']['priv_error']);
    }
    
    $list = get_admin_plans();
    $smarty->assign('ur_here',      '工作計劃');
    $smarty->assign('full_page',    1);

    $smarty->assign('list',         $list['data']);
    $smarty->assign('filter',       $list['filter']);
    $smarty->assign('record_count', $list['record_count']);
    $smarty->assign('page_count',   $list['page_count']);

    $sort_flag  = sort_flag($list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);
    
    $smarty->assign('admin_list', array('franz','kathy','howang'));
    
    assign_query_info();
    $smarty->display('admin_plan_list.htm');
}
elseif ($_REQUEST['act'] == 'query')
{
    /* 权限判断 */
    if (!$_SESSION['manage_cost'])
    {
        make_json_error($GLOBALS['_LANG']['priv_error']);
    }
    
    $list = get_admin_plans();
    $smarty->assign('list',         $list['data']);
    $smarty->assign('filter',       $list['filter']);
    $smarty->assign('record_count', $list['record_count']);
    $smarty->assign('page_count',   $list['page_count']);

    $sort_flag  = sort_flag($list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);

    make_json_result($smarty->fetch('admin_plan_list.htm'), '', array('filter' => $list['filter'], 'page_count' => $list['page_count']));
}
elseif (($_REQUEST['act'] == 'insert') || ($_REQUEST['act'] == 'update'))
{
    $is_ajax = empty($_REQUEST['is_ajax']) ? 0 : intval($_REQUEST['is_ajax']);
    $showError = function ($msg) use ($is_ajax) {
        if ($is_ajax)
        {
            make_json_error($msg);
        }
        else
        {
            sys_msg($msg);
        }
    };
    
    /* 权限判断 */
    if (!$_SESSION['manage_cost'])
    {
        $showError($GLOBALS['_LANG']['priv_error']);
    }
    
    $is_insert = ($_REQUEST['act'] == 'insert');
    
    if (!$is_insert)
    {
        $rec_id = empty($_POST['rec_id']) ? 0 : intval($_POST['rec_id']);
        if (empty($rec_id))
        {
            $showError('編輯時必須輸入編號');
        }
    }
    
    $admin_name = empty($_POST['admin_name']) ? '' : trim($_POST['admin_name']);
    if (empty($admin_name))
    {
        $showError('必須輸入員工名稱');
    }
    
    $finished_work = empty($_POST['finished_work']) ? '' : trim($_POST['finished_work']);
    $tomorrow_plan = empty($_POST['tomorrow_plan']) ? '' : trim($_POST['tomorrow_plan']);
    $weekly_plan = empty($_POST['weekly_plan']) ? '' : trim($_POST['weekly_plan']);
    
    $plan = array(
        'admin_name' => $admin_name,
        'finished_work' => $finished_work,
        'tomorrow_plan' => $tomorrow_plan,
        'weekly_plan' => $weekly_plan
    );
    
    if ($is_insert)
    {
        $plan['add_time'] = gmtime();
        
        $db->autoExecute($ecs->table('admin_plans'), $plan, 'INSERT');
        $act_name = '新增';
    }
    else
    {
        $db->autoExecute($ecs->table('admin_plans'), $plan, 'UPDATE', " `rec_id` = '" . $rec_id . "'");
        $act_name = '編輯';
    }
    
    if ($is_ajax)
    {
        make_json_result('', $act_name . '成功');
    }
    else
    {
        $link[0]['text'] = '返回列表';
        $link[0]['href'] = 'admin_plan.php?act=list';
        
        sys_msg($act_name . '成功', 0, $link, true);
    }
}
elseif ($_REQUEST['act'] == 'remove')
{
    if (!$_SESSION['manage_cost'])
    {
        make_json_error($GLOBALS['_LANG']['priv_error']);
    }
    
    $rec_id = intval($_GET['id']);
    
    $sql = "DELETE FROM " . $ecs->table('admin_plans') .
            "WHERE `rec_id` = '" . $rec_id . "' " .
            "LIMIT 1";
    $db->query($sql);
    
    $url = 'admin_plan.php?act=query&' . str_replace('act=remove', '', $_SERVER['QUERY_STRING']);
    
    header('Location: ' . $url);
    exit;
}

function get_admin_plans()
{
    global $db;
    
    $filter['sort_by']      = empty($_REQUEST['sort_by']) ? 'rec_id' : trim($_REQUEST['sort_by']);
    $filter['sort_order']   = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
    
    $filter['admin_name']   = empty($_REQUEST['admin_name']) ? '' : trim($_REQUEST['admin_name']);
    
    $sql = "SELECT count(*) " .
            "FROM " . $GLOBALS['ecs']->table('admin_plans') .
            "WHERE 1 " .
            (empty($filter['admin_name']) ? '' : " AND `admin_name` = '" . $filter['admin_name'] . "'");
    $filter['record_count'] = $GLOBALS['db']->getOne($sql);
    
    /* 分页大小 */
    $filter = page_and_size($filter);
    
    /* 查询 */
    $sql = "SELECT * " .
            "FROM " . $GLOBALS['ecs']->table('admin_plans') .
            "WHERE 1 " .
            (empty($filter['admin_name']) ? '' : " AND `admin_name` = '" . $filter['admin_name'] . "'") .
            "ORDER BY " . $filter['sort_by'] . " " . $filter['sort_order'] . " " .
            "LIMIT " . $filter['start'] . "," . $filter['page_size'];
    $data = $GLOBALS['db']->getAll($sql);
    
    foreach ($data as $key => $row)
    {
        $data[$key]['add_date'] = local_date('Y-m-d', $row['add_time']);
    }
    
    $arr = array('data' => $data, 'filter' => $filter, 'page_count' => $filter['page_count'], 'record_count' => $filter['record_count']);
    
    return $arr;
}

?>