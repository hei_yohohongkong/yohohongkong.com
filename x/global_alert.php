<?php
/**
 * 置頂公告
 */
define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require_once(ROOT_PATH . 'includes/cls_image.php');
$controller = new Yoho\cms\Controller\YohoBaseController('global_alert');
$alertController = new Yoho\cms\Controller\AlertController();

if (empty($_REQUEST['act']))
{
    $_REQUEST['act'] = 'list';
}
else
{
    $_REQUEST['act'] = trim($_REQUEST['act']);
}

$smarty->assign('lang', $_LANG);

if ($_REQUEST['act'] == 'list')
{
    $smarty->assign('ur_here',     '公告列表');
    $smarty->assign('action_link', array('text' => '新增公告', 'href' => 'global_alert.php?act=add'));

    assign_query_info();
    $smarty->display('global_alert_list.htm');
} elseif ($_REQUEST['act'] == "edit" || $_REQUEST['act'] == "add") {
    $alertController->editAction();
} elseif ($_REQUEST['act'] == "update" || $_REQUEST['act'] == "insert") {
    $alertController->updateAction();
} elseif ($_REQUEST['act'] == "query") {
    $alertController->ajaxQueryAction();
}


?>