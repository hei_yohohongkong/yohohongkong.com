<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_base.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_order.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_supplier.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
$controller = new Yoho\cms\Controller\YohoBaseController('erp_order');
$adminuserController = new Yoho\cms\Controller\AdminuserController();
$accountingController = new Yoho\cms\Controller\AccountingController();
@ini_set("memory_limit","256M");

$smarty->assign('manage_cost',  $_SESSION['manage_cost']);
$smarty->assign('is_accountant',  $_SESSION['is_accountant']);
if($_REQUEST['act'] == 'order_list')
{
	if(admin_priv('erp_order_view','',false) ||admin_priv('erp_order_manage','',false) ||admin_priv('erp_order_rate','',false) ||admin_priv('erp_order_approve','',false) )
	{
		// $page=isset($_REQUEST['page']) &&intval($_REQUEST['page']) >0 ?intval($_REQUEST['page']):1;
		// $order_status=isset($_REQUEST['o_s']) &&intval($_REQUEST['o_s']) >0 ?intval($_REQUEST['o_s']) : 0;
		// $supplier_id=isset($_REQUEST['s_id']) &&intval($_REQUEST['s_id']) >0 ?intval($_REQUEST['s_id']) : 0;
		// $purchaser_id=isset($_REQUEST['p_id']) &&intval($_REQUEST['p_id']) >0 ?intval($_REQUEST['p_id']) : 0;
		// $start_date=isset($_REQUEST['s_date'])?trim($_REQUEST['s_date']) : '';
		// $end_date=isset($_REQUEST['e_date'])?trim($_REQUEST['e_date']) : '';
		// $order_sn=isset($_REQUEST['order_sn'])?trim($_REQUEST['order_sn']) : '';
		// $payable=isset($_REQUEST['payable'])?intval($_REQUEST['payable']) : 0;
		// $pending_receive=isset($_REQUEST['pending_receive'])?intval($_REQUEST['pending_receive']) : 0;
		// include('./includes/ERP/page.class.php');
		// $num_per_page=20;
		// $mode=1;
		// $page_bar_num=6;
		// $page_style="page_style";
		// $current_page_style="current_page_style";
		// $start=$num_per_page*($page-1);
		// $cls_date=new cls_date();
		// $start_time=!empty($start_date)?$cls_date->date_to_stamp($start_date): 0;
		// $end_time=!empty($end_date)?$cls_date->date_to_stamp($end_date):0;
		// $agency_id=get_admin_agency_id($_SESSION['admin_id']);
		// $paras=array(
		// 	'order_status'=>$order_status,
		// 	'supplier_id'=>$supplier_id,
		// 	'purchaser_id'=>$purchaser_id,
		// 	'start_time'=>$start_time,
		// 	'end_time'=>$end_time,
		// 	'order_sn'=>$order_sn,
		// 	'agency_id'=>$agency_id,
		// 	'payable'=>$payable,
		// 	'pending_receive'=>$pending_receive
		// );
		// $total_num=get_order_count($paras);
		// $order_list=get_order_list($paras,$start,$num_per_page);
		// $smarty->assign('order_list',$order_list);
		// $url=fix_url($_SERVER['REQUEST_URI'],array('page'=>'','o_s'=>$order_status,'s_id'=>$supplier_id,'p_id'=>$purchaser_id,'s_date'=>$start_date,'e_date'=>$end_date,'order_sn'=>$order_sn));
		// $smarty->assign('o_s',$order_status);
		// $smarty->assign('s_id',$supplier_id);
		// $smarty->assign('p_id',$purchaser_id);
		// $smarty->assign('s_date',$start_date);
		// $smarty->assign('e_date',$end_date);
		// $smarty->assign('order_sn',$order_sn);
		// $smarty->assign('agency_id',$agency_id);
		// $smarty->assign('order_status',get_order_status());
		// $pager=new page(array('total_data'=>$total_num,'data_per_page'=>$num_per_page,'url'=>$url,'mode'=>$mode,'page_bar_num'=>$page_bar_num,'page_style'=>$page_style,'current_page_style'=>$current_page_style));
		// $smarty->assign('pager',$pager->show());
		
		// $order_list = get_erp_order_listtable();
		// $smarty->assign('order_list',   $order_list['order_list']);
		// $smarty->assign('filter',       $order_list['filter']);
		// $smarty->assign('record_count', $order_list['record_count']);
		// $smarty->assign('page_count',   $order_list['page_count']);
		$smarty->assign('o_s',          empty($_REQUEST['o_s']) ? 0 : intval($_REQUEST['o_s']));
		$smarty->assign('s_id',         empty($_REQUEST['s_id']) ? 0 : intval($_REQUEST['s_id']));
		$smarty->assign('p_id',         empty($_REQUEST['p_id']) ? 0 : intval($_REQUEST['p_id']));
		$smarty->assign('t_id',         empty($_REQUEST['t_id']) ? '' : $_REQUEST['t_id']);		
		$smarty->assign('s_date',       empty($_REQUEST['s_date']) ? '' : trim($_REQUEST['s_date']));
		$smarty->assign('e_date',       empty($_REQUEST['e_date']) ? '' : trim($_REQUEST['e_date']));
		$smarty->assign('order_sn',     empty($_REQUEST['order_sn']) ? '' : trim($_REQUEST['order_sn']));
		$smarty->assign('full_page',    1);
		
		$smarty->assign('agency_id', get_admin_agency_id($_SESSION['admin_id']));
		$smarty->assign('order_status', get_order_status());
		if((admin_priv('erp_order_manage','',false)))
		{
			$action_link = array('href'=>'erp_order_manage.php?act=add_order','text'=>$_LANG['erp_add_order']);
			$action_link2 = array('href'=>'erp_order_manage.php?act=add_credit','text'=>$_LANG['erp_add_credit']);
			$smarty->assign('action_link',$action_link);
			$smarty->assign('action_link2',$action_link2);
		}
		if((admin_priv('erp_order_manage','',false)) &&!(admin_priv('erp_order_rate','',false)) &&!(admin_priv('erp_order_approve','',false)))
		{
			$smarty->assign('supplier_list',get_admin_supplier(erp_get_admin_id()));
		}
		else
		{
			if($_SESSION['manage_cost'])
				$smarty->assign('supplier_list',get_admin_supplier(1));
			elseif(check_authz(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ONLYSELFSUPPLIERS))
				$smarty->assign('supplier_list',get_admin_supplier(erp_get_admin_id(),true));
			else
				$smarty->assign('supplier_list',get_admin_supplier(0));
		}
		$purchaser_list=array();
		$sql="select distinct create_by from ".$GLOBALS['ecs']->table('erp_order')." where 1";
		if($agency_id >0)
		{
			$sql.=" and agency_id='".$agency_id."'";
		}
		$res=$GLOBALS['db']->query($sql);
		while($row=mysql_fetch_assoc($res))
		{
			$sql="select user_name from ".$GLOBALS['ecs']->table('admin_user')." where user_id='".$row['create_by']."'";
			$purchaser_list[$row['create_by']]=$GLOBALS['db']->getOne($sql);
		}

		$smarty->assign('purchaser_list',$purchaser_list);
		$smarty->assign('po_order_type',Yoho\cms\Controller\ErpController::PO_ORDER_TYPE);
		$smarty->assign('ur_here',$GLOBALS['_LANG']['erp_order_list']);
		assign_query_info();
		$smarty->assign('po_order_type',Yoho\cms\Controller\ErpController::PO_ORDER_TYPE);
		$smarty->display('erp_order_list.htm');
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif($_REQUEST['act'] == 'query')
{
	if(admin_priv('erp_order_view','',false) ||admin_priv('erp_order_manage','',false) ||admin_priv('erp_order_rate','',false) ||admin_priv('erp_order_approve','',false) )
	{
		if ((!empty($_REQUEST['s_date'])) && (strstr($_REQUEST['s_date'], '-') === false))
	    {
	        $_REQUEST['s_date'] = local_date('Y-m-d', $_REQUEST['s_date']);
		}
		if ((!empty($_REQUEST['e_date'])) && (strstr($_REQUEST['e_date'], '-') === false))
		{
	        $_REQUEST['e_date'] = local_date('Y-m-d', $_REQUEST['e_date']);
	    }
		
		$order_list = get_erp_order_listtable();
		$controller->ajaxQueryAction($order_list['order_list'], $order_list['record_count'], false);
		// $smarty->assign('order_list',   $order_list['order_list']);
		// $smarty->assign('filter',       $order_list['filter']);
		// $smarty->assign('record_count', $order_list['record_count']);
		// $smarty->assign('page_count',   $order_list['page_count']);
		// $smarty->assign('o_s',          empty($_REQUEST['o_s']) ? 0 : intval($_REQUEST['o_s']));
		// $smarty->assign('s_id',         empty($_REQUEST['s_id']) ? 0 : intval($_REQUEST['s_id']));
		// $smarty->assign('p_id',         empty($_REQUEST['p_id']) ? 0 : intval($_REQUEST['p_id']));
		// $smarty->assign('t_id',         empty($_REQUEST['t_id']) ? '' : intval($_REQUEST['t_id']));		
		// $smarty->assign('s_date',       empty($_REQUEST['s_date']) ? '' : trim($_REQUEST['s_date']));
		// $smarty->assign('e_date',       empty($_REQUEST['e_date']) ? '' : trim($_REQUEST['e_date']));
		// $smarty->assign('order_sn',     empty($_REQUEST['order_sn']) ? '' : trim($_REQUEST['order_sn']));
		
		// make_json_result($smarty->fetch('erp_order_list.htm'), '', array('filter' => $order_list['filter'], 'page_count' => $order_list['page_count']));
	}
	else
	{
		make_json_error($_LANG['erp_no_permit']);
	}
}
elseif($_REQUEST['act'] == 'add_order')
{
	if((admin_priv('erp_order_manage','',false)))
	{
		$supplier_info=get_admin_supplier(erp_get_admin_id(), 1);
		if(empty($supplier_info))
		{
			$href="erp_order_manage.php?act=order_list";
			$text=$_LANG['erp_order_return_to_order_list'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_order_no_admin_supplier'],0,$link);
		}
		// YOHO: Default to "NA" supplier
		$default_supplier_id = $db->getOne("SELECT supplier_id FROM ".$GLOBALS['ecs']->table('erp_supplier')." WHERE name = 'NA'");
		$order_id=add_order($default_supplier_id);
		$main_warehouse_id = $erpController->getMainWarehouseId();

		if($order_id!=false)
		{
			$erpController->update_erp_order_warehouse_id($order_id,$main_warehouse_id);

			if(lock_order($order_id,'edit'))
			{
				$warehouse_list = $erpController->getWarehouseList(0,0);

				$smarty->assign('warehouse_list',$warehouse_list);
				$smarty->assign('url',$_SERVER["REQUEST_URI"]);
				$smarty->assign('supplier_info',$supplier_info);
				$order_info=get_order_info($order_id);
				$order_item_info=get_order_item_info($order_id);
				$smarty->assign('order_info',$order_info);
				$smarty->assign('order_item_info',$order_item_info);
				if($order_info['order_status']==4)
				{
					$href="erp_order_manage.php?act=print_order&order_id=".$order_id."&page=1";
					$text=$_LANG['erp_order_operation_print'];
					$action_link = array('href'=>$href,'text'=>$text);
					$smarty->assign('action_link',$action_link);
				}
				//$salespeople = $db->getCol("SELECT `sales_name` FROM " . $ecs->table('salesperson') . " WHERE available = 1");
				$salespeople = $adminuserController->getAdminsByPriv(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_PO_OP, 'sales_name');
	            $smarty->assign('salesperson_list', $salespeople);
				$smarty->assign('act','edit');
				$smarty->assign('ur_here',$GLOBALS['_LANG']['erp_edit_order']);
				assign_query_info();
				$smarty->assign('po_order_type',Yoho\cms\Controller\ErpController::PO_ORDER_TYPE);
				$smarty->assign('currencies', $accountingController->getCurrencies());
				$smarty->display('erp_order_info.htm');
			}
			else
			{
				$href="./erp_order_manage.php?act=order_list";
				$text=$_LANG['erp_order_return_to_order_list'];
				$link[] = array('href'=>$href,'text'=>$text);
				$href="erp_order_manage.php?act=view_order&order_id=".$order_id;
				$text=$_LANG['erp_order_view_order'];
				$link[] = array('href'=>$href,'text'=>$text);
				sys_msg($_LANG['erp_order_no_accessibility'],0,$link);
			}
		}
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif($_REQUEST['act'] == 'view_order')
{
	if(admin_priv('erp_order_view','',false) ||admin_priv('erp_order_manage','',false) ||admin_priv('erp_order_rate','',false) ||admin_priv('erp_order_approve','',false) )
	{
		$order_id=isset($_REQUEST['order_id']) &&intval($_REQUEST['order_id'])>0?intval($_REQUEST['order_id']):0;
		$order_info=get_order_info($order_id);
		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		if($agency_id>0 &&$order_info['agency_id']!=$agency_id)
		{
			$href="erp_order_manage.php?act=order_list";
			$text=$_LANG['erp_order_return'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_order_no_agency_access'],0,$link);
		}
		$order_item_info=get_order_item_info($order_id);
		$smarty->assign('order_info',$order_info);
		$smarty->assign('order_item_info',$order_item_info);
		$smarty->assign('act','view');
		$smarty->assign('ur_here',$GLOBALS['_LANG']['erp_view_order']);
		assign_query_info();
		$smarty->assign('currencies', $accountingController->getCurrencies());
		$smarty->assign('po_order_type',Yoho\cms\Controller\ErpController::PO_ORDER_TYPE);
		$smarty->display('erp_order_info.htm');
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif($_REQUEST['act'] == 'edit_order')
{
	if((admin_priv('erp_order_manage','',false)))
	{
		$warehouse_list = $erpController->getWarehouseList(erp_get_admin_id(),0);
		$smarty->assign('warehouse_list',$warehouse_list);

		$order_id=isset($_REQUEST['order_id']) &&intval($_REQUEST['order_id'])>0?intval($_REQUEST['order_id']):0;
		$order_info=get_order_info($order_id);
		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		if($agency_id>0 &&$order_info['agency_id']!=$agency_id)
		{
			$href="erp_order_manage.php?act=order_list";
			$text=$_LANG['erp_order_return'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_order_no_agency_access'],0,$link);
		}
		if(!is_admin_order($order_id,erp_get_admin_id()))
		{
			$href="erp_order_manage.php?act=order_list";
			$text=$_LANG['erp_order_return'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_order_no_access'],0,$link);
		}
		if(lock_order($order_id,'edit'))
		{
			$smarty->assign('url',$_SERVER["REQUEST_URI"]);
			$supplier_info=get_admin_supplier(erp_get_admin_id(), 1);
			$smarty->assign('supplier_info',$supplier_info);
			$order_item_info=get_order_item_info($order_id);
			$smarty->assign('order_info',$order_info);
			$smarty->assign('order_item_info',$order_item_info);
			if($order_info['order_status']==4)
			{
				$href="erp_order_manage.php?act=print_order&order_id=".$order_id."&page=1";
				$text=$_LANG['erp_order_operation_print'];
				$action_link = array('href'=>$href,'text'=>$text);
				$smarty->assign('action_link',$action_link);
			}
			//$salespeople = $db->getCol("SELECT `sales_name` FROM " . $ecs->table('salesperson') . " WHERE available = 1");
			$salespeople = $adminuserController->getAdminsByPriv(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_PO_OP, 'sales_name');
            $smarty->assign('salesperson_list', $salespeople);
			$smarty->assign('act','edit');
			$smarty->assign('ur_here',$GLOBALS['_LANG']['erp_edit_order']);
			assign_query_info();
			$smarty->assign('currencies', $accountingController->getCurrencies());
			$smarty->assign('po_order_type',Yoho\cms\Controller\ErpController::PO_ORDER_TYPE);
			$smarty->display('erp_order_info.htm');
		}
		else
		{
			$href="erp_order_manage.php?act=order_list";
			$text=$_LANG['erp_order_return_to_order_list'];
			$link[] = array('href'=>$href,'text'=>$text);
			$href="erp_order_manage.php?act=view_order&order_id=".$order_id;
			$text=$_LANG['erp_order_view_order'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_order_no_accessibility'],0,$link);
		}
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif($_REQUEST['act'] == 'rate_order')
{
	if((admin_priv('erp_order_rate','',false)))
	{
		$order_id=isset($_REQUEST['order_id']) &&intval($_REQUEST['order_id'])>0?intval($_REQUEST['order_id']):0;
		$order_info=get_order_info($order_id);
		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		if($agency_id>0 &&$order_info['agency_id']!=$agency_id)
		{
			$href="erp_order_manage.php?act=order_list";
			$text=$_LANG['erp_order_return'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_order_no_agency_access'],0,$link);
		}
		if(lock_order($order_id,'rate'))
		{
			$available_supplier_ids = array_map(function ($supplier) { return $supplier['supplier_id']; }, suppliers_list_name_by_admin());
			$order_item_info=get_order_item_info($order_id);
			$smarty->assign('is_self_po', in_array($order_info['supplier_id'], $available_supplier_ids));
			$smarty->assign('order_info',$order_info);
			$smarty->assign('order_item_info',$order_item_info);
			$smarty->assign('act','rate');
			$smarty->assign('ur_here',$GLOBALS['_LANG']['erp_rate_order']);
			assign_query_info();
			$smarty->assign('currencies', $accountingController->getCurrencies());
			$smarty->assign('po_order_type',Yoho\cms\Controller\ErpController::PO_ORDER_TYPE);
			$smarty->display('erp_order_info.htm');
		}
		else
		{
			$href="erp_order_manage.php?act=order_list";
			$text=$_LANG['erp_order_return_to_order_list'];
			$link[] = array('href'=>$href,'text'=>$text);
			$href="erp_order_manage.php?act=view_order&order_id=".$order_id;
			$text=$_LANG['erp_order_view_order'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_order_no_accessibility'],0,$link);
		}
	}
	else{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif($_REQUEST['act'] == 'approve_order')
{
	if((admin_priv('erp_order_approve','',false)))
	{
		$order_id=isset($_REQUEST['order_id']) &&intval($_REQUEST['order_id'])>0?intval($_REQUEST['order_id']):0;
		$order_info=get_order_info($order_id);
		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		if($agency_id>0 &&$order_info['agency_id']!=$agency_id)
		{
			$href="erp_order_manage.php?act=order_list";
			$text=$_LANG['erp_order_return'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_order_no_agency_access'],0,$link);
		}
		$order_item_info=get_order_item_info($order_id);
		$smarty->assign('order_info',$order_info);
		$smarty->assign('order_item_info',$order_item_info);
		$smarty->assign('act','approve');
		$smarty->assign('ur_here',$GLOBALS['_LANG']['erp_approve_order']);
		assign_query_info();
		$smarty->assign('currencies', $accountingController->getCurrencies());
		$smarty->assign('po_order_type',Yoho\cms\Controller\ErpController::PO_ORDER_TYPE);
		$smarty->display('erp_order_info.htm');
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif($_REQUEST['act'] == 'print_order')
{
	if(admin_priv('erp_order_manage','',false) ||admin_priv('erp_order_rate','',false) ||admin_priv('erp_order_approve','',false) || admin_priv(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ONLYSELFSUPPLIERS,'',false))
	{
		$order_id=isset($_REQUEST['order_id']) &&intval($_REQUEST['order_id'])>0?intval($_REQUEST['order_id']):0;
		$order_info=get_order_info($order_id);
		if(admin_priv(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ONLYSELFSUPPLIERS,'',false) && !(($order_info['create_by']==$_SESSION['admin_id']) || is_supplier_admin($order_info['supplier_id'],$_SESSION['admin_id']) || is_supplier_secondary_admin($order_info['supplier_id'],$_SESSION['admin_id'])))
			sys_msg($_LANG['erp_no_permit']." (您沒有被分派此供應商)",0,$link);

		$agency_id=get_admin_agency_id($_SESSION['admin_id']);
		if($agency_id>0 &&$order_info['agency_id']!=$agency_id)
		{
			$href="erp_order_manage.php?act=order_list";
			$text=$_LANG['erp_order_return'];
			$link[] = array('href'=>$href,'text'=>$text);
			sys_msg($_LANG['erp_order_no_agency_access'],0,$link);
		}

		$order_item_info=get_order_item_info($order_id);
		$smarty->assign('order_info',$order_info);
		$smarty->assign('order_item_info',$order_item_info);
		$smarty->assign('shop_name',$_CFG['shop_name']);
		$smarty->assign('total_page',count($order_item_info));
		assign_query_info();
		$smarty->display('erp_order_print.htm');
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif($_REQUEST['act'] == 'edit_expected_delivery_date')
{
	check_authz_json('erp_order_manage');
	
	$order_id = empty($_POST['id']) ? 0 : intval($_POST['id']);
	$date = empty($_POST['value']) ? 'N/A' : json_str_iconv(trim($_POST['value']));
	$date = ($date =='N/A') ? 0 : $date;
	$time = local_strtotime($date);
	
	if ($order_id == 0)
	{
		make_json_error('NO ORDER ID');
		exit;
	}
	
	$sql = 'UPDATE ' . $ecs->table('erp_order') . " SET expected_delivery_time='$time' WHERE order_id = '$order_id'";
	if ($db->query($sql))
	{
		make_json_result(local_date('Y-m-d', $time));
	}
	else
	{
		make_json_error($GLOBALS['db']->errorMsg());
	}
}
else if($_REQUEST['act'] == 'check_email_priv'){
	if(!admin_priv('erp_order_receipt','',false))
	{
		die(json_encode(array('msg'=>'-1','message'=>$_LANG['erp_no_permit'])));
	}
	die(json_encode(array()));
}
else if($_REQUEST['act'] == 'edit_email' || $_REQUEST['act'] == 'retrieve_orders' || $_REQUEST['act'] == 'retrieve_receipt' || $_REQUEST['act'] == 'remove_receipt')
{
	if(!admin_priv('erp_order_receipt','',false))
	{
		die(json_encode(array('msg'=>'-1')));
	}
	if($_REQUEST['act'] == 'edit_email')
	{
		if(isset($_REQUEST['req'])){
			if($_REQUEST['req'] == 'retrieve' && isset($_REQUEST['order_id'])){
				$sql = "SELECT * FROM ". $ecs->table('erp_order') . " AS o " .
						"LEFT JOIN " . $ecs->table('erp_supplier') . " AS s ON s.supplier_id = o.supplier_id " .
						"WHERE o.order_id = '" . $_REQUEST['order_id'] ."'";
				$res = $db->getRow($sql);
				$supplier_id = $res['supplier_id'];
				$main_admin_sql = "select distinct(admin_id) from ".$GLOBALS['ecs']->table('erp_supplier_admin')." where supplier_id='".$supplier_id."'";
				$main_admin_res = $GLOBALS['db']->getAll($main_admin_sql);
				$secondary_admin_sql = "select distinct(admin_id) from ".$GLOBALS['ecs']->table('erp_supplier_secondary_admin')." where supplier_id='".$supplier_id."'";
				$secondary_admin_res = $GLOBALS['db']->getAll($secondary_admin_sql);
				$all_admin = array_merge($main_admin_res, $secondary_admin_res);
				$all_admin_length = count($all_admin);
				$all_admin_email = "";
				for ($i=0; $i<$all_admin_length; $i++) {
					$admin_id = $all_admin[$i]["admin_id"];
					$admin_email_sql = "SELECT email FROM `ecs_admin_user` WHERE `user_id` = '".$admin_id."'";
					$admin_email = $db->getOne($admin_email_sql);
					if($i<$all_admin_length-1){
						$all_admin_email .= $admin_email . ",";
					} else {
						$all_admin_email .= $admin_email;
					}		
				}
				$email = isset($res['bank_contact']) ? $res['bank_contact'] : "";
				//$contact = isset($res['contact']) ? $res['contact'] : "";
				$order_sn = isset($res['order_sn']) ? $res['order_sn'] : "";
				echo json_encode(array('email'=>$email,'contact'=>$all_admin_email,'order_sn'=>$order_sn));
			}else if($_REQUEST['req'] == 'send'){
				require_once(ROOT_PATH . 'includes/cls_image.php');
				if(isset($_REQUEST['email_order_sn']) && isset($_REQUEST['email_addr'])){
					if (!file_exists(ROOT_PATH . DATA_DIR . '/paymentreceipt/')) {
						mkdir(ROOT_PATH . DATA_DIR . '/paymentreceipt/', 0777, true);
					}
					$file_location = ROOT_PATH . DATA_DIR . '/paymentreceipt/' . date('Ymd',time()) . '_' . $_REQUEST['email_order_sn'] . '_payment_receipt.'  . pathinfo($_FILES['email_file']['name'])['extension'];
					$upload_success = 0;
					if($_FILES['email_file']['size'] > 500 * 1024 && strpos($_FILES['email_file']['type'],"image") !== false){
						$img_info = getimagesize($_FILES['email_file']['tmp_name']);
						$img = new cls_image();
						$thumb = $img->make_thumb($_FILES['email_file']['tmp_name'],800,$img_info[1] / $img_info[0] * 800,ROOT_PATH . DATA_DIR . '/paymentreceipt/');
						if(file_exists(ROOT_PATH . $thumb)){
							if(rename(ROOT_PATH . $thumb,$file_location)){
								$upload_success = 1;
							}else{
								echo json_encode(array('msg'=>'2','debug'=>'101'));
							}
						}else{
							echo json_encode(array('msg'=>'2','debug'=>'100'));
						}
					}else if(isset($_FILES['email_file']) && $_FILES['email_file']['error'] == UPLOAD_ERR_OK){
						if(move_upload_file($_FILES['email_file']['tmp_name'], $file_location)){
							$upload_success = 2;
						}else{
							echo json_encode(array('msg'=>'2'));
						}
					}else{
						echo json_encode(array('msg'=>'3','debug'=>$_FILES['email_file']['error']));
					}
					if($upload_success > 0){
						if(file_exists($file_location)){
							$orders = "'" .$_REQUEST['email_order_sn'] . "'";
							$memo = "";
							$memo_sum = "";
							if(isset($_REQUEST['email_multiple_order']) && $_REQUEST['email_multiple_order'] != ""){
								$orders .= ',' . stripslashes($_REQUEST['email_multiple_order']);
								$memo = "已一併支付內部編號 <b>[" . implode("]</b>, <b>[",explode(",",str_replace("'","",$orders))) . "]</b>之訂單。";
							}
							$sql = "UPDATE " . $ecs->table('erp_order') . " SET receipt_location = '" .
							date('Ymd',time()) . '_' . $_REQUEST['email_order_sn'] . '_payment_receipt.'  . pathinfo($_FILES['email_file']['name'])['extension'] . "' " .
							"WHERE order_sn IN (" . $orders . ")";
							$db->query($sql);
							
							$recipient = isset($_REQUEST['email_recipient']) ? $_REQUEST['email_recipient'] : "";
							$tpl = get_mail_template('payment_receipt');
							$smarty->assign('order_sn', $_REQUEST['email_order_sn']);
							if(isset($_REQUEST['email_memo']) && $_REQUEST['email_memo'] != ""){
								$memo_sum = $_REQUEST['email_memo'];
								if($memo != "")
									$memo_sum .= "<br /><br />" . $memo;
							} else if($memo != "") {
								$memo_sum = $memo;
							}
							if($memo_sum != "")
								$smarty->assign('memo', $memo_sum);
							$smarty->assign('shop_name', $_CFG['shop_name']);
							$smarty->assign('send_date', local_date($_CFG['date_format']));
							if (file_exists($file_location))
							{
								$attachment = $file_location;
								$email_addr = explode(",",$_REQUEST['email_addr']);
								$recipients = explode(",",$recipient);
								$emails = array_merge($email_addr,$recipients);
								for($i = 0; $i < count($emails); $i++){
									// $names[$i] = isset($recipients[$i]) ? $recipients[$i] : "";
									$names[$i] = "";
								}
								$content = $smarty->fetch('str:' . $tpl['template_content']);
								send_mail_with_attachment($names,$emails,$tpl['template_subject'],$content,1,false,$attachment);
								echo json_encode(array('msg'=>'0'));
							}else{
								echo json_encode(array('msg'=>'1'));
							}
						}
					}
				}else{
					echo json_encode(array('msg'=>'4'));
				}
			}
		}
	}
	else if($_REQUEST['act'] == 'retrieve_orders')
	{
		$sql = "SELECT supplier_id FROM " . $ecs->table('erp_order') .
				" WHERE order_sn ='" . $_REQUEST['order_sn'] ."'";
		$sup_id = $db->getOne($sql);
		$sql = "SELECT order_sn FROM ". $ecs->table('erp_order') .
				" WHERE supplier_id = " . $sup_id . " AND fully_paid = 1 AND (receipt_location IS NULL OR receipt_location = '') AND order_sn != '" . $_REQUEST['order_sn'] ."' ORDER BY order_id DESC";
		$orders = $db->getCol($sql);
		echo json_encode(array('orders'=>$orders));
	}
	else if($_REQUEST['act'] == 'retrieve_receipt')
	{
		$sql = "SELECT receipt_location,order_sn FROM ". $ecs->table('erp_order') .
			" WHERE order_id = '" . $_REQUEST['order_id'] ."'";
		$res = $db->getRow($sql);
		$full_path = ROOT_PATH . DATA_DIR . '/paymentreceipt/' . $res['receipt_location'];
		if(file_exists($full_path)){
			$type = pathinfo($full_path, PATHINFO_EXTENSION);
			$data = file_get_contents($full_path);
			$dataUri = 'data:' . mime_content_type($full_path) . ';base64,' . base64_encode($data);
			$type = $type == 'pdf' ? 'pdf' : 'image';
			echo json_encode(array('src'=>$dataUri,'order_sn'=>$res['order_sn'],'type'=>$type));
		}else{
			echo json_encode(array('error'=>0,'order_sn'=>$res['order_sn']));
		}
	}
	else if($_REQUEST['act'] == 'remove_receipt')
	{
		$sql = "SELECT receipt_location FROM ". $ecs->table('erp_order') . " WHERE order_sn = '" . $_REQUEST['order_sn'] ."'";
		$location = $db->getOne($sql);
		$full_path = ROOT_PATH . DATA_DIR . '/paymentreceipt/' . $location;
		$sql = "UPDATE ". $ecs->table('erp_order') . "SET receipt_location = NULL WHERE receipt_location = '" . $location ."'";
		$db->query($sql);
		if(file_exists($full_path))
			unlink($full_path);
		echo json_encode(array('msg'=>'5'));
	}
}
else if($_REQUEST['act'] == 'add_credit')
{
	if(admin_priv('erp_order_manage','',false))
	{
		if($_REQUEST['ajax'])
		{
			$supplier_code = isset($_REQUEST['supplier_code']) ? trim($_REQUEST['supplier_code']) : '';
			$amount = !empty($_REQUEST['amount']) ? floatval($_REQUEST['amount']) : 0;
			$remark = !empty($_REQUEST['remark']) ? trim($_REQUEST['remark']) : "";
			if(empty($amount))
			{
				sys_msg("請輸入金額",1);
			}
			if($supplier_info = is_supplier_exist('',$supplier_code))
			{
				$param = array(
					'cn_sn' 		=> gen_credit_note_sn(),
					'supplier_id' 	=> $supplier_info['supplier_id'],
					'admin_id' 		=> $_SESSION['admin_id'],
					'init_amount' 	=> $amount,
					'curr_amount' 	=> $amount,
					'remark'		=> $remark
				);

				if(!empty($_REQUEST['goods_id']))
				{
					$param['goods_id'] = $_REQUEST['goods_id'];
				}

				if(add_credit_note($param))
				{
					sys_msg("成功".$_LANG['erp_add_credit'],0,array(array('href'=>'credit_note_report.php?act=list_credit','text'=>$_LANG['erp_credit_list'])));
				}else
				{
					sys_msg($_LANG['erp_add_credit']."失敗",1);
				}
			}
		}
		else
		{
			$supplier_info=get_admin_supplier(erp_get_admin_id(), 1);
			$smarty->assign('full_page',    1);
			$action_link = array('href'=>'erp_order_manage.php?act=order_list','text'=>$_LANG['erp_order_return']);
			$action_link2 = array('href'=>'credit_note_report.php?act=summary','text'=>$_LANG['erp_credit_summary']);
			$smarty->assign('action_link', $action_link);
			$smarty->assign('action_link2', $action_link2);
			$smarty->assign('supplier_info', $supplier_info);
			$smarty->assign('ur_here', $_LANG['erp_add_credit']);
			$smarty->assign('cn_edit', admin_priv('erp_credit_note','',false));
			assign_query_info();
			$smarty->display('erp_credit_note.htm');
		}
	}
	else
	{
		$href="erp_order_manage.php?act=order_list";
		$text=$_LANG['erp_order_return'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
else if($_REQUEST['act'] == 'ajaxEdit') {
	$controller->ajaxEditAction();
}
elseif ($_REQUEST['act'] == 'order_payment_list') {
	if(admin_priv('erp_order_view','',false) ||admin_priv('erp_order_manage','',false) ||admin_priv('erp_order_rate','',false) ||admin_priv('erp_order_approve','',false) )
	{
		$smarty->assign('o_s',          empty($_REQUEST['o_s']) ? 0 : intval($_REQUEST['o_s']));
		$smarty->assign('s_id',         empty($_REQUEST['s_id']) ? 0 : intval($_REQUEST['s_id']));
		$smarty->assign('p_id',         empty($_REQUEST['p_id']) ? 0 : intval($_REQUEST['p_id']));
		$smarty->assign('t_id',         empty($_REQUEST['t_id']) ? '' : $_REQUEST['t_id']);		
		$smarty->assign('s_date',       empty($_REQUEST['s_date']) ? '' : trim($_REQUEST['s_date']));
		$smarty->assign('e_date',       empty($_REQUEST['e_date']) ? '' : trim($_REQUEST['e_date']));
		$smarty->assign('order_sn',     empty($_REQUEST['order_sn']) ? '' : trim($_REQUEST['order_sn']));
		$smarty->assign('full_page',    1);
		
		$smarty->assign('agency_id', get_admin_agency_id($_SESSION['admin_id']));
		$smarty->assign('order_status', get_order_status());
		if((admin_priv('erp_order_manage','',false)) &&!(admin_priv('erp_order_rate','',false)) &&!(admin_priv('erp_order_approve','',false)))
		{
			$smarty->assign('supplier_list',get_admin_supplier(erp_get_admin_id()));
		}
		else
		{
			if($_SESSION['manage_cost'])
				$smarty->assign('supplier_list',get_admin_supplier(1));
			elseif(check_authz(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ONLYSELFSUPPLIERS))
				$smarty->assign('supplier_list',get_admin_supplier(erp_get_admin_id()));
			else
				$smarty->assign('supplier_list',get_admin_supplier(0));
		}
		$purchaser_list=array();
		$sql="select distinct create_by from ".$GLOBALS['ecs']->table('erp_order')." where 1";
		if($agency_id >0)
		{
			$sql.=" and agency_id='".$agency_id."'";
		}
		$res=$GLOBALS['db']->query($sql);
		while($row=mysql_fetch_assoc($res))
		{
			$sql="select user_name from ".$GLOBALS['ecs']->table('admin_user')." where user_id='".$row['create_by']."'";
			$purchaser_list[$row['create_by']]=$GLOBALS['db']->getOne($sql);
		}
		$smarty->assign('purchaser_list',$purchaser_list);
		$smarty->assign('po_order_type',Yoho\cms\Controller\ErpController::PO_ORDER_TYPE);
		$smarty->assign('ur_here',$GLOBALS['_LANG']['erp_order_list_pending_payment']);
		assign_query_info();
		$smarty->assign('po_order_type',Yoho\cms\Controller\ErpController::PO_ORDER_TYPE);
		$smarty->display('erp_order_payment_list.htm');
	}
	else
	{
		$href="index.php?act=main";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
	}
}
elseif($_REQUEST['act'] == 'payment_query')
{
	if(admin_priv('erp_order_view','',false) ||admin_priv('erp_order_manage','',false) ||admin_priv('erp_order_rate','',false) ||admin_priv('erp_order_approve','',false) )
	{
		if ((!empty($_REQUEST['s_date'])) && (strstr($_REQUEST['s_date'], '-') === false))
	    {
	        $_REQUEST['s_date'] = local_date('Y-m-d', $_REQUEST['s_date']);
		}
		if ((!empty($_REQUEST['e_date'])) && (strstr($_REQUEST['e_date'], '-') === false))
		{
	        $_REQUEST['e_date'] = local_date('Y-m-d', $_REQUEST['e_date']);
	    }
		
		$order_list = get_erp_order_listtable();
		$controller->ajaxQueryAction($order_list['order_list'], $order_list['record_count'], false);
	}
	else
	{
		make_json_error($_LANG['erp_no_permit']);
	}
}

function get_erp_order_listtable()
{
	$order_status=isset($_REQUEST['o_s']) &&intval($_REQUEST['o_s']) >0 ?intval($_REQUEST['o_s']) : 0;
	$supplier_id=isset($_REQUEST['s_id']) &&intval($_REQUEST['s_id']) >0 ?intval($_REQUEST['s_id']) : 0;
	$purchaser_id=isset($_REQUEST['p_id']) &&intval($_REQUEST['p_id']) >0 ?intval($_REQUEST['p_id']) : 0;
	$type_id=isset($_REQUEST['t_id']) ?$_REQUEST['t_id'] : '';
	$s_date=isset($_REQUEST['s_date'])?trim($_REQUEST['s_date']) : '';
	$e_date=isset($_REQUEST['e_date'])?trim($_REQUEST['e_date']) : '';
	$order_sn=isset($_REQUEST['order_sn'])?trim($_REQUEST['order_sn']) : '';
	$payable=isset($_REQUEST['payable'])?intval($_REQUEST['payable']) : 0;
	$pending_receive=isset($_REQUEST['pending_receive'])?intval($_REQUEST['pending_receive']) : 0;
	$start_time=!empty($s_date)?local_strtotime($s_date): 0;
	$end_time=!empty($e_date)?local_strtotime($e_date):0;
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	$start=isset($_REQUEST['start'])?intval($_REQUEST['start']) : 0;
	$page_size=isset($_REQUEST['page_size'])?intval($_REQUEST['page_size']) : 50;
	$good_key=isset($_REQUEST['good_key'])?trim($_REQUEST['good_key']) : '';
	$sort_by=isset($_REQUEST['sort_by'])?trim($_REQUEST['sort_by']) : '';
	$sort_order=isset($_REQUEST['sort_order'])?trim($_REQUEST['sort_order']) : '';
	$delivery_status=isset($_REQUEST['delivery_status'])?trim($_REQUEST['delivery_status']) : '';
	$order_paid=isset($_REQUEST['payment_status'])?trim($_REQUEST['payment_status']) : '';

	$filter = array(
		// Search parameters
		'order_status' => $order_status,
		'supplier_id' => $supplier_id,
		'purchaser_id' => $purchaser_id,
		'type' => $type_id,
		'start_time' => $start_time,
		'end_time' => $end_time,
		'order_sn' => $order_sn,
		'agency_id' => $agency_id,
		'payable' => $payable,
		'pending_receive' => $pending_receive,
		// Parameters with different name
		'o_s' => $order_status,
		's_id' => $supplier_id,
		'p_id' => $purchaser_id,
		's_date' => $s_date,
		'e_date' => $e_date,
		'good_key' => $good_key,
		'sort_by' => $sort_by,
		'sort_order' => $sort_order,
		'delivery_status' => $delivery_status,
		'order_paid' => $order_paid
	);
	
	$record_count = get_order_count($filter);

	$order_list = get_order_list($filter, $start,$page_size);
	
	$arr = array(
        'order_list' => $order_list,
        'record_count' => $record_count
    );
    return $arr;
}
?>