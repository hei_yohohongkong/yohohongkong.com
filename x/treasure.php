<?php

define('IN_ECS', true);
require dirname(__FILE__) . '/includes/init.php';
$controller = new Yoho\cms\Controller\YohoBaseController('treasure');
if ($_REQUEST['act'] == 'list') {
    $smarty->assign("status", $_CFG['treasure_start']);
    $smarty->assign("notice", $_CFG['treasure_notice']);
    $smarty->assign("promise", $_CFG['treasure_promise_id']);
    $smarty->assign("detail", $_CFG['treasure_promise_detail']);
    $smarty->assign("action_link_list", [["text" => "隨機產品連結", "href" => "treasure.php?act=gen_goods_link"], ["text" => "上傳Excel", "href" => "treasure.php?act=import"]]);
    $smarty->display("treasure_list.htm");
} elseif ($_REQUEST['act'] == 'query') {
    $_REQUEST['sort_by']    = empty($_REQUEST['sort_by']) ? 'treasure_id' : trim($_REQUEST['sort_by']);
    $_REQUEST['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
    $_REQUEST['start']      = empty($_REQUEST['start']) ? 0 : $_REQUEST['start'];
    $_REQUEST['page_size']  = empty($_REQUEST['page_size']) ? 200 : $_REQUEST['page_size'];

    if (in_array($_REQUEST['sort_by'], ['coupon_id', 'start_time', 'end_time'])) {
        $_REQUEST['sort_by'] = "t.$_REQUEST[sort_by]";
    }
    $sql = "SELECT t.*, c.coupon_code FROM " . $ecs->table("treasure") . " t " .
            "LEFT JOIN " . $ecs->table("coupons") . " c ON c.coupon_id = t.coupon_id " .
            "ORDER BY $_REQUEST[sort_by] $_REQUEST[sort_order] " .
            "LIMIT $_REQUEST[start], $_REQUEST[page_size]";
    $res = $db->getAll($sql);
    foreach ($res as $key => $row) {
        $res[$key]['start_date'] = local_date("Y-m-d H:i", $row['start_time']);
        $res[$key]['end_date'] = local_date("Y-m-d H:i", $row['end_time']);
        $res[$key]['opened'] = empty($row['open_time']) ? 0 : 1;
        
        if (!empty($row['coupon_id'])) {
            $res[$key]['coupon_code'] = "<a href='coupon.php?act=info&id=$row[coupon_id]' target='_blank'>$row[coupon_code]</a>";
        }
    }
    $count = $db->getOne("SELECT COUNT(*) FROM " . $ecs->table("treasure"));
    $controller->ajaxQueryAction($res, $count);
} elseif ($_REQUEST['act'] == 'gen_goods_link') {
    $res = [];
    if (!empty($_REQUEST['search_type'])) {
        $search_value = trim($_REQUEST['search_value']);
        if ($_REQUEST['search_type'] == "any" || empty($search_value)) {
            $list = $db->getCol("SELECT goods_id FROM " . $ecs->table("goods") . " WHERE is_delete = 0 AND is_on_sale = 1 AND is_real = 1");
        } elseif ($_REQUEST['search_type'] == "goods_name") {
            $list = $db->getCol("SELECT goods_id FROM " . $ecs->table("goods") . " WHERE is_delete = 0 AND is_on_sale = 1 AND is_real = 1 AND $_REQUEST[search_type] LIKE '%$search_value%'");
        } else {
            $list = $db->getCol("SELECT goods_id FROM " . $ecs->table("goods") . " WHERE is_delete = 0 AND is_on_sale = 1 AND is_real = 1 AND $_REQUEST[search_type] = '$search_value'");
        }
        shuffle($list);
        $max = empty($_REQUEST['max']) ? 50 : $_REQUEST['max'];
        foreach ($list as $goods_id) {
            if (count($res) >= $max) continue;
            $res[] = $goods_id;
        }
    }
    $smarty->assign("action_link_list", [["text" => "寶箱列表", "href" => "treasure.php?act=list"], ["text" => "上傳Excel", "href" => "treasure.php?act=import"]]);
    $smarty->assign("goods_ids", $res);
    $smarty->assign("count", count($res));
    $smarty->assign('cat_list', cat_list(0));
    $smarty->assign('brand_list', get_brand_list());
    $smarty->display("treasure_goods.htm");
} elseif ($_REQUEST['act'] == 'import') {
    $smarty->assign("action_link_list", [["text" => "寶箱列表", "href" => "treasure.php?act=list"], ["text" => "隨機產品連結", "href" => "treasure.php?act=gen_goods_link"]]);
    $smarty->display("treasure_import.htm");
} elseif ($_REQUEST['act'] == 'upload') {
    ini_set('memory_limit', '256M');
    define('PCLZIP_TEMPORARY_DIR', ROOT_PATH . 'temp/');

    include_once ROOT_PATH . 'includes/excel/PHPExcel.php';
    include_once ROOT_PATH . 'includes/excel/PHPExcel/IOFactory.php';
    \PHPExcel_Settings::setZipClass(\PHPExcel_Settings::PCLZIP);
    $fileName     = $_FILES["import_excel"]["tmp_name"];
    $fileType     = \PHPExcel_IOFactory::identify($fileName);
    $objReader    = \PHPExcel_IOFactory::createReader($fileType);
    $objPHPExcel  = $objReader->load($fileName);
    $objWorksheet = $objPHPExcel->getSheet(0);

    $start_time = local_strtotime(explode(" 至 ", $_POST['range'])[0]);
    $end_time = local_strtotime(explode(" 至 ", $_POST['range'])[1]);
    $count = 0;
    if (!empty($start_time) && !empty($end_time)) {
        foreach ($objWorksheet->getRowIterator() as $r_key => $row) {
            if ($r_key > 1) {
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(false);
                foreach ($cellIterator as $c_key => $cell) {
                    switch ($c_key) {
                        case 0: 
                            $treasure_name = mysql_real_escape_string(trim($cell->getValue()));
                            break;
                        case 1: 
                            $treasure_desc = mysql_real_escape_string(trim($cell->getValue()));
                            break;
                        case 2: 
                            $treasure_tnc = mysql_real_escape_string(trim($cell->getValue()));
                            break;
                        case 3: 
                            $path = trim($cell->getValue());
                            break;
                        case 4: 
                            $coupon_id = intval($cell->getValue());
                            break;
                    }
                }
                
                if (!empty($treasure_name) && !empty($path)) {
                    $sql = "INSERT INTO " . $ecs->table("treasure") . " (treasure_name, treasure_desc, treasure_tnc, path, coupon_id, start_time, end_time) VALUES ('$treasure_name', '$treasure_desc', '$treasure_tnc', '$path', '$coupon_id', $start_time, $end_time)";
                    $db->query($sql);
                    $count++;
                }
            }
        }
    }
    sys_msg("已新增 $count 條紀錄", 0, [["text" => "寶箱列表", "href" => "treasure.php?act=list"]]);
} elseif ($_REQUEST['act'] == 'update_config') {
    $status = empty($_REQUEST['config']) ? 0 : intval($_REQUEST['config']);
    $notice = empty($_REQUEST['notice']) ? "" : mysql_real_escape_string(trim($_REQUEST['notice']));
    $promise = empty($_REQUEST['promise']) ? 0 : trim($_REQUEST['promise']);
    $detail = empty($_REQUEST['detail']) ? 0 : trim($_REQUEST['detail']);
    $db->query("UPDATE " . $ecs->table("shop_config") . " SET value = $status WHERE code = 'treasure_start'");
    $db->query("UPDATE " . $ecs->table("shop_config") . " SET value = '$notice' WHERE code = 'treasure_notice'");
    $db->query("UPDATE " . $ecs->table("shop_config") . " SET value = '$promise' WHERE code = 'treasure_promise_id'");
    $db->query("UPDATE " . $ecs->table("shop_config") . " SET value = '$detail' WHERE code = 'treasure_promise_detail'");
    clear_all_files();
    $_CFG = load_config();

    sys_msg("已更新設置", 0, [["text" => "寶箱列表", "href" => "treasure.php?act=list"]]);
} elseif ($_REQUEST['act'] == 'stat') {
    $start_time = empty($_REQUEST['start']) ? 0 : local_strtotime($_REQUEST['start'] . " 21:00:00");
    $end_time = empty($_REQUEST['end']) ? 0 : local_strtotime($_REQUEST['end'] . " 20:59:59");
    $where = empty($start_time) || empty($end_time) ? "" : " AND t.start_time >= $start_time AND t.end_time <= $end_time";
    echo "Treasure found: " . $db->getOne("SELECT COUNT(*) FROM " . $ecs->table("treasure") . " t WHERE open_time != 0" . $where) . "<br>";
    foreach ($db->getAll("SELECT IFNULL(coupon_name, '冇獎') as coupon_name, COUNT(*) as total FROM " . $ecs->table("treasure") . " t LEFT JOIN " . $ecs->table("coupons") . " c ON c.coupon_id = t.coupon_id WHERE open_time != 0" . $where . " GROUP BY t.coupon_id") as $row) {
        echo "=> $row[coupon_name]: $row[total]<br>";
    }
    echo "<br>";

    echo "Treasure opened: " . $db->getOne("SELECT COUNT(*) FROM " . $ecs->table("treasure") . " t WHERE open_time != 0 AND confirm != 0" . $where) . "<br>";
    foreach ($db->getAll("SELECT IFNULL(coupon_name, '冇獎') as coupon_name, COUNT(*) as total FROM " . $ecs->table("treasure") . " t LEFT JOIN " . $ecs->table("coupons") . " c ON c.coupon_id = t.coupon_id WHERE open_time != 0 AND confirm != 0" . $where . " GROUP BY t.coupon_id") as $row) {
        echo "=> $row[coupon_name]: $row[total]<br>";
    }
    echo "<br>";

    echo "Unique found IP: " . $db->getOne("SELECT COUNT(DISTINCT ip) FROM " . $ecs->table("treasure") . " t WHERE open_time != 0" . $where) . "<br>";
    foreach ($db->getAll("SELECT IFNULL(coupon_name, '冇獎') as coupon_name, COUNT(DISTINCT ip) as total FROM " . $ecs->table("treasure") . " t LEFT JOIN " . $ecs->table("coupons") . " c ON c.coupon_id = t.coupon_id WHERE open_time != 0" . $where . " GROUP BY t.coupon_id") as $row) {
        echo "=> $row[coupon_name]: $row[total]<br>";
    }
    echo "<br>";

    echo "Unique opened IP: " . $db->getOne("SELECT COUNT(DISTINCT ip) FROM " . $ecs->table("treasure") . " t WHERE open_time != 0 AND confirm != 0" . $where) . "<br>";
    foreach ($db->getAll("SELECT IFNULL(coupon_name, '冇獎') as coupon_name, COUNT(DISTINCT ip) as total FROM " . $ecs->table("treasure") . " t LEFT JOIN " . $ecs->table("coupons") . " c ON c.coupon_id = t.coupon_id WHERE open_time != 0 AND confirm != 0" . $where . " GROUP BY t.coupon_id") as $row) {
        echo "=> $row[coupon_name]: $row[total]<br>";
    }
}
?>