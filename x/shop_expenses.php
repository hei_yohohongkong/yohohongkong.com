<?php

/***
* shop_expenses.php
* by howang 2014-11-07
*
* Accounting System
***/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_accounting.php');

if ($_REQUEST['act'] == 'list')
{
    $list = get_shop_expenses();
    $smarty->assign('ur_here',      '門店支出');
    $smarty->assign('full_page',    1);

    $smarty->assign('list',         $list['data']);
    $smarty->assign('filter',       $list['filter']);
    $smarty->assign('record_count', $list['record_count']);
    $smarty->assign('page_count',   $list['page_count']);
    
    $smarty->assign('action_link', array('text' => '新增門店支出', 'href' => 'shop_expenses.php?act=add'));
    $smarty->assign('action_link2', array('text' => $_LANG['actg_transactions'], 'href' => 'accounting_report.php?act=list'));

    $sort_flag  = sort_flag($list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);

    assign_query_info();
    $smarty->display('shop_expenses_list.htm');
}
elseif ($_REQUEST['act'] == 'query')
{
    $list = get_shop_expenses();
    $smarty->assign('list',         $list['data']);
    $smarty->assign('filter',       $list['filter']);
    $smarty->assign('record_count', $list['record_count']);
    $smarty->assign('page_count',   $list['page_count']);

    $sort_flag  = sort_flag($list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);

    make_json_result($smarty->fetch('shop_expenses_list.htm'), '', array('filter' => $list['filter'], 'page_count' => $list['page_count']));
}
elseif ($_REQUEST['act'] == 'add')
{
    $smarty->assign('ur_here',     '新增門店支出');
    
    $smarty->assign('action_link', array('text' => '門店支出列表', 'href' => 'shop_expenses.php?act=list'));
    $smarty->assign('action_link2', array('text' => $_LANG['actg_transactions'], 'href' => 'accounting_report.php?act=list'));
    
    $salespeople = $db->getCol("SELECT user_name as sales_name FROM ".$ecs->table('admin_user')." WHERE user_id=".$_SESSION['admin_id']);
    $smarty->assign('salesperson_list', $salespeople);
    $smarty->assign('admin_id', $_SESSION['admin_id']);
    $smarty->assign('admin_name', $_SESSION['admin_name']);
    
    $smarty->assign('deposit_accounts', $actg->getAccounts(array('can_deposit_cash' => 1)));
    
    $smarty->assign('expense_types', $actg->getShopExpenseTypes());
    
    assign_query_info();
    $smarty->display('shop_expense_info.htm');
}
elseif ($_REQUEST['act'] == 'insert')
{
    $operator = empty($_POST['operator']) ? '' : trim($_POST['operator']);
    if (empty($operator))
    {
        sys_msg('必須輸入操作員');
    }
    
    $expense_type_id = empty($_POST['expense_type_id']) ? 0 : intval($_POST['expense_type_id']);
    if (empty($expense_type_id))
    {
        sys_msg('必須選擇類型');
    }
    
    $deposit_account = empty($_POST['deposit_account']) ? 0 : intval($_POST['deposit_account']);
    if (($expense_type_id == 2) && (empty($deposit_account)))
    {
        sys_msg('必須選擇存款帳戶');
        $deposit_acc = $actg->getAccount(array('account_id' => $deposit_account));
        if ((empty($deposit_acc)) || (!$deposit_acc['can_deposit_cash']))
        {
            sys_msg('存款帳戶不正確');
        }
    }
    
    $expense_desc = empty($_POST['expense_desc']) ? '' : trim($_POST['expense_desc']);
    
    $amount = empty($_POST['amount']) ? 0.0 : doubleval($_POST['amount']);
    if (empty($amount))
    {
        sys_msg('必須輸入金額');
    }
    
    $remark = empty($_POST['remark']) ? '' : trim($_POST['remark']);
    
    $datetime = $actg->dbdate();
    
    $expense_year = empty($_POST['expense_year']) ? null : intval($_POST['expense_year']);

    $expense_month = empty($_POST['expense_month']) ? null : intval($_POST['expense_month']);

    $actg->start_transaction();
    
    $exp = array(
        'expense_date' => $datetime,
        'expense_type_id' => $expense_type_id,
        'expense_desc' => $expense_desc,
        'currency_code' => 'HKD',
        'amount' => $amount,
        'operator' => $operator,
        'remark' => $remark,
        'expense_year' => $expense_year,
        'expense_month' => $expense_month,
    );
    $expense_id = $actg->addShopExpense($exp);
    
    if ($expense_type_id == 2) // Deposit to bank
    {
        $expense = $actg->getShopExpense(array('expense_id' => $expense_id));
        
        $deposit_txn = array(
            'add_date' => $expense['expense_date'],
            'complete_date' => $expense['expense_date'],
            'currency_code' => $expense['currency_code'],
            'amount' => $expense['amount'],
            'payment_type_id' => 1, // 現金
            'account_id' => $deposit_account,
            'related_txn_id' => $expense['txn_id'],
            'remark' => '門店現金存款' . (empty($expense['remark']) ? '' : ' ' . $expense['remark']),
            'txn_type_id' => 44,
            'expense_year' => $expense_year,
            'expense_month' => $expense_month,
        );
        $actg->addTransaction($deposit_txn);
    }
    
    $actg->commit_transaction();

    $link[0]['text'] = '返回列表';
    $link[0]['href'] = 'shop_expenses.php?act=list';
    
    sys_msg('新增成功', 0, $link, true);
}

function get_shop_expenses()
{
    global $actg, $db;
    
    $filter['sort_by']      = empty($_REQUEST['sort_by']) ? 'expense_id' : trim($_REQUEST['sort_by']);
    $filter['sort_order']   = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);

    $sql = "SELECT count(*) FROM `actg_shop_expenses`";
    $filter['record_count'] = $db->getOne($sql);

    /* 分页大小 */
    $filter = page_and_size($filter);

    /* 查询 */
    $data = $actg->getShopExpenses(array(), array(
        'orderby' => $filter['sort_by'] . ' ' . $filter['sort_order'],
        'limit' => $filter['start'] . ',' . $filter['page_size']
    ));
    
    foreach ($data as $key => $row)
    {
        $data[$key]['expense_date_formatted'] = local_date($GLOBALS['_CFG']['time_format'], strtotime($row['expense_date']));
        $data[$key]['amount_formatted'] = $actg->money_format($row['amount'], $row['currency_format']);
    }
    
    $arr = array('data' => $data, 'filter' => $filter, 'page_count' => $filter['page_count'], 'record_count' => $filter['record_count']);
    
    return $arr;
}

?>