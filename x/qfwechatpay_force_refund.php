<?php

/***
* qfwechatpay_force_refund.php
* by Billy 20190527
*
* Make wechat pay refend request directly
***/

define('IN_ECS', true);
require(dirname(__FILE__) . '/includes/init.php');
require_once(ROOT_PATH . 'includes/lib_payment.php');

$refundController = new Yoho\cms\Controller\RefundController();
$adminUserController = new Yoho\cms\Controller\AdminuserController();
$weChatPayController = new Yoho\cms\Controller\WeChatPayLogController();
$payment_info = get_payment('qfwechatpay');

if($_SESSION['is_accountant'] != 1 && $_SESSION['role_id'] != 4 && $_SESSION['manage_cost'] != 1)
{
    sys_msg($GLOBALS['_LANG']['priv_error']);
};

if ($_GET['act'] == 'add' || empty($_REQUEST['act'])) 
{
    if (empty($_REQUEST['order_id'])) {
        header('Location: order.php?act=list');
        exit;
    }
    $transaction_array = $weChatPayController->get_all_payment_log($_REQUEST['order_id']);
    $refund_request_records = $refundController->getAllRefundRecords($_REQUEST['order_id']);
    $transaction_selection_table = [];
    $error_msg = '';
    $total_refund_request_amount = 0;
    $total_processed_refund_request_amount = 0;

    $payment_array = array_filter($transaction_array, function($item) {
        return ($item['order_type'] == 'payment');
    });

    $refund_array = array_filter($transaction_array, function($item) {
        return ($item['order_type'] == 'refund');
    });

    // Build pairing table
    foreach($payment_array as $payment_item) {
        $transaction_item = [
            'transaction_sn' => $payment_item['gateway_transaction_sn'],
            'out_trade_no' => $payment_item['out_trade_no'],
            'gateway_transaction_amount' => $payment_item['gateway_transaction_amount'],
            'outstanding_amount' => $payment_item['gateway_transaction_amount'],
            'status' => $payment_item['cancel'],
            'refund_ids' => '',
        ];

        $refund_search_result = array_filter($refund_array, function($item) use ($transaction_item) {
            return(strpos($item['out_trade_no'], $transaction_item['out_trade_no']) !== false);
        });

        foreach($refund_search_result as $refund_item) {
            $total_processed_refund_request_amount += $refund_item['gateway_transaction_amount'];

            // Base case for normal transaction state
            if (!empty($transaction_item['refund_ids'])) {
                $transaction_item['refund_ids'] .= ', ';
            }
            $transaction_item['refund_ids'] .= $refund_item['out_trade_no'];
            $transaction_item['outstanding_amount'] = bcsub($transaction_item['outstanding_amount'], $refund_item['gateway_transaction_amount'], 2);
        }
        $transaction_item['action'] = 'HKD <input name="amount[' . $payment_item['out_trade_no'] . ']" class="form-control" type="number" step="0.1" value="0" min="0" max="' . $transaction_item['outstanding_amount'] . '"/>';
        $transaction_item['outstanding_amount'] = 'HKD ' . $transaction_item['outstanding_amount'];
        $transaction_item['gateway_transaction_amount'] = 'HKD ' . $transaction_item['gateway_transaction_amount'];
        $transaction_selection_table[] = $transaction_item;
    }

    // Build Refund Record Table
    foreach($refund_request_records as $refund_request_record) {
        if ($refund_request_record['status'] != '4') {
            $total_refund_request_amount += $refund_request_record['refund_amount'];
        }
    }

    foreach($refund_array as $key => $value) {
        $refund_array[$key]['gateway_request_datetime'] = local_date('Y-m-d H:i:s', $value['gateway_request_datetime']);
    }

    if (isset($_REQUEST['error']) && $_REQUEST['error'] != "0000") {
        $errors = $_REQUEST['error'];
        foreach ($errors as $transaction_id => $error_code) {
            $content = '';
            if ($error_code == "1269") {
                $content = '帳戶未結算餘額不足, 無法操作';
            }
            elseif ($error_code == "1264") {
                $content = '要求退款額超出該交易未退款餘額';
            } 
            elseif ($error_code == "1251") {
                $content = '退款支付編號重複';
            } 
            else {
                $content = sprintf('未知錯誤(%s)', $error_code);
            }
            $error_msg .= sprintf("交易%s無法進行: %s <br/>", $transaction_id, $content);
        }
    }

    $smarty->assign('ur_here', $_LANG['wechatpay_force_refund']);
    $smarty->assign('order_id', $_REQUEST['order_id']);
    $smarty->assign('transaction_summary', $transaction_selection_table);
    $smarty->assign('total_refund_request_amount', $total_refund_request_amount);
    $smarty->assign('total_processed_refund_request_amount', $total_processed_refund_request_amount);
    $smarty->assign('wechat_pay_refund_records', $refund_array);
    $smarty->assign('error_msg', $error_msg);

    assign_query_info();
    $smarty->display('qfwechatpay_force_refund.htm');
}
elseif ($_GET['act'] == 'sync')
{
    if (empty($_REQUEST['order_id'])) {
        header('Location: order.php?act=list');
        exit;
    }
    $transaction_array = $weChatPayController->get_all_payment_log($_REQUEST['order_id']);
    $transaction_selection_table = [];

    $payment_array = array_filter($transaction_array, function($item) {
        return ($item['order_type'] == 'payment');
    });

    foreach($payment_array as $payment_item) {
        $parameter = [
            'gateway_transaction_sn' => $payment_item['gateway_transaction_sn'],
        ];

        $payment_record_query_response = $weChatPayController->get_qfwechaypay_transaction_record($parameter);
        $payment_record = json_decode($payment_record_query_response);
        if (sizeof($payment_record->data) != 1) {
            header('Location: qfwechatpay_force_refund.php?order_id=' . $_REQUEST['order_id']);
        }
        $payment_record = $payment_record->data[0];

        if ($payment_record->cancel != $payment_item['cancel']) {
            // Update the base payment data first
            $weChatPayController->insert_wechat_pay_log($payment_record, $payment_item['log_id']);
        }

        // Then query then add/update refund transaction
        for ($i = 0; $i <= 9; $i++) {
            $refund_indicator = $i == 0 ? '_r' : '_r'.$i;
            $parameter = [
                'order_payment_id' => $payment_record->out_trade_no . $refund_indicator
            ];

            $payment_record_refund_query_response = $weChatPayController->get_qfwechaypay_transaction_record($parameter);

            $refund_record = json_decode($payment_record_refund_query_response);
            if (sizeof($refund_record->data) != 1) {
                break;
            }
            // Then insert/ update the refund payment data
            $refund_record = $refund_record->data[0];
            $weChatPayController->insert_wechat_pay_log($refund_record, $payment_item['log_id']);
        }
    }

    // Passing error param after refund instead of parsing here
    $error = [];
    $error['error'] = "0000";
    if (isset($_REQUEST['error']) && gettype($_REQUEST['error']) == "array") {
        $error['error'] = $_REQUEST['error'];
    }

    header('Location: qfwechatpay_force_refund.php?order_id=' . $_REQUEST['order_id'] . '&' . http_build_query($error));
    exit;
}
elseif ($_POST['act'] == 'init_refund')
{
    $error['error'] = '0000'; // No Error
    if (empty($_REQUEST['order_id'])) {
        header('Location: order.php?act=list');
        exit;
    }

    if (empty($_POST['amount']) || sizeof($_POST['amount']) == 0) {
        header('Location: qfwechatpay_force_refund.php?order_id=' . $_REQUEST['order_id']);
        exit;
    }

    foreach($_POST['amount'] as $key => $amount)
    {
        if (empty($key) || empty($amount) || $amount == 0) {
            continue;
        }

        $pay_log_id = substr(explode('_', $key)[0], 13);

        if (!isset($pay_log_id) || empty($pay_log_id)) {
            continue;
        }
        
        $refund_result = $weChatPayController->refund_transaction($pay_log_id, $amount);
        $refund_result = json_decode($refund_result);
        if ($refund_result->error != '0') {
            $error_code['error'] = [];
            $refund_result = $refund_result->message;
            if (isset($refund_result->respcd)) {
                $error_code['error'][$refund_result->orig_syssn] = $refund_result->respcd;
            } else {
                $error_code['error'][$refund_result->orig_syssn] = "9999";
            }
        }
    }

    $result = $refundController->markAllProcessingRefundRequestToProcessed($_REQUEST['order_id']);

    header('Location: qfwechatpay_force_refund.php?act=sync&order_id=' . $_REQUEST['order_id'] . '&' . http_build_query($error_code));
    exit;
}
elseif ($_POST['act'] == 'update_refund_request')
{
    if (empty($_REQUEST['order_id'])) {
        header('Location: order.php?act=list');
        exit;
    }

    $result = $refundController->markAllProcessingRefundRequestToProcessed($_REQUEST['order_id']);
    
    header('Location: qfwechatpay_force_refund.php?act=sync&order_id=' . $_REQUEST['order_id']);
    exit;
}

?>
