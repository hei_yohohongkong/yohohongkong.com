<?php

/***
* deal_model.php
* by howang 2014-10-24
*
* Manage deal models
***/

define('IN_ECS', true);
require(dirname(__FILE__) . '/includes/init.php');


if ($_REQUEST['act'] == 'list')
{
    $list = get_deal_model();
    $smarty->assign('ur_here',      '保養產品型號');
    $smarty->assign('full_page',    1);

    $smarty->assign('list',         $list['data']);
    $smarty->assign('filter',       $list['filter']);
    $smarty->assign('record_count', $list['record_count']);
    $smarty->assign('page_count',   $list['page_count']);
    
    $smarty->assign('action_link', array('text' => '新增型號', 'href' => 'deal_model.php?act=add'));

    $sort_flag  = sort_flag($list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);

    assign_query_info();
    $smarty->display('deal_model_list.htm');
}
elseif ($_REQUEST['act'] == 'query')
{
    $list = get_deal_model();
    $smarty->assign('list',         $list['data']);
    $smarty->assign('filter',       $list['filter']);
    $smarty->assign('record_count', $list['record_count']);
    $smarty->assign('page_count',   $list['page_count']);

    $sort_flag  = sort_flag($list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);

    make_json_result($smarty->fetch('deal_model_list.htm'), '', array('filter' => $list['filter'], 'page_count' => $list['page_count']));
}
elseif ($_REQUEST['act'] == 'add')
{
    if (!$_SESSION['manage_cost'])
    {
        sys_msg('Admin only!');
    }
    
    $smarty->assign('ur_here',      '保養產品型號');
    
    $smarty->assign('action_link', array('text' => '返回列表', 'href' => 'deal_model.php?act=list'));
    
    assign_query_info();
    $smarty->display('deal_model_add.htm');
}
elseif ($_REQUEST['act'] == 'insert')
{
    if (!$_SESSION['manage_cost'])
    {
        sys_msg('Admin only!');
    }
    
    $model_name = empty($_REQUEST['model_name']) ? '' : $_REQUEST['model_name'];
    
    $model = array(
        'model_name' => $model_name
    );
    
    $db->autoExecute($ecs->table('warranty_models'), $model, 'INSERT');
    $model_id = $db->insert_id();

    $link[0]['text'] = '返回列表';
    $link[0]['href'] = 'deal_model.php?act=list';
    
    sys_msg('新增成功', 0, $link, true);
}
elseif ($_REQUEST['act'] == 'edit_name')
{
    if (!$_SESSION['manage_cost'])
    {
        make_json_error('Admin only!');
    }
    
    $model_id = intval($_POST['id']);
    $value = trim($_POST['val']);
    
    $db->query("UPDATE " . $ecs->table('warranty_models') . " SET `model_name` = '" . $value . "' WHERE `model_id` = '" . $model_id . "'");
    make_json_result($value);
}
elseif ($_REQUEST['act'] == 'remove')
{
    if (!$_SESSION['manage_cost'])
    {
        sys_msg('Admin only!');
    }
    
    $model_id = $_REQUEST['model_id'];

    $db->query("DELETE FROM " . $ecs->table('warranty_models') . " WHERE `model_id` = '" . $model_id . "' LIMIT 1");

    $link[0]['text'] = '返回列表';
    $link[0]['href'] = 'deal_model.php?act=list';
    
    sys_msg('刪除成功', 0, $link);
}

function get_deal_model()
{
    $filter['sort_by']      = empty($_REQUEST['sort_by']) ? 'model_id' : trim($_REQUEST['sort_by']);
    $filter['sort_order']   = empty($_REQUEST['sort_order']) ? 'ASC' : trim($_REQUEST['sort_order']);

    $sql = "SELECT count(*) FROM " . $GLOBALS['ecs']->table('warranty_models');
    $filter['record_count'] = $GLOBALS['db']->getOne($sql);

    /* 分页大小 */
    $filter = page_and_size($filter);

    /* 查询 */
    $sql = "SELECT * ";
    $sql.= " FROM " . $GLOBALS['ecs']->table('warranty_models');
    $sql.= " ORDER by " . $filter['sort_by'] . ' ' . $filter['sort_order'];
    $sql.= " LIMIT " . $filter['start'] . "," . $filter['page_size'];
    $data = $GLOBALS['db']->getAll($sql);
    
    $arr = array('data' => $data, 'filter' => $filter, 'page_count' => $filter['page_count'], 'record_count' => $filter['record_count']);

    return $arr;
}

?>