<?php
    define('IN_ECS', true);
    require(dirname(__FILE__) . '/includes/init.php');
    if($_REQUEST['act'] == 'list'){
        if(isset($_REQUEST['year']) && $_REQUEST['year'] != "所有"){
            $where = " WHERE year = " . $_REQUEST['year'];
            $smarty->assign('year', $_REQUEST['year']);
        }
        $sql = "SELECT DISTINCT year FROM " . $ecs->table('holidays') . " ORDER BY year ASC";
        $years = $db->getCol($sql);
        $sql = "SELECT * FROM " . $ecs->table('holidays') . $where . " ORDER BY year ASC, h_id ASC";
        $res = $db->getAll($sql);
        foreach($res as $row){
            $holidays[$row['year']][] = array('date'=>$row['date'],'info'=>$row['info']);
        }
        assign_query_info();
        $smarty->assign('years', $years);
        $smarty->assign('holidays', $holidays);
    }else if($_REQUEST['act'] == 'update'){
        admin_priv('holidays_update');
        $baseController = new Yoho\cms\Controller\YohoBaseController();
        $year = intval($_REQUEST['year']);
        $year = ($year > 0) ? $year : local_date('Y');
        $baseController->get_yoho_holidays($year,true);
        header('Location: holidays.php?act=list');
    }
    $smarty->assign('full_page', 1);
    $smarty->assign('ur_here', $_LANG['holidays']);
    $smarty->assign('can_update', intval(check_authz('holidays_update')));
    $smarty->assign('act', $_REQUEST['act']);
    $smarty->display('holidays.htm');
?>