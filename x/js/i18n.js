/***
* i18n.js
*
* Javascript for Internationalization
***/

/* jshint -W041 */

function run_after_jquery(callback) {
	if (typeof jQuery !== 'undefined') {
		callback(jQuery);
	} else {
		var have$ = (typeof $ === 'function');
		var script = document.createElement('script');
		script.src = '/yohohk/js/jquery.min.js';
		var head = document.getElementsByTagName('head')[0];
		var done = false;
		// Attach handlers for all browsers
		script.onload = script.onreadystatechange = function() {
			if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {
				done = true;
				if (typeof jQuery !== 'undefined') {
					if (have$) {
						jQuery.noConflict();
					}
					callback(jQuery);
				}
				script.onload = script.onreadystatechange = null;
				head.removeChild(script);
			}
		};
		head.appendChild(script);
	}
}

var YOHO = YOHO || {};
YOHO.admin = YOHO.admin || {};

run_after_jquery(function ($) {

YOHO.admin.i18n = {
	langs: {'zh_tw': '繁體中文', 'zh_cn': '简体中文', 'en_us': 'English'},
	default_lang: 'zh_tw',
	current_lang: 'zh_tw',
	exclude_update: ['perma_link'],
	
	init: function ()
	{
		if (window.listTable)
		{
			this.initListTable();
		}
		else if($.fn.dataTable && $.fn.dataTable.tables().length > 0) {
			// Add language selector
			var $langSel = this.createLanguageSelector();

			$('.edit-language-select').change(function(){
				var $lang = $(this).val();
				$.each($.fn.dataTable.tables(), function(key, table){
					$(table).data('language', {'edit_lang': $lang});
					$(table).DataTable().draw();
				});

			});
		} else {
			this.initEditPage();
		}
	},
	
	initListTable: function ()
	{
		// Tell listTable to handle language
		window.listTable.i18n = true;
		
		// Add language selector to use with listTable
		var $langSel = this.createLanguageSelector();
		$langSel.addClass('listtable-language-select');
		
		// Update listTable language
		var updateListTableLang = function (lang)
		{
			YOHO.admin.i18n.current_lang = lang;
			if ($langSel.val() != lang)
			{
				$langSel.val(lang);
			}
			if (window.localStorage.getItem('listtable_edit_lang') != lang)
			{
				window.localStorage.setItem('listtable_edit_lang', lang);
			}
			window.listTable.edit_lang = (lang == YOHO.admin.i18n.default_lang) ? '' : lang;
			window.listTable.loadList();
		};
		
		var edit_lang = window.localStorage.getItem('listtable_edit_lang');
		if (edit_lang)
		{
			if ($.inArray(edit_lang, this.langs) != -1)
			{
				if (edit_lang == this.default_lang)
				{
					YOHO.admin.i18n.current_lang = edit_lang;
					$langSel.val(edit_lang);
					window.listTable.edit_lang = '';
				}
				else
				{
					updateListTableLang(edit_lang);
				}
				
			}
			else
			{
				window.localStorage.removeItem('listtable_edit_lang');
			}
		}
		$langSel.change(function () {
			updateListTableLang($langSel.val());
		});
		
		$(window).bind('storage.listtable_edit_lang', function (e) {
			if (e.originalEvent.key == 'listtable_edit_lang')
			{
				var new_lang = e.originalEvent.newValue;
				if (($.inArray(new_lang, YOHO.admin.i18n.langs) != -1) && new_lang != YOHO.admin.i18n.current_lang)
				{
					updateListTableLang(new_lang);
				}
			}
		});
	},
	
	initEditPage: function () {
		if (window.tinymce){
			tinymce.init({
				selector: '.tinymce',
				style_formats_merge: false,
				style_formats: [
					{
						title: "Title(H3)", icon: "H3", format: "h3", styles: {}
					},
					{title: 'small', inline: 'small', styles: {}},
					{title: "向左對齊", icon: "alignleft", format: "alignleft"},
					{title: "置中對齊", icon: "aligncenter", format: "aligncenter"},
					{title: "向右對齊", icon: "alignright", format: "alignright"}
				],
				element_format : "html",
				// fontsize_formats: "12px 14px 16px 18px 24px 36px",
				forced_root_block : "",
				force_br_newlines : true,
				force_p_newlines : false,
				convert_urls: false,
				// relative_urls: false, // use absolute urls
				// remove_script_host: true, // keep absolute urls even if host match
				paste_as_text: true, // Prevent user pasting poor formated HTML
				plugins: [
				        "media autolink lists link image searchreplace code media table contextmenu paste autoresize imagetools preview jbimages textcolor colorpicker textpattern imgmap"
				],
				link_class_list: [
					{title: '按此購買', value: 'click-to-buy'},
					{title: 'None', value: ''},
				],
				setup: function(editor) {
					editor.addButton('instantuploadimage', {
						icon: true,
						image: '/yohohk/img/ico-instantuploadimage.png',
						tooltip: 'Upload image',
						onclick: function() {
							var $input = $('<input type="file" accept="image/png, image/jpeg, image/gif" style="display: none;">');
							$input.appendTo('body');
							$input.trigger('click');
							$input.change(function () {
								var imageFile = this.files[0];
								var formdata = new FormData();
								formdata.append('type', 'images');
								formdata.append('file', imageFile, imageFile.name);
								$.ajax({
									url: '/includes/tinymce/tinymce.php?act=tinymce_upload',
									type: 'post',
									data: formdata,
									processData: false,
									contentType: false,
									xhrFields: { withCredentials: true },
									dataType: 'json',
									complete: function () {
										$input.remove();
									},
									success: function (res) {
										var imageUrl = res.location;
										if (imageUrl)
										{
											editor.execCommand('mceInsertContent', false, 
												editor.dom.createHTML('img', {
													'id': '__mcenew',
													'src': imageUrl
												})
											);
										}
										else
										{
											alert('上傳失敗，請稍後再試。');
											console.log(res);
										}
									},
									error: function (xhr) {
										alert("上傳失敗，請稍後再試。\n" + xhr.statusText);
										console.log(xhr);
									}
								});
							});
						}
					});
				},

				menubar: false,
				mediaembed_service_url: 'SERVICE_URL',
				mediaembed_max_width: 450,
				toolbar: "styleselect | instantuploadimage image link media | bullist numlist | bold italic  strikethrough forecolor backcolor | removeformat | preview code | imgmap",
				extended_valid_elements : "img[usemap|class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name],map[id|name],area[shape|alt|coords|href|target]",
				valid_children : "+body[style]",
				image_upload_url: '/images/upload/Image/',
				plugin_preview_width: 790 + 33, // From observation, need to add 33 pixels
												// to cater margins and scroll bar
				plugin_preview_height: 500,
				width : "640"
			});
			$(document.forms).bindFirst('submit', function () {
				window.tinymce.EditorManager.triggerSave();
			});
		}
		if ($("#gjs").length > 0){
			$id = $("#gjs").data('id');
			var grapesEditor = grapesjs.init({
				showOffsets: 1,
				noticeOnUnload: 0,
				avoidInlineStyle: true,
				storageManager: {
					id: 'gjs-',             // Prefix identifier that will be used inside storing and loading
					type: 'local',          // Type of the storage
					autosave: false,         // Store data automatically
				},
				container: '#gjs',
				fromElement: true,
				assetManager: {
					upload: 'article.php?act=image_upload',
   					uploadName: 'file',
				},
				plugins: ['gjs-preset-webpage'],
				pluginsOpts: {
					'gjs-preset-webpage': {},
					'aviaryOpts': false
				},
				canvas: {
					styles: ['/yohohk/css/template.css']
				}
			});
			
			var blockManager = grapesEditor.BlockManager;

			//blockManager.getAll().reset();

			/*blockManager.add('marketing-template-1', {
				label: 'Product on Left',
				content: '<style>.article_goods {width: 100%; display: grid; grid-template-columns: 2fr 3fr; align-items: center; text-align: center;} .article_goods .article_goods_img img {width: 100%; margin: auto;}</style><div class="article_goods"><div class="article_goods_img"><img alt="goods_img"><div class="article_goods_name">Goods name comes here</div></div><div class="article_goods_desc"><div class="article_goods_detail">Description comes here</div></div></div>',
				"category":{  
					"id":"Template",
					"label":"Template",
					"open":true,
					"attributes":{}
				}
			})*/
			blockManager.add('marketing-template-2', {
				label: 'Comparison Table Header',
				content: '<div class="comparison comparison-header"><table class="comparison-table"><tr class="theader"><td class="col1 topic">[HEADER]</td><td class="col2 topic">[HEADER]</td><td class="col3 topic">[HEADER]</td><td class="col4 topic">[HEADER]</td><td class="col5 topic">[HEADER]</td><td class="col6 topic">[HEADER]</td></tr></table></div>',
				category: {  
					"id":"Template",
					"label":"Template",
					"open":true
				}
			});
			
			blockManager.add('marketing-template-3', {
				label: 'Comparison Table Column',
				content: '<div class="comparison comparison-detail"><table class="comparison-table"><tr class="tbody goods"><td class="col1"><div class="g_img"> </div><div class="g_name">CONTENT</div></td><td class="col2"><div class="t_img"> </div><div class="t_desc">CONTENT</div></td><td class="col4"><div class="p_desc"><ul><li>BULLET</li><li>BULLET</li><li>BULLET</li><li>BULLET</li><li>BULLET</li><li>BULLET</li></ul></div></td><td class="col5"><div class="p_desc"><ul><li>BULLET</li><li>BULLET</li><li>BULLET</li></ul></div></td></tr></table></div>',
				category: {  
					"id":"Template",
					"label":"Template",
					"open":true
				}
			});

			var remove_block = ["column1", "column2", "column3", "column3-7", "map", "h-navbar", "countdown", "form", "input", "textarea", "select", "button", "label", "checkbox", "radio", "link-block", "quote", "text-basic"];
			
			remove_block.forEach(function(block_id) {
				blockManager.remove(block_id);
			});

			//const blocks = blockManager.getAll();
			//console.log(JSON.stringify(blocks));


			deviceManager = grapesEditor.DeviceManager;
			grapesEditor.setComponents($('input[name="'+$id+'"]').val());
			grapesEditor.id = $id;
			grapesEditor.inlineHtml = function() {
				var html = grapesEditor.getHtml();
				var css  = grapesEditor.getCss();
				var js   = grapesEditor.getJs();
				var inlineHtml = '';
				var $script = "<script>"+js+"</script>";
				var $style  = "<style>"+css+"</style>";
				inlineHtml = $style+html+$script;
				return  inlineHtml;
			}
			var pfx = grapesEditor.getConfig().stylePrefix;
			var modal = grapesEditor.Modal;
			var cmdm = grapesEditor.Commands;
			var codeViewer = grapesEditor.CodeManager.getViewer('CodeMirror').clone();
			var cssCodeViewer = grapesEditor.CodeManager.getViewer('CodeMirror').clone();
			var pnm = grapesEditor.Panels;
			var container = document.createElement('div');
			var htmldiv = document.createElement('div');htmldiv.style='float:left;width:50%';
			var cssdiv = document.createElement('div');cssdiv.style='float:left;width:50%';
			var btnEdit = document.createElement('a');

			codeViewer.set({
				codeName: 'htmlmixed',
				readOnly: 0,
				theme: 'hopscotch',
				autoBeautify: true,
				autoCloseTags: true,
				autoCloseBrackets: true,
				lineWrapping: true,
				styleActiveLine: true,
				smartIndent: true,
				indentWithTabs: true,
				label: 'HTML'
			});
			cssCodeViewer.set({
				codeName: 'css',
				readOnly: 0,
				theme: 'hopscotch',
				autoBeautify: true,
				autoCloseTags: true,
				autoCloseBrackets: true,
				lineWrapping: true,
				styleActiveLine: true,
				smartIndent: true,
				indentWithTabs: true,
				label: 'CSS'
			});

			btnEdit.innerHTML = '修改';
			btnEdit.className = pfx + 'btn-prim ' + pfx + 'btn-import';
			btnEdit.onclick = function() {
				var code = codeViewer.editor.getValue();
				var css = cssCodeViewer.editor.getValue();
				grapesEditor.DomComponents.getWrapper().set('content', '');
				grapesEditor.setComponents(code.trim());
				grapesEditor.setStyle(css);
				modal.close();
			};

			cmdm.add('html-edit', {
				run: function(grapesEditor, sender) {
					sender && sender.set('active', 0);
					var viewer = codeViewer.editor;
					modal.setTitle('Edit code');
					var InnerHtml = grapesEditor.getHtml();
					var Css = grapesEditor.getCss();
					if (!viewer) {
						var txtarea = document.createElement('textarea');
						var csstxtarea = document.createElement('textarea');
						$(htmldiv).append(txtarea);
						container.appendChild(htmldiv);
						$(cssdiv).append(csstxtarea);
						container.appendChild(cssdiv);
						container.appendChild(btnEdit);
						codeViewer.init(txtarea);
						cssCodeViewer.init(csstxtarea);
						viewer = codeViewer.editor;
						cssViewer = cssCodeViewer.editor;
					}

					modal.setContent('');
					modal.setContent(container);
					codeViewer.setContent(InnerHtml);
					cssCodeViewer.setContent(Css);
					modal.open();
					viewer.refresh();
					cssViewer.refresh();
				}
			});

			pnm.addButton('options',
				[
					{
						id: 'edit',
						className: 'fa fa-edit',
						command: 'html-edit',
						attributes: {
							title: 'Edit'
						}
					}
				]
			);
			var codeButton = grapesEditor.Panels.getButton("options", "export-template");
			codeButton.collection.remove(codeButton);
		}
		if ((typeof window.localizableFields === 'undefined') ||
			(typeof window.localizedVersions === 'undefined'))
		{
			// Not localizable
			return;
		}
		// Create localizedVersions array if needed
		if (window.localizedVersions === null || $.isArray(window.localizedVersions))
		{
			window.localizedVersions = {};
		}

		// Create a two way map for fields mapping
		var fieldsMapping = new TwoWayMap((typeof window.localizableFieldsMapping === 'object') ? window.localizableFieldsMapping : {});
		
		// Save current language
		var currentLang = YOHO.admin.i18n.default_lang;
		YOHO.admin.i18n.current_lang = currentLang;
		
		// Save default version in a variable
		var defaultVersion = {};
		$.each(window.localizableFields, function (i, field) {
			var htmlField = fieldsMapping.get(field) || field;
			defaultVersion[field] = $('input[type="text"][name="' + htmlField + '"],' + 
									  'input[type="hidden"][name="' + htmlField + '"],' +
									  'textarea[name="' + htmlField + '"]').val();
		});
		
		// Decorate localizable fields
		$.each(window.localizableFields, function (i, field) {
			var htmlField = fieldsMapping.get(field) || field;
			// Handle ordinary textbox and textarea
			var $field = $('input[type="text"][name="' + htmlField + '"],textarea[name="' + htmlField + '"]');
			if($field.closest('form[name="grobalSearch"]').length > 0) {
				return true;
			}
			var $tinymce = $field.hasClass('tinymce');
			if (!$field.hasClass('tinymce'))
			{
				if($field.hasClass('form-control')) {
					$field.wrap($('<span>').css({'position': 'relative', 'display': 'inline-block', 'width': '100%'}));
				} else {
					$field.wrap($('<span>').css({'position': 'relative', 'display': 'inline-block', 'width': 'auto'}));
				}
				// float fix (for goods info page, goods_name)
				if ($field.css('float') != 'left')
				{
					$field.parent().css('float', $field.css('float'));
					$field.css('float', '');
				} else {
					$field.parent().css('float', 'left');
				}
				// Set a padding so we have room for the localizable indicator
				$field.css('padding-right', '20px');
				// Add the indicator
				var $localizableIndicator = $('<img src="images/localizable.png">').css({
					'top': '2px',
					'right': '6px',
					'position': 'absolute',
					'z-index': '2',
					'display': 'block',
					'width': '16px',
					'height': '16px'
				}).insertBefore($field);
			} else {
				$field.wrap($('<div>').css({'position': 'relative', 'display': 'inline-block', 'float':'left', 'width': '100%'}));
				$('<img src="images/localizable.png">').css({
					'top': '10px',
					'right': '6px',
					'position': 'absolute',
					'z-index': '2',
					'display': 'block',
					'width': '16px',
					'height': '16px',
					'font-size': '16px'
				}).insertAfter($field);
			}

			// Handle FCKeditor iframe
			var $fckFrame = $('iframe#' + htmlField + '___Frame');
			if ($fckFrame)
			{
				// Hack to make FCKeditor work after wrapping
				if (window.FCKeditorAPI)
				{
					// FCKeditor already loaded, unload it
					delete window.FCKeditorAPI;
				}
				
				$(	'input#' + htmlField + ',' +
					'input#' + htmlField + '___Config,' +
					'iframe#' + htmlField + '___Frame').wrapAll('<div style="position: relative"></div>');
				$('<img src="images/localizable.png">').css({
					'top': '6px',
					'right': '6px',
					'position': 'absolute',
					'z-index': '2',
					'display': 'block',
					'width': '16px',
					'height': '16px'
				}).insertAfter($fckFrame);
			}
		});
		
		// Add language selector
		var $langSel = this.createLanguageSelector();
		$langSel.addClass('edit-language-select');
		
		var saveCurrentEditPage = function () {
			$.each(window.localizableFields, function (i, field) {
				if (YOHO.admin.i18n.exclude_update.indexOf(field) != -1) return ;
				var htmlField = fieldsMapping.get(field) || field;
				// Tell FCKEditor to update it's hidden input field so we can grab it
				if (window.FCKeditorAPI)
				{
					var fckEditor = window.FCKeditorAPI.GetInstance(htmlField);
					if (typeof fckEditor !== 'undefined')
					{
						fckEditor.UpdateLinkedField();
					}
				}
				
				if (window.tinymce)
				{
					window.tinymce.EditorManager.triggerSave();
				}
				if(window.grapesjs && typeof grapesEditor !== "undefined" && htmlField == grapesEditor.id) {
					$('input[type="hidden"][name="' + htmlField + '"]' ).val(grapesEditor.inlineHtml());
				}

				var value = $('input[type="text"][name="' + htmlField + '"],' +
							  'input[type="hidden"][name="' + htmlField + '"],' +
							  'textarea[name="' + htmlField + '"]').val();
				value = $.trim(value);
				if (currentLang == YOHO.admin.i18n.default_lang)
				{
					defaultVersion[field] = value;
				}
				else
				{
					if (!window.localizedVersions[currentLang])
					{
						window.localizedVersions[currentLang] = {};
					}
					window.localizedVersions[currentLang][field] = value;
				}
			});
		};
		
		var updateEditPageLang = function (new_lang) {
			// Save current edit before switching
			saveCurrentEditPage();
			// Get new language version
			var thisVersion = window.localizedVersions[new_lang];
			if (typeof thisVersion === 'undefined')
			{
				thisVersion = defaultVersion;
			}
			// Update FCKEditor linked field first
			if (window.FCKeditorAPI)
			{
				$.each(window.FCKeditorAPI.Instances, function (i, fckEditor) {
					fckEditor.UpdateLinkedField();
				});
			}
			// Update to new language
			$.each(window.localizableFields, function (i, field) {
				if (YOHO.admin.i18n.exclude_update.indexOf(field) != -1) return ;
				var newValue = thisVersion[field];
				if (typeof newValue === 'undefined' || newValue === null)
				{
					newValue = '';
				}
				var htmlField = fieldsMapping.get(field) || field;
				var $field = $('input[type="text"][name="' + htmlField + '"],' +
							   'input[type="hidden"][name="' + htmlField + '"],' +
							   'textarea[name="' + htmlField + '"]');
				var oldValue = $field.val();
				$field.val(newValue);
				if(!$field.hasClass('tinymce'))
				{
					$field.highlight(1500);
				}
				if (window.FCKeditorAPI)
				{
					var fckEditor = window.FCKeditorAPI.GetInstance(htmlField);
					if (typeof fckEditor !== 'undefined')
					{
						fckEditor.SetHTML(newValue);
						fckEditor.UpdateLinkedField();
						//if (oldValue != newValue)
						{
							$('iframe#' + htmlField + '___Frame').highlight(1500);
						}
					}
				}
				if (window.tinymce)
				{
					$.each(tinymce.editors, function (i, editor) {
						if ($(editor.targetElm).attr('name') == htmlField)
						{
							editor.load();
							//if (oldValue != newValue)
							{
								$('iframe#tinymce_ifr').highlight(1500);
							}
						}
					});
				}
				if(window.grapesjs && typeof grapesEditor !== "undefined" && field == grapesEditor.id) {
					grapesEditor.setComponents(newValue);
					$('input[name="'+field+'"]').val(newValue);
				}
			});
			
			// Update language selector if needed
			if ($langSel.val() != new_lang)
			{
				$langSel.val(new_lang);
			}
			// Update currentLang variable
			currentLang = new_lang;
			YOHO.admin.i18n.current_lang = currentLang;
		};
		
		$('.edit-language-select').change(function () {
			updateEditPageLang($(this).val());
		});
		
		// Hook form submit to insert localized versions into form
		$(document.forms).bindFirst('submit', function (e) {
			// Switch back to default language (so default language get POSTed to server as usual)
			if (currentLang != YOHO.admin.i18n.default_lang)
			{
				// Due to a bug in FCKEditor, we need to switch them to Source mode first
				// Otherwise we may ended up posting localized value as default language value
				if (window.FCKeditorAPI)
				{
					$.each(window.localizableFields, function (i, field) {
						var htmlField = fieldsMapping.get(field) || field;
						
						var fckEditor = window.FCKeditorAPI.GetInstance(htmlField);
						if (typeof fckEditor !== 'undefined')
						{
							if (fckEditor.EditMode == 0) // 0 = WYSIWYG mode, 1 = Source mode
							{
								fckEditor.SwitchEditMode();
							}
						}
					});
				}
				// Switch to default language
				updateEditPageLang(YOHO.admin.i18n.default_lang);
			}else if(window.grapesjs && grapesEditor) {
				$.each(window.localizableFields, function (i, field) {
					var htmlField = fieldsMapping.get(field) || field;
					if (grapesEditor.id == htmlField)
					{
						console.log(grapesEditor.inlineHtml());
						$('input[type="hidden"][name="' + htmlField + '"]' ).val(grapesEditor.inlineHtml());
					}
				});
			}
			// Append localized versions to form
			var $input = $('<input type="hidden" name="localized_versions">');
			$input.val(JSON.stringify(window.localizedVersions));
			$(this).append($input);
		});
	},
	
	createLanguageSelector: function () {
		var $langSel = $('<select class="form-control edit-language-select"></select>');
		$.each(this.langs, function (i, lang) {
			var $opt = $('<option></option>').text(lang).val(i);
			$langSel.append($opt);
		});
		$('ol.breadcrumb').append($langSel);
		$langSel.wrap('<li class="top_btn lang-selector" style="display: inline-block;"></li>')
		return $langSel;
	},
	
	createFCKEditor: function (input_name, input_value) {
		input_value = (typeof input_value === 'undefined') ? '' : input_value;
		return '<input type="hidden" id="' + input_name + '" name="' + input_name + '" ' +
					'value="' + this.htmlspecialchars(input_value) + '" style="display:none" />' + 
				'<input type="hidden" id="' + input_name + '___Config" value="" style="display:none" />' +
				'<iframe id="' + input_name + '___Frame" ' +
					'src="../includes/fckeditor/editor/fckeditor.html?InstanceName=' + input_name + '&amp;Toolbar=Normal" ' +
					'width="100%" height="320" frameborder="0" scrolling="no"></iframe>';
	},
	
	// ref: http://stackoverflow.com/a/4835406
	htmlspecialchars: function (text) {
		var map = {
			'&': '&amp;',
			'<': '&lt;',
			'>': '&gt;',
			'"': '&quot;',
			"'": '&#039;'
		};
		return text.replace(/[&<>"']/g, function(m) { return map[m]; });
	}
};

// ref: http://stackoverflow.com/a/21070876
function TwoWayMap(map)
{
	this.map = map;
	this.reverseMap = {};
	for(var key in map)
	{
		var value = map[key];
		this.reverseMap[value] = key;   
	}
}
TwoWayMap.prototype.get = function(key){ return this.map[key]; };
TwoWayMap.prototype.revGet = function(key){ return this.reverseMap[key]; };

$.fn.extend({
	// ref: http://stackoverflow.com/a/2641047
	bindFirst: function(name, fn) {
	    // bind as you normally would
	    // don't want to miss out on any jQuery magic
	    this.on(name, fn);
		
	    // Thanks to a comment by @Martin, adding support for
	    // namespaced events too.
	    this.each(function() {
	        var handlers = $._data(this, 'events')[name.split('.')[0]];
	        // take out the handler we just inserted from the end
	        var handler = handlers.pop();
	        // move it at the beginning
	        handlers.splice(0, 0, handler);
	    });
	},
	// Add highlight function to jQuery
	highlight: function(fadeTime) {
		fadeTime = fadeTime || 500;
		$(this).each(function() {
			var el = $(this);
			el.before("<div/>");
			el.prev()
			.width(el.outerWidth())
			.height(el.outerHeight())
			.css({
				'position': 'absolute',
				'background-color': '#ffff99',
				'opacity': '.3',
				'pointer-events': 'none'
			})
			.fadeOut(fadeTime, function() {
				$(this).remove();
			});
		});
	},

	permaLink: function(param) {
		var config = {
			table: location.pathname.match(/\/(\w+).php/,'')[1],
			id: 0,
			init: "",
			origin: {},
			ref: 'title',
		}
		$.extend(config, param);

		$input_grp = $('<div class="input-group"></div>');
		$input = $('<input class="form-control" name="perma_link" type="text" value="" size="40" data-pass="' + (config.id == 0 ? 0 : 1) + '" />');
		$input_raw = $('<input name="perma_link_raw" type="hidden" value="" />');
		$confirm = $('<button class="btn btn-info" type="button" id="update_perma">確定</button>');
		$span = $('<span class="input-group-btn"></span>');

		$div = $('<div style="text-align: left; margin-top: 7px; width: 100%;">' + decodeURIComponent(config.init) + '</div>');

		if (config.id == 0) {
			$('input[name=' + config.ref + ']').change(function(e) {
				if ($input.data('pass') != '1') {
					$input.val($(e.target).val())
				}
			})
		}

		$(".form-control.edit-language-select").change(function(e) {
			var link = config.init;
			$input_grp.hide();
			$div.show().css('display', 'table');
			var lang = $(e.target).val();
			if (lang != "zh_tw") {
				if (config.origin.hasOwnProperty(lang) && config.origin[lang].hasOwnProperty('perma_link') && config.origin[lang].perma_link != null) {
					link = config.origin[lang].perma_link;
				} else {
					$div.hide();
					$input_grp.show();
					$input.val("");
				}
			} else {
				if (config.init == "") {
					$div.hide();
					$input_grp.show().css('display', 'table');
				}
				$input.val(config.init);
			}
			if (link != "") {
				$div.html(config.url + decodeURIComponent(link));
			}
		})
		$(".form-control.edit-language-select").change();

		$span.append($confirm)
		if (config.priv === '1' || config.id == 0) {
			$input_grp.append($input_raw)
			$input_grp.append($input)
			$input_grp.append($span)
		} else {
			$input_grp.append('<div class="red" style="text-align: left; margin-top: 7px; width: 100%; display: table;">未設定</div>')
		}
		$(this).append($input_grp)
		$(this).append($div)

		$confirm.click(function(){
			$.ajax({
				url: '/ajax/update_perma_link.php',
				dataType: 'json',
				data: {
					act: 'test',
					link: $input.val(),
				},
				success: function (res) {
					if (res.error == 1) {
						alert(res.message)
					} else {
						var lang = $(".form-control.edit-language-select").val();
						var link = res.content.encode;
						var msg = "是否確定把 " + YOHO.admin.i18n.langs[lang] + " 版的網址設為: " + config.url + decodeURIComponent(link);
						if (confirm(msg)) {
							if (lang != 'zh_tw') {
								if (typeof localizedVersions[lang] === "undefined") {
									localizedVersions[lang] = {};
								}
								localizedVersions[lang].perma_link = link;
								if (typeof config.origin[lang] === "undefined") {
									config.origin[lang] = {};
								}
								config.origin[lang].perma_link = link;
							} else {
								$input.data('pass', 1);
								$input_raw.val(res.content.origin)
								config.init = link;
							}
							$input.val(link);
							$input_grp.hide();
							$div.show();
							$div.html(config.url + decodeURIComponent(link));
						}
					}
				}
			})
		})
	},
	permaLink_append: function(param) {
		var config = {
			table: location.pathname.match(/\/(\w+).php/,'')[1],
			id: 0,
			init: "",
			origin: {},
			ref: 'title',
		}
		$.extend(config, param);

		$input_grp = $('<div class="col-md-6 col-sm-6 col-xs-12" style="padding-left:0"></div>');
		$input = $('<input class="form-control" id="permalink" name="perma_link" type="text" value="" size="40" data-pass="' + (config.id == 0 ? 0 : 1) + '" />');
		$input_raw = $('<input name="perma_link_raw" type="hidden" value="" />');

		$div = $('<div style="text-align: left; margin-top: 7px; width: 100%;">' + decodeURIComponent(config.init) + '</div>');

		if (config.id == 0) {
			$('input[name=' + config.ref + ']').change(function(e) {
				if ($input.data('pass') != '1') {
					$input.val($(e.target).val())
				}
			})
		}

		$(".form-control.edit-language-select").change(function(e) {
			var link = config.init;
			$input_grp.hide();
			$div.show().css('display', 'table');
			var lang = $(e.target).val();
			$('.form-control.cur_lang').val(lang);
			if (lang != "zh_tw") {
				if (config.origin.hasOwnProperty(lang) && config.origin[lang].hasOwnProperty('perma_link') && config.origin[lang].perma_link != null) {
					link = config.origin[lang].perma_link;
				} else {
					$div.hide();
					$input_grp.show();
					$input.val("");
				}
			} else {
				if (config.init == "") {
					$div.hide();
					$input_grp.show().css('display', 'table');
				}
				$input.val(config.init);
			}
			if (link != "") {
				$div.html(config.url + decodeURIComponent(link));
			}
		})
		$(".form-control.edit-language-select").change();
		
		if (config.priv === '1') {
			$div.dblclick(function(){
				var lang = $(".form-control.edit-language-select").val();
				if (lang != "zh_tw") {
					if (config.origin.hasOwnProperty(lang) && config.origin[lang].hasOwnProperty('perma_link') && config.origin[lang].perma_link != null) {
						link = config.origin[lang].perma_link;
						$input.val(link);
					} else {
						$input.val("");
					} 
				} else {
					$input.val(config.init);
				}
				$(this).hide();
				$input_grp.show();
				$input.focus();
			});
		}
		if (config.priv === '1' || config.id == 0) {
			$input_grp.append($input_raw)
			$input_grp.append($input)
		} else {
			$input_grp.append('<div class="red" style="text-align: left; margin-top: 7px; width: 100%; display: table;">未設定</div>')
		}
		$(this).append($input_grp)
		$(this).append($div)
		$input.blur(function(){
			$.ajax({
				url: '/ajax/update_perma_link.php',
				dataType: 'json',
				data: {
					act: 'test',
					link: $input.val(),
				},
				success: function (res) {
					if (res.error == 1) {
						alert(res.message)
					} else {
						var lang = $(".form-control.edit-language-select").val();
						var link = res.content.encode;
						// var msg = "是否確定把 " + YOHO.admin.i18n.langs[lang] + " 版的網址設為: " + config.url + decodeURIComponent(link);
						// if (confirm(msg)) {
							if (lang != 'zh_tw') {
								if (typeof localizedVersions[lang] === "undefined") {
									localizedVersions[lang] = {};
								}
								localizedVersions[lang].perma_link = link;
								if (typeof config.origin[lang] === "undefined") {
									config.origin[lang] = {};
								}
								config.origin[lang].perma_link = link;
							} else {
								$input.data('pass', 1);
								$input_raw.val(res.content.origin)
								config.init = link;
							}
							$input.val(link);
							$input_grp.hide();
							$div.show();
							$div.html(config.url + decodeURIComponent(link));
						// }
					}
				}
			})
		});
		
	},
	autoSetDesc: function(param) {
		var cat_id = param.cat_id;
		$.ajax({
			url: '/ajax/setDesc.php',
			dataType: 'json',
			data: {
				cat_id: cat_id,
			},
			success: function (res) {

			}
		})
	},

});

$(function () {
	YOHO.admin.i18n.init();
});

}); // end of run_after_jquery block
