function displayEmail(mail_id, currPage, ticket_id) {
    $('#email_display').load('crm.php?act=email&step=1&mail_id=' + mail_id, function() {
        updateTicketStatus('gmail_ticket_status', ticket_id);
        $('.email_page').fadeOut("fast", function(){
            $('#email_display').data({
                page: currPage,
                tid: ticket_id
            }).fadeIn();
            $('.gmail_quote').hide();
            if ($('.WordSection1').length > 1) {
                $('.WordSection1').hide();
                $($('.WordSection1').get()[0]).show();
                $('.thread_item.active').append("<a class='hideQuote' href='javascript:void(0)' onclick='$(this).parents(\".thread_item\").find(\"[class*=WordSection1], .gmail_quote\").show();$(this).hide();'>顯示引用文字</a>")
            }
            $('#email_display a').prop('target', '_blank');
            $('#email_display img').get().map(function(v){
                var nW = $(v).prop('naturalWidth'),
                    nH = $(v).prop('naturalHeight');
                if (nW > 600) {
                    nH = Math.floor(600 / nW * nH);
                    nW = 600;
                }
                if (nH > 500) {
                    nW = Math.floor(500 / nH * nW);
                    nH = 500;
                }
                $(v).width(nW);
                $(v).height(nH);
                if ($(v).parents('table').length > 0) {
                    $(v).parents('table').addClass('MsoNormalTable');
                }
            });
            $("#inbox_container > [class*=col-], #email_body").css("max-height", email_height - 10 + 'px');
            $('#email_body').animate({
                scrollTop: $('.thread_item.active').offset().top - $('#email_body').offset().top
            }, 'slow');
        });
    });
}

function loadEmail() {
    $.ajax({
        url: 'crm.php',
        type: 'get',
        data: {
            act: 'email',
        },
        success: function(res) {
            $('#email_display').hide();
            $('#email').show();
            $('#email').html(res);
            if (email_height == 0) {
                email_height = $("#crm_container").height() - 74;
            }
            $("#inbox_container > [class*=col-], #email_body").css("max-height", email_height + 'px');
            turn_on_icheck();
        }
    })
}

function searchEmail(query) {
    $(".email_page").find(".btn, input:not([type=hidden]), .ml_div").addClass("disabled");
    if (typeof query === 'undefined') {
        query = [$('input[name=mail_query]').val()];
        $('input[name=category], input[name=system], input[name=label]').get().map((v)=>parseInt($(v).data('select')) > 0 ? query.push((parseInt($(v).data('select')) == 2 ? '-' : '') + $(v).val()) : true)
        query = query.join(' ');
    }
    $.ajax({
        url: 'crm.php',
        type: 'get',
        data: {
            act: 'email',
            query: encodeURIComponent(query),
        },
        success: function(res) {
            $('#email_display').hide();
            $('#email').show();
            $('#email').html(res);
            $("#inbox_container > [class*=col-], #email_body").css("max-height", email_height + 'px');
            turn_on_icheck();
        }
    })
}

function inboxPage(pageToken) {
    if ( typeof pageToken === 'string') {
        if ($('#' + pageToken).length == 0) {
            $(".email_page").find(".btn, input:not([type=hidden]), .ml_div").addClass("disabled");
            var query = [$('input[name=mail_query]').val()];
            $('input[name=category], input[name=system], input[name=label]').get().map((v)=>parseInt($(v).data('select')) > 0 ? query.push((parseInt($(v).data('select')) == 2 ? '-' : '') + $(v).val()) : true)
            $.ajax({
                url: 'crm.php',
                type: 'get',
                data: {
                    act: 'email',
                    pageToken: pageToken,
                    query: encodeURIComponent(query.join(' ')),
                },
                success: function(res) {
                    $(".email_page").fadeOut();
                    $('#email').append(res)
                    $(".email_page").find(".btn, input:not([type=hidden]), .ml_div").removeClass("disabled");
                    $("#inbox_container > [class*=col-], #email_body").css("max-height", email_height + 'px');
                    turn_on_icheck();
                }
            })
        } else {
            $(".email_page").fadeOut();
            $('#' + pageToken).fadeIn();
            turn_on_icheck();
        }
    } else if (typeof pageToken === 'object') {
        if ($(pageToken).parents('.email_page').prev('.email_page').length > 0) {
            $(".email_page").fadeOut();
            $(pageToken).parents('.email_page').prev('.email_page').fadeIn();
        }
    }
}

function displayAttachment(img_id) {
    $div = $("<div class='attachment_showcase'></div>");
    $img = $("<img src='" + $(img_id).prop("src") + "'>");
    $div.append($img);
    $popup = modal.create("附件", $div);
    modal.show($popup);
}

function downloadAttachment () {
    $("a.a_dl:not(.downloaded):not(.downloading)").get().map(function(v){
        var mail_id = $(v).data("m");
        var attachment_id = $(v).data("a");
        var mimeType = $(v).data("t");
        $(v).addClass("downloading");
        $.ajax({
            url: 'crm.php',
            dataType: 'json',
            data: {
                act: 'email',
                step: 3,
                mail_id: mail_id,
                attachment_id: attachment_id,
                mimeType: mimeType,
            },
            success: function(res){
                if (res.error != 1) {
                    $(v).removeClass("downloading");
                    if (res.content.mime.indexOf("image") !== -1) {
                        var img_id = $(v).prop("download").replace(/[^A-z\d_]/g, "_").split(".")[0];
                        if (typeof img_id === "undefined" || img_id ==  "") {
                            img_id = "attachment_" + Math.floor(Math.random() * 10000) + 100000;
                        }
                        $(v).prop("href", "javascript:displayAttachment('#" + img_id + "')");
                        $(v).html("<img id='" + img_id + "' src='" + res.content.data + "'>");
                        $(v).addClass("downloaded");
                    } else {
                        $(v).prop("href", res.content.data);
                        $(v).find("div.f_dl").html("下載 <span>(" + res.content.size + ")</span>");
                        $(v).addClass("downloaded");
                    }
                }
            }
        })
    })
}

$(document).on('click', '#refresh, #email_search_btn', function() {
    email_update = 0;
    searchEmail();
})

$(document).on('click', '#back_inbox', function() {
    $('#email_display').hide();
    $('#email_body .thread_item').empty();
    if ($('#email_display').data('page') == "") {
        $('.email_page').first().show();
    } else {
        $('#' + $('#email_display').data('page')).show();
    }
    $('#email_display').data({
        page: "",
        tid: ""
    });
    $('#gmail_ticket_status').data("tid", "");
    $('#gmail_ticket_status').html("");
    if (email_update == 1) {
        $("#refresh").click();
    }
})

$(document).on('click', '#archive, #mass_archive', function(e) {
    var mail_id = "";
    if ($(e.target).prop('id') == "archive") {
        mail_id = $(e.target).data('mid');
    } else {
        if ($("input[name=archiveMail]:checked").length > 0) {
            mail_id = $("input[name=archiveMail]:checked").get().map((v)=>$(v).val()).join(",");
        }
    }

    if (mail_id == "") {
        alert("請選擇要封存的電郵");
    } else {
        var callback = $(e.target).prop('id') == "archive" ? function() {
            email_update = 1;
            $("#back_inbox").click();
        } : function() {
            $("#refresh").click();
        }

        updateLabel(mail_id, "", "INBOX", '已' + ($(e.target).prop('id') == "archive" ? '' : '批量') + '封存電郵', {
            data: {
                archive: 1
            },
            callback: callback
        });
    }
})

$(document).on('click', '.thread_item:not(.active)', function (e) {
    e.stopPropagation();
    $elem = $(e.target).parents('.thread_item').length > 0 ? $(e.target).parents('.thread_item') : $(e.target);
    var mail_id = $elem.data('mid');
    $elem.load('crm.php?act=email&step=1&mail_only=1&mail_id='+mail_id, function(){
        $elem.addClass('active');
        $elem.find('.gmail_quote').hide();
        if ($elem.find('[class*=WordSection1]').length > 1) {
            $elem.find('[class*=WordSection1]').hide();
            $($elem.find('.WordSection1').get()[0]).show();
            $elem.append("<a class='hideQuote' href='javascript:void(0)' onclick='$(this).parents(\".thread_item\").find(\"[class*=WordSection1], .gmail_quote\").show();$(this).hide();'>顯示引用文字</a>")
        }
        $('#email_display a').prop('target', '_blank');
        $('#email_display img').get().map(function(v){
            var nW = $(v).prop('naturalWidth'),
                nH = $(v).prop('naturalHeight');
            if (nW > 600) {
                nH = Math.floor(600 / nW * nH);
                nW = 600;
            }
            if (nH > 500) {
                nW = Math.floor(500 / nH * nW);
                nH = 500;
            }
            $(v).width(nW);
            $(v).height(nH);
            if ($(v).parents('table').length > 0) {
                $(v).parents('table').addClass('MsoNormalTable');
            }
        });
        $('#email_body').animate({
            scrollTop: $('#email_body').scrollTop() + $elem.offset().top - $('#email_body').offset().top
        }, 'slow');
    })
})

$(document).on("click", ".ml_div:not(.disabled)", function (e){
    $elem = $(e.target).hasClass("ml_div") ? $(e.target) : $(e.target).parents(".ml_div");
    var mid = $elem.data("mid"),
        tid = $elem.data("tid"),
        token = $elem.data("token");
    if (mid != "" && tid != "") {
        displayEmail(mid, token, tid);
    }
})

$(document).on("keypress", "input[name=mail_query]", function(e) {
    if (e.which == 13) {
        e.preventDefault();
        searchEmail();
    }
})

$(document).on('click', '#reply, #new_draft', function (e) {
    Dropzone.autoDiscover = false;
    tinymce.EditorManager.execCommand('mceRemoveEditor', true, "draft_body");
    $form = $("<form class='dropzone tab-content form-horizontal form-label-left'></form");
    var html = "<div class='row'>" +
                    "<div class='col-md-12'>" +
                        "<input type='submit' onclick='$(\"#draft_step\").val(\"send\")' value='傳送' class='btn btn-primary'>" +
                        "<input type='submit' onclick='$(\"#draft_step\").val(\"save\")' value='儲存草稿' class='btn btn-info'>" +
                    "</div>" +
                "</div>";
    if ($(e.target).prop('id') == 'new_draft') {
        html += "收件人<input class='form-control' name='email' type='text'>" +
                "主旨<input class='form-control' name='subject' type='text'><br>"; 
    } else {
        html += "<input type='hidden' name='threadId' value='" + $(e.target).data('tid') + "'>" +
                "<input type='hidden' name='ticketId' value='" + $(e.target).data('ticket') + "'>" +
                "<input type='hidden' name='mailId' value='" + $(e.target).data('mid') + "'>";
    }
    html += "<input type='hidden' name='act' value='draft'>" +
            "<input type='hidden' id='empty_mail' name='empty_mail' value='0'>" +
            "<input type='hidden' id='draft_step' name='draft_step' value=''>" +
            "<div class='row'>" +
                "<div class='col-md-12'>" +
                    "<textarea name='draft_body' id='draft_body' class='tinymce'></textarea>" +
                "</div>" +
            "</div>" +
            "<div class='row'>" +
                "<div class='col-md-12'>" +
                    "<h4>附件 <span style='font-size: 12px'>(最多上傳10個檔案)</h4>" +
                    "<div id='dz-container' class='dropzone dz-clickable'><div class='dz-default dz-message'><span>Drop files here to upload</span></div></div>" +
                "</div>" +
                "<input type='file' multiple='multiple' class='dz-hidden-input' style='visibility: hidden;'>" +
            "</div>";
    $form.append(html);
    $popup = modal.create($(e.target).prop('id') == 'new_draft' ? '撰寫郵件' : '回覆郵件', $form);
    var myDropzone = new Dropzone("#dz-container", {
        url: 'crm.php',
        uploadMultiple: true,
        autoProcessQueue: false,
        addRemoveLinks: true,
        dictRemoveFile: '移除',
        parallelUploads: 10,
        maxFiles: 10,
        init: function() {
            var thisDropzone = this;
            thisDropzone.on("success", function(file, res) {
                res = JSON.parse(res);
                if (res.error == 1) {
                    file.status = Dropzone.QUEUED;
                    $(file.previewElement).removeClass("dz-success dz-complete");
                    alert(res.message);
                } else {
                    var msg = $('#draft_step').val() == 'send' ? '已傳送電郵' : '已儲存至草稿';
                    myDropzone.removeAllFiles(true);
                    init_PNotify(msg);
                    modal.hide($popup);
                    email_update = 1;
                    if (res.content.hasOwnProperty("ticket_id") && res.content.hasOwnProperty("mail_id")) {
                        displayEmail(res.content.mail_id, "", res.content.ticket_id);
                    }
                }
            })
        }
    });
    myDropzone.on('sending', function(file, xhr, formData){
        var result = {};
        $.each($form.serializeArray(), function() {
            result[this.name] = this.value;
        });
        for (var key in result) {
            formData.append(key, result[key])
        }
    });
    $form.submit(function(ev) {
        ev.preventDefault();
        $('#draft_body').val(tinyMCE.activeEditor.getContent());
        if (myDropzone.files.length > 0) {
            myDropzone.processQueue();
        } else {
            var data = $(this).serialize();
            $.ajax({
                url: 'crm.php',
                data: data,
                dataType: 'json',
                success: function (res) {
                    if (res.error == 1) {
                        alert(res.message);
                    } else {
                        if (res.content == 'empty') {
                            if (confirm('是否' + ($('#draft_step').val() == 'send' ? '發送' : '儲存') + '空白電郵?')) {
                                $('#empty_mail').val('1');
                                $form.submit();
                            }
                        } else {
                            var msg = $('#draft_step').val() == 'send' ? '已傳送電郵' : '已儲存至草稿';
                            myDropzone.removeAllFiles(true);
                            init_PNotify(msg);
                            modal.hide($popup);
                            email_update = 1;
                            if (res.content.hasOwnProperty("ticket_id") && res.content.hasOwnProperty("mail_id")) {
                                displayEmail(res.content.mail_id, "", res.content.ticket_id);
                            }
                        }
                    }
                }
            })
        }
    })
    modal.show($popup);
    tinymce.EditorManager.execCommand('mceAddEditor', true, "draft_body");
})