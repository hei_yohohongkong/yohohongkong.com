var platformContentWidth = $('#platform_content').width();
var informationWidth = $('#information').width();
var email_update = 0;
var timer = {
    chatra_update_all: 0,
    chatra_update_this: {
        ticket_id: 0,
        timer_id: 0
    },
    aircall_update: 0
}
var d = new Date();
var today = d.getFullYear() + "-" + ('0' + (d.getMonth() + 1)).slice(-2) + "-" + ('0' + d.getDate()).slice(-2);
d.setDate(d.getDate() - 1);
var yesterday = d.getFullYear() + "-" + ('0' + (d.getMonth() + 1)).slice(-2) + "-" + ('0' + d.getDate()).slice(-2);
var aircall_page_data = {};
var chatra_last_update = 1532322414748;
var email_height = 0;
var YOHO_CHATRA_LAST_UPDATE = localStorage.getItem("YOHO_CHATRA_LAST_UPDATE");
var twilio_call_list = {};
if (YOHO_CHATRA_LAST_UPDATE == null || YOHO_CHATRA_LAST_UPDATE < chatra_last_update) {
    localStorage.removeItem("YOHO_CHATRA_ALL_MSG");
    localStorage.removeItem("YOHO_CHATRA_LAST_MSG");
    localStorage.removeItem("YOHO_CHATRA_USERS");
    localStorage.setItem("YOHO_CHATRA_LAST_UPDATE", chatra_last_update);
}
$(function(){
    $MENU_TOGGLE.click();

    var navMenuHeight = $NAV_MENU.height(),
        breadcrumbHeight = $('.breadcrumb').height(),
        rightcolHeight = $RIGHT_COL.height();
    $("#crm_container, #chatra_nav, #chatra_display, #aircall_nav, #twilio_nav").css('height', (rightcolHeight - navMenuHeight - breadcrumbHeight - 80) + "px");
    $("#chatra_display .panel-body").css("height", ($("#chatra_display").height() - 120) + 'px');
    $("#dashboard_result").css("height", (rightcolHeight - navMenuHeight - breadcrumbHeight - 160) + 'px');
    $("#dashboard_stat").css("height", (rightcolHeight - navMenuHeight - breadcrumbHeight - 120) + 'px');
    $('#platform_nav a').click(function(e){
        $('#platform_nav a').removeClass('active');
        $target = $(e.target).is('#platform_nav a') ? $(e.target) : $(e.target).parents('#platform_nav a');
        $target.addClass('active');
        if (timer.chatra_update_this.timer_id != 0) {
            clearInterval(timer.chatra_update_this.timer_id);
            timer.chatra_update_this = {
                ticket_id: 0,
                timer_id: 0
            }
        }
        $("#info_search_form input[type=text]").val("");
        loadInfo();
    })
    informationWidth = $('#information').width();
    platformContentWidth = $('#platform_content').width();
    loadDashboard();

    if (!!window.EventSource) {
        timer.chatra_update_all = 999;
        timer.aircall_update = 999;
        var source = new EventSource('sse.php?target=crm');
        source.addEventListener('start', function(e) {
            console.log(e.data);
        }, false);
        source.addEventListener('crmUpdate', function(e) {
            var data = JSON.parse(e.data);
            for (var event in data) {
                if (event == 'aircall') {
                    // listenAircall();
                } else if (event == 'chatra') {
                    listenChatraAll();
                    if (data[event].indexOf(timer.chatra_update_this.ticket_id + "") !== -1) {
                        listenChatraMsg(timer.chatra_update_this.ticket_id);
                    }
                } else if (event == 'twilio') {
                    listenTwilio(data[event]);
                }
            }
        }, false);
        source.addEventListener('close', function(e) {
            source.close();
        }, false);
    }
    loadInfo();
})

function loadDashboard(action) {
    var query = 'crm.php?act=dashboard';
    query += "&status=" + (typeof $("select[name=dashboard_search_status]").val() === "undefined" ? -1 : $("select[name=dashboard_search_status]").val());
    query += "&type=" + (typeof $("select[name=dashboard_search_type]").val() === "undefined" ? -1 : $("select[name=dashboard_search_type]").val());
    query += "&priority=" + (typeof $("select[name=dashboard_search_priority]").val() === "undefined" ? -1 : $("select[name=dashboard_search_priority]").val());
    query += "&platform=" + (typeof $("select[name=dashboard_search_platform]").val() === "undefined" ? 0 : $("select[name=dashboard_search_platform]").val());
    var last_update = $("input[name=dashboard_date]").val();
    var last_update_start = last_update.split(" to ")[0];
    var last_update_end = last_update.split(" to ")[1];
    var page = parseInt($("input[name=dashboard_search_page]").val());
    var max = parseInt($("input[name=dashboard_search_max]").val());
    query += "&last_update_start=" + last_update_start;
    query += "&last_update_end=" + last_update_end;
    if (typeof action === "undefined") {
        page = 0;
    } else if (action == "prev" && page > 0) {
        page--;
    } else if (action == "next" && page < max) {
        page++;
    }
    query += "&page=" + page;
    $("input[name=dashboard_search_page]").val(page);
    $("#dashboard_result").load(query)
}
function loadInfo(){
    var user_name = $("#info_search_form input[name=user_name]").val();
    var order_sn = $("#info_search_form input[name=order_sn]").val();
    var ticket_id = $("#info_search_form input[name=ticket_id]").val();
    var query = "crm.php?act=info";
    if (typeof user_name != "undefined" && user_name != "") {
        query += "&user_name=" + encodeURIComponent(user_name);
    }
    if (typeof order_sn != "undefined" && order_sn != "") {
        query += "&order_sn=" + encodeURIComponent(order_sn);
    }
    if (typeof ticket_id != "undefined" && ticket_id != "") {
        query += "&ticket_id=" + encodeURIComponent(ticket_id);
    }
    $('#information').load(query + ' #info_frame', function() {
        init_daterangepicker();
    });
    toggleContentWidth(2);
}

function toggleOrder(order_id) {
    $('#order_page').load('order.php?act=info_crm&order_id=' + order_id);
    toggleContentWidth(1);
}

function toggleContentWidth(step) {
    if (step == 1) {
        $('#platform_content, #information').addClass('toggleWidth');
    } else {
        $('#platform_content, #information').removeClass('toggleWidth');
    }
}

function triggerRefund(order_sn, type) {
    $.ajax({
        type: 'get',
        url: 'refund_requests.php',
        data: {
            act: 'add',
            order_sn: order_sn,
            compensate: type,
            crm: 1
        },
        success: function (res){
            $div = $("<div></div>");
            $div.html(res);
            var title = type == 0 ? '全單退款' : '積分補償';
            $popup = modal.create(title, $div)
            modal.show($popup);
            turn_on_icheck();
        }
    })
}

function triggerPay(order_id) {
    $.ajax({
        type: 'post',
        url: 'order.php',
        data: {
            act: 'operate',
            order_id: order_id,
            pay: '付款',
            crm: 1
        },
        success: function (res){
            $div = $("<div></div>");
            $div.html(res);
            $popup = modal.create('訂單操作：付款', $div, false, 'md')
            modal.show($popup);
            turn_on_icheck();
        }
    })
}

function triggerWSPay(order_id, order_sn) {
    $.ajax({
        type: 'post',
        url: 'wholesale_payments.php',
        data: {
            act: 'add',
            order_id: order_id,
            crm: 1
        },
        success: function (res){
            $div = $("<div></div>");
            $div.html(res);
            $popup = modal.create('批發訂單付款', $div)
            modal.show($popup);
            turn_on_icheck();
        }
    })
}

function triggerRemark(order_id, order_sn){
    $.ajax({
        url: 'crm.php',
        dataType: 'json',
        data: {
            act: 'salesperson'
        },
        success: function(res) {
            $html = $("<div class='x_panel'>" +
                        "<form name='theForm' class='tab-content form-horizontal form-label-left' onsubmit='return orderOperation(\"remark\")'>" +
                            "<div class='form-group'>" +
                                "<label class='control-label col-md-3'>操作員</label>" +
                                "<div class='col-md-6 col-sm-6 col-xs-12'><select class='form-control' name='operator'><option>請選擇..</option>" +
                                    res.content.sales.reduce((p,c,i)=>p += "<option" + (i == res.content.selected ? " selected" : "") + ">" + c + "</option>", "") +
                                "</select></div>" +
                            "</div>" +
                            "<div class='form-group'>" +
                                "<label class='control-label col-md-3'>操作備註</label>" +
                                "<div class='col-md-6 col-sm-6 col-xs-12'><textarea class='form-control' name='action_note' cols='60' rows='3'></textarea></div>" +
                            "</div>" +
                            "<div class='form-group'>" + 
                                "<div class='ln_solid'></div>" +
                                "<div class='col-md-9 col-sm-9 col-xs-12 col-md-offset-3'>" +
                                "<input type='submit' name='submit' value=' 確定 ' class='btn btn-primary'>" +
                                    "<input type='hidden' name='order_id' value=" + order_id + ">" +
                                    "<input type='hidden' name='order_sn' value=" + order_sn + ">" +
                                    "<input type='hidden' name='operation' value='after_service'>" +
                                    "<input type='hidden' name='act' value='operate_post'>" +
                                    "<input type='hidden'name='crm' value='1'>" +
                                "</div>" +
                            "</div>" +
                        "</form>" +
                    "</div>");
            $popup = modal.create('新增備註', $html);
            modal.show($popup);
        }
    })
}

function orderOperation(type) {
    if (type == 'pay') {
        if (check()) {
            $.ajax({
                url: 'order.php',
                dataType: 'json',
                data: $('form[name=theForm]').serialize(),
                success: function (res) {
                    if(res.error == 1){
                        alert(res.message);
                    } else {
                        var status_id = $('#platform_nav a.active').prop('id').replace("tab", "") + "ticket_status";
                        updateCRMInfo(status_id, {
                            type: 1,
                            // priority: 2,
                            // status: 1,
                            order_id: $('form[name=theForm]').find('input[name=order_id]').val()
                        });
                        modal.hide($('.modal'));
                        $('#info_search_form input[name=order_sn]').val(document.forms['theForm'].elements['order_sn'].value);
                        loadInfo();
                    }
                },
                error: function(res){
                    alert(res.responseText);
                }
            })
        }
    } else if (type == 'remark') {
        $.ajax({
                url: 'order.php',
                dataType: 'json',
                data: $('form[name=theForm]').serialize(),
                success: function (res) {
                    if(res.error == 1){
                        alert(res.message);
                    } else {
                        alert(res.content);
                        modal.hide($('.modal'));
                        $('#info_search_form input[name=order_sn]').val(document.forms['theForm'].elements['order_sn'].value);
                        loadInfo();
                    }
                },
                error: function(res){
                    alert(res.responseText);
                }
            })
    }
    return false;
}

function rollToChatBottom(elem) {
    $(elem).animate({
        scrollTop: $(elem)[0].scrollHeight
    }, 'slow');
}

function updateCRMInfo(id, obj, ticket_id) {
    var ticket = id == "" ? ticket_id : $('#' + id).data('tid') != "" ? $('#' + id).data('tid') : "";
    if (ticket != "") {
        var data = {
            act: 'link',
            ticket: ticket,
        }
        for(var key in obj) {
            data[key] = obj[key];
        }
        $.ajax({
            url: 'crm.php',
            dataType: 'json',
            data: data,
            success: function (res) {
                if (res.error == 1) {
                    alert(res.message);
                } else {
                    updateTicketStatus(id, ticket);
                    email_update = 1;
                }
            }
        })
    }
}

function updateTicketStatus(id, ticket_id, callback) {
    callback = typeof callback === "undefined" ? function(){} : callback;
    $('#' + id).load('crm.php?act=link&step=1&ticket_id=' + ticket_id, callback);
    $('#' + id).data('tid', ticket_id);
}

function updateLabel(mail_id, add, rm, msg, opt) {
    var data = {
        act: 'email',
        step: 2,
        mail_id: mail_id,
        add: add,
        rm: rm,
    };
    var callback = "";
    if (typeof opt !== "undefined") {
    if (typeof opt.data !== "undefined") {
        for (var key in opt.data) {
            data[key] = opt.data[key];
        }
    }
    if (typeof opt.callback !== "undefined") {
        callback = opt.callback;
    }
    }

    $.ajax({
        url: 'crm.php',
        type: 'get',
        dataType: 'json',
        data: data,
        success: function(res) {
            if (res.error == 1) {
                msg = res.message;
            } else {
                callback != ""  ? callback() : true;
            }
            init_PNotify(msg);
        }
    })
}

function createNotice (opts) {
    var icons = {
        success: 'info-circle',
        danger: 'exclamation-triangle'
    }
    var icon = typeof opts.icon === "undefined"
        ? typeof icons[opts.type] === "undefined"
            ? icons.info
            : icons[opts.type]
        : opts.icon;
    var notice_id = "crm_notice_" + $(".crm_notice_container").length;
    var html = "<div id='" + notice_id + "' class='crm_notice_container alert alert-" + opts.type + (typeof opts.addClass === "undefined" ? "" : " " + opts.addClass) + "'>" +
            "<div class='crm_notice_header' data-state='0'>" +
                "<i class='fa fa-" + icon + "' style='padding-right: 10px'></i>" + opts.title +
                "<i class='crm_notice_display fa fa-angle-down pull-right'></i>" +
            "</div>" +
            "<div class='crm_notice_body' id='" + (typeof opts.body_id === "undefined" ? "" : opts.body_id) + "'>" + opts.msg + "</div>" +
        "</div>";
    $div = $(html);
    $div.css("right", $(".crm_notice_container").length * 320);
    $div.appendTo('body');
    if (typeof opts.hide !== "undefined") {
        setTimeout(function () {
            if ($("#" + notice_id + " .crm_notice_header").data("state") == 0) {
                $("#" + notice_id + " .crm_notice_header").click();
            }
        }, opts.hide)
    }
}

function updateNoticePosition() {
    $(".crm_notice_container").get().map(function (v, i) {
        $(v).css("right", i * 320);
    })
}

function displayTicketDetail (ticket_id, platform) {
    $("#" + platform.toLowerCase() + "_tab .mail_list").click();
    if (platform == "Chatra") {
        displayChatra(ticket_id);
    } else if (platform == "Aircall") {
        displayAircall(ticket_id);
    }
}

function switchDashbord () {
    $(".dashboard_result, #dashboard_stat").toggle();
    $("#dashbord_switch").toggleClass(".fa-chart-bar .fa-list-alt")
}

function searchOfflinePayment (self) {
    if ($("#offline_search_bar").hasClass("active")) {
        $("#offline_search_bar").hide().animate({opacity: 0});
        $("#offline_search_result").hide();
        $(".top_btn .btn").text("線下付款")
    } else {
        $("#offline_search_bar").show().animate({opacity: 1});
        $("#offline_search_result").show();
        $(".top_btn .btn").text("X")
    }
    $("#offline_search_bar").toggleClass("active");
    $(".top_btn .btn").toggleClass("btn-primary btn-danger");
    $(".top_btn .btn").blur();
}

$(document).on('click', '.editable#crm_type_label', function(e) {
    e.stopImmediatePropagation();
    $('#dropdownTypeList').dropdown('toggle');
})

$(document).on('click', '#crm_status_label', function(e) {
    e.stopImmediatePropagation();
    $('#dropdownStatusList').dropdown('toggle');
})

$(document).on('click', '#crm_priority_label', function(e) {
    e.stopImmediatePropagation();
    $('#dropdownPriorityList').dropdown('toggle');
})

$(document).on('click', '.checkmark', function (e) {
    e.stopPropagation();
    $input = $(e.target).parents('.multiselect').children('input[type=checkbox]');
    var status = parseInt($input.data('select'));
    var name = $input.prop('name');
    $(e.target).removeClass('selected intermediate');
    $(e.target).html('');
    status = (status + 1) % ($input.data('accept') != 'intermediate' ? 2 : 3);
    if (status == 0 && name == 'crm_type') {
        status = 1;
    }
    
    if ($input.data('multiselect') == 'disable') {
        $('input[name=' + name + ']').data('select', 0);
        $('input[name=' + name + ']').parents('.multiselect').children('.checkmark').html("");
        $('input[name=' + name + ']').parents('.multiselect').children('.checkmark').removeClass('selected intermediate');
    }
    var status_id = "";
    if (['crm_type', 'crm_priority', 'crm_status'].indexOf(name) != -1) {
        var status_id = $('#platform_nav a.active').prop('id').replace("tab", "") + "ticket_status";
    }
    if (name == 'modify') {
        updateLabel($input.data('mail'), status == 1 ? $input.val() : '', status == 0 ? $input.val() : '', '已' + (status == 0 ? '移除' : '新增') + '標籤');
    } else if (name == 'crm_type') {
        updateCRMInfo(status_id, { 
            type: $input.val(),
            // priority: $input.val() == 4 ? 2 : ["5", "6"].indexOf($input.val()) == -1 ? 1 : 3,
            // status: 1,
        });
    } else if (name == 'crm_priority') {
        updateCRMInfo(status_id, { 
            priority: $input.val(),
        });
    } else if (name == 'crm_status') {
        updateCRMInfo(status_id, { 
            status: $input.val(),
        });
    }

    if (status == 1) {
        $(e.target).addClass('selected');
        $(e.target).html("<i class='fa fa-check'></i>");
    } else if (status == 2) {
        $(e.target).addClass('intermediate');
        $(e.target).html("<i class='fa fa-times'></i>");
    }
    $input.data('select', status)
})
$(document).on('click', '.multiselect', function (e) {
    e.stopPropagation();
    $(e.target).find('.checkmark').click()
})
$(document).on('click', '.checkmark i', function (e) {
    e.stopPropagation();
    $(e.target).closest('.checkmark').click()
})

$(document).on('click', '.info_tab', function(e){
    $('.info_tab').removeClass('active');
    $(e.target).addClass('active');
})

$(document).on('click', 'button.dropdown-toggle.disableClick', function(e) {
    e.stopPropagation();
    $(this).parent().toggleClass('open')
})

$(document).on('click', 'body', function(e) {
    if ($(e.target).parents('button.dropdown-toggle.disableClick').length == 0) {
        $('button.dropdown-toggle.disableClick').parent().removeClass('open');
    }
})

$(document).on("click", ".payment_confirm:not(.disabled)", function(e) {
    e.stopPropagation();
    updateCRMInfo("", {
        type: 1,
        status: 3,
    }, $(e.target).data("tid"))
    updateLabel( $(e.target).data("mid"), "STARRED", "", "已添加星號", {callback: function(){
        $("#refresh").click();
    }})
})

$(document).on("keypress", "#info_search_form input[type=text]", function(e) {
    if (e.which == 13) {
        e.preventDefault();
        loadInfo();
    }
})
$(document).on("keypress", ".msg_input_area", function (e){
    if(e.which == 13 && !e.shiftKey) {
        e.preventDefault();
        $("#" + $(e.target).data("button")).click();
    }
})

$(document).on("click", ".crm_notice_header", function (e) {
    $elem = $(e.target).hasClass("crm_notice_header") ? $(e.target) : $(e.target).parents(".crm_notice_header");
    $notice = $elem.parents(".crm_notice_container");
    $body = $notice.find(".crm_notice_body");
    var state = $elem.data('state');
    if (state == 0) {
        $body.hide();
    } else {
        $body.show();
    }
    $elem.data('state', state == 0 ? 1 : 0);
    $notice.find(".crm_notice_display").toggleClass("fa-angle-down fa-angle-up")
})

$(document).on("click", ".link_ticket", function (e) {
    var oid = $(e.target).data("oid");
    var uid = $(e.target).data("uid");
    var status_id = "#" + $('#platform_nav a.active').prop('id').replace("tab", "") + "ticket_status";
    if (oid !== "" || uid !== "") {
        $html = $("<div class='row' style='text-align: center'>" +
                        "<input type='hidden' name='link_ticket_order' value='" + oid + "'>" +
                        "<input type='hidden' name='link_ticket_user' value='" + uid + "'>" +
                        "<div class='col-md-4'>查詢編號</div>" +
                        "<div class='col-md-8'><input class='form-control' type='text' name='link_ticket_id' value='" + $(status_id).data("tid") + "'></div>" +
                    "</div>" +
                    "<div class='row' style='margin-top: 10px;'><div class='col-md-4'></div><div class='col-md-8'><button class='btn btn-primary link_ticket_confirm'>確定</button></div>");
        $popup = modal.create('連結查詢', $html, false, "sm");
        modal.show($popup);
        $html.find("button").click(function () {
            var oid = $("input[name=link_ticket_order]").val();
            var uid = $("input[name=link_ticket_user]").val();
            var tid = $("input[name=link_ticket_id]").val();
            if (tid == "") {
                alert("請填寫查詢編號");
            } else {
                if (oid == "" && uid == "") {
                    alert("沒有可連結的資料")
                } else {
                    $.ajax({
                        url: 'crm.php',
                        dataType: 'json',
                        data: {
                            act: 'link',
                            ticket: tid,
                            user_id: uid,
                            order_id: oid,
                        },
                        success: function (res) {
                            if (res.error == 1) {
                                alert(res.message)
                            } else {
                                init_PNotify("己完成連結");
                                modal.hide($popup)
                            }
                        }
                    })
                }
            }
        })
    }
})

// For refund / compensate
$(document).on('submit', 'form[name=form]', function(e){
    e.preventDefault();
    $.ajax({
        type: $(this).attr('method'),
        url: $(this).attr('action'),
        data: $(this).serialize(),
        dataType: 'json',
        success: function (res){
            if(res.error == 1){
                alert(res.message);
                $('form[name=form] input[name=Submit]').prop('disabled', false)
            } else {
                var status_id = $('#platform_nav a.active').prop('id').replace("tab", "") + "ticket_status";
                updateCRMInfo(status_id, {
                    type: 3,
                    // priority: 3,
                    // status: 1,
                    order_id: $('form[name=form]').find('input[name=order_id]').val()
                });
                alert(res.content ? res.content : '成功申請');
                modal.hide($('.modal'));
                $('#info_search_form input[name=order_sn]').val(document.forms['form'].elements['order_sn'].value);
                loadInfo();
            }
        }
    })
})

$(document).on('click', 'input[name=offline_search_confirm]', function() {
    $.ajax({
        url: 'crm.php',
        data: {
            act: 'offline',
            date: $("input[name=offline_search_date]").val(),
        },
        dataType: 'json',
        success: function(res){
            var list = res.content;
            $table = $("<table class='table table-striped'><tr><th></th><th>付款時間</th><th>訂單號</th><th>金額</th><th>查詢編號</th></tr></table>");
            list.map(function (v, i) {
                $table.append(
                    "<tr>" +
                        "<td>" + (i + 1) + "</td>" +
                        "<td>" + v.formatted_log_time + "</td>" +
                        "<td><a href='javascript:searchOfflinePayment();$(\"#info_search_form input[name=order_sn]\").val(\"" + v.order_sn + "\");loadInfo();'>" + v.order_sn + "</a></td>" +
                        "<td>" + v.money_paid + "</td>" +
                        "<td>" + (v.ticket_id == null ? "--" : "<a href='javascript:searchOfflinePayment();$(\"#info_search_form input[name=ticket_id]\").val(\"" + v.ticket_id + "\");loadInfo();'>" + v.ticket_id + "</a>") + "</td>" +
                    "</tr>"
                );
            })
            if (list.length == 0) {
                $table.append("<tr><td colspan='5' style='text-align: center'>沒有找到任何記錄</td></tr>")
            }
            $("#crm_info_tab").click();
            $("#offline_search_result").html("");
            $("#offline_search_result").append($table);
        }
    })
})

$(document).on('click', '#platform_nav > a', function (e) {
    $target = $(e.target).is('a') ? $(e.target) : $(e.target).parents('a');
    if ($target.data("init")) {
        eval($target.data("init"));
        $target.data("init", "")
    }
})