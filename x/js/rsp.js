$('.disabled_input').prop('disabled', true);
console.log($('.disabled_input'));
function checkExistOrder($this) {
    if($($this).val().length > 0) {
        $form = $(this).parent('form');
        $.ajax({
            url: 'rsp_record.php?act=findOrder',
            type: 'POST',
            data: {
                order_sn: $($this).val()
            },
            error: function (xhr) {
                alert('Ajax request 發生錯誤');
            },
            success: function (result) {
                result = jQuery.parseJSON(result);
                if (result.error > 0) {
                    alert(result.message);
                    $('.disabled_input').prop('disabled', true);
                } else {
                    // $form
                    $.each(result.goods_list, function($goods_id, $goods){
                        $('input[name="nr_cat['+$goods_id+']"]').val($goods.total);
                    });
                    $('input[name="tel"]').val(result.order.tel);
                    $('input[name="consignee"]').val(result.order.consignee);
                    $('input[name="address"]').val(result.order.address);
                    $('.disabled_input').prop('disabled', false);
                }
            }
        });
    }
}

