window.timer = [];
var stock_breakdown_top = stock_breakdown_top || 0;
var stock_breakdown_left = stock_breakdown_left || 0;
var stored_table = {};
$(document).on("mouseover", "a.stock_breakdown", function(e) {
    $target = $(e.target).hasClass('stock_breakdown') ? $(e.target) : $(e.target).parents('.stock_breakdown');
    $("td").removeClass("stock_breakdown_td");
    $target.parents('td').addClass("stock_breakdown_td");
    var gid = $target.data("gid");
    var pos = $target.data("pos");

    $div = $("<div class='stock_breakdown active'></div>");
    $div.css({
        top: $target.offset().top + $target.height() + 5 + stock_breakdown_top,
        left: $target.offset().left - 600 + stock_breakdown_left,
        'max-width': '800px'
    });
    $("div.stock_breakdown").remove();
    window.timer.map(v=>clearTimeout(v));
    if (gid in stored_table) {
        $div.append(stored_table[gid]);
        $("body").append($div);
    } else {
        $.ajax({
            url: (pos ? '/x/' : '') + 'erp_stock_inquiry.php',
            dataType: 'json',
            data: {
                act: 'stock_breakdown_query',
                gid: gid,
                pos: pos ? 1 : 0,
            },
            success: function (res) {
                var stocks = res.content.stocks;
                var warehouses = res.content.warehouses;
                var goods = res.content.goods;
                var warehouse_html = "";
                var stock_html = [];
                for (var warehouse_id in warehouses) {
                    warehouse_html += "<th>" + warehouses[warehouse_id] + "</th>";
                }
                for (var goods_id in stocks) {
                    var html = ["<td>" + goods[goods_id] + "</td>"];
                    var total_stocks = 0;
                    for (var warehouse_id in stocks[goods_id]) {
                        total_stocks += parseInt(stocks[goods_id][warehouse_id]);
                        html.push("<td>" + stocks[goods_id][warehouse_id] + "</td>");
                    }
                    html.splice(1, 0, "<td>" + total_stocks + "</td>");
                    stock_html.push("<tr>" + html.join("") + "</tr>");
                }
                $table = $("<table id='stock_breakdown_basic' class='table'>" +
                                "<tr>" +
                                    "<th>產品名稱</th>" +
                                    "<th>總庫存</th>" +
                                    warehouse_html +
                                    stock_html.join("") +
                                "</tr>" +
                            "</table>");
                $table.find("td").css("padding", "10px 5px");
                stored_table[gid] = $table;
                $div.append($table)
                $("body").append($div);
            }
        })
    }
}).on("mouseout", "a.stock_breakdown, div.stock_breakdown", function(e) {
	window.timer.push(setTimeout(function(){
        $("div.stock_breakdown").removeClass("active");
        $("td").removeClass("stock_breakdown_td");
	}, 500));
}).on("mouseover", "div.stock_breakdown", function(e) {
	window.timer.map(v=>clearTimeout(v));
})