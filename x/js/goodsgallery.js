/***
* goodsgallery.js
*
* Javascript for 產品相冊
***/


var YOHO = YOHO || {};

(function ($){

YOHO.goodsgallery = {
	
	goods_id: null,
	$galleryTable: null,
	$galleryList: null,
	$galleryUpload: null,
	
	init: function () {
		this.goods_id = parseInt($('input[type="hidden"][name="goods_id"]').val(), 10);
		this.$galleryTable = $('#gallery-table');
		this.$galleryList = this.$galleryTable.find('div.form-group').first().find('#galleryList');
		this.$galleryUpload = this.$galleryTable.find('div.form-group').last();
		this.thumbWidth = this.$galleryTable.data('thumbWidth');
		this.thumbHeight = this.$galleryTable.data('thumbHeight');
		
		if (this.goods_id)
		{
			this.setupUploader();
			//this.loadList();
		}
		else
		{
			$(this.$galleryUpload).html('若要新增圖片到產品相冊，請先儲存產品。').css('text-align', 'center');
		}
	},
	
	setupUploader: function () {
		if (!this.goods_id) return;
		var _this = this;
		// Build cropit container
		var $container = buildCropit('gallery', 400, 400);
		// Add text field and button to the container
		var $extraDiv = $('<div>').css({'margin-top': '4px', 'text-align': 'center'});
		var $inputDesc = $('<input placeholder="圖片描述">').css({'margin-right': '8px', 'width': '200px'});
		$extraDiv.append($inputDesc);
		var $btn = $('<input type="button" value="上傳圖片" class="btn btn-primary">');
		$extraDiv.append($btn);
		$extraDiv.appendTo($container);
		// Replace $galleryUpload content with cropit container
		this.$galleryUpload.empty().append($container);
		// Bind cropit events
		initCropit($container);
		// Handle upload button clicks
		$btn.click(function (event) {
			event.preventDefault();
			// Hide controls
			$container.find('input[type="file"]').hide();
			$inputDesc.hide();
			$btn.hide();
			// Set uploading text
			$container.append('<span style="text-align: center;">上傳中...</span>');
			// setTimeout with 0 timeout to allow browser update display before proceeding
			setTimeout(function() {
				// Exporting is slow if the image is large
				cropitExport($container, function (blob){
					// Upload the image with XHR2
					_this.addImage($inputDesc.val(), blob);
				});
			}, 0);
		});
	},

	loadList: function () {
		var _this = this;
		$.ajax({
			url: 'goods.php',
			type: 'get',
			data: {
				'act': 'get_gallery_images',
				'goods_id': _this.goods_id
			},
			dataType: 'json',
			success: function (result) {
				if (result.error === 0)
				{
					_this.$galleryList.empty();
					$.each(result.content, function (i, row) {
						var $div = $('' +
						'<div id="gallery_' + row.img_id + '" style="float:left; text-align:center; border: 1px solid #DADADA; margin: 4px; padding:2px;">' +
						'  <a href="javascript:;" data-img-id="' + row.img_id + '">[-]</a><br />' +
						'  <a href="goods.php?act=show_image&img_url=' + row.img_url + '" target="_blank">' +
						'  <img src="../' + (row.thumb_url ? row.thumb_url : row.img_url) + '" ' + (_this.thumbWidth > 0 ? 'width="' + _this.thumbWidth + '"' : '') + ' ' + (_this.thumbHeight > 0 ? 'height="' + _this.thumbHeight + '"' : '') + 'border="0" />' +
						' </a><br />' +
						'  <input type="text" value="' + row.img_desc.replace(/"/g, '&quot;') + '" size="15" name="old_img_desc[' + row.img_id + ']" />' +
						'  <input type="text" value="' + row.sort_order + '" size="3" style="text-align: center;" name="old_img_sort[' + row.img_id + ']" />' +
						'</div>');
						_this.$galleryList.append($div);
						// Bind delete click event
						$div.find('a[href^="javascript"]').click(function (event) {
							event.preventDefault();
							if (confirm('您確實要刪除該圖片嗎？'))
							{
								var img_id = $(this).data('imgId');
								_this.deleteImage(img_id);
							}
						});
					});
				}
				else
				{
					if (result.hasOwnProperty('message') && result.message.length)
					{
						alert(result.message);
					}
					else
					{
						alert('Error: ' + result.error);
					}
				}
			},
			error: function (xhr) {
				console.log(xhr);
			}
		});
	},
	
	addImage: function (img_desc, blob) {
		var _this = this;
		// Upload file with FormData + XHR2
		var formData = new FormData();
		// Add parameters to formData
		$.each({
			'act': 'add_gallery_image',
			'goods_id': _this.goods_id,
			'img_desc[]': img_desc,
			'img_file[]': ''
		}, function (key, val) {
			formData.append(key, val);
		});
		// Add the image to formData
		formData.append('img_url[]', blob, 'image.png');
		// AJAX POST the formData to server
		$.ajax({
			url: 'goods.php',
			type: 'post',
			// pass FormData to XHR2 directly
			data: formData,
			processData: false,
			contentType: false,
			dataType: 'json',
			success: function (result) {
				_this.loadList();
				_this.setupUploader();
			},
			error: function (xhr) {
				console.log(xhr);
			}
		});
	},
	
	deleteImage: function (img_id) {
		$.ajax({
			url: 'goods.php',
			type: 'get',
			data: {
				'is_ajax': 1,
				'act': 'drop_image',
				'img_id': img_id
			},
			dataType: 'json',
			success: function (result) {
				if (result.error === 0)
				{
					$('div#gallery_' + img_id).remove();
				}
				else
				{
					if (result.hasOwnProperty('message') && result.message.length)
					{
						alert(result.message);
					}
					else
					{
						alert('Error: ' + result.error);
					}
				}
			},
			error: function (xhr) {
				console.log(xhr);
			}
		});
	}
};

$(function () {
	YOHO.goodsgallery.init();
});

})(jQuery);