$(document).on('click', '.checkmark', function (e) {
    e.stopPropagation();
    $input = $(e.target).parents('.multiselect').children('input[type=checkbox]');
    var status = parseInt($input.data('select'));
    var name = $input.prop('name');
    $(e.target).removeClass('selected intermediate');
    $(e.target).html('');
    status = (status + 1) % ($input.data('accept') != 'intermediate' ? 2 : 3);
    
    if ($input.data('multiselect') == 'disable') {
        $('input[name=' + name + ']').data('select', 0);
        $('input[name=' + name + ']').parents('.multiselect').children('.checkmark').html("");
        $('input[name=' + name + ']').parents('.multiselect').children('.checkmark').removeClass('selected intermediate');
    }

    if (status == 1) {
        $(e.target).addClass('selected');
        $(e.target).html("<i class='fa fa-check'></i>");
    } else if (status == 2) {
        $(e.target).addClass('intermediate');
        $(e.target).html("<i class='fa fa-times'></i>");
    }
    $input.data('select', status);

    var callback = $input.data("callback");
    if (typeof callback !== 'undefined') {
        eval(callback);
    }
})
$(document).on('click', '.multiselect', function (e) {
    e.stopPropagation();
    $(e.target).find('.checkmark').click()
})
$(document).on('click', '.checkmark i', function (e) {
    e.stopPropagation();
    $(e.target).closest('.checkmark').click()
})
$(document).on('input', 'input.multiselect-search', function (e) {
    e.stopPropagation();
    var li = "li[data-name=" + $(e.target).data("name") + "]";
    $(li + ":not(.no_result)").css("margin-top", "0px");
    if ($(e.target).val() == "") {
        $(li).show();
        $(li).first().css("margin-top", "37px");
        $(li + ".no_result").hide();
    } else {
        $(li).hide();
        if ($(li + "[data-label*=" + $(e.target).val().toLowerCase() + "]").length == 0) {
            $(li + ".no_result").show();
        }
        $(li + "[data-label*=" + $(e.target).val().toLowerCase() + "]").show();
        $(li + "[data-label*=" + $(e.target).val().toLowerCase() + "]").first().css("margin-top", "37px");
    }
})
$(document).on("click", "body", function(e) {
    if (!$(e.target).hasClass("multiselect-search")) {
        $(".multiselect-search").val("").trigger("input");
    }
})