function listenAircall() {
    $.ajax({
        url: 'crm.php',
        dataType: 'json',
        data: {
            act: 'aircall',
            step: 2
        },
        success: function (res) {
            var result = res.content;
            var list = result.list
            $(".aircall_status:not(.aircall_miss)").html("").removeClass("aircall_answer");
            if (typeof list.missed == "undefined" && $(".aircall_miss_notify").length > 0) {
                $(".aircall_miss_notify").remove();
                updateNoticePosition();
                $(".aircall_miss").html("").removeClass("aircall_miss");
            }
            if (typeof list.forward == "undefined" && $(".aircall_forward_notify").length > 0) {
                $(".aircall_forward_notify").remove();
                updateNoticePosition();
            }
            for(var item in list) {
                if (list[item].length > 0 && item == "answering") {
                    $("#aircall_status_" + list[item][0].ticket_id).html("<i class='fa fa-microphone'></i> 接聽中");
                    $("#aircall_status_" + list[item][0].ticket_id).addClass("aircall_answer").removeClass("aircall_miss");
                    if (list[item][0].notify != 1) {
                        $("#aircall_search input[name=aircall_phone]").val(list[item][0].raw_digits);
                        loadAircall();
                        displayAircall(list[item][0].ticket_id);
                    }
                    if ($('#aircall_tab').hasClass('active') && list[item][0].raw_digits.indexOf("Anonymous") == -1) {
                        if ($("#aircall_ticket_status").data('tid') != list[item][0].ticket_id) {
                            $('#info_search_form input[name=user_name]').val(list[item][0].raw_digits);
                            loadInfo();
                        }
                    }
                } else if (item == "missed") {
                    // var msg = "";
                    // list[item].map((v, i)=>msg += "<b>" + (i + 1) + "</b>: <span class='aircall_handle_phone' data-tid='" + v.ticket_id + "' data-cid='" + v.call_id + "'>" + v.raw_digits + "</span><br>")
                    // if (msg != "") {
                    //     if ($(".aircall_miss_notify").length > 0) {
                    //         $("#aircall_miss_notice").html(msg);
                    //         // $("#aircall_miss_count").html(list[item].length);
                    //     } else {
                    //         createNotice({
                    //             type: "danger",
                    //             addClass: "aircall_miss_notify",
                    //             title: "未接來電(<span id='aircall_miss_count'>" + result.miss_count + "</span>)",
                    //             msg: msg,
                    //             body_id: "aircall_miss_notice",
                    //             hide: 5000
                    //         })
                    //     }
                    // }
                } else if (item == "forward") {
                    var msg = "";
                    list[item].map((v)=>msg += "<b>" + v.admin + "</b>: <span class='aircall_handle_phone' data-tid='" + v.ticket_id + "' data-cid='" + v.call_id + "'>" + v.raw_digits + "</span><br>")
                    if (msg != "") {
                        if ($(".aircall_forward_notify").length > 0) {
                            $("#aircall_forward_notice").html(msg);
                            $("#aircall_forward_count").html(list[item].length);
                        } else {
                            createNotice({
                                type: "success",
                                addClass: "aircall_forward_notify",
                                title: "接聽中(<span id='aircall_forward_count'>" + list[item].length + "</span>)",
                                msg: msg,
                                body_id: "aircall_forward_notice",
                                hide: 5000
                            })
                        }
                    }
                }
            }
        }
    })
}

function loadAircall(page){
    if (timer.aircall_update == 0 && !window.EventSource) {
        timer.aircall_update = setInterval(listenAircall, 3000);
    }
    var data = {
        act: 'aircall'
    }
    var missed = $("input[name=aircall_missed]").val();
    if ($("input[name=aircall_missed]").length == 0 || missed == 0) {
        if ($("#aircall_search").length != 0) {
            var aircall_phone = $("input[name=aircall_phone]").val();
            var aircall_date = $("#aircall_date").val();
            var split = aircall_date.split(" to ");

            data["aircall_phone"] = aircall_phone.replace(/[ \+]/g,"");
            data["aircall_start_date"] = split[0];
            data["aircall_end_date"] = split[1];
        }
    } else {
        data['missed'] = 1;
    }
    if (typeof page !== "undefined") {
        data["aircall_page"] = page;
    }
    $.ajax({
        url: 'crm.php',
        dataType: 'json',
        data: data,
        success: function(res){
            var result = res.content;
            var list = result.list;
            aircall_page_data = result.list;
            // Search bar
            var search = "<div id='aircall_search'><div class='row'>" +
                            "<div class='col-md-12'>" +
                                "<input name='aircall_missed' type='hidden' value='" + result.missed + "'>" +
                                "<div class='input-group' style='margin-bottom: 0px;'>" +
                                    "<span class='input-group-addon alert-default'><i class='fa fa-mobile-alt'></i></span>" +
                                    "<input class='form-control' type='text' name='aircall_phone' value='" + result.phone + "' style='max-width: 140px;'>" +
                                    "<span class='input-group-addon alert-default'><i class='fa fa-calendar-alt'></i></span>" +
                                    "<input class='form-control' type='text' name='aircall_date' id='aircall_date' data-type='daterange'>" +
                                    "<span class='input-group-btn'>" +
                                        "<button class='btn btn-info' type='button' onclick='$(\"input[name=aircall_missed]\").val(0);loadAircall();' style='margin: 0px'><i class='fa fa-search'></i></button>" +
                                    "</span>" +
                                "</div>" +
                            "</div>" +
                            "<div class='col-md-12' style='text-align: center; margin-bottom: 10px;'>" +
                                "<div class='btn-group btn-group-justified'>" +
                                    "<a id='aircall_prev' class='btn btn-warning disabled' role='button'><i class='fa fa-angle-left'></i></a>" +
                                    "<a id='aircall_search_missed' class='btn btn-danger' role='button'><i class='fa fa-microphone-slash'></i> 未接來電</a>" +
                                    "<a id='aircall_next' class='btn btn-warning disabled' role='button'><i class='fa fa-angle-right'></i></a>" +
                                "</div>" +
                            "</div>" +
                        "</div></div><div id='aircall_nav_list'></div>";
            $("#aircall_nav").html(search);
            if (list.length == 0) {
                $("#aircall_nav_list").append("<div class='aircall_nav_item active'><div class='row'><div class='col-md-12'>沒有找到任何記錄</div></div></div>");
            } else {
                list.map(function(v){
                    var last_call_class = v.call.answered_at === null ? " red" : " green";
                    var html = "<div class='aircall_nav_item' data-tid='" + v.ticket_id + "'>" +
                                    "<div class='row'>" +
                                        "<div class='col-md-8'>" +
                                            "<div class='row'>" +
                                                "<div class='col-md-8'><span class='from_user'>" + v.call.raw_digits + "</span></div>" +
                                                "<div class='col-md-4' style='text-align: right'><i class='fa fa-long-arrow-alt-" + (v.call.direction == "outbound" ? "up" : "down") + last_call_class + "'></i><i class='fa fa-phone" + (v.call.answered_at === null == 1 ? "-slash" : "") + last_call_class + "'></i></div>" +
                                            "</div>" +
                                            "<div class='row'>" +
                                                "<div class='col-md-6' title='通話長度'><i class='fa fa-stopwatch blue'></i> " + (v.call.answered_at === null == 1 ? "N/A" : v.call.answer_time) + "</div>" +
                                                "<div class='col-md-6'><span class='pull-right'>" + v.call.create_date + "</span></div>" +
                                            "</div>" +
                                        "</div>" +
                                        "<div class='col-md-4'>" +
                                            "<div style='height: 35px; text-align: center;'>" +
                                                "<div class='aircall_status' id='aircall_status_" + v.ticket_id + "'></div>" +
                                            "</div>" +
                                        "</div>" +
                                    "</div>" +
                                "</div>";
                    $("#aircall_nav_list").append(html);
                })
            }
            init_daterangepicker();
            if (result.start_date != "") {
                $("#aircall_date").data('daterangepicker').setStartDate(result.start_date);
            }
            if (result.end_date != "") {
                $("#aircall_date").data('daterangepicker').setEndDate(result.end_date);
            }
            if (result.prev_page != -1) {
                $("#aircall_prev").on("click", function(){loadAircall(result.prev_page)});
                $("#aircall_prev").removeClass("disabled");
            }
            if (result.next_page != -1) {
                $("#aircall_next").on("click", function(){loadAircall(result.next_page)});
                $("#aircall_next").removeClass("disabled");
            }
            $("#aircall_search_missed").click(function(){
                $("input[name=aircall_missed]").val(1);
                loadAircall();
            });
            $("#aircall_nav_list").height(($("#aircall_nav").height() - 80))
        }
    })
}

function displayAircall(ticket_id, page) {
    var data = {
        act: 'aircall',
        step: 1,
        ticket_id: ticket_id
    }
    if (typeof page !== "undefined") {
        data['page'] = page;
    }
    if (ticket_id !== null && typeof ticket_id !== 'undefined') {
    $.ajax({
        url: 'crm.php',
        dataType: 'json',
        data: data,
        success: function(res){
            var result = res.content;
            var list = result.list;

            if (result.user_name != "") {
                $('#info_search_form input[name=user_name]').val(result.user_name);
                loadInfo();
            }
            if (typeof page === "undefined") {
                $("#aircall_display").html("");
            }
            $(".aircall_message_load").remove();
            if (result.next_page != -1) {
                $load_more = $("<div class='row aircall_message_load' onclick='displayAircall(" + ticket_id + ", " + result.next_page + ")'><div class='col-md-12' style='text-align: center; margin-top: 10px'><span class='label label-default' style='padding: 5px 10px; cursor: pointer;'>載入更多</span></div></div>");
                $("#aircall_display").prepend($load_more)
            }
            Object.keys(list).sort().map(function(date){
                if ($(".aircall_date_divide[data-date=" + date + "]").length == 0) {
                    $div = $("<div class='row aircall_date_divide' data-date='" + date + "'><div class='col-md-12' style='text-align: center'><span class='label label-info' style='line-height: 2;'>" + (today == date ? '今日' : yesterday == date ? '昨日' : date) + "</span></div></div>");
                    var sorted_dates = $(".aircall_date_divide").get().map((v)=>$(v).data('date')).sort();
                     if (sorted_dates.length == 0 || date > sorted_dates[sorted_dates.length - 1]) {
                        $("#aircall_display").append($div);
                    } else if (date < sorted_dates[0]) {
                        $("#aircall_display").prepend($div);
                    } else {
                        sorted_dates.push(date);
                        $(".aircall_date_divide[data-date=" + sorted_dates[sorted_dates.sort().indexOf(date) + 1] + "]").before($div)
                    }
                } else {
                    $div = $(".aircall_date_divide[data-date=" + date + "]")
                }
                list[date].sort((a,b)=>a.started_at < b.started_at).map(function(v){
                    var html = "";
                    var color = v.answered_at === null ? " red" : " green";
                    var outbound = "fa fa-long-arrow-alt-" + (v.direction == "outbound" ? "up" : "down");
                    var missed = "fa fa-phone" + (v.answered_at === null ? "-slash" : "");
                    var extra_data = [];
                    if (v.missed_call_reason !== null) {
                        extra_data.push("<div class='col-md-1'><i class='fa fa-info-circle red' style='line-height: 1.5;'></i></div><div class='col-md-11'>" + v.missed_call_reason.split("_").map((v)=>v.charAt(0).toUpperCase() + v.substr(1)).join(" ") + "</div>")
                    }
                    if (v.voicemail !== null) {
                        extra_data.push("<div class='col-md-1'><i class='fa fa-microphone blue' style='line-height: 1.5;'></i></div><div class='col-md-5'><a href='" + v.voicemail + "' target='_blank'>留言</a></div>")
                    }
                    if (v.recording !== null) {
                        extra_data.push("<div class='col-md-1'><i class='fa fa-volume-down purple' style='line-height: 1.5;'></i></div><div class='col-md-5'><a href='" + v.recording + "' target='_blank'>錄音</a></div>")
                    }
                    html += "<div class='row aircall_record'>" +
                                "<div class='col-md-2'></div>" +
                                "<div class='col-md-8'>" +
                                    "<div class='row aircall_record_detail'>" +
                                        "<div class='col-md-5'><i class='fa fa-user'></i> " + (typeof v.user !== "undefined" && v.user != null ? v.user.name : "--") + "</div>" +
                                        "<div class='col-md-3'><i class='fa fa-alarm'></i> " + v.answer_time + "</div>" +
                                        "<div class='col-md-4'><i class='" + outbound + color + "'></i><i class='" + missed + color + "'></i> " + v.create_date + "</div>" +
                                        (extra_data.length > 0 ? "<div class='aircall_extra_info col-md-12'>" + extra_data.join("") + "</div>" : "") +
                                    "</div>" +
                                "</div>" +
                            "</div>";
                    $div.after(html);
                })
            })
        }
    })
    updateTicketStatus('aircall_ticket_status', ticket_id, function(){
        $("#aircall_display").height($("#aircall_nav").height() - $("#aircall_ticket_info").height());
    })
    }
}

$(document).on("keypress", "#aircall_search input[type=text]", function(e) {
    if (e.which == 13) {
        $("input[name=aircall_missed]").val(0);
        e.preventDefault();
        loadAircall();
    }
})

$(document).on("click", ".aircall_nav_item:not(.active)", function(e){
    $elem = $(e.target).hasClass("aircall_nav_item") ? $(e.target) : $(e.target).parents(".aircall_nav_item");
    $(".aircall_nav_item").removeClass("active")
    $elem.addClass("active")
    var ticket_id = $elem.data("tid");
    displayAircall(ticket_id);
})

$(document).on("click", ".aircall_handle_phone", function(e) {
    var cid = $(e.target).data('cid');
    var tid = $(e.target).data('tid');
    var phone = $(e.target).text();
    $("#aircall_tab .mail_list").click();
    displayAircall(tid);
    if (phone.indexOf("Anonymous") == -1) {
        $('#info_search_form input[name=user_name]').val(phone);
        loadInfo();
    }
})