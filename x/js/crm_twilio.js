$(function(){
    setTimeout(function(){
        init_twilio_panel();
    }, 1500)
})

function listenTwilio(data) {
    var items = [];
    data.map(function(v) {
        var detail = v.split("-");
        var list_id = detail[0]; // list_id -> ticket_id
        var action = detail[1];
        if (typeof action !== "undefined" && action !== "") {
            if (action == "queued") {
                items.push(list_id);
                if (!$(".twilio_call_button").hasClass("incoming") && $("#twilio_call_init").length == 0) {
                    $(".twilio_call_button").addClass("incoming");
                    if ($("#twilio_call_disconnect").length == 0) {
                        var audio = new Audio('sound/ringing.mp3');
                        audio.pause();
                        audio.play();
                    }
                }
                twilio_call_list[list_id] = {
                    act: action
                };
            } else if (action == "bridged") {
                if (typeof twilio_call_list[list_id] === "undefined") {
                    twilio_call_list[list_id] = {};
                }
                twilio_call_list[list_id].act = action;
            } else {
                delete twilio_call_list[list_id];
            }
        }
    })
    if (items.length > 0) {
        $.ajax({
            url: 'crm.php',
            dataType: 'json',
            data: {
                act: 'twilio',
                step: 2,
                list: items.join(",")
            },
            success: function(res) {
                res.content.list.map(function(v, i) {
                    twilio_call_list[v.ticket_id].sender = v.sender;
                })
                twilio_notify();
            }
        })
    } else {
        twilio_notify();
    }
}

function twilio_notify() {
    var msg = [];
    for (var ticket_id in twilio_call_list) {
        msg.push("<b>" + (msg.length + 1) + "</b>: <span class='twilio_handle_phone " + twilio_call_list[ticket_id].act + "' data-tid='" + ticket_id + "'>" + twilio_call_list[ticket_id].sender + "</span>" + (twilio_call_list[ticket_id].act == 'bridged' ? '<label style="margin-left: 10px; margin-bottom: 10px;" class="label label-danger">接聽中</label>' : ''));
    }
    msg = msg.join("<br>");
    $(".twilio_forward_notify").remove();
    if (msg != "") {
        createNotice({
            type: "info",
            addClass: "twilio_forward_notify",
            title: "轉接中(<span id='twilio_forward_count'>" + Object.keys(twilio_call_list).length + "</span>)",
            msg: msg,
            body_id: "twilio_forward_notice",
            hide: 5000
        });
    }
    if (Object.keys(twilio_call_list).length == 0) {
        $(".twilio_call_button").removeClass("incoming");
    }
    updateNoticePosition();
}

function init_twilio_panel() {
    $("body").append("<div id='twilio_call_init' class='twilio_call_button' title='未啟動'><i class='fa fa-phone'></i></div>")
}

function loadTwilio(page){
    if (timer.twilio_update == 0 && !window.EventSource) {
        timer.twilio_update = setInterval(listentwilio, 3000);
    }
    var data = {
        act: 'twilio'
    }
    var missed = $("input[name=twilio_missed]").val();
    if ($("input[name=twilio_missed]").length == 0 || missed == 0) {
        if ($("#twilio_search").length != 0) {
            var twilio_phone = $("input[name=twilio_phone]").val();
            var twilio_date = $("#twilio_date").val();
            var split = twilio_date.split(" to ");

            data["twilio_phone"] = twilio_phone.replace(/[ \+]/g,"");
            data["twilio_start_date"] = split[0];
            data["twilio_end_date"] = split[1];
        }
    } else {
        data['missed'] = 1;
    }
    if (typeof page !== "undefined") {
        data["twilio_page"] = page;
    }
    $.ajax({
        url: 'crm.php',
        dataType: 'json',
        data: data,
        success: function(res){
            var result = res.content;
            var list = result.list;
            twilio_page_data = result.list;
            // Search bar
            var search = "<div id='twilio_search'><div class='row'>" +
                            "<div class='col-md-12'>" +
                                "<input name='twilio_missed' type='hidden' value='" + result.missed + "'>" +
                                "<div class='input-group' style='margin-bottom: 0px;'>" +
                                    "<span class='input-group-addon alert-default'><i class='fa fa-mobile-alt'></i></span>" +
                                    "<input class='form-control' type='text' name='twilio_phone' value='" + result.phone + "' style='max-width: 140px;'>" +
                                    "<span class='input-group-addon alert-default'><i class='fa fa-calendar-alt'></i></span>" +
                                    "<input class='form-control' type='text' name='twilio_date' id='twilio_date' data-type='daterange'>" +
                                    "<span class='input-group-btn'>" +
                                        "<button class='btn btn-info' type='button' onclick='$(\"input[name=twilio_missed]\").val(0);loadTwilio();' style='margin: 0px'><i class='fa fa-search'></i></button>" +
                                    "</span>" +
                                "</div>" +
                            "</div>" +
                            "<div class='col-md-12' style='text-align: center; margin-bottom: 10px;'>" +
                                "<div class='btn-group btn-group-justified'>" +
                                    "<a id='twilio_prev' class='btn btn-warning disabled' role='button'><i class='fa fa-angle-left'></i></a>" +
                                    "<a id='twilio_search_missed' class='btn btn-danger' role='button'><i class='fa fa-microphone-slash'></i> 未接來電</a>" +
                                    "<a id='twilio_next' class='btn btn-warning disabled' role='button'><i class='fa fa-angle-right'></i></a>" +
                                "</div>" +
                            "</div>" +
                        "</div></div><div id='twilio_nav_list'></div>";
            $("#twilio_nav").html(search);
            if (list.length == 0) {
                $("#twilio_nav_list").append("<div class='twilio_nav_item active'><div class='row'><div class='col-md-12'>沒有找到任何記錄</div></div></div>");
            } else {
                list.map(function(v){
                    var last_call_class = " ";
                    var current = "";
                    var icon = "";
                    if (v.waited > 0) {
                        if (v.duration == "N/A") {
                            last_call_class += "red";
                        } else {
                            if (v.duration == "接聽中") {
                                current = "twilio_answer";
                            }
                            last_call_class += "green";
                        }
                    } else {
                        current = "twilio_queue";
                        last_call_class += "blue";
                    }
                    if (current == "twilio_answer") {
                        icon  = "<i class='fa fa-microphone'></i> 接聽中";
                    } else if (current == "twilio_queue") {
                        icon = "<i class='fa fa-pause'></i> 等候中";
                    }
                    var html = "<div class='twilio_nav_item' data-tid='" + v.ticket_id + "'>" +
                                    "<div class='row'>" +
                                        "<div class='col-md-8'>" +
                                            "<div class='row'>" +
                                                "<div class='col-md-8'><span class='from_user'>" + v.sender + "</span></div>" +
                                                "<div class='col-md-4' style='text-align: right'><i class='fa fa-long-arrow-alt-down" + last_call_class + "'></i><i class='fa fa-phone" + (v.waited > 0 && v.duration == "N/A" ? "-slash" : "") + last_call_class + "'></i></div>" +
                                            "</div>" +
                                            "<div class='row'>" +
                                                "<div class='col-md-5' title='通話長度'><i class='fa fa-stopwatch blue'></i> " + v.duration + "</div>" +
                                                "<div class='col-md-7'><span class='pull-right'>" + v.create_at + "</span></div>" +
                                            "</div>" +
                                        "</div>" +
                                        "<div class='col-md-4'>" +
                                            "<div style='height: 35px; text-align: center;'>" +
                                                "<div class='twilio_status " + current + "' id='twilio_status_" + v.ticket_id + "'>" + (icon != "" ? icon : "") + "</div>" +
                                            "</div>" +
                                        "</div>" +
                                    "</div>" +
                                "</div>";
                    $("#twilio_nav_list").append(html);
                })
            }
            init_daterangepicker();
            if (result.start_date != "") {
                $("#twilio_date").data('daterangepicker').setStartDate(result.start_date);
            }
            if (result.end_date != "") {
                $("#twilio_date").data('daterangepicker').setEndDate(result.end_date);
            }
            if (result.prev_page != -1) {
                $("#twilio_prev").on("click", function(){loadTwilio(result.prev_page)});
                $("#twilio_prev").removeClass("disabled");
            }
            if (result.next_page != -1) {
                $("#twilio_next").on("click", function(){loadTwilio(result.next_page)});
                $("#twilio_next").removeClass("disabled");
            }
            $("#twilio_search_missed").click(function(){
                $("input[name=twilio_missed]").val(1);
                loadTwilio();
            });
            $("#twilio_nav_list").height(($("#twilio_nav").height() - 80))
        }
    })
}

function displayTwilio(ticket_id, page) {
    var data = {
        act: 'twilio',
        step: 1,
        ticket_id: ticket_id
    }
    if (typeof page !== "undefined") {
        data['page'] = page;
    }
    if (ticket_id !== null && typeof ticket_id !== 'undefined') {
    $.ajax({
        url: 'crm.php',
        dataType: 'json',
        data: data,
        success: function(res){
            var result = res.content;
            var list = result.list ? result.list : {};

            if (result.user_name != "") {
                $('#info_search_form input[name=user_name]').val(result.user_name);
                loadInfo();
            }
            if (typeof page === "undefined") {
                $("#twilio_display").html("");
            }
            $(".twilio_message_load").remove();
            if (result.next_page != -1) {
                $load_more = $("<div class='row twilio_message_load' onclick='displayTwilio(" + ticket_id + ", " + result.next_page + ")'><div class='col-md-12' style='text-align: center; margin-top: 10px'><span class='label label-default' style='padding: 5px 10px; cursor: pointer;'>載入更多</span></div></div>");
                $("#twilio_display").prepend($load_more)
            }
            Object.keys(list).sort().map(function(date){
                if ($(".twilio_date_divide[data-date=" + date + "]").length == 0) {
                    $div = $("<div class='row twilio_date_divide' data-date='" + date + "'><div class='col-md-12' style='text-align: center'><span class='label label-info' style='line-height: 2;'>" + (today == date ? '今日' : yesterday == date ? '昨日' : date) + "</span></div></div>");
                    var sorted_dates = $(".twilio_date_divide").get().map((v)=>$(v).data('date')).sort();
                     if (sorted_dates.length == 0 || date > sorted_dates[sorted_dates.length - 1]) {
                        $("#twilio_display").append($div);
                    } else if (date < sorted_dates[0]) {
                        $("#twilio_display").prepend($div);
                    } else {
                        sorted_dates.push(date);
                        $(".twilio_date_divide[data-date=" + sorted_dates[sorted_dates.sort().indexOf(date) + 1] + "]").before($div)
                    }
                } else {
                    $div = $(".twilio_date_divide[data-date=" + date + "]")
                }
                list[date].sort((a,b)=>a.started_at < b.started_at).map(function(v){
                    var html = "";
                    var color = " " + (v.waited > 0 ? (v.answer_time == "N/A" ? "red" : "green") : "blue");
                    var outbound = "fa fa-long-arrow-alt-down";
                    var missed = "fa fa-phone" + (v.waited > 0 && v.answer_time == "N/A" ? "-slash" : "");
                    var extra_data = [];
                    if (v.levels !== null) {
                        extra_data.push(v.levels.split("_").join(" <i class='fa fa-angle-right'></i> "))
                    }
                    if (v.waited > 0 && typeof v.user_name !== "undefined" && v.user_name) {
                        extra_data.push("<span style='float: right'>" + v.user_name + "</span>")
                    }
                    html += "<div class='row twilio_record'>" +
                                "<div class='col-md-2'></div>" +
                                "<div class='col-md-8'>" +
                                    "<div class='row twilio_record_detail'>" +
                                        "<div class='col-md-5'><i class='fa fa-user'></i> " + v.sender + "</div>" +
                                        "<div class='col-md-3'><i class='fa fa-alarm'></i> " + v.answer_time + "</div>" +
                                        "<div class='col-md-4'><i class='" + outbound + color + "'></i><i class='" + missed + color + "'></i> " + v.create_date + "</div>" +
                                        (extra_data.length > 0 ? "<div class='twilio_extra_info col-md-12'>" + extra_data.join("") + "</div>" : "") +
                                    "</div>" +
                                "</div>" +
                            "</div>";
                    $div.after(html);
                })
            })
        }
    })
    updateTicketStatus('twilio_ticket_status', ticket_id, function(){
        $("#twilio_display").height($("#twilio_nav").height() - $("#twilio_ticket_info").height());
    })
    }
}

$(document).on("click", ".twilio_nav_item:not(.active)", function(e){
    $elem = $(e.target).hasClass("twilio_nav_item") ? $(e.target) : $(e.target).parents(".twilio_nav_item");
    $(".twilio_nav_item").removeClass("active")
    $elem.addClass("active")
    var ticket_id = $elem.data("tid");
    displayTwilio(ticket_id);
})

$(document).on("click", ".twilio_handle_phone", function(e) {
    var tid = $(e.target).data('tid');
    var phone = $(e.target).text();
    $("#twilio_tab .mail_list").click();
    displayTwilio(tid);
    if (phone.indexOf("Anonymous") == -1) {
        $('#info_search_form input[name=user_name]').val(phone);
        loadInfo();
    }
})

// Twilio client
$(document).on('click', '#twilio_call_label', function() {
    if ($("#twilio_call_init").length > 0) {
        alert("請先啟動Twilio")
    } else if ($("#twilio_call_connect").length > 0) {
        $("#twilio_call_connect").trigger("click");
    }
})

$(document).on('click', "#twilio_call_init", function() {
    $("#twilio_call_init").prop('id', 'twilio_call_await');
    $("#twilio_call_await").prop("title", "啟動中");
    $.getJSON('/ajax/agent_voice_response.php?admin_name=' + admin_name + '&step=1')
    .then(function (data) {
        device = new Twilio.Device(data.token);
        device.on('ready',function (device) {
            $("#twilio_call_await").prop('id', 'twilio_call_connect');
            $("#twilio_call_connect").prop("title", "連線");
        });
        device.on('error', function (error) {
            $(".twilio_call_button").prop('id', 'twilio_call_init');
            $("#twilio_call_init").prop("title", "未啟動");
            alert('Twilio.Device Error: ' + error.message);
        });
        device.on('connect', function (conn) {
            $("#twilio_call_connect").prop('id', 'twilio_call_disconnect');
            $("#twilio_call_disconnect").prop("title", "結束通話");
        });

        device.on('disconnect', function (conn) {
            $("#twilio_call_disconnect").prop('id', 'twilio_call_connect');
            $("#twilio_call_connect").prop("title", "連線");
        });
        $(document).on("click", "#twilio_call_connect", function(e) {
            if (device) {
                $("#twilio_call_connect").removeClass("incoming");
                device.connect();
            }
        })
        $(document).on("click", "#twilio_call_disconnect", function(e) {
            if (device) {
                device.disconnectAll();
            }
        })
    }).fail(function () {
        alert('無法使用Twilio Client');
    });
})