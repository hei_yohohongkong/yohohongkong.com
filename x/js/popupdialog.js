
(function ($, _win){

function PopupDialog(opts) {
	var _this = this;
	
	this.$popup = null;
	this.$backdrop = null;
	this.popupCss = {
		'position': 'fixed',
		'top': '50%',
		'left': '50%',
		'margin-top': '-150px',
		'margin-left': '-200px',
		'width': '400px',
		'height': '300px',
		'z-index': 12345678,
		'background-color': 'white',
		'border': 'solid 1px black',
		'display': 'none'
	};
	this.backdropCss = {
		'position': 'fixed',
		'top': 0,
		'left': 0,
		'width': '100%',
		'height': '100%',
		'z-index': 12345677,
		'background-color': 'rgba(0,0,0,0.1)',
		'display': 'none'
	};
	this.closeBtnCss = {
		'position': 'absolute',
		'top': '-20px',
		'right': '-1px',
		'width': '22px',
		'height': '19px',
		'border': 'solid 1px black',
		'border-bottom': 'none',
		'background-color': 'white',
		'font-size': '18px',
		'text-align': 'center',
		'cursor': 'pointer'
	};
	
	if ($.type(opts) === 'object')
	{
		$.each(['popupCss', 'backdropCss', 'closeBtnCss'], function (i, key) {
			if (opts.hasOwnProperty(key) && $.type(opts[key]) === 'object')
			{
				$.extend(_this[key], opts[key]);
				if (key == 'popupCss')
				{
					$.each([['width', 'margin-left'], ['height', 'margin-top']], function (i, arr) {
						var sizeProp = arr[0];
						var marginProp = arr[1];
						
						if (opts[key].hasOwnProperty(sizeProp) && !opts[key].hasOwnProperty(marginProp))
						{
							var str = opts[key][sizeProp];
							if ($.type(str) !== 'string')
							{
								str = String(str);
							}
							var matches = str.match(/^(-?[0-9.]+)([a-z]*)$/i);
							var value = matches[1];
							var unit = matches[2];
							
							_this.popupCss[marginProp] = '-' + (Number(value) / 2) + unit;
						}
					});
				}
			}
		});
	}
}

PopupDialog.prototype.show = function () {
	var _this = this;
	
	// Already shown?
	if ((this.$popup !== null) || (this.$backdrop !== null))
	{
		this.$popup.remove();
		this.$backdrop.remove();
	}
	// Setup popup div
	this.$popup = $('<div></div>');
	this.$popup.css(this.popupCss);
	
	// Setup backdrop to block clicks on other elements
	this.$backdrop = $('<div></div>');
	this.$backdrop.css(this.backdropCss);
	
	// Display the popup and backdrop with fade in animation
	this.$popup.appendTo('body').fadeIn('fast');
	this.$backdrop.appendTo('body').fadeIn('fast');
	
	// Add button for closing the popup
	this.$closeBtn = $('<div>&times;</div>');
	this.$closeBtn.css(this.closeBtnCss);
	this.$popup.append(this.$closeBtn);
	this.$closeBtn.click(function () {
		_this.hide();
	});
};

PopupDialog.prototype.hide = function () {
	var _this = this;
	// Early return if not shown
	if ((this.$popup === null) || (this.$backdrop === null))
	{
		return;
	}
	this.$popup.stop().fadeOut('fast', function () {
		_this.$popup.remove();
		_this.$popup = null;
	});
	this.$backdrop.stop().fadeOut('fast', function () {
		_this.$backdrop.remove();
		_this.$backdrop = null;
	});
};

// Export PopupDialog
_win.PopupDialog = PopupDialog;

})(jQuery,window);