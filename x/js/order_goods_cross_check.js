onload = function()
{
    $('.nav-sm').removeClass('nav-sm').addClass('nav-md');
    $("#order_sn").focus();
}

var start_corss_check = 0;
var order_goods = {};
var stocktake_order_goods = {};
var goods_all_pass_saved = 0;
var goods_all_pass = 0;
var order_sn = '';
var tracking_sn = '';
var delivery_id = '';
var goods_not_found = 0;
var goods_data = [];
var goods_list_html = '';
var auth_code_1 = '0313';  //angie
var operator = '';
var role_id = '';
var add_history_log = [];
var delivery_orders_list_html = '';

yohoTable.deferLoading = "0";
yohoTable.dataSet = null;
yohoTable.table_id = 'goods_sn_table';
yohoTable.url = 'order_goods_cross_check.php';

yohoTable.rowCallback = function(row, data, dataIndex) {
    // inital order goods
    order_goods[data.goods_sn] = data.send_number;
    goods_data.push(data);
    goods_list_html += data.goods_list_html_for_special;


};

yohoTable.drawCallback = function(setting,api) {
    // inital order goods
    $data = api.rows( {page:'current'} ).data();

    $('#goods_list_special').html(goods_list_html);
    
};

var $table = yoho_dataTable();
$("#goods_sn_table").DataTable().on('xhr.dt',function (e, settings, json, xhr) {
    if (json.extraData != null) {
        delivery_id = json.extraData.delivery_id;
        
        if (json.extraData.is_fls == 1) {
            alert('請檢查訂單不是FLS單');
            clear_all_goods_data();
            order_sn_error();
            return false;
        }

        if (json.extraData.delivery_id == '') {
            if ($('#order_sn').val() != '') {
                alert("請檢查'訂單編號', '物流追蹤編號'是否正確/ 訂單是否已經出貨");
            }
            clear_all_goods_data();
            order_sn_error();
            return false;
        }

        if (json.extraData.delivery_orders.length > 1) {
            $('#select_delivery_order_block').show();
        } else {
            $('#select_delivery_order_block').hide();
        }

        $.each(json.extraData.delivery_orders,function( key, value ) {
            delivery_orders_list_html += value.delivery_order_list_html;
        });

        $('#select_delivery_order_list').html(delivery_orders_list_html);

        // check passed already
        is_passed = json.extraData.is_passed;
        if (is_passed == 1) {
            // Indicate the cross check is completed
            $(".order_goods_cross_check:visible").css('border-color', 'green');
            $(".order_goods_cross_check:visible .cross-check-message-container").html('<p class="green pull-left">本訂單經已核對 操作人: '+json.extraData['action_user']+'<button class="btn btn-sm btn-danger reset-cross-check-btn">重新核對</button></p>');
            $.each(json.data, function(key, value){
                json.data[key].stocktake_number_html = "<span class='stocktake-" + value.goods_sn+ "' data-stocktake-number='" + value.send_number + "'>" + value.send_number + "</span>";
            });
            $('.order_goods_cross_check:visible').find('#goods_sn').prop('disabled', true);
            $('.order_goods_cross_check:visible').find('#goods_input_manual').prop('disabled', true);
            $('.order_goods_cross_check:visible').find('#reset_input').prop('disabled', true);

            $(document).on('click', '.reset-cross-check-btn', function(e){
                e.preventDefault();
                $.each(json.data, function(key, value){
                    json.data[key].stocktake_number_html = "<span class='stocktake-" + value.goods_sn+ "' data-stocktake-number='0'>0</span>";
                    $('.stocktake-'+value.goods_sn).html(json.data[key].stocktake_number_html);
                });
                $(".order_goods_cross_check:visible .cross-check-message-container").html('');
                $(".order_goods_cross_check:visible").css('border-color', 'red');
                var text = '開始核對,單號['+ order_sn +']' + (tracking_sn ? ',物流追蹤編號['+ tracking_sn +']' : '') + ',物流單號['+ delivery_id + ']';
                $('.order_goods_cross_check:visible').find('#goods_sn').prop('disabled', false);
                $('.order_goods_cross_check:visible').find('#goods_input_manual').prop('disabled', false);
                $('.order_goods_cross_check:visible').find('#reset_input').prop('disabled', false);
                add_history(text,10);
                $("#goods_sn").val('');
                $("#goods_sn").focus(); 
            });

            // if (confirm("這訂單之前已經核對過,進行再一次核對嗎?")) {
            //     var text = '開始核對,單號['+ order_sn +'],物流追蹤編號['+ tracking_sn +'],物流單號['+ delivery_id + ']';
            //     add_history(text,10);
            //     $("#goods_sn").val('');
            //     $("#goods_sn").focus(); 
            // } else {
            //     // location.reload(true);
            //     // window.location.href = "order_goods_cross_check.php?act=list";
            //     $.each(json.data, function(key, value){
            //         json.data[key].stocktake_number_html = "<span class='stocktake-" + value.goods_sn+ "' data-stocktake-number='" + value.send_number + "'>" + value.send_number + "</span>";
            //     });
            //     $('.order_goods_cross_check').find('#goods_sn').prop('disabled', true);
            //     $('.order_goods_cross_check').find('#goods_input_manual').prop('disabled', true);
            //     $('.order_goods_cross_check').find('#reset_input').prop('disabled', true);
            // }
        } else {
            $(".order_goods_cross_check:visible").css('border-color', 'red');
            $(".order_goods_cross_check:visible .cross-check-message-container").html('<p class="red pull-left">請核對本訂單 : </p>');
            var text = '開始核對,單號['+ order_sn +']' + (tracking_sn ? ',物流追蹤編號['+ tracking_sn +']' : '') + ',物流單號['+ delivery_id + ']';
            add_history(text,10);
            $("#goods_sn").val('');
            $("#goods_sn").focus();
        }
    }
});

$(document).on('click','#select_delivery_order',function(){
    $('#popup_select_delivery_order_modal').modal('toggle');
});


$(document).on('click','#submit_delivery_order',function(){
    var selected_delivery_order =$('input[name=selected_delivery_order]:checked').val();
    if (typeof selected_delivery_order == 'undefined') {
        alert('請選擇物流單號');
        return false;
    }

    start_corss_check = 1;
    clear_all_goods_data();
    load_delivery_order_goods(order_sn,selected_delivery_order);
    $("#order_sn_title").html('(<a target="_blank" href="order.php?act=info&order_sn='+ order_sn +'">'+order_sn+')');
    $('#popup_select_delivery_order_modal').modal('toggle');
});

$(document).on('click','#reset_input',function(){
    // refrash page
    // window.location.href = "order_goods_cross_check.php?act=list";
    // return false;
    location.reload(true);
});

$(document).on('click','#remove_wrong_goods_confirm',function(){
    var text = '確定,已移除不屬於訂單的產品';
    add_history(text,6);
    confirm_special_handle();
    order_goods_check_all_pass();
});

$(document).on('click','#submit_goods_qty_manual',function(){
    var order_sn = $('#order_sn').val();
    var tracking_sn = $('#tracking_sn').val();
    var goods_sn_manual = $('#goods_sn_manual').val();
    var goods_qty_manual = $('#goods_qty_manual').val();

    if (order_sn == '') {
        alert('請輸入訂單號');
        return false;
    }

    if (tracking_sn == '') {
        alert('請輸入物流追蹤編號');
        return false;
    }

    if (goods_sn_manual == '') {
        alert('請輸入產品貨號');
        return false;
    }

    if (goods_qty_manual == '') {
        alert('請輸入產品數量');
        return false;
    }

    var text = '手動輸入 貨號['+goods_sn_manual+'],數量['+goods_qty_manual + ']';
    add_history(text,3);

    order_goods_cross_check(goods_sn_manual,goods_qty_manual,false);

    $('#popup_goods_qty_manual_modal').modal('toggle');
    $("#goods_sn").val('').focus();
});

function confirm_special_handle() {
    goods_not_found = 0;
    //order_goods_check_all_pass();
    $("#remove_wrong_goods_confirm").hide();
    $("#special_handle").hide();
}

$(document).on('click','#submit_special_handle',function(){
    var goods_sn_special =$('input[name=goods_sn_special]:checked').val();
    var goods_qty_special = $('#goods_qty_special').val();
    var auth_code_special = $('#auth_code_special').val();

    if (goods_sn_special == '') {
        alert('請選擇產品');
        return false;
    }

    if (goods_qty_special == '') {
        alert('請輸入產品數量');
        return false;
    }

     if (role_id != 13 || role_id != 14) {
        if (auth_code_special == '') {
        alert('請輸入授權碼');
        return false;
        }

        if (auth_code_1 != auth_code_special && auth_code_2 != auth_code_special) {
        alert('授權碼不正確');
        return false;
        }
    }

    var auth_operator = '';
    if (auth_code_special == auth_code_1) {
        auth_operator = 'Angie';
    } else if (auth_code_special == auth_code_2) {
        auth_operator = 'Nelson';
    } 

    if (auth_operator == '') {
        auth_operator = 'N/A';
    }

    var text = '特別處理,貨號['+ goods_sn +'] -> ['+ goods_sn_special +'],數量['+goods_qty_special + '], 授權['+ auth_operator +']';
    add_history(text,7);

    goods_not_found = 0;
    order_goods_cross_check(goods_sn_special, goods_qty_special,false);
    confirm_special_handle();
    $('#popup_cross_check_special_handle_modal').modal('toggle');
    $("#goods_sn").val('').focus();
});

$("#new_serial_number").keypress(function(e) {
    if(e.which == 13) {
        event.preventDefault();
        // check stocktake goods
        var new_serial_number = '';
        new_serial_number = $("#new_serial_number").val();
        $("#new_serial_number").val('');

        if ($("#order_sn").val() == '') {
            alert('請輸入訂單號');
            return false;
        }

        if (new_serial_number == '') {
            alert('請輸入產品序號');
            return false;
        }

        // check if order sn
        $.ajax({
            url: 'order_goods_cross_check.php',
            type: 'post',
            cache: false,
            data: {
            'act': 'is_order_sn',
            'order_sn': new_serial_number,
        },
        dataType: 'json',
        success: function(result) {
            if (result.error == 0) {
                // if all pass then go to next order
                if (result.content == true) {
                    $("#order_sn").val(new_serial_number).focus();
                    clear_all_goods_data();
                    var e = $.Event("keypress");
                    e.keyCode = 13;
                    e.which = 13;
                    $("#order_sn").trigger(e);
                } else {
                    // save serial number for order
                    $.ajax({
                        url: 'order_goods_cross_check.php',
                        type: 'post',
                        cache: false,
                        data: {
                        'act': 'post_add_order_goods_serial_numbers',
                        'order_sn': order_sn,
                        'new_serial_number' : new_serial_number
                    },
                    dataType: 'json',
                    success: function(result) {
                        if (result.error == 0) {
                            var text = new_serial_number+'儲存序號成功';
                            $('#new_serial_number_success_text').html(text).show();
                            add_history(text,4,true);
                            saveCrossCheckSerialNumberLog();
                        }
                    },
                    error: function(xhr) {
                       alert('伺服器繁忙，請稍後再試。(' + xhr.status + ')');
                       console.log(xhr);
                    }
                    });
                }
            }
        },
        error: function(xhr) {
           alert('伺服器繁忙，請稍後再試。(' + xhr.status + ')');
           console.log(xhr);
        }
        });
    }
});

$("#special_handle").on('click',function(){
    $('#popup_cross_check_special_handle_modal').modal('toggle');
    
    $('#auth_code_special').val('');
    $('#goods_qty_special').val(1);
});

$("#goods_input_manual").on('click',function(){
    $('#popup_goods_qty_manual_modal').modal('toggle');
    
    setTimeout( goods_sn_manual_focus , 500);
    $('#goods_sn_manual').val('').focus();
    $('#goods_qty_manual').val('')
});

function goods_sn_manual_focus() {
     $('#goods_sn_manual').focus();
}

$("#goods_sn_manual").keypress(function(e) {
    if(e.which == 13) {
        event.preventDefault();
        $("#goods_qty_manual").focus();
    }
});

$("#goods_sn").keypress(function(e) {
    if(e.which == 13) {
        event.preventDefault();
        // check stocktake goods
        goods_sn = $("#goods_sn").val();

        if (goods_sn == '') {
            alert('請輸入貨號');
            return false;
        }

        $("#goods_sn").val('').focus();

        $('#goods_sn_special').html(goods_sn);

        if (goods_all_pass == 1 || goods_all_pass_saved == 1) {
            // check if order sn
            $.ajax({
                url: 'order_goods_cross_check.php',
                type: 'post',
                cache: false,
                data: {
                'act': 'is_order_sn',
                'order_sn': goods_sn,
            },
            dataType: 'json',
            success: function(result) {
                if (result.error == 0) {
                    // if all pass then go to next order
                    if (result.content == true) {

                        // save history log
                        var historyLog = getHistoryLog();
                        if (historyLog != '') {
                            saveCrossCheckLog(historyLog);
                        }
                        
                        if (goods_all_pass == 0) {
                            alert('訂單還未全部正確,請先處理好問題,然後再處理下一張訂單');
                            return false;
                        }

                        $("#order_sn").val(goods_sn).focus();
                        clear_all_goods_data();
                        var e = $.Event("keypress");
                        e.keyCode = 13;
                        e.which = 13;
                        $("#order_sn").trigger(e);
                    } else {
                        order_goods_cross_check(goods_sn);
                    }
                }
            },
            error: function(xhr) {
               alert('伺服器繁忙，請稍後再試。(' + xhr.status + ')');
               console.log(xhr);
            }
            });
        } else {
            order_goods_cross_check(goods_sn);
        }
    }
});

$(document).on('click','.revised_qty_button',function(){
    goods_sn = $(this).data('goods-sn');
    var text = '修正數量,貨號['+goods_sn+'],數量['+stocktake_order_goods[goods_sn] + ' -> '+ order_goods[goods_sn] +']';
    
    stocktake_order_goods[goods_sn] = order_goods[goods_sn];
    $('.stocktake-'+goods_sn).html(order_goods[goods_sn]);
    $('.stocktake-'+goods_sn).closest('tr').css('background','green');
    $('.stocktake-'+goods_sn).closest('tr').css('color','white');
    $("#goods_sn").val('').focus();

    add_history(text,5);
    order_goods_check_all_pass();
});

$("#order_sn").keypress(function(e) {
    if(e.which == 13) {
        event.preventDefault();
        corss_check();
    }
});

$("#tracking_sn").keypress(function(e) {
    if(e.which == 13) {
        event.preventDefault();
        corss_check();
    }
});

function corss_check(){
    if ($.trim($("#order_sn").val()).length > 0 && $("#order_sn").val() != 0){
        if ($.trim($("#tracking_sn").val()).length > 0 && $("#tracking_sn").val() != 0){
            start_corss_check = 1;
            $("#goods_sn").val('');
            $("#goods_sn").focus();
            
            order_sn = $("#order_sn").val();
            tracking_sn = $("#tracking_sn").val();

            clear_all_goods_data();
            load_delivery_order_goods(order_sn, 0, tracking_sn);
            $("#order_sn_title").html('(<a target="_blank" href="order.php?act=info&order_sn='+ order_sn +'">'+order_sn+')');
        } else {
            $("#tracking_sn").focus();
        }
    } else {
        $("#order_sn").focus();
    }
}

function load_delivery_order_goods(order_sn, delivery_id = 0, tracking_sn = 0)
{
    var $filter = {};
    $filter.order_sn = order_sn;
    $filter.delivery_id = delivery_id;
    $filter.invoice_no = tracking_sn;
    $table.search($filter);
}

function order_goods_check_all_pass() {
    
    $(".order_goods_cross_check:visible").find('.cross-check-message-container').html('');
    goods_all_pass = 1;
    $.each( order_goods, function( key, value ) {
        if (stocktake_order_goods[key] != value) {
            goods_all_pass = 0;
            return false;
        }
    });

    if (goods_all_pass == 1 && goods_not_found == 1) {
        alert_with_sound('訂單還未正確,請把不屬於訂單的產品移除');
        goods_all_pass = 0;
        return false;
    }

    if (goods_not_found == 1) {
        goods_all_pass = 0;
        return false;
    }

    if (goods_all_pass == 1) {
        goods_all_pass_saved = 1;
        // update data base all pass 1
        $("#order_all_pass").html('全部正確');
        $(".order_goods_cross_check:visible").css('border-color', 'green');
        $(".order_goods_cross_check:visible .cross-check-message-container").html('<p class="green pull-left">本訂單經已核對 <button class="btn btn-sm btn-danger reset-cross-check-btn">重新核對</button></p>');
        var text = '全部正確';
        add_history(text,8);
        var historyLog = getHistoryLog();
        saveCrossCheckLog(historyLog);
        savePickerPacker();
    } else {
        if (goods_all_pass_saved == 1) {
            // update data base all pass 0
        }
    }

    $("#goods_sn").val('').focus();
}

function savePickerPacker() {
    $.ajax({
        url: 'order_goods_cross_check.php',
        type: 'post',
        cache: false,
        data: {
        'act': 'save_picker_packer',
        'order_sn': order_sn,
        'operator': operator,
    },
    dataType: 'json',
    success: function(result) {
        if (result.error == 0) {
            
        }
    },
    error: function(xhr) {
        alert('伺服器繁忙，請稍後再試。(' + xhr.status + ')');
        console.log(xhr);
    }
    });
}

function clear_all_goods_data() {
    stocktake_order_goods = {};
    order_goods = {};
    goods_all_pass_saved = 0;
    goods_all_pass = 0;
    goods_list_html = '';
    add_history_log = [];
    delivery_orders_list_html = '';
    delivery_id = 0;

    $("#select_delivery_order_block").hide();
    $('#order_all_pass').html('');
    $('.cross_check_error_history').html('');
    $('#new_serial_number_success_text').html('').hide();
    $("#new_serial_number").val('');
    $("#order_sn_title").html('');
    $("#remove_wrong_goods_confirm").hide();
    $("#special_handle").hide();
}

function alert_with_sound(error_text) {
    alert(error_text);
    playAudioAlert();
}

function alert_error(error_text) { 
    
    add_history(error_text,2);
    saveCrossCheckErrorLog();
    alert(error_text);
    $("#order_all_pass").html('');
    goods_all_pass = 0;

    playAudioAlert();
    // log data base
    var historyLog = getHistoryLog();
    saveCrossCheckLog(historyLog);
}

function add_history(text,type,save_log = false) {
    if (type == 1) { // success
        text = '<div class="success">' + text + '</div>'
    } else if (type == 2) { // error
        text = '<div class="error">' + text + '</div>'
    } else if (type == 3) { // menual_input
        text = '<div class="menual_input">' + text + '</div>'
    } else if (type == 4) { // new_sn_input
        text = '<div class="new_sn_input">' + text + '</div>'
    } else if (type == 5) { // fix_goods_qty
        text = '<div class="fix_goods_qty">' + text + '</div>'
    } else if (type == 6) { // remove_wrong_goods
        text = '<div class="remove_wrong_goods">' + text + '</div>'
    } else if (type == 7) { // special_handle
        text = '<div class="special_handle">' + text + '</div>'
    } else if (type == 8) { // all correct
        text = '<div class="all_correct">' + text + '</div>'
    } else if (type == 9) { // goods_sn_input
        text = '<div class="goods_sn_input">' + text + '</div>'
    } else if (type == 10) { // order_sn_input
        text = '<div class="order_sn_input">' + text + '</div>'
    } else {
        text = '<div >' + text + '</div>'
    }

    $( ".cross_check_error_history" ).prepend(text);
    if (type != 4) {
        add_history_log.push(text);
    }

    // log data base
    if (save_log == true) {
        saveCrossCheckLog(text);
    }
}

function saveCrossCheckSerialNumberLog() {
    $.ajax({
        url: 'order_goods_cross_check.php',
        type: 'post',
        cache: false,
        data: {
        'act': 'save_cross_check_serial_number',
        'order_sn': order_sn,
        'delivery_id': delivery_id,
        'serial_number_scanned': 1,
    },
    dataType: 'json',
    success: function(result) {
        if (result.error == 0) {
            // if all pass then go to next order
            if (result.content == true) {
                
            }
        }
    },
    error: function(xhr) {
       alert('伺服器繁忙，請稍後再試。(' + xhr.status + ')');
       console.log(xhr);
    }
    });
}

function saveCrossCheckErrorLog() {
    $.ajax({
        url: 'order_goods_cross_check.php',
        type: 'post',
        cache: false,
        data: {
        'act': 'save_cross_check_error',
        'order_sn': order_sn,
        'delivery_id': delivery_id,
        'error': 1,
    },
    dataType: 'json',
    success: function(result) {
        if (result.error == 0) {
            // if all pass then go to next order
            if (result.content == true) {
                
            }
        }
    },
    error: function(xhr) {
       alert('伺服器繁忙，請稍後再試。(' + xhr.status + ')');
       console.log(xhr);
    }
    });
}

function getHistoryLog() {
    var text = '';
    $.each( add_history_log, function( key, value ) {
        text = text + value;
    });
    
    // clear log 
    add_history_log = [];

    return text;
}

function saveCrossCheckLog(text) {
    $.ajax({
        url: 'order_goods_cross_check.php',
        type: 'post',
        cache: false,
        data: {
        'act': 'save_cross_check_error_history',
        'order_sn': order_sn,
        'delivery_id': delivery_id,
        'passed': goods_all_pass,
        'operator': operator,
        'log' : text,
    },
    dataType: 'json',
    success: function(result) {
        if (result.error == 0) {
            // if all pass then go to next order
            if (result.content == true) {
                
            }
        }
    },
    error: function(xhr) {
       alert('伺服器繁忙，請稍後再試。(' + xhr.status + ')');
       console.log(xhr);
    }
    });
}

function order_goods_cross_check(goods_sn, qty=1, add_goods_input_history=true) {
    // 1. Check if it belongs to this order.
    if (typeof order_goods[goods_sn] == 'undefined') {
        var error_text = '錯誤,找不到產品('+ goods_sn +')';

        // add goods button 
        $("#remove_wrong_goods_confirm").show();
        $("#special_handle").show();
        goods_not_found = 1;
        alert_error(error_text);

        if (goods_not_found == 1) {
            order_goods_check_all_pass();
        }

        // log data base
        return false;
    }

    if (add_goods_input_history == true) {
        var text = '輸入產品貨號,貨號['+goods_sn+']';
        add_history(text,9);
    }
    
    // 2. check if the stock over
    if (typeof stocktake_order_goods[goods_sn] == 'undefined') {
        stocktake_order_goods[goods_sn] = 0;
    }

    if (parseInt(stocktake_order_goods[goods_sn]) + parseInt(qty) <= parseInt(order_goods[goods_sn])) {
        stocktake_order_goods[goods_sn] = parseInt(stocktake_order_goods[goods_sn]) + parseInt(qty) ;
        $('.stocktake-'+goods_sn).html(stocktake_order_goods[goods_sn]);

        if (stocktake_order_goods[goods_sn] == order_goods[goods_sn]) {
            $('.stocktake-'+goods_sn).closest('tr').css('background','green');
            $('.stocktake-'+goods_sn).closest('tr').css('color','white');
        }
    } else {
        stocktake_order_goods[goods_sn] = parseInt(stocktake_order_goods[goods_sn]) + parseInt(qty) ;
        var revised_button = '<input class="btn btn-success form-btn revised_qty_button" data-goods-sn="'+ goods_sn +'" type="button" id="revised_qty_button" name="revised_qty_button" value="數量已修正">';
        $('.stocktake-'+goods_sn).html(stocktake_order_goods[goods_sn] + ' ' +revised_button);
        $('.stocktake-'+goods_sn).closest('tr').css('background','red');
        $('.stocktake-'+goods_sn).closest('tr').css('color','white');
        
        var error_text = '錯誤,產品數量已經足夠,請檢查產品數量是否正確'+ '('+ goods_sn +')';
        alert_error(error_text);
        // log data base
    }

    // check if all is pass
    order_goods_check_all_pass();
}

function order_sn_error() { 
    $("#order_sn").val('').focus();
    $("#tracking_sn").val('');
}

$("#top-right").hide();


var x = document.getElementById("audio_alert"); 

function playAudioAlert() { 
    x.play();
} 

function pauseAudioAlert() { 
    x.pause(); 
}

function unloadAllCrossCheckListener()
{
    $("#goods_sn_table").DataTable().off('xhr.dt');
    $(document).off('click','#select_delivery_order');
    $(document).off('click','#submit_delivery_order');
    $(document).off('click','#reset_input');
    $(document).off('click','#remove_wrong_goods_confirm');
    $(document).off('click','#submit_goods_qty_manual');
    $(document).off('click','#submit_special_handle');
    $(document).off('keypress','#new_serial_number');
    $("#special_handle").off('click');
    $("#goods_input_manual").off('click');
    $("#goods_sn_manual").off('keypress');
    $("#goods_sn").off('keypress');
    $("#order_sn").off('keypress');
    $("#tracking_sn").off('keypress');
    $(document).off('click','.revised_qty_button');
    $(document).off('click', '.reset-cross-check-btn');
}
