/***
* actg.js
*
* Accounting System javascript file
***/

var YOHO = YOHO || {};

(function ($){

YOHO.actg = {
	
	sanitizeAmountInput: function ($amountInput) {
		$amountInput.change(function () {
			var amt = $(this).val();
			amt = amt.replace(/[^0-9.-]/g, '');
			if (amt.indexOf('-') != -1)
			{
				amt = amt.substr(0, 1) + amt.substr(1).replace(/-/g, '');
			}
			if (amt.indexOf('.') != -1)
			{
				amt = amt.substr(0, amt.indexOf('.')+1) + amt.substr(amt.indexOf('.')+1).replace(/\./g, '');
			}
			if (amt.length > 0)
			{
				try
				{
					amt = Number(Number(amt).toFixed(4));
				}
				catch (e)
				{
					amt = '0';
				}
			}
			$(this).val(amt);
		});
	},
	
	loaded: true
};

})(jQuery);