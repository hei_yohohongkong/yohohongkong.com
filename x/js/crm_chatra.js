var curr_page = 0;
function chatraMessageUpdate(ticket_id, page, update) {
    var data = {
        act: 'chatra',
        step: 1,
        ticket_id: ticket_id
    }
    if (typeof page !== "undefined") {
        data['page'] = page;
    }
    var YOHO_CHATRA_ALL_MSG = localStorage.getItem("YOHO_CHATRA_ALL_MSG");
    if (YOHO_CHATRA_ALL_MSG != null) {
        var saved = JSON.parse(YOHO_CHATRA_ALL_MSG);
        if (saved.hasOwnProperty(ticket_id) && saved[ticket_id].update) {
            data['update'] = saved[ticket_id].update;
        }
    }
    $.ajax({
        url: 'crm.php',
        dataType: "json",
        data: data,
        success: function (res) {
            if (res.error == 1) {
                alert(res.message);
            } else {
                var result = res.content;
                updateChatraAllMsgStorage(ticket_id, result.messages, result.page, result.order_sn, result.user_name);
                drawChatraMessages (ticket_id);

                if (typeof page === "undefined" || typeof update === "undefined") {
                    rollToChatBottom("#chatra_display .panel-body");
                }
                if (typeof page === "undefined") {
                    if (result.order_sn != "") {
                        $('#info_search_form input[name=order_sn]').val(result.order_sn);
                        loadInfo();
                    } else if (result.user_name != "") {
                        $('#info_search_form input[name=user_name]').val(result.user_name);
                        loadInfo();
                    }
                }
                $('#chatra_display a').prop('target', '_blank');
                $('#email_display img').get().map(function(v){
                    var width = $(v).width(),
                        height = $(v).height();
                    if (width / $(v).parent().width() > height / $(v).parent().height()) {
                        $(v).css('max-height', '100%');
                    } else {
                        $(v).css('max-width', '100%');
                    }
                })
            }
        }
    })
}

function listenChatraMsg (ticket_id) {
    var YOHO_CHATRA_ALL_MSG = JSON.parse(localStorage.getItem("YOHO_CHATRA_ALL_MSG"));
    $.ajax({
        url: 'crm.php',
        dataType: "json",
        data: {
            act: 'chatra',
            step: 4,
            ticket_id: ticket_id,
            update: YOHO_CHATRA_ALL_MSG[ticket_id].update
        },
        success: function (res) {
            if (res.error == 1) {
                alert(res.message);
            } else {
                if (res.content.update != 0) {
                    chatraMessageUpdate(ticket_id, undefined, true);
                } else {
                    var YOHO_CHATRA_ALL_MSG = localStorage.getItem("YOHO_CHATRA_ALL_MSG");
                    if (YOHO_CHATRA_ALL_MSG !== null) {
                        var saved = JSON.parse(YOHO_CHATRA_ALL_MSG);
                        if (saved.hasOwnProperty(ticket_id)) {
                            if (res.content.user_name != saved[ticket_id].user_name) {
                                saved[ticket_id].user_name = res.content.user_name;
                                if (res.content.order_sn == saved[ticket_id].order_sn) {
                                    $('#info_search_form input[name=user_name]').val(res.content.user_name);
                                    loadInfo();
                                }
                            }
                            if (res.content.order_sn != saved[ticket_id].order_sn) {
                                saved[ticket_id].order_sn = res.content.order_sn;
                                $('#info_search_form input[name=order_sn]').val(res.content.order_sn);
                                loadInfo();
                            }
                        }
                        localStorage.setItem("YOHO_CHATRA_ALL_MSG", JSON.stringify(saved));
                    }
                    var YOHO_CHATRA_LAST_MSG = localStorage.getItem("YOHO_CHATRA_LAST_MSG");
                    if (YOHO_CHATRA_LAST_MSG !== null) {
                        var saved = JSON.parse(YOHO_CHATRA_LAST_MSG);
                        for (var key in saved.conversations) {
                            if (saved.conversations[key].ticket_id == ticket_id) {
                                saved.conversations[key].new = 0;
                            }
                        }
                        localStorage.setItem("YOHO_CHATRA_LAST_MSG", JSON.stringify(saved));
                    }
                }
            }
        }
    })
}

function drawChatraMessages (ticket_id) {
    var YOHO_CHATRA_ALL_MSG = JSON.parse(localStorage.getItem("YOHO_CHATRA_ALL_MSG"));
    if (!!window.EventSource) {
        timer.chatra_update_this = {
            ticket_id: ticket_id,
        }
    } else {
    if (timer.chatra_update_this.ticket_id != ticket_id) {
        if (timer.chatra_update_this.timer_id != 0) {
            clearInterval(timer.chatra_update_this.timer_id);
        }
        timer.chatra_update_this = {
            ticket_id: ticket_id,
            timer_id: setInterval(function(){
                listenChatraMsg (ticket_id);
            }, 5000)
        }
    }
    }
    updateTicketStatus('chatra_ticket_status', ticket_id, function(){
        $("#chatra_display").height($("#chatra_nav").height() - $("#chatra_ticket_info").height());
        $("#chatra_display .panel-body").css("max-height", ($("#chatra_display").height() - 120) + 'px');
    });

    var messages_by_time = {};
    YOHO_CHATRA_ALL_MSG[ticket_id].messages.map(function(v){
        messages_by_time[v.create_at_date] ? messages_by_time[v.create_at_date].push(v) : messages_by_time[v.create_at_date] = [v]
    })
    $("#chatra_display .panel-body").html("");
    if (YOHO_CHATRA_ALL_MSG[ticket_id].page != 0) {
        $('<div class="row chatra_message_load">' +
                '<div class="col-md-12" style="text-align: center">' +
                    '<span class="label label-default" style="padding: 5px 10px; cursor: pointer;" onclick="displayChatra(' + ticket_id + ', ' + YOHO_CHATRA_ALL_MSG[ticket_id].page + ')"><i class="fa fa-angle-double-up"></i> 載入更多</span>' +
                '</div>' +
            '</div>').appendTo("#chatra_display .panel-body");
    }
    Object.keys(messages_by_time).sort().map(function(date) {
        var message = {};
        $('<div class="row chatra_date_divide">' +
                '<div class="col-md-12" style="text-align: center">' +
                    '<span class="label label-info" style="line-height: 2;">' +
                        (today == date ? '今日' : yesterday == date ? '昨日' : date) +
                    '</span>' +
                '</div>' +
            '</div>').appendTo("#chatra_display .panel-body");
        messages_by_time[date].map(function(msg) {
            message[msg.create_at_time] ? message[msg.create_at_time].push(msg) : message[msg.create_at_time] = [msg];
        })
        var prev_user = "";
        var getUser = false;
        Object.keys(message).sort().map(function(time) {
            message[time].map(function(msg) {
                if (msg.msg.type != 'agent' && typeof msg.msg.clientId !== "undefined" && msg.msg.clientId != null) {
                    var YOHO_CHATRA_USERS = JSON.parse(localStorage.getItem("YOHO_CHATRA_USERS"));
                    if (!(msg.msg.clientId in YOHO_CHATRA_USERS) && !getUser) {
                        getUser = true;
                        getChatraUsers(msg.msg.clientId);
                    }
                }
                var html = '<div class="row chatra_message' + (msg.temp == 1 ? ' chatra_message_temp' : msg.msg.type == 'agent' ? ' chatra_message_agent' : '') + '">';
                html += '<div class="col-md-7 chatra_message_content' + (msg.msg.type == 'agent' ? ' pull-right' : '') + '">';
                if (msg.temp != 1 && prev_user != msg.user) {
                    prev_user = msg.user;
                    html += '<div class="row chatra_message_info">' +
                        '<div class="col-md-8 from_user">' + msg.user + '</div>' +
                    '</div>';
                }
                html += '<div class="row chatra_message_body">' +
                            '<div class="col-md-9">';
                if (msg.msg.file) {
                    var file_id = msg.msg.file.name.replace(/[^A-z\d_]/g, "_").split(".")[0];
                    html += "<a class='email_attachment a_dl chatra_attachment' href='" + ((msg.msg.file.is_image == 1 || msg.msg.file.icon == "-image") ? "javascript:displayAttachment(\"#" + file_id + "\");" : msg.msg.file.url) + "' download='" + msg.msg.file.name + "'>";
                    if (msg.msg.file.is_image == 1 || msg.msg.file.icon == "-image") {
                        html += '<img id="' + file_id + '" src="' + msg.msg.file.url + '" title="' + msg.msg.file.name + '">';
                    } else {
                        html += "<div class='a_title'>" + msg.msg.file.name + "</div>" +
                                "<div class='a_name'>" +
                                    "<div class='f_name'>" + msg.msg.file.name + "</div>" +
                                    "<div class='f_dl'>下載 <span>(" + msg.msg.file.formatted_size + ")</span></div>" +
                                "</div>" +
                                "<div class='f_ext'><i class='f_icon fa fa-file" + msg.msg.file.icon + "'></i><div></div></div>";
                    }
                    html += "</a>";
                } else {
                    html += msg.msg.text
                }
                html += "</div>" +
                    '<div class="col-md-2"><span class="pull-right">' + msg.time + '</span></div>' +
                '</div>';
                $(html).appendTo("#chatra_display .panel-body");
            })
        })
    })
}

function displayChatra(ticket_id, page) {
    $('#chatra_ticket_info, #chatra_user_info').show();
    var update = false;
    var YOHO_CHATRA_ALL_MSG = localStorage.getItem("YOHO_CHATRA_ALL_MSG");
    if (YOHO_CHATRA_ALL_MSG == null) {
        update = true;
    } else {
        var saved = JSON.parse(YOHO_CHATRA_ALL_MSG);
        if (!saved.hasOwnProperty(ticket_id)) {
            update = true;
        } else {
            if (typeof page !== 'undefined') {
                chatraMessageUpdate(ticket_id, page, true);
            } else {
                $(".chatra_nav_item").addClass("disabled");
                $.ajax({
                    url: 'crm.php',
                    dataType: "json",
                    data: {
                        act: 'chatra',
                        step: 4,
                        ticket_id: ticket_id,
                        update: saved[ticket_id].update
                    },
                    success: function (res) {
                        if (res.error == 1) {
                            alert(res.message);
                        } else {
                            $(".chatra_nav_item").removeClass("disabled");
                            if (res.content.update == 0) {
                                drawChatraMessages (ticket_id);
                                if (saved[ticket_id].order_sn != $("#info_search_form input[name=hidden_order_sn]").val()) {
                                    $("#info_search_form input[name=order_sn]").val(saved[ticket_id].order_sn);
                                    loadInfo();
                                } else if (saved[ticket_id].user_name != $("#info_search_form input[name=hidden_user_name]").val() && saved[ticket_id].order_sn ==""){
                                    $("#info_search_form input[name=user_name]").val(saved[ticket_id].user_name);
                                    loadInfo();
                                }
                                rollToChatBottom("#chatra_display .panel-body");
                            } else {
                                chatraMessageUpdate(ticket_id, undefined, true);
                            }
                        }
                    }
                })
            }
        }
    }
    if (update) {
        chatraMessageUpdate(ticket_id, page, true)
    }
}

function drawChatraLastMessages() {
    var YOHO_CHATRA_LAST_MSG = JSON.parse(localStorage.getItem("YOHO_CHATRA_LAST_MSG"));
    var tickets_by_time = {};
    for (var key in YOHO_CHATRA_LAST_MSG.conversations) {
        tickets_by_time[key.split("-")[1]] ? tickets_by_time[key.split("-")[1]].push(YOHO_CHATRA_LAST_MSG.conversations[key]) : tickets_by_time[key.split("-")[1]] = [YOHO_CHATRA_LAST_MSG.conversations[key]];
    }
    var tid = $('.chatra_nav_item.active').data('tid');
    var today = Date.now() - Date.now() % 86400000 + (new Date().getTimezoneOffset() * 60000);
    var missing_user = [];
    $("#chatra_nav").html("");
    Object.keys(tickets_by_time).sort().reverse().filter((v, i) => i < 10 * (curr_page + 1)).map(function (key) {
        tickets_by_time[key].map(function (v){
            var YOHO_CHATRA_USERS = JSON.parse(localStorage.getItem("YOHO_CHATRA_USERS"));
            var user = null;
            if (v.msg.sender in YOHO_CHATRA_USERS && v.msg.type != "agent") {
                user = YOHO_CHATRA_USERS[v.msg.sender];
            } else if (v.msg.type == "agent") {
                for (var client_id in YOHO_CHATRA_USERS) {
                    if (YOHO_CHATRA_USERS[client_id].chatId == v.ref_id) {
                        user = YOHO_CHATRA_USERS[client_id];
                    }
                }
            }
            if (user == null) {
                user = {
                    color: '#1e90ff',
                    displayedName: 'Anonymous'
                };
                if (missing_user.indexOf(v.msg.sender) == -1 && v.msg.type != "agent") {
                    missing_user.push(v.msg.sender)
                }
            }
            var html = '<div class="chatra_nav_item' + (v.new == 1 ? ' chatra_nav_new' : '') + '" data-tid="' + v.ticket_id + '">' +
                            '<div class="row">' +
                                '<div class="col-md-2">' +
                                    '<div class="chatra_icon" style="background: ' + user.color + '">' +
                                        '<i class="fa fa-user"></i>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="col-md-10">' +
                                    '<div class="row">' +
                                        '<div class="col-md-8">' +
                                            '<span class="from_user" style="display: block;">' + user.displayedName + '</span>' +
                                        '</div>' +
                                        '<div class="col-md-4">' +
                                            '<span class="pull-right">' + (v.recent < today ? v.msg.create_at.substr(0, 10) : v.msg.create_at.substr(11, 5)) + '</span>' +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="row">' +
                                        '<div class="col-md-10">' +
                                            '<span class="from_mail" style="display: block;">' + v.msg.text + '</span>' +
                                        '</div>' +
                                        '<div class="col-md-2" style="text-align: center">' +
                                            (v.new == 1 ? '<i class="fa fa-exclamation red"></i>' : '') +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>';
            $("#chatra_nav_load").length == 0 ? $(html).appendTo("#chatra_nav") : $(html).before("#chatra_nav_load");
        })
    })
    if (Object.keys(YOHO_CHATRA_LAST_MSG.conversations).length > 10 || YOHO_CHATRA_LAST_MSG.next_page) {
        var html = '<div id="chatra_nav_load" data-page="' + (curr_page + 1) + '">' +
                        '<div class="row" style="margin: 0px">' +
                            '<div class="col-md-12">' +
                                '<div id="chatra_nav_load_icon">' +
                                    '<i class="fa fa-angle-double-down"></i>' +
                                '</div>載入更多' +
                            '</div>' +
                        '</div>' +
                    '</div>'
        $(html).appendTo("#chatra_nav")
    }
    if (tid != "") {
        $(".chatra_nav_item[data-tid=" + tid + "]").addClass("active");
        $(".chatra_nav_item[data-tid=" + tid + "]").find(".fa.fa-exclamation").remove();
    }
    if (missing_user.length > 0) {
        getChatraUsers(missing_user.join(","))
    }
}

function listenChatraAll() {
    loadChatra(undefined, true);
}

function loadChatra(page, update) {
    var data = {
        act: 'chatra',
    };
    if (typeof page !== "undefined") {
        data['page'] = page;
    }
    var YOHO_CHATRA_LAST_MSG = localStorage.getItem("YOHO_CHATRA_LAST_MSG");
    if (YOHO_CHATRA_LAST_MSG !== null) {
        drawChatraLastMessages();
        if (update == true) {
            data['update'] = JSON.parse(YOHO_CHATRA_LAST_MSG).update;
        }
    }
    $.ajax({
        url: 'crm.php',
        type: 'get',
        dataType: 'json',
        data: data,
        success: function(res) {
            if (res.error == 1) {
                alert(res.message);
            } else {
                var result = res.content
                var tickets = result.tickets;
                var users = result.users;
                if (users !== null) {
                    for (var key in users) {
                        updateChatraUsers(users[key]);
                    }
                }
                if (tickets !== null) {
                    updateChatraLastMsgStorage(tickets, result.next_page);
                    drawChatraLastMessages();
                }
                if (timer.chatra_update_all == 0 && !window.EventSource) {
                    timer.chatra_update_all = setInterval(listenChatraAll, 10000)
                }
            }
        }
    })
}

function updateChatraLastMsgStorage (tickets, next_page) {
    YOHO_CHATRA_LAST_MSG = localStorage.getItem("YOHO_CHATRA_LAST_MSG");
    if (YOHO_CHATRA_LAST_MSG === null) {
        YOHO_CHATRA_LAST_MSG = {
            conversations: {},
            update: Date.now(),
            next_page: next_page
        };
        for (var key in tickets) {
            if (key != "") {
                YOHO_CHATRA_LAST_MSG.conversations[key + "-" + tickets[key].recent] = tickets[key];
            }
        }
        localStorage.setItem("YOHO_CHATRA_LAST_MSG", JSON.stringify(YOHO_CHATRA_LAST_MSG));
    } else {
        var saved = JSON.parse(YOHO_CHATRA_LAST_MSG);
        var max_recent = saved.update;
        if (typeof next_page !== "undefined") {
            saved.next_page = next_page;
        }
        for (var key in tickets) {
            if (key != "") {
                var t_key = key + "-" + tickets[key].recent;
                if (!saved.conversations.hasOwnProperty(t_key)) {
                    for (var k in saved.conversations) {
                        if (k.indexOf(key) !== -1) {
                            tickets[key].new = tickets[key].new == 0 && saved.conversations[k].new == 1 ? 1 : tickets[key].new;
                            delete saved.conversations[k];
                        }
                    }
                    saved.conversations[t_key] = tickets[key];
                    max_recent = parseInt(tickets[key].recent) > max_recent ? parseInt(tickets[key].recent) : max_recent;
                }
            }
        }
        saved.update = max_recent;
        localStorage.setItem("YOHO_CHATRA_LAST_MSG", JSON.stringify(saved));
    }
}

function updateChatraAllMsgStorage (ticket_id, messages, page, order_sn, user_name) {
    var YOHO_CHATRA_ALL_MSG = localStorage.getItem("YOHO_CHATRA_ALL_MSG");
    var d = new Date();
    var has_update = false;
    var time = d.getFullYear() + "-" + ('0' + (d.getMonth() + 1)).slice(-2) + "-" + ('0' + d.getDate()).slice(-2) + " " + ('0' + d.getHours()).slice(-2) + ":" + ('0' + d.getMinutes()).slice(-2) + ":" + ('0' + d.getSeconds()).slice(-2);
    if (YOHO_CHATRA_ALL_MSG === null) {
        YOHO_CHATRA_ALL_MSG = {};
        YOHO_CHATRA_ALL_MSG[ticket_id] = {
            messages: messages,
            update: time,
            page: page,
            order_sn: order_sn,
            user_name: user_name
        };
        localStorage.setItem("YOHO_CHATRA_ALL_MSG", JSON.stringify(YOHO_CHATRA_ALL_MSG));
        has_update = true;
    } else {
        var saved = JSON.parse(YOHO_CHATRA_ALL_MSG);
        if (saved.hasOwnProperty(ticket_id)) {
            var list_ids = saved[ticket_id].messages.map((v)=>v.list_id);
            messages.map(function (v) {
                if (list_ids.indexOf(v.list_id) == -1) {
                    saved[ticket_id].messages.map(function (msg, i){
                        if (msg.msg.id == v.msg.id && msg.temp == 1) {
                            saved[ticket_id].messages.splice(i, 1);
                        }
                    })
                    saved[ticket_id].messages.push(v);
                    saved[ticket_id].update = time;
                    has_update = true;
                }
            })
        } else {
            saved[ticket_id] = {
                messages: messages,
                update: time
            }
            has_update = true;
        }
        saved[ticket_id].page = page;
        saved[ticket_id].order_sn != "" && order_sn == "" ? true : saved[ticket_id].order_sn = order_sn,
        saved[ticket_id].user_name != "" && user_name == "" ? true : saved[ticket_id].user_name = user_name
        localStorage.setItem("YOHO_CHATRA_ALL_MSG", JSON.stringify(saved));
    }

    if (has_update) {
        loadChatra(undefined, true);
    }
}

function updateChatraUsers (user) {
    var YOHO_CHATRA_USERS = localStorage.getItem("YOHO_CHATRA_USERS");
    var saved = YOHO_CHATRA_USERS === null ? {} : JSON.parse(YOHO_CHATRA_USERS);
    if (user != null && typeof user.id !== "undefined") {
        saved[user.id] = user;
    }
    localStorage.setItem("YOHO_CHATRA_USERS", JSON.stringify(saved));
}

function getChatraUsers (client_ids) {
    $.ajax({
        url: "crm.php",
        dataType: "json",
        data: {
            act: "chatra",
            step: 6,
            clientId: client_ids
        },
        success: function (res) {
            if (res.error != 1) {
                res.content.user.map((v) => updateChatraUsers(v));
            }
        }
    })
}

$(document).on('mouseover', '#email_display img, #chatra_display img', function(e) {
    if ($(e.target).parents('table').length == 0 && $(e.target).parents('a:not(.a_dl)').length == 0) {
        $div = $("<div class='hover_div'></div>");
        $div2 = $("<div>點擊" + ($(e.target).parents(".chatra_attachment").length > 0 ? "開啟" : "查看") +"</div>");
        var nW = $(e.target).prop('naturalWidth'),
            nH = $(e.target).prop('naturalHeight'),
            is_attachment = $(e.target).parents('a.email_attachment').length > 0;
        if (nW > 300) {
            nH = Math.floor(300 / nW * nH);
            nW = 300;
        }
        $div.css({
            width: (is_attachment ? 150 : nW) + 'px',
            height: (is_attachment ? 110 : nH) + 'px',
            position: 'absolute',
            top: is_attachment ? 0 : $(e.target).position().top,
            left: $(e.target).position().left,
            transition: '0.3s',
            'pointer-events': 'none',
        })
        $div2.css({
            position: 'relative',
            top: '50%',
            transform: 'translateY(-50%)',
            color: '#ddd',
            'text-align': 'center',
            'font-size': '18px',
        })
        $div.append($div2);
        $(e.target).after($div);
        if ($(e.target).parents(".chatra_attachment").length == 0) {
            $(e.target).css({
                width: nW + 'px',
                height: nH +'px',
            });
        }
        $(e.target).css({
            cursor: 'pointer',
            filter: 'brightness(50%)',
            transition: '0.3s'
        });
    }
}).on('mouseout', '#email_display img, #chatra_display img', function(e) {
    if ($(e.target).parents('table').length == 0 && $(e.target).parents('a:not(.a_dl)').length == 0) {
        $(e.target).css({
            filter: 'brightness(100%)'
        });
        $('.hover_div').remove();
    }
})

$(document).on('click', '.chatra_nav_item:not(.active)', function(e) {
    e.stopPropagation();
    $elem = $(e.target).hasClass('chatra_nav_item') ? $(e.target) : $(e.target).parents('.chatra_nav_item');
    $('.chatra_nav_item').removeClass('active');
    $elem.addClass('active');
    if ($elem.find(".fa.fa-exclamation").length > 0) {
        $elem.find(".fa.fa-exclamation").remove();
        var YOHO_CHATRA_LAST_MSG = JSON.parse(localStorage.getItem("YOHO_CHATRA_LAST_MSG"));
        for (var key in YOHO_CHATRA_LAST_MSG.conversations) {
            if (YOHO_CHATRA_LAST_MSG.conversations[key].ticket_id == $elem.data('tid')) {
                YOHO_CHATRA_LAST_MSG.conversations[key].new = 0
            }
        }
        localStorage.setItem("YOHO_CHATRA_LAST_MSG", JSON.stringify(YOHO_CHATRA_LAST_MSG));
        drawChatraLastMessages();
    }
    displayChatra($elem.data('tid'));
})

$(document).on('click', '#chatra_nav_load', function(e) {
    curr_page++;
    $elem = $(e.target).prop('id') == "chatra_nav_load" ? $(e.target) : $(e.target).parents('#chatra_nav_load');
    loadChatra($('#chatra_nav_load').data('page'));
    $elem.remove();
})

$(document).on('click', '#chatra_send', function(e) {
    var ticket_id = $("#chatra_ticket_status").data('tid');
    if (ticket_id != "") {
        var text = $("#chatra_textarea").val();
        if (text != "") {
            var now = new Date();
            var id = "message_" + Math.random() * 10000;
            while ($("#" + id).length > 0) {
                id = "message_" + Math.random() * 10000;
            }
            $("#chatra_display .panel-body").append('<div id="' + id + '" class="row chatra_message chatra_message_wait"><div class="col-md-6 chatra_message_content pull-right"><div class="row chatra_message_body"><div class="col-md-9">' + text + '</div><div class="col-md-3"><span class="pull-right">' + ('0' + (now.getHours() + 1)).slice(-2) + ":" + ('0' + now.getMinutes()).slice(-2) + '</span></div></div></div></div>');
            rollToChatBottom("#chatra_display .panel-body");
            $("#chatra_textarea").val("");
            $.ajax({
                url: "crm.php",
                dataType: "json",
                data:  {
                    act: "chatra",
                    step: 5,
                    ticket_id: ticket_id,
                    text: text
                },
                success: function (res) {
                    if (res.error == 1) {
                        alert(res.message);
                    } else {
                        var result = res.content;
                        var create_at = new Date(result.createdAt);
                        var msg = {
                            msg: result,
                            create_at_time: result.createdAt,
                            time: ('0' + create_at.getHours()).slice(-2) + ":" + ('0' + create_at.getMinutes()).slice(-2),
                            create_at_date: create_at.getFullYear() + "-" + ('0' + (create_at.getMonth() + 1)).slice(-2) + "-" + ('0' + create_at.getDate()).slice(-2),
                            unique_id: result.id,
                            temp: 1
                        }
                        var YOHO_CHATRA_ALL_MSG = localStorage.getItem("YOHO_CHATRA_ALL_MSG");
                        var saved = JSON.parse(YOHO_CHATRA_ALL_MSG);
                        saved[ticket_id].messages.push(msg)
                        localStorage.setItem("YOHO_CHATRA_ALL_MSG", JSON.stringify(saved));
                        $("#" + id).toggleClass("chatra_message_wait chatra_message_temp")
                        drawChatraMessages(ticket_id);
                    }
                }
            })
        }
    }
})