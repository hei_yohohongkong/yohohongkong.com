<?php

/**
 * ECSHOP 商品管理程序
 * ============================================================================
 * 版权所有 2005-2010 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: wangleisvn $
 * $Id: goods.php 17114 2010-04-16 07:13:03Z wangleisvn $
*/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/admin/goods.php');
require_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/ERP/lib_erp_goods.php');
$supplierController = new Yoho\cms\Controller\SupplierController();
$smarty->assign('lang', $_LANG);
/*------------------------------------------------------ */
//-- 商品列表
/*------------------------------------------------------ */
if ($_REQUEST['act'] == 'list') {
    $smarty->assign('ur_here', '供應商銷存報告');
    $cat_list = cat_list(0, $cat_id, false, 0, true, true);
    $brand_list = get_brand_list();
    $intro_list = get_intro_list();
    $suppliers_list_name = suppliers_list_name_by_admin();
    $supplier_stock_list = supplier_stock_list();
    $smarty->assign('cat_list',$cat_list);
    $smarty->assign('brand_list',$brand_list);
    $smarty->assign('intro_list',$intro_list);
    $smarty->assign('suppliers_list_name',$suppliers_list_name);
    $smarty->assign('supplier_stock_list',$supplier_stock_list['supplier_stock_list']);
    $smarty->assign('record_count',$supplier_stock_list['record_count']);
    assign_query_info();
    $smarty->display('supplier_stock_report.htm');
} elseif ($_REQUEST['act'] == 'query') {
    $supplier_stock_list = supplier_stock_list();
    $supplierController->ajaxQueryAction($supplier_stock_list['supplier_stock_list'], $supplier_stock_list['record_count'], false);
}

 function supplier_stock_list() {
    global $ecs, $db, $_LANG, $erpController;
    $where = "";
    $_REQUEST['cat_id'] = empty($_REQUEST['cat_id']) ? 0 : intval($_REQUEST['cat_id']);
    $_REQUEST['brand_id'] = empty($_REQUEST['brand_id']) ? 0 : intval($_REQUEST['brand_id']);
    $_REQUEST['include_goods']    = empty($_REQUEST['include_goods']) ? '' : trim($_REQUEST['include_goods']);
    $_REQUEST['intro_type']       = empty($_REQUEST['intro_type']) ? '' : trim($_REQUEST['intro_type']);
    $_REQUEST['start_date']    = empty($_REQUEST['start_date']) ? '' : trim($_REQUEST['start_date']);
    $_REQUEST['end_date']    = empty($_REQUEST['end_date']) ? '' : trim($_REQUEST['end_date']);
    $_REQUEST['suppliers_id']     = empty($_REQUEST['suppliers_id']) ? '' : trim($_REQUEST['suppliers_id']);
    $_REQUEST['sort_by'] = empty($_REQUEST['sort_by']) ? 'goods_id' : trim($_REQUEST['sort_by']);
    $_REQUEST['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
    $_REQUEST['start'] = isset($_REQUEST['start']) ?  $_REQUEST['start'] : 0;
    /* 分页大小 */
    $_REQUEST['page'] = empty($_REQUEST['page']) || (intval($_REQUEST['page']) <= 0) ? 1 : intval($_REQUEST['page']);

    if (isset($_REQUEST['page_size']) && intval($_REQUEST['page_size']) > 0)
    {
        $_REQUEST['page_size'] = intval($_REQUEST['page_size']);
    }
    else
    {
        $_REQUEST['page_size'] = 50;
    }
    // To prevent exceeding memory limit, max page_size is limited to 1000
    if ($_REQUEST['page_size'] > 1000)
    {
        $_REQUEST['page_size'] = 1000;
    }
    $_REQUEST['page_count']     = $_REQUEST['record_count'] > 0 ? ceil($_REQUEST['record_count'] / $_REQUEST['page_size']) : 1;
    /* 產品名稱/ID */
    if (!empty($_REQUEST['include_goods']))
    {
        if(strpos($_REQUEST['include_goods'], ",") > 0){
            $include_goods = explode(",", $_REQUEST['include_goods']);
    
            $include_goods_like_str = array_map(function ($goods_name) { return "goods_name like '%".trim(mysql_like_quote($goods_name))."%'"; }, $include_goods);
            $include_goods_sn_like_str = array_map(function ($goods_sn) { return "goods_name like '%".trim(mysql_like_quote($goods_sn))."%'"; }, $include_goods);
            $include_goods_id_str = array_map(function ($goods_id) { return "goods_id = '".trim(mysql_like_quote($goods_id))."'"; }, $include_goods);

            $where .= " AND (".implode(" OR ", $include_goods_like_str)." OR ".implode(" OR ", $include_goods_sn_like_str)." OR ".implode(" OR ", $include_goods_id_str).")";
        }
        else{
            $where .= " AND (goods_sn LIKE '%" . mysql_like_quote($_REQUEST['include_goods']) . "%' ";
            $where .= " OR goods_name LIKE '%" . mysql_like_quote($_REQUEST['include_goods']) . "%' ";
            $tmp = preg_split('/\s+/', $_REQUEST['include_goods']);
            if (count($tmp) > 1)
            {
                $where .= " OR ( 1";
                foreach ($tmp as $kw)
                {
                    $where .= " AND goods_name LIKE '%" . mysql_like_quote($kw) . "%' ";
                }
                $where .= ") ";
            }
            $where .= " OR goods_id = '" . mysql_like_quote($_REQUEST['include_goods']) . "')";
        }
        
    }
    /* 推荐类型 */
    switch ($_REQUEST['intro_type'])
    {
        case 'is_best':
            $where .= " AND is_best=1";
            break;
        case 'is_hot':
            $where .= ' AND is_hot=1';
            break;
        case 'is_new':
            $where .= ' AND is_new=1';
            break;
        case 'is_promote':
            $where .= " AND is_promote = 1 AND promote_price > 0 AND promote_start_date <= '$today' AND promote_end_date >= '$today'";
            break;
        case 'all_type';
            $where .= " AND (is_best=1 OR is_hot=1 OR is_new=1 OR (is_promote = 1 AND promote_price > 0 AND promote_start_date <= '" . $today . "' AND promote_end_date >= '" . $today . "'))";
    }
    /* 供應商 */
    if (!empty($_REQUEST['suppliers_id']))
    {
        $where .= " AND goods_id in (select goods_id from ".$GLOBALS['ecs']->table('erp_goods_supplier')." where supplier_id='".$_REQUEST['suppliers_id']."')";
    }
    if (isset($_REQUEST['cat_id']) && $_REQUEST['cat_id'] > 0)
    {
        $arr_cats = cat_list($_REQUEST['cat_id'], $_REQUEST['cat_id'], FALSE);
        $arr_cats_id = array();
        if (sizeof($arr_cats) > 0){
            foreach($arr_cats as $cat){
                array_push($arr_cats_id, $cat['cat_id']);
            }
        }
        $str_cats_id = implode(",", $arr_cats_id);
        $where .= " AND g.cat_id IN (" . $str_cats_id . ") ";
    }
    if ($_REQUEST['brand_id'] && $_REQUEST['brand_id'] > -1)
    {
        $where .= " AND g.brand_id = '" . $_REQUEST['brand_id'] . "' ";
    }
    if (!empty($_REQUEST['start_date'])) {
        $start_time = date('Y-m-d H:i:s', strtotime($_REQUEST['start_date']));
        $start_time_sql .= " AND last_updated >= '" . $start_time ."'";
    }
    if (!empty($_REQUEST['end_date'])) {
            $end_time = date ('Y-m-d H:i:s', strtotime($_REQUEST['end_date'])+86399);
            $end_time_sql .= " AND last_updated <= '" . $end_time ."'";
    }
    $base_sql = "SELECT goods_id, goods_name, goods_type, goods_sn, is_best, is_new, is_hot, " .
            ((empty($_REQUEST['suppliers_id']) && empty($_REQUEST['start_date']) && empty($_REQUEST['end_date'])) ? "sales," : "(SELECT IFNULL(SUM(`consumed_qty`),0) FROM " . $GLOBALS['ecs']->table('order_goods_supplier_consumption') . " as ogsc WHERE ogsc.goods_id = g.goods_id ". ((!empty($_REQUEST['suppliers_id'])) ? "AND `supplier_id` LIKE '".$_REQUEST['suppliers_id']."'" : "") . ((!empty($_REQUEST['start_date'])) ? $start_time_sql : "") . ((!empty($_REQUEST['end_date'])) ? $end_time_sql : "" ) . ") as sales," ).
            " (SELECT sum(ega.goods_qty) FROM " . $GLOBALS['ecs']->table('erp_goods_attr_stock') . " as ega WHERE ega.goods_id = g.goods_id AND ega.warehouse_id IN (".$erpController->getSellableWarehouseIds(true).")) as goods_number, ".
            " (promote_price > 0 AND promote_start_date <= '$today' AND promote_end_date >= '$today') AS is_promote, display_discount ".
            " FROM " . $GLOBALS['ecs']->table('goods') . " AS g WHERE is_delete='0'  AND is_real='1' AND is_on_sale = '1'  $where " .
            " ORDER BY $_REQUEST[sort_by] $_REQUEST[sort_order]";
    if (empty($_REQUEST['suppliers_id'])) { 
        $count_sql = "SELECT COUNT(c.goods_id) FROM ( ". $base_sql . ") AS c";
        $record_count   = $db->getOne($count_sql);     
        $sql = $base_sql ." LIMIT " . $_REQUEST['start'] . ",$_REQUEST[page_size]";
        $res = $db->getAll($sql); 
    } else {
        $count_sql = "SELECT COUNT(c.goods_id) FROM ( ". $base_sql . ") AS c";
        $record_count   = $db->getOne($count_sql);    
        $size = 5000;
        $times = ceil($record_count/$size);
        $res = [];
        for ($i=0; $i<$times; $i++) {
            $start = $size * $i;
            $sql =  $base_sql . " LIMIT $start, $size";
            $result = $db->getAll($sql);
            foreach ($result as $key => $row) {  
                array_push($res, $row);
            }  
        }  
        foreach ($res as $key => $vallue) {
            $goods_id = $vallue['goods_id'];
            $total_stock = $vallue['goods_number'];
            //產品的所有供應商
            $goods_suppliers_sql = "SELECT supplier_id FROM " . $GLOBALS['ecs']->table('erp_goods_supplier') . " WHERE `goods_id` = $goods_id";
            $goods_suppliers = $db->getAll($goods_suppliers_sql); 
            if (count($goods_suppliers) == 1) continue;
            //產品的最近po單
            $goods_po_sql = "SELECT eo.order_id, eoi.order_qty, eo.supplier_id FROM " . $GLOBALS['ecs']->table('erp_order_item') . " AS eoi LEFT JOIN ecs_erp_order AS eo ON eo.order_id = eoi.order_id WHERE `goods_id` = $goods_id ORDER BY eo.order_id DESC LIMIT 20";
            $goods_po = $db->getAll($goods_po_sql); 
            $accumulate_stock = 0;
            $selected_supplier_stock = 0;
            foreach ($goods_po as $po) {
                $po_supplier_id = $po['supplier_id'];
                $po_supplier_qty = $po['order_qty'];
                if ($po_supplier_id == $_REQUEST['suppliers_id']) {
                    if(($accumulate_stock + $po_supplier_qty) < $total_stock) {
                        $selected_supplier_stock += $po_supplier_qty;
                    } else {
                        $selected_supplier_stock += ($total_stock - $accumulate_stock);
                    }
                }
                $accumulate_stock += $po_supplier_qty;
                if($accumulate_stock >= $total_stock) {
                    $res[$key]['goods_number'] = $selected_supplier_stock;
                    break;
                }
            }
        }
        if ($_REQUEST['sort_by'] == "goods_number") {
            $goods_number_arr = array_column($res,"goods_number");
            if ($_REQUEST['sort_order'] == 'asc') {
                array_multisort($goods_number_arr, SORT_ASC, $res);
            } else {
                array_multisort($goods_number_arr, SORT_DESC, $res);
            }
        }
    }
 
  
    $arr = array('supplier_stock_list' => $res, 'record_count' => $record_count);
    return $arr;
}
function get_intro_list()
{
    return array(
        'is_best'    => $GLOBALS['_LANG']['is_best'],
        'is_new'     => $GLOBALS['_LANG']['is_new'],
        'is_hot'     => $GLOBALS['_LANG']['is_hot'],
        'is_promote' => $GLOBALS['_LANG']['is_promote'],
        'all_type' => $GLOBALS['_LANG']['all_type'],
    );
}

?>
