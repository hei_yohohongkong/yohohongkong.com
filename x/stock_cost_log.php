<?php

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require_once(ROOT_PATH . 'includes/lib_order.php');
require_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_goods.php');
require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/admin/statistic.php');
$smarty->assign('lang', $_LANG);

if($_REQUEST['act'] == 'list'){
    $start_date = strtotime(date("Y-m-d",strtotime("-1 day")));
    $end_date = strtotime(date("Y-m-d"));
    $type = [];
    $display_cat = false;
    $display_brand = false;
    $display_supp = false;
    
    if(!isset($_REQUEST['submit']) || $_REQUEST['type'] == '0'){
        $type = ["1","2","3"];
        $display_cat = true;
        $display_brand = true;
        $display_supp = true;
    }
    if(isset($_REQUEST['start_date']) && $_REQUEST['start_date'] != ""){
        $start_date = strtotime($_REQUEST['start_date']);
        $end_date = strtotime($_REQUEST['start_date']) + 86400;
    }
    if(isset($_REQUEST['end_date']) && $_REQUEST['end_date'] != ""){
        $end_date = strtotime($_REQUEST['end_date']);
    }
    $where = " WHERE report_date >= " . $start_date . " AND report_date < " . $end_date;
    if(isset($_REQUEST['type']) && $_REQUEST['type'] != ""){
        if(in_array($_REQUEST['type'],["1","3","5","7"])){
            $type[] = "1";
            $display_cat = true;
        }
        if(in_array($_REQUEST['type'],["2","3","6","7"])){
            $type[] = "2";
            $display_brand = true;
        }
        if(in_array($_REQUEST['type'],["4","5","6","7"])){
            $type[] = "3";
            $display_supp = true;
        }
    }
    $where .= " AND type IN(" . join(',',$type) . ") ";
    $brand_names = [];
    $cat_names = [];
    $supp_names = [];

    $sql = "SELECT brand_id,brand_name FROM " . $ecs->table('brand');
    $res = $db->getAll($sql);
    foreach($res as $row){
        $brand_names[$row['brand_id']] = $row['brand_name'];
    }
    $sql = "SELECT cat_id,cat_name FROM " . $ecs->table('category');
    $res = $db->getAll($sql);
    foreach($res as $row){
        $cat_names[$row['cat_id']] = $row['cat_name'];
    }
    $sql = "SELECT supplier_id,name FROM " . $ecs->table('erp_supplier');
    $res = $db->getAll($sql);
    foreach($res as $row){
        $supp_names[$row['supplier_id']] = $row['name'];
    }
 
    $sql = "SELECT * FROM " . $ecs->table('stock_cost_log') . $where . " AND created_at IN (SELECT MAX(created_at) FROM " . $ecs->table('stock_cost_log') . " GROUP BY report_date)";
    //die($sql);
    $res = $db->getAll($sql);
    
    $cat_list = [];
    $brand_list = [];
    $supp_list = [];
    foreach($res as $row){
        
        if($row['type'] == 1){
            $cat_list['total'] += $row['cost'];
            $cat_list['total_dead_30'] += $row['slowsoldcost_30'];
            $cat_list['total_dead_60'] += $row['slowsoldcost_60'];
            $cat_list['total_dead_90'] += $row['slowsoldcost_90'];
            $cat_list[] = array('name' => isset($cat_names[$row['type_id']]) ? $cat_names[$row['type_id']] : "沒有分類", 'cost' => $row['cost'], 'dead_cost_30' => $row['slowsoldcost_30'], 'dead_cost_60' => $row['slowsoldcost_60'], 'dead_cost_90' => $row['slowsoldcost_90']);
        } else if($row['type'] == 2){
            $brand_list['total'] += $row['cost'];
            $brand_list['total_dead_30'] += $row['slowsoldcost_30'];
            $brand_list['total_dead_60'] += $row['slowsoldcost_60'];
            $brand_list['total_dead_90'] += $row['slowsoldcost_90'];
            $brand_list[] = array('name' => isset($brand_names[$row['type_id']]) ? $brand_names[$row['type_id']] : "沒有品牌", 'cost' => $row['cost'], 'dead_cost_30' => $row['slowsoldcost_30'], 'dead_cost_60' => $row['slowsoldcost_60'], 'dead_cost_90' => $row['slowsoldcost_90']);
        } else if($row['type'] == 3){
            $supp_list['total'] += $row['cost'];
            $supp_list['total_dead_30'] += $row['slowsoldcost_30'];
            $supp_list['total_dead_60'] += $row['slowsoldcost_60'];
            $supp_list['total_dead_90'] += $row['slowsoldcost_90'];
            $supp_list[] = array('name' => isset($supp_names[$row['type_id']]) ? $supp_names[$row['type_id']] : "NA", 'cost' => $row['cost'], 'dead_cost_30' => $row['slowsoldcost_30'], 'dead_cost_60' => $row['slowsoldcost_60'], 'dead_cost_90' => $row['slowsoldcost_90']);
        } 
    }

    $format_key = ['total', 'total_dead_30', 'total_dead_60', 'total_dead_90'];
    foreach ($format_key as $key) {
        $cat_list[$key] = sprintf('%0.2f', $cat_list[$key]);
        $brand_list[$key] = sprintf('%0.2f', $brand_list[$key]);
        $supp_list[$key] = sprintf('%0.2f', $supp_list[$key]);
    }
    $smarty->assign('cat_list',     $cat_list);
    $smarty->assign('brand_list',   $brand_list);
    $smarty->assign('supp_list',    $supp_list);
	$smarty->assign('full_page',    1);
    $smarty->assign('ur_here',      $_LANG['stock_cost_log']);
    $smarty->assign('display_cat',$display_cat);
    $smarty->assign('display_brand',$display_brand);
    $smarty->assign('display_supp',$display_supp);
    assign_query_info();
    $smarty->display('stock_cost_log.htm');
}
?>