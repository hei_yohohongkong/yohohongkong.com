<?php

/***
* purchase_payments.php
* by howang 2014-12-03
*
* Accounting System
***/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_accounting.php');
require_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/ERP/lib_erp_order.php');
$controller = new Yoho\cms\Controller\YohoBaseController();
$adminuserController = new Yoho\cms\Controller\AdminuserController();
$accountingController = new Yoho\cms\Controller\AccountingController();
if ($_REQUEST['act'] == 'list')
{
    /* 权限判断 */
    admin_priv('erp_order_finish');
    
    /* 时间参数 */
    $start_date = (empty($_REQUEST['start_date'])) ? month_start() : $_REQUEST['start_date'];
    $end_date = (empty($_REQUEST['end_date'])) ? month_end() : $_REQUEST['end_date'];
    
    $order_sn = (empty($_REQUEST['order_sn'])) ? '' : trim($_REQUEST['order_sn']);
    
    // $list = get_purchase_payments();
    $smarty->assign('ur_here',      '採購訂單付款紀錄');
    // $smarty->assign('full_page',    1);
    
    // $smarty->assign('list',         $list['data']);
    // $smarty->assign('filter',       $list['filter']);
    // $smarty->assign('record_count', $list['record_count']);
    // $smarty->assign('page_count',   $list['page_count']);
    $smarty->assign('start_date',   $start_date);
    $smarty->assign('end_date',     $end_date);
    $smarty->assign('order_sn',     $order_sn);
    $smarty->assign('cfg_lang',     $_CFG['lang']);
    
    $smarty->assign('action_link', array('text' => '採購訂單列表', 'href' => 'erp_order_manage.php?act=order_list'));
    
    $smarty->assign('accounts',     $actg->getAccounts());
    $smarty->assign('payment_types',$actg->getPaymentTypes());
    
    assign_query_info();
    $smarty->display('purchase_payments_list.htm');
}
elseif ($_REQUEST['act'] == 'query')
{
    /* 权限判断 */
    check_authz_json('sale_order_stats');
    
    $list = get_purchase_payments();
    $controller->ajaxQueryAction($list['data'], $list['record_count'], false);
    // $smarty->assign('list',         $list['data']);
    // $smarty->assign('filter',       $list['filter']);
    // $smarty->assign('record_count', $list['record_count']);
    // $smarty->assign('page_count',   $list['page_count']);

}
elseif ($_REQUEST['act'] == 'add')
{
    /* 权限判断 */
    admin_priv('erp_order_finish');
    
    $order_id = empty($_REQUEST['order_id']) ? 0 : intval($_REQUEST['order_id']);
    if (empty($order_id))
    {
        sys_msg('必須輸入訂單號');
    }
    require_once(ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_order.php');
    require_once(ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_supplier.php');
    $order = get_order_info($order_id);
    if (!$order)
    {
        sys_msg('找不到該訂單');
    }
    
    $smarty->assign('ur_here',     '採購訂單付款');
    
    $smarty->assign('action_link', array('text' => '訂單付款紀錄', 'href' => 'purchase_payments.php?act=list&order_sn=' . $order['order_sn']));
    $smarty->assign('action_link2', array('text' => '返回訂單', 'href' => 'erp_order_manage.php?act=view_order&order_id=' . $order_id));
    $salesperson = $adminuserController->getAdminsByPriv(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_PAYMENT_OP, 'sales_name', [], true, true);
    $smarty->assign('salesperson_list', $salesperson);
    $smarty->assign('admin_id', $_SESSION['admin_id']);
    $smarty->assign('admin_name', $_SESSION['admin_name']);

    $smarty->assign('accounts', $accountingController->getAccountingTypes($accountingController->getAccountingType(['actg_type_code' => '11000'], 'actg_type_id')));
    // $smarty->assign('payment_types', $actg->getPaymentTypes());
    
    $smarty->assign('order_id',    $order_id);
    $order['create_time_formatted'] = local_date($_CFG['time_format'], $order['create_time']);
    $smarty->assign('order',       $order);
    
    $paid_amounts = $actgHooks->getPurchaseOrderPaidAmounts($order_id);
    if (empty($paid_amounts))
    {
        $default_currency = $actg->getCurrency(array('is_default' => 1));
        $currency = $actg->getCurrency(array('currency_code' => $order['currency_code']));
        $paid_str = $actg->money_format(0, $currency['currency_format']);
        $order_total_str = $actg->money_format($order['original_order_amount'], $currency['currency_format']) . " = <span class='red'>" . $actg->money_format($order['order_amount'], $default_currency['currency_format']) . "</span>";
        $due = round($order['order_amount'], 2);
        $due_str = $actg->money_format($order['original_order_amount'], $currency['currency_format']) . " = <span class='red'>" . $actg->money_format($due, $default_currency['currency_format']) . "</span>";
    }
    else
    {
        $currencies = array();
        foreach ($actg->getCurrencies() as $row)
        {
            $currencies[$row['currency_code']] = $row;
            if ($row['is_default'])
            {
                $default_currency = $row;
            }
        }
        $paid_str = implode(' + ', array_map(function ($amount, $currency_code) use ($actg, $currencies) {
            $currency = $currencies[$currency_code];
            return $actg->money_format($amount, $currency['currency_format']);
        }, $paid_amounts, array_keys($paid_amounts)));
        $total = array_reduce(array_map(function ($amount, $currency_code) use ($currencies) {
            $currency = $currencies[$currency_code];
            return bcmul($amount, $currency['currency_rate']);
        }, $paid_amounts, array_keys($paid_amounts)), 'bcadd', 0);
        if ((count($paid_amounts) > 1) || (empty($paid_amounts[$default_currency['currency_code']])))
        {
            $paid_str .= ' = ' . $actg->money_format($total, $default_currency['currency_format']);
        }
        $order_total_str = $actg->money_format($order['original_order_amount'], $currencies[$order['currency_code']]['currency_format']) . " = <span class='red'>" . $actg->money_format($order['order_amount'], $default_currency['currency_format']) . "</span>";
        $due = round(bcsub($order['order_amount'], $total), 2);
        $due_str = $actg->money_format($due, $default_currency['currency_format']);
    }
    $smarty->assign('paid_str',    $paid_str);
    $smarty->assign('order_total_str', $order_total_str);
    $smarty->assign('due', floatval($due));
    $smarty->assign('due_str', $due_str);

    $smarty->assign('currencies', json_encode($actg->getCurrencies()));
    
    assign_query_info();
    $smarty->display('purchase_payment_info.htm');
}
elseif ($_REQUEST['act'] == 'insert')
{
    /* 权限判断 */
    admin_priv('sale_order_stats');

    $order_id = empty($_POST['order_id']) ? 0 : intval($_POST['order_id']);
    if (empty($order_id))
    {
        sys_msg('必須輸入訂單號');
    }

    require_once(ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_order.php');
    require_once(ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_supplier.php');
    $order = get_order_info($order_id);
    if (!$order)
    {
        sys_msg('找不到該訂單');
    }

    $operator = empty($_POST['operator']) ? '' : trim($_POST['operator']);
    if (empty($operator))
    {
        sys_msg('必須輸入操作員');
    }

    $account_id = empty($_POST['actg_type_id']) ? 0 : intval($_POST['actg_type_id']);
    if (empty($account_id))
    {
        sys_msg('必須輸入帳戶');
    }
    $acc = $actg->getAccount(['actg_type_id' => $account_id]);
    if (empty($acc))
    {
        sys_msg('帳戶不正確');
    }

    $due = isset($_POST['due']) ? doubleval($_POST['due']) : 0.0;
    if (empty($due) || $due <= 0)
    {
        sys_msg('此訂單已完全付款');
    }

    $currency_code = $acc['currency_code'];

    $completed = isset($_POST['completed']) ? (!empty($_POST['completed'])) : true;

    $amount = empty($_POST['amount']) ? 0.0 : doubleval($_POST['amount']);
    $exchanged_amount = empty($_POST['exchanged_amount']) ? 0.0 : doubleval($_POST['exchanged_amount']);

    $cn_ids = empty($_POST['cn_id']) ? [] : explode(',',$_POST['cn_id']);
    $credit = doubleval(sum_curr_credit_note_amount($cn_ids));
    $used_sn = [];

    if (empty($amount) && empty($credit))
    {
        sys_msg('必須輸入金額');
    }
    if (empty($exchanged_amount) && empty($credit))
    {
        sys_msg('必須輸入港幣金額');
    }
    if (($completed) && (!$acc['allow_debit']) && (bccomp(bcadd(bcmul($amount, -1), $acc['account_balance']), 0) < 0))
    {
        sys_msg('金額超出帳戶結餘');
    }

    // $payment_type_id = empty($_POST['payment_type_id']) ? 0 : intval($_POST['payment_type_id']);
    // if (empty($payment_type_id))
    // {
    //     sys_msg('必須選擇付款方式');
    // }
    // $pay_type = $actg->getPaymentType(compact('payment_type_id'));
    // if (empty($pay_type))
    // {
    //     sys_msg('付款方式不正確');
    // }
    
    $bank_date = empty($_POST['bank_date']) ? 0 : $actg->dbdate(local_strtotime(trim($_POST['bank_date'])));

    $remark = empty($_POST['remark']) ? '' : trim($_POST['remark']);

    $datetime = $actg->dbdate();

    $expense_year = empty($_POST['expense_year']) ? null : intval($_POST['expense_year']);

    $expense_month = empty($_POST['expense_month']) ? null : intval($_POST['expense_month']);

    $actg_month = null;
    if (!empty($expense_year) && !empty($expense_month)) {
        $actg_month = sprintf("%04s/%02s", $expense_year, $expense_month);
    }

    // $actg->start_transaction();

    $total_paid = $due > $amount + $credit ? $amount + $credit : $due;

    $purchase_payment = array(
        'order_id' => $order_id,
        'add_date' => $datetime,
        'bank_date' => $bank_date ? $bank_date : ($completed ? $datetime : null),
        'complete_date' => $completed ? $datetime : null,
        'currency_code' => $currency_code,
        'amount' => $amount,
        'exchanged_amount' => $exchanged_amount,
        'payment_type_id' => $payment_type_id,
        'account_id' => $account_id,
        'operator' => $operator,
        'remark' => $remark,
        'cn_ids' => $cn_ids,
        'cn_amount' => $total_paid - $amount,
        'expense_year' => $expense_year,
        'expense_month' => $expense_month
    );

    $param = [
        'actg_type_id'  => $account_id,
        'created_by'    => $operator,
        'amount'        => $amount,
        'exchanged_amount' => $exchanged_amount,
        'currency_code' => $currency_code,
        'erp_order_id'  => $order_id,
        'supplier_id'   => $order['supplier_id'],
        'remark'        => $remark,
        'cn_ids'        => $cn_ids,
        'cn_amount'     => $total_paid - $amount,
        'fully_paid'    => isset($_POST['fully_paid']) ? (!empty($_POST['fully_paid'])) : false,
        'actg_month'    => $actg_month,
    ];

    if ($accountingController->insertPurchasePaymentTransactions($param)) {
        $link[0]['text'] = '採購訂單列表';
        $link[0]['href'] = 'erp_order_manage.php?act=order_list';
        $link[1]['text'] = '採購訂單付款紀錄';
        $link[1]['href'] = 'purchase_payments.php?act=list';

        $action_remark = "付款訂單 $order[order_sn]";
        $sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
                "VALUES(" . $order_id . ", " . ERP_PAID . "," . $_SESSION['admin_id'] . ",'" . $action_remark . "')";
        $GLOBALS['db']->query($sql);
        admin_log($order['order_sn'],'paid','erp_order');

        sys_msg('採購訂單付款成功', 0, $link, true);
    } else {
        sys_msg('採購訂單付款失敗');
    }

    // $purchase_payment_id = $actg->addPurchasePayment($purchase_payment);

    // if ($purchase_payment_id)
    // {
        // $fully_paid = isset($_POST['fully_paid']) ? (!empty($_POST['fully_paid'])) : false;
        
        // $paid_amounts = $actgHooks->getPurchaseOrderPaidAmounts($order_id);
        // $total_paid = $actgHooks->calculateTotalPaidAmount($paid_amounts);
        
        // $sql = "UPDATE " . $ecs->table('erp_order') . 
        //     "SET `actg_paid_amount` = '" . $total_paid . "' " .
        //     ($fully_paid ? ", `fully_paid` = 1 " : '') .
        //     "WHERE `order_id` = '" . $order_id . "'";
        // if (!$db->query($sql))
        // {
        //     $actg->rollback_transaction();
            
        //     sys_msg('無法更新採購訂單');
        // }
        
    //     $actg->commit_transaction();
        
    //     $link[0]['text'] = '採購訂單列表';
    //     $link[0]['href'] = 'erp_order_manage.php?act=order_list';
    //     $link[1]['text'] = '採購訂單付款紀錄';
    //     $link[1]['href'] = 'purchase_payments.php?act=list';

    //     $action_remark = "付款訂單 $order[order_sn]";
    //     $sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
    //             "VALUES(" . $order_id . ", " . ERP_PAID . "," . $_SESSION['admin_id'] . ",'" . $action_remark . "')";
    //     $GLOBALS['db']->query($sql);
    //     admin_log($order['order_sn'],'paid','erp_order');

    //     sys_msg('採購訂單付款成功', 0, $link, true);
    // }
    // else
    // {
    //     $actg->rollback_transaction();
        
    //     sys_msg('採購訂單付款失敗');
    // }
}
elseif ($_REQUEST['act'] == 'batch_add')
{
    /* 权限判断 */
    admin_priv('erp_order_finish');

    $order_ids = empty($_REQUEST['order_ids']) ? [] : explode(",", trim($_REQUEST['order_ids']));
    if (empty($order_ids))
    {
        sys_msg('必須輸入訂單號');
    }

    if (count($order_ids) == 1) {
        header('Location: purchase_payments.php?act=add&order_id=' . $order_ids[0]);
    }

    $order_sns = [];
    $process_ids = [];
    $total_paid = [];
    $total_order = [];
    $paid_str = [];
    $total_due_amount = 0;
    $supplier_id = 0;

    require_once(ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_order.php');
    require_once(ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_supplier.php');

    $currencies = array();
    foreach ($actg->getCurrencies() as $row) {
        $currencies[$row['currency_code']] = $row;
        if ($row['is_default']) {
            $default_currency = $row;
        }
    }
    foreach ($order_ids as $order_id) {
        $order = get_order_info($order_id);
        if (!$order) {
            continue;
        }
        if (empty($order['supplier_id'])) {
            continue;
        }
        if (empty($supplier_id)) {
            $supplier_id = $order['supplier_id'];
        } elseif ($supplier_id != $order['supplier_id']) {
            sys_msg('選擇了不同供應商');
        }
        $process_ids[] = $order_id;
        $order_sns[] = "<a href='erp_order_manage.php?act=view_order&order_id=$order_id'>$order[order_sn]</a>";
        $paid_amounts = $actgHooks->getPurchaseOrderPaidAmounts($order_id);
        if (empty($paid_amounts)) {
            if (!isset($total_paid[$default_currency['currency_code']])) {
                $total_paid[$default_currency['currency_code']] = 0;
            }
            if (!isset($total_order[$default_currency['currency_code']])) {
                $total_order[$default_currency['currency_code']] = 0;
            }
            if (!isset($total_due[$default_currency['currency_code']])) {
                $total_due[$default_currency['currency_code']] = 0;
            }
            $total_paid[$default_currency['currency_code']] = bcadd($total_paid[$default_currency['currency_code']], 0, 2);
            $total_order[$default_currency['currency_code']] = bcadd($total_order[$default_currency['currency_code']], round($order['order_amount'], 2), 2);
        } else {
            foreach ($paid_amounts as $currency_code => $amount) {
                if (!isset($total_paid[$currency_code])) {
                    $total_paid[$currency_code] = 0;
                }
                $total_paid[$currency_code] = bcadd($total_paid[$currency_code], round($amount, 2), 2);
            }

            if (!isset($total_order[$default_currency['currency_code']])) {
                $total_order[$default_currency['currency_code']] = 0;
            }
            $total_order[$default_currency['currency_code']] = bcadd($total_order[$default_currency['currency_code']], round($order['order_amount'], 2), 2);
        }
    }

    foreach ($total_order as $currency_code => $amount) {
        $total_due_amount = bcadd($total_due_amount, round(bcmul($amount, $currencies[$currency_code]['currency_rate'], 4), 2), 2);
    }
    $order_total_str = $actg->money_format($total_due_amount, $default_currency['currency_format']);
    foreach ($total_paid as $currency_code => $amount) {
        $paid_str[] = $actg->money_format($amount, $currencies[$currency_code]['currency_format']);
        $total_due_amount = bcsub($total_due_amount, round(bcmul($amount, $currencies[$currency_code]['currency_rate'], 4), 2), 2);
    }
    $due_str = $actg->money_format($total_due_amount, $default_currency['currency_format']);

    $smarty->assign('ur_here',     '採購訂單付款');

    $smarty->assign('action_link', array('text' => '訂單付款紀錄', 'href' => 'purchase_payments.php?act=list'));
    $smarty->assign('action_link2', array('text' => '返回訂單列表', 'href' => 'erp_order_manage.php?act=list'));
    $salesperson = $adminuserController->getAdminsByPriv(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_PAYMENT_OP, 'sales_name', [], true, true);
    $smarty->assign('salesperson_list', $salesperson);
    $smarty->assign('admin_id', $_SESSION['admin_id']);
    $smarty->assign('admin_name', $_SESSION['admin_name']);

    $smarty->assign('accounts', $accountingController->getAccountingTypes($accountingController->getAccountingType(['actg_type_code' => '11000'], 'actg_type_id')));
    $smarty->assign('payment_types', $actg->getPaymentTypes());

    $smarty->assign('order_ids',    implode(",", $process_ids));
    $smarty->assign('supplier_id',  $supplier_id);
    $smarty->assign('order_sns',    implode(", ", $order_sns));
    $smarty->assign('paid_str',     implode(" + ", $paid_str));
    $smarty->assign('order_total_str', $order_total_str);
    $smarty->assign('due', floatval($total_due_amount));
    $smarty->assign('due_str', $due_str);

    assign_query_info();
    $smarty->display('purchase_payment_info_batch.htm');
}
elseif ($_REQUEST['act'] == 'batch_insert')
{
    /* 权限判断 */
    admin_priv('sale_order_stats');

    $order_ids = empty($_REQUEST['order_ids']) ? [] : explode(",", trim($_REQUEST['order_ids']));
    if (empty($order_ids))
    {
        sys_msg('必須輸入訂單號');
    }

    $operator = empty($_POST['operator']) ? '' : trim($_POST['operator']);
    if (empty($operator))
    {
        sys_msg('必須輸入操作員');
    }

    $account_id = empty($_POST['actg_type_id']) ? 0 : intval($_POST['actg_type_id']);
    if (empty($account_id))
    {
        sys_msg('必須輸入帳戶');
    }
    $acc = $actg->getAccount(['actg_type_id' => $account_id]);
    if (empty($acc))
    {
        sys_msg('帳戶不正確');
    }

    $due = isset($_POST['due']) ? doubleval($_POST['due']) : 0.0;
    if (empty($due) || $due <= 0)
    {
        sys_msg('此訂單已完全付款');
    }

    $currency_code = $acc['currency_code'];

    $completed = isset($_POST['completed']) ? (!empty($_POST['completed'])) : true;

    $amount = empty($_POST['amount']) ? 0.0 : doubleval($_POST['amount']);

    if (empty($amount))
    {
        sys_msg('必須輸入金額');
    }

    if (($completed) && (!$acc['allow_debit']) && (bccomp(bcadd(bcmul($amount, -1), $acc['account_balance']), 0) < 0))
    {
        sys_msg('金額超出帳戶結餘');
    }

    $payment_type_id = empty($_POST['payment_type_id']) ? 0 : intval($_POST['payment_type_id']);
    if (empty($payment_type_id))
    {
        sys_msg('必須選擇付款方式');
    }
    $pay_type = $actg->getPaymentType(compact('payment_type_id'));
    if (empty($pay_type))
    {
        sys_msg('付款方式不正確');
    }

    $bank_date = empty($_POST['bank_date']) ? 0 : $actg->dbdate(local_strtotime(trim($_POST['bank_date'])));

    $remark = empty($_POST['remark']) ? '' : trim($_POST['remark']);

    $datetime = $actg->dbdate();

    $expense_year = empty($_POST['expense_year']) ? null : intval($_POST['expense_year']);

    $expense_month = empty($_POST['expense_month']) ? null : intval($_POST['expense_month']);

    $actg_month = null;
    if (!empty($expense_year) && !empty($expense_month)) {
        $actg_month = sprintf("%04s/%02s", $expense_year, $expense_month);
    }

    require_once(ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_order.php');
    require_once(ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_supplier.php');

    $process_orders = [];
    foreach ($order_ids as $order_id) {
        $order = get_order_info($order_id);
        if (!$order) {
            continue;
        }
        if (empty($order['supplier_id'])) {
            continue;
        }
        if (empty($supplier_id)) {
            $supplier_id = $order['supplier_id'];
        } elseif ($supplier_id != $order['supplier_id']) {
            sys_msg('選擇了不同供應商');
        }
        $process_orders[$order_id] = $order;
    }

    $paid_orders = [];
    foreach ($process_orders as $order_id => $order) {
        $paid_amounts = $actgHooks->getPurchaseOrderPaidAmounts($order_id);
        if (empty($paid_amounts)) {
            $current_due = round($order['order_amount'], 2);
        } else {
            $currencies = array();
            foreach ($actg->getCurrencies() as $row) {
                $currencies[$row['currency_code']] = $row;
                if ($row['is_default']) {
                    $default_currency = $row;
                }
            }
            $total = array_reduce(array_map(function ($current_amount, $currency_code) use ($currencies) {
                $currency = $currencies[$currency_code];
                return round(bcmul($current_amount, $currency['currency_rate'], 4), 2);
            }, $paid_amounts, array_keys($paid_amounts)), 'bcadd', 0);
            $current_due = bcsub(round($order['order_amount'], 2), $total, 2);
        }
        if (bccomp($amount, $current_due) == -1) continue;
        $purchase_payment = array(
            'order_id' => $order_id,
            'add_date' => $datetime,
            'bank_date' => $bank_date ? $bank_date : ($completed ? $datetime : null),
            'complete_date' => $completed ? $datetime : null,
            'currency_code' => $currency_code,
            'amount' => $current_due,
            'payment_type_id' => $payment_type_id,
            'account_id' => $account_id,
            'operator' => $operator,
            'remark' => $remark,
            'expense_year' => $expense_year,
            'expense_month' => $expense_month
        );

        $param = [
            'actg_type_id'  => $account_id,
            'created_by'    => $operator,
            'amount'        => $current_due,
            'currency_code' => $currency_code,
            'erp_order_id'  => $order_id,
            'supplier_id'   => $supplier_id,
            'remark'        => $remark,
            'fully_paid'    => true, // Batch payment must be fully paid
            'actg_month'    => $actg_month
        ];

        if ($purchase_payment_id = $accountingController->insertPurchasePaymentTransactions($param))
        {
            $paid_orders[] = $purchase_payment_id;

            $action_remark = "付款訂單 $order[order_sn]";
            $sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
                    "VALUES(" . $order_id . ", " . ERP_PAID . "," . $_SESSION['admin_id'] . ",'" . $action_remark . "')";
            $GLOBALS['db']->query($sql);
            admin_log($order['order_sn'],'paid','erp_order');
        } else {
            sys_msg('採購訂單付款失敗');
        }
        $amount = bcsub($amount, $current_due, 2);
    }
    $paid_txns = $GLOBALS['db']->getCol("SELECT actg_txn_id FROM `actg_purchase_payments` WHERE purchase_payment_id " . db_create_in($paid_orders));
    $new_actg_group_id = $accountingController->getNewActgGroupId();
    $new_txn_group_id = $accountingController->getNewTxnGroupId();
    $GLOBALS['db']->query("UPDATE `actg_accounting_transactions` SET actg_group_id = $new_actg_group_id, txn_group_id = $new_txn_group_id WHERE actg_txn_id " . db_create_in($paid_txns));
    $link[0]['text'] = '採購訂單列表';
    $link[0]['href'] = 'erp_order_manage.php?act=order_list';
    $link[1]['text'] = '採購訂單付款紀錄';
    $link[1]['href'] = 'purchase_payments.php?act=list';
    sys_msg('採購訂單付款成功', 0, $link, true);
}
elseif ($_REQUEST['act'] == 'complete')
{
    $is_ajax = empty($_REQUEST['is_ajax']) ? 0 : intval($_REQUEST['is_ajax']);
    $showError = function ($msg) use ($is_ajax) {
        if ($is_ajax)
        {
            make_json_error($msg);
        }
        else
        {
            sys_msg($msg);
        }
    };
    
    /* 权限判断 */
    if ($is_ajax)
    {
        check_authz_json('erp_order_finish');
    }
    else
    {
        admin_priv('erp_order_finish');
    }

    $order_id = empty($_REQUEST['order_id']) ? 0 : intval($_REQUEST['order_id']);
    if (empty($order_id))
    {
        $showError('必須輸入訂單號');
    }

    require_once(ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_order.php');
    require_once(ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_supplier.php');
    $order = get_order_info($order_id);
    if (!$order)
    {
        $showError('找不到該訂單');
    }
    
    $actg->start_transaction();
    
    $ok = $db->query("UPDATE " . $ecs->table('erp_order') . " SET `fully_paid` = 1 WHERE `order_id` = '" . $order_id . "'");
    
    if ($ok)
    {
        $actg->commit_transaction();
        $action_remark = "完成訂單 $order[order_sn]";
        $sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
                "VALUES(" . $order_id . ", " . ERP_COMPLETE . "," . $_SESSION['admin_id'] . ",'" . $action_remark . "')";
        $GLOBALS['db']->query($sql);
        admin_log($order['order_sn'],'complete','erp_order');
        if ($is_ajax)
        {
            make_json_result('', '標示為已付款成功');
        }
        else
        {
            $link[0]['text'] = '返回採購訂單列表';
            $link[0]['href'] = 'erp_order_manage.php?act=order_list';
            
            sys_msg('標示為已付款成功', 0, $link, true);
        }
    }
    else
    {
        $actg->rollback_transaction();
        
        $showError('標示為已付款失敗');
    }
}
elseif ($_REQUEST['act'] == 'delete')
{
    $is_ajax = empty($_REQUEST['is_ajax']) ? 0 : intval($_REQUEST['is_ajax']);
    $showError = function ($msg) use ($is_ajax) {
        if ($is_ajax)
        {
            make_json_error($msg);
        }
        else
        {
            sys_msg($msg);
        }
    };
    
    /* 权限判断 */
    if ($is_ajax)
    {
        check_authz_json('sale_order_stats');
    }
    else
    {
        admin_priv('sale_order_stats');
    }
    
    $purchase_payment_id = empty($_REQUEST['purchase_payment_id']) ? 0 : intval($_REQUEST['purchase_payment_id']);
    if (empty($purchase_payment_id))
    {
        $showError('必須輸入付款編號');
    }
    
    $purchasePayment = $actg->getPurchasePayments(compact('purchase_payment_id'));
    if (empty($purchasePayment))
    {
        $showError('找不到該付款紀錄');
    }

    $actg->start_transaction();

    $ok = $actg->deletePurchasePayment($purchase_payment_id);

    if ($ok)
    {
        $actg->commit_transaction();

        if ($is_ajax)
        {
            make_json_result('', '刪除成功');
        }
        else
        {
            $link[0]['text'] = '採購訂單付款紀錄';
            $link[0]['href'] = 'purchase_payments.php?act=list';

            sys_msg('刪除成功', 0, $link, true);
        }
    }
    else
    {
        $actg->rollback_transaction();

        $showError('刪除失敗');
    }
}
elseif ($_REQUEST['act'] == 'credit')
{
    $sql = "SELECT cn_id, cn_sn, curr_amount FROM " . $ecs->table('erp_credit_note') . " WHERE supplier_id = $_REQUEST[supplier_id] AND curr_amount > 0 ORDER BY curr_amount ASC";
    make_json_result($db->getAll($sql));
}

function month_start()
{
    return local_date('Y-m-01');
}

function month_end()
{
    return local_date('Y-m-t');
}

function get_purchase_payments()
{
    global $controller, $actg, $db;
    
    /* 排序参数 */
    $_REQUEST['sort_by']      = empty($_REQUEST['sort_by']) ? 'purchase_payment_id' : trim($_REQUEST['sort_by']);
    $_REQUEST['sort_order']   = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
    
    /* 时间参数 */
    $_REQUEST['start_date'] = empty($_REQUEST['start_date']) ? month_start() : $_REQUEST['start_date'];
    $_REQUEST['end_date'] = empty($_REQUEST['end_date']) ? month_end() : $_REQUEST['end_date'];
    $start_date = $actg->dbdate(local_strtotime($_REQUEST['start_date']));
    $end_date = $actg->dbdate(local_strtotime($_REQUEST['end_date']) + 86399);
    
    /* 其他參數 */
    $_REQUEST['order_sn'] = empty($_REQUEST['order_sn']) ? '' : trim($_REQUEST['order_sn']);
    $_REQUEST['account'] = empty($_REQUEST['account']) ? 0 : intval($_REQUEST['account']);
    $_REQUEST['payment_type'] = empty($_REQUEST['payment_type']) ? 0 : intval($_REQUEST['payment_type']);
    
    /* 查询数据的条件 */
    $where = "((t.add_date BETWEEN '" . $start_date . "' AND '" . $end_date . "') OR (at.created_at BETWEEN '" . $_REQUEST['start_date'] . "' AND '" . $_REQUEST['end_date'] . " 23:59:59')) " .
        (empty($_REQUEST['order_sn']) ? '' : " AND order_sn LIKE '%" . mysql_like_quote($_REQUEST['order_sn']) . "%' ") .
        (empty($_REQUEST['account']) ? '' : " AND a.account_id = '" . $_REQUEST['account'] . "' ") .
        (empty($_REQUEST['payment_type']) ? '' : " AND t.payment_type_id = '" . $_REQUEST['payment_type'] . "' ");
    
    $sql = "SELECT count(*) " . 
            "FROM `actg_purchase_payments` as pp " .
            "LEFT JOIN" . $GLOBALS['ecs']->table('erp_order') . " as eo ON pp.order_id = eo.order_id " .
			"LEFT JOIN `actg_account_transactions` as t ON pp.txn_id = t.txn_id AND pp.actg_txn_id IS NULL " .
			"LEFT JOIN `actg_accounting_transactions` as at ON pp.actg_txn_id = at.actg_txn_id AND pp.txn_id IS NULL AND at.`related_txn_id` IS NULL AND at.posted != '" . ACTG_POST_REJECTED . "' " .
			"LEFT JOIN `actg_accounts` as a ON ((t.account_id = a.account_id AND pp.actg_txn_id IS NULL) OR (at.actg_type_id = a.actg_type_id AND pp.txn_id IS NULL)) " .
            "WHERE " . $where;
    $_REQUEST['record_count'] = $db->getOne($sql);
    
    /* 查询 */
    $data = $actg->getPurchasePayments($where, array(
        'orderby' => $_REQUEST['sort_by'] . ' ' . $_REQUEST['sort_order'],
        'limit' => $_REQUEST['start'] . ',' . $_REQUEST['page_size']
    ));

    $index = [];
    $admins = $db->getAll("SELECT user_id, user_name FROM " . $GLOBALS['ecs']->table('admin_user'));
    foreach ($data as $key => $row)
    {
        $data[$key]['add_date_formatted'] = local_date($GLOBALS['_CFG']['time_format'], strtotime($row['add_date']));
        $data[$key]['complete_date_formatted'] = local_date($GLOBALS['_CFG']['time_format'], strtotime($row['complete_date']));
        $data[$key]['amount_formatted'] = $actg->money_format($row['amount'], $row['currency_format']);
        $data[$key]['_action'] = $controller->generateCrudActionBtn($row['purchase_payment_id'], '', '', [], ['remove' => ['icon_class' => 'fa fa-trash ','link'=>"javascript:void(0)", 'onclick'=>"deletePurchasePayment(\"$row[purchase_payment_id]\")", 'data', 'btn_class' => 'btn btn-danger', 'btn_size', 'label'=>_L('remove','移除'), 'title'=>_L('remove','移除')]]);
        if($row['cn_amount'] > 0)
        {
            $data[] = array(
                'purchase_payment_id' => $row['purchase_payment_id'],
                'order_id' => $row['order_id'],
                'order_sn' => $row['order_sn'],
                'operator' => $row['operator'],
                'complete_date' => $row['complete_date'],
                'add_date_formatted' => local_date($GLOBALS['_CFG']['time_format'], strtotime($row['add_date'])),
                'complete_date_formatted' => local_date($GLOBALS['_CFG']['time_format'], strtotime($row['complete_date'])),
                'remark' => $row['remark'],
                'account_name' => 'N/A',
                'payment_type_name' => 'Credit Note',
                'amount_formatted' => $actg->money_format($row['cn_amount'], $row['currency_format']),
                '_action' => $controller->generateCrudActionBtn($row['purchase_payment_id'], '', '', [], ['remove' => ['icon_class' => 'fa fa-trash ','link'=>"javascript:void(0)", 'onclick'=>"deletePurchasePayment(\"$row[purchase_payment_id]\")", 'data', 'btn_class' => 'btn btn-danger', 'btn_size', 'label'=>_L('remove','移除'), 'title'=>_L('remove','移除')]])
            );
            $index[] = $key;
        }
        foreach ($admins as $admin) {
            if ($admin['user_id'] == $row['operator'] || strtolower($admin['user_name']) == strtolower($row['operator'])) {
                $data[$key]['operator'] = $admin['user_name'];
                break;
            }
        }
    }
    while(count($index) > 0 && !in_array($_REQUEST['sort_by'],array('payment_type_name','account_name')))
    {
        $out = array_splice($data, count($data) - 1, 1);
        array_splice($data, array_pop($index) + 1, 0, $out);
    }
    
    $arr = array('data' => $data, 'record_count' => $_REQUEST['record_count']);
    
    return $arr;
}

?>