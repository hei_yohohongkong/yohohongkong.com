<?php

    define('IN_ECS', true);
    require(dirname(__FILE__) . '/includes/init.php');

    $goodsShippingDimensionController = new Yoho\cms\Controller\GoodsShippingDimensionController();

    if ($_GET['act'] == 'main' || (!isset($_GET['act'])) && !isset($_POST['act']))
    {
        $goodsShippingDimensionController->showGoodsShippingDimensionImportPage();
    }
    else if (isset($_POST['act']) && $_POST['act'] == 'upload')
    {
        $updateCategoryDefault = isset($_REQUEST['update_cat_default']) ? $_REQUEST['update_cat_default'] : false;
        $goodsShippingDimensionController->uploadGoodsShippingDimensionFile($updateCategoryDefault);
    }
    else if ($_GET['act'] == 'download_category_shipping_dimension_template')
    {
        $goodsShippingDimensionController->downloadCategoryShippingDimensionTemplate();
    }
    else if ($_GET['act'] == 'download_goods_shipping_dimension_template')
    {
        $goodsShippingDimensionController->downloadGoodsShippingDimensionTemplate();
    }
    else if ($_GET['act'] == 'download_goods_with_incomplete_shipping_dimension')
    {
        $goodsShippingDimensionController->getAllGoodsWithIncompleteShippingDimension();
    }
?>
