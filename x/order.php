<?php

/**
 * ECSHOP 订单管理
 * ============================================================================
 * 版权所有 2005-2010 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: yehuaixiao $
 * $Id: order.php 17157 2010-05-13 06:02:31Z yehuaixiao $
 */

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require_once(ROOT_PATH . 'includes/lib_order.php');
require_once(ROOT_PATH . 'includes/lib_payment.php');
require_once(ROOT_PATH . 'includes/lib_goods.php');
require_once(ROOT_PATH . 'includes/lib_erp.php');
require_once(ROOT_PATH . 'includes/lib_i18n.php');
require_once(ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_supplier.php');
require_once(ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_common.php');
require_once(ROOT_PATH . ADMIN_PATH . '/includes/lib_accounting.php');
$userController = new Yoho\cms\Controller\UserController();
$erpController = new Yoho\cms\Controller\ErpController();
$paymentController = new Yoho\cms\Controller\PaymentController();
$orderController = new Yoho\cms\Controller\OrderController();
$deliveryorderController = new Yoho\cms\Controller\DeliveryOrderController();
$orderProgressController = new Yoho\cms\Controller\OrderProgressController();
$addressController    = new Yoho\cms\Controller\AddressController();
$logisticsController = new Yoho\cms\Controller\LogisticsController();
$adminuserController = new Yoho\cms\Controller\AdminuserController();
$packageController = new Yoho\cms\Controller\PackageController();
$smarty->assign('logistics_list', $logisticsController->get_logistics_list(null, true));
$actg = $paymentController->actg;
$actgHooks = $paymentController->actgHooks;
if (!empty($_REQUEST['crm'])) {
    $smarty->assign('crm', 1);
}

/*------------------------------------------------------ */
//-- 订单查询
/*------------------------------------------------------ */
if ($_REQUEST['act'] == 'order_query')
{
    /* 检查权限 */
    admin_priv('order_view');

    /* 载入配送方式 */
    $smarty->assign('shipping_list', shipping_list());

    /* 载入支付方式 */
    $smarty->assign('pay_list', payment_list());

    /* 载入国家 */
    $smarty->assign('country_list', get_regions());

    /* 载入订单状态、付款状态、发货状态 */
    $smarty->assign('os_list', get_status_list('order'));
    $smarty->assign('ps_list', get_status_list('payment'));
    $smarty->assign('ss_list', get_status_list('shipping'));

	// Get sales agent name
	$salesagent =get_sales_agent_name();
	$smarty->assign('salesagents',$salesagent);
	
    /* 模板赋值 */
    $smarty->assign('ur_here', $_LANG['03_order_query']);
    $smarty->assign('action_link', array('href' => 'order.php?act=list', 'text' => $_LANG['02_order_list']));

    /* 显示模板 */
    assign_query_info();
    $smarty->display('order_query.htm');
}

/*------------------------------------------------------ */
//-- 订单列表
/*------------------------------------------------------ */

elseif (in_array($_REQUEST['act'], ['list', 'replacement_list', 'wholesale_list']) )
{
    /* 检查权限 */
    admin_priv('order_view');

    /* 模板赋值 */
    if($_REQUEST['act'] == 'list') {
        $ur_here = $_LANG['02_order_list'];
    }else if($_REQUEST['act'] == 'replacement_list') {
        $ur_here = '換貨單列表';
    }else if($_REQUEST['act'] == 'wholesale_list') {
        $ur_here = '批發訂單列表';
    }
    if($_POST) {
        $smarty->assign('order_query', $_POST);
        $ur_here = '訂單查詢結果';
    }

    $smarty->assign('ur_here', $ur_here);
    $smarty->assign('all_in_stock', $_REQUEST['all_in_stock']);
    $smarty->assign('action_link', array('href' => 'order.php?act=order_query', 'text' => $_LANG['03_order_query']));
    $smarty->assign('action_link2', array('href' => 'order.php?act=mass_delivery&mass_list=1', 'text' => '大量投寄清單'));
    //$smarty->assign('action_link3', array('href' => 'erp_shop_pickup_transfer.php?act=list', 'text' => '門市自取調貨'));
    $smarty->assign('action_link4', array('href' => 'erp_shop_to_warehouse_transfer.php?act=list', 'text' => '調貨(出貨用)'));

    $smarty->assign('order_sn', empty($_REQUEST['order_sn']) ? '' : $_REQUEST['order_sn']);
    $smarty->assign('status_list', $_LANG['cs']);   // 订单状态
    $smarty->assign('selected_status', isset($_REQUEST['composite_status']) ? $_REQUEST['composite_status'] : -1);
    // Operator list
    $salesperson_options = $adminuserController->getAdminsByPriv(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_ORDERMSG, 'name', ['user_name' => 'value']);
    foreach ($salesperson_options as $row) {
        $salespeople[] = $row['name'];
    }
    $smarty->assign('salesperson_list', $salespeople); // 銷售人員
    array_unshift($salesperson_options, array('name'=>'請選擇...','value'=>''));
    $smarty->assign('salesperson_options', $salesperson_options);
    $smarty->assign('language_list', available_language_names());
    $smarty->assign('os_unconfirmed',   OS_UNCONFIRMED);
    $smarty->assign('cs_await_pay',     CS_AWAIT_PAY);
    $smarty->assign('cs_await_ship',    CS_AWAIT_SHIP);
    $smarty->assign('need_purchase',     $_REQUEST['need_purchase']);
    $smarty->assign('includes_goods', empty($_REQUEST['includes_goods']) ? '' : trim($_REQUEST['includes_goods']));
    $smarty->assign('invoice_no', empty($_REQUEST['invoice_no']) ? '' : trim($_REQUEST['invoice_no']));
    $smarty->assign('serial_numbers', empty($_REQUEST['serial_numbers']) ? '' : trim($_REQUEST['serial_numbers']));
    $smarty->assign('replacement', empty($_REQUEST['replacement']) ? 0 : trim($_REQUEST['replacement']));
	// Get sales agent name
	$salesagent = get_sales_agent_name();
	$smarty->assign('salesagents',$salesagent);
    $smarty->assign('users',$userController->getWholesaleUsers());
    $smarty->assign('is_wholesale',(isset($_REQUEST['is_wholesale'])?true:false));
    $smarty->assign('wholesale_receivable',(isset($_REQUEST['wholesale_receivable'])?true:false));
    $order_list = order_list();
    $smarty->assign('order_list',$order_list['orders']);
    $smarty->assign('record_count',$order_list['record_count']);
    $salesagentController = new Yoho\cms\Controller\SalesagentController();;
    $smarty->assign('salesagent_list', $salesagentController->get_active_sales_agents());
    /* 显示模板 */
    assign_query_info();
    $smarty->display('order_list.htm');
}

/*------------------------------------------------------ */
//-- 排序、分页、查询
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'query')
{
    $order_list = order_list();
    // $smarty->assign('order_list',   $order_list['orders']);
    // $smarty->assign('record_count', $order_list['record_count']);
    $orderController->ajaxQueryAction($order_list['orders'], $order_list['record_count'], false);
}

/*------------------------------------------------------ */
//-- 订单详情页面
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'info' || $_REQUEST['act'] == 'info_crm')
{
    /* 根据订单id或订单号查询订单信息 */
    if (isset($_REQUEST['order_id']))
    {
        $order_id = intval($_REQUEST['order_id']);
        $order = order_info($order_id);
    }
    elseif (isset($_REQUEST['order_sn']))
    {
        $order_sn = trim($_REQUEST['order_sn']);
        $order = order_info(0, $order_sn);
    }
    else
    {
        /* 如果参数不存在，退出 */
        die('invalid parameter');
    }

    /* 如果订单不存在，退出 */
    if (empty($order))
    {
        die('order does not exist');
    }

    /* 根据订单是否完成检查权限 */
    if (order_finished($order))
    {
        admin_priv('order_view_finished');
    }
    else
    {
        admin_priv('order_view');
    }

    $class_color_address_type = array(1 => "label-blue", 2 => "label-orange", 99 => "label-gray");
    $class_color_lift_type = array(1 => "label-green", 2 => "label-red", 3 => "label-green-red");

    $address_info_lift = "";
    $class_address_info_lift =  "";
    $address_info_type =  "";
    $class_address_info_type = "";

    if ($order['address_lift'] != 0){
        $address_info_lift = $_LANG['address_type_lift_type_'.$order['address_lift']];
        $class_address_info_lift =  $class_color_lift_type[$order['address_lift']];
    }

    if ($order['cat_type'] != 0){
        $address_info_type =  $_LANG['address_type_'.$order['cat_type']];
        $class_address_info_type =  $class_color_address_type[$order['cat_type']];
    }

    $order['addr_lift'] = $address_info_lift;
    $order['addr_type'] = $address_info_type;
    $order['class_addr_lift'] = $class_address_info_lift;
    $order['class_addr_type'] = $class_address_info_type;
    
    /* 如果管理员属于某个办事处，检查该订单是否也属于这个办事处 */
    $sql = "SELECT agency_id FROM " . $ecs->table('admin_user') . " WHERE user_id = '$_SESSION[admin_id]'";
    $agency_id = $db->getOne($sql);
    if ($agency_id > 0)
    {
        if ($order['agency_id'] != $agency_id)
        {
            sys_msg($_LANG['priv_error']);
        }
    }

    /* 取得上一个、下一个订单号 */
    if (!empty($_COOKIE['ECSCP']['lastfilter']))
    {
        $filter = unserialize(urldecode($_COOKIE['ECSCP']['lastfilter']));
        if (!empty($filter['composite_status']))
        {
            $where = '';
            //综合状态
            switch($filter['composite_status'])
            {
                case CS_AWAIT_PAY :
                    $where .= order_query_sql('await_pay');
                    break;

                case CS_AWAIT_SHIP :
                    $where .= order_query_sql('await_ship');
                    break;

                case CS_FINISHED :
                    $where .= order_query_sql('finished');
                    break;

                case CS_SHIPPED :
                    $where .= order_query_sql('shipped');
                    break;

                default:
                    if ($filter['composite_status'] != -1)
                    {
                        $where .= " AND o.order_status = '$filter[composite_status]' ";
                    }
            }
        }
    }
    $sql = "SELECT MAX(order_id) FROM " . $ecs->table('order_info') . " as o WHERE order_id < '$order[order_id]'";
    if ($agency_id > 0)
    {
        $sql .= " AND agency_id = '$agency_id'";
    }
    if (!empty($where))
    {
        $sql .= $where;
    }
    $smarty->assign('prev_id', $db->getOne($sql));
    $sql = "SELECT MIN(order_id) FROM " . $ecs->table('order_info') . " as o WHERE order_id > '$order[order_id]'";
    if ($agency_id > 0)
    {
        $sql .= " AND agency_id = '$agency_id'";
    }
    if (!empty($where))
    {
        $sql .= $where;
    }
    $smarty->assign('next_id', $db->getOne($sql));

    /* 取得用户名 */
    if ($order['user_id'] > 0)
    {
        $user = user_info($order['user_id']);
        if (!empty($user))
        {
            $order['user_name'] = $user['user_name'];
        }
    }

    /* 取得所有办事处 */
    $sql = "SELECT agency_id, agency_name FROM " . $ecs->table('agency');
    $smarty->assign('agency_list', $db->getAll($sql));

    /* 取得区域名 */
    $sql = "SELECT concat(IFNULL(c.region_name, ''), '  ', IFNULL(p.region_name, ''), " .
                "'  ', IFNULL(t.region_name, ''), '  ', IFNULL(d.region_name, '')) AS region " .
            "FROM " . $ecs->table('order_info') . " AS o " .
                "LEFT JOIN " . $ecs->table('region') . " AS c ON o.country = c.region_id " .
                "LEFT JOIN " . $ecs->table('region') . " AS p ON o.province = p.region_id " .
                "LEFT JOIN " . $ecs->table('region') . " AS t ON o.city = t.region_id " .
                "LEFT JOIN " . $ecs->table('region') . " AS d ON o.district = d.region_id " .
            "WHERE o.order_id = '$order[order_id]'";
    $order['region'] = $db->getOne($sql);

    /* 格式化金额 */
    // $order['total_fee'] = $order['total_fee'] - $order['coupon'] - $order['integral_money'];
    // $order['formated_total_fee']      = price_format($order['total_fee'], false);
    if ($order['order_amount'] < 0)
    {
        $order['money_refund']          = abs($order['order_amount']);
        $refund = Yoho\cms\Controller\RefundController::currentProcessRefundAmount($order['order_id']);
        if ($order['order_status'] == OS_WAITING_REFUND) {
            $order['money_refund_due'] = $refund < $order['money_paid'] ? ($order['money_paid'] - $refund) : 0;
        } else {
            $order['money_refund_due'] = $refund < abs($order['order_amount']) ? abs($order['order_amount'] + $refund) : 0;
        }
        $order['formated_money_refund_due'] = price_format($order['money_refund_due']);
        $order['formated_money_refund'] = price_format(abs($order['order_amount']));
    } elseif ($order['order_type'] == Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE) {
        $refund = Yoho\cms\Controller\RefundController::currentProcessRefundAmount($order['order_id']);
        $order['money_due'] = 0;
        if (bccomp($refund, 0) == 1) {
            $order['money_refund'] = $refund;
            $order['formated_money_refund_due'] = price_format($order['money_refund_due']);
            $order['formated_money_refund'] = price_format(abs($order['money_refund']));
        }
        if (bccomp($order['order_amount'], $order['actg_paid_amount']) == 1) {
            $order['money_due'] = bcsub($order['order_amount'], $order['actg_paid_amount']);
        }
        $order['formated_money_due'] = price_format($order['money_due']);
    }
    $order['formated_cost'] = price_format($order['cp_price']);
    $order['profit'] = $order['money_paid'] + $order['order_amount'] - $order['cp_price'];
	$order['formated_profit'] = price_format($order['profit']);

	// Multi-currency payment
	if ($order['order_amount'] > 0)
	{
        // Get order pay_code
        $sql = "SELECT p.`pay_code` " .
        "FROM " . $ecs->table('order_info') . " as oi " .
        "LEFT JOIN " . $ecs->table('payment') . " as p ON p.pay_id = oi.pay_id " .
        "WHERE oi.order_id = '" . $order['order_id'] . "' LIMIT 1";
        $pay_code = $db->getOne($sql);

        $currency = $actgHooks->getCurrencyForPaymentModule($pay_code);
        if ($currency['currency_rate'] != 1) // Need currency conversion
        {
            $payment_amount = bcdiv($order['order_amount'], $currency['currency_rate']);
            $payment_amount = $actg->money_format($payment_amount, $currency['currency_format']);
            $order['payment_amount'] = $payment_amount;
        }
    }
    if($order['is_wholesale']){
        $order['formated_money_paid'] = price_format($order['actg_paid_amount']);
        $order['payment_amount'] = $order['fully_paid'] ? 0 : price_format($order['order_amount'] - $order['actg_paid_amount']);
    }

	if(!empty($order['user_id'])){//判断是否存在用户
	    $order['huodejifen'] = $db->getOne("SELECT pay_points FROM " .$ecs->table('account_log'). " WHERE change_desc like '%" . mysql_like_quote($order['order_sn']) . "%' and pay_points > 0");//訂單可獲得積分
	    $order['xiaofeijifen'] = $db->getOne("SELECT pay_points FROM " .$ecs->table('users'). " WHERE user_id = '$order[user_id]'");//用戶消费積分餘額
	    $order['zhanghu'] = $db->getOne("SELECT user_money FROM " .$ecs->table('users'). " WHERE user_id = '$order[user_id]'");//用戶消费積分餘額
	}

    /* display button according to admin role */
    $btn_access = array('order_pay', 'order_refund', 'order_cancel', 'order_unpay', 'order_remove', 'order_after_service', 'order_unhold');
    foreach($btn_access as $item)
    {
        if(check_authz($item))
        {
            $smarty->assign($item, 1);
        }
    }

    /* 其他处理 */
    $order['order_time']    = local_date($_CFG['time_format'], $order['add_time']);
    $order['pay_time_timestamp'] = $order['pay_time'];
    $order['pay_time']      = $order['pay_time'] > 0 ?
        local_date($_CFG['time_format'], $order['pay_time']) : $_LANG['ps'][PS_UNPAYED];
    $order['shipping_time'] = $order['shipping_time'] > 0 ?
        local_date($_CFG['time_format'], $order['shipping_time']) : $_LANG['ss'][SS_UNSHIPPED];
    $order['status']        = /*$_LANG['os'][$order['order_status']] . ',' . */$_LANG['ps'][$order['pay_status']] . ' ' . $_LANG['ss'][$order['shipping_status']];
    if (in_array($order['origin_pay_code'], Yoho\cms\Controller\FraudController::PAYMENT_CODES) && $order['pay_time'] > 0) {
        $order['status_top']    = "<a target='_blank' href='fraud_control.php?act=list&order=$order[order_sn]'><span id='risk_level' data-sn='$order[order_sn]' title='載入中'>信用卡盜用風險評級</span></a>";
    }
    // 
    // $do_sql = "SELECT d.invoice_no, d.delivery_id, l.logistics_name FROM ".$ecs->table('delivery_order')." as d ".
    //             " LEFT JOIN ".$ecs->table('logistics')." AS l ON l.logistics_id = d.logistics_id ".
    //             " WHERE d.order_id = $order[order_id] ";
    // $delivery_list = $db->getAll($do_sql);
    $delivery_list = $deliveryorderController->get_order_delivery_info($order[order_id]);

    // handle delivery invoice_no
    if($delivery_list){
        $invoice_link = array();
        foreach($delivery_list as $key => $delivery){
            $update_time = local_date($_CFG['time_format'], $delivery['update_time']);
            $delivery['logistics_name'] = empty($delivery['logistics_name']) ? "未完成出貨的物流單" : $delivery['logistics_name'];
            $link = "<a href='order.php?act=delivery_info&delivery_id=$delivery[delivery_id]' class='display_tracking'>"." ".$update_time ." [". $delivery[logistics_name] .' '. $delivery[invoice_no]."] ".($delivery[is_fls] ? '(FLS)' : '' )." </a>";
            $d_link = " <a target='_blank' href='order.php?act=delivery_print&delivery_id=$delivery[delivery_id]&order_id=$order[order_id]' class='btn btn-xs btn-default'>列印執貨單</a>";
            $invoice_link[] = ' <span class="delivery_info_container"><span class="display_delivery_info" data-oid="'.$order['order_id'].'" data-did="'.$delivery['delivery_id'].'" data-osn="' . $order['order_sn'] . '" data-tid="'.$delivery['invoice_no'].'"  data-added-date="'.$update_time.'" data-delivery-method="'.$delivery[logistics_name] .'" data-shipping-id="'.$delivery[shipping_id] .'" >' . $link . '</span>'.'<div style="display:none;" class="delivery_info_block_outsite"> <div style="display:block" class="delivery_info_block"><div class="goods_info x_panel"></div>'.(($delivery['logistics_name'] == '門店自取') ? '<div class="order_goods_cross_check x_panel"></div>' : '').'<div class="tracking_info x_panel"></div></div></div>'.$d_link.'</span>';
        }
        $order['invoice_no'] = implode("<br>", $invoice_link);
    }
    $order['invoice_no']    = $order['shipping_status'] == SS_UNSHIPPED || $order['shipping_status'] == SS_PREPARING ? $_LANG['ss'][SS_UNSHIPPED] : $order['invoice_no'];

    $pickup_to_buyer = nl2br($order['to_buyer']);
    $smarty->assign('pickup_to_buyer', $pickup_to_buyer);
  
    // get pick and pack users info
    $sql  = "SELECT * FROM " .$ecs->table('order_picker_packer') ." where order_sn = '".$order['order_sn']."' ";
    $picker_packer_result = $db->GetAll($sql);
   
    $order['picker_and_packer'] = '';
  
    foreach ($picker_packer_result as $pp_users) {
        if (!empty($pp_users['packer_user_ids'])) {
            $picker_name_ar = [];
            $picker_ids = explode(',',$pp_users['picker_user_ids']);
            foreach ($picker_ids as $picker_id){
                $temp = $adminuserController->get_user($picker_id);
                if (!empty($temp)) {
                  $picker_name_ar[] = $temp['user_name'];
                } 
            }

            $packer_name_ar = [];
            $packer_ids = explode(',',$pp_users['packer_user_ids']);
            foreach ($packer_ids as $packer_id){
                $temp = $adminuserController->get_user($packer_id);
                if (!empty($temp))  
                $packer_name_ar[] = $temp['user_name'];
            }
            
            $picker_and_packer_str = implode(',',$picker_name_ar) . ' | ' . implode(',',$packer_name_ar) . ' ('.$pp_users['date'].')';
          
            $order['picker_and_packer'] .= (!empty($order['picker_and_packer'])? '<br>' : "") . $picker_and_packer_str;
            // 
            // if (!empty($order['picker_and_packer'])) {
            //     $order['picker_and_packer'] .= '<br>'.$order['picker_and_packer'];
            // }else{
            //     $order['picker_and_packer'] .= $picker_and_packer_str;
            // }
        }
    }


    
    // howang: Get last pay / ship staff name
    // inspired by http://dev.mysql.com/doc/refman/5.5/en/example-maximum-column-group-row.html
    $sql = "SELECT a.operator, a.operation " .
            "FROM " . $ecs->table('admin_operation_log') . " as a " .
            "LEFT JOIN " . $ecs->table('admin_operation_log') . " as b " .
                "ON b.order_id = '".$order['order_id']."' " .
                "AND a.operation = b.operation " .
                "AND a.log_id < b.log_id " .
            "WHERE a.order_id = '".$order['order_id']."' " .
            "AND b.log_id IS NULL";
    $res = $db->getAll($sql);
    foreach ($res as $row)
    {
        if ($row['operation'] == '訂單付款')
        {
            $order['pay_operator'] = $row['operator'];
        }
        else if ($row['operation'] == '訂單發貨')
        {
            $order['shipping_operator'] = $row['operator'];
        }
    }

    // howang: Get ERP delivery id
    $sql = "SELECT delivery_id, delivery_sn FROM " . $ecs->table('erp_delivery') . " WHERE delivery_style_id IN (1,3) AND order_id = '$order[order_id]'";
    $order['erp_delivery'] = $db->getAll($sql);

    // howang: Get platform display name
    $platforms = Yoho\cms\Controller\OrderController::DB_SELLING_PLATFORMS;
    $order['platform_display'] = isset($platforms[$order['platform']]) ? $platforms[$order['platform']] : $order['platform'];

    // howang: Get coupon code
    $sql = "SELECT coupon_id, coupon_code FROM " . $ecs->table('coupons') . " WHERE coupon_id IN (SELECT coupon_id FROM " . $ecs->table('order_coupons') . " WHERE order_id = '" . $order['order_id'] . "')";
    $order['coupon_list'] = $db->getAll($sql);

    /* 取得订单的来源 */
    if ($order['from_ad'] == 0)
    {
        $order['referer'] = empty($order['referer']) ? $_LANG['from_self_site'] : $order['referer'];
    }
    elseif ($order['from_ad'] == -1)
    {
        $order['referer'] = $_LANG['from_goods_js'] . ' ('.$_LANG['from'] . $order['referer'].')';
    }
    else
    {
        /* 查询广告的名称 */
         $ad_name = $db->getOne("SELECT ad_name FROM " .$ecs->table('ad'). " WHERE ad_id='$order[from_ad]'");
         $order['referer'] = $_LANG['from_ad_js'] . $ad_name . ' ('.$_LANG['from'] . $order['referer'].')';
    }

    /* 此订单的发货备注(此订单的最后一条操作记录) */
    $sql = "SELECT action_note FROM " . $ecs->table('order_action').
           " WHERE order_id = '$order[order_id]' AND shipping_status = 1 ORDER BY log_time DESC";
    $order['invoice_note'] = $db->getOne($sql);

    /* 取得订单商品总重量 */
    $weight_price = order_weight_price($order['order_id']);
    $order['total_weight'] = $weight_price['formated_weight'];
    $order['is_wholesale'] = ((in_array($order['user_id'],$userController->getWholesaleUserIds())||(isset($order['order_type']) && $order['order_type']==Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE))?true:false);

    /* 参数赋值：订单 */
    $smarty->assign('order', $order);

    /* 取得用户信息 */
    if ($order['user_id'] > 0)
    {
        /* 用户等级 */
        if ($user['user_rank'] > 0)
        {
            $where = " WHERE rank_id = '$user[user_rank]' ";
        }
        else
        {
            $where = " WHERE min_points <= " . intval($user['rank_points']) . " ORDER BY min_points DESC ";
        }
        $sql = "SELECT rank_name FROM " . $ecs->table('user_rank') . $where;
        $user['rank_name'] = $db->getOne($sql);

        // 用户红包数量
        $day    = getdate();
        $today  = local_mktime(23, 59, 59, $day['mon'], $day['mday'], $day['year']);
        $sql = "SELECT COUNT(*) " .
                "FROM " . $ecs->table('bonus_type') . " AS bt, " . $ecs->table('user_bonus') . " AS ub " .
                "WHERE bt.type_id = ub.bonus_type_id " .
                "AND ub.user_id = '$order[user_id]' " .
                "AND ub.order_id = 0 " .
                "AND bt.use_start_date <= '$today' " .
                "AND bt.use_end_date >= '$today'";
        $user['bonus_count'] = $db->getOne($sql);
        $smarty->assign('user', $user);

        // 地址信息
        $sql = "SELECT * FROM " . $ecs->table('user_address') . " WHERE user_id = '$order[user_id]'";
        $smarty->assign('address_list', $db->getAll($sql));
    }

    /* 取得订单商品及货品 */
    $goods_list = array();
    $goods_attr = array();
    $packages = array();

    $sql = "SELECT o.*, IF(o.cost = 0, g.cost, o.cost) as cost, o.goods_attr, o.extension_code, o.is_package, g.suppliers_id, IFNULL(b.brand_name, '') AS brand_name, repair_sn, repair_id, ur.rank_name, IFNULL(mp.user_price, 0) as vip_price
            FROM " . $ecs->table('order_goods') . " AS o
                LEFT JOIN " . $ecs->table('goods') . " AS g
                    ON o.goods_id = g.goods_id
                LEFT JOIN " . $ecs->table('brand') . " AS b
                    ON g.brand_id = b.brand_id
                LEFT JOIN " . $ecs->table('repair_orders')." AS ro
                    ON ro.order_id=o.order_id AND ro.goods_id=o.goods_id AND ro.is_deleted=0
                LEFT JOIN " . $ecs->table('member_price') . " AS mp
                    ON mp.goods_id = o.goods_id AND mp.user_rank = '$user[user_rank]'
                LEFT JOIN " . $ecs->table('user_rank') . " AS ur
                    ON ur.rank_id = o.user_rank
            WHERE o.order_id = '$order[order_id]'";

    $res = $db->query($sql);

	// If order is sales agent
	$rate = array();
	if ($order['is_sa']){
		$sa_id = $order['type_id'];
		$sa = new Yoho\cms\Model\Salesagent($sa_id);
		$rates = $sa->getRates();
	}
    $sa_total = 0;
    $cant_delivery = array();

    $sql_get_claim_goods = "SELECT o.is_insurance, o.insurance_of, o.goods_price, o.is_package, o.parent_id, o.goods_id  
            FROM " . $ecs->table('order_goods') . " AS o 
            WHERE o.order_id = '$order[order_id]' AND o.is_insurance = '0' AND o.insurance_of = '0' AND o.goods_price > 0 AND o.is_package = '0' AND o.parent_id = '0' ";

    $arr_goods_id_price = [];

    foreach ($db->getAll($sql_get_claim_goods) as $goods){
        $arr_goods_id_price[$goods['goods_id']] = $goods['goods_price'];
    }

    while ($row = $db->fetchRow($res))
    {
        /* 虚拟商品支持 */
        if ($row['is_real'] == 0)
        {
            /* 取得语言项 */
            $filename = ROOT_PATH . 'plugins/' . $row['extension_code'] . '/languages/common_' . $_CFG['lang'] . '.php';
            if (file_exists($filename))
            {
                include_once($filename);
                if (!empty($_LANG[$row['extension_code'].'_link']))
                {
                    $row['goods_name'] = $row['goods_name'] . sprintf($_LANG[$row['extension_code'].'_link'], $row['goods_id'], $order['order_sn']);
                }
            }
        }

        if ($row['is_insurance'] == 1 && $row['insurance_of'] > 0){
            $insurance_of_name_sn = $GLOBALS['db']->getRow("SELECT goods_name, goods_sn FROM ".$GLOBALS['ecs']->table('goods')." WHERE goods_id = '".$row['insurance_of']."' AND goods_id = '".$row['insurance_of']."' AND is_insurance = 0 ");
            if (!empty($insurance_of_name_sn)){
                $row['goods_name'] .= " <br>[".$_LANG['insurance_of'].": ".$insurance_of_name_sn['goods_name']." (".$insurance_of_name_sn['goods_sn'].")]";
                $row['cost'] = $row['goods_price'];
            }
        }

        $row['formated_subtotal']       = price_format($row['goods_price'] * $row['goods_number']);
        $row['formated_goods_price']    = price_format($row['goods_price']);
        $row['formated_cost']           = price_format($row['cost']);
        $row['planned_delivery_date']   = empty($row['planned_delivery_date'])? null : local_date('Y-m-d', $row['planned_delivery_date'] );

        $agency_id=$order['agency_id'];

        $row['storage']=get_goods_stock($row['goods_id'],$row['goods_attr_id'],$agency_id);

        $goods_attr[] = explode(' ', trim($row['goods_attr'])); //将商品属性拆分为一个数组

        if ($row['extension_code'] == 'package_buy')
        {
            $row['storage'] = '';
            $row['brand_name'] = '';
            $row['package_goods_list'] = $packageController->get_package_goods($row['goods_id']);
        }

        if ($row['extension_code'] == 'package')
        {
            if (!in_array($row['is_package'], array_keys($packages))) {
                $packages[$row['is_package']] = $packageController->get_basic_package_info($row['is_package']);
                $packages[$row['is_package']]['qty'] = $row['goods_number'] / $packages[$row['is_package']]['items'][$row['goods_id']]['goods_number'];
                $packages[$row['is_package']]['total_price'] = bcmul($packages[$row['is_package']]['qty'], $packages[$row['is_package']]['base_price'], 2);
                $packages[$row['is_package']]['formated_total_price'] = price_format($packages[$row['is_package']]['total_price'], false);
                $packages[$row['is_package']]['rec_id'] = $row['rec_id'];
            }
            $packages[$row['is_package']]['items'][$row['goods_id']]['qty'] = $row['goods_number'];
            $packages[$row['is_package']]['items'][$row['goods_id']]['seller_note'] = $row['seller_note'];
            $packages[$row['is_package']]['items'][$row['goods_id']]['template_content'] = $row['template_content'];
            $row['package_goods_list'] = $packages[$row['is_package']]['items'];
        }

        //欠货数量（订单的数量减去已发货的数量，就是欠客户的数量）
        $row['lack_qty']= $row['goods_number'] >  $row['send_number']? $row['goods_number']-$row['send_number'] : 0;

        // 如果訂單未發貨, 而庫存少於數量
        if(in_array($order['shipping_status'],[SS_SHIPPED_ING,SS_SHIPPED_PART,SS_UNSHIPPED]) && $order['pay_status'] == PS_PAYED){
            $row['cd_qty'] = $row['storage'] - ($row['goods_number'] - $row['send_number']);
            if($row['cd_qty'] < 0){
                $row['cant_delivery'] = true;
                $cant_delivery[] = $row['goods_name'];
            }
        }

        $row['status_transfer'] = "";
        if (in_array($order['order_status'], array(1, 5, 6)) && in_array($order['shipping_status'], array(0, 3, 4, 5, 6)) && $order['pay_status'] == PS_PAYED){
            $status_transfer = "";
            $order_goods_transfer = $orderController->checkGoodTransferStatus($order['order_id'], $row['goods_id']);
            $temp_style = "";
            if ($order_goods_transfer){
                switch ($order_goods_transfer['status']) {
                    case 0:
                        $status_transfer = "待調貨";
                        $goods_transfer_id = $order_goods_transfer['transfer_id'];
                        $temp_style = "label-default";
                        break;
                    case 1:
                        $status_transfer = "調貨中";
                        $goods_transfer_id = $order_goods_transfer['transfer_id'];
                        $temp_style = "label-warning";
                        break;
                    default:
                }
                if ($status_transfer != ""){
                    $status_transfer = "<a href='../x/erp_warehouse_transfer_review.php?act=list&transfer_id=".$goods_transfer_id."' target='_blank'><div class='label ".$temp_style."' style='display: inline-block; font-size: 100%; font-weight: normal;'>".$status_transfer."</div></a>";
                    $row['status_transfer'] = $status_transfer;
                }
            }
        }

        if ($order['is_sa'] && isset($row['sa_rate_id']))
        {
            $rate_id = $row['sa_rate_id'];
            foreach ($rates as $rate){ //loop rate list
                if ($rate['sa_rate_id'] == $rate_id){ //found rate id
                    // save rate
                    $row['sa_rate'] = $rate['sa_rate'];
                    // count single price with rate
                    $rate_formula = (100 - $row['sa_rate']) / 100;
                    $row['sa_single_price'] = $row['goods_price'] * $rate_formula;
                    // count subtotal
                    $row['sa_subtotal_price'] = $row['sa_single_price'] * $row['goods_number'];
                    $row['formated_sa_subtotal']       = price_format($row['sa_subtotal_price']);
                    $sa_total += $row['sa_subtotal_price'];
                }
            }
        }

        $row['ins_duration'] = $row['ins_claim_times'] = $row['ins_claim_left'] = $row['ins_claim_max'] = 0;

        if ($order['pay_status'] == 2 && $row['is_insurance'] == 1 && $row['insurance_of'] > 0){
            $ins_duration = $orderController->getTnsuranceDurationByInsGoodsId($row['goods_id']);
            if ($ins_duration){
                $is_expired = (strtotime("+".$ins_duration." month", $order['pay_time_timestamp']) > $order['pay_time_timestamp']) ? FALSE : TRUE;
                if (!$is_expired){
                    $ins_claim_times = $orderController->getClaimTimesByOrderIdOrderGoodsId($row['order_id'], $row['rec_id']);
                    $ins_claim_left = $row['goods_number'] - $ins_claim_times;
                    $ins_claim_max = ceil(bcmul($arr_goods_id_price[$row['insurance_of']], '1.25', 2));
                    $row['ins_duration'] = $ins_duration;
                    $row['ins_claim_times'] = $ins_claim_times;
                    $row['ins_claim_left'] = $ins_claim_left;
                    $row['ins_claim_max'] = $ins_claim_max;
                }
            }
        }
    
        $goods_list[] = $row;
    }
    //  show sales agent rate total price
    $formated_sa_total = price_format($sa_total);
    $attr = array();
    $arr  = array();
    foreach ($goods_attr AS $index => $array_val)
    {
        foreach ($array_val AS $value)
        {
            $arr = explode(':', $value);//以 : 号将属性拆开
            $attr[$index][] =  @array('name' => $arr[0], 'value' => $arr[1]);
        }
    }
    $smarty->assign('formated_sa_total', $formated_sa_total);
    $smarty->assign('goods_attr', $attr);
    $smarty->assign('goods_list', $goods_list);
    $smarty->assign('packages', $packages);
    $smarty->assign('cant_delivery', $cant_delivery);
    /* 取得能执行的操作列表 */
    $operable_list = operable_list($order);
    $smarty->assign('operable_list', $operable_list);

    $wilson_logistics_id = $deliveryorderController->getWilsonLogisticsId();

    $smarty->assign('wilson_logistics_id', $wilson_logistics_id);

    /* 取得订单操作记录 */
    $act_list = array();
    $sql = "SELECT * FROM " . $ecs->table('order_action') . " WHERE order_id = '$order[order_id]' ORDER BY log_time DESC,action_id DESC";
    $res = $db->query($sql);
    while ($row = $db->fetchRow($res))
    {
        $row['order_status']    = $_LANG['os'][$row['order_status']];
        $row['pay_status']      = $_LANG['ps'][$row['pay_status']];
        $row['shipping_status'] = $_LANG['ss'][$row['shipping_status']];
        $row['action_time']     = local_date($_CFG['time_format'], $row['log_time']);
        $act_list[] = $row;
    }
    $smarty->assign('action_list', $act_list);

    /* 取得是否存在实体商品 */
    $smarty->assign('exist_real_goods', exist_real_goods($order['order_id']));

    $wholesale_company_list = array ('1' => 'Globiz','2' => 'Yoho Hong Kong');
    $smarty->assign('wholesale_company_list',   $wholesale_company_list);

    $wholesale_customers_list = $orderController->getWholesaleCustomer();

    $smarty->assign('wholesale_customers_list',   $wholesale_customers_list);

    $smarty->assign('class_color_address_type', array(1 => "label-blue", 2 => "label-orange"));
    $smarty->assign('class_color_lift_type', array(1 => "label-green", 2 => "label-red", 3 => "label-green-red"));

    /* 是否打印订单，分别赋值 */
    if (isset($_GET['print']))
    {
        $smarty->assign('shop_name',    $_CFG['shop_name']);
        $smarty->assign('shop_url',     $ecs->url());
        $smarty->assign('shop_address', $_CFG['shop_address']);
        $smarty->assign('service_phone',$_CFG['service_phone']);
        $smarty->assign('print_time',   local_date($_CFG['time_format']));
        $smarty->assign('action_user',  $_SESSION['admin_name']);

        $smarty->template_dir = '../' . DATA_DIR;
        $smarty->display('order_print.html');
    }
    /* 打印快递单 */
    elseif (isset($_GET['shipping_print']))
    {
        //$smarty->assign('print_time',   local_date($_CFG['time_format']));
        //发货地址所在地
        $region_array = array();
        $region_id = !empty($_CFG['shop_country']) ? $_CFG['shop_country'] . ',' : '';
        $region_id .= !empty($_CFG['shop_province']) ? $_CFG['shop_province'] . ',' : '';
        $region_id .= !empty($_CFG['shop_city']) ? $_CFG['shop_city'] . ',' : '';
        $region_id = substr($region_id, 0, -1);
        if (empty($region_id)) $region_id = "''";
        $region = $db->getAll("SELECT region_id, region_name FROM " . $ecs->table("region") . " WHERE region_id IN ($region_id)");
        if (!empty($region))
        {
            foreach($region as $region_data)
            {
                $region_array[$region_data['region_id']] = $region_data['region_name'];
            }
        }
        $smarty->assign('shop_name',    $_CFG['shop_name']);
        $smarty->assign('order_id',    $order_id);
        $smarty->assign('province', $region_array[$_CFG['shop_province']]);
        $smarty->assign('city', $region_array[$_CFG['shop_city']]);
        $smarty->assign('shop_address', $_CFG['shop_address']);
        $smarty->assign('service_phone',$_CFG['service_phone']);
        $shipping = $db->getRow("SELECT * FROM " . $ecs->table("shipping") . " WHERE shipping_id = " . $order['shipping_id']);

        //打印单模式
        if ($shipping['print_model'] == 2)
        {
            /* 可视化 */
            /* 快递单 */
            $shipping['print_bg'] = empty($shipping['print_bg']) ? '' : get_site_root_url() . $shipping['print_bg'];

            /* 取快递单背景宽高 */
            if (!empty($shipping['print_bg']))
            {
                $_size = @getimagesize($shipping['print_bg']);

                if ($_size != false)
                {
                    $shipping['print_bg_size'] = array('width' => $_size[0], 'height' => $_size[1]);
                }
            }

            if (empty($shipping['print_bg_size']))
            {
                $shipping['print_bg_size'] = array('width' => '1024', 'height' => '600');
            }

            /* 标签信息 */
            $lable_box = array();
            $lable_box['t_shop_country'] = $region_array[$_CFG['shop_country']]; //网店-国家
            $lable_box['t_shop_city'] = $region_array[$_CFG['shop_city']]; //网店-城市
            $lable_box['t_shop_province'] = $region_array[$_CFG['shop_province']]; //网店-省份
            $lable_box['t_shop_name'] = $_CFG['shop_name']; //网店-名称
            $lable_box['t_shop_district'] = ''; //网店-区/县
            $lable_box['t_shop_tel'] = $_CFG['service_phone']; //网店-联系电话
            $lable_box['t_shop_address'] = $_CFG['shop_address']; //网店-地址
            $lable_box['t_customer_country'] = $region_array[$order['country']]; //收件人-国家
            $lable_box['t_customer_province'] = $region_array[$order['province']]; //收件人-省份
            $lable_box['t_customer_city'] = $region_array[$order['city']]; //收件人-城市
            $lable_box['t_customer_district'] = $region_array[$order['district']]; //收件人-区/县
            $lable_box['t_customer_tel'] = $order['tel']; //收件人-电话
            $lable_box['t_customer_mobel'] = $order['mobile']; //收件人-手机
            $lable_box['t_customer_post'] = $order['zipcode']; //收件人-邮编
            $lable_box['t_customer_address'] = $order['address']; //收件人-详细地址
            $lable_box['t_customer_name'] = $order['consignee']; //收件人-姓名

            $gmtime_utc_temp = gmtime(); //获取 UTC 时间戳
            $lable_box['t_year'] = date('Y', $gmtime_utc_temp); //年-当日日期
            $lable_box['t_months'] = date('m', $gmtime_utc_temp); //月-当日日期
            $lable_box['t_day'] = date('d', $gmtime_utc_temp); //日-当日日期

            $lable_box['t_order_no'] = $order['order_sn']; //订单号-订单
            $lable_box['t_order_postscript'] = $order['postscript']; //备注-订单
            $lable_box['t_order_best_time'] = $order['best_time']; //送货时间-订单
            $lable_box['t_pigeon'] = '√'; //√-对号
            $lable_box['t_custom_content'] = ''; //自定义内容

            //标签替换
            $temp_config_lable = explode('||,||', $shipping['config_lable']);
            if (!is_array($temp_config_lable))
            {
                $temp_config_lable[] = $shipping['config_lable'];
            }
            foreach ($temp_config_lable as $temp_key => $temp_lable)
            {
                $temp_info = explode(',', $temp_lable);
                if (is_array($temp_info))
                {
                    $temp_info[1] = $lable_box[$temp_info[0]];
                }
                $temp_config_lable[$temp_key] = implode(',', $temp_info);
            }
            $shipping['config_lable'] = implode('||,||',  $temp_config_lable);

            $smarty->assign('shipping', $shipping);

            $smarty->display('print.htm');
        }
        elseif (!empty($shipping['shipping_print']))
        {
            /* 代码 */
            echo $smarty->fetch("str:" . $shipping['shipping_print']);
        }
        else
        {
            $shipping_code = $db->getOne("SELECT shipping_code FROM " . $ecs->table('shipping') . " WHERE shipping_id=" . $order['shipping_id']);
            if ($shipping_code)
            {
                include_once(ROOT_PATH . 'includes/modules/shipping/' . $shipping_code . '.php');
            }

            if (!empty($_LANG['shipping_print']))
            {
                echo $smarty->fetch("str:$_LANG[shipping_print]");
            }
            else
            {
                echo $_LANG['no_print_shipping'];
            }
        }
    }
    else
    {
        /* 模板赋值 */
        $smarty->assign('ur_here', $_LANG['order_info']);
        $smarty->assign('action_link', array('href' => 'order.php?act=list&' . list_link_postfix(), 'text' => $_LANG['02_order_list']));
        
        // Operator list
        $salespeople = $adminuserController->getAdminsByPriv(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_ORDERMSG, 'name');
        $smarty->assign('salesperson_list', $salespeople);

        $sql = "SELECT distinct au.user_name FROM " . $ecs->table('salesperson') . " s left join ".$ecs->table('admin_user')." au on (s.admin_id = au.user_id) WHERE ( is_disable is null or au.is_disable = 0 ) and s.available = 1 and au.user_name != '' order by user_name";
        $salespeople_operator = $db->getCol($sql);
        $smarty->assign('salesperson_list_operator', $salespeople_operator);

        /* 显示模板 */
        assign_query_info();
        $smarty->display('order_' . $_REQUEST['act'] . '.htm');
    }
}

/*------------------------------------------------------ */
//-- 发货单列表
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'delivery_list')
{
    /* 检查权限 */
    admin_priv('delivery_view');

    /* 查询 */
    $result = delivery_list();

    /* 模板赋值 */
    $smarty->assign('ur_here', $_LANG['09_delivery_order']);

    $smarty->assign('os_unconfirmed',   OS_UNCONFIRMED);
    $smarty->assign('cs_await_pay',     CS_AWAIT_PAY);
    $smarty->assign('cs_await_ship',    CS_AWAIT_SHIP);
    $smarty->assign('full_page',        1);

    $smarty->assign('delivery_list',   $result['delivery']);
    $smarty->assign('filter',       $result['filter']);
    $smarty->assign('record_count', $result['record_count']);
    $smarty->assign('page_count',   $result['page_count']);
    $smarty->assign('sort_update_time', '<img src="images/sort_desc.gif">');

    /* 显示模板 */
    assign_query_info();
    $smarty->display('delivery_list.htm');
}

/*------------------------------------------------------ */
//-- 搜索、排序、分页
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'delivery_query')
{
    /* 检查权限 */
    admin_priv('delivery_view');

    $result = delivery_list();

    $smarty->assign('delivery_list',   $result['delivery']);
    $smarty->assign('filter',       $result['filter']);
    $smarty->assign('record_count', $result['record_count']);
    $smarty->assign('page_count',   $result['page_count']);

    $sort_flag = sort_flag($result['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);
    make_json_result($smarty->fetch('delivery_list.htm'), '', array('filter' => $result['filter'], 'page_count' => $result['page_count']));
}

/*------------------------------------------------------ */
//-- 发货单详细
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'delivery_info')
{
    /* 检查权限 */
    admin_priv('delivery_view');

    $delivery_id = intval(trim($_REQUEST['delivery_id']));

    /* 根据发货单id查询发货单信息 */
    if (!empty($delivery_id))
    {
        $delivery_order = $deliveryorderController->delivery_order_info($delivery_id);
    }
    else
    {
        die('order does not exist');
    }

    /* 如果管理员属于某个办事处，检查该订单是否也属于这个办事处 */
    $sql = "SELECT agency_id FROM " . $ecs->table('admin_user') . " WHERE user_id = '" . $_SESSION['admin_id'] . "'";
    $agency_id = $db->getOne($sql);
    if ($agency_id > 0)
    {
        if ($delivery_order['agency_id'] != $agency_id)
        {
            sys_msg($_LANG['priv_error']);
        }

        /* 取当前办事处信息 */
        $sql = "SELECT agency_name FROM " . $ecs->table('agency') . " WHERE agency_id = '$agency_id' LIMIT 0, 1";
        $agency_name = $db->getOne($sql);
        $delivery_order['agency_name'] = $agency_name;
    }
    
    // Operator list
    $salespeople = $adminuserController->getAdminsByPriv(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_ORDERMSG, 'name');
    $smarty->assign('salesperson_list', $salespeople);

    /* 取得訂單資料 */
    if ($delivery_order['order_id'] > 0)
    {
        $order = order_info($delivery_order['order_id']);
        if (!empty($order))
        {
            $smarty->assign('order', $order);
        }
    }

    /* 取得用户名 */
    if ($delivery_order['user_id'] > 0)
    {
        $user = user_info($delivery_order['user_id']);
        if (!empty($user))
        {
            $delivery_order['user_name'] = $user['user_name'];
        }
    }

    /* 取得区域名 */
    $sql = "SELECT concat(IFNULL(c.region_name, ''), '  ', IFNULL(p.region_name, ''), " .
                "'  ', IFNULL(t.region_name, ''), '  ', IFNULL(d.region_name, '')) AS region " .
            "FROM " . $ecs->table('order_info') . " AS o " .
                "LEFT JOIN " . $ecs->table('region') . " AS c ON o.country = c.region_id " .
                "LEFT JOIN " . $ecs->table('region') . " AS p ON o.province = p.region_id " .
                "LEFT JOIN " . $ecs->table('region') . " AS t ON o.city = t.region_id " .
                "LEFT JOIN " . $ecs->table('region') . " AS d ON o.district = d.region_id " .
            "WHERE o.order_id = '" . $delivery_order['order_id'] . "'";
    $delivery_order['region'] = $db->getOne($sql);

    /* 是否保价 */
    $order['insure_yn'] = empty($order['insure_fee']) ? 0 : 1;

    /* 取得发货单商品 */
    $goods_sql = "SELECT *
                  FROM " . $ecs->table('delivery_goods') . "
                  WHERE delivery_id = " . $delivery_order['delivery_id'];
    $goods_list = $GLOBALS['db']->getAll($goods_sql);

    /* 是否存在实体商品 */
    $exist_real_goods = 0;
    if ($goods_list)
    {
        foreach ($goods_list as $value)
        {
            if ($value['is_real'])
            {
                $exist_real_goods++;
            }
        }
    }

    /* 取得订单操作记录 */
    $act_list = array();
    $sql = "SELECT * FROM " . $ecs->table('order_action') . " WHERE order_id = '" . $delivery_order['order_id'] . "' AND action_place = 1 ORDER BY log_time DESC,action_id DESC";
    $res = $db->query($sql);
    while ($row = $db->fetchRow($res))
    {
        $row['order_status']    = $_LANG['os'][$row['order_status']];
        $row['pay_status']      = $_LANG['ps'][$row['pay_status']];
        $row['shipping_status'] = ($row['shipping_status'] == SS_SHIPPED_ING) ? $_LANG['ss_admin'][SS_SHIPPED_ING] : $_LANG['ss'][$row['shipping_status']];
        $row['action_time']     = local_date($_CFG['time_format'], $row['log_time']);
        $act_list[] = $row;
    }
    $smarty->assign('action_list', $act_list);

    $no_tracking_logistics = [7,8,9];

    if (in_array($delivery_order['logistics_id'],$no_tracking_logistics)) {
        $delivery_status = '';
    } else {
        if ($delivery_order['status'] == DO_COLLECTED){
            $delivery_status = '(速遞已收件)';
        } else if ($delivery_order['status'] == DO_DELIVERED) {
            $delivery_status = '(速遞待收件)';
        } else if ($delivery_order['status'] == DO_NORMAL) {
            $delivery_status = '(待出貨)';
        }
    }

    /* 模板赋值 */
    $smarty->assign('delivery_order', $delivery_order);
    $smarty->assign('exist_real_goods', $exist_real_goods);
    $smarty->assign('goods_list', $goods_list);
    $smarty->assign('delivery_id', $delivery_id); // 发货单id
    $smarty->assign('delivery_status', $delivery_status);

    /* 显示模板 */
    $smarty->assign('ur_here', $_LANG['delivery_operate'] . $_LANG['detail']);
    $smarty->assign('action_link', array('href' => 'order.php?act=delivery_list&' . list_link_postfix(), 'text' => $_LANG['09_delivery_order']));
    $smarty->assign('action_act', ($delivery_order['status'] == 2) ? 'delivery_ship' : 'delivery_cancel_ship');
    assign_query_info();
    $smarty->display('delivery_info.htm');
    exit; //
}

/*------------------------------------------------------ */
//-- 執貨單+POS單列印
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'delivery_print')
{
    ini_set('display_errors',0);
    $html = '';
    $order_id = $_REQUEST['order_id'];
    $delivery_id = $_REQUEST['delivery_id'];
    $order_sn_list = array_column($order_sn, 'order_sn');

    assign_order_print($order_id, '', $delivery_id, 1);
    $smarty->template_dir = '../' . DATA_DIR;
    $template_order = $smarty->get_template_vars('order');
    $template = ($template_order['only_market_invoice']) ? 'order_print3.html' : 'order_print2.html';
    $html .= $smarty->fetch($template) .
        '<div style="PAGE-BREAK-AFTER:always"></div>';
    assign_order_print($order_id, '', $delivery_id, 2);
    $smarty->template_dir = '../' . DATA_DIR;
    $template_order = $smarty->get_template_vars('order');
    $template = ($template_order['only_market_invoice']) ? 'order_print3.html' : 'order_print2.html';
    $html .= $smarty->fetch($template) .
        '<div style="PAGE-BREAK-AFTER:always"></div>';
    echo $html;
    exit;
}

/*------------------------------------------------------ */
//-- 发货单发货确认
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'delivery_ship')
{
    /* 检查权限 */
    admin_priv('delivery_view');
    // Operation Log
    if (empty($_REQUEST['operator']))
    {
        sys_msg('訂單出貨必須填寫操作員');
    }
    $delivery_id   = intval(trim($_REQUEST['delivery_id']));
    $deliveryorderController->delivery_ship($order_id, $_REQUEST);
    /* 操作成功 */
    $links[] = array('text' => $_LANG['delivery_sn'] . $_LANG['detail'], 'href' => 'order.php?act=delivery_info&delivery_id=' . $delivery_id);
    $links[] = array('text' => $_LANG['09_delivery_order'], 'href' => 'order.php?act=delivery_list');
    sys_msg($_LANG['act_ok'], 0, $links);
}

/*------------------------------------------------------ */
//-- 发货单取消发货
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'delivery_cancel_ship')
{
    /* 检查权限 */
    admin_priv('delivery_view');

    /* 取得参数 */
    $delivery = '';
    $order_id   = intval(trim($_REQUEST['order_id']));        // 订单id
    $delivery_id   = intval(trim($_REQUEST['delivery_id']));        // 发货单id
    $delivery['invoice_no'] = isset($_REQUEST['invoice_no']) ? trim($_REQUEST['invoice_no']) : '';
    $action_note = isset($_REQUEST['action_note']) ? trim($_REQUEST['action_note']) : '';

    /* 根据发货单id查询发货单信息 */
    if (!empty($delivery_id))
    {
        $delivery_order = $deliveryorderController->delivery_order_info($delivery_id);
    }
    else
    {
        die('order does not exist');
    }

    /* 查询订单信息 */
    $order = order_info($order_id);

    global $db;

    $db->query('START TRANSACTION');

    /* 取消当前发货单物流单号 */
    $_delivery['invoice_no'] = '';
    $_delivery['status'] = DO_NORMAL;
    $query = $db->autoExecute($ecs->table('delivery_order'), $_delivery, 'UPDATE', "delivery_id = $delivery_id", 'SILENT');
    if (!$query)
    {
        /* 操作失败 */
        $db->query('ROLLBACK');

        $links[] = array('text' => $_LANG['delivery_sn'] . $_LANG['detail'], 'href' => 'order.php?act=delivery_info&delivery_id=' . $delivery_id);
        sys_msg($_LANG['act_false'], 1, $links);
        exit;
    }

    /* 修改定单发货单号 */
    // $invoice_no_order = explode('<br>', $order['invoice_no']);
    // $invoice_no_delivery = explode('<br>', $delivery_order['invoice_no']);
    // foreach ($invoice_no_order as $key => $value)
    // {
    //     $delivery_key = array_search($value, $invoice_no_delivery);
    //     if ($delivery_key !== false)
    //     {
    //         unset($invoice_no_order[$key], $invoice_no_delivery[$delivery_key]);
    //         if (count($invoice_no_delivery) == 0)
    //         {
    //             break;
    //         }
    //     }
    // }
    // $_order['invoice_no'] = implode('<br>', $invoice_no_order);

    /* 更新配送状态 */
    $order_finish = $deliveryorderController->get_all_delivery_finish($order_id);
    $shipping_status = ($order_finish == -1) ? SS_SHIPPED_PART : SS_SHIPPED_ING;
    $arr['shipping_status']     = $shipping_status;
    if ($shipping_status == SS_SHIPPED_ING)
    {
        $arr['shipping_time']   = ''; // 发货时间
    }
    // $arr['invoice_no']          = $_order['invoice_no'];
    // if(!update_order($order_id, $arr))
    // {
    //     $db->query('ROLLBACK');

    //     $links[] = array('text' => $_LANG['delivery_sn'] . $_LANG['detail'], 'href' => 'order.php?act=delivery_info&delivery_id=' . $delivery_id);
    //     sys_msg($_LANG['act_false'], 1, $links);
    //     exit;
    // }

    /* 发货单取消发货记录log */
    if(!order_action($order['order_sn'], $order['order_status'], $shipping_status, $order['pay_status'], $action_note, null, 1))
    {
        $db->query('ROLLBACK');

        $links[] = array('text' => $_LANG['delivery_sn'] . $_LANG['detail'], 'href' => 'order.php?act=delivery_info&delivery_id=' . $delivery_id);
        sys_msg($_LANG['act_false'], 1, $links);
        exit;
    }

    /* 发货单全退回时，退回其它 */
    if ($order['order_status'] == SS_SHIPPED_ING)
    {
        /* 如果订单用户不为空，计算积分，并退回 */
        if ($order['user_id'] > 0)
        {
            /* 取得用户信息 */
            $user = user_info($order['user_id']);

            /* 计算并退回积分 */
            $integral = integral_to_give($order);
            if(!log_account_change($order['user_id'], 0, 0, (-1) * intval($integral['rank_points']), (-1) * intval($integral['custom_points']), sprintf($_LANG['return_order_gift_integral'], $order['order_sn']), ACT_OTHER, 0, ['sales_order_id' => $order_id]))
            {
                $db->query('ROLLBACK');

                $links[] = array('text' => $_LANG['delivery_sn'] . $_LANG['detail'], 'href' => 'order.php?act=delivery_info&delivery_id=' . $delivery_id);
                sys_msg($_LANG['act_false'], 1, $links);
                exit;
            }

            /* todo 计算并退回红包 */
            if(!return_order_bonus($order_id))
            {
                $db->query('ROLLBACK');

                $links[] = array('text' => $_LANG['delivery_sn'] . $_LANG['detail'], 'href' => 'order.php?act=delivery_info&delivery_id=' . $delivery_id);
                sys_msg($_LANG['act_false'], 1, $links);
                exit;
            }

            // If the order used a referral coupon, give reward for the referer user
            $coupons = order_coupons($order_id);
            if (!empty($coupons))
            {
                foreach ($coupons as $coupon)
                {
                    if ($coupon['referer_id'] > 0)
                    {
                        $referral_integral = 1000;
                        $log_msg = '由於退貨或未出貨操作，退回推薦新會員 ' . $user['user_name'] . ' 訂單 ' . $order['order_sn'] . ' 贈送的積分';
                        if(!log_account_change($coupon['referer_id'], 0, 0, 0, (-1) * $referral_integral, $log_msg, ACT_OTHER, 0, ['sales_order_id' => $order_id]))
                        {
                            $db->query('ROLLBACK');

                            $links[] = array('text' => $_LANG['delivery_sn'] . $_LANG['detail'], 'href' => 'order.php?act=delivery_info&delivery_id=' . $delivery_id);
                            sys_msg($_LANG['act_false'], 1, $links);
                        }
                    }
                }
            }
        }
    }

    //退回库存（开入库单）
    if($_CFG['stock_dec_time']==0)
    {
        require_once(ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_goods_attr.php');
        require_once(ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_warehouse.php');

        if(!add_stock($delivery_id,$order_id,2))
        {
            $db->query('ROLLBACK');

            $links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=delivery_info&delivery_id=' . $delivery_id);
            sys_msg($_LANG['erp_warehousing_auto_on_cancel_ship_failed'], 1, $links);
        }
    }

    $db->query('COMMIT');

    /* 清除缓存 */
    clear_cache_files();

    /* 操作成功 */
    $links[] = array('text' => $_LANG['delivery_sn'] . $_LANG['detail'], 'href' => 'order.php?act=delivery_info&delivery_id=' . $delivery_id);
    sys_msg($_LANG['act_ok'], 0, $links);
}

/*------------------------------------------------------ */
//-- 退货单列表
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'back_list')
{
    /* 检查权限 */
    admin_priv('back_view');

    /* 模板赋值 */
    $smarty->assign('ur_here', $_LANG['10_back_order']);

    $smarty->assign('os_unconfirmed',   OS_UNCONFIRMED);
    $smarty->assign('cs_await_pay',     CS_AWAIT_PAY);
    $smarty->assign('cs_await_ship',    CS_AWAIT_SHIP);

    /* 显示模板 */
    assign_query_info();
    $smarty->display('back_list.htm');
}

/*------------------------------------------------------ */
//-- 搜索、排序、分页
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'back_query')
{
    /* 检查权限 */
    admin_priv('back_view');

    $result = back_list();
    $orderController->tableName = 'back_order';
    $orderController->ajaxQueryAction($result['back'], $result['record_count'], false);
    // $smarty->assign('back_list',   $result['back']);
    // $smarty->assign('filter',       $result['filter']);
    // $smarty->assign('record_count', $result['record_count']);
    // $smarty->assign('page_count',   $result['page_count']);

    // $sort_flag = sort_flag($result['filter']);
    // $smarty->assign($sort_flag['tag'], $sort_flag['img']);
    // make_json_result($smarty->fetch('back_list.htm'), '', array('filter' => $result['filter'], 'page_count' => $result['page_count']));
}

/*------------------------------------------------------ */
//-- 退货单详细
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'back_info')
{
    /* 检查权限 */
    admin_priv('back_view');

    $back_id = intval(trim($_REQUEST['back_id']));

    /* 根据发货单id查询发货单信息 */
    if (!empty($back_id))
    {
        $back_order = back_order_info($back_id);
    }
    else
    {
        die('order does not exist');
    }

    /* 如果管理员属于某个办事处，检查该订单是否也属于这个办事处 */
    $sql = "SELECT agency_id FROM " . $ecs->table('admin_user') . " WHERE user_id = '$_SESSION[admin_id]'";
    $agency_id = $db->getOne($sql);
    if ($agency_id > 0)
    {
        if ($back_order['agency_id'] != $agency_id)
        {
            sys_msg($_LANG['priv_error']);
        }

        /* 取当前办事处信息*/
        $sql = "SELECT agency_name FROM " . $ecs->table('agency') . " WHERE agency_id = '$agency_id' LIMIT 0, 1";
        $agency_name = $db->getOne($sql);
        $back_order['agency_name'] = $agency_name;
    }

    /* 取得用户名 */
    if ($back_order['user_id'] > 0)
    {
        $user = user_info($back_order['user_id']);
        if (!empty($user))
        {
            $back_order['user_name'] = $user['user_name'];
        }
    }

    /* 取得区域名 */
    $sql = "SELECT concat(IFNULL(c.region_name, ''), '  ', IFNULL(p.region_name, ''), " .
                "'  ', IFNULL(t.region_name, ''), '  ', IFNULL(d.region_name, '')) AS region " .
            "FROM " . $ecs->table('order_info') . " AS o " .
                "LEFT JOIN " . $ecs->table('region') . " AS c ON o.country = c.region_id " .
                "LEFT JOIN " . $ecs->table('region') . " AS p ON o.province = p.region_id " .
                "LEFT JOIN " . $ecs->table('region') . " AS t ON o.city = t.region_id " .
                "LEFT JOIN " . $ecs->table('region') . " AS d ON o.district = d.region_id " .
            "WHERE o.order_id = '" . $back_order['order_id'] . "'";
    $back_order['region'] = $db->getOne($sql);

    /* 是否保价 */
    $order['insure_yn'] = empty($order['insure_fee']) ? 0 : 1;

    /* 取得发货单商品 */
    $goods_sql = "SELECT *
                  FROM " . $ecs->table('back_goods') . "
                  WHERE back_id = " . $back_order['back_id'];
    $goods_list = $GLOBALS['db']->getAll($goods_sql);

    /* 是否存在实体商品 */
    $exist_real_goods = 0;
    if ($goods_list)
    {
        foreach ($goods_list as $value)
        {
            if ($value['is_real'])
            {
                $exist_real_goods++;
            }
        }
    }

    /* 模板赋值 */
    $smarty->assign('back_order', $back_order);
    $smarty->assign('exist_real_goods', $exist_real_goods);
    $smarty->assign('goods_list', $goods_list);
    $smarty->assign('back_id', $back_id); // 发货单id

    /* 显示模板 */
    $smarty->assign('ur_here', $_LANG['back_operate'] . $_LANG['detail']);
    $smarty->assign('action_link', array('href' => 'order.php?act=back_list&' . list_link_postfix(), 'text' => $_LANG['10_back_order']));
    assign_query_info();
    $smarty->display('back_info.htm');
    exit; //
}

/*------------------------------------------------------ */
//-- 修改订单（处理提交）
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'step_post')
{
    /* 检查权限 */
    admin_priv('order_edit');

    /* 取得参数 step */
    $step_list = array('user', 'edit_goods', 'add_goods', 'goods', 'consignee', 'shipping', 'payment', 'other', 'money', 'invoice');
    $step = isset($_REQUEST['step']) && in_array($_REQUEST['step'], $step_list) ? $_REQUEST['step'] : 'user';

    /* 取得参数 order_id */
    $order_id = isset($_REQUEST['order_id']) ? intval($_REQUEST['order_id']) : 0;
    if ($order_id > 0)
    {
        $old_order = order_info($order_id);
    }

    /* 取得参数 step_act 添加还是编辑 */
    $step_act = isset($_REQUEST['step_act']) ? $_REQUEST['step_act'] : 'add';

		$agency_id=get_admin_agency_id($_SESSION['admin_id']);

    /* 插入订单信息 */
    if ('user' == $step)
    {
        /* 取得参数：user_id */
        $user_id = ($_POST['anonymous'] == 1) ? 0 : intval($_POST['user']);
		setcookie("order_add_uid",$user_id,time()+3600*24*7);//UUECS 临时UID 保存7天

        /* 插入新订单，状态为无效 */
        $order = array(
            'user_id'           => $user_id,
            'add_time'          => gmtime(),
            'order_status'      => OS_INVALID,
            'shipping_status'   => SS_UNSHIPPED,
            'pay_status'        => PS_UNPAYED,
            'from_ad'           => 0,
            'referer'           => $_LANG['admin'],
            'agency_id'           => $agency_id
        );

        do
        {
            $order['order_sn'] = get_order_sn();
            if ($db->autoExecute($ecs->table('order_info'), $order, 'INSERT', '', 'SILENT'))
            {
                break;
            }
            else
            {
                if ($db->errno() != 1062)
                {
                    die($db->error());
                }
            }
        }
        while (true); // 防止订单号重复

        $order_id = $db->insert_id();

        /* todo 记录日志 */
        admin_log($order['order_sn'], 'add', 'order');

        /* 插入 pay_log */
        $sql = 'INSERT INTO ' . $ecs->table('pay_log') . " (order_id, order_amount, order_type, is_paid)" .
                " VALUES ('$order_id', 0, '" . PAY_ORDER . "', 0)";
        $db->query($sql);

        /* 下一步 */
        ecs_header("Location: order.php?act=" . $step_act . "&order_id=" . $order_id . "&step=goods\n");
        exit;
    }
    /* 编辑商品信息 */
    elseif ('edit_goods' == $step)
    {
        if (isset($_POST['rec_id']))
        {
            $log = '';
            foreach ($_POST['rec_id'] AS $key => $rec_id)
            {
                /* 取得参数 */
                $goods_price = floatval($_POST['goods_price'][$key]);
                $goods_number = intval($_POST['goods_number'][$key]);
                $goods_attr = $_POST['goods_attr'][$key];

                /* Get old order_goods info */
                $old_sql = "SELECT goods_price, goods_number, goods_attr, goods_name ".
                " FROM " . $ecs->table('order_goods') .
                " WHERE rec_id = '$rec_id' LIMIT 1";
                $old_goods_info = $db->getRow($old_sql);
                if($old_goods_info['goods_price'] != $_POST['goods_price'][$key])
                {
                    $log .= '<br>'.$old_goods_info['goods_name'].' 修改價錢: 由 '.$old_goods_info['goods_price'].' 至 '.$_POST['goods_price'][$key];
                }
                if($old_goods_info['goods_number'] != $_POST['goods_number'][$key])
                {
                    $log .= '<br>'.$old_goods_info['goods_name'].' 修改數量: 由 '.$old_goods_info['goods_number'].' 至 '.$_POST['goods_number'][$key];
                }
                if($old_goods_info['goods_attr'] != $_POST['goods_attr'][$key])
                {
                    $log .= '<br>'.$old_goods_info['goods_name'].' 修改屬性: 由 '.$old_goods_info['goods_attr'].' 至 '.$_POST['goods_attr'][$key];
                }
                /* 修改 */
                $sql = "UPDATE " . $ecs->table('order_goods') .
                        " SET goods_price = '$goods_price', " .
                        "goods_number = '$goods_number', " .
                        "goods_attr = '$goods_attr' " .
                        "WHERE rec_id = '$rec_id' LIMIT 1";
                $db->query($sql);
            }
            $sql = "UPDATE " . $ecs->table('order_goods') .
                    " SET goods_price = '$goods_price', " .
                    "goods_number = '$goods_number', " .
                    "goods_attr = '$goods_attr' " .
                    "WHERE rec_id = '$rec_id' LIMIT 1";
            $db->query($sql);
            
            if(!empty($log))order_log($step, $old_order, $log);
            /* 更新商品总金额和订单总金额 */
            recalculate_order_fee($order_id);
            
            /* todo 记录日志 */
            $sn = $old_order['order_sn'];
            $new_order = order_info($order_id);
            if ($old_order['total_fee'] != $new_order['total_fee'])
            {
                $sn .= ',' . sprintf($_LANG['order_amount_change'], $old_order['total_fee'], $new_order['total_fee']);
            }
            admin_log($sn, 'edit', 'order');

            /* Set edited_goods as true*/
            $_SESSION['edited_goods'] = 1;
        }

        /* 跳回订单商品 */
        ecs_header("Location: order.php?act=" . $step_act . "&order_id=" . $order_id . "&step=goods\n");
        exit;
    }
    /* 添加商品 */
    elseif ('add_goods' == $step)
    {
        /* 取得参数 */
        $goods_id = intval($_POST['goodslist']);
        $goods_price = $_POST['add_price'] != 'user_input' ? floatval($_POST['add_price']) : floatval($_POST['input_price']);
        $sa_rate_id = isset($_POST['sa_rate']) ? intval($_POST['sa_rate']) : 0;
        $goods_attr = '0';
        for ($i = 0; $i < $_POST['spec_count']; $i++)
        {
            if (is_array($_POST['spec_' . $i]))
            {
                $temp_array = $_POST['spec_' . $i];
                $temp_array_count = count($_POST['spec_' . $i]);
                for ($j = 0; $j < $temp_array_count; $j++)
                {
                    $goods_attr .= ',' . $temp_array[$j];
                }
            }
            else
            {
                $goods_attr .= ',' . $_POST['spec_' . $i];
            }
        }

        $goods_number = intval($_POST['add_number']);

        $goods_attr = explode(',',$goods_attr);
        $k   =   array_search(0,$goods_attr);
        unset($goods_attr[$k]);
				$attr_list = implode(',',$goods_attr);

				if(!empty($attr_list))
				{
	        $sql = "SELECT attr_value ".
	            'FROM ' . $GLOBALS['ecs']->table('goods_attr') .
	            "WHERE goods_attr_id in($attr_list)";

	        $res = $db->query($sql);

	        while ($row = $db->fetchRow($res))
	        {
	            $attr_value[] = $row['attr_value'];
	        }

        	$attr_value = implode(",",$attr_value);
        }
       	else{
       		$attr_value='';
       	}

        $agency_id=get_admin_agency_id($_SESSION['admin_id']);

        $stock=get_goods_stock($goods_id,$goods_attr,$agency_id);

        // /* 检查库存 */
        // if ($GLOBALS['_CFG']['use_storage'] == 1 && $goods_number > $stock)
        // {
        //                     $url = "order.php?act=" . $step_act . "&order_id=" . $order_id . "&step=goods";
        //
        //                     //echo '<a href="'.$url.'">'.$_LANG['goods_num_err'].'('.$stock.')' .'</a>';
        //                      sys_msg($_LANG['goods_num_err']);
        //                     exit;
        //
        //                     return false;
        // }

        $sql = "INSERT INTO " . $ecs->table('order_goods') .
                        "(order_id, goods_id, goods_name, goods_sn, goods_number, market_price, " .
                        "goods_price, goods_attr, is_real, extension_code, parent_id, is_gift, goods_attr_id, cost, sa_rate_id) " .
                    "SELECT '$order_id', goods_id, goods_name, goods_sn,".
                        "'$goods_number', market_price, '$goods_price', '" .$attr_value . "', " .
                        "is_real, extension_code, 0, 0 , '".$attr_list."', cost " .", ".$sa_rate_id." ".
                    "FROM " . $ecs->table('goods') .
                    " WHERE goods_id = '$goods_id' LIMIT 1";

        $db->query($sql);
        $new_rec_id = $db->insert_id();
        $new_sql = "SELECT goods_name, goods_number, goods_price "."FROM " . $ecs->table('order_goods') ."WHERE rec_id = ".$new_rec_id." LIMIT 1";
        $new_goods = $db->getRow($new_sql);
        
        /* check can group package */
        //$related_package = $packageController->get_package_goods_list($goods_id)
        //print_r($related_package);


        $log = $new_goods['goods_name']." 數量: ".$new_goods['goods_number'].", 價錢: ".$new_goods['goods_price'];
        order_log($step, $old_order, $log);
        /* 更新商品总金额和订单总金额 */
        recalculate_order_fee($order_id);

        /* todo 记录日志 */
        $sn = $old_order['order_sn'];
        $new_order = order_info($order_id);
        if ($old_order['total_fee'] != $new_order['total_fee'])
        {
            $sn .= ',' . sprintf($_LANG['order_amount_change'], $old_order['total_fee'], $new_order['total_fee']);
        }
        admin_log($sn, 'edit', 'order');

        /* Set edited_goods as true*/
        $_SESSION['edited_goods'] = 1;

        /* 跳回订单商品 */
        ecs_header("Location: order.php?act=" . $step_act . "&order_id=" . $order_id . "&step=goods\n");
        exit;
    }
    /* 商品 */
    elseif ('goods' == $step)
    {
        //print_r($_POST);die("dddd");
        /* 下一步 */
        if (isset($_POST['next']))
        {
            ecs_header("Location: order.php?act=" . $step_act . "&order_id=" . $order_id . "&step=goods\n");//shipping//consignee
            exit;
        }
        /* 完成 */
        elseif (isset($_POST['finish']))
        {
            /* 初始化提示信息和链接 */
            $msgs   = array();
            $links  = array();

            $order = order_info($order_id);
            
            /* */

            /* 檢查訂單會否因為修改商品而完成訂單 */
            if ($order['order_status'] == OS_SPLITED || $order['order_status'] == OS_SPLITING_PART )
            {
                $order_finish = $deliveryorderController->get_all_delivery_finish($order_id);
                $order['order_status']        = ($order_finish == 1) ? OS_CONFIRMED : $order['order_status'];
                update_order($order_id, $order);
                update_pay_log($order_id);
                // order_action($order['order_sn'], $order['order_status'], $order['shipping_status'], $order['pay_status'], '');
            } elseif($order['order_status'] == OS_CONFIRMED && $order['shipping_status'] == SS_SHIPPED_PART) {
                $order_finish = $deliveryorderController->get_all_delivery_finish($order_id);
                $order['order_status']        = ($order_finish == 1) ? OS_CONFIRMED : OS_SPLITING_PART;
                update_order($order_id, $order);
                update_pay_log($order_id);
                // order_action($order['order_sn'], $order['order_status'], $order['shipping_status'], $order['pay_status'], '');
            }
            
            /* 如果已付款，检查金额是否变动，并执行相应操作 */
            handle_order_money_change($order, $msgs, $links);

            /* Clear edited_goods */
            $_SESSION['edited_goods'] = 0;

            $orderController->orderAssignWarehouseId($order_id);

            /* 显示提示信息 */
            if (!empty($msgs))
            {
                sys_msg(join(chr(13), $msgs), 0, $links);
            }
            else
            {
                /* 跳转到订单详情 */
                ecs_header("Location: order.php?act=info&order_id=" . $order_id . "\n");
                exit;
            }
        }
    }
    /* 保存收货人信息 */
    elseif ('consignee' == $step)
    {
        $order = order_info($_REQUEST['order_id']);
        $order_id = $order['order_id'];
        if($_REQUEST['shipping_type'] == 'pickuppoint') {
            $locker_info = $_REQUEST;
            $shipping = shipping_area_info(intval($order['shipping_id']), [intval($order['country'])]);
            $locker_info['shipping_area'] = $shipping['shipping_area_id'];
            $locker_info['shipping']      = $order['shipping_id'];
            $address = $addressController->locker_consignee($order['user_id'], $locker_info, false);
            unset($address['address_id']);
            $consignee = empty($locker_info['locker_consignee']) ? $address['consignee'] : $locker_info['locker_consignee'];
            $email     = empty($locker_info['email']) ? $address['email'] : $locker_info['email'];
            $mobile    = empty($locker_info['mobile']) ? $address['mobile'] : $locker_info['mobile'];
            $tel       = empty($locker_info['locker_tel']) ? $address['tel'] : $locker_info['locker_tel'];
            $address['consignee']    = $consignee;
            $address['email']        = $email;
            $address['mobile']       = $mobile;
            $address['tel']          = $tel;
            $orderController->update_order_address($address, $order_id);
            $order = order_info($_REQUEST['order_id']);
            $address['agency_id'] = get_agency_by_regions(array($address['country'], $address['province'], $address['city'], $address['district']));
            update_order($order_id, $address);
        } else {
            // Address is not update
            if($_POST['need_update_address'] == 0 && !empty($_POST['old_address'])) {
                $order = $_POST;
                $address_info = ['address' => $_POST['old_address'], 'country' => $order['country'], 'province' => $order['province'], 'city' => $order['city'], 'district' => $order['district'], 'zipcode' => $order['zipcode'], 'consignee' => $order['consignee'], 'dest_type' => $order['dest_type'], 'lift' => $order['lift']];
                $address_info['address_type'] = '';
                $orderController->update_order_address($address_info, $order_id);
            } else {
                $order['address_type'] = '';
                $order = $addressController->handle_address_detail($_POST);
                
                $orderController->update_order_address($order, $order_id);
            }
			if ((trim($order['id_doc']) != "") && (strpos($order['id_doc'], '*') === FALSE)){
                $order['id_doc'] = encrypt_str(trim($order['id_doc']));
			} else {
                $old_id_doc = $orderController->getIdDocOrderInfo($order_id);
                if ($old_id_doc){
                    $order['id_doc'] = $old_id_doc;
                } else {
                    $order['id_doc'] = "";
                }
            }
            $order['agency_id'] = get_agency_by_regions(array($order['country'], $order['province'], $order['city'], $order['district']));
            update_order($order_id, $order);
        }
        // Check order update column
        $order = order_info($order_id);
        $change = array_diff($old_order, $order);
        
        $log = [];
        foreach($change as $name => $info) {
            if(in_array($name, ['country',
            'province',
            'city',
            'district',
            'sign_building',
            'street_name',
            'street_num',
            'locality',
            'administrative_area',
            'floor',
            'block',
            'phase',
            'room',
            'hk_area',
            'auto_break',
            'staying_time',
            'province_name',
            'agency_id',
            'area_type',
            'type_code',
            'address_type',
            'ssq',
            'create_at',
            'address_id', 'order_address_id', ])) continue;
            if ($name == "id_doc"){
                $log[] = '變更'.$_LANG['label_id_doc'];
            } else {
                $log[] = _L($name).'變更: '.$old_order[$name].' 改成 '.$order[$name];
            }
        }

        $log = implode('<br>', $log);
        if(!empty($change)) order_log($step, $old_order, $log);

        /* 该订单所属办事处是否变化 */
        $agency_changed = $old_order['agency_id'] != $order['agency_id'];
        /* 更新商品总金额和订单总金额 */
        recalculate_order_fee($order_id);
        /* todo 记录日志 */
        $sn = $old_order['order_sn'];
        admin_log($sn, 'edit', 'order');
        if (isset($_POST['next']))
        {
            /* 下一步 */
            if (exist_real_goods($order_id))
            {
                /* 存在实体商品，去配送方式 */
                ecs_header("Location: order.php?act=" . $step_act . "&order_id=" . $order_id . "&step=goods\n");
                exit;
            }
            else
            {
                /* 不存在实体商品，去支付方式 */
                ecs_header("Location: order.php?act=" . $step_act . "&order_id=" . $order_id . "&step=goods\n");
                exit;
            }
        }
        elseif (isset($_POST['finish']))
        {

            /* 如果是编辑且存在实体商品，检查收货人地区的改变是否影响原来选的配送 */
            if ('edit' == $step_act && exist_real_goods($order_id))
            {
                $order = order_info($order_id);

                /* 取得可用配送方式 */
                $region_id_list = array(
                    $order['country'], $order['province'], $order['city'], $order['district']
                );
                $shipping_list = available_shipping_list($region_id_list);

                /* 判断订单的配送是否在可用配送之内 */
                $exist = false;
                foreach ($shipping_list AS $shipping)
                {
                    if ($shipping['shipping_id'] == $order['shipping_id'])
                    {
                        $exist = true;
                        break;
                    }
                }

                /* 如果不在可用配送之内，提示用户去修改配送 */
                if (!$exist)
                {
                    // 修改配送为空，配送费和保价费为0
                    update_order($order_id, array('shipping_id' => 0, 'shipping_name' => ''));
                    $links[] = array('text' => $_LANG['step']['shipping'], 'href' => 'order.php?act=edit&order_id=' . $order_id . '&step=shipping');
                    sys_msg($_LANG['continue_shipping'], 1, $links);
                }
            }

            /* 初始化提示信息和链接 */
            $msgs   = array();
            $links  = array();
            /* 如果已付款，检查金额是否变动，并执行相应操作 */
            $order = order_info($order_id);
            handle_order_money_change($order, $msgs, $links);

            /* 显示提示信息 */
            if (!empty($msgs))
            {
                sys_msg(join(chr(13), $msgs), 0, $links);
            }

            /* 完成 */
            if ($agency_changed)
            {
                ecs_header("Location: order.php?act=list\n");
            }
            else
            {
                ecs_header("Location: order.php?act=info&order_id=" . $order_id . "\n");
            }
            exit;
        }
    }
    /* 保存配送信息 */
    elseif ('shipping' == $step)
    {
        /* 如果不存在实体商品，退出 */
        if (!exist_real_goods($order_id))
        {
            die ('Hacking Attemp');
        }

        /* 取得订单信息 */
        $order_info = order_info($order_id);
    
        if ($order_info['shipping_status'] == SS_SHIPPED || $order_info['shipping_status'] == SS_RECEIVED)
        {
            sys_msg($_LANG['cannot_edit_order_shipped']);
        }
        $old_shipping_name = $order_info['shipping_name'];
        $region_id_list = array($order_info['country'], $order_info['province'], $order_info['city'], $order_info['district']);
		//$region_id_list = array(1,0,0); // don't know why this is used instead of the line of code above, and it make the system didn't work as expected. therefore I restored the orignal code above
        $shipping_id = $_POST['shipping'];
        $insure      = $_POST['insure'];
        
        /* 保存订单 */
        update_order_shipping($order_id, $region_id_list, $shipping_id, $insure);
        
        // update warehouse id on order_goods
        $orderController->orderAssignWarehouseId($order_id);
        
        /* todo 记录日志 */
        $sn = $order_info['order_sn'];
        $new_order = order_info($order_id);
        if ($order_info['total_fee'] != $new_order['total_fee'])
        {
            $sn .= ',' . sprintf($_LANG['order_amount_change'], $order_info['total_fee'], $new_order['total_fee']);
        }
        admin_log($sn, 'edit', 'order');
        
        if (isset($_POST['next']))
        {
            /* 下一步 */
            ecs_header("Location: order.php?act=" . $step_act . "&order_id=" . $order_id . "&step=goods\n");
            exit;
        }
        elseif (isset($_POST['finish']))
        {
            /* 初始化提示信息和链接 */
            $msgs   = array();
            $links  = array();

            /* 如果已付款，检查金额是否变动，并执行相应操作 */
            $order = order_info($order_id);
            handle_order_money_change($order, $msgs, $links);

            /* 如果是编辑且配送不支持货到付款且原支付方式是货到付款 */
            if ('edit' == $step_act && $shipping['support_cod'] == 0)
            {
                $payment = payment_info($order['pay_id']);
                if ($payment['is_cod'] == 1)
                {
                    /* 修改支付为空 */
                    update_order($order_id, array('pay_id' => 0, 'pay_name' => ''));
                    $msgs[]     = $_LANG['continue_payment'];
                    $links[]    = array('text' => $_LANG['step']['payment'], 'href' => 'order.php?act=' . $step_act . '&order_id=' . $order_id . '&step=payment');
                }
            }

            // update shipment estimated time
            $orderController->orderSetShipmentEstimatedTime($order_id);

            /* 显示提示信息 */
            if (!empty($msgs))
            {
                sys_msg(join(chr(13), $msgs), 0, $links);
            }
            else
            {
                /* 完成 */
                ecs_header("Location: order.php?act=info&order_id=" . $order_id . "\n");
                exit;
            }
        }
    }
    /* 保存支付信息 */
    elseif ('payment' == $step)
    {
        /* 取得支付信息 */
        $pay_id = $_POST['payment'];
        $old_order = order_info($order_id);
        update_order_payment($order_id, $pay_id);
        
        /* todo 记录日志 */
        $sn = $old_order['order_sn'];
        $new_order = order_info($order_id);
        
        if ($old_order['total_fee'] != $new_order['total_fee'])
        {
            $sn .= ',' . sprintf($_LANG['order_amount_change'], $old_order['total_fee'], $new_order['total_fee']);
        }
        admin_log($sn, 'edit', 'order');
        
        
        if (isset($_POST['next']))
        {
            /* 下一步 */
            ecs_header("Location: order.php?act=" . $step_act . "&order_id=" . $order_id . "&step=goods\n");
            exit;
        }
        elseif (isset($_POST['finish']))
        {
            /* 初始化提示信息和链接 */
            $msgs   = array();
            $links  = array();

            /* 如果已付款，检查金额是否变动，并执行相应操作 */
            $order = order_info($order_id);
            handle_order_money_change($order, $msgs, $links);

            /* 显示提示信息 */
            if (!empty($msgs))
            {
                sys_msg(join(chr(13), $msgs), 0, $links);
            }
            else
            {
                /* 完成 */
                ecs_header("Location: order.php?act=info&order_id=" . $order_id . "\n");
                exit;
            }
        }
    }
    elseif ('other' == $step)
    {
        $old_order = order_info($order_id);
        /* 保存订单 */
        $order = array();
        if (isset($_POST['pack']) && $_POST['pack'] > 0)
        {
            $pack               = pack_info($_POST['pack']);
            $order['pack_id']   = $pack['pack_id'];
            $order['pack_name'] = addslashes($pack['pack_name']);
            $order['pack_fee']  = $pack['pack_fee'];
        }
        else
        {
            $order['pack_id']   = 0;
            $order['pack_name'] = '';
            $order['pack_fee']  = 0;
        }
        if (isset($_POST['card']) && $_POST['card'] > 0)
        {
            $card               = card_info($_POST['card']);
            $order['card_id']   = $card['card_id'];
            $order['card_name'] = addslashes($card['card_name']);
            $order['card_fee']  = $card['card_fee'];
            $order['card_message'] = $_POST['card_message'];
        }
        else
        {
            $order['card_id']   = 0;
            $order['card_name'] = '';
            $order['card_fee']  = 0;
            $order['card_message'] = '';
        }
	  // If select sales agent
	  if(isset($_POST['type_sn']) && $_POST['is_sa'] ){
          $order['type_sn']     = $_POST['type_sn'];
	   }
        $order['inv_type']      = $_POST['inv_type'];
        $order['inv_payee']     = $_POST['inv_payee'];
        $order['inv_content']   = $_POST['inv_content'];
        $order['how_oos']       = $_POST['how_oos'];
        $order['postscript']    = $_POST['postscript'];
        $order['serial_numbers']= $_POST['serial_numbers'];
        $order['to_buyer']      = $_POST['to_buyer'];
        update_order($order_id, $order);
        update_order_amount($order_id);
            
        /* 更新 pay_log */
        update_pay_log($order_id);

        /* todo 记录日志 */
        $sn = $old_order['order_sn'];
        $new_order = order_info($order_id);
        admin_log($sn, 'edit', 'order');
        order_log($step, $new_order, '');
        if (isset($_POST['next']))
        {
            /* 下一步 */
            ecs_header("Location: order.php?act=" . $step_act . "&order_id=" . $order_id . "&step=goods\n");
            exit;
        }
        elseif (isset($_POST['finish']))
        {
            /* 完成 */
            ecs_header("Location: order.php?act=info&order_id=" . $order_id . "\n");
            exit;
        }
    }
    elseif ('money' == $step)
    {
        /* 取得订单信息 */
        $old_order = order_info($order_id);
        if ($old_order['user_id'] > 0)
        {
            /* 取得用户信息 */
            $user = user_info($old_order['user_id']);
        }
        
        /* 保存信息 */
        $order                   = $old_order;
        $order['goods_amount']  = $old_order['goods_amount'];
        $order['discount']      = isset($_POST['discount']) && floatval($_POST['discount']) >= 0 ? round(floatval($_POST['discount']), 2) : 0;
        $order['tax']           = round(floatval($_POST['tax']), 2);
        $order['shipping_fee']  = isset($_POST['shipping_fee']) && floatval($_POST['shipping_fee']) >= 0 ? round(floatval($_POST['shipping_fee']), 2) : 0;
        $order['insure_fee']    = isset($_POST['insure_fee']) && floatval($_POST['insure_fee']) >= 0 ? round(floatval($_POST['insure_fee']), 2) : 0;
        $order['pay_fee']       = isset($_POST['pay_fee']) ? round(floatval($_POST['pay_fee']), 2) : 0;
        $order['pack_fee']      = isset($_POST['pack_fee']) && floatval($_POST['pack_fee']) >= 0 ? round(floatval($_POST['pack_fee']), 2) : 0;
        $order['card_fee']      = isset($_POST['card_fee']) && floatval($_POST['card_fee']) >= 0 ? round(floatval($_POST['card_fee']), 2) : 0;
        $order['money_paid']    = $old_order['money_paid'];
        $order['coupon']         = $old_order['coupon'];
        $order['integral']       = isset($_POST['integral']) ? intval($_POST['integral']) : $old_order['integral'];
        $order['integral_money'] = $old_order['integral_money'];
        $order['coupon_codes']   = empty($_POST['coupon_codes']) ? '' : $_POST['coupon_codes'];

        $order = $orderController->calculateOrderFee($order);
        update_order($order_id, $order);
        /* 更新 pay_log */
        update_pay_log($order_id);
        /* Unused by 2017-06-14 by Anthony */
        // // If discount > order_amount, set max discount is order_amount.
        // if($order['discount'] > $order['order_amount'])
        // {
        //     $order['discount'] = max($order['order_amount'], 0);
        // }
        // 
        // $order['order_amount'] -= $order['discount'];
        // 
        // /* 如果扣除discount後, 仍大於 0 */
        // if ($order['order_amount'] > 0)
        // {
        //     /* order_amount補回coupon 及 積分重新計算 */
        //     $order['order_amount'] += $order['coupon'] + $order['integral_money'];
        //     
        //     $coupon_codes = $order['coupon_codes'];
        //     $coupon_codes = array_unique(array_map('strtoupper',array_filter(array_map('trim',explode(',',$coupon_codes)))));
        // 
        //     $coupons_id = array();
        //     $coupons_flat = array();
        //     $coupons_percent = array();
        //     foreach ($coupon_codes as $coupon_code)
        //     {
        //         $coupon = coupon_info(0, $coupon_code);
        //         if ($coupon)
        //         {
        //             $coupons_id[] = $coupon['coupon_id'];
        //             if ($coupon['coupon_amount'] > 0)
        //             {
        //                 $coupons_flat[] = $coupon;
        //             }
        //             else // percentage discount
        //             {
        //                 $coupons_percent[] = $coupon;
        //             }
        //         }
        //     }
        // 
        //     $sql = "SELECT `coupon_id` FROM " . $ecs->table('order_coupons') . " WHERE `order_id` = '" . $order_id . "'";
        //     $old_order_coupons_id = $db->getCol($sql);
        // 
        //     $coupons_changed = false;
        //     foreach ($old_order_coupons_id as $coupon_id)
        //     {
        //         if (!in_array($coupon_id, $coupons_id))
        //         {
        //             $coupons_changed = true;
        //             break;
        //         }
        //     }
        //     foreach ($coupons_id as $coupon_id)
        //     {
        //         if (!in_array($coupon_id, $old_order_coupons_id))
        //         {
        //             $coupons_changed = true;
        //             break;
        //         }
        //     }
        // 
        //     if ($coupons_changed)
        //     {
        //         $db->query("DELETE FROM " . $ecs->table('coupon_log') . " WHERE `order_id` = '" . $order_id . "'");
        //         $db->query("DELETE FROM " . $ecs->table('order_coupons') . " WHERE `order_id` = '" . $order_id . "'");
        // 
        //         if (!empty($coupons_id))
        //         {
        //             $sql = "INSERT INTO " . $ecs->table('coupon_log') .
        //                     "(`coupon_id`, `coupon_code`, `user_id`, `use_time`, `order_id`) " .
        //                     "SELECT coupon_id, coupon_code, '" . $old_order['user_id'] . "', '" . gmtime() . "', '" . $order_id . "' " .
        //                     "FROM " . $ecs->table('coupons') .
        //                     "WHERE coupon_id " . db_create_in($coupons_id);
        //             $db->query($sql);
        //             $sql = "INSERT INTO " . $ecs->table('order_coupons') .
        //                     "(`order_id`, `coupon_id`) " .
        //                     "VALUES " .
        //                     implode(',', array_map(function ($coupon_id) use ($order_id) {
        //                         return "('" . $order_id . "', '" . $coupon_id . "')";
        //                     }, $coupons_id));
        //             $db->query($sql);
        //         }
        //     }
        // 
        //     $order_goods = order_goods($order_id);
        // 
        //     // Process flat discount first, then percentage discount
        //     $order['coupon'] = 0;
        //     foreach ($coupons_flat as $coupon)
        //     {
        //         $order['coupon'] += $coupon['coupon_amount'];
        //     }
        //     foreach ($coupons_percent as $coupon)
        //     {
        //         if ($coupon['cartwide']) // percentage apply to all products in cart
        //         {
        //             $order['coupon'] += floor(($coupon['coupon_discount'] / 100) * $order['goods_amount']);
        //         }
        //         else
        //         {
        //             $coupon_amount = 0;
        //             $coupon_goods = array_reduce($coupon['goods_list'], 'array_merge', array());
        //             foreach ($order_goods as $g)
        //             {
        //                 if (in_array($g['goods_id'], $coupon_goods))
        //                 {
        //                     $coupon_amount += floor(($coupon['coupon_discount'] / 100) * $g['goods_price'] * $g['goods_number']);
        //                 }
        //             }
        //             $order['coupon'] += $coupon_amount;
        //         }
        //     }
        // 
        //     $order['order_amount']  -= $order['coupon'];
        // 
        // 
        //     /* 使用優惠券之后待付款金额仍大于0 */
        //     if ($order['order_amount'] > 0)
        //     {
        //         if ($old_order['user_id'] > 0)
        //         {
        //             /* 如果选择了红包，先使用红包支付 */
        //             if ($_POST['bonus_id'] > 0)
        //             {
        //                 /* TODO 检查红包是否可用 */
        //                 $order['bonus_id']      = $_POST['bonus_id'];
        //                 $bonus                  = bonus_info($_POST['bonus_id']);
        //                 $order['bonus']         = $bonus['type_money'];
        // 
        //                 $order['order_amount']  -= $order['bonus'];
        //             }
        // 
        //             /* 使用红包之后待付款金额仍大于0 */
        //             if ($order['order_amount'] > 0)
        //             {
        //                 /* 如果设置了积分，再使用积分支付 */
        //                 if (isset($_POST['integral']) && intval($_POST['integral']) > 0)
        //                 {
        //                     /* 检查积分是否足够 */
        //                     $order['integral']          = intval($_POST['integral']);
        //                     $order['integral_money']    = value_of_integral(intval($_POST['integral']));
        //                     if ($old_order['integral'] + $user['pay_points'] < $order['integral'])
        //                     {
        //                         sys_msg($_LANG['pay_points_not_enough']);
        //                     }
        // 
        //                     $order['order_amount'] -= $order['integral_money'];
        //                 }
        // 
        //                 if ($order['order_amount'] > 0)
        //                 {
        //                     /* 如果设置了余额，再使用余额支付 */
        //                     if (isset($_POST['surplus']) && floatval($_POST['surplus']) >= 0)
        //                     {
        //                         /* 检查余额是否足够 */
        //                         $order['surplus'] = round(floatval($_POST['surplus']), 2);
        //                         if ($old_order['surplus'] + $user['user_money'] + $user['credit_line'] < $order['surplus'])
        //                         {
        //                             sys_msg($_LANG['user_money_not_enough']);
        //                         }
        // 
        //                         /* 如果红包和积分和余额足以支付，把待付款金额改为0，退回部分积分余额 */
        //                         $order['order_amount'] -= $order['surplus'];
        //                         if ($order['order_amount'] < 0)
        //                         {
        //                             $order['surplus']       += $order['order_amount'];
        //                             $order['order_amount']  = 0;
        //                         }
        //                     }
        //                 }
        //                 else
        //                 {
        //                     /* 如果红包和积分足以支付，把待付款金额改为0，退回部分积分 */
        //                     $order['integral_money']    += $order['order_amount'];
        //                     $order['integral']          = integral_of_value($order['integral_money']);
        //                     $order['order_amount']      = 0;
        //                 }
        //             }
        //             else
        //             {
        //                 /* 如果红包足以支付，把待付款金额设为0 */
        //                 $order['order_amount'] = 0;
        //             }
        //         }
        //     }
        //     else
        //     {
        //         // 優惠券扣除金額不能比訂單總金額多
        //         if ($order['order_amount'] + $order['money_paid'] < 0)
        //         {
        //             $order['coupon'] += $order['order_amount'] + $order['money_paid'];
        //             $order['order_amount'] = 0;
        //         }
        //     }
        // }
        // 
        // update_order($order_id, $order);
        // $new_order = order_info($order_id);
        // /* 最後再計算支付費用 */
        // update_order_payment($order_id, $new_order['pay_id']);
        // /* 更新 pay_log */
        // update_pay_log($order_id);
        // $order['order_amount'] += $order['shipping_fee'] + $order['insure_fee'] + $order['cod_fee'];
        
        /* todo 记录日志 */
        $sn = $old_order['order_sn'];
        $new_order = order_info($order_id);
        if ($old_order['total_fee'] != $new_order['total_fee'])
        {
            $sn .= ',' . sprintf($_LANG['order_amount_change'], $old_order['total_fee'], $new_order['total_fee']);
        }
        admin_log($sn, 'edit', 'order');
        /* Order log */
        handle_order_money_log($old_order, $new_order);
        
        /* 如果余额、积分、红包有变化，做相应更新 */
        update_user_integral_bouns($old_order, $order);

        if (isset($_POST['finish']))
        {
            /* 完成 */
            if ($step_act == 'add')
            {
                /* 订单改为已确认，（已付款） */
                $arr['order_status'] = OS_CONFIRMED;
                $arr['confirm_time'] = gmtime();
                if ($order['order_amount'] <= 0)
                {
                    $arr['pay_status']  = PS_PAYED;
                    $arr['pay_time']    = gmtime();
                }
                update_order($order_id, $arr);
            }

            /* 初始化提示信息和链接 */
            $msgs   = array();
            $links  = array();

            /* 如果已付款，检查金额是否变动，并执行相应操作 */
            $order = order_info($order_id);
            handle_order_money_change($order, $msgs, $links);

            /* 显示提示信息 */
            if (!empty($msgs))
            {
                sys_msg(join(chr(13), $msgs), 0, $links);
            }
            else
            {
                ecs_header("Location: order.php?act=info&order_id=" . $order_id . "\n");
                exit;
            }
        }
    }
    /* 保存发货后的配送方式和发货单号 */
    elseif ('invoice' == $step)
    {
        /* 如果不存在实体商品，退出 */
        if (!exist_real_goods($order_id))
        {
            die ('Hacking Attemp');
        }

        /* 保存订单 */
        $shipping_id    = $_POST['shipping'];
        $shipping       = shipping_info($shipping_id);
        $invoice_no     = trim($_POST['invoice_no']);
        $invoice_no     = str_replace(',', '<br>', $invoice_no);
        $order = array(
            'shipping_id'   => $shipping_id,
            'shipping_name' => addslashes($shipping['shipping_name'])
        );
        update_order($order_id, $order);

        /* todo 记录日志 */
        $sn = $old_order['order_sn'];
        admin_log($sn, 'edit', 'order');

        if (isset($_POST['finish']))
        {
            ecs_header("Location: order.php?act=info&order_id=" . $order_id . "\n");
            exit;
        }
    }
}

/*------------------------------------------------------ */
//-- 修改订单（载入页面）
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'add' || $_REQUEST['act'] == 'edit')
{
    /* 检查权限 */
    admin_priv('order_edit');
    
    //--------------------------------------------------------------------
    //UUECS 获取临时uname
    $order_id = isset($_GET['order_id']) ? intval($_GET['order_id']) : 0;
    if (isset($_COOKIE['order_add_uid'])) {
        $cookies_o_uname = get_user_name($_COOKIE['order_add_uid']);
        $smarty->assign('cookies_o_uid', $_COOKIE['order_add_uid']);
        $order['user_id'] = $_COOKIE['order_add_uid'];
        $smarty->assign('cookies_o_uname', $cookies_o_uname);
        $order = order_info($order_id);
        
        /* 取得用户信息 */
        $user = user_info($order['user_id']);
        /* 计算可用余额 */
        $smarty->assign('available_user_money', $order['surplus'] + $user['user_money']);
        /* 计算可用积分 */
        $smarty->assign('available_pay_points', $order['integral'] + $user['pay_points']);
        /* 取得用户可用红包 */
        $user_bonus = user_bonus($order['user_id'], $order['goods_amount']);
        if ($order['bonus_id'] > 0) {
            $bonus = bonus_info($order['bonus_id']);
            $user_bonus[] = $bonus;
        }
        $smarty->assign('available_bonus', $user_bonus);
    }
    
    /* 查询是否存在实体商品 */
    $exist_real_goods = exist_real_goods($order_id);
    $smarty->assign('exist_real_goods', $exist_real_goods);
    
    /* 取得收货地址列表 */
    if ($order['user_id'] > 0) {
        $smarty->assign('address_list', address_list($order['user_id']));
        
        $address_id = isset($_REQUEST['address_id']) ? intval($_REQUEST['address_id']) : 0;
        if ($address_id > 0) {
            $address = address_info($address_id);
            if ($address) {
                foreach ($address as $key => $value) {
                    $order[$key] = $value;
                }
                $smarty->assign('order', $order);
            }
        }
    }

    $stage = isset($_GET['stage']) ? ($_GET['stage']) : FALSE;
    
    if ($exist_real_goods) {
        /* 取得国家 */
        $smarty->assign('country_list', get_regions());
        if ($order['country'] > 0) {
            /* 取得省份 */
            $smarty->assign('province_list', get_regions(1, $order['country']));
            if ($order['province'] > 0) {
                /* 取得城市 */
                $smarty->assign('city_list', get_regions(2, $order['province']));
                if ($order['city'] > 0) {
                    /* 取得区域 */
                    $smarty->assign('district_list', get_regions(3, $order['city']));
                }
            }
        }
    }
    
    //----------------------------------
    /* 取得可用的支付方式列表 */
    if (exist_real_goods($order_id)) {
        $order = order_info($order_id);
        /* 存在实体商品 */
        $region_id_list = array(
            $order['country'], $order['province'], $order['city'], $order['district']
        );
        $shipping_area = shipping_area_info($order['shipping_id'], $region_id_list);
        $pay_fee = ($shipping_area['support_cod'] == 1) ? $shipping_area['pay_fee'] : 0;
        
        $filter_data = $order;
        if ($order['platform'] == "pos") $filter_data['is_pos'] = true;
        else if ($order['platform'] == "pos_exhibition") $filter_data['is_ex_pos'] = true;
        else $filter_data['is_cms'] = true;
        
        
        $payment_list = $paymentController->available_payment_list($shipping_area['support_cod'], $pay_fee, false, $filter_data);
    } else {
        /* 不存在实体商品 */
        $payment_list = $paymentController->available_payment_list(false);
    }
    
    /* 过滤掉使用余额支付 */
    foreach ($payment_list as $key => $payment) {
        if ($payment['pay_code'] == 'balance') {
            unset($payment_list[$key]);
        }
    }
    $smarty->assign('payment_list', $payment_list);
    //------------------------------------
    /* 取得可用的配送方式列表 */
    $region_id_list = array(
        $order['country'], $order['province'], $order['city'], $order['district']
    );
    $region_id_list = array(1, 0, 0);
    $shipping_list = available_shipping_list($region_id_list);
    
    /* 取得配送费用 */
    $total = order_weight_price($order_id);
    foreach ($shipping_list AS $key => $shipping) {
        $shipping_fee = shipping_fee($shipping['shipping_code'],
            unserialize($shipping['configure']), $total['weight'], $total['amount'], $total['number'], $total['price']);
        $shipping_list[$key]['shipping_fee'] = $shipping_fee;
        $shipping_list[$key]['format_shipping_fee'] = price_format($shipping_fee);
        $shipping_list[$key]['free_money'] = price_format($shipping['configure']['free_money']);
    }
    $smarty->assign('shipping_list', $shipping_list);
    
    // Get sales agent list
    $salesagentController = new Yoho\cms\Controller\SalesagentController();
    $salesagent = $salesagentController->get_active_sales_agents();
    $smarty->assign('salesagents', $salesagent);
    //------------------------------------
    //--------------------------------------------------------------------
    //--------------------------------------------------------------------

    /* 取得参数 order_id */
    $order_id = isset($_GET['order_id']) ? intval($_GET['order_id']) : 0;
    $smarty->assign('order_id', $order_id);

    /* 取得参数 step */
    $step_list = array('user', 'goods', 'consignee', 'shipping', 'payment', 'other', 'money');
    $step = isset($_GET['step']) && in_array($_GET['step'], $step_list) ? $_GET['step'] : 'user';
    $smarty->assign('step', $step);

    /* 取得参数 act */
    $act = $_GET['act'];
    $smarty->assign('ur_here',$_LANG['add_order']);
    $smarty->assign('step_act', $act);

    /* 取得订单信息 */
    if ($order_id > 0)
    {
        $order = order_info($order_id);
        $smarty->assign('ur_here',$_LANG['edit_order']);
        /* 发货单格式化 */
        // $order['invoice_no'] = str_replace('<br>', ',', $order['invoice_no']);

        /* 如果已发货，就不能修改订单了（配送方式和发货单号除外） */
        if (($order['shipping_status'] == SS_SHIPPED && ('consignee' != $step || $stage != "edit_ship_addr_tpye")) || $order['shipping_status'] == SS_RECEIVED)
        {
            // howang: also allow edit "其他信息"
            //if ($step != 'shipping')
            if (!in_array($step, array('shipping', 'other')))
            {
                sys_msg($_LANG['cannot_edit_order_shipped']);
            }
            //else
            elseif ($step == 'shipping')
            {
                $step = 'invoice';
                $smarty->assign('step', $step);
            }
        } else if ($step == 'consignee' && $stage == "edit_ship_addr_type") {
            $addr_id = isset($_GET['addr_id']) ? ($_GET['addr_id']) : FALSE;
            if (!$addr_id || $addr_id != $order['address_id']){
                sys_msg($_LANG['cannot_address_list']);
            }
        }
        
        
        if (in_array($order['order_status'], [OS_CANCELED, OS_INVALID, OS_WAITING_REFUND])) {
            if (!empty($order['ar_txn_id'])) {
                sys_msg('無法更改此訂單');
            }
        }
        if ($order['order_type'] == Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE) {
            if (!in_array($step, array('shipping', 'other')))
            {
                if (!empty($order['ar_txn_id'])) {
                    sys_msg('無法更改此訂單');
                }
            }
        }

        if (isset($order['order_type']) && $order['order_type'] == Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT)
        $sa_id = $order['type_id'];
    
    	if(!empty($sa_id)){
    
    		$salesagent = new  Yoho\cms\Model\Salesagent($sa_id);
    		$rates = $salesagent->getRates();
    
    		$smarty->assign('sa_rates', $rates);
    	}
        if($order['user_id']> 0 ){
            $user = user_info($order['user_id']);
        }
        $user_rank = empty($user['user_rank']) ? 0 : $user['user_rank'];
        $smarty->assign('user_rank', $user_rank);
        $smarty->assign('order', $order);
    }
    else
    {
        if ($act != 'add' || $step != 'user')
        {
            die('invalid params');
        }
    }
    
    /* 选择会员 */
    if ('user' == $step)
    {
        // 无操作
    }

    /* 增删改商品 */
    elseif ('goods' == $step)
    {
        /* 取得订单商品 */
        $goods_list = order_goods($order_id,false);
        if (!empty($goods_list))
        {
            foreach ($goods_list AS $key => $goods)
            {
                /* 计算属性数 */
                $attr = $goods['goods_attr'];
                if ($attr == '')
                {
                    $goods_list[$key]['rows'] = 1;
                }
                else
                {
                    $goods_list[$key]['rows'] = count(explode(chr(13), $attr));
                }
            }
        }

        if($order['user_id']> 0 ){
            $user = user_info($order['user_id']);
        }
        $user_rank = empty($user['user_rank']) ? 0 : $user['user_rank'];
        $smarty->assign('user_rank', $user_rank);
        $smarty->assign('goods_list', $goods_list);

        /* 取得商品总金额 */
        $smarty->assign('goods_amount', order_amount($order_id));
    }

    // 设置收货人
    elseif ('consignee' == $step)
    {
        $controller = new Yoho\cms\Controller\YohoBaseController();
        /* 查询是否存在实体商品 */
        $exist_real_goods = exist_real_goods($order_id);
        $sql = "SELECT st.shipping_type_code " .
        "FROM " . $ecs->table('shipping') . " as s " .
        "LEFT JOIN " . $ecs->table('shipping_type') . " as st ON st.shipping_type_id = s.shipping_type_id " .
        " WHERE s.shipping_id='" . $order['shipping_id'] . "'";
        $shipping_type = $db->getOne($sql);
        $order['shipping_type'] = $shipping_type;
        if ($order['id_doc'] != ""){
            $order['id_doc'] = decrypt_str($order['id_doc']);
        }
        $order['shipping_type'] = $shipping_type;
        $smarty->assign('order', $order);
        $smarty->assign('exist_real_goods', $exist_real_goods);

        /* 取得收货地址列表 */
        if ($order['user_id'] > 0)
        {
            $smarty->assign('address_list', address_list($order['user_id']));

            $address_id = isset($_REQUEST['address_id']) ? intval($_REQUEST['address_id']) : 0;
            if ($address_id > 0)
            {
                $address = address_info($address_id);
                if ($address)
                {
                    foreach ($address as $key => $value) {
                        $order[$key] = $value;
                    }
                    $smarty->assign('order', $order);
                }
            }
        }

        if ($exist_real_goods)
        {
            /* 取得国家 */
            $smarty->assign('country_list', get_regions());
            if ($order['country'] > 0)
            {
                /* 取得省份 */
                $smarty->assign('province_list', get_regions(1, $order['country']));
                if ($order['province'] > 0)
                {
                    /* 取得城市 */
                    $smarty->assign('city_list', get_regions(2, $order['province']));
                    if ($order['city'] > 0)
                    {
                        /* 取得区域 */
                        $smarty->assign('district_list', get_regions(3, $order['city']));
                    }
                }
            }
        }
        $addr_type = $addressController->get_address_types();
        array_unshift($addr_type, array("type_id" => "0", "type_cat" => "0","type_code" => "0","type_name" => "未選擇"));

        $lift_type_no = array(1, 2, 3);
        $lift_type = array('未選擇');
        foreach ($lift_type_no as $type){
            $type_txt = $_LANG['address_type_lift_type_'.$type];
            array_push($lift_type, $type_txt);
        }
        $smarty->assign('lift_type', $lift_type);
        $smarty->assign('addr_type', $addr_type);
    }

    // 选择配送方式
    elseif ('shipping' == $step)
    {
        /* 如果不存在实体商品 */
        if (!exist_real_goods($order_id))
        {
            die ('Hacking Attemp');
        }

        /* 取得可用的配送方式列表 */
        $region_id_list = array(
            $order['country'], $order['province'], $order['city'], $order['district']
        );
        $shipping_list = available_shipping_list($region_id_list);

        /* 取得配送费用 */
        $total = order_weight_price($order_id);
        foreach ($shipping_list AS $key => $shipping)
        {
            $shipping_fee = shipping_fee($shipping['shipping_code'],
                unserialize($shipping['configure']), $total['weight'], $total['amount'], $total['number'], $total['price']);
            $shipping_list[$key]['shipping_fee'] = $shipping_fee;
            $shipping_list[$key]['format_shipping_fee'] = price_format($shipping_fee);
            $shipping_list[$key]['free_money'] = price_format($shipping['configure']['free_money']);
        }
        $smarty->assign('shipping_list', $shipping_list);
    }

    // 选择支付方式
    elseif ('payment' == $step)
    {
        if ($order['pay_status'] == PS_PAYING || $order['pay_status'] == PS_PAYED)
        {
            sys_msg($_LANG['cannot_edit_order_payed']);
        }
        if ($order['money_paid'] > 0) {
            sys_msg('訂單已經有已付款金額, 不能修改');
        }
        /* 取得可用的支付方式列表 */
        if (exist_real_goods($order_id))
        {
            /* 存在实体商品 */
            $region_id_list = array(
                $order['country'], $order['province'], $order['city'], $order['district']
            );
            $shipping_area = shipping_area_info($order['shipping_id'], $region_id_list);
            $pay_fee = ($shipping_area['support_cod'] == 1) ? $shipping_area['pay_fee'] : 0;

            $filter_data = $order;
            if($order['platform'] == "pos") $filter_data['is_pos'] = true;
            else if($order['platform'] == "pos_exhibition") $filter_data['is_ex_pos'] = true;
            else $filter_data['is_cms'] = true;

            $payment_list = $paymentController->available_payment_list($shipping_area['support_cod'], $pay_fee, false, $filter_data);
        }
        else
        {
            /* 不存在实体商品 */
            $payment_list = $paymentController->available_payment_list(false);
        }

        /* 过滤掉使用余额支付 */
        foreach ($payment_list as $key => $payment)
        {
            if ($payment['pay_code'] == 'balance')
            {
                unset($payment_list[$key]);
            }
        }
        $smarty->assign('payment_list', $payment_list);
    }

    // 选择包装、贺卡
    elseif ('other' == $step)
    {
        /* 查询是否存在实体商品 */
        $exist_real_goods = exist_real_goods($order_id);
        $smarty->assign('exist_real_goods', $exist_real_goods);

        if ($exist_real_goods)
        {
            /* 取得包装列表 */
            $smarty->assign('pack_list', pack_list());

            /* 取得贺卡列表 */
            $smarty->assign('card_list', card_list());
        }
    }

    // 费用
    elseif ('money' == $step)
    {
        /* 查询是否存在实体商品 */
        $exist_real_goods = exist_real_goods($order_id);
        $smarty->assign('exist_real_goods', $exist_real_goods);

        /* 取得用户信息 */
        if ($order['user_id'] > 0)
        {
            $user = user_info($order['user_id']);

            /* 计算可用余额 */
            $smarty->assign('available_user_money', $order['surplus'] + $user['user_money']);

            /* 计算可用积分 */
            $smarty->assign('available_pay_points', $order['integral'] + $user['pay_points']);

            /* 取得用户可用红包 */
            $user_bonus = user_bonus($order['user_id'], $order['goods_amount']);
            if ($order['bonus_id'] > 0)
            {
                $bonus = bonus_info($order['bonus_id']);
                $user_bonus[] = $bonus;
            }
            $smarty->assign('available_bonus', $user_bonus);
            $smarty->assign('order', $order);
            $user_rank = empty($user['user_rank']) ? 0 : $user['user_rank'];
        }

        // Load associated coupons into $order
        $order['coupon_list'] = order_coupons($order_id);
        $smarty->assign('order', $order);

        /* 取得可用優惠券 */
        $available_coupons = available_coupons($order, order_amount($order_id), order_goods($order_id, false));
        $smarty->assign('available_coupons', $available_coupons);
    }

    // 发货后修改配送方式和发货单号
    elseif ('invoice' == $step)
    {
        if ($order['shipping_status'] == SS_SHIPPED || $order['shipping_status'] == SS_RECEIVED)
        {
            sys_msg($_LANG['cannot_edit_order_shipped']);
        }
    }

    /* 显示模板 */
    assign_query_info();
    $smarty->display('order_step.htm');
}

/*------------------------------------------------------ */
//-- 处理
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'process')
{
    /* 取得参数 func */
    $func = isset($_GET['func']) ? $_GET['func'] : '';

    /* 删除订单商品 */
    if ('drop_order_goods' == $func)
    {
        /* 检查权限 */
        admin_priv('order_edit');

        /* 取得参数 */
        $rec_id = intval($_GET['rec_id']);
        $step_act = $_GET['step_act'];
        $order_id = intval($_GET['order_id']);
        
        $gn_sql = "SELECT goods_name FROM " . $ecs->table('order_goods') .
                " WHERE rec_id = '$rec_id' LIMIT 1";
        $goods_name = $db->getOne($gn_sql);

        $gn_sql = "SELECT is_package FROM " . $ecs->table('order_goods') .
                " WHERE rec_id = '$rec_id' LIMIT 1";
        $package_id = $db->getOne($gn_sql);

        /* 删除 */
        $sql = "DELETE FROM " . $ecs->table('order_goods') .
                " WHERE rec_id = '$rec_id' LIMIT 1";
        $db->query($sql);
        
        if($package_id){
            $sql = "DELETE FROM " . $ecs->table('order_goods') .
                " WHERE is_package = '$package_id' and order_id = '$order_id'";
            $db->query($sql);
        }

        $order = order_info($order_id);
        order_log('drop_order_goods', $order, $goods_name);

        /* 更新商品总金额和订单总金额 */
        recalculate_order_fee($order_id);

        /* Set edited_goods as true*/
        $_SESSION['edited_goods'] = 1;
        
        /* 跳回订单商品 */
        ecs_header("Location: order.php?act=" . $step_act . "&order_id=" . $order_id . "&step=goods\n");
        exit;
    }

    /* 取消刚添加或编辑的订单 */
    elseif ('cancel_order' == $func)
    {
        $step_act = $_GET['step_act'];
        $order_id = isset($_GET['order_id']) ? intval($_GET['order_id']) : 0;
        if ($step_act == 'add')
        {
            /* 如果是添加，删除订单，返回订单列表 */
            if ($order_id > 0)
            {
                $sql = "DELETE FROM " . $ecs->table('order_info') .
                        " WHERE order_id = '$order_id' LIMIT 1";
                $db->query($sql);
            }
            ecs_header("Location: order.php?act=list\n");
            exit;
        }
        else
        {
            /* 如果是编辑，返回订单信息 */
            ecs_header("Location: order.php?act=info&order_id=" . $order_id . "\n");
            exit;
        }
    }

    /* 编辑订单时由于订单已付款且金额减少而退款 */
    elseif ('refund' == $func)
    {
        /* 处理退款 */
        $order_id       = $_REQUEST['order_id'];
        $refund_type    = $_REQUEST['refund'];
        $refund_note    = $_REQUEST['refund_note'];
        $refund_amount  = $_REQUEST['refund_amount'];
        $order          = order_info($order_id);
        order_refund($order, $refund_type, $refund_note, $refund_amount);

        $actgHooks->orderUnpaid($order_id, $refund_amount);

        /* 修改应付款金额为0，已付款金额减少 $refund_amount */
        update_order($order_id, array('order_amount' => 0, 'money_paid' => $order['money_paid'] - $refund_amount));
        $log = "原已付金額: ".$order['money_paid']. ", 退款: ". $refund_amount;
        order_log('refund', $order, $log);
        /* 返回订单详情 */
        ecs_header("Location: order.php?act=info&order_id=" . $order_id . "\n");
        exit;
    }

    /* 载入退款页面 */
    elseif ('load_refund' == $func)
    {
        $refund_amount = !empty($_REQUEST['refund_amount']) ? floatval($_REQUEST['refund_amount']) : 0;
        $smarty->assign('refund_amount', $refund_amount);
        $smarty->assign('formated_refund_amount', price_format($refund_amount));

        $anonymous = $_REQUEST['anonymous'];
        $smarty->assign('anonymous', $anonymous); // 是否匿名

        $order_id = intval($_REQUEST['order_id']);
        $refundController = new Yoho\cms\Controller\RefundController();
        $refundController->assignRefundInfo("", $order_id, $refund_amount);

        /* 显示模板 */
        $smarty->assign('ur_here', $_LANG['refund']);
        assign_query_info();
        $smarty->display('order_refund.htm');
    }

    else
    {
        die('invalid params');
    }
}

/*------------------------------------------------------ */
//-- 合并订单
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'merge')
{
    /* 检查权限 */
    admin_priv('order_os_edit');

    /* 取得满足条件的订单 */
    $sql = "SELECT o.order_sn, u.user_name " .
            "FROM " . $ecs->table('order_info') . " AS o " .
                "LEFT JOIN " . $ecs->table('users') . " AS u ON o.user_id = u.user_id " .
            "WHERE o.user_id > 0 " .
            "AND o.extension_code = '' " . order_query_sql('unprocessed');
    $smarty->assign('order_list', $db->getAll($sql));

    /* 模板赋值 */
    $smarty->assign('ur_here', $_LANG['04_merge_order']);
    $smarty->assign('action_link', array('href' => 'order.php?act=list', 'text' => $_LANG['02_order_list']));

    /* 显示模板 */
    assign_query_info();
    $smarty->display('merge_order.htm');
}

/*------------------------------------------------------ */
//-- 订单打印模板（载入页面）
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'templates')
{
    /* 检查权限 */
    admin_priv('order_os_edit');

    /* 读入订单打印模板文件 */
    $file_path    = ROOT_PATH. DATA_DIR . '/order_print.html';
    $file_content = file_get_contents($file_path);
    @fclose($file_content);

    /* 模板赋值 */
    $smarty->assign('file_content', $file_content);
    $smarty->assign('ur_here',      $_LANG['edit_order_templates']);
    $smarty->assign('action_link',  array('href' => 'order.php?act=list', 'text' => $_LANG['02_order_list']));
    $smarty->assign('act', 'edit_templates');

    /* 显示模板 */
    assign_query_info();
    $smarty->display('order_templates.htm');
}
/*------------------------------------------------------ */
//-- 订单打印模板（提交修改）
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'edit_templates')
{
    /* 更新模板文件的内容 */
    $file_name = @fopen('../' . DATA_DIR . '/order_print.html', 'w+');
    @fwrite($file_name, stripslashes($_POST['file_content']));
    @fclose($file_name);

    /* 提示信息 */
    $link[] = array('text' => $_LANG['back_list'], 'href'=>'order.php?act=list');
    sys_msg($_LANG['edit_template_success'], 0, $link);
}

/*------------------------------------------------------ */
//-- 操作订单状态（载入页面）
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'operate')
{
    
    $order_id = '';
    /* 检查权限 */
    empty($_REQUEST['crm']) ? admin_priv('order_os_edit') : check_authz_json('order_os_edit');

    /* 取得订单id（可能是多个，多个sn）和操作备注（可能没有） */
    if(isset($_REQUEST['order_id']))
    {
        $order_id= $_REQUEST['order_id'];
        if (!empty($_REQUEST['crm'])) {
            $smarty->assign('order_sn', $db->getOne("SELECT order_sn FROM " . $ecs->table('order_info') . " WHERE order_id = $order_id"));
        }
    }
    $batch          = isset($_REQUEST['batch']); // 是否批处理
    $action_note    = isset($_REQUEST['action_note']) ? trim($_REQUEST['action_note']) : '';

    $show_operator  = false;
    // We always using MB_CASE_TITLE make First word upper
    $operator       = isset($_REQUEST['operator']) ? strtolower(trim($_REQUEST['operator'])) : '';
    $operator       = mb_convert_case($operator, MB_CASE_TITLE, "UTF-8");
    
    /* 确认 */
    if (isset($_POST['confirm']))
    {
        $require_note   = false;
        $action         = $_LANG['op_confirm'];
        $operation      = 'confirm';
    }
    /* 付款 */
    elseif (isset($_POST['pay']))
    {
        /* 检查权限 */
        admin_priv('order_ps_edit');
        $require_note   = $_CFG['order_pay_note'] == 1;
        $action         = $_LANG['op_pay'];
        $operation      = 'pay';
        $show_operator  = true;

        extract($actgHooks->orderPayPreprocess($order_id));
    }
    /* 未付款 */
    elseif (isset($_POST['unpay']))
    {
        /* 检查权限 */
        admin_priv('order_ps_edit');

        $require_note   = $_CFG['order_unpay_note'] == 1;
        $order          = order_info($order_id);
        if ($order['money_paid'] > 0)
        {
            $show_refund = true;
        }
        $anonymous      = $order['user_id'] == 0;
        $action         = $_LANG['op_unpay'];
        $operation      = 'unpay';
    }
    /* 配货 */
    elseif (isset($_POST['prepare']))
    {
        $require_note   = false;
        $action         = $_LANG['op_prepare'];
        $operation      = 'prepare';
    }
    /* 生成發貨單 */
    elseif (isset($_POST['ship']))
    {
        
        /* 查询：检查权限 */
        admin_priv('order_ss_edit');

        $order_id = intval(trim($order_id));
        $action_note = trim($action_note);
        
        $data = $deliveryorderController->prepare_create_delivery_order($order_id, $action_note);
        extract($data);
        
        /* 模板赋值 */
        $smarty->assign('order', $order);
        $smarty->assign('exist_real_goods', $exist_real_goods);
        $smarty->assign('goods_attr', $attr);
        $smarty->assign('goods_list', $goods_list);

        $smarty->assign('order_id', $order_id); // 订单id
        $smarty->assign('operation', 'split'); // 订单id
        $smarty->assign('action_note', $action_note); // 发货操作信息

        /* 显示模板 */
        $smarty->assign('ur_here', $_LANG['order_operate'] . $_LANG['op_split']);
        assign_query_info();
        $smarty->display('order_delivery_info.htm');
        exit;
    }
    elseif(isset($_POST['quick_ship']))
    {
        /* 查询：检查权限 */
        admin_priv('order_ss_edit');

        $order = order_info($order_id);
        if (!empty($order['is_hold'])) {
            sys_msg("此訂單暫時無法出貨");
        }
        $order_id = intval(trim($order_id));
        $delivery_id = $deliveryorderController->quick_create_delivery_order($order_id, $_REQUEST);

        /* 操作成功 */
        $links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=info&order_id=' . $order_id);
        $links[] = array('text' => $_LANG['09_delivery_order'], 'href' => 'order.php?act=delivery_list');
        sys_msg($_LANG['act_ok'], 0, $links);

    }
    /* 未发货 */
    elseif (isset($_POST['unship']))
    {
        /* 检查权限 */
        admin_priv('order_ss_edit');

        $require_note   = $_CFG['order_unship_note'] == 1;
        $action         = $_LANG['op_unship'];
        $operation      = 'unship';
    }
    /* 收货确认 */
    elseif (isset($_POST['receive']))
    {
        $require_note   = $_CFG['order_receive_note'] == 1;
        $action         = $_LANG['op_receive'];
        $operation      = 'receive';
    }
    /* 手動完成訂單 */
    elseif (isset($_POST['finish_order']))
    {
        $require_note   = false;
        $action         = $_LANG['op_finish_order'];
        $operation      = 'finish_order';
    }
    /* 取消 */
    elseif (isset($_POST['cancel']))
    {
        $require_note   = $_CFG['order_cancel_note'] == 1;
        $action         = $_LANG['op_cancel'];
        $operation      = 'cancel';
        $show_cancel_note   = true;
        $order          = order_info($order_id);
        if ($order['money_paid'] > 0)
        {
            $show_refund = true;
        }
        $anonymous      = $order['user_id'] == 0;
    }
    /* 无效 */
    elseif (isset($_POST['invalid']))
    {
        $require_note   = $_CFG['order_invalid_note'] == 1;
        $action         = $_LANG['op_invalid'];
        $operation      = 'invalid';
    }
    /* 售后 */
    elseif (isset($_POST['after_service']))
    {
        $require_note   = true;
        $action         = $_LANG['op_after_service'];
        $operation      = 'after_service';
        $show_operator  = true;
    }
    /* 退货 */
    elseif (isset($_POST['return']))
    {
        $require_note   = $_CFG['order_return_note'] == 1;
        $order          = order_info($order_id);
        if ($order['money_paid'] > 0)
        {
            $show_refund = true;
        }
        $anonymous      = $order['user_id'] == 0;
        $action         = $_LANG['op_return'];
        $operation      = 'return';

    }
    /* 指派 */
    elseif (isset($_POST['assign']))
    {
        /* 取得参数 */
        $new_agency_id  = isset($_POST['agency_id']) ? intval($_POST['agency_id']) : 0;
        if ($new_agency_id == 0)
        {
            sys_msg($_LANG['js_languages']['pls_select_agency']);
        }

        /* 查询订单信息 */
        $order = order_info($order_id);

        /* 如果管理员属于某个办事处，检查该订单是否也属于这个办事处 */
        $sql = "SELECT agency_id FROM " . $ecs->table('admin_user') . " WHERE user_id = '$_SESSION[admin_id]'";
        $admin_agency_id = $db->getOne($sql);
        if ($admin_agency_id > 0)
        {
            if ($order['agency_id'] != $admin_agency_id)
            {
                sys_msg($_LANG['priv_error']);
            }
        }

        /* 修改订单相关所属的办事处 */
        if ($new_agency_id != $order['agency_id'])
        {
            $query_array = array('order_info', // 更改订单表的供货商ID
                                 'delivery_order', // 更改订单的发货单供货商ID
                                 'back_order'// 更改订单的退货单供货商ID
            );
            foreach ($query_array as $value)
            {
                $db->query("UPDATE " . $ecs->table($value) . " SET agency_id = '$new_agency_id' " .
                    "WHERE order_id = '$order_id'");

            }
        }

        /* 操作成功 */
        $links[] = array('href' => 'order.php?act=list&' . list_link_postfix(), 'text' => $_LANG['02_order_list']);
        sys_msg($_LANG['act_ok'], 0, $links);
    }
    /* 订单删除 */
    elseif (isset($_POST['remove']))
    {
        $require_note = false;
        $operation = 'remove';
        if (!$batch)
        {
            /* 检查能否操作 */
            $order = order_info($order_id);
            $operable_list = operable_list($order);
            if (!isset($operable_list['remove']))
            {
                die('Hacking attempt');
            }

            /* 删除订单 */
            $db->query("DELETE FROM ".$ecs->table('order_info'). " WHERE order_id = '$order_id'");
            $db->query("DELETE FROM ".$ecs->table('order_goods'). " WHERE order_id = '$order_id'");
            $db->query("DELETE FROM ".$ecs->table('order_action'). " WHERE order_id = '$order_id'");
            $db->query("DELETE FROM ".$ecs->table('coupon_log'). " WHERE order_id = '$order_id'");
            $db->query("DELETE FROM ".$ecs->table('order_coupons'). " WHERE order_id = '$order_id'");
            $action_array = array('delivery', 'back');
            del_delivery($order_id, $action_array);

            /* todo 记录日志 */
            admin_log($order['order_sn'], 'remove', 'order');

            /* 返回 */
            sys_msg($_LANG['order_removed'], 0, array(array('href'=>'order.php?act=list&' . list_link_postfix(), 'text' => $_LANG['return_list'])));
        }
    }
    /* 发货单删除 */
    elseif (isset($_REQUEST['remove_invoice']))
    {
        // 删除发货单
        $delivery_id=$_REQUEST['delivery_id'];
        $delivery_id = is_array($delivery_id) ? $delivery_id : array($delivery_id);

				global $db,$ecs;

				$db->query('START TRANSACTION');

				require_once(ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_goods_attr.php');
				require_once(ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_warehouse.php');

        foreach($delivery_id as $value_is)
        {
            $value_is = intval(trim($value_is));

            // 查询：发货单信息
            $delivery_order = $deliveryorderController->delivery_order_info($value_is);

            // 如果status不是退货
            if ($delivery_order['status'] != 1)
            {
                /* 处理退货 */
                if(!delivery_return_goods($value_is, $delivery_order))
                {
	                	$db->query('ROLLBACK');

							      $links[] = array('text' => $_LANG['return_to_delivery_list'], 'href' => 'order.php?act=delivery_list');
							      sys_msg($_LANG['act_false'], 1, $links);
							      exit;
                }
            }

            // 如果status是已发货并且发货单号不为空
            if ($delivery_order['status'] == 0 && $delivery_order['invoice_no'] != '')
            {
                /* 更新：删除订单中的发货单号 */
                if(!del_order_invoice_no($delivery_order['order_id'], $delivery_order['invoice_no']))
                {
                	$db->query('ROLLBACK');

						      $links[] = array('text' => $_LANG['return_to_delivery_list'], 'href' => 'order.php?act=delivery_list');
						      sys_msg($_LANG['act_false'], 1, $links);
						      exit;
                }
            }
            //如果是已经发货的，退回库存
            if ($delivery_order['status'] == 0 && $_CFG['stock_dec_time']==0)
            {
            	if(!add_stock($value_is,$delivery_order['order_id'],2))
            	{
                	$db->query('ROLLBACK');

						      $links[] = array('text' => $_LANG['return_to_delivery_list'], 'href' => 'order.php?act=delivery_list');
						      sys_msg($_LANG['erp_warehousing_auto_on_cancel_ship_failed'], 1, $links);
						      exit;
            	}
            }

            // 更新：删除发货单
            $sql = "DELETE FROM ".$ecs->table('delivery_order'). " WHERE delivery_id = '$value_is'";
            if(!$db->query($sql))
            {
                	$db->query('ROLLBACK');

						      $links[] = array('text' => $_LANG['return_to_delivery_list'], 'href' => 'order.php?act=delivery_list');
						      sys_msg($_LANG['act_false'], 1, $links);
						      exit;
            }
        }

        $db->query('COMMIT');

        /* 返回 */
        sys_msg($_LANG['tips_delivery_del'], 0, array(array('href'=>'order.php?act=delivery_list' , 'text' => $_LANG['return_list'])));
    }
     /* 退货单删除 */
    elseif (isset($_REQUEST['remove_back']))
    {
        $back_id = $_REQUEST['back_id'];
        /* 删除退货单 */
        if(is_array($back_id))
        {
        foreach ($back_id as $value_is)
            {
                $sql = "DELETE FROM ".$ecs->table('back_order'). " WHERE back_id = '$value_is'";
                $db->query($sql);
            }
        }
        else
        {
            $sql = "DELETE FROM ".$ecs->table('back_order'). " WHERE back_id = '$back_id'";
            $db->query($sql);
        }
        /* 返回 */
        sys_msg($_LANG['tips_back_del'], 0, array(array('href'=>'order.php?act=back_list' , 'text' => $_LANG['return_list'])));
    }
    /* 積分補償 */
    elseif (isset($_POST['compensate']))
    {
        $require_note   = false;
        $action         = $_LANG['op_compensate'];
        $operation      = 'compensate';
    }
    /* hold order */
    elseif (isset($_POST['hold_order']))
    {
        $action         = $_LANG['op_hold_order'];
        $operation      = 'hold_order';
    }
    /* hold order */
    elseif (isset($_POST['unhold_order']))
    {
        $action         = $_LANG['op_unhold_order'];
        $operation      = 'unhold_order';
    }
    /* 生成采购订单 */
    if(isset($_POST['purchase']))
    {
        //检查权限
        admin_priv('erp_order_manage');

        if (empty($_POST['order_id']))
        {
            sys_msg($_LANG['pls_select_order']);
        }

        //建立采购订单
        require_once(ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_goods_attr.php');
        require_once(ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_order.php');

        $order_sn_list = explode(',', $_POST['order_id']);

        $sql="select og.goods_id,og.goods_number,og.goods_attr_id from ".$ecs->table('order_goods')." as og,".$ecs->table('order_info')." as oi where ".
        			" oi.order_sn in (".trim($_POST['order_id'],',').") and oi.order_id=og.order_id ";

        $order_goods=$db->getAll($sql);

        $goods_ids=array();

        foreach($order_goods as $key => $goods)
        {
            if(!in_array($goods['goods_id'],$goods_ids))
            {
                $goods_ids[]=$goods['goods_id'];
            }
        }
        if(!empty($goods_ids))
        {
            $suppliers=array();
            $sql="select goods_id,supplier_id from ".$ecs->table('erp_goods_supplier')." where goods_id in (".implode(',',$goods_ids).")";
            $res=$db->query($sql);
            while($row=mysql_fetch_assoc($res))
            {
                if(!isset($suppliers[$row['goods_id']]))
                {
                    $suppliers[$row['goods_id']]=$row['supplier_id'];
                }
            }
            unset($goods_ids);
        }

        $new_order_goods=array();

        foreach($order_goods as $key => $goods)
        {
            $supplier_id=$suppliers[$goods['goods_id']] > 0 ? $suppliers[$goods['goods_id']] : 0;

            $new_order_goods[$supplier_id][]=$goods;

            unset($order_goods[$key]);
        }

        $num=0;

        //开始建立采购单
        foreach($new_order_goods as $supplier_id => $item)
        {
            //$erp_order_id=add_order($supplier_id);
            $erp_order_id=add_order2($supplier_id);

            foreach ($item as $row)
            {
                $attr_id=get_attr_id($row['goods_id'],$row['goods_attr_id']);

                //是否已经有相同属性的
                $sql="select order_item_id from ".$ecs->table('erp_order_item')." where order_id='".$erp_order_id."' and goods_id='".$row['goods_id']."' and attr_id='".$attr_id."'";
                $order_item_id=$db->getOne($sql);

                if(!empty($order_item_id))
                {
                    //修改数量
                    $sql="update ".$ecs->table('erp_order_item')." set order_qty=order_qty+".$row['goods_number'].",amount=amount+".($row['goods_number']*$row['purchase_price'])." where order_item_id='".$order_item_id."'";
                }
                else
                {
                    //插入订单项
                    $sql="insert into ".$ecs->table('erp_order_item').
                    " set order_id='".$erp_order_id.
                    "',goods_id='".$row['goods_id'].
                    "',attr_id='".$attr_id.
                    "',order_qty='".$row['goods_number'].
                    "',price='0".
                    "',amount='0".
                    "',specific_desc=''";
                }

                $db->query($sql);
            }
            $num++;
        }

        require(ROOT_PATH . 'languages/' .$_CFG['lang']. '/admin/ERP.php');

        $msg=sprintf($_LANG['erp_order_create_success'],$num);

        $links[] = array('text' => $_LANG['erp_order_view_list'], 'href' => 'erp_order_manage.php?act=order_list');
        $links[] = array('text' => $_LANG['erp_order_back'], 'href' => 'order.php?act=list');

        sys_msg($msg, 0, $links);
    }
    /* 批量打印订单 */
    elseif (isset($_POST['print']))
    {
        if (empty($_POST['order_id']))
        {
            sys_msg($_LANG['pls_select_order']);
        }

        /* 赋值公用信息 */
        $smarty->assign('shop_name',    $_CFG['shop_name']);
        $smarty->assign('shop_url',     $ecs->url());
        $smarty->assign('shop_address', $_CFG['shop_address']);
        $smarty->assign('service_phone',$_CFG['service_phone']);
        $smarty->assign('print_time',   local_date($_CFG['time_format']));
        $smarty->assign('action_user',  $_SESSION['admin_name']);

        $html = '';
        $order_sn_list = explode(',', $_POST['order_id']);
        foreach ($order_sn_list as $order_sn)
        {
            /* 取得订单信息 */
            $order = order_info(0, $order_sn);
            if (empty($order))
            {
                continue;
            }

            /* 根据订单是否完成检查权限 */
            if (order_finished($order))
            {
                if (!admin_priv('order_view_finished', '', false))
                {
                    continue;
                }
            }
            else
            {
                if (!admin_priv('order_view', '', false))
                {
                    continue;
                }
            }

            /* 如果管理员属于某个办事处，检查该订单是否也属于这个办事处 */
            $sql = "SELECT agency_id FROM " . $ecs->table('admin_user') . " WHERE user_id = '$_SESSION[admin_id]'";
            $agency_id = $db->getOne($sql);
            if ($agency_id > 0)
            {
                if ($order['agency_id'] != $agency_id)
                {
                    continue;
                }
            }

            /* 取得用户名 */
            if ($order['user_id'] > 0)
            {
                $user = user_info($order['user_id']);
                if (!empty($user))
                {
                    $order['user_name'] = $user['user_name'];
                }
            }

            /* 取得区域名 */
            $sql = "SELECT concat(IFNULL(c.region_name, ''), '  ', IFNULL(p.region_name, ''), " .
                        "'  ', IFNULL(t.region_name, ''), '  ', IFNULL(d.region_name, '')) AS region " .
                    "FROM " . $ecs->table('order_info') . " AS o " .
                        "LEFT JOIN " . $ecs->table('region') . " AS c ON o.country = c.region_id " .
                        "LEFT JOIN " . $ecs->table('region') . " AS p ON o.province = p.region_id " .
                        "LEFT JOIN " . $ecs->table('region') . " AS t ON o.city = t.region_id " .
                        "LEFT JOIN " . $ecs->table('region') . " AS d ON o.district = d.region_id " .
                    "WHERE o.order_id = '$order[order_id]'";
            $order['region'] = $db->getOne($sql);

            /* 其他处理 */
            $order['order_time']    = local_date($_CFG['time_format'], $order['add_time']);
            $order['pay_time']      = $order['pay_time'] > 0 ?
                local_date($_CFG['time_format'], $order['pay_time']) : $_LANG['ps'][PS_UNPAYED];
            $order['shipping_time'] = $order['shipping_time'] > 0 ?
                local_date($_CFG['time_format'], $order['shipping_time']) : $_LANG['ss'][SS_UNSHIPPED];
            $order['status']        = $_LANG['os'][$order['order_status']] . ',' . $_LANG['ps'][$order['pay_status']] . ',' . $_LANG['ss'][$order['shipping_status']];
            $order['invoice_no']    = $order['shipping_status'] == SS_UNSHIPPED || $order['shipping_status'] == SS_PREPARING ? $_LANG['ss'][SS_UNSHIPPED] : $order['invoice_no'];

            /* 此订单的发货备注(此订单的最后一条操作记录) */
            $sql = "SELECT action_note FROM " . $ecs->table('order_action').
                   " WHERE order_id = '$order[order_id]' AND shipping_status = 1 ORDER BY log_time DESC";
            $order['invoice_note'] = $db->getOne($sql);

            /* 参数赋值：订单 */
            $smarty->assign('order', $order);

            /* 取得订单商品 */
            $goods_list = array();
            $goods_attr = array();
            $sql = "SELECT o.*, g.goods_number AS storage, o.goods_attr, IFNULL(b.brand_name, '') AS brand_name " .
                    "FROM " . $ecs->table('order_goods') . " AS o ".
                    "LEFT JOIN " . $ecs->table('goods') . " AS g ON o.goods_id = g.goods_id " .
                    "LEFT JOIN " . $ecs->table('brand') . " AS b ON g.brand_id = b.brand_id " .
                    "WHERE o.order_id = '$order[order_id]' ";
            $res = $db->query($sql);
            while ($row = $db->fetchRow($res))
            {
                /* 虚拟商品支持 */
                if ($row['is_real'] == 0)
                {
                    /* 取得语言项 */
                    $filename = ROOT_PATH . 'plugins/' . $row['extension_code'] . '/languages/common_' . $_CFG['lang'] . '.php';
                    if (file_exists($filename))
                    {
                        include_once($filename);
                        if (!empty($_LANG[$row['extension_code'].'_link']))
                        {
                            $row['goods_name'] = $row['goods_name'] . sprintf($_LANG[$row['extension_code'].'_link'], $row['goods_id'], $order['order_sn']);
                        }
                    }
                }

                $row['formated_subtotal']       = price_format($row['goods_price'] * $row['goods_number']);
                $row['formated_goods_price']    = price_format($row['goods_price']);

                $goods_attr[] = explode(' ', trim($row['goods_attr'])); //将商品属性拆分为一个数组
                $goods_list[] = $row;
            }
            $attr = array();
            $arr  = array();
            foreach ($goods_attr AS $index => $array_val)
            {
                foreach ($array_val AS $value)
                {
                    $arr = explode(':', $value);//以 : 号将属性拆开
                    $attr[$index][] =  @array('name' => $arr[0], 'value' => $arr[1]);
                }
            }
            
            $smarty->assign('goods_attr', $attr);
            $smarty->assign('goods_list', $goods_list);

            $smarty->template_dir = '../' . DATA_DIR;
            $html .= $smarty->fetch('order_print.html') .
                '<div style="PAGE-BREAK-AFTER:always"></div>';
        }

        echo $html;
        exit;
    }
    /* 去发货 */
    elseif (isset($_POST['to_delivery']))
    {
        $url = 'order.php?act=delivery_list&order_sn='.$_REQUEST['order_sn'];

        ecs_header("Location: $url\n");
        exit;
    }
    elseif (isset($_POST['b2_export']))
    {
        $require_note   = false;
        $action         = '輸出到B2';
        $operation      = 'b2_export';
    }
    elseif (isset($_POST['order_export']))
    {
        $require_note   = false;
        $action         = '輸出訂單發貨單號';
        $operation      = 'order_export';
    }
    elseif (isset($_POST['mass_print']))
    {
        $require_note   = false;
        $action         = '列印執貨單';
        $operation      = 'mass_print';
    }
    elseif (isset($_POST['force_wechat_refund']))
    {
        $require_note   = false;
        $action         = '強制微信支付退款';
        $operation      = 'force_wechat_refund';
    }

    /* 直接处理还是跳到详细页面 */
    if (($require_note && $action_note == '') || isset($show_invoice_no) || ($show_actg_pay) || ($show_operator && $operator == ''))
    {
        /* 模板赋值 */
        $smarty->assign('require_note', $require_note); // 是否要求填写备注
        $smarty->assign('action_note', $action_note);   // 备注
        $smarty->assign('show_cancel_note', isset($show_cancel_note)); // 是否显示取消原因
        $smarty->assign('show_invoice_no', isset($show_invoice_no)); // 是否显示发货单号
        $smarty->assign('show_refund', isset($show_refund)); // 是否显示退款
        if ($show_actg_pay) // Accounting System: Select order payment method
        {
            $smarty->assign('show_actg_pay', $show_actg_pay);
            $smarty->assign('actg_payment_methods', $actg_payment_methods);
            $smarty->assign('max_pay_amount', $max_pay_amount);
        }
        if ($show_operator)
        {   
            $smarty->assign('show_operator', $show_operator);
            $smarty->assign('operator', $operator);
            $salespeople = $adminuserController->getAdminsByPriv(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_ORDERMSG, 'name');
            // We always using MB_CASE_TITLE make First word upper
            foreach($salespeople as $key => $value){
                $salespeople[$key] = mb_convert_case($value, MB_CASE_TITLE, "UTF-8");
            }
            $smarty->assign('salesperson_list', $salespeople);
        }
        $smarty->assign('anonymous', isset($anonymous) ? $anonymous : true); // 是否匿名
        $smarty->assign('order_id', $order_id); // 订单id
        $smarty->assign('batch', $batch);   // 是否批处理
        $smarty->assign('operation', $operation); // 操作

        /* 显示模板 */
        $smarty->assign('ur_here', $_LANG['order_operate'] . $action);
        assign_query_info();
        $smarty->display('order_operate.htm');
    }
    else
    {
        /* 直接处理 */
        header('Location: ' .
            'order.php' .
            '?act=' . ($batch ? 'batch_operate_post' : 'operate_post') .
            '&order_id=' . $order_id .
            '&operation=' . $operation .
            '&action_note=' . urlencode($action_note) .
            '&operator=' . urlencode($operator) .
            ($operation == 'pay' && !empty($max_pay_amount) ? "&pay_amount=$max_pay_amount" : "") .
            (isset($show_refund) ? '&refund=3' : '') // we don't use ecshop refund flow, so default to 3 (not handle)
        );
        exit;
        
        // if (!$batch)
        // {
        //     /* 一个订单 */
        //     ecs_header("Location: order.php?act=operate_post&order_id=" . $order_id .
        //             "&operation=" . $operation . "&action_note=" . urlencode($action_note) . "&operator=" . urlencode($operator) . "\n");
        //     exit;
        // }
        // else
        // {
        //     /* 多个订单 */
        //     ecs_header("Location: order.php?act=batch_operate_post&order_id=" . $order_id .
        //             "&operation=" . $operation . "&action_note=" . urlencode($action_note) . "&operator=" . urlencode($operator) . "\n");
        //     exit;
        // }
    }
}

/*------------------------------------------------------ */
//-- 操作订单状态（处理批量提交）
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'batch_operate_post')
{
    /* 检查权限 */
    admin_priv('order_os_edit');

    /* 取得参数 */
    $order_id   = $_REQUEST['order_id'];        // 订单id（逗号格开的多个订单id）
    $operation  = $_REQUEST['operation'];       // 订单操作
    $action_note= $_REQUEST['action_note'];     // 操作备注
    $order_id_list = explode(',', $order_id);

    /* 初始化处理的订单sn */
    $sn_list = array();
    $sn_not_list = array();

    /* 确认 */
    if ('confirm' == $operation)
    {
        foreach($order_id_list as $id_order)
        {
            $sql = "SELECT * FROM " . $ecs->table('order_info') .
                " WHERE order_id = '$id_order'" .
                " AND order_status = '" . OS_UNCONFIRMED . "'";
            $order = $db->getRow($sql);

            if($order)
            {
                 /* 检查能否操作 */
                $operable_list = operable_list($order);
                if (!isset($operable_list[$operation]))
                {
                    $sn_not_list[] = $id_order;
                    continue;
                }

                $order_id = $order['order_id'];

                /* 标记订单为已确认 */
                update_order($order_id, array('order_status' => OS_CONFIRMED, 'confirm_time' => gmtime()));
                update_order_amount($order_id);

                /* 记录log */
                order_action($order['order_sn'], OS_CONFIRMED, SS_UNSHIPPED, PS_UNPAYED, $action_note);

                /* 发送邮件 */
                if ($_CFG['send_confirm_email'] == '1')
                {
                    if (can_send_mail($order['email']) && !in_array($order['order_type'], [Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE, Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT]))
                    {
                        $tpl = get_mail_template('order_confirm');
                        $order['formated_add_time'] = local_date($GLOBALS['_CFG']['time_format'], $order['add_time']);
                        $smarty->assign('order', $order);
                        $smarty->assign('shop_name', $_CFG['shop_name']);
                        $smarty->assign('send_date', local_date($_CFG['date_format']));
                        $smarty->assign('sent_date', local_date($_CFG['date_format']));
                        $content = $smarty->fetch('str:' . $tpl['template_content']);
                        send_mail($order['consignee'], $order['email'], $tpl['template_subject'] . ' - ' . $order['order_sn'], $content, $tpl['is_html']);
                    }
                }

                $sn_list[] = $order['order_sn'];
            }
            else
            {
                $sn_not_list[] = $id_order;
            }
        }

        $sn_str = $_LANG['confirm_order'];
    }
    // 2015-06-02 we changed "invalid" to 退款, and there's no such thing as batch refund
    // /* 无效 */
    // elseif ('invalid' == $operation)
    // {
    //     foreach($order_id_list as $id_order)
    //     {
    //         $sql = "SELECT * FROM " . $ecs->table('order_info') .
    //             " WHERE order_sn = $id_order" . order_query_sql('unpay_unship');
    // 
    //         $order = $db->getRow($sql);
    // 
    //         if($order)
    //         {
    //              /* 检查能否操作 */
    //             $operable_list = operable_list($order);
    //             if (!isset($operable_list[$operation]))
    //             {
    //                 $sn_not_list[] = $id_order;
    //                 continue;
    //             }
    // 
    //             $order_id = $order['order_id'];
    // 
    //             /* 标记订单为“无效” */
    //             update_order($order_id, array('order_status' => OS_INVALID));
    // 
    //             /* 记录log */
    //             order_action($order['order_sn'], OS_INVALID, SS_UNSHIPPED, PS_UNPAYED, $action_note);
    // 
    //             /* 发送邮件 */
    //             if ($_CFG['send_invalid_email'] == '1')
    //             {
    //                 if (can_send_mail($order['email']))
    //                 {
    //                     $tpl = get_mail_template('order_invalid');
    //                     $smarty->assign('order', $order);
    //                     $smarty->assign('shop_name', $_CFG['shop_name']);
    //                     $smarty->assign('send_date', local_date($_CFG['date_format']));
    //                     $smarty->assign('sent_date', local_date($_CFG['date_format']));
    //                     $content = $smarty->fetch('str:' . $tpl['template_content']);
    //                     send_mail($order['consignee'], $order['email'], $tpl['template_subject'] . ' - ' . $order['order_sn'], $content, $tpl['is_html']);
    //                 }
    //             }
    // 
    //             /* 退还用户余额、积分、红包 */
    //             return_user_surplus_integral_bonus($order);
    // 
    //             $sn_list[] = $order['order_sn'];
    //         }
    //         else
    //         {
    //             $sn_not_list[] = $id_order;
    //         }
    //     }
    // 
    //     $sn_str = $_LANG['invalid_order'];
    // }
    elseif ('cancel' == $operation)
    {
        foreach($order_id_list as $id_order)
        {
            $sql = "SELECT * FROM " . $ecs->table('order_info') .
                " WHERE order_id = $id_order" . order_query_sql('unpay_unship');

            $order = $db->getRow($sql);
            if($order)
            {
                 /* 检查能否操作 */
                $operable_list = operable_list($order);
                if (!isset($operable_list[$operation]))
                {
                    $sn_not_list[] = $id_order;
                    continue;
                }

                $order_id = $order['order_id'];

                /* 标记订单为“取消”，记录取消原因 */
                $cancel_note = trim($_REQUEST['cancel_note']);
                update_order($order_id, array('order_status' => OS_CANCELED, 'to_buyer' => $cancel_note));

                /* 记录log */
                order_action($order['order_sn'], OS_CANCELED, $order['shipping_status'], PS_UNPAYED, $action_note);

                /* 发送邮件 */
                if ($_CFG['send_cancel_email'] == '1')
                {
                    if (can_send_mail($order['email']) && !in_array($order['order_type'], [Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE, Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT]))
                    {
                        $tpl = get_mail_template('order_cancel');
                        $smarty->assign('order', $order);
                        $smarty->assign('shop_name', $_CFG['shop_name']);
                        $smarty->assign('send_date', local_date($_CFG['date_format']));
                        $smarty->assign('sent_date', local_date($_CFG['date_format']));
                        $content = $smarty->fetch('str:' . $tpl['template_content']);
                        send_mail($order['consignee'], $order['email'], $tpl['template_subject'] . ' - ' . $order['order_sn'], $content, $tpl['is_html']);
                    }
                }

                /* 退还用户余额、积分、红包 */
                $orderController = new Yoho\cms\Controller\OrderController();
                $orderController->returnUserSurplusIntegralBonus($order);

                $flashdealController = new \Yoho\cms\Controller\FlashdealController();
                $flashdealController->cancelFlashdealOrder($order['order_id']);

                $sn_list[] = $order['order_sn'];
             }
            else
            {
                $sn_not_list[] = $id_order;
            }
        }

        $sn_str = $_LANG['cancel_order'];
    }
    elseif ('remove' == $operation)
    {
        foreach ($order_id_list as $id_order)
        {
            /* 检查能否操作 */
            $order = order_info($id_order);
            $operable_list = operable_list($order);
            if (!isset($operable_list['remove']))
            {
                $sn_not_list[] = $id_order;
                continue;
            }

            /* 删除订单 */
            $db->query("DELETE FROM ".$ecs->table('order_info'). " WHERE order_id = '$order[order_id]'");
            $db->query("DELETE FROM ".$ecs->table('order_goods'). " WHERE order_id = '$order[order_id]'");
            $db->query("DELETE FROM ".$ecs->table('order_action'). " WHERE order_id = '$order[order_id]'");
            $db->query("DELETE FROM ".$ecs->table('coupon_log'). " WHERE order_id = '$order[order_id]'");
            $db->query("DELETE FROM ".$ecs->table('order_coupons'). " WHERE order_id = '$order[order_id]'");
            $action_array = array('delivery', 'back');
            del_delivery($order['order_id'], $action_array);

            /* todo 记录日志 */
            admin_log($order['order_sn'], 'remove', 'order');

            $sn_list[] = $order['order_sn'];
        }

        $sn_str = $_LANG['remove_order'];
    }
    elseif ('b2_export' == $operation)
    {
        export_tqb_b2_xls($order_id_list);
    }
    elseif ('order_export' == $operation)
    {
        $orderController->generator_order_info_excel($order_id_list);
    }
    elseif ('mass_print' == $operation)
    {
        $deliveryorderController = new Yoho\cms\Controller\DeliveryOrderController();
        ini_set('display_errors',0);
        $html = '';
        foreach ($order_id_list as $order_id) {
            $delivery_list = $deliveryorderController->get_order_delivery_info($order_id);
            if($delivery_list){
                foreach($delivery_list as $key => $delivery){
                    $delivery_id = $delivery['delivery_id'];
                    assign_order_print($order_id, '', $delivery_id, 1);
                    $smarty->template_dir = '../' . DATA_DIR;
                    $template_order = $smarty->get_template_vars('order');
                    $template = ($template_order['only_market_invoice']) ? 'order_print3.html' : 'order_print2.html';
                    $html .= $smarty->fetch($template) .
                        '<div style="PAGE-BREAK-AFTER:always"></div>';
                    assign_order_print($order_id, '', $delivery_id, 2);
                    $smarty->template_dir = '../' . DATA_DIR;
                    $template_order = $smarty->get_template_vars('order');
                    $template = ($template_order['only_market_invoice']) ? 'order_print3.html' : 'order_print2.html';
                    $html .= $smarty->fetch($template) .
                        '<div style="PAGE-BREAK-AFTER:always"></div>';          
                }
            }

        }
        echo $html;
        exit;
    }
    else
    {
        die('invalid params');
    }

    /* 取得备注信息 */
//    $action_note = $_REQUEST['action_note'];

    if(empty($sn_not_list))
    {
        $sn_list = empty($sn_list) ? '' : $_LANG['updated_order'] . join($sn_list, ',');
        $msg = $sn_list;
        $links[] = array('text' => $_LANG['return_list'], 'href' => 'order.php?act=list&' . list_link_postfix());
        sys_msg($msg, 0, $links);
    }
    else
    {
        $order_list_no_fail = array();
        $sql = "SELECT * FROM " . $ecs->table('order_info') .
                " WHERE order_sn " . db_create_in($sn_not_list);
        $res = $db->query($sql);
        while($row = $db->fetchRow($res))
        {
            $order_list_no_fail[$row['order_id']]['order_id'] = $row['order_id'];
            $order_list_no_fail[$row['order_id']]['order_sn'] = $row['order_sn'];
            $order_list_no_fail[$row['order_id']]['order_status'] = $row['order_status'];
            $order_list_no_fail[$row['order_id']]['shipping_status'] = $row['shipping_status'];
            $order_list_no_fail[$row['order_id']]['pay_status'] = $row['pay_status'];

            $order_list_fail = '';
            foreach(operable_list($row) as $key => $value)
            {
                if($key != $operation)
                {
                    $order_list_fail .= $_LANG['op_' . $key] . ',';
                }
            }
            $order_list_no_fail[$row['order_id']]['operable'] = $order_list_fail;
        }

        /* 模板赋值 */
        $smarty->assign('order_info', $sn_str);
        $smarty->assign('action_link', array('href' => 'order.php?act=list', 'text' => $_LANG['02_order_list']));
        $smarty->assign('order_list',   $order_list_no_fail);

        /* 显示模板 */
        assign_query_info();
        $smarty->display('order_operate_info.htm');
    }
}

/*------------------------------------------------------ */
//-- 操作订单状态（处理提交）
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'operate_post')
{
    /* 检查权限 */
    admin_priv('order_os_edit');
    
    /* 取得参数 */
    $order_id   = intval(trim($_REQUEST['order_id']));        // 订单id
    $operation  = $_REQUEST['operation'];       // 订单操作

    /* 查询订单信息 */
    $order = order_info($order_id);

    /* 检查能否操作 */
    $operable_list = operable_list($order);
    if (!isset($operable_list[$operation]))
    {
        die('Hacking attempt');
    }

    /* 取得备注信息 */
    $action_note = $_REQUEST['action_note'];

    /* 初始化提示信息 */
    $msg = '';

    /* 确认 */
    if ('confirm' == $operation)
    {
        /* 标记订单为已确认 */
        update_order($order_id, array('order_status' => OS_CONFIRMED, 'confirm_time' => gmtime()));
        update_order_amount($order_id);

        /* 记录log */
        order_action($order['order_sn'], OS_CONFIRMED, SS_UNSHIPPED, PS_UNPAYED, $action_note);

        /* 发送邮件 */
        $cfg = $_CFG['send_confirm_email'];
        if ($cfg == '1')
        {
            if (can_send_mail($order['email']) && !in_array($order['order_type'], [Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE, Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT]))
            {
                $tpl = get_mail_template('order_confirm');
                $smarty->assign('order', $order);
                $smarty->assign('shop_name', $_CFG['shop_name']);
                $smarty->assign('send_date', local_date($_CFG['date_format']));
                $smarty->assign('sent_date', local_date($_CFG['date_format']));
                $content = $smarty->fetch('str:' . $tpl['template_content']);
                if (!send_mail($order['consignee'], $order['email'], $tpl['template_subject'] . ' - ' . $order['order_sn'], $content, $tpl['is_html']))
                {
                    $msg = $_LANG['send_mail_fail'];
                }
            }
        }
    }
    /* 付款 */
    elseif ('pay' == $operation)
    {
        /* 检查权限 */
        admin_priv('order_ps_edit');
        
        // Operation Log
        if (empty($_REQUEST['operator']))
        {
            empty($_REQUEST['crm']) ? sys_msg('訂單付款必須填寫操作員') : make_json_error('訂單付款必須填寫操作員');
        }

        /* 标记订单为已确认、已付款，更新付款时间和已支付金额，如果是货到付款，同时修改订单为“收货确认” */
        if ($order['order_status'] != OS_CONFIRMED && $order['order_status'] != OS_SPLITED && $order['order_status'] != OS_SPLITING_PART)
        {
            $arr['order_status'] = OS_CONFIRMED;
            $arr['confirm_time'] = gmtime();
        }
        $arr['pay_status']  = PS_UNPAYED;
        $pay_amount = empty($_REQUEST['pay_amount']) ? 0 : floatval($_REQUEST['pay_amount']);
        if (empty($pay_amount)) {
            empty($_REQUEST['crm']) ? sys_msg('訂單付款必須填寫金額') : make_json_error('訂單付款必須填寫金額');
        }
        if (bccomp($order['order_amount'], $pay_amount) == -1) {
            empty($_REQUEST['crm']) ? sys_msg('訂單付款金額不正確') : make_json_error('訂單付款金額不正確');
        }
        $arr['pay_time']    = gmtime();
        $arr['money_paid']  = bcadd($order['money_paid'], $pay_amount, 2);
        $arr['order_amount']= bcsub($order['order_amount'], $pay_amount, 2);
        $payment = payment_info($order['pay_id']);
        if (bccomp($arr['order_amount'], 0) == 0) {
            $arr['pay_status']  = PS_PAYED;
        }
        if ($payment['is_cod'])
        {
            $arr['shipping_status'] = SS_RECEIVED;
            $order['shipping_status'] = SS_RECEIVED;
        }
        update_order($order_id, $arr);

        $actg_payment_method = empty($_REQUEST['actg_payment_method']) ? 0 : intval($_REQUEST['actg_payment_method']);
        $actgHooks->orderPaid($order_id, $pay_amount, $action_note, $actg_payment_method, false, $_SESSION['admin_id']);

        /* 清除缓存 */
        clear_cache_files();

        $order = order_info($order_id);
        /* 记录log */
        order_action($order['order_sn'], $order['order_status'], $order['shipping_status'], $order['pay_status'], trim(($order['pay_status'] == PS_PAYED ? "" : "[部份付款] $$pay_amount ") . $action_note));

        // Operation log
        admin_operation_log($_REQUEST['operator'], '訂單付款', $order_id);

        // Close all not processed offline payment request
		$db->query("UPDATE " . $ecs->table("offline_payment") . " SET status = " . OFFLINE_PAYTMENT_CLOSED . " WHERE order_id = $order_id AND status < " . OFFLINE_PAYTMENT_FINISHED);
    }
    /* 设为未付款 */
    elseif ('unpay' == $operation)
    {
        /* 检查权限 */
        admin_priv('order_ps_edit');

        $tickets = $db->getCol("SELECT ticket_id FROM " . $ecs->table("offline_payment") . " WHERE order_id = $order_id AND ticket_id != 0");
        if (!empty($tickets)) {
            if ($db->getOne("SELECT COUNT(*) FROM " . $ecs->table('crm_ticket_info') . " WHERE status = " . CRM_STATUS_FINISHED . " AND type = " . CRM_REQUEST_ORDER_PAYMENT . " AND ticket_id " . db_create_in($tickets))) {
                sys_msg('此訂單已連結電郵，請先移除電郵之星號並標籤CRM查詢為處理中');
            }
        }

        // Disallow unpay if finished refund exist
        $refund_count = $db->getOne("SELECT COUNT(*) FROM " . $ecs->table("refund_requests") . " WHERE order_id = $order_id");
        if($refund_count)
        {
            sys_msg('此訂單含退款申請，不能設為未付款');
        }

        /* 标记订单为未付款，更新付款时间和已付款金额 */
        $arr = array(
            'pay_status'    => PS_UNPAYED,
            'pay_time'      => 0,
            'money_paid'    => 0,
            'order_amount'  => $order['money_paid']
        );
        update_order($order_id, $arr);

        /* todo 处理退款 */
        // $refund_type = @$_REQUEST['refund'];
        // $refund_note = @$_REQUEST['refund_note'];
        // order_refund($order, $refund_type, $refund_note);

        $actgHooks->orderUnpaid($order_id);
        
        /* 重新計算訂單金額 */
        update_order_amount($order_id);
        
        /* 更新 pay_log */
        update_pay_log($order_id);

        /* 清除缓存 */
        clear_cache_files();

        /* 记录log */
        order_action($order['order_sn'], OS_CONFIRMED, SS_UNSHIPPED, PS_UNPAYED, $action_note);
    }
    /* 配货 */
    elseif ('prepare' == $operation)
    {
        /* 标记订单为已确认，配货中 */
        if ($order['order_status'] != OS_CONFIRMED)
        {
            $arr['order_status']    = OS_CONFIRMED;
            $arr['confirm_time']    = gmtime();
        }
        $arr['shipping_status']     = SS_PREPARING;
        update_order($order_id, $arr);

        /* 记录log */
        order_action($order['order_sn'], OS_CONFIRMED, SS_PREPARING, $order['pay_status'], $action_note);

        /* 清除缓存 */
        clear_cache_files();
    }
    /* 發貨單確認 */
    elseif ('split' == $operation)
    {
        /* 检查权限 */
        admin_priv('order_ss_edit');
        $action_note = isset($_REQUEST['action_note']) ? trim($_REQUEST['action_note']) : '';
        $delivery_id = $deliveryorderController->create_delivery_order($order_id, $_REQUEST, $action_note);
        /* 操作成功 */
        if (!empty($_REQUEST['back_to_referer']))
        {
            $links[] = array('text' => $_LANG['02_order_list'], 'href' => $_SERVER['HTTP_REFERER']);
        }
        $links[] = array('text' => $_LANG['delivery_sn'] . $_LANG['detail'], 'href' => 'order.php?act=delivery_info&delivery_id=' . $delivery_id);
        $links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=info&order_id=' . $order_id);
        sys_msg($_LANG['act_ok'] . $msg, 0, $links);
    }
    /* 设为未发货 */
    elseif ('unship' == $operation)
    {
        /* 检查权限 */
        admin_priv('order_ss_edit');

				global $db,$ecs;

				$db->query('START TRANSACTION');

        /* 标记订单为“未发货”，更新发货时间, 订单状态为“确认” */
        if(!update_order($order_id, array('shipping_status' => SS_UNSHIPPED, 'shipping_time' => 0, 'invoice_no' => '', 'order_status' => OS_CONFIRMED)))
        {
					$db->query('ROLLBACK');

					$links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=info&order_id=' . $order_id);
					sys_msg($_LANG['act_false'], 1, $links);
        }

        /* 记录log */
        if(!order_action($order['order_sn'], $order['order_status'], SS_UNSHIPPED, $order['pay_status'], $action_note))
        {
					$db->query('ROLLBACK');

					$links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=info&order_id=' . $order_id);
					sys_msg($_LANG['act_false'], 1, $links);
        }

        /* 如果订单用户不为空，计算积分，并退回 */
        if ($order['user_id'] > 0)
        {
            /* 取得用户信息 */
            $user = user_info($order['user_id']);

            /* 计算并退回积分 */
            $integral = integral_to_give($order);
            if(!log_account_change($order['user_id'], 0, 0, (-1) * intval($integral['rank_points']), (-1) * intval($integral['custom_points']), sprintf($_LANG['return_order_gift_integral'], $order['order_sn']), ACT_OTHER, 0, ['sales_order_id' => $order_id]))
            {
                $db->query('ROLLBACK');

                $links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=info&order_id=' . $order_id);
                sys_msg($_LANG['act_false'], 1, $links);
            }

            /* todo 计算并退回红包 */
            if(!return_order_bonus($order_id))
            {
                $db->query('ROLLBACK');

                $links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=info&order_id=' . $order_id);
                sys_msg($_LANG['act_false'], 1, $links);
            }

            // If the order used a referral coupon, give reward for the referer user
            $coupons = order_coupons($order_id);
            if (!empty($coupons))
            {
                foreach ($coupons as $coupon)
                {
                    if ($coupon['referer_id'] > 0)
                    {
                        $referral_integral = 1000;
                        $log_msg = '由於退貨或未出貨操作，退回推薦新會員 ' . $user['user_name'] . ' 訂單 ' . $order['order_sn'] . ' 贈送的積分';
                        if(!log_account_change($coupon['referer_id'], 0, 0, 0, (-1) * $referral_integral, $log_msg, ACT_OTHER, 0, ['sales_order_id' => $order_id]))
                        {
                            $db->query('ROLLBACK');

                            $links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=info&order_id=' . $order_id);
                            sys_msg($_LANG['act_false'], 1, $links);
                        }
                    }
                }
            }
        }
        //退回库存（开入库单）
        if($_CFG['stock_dec_time']==0)
        {
            require_once(ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_goods_attr.php');
            require_once(ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_warehouse.php');

            $sql="select delivery_id from ".$ecs->table('delivery_order')." where order_id='".$order_id."' and status='0'"; // 已發貨

            // $res=$db->query($sql);
            // while(list($delivery_id)=mysql_fetch_row($res))

            $res = $db->getCol($sql);

            if (!empty($res))
            {
                foreach($res as $delivery_id)
                {
                    if(!add_stock($delivery_id,$order_id,2))
                    {
                        $db->query('ROLLBACK');

                        $links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=info&order_id=' . $order_id);
                        sys_msg($_LANG['erp_warehousing_auto_on_cancel_ship_failed'], 1, $links);
                    }
                }
            }
            else
            {
                $sql="select delivery_id from ".$ecs->table('delivery_order')." where order_id='".$order_id."' and status='2'"; // 正常
                $res = $db->getCol($sql);
                if (empty($res))
                {
                    // hacked by howang: return stock without 發貨單
                    if(!add_stock(0,$order_id,2))
                    {
                        $db->query('ROLLBACK');

                        $links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=info&order_id=' . $order_id);
                        sys_msg($_LANG['erp_warehousing_auto_on_cancel_ship_failed'], 1, $links);
                    }
                }
            }
        }

        /* 删除发货单 */
        if(!del_order_delivery($order_id))
        {
 					$db->query('ROLLBACK');

					$links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=info&order_id=' . $order_id);
					sys_msg($_LANG['act_false'], 1, $links);
        }

        /* 将订单的商品发货数量更新为 0 */
        $sql = "UPDATE " . $GLOBALS['ecs']->table('order_goods') . "
                SET send_number = 0
                WHERE order_id = '$order_id'";

        if(!$GLOBALS['db']->query($sql, 'SILENT'))
        {
 					$db->query('ROLLBACK');

					$links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=info&order_id=' . $order_id);
					sys_msg($_LANG['act_false'], 1, $links);
        }

        $db->query('COMMIT');

        /* 清除缓存 */
        clear_cache_files();
    }
    /* 收货确认 */
    elseif ('receive' == $operation)
    {
        /* 标记订单为“收货确认”，如果是货到付款，同时修改订单为已付款 */
        $arr = array('shipping_status' => SS_RECEIVED);
        $payment = payment_info($order['pay_id']);
        if ($payment['is_cod'])
        {
            $arr['pay_status'] = PS_PAYED;
            $order['pay_status'] = PS_PAYED;
        }
        update_order($order_id, $arr);

        /* 记录log */
        order_action($order['order_sn'], $order['order_status'], SS_RECEIVED, $order['pay_status'], $action_note);
    }
    /* 手動完成訂單 */
    elseif ('finish_order' == $operation)
    {

        // Check order status and shipping status:
        if($order['order_status'] == OS_CONFIRMED && $order['shipping_status'] == SS_SHIPPED_PART){
            define('GMTIME_UTC', gmtime()); // 获取 UTC 时间戳
            $order_finish = $deliveryorderController->get_all_delivery_finish($order_id);
            $arr = array();
            $arr['shipping_status']        = ($order_finish == 1) ? SS_SHIPPED : $order['shipping_status'];
            $arr['shipping_time']       = GMTIME_UTC; // 发货时间
            if(!update_order($order_id, $arr))
            {
                $db->query('ROLLBACK');

                $links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=info&order_id=' . $order_id);
                sys_msg($_LANG['act_false'], 1, $links);
            }

            /* 如果当前订单已经全部发货 */
            if ($order_finish)
            {
                $order = order_info($order_id);
                /* 如果订单用户不为空，计算积分，并发给用户；发红包 */
                if ($order['user_id'] > 0)
                {
                    /* 取得用户信息 */
                    $user = user_info($order['user_id']);

                    /* 计算并发放积分 */
                    $integral = integral_to_give($order);

                    if(!log_account_change($order['user_id'], 0, 0, intval($integral['rank_points']), intval($integral['custom_points']), sprintf($_LANG['order_gift_integral'], $order['order_sn']), ACT_OTHER, 0, ['sales_order_id' => $order_id]))
                    {
                        $db->query('ROLLBACK');

                        $links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=info&order_id=' . $order_id);
                        sys_msg($_LANG['act_false'] . __LINE__, 1, $links);
                    }

                    /* 发放红包 */
                    if(!send_order_bonus($order_id))
                    {
                        $db->query('ROLLBACK');

                        $links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=info&order_id=' . $order_id);
                        sys_msg($_LANG['act_false'] . __LINE__, 1, $links);
                    }

                    // If the order used a referral coupon, give reward for the referer user
                    $coupons = order_coupons($order_id);
                    if (!empty($coupons))
                    {
                        foreach ($coupons as $coupon)
                        {
                            if ($coupon['referer_id'] > 0)
                            {
                                $referral_integral = 1000;
                                $log_msg = '推薦新會員 ' . $user['user_name'] . ' 訂單 ' . $order['order_sn'] . ' 贈送的積分';
                                if(!log_account_change($coupon['referer_id'], 0, 0, 0, $referral_integral, $log_msg, ACT_OTHER, 0, ['sales_order_id' => $order_id]))
                                {
                                    $db->query('ROLLBACK');

                                    $links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=info&order_id=' . $order_id);
                                    sys_msg($_LANG['act_false'] . __LINE__, 1, $links);
                                }
                            }
                        }
                    }
                }

            }

            /* 记录log */
            order_action($order['order_sn'], $order['order_status'], SS_SHIPPED, $order['pay_status'], $action_note);

            /* 清除缓存 */
            clear_cache_files();
        } else {
            $links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=info&order_id=' . $order_id);
            sys_msg($_LANG['act_false'], 1, $links);
        }

    }
    /* 取消 */
    elseif ('cancel' == $operation)
    {
        if($order['money_paid'] > 0)
        {
            sys_msg("已付款訂單無法直接取消", 0, array(array('text' => $GLOBALS['_LANG']['go_back'], 'href' => 'javascript:history.go(-1)')));
        }
        /* 标记订单为“取消”，记录取消原因 */
        $cancel_note = isset($_REQUEST['cancel_note']) ? trim($_REQUEST['cancel_note']) : '';
        $arr = array(
            'order_status'  => OS_CANCELED,
            'to_buyer'      => $cancel_note,
            'pay_status'    => PS_UNPAYED,
            'pay_time'      => 0,
            'money_paid'    => 0,
            'order_amount'  => $order['money_paid']
        );
        update_order($order_id, $arr);

        // /* todo 处理退款 */
        // if ($order['money_paid'] > 0)
        // {
        //     $refund_type = $_REQUEST['refund'];
        //     $refund_note = $_REQUEST['refund_note'];
        //     order_refund($order, $refund_type, $refund_note);

        //     $actgHooks->orderUnpaid($order_id);
            
        //     /* 重新計算訂單金額 */
        //     update_order_amount($order_id);
            
        //     /* 更新 pay_log */
        //     update_pay_log($order_id);
        // }

        /* 记录log */
        order_action($order['order_sn'], OS_CANCELED, $order['shipping_status'], PS_UNPAYED, $action_note);

        /* 退还用户余额、积分、红包 */
        $orderController = new Yoho\cms\Controller\OrderController();
        $orderController->returnUserSurplusIntegralBonus($order);
        $flashdealController = new \Yoho\cms\Controller\FlashdealController();
        $flashdealController->cancelFlashdealOrder($order['order_id']);

        /* 发送邮件 */
        $cfg = $_CFG['send_cancel_email'];
        if ($cfg == '1')
        {
            if (can_send_mail($order['email']) && !in_array($order['order_type'], [Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE, Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT]))
            {
                $tpl = get_mail_template('order_cancel');
                $smarty->assign('order', $order);
                $smarty->assign('shop_name', $_CFG['shop_name']);
                $smarty->assign('send_date', local_date($_CFG['date_format']));
                $smarty->assign('sent_date', local_date($_CFG['date_format']));
                $content = $smarty->fetch('str:' . $tpl['template_content']);
                if (!send_mail($order['consignee'], $order['email'], $tpl['template_subject'] . ' - ' . $order['order_sn'], $content, $tpl['is_html']))
                {
                    $msg = $_LANG['send_mail_fail'];
                }
            }
        }

        // Close all not processed offline payment request
		$db->query("UPDATE " . $ecs->table("offline_payment") . " SET status = " . OFFLINE_PAYTMENT_CLOSED . " WHERE order_id = $order_id AND status < " . OFFLINE_PAYTMENT_FINISHED);
    }
    // /* 设为无效 */
    // elseif ('invalid' == $operation)
    // {
    //     /* 标记订单为“无效”、“未付款” */
    //     update_order($order_id, array('order_status' => OS_INVALID));
    // 
    //     /* 记录log */
    //     order_action($order['order_sn'], OS_INVALID, $order['shipping_status'], PS_UNPAYED, $action_note);
    // 
    //     /* 发送邮件 */
    //     $cfg = $_CFG['send_invalid_email'];
    //     if ($cfg == '1')
    //     {
    //         if (can_send_mail($order['email']))
    //         {
    //             $tpl = get_mail_template('order_invalid');
    //             $smarty->assign('order', $order);
    //             $smarty->assign('shop_name', $_CFG['shop_name']);
    //             $smarty->assign('send_date', local_date($_CFG['date_format']));
    //             $smarty->assign('sent_date', local_date($_CFG['date_format']));
    //             $content = $smarty->fetch('str:' . $tpl['template_content']);
    //             if (!send_mail($order['consignee'], $order['email'], $tpl['template_subject'] . ' - ' . $order['order_sn'], $content, $tpl['is_html']))
    //             {
    //                 $msg = $_LANG['send_mail_fail'];
    //             }
    //         }
    //     }
    // 
    //     /* 退货用户余额、积分、红包 */
    //     return_user_surplus_integral_bonus($order);
    // }
    /* 退款 */
    elseif ('invalid' == $operation) // 2015-06-02: Changed "invalid" from "無效" to "退款"
    {
        header('Location: refund_requests.php?act=add&order_sn=' . $order['order_sn']);
        exit;
    }
    /* 退货 */
    elseif ('return' == $operation)
    {
        /* 定义当前时间 */
        define('GMTIME_UTC', gmtime()); // 获取 UTC 时间戳

        global $db,$ecs;

        $db->query('START TRANSACTION');

        //退回库存（开入库单）
        if($_CFG['stock_dec_time']==0)
        {
            require_once(ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_goods_attr.php');
            require_once(ROOT_PATH . ADMIN_PATH . '/includes/ERP/lib_erp_warehouse.php');

            $sql="select delivery_id from ".$ecs->table('delivery_order')." where order_id='".$order_id."' and status='0'";

            // $res=$db->query($sql);
            // while(list($delivery_id)=mysql_fetch_row($res))

            $res = $db->getCol($sql);
            if (!empty($res))
            {
                foreach($res as $delivery_id)
                {
                    if(!add_stock($delivery_id,$order_id,3))
                    {
                        $db->query('ROLLBACK');

                        $links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=info&order_id=' . $order_id);
                        sys_msg($_LANG['erp_warehousing_auto_on_cancel_ship_failed'], 1, $links);
                    }
                }
            }
            else
            {
                // hacked by howang: return stock without 發貨單
                if(!add_stock(0,$order_id,3))
                {
                    $db->query('ROLLBACK');

                    $links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=info&order_id=' . $order_id);
                    sys_msg($_LANG['erp_warehousing_auto_on_cancel_ship_failed'], 1, $links);
                }
            }
        }

        /* 过滤数据 */
        $_REQUEST['refund'] = isset($_REQUEST['refund']) ? $_REQUEST['refund'] : '';
        $_REQUEST['refund_note'] = isset($_REQUEST['refund_note']) ? $_REQUEST['refund'] : '';

        /* 标记订单为“退货”、“未付款”、“未发货” */
        $arr = array('order_status'     => OS_RETURNED,
                     'pay_status'       => PS_UNPAYED,
                     'shipping_status'  => SS_UNSHIPPED,
                     'money_paid'       => 0,
                     'invoice_no'       => '',
                     'order_amount'     => $order['money_paid']);
        if(!update_order($order_id, $arr))
        {
					$db->query('ROLLBACK');

					$links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=info&order_id=' . $order_id);
					sys_msg($_LANG['act_false'], 1, $links);
        }

        /* todo 处理退款 */
        if ($order['pay_status'] != PS_UNPAYED)
        {
            $refund_type = $_REQUEST['refund'];
            $refund_note = $_REQUEST['refund'];

            if(!order_refund($order, $refund_type, $refund_note))
            {
		 					$db->query('ROLLBACK');

							$links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=info&order_id=' . $order_id);
							sys_msg($_LANG['act_false'], 1, $links);
            }

            $actgHooks->orderUnpaid($order_id);
        }

        /* 记录log */
        if(!order_action($order['order_sn'], OS_RETURNED, SS_UNSHIPPED, PS_UNPAYED, $action_note))
        {
		 					$db->query('ROLLBACK');

							$links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=info&order_id=' . $order_id);
							sys_msg($_LANG['act_false'], 1, $links);
        }

        /* 如果订单用户不为空，计算积分，并退回 */
        if ($order['user_id'] > 0)
        {
            /* 取得用户信息 */
            $user = user_info($order['user_id']);

            $sql = "SELECT  goods_number, send_number FROM". $GLOBALS['ecs']->table('order_goods') . "
                WHERE order_id = '".$order['order_id']."'";

            $goods_num = $db->query($sql);
            $goods_num = $db->fetchRow($goods_num);

            if($goods_num['goods_number'] == $goods_num['send_number'])
            {
                /* 计算并退回积分 */
                $integral = integral_to_give($order);

                if(!log_account_change($order['user_id'], 0, 0, (-1) * intval($integral['rank_points']), (-1) * intval($integral['custom_points']), sprintf($_LANG['return_order_gift_integral'], $order['order_sn']), ACT_OTHER, 0, ['sales_order_id' => $order_id]))
              	{
                    $db->query('ROLLBACK');

                    $links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=info&order_id=' . $order_id);
                    sys_msg($_LANG['act_false'], 1, $links);
              	}
            }
            /* todo 计算并退回红包 */
            if(!return_order_bonus($order_id))
            {
                $db->query('ROLLBACK');

                $links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=info&order_id=' . $order_id);
                sys_msg($_LANG['act_false'], 1, $links);
            }

            // If the order used a referral coupon, give reward for the referer user
            $coupons = order_coupons($order_id);
            if (!empty($coupons))
            {
                foreach ($coupons as $coupon)
                {
                    if ($coupon['referer_id'] > 0)
                    {
                        $referral_integral = 1000;
                        $log_msg = '由於退貨或未出貨操作，退回推薦新會員 ' . $user['user_name'] . ' 訂單 ' . $order['order_sn'] . ' 贈送的積分';
                        if(!log_account_change($coupon['referer_id'], 0, 0, 0, (-1) * $referral_integral, $log_msg, ACT_OTHER, 0, ['sales_order_id' => $order_id]))
                        {
                            $db->query('ROLLBACK');

                            $links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=info&order_id=' . $order_id);
                            sys_msg($_LANG['act_false'], 1, $links);
                        }
                    }
                }
            }
        }

        /* 退货用户余额、积分、红包 */
        $orderController = new Yoho\cms\Controller\OrderController();
        if(!$orderController->returnUserSurplusIntegralBonus($order))
        {
				 				$db->query('ROLLBACK');echo 1;die;

								$links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=info&order_id=' . $order_id);
								sys_msg($_LANG['act_false'], 1, $links);
        }

        /* 获取当前操作员 */
        $delivery['action_user'] = $_SESSION['admin_name'];
        /* 添加退货记录 */
        $delivery_list = array();
        $sql_delivery = "SELECT *
                         FROM " . $ecs->table('delivery_order') . "
                         WHERE status IN (".DO_DELIVERED.", ".DO_NORMAL.",".DO_COLLECTED.")
                         AND order_id = " . $order['order_id'];
        $delivery_list = $GLOBALS['db']->getAll($sql_delivery);
        if ($delivery_list)
        {
            foreach ($delivery_list as $list)
            {
                $sql_back = "INSERT INTO " . $ecs->table('back_order') . " (delivery_sn, order_sn, order_id, add_time, shipping_id, user_id, action_user, consignee, address, Country, province, City, district, sign_building, Email,Zipcode, Tel, Mobile, best_time, postscript, how_oos, insure_fee, shipping_fee, update_time, suppliers_id, return_time, agency_id, invoice_no) VALUES ";

                $sql_back .= " ( '" . $list['delivery_sn'] . "', '" . $list['order_sn'] . "',
                              '" . $list['order_id'] . "', '" . $list['add_time'] . "',
                              '" . $list['shipping_id'] . "', '" . $list['user_id'] . "',
                              '" . $delivery['action_user'] . "', '" . $list['consignee'] . "',
                              '" . $list['address'] . "', '" . $list['country'] . "', '" . $list['province'] . "',
                              '" . $list['city'] . "', '" . $list['district'] . "', '" . $list['sign_building'] . "',
                              '" . $list['email'] . "', '" . $list['zipcode'] . "', '" . $list['tel'] . "',
                              '" . $list['mobile'] . "', '" . $list['best_time'] . "', '" . $list['postscript'] . "',
                              '" . $list['how_oos'] . "', '" . $list['insure_fee'] . "',
                              '" . $list['shipping_fee'] . "', '" . $list['update_time'] . "',
                              '" . $list['suppliers_id'] . "', '" . GMTIME_UTC . "',
                              '" . $list['agency_id'] . "', '" . $list['invoice_no'] . "'
                              )";
                if(!$GLOBALS['db']->query($sql_back, 'SILENT'))
                {
					 				$db->query('ROLLBACK');

									$links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=info&order_id=' . $order_id);
									sys_msg($_LANG['act_false'], 1, $links);
                }

                $back_id = $GLOBALS['db']->insert_id();

                $sql_back_goods = "INSERT INTO " . $ecs->table('back_goods') . " (back_id, goods_id, product_id, product_sn, goods_name,goods_sn, is_real, send_number, goods_attr)
                                   SELECT '$back_id', goods_id, product_id, product_sn, goods_name, goods_sn, is_real, send_number, goods_attr
                                   FROM " . $ecs->table('delivery_goods') . "
                                   WHERE delivery_id = " . $list['delivery_id'];

                if(!$GLOBALS['db']->query($sql_back_goods, 'SILENT'))
                {
					 				$db->query('ROLLBACK');

									$links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=info&order_id=' . $order_id);
									sys_msg($_LANG['act_false'], 1, $links);
                }
            }
        }

        /* 修改订单的发货单状态为退货 */
        $sql_delivery = "UPDATE " . $ecs->table('delivery_order') . "
                         SET status = 1
                         WHERE status IN (".DO_DELIVERED.", ".DO_NORMAL.",".DO_COLLECTED.")
                         AND order_id = " . $order['order_id'];
        if(!$GLOBALS['db']->query($sql_delivery, 'SILENT'))
        {
				 			$db->query('ROLLBACK');

							$links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=info&order_id=' . $order_id);
							sys_msg($_LANG['act_false'], 1, $links);
        }

        /* 将订单的商品发货数量更新为 0 */
        $sql = "UPDATE " . $GLOBALS['ecs']->table('order_goods') . "
                SET send_number = 0
                WHERE order_id = '$order_id'";
        if(!$GLOBALS['db']->query($sql, 'SILENT'))
				{
				 			$db->query('ROLLBACK');

							$links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=info&order_id=' . $order_id);
							sys_msg($_LANG['act_false'], 1, $links);
				}

				$db->query('COMMIT');

        /* 清除缓存 */
        clear_cache_files();
    }
    elseif ('after_service' == $operation)
    {
        // Operation Log
        if (empty($_REQUEST['operator']))
        {
            sys_msg('儲存備註必須填寫操作員');
        }
        
        /* 记录log */
        order_action($order['order_sn'], $order['order_status'], $order['shipping_status'], $order['pay_status'], '[' . $_LANG['op_after_service'] . '] ' . $action_note, $_REQUEST['operator']);
    }
    elseif ('compensate' == $operation)
    {
        header('Location: refund_requests.php?act=add&compensate=1&order_sn=' . $order['order_sn']);
        exit;
    }
    /* hold order */
    elseif ('hold_order' == $operation)
    {
        /* 标记订单为“收货确认”，如果是货到付款，同时修改订单为已付款 */
        $arr = array('is_hold' => 1, 'to_buyer' => $_LANG['hold_order_message']['default'] . " $order[to_buyer]");
        update_order($order_id, $arr);
        order_action($order['order_sn'], $order['order_status'], $order['shipping_status'], $order['pay_status'], trim("[$_LANG[op_hold_order]] $action_note"));
    }
    /* unhold order */
    elseif ('unhold_order' == $operation)
    {
        /* 标记订单为“收货确认”，如果是货到付款，同时修改订单为已付款 */
        $arr = array('is_hold' => 0, 'to_buyer' => trim(str_replace($_LANG['hold_order_message'], "", $order['to_buyer'])));
        update_order($order_id, $arr);
        order_action($order['order_sn'], $order['order_status'], $order['shipping_status'], $order['pay_status'], trim("[$_LANG[op_unhold_order]] $action_note"));
    }
    elseif ($operation == 'force_wechat_refund')
    {
        header('Location: qfwechatpay_force_refund.php?act=add&order_id=' . $order['order_id']);
        exit;
    }
    else
    {
        die('invalid params');
    }

    /* 操作成功 */
    if (!empty($_REQUEST['back_to_referer']))
    {
        $links[] = array('text' => $_LANG['02_order_list'], 'href' => $_SERVER['HTTP_REFERER']);
    }
    $links[] = array('text' => $_LANG['order_info'], 'href' => 'order.php?act=info&order_id=' . $order_id);
    empty($_REQUEST['crm']) ? sys_msg($_LANG['act_ok'] . $msg, 0, $links) : make_json_response($_LANG['act_ok'] . $msg);
}

elseif ($_REQUEST['act'] == 'json')
{
    include_once(ROOT_PATH . 'includes/cls_json.php');
    $json = new JSON();

    $func = $_REQUEST['func'];
    if ($func == 'get_goods_info')
    {
        /* 取得商品信息 */
        $goods_id = $_REQUEST['goods_id'];
        $sql = "SELECT goods_id, c.cat_name, goods_sn, goods_name, b.brand_name, " .
                "(SELECT sum(goods_qty) FROM ".$ecs->table('erp_goods_attr_stock')." WHERE goods_id = g.goods_id AND warehouse_id IN (".$erpController->getSellableWarehouseIds(true).")) as goods_number, ".
                "market_price, shop_price, promote_price, " .
                "promote_start_date, promote_end_date, goods_brief, goods_type, is_promote " .
                "FROM " . $ecs->table('goods') . " AS g " .
                "LEFT JOIN " . $ecs->table('brand') . " AS b ON g.brand_id = b.brand_id " .
                "LEFT JOIN " . $ecs->table('category') . " AS c ON g.cat_id = c.cat_id " .
                " WHERE goods_id = '$goods_id'";
        $goods = $db->getRow($sql);
        $today = gmtime();
        $goods['goods_price'] = ($goods['is_promote'] == 1 &&
            $goods['promote_start_date'] <= $today && $goods['promote_end_date'] >= $today) ?
            $goods['promote_price'] : $goods['shop_price'];

        /* 取得会员价格 */
        $sql = "SELECT p.user_price, r.rank_name, p.user_rank " .
                "FROM " . $ecs->table('member_price') . " AS p, " .
                    $ecs->table('user_rank') . " AS r " .
                "WHERE p.user_rank = r.rank_id " .
                "AND p.goods_id = '$goods_id' ";
        $goods['user_price'] = $db->getAll($sql);

        /* 取得商品属性 */
        $sql = "SELECT a.attr_id, a.attr_name, g.goods_attr_id, g.attr_value, g.attr_price, a.attr_input_type, a.attr_type " .
                "FROM " . $ecs->table('goods_attr') . " AS g, " .
                    $ecs->table('attribute') . " AS a " .
                "WHERE (a.attr_type=1 or a.attr_type=2) and g.attr_id = a.attr_id " .
                "AND g.goods_id = '$goods_id' ";
        $goods['attr_list'] = array();
        $res = $db->query($sql);
        while ($row = $db->fetchRow($res))
        {
            $goods['attr_list'][$row['attr_id']][] = $row;
        }
        $goods['attr_list'] = array_values($goods['attr_list']);

        echo $json->encode($goods);
    }
}

/*------------------------------------------------------ */
//-- 合并订单
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'ajax_merge_order')
{
    /* 检查权限 */
    admin_priv('order_os_edit');

    include_once(ROOT_PATH . 'includes/cls_json.php');
    $json = new JSON();

    $from_order_sn = empty($_POST['from_order_sn']) ? '' : json_str_iconv(substr($_POST['from_order_sn'], 1));
    $to_order_sn = empty($_POST['to_order_sn']) ? '' : json_str_iconv(substr($_POST['to_order_sn'], 1));

    $m_result = merge_order($from_order_sn, $to_order_sn);
    $result = array('error'=>0,  'content'=>'');
    if ($m_result === true)
    {
        $result['message'] = $GLOBALS['_LANG']['act_ok'];
    }
    else
    {
        $result['error'] = 1;
        $result['message'] = $m_result;
    }
    die($json->encode($result));
}

/*------------------------------------------------------ */
//-- 删除订单
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'remove')
{
    /* 检查权限 */
    admin_priv('order_edit');

    $order_id = intval($_REQUEST['id']);

    /* 检查权限 */
    check_authz_json('order_edit');

    /* 检查订单是否允许删除操作 */
    $order = order_info($order_id);
    $operable_list = operable_list($order);
    if (!isset($operable_list['remove']))
    {
        make_json_error('Hacking attempt');
        exit;
    }

    $GLOBALS['db']->query("DELETE FROM ".$GLOBALS['ecs']->table('order_info'). " WHERE order_id = '$order_id'");
    $GLOBALS['db']->query("DELETE FROM ".$GLOBALS['ecs']->table('order_goods'). " WHERE order_id = '$order_id'");
    $GLOBALS['db']->query("DELETE FROM ".$GLOBALS['ecs']->table('order_action'). " WHERE order_id = '$order_id'");
    $GLOBALS['db']->query("DELETE FROM ".$GLOBALS['ecs']->table('coupon_log'). " WHERE order_id = '$order_id'");
    $GLOBALS['db']->query("DELETE FROM ".$GLOBALS['ecs']->table('order_coupons'). " WHERE order_id = '$order_id'");
    $action_array = array('delivery', 'back');
    del_delivery($order_id, $action_array);

    if ($GLOBALS['db'] ->errno() == 0)
    {
        $url = 'order.php?act=query&' . str_replace('act=remove_order', '', $_SERVER['QUERY_STRING']);

        ecs_header("Location: $url\n");
        exit;
    }
    else
    {
        make_json_error($GLOBALS['db']->errorMsg());
    }
}

/*------------------------------------------------------ */
//-- 根据关键字和id搜索用户
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'search_users')
{
    include_once(ROOT_PATH . 'includes/cls_json.php');
    $json = new JSON();

    $id_name = empty($_GET['id_name']) ? '' : json_str_iconv(trim($_GET['id_name']));

    $result = array('error'=>0, 'message'=>'', 'content'=>'');
    if ($id_name != '')
    {
        $sql = "SELECT user_id, user_name FROM " . $GLOBALS['ecs']->table('users') .
                " WHERE user_id LIKE '%" . mysql_like_quote($id_name) . "%'" .
                " OR user_name LIKE '%" . mysql_like_quote($id_name) . "%'" .
                " LIMIT 20";
        $res = $GLOBALS['db']->query($sql);

         $result['userlist'] = array();
        while ($row = $GLOBALS['db']->fetchRow($res))
        {
             $result['userlist'][] = array('user_id' => $row['user_id'], 'user_name' => $row['user_name']);
        }
    }
    else
    {
        $result['error'] = 1;
        $result['message'] = 'NO KEYWORDS!';
    }

    die($json->encode($result));
}

/*------------------------------------------------------ */
//-- 根据关键字搜索商品
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'search_goods')
{
    include_once(ROOT_PATH . 'includes/cls_json.php');
    $json = new JSON();

    $keyword = empty($_GET['keyword']) ? '' : json_str_iconv(trim($_GET['keyword']));

    $result = array('error'=>0, 'message'=>'', 'content'=>'');

    if ($keyword != '')
    {
        $sql = "SELECT goods_id, goods_name, goods_sn FROM " . $GLOBALS['ecs']->table('goods') .
                " WHERE is_delete = 0" .
                " AND is_on_sale = 1" .
                " AND is_alone_sale = 1" .
                " AND (goods_id LIKE '%" . mysql_like_quote($keyword) . "%'" .
                " OR goods_name LIKE '%" . mysql_like_quote($keyword) . "%'" .
                " OR goods_sn LIKE '%" . mysql_like_quote($keyword) . "%')" .
                " LIMIT 20";
        $res = $GLOBALS['db']->query($sql);

        $result['goodslist'] = array();
        while ($row = $GLOBALS['db']->fetchRow($res))
        {
            $result['goodslist'][] = array('goods_id' => $row['goods_id'], 'name' => $row['goods_id'] . '  ' . $row['goods_name'] . '  ' . $row['goods_sn']);
        }
    }
    else
    {
        $result['error'] = 1;
        $result['message'] = 'NO KEYWORDS';
    }
    die($json->encode($result));
}

/*------------------------------------------------------ */
//-- 编辑收货单号
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'edit_invoice_no')
{
    /* 检查权限 */
    check_authz_json('order_edit');

    $no = empty($_POST['val']) ? 'N/A' : json_str_iconv(trim($_POST['val']));
    $no = $no=='N/A' ? '' : $no;
    $order_id = empty($_POST['id']) ? 0 : intval($_POST['id']);

    if ($order_id == 0)
    {
        make_json_error('NO ORDER ID');
        exit;
    }

    $sql = 'UPDATE ' . $GLOBALS['ecs']->table('order_info') . " SET invoice_no='$no' WHERE order_id = '$order_id'";
    if ($GLOBALS['db']->query($sql))
    {
        if (empty($no))
        {
            make_json_result('N/A');
        }
        else
        {
            make_json_result(stripcslashes($no));
        }
    }
    else
    {
        make_json_error($GLOBALS['db']->errorMsg());
    }
}

/*------------------------------------------------------ */
//-- 编辑付款备注
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'edit_pay_note')
{
    /* 检查权限 */
    check_authz_json('order_edit');

    $no = empty($_POST['val']) ? 'N/A' : json_str_iconv(trim($_POST['val']));
    $no = $no=='N/A' ? '' : $no;
    $order_id = empty($_POST['id']) ? 0 : intval($_POST['id']);

    if ($order_id == 0)
    {
        make_json_error('NO ORDER ID');
        exit;
    }

    $sql = 'UPDATE ' . $GLOBALS['ecs']->table('order_info') . " SET pay_note='$no' WHERE order_id = '$order_id'";
    if ($GLOBALS['db']->query($sql))
    {
        if (empty($no))
        {
            make_json_result('N/A');
        }
        else
        {
            make_json_result(stripcslashes($no));
        }
    }
    else
    {
        make_json_error($GLOBALS['db']->errorMsg());
    }
}

/*------------------------------------------------------ */
//-- 编辑訂單成本
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'ajaxEdit')
{
    /* 检查权限 */
    check_authz_json('order_edit');
    if($_REQUEST['col'] == 'cp_price') {
        $cost = empty($_POST['value']) ? 0 : doubleval($_POST['value']);
        $order_id = empty($_POST['id']) ? 0 : intval($_POST['id']);

        if ($order_id == 0)
        {
            make_json_error('NO ORDER ID');
            exit;
        }

        $sql = 'UPDATE ' . $GLOBALS['ecs']->table('order_info') . " SET cp_price = '$cost' WHERE order_id = '$order_id'";
        if ($GLOBALS['db']->query($sql))
        {
            make_json_result($cost);
        }
        else
        {
            make_json_error($GLOBALS['db']->errorMsg());
        }
    } else {
        $orderController->ajaxEditAction();
    }
    
}

/*------------------------------------------------------ */
//-- 编辑訂單產品成本
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'edit_order_goods_cost')
{
    /* 检查权限 */
    check_authz_json('order_edit');
    if (!$_SESSION['manage_cost'])
    {
        make_json_error('Admin only!');
    }

    $cost = empty($_POST['val']) ? 0 : doubleval($_POST['val']);
    $order_goods_id = empty($_POST['id']) ? 0 : intval($_POST['id']);

    if ($order_goods_id == 0)
    {
        make_json_error('NO ORDER GOODS ID');
        exit;
    }

    $sql = 'UPDATE ' . $GLOBALS['ecs']->table('order_goods') . " SET cost = '$cost' WHERE rec_id = '$order_goods_id'";
    if ($GLOBALS['db']->query($sql))
    {
        make_json_result(price_format($cost, false));
    }
    else
    {
        make_json_error($GLOBALS['db']->errorMsg());
    }
}

/*------------------------------------------------------ */
//-- 编辑訂單銷售人員
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'edit_salesperson')
{
    /* 检查权限 */
    check_authz_json('order_edit');
    if (!$_SESSION['manage_cost'])
    {
        make_json_error('Admin only!');
    }

    $salesperson = empty($_POST['val']) ? 'admin' : trim($_POST['val']);
    $order_id = empty($_POST['id']) ? 0 : intval($_POST['id']);

    if ($order_id == 0)
    {
        make_json_error('NO ORDER ID');
        exit;
    }

    $sql = 'UPDATE ' . $GLOBALS['ecs']->table('order_info') . " SET salesperson = '$salesperson' WHERE order_id = '$order_id'";
    if ($GLOBALS['db']->query($sql))
    {
        make_json_result($salesperson);
    }
    else
    {
        make_json_error($GLOBALS['db']->errorMsg());
    }
}

/*------------------------------------------------------ */
//-- 获取订单商品信息
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'get_goods_info')
{
    /* 取得订单商品 */
    $order_id = isset($_REQUEST['order_id'])?intval($_REQUEST['order_id']):0;
    if (empty($order_id))
    {
        make_json_response('', 1, $_LANG['error_get_goods_info']);
    }
    $goods_list = array();
    $goods_attr = array();
    $sql = "SELECT o.*, g.goods_thumb, (SELECT SUM(ega.goods_qty) FROM " . $ecs->table('erp_goods_attr_stock') . " AS ega WHERE g.goods_id = ega.goods_id AND ega.warehouse_id IN (".$erpController->getSellableWarehouseIds(true).")) AS storage, o.goods_attr, IFNULL(b.brand_name, '') AS brand_name, g.cost " .
            "FROM " . $ecs->table('order_goods') . " AS o ".
            "LEFT JOIN " . $ecs->table('goods') . " AS g ON o.goods_id = g.goods_id " .
            "LEFT JOIN " . $ecs->table('brand') . " AS b ON g.brand_id = b.brand_id " .
            "WHERE o.order_id = '{$order_id}' ";
    $res = $db->query($sql);
    while ($row = $db->fetchRow($res))
    {
        /* 虚拟商品支持 */
        if ($row['is_real'] == 0)
        {
            /* 取得语言项 */
            $filename = ROOT_PATH . 'plugins/' . $row['extension_code'] . '/languages/common_' . $_CFG['lang'] . '.php';
            if (file_exists($filename))
            {
                include_once($filename);
                if (!empty($_LANG[$row['extension_code'].'_link']))
                {
                    $row['goods_name'] = $row['goods_name'] . sprintf($_LANG[$row['extension_code'].'_link'], $row['goods_id'], $order['order_sn']);
                }
            }
        }

        if ($row['is_insurance'] == 1 && $row['insurance_of'] > 0){
            $sql = "SELECT goods_name, goods_sn ".
                    "FROM ".$GLOBALS['ecs']->table('order_goods')." ".
                    "WHERE order_id = ".$order_id." ".
                    "AND goods_id = '".$row['insurance_of']."' ".
                    "AND extension_code != 'package' ".
                    "AND is_package = 0 ".
                    "AND is_insurance = 0 ".
                    "AND insurance_of = 0 ".
                    "AND is_gift = 0 ";
            $result = $GLOBALS['db']->getRow($sql);
            if (!empty($result)){
                $row['insurance_of_name'] = $result['goods_name'];
                $row['insurance_of_sn'] = $result['goods_sn'];
                $row['goods_name'] .= " [".$_LANG['insurance_of_sn'].": ".$result['goods_sn']."]";
                $row['cost'] = $row['goods_price'];
            }
        }

        $row['formated_subtotal']       = price_format($row['goods_price'] * $row['goods_number']);
        $row['formated_goods_price']    = price_format($row['goods_price']);
        $row['formated_cost']           = price_format($row['cost'], false);
        $_goods_thumb = get_image_path($row['goods_id'], $row['goods_thumb'], true);
        $_goods_thumb = ((strpos($_goods_thumb, 'http://') === 0) || (strpos($_goods_thumb, 'https://') === 0)) ? $_goods_thumb : $ecs->url() . $_goods_thumb;
        $row['goods_thumb'] = $_goods_thumb;
        $goods_attr[] = explode(' ', trim($row['goods_attr'])); //将商品属性拆分为一个数组
        $goods_list[] = $row;
    }
    $attr = array();
    $arr  = array();
    foreach ($goods_attr AS $index => $array_val)
    {
        foreach ($array_val AS $value)
        {
            $arr = explode(':', $value);//以 : 号将属性拆开
            $attr[$index][] =  @array('name' => $arr[0], 'value' => $arr[1]);
        }
    }

    $smarty->assign('goods_attr', $attr);
    $smarty->assign('goods_list', $goods_list);
    $str = $smarty->fetch('order_goods_info.htm');
    $goods[] = array('order_id' => $order_id, 'str' => $str);
    make_json_result($goods);
}

/*------------------------------------------------------ */
//-- 获取订单操作信息
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'get_order_actions')
{
    /* 取得订单商品 */
    $order_id = isset($_REQUEST['order_id'])?intval($_REQUEST['order_id']):0;
    if (empty($order_id))
    {
        make_json_error('no order id');
    }
    
    $sql = "SELECT * FROM " . $ecs->table('order_action') . " WHERE order_id = '" . $order_id . "' ORDER BY action_id DESC";
    $order_actions = $db->getAll($sql);
    
    foreach ($order_actions as $key => $row)
    {
        $order_actions[$key]['order_status_formatted']    = $_LANG['os'][$row['order_status']];
        $order_actions[$key]['pay_status_formatted']      = $_LANG['ps'][$row['pay_status']];
        $order_actions[$key]['shipping_status_formatted'] = $_LANG['ss'][$row['shipping_status']];
        $order_actions[$key]['log_time_formatted']        = local_date($_CFG['time_format'], $row['log_time']);
    }
    
    make_json_result($order_actions);
}
/*------------------------------------------------------ */
//-- AJAX 編輯訂單 更新價錢
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'update_price')
{
    include_once(ROOT_PATH . 'includes/cls_json.php');
    $json = new JSON();
    
    $order_id = empty($_GET['order_id']) ? 0 : json_str_iconv(trim($_GET['order_id']));
    if(!$order_id){
        $result = array('error'=>1, 'message'=>'No order id', 'content'=>'');
        die($json->encode($result));
    }
    $order        = order_info($order_id);
    $old_integral = $order['integral'];
    $order['shipping_fee'] = isset($_GET['shipping_fee']) ? json_str_iconv(trim($_GET['shipping_fee'])) : $order['shipping_fee'];
    $order['shipping_fee2'] = isset($_GET['shipping_fee']) ? json_str_iconv(trim($_GET['shipping_fee'])) : $order['shipping_fee'];
    $order['pay_fee']      = isset($_GET['pay_fee']) ? json_str_iconv(trim($_GET['pay_fee'])) :  $order['pay_fee'];
    $order['discount']     = isset($_GET['discount']) ? json_str_iconv(trim($_GET['discount'])) : $order['discount'];
    $order['integral']     = isset($_GET['integral']) ? json_str_iconv(trim($_GET['integral'])) : $order['integral'];
    
    $result = array('error'=>0, 'message'=>'', 'content'=>'');
    $integral_money = value_of_integral($integral);
    $old_amount = $order['order_amount'];

    $order = $orderController->calculateOrderFee($order);
    
    if($order['order_amount'] < 0 && $order['discount'] > 0 )
    {
        $result = array('error'=>1, 'message'=>'應付款金額少於0, 不能設置折扣');
    }

    if($order['order_amount'] < 0 && $order['integral'] > $old_integral )
    {
        $result = array('error'=>1, 'message'=>'應付款金額將少於0, 不能設置積分');
    }
    
    $result['pay_fee'] = $order['pay_fee'];
    $result['shipping_fee'] = number_format($order['shipping_fee'], 2, '.', '');
    $result['order_amount'] = $order['order_amount'];
    $result['order_amount_format'] = price_format(abs($order['order_amount']), false);
    $result['integral'] = $order['integral'];
    $result['total_amount'] = $order['formated_total_amount'];
    $result['discount'] = number_format($order['discount'], 2, '.', '');
    $result['integral2'] = $order['integral2'];
    die($json->encode($result));
}
elseif ($_REQUEST['act'] == 'check_invoice')
{
    $res = $logisticsController->get_tracking_status($_REQUEST);
    make_json_result($res);
}
elseif ($_REQUEST['act'] == 'update_invoice_no')
{
    $order_id = $_REQUEST['order_id'];
    $delivery_id = $_REQUEST['delivery_id'];
    $invoice_no = str_replace(" ","", $_REQUEST['invoice_no']);
    $logistics_id = $_REQUEST['logistics_id'];
    $sql = "UPDATE ". $ecs->table('delivery_order')." SET invoice_no = '$invoice_no' ".($logistics_id > 0 ? ", logistics_id = $logistics_id": ", logistics_id = 0 ")." WHERE delivery_id = $delivery_id ";

    if($db->query($sql)){
        $order = order_info($order_id);
        $sql = "select logistics_name from".$ecs->table('logistics')." where logistics_id = $logistics_id limit 1";
        $log_name = $db->getOne($sql);
        $action_note    = "物流更新 $log_name: $invoice_no";
        order_action($order['order_sn'], $order['order_status'], $order['shipping_status'], $order['pay_status'], $action_note, null, 1);
        make_json_result(true);
    } else {
        make_json_result(false);
    }
}
elseif($_REQUEST['act'] == 'change_mass_logistic_method'){
    $sql = "update ".$ecs->table('mass_delivery')." set logistics = '".$_REQUEST['logistic_method']."' where mass_key = '".$_REQUEST['mass_key']."' ";
    $db->query($sql);
    make_json_result(true);
}
elseif($_REQUEST['act'] == 'mass_delivery'){
    $smarty->assign('MK_CANCEL', MK_CANCEL);
    $smarty->assign('MK_UNDELIVERY', MK_UNDELIVERY);
    $smarty->assign('MK_DELIVERY', MK_DELIVERY);
    $smarty->assign('MK_EXPORT', MK_EXPORT);
    $smarty->assign('MK_IMPORT', MK_IMPORT);
    $smarty->assign('MK_MAIL', MK_MAIL);
    $smarty->assign('MK_FINISH', MK_FINISH);
    if(isset($_REQUEST['ajax'])){
        if(isset($_REQUEST['order_sn'])){
            echo $deliveryorderController->ajax_respond();
        }
    }else if(isset($_REQUEST['list'])){
        $deliveryorderController->massStepAction();
    }else if(isset($_REQUEST['print'])){
        ini_set('display_errors',0);
        $html = '';
        $order_sn_list = $deliveryorderController->get_order_sn();
        //$order_sn_list = array_column($order_sn, 'order_sn');
        foreach ($order_sn_list as $order_sn_key => $order)
        {
            assign_order_print(0,$order['order_sn'], $order['delivery_id'], 1);
            $smarty->template_dir = '../' . DATA_DIR;
            $template_order = $smarty->get_template_vars('order');
            $template = ($template_order['only_market_invoice']) ? 'order_print3.html' : 'order_print2.html';
            $html .= $smarty->fetch($template) .
                '<div style="PAGE-BREAK-AFTER:always"></div>';
            assign_order_print(0,$order['order_sn'], $order['delivery_id'], 2);

            //assign_order_print($order_id, '', $delivery_id, 2);
            $smarty->template_dir = '../' . DATA_DIR;
            $template_order = $smarty->get_template_vars('order');
            $template = ($template_order['only_market_invoice']) ? 'order_print3.html' : 'order_print2.html';
            $html .= $smarty->fetch($template) .
                '<div style="PAGE-BREAK-AFTER:always"></div>';

            $sql = "UPDATE " . $ecs->table('mass_delivery') . " SET process = ".MK_FINISH." WHERE mass_key = '" . $_REQUEST['mass_key'] . "' AND order_id = (SELECT order_id FROM " . $ecs->table('order_info') . " WHERE order_sn = '" . $order['order_sn'] . "')";
            $db->query($sql);
        }
        echo $html;
        exit;
    }else if(isset($_REQUEST['export'])){
        $deliveryorderController->export_excel();
    }else if(isset($_REQUEST['export_fls'])){
        $deliveryorderController->export_fls_excel();
    }else if(isset($_REQUEST['export_wilson'])){
        $deliveryorderController->export_wilson_excel();
    }else if(isset($_REQUEST['uploadEflocker'])){
        $deliveryorderController->uploadEflocker();
    }else if(isset($_REQUEST['import'])){
        if(isset($_FILES["import_excel"])){
            header('Content-Type: application/json');
            echo $deliveryorderController->import_excel();
            exit;
        }
    }else if(isset($_REQUEST['init'])){
        $deliveryorderController->massInitAction();
    }else if(isset($_REQUEST['mass_list_query'])){
        $controller = new Yoho\cms\Controller\YohoBaseController();

        $page_size = empty($_REQUEST['page_size']) ? 50 : $_REQUEST['page_size'];
        $start = empty($_REQUEST['start']) ? 0 : $_REQUEST['start'];
        $sort_by = empty($_REQUEST['sort_by']) ? 'max(mass_id)' : $_REQUEST['sort_by'];
        $sort_order = empty($_REQUEST['sort_order']) ? 'desc' : $_REQUEST['sort_order'];
        if($_REQUEST['sort_by'] == 'order_sn') $sort_by = 'max(mass_id)';
        $sql = "SELECT count(md.order_id) as order_num, md.mass_key, md.logistics, MAX(md.process) as process, MIN(md.process) as min_process, GROUP_CONCAT(o.order_sn) as order_sn_list FROM ".$ecs->table('mass_delivery')." as md ".
        " LEFT JOIN ".$ecs->table('order_info')." as o ON o.order_id = md.order_id GROUP BY md.mass_key order by $sort_by $sort_order limit $start, $page_size";
        $res = $db->getAll($sql);

        $sql_count = "SELECT count(distinct mass_key) FROM ".$ecs->table('mass_delivery');
        $record_count = $db->getOne($sql_count);



        $mass_list = [];
        foreach ($res as $key => $mass) {
            $mass['id'] = count($res) - $key;
            $order_sn_list = explode(',', $mass['order_sn_list']);
            $sql = "SELECT GROUP_CONCAT(o.order_sn) as no_d_id_list FROM ".$ecs->table('mass_delivery')." as md ".
                " LEFT JOIN ".$ecs->table('order_info')." as o ON o.order_id = md.order_id WHERE o.order_sn ".db_create_in($order_sn_list)." AND md.delivery_id = 0 GROUP BY md.mass_key ";
            $no_d_id_list = $db->getOne($sql);
            $no_d_id_list = explode(',', $no_d_id_list);
            $html = [];

            $logistics = '<select style="display:none" class="form-control change-mass-logistic-method" data-mass-key = "'.$mass['mass_key'].'" name="logistics">
              <option value="">---請選擇---</option>
              <option value="sfplus">順豐到家</option>
              <option value="tqb">宅急便B2(Web)</option>
              <option value="sf">順豐</option>
              <option value="storepicking">門店自取</option>
              <option value="eflocker">順便智能櫃</option>
              <option value="lockerlife">Lockerlife</option>
              <option value="pakpobox">pakpobox</option>
              <option value="ztopickup">中通</option>
              <option value="dpex">DPEX</option>
              <option value="fedex">Fedex</option>
              <option value="4px">四方集運</option>
              <option disabled="disabled">----------</option>
              <option value="wilson">Wilson</option>
              <option value="other">其他</option>
            </select>';

            foreach ($order_sn_list as $order_sn) {
                $html[] = '<a'.(in_array($order_sn, $no_d_id_list) ? " class='red' " : " ").' href="order.php?act=info&order_sn='.$order_sn.'">'.$order_sn.'</a>';
            }
            $html = implode(', ', $html);
            $mass['order_sn'] = $html;
            $mass['process_string'] = ($_LANG['mass_process_'. $mass['process']]) ? $_LANG['mass_process_'.$mass['process']] : $mass['process'];
            $mass['_action'] = "<a href='order.php?act=mass_delivery&mass_key=$mass[mass_key]&list=1'>管理</a> ".$logistics;
            switch ($mass['logistics']) {
                case 'sfplus': $mass['logistics_name'] = '順豐到家'; break;
                case 'tqb': $mass['logistics_name'] = '宅急便B2(Web)'; break;
                case 'sf': $mass['logistics_name'] = '順豐'; break;
                case 'storepicking': $mass['logistics_name'] = '門店自取'; break;
                case 'lockerlife': $mass['logistics_name'] = 'Lockerlife'; break;
                case 'pakpobox': $mass['logistics_name'] = 'pakpobox'; break;
                case 'eflocker': $mass['logistics_name'] = '順便智能櫃'; break;
                case 'ztopickup': $mass['logistics_name'] = '中通'; break;
                case 'dpex': $mass['logistics_name'] = 'DPEX'; break;
                case 'fedex': $mass['logistics_name'] = 'Fedex'; break;
                case '4px': $mass['logistics_name'] = '四方集運'; break;
                case 'wilson': $mass['logistics_name'] = 'Wilson'; break;
                case 'other': $mass['logistics_name'] = '其他'; break;
            }
            $mass_list[] = $mass;
        }
        $controller->ajaxQueryAction($mass_list, $record_count, false);
    }else if(isset($_REQUEST['mass_list'])){
        $smarty->assign('full_page', 1);
        assign_query_info();
        $smarty->display('order_mass_list.htm');
    }else if(isset($_REQUEST['remove'])){
        $mass_key = $_REQUEST['mass_key'];
        $res = $db->query("DELETE FROM ".$ecs->table('mass_delivery'). " WHERE mass_key = '$mass_key'");
        header('Content-Type: application/json');
        echo json_encode(['status'=> 1]);
        exit;
    }else if(isset($_REQUEST['send_mass_mail'])){
        $mass_key = $_REQUEST['mass_key'];
        $id_list  = $_REQUEST['id_list'];
        $res = $deliveryorderController->mass_send_email($mass_key, $id_list);
        header('Content-Type: application/json');
        echo json_encode($res);
        exit;
    }
}
elseif ($_REQUEST['act'] == 'get_delivery_goods_info')
{
    $delivery_id = $_REQUEST['delivery_id'];
    $res = $deliveryorderController->get_delivery_goods_info($delivery_id);

    make_json_result($res);
}
elseif ($_REQUEST['act'] == 'local_pickup_confirm')
{
    $delivery_id = $_REQUEST['delivery_id'];
    $delivery_status = $_REQUEST['delivery_status'];
    $operator = $_REQUEST['operator'];
    $order_id = $_REQUEST['order_id'];
    $res = $deliveryorderController->local_pickup_confirm($delivery_id,$delivery_status,$operator,$order_id);

    make_json_result($res);
}
elseif ($_REQUEST['act'] == 'local_pickup_notified')
{
    $delivery_id = $_REQUEST['delivery_id'];
    $notified = $_REQUEST['notified'];
    $order_id = $_REQUEST['order_id'];
    $operator = $_REQUEST['operator'];
    $res = $deliveryorderController->local_pickup_notified($delivery_id,$notified,$operator,$order_id);

    make_json_result($res);
}
elseif ($_REQUEST['act'] == 'update_order_wholesale_company')
{
    $order_id = $_REQUEST['order_id'];
    $ws_company_id = $_REQUEST['ws_company_id'];
    $res = $orderController->updateOrderWholesaleCompany($order_id,$ws_company_id);
    make_json_result($res);
}
elseif ($_REQUEST['act'] == 'update_order_wholesale_customer')
{
    $order_id = $_REQUEST['order_id'];
    $ws_customer_id = $_REQUEST['ws_customer_id'];
    $res = $orderController->updateOrderWholesaleCustomer($order_id,$ws_customer_id);
    make_json_result($res);
}
elseif ($_REQUEST['act'] == 'update_address_info')
{
    $order_sn = $_REQUEST['order_sn'];
    $address_type = $_REQUEST['addr_arr'][0];
    $address_lift_type = $_REQUEST['addr_arr'][1];
    $address_verify =  $_REQUEST['addr_verify'];
    $res = $orderController->updateAddressInfo($order_sn, $address_type, $address_lift_type, $address_verify);
    make_json_result($res);
}
elseif ($_REQUEST['act'] == 'get_mass_delivery_address_attribute')
{
    $addr_id = $_REQUEST['addr_id'];
    $logistic = $_REQUEST['logistic'];
    $res = $deliveryorderController->getMassDeliveryAddressAttribute($addr_id, $logistic);
    if ($res){
        if ($res['lift'] > 0){
            $res['lift'] = $_LANG['address_type_lift_type_'.$res['lift']];
        }
    }
    make_json_result($res);
}
elseif ($_REQUEST['act'] == 'get_order_goods_cross_check_table')
{
    $erp_admin_id = erp_get_admin_id();
    $role_id = $adminuserController->get_user_role($erp_admin_id);

    $order_id = $_REQUEST['order_id'] ? $_REQUEST['order_id'] : null;

    $smarty->assign('role_id', $role_id);
    $smarty->assign('operator', $erp_admin_id);
    $smarty->assign('order_id', $order_id);
    $smarty->assign('cfg_lang', $GLOBALS['_CFG']['lang']);
    $smarty->assign('lang', $GLOBALS['LANG']);

    make_json_result($smarty->fetch('order_goods_cross_check_for_order_info.htm'));
} elseif ($_REQUEST['act'] == 'add_claim'){

    if (!empty($_REQUEST['rec_id']) && !empty($_REQUEST['order_id']) && !empty($_REQUEST['duration']) && !empty($_REQUEST['claim_amount'])){
        $insuranceController    = new Yoho\cms\Controller\InsuranceController();
        include_once(ROOT_PATH . '/includes/cls_image.php');
        $image = new cls_image($_CFG['bgcolor']);
        //dd($_FILES['goods_img']);
        //dd($_REQUEST);
        if (isset($_FILES['goods_img']['error']))
        {
            $php_maxsize = ini_get('upload_max_filesize');
            $htm_maxsize = '2M';
    
            if ($_FILES['goods_img']['error'] == 0)
            {
                if (!$image->check_img_type($_FILES['goods_img']['type']))
                {
                    sys_msg($_LANG['invalid_goods_img'], 0, array(array("href"=>"order.php?act=info&order_id=".$_REQUEST['order_id'], "text" => "返回訂單")));
                }
            }
            elseif ($_FILES['goods_img']['error'] == 1)
            {
                sys_msg(sprintf($_LANG['goods_img_too_big'], $php_maxsize), 0, array(array("href"=>"order.php?act=info&order_id=".$_REQUEST['order_id'], "text" => "返回訂單")));
            }
            elseif ($_FILES['goods_img']['error'] == 2)
            {
                sys_msg(sprintf($_LANG['goods_img_too_big'], $htm_maxsize), 0, array(array("href"=>"order.php?act=info&order_id=".$_REQUEST['order_id'], "text" => "返回訂單")));
            }
        }

        $original_img = $image->upload_image($_FILES['goods_img'],'', '',false, false);
    
        if ($original_img === false)
        {
            sys_msg($image->error_msg(), 0, array(array("href"=>"order.php?act=info&order_id=".$_REQUEST['order_id'], "text" => "返回訂單")));
        }

        //echo "<br>img: ".$original_img;
    
        $approve_by_name = empty($_SESSION['admin_name'])?'':$_SESSION['admin_name'];

        $approve_time = gmtime();

        $ins_claim_times = $orderController->getClaimTimesByOrderIdOrderGoodsId($_REQUEST['order_id'], $_REQUEST['rec_id']);

        $sql = "SELECT og.goods_number, oi.pay_time, oi.order_sn, og.goods_name ".
                "FROM ".$ecs->table('order_goods')." AS og ".
                "LEFT JOIN ".$ecs->table('order_info')." AS oi ON oi.order_id = og.order_id ".
                "WHERE og.rec_id = '".$_REQUEST['rec_id']."' ";
        
        $ins_info = $db->getRow($sql);

        if (empty($ins_info)){
            sys_msg("訂單資料錯誤，請重新提交", 0, array(array("href"=>"order.php?act=info&order_id=".$_REQUEST['order_id'], "text" => "返回訂單")));
        }

        $ins_claim_left = $ins_info['goods_number'] - $ins_claim_times;

        if ($ins_claim_left <= 0){
            sys_msg("索賠次數已達上限", 0, array(array("href"=>"order.php?act=info&order_id=".$_REQUEST['order_id'], "text" => "返回訂單")));
        }
    
        $is_added = $insuranceController->addClaimRecord($_REQUEST['rec_id'], $_REQUEST['order_id'], $_REQUEST['claim_amount'], $original_img, $approve_time, $approve_by_name);

        if ($is_added){
            if (!empty($_REQUEST['order_id']) && !empty($_REQUEST['claim_amount']) && !empty($ins_info['order_sn'])){
                $sql = "SELECT account_id ".
                        "FROM `actg_order_payments` ".
                        "WHERE order_id = '".$_REQUEST['order_id']."' ".
                        "AND amount >= 0 ".
                        "ORDER BY order_payment_id DESC ";
                $order_payment_ac_id = $db->getOne($sql);
                if (!empty($order_payment_ac_id)){
                    $accountingController = new Yoho\cms\Controller\AccountingController();
                    $doubleEntries = [
                        'sales_order_id'    => $_REQUEST['order_id'],
                        'from_actg_type_id' => $accountingController->getAccountingType(['actg_type_code' => '47001', 'enabled' => 1], 'actg_type_id'),
                        'to_actg_type_id'   => $accountingController->getAccountingTypeByAccountId($order_payment_ac_id, 'actg_type_id'),
                        //'to_actg_type_id'   => $accountingController->getAccountingType(['actg_type_code' => '32200', 'enabled' => 1], 'actg_type_id'),
                        'amount'            => $_REQUEST['claim_amount'],
                        'remark'            => "訂單 ".$ins_info['order_sn']." 保險服務索賠"
                    ];
                    $new_txn_id = $accountingController->insertDoubleEntryTransactions($doubleEntries);
                }
            }
            $order = order_info($_REQUEST['order_id']);
            order_log('insurance_claim', $order, ' '.$ins_info['goods_name'].' - 索賠金額: '.$_REQUEST['claim_amount']);
            sys_msg("已成功提交保險索賠", 0, array(array("href"=>"order.php?act=info&order_id=".$_REQUEST['order_id'], "text" => "返回訂單")));
        } else {
            sys_msg("未能提交保險索賠，請重新提交", 0, array(array("href"=>"order.php?act=info&order_id=".$_REQUEST['order_id'], "text" => "返回訂單")));
        }

    } else {
        sys_msg("資料錯誤，請重新提交", 0, array(array("href"=>"order.php?act=info&order_id=".$_REQUEST['order_id'], "text" => "返回訂單")));
    }
}
/**
 * 取得状态列表
 * @param   string  $type   类型：all | order | shipping | payment
 */
function get_status_list($type = 'all')
{
    global $_LANG;

    $list = array();

    if ($type == 'all' || $type == 'order')
    {
        $pre = $type == 'all' ? 'os_' : '';
        foreach ($_LANG['os'] AS $key => $value)
        {
            $list[$pre . $key] = $value;
        }
    }

    if ($type == 'all' || $type == 'shipping')
    {
        $pre = $type == 'all' ? 'ss_' : '';
        foreach ($_LANG['ss'] AS $key => $value)
        {
            $list[$pre . $key] = $value;
        }
    }

    if ($type == 'all' || $type == 'payment')
    {
        $pre = $type == 'all' ? 'ps_' : '';
        foreach ($_LANG['ps'] AS $key => $value)
        {
            $list[$pre . $key] = $value;
        }
    }
    return $list;
}

/**
 * 更新订单总金额
 * @param   int     $order_id   订单id
 * @return  bool
 */
function update_order_amount($order_id)
{
    include_once(ROOT_PATH . 'includes/lib_order.php');
    //更新订单总金额
    $sql = "UPDATE " . $GLOBALS['ecs']->table('order_info') .
            " SET order_amount = " . order_due_field() .
            " WHERE order_id = '$order_id' LIMIT 1";

    return $GLOBALS['db']->query($sql);
}

/**
 * 返回某个订单可执行的操作列表，包括权限判断
 * @param   array   $order      订单信息 order_status, shipping_status, pay_status
 * @param   bool    $is_cod     支付方式是否货到付款
 * @return  array   可执行的操作  confirm, pay, unpay, prepare, ship, unship, receive, cancel, invalid, return, drop
 * 格式 array('confirm' => true, 'pay' => true)
 */
function operable_list($order)
{
    /* 取得订单状态、发货状态、付款状态 */
    $os = $order['order_status'];
    $ss = $order['shipping_status'];
    $ps = $order['pay_status'];

    $custom_service_list = ['compensate'];

    /* 取得订单操作权限 */
    $actions = $_SESSION['action_list'];
    if ($actions == 'all')
    {
        $priv_list  = array('os' => true, 'ss' => true, 'ps' => true, 'edit' => true);
    }
    else
    {
        $actions    = ',' . $actions . ',';
        $priv_list  = array(
            'os'    => strpos($actions, ',order_os_edit,') !== false,
            'ss'    => strpos($actions, ',order_ss_edit,') !== false,
            'ps'    => strpos($actions, ',order_ps_edit,') !== false,
            'edit'  => strpos($actions, ',order_edit,') !== false
        );
    }

    /* 取得订单支付方式是否货到付款 */
    $payment = payment_info($order['pay_id']);
    $is_cod  = $payment['is_cod'] == 1;

    /* 根据状态返回可执行操作 */
    $list = array();
    if (OS_UNCONFIRMED == $os)
    {
        /* 状态：未确认 => 未付款、未发货 */
        if ($priv_list['os'])
        {
            $list['confirm']    = true; // 确认
            $list['cancel']     = true; // 取消
            if ($is_cod)
            {
                /* 货到付款 */
                if ($priv_list['ss'])
                {
                    //$list['prepare'] = true; // 配货
                    $list['split'] = true; // 分单
                }
            }
            else
            {
                /* 不是货到付款 */
                if ($priv_list['ps'])
                {
                    $list['pay'] = true;  // 付款
                }
            }
        }
    }
    elseif (OS_CONFIRMED == $os || OS_SPLITED == $os || OS_SPLITING_PART == $os)
    {
        /* 状态：已确认 */
        if (PS_UNPAYED == $ps)
        {
            /* 状态：已确认、未付款 */
            if (SS_UNSHIPPED == $ss || SS_PREPARING == $ss)
            {
                /* 状态：已确认、未付款、未发货（或配货中） */
                if ($priv_list['os'])
                {
                    $list['cancel'] = true; // 取消
                }
                if ($is_cod)
                {
                    /* 货到付款 */
                    if ($priv_list['ss'])
                    {
                        if (SS_UNSHIPPED == $ss)
                        {
                            //$list['prepare'] = true; // 配货
                        }
                        $list['split'] = true; // 分单
                    }
                }
                else
                {
                    /* 不是货到付款 */
                    if ($priv_list['ps'])
                    {
                        $list['pay'] = true; // 付款
                    }
                }
            }
            /* 状态：已确认、未付款、发货中 */
            elseif (SS_SHIPPED_ING == $ss || SS_SHIPPED_PART == $ss)
            {
                // 部分分单
                if (OS_SPLITING_PART == $os)
                {
                    // 如果有未付款, 不允許開發貨單
                    // $list['split'] = true; // 分单
                }
                //$list['to_delivery'] = true; // 去发货
                $list['pay'] = true; // 付款
                if ($order['is_wholesale']==true) {
                    $list['unship'] = true;
                }
            }
            else
            {
                /* 状态：已确认、未付款、已发货或已收货 => 货到付款 */
                if ($priv_list['ps'])
                {
                    $list['pay'] = true; // 付款
                }
                if ($priv_list['ss'])
                {
                    if (SS_SHIPPED == $ss)
                    {
                        //$list['receive'] = true; // 收货确认
                    }
                    $list['unship'] = true; // 设为未发货
                    if ($priv_list['os'])
                    {
                        $list['return'] = true; // 退货
                    }
                }
            }
        }
        else /* 状态：已确认、已付款和付款中 */
        {
            $list['invalid'] = true; // 退款
            $list['customer_service'] = true; // 售后

            /* 状态：已确认、已付款和付款中 */
            if (SS_UNSHIPPED == $ss || SS_PREPARING == $ss)
            {
                $list['hold_order'] = true;
                $list['unhold_order'] = true;
                /* 状态：已确认、已付款和付款中、未发货（配货中） => 不是货到付款 */
                if ($priv_list['ss'])
                {
                    if (SS_UNSHIPPED == $ss)
                    {
                        //$list['prepare'] = true; // 配货
                    }
                    $list['split'] = true; // 分单
                    $list['quick_ship'] = true;
                }
                if ($priv_list['ps'])
                {
                    $list['unpay'] = true; // 设为未付款
                    if ($priv_list['os'])
                    {
                        $list['cancel'] = true; // 取消
                    }
                }
            }
            /* 状态：已确认、已付款、发货中[已发货(部分商品) || 发货中(处理分单)] */
            elseif (SS_SHIPPED_ING == $ss || SS_SHIPPED_PART == $ss)
            {
                $list['hold_order'] = true;
                $list['unhold_order'] = true;
                // 部分分单
                if (OS_SPLITING_PART == $os)
                {
                    $list['split'] = true; // 分单
                    $list['quick_ship'] = true;
                    //$list['to_delivery'] = true; // 去发货
                    //$list['prepare'] = true;
                } elseif( OS_CONFIRMED == $os && SS_SHIPPED_PART == $ss) { /* 已確認,已付款,已發貨(部分產品) */
                    $list['finish_order'] = true; //In this order status, sales team maybe need update sghiiping / discount fee, need this btn to manual finish order.
                } else {
                    //$list['to_delivery'] = true; // 去发货
                    //$list['prepare'] = true;
                    $list['split'] = true; // 分单
                    $list['quick_ship'] = true;
                }

                if ($order['is_wholesale']==true) {
                    $list['unship'] = true;
                }
            }
            else
            {
                /* 状态：已确认、已付款和付款中、已发货或已收货 */
                if ($priv_list['ss'])
                {
                    if (SS_SHIPPED == $ss)
                    {
                        //$list['receive'] = true; // 收货确认
                    }
                    if (!$is_cod)
                    {
                        $list['unship'] = true; // 设为未发货
                    }
                }
                if ($priv_list['ps'] && $is_cod)
                {
                    $list['unpay']  = true; // 设为未付款
                }
                if ($priv_list['os'] && $priv_list['ss'] && $priv_list['ps'])
                {
                    $list['return'] = true; // 退货（包括退款）
                }
            }
        }
        // Orders with user 11111111, 88888888, 99999999
        //if (in_array($order['user_id'], array('4940','5022','5023')))
        //REMARK: there isn't an "is_wholesale" in order,
        //TODO: make use of the new order_type field with constant OrderController::DB_ORDER_TYPE_WHOLESALE
        if($order['is_wholesale']==true)
        {
            $list['pay'] = false;
            //$list['unship'] = false;
            //$list['invalid'] = false;
            $list['wholesale_pay'] = ($ps==PS_PAYED?false:true); // 批發訂單付款
            $list['quick_ship'] = true;
            $list['split'] = true;
        }
    }
    elseif (OS_CANCELED == $os)
    {
        /* 状态：取消 */
        if ($priv_list['os'])
        {
            $list['confirm'] = true;
        }
        if ($priv_list['edit'])
        {
            $list['remove'] = true;
        }
    }
    elseif (OS_INVALID == $os)
    {
        /* 状态：退款 */
        if ($priv_list['os'])
        {
            $list['confirm'] = true;
        }
        if ($priv_list['edit'])
        {
            $list['remove'] = true;
        }
    }
    elseif (OS_WAITING_REFUND == $os)
    {
        /* 状态：等待全單退款 */
        if ($priv_list['os'])
        {
            // $list['confirm'] = true;
        }
        if ($priv_list['edit'])
        {
            // $list['remove'] = true;
        }

        if ($order['origin_pay_code'] == 'qfwechatpay')
        {
            $list['force_wechat_refund'] = true;
        }
        $list['customer_service'] = true; // 售后
    }
    elseif (OS_RETURNED == $os)
    {
        /* 状态：退货 */
        if ($priv_list['os'])
        {
            $list['confirm'] = true;
        }
    }

    /* 修正发货操作 */
    if (!empty($list['split']))
    {
        /* 如果是团购活动且未处理成功，不能发货 */
        if ($order['extension_code'] == 'group_buy')
        {
            include_once(ROOT_PATH . 'includes/lib_goods.php');
            $group_buy = group_buy_info(intval($order['extension_id']));
            if ($group_buy['status'] != GBS_SUCCEED)
            {
                unset($list['split']);
                unset($list['quick_ship']);
                unset($list['to_delivery']);
            }
        }

        /* 如果部分发货 不允许 取消 订单 */
        if (order_deliveryed($order['order_id']))
        {
            $list['return'] = true; // 退货（包括退款）
            unset($list['cancel']); // 取消
        }
    }

    /* 出貨後不能退款 */
    if(SS_UNSHIPPED != $ss) {
        $list['invalid'] = false;
    }

    /* 儲存備註 */
    $list['after_service'] = true;

    if (!empty($order['is_hold'])) {
        unset($list['invalid']);
        unset($list['customer_service']);
        unset($list['split']);
        unset($list['quick_ship']);
        unset($list['to_delivery']);
    }

    if ($order['order_type'] == Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE) {
        unset($list['invalid']); // 20190528: Wholesale order should not be refunded -> require to handle previous payments first
    }

    /* Set cs list */
    if ($list['customer_service']) {
        foreach ($custom_service_list as $item) {
            $list[$item] = true;
        }
    }

    return $list;
}

/**
 * 处理编辑订单时订单金额变动
 * @param   array   $order  订单信息
 * @param   array   $msgs   提示信息
 * @param   array   $links  链接信息
 */
function handle_order_money_change($order, &$msgs, &$links)
{
    $order_id = $order['order_id'];
    $refundController = new Yoho\cms\Controller\RefundController();
    if ($order['pay_status'] == PS_PAYED || $order['pay_status'] == PS_PAYING || $order['order_type'] == Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE)
    {
        /* 应付款金额 */
	    if($order['order_type'] == Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE){
	        $money_dues = $order['order_amount'] - $order['actg_paid_amount'];
	    } else {
        	$money_dues = $order['order_amount'];
	    }

        // get processing refund amount
        $refund = $refundController->currentProcessRefundAmount($order_id);

        // need extra pay
	    if ($money_dues > 0){
	        $order['pay_status']  = PS_UNPAYED;
	        $order['pay_time']    = 0;
	        $order['fully_paid']  = 0;
	            $order['pay_status']  = PS_UNPAYED;
	            $order['pay_time']    = 0;
	            /* 修改订单为未付款 */
	        update_order($order_id, $order);
            order_log('handle_payment_status', $order, '');
            $req = $refundController->rejectProcessingRefund($order_id);
            if (!empty($req)) {
                order_log('modify_refund', $order, ': 取消 #' . implode(', #', $req));
            }
	            $msgs[]     = $GLOBALS['_LANG']['amount_increase'];
	            $links[]    = array('text' => $GLOBALS['_LANG']['order_info'], 'href' => 'order.php?act=info&order_id=' . $order_id);
        }
	    elseif ($money_dues == 0 && $order['order_type'] == Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE){
	        $old_pay_status = $order['pay_status'];
	        $order['fully_paid']  = 1;
	        $order['pay_status']  = PS_PAYED;
	        $order['pay_time']    = gmtime();
	        if($old_pay_status != $order['pay_status']){
	            update_order($order_id, $order);
	            order_log('handle_payment_status', $order, '');
	        }
        }
        // extra pay and refund not needed 
        elseif ($money_dues == 0)
        {
            $req = $refundController->rejectProcessingRefund($order_id);
            if (!empty($req)) {
                order_log('modify_refund', $order, ': 取消 #' . implode(', #', $req));
            }
        }
        // need refund and 1.create new refund or 2.edit existing refund
        elseif ($money_dues < 0 && $money_dues != -$refund)
        {
            // Need extra refund
            if($money_dues < -$refund)
            {
                $anonymous  = $order['user_id'] > 0 ? 0 : 1;
                $msgs[]     = $GLOBALS['_LANG']['amount_decrease'];
                $links[]    = array('text' => $GLOBALS['_LANG']['refund'], 'href' => 'order.php?act=process&func=load_refund&anonymous=' .
                    $anonymous . '&order_id=' . $order_id . '&refund_amount=' . abs($money_dues + $refund));
            }
            // Need reduce refund amount
            else
            {
                $accountingController = new Yoho\cms\Controller\AccountingController();
                $curr_refund = $refund;
                // loop all request until total refund = money due
                while($curr_refund > -$money_dues)
                {
                    $res = $GLOBALS['db']->getRow("SELECT * FROM " . $GLOBALS['ecs']->table('refund_requests') . " WHERE order_id = $order_id AND status " . db_create_in(array(REFUND_PENDING, REFUND_PROCESS)) . " AND refund_type != " . REFUND_COMPENSATE . " ORDER BY rec_id DESC LIMIT 1");
                    $refund_amount = floatval($res['refund_amount']) + value_of_integral(intval($res['integral_amount']));

                    // if new refund amount > 0 ? update amount : reject request
                    if($refund_amount > $curr_refund + $money_dues) {
                        $tmp_amount = $refund_amount - ($curr_refund + $money_dues);
                        $curr_refund = 0;

                        $new_refund = [
                            'rec_id'            => $res['rec_id'],
                            'sales_order_id'    => $res['order_id'],
                            'amount'            => $tmp_amount,
                            'order_sn'          => $order['order_sn'],
                        ];
                        if(!empty($res['refund_amount']) && bccomp($res['refund_amount'], 0) == 1) {
                            $GLOBALS['db']->query("UPDATE " . $GLOBALS['ecs']->table('refund_requests') . " SET refund_amount = $tmp_amount WHERE rec_id = $res[rec_id]");
                            $new_refund_type = 0;
                        } else if(!empty($res['integral_amount'])) {
                            $integral_amount = integral_of_value($tmp_amount);
                            $GLOBALS['db']->query("UPDATE " . $GLOBALS['ecs']->table('refund_requests') . " SET integral_amount = $integral_amount WHERE rec_id = $res[rec_id]");
                            $new_refund['integral_amount'] = $integral_amount;
                            $new_refund_type = 1;
                        }
                        $apply_txn_id = $accountingController->insertRefundUpdateTransactions($new_refund, 0, $new_refund_type);
                        $GLOBALS['db']->query("UPDATE " . $GLOBALS['ecs']->table('refund_requests') . " SET apply_txn_id = $apply_txn_id WHERE rec_id = $res[rec_id]");
                    } else {
                        $tmp_amount = 0;
                        $curr_refund -= $refund_amount;
                        $complete_txn_id = $accountingController->insertRefundUpdateTransactions(['rec_id' => $res['rec_id']], 1);
                        $GLOBALS['db']->query("UPDATE " . $GLOBALS['ecs']->table('refund_requests') . " SET update_time = " . gmtime() . ", status = " . REFUND_REJECT . ", operator_id = 1, complete_txn_id = $complete_txn_id WHERE rec_id = $res[rec_id]");
                    }
                    $update_id[] = $res['rec_id'];
                }
                if(!empty($update_id))
                    order_log('modify_refund', $order, ': 編輯 #' . implode(', #', $update_id));
            }
        }
    } else if ($order['order_amount'] <= 0) {
        $order['pay_status']  = PS_PAYED;
        $order['pay_time']    = gmtime();

        update_order($order_id, $order);
        order_log('handle_payment_status', $order, '');
    }
}

/**
 *  获取订单列表信息
 *
 * @access  public
 * @param
 *
 * @return void
 */
function order_list()
{
    // howang: Increase memory limit
    ini_set('memory_limit', '256M');
    
    global $userController,$erpController;

    // $result = get_filter();
    // if ($result === false)
    {
        /* 过滤信息 */
        $_REQUEST['order_sn'] = empty($_REQUEST['order_sn']) ? '' : trim($_REQUEST['order_sn']);
        if (!empty($_GET['is_ajax']) && $_GET['is_ajax'] == 1)
        {
            $_REQUEST['consignee'] = json_str_iconv($_REQUEST['consignee']);
            //$_REQUEST['address'] = json_str_iconv($_REQUEST['address']);
        }
        $_REQUEST['consignee'] = empty($_REQUEST['consignee']) ? '' : trim($_REQUEST['consignee']);
        $_REQUEST['email'] = empty($_REQUEST['email']) ? '' : trim($_REQUEST['email']);
        $_REQUEST['address'] = empty($_REQUEST['address']) ? '' : trim($_REQUEST['address']);
        $_REQUEST['zipcode'] = empty($_REQUEST['zipcode']) ? '' : trim($_REQUEST['zipcode']);
        $_REQUEST['tel'] = empty($_REQUEST['tel']) ? '' : trim($_REQUEST['tel']);
        $_REQUEST['mobile'] = empty($_REQUEST['mobile']) ? 0 : intval($_REQUEST['mobile']);
        $_REQUEST['country'] = empty($_REQUEST['country']) ? 0 : intval($_REQUEST['country']);
        $_REQUEST['province'] = empty($_REQUEST['province']) ? 0 : intval($_REQUEST['province']);
        $_REQUEST['city'] = empty($_REQUEST['city']) ? 0 : intval($_REQUEST['city']);
        $_REQUEST['district'] = empty($_REQUEST['district']) ? 0 : intval($_REQUEST['district']);
        $_REQUEST['shipping_id'] = empty($_REQUEST['shipping_id']) ? 0 : intval($_REQUEST['shipping_id']);
        $_REQUEST['shipping_ids'] = empty($_REQUEST['shipping_ids']) ? 0 : implode(',',array_map('intval',explode(',',$_REQUEST['shipping_ids'])));
        $_REQUEST['pay_id'] = empty($_REQUEST['pay_id']) ? 0 : intval($_REQUEST['pay_id']);
        $_REQUEST['pay_ids'] = empty($_REQUEST['pay_ids']) ? 0 : implode(',',array_map('intval',explode(',',$_REQUEST['pay_ids'])));
        $_REQUEST['order_status'] = isset($_REQUEST['order_status']) ? intval($_REQUEST['order_status']) : -1;
        $_REQUEST['shipping_status'] = isset($_REQUEST['shipping_status']) ? $_REQUEST['shipping_status'] : -1;
        $_REQUEST['pay_status'] = isset($_REQUEST['pay_status']) ? intval($_REQUEST['pay_status']) : -1;
        $_REQUEST['user_id'] = empty($_REQUEST['user_id']) ? 0 : intval($_REQUEST['user_id']);
        $_REQUEST['user_ids'] = empty($_REQUEST['user_ids']) ? '' : trim($_REQUEST['user_ids']);
        $_REQUEST['user_name'] = empty($_REQUEST['user_name']) ? '' : trim($_REQUEST['user_name']);
        $_REQUEST['composite_status'] = isset($_REQUEST['composite_status']) ? intval($_REQUEST['composite_status']) : -1;
        $_REQUEST['group_buy_id'] = isset($_REQUEST['group_buy_id']) ? intval($_REQUEST['group_buy_id']) : 0;
        $_REQUEST['platform'] = empty($_REQUEST['platform']) ? '' : trim($_REQUEST['platform']);

        $_REQUEST['sort_by'] = empty($_REQUEST['sort_by']) ? 'add_time' : trim($_REQUEST['sort_by']);
        $_REQUEST['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);

        $_REQUEST['start_time'] = empty($_REQUEST['start_time']) ? '' : (strpos($_REQUEST['start_time'], '-') > 0 ?  local_strtotime($_REQUEST['start_time']) : $_REQUEST['start_time']);
        $_REQUEST['end_time'] = empty($_REQUEST['end_time']) ? '' : (strpos($_REQUEST['end_time'], '-') > 0 ?  local_strtotime($_REQUEST['end_time']) : $_REQUEST['end_time']);

        $_REQUEST['pay_start_time'] = empty($_REQUEST['pay_start_time']) ? '' : (strpos($_REQUEST['pay_start_time'], '-') > 0 ?  local_strtotime($_REQUEST['pay_start_time']) : $_REQUEST['pay_start_time']);
        $_REQUEST['pay_end_time'] = empty($_REQUEST['pay_end_time']) ? '' : (strpos($_REQUEST['pay_end_time'], '-') > 0 ?  local_strtotime($_REQUEST['pay_end_time']) : $_REQUEST['pay_end_time']);
        $_REQUEST['ship_start_time'] = empty($_REQUEST['ship_start_time']) ? '' : (strpos($_REQUEST['ship_start_time'], '-') > 0 ?  local_strtotime($_REQUEST['ship_start_time']) : $_REQUEST['ship_start_time']);
        $_REQUEST['ship_end_time'] = empty($_REQUEST['ship_end_time']) ? '' : (strpos($_REQUEST['ship_end_time'], '-') > 0 ?  local_strtotime($_REQUEST['ship_end_time']) : $_REQUEST['ship_end_time']);
        $_REQUEST['salesperson'] = empty($_REQUEST['salesperson']) ? '' : trim($_REQUEST['salesperson']);
        $_REQUEST['have_discount'] = empty($_REQUEST['have_discount']) ? 0 : intval($_REQUEST['have_discount']);
        $_REQUEST['includes_goods'] = empty($_REQUEST['includes_goods']) ? '' : trim($_REQUEST['includes_goods']);
        $_REQUEST['wholesale_receivable'] = empty($_REQUEST['wholesale_receivable']) ? 0 : intval($_REQUEST['wholesale_receivable']);
        $_REQUEST['serial_numbers'] = empty($_REQUEST['serial_numbers']) ? '' : trim($_REQUEST['serial_numbers']);
        $_REQUEST['flashdeal_id'] = empty($_REQUEST['flashdeal_id']) ? 0 : intval($_REQUEST['flashdeal_id']);
        $_REQUEST['replacement'] = empty($_REQUEST['replacement']) ? 0 : intval($_REQUEST['replacement']);
        $_REQUEST['have_preorder'] = empty($_REQUEST['have_preorder']) ? 0 : intval($_REQUEST['have_preorder']);
        $_REQUEST['stock_incorrect'] = empty($_REQUEST['stock_incorrect']) ? 0 : intval($_REQUEST['stock_incorrect']);
        $_REQUEST['display_order_goods'] = empty($_REQUEST['display_order_goods']) ? 0 : intval($_REQUEST['display_order_goods']);
        $_REQUEST['all_in_stock'] = (in_array($_REQUEST['composite_status'], [CS_AWAIT_SHIP,CS_AWAIT_SHIP_HK,CS_AWAIT_PICK_HK,CS_AWAIT_SHIP_NON_HK,CS_AWAIT_SHIP_HK_YOHO, CS_AWAIT_SHIP_LOCKER,CS_AWAIT_HK_FLS])) ? (empty($_REQUEST['all_in_stock']) ? 0 : intval($_REQUEST['all_in_stock'])) : 0;
        $_REQUEST['need_purchase'] = (in_array($_REQUEST['composite_status'], [CS_AWAIT_SHIP,CS_AWAIT_SHIP_HK,CS_AWAIT_PICK_HK,CS_AWAIT_SHIP_NON_HK,CS_AWAIT_SHIP_HK_YOHO, CS_AWAIT_SHIP_LOCKER,CS_AWAIT_HK_FLS])) ? (empty($_REQUEST['need_purchase']) ? 0 : intval($_REQUEST['need_purchase'])) : 0;
        $_REQUEST['invoice_no'] = empty($_REQUEST['invoice_no']) ? '' : trim($_REQUEST['invoice_no']);
        $_REQUEST['have_partial_ship_history'] = empty($_REQUEST['have_partial_ship_history']) ? 0 : intval($_REQUEST['have_partial_ship_history']);
        $_REQUEST['is_wholesale'] = empty($_REQUEST['is_wholesale']) ? 0 : intval($_REQUEST['is_wholesale']);
        $_REQUEST['logistics_id'] = empty($_REQUEST['logistics_id']) ? 0 : intval($_REQUEST['logistics_id']);
        $_REQUEST['sa_id'] = empty($_REQUEST['sa_id']) ? 0 : intval($_REQUEST['sa_id']);
        $_REQUEST['display_sales_order'] = isset($_REQUEST['display_sales_order']) ?  $_REQUEST['display_sales_order'] : 0;
        $_REQUEST['display_sales_order'] = $_REQUEST['composite_status'] == CS_AWAIT_SHIP_HK_YOHO ? 2 : $_REQUEST['display_sales_order'];
        $_REQUEST['display_normal_order'] = isset($_REQUEST['display_normal_order']) ?  $_REQUEST['display_normal_order'] : 0;
        $_REQUEST['start'] = isset($_REQUEST['start']) ?  $_REQUEST['start'] : 0;
        //$_REQUEST['page_size'] = isset($_REQUEST['page_size']) ?  $_REQUEST['page_size'] : 50;
        $_REQUEST['is_fls'] = isset($_REQUEST['is_fls']) ?  $_REQUEST['is_fls'] : 0;
        $_REQUEST['total_fee'] = empty($_REQUEST['total_fee']) ? -1 : floatval($_REQUEST['total_fee']);
        
        if ($_REQUEST['composite_status'] == CS_AWAIT_HK_FLS) {
            $_REQUEST['all_in_stock'] = 1;
        }

        $where = 'WHERE 1 ';
        if (!$_SESSION['manage_cost'])
        {
            // Hide 88888888, 99999999 orders from non-admin users
            //Deprecated: after deployed new order_type this logic does not apply anymore
            //$where .= " AND o.user_id NOT IN (5022,5023)";
        }
        if ($_REQUEST['order_sn'])
        {
            $where .= " AND o.order_sn LIKE '%" . mysql_like_quote($_REQUEST['order_sn']) . "%'";
        }
        if ($_REQUEST['consignee'])
        {
            $where .= " AND o.consignee LIKE '%" . mysql_like_quote($_REQUEST['consignee']) . "%'";
        }
        if ($_REQUEST['email'])
        {
            $where .= " AND o.email LIKE '%" . mysql_like_quote($_REQUEST['email']) . "%'";
        }
        if ($_REQUEST['address'])
        {
            $where .= " AND o.address LIKE '%" . mysql_like_quote($_REQUEST['address']) . "%'";
        }
        if ($_REQUEST['zipcode'])
        {
            $where .= " AND o.zipcode LIKE '%" . mysql_like_quote($_REQUEST['zipcode']) . "%'";
        }
        if ($_REQUEST['tel'])
        {
            $where .= " AND o.tel LIKE '%" . mysql_like_quote($_REQUEST['tel']) . "%'";
        }
        if ($_REQUEST['mobile'])
        {
            $where .= " AND o.mobile LIKE '%" .mysql_like_quote($_REQUEST['mobile']) . "%'";
        }
        if ($_REQUEST['country'])
        {
            $where .= " AND o.country = '$_REQUEST[country]'";
        }
        if ($_REQUEST['province'])
        {
            $where .= " AND o.province = '$_REQUEST[province]'";
        }
        if ($_REQUEST['city'])
        {
            $where .= " AND o.city = '$_REQUEST[city]'";
        }
        if ($_REQUEST['district'])
        {
            $where .= " AND o.district = '$_REQUEST[district]'";
        }
        if ($_REQUEST['shipping_id'])
        {
            $where .= " AND o.shipping_id  = '$_REQUEST[shipping_id]'";
        }
        if ($_REQUEST['shipping_ids'])
        {
            $where .= " AND o.shipping_id  IN ($_REQUEST[shipping_ids])";
        }
        if ($_REQUEST['pay_id'])
        {
            $where .= " AND o.pay_id  = '$_REQUEST[pay_id]'";
        }
        if ($_REQUEST['pay_ids'])
        {
            $where .= " AND o.pay_id  IN ($_REQUEST[pay_ids])";
        }
        if ($_REQUEST['order_status'] != -1)
        {
            $where .= " AND o.order_status  = '$_REQUEST[order_status]'";
        } elseif (!in_array($_REQUEST['composite_status'], [OS_CANCELED, CS_CANCELED_SELF, CS_CANCELED_STAFF]) && !($_REQUEST['order_sn'] || $_REQUEST['user_id'] || $_REQUEST['user_ids'] || $_REQUEST['user_name'])) {
            $where .= " AND o.order_status != '" . OS_CANCELED . "'";
        }
        if ($_REQUEST['shipping_status'] != -1)
        {
            $where .= " AND o.shipping_status ".db_create_in(array_map('intval', explode(',',$_REQUEST['shipping_status'])));
        }
        if ($_REQUEST['pay_status'] != -1)
        {
            $where .= " AND o.pay_status = '$_REQUEST[pay_status]'";
        }
        if ($_REQUEST['user_id'])
        {
            $where .= " AND o.user_id = '$_REQUEST[user_id]'";
        }
        if ($_REQUEST['user_ids'])
        {
            $where .= " AND o.user_id " . db_create_in(array_map('intval', explode(',',$_REQUEST['user_ids'])));
        }
        if ($_REQUEST['user_name'])
        {
            $where .= " AND u.user_name LIKE '%" . mysql_like_quote($_REQUEST['user_name']) . "%'";
        }
        if ($_REQUEST['start_time'])
        {
            $where .= " AND o.add_time >= '$_REQUEST[start_time]'";
        }
        if ($_REQUEST['end_time'])
        {
            $where .= " AND o.add_time <= '$_REQUEST[end_time]'";
        }
        if ($_REQUEST['pay_start_time'])
        {
            $where .= " AND o.pay_time >= '$_REQUEST[pay_start_time]'";
        }
        if ($_REQUEST['pay_end_time'])
        {
            $where .= " AND o.pay_time <= '$_REQUEST[pay_end_time]'";
        }
        if ($_REQUEST['ship_start_time'])
        {
            $where .= " AND o.shipping_time >= '$_REQUEST[ship_start_time]'";
        }
        if ($_REQUEST['ship_end_time'])
        {
            $where .= " AND o.shipping_time <= '$_REQUEST[ship_end_time]'";
        }
        if ($_REQUEST['salesperson'])
        {
            $where .= " AND o.salesperson = '" . $_REQUEST['salesperson'] . "'";
        }
        if($_REQUEST['display_sales_order'] == 1){
            $where .=  " AND o.order_type = '" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT . "' ";
        }
        if($_REQUEST['display_normal_order'] || $_REQUEST['display_sales_order'] == 2){
            $where .= " AND (order_type <> '" . Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT . "' OR order_type IS NULL) ";
			$where .= " AND o.user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).") ";
        }
        if($_REQUEST['platform'])
        {
            $where .= " AND o.platform = '$_REQUEST[platform]'";
        }
	   
        if ($_REQUEST['have_discount'])
        {
            $where .= " AND o.discount > 0";
        }
        if ($_REQUEST['includes_goods'])
        {
            if (!preg_match('/^([0-9]+,)*[0-9]+$/', $_REQUEST['includes_goods']))
            {
                $error_msg = '「包含產品ID」欄位輸入錯誤，請輸入以逗號(,)分隔的產品ID';
                if ($_REQUEST['is_ajax'])
                {
                    make_json_error($error_msg);
                }
                else
                {
                    sys_msg($error_msg);
                }
            }
            $where .=" AND o.order_id IN (" .
                        "SELECT og.`order_id` " .
                        "FROM " . $GLOBALS['ecs']->table('order_goods') . " as og " .
                        "WHERE og.goods_id IN ({$_REQUEST['includes_goods']}) " .
                    ")";
        }
        if ($_REQUEST['wholesale_receivable'])
        {
            //To handle the old 999999
            $where .= " AND (o.order_type='".Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE."' OR o.user_id IN ('".implode("','",$userController->getWholesaleUserIds())."') OR o.user_id = 5022) AND o.fully_paid = 0";
        }
        if ($_REQUEST['serial_numbers'])
        {
            $where .= " AND (o.serial_numbers LIKE '%" . mysql_like_quote($_REQUEST['serial_numbers']) . "%' OR (o.order_type " . db_create_in(array( Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT, Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE)) . " AND o.type_sn LIKE '%" . mysql_like_quote($_REQUEST['serial_numbers']) . "%') )";
        }
        if ($_REQUEST['flashdeal_id'])
        {
            $where .=" AND EXISTS (" .
                        "SELECT og.`order_id` " .
                        "FROM " . $GLOBALS['ecs']->table('order_goods') . " as og " .
                        "WHERE og.order_id = o.order_id " .
                        "AND og.flashdeal_id = '" . $_REQUEST['flashdeal_id'] . "' " .
                    ")";
        }
        if ($_REQUEST['replacement'])
        {
            $where .= " AND o.postscript LIKE '%換貨，%'";
        }
        if ($_REQUEST['have_preorder'])
        {
            $where .= " AND (o.postscript LIKE '%訂貨%天%' OR o.order_remarks LIKE '%訂貨%天%' OR o.to_buyer LIKE '%訂貨%天%' OR o.order_id IN (SELECT order_id FROM " .$GLOBALS['ecs']->table('order_goods'). " where planned_delivery_date > 0 group by order_id )) ";
        }
        if ($_REQUEST['stock_incorrect'])
        {
            $where .= " AND o.platform != 'pos' " .
                    "AND EXISTS (" .
                        "SELECT og.`order_id` " .
                        "FROM " . $GLOBALS['ecs']->table('order_goods') . " as og " .
                        "WHERE og.order_id = o.order_id AND og.replaced_qty = 0 " .
                        "GROUP BY og.rec_id " .
                        "HAVING SUM(cast(og.goods_number as signed) - cast(og.delivery_qty as signed)) > 0" .
                    ") AND o.order_id NOT IN (" .
                        "SELECT DISTINCT replace_order_id FROM " . $GLOBALS['ecs']->table('order_info') .
                        " WHERE replace_order_id != 0" .
                    ")";
        }
        if ($_REQUEST['invoice_no'])
        {
            $where .= " AND (do.invoice_no LIKE '%" . mysql_like_quote($_REQUEST['invoice_no']) . "%' OR o.invoice_no LIKE '%" . mysql_like_quote($_REQUEST['invoice_no']) . "%') ";
        }
        if ($_REQUEST['logistics_id'] > 0)
        {
            $where .= " AND do.logistics_id = $_REQUEST[logistics_id] ";
        }

        if ($_REQUEST['is_fls']  > 0)
        {
            $where .= " AND do.is_fls = 1 ";
        }

        if ($_REQUEST['total_fee'] != -1)
        {
            $where .= " AND (" .
                "o.goods_amount + o.tax + o.shipping_fee + o.insure_fee + o.pay_fee + o.pack_fee + o.card_fee - o.coupon - o.integral_money - o.money_paid = $_REQUEST[total_fee] OR " .
                "o.order_amount = $_REQUEST[total_fee] OR " .
                "o.money_paid = $_REQUEST[total_fee]" .
            ") ";
        }

        if ($_REQUEST['have_partial_ship_history'])
        {
            $where .= " AND o.order_id IN (" .
                        "SELECT oa.`order_id` " .
                        "FROM " . $GLOBALS['ecs']->table('order_action') . " as oa " .
                        "WHERE oa.shipping_status = 4 " .
                    ")";
        }
        if($_REQUEST['is_wholesale']==1){
            $where .= " AND (o.user_id IN (".implode(',', $userController->getWholesaleUserIds()).") OR o.order_type='".Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE."')";
        }elseif(!$_REQUEST['wholesale_receivable']){
            $where .= " AND (o.order_type <> '".Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE."' OR o.order_type IS NULL) AND o.user_id NOT IN (".implode(',', $userController->getWholesaleUserIds()).")";
        }

        if ($_REQUEST['sa_id'] > 0)
        {
            $where .= " AND o.type_id = $_REQUEST[sa_id] AND o.order_type = 'sa' ";
        }

        if ( in_array($_REQUEST['composite_status'], [CS_AWAIT_SHIP, CS_AWAIT_SHIP_HK,CS_AWAIT_SHIP_LOCKER,CS_AWAIT_PICK_HK,CS_AWAIT_SHIP_NON_HK,CS_AWAIT_SHIP_HK_YOHO,CS_AWAIT_HK_FLS,CS_AWAIT_HK_WILSON])) {
            $pagination_by_sql = false;
        } else {
            $pagination_by_sql = true;
        }
        
        //综合状态
        switch($_REQUEST['composite_status'])
        {
            case CS_AWAIT_PAY :
                $where .= order_query_sql('await_pay', 'o.');
                break;

            case CS_AWAIT_SHIP :
                $where .= order_query_sql('await_ship', 'o.');
                break;

            case CS_FINISHED :
                $where .= order_query_sql('finished', 'o.');
                break;

            case CS_SHIPPED :
                $where .= order_query_sql('shipped', 'o.');
                break;

            case PS_PAYING :
                if ($_REQUEST['composite_status'] != -1)
                {
                    $where .= " AND o.pay_status = '$_REQUEST[composite_status]' ";
                }
                break;

            case CS_AWAIT_SHIP_HK : case CS_AWAIT_SHIP_HK_YOHO :
                //if ($_REQUEST['all_in_stock'] > 0 || $_REQUEST['need_purchase'] > 0) {
                    $where .= order_query_sql('await_ship', 'o.');
                //} else {
                //    $where .= order_query_sql('await_ship', 'o.') . " AND o.shipping_id IN (3,9,10) AND o.country = 3409 ";
                //}
                break;
            case CS_AWAIT_HK_FLS :
                // if ($_REQUEST['all_in_stock'] > 0 || $_REQUEST['need_purchase'] > 0) {
                    $where .= order_query_sql('await_ship', 'o.');
                //} else {
                    
                //}

                break;
            case CS_AWAIT_HK_WILSON :
                // if ($_REQUEST['all_in_stock'] > 0 || $_REQUEST['need_purchase'] > 0) {
                    $where .= order_query_sql('await_ship', 'o.');
                //} else {
                    
                //}

                break;    
            case CS_AWAIT_SHIP_LOCKER :
                //if ($_REQUEST['all_in_stock'] > 0 || $_REQUEST['need_purchase'] > 0) {
                    $where .= order_query_sql('await_ship', 'o.');
                //} else {
                //   $where .= order_query_sql('await_ship', 'o.') . " AND o.shipping_id = 8 AND o.country = 3409 ";
                //}
                break;

            case CS_AWAIT_PICK_HK :
                //if ($_REQUEST['all_in_stock'] > 0 || $_REQUEST['need_purchase'] > 0) {
                    $where .= order_query_sql('await_ship', 'o.');
                //} else {
                //    $where .= order_query_sql('await_ship', 'o.') . " AND o.shipping_id IN (2,5) ";
                //}
                break;

            case CS_AWAIT_SHIP_NON_HK :
                //if ($_REQUEST['all_in_stock'] > 0 || $_REQUEST['need_purchase'] > 0) {
                    $where .= order_query_sql('await_ship', 'o.');
                //} else {
                //    $where .= order_query_sql('await_ship', 'o.') . " AND o.shipping_id = 3 AND o.country != 3409 ";
                //}
                break;

            case CS_FINISHED_PICK :
                $where .= order_query_sql('shipped_unconfirm', 'o.') . " AND o.shipping_id IN (2,5) ";

                // if ($_REQUEST['display_order_not_packed'] == 1) 
                // {
                //     $where .= " AND do.status != 4 And do.status != 3 And do.logistics_id = 7  ";
                   
                // } 
                // else if ($_REQUEST['display_order_not_pickup'] == 1)
                // {
                //     $where .= " AND do.status != 3 And do.logistics_id = 7  ";
                // }
               
                if ($_REQUEST['display_store_pickup_filter'] == 1) // 未Pack貨
                {
                    $where .= " AND do.status != 4 And do.status != 3 And do.logistics_id = 7  ";
                } 
                else if ($_REQUEST['display_store_pickup_filter'] == 2) //已Pack貨(未通知取件)
                {
                    $where .= " AND do.status = 4 And do.pickup_notified = 0 And do.logistics_id = 7  ";
                }
                else if ($_REQUEST['display_store_pickup_filter'] == 3) //未取貨(已通知取件)
                {
                    $where .= " AND do.status != 3 And do.pickup_notified = 1 And do.logistics_id = 7  ";   
                }
                else if ($_REQUEST['display_store_pickup_filter'] == 4) //未取貨
                {
                    $where .= " AND do.status != 3 And do.logistics_id = 7  ";
                }

                break;

            case CS_PAYED_CASH :
                $where .= " AND o.order_status IN (" . OS_CONFIRMED . ',' . OS_SPLITED . ',' . OS_SPLITING_PART . ") ".
                          " AND o.money_paid > 0 ".
                          " AND o.shipping_status NOT IN (" . SS_SHIPPED . ',' . SS_RECEIVED . ") ".
                          " AND o.pay_id IN (3) ";
                if(!$_REQUEST['platform']) $where .= " AND o.platform <> 'pos_exhibition'";
                break;

            case CS_CANCELED_STAFF : case CS_CANCELED_SELF :
                $where .= " AND o.order_status = " . OS_CANCELED . " ";
                break;

            default:
                if ($_REQUEST['composite_status'] != -1)
                {
                    $where .= " AND o.order_status = '$_REQUEST[composite_status]' ";
                }
        }

        /* 团购订单 */
        if ($_REQUEST['group_buy_id'])
        {
            $where .= " AND o.extension_code = 'group_buy' AND o.extension_id = '$_REQUEST[group_buy_id]' ";
        }

        /* 如果管理员属于某个办事处，只列出这个办事处管辖的订单 */
        $sql = "SELECT agency_id FROM " . $GLOBALS['ecs']->table('admin_user') . " WHERE user_id = '$_SESSION[admin_id]'";
        $agency_id = $GLOBALS['db']->getOne($sql);
        if ($agency_id > 0)
        {
            $where .= " AND o.agency_id = '$agency_id' ";
        }

        /* 分页大小 */
        $_REQUEST['page'] = empty($_REQUEST['page']) || (intval($_REQUEST['page']) <= 0) ? 1 : intval($_REQUEST['page']);

        if (isset($_REQUEST['page_size']) && intval($_REQUEST['page_size']) > 0)
        {
            $_REQUEST['page_size'] = intval($_REQUEST['page_size']);
        }
        // elseif (isset($_COOKIE['ECSCP']['page_size']) && intval($_COOKIE['ECSCP']['page_size']) > 0)
        // {
        //     $_REQUEST['page_size'] = intval($_COOKIE['ECSCP']['page_size']);
        // }
        else
        {
            $_REQUEST['page_size'] = 50;
        }
        
        // To prevent exceeding memory limit, max page_size is limited to 1000
        if ($_REQUEST['page_size'] > 1000)
        {
            $_REQUEST['page_size'] = 1000;
        }

        /* 记录总数 */
        if ( $_REQUEST['display_store_pickup_filter'] > 0 || $_REQUEST['display_order_not_pickup'] == 1 || $_REQUEST['display_order_not_packed'] == 1 ) {
            $sql = "SELECT COUNT( distinct o.order_id) " ;
        } else {
            $sql = "SELECT COUNT( o.order_id) " ;
        }

        // if ($_REQUEST['composite_status'] == CS_AWAIT_HK_FLS) {
        // 
        // } else {
        //     $sellableWarehouseIds = $erpController->getSellableWarehouseIds(true); 
        // }

        $wilson_warehouse_id = $erpController->getWilsonWarehouseId();
        $fls_warehouse_id = $erpController->getFlsWarehouseId();
        $main_warehouse_id = $erpController->getMainWarehouseId();
        $shop_warehouse_id = $erpController->getShopWarehouseId();
        $sellableWarehouseIdsFls = implode(',',[$fls_warehouse_id]);
        
        $sellableWarehouseIds = $erpController->getSellableWarehouseIds(true); 

        if ($pagination_by_sql == true) {
            $sql .=  " FROM " . $GLOBALS['ecs']->table('order_info') . " AS o " .
                    ( $_REQUEST['user_name'] != '' ? " left join " . $GLOBALS['ecs']->table('users') . " AS u on (o.user_id = u.user_id) " : '') .
                    (($_REQUEST['logistics_id']>0 || $_REQUEST['invoice_no'] || $_REQUEST['display_store_pickup_filter'] > 0 || $_REQUEST['display_order_not_pickup'] == 1  || $_REQUEST['display_order_not_packed'] == 1 || $_REQUEST['is_fls'] == 1) ? " LEFT JOIN " .$GLOBALS['ecs']->table('delivery_order'). " AS do ON do.order_id=o.order_id " : "").
                    $where .
                    (($_REQUEST['all_in_stock'] || $_REQUEST['need_purchase']) ? "AND (SELECT IF(min(stock) >= 0, 'Y', 'N') FROM (SELECT og.order_id, (SUM(ega.goods_qty) - og.goods_number + og.send_number) as stock FROM " . $GLOBALS['ecs']->table('erp_goods_attr_stock') . " as ega LEFT JOIN " .$GLOBALS['ecs']->table('order_goods'). " as og ON ega.goods_id = og.goods_id AND ega.warehouse_id IN (".$sellableWarehouseIds.") GROUP BY og.order_id, og.goods_id) as gs WHERE gs.order_id = o.order_id) = '" . ($_REQUEST['need_purchase'] ? "N" : "Y") . "'" : '') .
                     ($_REQUEST['need_purchase'] ? "AND IFNULL((SELECT max(1) FROM ".$GLOBALS['ecs']->table('order_action')." as oa WHERE oa.order_id = o.order_id AND oa.action_note LIKE '%已向供應商訂貨%'), 0) = 0" : '') ;

           
           $record_count   = $GLOBALS['db']->getOne($sql);
           
        }

        $_REQUEST['page_count']     = $_REQUEST['record_count'] > 0 ? ceil($_REQUEST['record_count'] / $_REQUEST['page_size']) : 1;

        /* 查询 */
        $sql = "SELECT o.order_id, o.order_sn, o.add_time, o.order_status, o.shipping_id, o.shipping_status, o.order_amount, o.money_paid, o.cp_price, " .
                    "o.pay_status, o.order_type, o.type_sn, o.type_id, o.actg_paid_amount,o.pay_id, o.extension_code, o.extension_id, o.postscript, o.serial_numbers,  " .
                    "o.consignee, o.address, o.country, o.email, o.tel, o.pay_time, s.shipping_name, o.shipping_fod, o.language, p.pay_name, " .
                    "(" . order_amount_field('o.') . ") AS total_fee, o.to_buyer, " .
                    "(o.money_paid + o.order_amount) AS order_total, " . // howang: money_paid(已付金額) + order_amount(應付金額) = 訂單總金額
                  //  (in_array($_REQUEST['composite_status'], [CS_AWAIT_SHIP,CS_AWAIT_SHIP_HK,CS_AWAIT_PICK_HK,CS_AWAIT_SHIP_NON_HK,CS_AWAIT_SHIP_HK_YOHO,CS_AWAIT_SHIP_LOCKER,CS_AWAIT_HK_FLS]) ? "IF((SELECT min(stock) FROM (SELECT og.order_id, (SUM(ega.goods_qty) - og.goods_number + og.send_number) as stock FROM " . $GLOBALS['ecs']->table('erp_goods_attr_stock') . " as ega LEFT JOIN " .$GLOBALS['ecs']->table('order_goods'). " as og ON ega.goods_id = og.goods_id AND ega.warehouse_id IN (".$sellableWarehouseIds.") GROUP BY og.order_id, og.goods_id) as gs WHERE gs.order_id = o.order_id) >= 0, 'Y', 'N') AS all_in_stock, " : ''). // Added by howang
                    "IFNULL((SELECT max(flashdeal_id) FROM " .$GLOBALS['ecs']->table('order_goods'). " as og1 WHERE og1.order_id = o.order_id GROUP BY og1.order_id), 0) as flashdeal_id, ".
                    "IFNULL(u.user_name, '" .$GLOBALS['_LANG']['anonymous']. "') AS buyer, o.user_id, o.salesperson, o.platform, ".
                    "o.fully_paid, o.actg_paid_amount, ". // Accounting System for wholesale orders
                    "IFNULL((SELECT count(*) FROM ".$GLOBALS['ecs']->table('order_action')." as oa WHERE oa.order_id = o.order_id), 0) as action_count, " . // Action count
                    "IFNULL((SELECT 1 FROM ".$GLOBALS['ecs']->table('order_action')." as oa WHERE oa.order_id = o.order_id AND oa.action_user = 'buyer' AND oa.order_status = '" . OS_CANCELED . "' limit 1 ), 0) as buyer_cancel, " . // Cancel count
                    "IFNULL((SELECT max(1) FROM ".$GLOBALS['ecs']->table('order_action')." as oa WHERE oa.order_id = o.order_id AND oa.action_note LIKE '%已向供應商訂貨%'), 0) as ordered_from_supplier " .
                " FROM " . $GLOBALS['ecs']->table('order_info') . " AS o " .
                //" LEFT JOIN " .$GLOBALS['ecs']->table('erp_order'). " AS eo ON eo.client_order_sn=o.order_sn ".
                (($_REQUEST['logistics_id']>0 || $_REQUEST['invoice_no'] || $_REQUEST['display_store_pickup_filter'] > 0 || $_REQUEST['display_order_not_pickup'] == 1 ||  $_REQUEST['display_order_not_packed'] == 1 || $_REQUEST['is_fls'] == 1 ) ? " LEFT JOIN " .$GLOBALS['ecs']->table('delivery_order'). " AS do ON do.order_id=o.order_id " : "").
                " LEFT JOIN " .$GLOBALS['ecs']->table('payment'). " AS p ON p.pay_id=o.pay_id ".
                " LEFT JOIN " .$GLOBALS['ecs']->table('shipping'). " AS s ON s.shipping_id=o.shipping_id ".
                " LEFT JOIN " .$GLOBALS['ecs']->table('users'). " AS u ON u.user_id=o.user_id ". $where .
                //" Group by o.order_id ".
                //($_REQUEST['all_in_stock'] ? "HAVING all_in_stock = 'Y' " : '') .
                //($_REQUEST['need_purchase'] ? "HAVING all_in_stock = 'N' AND ordered_from_supplier = 0 " : '') .
                ($_REQUEST['need_purchase'] ? "HAVING ordered_from_supplier = 0 " : '') .
                ($_REQUEST['composite_status'] == CS_CANCELED_SELF ? "HAVING buyer_cancel = 1 " : "") .
                ($_REQUEST['composite_status'] == CS_CANCELED_STAFF ? "HAVING buyer_cancel = 0 " : "") .
                ($_REQUEST['display_store_pickup_filter'] > 0 || $_REQUEST['display_order_not_pickup'] == 1 || $_REQUEST['display_order_not_packed'] == 1 ? "Group by o.order_id " : '') .
                " ORDER BY $_REQUEST[sort_by] $_REQUEST[sort_order] ";

                if ($pagination_by_sql == true) {
                    $sql .= " LIMIT " . $_REQUEST['start'] . ",$_REQUEST[page_size]";
                }
        
        foreach (array('order_sn', 'consignee', 'email', 'address', 'zipcode', 'tel', 'user_name') AS $val)
        {
            $_REQUEST[$val] = stripslashes($_REQUEST[$val]);
        }
        // set_filter($filter, $sql);
    }
    // else
    // {
    //     $sql    = $result['sql'];
    //     $_REQUEST = $result['filter'];
    // }

    $row = $GLOBALS['db']->getAll($sql);

    // Platform list
    $platforms = Yoho\cms\Controller\OrderController::DB_SELLING_PLATFORMS;

    // Order goods list
    $order_ids = array_map(function ($order) { return $order['order_id']; }, $row);
    // $sql = "SELECT og.*, g.goods_thumb " .
    //         ($_REQUEST['display_order_goods'] ? ", (SELECT SUM(ega.goods_qty) FROM " . $GLOBALS['ecs']->table('erp_goods_attr_stock') . " AS ega WHERE g.goods_id = ega.goods_id AND ega.warehouse_id IN (".$erpController->getSellableWarehouseIds(true).")) AS storage " : '') .
    //         "FROM " . $GLOBALS['ecs']->table('order_goods') . " as og " .
    //         "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON og.goods_id = g.goods_id " .
    //         "WHERE order_id " . db_create_in($order_ids);

    // $sql = "SELECT og.*, g.goods_thumb " .
    //         (1 ? ",  (SELECT IFNULL(SUM(ega.goods_qty),0) FROM " . $GLOBALS['ecs']->table('erp_goods_attr_stock') . " AS ega WHERE g.goods_id = ega.goods_id AND ega.warehouse_id IN (".$sellableWarehouseIds.")) AS storage " : '') .
    //         "FROM " . $GLOBALS['ecs']->table('order_goods') . " as og " .
    //         "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON og.goods_id = g.goods_id " .
    //         "WHERE order_id " . db_create_in($order_ids);

    // check 代理送貨已出PO
    $po_sql = "select eo.order_sn as order_sn_po, oi.order_id from ".$GLOBALS['ecs']->table('erp_order')." eo left join ".$GLOBALS['ecs']->table('order_info')." oi on (eo.client_order_sn = oi.order_sn) where oi.order_id ". db_create_in($order_ids) ." ";

    //echo $po_sql;
    $po_sn_list = $GLOBALS['db']->getAll($po_sql);

    $po_sn_list_ar = [];
    foreach($po_sn_list as $po_sn) {
        $po_sn_list_ar[$po_sn['order_id']][] = $po_sn['order_sn_po'];
    }

    $order_goods_ids = [];
    // get order goods ids
    $sql = "SELECT distinct og.goods_id " .
            "FROM " . $GLOBALS['ecs']->table('order_goods') . " as og " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON og.goods_id = g.goods_id " .
            "WHERE order_id " . db_create_in($order_ids);
    $order_goods_ids = $GLOBALS['db']->getCol($sql);

    // for globals goods qty
    $sql = "SELECT ega.goods_id, IFNULL(SUM(ega.goods_qty),0) as storage " .
            "FROM " . $GLOBALS['ecs']->table('erp_goods_attr_stock') . " AS ega " .
            "WHERE ega.goods_id " . db_create_in($order_goods_ids) ." AND ega.warehouse_id IN (".$sellableWarehouseIds.") ".
            "group by goods_id";
    
    $order_goods = $GLOBALS['db']->getAll($sql);

    $goods_qty = [];
    foreach ($order_goods as $order_goods_info){
        $goods_qty[$order_goods_info['goods_id']] = $order_goods_info['storage'];
    }

    foreach ($order_goods_ids as $order_goods_id) {
        if (!isset($goods_qty[$order_goods_id])) {
            $goods_qty[$order_goods_id] = 0;
        }
    }

    $sql = "SELECT og.*, g.goods_thumb, g.goods_height, g.goods_width, g.goods_length " .
        "FROM " . $GLOBALS['ecs']->table('order_goods') . " as og " .
        "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON og.goods_id = g.goods_id " .
        "WHERE order_id " . db_create_in($order_ids);

    $order_goods = $GLOBALS['db']->getAll($sql);

    $order_goods_by_order_id = [];
    foreach ($order_goods as $order_goods_key =>  $order_goods_info){
        // add storage to order_goods ar
        $order_goods[$order_goods_key]['storage'] = $goods_qty[$order_goods_info['goods_id']];
        $order_goods_by_order_id[$order_goods_info['order_id']][] = $order_goods[$order_goods_key];
    }

    // get all warehouse qty
    $goods_storage_wh = $erpController->getSellableProductsQtyByWarehouses($order_goods_ids,null,false);

    // for FLS warehouse
    $goods_qty_fls = [];
    foreach ($goods_storage_wh as $goods_storage_wh_key => $goods_storage_wh_info){
        $goods_qty_fls[$goods_storage_wh_key] = isset($goods_storage_wh_info[$fls_warehouse_id])? $goods_storage_wh_info[$fls_warehouse_id]:0;
    }

    // for FLS warehouse
    // $sql = "SELECT ega.goods_id, IFNULL(SUM(ega.goods_qty),0) as storage " .
    //         "FROM " . $GLOBALS['ecs']->table('erp_goods_attr_stock') . " AS ega " .
    //         "WHERE ega.goods_id " . db_create_in($order_goods_ids) ." AND ega.warehouse_id IN (".$sellableWarehouseIdsFls.") ".
    //         "group by goods_id";
    // 
    // $order_goods_fls = $GLOBALS['db']->getAll($sql);
    // 
    // $goods_qty_fls = [];
    // foreach ($order_goods_fls as $order_goods_info){
    //     $goods_qty_fls[$order_goods_info['goods_id']] = $order_goods_info['storage'];
    // }
    // 
    // foreach ($order_goods_ids_fls as $order_goods_id_fls) {
    //     if (!isset($goods_qty_fls[$order_goods_id_fls])) {
    //         $goods_qty_fls[$order_goods_id_fls] = 0;
    //     } 
    // }

    // for main warehouse without fls
    // foreach ($goods_qty as $goods_key => $qty){
    //     $main_goods_qty[$goods_key] = $qty - $goods_qty_fls[$goods_key];
    // }

    // for Wilson warehouse
    $goods_qty_wilson = [];
    foreach ($goods_storage_wh as $goods_storage_wh_key => $goods_storage_wh_info){
        $goods_qty_wilson[$goods_storage_wh_key] = isset($goods_storage_wh_info[$wilson_warehouse_id])? $goods_storage_wh_info[$wilson_warehouse_id]:0;
    }

    // only main warehouse (exclude fls, shop, wilson)
    $main_goods_qty = [];
    foreach ($goods_storage_wh as $goods_storage_wh_key => $goods_storage_wh_info){
        $main_goods_qty[$goods_storage_wh_key] = isset($goods_storage_wh_info[$main_warehouse_id])? $goods_storage_wh_info[$main_warehouse_id]:0;
    }

    // get shop warehouse
    $shop_goods_qty = [];
    foreach ($goods_storage_wh as $goods_storage_wh_key => $goods_storage_wh_info){
        $shop_goods_qty[$goods_storage_wh_key] = isset($goods_storage_wh_info[$shop_warehouse_id])? $goods_storage_wh_info[$shop_warehouse_id]:0;
    }

    // get qty excluded shop
    $goods_qty_excluded_shop = [];
    foreach ($order_goods_ids as $order_goods_id) {
        $goods_qty_excluded_shop[$order_goods_id] = $goods_qty[$order_goods_id] - $shop_goods_qty[$order_goods_id] ;
    }

    // Get sales agent name
	$salesagent = get_sales_agent_name();
	
    if (in_array($_REQUEST['composite_status'], [CS_AWAIT_SHIP, CS_AWAIT_SHIP_HK,CS_AWAIT_SHIP_LOCKER,CS_AWAIT_PICK_HK,CS_AWAIT_SHIP_NON_HK,CS_AWAIT_SHIP_HK_YOHO,CS_AWAIT_HK_FLS,CS_AWAIT_HK_WILSON])) {
        $row_temp = $row;
        // sort by paid time
        usort($row_temp, function($a, $b) {
            return $a['pay_time'] - $b['pay_time'];
        });

        $orders_can_ship = [];
        $orders_cant_ship = [];
        $orders_can_ship_fls = [];
        $orders_can_ship_wilson = [];
        $orders_can_ship_shop = [];
        $orders_cant_ship_shop = [];

        if (in_array($_REQUEST['composite_status'], [CS_AWAIT_SHIP_HK,CS_AWAIT_SHIP_LOCKER,CS_AWAIT_PICK_HK,CS_AWAIT_SHIP_NON_HK,CS_AWAIT_SHIP_HK_YOHO,CS_AWAIT_HK_FLS,CS_AWAIT_HK_WILSON])) {
            // check FLS Stock 
            foreach ($row_temp AS $key => $value)
            {
                if (isset($order_goods_by_order_id[$value['order_id']]))
                {
                    $all_order_goods_in_stock = true;
                    // FLS new flow
                    // in 待出貨(本地速遞) HK
                    if (in_array($value['shipping_id'],[3]) and $value['country'] == 3409) {
                        foreach ($order_goods_by_order_id[$value['order_id']] as $ogs)
                        {
                            if ( ($goods_qty_fls[$ogs['goods_id']] - $ogs['goods_number'] + $ogs['send_number']) >= 0) {
                                $goods_qty_fls[$ogs['goods_id']] = $goods_qty_fls[$ogs['goods_id']] -$ogs['goods_number'] + $ogs['send_number'];
                            } else {
                                $goods_qty_fls[$ogs['goods_id']] = 0;
                                $all_order_goods_in_stock = false;
                            }
                        }

                        // all in stock
                        if ($all_order_goods_in_stock == true) {
                          $orders_can_ship_fls[] = $value['order_id']; 
                        }
                    }
                }
            }

            // check Wilson Stock 
            foreach ($row_temp AS $key => $value)
            {
                if (isset($order_goods_by_order_id[$value['order_id']]))
                {
                    $all_order_goods_in_stock = true;
                    // Wilson new flow
                    // in 待出貨(本地速遞) HK
                    if (in_array($value['shipping_id'],[3]) and $value['country'] == 3409) {
                        foreach ($order_goods_by_order_id[$value['order_id']] as $ogs)
                        {
                            if ( ($goods_qty_wilson[$ogs['goods_id']] - $ogs['goods_number'] + $ogs['send_number']) >= 0) {
                                $goods_qty_wilson[$ogs['goods_id']] = $goods_qty_wilson[$ogs['goods_id']] -$ogs['goods_number'] + $ogs['send_number'];
                            } else {
                                $goods_qty_wilson[$ogs['goods_id']] = 0;
                                $all_order_goods_in_stock = false;
                            }
                        }

                        // all in stock
                        if ($all_order_goods_in_stock == true) {
                            $orders_can_ship_wilson[] = $value['order_id']; 
                        }
                    }
                }
            }
        }

        if (in_array($_REQUEST['composite_status'], [CS_AWAIT_PICK_HK])) {
            // check Shop Stock // for store pick up
            foreach ($row_temp AS $key => $value)
            {
                if (isset($order_goods_by_order_id[$value['order_id']]))
                {
                    $all_order_goods_in_stock = true;
                    //待出貨(門店自取)
                    if (in_array($value['shipping_id'],[2,5])) {
                        foreach ($order_goods_by_order_id[$value['order_id']] as $ogs)
                        {
                            if ( ($shop_goods_qty[$ogs['goods_id']] - $ogs['goods_number'] + $ogs['send_number']) >= 0) {
                                $shop_goods_qty[$ogs['goods_id']] = $shop_goods_qty[$ogs['goods_id']] -$ogs['goods_number'] + $ogs['send_number'];
                            } else {
                                $shop_goods_qty[$ogs['goods_id']] = 0;
                                $all_order_goods_in_stock = false;
                            }
                        }

                        // all in stock
                        if ($all_order_goods_in_stock == true) {
                          $orders_can_ship_shop[] = $value['order_id']; 
                        } else {
                          $orders_cant_ship_shop[] = $value['order_id'];
                        }
                    }
                }
            }
        }
        // echo '<pre>';
        // print_r($orders_can_ship_shop);
        // print_r($orders_cant_ship_shop);
        // exit;
        if ($_REQUEST['composite_status'] == CS_AWAIT_HK_FLS) {
            $row_temp_2 = [];
            foreach ($row AS $key => $value)
            {
                if (in_array($value['order_id'],$orders_can_ship_fls)) {
                    $row_temp_2[] = $value;
                }
            }
            $row = $row_temp_2;
        } else if ($_REQUEST['composite_status'] == CS_AWAIT_HK_WILSON) {
            $row_temp_2 = [];
            foreach ($row AS $key => $value)
            {
                if (in_array($value['order_id'],$orders_can_ship_wilson)) {
                    $row_temp_2[] = $value;
                }
            }
            $row = $row_temp_2;
        } else if ($_REQUEST['composite_status'] == CS_AWAIT_PICK_HK) {
            $row_temp_2 = [];
            foreach ($row AS $key => $value)
            {
                if ($_REQUEST['all_in_stock'] > 0) {
                    if (in_array($value['order_id'],$orders_can_ship_shop)) {
                        $row_temp_2[] = $value;
                    }
                } else if ($_REQUEST['need_purchase'] > 0) {
                    if (in_array($value['order_id'],$orders_cant_ship_shop)) {
                        $row_temp_2[] = $value;
                    }
                } else {
                    if (in_array($value['order_id'],$orders_can_ship_shop) || in_array($value['order_id'],$orders_cant_ship_shop) ) {
                        $row_temp_2[] = $value;
                    }
                }
            }
            $row = $row_temp_2;
        } else {
            foreach ($row_temp AS $key => $value)
            {
                if (in_array($_REQUEST['composite_status'],[CS_AWAIT_SHIP_HK,CS_AWAIT_SHIP_HK_YOHO]))
                {
                    // skip directly, because it will be ship by FLS warehouse
                    if (in_array($value['order_id'],$orders_can_ship_fls)) {
                        continue;
                    }

                    // skip directly, because it will be ship by WILSON warehouse
                    if (in_array($value['order_id'],$orders_can_ship_wilson)) {
                        continue;
                    }
                }

                // filter the order can't be shipped
                if (isset($order_goods_by_order_id[$value['order_id']]))
                {
                    $all_order_goods_in_stock = true;
                    if (in_array($_REQUEST['composite_status'],[CS_AWAIT_SHIP_LOCKER,CS_AWAIT_SHIP_NON_HK]))
                    {
                        // goods qty without fls without wilson
                        foreach ($order_goods_by_order_id[$value['order_id']] as $ogs)
                        {
                            if ( $main_goods_qty[$ogs['goods_id']] - $ogs['goods_number'] + $ogs['send_number'] >= 0) {
                                $main_goods_qty[$ogs['goods_id']] = $main_goods_qty[$ogs['goods_id']] -$ogs['goods_number'] + $ogs['send_number'];
                            } else {
                                $main_goods_qty[$ogs['goods_id']] = 0;
                                $all_order_goods_in_stock = false;
                            }
                        }
                    } else {
                        // goods qty global
                        // foreach ($order_goods_by_order_id[$value['order_id']] as $ogs)
                        // {
                        //     if ($goods_qty[$ogs['goods_id']] - $ogs['goods_number'] + $ogs['send_number'] >= 0) {
                        //         $goods_qty[$ogs['goods_id']] = $goods_qty[$ogs['goods_id']] -$ogs['goods_number'] + $ogs['send_number'];
                        //     } else {
                        //         $goods_qty[$ogs['goods_id']] = 0;
                        //         $all_order_goods_in_stock = false;
                        //     }
                        // }

                        if (in_array($_REQUEST['composite_status'],[CS_AWAIT_SHIP])) {
                            // goods qty global
                            foreach ($order_goods_by_order_id[$value['order_id']] as $ogs)
                            {
                                if ($goods_qty[$ogs['goods_id']] - $ogs['goods_number'] + $ogs['send_number'] >= 0) {
                                    $goods_qty[$ogs['goods_id']] = $goods_qty[$ogs['goods_id']] -$ogs['goods_number'] + $ogs['send_number'];
                                } else {
                                    $goods_qty[$ogs['goods_id']] = 0;
                                    $all_order_goods_in_stock = false;
                                }
                            }
                        } else {
                            if (in_array($_REQUEST['composite_status'],[CS_AWAIT_SHIP_HK])) {
                                // goods qty global - shop qty
                                foreach ($order_goods_by_order_id[$value['order_id']] as $ogs)
                                {
                                    if ($goods_qty_excluded_shop[$ogs['goods_id']] - $ogs['goods_number'] + $ogs['send_number'] >= 0) {
                                        $goods_qty_excluded_shop[$ogs['goods_id']] = $goods_qty_excluded_shop[$ogs['goods_id']] -$ogs['goods_number'] + $ogs['send_number'];
                                    } else {
                                        $goods_qty_excluded_shop[$ogs['goods_id']] = 0;
                                        $all_order_goods_in_stock = false;
                                    }
                                }
                            } else {
                                // goods qty main
                                foreach ($order_goods_by_order_id[$value['order_id']] as $ogs)
                                {
                                    if ($main_goods_qty[$ogs['goods_id']] - $ogs['goods_number'] + $ogs['send_number'] >= 0) {
                                        $main_goods_qty[$ogs['goods_id']] = $main_goods_qty[$ogs['goods_id']] -$ogs['goods_number'] + $ogs['send_number'];
                                    } else {
                                        $main_goods_qty[$ogs['goods_id']] = 0;
                                        $all_order_goods_in_stock = false;
                                    }
                                }
                            }
                        } 
                    }

                    if ($all_order_goods_in_stock == true) {
                        $orders_can_ship[] = $value['order_id']; 
                    } else {
                        $orders_cant_ship[] = $value['order_id']; 
                    }
                }
            }

            $row_temp = [];
            foreach ($row AS $key => $value)
            {
                if ($_REQUEST['all_in_stock'] > 0) {
                    if (in_array($value['order_id'],$orders_cant_ship)) {
                        continue;
                    }
                }

                if ($_REQUEST['need_purchase'] > 0) {
                    if (in_array($value['order_id'],$orders_can_ship)) {
                        continue;
                    }
                }

                if (in_array($value['order_id'],$orders_can_ship_fls)) {
                    continue;
                }

                if (in_array($value['order_id'],$orders_can_ship_wilson)) {
                    continue;
                }

                $row_temp[] = $value;
            }

            $row_temp_2 = [];
            foreach ($row_temp AS $key => $value)
            {
                //综合状态
                switch($_REQUEST['composite_status'])
                {
                    case CS_AWAIT_SHIP_HK : case CS_AWAIT_SHIP_HK_YOHO :
                        if (in_array($value['shipping_id'],[3,9,10,14]) and $value['country'] == 3409) {
                            $row_temp_2[] = $value;
                        }
                        break;
                    case CS_AWAIT_HK_FLS :
                        if (in_array($value['shipping_id'],[3]) and $value['country'] == 3409) {
                            $row_temp_2[] = $value;
                        }
                        break;
                    case CS_AWAIT_HK_WILSON :
                        if (in_array($value['shipping_id'],[3]) and $value['country'] == 3409) {
                            $row_temp_2[] = $value;
                        }
                        break;    
                    case CS_AWAIT_SHIP_LOCKER :
                        if (in_array($value['shipping_id'],[8, 11, 12, 13]) and $value['country'] == 3409) {
                            $row_temp_2[] = $value;
                        }
                        break;
                    case CS_AWAIT_PICK_HK :
                        if (in_array($value['shipping_id'],[2,5])) {
                            $row_temp_2[] = $value;
                        }
                        break;
                    case CS_AWAIT_SHIP_NON_HK :
                        if (in_array($value['shipping_id'],[3]) and $value['country'] != 3409 ) {
                            $row_temp_2[] = $value;
                        }
                        break;
                    default:
                        $row_temp_2[] = $value; 
                }
            }
            $row = $row_temp_2;
        }
    }

    if ($pagination_by_sql == false) {
        $record_count = sizeof($row);
        $row = array_slice( $row, $_REQUEST['start'] , $_REQUEST['page_size'] ); 
    }

    /* 格式话数据 */
    foreach ($row AS $key => $value)
    {
        $row[$key]['formated_order_amount'] = price_format($value['order_amount']);
        $row[$key]['formated_money_paid'] = price_format($value['money_paid']);
        $row[$key]['formated_total_fee'] = price_format($value['total_fee']);
        $row[$key]['formated_order_total'] = price_format($value['order_total']);
        $row[$key]['short_order_time'] = local_date('m-d H:i', $value['add_time']);
        $row[$key]['formated_order_cp_price'] = price_format($value['cp_price']);
        if ($value['order_status'] == OS_INVALID || $value['order_status'] == OS_CANCELED)
        {
            /* 如果该订单为无效或取消则显示删除链接 */
            $row[$key]['can_remove'] = 1;
        }
        else
        {
            $row[$key]['can_remove'] = 0;
        }
        $row[$key]['platform_display'] = isset($platforms[$value['platform']]) ? $platforms[$value['platform']] : $value['platform'];
        
        // Extract order_goods for this order, and find most expensive goods
        $row[$key]['goods_list'] = array();
        $most_expensive = -1;
        foreach ($order_goods as $og)
        {
            if ($og['order_id'] == $value['order_id'])
            {
                $og['goods_thumb'] = get_image_path($og['goods_id'], $og['goods_thumb'], true);
                $og['goods_thumb'] = ((strpos($og['goods_thumb'], 'http://') === 0) || (strpos($og['goods_thumb'], 'https://') === 0)) ? $og['goods_thumb'] : $GLOBALS['ecs']->url() . $og['goods_thumb'];
                $og['formated_subtotal'] = price_format($og['goods_price'] * $og['goods_number']);
                $og['formated_goods_price'] = price_format($og['goods_price']);
                $og['formated_cost'] = price_format($og['cost'], false);
                
                if($_REQUEST['display_order_goods'])$row[$key]['goods_list'][] = $og;
                if ($og['goods_price'] > $most_expensive)
                {
                    $most_expensive = $og['goods_price'];
                    //$row[$key]['most_expensive_goods'] = $og;
                    $row[$key]['ex_goods_thumb'] = $og['goods_thumb'];
                }
            }
        }

        // all goods weight
        foreach ($order_goods as $og)
        {
            if ($og['order_id'] == $value['order_id'])
            {
                if ($og['goods_length'] > 0 && $og['goods_width'] > 0 && $og['goods_height'] > 0) {
                    $value['LWH'] += $og['goods_length'] + $og['goods_width'] + $og['goods_height'];
                } else {
                    $value['LWH'] = 0;
                    break;
                }
            }
        }

        $row[$key]['is_wholesale'] = ((in_array($value['user_id'], $userController->getWholesaleUserIds())||(isset($value['order_type']) && $value['order_type']==Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE))?TRUE:FALSE);
        $row[$key]['due_amount'] = $value['fully_paid'] ? 0 : $value['order_amount'] - $value['actg_paid_amount'];
        $row[$key]['due_amount_formatted'] = price_format($row[$key]['due_amount']);
        
        // Flag risky transactions
        $row[$key]['risky'] = false;
        if ($value['pay_id'] == '7') // PayPal
        {
            if ((($value['country'] == '3409') && (doubleval($value['order_total']) >= 6000)) || // 香港, $6000 or above
                (($value['country'] == '3436') && (doubleval($value['order_total']) >= 3000)) || // 中國, $3000 or above
                (($value['country'] == '3411') && (doubleval($value['order_total']) >= 3000)))   // 台灣, $3000 or above
            {
                $row[$key]['risky'] = true;
            }
        }
        if ($value['pay_id'] == '3' && $value['pay_status'] == PS_PAYED && in_array($value['shipping_status'], [SS_SHIPPED, SS_RECEIVED])) //POS單: 門市購買, 已付款, 已出貨/收貨確認
        {
            // Find order_action : check order is reship order.
            $oa_sql = "SELECT * FROM ".$GLOBALS['ecs']->table('order_action')." WHERE order_id = ".$value['order_id']." ORDER BY action_id DESC ";
            $log_list = $GLOBALS['db']->getAll($oa_sql);

            // Check order status
            $have_unshipping_log = false;
            foreach($log_list as $log_key => $log) {
                $status = $log['shipping_status'];
                if(in_array($status, [SS_SHIPPED, SS_RECEIVED])) {
                    if($have_unshipping_log == true) {
                        $row[$key]['reship_order'] = true; // reship order
                        break;
                    }
                } else if(in_array($status, [SS_UNSHIPPED])) {
                    $have_unshipping_log = true;
                }
            }
        }
	   // If is sales agent order
        if($value['order_type'] == Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT)
	    {
           //TODO: hardcode HKTV :H5119002
          //$row[$key]['special_sa'] = strpos($value['serial_numbers'], 'H5119002') !== false ? true:false;
		  $row[$key]['is_sa'] = true;
		  $row[$key]['sa_sn'] = $value['type_sn'];
		  $row[$key]['sa_name']  = isset($salesagent[$value['type_id']]) ? $salesagent[$value['type_id']] : '';
        }

        // consignee info
        $consignee_info = '';
        if ($value['consignee'])$consignee_info .= "<i class='fa fa-user'></i> <a href='mailto:$value[email]'>$value[consignee]</a>";
        if ($value['tel'])$consignee_info .= " <i class='fa fa-phone-square'></i>". $value[tel]. ($value['LWH'] > 0 ? '<b style="color:#9CC2CB"> (LWH - '.$value['LWH'].')</b>':'');
        if ($row[$key]['is_wholesale']) $consignee_info .= "<br/><i class='fa fa-truck'></i> <b>$value[shipping_name] (批發訂單$value[type_sn])</b>";
        else $consignee_info .= "<br/><b><i class='fa fa-truck'></i> $value[shipping_name] : ".(($value['shipping_fod'])?'(運費到付)':'')."</b> $value[address]";
        
        if (trim($value['order_type']) == 'sa' && !empty($value['type_id']) && !empty($value['type_sn'])) $consignee_info .= "<br /><span style='font-weight: bold; color: #aa6555'><i class='fa fa-thumbtack'></i> 代理銷售: </span><span style='color: #aa6555'>".$row[$key]['sa_name']." [$value[type_sn]]</span>";
        if (strlen(trim($value['postscript'])) > 0) $consignee_info .= "<br /><b><i class='fa fa-file-alt'></i> 客戶備註: $value[postscript]</b>";
        if (strlen(trim($value['to_buyer'])) > 0) $consignee_info .= "<br /><b class='blue'><i class='fa fa-thumbtack'></i> 內部備註: $value[to_buyer]</b>";
        $row[$key]['consignee_info'] = $consignee_info;

        // salesperson info
        $salesperson_info = $value['salesperson'];
        $salesperson_info .= "<br>".$row[$key]['platform_display'];
        if ($row[$key]['flashdeal_id'])$salesperson_info .= '(閃購)';
        $lang_list = available_language_names();
        if ($row[$key]['language'])$salesperson_info .= "<br>".($lang_list[$value['language']] ? $lang_list[$value['language']]: $value['language']);
        if ($row[$key]['is_sa'])$salesperson_info .= "<br>".$row[$key]['sa_name'];
        $row[$key]['salesperson_info'] = $salesperson_info;
        global $_LANG;
        $all_status = $_LANG['icon']['ps'][$value['pay_status']]." | ".$_LANG['icon']['ss'][$value['shipping_status']];

        //if ($value['all_in_stock']) {

        if (in_array($_REQUEST['composite_status'], [CS_AWAIT_SHIP,CS_AWAIT_SHIP_HK,CS_AWAIT_SHIP_LOCKER,CS_AWAIT_PICK_HK,CS_AWAIT_SHIP_NON_HK,CS_AWAIT_SHIP_HK_YOHO,CS_AWAIT_HK_FLS,CS_AWAIT_HK_WILSON])) {
            if (in_array($value['order_id'],$orders_can_ship) || in_array($value['order_id'],$orders_can_ship_fls) || in_array($value['order_id'],$orders_can_ship_wilson) || in_array($value['order_id'],$orders_can_ship_shop)) {
                $all_status.= " | ".$_LANG['icon']['all_in_stock']['Y'];
            } else {
                $all_status.= " | ".$_LANG['icon']['all_in_stock']['N'];
            }
        }
        //}

        //if ($value['all_in_stock']) $all_status.= " | ".$_LANG['icon']['all_in_stock'][$value['all_in_stock']];

        if (!empty($po_sn_list_ar[$row[$key]['order_id']])) {
            foreach ($po_sn_list_ar[$row[$key]['order_id']] as $po_order_sn) {
                $row[$key]['po_sn_list'] .= '<a target="_blank" href="erp_order_manage.php?act=order_list&order_sn='.$po_order_sn.'">'.$po_order_sn.'</a>';
                $row[$key]['po_sn_list'] .= '<br>';
            }
            //$row[$key]['po_sn_list'] = $po_sn_list_ar[$row[$key]['order_id']];
        }else {
            $row[$key]['po_sn_list'] = '';
        }
        
        $row[$key]['all_status'] = $all_status;

        // action
        $action = '';
        $action .= '<a class="badge '.($value['ordered_from_supplier'] ? 'bg-green':'bg-blue').'" href="javascript:;" onclick="addRemarkPopup('.$value['order_id'].')">'.$value['action_count'].'</a>';
        //$action .= '<a href="order.php?act=info&order_id='.$value['order_id'].'">'.$_LANG['detail'].'</a>';
        if (check_authz('order_remove')) {
            if ($row[$key]['can_remove']) $action .= '<br /><a href="javascript:;" data-url="order.php?act=remove&amp;id='.$value['order_id'].'" onclick="cellRemove(this);">'.$_LANG['remove'].'</a>';
        }
        if ($row[$key]['is_wholesale'] || $_REQUEST['wholesale_receivable'])
        $action .= ($row[$key]['fully_paid'] ? '<br /><a href="wholesale_payments.php?act=list&order_sn='.$value['order_sn'].'">付款紀錄</a>' : '<br /><a href="wholesale_payments.php?act=add&order_id='.$value['order_id'].'">付款</a>').
      	' | <a href="javascript:;" onclick="javascript:completeOrderPayment('.$value['order_id'].')">完成</a>';
        $row[$key]['_action'] = $action;
    }

    // echo '<pre>';
    // print_r($row);
    // exit;
    $arr = array('orders' => $row, 'record_count' => $record_count);

    return $arr;
}

/**
 * 更新订单对应的 pay_log
 * 如果未支付，修改支付金额；否则，生成新的支付log
 * @param   int     $order_id   订单id
 */
function update_pay_log($order_id)
{
    $order_id = intval($order_id);
    if ($order_id > 0)
    {
        $sql = "SELECT order_amount FROM " . $GLOBALS['ecs']->table('order_info') .
                " WHERE order_id = '$order_id'";
        $order_amount = $GLOBALS['db']->getOne($sql);
        if (!is_null($order_amount))
        {
            $sql = "SELECT log_id FROM " . $GLOBALS['ecs']->table('pay_log') .
                    " WHERE order_id = '$order_id'" .
                    " AND order_type = '" . PAY_ORDER . "'" .
                    " AND is_paid = 0";
            $log_id = intval($GLOBALS['db']->getOne($sql));
            if ($log_id > 0)
            {
                /* 未付款，更新支付金额 */
                $sql = "UPDATE " . $GLOBALS['ecs']->table('pay_log') .
                        " SET order_amount = '$order_amount' " .
                        "WHERE log_id = '$log_id' LIMIT 1";
            }
            else
            {
                /* 已付款，生成新的pay_log */
                $sql = "INSERT INTO " . $GLOBALS['ecs']->table('pay_log') .
                        " (order_id, order_amount, order_type, is_paid)" .
                        "VALUES('$order_id', '$order_amount', '" . PAY_ORDER . "', 0)";
            }
            $GLOBALS['db']->query($sql);
        }
    }
}

/**
 * 取得供货商列表
 * @return array    二维数组
 */
function get_suppliers_list()
{
    $sql = 'SELECT * FROM ' . $GLOBALS['ecs']->table('erp_supplier') . ' WHERE is_valid = 1 ORDER BY supplier_id desc';
    $res = $GLOBALS['db']->getAll($sql);

    if (!is_array($res))
    {
        $res = array();
    }

    return $res;
}

/**
 * 取得订单商品
 * @param   array     $order  订单数组
 * @return array
 */
function get_order_goods($order)
{
    $goods_list = array();
    $goods_attr = array();

    $sql = "SELECT o.*, o.goods_attr, IFNULL(b.brand_name, '') AS brand_name " .
            "FROM " . $GLOBALS['ecs']->table('order_goods') . " AS o ".
            "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " AS g ON o.goods_id = g.goods_id " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('brand') . " AS b ON g.brand_id = b.brand_id " .
            "WHERE o.order_id = '$order[order_id]' ";

    $res = $GLOBALS['db']->query($sql);

    if(!isset($order['agency_id']))
    {
    	$sql="select agency_id from ".$GLOBALS['ecs']->table('order_info')." where order_id='".$order['order_id']."'";
    	$order['agency_id']=$GLOBALS['db']->getOne($sql);
    }

    $agency_id=$order['agency_id'];

    while ($row = $GLOBALS['db']->fetchRow($res))
    {
        //库存
        $row['storage']=get_goods_stock($row['goods_id'],$row['goods_attr_id'],$agency_id);

        // 虚拟商品支持
        if ($row['is_real'] == 0)
        {
            /* 取得语言项 */
            $filename = ROOT_PATH . 'plugins/' . $row['extension_code'] . '/languages/common_' . $GLOBALS['_CFG']['lang'] . '.php';
            if (file_exists($filename))
            {
                include_once($filename);
                if (!empty($GLOBALS['_LANG'][$row['extension_code'].'_link']))
                {
                    $row['goods_name'] = $row['goods_name'] . sprintf($GLOBALS['_LANG'][$row['extension_code'].'_link'], $row['goods_id'], $order['order_sn']);
                }
            }
        }

        $row['formated_subtotal']       = price_format($row['goods_price'] * $row['goods_number']);
        $row['formated_goods_price']    = price_format($row['goods_price']);

        $goods_attr[] = explode(' ', trim($row['goods_attr'])); //将商品属性拆分为一个数组

        if ($row['extension_code'] == 'package_buy')
        {
            $row['storage'] = '';
            $row['brand_name'] = '';
            $row['package_goods_list'] = $packageController->get_package_goods_list($row['goods_id']);
        }

        //处理货品id
        $row['product_id'] = empty($row['product_id']) ? 0 : $row['product_id'];

        $goods_list[] = $row;
    }

    $attr = array();
    $arr  = array();
    foreach ($goods_attr AS $index => $array_val)
    {
        foreach ($array_val AS $value)
        {
            $arr = explode(':', $value);//以 : 号将属性拆开
            $attr[$index][] =  @array('name' => $arr[0], 'value' => $arr[1]);
        }
    }

    return array('goods_list' => $goods_list, 'attr' => $attr);
}

/**
 * 取得礼包列表
 * @param   integer     $package_id  订单商品表礼包类商品id
 * @return array
 */
function get_package_goods_list($package_id)
{
    $sql = "SELECT pg.goods_id, g.goods_name, (CASE WHEN pg.product_id > 0 THEN p.product_number ELSE g.goods_number END) AS goods_number, p.goods_attr, p.product_id, pg.goods_number AS
            order_goods_number, g.goods_sn, g.is_real, p.product_sn
            FROM " . $GLOBALS['ecs']->table('package_goods') . " AS pg
                LEFT JOIN " .$GLOBALS['ecs']->table('goods') . " AS g ON pg.goods_id = g.goods_id
                LEFT JOIN " . $GLOBALS['ecs']->table('products') . " AS p ON pg.product_id = p.product_id
            WHERE pg.package_id = '$package_id'";
    $resource = $GLOBALS['db']->query($sql);
    if (!$resource)
    {
        return array();
    }

    $row = array();

    /* 生成结果数组 取存在货品的商品id 组合商品id与货品id */
    $good_product_str = '';
    while ($_row = $GLOBALS['db']->fetch_array($resource))
    {
        if ($_row['product_id'] > 0)
        {
            /* 取存商品id */
            $good_product_str .= ',' . $_row['goods_id'];

            /* 组合商品id与货品id */
            $_row['g_p'] = $_row['goods_id'] . '_' . $_row['product_id'];
        }
        else
        {
            /* 组合商品id与货品id */
            $_row['g_p'] = $_row['goods_id'];
        }

        //生成结果数组
        $row[] = $_row;
    }
    $good_product_str = trim($good_product_str, ',');

    /* 释放空间 */
    unset($resource, $_row, $sql);

    /* 取商品属性 */
    if ($good_product_str != '')
    {
        $sql = "SELECT ga.goods_attr_id, ga.attr_value, ga.attr_price, a.attr_name
                FROM " .$GLOBALS['ecs']->table('goods_attr'). " AS ga, " .$GLOBALS['ecs']->table('attribute'). " AS a
                WHERE a.attr_id = ga.attr_id
                AND a.attr_type = 1
                AND goods_id IN ($good_product_str)";
        $result_goods_attr = $GLOBALS['db']->getAll($sql);

        $_goods_attr = array();
        foreach ($result_goods_attr as $value)
        {
            $_goods_attr[$value['goods_attr_id']] = $value;
        }
    }

    /* 过滤货品 */
    $format[0] = "%s:%s[%d] <br>";
    $format[1] = "%s--[%d]";
    foreach ($row as $key => $value)
    {
        if ($value['goods_attr'] != '')
        {
            $goods_attr_array = explode('|', $value['goods_attr']);

            $goods_attr = array();
            foreach ($goods_attr_array as $_attr)
            {
                $goods_attr[] = sprintf($format[0], $_goods_attr[$_attr]['attr_name'], $_goods_attr[$_attr]['attr_value'], $_goods_attr[$_attr]['attr_price']);
            }

            $row[$key]['goods_attr_str'] = implode('', $goods_attr);
        }

        $row[$key]['goods_name'] = sprintf($format[1], $value['goods_name'], $value['order_goods_number']);
    }

    return $row;


//    $sql = "SELECT pg.goods_id, CONCAT(g.goods_name, ' -- [', pg.goods_number, ']') AS goods_name,
//            g.goods_number, pg.goods_number AS order_goods_number, g.goods_sn, g.is_real " .
//            "FROM " . $GLOBALS['ecs']->table('package_goods') . " AS pg, " .
//                $GLOBALS['ecs']->table('goods') . " AS g " .
//            "WHERE pg.package_id = '$package_id' " .
//            "AND pg.goods_id = g.goods_id ";
//    $row = $GLOBALS['db']->getAll($sql);
//
//    return $row;
}

/**
 * 判断订单是否已发货（含部分发货）
 * @param   int     $order_id  订单 id
 * @return  int     1，已发货；0，未发货
 */
function order_deliveryed($order_id)
{
    $return_res = 0;

    if (empty($order_id))
    {
        return $return_res;
    }

    $sql = 'SELECT COUNT(delivery_id)
             FROM ' . $GLOBALS['ecs']->table('delivery_order') . '
             WHERE order_id = \''. $order_id . '\'
             AND (status = '.DO_DELIVERED.' or status = '.DO_RECEIVED.' or status = '.DO_PACKED.') ';
    $sum = $GLOBALS['db']->getOne($sql);

    if ($sum)
    {
        $return_res = 1;
    }

    return $return_res;
}

/**
 * 删除发货单(不包括已退货的单子)
 * @param   int     $order_id  订单 id
 * @return  int     1，成功；0，失败
 */
function del_order_delivery($order_id)
{
    $return_res = 0;

    if (empty($order_id))
    {
        return $return_res;
    }

    $sql = 'DELETE O, G
            FROM ' . $GLOBALS['ecs']->table('delivery_order') . ' AS O, ' . $GLOBALS['ecs']->table('delivery_goods') . ' AS G
            WHERE O.order_id = \'' . $order_id . '\'
            AND ( O.status = '.DO_DELIVERED.' or O.status = '.DO_RECEIVED.' or O.status = '.DO_PACKED.' or O.status = '.DO_COLLECTED.' or O.status = '.DO_NORMAL.')
            AND O.delivery_id = G.delivery_id';
    $query = $GLOBALS['db']->query($sql, 'SILENT');

    if ($query)
    {
        $return_res = 1;
    }

    return $return_res;
}

/**
 * 删除订单所有相关单子
 * @param   int     $order_id      订单 id
 * @param   int     $action_array  操作列表 Array('delivery', 'back', ......)
 * @return  int     1，成功；0，失败
 */
function del_delivery($order_id, $action_array)
{
    $return_res = 0;

    if (empty($order_id) || empty($action_array))
    {
        return $return_res;
    }

    $query_delivery = 1;
    $query_back = 1;
    if (in_array('delivery', $action_array))
    {
        $sql = 'DELETE O, G
                FROM ' . $GLOBALS['ecs']->table('delivery_order') . ' AS O, ' . $GLOBALS['ecs']->table('delivery_goods') . ' AS G
                WHERE O.order_id = \'' . $order_id . '\'
                AND O.delivery_id = G.delivery_id';
        $query_delivery = $GLOBALS['db']->query($sql, 'SILENT');
    }
    if (in_array('back', $action_array))
    {
        $sql = 'DELETE O, G
                FROM ' . $GLOBALS['ecs']->table('back_order') . ' AS O, ' . $GLOBALS['ecs']->table('back_goods') . ' AS G
                WHERE O.order_id = \'' . $order_id . '\'
                AND O.back_id = G.back_id';
        $query_back = $GLOBALS['db']->query($sql, 'SILENT');
    }

    if ($query_delivery && $query_back)
    {
        $return_res = 1;
    }

    return $return_res;
}

/**
 *  获取发货单列表信息
 *
 * @access  public
 * @param
 *
 * @return void
 */
function delivery_list()
{
    $result = get_filter();
    if ($result === false)
    {
        $aiax = isset($_GET['is_ajax']) ? $_GET['is_ajax'] : 0;

        /* 过滤信息 */
        $filter['delivery_sn'] = empty($_REQUEST['delivery_sn']) ? '' : trim($_REQUEST['delivery_sn']);
        $filter['order_sn'] = empty($_REQUEST['order_sn']) ? '' : trim($_REQUEST['order_sn']);
        $filter['order_id'] = empty($_REQUEST['order_id']) ? 0 : intval($_REQUEST['order_id']);
        if ($aiax == 1 && !empty($_REQUEST['consignee']))
        {
            $_REQUEST['consignee'] = json_str_iconv($_REQUEST['consignee']);
        }
        $filter['consignee'] = empty($_REQUEST['consignee']) ? '' : trim($_REQUEST['consignee']);
        $filter['status'] = isset($_REQUEST['status']) ? $_REQUEST['status'] : -1;

        $filter['sort_by'] = empty($_REQUEST['sort_by']) ? 'update_time' : trim($_REQUEST['sort_by']);
        $filter['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
        $filter['t_status'] = empty($_REQUEST['t_status']) ? '' : trim($_REQUEST['t_status']);
        $filter['l_id']     = empty($_REQUEST['l_id']) ? 0 : intval($_REQUEST['l_id']);
        $filter['country']     = empty($_REQUEST['country']) ? 0 : intval($_REQUEST['country']);

        $where = 'WHERE 1 ';
        if ($filter['order_sn'])
        {
            $where .= " AND order_sn LIKE '%" . mysql_like_quote($filter['order_sn']) . "%'";
        }
        if ($filter['consignee'])
        {
            $where .= " AND consignee LIKE '%" . mysql_like_quote($filter['consignee']) . "%'";
        }
        if ($filter['status'] >= 0)
        {
            $where .= " AND status = '" . mysql_like_quote($filter['status']) . "'";
        }
        if ($filter['delivery_sn'])
        {
            $where .= " AND delivery_sn LIKE '%" . mysql_like_quote($filter['delivery_sn']) . "%'";
        }
        if ($filter['t_status'])
        {
            $where .= " AND track_status = '".$filter['t_status']."' ";
        }
        if ($filter['l_id'] > 0)
        {
            $where .= " AND logistics_id = ".$filter['l_id']." ";
        }
        if ($filter['country'] > 0)
        {
            $where .= " AND country = ".$filter['country']." ";
        }

        /* 获取管理员信息 */
        $admin_info = admin_info();

        /* 如果管理员属于某个办事处，只列出这个办事处管辖的发货单 */
        if ($admin_info['agency_id'] > 0)
        {
            $where .= " AND agency_id = '" . $admin_info['agency_id'] . "' ";
        }

        /* 如果管理员属于某个供货商，只列出这个供货商的发货单 */
        if ($admin_info['suppliers_id'] > 0)
        {
            $where .= " AND suppliers_id = '" . $admin_info['suppliers_id'] . "' ";
        }

        /* 分页大小 */
        $filter['page'] = empty($_REQUEST['page']) || (intval($_REQUEST['page']) <= 0) ? 1 : intval($_REQUEST['page']);

        if (isset($_REQUEST['page_size']) && intval($_REQUEST['page_size']) > 0)
        {
            $filter['page_size'] = intval($_REQUEST['page_size']);
        }
        elseif (isset($_COOKIE['ECSCP']['page_size']) && intval($_COOKIE['ECSCP']['page_size']) > 0)
        {
            $filter['page_size'] = intval($_COOKIE['ECSCP']['page_size']);
        }
        else
        {
            $filter['page_size'] = 15;
        }

        /* 记录总数 */
        $sql = "SELECT COUNT(*) FROM " . $GLOBALS['ecs']->table('delivery_order') . $where;
        $filter['record_count']   = $GLOBALS['db']->getOne($sql);
        $filter['page_count']     = $filter['record_count'] > 0 ? ceil($filter['record_count'] / $filter['page_size']) : 1;

        /* 查询 */
        $sql = "SELECT delivery_id, delivery_sn, order_sn, order_id, add_time, action_user, consignee, country,
                       province, city, district, tel, status, update_time, email, suppliers_id
                FROM " . $GLOBALS['ecs']->table("delivery_order") . "
                $where
                ORDER BY " . $filter['sort_by'] . " " . $filter['sort_order']. "
                LIMIT " . ($filter['page'] - 1) * $filter['page_size'] . ", " . $filter['page_size'] . " ";

        set_filter($filter, $sql);
    }
    else
    {
        $sql    = $result['sql'];
        $filter = $result['filter'];
    }

    /* 获取供货商列表 */
    $suppliers_list = get_suppliers_list();
    $_suppliers_list = array();
    foreach ($suppliers_list as $value)
    {
        $_suppliers_list[$value['suppliers_id']] = $value['suppliers_name'];
    }

    $row = $GLOBALS['db']->getAll($sql);

    /* 格式化数据 */
    foreach ($row AS $key => $value)
    {
        $row[$key]['add_time'] = local_date($GLOBALS['_CFG']['time_format'], $value['add_time']);
        $row[$key]['update_time'] = local_date($GLOBALS['_CFG']['time_format'], $value['update_time']);
        if ($value['status'] == 1)
        {
            $row[$key]['status_name'] = $GLOBALS['_LANG']['delivery_status'][1];
        }
        elseif ($value['status'] == 2)
        {
            $row[$key]['status_name'] = $GLOBALS['_LANG']['delivery_status'][2];
        }
        else
        {
        $row[$key]['status_name'] = $GLOBALS['_LANG']['delivery_status'][0];
        }
        $row[$key]['suppliers_name'] = isset($_suppliers_list[$value['suppliers_id']]) ? $_suppliers_list[$value['suppliers_id']] : '';
    }
    $arr = array('delivery' => $row, 'filter' => $filter, 'page_count' => $filter['page_count'], 'record_count' => $filter['record_count']);

    return $arr;
}

/**
 *  获取退货单列表信息
 *
 * @access  public
 * @param
 *
 * @return void
 */
function back_list()
{

        $aiax =1;

        /* 过滤信息 */
        $_REQUEST['delivery_sn'] = empty($_REQUEST['delivery_sn']) ? '' : trim($_REQUEST['delivery_sn']);
        $_REQUEST['order_sn'] = empty($_REQUEST['order_sn']) ? '' : trim($_REQUEST['order_sn']);
        $_REQUEST['order_id'] = empty($_REQUEST['order_id']) ? 0 : intval($_REQUEST['order_id']);
        if ($aiax == 1 && !empty($_REQUEST['consignee']))
        {
            $_REQUEST['consignee'] = json_str_iconv($_REQUEST['consignee']);
        }
        $_REQUEST['consignee'] = empty($_REQUEST['consignee']) ? '' : trim($_REQUEST['consignee']);

        $_REQUEST['sort_by'] = empty($_REQUEST['sort_by']) ? 'update_time' : trim($_REQUEST['sort_by']);
        $_REQUEST['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);

        $where = 'WHERE 1 ';
        if ($_REQUEST['order_sn'])
        {
            $where .= " AND order_sn LIKE '%" . mysql_like_quote($_REQUEST['order_sn']) . "%'";
        }
        if ($_REQUEST['consignee'])
        {
            $where .= " AND consignee LIKE '%" . mysql_like_quote($_REQUEST['consignee']) . "%'";
        }
        if ($_REQUEST['delivery_sn'])
        {
            $where .= " AND delivery_sn LIKE '%" . mysql_like_quote($_REQUEST['delivery_sn']) . "%'";
        }

        /* 获取管理员信息 */
        $admin_info = admin_info();

        /* 如果管理员属于某个办事处，只列出这个办事处管辖的发货单 */
        if ($admin_info['agency_id'] > 0)
        {
            $where .= " AND agency_id = '" . $admin_info['agency_id'] . "' ";
        }

        /* 如果管理员属于某个供货商，只列出这个供货商的发货单 */
        if ($admin_info['suppliers_id'] > 0)
        {
            $where .= " AND suppliers_id = '" . $admin_info['suppliers_id'] . "' ";
        }

        /* 分页大小 */
        $_REQUEST['page'] = empty($_REQUEST['page']) || (intval($_REQUEST['page']) <= 0) ? 1 : intval($_REQUEST['page']);

        // if (isset($_REQUEST['page_size']) && intval($_REQUEST['page_size']) > 0)
        // {
        //     $_REQUEST['page_size'] = intval($_REQUEST['page_size']);
        // }
        // elseif (isset($_COOKIE['ECSCP']['page_size']) && intval($_COOKIE['ECSCP']['page_size']) > 0)
        // {
        //     $_REQUEST['page_size'] = intval($_COOKIE['ECSCP']['page_size']);
        // }
        // else
        // {
        //     $_REQUEST['page_size'] = 15;
        // }

        /* 记录总数 */
        $sql = "SELECT COUNT(*) FROM " . $GLOBALS['ecs']->table('back_order') . $where;
        $_REQUEST['record_count']   = $GLOBALS['db']->getOne($sql);
        // $_REQUEST['page_count']     = $_REQUEST['record_count'] > 0 ? ceil($_REQUEST['record_count'] / $_REQUEST['page_size']) : 1;

        /* 查询 */
        $sql = "SELECT back_id, delivery_sn, order_sn, order_id, add_time, action_user, consignee, country,
                       province, city, district, tel, status, update_time, email, return_time
                FROM " . $GLOBALS['ecs']->table("back_order") . "
                $where
                ORDER BY " . $_REQUEST['sort_by'] . " " . $_REQUEST['sort_order']. "
                LIMIT " . $_REQUEST['start'] . ",$_REQUEST[page_size]";

    $row = $GLOBALS['db']->getAll($sql);

    /* 格式化数据 */
    foreach ($row AS $key => $value)
    {
        $row[$key]['return_time'] = local_date($GLOBALS['_CFG']['time_format'], $value['return_time']);
        $row[$key]['add_time'] = local_date($GLOBALS['_CFG']['time_format'], $value['add_time']);
        $row[$key]['update_time'] = local_date($GLOBALS['_CFG']['time_format'], $value['update_time']);
        if ($value['status'] == 1)
        {
            $row[$key]['status_name'] = $GLOBALS['_LANG']['delivery_status'][1];
        }
        else
        {
        $row[$key]['status_name'] = $GLOBALS['_LANG']['delivery_status'][0];
        }
        $row[$key]['_action'] = "<a href='order.php?act=back_info&back_id=$value[back_id]'>"._L('detail', '查看')."</a>|".
        "<a onclick='cellRemove(this);' data-url='order.php?act=operate&remove_back=1&back_id=$value[back_id]'>"._L('remove', '移除')."</a>";
    }
    $arr = array('back' => $row, 'record_count' => $_REQUEST['record_count']);

    return $arr;
}

/**
 * 取得退货单信息
 * @param   int     $back_id   退货单 id（如果 back_id > 0 就按 id 查，否则按 sn 查）
 * @return  array   退货单信息（金额都有相应格式化的字段，前缀是 formated_ ）
 */
function back_order_info($back_id)
{
    $return_order = array();
    if (empty($back_id) || !is_numeric($back_id))
    {
        return $return_order;
    }

    $where = '';
    /* 获取管理员信息 */
    $admin_info = admin_info();

    /* 如果管理员属于某个办事处，只列出这个办事处管辖的发货单 */
    if ($admin_info['agency_id'] > 0)
    {
        $where .= " AND agency_id = '" . $admin_info['agency_id'] . "' ";
    }

    /* 如果管理员属于某个供货商，只列出这个供货商的发货单 */
    if ($admin_info['suppliers_id'] > 0)
    {
        $where .= " AND suppliers_id = '" . $admin_info['suppliers_id'] . "' ";
    }

    $sql = "SELECT * FROM " . $GLOBALS['ecs']->table('back_order') . "
            WHERE back_id = '$back_id'
            $where
            LIMIT 0, 1";
    $back = $GLOBALS['db']->getRow($sql);
    if ($back)
    {
        /* 格式化金额字段 */
        $back['formated_insure_fee']     = price_format($back['insure_fee'], false);
        $back['formated_shipping_fee']   = price_format($back['shipping_fee'], false);

        /* 格式化时间字段 */
        $back['formated_add_time']       = local_date($GLOBALS['_CFG']['time_format'], $back['add_time']);
        $back['formated_update_time']    = local_date($GLOBALS['_CFG']['time_format'], $back['update_time']);
        $back['formated_return_time']    = local_date($GLOBALS['_CFG']['time_format'], $back['return_time']);

        $return_order = $back;
    }

    return $return_order;
}

/**
 * 获取超级礼包商品已发货数
 *
 * @param       int         $package_id         礼包ID
 * @param       int         $goods_id           礼包的产品ID
 * @param       int         $order_id           订单ID
 * @param       varchar     $extension_code     虚拟代码
 * @param       int         $product_id         货品id
 *
 * @return  int     数值
 */
function package_sended($package_id, $goods_id, $order_id, $extension_code, $product_id = 0)
{
    if (empty($package_id) || empty($goods_id) || empty($order_id) || empty($extension_code))
    {
        return false;
    }

    $sql = "SELECT SUM(DG.send_number)
            FROM " . $GLOBALS['ecs']->table('delivery_goods') . " AS DG, " . $GLOBALS['ecs']->table('delivery_order') . " AS o
            WHERE o.delivery_id = DG.delivery_id
            AND o.status IN (".DO_DELIVERED.", ".DO_NORMAL.", ".DO_RECEIVED.", ".DO_PACKED.", ".DO_COLLECTED.")
            AND o.order_id = '$order_id'
            AND DG.parent_id = '$package_id'
            AND DG.goods_id = '$goods_id'
            AND DG.extension_code = '$extension_code'";
    $sql .= ($product_id > 0) ? " AND DG.product_id = '$product_id'" : '';

    $send = $GLOBALS['db']->getOne($sql);

    return empty($send) ? 0 : $send;
}

/**
 * 改变订单中商品库存
 * @param   int     $order_id  订单 id
 * @param   array   $_sended   Array(‘商品id’ => ‘此单发货数量’)
 * @param   array   $goods_list
 * @return  Bool
 */
function change_order_goods_storage_split($order_id, $_sended, $goods_list = array())
{
    /* 参数检查 */
    if (!is_array($_sended) || empty($order_id))
    {
        return false;
    }

    foreach ($_sended as $key => $value)
    {
        // 商品（超值礼包）
        if (is_array($value))
        {
            if (!is_array($goods_list))
            {
                $goods_list = array();
            }
            foreach ($goods_list as $goods)
            {
                if (($key != $goods['rec_id']) || (!isset($goods['package_goods_list']) || !is_array($goods['package_goods_list'])))
                {
                    continue;
                }

                // 超值礼包无库存，只减超值礼包商品库存
                foreach ($goods['package_goods_list'] as $package_goods)
                {
                    if (!isset($value[$package_goods['goods_id']]))
                    {
                        continue;
                    }

                    // 减库存：商品（超值礼包）（实货）、商品（超值礼包）（虚货）
                    $sql = "UPDATE " . $GLOBALS['ecs']->table('goods') ."
                            SET goods_number = goods_number - '" . $value[$package_goods['goods_id']] . "'
                            WHERE goods_id = '" . $package_goods['goods_id'] . "' ";
                    $GLOBALS['db']->query($sql);
                }
            }
        }
        // 商品（实货）
        elseif (!is_array($value))
        {
            /* 检查是否为商品（实货） */
            foreach ($goods_list as $goods)
            {
                if ($goods['rec_id'] == $key && $goods['is_real'] == 1)
                {
                    $sql = "UPDATE " . $GLOBALS['ecs']->table('goods') . "
                            SET goods_number = goods_number - '" . $value . "'
                            WHERE goods_id = '" . $goods['goods_id'] . "' ";
                    $GLOBALS['db']->query($sql, 'SILENT');
                    break;
                }
            }
        }
    }

    return true;
}

/**
 *  超值礼包虚拟卡发货、跳过修改订单商品发货数的虚拟卡发货
 *
 * @access  public
 * @param   array      $goods      超值礼包虚拟商品列表数组
 * @param   string      $order_sn   本次操作的订单
 *
 * @return  boolen
 */
function package_virtual_card_shipping($goods, $order_sn)
{
    if (!is_array($goods))
    {
        return false;
    }

    /* 包含加密解密函数所在文件 */
    include_once(ROOT_PATH . 'includes/lib_code.php');

    // 取出超值礼包中的虚拟商品信息
    foreach ($goods as $virtual_goods_key => $virtual_goods_value)
    {
        /* 取出卡片信息 */
        $sql = "SELECT card_id, card_sn, card_password, end_date, crc32
                FROM ".$GLOBALS['ecs']->table('virtual_card')."
                WHERE goods_id = '" . $virtual_goods_value['goods_id'] . "'
                AND is_saled = 0
                LIMIT " . $virtual_goods_value['num'];
        $arr = $GLOBALS['db']->getAll($sql);
        /* 判断是否有库存 没有则推出循环 */
        if (count($arr) == 0)
        {
            continue;
        }

        $card_ids = array();
        $cards = array();

        foreach ($arr as $virtual_card)
        {
            $card_info = array();

            /* 卡号和密码解密 */
            if ($virtual_card['crc32'] == 0 || $virtual_card['crc32'] == crc32(AUTH_KEY))
            {
                $card_info['card_sn'] = decrypt($virtual_card['card_sn']);
                $card_info['card_password'] = decrypt($virtual_card['card_password']);
            }
            elseif ($virtual_card['crc32'] == crc32(OLD_AUTH_KEY))
            {
                $card_info['card_sn'] = decrypt($virtual_card['card_sn'], OLD_AUTH_KEY);
                $card_info['card_password'] = decrypt($virtual_card['card_password'], OLD_AUTH_KEY);
            }
            else
            {
                $GLOBALS['db']->query('ROLLBACK');
                return false;
            }
            $card_info['end_date'] = date($GLOBALS['_CFG']['date_format'], $virtual_card['end_date']);
            $card_ids[] = $virtual_card['card_id'];
            $cards[] = $card_info;
        }

        /* 标记已经取出的卡片 */
        $sql = "UPDATE ".$GLOBALS['ecs']->table('virtual_card')." SET ".
           "is_saled = 1 ,".
           "order_sn = '$order_sn' ".
           "WHERE " . db_create_in($card_ids, 'card_id');
        if (!$GLOBALS['db']->query($sql))
        {
           	$GLOBALS['db']->query('ROLLBACK');
            return false;
        }

        /* 获取订单信息 */
        $sql = "SELECT order_id, order_sn, consignee, email, order_type FROM ".$GLOBALS['ecs']->table('order_info'). " WHERE order_sn = '$order_sn'";
        $order = $GLOBALS['db']->GetRow($sql);

        $cfg = $GLOBALS['_CFG']['send_ship_email'];
        if ($cfg == '1')
        {
            if (can_send_mail($order['email'], true) && !in_array($order['order_type'], [Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE, Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT]))
            {
                /* 发送邮件 */
                $GLOBALS['smarty']->assign('virtual_card',                   $cards);
                $GLOBALS['smarty']->assign('order',                          $order);
                $GLOBALS['smarty']->assign('goods',                          $virtual_goods_value);

                $GLOBALS['smarty']->assign('send_time', date('Y-m-d H:i:s'));
                $GLOBALS['smarty']->assign('shop_name', $GLOBALS['_CFG']['shop_name']);
                $GLOBALS['smarty']->assign('send_date', date('Y-m-d'));
                $GLOBALS['smarty']->assign('sent_date', date('Y-m-d'));

                $tpl = get_mail_template('virtual_card');
                $content = $GLOBALS['smarty']->fetch('str:' . $tpl['template_content']);
                send_mail($order['consignee'], $order['email'], $tpl['template_subject'] . ' - ' . $order['order_sn'], $content, $tpl['is_html']);
            }
        }
    }

    return true;
}

/**
 * 删除发货单时进行退货
 *
 * @access   public
 * @param    int     $delivery_id      发货单id
 * @param    array   $delivery_order   发货单信息数组
 *
 * @return  void
 */
function delivery_return_goods($delivery_id, $delivery_order)
{
    /* 查询：取得发货单商品 */
    $goods_sql = "SELECT *
                 FROM " . $GLOBALS['ecs']->table('delivery_goods') . "
                 WHERE delivery_id = " . $delivery_order['delivery_id'];
    $goods_list = $GLOBALS['db']->getAll($goods_sql);
    
    /* 更新： */
    $sql = "UPDATE " . $GLOBALS['ecs']->table('order_goods') .
           " SET send_number = send_number-'".$goods_list[0]['send_number']. "'".
           " WHERE order_id = '".$delivery_order['order_id']."' AND goods_id = '".$goods_list[0]['goods_id']."' LIMIT 1";

    if(!$GLOBALS['db']->query($sql))
    {
    	return false;
    }

    $sql = "UPDATE " . $GLOBALS['ecs']->table('order_info') .
           " SET shipping_status = '0' , order_status = 1".
           " WHERE order_id = '".$delivery_order['order_id']."' LIMIT 1";
    if(!$GLOBALS['db']->query($sql))
    {
    	return false;
    }
    return true;
}

/**
 * 删除发货单时删除其在订单中的发货单号
 *
 * @access   public
 * @param    int      $order_id              定单id
 * @param    string   $delivery_invoice_no   发货单号
 *
 * @return  void
 */
function del_order_invoice_no($order_id, $delivery_invoice_no)
{
    /* 查询：取得订单中的发货单号 */
    $sql = "SELECT invoice_no
            FROM " . $GLOBALS['ecs']->table('order_info') . "
            WHERE order_id = '$order_id'";
    $order_invoice_no = $GLOBALS['db']->getOne($sql);

    /* 如果为空就结束处理 */
    if (empty($order_invoice_no))
    {
        return true;
    }

    /* 去除当前发货单号 */
    $order_array = explode('<br>', $order_invoice_no);
    $delivery_array = explode('<br>', $delivery_invoice_no);

    foreach ($order_array as $key => $invoice_no)
    {
        if ($ii = array_search($invoice_no, $delivery_array))
        {
            unset($order_array[$key], $delivery_array[$ii]);
        }
    }

    $arr['invoice_no'] = implode('<br>', $order_array);

    return update_order($order_id, $arr);
}

/**
 * 获取站点根目录网址
 *
 * @access  private
 * @return  Bool
 */
function get_site_root_url()
{
    return ((isset($_SERVER['HTTPS']) && (strtolower($_SERVER['HTTPS']) != 'off')) ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . str_replace('/' . ADMIN_PATH . '/order.php', '', PHP_SELF);

}

function get_user_name($user_id)
{
	$result='匿名';
	if(isset($user_id))
	{
		$sql="select user_name from ".$GLOBALS['ecs']->table('users')." where user_id='".$user_id."'";
		$result=$GLOBALS['db']->getOne($sql);
	}
	return $result;
}

function export_tqb_b2_xls($order_sns)
{
    global $db, $ecs;
    
    $sql = "SELECT * " .
            "FROM " . $ecs->table('order_info') . " as oi " .
            "WHERE `order_sn` " . db_create_in($order_sns);
    $orders = $db->getAll($sql);
    
    require_once ROOT_PATH . 'includes/excel/PHPExcel.php';
    $objPHPExcel = new PHPExcel();
    $objPHPExcel->setActiveSheetIndex(0);
    
    $objPHPExcel->getActiveSheet()
    ->setCellValue('A1', ecs_iconv(EC_CHARSET, 'UTF8', '客戶管理編號'))
    ->setCellValue('B1', ecs_iconv(EC_CHARSET, 'UTF8', '托運單種類'))
    ->setCellValue('C1', ecs_iconv(EC_CHARSET, 'UTF8', '商品種類'))
    ->setCellValue('D1', ecs_iconv(EC_CHARSET, 'UTF8', '托運單編號'))
    ->setCellValue('E1', ecs_iconv(EC_CHARSET, 'UTF8', '希望配送日指定'))
    ->setCellValue('F1', ecs_iconv(EC_CHARSET, 'UTF8', '預定出貨日'))
    ->setCellValue('G1', ecs_iconv(EC_CHARSET, 'UTF8', '預定收件日'))
    ->setCellValue('H1', ecs_iconv(EC_CHARSET, 'UTF8', '希望收件時段'))
    ->setCellValue('I1', ecs_iconv(EC_CHARSET, 'UTF8', '收件人編號'))
    ->setCellValue('J1', ecs_iconv(EC_CHARSET, 'UTF8', '收件人電話'))
    ->setCellValue('K1', ecs_iconv(EC_CHARSET, 'UTF8', '收件人內線電話'))
    ->setCellValue('L1', ecs_iconv(EC_CHARSET, 'UTF8', '預留1'))
    ->setCellValue('M1', ecs_iconv(EC_CHARSET, 'UTF8', '收件人地址1'))
    ->setCellValue('N1', ecs_iconv(EC_CHARSET, 'UTF8', '收件人地址2'))
    ->setCellValue('O1', ecs_iconv(EC_CHARSET, 'UTF8', '收件人地址3'))
    ->setCellValue('P1', ecs_iconv(EC_CHARSET, 'UTF8', '收件人地址4'))
    ->setCellValue('Q1', ecs_iconv(EC_CHARSET, 'UTF8', '收件人地址5'))
    ->setCellValue('R1', ecs_iconv(EC_CHARSET, 'UTF8', '收件人姓名'))
    ->setCellValue('S1', ecs_iconv(EC_CHARSET, 'UTF8', '寄件人編號'))
    ->setCellValue('T1', ecs_iconv(EC_CHARSET, 'UTF8', '寄件人電話'))
    ->setCellValue('U1', ecs_iconv(EC_CHARSET, 'UTF8', '寄件人內線電話'))
    ->setCellValue('V1', ecs_iconv(EC_CHARSET, 'UTF8', '預留2'))
    ->setCellValue('W1', ecs_iconv(EC_CHARSET, 'UTF8', '寄件人地址1'))
    ->setCellValue('X1', ecs_iconv(EC_CHARSET, 'UTF8', '寄件人地址2'))
    ->setCellValue('Y1', ecs_iconv(EC_CHARSET, 'UTF8', '寄件人地址3'))
    ->setCellValue('Z1', ecs_iconv(EC_CHARSET, 'UTF8', '寄件人姓名'))
    ->setCellValue('AA1', ecs_iconv(EC_CHARSET, 'UTF8', '內載物編號1'))
    ->setCellValue('AB1', ecs_iconv(EC_CHARSET, 'UTF8', '內載物1'))
    ->setCellValue('AC1', ecs_iconv(EC_CHARSET, 'UTF8', '內載物編號2'))
    ->setCellValue('AD1', ecs_iconv(EC_CHARSET, 'UTF8', '內載物2'))
    ->setCellValue('AE1', ecs_iconv(EC_CHARSET, 'UTF8', '特別貨物處理1'))
    ->setCellValue('AF1', ecs_iconv(EC_CHARSET, 'UTF8', '特別貨物處理2'))
    ->setCellValue('AG1', ecs_iconv(EC_CHARSET, 'UTF8', '注意事項'))
    ->setCellValue('AH1', ecs_iconv(EC_CHARSET, 'UTF8', '貨款金額'))
    ->setCellValue('AI1', ecs_iconv(EC_CHARSET, 'UTF8', '代理店自取設定'))
    ->setCellValue('AJ1', ecs_iconv(EC_CHARSET, 'UTF8', '暫代保管的辦事處編號'))
    ->setCellValue('AK1', ecs_iconv(EC_CHARSET, 'UTF8', '發行張數'))
    ->setCellValue('AL1', ecs_iconv(EC_CHARSET, 'UTF8', '付款客戶編號'))
    ->setCellValue('AM1', ecs_iconv(EC_CHARSET, 'UTF8', '付款客戶分類編號'))
    ->setCellValue('AN1', ecs_iconv(EC_CHARSET, 'UTF8', '運費管理號碼'))
    ;
    
    $set_cell_value = function ($pos, $value) use ($objPHPExcel) {
        $objPHPExcel->getActiveSheet()->setCellValueExplicit($pos, ecs_iconv(EC_CHARSET, 'UTF8', $value), PHPExcel_Cell_DataType::TYPE_STRING);
    };
    
    $i = 2;
    foreach ($orders AS $key => $order)
    {
        $set_cell_value('A'.$i, $order['order_sn']); // 客戶管理編號
        $set_cell_value('B'.$i, $order['shipping_fod'] ? '31' : '01'); // 托運單種類
        $set_cell_value('C'.$i, '0'); // 商品種類
        $set_cell_value('D'.$i, ''); // 托運單編號
        $set_cell_value('E'.$i, '0'); // 希望配送日指定
        $set_cell_value('F'.$i, local_date('dmY')); // 預定出貨日
        $set_cell_value('G'.$i, local_date('dmY', local_strtotime('+1 day'))); // 預定收件日
        $set_cell_value('H'.$i, '0000'); // 希望收件時段
        $set_cell_value('I'.$i, ''); // 收件人編號
        $set_cell_value('J'.$i, $order['mobile']); // 收件人電話
        $set_cell_value('K'.$i, ''); // 收件人內線電話
        $set_cell_value('L'.$i, ''); // 預留1
        $set_cell_value('M'.$i, $order['address']); // 收件人地址1
        $set_cell_value('N'.$i, ''); // 收件人地址2
        $set_cell_value('O'.$i, ''); // 收件人地址3
        $set_cell_value('P'.$i, ''); // 收件人地址4
        $set_cell_value('Q'.$i, ''); // 收件人地址5
        $set_cell_value('R'.$i, $order['consignee']); // 收件人姓名
        $set_cell_value('S'.$i, ''); // 寄件人編號
        $set_cell_value('T'.$i, '53356996'); // 寄件人電話
        $set_cell_value('U'.$i, ''); // 寄件人內線電話
        $set_cell_value('V'.$i, ''); // 預留2
        $set_cell_value('W'.$i, '502 Union Building'); // 寄件人地址1
        $set_cell_value('X'.$i, '112 How Ming Street'); // 寄件人地址2
        $set_cell_value('Y'.$i, 'Kwun Tong, Kowloon'); // 寄件人地址3
        $set_cell_value('Z'.$i, '友和 YOHO'); // 寄件人姓名
        $set_cell_value('AA'.$i, ''); // 內載物編號1
        $set_cell_value('AB'.$i, ''); // 內載物1
        $set_cell_value('AC'.$i, ''); // 內載物編號2
        $set_cell_value('AD'.$i, ''); // 內載物2
        $set_cell_value('AE'.$i, ''); // 特別貨物處理1
        $set_cell_value('AF'.$i, ''); // 特別貨物處理2
        $set_cell_value('AG'.$i, ''); // 注意事項
        $set_cell_value('AH'.$i, '0'); // 貨款金額
        $set_cell_value('AI'.$i, '0'); // 代理店自取設定
        $set_cell_value('AJ'.$i, ''); // 暫代保管的辦事處編號
        $set_cell_value('AK'.$i, '01'); // 發行張數
        $set_cell_value('AL'.$i, '85239647805'); // 付款客戶編號
        $set_cell_value('AM'.$i, ''); // 付款客戶分類編號
        $set_cell_value('AN'.$i, '01'); // 運費管理號碼
        $i++;
    }
    
    // Auto resize columns
    for ($col = 'A'; $col <= 'Z'; $col++)
    {
        $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
    }
    for ($col = 'A'; $col <= 'N'; $col++)
    {
        $objPHPExcel->getActiveSheet()->getColumnDimension('A'.$col)->setAutoSize(true);
    }
    
    // Create actual excel file data
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    
    // Output to browser
    header("Content-type: application/vnd.ms-excel; charset=utf-8");
    header("Content-Disposition: attachment; filename=B2.xls");
    $objWriter->save('php://output');
    exit;
}

function get_sales_agent_name()
{
	// Get sales agent name
	$salesagentController = new Yoho\cms\Controller\SalesagentController();
	$all_sa = $salesagentController->get_all_sales_agents();
	$salesagent = array();
	foreach ($all_sa AS $key => $sa){
		$salesagent[$sa['sa_id']] = $sa['name'];
	}

	return $salesagent;
}

function update_order_shipping($order_id, $region_id_list, $shipping_id, $insure = null)
{
    $shipping = shipping_area_info($shipping_id, $region_id_list);
    $order = order_info($order_id);
    
    $old_shipping_id = $order['shipping_id'];
    $old_shipping_name = $order['shipping_name'];
    $new_shipping_name = $shipping['shipping_name'];
    
    $order = array(
        'shipping_id' => $shipping_id,
        'shipping_name' => addslashes($shipping['shipping_name']),
    );
    update_order($order_id, $order);
    
    if($old_shipping_name != $new_shipping_name && $old_shipping_id != $shipping_id)
    {
        $order = order_info($order_id);
        $log = '<strong>'.$old_shipping_name.'</strong> 變更成 <strong>'.$new_shipping_name.'</strong>';
        order_log($step, $order, $log);
    }
    
    /* 更新商品总金额和订单总金额 */
    recalculate_order_fee($order_id);

    /* 清除首页缓存：发货单查询 */
    clear_cache_files('index.dwt');

}

function update_order_payment($order_id, $pay_id)
{
    $payment = payment_info($pay_id);
    $order = order_info($order_id);
    
    $old_pay_name = $order['pay_name'];
    $new_pay_name = $payment['pay_name'];
    $old_pay_id   = $order['pay_id'];
    
    $order = array(
        'pay_id' => $pay_id,
        'pay_name' => addslashes($payment['display_name']),
    );
    update_order($order_id, $order);
    
    /* Save order log */
    if($old_pay_name != $new_pay_name && $old_pay_id != $pay_id)
    {
        $order = order_info($order_id);
        $log = '<strong>'.$old_pay_name.'</strong> 變更成 <strong>'.$new_pay_name.'</strong>';
        order_log($step, $order, $log);
    }
    
    /* 更新商品总金额和订单总金额 */
    recalculate_order_fee($order_id);

    if ($payment['is_online_pay'] == '0') {
		$crmController = new Yoho\cms\Controller\CrmController();
		$crmController->init_offline_payment_flow($order_id);
	} else {
		$GLOBALS['db']->query("UPDATE " . $GLOBALS['ecs']->table("offline_payment") . " SET status = " . OFFLINE_PAYTMENT_CLOSED . " WHERE order_id = $order_id AND status < " . OFFLINE_PAYTMENT_FINISHED);
	}
}

/* If update order goods, update coupon amount */
function edit_order_goods_update_coupon($order)
{
    // Coupon
    $total['coupon'] = 0;
    $order_id = $order['order_id'];
    
    // Get order coupons list
    $sql = "SELECT coupon_id FROM ". $GLOBALS['ecs']->table('order_coupons').
    " WHERE order_id = " .$order['order_id'];
    $order['coupon_id'] = $GLOBALS['db']->getCol($sql);
    
    // Count order total goods price
    $sql = "SELECT SUM(goods_price * goods_number) FROM " . $GLOBALS['ecs']->table('order_goods') .
            "WHERE order_id = ".$order['order_id'];
    $order['goods_price'] = $GLOBALS['db']->getOne($sql);

    // If order have coupon, re count the amount.
    if (!empty($order['coupon_id']))
    {
        $coupons_flat = array();
        $coupons_percent = array();
        $coupons_free_shipping = array();
        foreach ($order['coupon_id'] as $coupon_id)
        {
            $coupon = coupon_info($coupon_id);

            if ($coupon)
            {
                if ($coupon['coupon_amount'] > 0)
                {
                    $coupons_flat[] = $coupon;
                }
                elseif ($coupon['coupon_discount'] > 0)
                {
                    $coupons_percent[] = $coupon;
                }
                if ($coupon['free_shipping'])
                {
                    $coupons_free_shipping[] = $coupon;
                }
            }
        }
        // Process flat discount first, then percentage discount
        foreach ($coupons_flat as $coupon)
        {
            $total['coupon'] += $coupon['coupon_amount'];
        }
        foreach ($coupons_percent as $coupon)
        {
            if ($coupon['cartwide']) // percentage apply to all products in cart
            {
                $total['coupon'] += floor(($coupon['coupon_discount'] / 100) * $order['goods_price']);
            }
            else
            {
                $coupon_amount = 0;
                $coupon_goods = array_reduce($coupon['goods_list'], 'array_merge', array());
                foreach ($goods AS $g)
                {
                    if (in_array($g['goods_id'], $coupon_goods))
                    {
                        $coupon_amount += floor(($coupon['coupon_discount'] / 100) * $g['goods_price'] * $g['goods_number']);
                    }
                }
                $total['coupon'] += $coupon_amount;
            }
        }
    }

    $total['coupon_formated'] = price_format($total['coupon'], false);
    
    $order['coupon'] = $total['coupon'];
    $order['coupon_formated'] = $total['coupon_formated'];

    update_order($order_id, $order);
    update_order_amount($order_id);
    /* 更新 pay_log */
    update_pay_log($order_id);
}

/* If order update, recalculate order fee */
function recalculate_order_fee($order_id)
{
    /* Step 1: Get order info */
    $order = order_info($order_id);
    $old_order = $order;
    $consignee = array('country'=>$order['country'], 'province'=>$order['province'], 'city'=>$order['city'], 'district'=>$order['district']);
    /* Step 2: Get order goods */
    $goods = order_goods($order_id);
    
    /* Step 3: count order fee */
    $orderController = new Yoho\cms\Controller\OrderController();
    $total = $orderController->order_fee_flow($order, $goods, $consignee, $platform = 'cms');
    
    $order['bonus']        = $total['bonus'];
    $order['coupon']       = $total['coupon'];
    $order['goods_amount'] = $total['goods_price'];
    $order['discount']     = $total['discount'];
    $order['surplus']      = $total['surplus'];
    $order['tax']          = $total['tax'];
    $order['cn_ship_tax']  = $total['cn_ship_tax'];
    $order['cp_price']     = !empty($total['cp_price']) ? $total['cp_price'] : '0';
    
    //$order['cn_ship_tax'] = 0;
    //if ($order['country'] == 3436 && ($order['shipping_id'] == 3 || $order['shipping_name'] == '速遞')){
    //    $total['cn_ship_tax'] = round(($order['goods_price'] * (ORDER_CN_SHIP_TAX / 100)), 2, PHP_ROUND_HALF_UP);
    //}

    /* Step 4: Update order shipping info */
    if ($order['shipping_id'] > 0)
    {
        $shipping = shipping_info($order['shipping_id']);
        $order['shipping_name'] = addslashes($shipping['shipping_name']);
    }
    $order['shipping_fee'] = $total['shipping_fee'];
    $order['insure_fee']   = $total['shipping_insure'];

    /* Step 5: Update order payment info */
    if ($order['pay_id'] > 0)
    {
        $payment = payment_info($order['pay_id']);//sbsn 货到付款
        // $order['pay_name'] = addslashes($payment['pay_name']);
        $order['pay_name'] = addslashes($payment['display_name']);
    }
    $order['pay_fee'] = $total['pay_fee'];
    $order['cod_fee'] = $total['cod_fee'];

    /* Step 6: Update order pack info */
    if ($order['pack_id'] > 0)
    {
        $pack               = pack_info($order['pack_id']);
        $order['pack_name'] = addslashes($pack['pack_name']);
    }
    $order['pack_fee'] = $total['pack_fee'];

    /* Step 7: Update order card info */
    if ($order['card_id'] > 0)
    {
        $card               = card_info($order['card_id']);
        $order['card_name'] = addslashes($card['card_name']);
    }
    $order['card_fee']      = $total['card_fee'];
    $order['order_amount']  = number_format($total['amount'], 2, '.', '');

    $order['integral_money']   = $total['integral_money'];
    $order['integral']         = $total['integral'];
    
    /* Step 8: Update order extension code info */
    if ($order['extension_code'] == 'exchange_goods')
    {
        $order['integral_money']   = 0;
        $order['integral']         = $total['exchange_integral'];
    }
    
    /* Step 9: Save order log */
    handle_order_money_log($old_order, $order);
    
    /* Step 10: Save order */
    update_order($order_id, $order);
    
    /* Step 11: Update user integral */
    update_user_integral_bouns($old_order, $order);
    
    /* Step 12: Update pay log */
    update_pay_log($order_id);
}

/* Set order log */
function order_log($step, $order, $log)
{
    global $_LANG;
    switch ($step)
    {
        case 'edit_goods':
            $action = '編輯產品 ';
            break;
        case 'add_goods':
            $action = '新增產品 ';
            break;
        case 'drop_order_goods':
            $action = '刪除產品 ';
            break;
        case 'refund':
            $action = '退款 ';
            break;
        case 'handle_payment_status':
            $action = '付款狀態變更 ';
            break;
        case 'payment':
            $action = '變更支付方式 ';
            break;
        case 'shipping':
            $action = '變更配送方式 ';
            break;
        case 'other':
            $action = '備註更新 ';
            break;
        case 'consignee':
            $action = '收貨人信息更新 ';
            if(!empty($log)) $action = '<br>';
            break;
        case 'money':
            $action = '費用信息更新 ';
            break;
        case 'modify_refund':
            $action = '修改退款申請';
            break;
        case 'insurance_claim':
            $action = '保險服務索賠';
            break;
        default:
            $action = $step;
            break;
    }
    
    $admin_id = $_SESSION['admin_id'];
    $sql = "SELECT user_name FROM ".$GLOBALS['ecs']->table('admin_user')." WHERE user_id = ".$admin_id." LIMIT 1";
    $admin_name = $GLOBALS['db']->getOne($sql);
    
    order_action($order['order_sn'], $order['order_status'], $order['shipping_status'], $order['pay_status'],
    '[' .$_LANG['system_log'] . '] ' .$action.  $log , $admin_name);
}

function handle_order_money_log($old_order, $new_order)
{
    $log = '';
    //shipping_fee, pay_fee, discount, integral, coupon
    if($old_order['shipping_fee'] != $new_order['shipping_fee']){
        $log .= '<br>配送費用修改: 由 <strong>'.$old_order['shipping_fee'].'</strong> 改成 <strong>'.$new_order['shipping_fee'].'</strong>';
    }
    if($old_order['discount'] != $new_order['discount']){
        $log .= '<br>折扣費用修改: 由 <strong>'.$old_order['discount'].'</strong> 改成 <strong>'.$new_order['discount'].'</strong>';
    }
    if($old_order['coupon'] != $new_order['coupon']){
        $log .= '<br>優惠卷金額修改: 由 <strong>'.$old_order['coupon'].'</strong> 改成 <strong>'.$new_order['coupon'].'</strong>';
    }
    if($old_order['integral'] != $new_order['integral']){
        $log .= '<br>積分修改: 由 <strong>'.$old_order['integral'].'</strong> 改成 <strong>'.$new_order['integral'].'</strong>';
    }
    if($old_order['pay_fee'] != $new_order['pay_fee']){
        if($new_order['pay_fee'] > 0)
        $log .= '<br>支付費用修改: 由 <strong>'.$old_order['pay_fee'].'</strong> 改成 <strong>'.$new_order['pay_fee'].'</strong>';
        else
        $log .= '<br>支付折扣修改: 由 <strong>'.$old_order['pay_fee'].'</strong> 改成 <strong>'.$new_order['pay_fee'].'</strong>';
    }
    if(!empty($log))order_log($step, $new_order, $log);
}

/* 如果余额、积分、红包有变化，做相应更新 */
function update_user_integral_bouns($old_order, $order)
{
    global $_LANG;
    
    if ($old_order['user_id'] > 0)
    {
        $user_money_change = $old_order['surplus'] - $order['surplus'];
        if ($user_money_change != 0)
        {
            log_account_change($old_order['user_id'], $user_money_change, 0, 0, 0, sprintf($_LANG['change_use_surplus'], $old_order['order_sn']));
        }

        $pay_points_change = $old_order['integral'] - $order['integral'];
        if ($pay_points_change != 0)
        {
            log_account_change($old_order['user_id'], 0, 0, 0, $pay_points_change, sprintf($_LANG['change_use_integral'], $old_order['order_sn']), ACT_OTHER, 0, ['type' => 'sales', 'sales_order_id' => $old_order['order_id']]);
        }

        if ($old_order['bonus_id'] != $order['bonus_id'])
        {
            if ($old_order['bonus_id'] > 0)
            {
                $sql = "UPDATE " . $ecs->table('user_bonus') .
                        " SET used_time = 0, order_id = 0 " .
                        "WHERE bonus_id = '$old_order[bonus_id]' LIMIT 1";
                $GLOBALS['db']->query($sql);
            }

            if ($order['bonus_id'] > 0)
            {
                $sql = "UPDATE " . $ecs->table('user_bonus') .
                        " SET used_time = '" . gmtime() . "', order_id = '$order_id' " .
                        "WHERE bonus_id = '$order[bonus_id]' LIMIT 1";
                $GLOBALS['db']->query($sql);
            }
        }
    }
}
?>
