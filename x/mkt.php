<?php
/**
 * ECSHOP 控制台首页
 * ============================================================================
 * 版权所有 2005-2010 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liuhui $
 * $Id: index.php 17163 2010-05-20 10:13:23Z liuhui $
 * Updated By Anthony@YOHO 2018-03-23
*/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

assign_query_info();
//$smarty->assign('ecs_version',  VERSION);
//$smarty->assign('ecs_release',  RELEASE);
//$smarty->assign('ecs_lang',     $_CFG['lang']);
//$smarty->assign('ecs_charset',  strtoupper(EC_CHARSET));
//$smarty->assign('install_date', local_date($_CFG['date_format'], $_CFG['install_date']));
$smarty->assign('ur_here',     $_LANG['mkt_method']);
$smarty->display('mkt.htm');

?>
