<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_base.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_order.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_supplier.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
if ($_REQUEST['act'] == 'change_description')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_order_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$order_id=isset($_REQUEST['order_id']) &&intval($_REQUEST['order_id']) ?intval($_REQUEST['order_id']) : 0;
	$new_description=isset($_REQUEST['new_description']) ?trim($_REQUEST['new_description']) : '';
	if(empty($order_id))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$order_info=is_order_exist($order_id);
	if(!$order_info)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if($agency_id>0 &&$order_info['agency_id']!=$agency_id)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_no_agency_access'];
		die($json->encode($result));
	}
	if($order_info['order_status']!=1)
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_order_no_acts'];
		die($json->encode($result));
	}
	if(!is_admin_order($order_id,erp_get_admin_id()))
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_order_no_access'];
		die($json->encode($result));
	}
	if(lock_order($order_id,'edit'))
	{
		$result['error']=0;
		$sql="update ".$ecs->table('erp_order')." set description='".$new_description."' where order_id='".$order_id."'";
		$db->query($sql);
		$action_remark = "編輯訂單 $order_info[order_sn] 描述 | " .
			(!empty($order_info['new_description']) ? "由 $order_info[new_description] 至 $new_description" : "新增 $new_description");
		$sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
				"VALUES(" . $order_id . ", " . ERP_UPDATE . "," . $_SESSION['admin_id'] . ",'" . mysql_escape_string($action_remark) . "')";
		$GLOBALS['db']->query($sql);
		die($json->encode($result));
	}
	else
	{
		$result['error']=-1;
		$result['message']=$_LANG['erp_order_no_accessibility'];
		die($json->encode($result));
	}
}
elseif ($_REQUEST['act'] == 'change_supplier')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_order_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$order_id=isset($_REQUEST['order_id']) &&intval($_REQUEST['order_id']) ?intval($_REQUEST['order_id']):0;
	$supplier_code=isset($_REQUEST['supplier_code']) ?trim($_REQUEST['supplier_code']): '';
	if(empty($order_id) ||empty($supplier_code))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$order_info=is_order_exist($order_id);
	if(!$order_info)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if($agency_id>0 &&$order_info['agency_id']!=$agency_id)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_no_agency_access'];
		die($json->encode($result));
	}
	if($order_info['order_status']!=1)
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_order_no_acts'];
		die($json->encode($result));
	}
	if(!is_admin_order($order_id,erp_get_admin_id()))
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_order_no_access'];
		die($json->encode($result));
	}
	if(lock_order($order_id,'edit'))
	{
		if($supplier_info = is_supplier_exist('',$supplier_code))
		{
			$sql="select supplier_id from ".$ecs->table('erp_supplier')." where code='".$supplier_code."'";
			$supplier_id=$db->getOne($sql);
			$sql="update ".$ecs->table('erp_order')." set supplier_id='".$supplier_id."', payment_term='" . get_supplier_payment_term($supplier_id) . "' where order_id='".$order_id."'";
			$db->query($sql);
			$action_remark = "編輯訂單 $order_info[order_sn] 供應商 | " .
				(!empty($order_info['supplier_id']) ? "由 " . is_supplier_exist($order_info['supplier_id'])['name'] . " 至 " . is_supplier_exist('',$supplier_code)['name'] : "設定為 " . is_supplier_exist('',$supplier_code)['name']);
			$sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
					"VALUES(" . $order_id . ", " . ERP_UPDATE . "," . $_SESSION['admin_id'] . ",'" . mysql_escape_string($action_remark) . "')";
			$GLOBALS['db']->query($sql);
			delete_order_item($order_id);
			$result['error']=0;
			die($json->encode($result));
		}
		else
		{
			$result['error']=4;
			$result['message']=$_LANG['erp_order_supplier_not_exist'];
			die($json->encode($result));
		}
	}
	else
	{
		$result['error']=-1;
		$result['message']=$_LANG['erp_order_no_accessibility'];
		die($json->encode($result));
	}
}
elseif ($_REQUEST['act'] == 'change_type')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_order_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$order_id=isset($_REQUEST['order_id']) &&intval($_REQUEST['order_id']) ?intval($_REQUEST['order_id']):0;
	$type_id=isset($_REQUEST['type_id']) ?trim($_REQUEST['type_id']): '';
	if(empty($order_id) || $type_id=="")
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$order_info=is_order_exist($order_id);
	if(!$order_info)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if($agency_id>0 &&$order_info['agency_id']!=$agency_id)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_no_agency_access'];
		die($json->encode($result));
	}
	if($order_info['order_status']!=1)
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_order_no_acts'];
		die($json->encode($result));
	}
	if(!is_admin_order($order_id,erp_get_admin_id()))
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_order_no_access'];
		die($json->encode($result));
	}
	if($type_id==array_flip(Yoho\cms\Controller\ErpController::PO_ORDER_TYPE)[Yoho\cms\Controller\ErpController::DB_WAREHOUSING_STYLE_REPAIRANDSTOCK]){
		//delete

		if(delete_order_item($order_id) &&delete_order($order_id))
		{
			$result['error']=0;
			$result['message']=$_LANG['erp_order_delete_sucess'];
		}

		
		
		//redirect
		$result['error']='rdr';
		$result['message']="由供應商收回維修好的庫存, 請直接使用入貨單"."\n"."將會自動轉至頁面";
		$result['url']="erp_warehousing_manage.php?act=add_rma_from_po";
		die($json->encode($result));
	}
	if(lock_order($order_id,'edit'))
	{
		if(in_array($type_id,array_keys(Yoho\cms\Controller\ErpController::PO_ORDER_TYPE)))
		{
			$sql="update ".$ecs->table('erp_order')." set type='".$type_id."' where order_id='".$order_id."'";
			$db->query($sql);
			$action_remark = "編輯訂單 $order_info[order_sn] 類別 | 設定為 " . Yoho\cms\Controller\ErpController::PO_ORDER_TYPE[$type_id];
			$sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
					"VALUES(" . $order_id . ", " . ERP_UPDATE . "," . $_SESSION['admin_id'] . ",'" . mysql_escape_string($action_remark) . "')";
			$GLOBALS['db']->query($sql);
			delete_client_details($order_id);
			$result['error']=0;
			die($json->encode($result));
		}
		else
		{
			$result['error']=4;
			$result['message']=$_LANG['erp_order_type_not_exist'];
			die($json->encode($result));
		}
	}
	else
	{
		$result['error']=-1;
		$result['message']=$_LANG['erp_order_no_accessibility'];
		die($json->encode($result));
	}
}
elseif ($_REQUEST['act'] == 'change_warehouse')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_order_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$order_id=isset($_REQUEST['order_id']) && intval($_REQUEST['order_id']) ? intval($_REQUEST['order_id']):0;
	$warehouse_id=isset($_REQUEST['warehouse_id']) ? trim($_REQUEST['warehouse_id']): '';
	if(empty($order_id) || empty($warehouse_id))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$order_info=is_order_exist($order_id);
	if(!$order_info)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if($agency_id>0 &&$order_info['agency_id']!=$agency_id)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_no_agency_access'];
		die($json->encode($result));
	}
	if($order_info['order_status']!=1)
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_order_no_acts'];
		die($json->encode($result));
	}
	if(!is_admin_order($order_id,erp_get_admin_id()))
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_order_no_access'];
		die($json->encode($result));
	}
	if(lock_order($order_id,'edit'))
	{
		$sql="update ".$ecs->table('erp_order')." set warehouse_id='".$warehouse_id."' where order_id='".$order_id."'";
		$db->query($sql);
	
		$erpController =  new Yoho\cms\Controller\ErpController();
		if (empty($order_info['warehouse_id'])) {
			$warehouse_info_old['name'] = '';
		} else {
			$warehouse_info_old = $erpController->getWarehouseById($order_info['warehouse_id']);
		}
		
		$warehouse_info_new =  $erpController->getWarehouseById($warehouse_id);

		$action_remark = "編輯訂單 $order_info[order_sn] 收貨貨倉 | " .
			(!empty($order_info['warehouse_id']) ? "由 " . $warehouse_info_old['name'] . " 轉 " .  $warehouse_info_new['name'] : $warehouse_info_new['name']);
		$sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
				"VALUES(" . $order_id . ", " . ERP_UPDATE . "," . $_SESSION['admin_id'] . ",'" . $action_remark . "')";
		$GLOBALS['db']->query($sql);
		$result['error']=0;
		die($json->encode($result));
	}
	else
	{
		$result['error']=-1;
		$result['message']=$_LANG['erp_order_no_accessibility'];
		die($json->encode($result));
	}
}
elseif ($_REQUEST['act'] == 'change_operator')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_order_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$order_id=isset($_REQUEST['order_id']) && intval($_REQUEST['order_id']) ? intval($_REQUEST['order_id']):0;
	$operator=isset($_REQUEST['operator']) ? trim($_REQUEST['operator']): '';
	if(empty($order_id) || empty($operator))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$order_info=is_order_exist($order_id);
	if(!$order_info)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if($agency_id>0 &&$order_info['agency_id']!=$agency_id)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_no_agency_access'];
		die($json->encode($result));
	}
	if($order_info['order_status']!=1)
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_order_no_acts'];
		die($json->encode($result));
	}
	if(!is_admin_order($order_id,erp_get_admin_id()))
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_order_no_access'];
		die($json->encode($result));
	}
	if(lock_order($order_id,'edit'))
	{
		$sql="update ".$ecs->table('erp_order')." set operator='".$operator."' where order_id='".$order_id."'";
		$db->query($sql);
		$action_remark = "編輯訂單 $order_info[order_sn] 操作員 | " .
			(!empty($order_info['operator']) ? "由 " . $order_info['operator'] . " 至 " . $operator : "新增 " . $operator);
		$sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
				"VALUES(" . $order_id . ", " . ERP_UPDATE . "," . $_SESSION['admin_id'] . ",'" . mysql_escape_string($action_remark) . "')";
		$GLOBALS['db']->query($sql);
		$result['error']=0;
		die($json->encode($result));
	}
	else
	{
		$result['error']=-1;
		$result['message']=$_LANG['erp_order_no_accessibility'];
		die($json->encode($result));
	}
}
elseif ($_REQUEST['act'] == 'change_currency')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_order_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$order_id=isset($_REQUEST['order_id']) &&intval($_REQUEST['order_id']) ?intval($_REQUEST['order_id']):0;
	$currency_code=isset($_REQUEST['currency_code']) ?trim($_REQUEST['currency_code']): '';
	if(empty($order_id) ||empty($currency_code))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$order_info=is_order_exist($order_id);
	if(!$order_info)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if($agency_id>0 &&$order_info['agency_id']!=$agency_id)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_no_agency_access'];
		die($json->encode($result));
	}
	if($order_info['order_status']!=1)
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_order_no_acts'];
		die($json->encode($result));
	}
	if(!is_admin_order($order_id,erp_get_admin_id()))
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_order_no_access'];
		die($json->encode($result));
	}
	if(lock_order($order_id,'edit'))
	{
		$accountingController = new Yoho\cms\Controller\AccountingController();
		if(!empty($accountingController->getCurrencies(['currency_code' => $currency_code])))
		{
			$sql="update ".$ecs->table('erp_order')." set currency_code='".$currency_code."' where order_id='".$order_id."'";
			$db->query($sql);
			$action_remark = "編輯訂單 $order_info[order_sn] 貨幣 | " .
				(!empty($order_info['supplier_id']) ? ("由 " . $order_info['currency_code'] . " 至 " . $currency_code) : ("設定為 " . $currency_code));
			$sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
					"VALUES(" . $order_id . ", " . ERP_UPDATE . "," . $_SESSION['admin_id'] . ",'" . mysql_escape_string($action_remark) . "')";
			$GLOBALS['db']->query($sql);
			$result['error']=0;
			die($json->encode($result));
		}
		else
		{
			$result['error']=4;
			$result['message']='Currency code not exist';
			die($json->encode($result));
		}
	}
	else
	{
		$result['error']=-1;
		$result['message']=$_LANG['erp_order_no_accessibility'];
		die($json->encode($result));
	}
}
elseif ($_REQUEST['act'] == 'get_goods_attr')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_order_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$order_id=isset($_REQUEST['order_id']) &&intval($_REQUEST['order_id'])>0 ?intval($_REQUEST['order_id']):0;
	$order_item_id=isset($_REQUEST['order_item_id']) &&intval($_REQUEST['order_item_id'])>0 ?intval($_REQUEST['order_item_id']):0;
	if(empty($order_id) ||empty($order_item_id))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$order_info=is_order_exist($order_id);
	if(!$order_info)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_not_exist'];
		die($json->encode($result));
	}
	if($order_info['order_status']!=1)
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_order_no_acts'];
		die($json->encode($result));
	}
	if(!is_admin_order($order_id,erp_get_admin_id()))
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_order_no_access'];
		die($json->encode($result));
	}
	if(!is_order_item_exist($order_item_id))
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_order_item_not_exist'];
		die($json->encode($result));
	}
	if(lock_order($order_id,'edit'))
	{
		$sql="select goods_id from ".$GLOBALS['ecs']->table('erp_order_item')." where order_item_id='".$order_item_id."'";
		$goods_id=$GLOBALS['db']->getOne($sql);
		if(empty($goods_id))
		{
			$result['error']=5;
			$result['message']=$_LANG['erp_order_select_goods_in_advance'];
			die($json->encode($result));
		}
		else
		{
			$sql="select attr_id from ".$GLOBALS['ecs']->table('erp_order_item')." where order_item_id='".$order_item_id."'";
			$attr_id=$GLOBALS['db']->getOne($sql);
			$goods_attr=goods_attr($goods_id,$attr_id);
			if(empty($goods_attr))
			{
				$result['error']=5;
				$result['message']=$_LANG['erp_order_goods_without_attr'];
				die($json->encode($result));
			}
			else
			{
				$smarty->assign('goods_attr',$goods_attr);
				$attr_info=$smarty->fetch('erp_goods_attr.htm');
				$result['error']=0;
				$result['attr_info']=$attr_info;
				$result['attr_num']=count($goods_attr);
				die($json->encode($result));
			}
		}
	}
	else
	{
		$result['error']=-1;
		$result['message']=$_LANG['erp_order_no_accessibility'];
		die($json->encode($result));
	}
}
elseif($_REQUEST['act'] == 'withdrawal_to_edit')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_order_rate','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$order_id=isset($_REQUEST['order_id']) &&intval($_REQUEST['order_id'])>0?intval($_REQUEST['order_id']):0;
	if(empty($order_id))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$order_info=is_order_exist($order_id);
	if(!$order_info)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if($agency_id>0 &&$order_info['agency_id']!=$agency_id)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_no_agency_access'];
		die($json->encode($result));
	}
	if($order_info['order_status']!=2)
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_order_no_acts'];
		die($json->encode($result));
	}
	$sql="update ".$GLOBALS['ecs']->table('erp_order')."set order_status='1' where order_id='".$order_id."'";
	$GLOBALS['db']->query($sql);
	$action_remark = "編輯訂單 $order_info[order_sn] 狀態 | 由 " . ORDER_STATUS[$order_info['order_status']] . " 至 " . ORDER_STATUS[1];
	$sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
			"VALUES(" . $order_id . ", " . ERP_UPDATE . "," . $_SESSION['admin_id'] . ",'" . $action_remark . "')";
	$GLOBALS['db']->query($sql);
	admin_log($order_info['order_sn'],'withdraw_edit','erp_order');
	$result['error']=0;
	die($json->encode($result));
}
elseif($_REQUEST['act'] == 'withdrawal_to_rate')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_order_approve','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$order_id=isset($_REQUEST['order_id']) &&intval($_REQUEST['order_id'])>0?intval($_REQUEST['order_id']):0;
	if(empty($order_id))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$order_info=is_order_exist($order_id);
	if(!$order_info)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if($agency_id>0 &&$order_info['agency_id']!=$agency_id)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_no_agency_access'];
		die($json->encode($result));
	}
	if($order_info['order_status']!=3)
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_order_no_acts'];
		die($json->encode($result));
	}
	$sql="update ".$GLOBALS['ecs']->table('erp_order')."set order_status='2' where order_id='".$order_id."'";
	$GLOBALS['db']->query($sql);
	$action_remark = "編輯訂單 $order_info[order_sn] 狀態 | 由 " . ORDER_STATUS[$order_info['order_status']] . " 至 " . ORDER_STATUS[2];
	$sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
			"VALUES(" . $order_id . ", " . ERP_UPDATE . "," . $_SESSION['admin_id'] . ",'" . $action_remark . "')";
	$GLOBALS['db']->query($sql);
	admin_log($order_info['order_sn'],'withdraw_rate','erp_order');
	$result['error']=0;
	die($json->encode($result));
}
elseif ($_REQUEST['act'] == 'set_goods_attr')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_order_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$order_id=isset($_REQUEST['order_id']) &&intval($_REQUEST['order_id'])>0?intval($_REQUEST['order_id']):0;
	$order_item_id=isset($_REQUEST['order_item_id']) &&intval($_REQUEST['order_item_id'])>0?intval($_REQUEST['order_item_id']):0;
	$ids=isset($_REQUEST['ids'])?trim($_REQUEST['ids']):'';
	if(empty($order_id) ||empty($order_item_id))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$order_info=is_order_exist($order_id);
	if(!$order_info)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if($agency_id>0 &&$order_info['agency_id']!=$agency_id)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_no_agency_access'];
		die($json->encode($result));
	}
	if($order_info['order_status']!=1)
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_order_no_acts'];
		die($json->encode($result));
	}
	if(!is_admin_order($order_id,erp_get_admin_id()))
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_order_no_access'];
		die($json->encode($result));
	}
	if(!is_order_item_exist($order_item_id))
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_order_item_not_exist'];
		die($json->encode($result));
	}
	if(lock_order($order_id,'edit'))
	{
		$sql="select goods_id from ".$GLOBALS['ecs']->table('erp_order_item')." where order_item_id='".$order_item_id."'";
		$goods_id=$GLOBALS['db']->getOne($sql);
		$attr_id=get_attr_id($goods_id,$ids);
		$sql="update ".$GLOBALS['ecs']->table('erp_order_item')."set attr_id='".$attr_id."' where order_item_id='".$order_item_id."'";
		$GLOBALS['db']->query($sql);
		$action_remark = "編輯訂單 $order_info[order_sn] 屬性 | 由 " . $order_info['attr_id'] . " 至 " . $attr_id;
		$sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
				"VALUES(" . $order_id . ", " . ERP_UPDATE . "," . $_SESSION['admin_id'] . ",'" . mysql_escape_string($action_remark) . "')";
		$GLOBALS['db']->query($sql);
		$sql="select * from ".$GLOBALS['ecs']->table('erp_order_item')." where order_id='".$order_id."' and goods_id='".$goods_id."' and attr_id='".$attr_id."'";
		$order_item_info=$GLOBALS['db']->getAll($sql);
		if(!empty($order_item_info) &&count($order_item_info)>=2)
		{
			$item_info = get_order_item_info("",$order_item_id);
			$order_qty=0;
			foreach($order_item_info as $key =>$order_item)
			{
				$order_qty+=$order_item['order_qty'];
			}
			$sql="update ".$GLOBALS['ecs']->table('erp_order_item')." set order_qty='".$order_qty."' where order_item_id='".$order_item_id."'";
			$GLOBALS['db']->query($sql);
			$action_remark = "編輯訂單 $order_info[order_sn] 產品 | 調整 " . $item_info['goods_name'] . " 數量由 " . $item_info['order_qty'] . " 至 " . $order_qty;
			$sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
					"VALUES(" . $order_id . ", " . ERP_UPDATE . "," . $_SESSION['admin_id'] . ",'" . $action_remark . "')";
			$GLOBALS['db']->query($sql);
			$sql = "SELECT order_item_id, goods_id FROM " . $GLOBALS['ecs']->table('erp_order_item') .
					" WHERE order_id='".$order_id."' and attr_id='".$attr_id."' and order_item_id<>'".$order_item_id."'";
			$ori_info = $GLOBALS['db']->getAll($sql);
			$sql="delete from ".$GLOBALS['ecs']->table('erp_order_item')." where order_id='".$order_id."' and attr_id='".$attr_id."' and order_item_id<>'".$order_item_id."'";
			$GLOBALS['db']->query($sql);
			foreach($ori_info as $row){
				$action_remark = "編輯訂單 $order_info[order_sn] 產品 | 移除產品 " . get_order_item_info("",$row['order_item_id'])['goods_name'];
				$sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
						"VALUES(" . $order_id . ", " . ERP_UPDATE . "," . $_SESSION['admin_id'] . ",'" . mysql_escape_string($action_remark) . "')";
				$GLOBALS['db']->query($sql);
			}
		}
		$result['error']=0;
		die($json->encode($result));
	}
	else
	{
		$result['error']=-1;
		$result['message']=$_LANG['erp_order_no_accessibility'];
		die($json->encode($result));
	}
}
/* elseif ($_REQUEST['act'] == 'change_goods_sn')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_order_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$order_id=isset($_REQUEST['order_id']) &&intval($_REQUEST['order_id'])>0?intval($_REQUEST['order_id']):0;
	$order_item_id=isset($_REQUEST['order_item_id']) &&intval($_REQUEST['order_item_id'])>0?intval($_REQUEST['order_item_id']):0;
	$goods_sn=isset($_REQUEST['goods_sn'])?trim($_REQUEST['goods_sn']):'';
	if(empty($order_id) ||empty($order_item_id))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$order_info=is_order_exist($order_id);
	if(!$order_info)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if($agency_id>0 &&$order_info['agency_id']!=$agency_id)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_no_agency_access'];
		die($json->encode($result));
	}
	if($order_info['order_status']!=1)
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_order_no_acts'];
		die($json->encode($result));
	}
	if(!is_admin_order($order_id,erp_get_admin_id()))
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_order_no_access'];
		die($json->encode($result));
	}
	if(!is_order_item_exist($order_item_id))
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_order_item_not_exist'];
		die($json->encode($result));
	}
	if(!is_goods_exist('',$goods_sn))
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_order_goods_not_exist'];
		die($json->encode($result));
	}
	if(!is_admin_goods(erp_get_admin_id(),'',$goods_sn))
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_order_not_admin_goods'];
		die($json->encode($result));
	}
	$sql="select supplier_id from ".$GLOBALS['ecs']->table('erp_order')." where order_id='".$order_id."'";
	$supplier_id=$GLOBALS['db']->getOne($sql);
	if(!is_supplier_goods($supplier_id,'',$goods_sn))
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_order_not_supplier_goods'];
		die($json->encode($result));
	}
	if(lock_order($order_id,'edit'))
	{
		$sql="select goods_id from ".$GLOBALS['ecs']->table('goods')." where goods_sn='".$goods_sn."'";
		$goods_id=$GLOBALS['db']->getOne($sql);
		$attr_id=get_attr_id($goods_id);
		$sql="update ".$GLOBALS['ecs']->table('erp_order_item')." set goods_id='".$goods_id."' ";
		$sql.=", attr_id='".$attr_id."'";
		$sql.=" where order_item_id='".$order_item_id."'";
		$GLOBALS['db']->query($sql);
		$result['error']=0;
		die($json->encode($result));
	}
	else
	{
		$result['error']=-1;
		$result['message']=$_LANG['erp_order_no_accessibility'];
		die($json->encode($result));
	}
}*/
elseif ($_REQUEST['act'] == 'delete_order')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_order_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$order_id=isset($_REQUEST['order_id']) &&intval($_REQUEST['order_id'])>0?intval($_REQUEST['order_id']):0;
	if(empty($order_id))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$order_info=is_order_exist($order_id);
	if(!$order_info)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if($agency_id>0 &&$order_info['agency_id']!=$agency_id)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_no_agency_access'];
		die($json->encode($result));
	}
	if($order_info['order_status']!=1)
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_order_no_acts'];
		die($json->encode($result));
	}
	if(!is_admin_order($order_id,erp_get_admin_id()))
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_order_no_access'];
		die($json->encode($result));
	}
	if(lock_order($order_id,'delete'))
	{
		if(delete_order_item($order_id) &&delete_order($order_id))
		{
			$result['error']=0;
			$result['message']=$_LANG['erp_order_delete_sucess'];
			die($json->encode($result));
		}
	}
	else
	{
		$result['error']=-1;
		$result['message']=$_LANG['erp_order_no_accessibility'];
		die($json->encode($result));
	}
}
elseif ($_REQUEST['act'] == 'to_confirm')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_order_approve','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$order_id=isset($_REQUEST['order_id']) &&intval($_REQUEST['order_id'])>0?intval($_REQUEST['order_id']):0;
	$approve_remark=isset($_REQUEST['approve_remark'])?trim($_REQUEST['approve_remark']):'';

	if(empty($order_id))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$order_info=is_order_exist($order_id);
	if(!$order_info)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if($agency_id>0 &&$order_info['agency_id']!=$agency_id)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_no_agency_access'];
		die($json->encode($result));
	}
	if($order_info['order_status']>3)
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_order_no_acts'];
		die($json->encode($result));
	}
	if(!is_admin_order($order_id,erp_get_admin_id()))
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_order_no_access'];
		die($json->encode($result));
	}
	if(empty($order_info['operator']))
	{
		$result['error']=1;
		$result['message']='提交財務前請先填寫操作員。';
		die($json->encode($result));
	}
	$order_integrity=check_order_integrity($order_id);
	if($order_integrity!==true)
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_order_not_completed'];
		die($json->encode($result));
	}
	if(lock_order($order_id,'to_confirm'))
	{
		if(order_act_record($order_id,'confirm'))
		{
			$rate = $GLOBALS['db']->getOne("SELECT currency_rate FROM actg_currencies WHERE currency_code = '$order_info[currency_code]'");
			$GLOBALS['db']->query("UPDATE " . $GLOBALS['ecs']->table('erp_order_item') . "SET price = original_price * $rate, amount = original_amount * $rate, shipping_price = original_shipping_price * $rate WHERE order_id = $order_id");
			
			$action_remark = "確認提交訂單 " . $order_info['order_sn'];
			$sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
					"VALUES(" . $order_id . ", " . ERP_APPROVE . "," . $_SESSION['admin_id'] . ",'" . $action_remark . "')";
			$GLOBALS['db']->query($sql);
			admin_log($order_info['order_sn'],'approve','erp_order');
			$action_remark = "編輯訂單 $order_info[order_sn] 狀態 | 由 " . ORDER_STATUS[$order_info['order_status']] . " 至 ";

			$accountingController = new Yoho\cms\Controller\AccountingController();
			$erpController = new Yoho\cms\Controller\ErpController();
			$erp_order_info = $erpController->get_erp_order_info($order_id);
			$param = [
				'supplier_id'		=> $erp_order_info['supplier_id'],
				'currency_code'		=> $erp_order_info['currency_code'],
				'order_amount'		=> $erp_order_info['order_amount'], // exchanged to HKD
				'shipping_amount'	=> $erp_order_info['shipping_amount'], // exchanged to HKD
				'erp_order_id'		=> $order_id,
				'created_by'		=> $_SESSION['admin_id'],
				'remark'			=> "採購訂單 $order_info[order_sn] 審核",
			];
			$actg_txn_id = $accountingController->insertPurchaseTransactions($param);
			if (!$actg_txn_id) {
				$result['error']=4;
				$result['message']='無法新增會計紀錄';
				die($json->encode($result));
			}

			$sql="update ".$GLOBALS['ecs']->table('erp_order')." set ";
			$sql.="order_status ='4', approve_remark='".mysql_escape_string($approve_remark)."', actg_txn_id = '$actg_txn_id' ";
			$sql.="where order_id='".$order_id."'";
			$action_remark .= ORDER_STATUS[4];

			if($GLOBALS['db']->query($sql))
			{
				$sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
						"VALUES(" . $order_id . ", " . ERP_UPDATE . "," . $_SESSION['admin_id'] . ",'" . mysql_escape_string($action_remark) . "')";
				$GLOBALS['db']->query($sql);
				$result['error']=0;
				$result['message']=$_LANG['erp_order_approve_success'];
				die($json->encode($result));
			}

			/*
			if(post_order($order_id))
			{
				$result['error']=0;
				$result['message']=$_LANG['erp_order_post_success'];
				die($json->encode($result));
			}
			else
			{
				$result['error']=3;
				$result['message']=$_LANG['erp_order_post_failed'];
				die($json->encode($result));
			}
			*/
		}
	}
	else
	{
		$result['error']=-1;
		$result['message']=$_LANG['erp_order_no_accessibility'];
		die($json->encode($result));
	}
}
elseif ($_REQUEST['act'] == 'post_to_rate')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_order_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$order_id=isset($_REQUEST['order_id']) &&intval($_REQUEST['order_id'])>0?intval($_REQUEST['order_id']):0;
	if(empty($order_id))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$order_info=is_order_exist($order_id);
	if(!$order_info)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if($agency_id>0 &&$order_info['agency_id']!=$agency_id)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_no_agency_access'];
		die($json->encode($result));
	}
	if($order_info['order_status']!=1)
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_order_no_acts'];
		die($json->encode($result));
	}
	if(!is_admin_order($order_id,erp_get_admin_id()))
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_order_no_access'];
		die($json->encode($result));
	}
	if(empty($order_info['operator']))
	{
		$result['error']=1;
		$result['message']='提交財務前請先填寫操作員。';
		die($json->encode($result));
	}
	$order_integrity=check_order_integrity($order_id);
	if($order_integrity!==true)
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_order_not_completed'];
		die($json->encode($result));
	}
	if(lock_order($order_id,'post_to_rate'))
	{
		if(order_act_record($order_id,'post'))
		{
			if(post_order($order_id))
			{
				$result['error']=0;
				$result['message']=$_LANG['erp_order_post_success'];
				die($json->encode($result));
			}
			else
			{
				$result['error']=3;
				$result['message']=$_LANG['erp_order_post_failed'];
				die($json->encode($result));
			}
		}
	}
	else
	{
		$result['error']=-1;
		$result['message']=$_LANG['erp_order_no_accessibility'];
		die($json->encode($result));
	}
}
elseif ($_REQUEST['act'] == 'change_order_qty')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_order_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$order_id=isset($_REQUEST['order_id']) &&intval($_REQUEST['order_id'])>0?intval($_REQUEST['order_id']):0;
	$order_item_id=isset($_REQUEST['order_item_id']) &&intval($_REQUEST['order_item_id'])>0?intval($_REQUEST['order_item_id']):0;
	$order_qty=isset($_REQUEST['order_qty']) &&intval($_REQUEST['order_qty'])>0?intval($_REQUEST['order_qty']):0;
	if(empty($order_id) ||empty($order_item_id))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$order_info=is_order_exist($order_id);
	if(!$order_info)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if($agency_id>0 &&$order_info['agency_id']!=$agency_id)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_no_agency_access'];
		die($json->encode($result));
	}
	if($order_info['order_status']!=1)
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_order_no_acts'];
		die($json->encode($result));
	}
	if(!is_admin_order($order_id,erp_get_admin_id()))
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_order_no_access'];
		die($json->encode($result));
	}
	if(!is_order_item_exist($order_item_id))
	{
		$result['error']=4;
		$result['message']=$_LANG['erp_order_item_not_exist'];
		die($json->encode($result));
	}
	if(lock_order($order_id,'edit'))
	{		
		// $sql="select goods_id from ".$GLOBALS['ecs']->table('erp_order_item')." where order_id='".$order_id."' and order_item_id='".$order_item_id."'";
		// $order_item_info=$db->getRow($sql);
		// 
		// $sql="select shipping_price from ".$GLOBALS['ecs']->table('goods')." where goods_id='".$order_item_info["goods_id"]."'";
		// $good_info=$db->getRow($sql);
		
		//$sql="update ".$GLOBALS['ecs']->table('erp_order_item')." set order_qty='".$order_qty."',shipping_price='".($good_info["shipping_price"]*$order_qty)."' where order_item_id='".$order_item_id."'";
		$item_info = get_order_item_info("",$order_item_id);
		$sql="update ".$GLOBALS['ecs']->table('erp_order_item')." set order_qty='".$order_qty."' where order_item_id='".$order_item_id."'";

		$GLOBALS['db']->query($sql);
		$action_remark = "編輯訂單 $order_info[order_sn] 產品 | 調整 " . $item_info['goods_name'] . " 數量由 " . $item_info['order_qty'] . " 至 " . $order_qty;
		$sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
				"VALUES(" . $order_id . ", " . ERP_UPDATE . "," . $_SESSION['admin_id'] . ",'" . mysql_escape_string($action_remark) . "')";
		$GLOBALS['db']->query($sql);
		$result['error']=0;
		$result['message']='';
		die($json->encode($result));
	}
	else
	{
		$result['error']=-1;
		$result['message']=$_LANG['erp_order_no_accessibility'];
		die($json->encode($result));
	}
}
elseif ($_REQUEST['act'] == 'delete_order_item')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_order_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$order_id=isset($_REQUEST['order_id']) &&intval($_REQUEST['order_id'])>0?intval($_REQUEST['order_id']):0;
	$order_item_id=isset($_REQUEST['order_item_id']) &&intval($_REQUEST['order_item_id'])>0?intval($_REQUEST['order_item_id']):0;
	if(empty($order_id) ||empty($order_item_id))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$order_info=is_order_exist($order_id);
	if(!$order_info)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if($agency_id>0 &&$order_info['agency_id']!=$agency_id)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_no_agency_access'];
		die($json->encode($result));
	}
	if($order_info['order_status']!=1)
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_order_no_acts'];
		die($json->encode($result));
	}
	if(!is_admin_order($order_id,erp_get_admin_id()))
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_order_no_access'];
		die($json->encode($result));
	}
	if(!is_order_item_exist($order_item_id))
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_order_item_not_exist'];
		die($json->encode($result));
	}
	if(lock_order($order_id,'delete'))
	{
		delete_order_item('',$order_item_id);
		$result['error']=0;
		die($json->encode($result));
	}
	else
	{
		$result['error']=-1;
		$result['message']=$_LANG['erp_order_no_accessibility'];
		die($json->encode($result));
	}
}
elseif ($_REQUEST['act'] == 'update_order')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_order_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$order_id=isset($_REQUEST['order_id']) &&intval($_REQUEST['order_id'])>0?intval($_REQUEST['order_id']):0;
	$goods_sn=isset($_REQUEST['goods_sn'])?trim($_REQUEST['goods_sn']):'';
	$goods_name=isset($_REQUEST['goods_name'])?trim($_REQUEST['goods_name']):'';
	$ids=isset($_REQUEST['ids'])?trim($_REQUEST['ids']):'';
	$goods_qty=isset($_REQUEST['goods_qty']) &&intval($_REQUEST['goods_qty'])>0?intval($_REQUEST['goods_qty']):1;
	if(empty($order_id))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	if(empty($goods_sn) &&empty($goods_name))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_order_goods_sn_or_goods_name_required'];
		die($json->encode($result));
	}
	$order_info=is_order_exist($order_id);
	if(!$order_info)
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_order_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if($agency_id>0 &&$order_info['agency_id']!=$agency_id)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_no_agency_access'];
		die($json->encode($result));
	}
	if($order_info['order_status']!=1)
	{
		$result['error']=6;
		$result['message']=$_LANG['erp_order_no_acts'];
		die($json->encode($result));
	}
	if(!is_admin_order($order_id ,erp_get_admin_id()))
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_order_no_access'];
		die($json->encode($result));
	}
	if(!empty($goods_sn))
	{
		$goods_id=check_goods_sn($goods_sn);
		if(empty($goods_id))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_order_wrong_goods_sn'];
			die($json->encode($result));
		}
	}
	else
	{
		$goods_id=check_goods_name($goods_name);
		if(empty($goods_id))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_order_wrong_goods_name'];
			die($json->encode($result));
		}
	}
	$supplier_id=$order_info['supplier_id'];
	if(!is_supplier_goods($supplier_id,$goods_id))
	{
		$result['error']=6;
		$result['message']=$_LANG['erp_order_not_supplier_goods'];
		die($json->encode($result));
	}
	if(empty($ids))
	{
		
		// $sql="select shipping_price from ".$ecs->table('goods')." where goods_id='".$goods_id."'";
		// $good_info=$db->getRow($sql);
		
		$sql="select order_item_id,order_qty from ".$ecs->table('erp_order_item')." where order_id='".$order_id."' and goods_id='".$goods_id."'";
		$order_item_info=$db->getRow($sql);
		if(empty($order_item_info))
		{
			$attr_id=get_attr_id($goods_id);
			//$sql="insert into ".$ecs->table('erp_order_item')." set order_id='".$order_id."',goods_id='".$goods_id."',attr_id='".$attr_id."',order_qty='".$goods_qty."',shipping_price='".($goods_qty*$good_info["shipping_price"])."'";
			$sql="insert into ".$ecs->table('erp_order_item')." set order_id='".$order_id."',goods_id='".$goods_id."',attr_id='".$attr_id."',order_qty='".$goods_qty."'";
			$db->query($sql);
			$item_info = get_order_item_info("",$db->insert_id());
			$action_remark = "編輯訂單 $order_info[order_sn] 產品 | 新增 " . $item_info['goods_name'] . " 數量 " . $goods_qty;
			$sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
					"VALUES(" . $order_id . ", " . ERP_UPDATE . "," . $_SESSION['admin_id'] . ",'" . mysql_escape_string($action_remark) . "')";
			$GLOBALS['db']->query($sql);
			$result['error']=0;
			die($json->encode($result));
		}
		else
		{
			//$sql="update ".$ecs->table('erp_order_item')." set order_qty=order_qty+'".$goods_qty."',shipping_price='".(($goods_qty+$order_item_info["order_qty"])*$good_info["shipping_price"])."' where order_item_id ='".$order_item_info['order_item_id']."'";
			$item_info = get_order_item_info("",$order_item_info['order_item_id']);
			$sql="update ".$ecs->table('erp_order_item')." set order_qty=order_qty+'".$goods_qty."' where order_item_id ='".$order_item_info['order_item_id']."'";
			$db->query($sql);
			$action_remark = "編輯訂單 $order_info[order_sn] 產品 | 調整 " . $item_info['goods_name'] . " 數量由 " . $item_info['order_qty'] . " 至 " . (intval($item_info['order_qty']) + $goods_qty);
			$sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
					"VALUES(" . $order_id . ", " . ERP_UPDATE . "," . $_SESSION['admin_id'] . ",'" . mysql_escape_string($action_remark) . "')";
			$GLOBALS['db']->query($sql);
			$result['error']=0;
			die($json->encode($result));
		}
	}
	else
	{
		// $sql="select shipping_price from ".$ecs->table('goods')." where goods_id='".$goods_id."'";
		// $good_info=$db->getRow($sql);
		
		$ids=trim($ids,',');
		$attr_id=get_attr_id($goods_id,$ids);
		$sql="select order_item_id,order_qty from ".$ecs->table('erp_order_item')." where order_id='".$order_id."' and goods_id='".$goods_id."' and attr_id='".$attr_id."'";
		$order_item_info=$db->getRow($sql);
		if(empty($order_item_info))
		{
			//$sql="insert into ".$ecs->table('erp_order_item')." set order_id='".$order_id."',goods_id='".$goods_id."',attr_id='".$attr_id."',order_qty='".$goods_qty."',shipping_price='".($goods_qty*$good_info["shipping_price"])."'";
			$sql="insert into ".$ecs->table('erp_order_item')." set order_id='".$order_id."',goods_id='".$goods_id."',attr_id='".$attr_id."',order_qty='".$goods_qty."'";
			$db->query($sql);
			$item_info = get_order_item_info("",$db->insert_id());
			$action_remark = "編輯訂單 $order_info[order_sn] 產品 | 新增 " . $item_info['goods_name'] . " 數量 " . $goods_qty;
			$sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
					"VALUES(" . $order_id . ", " . ERP_UPDATE . "," . $_SESSION['admin_id'] . ",'" . mysql_escape_string($action_remark) . "')";
			$GLOBALS['db']->query($sql);
			$result['error']=0;
			die($json->encode($result));
		}
		else
		{
			//$sql="update ".$ecs->table('erp_order_item')." set order_qty=order_qty+'".$goods_qty."',shipping_price='".(($goods_qty+$order_item_info["order_qty"])*$good_info["shipping_price"])."' where order_item_id='".$order_item_info['order_item_id']."'";
			$item_info = get_order_item_info("",$order_item_info['order_item_id']);
			$sql="update ".$ecs->table('erp_order_item')." set order_qty=order_qty+'".$goods_qty."' where order_item_id='".$order_item_info['order_item_id']."'";
			$db->query($sql);
			$action_remark = "編輯訂單 $order_info[order_sn] 產品 | 調整 " . $item_info['goods_name'] . " 數量由 " . $item_info['order_qty'] . " 至 " . (intval($item_info['order_qty']) + $goods_qty);
			$sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
					"VALUES(" . $order_id . ", " . ERP_UPDATE . "," . $_SESSION['admin_id'] . ",'" . mysql_escape_string($action_remark) . "')";
			$GLOBALS['db']->query($sql);
			$result['error']=0;
			die($json->encode($result));
		}
	}
}
elseif ($_REQUEST['act'] == 'fill_delievery_info')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_order_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$order_id=isset($_REQUEST['order_id']) &&intval($_REQUEST['order_id']) ?intval($_REQUEST['order_id']) : 0;
	$order_sn=isset($_REQUEST['order_sn'])?trim($_REQUEST['order_sn']):'';
	if(empty($order_sn))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$order_info=is_order_exist($order_id);
	$client_order_info=is_client_order_exist('',$order_sn);

	if(!$order_info||!$client_order_info)
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_order_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if($agency_id>0 &&$order_info['agency_id']!=$agency_id)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_no_agency_access'];
		die($json->encode($result));
	}
	if($order_info['order_status']!=1)
	{
		$result['error']=6;
		$result['message']=$_LANG['erp_order_no_acts'];
		die($json->encode($result));
	}
	if(!is_admin_order($order_id ,erp_get_admin_id()))
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_order_no_access'];
		die($json->encode($result));
	}
	if(lock_order($order_id,'edit'))
	{	
		$result['error']=0;
		$client_order_info = addslashes_deep($client_order_info);
		$sql="update ".$ecs->table('erp_order')." set client_order_sn='".$client_order_info["order_sn"]."', client_name='".$client_order_info["consignee"]."', client_tel='".$client_order_info["tel"]."', client_mobile='".$client_order_info["mobile"]."', client_address='".$client_order_info["address"]."' where order_id='".$order_id."'";
		$db->query($sql);
		$action_remark = "編輯訂單 $order_info[order_sn] 代理送貨資料";
		$sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
				"VALUES(" . $order_id . ", " . ERP_UPDATE . "," . $_SESSION['admin_id'] . ",'" . $action_remark . "')";
		$GLOBALS['db']->query($sql);
		die($json->encode($result));
	}
	else
	{
		$result['error']=-1;
		$result['message']=$_LANG['erp_order_no_accessibility'];
		die($json->encode($result));
	}
}
elseif ($_REQUEST['act'] == 'change_client_name')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_order_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$order_id=isset($_REQUEST['order_id']) &&intval($_REQUEST['order_id']) ?intval($_REQUEST['order_id']) : 0;
	$client_name=isset($_REQUEST['client_name']) ?trim($_REQUEST['client_name']) : '';
	if(empty($order_id))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$order_info=is_order_exist($order_id);
	if(!$order_info)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if($agency_id>0 &&$order_info['agency_id']!=$agency_id)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_no_agency_access'];
		die($json->encode($result));
	}
	if($order_info['order_status']!=1)
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_order_no_acts'];
		die($json->encode($result));
	}
	if(!is_admin_order($order_id,erp_get_admin_id()))
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_order_no_access'];
		die($json->encode($result));
	}
	if(lock_order($order_id,'edit'))
	{
		$result['error']=0;
		$sql="update ".$ecs->table('erp_order')." set client_name='".$client_name."' where order_id='".$order_id."'";
		$db->query($sql);
		$action_remark = "編輯訂單 $order_info[order_sn] 代理送貨資料 | 姓名";
		$sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
				"VALUES(" . $order_id . ", " . ERP_UPDATE . "," . $_SESSION['admin_id'] . ",'" . $action_remark . "')";
		$GLOBALS['db']->query($sql);
		die($json->encode($result));
	}
	else
	{
		$result['error']=-1;
		$result['message']=$_LANG['erp_order_no_accessibility'];
		die($json->encode($result));
	}
}
elseif ($_REQUEST['act'] == 'change_client_tel')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_order_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$order_id=isset($_REQUEST['order_id']) &&intval($_REQUEST['order_id']) ?intval($_REQUEST['order_id']) : 0;
	$client_tel=isset($_REQUEST['client_tel']) ?trim($_REQUEST['client_tel']) : '';
	if(empty($order_id))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$order_info=is_order_exist($order_id);
	if(!$order_info)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if($agency_id>0 &&$order_info['agency_id']!=$agency_id)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_no_agency_access'];
		die($json->encode($result));
	}
	if($order_info['order_status']!=1)
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_order_no_acts'];
		die($json->encode($result));
	}
	if(!is_admin_order($order_id,erp_get_admin_id()))
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_order_no_access'];
		die($json->encode($result));
	}
	if(lock_order($order_id,'edit'))
	{
		$result['error']=0;
		$sql="update ".$ecs->table('erp_order')." set client_tel='".$client_tel."' where order_id='".$order_id."'";
		$db->query($sql);
		$action_remark = "編輯訂單 $order_info[order_sn] 代理送貨資料 | 電話";
		$sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
				"VALUES(" . $order_id . ", " . ERP_UPDATE . "," . $_SESSION['admin_id'] . ",'" . $action_remark . "')";
		$GLOBALS['db']->query($sql);
		die($json->encode($result));
	}
	else
	{
		$result['error']=-1;
		$result['message']=$_LANG['erp_order_no_accessibility'];
		die($json->encode($result));
	}
}
elseif ($_REQUEST['act'] == 'change_client_mobile')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_order_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$order_id=isset($_REQUEST['order_id']) &&intval($_REQUEST['order_id']) ?intval($_REQUEST['order_id']) : 0;
	$client_mobile=isset($_REQUEST['client_mobile']) ?trim($_REQUEST['client_mobile']) : '';
	if(empty($order_id))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$order_info=is_order_exist($order_id);
	if(!$order_info)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if($agency_id>0 &&$order_info['agency_id']!=$agency_id)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_no_agency_access'];
		die($json->encode($result));
	}
	if($order_info['order_status']!=1)
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_order_no_acts'];
		die($json->encode($result));
	}
	if(!is_admin_order($order_id,erp_get_admin_id()))
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_order_no_access'];
		die($json->encode($result));
	}
	if(lock_order($order_id,'edit'))
	{
		$result['error']=0;
		$sql="update ".$ecs->table('erp_order')." set client_mobile='".$client_mobile."' where order_id='".$order_id."'";
		$db->query($sql);
		$action_remark = "編輯訂單 $order_info[order_sn] 代理送貨資料 | 手提";
		$sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
				"VALUES(" . $order_id . ", " . ERP_UPDATE . "," . $_SESSION['admin_id'] . ",'" . $action_remark . "')";
		$GLOBALS['db']->query($sql);
		die($json->encode($result));
	}
	else
	{
		$result['error']=-1;
		$result['message']=$_LANG['erp_order_no_accessibility'];
		die($json->encode($result));
	}
}
elseif ($_REQUEST['act'] == 'change_client_address')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_order_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$order_id=isset($_REQUEST['order_id']) &&intval($_REQUEST['order_id']) ?intval($_REQUEST['order_id']) : 0;
	$client_address=isset($_REQUEST['client_address']) ?trim($_REQUEST['client_address']) : '';
	if(empty($order_id))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$order_info=is_order_exist($order_id);
	if(!$order_info)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if($agency_id>0 &&$order_info['agency_id']!=$agency_id)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_no_agency_access'];
		die($json->encode($result));
	}
	if($order_info['order_status']!=1)
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_order_no_acts'];
		die($json->encode($result));
	}
	if(!is_admin_order($order_id,erp_get_admin_id()))
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_order_no_access'];
		die($json->encode($result));
	}
	if(lock_order($order_id,'edit'))
	{
		$result['error']=0;
		$sql="update ".$ecs->table('erp_order')." set client_address='".$client_address."' where order_id='".$order_id."'";
		$db->query($sql);
		$action_remark = "編輯訂單 $order_info[order_sn] 代理送貨資料 | 地址";
		$sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
				"VALUES(" . $order_id . ", " . ERP_UPDATE . "," . $_SESSION['admin_id'] . ",'" . $action_remark . "')";
		$GLOBALS['db']->query($sql);
		die($json->encode($result));
	}
	else
	{
		$result['error']=-1;
		$result['message']=$_LANG['erp_order_no_accessibility'];
		die($json->encode($result));
	}
}
elseif ($_REQUEST['act'] == 'check_goods_sn'||$_REQUEST['act'] == 'check_goods_name')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if($_REQUEST['act'] == 'check_goods_sn')
	{
		$goods_sn=trim($_REQUEST['goods_sn']);
		$goods_id=check_goods_sn($goods_sn);
		if(empty($goods_id))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_order_wrong_goods_sn'];
			die($json->encode($result));
		}
	}
	else
	{
		$goods_name=trim($_REQUEST['goods_name']);
		$goods_id=check_goods_name($goods_name);
		if(empty($goods_id))
		{
			$result['error']=1;
			$result['message']=$_LANG['erp_order_wrong_goods_name'];
			die($json->encode($result));
		}
	}
	$goods_attr=goods_attr($goods_id);
	$smarty->assign('goods_attr',$goods_attr);
	$attr_info=$smarty->fetch('erp_select_goods_attr.htm');
	$result['attr_info']=$attr_info;
	$result['attr_num']=count($goods_attr);
	$result['error']=0;
	die($json->encode($result));
}
elseif ($_REQUEST['act'] == 'goods_sn_tips')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	$goods_sn=trim($_REQUEST['goods_sn']);
	$fetch_goods_sn=array();
	$sql="select g.goods_sn from ".$GLOBALS['ecs']->table('goods')." as g ";
	$sql.=" where g.goods_sn like '%".$goods_sn."%' order by goods_id desc limit 10";
	$res=$GLOBALS['db']->query($sql);
	while($row=mysql_fetch_assoc($res))
	{
		$fetch_goods_sn[]=$row['goods_sn'];
	}
	$smarty->assign('goods_sn',$fetch_goods_sn);
	$content=$smarty->fetch('erp_goods_sn_tips.htm');
	$result['error']=0;
	$result['content']=$content;
	$result['num']=count($fetch_goods_sn);
	die($json->encode($result));
}
elseif ($_REQUEST['act'] == 'goods_name_tips')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	$goods_name=trim($_REQUEST['goods_name']);
	$fetch_goods_name=array();
	$sql="select g.goods_name from ".$GLOBALS['ecs']->table('goods')." as g ";
	$sql.=" where g.goods_name like '%".$goods_name."%' order by goods_id desc limit 10";
	$res=$GLOBALS['db']->query($sql);
	while($row=mysql_fetch_assoc($res))
	{
		$fetch_goods_name[]=$row['goods_name'];
	}
	$smarty->assign('goods_name',$fetch_goods_name);
	$content=$smarty->fetch('erp_goods_name_tips.htm');
	$result['error']=0;
	$result['content']=$content;
	$result['num']=count($fetch_goods_name);
	die($json->encode($result));
}
elseif ($_REQUEST['act'] == 'change_specs')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_order_manage','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$order_id=isset($_REQUEST['order_id']) &&intval($_REQUEST['order_id'])>0?intval($_REQUEST['order_id']):0;
	$order_item_id=isset($_REQUEST['order_item_id']) &&intval($_REQUEST['order_item_id'])>0?intval($_REQUEST['order_item_id']):0;
	$specific_desc=isset($_REQUEST['specific_desc'])?trim($_REQUEST['specific_desc']):'';
	if(empty($order_id) ||empty($order_item_id))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$order_info=is_order_exist($order_id);
	if(!$order_info)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if($agency_id>0 &&$order_info['agency_id']!=$agency_id)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_no_agency_access'];
		die($json->encode($result));
	}
	if($order_info['order_status']!=1)
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_order_no_acts'];
		die($json->encode($result));
	}
	if(!is_admin_order($order_id,erp_get_admin_id()))
	{
		$result['error']=5;
		$result['message']=$_LANG['erp_order_no_access'];
		die($json->encode($result));
	}
	if(!is_order_item_exist($order_item_id))
	{
		$result['error']=4;
		$result['message']=$_LANG['erp_order_item_not_exist'];
		die($json->encode($result));
	}
	if(lock_order($order_id,'edit'))
	{
		$sql="update ".$GLOBALS['ecs']->table('erp_order_item')." set ";
		$sql.="specific_desc='".$specific_desc."'";
		$sql.="where order_item_id='".$order_item_id."'";
		if($GLOBALS['db']->query($sql))
		{
			$item_info = get_order_item_info("",$order_item_id);
			$action_remark = "編輯訂單 $order_info[order_sn] 產品 | 更改 " . $item_info['goods_name'] . " 產品要求";
			$sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
					"VALUES(" . $order_id . ", " . ERP_UPDATE . "," . $_SESSION['admin_id'] . ",'" . mysql_escape_string($action_remark) . "')";
			$GLOBALS['db']->query($sql);
			$result['error']=0;
			die($json->encode($result));
		}
	}
	else
	{
		$result['error']=-1;
		$result['message']=$_LANG['erp_order_no_accessibility'];
		die($json->encode($result));
	}
}
elseif ($_REQUEST['act'] == 'change_price')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_order_rate','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$order_id=isset($_REQUEST['order_id']) &&intval($_REQUEST['order_id'])>0?intval($_REQUEST['order_id']):0;
	$order_item_id=isset($_REQUEST['order_item_id'])&&intval($_REQUEST['order_item_id'])>0?intval($_REQUEST['order_item_id']):0;
	$price=isset($_REQUEST['price']) &&floatval($_REQUEST['price'])>0?floatval($_REQUEST['price']):0;
	if(empty($order_id) ||empty($order_item_id))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$order_info=is_order_exist($order_id);
	if(!$order_info)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if($agency_id>0 &&$order_info['agency_id']!=$agency_id)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_no_agency_access'];
		die($json->encode($result));
	}
	//if($order_info['order_status']>2)
	//keep process for other type of erp_order
	if(($order_info['order_status'] != 2 && $order_info['type'] != 2) || ($order_info['order_status'] > 2 && $order_info['type'] == 2))
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_order_no_acts'];
		die($json->encode($result));
	}
	if(!is_order_item_exist($order_item_id))
	{
		$result['error']=4;
		$result['message']=$_LANG['erp_order_item_not_exist'];
		die($json->encode($result));
	}
	if(lock_order($order_id,'rate'))
	{
		if(order_act_record($order_id,'rate'))
		{
			$sql="update ".$GLOBALS['ecs']->table('erp_order_item')." set original_price='".$price."',original_amount=order_qty*original_price where order_item_id=".$order_item_id."";
			$GLOBALS['db']->query($sql);
			$item_info = get_order_item_info("",$order_item_id);
			$action_remark = "編輯訂單 $order_info[order_sn] 產品 | 調整 " . $item_info['goods_name'] . " 定購價格至 " . $price;
			$sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
					"VALUES(" . $order_id . ", " . ERP_UPDATE . "," . $_SESSION['admin_id'] . ",'" . mysql_escape_string($action_remark) . "')";
			$GLOBALS['db']->query($sql);
			$result['error']=0;
			$result['price']=$price;
			$result['amount']=$GLOBALS['db']->getOne("SELECT original_amount FROM ".$GLOBALS['ecs']->table('erp_order_item')." WHERE order_item_id=".$order_item_id."");
			$shipping_sum=$GLOBALS['db']->getOne("SELECT sum(original_shipping_price) FROM ".$GLOBALS['ecs']->table('erp_order_item')." WHERE order_item_id=".$order_item_id."");
			$amount_sum=$GLOBALS['db']->getOne("SELECT sum(original_amount) FROM ".$GLOBALS['ecs']->table('erp_order_item')." WHERE order_id='".$order_id."'");
			$cn_amount_sum=$GLOBALS['db']->getOne("SELECT sum(cn_amount) FROM ".$GLOBALS['ecs']->table('erp_order_item')." WHERE order_id='".$order_id."'");
			$result['total_amount'] = round(bcsub(bcadd($shipping_sum, $amount_sum, 4), $cn_amount_sum, 4), 2);
			die($json->encode($result));
		}
	}
	else
	{
		$result['error']=-1;
		$result['message']=$_LANG['erp_order_no_accessibility'];
		die($json->encode($result));
	}
}
elseif ($_REQUEST['act'] == 'change_shipping')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_order_rate','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$order_id=isset($_REQUEST['order_id']) &&intval($_REQUEST['order_id'])>0?intval($_REQUEST['order_id']):0;
	$order_item_id=isset($_REQUEST['order_item_id'])&&intval($_REQUEST['order_item_id'])>0?intval($_REQUEST['order_item_id']):0;
	$shipping=isset($_REQUEST['shipping']) &&floatval($_REQUEST['shipping'])>0?floatval($_REQUEST['shipping']):0;
	if(empty($order_id) ||empty($order_item_id))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$order_info=is_order_exist($order_id);
	if(!$order_info)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if($agency_id>0 &&$order_info['agency_id']!=$agency_id)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_no_agency_access'];
		die($json->encode($result));
	}
	//if($order_info['order_status']>2)
	//keep process for other type of erp_order
	if(($order_info['order_status'] != 2 && $order_info['type'] != 2) || ($order_info['order_status'] > 2 && $order_info['type'] == 2))
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_order_no_acts'];
		die($json->encode($result));
	}
	if(!is_order_item_exist($order_item_id))
	{
		$result['error']=4;
		$result['message']=$_LANG['erp_order_item_not_exist'];
		die($json->encode($result));
	}
	if(lock_order($order_id,'rate'))
	{
		if(order_act_record($order_id,'rate'))
		{
			$sql="update ".$GLOBALS['ecs']->table('erp_order_item')." set original_shipping_price='".$shipping."' where order_item_id=".$order_item_id."";
			$GLOBALS['db']->query($sql);
			$item_info = get_order_item_info("",$order_item_id);
			$action_remark = "編輯訂單 $order_info[order_sn] 產品 | 調整 " . $item_info['goods_name'] . " 運費至 " . $shipping;
			$sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
					"VALUES(" . $order_id . ", " . ERP_UPDATE . "," . $_SESSION['admin_id'] . ",'" . mysql_escape_string($action_remark) . "')";
			$GLOBALS['db']->query($sql);
			$result['error']=0;
			$result['shipping']=$shipping;
			$shipping_sum=$GLOBALS['db']->getOne("SELECT sum(original_shipping_price) FROM ".$GLOBALS['ecs']->table('erp_order_item')." WHERE order_item_id=".$order_item_id."");
			$amount_sum=$GLOBALS['db']->getOne("SELECT sum(original_amount) FROM ".$GLOBALS['ecs']->table('erp_order_item')." WHERE order_id='".$order_id."'");
			$cn_amount_sum=$GLOBALS['db']->getOne("SELECT sum(cn_amount) FROM ".$GLOBALS['ecs']->table('erp_order_item')." WHERE order_id='".$order_id."'");
			$result['total_amount'] = round(bcsub(bcadd($shipping_sum, $amount_sum, 4), $cn_amount_sum, 4), 2);
			die($json->encode($result));
		}
	}
	else
	{
		$result['error']=-1;
		$result['message']=$_LANG['erp_order_no_accessibility'];
		die($json->encode($result));
	}
}
elseif ($_REQUEST['act'] == 'change_cn')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_order_rate','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$order_id=isset($_REQUEST['order_id']) &&intval($_REQUEST['order_id'])>0?intval($_REQUEST['order_id']):0;
	$order_item_id=isset($_REQUEST['order_item_id'])&&intval($_REQUEST['order_item_id'])>0?intval($_REQUEST['order_item_id']):0;
	$cn_id=isset($_REQUEST['cn_id'])?intval($_REQUEST['cn_id']):0;
	$cn_amount=isset($_REQUEST['amount'])?floatval($_REQUEST['amount']):0;

	if(empty($order_id) ||empty($order_item_id))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$order_info=is_order_exist($order_id);
	if(!$order_info)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if($agency_id>0 &&$order_info['agency_id']!=$agency_id)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_no_agency_access'];
		die($json->encode($result));
	}
	//if($order_info['order_status']>2)
	//keep process for other type of erp_order0
	if(($order_info['order_status'] != 2 && $order_info['type'] != 2) || ($order_info['order_status'] > 2 && $order_info['type'] == 2))
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_order_no_acts'];
		die($json->encode($result));
	}
	if(!is_order_item_exist($order_item_id))
	{
		$result['error']=4;
		$result['message']=$_LANG['erp_order_item_not_exist'];
		die($json->encode($result));
	}
	if(lock_order($order_id,'rate'))
	{
		if(order_act_record($order_id,'rate'))
		{
			$item_info = get_order_item_info("",$order_item_id);
			$supplier = $GLOBALS['db']->getOne("SELECT name FROM " . $GLOBALS['ecs']->table('erp_supplier') . "WHERE supplier_id = '$order_info[supplier_id]'");

			if($item_info['cn_id'])
			{
				$old_cn = $GLOBALS['db']->getRow("SELECT * FROM ".$GLOBALS['ecs']->table('erp_credit_note')." WHERE cn_id = $item_info[cn_id]");
				$actg_txn_id = $GLOBALS['db']->getOne("SELECT `actg_txn_id` FROM `actg_credit_note_transactions` WHERE `cn_txn_id` = $item_info[cn_txn_id]");

				if (!empty($actg_txn_id)) {
					$accountingController = new Yoho\cms\Controller\AccountingController();
					$reverse = [
						'actg_txn_id' => $actg_txn_id,
						'created_by' => $_SESSION['admin_id'],
						'remark' => "移除 $cn[cn_sn] Rebate $order_info[order_sn] #$order_item_id"
					];
					if (!$accountingController->reverseAccountingTransaction($reverse)) {
						$result['error']=4;
						$result['message']='修改會計紀錄失敗';
						die($json->encode($result));
					}
				}
				$sql="update ".$GLOBALS['ecs']->table('erp_order_item')." set cn_id=0,cn_amount=0,cn_txn_id=0 where order_item_id=".$order_item_id."";
				$GLOBALS['db']->query($sql);
				$action_remark = "編輯訂單 $order_info[order_sn] 產品 | 移除 " . $item_info['goods_name'] . " Credit Note ";
				$sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
						"VALUES(" . $order_id . ", " . ERP_UPDATE . "," . $_SESSION['admin_id'] . ",'" . mysql_escape_string($action_remark) . "')";
				$GLOBALS['db']->query($sql);
			}
			if($cn_id)
			{
				if(!is_credit_note_exist($cn_id))
				{
					$result['error']=5;
					$result['message']=$_LANG['erp_credit_note_not_exist'];
					die($json->encode($result));
				}
				else
				{
					$sql = "SELECT * FROM ".$GLOBALS['ecs']->table('erp_credit_note')." WHERE cn_id = $cn_id";
					$cn = $GLOBALS['db']->getRow($sql);
					if($cn_amount > 0)
					{
						$cn_amount = $cn_amount > floatval($cn['curr_amount']) ? floatval($cn['curr_amount']) : $cn_amount;
					}
					$param = array(
						'cn_id' => $cn_id,
						'curr_amount' => $cn['curr_amount'] - $cn_amount,
						'txn_info' => array(
							'supplier_id' => $order_info['supplier_id'],
							'erp_order_id' => $order_id,
							'amount' => -$cn_amount,
							'txn_remark' => "$cn[cn_sn] Rebate $order_info[order_sn] #$order_item_id",
							'remark' => "使用: $cn[cn_sn] 至 $order_info[order_sn] 產品 #$order_item_id"
						)
					);
					$erpController = new Yoho\cms\Controller\ErpController();
					$insert_id = $erpController->update_credit_note($param);
					$sql="update ".$GLOBALS['ecs']->table('erp_order_item')." set cn_id=".$cn_id.",cn_amount=$cn_amount,cn_txn_id=$insert_id where order_item_id=".$order_item_id."";
					$GLOBALS['db']->query($sql);
					$action_remark = "編輯訂單 $order_info[order_sn] 產品 | 應用Credit Note " . $cn_sn . " 至 " . $item_info['goods_name'];
					$sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
							"VALUES(" . $order_id . ", " . ERP_UPDATE . "," . $_SESSION['admin_id'] . ",'" . mysql_escape_string($action_remark) . "')";
					$GLOBALS['db']->query($sql);
					$result['error']=0;
					$result['amount'] = $cn_amount;
					$shipping_sum=$GLOBALS['db']->getOne("SELECT sum(original_shipping_price) FROM ".$GLOBALS['ecs']->table('erp_order_item')." WHERE order_item_id=".$order_item_id."");
					$amount_sum=$GLOBALS['db']->getOne("SELECT sum(original_amount) FROM ".$GLOBALS['ecs']->table('erp_order_item')." WHERE order_id='".$order_id."'");
					$cn_amount_sum=$GLOBALS['db']->getOne("SELECT sum(cn_amount) FROM ".$GLOBALS['ecs']->table('erp_order_item')." WHERE order_id='".$order_id."'");
					$result['total_amount'] = $shipping_sum+$amount_sum-$cn_amount_sum;
					die($json->encode($result));
				}
			}
		}
	}
	else
	{
		$result['error']=-1;
		$result['message']=$_LANG['erp_order_no_accessibility'];
		die($json->encode($result));
	}
	$result['error']=0;
	$shipping_sum=$GLOBALS['db']->getOne("SELECT sum(original_shipping_price) FROM ".$GLOBALS['ecs']->table('erp_order_item')." WHERE order_id='".$order_id."'");
	$amount_sum=$GLOBALS['db']->getOne("SELECT sum(original_amount) FROM ".$GLOBALS['ecs']->table('erp_order_item')." WHERE order_id='".$order_id."'");
	$cn_amount_sum=$GLOBALS['db']->getOne("SELECT sum(cn_amount) FROM ".$GLOBALS['ecs']->table('erp_order_item')." WHERE order_id='".$order_id."'");
	$result['total_amount'] = round(bcsub(bcadd($shipping_sum, $amount_sum, 4), $cn_amount_sum, 4), 2);
	$result['shipping_sum'] = $shipping_sum;
	$result['amount_sum'] = $amount_sum;
	$result['cn_amount_sum'] = $cn_amount_sum;
	die($json->encode($result));
}
elseif ($_REQUEST['act'] == 'post_to_approve')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if(!admin_priv('erp_order_rate','',false))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$order_id=isset($_REQUEST['order_id']) &&intval($_REQUEST['order_id'])>0?intval($_REQUEST['order_id']):0;
	if(empty($order_id))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$order_info=is_order_exist($order_id);
	if(!$order_info)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if($agency_id>0 &&$order_info['agency_id']!=$agency_id)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_no_agency_access'];
		die($json->encode($result));
	}
	if($order_info['order_status']!=2)
	{
		$result['error']=3;
		$result['message']=$_LANG['erp_order_no_acts'];
		die($json->encode($result));
	}
	if(lock_order($order_id,'post_to_approve'))	
	{
		if(order_act_record($order_id,'submit'))
		{
			$rate = $GLOBALS['db']->getOne("SELECT currency_rate FROM actg_currencies WHERE currency_code = '$order_info[currency_code]'");
			$GLOBALS['db']->query("UPDATE " . $GLOBALS['ecs']->table('erp_order_item') . "SET price = original_price * $rate, amount = original_amount * $rate, shipping_price = original_shipping_price * $rate WHERE order_id = $order_id");
			$sql="update ".$GLOBALS['ecs']->table('erp_order')." set ";
			$sql.="order_status ='3' ";
			$sql.="where order_id='".$order_id."'";
			if($GLOBALS['db']->query($sql))
			{
				$action_remark = "編輯訂單 $order_info[order_sn] 狀態 | 由 " . ORDER_STATUS[$order_info['order_status']] . " 至 " . ORDER_STATUS[3];
				$sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
						"VALUES(" . $order_id . ", " . ERP_UPDATE . "," . $_SESSION['admin_id'] . ",'" . mysql_escape_string($action_remark) . "')";
				$GLOBALS['db']->query($sql);
				admin_log($order_info['order_sn'],'submit_rate','erp_order');
				$result['error']=0;
				$result['message']=$_LANG['erp_order_post_success'];
				die($json->encode($result));
			}
		}
	}
	else
	{
		$result['error']=-1;
		$result['message']=$_LANG['erp_order_no_accessibility'];
		die($json->encode($result));
	}
}
elseif ($_REQUEST['act'] == 'approve_pass'||$_REQUEST['act'] == 'approve_reject'||$_REQUEST['act'] == 'unapprove')
{
	include('../includes/cls_json.php');
	$json  = new JSON;
	if((!admin_priv('erp_order_approve','',false) && $_REQUEST['act']!='unapprove') || ($_REQUEST['act']=='unapprove' && !admin_priv(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_PO_UNAPPROVE,'',false)))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_no_permit'];
		die($json->encode($result));
	}
	$order_id=isset($_REQUEST['order_id']) &&intval($_REQUEST['order_id'])>0?intval($_REQUEST['order_id']):0;
	$approve_remark=isset($_REQUEST['approve_remark'])?trim($_REQUEST['approve_remark']):'';
	$act=isset($_REQUEST['act'])?trim($_REQUEST['act']):'';
	if(empty($order_id) ||empty($act))
	{
		$result['error']=1;
		$result['message']=$_LANG['erp_wrong_parameter'];
		die($json->encode($result));
	}
	$order_info=is_order_exist($order_id);
	if(!$order_info)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_not_exist'];
		die($json->encode($result));
	}
	$agency_id=get_admin_agency_id($_SESSION['admin_id']);
	if($agency_id>0 &&$order_info['agency_id']!=$agency_id)
	{
		$result['error']=2;
		$result['message']=$_LANG['erp_order_no_agency_access'];
		die($json->encode($result));
	}
	if ($act=='unapprove')
	{
		// $sql="select '1' from ".$GLOBALS['ecs']->table('erp_warehousing');
		// $sql.="where `warehousing_style_id` = 1 and `status` = 3 and `order_id` = '".$order_id."'";
		// if ($GLOBALS['db']->getOne($sql))
		// {
		// 	$result['error']=3;
		// 	$result['message']="反審核失敗，該採購單已入貨！";
		// 	die($json->encode($result));
		// }

		$action_remark = "反審核訂單 $order_info[order_sn]";
		$sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
				"VALUES(" . $order_id . ", " . ERP_UNAPPROVE . "," . $_SESSION['admin_id'] . ",'" . $action_remark . "')";
		$GLOBALS['db']->query($sql);
		admin_log($order_info['order_sn'],'unapprove','erp_order');

		$erpController = new Yoho\cms\Controller\ErpController();
		$erp_order_info = $erpController->get_erp_order_info($order_id);

		if (!empty($erp_order_info['actg_txn_id'])) {
			$accountingController = new Yoho\cms\Controller\AccountingController();
			$param = [
				'actg_txn_id' => $erp_order_info['actg_txn_id'],
				'created_by' => $_SESSION['admin_id'],
				'remark' => "採購訂單 $order_info[order_sn] 反審核"
			];
			if (!$accountingController->reverseAccountingTransaction($param)) {
				$result['error']=4;
				$result['message']='修改會計紀錄失敗';
				die($json->encode($result));
			}
		}

		$sql="update ".$GLOBALS['ecs']->table('erp_order')." set ";
		//keep process for other type of erp_order
		if ($order_info['type'] == 2){
			$sql.="post_by = '0', post_time = '0', ";
			$sql.="rate_by = '0', rate_time = '0', ";
			$sql.="submit_by = '0', submit_time = '0', ";
		}
		$sql.="approve_by = '0', approve_time = '0', ";
		
		//keep process for other type of erp_order
		if ($order_info['type'] == 2){
			$sql.="order_status ='1', approve_remark='', actg_txn_id = '0' ";
		} else {
			$sql.="order_status ='3', approve_remark='', actg_txn_id = '0' ";
		}
		$sql.="where order_id='".$order_id."'";
		if($GLOBALS['db']->query($sql))
		{
			//keep process for other type of erp_order
			if ($order_info['type'] == 2){
				$action_remark = "編輯訂單 $order_info[order_sn] 狀態 | 由 " . ORDER_STATUS[$order_info['order_status']] . " 至 " . ORDER_STATUS[1];
			} else {
				$action_remark = "編輯訂單 $order_info[order_sn] 狀態 | 由 " . ORDER_STATUS[$order_info['order_status']] . " 至 " . ORDER_STATUS[3];
			}
			$sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
					"VALUES(" . $order_id . ", " . ERP_UPDATE . "," . $_SESSION['admin_id'] . ",'" . mysql_escape_string($action_remark) . "')";
			$GLOBALS['db']->query($sql);
			$result['error']=0;
			$result['message']='';
			die($json->encode($result));
		}
	}
	else
	{
		if($order_info['order_status']!=3)
		{
			$result['error']=3;
			$result['message']=$_LANG['erp_order_no_acts'];
			die($json->encode($result));
		}
		if(lock_order($order_id,'approve'))	
		{
			if(order_act_record($order_id,'approve'))
			{
				$action_remark = "審核訂單 " . $order_info['order_sn'];
				$sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
						"VALUES(" . $order_id . ", " . ERP_APPROVE . "," . $_SESSION['admin_id'] . ",'" . $action_remark . "')";
				$GLOBALS['db']->query($sql);
				admin_log($order_info['order_sn'],'approve','erp_order');
				$action_remark = "編輯訂單 $order_info[order_sn] 狀態 | 由 " . ORDER_STATUS[$order_info['order_status']] . " 至 ";
				if($act=='approve_pass')
				{
					$accountingController = new Yoho\cms\Controller\AccountingController();
					$erpController = new Yoho\cms\Controller\ErpController();
					$erp_order_info = $erpController->get_erp_order_info($order_id);
					$param = [
						'supplier_id'		=> $erp_order_info['supplier_id'],
						'currency_code'		=> $erp_order_info['currency_code'],
						'order_amount'		=> $erp_order_info['order_amount'], // exchanged to HKD
						'shipping_amount'	=> $erp_order_info['shipping_amount'], // exchanged to HKD
						'erp_order_id'		=> $order_id,
						'created_by'		=> $_SESSION['admin_id'],
						'remark'			=> "採購訂單 $order_info[order_sn] 審核",
					];
					$actg_txn_id = $accountingController->insertPurchaseTransactions($param);
					if (!$actg_txn_id) {
						$result['error']=4;
						$result['message']='無法新增會計紀錄';
						die($json->encode($result));
					}

					$sql="update ".$GLOBALS['ecs']->table('erp_order')." set ";
					$sql.="order_status ='4', approve_remark='".mysql_escape_string($approve_remark)."', actg_txn_id = '$actg_txn_id' ";
					$sql.="where order_id='".$order_id."'";
					$action_remark .= ORDER_STATUS[4];
				}
				else if($act=='approve_reject')
				{
					$sql="update ".$GLOBALS['ecs']->table('erp_order')." set ";
					$sql.="order_status ='5', approve_remark='".mysql_escape_string($approve_remark)."' ";
					$sql.="where order_id='".$order_id."'";
					$action_remark .= ORDER_STATUS[5];
				}
				if($GLOBALS['db']->query($sql))
				{
					$sql = "INSERT INTO " . $GLOBALS['ecs']->table('erp_order_log') . "(order_id,action,create_by,remarks)" .
							"VALUES(" . $order_id . ", " . ERP_UPDATE . "," . $_SESSION['admin_id'] . ",'" . mysql_escape_string($action_remark) . "')";
					$GLOBALS['db']->query($sql);
					$result['error']=0;
					$result['message']=$_LANG['erp_order_approve_success'];
					die($json->encode($result));
				}
			}
		}
		else
		{
			$result['error']=-1;
			$result['message']=$_LANG['erp_order_no_accessibility'];
			die($json->encode($result));
		}
	}
}
function get_admin_name($admin_id){
	$sql = "SELECT user_name FROM " . $GLOBALS['ecs']->table('admin_user') . " WHERE user_id = " . $admin_id;
	return $GLOBALS['db']->getOne($sql);
}
function is_credit_note_exist($cn_id){
	$sql = "SELECT 1 FROM " . $GLOBALS['ecs']->table('erp_credit_note') . " WHERE curr_amount > 0 AND cn_id = " . $cn_id;
	return $GLOBALS['db']->getOne($sql);
}
?>
