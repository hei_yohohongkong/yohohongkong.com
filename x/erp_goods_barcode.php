<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_base.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_warehouse.php');
require(dirname(__FILE__) .'/includes/ERP/cls/cls_date.php');
if($_REQUEST['act'] == 'list'||$_REQUEST['act'] == 'print'||$_REQUEST['act'] == 'export')
{
if(admin_priv('erp_sys_manage','',false) ||admin_priv('erp_warehouse_view','',false) ||admin_priv('erp_warehouse_manage','',false) ||admin_priv('erp_warehouse_approve','',false))
{
$page=isset($_REQUEST['page']) &&intval($_REQUEST['page']) >0 ?intval($_REQUEST['page']):1;
$goods_sn=isset($_REQUEST['goods_sn'])?trim($_REQUEST['goods_sn']):'';
$goods_name=isset($_REQUEST['goods_name'])?trim($_REQUEST['goods_name']):'';
$goods_barcode=isset($_REQUEST['goods_barcode'])?trim($_REQUEST['goods_barcode']):'';
include('./includes/ERP/page.class.php');
$num_per_page=10;
$mode=1;
$page_bar_num=6;
$page_style="page_style";
$current_page_style="current_page_style";
$start=$num_per_page*($page-1);
$paras=array(
'goods_sn'=>$goods_sn,
'goods_name'=>$goods_name,
'goods_barcode'=>$goods_barcode
);
$total_num=get_erp_goods_barcode_count($paras);
$goods_list=get_erp_goods_barcode($paras,$start,$num_per_page);
$smarty->assign('goods_list',$goods_list);
$url=fix_url($_SERVER['REQUEST_URI'],array(
'page'=>'',
'goods_sn'=>$goods_sn,
'goods_name'=>$goods_name,
'goods_barcode'=>$goods_barcode
)
);
$pager=new page(array('total_data'=>$total_num,'data_per_page'=>$num_per_page,'url'=>$url,'mode'=>$mode,'page_bar_num'=>$page_bar_num,'page_style'=>$page_style,'current_page_style'=>$current_page_style));
$smarty->assign('pager',$pager->show());
$smarty->assign('goods_sn',$goods_sn);
$smarty->assign('goods_name',$goods_name);
$smarty->assign('goods_barcode',$goods_barcode);
$smarty->assign('page',$page);
$smarty->assign('ur_here',$_LANG['erp_goods_barcode_setting']);
if(admin_priv('erp_sys_manage','',false))
{
$smarty->assign('allow_edit',1);
}
else{
$smarty->assign('allow_edit',0);
}
if($_REQUEST['act'] == 'list'||$_REQUEST['act'] == 'print')
{
$template=$_REQUEST['act'] == 'list'?'erp_goods_barcode_list.htm': 'erp_goods_barcode_print.htm';
$smarty->display($template);
}
else{
require(dirname(__FILE__) .'/includes/ERP/cls/cls_csv.php');
require(dirname(__FILE__) .'/includes/ERP/cls/cls_file.php');
include(ROOT_PATH.'/includes/cls_json.php');
$export=array();
$row['goods_id']=$_LANG['erp_barcode_goods_id'];
$row['goods_sn']=$_LANG['erp_barcode_goods_sn'];
$row['goods_name']=$_LANG['erp_barcode_goods_name'];
$row['goods_attr']=$_LANG['erp_barcode_goods_attr'];
$row['goods_barcode']=$_LANG['erp_barcode_goods_barcode'];
$export[]=$row;
if(!empty($goods_list))
{
foreach($goods_list as $goods_id =>$v)
{
if($v['goods_info']['no_attr']==1)
{
$row['goods_id']=$goods_id;
$row['goods_sn']=$v['goods_info']['goods_sn'];
$row['goods_name']=$v['goods_info']['goods_name'];
$row['goods_attr']='$$';
$row['goods_barcode']=$v['goods_info']['barcode'];
$export[]=$row;
}
else{
foreach($v['attr'] as $k =>$attr)
{
$row['goods_id']=$goods_id;
$row['goods_sn']=$v['goods_info']['goods_sn'];
$row['goods_name']=$v['goods_info']['goods_name'];
$attr_value='';
foreach($attr['attr'] as $i =>$attr_v)
{
$attr_value.=$attr_v['attr_value'];
}
$row['goods_attr']=$attr_value;
$row['goods_barcode']=$attr['barcode'];
$export[]=$row;
}
}
}
}
$csv=new cls_csv();
$file_stream=$csv->encode($export);
$file_stream=iconv('utf-8','gb2312',$file_stream);
$file=new cls_file();
$path=ROOT_PATH.'data/barcode/';
$random_name=random_string('numeric',8);
$file_name=date('Y-m-d').'-'.$random_name.'.csv';
while(file_exists($path.$file_name))
{
$random_name=random_string('numeric',8);
$file_name=date('Y-m-d').'-'.$random_name.'.csv';
}
$file->write_file($path,$file_name,$file_stream,$op='rewrite');
$json  = new JSON;
$server_address=$_SERVER["SERVER_NAME"];
$phpself=$_SERVER["PHP_SELF"];
$port=$_SERVER["SERVER_PORT"];
$path=str_replace(ADMIN_PATH.'/erp_goods_barcode.php','',$phpself);
$base_url=$server_address.':'.$port.$path;
$url='http://'.$base_url.'data/barcode/'.$file_name;
$result['error']=0;
$result['url']=$url;
die($json->encode($result));
}
}
else{
$href="index.php?act=main";
$text=$_LANG['erp_retun_to_center'];
$link[] = array('href'=>$href,'text'=>$text);
sys_msg($_LANG['erp_no_permit'],0,$link);
}
}
elseif($_REQUEST['act'] == 'set_barcode')
{
include('../includes/cls_json.php');
$json  = new JSON;
if(!admin_priv('erp_sys_manage','',false))
{
$result['error']=1;
$result['message']=$_LANG['erp_no_permit'];
die($json->encode($result));
}
$obj_id=isset($_REQUEST['obj_id'])?trim($_REQUEST['obj_id']) : '';
$barcode=isset($_REQUEST['barcode'])?trim($_REQUEST['barcode']) : '';
if(empty($obj_id))
{
$result['error']=2;
$result['message']=$_LANG['erp_wrong_parameter'];
die($json->encode($result));
}
$obj_id=explode('_',$obj_id);
$goods_id=$obj_id[0];
unset($obj_id[0]);
if(count($obj_id) >0)
{
$attr_id_list=implode(',',$obj_id);
$sql="select barcode_id from ".$GLOBALS['ecs']->table('erp_goods_attr')." where goods_id='".$goods_id."' and attr_id_no_dynamic='".$attr_id_list."' limit 1";
$barcode_id=$GLOBALS['db']->getOne($sql);
if($barcode_id >0)
{
$sql="update ".$GLOBALS['ecs']->table('erp_goods_barcode')." set barcode='".$barcode."' where id='".$barcode_id."'";
$GLOBALS['db']->query($sql,'SILENT');
$error_no = $GLOBALS['db']->errno();
if ($error_no == 1062)
{
$result['error']=3;
$result['message']=$_LANG['erp_barcode_exists'];
die($json->encode($result));
}
}
else{
$sql="insert into ".$GLOBALS['ecs']->table('erp_goods_barcode')." set barcode='".$barcode."'";
$GLOBALS['db']->query($sql,'SILENT');
$error_no = $GLOBALS['db']->errno();
if ($error_no == 1062)
{
$result['error']=3;
$result['message']=$_LANG['erp_barcode_exists'];
die($json->encode($result));
}
$barcode_id=$GLOBALS['db']->insert_id();
$sql="update ".$GLOBALS['ecs']->table('erp_goods_attr')." set barcode_id='".$barcode_id."' where goods_id='".$goods_id."' and attr_id_no_dynamic='".$attr_id_list."'";
$GLOBALS['db']->query($sql);
}
}
else{
$sql="select barcode_id from ".$GLOBALS['ecs']->table('erp_goods_attr')." where goods_id='".$goods_id."' limit 1";
$barcode_id=$GLOBALS['db']->getOne($sql);
if($barcode_id >0)
{
$sql="update ".$GLOBALS['ecs']->table('erp_goods_barcode')." set barcode='".$barcode."' where id='".$barcode_id."'";
$GLOBALS['db']->query($sql,'SILENT');
$error_no = $GLOBALS['db']->errno();
if ($error_no == 1062)
{
$result['error']=3;
$result['message']=$_LANG['erp_barcode_exists'];
die($json->encode($result));
}
}
else{
$sql="insert into ".$GLOBALS['ecs']->table('erp_goods_barcode')." set barcode='".$barcode."'";
$GLOBALS['db']->query($sql,'SILENT');
$error_no = $GLOBALS['db']->errno();
if ($error_no == 1062)
{
$result['error']=3;
$result['message']=$_LANG['erp_barcode_exists'];
die($json->encode($result));
}
$barcode_id=$GLOBALS['db']->insert_id();
$sql="update ".$GLOBALS['ecs']->table('erp_goods_attr')." set barcode_id='".$barcode_id."' where goods_id='".$goods_id."'";
$GLOBALS['db']->query($sql);
}
}
$result['error']=0;
$result['message']='';
die($json->encode($result));
}
?>
