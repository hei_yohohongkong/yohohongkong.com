<?php

define('IN_ECS', true);
require(dirname(__FILE__) . '/includes/init.php');
$adminTaskTemplatesController = new Yoho\cms\Controller\AdminTaskTemplatesController();
$ctrlTask = new Yoho\cms\Controller\TaskController();
if ($_REQUEST['act'] == 'list')
{
    $smarty->assign('ur_here',      $_LANG['admin_task_templates']);

    $smarty->assign('action_link', array('text' => $_LANG['admin_task_templates_new'], 'href' => 'admin_task_templates.php?act=edit'));

    assign_query_info();
    $smarty->display('admin_task_templates_list.htm');
}
elseif ($_REQUEST['act'] == 'query')
{
    $list = $adminTaskTemplatesController->getAdminTaskTemplates();
    $adminTaskTemplatesController->ajaxQueryAction($list['data'],$list['record_count']);

}
elseif ($_REQUEST['act'] == 'remove')
{
    $template_id = intval($_REQUEST['id']);
    if ($adminTaskTemplatesController->removeAdminTaskTemplate($template_id)){
        // delete task template used
        $ctrlTask->deleteTaskTemplateUsed($template_id);
    }
    
    $url = 'admin_task_templates.php?act=query&' . str_replace('act=' . $_REQUEST['act'], '', $_SERVER['QUERY_STRING']);
    
    header('Location: ' . $url);
    exit;
}
elseif ($_REQUEST['act'] == 'edit')
{
    $adminTaskTemplatesController->editAction();

}
elseif ($_REQUEST['act'] == 'update' || $_REQUEST['act'] == 'insert')
{
    $adminTaskTemplatesController->updateAction();
}
?>
