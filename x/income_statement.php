<?php

define('IN_ECS', true);

require dirname(__FILE__) . '/includes/init.php';
require_once ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_accounting.php';

if ($_REQUEST['act'] == 'list') {
    $tree = [];
    $expense = getTotalExpense(1);
    $tree = generateTree($tree, 'Sales', $expense);
    $expense = getTotalExpense(2);
    $tree = generateTree($tree, 'Operating Expense', $expense);
    $expense = getTotalExpense(3);
    $tree = generateTree($tree, 'Non-Operation', $expense);
    assign_query_info();
    $smarty->assign('ur_here', 'Income Statement');
} elseif ($_REQUEST['act'] == 'cash_list') {
    $tree = [];
    $expense = getTotalExpense(4);
    $tree = generateTree($tree, 'Operating Activities', $expense);
    $expense = getTotalExpense(5);
    $tree = generateTree($tree, 'Investing Activities', $expense);
    $expense = getTotalExpense(6);
    $tree = generateTree($tree, 'Financing Activities', $expense);
    assign_query_info();
    $smarty->assign('ur_here', 'Cash Flow Statement');
}
for ($i = 2017; $i <= local_date('Y'); $i++) {
    $all_year[] = $i;
}
for ($i = 1; $i <= 12; $i++) {
    $all_month[] = $i;
}
$smarty->assign('tree', $tree);
$smarty->assign('all_month', $all_month);
$smarty->assign('all_year', $all_year);
$smarty->display('income_statement.htm');

/**
 * Get all transactions from specific types
 * 
 * @param int $type expense type, 0: all without exclude type; 1: sales; 2: operating
 * 
 * @return array tree and sum od expense
 */
function getTotalExpense($type = 0)
{
    $year = !empty($_REQUEST['year']) ? $_REQUEST['year'] : local_date('Y');
    $month = !empty($_REQUEST['month']) ? $_REQUEST['month'] : local_date('n');
    $GLOBALS['smarty']->assign('month', $month);
    $GLOBALS['smarty']->assign('year', $year);

    $exclude_type = [0,44,47,55,69,80]; // Exclude: Unspecified, Internal Transfer, Cashier Adjustment, Credit Note, Account Transfer, Fixed Asset
    $sales_type = [1,3,4,10,46,73]; // Sales type: Sales, Repair Income, Purchase, Shipping Expense, Refund, Sponsorship Income
    $non_op_type = [64,71]; // Tax, Interest Income
    $invest_act_type = [80,81]; // Fixed Asset, Property Sale
    $finance_act_type = [83,84,85,86]; // Issuing Stock, Loan, Dividend, Loan Repayment
    switch ($type) {
        case 0:
            $where_type = "NOT" . db_create_in($exclude_type);
            break;
        case 1:
            $where_type = db_create_in($sales_type);
            break;
        case 2:
            $where_type = "NOT" . db_create_in(array_merge($exclude_type, $sales_type, $non_op_type));
            break;
        case 3:
            $where_type = db_create_in($non_op_type);
            break;
        case 4:
            $where_type = "NOT" . db_create_in(array_merge($exclude_type, $non_op_type, $invest_act_type));
            break;
        case 5:
            $where_type = db_create_in($invest_act_type);
            break;
        case 6:
            $where_type = db_create_in($finance_act_type);
            break;
    }
    $sql = "SELECT t.txn_type_id as child_id, SUM(amount * currency_rate) as expense, tt.txn_type_name as child_name, " .
        "IFNULL(tt2.txn_type_name, '') as parent_name, tt.txn_type_parent as parent_id FROM actg_account_transactions t " .
        "LEFT JOIN actg_transaction_types tt on t.txn_type_id = tt.txn_type_id " .
        "LEFT JOIN actg_transaction_types tt2 on tt2.txn_type_id = tt.txn_type_parent " .
        "LEFT JOIN actg_currencies c on c.currency_code = t.currency_code " .
        "WHERE (t.txn_type_id " . $where_type . " OR tt.txn_type_parent " . $where_type . ")" .
        "AND expense_year = " . $year . " AND expense_month = " . $month .
        " GROUP BY t.txn_type_id";
    $total_expense = $GLOBALS['db']->getAll($sql);

    $total = 0;
    foreach ($total_expense as $row) {
        $branch = [
            'name'              => $row['child_name'],
            'expense'           => floatval($row['expense']),
            'formatted_expense' => number_format(floatval($row['expense']), 2)
        ];
        if (empty($row['parent_id'])) {
            $tree[$row['child_id']]['name'] = $row['child_name'];
            $tree[$row['child_id']]['expense'] = floatval($row['expense']);
            $tree[$row['child_id']]['formatted_expense'] = number_format($tree[$row['child_id']]['expense'], 2);
        } elseif (!empty($row['parent_id'])) {
            $tree[$row['parent_id']]['name'] = $row['parent_name'];
            $tree[$row['parent_id']]['expense'] = bcadd($tree[$row['parent_id']]['expense'], floatval($row['expense']));
            $tree[$row['parent_id']]['formatted_expense'] = number_format($tree[$row['parent_id']]['expense'], 2);
            $tree[$row['parent_id']]['child'][] = $branch;
        }
        $total = bcadd($total, floatval($row['expense']), 2);
    }
    return [
        'tree' => $tree,
        'total' => $total
    ];
}

/**
 * Create tree
 * 
 * @param array  $tree    original tree
 * @param string $name    section name
 * @param array  $expense data
 * 
 * @return array new tree
 */
function generateTree($tree, $name, $expense)
{
    if (!empty($expense['tree'])) {
        $tree[] = [
            'name' => $name,
            'tree' => $expense['tree'],
            'total' => number_format($expense['total'], 2)
        ];
    }
    return $tree;
}