<?php

/**
 * ECSHOP 超值礼包管理程序
 * ============================================================================
 * 版权所有 2005-2010 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liuhui $
 * $Id: package.php 17063 2010-03-25 06:35:46Z liuhui $
*/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
$exc = new exchange($ecs->table("goods_activity"), $db, 'act_id', 'act_name');
// define controller
$userController = new Yoho\cms\Controller\UserController();
$packageController = new Yoho\cms\Controller\PackageController();
$controller     = new Yoho\cms\Controller\YohoBaseController('goods_activity');

/*------------------------------------------------------ */
//-- 添加活动
/*------------------------------------------------------ */
if ($_REQUEST['act'] == 'add')
{
    /* 权限判断 */
    admin_priv('package_manage');

    /* 组合商品 */
    $group_goods_list = array();
    $sql = "DELETE FROM " .$ecs->table('package_goods') .
            " WHERE package_id = 0 AND admin_id = '$_SESSION[admin_id]'";

    $db->query($sql);

    /* 初始化信息 */
    $start_time = local_date('Y-m-d');
    $end_time   = local_date('Y-m-d', strtotime('+1 month'));
    $package     = array('package_price'=>'', 'start_time' => $start_time, 'end_time' => $end_time);

    $smarty->assign('package',      $package);
    $smarty->assign('ur_here',      $_LANG['package_add']);
    $smarty->assign('action_link',  array('text' => $_LANG['14_package_list'], 'href'=>'package.php?act=list'));
    $smarty->assign('cat_list',     cat_list());
    $smarty->assign('brand_list',   get_brand_list());
    $smarty->assign('form_action',  'insert');

    // Multiple language support
    $localizable_fields = localizable_fields_for_table('goods_activity');
    $smarty->assign('localizable_fields', $localizable_fields);
    $smarty->assign('localized_versions', get_localized_versions('goods_activity', $_REQUEST['id'], $localizable_fields));

    assign_query_info();
    $smarty->display('package_info.htm');
}

elseif ($_REQUEST['act'] =='insert')
{
    /* 权限判断 */
    admin_priv('package_manage');

    $sql = "SELECT COUNT(*) ".
           " FROM " . $ecs->table('goods_activity').
           " WHERE act_type='" . GAT_PACKAGE . "' AND act_name='" . $_POST['package_name'] . "'" ;
    if ($db->getOne($sql))
    {
        sys_msg(sprintf($_LANG['package_exist'],  $_POST['package_name']) , 1);
    }


    /* 将时间转换成整数 */
    $start_time = local_strtotime(date('Y-m-d H:i:s', strtotime($_POST['start_time'])));
    $end_time = local_strtotime(date('Y-m-d H:i:s', strtotime($_POST['end_time'] . "+ 86399 SECONDS")));

    /* 处理提交数据 */
    if (empty($_POST['package_price']))
    {
        $_POST['package_price'] = 0;
    }

    $info = array('package_price'=>$_POST['package_price']);

    // handle simplified chinese
    $localizable_fields = [
        "act_name" => $_POST['package_name'],
        "act_tag" => $_POST['package_tag'],
        "act_desc" => $_POST['desc'],
    ];
    to_simplified_chinese($localizable_fields);

    /* 插入数据 */
    $record = array('act_name'=>$_POST['package_name'], 'act_tag'=>$_POST['package_tag'], 'act_desc'=>$_POST['desc'],
                    'act_type'=>GAT_PACKAGE, 'start_time'=>$start_time,
                    'end_time'=>$end_time, 'is_finished'=>0, 'ext_info'=>serialize($info));

    $db->AutoExecute($ecs->table('goods_activity'),$record,'INSERT');

    /* 礼包编号 */
    $package_id = $db->insert_id();

    // Multiple language support
    save_localized_versions('goods_activity', $package_id);

    handle_packagep_goods($package_id);
    regen_related_category($package_id);

    admin_log($_POST['package_name'],'add','package');
    $link[] = array('text' => $_LANG['back_list'], 'href'=>'package.php?act=list');
    $link[] = array('text' => $_LANG['continue_add'], 'href'=>'package.php?act=add');
    sys_msg($_LANG['add_succeed'],0,$link);
}

/*------------------------------------------------------ */
//-- 编辑活动
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'edit')
{
    /* 权限判断 */
    admin_priv('package_manage');

    $package            = $packageController->get_package_info($_REQUEST['id']);
    $package_goods_list = $packageController->get_package_goods($_REQUEST['id']); // 礼包商品

    $smarty->assign('package',           $package);
    $smarty->assign('ur_here',           $_LANG['package_edit']);
    $smarty->assign('action_link',       array('text' => $_LANG['14_package_list'], 'href'=>'package.php?act=list&' . list_link_postfix()));
    $smarty->assign('cat_list',     cat_list());
    $smarty->assign('brand_list',   get_brand_list());
    $smarty->assign('form_action',       'update');
    $smarty->assign('package_goods_list', $package_goods_list);

    // Multiple language support
    $localizable_fields = localizable_fields_for_table('goods_activity');
    $smarty->assign('localizable_fields', $localizable_fields);
    $smarty->assign('localized_versions', get_localized_versions('goods_activity', $_REQUEST['id'], $localizable_fields));
    assign_query_info();
    $smarty->display('package_info.htm');

}
elseif ($_REQUEST['act'] =='update')
{
    /* 权限判断 */
    admin_priv('package_manage');

    /* 将时间转换成整数 */
    $start_time = local_strtotime(date('Y-m-d H:i:s', strtotime($_POST['start_time'])));
    $end_time = local_strtotime(date('Y-m-d H:i:s', strtotime($_POST['end_time'] . "+ 86399 SECONDS")));

    /* 处理提交数据 */
    if (empty($_POST['package_price']))
    {
        $_POST['package_price'] = 0;
    }

    /* 检查活动重名 */
    $sql = "SELECT COUNT(*) ".
           " FROM " . $ecs->table('goods_activity').
           " WHERE act_type='" . GAT_PACKAGE . "' AND act_name='" . $_POST['package_name'] . "' AND act_id <> '" .  $_POST['id'] . "'" ;
    if ($db->getOne($sql))
    {
        sys_msg(sprintf($_LANG['package_exist'],  $_POST['package_name']) , 1);
    }


    $info = array('package_price'=>$_POST['package_price']);

    // handle simplified chinese
    $localizable_fields = [
        "act_name" => $_POST['package_name'],
        "act_tag" => $_POST['package_tag'],
        "act_desc" => $_POST['desc'],
    ];
    $localizable_fields = $controller->checkRequireSimplied("goods_activity", $localizable_fields, $_POST['id']);
    to_simplified_chinese($localizable_fields);

    /* 更新数据 */
    $record = array('act_name' => $_POST['package_name'], 'act_tag' => $_POST['package_tag'], 'start_time' => $start_time, 'end_time' => $end_time,
                    'act_desc' => $_POST['desc'], 'ext_info'=>serialize($info));
    $db->autoExecute($ecs->table('goods_activity'), $record, 'UPDATE', "act_id = '" . $_POST['id'] . "' AND act_type = " . GAT_PACKAGE );

    regen_related_category($_POST['id']);

    // Multiple language support
    save_localized_versions('goods_activity', $_POST['id']);

    admin_log($_POST['package_name'],'edit','package');
    $link[] = array('text' => $_LANG['back_list'], 'href'=>'package.php?act=list&' . list_link_postfix());
    sys_msg($_LANG['edit_succeed'],0,$link);
}

/*------------------------------------------------------ */
//-- 删除指定的活动
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'remove')
{
    check_authz_json('package_manage');

    $id = intval($_GET['id']);

    $exc->drop($id);

    $sql = "DELETE FROM " .$ecs->table('package_goods') .
            " WHERE package_id='$id'";
    $db->query($sql);

    $sql = "UPDATE " .$ecs->table('goods_activity') .
            "SET related_category = '' WHERE act_id='$id'";
    $db->query($sql);

    $url = 'package.php?act=list';

    ecs_header("Location: $url\n");
    exit;
}

/*------------------------------------------------------ */
//-- 活动列表
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'list')
{
    //admin_priv('package');

    $list = $packageController->get_packagelist();
    //print_r($list);
    $smarty->assign('package_list', $list);

    $smarty->assign('ur_here',      $_LANG['14_package_list']);
    $smarty->assign('action_link',  array('text' => $_LANG['package_add'], 'href'=>'package.php?act=add'));

    $abnormal_package = $packageController->get_abnormal_package_ids();
    $smarty->assign('abnormal_package_count', count($abnormal_package));

    assign_query_info();
    $smarty->display('package_list.htm');
}

/*------------------------------------------------------ */
//-- 查询、翻页、排序
/*------------------------------------------------------ */
/*
elseif ($_REQUEST['act'] == 'query')
{
    $list = $packageController->get_packagelist();

    $controller->ajaxQueryAction($list['packages'], $list['record_count'], ['edit', 'remove'], ['fields_mapping' => ['package_name' => 'act_name', 'package_tag' => 'act_tag']] );
}
*/

/*------------------------------------------------------ */
//-- 编辑活动名称
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'edit_package_name')
{
    check_authz_json('package_manage');

    $id = intval($_POST['id']);
    $val = json_str_iconv(trim($_POST['val']));

    /* 检查活动重名 */
    $sql = "SELECT COUNT(*) ".
           " FROM " . $ecs->table('goods_activity').
           " WHERE act_type='" . GAT_PACKAGE . "' AND act_name='$val' AND act_id <> '$id'" ;
    if ($db->getOne($sql))
    {
        make_json_error(sprintf($_LANG['package_exist'],  $val));
    }

    $exc->edit("act_name='$val'", $id);
    make_json_result(stripslashes($val));
}

/*------------------------------------------------------ */
//-- 搜索商品
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'search_goods')
{
    include_once(ROOT_PATH . 'includes/cls_json.php');
    $json = new JSON;

    $filters = $json->decode($_GET['JSON']);

    //$arr = get_goods_list($filters);
    $arr = get_goods_list_from_include_goods($filters);

    $opt = array();
    foreach ($arr AS $key => $val)
    {
        $opt[$key] = array('value' => $val['goods_id'],
                        'text' => $val['goods_name'],
                        'data' => $val['shop_price']);

        $opt[$key]['products'] = get_good_products($val['goods_id']);
    }

    make_json_result($opt);
}

/*------------------------------------------------------ */
//-- 搜索商品，仅返回名称及ID
/*------------------------------------------------------ */

//elseif ($_REQUEST['act'] == 'get_goods_list')
//{
//    include_once(ROOT_PATH . 'includes/cls_json.php');
//    $json = new JSON;
//
//    $filters = $json->decode($_GET['JSON']);
//
//    $arr = get_goods_list($filters);
//
//    $opt = array();
//    foreach ($arr AS $key => $val)
//    {
//        $opt[$key] = array('value' => $val['goods_id'],
//                        'text' => $val['goods_name'],
//                        'data' => $val['shop_price']);
//
//        $opt[$key]['products'] = get_good_products($val['goods_id']);
//    }
//
//    make_json_result($opt);
//}

/*------------------------------------------------------ */
//-- 增加一个商品
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'add_package_goods')
{
    include_once(ROOT_PATH . 'includes/cls_json.php');
    $json = new JSON;

    check_authz_json('package_manage');

    $fittings   = $json->decode($_GET['add_ids']);
    $arguments  = $json->decode($_GET['JSON']);
    $package_id = $arguments[0];
    $number     = $arguments[1];
    $price     = $arguments[2];

    foreach ($fittings AS $val)
    {
        $val_array = explode('_', $val);
        if (!isset($val_array[1]) || $val_array[1] <= 0)
        {
            $val_array[1] = 0;
        }

        $sql = "INSERT INTO " . $ecs->table('package_goods') . " (package_id, goods_id, product_id, goods_number, goods_price, admin_id) " .
                "VALUES ('$package_id', '" . $val_array[0] . "', '" . $val_array[1] . "', '$number', '$price', '$_SESSION[admin_id]')";
        $db->query($sql, 'SILENT');
    }

    
    $arr = $packageController->get_package_goods($package_id);
    $opt = array();

    foreach ($arr AS $val)
    {
        
        $opt[] = array('value'      => $val['g_p'],
                        'text'      => $val['goods_name'],
                        'price'     => $val['goods_price'],
                        'highlight' => $val['highlight'],
                        'data'      => '');
    }
    clear_cache_files();
    make_json_result($opt);
}

/*------------------------------------------------------ */
//-- 删除一个商品
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'drop_package_goods')
{
    include_once(ROOT_PATH . 'includes/cls_json.php');
    $json = new JSON;

    check_authz_json('package_manage');

    $fittings   = $json->decode($_GET['drop_ids']);
    $arguments  = $json->decode($_GET['JSON']);
    $package_id = $arguments[0];

    $goods  = array();
    $g_p    = array();
    foreach ($fittings AS $val)
    {
        $val_array = explode('_', $val);
        if (isset($val_array[1]) && $val_array[1] > 0)
        {
            $g_p['product_id'][] = $val_array[1];
            $g_p['goods_id'][] = $val_array[0];
        }
        else
        {
            $goods[] = $val_array[0];
        }
    }

    if (!empty($goods))
    {
        $sql = "DELETE FROM " .$ecs->table('package_goods') .
                " WHERE package_id='$package_id' AND " .db_create_in($goods, 'goods_id');
        if ($package_id == 0)
        {
            $sql .= " AND admin_id = '$_SESSION[admin_id]'";
        }
        $db->query($sql);
    }

    if (!empty($g_p))
    {
        $sql = "DELETE FROM " .$ecs->table('package_goods') .
                " WHERE package_id='$package_id' AND " .db_create_in($g_p['goods_id'], 'goods_id') . " AND " . db_create_in($g_p['product_id'], 'product_id');
        if ($package_id == 0)
        {
            $sql .= " AND admin_id = '$_SESSION[admin_id]'";
        }
        $db->query($sql);
    }

    $arr = $packageController->get_package_goods($package_id);
    $opt = array();

    foreach ($arr AS $val)
    {
        $opt[] = array('value'      => $val['goods_id'],
                        'text'      => $val['goods_name'],
                        'price'     => $val['goods_price'],
                        'highlight' => $val['highlight'],
                        'data'      => '');
    }

    clear_cache_files();
    make_json_result($opt);

}
//use one time, remove it after bulk update
elseif ($_REQUEST['act'] == 'bulk_update_pak_related_cat')
{
    $sql = "SELECT act_id FROM " . $ecs->table('goods_activity');
    $paks = $db->getAll($sql);
    foreach($paks as $pak){
        $package_id = $pak['act_id'];
        regen_related_category($package_id);
    }
}
elseif ($_REQUEST['act'] == 'ajax_update_status')
{
    admin_priv('package_on_off');
    //admin_priv('menu_flashdeal_events');
    if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0){
        $package_id = $_REQUEST['id'];
    }
    if (isset($_REQUEST['status']) && $_REQUEST['status'] != ''){
        if ($_REQUEST['status'] == 'true'){
            $new_status = 1;
        } elseif ($_REQUEST['status'] == 'false'){
            $new_status = 0;
        }
    }

    updateStatusById($package_id, $new_status);

    $current_package = $packageController->get_package_info($package_id);
    if ($current_package){

        $current_package_status = (string)$current_package['status'];

        $str_now = date('Y-m-d H:i:s');
        $datetime_now = DateTime::createFromFormat($format, $str_now);
        $datetime_start = DateTime::createFromFormat($format, $current_favourable['start_time']);
        $datetime_end = DateTime::createFromFormat($format, $current_favourable['end_time']);

        if ($datetime_start < $datetime_now && $datetime_end > $datetime_now){
            if ($current_package['status'] == 1){
                $current_package_status_on_off = '1';
            } else {
                $current_package_status_on_off = '0';
            }
        } elseif ($datetime_start > $datetime_now) {
            if ($current_package['status'] == 1){
                $current_package_status_on_off = '2';
            } else {
                $current_package_status_on_off = '0';
            }
        } else {
            $current_package_status_on_off = '0';
        }

        return make_json_result(array('status' => $current_package_status, 'on_off' => $current_package_status_on_off));
    }
    
}


/**
 * 保存某礼包的商品
 * @param   int     $package_id
 * @return  void
 */
function handle_packagep_goods($package_id)
{
    $sql = "UPDATE " . $GLOBALS['ecs']->table('package_goods') . " SET " .
            " package_id = '$package_id' " .
            " WHERE package_id = '0'" .
            " AND admin_id = '$_SESSION[admin_id]'";
    $GLOBALS['db']->query($sql);
}

function regen_related_category($package_id)
{
    $sql = "SELECT goods_id FROM " . $GLOBALS['ecs']->table('package_goods') .
        " WHERE package_id = " . $package_id;
    $goods_ids = $GLOBALS['db']->getCol($sql);

    $sql = "SELECT cat_id FROM " . $GLOBALS['ecs']->table('goods') .
        " WHERE goods_id " . db_create_in($goods_ids) . 
        " GROUP BY cat_id";
    $related_category = $GLOBALS['db']->getCol($sql);
    $related_category = join(',', $related_category);

    $sql = "UPDATE " .$GLOBALS['ecs']->table('goods_activity') .
            "SET related_category = '".$related_category."' WHERE act_id='$package_id'";
    $GLOBALS['db']->query($sql);
}

function updateStatusById($id, $status){
    $sql = "UPDATE ".$GLOBALS['ecs']->table('goods_activity')." SET status = '$status' WHERE act_id = '$id'";
    $res = $GLOBALS['db']->query($sql);
}

?>