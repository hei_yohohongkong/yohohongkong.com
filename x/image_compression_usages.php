<?php

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require(ROOT_PATH . 'includes/lib_order.php');
require_once(ROOT_PATH .'/languages/zh_tw/admin/order.php');

$imageCompressionController = new Yoho\cms\Controller\ImageCompressionController();

if (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'query')) {
    /* 检查权限 */
    $result = $imageCompressionController->getTinyUsagesList();
    $imageCompressionController->ajaxQueryAction($result['data'],$result['record_count'],false);
}else if (isset($_REQUEST['act']) && $_REQUEST['act'] == 'list') {
    $smarty->assign('ur_here', 'Tiny壓縮圖片使用量報告');
    $smarty->assign('image_compression_limit', $_CFG['lang']);
    $smarty->assign('cfg_tiny_limit', $_CFG['tiny_limit']);
    $smarty->assign('lang', $_LANG);
    /* 显示页面 */
    assign_query_info();
    $smarty->display('image_usage_report_list.htm');
}