<?php

/***
* mail_sent_log.php
* by howang 2014-07-03
*
* List of email sent by the system
***/

define('IN_ECS', true);
require(dirname(__FILE__) . '/includes/init.php');


if ($_REQUEST['act'] == 'list')
{
    /* 检查权限 */
    admin_priv('mail_sent_log');
    
    $list = get_sent_log();
    $smarty->assign('ur_here',      $_LANG['mail_sent_log']);
    $smarty->assign('full_page',    1);

    $smarty->assign('list',         $list['data']);
    $smarty->assign('filter',       $list['filter']);
    $smarty->assign('record_count', $list['record_count']);
    $smarty->assign('page_count',   $list['page_count']);
    
    $sort_flag  = sort_flag($list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);

    assign_query_info();
    $smarty->display('mail_sent_log.htm');
}
elseif ($_REQUEST['act'] == 'query')
{
    /* 检查权限 */
    check_authz_json('mail_sent_log');
    
    $list = get_sent_log();
    $smarty->assign('list',         $list['data']);
    $smarty->assign('filter',       $list['filter']);
    $smarty->assign('record_count', $list['record_count']);
    $smarty->assign('page_count',   $list['page_count']);

    $sort_flag  = sort_flag($list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);

    make_json_result($smarty->fetch('mail_sent_log.htm'), '', array('filter' => $list['filter'], 'page_count' => $list['page_count']));
}
elseif (in_array($_REQUEST['act'], array('view','resend','sendtome')))
{
    /* 检查权限 */
    admin_priv('mail_sent_log');

    $id = empty($_REQUEST['id']) ? 0 : intval($_REQUEST['id']);
    
    if ($id <= 0)
    {
        sys_msg('找不到該郵件', 1);
    }
    
    $mail = array_pop($db->getAll('SELECT * FROM ' . $ecs->table('sent_mails') . ' WHERE sent_id = ' . $id));
    
    if ($_REQUEST['act'] == 'view')
    {
        if ($mail['is_html'])
        {
            header('Content-Type: text/html;charset=utf-8');
        }
        else
        {
            header('Content-Type: text/plain;charset=utf-8');
        }
        
        if ($mail['is_html']) echo '<pre>';
        echo 'To: ' . ($mail['to_name'] ? $mail['to_name'] . ' &lt;' . $mail['to_email'] . '&gt;' : $mail['to_email']) . "\n";
        echo 'Date: ' . local_date($_CFG['time_format'], $mail['sent_time']) . "\n";
        echo 'Subject: ' . $mail['subject'] . "\n";
        echo "\n";
        if ($mail['is_html']) echo '</pre>';
        echo $mail['content'];
        exit;
    }
    else if ($_REQUEST['act'] == 'resend')
    {
        define('BYPASS_MAIL_LOG',true);
        $success = send_mail($mail['to_name'], $mail['to_email'], $mail['subject'], $mail['content'], $mail['is_html']);
        if ($success)
        {
            sys_msg('重新發送郵件成功');
        }
        else
        {
            sys_msg('重新發送郵件失敗', 1);
        }
    }
    else if ($_REQUEST['act'] == 'sendtome')
    {
        define('BYPASS_MAIL_LOG',true);
        $email = empty($_REQUEST['email']) ? '' : $_REQUEST['email'];
        if (empty($email))
        {
            sys_msg('請輸入電郵地址', 1);
        }
        $success = send_mail($mail['to_name'], $email, '[副本] ' . $mail['subject'], $mail['content'], $mail['is_html']);
        if ($success)
        {
            sys_msg('發送郵件副本到 ' . $email . ' 成功');
        }
        else
        {
            sys_msg('發送郵件副本到 ' . $email . ' 失敗', 1);
        }
    }
}


function get_sent_log()
{
    $result = get_filter();
    if ($result === false)
    {
        $filter['sort_by']      = empty($_REQUEST['sort_by']) ? 'sent_id' : trim($_REQUEST['sort_by']);
        $filter['sort_order']   = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);

        $sql = "SELECT count(*) FROM " . $GLOBALS['ecs']->table('sent_mails');
        $filter['record_count'] = $GLOBALS['db']->getOne($sql);

        /* 分页大小 */
        $filter = page_and_size($filter);

        /* 查询 */
        $sql = "SELECT * ";
        $sql.= " FROM " . $GLOBALS['ecs']->table('sent_mails');
        $sql.= " ORDER by " . $filter['sort_by'] . ' ' . $filter['sort_order'];
        $sql.= " LIMIT " . $filter['start'] . ",$filter[page_size]";
        set_filter($filter, $sql);
    }
    else
    {
        $sql    = $result['sql'];
        $filter = $result['filter'];
    }

    $data = $GLOBALS['db']->getAll($sql);
    
    foreach ($data as $key => $row)
    {
        $data[$key]['content_display'] = $row['is_html'] ? preg_replace('/(\s)+/', ' ', strip_tags($row['content'])) : nl2br($row['content']);
        if(mb_strlen($data[$key]['content_display'], 'UTF-8') > 100)
        {
            $data[$key]['content_display'] = mb_substr($data[$key]['content_display'], 0, 97, 'UTF-8') . '...';
        }
        $data[$key]['sent_date'] = local_date($GLOBALS['_CFG']['time_format'], $row['sent_time']);
    }

    $arr = array('data' => $data, 'filter' => $filter, 'page_count' => $filter['page_count'], 'record_count' => $filter['record_count']);

    return $arr;
}
?>