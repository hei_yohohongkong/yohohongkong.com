<?php
define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require_once(ROOT_PATH . 'includes/lib_order.php');
require_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_goods.php');
require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/admin/statistic.php');

$smarty->assign('lang', $_LANG);
$userController = new Yoho\cms\Controller\UserController();
$erpController = new Yoho\cms\Controller\ErpController();
$warehouseStockController = new Yoho\cms\Controller\WarehouseStockController();
if(isset($_REQUEST['act'])){
    $sql = "SELECT warehouse_id AS wh_id, name AS wh_name FROM ".$ecs->table('erp_warehouse')." ".
            "WHERE is_valid = 1 AND is_on_sale = 1 ".
            "ORDER BY wh_id ASC";
    $temp_warehouse_list = $db->getAll($sql);
    $warehouse_list = [];
    foreach ($temp_warehouse_list as $wh){
        $warehouse_list[$wh['wh_id']] = $wh['wh_name'];
    }

    $sql = "SELECT warehouse_id AS wh_id FROM ".$ecs->table('erp_warehouse')." ".
            "WHERE is_valid = 1 AND is_main = 1";
    $main_warehouse = $db->getOne($sql);

    if (empty($main_warehouse)){
        $main_wh_id = 8;
    } else {
        $main_wh_id = $main_warehouse;
    }
    $wh_id = empty($_REQUEST['wh_id']) ? $main_wh_id : intval($_REQUEST['wh_id']);

    $_REQUEST['on_sale_only'] = (!isset($_REQUEST['on_sale_only']) ? TRUE : ($_REQUEST['on_sale_only'] == 1 ? TRUE : FALSE));

    $smarty->assign('wh_id',       $wh_id);
    $smarty->assign('warehouse_list', $warehouse_list);
    $smarty->assign('on_sale_only', $_REQUEST['on_sale_only']);
    if($_REQUEST['act'] == 'query'){
        if (isset($_REQUEST['start_date']) && strstr($_REQUEST['start_date'], '-') === false)
        {
            $_REQUEST['start_date'] = local_date('Y-m-d', $_REQUEST['start_date']);
            $_REQUEST['end_date'] = local_date('Y-m-d', $_REQUEST['end_date']);
        }
        
        $_REQUEST['order_type'] = '4';
        $_REQUEST['deprecatedflow'] = 'N';
        $data = $warehouseStockController->get_sale_warehouse(false, '', $wh_id);

        $smarty->assign('data',            $data['data']);
        $smarty->assign('total_discount',  $data['total_discount']);
        $smarty->assign('start_date',      $_REQUEST['start_date']);
        $smarty->assign('end_date',        $_REQUEST['end_date']);

        $warehouseStockController->ajaxQueryAction($data['data'], $data['record_count'], false);

        make_json_result($smarty->fetch('warehouse_turnover.htm'), '');
    }else if($_REQUEST['act'] == 'list'){
        if (!isset($_REQUEST['start_date']))
        {
            $start_date = strtotime(month_start());
        }
        if (!isset($_REQUEST['end_date']))
        {
            $end_date = strtotime(month_end());
        }
        
        $smarty->assign('manage_cost',  $_SESSION['manage_cost']);
        $smarty->assign('is_accountant',  $_SESSION['is_accountant']);
		$_REQUEST['order_type'] = '4';
        $_REQUEST['deprecatedflow'] = 'N';
        $supplier_list = $GLOBALS['db']->getAll("SELECT supplier_id, name as supplier_name FROM " . $GLOBALS['ecs']->table('erp_supplier'));
        $smarty->assign('supplier_list',  $supplier_list);
        $smarty->assign('ur_here',        $_LANG['warehouse_turnover']);
        //$smarty->assign('total_discount', $data['total_discount']);
        $smarty->assign('full_page',      1);
        $smarty->assign('start_date',     local_date('Y-m-d', $start_date));
        $smarty->assign('end_date',       local_date('Y-m-d', $end_date));
        $smarty->assign('data',           $data['data']);
        $smarty->assign('cfg_lang',       $_CFG['lang']);
        $smarty->assign('date_today',  local_date('Y-m-d', local_strtotime('today')));
        $smarty->assign('7_date_before',  local_date('Y-m-d', local_strtotime('-7 days')));
        $smarty->assign('30_date_before',  local_date('Y-m-d', local_strtotime('-30 days')));
        $smarty->display('warehouse_turnover.htm');
    }
}

function month_start()
{
	return local_date('Y-m-01');
}
function month_end()
{
	return local_date('Y-m-t');
}
?>