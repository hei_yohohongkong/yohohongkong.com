<?php

define('IN_ECS', true);
require dirname(__FILE__) . '/includes/init.php';
require_once ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_accounting.php';

$accountingController = new Yoho\cms\Controller\AccountingController();

if ($_REQUEST['act'] == 'list') {
    if (check_authz(Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_POSTING_OP)) {
        $smarty->assign('posting_approve', 1);
    }
    $action_link_list = [
        [
            'text' => $_LANG['accounting_types'],
            'href' => 'accounting_system_type.php?act=list'
        ],
        [
            'text' => $_LANG['accounting_transaction_posted_list'],
            'href' => 'accounting_system.php?act=posted_list'
        ],
        [
            'text' => $_LANG['accounting_insert_txn'],
            'href' => 'accounting_system.php?act=insert'
        ],
    ];
    $smarty->assign('ur_here', $_LANG['accounting_system']);
    $smarty->assign('action_link_list', $action_link_list);
    $smarty->assign('system_user', $accountingController::ACTG_SYSTEM_USER);
    $smarty->assign('operators', $accountingController->getAccountingOperators());
    $smarty->assign('txn_types', $accountingController->getAccountingTypes(0, false));
    $smarty->assign('suppliers', $accountingController->getAccountingSuppliersList());
    $smarty->assign('wholesales', $accountingController->getAccountingWholesaleCustomerList());
    $smarty->assign('salesagents', $accountingController->getAccountingSalesAgentList());
    $smarty->display('accounting_transaction_list.htm');
} elseif ($_REQUEST['act'] == 'query') {
    $res = $accountingController->getAccountingTransactions();
    $accountingController->ajaxQueryAction($res);
} elseif ($_REQUEST['act'] == 'insert') {
    $action_link_list = [
        [
            'text' => $_LANG['accounting_system'],
            'href' => 'accounting_system.php?act=list'
        ],
    ];
    $smarty->assign('ur_here', $_LANG['accounting_insert_txn']);
    $smarty->assign('action_link_list', $action_link_list);
    $smarty->assign('txn_types', $accountingController->getAccountingTypes(0, false));
    $smarty->assign('currencies', $actg->getCurrencies());
    $smarty->display('accounting_transaction_insert.htm');
} elseif ($_REQUEST['act'] == 'update') {
    $debit = [];
    $credit = [];
    $sum = 0;

    foreach (array_unique($_REQUEST['txn_currency']) as $code) {
        $currencies[$code] = $actg->getCurrency(['currency_code' => $code]);
    }
    foreach ($_REQUEST['txn_type_ids'] as $key => $txn_type_id) {
        if (!empty($txn_type_id)) {
            $currency_code = trim($_REQUEST['txn_currency'][$key]);
            if (bccomp(floatval($_REQUEST['debit_txn_amount'][$key]), 0, 2) == 1) {
                $amount = floatval($_REQUEST['debit_txn_amount'][$key]);
                $exchanged_amount = floatval($_REQUEST['exchanged_value'][$key]);
                $debit[] = [
                    'txn_type_id'   => $txn_type_id,
                    'amount'        => $amount,
                    'currency_code' => $currency_code,
                    'exchanged_amount' => $exchanged_amount,
                ];
            } elseif (bccomp(floatval($_REQUEST['credit_txn_amount'][$key]), 0, 2) == 1) {
                $amount = bcmul(floatval($_REQUEST['credit_txn_amount'][$key]), -1);
                $exchanged_amount = floatval($_REQUEST['exchanged_value'][$key]);
                $exchanged_amount = ( $exchanged_amount < 0 ) ? $exchanged_amount : ( $exchanged_amount * -1 );
                $credit[] = [
                    'txn_type_id'   => $txn_type_id,
                    'amount'        => $amount,
                    'currency_code' => $currency_code,
                    'exchanged_amount' => $exchanged_amount,
                ];
            }
            $sum = bcadd($sum, $exchanged_amount, 2);
        }
    }

    if (bccomp($sum, 0, 2) != 0) {
        sys_msg($_LANG['mismatch_amount'], 1);
    }
    if (empty($debit) || empty($credit)) {
        sys_msg($_LANG['empty_record'], 1);
    }

    $related_txn = 0;
    $actg->start_transaction();
    foreach (array_merge($debit, $credit) as $txn) {
        $param = [
            'actg_type_id'      => $txn['txn_type_id'],
            'currency_code'     => $txn['currency_code'],
            'amount'            => $txn['amount'],
            'exchanged_amount'  => $txn['exchanged_amount'],
            'txn_date'          => trim($_REQUEST['txn_date']),
            'remark'            => trim($_REQUEST['remark']),
            'created_by'        => intval($_REQUEST['create_by']),
            'actg_month'        => "$_REQUEST[actg_month_year]/$_REQUEST[actg_month_month]",
        ];
        if (!empty($related_txn)) {
            $param['related_txn_id'] = $related_txn;
        }

        $txn_id = $accountingController->insertTransaction($param);
        if (!$txn_id) {
            $actg->rollback_transaction();
            sys_msg($_LANG['insert_fail'], 1);
        }
        if (empty($related_txn)) {
            $related_txn = $txn_id;
        }
    }
    $actg->commit_transaction();
    sys_msg($_LANG['insert_success'], 0, [
        [
            'text' => $_LANG['accounting_system'],
            'href' => 'accounting_system.php?act=list'
        ]
    ]);
} elseif ($_REQUEST['act'] == 'post') {
    $accountingController->postAccountingTransactions();
} elseif ($_REQUEST['act'] == 'posted_list') {
    $action_link_list = [
        [
            'text' => $_LANG['accounting_system'],
            'href' => 'accounting_system.php?act=list'
        ],
    ];
    $smarty->assign('posted', 1);
    $smarty->assign('ur_here', $_LANG['accounting_transaction_posted_list']);
    $smarty->assign('action_link_list', $action_link_list);
    $smarty->assign('operators', $accountingController->getAccountingOperators(true));
    $smarty->display('accounting_transaction_list.htm');
} elseif ($_REQUEST['act'] == 'posted_query') {
    $res = $accountingController->getPostedAccountingTransactions();
    $accountingController->ajaxQueryAction($res);
}

?>