<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require_once(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require_once(dirname(__FILE__) .'/includes/ERP/lib_erp_finance.php');
require_once(dirname(__FILE__) .'/includes/ERP/lib_erp_supplier.php');
require_once(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
if ($_REQUEST['act'] == 'gather')
{
include('../includes/cls_json.php');
$json  = new JSON;
if(!admin_priv('erp_finance_manage','',false))
{
$result['error']=1;
$result['message']=$_LANG['erp_no_permit'];
die($json->encode($result));
}
$order_id=isset($_REQUEST['order_id']) &&intval($_REQUEST['order_id'])>0?intval($_REQUEST['order_id']):0;
$gather_amount=isset($_REQUEST['gather_amount']) &&floatval($_REQUEST['gather_amount'])>0?floatval($_REQUEST['gather_amount']):0;
$gathering_id=create_gathering($order_id,$gather_amount);
if($gathering_id==-1)
{
$result['error']=2;
$result['message']=$GLOBALS['_LANG']['erp_receivable_no_agency_access'];
die($json->encode($result));
}
if($gathering_id==-2)
{
$result['error']=2;
$result['message']=$GLOBALS['_LANG']['erp_receivable_no_valid_bank_account'];
die($json->encode($result));
}
if(lock_gathering($gathering_id,'edit'))	
{
if(gathering_act_record($gathering_id,'modify'))
{
$result['error']=0;
die($json->encode($result));
}
}
else{
$result['error']=-1;
$result['message']=$_LANG['erp_gathering_no_accessibility'];
die($json->encode($result));
}
}
if($_REQUEST['act'] == 'withdrawal_to_edit')
{
include('../includes/cls_json.php');
$json  = new JSON;
if(!admin_priv('erp_finance_approve','',false))
{
$result['error']=1;
$result['message']=$_LANG['erp_no_permit'];
die($json->encode($result));
}
$gathering_id=isset($_REQUEST['gathering_id']) &&intval($_REQUEST['gathering_id'])>0?intval($_REQUEST['gathering_id']):0;
$sql="update ".$GLOBALS['ecs']->table('erp_gathering')." set gathering_status='1' where gathering_id='".$gathering_id."'";
$GLOBALS['db']->query($sql);
$result['error']=0;
die($json->encode($result));
}
?>
