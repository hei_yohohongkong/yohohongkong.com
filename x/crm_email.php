<?php
define('IN_ECS', true);

require dirname(__FILE__) . '/includes/init.php';
require_once ROOT_PATH . 'includes/lib_order.php';
require_once ROOT_PATH . 'includes/Google_api_client/vendor/autoload.php';

$user = 'info@yohohongkong.com';

// function getHeader($headers, $name) {
//     foreach($headers as $header) {
//         if($header['name'] == $name) {
//             return $header['value'];
//         }
//     }
// }

// putenv("GOOGLE_APPLICATION_CREDENTIALS=$KEY_FILE_LOCATION");
// $client = new Google_Client();
// $client->useApplicationDefaultCredentials();
// $client->setScopes([Google_Service_Gmail::MAIL_GOOGLE_COM]);
// $client->setSubject($user);
// $client->setCacheConfig(['lifetime' => 0]);
// $service = new Google_Service_Gmail($client);

// $results = $service->users_messages->listUsersMessages($user);
// foreach($results as $mail){
//    $message = $service->users_messages->get($user, $mail['id']);
//     $headers = $message->getPayload()->getHeaders();
//     $subject = getHeader($headers, 'Subject');
//     echo $subject."\n";
// }

if (isset($_SESSION['google_access_token']) && isset($_SESSION['google_expiry']) && intval($_SESSION['google_expiry']) > gmtime()) {
    $access_token = $_SESSION['google_access_token'];
} else {
    $KEY_FILE_LOCATION =  ROOT_PATH . 'data/yoho_service_account.json';
    $client = new Google_Client();
    $client->setApplicationName("Yoho Gmail API");
    $client->setAuthConfig($KEY_FILE_LOCATION);
    $client->setScopes(['https://mail.google.com/']);
    $client->useApplicationDefaultCredentials();
    $client->fetchAccessTokenWithAssertion();
    $client->setSubject($user);
    
    $token = $client->getAccessToken();
    $access_token = $token['access_token'];
    $expiry = gmtime() + $token['expires_in'];
    $_SESSION['google_access_token'] = $access_token;
    $_SESSION['google_expiry'] = $expiry;
}


// Get the API client and construct the service object.
// $client = getClient();
// $service = new Google_Service_Gmail($client);

// Print the labels in the user's account.
// $user = 'me';
// $results = $service->users_labels->listUsersLabels($user);

// if (count($results->getLabels()) == 0) {
//     print "No labels found.\n";
// } else {
//     print "Labels:\n";
//     foreach ($results->getLabels() as $label) {
//         printf("- %s\n", $label->getName());
//     }
// }
?>