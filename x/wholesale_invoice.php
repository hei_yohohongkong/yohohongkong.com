<?php
define('IN_ECS', true);

require dirname(__FILE__) . '/includes/init.php';
require_once ROOT_PATH . 'includes/lib_order.php';
$template_types = ["globiz_30_template", "globiz_fp_template", "yoho_30_template", "yoho_fp_template"];
$template_dn_types = ["globiz_dn_template", "yoho_dn_template"];

if ($_REQUEST['act'] == "list") {
    $sql = "SELECT * FROM " . $ecs->table("wholesale_customer") . (!empty($_REQUEST['json']) ? " ORDER BY company ASC" : "");
    $res = $db->getAll($sql);
    if (!empty($_REQUEST['json'])) {
        $arr = array(
            'ws_list' => $res,
            't_list'  => ['Globiz - 30日內付款', 'Globiz - 出貨前付款', 'Yoho - 30日內付款', 'Yoho - 出貨前付款']
        );
        make_json_response($arr);
    }
    $users = $db->getAll("SELECT user_id, user_name, wsid FROM " . $ecs->table('users') . " WHERE wsid != 0");
    foreach ($users as $user) {
        $ws_users[$user['wsid']][] = $user;
    }
    foreach ($res as $key => $row) {
        $res[$key]['users'] = $ws_users[$row['wsid']];
    }
    $smarty->assign('action_link',  array('text' => $_LANG['wholesale_user_info_insert'], 'href' => 'wholesale_invoice.php?act=add'));
    $smarty->assign('action_link2',  array('text' => $_LANG['04_users_add'], 'href'=>'users.php?act=add&is_wholesale=1'));
    $smarty->assign("list", $res);
    $smarty->assign("ur_here", $_LANG["wholesale_user_info"]);
    assign_query_info();
    $smarty->display("wholesale_invoice.htm");
} elseif ($_REQUEST['act'] == "add") {
    admin_priv('wholesale_user_manage');
    $smarty->assign('action_link',  array('text' => $_LANG['wholesale_user_info'], 'href' => 'wholesale_invoice.php?act=list'));
    $smarty->assign("ur_here", $_LANG["wholesale_user_info_insert"]);
    $smarty->display("wholesale_invoice.htm");
} elseif ($_REQUEST['act'] == "insert") {
    admin_priv('wholesale_user_manage');
    $company = empty($_POST["company"]) ? "" : $_POST["company"];
    $name = empty($_POST["name"]) ? "" : $_POST["name"];
    $phone = empty($_POST["phone"]) ? "" : $_POST["phone"];
    $address = empty($_POST["address"]) ? "" : $_POST["address"];
    $user_list = empty($_POST["user_list"]) ? [] : array_unique($_POST["user_list"]);

    if (empty($company)) {
        sys_msg("請輸入公司資料");
    }
    $sql = "INSERT INTO " . $ecs->table("wholesale_customer") . " (company, name, phone, address) VALUES ('$company', '$name', '$phone', '$address')";
    if ($db->query($sql)) {
        $wsid = $db->insert_id();
        if (!empty($user_list)) {
            $db->query("UPDATE " . $ecs->table("users") . " SET wsid = $wsid WHERE user_id " . db_create_in($user_list));
        }
        sys_msg("新增成功", 0, array(array('text' => $_LANG['wholesale_user_info'], 'href' => 'wholesale_invoice.php?act=list')));
    } else {
        sys_msg("新增失敗", 1);        
    }
} elseif ($_REQUEST['act'] == 'edit_company' || $_REQUEST['act'] == 'edit_name' || $_REQUEST['act'] == 'edit_phone' || $_REQUEST['act'] == 'edit_address') {
    $exc = new exchange($ecs->table('wholesale_customer'), $db, 'wsid', 'company');
    $act = str_replace("edit_", "", $_REQUEST['act']);
    $wsid = intval($_POST['id']);
    $val  = $_POST['val'];
    if ($act == "company" && empty($val)) {
        make_json_error('請輸入公司名稱');
    }
    if ($exc->edit("$act = '$val'", $wsid)) {
        make_json_result($val);
    }

} elseif ($_REQUEST['act'] == "print") {
    if (!extension_loaded('zip')) {
        sys_msg("請先安裝 / 啟用PHP Zip模組");
    }
    $order_id = $_REQUEST['order_id'];
    $wsid = $_REQUEST['wsid'];
    $template_id = empty($_REQUEST['template']) && $_REQUEST['template'] != 0 ? -1 : intval($_REQUEST['template']);
    if (empty($order_id)) {
        sys_msg("請選擇訂單");
    }
    if (empty($wsid)) {
        sys_msg("請選擇批發顧客");
    }
    if ($template_id == -1) {
        sys_msg("請選擇模板");
    }
    $order_info = order_info($order_id);
    if ($order_info['order_type'] != "ws") {
        sys_msg("非採購訂單無法使用本功能");
    }
    $order_goods = order_goods($order_id);
    $customer = $db->getRow("SELECT * FROM " . $ecs->table("wholesale_customer") . " WHERE wsid = $wsid");
    if (empty($customer)) {
        sys_msg("沒有此批發顧客");        
    }
    ini_set('memory_limit', '256M');
    define('PCLZIP_TEMPORARY_DIR', ROOT_PATH . 'temp/');
    $precision = ini_get('precision');

    include_once ROOT_PATH . 'includes/excel/PHPExcel.php';
    include_once ROOT_PATH . 'includes/excel/PHPExcel/IOFactory.php';
    \PHPExcel_Settings::setZipClass(\PHPExcel_Settings::PCLZIP);
    $template_type = $template_types[$template_id];
    $fileName     = ROOT_PATH . "data/wholesale_invoice/$template_type.xlsx";

    $newFile = "INVOICE_$order_info[order_sn].xlsx";
    $fileType = \PHPExcel_IOFactory::identify($fileName);
    $objReader    = \PHPExcel_IOFactory::createReader($fileType);
    $objPHPExcel  = $objReader->load($fileName);
    $objWorksheet = $objPHPExcel->getSheet(0);

    

    if (in_array($template_type, array("globiz_30_template", "globiz_fp_template"))) {

        // 1  Globiz 2 Yoho Hong Kong
        $ws_company_id = 1;

        $objWorksheet->setCellValueByColumnAndRow(3, 4, local_date("M j, Y"));
        $objWorksheet->setCellValueByColumnAndRow(3, 5, $order_info['order_sn']);
        $objWorksheet->setCellValueByColumnAndRow(3, 7, $customer['company']);
        $objWorksheet->setCellValueByColumnAndRow(3, 8, $customer['name']);
        $objWorksheet->setCellValueByColumnAndRow(3, 9, $customer['phone']);
        $objWorksheet->setCellValueByColumnAndRow(0, 10, $customer['address']);
        $objWorksheet->setCellValueByColumnAndRow(4, 24, "=SUM(E12:E23)");

        if (count($order_goods) > 12) {
            $objPHPExcel->getActiveSheet()->insertNewRowBefore(13, count($order_goods) - 10);
        }

        $currencyFormat = "_-\$* #,##0_ ;_-\$* \-#,##0\ ;_-\$* \-_ ;_-@_";
        foreach ($order_goods as $key => $goods) {
            $row = 12 + $key;
            $objWorksheet->setCellValueByColumnAndRow(0, $row, $goods['goods_name']);
            $objWorksheet->setCellValueByColumnAndRow(1, $row, $goods['goods_number']);
            $objWorksheet->setCellValueByColumnAndRow(2, $row, $goods['market_price']);
            $objWorksheet->setCellValueByColumnAndRow(3, $row, $goods['goods_price']);
            $objWorksheet->setCellValueByColumnAndRow(4, $row, "=B$row*D$row");
            $objWorksheet->getStyleByColumnAndRow(2, $row)->getNumberFormat()->setFormatCode($currencyFormat);
            $objWorksheet->getStyleByColumnAndRow(3, $row)->getNumberFormat()->setFormatCode($currencyFormat);
            $objWorksheet->getStyleByColumnAndRow(4, $row)->getNumberFormat()->setFormatCode($currencyFormat);
        }
    } elseif (in_array($template_type, array("yoho_30_template", "yoho_fp_template"))) {

        // 1  Globiz 2 Yoho Hong Kong
        $ws_company_id = 1;

        $objWorksheet->setCellValueByColumnAndRow(2, 3, local_date("M j, Y"));
        $objWorksheet->setCellValueByColumnAndRow(2, 4, $order_info['order_sn']);
        $objWorksheet->setCellValueByColumnAndRow(2, 6, $customer['company']);
        $objWorksheet->setCellValueByColumnAndRow(2, 7, $customer['name']);
        $objWorksheet->setCellValueByColumnAndRow(2, 8, $customer['phone']);
        $objWorksheet->setCellValueByColumnAndRow(1, 9, $customer['address']);
        $objWorksheet->setCellValueByColumnAndRow(4, 24, "=SUM(E12:E23)");

        if (count($order_goods) > 12) {
            $objPHPExcel->getActiveSheet()->insertNewRowBefore(13, count($order_goods) - 10);
        }

        $currencyFormat = "_-\$* #,##0_ ;_-\$* \-#,##0\ ;_-\$* \-_ ;_-@_";
        foreach ($order_goods as $key => $goods) {
            $row = 12 + $key;
            $objWorksheet->setCellValueByColumnAndRow(0, $row, $goods['goods_name']);
            $objWorksheet->setCellValueByColumnAndRow(1, $row, $goods['goods_number']);
            $objWorksheet->setCellValueByColumnAndRow(2, $row, $goods['market_price']);
            $objWorksheet->setCellValueByColumnAndRow(3, $row, $goods['goods_price']);
            $objWorksheet->setCellValueByColumnAndRow(4, $row, "=B$row*D$row");
            $objWorksheet->getStyleByColumnAndRow(2, $row)->getNumberFormat()->setFormatCode($currencyFormat);
            $objWorksheet->getStyleByColumnAndRow(3, $row)->getNumberFormat()->setFormatCode($currencyFormat);
            $objWorksheet->getStyleByColumnAndRow(4, $row)->getNumberFormat()->setFormatCode($currencyFormat);
        }
    }elseif ($template_type == "yoho_esd_template") {
        // 1  Globiz 2 Yoho Hong Kong
        $ws_company_id = 2;

        $objWorksheet->setCellValueByColumnAndRow(5, 4, "INV$order_info[order_sn]");
        $objWorksheet->setCellValueByColumnAndRow(7, 4, local_date("M j, Y"));
        $objWorksheet->setCellValueByColumnAndRow(0, 7, $customer['company']);
        $objWorksheet->setCellValueByColumnAndRow(7, 30, "=SUM(H14:H27)");

        if (count($order_goods) > 12) {
            $objPHPExcel->getActiveSheet()->insertNewRowBefore(15, count($order_goods) - 10);
        }

        $currencyFormat = "_(* #,##0.00_);_(* \(#,##0.00\);_(* \-??_);_(@_)";
        foreach ($order_goods as $key => $goods) {
            $row = 14 + $key;
            $objWorksheet->mergeCells("A$row:E$row");
            $objWorksheet->setCellValueByColumnAndRow(0, $row, $goods['goods_name']);
            $objWorksheet->setCellValueByColumnAndRow(5, $row, $goods['goods_number']);
            $objWorksheet->setCellValueByColumnAndRow(6, $row, $goods['goods_price']);
            $objWorksheet->setCellValueByColumnAndRow(7, $row, "=F$row*G$row");
            $objWorksheet->getStyleByColumnAndRow(6, $row)->getNumberFormat()->setFormatCode($currencyFormat);
            $objWorksheet->getStyleByColumnAndRow(7, $row)->getNumberFormat()->setFormatCode($currencyFormat);
        }
    } elseif ($template_type == "yoho_ny_template") {
        // 1  Globiz 2 Yoho Hong Kong
        $ws_company_id = 2;

        $objWorksheet->setCellValueByColumnAndRow(2, 3, local_date("M j, Y"));
        $objWorksheet->setCellValueByColumnAndRow(2, 4, $order_info['order_sn']);
        $objWorksheet->setCellValueByColumnAndRow(2, 6, $customer['company']);
        $objWorksheet->setCellValueByColumnAndRow(2, 7, $customer['name']);
        $objWorksheet->setCellValueByColumnAndRow(2, 8, $customer['phone']);
        $objWorksheet->setCellValueByColumnAndRow(1, 9, $customer['address']);
        $objWorksheet->setCellValueByColumnAndRow(4, 24, "=SUM(E12:E23)");

        if (count($order_goods) > 12) {
            $objPHPExcel->getActiveSheet()->insertNewRowBefore(13, count($order_goods) - 10);
        }

        $currencyFormat = "_(* #,##0.00_);_(* \(#,##0.00\);_(* \-??_);_(@_)";
        foreach ($order_goods as $key => $goods) {
            $row = 12 + $key;
            $objWorksheet->setCellValueByColumnAndRow(0, $row, $goods['goods_name']);
            $objWorksheet->setCellValueByColumnAndRow(1, $row, $goods['goods_number']);
            $objWorksheet->setCellValueByColumnAndRow(2, $row, $goods['market_price']);
            $objWorksheet->setCellValueByColumnAndRow(3, $row, $goods['goods_price']);
            $objWorksheet->setCellValueByColumnAndRow(4, $row, "=B$row*D$row");
            $objWorksheet->getStyleByColumnAndRow(2, $row)->getNumberFormat()->setFormatCode($currencyFormat);
            $objWorksheet->getStyleByColumnAndRow(3, $row)->getNumberFormat()->setFormatCode($currencyFormat);
            $objWorksheet->getStyleByColumnAndRow(4, $row)->getNumberFormat()->setFormatCode($currencyFormat);
        }
    }



    header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.‌​sheet; charset=utf-8');
    header('Content-Disposition: attachment; filename=' . $newFile);
    header('Cache-Control: max-age=0');
    $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, $fileType);
    $objWriter->setPreCalculateFormulas(true);
    $objWriter->save('php://output');
} elseif ($_REQUEST['act'] == "dn_export") {
    if (!extension_loaded('zip')) {
        sys_msg("請先安裝 / 啟用PHP Zip模組");
    }
    $delivery_id = $_REQUEST['delivery_id'];
    $wsid = $_REQUEST['wsid'];

    $sql = "select * from ".$ecs->table("delivery_order")." oi where delivery_id = ".$delivery_id. "";
    $deliveryInfo = $db->getRow($sql);

    $sql = "select * from ".$ecs->table("order_info")." oi where order_id = ".$deliveryInfo['order_id']. "";
    $orderInfo = $db->getRow($sql);
    
    // 1  Globiz 2 Yoho Hong Kong
    if ($orderInfo['ws_company_id'] == 1) {
        $template_id = 0;
    } else {
        $template_id = 1;
    }

    $wsid = $orderInfo['ws_customer_id'];

    $order_id = $deliveryInfo['order_id'];

    $order_info = order_info($order_id);
    if ($order_info['order_type'] != "ws") {
        sys_msg("非採購訂單無法使用本功能");
    }
    //$order_goods = order_goods($order_id);
    $deliveryOrderController = new Yoho\cms\Controller\DeliveryOrderController();
    $order_goods = $deliveryOrderController->getDeliveryOrderGoods($delivery_id);

    $customer = $db->getRow("SELECT * FROM " . $ecs->table("wholesale_customer") . " WHERE wsid = $wsid");
    if (empty($customer)) {
        sys_msg("沒有此批發顧客");        
    }
    ini_set('memory_limit', '256M');
    define('PCLZIP_TEMPORARY_DIR', ROOT_PATH . 'temp/');
    $precision = ini_get('precision');

    include_once ROOT_PATH . 'includes/excel/PHPExcel.php';
    include_once ROOT_PATH . 'includes/excel/PHPExcel/IOFactory.php';
    \PHPExcel_Settings::setZipClass(\PHPExcel_Settings::PCLZIP);
    $template_type = $template_dn_types[$template_id];
    $fileName     = ROOT_PATH . "data/wholesale_invoice/$template_type.xlsx";

    $newFile = "DELIVERY_$order_info[order_sn].xlsx";
    $fileType = \PHPExcel_IOFactory::identify($fileName);
    $objReader    = \PHPExcel_IOFactory::createReader($fileType);
    $objPHPExcel  = $objReader->load($fileName);
    $objWorksheet = $objPHPExcel->getSheet(0);

    if (in_array($template_type, array("globiz_dn_template"))) {
        $objWorksheet->setCellValueByColumnAndRow(3, 4, local_date("M j, Y"));
        $objWorksheet->setCellValueByColumnAndRow(3, 5, $order_info['order_sn']);
        $objWorksheet->setCellValueByColumnAndRow(3, 7, $customer['company']);
        $objWorksheet->setCellValueByColumnAndRow(3, 8, $customer['name']);
        $objWorksheet->setCellValueByColumnAndRow(3, 9, $customer['phone']);
        $objWorksheet->setCellValueByColumnAndRow(0, 10, $customer['address']);
        // $objWorksheet->setCellValueByColumnAndRow(4, 24, "=SUM(E12:E23)");

        if (count($order_goods) > 12) {
            $objPHPExcel->getActiveSheet()->insertNewRowBefore(13, count($order_goods) - 10);
        }

        $currencyFormat = "_-\$* #,##0_ ;_-\$* \-#,##0\ ;_-\$* \-_ ;_-@_";
        foreach ($order_goods as $key => $goods) {
            $row = 12 + $key;
            $objWorksheet->setCellValueByColumnAndRow(0, $row, $goods['goods_name']);
            $objWorksheet->setCellValueByColumnAndRow(1, $row, $goods['send_number']);
            //$objWorksheet->setCellValueByColumnAndRow(2, $row, $goods['market_price']);
            //$objWorksheet->setCellValueByColumnAndRow(3, $row, $goods['goods_price']);
            //$objWorksheet->setCellValueByColumnAndRow(4, $row, "=B$row*D$row");
            $objWorksheet->getStyleByColumnAndRow(2, $row)->getNumberFormat()->setFormatCode($currencyFormat);
            $objWorksheet->getStyleByColumnAndRow(3, $row)->getNumberFormat()->setFormatCode($currencyFormat);
            $objWorksheet->getStyleByColumnAndRow(4, $row)->getNumberFormat()->setFormatCode($currencyFormat);
        }
    } else if (in_array($template_type, array("yoho_dn_template"))) {
        $objWorksheet->setCellValueByColumnAndRow(2, 3, local_date("M j, Y"));
        $objWorksheet->setCellValueByColumnAndRow(2, 4, $order_info['order_sn']);
        $objWorksheet->setCellValueByColumnAndRow(2, 6, $customer['company']);
        $objWorksheet->setCellValueByColumnAndRow(2, 7, $customer['name']);
        $objWorksheet->setCellValueByColumnAndRow(2, 8, $customer['phone']);
        $objWorksheet->setCellValueByColumnAndRow(1, 9, $customer['address']);
        // $objWorksheet->setCellValueByColumnAndRow(4, 24, "=SUM(E12:E23)");

        if (count($order_goods) > 12) {
            $objPHPExcel->getActiveSheet()->insertNewRowBefore(13, count($order_goods) - 10);
        }

        $currencyFormat = "_-\$* #,##0_ ;_-\$* \-#,##0\ ;_-\$* \-_ ;_-@_";
        foreach ($order_goods as $key => $goods) {
            $row = 12 + $key;
            $objWorksheet->setCellValueByColumnAndRow(0, $row, $goods['goods_name']);
            $objWorksheet->setCellValueByColumnAndRow(1, $row, $goods['send_number']);
            //$objWorksheet->setCellValueByColumnAndRow(2, $row, $goods['market_price']);
            //$objWorksheet->setCellValueByColumnAndRow(3, $row, $goods['goods_price']);
            //$objWorksheet->setCellValueByColumnAndRow(4, $row, "=B$row*D$row");
            $objWorksheet->getStyleByColumnAndRow(2, $row)->getNumberFormat()->setFormatCode($currencyFormat);
            $objWorksheet->getStyleByColumnAndRow(3, $row)->getNumberFormat()->setFormatCode($currencyFormat);
            $objWorksheet->getStyleByColumnAndRow(4, $row)->getNumberFormat()->setFormatCode($currencyFormat);
        }
    }

    header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.‌​sheet; charset=utf-8');
    header('Content-Disposition: attachment; filename=' . $newFile);
    header('Cache-Control: max-age=0');
    $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, $fileType);
    $objWriter->setPreCalculateFormulas(true);
    $objWriter->save('php://output');
} elseif ($_REQUEST['act'] == "search_user") {
    $result = [
        'users' => []
    ];
    $search = preg_replace('/\s/', '', trim($_REQUEST['user']));
    $wsid = trim($_REQUEST['wsid']);
    if (!empty($search)) {
        $result['users'] = $db->getAll("SELECT user_id, user_name FROM " . $ecs->table('users') . " WHERE user_name LIKE '%$search%' AND is_wholesale = 1 AND wsid = 0 ORDER BY user_id DESC LIMIT 10");
    }
    make_json_response($result);
} elseif ($_REQUEST['act'] == "bind_user") {
    $wsid = trim($_REQUEST['wsid']);
    $user_id = trim($_REQUEST['user_id']);
    if (empty($wsid) || empty($user_id)) {
        sys_msg('請選擇批發客戶');
    }
    $user_list = getWholesaleUserList($wsid);
    if (!in_array($user_id, $user_list)) {
        $db->query("UPDATE " . $ecs->table('users') . " SET wsid = $wsid WHERE user_id = $user_id");
    }
    sys_msg('更新成功');
} elseif ($_REQUEST['act'] == "unbind_user") {
    $wsid = trim($_REQUEST['wsid']);
    $user_id = trim($_REQUEST['user_id']);
    if (empty($wsid) || empty($user_id)) {
        sys_msg('請選擇批發客戶');
    }
    $user_list = getWholesaleUserList($wsid);
    if (($key = array_search($user_id, $user_list)) !== false) {
        $db->query("UPDATE " . $ecs->table('users') . " SET wsid = 0 WHERE user_id = $user_id");
    }
    sys_msg('更新成功');
} elseif ($_REQUEST['act'] == "create_ar") {
    $order_id = $_REQUEST['order_id'];
    $wsid = $_REQUEST['wsid'];
    if (empty($order_id)) {
        make_json_error("請選擇訂單");
    }
    if (empty($wsid)) {
        make_json_error("請選擇批發顧客");
    }
    $order_info = order_info($order_id);
    if ($order_info['order_type'] != "ws") {
        make_json_error("非採購訂單無法使用本功能");
    }
    if (!empty($order_info['ar_txn_id'])) {
        make_json_error("此訂單已建立AR");
    }
    $order_goods = order_goods($order_id);

    // Create confirmed account receivable
    $ar_amount = 0;
    foreach ($order_goods as $goods) {
        $ar_amount = bcadd($ar_amount, bcmul($goods['goods_price'], $goods['goods_number'], 2), 2);
    }

    $accountingController = new Yoho\cms\Controller\AccountingController();
    $total_paid = $accountingController->getTotalPaid($order_id);

    if (bccomp($ar_amount, $total_paid) != -1 && bccomp($ar_amount, 0) == 1) {
        $param = [
            'sales_order_id'    => $order_id,
            'wsid'              => $wsid,
            'amount'            => bcsub($ar_amount, $total_paid, 2),
            'order_sn'          => $order_info['order_sn'],
        ];
        $ar_txn_id = $accountingController->insertWSAccountReceivableTransactions($param);
        if ($ar_txn_id) {
            $db->query("UPDATE " . $ecs->table('order_info') . " SET ar_txn_id = $ar_txn_id WHERE order_id = $order_id");
            make_json_response(1);
        }
    }
    make_json_error("無法建立AR");
}

function getWholesaleUserList($wsid = 0) {
    global $db, $ecs;
    return $db->getCol("SELECT user_id FROM " . $ecs->table('users') . " WHERE wsid " . (empty($wsid) ? "!= 0" : "= $wsid"));
}
?>