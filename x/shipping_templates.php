<?php

define('IN_ECS', true);
require(dirname(__FILE__) . '/includes/init.php');
$shippingTemplatesController = new Yoho\cms\Controller\ShippingTemplatesController();
if ($_REQUEST['act'] == 'list')
{
    $smarty->assign('ur_here',      $_LANG['shipping_templates']);

    $smarty->assign('action_link', array('text' => '新增安裝條款模版', 'href' => 'shipping_templates.php?act=edit'));

    assign_query_info();
    $smarty->display('shipping_templates_list.htm');
}
elseif ($_REQUEST['act'] == 'query')
{
    $list = $shippingTemplatesController->get_shipping_templates();
    $shippingTemplatesController->ajaxQueryAction($list['data'], $list['record_count']);
}
elseif ($_REQUEST['act'] == 'ajaxEdit')
{
    $shippingTemplatesController->ajaxEditAction();
}
elseif ($_REQUEST['act'] == 'edit_content')
{
    $template_id = intval($_POST['id']);
    $value = trim($_POST['val']);
    
    // Multiple language support
    $edit_lang = empty($_REQUEST['edit_lang']) ? '' : trim($_REQUEST['edit_lang']);
    if ($edit_lang && !in_array($edit_lang, available_languages()))
    {
        make_json_error('Invalid language');
    }
    
    if (localized_update('shipping_templates', $template_id, array('template_content' => $value), $edit_lang))
    {
        // Update all associated goods
        $sql = "SELECT `goods_id` FROM " . $ecs->table('goods') . " WHERE `shipping_template_id` = '" . $template_id . "'";
        $goods_ids = $db->getCol($sql);
        if (!empty($goods_ids))
        {
            //localized_update('goods', $goods_ids, array('shipping_note' => $value), $edit_lang);
            
            clear_cache_files();
        }
        
        make_json_result(stripcslashes($value));
    }
    else
    {
        make_json_error('修改安裝條款模版失敗');
    }
}
elseif ($_REQUEST['act'] == 'remove')
{
    $template_id = intval($_REQUEST['id']);
    
    $sql = "DELETE FROM " . $ecs->table('shipping_templates') .
            " WHERE `template_id` = '" . $template_id . "' ";
    $db->query($sql);
    $sql = "DELETE FROM " . $ecs->table('shipping_templates_lang') .
            " WHERE `template_id` = '" . $template_id . "' ";
    $db->query($sql);
    // Reset linked goods to "custom shipping"
    $sql = "UPDATE " . $ecs->table('goods') . " SET `shipping_template_id` = 0 " .
            " WHERE `shipping_template_id` = '" . $template_id . "' ";
    $db->query($sql);
    
    $url = 'shipping_templates.php?act=query&' . str_replace('act=' . $_REQUEST['act'], '', $_SERVER['QUERY_STRING']);
    
    header('Location: ' . $url);
    exit;
}
elseif ($_REQUEST['act'] == 'get_shipping_template')
{
    $template_id = intval($_REQUEST['id']);
    
    $sql = "SELECT * " .
            "FROM " . $ecs->table('shipping_templates') .
            "WHERE `template_id` = '" . $template_id . "' ";
    $data = $db->getRow($sql);
    
    $localized_versions = get_localized_versions('shipping_templates', $template_id);
    
    header('Content-Type: application/json;charset=utf-8');
    echo json_encode(array(
        'data' => $data,
        'localized_versions' => $localized_versions
    ));
    exit;
}
elseif ($_REQUEST['act'] == 'edit')
{
    // $template_id = intval($_REQUEST['template_id']);
    // $data = $shippingTemplatesController->get_shipping_template($template_id);

    // $smarty->assign('ur_here',      $_LANG['shipping_templates_edit']);
    // $smarty->assign('data',$data);

    // $localizable_fields = localizable_fields_for_table('shipping_templates');
    // $smarty->assign('localizable_fields', $localizable_fields);
    // $smarty->assign('localized_versions', get_localized_versions('shipping_templates', $_REQUEST['template_id'], $localizable_fields));

    // $smarty->assign('action_link', array('text' => '安裝條款模版', 'href' => 'shipping_templates.php?act=list')); 
    // assign_query_info();
    // $smarty->display('shipping_templates_edit.htm');
    $shippingTemplatesController->editAction();
}
elseif ($_REQUEST['act'] == 'update' || $_REQUEST['act'] == 'insert')
{
    // if(isset($_REQUEST['template_name']) && isset($_REQUEST['template_content'])){
    //     if($shippingTemplatesController->insert_update_shipping_template($_REQUEST)){
    //         echo json_encode(array('success'=>1,'message'=>'更改成功'));
    //     } else {
    //         echo json_encode(array('success'=>0,'message'=>'未輸入英文版本'));
    //     }
    // }else{
    //     echo json_encode(array('success'=>0,'message'=>'更改失敗'));
    // }
    // clear_cache_files();
    $shippingTemplatesController->updateAction();
}
?>
