<?php

/**
 * ECSHOP 专题管理
 * ============================================================================
 * 版权所有 2005-2010 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liuhui $
 * $Id: topic.php 17063 2010-03-25 06:35:46Z liuhui $
*/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
$topicController = new Yoho\cms\Controller\TopicController();
/* act操作项的初始化 */
if (empty($_REQUEST['act']))
{
    $_REQUEST['act'] = 'list';
}
else
{
    $_REQUEST['act'] = trim($_REQUEST['act']);
}

/* 配置风格颜色选项 */
$topic_style_color = array(
                        '0'         => '008080',
                        '1'         => '008000',
                        '2'         => 'ffa500',
                        '3'         => 'ff0000',
                        '4'         => 'ffff00',
                        '5'         => '9acd32',
                        '6'         => 'ffd700'
                          );
$allow_suffix = array('gif', 'jpg', 'png', 'jpeg', 'bmp', 'swf');

/*------------------------------------------------------ */
//-- 专题列表页面
/*------------------------------------------------------ */
if ($_REQUEST['act'] == 'list')
{
    admin_priv('topic_manage');

    $smarty->assign('ur_here',     $_LANG['09_topic']);

    $smarty->assign('full_page',   1);

    $topic_cat_list = $db->getAll("SELECT topic_cat_id as `value`, topic_cat_name as `name` FROM " . $ecs->table('topic_category'));

    if (!empty($_REQUEST['edit_lang']))
    {
        $topic_cat_list = localize_db_result_lang($_REQUEST['edit_lang'], 'topic_category', $topic_cat_list, array('value' => 'topic_cat_id', 'name' => 'topic_cat_name'));
    }

    $smarty->assign('topic_cat_list', $topic_cat_list);
    
    $smarty->assign('action_link', array('text' => '新增推廣優惠', 'href' => 'topic.php?act=add'));
    $smarty->assign('action_link2', array('text' => '推廣優惠分類', 'href' => 'topic.php?act=list_cat'));
    
    assign_query_info();
    $smarty->display('topic_list.htm');
}
elseif ($_REQUEST["act"] == "query")
{
    check_authz_json('topic_manage');
    
    $topic_list = $topicController->get_topic_list();
    $topicController->ajaxQueryAction($topic_list['item'], $topic_list['record_count'], false);

}
/* 添加,编辑 */
elseif ($_REQUEST['act'] == 'add' || $_REQUEST['act'] == 'edit')
{
    admin_priv('topic_manage');

    $isadd     = $_REQUEST['act'] == 'add';
    $smarty->assign('isadd', $isadd);
    $topic_id  = empty($_REQUEST['id']) ? 0 : intval($_REQUEST['id']);

    $smarty->assign('ur_here',     $_LANG['09_topic']);
    $smarty->assign('action_link', list_link($isadd));

    $smarty->assign('cat_list',   cat_list(0, 1));
    $smarty->assign('brand_list', get_brand_list());
    $smarty->assign('cfg_lang',   $_CFG['lang']);
    $smarty->assign('topic_style_color',   $topic_style_color);
    
    $topic_cat_list = $db->getAll("SELECT topic_cat_id, topic_cat_name FROM " . $ecs->table('topic_category'));
    $smarty->assign('topic_cat_list', $topic_cat_list);

    $width_height = get_toppic_width_height();
    if(isset($width_height['pic']['width']) && isset($width_height['pic']['height']))
    {
        $smarty->assign('width_height', sprintf($_LANG['tips_width_height'], $width_height['pic']['width'], $width_height['pic']['height']));
    }
    if(isset($width_height['title_pic']['width']) && isset($width_height['title_pic']['height']))
    {
        $smarty->assign('title_width_height', sprintf($_LANG['tips_title_width_height'], $width_height['title_pic']['width'], $width_height['title_pic']['height']));
    }

    // Multiple language support
    $localizable_fields = localizable_fields_for_table('topic');
    $smarty->assign('localizable_fields', $localizable_fields);
    
    if (!$isadd)
    {
        $sql = "SELECT * FROM " . $ecs->table('topic') . " WHERE topic_id = '$topic_id'";
        $topic = $db->getRow($sql);
        $topic['start_time'] = local_date('Y-m-d', $topic['start_time']);
        $topic['end_time']   = local_date('Y-m-d', $topic['end_time']);

        require(ROOT_PATH . 'includes/cls_json.php');

        $topic_data = @unserialize($topic['data']);
        if (!$topic_data) // Load old malformed data
        {
            $topic['data'] = addcslashes($topic['data'], "'");
            $topic_data = @unserialize($topic['data']);
        }
        $topic['data'] = json_encode($topic_data);

        if (empty($topic['topic_img']) && empty($topic['htmls']))
        {
            $topic['topic_type'] = 0;
        }
        elseif ($topic['htmls'] != '')
        {
            $topic['topic_type'] = 2;
        }
        elseif (preg_match('/.swf$/i', $topic['topic_img']))
        {
            $topic['topic_type'] = 1;
        }
        else
        {
            $topic['topic_type'] = '';
        }

        $topic_ids = explode(",", $topic["topic_cat_id"]);
        foreach ($topic_cat_list as $key => $topic_cat) {
            if (in_array($topic_cat['topic_cat_id'], $topic_ids)) {
                $topic_cat_list[$key]['selected'] = 1;
            }
        }

        if (file_exists(ROOT_PATH . DATA_DIR . "/afficheimg/topic/$topic_id" . "_icon.png")) {
            $topic['has_theme_icon'] = true;
        }
        $smarty->assign('topic_cat_list', $topic_cat_list);

        $smarty->assign('topic', $topic);
        
        // Multiple language support
        $smarty->assign('localized_versions', get_localized_versions('topic', $topic_id, $localizable_fields));
        
        $smarty->assign('act',   "update");
    }
    else
    {
        $topic = array('title' => '', 'tag' => '', 'topic_type' => 0, 'url' => 'http://', 'data' => json_encode(''));
        $smarty->assign('topic', $topic);
        
        $smarty->assign('act', "insert");
    }
    assign_query_info();
    $smarty->display('topic_edit.htm');
}
elseif ($_REQUEST['act'] == 'insert' || $_REQUEST['act'] == 'update')
{
    admin_priv('topic_manage');

    $is_insert = $_REQUEST['act'] == 'insert';
    $topic_id  = empty($_POST['topic_id']) ? 0 : intval($_POST['topic_id']);
    $topic_type= empty($_POST['topic_type']) ? 0 : intval($_POST['topic_type']);

    switch ($topic_type)
    {
        case '0':
        case '1':

            // 主图上传
            if ($_FILES['topic_img']['name'] && $_FILES['topic_img']['size'] > 0)
            {
                /* 检查文件合法性 */
                if(!get_file_suffix($_FILES['topic_img']['name'], $allow_suffix))
                {
                    sys_msg($_LANG['invalid_type']);
                }

                /* 处理 */
                $name = date('Ymd');
                for ($i = 0; $i < 6; $i++)
                {
                    $name .= chr(mt_rand(97, 122));
                }
                $name .= '.' . end(explode('.', $_FILES['topic_img']['name']));
                $target = ROOT_PATH . DATA_DIR . '/afficheimg/' . $name;

                if (move_upload_file($_FILES['topic_img']['tmp_name'], $target))
                {
                    $topic_img = DATA_DIR . '/afficheimg/' . $name;
                }
            }
            else if (!empty($_REQUEST['url']))
            {
                /* 来自互联网图片 不可以是服务器地址 */
                if(strstr($_REQUEST['url'], 'http') && !strstr($_REQUEST['url'], $_SERVER['SERVER_NAME']))
                {
                    /* 取互联网图片至本地 */
                    $topic_img = get_url_image($_REQUEST['url']);
                }
                else{
                    sys_msg($_LANG['web_url_no']);
                }
            }
            unset($name, $target);

            $topic_img = empty($topic_img) ? $_POST['img_url'] : $topic_img;
            $htmls = '';

        break;

        case '2':

            $htmls     = $_POST['htmls'];

            $topic_img = '';

        break;
    }

    // 标题图上传
    if ($_FILES['title_pic']['name'] && $_FILES['title_pic']['size'] > 0)
    {
        /* 检查文件合法性 */
        if(!get_file_suffix($_FILES['title_pic']['name'], $allow_suffix))
        {
            sys_msg($_LANG['invalid_type']);
        }

        /* 处理 */
        $name = date('Ymd');
        for ($i = 0; $i < 6; $i++)
        {
            $name .= chr(mt_rand(97, 122));
        }
        $name .= '.' . end(explode('.', $_FILES['title_pic']['name']));
        $target = ROOT_PATH . DATA_DIR . '/afficheimg/' . $name;

        if (move_upload_file($_FILES['title_pic']['tmp_name'], $target))
        {
            $title_pic = DATA_DIR . '/afficheimg/' . $name;
        }
    }
    else if (!empty($_REQUEST['title_url']))
    {
        /* 来自互联网图片 不可以是服务器地址 */
        if(strstr($_REQUEST['title_url'], 'http') && !strstr($_REQUEST['title_url'], $_SERVER['SERVER_NAME']))
        {
            /* 取互联网图片至本地 */
            $title_pic = get_url_image($_REQUEST['title_url']);
        }
        else{
            sys_msg($_LANG['web_url_no']);
        }
    }
    unset($name, $target);
    $title_pic = empty($title_pic) ? $_POST['title_img_url'] : $title_pic;
    
    // Theme pic, used in promotion list page
    $theme_pic_path = handle_uploaded_image('theme_pic', 325, 215);
    
    // Theme Icon, used in product page
    if(!($_FILES['theme_icon']['name'] && $_FILES['theme_icon']['size'] > 0)){
        if (!file_exists(ROOT_PATH . DATA_DIR . "/afficheimg/topic/$topic_id" . "_icon.png")) {
            do
            {
                $name = date('Ymd');
                for ($i = 0; $i < 6; $i++)
                {
                    $name .= chr(mt_rand(97, 122));
                }
                $save_path = ROOT_PATH . DATA_DIR . '/afficheimg/' . $name . '.png';
            }
            while (file_exists($save_path));
            $default_img_path = ROOT_PATH . 'images/topic_default.png';
            exec('cp -p ' . escapeshellarg($default_img_path) . ' ' . escapeshellarg($save_path));
            $theme_icon_path = $save_path;
        }
    }
    else{
        $theme_icon_path = handle_uploaded_image('theme_icon', 32, 32);
    }
    

    require(ROOT_PATH . 'includes/cls_json.php');

    $start_time = local_strtotime($_POST['start_time']);
    $end_time   = local_strtotime($_POST['end_time']);

    // $json         = new JSON;
    // $tmp_data     = $json->decode($_POST['topic_data']);
    // $data         = serialize($tmp_data);
    $goods_ids = array();
    $topic_data_temp = json_decode(stripslashes($_POST['topic_data']));

    foreach($topic_data_temp as $brand){
        foreach($brand as $product){
            $goods_ids[] = explode("|", $product)[1];
        }
    }
    $sql = "SELECT cat_id FROM " . $ecs->table('goods') .
        " WHERE goods_id " . db_create_in($goods_ids) . 
        " GROUP BY cat_id";
    $related_category = $db->getCol($sql);
    $related_category = join(',', $related_category);

    $data         = addslashes(serialize($topic_data_temp));
    $base_style   = $_POST['base_style'];
    $keywords     = $_POST['keywords'];
    $description  = $_POST['description'];
    $topic_cat_id = isset($_POST['topic_cat_id']) ? trim($_POST['topic_cat_id']) : 0;

    // handle simplified chinese
    $localizable_fields = [
        "title"         => $_POST['title'],
        "tag"           => $_POST['tag'],
        "intro"         => $_POST['topic_intro'],
        "intro_mobile"  => $_POST['topic_intro_mobile'],
        "keywords"      => $keywords,
        "description"   => $description,
    ];

    if ($is_insert)
    {
        if (empty($_POST['perma_link'])) {
            sys_msg("請設定永久鏈結");
        }
        $localizable_fields['perma_link'] = empty($_POST["perma_link_raw"]) ? $_POST['title'] : $_POST["perma_link_raw"];
        $sql = "INSERT INTO " . $ecs->table('topic') . " (title,tag,start_time,end_time,data,intro,intro_mobile,template,css,topic_img,title_pic,base_style, htmls,keywords,description,topic_cat_id,redirection,perma_link,related_category,show_top_50)" .
                "VALUES ('$_POST[title]','$_POST[tag]','$start_time','$end_time','$data','$_POST[topic_intro]','$_POST[topic_intro_mobile]','$_POST[topic_template_file]','$_POST[topic_css]', '$topic_img', '$title_pic', '$base_style', '$htmls','$keywords','$description','$topic_cat_id','$_POST[redirection]','".trim($_POST['perma_link'])."','$related_category','$_POST[show_top_50]')";
    }
    else
    {
        $localizable_fields = $topicController->checkRequireSimplied("topic", $localizable_fields, $topic_id);
        $sql = "UPDATE " . $ecs->table('topic') .
                "SET title='$_POST[title]',tag='$_POST[tag]',start_time='$start_time',end_time='$end_time',data='$data',intro='$_POST[topic_intro]',intro_mobile='$_POST[topic_intro_mobile]',template='$_POST[topic_template_file]',css='$_POST[topic_css]', topic_img='$topic_img', title_pic='$title_pic', base_style='$base_style', htmls='$htmls', keywords='$keywords', description='$description', topic_cat_id='$topic_cat_id',redirection='$_POST[redirection]',related_category='$related_category',show_top_50='$_POST[show_top_50]'" .
               " WHERE topic_id='$topic_id' LIMIT 1";
    }
    to_simplified_chinese($localizable_fields);

    $db->query($sql);
    
    if ($is_insert)
    {
        $topic_id = $db->insert_id();
    }
    
    // Move theme pic to destination
    if (!empty($theme_pic_path))
    {
        rename($theme_pic_path, ROOT_PATH . DATA_DIR . '/afficheimg/topic/' . $topic_id . '.png');
    }
    
    // Move theme icon to destination
    if (!empty($theme_icon_path))
    {
        rename($theme_icon_path, ROOT_PATH . DATA_DIR . '/afficheimg/topic/' . $topic_id . '_icon.png');
    }
    
    // Multiple language support
    save_localized_versions('topic', $topic_id);

    clear_cache_files();

    $links[] = array('href' => 'topic.php', 'text' =>  $_LANG['back_list']);
    sys_msg($_LANG['succed'], 0, $links);

}
elseif ($_REQUEST['act'] == 'update_goods_list_specific')
{
    $sql = 'SELECT goods_id, goods_name FROM `ecs_goods` WHERE shop_price >= 200 and shop_price <=1500 and is_on_sale = 1';
    $results = $db->getAll($sql);
    $update_ar = [];
    foreach ($results as $item) {
        $update_ar[] = $item['goods_name'] .'|'. $item['goods_id'];
    }

    $newobj = new stdClass();//create a new
    $newobj->default =  $update_ar;
    $data = addslashes(serialize( $newobj));

    $sql = "UPDATE " . $ecs->table('topic') .
    " SET data='$data' " .
    " WHERE topic_id='".$_REQUEST['id']."' LIMIT 1";
    $db->query($sql);

    echo 'OK';
}
elseif ($_REQUEST['act'] == 'get_goods_list')
{
    include_once(ROOT_PATH . 'includes/cls_json.php');
    $json = new JSON;

    $filters = $json->decode($_GET['JSON']);

    //$arr = get_goods_list($filters);
    $arr = get_goods_list_from_include_goods($filters);
    $opt = array();

    foreach ($arr AS $key => $val)
    {
        $opt[] = array('value' => $val['goods_id'],
                       'text'  => $val['goods_name']);
    }

    make_json_result($opt);
}
elseif ($_REQUEST["act"] == "remove" || $_REQUEST["act"] == "batch")
{
    admin_priv('topic_manage');
    $sql = "DELETE FROM " . $ecs->table('topic') . " WHERE ";
    $sql2 = "DELETE FROM " . $ecs->table('topic_lang') . " WHERE ";

    if (!empty($_REQUEST['id']))
    {
        $sql .= db_create_in($_REQUEST['id'], 'topic_id');
        $sql2 .= db_create_in($_REQUEST['id'], 'topic_id');
    }
    else
    {
        exit;
    }

    $db->query($sql);
    $db->query($sql2);

    clear_cache_files();

    if (!empty($_REQUEST['is_ajax']))
    {
        $url = 'topic.php?act=list&' . str_replace('act=delete', '', $_SERVER['QUERY_STRING']);
        ecs_header("Location: $url\n");
        exit;
    }

    $links[] = array('href' => 'topic.php', 'text' =>  $_LANG['back_list']);
    sys_msg($_LANG['succed'], 0, $links);
}
elseif ($_REQUEST['act'] == 'ajaxEdit')
{
    check_authz_json('topic_manage');
    if($_REQUEST['col'] == 'title') {
        $topic_id = intval($_POST['id']);
        $title = json_str_iconv(trim($_POST['value']));
        // Multiple language support
        $edit_lang = empty($_REQUEST['edit_lang']) ? '' : trim($_REQUEST['edit_lang']);
        if ($edit_lang && !in_array($edit_lang, available_languages()))
        {
            make_json_error('Invalid language');
        }

        if (localized_update('topic', $topic_id, array('title' => $title), $edit_lang))
        {
            clear_cache_files();
            make_json_result(stripcslashes($title));
        }
        else
        {
            make_json_error('修改名稱失敗');
        }
    } else if($_REQUEST['col'] == 'sort_order') {
        check_authz_json('topic_manage');

        $id  = intval($_POST['id']);
        $val = intval($_POST['value']);

        $sql = "UPDATE " . $ecs->table('topic') .
                " SET sort_order = '$val'" .
                " WHERE topic_id = '$id' LIMIT 1";
        $db->query($sql);
        clear_cache_files();
        make_json_result($val);
    } else {
        $topicController->ajaxEditAction();
    }
}
// Topic Category
elseif ($_REQUEST['act'] == 'list_cat')
{
    admin_priv('topic_manage');

    $smarty->assign('ur_here',     $_LANG['09_topic']);

    $smarty->assign('full_page',   1);
    $cat_list = get_topic_cat_list();

    $smarty->assign('topic_cat_list', $cat_list['item']);
    $smarty->assign('filter',         $cat_list['filter']);
    $smarty->assign('record_count',   $cat_list['record_count']);
    $smarty->assign('page_count',     $cat_list['page_count']);

    $sort_flag  = sort_flag($cat_list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);
    
    $smarty->assign('action_link', array('text' => '新增分類', 'href' => 'javascript:create_topic_cat();'));
    $smarty->assign('action_link2', array('text' => '返回推廣優惠', 'href' => 'topic.php?act=list'));
    
    assign_query_info();
    $smarty->display('topic_cat_list.htm');
}
elseif ($_REQUEST['act'] == 'query_cat')
{
    check_authz_json('topic_manage');
    
    $cat_list = get_topic_cat_list();
    $smarty->assign('topic_cat_list', $cat_list['item']);
    $smarty->assign('filter',         $cat_list['filter']);
    $smarty->assign('record_count',   $cat_list['record_count']);
    $smarty->assign('page_count',     $cat_list['page_count']);
    
    /* 排序标记 */
    $sort_flag  = sort_flag($cat_list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);
    
    make_json_result($smarty->fetch('topic_cat_list.htm'), '', array('filter' => $cat_list['filter'], 'page_count' => $cat_list['page_count']));
}
elseif ($_REQUEST['act'] == 'add_cat')
{
    check_authz_json('topic_manage');
    
    $topic_cat_name = empty($_REQUEST['topic_cat_name']) ? '' : trim($_REQUEST['topic_cat_name']);
    
    if (empty($topic_cat_name))
    {
        make_json_error('無效的分類名稱');
        exit;
    }
    
    $sql = "SELECT '1' FROM " . $ecs->table('topic_category') . " WHERE topic_cat_name = '" . $topic_cat_name . "' LIMIT 1";
    if ($db->getOne($sql))
    {
        make_json_error('該分類名稱已存在');
        exit;
    }
    
    $sql = "INSERT INTO " . $ecs->table('topic_category') . " (`topic_cat_name`) VALUES ('" . $topic_cat_name . "')";
    if ($db->query($sql))
    {
        localized_update('topic_category', $db->insert_id(), array('topic_cat_name' => translate_to_simplified_chinese($topic_cat_name)), 'zh_cn');
        clear_cache_files();
        
        make_json_result('新增分類「' . $topic_cat_name . '」成功！');
    }
    else
    {
        make_json_error('新增失敗');
    }
}
elseif ($_REQUEST['act'] == 'edit_topic_cat_name')
{
    check_authz_json('topic_manage');
    
    $topic_cat_id = intval($_POST['id']);
    $topic_cat_name = json_str_iconv(trim($_POST['val']));
    
    // Multiple language support
    $edit_lang = empty($_REQUEST['edit_lang']) ? '' : trim($_REQUEST['edit_lang']);
    if ($edit_lang && !in_array($edit_lang, available_languages()))
    {
        make_json_error('Invalid language');
    }

    // $sql = "UPDATE " .$ecs->table('topic_category'). " SET ".
    //        "topic_cat_name = '" . $topic_cat_name . "' ".
    //        "WHERE topic_cat_id = '$topic_cat_id'";
    // if ($db->query($sql))
    if (localized_update('topic_category', $topic_cat_id, array('topic_cat_name' => $topic_cat_name), $edit_lang))
    {
        if (empty($edit_lang) || $edit_lang == default_language()) {
            localized_update('topic_category', $topic_cat_id, array('topic_cat_name' => translate_to_simplified_chinese($topic_cat_name)), 'zh_cn');
        }
        clear_cache_files();
        
        make_json_result(stripcslashes($topic_cat_name));
    }
    else
    {
        make_json_error('修改分類名稱失敗');
    }
}
elseif ($_REQUEST['act'] == 'edit_topic_category')
{
    check_authz_json('topic_manage');
    
    $topic_id = intval($_POST['id']);
    $topic_cat_id = intval($_POST['value']);

    $sql = "UPDATE " .$ecs->table('topic'). " SET ".
           "topic_cat_id = '" . $topic_cat_id . "' ".
           "WHERE topic_id = '$topic_id'";
    if ($db->query($sql))
    {
        clear_cache_files();
        
        make_json_result($topic_cat_id);
    }
    else
    {
        make_json_error('修改推廣優惠分類失敗');
    }
}
elseif ($_REQUEST['act'] == 'delete_cat')
{
    check_authz_json('topic_manage');

    $topic_cat_id = empty($_REQUEST['id']) ? 0 : intval($_REQUEST['id']);
    
    if (empty($topic_cat_id))
    {
        make_json_error('無效的分類ID');
        exit;
    }
    
    $sql = "SELECT title FROM " . $ecs->table('topic') . " WHERE topic_cat_id = '" . $topic_cat_id . "'";
    $res = $db->getCol($sql);
    if ($res)
    {
        make_json_error("刪除此分類前，請先移走/刪除以下推廣優惠：\n" . implode("\n", $res));
        exit;
    }
    
    $sql = "DELETE FROM " . $ecs->table('topic_category') . " WHERE topic_cat_id = '" . $topic_cat_id . "' LIMIT 1";
    if ($db->query($sql))
    {
        clear_cache_files();
        
        $url = 'topic.php?act=query_cat&' . str_replace('act=delete_cat', '', $_SERVER['QUERY_STRING']);
        header('Location: ' . $url);
        exit;
    }
    else
    {
        make_json_error('刪除分類失敗');
    }
}
//use one time, remove it after bulk update
elseif ($_REQUEST['act'] == 'bulk_update_topic_related_cat')
{
    $sql = "SELECT topic_id, data FROM " . $ecs->table('topic');
    $topics = $db->getAll($sql);
    foreach($topics as $topic){
        $topic_id = $topic['topic_id'];
        $topic_data_temp = @unserialize($topic['data']);
        if (!$topic_data) // Load old malformed data
        {
            $topic['data'] = addcslashes($topic['data'], "'");
            $topic_data_temp = @unserialize($topic['data']);
        }
        $goods_ids = array();
        foreach($topic_data_temp as $brand){
            foreach($brand as $product){
                $goods_ids[] = explode("|", $product)[1];
            }
        }

        $sql = "SELECT cat_id FROM " . $ecs->table('goods') .
            " WHERE goods_id " . db_create_in($goods_ids) . 
            " GROUP BY cat_id";
        $related_category = $db->getCol($sql);
        $related_category = join(',', $related_category);

        $sql = "UPDATE " .$ecs->table('topic') .
            "SET related_category = '".$related_category."' WHERE topic_id='$topic_id'";
        $GLOBALS['db']->query($sql);
    }
}
/**
 * 列表链接
 * @param   bool    $is_add     是否添加（插入）
 * @param   string  $text       文字
 * @return  array('href' => $href, 'text' => $text)
 */
function list_link($is_add = true, $text = '')
{
    $href = 'topic.php?act=list';
    if (!$is_add)
    {
        $href .= '&' . list_link_postfix();
    }
    if ($text == '')
    {
        $text = $GLOBALS['_LANG']['topic_list'];
    }

    return array('href' => $href, 'text' => $text);
}

function get_toppic_width_height()
{
    $width_height = array();

    $file_path = ROOT_PATH . 'themes/' . $GLOBALS['_CFG']['template'] . '/topic.dwt';
    if (!file_exists($file_path) || !is_readable($file_path))
    {
        return $width_height;
    }

    $string = file_get_contents($file_path);

    $pattern_width = '/var\s*topic_width\s*=\s*"(\d+)";/';
    $pattern_height = '/var\s*topic_height\s*=\s*"(\d+)";/';
    preg_match($pattern_width, $string, $width);
    preg_match($pattern_height, $string, $height);
    if(isset($width[1]))
    {
        $width_height['pic']['width'] = $width[1];
    }
    if(isset($height[1]))
    {
        $width_height['pic']['height'] = $height[1];
    }
    unset($width, $height);

    $pattern_width = '/TitlePicWidth:\s{1}(\d+)/';
    $pattern_height = '/TitlePicHeight:\s{1}(\d+)/';
    preg_match($pattern_width, $string, $width);
    preg_match($pattern_height, $string, $height);
    if(isset($width[1]))
    {
        $width_height['title_pic']['width'] = $width[1];
    }
    if(isset($height[1]))
    {
        $width_height['title_pic']['height'] = $height[1];
    }

    return $width_height;
}

function get_url_image($url)
{
    $ext = strtolower(end(explode('.', $url)));
    if($ext != "gif" && $ext != "jpg" && $ext != "png" && $ext != "bmp" && $ext != "jpeg")
    {
        return $url;
    }

    $name = date('Ymd');
    for ($i = 0; $i < 6; $i++)
    {
        $name .= chr(mt_rand(97, 122));
    }
    $name .= '.' . $ext;
    $target = ROOT_PATH . DATA_DIR . '/afficheimg/' . $name;

    $tmp_file = DATA_DIR . '/afficheimg/' . $name;
    $filename = ROOT_PATH . $tmp_file;

    $img = file_get_contents($url);

    $fp = @fopen($filename, "a");
    fwrite($fp, $img);
    fclose($fp);

    return $tmp_file;
}

function handle_uploaded_image($fieldname, $target_width, $target_height, $ext = '.png')
{
    if ($_FILES[$fieldname]['name'] && $_FILES[$fieldname]['size'] > 0)
    {
        do
        {
            $name = date('Ymd');
            for ($i = 0; $i < 6; $i++)
            {
                $name .= chr(mt_rand(97, 122));
            }
            $save_path = ROOT_PATH . DATA_DIR . '/afficheimg/' . $name;
        }
        while (file_exists($save_path . $ext));
        
        $tmp_name = $_FILES[$fieldname]['tmp_name'];
        
        $size = getimagesize($tmp_name);
        
        $width = $size[0];
        $height = $size[1];
        $type = $size['mime'];
        
        if ($width > 2048 || $height > 2048)
        {
            sys_msg('圖片尺寸過大，請先把圖片縮小至 1024x1024 以內。');
        }
        
        if ($type == 'image/jpeg')
        {
            $tmp_path = $save_path . '_tmp.jpg';
        }
        else if ($type == 'image/gif')
        {
            $tmp_path = $save_path . '_tmp.gif';
        }
        else if ($type == 'image/png')
        {
            $tmp_path = $save_path . '_tmp.png';
        }
        else
        {
            sys_msg('圖片格式錯誤，請確認圖片格式是 jpg, gif, png');
        }
        
        if (move_uploaded_file($tmp_name, $tmp_path))
        {
            $save_path .= $ext;
            
            if (($width != $target_width) || ($height != $target_height))
            {
                // Resize the image to target size
                exec('/usr/bin/convert ' . escapeshellarg($tmp_path) . ' -resize ' . $target_width . 'x' . $target_height . '^ -gravity center -extent ' . $target_width . 'x' . $target_height . ' ' . escapeshellarg($save_path));
                unlink($tmp_path);
            }
            else
            {
                rename($tmp_path, $save_path);
            }
        }
        
        return $save_path;
    }
    else
    {
        return '';
    }
}

function get_topic_cat_list()
{
    /* 查询条件 */
    $filter['sort_by']    = empty($_REQUEST['sort_by']) ? 'topic_cat_id' : trim($_REQUEST['sort_by']);
    $filter['sort_order'] = empty($_REQUEST['sort_order']) ? 'ASC' : trim($_REQUEST['sort_order']);
    
    $sql = "SELECT COUNT(*) FROM " . $GLOBALS['ecs']->table('topic_category');
    $filter['record_count'] = $GLOBALS['db']->getOne($sql);
    
    /* 分页大小 */
    $filter = page_and_size($filter);
    
    $sql = "SELECT * " .
            "FROM " . $GLOBALS['ecs']->table('topic_category') .
            "ORDER BY $filter[sort_by] $filter[sort_order] " .
            "LIMIT $filter[start], $filter[page_size]";
    $res = $GLOBALS['db']->getAll($sql);
    
    // Multiple language support
    if (!empty($_REQUEST['edit_lang']))
    {
        $res = localize_db_result_lang($_REQUEST['edit_lang'], 'topic_category', $res);
    }
    
    foreach ($res as $key => $row)
    {
        $res[$key]['url'] = build_uri('topic_cat', array('tcid' => $row['topic_cat_id']));
    }
    
    $arr = array('item' => $res, 'filter' => $filter, 'page_count' => $filter['page_count'], 'record_count' => $filter['record_count']);
    
    return $arr;
}

?>
