<?php

/**
 * ECSHOP 属性规格管理
 * ============================================================================
 * 版权所有 2005-2010 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liuhui $
 * $Id: attribute.php 17119 2010-04-21 07:58:09Z liuhui $
*/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

$attrbuteController = new Yoho\cms\Controller\AttributeController();

/* act操作项的初始化 */
$_REQUEST['act'] = trim($_REQUEST['act']);
if (empty($_REQUEST['act']))
{
    $_REQUEST['act'] = 'list';
}

$exc = new exchange($ecs->table("attribute"), $db, 'attr_id', 'attr_name');

/*------------------------------------------------------ */
//-- 属性列表
/*------------------------------------------------------ */
if ($_REQUEST['act'] == 'list')
{
    admin_priv('attr_manage');
    
    $goods_type = isset($_GET['goods_type']) ? intval($_GET['goods_type']) : 0;

    $smarty->assign('ur_here',          $_LANG['09_attribute_list']);
    $smarty->assign('action_link',      array('href' => 'attribute.php?act=add&goods_type='.$goods_type , 'text' => $_LANG['10_attribute_add']));
    $smarty->assign('action_link2', array('href' => 'attribute.php?act=cat_list', 'text' => $_LANG['09_attribute_cat_list']));
    //$smarty->assign('goods_type_list', cat_list(0, $goods_type)); // Old :  goods_type_list($goods_type), Change to use: cat_list(0, $goods_type)
    $smarty->assign('goods_type_list', cat_list(0, $goods_type, false, 0, true, true));
    $smarty->assign('full_page',        1);
    /* 显示模板 */
    assign_query_info();
    $smarty->display('attribute_list.htm');
}

/*------------------------------------------------------ */
//-- 排序、翻页
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'query')
{
    $attrbuteController->ajaxQueryAction($list['item'], $list['record_count']);
    /*// Multiple language support
    if (!empty($_REQUEST['edit_lang']))
    {
        $list['item'] = localize_db_result_lang($_REQUEST['edit_lang'], 'attribute', $list['item']);
    }
    $smarty->assign('attr_list',    $list['item']);
    $smarty->assign('filter',       $list['filter']);
    $smarty->assign('record_count', $list['record_count']);
    $smarty->assign('page_count',   $list['page_count']);

    $sort_flag  = sort_flag($list['filter']);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);

    make_json_result($smarty->fetch('attribute_list.htm'), '',
        array('filter' => $list['filter'], 'page_count' => $list['page_count']));*/
}

elseif ($_REQUEST['act'] == 'cat_list')
{
    /* 检查权限 */
    admin_priv('attr_manage');
    $cat_list = cat_list(0, 0, false);

    $smarty->assign('ur_here',          $_LANG['09_attribute_cat_list']);
    $smarty->assign('action_link', array('href' => 'attribute.php?act=list', 'text' => $_LANG['09_attribute_list']));
    $smarty->assign('full_page',        1);
    $smarty->assign('cat_info',    $cat_list);
    assign_query_info();
    $smarty->display('attribute_cat_list.htm');
}
/*------------------------------------------------------ */
//-- 添加/编辑属性
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'add' || $_REQUEST['act'] == 'edit')
{
    /* 检查权限 */
    admin_priv('attr_manage');

    /* 添加还是编辑的标识 */
    $is_add = $_REQUEST['act'] == 'add';
    $smarty->assign('form_act', $is_add ? 'insert' : 'update');

    $goods_type_list = cat_list(0, $attr['cat_id'], false, 0, true, true);

    /* 取得属性信息 */
    if ($is_add)
    {
        $goods_type = isset($_GET['goods_type']) ? intval($_GET['goods_type']) : 0;
        $attr = array(
            'attr_id' => 0,
            'cat_id' => $goods_type,
            'attr_name' => '',
            'attr_input_type' => 1, // default: 從下面的列表中選擇（一行代表一個可選值）
            'attr_index'  => 0,
            'attr_values' => '',
            'attr_type' => 0,
            'is_linked' => 0,
        );
    }
    else
    {
        $sql = "SELECT * FROM " . $ecs->table('attribute') . " WHERE attr_id = '$_REQUEST[id]'";
        $attr = $db->getRow($sql);
    }

    $attr['cat_name'] = $goods_type_list[$attr["cat_id"]]["cat_name"];

    $attr_id = empty($_REQUEST['id']) ? 0 : $_REQUEST['id'];
    $smarty->assign('attr', $attr);
    $smarty->assign('attr_groups', get_attr_groups($attr['cat_id']));

    /* 取得商品分类列表 */
    //$smarty->assign('goods_type_list', cat_list(0, $attr['cat_id'])); // Old : goods_type_list($attr['cat_id']) ,Change to use: cat_list(0, $attr['cat_id'])
    $smarty->assign('goods_type_list', $goods_type_list);

    /* 模板赋值 */
    $smarty->assign('ur_here', $is_add ?$_LANG['10_attribute_add']:$_LANG['52_attribute_add']);
    $smarty->assign('action_link', array('href' => 'attribute.php?act=list', 'text' => $_LANG['09_attribute_list']));

    // Multiple language support
    $localizable_fields = localizable_fields_for_table('attribute');
    $localized_versions = get_localized_versions('attribute', $attr_id, $localizable_fields);
    $smarty->assign('localizable_fields', $localizable_fields);
    $smarty->assign('localized_versions', $localized_versions);

    /* 显示模板 */
    assign_query_info();
    $smarty->display('attribute_info.htm');
}

/*------------------------------------------------------ */
//-- 插入/更新属性
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'insert' || $_REQUEST['act'] == 'update')
{
    /* 检查权限 */
    admin_priv('attr_manage');

    /* 插入还是更新的标识 */
    $is_insert = $_REQUEST['act'] == 'insert';

    /* 检查名称是否重复 */
    $exclude = empty($_POST['attr_id']) ? 0 : intval($_POST['attr_id']);
    if (!$exc->is_only('attr_name', $_POST['attr_name'], $exclude, " cat_id = '$_POST[cat_id]'"))
    {
        sys_msg($_LANG['name_exist'], 1);
    }

    $cat_id = $_REQUEST['cat_id'];

    /* 取得属性信息 */
    $attr = array(
        'cat_id'          => $_POST['cat_id'],
        'attr_name'       => $_POST['attr_name'],
        'attr_index'      => $_POST['attr_index'],
        'attr_input_type' => 1, //hardcode :1
        'is_linked'       => $_POST['is_linked'],
        'attr_values'     => isset($_POST['attr_values']) ? $_POST['attr_values'] : '',
        'attr_type'       => empty($_POST['attr_type']) ? '0' : intval($_POST['attr_type']),
        'attr_group'      => isset($_POST['attr_group']) ? intval($_POST['attr_group']) : 0,
        'is_dynamic'      => 0, //hardcode :disable
    );

    // handle simplified chinese
    $localizable_fields = [
        "attr_name"     => $attr["attr_name"],
        "attr_values"   => $attr['attr_values'],
    ];
    if (!$is_insert) {
        $localizable_fields = $attrbuteController->checkRequireSimplied("attribute", $localizable_fields, $_POST['attr_id']);
    }
    to_simplified_chinese($localizable_fields);

    /* 入库、记录日志、提示信息 */
    if ($is_insert)
    {
        $db->autoExecute($ecs->table('attribute'), $attr, 'INSERT');
        $attr_id = $db->insert_id();
        $attrbuteController->auto_update_cat_filter_attr($_POST['cat_id']);
        admin_log($_POST['attr_name'], 'add', 'attribute');
        // Multiple language support
        save_localized_versions('attribute', $attr_id);
        clear_cache_files(); // 清除相关的缓存文件
        $links = array(
            array('text' => $_LANG['add_next'], 'href' => '?act=add&goods_type=' . $_POST['cat_id']),
            array('text' => $_LANG['back_list'], 'href' => '?act=list'),
        );
        sys_msg(sprintf($_LANG['add_ok'], $attr['attr_name']), 0, $links);
    }
    else
    {
        //$sql = "SELECT * FROM " . $ecs->table('attribute') . " WHERE attr_id = '$_POST[attr_id]'";
        //$old_attr = $db->getRow($sql);

        $db->autoExecute($ecs->table('attribute'), $attr, 'UPDATE', "attr_id = '$_POST[attr_id]'");
        $attrbuteController->auto_update_cat_filter_attr($_POST['cat_id']);
        //$attrbuteController->auto_update_goods_attr($_POST['attr_id'], $_POST['attr_values'], $old_attr['attr_values']);
        admin_log($_POST['attr_name'], 'edit', 'attribute');
        // Multiple language support
        save_localized_versions('attribute', $_POST['attr_id']);
        clear_cache_files(); // 清除相关的缓存文件
        $links = array(
            array('text' => $_LANG['back_list'], 'href' => '?act=list&amp;goods_type='.$_POST['cat_id'].''),
        );
        sys_msg(sprintf($_LANG['edit_ok'], $attr['attr_name']), 0, $links);
    }
}

/*------------------------------------------------------ */
//-- 删除属性(一个或多个)
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'batch')
{
    /* 检查权限 */
    admin_priv('attr_manage');

    /* 取得要操作的编号 */
    if (isset($_POST['id']))
    {
        $ids   = isset($_POST['id']) ? $_POST['id'] : 0;
        $count = count(explode(",", $ids));

        $sql = "DELETE FROM " . $ecs->table('attribute') . " WHERE attr_id " . db_create_in($ids);
        $db->query($sql);

        $sql = "DELETE FROM " . $ecs->table('attribute_lang') . " WHERE attr_id " . db_create_in($ids);
        $db->query($sql);

        $sql = "DELETE ga.*, gal.* FROM " . $ecs->table('goods_attr'). " as ga ".
            "LEFT JOIN " .$ecs->table('goods_attr_lang'). " as gal ON ga.goods_attr_id = gal.goods_attr_id ".
            " WHERE ga.attr_id " . db_create_in($ids);
        $db->query($sql);
        foreach ($ids as $key => $id)
        {
            $attrbuteController->auto_update_cat_filter_attr($id);
        }
        /* 记录日志 */
        admin_log('', 'batch_remove', 'attribute');
        clear_cache_files();

        $link[] = array('text' => $_LANG['back_list'], 'href' => 'attribute.php?act=list');
        sys_msg(sprintf($_LANG['drop_ok'], $count), 0, $link);
    }
    else
    {
        $link[] = array('text' => $_LANG['back_list'], 'href' => 'attribute.php?act=list');
        sys_msg($_LANG['no_select_arrt'], 0, $link);
    }
}

/*------------------------------------------------------ */
//-- 编辑属性名称
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'ajaxEdit') {

    $attrbuteController->ajaxEditAction();

}
elseif ($_REQUEST['act'] == 'edit_attr_name')
{
    check_authz_json('attr_manage');

    $id = intval($_POST['id']);
    $val = json_str_iconv(trim($_POST['val']));

    /* 取得该属性所属商品类型id */
    $cat_id = $exc->get_name($id, 'cat_id');

    /* 检查属性名称是否重复 */
    if (!$exc->is_only('attr_name', $val, $id, " cat_id = '$cat_id'"))
    {
        make_json_error($_LANG['name_exist']);
    }

    $exc->edit("attr_name='$val'", $id);

    admin_log($val, 'edit', 'attribute');

    make_json_result(stripslashes($val));
}

/*------------------------------------------------------ */
//-- 编辑排序序号
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'edit_sort_order')
{
    check_authz_json('attr_manage');

    $id = intval($_POST['id']);
    $val = intval($_POST['val']);

    $exc->edit("sort_order='$val'", $id);

    admin_log(addslashes($exc->get_name($id)), 'edit', 'attribute');

    make_json_result(stripslashes($val));
}

/*------------------------------------------------------ */
//-- 删除商品属性
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'remove')
{
    check_authz_json('attr_manage');

    $id = intval($_GET['id']);

    $db->query("DELETE FROM " .$ecs->table('attribute'). " WHERE attr_id='$id'");
    $db->query("DELETE FROM " .$ecs->table('attribute_lang'). " WHERE attr_id='$id'");
    $db->query("DELETE ga.*, gal.* FROM " .$ecs->table('goods_attr'). " as ga ".
        "LEFT JOIN " .$ecs->table('goods_attr_lang'). " as gal ON ga.goods_attr_id = gal.goods_attr_id ".
        "WHERE ga.attr_id='$id'");
    $attrbuteController->auto_update_cat_filter_attr($id);

    $url = 'attribute.php?act=query&' . str_replace('act=remove', '', $_SERVER['QUERY_STRING']);

    ecs_header("Location: $url\n");
    exit;
}

/*------------------------------------------------------ */
//-- 获取某属性商品数量
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'get_attr_num')
{
    check_authz_json('attr_manage');

    $id = intval($_GET['attr_id']);

    $sql = "SELECT COUNT(*) ".
           " FROM " . $ecs->table('goods_attr') . " AS a, ".
           $ecs->table('goods') . " AS g ".
           " WHERE g.goods_id = a.goods_id AND g.is_delete = 0 AND attr_id = '$id' ";

    $goods_num = $db->getOne($sql);

    if ($goods_num > 0)
    {
        $drop_confirm = sprintf($_LANG['notice_drop_confirm'], $goods_num);
    }
    else
    {
        $drop_confirm = $_LANG['drop_confirm'];
    }

    make_json_result(array('attr_id'=>$id, 'drop_confirm'=>$drop_confirm));
}

/*------------------------------------------------------ */
//-- 获得指定商品类型下的所有属性分组
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'get_attr_groups')
{
    check_authz_json('attr_manage');

    $cat_id = intval($_GET['cat_id']);
    $groups = get_attr_groups($cat_id);

    make_json_result($groups);
}

/*------------------------------------------------------ */
//-- Batch handle goods_attr by attribute
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'attr_goods_list')
{
    /* 检查权限 */
    admin_priv('attr_manage');

    $attr_id = $_REQUEST['attr_id'];
    $filter['attr_id']    = $attr_id;
    $filter['keyword']    = empty($_REQUEST['keyword']) ? '' : trim($_REQUEST['keyword']);
    $filter['sort_by']    = empty($_REQUEST['sort_by']) ? 'goods_id' : trim($_REQUEST['sort_by']);
    $filter['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
    $data = $attrbuteController->get_attr_goods_list($attr_id, $filter);
    $smarty->assign('attr_id', $attr_id);
    $smarty->assign('ur_here', $data['attribute']['cat_name']."：".$data['attribute']['attr_name']);
    $smarty->assign('attribute', $data['attribute']);
    $smarty->assign('attr_values', $data['attr_values']);
    $smarty->assign('goods_list', $data['goods_list']);

    assign_query_info();

    /* Display */
    if($_REQUEST['act'] == 'attr_goods_list')
    {
        $smarty->assign('action_link', array('href' => 'attribute.php?act=list', 'text' => $_LANG['09_attribute_list']));
        $smarty->assign('full_page',    1);
        $smarty->display('goods_attr_by_attr.htm');
    }
    elseif($_REQUEST['act'] == 'query_attr_goods')
    {
        make_json_result($smarty->fetch('goods_attr_by_attr.htm'), '',
        array('filter' => $data['filter'], 'page_count' => $data['page_count']));
    }
}

/*------------------------------------------------------ */
//-- Submit batch handle goods_attr by attribute
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'submit_attr_goods')
{
    /* 检查权限 */
    admin_priv('attr_manage');

    $res = $attrbuteController->update_goods_attr_by_attr_value($_REQUEST);
    if($res)
    {
        $link[] = array('text' => $_LANG['back_list'], 'href' => 'attribute.php?act=attr_goods_list&attr_id='. $_REQUEST['attr_id']);
        sys_msg($_LANG['add_goods_attr_ok'], 0, $link);
    } else {
        $link[] = array('text' => $_LANG['back_list'], 'href' => 'attribute.php?act=attr_goods_list&attr_id='. $_REQUEST['attr_id']);
        sys_msg($_LANG['add_goods_attr_error'], 0, $link);
    }
}

/*------------------------------------------------------ */
//-- Batch handle goods_attr by category
/*------------------------------------------------------ */

elseif ($_REQUEST['act'] == 'cat_goods_list')
{
    /* 检查权限 */
    admin_priv('attr_manage');

    $filter['cat_id']     = $_REQUEST['cat_id'];
    $filter['keyword']    = empty($_REQUEST['keyword']) ? '' : trim($_REQUEST['keyword']);
    $filter['sort_by']    = empty($_REQUEST['sort_by']) ? 'goods_id' : trim($_REQUEST['sort_by']);
    $filter['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
    $data = $attrbuteController->get_cat_goods_list($_REQUEST['cat_id'], $filter);

    $smarty->assign('attr_list', $data['attr_list']);
    $smarty->assign('col_num', $data['col_num']);
    $smarty->assign('goods_list', $data['goods_list']);
    $smarty->assign('filter', $data['filter']);
    $smarty->assign('record_count', $data['record_count']);
    $smarty->assign('page_count',   $data['page_count']);
    $smarty->assign('ur_here', $data['cat_name']." ".$_LANG['batch_add_goods_attr']);
    $smarty->assign('cat_id', $_REQUEST['cat_id']);
    $sort_flag  = sort_flag($filter);
    $smarty->assign($sort_flag['tag'], $sort_flag['img']);
    assign_query_info();

    /* Display */
    if($_REQUEST['act'] == 'cat_goods_list')
    {
        $smarty->assign('action_link', array('href' => 'attribute.php?act=list', 'text' => $_LANG['09_attribute_list']));
        $smarty->assign('full_page',    1);
        $smarty->display('goods_attr_by_cat.htm');
    }
    elseif($_REQUEST['act'] == 'query_cat_goods')
    {
        make_json_result($smarty->fetch('goods_attr_by_cat.htm'), '',
        array('filter' => $data['filter'], 'page_count' => $data['page_count']));
    }

}

elseif ($_REQUEST['act'] == 'submit_cat_goods')
{
    /* 检查权限 */
    admin_priv('attr_manage');
    $res = $attrbuteController->update_goods_attr_by_cat($_REQUEST);
    if($res)
    {
        $link[] = array('text' => $_LANG['back_list'], 'href' => 'attribute.php?act=cat_goods_list&cat_id='. $_REQUEST['cat_id']);
        sys_msg($_LANG['add_goods_attr_ok'], 0, $link);
    } else {
        $link[] = array('text' => $_LANG['back_list'], 'href' => 'attribute.php?act=cat_goods_list&cat_id='. $_REQUEST['cat_id']);
        sys_msg($_LANG['add_goods_attr_error'], 0, $link);
    }
}

/*------------------------------------------------------ */
//-- PRIVATE FUNCTIONS
/*------------------------------------------------------ */


?>
