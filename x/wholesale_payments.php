<?php

/***
* wholesale_payments.php
* by howang 2014-12-02
*
* Accounting System
***/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_accounting.php');
$userController = new Yoho\cms\Controller\UserController();
$warehousePaymentController = new Yoho\cms\Controller\YohoBaseController("order_info");

if (!empty($_REQUEST['crm'])) {
    $smarty->assign('crm', 1);
}

if ($_REQUEST['act'] == 'list')
{
    /* 权限判断 */
    admin_priv('sale_order_stats');

    /* 时间参数 */
    $start_date = (empty($_REQUEST['start_date'])) ? month_start() : $_REQUEST['start_date'];
    $end_date = (empty($_REQUEST['end_date'])) ? month_end() : $_REQUEST['end_date'];
    $smarty->assign('ur_here',      '批發訂單付款紀錄');
    $smarty->assign('full_page',   1);
    $smarty->assign('users', $userController->getWholesaleUsers());
    $smarty->assign('start_date',   $start_date);
    $smarty->assign('end_date',     $end_date);
    $smarty->assign('action_link', array('text' => '批發訂單列表', 'href' => 'order.php?act=wholesale_list&is_wholesale=1'));
    $smarty->assign('accounts',     $actg->getAccounts());
    $smarty->assign('payment_types',$actg->getPaymentTypes());
    $smarty->assign('order_sn', $_REQUEST['order_sn']);
    assign_query_info();
    $smarty->display('wholesale_payments_list.htm');
}
elseif ($_REQUEST['act'] == 'query')
{
    /* 权限判断 */
    check_authz_json('sale_order_stats');
    
    $list = get_wholesale_payments();
    $warehousePaymentController->ajaxQueryAction($list['data'], $list['record_count'], false);
}
elseif ($_REQUEST['act'] == 'add')
{
    /* 权限判断 */
    admin_priv('sale_order_stats');
    
    $order_id = empty($_REQUEST['order_id']) ? 0 : intval($_REQUEST['order_id']);
    if (empty($order_id))
    {
        sys_msg('必須輸入訂單號');
    }
    require_once(ROOT_PATH . 'includes/lib_order.php');
    $order = order_info($order_id);
    if (!$order)
    {
        sys_msg('找不到該訂單');
    }
    // Only allow order with user 11111111, 88888888, 99999999
    if (!in_array($order['user_id'], $userController->getWholesaleUserIds()) && $order['order_type']!=Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE)
    {
        sys_msg('該訂單並不是批發訂單');
    }
    
    $smarty->assign('ur_here',     '批發訂單付款');
    
    $smarty->assign('action_link', array('text' => '訂單付款紀錄', 'href' => 'wholesale_payments.php?act=list&order_sn=' . $order['order_sn']));
    $smarty->assign('action_link2', array('text' => '返回訂單', 'href' => 'order.php?act=info&order_id=' . $order_id));
    $salesperson = $db->getCol("SELECT user_name as sales_name FROM ".$ecs->table('admin_user')." WHERE user_id=".$_SESSION['admin_id']);
    $smarty->assign('salesperson_list', $salesperson);
    $smarty->assign('admin_id', $_SESSION['admin_id']);
    $smarty->assign('admin_name', $_SESSION['admin_name']);
    
    $smarty->assign('accounts', $actg->getAccounts());
    $smarty->assign('payment_types', $actg->getPaymentTypes());
    
    $smarty->assign('order_id',    $order_id);
    $smarty->assign('order',       $order);

    $paid_amounts = $actgHooks->getWholesaleOrderPaidAmounts($order_id);
    if (empty($paid_amounts))
    {
        $currency = $actg->getCurrency(array('is_default' => 1));
        $paid_str = $actg->money_format(0, $currency['currency_format']);
        $order_total_str = $actg->money_format($order['order_amount'], $currency['currency_format']);
        $due_str = $actg->money_format($order['order_amount'], $currency['currency_format']);
        $due = $order['order_amount'];
    }
    else
    {
        $currencies = array();
        foreach ($actg->getCurrencies() as $row)
        {
            $currencies[$row['currency_code']] = $row;
            if ($row['is_default'])
            {
                $default_currency = $row;
            }
        }
        $paid_str = implode(' + ', array_map(function ($amount, $currency_code) use ($actg, $currencies) {
            $currency = $currencies[$currency_code];
            return $actg->money_format($amount, $currency['currency_format']);
        }, $paid_amounts, array_keys($paid_amounts)));
        $total = array_reduce(array_map(function ($amount, $currency_code) use ($currencies) {
            $currency = $currencies[$currency_code];
            return bcmul($amount, $currency['currency_rate']);
        }, $paid_amounts, array_keys($paid_amounts)), 'bcadd', 0);
        if ((count($paid_amounts) > 1) || (empty($paid_amounts[$default_currency['currency_code']])))
        {
            $paid_str .= ' = ' . $actg->money_format($total, $default_currency['currency_format']);
        }
        $order_total_str = $actg->money_format($order['order_amount'], $default_currency['currency_format']);
        $due_str = $actg->money_format(bcsub($order['order_amount'], $total), $default_currency['currency_format']);
        $due = bcsub($order['order_amount'], $total, 2);
    }
    $smarty->assign('paid_str',    $paid_str);
    $smarty->assign('order_total_str', $order_total_str);
    $smarty->assign('due_str', $due_str);
    $smarty->assign('due', $due);

    $smarty->assign('currencies', json_encode($actg->getCurrencies()));
    
    assign_query_info();
    $smarty->display('wholesale_payment_info.htm');
}
elseif ($_REQUEST['act'] == 'insert')
{
    /* 权限判断 */
    empty($_REQUEST['crm']) ? admin_priv('sale_order_stats') : check_authz_json('sale_order_stats');

    $order_id = empty($_POST['order_id']) ? 0 : intval($_POST['order_id']);
    if (empty($order_id))
    {
        empty($_REQUEST['crm']) ? sys_msg('必須輸入訂單號') : make_json_error('必須輸入訂單號');
    }

    require_once(ROOT_PATH . 'includes/lib_order.php');
    $order = order_info($order_id);
    if (!$order)
    {
        empty($_REQUEST['crm']) ? sys_msg('找不到該訂單') : make_json_error('找不到該訂單');
    }
    // Only allow order with user 11111111, 88888888, 99999999
    if (!in_array($order['user_id'], $userController->getWholesaleUserIds()) && $order['order_type']!=Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE)
    {
        empty($_REQUEST['crm']) ? sys_msg('該訂單並不是批發訂單') : make_json_error('該訂單並不是批發訂單');
    }

    if (empty($order['ar_txn_id'])) {
        empty($_REQUEST['crm']) ? sys_msg('請先建立批發訂單應收款項之會計記錄') : make_json_error('請先建立批發訂單應收款項之會計記錄');
    }

    $operator = empty($_POST['operator']) ? '' : trim($_POST['operator']);
    if (empty($operator))
    {
        empty($_REQUEST['crm']) ? sys_msg('必須輸入操作員') : make_json_error('必須輸入操作員');
    }

    $account_id = empty($_POST['account_id']) ? 0 : intval($_POST['account_id']);
    if (empty($account_id))
    {
        empty($_REQUEST['crm']) ? sys_msg('必須輸入帳戶') : make_json_error('必須輸入帳戶');
    }
    $acc = $actg->getAccount(compact('account_id'));
    if (empty($acc))
    {
        empty($_REQUEST['crm']) ? sys_msg('帳戶不正確') : make_json_error('帳戶不正確');
    }

    $currency_code = $acc['currency_code'];

    $completed = isset($_POST['completed']) ? (!empty($_POST['completed'])) : true;

    $amount = empty($_POST['amount']) ? 0.0 : doubleval($_POST['amount']);
    if (empty($amount))
    {
        empty($_REQUEST['crm']) ? sys_msg('必須輸入金額') : make_json_error('必須輸入金額');
    }

    $exchanged_amount = empty($_POST['exchanged_amount']) ? 0.0 : doubleval($_POST['exchanged_amount']);
    if (empty($exchanged_amount))
    {
        empty($_REQUEST['crm']) ? sys_msg('必須輸入港幣金額') : make_json_error('必須輸入金港幣額');
    }

    if (($completed) && (!$acc['allow_debit']) && (bccomp(bcadd($amount, $acc['account_balance']), 0) < 0))
    {
        empty($_REQUEST['crm']) ? sys_msg('金額超出帳戶結餘') : make_json_error('金額超出帳戶結餘');
    }

    $payment_type_id = empty($_POST['payment_type_id']) ? 0 : intval($_POST['payment_type_id']);
    if (empty($payment_type_id))
    {
        empty($_REQUEST['crm']) ? sys_msg('必須選擇付款方式') : make_json_error('必須選擇付款方式');
    }
    $pay_type = $actg->getPaymentType(compact('payment_type_id'));
    if (empty($pay_type))
    {
        empty($_REQUEST['crm']) ? sys_msg('付款方式不正確') : make_json_error('付款方式不正確');
    }

    $remark = empty($_POST['remark']) ? '' : trim($_POST['remark']);
    $type_sn = empty($_POST['type_sn']) ? '' : trim($_POST['type_sn']);

    $datetime = $actg->dbdate();

    $actg->start_transaction();
    $wholesale_payment = array(
        'order_id' => $order_id,
        'add_date' => $datetime,
        'complete_date' => $completed ? $datetime : null,
        'currency_code' => $currency_code,
        'amount' => $amount,
        'exchanged_amount' => $exchanged_amount,
        'payment_type_id' => $payment_type_id,
        'account_id' => $account_id,
        'operator' => $operator,
        'remark' => $remark,
        'type_sn' => $type_sn
    );
    $wholesale_payment_id = $actg->addWholesalePayment($wholesale_payment);

    if ($wholesale_payment_id)
    {
        $fully_paid = isset($_POST['fully_paid']) ? (!empty($_POST['fully_paid'])) : false;
        
        $paid_amounts = $actgHooks->getWholesaleOrderPaidAmounts($order_id);
        $total_paid = $actgHooks->calculateTotalPaidAmount($paid_amounts);
        
        $sql = "UPDATE " . $ecs->table('order_info') . 
            "SET `actg_paid_amount` = '" . $total_paid . "' , pay_status = ".($fully_paid?PS_PAYED:PS_PAYING)." " .
            ($fully_paid ? ", `fully_paid` = 1 " : '') .
            "WHERE `order_id` = '" . $order_id . "'";
        if (!$db->query($sql))
        {
            $actg->rollback_transaction();
            
            empty($_REQUEST['crm']) ? sys_msg('無法更新批發訂單') : make_json_error('付款方式不正確');
        }
        
        $actg->commit_transaction();
        
        $link[0]['text'] = '批發訂單列表';
        $link[0]['href'] = 'order.php?act=list&is_wholesale=1';
        $link[1]['text'] = '批發訂單付款紀錄';
        $link[1]['href'] = 'wholesale_payments.php?act=list';

        empty($_REQUEST['crm']) ? sys_msg('批發訂單付款成功', 0, $link, true) : make_json_response('批發訂單付款成功');
    }
    else
    {
        $actg->rollback_transaction();

        empty($_REQUEST['crm']) ? sys_msg('批發訂單付款失敗') : make_json_error('批發訂單付款失敗');
    }
}
elseif ($_REQUEST['act'] == 'complete')
{
    $is_ajax = empty($_REQUEST['is_ajax']) ? 0 : intval($_REQUEST['is_ajax']);
    $showError = function ($msg) use ($is_ajax) {
        if ($is_ajax)
        {
            make_json_error($msg);
        }
        else
        {
            sys_msg($msg);
        }
    };
    
    /* 权限判断 */
    if ($is_ajax)
    {
        check_authz_json('sale_order_stats');
    }
    else
    {
        admin_priv('sale_order_stats');
    }
    
    $order_id = empty($_REQUEST['order_id']) ? 0 : intval($_REQUEST['order_id']);
    if (empty($order_id))
    {
        $showError('必須輸入訂單號');
    }
    
    require_once(ROOT_PATH . 'includes/lib_order.php');
    $order = order_info($order_id);
    if (!$order)
    {
        $showError('找不到該訂單');
    }
    // Only allow order with user 11111111, 88888888, 99999999
    if (!in_array($order['user_id'], $userController->getWholesaleUserIds()) && $order['order_type']!=Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE)
    {
        $showError('該訂單並不是批發訂單');
    }
    
    $actg->start_transaction();
    
    $ok = $db->query("UPDATE " . $ecs->table('order_info') . " SET `fully_paid` = 1 WHERE `order_id` = '" . $order_id . "'");
    
    if ($ok)
    {
        $actg->commit_transaction();
        
        if ($is_ajax)
        {
            make_json_result('', '標示為已付款成功');
        }
        else
        {
            $link[0]['text'] = '返回批發訂單列表';
            $link[0]['href'] = 'order.php?act=list&is_wholesale=1';
            
            sys_msg('標示為已付款成功', 0, $link, true);
        }
    }
    else
    {
        $actg->rollback_transaction();
        
        $showError('標示為已付款失敗');
    }
}

function month_start()
{
    return local_date('Y-m-01');
}

function month_end()
{
    return local_date('Y-m-t');
}

function get_wholesale_payments()
{
    global $actg, $db;
    
    /* 排序参数 */
    $filter['sort_by']      = empty($_REQUEST['sort_by']) ? 'wholesale_payment_id' : trim($_REQUEST['sort_by']);
    $filter['sort_order']   = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
    
    /* 时间参数 */
    $filter['start_date'] = empty($_REQUEST['start_date']) ? month_start() : $_REQUEST['start_date'];
    $filter['end_date'] = empty($_REQUEST['end_date']) ? month_end() : $_REQUEST['end_date'];
    $start_date = $actg->dbdate(local_strtotime($filter['start_date']));
    $end_date = $actg->dbdate(local_strtotime($filter['end_date']) + 86399);
    
    /* 其他參數 */
    $filter['order_sn'] = empty($_REQUEST['order_sn']) ? '' : trim($_REQUEST['order_sn']);
    $filter['account'] = empty($_REQUEST['account']) ? 0 : intval($_REQUEST['account']);
    $filter['payment_type'] = empty($_REQUEST['payment_type']) ? 0 : intval($_REQUEST['payment_type']);
    $filter['user_id']  =  empty($_REQUEST['user_id']) ? 0 : intval($_REQUEST['user_id']);
    
    /* 查询数据的条件 */
    $where = "((t.add_date BETWEEN '" . $start_date . "' AND '" . $end_date . "') OR (at.created_at BETWEEN '" . $_REQUEST['start_date'] . "' AND '" . $_REQUEST['end_date'] . " 23:59:59')) " .
        (empty($filter['order_sn']) ? '' : " AND order_sn LIKE '%" . mysql_like_quote($filter['order_sn']) . "%' ") .
        (empty($filter['account']) ? '' : " AND a.account_id = '" . $filter['account'] . "' ") .
        (empty($filter['payment_type']) ? '' : " AND t.payment_type_id = '" . $filter['payment_type'] . "' ") .
        (empty($filter['user_id']) ? '' : " AND oi.user_id = ".$filter['user_id']." ");
    
    $sql = "SELECT count(*) " . 
            "FROM `actg_wholesale_payments` as wp " .
            "LEFT JOIN" . $GLOBALS['ecs']->table('order_info') . " as oi ON wp.order_id = oi.order_id " .
            "LEFT JOIN `actg_account_transactions` as t ON wp.txn_id = t.txn_id " .
            "LEFT JOIN `actg_accounting_transactions` as at ON wp.actg_txn_id = at.actg_txn_id " .
            "WHERE " . $where;
    $filter['record_count'] = $db->getOne($sql);

    /* 查询 */
    $data = $actg->getWholesalePayments($where, array(
        'orderby' => $filter['sort_by'] . ' ' . $filter['sort_order'],
        'limit' => $_REQUEST['start'] . ',' . $_REQUEST['page_size']
    ));
    
    $admins = $db->getAll("SELECT user_id, user_name FROM " . $GLOBALS['ecs']->table('admin_user'));
    foreach ($data as $key => $row)
    {
        $data[$key]['add_date_formatted'] = local_date($GLOBALS['_CFG']['time_format'], strtotime($row['add_date']));
        $data[$key]['complete_date_formatted'] = local_date($GLOBALS['_CFG']['time_format'], strtotime($row['complete_date']));
        if (!$row['complete_date']) {
            $data[$key]['complete_date_formatted'] = "<a href='javascript:void(0)' onclick='javascript:completeTransaction(\"$row[txn_id]\")' title='按此完成交易'>未完成</a>";
        }
        $data[$key]['order_sn'] = empty($row['order_sn']) ? $row['order_sn'] : "<a href='order.php?act=info&order_id=$row[order_id]'>$row[order_sn]</a>";
        $data[$key]['order_amount_formatted'] = $actg->money_format($row['order_amount'], $row['currency_format']);
        $data[$key]['amount_formatted'] = $actg->money_format($row['amount'], $row['currency_format']);
        $data[$key]['fully_paid'] = $row['pay_status']==PS_PAYED?true:false;
        if (!$data[$key]['fully_paid']) {
            $data[$key]['order_amount_formatted'] = "<span style='color:red'>" . $data[$key]['order_amount_formatted'] . "</span>";
        }
        foreach ($admins as $admin) {
            if ($admin['user_id'] == $row['operator'] || strtolower($admin['user_name']) == strtolower($row['operator'])) {
                $data[$key]['operator'] = $admin['user_name'];
                break;
            }
        }
    }
    
    $arr = array('data' => $data, 'filter' => $filter, 'page_count' => $filter['page_count'], 'record_count' => $filter['record_count']);
    
    return $arr;
}

?>