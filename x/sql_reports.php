<?php

/***
* sql_reports.php
* by billyLee 2019-04-25
*
* SQL Reports 
***/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
$sqlReportController = new Yoho\cms\Controller\SqlReportController();

if (($_REQUEST['act'] == 'main') || (empty($_REQUEST['act'])))
{
    $sqlReportController->showSqlReportPage();
}
 elseif ($_REQUEST['act'] == 'customer_report')
{
    $sqlReportController->getCustomerPurchaseQuery($_REQUEST['query']);
}
elseif ($_REQUEST['act'] == 'accounting_entry_report')
{
    $sqlReportController->getAccountingEntryRecordQuery($_REQUEST['query']);
}
elseif ($_REQUEST['act'] == 'user_list_report')
{
    $sqlReportController->getUserListQuery($_REQUEST['query']);
}
elseif($_REQUEST['act'] == 'query_queue')
{
    $sqlReportController->getReportQueryQueue(!empty($_REQUEST['type']) ? $_REQUEST['type'] : null);
}
elseif($_REQUEST['act'] == 'download_report')
{
    $sqlReportController->downloadReport($_REQUEST['query_id']);
}

?>
