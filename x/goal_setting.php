<?php
/**
 * goal_setting.php
 */
define('IN_ECS', true);

require dirname(__FILE__) . '/includes/init.php';
require_once ROOT_PATH . 'includes/cls_image.php';
$statisticsController = new Yoho\cms\Controller\StatisticsController();

if (empty($_REQUEST['act'])) {
    $_REQUEST['act'] = 'menu';
} else {
    $_REQUEST['act'] = trim($_REQUEST['act']);
}

$smarty->assign('lang', $_LANG);

if ($_REQUEST['act'] == 'menu') {
    $statisticsController->menuAction();

} elseif ($_REQUEST['act'] == "edit" || $_REQUEST['act'] == "add") {
    $statisticsController->addAction();
} elseif ($_REQUEST['act'] == "get_create_step") {
    $statisticsController->getFormNextStep($_GET);
} elseif ($_REQUEST['act'] == "create") {
    $statisticsController->insertAction();
} elseif ($_REQUEST['act'] == 'list') {
    $statisticsController->listAction();
} elseif ($_REQUEST['act'] == 'remove') {
    $statisticsController->ajaxRemoveAction();
} elseif ($_REQUEST['act'] == 'upload') {
    $statisticsController->uploadAction();
} elseif ($_REQUEST['act'] == 'import') {
    $statisticsController->importAction($_REQUEST);
}