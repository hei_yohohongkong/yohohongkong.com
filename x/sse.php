<?php

define('IN_ECS', true);

require dirname(__FILE__) . '/includes/init.php';

$target = trim($_REQUEST['target']);

header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');

if (!empty($target)) {
    $lastEventId = $_SERVER["HTTP_LAST_EVENT_ID"];
    $eventData = "";
    if (empty($lastEventId)) {
        $eventData .= "id: " . gmtime() . PHP_EOL;
        $eventData .= "event: start" . PHP_EOL;
        $eventData .= "data: started" . PHP_EOL;
    } else {
        $time = $lastEventId;
        $messages = $db->getAll("SELECT * FROM " . $ecs->table("sse") . " WHERE time >= $time AND target = '$target'");
        $eventData .= "id: " . gmtime() . PHP_EOL;
        foreach ($messages as $msg) {
            if (!in_array($msg['data'], $events[$msg['event']])) {
                $events[$msg['event']][] = $msg['data'];
            }
        }
        if (empty($events)) {
            $eventData .= "event: wait" . PHP_EOL;
            $eventData .= "data: waiting" . PHP_EOL;
        } else {
            $eventData .= "event: $target" ."Update" . PHP_EOL;
            $eventData .= "data: " . json_encode($events) . PHP_EOL;
        }
    }
    if (!empty($eventData)) {
        echo $eventData . PHP_EOL;
    }
}