<?php

/***
* review_sales.php
* by howang 2014-08-15
*
* View user's review on our sales
***/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
$commentController = new Yoho\cms\Controller\CommentController();
$smarty->assign('lang', $_LANG);

if ((isset($_REQUEST['act'])) && ($_REQUEST['act'] == 'query'))
{
    /* 检查权限 */
    check_authz_json('sale_order_stats');
    
    $controller = new Yoho\cms\Controller\YohoBaseController();
    $res = $commentController->getReviewList($commentController::REVIEW_SALES_TABLE_ID);
    $controller->ajaxQueryAction($res['data'], $res['record_count'], false);
}
elseif ($_REQUEST['act'] == 'edit_remark')
{
    /* 检查权限 */
    check_authz_json('sale_order_stats');
    
    $review_id = intval($_POST['id']);
    $value = trim($_POST['val']);
    
    $db->query("UPDATE " . $ecs->table('review_sales') . " " .
                "SET `remark` = '" . $value . "' " .
                "WHERE `review_id` = '" . $review_id . "'");
    make_json_result(stripslashes($value));
}
/*------------------------------------------------------ */
//--商品明细列表
/*------------------------------------------------------ */
else
{
    /* 权限判断 */
    admin_priv('sale_order_stats');

    $avgs = $commentController->getReviewAvgScore($commentController::REVIEW_SALES_TABLE_ID);
    $smarty->assign('items', $avgs['items']);
    $smarty->assign('avg', $avgs['avg']);
    $smarty->assign('admin_avg', $avgs['admin_avg']);
    $smarty->assign('min_year', $avgs['min_year']);

    $smarty->assign('ur_here',      $_LANG['review_sales']);
    $smarty->assign('full_page',    1);
    
    /* 显示页面 */
    assign_query_info();
    $smarty->display('review_sales.htm');
}

function get_sales_review($is_pagination = true)
{
    global $db, $ecs, $_CFG;
    
    $filter['sales_id']   = empty($_REQUEST['sales_id'])   ? 0 : intval($_REQUEST['sales_id']);
    
    $filter['sort_by']    = empty($_REQUEST['sort_by'])    ? 'time' : trim($_REQUEST['sort_by']);
    $filter['sort_order'] = empty($_REQUEST['sort_order']) ? 'DESC' : trim($_REQUEST['sort_order']);
    
    $sql = "SELECT count(*) FROM " . $ecs->table('review_sales') . (!empty($filter['sales_id']) ? " WHERE `sales_id` = " . $filter['sales_id'] . " " : '');
    $filter['record_count'] = $db->getOne($sql);
    
    /* 分页大小 */
    $filter = page_and_size($filter);
    
    $sql = "SELECT r.`review_id`, r.`user_id`, u.`user_name`, rei.`content` as display_name, r.`sales_id`, s.`sales_name`, r.`order_id`, o.`order_sn`, r.`score1`, r.`score2`, r.`score3`, r.`comment`, r.`time`, r.`remark` ".
           "FROM " . $ecs->table('review_sales') . " as r " .
           "LEFT JOIN " . $ecs->table('salesperson') . " as s ON r.`sales_id` = s.`sales_id` " .
           "LEFT JOIN " . $ecs->table('users') . " as u ON r.`user_id` = u.`user_id` " .
           "LEFT JOIN " . $ecs->table('reg_extend_info') . " as rei ON rei.`user_id` = u.`user_id` AND rei.`reg_field_id` = 10 " .
           "LEFT JOIN " . $ecs->table('order_info') . " as o ON r.`order_id` = o.`order_id` " .
           (!empty($filter['sales_id']) ? "WHERE r.`sales_id` = " . $filter['sales_id'] . " " : '') .
           "ORDER BY " . $filter['sort_by'] . " " . $filter['sort_order'];
    
    if ($is_pagination)
    {
        $sql .= " LIMIT " . $filter['start'] . ', ' . $filter['page_size'];
    }
    $data = $db->getAll($sql);
    
    foreach ($data as $key => $row)
    {
        $data[$key]['date'] = local_date($_CFG['date_format'], $row['time']);
        $data[$key]['display_name'] = empty($row['display_name']) ? $row['user_name'] : $row['display_name'];
    }
    
    $arr = array(
        'data' => $data,
        'filter' => $filter,
        'page_count' => $filter['page_count'],
        'record_count' => $filter['record_count']
    );
    return $arr;
}

?>