<?php

define('IN_ECS',true);
require_once(dirname(__FILE__) .'/includes/init.php');
require_once(dirname(__FILE__) .'/includes/ERP/lib_erp_base.php');
require_once(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require_once(dirname(__FILE__) .'/includes/ERP/lib_erp_goods.php');
require_once(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
require_once(dirname(__FILE__) .'/includes/ERP/lib_erp_warehouse.php');
require_once(dirname(__FILE__) .'/includes/ERP/cls/cls_date.php');
require_once(dirname(__FILE__) .'/../includes/lib_order.php');
$deliveryOrderController = new Yoho\cms\Controller\DeliveryOrderController();
$adminuserController = new Yoho\cms\Controller\AdminuserController();
$orderWholesaleDeliveryController = new Yoho\cms\Controller\OrderWholesaleDeliveryController();

if($_REQUEST['act'] == 'list' || $_REQUEST['act'] == ''){
	if( admin_priv('erp_warehouse_view','',false) || admin_priv('erp_warehouse_manage','',false) || admin_priv('erp_warehouse_approve','',false))
	{
        include('./includes/ERP/page.class.php');

		if (isset($_REQUEST['goods_sn']) &&!empty($_REQUEST['goods_sn'])) {
			$filter['goods_sn'] = $_REQUEST['goods_sn'];
		}

        $status_options = $deliveryOrderController->getWholesaleDeliveryStatusOption();
        $smarty->assign('status_options', $status_options);
		$smarty->assign('filter', $filter);
	//	$smarty->assign('type_options', $warehouseTransferController->getTypeOptions());
    //    $smarty->assign('status_options', $warehouseTransferController->getStatusOptions());
	//	$smarty->assign('reason_type_options', $warehouseTransferController->getReasonTypeOptions());
	//	$smarty->assign('warehouse_list_options', $warehouseTransferController->getWareHouseListOptions());
	//	$smarty->assign('operator_options', $warehouseTransferController->personInChargeOption(false));
        $smarty->assign('ur_here', '批發單執貨列表');
        $smarty->assign('full_page', 1);
        $smarty->assign('start_date', $result['start_date']);
        $smarty->assign('end_date', $result['end_date']);
        $smarty->assign('cfg_lang', $_CFG['lang']);
        $smarty->assign('lang', $_LANG);
        $smarty->display('erp_order_wholesale_delivery_list.htm');
    }else{
        $href="index.php";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
    }
} elseif ($_REQUEST['act'] == 'query') {
    $result = $orderWholesaleDeliveryController->getOrderWholesaleDeliveryList();
    $orderWholesaleDeliveryController->ajaxQueryAction($result['data'],$result['record_count'],false);
} elseif ($_REQUEST['act'] == 'post_order_wholesale_delivery_follow_up_remark_submit') {
    $result = $orderWholesaleDeliveryController->orderWholesaleDeliveryFollowUpRemarkSubmit();
    if($result !== false){
        make_json_result($result);
    }else{
        make_json_error('更新時發生錯誤');
    }
} elseif ($_REQUEST['act'] == 'dn_picked_confirm') {
	$delivery_id = $_REQUEST['delivery_id'];
	$delivery_status = $_REQUEST['delivery_status'];
	$operator = erp_get_admin_id();
	
    $res = $deliveryOrderController->wholesale_dn_confirm($delivery_id,$delivery_status,$operator);

    make_json_result($res);
}


?>