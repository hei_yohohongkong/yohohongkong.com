<?php
/**
 * staff_purchase.php
 */
define('IN_ECS', true);
require(dirname(__FILE__) . '/includes/init.php');
$adminuserController = new Yoho\cms\Controller\AdminuserController();
$admin_priv = $adminuserController::DB_ACTION_CODE_STAFF_PURCHASE;
if(empty($_REQUEST['act'])) $_REQUEST['act'] = 'list';
if ($_REQUEST['act'] == 'list')
{
    $smarty->assign('ur_here',      $_LANG['staff_purchase_list']);
    $smarty->assign('action_link',  array('text' => $_LANG['staff_purchase_add'], 'href' => 'staff_purchase.php?act=add'));
    assign_query_info();
    $smarty->display('staff_purchase_list.htm');
}
elseif ($_REQUEST['act'] == 'query')
{
    $adminuserController->ajaxQueryStaffPurchaseAction();
}
elseif ($_REQUEST['act'] == 'add')
{
    $smarty->assign('ur_here',      $_LANG['staff_purchase_add']);
    $smarty->assign('action_link',  array('text' => $_LANG['staff_purchase_list'], 'href' => 'staff_purchase.php?act=list'));
    $smarty->assign('cat_list', cat_list(0, $goods['cat_id']));
    $smarty->assign('brand_list', get_brand_list());
    $smarty->display('staff_purchase_apply.htm');
}
elseif ($_REQUEST['act'] == 'apply') {
    $success = $adminuserController->applyStaffPurchase($_POST);
    if($success) {
        /* 清除缓存 */
        clear_cache_files();
        $link[0]['text'] = $_LANG['staff_purchase_list'];
        $link[0]['href'] = 'staff_purchase.php?act=list';
        $note = '你的申請已經提交';
        sys_msg($note, 0, $link);
    } else {
        sys_msg('提交失敗', 1);
    }
}
elseif ($_REQUEST['act'] == 'view' ) {
    $data = $adminuserController->getStaffPurchase($_REQUEST['id']);
    $smarty->assign('ur_here',      $_LANG['staff_purchase_info']);
    $smarty->assign('action_link',  array('text' => $_LANG['staff_purchase_list'], 'href' => 'staff_purchase.php?act=list'));
    $smarty->assign('staff_purchase', $data);
    $smarty->display('staff_purchase_info.htm');
}
elseif ($_REQUEST['act'] == 'approve') {
    admin_priv($admin_priv);
    $data = $adminuserController->getStaffPurchase($_REQUEST['id'], 1);
    $smarty->assign('ur_here',      $_LANG['staff_purchase_info']);
    $smarty->assign('action_link',  array('text' => $_LANG['staff_purchase_list'], 'href' => 'staff_purchase.php?act=list'));
    $smarty->assign('staff_purchase', $data);
    $smarty->assign('approve', 1);
    $smarty->display('staff_purchase_info.htm');
}
elseif ($_REQUEST['act'] == 'get_goods_list')
{
    include_once(ROOT_PATH . 'includes/cls_json.php');
    $json = new JSON;

    $filters = $json->decode($_GET['JSON']);

    $arr = get_goods_list($filters);
    $opt = array();

    foreach ($arr AS $key => $val)
    {
        $opt[] = array('value' => $val['goods_id'],
                        'text' => $val['goods_name'],
                        'data' => $val['shop_price']);
    }

    make_json_result($opt);
}
elseif ($_REQUEST['act'] == 'get_approve_price')
{
    include_once(ROOT_PATH . 'includes/cls_json.php');
    $json = new JSON;
    $goodsController = new Yoho\cms\Controller\GoodsController();
    $crp = ['srp_percentage' => $_POST['per'], 'crp_type' => $_POST['type']];
    $sp_price = $goodsController->corporatePriceSuggestion([], $_POST['goods_id'], $crp);

    $opt = array('price' => $sp_price);

    make_json_result($opt);
}
elseif($_REQUEST['act'] == 'to_approve') {
    $success = $adminuserController->approveStaffPurchase($_POST);
    if($success) {
        /* 清除缓存 */
        clear_cache_files();
        $link[0]['text'] = $_LANG['staff_purchase_list'];
        $link[0]['href'] = 'staff_purchase.php?act=list';
        $note = '完成審核';
        sys_msg($note, 0, $link);
    } else {
        sys_msg('提交失敗', 1);
    }
}
?>