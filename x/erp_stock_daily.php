<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require_once(ROOT_PATH .ADMIN_PATH .'/includes/ERP/lib_erp_warehouse.php');
require_once(ROOT_PATH .ADMIN_PATH .'/includes/ERP/lib_erp_goods_attr.php');
require_once(ROOT_PATH .ADMIN_PATH .'/includes/ERP/lib_erp_goods.php');
require_once(ROOT_PATH .ADMIN_PATH .'/includes/ERP/cls/cls_date.php');
$cls_date=new cls_date();
$smarty->assign('lang',$_LANG);
$_REQUEST['act']=empty($_REQUEST['act'])?'list': trim($_REQUEST['act']);
if(!admin_priv('erp_warehouse_view','',false) &&!admin_priv('erp_warehouse_manage','',false) &&!admin_priv('erp_warehouse_approve','',false))
{
$href="index.php?act=main";
$text=$_LANG['erp_retun_to_center'];
$link[] = array('href'=>$href,'text'=>$text);
sys_msg($_LANG['erp_no_permit'],0,$link);
}
if($_REQUEST['act'] == 'list')
{
$first_date=$cls_date->get_first_day(local_date('Y-m-d',gmtime()));
$last_date=$cls_date->get_last_day(local_date('Y-m-d',gmtime()));
$start_date=isset($_REQUEST['start_date']) &&!empty($_REQUEST['start_date'])?trim($_REQUEST['start_date']):$first_date;
$end_date=isset($_REQUEST['end_date']) &&!empty($_REQUEST['end_date'])?trim($_REQUEST['end_date']):$last_date;
$warehouse_id=isset($_REQUEST['warehouse_id']) &&intval($_REQUEST['warehouse_id'])>0 ?intval($_REQUEST['warehouse_id']):0;
$goods_sn=isset($_REQUEST['goods_sn'])?trim($_REQUEST['goods_sn']):'';
$ids=isset($_REQUEST['ids'])?trim($_REQUEST['ids']):'';
$agency_id=get_admin_agency_id($_SESSION['admin_id']);
$warehouse_list=get_warehouse_list(0,$agency_id,1);
$warehouse_ids=array_keys($warehouse_list);
if($warehouse_id>0 &&!in_array($warehouse_id,$warehouse_ids))
{
$href="erp_stock_daily.php?act=list";
$text=$_LANG['erp_stock_daily_return'];
$link[] = array('href'=>$href,'text'=>$text);
sys_msg($_LANG['erp_stock_daily_no_agency_access'],0,$link);
}
$sql="select goods_name,goods_id from ".$ecs->table('goods')." where goods_sn='".$goods_sn."'";
$goods_info=$db->getRow($sql);
$goods_id=$goods_info['goods_id'];
$ids=!empty($ids)?trim($ids,',') : '';
$attr_id=get_attr_id($goods_id,$ids);
$goods_attr=get_attr_info($attr_id);
$attrs='';
if(!empty($goods_attr['attr_info']))
{
foreach($goods_attr['attr_info'] as $key =>$attr)
{
$attrs.=$attr['attr_name']."：".$attr['attr_value'].' ';
}
}
$start_time=$cls_date->date_to_stamp($start_date);
$end_time=$cls_date->date_to_stamp($end_date);
$sql="select max(stock) as max_stock from ".$GLOBALS['ecs']->table('erp_stock')." where goods_id='".$goods_id."' and goods_attr_id='".$attr_id."' and warehouse_id='".$warehouse_id."' and act_time>=".$start_time." and act_time<=".$end_time."";
$max_stock=$GLOBALS['db']->getOne($sql);
$max_stock=empty($max_stock)?0 : $max_stock;
$sql="select stock_id,goods_id,goods_attr_id,stock,act_time,act_date from ".$GLOBALS['ecs']->table('erp_stock')." where goods_id='".$goods_id."' and goods_attr_id='".$attr_id."' and warehouse_id='".$warehouse_id."' and act_time>=".$start_time." and act_time<=".$end_time." order by stock_id asc";
$stock_list=$GLOBALS['db']->getAll($sql);
$stock_data="<graph caption='".$_LANG['erp_stock_daily']."(".$goods_info['goods_name'].' '.$attrs.")"."' shownames='1' showvalues='1' decimalPrecision='0' yaxisminvalue='0' yaxismaxvalue='".$max_stock."' animation='1' outCnvBaseFontSize='12' baseFontSize='12' xaxisname='".$_LANG['erp_stock_daily_date']."' yaxisname='".$_LANG['erp_stock_daily_stock']."' >";
if(!empty($stock_list))
{
foreach($stock_list as $key =>$stock)
{
$act_date=empty($stock['act_date'])?local_date('Y-m-d',$stock['act_time']) : $stock['act_date'];
$stock_data.="<set name='".$act_date."' value='".$stock['stock']."' color='33FF66' />";
}
}
$stock_data.="</graph>";
$smarty->assign('stock_data',$stock_data);
$smarty->assign('start_date',$start_date);
$smarty->assign('end_date',$end_date);
$smarty->assign('warehouse_id',$warehouse_id);
$smarty->assign('goods_sn',$goods_sn);
$smarty->assign('warehouse_list',$warehouse_list);
$smarty->assign('ur_here',$_LANG['erp_stock_daily']);
$smarty->display('erp_stock_daily.htm');
}
elseif ($_REQUEST['act'] == 'check_goods_sn')
{
include('../includes/cls_json.php');
$json  = new JSON;
$goods_sn=trim($_REQUEST['goods_sn']);
if(!check_goods_sn($goods_sn))
{
$result['error']=1;
$result['message']=$_LANG['erp_delivery_wrong_goods_sn'];
die($json->encode($result));
}
else{
$sql="select goods_id from ".$GLOBALS['ecs']->table('goods')." where goods_sn='".$goods_sn."'";
$goods_id=$GLOBALS['db']->getOne($sql);
$goods_attr=goods_attr($goods_id);
$smarty->assign('goods_attr',$goods_attr);
$attr_info=$smarty->fetch('erp_select_goods_attr.htm');
$result['attr_info']=$attr_info;
$result['attr_num']=count($goods_attr);
$result['error']=0;
die($json->encode($result));
}
}
elseif ($_REQUEST['act'] == 'goods_sn_tips')
{
include('../includes/cls_json.php');
$json  = new JSON;
$goods_sn=trim($_REQUEST['goods_sn']);
$fetch_goods_sn=array();
$sql="select g.goods_sn from ".$GLOBALS['ecs']->table('goods')." as g ";
$sql.=" where g.goods_sn like '%".$goods_sn."%' order by goods_id desc limit 10";
$res=$GLOBALS['db']->query($sql);
while($row=mysql_fetch_assoc($res))
{
$fetch_goods_sn[]=$row['goods_sn'];
}
$smarty->assign('goods_sn',$fetch_goods_sn);
$content=$smarty->fetch('erp_goods_sn_tips.htm');
$result['error']=0;
$result['content']=$content;
$result['num']=count($fetch_goods_sn);
die($json->encode($result));
}
?>
