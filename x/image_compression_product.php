<?php

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require(ROOT_PATH . 'includes/lib_order.php');
require_once(ROOT_PATH .'/languages/zh_tw/admin/order.php');

$imageCompressionController = new Yoho\cms\Controller\ImageCompressionController();

if (empty($_REQUEST['act'])) {
    $_REQUEST['act'] = 'list';
}

if (isset($_REQUEST['act']) && ($_REQUEST['act'] == 'query')) {
    /* 检查权限 */
    $result = $imageCompressionController->getProductList();
    $imageCompressionController->ajaxQueryAction($result['data'],$result['record_count'],false,$result['extra_data']);
}else if (isset($_REQUEST['act']) && $_REQUEST['act'] == 'list') {
    $date_options = array('2018-06月' => '2018-06-01,2018-07-01','2018-05月'=>'2018-05-01,2018-06-01','2018-04月'=>'2018-04-01,2018-05-01');
    // print_r($date_options);
    // exit;
    $smarty->assign('date_options', $date_options);
    $smarty->assign('ur_here', '舊產品圖片處理');
    $smarty->assign('image_compression_limit', $_CFG['lang']);
    $smarty->assign('cfg_tiny_limit', $_CFG['tiny_limit']);
    $smarty->assign('lang', $_LANG);
    /* 显示页面 */
    assign_query_info();
    $smarty->display('image_compression_product.htm');
}else if (isset($_REQUEST['act']) && $_REQUEST['act'] == 'post_image_compression_process') {
    set_time_limit(0);
    $result = $imageCompressionController->image_compression_process();
    if(!isset($result['error'])){
        make_json_result($result);
    }else{
     	make_json_error(implode('<br>',$result['error']));
    }
}


