<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require_once(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require_once(dirname(__FILE__) .'/includes/ERP/lib_erp_finance.php');
require_once(dirname(__FILE__) .'/includes/ERP/lib_erp_supplier.php');
require_once(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
if ($_REQUEST['act'] == 'edit_gathering')
{
include('../includes/cls_json.php');
$json  = new JSON;
if(!admin_priv('erp_finance_manage','',false))
{
$result['error']=1;
$result['message']=$_LANG['erp_no_permit'];
die($json->encode($result));
}
$gathering_id=isset($_REQUEST['gathering_id']) &&intval($_REQUEST['gathering_id'])>0?intval($_REQUEST['gathering_id']):0;
$gathering_info=is_gathering_exists($gathering_id);
if(empty($gathering_info))
{
$result['error']=4;
$result['message']=$_LANG['erp_gathering_not_exist'];
die($json->encode($result));
}
$agency_id=get_admin_agency_id($_SESSION['admin_id']);
if($agency_id >0 &&$agency_id!=$gathering_info['agency_id'])
{
$result['error']=4;
$result['message']=$_LANG['erp_gathering_no_agency_access'];
die($json->encode($result));
}
if($gathering_info['gathering_status']!=1)
{
$result['error']=4;
$result['message']=$_LANG['erp_gathering_no_acts'];
die($json->encode($result));
}
if(lock_gathering($gathering_id,'edit'))	
{
if(gathering_act_record($gathering_id,'modify'))
{
$result['error']=0;
die($json->encode($result));
}
}
else{
$result['error']=-1;
$result['message']=$_LANG['erp_gathering_no_accessibility'];
die($json->encode($result));
}
}
elseif ($_REQUEST['act'] == 'change_order')
{
include('../includes/cls_json.php');
$json  = new JSON;
if(!admin_priv('erp_finance_manage','',false))
{
$result['error']=1;
$result['message']=$_LANG['erp_no_permit'];
die($json->encode($result));
}
$gathering_id=isset($_REQUEST['gathering_id']) &&intval($_REQUEST['gathering_id'])>0?intval($_REQUEST['gathering_id']):0;
$order_id=isset($_REQUEST['order_id']) &&intval($_REQUEST['order_id'])>0?intval($_REQUEST['order_id']):0;
$gather_amount=isset($_REQUEST['gather_amount']) &&floatval($_REQUEST['gather_amount'])>0?floatval($_REQUEST['gather_amount']):0;
$gathering_info=is_gathering_exists($gathering_id);
if(empty($gathering_info))
{
$result['error']=4;
$result['message']=$_LANG['erp_gathering_not_exist'];
die($json->encode($result));
}
$agency_id=get_admin_agency_id($_SESSION['admin_id']);
if($agency_id >0 &&$agency_id!=$gathering_info['agency_id'])
{
$result['error']=4;
$result['message']=$_LANG['erp_gathering_no_agency_access'];
die($json->encode($result));
}
if($gathering_info['gathering_status']!=1)
{
$result['error']=4;
$result['message']=$_LANG['erp_gathering_no_acts'];
die($json->encode($result));
}
if(lock_gathering($gathering_id,'edit'))	
{
if(gathering_act_record($gathering_id,'modify'))
{
update_gathering($gathering_id,$order_id,$gather_amount);
$gathering_info=get_gathering_info($gathering_id);
$result['pay_from']=$gathering_info[0]['user_info']['user_name'];
$result['bill_date']=$gathering_info[0]['bill_date'];
$result['bill_amount']=$gathering_info[0]['bill_amount'];
$result['bill_total_receivable']=$gathering_info[0]['bill_total_receivable'];
$result['bill_gathered_receivable']=$gathering_info[0]['bill_gathered_receivable'];
$result['bill_receivable_balance']=$gathering_info[0]['bill_receivable_balance'];
$result['gather_amount']=$gathering_info[0]['gather_amount'];
$result['bill_no']=$gathering_info[0]['order_info']['order_sn'];
$result['error']=0;
die($json->encode($result));
}
}
else{
$result['error']=-1;
$result['message']=$_LANG['erp_gathering_no_accessibility'];
die($json->encode($result));
}
}
elseif ($_REQUEST['act'] == 'change_bank_account')
{
include('../includes/cls_json.php');
$json  = new JSON;
if(!admin_priv('erp_finance_manage','',false))
{
$result['error']=1;
$result['message']=$_LANG['erp_no_permit'];
die($json->encode($result));
}
$gathering_id=isset($_REQUEST['gathering_id']) &&intval($_REQUEST['gathering_id'])>0?intval($_REQUEST['gathering_id']):0;
$bank_account_id=isset($_REQUEST['bank_account_id']) &&intval($_REQUEST['bank_account_id'])>0?intval($_REQUEST['bank_account_id']):0;
$gathering_info=is_gathering_exists($gathering_id);
if(empty($gathering_info))
{
$result['error']=4;
$result['message']=$_LANG['erp_gathering_not_exist'];
die($json->encode($result));
}
$agency_id=get_admin_agency_id($_SESSION['admin_id']);
if($agency_id >0 &&$agency_id!=$gathering_info['agency_id'])
{
$result['error']=4;
$result['message']=$_LANG['erp_gathering_no_agency_access'];
die($json->encode($result));
}
if($gathering_info['gathering_status']!=1)
{
$result['error']=4;
$result['message']=$_LANG['erp_gathering_no_acts'];
die($json->encode($result));
}
if(lock_gathering($gathering_id,'edit'))	
{
if(gathering_act_record($gathering_id,'modify'))
{
$sql="update ".$GLOBALS['ecs']->table('erp_gathering')." set account_id='".$bank_account_id."' where gathering_id='".$gathering_id."'";
$GLOBALS['db']->query($sql);
$account_info=get_bank_account_info($bank_account_id);
$result['error']=0;
$result['account_name']=$account_info['account_name'];
die($json->encode($result));
}
}
else{
$result['error']=-1;
$result['message']=$_LANG['erp_gathering_no_accessibility'];
die($json->encode($result));
}
}
elseif ($_REQUEST['act'] == 'change_gathering_remark')
{
include('../includes/cls_json.php');
$json  = new JSON;
if(!admin_priv('erp_finance_manage','',false))
{
$result['error']=1;
$result['message']=$_LANG['erp_no_permit'];
die($json->encode($result));
}
$gathering_id=isset($_REQUEST['gathering_id']) &&intval($_REQUEST['gathering_id'])>0?intval($_REQUEST['gathering_id']):0;
$gathering_remark=isset($_REQUEST['gathering_remark'])?trim($_REQUEST['gathering_remark']):'';
$gathering_info=is_gathering_exists($gathering_id);
if(empty($gathering_info))
{
$result['error']=4;
$result['message']=$_LANG['erp_gathering_not_exist'];
die($json->encode($result));
}
$agency_id=get_admin_agency_id($_SESSION['admin_id']);
if($agency_id >0 &&$agency_id!=$gathering_info['agency_id'])
{
$result['error']=4;
$result['message']=$_LANG['erp_gathering_no_agency_access'];
die($json->encode($result));
}
if($gathering_info['gathering_status']!=1)
{
$result['error']=4;
$result['message']=$_LANG['erp_gathering_no_acts'];
die($json->encode($result));
}
if(lock_gathering($gathering_id,'edit'))	
{
if(gathering_act_record($gathering_id,'modify'))
{
$sql="update ".$GLOBALS['ecs']->table('erp_gathering')." set gathering_remark='".$gathering_remark."' where gathering_id='".$gathering_id."'";
$GLOBALS['db']->query($sql);
$result['error']=0;
die($json->encode($result));
}
}
else{
$result['error']=-1;
$result['message']=$_LANG['erp_gathering_no_accessibility'];
die($json->encode($result));
}
}
elseif ($_REQUEST['act'] == 'change_gather_amount')
{
include('../includes/cls_json.php');
$json  = new JSON;
if(!admin_priv('erp_finance_manage','',false))
{
$result['error']=1;
$result['message']=$_LANG['erp_no_permit'];
die($json->encode($result));
}
$gathering_id=isset($_REQUEST['gathering_id']) &&intval($_REQUEST['gathering_id'])>0?intval($_REQUEST['gathering_id']):0;
$gather_amount=isset($_REQUEST['gather_amount']) &&floatval($_REQUEST['gather_amount'])>0?floatval($_REQUEST['gather_amount']):0;
$gathering_info=is_gathering_exists($gathering_id);
if(empty($gathering_info))
{
$result['error']=4;
$result['message']=$_LANG['erp_gathering_not_exist'];
die($json->encode($result));
}
$agency_id=get_admin_agency_id($_SESSION['admin_id']);
if($agency_id >0 &&$agency_id!=$gathering_info['agency_id'])
{
$result['error']=4;
$result['message']=$_LANG['erp_gathering_no_agency_access'];
die($json->encode($result));
}
if($gathering_info['gathering_status']!=1)
{
$result['error']=4;
$result['message']=$_LANG['erp_gathering_no_acts'];
die($json->encode($result));
}
if(lock_gathering($gathering_id,'edit'))	
{
if(gathering_act_record($gathering_id,'modify'))
{
$gathering_info=get_gathering_info($gathering_id);
$order_id=$gathering_info[0]['bill_id'];
update_gathering($gathering_id,$order_id,$gather_amount);
$result['error']=0;
die($json->encode($result));
}
}
else{
$result['error']=-1;
$result['message']=$_LANG['erp_gathering_no_accessibility'];
die($json->encode($result));
}
}
elseif ($_REQUEST['act'] == 'delete_gathering')
{
include('../includes/cls_json.php');
$json  = new JSON;
if(!admin_priv('erp_finance_manage','',false))
{
$result['error']=1;
$result['message']=$_LANG['erp_no_permit'];
die($json->encode($result));
}
$gathering_id=isset($_REQUEST['gathering_id']) &&intval($_REQUEST['gathering_id'])>0?intval($_REQUEST['gathering_id']):0;
$gathering_info=is_gathering_exists($gathering_id);
if(empty($gathering_info))
{
$result['error']=4;
$result['message']=$_LANG['erp_gathering_not_exist'];
die($json->encode($result));
}
$agency_id=get_admin_agency_id($_SESSION['admin_id']);
if($agency_id >0 &&$agency_id!=$gathering_info['agency_id'])
{
$result['error']=4;
$result['message']=$_LANG['erp_gathering_no_agency_access'];
die($json->encode($result));
}
if($gathering_info['gathering_status']!=1)
{
$result['error']=4;
$result['message']=$_LANG['erp_gathering_no_acts'];
die($json->encode($result));
}
if(lock_gathering($gathering_id,'delete'))	
{
if(delete_gathering($gathering_id))
{
$result['error']=0;
$result['message']=$_LANG['erp_gathering_delete_sucess'];
die($json->encode($result));
}
}
else{
$result['error']=-1;
$result['message']=$_LANG['erp_gathering_no_accessibility'];
die($json->encode($result));
}
}
elseif ($_REQUEST['act'] == 'post_to_approve')
{
include('../includes/cls_json.php');
$json  = new JSON;
if(!admin_priv('erp_finance_manage','',false))
{
$result['error']=1;
$result['message']=$_LANG['erp_no_permit'];
die($json->encode($result));
}
$gathering_id=isset($_REQUEST['gathering_id']) &&intval($_REQUEST['gathering_id'])>0?intval($_REQUEST['gathering_id']):0;
$gathering_info=is_gathering_exists($gathering_id);
if(empty($gathering_info))
{
$result['error']=4;
$result['message']=$_LANG['erp_gathering_not_exist'];
die($json->encode($result));
}
$agency_id=get_admin_agency_id($_SESSION['admin_id']);
if($agency_id >0 &&$agency_id!=$gathering_info['agency_id'])
{
$result['error']=4;
$result['message']=$_LANG['erp_gathering_no_agency_access'];
die($json->encode($result));
}
if($gathering_info['gathering_status']!=1)
{
$result['error']=4;
$result['message']=$_LANG['erp_gathering_no_acts'];
die($json->encode($result));
}
if(lock_gathering($gathering_id,'post_to_approve'))	
{
if(gathering_act_record($gathering_id,'post'))
{
$sql="update ".$GLOBALS['ecs']->table('erp_gathering')." set ";
$sql.="gathering_status ='2' ";
$sql.="where gathering_id='".$gathering_id."'";
if($GLOBALS['db']->query($sql))
{
$result['error']=0;
$result['message']=$_LANG['erp_gathering_post_success'];
die($json->encode($result));
}
}
}
else{
$result['error']=-1;
$result['message']=$_LANG['erp_gathering_no_accessibility'];
die($json->encode($result));
}
}
elseif ($_REQUEST['act'] == 'approve_pass'||$_REQUEST['act'] == 'approve_reject')
{
include('../includes/cls_json.php');
$json  = new JSON;
if(!admin_priv('erp_finance_approve','',false))
{
$result['error']=1;
$result['message']=$_LANG['erp_no_permit'];
die($json->encode($result));
}
$gathering_id=isset($_REQUEST['gathering_id']) &&intval($_REQUEST['gathering_id'])>0?intval($_REQUEST['gathering_id']):0;
$approve_remark=isset($_REQUEST['approve_remark'])?trim($_REQUEST['approve_remark']):'';
$act=isset($_REQUEST['act'])?trim($_REQUEST['act']):'';
$gathering_info=is_gathering_exists($gathering_id);
if(empty($gathering_info))
{
$result['error']=4;
$result['message']=$_LANG['erp_gathering_not_exist'];
die($json->encode($result));
}
$agency_id=get_admin_agency_id($_SESSION['admin_id']);
if($agency_id >0 &&$agency_id!=$gathering_info['agency_id'])
{
$result['error']=4;
$result['message']=$_LANG['erp_gathering_no_agency_access'];
die($json->encode($result));
}
if($gathering_info['gathering_status']!=2)
{
$result['error']=4;
$result['message']=$_LANG['erp_gathering_no_acts'];
die($json->encode($result));
}
if(lock_gathering($gathering_id,'approve'))	
{
if(gathering_act_record($gathering_id,'approve'))
{
if($act=='approve_pass')
{
if(approve_gathering_pass($gathering_id,$approve_remark))
{
$result['error']=0;
$result['message']=$_LANG['erp_gathering_approve_success'];
die($json->encode($result));
}
}
else if($act=='approve_reject')
{
if(approve_gathering_reject($gathering_id,$approve_remark))	
{
$result['error']=0;
$result['message']=$_LANG['erp_gathering_approve_success'];
die($json->encode($result));
}
}
}
}
else{
$result['error']=-1;
$result['message']=$_LANG['erp_gathering_no_accessibility'];
die($json->encode($result));
}
}
?>
