<?php

define('IN_ECS',true);
require(dirname(__FILE__) .'/includes/init.php');
require_once(ROOT_PATH . 'includes/lib_order.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_base.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_common.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_goods_attr.php');
require(dirname(__FILE__) .'/includes/ERP/lib_erp_warehouse.php');
require(dirname(__FILE__) .'/includes/ERP/cls/cls_date.php');
$stocktakeController = new Yoho\cms\Controller\StocktakeController();
$adminuserController = new Yoho\cms\Controller\AdminuserController();
$deliveryOrderController = new Yoho\cms\Controller\DeliveryOrderController();
$orderProgressController = new Yoho\cms\Controller\OrderProgressController();

if($_REQUEST['act'] == 'list' || $_REQUEST['act'] == ''){
	if( admin_priv('erp_warehouse_view','',false) || admin_priv('erp_warehouse_manage','',false) || admin_priv('erp_warehouse_approve','',false))
	{
        include('./includes/ERP/page.class.php');

        $filename = ROOT_PATH . 'languages/zh_tw/admin/order.php';
        if (file_exists($filename))
        {
            include_once($filename);
        }

        // today collected
       // $today_collected = $orderProgressController->getTodayCollected();

        //dd($today_collected);
        // exit;

        // define('SS_UNSHIPPED',              0); // 未发货
        // define('SS_SHIPPED',                1); // 已发货
        // define('SS_RECEIVED',               2); // 已收货
        // define('SS_PREPARING',              3); // 备货中
        // define('SS_SHIPPED_PART',           4); // 已发货(部分商品)
        // define('SS_SHIPPED_ING',            5); // 发货中(处理分单)
        // define('OS_SHIPPED_PART',           6); // 已发货(部分商品)

        $ship_status_options = array (SS_UNSHIPPED => '未出貨',SS_SHIPPED => '已出貨',SS_SHIPPED_PART=> '部分出貨'); 
        $smarty->assign('ship_status_options', $ship_status_options);
        $smarty->assign('action_link', array('href' => 'order_progress_report.php?act=order_progress_history', 'text' => '查詢記錄'));
        $smarty->assign('today_collected', $today_collected);
		$smarty->assign('logistics_method', $deliveryOrderController->getLogisticsMethod());
		$smarty->assign('type_options', $stocktakeController->getTypeOptions());
        $smarty->assign('status_options', $stocktakeController->getStocktakeStatusOptions());
        $smarty->assign('ur_here', '出貨進度');
        $smarty->assign('full_page', 1);
        $smarty->assign('start_date', $result['start_date']);
        $smarty->assign('end_date', $result['end_date']);
        $smarty->assign('cfg_lang', $_CFG['lang']);
        $smarty->assign('lang', $_LANG);
        $smarty->display('order_progress_report.htm');
    }else{
        $href="index.php";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
    }

} elseif ($_REQUEST['act'] == 'order_progress_history' || $_REQUEST['act'] == '') {
	if( admin_priv('erp_warehouse_view','',false) || admin_priv('erp_warehouse_manage','',false) || admin_priv('erp_warehouse_approve','',false))
	{
        include('./includes/ERP/page.class.php');

        $filename = ROOT_PATH . 'languages/zh_tw/admin/order.php';
        if (file_exists($filename))
        {
            include_once($filename);
        }

        // today collected
        // $dashboard = $orderProgressController->getProgressDashboard();

        //dd($today_collected);
        // exit;

        $smarty->assign('action_link', array('href' => 'order_progress_report.php?act=list', 'text' => '返回訂單進度'));
        $smarty->assign('dashboard', $dashboard);
		$smarty->assign('logistics_method', $deliveryOrderController->getLogisticsMethod());
		$smarty->assign('type_options', $stocktakeController->getTypeOptions());
        $smarty->assign('status_options', $stocktakeController->getStocktakeStatusOptions());
        $smarty->assign('ur_here', '核對訂單');
        $smarty->assign('full_page', 1);
        $smarty->assign('start_date', $result['start_date']);
        $smarty->assign('end_date', $result['end_date']);
        $smarty->assign('cfg_lang', $_CFG['lang']);
        $smarty->assign('lang', $_LANG);
        $smarty->display('order_progress_report_history.htm');
    }else{
        $href="index.php";
		$text=$_LANG['erp_retun_to_center'];
		$link[] = array('href'=>$href,'text'=>$text);
		sys_msg($_LANG['erp_no_permit'],0,$link);
    }
} elseif ($_REQUEST['act'] == 'query') {
    $result = $orderProgressController->getOrderProgressList(true,'remain');
    $orderProgressController->ajaxQueryAction($result['data'],$result['record_count'],false);
} elseif ($_REQUEST['act'] == 'query_delay') {
    $result = $orderProgressController->getOrderProgressList(true,'delay');
    $orderProgressController->ajaxQueryAction($result['data'],$result['record_count'],false);
} elseif ($_REQUEST['act'] == 'query_history') {
    $result = $orderProgressController->getOrderProgressList(true,'history');
    $orderProgressController->ajaxQueryAction($result['data'],$result['record_count'],false);
} elseif ($_REQUEST['act'] == 'post_process_remark_submit') {
    $result = $orderProgressController->progressRemarkSubmit();
    if($result !== false){
        make_json_result($result);
    }else{
        make_json_error('更新時發生錯誤');
    }
} elseif ($_REQUEST['act'] == 'get_progress_dashboard') {
    $result = $orderProgressController->getProgressDashboard();
    if($result !== false){
        make_json_result($result);
    }else{
        make_json_error('更新時發生錯誤');
    }
} elseif ($_REQUEST['act'] == 'update_order_estimated_time') {
    $result = $orderProgressController->updateOrderEstimatedTime();
    if($result !== false){
        make_json_result($result);
    }else{
        make_json_error('更新時發生錯誤');
    }
} elseif ($_REQUEST['act'] == 'check_order_all_in_stock') {
    $result = $orderProgressController->checkOrderAllInStock();
    
    exit;
}


?>