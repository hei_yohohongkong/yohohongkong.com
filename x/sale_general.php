<?php

/**
 * ECSHOP 销售概况
 * ============================================================================
 * 版权所有 2005-2010 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liuhui $
 * $Id: sale_general.php 17063 2010-03-25 06:35:46Z liuhui $
*/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require_once(ROOT_PATH . 'languages/' . $_CFG['lang'] . '/admin/statistic.php');
$smarty->assign('lang', $_LANG);

/* 权限判断 */
admin_priv('sale_order_stats');

/* act操作项的初始化 */
if (empty($_REQUEST['act']) || !in_array($_REQUEST['act'], array('list', 'download')))
{
    $_REQUEST['act'] = 'list';
}

/* 取得查询类型和查询时间段 */
if (empty($_POST['query_by_year']) && empty($_POST['query_by_month']))
{
    if (empty($_GET['query_type']))
    {
        /* 默认当年的月走势 */
        $query_type = 'month';
        $start_time = mktime(0, 0, 0, 1, 1, intval(date('Y')));
        $end_time   = gmtime();
    }
    else
    {
        /* 下载时的参数 */
        $query_type = $_GET['query_type'];
        $start_time = $_GET['start_time'];
        $end_time   = $_GET['end_time'];
    }
}
else
{
    if (isset($_POST['query_by_year']))
    {
        /* 年走势 */
        $query_type = 'year';
        $start_time = mktime(0, 0, 0, 1, 1, intval($_POST['year_beginYear']));
        $end_time   = mktime(23, 59, 59, 12, 31, intval($_POST['year_endYear']));
    }
    else
    {
        /* 月走势 */
        $query_type = 'month';
        $start_time = strtotime("00:00:00 $_POST[month_begin]-1");
        $end_time = strtotime("23:59:59 last day of $_POST[month_end]");
    }
}

/* 分组统计订单数和销售额：已发货时间为准 */
$cond = " WHERE order_status IN('" . OS_CONFIRMED . "', '" . OS_SPLITED . "')" .
        " AND pay_status IN('" . PS_PAYED . "','" . PS_PAYING . "') " .
        " AND shipping_status IN('" . SS_SHIPPED . "', '" . SS_RECEIVED . "') " .
        " AND shipping_time >= '$start_time' AND shipping_time <= '$end_time'";
$cond2 = " WHERE order_status IN('" . OS_CONFIRMED . "', '" . OS_SPLITED . "')" .
        " AND pay_status IN('" . PS_PAYED . "','" . PS_PAYING . "') " .
        " AND shipping_status NOT IN('" . SS_SHIPPED . "', '" . SS_RECEIVED . "') " .
        " AND add_time >= '$start_time' AND add_time <= '$end_time'";
$group_by = " GROUP BY period, type_id ";
$format = ($query_type == 'year') ? '%Y' : '%Y-%m';
$sql_base = "SELECT sa.name, stat.* FROM (SELECT type_id, DATE_FORMAT(FROM_UNIXTIME(shipping_time), '$format') AS period, COUNT(*) AS order_count, " .
            "SUM(goods_amount + shipping_fee + insure_fee + pay_fee + pack_fee + card_fee - discount) AS order_amount FROM " . $ecs->table('order_info') . $cond;
$sql_base2 = "SELECT sa.name, stat.* FROM (SELECT type_id, DATE_FORMAT(FROM_UNIXTIME(add_time), '$format') AS period, COUNT(*) AS order_count, " .
            "SUM(goods_amount + shipping_fee + insure_fee + pay_fee + pack_fee + card_fee - discount) AS order_amount FROM " . $ecs->table('order_info') . $cond2;
$sql_end = $group_by . ") stat LEFT JOIN " . $ecs->table('sales_agent') . " sa ON sa.sa_id = stat.type_id ORDER BY stat.period ASC, stat.type_id ASC";
$sql = $sql_base . $sql_end;
$data_list = $db->getAll($sql);
$sql = $sql_base . " AND order_type = 'sa'" . $sql_end;
$b2b2c_list = $db->getAll($sql);
$sql = $sql_base2 . " AND order_type = 'sa'" . $sql_end;
$b2b2c_await_list = $db->getAll($sql);
$sql = $sql_base . " AND (order_type != 'sa' OR order_type IS NULL)" . $sql_end;
$non_b2b2c_list = $db->getAll($sql);

/*------------------------------------------------------ */
//-- 显示统计信息
/*------------------------------------------------------ */
if ($_REQUEST['act'] == 'list')
{
    /* 赋值查询时间段 */
    $smarty->assign('start_time',   date('Y-m-d', $start_time));
    $smarty->assign('end_time',     date('Y-m-d', $end_time));
    $caption = [$_LANG['comment_order_all'],$_LANG['comment_order_non_b2b2c'],$_LANG['comment_order_b2b2c'],$_LANG['comment_order_b2b2c_await']];
    /* 赋值统计数据 */
    $xml = "' xAxisName='%s' showValues='1' decimals='0' formatNumberScale='0'>%s</graph>";
    $xml2 = "' xAxisName='%s' showValues='1' decimals='0' formatNumberScale='0'>%s</graph>";
    $set = "<set label='%s' value='%s' color='%s'/>";
    $i = 0;
    $data_res = array();
    $b2b2c_res = array();
    $b2b2c_res2 = array();
    $b2b2c_await_res = array();
    $b2b2c_await_res2 = array();
    $non_b2b2c_res = array();
    $data_array = array();
    $b2b2c_array = array();
    $b2b2c_array2 = array();
    $b2b2c_await_array = array();
    $b2b2c_await_array2 = array();
    $b2b2c_array_agent = array();
    $non_b2b2c_array = array();
    foreach ($data_list as $data)
    {
        $data_array['count'][$data['period']] += $data['order_count'];
        $data_array['amount'][$data['period']] += $data['order_amount'];
    }
    foreach ($b2b2c_list as $data)
    {
        if(!in_array($data['name'],$b2b2c_array2['sa_name']))
            $b2b2c_array2['sa_name'][] = $data['name'];
        $b2b2c_array['count'][$data['period']] += $data['order_count'];
        $b2b2c_array['amount'][$data['period']] += $data['order_amount'];
        $b2b2c_array2['count'][$data['period']][$data['name']] += $data['order_count'];
        $b2b2c_array2['amount'][$data['period']][$data['name']] += $data['order_amount'];
        $i++;
    }
    foreach ($b2b2c_await_list as $data)
    {
        if(!in_array($data['name'],$b2b2c_await_array2['sa_name']))
            $b2b2c_await_array2['sa_name'][] = $data['name'];
        $b2b2c_await_array['count'][$data['period']] += $data['order_count'];
        $b2b2c_await_array['amount'][$data['period']] += $data['order_amount'];
        $b2b2c_await_array2['count'][$data['period']][$data['name']] += $data['order_count'];
        $b2b2c_await_array2['amount'][$data['period']][$data['name']] += $data['order_amount'];
    }
    foreach ($non_b2b2c_list as $data)
    {
        $non_b2b2c_array['count'][$data['period']] += $data['order_count'];
        $non_b2b2c_array['amount'][$data['period']] += $data['order_amount'];
    }
    foreach ($data_array as $key => $value)
    {
        $i = 0;
        $id = 'all_order_' . $key;
        foreach($value as $k => $v){
            $all_order[$id]['data']['datasets'][0]['data'][] = $v;
            $all_order[$id]['data']['datasets'][0]['backgroundColor'][] = '#' . chart_color($i);
            $all_order[$id]['data']['labels'][] = $k;
            $i++;
        }
        $all_order[$id]['label'] = $_LANG['order_' . $key . '_trend'];
        $all_order[$id]['title'] = $caption[0];
    }
    foreach ($b2b2c_array as $key => $value)
    {
        $i = 0;
        $id = 'b2b2c_' . $key;
        foreach($value as $k => $v){
            $all_order[$id]['data']['datasets'][0]['data'][] = $v;
            $all_order[$id]['data']['datasets'][0]['backgroundColor'][] = '#' . chart_color($i);
            $all_order[$id]['data']['labels'][] = $k;
            foreach ($b2b2c_array2['sa_name'] as $name){
                $b2b2c_array_agent[$key][$name][$k] = isset($b2b2c_array2[$key][$k][$name]) ? $b2b2c_array2[$key][$k][$name] : 0;
            }
            $i++;
        }
        $all_order[$id]['label'] = $_LANG['order_' . $key . '_trend'];
        $all_order[$id]['title'] = $caption[2];
    }
    foreach ($b2b2c_array_agent as $key => $value)
    {
        $cat = [];
        $i = 0;
        $id = 'b2b2c_' . $key . '_agent';
        foreach($value as $name => $data){
            $all_order[$id]['data']['datasets'][$i]['label'][] = $name;
            foreach ($data as $k => $v){
                $all_order[$id]['data']['datasets'][$i]['data'][] = $v;
                $all_order[$id]['data']['datasets'][$i]['backgroundColor'][] = '#' . chart_color($i);
                if(!in_array($k,$cat)){
                    $cat[] = $k;
                    $all_order[$id]['data']['labels'][] = $k;
                }
            }
            $i++;
        }
        $all_order[$id]['label'] = $_LANG['order_' . $key . '_trend'];
        $all_order[$id]['title'] = $caption[2];
        $all_order[$id]['legend'] = 1;
    }
    foreach ($b2b2c_await_array as $key => $value)
    {
        $i = 0;
        $id = 'b2b2c_' . $key . '_await';
        foreach($value as $k => $v){
            $all_order[$id]['data']['datasets'][0]['data'][] = $v;
            $all_order[$id]['data']['datasets'][0]['backgroundColor'][] = '#' . chart_color($i);
            $all_order[$id]['data']['labels'][] = $k;
            foreach ($b2b2c_await_array2['sa_name'] as $name){
                $b2b2c_await_array_agent[$key][$name][$k] = isset($b2b2c_await_array2[$key][$k][$name]) ? $b2b2c_await_array2[$key][$k][$name] : 0;
            }
            $i++;
        }
        $all_order[$id]['label'] = $_LANG['order_' . $key . '_trend'];
        $all_order[$id]['title'] = $caption[3];
    }
    foreach ($b2b2c_await_array_agent as $key => $value)
    {
        $cat = [];
        $i = 0;
        $id = 'b2b2c_' . $key . '_agent_await';
        foreach($value as $name => $data){
            $all_order[$id]['data']['datasets'][$i]['label'][] = $name;
            foreach ($data as $k => $v){
                $all_order[$id]['data']['datasets'][$i]['data'][] = $v;
                $all_order[$id]['data']['datasets'][$i]['backgroundColor'][] = '#' . chart_color($i);
                if(!in_array($k,$cat)){
                    $cat[] = $k;
                    $all_order[$id]['data']['labels'][] = $k;
                }
            }
            $i++;
        }
        $all_order[$id]['label'] = $_LANG['order_' . $key . '_trend'];
        $all_order[$id]['title'] = $caption[3];
        $all_order[$id]['legend'] = 1;
    }
    foreach ($non_b2b2c_array as $key => $value)
    {
        $i = 0;
        $id = 'non_b2b2c_' . $key;
        foreach($value as $k => $v){
            $all_order[$id]['data']['datasets'][0]['data'][] = $v;
            $all_order[$id]['data']['datasets'][0]['backgroundColor'][] = '#' . chart_color($i);
            $all_order[$id]['data']['labels'][] = $k;
            $i++;
        }
        $all_order[$id]['label'] = $_LANG['order_' . $key . '_trend'];
        $all_order[$id]['title'] = $caption[1];
    }
    $smarty->assign('barCharts', $all_order); // 订单数统计数据

    /* 根据查询类型生成文件名 */
    if ($query_type == 'year')
    {
        $filename = date('Y', $start_time) . "_" . date('Y', $end_time) . '_report';
    }
    else
    {
       $filename = date('Ym', $start_time) . "_" . date('Ym', $end_time) . '_report';
    }
    $smarty->assign('action_link',
    array('text' => $_LANG['down_sales_stats'],
          'href'=>'sale_general.php?act=download&filename=' . $filename .
            '&query_type=' . $query_type . '&start_time=' . $start_time . '&end_time=' . $end_time));

    /* 显示模板 */
    $smarty->assign('ur_here', $_LANG['report_sell']);
    assign_query_info();
    $smarty->display('sale_general.htm');
}

/*------------------------------------------------------ */
//-- 下载EXCEL报表
/*------------------------------------------------------ */
elseif ($_REQUEST['act'] == 'download')
{
    /* 文件名 */
    $filename = !empty($_REQUEST['filename']) ? trim($_REQUEST['filename']) : '';

    header("Content-type: application/vnd.ms-excel; charset=utf-8");
    header("Content-Disposition: attachment; filename=$filename.xls");

    /* 文件标题 */
    echo ecs_iconv(EC_CHARSET, 'BIG5', $filename . $_LANG['sales_statistics']) . "\t\n";

    /* 订单数量, 销售出商品数量, 销售金额 (所有訂單)*/
    echo ecs_iconv(EC_CHARSET, 'BIG5', '所有訂單') . "\t\n";
    echo ecs_iconv(EC_CHARSET, 'BIG5', $_LANG['period']) ."\t";
    echo ecs_iconv(EC_CHARSET, 'BIG5', $_LANG['order_count_trend']) ."\t";
    echo ecs_iconv(EC_CHARSET, 'BIG5', $_LANG['order_amount_trend']) . "\t\n";

    foreach ($data_list AS $data)
    {
        echo ecs_iconv(EC_CHARSET, 'BIG5', $data['period']) . "\t";
        echo ecs_iconv(EC_CHARSET, 'BIG5', $data['order_count']) . "\t";
        echo ecs_iconv(EC_CHARSET, 'BIG5', $data['order_amount']) . "\t";
        echo "\n";
    }
    echo "\n";

    /* 订单数量, 销售出商品数量, 销售金额 (非B2B2C 訂單)*/
    echo ecs_iconv(EC_CHARSET, 'BIG5', '非B2B2C 訂單') . "\t\n";
    echo ecs_iconv(EC_CHARSET, 'BIG5', $_LANG['period']) ."\t";
    echo ecs_iconv(EC_CHARSET, 'BIG5', $_LANG['order_count_trend']) ."\t";
    echo ecs_iconv(EC_CHARSET, 'BIG5', $_LANG['order_amount_trend']) . "\t\n";

    foreach ($non_b2b2c_list AS $data)
    {
        echo ecs_iconv(EC_CHARSET, 'BIG5', $data['period']) . "\t";
        echo ecs_iconv(EC_CHARSET, 'BIG5', $data['order_count']) . "\t";
        echo ecs_iconv(EC_CHARSET, 'BIG5', $data['order_amount']) . "\t";
        echo "\n";
    }
    echo "\n";

    /* 订单数量, 销售出商品数量, 销售金额 (B2B2C 訂單)*/
    echo ecs_iconv(EC_CHARSET, 'BIG5', 'B2B2C 訂單') . "\t\n";
    echo ecs_iconv(EC_CHARSET, 'BIG5', $_LANG['period']) ."\t";
    echo ecs_iconv(EC_CHARSET, 'BIG5', $_LANG['order_count_trend']) ."\t";
    echo ecs_iconv(EC_CHARSET, 'BIG5', $_LANG['order_amount_trend']) . "\t\n";

    foreach ($b2b2c_list AS $data)
    {
        echo ecs_iconv(EC_CHARSET, 'BIG5', $data['period']) . "\t";
        echo ecs_iconv(EC_CHARSET, 'BIG5', $data['order_count']) . "\t";
        echo ecs_iconv(EC_CHARSET, 'BIG5', $data['order_amount']) . "\t";
        echo "\n";
    }
    echo "\n";
}

?>