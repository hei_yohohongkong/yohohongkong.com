<?php

/* * *
 * sa_batch_order.php
 * by Anthony 2018-02-08
 *
 * Batch create sa order function
 * * */
define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/admin/salesagent.php');

$salesagentController = new Yoho\cms\Controller\SalesagentController();
$orderController = new Yoho\cms\Controller\OrderController();
$admin_priv = $salesagentController::DB_ACTION_CODE_SALES_AGENT;
use Yoho\cms\Model;

$_REQUEST['act'] = !empty($_REQUEST['act']) ? $_REQUEST['act'] : 'info';
switch ($_REQUEST['act']) {

    case 'info':
        /* 检查权限 */
		admin_priv($admin_priv);
		// title
        $smarty->assign('ur_here', $_LANG['sa_batch_order']);
        $smarty->assign('full_page', 1);
        $smarty->assign('salespeople', $db->getAll('SELECT sales_id, sales_name FROM ' . $GLOBALS['ecs']->table('salesperson') . ' AS sp WHERE available = 1 AND allow_pos = 1'));

        $smarty->assign('sales_agents', $salesagentController->get_active_sales_agents());
		assign_query_info();
        $smarty->display('sa_batch.htm');
        break;
    case 'am_upload':
        header('Content-Type: application/json');
        echo $salesagentController->import_asiamiles_excel();
        exit;
        break;
    case 'batch_create_order':
        header('Content-Type: application/json');
        echo $orderController->batch_create_sa_order();
        exit;
        break;
    case 'download_record':
        header('Content-Type: application/json');
        echo $orderController->export_batch_order_excel();
        exit;
        break;
}