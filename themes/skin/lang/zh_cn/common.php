<?php
/**
* @範本   Vanity
* @版本   1.5.0
* @作者   david http://www.shopiy.com/
* @版權   Copyright (C) 2009 - 2011 Shopiy
* @許可   Shopiy授權合約 (http://www.shopiy.com/license)
*/

$_LANG['all_category'] = '全部商品分類';
$_LANG['all_category_select'] = '--------所有分類--------';
$_LANG['all_goods'] = '全部';
$_LANG['keyword_tip'] = '請輸入您想要的商品';
$_LANG['cart'] = '購物車';
$_LANG['goods_number_tip'] = '修改數量後別忘了更新購物車';
$_LANG['continue_shopping'] = '&laquo; 繼續購買商品';
$_LANG['checkout_now'] = '立即結算 &raquo;';
$_LANG['to_cart'] = '去購物車結算 &raquo;';
$_LANG['cart_empty'] = '您的購物車中沒有商品';
$_LANG['show_package'] = '查看商品';
$_LANG['shopping_money'] = '購物金額小計 %s';
$_LANG['submit_order'] = '提交訂單 &raquo;';
$_LANG['back_cart'] = '&laquo; 返回購物車';
$_LANG['enter_bonus_sn'] = '輸入紅包序號';

$_LANG['colon'] = '：';
$_LANG['clear'] = '清除';
$_LANG['close'] = '關閉';
$_LANG['cart_info'] = '您的購物車有<em>%d</em>件商品';
$_LANG['more'] = '更多';
$_LANG['next'] = '更多';
$_LANG['next'] = '下一個';
$_LANG['previous'] = '上一個';
$_LANG['manage'] = '管理';
$_LANG['email'] = 'E-mail';
$_LANG['preview'] = '預覽';
$_LANG['invoice_list'] = '近期物流單';
$_LANG['subscription'] = '郵件訂閱';
$_LANG['order_status'] = '狀態';
$_LANG['vote_tip_s'] = '請勾選一個選項';
$_LANG['vote_tip_m'] = '請勾選一個或多個選項';
$_LANG['brands_list'] = '品牌列表';
$_LANG['email_invalid'] = 'E-mail地址無效';
$_LANG['invalid_order_sn'] = '訂單號無效';
$_LANG['input_order_sn'] = '請輸入訂單號';
$_LANG['vote'] = '站內調查';
$_LANG['hot_search'] = '熱門搜索：';
$_LANG['same_brand_goods'] = '相同品牌的商品';
$_LANG['same_cat_goods'] = '相同分類的商品';
$_LANG['same_cat_articles'] = '相同分類的商品';

$_LANG['login'] = '登錄';
$_LANG['btn_login'] = '登錄';
$_LANG['btn_register'] = '註冊';
$_LANG['btn_detail'] = '詳情';
$_LANG['btn_search'] = '搜索';
$_LANG['btn_buy'] = '購買';
$_LANG['btn_collect'] = '收藏';
$_LANG['btn_compare'] = '比較';
$_LANG['btn_affiliate'] = '推薦';
$_LANG['btn_booking'] = '預 定';
$_LANG['btn_edit'] = '修 改';
$_LANG['btn_bidding'] = '出 價';
$_LANG['ur_here'] = '當前位置：';
$_LANG['remove_all'] = '<span class="button"><span>移除所有</span></span>';
$_LANG['btn_logout'] = '退出';
$_LANG['goods_info'] = '商品資訊';
$_LANG['goods_description'] = '商品描述';
$_LANG['quantity'] = '數量';
$_LANG['original_price'] = '原價';
$_LANG['goods_properties'] = '商品屬性';
$_LANG['attr_name'] = '屬性名稱';
$_LANG['attr_value'] = '屬性值';

$_LANG['recommend'] = '推薦此商品';

$_LANG['goods_free_shipping'] = '此商品為<em>免運費</em>商品，計算配送金額時將不計入配送費用。';
$_LANG['goods_sn'] = '商品編號';
$_LANG['goods_click_count'] = '商品點擊數：';
$_LANG['promote_price'] = '促銷價';
$_LANG['promotion_goods'] = '限時促銷';
$_LANG['multi_choice'] = '可多選';
$_LANG['end_time'] = '結束時間';
$_LANG['sale_end_time'] = '促銷剩餘時間';
$_LANG['multi_choice'] = '可多選';
$_LANG['loading_amount'] = '計算中...';
$_LANG['goods_tags'] = '商品標籤';
$_LANG['volume_price_label'] = '購買數量對應的價格';
$_LANG['number_to'] = '達到';
$_LANG['discount_price'] = '團購價';
$_LANG['rank_price_label'] = '會員價';
$_LANG['user_rank'] = '使用者等級';
$_LANG['comment_form_title'] = '請在這裡發表你的評價';
$_LANG['message_form_title'] = '請在這裡添加留言';
$_LANG['search_form_title'] = '請在這裡輸入你的搜索條件';
$_LANG['required_indicates'] = ' 為必填項';
$_LANG['re_name'] = '客服答覆：';
$_LANG['label_username'] = '用戶名：';
$_LANG['label_email'] = 'E-mail：';
$_LANG['label_comment_rank'] = '評價等級：';
$_LANG['comment_rank_5'] = '很好';
$_LANG['comment_rank_4'] = '還不錯';
$_LANG['comment_rank_3'] = '一般';
$_LANG['comment_rank_2'] = '不太滿意';
$_LANG['comment_rank_1'] = '很差';
$_LANG['label_keywords'] = '關鍵字：';
$_LANG['label_sc_ds'] = '搜索簡介：';
$_LANG['label_category'] = '選擇分類：';
$_LANG['label_brand'] = '選擇品牌：';
$_LANG['label_price'] = '價格範圍：';
$_LANG['label_extension'] = '擴展選項：';
$_LANG['label_hidden_outstock'] = '隱藏脫銷商品：';
$_LANG['related_goods'] = '相關商品';
$_LANG['shopping_and_other'] = '顧客購買的還有';
$_LANG['fittings'] = '相關配件';

$_LANG['display'] = '顯示';
$_LANG['order_by'] = '排序';
$_LANG['display_list'] = '大圖顯示';
$_LANG['display_grid'] = '小圖顯示';
$_LANG['display_text'] = '清單顯示';
$_LANG['order_by_time'] = '上架時間';
$_LANG['order_by_update'] = '更新時間';
$_LANG['order_by_price'] = '價格';
$_LANG['order_by_point'] = '積分';
$_LANG['order_by_price_desc'] = '價格從高到低';
$_LANG['order_by_time_desc'] = '上架時間從晚到早';
$_LANG['order_by_time_asc'] = '上架時間從早到晚';
$_LANG['order_by_price_desc'] = '價格從高到低';
$_LANG['order_by_price_asc'] = '價格從低到高';
$_LANG['order_by_point_desc'] = '積分從多到少';
$_LANG['order_by_point_asc'] = '積分從少到多';

$_LANG['page_prev'] = '&laquo; 上一頁';
$_LANG['page_next'] = '下一頁 &raquo;';

$_LANG['goods_empty'] = '沒有對應的商品';


$_LANG['your_choose'] = '你的選擇';
$_LANG['tags_cloud_empty'] = '當前還沒有人添加標籤';
$_LANG['myship'] = '配送方式查詢';
$_LANG['my_integral'] = '我的積分：';
$_LANG['snatch_desc'] = '活動介紹';
$_LANG['snatch_price'] = '出價列表';
$_LANG['curr_status'] = '當前狀態';
$_LANG['not_only_price'] = '已有相同價格';
$_LANG['snatch_needful'] = '提示：您需要先註冊成為本站會員並且登錄後，才能參加此活動!';
$_LANG['snatch_start_time'] = '本次活動從<em>%s</em>到<em>%s</em>截止';
$_LANG['brand_desc'] = '品牌介紹：';
$_LANG['group_buy_intro'] = '團購說明';
$_LANG['group_buy_amount_to'] = '團購數量達到';
$_LANG['price_list_empty'] = '尚無出價記錄';


$_LANG['copyright'] = '&copy; %s %s&nbsp;版權所有&nbsp;&nbsp;';

/* user */
$_LANG['login_title'] = '用戶登錄';
$_LANG['register_title'] = '註冊新會員';
$_LANG['register_now'] = '立即註冊';
$_LANG['login_now'] = '立即登錄';
$_LANG['reset_password'] = '重置密碼';
$_LANG['login_text'] = '如果您已經擁有本站帳號，可以直接登錄。';
$_LANG['new_account'] = '註冊新帳號';
$_LANG['register_text'] = '如果您想登錄，需要擁有一個帳號。現在就免費註冊一個帳號吧。';
$_LANG['get_password_tip'] = '小提示';
$_LANG['get_password_tip_text'] = '填寫正確的用戶名和註冊郵箱位址後會收到包含取回密碼連結的郵件。<br />如果您想馬上登錄，可以使用密碼問題來修改密碼。';
$_LANG['get_password_tip_text2'] = '如果您忘記了密碼問題答案，可以使用註冊郵箱來修改密碼。';
$_LANG['get_password_tip_text3'] = '為安全起見，設定的新密碼不要過於簡單。一個好的密碼一般由數位和字母組合而成並長度不小於8位。';

$_LANG['collection_empty'] = '你還沒有收藏過商品';
$_LANG['generate_code_title'] = '生成推薦代碼';
$_LANG['booking_empty'] = '你還沒有進行過缺貨登記';
$_LANG['booking_form_title'] = '請在這裡填寫詳細的缺貨登記資訊';
$_LANG['code_type_text'] = '文字版';
$_LANG['code_type_image'] = '圖片版';
$_LANG['copy_code'] = '複製代碼';
$_LANG['profile_form_title'] = '請在這裡填寫準確的個人資訊';
$_LANG['edit_password_indicates'] = '如果沒修改密碼請留空';
$_LANG['edit_password_form_title'] = '修改密碼';
$_LANG['order_empty'] = '你還沒有訂單';
$_LANG['track_packages_empty'] = '沒有正在配送中的訂單';
$_LANG['message_list_empty'] = '你還沒有留言過';
$_LANG['account_log_empty'] = '尚無記錄';
$_LANG['shop_prices'] = '售價';
$_LANG['return_to_cart'] = '放回購物車 &raquo;';
$_LANG['carriage_free'] = '免運費';

$_LANG['unlimited'] = '無限制';
$_LANG['content_empty'] = '沒有內容';
$_LANG['relative_file'] = '相關下載';
$_LANG['searchkeywords_notice'] = '匹配多個關鍵字全部，可用 "空格" 或 "AND" 連接。如 "紅色 AND 推薦"<br />匹配多個關鍵字其中部分，可用"+"或 "OR" 連接。如 "紅色 OR 藍色"';

$_LANG['welcome_to'] = '歡迎您來到';
$_LANG['reg_free'] = '免費註冊';
$_LANG['or'] = '或者';
$_LANG['excla'] = '！';
$_LANG['period'] = '。';
$_LANG['sale'] = '優惠中';
$_LANG['article_search'] = '文章搜索';
$_LANG['help_center'] = '幫助中心';
$_LANG['captcha_tip'] = '點擊刷新驗證碼';


$_LANG['shortage'] = '加入購物車失敗：超過購買上限';//'對不起，該商品庫存不足，已暫停銷售。<br />你現在要進行缺貨登記來預訂該商品嗎？';


$_LANG['contact_information'] = '客服中心';
$_LANG['back_to_top'] = '返回頂部';
$_LANG['comments_amounts_pre'] = '已有';
$_LANG['comments_amounts_suf'] = '人評價';


$_LANG['dashboard'] = '控制台';
$_LANG['profile'] = '個人資料';
$_LANG['messages_and_comments'] = '留言和評價';
$_LANG['collections_and_booking'] = '收藏和缺貨登記';
$_LANG['account_and_bonus'] = '資金和紅包管理';
$_LANG['recent_orders'] = '近期訂單';

$_LANG['links'] = '連結';

$_LANG['cat_brands_label'] = '推薦品牌';
$_LANG['cat_promotion_label'] = '促銷活動';
$_LANG['top_exchange'] = '熱門兌換';
$_LANG['free'] = '免費';
$_LANG['free_pre'] = '購物滿';
$_LANG['free_suf'] = '免費';
$_LANG['not_supported_by_current_shipping_method'] = '當前配送方式不支援';

$_LANG['promote_end_date_format'] = 'Y-m-d H:i:s'; // 促銷商品的結束時間的格式

?>
