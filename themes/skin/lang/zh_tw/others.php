<?php
/**
* @範本   Vanity
* @版本   1.5.0
* @作者   david http://www.shopiy.com/
* @版權   Copyright (C) 2009 - 2011 Shopiy
* @許可   Shopiy授權合約 (http://www.shopiy.com/license)
*/

$_LANG['agreement'] = '我已看過並接受<a href="article.php?cat_id=-1" id="agr_link">《使用者協定》</a>';
$_LANG['last_month_order'] = '最近30天的訂單';
$_LANG['affiliate_code'] = '推薦代碼';
$_LANG['search_ship'] = '搜索配送方式';
$_LANG['back_home'] = '<a href="index.php" class="button"><span>返回首頁</span></a>';
$_LANG['goto_user_center'] = '<a href="user.php" class="button"><span>用戶中心</span></a>';
$_LANG['shopping_money'] = '購物金額小計<em class="price">%s</em>';


$_LANG['your_auction'] = '您竟拍到了<strong>%s</strong> ，現在去 <a href="auction.php?act=view&amp;id=%s" class="button"><span">購買</span></a>';
$_LANG['your_snatch'] = '您奪寶奇兵竟拍到了<strong>%s</strong> ，現在去 <a href="snatch.php?act=main&amp;id=%s" class="button"><span>購買</span></a>';
$_LANG['your_discount'] = '根據優惠活動<a href="activity.php">%s</a>，您可以享受折扣 %s';

?>
