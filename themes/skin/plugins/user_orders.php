<?php
/*
插件名稱: 訂單列表
描    述: 獲取用戶的訂單列表
作    者: David@Shopiy.com
版    本: 1.0
作者網站: http://www.shopiy.com/
*/

/*
參數說明:
id        用戶id
number    顯示數量
start     起始數
form      模板模塊 默認是library/siy_order_list.lbi
*/

function siy_user_orders($atts)
{
	$user_id = !empty($atts['id']) ? intval($atts['id']) : ($_SESSION['user_id'] > 0 ? $_SESSION['user_id'] : 0);
	$number = !empty($atts['number']) ? intval($atts['number']) : 10;
	$start = !empty($atts['start']) ? intval($atts['start']) : 0;
	if ($user_id == '0'){
		return false;
	} else {
		$orders = siy_get_user_orders($user_id, $number, $start);
	}
	$GLOBALS['smarty']->assign('orders', $orders);
	$form = (!empty($atts['form'])) ? $atts['form'] : 'library/siy_order_list.lbi';
	$val= $GLOBALS['smarty']->fetch($form);
	return $val;
}

function siy_get_user_orders($user_id, $number = 10, $start = 0)
{
	require_once(ROOT_PATH . '/includes/lib_clips.php');

	// Get PayMe Payment Code
	$paymeController = new Yoho\cms\Controller\PaymeController();
	$payme_id_sql = "SELECT pay_id FROM " . $GLOBALS['ecs']->table('payment') . " WHERE pay_code = 'payme'";
	$payme_id = $GLOBALS['db']->getOne($payme_id_sql);

	$arr = array();
	$sql = "SELECT order_id, order_sn, order_status, shipping_status, pay_status, add_time, reviewed, pay_id, " .
		"(goods_amount + shipping_fee + insure_fee + pay_fee + pack_fee + card_fee + tax - discount) AS total_fee, ".
		"(money_paid + order_amount) as total_amount ". // howang: money_paid(已付金額) + order_amount(應付金額) = 訂單總金額
		" FROM " .$GLOBALS['ecs']->table('order_info') .
		" WHERE user_id = '$user_id' AND order_status != " . OS_CANCELED . " ORDER BY add_time DESC";
	$res = $GLOBALS['db']->SelectLimit($sql, $number, $start);

	while ($row = $GLOBALS['db']->fetchRow($res))
	{
		// Update existing payment status in case of incomplete PayMe payments
		if ($row['pay_id'] == $payme_id && $row['pay_status'] == PS_UNPAYED) {
			// Update status
			$logId = get_paylog_id($row['order_id'], $pay_type = PAY_ORDER);
			if ($paymeController->updateOrderPaymeStatus($row['order_id'], $logId) == true) {
				$row['order_status'] = OS_CONFIRMED;
				$row['pay_status'] = PS_PAYED;
			};
		}

		/* Get expensive goods Image */
		$sql = "SELECT g.goods_thumb, (og.goods_price * og.goods_number ) as sub_total ".
			" FROM ".$GLOBALS['ecs']->table('order_goods') ." as og ".
			" LEFT JOIN ".$GLOBALS['ecs']->table('goods')." as g ON og.goods_id = g.goods_id ".
			" WHERE og.order_id = $row[order_id] ORDER BY sub_total DESC LIMIT 1";
		$goods_thumb = $GLOBALS['db']->getOne($sql);
		$row['expensive_goods_thumb'] = ((strpos($goods_thumb, 'http://') === 0) || (strpos($goods_thumb, 'https://') === 0)) ? $goods_thumb : $GLOBALS['ecs']->url() .$goods_thumb;
		$can_review = ((!in_array($row['order_status'], array(OS_UNCONFIRMED, OS_CANCELED, OS_INVALID, OS_RETURNED, OS_WAITING_REFUND))) && 
						($row['pay_status'] == PS_PAYED) && 
						(in_array($row['shipping_status'], array(SS_SHIPPED, SS_RECEIVED))) && 
						(!$row['reviewed']));
		
		if ($row['order_status'] == OS_UNCONFIRMED)
		{
			$row['handler'] = '<a href="user.php?act=cancel_order&order_id=' . $row['order_id'] . '" onclick="if (!confirm(\'' . _L('user_order_cancel_prompt', '您確認要取消該訂單嗎？') . '\')) return false;" class=" action-button action-button-gray"><span>' . _L('user_order_cancel', '取消') . '</span></a> <a target="_blank" href="user.php?act=order_detail&order_id=' . $row['order_id'] . '&payl=1" class="button brighter_button"><span>' . _L('user_order_pay', '付款') . '</span></a>';
		}
		else if ($row['order_status'] == OS_SPLITED)
		{
			if ($row['shipping_status'] == SS_SHIPPED)
			{
				@$row['handler'] = '<a href="javascript:void(0);" data-order-id="' . $row['order_id'] . '" class="button brighter_button affirm_received_btn"><span>' . _L('user_confirm_delivery', '確認收貨') . '</span></a>';
				if ($can_review)
				{
					@$row['handler'] .= " <a href=\"review.php?order_id=" .$row['order_id']. '" class="button"><span>' . _L('user_order_review', '評價') . '</span></a>';
				}
			}
			//elseif ($row['shipping_status'] == SS_RECEIVED)
			//{
			//	@$row['handler'] = '<span class="status shipping_status_2">'.$GLOBALS['_LANG']['ss_received'] .'</span>';
			//}
			else
			{
				if ($row['pay_status'] == PS_UNPAYED)
				{
					@$row['handler'] = '<a href="user.php?act=order_detail&order_id=' . $row['order_id'] . '" class="button brighter_button"><span>' . _L('user_order_pay', '付款') . '</span></a>';
				}
				elseif ($can_review)
				{
					@$row['handler'] = '<a href="review.php?order_id=' . $row['order_id'] . '" class="button"><span>' . _L('user_order_review', '評價') . '</span></a>';
				}
				else
				{
					@$row['handler'] = '<a href="user.php?act=order_detail&order_id=' . $row['order_id'] . '" class="button"><span>' . _L('user_order_view', '查看訂單') . '</span></a>';
				}
			}
		}
		else
		{
			//$row['handler'] = '<span class="status order_status_'.$row['order_status'].'">'.$GLOBALS['_LANG']['os'][$row['order_status']] .'</span>';
			$row['handler'] = '<a href="user.php?act=order_detail&order_id=' . $row['order_id'] . '" class="button"><span>' . _L('user_order_view', '查看訂單') . '</span></a>';
			if ($can_review)
			{
				@$row['handler'] .= " <a href=\"review.php?order_id=" .$row['order_id']. '" class="button"><span>' . _L('user_order_review', '評價') . '</span></a>';
			}
			if($row['order_status'] == OS_CONFIRMED && $row['pay_status'] != PS_PAYED && $row['shipping_status'] == SS_UNSHIPPED)     
			{
				$row['handler'] = '<a target="_blank" href="/user.php?act=order_detail&order_id=' . $row['order_id'] . '&payl=1" class="button brighter_button"><span>' . _L('user_order_pay', '付款') . '</span></a>';
			}
			elseif ($row['shipping_status'] == SS_SHIPPED)
			{
				@$row['handler'] = '<a href="javascript:void(0);" data-order-id="' . $row['order_id'] . '" class="button brighter_button affirm_received_btn"><span>' . _L('user_confirm_delivery', '確認收貨') . '</span></a>';
				if ($can_review)
				{
					@$row['handler'] .= " <a href=\"review.php?order_id=" .$row['order_id']. '" class="button"><span>' . _L('user_order_review', '評價') . '</span></a>';
				}
			}
		}
		
		$row['shipping_status'] = ($row['shipping_status'] == SS_SHIPPED_ING) ? SS_PREPARING : $row['shipping_status'];
		// $row['order_status'] = '<em class="order_status_'.$row['order_status'].'">'.$GLOBALS['_LANG']['os'][$row['order_status']].'</em>
		// 						<em class="pay_status_'.$row['pay_status'].'">'.$GLOBALS['_LANG']['ps'][$row['pay_status']].'</em>
		// 						<em class="shipping_status_'.$row['shipping_status'].'">'.$GLOBALS['_LANG']['ss'][$row['shipping_status']].'</em>';
		// Order step
		if (in_array($row['order_status'], array(OS_CANCELED, OS_INVALID, OS_RETURNED, OS_WAITING_REFUND)))
		{
			$row['step'] = 0;
		}
		else if (in_array($row['pay_status'], array(PS_UNPAYED, PS_PAYING)))
		{
			$row['step'] = 1;
		}
		else if (!in_array($row['shipping_status'], array(SS_SHIPPED, SS_RECEIVED)))
		{
			$row['step'] = 2;
		}
		else if ($row['shipping_status'] != SS_RECEIVED)
		{
			$row['step'] = 3;
		}
		else if (!$row['reviewed'])
		{
			$row['step'] = 4;
		}
		else
		{
			$row['step'] = 5;
		}
		// Order Status, YOHO's way
		if (in_array($row['order_status'], array(OS_CANCELED, OS_INVALID, OS_RETURNED, OS_WAITING_REFUND)))
		{
			$row['order_status'] = '<div><em class="order_status_'.$row['order_status'].' order_step os'.$row['step'].'"><span>'.$GLOBALS['_LANG']['os'][$row['order_status']].'</span></em></div>';
		}
		else if (in_array($row['pay_status'], array(PS_UNPAYED, PS_PAYING)))
		{
			$row['order_status'] = '<div><em class="pay_status_'.$row['pay_status'].' order_step os'.$row['step'].'"><span>'.$GLOBALS['_LANG']['ps'][$row['pay_status']].'</span></em></div>';
		}
		else if ($row['order_status'] == OS_SPLITED && $row['reviewed'] == 0)
		{
			$row['order_status'] = '<div><em class="shipping_status_'.$row['shipping_status'].' order_step os'.$row['step'].'"><span>'.$GLOBALS['_LANG']['wait_review'].'</span></em></div>';
		}
		else
		{
			$row['order_status'] = '<div><em class="shipping_status_'.$row['shipping_status'].' order_step os'.$row['step'].'"><span>'.($row['shipping_status'] == SS_RECEIVED ? $GLOBALS['_LANG']['ss_received'] : $GLOBALS['_LANG']['ss'][$row['shipping_status']]).'</span></em></div>';
		}

		$row['progress'] = $row['step'] * 20;
		$arr[] = array('order_id'       => $row['order_id'],
			'order_sn'       => $row['order_sn'],
			'order_time'     => local_date("Y-m-d H:i", $row['add_time']),
			'order_status'   => $row['order_status'],
			'total_fee'      => price_format($row['total_fee'], 3),
			'total_amount'   => price_format($row['total_amount'], 3),
			'progress'       => $row['progress'],
			'step'           => $row['step'],
			'goods_thumb'    => $row['expensive_goods_thumb'],
			'handler'        => $row['handler']);
	}
	
	return $arr;
}

?>