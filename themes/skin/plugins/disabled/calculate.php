<?php
/*
插件名稱: 計算
描    述: 使用指定公式計算
作    者: David@Shopiy.com
版    本: 1.0
作者網站: http://www.shopiy.com/
*/

/*
參數說明:
number    原始數值
formula   公式
*/

function siy_calculate($atts) {
	eval('$result='.$atts['number'].$atts['formula'].';');
	return $result;
}

?>
