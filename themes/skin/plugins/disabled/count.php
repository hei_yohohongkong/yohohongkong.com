<?php
/*
插件名稱: 統計數組數量
描    述: 統計數組數量
作    者: David@Shopiy.com
版    本: 1.0
作者網站: http://www.shopiy.com/
*/

/*
參數說明:
array     數組
*/

function siy_count($atts) {
	return count($atts['array']);
}

?>
