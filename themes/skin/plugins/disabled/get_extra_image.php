<?php
/*
插件名稱: 獲取圖片
描    述: 獲取圖片並生成img標籤
作    者: David@Shopiy.com
版    本: 1.0
作者網站: http://www.shopiy.com/
*/

/*
參數說明:
prefix    前綴
fname      名稱
suffix    後綴
*/

function siy_get_extra_image($atts) {
	$prefix = !empty($atts['prefix']) ? $atts['prefix'] : '';
	$fname = !empty($atts['fname']) ? $atts['fname'] : '';
	$suffix = !empty($atts['suffix']) ? $atts['suffix'] : '.gif';

	$img = 'static/img/extra/' . $prefix . $fname . $suffix;

	$str = (file_exists(ROOT_PATH.$img)) ? '<img src="'.$GLOBALS['_CFG']['static_path'].$img.'" />' : '';

	return $str;
}

?>
