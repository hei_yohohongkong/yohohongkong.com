<?php
/*
插件名稱: 格式化日期
描    述: 獲取用戶的詳細信息
作    者: David@Shopiy.com
版    本: 1.0
作者網站: http://www.shopiy.com/
*/

/*
參數說明:
date      日期
format    格式
timezone  時區；如果是已調整好的時區的日期，請設置為0
*/

function siy_date_format($atts) {
	$date = !empty($atts['date']) ? (is_numeric($atts['date']) ? $atts['date'] : strtotime($atts['date'])) : gmtime();
	$timezone = isset($atts['timezone']) ? $atts['timezone'] : (isset($_SESSION['timezone']) ? $_SESSION['timezone'] : $GLOBALS['_CFG']['timezone']);
	$date += ($timezone * 3600);
	$format = !empty($atts['format']) ? str_replace('&nbsp;', ' ', $atts['format']) : 'Y-m-d H:i:s';
	$date_out = date($format, $date);
	return $date_out;
}

?>
