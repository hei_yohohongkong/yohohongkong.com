<?php
/*
插件名稱: 商品購買記錄統計
描    述: 商品購買記錄統計
作    者: David@Shopiy.com
版    本: 1.0
作者網站: http://www.shopiy.com/
*/

/*
參數說明:
id        商品id
type      評價類別 0為商品 1為文章
*/

function siy_comment_count($atts) {
	$id = !empty($atts['id']) ? intval($atts['id']) : 0;
	$type = !empty($atts['type']) ? intval($atts['type']) : 0;
	$count = $GLOBALS['db']->getOne('SELECT COUNT(*) FROM ' .$GLOBALS['ecs']->table('comment').
		" WHERE id_value = '$id' AND comment_type = '$type' AND status = 1 AND parent_id = 0");
	return $count;
}

?>
