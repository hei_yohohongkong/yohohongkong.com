<?php
/*
插件名稱: 自定義鏈接生成
描    述: 創建支持重寫的鏈接
作    者: David@Shopiy.com
版    本: 1.0
作者網站: http://www.shopiy.com/
*/

/*
參數說明:
app       category goods brand article_cat article group_buy auction snatch search exchange exchange_goods 

cid       分類id
gid       商品id
bid       品牌id
acid      拍賣活動id
aid       文章id
sid       奪寶奇兵id
gbid      團購id
auid      拍賣活動id
sort      排序方式 goods_id last_update shop_price exchange_integral
order     DESC 降序/ASC 升序

price_min
price_max
filter_attr


append    附加字串
page      頁數
keywords  搜索關鍵詞
size      每頁顯示數
*/

function siy_build_uri($atts) {
	$app       = $atts['app'];
	$params    = $atts;
	$append    = $atts['append'];
	$page      = $atts['page'];
	$keywords  = $atts['keywords'];
	$size      = $atts['size'];

	$uri = build_uri($app, $params, $append, $page, $keywords, $size);

	return $uri;
}

?>
