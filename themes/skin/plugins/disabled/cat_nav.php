<?php
/*
插件名稱: 瀏覽歷史
描    述: 獲取用戶的瀏覽歷史
作    者: David@Shopiy.com
版    本: 1.2
作者網站: http://www.shopiy.com/
*/

/*
參數說明:
cid       母分類id
ccid      當前分類id
check     檢測狀態 parent 母分類是否有有效的子分類 current 是否是當前分類及其母分類（需要同時傳遞當前分類ccid） 
form      模板模塊 默認是library/siy_cat_nav.lbi
*/

function siy_cat_nav($atts) {
	$cid = $atts['cid'];
	$ctype = $atts['ctype'];
	if(empty($cid)) return false;
	$ccid = (!empty($atts['ccid'])) ? $atts['ccid'] : '0';
	$check = $atts['check'];
	if(!empty($check)) {
		if ($ctype == 'c') {// 商品分類
			if ($check == 'parent'){
				$count = $GLOBALS['db']->getOne('SELECT COUNT(*) FROM ' .$GLOBALS['ecs']->table('category').
					'WHERE parent_id = ' . $cid . ' AND is_show = 1');
				$str = ($count > 0) ? 'parent' : '';
			} elseif ($check == 'current') {
				$sql = 'SELECT parent_id FROM ' . $GLOBALS['ecs']->table('category') . " WHERE cat_id = '$ccid'";
				$parent_id = $GLOBALS['db']->getOne($sql);
				$sql = 'SELECT parent_id FROM ' . $GLOBALS['ecs']->table('category') . " WHERE cat_id = '$parent_id'";
				$grand_id = $GLOBALS['db']->getOne($sql);
				if ($cid == $ccid) {
					$str = 'current';
				} elseif ($cid == $parent_id) {
					$str = 'current current_parent';
				} elseif ($cid == $grand_id) {
					$str = 'current current_grand';
				} else {
					return false;
				}
			} else {
				return false;
			}
		} elseif ($ctype == 'a') {// 文章分類
			if ($check == 'parent'){
				$count = $GLOBALS['db']->getOne('SELECT COUNT(*) FROM ' .$GLOBALS['ecs']->table('article_cat').
					'WHERE parent_id = ' . $cid . ' ');
				$str = ($count > 0) ? 'parent' : '';
			} elseif ($check == 'current') {
				$sql = 'SELECT parent_id FROM ' . $GLOBALS['ecs']->table('article_cat') . " WHERE cat_id = '$ccid'";
				$parent_id = $GLOBALS['db']->getOne($sql);
				$sql = 'SELECT parent_id FROM ' . $GLOBALS['ecs']->table('article_cat') . " WHERE cat_id = '$parent_id'";
				$grand_id = $GLOBALS['db']->getOne($sql);
				if ($cid == $ccid) {
					$str = 'current';
				} elseif ($cid == $parent_id) {
					$str = 'current current_parent';
				} elseif ($cid == $grand_id) {
					$str = 'current current_grand';
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	} else {
		if ($ctype == 'c') {// 商品分類
			$sql =  'SELECT cat_id, cat_name ' .
				'FROM ' . $GLOBALS['ecs']->table('category') .
				'WHERE parent_id = ' . $cid . ' AND is_show = 1 ORDER BY sort_order ASC';
			$res = $GLOBALS['db']->getAll($sql);

			$cat_nav = array();
			foreach ($res as $key => $val)
			{
				$cat_nav[$key]['id'] = $val['cat_id'];
				$cat_nav[$key]['name'] = $val['cat_name'];
				$cat_nav[$key]['url'] = build_uri('category', array('cid'=>$val['cat_id']), $val['cat_name']);
			}
		} elseif ($ctype == 'a') {// 文章分類
			$sql =  'SELECT cat_id, cat_name ' .
				'FROM ' . $GLOBALS['ecs']->table('article_cat') .
				'WHERE parent_id = ' . $cid . ' ORDER BY sort_order ASC';
			$res = $GLOBALS['db']->getAll($sql);

			$cat_nav = array();
			foreach ($res as $key => $val)
			{
				$cat_nav[$key]['id'] = $val['cat_id'];
				$cat_nav[$key]['name'] = $val['cat_name'];
				$cat_nav[$key]['url'] = build_uri('article_cat', array('acid'=>$val['cat_id']), $val['cat_name']);
			}
		} else {
			return false;
		}

		$GLOBALS['smarty']->assign('cat_nav', $cat_nav);
		$form = (!empty($atts['form'])) ? $atts['form'] : 'library/siy_cat_nav.lbi';
		$str= $GLOBALS['smarty']->fetch($form);
	}

	return $str;
}

?>
