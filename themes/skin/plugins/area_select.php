<?php

// HTML select for areas
// by howang http://www.howang.hk

function siy_area_select($atts)
{
	$select_name = !empty($atts['select_name']) ? $atts['select_name'] : '';
	$class = !empty($atts['class']) ? $atts['class'] : '';
	$style = !empty($atts['style']) ? $atts['style'] : '';
	$value = !empty($atts['value']) ? $atts['value'] : '';
	
	$please_select = _L('area_select_please_select', '請選擇...');
	
	// Test if current language is sortable (there's no good way to sort Chinese)
	$sortable = mb_check_encoding($please_select, 'ASCII');
	
	$areas = array(
		'香港島' => array('中西區','灣仔區','東區','南區'),
		'九龍' => array('油尖旺區','深水埗區','九龍城區','黃大仙區','觀塘區'),
		'新界' => array('葵青區','荃灣區','屯門區','元朗區','北區','大埔區','沙田區','西貢區','離島區'),
		'其他' => array('中國大陸','澳門','台灣','其他')
	);
	foreach ($areas as $area => $subareas)
	{
		foreach ($subareas as $key => $subarea)
		{
			$subareas[$key] = array(
				'name' => _L('area_select_' . $subarea, $subarea),
				'value' => $subarea
			);
		}
		if ($sortable)
		{
			uasort($subareas, function ($subarea1, $subarea2) {
				// 其他 must be at bottom
				if ($subarea1['value'] == '其他')
				{
					return 1;
				}
				else if ($subarea2['value'] == '其他')
				{
					return -1;
				}
				else
				{
					return strcasecmp($subarea1['name'], $subarea2['name']);
				}
			});
		}
		$areas[$area] = array(
			'name' => _L('area_select_' . $area, $area),
			'value' => $area,
			'subareas' => $subareas
		);
	}
	
	$output  = '<select name="' . $select_name . '" class="' . $class . '" style="' . $style . '">';
	$output .= '<option value=""' . ($value == '' ? ' selected="selected"' : '') . '>' . $please_select . '</option>';
	foreach ($areas as $area)
	{
		$output .= '<optgroup label="' . $area['name'] . '">';
		foreach ($area['subareas'] as $subarea)
		{
			$output .= '<option value="' . $subarea['value'] . '"' . ($value == $subarea['value'] ? ' selected="selected"' : '') . '>' . $subarea['name'] . '</option>';
		}
		$output .= '</optgroup>';
	}
	$output .= '</select>';
	
	return $output;
}

?>