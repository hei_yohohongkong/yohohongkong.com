<?php
/**
* @模板   Vanity
* @版本   1.5.0
* @作者   david http://www.shopiy.com/
* @版權   Copyright (C) 2009 - 2011 Shopiy
* @許可   Shopiy許可協議 (http://www.shopiy.com/license)
*/

$_CFG['static_path'] = ''; // 靜態文件路徑
$_CFG['auto_resize'] = true; // 是否啟用根據顯示器大小自動選擇寬窄風格
$_CFG['three_column'] = true; // 是否啟用首頁的三欄佈局
$_CFG['logo'] = 'yohohk/img/logo.gif'; // 網站Logo
$_CFG['color'] = false; // 配色方案 false為默認 blue/green/black
$_CFG['links_enabled'] = false; // 是否啟用首頁的友情鏈接
$_CFG['tags_enabled'] = false; // 是否啟用商品標籤功能
$_CFG['compare_enabled'] = true; // 是否啟用商品對比功能
$_CFG['goods_popup_menu_enabled'] = false; // 商品列表中商品的彈出菜單
$_CFG['gallery_thumbnails_enabled'] = false; // 整合了「相冊縮略圖」模塊才能啟用此設置，在放大鏡時可以使用商品圖片尺寸的相冊縮略圖。
$_CFG['gallery_thumbnails_small_width'] = '40'; // 整合了「相冊縮略圖」模塊才能啟用此設置，最小縮略圖的寬度。
$_CFG['gallery_thumbnails_small_height'] = '40'; // 整合了「相冊縮略圖」模塊才能啟用此設置，最小縮略圖的高度。
$_CFG['slider_banner_height'] = false; // 首頁滾動商品圖片高度，設置為false取消自定義高度，如果要改為120px高，就把false改為（'120'）引號內。
$_CFG['top_navigator_number'] = '6'; // 頂部導航欄顯示數量限制
$_CFG['main_navigator_number'] = '9'; // 主導航欄顯示數量限制
$_CFG['bottom_navigator_number'] = '12'; // 底部導航欄顯示數量限制
$_CFG['hide_category_extra'] = false; // 是否隱藏全部分類樹右側的「促銷活動」和「推薦品牌」。
$_CFG['cat_promotion_number'] = '20'; // 全部分類樹右側的「促銷活動」顯示數量限制，但為0時隱藏。
$_CFG['cat_brands_number'] = '20'; // 全部分類樹右側的「推薦品牌」顯示數量限制，但為0時隱藏。
$_CFG['gallery_mode'] = 'cloud_zoom'; // 商品相冊展示模式 可選擇：'default','flash','color_box','cloud_zoom'。
$_CFG['display_mode_enabled'] = true; // 是否啟用商品列表的視圖選擇功能
$_CFG['display_list_enabled'] = true; // 是否啟用商品列表的大圖顯示
$_CFG['display_text_enabled'] = true; // 是否啟用商品列表的列表顯示
$_CFG['goods_click_count_enabled'] = true; // 是否啟用商品點擊統計
$_CFG['no_picture'] = 'static/img/no_picture.gif'; // 商品空白圖片
$_CFG['sales_ranking_number'] = '5'; // 銷售排行顯示數量
$_CFG['index_brands_number'] = '10'; // 首頁品牌列表的顯示數量
$_CFG['price_zero_format'] = sprintf($GLOBALS['_CFG']['currency_format'], '0.00'); // 價格為0時的格式化，用在某些判斷價格是否為零的地方。



?>