<?php
/**
* @模板   Vanity
* @版本   1.5.0
* @作者   david http://www.shopiy.com/
* @版權   Copyright (C) 2009 - 2011 Shopiy
* @許可   Shopiy許可協議 (http://www.shopiy.com/license)
*/

if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

@require_once(ROOT_PATH . 'themes/' . $_CFG['template'] . '/options.php');

if (!defined('INIT_NO_SMARTY'))
{
	$hu = $ecs->url();
	$pname = basename($_SERVER['SCRIPT_NAME'], '.php');
	$plugins = glob(ROOT_PATH . 'themes/' . $_CFG['template'] . '/plugins/*.php');
	foreach($plugins as $plugin)
	{
		require_once($plugin);
	}
	$smarty->assign('hu', $hu);
	$smarty->assign('pname', $pname);
	$smarty->assign('option', $_CFG);
}




function siy_function($atts)
{
	return $atts['function']($atts['content']);
}

?>