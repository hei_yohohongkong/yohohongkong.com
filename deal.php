<?php

/***
 * deal.php
 * by howang 2014-06-23
 *
 * Third party warranty registration form
 ***/

define('IN_ECS', true);

// This page don't have mobile version
define('DESKTOP_ONLY', true);

require(dirname(__FILE__) . '/includes/init.php');

if ((DEBUG_MODE & 2) != 2)
{
	$smarty->caching = true;
}

$models_list = $db->getCol("SELECT model_name FROM " . $ecs->table('warranty_models') . " ORDER BY `model_id`");

$action = empty($_REQUEST['act']) ? '' : $_REQUEST['act'];

if (($action == 'register') || ($action == 'aregister'))
{
	$data = array();
	$data['model'] = empty($_POST['model']) ? '' : $_POST['model'];
	$data['mobile'] = empty($_POST['mobile']) ? '' : $_POST['mobile'];
	$data['email'] = empty($_POST['email']) ? '' : $_POST['email'];
	
	$error_msg = '';
	if (!in_array($data['model'], $models_list))
	{
		$error_msg = sprintf(_L('deal_error_invalid', '請輸入正確的%s！'), _L('deal_model', '產品型號'));
	}
	elseif (!validate_mobile($data['mobile']))
	{
		$error_msg = sprintf(_L('deal_error_invalid', '請輸入正確的%s！'), _L('deal_mobile', '手提電話號碼'));
	}
	elseif (!validate_email($data['email']))
	{
		$error_msg = sprintf(_L('deal_error_invalid', '請輸入正確的%s！'), _L('deal_email', '電子郵件地址'));
	}
	else
	{
		$sql = "SELECT '1' FROM " . $ecs->table('warranty_registration') .
				"WHERE model = '" . $data['model'] . "' " .
				"AND mobile = '" . $data['mobile'] . "' " .
				"AND email = '" . $data['email'] . "' ";
		if ($db->getOne($sql))
		{
			$error_msg = _L('deal_error_exists', '您已經登記過了！');
		}
	}
	
	if ($error_msg != '')
	{
		if ($action == 'register')
		{
			show_message($error_msg);
		}
		else // if ($action == 'aregister')
		{
			output_json(array('status' => 0, 'error' => $error_msg));
		}
	}
	
	$data['reg_time'] = gmtime();
	
	$db->autoExecute($ecs->table('warranty_registration'), $data, 'INSERT');
	
	$msg = _L('deal_success', '您已成功登記，謝謝！');
	if ($action == 'register')
	{
		show_message($msg, _L('global_go_home', '返回首頁'), $ecs->url(), 'info');
	}
	else // if ($action == 'aregister')
	{
		output_json(array('status' => 1, 'message' => $msg));
	}
}

/*------------------------------------------------------ */
//-- 判断是否存在缓存，如果存在则调用缓存，反之读取相应内容
/*------------------------------------------------------ */
/* 缓存编号 */
$cache_id = sprintf('%X', crc32($_SESSION['user_rank'] . '-' . $_CFG['lang']));
if (!$smarty->is_cached('deal.dwt', $cache_id))
{
	assign_template();
	$position = assign_ur_here(0, _L('deal_warranty_registration', 'Warranty Registration'));
	$smarty->assign('page_title',	$position['title']);	// 页面标题
	$smarty->assign('ur_here',		$position['ur_here']);  // 当前位置
	/* meta information */
	$smarty->assign('keywords',		htmlspecialchars($_CFG['shop_keywords']));
	$smarty->assign('description',	htmlspecialchars($_CFG['shop_desc']));
	
	//$smarty->assign('helps',       get_shop_help());       // 网店帮助
	//$smarty->assign('t1',          get_ly_newtree(0));// 分类ly sbsn  20130311
	
	$smarty->assign('models_list', $models_list);
}
$smarty->display('deal.dwt', $cache_id);

exit;

function output_json($obj)
{
	header('Content-Type: application/json');
	echo json_encode($obj);
	exit;
}

function validate_email($str)
{
	return preg_match('/^[a-z0-9][\w\.]*@([a-z0-9][a-z0-9-]*\.)+[a-z]{2,5}$/i', $str);
}

function validate_mobile($str)
{
	// 香港手提電話號碼: 5,6或9字頭，共8位數字
	$hk_mobile = preg_match('/^[456789][0-9]{7}$/', $str);
	// 大陸手機號: 1字頭，第二位是345678其中一個，共11位數字
	$mainland_mobile = preg_match('/^1[345678][0-9]{9}$/', $str);
	
	return $hk_mobile || $mainland_mobile;
}

?>