<?php

/**
 * ECSHOP 商品分类
 * ============================================================================
 * * 版权所有 2005-2012 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liubo $
 * $Id: category.php 17217 2011-01-19 06:29:08Z liubo $
*/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

if ((DEBUG_MODE & 2) != 2)
{
    $smarty->caching = true;
}
$attrbuteController = new Yoho\cms\Controller\AttributeController();
$CategoryController = new Yoho\cms\Controller\CategoryController();
$goodsController = new Yoho\cms\Controller\GoodsController();
/*------------------------------------------------------ */
//-- INPUT
/*------------------------------------------------------ */

/* 获得请求的分类 ID */
if (isset($_REQUEST['id']))
{
    $cat_id = intval($_REQUEST['id']);
}
elseif (isset($_REQUEST['category']))
{
    $cat_id = intval($_REQUEST['category']);
}
else
{
    /* 如果分类ID为0，则返回首页 */
    header('Location: /');
    exit;
}

// Redirect user to new URL without the php filename
if (strpos($_SERVER['REQUEST_URI'], 'category.php') !== false)
{
    $new_url = build_uri('category', array('cid' => $cat_id));
    
    // Append with original query string parameters, except the id which is already included in $new_url
    parse_str($_SERVER["QUERY_STRING"], $get);
    if (isset($get['id'])) unset($get['id']);
    if (isset($get['category'])) unset($get['category']);
    if (!empty($get))
    {
        $new_url .= (strpos($new_url, '?') === false ? '?' : '&') . http_build_query($get, '', '&');
    }
    
    header('Location: ' . $new_url);
    exit;
}

/* 初始化分页信息 */
$page = isset($_REQUEST['page'])   && intval($_REQUEST['page'])  > 0 ? intval($_REQUEST['page'])  : 1;
$size = isset($_CFG['page_size'])  && intval($_CFG['page_size']) > 0 ? intval($_CFG['page_size']) : 10;
$brand = isset($_REQUEST['brand']) && intval($_REQUEST['brand']) > 0 ? intval($_REQUEST['brand']) : 0;
$price_max = isset($_REQUEST['price_max']) && intval($_REQUEST['price_max']) > 0 ? intval($_REQUEST['price_max']) : 0;
$price_min = isset($_REQUEST['price_min']) && intval($_REQUEST['price_min']) > 0 ? intval($_REQUEST['price_min']) : 0;
$filter_attr_str = isset($_REQUEST['filter_attr']) ? htmlspecialchars(trim($_REQUEST['filter_attr'])) : '0';

$filter_attr_str = trim(urldecode($filter_attr_str));
$filter_attr_str = preg_match('/^[\d\.]+$/', $filter_attr_str) ? $filter_attr_str : '';
$filter_attr = empty($filter_attr_str) ? '' : explode('.', $filter_attr_str);

/* 排序、显示方式以及类型 */
//$default_display_type = $_CFG['show_order_type'] == '0' ? 'list' : ($_CFG['show_order_type'] == '1' ? 'grid' : 'text');
$default_display_type = $_CFG['show_order_type'] == '0' ? 'list' : 'grid';
$default_sort_order_method = $_CFG['sort_order_method'] == '0' ? 'DESC' : 'ASC';
$default_sort_order_type   = $_CFG['sort_order_type'] == '0' ? 'last_update' : ($_CFG['sort_order_type'] == '1' ? 'shop_price' : 'goods_id');

$sort  = (isset($_REQUEST['sort'])  && in_array(trim(strtolower($_REQUEST['sort'])), array('goods_id', 'shop_price', 'last_update','shownum'))) ? trim($_REQUEST['sort'])  : $default_sort_order_type;
$order = (isset($_REQUEST['order']) && in_array(trim(strtoupper($_REQUEST['order'])), array('ASC', 'DESC')))                              ? trim($_REQUEST['order']) : $default_sort_order_method;
$display  = (isset($_REQUEST['display']) && in_array(trim(strtolower($_REQUEST['display'])), array('list', 'grid', 'text'))) ? trim($_REQUEST['display'])  : (isset($_COOKIE['ECS']['display']) ? $_COOKIE['ECS']['display'] : $default_display_type);
//$display  = in_array($display, array('list', 'grid', 'text')) ? $display : 'text';
$top = isset($_REQUEST['top']) ? $_REQUEST['top'] : 0;
$display  = in_array($display, array('list', 'grid')) ? $display : $default_display_type;
setcookie('ECS[display]', $display, gmtime() + 86400 * 7);

/*------------------------------------------------------ */
//-- PROCESSOR
/*------------------------------------------------------ */

/* 页面的缓存ID */
$cache_id = sprintf('%X', crc32(
    $cat_id . '-' .
    $display . '-' .
    $sort  .'-' .
    $order  .'-' .
    $page . '-' .
    $size . '-' .
    $_SESSION['user_rank'] . '-' .
    $_CFG['lang'] . '-' .
    $brand . '-' .
    $price_max . '-' .
    $price_min . '-' .
    $filter_attr_str
));

if (!$smarty->is_cached('category.dwt', $cache_id))
{
    /* 如果页面没有被缓存则重新获取页面的内容 */

    $children = get_children($cat_id);

    $cat = get_cat_info($cat_id);   // 获得分类的相关信息
    $cat_desc = $cat['cat_desc'];
    if (!empty($cat))
    {
        $smarty->assign('keywords',    htmlspecialchars($cat['keywords']));
        $brandNames = $CategoryController->getDescBrands($cat_id);
        $len = count($brandNames);
        $brand_names = '';
        $insert_pos_zh = strpos($cat_desc,"買") ? strpos($cat_desc,"買") : strpos($cat_desc,"买");
        if($insert_pos_zh) {
            for ($x = 0; $x < $len; $x++) {
                $brandName = $brandNames[$x];
                if ($x < $len-1) {
                    if(strlen($brandName["b_brand_name"])!=0){
                        $brand_names .= $brandName["b_brand_name"] ."、";
                    } 
                    //bl_brand_name: english brand name
                    if(strlen($brandName["bl_brand_name"])!=0 && $brandName["bl_brand_name"] != $brandName["b_brand_name"]){
                        // var_dump($brandName["bl_brand_name"]);
                        $brand_names .= $brandName["bl_brand_name"] ."、";
                    } 
                } else {
                    if(strlen($brandName["bl_brand_name"])!=0 && strlen($brandName["b_brand_name"])!=0){   
                        if($brandName["bl_brand_name"] != $brandName["b_brand_name"]){
                            $brand_names .= $brandName["b_brand_name"] ."、";
                            $brand_names .= $brandName["bl_brand_name"] ." ";   
                        } else {
                            $brand_names .= $brandName["b_brand_name"] ." ";
                        } 
                    } else if (strlen($brandName["bl_brand_name"])!=0 && strlen($brandName["b_brand_name"])==0){
                        $brand_names .= $brandName["bl_brand_name"] ." ";
                    } else if (strlen($brandName["bl_brand_name"])==0 && strlen($brandName["b_brand_name"])!=0){
                        $brand_names .= $brandName["b_brand_name"] ." ";
                    }
                }
            }
            $insert_pos = $insert_pos_zh +4;
            $cat['cat_desc'] = substr_replace($cat_desc, $brand_names , $insert_pos,0);
        } else {
            for ($x = 0; $x < $len; $x++) {
                $brandName = $brandNames[$x];
                if ($x < $len-1) {
                    if(strlen($brandName["bl_brand_name"])!=0){
                        $brand_names .= $brandName["bl_brand_name"] .", ";
                    } 
                } else {
                    if(strlen($brandName["bl_brand_name"])!=0){
                        $brand_names_len = strlen($brand_names);
                        $brand_names = substr($brand_names,0,$brand_names_len-2);
                        $brand_names .= " and " . $brandName["bl_brand_name"] ." ";
                    } 
                }
            }
            $cat['cat_desc'] = $brand_names . $cat_desc;
        }
        $smarty->assign('description', htmlspecialchars($cat['cat_desc']));
        $smarty->assign('cat_style',   htmlspecialchars($cat['style']));
    }
    else
    {
        /* 如果分类不存在则返回首页 */
        header('Location: /');
        exit;
    }

    /* 赋值固定内容 */
    if ($brand > 0)
    {
        $brand_name = hw_get_brand_name($brand);
    }
    else
    {
        $brand_name = '';
    }

    // howang: Disable unused code
    // /* 获取价格分级 */
    // if ($cat['grade'] == 0  && $cat['parent_id'] != 0)
    // {
    //     $cat['grade'] = get_parent_grade($cat_id); //如果当前分类级别为空，取最近的上级分类
    // }
    // 
    // if ($cat['grade'] > 1)
    // {
    //     /* 需要价格分级 */
    // 
    //     /*
    //         算法思路：
    //             1、当分级大于1时，进行价格分级
    //             2、取出该类下商品价格的最大值、最小值
    //             3、根据商品价格的最大值来计算商品价格的分级数量级：
    //                     价格范围(不含最大值)    分级数量级
    //                     0-0.1                   0.001
    //                     0.1-1                   0.01
    //                     1-10                    0.1
    //                     10-100                  1
    //                     100-1000                10
    //                     1000-10000              100
    //             4、计算价格跨度：
    //                     取整((最大值-最小值) / (价格分级数) / 数量级) * 数量级
    //             5、根据价格跨度计算价格范围区间
    //             6、查询数据库
    // 
    //         可能存在问题：
    //             1、
    //             由于价格跨度是由最大值、最小值计算出来的
    //             然后再通过价格跨度来确定显示时的价格范围区间
    //             所以可能会存在价格分级数量不正确的问题
    //             该问题没有证明
    //             2、
    //             当价格=最大值时，分级会多出来，已被证明存在
    //     */
    // 
    //     $sql = "SELECT min(g.shop_price) AS min, max(g.shop_price) as max ".
    //            " FROM " . $ecs->table('goods'). " AS g ".
    //            " WHERE ($children OR " . get_extension_goods($children) . ') AND g.is_delete = 0 AND g.is_on_sale = 1 AND g.is_alone_sale = 1  ';
    //            //获得当前分类下商品价格的最大值、最小值
    // 
    //     $row = $db->getRow($sql);
    // 
    //     // 取得价格分级最小单位级数，比如，千元商品最小以100为级数
    //     $price_grade = 0.0001;
    //     for($i=-2; $i<= log10($row['max']); $i++)
    //     {
    //         $price_grade *= 10;
    //     }
    // 
    //     //跨度
    //     $dx = ceil(($row['max'] - $row['min']) / ($cat['grade']) / $price_grade) * $price_grade;
    //     if($dx == 0)
    //     {
    //         $dx = $price_grade;
    //     }
    // 
    //     for($i = 1; $row['min'] > $dx * $i; $i ++);
    // 
    //     for($j = 1; $row['min'] > $dx * ($i-1) + $price_grade * $j; $j++);
    //     $row['min'] = $dx * ($i-1) + $price_grade * ($j - 1);
    // 
    //     for(; $row['max'] >= $dx * $i; $i ++);
    //     $row['max'] = $dx * ($i) + $price_grade * ($j - 1);
    //     
    //     $step = $price_grade;
    // 
    //     $sql = "SELECT (FLOOR((g.shop_price - $row[min]) / $dx)) AS sn, COUNT(*) AS goods_num  ".
    //            " FROM " . $ecs->table('goods') . " AS g ".
    //            " WHERE ($children OR " . get_extension_goods($children) . ') AND g.is_delete = 0 AND g.is_on_sale = 1 AND g.is_alone_sale = 1 '.
    //            " GROUP BY sn ";
    // 
    //     $price_grade = $db->getAll($sql);
    //     
    //     $price_range = array();
    //     $price_range[] = 0;
    //     foreach ($price_grade as $val)
    //     {
    //         $this_grade = $row['min'] + round($dx * $val['sn']);
    //         if ($this_grade > 0)
    //         {
    //             $price_range[] = $this_grade;
    //         }
    //     }
    //     $price_range[] = $row['min'] + round($dx * ($price_grade[count($price_grade) - 1]['sn'] + 1));
    //     $smarty->assign('price_range',      $price_range);
    //     $smarty->assign('price_range_step', $step);
    //     $smarty->assign('price_range_min',  isset($price_range[0]) ? $price_range[0] : 0);
    //     $smarty->assign('price_range_max',  isset($price_range[0]) ? $price_range[count($price_range) - 1] : 20000);
    // 
    //     foreach ($price_grade as $key=>$val)
    //     {
    //         $temp_key = $key + 1;
    //         $price_grade[$temp_key]['goods_num'] = $val['goods_num'];
    //         $price_grade[$temp_key]['start'] = $row['min'] + round($dx * $val['sn']);
    //         $price_grade[$temp_key]['end'] = $row['min'] + round($dx * ($val['sn'] + 1));
    //         $price_grade[$temp_key]['price_range'] = $price_grade[$temp_key]['start'] . '&nbsp;-&nbsp;' . $price_grade[$temp_key]['end'];
    //         $price_grade[$temp_key]['formated_start'] = price_format($price_grade[$temp_key]['start']);
    //         $price_grade[$temp_key]['formated_end'] = price_format($price_grade[$temp_key]['end']);
    //         $price_grade[$temp_key]['url'] = build_uri('category', array('cid'=>$cat_id, 'bid'=>$brand, 'price_min'=>$price_grade[$temp_key]['start'], 'price_max'=> $price_grade[$temp_key]['end'], 'filter_attr'=>$filter_attr_str), $cat['cat_name']);
    // 
    //         /* 判断价格区间是否被选中 */
    //         if (isset($_REQUEST['price_min']) && $price_grade[$temp_key]['start'] == $price_min && $price_grade[$temp_key]['end'] == $price_max)
    //         {
    //             $price_grade[$temp_key]['selected'] = 1;
    //         }
    //         else
    //         {
    //             $price_grade[$temp_key]['selected'] = 0;
    //         }
    //     }
    // 
    //     $price_grade[0]['start'] = 0;
    //     $price_grade[0]['end'] = 0;
    //     $price_grade[0]['price_range'] = $_LANG['all_attribute'];
    //     $price_grade[0]['url'] = build_uri('category', array('cid'=>$cat_id, 'bid'=>$brand, 'price_min'=>0, 'price_max'=> 0, 'filter_attr'=>$filter_attr_str), $cat['cat_name']);
    //     $price_grade[0]['selected'] = empty($price_max) ? 1 : 0;
    // 
    //     $smarty->assign('price_grade',     $price_grade);
    // 
    // }
    $search_parameters = array('cid'=>$cat_id, 'bid'=>$brand, 'price_min'=>$price_min, 'price_max'=>$price_max, 'filter_attr'=>$filter_attr, 'filter_attr_str'=>$filter_attr_str,'sort'=>$sort,'order'=>$order);
    // Price Filter
    hw_assign_price_range($CategoryController->getRelatedWhere($cat_id), $_SESSION['user_rank']);

    // Brands
    $brands = $CategoryController->getRelatedBrands($cat_id, $search_parameters);
    $smarty->assign('brands', $brands);
    // Attrbute
    $attr_data = $attrbuteController->get_attr_data($cat_id, $filter_attr, 'category',  $search_parameters);
    $ext = $attr_data['ext']; //商品查询条件扩展
    $all_attr_list = $attr_data['all_attr_list'];

    assign_template('c', array($cat_id), 'category');

    $position = assign_ur_here($cat_id, $brand_name);
    $smarty->assign('page_title',       $position['title']);    // 页面标题
    $smarty->assign('ur_here',          $position['ur_here']);  // 当前位置

    $t1 = $CategoryController->getTopCategories(0);
    $smarty->assign('t1',              $t1);
    //$smarty->assign('categories',       get_categories_tree($cat_id)); // 分类树
    $smarty->assign('helps',            get_shop_help());              // 网店帮助
    //$smarty->assign('top_goods',        get_top10());                  // 销售排行
    //$smarty->assign('show_marketprice', $_CFG['show_marketprice']);
    $smarty->assign('category',         $cat);
    $smarty->assign('category_id',      $cat_id);
    $smarty->assign('category_name',    $cat['cat_name']);
    $smarty->assign('filter_attr_list',  $all_attr_list);
    
    $smarty->assign('price_max',        $price_max);
    $smarty->assign('price_min',        $price_min);
    $smarty->assign('filter_attr',      $filter_attr_str);
    $smarty->assign('feed_url',         ($_CFG['rewrite'] == 1) ? "feed-c$cat_id.xml" : 'feed.php?cat=' . $cat_id); // RSS URL

    $smarty->assign('brand_id',         $brand);
    $smarty->assign('brandname',        $brand_name);

    
    $smarty->assign('data_dir',         DATA_DIR);
    
    // $smarty->assign('promotion_info',   get_promotion_info());
    
    // /* 调查 */
    // $vote = get_vote();
    // if (!empty($vote))
    // {
    //     $smarty->assign('vote_id',      $vote['id']);
    //     $smarty->assign('vote',         $vote['content']);
    // }
    
    // $smarty->assign('new_goods',        get_category_recommend_goods('new', $children, $brand, $price_min, $price_max, $ext));
    // $smarty->assign('best_goods',       get_category_recommend_goods('best', $children, $brand, $price_min, $price_max, $ext));
    // $smarty->assign('promotion_goods',  get_category_recommend_goods('promote', $children, $brand, $price_min, $price_max, $ext));
    // $smarty->assign('hot_goods',        get_category_recommend_goods('hot', $children, $brand, $price_min, $price_max, $ext));
    
    // 店長推薦 - 最新「精品」
    // $smarty->assign('bestgoods',       category_get_goods_all(0,0,0,0,'goods_id','desc',0,12,0,"AND $children AND g.is_best=1"));
    // 熱賣產品 - 最新「熱銷」
    // $smarty->assign('hotgoods',        category_get_goods_all(0,0,0,0,'goods_id','desc',0,12,0,"AND $children AND g.is_hot=1"));
    // 新產品 - 最新「新品」
    // $smarty->assign('newgoods',        category_get_goods_all(0,0,0,0,'goods_id','desc',0,12,0,"AND $children AND g.is_new=1"));
    
    $count = get_cagtegory_goods_count($children, $brand, $price_min, $price_max, $ext);
    $max_page = ($count> 0) ? ceil($count / $size) : 1;
    if ($page > $max_page)
    {
        $page = $max_page;
    }
    $goodslist = hw_category_get_goods($children, $brand, $price_min, $price_max, $ext, $size, $page, $sort, $order, 0, $GLOBALS['display'], []);
    $goods_id_arr = array();
    foreach($goodslist as $val){
        $goods_id_arr[] = $val['goods_id'];
    }

    //get favourable list
    $favourable_arr = $goodsController->product_list_get_favourable_info($_SESSION['user_rank'], $goods_id_arr);
    
    //get package list
    $package_arr = $goodsController->product_list_get_package_info($goods_id_arr);

    //get topic list
    $topic_arr = $goodsController->product_list_get_topic_info($goods_id_arr);

    $smarty->assign('goods_list',       $goodslist);
    $smarty->assign('favourable_list',  $favourable_arr);
    $smarty->assign('package_list',     $package_arr);
    $smarty->assign('topic_list',      $topic_arr);
    $smarty->assign('script_name',      'category');
    $smarty->assign('display',          $display);

    // Left side Category Menu
    $smarty->assign('left_cat_menu',         $CategoryController->getLeftCatMenu($cat_id));
    $cat_list = cat_list(0, 0, false, 0, false);
    $group_cat_list = $CategoryController->groupCat($cat_list);
    $level = intval($cat_list[$cat_id]['level']) + 1;
    $smarty->assign('level',           $level);
    switch($level){
        case 1:
            // Hot Category
            $smarty->assign('hot_category',          $CategoryController->getHotCategory(7, $cat_id));
            // Cat session
            $smarty->assign('cat_session',           $group_cat_list[$cat_id]);
            break;
        case 2:
            // Cat session
            $smarty->assign('cat_session',           $group_cat_list[$cat['parent_id']]['children'][$cat_id]);
            break;
    };
    $smarty->assign('sort',             $sort);
    $smarty->assign('order',            $order);
    $smarty->assign('top',              $top);
    // Header image
    $smarty->assign('category_header_image', '/' . get_category_header_image($cat_id));

    foreach ($brands as $row) {
        if ($row['brand_id'] = $brand) {
            $bperma = $row['bperma'];
        }
    }
    assign_pager('category',            $cat_id, $count, $size, $sort, $order, $page, '', $brand, $price_min, $price_max, $display, $filter_attr_str, '', '', ['cperma' => $cat['cperma'], 'bperma' => $bperma]); // 分页
    assign_dynamic('category'); // 动态内容
}

$catlist = array();
foreach(get_parent_cats($cat_id) as $k=>$v)
{
    $catlist[] = $v['cat_id'];
}
$smarty->assign('catlist',  $catlist);           // set parent categorey list

$smarty->display('category.dwt', $cache_id);

/*------------------------------------------------------ */
//-- PRIVATE FUNCTION
/*------------------------------------------------------ */

/**
 * 获得分类的信息
 *
 * @param   integer $cat_id
 *
 * @return  void
 */
function get_cat_info($cat_id)
{
    $sql = "SELECT " .
        "IFNULL(cl.cat_name, c.cat_name) as cat_name, " .
        "IFNULL(cl.keywords, c.keywords) as keywords, " .
        "IFNULL(cl.cat_desc, c.cat_desc) as cat_desc, " .
        "style, grade, filter_attr, parent_id, cperma " .
        "FROM " . $GLOBALS['ecs']->table('category') . " as c " .
        "LEFT JOIN" . $GLOBALS['ecs']->table('category_lang') . " as cl ON c.cat_id = cl.cat_id AND cl.lang = '" . $GLOBALS['_CFG']['lang']  . "' " .
        "LEFT JOIN (SELECT id, table_name, lang, perma_link as cperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") cpl ON cpl.id = c.cat_id  AND cpl.table_name = 'category' AND cpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
        " WHERE c.cat_id = '$cat_id'";
    $row = $GLOBALS['db']->getRow($sql);
    
    if (!empty($row))
    {
        // Absolute URLs
        $row['cat_uri'] = build_uri('category', array('cid'=>$cat_id, 'cname'=>$row['cat_name'], 'cperma' => $row['cperma']));
        $row['cat_desktop_url'] = desktop_url($row['cat_uri']);
        $row['cat_mobile_url'] = mobile_url($row['cat_uri']);
        
        // Language URLs
        $row['cat_lang_urls'] = array();
        foreach (available_languages() as $_lang)
        {
            $sql = "SELECT IFNULL(cl.cat_name, c.cat_name) as cat_name, cperma " .
                    "FROM " . $GLOBALS['ecs']->table('category') . " as c " .
                    "LEFT JOIN " . $GLOBALS['ecs']->table('category_lang') . " as cl " .
                        "ON cl.cat_id = c.cat_id AND cl.lang = '" . $_lang . "' " .
                    "LEFT JOIN (SELECT id, table_name, lang, perma_link as cperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") cpl ON cpl.id = c.cat_id  AND cpl.table_name = 'category' AND cpl.lang = '" . $_lang . "' " .
                    "WHERE c.cat_id = '" . $cat_id . "'";
            $cat_localized = $GLOBALS['db']->getRow($sql);
            $row['cat_lang_urls'][$_lang] = absolute_url('/' . $_lang . build_uri('category', array('cid'=>$cat_id, 'cname'=>$cat_localized['cat_name'], 'cperma'=>$cat_localized['cperma']), '', 0, '', 0, $_lang));
        }
    }
    
    return $row;
}

/**
 * 获得分类下的商品
 *
 * @access  public
 * @param   string  $children
 * @return  array
 */
function category_get_goods($children, $brand, $min, $max, $ext, $size, $page, $sort, $order, $is_promote=0)
{
    $display = $GLOBALS['display'];
    return hw_category_get_goods($children, $brand, $min, $max, $ext, $size, $page, $sort, $order, $is_promote, $display);
}

/**
 * 获得分类下的商品总数
 *
 * @access  public
 * @param   string     $cat_id
 * @return  integer
 */
function get_cagtegory_goods_count($children, $brand = 0, $min = 0, $max = 0, $ext='',$is_promote=0)
{
    return hw_get_cagtegory_goods_count($children, $brand, $min, $max, $ext, $is_promote);
}

/**
 * 取得最近的上级分类的grade值
 *
 * @access  public
 * @param   int     $cat_id    //当前的cat_id
 *
 * @return int
 */
function get_parent_grade($cat_id)
{
    static $res = NULL;

    if ($res === NULL)
    {
        $data = read_static_cache('cat_parent_grade');
        if ($data === false)
        {
            $sql = "SELECT parent_id, cat_id, grade ".
                   " FROM " . $GLOBALS['ecs']->table('category');
            $res = $GLOBALS['db']->getAll($sql);
            write_static_cache('cat_parent_grade', $res);
        }
        else
        {
            $res = $data;
        }
    }

    if (!$res)
    {
        return 0;
    }

    $parent_arr = array();
    $grade_arr = array();

    foreach ($res as $val)
    {
        $parent_arr[$val['cat_id']] = $val['parent_id'];
        $grade_arr[$val['cat_id']] = $val['grade'];
    }

    while ($parent_arr[$cat_id] >0 && $grade_arr[$cat_id] == 0)
    {
        $cat_id = $parent_arr[$cat_id];
    }

    return $grade_arr[$cat_id];

}

function get_ly_arrname($filter_attr_str)
{
	$a = array_shift(explode('.', $filter_attr_str));
	
    return $GLOBALS['db']->getOne('SELECT attr_value FROM ' . $GLOBALS['ecs']->table('goods_attr') . " WHERE goods_attr_id=$a");
}

function get_category_header_image($cat_id)
{
    global $_CFG;
    
    $header_lang_suffix = '_' . $_CFG['lang'];
    $header_image_path = 'data/afficheimg/category/' . $cat_id . '_header' . $header_lang_suffix . '.png';
    if (file_exists(ROOT_PATH . $header_image_path))
    {
        return $header_image_path;
    }
    $header_image_path = 'data/afficheimg/category/' . $cat_id . '_header.png';
    if (file_exists(ROOT_PATH . $header_image_path))
    {
        return $header_image_path;
    }
}

?>