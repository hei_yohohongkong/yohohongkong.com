<?php

define('IN_ECS', true);

define('FORCE_HTTPS', true);

define('DESKTOP_ONLY', true); // This page don't have mobile version

define('IS_POS', true);

require(dirname(__FILE__) . '/includes/init.php');
require(ROOT_PATH . 'includes/lib_order.php');
require(ROOT_PATH . 'includes/lib_order_1.php');
require(ROOT_PATH . 'includes/lib_erp.php');
require_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_accounting.php');
$orderController = new Yoho\cms\Controller\OrderController();
$rspController   = new Yoho\cms\Controller\RspController();
$order_fee_platform = $orderController::ORDER_FEE_PLATFORM_POS;

$_CFG['lang'] = 'zh_tw'; // only TC version for pos

/* 载入语言文件 */
require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/user.php');
require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/shopping_flow.php');
require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/order_print.php');
$erpController = new Yoho\cms\Controller\ErpController();
$rmaController = new Yoho\cms\Controller\RmaController();
$addressController = new Yoho\cms\Controller\AddressController();

/*------------------------------------------------------ */
//-- PROCESSOR
/*------------------------------------------------------ */

$smarty->assign('lang', $_LANG);


// Check admin login
$admin = check_admin_login();
if (!$admin)
{
	show_message($_LANG['not_login'], $_LANG['back_home'], $ecs->url(), 'error');
	exit;
}
$smarty->assign('admin', $admin);

$position = assign_ur_here(0, 'POS 系統');
$smarty->assign('page_title',	  $position['title']);	// 页面标题
$smarty->assign('ur_here',		 $position['ur_here']);  // 当前位置

/*------------------------------------------------------ */
//-- 完成所有订单操作，提交到数据库
/*------------------------------------------------------ */
if ($_REQUEST['step'] == 'done')
{
	include_once('includes/lib_clips.php');
    include_once('includes/lib_payment.php');

    /* 取得 POS 模式 */
    $mode = empty($_POST['mode']) ? 'normal' : trim($_POST['mode']);
    $mode = in_array($mode, array('normal','reserve','replace','sales-agent','ex_normal')) ? $mode : 'normal';
	// $isWholesale = $_POST['is_wholesale']=='1'?TRUE:FALSE;

	/* 取得购物类型 */
	$flow_type = isset($_SESSION['flow_type']) ? intval($_SESSION['flow_type']) : CART_GENERAL_GOODS;
	
	/*  如果有自定義商品價錢 */
    // $custom_price = empty($_REQUEST['custom_price']) ? array() : $_REQUEST['custom_price'];
    
    // $got_custom_price = empty($custom_price) ? FALSE : TRUE;
    $got_custom_price = FALSE;

	/* 订单中的商品 */
    $user_name = empty($_POST['user_name'])? trim($_POST['mobile']) : trim($_POST['user_name']);
    $cart_goods = cart_goods_1($flow_type, $custom_price, $user_name, 0, -1, false);
    
    $sa_offline_redeem = FALSE;

    if (empty($cart_goods))
    {
       show_message('您的訂單中沒有任何產品', '返回 POS 系統', '/pos_new.php', 'warning');
       exit;
    }
	
	/* 检查商品总额是否达到最低限购金额 */
	if ($flow_type == CART_GENERAL_GOODS && cart_amount_1(true, CART_GENERAL_GOODS) < $_CFG['min_goods_amount'])
	{
		show_message(sprintf($_LANG['goods_amount_not_enough'], price_format($_CFG['min_goods_amount'], false)));
	}
	
	/* 检查商品库存 */
	/* 如果使用库存，且下订单时减库存，则减少库存 */
	if ($_CFG['use_storage'] == '1' && $_CFG['stock_dec_time'] == SDT_PLACE)
	{
		$cart_goods_stock = get_cart_goods_1();
		$_cart_goods_stock = array();
		foreach ($cart_goods_stock['goods_list'] as $value)
		{
			$_cart_goods_stock[$value['goods_id']] = $value['goods_number'];
		}
		flow_cart_stock_1($_cart_goods_stock);
		unset($cart_goods_stock, $_cart_goods_stock);
	}

	// howang: auto populate email for user
	if (!empty($_POST['user_id']))
	{
		$user_name = trim($_POST['user_id']);
		$sql = "SELECT user_id, email, is_wholesale FROM " . $ecs->table('users') . " WHERE user_name = '$user_name'";
		$row = $db->getRow($sql);
		if (empty($row))
		{
			$_POST['user_id'] = '';
		}
		else
		{
			$_POST['user_id'] = $row['user_id'];
			if (empty($_POST['email'])) $_POST['email'] = $row['email'];
            //block incorrect wholesale order
            if($_POST['is_wholesale']=='1' && $row['is_wholesale']==0){
                show_message('此會員並非批發會員。您可以 1)使用「批發銷售」 或2)建立新批發會員', '返回 POS 系統', '/pos_new.php', 'warning');
                exit;
            }
		}

	}
	
	// howang: check if user already exists
	if ((empty($_POST['user_id'])) && (!empty($_POST['mobile'])))
	{
		$user_name = trim($_POST['mobile']);
		$sql = "SELECT user_id, email,is_wholesale FROM " . $ecs->table('users') . " WHERE user_name = '$user_name'";
		$row = $db->getRow($sql);
		if (empty($row))
		{
			$_POST['user_id'] = '';
		}
		else
		{
			$_POST['user_id'] = $row['user_id'];
			if (empty($_POST['email'])) $_POST['email'] = $row['email'];
            //block incorrect wholesale order
            if($_POST['is_wholesale']=='1' && $row['is_wholesale']==0){
                show_message('此會員並非批發會員。您可以 1)使用「批發銷售」 或2)建立新批發會員', '返回 POS 系統', '/pos_new.php', 'warning');
                exit;
            }
		}

	}

	// Handle : 電郵已有登記用戶但與$username 不符
	if(!empty($_POST['email'])){
		$email    = trim($_POST['email']);
		$user_name = trim($_POST['user_id']);
		$sql = "SELECT user_id, email, is_wholesale FROM " . $ecs->table('users') . " WHERE email = '".$email."'";
		$row = $db->getRow($sql);
		if (!empty($row) && ($row['user_id'] != $user_name)){ // if have user using this email and not match $user_name
			$_POST['email'] = ''; // clear email.
		}
	}

	/* 注冊新會員 */
	if (($_POST['user_id'] == '') && (!empty($_POST['mobile'])))
	{
		include_once(ROOT_PATH . 'includes/lib_passport.php');
		$username = empty($_POST['mobile'])? '' : make_semiangle(trim($_POST['mobile']));
		$email    = isset($_POST['email']) ? trim($_POST['email']) : '';
		$email    = is_email($email) ? $email :  $username.'@yohohongkong.com';
        
        while ($username == "" || $user->check_user($username) || admin_registered($username))
		{
			$username = $_POST['mobile'] ? $_POST['mobile'] . "_" : "rand";
       		$username .= mt_rand(100, 999);        	
		}
		
        $password = "yohohongkong"; 
        $other['msn']          = '';
        $other['qq']           = '';
        $other['office_phone'] = '';
        $other['home_phone']   = empty($_POST['tel'])? '' : make_semiangle(trim($_POST['tel']));
        $other['mobile_phone'] = empty($_POST['mobile']) ? '' : make_semiangle(trim($_POST['mobile']));
        $other['is_wholesale'] = $isWholesale;
        $is_pos = true;
        if (register($username, $password, $email, $other, $is_pos) !== false)
        {
			$smarty->assign('username', $username);
			$smarty->assign('password', $password);
			$sql = "SELECT user_id FROM " . $ecs->table('users') . " WHERE user_name = '$username'";
			$_POST['user_id'] = $db->getOne($sql);
            
            // send verification email
            if (!is_placeholder_email($email))
            {
    		    send_regiter_hash_async($_POST['user_id']);
            }
        }

	}
	
    /* 保存收货人信息 */
    if ($_POST['shipping_type'] == 'retail_store') {
        $consignee = [];
        $consignee['consignee']  = null;
        $consignee['email']      = $_POST['email'];
        $consignee['zipcode']    = '';
        $consignee['tel']        = $_POST['mobile'];
        $consignee['mobile']     = $_POST['mobile'];
        $consignee['best_time']  = '';
        $consignee['city']       = 0;
        $consignee['country']    = 3409;
        $consignee['district']   = 0;
    } else if ($_POST['shipping_type'] == 'express') {

        if(isset($_POST['new_address'])) { // new address, only save into order info and order address, do not update into user address
            $new_address = $_POST['new_address'];
            $consignee = $addressController->breakOffAddress($new_address['address'], $new_address['country']);
            $consignee = $addressController->handle_address_detail($consignee, false);

            $consignee['address_id']    = empty($_POST['address_id'])      ? 0     :   intval($_POST['address_id']);
            $consignee['consignee']     = empty($new_address['name'])      ? ''    :   compile_str(trim($new_address['name']));
            $consignee['email']         = empty($_POST['email'])           ? 0     :   compile_str($_POST['email']);
            $consignee['zipcode']       = empty($new_address['zipcode'])   ? ''    :   compile_str(make_semiangle(trim($new_address['zipcode'])));
            $consignee['tel']           = empty($new_address['phone'])     ? ''    :   compile_str(make_semiangle(trim($new_address['phone'])));
            $consignee['mobile']        = empty($new_address['phone'])     ? ''    :   compile_str(make_semiangle(trim($new_address['phone'])));
            $consignee['country']       = empty($new_address['country'])   ? 3409  :   intval($new_address['country']);
            $consignee['lift']          = empty($new_address['lift'])      ? ''    :   compile_str(make_semiangle(trim($new_address['lift'])));
            $consignee['dest_type']     = empty($new_address['dest_type']) ? ''    :compile_str(make_semiangle(trim($new_address['dest_type'])));
        } else {
            $consignee = get_consignee($_POST['user_id'], TRUE);
        }
    } else if ($_POST['shipping_type'] == 'pickuppoint') {
        $consignee = $addressController->locker_consignee($_POST['user_id'], $_POST['locker_info']);
    }

	// Shipping handle: value= 99, sa shipping
	$shipping_id = isset($_POST['shipping']) ? intval($_POST['shipping']) : 0;
	// if($shipping_id == 99){
	// 	// Block if order type is not sa
	// 	if($mode != 'sales-agent'){
	// 		show_message('此速遞只供代理銷售使用', '返回 POS 系統', '/pos.php', 'warning');
	// 		exit;
	// 	}
	// 	$region = ['district'=>$consignee['district'],'city'=>$consignee['city'],'province'=>$consignee['province'],'country'=>$consignee['country']];
	// 	$shipping_list = available_shipping_list($region);
    //     foreach ($shipping_list as $k => $shipping) {
    //         if(in_array($shipping['shipping_id'], [2, 4, 5])) unset($shipping_list[$k]);
	// 	}
	// 	$shipping_list = array_values($shipping_list);
	// 	// Default only get the first shipping and if not fund we will use '速遞'
	// 	$shipping_id = empty($shipping_list[0]['shipping_id']) ? 3 : $shipping_list[0]['shipping_id'];
	// }
    $_POST['how_oos']        = isset($_POST['how_oos']) ? intval($_POST['how_oos']) : 0;
    $_POST['card_message']   = isset($_POST['card_message']) ? htmlspecialchars($_POST['card_message']) : '';
    $_POST['inv_type']       = !empty($_POST['inv_type']) ? htmlspecialchars($_POST['inv_type']) : '';
    $_POST['inv_payee']      = isset($_POST['inv_payee']) ? htmlspecialchars($_POST['inv_payee']) : '';
    $_POST['inv_content']    = isset($_POST['inv_content']) ? htmlspecialchars($_POST['inv_content']) : '';
	$_POST['postscript']     = isset($_POST['postscript']) ? trim($_POST['postscript']) : '';
	$_POST['internal_remarks']  = isset($_POST['internal_remarks']) ? trim($_POST['internal_remarks']) : '';
	$_POST['order_remarks']  = isset($_POST['order_remarks']) ? trim($_POST['order_remarks']) : '';
    $_POST['serial_numbers'] = isset($_POST['serial_numbers']) ? trim($_POST['serial_numbers']) : '';
    $order = array(
		'shipping_id'     => $shipping_id,
        'pay_id'          => isset($_POST['payment']) ? intval($_POST['payment']) : 0,
        'pack_id'         => isset($_POST['pack']) ? intval($_POST['pack']) : 0,
        'card_id'         => isset($_POST['card']) ? intval($_POST['card']) : 0,
        'card_message'    => trim($_POST['card_message']),
        'surplus'         => isset($_POST['surplus']) ? floatval($_POST['surplus']) : 0.00,
        'integral'        => isset($_POST['integral']) ? intval($_POST['integral']) : 0,
        'bonus_id'        => isset($_POST['bonus']) ? intval($_POST['bonus']) : 0,
        'coupon_code'     => is_array($_POST['coupon']) ? array_filter(array_map('trim', $_POST['coupon'])) : array_filter(array_map('trim',explode(',',$_POST['coupon']))),
        'need_inv'        => empty($_POST['need_inv']) ? 0 : 1,
        'inv_type'        => $_POST['inv_type'],
        'inv_payee'       => trim($_POST['inv_payee']),
        'inv_content'     => $_POST['inv_content'],
		'postscript'      => trim($_POST['postscript']),
		'to_buyer'        => trim($_POST['internal_remarks']),
		'order_remarks'   => trim($_POST['order_remarks']),
        'serial_numbers'  => trim($_POST['serial_numbers']),
        'how_oos'         => isset($_LANG['oos'][$_POST['how_oos']]) ? addslashes($_LANG['oos'][$_POST['how_oos']]) : '',
        'need_insure'     => isset($_POST['need_insure']) ? intval($_POST['need_insure']) : 0,
        'user_id'         => trim($_POST['user_id']),
        'add_time'        => gmtime(),
        'order_status'    => OS_UNCONFIRMED,
        'shipping_status' => SS_UNSHIPPED,
        'pay_status'      => PS_UNPAYED,
        'agency_id'       => get_agency_by_regions(array($consignee['country'], $consignee['province'], $consignee['city'], $consignee['district'])),
		'user_agency_id'  => $user_agency_id
    );

	/* 收货人信息 */
    foreach ($consignee as $key => $value)
    {
        $order[$key] = addslashes($value);
    }
    
    if ($mode == 'normal' && $order['pay_id'] == 99){
        $sa_offline_redeem = TRUE;

        $order['type_id'] = $_POST['sa_id'];
        $order['type_sn'] = $_POST['sa_sn'];

        $order['pay_id'] = 11;

        $mode = 'sales-agent';
    }

    if($mode == 'sales-agent')
	{
		$order['order_type'] = \Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT;
	}

	// if($isWholesale==true){
    //     $order['order_type'] = Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE;
    // }

    /* 扩展信息 */
    if (isset($_SESSION['flow_type']) && intval($_SESSION['flow_type']) != CART_GENERAL_GOODS)
    {
        $order['extension_code'] = $_SESSION['extension_code'];
        $order['extension_id'] = $_SESSION['extension_id'];
    }
    else
    {
        $order['extension_code'] = '';
        $order['extension_id'] = 0;
    }

    /* 检查积分余额是否合法 */
    //$user_id = $_SESSION['user_id'];
	$user_id = $_POST['user_id'];
    if ($user_id > 0)
    {
        $user_info = user_info($user_id);

        $order['surplus'] = min($order['surplus'], $user_info['user_money'] + $user_info['credit_line']);
        if ($order['surplus'] < 0)
        {
            $order['surplus'] = 0;
        }

        // 查询用户有多少积分
        $flow_points = flow_available_points_1();  // 该订单允许使用的积分
        $user_points = $user_info['pay_points']; // 用户的积分总数

        $order['integral'] = min($order['integral'], $user_points, $flow_points);
        if ($order['integral'] < 0)
        {
            $order['integral'] = 0;
        }
    }
    else
    {
        $order['surplus']  = 0;
        $order['integral'] = 0;
    }

    /* 检查红包是否存在 */
    if ($order['bonus_id'] > 0)
    {
        $bonus = bonus_info($order['bonus_id']);

        if (empty($bonus) || $bonus['user_id'] != $user_id || $bonus['order_id'] > 0 || $bonus['min_goods_amount'] > cart_amount_1(true, $flow_type))
        {
            $order['bonus_id'] = 0;
        }
    }
    elseif (isset($_POST['bonus_sn']))
    {
        $bonus_sn = trim($_POST['bonus_sn']);
        $bonus = bonus_info(0, $bonus_sn);
        $now = gmtime();
        if (empty($bonus) || $bonus['user_id'] > 0 || $bonus['order_id'] > 0 || $bonus['min_goods_amount'] > cart_amount_1(true, $flow_type) || $now > $bonus['use_end_date'])
        {
        }
        else
        {
            if ($user_id > 0)
            {
                $sql = "UPDATE " . $ecs->table('user_bonus') . " SET user_id = '$user_id' WHERE bonus_id = '$bonus[bonus_id]' LIMIT 1";
                $db->query($sql);
            }
            $order['bonus_id'] = $bonus['bonus_id'];
            $order['bonus_sn'] = $bonus_sn;
        }
    }
    
    // Check coupon
    $coupon_promote = [];
	if (!empty($order['coupon_code']))
	{
		$order['coupon_code'] = array_unique(array_map('strtoupper', $order['coupon_code']));
		
		$cart_amount = cart_amount_1(true, $flow_type);
		
		foreach ($order['coupon_code'] as $coupon_code)
		{
			$coupon = coupon_info(0, $coupon_code);
			
			if (verify_coupon($coupon, $order, $cart_amount, $cart_goods, $user_id))
			{
				// Assign coupon_id to order
				$order['coupon_id'] = isset($order['coupon_id']) ? $order['coupon_id'] : array();
				$order['coupon_id'][] = $coupon['coupon_id'];
				$order['postscript'] = $coupon['is_remark'] ? $order['postscript']."\n".$coupon['coupon_name'] : $order['postscript'];
                if (!empty($coupon['promote_id'])) {
                    $promote = [
                        "promote_id"    => $coupon['promote_id'],
                        "coupon_id"     => $coupon['coupon_id'],
                        "data"          => [],
                    ];
                    if ($coupon['promote_id'] == 1) {
                        $promote['data']['theClubId'] = trim($_POST['theClubId']);
                    }
                    $coupon_promote[] = $promote;
                }
			}
		}
	}
	
	// howang: 檢查是否包括需訂貨產品
    $goods_preorder = check_goods_preorder($cart_goods);
    if (!empty($goods_preorder))
    {
        $preorder_msg = '';
        foreach ($goods_preorder as $preorder)
        {
            $preorder_msg .= $preorder['goods_name'] . ' ' . $preorder['preorder_mode'] . ' ' . $preorder['pre_sale_days'] . '天';
            //$preorder_msg .= ' (' . $preorder['pre_sale_date_formatted'] . ')';
            $preorder_msg .= "\n";
        }
        $order['to_buyer'] = (empty($order['to_buyer']) ? '' : $order['to_buyer']);
    }

    /* 订单中的总额 */
    $total = $orderController->order_fee_flow($order, $cart_goods, $consignee, $order_fee_platform, $_POST['vardiscount']);
    
    $order['bonus']        = $total['bonus'];
    $order['coupon']       = $total['coupon'];
    $order['goods_amount'] = $total['goods_price'];
    $order['discount']     = $total['discount'];
    $order['surplus']      = $total['surplus'];
    $order['tax']          = $total['tax'];
    $order['cp_price']     = !empty($total['cp_price']) ? $total['cp_price'] : '0';
    
    // 购物车中的商品能享受红包支付的总额
    $discount_amout = compute_discount_amount_1();
    // 红包和积分最多能支付的金额为商品总额
    $temp_amout = $order['goods_amount'] - $discount_amout;
    if ($temp_amout <= 0)
    {
        $order['bonus_id'] = 0;
        $order['coupon_id'] = array();
    }

    /* 配送方式 */
    if ($order['shipping_id'] > 0)
    {
        $shipping = shipping_info($order['shipping_id']);
        $order['shipping_name'] = addslashes($shipping['shipping_name']);
    }
    $order['shipping_fee'] = $total['shipping_fee'];
    $order['insure_fee']   = $total['shipping_insure'];

    /* 支付方式 */
    if ($order['pay_id'] > 0)
    {
        $payment = payment_info($order['pay_id']);
        // $order['pay_name'] = addslashes($payment['pay_name']);
        $order['pay_name'] = addslashes($payment['display_name']);
    }
    $order['pay_fee'] = $total['pay_fee'];
    $order['cod_fee'] = $total['cod_fee'];

    /* 商品包装 */
    if ($order['pack_id'] > 0)
    {
        $pack               = pack_info($order['pack_id']);
        $order['pack_name'] = addslashes($pack['pack_name']);
    }
    $order['pack_fee'] = $total['pack_fee'];

    /* 祝福贺卡 */
    if ($order['card_id'] > 0)
    {
        $card               = card_info($order['card_id']);
        $order['card_name'] = addslashes($card['card_name']);
    }
    $order['card_fee']      = $total['card_fee'];

    $order['order_amount']  = number_format($total['amount'], 2, '.', '');

    /* 如果全部使用余额支付，检查余额是否足够 */
    if ($payment['pay_code'] == 'balance' && $order['order_amount'] > 0)
    {
        if($order['surplus'] >0) //余额支付里如果输入了一个金额
        {
            $order['order_amount'] = $order['order_amount'] + $order['surplus'];
            $order['surplus'] = 0;
        }
        if ($order['order_amount'] > ($user_info['user_money'] + $user_info['credit_line']))
        {
           show_message($_LANG['balance_not_enough']);
        }
        else
        {
            $order['surplus'] = $order['order_amount'];
            $order['order_amount'] = 0;
        }
    }

    /* 如果订单金额为0（使用余额或积分或红包支付），修改订单状态为已确认、已付款 */
    if ($order['order_amount'] <= 0)
    {
        $order['order_status'] = OS_CONFIRMED;
        $order['confirm_time'] = gmtime();
        $order['pay_status']   = PS_PAYED;
        $order['pay_time']     = gmtime();
        $order['order_amount'] = 0;
    }

    $order['integral_money']   = $total['integral_money'];
    $order['integral']         = $total['integral'];

    if ($order['extension_code'] == 'exchange_goods')
    {
        $order['integral_money']   = 0;
        $order['integral']         = $total['exchange_integral'];
    }

    $order['from_ad']          = !empty($_SESSION['from_ad']) ? $_SESSION['from_ad'] : '0';
    $order['referer']          = !empty($_SESSION['referer']) ? addslashes($_SESSION['referer']) : '';

    /* 记录扩展信息 */
    if ($flow_type != CART_GENERAL_GOODS)
    {
        $order['extension_code'] = $_SESSION['extension_code'];
        $order['extension_id'] = $_SESSION['extension_id'];
    }

    // 經 POS 系統下單不計算推薦人 (以免員工不小心按下了 affiliate link 而令所有門市訂單錯誤歸納為推薦訂單)
    // $affiliate = unserialize($_CFG['affiliate']);
    // if(isset($affiliate['on']) && $affiliate['on'] == 1 && $affiliate['config']['separate_by'] == 1)
    // {
    //     //推荐订单分成
    //     $parent_id = get_affiliate();
    //     if($user_id == $parent_id)
    //     {
    //         $parent_id = 0;
    //     }
    // }
    // elseif(isset($affiliate['on']) && $affiliate['on'] == 1 && $affiliate['config']['separate_by'] == 0)
    // {
    //     //推荐注册分成
    //     $parent_id = 0;
    // }
    // else
    // {
    //     //分成功能关闭
    //     $parent_id = 0;
    // }
    // $order['parent_id'] = $parent_id;
	$order['is_separate'] = $orderController::ORDER_SEPARATE_IGNORE;
	// 記錄此訂單相關的銷售人員以及銷售平台
	$order['salesperson'] = empty($_POST['salesperson']) ? 'admin' : $_POST['salesperson'];
	$order['platform'] = 'pos';
	// Clear address_id
    $order['address_id'] = 0;

    $arr_sa_goods_price = [];

    // if($isWholesale==true){
    //     //Wholesale to be unpayed , confirmed, shipped.
    //     $order['order_status'] = OS_CONFIRMED;
    //     $order['pay_status'] = PS_UNPAYED;
    //     $order['shipping_status'] = SS_RECEIVED;
    //     $order['confirm_time'] = $order['add_time'];
    //     $order['shipping_time'] = $order['add_time'];
    //     $order['money_paid'] = 0;

    //     $order['order_type'] = Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE;
    //     $order['type_sn'] = $_POST['type_sn'];
    //     $order['ws_customer_id'] = $_POST['ws_customer_id'];
    //     $order['type_id'] = $user_id;

    // }else
    
    if (($mode == 'normal') || ($mode == 'replace')){
        // 正常銷售：初始為已確認、已付款、已收貨
        $order['order_status'] = OS_CONFIRMED;
        $order['pay_status'] = PS_PAYED;
        $order['shipping_status'] = SS_RECEIVED;
        $order['confirm_time'] = $order['add_time'];
        $order['pay_time'] = $order['add_time'];
        $order['shipping_time'] = $order['add_time'];
        $order['money_paid'] = $order['order_amount'];
        $order['order_amount'] = 0;
        
	}else if ($mode == 'reserve'){
		// 訂貨：初始為已確認、未付款/已付款、未發貨
		$order['order_status'] = OS_CONFIRMED;
		$order['confirm_time'] = $order['add_time'];
		$order['shipping_status'] = SS_UNSHIPPED;
		$order['shipping_time'] = 0;
		
		$deposit = empty($_POST['deposit']) ? 0 : doubleval($_POST['deposit']);
		if (!empty($deposit) && $deposit < $order['order_amount'])
		{
			$order['pay_status'] = PS_UNPAYED;
        	$order['pay_time'] = 0;
        	$order['money_paid'] = $deposit;
        	$order['order_amount'] = $order['order_amount'] - $deposit;
		}
		else
		{
        	$order['pay_status'] = PS_PAYED;
        	$order['pay_time'] = $order['add_time'];
        	$order['money_paid'] = $order['order_amount'];
        	$order['order_amount'] = 0;
		}
	}else if ($mode == 'sales-agent'){

        // 代理銷售：初始為已確認、已付款、未發貨

		$order['order_status'] = OS_CONFIRMED;
        $order['pay_status'] = PS_PAYED;
        if ($sa_offline_redeem && !$got_custom_price){
            $order['shipping_status'] = SS_RECEIVED;
        } else {
            $order['shipping_status'] = SS_UNSHIPPED;
        }
		$order['confirm_time'] = $order['add_time'];
		$order['pay_time'] = $order['add_time'];
		$order['shipping_time'] = 0;
		$order['money_paid'] = $order['order_amount'];
		$order['order_amount'] = 0;
		$order['type_id'] = $_POST['sa_id'];
        $order['type_sn'] = $_POST['sa_sn'];

        if(!$got_custom_price){
            $goods_got_price = TRUE;
            foreach ($_POST['goods_number'] as $good_id => $good_no){
                $good_ga_price = $orderController->getGoodGaPrice($_POST['sa_id'], $good_id);
                if (!empty($good_ga_price)){
                    $arr_sa_goods_price[$good_id] = $good_ga_price;
                } else {
                    $goods_got_price = FALSE;
                    $arr_sa_goods_price[$good_id] = 0;
                }
            }

            if($sa_offline_redeem && !$goods_got_price){
                show_message('未能找到訂單貨品的代理銷售價錢', '返回 POS 系統', '/pos_new.php', 'warning');
                exit;
            }

            $total_ag_price = 0;
            foreach ($_POST['goods_number'] as $good_id => $good_no){
                $good_price = $arr_sa_goods_price[$good_id]*$good_no;
                $total_ag_price += $good_price;
                $order['order_amount'] = 0;
                $order['money_paid'] = $order['goods_amount'] = round($total_ag_price,0);
            }

        }

		$order['order_type'] = \Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT;
	}
	
    /* 插入订单表 */
    $error_no = 0;
    do
    {
        $order['order_sn'] = get_order_sn(); //获取新订单号
        $GLOBALS['db']->autoExecute($GLOBALS['ecs']->table('order_info'), $order, 'INSERT', '', 'SILENT');

        $error_no = $GLOBALS['db']->errno();

        if ($error_no > 0 && $error_no != 1062)
        {
            die($GLOBALS['db']->errorMsg());
        }
    }
    while ($error_no == 1062); //如果是订单号重复则重新提交数据

    $new_order_id = $db->insert_id();
    $order['order_id'] = $new_order_id;
	
	$user_rank = empty($user_info['user_rank']) ? 0 : $user_info['user_rank'];
    /* 插入订单商品 */
    
        // TODO: If is vip user, insert vip price to order_goods table, but cart1 goods_price is saving goods origin price/promote_price
        $sql = "INSERT INTO " . $ecs->table('order_goods') . "( " .
                    "order_id, goods_id, goods_name, goods_sn, product_id, goods_number, market_price, ".
                    "goods_price, goods_attr, is_real, extension_code, parent_id, is_gift, is_package, goods_attr_id, cost, user_rank) ".
                " SELECT '$new_order_id', c.goods_id, c.goods_name, c.goods_sn, c.product_id, c.goods_number, c.market_price, ";
        if ($mode == 'sales-agent'){
            $sql .= "IFNULL(sag.sa_price, c.goods_price), ";
        } else {
            $sql .= "CASE WHEN c.is_gift > 0 THEN c.goods_price WHEN c.is_package > 0 THEN c.goods_price ELSE LEAST(IFNULL(mp.user_price, c.goods_price),c.goods_price) END, ";
        }
        $sql .= "c.goods_attr, c.is_real, c.extension_code, c.parent_id, c.is_gift, c.is_package, c.goods_attr_id, g.cost, c.user_rank  ".
                " FROM " . $ecs->table('cart1') . "as c ".
                " LEFT JOIN " . $ecs->table('goods') . " as g ON c.goods_id = g.goods_id ".
                " LEFT JOIN " . $GLOBALS['ecs']->table('member_price') . " AS mp ON mp.goods_id = c.goods_id AND mp.user_rank = '$user_rank' ";
        if ($mode == 'sales-agent'){
            $sql .= "LEFT JOIN " . $GLOBALS['ecs']->table('sales_agent_goods') . " AS sag ON sag.sa_id = '" . $_POST['sa_id'] . "' AND sag.goods_id = g.goods_id ";
        }
        $sql .= " WHERE session_id = '".SESS_ID."' AND rec_type = '$flow_type'";
        $db->query($sql);
    
    // update warehouse_id (POS)
    $shop_warehouse_id = $erpController->getShopWarehouseId();
    // if ($isWholesale == true) {
    //     $shop_warehouse_id = $erpController->getMainWarehouseId();
    // } else {
    //     $shop_warehouse_id = $erpController->getShopWarehouseId();
    // }

    $orderController->updateOrderGoodsWarehouseId($new_order_id,$shop_warehouse_id);

    /* 標記VIP價錢  */
    $sql = "SELECT og.rec_id, og.goods_id, og.goods_price, u.user_id, og.user_rank as goods_user_rank FROM " . $ecs->table('order_goods') . ' og' .
            " LEFT JOIN " . $ecs->table('order_info') . " oi ON og.order_id = oi.order_id" .
            " LEFT JOIN " . $ecs->table('users') . " u ON oi.user_id = u.user_id" .
            " WHERE og.order_id = " . $new_order_id;
    $goods_list = $db->getAll($sql);
    foreach($goods_list as $goods){
        $is_vip = intval($orderController->is_vip_price($goods['goods_id'],$goods['user_id'],$goods['goods_price']));

        if($is_vip) { // Anthony: 如果購物車價格為特殊價格, 但等同VIP價, 預設為VIP價.
            $sql = "UPDATE " . $ecs->table('order_goods') . " SET user_rank = 2 " .
                " WHERE is_gift = 0 AND is_package = 0 AND order_id = " . $new_order_id . " AND goods_id = " . $goods['goods_id'] . " AND rec_id = " . $goods['rec_id'];
            $db->query($sql);
        }
    }

    if (!empty($coupon_promote)) {
        foreach ($coupon_promote as $promote) {
            $db->query("INSERT INTO " . $ecs->table("coupon_promote_info") . " (order_id, promote_id, user_id, coupon_id, data) VALUES ('$new_order_id', '$promote[promote_id]', '$order[user_id]', '$promote[coupon_id]', '" . \json_encode($promote['data']) . "')");
        }
    }

	/* Insert Order address */
    $order_address_id = $orderController->update_order_address($consignee, $new_order_id);
	$order['address_id'] = $order_address_id;

	/* 修改拍卖活动状态 */
    if ($order['extension_code']=='auction')
    {
        $sql = "UPDATE ". $ecs->table('goods_activity') ." SET is_finished='2' WHERE act_id=".$order['extension_id'];
        $db->query($sql);
    }
    
    /* 处理余额、积分 */
    if ($order['user_id'] > 0 && $order['surplus'] > 0)
    {
        log_account_change($order['user_id'], $order['surplus'] * (-1), 0, 0, 0, sprintf($_LANG['pay_order'], $order['order_sn']));
    }
    if ($order['user_id'] > 0 && $order['integral'] > 0)
    {
        log_account_change($order['user_id'], 0, 0, 0, $order['integral'] * (-1), sprintf($_LANG['pay_order'], $order['order_sn']), ACT_OTHER, 0, ['type' => 'sales', 'sales_order_id' => $new_order_id]);
    }
    
    /* 处理红包 */
    if ($order['bonus_id'] > 0 && $temp_amout > 0)
    {
        use_bonus($order['bonus_id'], $new_order_id);
    }
    
    // Use coupon
    if (!empty($order['coupon_id']))
    {
	    foreach ($order['coupon_id'] as $coupon_id)
	    {
		    use_coupon($coupon_id, $new_order_id, $order['user_id']);
		}
    }
	
	// 如果有付款，記錄到 Accounting System 中
	if ($order['money_paid'] > 0)
	{
		// Accounting System, order paid
		$actgHooks->orderPaid($new_order_id, $order['money_paid'], '', 0, true, $admin['user_id']);
	}
	
	// 如果已全數付款並且是已出貨狀態...
	if (($order['order_amount'] <= 0) && (in_array($order['shipping_status'], array(SS_SHIPPED, SS_RECEIVED))))
	{
		$erpController->pos_erp_delivery($new_order_id, $admin, $mode);
	}
	
	// 處理換貨
	if (($mode == 'replace') &&
		(!empty($_POST['replace'])) && (is_array($_POST['replace'])) &&
		(!empty($_POST['replace_qty'])) && (is_array($_POST['replace_qty'])))
	{
        $replace_order_id = empty($_POST['replace_order_id']) ? 0 : intval($_POST['replace_order_id']);
        
		// Link replaced order to replacing order
		$sql = "UPDATE " . $ecs->table('order_info') . " SET `replace_order_id` = '" . $replace_order_id . "' WHERE `order_id` = '" . $order['order_id'] ."' LIMIT 1";
		$db->query($sql);
		
		// Save replaced quantity on replaced goods
		foreach ($_POST['replace_qty'] as $rec_id => $qty)
		{
			$sql = "UPDATE " . $ecs->table('order_goods') . " SET `replaced_qty` = `replaced_qty` + " . intval($qty) . " WHERE `rec_id` = '" . intval($rec_id) ."' AND `order_id` = '" . $replace_order_id . "' LIMIT 1";
			$db->query($sql);
		}
        
		// 自動入庫
        //1 prepare warehouseRec[]
        $warehouseRec=[];
        foreach($_POST['replace'] as $recId=>$warehouseId){
            if(!$warehouseRec[$warehouseId])
                $warehouseRec[$warehouseId]=[];
            $warehouseRec[$warehouseId][$recId]=$_POST['replace_qty'][$recId];
        }

        //2 loop through warehouserecords
        foreach($warehouseRec as $warehouseId=>$items){
            //2-1 create erp order
            $warehousing_sn = 'POS' . $order['order_id'];
            $erp_warehousing = array(
                'warehousing_sn'       => 'POS' . $order['order_id'],
                'warehousing_style_id' => '3', // 入貨類型: 退貨入庫
                'warehouse_id'         => $warehouseId, // 貨倉: 總部貨倉
                'warehousing_from'     => empty($_POST['salesperson']) ? 'POS' : $_POST['salesperson'], // 交貨人
                'order_id'             => $replace_order_id,
                'create_time'          => $order['add_time'],
                'create_by'            => empty($admin['user_id']) ? 1 : $admin['user_id'],
                'post_time'            => $order['add_time'],
                'post_by'              => empty($admin['user_id']) ? 1 : $admin['user_id'],
                'post_remark'          => $order['postscript'],
                'status'               => '2', // 入貨單狀態: 主管審核中
            );
            $db->autoExecute($ecs->table('erp_warehousing'), $erp_warehousing, 'INSERT');
            $warehousing_id = $db->insert_id();
            $erp_warehousing['warehousing_id'] = $warehousing_id;

            //2-2 loop through items and add to erp order
            //2-2-1 insert all items with dummy qty first
            /* 插入订单商品 */
            $sql = "INSERT INTO " . $ecs->table('erp_warehousing_item') .
                " (warehousing_id, goods_id, attr_id, received_qty, price, order_item_id) ".
                " SELECT '" . $warehousing_id . "', goods_id, ".
                    "(SELECT attr_id FROM " . $ecs->table('erp_goods_attr') . " WHERE goods_id = og.goods_id AND goods_attr_id_list = '$$'), ".
                    "goods_number, 0, rec_id ".
                " FROM " .$ecs->table('order_goods') . " as og ".
                " WHERE rec_id " . db_create_in(array_keys($items));
            $db->query($sql);

            //2-2-2 then now update correct qty
            // 修正入庫數量
            $goods_to_rma = [];
            foreach ($items as $rec_id => $qty){
                $sql = "UPDATE " . $ecs->table('erp_warehousing_item') . " " .
                        "SET `received_qty` = " . intval($qty) . " " .
                        "WHERE `warehousing_id` = '" . $warehousing_id . "' " .
                            "AND `order_item_id` = '" . intval($rec_id) ."' " .
                        "LIMIT 1";
                $db->query($sql);

                // if warehouse id = 2 暫存倉(需檢查/維修), add to RMA
                if ($warehouseId == 2) {
                    if ($qty > 0) {
                        $sql= "SELECT goods_id FROM " . $ecs->table('order_goods') . " where rec_id = '".$rec_id."' ";
                        $replace_item = $db->getRow($sql);
                        $operator = empty($_POST['salesperson']) ? 'admin' : $_POST['salesperson'];
                        $reason = $_POST['postscript'];
                        $new_rma_id = $rmaController->createRma($rmaController::CHANNEL_REPLACEMENT,null,$replace_order_id,$replace_item['goods_id'],$qty,$operator,$reason);
                        $goods_to_rma[$replace_item['goods_id']] = $new_rma_id;
                    }
                }
            }
            //3 auto approval
            $replace_order_sn = $db->getOne("SELECT order_sn FROM " . $ecs->table('order_info') . " WHERE order_id = $replace_order_id");
            $baseTxn = [
                'sales_order_id'	=> $replace_order_id,
                'warehousing_id'	=> $warehousing_id,
                'remark'			=> "訂單 $replace_order_sn 換貨入庫 $warehousing_sn",
                'created_by'        => empty($admin['user_id']) ? 1 : $admin['user_id']
            ];
            $erpController->approve_erp_warehousing($erp_warehousing, 'POS換貨自動入庫', $admin, $baseTxn);

            // update rma id to stock table
            foreach($goods_to_rma as $goods_id => $rma_id ) {
                $sql = 'update '.$ecs->table('erp_stock').' set rma_id = '.$rma_id.' where w_d_sn = "'.$warehousing_sn.'" and warehouse_id = 2 and w_d_style_id = 3 and goods_id = '.$goods_id.' ';
                $db->query($sql);
            }

            unset($warehouseId);
            unset($erp_warehousing);
            unset($items);
        }
    }
    
    if ($mode == 'sales-agent' && $sa_offline_redeem && !$got_custom_price){
        $arr_sa_good_rate = []; // set to 0
        foreach ($arr_sa_goods_price as $goods_id => $goods_price){
            $sa_rate_id = $orderController->getSaRateId($_POST['sa_id']);
            if (!empty($sa_rate_id)){
                $arr_sa_good_rate[$goods_id] = $sa_rate_id;
            } else {
                $arr_sa_good_rate[$goods_id] = 0;
            }
        }
        $_POST['goods_rate'] = $arr_sa_good_rate;
    }
	
	// 處理代理銷售
	if(($mode == 'sales-agent') && (!empty($_POST['goods_rate'])) && (is_array($_POST['goods_rate']))){
		// Save goods rate on order goods
		foreach ($_POST['goods_rate'] as $goods_id => $sa_rate_id)
		{
			$sql = "UPDATE " . $ecs->table('order_goods') . " SET `sa_rate_id` = " . $sa_rate_id. " ".
							" WHERE `order_id` = '" . $order['order_id'] . "' AND `goods_id` = '" . $goods_id . "' LIMIT 1";
			$db->query($sql);
		}
	}

    // If have custom price , update order_goods table: goods_price
    if ($mode == 'sales-agent' && $sa_offline_redeem && !$got_custom_price){

            foreach ($arr_sa_goods_price as $goods_id => $goods_price)
            {
                $sql = "UPDATE " . $ecs->table('order_goods') . " SET `goods_price` = '" . $goods_price . "' , user_rank = 0 ".
                                " WHERE `order_id` = '" . $order['order_id'] . "' AND `goods_id` = '" . $goods_id . "' LIMIT 1";

                $db->query($sql);
            }

    } else {
        // if(!empty($custom_price) && is_array($custom_price)){
        //     foreach ($custom_price as $goods_id => $new_goods_price)
        //     {
        //         $sql = "UPDATE " . $ecs->table('order_goods') . " SET `goods_price` = '" . $new_goods_price."' , user_rank = 0 ". // If we have custom price, not count it is user rank price
        //                         " WHERE `order_id` = '" . $order['order_id'] . "' AND `goods_id` = '" . $goods_id . "' LIMIT 1";
        //         $db->query($sql);
        //     }
        // }
    }

    /* 给商家发邮件 */
    /* 增加是否给客服发送邮件选项 */
    if ($_CFG['send_service_email'] && $_CFG['service_email'] != '')
    {
        $tpl = get_mail_template('remind_of_new_order');
        $smarty->assign('order', $order);
        $smarty->assign('goods_list', $cart_goods);
        $smarty->assign('shop_name', $_CFG['shop_name']);
        $smarty->assign('send_date', date($_CFG['time_format']));
        $content = $smarty->fetch('str:' . $tpl['template_content']);
        send_mail($_CFG['shop_name'], $_CFG['service_email'], $tpl['template_subject'], $content, $tpl['is_html']);
    }
	
    /* 如果需要，发短信 */
    if ($_CFG['sms_order_placed'] == '1' && $_CFG['sms_shop_mobile'] != '')
    {
        include_once('includes/cls_sms.php');
        $sms = new sms();
        $msg = $order['pay_status'] == PS_UNPAYED ?
            $_LANG['order_placed_sms'] : $_LANG['order_placed_sms'] . '[' . $_LANG['sms_paid'] . ']';
        $sms->send($_CFG['sms_shop_mobile'], sprintf($msg, $order['consignee'], $order['tel']), 0);
    }
    // Create need removal form.
    $need_removal_list = $rspController->needRemovalGoods($cart_goods);
    if(!empty($need_removal_list)) {
        $rspController->handle_insert_record($_REQUEST, $rspController::RSP_FROM_VOICE, $order['order_id']);
    }
    /* 清空购物车 */
    clear_cart_1($flow_type);
	
    /* 清除缓存，否则买了商品，但是前台页面读取缓存，商品数量不减少 */
    clear_all_files();
	
	/* 插入支付日志 */
    $order['log_id'] = insert_pay_log($new_order_id, $order['order_amount'], PAY_ORDER);
	
    /* 订单信息 */
    $smarty->assign('order',      $order);
    $smarty->assign('total',      $total);
    $smarty->assign('goods_list', $cart_goods);
    $smarty->assign('order_submit_back', sprintf($_LANG['order_submit_back'], $_LANG['back_home'], $_LANG['goto_user_center'])); // 返回提示
    
    if(!empty($order['shipping_name']))
    {
        $order['shipping_name'] = trim(stripcslashes($order['shipping_name']));
    }
    
    user_uc_call('add_feed', array($order['order_id'], BUY_GOODS)); //推送feed到uc
    unset($_SESSION['flow_consignee']); // 清除session中保存的收货人信息
    unset($_SESSION['flow_order']);
	unset($_SESSION['direct_shopping']);

    $smarty->assign('step', $_REQUEST['step']);

    // Handle back POS
    $refer = $_SERVER['HTTP_REFERER'];
    $path = parse_url($refer, PHP_URL_PATH);
    $pathFragments = explode('/', $path);
    $end = end($pathFragments);
    if(in_array($end, ['pos_new.php'])){
        $back_href = "/".$end;
    } else {
        $back_href = '/pos_new.php';
    }
    $smarty->assign('back_href', $back_href);
    $smarty->display('pos/pos_done.dwt');

}
?>
