<?php

/**
 * ECSHOP 文章内容
 * ============================================================================
 * * 版权所有 2005-2012 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liubo $
 * $Id: article.php 17217 2011-01-19 06:29:08Z liubo $
*/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

if ((DEBUG_MODE & 2) != 2)
{
    $smarty->caching = true;
}

if (!isSecure()) {
    $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: ' . $redirect);
    exit;
}

/*------------------------------------------------------ */
//-- INPUT
/*------------------------------------------------------ */

$_REQUEST['id'] = isset($_REQUEST['id']) ? intval($_REQUEST['id']) : 0;
$article_id     = $_REQUEST['id'];
if(isset($_REQUEST['cat_id']) && $_REQUEST['cat_id'] < 0)
{
    $article_id = $db->getOne("SELECT article_id FROM " . $ecs->table('article') . " WHERE cat_id = '".intval($_REQUEST['cat_id'])."' ");
}

/*------------------------------------------------------ */
//-- PROCESSOR
/*------------------------------------------------------ */

$cache_id = sprintf('%X', crc32($article_id . '-' . $_CFG['lang']));

if (!$smarty->is_cached('article.dwt', $cache_id))
{
    /* 文章详情 */
    $article = get_article_info($article_id);

    if (empty($article))
    {
        header('Location: /');
        exit;
    }

    if (!empty($article['link']) && $article['link'] != 'http://' && $article['link'] != 'https://')
    {
        header('Location: ' . $article['link']);
        exit;
    }

    $catlist = array_map(function ($cat) { return $cat['cat_id']; }, get_article_parent_cats($article['cat_id']));
    assign_template('a', $catlist);

    $position = assign_ur_here($article['cat_id'], $article['title']);
    $smarty->assign('page_title',       $position['title']);    // 页面标题
    $smarty->assign('ur_here',          $position['ur_here']);  // 当前位置
    $smarty->assign('id',               $article_id);
    $smarty->assign('article',          $article);
    $smarty->assign('keywords',         htmlspecialchars(empty($article['keywords']) ? $_CFG['shop_keywords'] : $article['keywords']));
    $smarty->assign('description',      htmlspecialchars(empty($article['description']) ? $_CFG['shop_desc'] : $article['description']));
    $smarty->assign('helps',            get_shop_help()); // 网店帮助
    $smarty->assign('hot_lists',            get_hot_article($article['cat_id'])); // Hot articles list
    $CategoryController = new Yoho\cms\Controller\CategoryController();
    $t1 = $CategoryController->getTopCategories(0);
    $smarty->assign('t1',              $t1);
    $smarty->assign('article_categories', article_categories_tree($article['cat_id'])); //文章分类树
//    $smarty->assign('related_goods',    article_related_goods($_REQUEST['id']));  // 相關商品

    // $smarty->assign('username',         $_SESSION['user_name']);
    // $smarty->assign('email',            $_SESSION['email']);
    // $smarty->assign('type',            '1');

    // /* 验证码相关设置 */
    // if ((intval($_CFG['captcha']) & CAPTCHA_COMMENT) && gd_version() > 0)
    // {
    //     $smarty->assign('enabled_captcha', 1);
    //     $smarty->assign('rand',            mt_rand());
    // }

    // $smarty->assign('comment_type', 1);

    // /* 相关商品 */
    $sql = "SELECT a.goods_id, g.goods_name " .
             "FROM " . $ecs->table('goods_article') . " AS a, " . $ecs->table('goods') . " AS g " .
             "WHERE a.goods_id = g.goods_id " .
             "AND a.article_id = '$_REQUEST[id]' ";
    $arr = $db->getAll($sql);
    $goods_id = array();
    foreach ($arr as $good)
    {
        array_push($goods_id, $good['goods_id']);
    }

    $sort_goods_arr = array();
    $sql = hw_goods_list_sql(db_create_in($goods_id, 'g.goods_id'), 'goods_id', 'DESC');
    $res = $db->getAll($sql);
    $goods_list = hw_process_goods_rows($res);

    $smarty->assign('goods_list',   $goods_list);          // 商品列表

     /* 上一篇下一篇文章 */
     $next_article = $db->getRow(
         "SELECT article_id, title, aperma FROM " .$ecs->table('article'). ' a ' .
         "LEFT JOIN (SELECT id, table_name, lang, perma_link as aperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") apl ON apl.id = a.article_id  AND apl.table_name = 'article' AND apl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
         " WHERE article_id > $article_id AND cat_id=$article[cat_id] AND is_open=1 LIMIT 1"
    );
     if (!empty($next_article))
     {
         $next_article = localize_db_row('article', $next_article);
         $next_article['url'] = build_uri('article', array('aid'=>$next_article['article_id'], 'aperma' => $next_article['aperma']), $next_article['title']);
         $smarty->assign('next_article', $next_article);
     }

     $prev_aid = $db->getOne("SELECT max(article_id) FROM " . $ecs->table('article') . " WHERE article_id < $article_id AND cat_id=$article[cat_id] AND is_open=1");
     if (!empty($prev_aid))
     {
         $prev_article = $db->getRow(
             "SELECT article_id, title, aperma FROM " .$ecs->table('article'). ' a ' .
             "LEFT JOIN (SELECT id, table_name, lang, perma_link as aperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") apl ON apl.id = a.article_id  AND apl.table_name = 'article' AND apl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
             " WHERE article_id = $prev_aid"
        );
         $prev_article = localize_db_row('article', $prev_article);
         $prev_article['url'] = build_uri('article', array('aid'=>$prev_article['article_id'], 'aperma'=>$prev_article['aperma']), $prev_article['title']);
         $smarty->assign('prev_article', $prev_article);
     }

    assign_dynamic('article');
}

/* 更新点击次数 */
$db->query('UPDATE ' . $ecs->table('article') . " SET click_count = click_count + 1 WHERE article_id = '$_REQUEST[id]'");

$smarty->display('article.dwt', $cache_id);

?>