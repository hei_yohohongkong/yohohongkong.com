<?php

/**
 * ECSHOP 商品详情
 * ============================================================================
 * * 版权所有 2005-2012 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liubo $
 * $Id: goods.php 17217 2011-01-19 06:29:08Z liubo $
*/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require(ROOT_PATH . 'includes/lib_order.php');

if ((DEBUG_MODE & 2) != 2)
{
    $smarty->caching = true;
}

$fleashdealController = new Yoho\cms\Controller\FlashdealController();
$feedController       = new Yoho\cms\Controller\FeedController();
$packageController = new Yoho\cms\Controller\PackageController();
$orderController   = new Yoho\cms\Controller\OrderController();
$order_fee_platform = $orderController::ORDER_FEE_PLATFORM_WEB;
// $affiliate = unserialize($GLOBALS['_CFG']['affiliate']);
// $smarty->assign('affiliate', $affiliate);

/*------------------------------------------------------ */
//-- INPUT
/*------------------------------------------------------ */

$goods_id = isset($_REQUEST['id'])  ? intval($_REQUEST['id']) : 0;

// goods id 19445 is the first product create on 2018-07-01, after 19445 products will redirect with https.
if (!empty($goods_id) && $goods_id >= 19445) {
    if (!isSecure()) {
        $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        header('HTTP/1.1 301 Moved Permanently');
        header('Location: ' . $redirect);
        exit;
    }
}

// Redirect user to new URL without the php filename
if (strpos($_SERVER['REQUEST_URI'], 'goods.php') !== false)
{
    $new_url = build_uri('goods', array('gid'=>$goods_id));
    
    // Append with original query string parameters, except the id which is already included in $new_url
    parse_str($_SERVER["QUERY_STRING"], $get);
    if (isset($get['id'])) unset($get['id']);
    if (!empty($get))
    {
        $new_url .= (strpos($new_url, '?') === false ? '?' : '&') . http_build_query($get, '', '&');
    }
    
    header('Location: ' . $new_url);
    exit;
}

// If goods is on flashdeal today
$flashdeal_id = $_REQUEST['fid'] ? intval($_REQUEST['fid']) : $fleashdealController->check_goods_is_flashdeal($goods_id);
$is_flashdeal_today = $fleashdealController->is_flashdeal_today();
//$is_flashdeal_today = false;

/*------------------------------------------------------ */
//-- 改变属性、数量时重新计算商品价格
/*------------------------------------------------------ */

if (!empty($_REQUEST['act']) && $_REQUEST['act'] == 'price')
{
    include('includes/cls_json.php');

    $json   = new JSON;
    $res    = array('err_msg' => '', 'result' => '', 'qty' => 1);

    $attr_id    = isset($_REQUEST['attr']) ? explode(',', $_REQUEST['attr']) : array();
    $number     = (isset($_REQUEST['number'])) ? intval($_REQUEST['number']) : 1;

    if ($goods_id == 0)
    {
        $res['err_msg'] = $_LANG['err_change_attr'];
        $res['err_no']  = 1;
    }
    else
    {
        if ($number == 0)
        {
            $res['qty'] = $number = 1;
        }
        else
        {
            $res['qty'] = $number;
        }

        $final_price     = get_final_price($goods_id, $number, true, $attr_id);
        $shop_price      = $final_price['final_price'];
        $goods_user_rank =  $final_price['user_rank_id'];
        $res['result'] = price_format($shop_price * $number);
    }

    die($json->encode($res));
}


/*------------------------------------------------------ */
//-- 商品购买记录ajax处理
/*------------------------------------------------------ */

if (!empty($_REQUEST['act']) && $_REQUEST['act'] == 'gotopage')
{
    include('includes/cls_json.php');

    $json   = new JSON;
    $res    = array('err_msg' => '', 'result' => '');

    $goods_id   = isset($_REQUEST['id']) ? intval($_REQUEST['id']) : 0;
    $page    = (isset($_REQUEST['page'])) ? intval($_REQUEST['page']) : 1;

    if (!empty($goods_id))
    {
        $need_cache = $GLOBALS['smarty']->caching;
        $need_compile = $GLOBALS['smarty']->force_compile;

        $GLOBALS['smarty']->caching = false;
        $GLOBALS['smarty']->force_compile = true;

        /* 商品购买记录 */
        $sql = 'SELECT u.user_name,u.user_rank, og.goods_number, oi.add_time, og.goods_name ,og.goods_price, IF(oi.order_status IN (2, 3, 4), 0, 1) AS order_status ' .
               'FROM ' . $ecs->table('order_info') . ' AS oi LEFT JOIN ' . $ecs->table('users') . ' AS u ON oi.user_id = u.user_id, ' . $ecs->table('order_goods') . ' AS og ' .
               'WHERE oi.order_id = og.order_id AND og.goods_id = ' . $goods_id . ' ORDER BY oi.add_time DESC LIMIT ' . (($page > 1) ? ($page-1) : 0) * 5 . ',5';
        $bought_notes = $db->getAll($sql);

        foreach ($bought_notes as $key => $val)
		
        {
            $bought_notes[$key]['add_time'] = local_date("Y-m-d G:i:s", $val['add_time']);
				$bought_notes[$key]['consignee1'] =getstr($val['user_name'],4);  //增加收货人真实名
        }

        $sql = 'SELECT count(*) ' .
               'FROM ' . $ecs->table('order_info') . ' AS oi LEFT JOIN ' . $ecs->table('users') . ' AS u ON oi.user_id = u.user_id, ' . $ecs->table('order_goods') . ' AS og ' .
               'WHERE oi.order_id = og.order_id  AND og.goods_id = ' . $goods_id;
        $count = $db->getOne($sql);
		$size = 5;
        $page_count = ($count > 0) ? intval(ceil($count / $size)) : 1;
	

        /* 商品购买记录分页样式 */
        /* 分页样式 */
        $_pagenum = 10;     // 显示的页码
        $_offset = 4;       // 当前页偏移值
        $_from = $_to = 0;  // 开始页, 结束页
        if($_pagenum > $page_count)
        {
            $_from = 1;
            $_to = $page_count;
        }
        else
        {
            $_from = $page - $_offset;
            $_to = $_from + $_pagenum - 1;
            if($_from < 1)
            {
                $_to = $page + 1 - $_from;
                $_from = 1;
                if($_to - $_from < $_pagenum)
                {
                $_to = $_pagenum;
                }
            }
            elseif($_to > $page_count)
            {
                $_from = $page_count - $_pagenum + 1;
                $_to = $page_count;
            }
        }
        $pager = array();
        $pager['page']         = $page ;
        $pager['size']         = $size ;
        $pager['record_count'] = $count;
        $pager['page_count']   = $page_count;
        $pager['page_first']   = "1";
        $pager['page_prev']    = $page > 1 ? $page-1 : '1';
        $pager['page_next']    = $page < $page_count  ? $page+1 : '1';
        $pager['page_last']    = $page < $page_count ? $page_count : '1';
        $pager['page_number'] = array();
        for ($i=$_from;$i<=$_to;++$i)
        {
            $pager['page_number'][$i] = $url_format . $i;
        }
        $smarty->assign('notes', $bought_notes);
        $smarty->assign('pager', $pager);
        
        
        $res['result'] = $GLOBALS['smarty']->fetch('library/bought_notes.lbi');
        
        $GLOBALS['smarty']->caching = $need_cache;
        $GLOBALS['smarty']->force_compile = $need_compile;
    }

    foreach($res as $color)
    {
        echo $color;
    }
    exit();
}


/*------------------------------------------------------ */
//-- PROCESSOR
/*------------------------------------------------------ */
//Clear cache when flashdeal start
$now = gmtime();
$todaystarttime = local_strtotime(local_date('Y-m-d 00:00:00'));

$is_now = $db->getOne(
    "SELECT 1 ".
    "FROM ".$ecs->table('flashdeals')." AS fd ".
    "LEFT JOIN ".$ecs->table('flashdeal_event')." AS fde ON fde.fd_event_id = fd.fd_event_id ".
    "WHERE fde.status = 1 AND fd.deal_date BETWEEN '" . $todaystarttime . "' AND '" . $now . "' "
);

if (isset($_REQUEST['c_ur']) && !empty($_REQUEST['c_lang']) && !empty($_REQUEST['c_fdid'])) {
    $_SESSION['user_rank'] = $_REQUEST['c_ur'];
    $_CFG['lang'] = $_REQUEST['c_lang'];
    $flashdeal_id = $_REQUEST['c_fdid'];
    $_REQUEST['fid'] = $_REQUEST['c_fdid'];
    $is_now = 1;
    $is_testing = '';
}

$cache_id = $goods_id . '-' . $_SESSION['user_rank'] . '-' . $_CFG['lang'].'-'.$flashdeal_id.'-'.$is_now.'-'.$is_testing;
$cache_id = sprintf('%X', crc32($cache_id));
$cache_id =  $goods_id . '-' . $cache_id ;

$smarty->assign('is_flashdeal_today', $is_flashdeal_today);

$smarty->assign('now_time',  gmtime());           // 当前系统时间
$VipController = new Yoho\cms\Controller\VipController();

//获取关联礼包
if ($is_flashdeal_today && $flashdeal_id > 0) {
    $package_goods_list = [];
} else {
    $package_goods_list = $packageController->get_package_goods_list($goods_id);
}

//print_r($package_goods_list);
$smarty->assign('package_goods_list',$package_goods_list);    // 获取关联礼包
assign_dynamic('goods');
$volume_price_list = get_volume_price_list($goods_id, '1');
$smarty->assign('volume_price_list',$volume_price_list);    // 商品优惠价格区间

//获取優惠活動
if ($is_flashdeal_today && $flashdeal_id > 0) {
    $favourable_list_by_goods = [];
} else {
    $favourable_list_by_goods = $orderController->favourable_list_by_goods($_SESSION['user_rank'], $_SESSION['user_id'], $goods_id, $order_fee_platform);
}
$smarty->assign('favourable_list_by_goods',$favourable_list_by_goods);    // 获取優惠活動

//insurance

$insurance_plan_list = $orderController->getInsurancePlanByGoodsID($goods_id); // insurance plan list of this good
$is_insurance = FALSE; // if good got insurance plan
$type_insurance = array();

if (!empty($insurance_plan_list)){
    $is_insurance = TRUE;
    foreach ($insurance_plan_list as $plan_name => $val){
        array_push($type_insurance, $plan_name);
    }
}

$smarty->assign('is_insurance',$is_insurance); 

$smarty->assign('type_insurance',$type_insurance);

$smarty->assign('insurance_plan_list',$insurance_plan_list); 

// if ($is_testing){
//     $goods = get_goods_info($goods_id,true);
// } else {
//     $goods = get_goods_info($goods_id);
// }

if (!$smarty->is_cached('goods.dwt', $cache_id))
{
    $smarty->assign('image_width',    $_CFG['image_width']);
    $smarty->assign('image_height',   $_CFG['image_height']);
    $smarty->assign('helps',          get_shop_help()); // 网店帮助
    $smarty->assign('id',             $goods_id);
    $smarty->assign('type',           0);
    $smarty->assign('cfg',            $_CFG);
    $smarty->assign('promotion',      get_promotion_info($goods_id));//促销信息
    $smarty->assign('promotion_info', get_promotion_info());
    if($flashdeal_id > 0) {
        $smarty->assign('flashdeal_id', $flashdeal_id);
    }

    /* 获得商品的信息 */

    if ($is_testing){
        $goods = get_goods_info($goods_id,true);
    } else {
        $goods = get_goods_info($goods_id);
    }

    if ($goods === false)
    {
        /* 如果没有找到任何记录则跳回到首页 */
        ecs_header("Location: /\n");
        exit;
    }
    else
    {
        // YOHO: Don't show accurate stock number
        $goods['goods_number'] = $goods['goods_number'] < 20 ? $goods['goods_number'] : 20;
        
        if ($goods['brand_id'] > 0)
        {
            $goods['goods_brand_url'] = build_uri('brand', array('bid'=>$goods['brand_id'], 'bperma'=>$goods['bperma']), $goods['goods_brand']);
            $goods['goods_brand_logo'] = $db->getOne("SELECT brand_logo FROM " . $ecs->table('brand') . " WHERE brand_id = '$goods_id' LIMIT 1");
        }
        
        $shop_price   = $goods['shop_price'];
        $linked_goods = get_linked_goods($goods_id);
        
        $goods['goods_style_name'] = add_style($goods['goods_name'], $goods['goods_name_style']);
        
        /* 购买该商品可以得到多少钱的红包 */
        if ($goods['bonus_type_id'] > 0)
        {
            $time = gmtime();
            $sql = "SELECT type_money FROM " . $ecs->table('bonus_type') .
                    " WHERE type_id = '$goods[bonus_type_id]' " .
                    " AND send_type = '" . SEND_BY_GOODS . "' " .
                    " AND send_start_date <= '$time'" .
                    " AND send_end_date >= '$time'";
            $goods['bonus_money'] = floatval($db->getOne($sql));
            if ($goods['bonus_money'] > 0)
            {
                $goods['bonus_money'] = price_format($goods['bonus_money']);
            }
        }

        $smarty->assign('goods',              $goods);
        $smarty->assign('goods_id',           $goods['goods_id']);
        $smarty->assign('promote_end_time',   $goods['gmt_end_time']);
		//$smarty->assign('showprice',          $goods['gmt_end_time']==0? normal:0);
		$smarty->assign('endtime',            ($goods['gmt_end_time']-gmtime())*1000);
		
        $smarty->assign('categories',         get_categories_tree($goods['cat_id']));  // 分类树
        
        /* meta */
        $smarty->assign('keywords',           htmlspecialchars($goods['keywords']));
        $smarty->assign('description',        htmlspecialchars($goods['goods_brief']));
        $catlist = array();
        foreach(get_parent_cats($goods['cat_id']) as $k=>$v)
        {
            $catlist[] = $v['cat_id'];
        }

        $feed = $feedController->goods_insert_code($goods);
        $smarty->assign('stats_code_insert', $feed['stats_code_insert']);
        $smarty->assign('extra_insert_code', $feed['extra_insert_code']);
        assign_template('c', $catlist, 'goods');
        
        // /* 上一个商品下一个商品 */
        // $prev_gid = $db->getOne("SELECT goods_id FROM " .$ecs->table('goods'). " WHERE cat_id=" . $goods['cat_id'] . " AND goods_id > " . $goods['goods_id'] . " AND is_on_sale = 1 AND is_alone_sale = 1 AND is_delete = 0 LIMIT 1");
        // if (!empty($prev_gid))
        // {
        //     $prev_good['url'] = build_uri('goods', array('gid' => $prev_gid), $goods['goods_name']);
        //     $smarty->assign('prev_good', $prev_good);//上一个商品
        // }
        //
        // $next_gid = $db->getOne("SELECT max(goods_id) FROM " . $ecs->table('goods') . " WHERE cat_id=".$goods['cat_id']." AND goods_id < ".$goods['goods_id'] . " AND is_on_sale = 1 AND is_alone_sale = 1 AND is_delete = 0");
        // if (!empty($next_gid))
        // {
        //     $next_good['url'] = build_uri('goods', array('gid' => $next_gid), $goods['goods_name']);
        //     $smarty->assign('next_good', $next_good);//下一个商品
        // }
        
        $position = assign_ur_here($goods['cat_id'], $goods['goods_name']);
        
        /* current position */
        $smarty->assign('page_title',          $position['title']);                    // 页面标题
        $smarty->assign('ur_here',             $position['ur_here']);                  // 当前位置
        $CategoryController = new Yoho\cms\Controller\CategoryController();
        $t1 = $CategoryController->getTopCategories(0);
        $smarty->assign('t1',              $t1);
		
        $properties = get_goods_properties($goods_id);  // 获得商品的规格和属性
        
        $smarty->assign('properties',          $properties['pro']);                              // 商品属性
        $smarty->assign('specification',       $properties['spe']);                              // 商品规格
        $smarty->assign('attribute_linked',    get_same_attribute_goods($properties));           // 相同属性的关联商品
        $smarty->assign('related_goods',       $linked_goods);                                   // 关联商品
        //$smarty->assign('goods_article_list',  get_linked_articles($goods_id));                  // 关联文章
        $smarty->assign('fittings',            get_goods_fittings(array($goods_id)));                   // 配件
        $smarty->assign('rank_prices',         get_user_rank_prices($goods_id, $shop_price));    // 会员等级价格
        $smarty->assign('pictures',            get_goods_gallery($goods_id));                    // 商品相册
        //var_dump(get_goods_gallery($goods_id));
        /* disabled by Anthony: For 11/11 flashdeal. */
        // $smarty->assign('bought_goods',        get_also_bought($goods_id));                      // 购买了该商品的用户还购买了哪些商品
        // $smarty->assign('goods_rank',          get_goods_rank($goods_id));                       // 商品的销售排名 
	    // $smarty->assign('brand_hot',           category_get_goods_all(0,$goods['brand_id'],0,0,'shownum','desc',0,4,0)); // 热销品牌排行榜
	    // $smarty->assign('sell_new',            category_get_goods_all(0,0,0,0,'rand()','desc',0,3,0,'and is_new=1'));  // 新品随机推荐
		// $smarty->assign('sell_hot',            category_get_goods_all(0,0,0,0,'shownum','desc',0,4,0));  //总站热销排行榜
        
        // reviewable_orders for cache


        // 关联文章
        // $smarty->assign('goods_article_list',  get_linked_articles($goods_id));

        //brand-logo
        if (isset($goods['brand_id']) && $goods['brand_id'] > 0) {
            $sql = "SELECT brand_logo, brand_id, brand_name, brand_banner, bperma FROM " . $ecs->table('brand') . " b " .
                    "LEFT JOIN (SELECT id, table_name, lang, perma_link as bperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") bpl ON bpl.id = b.brand_id  AND bpl.table_name = 'brand' AND bpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
                    " WHERE brand_id = '" . $goods['brand_id'] . "'" .
                    "LIMIT 1";
            $brand = $db->getRow($sql);
            $brand_url = build_uri('brand', array('cid' => 0, 'bid' => $brand['brand_id'], 'bperma' => $brand['bperma']), $brand['brand_name']);
            if ($brand['brand_logo']) {
                $brand_logo = '/' . DATA_DIR . '/brandlogo/' . $brand['brand_logo'];
                $smarty->assign('brand_logo', $brand_logo);
            }
            if($brand['brand_banner']) {
                $brand_banner = '/' . DATA_DIR . '/brandbanner/' . $brand['brand_banner'];
                $smarty->assign('brand_banner', $brand_banner);
            }
            $smarty->assign('brand_name', $brand['brand_name']);
            $smarty->assign('brand_url', $brand_url);
        }

        //获取tag
        $tagController = new Yoho\cms\Controller\TagController();
        $tag_array = $tagController->get_tags_by_goods($goods_id);
        $smarty->assign('tags', $tag_array);
        

        $smarty->assign('goods',              $goods);
        if ($is_flashdeal_today) {
            $smarty->assign('insert_reviewable_orders', insert_reviewable_orders());
            
            $comment_arr['name']  = 'comments';
            $comment_arr['id']  = $goods_id;
            $comment_arr['type']  = 0 ;
            $comment_arr['lei'] = 1;
            $smarty->assign('insert_comments_goods', insert_comments($comment_arr));
            
            $goodspromote_arr['name'] = 'goodspromote';
            $goodspromote_arr['goods_id'] = $goods_id ;
            $goodspromote_arr['return_output'] = false ;
            insert_goodspromote($goodspromote_arr);

            $flash_widget_arr['name'] = 'flashdeal_widget';
            $flash_widget_arr['id'] = $goods_id ;
            $flash_widget_arr['type'] = 'goods' ;
            $flash_widget_arr['return_output'] = false ;
            insert_flashdeal_widget($flash_widget_arr);
        }
    }
} 


/* 记录浏览历史 */
if (!empty($_COOKIE['ECS']['history']))
{
    $history = explode(',', $_COOKIE['ECS']['history']);

    array_unshift($history, $goods_id);
    $history = array_unique($history);

    while (count($history) > $_CFG['history_number'])
    {
        array_pop($history);
    }

    setcookie('ECS[history]', implode(',', $history), gmtime() + 3600 * 24 * 30, $cookie_path, $cookie_domain);
}
else
{
    setcookie('ECS[history]', $goods_id, gmtime() + 3600 * 24 * 30, $cookie_path, $cookie_domain);
}

$smarty->assign('catlist',  $catlist);           // set parent categorey list


/* 更新点击次数 */
$db->query('UPDATE ' . $ecs->table('goods') . " SET click_count = click_count + 1 WHERE goods_id = '$_REQUEST[id]'");

$smarty->assign('page_lang',  $GLOBALS['_CFG']['lang']);
$smarty->assign('now_time',  gmtime());           // 当前系统时间
//$VipController = new Yoho\cms\Controller\VipController();

$smarty->assign('vip',       $VipController->get_goods_page_vip_info());
$smarty->display('goods.dwt', $cache_id);

/*------------------------------------------------------ */
//-- PRIVATE FUNCTION
/*------------------------------------------------------ */

/**
 * 获得指定商品的各会员等级对应的价格
 *
 * @access  public
 * @param   integer     $goods_id
 * @return  array
 */
function get_user_rank_prices($goods_id, $shop_price)
{
    $sql = "SELECT rank_id, IFNULL(mp.user_price, r.discount * $shop_price / 100) AS price, r.rank_name, r.discount " .
            'FROM ' . $GLOBALS['ecs']->table('user_rank') . ' AS r ' .
            'LEFT JOIN ' . $GLOBALS['ecs']->table('member_price') . " AS mp ".
                "ON mp.goods_id = '$goods_id' AND mp.user_rank = r.rank_id " .
            "WHERE r.show_price = 1 OR r.rank_id = '$_SESSION[user_rank]'";
    $res = $GLOBALS['db']->query($sql);

    $arr = array();
    while ($row = $GLOBALS['db']->fetchRow($res))
    {

        $arr[$row['rank_id']] = array(
                        'rank_name' => htmlspecialchars($row['rank_name']),
                        'price'     => price_format($row['price']));
    }

    return $arr;
}

/**
 * 获得指定商品的销售排名
 *
 * @access  public
 * @param   integer     $goods_id
 * @return  integer
 */
function get_goods_rank($goods_id)
{
    /* 统计时间段 */
    $period = intval($GLOBALS['_CFG']['top10_time']);
    if ($period == 1) // 一年
    {
        $ext = " AND o.add_time > '" . local_strtotime('-1 years') . "'";
    }
    elseif ($period == 2) // 半年
    {
        $ext = " AND o.add_time > '" . local_strtotime('-6 months') . "'";
    }
    elseif ($period == 3) // 三个月
    {
        $ext = " AND o.add_time > '" . local_strtotime('-3 months') . "'";
    }
    elseif ($period == 4) // 一个月
    {
        $ext = " AND o.add_time > '" . local_strtotime('-1 months') . "'";
    }
    else
    {
        $ext = '';
    }

    /* 查询该商品销量 */
    $sql = 'SELECT IFNULL(SUM(g.goods_number), 0) ' .
        'FROM ' . $GLOBALS['ecs']->table('order_info') . ' AS o, ' .
            $GLOBALS['ecs']->table('order_goods') . ' AS g ' .
        "WHERE o.order_id = g.order_id " .
        "AND o.order_status = '" . OS_CONFIRMED . "' " .
        "AND o.shipping_status " . db_create_in(array(SS_SHIPPED, SS_RECEIVED)) .
        " AND o.pay_status " . db_create_in(array(PS_PAYED, PS_PAYING)) .
        " AND g.goods_id = '$goods_id'" . $ext;
    $sales_count = $GLOBALS['db']->getOne($sql);

    if ($sales_count > 0)
    {
        /* 只有在商品销售量大于0时才去计算该商品的排行 */
        $sql = 'SELECT DISTINCT SUM(goods_number) AS num ' .
                'FROM ' . $GLOBALS['ecs']->table('order_info') . ' AS o, ' .
                    $GLOBALS['ecs']->table('order_goods') . ' AS g ' .
                "WHERE o.order_id = g.order_id " .
                "AND o.order_status = '" . OS_CONFIRMED . "' " .
                "AND o.shipping_status " . db_create_in(array(SS_SHIPPED, SS_RECEIVED)) .
                " AND o.pay_status " . db_create_in(array(PS_PAYED, PS_PAYING)) . $ext .
                " GROUP BY g.goods_id HAVING num > $sales_count";
        $res = $GLOBALS['db']->query($sql);

        $rank = $GLOBALS['db']->num_rows($res) + 1;

        if ($rank > 10)
        {
            $rank = 0;
        }
    }
    else
    {
        $rank = 0;
    }

    return $rank;
}

/**
 * 获得商品选定的属性的附加总价格
 *
 * @param   integer     $goods_id
 * @param   array       $attr
 *
 * @return  void
 */
function get_attr_amount($goods_id, $attr)
{
    $sql = "SELECT SUM(attr_price) FROM " . $GLOBALS['ecs']->table('goods_attr') .
        " WHERE goods_id='$goods_id' AND " . db_create_in($attr, 'goods_attr_id');

    return $GLOBALS['db']->getOne($sql);
}

?>
