<?php
/**
* YOHO Lucky Draw page
* 20180731
*/
define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

if ((DEBUG_MODE & 2) != 2) {
    $smarty->caching = true;
}

$luckyDrawController = new Yoho\cms\Controller\LuckyDrawController();

$lucky_draw_active = $luckyDrawController->getActiveLuckyDrawEvent();

if (empty($lucky_draw_active)) {
	header('Location: /luckydraw');
	exit; 
}

$lucky_draw_id = $lucky_draw_active[0];

$lucky_draw_info = $luckyDrawController->getLuckyDrawEventInfo($lucky_draw_id);


/* 赋值固定内容 */
assign_template();
$position = assign_ur_here(0);

$smarty->assign('page_title', $position['title']);    // 页面标题
$smarty->assign('ur_here',    $position['ur_here']);  // 当前位置
$categoryController = new Yoho\cms\Controller\CategoryController();
$t1 = $categoryController->getTopCategories(0);
$smarty->assign('t1',              $t1);
$smarty->assign('helps', get_shop_help());              // 网店帮助
$smarty->assign('lucky_draw_info', $lucky_draw_info);
 
$smarty->display('lucky_draw_tnc.dwt');
