<?php
/**
* YOHO VIP page
* Anthony@YOHO 20170720
*/
define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

if ((DEBUG_MODE & 2) != 2) {
    $smarty->caching = true;
}

$user_id = $_SESSION['user_id'];
$user_rank = $_SESSION['user_rank'];
$rank_points = $_SESSION['rank_points'];
$luckyDrawController = new Yoho\cms\Controller\LuckyDrawController();
$lucky_draw_active = $luckyDrawController->getActiveLuckyDrawEvent();

if (!empty($_REQUEST['action']) && $_REQUEST['action'] == 'lucky_draw_open') {
    $ticket_id = $_REQUEST['play_id'];
    // get the prize (gift id)
    $result = $luckyDrawController->getUserLuckyDrawOpen($ticket_id,$user_id);
    
    if (isset($result['error'])) {
        echo json_encode($result);
    } else {
        echo json_encode(array('data' => $result) );
    }
    exit;
}

if (empty($lucky_draw_active)) {
	header('Location: /luckydraw');
	exit; 
}

$lucky_draw_id = $lucky_draw_active[0];
$user_ticket_count = $luckyDrawController->getDrawTicketCount($user_id,$lucky_draw_id);
$is_gift_enough = $luckyDrawController->checkGiftEnough($lucky_draw_id);
$is_hold_lucky_draw = $luckyDrawController->isHoldLuckyDraw($lucky_draw_id);

if (empty($lucky_draw_id)) {
    // redirect
   	header('Location: /luckydraw');
   	exit;
}

if ($user_ticket_count == 0){
    // redirect
   	header('Location: /luckydraw');
   	exit;
}

if ($is_gift_enough == 0 || $is_hold_lucky_draw == 1){
    // redirect
   	header('Location: /luckydraw');
   	exit;
}


$ticket_id = $luckyDrawController->getUserLuckyDrawTicket($user_id,$lucky_draw_id);

if (empty($ticket_id)) {
    // redirect
   	header('Location: /luckydraw');
   	exit;
}

$draw_results = $luckyDrawController->drawStart($ticket_id);
if (empty($draw_results)) {
    // redirect
   	header('Location: /luckydraw');
   	exit;
}
$draw_results_detail = $luckyDrawController->getDrawResultsDetail($draw_results);
$lucky_draw_info = $luckyDrawController->getLuckyDrawEventInfo($lucky_draw_id);
$draw_index = [1,2,3,8,4,7,6,5];
foreach ($draw_results_detail as $key => &$detail) {
    $detail['index'] = $draw_index[$key];
}

/* 赋值固定内容 */
assign_template();
$position = assign_ur_here(0);
$smarty->assign('page_title', $position['title']);    // 页面标题
$smarty->assign('ur_here',    $position['ur_here']);  // 当前位置
$smarty->assign('helps', get_shop_help());              // 网店帮助
$smarty->assign('user_ticket_count', $user_ticket_count);
$smarty->assign('draw_results', $draw_results);
$smarty->assign('draw_results_detail', $draw_results_detail);
$smarty->assign('ticket_id', $ticket_id);
$smarty->assign('lucky_draw_info', $lucky_draw_info);


$smarty->display('lucky_draw_play.dwt');
