<?php

/**
 * ECSHOP Q&A
 * $Id: support.php
*/


define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

if ((DEBUG_MODE & 2) != 2)
{
    $smarty->caching = true;
}

/*------------------------------------------------------ */
//-- INPUT
/*------------------------------------------------------ */
if($_REQUEST['chatbot']) {
    if(!$_COOKIE["support_vote_".intval($_REQUEST['article_id'])]) {
        $set = ($_REQUEST['is_useless']) ? "is_useless = is_useless + 1" : "is_helpful = is_helpful + 1";
        $db->query("UPDATE ".$ecs->table('article')."SET ".$set." WHERE article_id = ".intval($_REQUEST['article_id']), SILENT);
        setcookie("support_vote_".intval($_REQUEST['article_id']), "1", time()+82600);
    }
    return true;
    /*
    if($_REQUEST['is_useless']) {
        $position = assign_ur_here();
        $smarty->assign('data',              $_REQUEST);
        $smarty->assign('page_title',           $position['title'] . '- Chatbot');     // 页面标题
        $smarty->assign('ur_here',              $position['ur_here']);   // 当前位置
        $smarty->display('support_chatbot.dwt', $cache_id);
        exit();
    } elseif($_REQUEST['is_helpful']) {
        return true;
    }
    */
}

/* 获得指定的分类ID */
if (!empty($_GET['id']))
{
    $id = intval($_GET['id']);
}
if (!empty($_GET['cat_id']))
{
    $cat_id = intval($_GET['cat_id']);
}
elseif (!empty($_GET['category']))
{
    $cat_id = intval($_GET['category']);
}
/* 获得当前页码 */
$page   = !empty($_REQUEST['page'])  && intval($_REQUEST['page'])  > 0 ? intval($_REQUEST['page'])  : 1;

/*------------------------------------------------------ */
//-- PROCESSOR
/*------------------------------------------------------ */

/* 获得页面的缓存ID */
$cache_id = sprintf('%X', crc32('support-'.$cat_id . '-'.$id . '-' . $page . '-' . $_CFG['lang'].'-'.trim($_REQUEST['keywords'])));

/* 如果页面没有被缓存则重新获得页面的内容 */
if (!$smarty->is_cached('support_cat.dwt', $cache_id))
{
    $where = "ac.cat_id = '" . $cat_id . "'";
    if(empty($cat_id))$where = "ac.parent_id = 0 ";
    $sql = "SELECT IFNULL(acl.cat_name, ac.cat_name) as cat_name, " .
            "IFNULL(acl.keywords, ac.keywords) as keywords, " .
            "IFNULL(acl.cat_desc, ac.cat_desc) as cat_desc, ac.cat_id " .
            "FROM " . $ecs->table('article_cat') . " as ac " .
            "LEFT JOIN " . $ecs->table('article_cat_lang') . " as acl ON acl.cat_id = ac.cat_id AND acl.lang = '" . $_CFG['lang'] . "' " .
            "WHERE $where AND cat_type IN(".SUPPORT_CAT.")";
    $article_cat = $db->getRow($sql);
    if($id > 0 && empty($_REQUEST['keywords'])) {
        /* 文章详情 */
        $article = get_article_info($id);
        $cat_id = $article['cat_id'];
        $smarty->assign('article',              $article);
    }
    if ($article_cat === false || empty($article_cat))
    {
        /* 如果没有找到任何记录则返回首页 */
        header('Location: /');
        exit;
    }
    if (empty($cat_id) && empty($article)) {
        $index = 1;
        $cat_id = $article_cat['cat_id'];
    }

    assign_template('a', array($cat_id));
    $position = assign_ur_here($cat_id);
    $smarty->assign('page_title',           $position['title']);     // 页面标题
    $smarty->assign('ur_here',              $position['ur_here']);   // 当前位置
    $smarty->assign('helps',                get_shop_help());        // 网店帮助
    $CategoryController = new Yoho\cms\Controller\CategoryController();
    $t1 = $CategoryController->getTopCategories(0);
    $smarty->assign('t1',              $t1);
    $parent_id = $db->getOne('SELECT cat_id FROM '.$ecs->table('article_cat')." WHERE parent_id = 0 AND cat_type IN(".SUPPORT_CAT.")");
    $smarty->assign('article_categories',   article_categories_tree($parent_id)); //文章分类树
    $smarty->assign('cat_id',               $cat_id);
    $smarty->assign('cat_name',             $article_cat['cat_name']);
    $smarty->assign('keywords',             htmlspecialchars($article_cat['keywords']));
    $smarty->assign('description',          htmlspecialchars($article_cat['cat_desc']));
    $smarty->assign('hot_lists',            get_hot_article($cat_id, 0, 1)); // Hot articles list
    // $smarty->assign('feed_url',             ($_CFG['rewrite'] == 1) ? "feed-typearticle_cat" . $cat_id . ".xml" : 'feed.php?type=article_cat' . $cat_id); // RSS URL

    /* 获得文章总数 */
    $size   = isset($_CFG['article_page_size']) && intval($_CFG['article_page_size']) > 0 ? intval($_CFG['article_page_size']) : 20;
    $count  = get_article_count($cat_id);
    $pages  = ($count > 0) ? ceil($count / $size) : 1;

    if ($page > $pages)
    {
        $page = $pages;
    }
    $pager['search']['id'] = $cat_id;
    $keywords = '';
    $goon_keywords = ''; //继续传递的搜索关键词

    /* 获得文章列表 */
    if (isset($_REQUEST['keywords']))
    {
        $smarty->assign('keywords', $_REQUEST['keywords']);
        $keywords = addslashes(htmlspecialchars(urldecode(trim($_REQUEST['keywords']))));
        $pager['search']['keywords'] = $keywords;
        $search_url = substr(strrchr($_POST['cur_url'], '/'), 1);

        $smarty->assign('search_value',    stripslashes(stripslashes($keywords)));
        $smarty->assign('search_url',       $search_url);
        $count  = get_article_count($cat_id, $keywords);
        $pages  = ($count > 0) ? ceil($count / $size) : 1;
        if ($page > $pages)
        {
            $page = $pages;
        }

        $goon_keywords = urlencode($_REQUEST['keywords']);
    }

    if(isset($index) && empty($keywords)) {
        $article_list = [];
        $smarty->assign('index_col', 1);
    } elseif(empty($article)) {
        $article_list = get_cat_articles($cat_id, $page, $size ,$keywords);
        foreach($article_list as $key => $article) {
            $article_list[$key]['url'] = '/support/answer/'.$article['id'].'-'.$article['title'];
        }
        $smarty->assign('article_list', $article_list);
    }
    /* 分页 */
    assign_pager('support_cat', $cat_id, $count, $size, '', '', $page, $goon_keywords);
    assign_dynamic('support_cat');
}

$smarty->display('support_cat.dwt', $cache_id);

?>