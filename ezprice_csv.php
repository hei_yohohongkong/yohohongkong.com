<?php

/***
 * ezprice_csv.php
 * by howang 2015-09-01
 *
 * PHP Script for generating CSV for EZprice
 ***/

if (extension_loaded('newrelic'))
{
	newrelic_background_job(true);
}

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

ini_set('memory_limit', '512M');

$cache_name = 'ezprice_csv';
$data = read_static_cache($cache_name);

if (empty($data))
{
	$site_url = $ecs->url();
	$site_url_no_slash = substr($site_url, 0, -1);

	$fields = array('商品編號','品牌','商品名稱','型號','商品類型','商品價格','購買網址','運送方式','付款方式','小圖檔位置','大圖檔位置','分類代號|分類名稱','國際條碼','規格 or 說明');

	$sql = "SELECT g.goods_id, b.brand_name, g.goods_name, c.cat_name, g.shop_price, g.goods_thumb, g.goods_img, g.cat_id, g.goods_sn, g.goods_desc " .
	        "FROM " . $ecs->table('goods') . " as g " .
	        "LEFT JOIN " . $ecs->table('category') . " as c ON c.cat_id = g.cat_id " .
	        "LEFT JOIN " . $ecs->table('brand') . " as b ON b.brand_id = g.brand_id " .
	        "WHERE g.is_delete = 0 AND g.is_hidden = 0 AND g.is_real = 1 AND g.is_on_sale = 1 " .
	        "GROUP BY g.goods_id";
	$res = $db->getAll($sql);
	foreach ($res as $key => $row)
	{
		$res[$key]['shop_price'] = price_format($row['shop_price']);
	    $res[$key]['goods_url'] = $site_url_no_slash . build_uri('goods', array('gid' => $row['goods_id'], 'gname' => $row['goods_name']));
	    $res[$key]['goods_thumb'] = $site_url . get_image_path($row['goods_id'], $row['goods_thumb'], true);
	    $res[$key]['goods_img'] = $site_url . get_image_path($row['goods_id'], $row['goods_img']);
	    $res[$key]['cat_id'] = get_ezprice_category($row['cat_id']);
	}
	
	$data = generate_csv_string($fields);
	foreach ($res as $row)
	{
	    $csv_row = array(
	        $row['goods_id'], // 商品編號
	        $row['brand_name'], // 品牌
	        $row['goods_name'], // 商品名稱
	        '', // 型號
	        $row['cat_name'], // 商品類型
	        $row['shop_price'], // 商品價格
	        $row['goods_url'], // 購買網址
	        '宅配', // 運送方式
	        '信用卡', // 付款方式
	        $row['goods_thumb'], // 小圖檔位置
	        $row['goods_img'], // 大圖檔位置
	        $row['cat_id'], // 分類代號
	        $row['goods_sn'], // 國際條碼
	        $row['goods_desc'], // 規格 or 說明
	    );
	    $data .= generate_csv_string($csv_row);
	}
	
	write_static_cache($cache_name, $data);
}

header('Content-Type: text/csv;charset=utf-8');
header('Content-Disposition: attachment;filename=ezprice.csv');
echo $data;
exit;

// Modified from http://php.net/manual/en/function.fputcsv.php#77866
function generate_csv_string($fields = array(), $delimiter = ',', $enclosure = '"')
{
    $str = '';
    $escape_char = '\\';
    foreach ($fields as $value) {
        if (strpos($value, $delimiter) !== false ||
        strpos($value, $enclosure) !== false ||
        strpos($value, "\n") !== false ||
        strpos($value, "\r") !== false ||
        strpos($value, "\t") !== false ||
        strpos($value, ' ') !== false) {
            $str2 = $enclosure;
            $escaped = 0;
            $len = strlen($value);
            for ($i=0;$i<$len;$i++) {
                if ($value[$i] == $escape_char) {
                    $escaped = 1;
                } else if (!$escaped && $value[$i] == $enclosure) {
                    $str2 .= $enclosure;
                } else {
                    $escaped = 0;
                }
                $str2 .= $value[$i];
            }
            $str2 .= $enclosure;
            $str .= $str2.$delimiter;
        } else {
            $str .= $value.$delimiter;
        }
    }
    $str = substr($str,0,strlen($delimiter)*-1);
    $str .= "\n";
    return $str;
}

function get_ezprice_category($yoho_cat_id)
{
    // 流動產品 and its sub-categories
    if (in_array($yoho_cat_id, array(1,142,163,6,186,81,19,188,202,211,187,164,213,209,246,156,2)))
    {
        return '3|手機/相機/DV及週邊';
    }
    // 美容及護理 and its sub-categories
    elseif (in_array($yoho_cat_id, array(23,39,206,43,157,178,145,143,41,144,198,40,177,199,207,182,176,244,280,158)))
    {
        return '6|美妝保養';
    }
    // 穿戴式裝置 and its sub-categories
    elseif (in_array($yoho_cat_id, array(171,172,135,205,137,180,204,161)))
    {
        return '14|男女服飾';
    }
    // 電腦文儀 and its sub-categories
    elseif (in_array($yoho_cat_id, array(44,50,55,240,51,159,151,160,153,250)))
    {
        return '1|3C電腦/網路及週邊';
    }
    // 數碼影像 and its sub-categories
    elseif (in_array($yoho_cat_id, array(230,231,232,233,234,235,237,107,167,170)))
    {
        return '3|手機/相機/DV及週邊';
    }
    // 電視影音 and its sub-categories
    elseif (in_array($yoho_cat_id, array(255,260,257,102,256,169,258,259)))
    {
        return '2|家電/視聽/電玩';
    }
    // 家庭電器 and its sub-categories
    elseif (in_array($yoho_cat_id, array(184,185,189,225,166,226,140,252,210,261,275,278,281,282,133,224,223,208,201,192,190,283)))
    {
        return '2|家電/視聽/電玩';
    }
    // 汽車用品 and its sub-categories
    elseif (in_array($yoho_cat_id, array(110,132,174,175,214,277)))
    {
        return '4|汽車機車及相關部品';
    }
    
    // If category didn't match, return a default value
    return '1|3C電腦/網路及週邊';
}

?>