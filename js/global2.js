//延迟执行代码的插件，主要包括延迟，阻止重复执行
(function($){var a={},c="doTimeout",d=Array.prototype.slice;$[c]=function(){return b.apply(window,[0].concat(d.call(arguments)))};$.fn[c]=function(){var f=d.call(arguments),e=b.apply(this,[c+f[0]].concat(f));return typeof f[0]==="number"||typeof f[1]==="number"?this:e};function b(l){var m=this,h,k={},g=l?$.fn:$,n=arguments,i=4,f=n[1],j=n[2],p=n[3];if(typeof f!=="string"){i--;f=l=0;j=n[1];p=n[2]}if(l){h=m.eq(0);h.data(l,k=h.data(l)||{})}else{if(f){k=a[f]||(a[f]={})}}k.id&&clearTimeout(k.id);delete k.id;function e(){if(l){h.removeData(l)}else{if(f){delete a[f]}}}function o(){k.id=setTimeout(function(){k.fn()},j)}if(p){k.fn=function(q){if(typeof p==="string"){p=g[p]}p.apply(m,d.call(n,i))===true&&!q?o():e()};o()}else{if(k.fn){j===undefined?e():k.fn(j===false);return true}else{e()}}}})(jQuery);
//延迟加载图片
function lazyload(option){var settings={defObj:null,defHeight:-200};settings=$.extend(settings,option||{});var defHeight=settings.defHeight,defObj=(typeof settings.defObj=="object")?settings.defObj.find("img"):$(settings.defObj).find("img");var pageTop=function(){return document.documentElement.clientHeight+Math.max(document.documentElement.scrollTop,document.body.scrollTop)-settings.defHeight;};var imgLoad=function(){defObj.each(function(){if($(this).offset().top<=pageTop()){var original=$(this).attr("original");if(original){$(this).attr("src",original).removeAttr("original").hide().fadeIn();};};});};imgLoad();$(window).bind("scroll",function(){imgLoad();});};
//返回顶部
function back2top(){$(document.body).append("<div id='main_back' style='width:55px; height:87px;position:fixed;bottom:20px;right:16px;display:none;'><a title='QQ咨询' href='http://wpa.qq.com/msgrd?v=3&uin=4006688131&site=qq&menu=yes' target='_blank' id='qq_sercive'></a><a title='返回顶部' id='back2top'></a></div>");$(window).scroll(function(){var $sTop=$(window).scrollTop();var $wHeight=$(window).height();if($sTop>100){if(!window.XMLHttpRequest){$("#main_back").css({'top':$sTop+$wHeight-76,'position':'absolute'});}$("#main_back").fadeIn();}else{$("#main_back").fadeOut();}});$("#back2top").click(function(){$('body,html').animate({scrollTop:0},200);return false;});}
//收藏网站
function AddFavorite(sURL,sTitle){try{window.external.addFavorite(sURL,sTitle);}catch(e){try{window.sidebar.addPanel(sTitle,sURL,"");}catch(e){alert("加入收藏失败，请使用Ctrl+D进行添加");}}}


$(function(){
$("#header div.links").load("/user/loginAuth?"+new Date().getTime(),function(){
	updateCartCount();
});
//导航栏
$('#header .header_bottom li.current').prev('li').addClass('fix');
$('#header .header_bottom li').hover(
	function(){
		$('#header .header_bottom li').removeClass('hover');
		$(this).prev('li').addClass('hover');
	},
	function(){
		$(this).prev('li').removeClass('hover');
	}
);

//页头搜索框
$('#Search_text').focus(function(){
	$(this).addClass('focus');
	if (($.trim($(this).val()) == '输入关键字')||($.trim($(this).val()) == '你喜欢的化妆品？')) $(this).val('');
});
$('#Search_text').blur(function(){
	if ($.trim($(this).val()) == '')
	{
		$(this).removeClass('focus');
		$(this).val('');
	}
});
$('#search_fm').submit(function(){
	if ($.trim($('#search_fm input.text').val()) == '输入关键字') return false;
});
//页头搜索框 end

//延迟加载页面上的图片
lazyload({defObj:"#wrapper"});

//回到页面顶部
back2top();	

//我的购物车弹窗效果
$('.header_top .item3').live('mouseenter mouseleave', function(event) {
	  var $item3 = $(this);	
	  if (event.type == 'mouseenter') {
		  $item3.doTimeout('item3',300,function(){
			$item3.find('.cart_rukou').addClass('cart_rukou_on');
			$item3.find('.mycart_popup').show();  
			$item3.find('.mycart_popup').load('/new_cart.php');
			return false;
			})
	  }else {
		  $item3.doTimeout('item3',300,function(){
			$item3.find('.cart_rukou').removeClass('cart_rukou_on');
			$item3.find('.mycart_popup').hide();
			return false;
			})
	  }
 });

//在线咨询下拉效果
$('#header .kefu').hover(function(){
	var $kefu = $(this); 
	$kefu.doTimeout('kefu',300,function(){
		$kefu.find('.kefu_box').show().animate({height:"66"},300);
		$kefu.find('h4').css('border-right-color','#cccccc');
	})
},function(){
	var $kefu = $(this); 
	$kefu.doTimeout('kefu',300,function(){
		$kefu.find('.kefu_box').animate({height:"0"},300).fadeOut(100);
		$kefu.find('h4').css('border-right-color','#fbfbfb');
	})
})
//在线咨询弹窗
 $('#nala_say').bind("click",function(){
	window.location.href="http://wpa.qq.com/msgrd?v=3&uin=4006688131&site=qq&menu=yes";
 })

//页头我的nala上浮效果
$('#header .links .item2').live('mouseenter mouseleave', function(event) {
	if (event.type == 'mouseenter'){
		$(this).doTimeout('item2',300,function(){
			$('#header .mynala_popup').show(); $('#header .mynala_link').addClass('mynala_link_on');return false;
		})
	}else{
		$(this).doTimeout('item2',300,function(){
			$('#header .mynala_popup').hide(); $('#header .mynala_link').removeClass('mynala_link_on');return false;
		})
	}
});
		
})

//我的NALA订单等表格偶数行添加背景色
$(document).ready(function(){
	  $("div.my_nala_detail table tr:odd").css("background","#f9f9f9");
	  $("div.my_nala_detail table tr").last().css("background","none");
	});
//更新购物车
document.writeln("<iframe height=\'0\' id=\'iframepage\' name=\'iframepage\' width=\'0\'  scrolling=\'auto\' align=\'top\' frameborder=\'0\' src=\'\/new_cart.php\' ><\/iframe>")