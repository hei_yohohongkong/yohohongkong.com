/*
*【b2c】全局JS处理文件
* update by: pannysp@2013-02-21 16:00
*/

//jQuery.easing动画效果插件
jQuery.extend(jQuery.easing,{def:'easeOutQuad',swing:function(x,t,b,c,d){return jQuery.easing[jQuery.easing.def](x,t,b,c,d);},easeInQuad:function(x,t,b,c,d){return c*(t/=d)*t+b;},easeOutQuad:function(x,t,b,c,d){return-c*(t/=d)*(t-2)+b;},easeInExpo:function(x,t,b,c,d){return(t==0)?b:c*Math.pow(2,10*(t/d-1))+b;},easeOutExpo:function(x,t,b,c,d){return(t==d)?b+c:c*(-Math.pow(2,-10*t/d)+1)+b;},easeInBack:function(x,t,b,c,d,s){if(s==undefined)s=1.70158;return c*(t/=d)*t*((s+1)*t-s)+b;},easeOutBack:function(x,t,b,c,d,s){if(s==undefined)s=1.70158;return c*((t=t/d-1)*t*((s+1)*t+s)+1)+b;},easeInOutBack:function(x,t,b,c,d,s){if(s==undefined)s=1.70158;if((t/=d/2)<1)return c/2*(t*t*(((s*=(1.525))+1)*t-s))+b;return c/2*((t-=2)*t*(((s*=(1.525))+1)*t+s)+2)+b;}});

var NALA = NALA || {};
(function($){
NALA.check = {
    //IE6判定
    isIE6: window.VBArray && !window.XMLHttpRequest,
    //判断是否为中英文，数字，下划线，减号
    isNick: function(str) {
        var nickReg = /^[\u4e00-\u9fa5A-Za-z0-9-_]+$/;
        return nickReg.test(str);
    },
    //判断邮箱
    isEmail: function(str) {
        var emailReg = /^[a-z0-9]\w*@([a-z0-9][a-z0-9-]*\.)+[a-z]{2,5}$/i;
        return emailReg.test(str);
    },
    //判断URL
    isUrl: function(str) {
        var urlReg = /^http:\/\/([\w-]+\.)+[\w-]+(\/[\w-.\/?%&=]*)?$/;
        return urlReg.test(str);
    },
    //判断数字
    isNum: function(str) {
        var numReg = /^[0-9]+$/;
        return numReg.test(str);
    }
},




//全局弹窗
NALA.dialog = {
    close: function (id) {
        var i,list = $.dialog.list;
        if(id){
            list[id].close();
        }else{
            for (i in list) {
                list[i].close();
            }
        }
    },
    creat: function (flag,html,options) {
        var dialog = null;
        options = $.extend({
            fixed:true,
            title:false,
            lock:false,
            id: 'dg_'+flag,
            init: function() {},
            content: '<div class="dg_'+flag+'">'+html+'</div>'
        },options);
        
        dialog=$.dialog(options);
        return dialog;
    },
    success: function (html,time) {
        var dialog = null;
        time = time || 3;
        this.close();
        html = '<h4 class="ico ok_ico">'+html+'</h4><div class="tocheck">&nbsp;</div>'
        dialog = this.creat('success',html);
        dialog.time(time);
    },
    warn: function (html){
        var dialog = null;
        html = '<h4 class="ico warn_ico">'+html+'</h4><div class="tocheck cle"><a class="btn btn2" onclick="NALA.dialog.close(\'dg_warn\')" href="javascript:;">&nbsp;&nbsp;确&nbsp;定&nbsp;&nbsp;</a></div>';
        dialog = this.creat('warn',html);
        dialog.lock();
    },
    confirm: function (html,callback) {
        var dialog = null;
        dialog = this.creat('confirm',html);
        dialog.button({
            name: '确定',
            focus: true,
            callback: callback
        },{
            name: '取消'
        });
    },
    addCart: function(id, itemNumber) {
        var html, dialog = null;
        $.ajax({
            url: '/cart/ajaxBuy',
            // url: 'ajaxBuy.html',
            type:'post',
            cache: false,
            data:{'id':id,'itemNumber':itemNumber},
            dataType:'json',
            success: function(data){
                if(data.status == 0){
                    html = '<h5 class="ico warn_ico">'+data.text+'</h5>';
                    dialog = NALA.dialog.creat('addcart',html);
                    dialog.time(3);
                }else if(data.status == 1){
                    html = '<h4 class="ico ok_ico">成功加入购物车</h4>'+
                        '<p>购物车共有<span class="red">'+data.count+'</span>件商品，合计:<span class="totalprice">￥'+(data.totalPrice*100).toFixed(0)+'</span></p>'+
                        '<div class="tocheck cle"><a class="btn" onclick="NALA.dialog.close(\'dg_addcart\')" href="javascript:;">再逛逛</a><a class="toother" href="/cart/mycart">去购物车结算&gt;&gt;</a></div>';
                    dialog = NALA.dialog.creat('addcart',html);
                }
            },
            error: function(xhr){
                NALA.dialog.warn("服务器忙，请稍后再试。("+xhr.status+")");
            }
        });
    },
    showLogin:function(){
        var _login_box = null,
        html = '<div class="login_dialog" id="login_dialog"><ul>'+
                '<li><span>用户名：</span><input type="text" name="j_username" class="text"></li>'+
                '<li><span>密&nbsp;&nbsp;码：</span><input type="password" name="j_password" class="text"><a href="/user/alertMisPwd" class="gray">忘记密码？</a></li>'+
                '<li><a href="javascript:;" class="btn">登 录</a><a href="/user/signup" class="gray">快速注册</a></li>'+
            '</ul><div class="other">'+
            '<p class="gray">你也可以通过我们战略合作伙伴账号进行登录！</p>'+
            '<p><a title="用微博账户登录" class="sina" href="https://api.weibo.com/oauth2/authorize?client_id=1244150409&amp;response_type=code&amp;redirect_uri=http://www.nala.com.cn/user/sinaLogin"></a>'+
            '<a title="用QQ账户登录" class="qq" href="https://graph.qq.com/oauth2.0/authorize?response_type=code&amp;client_id=100224827&amp;state=1&amp;redirect_uri=www.nala.com.cn/user/qqLoginCallback"></a>'+
            '<a title="使用淘宝帐号登录" class="taobao" href="http://www.nala.com.cn/user/taobaoLogin"></a>'+
            '<a title="使用百度登录" class="baidu" href="http://www.nala.com.cn/user/baiduLogin?login=baidu"></a></p></div></div>';

        _login_box = $.dialog({
            title: '请登录NALA商城',
            lock: true,
            padding:"20px 35px",
            id: "login",
            content:html,
            init: function() {
                var $loginbox = $("#login_dialog"),
                $name = $loginbox.find('input[name=j_username]'),
                $pwd = $loginbox.find('input[name=j_password]');
                $loginbox.on('click','a.btn',function() {
                    var _name = $.trim($name.val()), text,
                    _pwd = $.trim($pwd.val());
                    if(_name == '' || _pwd == ''){
                        text = (_name == '') ? '请输入用户名':'请输入密码';
                        NALA.dialog.warn(text);
                        return false;
                    }
                    var params = {
                        'j_username': _name,
                        'j_password': _pwd,
                        '_spring_security_remember_me':'on'
                    }
                    $.ajax({
                        url:"/j_spring_security_check",
                        type:'post',
                        data:params,
                        dataType:'json',
                        success: function(data){            
                            if(data.success){
                                location.reload();
                            }else{
                                NALA.dialog.warn('您输入的密码和用户名不匹配！')
                            }
                        } 
                    });
                });
            }
        });
    }
},

NALA.common = {

    // 获取屏幕右侧悬浮区域位置
    getWinRight: function() {
        return Math.floor(($(window).width()-1008)/2-94);
    },

    // 悬浮定位方法
    fixed: function($elem, fixedTop) {
        var fixedTop = fixedTop || 0;
        $elem.each(function() {
            var $this = $(this),
            _oTop = $this.offset().top;

            if(fixedTop == 0 && NALA.check.isIE6){
                fixedTop -= _oTop;
            }
            
            $(window).on("scroll", function () {
                var _sTop = $(this).scrollTop();
                if (_sTop > _oTop) {
                    $this.addClass('fixed');
                    if (NALA.check.isIE6) {
                        $this.css({
                            'top' : _sTop + fixedTop,
                            'position' : 'absolute'
                        });
                    }else{ 
                        $this.css({
                            'top' : fixedTop,
                            'position' : 'fixed'
                        });
                    }
                } else {
                    $this.removeClass('fixed');
                    $this[0].style.top = '';
                    $this[0].style.position = '';
                }
            });
        });
    },

    //延迟加载图片
    lazyload: function(elem) {
        var $win = $(window),
        winheight = $win.height(),
        $images = $(elem).find("img"),

        pageTop = function() {
            return winheight + $win.scrollTop() + 50;
        },

        imgLoad = function() {
            $images.each(function () {
                var _this = $(this);
                if (_this.offset().top <= pageTop()) {
                    var trueSrc = _this.attr("original");
                    if (trueSrc) {
                        _this.hide().attr("src", trueSrc).removeAttr("original").fadeIn();
                    }
                }
            });
        };

        imgLoad();
        $win.on("scroll", function () {
            imgLoad();
        });
    },

    //页头搜索框
    searchBar: function(width, showHot) {
        var $s_form = $("#search_fm"),
        $sInput = $s_form.find('.sea_input'),
        $sBox =$s_form.parent('.search_box'),
        showHot = showHot || false,
        _offset = $sBox.offset();

        var $hotkey = $('<dl class="search-hotkey"><dt>热词：</dt><dd><a href="/item/search?content=%E4%BF%9D%E6%B9%BF&amp;dataBi=k1">保湿</a><a href="/item/search?content=BB%E9%9C%9C">BB霜</a><a href="/item/search?content=%E9%9B%85%E8%AF%97%E5%85%B0%E9%BB%9B&amp;dataBi=k1妆">雅诗兰黛</a><a href="/item/search?content=%E5%94%87%E8%86%8F&amp;dataBi=k1">唇膏</a><a href="/item/search?content=%E8%A1%A5%E6%B0%B4&amp;dataBi=k1">补水</a><a href="/item/search?content=%E6%8E%A7%E6%B2%B9&amp;dataBi=k1">控油</a></dd></dl>');
            
        $sInput.val('请输入商品或品牌').focus(function(){
            var $sWords = $.trim($sInput.val());
            $sBox.css({backgroundPosition:"0 100%"});
            if($sWords == '请输入商品或品牌'){
                $sInput.val('');
                if(showHot){
                    $hotkey.css({'left':_offset.left,'top':_offset.top}).appendTo('body');
                    bind_hotKeyHide($hotkey);
                }
            }
        }).blur(function(){
            if($.trim($sInput.val()) == ''){
                $sBox.css({backgroundPosition:"0 0"});
                $sInput.val('请输入商品或品牌');
            }
        });
        $s_form.submit(function(){
            if($.trim($sInput.val()) == '请输入商品或品牌' || $.trim($sInput.val()) == '' ){
                location.href = "/item/search";
                return false;
            }
        });

        // 页头搜索自动补全
        $sInput.autocomplete("/item/associateSearch", { width: width, max: 10, scroll:false, 
            formatItem:function(row) {
                $hotkey.remove();
                return row[0];
            }});

        function bind_hotKeyHide(elem) {
            $(document).on("click",function(e){
                if($(e.target).parents(".search-hotkey").length == 0 && e.target.className != 'search-hotkey' && e.target.id != 'textfield'){
                    elem.remove();
                }
            });
        }
    },

    //返回顶部
    back2top: function () {
        var right= NALA.common.getWinRight(),
        $win = $(window),
        _wHeight = $win.height(),
        $back2top = $('<div class="back2top" style="right:'+right+'px"><a href="http://wpa.qq.com/msgrd?v=3&amp;uin=800042237&amp;site=qq&amp;menu=yes" target="_blank" class="zixun">在线咨询</a><a class="back2btn" href="javascript:;"><img alt="Back to Top" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /></a></div>').appendTo("body");
        $win.scroll(function () {
            var _sTop = $win.scrollTop();
            if (_sTop > 300) {
                if (NALA.check.isIE6) {
                    $back2top.css({
                        'top' : _sTop + _wHeight - 127
                    });
                }
                if($back2top.data('ok') != 1){
                    $back2top.stop(true).animate({height:"117px"},200, function() {
                        $(this).data("ok",1);
                    });
                }
                
            } else {
                if($back2top.data('ok') == 1){
                    $back2top.stop(true).animate({height:"68px"},200, function() {
                        $(this).removeData("ok");
                    });
                }

                if (NALA.check.isIE6) {
                    $back2top.css({
                        'top' : _sTop + _wHeight - 78
                    });
                }
            }
        });
        $back2top.find(".back2btn").click(function () {
            $('body,html').animate({ scrollTop : 0 }, 500);
            return false;
        });
    },

    // 右侧悬浮工具栏
    toolBar: function(){
        $.ajax({
            url: "/ajax/toolbar.php",
            // url: "toolBar.html",
            type:"get",
            cache: false,
            dataType:"html",
            success: function(data){
                var $toolbar = $(data), $triggers_tab = null, $J_panels = null, $panel = null;

                var tb_switchable = function(id) {
                    var $elem = $(id);
                    if($elem.find(".h_panel").length>1){
                        $elem.find(".tips_01").switchable(id+" .h_panel", {effect:"scroll",speed:0.2,delay:0.12,triggerType:"mouse",easing:"swing",circular:true});
                    }else{
                        $elem.find(".tips_01").hide();
                    }
                };

                $toolbar.css({"right": NALA.common.getWinRight()}).appendTo("body");
                $toolbar.find("li").hover(function(){
                    $(this).addClass("hover");
                }, function(){
                    $(this).removeClass("hover");
                });
                $triggers_tab = $toolbar.find("li.triggers_tab");
                $J_panels = $("#J_panels");
                $panel = $J_panels.find(".panel");
                $triggers_tab.on('click',function(e){
                    var $this = $(this),
                        $index = $this.index();
                    
                    if(e.target.parentNode.className != 'empty_tip'){
                        if($panel.filter(":animated").length>0){
                            return false;
                        }else{
                            if($this.hasClass("current")){
                                $panel.eq($index).animate({right:"-211px"},200,function(){
                                    $(this).hide();
                                    $J_panels.parent(".panels_box").hide();
                                    $this.removeClass("current");
                                });
                            }else{
                                $J_panels.parent(".panels_box").show();
                                if($panel.filter(":visible").length>0){
                                    $panel.filter(":visible").animate({right:"-211px"},200,function(){
                                        $(this).hide();
                                        $panel.eq($index).show().animate({right:"10px"},200);
                                    });
                                }else{
                                    $panel.eq($index).show().animate({right:"10px"},200);
                                }
                                $this.addClass("current").siblings().removeClass("current");
                            }
                        }
                        return false;
                    }
                });

                $(document).on("click",function(e){
                    if($(e.target).parents(".tb_box").length==0 && $panel.filter(":visible").length ==1){
                        $toolbar.find("li.triggers_tab").removeClass("current");
                        $panel.filter(":visible").animate({right:"-211px"},200,function () {
                            $(this).hide();
                            $J_panels.parent(".panels_box").hide();
                        });
                    }
                });

                $J_panels.on('click','.J_tbClose',function(){
                    var $cPanel = $(this).parent();
                    $cPanel.animate({right:"-211px"},200,function(){
                        $cPanel.hide();
                        $triggers_tab.removeClass("current");
                        $J_panels.parent(".panels_box").hide();
                    })
                });

                $J_panels.find("dl").hover(function(){$(this).css({"backgroundColor":"#f1f1f1"})},function(){$(this).css({"backgroundColor":"#ffffff"})});

                tb_switchable("#J_tbBrands");
                tb_switchable("#J_tbHistory");
            
                NALA.common.fixed($toolbar, 20);
            }
        });
     },

    // 返回时间JSON，参数为秒
    timeJson: function(times) {
        var oTime = {};
        oTime.secs = Math.floor(times % 60);
        oTime.mins = Math.floor(times / 60 % 60);
        oTime.hours = Math.floor(times / 60 / 60);
        if(oTime.secs<10){
            oTime.secs = '0' + oTime.secs;
        }
        if(oTime.mins<10){
            oTime.mins = '0' + oTime.mins;
        }
        if(oTime.hours<10){
            oTime.hours = '0' + oTime.hours;
        }
        return oTime;
    },

    // 倒计时，包括显示毫秒跳动
    countDown: function(millisecond, options) {
        var _this = this, _dot = 9,
        _time = Math.floor(millisecond/1000),
        options = $.extend({
            timeDiv: $('#countdown em'), //倒计时显示区域
            dotTime: false  //是否显示毫秒
        },options);

        var $time = options.timeDiv;
        var timeDown = function (times) {
            var temp = _this.timeJson(times);
            $time.html(temp.hours+':'+temp.mins+':'+temp.secs);
        };

        timeDown(_time);
        setInterval(function(){
            if(_time>0){
                timeDown(--_time);
            }else{
                location.reload();
            }
        },1000);

        if(options.dotTime){
            var $dot = $('<i class="dottime">.9</i>').insertAfter($time);
            setInterval(function(){
                if(_dot < 0){
                    _dot=9;
                }
                $dot.text('.'+_dot);
                _dot--;
            },100);
        }
    }

}

})(jQuery);

$(function(){

    var $header = $("#header"),
    _other_url = /activity|active/;
    //获取用户登录信息
    $header.find("div.hd_bar").load("/ajax/loginAuth3.php?from=index" + new Date().getTime());
    // $header.find("div.hd_bar").load("loginAuth3.html?from=index" + new Date().getTime());

    // 搜索栏自动补全宽度设置
    (function($) {
        if($header.find('.hd_index').length>0){
            NALA.common.searchBar(310, true);
        }else{
            NALA.common.searchBar(170);
        }
    })(jQuery);
    

    if(NALA.common.getWinRight()>0){
        if(!_other_url.test(location.href)){
            NALA.common.toolBar();
        }
        NALA.common.back2top();
    }
    
    // 主导航下级显示隐藏
    var $nav_trigger = $("#J_navCata").find("li.hassub"),
    $nav_body = $("#J_subNav").find(".J_subView"),
    _nav_time = null;
    if($nav_trigger.length>0){
        $nav_trigger.on("mouseleave",function() {
            var i = $(this).index();
            _nav_time = setTimeout(function(){
                $nav_body.eq(i).hide();
            }, 300);
        }).on("mouseenter",function() {
            var i = $(this).index();
            if(_nav_time !== null){
                clearTimeout(_nav_time);
            }
            $nav_body.hide().eq(i).show(); 
        });

        $nav_body.on("mouseleave",function() {
            var _this = $(this);
            _nav_time = setTimeout(function(){
                _this.hide();
            }, 300);
        }).on("mouseenter",function() {
            if(_nav_time !== null){
                clearTimeout(_nav_time);
            }
        });
    }

    
    // 附属导航更多
    $("#J_navMore").hover(function() {
        $(this).addClass("hover");
    },function() {
        $(this).removeClass("hover");
    });

    //延迟加载图片2
    if($('#wrapper').length>0){
        NALA.common.lazyload("#wrapper");
    }
    if($('#active_main').length>0){
        NALA.common.lazyload("#active_main");
    }

});


