/*
*【b2c】首页JS处理文件
* update : 2013-02-21 16:00
*/
var NALA = NALA || {};
NALA.homePage={
    init:function(){
        this.swichable();
        this.morningSale();
		this.mainNav_animate();
    },
    swichable:function(){
        // 首页轮播图
        $(".trigger_hd").switchable(".switchable_hd li", {
            effect:"lazyScroll",speed:0.7,delay:0.12,triggerType:"mouse",easing:"swing",vertical:false,circular:true}).carousel().autoplay({interval:7,order:true,api:true });

        // 猜你喜欢右侧tip滚动
		var noticeApi = $(".trigger_notice").switchable(".switchable_notice li", { effect:"scroll",speed:0.7,delay:0.12,triggerType:"click",vertical:false}).carousel().autoplay({interval:6,api:true });
		$(".notice_btns .next").click(function(){noticeApi.next()});
		$(".notice_btns .prev").click(function(){noticeApi.prev()});

        // 排行榜 24小时
        $(".nav_rank").switchable(".switch_rank .panel", {speed:0.7,delay:0.12,triggerType:"mouse",vertical:false});

        // 第3栏大图切换
        $(".switchable-triggerBox").switchable(".switchable-content2 li", {
            effect:"fade",speed:0.2,delay:0.12,triggerType:"mouse",easing:"swing",circular:true}).autoplay({interval:7});
    },
	
    // 主导航栏滑动效果
    mainNav_animate:function(){
        var $mainCata = $("#J_mainCata"),
            $subCata = $("#J_subCata"),
            $arrowBtn = $mainCata.nextAll('div.J_arrowBtn'),
            animater = null;

        $mainCata.on("mouseenter","li",function(){
            var $this = $(this),
                _index =$this.index(),
                _top = $this.position().top;
            $subCata.stop().css({'opacity':1});
            $this.addClass("current").siblings().removeClass("current");

            if(animater !== null){
                clearTimeout(animater);
            }
            animater = setTimeout(function(){
                var _top2 = _top, 
                $subView = $subCata.find(".J_subView").hide().eq(_index).show();
                if(_index == 4){
                    _top2 = _top - 178;
                    _top += 4;
                }
                if($arrowBtn.is(":visible")){
                    $subCata.css({left:"180px"}).show().animate({top:_top2},200);
                    $arrowBtn.animate({top:_top + 19},200);
                }else{
                    $subCata.css({top:_top2}).show().animate({left:"180px"},200);
                    $arrowBtn.css({top:_top + 19}).show();
                }
            }, 200);
            return false;
        });

        $mainCata.parent(".cata_index").on("mouseleave",function(){
            if(animater !== null){
                clearTimeout(animater);
            }
            animater = setTimeout(function(){
                $arrowBtn.hide();
                $subCata.animate({"left":"0"},200,function(){
                    $subCata.hide().find(".J_subView").hide();
                    $mainCata.find("li").removeClass("current");
                });
            }, 200);
        }).on("mouseenter",function(){
            if(animater !== null){
                clearTimeout(animater);
            }
        });

        if(!NALA.check.isIE6){
            $mainCata.nextAll(".J_subImg").find("a").each(function(i){
                $subCata.find(".J_subView").eq(i).append(this);
            });
        }
    },

    // 早中晚模块
	/* morningSale:function(){
        $(".hd_tuan").load("/item/onedayIndex?" + new Date().getTime(), function (){
        // $(".hd_tuan").load("json.html?" + new Date().getTime(), function (){
			var _this = $(this).find(".oneday_timer"), $time,
            started = _this.data('started'),
			_time = _this.data('time');
            if(started){
                _this.html('<span class="pink">进行中</span><em>08:08:08</em>后结束');
            }else{
                _this.html('<span class="pink">即将开始</span><em>08:08:08</em>后开始');
            }
            $time = _this.find("em");
            NALA.common.countDown(_time,{timeDiv: $time});


            var wrap = $("#zao_wrapper"), switchGoods,
            canAnimate = wrap.hasClass('wu_wrapper');
            if(!canAnimate){
                var prev = wrap.find('a.prev'),
                next = wrap.find('a.next');
                switchGoods = wrap.find(".zao_trigger").switchable("#zao_wrapper li", { effect:"scroll",speed:0.7,triggerType:"click"}).carousel().autoplay({interval:7,api: true });
                wrap.hover(function () {
                    next.fadeIn();
                    prev.fadeIn();
                    wrap.addClass('hover');
                },function () {
                    next.hide();
                    prev.hide();
                    wrap.removeClass('hover');
                });
            }else{
                wrap.hover(function () {
                    wrap.addClass('hover');
                },function () {
                    wrap.removeClass('hover');
                });
            }
            wrap.on('click',function(e) {
                if(e.target.className=="next"){
                    switchGoods.next();
                }else if(e.target.className=="prev"){
                    switchGoods.prev();
                }else{
                    window.open("/item/oneday");
                }
                return false;
            });
		});
	}*/
};


$(function(){

    // 获取推荐品牌
    $.ajax({
        url: '/brand/getRecommendBrandAjax?from=index' + new Date().getTime(),
        // url: 'getRecommendBrandAjax.html?from=index' + new Date().getTime(),
        type:'get',
        success: function(result){
            var _api, $box = $(".brands_box"),
            prev = $box.find("a.prev"),
            next = $box.find("a.next");
            // $box.find('.brands_con').html(result);
            $box.html(result);
            // _api = $box.find(".trigger").switchable("#brands_box ul", { effect:"scroll",speed:0.7,triggerType:"click"}).carousel().autoplay({interval:7,api: true });
            
            // prev.click(function() {
            //     _api.prev();
            // });
            // next.click(function() {
            //     _api.next();
            // });
        }
    });

    NALA.homePage.init();

    $("#J_editorImg").hover(function(){
        $(this).find("img").eq(0).hide().siblings().show();
    },function(){
        $(this).find("img").eq(0).show().siblings().hide();
    });
});