//http://docs.seleniumhq.org/docs/08_user_extensions.jsp
Selenium.prototype.doExecute = function(script) {
    this.browserbot.getCurrentWindow().eval(script);
};
Selenium.prototype.getExecute = function(script) {
    return this.browserbot.getCurrentWindow().eval(script);
};
Selenium.prototype.getJqMethod = function(selector, method) {
    return this.getExecute('$("' + selector + '").' + method + '();');
};
Selenium.prototype.getJqText = function(selector) {
    return this.getJqMethod(selector, "text");
};
Selenium.prototype.getJqHtml = function(selector) {
    return this.getJqMethod(selector, "html");
};
Selenium.prototype.getJqVal = function(selector) {
    return this.getJqMethod(selector, "val");
};
PageBot.prototype.locateElementByJq = function(selector, inDocument) {
    // FF/Chrome/IE9+: defaultView, OldIE: parentWindow
    return (inDocument.defaultView || inDocument.parentWindow)
            .eval("jQuery('" + selector.replace(/'/g, "\\'") + "')[0];");
};

Selenium.prototype.doRoundUp = function(elementOne, elementTwo)
{
 var val = elementOne.split(',');
 var value = val[0];
 var digits = val[1];
 var numberRounded = (Math.round((value*Math.pow(10,digits)).toFixed(digits-1))/Math.pow(10,digits)).toFixed(digits);
 storedVars[ elementTwo ] = numberRounded;
};
