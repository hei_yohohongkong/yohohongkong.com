module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		cssmin: {
			mobile: {
				options: {
					report: 'min',
					sourceMap: true
				},
				files: {
					'm/dist/css/yoho-all.min.css': [
						'm/dev/css/bootstrap.css',
						'm/dev/css/typeaheadjs.css',
						'm/dev/css/bootstrapValidator.css',
						'm/dev/css/jquery.rateyo.css',
						'm/dev/css/yoho.css',
						'm/dev/css/payme.css'
					]
				}
			}
		},
		uglify: {
			mobile: {
				options: {
					// For debug
					//beautify: true,
					//mangle: false,
					sourceMap: true
				},
				files: {
					'm/dist/js/yoho-all.min.js': [
						'm/dev/js/jquery-1.11.3.js',
						'm/dev/js/bootstrap.js',
						'm/dev/js/jquery.range.js',
						'm/dev/js/jquery.rateyo.js',
						'm/dev/js/jquery.rwdImageMaps.min.js',
						'm/dev/js/jquery.countdown.button.js',
						'm/dev/js/typeahead.bundle.js',
						'm/dev/js/jquery.cookie.js',
						'm/dev/js/bootstrapValidator.js',
						'm/dev/js/jquery.touchSwipe.js',
						'm/dev/js/sprintf.js',
						'm/dev/js/yoho.js',
						'm/dev/js/payment/payme.js'
					],
				}
			}
		},
		copy: {
			mobile: {
				files: [
					{
						expand: true,
						cwd: 'm/dev/',
						src: ['fonts/**', 'img/**'],
						dest: 'm/dist/'
					}
				]
			}
		},
		hashres: {
			options: {
				encoding: 'utf8',
				fileNameFormat: '${name}.${ext}?${hash}',
				renameFiles: false
			},
			mobile: {
				src: [
					'm/dist/css/yoho-all.min.css',
					'm/dist/js/yoho-all.min.js'
				],
				dest: [
					'm/templates/library/hashres.lbi'
				]
			}
		},
		watch: {
			reloadgrunt: {
				files: ['Gruntfile.js']
			},
			mobilecss: {
				files: ['m/dev/css/*.css'],
				tasks: ['cssmin:mobile', 'hashres:mobile']
			},
			mobileassets: {
				files: ['m/dev/fonts/*', 'm/dev/img/*'],
				tasks: ['clean:mobileassets', 'copy:mobile']
			},
			mobilejs: {
				files: ['m/dev/js/*.js'],
				tasks: ['uglify:mobile', 'hashres:mobile']
			},
			mobilelivereload: {
				// Add script in HTML output to enable Live Reload
				// <script src="https://server2.yohohongkong.com:35729/livereload.js" async defer></script>
				options: {
					cwd: 'm/',
					livereload: 35629
				},
				files: [
					'templates/**',
					'!templates/library/hashres.lbi',
					'dist/fonts/*',
					'dist/img/*',
					'dist/css/yoho-all.min.css',
					'dist/js/yoho-all.min.js'
				]
			},
			desktoplivereload: {
				// Add script in HTML output to enable Live Reload
				// <script src="https://server2.yohohongkong.com:35730/livereload.js" async defer></script>
				options: {
					livereload: 35630
				},
				files: [
					'themes/skin/**',
					'yohohk/**'
				]
			},
			/*adminlivereload: {
				// Add script in HTML output to enable Live Reload
				// <script src="https://server2.yohohongkong.com:35730/livereload.js" async defer></script>
				options: {
					livereload: {
						port: 35730,
						key: grunt.file.read('/etc/ssl/private/apache-selfsigned.key'),
						cert: grunt.file.read('/etc/ssl/certs/apache-selfsigned.crt')
						// you can pass in any other options you'd like to the https server, as listed here: http://nodejs.org/api/tls.html#tls_tls_createserver_options_secureconnectionlistener
					}
				},
				files: [
					'x/**'
				],
			},*/
		},
		clean: {
			mobile: [
				'm/dist/fonts/*',
				'm/dist/img/*',
				'm/dist/css/yoho-all.min.css',
				'm/dist/js/yoho-all.min.js'
			],
			mobileassets: [
				'm/dist/fonts/*',
				'm/dist/img/*'
			]
		}
	});
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-hashres');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.registerTask('default', [
		'clean:mobile',
		'cssmin:mobile',
		'uglify:mobile',
		'copy:mobile',
		'hashres:mobile'
	]);
};
