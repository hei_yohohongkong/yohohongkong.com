<?php

/***
* yoho313.php
* by howang 2015-03-06
*
* YOHO Hong Kong 313 Anniversary Event Page
***/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

if ((DEBUG_MODE & 2) != 2)
{
    $smarty->caching = true;
}

$topic_id = 98;
$sql = "SELECT * FROM " . $ecs->table('topic') .
        "WHERE topic_id = '$topic_id'";
$topic = $db->getRow($sql);

// $admin = check_admin_login();
// $is_admin = is_array($admin) && !empty($admin['user_id']);

//$cache_id = sprintf('%X', crc32($_SESSION['user_rank'] . '-' . $_CFG['lang'] . '-' . $topic_id . '-' . local_date('Y-m-d') . '-' . $is_admin));
$cache_id = sprintf('%X', crc32($_SESSION['user_rank'] . '-' . $_CFG['lang'] . '-' . $topic_id));

if (!$smarty->is_cached('yoho313.dwt', $cache_id))
{
    if (empty($topic['topic_img']))
    {
        $topic['header_type'] = 'html';
    }
    else
    {
        if ((strpos($topic['topic_img'], '://')  === false) && (substr($topic['topic_img'], 0, 1) != '/'))
        {
            $topic['topic_img'] = '/' . $topic['topic_img'];
        }
        
        if (strpos($topic['topic_img'], '.swf')  !== false)
        {
            $topic['header_type'] = 'flash';
        }
        else
        {
            $topic['header_type'] = 'image';
        }
    }
    
    // $where = "g.promote_price > 0 " .
    //     "AND (" . gmtime() . " BETWEEN g.promote_start_date AND g.promote_end_date)";
    // $sql = hw_goods_list_sql($where, 'goods_id', 'DESC');
    // $res = $GLOBALS['db']->getAll($sql);
    // $sale_goods = hw_process_goods_rows($res);
    
    $tmp = @unserialize($topic['data']);
    if (!$tmp) // Load old malformed data
    {
        $topic['data'] = addcslashes($topic['data'], "'");
        $tmp = @unserialize($topic['data']);
    }
    $data_arr = (array)$tmp;

    $goods_id = array();
    $sort_goods_arr = array();

    foreach ($data_arr AS $key=>$value)
    {
        foreach($value AS $k => $val)
        {
            $opt = explode('|', $val);
            $data_arr[$key][$k] = $opt[1];
            $goods_id[] = $opt[1];
            $sort_goods_arr[($key == 'default' ? _L('topic_all_goods', '全部相關產品') : $key)] = array();
        }
    }
    
    $sql = hw_goods_list_sql(db_create_in($goods_id, 'g.goods_id'), 'goods_id', 'DESC');
    $res = $db->getAll($sql);
    $res = hw_process_goods_rows($res);
    
    foreach ($res as $row)
    {
        foreach ($data_arr AS $key => $value)
        {
            foreach ($value AS $val)
            {
                if ($val == $row['goods_id'])
                {
                    $key = $key == 'default' ? _L('topic_all_goods', '全部相關產品') : $key;
                    $sort_goods_arr[$key][] = $row;
                }
            }
        }
    }

    /* 模板赋值 */
    assign_template();
    $position = assign_ur_here(0, $topic['title']);
    $smarty->assign('page_title',       $position['title']);       // 页面标题
    $smarty->assign('ur_here',          $position['ur_here']);     // 当前位置
    $smarty->assign('show_marketprice', $_CFG['show_marketprice']);
    // $smarty->assign('sale_goods',       $sale_goods);              // 商品列表
    $smarty->assign('sort_goods_arr',   $sort_goods_arr);          // 商品列表
    $smarty->assign('topic',            $topic);                   // 专题信息
    $smarty->assign('keywords',         $topic['keywords']);       // 专题信息
    $smarty->assign('description',      $topic['description']);    // 专题信息
    $smarty->assign('title_pic',        $topic['title_pic']);      // 分类标题图片地址
    $smarty->assign('base_style',       '#' . $topic['base_style']);     // 基本风格样式颜色
    
    $CategoryController = new Yoho\cms\Controller\CategoryController();
    $t1 = $CategoryController->getTopCategories(0);
    $smarty->assign('t1',              $t1);
    $smarty->assign('helps',            get_shop_help());
    
    //$launch_time = local_mktime(0, 0, 0, 3, 13, 2015); // 2015-03-13
    //$launched = (gmtime() >= $launch_time) || $is_admin;
    $launched = true;
    $smarty->assign('launched',         $launched);
}
/* 显示模板 */
$smarty->display('yoho313.dwt', $cache_id);

?>