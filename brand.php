<?php

/**
 * ECSHOP 品牌列表
 * ============================================================================
 * * 版权所有 2005-2012 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liubo $
 * $Id: brand.php 17217 2011-01-19 06:29:08Z liubo $
 */
define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
$attrbuteController = new Yoho\cms\Controller\AttributeController();
$categoryController = new Yoho\cms\Controller\CategoryController();

if ((DEBUG_MODE & 2) != 2) {
    $smarty->caching = true;
}

if (!isSecure()) {
    $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: ' . $redirect);
    exit;
}

/* ------------------------------------------------------ */
//-- INPUT
/* ------------------------------------------------------ */

/* 获得请求的分类 ID */
if (!empty($_REQUEST['id'])) {
    $brand_id = intval($_REQUEST['id']);
}
if (!empty($_REQUEST['brand'])) {
    $brand_id = intval($_REQUEST['brand']);
}
$brand_id = empty($brand_id) ? 0 : $brand_id;

// Redirect user to new URL without the php filename
if (strpos($_SERVER['REQUEST_URI'], 'brand.php') !== false) {
    $new_url = build_uri('brand', array('bid' => $brand_id));

    // Append with original query string parameters, except the id which is already included in $new_url
    parse_str($_SERVER["QUERY_STRING"], $get);
    if (isset($get['id']))
        unset($get['id']);
    if (isset($get['brand']))
        unset($get['brand']);
    if (!empty($get)) {
        $new_url .= (strpos($new_url, '?') === false ? '?' : '&') . http_build_query($get, '', '&');
    }

    header('Location: ' . $new_url);
    exit;
}

if (empty($brand_id)) {
    /* 缓存编号 */
    $cache_id = sprintf('%X', crc32($_CFG['lang']));
    if (!$smarty->is_cached('brand_list.dwt', $cache_id)) {
        assign_template();
        $position = assign_ur_here('', _L('brand_list', '全部品牌'));
        $smarty->assign('page_title', $position['title']);    // 页面标题
        $smarty->assign('ur_here', $position['ur_here']);  // 当前位置
        $smarty->assign('keywords', htmlspecialchars($_CFG['shop_keywords']));
        $smarty->assign('description', htmlspecialchars($_CFG['shop_desc']));
        $smarty->assign('helps', get_shop_help());       // 网店帮助
        $t1 = $categoryController->getTopCategories(0);
        $smarty->assign('t1',              $t1);

        $smarty->assign('brand_list', get_brands());
    }
    $smarty->display('brand_list.dwt', $cache_id);
    exit();
}

/* 初始化分页信息 */
$page = !empty($_REQUEST['page']) && intval($_REQUEST['page']) > 0 ? intval($_REQUEST['page']) : 1;
$size = !empty($_CFG['page_size']) && intval($_CFG['page_size']) > 0 ? intval($_CFG['page_size']) : 10;
$cat_id = !empty($_REQUEST['cat']) && intval($_REQUEST['cat']) > 0 ? intval($_REQUEST['cat']) : 0;
$price_min = isset($_REQUEST['price_min']) && intval($_REQUEST['price_min']) > 0 ? intval($_REQUEST['price_min']) : 0;
$price_max = isset($_REQUEST['price_max']) && intval($_REQUEST['price_max']) > 0 ? intval($_REQUEST['price_max']) : 0;

/* 排序、显示方式以及类型 */
$default_display_type = $_CFG['show_order_type'] == '0' ? 'list' : ($_CFG['show_order_type'] == '1' ? 'grid' : 'text');
$default_sort_order_method = $_CFG['sort_order_method'] == '0' ? 'DESC' : 'ASC';
$default_sort_order_type   = $_CFG['sort_order_type'] == '0' ? 'last_update' : ($_CFG['sort_order_type'] == '1' ? 'shop_price' : 'goods_id');

$sort = (isset($_REQUEST['sort']) && in_array(trim(strtolower($_REQUEST['sort'])), array('goods_id', 'shop_price', 'last_update', 'shownum'))) ? trim($_REQUEST['sort']) : $default_sort_order_type;
$order = (isset($_REQUEST['order']) && in_array(trim(strtoupper($_REQUEST['order'])), array('ASC', 'DESC'))) ? trim($_REQUEST['order']) : $default_sort_order_method;
$display = (isset($_REQUEST['display']) && in_array(trim(strtolower($_REQUEST['display'])), array('list', 'grid', 'text'))) ? trim($_REQUEST['display']) : (isset($_COOKIE['ECS']['display']) ? $_COOKIE['ECS']['display'] : $default_display_type);
//$display  = in_array($display, array('list', 'grid', 'text')) ? $display : 'text';
$display = in_array($display, array('list', 'grid')) ? $display : 'grid';
$filter_attr_str = isset($_REQUEST['filter_attr']) ? htmlspecialchars(trim($_REQUEST['filter_attr'])) : '0';

$filter_attr_str = trim(urldecode($filter_attr_str));
$filter_attr_str = preg_match('/^[\d\.]+$/', $filter_attr_str) ? $filter_attr_str : '';
$filter_attr = empty($filter_attr_str) ? '' : explode('.', $filter_attr_str);
setcookie('ECS[display]', $display, gmtime() + 86400 * 7);

/* ------------------------------------------------------ */
//-- PROCESSOR
/* ------------------------------------------------------ */

/* 页面的缓存ID */
$cache_id = sprintf('%X', crc32(
                $brand_id . '-' .
                $cat_id . '-' .
                $price_min . '-' .
                $price_max . '-' .
                $sort . '-' .
                $order . '-' .
                $page . '-' .
                $size . '-' .
                $filter_attr_str . '-' .
                $_SESSION['user_rank'] . '-' .
                $_CFG['lang']));

if (!$smarty->is_cached('brand.dwt', $cache_id)) {
    $brand_info = get_brand_info($brand_id);
    if (empty($brand_info)) {
        header('Location: /brand');
        exit;
    }

    $children = get_children($cat_id);
    
    /* 属性筛选 */
    $attr_data = $attrbuteController->get_attr_data($cat_id, $filter_attr, 'brand', array('cid'=>$cat_id, 'bid'=>$brand_id, 'price_min'=>$price_min, 'price_max'=>$price_max, 'filter_attr'=>$filter_attr));
    $ext = $attr_data['ext']; //商品查询条件扩展
    $all_attr_list = $attr_data['all_attr_list'];

    $count = hw_get_cagtegory_goods_count($children, $brand_id, $price_min, $price_max, $ext);
    $goodslist = hw_category_get_goods($children, $brand_id, $price_min, $price_max, $ext, $size, $page, $sort, $order);
    $where = "g.brand_id = '" . $brand_id . "' AND g.is_on_sale = 1 AND g.is_alone_sale = 1 AND g.is_delete = 0 ";
    $left_menu = $categoryController->getLeftCatMenu($cat_id, "brand", $where, $brand_id);
    $lang = $_CFG['lang'];
    $l3_cat_name = "";
    foreach($left_menu as $val){
        $level2 = $val["children"];
        foreach($level2 as  $val2){
            $level3 = $val2["children"];

            foreach($level3 as $key => $val3){
                if($lang == 'en_us') {
                        $l3_cat_name .= $val3["cat_name"] ."，";   
                } else {
                        $l3_cat_name .= $val3["cat_name"] ."、";
                }
            }
        }      
    }
    $l3_cat_name_len = strlen($l3_cat_name);
    $l3_cat_name = substr($l3_cat_name,0,$l3_cat_name_len-3);
    $brand_desc = $brand_info['brand_desc'];   
    $insert_pos = strpos($brand_desc,"-");
    $brand_info['brand_desc'] = substr_replace($brand_desc, $l3_cat_name , $insert_pos-1,0);
    

    $goods_id_arr = array();
    foreach($goodslist as $val){
        $goods_id_arr[] = $val['goods_id'];
    }
    $goodsController = new Yoho\cms\Controller\GoodsController();
    //get favourable list
    $favourable_arr = $goodsController->product_list_get_favourable_info($_SESSION['user_rank'], $goods_id_arr);

    //get package list
    $package_arr = $goodsController->product_list_get_package_info($goods_id_arr);

    //get topic list
    $topic_arr = $goodsController->product_list_get_topic_info($goods_id_arr);


    /* 赋值固定内容 */
    assign_template();
    $position = assign_ur_here($cat_id, $brand_info['brand_name']);
    $smarty->assign('page_title', $position['title']);   // 页面标题
    $smarty->assign('ur_here', $position['ur_here']); // 当前位置
    $smarty->assign('brand_id', $brand_id);
    $smarty->assign('category_id', $cat_id);
    $smarty->assign('brand_info', $brand_info);
    $smarty->assign('goods_list', $goodslist);
    $smarty->assign('favourable_list', $favourable_arr);
    $smarty->assign('package_list',    $package_arr);
    $smarty->assign('topic_list',      $topic_arr);
    $smarty->assign('script_name', 'brand');
    $smarty->assign('display', $display);
    $smarty->assign('sort', $sort);
    $smarty->assign('order', $order);
    $smarty->assign('data_dir', DATA_DIR);
    $smarty->assign('keywords', htmlspecialchars($brand_info['brand_keywords']));

    $t1 = $categoryController->getTopCategories(0);
    $smarty->assign('t1',              $t1);
    $smarty->assign('helps', get_shop_help());              // 网店帮助
    $smarty->assign('filter_attr_list',  $all_attr_list);
    // Left side Category Menu
    $pid = 0;
    $smarty->assign('cat_menu_pid', $pid);
    $smarty->assign('cat_menu_pname', hw_get_cat_name($pid));
    $smarty->assign('cat_menu_plink', '/brand/' . $brand_id . '-' . uri_encode($brand_info['brand_name']));
    $cat_menu = array();


    $smarty->assign('description', htmlspecialchars($brand_info['brand_desc']));

    if($category > 0){
        $first_parent = array_reverse($categoryController->getParentLevel($category));
        $smarty->assign('first_parent_cat',$first_parent[0]['cat_id']);
    }

    $smarty->assign('left_cat_menu', $left_menu);
    $smarty->assign('filter_attr',      $filter_attr_str);
    // Price Filter
    $smarty->assign('price_min', $price_min);
    $smarty->assign('price_max', $price_max);
    hw_assign_price_range("g.is_delete = 0 AND g.is_on_sale = 1 AND g.is_alone_sale = 1 AND g.brand_id = '$brand_id'",$_SESSION['user_rank']);

    assign_pager('brand', $cat_id, $count, $size, $sort, $order, $page, $ext, $brand_id, 0, 0, $display, $is_promote,'','',['bperma' => $brand_info['bperma']]); // 分页
    assign_dynamic('brand'); // 动态内容
}

$smarty->display('brand.dwt', $cache_id);

/* ------------------------------------------------------ */
//-- PRIVATE FUNCTION
/* ------------------------------------------------------ */

/**
 * 获得与指定品牌相关的分类
 *
 * @access  public
 * @param   integer $brand
 * @return  array
 */
function brand_related_cat($brand_id, $brand_name = '') {
    $where = "g.brand_id = '" . $brand_id . "' AND g.is_on_sale = 1 AND g.is_alone_sale = 1 AND g.is_delete = 0 ";

    $res = $GLOBALS['db']->getAll($sql);
    global $smarty, $db, $ecs;

    $array      = array();
    $parent_ids = array();
    foreach ($res as $key => $row) {
        $array[$row['parent_cat_name']][$key] = $row;
        $array[$row['parent_cat_name']][$key]['url'] = build_uri('brand', array(
            'cid' => $row['cat_id'], 'cname' => $row['child_cat_name'],
            'bid' => $brand_id, 'bname' => $brand_name), $row['child_cat_name']);
        $array[$row['parent_cat_name']][$key]['child_cat_name'] = $row['child_cat_name'];
        $array[$row['parent_cat_name']][$key]['parent_cat_name'] = $row['parent_cat_name'];
        $array[$row['parent_cat_name']]['id'] = $row['p_id'];
        $parent_ids[] = $row['p_id'];
    }

    /* Anthony: Get Level 1 category sort order */
    $sql = "SELECT IFNULL(cl.cat_name, c.cat_name) as cat_name FROM ".$ecs->table('category') . " AS c " .
            " LEFT JOIN " . $ecs->table('category_lang') . " as cl ON c.cat_id = cl.cat_id AND cl.lang = '" . $_CFG['lang'] . "' ".
            " WHERE c.cat_id ".db_create_in($parent_ids)." ORDER BY c.sort_order ";
    $p_sort_order = $db->getCol($sql);

    /* Sort by Level 1 sort order  */
    $array = array_replace(array_flip($p_sort_order), $array);

    $result = array();
    $result[0] = $array;
    $result[1] = count($res);
    return $result;
}

function brand_related_attr($brand_id)
{
    global $smarty, $db, $ecs;

    $sql = "SELECT a.attr_id from ".$ecs->table('attribute')." as a ".
    "left join  ".$ecs->table('goods')." as g on g.brand_id = $brand_id ".
    "left join ".$ecs->table('goods_attr')." as ga on ga.goods_id = g.goods_id ".
    "WHERE a.attr_id = ga.attr_id group by a.attr_id";

    return $db->getCol($sql);
}

?>
