<?php
define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
assign_template();
if(isset($_REQUEST['success'])){
    if(isset($_REQUEST['subscribe']))
        show_message(_L('subscription_message_subscribe_success', "您已經訂閱了友和電子報，請前往電郵點選確認鏈結以完成訂閱程序"));
    else if(isset($_REQUEST['update']))
        show_message(_L('subscription_message_update_success', '您已成功更新資料！\n若您有更新電郵地址，請前往郵箱點擊確認訂閱電子報的鏈結'));    
    else if(isset($_REQUEST['unsubscribe']))
        show_message(_L('subscription_message_unsubscribe_success', '您已取消訂閱友和電子報'));
    else if(isset($_REQUEST['confirm']))
        show_message(_L('subscription_message_confirm_success', '您已成功訂閱友和電子報'));
    else
        header('Location: /');
}else if(isset($_REQUEST['testing'])){
    $CategoryController = new Yoho\cms\Controller\CategoryController();
    if(isset($_REQUEST['email']))
        $smarty->assign('email',        $_REQUEST['email']);
    assign_template();
    $base_url = "includes/sendy/";
    $api_key = "ePI3ubOA2H5bllwAKSev";
    $list_id = "0Cyr16RO3RMNYTGGPZBH5g";
    if(strpos($_SERVER['SERVER_NAME'], 'beta.yohohongkong.com') === false){
        $base_url = "https://email.yohohongkong.com/s/";
        $api_key = "ywtBI7KMOSijkDXsah1M";
        $list_id = "HsrtT0Hb8kqxF0vPPPUBNg";
    }
    $smarty->assign('base_url',         $base_url);
    $smarty->assign('api_key',          $api_key);
    $smarty->assign('list_id',          $list_id);
    $smarty->assign('subscribe',        intval(isset($_REQUEST['subscribe'])));
    $smarty->assign('update',           intval(isset($_REQUEST['update'])));
    $smarty->assign('unsubscribe',      intval(isset($_REQUEST['unsubscribe'])));
    $smarty->assign('success',          intval(isset($_REQUEST['success'])));
    $smarty->assign('page_title',       '訂閱友和電子報');                   // 页面标题
    $smarty->assign('ur_here',          '訂閱友和電子報');                   // 当前位置
    $smarty->assign('helps',            get_shop_help());                   // 网店帮助
    $smarty->assign('navigator_list',   get_navigator());                   // Nav Links
    $t1 = $CategoryController->getTopCategories(0);
    $smarty->assign('t1',              $t1);
    $smarty->display('subscribe.dwt');
}else{
    header('Location: /');
}
?>