<?php

/**
 * ECSHOP
 * ============================================================================
 * * 版權所有 2005-2012 上海商派網絡科技有限公司，並保留所有權利。
 * 網站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 這不是一個自由軟件！您只能在不用於商業目的的前提下對程序代碼進行修改和
 * 使用；不允許對程序代碼以任何形式任何目的的再發佈。
 * ============================================================================
 * $Author: liubo $
 * $Id: message.php 17217 2011-01-19 06:29:08Z liubo $
*/
/* 這段信息 */
$_LANG['sender_id'] = '指派者';
$_LANG['receiver_id'] = '接收者';
$_LANG['send_date'] = '發送日期';
$_LANG['read_date'] = '閱讀日期';
$_LANG['status'] = '狀態';
$_LANG['readed'] = '是否已讀';
$_LANG['deleted'] = '是否刪除';
$_LANG['title'] = '標題';
$_LANG['message'] = '內容';

$_LANG['view_msg'] = '查看工作';
$_LANG['send_msg'] = '新增工作';
$_LANG['edit_msg'] = '編輯工作';
$_LANG['drop_msg'] = '刪除工作';
$_LANG['all_amdin'] = '所有管理員';
$_LANG['msg_list'] = '工作列表';
$_LANG['no_read'] = '未閱讀';
$_LANG['next_list'] = '下一條';
$_LANG['action_succeed'] = '操作成功!';

$_LANG['back_list'] = '返回工作列表';
$_LANG['continue_send_msg'] = '繼續新增工作';

/* 提示信息 */
$_LANG['js_languages']['pic_empty'] = '請填寫跟進人!';
$_LANG['js_languages']['title_empty'] = '請填寫工作主題!';
$_LANG['js_languages']['message_empty'] = '請填寫工作內容!';

$_LANG['select_msg_type'] = '選擇查看類型';

$_LANG['message_type'][0] = '所有未封存的工作';
$_LANG['message_type'][1] = '所有我的工作';
$_LANG['message_type'][2] = '我指派的工作';
$_LANG['message_type'][3] = '已封存的工作';

$_LANG['drop_msg'] = '刪除選中';

$_LANG['batch_drop_success'] = '成功刪除了 %d 個工作記錄';
$_LANG['no_select_msg'] = '您現在沒有任何工作';

$_LANG['view_msg'] = '查看/跟進';
$_LANG['edit']     = '編輯';
$_LANG['remove']   = '封存';
$_LANG['restore']  = '還原';
$_LANG['delete']   = '您現在沒有任何工作';
$_LANG['permanent_delete'] = '永久刪除';
$_LANG['oneClickArchive'] = '一按封存';
$_LANG['oneClickFinish']  = '一按完成';

?>