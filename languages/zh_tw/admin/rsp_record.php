<?php


$_LANG['back_list'] = '返回列表';
$_LANG['dispose_succed'] = '處理成功';
$_LANG['rsp_add'] = '新增WEEE回收記錄';
$_LANG['add_rsp'] = '新增WEEE回收記錄';
$_LANG['rsp_list'] = 'WEEE回收記錄列表';
$_LANG['rsp_view'] = 'WEEE回收記錄查看';

$_LANG['rsp_id'] = '編號';
$_LANG['rsp_sn'] = 'WEEE回收編號';
$_LANG['status'] = '狀態';

$_LANG['create_at'] = '記錄時間';
$_LANG['create_from'] = '記錄途徑';
$_LANG['order_sn'] = '訂單號';
$_LANG['consignee'] = '聯絡人姓名';
$_LANG['tel'] = '聯絡電話';
$_LANG['address'] = '地址';
$_LANG['buy_total'] = '購買電器數目';

$_LANG['call_date'] = '通知收集日期';
$_LANG['call_from'] = '通知途徑';
$_LANG['appointed_collection_date'] = '約定收集日期';

$_LANG['collection_date'] = '已收集日期';
$_LANG['collection_name'] = '收集者';
$_LANG['collection_total'] = '應收電器數目';
$_LANG['real_collection_total'] = '實收電器數目';
$_LANG['remark'] = '備註';

$_LANG['collection_info'] = '收集者資訊';

?>