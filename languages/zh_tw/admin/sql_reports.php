<?php


/* Data Export Headings */
$_LANG['order_date']    = '開單日期';
$_LANG['order_sn']    = '訂單編號';
$_LANG['goods_name']    = '產品名稱';
$_LANG['goods_number']    = '購買數量';
$_LANG['user_id']    = '用戶編號';
$_LANG['user_name']    = '用戶名稱';
$_LANG['email']    = '用戶電郵';
$_LANG['user_email_registered']    = '已經填入電郵';
$_LANG['user_pay_points'] = "積分結餘";

/* Webpage headings */
$_LANG['customer_purchase_report_block_heading'] = "顧客購物報告";
$_LANG['member_table'] = '用戶報表';

$_LANG['details'] = "篩選細節";
$_LANG['report_days'] = "報告日數";
$_LANG['start_date'] = "開始日期";
$_LANG['end_date'] = "結束日期";
$_LANG['gender'] = "性別";
$_LANG['gender_unknown'] = "不明";
$_LANG['male'] = "男";
$_LANG['female'] = "女";
$_LANG['goods_id_list'] = "產品編號";
$_LANG['goods_id_list_placeholder'] = "產品ID(可用逗號,分隔多個ID)";
$_LANG['category'] = "分類";
$_LANG['brand'] = "品牌";
$_LANG['user_email_registered'] = "是否已填電郵";
$_LANG['all'] = "所有";
$_LANG['yes'] = "是";
$_LANG['no'] = "否";
$_LANG['detailed_report'] = "詳細版本";

$_LANG['query_created'] = '報表查詢已建立';
$_LANG['query_pending'] = '等候中';
$_LANG['query_done'] = '完成';
$_LANG['query_expired'] = '過時中止';
$_LANG['download_query_report'] = '下載報表';
$_LANG['download_query_report_not_available'] = '不適用';

$_LANG['query_status_list_heading'] = "最近的報告紀錄 (每五秒更新)";
$_LANG['query_date'] = "日期";
$_LANG['query_type'] = "類型";
$_LANG['query_status'] = "狀態";
$_LANG['query_report_download'] = "下載";

$_LANG['export_data'] = "建立報告";

/* Days options */
$_LANG['30_days_ago'] = '30 日前';
$_LANG['60_days_ago'] = '60 日前';
$_LANG['90_days_ago'] = '90 日前';

/* Accounting Entry Report */
$_LANG['accounting_entry_id'] = "編號";
$_LANG['accounting_entry_operator'] = "操作員";
$_LANG['accounting_entry_txn_date'] = "交易日期";
$_LANG['accounting_entry_actg_month'] = "會計月份";
$_LANG['accounting_entry_txn_account_name'] = "交易帳戶";
$_LANG['accounting_entry_txn_account_id'] = "交易帳戶編號";
$_LANG['accounting_entry_remark'] = "備註";
$_LANG['accounting_entry_debit'] = "Debit";
$_LANG['accounting_entry_credit'] = "Credit";
$_LANG['accounting_entry_status'] = "狀態";

/* Member Table */


/* JavaScript-related strings */
// Validation-related
$_LANG['js_languages']['no_end_date'] = '請輸入結束日期';
$_LANG['js_languages']['no_start_date'] = '請輸入開始日期';
$_LANG['js_languages']['incorrect_date_range_order'] = '請確保結束日期是在開始日期之後或相等';

// Form headings
$_LANG['js_languages']['customer_purchase_report_filenmae_form_heading'] = '請輸入報表檔案名稱(包括.xls/.xlsx 副檔名)';
$_LANG['js_languages']['report_filename'] = '檔案名稱';
$_LANG['js_languages']['initialize_data_export'] = '開始建立報表';

