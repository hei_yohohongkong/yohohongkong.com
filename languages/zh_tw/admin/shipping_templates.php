<?php

$_LANG['template_name']    = '安裝條款名稱';
$_LANG['template_content'] = '安裝條款內容';
$_LANG['template_content_notice'] = '請確保已同時輸入英文版本';
$_LANG['edit_shipping_templates'] = '編輯安裝條款模板';
$_LANG['shipping_templates_list'] = '安裝條款模板列表';