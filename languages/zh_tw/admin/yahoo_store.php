<?php
//table
$_LANG['created_at'] = '新增日期';
$_LANG['goods_name'] = '產品名稱';
$_LANG['market_price'] = '市場價錢';
$_LANG['yahoo_price'] = '雅虎售價';
$_LANG['yahoo_quantity'] = '數量';
$_LANG['yahoo_brand_id'] = '雅虎分類ID';
$_LANG['batch_handle_ok'] = '批量操作成功。';
// ajax
$_LANG['yahoo_price_invalid'] = '您輸入了一個非法的價格。';
$_LANG['yahoo_qty_invalid'] = '您輸入了一個非法的數量。';
/* 提示信息 */
$_LANG['drop_confirm'] = '您確認要刪除商品嗎？';
$_LANG['export_confirm'] = '您確認要匯出商品到CSV嗎？';

?>
