<?php
$_LANG['rank_program_application'] = "會員計劃申請";
$_LANG['rank_program_validate'] = "審核會員申請";
$_LANG['apllication_status'][RANK_APPLICATION_WAIT] = "等待審核";
$_LANG['apllication_status'][RANK_APPLICATION_PREPARE] = "通過自動審核";
$_LANG['apllication_status'][RANK_APPLICATION_APPROVED] = "通過完全審核";
$_LANG['apllication_status'][RANK_APPLICATION_DISAPPROVE] = "拒絕審核";
$_LANG['apllication_status'][RANK_APPLICATION_FURTHER] = "需更新資料";

$_LANG['formatted_apllication_status'][RANK_APPLICATION_WAIT] = "<span class='purple'>等待審核</span>";
$_LANG['formatted_apllication_status'][RANK_APPLICATION_PREPARE] = "<span class='green'>通過自動審核</span>";
$_LANG['formatted_apllication_status'][RANK_APPLICATION_APPROVED] = "<span class='green'>通過完全審核</span>";
$_LANG['formatted_apllication_status'][RANK_APPLICATION_DISAPPROVE] = "<span class='aero'>拒絕審核</span>";
$_LANG['formatted_apllication_status'][RANK_APPLICATION_FURTHER] = "<span class='red'>需更新資料</span>";
?>