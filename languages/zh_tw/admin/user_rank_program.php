<?php
$_LANG['add_rank_program'] = "新增會員計劃";
$_LANG['add_organization'] = "新增計劃驗證院校/機構";
$_LANG['edit_organization'] = "編輯";
$_LANG['edit_organization_ur_here'] = "編輯計劃驗證院校/機構";
$_LANG['list_organization'] = "計劃驗證院校/機構列表";
$_LANG['rank_program_status'][RANK_PROGRAM_HIDE] = "已隱藏";
$_LANG['rank_program_status'][RANK_PROGRAM_SHOW] = "進行中";
$_LANG['rank_program_status'][RANK_PROGRAM_END]  = "已結束";

$_LANG['list_pricing_profile'] = "價格方案";
$_LANG['add_pricing_profile'] = "新增價格方案";
$_LANG['edit_pricing_profile_ur_here'] = "修改價格方案";

$_LANG['profile_name']   = "名稱";
$_LANG['edit_pricing_profile'] = "編輯";
$_LANG['organization_name']   = "院校/機構名稱";
$_LANG['organization_domain'] = "電郵域名";
$_LANG['organization_email']  = "聯絡電郵";
$_LANG['organization_info']   = "資料";
$_LANG['organization_type']   = "類型";
$_LANG['enabled']             = "可用";

$_LANG['formatted_rank_program_status'][RANK_PROGRAM_HIDE] = "<span class='red'>已隱藏</span>";
$_LANG['formatted_rank_program_status'][RANK_PROGRAM_SHOW] = "<span class='green'>進行中</span>";
$_LANG['formatted_rank_program_status'][RANK_PROGRAM_END]  = "<span class='aero'>已結束</span>";
?>