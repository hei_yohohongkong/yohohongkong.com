<?php

/**
 * ECSHOP 權限名稱語言文件
 * ============================================================================
 * * 版權所有 2005-2012 上海商派網絡科技有限公司，並保留所有權利。
 * 網站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 這不是一個自由軟件！您只能在不用於商業目的的前提下對程序代碼進行修改和
 * 使用；不允許對程序代碼以任何形式任何目的的再發佈。
 * ============================================================================
 * $Author: liubo $
 * $Id: priv_action.php 17217 2011-01-19 06:29:08Z liubo $
*/

$_LANG['memu_show'] = '於選單中顯示';
$_LANG['unclassified'] = '未分類權限';

/* 權限管理的一級分組 */
$_LANG['goods'] = '產品管理';
$_LANG['cms_manage'] = '資訊管理';
$_LANG['users_manage'] = '會員管理';
$_LANG['priv_manage'] = '權限管理';
$_LANG['sys_manage'] = '系統設置';
$_LANG['order_manage'] = '訂單管理';
$_LANG['promotion'] = '促銷管理';
$_LANG['email'] = '郵件管理';
$_LANG['templates_manage'] = '模板管理';
$_LANG['db_manage'] = '數據庫管理';
$_LANG['sms_manage'] = '短信管理';

/**************************************************************************/
//進銷存系統權限管理的一級分組
$_LANG['erp_priv_sys_manage'] = '進銷存系統設置';
$_LANG['erp_priv_order_manage'] = '進銷存訂單管理';
$_LANG['erp_priv_warehouse_manage'] = '進銷存庫存管理';
$_LANG['erp_priv_finance_manage'] = '進銷存財務管理';

//進銷存系統系統設置部分的權限
$_LANG['erp_sys_manage'] = 'ERP系統權限';

//進銷存採購訂單部分的權限
$_LANG['erp_order_view'] = '查看採購訂單';
$_LANG['erp_order_manage'] = '管理採購訂單';
$_LANG['erp_order_manage_alt'] =  implode("<br>", ['採購訂單列表 - 供應商Dropdown','(*如要套用「供應商<>使用者」權限,請除移「採購訂單定價」及「審核採購訂單」)']);
$_LANG['erp_order_rate'] = '採購訂單定價';
$_LANG['erp_order_approve'] = '審核採購訂單';
$_LANG['erp_order_unapprove'] = '反審核採購訂單';
$_LANG['erp_order_receipt'] = '查閱/發送採購訂單收據';
$_LANG['erp_order_finish'] = '完成採購訂單';
$_LANG['erp_credit_note'] = '管理Credit Note';
$_LANG['erp_order_finish_alt'] = implode("<br>", ['採購訂單列表 - 付款','採購訂單列表 - 完成','採購訂單列表 - 付款紀錄']);

//進銷存庫存管理部分的權限
$_LANG['erp_warehouse_view'] = '查看庫存';
$_LANG['erp_warehouse_manage'] = '庫存管理';
$_LANG['erp_warehouse_approve'] = '審核出入庫';

//進銷存財務部分的權限
$_LANG['erp_finance_view'] = '查看財務信息';
$_LANG['erp_finance_manage'] = '管理財務信息';
$_LANG['erp_finance_approve'] = '審核收付款單';
/**************************************************************************/

//產品管理部分的權限
$_LANG['goods_manage'] = '產品添加/編輯';
$_LANG['remove_back'] = '產品刪除/恢復';
$_LANG['cat_manage'] = '分類添加/編輯';
$_LANG['cat_drop'] = '分類轉移/刪除';
$_LANG['attr_manage'] = '產品屬性管理';
$_LANG['brand_manage'] = '產品品牌管理';
$_LANG['comment_priv'] = '用家評價管理';
$_LANG['goods_type'] = '產品類型';
$_LANG['tag_manage'] = '標籤管理';
$_LANG['goods_auto'] = '產品自動上下架';
$_LANG['topic_manage'] = '專題管理';
$_LANG['virualcard'] = '虛擬卡管理';
$_LANG['picture_batch'] = '圖片批量處理';
$_LANG['gen_goods_script'] = '生成產品代碼';
$_LANG['suppliers_goods'] = '供貨商產品管理';

$_LANG['batch_on_sale'] = '批量操作 - 上架';
$_LANG['batch_not_on_sale'] = '批量操作 - 下架';
$_LANG['goods_export'] = '批量操作 - 產品批量導出';
$_LANG['batch_shipping_fee'] = '批量操作 - 另收運費';
$_LANG['batch_warranty'] = '批量操作 - 保養條款';
$_LANG['batch_suppliers'] = '批量操作 - 供應商';
$_LANG['batch_brand'] = '批量操作 - 品牌';
$_LANG['batch_move_to'] = '批量操作 - 轉移到分類';
$_LANG['batch_trash'] = '批量操作 - 回收站';
$_LANG['goods_batch'] = '批量操作 - 產品批量修改';
$_LANG['batch_sort_order'] = '批量操作 - 修改排序';
$_LANG['batch_id_list'] = '批量操作 - 生成產品ID列表';
$_LANG['batch_add_tag'] = '批量操作 - 新增Tag';

//資訊管理部分的權限
$_LANG['article_cat'] = '資訊分類管理';
$_LANG['article_manage'] = '資訊內容管理';
$_LANG['shopinfo_manage'] = '網店信息管理';
$_LANG['shophelp_manage'] = '網店幫助管理';
$_LANG['vote_priv'] = '在線調查管理';
$_LANG['article_auto'] = '資訊自動發佈';

//會員信息管理
$_LANG['integrate_users'] = '會員數據整合';
$_LANG['sync_users'] = '同步會員數據';
$_LANG['users_manages'] = '會員添加/編輯';
$_LANG['users_drop'] = '會員刪除';
$_LANG['user_rank'] = '會員等級管理';
$_LANG['feedback_priv'] = '會員留言管理';
$_LANG['surplus_manage'] = '會員餘額管理';
$_LANG['account_manage'] = '會員賬戶管理';
$_LANG['wholesale_user_manage'] = '批發會員管理';

//權限管理部分的權限
$_LANG['admin_manage'] = '管理員添加/編輯';
$_LANG['admin_drop'] = '刪除管理員';
$_LANG['allot_priv'] = '分派權限';
$_LANG['logs_manage'] = '管理日誌列表';
$_LANG['logs_drop'] = '刪除管理日誌';
$_LANG['template_manage'] = '模板管理';
$_LANG['agency_manage'] = '辦事處管理';
$_LANG['suppliers_manage'] = '供貨商管理';
$_LANG['role_manage'] = '角色管理';
$_LANG[Yoho\cms\Controller\TaskController::ACTION_CODE_ADVANCED_STATUS] = '進階工作設定(Resolve/Archive)';
$_LANG[Yoho\cms\Controller\TaskController::ACTION_CODE_ADVANCED_STATUS.'_alt'] = implode("<br>", ['可以將工作設為「已完成」或「封存」']);
$_LANG[Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_SUPERADMIN] = 'Super Admin';
$_LANG[Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_SUPERADMIN.'_alt'] = implode("<br>", ['SESSION manage_cost 「超級管理員」','- 查看完整會員列表','- 打印及查看完整採購訂單資料(其他使用者則受限制)']);
$_LANG[Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_PREORDER_OP] = '預售登記:<br>可被指派為「操作員」';
$_LANG[Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_PREORDER_OP.'_alt'] = '於Dropdown中顯示管理員名稱';
$_LANG[Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_STAFF_PURCHASE] = '員工優惠審核';
//系統設置部分權限
$_LANG['shop_config'] = '商店設置';
$_LANG['shop_authorized'] = '授權證書';
$_LANG['webcollect_manage'] = '網羅天下管理';
$_LANG['ship_manage'] = '配送方式管理';
$_LANG['payment'] = '支付方式管理';
$_LANG['logistics'] = '物流公司列表管理';
$_LANG['shiparea_manage'] = '配送區域管理';
$_LANG['area_manage'] = '地區列表管理';
$_LANG['friendlink'] = '友情鏈接管理';
$_LANG['db_backup'] = '數據庫備份';
$_LANG['db_renew'] = '數據庫恢復';
$_LANG['flash_manage'] = '首頁主廣告管理'; //Flash 播放器管理
$_LANG['navigator'] = '自定義導航欄';
$_LANG['holidays_update'] = '更新公眾假期列表';
$_LANG['cron'] = '計劃任務';
$_LANG['affiliate'] = '會員推薦計劃設置';
$_LANG['affiliate_ck'] = '會員推薦回贈管理';
$_LANG['sitemap'] = '站點地圖管理';
$_LANG['file_check'] = '文件校驗';
$_LANG['file_priv'] = '文件權限檢驗';
$_LANG['reg_fields'] = '會員註冊項管理';


//訂單管理部分權限
$_LANG['order_os_edit'] = '編輯訂單狀態';
$_LANG['order_ps_edit'] = '編輯付款狀態';
$_LANG['order_ss_edit'] = '編輯出貨狀態';
$_LANG['order_edit'] = '添加編輯訂單';
$_LANG['order_view'] = '查看未完成訂單';
$_LANG['order_view_finished'] = '查看已完成訂單';
$_LANG['repay_manage'] = '退款申請管理';
$_LANG['booking'] = '缺貨登記管理';
$_LANG['sale_order_stats'] = '訂單銷售統計';
$_LANG['client_flow_stats'] = '客戶流量統計';
$_LANG['delivery_view'] = '查看物流單';
$_LANG['back_view'] = '查看退貨單';
$_LANG['order_pay'] = '訂單付款';
$_LANG['order_unpay'] = '訂單未付款';
$_LANG['order_cancel'] = '訂單取消';
$_LANG['order_refund'] = '訂單退款';
$_LANG['order_remove'] = '訂單移除';
$_LANG['order_after_service'] = '訂單售後服務';
$_LANG['order_unhold'] = '訂單允許出貨';
$_LANG['order_fraud_detail'] = '信用卡盜用風險';
$_LANG[Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_ORDERMSG] = '訂單信息:<br>可被指派為「操作員」';
$_LANG[Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_ORDERMSG.'_alt'] = '於Dropdown中顯示管理員名稱';
$_LANG[Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_OUTOFSTOCK_FIRSTOP] = '缺貨登記:<br>可被指派為「首次操作員」';
$_LANG[Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_OUTOFSTOCK_FIRSTOP.'_alt'] = '於Dropdown中顯示管理員名稱';
$_LANG[Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_OUTOFSTOCK_RECOP] = '缺貨登記:<br>可被指派為「到貨操作員」';
$_LANG[Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_OUTOFSTOCK_RECOP.'_alt'] = '於Dropdown中顯示管理員名稱';

//促銷管理
$_LANG['snatch_manage'] = '奪寶奇兵';
$_LANG['bonus_manage'] = '優惠券管理';
$_LANG['card_manage'] = '祝福賀卡';
$_LANG['pack'] = '產品包裝';
$_LANG['ad_manage'] = '廣告管理';
$_LANG['gift_manage'] = '贈品管理';
$_LANG['auction'] = '拍賣活動';
$_LANG['group_by'] = '團購活動';
$_LANG['favourable'] = '贈品/換購管理';
$_LANG['flashdeal_events'] = '閃購管理';
$_LANG['whole_sale'] = '批發管理';
$_LANG['package_manage'] = '套裝優惠';
$_LANG['exchange_goods'] = '積分商城產品';
$_LANG['flashdeal_events_on_off'] = '閃購活動開關';
$_LANG['favourable_on_off'] = '贈品/換購開關';
$_LANG['package_on_off'] = '套裝優惠開關';

//郵件管理
$_LANG['attention_list'] = '關注管理';
$_LANG['email_list'] = '郵件訂閱管理';
$_LANG['magazine_list'] = '雜誌管理';
$_LANG['view_sendlist'] = '郵件隊列管理';

//模板管理
$_LANG['template_select'] = '模板選擇';
$_LANG['template_setup']  = '模板設置';
$_LANG['library_manage']  = '庫項目管理';
$_LANG['lang_edit']       = '語言項編輯';
$_LANG['backup_setting']  = '模板設置備份';
$_LANG['mail_template']  = '郵件模板管理';

//數據庫管理
$_LANG['db_backup']    = '數據備份';
$_LANG['db_renew']     = '數據恢復';
$_LANG['db_optimize']  = '數據表優化';
$_LANG['sql_query']    = 'SQL查詢';
$_LANG['convert']      = '轉換數據';

//短信管理
$_LANG['my_info']         = '賬號信息';
$_LANG['sms_send']        = '發送短信';
$_LANG['sms_charge']      = '短信充值';
$_LANG['send_history']    = '發送記錄';
$_LANG['charge_history']  = '充值記錄 ';

//danger zone
$_LANG['danger_zone']   = 'Danger Zone™';
$_LANG['danger_zone_base']   = '發送電郵';
$_LANG['danger_zone_edit']   = '更改資料';
$_LANG['danger_zone_admin']   = '重整系統';
$_LANG['edit_perma_link'] = '編輯永久鏈結';
$_LANG['goal_setting'] = 'KPI輔助';
$_LANG['team_kpi'] = '團隊KPI';
//選單項目
$_LANG['menu_access'] = '選單項目';
$_LANG['menu_01_goods_list'] = $_LANG['01_goods_list'];
$_LANG['menu_goal_setting'] = 'KPI輔助';
$_LANG['menu_team_kpi'] = '團隊KPI';
$_LANG['menu_02_goods_add'] = $_LANG['02_goods_add'];
$_LANG['menu_03_category_list'] = $_LANG['03_category_list'];
$_LANG['menu_05_comment_manage'] = $_LANG['05_comment_manage'];
$_LANG['menu_06_goods_brand_list'] = $_LANG['06_goods_brand_list'];
$_LANG['menu_08_goods_type'] = $_LANG['08_goods_type'];
$_LANG['menu_08_attribute'] = $_LANG['08_attribute'];
$_LANG['menu_09_logistics_list'] = $_LANG['logistics'];
$_LANG['menu_11_goods_trash'] = $_LANG['11_goods_trash'];
$_LANG['menu_12_batch_pic'] = $_LANG['12_batch_pic'];
$_LANG['menu_13_batch_add'] = $_LANG['13_batch_add'];
$_LANG['menu_14_goods_export'] = $_LANG['14_goods_export'];
$_LANG['menu_15_batch_edit'] = $_LANG['15_batch_edit'];
$_LANG['menu_16_goods_script'] = $_LANG['16_goods_script'];
$_LANG['menu_17_tag_manage'] = $_LANG['17_tag_manage'];
$_LANG['menu_50_virtual_card_list'] = $_LANG['50_virtual_card_list'];
$_LANG['menu_51_virtual_card_add'] = $_LANG['51_virtual_card_add'];
$_LANG['menu_52_virtual_card_change'] = $_LANG['52_virtual_card_change'];
$_LANG['menu_goods_auto'] = $_LANG['goods_auto'];
$_LANG['menu_goods_cost_log'] = $_LANG['goods_cost_log'];
$_LANG['menu_goods_price_log'] = $_LANG['goods_price_log'];
$_LANG['menu_warranty_templates'] = $_LANG['warranty_templates'];
$_LANG['menu_shipping_templates'] = $_LANG['shipping_templates'];
$_LANG['menu_auto_pricing'] = $_LANG['auto_pricing'];
$_LANG['menu_02_book'] = $_LANG['02_book'];
//$_LANG['menu_02_book_alt'] = '相關權限:'.$_LANG['comment_priv'];
$_LANG['menu_02_dai'] = $_LANG['02_dai'];
//$_LANG['menu_02_dai_alt'] = '相關權限:'.$_LANG['comment_priv'];
$_LANG['menu_031_plans'] = $_LANG['031_plans'];
$_LANG['menu_03_taskes'] = $_LANG['03_taskes'];
$_LANG['menu_04_redmine'] = $_LANG['04_redmine'];
$_LANG['menu_05_refund_requests'] = $_LANG['05_refund_requests'];
$_LANG[Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_REFUND_PIC] = '退款及積分申請 - 可被指派為「跟進人」';
$_LANG['menu_order_wholesale_delivery'] = $_LANG['order_wholesale_delivery'];

$_LANG['menu_06_pending_receive'] = $_LANG['06_pending_receive'];
$_LANG['menu_07_repair_orders'] = $_LANG['07_repair_orders'];
$_LANG['menu_07_replace_orders'] = $_LANG['07_replace_orders'];
$_LANG['menu_08_goods_feedback_problem'] = $_LANG['08_goods_feedback_problem'];
$_LANG['menu_09_goods_feedback_price'] = $_LANG['09_goods_feedback_price'];
$_LANG['menu_10_abnormal_cost_log'] = $_LANG['10_abnormal_cost_log'];
$_LANG['menu_11_suggested_promotions'] = $_LANG['11_suggested_promotions'];
$_LANG['menu_sales_agent_manage'] = $_LANG['sales_agent_manage'];
$_LANG['menu_deal_log'] = $_LANG['deal_log'];
$_LANG['menu_yahoo_store'] = $_LANG['yahoo_store'];
$_LANG['menu_quotation'] = $_LANG['quotation'];
$_LANG['menu_rsp_record'] = $_LANG['rsp_record'];
$_LANG[Yoho\cms\Controller\HktvmallController::DB_ACTION_CODE_MENU_HKTVMALL] = $_LANG[Yoho\cms\Controller\HktvmallController::DB_ACTION_CODE_HKTVMALL];
$_LANG['menu_staff_purchase'] = $_LANG[Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_STAFF_PURCHASE];
$_LANG['menu_02_snatch_list'] = $_LANG['02_snatch_list'];
$_LANG['menu_04_bonustype_list'] = $_LANG['04_bonustype_list'];
$_LANG['menu_06_pack_list'] = $_LANG['06_pack_list'];
$_LANG['menu_07_card_list'] = $_LANG['07_card_list'];
$_LANG['menu_08_group_buy'] = $_LANG['08_group_buy'];
$_LANG['menu_09_topic'] = $_LANG['09_topic'];
$_LANG['menu_10_auction'] = $_LANG['10_auction'];
$_LANG['menu_12_favourable'] = $_LANG['12_favourable'];
$_LANG['menu_13_wholesale'] = $_LANG['13_wholesale'];
$_LANG['menu_14_package_list'] = $_LANG['14_package_list'];
$_LANG['menu_15_exchange_goods'] = $_LANG['15_exchange_goods'];
$_LANG['menu_16_coupons'] = $_LANG['16_coupons'];
$_LANG['menu_17_flashdeals'] = $_LANG['17_flashdeals'];
$_LANG['menu_flashdeal_events'] = $_LANG['flashdeal_events'];
$_LANG['menu_18_package'] = $_LANG['18_package'];
$_LANG['menu_19_mkt_method'] = $_LANG['19_mkt_method'];
$_LANG['menu_02_order_list'] = $_LANG['02_order_list'];
$_LANG['menu_03_order_query'] = $_LANG['03_order_query'];
$_LANG['menu_04_merge_order'] = $_LANG['04_merge_order'];
$_LANG['menu_05_edit_order_print'] = $_LANG['05_edit_order_print'];
$_LANG['menu_06_undispose_booking'] = $_LANG['06_undispose_booking'];
$_LANG['menu_08_add_order'] = $_LANG['08_add_order'];
$_LANG['menu_09_delivery_order'] = $_LANG['09_delivery_order'];
$_LANG['menu_10_back_order'] = $_LANG['10_back_order'];
$_LANG['menu_ad_position'] = $_LANG['ad_position'];
$_LANG['menu_ad_list'] = $_LANG['ad_list'];
$_LANG['menu_ad_pospromote'] = $_LANG['ad_pospromote'];
$_LANG['menu_ad_campaign'] = $_LANG['ad_campaign'];
$_LANG['menu_homepage_carousel'] = $_LANG['homepage_carousel'];
$_LANG['menu_flow_stats'] = $_LANG['flow_stats'];
$_LANG['menu_searchengine_stats'] = $_LANG['searchengine_stats'];
$_LANG['menu_z_clicks_stats'] = $_LANG['z_clicks_stats'];
$_LANG['menu_report_guest'] = $_LANG['report_guest'];
$_LANG['menu_report_order'] = $_LANG['report_order'];
$_LANG['menu_report_sell'] = $_LANG['report_sell'];
$_LANG['menu_report_kpi'] = $_LANG['report_kpi'];
$_LANG['menu_report_kpi_market'] = $_LANG['report_kpi_market'];
$_LANG['menu_report_logistics'] = $_LANG['report_logistics'];
$_LANG['menu_report_ex_pos'] = $_LANG['report_ex_pos'];
$_LANG['menu_sale_list'] = $_LANG['sale_list'];
$_LANG['menu_sell_stats'] = $_LANG['sell_stats'];
$_LANG['menu_report_users'] = $_LANG['report_users'];
$_LANG['menu_report_order_address'] = $_LANG['report_order_address'];
$_LANG['menu_visit_buy_per'] = $_LANG['visit_buy_per'];
$_LANG['menu_sale_commission'] = $_LANG['sale_commission'];
$_LANG['menu_stock_cost'] = $_LANG['stock_cost'];
$_LANG['menu_stock_cost_brand'] = $_LANG['stock_cost_brand'];
$_LANG['menu_stock_cost_supplier'] = $_LANG['stock_cost_supplier'];
$_LANG['menu_supplier_stock_report'] = $_LANG['supplier_stock_report'];
$_LANG['menu_stock_cost_log'] = $_LANG['stock_cost_log'];
$_LANG['menu_supplier_turnover'] = $_LANG['supplier_turnover'];
$_LANG['menu_warehouse_turnover'] = $_LANG['warehouse_turnover'];
$_LANG['menu_sale_profit'] = $_LANG['sale_profit'];
$_LANG['menu_sale_payment'] = $_LANG['sale_payment'];
$_LANG['menu_review_shop'] = $_LANG['review_shop'];
$_LANG['menu_review_sales'] = $_LANG['review_sales'];
$_LANG['menu_review_cs'] = $_LANG['review_cs'];
$_LANG['menu_employee_evaluation'] = $_LANG['employee_evaluation'];
$_LANG['menu_03_article_list'] = $_LANG['03_article_list'];
$_LANG['menu_02_articlecat_list'] = $_LANG['02_articlecat_list'];
$_LANG['menu_vote_list'] = $_LANG['vote_list'];
$_LANG['menu_article_auto'] = $_LANG['article_auto'];
$_LANG['menu_03_users_list'] = $_LANG['03_users_list'];
$_LANG['menu_04_users_add'] = $_LANG['04_users_add'];
$_LANG['menu_05_user_rank_list'] = $_LANG['05_user_rank_list'];
$_LANG['menu_06_list_integrate'] = $_LANG['06_list_integrate'];
$_LANG['menu_08_unreply_msg'] = $_LANG['08_unreply_msg'];
$_LANG['menu_09_user_account'] = $_LANG['09_user_account'];
$_LANG['menu_10_user_account_manage'] = $_LANG['10_user_account_manage'];
$_LANG['menu_11_user_export'] = $_LANG['11_user_export'];
$_LANG['menu_12_upcoming_birthdays'] = $_LANG['12_upcoming_birthdays'];
$_LANG['menu_13_wishlist'] = $_LANG['13_wishlist'];
$_LANG['menu_account_log'] = $_LANG['account_log'];
$_LANG['menu_admin_logs'] = $_LANG['admin_logs'];
$_LANG['menu_admin_list'] = $_LANG['admin_list'];
$_LANG['menu_admin_role'] = $_LANG['admin_role'];
$_LANG['menu_agency_list'] = $_LANG['agency_list'];
$_LANG['menu_01_shop_config'] = $_LANG['01_shop_config'];
$_LANG['menu_shop_authorized'] = $_LANG['shop_authorized'];
$_LANG['menu_shp_webcollect'] = $_LANG['shp_webcollect'];
$_LANG['menu_02_payment_list'] = $_LANG['02_payment_list'];
$_LANG['menu_03_shipping_list'] = $_LANG['03_shipping_list'];
$_LANG['menu_04_mail_settings'] = $_LANG['04_mail_settings'];
$_LANG['menu_05_area_list'] = $_LANG['05_area_list'];
$_LANG['menu_07_cron_schcron'] = $_LANG['07_cron_schcron'];
$_LANG['menu_08_friendlink_list'] = $_LANG['08_friendlink_list'];
$_LANG['menu_sitemap'] = $_LANG['sitemap'];
$_LANG['menu_check_file_priv'] = $_LANG['check_file_priv'];
$_LANG['menu_captcha_manage'] = $_LANG['captcha_manage'];
$_LANG['menu_ucenter_setup'] = $_LANG['ucenter_setup'];
$_LANG['menu_flashplay'] = $_LANG['flashplay'];
$_LANG['menu_navigator'] = $_LANG['navigator'];
$_LANG['menu_holidays'] = $_LANG['holidays'];
$_LANG['menu_file_check'] = $_LANG['file_check'];
$_LANG['menu_021_reg_fields'] = $_LANG['021_reg_fields'];
$_LANG['menu_02_template_select'] = $_LANG['02_template_select'];
$_LANG['menu_03_template_setup'] = $_LANG['03_template_setup'];
$_LANG['menu_04_template_library'] = $_LANG['04_template_library'];
$_LANG['menu_05_edit_languages'] = $_LANG['05_edit_languages'];
$_LANG['menu_06_template_backup'] = $_LANG['06_template_backup'];
$_LANG['menu_mail_template_manage'] = $_LANG['mail_template_manage'];
$_LANG['menu_02_db_manage'] = $_LANG['02_db_manage'];
$_LANG['menu_03_db_optimize'] = $_LANG['03_db_optimize'];
$_LANG['menu_04_sql_query'] = $_LANG['04_sql_query'];
$_LANG['menu_convert'] = $_LANG['convert'];
$_LANG['menu_03_sms_send'] = $_LANG['03_sms_send'];
$_LANG['menu_affiliate'] = $_LANG['affiliate'];
$_LANG['menu_affiliate_ck'] = $_LANG['affiliate_ck'];
$_LANG['menu_affiliateyoho'] = $_LANG['affiliateyoho'];
$_LANG['menu_affiliateyoho_orders'] = $_LANG['affiliateyoho_orders'];
$_LANG['menu_affiliateyoho_rewards'] = $_LANG['affiliateyoho_rewards'];
$_LANG['menu_email_list'] = $_LANG['email_list'];
$_LANG['menu_magazine_list'] = $_LANG['magazine_list'];
$_LANG['menu_attention_list'] = $_LANG['attention_list'];
$_LANG['menu_view_sendlist'] = $_LANG['view_sendlist'];
$_LANG['menu_mail_sent_log'] = $_LANG['mail_sent_log'];
$_LANG['menu_edm_generator'] = $_LANG['edm_generator'];
$_LANG['menu_erp_supplier_list'] = $_LANG['erp_supplier_list'];
$_LANG['menu_erp_supplier_group'] = $_LANG['erp_supplier_group'];
$_LANG[Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ONLYSELFSUPPLIERS] = '只能存取已分派供應商';
$_LANG[Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ONLYSELFSUPPLIERS.'_alt'] = implode('<br>- ', ['(註1:如不啟用此選項,非Super Admin角色將不能存取任何供應商)','(註2:Super Admin可以存取所有供應商, 並不受此選項限制','相關權限:'.$_LANG['erp_order_manage']]);
$_LANG['menu_erp_warehouse_list'] = $_LANG['erp_warehouse_list'];
$_LANG['menu_erp_warehousing_style_setting'] = $_LANG['erp_warehousing_style_setting'];
$_LANG['menu_erp_delivery_style_setting'] = $_LANG['erp_delivery_style_setting'];
$_LANG['menu_erp_account_setting'] = $_LANG['erp_account_setting'];
$_LANG['menu_erp_order_list'] = $_LANG['erp_order_list'];
$_LANG['menu_erp_order_list_pending_receive'] = $_LANG['erp_order_list_pending_receive'];
$_LANG['menu_erp_goods_warehousing_list'] = $_LANG['erp_goods_warehousing_list'];
$_LANG['menu_erp_goods_delivery_list'] = $_LANG['erp_goods_delivery_list'];
$_LANG['menu_erp_stock_inquiry'] = $_LANG['erp_stock_inquiry'];
$_LANG['menu_erp_goods_warehousing_and_delivery_record'] = $_LANG['erp_goods_warehousing_and_delivery_record'];
$_LANG['menu_erp_stock_check'] = $_LANG['erp_stock_check'];
$_LANG['menu_erp_stock_transfer'] = $_LANG['erp_stock_transfer'];
$_LANG['menu_erp_stock_daily'] = $_LANG['erp_stock_daily'];
$_LANG['menu_erp_account_payable_list'] = $_LANG['erp_account_payable_list'];
$_LANG['menu_erp_account_receivable_list'] = $_LANG['erp_account_receivable_list'];
$_LANG['menu_erp_payment_list'] = $_LANG['erp_payment_list'];
$_LANG['menu_erp_gathering_list'] = $_LANG['erp_gathering_list'];
$_LANG['menu_erp_account_list'] = $_LANG['erp_account_list'];
$_LANG['menu_erp_sales_agent_list'] = $_LANG['erp_sales_agent_list'];
$_LANG['menu_erp_stock_abnormal'] = $_LANG['erp_stock_abnormal'];
$_LANG['menu_actg_account_summary'] = $_LANG['actg_account_summary'];
$_LANG['menu_actg_transactions'] = $_LANG['actg_transactions'];
$_LANG['menu_actg_shop_expenses'] = $_LANG['actg_shop_expenses'];
$_LANG['menu_actg_currency_exchanges'] = $_LANG['actg_currency_exchanges'];
$_LANG['menu_actg_payment_wholesale'] = $_LANG['actg_payment_wholesale'];
$_LANG['menu_actg_payment_purchase'] = $_LANG['actg_payment_purchase'];
$_LANG['menu_actg_wholesale_orders'] = $_LANG['actg_wholesale_orders'];
$_LANG['menu_actg_xaccount_receivable'] = $_LANG['actg_xaccount_receivable'];
$_LANG['menu_actg_xaccount_payable'] = $_LANG['actg_xaccount_payable'];
$_LANG['menu_danger_zone'] = $_LANG['danger_zone'];
$_LANG['menu_trends_keywords'] = $_LANG['trends_keywords'];
$_LANG['menu_balance_sheet'] = $_LANG['balance_sheet'];
$_LANG['menu_credit_note_summary'] = $_LANG['credit_note_summary'];
$_LANG['menu_sale_cross_check'] = $_LANG['sale_cross_check'];
$_LANG['menu_income_statement'] = $_LANG['income_statement'];
$_LANG['menu_cash_flow_statement'] = $_LANG['cash_flow_statement'];
$_LANG['menu_bank_reconciliation'] = $_LANG['bank_reconciliation'];
$_LANG['menu_bank_reconciliation_match'] = $_LANG['bank_reconciliation_match'];
$_LANG['menu_accounting_system'] = $_LANG['accounting_system'];
$_LANG['menu_trial_balance'] = $_LANG['trial_balance'];
$_LANG['menu_order_picker_packer_import'] = $_LANG['order_picker_packer_import'];
$_LANG['menu_wholesale_user_info'] = $_LANG['wholesale_user_info'];
$_LANG['menu_image_compression_usages'] = $_LANG['image_compression_usages'];
$_LANG['menu_order_goods_sn'] = $_LANG['order_goods_sn'];
$_LANG['menu_crm_system'] = $_LANG['crm_system'];
$_LANG['menu_ivr_system'] = $_LANG['ivr_system'];
$_LANG['menu_op_system'] = $_LANG['op_system'];
$_LANG['menu_goods_stats'] = $_LANG['goods_stats'];
$_LANG['menu_order_list_rank_program'] = $_LANG['order_list_rank_program'];
$_LANG['menu_order_progress_report'] = $_LANG['order_progress_report'];
$_LANG['menu_order_ship'] = $_LANG['order_ship'];

$_LANG['menu_image_compression_product'] = $_LANG['image_compression_product'];
$_LANG['menu_rma'] = $_LANG['rma'];
$_LANG['menu_lucky_draw'] = $_LANG['lucky_draw'];
$_LANG['menu_06_user_rank_program'] = $_LANG['user_rank_program'];
$_LANG['menu_write_off'] = $_LANG['write_off'];
$_LANG['menu_rma_report'] = $_LANG['rma_report'];
$_LANG['menu_order_goods_cross_check'] = $_LANG['order_goods_cross_check'];
$_LANG['menu_stock_transfer_cross_check'] = $_LANG['stock_transfer_cross_check'];
$_LANG['menu_order_goods_cross_check_report'] = $_LANG['order_goods_cross_check_report'];
$_LANG['menu_order_express_collected_import'] = $_LANG['order_express_collected_import'];
$_LANG['menu_support_manage'] = $_LANG['support_manage'];
$_LANG['menu_global_alert'] = $_LANG['global_alert'];
$_LANG['menu_shop_restore'] = $_LANG['shop_restore'];
$_LANG['menu_sql_report'] = $_LANG['sql_report'];
$_LANG['menu_goods_shipping_dimension_import'] = $_LANG['goods_shipping_dimension_import'];
$_LANG['menu_wechatpay_force_refund'] = $_LANG['wechatpay_force_refund'];
$_LANG['menu_erp_order_list_pending_payment'] = $_LANG['erp_order_list_pending_payment'];
$_LANG['menu_mkt_method'] = $_LANG['mkt_method'];
$_LANG['menu_esl_operation'] = $_LANG['esl_operation'];
$_LANG['menu_product_commission'] = $_LANG['product_commission'];
$_LANG['menu_sale_cash_check'] = $_LANG['sale_cash_check'];
$_LANG['menu_shop_commission'] = $_LANG['shop_commission'];
$_LANG['menu_shop_salary'] = $_LANG['shop_salary'];
$_LANG['product_commission_select'] = '產品佣金';
$_LANG['product_commission_setting'] = '佣金設置';
$_LANG['commission_statistics'] = '佣金統計';
$_LANG['estimated_sales_manage'] = '預計銷量添加/編輯';
$_LANG['product_commission_update'] = '修改產品佣金';
$_LANG['shop_target_manage'] = '門市目標添加/編輯';

$_LANG[Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_REFUND_APP] = '退款及積分申請 - 可被指派為「申請人」';
$_LANG[Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_PO_OP] = '添加新採購訂單 - 可被指派為「操作員」';
$_LANG[Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_WAREHOUSE_OP] = '入貨訂單 - 可被指派為「操作員」';
$_LANG[Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_PAYMENT_OP] = '會計系統 - 可被指派為「操作員」';
$_LANG['operator_list'] = '操作員列表';
$_LANG[Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_PAYMENT_OP.'_alt'] = '新增交易, 新增轉帳, 採購訂單付款';
$_LANG[Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_POSTING_OP] = '會計系統 - 可被指派為「審核員」';
$_LANG[Yoho\cms\Controller\AdminuserController::DB_ACTION_CODE_ASSIGNABLE_POSTING_OP.'_alt'] = '審核會計紀錄';
?>
