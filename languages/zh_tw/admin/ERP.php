<?php

/* 菜單分類部分 */
$_LANG['05_erp_order_manage'] = '採購訂單';
$_LANG['06_erp_stock_manage'] = '庫存系統';
$_LANG['19_erp_system_manage'] = 'ERP系統設置';
$_LANG['20_erp_finance_manage'] = '財務管理';


/* 採購訂單 */
$_LANG['erp_order_list'] = '採購訂單列表';
$_LANG['erp_order_add'] = '錄入採購訂單';
$_LANG['erp_order_list_pending_receive'] = '即將送貨採購訂單';
$_LANG['erp_order_list_pending_payment'] = "等待付款已送貨採購訂單";


/* 存貨管理 */
$_LANG['erp_goods_list'] = '網店產品列表';
$_LANG['erp_goods_warehousing'] = '產品入貨';
$_LANG['erp_goods_delivery'] = '產品出貨';
$_LANG['erp_goods_dead_stock'] = '呆滯庫存分析';
$_LANG['erp_goods_warehousing_report'] = '產品入貨報表';
$_LANG['erp_goods_delivery_report'] = '產品出貨報表';
$_LANG['erp_goods_warehousing_list'] = '入貨訂單';
$_LANG['erp_goods_delivery_list'] = '出貨訂單';
$_LANG['erp_stock_inquiry'] = '即時存貨查詢';
$_LANG['erp_goods_warehousing_and_delivery_record'] = '出入貨流水帳';
$_LANG['erp_stock_abnormal'] = '庫存異常';
$_LANG['erp_stock_check'] = '存貨盤點';
$_LANG['erp_stock_transfer'] = '貨倉調貨';

$_LANG['erp_stock_daily'] = '存貨變動圖表';

/* 財務管理 */
$_LANG['erp_account_payable_list'] = '應付帳款列表';
$_LANG['erp_account_receivable_list'] = '應收帳款列表';

$_LANG['erp_payment_list'] = '付款單列表';
$_LANG['erp_gathering_list'] = '收款單列表';
$_LANG['erp_account_list'] = '銀行帳號列表';


/* 系統設置 */
$_LANG['erp_supplier_list'] = '供應商列表';
$_LANG['erp_supplier_group'] = '供應商類別';
$_LANG['erp_warehouse_setting'] = '貨倉設置';
$_LANG['erp_warehousing_style_setting'] = '入貨類型設置';
$_LANG['erp_delivery_style_setting'] = '出貨類型設置';
$_LANG['erp_account_setting'] = '銀行帳號設置';
$_LANG['erp_payment_style_setting'] = '付款類型設置';
$_LANG['erp_goods_barcode_setting'] = '產品條碼設置';

/* 通用*/
$_LANG['erp_yes'] = '是';
$_LANG['erp_no'] = '否';
$_LANG['erp_sure_to_delete'] = '您確定要刪除嗎？';
$_LANG['erp_required_item'] = '（必填）';
$_LANG['erp_is_valid'] = '是否有效';
$_LANG['erp_operation']='操作';

$_LANG['erp_operation_delete'] = '刪除';
$_LANG['erp_operation_edit'] = '編輯';
$_LANG['erp_operation_view'] = '查看';
$_LANG['erp_operation_print'] = '打印';
$_LANG['erp_operation_post'] = '提交';

$_LANG['erp_operation_withdrawal']='退回';

$_LANG['erp_operation_rate'] = '定價';
$_LANG['erp_operation_approve'] = '審核';

$_LANG['erp_operation_post_to_finance'] = '提交財務';
$_LANG['erp_operation_post_to_approve'] = '提交審核';

$_LANG['erp_operation_approve_pass'] = '通過審核';
$_LANG['erp_operation_approve_reject'] = '拒絕審核';

$_LANG['erp_operation_send_email'] = '傳送收據';
$_LANG['erp_operation_check_receipt'] = '查看收據';

$_LANG['erp_invalid_img'] = '您上傳的圖片格式不正確！';
$_LANG['erp_img_too_big'] = '圖片文件太大了（最大值：%s），無法上傳。';
$_LANG['erp_no_permit']='對不起，您沒有進行此操作的權限！';
$_LANG['erp_not_a_number']='請輸入數字！';
$_LANG['erp_wrong_parameter']='參數錯誤！';
$_LANG['erp_retun_to_center']='返回到管理中心';
$_LANG['erp_sum']='合計：';

$_LANG['erp_admin']='負責人';
$_LANG['erp_id']='序號';

$_LANG['erp_NULL']='EMPTY';

$_LANG['erp_operation_withdrawal']='退回';
$_LANG['search_keyword']='關鍵字';
$_LANG['all_record']='所有記錄';

$_LANG['erp_btn_ok']='確定';

/*供應商分類*/
$_LANG['erp_add_supplier_group']='添加供應商分類';
$_LANG['erp_edit_supplier_group']='編輯供應商分類';
$_LANG['erp_supplier_group_name']='供應商分類';
$_LANG['erp_supplier_admin']='供應商負責人';
$_LANG['erp_supplier_group_suppliers_count']='供應商數量';
$_LANG['erp_supplier_group_has_supplier']='此供應商分類下尚有供應商，不能刪除。';
$_LANG['erp_supplier_group_sure_to_delete']='您確定要刪除此供應商分類嗎？';
$_LANG['erp_supplier_group_name_required'] = "請輸入供應商分類名稱。";

$_LANG['erp_supplier_group_edit_success']='成功更改供應商分類';
$_LANG['erp_supplier_group_add_success']='成功添加供應商分類';
$_LANG['erp_supplier_group_return']='返回到供應商分類列表';


/* 供應商列表 */
$_LANG['erp_add_supplier'] = '添加供應商';
$_LANG['erp_edit_supplier'] = '編輯供應商';
$_LANG['erp_supplier_id'] = '序號';
$_LANG['erp_supplier_code'] = '編號';
$_LANG['erp_supplier_address'] = '地址';
$_LANG['erp_supplier_name'] = '名稱';
$_LANG['erp_supplier_contact'] = '聯繫人';
$_LANG['erp_supplier_tel'] = '電話';
$_LANG['erp_supplier_fax'] = '傳真';
$_LANG['erp_supplier_qq'] = 'QQ';
$_LANG['erp_supplier_msn'] = 'MSN';
$_LANG['erp_supplier_bank_name'] = '銀行名稱';
$_LANG['erp_supplier_bank_account'] = '戶口號碼';
$_LANG['erp_supplier_bank_contact'] = '對方會計部電郵';
$_LANG['erp_supplier_code_exist'] = "供應商編碼重複，請重新輸入！";
$_LANG['erp_supplier_code_required'] = "請輸入供應商編碼。";
$_LANG['erp_supplier_name_required'] = "請輸入供應商名稱。";
$_LANG['erp_supplier_not_exist'] = "此供應商不存在或已被刪除。";
$_LANG['erp_supplier_search_code'] = '供應商編號';
$_LANG['erp_supplier_has_order'] = '此供應商有關聯的訂單，不能刪除。';
$_LANG['erp_supplier_has_goods'] = '此供應商有關聯的產品，不能刪除。';

$_LANG['erp_supplier_edit_success']='成功更改供應商';
$_LANG['erp_supplier_add_success']='成功添加供應商';
$_LANG['erp_supplier_return'] = '返回到供應商列表';
$_LANG['erp_supplier_goods_count'] = '產品數量';
$_LANG['erp_supplier_order_count'] = '訂單數量';
$_LANG['erp_supplier_payment_term'] = '付款條件';
$_LANG['erp_supplier_payment_term_month_statement'] = "月結";
$_LANG['erp_supplier_payment_term_7_day'] = "7日";
$_LANG['erp_supplier_payment_term_immediate'] = "立即";
$_LANG['erp_supplier_payment_corresponding_company'] = "入賬公司";

/* 採購訂單 */
$_LANG['erp_add_order'] = '添加新定單';

$_LANG['erp_edit_order'] = '編輯定單';
$_LANG['erp_view_order'] = '查看定單';
$_LANG['erp_rate_order'] = '定單定價';
$_LANG['erp_approve_order'] = '審核定單';

$_LANG['erp_order_return'] = '返回到採購訂單列表';
$_LANG['erp_order_no_access'] = '不能編輯其他採購員建立的訂單';

$_LANG['erp_order_no_acts'] = '該訂單狀態不能進行此操作';

$_LANG['erp_order_select_supplier_advance'] = '請先選擇供應商';

$_LANG['erp_operation_change_attr'] = '屬性';

$_LANG['erp_order_id'] = '序號';
$_LANG['erp_order_sn'] = '編號';
$_LANG['erp_order_description'] = '描述';
$_LANG['erp_order_type'] = '類別';
$_LANG['erp_order_supplier'] = '供應商';
$_LANG['erp_order_purchaser'] = '採購員';
$_LANG['erp_order_agency'] = '辦事處';
$_LANG['erp_order_date'] = '訂單日期';

$_LANG['erp_order_item_id'] = '序號';
$_LANG['erp_order_item_goods_sn'] = 'Barcode';
$_LANG['erp_order_item_goods_name'] = '產品名稱';
$_LANG['erp_order_item_goods_img'] = '圖片';
$_LANG['erp_order_item_order_qty'] = '定購數量';
$_LANG['erp_order_item_goods_attr'] = '產品屬性';
$_LANG['erp_order_item_order_price'] = '定購價格';
$_LANG['erp_order_item_order_shipping_price'] = '運費';
$_LANG["erp_order_total_shipping_price"] = '總運費';
$_LANG['erp_order_item_order_amount'] = '總價';
$_LANG['erp_order_client_name'] = '姓名';
$_LANG['erp_order_client_tel'] = '電話';
$_LANG['erp_order_client_mobile'] = '手機';
$_LANG['erp_order_client_address'] = '地址';
$_LANG['erp_order_delievery_agent_details'] = '送貨资料';
$_LANG["erp_print_expected_delievery_date"] = '預計送貨日期：';

$_LANG['erp_order_goods_sn'] ='貨號：';
$_LANG['erp_order_goods_name'] ='名稱：';
$_LANG['erp_order_input_qty'] ='數量：';

$_LANG['erp_order_goods_sn_required'] ='請輸入貨號。';
$_LANG['erp_order_goods_name_required'] ='請輸入名稱。';
$_LANG['erp_order_goods_sn_or_goods_name_required'] ='請輸入貨號或產品名稱。';

$_LANG['erp_order_wrong_goods_sn'] ='請核對輸入的產品貨號。';
$_LANG['erp_order_wrong_goods_name'] ='請核對輸入的產品名稱。';

$_LANG['erp_order_wrong_goods_sn_and_goods_name'] ='請核對輸入的產品貨號或產品名稱。';

$_LANG['erp_order_status'] = '訂單狀態';
$_LANG['erp_order_status_inputing'] = '訂單錄入中';//order_status=1
$_LANG['erp_order_status_rating'] = '等待財務處理';//order_status=2
$_LANG['erp_order_status_approving'] = '等待主管審核訂單';//order_status=3
$_LANG['erp_order_status_approve_pass'] = '審核已通過';//order_status=4
$_LANG['erp_order_status_approve_reject'] = '審核未通過';//order_status=5

$_LANG['erp_order_search_order_status'] = '訂單狀態';
$_LANG['erp_order_search_supplier'] = '所有供應商';
$_LANG['erp_order_search_purchaser'] = '所有採購員';
$_LANG['erp_order_search_type'] = '所有類別';
$_LANG['erp_order_search_start_date'] = '起始日期';
$_LANG['erp_order_search_end_date'] = '結束日期';
$_LANG['erp_order_search_order_sn'] = '訂單編號';
$_LANG['erp_order_search_good_keyword'] = '產品關鍵字(編號/貨號/名稱)';

$_LANG['erp_order_operation_view_details'] = '要求';
$_LANG['erp_order_operation_view_approve_remark'] = '審核備註';

$_LANG['erp_order_no_admin_supplier'] = '管理員尚未給您分配供應商，不能新建採購訂單。';
$_LANG['erp_order_description_required'] = '訂單描述不得留空！';
$_LANG['erp_order_supplier_code_required'] = '請輸入工廠編號！';

$_LANG['erp_order_no_agency_access'] = "不能查看、編輯其他辦事處的採購單";

$_LANG['erp_order_supplier_not_exist'] = "該供應商不存在、無效或已被刪除";
$_LANG['erp_order_select_supplier'] = "請選擇";

$_LANG['erp_order_goods_without_attr'] = '此產品沒有可選屬性。';
$_LANG['erp_order_attr_not_select'] = '請選擇產品屬性。';

$_LANG['erp_order_sure_to_change_order_supplier'] = "您確定要更改供應商嗎？";
$_LANG['erp_order_sure_to_change_order_description'] = "您確定要更改訂單描述嗎？";
$_LANG['erp_order_sure_to_delete_order'] = "您確定要刪除此訂單嗎？";
$_LANG['erp_order_sure_to_change_goods_sn'] = "您確定要更改產品貨號嗎？";
$_LANG['erp_order_sure_to_change_order_qty'] = "您確定要更改訂單數量嗎？";
$_LANG['erp_order_sure_to_delete_order_item'] = "您確定要刪除此訂單項嗎？";
$_LANG['erp_order_sure_to_post_order'] = "您確定要提交此訂單給財務嗎？";

$_LANG['erp_order_sure_to_withdrawal_to_edit'] = "您確定要退回此訂單嗎？";
$_LANG['erp_order_sure_to_withdrawal_to_rate'] = "您確定要退回此訂單嗎？";

$_LANG['erp_order_sure_to_change_specs'] = "您確定要更改訂單要求嗎？";
$_LANG['erp_order_sure_to_change_price'] = "您確定要更改價格嗎？";
$_LANG['erp_order_sure_to_post_to_approve'] = "您確定要提交訂單給主管審核嗎？";
$_LANG['erp_order_sure_to_approve'] = "您確定要提交審核嗎？";

$_LANG['erp_order_no_accessibility']='其他用戶正在編輯此訂單,不能進行此項操作！';
$_LANG['erp_order_return_to_order_list']='返回到採購訂單列表。';

$_LANG['erp_order_not_admin_goods']='您沒有此產品的管理權。';
$_LANG['erp_order_not_supplier_goods']='此產品不屬於您所選的供應商。';

$_LANG['erp_order_select_goods_in_advance']='請先選擇產品。';

$_LANG['erp_order_not_exist']='此訂單不存在或已被刪除。';
$_LANG['erp_order_item_not_exist']='此訂單項不存在或已被刪除。';
$_LANG['erp_order_item_attr_not_exist']='此產品屬性不存在或已被刪除。';
$_LANG['erp_order_goods_not_exist']='此產品不存在或已被刪除。';
$_LANG['erp_credit_note_not_exist']='此Credit Note不存在或沒有餘額。';
$_LANG['erp_order_view_order']='查看此訂單';
$_LANG['erp_order_delete_sucess']='成功刪除訂單。';
$_LANG['erp_order_post_success']='成功提交訂單。';
$_LANG['erp_order_approve_success']='成功提交審核結果。';

$_LANG['erp_order_post_failed']='提交訂單出錯。';

$_LANG['erp_order_not_completed']="此訂單尚未完善，不能提交,需要檢查的項目有：
 * 1，是否選擇了供應商
 * 2，是否最少含一條訂單項
 * 3，是否所有訂單項都選擇了產品
 * 4，有屬性的產品是否都選了屬性
 * 5，每個訂單項是否都輸入了定購數量";

$_LANG['erp_order_no_order_item']='此訂單無訂單項，請完善定單後再提交。';
$_LANG['erp_order_no_supplier']='未選擇供應商，不能提交。';
$_LANG['erp_order_no_goods']='最少有一個訂單項未輸入產品貨號，不能提交。';
$_LANG['erp_order_no_goods_attr']='最少有一個訂單項尚未選擇產品屬性，不能提交。';
$_LANG['erp_order_no_goods_qty']='最少有一個訂單項尚未輸入定購數量，不能提交。';
$_LANG['erp_order_no_img']='無圖片';
$_LANG['erp_order_input_goods_sn']='輸入貨號';

$_LANG['erp_order_specific']='說明及要求';

$_LANG['erp_approve_remark'] = '審核備註:';

$_LANG['erp_order_sum_total']='總計：';
$_LANG['erp_order_qty_unit']='件';
$_LANG['erp_order_amount_unit']='元';

$_LANG['erp_order_create_success']='成功建立%s個採購訂單。';

$_LANG['erp_order_view_list']='查看採購訂單列表。';

$_LANG['erp_order_back']='返回到訂單列表';

/* 打印採購訂單 */
$_LANG['erp_print_order_title']='採購訂單';
$_LANG['erp_print_order_sn']='採購訂單編號：';
$_LANG['erp_print_create_date']='下單日期：';
$_LANG['erp_print_order_supplier_name']='供應商名稱：';
$_LANG['erp_print_order_user1']='制單：';
$_LANG['erp_print_order_user2']='財務：';
$_LANG['erp_print_order_user3']='審核：';
$_LANG['erp_print_order_user4']='供應商簽字：';

/* 貨倉設置 */
$_LANG['erp_add_warehouse']='添加貨倉';
$_LANG['erp_warehouse_list']='貨倉列表';
$_LANG['erp_warehouse_id'] = '序號';
$_LANG['erp_warehouse_name'] = '名稱';
$_LANG['erp_warehouse_description'] = '描述';
$_LANG['erp_warehouse_administer'] = '倉管';
$_LANG['erp_warehouse_delivery_number']='出貨單數';
$_LANG['erp_warehouse_warehousing_number']='入貨單數';
$_LANG['erp_warehouse_return'] = '返回到貨倉列表';
$_LANG['erp_edit_warehouse'] = '編輯貨倉';
$_LANG['erp_warehouse_edit_success'] = '成功更改貨倉';
$_LANG['erp_warehouse_add_success'] = '成功添加貨倉';
$_LANG['erp_warehouse_has_warehousing'] ='此貨倉有相關聯的入貨單，不能刪除。';
$_LANG['erp_warehouse_has_delivery'] ='此貨倉有相關聯的出貨單，不能刪除。';

$_LANG['erp_warehouse_has_stock'] ='此貨倉下尚有庫存，不能刪除。';

$_LANG['erp_warehousing_style_has_warehousing'] ='此入貨類型有相關聯的入貨單，不能刪除。';
$_LANG['erp_delivery_style_has_delivery'] ='此出貨類型有相關聯的出貨單，不能刪除。';
$_LANG['erp_warehouse_not_exist'] ='貨倉不存在或已被刪除！';
$_LANG['erp_warehouse_sure_to_change_description'] = "您確定要更改貨倉描述嗎？";
$_LANG['erp_warehouse_description_required'] = "貨倉描述不得留空，請重新輸入！";
$_LANG['erp_warehouse_administer_required'] ='倉管不得留空，請重新輸入！';
$_LANG['erp_warehouse_name_required'] ='倉管名不得留空，請重新輸入！';
$_LANG['erp_warehouse_sure_to_change_administer'] = "您確定要更改貨倉管理人嗎？";

$_LANG['erp_warehouse_agency'] ='所屬辦事處';

$_LANG['erp_warehouse_select_agency'] ='選擇辦事處';

$_LANG['erp_warehouse_is_on_sale'] = '是否可銷售';
$_LANG['erp_warehouse_is_on_sale_tips'] = '可銷售是可用於銷售的貨倉，用戶在下訂單的時候，會檢查該貨倉的庫存';

/* 出入貨類型 */
$_LANG['erp_warehousing_style_list']='入貨類型列表';
$_LANG['erp_add_warehousing_style']='添加入貨類型';
$_LANG['erp_warehousing_style_id'] = '序號';
$_LANG['erp_warehousing_style'] = '入貨類型';
$_LANG['erp_warehousing_style_not_exist'] ='入貨類型不存在或已被刪除！';
$_LANG['erp_warehousing_style_required'] ='入貨類型不得留空，請重新輸入！';

$_LANG['erp_warehousing_style_return'] = '返回到入貨類型列表';
$_LANG['erp_warehousing_style_add_success'] = '成功添加入貨類型';
$_LANG['erp_warehousing_style_edit_success'] = '成功更改入貨類型';
$_LANG['erp_edit_warehousing_style'] = '編輯入貨類型';

$_LANG['erp_warehousing_style_preserved'] = '系統保留入貨類型，不能編輯';

$_LANG['erp_delivery_style_list']='出貨類型列表';
$_LANG['erp_add_delivery_style']='添加出貨類型';
$_LANG['erp_delivery_style_id'] = '序號';
$_LANG['erp_delivery_style'] = '出貨類型';
$_LANG['erp_delivery_style_not_exist'] ='出貨類型不存在或已被刪除！';
$_LANG['erp_delivery_style_required'] ='出貨類型不得留空，請重新輸入！';
$_LANG['erp_delivery_without_valid_warehouse'] ='管理員尚未給您分配貨倉，不能新建出貨單。';

$_LANG['erp_delivery_add_failed'] ='建立出貨單發生錯誤';

$_LANG['erp_delivery_auto_on_ship'] = '出貨自動扣除庫存';

$_LANG['erp_delivery_auto_on_ship_failed'] = '出貨自動扣除庫存失敗';

$_LANG['erp_delivery_style_return'] = '返回到出貨類型列表';
$_LANG['erp_delivery_style_add_success'] = '成功添加出貨類型';
$_LANG['erp_delivery_style_edit_success'] = '成功更改出貨類型';
$_LANG['erp_edit_delivery_style'] = '編輯出貨類型';

/* 入貨單 */
$_LANG['erp_warehousing_id'] = '序號';
$_LANG['erp_warehousing_sn'] ='入貨單號';
$_LANG['erp_warehousing_warehouse'] ='收貨貨倉';
$_LANG['erp_warehousing_agency'] ='辦事處';
$_LANG['erp_warehousing_warehousing_style'] ='入貨類型';
$_LANG['erp_warehousing_order'] ='關聯訂單號';
$_LANG['erp_warehousing_date'] ='入貨日期';
$_LANG['erp_warehousing_from'] ='交貨人';
$_LANG['erp_warehousing_admin'] ='倉管';
$_LANG['erp_warehousing_status'] ='入貨單狀態';

$_LANG['erp_warehousing_start_date'] ='起始日期';
$_LANG['erp_warehousing_end_date'] ='結束日期';

$_LANG['erp_warehousing_no_access'] = '不能編輯其他倉管員建立的入貨單';

$_LANG['erp_warehouse_no_access'] = '沒有權限對此貨倉進行出入貨操作';

$_LANG['erp_warehousing_status_inputing'] = '入貨單錄入中';//order_status=1
$_LANG['erp_warehousing_status_rating'] = '財務處理中';//order_status=2
$_LANG['erp_warehousing_status_approving'] = '主管審核中';//order_status=3
$_LANG['erp_warehousing_status_approve_pass'] = '審核已通過';//order_status=4
$_LANG['erp_warehousing_status_approve_reject'] = '審核未通過';//order_status=5

$_LANG['erp_warehousing_operation_approve_remark'] = '審核備註';

$_LANG['erp_warehousing_item_id'] = '序號';
$_LANG['erp_warehousing_item_goods_sn'] ='Barcode';
$_LANG['erp_warehousing_item_goods_img'] ='圖片';
$_LANG['erp_warehousing_item_goods_attr'] ='產品屬性';
$_LANG['erp_warehousing_item_expected_qty'] ='應收數量';
$_LANG['erp_warehousing_item_received_qty'] ='實收數量';
$_LANG['erp_warehousing_item_remark'] ='備註';
$_LANG['erp_warehousing_item_goods_barcode'] ='條碼';

$_LANG['erp_warehousing_item'] ='入貨單項';

$_LANG['erp_warehousing_select_warehouse'] ='請選擇貨倉';
$_LANG['erp_warehousing_item_not_exist'] ='入貨單項不存在或已被刪除！';
$_LANG['erp_warehousing_choose_order'] ='選擇訂單';

$_LANG['erp_warehousing_no_acts'] = '該入貨單狀態不能進行此操作';

$_LANG['erp_warehousing_no_agency_access'] = '不能查看、編輯其他辦事處的入貨單';

$_LANG['erp_warehousing_not_exist'] ='入貨單不存在或已被刪除！';
$_LANG['erp_warehousing_no_accessibility']='其他用戶正在編輯此入貨單,不能進行此項操作！';
$_LANG['erp_warehousing_return_to_warehousing_list']='返回到入貨訂單';

$_LANG['erp_warehousing_approve_pass_failed'] ='入貨單審核失敗';

$_LANG['erp_warehousing_view_warehousing']='查看入貨單';
$_LANG['erp_warehousing_edit_warehousing']='編輯入貨單';
$_LANG['erp_warehousing_approve_warehousing']='審核入貨單';

$_LANG['erp_warehousing_not_completed']="此入貨單尚未完善，不能提交，需要檢查的項目有：
 * 1，如果是關聯採購訂單入貨，是否已經選擇了訂單
 * 2，是否輸入了交貨人
 * 3，是否最少有一個入貨單項
 * 4，是否每個入貨單項都有入貨數量";

$_LANG['erp_warehousing_sure_to_withdrawal_to_edit'] = "您確定要退回此入貨單嗎？";

$_LANG['erp_warehousing_sure_to_change_warehouse'] ='您確定要更改收貨貨倉嗎？';
$_LANG['erp_warehousing_sure_to_change_warehousing_style'] ='您確定要更改入貨類型嗎？';
$_LANG['erp_warehousing_sure_to_change_order'] ='您確定要更改關聯訂單嗎？';
$_LANG['erp_warehousing_sure_to_change_warehousing_from'] ='您確定要更改交貨人嗎？';
$_LANG['erp_warehousing_sure_to_post_warehousing'] ='您確定要提交入貨單給主管審核嗎？';
$_LANG['erp_warehousing_sure_to_post_approve'] ='您確定要提交審核嗎？';
$_LANG['erp_warehousing_sure_to_change_received_qty'] ='您確定要更改實收數量嗎？';
$_LANG['erp_warehousing_sure_to_change_price'] ='您確定要更改入貨價格嗎？';
$_LANG['erp_warehousing_sure_to_delete_warehousing_item'] ='您確定要刪除此入貨單項嗎？';
$_LANG['erp_warehousing_sure_to_delete_warehousing'] ='您確定要刪除此入貨單嗎？';

$_LANG['erp_warehousing_warehousing_from_required'] ='請填寫交貨人';

$_LANG['erp_warehousing_input_barcode'] ='條碼：';
$_LANG['erp_warehousing_input_qty'] ='數量：';
$_LANG['erp_warehousing_input_warehousing_from'] ='輸入交貨人';

$_LANG['erp_warehousing_import_warehousing'] ='導入數據：';

$_LANG['erp_warehousing_input_goods_sn'] ='貨號：';
$_LANG['erp_warehousing_input_goods_name'] ='名稱：';
$_LANG['erp_warehousing_input_goods_barcode'] ='條碼：';

$_LANG['erp_warehousing_goods_sn_required'] ='請輸入貨號。';
$_LANG['erp_warehousing_goods_sn_or_goods_name_or_goods_barcode_required'] ='請輸入條碼、貨號或產品名稱。';

$_LANG['erp_warehousing_wrong_goods_sn'] ='請核對輸入的產品貨號。';
$_LANG['erp_warehousing_wrong_goods_name'] ='請核對輸入的產品名稱。';
$_LANG['erp_warehousing_item_warehousing_price'] ='入貨價格';
$_LANG['erp_warehousing_wrong_goods_barcode'] ='請核對輸入的產品條碼。';

$_LANG['erp_warehousing_without_choosing_order'] ='請選擇關聯訂單號！';
$_LANG['erp_warehousing_without_warehousing_from'] ='請輸入交貨人！';
$_LANG['erp_warehousing_without_warehousing_item'] ='此入貨單無入貨單項，不能提交！';
$_LANG['erp_warehousing_without_warehousing_qty'] ='最少有一條入貨單項未輸入實收數，不能提交！';
$_LANG['erp_warehousing_only_text_file'] ='文件類型錯誤，正確的文件應當是以"txt"為擴展名的文本文件！';
$_LANG['erp_warehousing_return'] ='返回到入貨單編輯頁面';
$_LANG['erp_warehousing_invalid_file'] ='您上傳的文件格式不正確！';
$_LANG['erp_warehousing_batch_warehousing_success'] ='成功導入入貨數據！';
$_LANG['erp_warehousing_invalid_goods_sn'] ='導入的數據含無效的貨號！';

$_LANG['erp_warehousing_without_valid_warehouse'] ='管理員尚未給您分配貨倉，不能新建入貨單。';

$_LANG['erp_warehousing_auto_on_cancel_ship_failed'] ='自動增加庫存失敗';

$_LANG['erp_warehousing_auto_on_cancel_ship'] = '取消出貨自動增加庫存';


/* 打印入貨單 */
$_LANG['erp_print_warehousing_title']='入貨單';

$_LANG['erp_print_warehousing_user1']='制單：';
$_LANG['erp_print_warehousing_user2']='倉管：';
$_LANG['erp_print_warehousing_user3']='交貨人：';
$_LANG['erp_print_warehousing_user4']='審核：';

/* 網店產品庫存列表 */

$_LANG['erp_goods_list_goods_id'] = '序號';
$_LANG['erp_goods_list_goods_img'] ='圖片';
$_LANG['erp_goods_list_goods_sn'] ='Barcode';
$_LANG['erp_goods_list_goods_provider_sn'] ='供應商貨號';
$_LANG['erp_goods_list_goods_name'] ='產品名稱';
$_LANG['erp_goods_list_goods_number'] ='產品庫存';
$_LANG['erp_goods_list_goods_warn_number'] ='警告庫存';
$_LANG['erp_goods_list_goods_goods_attr'] ='產品屬性';
$_LANG['erp_goods_list_set_goods_warn_number'] ='設置警告庫存';

$_LANG['erp_goods_list_search_cat'] ='產品類別';
$_LANG['erp_goods_list_search_goods_sn'] ='產品貨號';
$_LANG['erp_goods_list_search_provider_goods_sn'] ='供應商貨號';
$_LANG['erp_goods_list_sure_to_change_warn_number'] ='您確定要修改警告庫存嗎？';

/* 出貨單 */
$_LANG['erp_delivery_id'] = '序號';
$_LANG['erp_delivery_sn'] ='出貨單號';
$_LANG['erp_delivery_warehouse'] ='出貨貨倉';
$_LANG['erp_delivery_agency'] ='辦事處';
$_LANG['erp_delivery_delivery_style'] ='出貨類型';
$_LANG['erp_delivery_date'] ='出貨日期';
$_LANG['erp_delivery_to'] ='提貨人';
$_LANG['erp_delivery_admin'] ='倉管';
$_LANG['erp_delivery_status'] ='出貨單狀態';
$_LANG['erp_delivery_order'] ='關聯訂單號';

$_LANG['erp_delivery_start_date'] ='起始日期';
$_LANG['erp_delivery_end_date'] ='結束日期';

$_LANG['erp_delivery_status_inputing'] = '出貨單錄入中';//order_status=1
$_LANG['erp_delivery_status_rating'] = '財務處理中';//order_status=2
$_LANG['erp_delivery_status_approving'] = '主管審核中';//order_status=3
$_LANG['erp_delivery_status_approve_pass'] = '審核已通過';//order_status=4
$_LANG['erp_delivery_status_approve_reject'] = '審核未通過';//order_status=5

$_LANG['erp_delivery_operation_approve_remark'] = '審核備註';

$_LANG['erp_delivery_no_access'] = '不能編輯其他倉管員建立的出貨單';

$_LANG['erp_delivery_no_agency_access'] = '不能查看、編輯其他辦事處的出貨單';

$_LANG['erp_delivery_no_acts'] = '該出貨單狀態不能進行此操作';

$_LANG['erp_delivery_item_id'] = '序號';
$_LANG['erp_delivery_item_goods_sn'] ='Barcode';
$_LANG['erp_delivery_item_goods_img'] ='圖片';
$_LANG['erp_delivery_item_goods_attr'] ='產品屬性';
$_LANG['erp_delivery_item_goods_stock'] ='產品庫存';
$_LANG['erp_delivery_item_expected_qty'] ='應發數量';
$_LANG['erp_delivery_item_delivered_qty'] ='實發數量';
$_LANG['erp_delivery_item_remark'] ='備註';
$_LANG['erp_delivery_item_goods_barcode'] ='條碼';

$_LANG['erp_delivery_not_completed']="此出貨單尚未完善，不能提交，需要檢查的項目有：
 * 1，如果是關聯銷售訂單出貨，是否已經選擇了訂單
 * 2，是否輸入了提貨人
 * 3，是否最少有一個出貨單項
 * 4，是否每個出貨單項都有出貨數量
 * 5，是否每個出貨單項對應的產品庫存都足夠";

$_LANG['erp_delivery_item_not_exist'] ='出貨單項不存在或已被刪除！';
$_LANG['erp_delivery_choose_order'] ='選擇訂單';
$_LANG['erp_delivery_select_warehouse'] ='請選擇貨倉';

$_LANG['erp_delivery_stock_not_enough'] ='該產品此屬性庫存不足（庫存數%s）。';

$_LANG['erp_delivery_not_exist'] ='出貨單不存在或已被刪除！';
$_LANG['erp_delivery_no_accessibility']='其他用戶正在編輯此出貨單,不能進行此項操作！';
$_LANG['erp_delivery_return_to_delivery_list']='返回到出貨訂單';

$_LANG['erp_delivery_approve_pass_failed'] ='出貨單審核失敗';
$_LANG['erp_delivery_approve_pass_failed_no_stock'] ='庫存不足，出貨單審核失敗';

$_LANG['erp_delivery_view_delivery']='查看出貨單';
$_LANG['erp_delivery_edit_delivery']='編輯出貨單';
$_LANG['erp_delivery_approve_delivery']='審核出貨單';

$_LANG['erp_delivery_sure_to_change_warehouse'] ='您確定要更改出貨貨倉嗎？';
$_LANG['erp_delivery_sure_to_change_delivery_style'] ='您確定要更改出貨類型嗎？';
$_LANG['erp_delivery_sure_to_change_order'] ='您確定要更改關聯訂單嗎？';
$_LANG['erp_delivery_sure_to_change_delivery_to'] ='您確定要更改提貨人嗎？';

$_LANG['erp_delivery_sure_to_post_delivery'] ='提交後不能再次更改出貨單，您確定要提交嗎？';

$_LANG['erp_delivery_sure_to_submit_delivery'] ='您確定要提交出貨單給主管審核嗎？';
$_LANG['erp_delivery_sure_to_post_approve'] ='您確定要提交審核嗎？';
$_LANG['erp_delivery_sure_to_change_delivered_qty'] ='您確定要更改實發數量嗎？';
$_LANG['erp_delivery_sure_to_change_price'] ='您確定要更改出貨價格嗎？';
$_LANG['erp_delivery_sure_to_delete_delivery_item'] ='您確定要刪除此出貨單項嗎？';
$_LANG['erp_delivery_sure_to_delete_delivery'] ='您確定要刪除此出貨單嗎？';

$_LANG['erp_delivery_delivery_to_required'] ='請填寫提貨人';

$_LANG['erp_delivery_input_barcode'] ='條碼：';
$_LANG['erp_delivery_input_qty'] ='數量：';
$_LANG['erp_delivery_input_delivery_to'] ='輸入提貨人';

$_LANG['erp_delivery_import_delivery'] ='導入數據：';

$_LANG['erp_delivery_input_goods_sn'] ='貨號：';
$_LANG['erp_delivery_input_goods_name'] ='名稱：';
$_LANG['erp_delivery_input_goods_barcode'] ='條碼：';

$_LANG['erp_delivery_stock_not_enough'] ='出貨數量大於出貨數量（現有庫存%d），不能出貨';

$_LANG['erp_delivery_warehouse_cant_change'] ='刪除出貨單項後才能更改出貨貨倉';

$_LANG['erp_delivery_goods_sn_required'] ='請輸入貨號。';
$_LANG['erp_delivery_goods_name_required'] ='請輸入產品名稱。';
$_LANG['erp_delivery_goods_sn_or_goods_name_or_goods_barcode_required'] ='請輸入條碼、貨號或產品名稱。';
$_LANG['erp_delivery_wrong_goods_sn'] ='請核對輸入的產品貨號。';
$_LANG['erp_delivery_wrong_goods_name'] ='請核對輸入的產品名稱。';
$_LANG['erp_delivery_wrong_goods_barcode'] ='請核對輸入的產品條碼。';

$_LANG['erp_delivery_item_delivery_price'] ='出貨價格';

$_LANG['erp_delivery_without_choosing_order'] ='請選擇關聯訂單號！';
$_LANG['erp_delivery_without_delivery_to'] ='請輸入提貨人！';
$_LANG['erp_delivery_without_delivery_item'] ='此出貨單無出貨單項，不能提交！';
$_LANG['erp_delivery_without_delivery_qty'] ='最少有一條出貨單項未輸入實發數量，不能提交！';
$_LANG['erp_delivery_only_text_file'] ='文件類型錯誤，正確的文件應當是以"txt"為擴展名的文本文件！';
$_LANG['erp_delivery_return'] ='返回到出貨單編輯頁面';
$_LANG['erp_delivery_invalid_file'] ='您上傳的文件格式不正確！';
$_LANG['erp_delivery_batch_delivery_success'] ='成功導入出貨數據！';
$_LANG['erp_delivery_invalid_goods_sn'] ='導入的數據含無效的貨號！';

$_LANG['erp_delivery_no_stock'] ='至少有一個訂單項因庫存不足，沒有導入，請檢查庫存。';

/* 打印出貨單 */
$_LANG['erp_print_delivery_title']='出貨單';
$_LANG['erp_print_delivery_user1']='制單：';
$_LANG['erp_print_delivery_user2']='倉管：';
$_LANG['erp_print_delivery_user3']='提貨人：';
$_LANG['erp_print_delivery_user4']='審核：';

/* 產品庫存列表 */
$_LANG['erp_stock_id'] = '序號';
$_LANG['erp_stock_goods_id'] = '產品ID';
$_LANG['erp_stock_goods_img'] ='圖片';
$_LANG['erp_stock_goods_sn'] ='Barcode';
$_LANG['erp_stock_goods_provider_sn'] ='供應商貨號';
$_LANG['erp_stock_goods_name'] ='產品名稱';
$_LANG['erp_stock_goods_barcode'] ='產品條碼';
$_LANG['erp_stock_warehouse_name'] ='貨倉名稱';
$_LANG['erp_stock_goods_number'] ='產品庫存';
$_LANG['erp_stock_warehouse_attr_qty'] ='貨倉-存貨';

$_LANG['erp_stock_warehouse'] ='貨倉';
$_LANG['erp_stock_agency'] ='辦事處';
$_LANG['erp_stock_attr'] ='屬性';
$_LANG['erp_stock_qty'] ='庫存';

$_LANG['erp_stock_search_warehouse'] ='請選擇貨倉';
$_LANG['erp_stock_search_goods_sn'] ='產品貨號';
$_LANG['erp_stock_search_goods_name'] ='產品名稱';
$_LANG['erp_stock_search_goods_barcode'] ='產品條碼';
$_LANG['erp_stock_search_provider_goods_sn'] ='供應商貨號';

/* 產品存貨盤點表 */
$_LANG['erp_stock_check_id'] = '序號';
$_LANG['erp_stock_check_goods_img'] ='圖片';
$_LANG['erp_stock_check_goods_sn'] ='Barcode';
$_LANG['erp_stock_check_goods_name'] ='產品名稱';
$_LANG['erp_stock_check_goods_barcode'] ='產品條碼';
$_LANG['erp_stock_check_goods_provider_sn'] ='供應商貨號';
$_LANG['erp_stock_check_goods_attr'] ='產品屬性';
$_LANG['erp_stock_check_warehouse_name'] ='貨倉';
$_LANG['erp_stock_check_agency'] ='辦事處';
$_LANG['erp_stock_check_goods_sn'] ='產品貨號';
$_LANG['erp_stock_check_begin_qty'] ='期初庫存';
$_LANG['erp_stock_check_end_qty'] ='期末庫存';

$_LANG['erp_stock_check_select_warehouse'] ='請選擇貨倉';

$_LANG['erp_stock_check_warehousing_qty'] ='入貨數量';
$_LANG['erp_stock_check_delivery_qty'] ='出貨數量';

$_LANG['erp_stock_check_search_start_date'] ='起始日期';
$_LANG['erp_stock_check_search_end_date'] ='結束日期';

$_LANG['erp_stock_check_print'] ='打印存貨盤點表';

/* 產品出入貨記錄(序時薄) */
$_LANG['erp_stock_record_id'] = '序號';
$_LANG['erp_stock_record_goods_img'] ='圖片';
$_LANG['erp_stock_record_goods_sn'] ='Barcode';
$_LANG['erp_stock_record_goods_name'] ='產品名稱';
$_LANG['erp_stock_record_goods_attr'] ='產品屬性';
$_LANG['erp_stock_record_goods_barcode'] ='條碼';
$_LANG['erp_stock_record_warehouse_name'] ='貨倉';
$_LANG['erp_stock_record_agency'] ='辦事處';
$_LANG['erp_stock_record_w_d_style'] ='出入貨類型';
$_LANG['erp_stock_record_w_d_sn'] ='出入貨單號';
$_LANG['erp_stock_record_order_sn'] ='關聯訂單號';
$_LANG['erp_stock_record_w_d_qty'] ='數量';
$_LANG['erp_stock_record_stock'] ='存貨';
$_LANG['erp_stock_record_w_d_name'] ='提/交貨人';

$_LANG['erp_stock_record_goods_price'] ='產品價格';
$_LANG['erp_stock_record_goods_amount'] ='產品金額';

$_LANG['erp_stock_record_date'] ='日期';

$_LANG['erp_stock_record_search_stock_style_both'] ='出貨和入貨';
$_LANG['erp_stock_record_search_stock_style_only_warehousing'] ='入貨';
$_LANG['erp_stock_record_search_stock_style_only_delivery'] ='出貨';

$_LANG['erp_stock_record_search_warehouse'] ='請選擇貨倉';
$_LANG['erp_stock_record_search_w_d_style'] ='請選擇出入貨類型';
$_LANG['erp_stock_record_search_goods_sn'] ='產品貨號';
$_LANG['erp_stock_record_search_goods_name'] ='產品名稱';
$_LANG['erp_stock_record_search_goods_barcode'] ='產品條碼';
$_LANG['erp_stock_record_search_provider_goods_sn'] ='供應商貨號';
$_LANG['erp_stock_record_search_start_date'] ='起始日期';
$_LANG['erp_stock_record_search_end_date'] ='結束日期';

$_LANG['erp_stock_record_search_supplier'] ='請選擇供貨商';
$_LANG['erp_stock_record_search_brand'] ='請選擇品牌';

/* 貨倉調貨 */
$_LANG['erp_stock_transfer_id'] ='序號';
$_LANG['erp_stock_transfer_sn'] ='編號';
$_LANG['erp_stock_transfer_warehouse_from'] ='出貨貨倉';
$_LANG['erp_stock_transfer_agency_from'] ='出貨辦事處';
$_LANG['erp_stock_transfer_warehouse_to'] ='收貨貨倉';
$_LANG['erp_stock_transfer_agency_to'] ='收貨辦事處';
$_LANG['erp_stock_transfer_admin_from'] ='出貨倉管';
$_LANG['erp_stock_transfer_admin_to'] ='收貨倉管';
$_LANG['erp_stock_transfer_transfer_qty'] ='調貨數量';
$_LANG['erp_stock_transfer_create_time'] ='出貨日期';
$_LANG['erp_stock_transfer_transfer_time'] ='收貨日期';
$_LANG['erp_stock_transfer_status'] ='狀態';

$_LANG['erp_stock_transfer_status_inputing'] = '調貨單錄入中';//status=1
$_LANG['erp_stock_transfer_status_await_recieve'] = '待確認收貨';//status=2
$_LANG['erp_stock_transfer_status_approve_pass'] = '已調貨';//status=3
$_LANG['erp_stock_transfer_status_approve_reject'] = '拒絕收貨';//status=4

$_LANG['erp_operation_post_transfer'] ='提交調貨';
$_LANG['erp_operation_confirm_transfer'] ='確認調貨';
$_LANG['erp_operation_reject_transfer'] ='拒絕調貨';
$_LANG['erp_operation_withdrawal_transfer'] ='退回';

$_LANG['erp_stock_transfer_search_warehouse_from'] ='選擇出貨貨倉';
$_LANG['erp_stock_transfer_search_warehouse_to'] ='選擇收貨貨倉';
$_LANG['erp_stock_transfer_search_admin_from'] ='選擇出貨倉管';
$_LANG['erp_stock_transfer_search_admin_to'] ='選擇收貨倉管';
$_LANG['erp_stock_transfer_search_goods_sn'] ='產品貨號';
$_LANG['erp_stock_transfer_search_goods_name'] ='產品名稱';
$_LANG['erp_stock_transfer_search_status'] ='調貨單狀態';

$_LANG['erp_stock_transfer_view'] ='查看調貨單';
$_LANG['erp_stock_transfer_edit'] ='編輯調貨單';
$_LANG['erp_stock_transfer_confirm'] ='確認調貨單';

$_LANG['erp_stock_transfer_operation_print'] ='打印調貨單';
$_LANG['erp_stock_transfer_item_id'] ='序號';
$_LANG['erp_stock_transfer_item_goods_sn'] ='Barcode';
$_LANG['erp_stock_transfer_item_goods_img'] ='圖片';
$_LANG['erp_stock_transfer_item_goods_name'] ='產品名稱';
$_LANG['erp_stock_transfer_item_goods_attr'] ='屬性';
$_LANG['erp_stock_transfer_item_transfer_qty'] ='調貨數量';
$_LANG['erp_stock_transfer_reurn'] ='返回到調貨單列表';
$_LANG['erp_stock_transfer_no_access'] ='不能編輯其他倉管員建立的調貨單';
$_LANG['erp_order_no_accessibility'] ='其他用戶正在編輯此調貨單,不能進行此項操作！';
$_LANG['erp_stock_transfer_without_valid_warehouse'] ='管理員尚未給您分配貨倉，不能建立、編輯或修改調貨單。';
$_LANG['erp_stock_transfer_select_warehouse_from'] ='選擇出貨貨倉';
$_LANG['erp_stock_transfer_select_warehouse_to'] ='選擇收貨貨倉';
$_LANG['erp_stock_transfer_not_exists'] ='調貨單不存在';
$_LANG['erp_stock_transfer_item_not_exists'] ='調貨單項不存在';
$_LANG['erp_stock_transfer_no_access'] ='不能編輯其他倉管員建立的調貨庫單';
$_LANG['erp_stock_transfer_no_accessibility']='其他用戶正在編輯此調貨庫單,不能進行此項操作！';
$_LANG['erp_stock_transfer_warehouse_same'] ='收貨貨倉和出貨貨倉不能是同一個';
$_LANG['erp_stock_transfer_input_goods_sn'] ='貨號：';
$_LANG['erp_stock_transfer_input_goods_name'] ='名稱：';
$_LANG['erp_stock_transfer_input_qty'] ='數量：';
$_LANG['erp_stock_transfer_stock_not_enough'] ='調貨數量大於出貨數量（現有庫存%d），不能調貨';
$_LANG['erp_stock_transfer_warehouse_cant_change'] ='刪除調貨單項後才能更改出貨貨倉';
$_LANG['erp_stock_transfer_goods_sn_required'] ='請輸入貨號。';
$_LANG['erp_stock_transfer_goods_name_required'] ='請輸入貨號。';
$_LANG['erp_stock_transfer_goods_sn_or_goods_name_required'] ='請輸入貨號或產品名稱。';
$_LANG['erp_stock_transfer_wrong_goods_sn'] ='請核對輸入的產品貨號。';
$_LANG['erp_stock_transfer_wrong_goods_name'] ='請核對輸入的產品名稱。';
$_LANG['erp_stock_transfer_goods_sn_required'] ='請輸入貨號。';
$_LANG['erp_stock_transfer_no_acts'] = '該調貨單狀態不能進行此操作';
$_LANG['erp_stock_transfer_sure_to_delete'] ='您確定要刪除此調貨單嗎？';
$_LANG['erp_stock_transfer_sure_to_delete_item'] ='您確定要刪除此調貨單項嗎？';
$_LANG['erp_add_stock_transfer'] = '貨倉調貨';
$_LANG['erp_stock_transfer_sure_to_post'] = '您確定要提交此調貨單嗎？';
$_LANG['erp_stock_transfer_sure_to_withdrawal'] = '您確定要退回此調貨單嗎？';

$_LANG['erp_stock_transfer_return_to_list'] ='返回到貨倉調貨';
$_LANG['erp_stock_transfer_no_agency_access'] ='不能查看、編輯其他辦事處的調貨單';

$_LANG['erp_stock_transfer_not_completed']="此調貨單尚未完善，不能提交，需要檢查的項目有：
 * 1，收貨貨倉和出貨貨倉不能相同
 * 2，是否最少有1個調貨單項
 * 3，每條調貨單項的出貨數量是否大於0";
 
 $_LANG['erp_stock_transfer_no_confirm_accessibility'] = '您不是該調貨單收貨貨倉的倉管，不能執行此操作。';
$_LANG['erp_stock_transfer_sure_to_confirm'] ='您確定要確認此調貨單嗎？';
$_LANG['erp_stock_transfer_confirm_failed'] ='調貨單確認操作失敗';
$_LANG['erp_stock_transfer_confirm_failed_no_stock'] ='庫存不夠，調貨單確認操作失敗';
$_LANG['erp_stock_transfer_w_d_style'] ='貨倉調貨';

$_LANG['erp_stock_transfer_print_title'] ='貨倉調貨單';
$_LANG['erp_stock_transfer_print_user1'] ='出貨倉管';
$_LANG['erp_stock_transfer_print_user2'] ='收貨倉管';
$_LANG['erp_stock_transfer_print_user3'] ='主管';

/* 存貨變動圖表 */
$_LANG['erp_stock_daily_goods_sn'] = '貨號';
$_LANG['erp_stock_daily_start_date'] = '開始日期';
$_LANG['erp_stock_daily_end_date'] = '結束日期';
$_LANG['erp_stock_daily_warehouse'] = '貨倉';
$_LANG['erp_stock_daily_date'] = '日期';
$_LANG['erp_stock_daily_stock'] = '庫存';
$_LANG['erp_stock_daily_select_warehouse'] = '選擇貨倉';
$_LANG['erp_stock_daily_warehouse_required'] = '請選擇貨倉';

$_LANG['erp_stock_daily_goods_sn_required'] = '請輸入產品貨號';
$_LANG['erp_stock_daily_goods_sn_wrong'] = '您輸入的貨號有誤';
$_LANG['erp_stock_daily_return'] = '返回';

$_LANG['erp_stock_daily_no_agency_access'] = '不能查看、編輯其他辦事處的庫存';

$_LANG['last_update'] = '最後更新日期';
$_LANG['now_update'] = '更新記錄';
$_LANG['update_success'] = '分析記錄已成功更新!';
$_LANG['view_log'] = '查看分析記錄';
$_LANG['select_year_month'] = '查詢年月';

$_LANG['pv_stats'] = '綜合訪問數據';
$_LANG['integration_visit'] = '綜合訪問量';
$_LANG['seo_analyse'] = '搜索引擎分析';
$_LANG['area_analyse'] = '地域分佈';
$_LANG['visit_site'] = '來訪網站分析';
$_LANG['key_analyse'] = '關鍵字分析';

$_LANG['start_date'] = '開始日期';
$_LANG['end_date'] = '結束日期';
$_LANG['query'] = '查詢';
$_LANG['result_filter'] = '過濾結果';
$_LANG['compare_query'] = '比較查詢';
$_LANG['year_status'] = '年走勢';
$_LANG['month_status'] = '月走勢';

$_LANG['year'] = '年';
$_LANG['month'] = '月';
$_LANG['day'] = '日';
$_LANG['year_format'] = '%Y';
$_LANG['month_format'] = '%c';

$_LANG['from'] = '從';
$_LANG['to'] = '到';
$_LANG['view'] = '查看';

/* 應付款列表 應付款詳情 */
$_LANG['erp_payable_order_id'] = '序號';
$_LANG['erp_payable_order_sn'] = '編號';
$_LANG['erp_payable_order_description'] = '描述';
$_LANG['erp_payable_order_agency'] = '辦事處';
$_LANG['erp_payable_order_supplier'] = '供應商';
$_LANG['erp_payable_order_date'] = '訂單日期';
$_LANG['erp_payable_order_amount'] = '訂單金額';
$_LANG['erp_payable_total_payable_amount'] = '應付款';
$_LANG['erp_payable_total_paid_amount'] = '已付款';
$_LANG['erp_payable_payable'] = '剩餘款';
$_LANG['erp_payable_pay'] = '付款金額';

$_LANG['erp_payable_operation_pay'] = '付款';

$_LANG['erp_payable_search_all_orders'] = '所有訂單';
$_LANG['erp_payable_search_payable_orders'] = '應付款訂單';
$_LANG['erp_payable_search_order_sn'] = '訂單編號';

$_LANG['erp_payable_no_valid_bank_account'] = '尚未設置有效的銀行帳戶，不能建立付款單。';

$_LANG['erp_payable_more_than_payable'] = '付款金額不能大於應付款！';

$_LANG['erp_payable_details'] = '應付款詳情';

$_LANG['erp_payable_sure_to_withdrawal_to_edit'] = "您確定要退回此付款單嗎？";

$_LANG['erp_payable_search_order_sn'] = '訂單編號';

$_LANG['erp_payable_search_supplier_name'] = '供應商名稱';

$_LANG['erp_payable_no_agency_access'] = '不能查看、編輯其他辦事處的應付款';

/* 應收款列表 應收款詳情 */
$_LANG['erp_receivable_order_id'] = '序號';
$_LANG['erp_receivable_order_sn'] = '編號';
$_LANG['erp_receivable_agency'] = '辦事處';
$_LANG['erp_receivable_order_description'] = '描述';
$_LANG['erp_receivable_order_username'] = '用戶名';
$_LANG['erp_receivable_order_consignee'] = '收貨人';
$_LANG['erp_receivable_order_shipping_date'] = '出貨時間';
$_LANG['erp_receivable_order_amount'] = '訂單金額';
$_LANG['erp_receivable_total_receivable_amount'] = '應收款';
$_LANG['erp_receivable_total_gathered_amount'] = '已收款';
$_LANG['erp_receivable_receivable'] = '剩餘款';
$_LANG['erp_receivable_gather'] = '付款金額';

$_LANG['erp_receivable_goods_id'] = '序號';
$_LANG['erp_receivable_goods_sn'] = 'Barcode';
$_LANG['erp_receivable_goods_img'] = '圖片';
$_LANG['erp_receivable_goods_attr'] = '產品屬性';
$_LANG['erp_receivable_goods_price'] = '產品價格';
$_LANG['erp_receivable_goods_order_qty'] = '購買數量';
$_LANG['erp_receivable_goods_delivered_qty'] = '已出貨數量';
$_LANG['erp_receivable_goods_amount'] = '總價';

$_LANG['erp_receivable_operation_gather'] = '收款';

$_LANG['erp_receivable_search_all_orders'] = '所有訂單';
$_LANG['erp_receivable_search_receivable_orders'] = '應收款訂單';
$_LANG['erp_receivable_search_order_sn'] = '訂單編號';

$_LANG['erp_receivable_more_than_receivable'] = '收款金額不能大於應收款！';

$_LANG['erp_receivable_details'] = '應收款詳情';

$_LANG['erp_receivable_no_valid_bank_account'] = '尚未設置有效的銀行帳戶，不能建立收款單。';

$_LANG['erp_receivable_sure_to_withdrawal_to_edit'] = "您確定要退回此收款單嗎？";

$_LANG['erp_receivable_no_agency_access'] = '不能查看、編輯其他辦事處的應收款';

/* 銀行帳號 */
$_LANG['erp_bank_account_id'] = '序號';
$_LANG['erp_bank_account_name'] = '帳戶名稱';
$_LANG['erp_bank_account_no'] = '帳號';
$_LANG['erp_bank_account_agency'] = '所屬辦事處';
$_LANG['erp_bank_account_balance'] = '帳戶餘額';

$_LANG['erp_bank_account_not_exists'] = '該銀行帳號不存在或已被刪除！';

$_LANG['erp_bank_account_details_id'] = '序號';
$_LANG['erp_bank_account_details_income_expenses'] = '收入或支出';
$_LANG['erp_bank_account_details_payment_gathering'] = '關聯收付款單號';
$_LANG['erp_bank_account_details_order'] = '關聯訂單號';
$_LANG['erp_bank_account_details_date'] = '收支日期';
$_LANG['erp_bank_account_details_remark'] = '備註';

$_LANG['erp_bank_account_details_start_date'] = ' 起始日期';
$_LANG['erp_bank_account_details_end_date'] = ' 結束日期';

$_LANG['erp_bank_account_add'] = '添加銀行帳號';
$_LANG['erp_bank_account_edit'] = '編輯銀行帳號';
$_LANG['erp_bank_account_name_required'] = '請輸入銀行帳戶名稱。';
$_LANG['erp_bank_account_no_required'] = '請輸入銀行帳號。';

$_LANG['erp_bank_account_select_agency'] = '選擇辦事處';

$_LANG['erp_bank_account_return_to_list'] = '返回到銀行帳號列表';

$_LANG['erp_bank_account_has_gathering'] = '此銀行帳號有相關聯的收款單，不能編輯或刪除。';
$_LANG['erp_bank_account_has_payment'] = '此銀行帳號有相關聯的付款單，不能編輯或刪除。';
$_LANG['erp_bank_account_has_bank_record'] = '此銀行帳號有收付款記錄，不能編輯或刪除';

$_LANG['erp_bank_account_no_agency_access'] = '不能查看、編輯其他辦事處的銀行帳號信息';

$_LANG['erp_bank_account_details'] = '銀行帳戶收支明細';

$_LANG['erp_bank_account_return'] = '返回到銀行帳號列表';

$_LANG['erp_bank_account_add_success'] = '成功添加銀行帳號';

/* 打印銀行收支清單 */
$_LANG['erp_print_bank_account_details']='銀行帳戶收支清單';

/* 付款類型 */
$_LANG['erp_payment_style_id'] = '序號';
$_LANG['erp_payment_style'] = '付款類型';
$_LANG['erp_payment_style_not_exists'] = '該付款類型不存在或已被刪除！';

/* 付款單 */
$_LANG['erp_add_payment'] = '添加付款單';

$_LANG['erp_payment_id'] = '序號';
$_LANG['erp_payment_sn'] = '付款單編號';
$_LANG['erp_payment_date'] = '付款日期';
$_LANG['erp_payment_pay_to'] = '收款人';
$_LANG['erp_payment_remark'] = '付款說明';
$_LANG['erp_payment_agency'] = '辦事處';
$_LANG['erp_payment_order_id'] = '訂單號';
$_LANG['erp_payment_order_date'] = '訂單日期';
$_LANG['erp_payment_order_amount'] = '訂單總金額';
$_LANG['erp_payment_order_total_payable'] = '訂單總應付款';
$_LANG['erp_payment_order_paid_payable'] = '訂單已付應付款';
$_LANG['erp_payment_order_payable_balance'] = '訂單剩餘應付款';
$_LANG['erp_payment_pay_amount'] = '本次付款金額';
$_LANG['erp_payment_account'] = '付款帳號';

$_LANG['erp_payment_status'] = '付款單狀態';

$_LANG['erp_payment_operation_view_approve_remark'] = '審核備註';

$_LANG['erp_payment_status_inputing'] = '付款單錄入中';//payment_status=1
$_LANG['erp_payment_status_approving'] = '等待主管審核';//payment_status=2
$_LANG['erp_payment_status_approve_pass'] = '審核已通過';//payment_status=3
$_LANG['erp_payment_status_approve_reject'] = '審核未通過';//payment_status=4

$_LANG['erp_payment_search_payment_status'] = '付款單狀態';
$_LANG['erp_payment_search_pay_to'] = '收款人';
$_LANG['erp_payment_search_start_date'] = '起始日期';
$_LANG['erp_payment_search_end_date'] = '結束日期';
$_LANG['erp_payment_search_payment_sn'] = '付款單編號';
$_LANG['erp_payment_search_bill_sn'] = '訂單編號';

$_LANG['erp_payment_no_pay'] = '您還沒有輸入付款金額。';

$_LANG['erp_payment_return_to_payment_list']='返回到付款單列表';
$_LANG['erp_payment_view_payment']='查看此付款單';

$_LANG['erp_payment_sure_to_post'] = '您確定要付款嗎？';
$_LANG['erp_payment_sure_to_pay'] = "您確定要付款，並生成一個付款單嗎？";
$_LANG['erp_payment_sure_to_delete_payment']='您確定要刪除付款單嗎？';
$_LANG['erp_payment_sure_to_post_payment']='您確定要提交付款單嗎？';
$_LANG['erp_payment_sure_to_change_bank_account']='您確定要更改付款銀行帳號嗎？';
$_LANG['erp_payment_sure_to_change_pay_amount']='您確定要更改付款金額嗎？';
$_LANG['erp_payment_sure_to_approve'] = "您確定要提交審核嗎？";
$_LANG['erp_payment_sure_to_change_order'] = '您確定要更改訂單嗎？';

$_LANG['erp_payment_greater_than_payable']='付款金額不能大於訂單剩餘應付款。';

$_LANG['erp_payment_not_exist']='此付款單不存在或已被刪除！';
$_LANG['erp_payment_delete_sucess']='成功刪除付款單';
$_LANG['erp_payment_no_accessibility']='其他用戶正在編輯此付款單,不能進行此項操作！';

$_LANG['erp_payment_post_success']='成功提交付款單。';
$_LANG['erp_payment_approve_success']='成功提交審核。';

$_LANG['erp_payment_no_agency_access']='不能查看、編輯其他辦事處的付款單';

$_LANG['erp_payment_no_acts'] = '該付款單狀態不能進行此操作';

/* 收款單 */
$_LANG['erp_add_gathering'] = '添加收款單';

$_LANG['erp_gathering_anonymous'] = '匿名用戶';

$_LANG['erp_gathering_id'] = '序號';
$_LANG['erp_gathering_sn'] = '收款單編號';
$_LANG['erp_gathering_date'] = '收款日期';
$_LANG['erp_gathering_pay_from'] = '付款人';
$_LANG['erp_gathering_remark'] = '收款說明';
$_LANG['erp_gathering_agency'] = '辦事處';
$_LANG['erp_gathering_order_id'] = '訂單號';
$_LANG['erp_gathering_order_date'] = '訂單日期';
$_LANG['erp_gathering_order_amount'] = '訂單總金額';
$_LANG['erp_gathering_order_total_receivable'] = '訂單總應收款';
$_LANG['erp_gathering_order_paid_receivable'] = '訂單已付應收款';
$_LANG['erp_gathering_order_receivable_balance'] = '訂單剩餘應收款';
$_LANG['erp_gathering_gather_amount'] = '收款金額';
$_LANG['erp_gathering_account'] = '收款帳號';

$_LANG['erp_gathering_status'] = '收款單狀態';

$_LANG['erp_gathering_operation_view_approve_remark'] = '審核備註';

$_LANG['erp_gathering_status_inputing'] = '收款單錄入中';//gathering_status=1
$_LANG['erp_gathering_status_approving'] = '等待主管審核';//gathering_status=2
$_LANG['erp_gathering_status_approve_pass'] = '審核已通過';//gathering_status=3
$_LANG['erp_gathering_status_approve_reject'] = '審核未通過';//gathering_status=4

$_LANG['erp_gathering_status_revised'] = '訂單已修訂';

$_LANG['erp_gathering_search_gathering_status'] = '收款單狀態';
$_LANG['erp_gathering_search_pay_from'] = '付款人';
$_LANG['erp_gathering_search_start_date'] = '起始日期';
$_LANG['erp_gathering_search_end_date'] = '結束日期';
$_LANG['erp_gathering_search_gathering_sn'] = '收款單編號';
$_LANG['erp_gathering_search_order_sn'] = '訂單編號';

$_LANG['erp_gathering_no_gather'] = '您還沒有輸入收款金額。';

$_LANG['erp_gathering_return_to_gathering_list']='返回到收款單列表。';
$_LANG['erp_gathering_view_gathering']='查看此收款單。';

$_LANG['erp_gathering_sure_to_change_order'] = '您確定要更改訂單嗎？';
$_LANG['erp_gathering_sure_to_post'] = '您確定要收款嗎？';
$_LANG['erp_gathering_sure_to_gather'] = "您確定要收款，並生成一個收款單嗎？";
$_LANG['erp_gathering_sure_to_delete_gathering']='您確定要刪除收款單嗎？';
$_LANG['erp_gathering_sure_to_post_gathering']='您確定要提交收款單嗎？';
$_LANG['erp_gathering_sure_to_change_bank_account']='您確定要更改收款銀行帳號嗎？';
$_LANG['erp_gathering_sure_to_change_gather_amount']='您確定要更改收款金額嗎？';
$_LANG['erp_gathering_sure_to_approve'] = "您確定要提交審核嗎？";

$_LANG['erp_gathering_greater_than_receivable']='收款金額不能大於訂單剩餘應收款。';

$_LANG['erp_gathering_not_exist']='此收款單不存在或已被刪除！';
$_LANG['erp_gathering_delete_sucess']='成功刪除收款單。';
$_LANG['erp_gathering_no_accessibility']='其他用戶正在編輯此收款單,不能進行此項操作！';

$_LANG['erp_gathering_post_success']='成功提交收款單。';
$_LANG['erp_gathering_approve_success']='成功提交審核。';

$_LANG['erp_gathering_no_agency_access']='不能查看、編輯其他辦事處的收款單';

$_LANG['erp_gathering_no_acts']='該收款單狀態不能進行此操作';

/* 產品條碼列表 */
$_LANG['erp_barcode_id'] = '序號';
$_LANG['erp_barcode_goods_id'] = '產品ID';
$_LANG['erp_barcode_goods_img'] ='圖片';
$_LANG['erp_barcode_goods_sn'] ='Barcode';
$_LANG['erp_barcode_goods_name'] ='產品名稱';
$_LANG['erp_barcode_goods_attr'] ='產品屬性';
$_LANG['erp_barcode_goods_barcode'] ='產品條碼';
$_LANG['erp_barcode_attr_and_barcode'] ='屬性-條碼';

$_LANG['erp_barcode_exists'] ='該條碼已存在，請重新輸入。';

$_LANG['erp_barcode_search_warehouse'] ='請選擇貨倉';
$_LANG['erp_barcode_search_goods_sn'] ='產品貨號';
$_LANG['erp_barcode_search_goods_name'] ='產品名稱';
$_LANG['erp_barcode_search_provider_goods_sn'] ='供應商貨號';

/* Sales agent */
$_LANG['erp_sales_agent_id'] = '序號';
$_LANG['erp_sales_agent_list'] = '代理銷售';
$_LANG['erp_sales_agent'] ='代理銷售';
$_LANG['sales_agent'] ='代理銷售';
$_LANG['sale_agent_name'] ='代理銷售名稱';
$_LANG['sales_agent_rate'] ='手續費';
$_LANG['erp_sales_agent_not_exists'] = '該代理銷售不存在或已被刪除！';

/* Credit Note */
$_LANG['erp_credit_summary'] = 'Credit Note概覽';
$_LANG['erp_credit_list'] = 'Credit Note列表';
$_LANG['erp_credit_transaction'] = 'Credit Note交易紀錄';
$_LANG['erp_add_credit'] = '添加Credit Note';
$_LANG['erp_paid_credit'] = '支付Credit Note';

/* Revise PO */
$_LANG['po_revise_status'][PO_REVISE_CREATED] = "已建立";
$_LANG['po_revise_status'][PO_REVISE_SUBMITTED] = "待審核";
$_LANG['po_revise_status'][PO_REVISE_APPROVED] = "已批核";
$_LANG['po_revise_status'][PO_REVISE_DISAPPROVED] = "已拒絕";
?>
