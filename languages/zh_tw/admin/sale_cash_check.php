<?php

/**
 * ECSHOP 管理中心優惠活動語言文件
 * ============================================================================
 * * 版權所有 2005-2012 上海商派網絡科技有限公司，並保留所有權利。
 * 網站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 這不是一個自由軟件！您只能在不用於商業目的的前提下對程序代碼進行修改和
 * 使用；不允許對程序代碼以任何形式任何目的的再發佈。
 * ============================================================================
 * $Author: liubo $
 * $Id: favourable.php 17217 2011-01-19 06:29:08Z liubo $
 */


$_LANG['balance_operation'] = '收入/支出操作';
$_LANG['shop'] = '門店';
$_LANG['report_date'] = '報表日期';
$_LANG['income'] = '收入';
$_LANG['expenses'] = '支出';
$_LANG['reviewer'] = '報表審核人';
$_LANG['confirm_review'] = '確認當日報表數據';
$_LANG['pls_select_reviewer'] = '請選擇審核人';
$_LANG['confirm_submit'] = '確認當日報表后會截圖，之後查詢不會更改當日數據';


/* list */
$_LANG['shop_open_cash_balance'] = '門市開店現金結餘';
$_LANG['shop_close_cash_balance'] = '門市關店現金結餘';
$_LANG['c1_cash'] = 'C1流動現金(HKD)';
$_LANG['c2_cash'] = 'C2開門現金(HKD)';
$_LANG['d_down_payment'] = 'D訂金袋(HKD)';
$_LANG['operator'] = '操作人';
$_LANG['total'] = '合計(C1+C2+D)';
$_LANG['cash_shipped'] = '當日現金(已出貨)';
$_LANG['cash_unshipped'] = '當日現金(未出貨)';
$_LANG['cash_refund'] = '當日現金退款';
$_LANG['transfer'] = '轉數';
$_LANG['today_ship_not_today_paid_total'] = '當日出貨但非當日訂單總額';
$_LANG['review_report_ok'] = '報表審核成功';

/* operation */
$_LANG['operation_date'] = '日期';
$_LANG['operation_type'] = '操作類型';
$_LANG['operation_type_income'] = '收入';
$_LANG['operation_type_expenses'] = '支出';
$_LANG['operation_reason'] = '收入/支出原因';
$_LANG['operation_reason_bank'] = '存入銀行現金';
$_LANG['operation_reason_expenses'] = '費用支出';
$_LANG['operation_reason_supplier'] = '找數給供應商';
$_LANG['operation_reason_other'] = '其它';
$_LANG['operation_remarks'] = '描述';
$_LANG['operation_account'] = '賬戶';
$_LANG['c1'] = 'C1流動現金(HKD)';
$_LANG['c2'] = 'C2開門現金(HKD)';
$_LANG['d'] = 'D訂金袋(HKD)';
$_LANG['operation_operater'] = '操作人';
$_LANG['operation_amount'] = '金額';
$_LANG['bank_name'] = '銀行名稱';
$_LANG['supplier_name'] = '供應商名稱';
$_LANG['add_operation_ok'] = '新增紀錄成功';
$_LANG['continue_balance_operation'] = '繼續新增收入/支出紀錄';
$_LANG['back_to_report'] = '返回報告';
$_LANG['pls_set_operation_type'] = '請選擇操作類型';
$_LANG['pls_set_operation_reason'] = '請選擇收入/支出原因';
$_LANG['pls_enter_bank_name'] = '請輸入銀行名稱';
$_LANG['pls_enter_supplier_name'] = '請輸入供應商名稱';
$_LANG['pls_set_operation_account'] = '請選擇賬戶';
$_LANG['pls_set_user_id'] = '請選擇操作人';
$_LANG['operation_amount_err'] = '金額錯誤';


?>