<?php 

// 插件語言包
/*
**/

$_LANG['website_name'] = '插件名稱';
$_LANG['website_qq'] = '作者QQ';
$_LANG['website_author'] = '作者';
$_LANG['website_email'] = '郵箱地址';
$_LANG['ur_here'] = '第三方登錄插件';
$_LANG['ur_view']  =  '查看第三方插件';
$_LANG['ur_install'] =  '安裝第三方插件';
$_LANG['webstte_list'] = '插件列表';
$_LANG['user_rank'] = '用戶等級名稱';
$_LANG['yes_install'] = '安裝完成';
$_LANG['yes_update'] = '修改插件成功';
$_LANG['version'] = '版本';

$_LANG['yes_uninstall'] = '卸載成功';
$_LANG['no_uninstall'] = '卸載失敗,找不到文件';
$_LANG['yes_init'] = '初始化成功';
$_LANG['init'] = '初始化插件';
$_LANG['help_app_key'] = '第三方網站申請的APP_KEY , 可能有的名稱不同， 如不同，請填寫<申請時所得的最短的一行>';
$_LANG['help_app_secret'] = '第三方網站申請的APP_SECRET ，可能有的名稱不同，如不同，請填寫<申請時所得的最長的一行>';
$_LANG['help_rank_name'] = '所屬的用戶組名稱, 如果是 QQ 插件,如 QQ用戶';
$_LANG['update_time'] = '更新日期';
$_LANG['website_web'] = '申請地址';
$_LANG['once'] = '立即申請';
$_LANG['batch_create'] = '批量生成';
$_LANG['warning'] = '提示:批量生成調用代碼並複製到您的模版文件應用處';
$_LANG['warning2'] = '提示：代碼已經生成成功,請先複製此 {insert_scripts files="website.js"} 段代碼到您的模版頭部';


$_LANG['is_show_name'] = '顯示登錄名';
$_LANG['is_show_title'] = '顯示窗口標題';
$_LANG['js_languages']['confrim_uninstall'] = '您確定批量卸載？';
$_LANG['batch_yes'] = '批量處理成功';
$_LANG['qita'] = '其他登錄方式';

$_LANG['is_show_help'] = '顯示氣泡';
$_LANG['is_open'] = '彈出窗';
$_LANG['login'] = '登錄';

$_LANG['eavl'] = '代碼執行結果';

$_LANG['copycode'] = '複製代碼';
$_LANG['help_evaljavascript'] = '請複製此段代碼到您的模版任意位置，放置前須知。 <br> 您必須先複製 {insert_scripts files="website.js"} 這段代碼到此段代碼的前面執行一次！否則後果自負';
?>