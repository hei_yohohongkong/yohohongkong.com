<?php
/**
 * ECSHOP 積分兌換產品語言項
 * ============================================================================
 * * 版權所有 2005-2012 上海商派網絡科技有限公司，並保留所有權利。
 * 網站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 這不是一個自由軟件！您只能在不用於商業目的的前提下對程序代碼進行修改和
 * 使用；不允許對程序代碼以任何形式任何目的的再發佈。
 * ============================================================================
 * $Author: liubo $
 * $Id: exchange_goods.php 17217 2011-01-19 06:29:08Z liubo $
*/

$_LANG['title'] ='關鍵字';
$_LANG['goods_id'] ='編號';
$_LANG['goods_name'] ='產品';
$_LANG['exchange_integral'] = '使用積分值';
$_LANG['is_exchange'] ='是否可兌換';
$_LANG['is_hot'] ='是否熱銷';
$_LANG['button_remove'] ='批量刪除';

$_LANG['keywords'] = '關鍵字';
$_LANG['goodsid'] ='產品';
$_LANG['make_option'] = '請先搜索產品生成選項列表';
$_LANG['integral'] ='積分值';
$_LANG['isexchange'] ='可兌換';
$_LANG['isnotexchange'] ='不可兌換';
$_LANG['ishot'] ='熱銷';
$_LANG['isnothot'] ='非熱銷';

$_LANG['notice_goodsid'] ='需要先搜索產品，生成產品列表，然後再選擇';
$_LANG['notice_integral'] ='兌換本產品需要消耗的積分值';

/* 提示信息 */
$_LANG['goods_exist'] ='產品已經存在';
$_LANG['back_list'] ='返回產品列表';
$_LANG['continue_add'] ='繼續添加新產品';
$_LANG['articleadd_succeed'] ='產品已經添加成功';
$_LANG['articleedit_succeed'] ='產品成功編輯';
$_LANG['drop_confirm'] = '您確認要刪除這件產品嗎？';
$_LANG['batch_remove_succeed'] = '您已經成功刪除 %d 件產品';
$_LANG['exchange_integral_invalid'] = '積分值為空或不是數字';

/*JS 語言項*/
$_LANG['js_languages']['no_goods_id'] = '沒有選擇產品';
$_LANG['js_languages']['invalid_exchange_integral'] = '積分值為空或不是數字';
?>