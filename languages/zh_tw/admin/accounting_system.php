<?php

$_LANG['mismatch_amount'] = 'Debit/Credit金額不符';
$_LANG['empty_amount'] = '未輸入Debit/Credit金額';
$_LANG['empty_account'] = '未選擇帳戶';
$_LANG['empty_date'] = '未選擇交易日期';
$_LANG['empty_record'] = '未輸入有效的Debit/Credit紀錄';
$_LANG['insert_fail'] = '新增交易失敗';
$_LANG['insert_success'] = '新增交易成功';

$_LANG['insert_type_empty_code'] = '未輸入代號';
$_LANG['insert_type_empty_name'] = '未輸入名稱';
$_LANG['insert_type_existing_code'] = '已使用此代號';
$_LANG['insert_type_invalid_code'] = '無法使用此代號';
$_LANG['insert_type_success'] = '新增帳戶成功';

$_LANG['accounting_types'] = '帳戶列表';
$_LANG['accounting_insert_type'] = '建立主類別';
$_LANG['accounting_insert_txn'] = '新增會計交易';
$_LANG['accounting_transaction_posted_list'] = '已入帳交易';

$_LANG['account_posting_status'][ACTG_POST_PENDING] = "Pending";
$_LANG['account_posting_status'][ACTG_POST_APPROVED] = "Posted";
$_LANG['account_posting_status'][ACTG_POST_REJECTED] = "Rejected";
?>