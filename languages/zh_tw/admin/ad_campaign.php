<?php

$_LANG['campaign_name'] = '廣告系列名稱';
$_LANG['campaign_desc'] = '廣告系列簡介';

$_LANG['created_at']    = '新增日期';
$_LANG['start_date']    = '開始日期';
$_LANG['end_date']      = '結束日期';
$_LANG['click_count']   = '點擊次數';
$_LANG['ads_stats']     = '生成訂單';

$_LANG['add_campaign']  = '新增廣告系列';
$_LANG['campaign_edit'] = '編輯廣告系列';
$_LANG['campaign_list'] = '廣告系列列表';
$_LANG['back_list']     = '回到列表';

$_LANG['edit_ok']       = '編輯成功';
$_LANG['add_ok']        = '新增成功';
$_LANG['is_enabled']        = '是';
$_LANG['no_enabled']        = '否';

$_LANG['campaign_not_available'] = '廣告系列不存在';
