<?php

/**
 * ECSHOP 程序說明
 * ===========================================================
 * * 版權所有 2005-2012 上海商派網絡科技有限公司，並保留所有權利。
 * 網站地址: http://www.ecshop.com；
 * ----------------------------------------------------------
 * 這不是一個自由軟件！您只能在不用於商業目的的前提下對程序代碼進行修改和
 * 使用；不允許對程序代碼以任何形式任何目的的再發佈。
 * ==========================================================
 * $Author: liubo $
 * $Id: affiliate.php 17217 2011-01-19 06:29:08Z liubo $
 */

$_LANG['on'] = '開啟';
$_LANG['off'] = '關閉';
$_LANG['single'] = '首次下單';
$_LANG['all'] = '所有訂單';

$_LANG['form_general'] = '基本設定';
$_LANG['form_referrer'] = '推薦人設定';
$_LANG['form_referee'] = '被推薦人設定';

$_LANG['separate_by'][0] = '層層疊推薦回贈';
$_LANG['separate_by'][1] = '「友」福同享';

$_LANG['expire'] = '推薦時效：';
$_LANG['affiliate_btn_img'] = '按鈕圖片：';
$_LANG['level_order_num'] = '優惠訂單數：';
$_LANG['level_order_amount'] = '訂單產品所需金額：';
$_LANG['level_point_all'] = '下單積分回贈總額：';
$_LANG['level_money_all'] = '下單現金回贈總額：';
$_LANG['level_point_all'] = '下單積分回贈總額：';
$_LANG['level_money_all'] = '下單現金回贈總額：';
$_LANG['level_register_all'] = '註冊積分回贈數：';
$_LANG['level_register_up'] = '等級積分回贈上限：';
$_LANG['level_point'] = '積分回贈百分比';
$_LANG['level_money'] = '現金回贈百分比';
$_LANG['instant_money_discount'] = '即時訂單現金折扣';
$_LANG['instant_point_refund'] = '即時訂單積分回贈';
$_LANG['auto_ck'] = '自動批准回贈';
$_LANG['edit_ok'] = '操作成功';
$_LANG['level_error'] = '最多可以設5個級別！';
$_LANG['levels'] = '推薦人級別';
$_LANG['js_languages']['lang_removeconfirm'] = '您確定要刪除這個等級麼？';
$_LANG['js_languages']['save'] = '保存';
$_LANG['js_languages']['cancel'] = '取消';

$_LANG['unit']['hour'] = '小時';
$_LANG['unit']['day'] = '天';
$_LANG['unit']['week'] = '周';

$_LANG['addrow'] = '增加';

$_LANG['all_null'] = '不能全為空';

$_LANG['help_expire'] = 'Cookie 存活時間，訪問者點擊某推薦人的網址後，在此時間段內成功註冊會被設定為該推薦人的所介紹的。（同時設定於推廣大使功能）';
$_LANG['help_loa'] = '訂單產品總金額需要到此金額才可享有推薦優惠。(0為不設上限)';
$_LANG['help_lpa'] = '訂單回贈積分。可輸入百份比(e.g. 1%)或直接輸入積分(e.g. 10)';
$_LANG['help_lma'] = '訂單回贈金額。可輸入百份比(e.g. 1%)或直接輸入金額(e.g. 10)';
$_LANG['help_lra'] = '推薦朋友註冊，推薦人即時能獲得的等級積分。';
$_LANG['help_lru'] = '等級積分到此上限則不再獎勵介紹註冊積分。(0為不設上限)';
$_LANG['help_lon'] = '是否只有首次下單才獲得獎勵(不計算已取消訂單)。';
$_LANG['help_ack'] = '當訂單付款後，是否允許自動批准回贈推薦人獎勵。';
$_LANG['help_imd'] = '推薦訂單將可獲得的即時現金折扣(將設於訂單折扣內),可輸入訂單產品總金額百份比(e.g. 1%)或直接輸入折扣金額(e.g. 10)';
$_LANG['help_ipr'] = '推薦訂單將可獲得的即時積分回贈, 可輸入百份比(e.g. 1%)或直接輸入積分(e.g. 10)';
?>