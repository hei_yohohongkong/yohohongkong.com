<?php

$_LANG['template_name']    = '保養條款名稱';
$_LANG['template_content'] = '保養條款內容';
$_LANG['template_content_notice'] = '請確保已同時輸入英文版本';
$_LANG['edit_warranty_templates'] = '編輯保養條款模板';
$_LANG['warranty_templates_list'] = '保養條款模板列表';