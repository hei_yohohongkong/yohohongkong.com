<?php

/**
 * ECSHOP 管理中心管理員操作內容語言文件
 * ============================================================================
 * * 版權所有 2005-2012 上海商派網絡科技有限公司，並保留所有權利。
 * 網站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 這不是一個自由軟件！您只能在不用於商業目的的前提下對程序代碼進行修改和
 * 使用；不允許對程序代碼以任何形式任何目的的再發佈。
 * ============================================================================
 * $Author: liubo $
 * $Id: log_action.php 17217 2011-01-19 06:29:08Z liubo $
*/

/*------------------------------------------------------ */
//-- 操作類型
/*------------------------------------------------------ */
$_LANG['log_action']['add'] = '添加';
$_LANG['log_action']['remove'] = '刪除';
$_LANG['log_action']['edit'] = '編輯';
$_LANG['log_action']['install'] = '安裝';
$_LANG['log_action']['uninstall'] = '卸載';
$_LANG['log_action']['setup'] = '設置';
$_LANG['log_action']['batch_remove'] = '批量刪除';
$_LANG['log_action']['trash'] = '回收';
$_LANG['log_action']['restore'] = '還原';
$_LANG['log_action']['batch_trash'] = '批量回收';
$_LANG['log_action']['batch_restore'] = '批量還原';
$_LANG['log_action']['batch_upload'] = '批量上傳';
$_LANG['log_action']['batch_edit'] = '批量編輯';
$_LANG['log_action']['submit_edit'] = '提交財務';
$_LANG['log_action']['withdraw_edit'] = '退回財務';
$_LANG['log_action']['submit_rate'] = '提交審核';
$_LANG['log_action']['withdraw_rate'] = '退回審核';
$_LANG['log_action']['approve'] = '審核';
$_LANG['log_action']['unapprove'] = '反審核';
$_LANG['log_action']['paid'] = '付款';
$_LANG['log_action']['complete'] = '完成';

/*------------------------------------------------------ */
//-- 操作內容
/*------------------------------------------------------ */
$_LANG['log_action']['users'] = '會員賬號';
$_LANG['log_action']['shipping'] = '配送方式';
$_LANG['log_action']['shipping_area'] = '配送區域';
$_LANG['log_action']['area_region'] = '配送區域中的地區';
$_LANG['log_action']['brand'] = '品牌管理';
$_LANG['log_action']['category'] = '產品分類';
$_LANG['log_action']['pack'] = '產品包裝';
$_LANG['log_action']['card'] = '產品賀卡';
$_LANG['log_action']['articlecat'] = '資訊分類';
$_LANG['log_action']['article'] = '資訊';
$_LANG['log_action']['shophelp'] = '網店幫助資訊';
$_LANG['log_action']['shophelpcat'] = '網店幫助分類';
$_LANG['log_action']['shopinfo'] = '網店信息資訊';
$_LANG['log_action']['attribute'] = '屬性';
$_LANG['log_action']['privilege'] = '權限管理';
$_LANG['log_action']['user_rank'] = '會員等級';
$_LANG['log_action']['snatch'] = '奪寶奇兵';
$_LANG['log_action']['bonustype'] = '紅包類型';
$_LANG['log_action']['userbonus'] = '用戶紅包';
$_LANG['log_action']['vote'] = '在線調查';
$_LANG['log_action']['friendlink'] = '友情鏈接';
$_LANG['log_action']['goods'] = '產品';
$_LANG['log_action']['payment'] = '支付方式';
$_LANG['log_action']['order'] = '訂單';
$_LANG['log_action']['erp_order'] = '採購訂單';
$_LANG['log_action']['erp_order_item'] = '採購訂單產品';
$_LANG['log_action']['agency'] = '辦事處';
$_LANG['log_action']['auction'] = '拍賣活動';
$_LANG['log_action']['favourable'] = '優惠活動';
$_LANG['log_action']['wholesale'] = '批發活動';

$_LANG['log_action']['adminlog'] = '操作日誌';
$_LANG['log_action']['admin_message'] = '管理員留言';
$_LANG['log_action']['users_comment'] = '用家評價';
$_LANG['log_action']['ads_position'] = '廣告位置';
$_LANG['log_action']['ads'] = '廣告';
$_LANG['log_action']['area'] = '地區';

$_LANG['log_action']['group_buy'] = '團購產品';
$_LANG['log_action']['goods_type'] = '產品類型';
$_LANG['log_action']['goods_stock'] = '產品庫存';
$_LANG['log_action']['booking'] = '缺貨登記管理';
$_LANG['log_action']['tag_manage'] = '標籤管理';
$_LANG['log_action']['shop_config'] = '商店設置';
$_LANG['log_action']['languages'] = '前台語言項';
$_LANG['log_action']['user_surplus'] = '會員餘額';
$_LANG['log_action']['message'] = '會員留言';
$_LANG['log_action']['fckfile'] = 'FCK文件';

$_LANG['log_action']['db_backup'] = '數據庫備份';

$_LANG['log_action']['package'] = '套裝優惠';

$_LANG['log_action']['exchange_goods'] = '積分可兌換的產品';

$_LANG['log_action']['suppliers'] = '供貨商管理';

$_LANG['log_action']['reg_fields'] = '會員註冊項';

$_LANG['log_action']['role_change'] = '更改角色';
$_LANG['log_action']['role_change_target'] = '管理員 ID';
$_LANG['log_action']['role_change_to'] = '新角色 ID';

$_LANG['log_action']['salesperson_status'] = '銷售員狀態';
$_LANG['log_action']['salesperson_action'] = '銷售員操作';
$_LANG['log_action']['create_from_admin'] = '從AdminUser新增';
$_LANG['log_action']['activate'] = '啟用';
$_LANG['log_action']['deactivate'] = '停用';
$_LANG['log_action']['archive'] = '封存';
?>