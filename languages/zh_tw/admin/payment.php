<?php

/**
 * ECSHOP 管理中心支付方式管理語言文件
 * ============================================================================
 * * 版權所有 2005-2012 上海商派網絡科技有限公司，並保留所有權利。
 * 網站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 這不是一個自由軟件！您只能在不用於商業目的的前提下對程序代碼進行修改和
 * 使用；不允許對程序代碼以任何形式任何目的的再發佈。
 * ============================================================================
 * $Author: liubo $
 * $Id: payment.php 17217 2011-01-19 06:29:08Z liubo $
*/

$_LANG['payment'] = '支付方式';
$_LANG['payment_rule'] = '支付規則';
$_LANG['payment_name'] = '支付方式名稱';
$_LANG['payment_display_name'] = '前台支付方式顯示名稱';
$_LANG['is_pos'] = 'POS';
$_LANG['is_online'] = '在線支付';
$_LANG['is_online_pay'] = '線上支付';
$_LANG['version'] = '插件版本';
$_LANG['payment_desc'] = '支付方式描述';
$_LANG['short_pay_fee'] = '費用';
$_LANG['payment_author'] = '插件作者';
$_LANG['payment_is_cod'] = '貨到付款？';
$_LANG['payment_is_online'] = '在線支付？';
$_LANG['payment_is_pos'] = 'POS顯示？';
$_LANG['payment_platform'] = '顯示平台';
$_LANG['payment_is_online_pay'] = '線上支付?';

$_LANG['web'] = '網站';
$_LANG['mobile'] = '手機版';

$_LANG['name_is_null'] = '您沒有輸入支付方式名稱！';
$_LANG['name_exists'] = '該支付方式名稱已存在！';

$_LANG['pay_fee'] = '支付手續費';
$_LANG['back_list'] = '返回支付方式列表';
$_LANG['install_ok'] = '安裝成功';
$_LANG['edit_ok'] = '編輯成功';
$_LANG['uninstall_ok'] = '卸載成功';

$_LANG['invalid_pay_fee'] = '支付費用不是一個合法的價格';
$_LANG['decide_by_ship'] = '配送決定';

$_LANG['edit_after_install'] = '該支付方式尚未安裝，請你安裝後再編輯';
$_LANG['payment_not_available'] = '該支付插件不存在或尚未安裝';

$_LANG['js_languages']['lang_removeconfirm'] = '您確定要卸載該支付方式嗎？';

$_LANG['ctenpay']           = '立即註冊財付通商戶號';
$_LANG['ctenpay_url']       = 'http://union.tenpay.com/mch/mch_register_b2c.shtml?sp_suggestuser=542554970';
$_LANG['ctenpayc2c_url']    = 'https://www.tenpay.com/mchhelper/mch_register_c2c.shtml?sp_suggestuser=542554970';
$_LANG['tenpay']  = '即時到賬';
$_LANG['tenpayc2c'] = '中介擔保';

$_LANG['dualpay'] = '標準雙接口';
$_LANG['escrow'] = '擔保交易接口';
$_LANG['fastpay'] = '即時到帳交易接口';
$_LANG['alipay_pay_method'] = '選擇接口類型：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
$_LANG['getPid'] = '獲取Pid、Key';

$_LANG['rule_type'] = '規則類型';
$_LANG['rule_list'] = '規則列表';
$_LANG['rule_detail'] = '規則內容';
$_LANG['sub_rule'] = '共同規則';
$_LANG['not_display'] = '顯示&nbsp';
$_LANG['new_rule'] = '新增規則';
$_LANG['back_rule_list'] = '返回支付規則列表';
$_LANG['rule_not_available'] = '該支付規則不存在';

/* Type */
$_LANG['type_country'] = '送貨國家';
$_LANG['type_price'] = '產品總價';
$_LANG['type_rank'] = '會員積分';

/* Equivalent */
$_LANG['eq'] = '等於';
$_LANG['neq'] = '不等於';
$_LANG['gt'] = '大於';
$_LANG['lt'] = '小於';
$_LANG['gte'] = '大於等於';
$_LANG['lte'] = '小於等於';

?>
