<?php
$_LANG['auto_pricing_product_list'] = '自動定價主頁';
$_LANG['auto_pricing_product_edit'] = '編輯產品定價策略';
$_LANG['auto_pricing_strategy_list'] = '定價策略';
$_LANG['auto_pricing_strategy_add'] = '新增定價策略';
$_LANG['auto_pricing_strategy_edit'] = '編輯定價策略';
$_LANG['auto_pricing_strategy_default'] = '預設定價策略';
$_LANG['auto_pricing_rule_list'] = '排除產品';
$_LANG['auto_pricing_rule_add'] = '新增排除產品';
$_LANG['auto_pricing_rule_edit'] = '編輯排除產品';
$_LANG['include_goods_hint'] = '產品名稱/ID 可用逗號,分隔多個名稱或ID';

$_LANG['formatted_status'][AUTO_PRICE_STATUS_SUGGESTED] = "等待審核";
$_LANG['formatted_status'][AUTO_PRICE_STATUS_APPLIED] = "進行中";
$_LANG['formatted_status'][AUTO_PRICE_STATUS_REJECTED] = "已忽略";
$_LANG['formatted_status'][AUTO_PRICE_STATUS_EXPIRED] = "已完結";
?>