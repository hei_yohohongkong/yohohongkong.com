<?php
//table
$_LANG['created_at'] = '新增日期';
$_LANG['goods_name'] = '產品名稱';
$_LANG['market_price'] = '市場價錢';

/* 訂單搜索 */
$_LANG['label_consignee'] = '聯絡人：';
$_LANG['label_email'] = '電子郵件：';
$_LANG['label_address'] = '地址：';
$_LANG['label_tel'] = '電話：';
$_LANG['consignee_info'] = '報價單聯絡信息';
$_LANG['goods_info'] = '產品信息';
$_LANG['goods_name_brand'] = '產品名稱 [ 品牌 ]';
$_LANG['goods_sn'] = '貨號';
$_LANG['goods_price'] = '價格';
$_LANG['goods_number'] = '數量';
$_LANG['goods_attr'] = '屬性';
$_LANG['goods_delivery'] = '已出貨數量';
$_LANG['goods_delivery_curr'] = '此單出貨數量';
$_LANG['storage'] = '庫存';
$_LANG['subtotal'] = '小計';
$_LANG['label_total'] = '合計：';
$_LANG['company_name']  = "公司名稱";
$_LANG['contact_tel']   = "聯絡電話";
$_LANG['contact_name']  = "聯絡名稱";
$_LANG['contact_email'] = "聯絡電郵";
$_LANG['quotation_sn'] = "報價單編號";
$_LANG['quotation_fee'] = "總數";
?>
