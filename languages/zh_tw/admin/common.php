<?php

/**
 * ECSHOP 管理中心共用語言文件
 * ============================================================================
 * * 版權所有 2005-2012 上海商派網絡科技有限公司，並保留所有權利。
 * 網站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 這不是一個自由軟件！您只能在不用於商業目的的前提下對程序代碼進行修改和
 * 使用；不允許對程序代碼以任何形式任何目的的再發佈。
 * ============================================================================
 * $Author: liubo $
 * $Id: common.php 17217 2011-01-19 06:29:08Z liubo $
*/
$_LANG['website'] = '第三方登錄插件管理';
$_LANG['fuzhi'] = '產品標籤';
$_LANG['app_name'] = 'ECSHOP';
$_LANG['cp_home'] = '友和網店管理系統';
$_LANG['copyright'] = '&copy; 2014 友和 YOHO, 保留所有的權利';
$_LANG['query_info'] = '共執行 %d 個查詢，用時 %s 秒';
$_LANG['online_user'] = '在線 %d 人(誤差值為3分鐘)';
$_LANG['memory_info'] = '，內存佔用 %0.3f MB';
$_LANG['gzip_enabled'] = '，Gzip 已啟用';
$_LANG['gzip_disabled'] = '，Gzip 已禁用';
$_LANG['loading'] = '正在處理您的請求...';
$_LANG['js_languages']['process_request'] = '正在處理您的請求...';
$_LANG['js_languages']['todolist_caption'] = '記事本';
$_LANG['js_languages']['todolist_autosave'] = '自動保存';
$_LANG['js_languages']['todolist_save'] = '保存';
$_LANG['js_languages']['todolist_clear'] = '清除';
$_LANG['js_languages']['todolist_confirm_save'] = '是否將更改保存到記事本？';
$_LANG['js_languages']['todolist_confirm_clear'] = '是否清空內容？';
$_LANG['auto_redirection'] = '如果您不做出選擇，將在 <span id="spanSeconds">3</span> 秒後跳轉到第一個鏈接地址。';
$_LANG['password_rule'] = '密碼應只包含英文字符、數字.長度在6--16位之間';
$_LANG['username_rule'] = '用戶名應為漢字、英文字符、數字組合，3到15位';
$_LANG['plugins_not_found'] = '插件 %s 無法定位';
$_LANG['no_records'] = '沒有找到任何記錄';
$_LANG['role_describe'] = '角色描述';
$_LANG['preview'] = '查看網店';
$_LANG['signout'] = '退出';
$_LANG['profile'] = '個人設置';
$_LANG['clear_cache'] = '清除緩存';

$_LANG['require_field'] = '<span class="require-field">*</span>';
$_LANG['yes'] = '是';
$_LANG['no'] = '否';
$_LANG['record_id'] = '編號';
$_LANG['handler'] = '操作';
$_LANG['install'] = '安裝';
$_LANG['uninstall'] = '卸載';
$_LANG['list'] = '列表';
$_LANG['add'] = '添加';
$_LANG['edit'] = '編輯';
$_LANG['view'] = '查看';
$_LANG['remove'] = '移除';
$_LANG['drop'] = '刪除';
$_LANG['confirm_delete'] = '您確定要刪除嗎？';
$_LANG['disabled'] = '禁用';
$_LANG['enabled'] = '啟用';
$_LANG['setup'] = '設置';
$_LANG['success'] = '成功';
$_LANG['sort_order'] = '排序';
$_LANG['trash'] = '回收站';
$_LANG['restore'] = '還原';
$_LANG['close_window'] = '關閉窗口';
$_LANG['btn_select'] = '選擇';
$_LANG['operator'] = '操作人';
$_LANG['cancel'] = '取消';

$_LANG['empty'] = '不能為空';
$_LANG['repeat'] = '已存在';
$_LANG['is_int'] = '應該為整數';

$_LANG['button_submit'] = ' 確定 ';
$_LANG['button_save'] = ' 保存 ';
$_LANG['button_reset'] = ' 重置 ';
$_LANG['button_search'] = ' 搜尋 ';

$_LANG['priv_error'] = '對不起,您沒有執行此項操作的權限!';
$_LANG['drop_confirm'] = '您確認要刪除這條記錄嗎?';
$_LANG['form_notice'] = '點擊此處查看提示信息';
$_LANG['upfile_type_error'] = '上傳文件的類型不正確!';
$_LANG['upfile_error'] = '上傳文件失敗!';
$_LANG['no_operation'] = '您沒有選擇任何操作';

$_LANG['go_back'] = '返回上一頁';
$_LANG['back'] = '返回';
$_LANG['continue'] = '繼續';
$_LANG['system_message'] = '系統信息';
$_LANG['check_all'] = '全選';
$_LANG['select_please'] = '請選擇...';
$_LANG['all_category'] = '所有分類';
$_LANG['all_brand'] = '所有品牌';
$_LANG['refresh'] = '刷新';
$_LANG['update_sort'] = '更新排序';
$_LANG['modify_failure'] = '修改失敗!';
$_LANG['attradd_succed'] = '操作成功!';
$_LANG['todolist'] = '記事本';
$_LANG['n_a'] = 'N/A';

/* 提示 */
$_LANG['sys']['wrong'] = '錯誤：';

/* 編碼 */
$_LANG['charset']['utf8'] = '國際化編碼（utf8）';
$_LANG['charset']['zh_cn'] = '簡體中文';
$_LANG['charset']['zh_tw'] = '繁體中文';
$_LANG['charset']['en_us'] = '美國英語';
$_LANG['charset']['en_uk'] = '英文';

/* 新訂單通知 */
$_LANG['order_notify'] = '新訂單通知';
$_LANG['new_order_1'] = '您有 ';
$_LANG['new_order_2'] = ' 個新訂單以及 ';
$_LANG['new_order_3'] = ' 個新付款的訂單';
$_LANG['new_order_link'] = '點擊查看新訂單';

/*語言項*/
$_LANG['chinese_simplified'] = '簡體中文';
$_LANG['english'] = '英文';

/* 分頁 */
$_LANG['total_records'] = '總計 ';
$_LANG['total_pages'] = '個記錄分為';
$_LANG['page_size'] = '頁，每頁';
$_LANG['page_current'] = '頁當前第';
$_LANG['page_first'] = '第一頁';
$_LANG['page_prev'] = '上一頁';
$_LANG['page_next'] = '下一頁';
$_LANG['page_last'] = '最末頁';
$_LANG['admin_home'] = '起始頁';

/* 重量 */
$_LANG['gram'] = '克';
$_LANG['kilogram'] = '千克';

/* 菜單分類部分 */
//  新增管理中
$_LANG['01_new'] = '新贈管理';
$_LANG['02_cat_and_goods'] = '產品管理';
// $_LANG['03_promotion'] = '促銷管理';
$_LANG['03_order'] = '訂單管理';
$_LANG['04_market'] = '市場推廣';
// $_LANG['05_banner'] = '廣告管理';
$_LANG['06_stats'] = '報表統計';
// $_LANG['07_content'] = '資訊管理';
$_LANG['08_members'] = '會員管理';
// $_LANG['09_others'] = '雜項管理';
$_LANG['09_priv_admin'] = 'HRM系統';
$_LANG['11_system'] = '系統設置';
$_LANG['12_template'] = '模板管理';
$_LANG['13_backup'] = '數據庫管理';
$_LANG['14_sms'] = '短信管理';
$_LANG['15_rec'] = '推薦管理';
$_LANG['16_email_manage'] = '郵件群發管理';

/* 產品管理 */



$_LANG['01_flgs'] = '分類歸屬';
$_LANG['02_xilie'] = '系列分類';
$_LANG['03_goodsbiao'] = 'GOODs工具表';



$_LANG['01_goods_list'] = '產品列表';
$_LANG['02_goods_add'] = '添加新產品';
$_LANG['03_category_list'] = '產品分類';
$_LANG['04_category_add'] = '添加分類';
$_LANG['05_comment_manage'] = '用家評價';
$_LANG['06_goods_brand_list'] = '產品品牌';
$_LANG['07_brand_add'] = '添加品牌';
$_LANG['08_attribute'] = '產品屬性';
$_LANG['08_goods_type'] = '產品類型';
$_LANG['09_attribute_list'] = '產品屬性';
$_LANG['10_attribute_add'] = '添加屬性';
$_LANG['11_goods_trash'] = '產品回收站';
$_LANG['12_batch_pic'] = '圖片批量處理';
$_LANG['13_batch_add'] = '產品批量上傳';
$_LANG['15_batch_edit'] = '產品批量修改';
$_LANG['16_goods_script'] = '生成產品代碼';
$_LANG['17_tag_manage'] = '標籤管理';
$_LANG['18_product_list'] = '貨品列表';
$_LANG['52_attribute_add'] = '編輯屬性';
$_LANG['53_suppliers_goods'] = '供貨商產品管理';
$_LANG['staff_purchase_list'] = '員工優惠列表';
$_LANG['staff_purchase_add'] = '申請員工優惠';
$_LANG['staff_purchase_info'] = '員工優惠申請資訊';
$_LANG['image_compression_product'] = '舊產品圖片處理';
$_LANG['download_userslist_xlsx'] = '下載會員列表';
$_LANG['goods_shipping_dimension_import'] = '匯入產品尺寸資料';

$_LANG['14_goods_export'] = '產品批量導出';
$_LANG['goods_cost_log'] = '產品成本變動紀錄';
$_LANG['goods_price_log'] = '產品價格變動紀錄';
$_LANG['warranty_templates'] = '保養條款模版';
$_LANG['auto_pricing'] = '產品自動定價';

$_LANG['50_virtual_card_list'] = '虛擬產品列表';
$_LANG['51_virtual_card_add'] = '添加虛擬產品';
$_LANG['52_virtual_card_change'] = '更改加密串';
$_LANG['goods_auto'] = '產品自動上下架';
$_LANG['article_auto'] = '資訊自動發佈';
$_LANG['navigator'] = '自定義導航欄';
$_LANG['holidays'] = '公眾假期列表';

/* 促銷管理 */
$_LANG['02_snatch_list'] = '奪寶奇兵';
$_LANG['snatch_add'] = '添加奪寶奇兵';
$_LANG['04_bonustype_list'] = '紅包類型';
$_LANG['bonustype_add'] = '添加紅包類型';
$_LANG['05_bonus_list'] = '線下紅包';
$_LANG['bonus_add'] = '添加會員紅包';
$_LANG['06_pack_list'] = '產品包裝';
$_LANG['07_card_list'] = '祝福賀卡';
$_LANG['pack_add'] = '添加新包裝';
$_LANG['card_add'] = '添加新賀卡';
$_LANG['08_group_buy'] = '團購活動';
$_LANG['09_topic'] = '優惠資訊';
$_LANG['topic_add'] = '添加專題';
$_LANG['topic_list'] = '專題列表';
$_LANG['10_auction'] = '拍賣活動';
$_LANG['12_favourable'] = '優惠活動';
$_LANG['13_wholesale'] = '批發管理';
$_LANG['ebao_commend'] = '易寶推薦';
$_LANG['14_package_list'] = '套裝優惠';
$_LANG['package_add'] = '添加套裝優惠';
$_LANG['16_coupons'] = '優惠券';
$_LANG['17_flashdeals'] = '閃購管理';
$_LANG['flashdeal_events'] = '閃購活動';
$_LANG['18_package'] = '套裝優惠';
$_LANG['lucky_draw'] = '抽獎管理';
$_LANG['support_manage'] = 'Q&A管理';
$_LANG['global_alert'] = '公告管理';
//$_LANG['19_mkt_method'] = '營銷玩法';
//Powered By UUECS QQ909065309 HTTP://Www.UUECS.Com
$_LANG['01_richang'] = '日常工作';
$_LANG['02_dai'] = '<font class="red">缺貨登記</font>';
$_LANG['02_book'] = '<font class="red">預售登記</font>';
$_LANG['031_plans'] = '<font color=#00ACEE>工作計劃</font>';
$_LANG['03_taskes'] = '<font class="blue">工作清單</font>';
$_LANG['04_redmine'] = '<font color=#00ACEE>專案管理系統</font>';
$_LANG['05_refund_requests'] = '退款及積分申請';
$_LANG['06_pending_receive'] = '即將派送貨物清單';
$_LANG['07_repair_orders'] = '維修單列表';
$_LANG['07_replace_orders'] = '換貨單列表';
$_LANG['08_goods_feedback_problem'] = '產品問題回報';
$_LANG['09_goods_feedback_price'] = '產品售價提議';
$_LANG['10_abnormal_cost_log'] = '成本異常';
$_LANG['11_suggested_promotions'] = '特價建議';
$_LANG['sales_agent_manage'] = 'B2B2C批量入單';
$_LANG['deal_log'] = '保養登記';
$_LANG['sale_cross_check'] = '門店對數';
$_LANG['order_picker_packer_import'] = '匯入出貨人員資料';
$_LANG['date'] = '日期';
$_LANG['rma'] = 'RMA列表';
$_LANG['rsp_record'] = 'WEEE回收記錄';
$_LANG['goal_setting'] = 'KPI輔助';
$_LANG['shop_restore'] = '門市補貨';
$_LANG['team_kpi'] = '團隊KPI';

/* 訂單管理 */
$_LANG['02_order_list'] = '訂單列表';
$_LANG['order_ship'] = '出貨列表';
$_LANG['03_order_query'] = '訂單查詢';
$_LANG['04_merge_order'] = '合併訂單';
$_LANG['05_edit_order_print'] = '訂單打印';
$_LANG['06_undispose_booking'] = '缺貨登記';
$_LANG['08_add_order'] = '添加訂單';
$_LANG['09_delivery_order'] = '物流單列表';
$_LANG['10_back_order'] = '退貨單列表';
$_LANG['order_picker_packer_import'] = '匯入出貨人員資料';
$_LANG['order_goods_sn'] = '輸入產品序號';
$_LANG['order_goods_cross_check'] = '訂單產品核對';
$_LANG['stock_transfer_cross_check'] = '調貨產品核對';
$_LANG['order_express_collected_import'] = '匯入訂單速遞已收件';

/* 廣告管理 */
$_LANG['ad_position'] = '廣告位置';
$_LANG['ad_list'] = '廣告列表';
$_LANG['ad_campaign'] = '廣告系列列表';
$_LANG['ad_pospromote'] = 'POS廣告';
$_LANG['homepage_carousel'] = '首頁廣告輪播';

/* 報表統計 */
$_LANG['flow_stats'] = '流量分析';
$_LANG['searchengine_stats'] = '搜尋引擎';
$_LANG['report_order'] = '訂單統計';
$_LANG['report_sell'] = '銷售概況';
$_LANG['sell_stats'] = '銷售排行';
$_LANG['sale_list'] = '銷售明細';
$_LANG['report_guest'] = '客戶統計';
$_LANG['report_users'] = '會員排行';
$_LANG['report_kpi'] = 'KPI(Other)';
$_LANG['report_kpi_market'] = 'KPI(Market Team)';
$_LANG['report_ex_pos'] = '展場報告';
$_LANG['visit_buy_per'] = 'Big Data - 瀏覽轉換率';
$_LANG['z_clicks_stats'] = '站外投放JS';
$_LANG['sale_commission'] = '佣金統計';
$_LANG['stock_cost'] = '分類報告';
$_LANG['stock_cost_brand'] = '品牌報告';
$_LANG['stock_cost_supplier'] = '供應商報告';
$_LANG['supplier_turnover'] = '供應商Turnover';
$_LANG['warehouse_turnover'] = '貨倉Turnover';
$_LANG['report_logistics'] = '物流報告';
$_LANG['stock_cost_log'] = '庫存成本紀錄';
$_LANG['sale_profit'] = '盈利統計';
$_LANG['sale_payment'] = 'B2C對數';
$_LANG['review_shop'] = '友和評價';
$_LANG['review_sales'] = '員工評價';
$_LANG['review_cs'] = '客服評價';
$_LANG['employee_evaluation'] = '員工評核';
$_LANG['trends_keywords'] = 'Big Data - 關鍵字趨勢';
$_LANG['report_order_address'] = 'Big Data - 地址';
$_LANG['goods_stats'] = 'Big Data - 產品';
$_LANG['order_wholesale_delivery'] = '批發單執貨列表';
$_LANG['order_list_rank_program'] = '會員計劃訂單列表';
$_LANG['order_progress_report'] = '出貨進度';
$_LANG['sql_report'] = 'Data Export';
$_LANG['supplier_stock_report'] = '供應商銷存報告';

/* 資訊管理 */
$_LANG['02_articlecat_list'] = '資訊分類';
$_LANG['articlecat_add'] = '添加資訊分類';
$_LANG['03_article_list'] = '資訊列表';
$_LANG['article_add'] = '添加新資訊';
$_LANG['shop_article'] = '網店資訊';
$_LANG['shop_info'] = '網店信息';
$_LANG['shop_help'] = '網店幫助';
$_LANG['vote_list'] = '在線調查';
$_LANG['support_list'] = 'Q&A分類';
$_LANG['support_add'] = 'Q&A';
$_LANG['support_list_add'] = '添加Q&A分類';
$_LANG['back_support_list'] = '返回Q&A分類';
$_LANG['support_cat_edit'] = '編輯Q&A分類';
$_LANG['global_alert_list'] = '公告列表';

/* 會員管理 */
$_LANG['08_unreply_msg'] = '會員留言';
$_LANG['03_users_list'] = '會員列表';
$_LANG['04_users_add'] = '添加會員';
$_LANG['05_user_rank_list'] = '會員等級';
$_LANG['06_user_rank_program'] = '特殊會員計劃';
$_LANG['06_list_integrate'] = '會員整合';
$_LANG['09_user_account'] = '充值和提現申請';
$_LANG['10_user_account_manage'] = '資金管理';
$_LANG['11_user_export'] = '導出會員電郵';
$_LANG['12_upcoming_birthdays'] = '即將生日的會員';
$_LANG['13_wishlist'] = '願望清單';
$_LANG['account_log'] = '會員積分變動紀錄';

/* 權限管理 */
$_LANG['admin_list'] = '管理員列表';
$_LANG['admin_list_role'] = '角色列表';
$_LANG['admin_role'] = '角色管理';
$_LANG['admin_add'] = '添加管理員';
$_LANG['admin_add_role'] = '添加角色';
$_LANG['admin_edit_role'] = '修改角色';
$_LANG['admin_logs'] = '管理員日誌';
$_LANG['agency_list'] = '辦事處列表';
$_LANG['suppliers_list'] = '供貨商列表';
$_LANG['admin_edit_nav_list'] = '修改角色導航欄';

/* 系統設置 */
$_LANG['01_shop_config'] = '商店設置';
$_LANG['shop_authorized'] = '授權證書';
$_LANG['shp_webcollect'] = '網羅天下';
$_LANG['02_payment_list'] = '支付方式';
$_LANG['03_shipping_list'] = '配送方式';
$_LANG['04_mail_settings'] = '郵件服務器設置';
$_LANG['05_area_list'] = '地區列表';
$_LANG['07_cron_schcron'] = '計劃任務';
$_LANG['08_friendlink_list'] = '友情鏈接';
$_LANG['09_logistics_list'] = '物流列表';
$_LANG['shipping_area_list'] = '配送區域';
$_LANG['sitemap'] = '站點地圖';
$_LANG['check_file_priv'] = '文件權限檢測';
$_LANG['captcha_manage'] = '驗證碼管理';
$_LANG['fckfile_manage'] = 'Fck上傳文件管理';
$_LANG['ucenter_setup'] = 'UCenter設置';
$_LANG['file_check'] = '文件校驗';
$_LANG['021_reg_fields'] = '會員註冊項設置';
$_LANG['022_insurance_service'] = '保險服務';
$_LANG['menu_022_insurance_service'] = '保險服務';

/* 模板管理 */
$_LANG['02_template_select'] = '模板選擇';
$_LANG['03_template_setup'] = '設置模板';
$_LANG['04_template_library'] = '庫項目管理';
$_LANG['mail_template_manage'] = '郵件模板';
$_LANG['05_edit_languages'] = '語言項編輯';
$_LANG['06_template_backup'] = '模板設置備份';
/* 數據庫管理 */
$_LANG['02_db_manage'] = '數據備份';
$_LANG['03_db_optimize'] = '數據表優化';
$_LANG['04_sql_query'] = 'SQL查詢';
$_LANG['05_synchronous'] = '同步數據';
$_LANG['convert'] = '轉換數據';

/* 短信管理 */
$_LANG['02_sms_my_info'] = '賬號信息';
$_LANG['03_sms_send'] = '發送短信';
$_LANG['04_sms_charge'] = '賬戶充值';
$_LANG['05_sms_send_history'] = '發送記錄';
$_LANG['06_sms_charge_history'] = '充值記錄';

/* 推薦管理 */
$_LANG['affiliate'] = '會員推薦計劃設置';
$_LANG['affiliate_ck'] = '會員推薦回贈管理';
$_LANG['affiliateyoho'] = '推廣大使列表';
$_LANG['affiliateyoho_orders'] = '推廣訂單列表';
$_LANG['affiliateyoho_rewards'] = '推廣獎勵列表';

$_LANG['flashplay'] = '首頁主廣告管理';
$_LANG['search_log'] = '搜尋關鍵字';
$_LANG['email_list'] = '郵件訂閱管理';
$_LANG['magazine_list'] = '雜誌管理';
$_LANG['attention_list'] = '關注管理';
$_LANG['view_sendlist'] = '郵件隊列管理';
$_LANG['mail_sent_log'] = '已發送郵件紀錄';
$_LANG['edm_generator'] = '促銷郵件產生器';

/* 積分兌換管理 */
$_LANG['15_exchange_goods'] = '積分商城產品';
$_LANG['15_exchange_goods_list'] = '積分商城產品列表';
$_LANG['exchange_goods_add'] = '添加新產品';

/* Accounting System */
$_LANG['10_accounting_system'] = '會計系統';
$_LANG['actg_account_summary'] = '帳戶概覽';
$_LANG['actg_transactions'] = 'Book Records';
$_LANG['actg_shop_expenses'] = '門店支出';
$_LANG['actg_currency_exchanges'] = '轉帳紀錄';
$_LANG['actg_payment_wholesale'] = '批發單付款紀錄';
$_LANG['actg_payment_purchase'] = '採購單付款紀錄';
$_LANG['actg_wholesale_orders'] = '批發訂單列表';
$_LANG['actg_xaccount_receivable'] = '批發單應收賬款';
$_LANG['actg_xaccount_payable'] = '採購單應付賬款';
$_LANG['balance_sheet'] = 'Balance Sheet';
$_LANG['balance_sheet_woc'] = 'Balance Sheet(虧欠顧客)';
$_LANG['balance_sheet_sou'] = 'Balance Sheet(供應商虧欠)';
$_LANG['balance_sheet_wosn'] = 'Balance Sheet(虧欠非寄賣供應商)';
$_LANG['balance_sheet_wosc'] = 'Balance Sheet(虧欠寄賣供應商)';
$_LANG['balance_sheet_wows'] = 'Balance Sheet(虧欠批發會員)';
$_LANG['balance_sheet_wsou'] = 'Balance Sheet(批發會員虧欠)';
$_LANG['credit_note_summary'] = 'Credit Note概覽';
$_LANG['income_statement'] = 'Income Statement';
$_LANG['cash_flow_statement'] = 'Cash Flow Statement';
$_LANG['bank_reconciliation'] = 'Bank Records';
$_LANG['bank_reconciliation_match'] = 'Reconciliation';
$_LANG['accounting_system'] = '會計紀錄';
$_LANG['trial_balance'] = 'Trial Balance';

/* shop management */
$_LANG['12_shop'] = '門店管理';
$_LANG['product_commission'] = '產品佣金及銷售情況';
$_LANG['shop_commission'] = '門市佣金計算';
$_LANG['shop_salary'] = '門市薪資計算';
$_LANG['commission_setting'] = '佣金設置';
$_LANG['sale_cash_check'] = '門店現金對數';

/* cls_image類的語言項 */
$_LANG['directory_readonly'] = '目錄 % 不存在或不可寫';
$_LANG['invalid_upload_image_type'] = '不是允許的圖片格式';
$_LANG['upload_failure'] = '文件 %s 上傳失敗。';
$_LANG['missing_gd'] = '沒有安裝GD庫';
$_LANG['missing_orgin_image'] = '找不到原始圖片 %s ';
$_LANG['nonsupport_type'] = '不支持該圖像格式 %s ';
$_LANG['creating_failure'] = '創建圖片失敗';
$_LANG['writting_failure'] = '圖片寫入失敗';
$_LANG['empty_watermark'] = '水印文件參數不能為空';
$_LANG['missing_watermark'] = '找不到水印文件%s';
$_LANG['create_watermark_res'] = '創建水印圖片資源失敗。水印圖片類型為%s';
$_LANG['create_origin_image_res'] = '創建原始圖片資源失敗，原始圖片類型%s';
$_LANG['invalid_image_type'] = '無法識別水印圖片 %s ';
$_LANG['file_unavailable'] = '文件 %s 不存在或不可讀';

/* 郵件發送錯誤信息 */
$_LANG['smtp_setting_error'] = '郵件服務器設置信息不完整';
$_LANG['smtp_connect_failure'] = '無法連接到郵件服務器 %s';
$_LANG['smtp_login_failure'] = '郵件服務器驗證帳號或密碼不正確';
$_LANG['sendemail_false'] = '郵件發送失敗，請檢查您的郵件服務器設置！';
$_LANG['smtp_refuse'] = '服務器拒絕發送該郵件';
$_LANG['disabled_fsockopen'] = '服務器已禁用 fsocketopen 函數。';

/* 虛擬卡 */
$_LANG['virtual_card_oos'] = '虛擬卡已缺貨';

$_LANG['span_edit_help'] = '點擊修改內容';
$_LANG['href_sort_help'] = '點擊對列表排序';

$_LANG['catname_exist'] = '已存在相同的分類名稱!';
$_LANG['brand_name_exist'] = '已存在相同的品牌名稱!';

$_LANG['alipay_login'] = '<a href="https://www.alipay.com/user/login.htm?goto=https%3A%2F%2Fwww.alipay.com%2Fhimalayas%2Fpracticality_profile_edit.htm%3Fmarket_type%3Dfrom_agent_contract%26customer_external_id%3D%2BC4335319945672464113" target="_blank">立即免費申請支付接口權限</a>';
$_LANG['alipay_look'] = '<a href=\"https://www.alipay.com/himalayas/practicality.htm\" target=\"_blank\">請申請成功後登錄支付寶賬戶查看</a>';

/* Yahoo商店 */
$_LANG['yahoo_store'] = 'Yahoo商店';
$_LANG['add_yahoo_item'] = '新增Yahoo商品';
$_LANG['export_csv'] = '匯出更新價格產品CSV';

/* Sales Person */
$_LANG['21_salesperson_system'] = 'Sales Person';
$_LANG['sp_section_list'] = 'List All';
$_LANG['sp_section_add'] = 'Add Sales Person';
$_LANG['add_hktv_item'] = '新增HKTVMALL商品';
$_LANG[Yoho\cms\Controller\HktvmallController::DB_ACTION_CODE_HKTVMALL] = 'HKTVMALL';

$_LANG['quotation'] = '報價單';

$_LANG['22_danger_zone'] = 'Danger Zone';
$_LANG['danger_zone'] = 'Danger Zone';
$_LANG['add_logistics']  = '新增物流';
$_LANG['edit_logistics']  = '編輯物流';
$_LANG['logistics_name'] = '物流名稱';
$_LANG['logistics_desc'] = '物流簡介';
$_LANG['logistics_url']  = '網址連結';
$_LANG['no_logistics']   = '沒有物流';

$_LANG['shipping_templates'] = '安裝條款模版';
$_LANG['shipping_templates_edit'] = '編輯安裝條款';

$_LANG['wholesale_user_info'] = '批發訂單顧客資料';
$_LANG['wholesale_user_info_insert'] = '新增顧客資料';
$_LANG['image_compression_usages'] = 'Tiny使用量報告';
$_LANG['rma_report'] = 'RMA報告';
$_LANG['order_goods_cross_check_report'] = '訂單產品核對報告';
$_LANG['sql_report'] = 'Data Export';

$_LANG['section_crm_system'] = 'CRM系統';
$_LANG['crm_system'] = '客戶服務';
$_LANG['ivr_system'] = 'IVR';
$_LANG['op_system'] = '線下支付';

$_LANG['user_rank_program'] = '特殊會員計劃';
$_LANG['user_rank_validate'] = '會員計劃審核';

$_LANG['write_off'] = '產品報廢';
$_LANG['hold_order_message']['default'] = "Hold - 請等待審核";
$_LANG['hold_order_message']['user']    = "Hold - 可疑會員訂單";
$_LANG['hold_order_message']['order']   = "Hold - 高風險訂單";

$_LANG['wechatpay_force_refund'] = '微信支付強制退款';

$_LANG['unlink_bank_reconciliation'] = '解除對數紀錄';
$_LANG['accounting_action'] = '會計相關';

$_LANG['mkt_method'] = '營銷玩法';

$_LANG['esl_operation'] = 'ESL 同步管理';
?>
