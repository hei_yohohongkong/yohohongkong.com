<?php
$_LANG['crm_type'][CRM_REQUEST_UNCATEGORIZED]       = '未分類';
$_LANG['crm_type'][CRM_REQUEST_ORDER_QUERY]         = '售前查詢';
$_LANG['crm_type'][CRM_REQUEST_ORDER_PAYMENT]       = '訂單付款';
$_LANG['crm_type'][CRM_REQUEST_ORDER_SHIPPING]      = '訂單物流';
$_LANG['crm_type'][CRM_REQUEST_ORDER_AFTER_SALE]    = '售後服務';
$_LANG['crm_type'][CRM_REQUEST_SHIP_WRONG]          = '寄錯貨';
$_LANG['crm_type'][CRM_REQUEST_COMPLAIN]            = '投訴';
$_LANG['crm_type'][CRM_REQUEST_QUOTATION]           = '報價';
$_LANG['crm_type'][CRM_REQUEST_COOPERATION]         = '合作';
$_LANG['crm_type'][CRM_REQUEST_QUERY]               = '其他';

$_LANG['crm_type_label'][CRM_REQUEST_UNCATEGORIZED]       = "<span class='label label-default'>" . $_LANG['crm_type'][CRM_REQUEST_UNCATEGORIZED] . "</span>";
$_LANG['crm_type_label'][CRM_REQUEST_ORDER_QUERY]         = "<span class='label label-info'>" . $_LANG['crm_type'][CRM_REQUEST_ORDER_QUERY] . "</span>";
$_LANG['crm_type_label'][CRM_REQUEST_ORDER_PAYMENT]       = "<span class='label label-info'>" . $_LANG['crm_type'][CRM_REQUEST_ORDER_PAYMENT] . "</span>";
$_LANG['crm_type_label'][CRM_REQUEST_ORDER_SHIPPING]      = "<span class='label label-info'>" . $_LANG['crm_type'][CRM_REQUEST_ORDER_SHIPPING] . "</span>";
$_LANG['crm_type_label'][CRM_REQUEST_ORDER_AFTER_SALE]    = "<span class='label label-warning'>" . $_LANG['crm_type'][CRM_REQUEST_ORDER_AFTER_SALE] . "</span>";
$_LANG['crm_type_label'][CRM_REQUEST_SHIP_WRONG]          = "<span class='label label-danger'>" . $_LANG['crm_type'][CRM_REQUEST_SHIP_WRONG] . "</span>";
$_LANG['crm_type_label'][CRM_REQUEST_COMPLAIN]            = "<span class='label label-danger'>" . $_LANG['crm_type'][CRM_REQUEST_COMPLAIN] . "</span>";
$_LANG['crm_type_label'][CRM_REQUEST_QUOTATION]           = "<span class='label label-warning'>" . $_LANG['crm_type'][CRM_REQUEST_QUOTATION] . "</span>";
$_LANG['crm_type_label'][CRM_REQUEST_COOPERATION]         = "<span class='label label-primary'>" . $_LANG['crm_type'][CRM_REQUEST_COOPERATION] . "</span>";
$_LANG['crm_type_label'][CRM_REQUEST_QUERY]               = "<span class='label label-default'>" . $_LANG['crm_type'][CRM_REQUEST_QUERY] . "</span>";

$_LANG['crm_status'][CRM_STATUS_PENDING]       = '未處理';
$_LANG['crm_status'][CRM_STATUS_PROCESSING]    = '處理中';
$_LANG['crm_status'][CRM_STATUS_REPLIED]       = '已回覆';
$_LANG['crm_status'][CRM_STATUS_FINISHED]      = '已處理';
$_LANG['crm_status'][CRM_STATUS_ARCHIVED]      = '已封存';
$_LANG['crm_status_auto']                      = '已自動回覆';

$_LANG['crm_status_label'][CRM_STATUS_PENDING]       = "<span class='label label-danger'>" . $_LANG['crm_status'][CRM_STATUS_PENDING] . "</span>";
$_LANG['crm_status_label'][CRM_STATUS_PROCESSING]    = "<span class='label label-warning'>" . $_LANG['crm_status'][CRM_STATUS_PROCESSING] . "</span>";
$_LANG['crm_status_label'][CRM_STATUS_REPLIED]       = "<span class='label label-primary'>" . $_LANG['crm_status'][CRM_STATUS_REPLIED] . "</span>";
$_LANG['crm_status_label'][CRM_STATUS_FINISHED]      = "<span class='label label-success'>" . $_LANG['crm_status'][CRM_STATUS_FINISHED] . "</span>";
$_LANG['crm_status_label'][CRM_STATUS_ARCHIVED]      = "<span class='label label-default'>" . $_LANG['crm_status'][CRM_STATUS_ARCHIVED] . "</span>";
$_LANG['crm_status_label_auto']                      = "<span class='label label-primary'>" . $_LANG['crm_status_auto'] . "</span>";

$_LANG['crm_priority'][CRM_PRIORITY_NOT_SET]   = '未設定';
$_LANG['crm_priority'][CRM_PRIORITY_LOW]       = '低';
$_LANG['crm_priority'][CRM_PRIORITY_MEDIUM]    = '中';
$_LANG['crm_priority'][CRM_PRIORITY_HIGH]      = '高';

$_LANG['crm_priority_label'][CRM_PRIORITY_LOW]       = "<span class='label label-success'>" . $_LANG['crm_priority'][CRM_PRIORITY_LOW] . "</span>";
$_LANG['crm_priority_label'][CRM_PRIORITY_MEDIUM]    = "<span class='label label-warning'>" . $_LANG['crm_priority'][CRM_PRIORITY_MEDIUM] . "</span>";
$_LANG['crm_priority_label'][CRM_PRIORITY_HIGH]      = "<span class='label label-danger'>" . $_LANG['crm_priority'][CRM_PRIORITY_HIGH] . "</span>";

$_LANG['crm_platform'][CRM_PLATFORM_GMAIL]      = 'Gmail';
$_LANG['crm_platform'][CRM_PLATFORM_FACEBOOK]   = 'Facebook';
$_LANG['crm_platform'][CRM_PLATFORM_WECHAT]     = 'WeChat';
$_LANG['crm_platform'][CRM_PLATFORM_CHATRA]     = 'Chatra';
$_LANG['crm_platform'][CRM_PLATFORM_AIRCALL]    = 'Aircall';
$_LANG['crm_platform'][CRM_PLATFORM_TWILIO]     = 'Twilio';
?>