<?php

/**
 * ECSHOP
 * ============================================================================
 * 版權所有 2005-2010 上海商派網絡科技有限公司，並保留所有權利。
 * 網站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 這不是一個自由軟件！您只能在不用於商業目的的前提下對程序代碼進行修改和
 * 使用；不允許對程序代碼以任何形式任何目的的再發佈。
 * ============================================================================
 * $Author $
 * $Id $
*/

$_LANG['package_id'] = '編號';
$_LANG['package_name'] = '套裝優惠名稱';
$_LANG['package_tag'] = '套裝優惠展示標籤';
$_LANG['goodsid'] = '活動關聯產品';
$_LANG['goods_name'] = '關聯產品名稱';
$_LANG['start_time'] = '活動開始時間';
$_LANG['end_time'] = '活動結束時間';
$_LANG['package_price'] = '禮包價格';
$_LANG['package_edit'] = '編輯套裝優惠活動';
$_LANG['desc'] = '套裝優惠活動描述';
$_LANG['make_option'] = '請先搜索產品生成選項列表';
$_LANG['keywords'] = '產品關鍵字';
$_LANG['search_goods'] = '產品搜索';
$_LANG['all_goods'] = '可選產品';
$_LANG['package_goods'] = '該禮包的產品';
$_LANG['goods_number'] = '數量';
$_LANG['saving'] = '立即節省';
$_LANG['include_goods'] = '產品名稱/ID';
$_LANG['include_goods_hint'] = '可用逗號,分隔多個名稱或ID';
$_LANG['act_is_going'] = '僅顯示進行中的活動';

/* 提示信息 */
$_LANG['no_goods'] = '你輸入的產品不存在，請查證後再輸入';
$_LANG['package_exist'] = '套裝優惠 %s 已經存在';
$_LANG['back_list'] = '返回活動列表';
$_LANG['continue_add'] = '繼續添加新活動';
$_LANG['add_succeed'] = '添加成功';
$_LANG['edit_succeed'] = '編輯成功';
$_LANG['edit_fail'] = '編輯失敗';
$_LANG['no_name'] = '活動名不能為空';

/* 幫助信息 */
$_LANG['notice_goodsid'] = '需要先搜索產品，生成產品列表，然後再選擇';
$_LANG['notice_package_price'] = '購買禮包的價格';

/*JS 語言項*/
$_LANG['js_languages']['no_name'] = '沒有輸入活動名';
$_LANG['js_languages']['no_desc'] = '沒有輸入活動描述';
$_LANG['js_languages']['no_goods_id'] = '沒有選擇產品';
$_LANG['js_languages']['invalid_starttime'] = '輸入的起始時間格式不對，月份，時間應補足兩位';
$_LANG['js_languages']['invalid_endtime'] = '輸入的結束時間格式不對，月份，時間應補足兩位';
$_LANG['js_languages']['invalid_gt'] = '輸入的結束時間應大於起始日期';
$_LANG['js_languages']['search_is_null'] = '沒有搜索到任何產品，請重新搜索';
$_LANG['js_languages']['invalid_package_price'] = '禮包價格為空或不是數字';

/* Ecodemall */
@include(ROOT_PATH.'themes/'.$_CFG['template'].'/lang/'.$_CFG['lang'].'/others.php');
?>