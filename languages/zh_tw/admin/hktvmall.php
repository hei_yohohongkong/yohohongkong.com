<?php
# @Author: Anthony
# @Date:   2017-05-16T17:11:20+08:00
# @Filename: hktvmall.php

//table
$_LANG['created_at'] = '新增日期';
$_LANG['goods_name'] = '產品名稱';
$_LANG['market_price'] = '市場價錢';
$_LANG['hktv_price'] = 'HKTVMALL售價';
$_LANG['hktv_qty'] = '數量';
$_LANG['brand_name'] = '品牌';
$_LANG['hktv_rate'] = '手續費';
$_LANG['batch_handle_ok'] = '批量操作成功。';
// ajax
$_LANG['hktv_edit_invalid'] = '您輸入了一個非法的格式。';
$_LANG['hktv_edit_error'] = '您輸入了一個非法的格式。';
/* 提示信息 */
$_LANG['drop_confirm'] = '您確認要刪除商品嗎？';
$_LANG['export_confirm'] = '您確認要匯出商品到CSV嗎？';

?>
