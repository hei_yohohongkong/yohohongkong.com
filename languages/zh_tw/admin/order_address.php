<?php
$_LANG['order_address_edit']    = "編輯訂單地址";
$_LANG['order_address_report_title']  = "Big Data - 地址";
$_LANG['order_address_report']  = "訂單地址報告";
$_LANG['order_address_list']    = "訂單地址列表";
$_LANG['label_address']         = '地址：';
$_LANG['label_area']            = '所在地區：';
$_LANG['button_cancel']         = ' 取消 ';
$_LANG['order_address_return']  = '返回編輯訂單地址';
$_LANG['address_type'] = "地址類型";
$_LANG['address_type_1'] = "住宅";
$_LANG['address_type_2'] = "工商";
$_LANG['address_type_99'] = "其他";
$_LANG['address_type_other'] = "其他";
$_LANG['address_type_lift'] = "升降機";
$_LANG['address_type_lift_type_0'] = "未選擇";
$_LANG['address_type_lift_type_1'] = "設有升降機";
$_LANG['address_type_lift_type_2'] = "沒有升降機";
$_LANG['address_type_lift_type_3'] = "設有升降機(不到達地面)";
$_LANG['address_type_lift_label_1'] = "有𨋢";
$_LANG['address_type_lift_label_2'] = "冇𨋢";
$_LANG['address_type_lift_label_3'] = "𨋢不到地面";
?>