<?php

$_LANG['offline_payment_status'][OFFLINE_PAYTMENT_PENDING]  = "<span class='blue'>未上傳</span>";
$_LANG['offline_payment_status'][OFFLINE_PAYTMENT_UPLOADED] = "<span class='red'>侍處理</span>";
$_LANG['offline_payment_status'][OFFLINE_PAYTMENT_EXTRA_INFO]  = "<span class='blue'>待更新</span>";
$_LANG['offline_payment_status'][OFFLINE_PAYTMENT_FINISHED] = "<span class='green'>已完成</span>";
$_LANG['offline_payment_status'][OFFLINE_PAYTMENT_CLOSED]   = "不處理";
$_LANG['op_system_verify'] = "線下支付申請";

?>