<?php

//zh_tw
global $_LANG;

$_LANG['stripeapplepay']                 = 'Apple Pay (Stripe)';
$_LANG['stripeapplepay_desc']            = '' ;
$_LANG['stripe_pay_now_message']          = '請按下列Apple Pay 按鈕啟動付款流程';

$_LANG['stripeapplepay_unable_to_make_payment'] = '無法以Apple Pay支付';
$_LANG['stripeapplepay_logged_in'] = '請確認裝置/瀏覽器是否已登入Apple帳戶';
$_LANG['stripeapplepay_configured_payment'] = '請確認Apple Pay 帳戶內是否已綁定有效信用卡';
$_LANG['stripeapplepay_supported_browser'] = '請確認裝置/瀏覽器是否為最新版本並支援Apple Pay';
$_LANG['stripeapplepay_browser_privilege'] = "請確認瀏覽器是否允許存取付款方式";
$_LANG['stripeapplepay_configure_instruction'] = "如需詳細設定方式, 請前往:<br/><a href=\\\"https://support.apple.com/zh-hk/HT204506\\\" target='_blank'>設定 Apple Pay</a>";
$_LANG['stripeapplepay_supported_platform_heading'] = "Apple Pay 支援以下平台:";
$_LANG['stripeapplepay_supported_platform'] = [
    "iPhone 上的 Safari",
];
