<?php

global $_LANG;

$_LANG['alipayhk'] = '支付寶HK';
$_LANG['alipayhk_desc'] = '國內先進的網上支付平台。(香港錢包)';
$_LANG['alipayhk_merchant_pid'] = '合作者身份(Pid)';
$_LANG['alipayhk_md5_signature_key'] = '安全校驗碼(Key)';
$_LANG['alipayhk_api_base_gateway'] = 'API 接口';

$_LANG['alipayhk_instruction_use_alipay_scan_qr_code_proceed_payment'] = "使用AlipayHK掃描QR Code <br/>以完成支付";
$_LANG['alipayhk_instruction_no_alipayhk_app'] = "沒有AlipayHK App?";
$_LANG['alipayhk_scan_qr_code_to_download'] = "掃描QR Code 進行下載";
?>
