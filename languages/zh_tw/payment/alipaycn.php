<?php

global $_LANG;

$_LANG['alipaycn'] = '支付寶(中國)';
$_LANG['alipaycn_desc'] = '國內先進的網上支付平台。(大陸錢包)';
$_LANG['alipaycn_merchant_pid'] = '合作者身份(Pid)';
$_LANG['alipaycn_md5_signature_key'] = '安全校驗碼(Key)';
$_LANG['alipaycn_api_base_gateway'] = 'API 接口';

$_LANG['alipaycn_instruction_use_alipay_scan_qr_code_proceed_payment'] = '使用支付寶 App掃描二維碼<br/>以完成支付';
$_LANG['alipaycn_instruction_no_alipaycn_app'] = "沒有支付寶 App?";
$_LANG['alipaycn_scan_qr_code_to_download'] = "掃描二維碼進行下載";
?>
