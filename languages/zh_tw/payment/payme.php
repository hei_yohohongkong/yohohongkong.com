<?php

global $_LANG;

$_LANG['payme'] = "Payme";
$_LANG['payme_desc'] = 'PayMe是由匯豐銀行推出的電子錢包服務, 容許不同銀行用戶間自由轉帳';
$_LANG['payme_client_id'] = '用戶識別ID (Client ID)';
$_LANG['payme_client_secret'] = '密鑰(Secret Key)';
$_LANG['payme_signing_key_id'] = '簽名密鑰ID(Signing Key ID)';
$_LANG['payme_signing_key'] = '簽名密鑰(Signing Key)';
$_LANG['payme_api_base_url'] = 'API 網址';
$_LANG['payme_api_version'] = 'API Version';

// Payment Page Message
$_LANG['payme_scan_paycode_with_payme'] = "使用PayMe掃描以下PayCode";
$_LANG['payme_donot_close_this_page_until_payment_complete'] = "交易完成前切勿離開本頁";
$_LANG['payme_payment_instruction_title'] = "使用PayMe付款";
$_LANG['payme_payment_instruction_open_app'] = "打開PayMe應用程式";
$_LANG['payme_payment_instruction_scan_to_authorize'] = "掃描PayCode以授權付款";
$_LANG['payme_payment_instruction_wait_for_confirm'] = "在應用程式內完成付款，並在本頁等待確認";
$_LANG['payme_complete_payment_before_please'] = "請在";
$_LANG['payme_complete_payment_before'] = "前完成付款";

// PayMe App Instruction
$_LANG['payme_payment_instruction_use_app_title'] = "使用PayCode掃描器";
$_LANG['payme_payment_instruction_use_app_instruction'] = "在PayMe首頁，你可以...";
$_LANG['payme_payment_instruction_use_app_tap_icon_and_select_scanner'] = "輕觸PayCode圖示並選擇掃描器";
$_LANG['payme_payment_instruction_use_app_or'] = "或";
$_LANG['payme_payment_instruction_use_app_swipe_left'] = "向左掃以啟動掃描器";

// Payme Mobile Web Instruction
$_LANG['payme_payment_instruction_mobile'] = "請按以下\"Pay with PayMe\" 按鈕前往PayMe應用程式付款。";

// Payme Error
$_LANG['payme_error_unexpected_api_gateway_error'] = "無法連接PayMe服務,請改用其他付款方式或稍後再試<br/>如情況持續,請聯絡我們";
$_LANG['payme_error_unexpected_not_authenticated'] = "PayMe服務出現錯誤,請改用其他付款方式或稍後再試<br/>如情況持續,請聯絡我們";
$_LANG['payme_error_unexpected_not_validated'] = "PayMe服務出現錯誤,請改用其他付款方式或稍後再試<br/>如情況持續,請聯絡我們";
$_LANG['payme_error_unexpected_execution_error'] = "PayMe服務出現錯誤,請改用其他付款方式或稍後再試<br/>如情況持續,請聯絡我們";
$_LANG['payme_error_unexpected_timeout'] = "回應超時, 請重新載入本網頁或改用其他付款方式";
$_LANG['payme_error_unexpected_header_error'] = "PayMe服務出現錯誤,請改用其他付款方式或稍後再試<br/>如情況持續,請聯絡我們";
$_LANG['payme_error_unexpected_too_many_request'] = "PayMe服務現正繁忙, 請稍後再試";
$_LANG['payme_error_unexpected_validation_error'] = "PayMe服務出現錯誤,請改用其他付款方式或稍後再試<br/>如情況持續,請聯絡我們";
$_LANG['payme_error_unexpected_payme_related_error'] = "PayMe服務出現錯誤,請改用其他付款方式或稍後再試<br/>如情況持續,請聯絡我們";
$_LANG['payme_error_unexpected_internal_server_error'] = "PayMe服務出現錯誤,請改用其他付款方式或稍後再試<br/>如情況持續,請聯絡我們";
$_LANG['payme_error_unexpected_paycode_not_found_error'] = "找不到/ 無法生成PayCode,請改用其他付款方式或稍後再試<br/>如情況持續,請聯絡我們";
$_LANG['payme_error_unexpected_paycode_expired'] = "PayCode 已過期,請改用其他付款方式或稍後再試";
$_LANG['payme_error_paycode_expired_unconfirmed_payment'] = "交易狀態無法確認,<br/>如已付款請立刻聯絡我們";

$_LANG['payme_payment_successful_waiting_redirect'] = "正在確認交易狀態, 請稍等";

?>