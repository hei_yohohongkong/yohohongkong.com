<?php

//zh_tw
global $_LANG;

$_LANG['stripegooglepay']                 = 'Google Pay (Stripe)';
$_LANG['stripegooglepay_desc']            = '' ;
$_LANG['stripe_pay_now_message']          = '請按下列Google Pay 按鈕啟動付款流程';

$_LANG['stripegooglepay_unable_to_make_payment'] = '無法以Google Pay支付';
$_LANG['stripegooglepay_logged_in'] = '請確認裝置/瀏覽器是否已登入Google帳戶';
$_LANG['stripegooglepay_configured_payment'] = '請確認Google Pay 帳戶內是否已綁定有效信用卡';
$_LANG['stripegooglepay_supported_browser'] = '請確認裝置/瀏覽器是否為最新版本並支援Google Pay';
$_LANG['stripegooglepay_browser_privilege'] = '請確認瀏覽器是否允許存取付款方式';
$_LANG['stripegooglepay_configure_instruction'] = "如需詳細設定方式, 請前往:<br/><a href=\\\"https://support.google.com/pay/answer/7625055?hl=zh-HK\\\" target='_blank'>設定 Google Pay</a>";
$_LANG['stripegooglepay_supported_platform_heading'] = "Google Pay 支援以下平台:";
$_LANG['stripegooglepay_supported_platform'] = [
    "Android手機上的 Google Chrome"
];

// Tooltip on yoho.php
$_LANG['stripegooglepay_accept_payment_through'] = "支援直接以 Google Pay 付款";
$_LANG['stripegooglepay_support_platform'] = "注意: Google Pay 只支援Android 手機上的 Google Chrome for Android 瀏覽器結帳";
$_LANG['stripegooglepay_credit_card_data_remain_confidential'] = "所有付款經由 Stripe 進行，一切客戶信用卡資料為保密，本商戶亦沒有存底";
$_LANG['stripegooglepay_exchange_rate'] = "所有項款皆以港幣 (HKD) 結算，結算匯率將由銀行/金融機構/ Stripe 提供";
// $_LANG['stripe_secret_key'] 			  = 'Secret key';
// $_LANG['stripe_publishable_key'] 		  = 'Publishable key';
// $_LANG['card_number'] 			  		  = '信用卡號碼';
// $_LANG['sumbit'] 			  	  		  = '提交';
// $_LANG['stripe_txn_id']                   = 'Stripe 交易編號';
// $_LANG['stripe_currency']              	  = '支付貨幣';
// $_LANG['stripe_currency_range']['AUD']    = '澳元';
// $_LANG['stripe_currency_range']['CAD']    = '加元';
// $_LANG['stripe_currency_range']['EUR']    = '歐元';
// $_LANG['stripe_currency_range']['GBP']    = '英鎊';
// $_LANG['stripe_currency_range']['JPY']    = '日元';
// $_LANG['stripe_currency_range']['USD']    = '美元';
// $_LANG['stripe_currency_range']['HKD']    = '港元';

