<?php
/**
 * ECSHOP 盛付通支付插件 語言文件
 * ----------------------------------------------------------------------------
 * Jacklee的博客 致力於php技術
 * http://www.phpally.com
 * ----------------------------------------------------------------------------
 * @author: Jacklee
 * @email: jack349392900#gmail.com
 * @date: 2012-03-09
 */
 
global $_LANG;
 
$_LANG['sft2']   = '盛付通';
$_LANG['sft2_icbc']   = '工商銀行盛付通';
$_LANG['sft2_ccb']   = '建設銀行盛付通';
$_LANG['sft2_abc']   = '農業銀行';
$_LANG['sft2_bcom']   = '交通銀行盛付通';
$_LANG['sft2_cmb']   = '招商銀行盛付通';
$_LANG['sft2_cmbc']   = '民生銀行盛付通';
$_LANG['sft2_sdb']   = '深發銀行盛付通';
$_LANG['sft2_boc']   = '中國銀行盛付通';
$_LANG['sft2_bob']   = '北京銀行盛付通';
 
$_LANG['sft2_desc']    = '盛付通是國內領先的獨立第三方支付企業，由上海盛大提供服務支持。';
$_LANG['sft2_desc_icbc'] = '通過盛付通帳戶，用戶可以直接用工行網銀支付。';
$_LANG['sft2_desc_ccb'] = '通過盛付通帳戶，用戶可以直接用建設網銀支付。';
$_LANG['sft2_desc_abc'] = '您可以通過農業網銀直接支付。';
$_LANG['sft2_desc_bcom'] = '通過盛付通帳戶，用戶可以直接用交通網銀支付。';
$_LANG['sft2_desc_cmb'] = '通過盛付通帳戶，用戶可以直接用招商網銀支付。';
$_LANG['sft2_desc_cmbc'] = '通過盛付通帳戶，用戶可以直接用民生網銀支付。';
$_LANG['sft2_desc_sdb'] = '通過盛付通帳戶，用戶可以直接用深發網銀支付。';
$_LANG['sft2_desc_boc'] = '通過盛付通帳戶，用戶可以直接用中國網銀支付。';
$_LANG['sft2_desc_bob'] = '通過盛付通帳戶，用戶可以直接用北京網銀支付。';
 
$_LANG['sft2_account'] = '商戶號';
$_LANG['sft2_account_desc'] = '人民幣支付網關的收款帳號';
$_LANG['sft2_key']     = '商戶密鑰';
$_LANG['pay_button2'] = '立即支付';
 
?>