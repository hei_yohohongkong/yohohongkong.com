<?php
//zh_tw
global $_LANG;

$_LANG['braintree']                       = 'braintree';
$_LANG['braintreepaypal']                 = 'braintreepaypal';
$_LANG['braintree_desc']                  = 'braintree' ;
$_LANG['braintree_merchantId'] 			  = 'Merchant Id';
$_LANG['braintree_publicKey'] 			  = 'Public Key';
$_LANG['braintree_privateKey'] 			  = 'Private Key';
$_LANG['card_number'] 			  		  = '信用卡號碼';
$_LANG['expiration_date'] 			  	  = '有效日期';
$_LANG['sumbit'] 			  	  		  = '提交';
$_LANG['braintree_txn_id']                = 'Braintree 交易編號';
$_LANG['braintree_paypal_txn_id']         = 'Braintree-PAYPAL 交易編號';
$_LANG['paypal_currency']                 = '支付貨幣';
$_LANG['paypal_currency_range']['AUD']    = '澳元';
$_LANG['paypal_currency_range']['CAD']    = '加元';
$_LANG['paypal_currency_range']['EUR']    = '歐元';
$_LANG['paypal_currency_range']['GBP']    = '英鎊';
$_LANG['paypal_currency_range']['JPY']    = '日元';
$_LANG['paypal_currency_range']['USD']    = '美元';
$_LANG['paypal_currency_range']['HKD']    = '港元';

?>
