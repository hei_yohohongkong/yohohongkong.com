<?php

global $_LANG;

$_LANG['stock_cost']                    = '存貨成本';
$_LANG['stock_cost_desc']               = '記錄每日存貨成本';
$_LANG['stock_cost_cat']                = '分類成本';
$_LANG['stock_cost_brand']              = '品牌成本';
$_LANG['stock_cost_supp']               = '供應商成本';
$_LANG['stock_cost_cat_range']['1']     = '是';
$_LANG['stock_cost_cat_range']['0']     = '否';
$_LANG['stock_cost_brand_range']['1']   = '是';
$_LANG['stock_cost_brand_range']['0']   = '否';
$_LANG['stock_cost_supp_range']['1']    = '是';
$_LANG['stock_cost_supp_range']['0']    = '否';

?>