<?php

global $_LANG;

$_LANG['products']            = '產品處理';
$_LANG['products_desc']       = "定時處理產品相關資料\n1. 移除新品標籤\n2. 自動定價\n3. 產品成本過高提醒";
$_LANG['auto_best_seller']            = '自動更新暢銷產品';
$_LANG['auto_best_seller_desc']       = '自動更新暢銷產品(GOOGLE 跟 瀏覽轉換率)';
$_LANG['products_is_new_month']   = '保留新品標籤時限';
$_LANG['products_is_new_month_range'][1] = '1個月';
$_LANG['products_is_new_month_range'][2] = '2個月';
$_LANG['products_is_new_month_range'][3] = '3個月';
$_LANG['products_is_new_month_range'][6] = '6個月';
$_LANG['auto_pricing']   = '自動定價';
$_LANG['auto_pricing_range'][0] = '否';
$_LANG['auto_pricing_range'][1] = '是';
$_LANG['auto_pricing_dead']   = '自動定價(呆壞貨)';
$_LANG['auto_pricing_dead_range'][0] = '否';
$_LANG['auto_pricing_dead_range'][1] = '是';
$_LANG['max_on_sale_number']   = '促銷數量';
?>