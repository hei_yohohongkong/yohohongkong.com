<?php

global $_LANG;

$_LANG['offline_payment']                   = '自動標籤已付款';
$_LANG['offline_payment_desc']              = '定期將收到銀行付款通知的電郵的訂單標籤為已付款';
$_LANG['amount_protect_days']               = '不重複金額期限';
$_LANG['amount_protect_days_range']['0']    = '沒有';
$_LANG['amount_protect_days_range']['1']    = '1日';
$_LANG['amount_protect_days_range']['2']    = '2日';
$_LANG['amount_protect_days_range']['3']    = '3日';
$_LANG['amount_protect_days_range']['5']    = '5日';
$_LANG['amount_protect_days_range']['10']   = '10日';
$_LANG['order_add_days']                    = '有效下單時間';
$_LANG['order_add_days_range']['0']         = '沒有';
$_LANG['order_add_days_range']['1']         = '1日';
$_LANG['order_add_days_range']['2']         = '2日';
$_LANG['order_add_days_range']['3']         = '3日';
$_LANG['order_add_days_range']['5']         = '5日';
$_LANG['order_add_days_range']['10']        = '10日';
?>