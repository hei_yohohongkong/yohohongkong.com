<?php

global $_LANG;

$_LANG['sendy']                 = 'Sendy (EDM)';
$_LANG['sendy_desc']            = '定期導入會員資料至Sendy (會員電郵、生日月份、出生年份、VIP狀態、註冊年份)';
$_LANG['email_list_id']         = 'Email List ID';
$_LANG['csv_max_rec']           = 'CSV紀錄上限';
$_LANG['csv_delete']            = 'CSV自動移除';
$_LANG['csv_delete_range']['0']   = '否';
$_LANG['csv_delete_range']['1']   = '是';
?>