<?php

global $_LANG;

$_LANG['keywords']                  = '品牌及分類描述';
$_LANG['keywords_desc']             = '定時更新品牌及分類描述';
$_LANG['view_mode']                 = '檢示模式';
$_LANG['view_mode_range'][0]        = '否';
$_LANG['view_mode_range'][1]        = '是';
$_LANG['brand_sort']                = '分類展示排序';
$_LANG['brand_update']              = '品牌自動更新';
$_LANG['brand_prefix_chi']          = '品牌前綴(中)';
$_LANG['brand_suffix_chi']          = '品牌後綴(中)';
$_LANG['brand_prefix_eng']          = '品牌前綴(英)';
$_LANG['brand_suffix_eng']          = '品牌後綴(英)';
$_LANG['brand_sort_range'][0]       = '產品數量優先';
$_LANG['brand_sort_range'][1]       = '產品銷量優先';
$_LANG['brand_update_range'][0]     = '否';
$_LANG['brand_update_range'][1]     = '是';
$_LANG['category_sort']             = '品牌展示排序';
$_LANG['category_update']           = '分類自動更新';
$_LANG['cat_prefix_chi']            = '分類前綴(中)';
$_LANG['cat_suffix_chi']            = '分類後綴(中)';
$_LANG['cat_prefix_eng']            = '分類前綴(英)';
$_LANG['cat_suffix_eng']            = '分類後綴(英)';
$_LANG['category_sort_range'][0]    = '產品數量優先';
$_LANG['category_sort_range'][1]    = '產品銷量優先';
$_LANG['category_update_range'][0]  = '否';
$_LANG['category_update_range'][1]  = '是';
?>