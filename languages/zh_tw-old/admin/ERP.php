<?php

/* 菜单分类部分 */
$_LANG['17_erp_order_manage'] = '采购订单管理';
$_LANG['18_erp_stock_manage'] = '库存管理';
$_LANG['19_erp_system_manage'] = '系统设置';
$_LANG['20_erp_finance_manage'] = '财务管理';


/* 采购订单 */
$_LANG['erp_order_list'] = '采购订单列表';
$_LANG['erp_order_add'] = '录入采购订单';


/* 库存管理 */
$_LANG['erp_goods_list'] = '网店产品列表';
$_LANG['erp_goods_warehousing'] = '产品入库';
$_LANG['erp_goods_delivery'] = '产品出库';
$_LANG['erp_goods_dead_stock'] = '呆滞库存分析';
$_LANG['erp_goods_warehousing_report'] = '产品入库报表';
$_LANG['erp_goods_delivery_report'] = '产品出库报表';
$_LANG['erp_goods_warehousing_list'] = '入库单列表';
$_LANG['erp_goods_delivery_list'] = '出库单列表';
$_LANG['erp_stock_inquiry'] = '即时库存查询';
$_LANG['erp_goods_warehousing_and_delivery_record'] = '出入库序时簿';
$_LANG['erp_stock_check'] = '库存盘点';
$_LANG['erp_stock_transfer'] = '仓库转拨单列表';

$_LANG['erp_stock_daily'] = '库存变动报表';

/* 财务管理 */
$_LANG['erp_account_payable_list'] = '应付帐款列表';
$_LANG['erp_account_receivable_list'] = '应收帐款列表';

$_LANG['erp_payment_list'] = '付款单列表';
$_LANG['erp_gathering_list'] = '收款单列表';
$_LANG['erp_account_list'] = '银行帐号列表';


/* 系统设置 */
$_LANG['erp_supplier_list'] = '供应商列表';
$_LANG['erp_supplier_group'] = '供应商类别';
$_LANG['erp_warehouse_setting'] = '仓库设置';
$_LANG['erp_warehousing_style_setting'] = '入库类型设置';
$_LANG['erp_delivery_style_setting'] = '出库类型设置';
$_LANG['erp_account_setting'] = '银行帐号设置';
$_LANG['erp_payment_style_setting'] = '付款类型设置';
$_LANG['erp_goods_barcode_setting'] = '商品条码设置';

/* 通用*/
$_LANG['erp_yes'] = '是';
$_LANG['erp_no'] = '否';
$_LANG['erp_sure_to_delete'] = '您确定要删除吗？';
$_LANG['erp_required_item'] = '（必填）';
$_LANG['erp_is_valid'] = '是否有效';
$_LANG['erp_operation']='操作';

$_LANG['erp_operation_delete'] = '删除';
$_LANG['erp_operation_edit'] = '编辑';
$_LANG['erp_operation_view'] = '查看';
$_LANG['erp_operation_print'] = '打印';
$_LANG['erp_operation_post'] = '提交';

$_LANG['erp_operation_withdrawal']='退回';

$_LANG['erp_operation_rate'] = '定价';
$_LANG['erp_operation_approve'] = '审核';

$_LANG['erp_operation_post_to_finance'] = '提交财务';
$_LANG['erp_operation_post_to_approve'] = '提交审核';

$_LANG['erp_operation_approve_pass'] = '通过审核';
$_LANG['erp_operation_approve_reject'] = '拒绝审核';

$_LANG['erp_invalid_img'] = '您上传的图片格式不正确！';
$_LANG['erp_img_too_big'] = '图片文件太大了（最大值：%s），无法上传。';
$_LANG['erp_no_permit']='对不起，您没有进行此操作的权限！';
$_LANG['erp_not_a_number']='请输入数字！';
$_LANG['erp_wrong_parameter']='参数错误！';
$_LANG['erp_retun_to_center']='返回到管理中心';
$_LANG['erp_sum']='合计：';

$_LANG['erp_admin']='负责人';
$_LANG['erp_id']='序号';

$_LANG['erp_NULL']='EMPTY';

$_LANG['erp_operation_withdrawal']='退回';
$_LANG['search_keyword']='关键字';
$_LANG['all_record']='所有记录';

$_LANG['erp_btn_ok']='确定';

/*供应商分类*/
$_LANG['erp_add_supplier_group']='添加供应商分类';
$_LANG['erp_edit_supplier_group']='编辑供应商分类';
$_LANG['erp_supplier_group_name']='供应商分类';
$_LANG['erp_supplier_group_suppliers_count']='供应商数量';
$_LANG['erp_supplier_group_has_supplier']='此供应商分类下尚有供应商，不能删除。';
$_LANG['erp_supplier_group_sure_to_delete']='您确定要删除此供应商分类吗？';
$_LANG['erp_supplier_group_name_required'] = "请输入供应商分类名称。";

$_LANG['erp_supplier_group_edit_success']='成功更改供应商分类';
$_LANG['erp_supplier_group_add_success']='成功添加供应商分类';
$_LANG['erp_supplier_group_return']='返回到供应商分类列表';


/* 供应商列表 */
$_LANG['erp_add_supplier'] = '添加供应商';
$_LANG['erp_edit_supplier'] = '编辑供应商';
$_LANG['erp_supplier_id'] = '序号';
$_LANG['erp_supplier_code'] = '编号';
$_LANG['erp_supplier_address'] = '地址';
$_LANG['erp_supplier_name'] = '名称';
$_LANG['erp_supplier_contact'] = '联系人';
$_LANG['erp_supplier_tel'] = '电话';
$_LANG['erp_supplier_fax'] = '传真';
$_LANG['erp_supplier_qq'] = 'QQ';
$_LANG['erp_supplier_msn'] = 'MSN';
$_LANG['erp_supplier_code_exist'] = "供应商编码重复，请重新输入！";
$_LANG['erp_supplier_code_required'] = "请输入供应商编码。";
$_LANG['erp_supplier_name_required'] = "请输入供应商名称。";
$_LANG['erp_supplier_not_exist'] = "此供应商不存在或已被删除。";
$_LANG['erp_supplier_search_code'] = '供应商编号';
$_LANG['erp_supplier_has_order'] = '此供应商有关联的订单，不能删除。';
$_LANG['erp_supplier_has_goods'] = '此供应商有关联的产品，不能删除。';

$_LANG['erp_supplier_edit_success']='成功更改供应商';
$_LANG['erp_supplier_add_success']='成功添加供应商';
$_LANG['erp_supplier_return'] = '返回到供应商列表';
$_LANG['erp_supplier_goods_count'] = '产品数量';
$_LANG['erp_supplier_order_count'] = '订单数量';

/* 采购订单 */
$_LANG['erp_add_order'] = '添加新定单';

$_LANG['erp_edit_order'] = '编辑定单';
$_LANG['erp_view_order'] = '查看定单';
$_LANG['erp_rate_order'] = '定单定价';
$_LANG['erp_approve_order'] = '审核定单';

$_LANG['erp_order_return'] = '返回到采购订单列表';
$_LANG['erp_order_no_access'] = '不能编辑其他采购员建立的订单';

$_LANG['erp_order_no_acts'] = '该订单状态不能进行此操作';

$_LANG['erp_order_select_supplier_advance'] = '请先选择供应商';

$_LANG['erp_operation_change_attr'] = '属性';

$_LANG['erp_order_id'] = '序号';
$_LANG['erp_order_sn'] = '编号';
$_LANG['erp_order_description'] = '描述';
$_LANG['erp_order_supplier'] = '供应商';
$_LANG['erp_order_purchaser'] = '采购员';
$_LANG['erp_order_agency'] = '办事处';
$_LANG['erp_order_date'] = '订单日期';

$_LANG['erp_order_item_id'] = '序号';
$_LANG['erp_order_item_goods_sn'] = '产品货号';
$_LANG['erp_order_item_goods_name'] = '产品名称';
$_LANG['erp_order_item_goods_img'] = '产品图片';
$_LANG['erp_order_item_order_qty'] = '定购数量';
$_LANG['erp_order_item_goods_attr'] = '产品属性';
$_LANG['erp_order_item_order_price'] = '定购价格';
$_LANG['erp_order_item_order_amount'] = '总价';

$_LANG['erp_order_goods_sn'] ='货号：';
$_LANG['erp_order_goods_name'] ='名称：';
$_LANG['erp_order_input_qty'] ='数量：';

$_LANG['erp_order_goods_sn_required'] ='请输入货号。';
$_LANG['erp_order_goods_name_required'] ='请输入名称。';
$_LANG['erp_order_goods_sn_or_goods_name_required'] ='请输入货号或产品名称。';

$_LANG['erp_order_wrong_goods_sn'] ='请核对输入的产品货号。';
$_LANG['erp_order_wrong_goods_name'] ='请核对输入的产品名称。';

$_LANG['erp_order_wrong_goods_sn_and_goods_name'] ='请核对输入的产品货号或产品名称。';

$_LANG['erp_order_status'] = '订单状态';
$_LANG['erp_order_status_inputing'] = '订单录入中';//order_status=1
$_LANG['erp_order_status_rating'] = '等待财务处理';//order_status=2
$_LANG['erp_order_status_approving'] = '等待主管审核订单';//order_status=3
$_LANG['erp_order_status_approve_pass'] = '审核已通过';//order_status=4
$_LANG['erp_order_status_approve_reject'] = '审核未通过';//order_status=5

$_LANG['erp_gathering_status_revised'] = '订单已修订';

$_LANG['erp_order_search_order_status'] = '订单状态';
$_LANG['erp_order_search_supplier'] = '所有供应商';
$_LANG['erp_order_search_purchaser'] = '所有采购员';
$_LANG['erp_order_search_start_date'] = '起始日期';
$_LANG['erp_order_search_end_date'] = '结束日期';
$_LANG['erp_order_search_order_sn'] = '订单编号';
$_LANG['erp_order_search_good_keyword'] = '产品关键字(编号/货号/名称)';

$_LANG['erp_order_operation_view_details'] = '要求';
$_LANG['erp_order_operation_view_approve_remark'] = '审核备注';

$_LANG['erp_order_no_admin_supplier'] = '管理员尚未给您分配供应商，不能新建采购订单。';
$_LANG['erp_order_description_required'] = '订单描述不得留空！';
$_LANG['erp_order_supplier_code_required'] = '请输入工厂编号！';

$_LANG['erp_order_no_agency_access'] = "不能查看、编辑其他办事处的采购单";

$_LANG['erp_order_supplier_not_exist'] = "该供应商不存在、无效或已被删除";
$_LANG['erp_order_select_supplier'] = "请选择";

$_LANG['erp_order_goods_without_attr'] = '此产品没有可选属性。';
$_LANG['erp_order_attr_not_select'] = '请选择产品属性。';

$_LANG['erp_order_sure_to_change_order_supplier'] = "您确定要更改供应商吗？";
$_LANG['erp_order_sure_to_change_order_description'] = "您确定要更改订单描述吗？";
$_LANG['erp_order_sure_to_delete_order'] = "您确定要删除此订单吗？";
$_LANG['erp_order_sure_to_change_goods_sn'] = "您确定要更改产品货号吗？";
$_LANG['erp_order_sure_to_change_order_qty'] = "您确定要更改订单数量吗？";
$_LANG['erp_order_sure_to_delete_order_item'] = "您确定要删除此订单项吗？";
$_LANG['erp_order_sure_to_post_order'] = "您确定要提交此订单给财务吗？";

$_LANG['erp_order_sure_to_withdrawal_to_edit'] = "您确定要退回此订单吗？";
$_LANG['erp_order_sure_to_withdrawal_to_rate'] = "您确定要退回此订单吗？";

$_LANG['erp_order_sure_to_change_specs'] = "您确定要更改订单要求吗？";
$_LANG['erp_order_sure_to_change_price'] = "您确定要更改价格吗？";
$_LANG['erp_order_sure_to_post_to_approve'] = "您确定要提交订单给主管审核吗？";
$_LANG['erp_order_sure_to_approve'] = "您确定要提交审核吗？";

$_LANG['erp_order_no_accessibility']='其他用户正在编辑此订单,不能进行此项操作！';
$_LANG['erp_order_return_to_order_list']='返回到采购订单列表。';

$_LANG['erp_order_not_admin_goods']='您没有此商品的管理权。';
$_LANG['erp_order_not_supplier_goods']='此商品不属于您所选的供应商。';

$_LANG['erp_order_select_goods_in_advance']='请先选择产品。';

$_LANG['erp_order_not_exist']='此订单不存在或已被删除。';
$_LANG['erp_order_item_not_exist']='此订单项不存在或已被删除。';
$_LANG['erp_order_item_attr_not_exist']='此产品属性不存在或已被删除。';
$_LANG['erp_order_goods_not_exist']='此产品不存在或已被删除。';
$_LANG['erp_order_view_order']='查看此订单';
$_LANG['erp_order_delete_sucess']='成功删除订单。';
$_LANG['erp_order_post_success']='成功提交订单。';
$_LANG['erp_order_approve_success']='成功提交审核结果。';

$_LANG['erp_order_post_failed']='提交订单出错。';

$_LANG['erp_order_not_completed']="此订单尚未完善，不能提交,需要检查的项目有：
 * 1，是否选择了供应商
 * 2，是否最少含一条订单项
 * 3，是否所有订单项都选择了产品
 * 4，有属性的产品是否都选了属性
 * 5，每个订单项是否都输入了定购数量";

$_LANG['erp_order_no_order_item']='此订单无订单项，请完善定单后再提交。';
$_LANG['erp_order_no_supplier']='未选择供应商，不能提交。';
$_LANG['erp_order_no_goods']='最少有一个订单项未输入产品货号，不能提交。';
$_LANG['erp_order_no_goods_attr']='最少有一个订单项尚未选择产品属性，不能提交。';
$_LANG['erp_order_no_goods_qty']='最少有一个订单项尚未输入定购数量，不能提交。';
$_LANG['erp_order_no_img']='无图片';
$_LANG['erp_order_input_goods_sn']='输入货号';

$_LANG['erp_order_specific']='说明及要求';

$_LANG['erp_approve_remark'] = '审核备注:';

$_LANG['erp_order_sum_total']='总计：';
$_LANG['erp_order_qty_unit']='件';
$_LANG['erp_order_amount_unit']='元';

$_LANG['erp_order_create_success']='成功建立%s个采购订单。';

$_LANG['erp_order_view_list']='查看采购订单列表。';

$_LANG['erp_order_back']='返回到订单列表';

/* 打印采购订单 */
$_LANG['erp_print_order_title']='采购订单';
$_LANG['erp_print_order_sn']='采购订单编号：';
$_LANG['erp_print_create_date']='下单日期：';
$_LANG['erp_print_order_supplier_name']='供应商名称：';
$_LANG['erp_print_order_user1']='制单：';
$_LANG['erp_print_order_user2']='财务：';
$_LANG['erp_print_order_user3']='审核：';
$_LANG['erp_print_order_user4']='供应商签字：';

/* 仓库设置 */
$_LANG['erp_add_warehouse']='添加仓库';
$_LANG['erp_warehouse_list']='仓库列表';
$_LANG['erp_warehouse_id'] = '序号';
$_LANG['erp_warehouse_name'] = '名称';
$_LANG['erp_warehouse_description'] = '描述';
$_LANG['erp_warehouse_administer'] = '仓管';
$_LANG['erp_warehouse_delivery_number']='出库单数';
$_LANG['erp_warehouse_warehousing_number']='入库单数';
$_LANG['erp_warehouse_return'] = '返回到仓库列表';
$_LANG['erp_edit_warehouse'] = '编辑仓库';
$_LANG['erp_warehouse_edit_success'] = '成功更改仓库';
$_LANG['erp_warehouse_add_success'] = '成功添加仓库';
$_LANG['erp_warehouse_has_warehousing'] ='此仓库有相关联的入库单，不能删除。';
$_LANG['erp_warehouse_has_delivery'] ='此仓库有相关联的出库单，不能删除。';

$_LANG['erp_warehouse_has_stock'] ='此仓库下尚有库存，不能删除。';

$_LANG['erp_warehousing_style_has_warehousing'] ='此入库类型有相关联的入库单，不能删除。';
$_LANG['erp_delivery_style_has_delivery'] ='此出库类型有相关联的出库单，不能删除。';
$_LANG['erp_warehouse_not_exist'] ='仓库不存在或已被删除！';
$_LANG['erp_warehouse_sure_to_change_description'] = "您确定要更改仓库描述吗？";
$_LANG['erp_warehouse_description_required'] = "仓库描述不得留空，请重新输入！";
$_LANG['erp_warehouse_administer_required'] ='仓管不得留空，请重新输入！';
$_LANG['erp_warehouse_name_required'] ='仓管名不得留空，请重新输入！';
$_LANG['erp_warehouse_sure_to_change_administer'] = "您确定要更改仓库管理人吗？";

$_LANG['erp_warehouse_agency'] ='所属办事处';

$_LANG['erp_warehouse_select_agency'] ='选择办事处';

$_LANG['erp_warehouse_is_on_sale'] = '是否可销售';
$_LANG['erp_warehouse_is_on_sale_tips'] = '可销售是可用于销售的仓库，用户在下订单的时候，会检查该仓库的库存';

/* 出入库类型 */
$_LANG['erp_warehousing_style_list']='入库类型列表';
$_LANG['erp_add_warehousing_style']='添加入库类型';
$_LANG['erp_warehousing_style_id'] = '序号';
$_LANG['erp_warehousing_style'] = '入库类型';
$_LANG['erp_warehousing_style_not_exist'] ='入库类型不存在或已被删除！';
$_LANG['erp_warehousing_style_required'] ='入库类型不得留空，请重新输入！';

$_LANG['erp_warehousing_style_return'] = '返回到入库类型列表';
$_LANG['erp_warehousing_style_add_success'] = '成功添加入库类型';
$_LANG['erp_warehousing_style_edit_success'] = '成功更改入库类型';
$_LANG['erp_edit_warehousing_style'] = '编辑入库类型';

$_LANG['erp_warehousing_style_preserved'] = '系统保留入库类型，不能编辑';

$_LANG['erp_delivery_style_list']='出库类型列表';
$_LANG['erp_add_delivery_style']='添加出库类型';
$_LANG['erp_delivery_style_id'] = '序号';
$_LANG['erp_delivery_style'] = '出库类型';
$_LANG['erp_delivery_style_not_exist'] ='出库类型不存在或已被删除！';
$_LANG['erp_delivery_style_required'] ='出库类型不得留空，请重新输入！';
$_LANG['erp_delivery_without_valid_warehouse'] ='管理员尚未给您分配仓库，不能新建出库单。';

$_LANG['erp_delivery_add_failed'] ='建立出库单发生错误';

$_LANG['erp_delivery_auto_on_ship'] = '发货自动扣除库存';

$_LANG['erp_delivery_auto_on_ship_failed'] = '发货自动扣除库存失败';

$_LANG['erp_delivery_style_return'] = '返回到出库类型列表';
$_LANG['erp_delivery_style_add_success'] = '成功添加出库类型';
$_LANG['erp_delivery_style_edit_success'] = '成功更改出库类型';
$_LANG['erp_edit_delivery_style'] = '编辑出库类型';

/* 入库单 */
$_LANG['erp_warehousing_id'] = '序号';
$_LANG['erp_warehousing_sn'] ='入库单号';
$_LANG['erp_warehousing_warehouse'] ='收货仓库';
$_LANG['erp_warehousing_agency'] ='办事处';
$_LANG['erp_warehousing_warehousing_style'] ='入库类型';
$_LANG['erp_warehousing_order'] ='关联订单号';
$_LANG['erp_warehousing_date'] ='入库日期';
$_LANG['erp_warehousing_from'] ='交货人';
$_LANG['erp_warehousing_admin'] ='仓管';
$_LANG['erp_warehousing_status'] ='入库单状态';

$_LANG['erp_warehousing_start_date'] ='起始日期';
$_LANG['erp_warehousing_end_date'] ='结束日期';

$_LANG['erp_warehousing_no_access'] = '不能编辑其他仓管员建立的入库单';

$_LANG['erp_warehouse_no_access'] = '没有权限对此仓库进行出入库操作';

$_LANG['erp_warehousing_status_inputing'] = '入库单录入中';//order_status=1
$_LANG['erp_warehousing_status_rating'] = '财务处理中';//order_status=2
$_LANG['erp_warehousing_status_approving'] = '主管审核中';//order_status=3
$_LANG['erp_warehousing_status_approve_pass'] = '审核已通过';//order_status=4
$_LANG['erp_warehousing_status_approve_reject'] = '审核未通过';//order_status=5

$_LANG['erp_warehousing_operation_approve_remark'] = '审核备注';

$_LANG['erp_warehousing_item_id'] = '序号';
$_LANG['erp_warehousing_item_goods_sn'] ='产品货号';
$_LANG['erp_warehousing_item_goods_img'] ='产品图片';
$_LANG['erp_warehousing_item_goods_attr'] ='产品属性';
$_LANG['erp_warehousing_item_expected_qty'] ='应收数量';
$_LANG['erp_warehousing_item_received_qty'] ='实收数量';
$_LANG['erp_warehousing_item_remark'] ='备注';
$_LANG['erp_warehousing_item_goods_barcode'] ='条码';

$_LANG['erp_warehousing_item'] ='入库单项';

$_LANG['erp_warehousing_select_warehouse'] ='请选择仓库';
$_LANG['erp_warehousing_item_not_exist'] ='入库单项不存在或已被删除！';
$_LANG['erp_warehousing_choose_order'] ='选择订单';

$_LANG['erp_warehousing_no_acts'] = '该入库单状态不能进行此操作';

$_LANG['erp_warehousing_no_agency_access'] = '不能查看、编辑其他办事处的入库单';

$_LANG['erp_warehousing_not_exist'] ='入库单不存在或已被删除！';
$_LANG['erp_warehousing_no_accessibility']='其他用户正在编辑此入库单,不能进行此项操作！';
$_LANG['erp_warehousing_return_to_warehousing_list']='返回到入库单列表';

$_LANG['erp_warehousing_approve_pass_failed'] ='入库单审核失败';

$_LANG['erp_warehousing_view_warehousing']='查看入库单';
$_LANG['erp_warehousing_edit_warehousing']='编辑入库单';
$_LANG['erp_warehousing_approve_warehousing']='审核入库单';

$_LANG['erp_warehousing_not_completed']="此入库单尚未完善，不能提交，需要检查的项目有：
 * 1，如果是关联采购订单入库，是否已经选择了订单
 * 2，是否输入了交货人
 * 3，是否最少有一个入库单项
 * 4，是否每个入库单项都有入库数量";

$_LANG['erp_warehousing_sure_to_withdrawal_to_edit'] = "您确定要退回此入库单吗？";

$_LANG['erp_warehousing_sure_to_change_warehouse'] ='您确定要更改收货仓库吗？';
$_LANG['erp_warehousing_sure_to_change_warehousing_style'] ='您确定要更改入库类型吗？';
$_LANG['erp_warehousing_sure_to_change_order'] ='您确定要更改关联订单吗？';
$_LANG['erp_warehousing_sure_to_change_warehousing_from'] ='您确定要更改交货人吗？';
$_LANG['erp_warehousing_sure_to_post_warehousing'] ='您确定要提交入库单给主管审核吗？';
$_LANG['erp_warehousing_sure_to_post_approve'] ='您确定要提交审核吗？';
$_LANG['erp_warehousing_sure_to_change_received_qty'] ='您确定要更改实收数量吗？';
$_LANG['erp_warehousing_sure_to_change_price'] ='您确定要更改入库价格吗？';
$_LANG['erp_warehousing_sure_to_delete_warehousing_item'] ='您确定要删除此入库单项吗？';
$_LANG['erp_warehousing_sure_to_delete_warehousing'] ='您确定要删除此入库单吗？';

$_LANG['erp_warehousing_warehousing_from_required'] ='请填写交货人';

$_LANG['erp_warehousing_input_barcode'] ='条码：';
$_LANG['erp_warehousing_input_qty'] ='数量：';
$_LANG['erp_warehousing_input_warehousing_from'] ='输入交货人';

$_LANG['erp_warehousing_import_warehousing'] ='导入数据：';

$_LANG['erp_warehousing_input_goods_sn'] ='货号：';
$_LANG['erp_warehousing_input_goods_name'] ='名称：';
$_LANG['erp_warehousing_input_goods_barcode'] ='条码：';

$_LANG['erp_warehousing_goods_sn_required'] ='请输入货号。';
$_LANG['erp_warehousing_goods_sn_or_goods_name_or_goods_barcode_required'] ='请输入条码、货号或产品名称。';

$_LANG['erp_warehousing_wrong_goods_sn'] ='请核对输入的产品货号。';
$_LANG['erp_warehousing_wrong_goods_name'] ='请核对输入的产品名称。';
$_LANG['erp_warehousing_item_warehousing_price'] ='入库价格';
$_LANG['erp_warehousing_wrong_goods_barcode'] ='请核对输入的产品条码。';

$_LANG['erp_warehousing_without_choosing_order'] ='请选择关联订单号！';
$_LANG['erp_warehousing_without_warehousing_from'] ='请输入交货人！';
$_LANG['erp_warehousing_without_warehousing_item'] ='此入库单无入库单项，不能提交！';
$_LANG['erp_warehousing_without_warehousing_qty'] ='最少有一条入库单项未输入实收数，不能提交！';
$_LANG['erp_warehousing_only_text_file'] ='文件类型错误，正确的文件应当是以"txt"为扩展名的文本文件！';
$_LANG['erp_warehousing_return'] ='返回到入库单编辑页面';
$_LANG['erp_warehousing_invalid_file'] ='您上传的文件格式不正确！';
$_LANG['erp_warehousing_batch_warehousing_success'] ='成功导入入库数据！';
$_LANG['erp_warehousing_invalid_goods_sn'] ='导入的数据含无效的货号！';

$_LANG['erp_warehousing_without_valid_warehouse'] ='管理员尚未给您分配仓库，不能新建入库单。';

$_LANG['erp_warehousing_auto_on_cancel_ship_failed'] ='自动增加库存失败';

$_LANG['erp_warehousing_auto_on_cancel_ship'] = '取消发货自动增加库存';


/* 打印入库单 */
$_LANG['erp_print_warehousing_title']='入库单';

$_LANG['erp_print_warehousing_user1']='制单：';
$_LANG['erp_print_warehousing_user2']='仓管：';
$_LANG['erp_print_warehousing_user3']='交货人：';
$_LANG['erp_print_warehousing_user4']='审核：';

/* 网店产品库存列表 */

$_LANG['erp_goods_list_goods_id'] = '序号';
$_LANG['erp_goods_list_goods_img'] ='产品图片';
$_LANG['erp_goods_list_goods_sn'] ='产品货号';
$_LANG['erp_goods_list_goods_provider_sn'] ='供应商货号';
$_LANG['erp_goods_list_goods_name'] ='产品名称';
$_LANG['erp_goods_list_goods_number'] ='产品库存';
$_LANG['erp_goods_list_goods_warn_number'] ='警告库存';
$_LANG['erp_goods_list_goods_goods_attr'] ='产品属性';
$_LANG['erp_goods_list_set_goods_warn_number'] ='设置警告库存';

$_LANG['erp_goods_list_search_cat'] ='产品类别';
$_LANG['erp_goods_list_search_goods_sn'] ='产品货号';
$_LANG['erp_goods_list_search_provider_goods_sn'] ='供应商货号';
$_LANG['erp_goods_list_sure_to_change_warn_number'] ='您确定要修改警告库存吗？';

/* 出库单 */
$_LANG['erp_delivery_id'] = '序号';
$_LANG['erp_delivery_sn'] ='出库单号';
$_LANG['erp_delivery_warehouse'] ='发货仓库';
$_LANG['erp_delivery_agency'] ='办事处';
$_LANG['erp_delivery_delivery_style'] ='出库类型';
$_LANG['erp_delivery_date'] ='出库日期';
$_LANG['erp_delivery_to'] ='提货人';
$_LANG['erp_delivery_admin'] ='仓管';
$_LANG['erp_delivery_status'] ='出库单状态';
$_LANG['erp_delivery_order'] ='关联订单号';

$_LANG['erp_delivery_start_date'] ='起始日期';
$_LANG['erp_delivery_end_date'] ='结束日期';

$_LANG['erp_delivery_status_inputing'] = '出库单录入中';//order_status=1
$_LANG['erp_delivery_status_rating'] = '财务处理中';//order_status=2
$_LANG['erp_delivery_status_approving'] = '主管审核中';//order_status=3
$_LANG['erp_delivery_status_approve_pass'] = '审核已通过';//order_status=4
$_LANG['erp_delivery_status_approve_reject'] = '审核未通过';//order_status=5

$_LANG['erp_delivery_operation_approve_remark'] = '审核备注';

$_LANG['erp_delivery_no_access'] = '不能编辑其他仓管员建立的出库单';

$_LANG['erp_delivery_no_agency_access'] = '不能查看、编辑其他办事处的出库单';

$_LANG['erp_delivery_no_acts'] = '该出库单状态不能进行此操作';

$_LANG['erp_delivery_item_id'] = '序号';
$_LANG['erp_delivery_item_goods_sn'] ='产品货号';
$_LANG['erp_delivery_item_goods_img'] ='产品图片';
$_LANG['erp_delivery_item_goods_attr'] ='产品属性';
$_LANG['erp_delivery_item_goods_stock'] ='产品库存';
$_LANG['erp_delivery_item_expected_qty'] ='应发数量';
$_LANG['erp_delivery_item_delivered_qty'] ='实发数量';
$_LANG['erp_delivery_item_remark'] ='备注';
$_LANG['erp_delivery_item_goods_barcode'] ='条码';

$_LANG['erp_delivery_not_completed']="此出库单尚未完善，不能提交，需要检查的项目有：
 * 1，如果是关联销售订单出库，是否已经选择了订单
 * 2，是否输入了提货人
 * 3，是否最少有一个出库单项
 * 4，是否每个出库单项都有出库数量
 * 5，是否每个出库单项对应的产品库存都足够";

$_LANG['erp_delivery_item_not_exist'] ='出库单项不存在或已被删除！';
$_LANG['erp_delivery_choose_order'] ='选择订单';
$_LANG['erp_delivery_select_warehouse'] ='请选择仓库';

$_LANG['erp_delivery_stock_not_enough'] ='该产品此属性库存不足（库存数%s）。';

$_LANG['erp_delivery_not_exist'] ='出库单不存在或已被删除！';
$_LANG['erp_delivery_no_accessibility']='其他用户正在编辑此出库单,不能进行此项操作！';
$_LANG['erp_delivery_return_to_delivery_list']='返回到出库单列表';

$_LANG['erp_delivery_approve_pass_failed'] ='出库单审核失败';
$_LANG['erp_delivery_approve_pass_failed_no_stock'] ='库存不足，出库单审核失败';

$_LANG['erp_delivery_view_delivery']='查看出库单';
$_LANG['erp_delivery_edit_delivery']='编辑出库单';
$_LANG['erp_delivery_approve_delivery']='审核出库单';

$_LANG['erp_delivery_sure_to_change_warehouse'] ='您确定要更改发货仓库吗？';
$_LANG['erp_delivery_sure_to_change_delivery_style'] ='您确定要更改出库类型吗？';
$_LANG['erp_delivery_sure_to_change_order'] ='您确定要更改关联订单吗？';
$_LANG['erp_delivery_sure_to_change_delivery_to'] ='您确定要更改提货人吗？';

$_LANG['erp_delivery_sure_to_post_delivery'] ='提交后不能再次更改出库单，您确定要提交吗？';

$_LANG['erp_delivery_sure_to_submit_delivery'] ='您确定要提交出库单给主管审核吗？';
$_LANG['erp_delivery_sure_to_post_approve'] ='您确定要提交审核吗？';
$_LANG['erp_delivery_sure_to_change_delivered_qty'] ='您确定要更改实发数量吗？';
$_LANG['erp_delivery_sure_to_change_price'] ='您确定要更改出库价格吗？';
$_LANG['erp_delivery_sure_to_delete_delivery_item'] ='您确定要删除此出库单项吗？';
$_LANG['erp_delivery_sure_to_delete_delivery'] ='您确定要删除此出库单吗？';

$_LANG['erp_delivery_delivery_to_required'] ='请填写提货人';

$_LANG['erp_delivery_input_barcode'] ='条码：';
$_LANG['erp_delivery_input_qty'] ='数量：';
$_LANG['erp_delivery_input_delivery_to'] ='输入提货人';

$_LANG['erp_delivery_import_delivery'] ='导入数据：';

$_LANG['erp_delivery_input_goods_sn'] ='货号：';
$_LANG['erp_delivery_input_goods_name'] ='名称：';
$_LANG['erp_delivery_input_goods_barcode'] ='条码：';

$_LANG['erp_delivery_stock_not_enough'] ='出库数量大于出库数量（现有库存%d），不能出库';

$_LANG['erp_delivery_warehouse_cant_change'] ='删除出库单项后才能更改出货仓库';

$_LANG['erp_delivery_goods_sn_required'] ='请输入货号。';
$_LANG['erp_delivery_goods_name_required'] ='请输入产品名称。';
$_LANG['erp_delivery_goods_sn_or_goods_name_or_goods_barcode_required'] ='请输入条码、货号或产品名称。';
$_LANG['erp_delivery_wrong_goods_sn'] ='请核对输入的产品货号。';
$_LANG['erp_delivery_wrong_goods_name'] ='请核对输入的产品名称。';
$_LANG['erp_delivery_wrong_goods_barcode'] ='请核对输入的产品条码。';

$_LANG['erp_delivery_item_delivery_price'] ='出库价格';

$_LANG['erp_delivery_without_choosing_order'] ='请选择关联订单号！';
$_LANG['erp_delivery_without_delivery_to'] ='请输入提货人！';
$_LANG['erp_delivery_without_delivery_item'] ='此出库单无出库单项，不能提交！';
$_LANG['erp_delivery_without_delivery_qty'] ='最少有一条出库单项未输入实发数量，不能提交！';
$_LANG['erp_delivery_only_text_file'] ='文件类型错误，正确的文件应当是以"txt"为扩展名的文本文件！';
$_LANG['erp_delivery_return'] ='返回到出库单编辑页面';
$_LANG['erp_delivery_invalid_file'] ='您上传的文件格式不正确！';
$_LANG['erp_delivery_batch_delivery_success'] ='成功导入出库数据！';
$_LANG['erp_delivery_invalid_goods_sn'] ='导入的数据含无效的货号！';

$_LANG['erp_delivery_no_stock'] ='至少有一个订单项因库存不足，没有导入，请检查库存。';

/* 打印出库单 */
$_LANG['erp_print_delivery_title']='出库单';
$_LANG['erp_print_delivery_user1']='制单：';
$_LANG['erp_print_delivery_user2']='仓管：';
$_LANG['erp_print_delivery_user3']='提货人：';
$_LANG['erp_print_delivery_user4']='审核：';

/* 产品库存列表 */
$_LANG['erp_stock_id'] = '序号';
$_LANG['erp_stock_goods_id'] = '产品ID';
$_LANG['erp_stock_goods_img'] ='产品图片';
$_LANG['erp_stock_goods_sn'] ='产品货号';
$_LANG['erp_stock_goods_provider_sn'] ='供应商货号';
$_LANG['erp_stock_goods_name'] ='产品名称';
$_LANG['erp_stock_goods_barcode'] ='产品条码';
$_LANG['erp_stock_warehouse_name'] ='仓库名称';
$_LANG['erp_stock_goods_number'] ='产品库存';
$_LANG['erp_stock_warehouse_attr_qty'] ='仓库-条码-属性-库存';

$_LANG['erp_stock_warehouse'] ='仓库';
$_LANG['erp_stock_agency'] ='办事处';
$_LANG['erp_stock_attr'] ='属性';
$_LANG['erp_stock_qty'] ='库存';

$_LANG['erp_stock_search_warehouse'] ='请选择仓库';
$_LANG['erp_stock_search_goods_sn'] ='产品货号';
$_LANG['erp_stock_search_goods_name'] ='产品名称';
$_LANG['erp_stock_search_goods_barcode'] ='产品条码';
$_LANG['erp_stock_search_provider_goods_sn'] ='供应商货号';

/* 产品库存盘点表 */
$_LANG['erp_stock_check_id'] = '序号';
$_LANG['erp_stock_check_goods_img'] ='产品图片';
$_LANG['erp_stock_check_goods_sn'] ='产品货号';
$_LANG['erp_stock_check_goods_name'] ='产品名称';
$_LANG['erp_stock_check_goods_barcode'] ='产品条码';
$_LANG['erp_stock_check_goods_provider_sn'] ='供应商货号';
$_LANG['erp_stock_check_goods_attr'] ='产品属性';
$_LANG['erp_stock_check_warehouse_name'] ='仓库';
$_LANG['erp_stock_check_agency'] ='办事处';
$_LANG['erp_stock_check_goods_sn'] ='产品货号';
$_LANG['erp_stock_check_begin_qty'] ='期初库存';
$_LANG['erp_stock_check_end_qty'] ='期末库存';

$_LANG['erp_stock_check_select_warehouse'] ='请选择仓库';

$_LANG['erp_stock_check_warehousing_qty'] ='入库数量';
$_LANG['erp_stock_check_delivery_qty'] ='出库数量';

$_LANG['erp_stock_check_search_start_date'] ='起始日期';
$_LANG['erp_stock_check_search_end_date'] ='结束日期';

$_LANG['erp_stock_check_print'] ='打印库存盘点表';

/* 产品出入库记录(序时薄) */
$_LANG['erp_stock_record_id'] = '序号';
$_LANG['erp_stock_record_goods_img'] ='产品图片';
$_LANG['erp_stock_record_goods_sn'] ='产品货号';
$_LANG['erp_stock_record_goods_name'] ='产品名称';
$_LANG['erp_stock_record_goods_attr'] ='产品属性';
$_LANG['erp_stock_record_goods_barcode'] ='条码';
$_LANG['erp_stock_record_warehouse_name'] ='仓库名称';
$_LANG['erp_stock_record_agency'] ='办事处';
$_LANG['erp_stock_record_w_d_style'] ='出入库类型';
$_LANG['erp_stock_record_w_d_sn'] ='出入库单号';
$_LANG['erp_stock_record_order_sn'] ='关联订单号';
$_LANG['erp_stock_record_w_d_qty'] ='出入库数';
$_LANG['erp_stock_record_stock'] ='库存数量';
$_LANG['erp_stock_record_w_d_name'] ='提/交货人';

$_LANG['erp_stock_record_goods_price'] ='产品价格';
$_LANG['erp_stock_record_goods_amount'] ='产品金额';

$_LANG['erp_stock_record_date'] ='出入库日期';

$_LANG['erp_stock_record_search_stock_style_both'] ='出库和入库';
$_LANG['erp_stock_record_search_stock_style_only_warehousing'] ='入库';
$_LANG['erp_stock_record_search_stock_style_only_delivery'] ='出库';

$_LANG['erp_stock_record_search_warehouse'] ='请选择仓库';
$_LANG['erp_stock_record_search_w_d_style'] ='请选择出入库类型';
$_LANG['erp_stock_record_search_goods_sn'] ='产品货号';
$_LANG['erp_stock_record_search_goods_name'] ='产品名称';
$_LANG['erp_stock_record_search_goods_barcode'] ='产品条码';
$_LANG['erp_stock_record_search_provider_goods_sn'] ='供应商货号';
$_LANG['erp_stock_record_search_start_date'] ='起始日期';
$_LANG['erp_stock_record_search_end_date'] ='结束日期';

$_LANG['erp_stock_record_search_supplier'] ='请选择供货商';
$_LANG['erp_stock_record_search_brand'] ='请选择品牌';

/* 仓库转拨 */
$_LANG['erp_stock_transfer_id'] ='序号';
$_LANG['erp_stock_transfer_sn'] ='编号';
$_LANG['erp_stock_transfer_warehouse_from'] ='发货仓库';
$_LANG['erp_stock_transfer_agency_from'] ='发货办事处';
$_LANG['erp_stock_transfer_warehouse_to'] ='收货仓库';
$_LANG['erp_stock_transfer_agency_to'] ='收货办事处';
$_LANG['erp_stock_transfer_admin_from'] ='发货仓管';
$_LANG['erp_stock_transfer_admin_to'] ='收货仓管';
$_LANG['erp_stock_transfer_transfer_qty'] ='转拨数量';
$_LANG['erp_stock_transfer_create_time'] ='发货日期';
$_LANG['erp_stock_transfer_transfer_time'] ='收货日期';
$_LANG['erp_stock_transfer_status'] ='状态';

$_LANG['erp_stock_transfer_status_inputing'] = '转拨单录入中';//status=1
$_LANG['erp_stock_transfer_status_await_recieve'] = '待确认收货';//status=2
$_LANG['erp_stock_transfer_status_approve_pass'] = '已转拨';//status=3
$_LANG['erp_stock_transfer_status_approve_reject'] = '拒绝收货';//status=4

$_LANG['erp_operation_post_transfer'] ='提交转拨';
$_LANG['erp_operation_confirm_transfer'] ='确认转拨';
$_LANG['erp_operation_reject_transfer'] ='拒绝转拨';
$_LANG['erp_operation_withdrawal_transfer'] ='退回';

$_LANG['erp_stock_transfer_search_warehouse_from'] ='选择发货仓库';
$_LANG['erp_stock_transfer_search_warehouse_to'] ='选择收货仓库';
$_LANG['erp_stock_transfer_search_admin_from'] ='选择发货仓管';
$_LANG['erp_stock_transfer_search_admin_to'] ='选择收货仓管';
$_LANG['erp_stock_transfer_search_goods_sn'] ='产品货号';
$_LANG['erp_stock_transfer_search_goods_name'] ='产品名称';
$_LANG['erp_stock_transfer_search_status'] ='转拨单状态';

$_LANG['erp_stock_transfer_view'] ='查看转拨单';
$_LANG['erp_stock_transfer_edit'] ='编辑转拨单';
$_LANG['erp_stock_transfer_confirm'] ='确认转拨单';

$_LANG['erp_stock_transfer_operation_print'] ='打印转拨单';
$_LANG['erp_stock_transfer_item_id'] ='序号';
$_LANG['erp_stock_transfer_item_goods_sn'] ='产品货号';
$_LANG['erp_stock_transfer_item_goods_img'] ='产品图片';
$_LANG['erp_stock_transfer_item_goods_name'] ='产品名称';
$_LANG['erp_stock_transfer_item_goods_attr'] ='属性';
$_LANG['erp_stock_transfer_item_transfer_qty'] ='转拨数量';
$_LANG['erp_stock_transfer_reurn'] ='返回到转拨单列表';
$_LANG['erp_stock_transfer_no_access'] ='不能编辑其他仓管员建立的转拨单';
$_LANG['erp_order_no_accessibility'] ='其他用户正在编辑此转拨单,不能进行此项操作！';
$_LANG['erp_stock_transfer_without_valid_warehouse'] ='管理员尚未给您分配仓库，不能建立、编辑或修改转拨单。';
$_LANG['erp_stock_transfer_select_warehouse_from'] ='选择发货仓库';
$_LANG['erp_stock_transfer_select_warehouse_to'] ='选择收货仓库';
$_LANG['erp_stock_transfer_not_exists'] ='转拨单不存在';
$_LANG['erp_stock_transfer_item_not_exists'] ='转拨单项不存在';
$_LANG['erp_stock_transfer_no_access'] ='不能编辑其他仓管员建立的转拨库单';
$_LANG['erp_stock_transfer_no_accessibility']='其他用户正在编辑此转拨库单,不能进行此项操作！';
$_LANG['erp_stock_transfer_warehouse_same'] ='收货仓库和发货仓库不能是同一个';
$_LANG['erp_stock_transfer_input_goods_sn'] ='货号：';
$_LANG['erp_stock_transfer_input_goods_name'] ='名称：';
$_LANG['erp_stock_transfer_input_qty'] ='数量：';
$_LANG['erp_stock_transfer_stock_not_enough'] ='转拨数量大于出库数量（现有库存%d），不能转拨';
$_LANG['erp_stock_transfer_warehouse_cant_change'] ='删除转拨单项后才能更改发货仓库';
$_LANG['erp_stock_transfer_goods_sn_required'] ='请输入货号。';
$_LANG['erp_stock_transfer_goods_name_required'] ='请输入货号。';
$_LANG['erp_stock_transfer_goods_sn_or_goods_name_required'] ='请输入货号或产品名称。';
$_LANG['erp_stock_transfer_wrong_goods_sn'] ='请核对输入的产品货号。';
$_LANG['erp_stock_transfer_wrong_goods_name'] ='请核对输入的产品名称。';
$_LANG['erp_stock_transfer_goods_sn_required'] ='请输入货号。';
$_LANG['erp_stock_transfer_no_acts'] = '该转拨单状态不能进行此操作';
$_LANG['erp_stock_transfer_sure_to_delete'] ='您确定要删除此转拨单吗？';
$_LANG['erp_stock_transfer_sure_to_delete_item'] ='您确定要删除此转拨单项吗？';
$_LANG['erp_add_stock_transfer'] = '仓库转拨';
$_LANG['erp_stock_transfer_sure_to_post'] = '您确定要提交此转拨单吗？';
$_LANG['erp_stock_transfer_sure_to_withdrawal'] = '您确定要退回此转拨单吗？';

$_LANG['erp_stock_transfer_return_to_list'] ='返回到仓库转拨单列表';
$_LANG['erp_stock_transfer_no_agency_access'] ='不能查看、编辑其他办事处的转拨单';

$_LANG['erp_stock_transfer_not_completed']="此转拨单尚未完善，不能提交，需要检查的项目有：
 * 1，收货仓库和发货仓库不能相同
 * 2，是否最少有1个转拨单项
 * 3，每条转拨单项的发货数量是否大于0";
 
 $_LANG['erp_stock_transfer_no_confirm_accessibility'] = '您不是该转拨单收货仓库的仓管，不能执行此操作。';
$_LANG['erp_stock_transfer_sure_to_confirm'] ='您确定要确认此转拨单吗？';
$_LANG['erp_stock_transfer_confirm_failed'] ='转拨单确认操作失败';
$_LANG['erp_stock_transfer_confirm_failed_no_stock'] ='库存不够，转拨单确认操作失败';
$_LANG['erp_stock_transfer_w_d_style'] ='仓库转拨';

$_LANG['erp_stock_transfer_print_title'] ='仓库转拨单';
$_LANG['erp_stock_transfer_print_user1'] ='发货仓管';
$_LANG['erp_stock_transfer_print_user2'] ='收货仓管';
$_LANG['erp_stock_transfer_print_user3'] ='主管';

/* 库存变动报表 */
$_LANG['erp_stock_daily_goods_sn'] = '货号';
$_LANG['erp_stock_daily_start_date'] = '开始日期';
$_LANG['erp_stock_daily_end_date'] = '结束日期';
$_LANG['erp_stock_daily_warehouse'] = '仓库';
$_LANG['erp_stock_daily_date'] = '日期';
$_LANG['erp_stock_daily_stock'] = '库存';
$_LANG['erp_stock_daily_select_warehouse'] = '选择仓库';
$_LANG['erp_stock_daily_warehouse_required'] = '请选择仓库';

$_LANG['erp_stock_daily_goods_sn_required'] = '请输入产品货号';
$_LANG['erp_stock_daily_goods_sn_wrong'] = '您输入的货号有误';
$_LANG['erp_stock_daily_return'] = '返回';

$_LANG['erp_stock_daily_no_agency_access'] = '不能查看、编辑其他办事处的库存';

$_LANG['last_update'] = '最后更新日期';
$_LANG['now_update'] = '更新记录';
$_LANG['update_success'] = '分析记录已成功更新!';
$_LANG['view_log'] = '查看分析记录';
$_LANG['select_year_month'] = '查询年月';

$_LANG['pv_stats'] = '综合访问数据';
$_LANG['integration_visit'] = '综合访问量';
$_LANG['seo_analyse'] = '搜索引擎分析';
$_LANG['area_analyse'] = '地域分布';
$_LANG['visit_site'] = '来访网站分析';
$_LANG['key_analyse'] = '关键字分析';

$_LANG['start_date'] = '开始日期';
$_LANG['end_date'] = '结束日期';
$_LANG['query'] = '查询';
$_LANG['result_filter'] = '过滤结果';
$_LANG['compare_query'] = '比较查询';
$_LANG['year_status'] = '年走势';
$_LANG['month_status'] = '月走势';

$_LANG['year'] = '年';
$_LANG['month'] = '月';
$_LANG['day'] = '日';
$_LANG['year_format'] = '%Y';
$_LANG['month_format'] = '%c';

$_LANG['from'] = '从';
$_LANG['to'] = '到';
$_LANG['view'] = '查看';

/* 应付款列表 应付款详情 */
$_LANG['erp_payable_order_id'] = '序号';
$_LANG['erp_payable_order_sn'] = '编号';
$_LANG['erp_payable_order_description'] = '描述';
$_LANG['erp_payable_order_agency'] = '办事处';
$_LANG['erp_payable_order_supplier'] = '供应商';
$_LANG['erp_payable_order_date'] = '订单日期';
$_LANG['erp_payable_order_amount'] = '订单金额';
$_LANG['erp_payable_total_payable_amount'] = '应付款';
$_LANG['erp_payable_total_paid_amount'] = '已付款';
$_LANG['erp_payable_payable'] = '剩余款';
$_LANG['erp_payable_pay'] = '付款金额';

$_LANG['erp_payable_operation_pay'] = '付款';

$_LANG['erp_payable_search_all_orders'] = '所有订单';
$_LANG['erp_payable_search_payable_orders'] = '应付款订单';
$_LANG['erp_payable_search_order_sn'] = '订单编号';

$_LANG['erp_payable_no_valid_bank_account'] = '尚未设置有效的银行帐户，不能建立付款单。';

$_LANG['erp_payable_more_than_payable'] = '付款金额不能大于应付款！';

$_LANG['erp_payable_details'] = '应付款详情';

$_LANG['erp_payable_sure_to_withdrawal_to_edit'] = "您确定要退回此付款单吗？";

$_LANG['erp_payable_search_order_sn'] = '订单编号';

$_LANG['erp_payable_search_supplier_name'] = '供应商名称';

$_LANG['erp_payable_no_agency_access'] = '不能查看、编辑其他办事处的应付款';

/* 应收款列表 应收款详情 */
$_LANG['erp_receivable_order_id'] = '序号';
$_LANG['erp_receivable_order_sn'] = '编号';
$_LANG['erp_receivable_agency'] = '办事处';
$_LANG['erp_receivable_order_description'] = '描述';
$_LANG['erp_receivable_order_username'] = '用户名';
$_LANG['erp_receivable_order_consignee'] = '收货人';
$_LANG['erp_receivable_order_shipping_date'] = '发货时间';
$_LANG['erp_receivable_order_amount'] = '订单金额';
$_LANG['erp_receivable_total_receivable_amount'] = '应收款';
$_LANG['erp_receivable_total_gathered_amount'] = '已收款';
$_LANG['erp_receivable_receivable'] = '剩余款';
$_LANG['erp_receivable_gather'] = '付款金额';

$_LANG['erp_receivable_goods_id'] = '序号';
$_LANG['erp_receivable_goods_sn'] = '产品货号';
$_LANG['erp_receivable_goods_img'] = '产品图片';
$_LANG['erp_receivable_goods_attr'] = '产品属性';
$_LANG['erp_receivable_goods_price'] = '产品价格';
$_LANG['erp_receivable_goods_order_qty'] = '购买数量';
$_LANG['erp_receivable_goods_delivered_qty'] = '已发货数量';
$_LANG['erp_receivable_goods_amount'] = '总价';

$_LANG['erp_receivable_operation_gather'] = '收款';

$_LANG['erp_receivable_search_all_orders'] = '所有订单';
$_LANG['erp_receivable_search_receivable_orders'] = '应收款订单';
$_LANG['erp_receivable_search_order_sn'] = '订单编号';

$_LANG['erp_receivable_more_than_receivable'] = '收款金额不能大于应收款！';

$_LANG['erp_receivable_details'] = '应收款详情';

$_LANG['erp_receivable_no_valid_bank_account'] = '尚未设置有效的银行帐户，不能建立收款单。';

$_LANG['erp_receivable_sure_to_withdrawal_to_edit'] = "您确定要退回此收款单吗？";

$_LANG['erp_receivable_no_agency_access'] = '不能查看、编辑其他办事处的应收款';

/* 银行帐号 */
$_LANG['erp_bank_account_id'] = '序号';
$_LANG['erp_bank_account_name'] = '帐户名称';
$_LANG['erp_bank_account_no'] = '帐号';
$_LANG['erp_bank_account_agency'] = '所属办事处';
$_LANG['erp_bank_account_balance'] = '帐户余额';

$_LANG['erp_bank_account_not_exists'] = '该银行帐号不存在或已被删除！';

$_LANG['erp_bank_account_details_id'] = '序号';
$_LANG['erp_bank_account_details_income_expenses'] = '收入或支出';
$_LANG['erp_bank_account_details_payment_gathering'] = '关联收付款单号';
$_LANG['erp_bank_account_details_order'] = '关联订单号';
$_LANG['erp_bank_account_details_date'] = '收支日期';
$_LANG['erp_bank_account_details_remark'] = '备注';

$_LANG['erp_bank_account_details_start_date'] = ' 起始日期';
$_LANG['erp_bank_account_details_end_date'] = ' 结束日期';

$_LANG['erp_bank_account_add'] = '添加银行帐号';
$_LANG['erp_bank_account_edit'] = '编辑银行帐号';
$_LANG['erp_bank_account_name_required'] = '请输入银行帐户名称。';
$_LANG['erp_bank_account_no_required'] = '请输入银行帐号。';

$_LANG['erp_bank_account_select_agency'] = '选择办事处';

$_LANG['erp_bank_account_return_to_list'] = '返回到银行帐号列表';

$_LANG['erp_bank_account_has_gathering'] = '此银行帐号有相关联的收款单，不能编辑或删除。';
$_LANG['erp_bank_account_has_payment'] = '此银行帐号有相关联的付款单，不能编辑或删除。';
$_LANG['erp_bank_account_has_bank_record'] = '此银行帐号有收付款记录，不能编辑或删除';

$_LANG['erp_bank_account_no_agency_access'] = '不能查看、编辑其他办事处的银行帐号信息';

$_LANG['erp_bank_account_details'] = '银行帐户收支明细';

$_LANG['erp_bank_account_return'] = '返回到银行帐号列表';

$_LANG['erp_bank_account_add_success'] = '成功添加银行帐号';

/* 打印银行收支清单 */
$_LANG['erp_print_bank_account_details']='银行帐户收支清单';

/* 付款类型 */
$_LANG['erp_payment_style_id'] = '序号';
$_LANG['erp_payment_style'] = '付款类型';
$_LANG['erp_payment_style_not_exists'] = '该付款类型不存在或已被删除！';

/* 付款单 */
$_LANG['erp_add_payment'] = '添加付款单';

$_LANG['erp_payment_id'] = '序号';
$_LANG['erp_payment_sn'] = '付款单编号';
$_LANG['erp_payment_date'] = '付款日期';
$_LANG['erp_payment_pay_to'] = '收款人';
$_LANG['erp_payment_remark'] = '付款说明';
$_LANG['erp_payment_agency'] = '办事处';
$_LANG['erp_payment_order_id'] = '订单号';
$_LANG['erp_payment_order_date'] = '订单日期';
$_LANG['erp_payment_order_amount'] = '订单总金额';
$_LANG['erp_payment_order_total_payable'] = '订单总应付款';
$_LANG['erp_payment_order_paid_payable'] = '订单已付应付款';
$_LANG['erp_payment_order_payable_balance'] = '订单剩余应付款';
$_LANG['erp_payment_pay_amount'] = '本次付款金额';
$_LANG['erp_payment_account'] = '付款帐号';

$_LANG['erp_payment_status'] = '付款单状态';

$_LANG['erp_payment_operation_view_approve_remark'] = '审核备注';

$_LANG['erp_payment_status_inputing'] = '付款单录入中';//payment_status=1
$_LANG['erp_payment_status_approving'] = '等待主管审核';//payment_status=2
$_LANG['erp_payment_status_approve_pass'] = '审核已通过';//payment_status=3
$_LANG['erp_payment_status_approve_reject'] = '审核未通过';//payment_status=4

$_LANG['erp_payment_search_payment_status'] = '付款单状态';
$_LANG['erp_payment_search_pay_to'] = '收款人';
$_LANG['erp_payment_search_start_date'] = '起始日期';
$_LANG['erp_payment_search_end_date'] = '结束日期';
$_LANG['erp_payment_search_payment_sn'] = '付款单编号';
$_LANG['erp_payment_search_bill_sn'] = '订单编号';

$_LANG['erp_payment_no_pay'] = '您还没有输入付款金额。';

$_LANG['erp_payment_return_to_payment_list']='返回到付款单列表';
$_LANG['erp_payment_view_payment']='查看此付款单';

$_LANG['erp_payment_sure_to_post'] = '您确定要付款吗？';
$_LANG['erp_payment_sure_to_pay'] = "您确定要付款，并生成一个付款单吗？";
$_LANG['erp_payment_sure_to_delete_payment']='您确定要删除付款单吗？';
$_LANG['erp_payment_sure_to_post_payment']='您确定要提交付款单吗？';
$_LANG['erp_payment_sure_to_change_bank_account']='您确定要更改付款银行帐号吗？';
$_LANG['erp_payment_sure_to_change_pay_amount']='您确定要更改付款金额吗？';
$_LANG['erp_payment_sure_to_approve'] = "您确定要提交审核吗？";
$_LANG['erp_payment_sure_to_change_order'] = '您确定要更改订单吗？';

$_LANG['erp_payment_greater_than_payable']='付款金额不能大于订单剩余应付款。';

$_LANG['erp_payment_not_exist']='此付款单不存在或已被删除！';
$_LANG['erp_payment_delete_sucess']='成功删除付款单';
$_LANG['erp_payment_no_accessibility']='其他用户正在编辑此付款单,不能进行此项操作！';

$_LANG['erp_payment_post_success']='成功提交付款单。';
$_LANG['erp_payment_approve_success']='成功提交审核。';

$_LANG['erp_payment_no_agency_access']='不能查看、编辑其他办事处的付款单';

$_LANG['erp_payment_no_acts'] = '该付款单状态不能进行此操作';

/* 收款单 */
$_LANG['erp_add_gathering'] = '添加收款单';

$_LANG['erp_gathering_anonymous'] = '匿名用户';

$_LANG['erp_gathering_id'] = '序号';
$_LANG['erp_gathering_sn'] = '收款单编号';
$_LANG['erp_gathering_date'] = '收款日期';
$_LANG['erp_gathering_pay_from'] = '付款人';
$_LANG['erp_gathering_remark'] = '收款说明';
$_LANG['erp_gathering_agency'] = '办事处';
$_LANG['erp_gathering_order_id'] = '订单号';
$_LANG['erp_gathering_order_date'] = '订单日期';
$_LANG['erp_gathering_order_amount'] = '订单总金额';
$_LANG['erp_gathering_order_total_receivable'] = '订单总应收款';
$_LANG['erp_gathering_order_paid_receivable'] = '订单已付应收款';
$_LANG['erp_gathering_order_receivable_balance'] = '订单剩余应收款';
$_LANG['erp_gathering_gather_amount'] = '收款金额';
$_LANG['erp_gathering_account'] = '收款帐号';

$_LANG['erp_gathering_status'] = '收款单状态';

$_LANG['erp_gathering_operation_view_approve_remark'] = '审核备注';

$_LANG['erp_gathering_status_inputing'] = '收款单录入中';//gathering_status=1
$_LANG['erp_gathering_status_approving'] = '等待主管审核';//gathering_status=2
$_LANG['erp_gathering_status_approve_pass'] = '审核已通过';//gathering_status=3
$_LANG['erp_gathering_status_approve_reject'] = '审核未通过';//gathering_status=4

$_LANG['erp_gathering_search_gathering_status'] = '收款单状态';
$_LANG['erp_gathering_search_pay_from'] = '付款人';
$_LANG['erp_gathering_search_start_date'] = '起始日期';
$_LANG['erp_gathering_search_end_date'] = '结束日期';
$_LANG['erp_gathering_search_gathering_sn'] = '收款单编号';
$_LANG['erp_gathering_search_order_sn'] = '订单编号';

$_LANG['erp_gathering_no_gather'] = '您还没有输入收款金额。';

$_LANG['erp_gathering_return_to_gathering_list']='返回到收款单列表。';
$_LANG['erp_gathering_view_gathering']='查看此收款单。';

$_LANG['erp_gathering_sure_to_change_order'] = '您确定要更改订单吗？';
$_LANG['erp_gathering_sure_to_post'] = '您确定要收款吗？';
$_LANG['erp_gathering_sure_to_gather'] = "您确定要收款，并生成一个收款单吗？";
$_LANG['erp_gathering_sure_to_delete_gathering']='您确定要删除收款单吗？';
$_LANG['erp_gathering_sure_to_post_gathering']='您确定要提交收款单吗？';
$_LANG['erp_gathering_sure_to_change_bank_account']='您确定要更改收款银行帐号吗？';
$_LANG['erp_gathering_sure_to_change_gather_amount']='您确定要更改收款金额吗？';
$_LANG['erp_gathering_sure_to_approve'] = "您确定要提交审核吗？";

$_LANG['erp_gathering_greater_than_receivable']='收款金额不能大于订单剩余应收款。';

$_LANG['erp_gathering_not_exist']='此收款单不存在或已被删除！';
$_LANG['erp_gathering_delete_sucess']='成功删除收款单。';
$_LANG['erp_gathering_no_accessibility']='其他用户正在编辑此收款单,不能进行此项操作！';

$_LANG['erp_gathering_post_success']='成功提交收款单。';
$_LANG['erp_gathering_approve_success']='成功提交审核。';

$_LANG['erp_gathering_no_agency_access']='不能查看、编辑其他办事处的收款单';

$_LANG['erp_gathering_no_acts']='该收款单状态不能进行此操作';

/* 产品条码列表 */
$_LANG['erp_barcode_id'] = '序号';
$_LANG['erp_barcode_goods_id'] = '产品ID';
$_LANG['erp_barcode_goods_img'] ='产品图片';
$_LANG['erp_barcode_goods_sn'] ='产品货号';
$_LANG['erp_barcode_goods_name'] ='产品名称';
$_LANG['erp_barcode_goods_attr'] ='产品属性';
$_LANG['erp_barcode_goods_barcode'] ='产品条码';
$_LANG['erp_barcode_attr_and_barcode'] ='属性-条码';

$_LANG['erp_barcode_exists'] ='该条码已存在，请重新输入。';

$_LANG['erp_barcode_search_warehouse'] ='请选择仓库';
$_LANG['erp_barcode_search_goods_sn'] ='产品货号';
$_LANG['erp_barcode_search_goods_name'] ='产品名称';
$_LANG['erp_barcode_search_provider_goods_sn'] ='供应商货号';
?>
