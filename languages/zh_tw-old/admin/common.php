<?php

/**
 * ECSHOP 管理中心共用語言文件
 * ============================================================================
 * 版權所有 2005-2011 上海商派網絡科技有限公司，並保留所有權利。
 * 網站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 這不是一個自由軟件！您只能在不用於商業目的的前提下對程序代碼進行修改和
 * 使用；不允許對程序代碼以任何形式任何目的的再發佈。
 * ============================================================================
 * $Author: liubo $
 * $Id: common.php 17217 2011-01-19 06:29:08Z liubo $
*/

$_LANG['app_name'] = '友和電子消費品';
$_LANG['cp_home'] = '網站管理';
$_LANG['copyright'] = '版權所有 &copy; 2012 - 2013 友和電子消費品';
$_LANG['query_info'] = '共執行 %d 個查詢，用時 %s 秒';
$_LANG['memory_info'] = '，內存佔用 %0.3f MB';
$_LANG['gzip_enabled'] = '，Gzip 已啟用';
$_LANG['gzip_disabled'] = '，Gzip 已禁用';
$_LANG['loading'] = '正在處理您的請求...';
$_LANG['js_languages']['process_request'] = '正在處理您的請求...';
$_LANG['js_languages']['todolist_caption'] = '記事本';
$_LANG['js_languages']['todolist_autosave'] = '自動保存';
$_LANG['js_languages']['todolist_save'] = '保存';
$_LANG['js_languages']['todolist_clear'] = '清除';
$_LANG['js_languages']['todolist_confirm_save'] = '是否將更改保存到記事本？';
$_LANG['js_languages']['todolist_confirm_clear'] = '是否清空內容？';
$_LANG['auto_redirection'] = '如果您不做出選擇，將在 <span id="spanSeconds">3</span> 秒後跳轉到第一個鏈接地址。';
$_LANG['password_rule'] = '密碼應只包含英文字符、數字.長度在6--16位之間';
$_LANG['username_rule'] = '用戶名應為漢字、英文字符、數字組合，3到15位';
$_LANG['plugins_not_found'] = '插件 %s 無法定位';
$_LANG['no_records'] = '沒有找到任何記錄';
$_LANG['role_describe'] = '角色描述';

$_LANG['require_field'] = '<span class="require-field">*</span>';
$_LANG['yes'] = '是';
$_LANG['no'] = '否';
$_LANG['record_id'] = '編號';
$_LANG['handler'] = '操作';
$_LANG['install'] = '安裝';
$_LANG['uninstall'] = '卸載';
$_LANG['list'] = '列表';
$_LANG['add'] = '添加';
$_LANG['edit'] = '編輯';
$_LANG['view'] = '查看';
$_LANG['remove'] = '移除';
$_LANG['drop'] = '刪除';
$_LANG['confirm_delete'] = '您確定要刪除嗎?';
$_LANG['disabled'] = '禁用';
$_LANG['enabled'] = '啟用';
$_LANG['setup'] = '設置';
$_LANG['success'] = '成功';
$_LANG['sort_order'] = '排序';
$_LANG['trash'] = '回收站';
$_LANG['restore'] = '還原';
$_LANG['close_window'] = '關閉窗口';
$_LANG['btn_select'] = '選擇';
$_LANG['operator'] = '操作人';
$_LANG['cancel'] = '取消';

$_LANG['empty'] = '不能為空';
$_LANG['repeat'] = '已存在';
$_LANG['is_int'] = '應該為整數';

$_LANG['button_submit'] = ' 確定 ';
$_LANG['button_save'] = ' 保存 ';
$_LANG['button_reset'] = ' 重置 ';
$_LANG['button_search'] = ' 搜索 ';

$_LANG['priv_error'] = '對不起,您沒有執行此項操作的權限!';
$_LANG['drop_confirm'] = '您確認要刪除這條記錄嗎?';
$_LANG['form_notice'] = '點擊此處查看提示信息';
$_LANG['upfile_type_error'] = '上傳文件的類型不正確!';
$_LANG['upfile_error'] = '上傳文件失敗!';
$_LANG['no_operation'] = '你沒有選擇任何操作';

$_LANG['go_back'] = '返回上一頁';
$_LANG['back'] = '返回';
$_LANG['continue'] = '繼續';
$_LANG['system_message'] = '系統信息';
$_LANG['check_all'] = '全選';
$_LANG['select_please'] = '請選擇...';
$_LANG['all_category'] = '所有分類';
$_LANG['all_brand'] = '所有品牌';
$_LANG['refresh'] = '刷新';
$_LANG['update_sort'] = '更新排序';
$_LANG['modify_failure'] = '修改失敗!';
$_LANG['attradd_succed'] = '操作成功!';
$_LANG['todolist'] = '記事本';
$_LANG['n_a'] = 'N/A';

/* 提示 */
$_LANG['sys']['wrong'] = '錯誤：';

/* 編碼 */
$_LANG['charset']['utf8'] = '國際化編碼（utf8）';
$_LANG['charset']['zh_cn'] = '簡體中文';
$_LANG['charset']['zh_tw'] = '繁體中文';
$_LANG['charset']['en_us'] = '美國英語';
$_LANG['charset']['en_uk'] = '英文';

/* 新訂單通知 */
$_LANG['order_notify'] = '新訂單通知';
$_LANG['new_order_1'] = '您有 ';
$_LANG['new_order_2'] = ' 個新訂單以及 ';
$_LANG['new_order_3'] = ' 個新付款的訂單';
$_LANG['new_order_link'] = '點擊查看新訂單';

/*語言項*/
$_LANG['chinese_simplified'] = '簡體中文';
$_LANG['english'] = '英文';

/* 分頁 */
$_LANG['total_records'] = '總計 ';
$_LANG['total_pages'] = '個記錄分為';
$_LANG['page_size'] = '頁，每頁';
$_LANG['page_current'] = '頁當前第';
$_LANG['page_first'] = '第一頁';
$_LANG['page_prev'] = '上一頁';
$_LANG['page_next'] = '下一頁';
$_LANG['page_last'] = '最末頁';
$_LANG['admin_home'] = '起始頁';

/* 重量 */
$_LANG['gram'] = '克';
$_LANG['kilogram'] = '千克';

/* 菜單分類部分 */
$_LANG['02_cat_and_goods'] = '商品管理';
$_LANG['03_richang'] = '日常工作';
$_LANG['03_promotion'] = '促銷管理';
$_LANG['04_order'] = '訂單管理';
$_LANG['05_banner'] = '廣告管理';
$_LANG['06_stats'] = '報表統計';
$_LANG['07_content'] = '文章管理';
$_LANG['08_members'] = '會員管理';
$_LANG['09_others'] = '雜項管理';
$_LANG['10_priv_admin'] = '權限管理';
$_LANG['11_system'] = '系統設置';
$_LANG['12_template'] = '模板管理';
$_LANG['13_backup'] = '數據庫管理';
$_LANG['14_sms'] = '短信管理';
$_LANG['15_rec'] = '推薦管理';
$_LANG['16_email_manage'] = '郵件群發管理';

/* 商品管理 */
$_LANG['01_goods_list'] = '商品列表';
$_LANG['02_goods_add'] = '添加新商品';
$_LANG['03_category_list'] = '商品分類';
$_LANG['04_category_add'] = '添加分類';
$_LANG['05_comment_manage'] = '用家評價';
$_LANG['06_goods_brand_list'] = '商品品牌';
$_LANG['07_brand_add'] = '添加品牌';
$_LANG['08_goods_type'] = '商品類型';
$_LANG['09_attribute_list'] = '商品屬性';
$_LANG['10_attribute_add'] = '添加屬性';
$_LANG['11_goods_trash'] = '商品回收站';
$_LANG['12_batch_pic'] = '圖片批量處理';
$_LANG['13_batch_add'] = '商品批量上傳';
$_LANG['15_batch_edit'] = '商品批量修改';
$_LANG['16_goods_script'] = '生成商品代碼';
$_LANG['17_tag_manage'] = '標籤管理';
$_LANG['18_product_list'] = '貨品列表';
$_LANG['52_attribute_add'] = '編輯屬性';
$_LANG['53_suppliers_goods'] = '供貨商商品管理';

$_LANG['14_goods_export'] = '商品批量導出';

$_LANG['50_virtual_card_list'] = '虛擬卡商品列表';
$_LANG['51_virtual_card_add'] = '添加虛擬商品';
$_LANG['52_virtual_card_change'] = '更改加密串';
$_LANG['goods_auto'] = '商品自動上下架';
$_LANG['article_auto'] = '文章自動發佈';
$_LANG['navigator'] = '自定義導航欄';

/* 促銷管理 */
$_LANG['02_snatch_list'] = '奪寶奇兵';
$_LANG['snatch_add'] = '添加奪寶奇兵';
$_LANG['04_bonustype_list'] = '紅包類型';
$_LANG['bonustype_add'] = '添加紅包類型';
$_LANG['05_bonus_list'] = '線下紅包';
$_LANG['bonus_add'] = '添加會員紅包';
$_LANG['06_pack_list'] = '商品包裝';
$_LANG['07_card_list'] = '祝福賀卡';
$_LANG['pack_add'] = '添加新包裝';
$_LANG['card_add'] = '添加新賀卡';
$_LANG['08_group_buy'] = '團購活動';
$_LANG['09_topic'] = '專題管理';
$_LANG['topic_add'] = '添加專題';
$_LANG['topic_list'] = '專題列表';
$_LANG['10_auction'] = '拍賣活動';
$_LANG['12_favourable'] = '著數優惠';
$_LANG['13_wholesale'] = '批發管理';
$_LANG['ebao_commend'] = '易寶推薦';
$_LANG['14_package_list'] = '套裝優惠';
$_LANG['package_add'] = '添加套裝優惠';

//Powered By UUECS QQ909065309 HTTP://Www.UUECS.Com


/* 訂單管理 */
$_LANG['02_order_list'] = '訂單列表';
$_LANG['03_order_query'] = '訂單查詢';
$_LANG['04_merge_order'] = '合併訂單';
$_LANG['05_edit_order_print'] = '訂單打印';
$_LANG['06_undispose_booking'] = '缺貨登記';
$_LANG['08_add_order'] = '添加訂單';
$_LANG['09_delivery_order'] = '發貨單列表';
$_LANG['10_back_order'] = '退貨單列表';

/* 廣告管理 */
$_LANG['ad_position'] = '廣告位置';
$_LANG['ad_list'] = '廣告列表';

/* 報表統計 */
$_LANG['flow_stats'] = '流量分析';
$_LANG['searchengine_stats'] = '搜索引擎';
$_LANG['report_order'] = '訂單統計';
$_LANG['report_sell'] = '銷售概況';
$_LANG['sell_stats'] = '銷售排行';
$_LANG['sale_list'] = '銷售明細';
$_LANG['report_guest'] = '客戶統計';
$_LANG['report_users'] = '會員排行';
$_LANG['visit_buy_per'] = '訪問購買率';
$_LANG['z_clicks_stats'] = '站外投放JS';

/* 文章管理 */
$_LANG['02_articlecat_list'] = '文章分類';
$_LANG['articlecat_add'] = '添加文章分類';
$_LANG['03_article_list'] = '文章列表';
$_LANG['article_add'] = '添加新文章';
$_LANG['shop_article'] = '網店文章';
$_LANG['shop_info'] = '網店信息';
$_LANG['shop_help'] = '網店幫助';
$_LANG['vote_list'] = '在線調查';

/* 會員管理 */
$_LANG['08_unreply_msg'] = '會員留言';
$_LANG['03_users_list'] = '會員列表';
$_LANG['04_users_add'] = '添加會員';
$_LANG['05_user_rank_list'] = '會員等級';
$_LANG['06_list_integrate'] = '會員整合';
$_LANG['09_user_account'] = '充值和提現申請';
$_LANG['10_user_account_manage'] = '資金管理';

/* 權限管理 */
$_LANG['admin_list'] = '管理員列表';
$_LANG['admin_list_role'] = '角色列表';
$_LANG['admin_role'] = '角色管理';
$_LANG['admin_edit_role'] = '修改角色';
$_LANG['admin_add'] = '添加管理員';
$_LANG['admin_add_role'] = '添加角色';
$_LANG['admin_logs'] = '管理員日誌';
$_LANG['agency_list'] = '辦事處列表';
$_LANG['suppliers_list'] = '供貨商列表';

/* 系統設置 */
$_LANG['01_shop_config'] = '商店設置';
$_LANG['shop_authorized'] = '授權證書';
$_LANG['shp_webcollect'] = '網羅天下';
$_LANG['02_payment_list'] = '支付方式';
$_LANG['03_shipping_list'] = '配送方式';
$_LANG['04_mail_settings'] = '郵件服務器設置';
$_LANG['05_area_list'] = '地區列表';
$_LANG['07_cron_schcron'] = '計劃任務';
$_LANG['08_friendlink_list'] = '友情鏈接';
$_LANG['shipping_area_list'] = '配送區域';
$_LANG['sitemap'] = '站點地圖';
$_LANG['check_file_priv'] = '文件權限檢測';
$_LANG['captcha_manage'] = '驗證碼管理';
$_LANG['fckfile_manage'] = 'Fck上傳文件管理';
$_LANG['ucenter_setup'] = 'UCenter設置';
$_LANG['file_check'] = '文件校驗';
$_LANG['021_reg_fields'] = '會員註冊項設置';

/* 模板管理 */
$_LANG['02_template_select'] = '模板選擇';
$_LANG['03_template_setup'] = '設置模板';
$_LANG['04_template_library'] = '庫項目管理';
$_LANG['mail_template_manage'] = '郵件模板';
$_LANG['05_edit_languages'] = '語言項編輯';
$_LANG['06_template_backup'] = '模板設置備份';
/* 數據庫管理 */
$_LANG['02_db_manage'] = '數據備份';
$_LANG['03_db_optimize'] = '數據表優化';
$_LANG['04_sql_query'] = 'SQL查詢';
$_LANG['05_synchronous'] = '同步數據';
$_LANG['convert'] = '轉換數據';

/* 短信管理 */
$_LANG['02_sms_my_info'] = '賬號信息';
$_LANG['03_sms_send'] = '發送短信';
$_LANG['04_sms_charge'] = '賬戶充值';
$_LANG['05_sms_send_history'] = '發送記錄';
$_LANG['06_sms_charge_history'] = '充值記錄';

$_LANG['affiliate'] = '推薦設置';
$_LANG['affiliate_ck'] = '分成管理';
$_LANG['flashplay'] = '首页主广告管理';
$_LANG['search_log'] = '搜索關鍵字';
$_LANG['email_list'] = '郵件訂閱管理';
$_LANG['magazine_list'] = '雜誌管理';
$_LANG['attention_list'] = '關注管理';
$_LANG['view_sendlist'] = '郵件隊列管理';

/* 积分兑换管理 */
$_LANG['15_exchange_goods'] = '積分商城商品';
$_LANG['15_exchange_goods_list'] = '積分商城商品列表';
$_LANG['exchange_goods_add'] = '添加新商品';

/* cls_image類的語言項 */
$_LANG['directory_readonly'] = '目錄 % 不存在或不可寫';
$_LANG['invalid_upload_image_type'] = '不是允許的圖片格式';
$_LANG['upload_failure'] = '文件 %s 上傳失敗。';
$_LANG['missing_gd'] = '沒有安裝GD庫';
$_LANG['missing_orgin_image'] = '找不到原始圖片 %s ';
$_LANG['nonsupport_type'] = '不支持該圖像格式 %s ';
$_LANG['creating_failure'] = '創建圖片失敗';
$_LANG['writting_failure'] = '圖片寫入失敗';
$_LANG['empty_watermark'] = '水印文件參數不能為空';
$_LANG['missing_watermark'] = '找不到水印文件%s';
$_LANG['create_watermark_res'] = '創建水印圖片資源失敗。水印圖片類型為%s';
$_LANG['create_origin_image_res'] = '創建原始圖片資源失敗，原始圖片類型%s';
$_LANG['invalid_image_type'] = '無法識別水印圖片 %s ';
$_LANG['file_unavailable'] = '文件 %s 不存在或不可讀';

/* 郵件發送錯誤信息 */
$_LANG['smtp_setting_error'] = '郵件服務器設置信息不完整';
$_LANG['smtp_connect_failure'] = '無法連接到郵件服務器 %s';
$_LANG['smtp_login_failure'] = '郵件服務器驗證帳號或密碼不正確';
$_LANG['sendemail_false'] = '郵件發送失敗，請檢查您的郵件服務器設置！';
$_LANG['smtp_refuse'] = '服務器拒絕發送該郵件';
$_LANG['disabled_fsockopen'] = '服務器已禁用 fsocketopen 函數。';

/* 虛擬卡 */
$_LANG['virtual_card_oos'] = '虛擬卡已缺貨';

$_LANG['span_edit_help'] = '點擊修改內容';
$_LANG['href_sort_help'] = '點擊對列表排序';

$_LANG['catname_exist'] = '已存在相同的分類名稱!';
$_LANG['brand_name_exist'] = '已存在相同的品牌名稱!';

$_LANG['alipay_login'] = '<a href="https://www.alipay.com/user/login.htm?goto=https%3A%2F%2Fwww.alipay.com%2Fhimalayas%2Fpracticality_profile_edit.htm%3Fmarket_type%3Dfrom_agent_contract%26customer_external_id%3D%2BC4335319945672464113" target="_blank">立即免費申請支付接口權限</a>';
$_LANG['alipay_look'] = '<a href=\"https://www.alipay.com/himalayas/practicality.htm\" target=\"_blank\">請申請成功後登錄支付寶賬戶查看</a>';

/* 进销存菜单分类部分 */
$_LANG['17_erp_stock_manage'] = "进销存管理";
$_LANG['18_erp_order_manage'] = '采购订单管理';
$_LANG['19_erp_system_manage'] = "进销存设置";
$_LANG['20_erp_finance_manage'] = '财务管理';

/* 进销存采购订单 */
$_LANG['01_erp_order_add'] = '录入采购订单';
$_LANG['02_erp_order_list'] = '采购订单列表';

/* 进销存库存管理 */
$_LANG['erp_goods_warehousing'] = '产品入库';
$_LANG['erp_goods_delivery'] = '产品出库';
$_LANG['erp_goods_dead_stock'] = "呆滞库存分析";
$_LANG['erp_goods_warehousing_report'] = '产品入库报表';
$_LANG['erp_goods_delivery_report'] = '产品出库报表';
$_LANG['01_erp_goods_warehousing_list'] = '入库单列表';
$_LANG['02_erp_goods_delivery_list'] = '出库单列表';
$_LANG['03_erp_goods_warehousing_and_delivery_record'] = '出入库序时簿';
$_LANG['04_erp_stock_inquiry'] = "实时库存查询";
$_LANG['05_erp_stock_check'] = "库存盘点";
$_LANG['06_erp_goods_list'] = '关联产品列表';

/* 进销存财务管理 */
$_LANG['erp_account_payable_list'] = '应付帐款列表';
$_LANG['erp_account_receivable_list'] = '应收帐款列表';
$_LANG['erp_payment_list'] = '付款单列表';
$_LANG['erp_gathering_list'] = '收款单列表';
$_LANG['erp_account_list'] = '银行帐号列表';

/* 进销存系统设置 */
$_LANG['erp_supplier_list'] = '供应商列表';
$_LANG['erp_supplier_group'] = '供应商分类';
$_LANG['erp_warehouse_setting'] = '仓库设置';
$_LANG['erp_warehousing_style_setting'] = '入库类型设置';
$_LANG['erp_delivery_style_setting'] = '出库类型设置';
$_LANG['erp_account_setting'] = '银行帐号设置';
$_LANG['erp_payment_style_setting'] = '付款类型设置';

/* 进销存公用 */
$_LANG['erp_yes'] = '是';
$_LANG['erp_no'] = '否';
$_LANG['erp_sure_to_delete'] = '您确定要删除吗？';
$_LANG['erp_required_item'] = '（必填）';
$_LANG['erp_is_valid'] = '是否有效';
$_LANG['erp_operation']='操作';
$_LANG['erp_operation_delete'] = '删除';
$_LANG['erp_operation_edit'] = '编辑';
$_LANG['erp_operation_view'] = '查看';
$_LANG['erp_operation_print'] = '打印';
$_LANG['erp_operation_post'] = '提交';
$_LANG['erp_operation_withdrawal']='退回';
$_LANG['erp_operation_rate'] = '定价';
$_LANG['erp_operation_approve'] = '审核';
$_LANG['erp_operation_post_to_finance'] = '提交财务';
$_LANG['erp_operation_post_to_approve'] = '提交审核';
$_LANG['erp_operation_approve_pass'] = '通过审核';
$_LANG['erp_operation_approve_reject'] = '拒绝审核';
$_LANG['erp_invalid_img'] = '您上传的图片格式不正确！';
$_LANG['erp_img_too_big'] = '图片文件太大了（最大值：%s），无法上传。';
$_LANG['erp_no_permit']='对不起，您没有进行此操作的权限！';
$_LANG['erp_not_a_number']='请输入数字！';
$_LANG['erp_wrong_parameter']='参数错误！';
$_LANG['erp_retun_to_center']='返回到管理中心';
$_LANG['erp_sum']='合计：';
$_LANG['erp_admin']='负责人';
$_LANG['erp_id']='序号';
$_LANG['erp_NULL']='EMPTY';
$_LANG['erp_operation_withdrawal']='退回';

/* 进销存供应商分类 */
$_LANG['erp_add_supplier_group']='添加供应商分类';
$_LANG['erp_edit_supplier_group']='编辑供应商分类';
$_LANG['erp_supplier_group_name']='供应商分类';
$_LANG['erp_supplier_group_has_supplier']='此供应商分类下尚有供应商，不能删除。';
$_LANG['erp_supplier_group_sure_to_delete']='您确定要删除此供应商分类吗？';
$_LANG['erp_supplier_group_name_required'] = "请输入供应商分类名称。";

/* 进销存供应商列表 */
$_LANG['erp_add_supplier'] = '添加供应商';
$_LANG['erp_edit_supplier'] = '编辑供应商';
$_LANG['erp_supplier_id'] = '序号';
$_LANG['erp_supplier_code'] = '编号';
$_LANG['erp_supplier_address'] = '地址';
$_LANG['erp_supplier_name'] = '名称';
$_LANG['erp_supplier_contact'] = '联系人';
$_LANG['erp_supplier_tel'] = '电话';
$_LANG['erp_supplier_fax'] = '传真';
$_LANG['erp_supplier_qq'] = 'QQ';
$_LANG['erp_supplier_msn'] = 'MSN';
$_LANG['erp_supplier_code_exist'] = "供应商编码重复，请重新输入！";
$_LANG['erp_supplier_code_required'] = "请输入供应商编码。";
$_LANG['erp_supplier_name_required'] = "请输入供应商名称。";
$_LANG['erp_supplier_not_exist'] = "此供应商不存在或已被删除。";
$_LANG['erp_supplier_search_code'] = '供应商编号';
$_LANG['erp_supplier_has_order'] = '此供应商有关联的订单，不能删除。';

/* 进销存采购订单 */
$_LANG['erp_add_order'] = '录入采购订单';
$_LANG['erp_edit_order'] = '编辑订单';
$_LANG['erp_view_order'] = '查看订单';
$_LANG['erp_rate_order'] = '订单定价';
$_LANG['erp_approve_order'] = '审核订单';
$_LANG['erp_operation_change_attr'] = '属性';
$_LANG['erp_order_id'] = '序号';
$_LANG['erp_order_sn'] = '编号';
$_LANG['erp_order_description'] = '描述';
$_LANG['erp_order_supplier'] = '供应商';
$_LANG['erp_order_date'] = '订单日期';
$_LANG['erp_order_item_id'] = '序号';
$_LANG['erp_order_item_goods_sn'] = '产品货号';
$_LANG['erp_order_item_goods_name'] = '产品名称';
$_LANG['erp_order_item_goods_img'] = '产品图片';
$_LANG['erp_order_item_order_qty'] = '定购数量';
$_LANG['erp_order_item_goods_attr'] = '产品属性';
$_LANG['erp_order_item_order_price'] = '定购价格';
$_LANG['erp_order_item_order_amount'] = '总价';
$_LANG['erp_order_status'] = '订单状态';
$_LANG['erp_order_status_inputing'] = '订单录入中';//order_status=1
$_LANG['erp_order_status_rating'] = '等待财务处理';//order_status=2
$_LANG['erp_order_status_approving'] = '等待主管审核订单';//order_status=3
$_LANG['erp_order_status_approve_pass'] = '审核已通过';//order_status=4
$_LANG['erp_order_status_approve_reject'] = '审核未通过';//order_status=5
$_LANG['erp_order_search_order_status'] = '订单状态';
$_LANG['erp_order_search_supplier'] = '供应商';
$_LANG['erp_order_search_start_date'] = '起始日期';
$_LANG['erp_order_search_end_date'] = '结束日期';
$_LANG['erp_order_search_order_sn'] = '订单编号';
$_LANG['erp_order_operation_view_details'] = '要求';
$_LANG['erp_order_operation_view_approve_remark'] = '审核备注';
$_LANG['erp_order_no_admin_supplier'] = '管理员尚未给您分配供应商，不能新建采购订单。';
$_LANG['erp_order_description_required'] = '订单描述不得留空！';
$_LANG['erp_order_supplier_code_required'] = '请输入工厂编号！';
$_LANG['erp_order_size_not_valid'] = '此尺码无效，请核对！可用的尺码有：' ;
$_LANG['erp_order_goods_without_size'] = '该产品无尺码！' ;
$_LANG['erp_order_goods_size_cannot_be_deleted'] = '该订单项最少应有一条尺码项，不能删除该尺码！' ;
$_LANG['erp_order_pls_input_goods_sn'] = '请先输入产品货号后再添加产品尺码！' ;
$_LANG['erp_order_supplier_not_exist'] = "该工厂不存在、无效或已被删除,请选择其他工厂！";
$_LANG['erp_order_select_supplier'] = "请选择";
$_LANG['erp_order_goods_without_attr'] = '此产品没有可选属性。';
$_LANG['erp_order_attr_not_select'] = '请选择产品属性。';
$_LANG['erp_order_sure_to_change_order_supplier'] = "您确定要更改供应商吗？";
$_LANG['erp_order_sure_to_change_order_description'] = "您确定要更改订单描述吗？";
$_LANG['erp_order_sure_to_delete_order'] = "您确定要删除此订单吗？";
$_LANG['erp_order_sure_to_change_goods_sn'] = "您确定要更改产品货号吗？";
$_LANG['erp_order_sure_to_delete_goods_size'] = "您确定要删除此尺码吗？";
$_LANG['erp_order_sure_to_change_goods_size'] = "您确定要更改产品尺码？";
$_LANG['erp_order_sure_to_change_order_qty'] = "您确定要更改订单数量吗？";
$_LANG['erp_order_sure_to_delete_order_item'] = "您确定要删除此订单项吗？";
$_LANG['erp_order_sure_to_post_order'] = "您确定要提交此订单给财务吗？";
$_LANG['erp_order_sure_to_withdrawal_to_edit'] = "您确定要退回此订单吗？";
$_LANG['erp_order_sure_to_withdrawal_to_rate'] = "您确定要退回此订单吗？";
$_LANG['erp_order_sure_to_change_specs'] = "您确定要更改订单要求吗？";
$_LANG['erp_order_sure_to_change_price'] = "您确定要更改价格吗？";
$_LANG['erp_order_sure_to_post_to_approve'] = "您确定要提交订单给主管审核吗？";
$_LANG['erp_order_sure_to_approve'] = "您确定要提交审核吗？";
$_LANG['erp_order_no_accessibility']='其他用户正在编辑此订单,不能进行此项操作！';
$_LANG['erp_order_return_to_order_list']='返回到采购订单列表。';
$_LANG['erp_order_not_admin_goods']='您没有此商品的管理权。';
$_LANG['erp_order_not_supplier_goods']='此商品不属于您所选的供应商。';
$_LANG['erp_order_select_goods_in_advance']='请先选择产品。';
$_LANG['erp_order_not_exist']='此订单不存在或已被删除。';
$_LANG['erp_order_item_not_exist']='此订单项不存在或已被删除。';
$_LANG['erp_order_item_attr_not_exist']='此产品属性不存在或已被删除。';
$_LANG['erp_order_goods_not_exist']='此产品不存在或已被删除。';
$_LANG['erp_order_view_order']='查看此订单';
$_LANG['erp_order_delete_sucess']='成功删除订单。';
$_LANG['erp_order_post_success']='成功提交订单。';
$_LANG['erp_order_approve_success']='成功提交审核结果。';
$_LANG['erp_order_post_failed']='提交订单出错。';
$_LANG['erp_order_not_completed']='此订单尚未完善，不能提交。';
$_LANG['erp_order_no_order_item']='此订单无订单项，请完善订单后再提交。';
$_LANG['erp_order_no_supplier']='未选择供应商，不能提交。';
$_LANG['erp_order_no_goods']='最少有一个订单项未输入产品货号，不能提交。';
$_LANG['erp_order_no_goods_attr']='最少有一个订单项尚未选择产品属性，不能提交。';
$_LANG['erp_order_no_goods_qty']='最少有一个订单项尚未输入定购数量，不能提交。';
$_LANG['erp_order_no_img']='无图片';
$_LANG['erp_order_input_goods_sn']='输入货号';
$_LANG['erp_order_specific']='说明及要求';
$_LANG['erp_approve_remark'] = '审核备注:';
$_LANG['erp_order_sum_total']='总计：';
$_LANG['erp_order_qty_unit']='件';
$_LANG['erp_order_amount_unit']='元';

/* 进销存打印采购订单 */
$_LANG['erp_print_order_title']='采购订单';
$_LANG['erp_print_order_sn']='采购订单编号：';
$_LANG['erp_print_create_date']='下单日期：';
$_LANG['erp_print_order_supplier_name']='供应商名称：';
$_LANG['erp_print_order_user1']='制单：';
$_LANG['erp_print_order_user2']='财务：';
$_LANG['erp_print_order_user3']='审核：';
$_LANG['erp_print_order_user4']='供应商签字：';

/* 进销存仓库设置 */
$_LANG['erp_add_warehouse']='添加仓库';
$_LANG['erp_warehouse_list']='仓库列表';
$_LANG['erp_warehouse_id'] = '序号';
$_LANG['erp_warehouse_name'] = '名称';
$_LANG['erp_warehouse_description'] = '描述';
$_LANG['erp_warehouse_administer'] = '仓管';
$_LANG['erp_warehouse_has_warehousing'] ='此仓库有相关联的入库单，不能删除。';
$_LANG['erp_warehouse_has_delivery'] ='此仓库有相关联的出库单，不能删除。';
$_LANG['erp_warehousing_style_has_warehousing'] ='此入库类型有相关联的入库单，不能删除。';
$_LANG['erp_delivery_style_has_delivery'] ='此出库类型有相关联的出库单，不能删除。';
$_LANG['erp_warehouse_not_exist'] ='仓库不存在或已被删除！';
$_LANG['erp_warehouse_sure_to_change_description'] = "您确定要更改仓库描述吗？";
$_LANG['erp_warehouse_description_required'] = "仓库描述不得留空，请重新输入！";
$_LANG['erp_warehouse_administer_required'] ='仓管不得留空，请重新输入！';
$_LANG['erp_warehouse_name_required'] ='仓管名不得留空，请重新输入！';
$_LANG['erp_warehouse_sure_to_change_administer'] = "您确定要更改仓库管理人吗？";
$_LANG['erp_add_warehousing_style']='添加入库类型';
$_LANG['erp_warehousing_style_id'] = '序号';
$_LANG['erp_warehousing_style'] = '入库类型';
$_LANG['erp_warehousing_style_not_exist'] ='入库类型不存在或已被删除！';
$_LANG['erp_warehousing_style_required'] ='入库类型不得留空，请重新输入！';
$_LANG['erp_add_delivery_style']='添加出库类型';
$_LANG['erp_delivery_style_id'] = '序号';
$_LANG['erp_delivery_style'] = '出库类型';
$_LANG['erp_delivery_style_not_exist'] ='出库类型不存在或已被删除！';
$_LANG['erp_delivery_style_required'] ='出库类型不得留空，请重新输入！';
$_LANG['erp_delivery_without_valid_warehouse'] ='管理员尚未给您分配仓库，不能新建出库单。';

/* 进销存入库单 */
$_LANG['erp_warehousing_id'] = '序号';
$_LANG['erp_warehousing_sn'] ='入库单号';
$_LANG['erp_warehousing_warehouse'] ='收货仓库';
$_LANG['erp_warehousing_warehousing_style'] ='入库类型';
$_LANG['erp_warehousing_order'] ='关联订单号';
$_LANG['erp_warehousing_date'] ='入库日期';
$_LANG['erp_warehousing_from'] ='交货人';
$_LANG['erp_warehousing_admin'] ='仓管';
$_LANG['erp_warehousing_status'] ='入库单状态';
$_LANG['erp_warehousing_start_date'] ='起始日期';
$_LANG['erp_warehousing_end_date'] ='结束日期';
$_LANG['erp_warehousing_status_inputing'] = '入库单录入中';//order_status=1
$_LANG['erp_warehousing_status_rating'] = '财务处理中';//order_status=2
$_LANG['erp_warehousing_status_approving'] = '主管审核中';//order_status=3
$_LANG['erp_warehousing_status_approve_pass'] = '审核已通过';//order_status=4
$_LANG['erp_warehousing_status_approve_reject'] = '审核未通过';//order_status=5
$_LANG['erp_warehousing_operation_approve_remark'] = '审核备注';
$_LANG['erp_warehousing_item_id'] = '序号';
$_LANG['erp_warehousing_item_goods_sn'] ='产品货号';
$_LANG['erp_warehousing_item_goods_img'] ='产品图片';
$_LANG['erp_warehousing_item_goods_attr'] ='产品属性';
$_LANG['erp_warehousing_item_expected_qty'] ='应收数量';
$_LANG['erp_warehousing_item_received_qty'] ='实收数量';
$_LANG['erp_warehousing_item_remark'] ='备注';
$_LANG['erp_warehousing_item_goods_barcode'] ='条码';
$_LANG['erp_warehousing_item'] ='入库单项';
$_LANG['erp_warehousing_select_warehouse'] ='请选择仓库';
$_LANG['erp_warehousing_item_not_exist'] ='入库单项不存在或已被删除！';
$_LANG['erp_warehousing_choose_order'] ='选择订单';
$_LANG['erp_warehousing_not_exist'] ='入库单不存在或已被删除！';
$_LANG['erp_warehousing_no_accessibility']='其他用户正在编辑此入库单,不能进行此项操作！';
$_LANG['erp_warehousing_return_to_warehousing_list']='返回到入库单列表';
$_LANG['erp_warehousing_view_warehousing']='查看入库单';
$_LANG['erp_warehousing_edit_warehousing']='编辑入库单';
$_LANG['erp_warehousing_approve_warehousing']='审核入库单';
$_LANG['erp_warehousing_sure_to_withdrawal_to_edit'] = "您确定要退回此入库单吗？";
$_LANG['erp_warehousing_sure_to_change_warehouse'] ='您确定要更改收货仓库吗？';
$_LANG['erp_warehousing_sure_to_change_warehousing_style'] ='您确定要更改入库类型吗？';
$_LANG['erp_warehousing_sure_to_change_order'] ='您确定要更改关联订单吗？';
$_LANG['erp_warehousing_sure_to_change_warehousing_from'] ='您确定要更改交货人吗？';
$_LANG['erp_warehousing_sure_to_post_warehousing'] ='您确定要提交入库单给主管审核吗？';
$_LANG['erp_warehousing_sure_to_post_approve'] ='您确定要提交审核吗？';
$_LANG['erp_warehousing_sure_to_change_received_qty'] ='您确定要更改实收数量吗？';
$_LANG['erp_warehousing_sure_to_change_price'] ='您确定要更改入库价格吗？';
$_LANG['erp_warehousing_sure_to_delete_warehousing_item'] ='您确定要删除此入库单项吗？';
$_LANG['erp_warehousing_sure_to_delete_warehousing'] ='您确定要删除此入库单吗？';
$_LANG['erp_warehousing_warehousing_from_required'] ='请填写交货人';
$_LANG['erp_warehousing_input_barcode'] ='条码：';
$_LANG['erp_warehousing_input_qty'] ='数量：';
$_LANG['erp_warehousing_input_warehousing_from'] ='输入交货人';
$_LANG['erp_warehousing_import_warehousing'] ='导入数据：';
$_LANG['erp_warehousing_input_goods_sn'] ='货号：';
$_LANG['erp_warehousing_goods_sn_required'] ='请输入货号。';
$_LANG['erp_warehousing_wrong_goods_sn'] ='请核对输入的产品货号。';
$_LANG['erp_warehousing_goods_size_required'] ='请输入尺码。';
$_LANG['erp_warehousing_size_not_valid'] = '此尺码无效，请核对！' ;
$_LANG['erp_warehousing_item_warehousing_price'] ='入库价格';
$_LANG['erp_warehousing_without_choosing_order'] ='请选择关联订单号！';
$_LANG['erp_warehousing_without_warehousing_from'] ='请输入交货人！';
$_LANG['erp_warehousing_without_warehousing_item'] ='此入库单无入库单项，不能提交！';
$_LANG['erp_warehousing_without_warehousing_qty'] ='最少有一条入库单项未输入实收数，不能提交！';
$_LANG['erp_warehousing_only_text_file'] ='文件类型错误，正确的文件应当是以"txt"为扩展名的文本文件！';
$_LANG['erp_warehousing_return'] ='返回到入库单编辑页面';
$_LANG['erp_warehousing_invalid_file'] ='您上传的文件格式不正确！';
$_LANG['erp_warehousing_batch_warehousing_success'] ='成功导入入库数据！';
$_LANG['erp_warehousing_invalid_goods_sn'] ='导入的数据含无效的货号！';
$_LANG['erp_warehousing_invalid_goods_size'] ='导入的数据含无效的产品尺码！';
$_LANG['erp_warehousing_without_valid_warehouse'] ='管理员尚未给您分配仓库，不能新建入库单。';

/* 进销存打印入库单 */
$_LANG['erp_print_warehousing_title']='入库单';
$_LANG['erp_print_warehousing_user1']='制单：';
$_LANG['erp_print_warehousing_user2']='仓管：';
$_LANG['erp_print_warehousing_user3']='交货人：';
$_LANG['erp_print_warehousing_user4']='审核：';

/* 进销存产品库存列表 */
$_LANG['erp_goods_list_goods_id'] = '序号';
$_LANG['erp_goods_list_goods_img'] ='产品图片';
$_LANG['erp_goods_list_goods_sn'] ='产品货号';
$_LANG['erp_goods_list_goods_provider_sn'] ='供应商货号';
$_LANG['erp_goods_list_goods_name'] ='产品名称';
$_LANG['erp_goods_list_goods_number'] = "产品库存";
$_LANG['erp_goods_list_goods_warn_number'] = "警告库存";
$_LANG['erp_goods_list_goods_goods_attr'] ='产品属性';
$_LANG['erp_goods_list_set_goods_warn_number'] = "设置警告库存";
$_LANG['erp_goods_list_search_cat'] ='产品类别';
$_LANG['erp_goods_list_search_goods_sn'] ='产品货号';
$_LANG['erp_goods_list_search_provider_goods_sn'] ='供应商货号';
$_LANG['erp_goods_list_sure_to_change_warn_number'] = "您确定要修改警告库存吗？";

/* 进销存出仓单 */
$_LANG['erp_delivery_id'] = '序号';
$_LANG['erp_delivery_sn'] ='出库单号';
$_LANG['erp_delivery_warehouse'] ='发货仓库';
$_LANG['erp_delivery_delivery_style'] ='出库类型';
$_LANG['erp_delivery_date'] ='出库日期';
$_LANG['erp_delivery_to'] ='提货人';
$_LANG['erp_delivery_admin'] ='仓管';
$_LANG['erp_delivery_status'] ='出库单状态';
$_LANG['erp_delivery_order'] ='关联订单号';
$_LANG['erp_delivery_start_date'] ='起始日期';
$_LANG['erp_delivery_end_date'] ='结束日期';
$_LANG['erp_delivery_status_inputing'] = '出库单录入中';//order_status=1
$_LANG['erp_delivery_status_rating'] = '财务处理中';//order_status=2
$_LANG['erp_delivery_status_approving'] = '主管审核中';//order_status=3
$_LANG['erp_delivery_status_approve_pass'] = '审核已通过';//order_status=4
$_LANG['erp_delivery_status_approve_reject'] = '审核未通过';//order_status=5
$_LANG['erp_delivery_operation_approve_remark'] = '审核备注';
$_LANG['erp_delivery_item_id'] = '序号';
$_LANG['erp_delivery_item_goods_sn'] ='产品货号';
$_LANG['erp_delivery_item_goods_img'] ='产品图片';
$_LANG['erp_delivery_item_goods_attr'] ='产品属性';
$_LANG['erp_delivery_item_expected_qty'] ='应发数量';
$_LANG['erp_delivery_item_delivered_qty'] ='实发数量';
$_LANG['erp_delivery_item_remark'] ='备注';
$_LANG['erp_delivery_item_goods_barcode'] ='条码';
$_LANG['erp_delivery_item_not_exist'] ='出库单项不存在或已被删除！';
$_LANG['erp_delivery_choose_order'] ='选择订单';
$_LANG['erp_delivery_select_warehouse'] ='请选择仓库';
$_LANG['erp_delivery_stock_not_enough'] = "该产品此属性库存不足（库存数%s）。";
$_LANG['erp_delivery_not_exist'] ='出库单不存在或已被删除！';
$_LANG['erp_delivery_no_accessibility']='其他用户正在编辑此出库单,不能进行此项操作！';
$_LANG['erp_delivery_return_to_delivery_list']='返回到出库单列表';
$_LANG['erp_delivery_view_delivery']='查看此出库单';
$_LANG['erp_delivery_sure_to_change_warehouse'] ='您确定要更改发货仓库吗？';
$_LANG['erp_delivery_sure_to_change_delivery_style'] ='您确定要更改出库类型吗？';
$_LANG['erp_delivery_sure_to_change_order'] ='您确定要更改关联订单吗？';
$_LANG['erp_delivery_sure_to_change_delivery_to'] ='您确定要更改提货人吗？';
//$_LANG['erp_delivery_sure_to_post_delivery'] ='您确定要提交出库单给财务吗？';
$_LANG['erp_delivery_sure_to_post_delivery'] ='提交后不能再次更改出库单，您确定要提交吗？';
$_LANG['erp_delivery_sure_to_submit_delivery'] ='您确定要提交出库单给主管审核吗？';
$_LANG['erp_delivery_sure_to_post_approve'] ='您确定要提交审核吗？';
$_LANG['erp_delivery_sure_to_change_delivered_qty'] ='您确定要更改实发数量吗？';
$_LANG['erp_delivery_sure_to_change_price'] ='您确定要更改出库价格吗？';
$_LANG['erp_delivery_sure_to_delete_delivery_item'] ='您确定要删除此出库单项吗？';
$_LANG['erp_delivery_sure_to_delete_delivery'] ='您确定要删除此出库单吗？';
$_LANG['erp_delivery_delivery_to_required'] ='请填写提货人';
$_LANG['erp_delivery_input_barcode'] ='条码：';
$_LANG['erp_delivery_input_qty'] ='数量：';
$_LANG['erp_delivery_input_delivery_to'] ='输入提货人';
$_LANG['erp_delivery_import_delivery'] ='导入数据：';
$_LANG['erp_delivery_input_goods_sn'] ='货号：';
$_LANG['erp_delivery_input_goods_size'] ='尺码：';
$_LANG['erp_delivery_goods_sn_required'] ='请输入货号。';
$_LANG['erp_delivery_wrong_goods_sn'] ='请核对输入的产品货号。';
$_LANG['erp_delivery_goods_size_required'] ='请输入尺码。';
$_LANG['erp_delivery_size_not_valid'] = '此尺码无效，请核对！' ;
$_LANG['erp_delivery_item_delivery_price'] ='出库价格';
$_LANG['erp_delivery_without_choosing_order'] ='请选择关联订单号！';
$_LANG['erp_delivery_without_delivery_to'] ='请输入提货人！';
$_LANG['erp_delivery_without_delivery_item'] ='此出库单无出库单项，不能提交！';
$_LANG['erp_delivery_without_delivery_qty'] ='最少有一条出库单项未输入实发数量，不能提交！';
$_LANG['erp_delivery_only_text_file'] ='文件类型错误，正确的文件应当是以"txt"为扩展名的文本文件！';
$_LANG['erp_delivery_return'] ='返回到出库单编辑页面';
$_LANG['erp_delivery_invalid_file'] ='您上传的文件格式不正确！';
$_LANG['erp_delivery_batch_delivery_success'] ='成功导入出库数据！';
$_LANG['erp_delivery_invalid_goods_sn'] ='导入的数据含无效的货号！';
$_LANG['erp_delivery_invalid_goods_size'] ='导入的数据含无效的产品尺码！';
$_LANG['erp_delivery_no_stock'] = "至少有一个订单项因库存不足，没有导入，请检查库存。";

/* 进销存打印出库单 */
$_LANG['erp_print_delivery_title']='出库单';
$_LANG['erp_print_delivery_user1']='制单：';
$_LANG['erp_print_delivery_user2']='仓管：';
$_LANG['erp_print_delivery_user3']='提货人：';
$_LANG['erp_print_delivery_user4']='审核：';

/* 进销存产品库存列表 */
$_LANG['erp_stock_goods_id'] = '序号';
$_LANG['erp_stock_goods_img'] ='产品图片';
$_LANG['erp_stock_goods_sn'] ='产品货号';
$_LANG['erp_stock_goods_provider_sn'] ='供应商货号';
$_LANG['erp_stock_goods_name'] ='产品名称';
$_LANG['erp_stock_warehouse_name'] ='仓库名称';
$_LANG['erp_stock_goods_number'] = "产品库存";
$_LANG['erp_stock_goods_goods_size'] ='尺码';
$_LANG['erp_stock_warehouse_attr_qty'] = "仓库-属性-库存";
$_LANG['erp_stock_search_warehouse'] ='请选择仓库';
$_LANG['erp_stock_search_goods_sn'] ='产品货号';
$_LANG['erp_stock_search_provider_goods_sn'] ='供应商货号';

/* 进销存产品库存盘点表 */
$_LANG['erp_stock_check_id'] = '序号';
$_LANG['erp_stock_check_goods_img'] ='产品图片';
$_LANG['erp_stock_check_goods_sn'] ='产品货号';
$_LANG['erp_stock_check_goods_provider_sn'] ='供应商货号';
$_LANG['erp_stock_check_goods_attr'] ='产品属性';
$_LANG['erp_stock_check_warehouse_name'] ='仓库';
$_LANG['erp_stock_check_begin_qty'] = "期初库存";
$_LANG['erp_stock_check_end_qty'] = "期末库存";
$_LANG['erp_stock_check_select_warehouse'] ='请选择仓库';
$_LANG['erp_stock_check_warehousing_qty'] ='入库数量';
$_LANG['erp_stock_check_delivery_qty'] ='出库数量';
$_LANG['erp_stock_check_search_start_date'] ='起始日期';
$_LANG['erp_stock_check_search_end_date'] ='结束日期';
$_LANG['erp_stock_check_print'] = "打印库存盘点表";

/* 进销存产品出入库记录(序时薄) */
$_LANG['erp_stock_record_id'] = '序号';
$_LANG['erp_stock_record_goods_img'] ='产品图片';
$_LANG['erp_stock_record_goods_sn'] ='产品货号';
$_LANG['erp_stock_record_goods_name'] ='产品名称';
$_LANG['erp_stock_record_goods_attr'] ='产品属性';
$_LANG['erp_stock_record_warehouse_name'] ='仓库名称';
$_LANG['erp_stock_record_w_d_style'] ='出入库类型';
$_LANG['erp_stock_record_w_d_sn'] ='出入库单号';
$_LANG['erp_stock_record_order_sn'] ='关联订单号';
$_LANG['erp_stock_record_w_d_qty'] ='出入库数';
$_LANG['erp_stock_record_w_d_name'] ='提/交货人';
$_LANG['erp_stock_record_goods_price'] ='产品价格';
$_LANG['erp_stock_record_goods_amount'] ='产品金额';
$_LANG['erp_stock_record_date'] ='出入库日期';
$_LANG['erp_stock_record_search_stock_style_both'] ='出库和入库';
$_LANG['erp_stock_record_search_stock_style_only_warehousing'] ='入库';
$_LANG['erp_stock_record_search_stock_style_only_delivery'] ='出库';
$_LANG['erp_stock_record_search_warehouse'] ='请选择仓库';
$_LANG['erp_stock_record_search_w_d_style'] ='请选择出入库类型';
$_LANG['erp_stock_record_search_goods_sn'] ='产品货号';
$_LANG['erp_stock_record_search_provider_goods_sn'] ='供应商货号';
$_LANG['erp_stock_record_search_start_date'] ='起始日期';
$_LANG['erp_stock_record_search_end_date'] ='结束日期';
$_LANG['erp_stock_record_search_goods_sn'] ='产品货号';

/* 进销存应付款列表 应付款详情 */
$_LANG['erp_payable_order_id'] = '序号';
$_LANG['erp_payable_order_sn'] = '编号';
$_LANG['erp_payable_order_description'] = '描述';
$_LANG['erp_payable_order_supplier'] = '供应商';
$_LANG['erp_payable_order_date'] = '订单日期';
$_LANG['erp_payable_order_amount'] = '订单金额';
$_LANG['erp_payable_total_payable_amount'] = '应付款';
$_LANG['erp_payable_total_paid_amount'] = '已付款';
$_LANG['erp_payable_payable'] = '剩余款';
$_LANG['erp_payable_pay'] = '付款金额';
$_LANG['erp_payable_operation_pay'] = '付款';
$_LANG['erp_payable_search_all_orders'] = '所有订单';
$_LANG['erp_payable_search_payable_orders'] = '应付款订单';
$_LANG['erp_payable_search_order_sn'] = '订单编号';
$_LANG['erp_payable_no_valid_bank_account'] = '尚未设置有效的银行帐户，不能建立付款单。';
$_LANG['erp_payable_more_than_payable'] = '付款金额不能大于应付款！';
$_LANG['erp_payable_details'] = '应付款详情';
$_LANG['erp_payable_sure_to_withdrawal_to_edit'] = "您确定要退回此付款单吗？";

/* 进销存应收款列表 应收款详情 */
$_LANG['erp_receivable_order_id'] = '序号';
$_LANG['erp_receivable_order_sn'] = '编号';
$_LANG['erp_receivable_order_description'] = '描述';
$_LANG['erp_receivable_order_username'] = '用户名';
$_LANG['erp_receivable_order_consignee'] = '收货人';
$_LANG['erp_receivable_order_shipping_date'] = '发货时间';
$_LANG['erp_receivable_order_amount'] = '订单金额';
$_LANG['erp_receivable_total_receivable_amount'] = '应收款';
$_LANG['erp_receivable_total_gathered_amount'] = '已收款';
$_LANG['erp_receivable_receivable'] = '剩余款';
$_LANG['erp_receivable_gather'] = '付款金额';
$_LANG['erp_receivable_goods_id'] = '序号';
$_LANG['erp_receivable_goods_sn'] = '产品货号';
$_LANG['erp_receivable_goods_img'] = '产品图片';
$_LANG['erp_receivable_goods_attr'] = '产品属性';
$_LANG['erp_receivable_goods_price'] = '产品价格';
$_LANG['erp_receivable_goods_order_qty'] = '购买数量';
$_LANG['erp_receivable_goods_delivered_qty'] = '已发货数量';
$_LANG['erp_receivable_goods_amount'] = '总价';
$_LANG['erp_receivable_operation_gather'] = '收款';
$_LANG['erp_receivable_search_all_orders'] = '所有订单';
$_LANG['erp_receivable_search_receivable_orders'] = '应收款订单';
$_LANG['erp_receivable_search_order_sn'] = '订单编号';
$_LANG['erp_receivable_more_than_receivable'] = '收款金额不能大于应收款！';
$_LANG['erp_receivable_details'] = '应收款详情';
$_LANG['erp_receivable_no_valid_bank_account'] = '尚未设置有效的银行帐户，不能建立收款单。';
$_LANG['erp_receivable_sure_to_withdrawal_to_edit'] = "您确定要退回此收款单吗？";

/* 进销存银行帐号 */
$_LANG['erp_bank_account_id'] = '序号';
$_LANG['erp_bank_account_name'] = '帐户名称';
$_LANG['erp_bank_account_no'] = '帐号';
$_LANG['erp_bank_account_balance'] = '帐户余额';
$_LANG['erp_bank_account_not_exists'] = '该银行帐号不存在或已被删除！';
$_LANG['erp_bank_account_details_id'] = '序号';
$_LANG['erp_bank_account_details_income_expenses'] = '收入或支出';
$_LANG['erp_bank_account_details_payment_gathering'] = '关联收付款单号';
$_LANG['erp_bank_account_details_order'] = '关联订单号';
$_LANG['erp_bank_account_details_date'] = '收支日期';
$_LANG['erp_bank_account_details_remark'] = '备注';
$_LANG['erp_bank_account_details_start_date'] = ' 起始日期';
$_LANG['erp_bank_account_details_end_date'] = ' 结束日期';
$_LANG['erp_bank_account_add'] = '添加银行帐号';
$_LANG['erp_bank_account_name_required'] = '请输入银行帐户名称。';
$_LANG['erp_bank_account_no_required'] = '请输入银行帐号。';
$_LANG['erp_bank_account_has_gathering'] = '此银行帐号有相关联的收款单，不能删除。';
$_LANG['erp_bank_account_has_payment'] = '此银行帐号有相关联的付款单，不能删除。';
$_LANG['erp_bank_account_has_bank_record'] = '此银行帐号有收付款记录，不能删除';
$_LANG['erp_bank_account_details'] = '银行帐户收支明细';

/* 进销存打印银行收支清单 */
$_LANG['erp_print_bank_account_details']='银行帐户收支清单';

/* 进销存付款类型 */
$_LANG['erp_payment_style_id'] = '序号';
$_LANG['erp_payment_style'] = '付款类型';
$_LANG['erp_payment_style_not_exists'] = '该付款类型不存在或已被删除！';

/* 进销存付款单 */
$_LANG['erp_add_payment'] = '添加付款单';
$_LANG['erp_payment_id'] = '序号';
$_LANG['erp_payment_sn'] = '付款单编号';
$_LANG['erp_payment_date'] = '付款日期';
$_LANG['erp_payment_pay_to'] = '收款人';
$_LANG['erp_payment_remark'] = '付款说明';
$_LANG['erp_payment_order_id'] = '订单号';
$_LANG['erp_payment_order_date'] = '订单日期';
$_LANG['erp_payment_order_amount'] = '订单总金额';
$_LANG['erp_payment_order_total_payable'] = '订单总应付款';
$_LANG['erp_payment_order_paid_payable'] = '订单已付应付款';
$_LANG['erp_payment_order_payable_balance'] = '订单剩余应付款';
$_LANG['erp_payment_pay_amount'] = '本次付款金额';
$_LANG['erp_payment_account'] = '付款帐号';
$_LANG['erp_payment_status'] = '付款单状态';
$_LANG['erp_payment_operation_view_approve_remark'] = '审核备注';
$_LANG['erp_payment_status_inputing'] = '付款单录入中';//payment_status=1
$_LANG['erp_payment_status_approving'] = '等待主管审核';//payment_status=2
$_LANG['erp_payment_status_approve_pass'] = '审核已通过';//payment_status=3
$_LANG['erp_payment_status_approve_reject'] = '审核未通过';//payment_status=4
$_LANG['erp_payment_search_payment_status'] = '付款单状态';
$_LANG['erp_payment_search_pay_to'] = '收款人';
$_LANG['erp_payment_search_start_date'] = '起始日期';
$_LANG['erp_payment_search_end_date'] = '结束日期';
$_LANG['erp_payment_search_payment_sn'] = '付款单编号';
$_LANG['erp_payment_no_pay'] = '您还没有输入付款金额。';
$_LANG['erp_payment_return_to_payment_list']='返回到付款单列表';
$_LANG['erp_payment_view_payment']='查看此付款单';
$_LANG['erp_payment_sure_to_post'] = '您确定要付款吗？';
$_LANG['erp_payment_sure_to_pay'] = "您确定要付款，并生成一个付款单吗？";
$_LANG['erp_payment_sure_to_delete_payment']='您确定要删除付款单吗？';
$_LANG['erp_payment_sure_to_post_payment']='您确定要提交付款单吗？';
$_LANG['erp_payment_sure_to_change_bank_account']='您确定要更改付款银行帐号吗？';
$_LANG['erp_payment_sure_to_change_pay_amount']='您确定要更改付款金额吗？';
$_LANG['erp_payment_sure_to_approve'] = "您确定要提交审核吗？";
$_LANG['erp_payment_sure_to_change_order'] = '您确定要更改订单吗？';
$_LANG['erp_payment_greater_than_payable']='付款金额不能大于订单剩余应付款。';
$_LANG['erp_payment_not_exist']='此付款单不存在或已被删除！';
$_LANG['erp_payment_delete_sucess']='成功删除付款单';
$_LANG['erp_payment_no_accessibility']='其他用户正在编辑此付款单,不能进行此项操作！';
$_LANG['erp_payment_post_success']='成功提交付款单。';
$_LANG['erp_payment_approve_success']='成功提交审核。';

/* 进销存收款单 */
$_LANG['erp_add_gathering'] = '添加收款单';
$_LANG['erp_gathering_anonymous'] = '匿名用户';
$_LANG['erp_gathering_id'] = '序号';
$_LANG['erp_gathering_sn'] = '收款单编号';
$_LANG['erp_gathering_date'] = '收款日期';
$_LANG['erp_gathering_pay_from'] = '付款人';
$_LANG['erp_gathering_remark'] = '收款说明';
$_LANG['erp_gathering_order_id'] = '订单号';
$_LANG['erp_gathering_order_date'] = '订单日期';
$_LANG['erp_gathering_order_amount'] = '订单总金额';
$_LANG['erp_gathering_order_total_receivable'] = '订单总应收款';
$_LANG['erp_gathering_order_paid_receivable'] = '订单已付应收款';
$_LANG['erp_gathering_order_receivable_balance'] = '订单剩余应收款';
$_LANG['erp_gathering_gather_amount'] = '收款金额';
$_LANG['erp_gathering_account'] = '收款帐号';
$_LANG['erp_gathering_status'] = '收款单状态';
$_LANG['erp_gathering_operation_view_approve_remark'] = '审核备注';
$_LANG['erp_gathering_status_inputing'] = '收款单录入中';//gathering_status=1
$_LANG['erp_gathering_status_approving'] = '等待主管审核';//gathering_status=2
$_LANG['erp_gathering_status_approve_pass'] = '审核已通过';//gathering_status=3
$_LANG['erp_gathering_status_approve_reject'] = '审核未通过';//gathering_status=4
$_LANG['erp_gathering_search_gathering_status'] = '收款单状态';
$_LANG['erp_gathering_search_pay_from'] = '付款人';
$_LANG['erp_gathering_search_start_date'] = '起始日期';
$_LANG['erp_gathering_search_end_date'] = '结束日期';
$_LANG['erp_gathering_search_gathering_sn'] = '收款单编号';
$_LANG['erp_gathering_no_gather'] = '您还没有输入收款金额。';
$_LANG['erp_gathering_return_to_gathering_list']='返回到收款单列表。';
$_LANG['erp_gathering_view_gathering']='查看此收款单。';
$_LANG['erp_gathering_sure_to_change_order'] = '您确定要更改订单吗？';
$_LANG['erp_gathering_sure_to_post'] = '您确定要收款吗？';
$_LANG['erp_gathering_sure_to_gather'] = "您确定要收款，并生成一个收款单吗？";
$_LANG['erp_gathering_sure_to_delete_gathering']='您确定要删除收款单吗？';
$_LANG['erp_gathering_sure_to_post_gathering']='您确定要提交收款单吗？';
$_LANG['erp_gathering_sure_to_change_bank_account']='您确定要更改收款银行帐号吗？';
$_LANG['erp_gathering_sure_to_change_gather_amount']='您确定要更改收款金额吗？';
$_LANG['erp_gathering_sure_to_approve'] = "您确定要提交审核吗？";
$_LANG['erp_gathering_greater_than_receivable']='收款金额不能大于订单剩余应收款。';
$_LANG['erp_gathering_not_exist']='此收款单不存在或已被删除！';
$_LANG['erp_gathering_delete_sucess']='成功删除收款单。';
$_LANG['erp_gathering_no_accessibility']='其他用户正在编辑此收款单,不能进行此项操作！';
$_LANG['erp_gathering_post_success']='成功提交收款单。';
$_LANG['erp_gathering_approve_success']='成功提交审核。';
?>