<?php

global $_LANG;

$_LANG['products']            = '產品處理';
$_LANG['products_desc']       = '定時處理產品相關資料';
$_LANG['products_is_new_month']   = '保留新品標籤時限';
$_LANG['products_is_new_month_range'][1] = '1個月';
$_LANG['products_is_new_month_range'][2] = '2個月';
$_LANG['products_is_new_month_range'][3] = '3個月';
$_LANG['products_is_new_month_range'][6] = '6個月';
?>