<?php

global $_LANG;

$_LANG['payme'] = "Payme";
$_LANG['payme_desc'] = 'PayMe是由汇丰银行推出的电子钱包服务, 容许不同银行用户间自由转帐';
$_LANG['payme_client_id'] = '用户识别ID (Client ID)';
$_LANG['payme_client_secret'] = '密钥(Secret Key)';
$_LANG['payme_signing_key_id'] = '签名密钥ID(Signing Key ID)';
$_LANG['payme_signing_key'] = '签名密钥(Signing Key)';
$_LANG['payme_api_base_url'] = 'API 网址';
$_LANG['payme_api_version'] = 'API 版本';

// Payment Page Message
$_LANG['payme_scan_paycode_with_payme'] = "使用PayMe扫描以下PayCode";
$_LANG['payme_donot_close_this_page_until_payment_complete'] = "交易完成前切勿离开本页";
$_LANG['payme_payment_instruction_title'] = "使用PayMe付款";
$_LANG['payme_payment_instruction_open_app'] = "打开PayMe应用程式";
$_LANG['payme_payment_instruction_scan_to_authorize'] = "扫描PayCode以授权付款";
$_LANG['payme_payment_instruction_wait_for_confirm'] = "在应用程式内完成付款，并在本页等待确认";
$_LANG['payme_complete_payment_before_please'] = "请在";
$_LANG['payme_complete_payment_before'] = "前完成付款";

// PayMe App Instruction
$_LANG['payme_payment_instruction_use_app_title'] = "使用PayCode扫描器";
$_LANG['payme_payment_instruction_use_app_instruction'] = "在PayMe首页，你可以...";
$_LANG['payme_payment_instruction_use_app_tap_icon_and_select_scanner'] = "轻触PayCode图示并选择扫描器";
$_LANG['payme_payment_instruction_use_app_or'] = "或";
$_LANG['payme_payment_instruction_use_app_swipe_left'] = "向左扫以启动扫描器";

// Payme Mobile Web Instruction
$_LANG['payme_payment_instruction_mobile'] = "请按以下\"Pay with PayMe\" 按钮前往PayMe应用程式付款。";

// Payme Error
$_LANG['payme_error_unexpected_api_gateway_error'] = "无法连接PayMe服务,请改用其他付款方式或稍后再试<br/>如情况持续,请联络我们";
$_LANG['payme_error_unexpected_not_authenticated'] = "PayMe服务出现错误,请改用其他付款方式或稍后再试<br/>如情况持续,请联络我们";
$_LANG['payme_error_unexpected_not_validated'] = "PayMe服务出现错误,请改用其他付款方式或稍后再试<br/>如情况持续,请联络我们";
$_LANG['payme_error_unexpected_execution_error'] = "PayMe服务出现错误, 请尽快联络我们";
$_LANG['payme_error_unexpected_timeout'] = "回应超时, 请重新载入本网页或更改付款方法";
$_LANG['payme_error_unexpected_header_error'] = "PayMe服务出现错误, 请改用其他付款方式或稍后再试<br/>如情况持续,请联络我们";
$_LANG['payme_error_unexpected_too_many_request'] = "PayMe服务现正繁忙, 请稍后再试";
$_LANG['payme_error_unexpected_validation_error'] = "PayMe服务出现错误,请改用其他付款方式或稍后再试<br/>如情况持续,请联络我们";
$_LANG['payme_error_unexpected_payme_related_error'] = "PayMe服务出现错误,请改用其他付款方式或稍后再试<br/>如情况持续,请联络我们";
$_LANG['payme_error_unexpected_internal_server_error'] = "PayMe服务出现错误,请改用其他付款方式或稍后再试<br/>如情况持续,请联络我们";
$_LANG['payme_error_unexpected_paycode_not_found_error'] = "找不到/ 无法生成PayCode,请改用其他付款方式或稍后再试<br/>如情况持续,请联络我们";
$_LANG['payme_error_unexpected_paycode_expired'] = "PayCode 已过期,请改用其他付款方式或稍后再试";
$_LANG['payme_error_paycode_expired_unconfirmed_payment'] = "交易状态无法确认,<br/>如已付款请立刻联络我们";

$_LANG['payme_payment_successful_waiting_redirect'] = "正在确认交易状态, 请稍等";

?>