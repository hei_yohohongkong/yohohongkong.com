<?php

//zh_cn
global $_LANG;

$_LANG['stripeapplepay']   =   'Apple Pay (Stripe)';
$_LANG['stripeapplepay_desc'] = '' ;
$_LANG['stripe_pay_now_message'] = '请按下列Apple Pay 按钮启动付款流程';

$_LANG['stripeapplepay_unable_to_make_payment'] = '无法以Apple Pay支付';
$_LANG['stripeapplepay_logged_in'] = '请确认装置/浏览器是否已登入Apple帐户';
$_LANG['stripeapplepay_configured_payment'] = '请确认Apple Pay 帐户内是否已绑定有效信用卡';
$_LANG['stripeapplepay_supported_browser'] = '请确认装置/浏览器是否为最新版本并支援Apple Pay';
$_LANG['stripeapplepay_browser_privilege'] = "请确认浏览器是否允许存取付款方式";
$_LANG['stripeapplepay_configure_instruction'] = "如需详细设定方式, 请前往:<br/><a href=\\\"https://support.apple.com/zh-cn/HT204506\\\" target='_blank'>设定 Apple Pay</a>";
$_LANG['stripeapplepay_supported_platform_heading'] = "Apple Pay 支援以下平台:";
$_LANG['stripeapplepay_supported_platform'] = [
    "iPhone 上的 Safari",
];
