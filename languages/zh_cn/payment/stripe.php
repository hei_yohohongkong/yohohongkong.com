<?php

//zh_cn
global $_LANG;

$_LANG['stripe']                          = 'Stripe';
$_LANG['stripe_desc']                     = 'Stripe' ;
$_LANG['stripe_secret_key'] 			  = 'Secret key';
$_LANG['stripe_publishable_key'] 		  = 'Publishable key';
$_LANG['card_number'] 			  		  = '信用卡号码';
$_LANG['sumbit'] 			  	  		  = '提交';
$_LANG['stripe_txn_id']                   = 'Stripe 交易编号';
$_LANG['stripe_currency']              	  = '支付货币';
$_LANG['stripe_currency_range']['AUD']    = '澳元';
$_LANG['stripe_currency_range']['CAD']    = '加元';
$_LANG['stripe_currency_range']['EUR']    = '欧元';
$_LANG['stripe_currency_range']['GBP']    = '英镑';
$_LANG['stripe_currency_range']['JPY']    = '日元';
$_LANG['stripe_currency_range']['USD']    = '美元';
$_LANG['stripe_currency_range']['HKD']    = '港元';


?>