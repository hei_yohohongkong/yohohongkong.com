<?php

global $_LANG;

$_LANG['alipayhk'] = '支付宝HK';
$_LANG['alipayhk_desc'] = '国内先进的网上支付平台。(香港钱包)';
$_LANG['alipayhk_merchant_pid'] = '合作者身份(Pid)';
$_LANG['alipayhk_md5_signature_key'] = '安全校验码(Key)';
$_LANG['alipayhk_api_base_gateway'] = 'API 接口';

$_LANG['alipayhk_instruction_use_alipay_scan_qr_code_proceed_payment'] = '使用AlipayHK扫描QR码<br/>以完成支付';
$_LANG['alipayhk_instruction_no_alipayhk_app'] = "沒有AlipayHK App?";
$_LANG['alipayhk_scan_qr_code_to_download'] = "扫描QR码进行下载";
?>
