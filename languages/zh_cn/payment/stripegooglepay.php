<?php

//zh_cn
global $_LANG;

$_LANG['stripegooglepay']   =   'Google Pay (Stripe)';
$_LANG['stripegooglepay_desc'] = '' ;
$_LANG['stripe_pay_now_message'] = '请按下列Google Pay 按钮启动付款流程';

$_LANG['stripegooglepay_unable_to_make_payment'] = '无法以Google Pay支付';
$_LANG['stripegooglepay_logged_in'] = '请确认装置/浏览器是否已登入Google帐户';
$_LANG['stripegooglepay_configured_payment'] = '请确认Google Pay 帐户内是否已绑定有效信用卡';
$_LANG['stripegooglepay_supported_browser'] = '请确认装置/浏览器是否为最新版本并支援Google Pay';
$_LANG['stripegooglepay_browser_privilege'] = '请确认浏览器是否允许存取付款方式';
$_LANG['stripegooglepay_configure_instruction'] = "如需详细设定方式, 请前往:<br/><a href=\\\"https://support.google.com/pay/answer/7625055?hl=zh-Hans\\\" target='_blank'>设定 Google Pay</a>";
$_LANG['stripegooglepay_supported_platform_heading'] = "Google Pay 支援以下平台:";
$_LANG['stripegooglepay_supported_platform'] = [
    "Android手机上的 Google Chrome"
];

// Tooltip on yoho.php
