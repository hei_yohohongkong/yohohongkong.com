<?php

global $_LANG;

$_LANG['alipaycn'] = '支付宝(中国)';
$_LANG['alipaycn_desc'] = '国内先进的网上支付平台。(大陆钱包)';
$_LANG['alipaycn_merchant_pid'] = '合作者身份(Pid)';
$_LANG['alipaycn_md5_signature_key'] = '安全校验码(Key)';
$_LANG['alipaycn_api_base_gateway'] = 'API 接口';

$_LANG['alipaycn_instruction_use_alipay_scan_qr_code_proceed_payment'] = '使用支付宝 App扫描二维码<br/>以完成支付';
$_LANG['alipaycn_instruction_no_alipaycn_app'] = "沒有支付宝 App?";
$_LANG['alipaycn_scan_qr_code_to_download'] = "扫描二维码进行下载";
?>
