<?php
/**
 * ECSHOP 盛付通支付插件 语言文件
 * ----------------------------------------------------------------------------
 * Jacklee的博客 致力于php技术
 * http://www.phpally.com
 * ----------------------------------------------------------------------------
 * @author: Jacklee
 * @email: jack349392900#gmail.com
 * @date: 2012-03-09
 */
 
global $_LANG;
 
$_LANG['sft2']   = '盛付通';
$_LANG['sft2_icbc']   = '工商银行盛付通';
$_LANG['sft2_ccb']   = '建设银行盛付通';
$_LANG['sft2_abc']   = '农业银行';
$_LANG['sft2_bcom']   = '交通银行盛付通';
$_LANG['sft2_cmb']   = '招商银行盛付通';
$_LANG['sft2_cmbc']   = '民生银行盛付通';
$_LANG['sft2_sdb']   = '深发银行盛付通';
$_LANG['sft2_boc']   = '中国银行盛付通';
$_LANG['sft2_bob']   = '北京银行盛付通';
 
$_LANG['sft2_desc']    = '盛付通是国内领先的独立第三方支付企业，由上海盛大提供服务支持。';
$_LANG['sft2_desc_icbc'] = '通过盛付通帐户，用户可以直接用工行网银支付。';
$_LANG['sft2_desc_ccb'] = '通过盛付通帐户，用户可以直接用建设网银支付。';
$_LANG['sft2_desc_abc'] = '您可以通过农业网银直接支付。';
$_LANG['sft2_desc_bcom'] = '通过盛付通帐户，用户可以直接用交通网银支付。';
$_LANG['sft2_desc_cmb'] = '通过盛付通帐户，用户可以直接用招商网银支付。';
$_LANG['sft2_desc_cmbc'] = '通过盛付通帐户，用户可以直接用民生网银支付。';
$_LANG['sft2_desc_sdb'] = '通过盛付通帐户，用户可以直接用深发网银支付。';
$_LANG['sft2_desc_boc'] = '通过盛付通帐户，用户可以直接用中国网银支付。';
$_LANG['sft2_desc_bob'] = '通过盛付通帐户，用户可以直接用北京网银支付。';
 
$_LANG['sft2_account'] = '商户号';
$_LANG['sft2_account_desc'] = '人民币支付网关的收款帐号';
$_LANG['sft2_key']     = '商户密钥';
$_LANG['pay_button2'] = '立即支付';
 
?>