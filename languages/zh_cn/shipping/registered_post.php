<?php

/***
* registered_post.php
* by howang 2015-11-04
*
* Language file for Registered Post shipping module
***/

$_LANG['registered_post']      = 'Registered Post';
$_LANG['registered_post_desc'] = 'Registered Post的描述';
$_LANG['base_fee']             = '100克以內費用：';
$_LANG['step_fee']             = '101克以上續重10克費用：';
$_LANG['pack_fee']             = '包裝費用：';

?>