<?php

/***
* fedex_priority.php
* by howang 2015-11-03
*
* Language file for Fedex Priority shipping module
***/

$_LANG['fedex_priority']      = 'Fedex Priority';
$_LANG['fedex_priority_desc'] = 'Fedex Priority的描述';
$_LANG['base_fee']            = '500克以内费用：';
$_LANG['step_fee']            = '501克以上续重500克费用：';
$_LANG['pack_fee']            = '包装费用：';

?>