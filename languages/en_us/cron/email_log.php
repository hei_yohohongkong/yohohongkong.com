<?php

global $_LANG;

$_LANG['email_log']            = '電郵副本';
$_LANG['email_log_desc']       = '刪除逾時的電郵副本';
$_LANG['email_log_year']   = '刪除幾多年前的紀錄';
$_LANG['email_log_year_range']['1'] = '1年';
$_LANG['email_log_year_range']['2'] = '2年';
$_LANG['email_log_year_range']['3'] = '3年';
?>