<?php

global $_LANG;

$_LANG['expired_po']            = '採購/出入貨訂單';
$_LANG['expired_po_desc']       = '刪除逾時的採購及出入貨訂單';
$_LANG['expired_po_month']   = '刪除幾多個月前的訂單';
$_LANG['expired_po_month_range']['1'] = '1個月';
$_LANG['expired_po_month_range']['3'] = '3個月';
$_LANG['expired_po_month_range']['6'] = '6個月';
$_LANG['expired_po_month_range']['9'] = '9個月';
$_LANG['expired_po_month_range']['12'] = '12個月';
?>