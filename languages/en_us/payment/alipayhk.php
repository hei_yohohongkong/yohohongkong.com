<?php

global $_LANG;

$_LANG['alipayhk'] = 'AlipayHK';
$_LANG['alipayhk_desc'] = 'Alipay payment platform operates inside Hong Kong.';
$_LANG['alipayhk_merchant_pid'] = 'Partner ID (Pid)';
$_LANG['alipayhk_md5_signature_key'] = 'Signature Key';
$_LANG['alipayhk_api_base_gateway'] = 'API Base URL';

$_LANG['alipayhk_instruction_use_alipay_scan_qr_code_proceed_payment'] = 'Scan QR Code thorugh AlipayHK App <br/> to proceed payment';
$_LANG['alipayhk_instruction_no_alipayhk_app'] = "No AlipayHK App?";
$_LANG['alipayhk_scan_qr_code_to_download'] = "Scan QR Code to download";
?>
