<?php

global $_LANG;

$_LANG['alipaycn'] = 'Alipay(China)';
$_LANG['alipaycn_desc'] = 'Alipay payment platform Mainland Wallet.';
$_LANG['alipaycn_merchant_pid'] = 'Partner ID (Pid)';
$_LANG['alipaycn_md5_signature_key'] = 'Signature Key';
$_LANG['alipaycn_api_base_gateway'] = 'API Base URL';


$_LANG['alipaycn_instruction_use_alipay_scan_qr_code_proceed_payment'] = 'Scan QR Code thorugh Alipay App <br/> to proceed payment';
$_LANG['alipaycn_instruction_no_alipaycn_app'] = "No Alipay App?";
$_LANG['alipaycn_scan_qr_code_to_download'] = "Scan QR Code to download";
?>

