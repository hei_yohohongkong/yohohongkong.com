<?php

global $_LANG;

$_LANG['payme'] = "Payme";
$_LANG['payme_desc'] = 'PayMe is a peer-to-peer payments service from HSBC allowing users to make inter-bank payments through it.';
$_LANG['payme_client_id'] = 'Client ID';
$_LANG['payme_client_secret'] = 'Secret Key';
$_LANG['payme_signing_key_id'] = 'Signing Key ID';
$_LANG['payme_signing_key'] = 'Signing Key';
$_LANG['payme_api_base_url'] = 'API Base URL';
$_LANG['payme_api_version'] = 'API Version';

// Payment Page Message
$_LANG['payme_scan_paycode_with_payme'] = "Scan this PayCode with PayMe";
$_LANG['payme_donot_close_this_page_until_payment_complete'] = "Please do not close this page before payment is complete";
$_LANG['payme_payment_instruction_title'] = "Paying with PayMe";
$_LANG['payme_payment_instruction_open_app'] = "Open the PayMe App";
$_LANG['payme_payment_instruction_scan_to_authorize'] = "Scan the PayCode to authorise payment";
$_LANG['payme_payment_instruction_wait_for_confirm'] = "Complete payment in the app and wait for confirmation here";
$_LANG['payme_complete_payment_before_please'] = "Complete payment before ";
$_LANG['payme_complete_payment_before'] = " ";

// PayMe App Instruction
$_LANG['payme_payment_instruction_use_app_title'] = "Using your PayCode scanner";
$_LANG['payme_payment_instruction_use_app_instruction'] = "From the PayMe home screen, either...";
$_LANG['payme_payment_instruction_use_app_tap_icon_and_select_scanner'] = "Tap the PayCode icon and select scanner";
$_LANG['payme_payment_instruction_use_app_or'] = "or";
$_LANG['payme_payment_instruction_use_app_swipe_left'] = "Swipe left to open scanner";

// Payme Mobile Web Instruction
$_LANG['payme_payment_instruction_mobile'] = "You will be redirected to PayMe app once the following \"Pay with PayMe\" button is tapped.";

// Payme Error
$_LANG['payme_error_unexpected_api_gateway_error'] = "Unable to connect to PayMe Service, please change payment method or try again later.<br/>Please contact us if the issue persists";
$_LANG['payme_error_unexpected_not_authenticated'] = "Encountered PayMe Service Error, please change payment method or try again later.<br/>Please contact us if the issue persists";
$_LANG['payme_error_unexpected_not_validated'] = "Encountered PayMe Service Error, please change payment method or try again later.<br/>Please contact us if the issue persists";
$_LANG['payme_error_unexpected_execution_error'] = "Encountered PayMe Service Error, please change payment method or try again later.<br/>Please contact us if the issue persists";
$_LANG['payme_error_unexpected_timeout'] = "Request Timed out, please reload this page or change payment method";
$_LANG['payme_error_unexpected_header_error'] = "Encountered PayMe Service Error, please change payment method or try again later.<br/>Please contact us if the issue persists";
$_LANG['payme_error_unexpected_too_many_request'] = "PayMe Service is busy, please try again later";
$_LANG['payme_error_unexpected_validation_error'] = "Encountered PayMe Service Error, please change payment method or try again later.<br/>Please contact us if the issue persists";
$_LANG['payme_error_unexpected_payme_related_error'] = "Encountered PayMe Service Error, please change payment method or try again later.<br/>Please contact us if the issue persists";
$_LANG['payme_error_unexpected_internal_server_error'] = "Encountered PayMe Service Error, please reload this page later or change payment method, <br/>Please contact us if the issue persists";
$_LANG['payme_error_unexpected_paycode_not_found_error'] = "PayCode Not Found or Not Generated, please reload this page or change payment method, <br/>Please contact us if the issue persists";
$_LANG['payme_error_unexpected_paycode_expired'] = "PayCode has expired, please reload this page or change payment method";
$_LANG['payme_error_paycode_expired_unconfirmed_payment'] = "PayCode has expired but payment status could not be confirmed, please contact us if you have paid";

$_LANG['payme_payment_successful_waiting_redirect'] = "Waiting for payment status, please wait";

?>