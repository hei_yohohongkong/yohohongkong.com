<?php

//en_us
global $_LANG;

$_LANG['stripeapplepay']   =   'Apple Pay (Stripe)';
$_LANG['stripeapplepay_desc'] = '' ;
$_LANG['stripe_pay_now_message'] = 'Please press the following Apple Pay button to begin payment process';

$_LANG['stripeapplepay_unable_to_make_payment'] = 'Unable to pay with Apple Pay';
$_LANG['stripeapplepay_logged_in'] = 'Please confirm your device/ browser has your Apple account logged in';
$_LANG['stripeapplepay_configured_payment'] = 'Please confirm your Apple Pay account has valid credit card binded';
$_LANG['stripeapplepay_supported_browser'] = 'Please confirm your device/ browser supports Apple Pay and updated to latest version';
$_LANG['stripeapplepay_browser_privilege'] = "Please confirm access for payment methods of your browser is allowed";
$_LANG['stripeapplepay_configure_instruction'] = "If intented to set up your Apple Pay, Please refer to<br/><a href=\\\"https://support.apple.com/en-us/HT204506\\\" target='_blank'>Set up Apple Pay</a>";
$_LANG['stripeapplepay_supported_platform_heading'] = "Apple Pay supports following platform:";
$_LANG['stripeapplepay_supported_platform'] = [
    "Safari on iPhone",
];
