<?php

//en_us
global $_LANG;

$_LANG['stripe']                          = 'Stripe';
$_LANG['stripe_desc']                     = 'Stripe' ;
$_LANG['stripe_secret_key'] 			  = 'Secret key';
$_LANG['stripe_publishable_key'] 		  = 'Publishable key';
$_LANG['card_number'] 			  		  = 'Card Number';
$_LANG['sumbit'] 			  	  		  = 'Sumbit';
$_LANG['stripe_txn_id']                   = 'Stripe 交易編號';
$_LANG['stripe_currency']              	  = '支付貨幣';
$_LANG['stripe_currency_range']['AUD']    = '澳元';
$_LANG['stripe_currency_range']['CAD']    = '加元';
$_LANG['stripe_currency_range']['EUR']    = '歐元';
$_LANG['stripe_currency_range']['GBP']    = '英鎊';
$_LANG['stripe_currency_range']['JPY']    = '日元';
$_LANG['stripe_currency_range']['USD']    = '美元';
$_LANG['stripe_currency_range']['HKD']    = '港元';


?>