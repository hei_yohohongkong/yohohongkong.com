<?php

global $_LANG;

$_LANG['qfwechatpay'] = 'WeChat Pay';
$_LANG['qfwechatpay_desc'] = 'WeChat Pay (Online Payment)';
$_LANG['qfwechatpay_account'] = 'WeChat Pay Account';
$_LANG['qfwechatpay_key'] = 'Signature Key(Key)';
$_LANG['qfwechatpay_code'] = "Identifier(Code)"
?>