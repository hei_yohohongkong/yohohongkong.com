<?php

//en_us
global $_LANG;

$_LANG['stripegooglepay']   =   'Google Pay (Stripe)';
$_LANG['stripegooglepay_desc'] = '' ;
$_LANG['stripe_pay_now_message'] = 'Please press the following Google Pay button to begin payment process';

$_LANG['stripegooglepay_unable_to_make_payment'] = 'Unable to pay with Google Pay';
$_LANG['stripegooglepay_logged_in'] = 'Please confirm your device/ browser has your Google account logged in';
$_LANG['stripegooglepay_configured_payment'] = 'Please confirm your Google Pay account has valid credit card binded';
$_LANG['stripegooglepay_supported_browser'] = 'Please confirm your device/ browser supports Google Pay and updated to latest version';
$_LANG['stripegooglepay_browser_privilege'] = 'Please confirm access for payment methods of your browser is allowed';
$_LANG['stripegooglepay_configure_instruction'] = "If intented to set up your Google Pay, Please refer to<br/><a href=\\\"https://support.google.com/pay/answer/7625055?hl=en\\\" target='_blank'>Set up Google Pay</a>";
$_LANG['stripegooglepay_supported_platform_heading'] = "Google Pay supports following platform:";
$_LANG['stripegooglepay_supported_platform'] = [
    "Google Chrome on Android Phone"
];

// Tooltips text in yoho.php