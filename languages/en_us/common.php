<?php

/**
 * ECSHOP 前臺語言檔
 * ============================================================================
 * 版權所有 2005-2010 上海商派網路科技有限公司，並保留所有權利。
 * 網站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 這不是一個自由軟體！您只能在不用於商業目的的前提下對程式碼進行修改和
 * 使用；不允許對程式碼以任何形式任何目的的再發佈。
 * ============================================================================
 * $Author: liuhui $
 * $Id: common.php 17063 2010-03-25 06:35:46Z liuhui $
*/

/* 使用者登錄語言項 */
$_LANG['empty_username_password'] = '對不起，您必須完整填寫用戶名和密碼。';
$_LANG['shot_message'] = "短消息";

/* 公共語言項 */
$_LANG['sys_msg'] = '系統提示';
$_LANG['catalog'] = '目錄';
$_LANG['please_view_order_detail'] = '產品已出貨，詳情請到使用者中心訂單詳情查看';
$_LANG['user_center'] = '會員帳戶';
$_LANG['shop_closed'] = '本店盤點中，請您稍後再來...';
$_LANG['shop_register_closed'] = '該網店暫停註冊';
$_LANG['shop_upgrade'] = "本店升級中，管理員從 <a href=\"admin/\">管理中心</a> 登錄後，系統會自動完成升級";
$_LANG['js_languages']['process_request'] = '正在處理您的請求...';
$_LANG['process_request'] = '正在處理您的請求...';
$_LANG['please_waiting'] = '請稍等, 正在載入中...';
$_LANG['icp_number'] = 'ICP備案證書號';
$_LANG['plugins_not_found'] = "外掛程式 %s 無法定位";
$_LANG['home'] = '首頁';
$_LANG['back_up_page'] = '返回上一頁';
$_LANG['close_window'] = '關閉窗口';
$_LANG['back_home'] = '返回首頁';
$_LANG['ur_here'] = '當前位置:';
$_LANG['all_goods'] = '全部產品';
$_LANG['all_recommend'] = "全部推薦";
$_LANG['all_attribute'] = "All";
$_LANG['promotion_goods'] = '促銷產品';
$_LANG['best_goods'] = '精品推薦';
$_LANG['new_goods'] = '新品上市';
$_LANG['hot_goods'] = '熱銷產品';
$_LANG['view_cart'] = '查看購物車';
$_LANG['catalog'] = '所有分類';
$_LANG['regist_login'] = '註冊/登錄';
$_LANG['profile'] = '個人資料';
$_LANG['query_info'] = "共執行 %d 個查詢，用時 %f 秒，線上 %d 人";
$_LANG['gzip_enabled'] = '，Gzip 已啟用';
$_LANG['gzip_disabled'] = '，Gzip 已禁用';
$_LANG['memory_info'] = '，佔用記憶體 %0.3f MB';
$_LANG['cart_info'] = '您的購物車中有 %d 件產品，總計金額 %s。';
$_LANG['shopping_and_other'] = '購買過此產品的人還購買過';
$_LANG['bought_notes'] = '購買記錄';
$_LANG['later_bought_amounts'] = '近期成交數量';
$_LANG['bought_time'] = '購買時間';
$_LANG['turnover'] = '成交';
$_LANG['no_notes'] = '還沒有人購買過此產品';
$_LANG['shop_price'] = '售價';
$_LANG['market_price'] = "市場價錢";
$_LANG['goods_brief'] = '產品描述：';
$_LANG['goods_album'] = '產品相冊';
$_LANG['promote_price'] = "促銷價：";
$_LANG['fittings_price'] = '配件價格：';
$_LANG['collect'] = '加入我的最愛';
$_LANG['add_to_cart'] = '加入購物車';
$_LANG['return_to_cart'] = '放回購物車';
$_LANG['search_goods'] = '產品搜索';
$_LANG['search'] = '搜索';
$_LANG['wholesale_search'] = '搜索批發產品';
$_LANG['article_title'] = '資訊標題';
$_LANG['article_author'] = '作者';
$_LANG['article_add_time'] = '添加日期';
$_LANG['relative_file'] = '[ 相關下載 ]';
$_LANG['category'] = '分類';
$_LANG['brand'] = '品牌';
$_LANG['price_min'] = '最小價格';
$_LANG['price_max'] = '最大價格';
$_LANG['goods_name'] = '產品名稱';
$_LANG['goods_attr'] = '產品屬性';
$_LANG['goods_price_ladder'] = '價格階梯';
$_LANG['ladder_price'] = '批發價格';
$_LANG['shop_prices'] = '本店價';
$_LANG['market_prices'] = "市場價錢";
$_LANG['deposit'] = '團購保證金';
$_LANG['amount'] = '產品小計';
$_LANG['number'] = '購買數量';
$_LANG['handle'] = '操作';
$_LANG['add'] = '添加';
$_LANG['edit'] = '編輯';
$_LANG['drop'] = '刪除';
$_LANG['view'] = '查看';
$_LANG['modify'] = '修改';
$_LANG['is_cancel'] = '取消';
$_LANG['amend_amount'] = '修改數量';
$_LANG['end'] = '結束';
$_LANG['require_field'] = '(必填)';
$_LANG['search_result'] = '搜索結果';
$_LANG['order_number'] = '訂單編號';
$_LANG['consignment'] = '物流單';
$_LANG['activities'] = '產品正在進行的活動';
$_LANG['remark_package'] = '套裝優惠';
$_LANG['old_price'] = '原  價：';
$_LANG['package_price'] = '禮包價：';
$_LANG['then_old_price'] = '節  省：';
$_LANG['free_goods'] = '免運費產品';

$_LANG['searchkeywords_notice'] = '匹配多個關鍵字全部，可用 "空格" 或 "AND" 連接。如 win32 AND unix<br />匹配多個關鍵字其中部分，可用"+"或 "OR" 連接。如 win32 OR unix';
$_LANG['hidden_outstock'] = '隱藏已脫銷的產品';
$_LANG['keywords'] = '關鍵字';
$_LANG['sc_ds'] = '搜索簡介';
$_LANG['button_search'] = '立即搜索';
$_LANG['no_search_result'] = '無法搜索到您要找的產品！';
$_LANG['all_category'] = '所有分類';
$_LANG['all_brand'] = '所有品牌';
$_LANG['all_option'] = '請選擇';
$_LANG['extension'] = '擴展選項';
$_LANG['gram'] = '克';
$_LANG['kilogram'] = '千克';
$_LANG['goods_sn'] = "Barcode : ";
$_LANG['goods_brand'] = '產品品牌：';
$_LANG['goods_weight'] = '產品重量：';
$_LANG['goods_number'] = '產品庫存：';
$_LANG['goods_give_integral'] = "贈送積分";
$_LANG['goods_integral'] = '購買此產品可使用：';
$_LANG['goods_bonus'] = '購買此產品可獲得優惠券：';
$_LANG['goods_free_shipping'] = '此產品為免運費產品，計算配送金額時將不計入配送費用';
$_LANG['goods_rank'] = '用戶評價：';
$_LANG['goods_compare'] = '產品比較';
$_LANG['properties'] = '產品屬性：';
$_LANG['brief'] = '簡要介紹：';
$_LANG['add_time'] = '上架時間：';
$_LANG['residual_time'] = '剩餘時間：';
$_LANG['day'] = '天';
$_LANG['hour'] = '小時';
$_LANG['minute'] = '分鐘';
$_LANG['compare'] = '比較';
$_LANG['volume_price'] = '購買產品達到以下數量區間時可享受的優惠價格';
$_LANG['number_to'] = '數量';
$_LANG['article_list'] = '資訊列表';


/* 產品比較JS語言項 */
$_LANG['compare_js']['button_compare'] = '比較選定產品';
$_LANG['compare_js']['exist'] = '您已經選擇了%s';
$_LANG['compare_js']['count_limit'] = '最多只能選擇4個產品進行對比';
$_LANG['compare_js']['goods_type_different'] = '\"%s\"和已選擇產品類型不同無法進行對比';

$_LANG['bonus'] = '優惠券：';
$_LANG['no_comments'] = '暫時還沒有任何用家評價';
$_LANG['give_comments_rank'] = '給出';
$_LANG['comments_rank'] = '評價';
$_LANG['comment_num'] = "用家評價 %d 條記錄";
$_LANG['login_please'] = '由於您還沒有登錄，因此您還不能使用該功能。';
$_LANG['collect_existed'] = '該產品已經存在於您的我的最愛中。';
$_LANG['collect_success'] = '該產品已經成功地加入了您的我的最愛。';
$_LANG['copyright'] = "&copy; 2005-%s %s 版權所有，並保留所有權利。";
$_LANG['no_ads_id'] = '沒有指定廣告的ID以及跳轉的URL地址!';
$_LANG['remove_collection_confirm'] = '您確定要從我的最愛中刪除選定的產品嗎？';
$_LANG['err_change_attr'] = '沒有找到指定的產品或者沒有找到指定的產品屬性。';

$_LANG['collect_goods'] = '收藏產品';
$_LANG['plus'] = '加';
$_LANG['minus'] = '減';
$_LANG['yes'] = '是';
$_LANG['no'] = '否';

$_LANG['same_attrbiute_goods'] = '相同%s的產品';

/* TAG */
$_LANG['button_submit_tag'] = '添加我的標記';
$_LANG['tag_exists'] = '您已經為該產品添加過一個標記，請不要重複提交.';
$_LANG['tag_cloud'] = '標籤雲';
$_LANG['tag_anonymous'] = '對不起，只有註冊會員並且正常登錄以後才能提交標記。';
$_LANG['tag_cloud_desc'] = '標籤雲（Tag cloud）是用以表示一個網站中的內容標籤。 標籤（tag、關鍵字）是一種更為靈活、有趣的產品分類方式，您可以為每個產品添加一個或多個標籤，那麼可以通過點擊這個標籤查看產品其他會員提交的與您的標籤一樣的產品,能夠讓您使用最快的方式查找某一個標籤的所有網店產品。比方說點擊「紅色」這個標籤，就可以打開這樣的一個頁面，顯示所有的以「紅色」 為標籤的網店產品';

/* AJAX 相關 */
$_LANG['invalid_captcha'] = '對不起，您輸入的驗證碼不正確。';
$_LANG['goods_exists'] = '對不起，您的購物車中已經存在相同的產品。';
$_LANG['fitting_goods_exists'] = '對不起，您的購物車中已經添加了該配件。';
$_LANG['invalid_number'] = '對不起，您輸入了一個非法的產品數量。';
$_LANG['not_on_sale'] = '對不起，該產品已經下架。';
$_LANG['no_basic_goods'] = '對不起，您希望將該產品做為配件購買，可是購物車中還沒有該產品的基本件。';
$_LANG['cannt_alone_sale'] = '對不起，該產品不能單獨銷售。';
$_LANG['shortage'] = "對不起，該產品存貨不足。\n 如有疑問請致電 53356996 。";
$_LANG['shortage_little'] = "該產品已經庫存不足。已將您的購貨數量修改為 %d。\n您現在要去購物車嗎？";
$_LANG['oos_tips'] = '該產品已經庫存不足。您現在要進行缺貨登記嗎？';

$_LANG['addto_cart_success_1'] = "該產品已添加到購物車，您現在還需要繼續購物嗎？\n如果您希望馬上結算，請點擊「確定」按鈕。\n如果您希望繼續購物，請點擊「取消」按鈕。";
$_LANG['addto_cart_success_2'] = "該產品已添加到購物車，您現在還需要繼續購物嗎？\n如果您希望繼續購物，請點擊「確定」按鈕。\n如果您希望馬上結算，請點擊「取消」按鈕。";
$_LANG['no_keywords'] = "請輸入搜索關鍵字！";

/* 分頁排序 */
$_LANG['exchange_sort']['goods_id'] = '按上架時間排序';
$_LANG['exchange_sort']['exchange_integral'] = '按積分排序';
$_LANG['exchange_sort']['last_update'] = '按更新時間排序';
$_LANG['sort']['goods_id'] = '按上架時間排序';
$_LANG['sort']['shop_price'] = '按價格排序';
$_LANG['sort']['last_update'] = '按更新時間排序';
$_LANG['order']['DESC'] = '倒序';
$_LANG['order']['ASC'] = '正序';
$_LANG['pager_1'] = '總計 ';
$_LANG['pager_2'] = ' 個記錄';
$_LANG['pager_3'] = '，共 ';
$_LANG['pager_4'] = ' 頁。';
$_LANG['page_first'] = '第一頁';
$_LANG['page_prev'] = '上一頁';
$_LANG['page_next'] = '下一頁';
$_LANG['page_last'] = '最末頁';
$_LANG['btn_display'] = '顯示方式';

/* 投票 */
$_LANG['vote_times'] = '參與人次';
$_LANG['vote_ip_same'] = '對不起，您已經投過票了!';
$_LANG['submit_vote'] = '投票';
$_LANG['submit_reset'] = '重選';
$_LANG['vote_success'] = '恭喜你，投票成功';

/* 評價 */
$_LANG['cmt_submit_done'] = '您的評價已成功發表, 感謝您的參與!';
$_LANG['cmt_submit_wait'] = "您的評價已成功發表, 請等待管理員的審核!";
$_LANG['cmt_lang']['cmt_empty_username'] = '請輸入您的用戶名稱';
$_LANG['cmt_lang']['cmt_empty_email'] = '請輸入您的電子郵寄地址';
$_LANG['cmt_lang']['cmt_error_email'] = '電子郵寄地址格式不正確';
$_LANG['cmt_lang']['cmt_empty_content'] = '您沒有輸入評價的內容';
$_LANG['cmt_spam_warning'] = '您至少在30秒後才可以繼續發表評價!';
$_LANG['cmt_lang']['captcha_not_null'] = '驗證碼不能為空!';
$_LANG['cmt_lang']['cmt_invalid_comments'] = '無效的評價內容!';
$_LANG['invalid_comments'] = '無效的評價內容!';
$_LANG['error_email'] = '電子郵寄地址格式不正確!';
$_LANG['admin_username'] = "管理員：";
$_LANG['reply_comment'] = '回復';
$_LANG['comment_captcha'] = '驗證碼';
$_LANG['comment_login'] = '只有註冊會員才能發表評價，請您登錄後再發表評價';
$_LANG['comment_custom'] = '評價失敗。只有在本店購買過產品的註冊會員才能發表評價。';
$_LANG['comment_brought'] = '評價失敗。只有購買過此產品的註冊用戶才能評價該產品。';
$_LANG['anonymous'] = '匿名使用者';

/* 其他資訊 */
$_LANG['js_languages']['goodsname_not_null'] = '產品名不能為空！';

/* 產品比較 */
$_LANG['compare_remove'] = '移除';
$_LANG['compare_no_goods'] = '您沒有選定任何需要比較的產品或者比較的產品數少於 2 個。';

$_LANG['no_user_name'] = '該用戶名不存在';
$_LANG['undifine_rank'] = '沒有定義會員等級';
$_LANG['not_login'] = "您還沒有登入";
$_LANG['half_info'] = '資訊不全，請填寫所有資訊';
$_LANG['no_id'] = '沒有產品ID';
$_LANG['save_success'] = '修改成功';
$_LANG['drop_consignee_confirm'] = '您確定要刪除該收貨人資訊嗎？';

/* 奪寶奇兵 */
$_LANG['snatch_js']['price_not_null'] = '價格不能為空';
$_LANG['snatch_js']['price_not_number'] = '價格只能是數字';
$_LANG['snatch_list'] = '奪寶奇兵列表';
$_LANG['not_in_range'] = '你只能在%d到%d之間出價';
$_LANG['also_bid'] = '你已經出過價格 %s 了';
$_LANG['lack_pay_points'] = '你積分不夠，不能出價';
$_LANG['snatch'] = '奪寶奇兵';
$_LANG['snatch_is_end'] = '活動已經結束';
$_LANG['snatch_start_time'] = '本次活動從 %s 到 %s 截止';
$_LANG['price_extent'] = '出價範圍為';
$_LANG['user_to_use_up'] = '用戶可多次出價，每次消耗';
$_LANG['snatch_victory_desc'] = '當本期活動截止時，系統將從所有競價獎品的用戶中，選出在所有競價中出價最低、且沒有其他出價與該價格重複的用戶（即最低且唯一競價），成為該款獎品的獲勝者.';
$_LANG['price_less_victory'] = '如果用戶獲勝的價格低於';
$_LANG['price_than_victory'] = '將能按當期競拍價購得該款獎品；如果用戶獲勝的價格高於';
$_LANG['or_can'] = '則能以';
$_LANG['shopping_product'] = '購買該款獎品';
$_LANG['victory_price_product'] = '獲勝用戶將能按當期競拍價購得該款獎品.';
$_LANG['now_not_snatch'] = '當前沒有活動';
$_LANG['my_integral'] = '我的積分';
$_LANG['bid'] = '出價';
$_LANG['me_bid'] = '我要出價';
$_LANG['me_now_bid'] = '我的出價';
$_LANG['only_price'] = '唯一價格';
$_LANG['view_snatch_result'] = '活動結果';
$_LANG['victory_user'] = '獲獎用戶';
$_LANG['price_bid'] = '所出價格';
$_LANG['bid_time'] = '出價時間';
$_LANG['not_victory_user'] = '沒有獲獎用戶';
$_LANG['snatch_log'] = '參加奪寶奇兵%s ';
$_LANG['not_for_you'] = '你不是獲勝者，不能購買';
$_LANG['order_placed'] = '您已經下過訂單了，如果您想重新購買，請先取消原來的訂單';

/* 購物流程中的前臺部分 */
$_LANG['select_spe'] = '請選擇產品屬性';

/* 購物流程中的訂單部分 */
$_LANG['price'] = '價格';
$_LANG['name'] = '名稱';
$_LANG['describe'] = '描述';
$_LANG['fee'] = '費用';
$_LANG['free_money'] = '免費額度';
$_LANG['img'] = '圖片';
$_LANG['no_pack'] = '不要包裝';
$_LANG['no_card'] = '不要賀卡';
$_LANG['bless_note'] = '祝福語';
$_LANG['use_integral'] = '使用積分';
$_LANG['can_use_integral'] = '您當前的可用積分為';
$_LANG['noworder_can_integral'] = '本訂單最多可以使用';
$_LANG['use_surplus'] = '使用餘額';
$_LANG['your_surplus'] = '您當前的可用餘額為';
$_LANG['pay_fee'] = '支付手續費';
$_LANG['insure_fee'] = '保價費用';
$_LANG['need_insure'] = '配送是否需要保價';
$_LANG['cod'] = '配送決定';

$_LANG['curr_stauts'] = '當前狀態';
$_LANG['use_bonus'] = '使用優惠券';
$_LANG['use_bonus_kill'] = '使用線下優惠券';
$_LANG['invoice'] = '開發票';
$_LANG['invoice_type'] = '發票類型';
$_LANG['invoice_title'] = '發票抬頭';
$_LANG['invoice_content'] = '發票內容';
$_LANG['order_postscript'] = '訂單附言';
$_LANG['booking_process'] = '缺貨處理';
$_LANG['complete_acquisition'] = '該訂單完成後，您將獲得';
$_LANG['with_price'] = '以及價值';
$_LANG['de'] = '的';
$_LANG['bonus'] = '優惠券';
$_LANG['goods_all_price'] = '產品小計';
$_LANG['discount'] = '折扣';
$_LANG['tax'] = '發票稅額';
$_LANG['shipping_fee'] = '配送費用';
$_LANG['pack_fee'] = '包裝費用';
$_LANG['card_fee'] = '賀卡費用';
$_LANG['total_fee'] = '應付款金額';
$_LANG['self_site'] = '本站';
$_LANG['order_gift_integral'] = '訂單 %s 贈送的積分';

$_LANG['order_payed_sms'] = '訂單 %s 付款了。收貨人：%s；電話：%s。';

/* 缺貨處理 */
$_LANG['oos'][OOS_WAIT] = '等待所有產品備齊後再發';
$_LANG['oos'][OOS_CANCEL] = '取消訂單';
$_LANG['oos'][OOS_CONSULT] = '與店主協商';

/* 評價部分 */
$_LANG['username'] = '用戶名';
$_LANG['email'] = '電子郵寄地址';
$_LANG['comment_rank'] = '評價等級';
$_LANG['comment_content'] = '評價內容';
$_LANG['submit_comment'] = '提交評價';
$_LANG['button_reset'] = '重置表單';
$_LANG['goods_comment'] = '產品評價';
$_LANG['article_comment'] = '資訊評價';

/* 支付確認部分 */
$_LANG['pay_status'] = '支付狀態';
$_LANG['pay_not_exist'] = '此支付方式不存在或者參數錯誤！';
$_LANG['pay_disabled'] = '此支付方式還沒有被啟用！';
$_LANG['pay_success'] = '您此次的支付操作已成功！';
$_LANG['pay_fail'] = '支付操作失敗，請返回重試！';

/* 資訊部分 */
$_LANG['new_article'] = '最新資訊';
$_LANG['shop_notice'] = '商店公告';
$_LANG['order_already_received'] = '此訂單已經確認過了，感謝您在本站購物，歡迎再次光臨。';
$_LANG['order_invalid'] = '您提交的訂單不正確。';
$_LANG['act_ok'] = '謝謝您通知我們您已收到貨，感謝您在本站購物，歡迎再次光臨。';
$_LANG['receive'] = '收貨確認';
$_LANG['buyer'] = '買家';
$_LANG['next_article'] = '下一篇';
$_LANG['prev_article'] = '上一篇';

/* 虛擬產品 */
$_LANG['virtual_goods_ship_fail'] = '自動出貨失敗，請儘快聯繫商家重新出貨';

/* 選購中心 */
$_LANG['pick_out'] = '選購中心';
$_LANG['fit_count'] = "共有 %s 件產品符合條件";
$_LANG['goods_type'] = "產品類型";
$_LANG['remove_all'] = '移除所有';
$_LANG['advanced_search'] = '高級搜索';
$_LANG['activity'] = '本產品正在進行';
$_LANG['order_not_exists'] = "非常抱歉，沒有找到指定的訂單。請和網站管理員聯繫。";

$_LANG['promotion_time'] = '的時間為%s到%s，趕快來搶吧！';

/* 倒計時 */
$_LANG['goods_js']['day'] = '天';
$_LANG['goods_js']['hour'] = '小時';
$_LANG['goods_js']['minute'] = '分鐘';
$_LANG['goods_js']['second'] = '秒';
$_LANG['goods_js']['end'] = '結束';

$_LANG['favourable'] = '優惠活動';

/* 團購部分語言項 */
$_LANG['group_buy'] = '團購活動';
$_LANG['group_buy_goods'] = '團購產品';
$_LANG['gb_goods_name'] = '團購產品：';
$_LANG['gb_start_date'] = '開始時間：';
$_LANG['gb_end_date'] = '結束時間：';
$_LANG['gbs_pre_start'] = '該團購活動尚未開始，請繼續關注。';
$_LANG['gbs_under_way'] = '該團購活動正在火熱進行中，距離結束時間還有：';
$_LANG['gbs_finished'] = '該團購活動已結束，正在等待處理...';
$_LANG['gbs_succeed'] = '該團購活動已成功結束！';
$_LANG['gbs_fail'] = '該團購活動已結束，沒有成功。';
$_LANG['gb_price_ladder'] = '價格階梯：';
$_LANG['gb_ladder_amount'] = '數量';
$_LANG['gb_ladder_price'] = '價格';
$_LANG['gb_deposit'] = '保證金：';
$_LANG['gb_restrict_amount'] = '限購數量：';
$_LANG['gb_gift_integral'] = '贈送積分：';
$_LANG['gb_cur_price'] = '當前價格：';
$_LANG['gb_valid_goods'] = '當前定購數量：';
$_LANG['gb_final_price'] = '成交價格：';
$_LANG['gb_final_amount'] = '成交數量：';
$_LANG['gb_notice_login'] = '提示：您需要先註冊成為本站會員並且登錄後，才能參加產品團購!';
$_LANG['gb_error_goods_lacking'] = '對不起，產品庫存不足，請您修改數量！';
$_LANG['gb_error_status'] = '對不起，該團購活動已經結束或尚未開始，現在不能參加！';
$_LANG['gb_error_login'] = '對不起，您沒有登錄，不能參加團購，請您先登錄！';
$_LANG['group_goods_empty'] = '當前沒有團購活動';

/* 拍賣部分語言項 */
$_LANG['auction'] = '拍賣活動';
$_LANG['act_status'] = '活動狀態';
$_LANG['au_current_price'] = '當前價格';
$_LANG['act_start_time'] = '開始時間';
$_LANG['act_end_time'] = '結束時間';
$_LANG['au_start_price'] = '起拍價';
$_LANG['au_end_price'] = '一口價';
$_LANG['au_amplitude'] = '加價幅度';
$_LANG['au_deposit'] = '保證金';
$_LANG['no_auction'] = '當前沒有拍賣活動';
$_LANG['au_pre_start'] = '該拍賣活動尚未開始';
$_LANG['au_under_way'] = '該拍賣活動正在進行中，距離結束時間還有：';
$_LANG['au_under_way_1'] = '該拍賣活動正在進行中';
$_LANG['au_bid_user_count'] = '已出價人數';
$_LANG['au_last_bid_price'] = '最後出價';
$_LANG['au_last_bid_user'] = '最後出價的買家';
$_LANG['au_last_bid_time'] = '最後出價時間';
$_LANG['au_finished'] = '該拍賣活動已結束';
$_LANG['au_bid_user'] = '買家';
$_LANG['au_bid_price'] = '出價';
$_LANG['au_bid_time'] = '時間';
$_LANG['au_bid_status'] = '狀態';
$_LANG['no_bid_log'] = '暫時沒有買家出價';
$_LANG['au_bid_ok'] = '領先';
$_LANG['au_i_want_bid'] = '我要出價';
$_LANG['button_bid'] = '出價';
$_LANG['button_buy'] = '立即購買';
$_LANG['au_not_under_way'] = '拍賣活動已結束，不能再出價了';
$_LANG['au_bid_price_error'] = '請輸入正確的價格';
$_LANG['au_bid_after_login'] = '您只有註冊成為會員並且登錄之後才能出價';
$_LANG['au_bid_repeat_user'] = '您已經是這個產品的最高出價人了';
$_LANG['au_your_lowest_price'] = '您的出價不能低於 %s';
$_LANG['au_user_money_short'] = '您的可用資金不足，請先到用戶中心充值';
$_LANG['au_unfreeze_deposit'] = '解凍拍賣活動的保證金：%s';
$_LANG['au_freeze_deposit'] = '凍結拍賣活動的保證金：%s';
$_LANG['au_not_finished'] = '該拍賣活動尚未結束，不能購買';
$_LANG['au_order_placed'] = '您已經下過訂單了，如果您想重新購買，請先取消原來的訂單';
$_LANG['au_no_bid'] = '該拍賣活動沒有人出價，不能購買';
$_LANG['au_final_bid_not_you'] = '您不是最高出價者，不能購買';
$_LANG['au_buy_after_login'] = '請您先登錄';
$_LANG['au_is_winner'] = '恭喜您，您已經贏得了該產品的購買權。請點擊下麵的購買按鈕將您的寶貝買回家吧。';

/* 批發部分語言項 */
$_LANG['ws_user_rank'] = '您的等級暫時無法查看批發方案';
$_LANG['ws_login_please'] = '請您先登錄';
$_LANG['ws_return_home'] = '返回首頁';
$_LANG['wholesale'] = '批發';
$_LANG['no_wholesale'] = '沒有批發產品';
$_LANG['ws_price'] = '批發價';
$_LANG['ws_subtotal'] = '小計';
$_LANG['ws_invalid_goods_number'] = '請輸入正確的數量';
$_LANG['ws_attr_not_matching'] = '您選擇的產品屬性不存在，請參照批發價格單選擇';
$_LANG['ws_goods_number_not_enough'] = '您購買的數量沒有達到批發的最小數量，請參照批發價格單';
$_LANG['ws_goods_attr_exists'] = '該產品已經在購物車中，不能再次加入';
$_LANG['ws_remark'] = '請輸入您的聯繫方式、付款方式和配送方式等資訊';
$_LANG['ws_order_submitted'] = '您的訂單已提交成功，請記住您的訂單編號: %s。';
$_LANG['ws_price_list'] = '價格單';

/* 積分兌換部分語言項 */
$_LANG['exchange'] = '積分商城';
$_LANG['exchange_integral'] = '消耗積分：';
$_LANG['exchange_goods'] = '立刻兌換';
$_LANG['eg_error_login'] = '對不起，您沒有登錄，不能參加兌換，請您先登錄！';
$_LANG['eg_error_status'] = '對不起，該產品已經取消，現在不能兌換！';
$_LANG['eg_error_integral'] = '對不起，您現有的積分值不夠兌換本產品！';
$_LANG['notice_eg_integral'] = '積分商城產品需要消耗積分：';
$_LANG['eg_error_number'] = '對不起，該產品庫存不足，現在不能兌換！';

/* 會員登錄註冊 */
$_LANG['member_name'] = '會員';
$_LANG['password'] = '密碼';
$_LANG['confirm_password'] = '確認密碼';
$_LANG['sign_up'] = '註冊新會員';
$_LANG['forgot_password'] = '您忘記密碼了嗎？';
$_LANG['hello'] = '您好';
$_LANG['welcome_return'] = '歡迎您回來';
$_LANG['now_account'] = '您的帳戶中現在有';
$_LANG['balance'] = '餘額';
$_LANG['along_with'] = '以及';
$_LANG['preferential'] = '優惠券';
$_LANG['edit_user_info'] = '進入用戶中心';
$_LANG['logout'] = '退出';
$_LANG['user_logout'] = '退出';
$_LANG['welcome'] = '歡迎光臨本店';
$_LANG['user_login'] = '會員登陸';
$_LANG['login_now'] = '立即登陸';
$_LANG['reg_now'] = '立即註冊';

/* 產品品牌頁 */
$_LANG['official_site'] = '官方網站：';
$_LANG['brand_category'] = '分類流覽：';
$_LANG['all_category'] = '所有分類';

/* 產品分類頁 */
$_LANG['goods_filter'] = '產品篩選';

/* cls_image類的語言項 */
$_LANG['directory_readonly'] = '目錄 % 不存在或不可寫';
$_LANG['invalid_upload_image_type'] = '不是允許的圖片格式';
$_LANG['upload_failure'] = '檔 %s 上傳失敗。';
$_LANG['missing_gd'] = '沒有安裝GD庫';
$_LANG['missing_orgin_image'] = '找不到原始圖片 %s ';
$_LANG['nonsupport_type'] = '不支援該圖像格式 %s ';
$_LANG['creating_failure'] = '創建圖片失敗';
$_LANG['writting_failure'] = '圖片寫入失敗';
$_LANG['empty_watermark'] = '浮水印檔參數不能為空';
$_LANG['missing_watermark'] = '找不到浮水印檔%s';
$_LANG['create_watermark_res'] = '創建浮水印圖片資源失敗。浮水印圖片類型為%s';
$_LANG['create_origin_image_res'] = '創建原始圖片資源失敗，原始圖片類型%s';
$_LANG['invalid_image_type'] = '無法識別浮水印圖片 %s ';
$_LANG['file_unavailable'] = '文件 %s 不存在或不可讀';

/* 郵件發送錯誤資訊 */
$_LANG['smtp_setting_error'] = '郵件伺服器設置資訊不完整';
$_LANG['smtp_connect_failure'] = '無法連接到郵件伺服器 %s';
$_LANG['smtp_login_failure'] = '郵件伺服器驗證帳號或密碼不正確';
$_LANG['smtp_refuse'] = '伺服器拒絕發送該郵件';
$_LANG['sendemail_false'] = "郵件發送失敗，請與網站管理員聯繫！";
$_LANG['disabled_fsockopen'] = 'fsockopen函數被禁用';

$_LANG['topic_goods_empty'] = '當前沒有專題產品';
$_LANG['email_list_ok'] = '訂閱';
$_LANG['email_list_cancel'] = '退訂';
$_LANG['email_invalid'] = '郵寄地址非法！';
$_LANG['email_alreadyin_list'] = '郵寄地址已經存在於列表中！';
$_LANG['email_notin_list'] = '郵寄地址不在列表中！';
$_LANG['email_re_check'] = '已經重新發送驗證郵件，請查收並確認！';
$_LANG['email_check'] = '請查收郵件進行確認操作！';
$_LANG['email_not_alive'] = '此郵寄地址是未驗證狀態，不需要退訂！';
$_LANG['check_mail'] = '驗證郵件';
$_LANG['check_mail_content'] = "%s 您好：<br><br>這是由%s發送的郵件訂閱驗證郵件,點擊以下的連結位址,完成驗證操作。<br><a href=\"%s\" target=\"_blank\">%s</a>\n<br><br>%s<br>%s";
$_LANG['email_checked'] = '郵件已經被確認！';
$_LANG['hash_wrong'] = '驗證串錯誤！請核對驗證串或輸入email位址重新發送驗證串！';
$_LANG['email_canceled'] = '郵件已經被退定！';
$_LANG['goods_click_count'] = '產品點擊數';
$_LANG['p_y']['link_start'] = '<a href="http://www.ecshop.com" target="_blank" style=" font-family:Verdana; font-size:11px;">';
$_LANG['p_y']['link_p'] = 'Powe';
$_LANG['p_y']['link_r'] = 'red&nbsp;';
$_LANG['p_y']['link_b'] = 'by&nbsp;';
$_LANG['p_y']['main_start'] = '<strong><span style="color: #3366FF">';
$_LANG['p_y']['main_e'] = 'E';
$_LANG['p_y']['main_c'] = 'CSho';
$_LANG['p_y']['main_p'] = 'p</span>&nbsp;';
$_LANG['p_y']['v_s'] = '<span style="color: #FF9966">';
$_LANG['p_y']['v'] = VERSION;
$_LANG['p_y']['link_end'] = '</span></strong></a>&nbsp;';

/* 虛擬卡 */
$_LANG['card_sn'] = '卡片序號';
$_LANG['card_password'] = '卡片密碼';
$_LANG['end_date'] = '截至日期';
$_LANG['virtual_card_oos'] = '虛擬卡已缺貨';

/* 訂單狀態查詢 */
$_LANG['invalid_order_sn'] = '無效訂單編號';
$_LANG['order_status'] = '訂單狀態';
$_LANG['shipping_date'] = '出貨時間';
$_LANG['query_order'] = '查詢該訂單編號';
$_LANG['order_query_toofast'] = '您的提交頻率太高，歇會兒再查吧。';

$_LANG['online_info'] = '當前共有 %s 人線上';

/* 按鈕 */
$_LANG['btn_direct_buy'] = '直接購買';
$_LANG['btn_buy'] = '購買';
$_LANG['btn_collect'] = '收藏';
$_LANG['btn_add_to_cart'] = '加入購物車';
$_LANG['btn_add_to_collect'] = '添加收藏';

$_LANG['stock_up'] = '缺貨';


$_LANG['hot_search'] = '熱門搜索';

$_LANG['please_select_attr'] = '你加入購物車的產品有不同型號可選，你是否要立即跳轉到產品詳情選擇型號？';

/* 促銷資訊欄 */
$_LANG['snatch_promotion'] = '[奪寶]';
$_LANG['group_promotion'] = '[團購]';
$_LANG['auction_promotion'] = '[拍賣]';
$_LANG['favourable_promotion'] = '[優惠]';
$_LANG['wholesale_promotion'] = '[批發]';
$_LANG['package_promotion'] = '[禮包]';

/* feed推送 */
$_LANG['feed_user_buy'] = "購買了";
$_LANG['feed_user_comment'] = "評價了";
$_LANG['feed_goods_price'] = "產品價格";
$_LANG['feed_goods_desc'] = "產品描述";

/* 留言板 */
$_LANG['shopman_comment'] = '產品評價';
$_LANG['message_ping'] = '評';
$_LANG['message_board'] = "留言板";
$_LANG['post_message'] = "我要留言";
$_LANG['message_title'] = '主題';
$_LANG['message_time'] = '留言時間';
$_LANG['reply_time'] = '回復時間';
$_LANG['shop_owner_reply'] = '店主回復';
$_LANG['message_board_type'] = '留言類型';
$_LANG['message_content'] = '留言內容';
$_LANG['message_anonymous'] = '匿名留言';
$_LANG['message_type'][M_MESSAGE] = '留言';
$_LANG['message_type'][M_COMPLAINT] = '投訴';
$_LANG['message_type'][M_ENQUIRY] = '詢問';
$_LANG['message_type'][M_CUSTOME] = '售後';
$_LANG['message_type'][M_BUY] = '求購';
$_LANG['message_type'][M_BUSINESS] = '商家留言';
$_LANG['message_type'][M_COMMENT] = '評價';
$_LANG['message_board_js']['msg_empty_email'] = '請輸入您的電子郵寄地址';
$_LANG['message_board_js']['msg_error_email'] = '電子郵寄地址格式不正確';
$_LANG['message_board_js']['msg_title_empty'] = '留言標題為空';
$_LANG['message_board_js']['msg_content_empty'] = '留言內容為空';
$_LANG['message_board_js']['msg_captcha_empty'] = '驗證碼為空';
$_LANG['message_board_js']['msg_title_limit'] = '留言標題不能超過200個字';
$_LANG['message_submit_wait'] = '您的留言已成功發表, 請等待管理員的審核!';
$_LANG['message_submit_done'] = '發表留言成功';
$_LANG['message_board_close'] = "暫停留言板功能";
$_LANG['upload_file_limit'] = '檔大小超過了限制 %dKB';
$_LANG['message_list_lnk'] = '返回留言列表';

/* 報價單 */
$_LANG['quotation'] = "報價單";
$_LANG['print_quotation'] = "列印報價單";
$_LANG['goods_inventory'] = "庫存";
$_LANG['goods_category'] = "產品分類";
$_LANG['shopman_reply'] = '管理員回復';

/* 相冊JS語言項 */
$_LANG['gallery_js']['close_window'] = '您是否關閉當前窗口';
$_LANG['submit'] = '提 交';
$_LANG['reset'] = '重 置';
$_LANG['order_query'] = '訂單查詢';
$_LANG['shipping_query'] = '出貨查詢';
$_LANG['view_history'] = '流覽歷史';
$_LANG['clear_history'] = '[清空]';
$_LANG['no_history'] = '您已清空最近流覽過的產品';
$_LANG['goods_tag'] = '產品標籤';
$_LANG['releate_goods'] = '相關產品';
$_LANG['goods_list'] = '產品列表';
$_LANG['favourable_goods'] = '收藏該產品';
$_LANG['accessories_releate'] = '相關配件';
$_LANG['article_releate'] = '相關資訊';
$_LANG['email_subscribe'] = '郵件訂閱';
$_LANG['consignee_info'] = '收貨人信息';
$_LANG['user_comment'] = '用家評價';
$_LANG['total'] = '共';
$_LANG['user_comment_num'] = '條評價';
$_LANG['auction_goods'] = '拍賣產品';
$_LANG['auction_goods_info'] = '拍賣產品詳情';
$_LANG['article_cat'] = '資訊分類';
$_LANG['online_vote'] = '線上調查';
$_LANG['new_price'] = '最新出價';
$_LANG['promotion_info'] = '促銷資訊';
$_LANG['price_grade'] = '價格範圍';
$_LANG['your_choice'] = '您的選擇';
$_LANG['system_info'] = '系統資訊';
$_LANG['all_tags'] = '所有標籤';
$_LANG['activity_list'] = '活動列表';
$_LANG['package_list'] = '禮包列表';
$_LANG['treasure_info'] = '寶貝詳情';
$_LANG['activity_desc'] = '活動描述';
$_LANG['activity_intro'] = '活動介紹';
$_LANG['get_password'] = '找回密碼';
$_LANG['fee_total'] = '費用總計';
$_LANG['other_info'] = '其它資訊';
$_LANG['user_balance'] = '會員餘額';
$_LANG['wholesale_goods_cart'] = '批發產品購物車';
$_LANG['wholesale_goods_list'] = '批發產品列表';
$_LANG['bid_record'] = '出價記錄';
$_LANG['shipping_method'] = '配送方式';
$_LANG['payment_method'] = '支付方式';
$_LANG['goods_package'] = '產品包裝';
$_LANG['goods_card'] = '祝福賀卡';
$_LANG['groupbuy_intro'] = '團購說明';
$_LANG['groupbuy_goods_info'] = '團購產品詳情';
$_LANG['act_time'] = '起止時間';
$_LANG['top10'] = '銷售排行';

/* 優惠活動 */
$_LANG['label_act_name'] = '優惠活動名稱：';
$_LANG['label_start_time'] = '優惠開始時間：';
$_LANG['label_end_time'] = '優惠結束時間：';
$_LANG['label_user_rank'] = '享受優惠的會員等級：';
$_LANG['not_user'] = '非會員';
$_LANG['label_act_range'] = '優惠範圍：';
$_LANG['far_all'] = '全部產品';
$_LANG['far_category'] = '以下分類';
$_LANG['far_brand'] = '以下品牌';
$_LANG['far_goods'] = '以下產品';
$_LANG['label_min_amount'] = '金額下限：';
$_LANG['label_max_amount'] = '金額上限：';
$_LANG['notice_max_amount'] = '0表示沒有上限';
$_LANG['label_act_type'] = '優惠方式：';
$_LANG['fat_goods'] = '享受贈品（特惠品）';
$_LANG['fat_price'] = '享受現金減免';
$_LANG['fat_discount'] = '享受價格折扣';
$_LANG['orgtotal'] = '原始價格';
$_LANG['heart_buy'] = '心動不如行動';

/* 其他範本涉及常用語言項 */
$_LANG['label_regist'] = '用戶註冊';
$_LANG['label_login'] = '用戶登錄';
$_LANG['label_profile'] = '使用者資訊';
$_LANG['label_collection'] = '我的收藏';
$_LANG['article_list'] = '資訊列表';
$_LANG['preferences_price'] = '優惠價格';
$_LANG['divided_into'] = '分成規則';

/* new */
$_LANG['goods_details'] = '產品功能';
$_LANG['goods_transaction'] = '產品交易';
$_LANG['goods_review'] = '產品評分';
$_LANG['goods_promise'] = '網上購物保障';
$_LANG['goods_inquiry'] = "顧客查詢";
$_LANG['goods_buyer'] = '客人';
$_LANG['goods_soldname'] = '產品名稱';
$_LANG['goods_soldprice'] = '售出價錢';
$_LANG['goods_soldtime'] = '售出時間';
$_LANG['goods_alreadysold'] = '已賣出';
$_LANG['goods_soldunit'] = '件';
$_LANG['index_wel'] = "歡迎";
$_LANG['user_login'] = '登入';
$_LANG['user_register'] = '註冊';
$_LANG['flow_cart'] = '購物車';
$_LANG['user_order'] = '訂單';
$_LANG['index_sale'] = '限時秒殺優惠';
$_LANG['favourite'] = "加入我的最愛";
$_LANG['hotline'] = '銷售熱線';
/* Ecodemall */
@include(ROOT_PATH.'themes/'.$_CFG['template'].'/lang/'.$_CFG['lang'].'/common.php');
?>
