<?php
define('IN_ECS', true);

require dirname(__FILE__) . '/includes/init.php';
assign_template();
if (isset($_REQUEST['success'])) {
    if (isset($_REQUEST['subscribe'])) {
        show_message(_L('subscription_message_subscribe_success', "您已經訂閱了友和電子報，請前往電郵點選確認鏈結以完成訂閱程序"));
    } elseif (isset($_REQUEST['update'])) {
        show_message(_L('subscription_message_update_success', '您已成功更新資料！\n若您有更新電郵地址，請前往郵箱點擊確認訂閱電子報的鏈結'));
    } elseif (isset($_REQUEST['unsubscribe'])) {
        show_message(_L('subscription_message_unsubscribe_success', '您已取消訂閱友和電子報'));
    } elseif (isset($_REQUEST['confirm'])) {
        show_message(_L('subscription_message_confirm_success', '您已成功訂閱友和電子報'));
    } else {
        header('Location: /');
    }
} else {
    header('Location: /');
}
?>