<?php
define('IN_ECS', true);

define('FORCE_HTTPS', true);

require_once dirname(__FILE__) . '/includes/init.php';
require_once ROOT_PATH . 'includes/lib_order.php';
if (!empty(trim($_REQUEST['order_id']))) {
    if (empty($_SESSION['user_id'])) {
        // this link is only for login users
        show_message(_L('order_receipt_message_require_login', '請先登入帳戶再上傳。'), _L('global_go_home', '返回首頁'), '/', 'error', false);
    }
    $_REQUEST['orcode'] = "error";
    $temp_order_id = intval(trim($_REQUEST['order_id']));
    $temp_order = order_info($temp_order_id);
    if (payment_info($temp_order['pay_id'])['is_online_pay'] == '0' && in_array($temp_order['order_status'], [OS_CONFIRMED, OS_UNCONFIRMED]) && in_array($temp_order['pay_status'], [PS_PAYING, PS_UNPAYED])) {
        $records = $db->getAll("SELECT * FROM " . $ecs->table("offline_payment") . " WHERE order_id = $temp_order_id AND status < " . OFFLINE_PAYTMENT_FINISHED . " ORDER BY status ASC");
        if (!empty($records)) {
            foreach ($records as $rec) {
                if ($_REQUEST['orcode'] == "error" && in_array($rec['status'], [OFFLINE_PAYTMENT_PENDING, OFFLINE_PAYTMENT_EXTRA_INFO])) {
                    $_REQUEST['orcode'] = $rec['token'];
                    $update = [];
                    if (intval($rec['expire_time']) < gmtime()) {
                        $expire_time = gmtime() + 86400;
                        $_REQUEST['orcode'] = md5("$rec[user_id]-$rec[order_id]-$rec[status]-$expire_time");
                        $update[] = 'expire_time = ' . $expire_time;
                        $update[] = "token = '$_REQUEST[orcode]'";
                    }
                    if (intval($temp_order['pay_id']) != intval($rec['pay_id'])) {
                        $update[] = "pay_id = $temp_order[pay_id]";
                    }
                    if (!empty($update)) {
                        $db->query("UPDATE " . $ecs->table("offline_payment") . " SET " . implode(", ", $update) . " WHERE op_id = $rec[op_id]");
                    }
                } elseif ($_REQUEST['orcode'] == "error" && $rec['status'] == OFFLINE_PAYTMENT_UPLOADED) {
                    $_REQUEST['orcode'] = $rec['token'];
                }
            }
        } else {
            $expire_time = gmtime() + 86400;
            $_REQUEST['orcode'] = md5("$temp_order[user_id]-$temp_order[order_id]-" . OFFLINE_PAYTMENT_PENDING . "-$expire_time");
            $db->query("INSERT INTO " . $ecs->table("offline_payment") . " (user_id, order_id, pay_id, status, expire_time, token) VALUES ($temp_order[user_id], $temp_order[order_id], $temp_order[pay_id], " . OFFLINE_PAYTMENT_PENDING . ", $expire_time, '$_REQUEST[orcode]')");
        }
    }
}
if (!empty($_REQUEST['orcode'])) {
    $token = trim($_REQUEST['orcode']);
    $record = validateToken($token);
    if (!empty($record)) {
        $payment = payment_info($record['pay_id']);
        if (strpos($payment['pay_code'], 'bank') !== false) {
            $banks = $payment['pay_config_list']['banks'];
        }
        if ($payment['pay_code'] == 'fps') {
            $fps = [
                'phone' => $payment['pay_config_list']['phone'],
                'email' => $payment['pay_config_list']['email'],
                'fps_id' => $payment['pay_config_list']['fps_id'],
            ];
        }
        $smarty->assign('order_sn', $record['order_sn']);
        $smarty->assign('status', $record['status']);
        $smarty->assign('pay_account', $record['pay_account']);
        $smarty->assign('banks', $banks);
        $smarty->assign('fps', $fps);
        $smarty->assign('pay_name', $payment['pay_name']);
        $smarty->assign('token', $token);
        $now = gmtime();
        $validate_token = md5((empty($_SESSION['user_id']) ? $record['user_id'] : $_SESSION['user_id']) . "-$record[order_id]-$record[pay_id]-$record[status]-$now");
        $_SESSION['order_receipt_validate'] = $now;
        $smarty->assign('validate_token', $validate_token);
        
        /* 赋值固定内容 */
        assign_template();
        $position = assign_ur_here(0);

        $smarty->assign('page_title', $position['title']);    // 页面标题
        $smarty->assign('ur_here',    $position['ur_here']);  // 当前位置
        $categoryController = new Yoho\cms\Controller\CategoryController();
        $t1 = $categoryController->getTopCategories(0);
        $smarty->assign('t1',              $t1);
        $smarty->assign('helps', get_shop_help());              // 网店帮助
        
        $smarty->display('order_receipts.html');
    }
} elseif (!empty($_REQUEST['uploads'])) {
    $token = trim($_REQUEST['token']);
    $record = validateToken($token);
    $validate_token = md5((empty($_SESSION['user_id']) ? $record['user_id'] : $_SESSION['user_id']) . "-$record[order_id]-$record[pay_id]-$record[status]-$_SESSION[order_receipt_validate]");
    if ($_FILES['pay_proof']['error'] === 1) {
        show_message(_L('order_receipt_message_upload_fail_filesize', '檔案過大，請先壓縮檔案'), _L('global_go_home', '返回首頁'), '/', 'error', false);
    }
    if ($validate_token != $_REQUEST['validate_token']) {
        show_message(_L('order_receipt_message_upload_fail', '上傳失敗，請重新上傳'), _L('global_go_home', '返回首頁'), '/', 'error', false);
    }
    if (!file_exists($_FILES['pay_proof']['tmp_name']) || !is_uploaded_file($_FILES['pay_proof']['tmp_name'])) {
        show_message(_L('order_receipt_message_upload_fail', '上傳失敗，請重新上傳'), _L('global_go_home', '返回首頁'), '/', 'error', false);
    }
    $path = ROOT_PATH . 'data/order_receipt/';
    if (!file_exists($path)) {
        mkdir($path, 0777, true);
    }

    $pay_account = intval(trim($_REQUEST['pay_account']));
    $mime = mime_content_type($_FILES['pay_proof']['tmp_name']);
    $ext = explode("/", $mime)[1];
    if (!empty($record['files'])) {
        $file_count = count(explode(",", $record['files']));
    }
    $filename = $token . $file_count . (empty($ext) ? "" : ".$ext");
    $account_id = 0;
    $payment = payment_info($record['pay_id']);
    if (strpos($payment['pay_code'], 'bank') !== false && !empty($payment['pay_config_list']['banks'])) {
        if (!empty($payment['pay_config_list']['banks'][$pay_account])) {
            $account_id = $pay_account;
        }
    }

    unset($_SESSION['order_receipt_validate']);
    if ($_FILES['pay_proof']['error'] == UPLOAD_ERR_OK) {
        if (!preg_match('/(image\/(.*)|(.*)\/pdf)/', $mime)) {
            show_message(_L('order_receipt_message_invalid_format', '檔案格式不符，請重新上傳'), _L('global_go_home', '返回首頁'), '/');
        }
        if (move_upload_file($_FILES['pay_proof']['tmp_name'], $path . $filename)) {
            $files = empty($record['files']) ? $filename : "$record[files],$filename";
            $db->query("UPDATE " . $ecs->table('offline_payment') . " SET upload_time = CURRENT_TIMESTAMP, status = " . OFFLINE_PAYTMENT_UPLOADED . ", files = '$files', pay_account = $account_id WHERE token = '$token'");
            show_message(_L('order_receipt_message_upload_success', '上傳成功，客服將會儘快處理你的訂單'), _L('global_go_home', '返回首頁'), '/');
        }
    }

    // 上傳失敗
    show_message(_L('order_receipt_message_upload_fail', '上傳失敗，請重新上傳'), _L('global_go_home', '返回首頁'), '/', 'error', false);
}

function validateToken($token)
{
    global $db, $ecs;
    $record = $db->getRow("SELECT op.*, oi.order_sn, oi.order_status, oi.pay_status FROM " . $ecs->table("offline_payment") . " op LEFT JOIN " . $ecs->table("order_info") . " oi ON oi.order_id = op.order_id WHERE token = '$token'");

    if (empty($record)) {
        show_message(_L('order_receipt_message_invalid_link', '你輸入了無效的連結'), _L('global_go_home', '返回首頁'), '/', 'error', false);
    }
    $user_id    = $record['user_id'];
    $order_id   = $record['order_id'];
    $status     = intval($record['status']);
    $expire     = intval($record['expire_time']);

    if ($status == OFFLINE_PAYTMENT_UPLOADED) {
        show_message(_L('order_receipt_message_uploaded', '訂單已上傳證明'), _L('global_go_home', '返回首頁'), '/', 'error', false);
    }
    if (md5("$user_id-$order_id-$status-$expire") != $token) {
        // 驗證失敗
        show_message(_L('order_receipt_message_invalid_link', '你輸入了無效的連結'), _L('global_go_home', '返回首頁'), '/', 'error', false);
    }
    if (!in_array($status, [OFFLINE_PAYTMENT_PENDING, OFFLINE_PAYTMENT_EXTRA_INFO]) || !in_array($record['order_status'], [OS_CONFIRMED, OS_UNCONFIRMED]) || !in_array($record['pay_status'], [PS_PAYING, PS_UNPAYED])) {
        // 連結失效
        show_message(_L('order_receipt_message_invalid_link', '你輸入了無效的連結'), _L('global_go_home', '返回首頁'), '/', 'error', false);
    }
    if (!empty($_SESSION['user_id']) && $_SESSION['user_id'] != $user_id) {
        // 人地既單
        show_message(_L('order_receipt_message_invalid_user', '無法存取其他用戶的訂單'), _L('global_go_home', '返回首頁'), '/', 'error', false);
    }
    if (gmtime() > $expire) {
        // 過期
        show_message(_L('order_receipt_message_expire_link', '此連結已過期！') . _L('order_receipt_message_require_login', '請先登入帳戶再上傳。'), _L('global_go_home', '返回首頁'), '/', 'error', false);
    }
    return $record;
}
?>