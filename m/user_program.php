<?php
/**
* YOHO User rank program page
* Anthony@YOHO 20180801
*/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

if ((DEBUG_MODE & 2) != 2)
{
    $smarty->caching = true;
}

$user_id = $_SESSION['user_id'];
$user_rank = $_SESSION['user_rank'];
$rank_points = $_SESSION['rank_points'];
/* 初始化分页信息 */
$program_code = empty($_REQUEST['pcode']) ? '' : str_replace("/", "", trim($_REQUEST['pcode']));
$_REQUEST['pcode'] = $program_code;
$userRankController = new Yoho\cms\Controller\UserRankController();
$page = !empty($_REQUEST['page']) && intval($_REQUEST['page']) > 0 ? intval($_REQUEST['page']) : 1;
$size = !empty($_CFG['page_size']) && intval($_CFG['page_size']) > 0 ? intval($_CFG['page_size']) : 10;
$cat_id = !empty($_REQUEST['cid']) && intval($_REQUEST['cid']) > 0 ? intval($_REQUEST['cid']) : 0;
$brand_id = !empty($_REQUEST['bid']) && intval($_REQUEST['bid']) > 0 ? intval($_REQUEST['bid']) : 0;
$price_min = isset($_REQUEST['price_min']) && intval($_REQUEST['price_min']) > 0 ? intval($_REQUEST['price_min']) : 0;
$price_max = isset($_REQUEST['price_max']) && intval($_REQUEST['price_max']) > 0 ? intval($_REQUEST['price_max']) : 0;
$special = (isset($_REQUEST['special'])) ? trim($_REQUEST['special']) : $_SESSION['user_rank_program_data'][$program_id];

/* 排序、显示方式以及类型 */
$default_display_type = $_CFG['show_order_type'] == '0' ? 'list' : ($_CFG['show_order_type'] == '1' ? 'grid' : 'text');
$default_sort_order_method = $_CFG['sort_order_method'] == '0' ? 'DESC' : 'ASC';
$default_sort_order_type   = $_CFG['sort_order_type'] == '0' ? 'last_update' : ($_CFG['sort_order_type'] == '1' ? 'shop_price' : 'goods_id');

$sort = (isset($_REQUEST['sort']) && in_array(trim(strtolower($_REQUEST['sort'])), array('goods_id', 'shop_price', 'last_update', 'shownum'))) ? trim($_REQUEST['sort']) : $default_sort_order_type;
$order = (isset($_REQUEST['order']) && in_array(trim(strtoupper($_REQUEST['order'])), array('ASC', 'DESC'))) ? trim($_REQUEST['order']) : $default_sort_order_method;
$display = (isset($_REQUEST['display']) && in_array(trim(strtolower($_REQUEST['display'])), array('list', 'grid', 'text'))) ? trim($_REQUEST['display']) : (isset($_COOKIE['m_display']) ? $_COOKIE['m_display'] : $default_display_type);
//$display  = in_array($display, array('list', 'grid', 'text')) ? $display : 'text';
$display = in_array($display, array('list', 'grid')) ? $display : 'grid';
setcookie('m_display', $display, gmtime() + 86400 * 7);

/* ------------------------------------------------------ */
//-- PROCESSOR
/* ------------------------------------------------------ */
$program_id = $db->getOne('SELECT program_id FROM'.$ecs->table('user_rank_program'). " where program_code = '$program_code' LIMIT 1");
if (empty($program_id)) {
    header('Location: /');
    exit;
}

$smarty->assign('data_dir', DATA_DIR);
$program = $db->getRow('SELECT urp.program_id, IFNULL(urpl.program_name, urp.program_name) as program_name, urp.verifiy_type, urp.special_code, urp.validate, urp.description '.
' FROM'.$ecs->table('user_rank_program').'as urp '.
' LEFT JOIN ' . $ecs->table('user_rank_program_lang') . " as urpl ON urpl.program_id = urp.program_id AND urpl.lang = '".$_CFG['lang'] ."' ".
" where program_code = '$program_code' LIMIT 1");
if($special || ($program['validate'] == 0 && ($program['verifiy_type'] == 2 || $program['verifiy_type'] == 3))) {
    $data = [];
    $data['pid']     = $program_id;
    $data['special'] = $special;

    if ($program['verifiy_type'] == 3){
        $input_foreign_id = "";
        $str_remark = "";
        if (isset($_REQUEST['input_foreign_id'])){
            $input_foreign_id = htmlentities(preg_replace('/\s+/', '', $_REQUEST['input_foreign_id']));
            $data['foreign_id'] = $input_foreign_id;
            if ($data['foreign_id'] == ""){
                $str_remark = $_LANG['user_rank_program_input_again'];
            }
        }
    }

    $_SESSION['user_rank_program_special']['code'] = $special;
    $_SESSION['user_rank_program_special']['url']  = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $reg_result = $userRankController->sumbitUserRegister($data);
    if ($reg_result['error'] == 1 && $reg_result['msg'] == 'user_rank_program_foreign_id_not_received'){
        $smarty->assign('program_id', $program_id);
        $smarty->assign('verifiy_type', $program['verifiy_type']);
        $smarty->assign('program_name', $program['program_name']);
        $smarty->assign('program_desc', $program['description']);
        $smarty->assign('remark', $str_remark);
        $CategoryController = new Yoho\cms\Controller\CategoryController();
        $t1 = $CategoryController->getTopCategories(0);
        $smarty->assign('t1', $t1);
        $smarty->assign('helps', get_shop_help()); 
        $smarty->assign('page_title', $program['program_name']); 
        $smarty->assign('ur_here',    $program['program_name']);
        $smarty->assign('noindex', 1);
        $smarty->assign('script_name', 'programs');
        assign_template();
        $smarty->display('user_program_insert_id.html');
        exit;
    }
    unset($data);
    $smarty->assign('noindex', 1);
}
/* 赋值固定内容 */
assign_template();
// 特殊會員認證
$now = strtotime(local_date('Y-m-d H:i:s'));
$sql = "SELECT urp.program_id, IFNULL(urpl.program_name, urp.program_name) as program_name, IFNULL(urpl.description, urp.description) as description, urp.theme_color, urp.status, urp.banner, urp.program_code, urp.verifiy_type, urp.special_code ".
" FROM " . $ecs->table("user_rank_program")." as urp ".
"LEFT JOIN " . $ecs->table('user_rank_program_lang') . " as urpl ON urpl.program_id = urp.program_id AND urpl.lang = '".$_CFG['lang'] ."' ".
" WHERE urp.program_id = ".$program_id;
$program = $db->getRow($sql);
$smarty->assign('program', $program);

$record = $db->getRow("SELECT * FROM " . $ecs->table("user_rank_program_application") . " WHERE program_id = $program_id AND user_id = $_SESSION[user_id]");
if (!empty($record)) {
    $stage = $record['stage'];
    $now = strtotime(local_date('Y-m-d H:i:s'));
    $expiry = strtotime($record['expiry']);
    if ($expiry < $now) {
        $stage = RANK_APPLICATION_DISAPPROVE;
        if($program['status'] == RANK_PROGRAM_END) {
            $stage = 5;
        }
    }
    if (empty($_SESSION['user_id'])) $stage = 6;
} else {
    $stage = -1;
    if(!in_array($program['status'], [RANK_PROGRAM_HIDE, RANK_PROGRAM_SHOW])) {
        header('Location: user.php');
        exit;
    }
}
$record['expiry'] = date("Y-m-d", strtotime($record['expiry']));
if (empty($_SESSION['user_id'])) {
    $_SESSION["back_act_from_event"] = "programs/$program[program_code]";
}
$smarty->assign('stage',  $stage);
$smarty->assign('record', $record);
$CategoryController = new Yoho\cms\Controller\CategoryController();
$t1 = $CategoryController->getTopCategories(0);
$smarty->assign('t1',              $t1);
$smarty->assign('helps', get_shop_help());              // 网店帮助

$sql = "SELECT rank_id FROM " . $ecs->table("user_rank_program") .
    " WHERE program_id = $program_id";
$rank_id = $db->getOne($sql);
$goods_list_info = $userRankController->getGoodsList($rank_id, $cat_id, $brand_id, $price_min, $price_max, $ext, $size, $page, $sort, $order);
$sort_names = array(
    'last_update' => _L('productlist_sort_last_update', '預設排序'),
    'shownum' => _L('productlist_sort_shownum', '銷量排序'),
    'goods_id' => _L('productlist_sort_goods_id', '上架時間'),
    'shop_price' => _L('productlist_sort_price', '價格排序')
);
$sort_name = isset($sort_names[$sort]) ? $sort_names[$sort] : _L('productlist_sort_by', '排序方式');
$smarty->assign('sort_name', $sort_name);

$new_order = $order == "DESC" ? "ASC" : "DESC";
$sort_links = array(
    'last_update' => ['url'=>build_uri('programs', array('cid' => $cat_id, 'sort' => 'last_update', 'order' => 'DESC', 'page' => $page,'bid'=>$brand_id, 'price_min'=>$price_min, 'price_max'=>$price_max, 'filter_attr'=>$filter_attr_str))],
    'shownum' => ['url'=>build_uri('programs', array('cid' => $cat_id, 'sort' => 'shownum', 'order' => 'DESC', 'page' => $page,'bid'=>$brand_id, 'price_min'=>$price_min, 'price_max'=>$price_max, 'filter_attr'=>$filter_attr_str))],
    'goods_id' => ['url'=>build_uri('programs', array('cid' => $cat_id, 'sort' => 'goods_id', 'order' => 'DESC', 'page' => $page,'bid'=>$brand_id, 'price_min'=>$price_min, 'price_max'=>$price_max, 'filter_attr'=>$filter_attr_str))],
    'shop_price' => ['url'=>build_uri('programs', array('cid' => $cat_id, 'sort' => 'shop_price', 'order' => 'DESC', 'page' => $page,'bid'=>$brand_id, 'price_min'=>$price_min, 'price_max'=>$price_max, 'filter_attr'=>$filter_attr_str))],
);
foreach ($sort_links as $key => $value) {
    if($sort == $key){
        $sort_links[$key]['order'] = $order;
        $sort_links[$key]['url']   = build_uri('programs', array('cid' => $cat_id, 'sort' => $key, 'order' => $new_order, 'page' => $page,'bid'=>$brand_id, 'price_min'=>$price_min, 'price_max'=>$price_max, 'filter_attr'=>$filter_attr_str));
    }
}

$goods_id_arr = array();
foreach($goods_list_info['goodslist'] as $val){
    $goods_id_arr[] = $val['goods_id'];
}
$goodsController = new Yoho\cms\Controller\GoodsController();
//get favourable list
$favourable_arr = $goodsController->product_list_get_favourable_info($_SESSION['user_rank'], $goods_id_arr);
//get package list
$package_arr = $goodsController->product_list_get_package_info($goods_id_arr);
//get topic list
$topic_arr = $goodsController->product_list_get_topic_info($goods_id_arr);

$smarty->assign('page_title', $program['program_name']);  // 页面标题
$smarty->assign('ur_here',    $program['program_name']);  // 当前位置
$smarty->assign('sort_links', $sort_links);
$smarty->assign('goods_list', $goods_list_info['goodslist']);
$smarty->assign('favourable_list', $favourable_arr);
$smarty->assign('package_list',    $package_arr);
$smarty->assign('topic_list',      $topic_arr);
$smarty->assign('script_name', 'programs');
$smarty->assign('program', $program);
$smarty->assign('display', $display);
$smarty->assign('sort', $sort);
$smarty->assign('order', $order);
$smarty->assign('rank_points', $rank_points);
unset($goods_list_info['left_cat_menu']['count']);
$smarty->assign('filter_cat_menu', $goods_list_info['left_cat_menu']);
$smarty->assign('left_cat_menu_count',$goods_list_info['left_cat_menu_count']);
$smarty->assign('brands',           $goods_list_info['brands']);
$smarty->assign('brand_id',        $goods_list_info['brand_id']);
$smarty->assign('is_program_page',        1);
// Price Filter
$smarty->assign('price_min', $price_min);
$smarty->assign('price_max', $price_max);

assign_pager('programs', $cat_id, $goods_list_info['count'], $size, $sort, $order, $page, $ext, $brand_id, $price_min, $price_max, $display, $is_promote); // 分页
assign_dynamic('programs'); // 动态内容


$smarty->display('user_program_goods.html');
