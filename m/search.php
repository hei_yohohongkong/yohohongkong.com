<?php

/**
 * ECSHOP 搜索程序
 * ============================================================================
 * * 版权所有 2005-2012 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liubo $
 * $Id: search.php 17217 2011-01-19 06:29:08Z liubo $
*/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

if (!function_exists("htmlspecialchars_decode"))
{
    function htmlspecialchars_decode($string, $quote_style = ENT_COMPAT)
    {
        return strtr($string, array_flip(get_html_translation_table(HTML_SPECIALCHARS, $quote_style)));
    }
}

if (!isSecure()) {
    $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: ' . $redirect);
    exit;
}

// check if set keywords parma, then 301 redirect with new link format
if (!empty($_REQUEST['keywords'])) {
    $build_parameter_url = '';
    foreach ($_REQUEST as $para_key => $para_value) {
        if ($para_key != 'keywords') {
            $build_parameter_url .= ($build_parameter_url == ''? '?' : '&' ) . $para_key.'='.$para_value;
        }
    }

    if (strpos($_SERVER['REQUEST_URI'], 'search.php?keywords=') !== false) {
        $_REQUEST['keywords'] = stripslashes($_REQUEST['keywords']);
        $temp_keyword = urlencode($_REQUEST['keywords']);
        $temp_keyword = str_replace("%2F","/",$temp_keyword);
        $redirect = 'https://' . $_SERVER['HTTP_HOST'] . '/keyword/'.$temp_keyword.$build_parameter_url;
        header('HTTP/1.1 301 Moved Permanently');
        header('Location: ' . $redirect);
        exit;
    }
}

/* if (empty($_GET['encode']))
{
    $string = array_merge($_GET, $_POST);
    if (get_magic_quotes_gpc())
    {
        require(dirname(__FILE__) . '/includes/lib_base.php');
        require(dirname(__FILE__) . '/includes/lib_common.php');

        $string = stripslashes_deep($string);
    }
    $string['search_encode_time'] = time();
    $string = str_replace('+', '%2b', base64_encode(serialize($string)));

    header("Location: search.php?encode=$string\n");

    exit;
}
else
{
    $string = base64_decode(trim($_GET['encode']));
    if ($string !== false)
    {
        $string = unserialize($string);
        if ($string !== false)
        {

            if (!empty($string['search_encode_time']))
            {
                if (time() > $string['search_encode_time'] + 2)
                {
                    define('INGORE_VISIT_STATS', true);
                }
            }
            else
            {
                define('INGORE_VISIT_STATS', true);
            }
        }
        else
        {
            $string = array();
        }
    }
    else
    {
        $string = array();
    }
}
*/
$attrbuteController = new Yoho\cms\Controller\AttributeController();
$goodsController = new Yoho\cms\Controller\GoodsController();
// howang: Decode encoded search in case our visitor used an old search link
if (!empty($_GET['encode']))
{
    $string = base64_decode(trim($_GET['encode']));
    if ($string !== false)
    {
        $string = unserialize($string);
        if ($string !== false)
        {
            if (isset($string['keywords']))
            {
                header('Location: /search.php?keywords=' . $string['keywords']);
                exit;
            }
        }
    }
    // Decode fail, just redirect with no keyword
    header('Location: /search.php');
    exit;
}

$search_request_count = isset($_CFG['search_request_count']) ? $_CFG['search_request_count'] : 10;
$search_request_interval = isset($_CFG['search_request_interval']) ? $_CFG['search_request_interval'] : 10;
if(throttlerequest($search_request_count,$search_request_interval)) {
    echo "<script>alert('搜尋過於频繁,请稍后再试,將返回首頁');document.location='./'</script>";
    exit;
}
//$_REQUEST = array_merge($_REQUEST, addslashes_deep($string));

$_REQUEST['act'] = !empty($_REQUEST['act']) ? trim($_REQUEST['act']) : '';


/*------------------------------------------------------ */
//-- 高级搜索
/*------------------------------------------------------ */
if ($_REQUEST['act'] == 'advanced_search')
{
    // for some reason the advanced_search templete is missing...
    // just redirect to homepage for now
    header('Location: /');
    exit;
}
/*------------------------------------------------------ */
//-- 搜索结果
/*------------------------------------------------------ */
else
{
    $_REQUEST['keywords']   = !empty($_REQUEST['keywords'])   ? trim($_REQUEST['keywords'])     : '';
    $_REQUEST['brand']      = !empty($_REQUEST['brand'])      ? intval($_REQUEST['brand'])      : 0;
    $_REQUEST['category']   = !empty($_REQUEST['category'])   ? intval($_REQUEST['category'])   : 0;
    $_REQUEST['price_min']  = !empty($_REQUEST['price_min'])  ? intval($_REQUEST['price_min'])  : 0;
    $_REQUEST['price_max']  = !empty($_REQUEST['price_max'])  ? intval($_REQUEST['price_max'])  : 0;
    $_REQUEST['goods_type'] = !empty($_REQUEST['goods_type']) ? intval($_REQUEST['goods_type']) : 0;
    $_REQUEST['sc_ds']      = !empty($_REQUEST['sc_ds']) ? intval($_REQUEST['sc_ds']) : 0;
    $_REQUEST['outstock']   = !empty($_REQUEST['outstock']) ? 1 : 0;

    $action = '';

    if (isset($_CFG['simple_search_enable']) && $_CFG['simple_search_enable'] == true) {
        $simple_search = true;
    } else {
        $simple_search = false;
    }

    // if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'form')
    // {
    //     /* 要显示高级搜索栏 */
    //     $adv_value['keywords']  = htmlspecialchars(stripcslashes($_REQUEST['keywords']));
    //     $adv_value['brand']     = $_REQUEST['brand'];
    //     $adv_value['min_price'] = $_REQUEST['min_price'];
    //     $adv_value['max_price'] = $_REQUEST['max_price'];
    //     $adv_value['category']  = $_REQUEST['category'];
    //
    //     $attributes = get_seachable_attributes($_REQUEST['goods_type']);
    //
    //     /* 将提交数据重新赋值 */
    //     foreach ($attributes['attr'] AS $key => $val)
    //     {
    //         if (!empty($_REQUEST['attr'][$val['id']]))
    //         {
    //             if ($val['type'] == 2)
    //             {
    //                 $attributes['attr'][$key]['value']['from'] = !empty($_REQUEST['attr'][$val['id']]['from']) ? htmlspecialchars(stripcslashes(trim($_REQUEST['attr'][$val['id']]['from']))) : '';
    //                 $attributes['attr'][$key]['value']['to']   = !empty($_REQUEST['attr'][$val['id']]['to'])   ? htmlspecialchars(stripcslashes(trim($_REQUEST['attr'][$val['id']]['to'])))   : '';
    //             }
    //             else
    //             {
    //                 $attributes['attr'][$key]['value'] = !empty($_REQUEST['attr'][$val['id']]) ? htmlspecialchars(stripcslashes(trim($_REQUEST['attr'][$val['id']]))) : '';
    //             }
    //         }
    //     }
    //     if ($_REQUEST['sc_ds'])
    //     {
    //         $smarty->assign('scck',            'checked');
    //     }
    //     $smarty->assign('adv_val',             $adv_value);
    //     $smarty->assign('goods_type_list',     $attributes['cate']);
    //     $smarty->assign('goods_attributes',    $attributes['attr']);
    //     $smarty->assign('goods_type_selected', $_REQUEST['goods_type']);
    //     $smarty->assign('cat_list',            cat_list(0, $adv_value['category'], true, 2, false));
    //     $smarty->assign('brand_list',          get_brand_list());
    //     $smarty->assign('action',              'form');
    //     $smarty->assign('use_storage',         $_CFG['use_storage']);
    //     $smarty->assign('script_name',         'search');//ly sbsn 2013 0308
    //     $action = 'form';
    // }

    /* 初始化搜索条件 */
    $keywords  = '';
    $tag_where = '';
    $filter_attr_str = isset($_REQUEST['filter_attr']) ? htmlspecialchars(trim($_REQUEST['filter_attr'])) : '0';
    $filter_attr_str = trim(urldecode($filter_attr_str));
    $filter_attr_str = preg_match('/^[\d\.]+$/', $filter_attr_str) ? $filter_attr_str : '';
    $filter_attr = empty($filter_attr_str) ? '' : explode('.', $filter_attr_str);
    if (!empty($_REQUEST['keywords']))
    {
        $arr = array();
        if (stristr($_REQUEST['keywords'], ' AND ') !== false)
        {
            /* 检查关键字中是否有AND，如果存在就是并 */
            $arr        = explode('AND', $_REQUEST['keywords']);
            $operator   = " AND ";
        }
        elseif (stristr($_REQUEST['keywords'], ' OR ') !== false)
        {
            /* 检查关键字中是否有OR，如果存在就是或 */
            $arr        = explode('OR', $_REQUEST['keywords']);
            $operator   = " OR ";
        }
        elseif (stristr($_REQUEST['keywords'], ' + ') !== false)
        {
            /* 检查关键字中是否有加号，如果存在就是或 */
            $arr        = explode('+', $_REQUEST['keywords']);
            $operator   = " OR ";
        }
        else
        {
            //* 检查关键字中是否有空格，如果存在就是并 */
            $arr  = explode(' ', $_REQUEST['keywords']);
            if (sizeof($arr) == 1) {
                $arr = $goodsController->cutWords($_REQUEST['keywords']);
            }
//
//            $arr        = explode(' ', $_REQUEST['keywords']);
            $operator   = " AND ";

            if (!empty($temp_keywords)) {
                $_REQUEST['keywords'] = $temp_keywords;
            }
        }
        $cb_operator = ' OR ';
        $keywords = 'AND ((';
        $keywords_cat_brands = 'AND ((';
        $localized_keyword = "OR g.goods_id IN (SELECT gl.goods_id FROM " . $ecs->table('goods_lang') . " as gl WHERE (";
        $tagController = new Yoho\cms\Controller\TagController();
        $goods_ids = $tagController->get_goods_id_by_tag($_REQUEST['keywords']);
        foreach ($arr AS $key => $val)
        {
            // IF have Simplified Chinese, change to Traditional Chinese
            $tmp = iconv('UTF-8', 'GB2312', $val);
            if($tmp){
                $val = iconv('GB2312', 'BIG5', $tmp);
                $val = iconv('BIG5', 'UTF-8',$val);
            }

            if ($key > 0 && $key < count($arr) && count($arr) > 1)
            {
                $keywords .= $operator;
                $keywords_cat_brands .= $cb_operator;
                $localized_keyword .= $operator;
            }
            $val_html   = mysql_like_quote(htmlspecialchars(trim($val)));
            $val        = mysql_like_quote(trim($val));
            $sc_dsad    = $_REQUEST['sc_ds'] ? " OR g.goods_desc LIKE '%$val_html%'" : '';

            if ($simple_search == true) {
                $keywords  .= "(g.goods_name LIKE '%$val%')";
            } else {
                $keywords  .= "(g.goods_name LIKE '%$val%' OR g.goods_sn LIKE '%$val%' OR g.keywords LIKE '%$val%' $sc_dsad)";
            }

            $keywords_cat_brands  .= "(gb.brand_name LIKE '%$val%' OR gc.cat_name LIKE '%$val%' OR gc.cat_desc LIKE '%$val%' OR gbl.brand_name LIKE '%$val%' OR gcl.cat_name LIKE '%$val%')";
            $localized_keyword .= "(gl.goods_name LIKE '%$val%' OR gl.keywords LIKE '%$val%')";

            $db->autoReplace($ecs->table('keywords'), array('date' => local_date('Y-m-d'),
                'searchengine' => 'ecshop', 'keyword' => addslashes(str_replace('%', '', $val)), 'count' => 1), array('count' => 1));
        }
        $keywords .= ')' . $localized_keyword . ") AND gl.lang = '" . $_CFG['lang'] . "')" . ')';
        $keywords_cat_brands .= '))';

        // get goods id query;
        $sql = "SELECT g.goods_id FROM ".   $ecs->table('goods') . " as g WHERE 1  ".$keywords."  ";
        $goods_id_localized = $db->getCol($sql);

        $goods_ids = array_unique($goods_ids);
        $tag_where = implode(',', $goods_ids);
        if (!empty($tag_where))
        {
            $tag_where = 'OR g.goods_id ' . db_create_in($tag_where);
        }
    } elseif(empty($_REQUEST['keywords']) && empty($_REQUEST['intro'])) { // We go back to homepage when no keywords.
        header('Location: /');
    }

    $category   = !empty($_REQUEST['category']) ? intval($_REQUEST['category'])        : 0;
    $categories = ($category > 0)               ? ' AND ' . get_children($category)    : '';
    $brand      = $_REQUEST['brand']            ? " AND brand_id = '$_REQUEST[brand]'" : '';
    $outstock   = !empty($_REQUEST['outstock']) ? " AND g.goods_number > 0 "           : '';

    $min_price  = $_REQUEST['price_min'] != 0                               ? " AND g.shop_price >= '$_REQUEST[price_min]'" : '';
    $max_price  = $_REQUEST['price_max'] != 0 || $_REQUEST['price_min'] < 0 ? " AND g.shop_price <= '$_REQUEST[price_max]'" : '';

    /* 排序、显示方式以及类型 */
    $default_display_type = $_CFG['show_order_type'] == '1' ? 'list' : ($_CFG['show_order_type'] == '0' ? 'grid' : 'text');
    $default_sort_order_method = $_CFG['sort_order_method'] == '0' ? 'DESC' : 'ASC';
    $default_sort_order_type   = $_CFG['sort_order_type'] == '0' ? 'last_update' : ($_CFG['sort_order_type'] == '1' ? 'shop_price' : 'goods_id');

    $sort  = (isset($_REQUEST['sort'])  && in_array(trim(strtolower($_REQUEST['sort'])), array('goods_id', 'shop_price', 'last_update','shownum'))) ? trim($_REQUEST['sort'])  : $default_sort_order_type;
    $order = (isset($_REQUEST['order']) && in_array(trim(strtoupper($_REQUEST['order'])), array('ASC', 'DESC')))                              ? trim($_REQUEST['order']) : $default_sort_order_method;
    $display  = (isset($_REQUEST['display']) && in_array(trim(strtolower($_REQUEST['display'])), array('list', 'grid', 'text'))) ? trim($_REQUEST['display'])  : (isset($_COOKIE['m_display']) ? $_COOKIE['m_display'] : $default_display_type);
    $display  = in_array($display, array('list', 'grid', 'text')) ? $display : 'text';
    $is_promote = intval($_REQUEST['is_promote']) > 0 ? $_REQUEST['is_promote'] : 0;  //ly sbsn 130303只看促销
    $_SESSION['display_search'] = $display;

/*------------------------------------------------------ */


    $page       = !empty($_REQUEST['page'])  && intval($_REQUEST['page'])  > 0 ? intval($_REQUEST['page'])  : 1;
    $size       = !empty($_CFG['page_size']) && intval($_CFG['page_size']) > 0 ? intval($_CFG['page_size']) : 10;

    $intromode = '';    //方式，用于决定搜索结果页标题图片

    if (!empty($_REQUEST['intro']))
    {
        switch ($_REQUEST['intro'])
        {
            case 'best':
                $intro   = ' AND g.is_best = 1';
                $intromode = 'best';
                $ur_here = _L('search_best_goods', '店長推薦');
                break;
            case 'new':
                $intro   = ' AND g.is_new = 1';
                $intromode ='new';
                $ur_here = _L('search_new_goods', '新產品');
                break;
            case 'comingsoon':
                $intro   = ' AND g.is_pre_sale = 1';
                $intromode ='comingsoon';
                $ur_here = _L('search_pre_sale_goods', '預售產品');
                break;
            case 'hot':
                $intro   = ' AND g.is_hot = 1';
                $intromode = 'hot';
                $ur_here = _L('search_hot_goods', '熱賣產品');
                break;
            case 'promotion':
                $time    = gmtime();
                $intro   = " AND g.promote_price > 0 AND g.promote_start_date <= '$time' AND g.promote_end_date >= '$time'";
                $intromode = 'promotion';
                $ur_here = _L('search_promotion_goods', '限時促銷');
                break;
            default:
                $intro   = '';
        }
    }
    else
    {
        $intro = '';
    }

    if (empty($ur_here))
    {
        $ur_here = _L('search_search_goods', '產品搜尋');
    }

    /*------------------------------------------------------ */
    //-- 属性检索
    /*------------------------------------------------------ */
    $attr_in  = '';
    $attr_num = 0;
    $attr_url = '';
    $attr_arg = array();

    if (!empty($_REQUEST['attr']))
    {
        $sql = "SELECT goods_id, COUNT(*) AS num FROM " . $ecs->table("goods_attr") . " WHERE 0 ";
        foreach ($_REQUEST['attr'] AS $key => $val)
        {
            if (is_not_null($val) && is_numeric($key))
            {
                $attr_num++;
                $sql .= " OR (1 ";

                if (is_array($val))
                {
                    $sql .= " AND attr_id = '$key'";

                    if (!empty($val['from']))
                    {
                        $sql .= is_numeric($val['from']) ? " AND attr_value >= " . floatval($val['from'])  : " AND attr_value >= '$val[from]'";
                        $attr_arg["attr[$key][from]"] = $val['from'];
                        $attr_url .= "&amp;attr[$key][from]=$val[from]";
                    }

                    if (!empty($val['to']))
                    {
                        $sql .= is_numeric($val['to']) ? " AND attr_value <= " . floatval($val['to']) : " AND attr_value <= '$val[to]'";
                        $attr_arg["attr[$key][to]"] = $val['to'];
                        $attr_url .= "&amp;attr[$key][to]=$val[to]";
                    }
                }
                else
                {
                    /* 处理选购中心过来的链接 */
                    $sql .= isset($_REQUEST['pickout']) ? " AND attr_id = '$key' AND attr_value = '" . $val . "' " : " AND attr_id = '$key' AND attr_value LIKE '%" . mysql_like_quote($val) . "%' ";
                    $attr_url .= "&amp;attr[$key]=$val";
                    $attr_arg["attr[$key]"] = $val;
                }

                $sql .= ')';
            }
        }

        /* 如果检索条件都是无效的，就不用检索

         */



        if ($attr_num > 0)
        {
            $sql .= " GROUP BY goods_id HAVING num = '$attr_num'";

            $row = $db->getCol($sql);
            if (count($row))
            {
                $attr_in = " AND " . db_create_in($row, 'g.goods_id');
            }
            else
            {
                $attr_in = " AND 0 ";
            }
        }
    }
    elseif (isset($_REQUEST['pickout']))
    {
        /* 从选购中心进入的链接 */
        $sql = "SELECT DISTINCT(goods_id) FROM " . $ecs->table('goods_attr');
        $col = $db->getCol($sql);
        //如果商店没有设置商品属性,那么此检索条件是无效的
        if (!empty($col))
        {
            $attr_in = " AND " . db_create_in($col, 'g.goods_id');
        }
    }

    if($is_promote > 0)
    {
        $wherep = " AND g.is_promote = 1 ";
    }

    $goods_tag_where = $tag_where;
    if(!empty($brand)){
        $goods_tag_where .=  " AND g.brand_id = '$_REQUEST[brand]'";
    }
    if(!empty(categories)){
        $goods_tag_where .= " $categories";
    }
    /* 分页 */
    $search_parameters = array(
        'keywords'    => stripslashes($_REQUEST['keywords']),
        'category'    => $category,
        'brand'       => $_REQUEST['brand'],
        'sort'        => $sort,
        'order'       => $order,
        'price_min'   => $_REQUEST['price_min'],
        'price_max'   => $_REQUEST['price_max'],
        'action'      => $action,
        'intro'       => empty($intromode) ? '' : trim($intromode),
        'goods_type'  => $_REQUEST['goods_type'],
        'sc_ds'       => $_REQUEST['sc_ds'],
        'outstock'    => $_REQUEST['outstock'],
        'filter_attr' => $_REQUEST['filter_attr']
    );
    $search_parameters = array_merge($search_parameters, $attr_arg);
    /* Filter Attribute */
    $attr_data = $attrbuteController->get_attr_data($category, $filter_attr, 'search', $search_parameters);
    $attr_goods = $attr_data['ext']; //商品查询条件扩展
    $attr_goods_list = $attr_data['ext_goods'];
    $all_attr_list = $attr_data['all_att戈1r_list'];
    /* 获得符合条件的商品总数 */
    $sql   = "SELECT COUNT(*) FROM " .$ecs->table('goods'). " AS g ".
        "LEFT JOIN  " . $ecs->table('member_price'). " AS mp ON g.goods_id = mp.goods_id AND mp.user_rank = $_SESSION[user_rank] ".
        "WHERE g.is_delete = 0 AND g.is_hidden = 0 AND g.is_on_sale = 1 AND g.is_alone_sale = 1 $wherep $attr_in ".  //判断总数  $where
        "AND (( 1 " . $categories . $keywords . $brand . $min_price . $max_price . $intro . $outstock .$attr_goods." ) ".$goods_tag_where." )";
    $count = $db->getOne($sql);
    if($count == 0 && empty($_REQUEST['brand']) && empty($attr_goods_list)) {
        // try to search by brand / cat, but exclude cat menu, filter and brand
        $kcbsql = 'SELECT DISTINCT g.goods_id FROM ' . $ecs->table('goods') . " as g ".
            "LEFT JOIN ". $ecs->table('category') ." as gc on gc.cat_id = g.cat_id and gc.is_show = 1 ".
            "LEFT JOIN ". $ecs->table('brand') ." as gb on gb.brand_id = g.brand_id and gb.is_show = 1 ".
            "LEFT JOIN ". $ecs->table('category_lang') ." as gcl on gcl.cat_id = g.cat_id ".
            "LEFT JOIN ". $ecs->table('brand_lang') ." as gbl on gbl.brand_id = g.brand_id ".
            " WHERE 1 ".$keywords_cat_brands;
        $kcb_goods_list = $db->getCol($kcbsql);
        if(!empty($kcb_goods_list)) {
            $guess_result = 1;
            $smarty->assign('guess_result',      $guess_result);
            $kcb_goods_list = array_unique($kcb_goods_list);
            $tag_where = implode(',', $kcb_goods_list);
            if (!empty($tag_where))
            {
                $tag_where = 'OR g.goods_id ' . db_create_in($tag_where);
            }
            $goods_tag_where = $tag_where;
            if(!empty($brand)){
                $goods_tag_where .=  " AND g.brand_id = '$_REQUEST[brand]'";
            }
            if(!empty($categories)){
                $goods_tag_where .= " $categories";
            }
        }
        /* 获得符合条件的商品总数 */
        $sql   = "SELECT COUNT(*) FROM " .$ecs->table('goods'). " AS g ".
            "LEFT JOIN  " . $ecs->table('member_price'). " AS mp ON g.goods_id = mp.goods_id AND mp.user_rank = $_SESSION[user_rank] ".
            "WHERE g.is_delete = 0 AND g.is_hidden = 0 AND g.is_on_sale = 1 AND g.is_alone_sale = 1 $wherep $attr_in ".  //判断总数  $where
            "AND (( 1 " . $categories . $keywords . $brand . $min_price . $max_price . $intro . $outstock .$attr_goods." ) ".$goods_tag_where." )";
        $count = $db->getOne($sql);
    }

    $max_page = ($count> 0) ? ceil($count / $size) : 1;
    if ($page > $max_page)
    {
        $page = $max_page;
    }

    /* 查询商品 */
    $sql_where = "g.is_delete = 0 AND g.is_hidden = 0 AND g.is_on_sale = 1 AND g.is_alone_sale = 1 $wherep $attr_in ".
        "AND (( 1 " . $categories . $keywords . $brand . $min_price . $max_price . $intro . $outstock  .$attr_goods. " ) ".$goods_tag_where." ) ";
    $sql = hw_goods_list_sql($sql_where, $sort, $order, $size, ($page - 1) * $size, 0);
    $res = $db->getAll($sql);
    $arr = hw_process_goods_rows($res);
    $goods_id_arr = array();
    foreach($arr as $val){
        $goods_id_arr[] = $val['goods_id'];
    }

    //get favourable list
    $favourable_arr = $goodsController->product_list_get_favourable_info($_SESSION['user_rank'], $goods_id_arr);

    //get package list
    $package_arr = $goodsController->product_list_get_package_info($goods_id_arr);

    //get topic list
    $topic_arr = $goodsController->product_list_get_topic_info($goods_id_arr);

    $smarty->assign('goods_list',      $arr);
    $smarty->assign('favourable_list', $favourable_arr);
    $smarty->assign('package_list',    $package_arr);
    $smarty->assign('topic_list',      $topic_arr);
    $smarty->assign('category',        $category);
    $smarty->assign('keywords',        htmlspecialchars(stripslashes($_REQUEST['keywords'])));
    $smarty->assign('is_search',       true);
    $smarty->assign('search_keywords', stripslashes(htmlspecialchars_decode($_REQUEST['keywords'])));
    $smarty->assign('brand',           $_REQUEST['brand']);
    $smarty->assign('outstock',        $_REQUEST['outstock']);
    $smarty->assign('script_name',     'search');//ly sbsn 2013 0308
    $categoryController = new Yoho\cms\Controller\CategoryController();
    $t1 = $categoryController->getTopCategories(0);
    $smarty->assign('t1',              $t1);
    $smarty->assign('sxb',             $sxb);// 分类ly sbsn  20130311
    $yohoController = new Yoho\cms\Controller\YohoBaseController();
    $canonical_url  = $yohoController->getFullUrl('/keyword/'.$_REQUEST['keywords']);
    $smarty->assign('canonical_url', $canonical_url);
    $smarty->assign('filter_attr',      $filter_attr_str);
    $smarty->assign('filter_attr_list',  $all_attr_list);

    // Filter Category Menu
    $goods_id_localized_sql = '';
    if (!empty($goods_id_localized)) {
       $goods_id_localized_sql = ' and g.goods_id '.db_create_in($goods_id_localized) ;
    }
    $category_menu_where = " g.is_delete = 0 AND g.is_hidden = 0 AND g.is_on_sale = 1 AND g.is_alone_sale = 1 $wherep $attr_in " . //判断总数  $where
    "AND (( 1 " . $goods_id_localized_sql . $min_price . $max_price . $intro . $outstock . " ) " . $tag_where . " ) ";
    $left_menu = $categoryController->getLeftCatMenu($category, "search", $category_menu_where, 0, $search_parameters);
    
    if($category > 0){
        $first_parent = array_reverse($categoryController->getParentLevel($category));
        $smarty->assign('first_parent_cat',$first_parent[0]['cat_id']);
    }
    unset($left_menu['count']);
    $smarty->assign('filter_cat_menu',$left_menu);
    $category_name = get_cat_info($category)['cat_name'];
    $smarty->assign('category_name',$category_name);

    // Brands Filter
    $brand_sql = "SELECT b.brand_logo, b.brand_id, IFNULL(bl.brand_name, b.brand_name) as brand_name, count(b.brand_id) as num " .
        "FROM " . $ecs->table('goods') . " AS g " .
        "LEFT JOIN " . $ecs->table('brand') . " AS b ON g.brand_id = b.brand_id " .
        "LEFT JOIN  " . $ecs->table('member_price'). " AS mp ON g.goods_id = mp.goods_id AND mp.user_rank = $_SESSION[user_rank] ".
        "LEFT JOIN " . $ecs->table('brand_lang') . "as bl ON bl.brand_id = b.brand_id AND bl.lang = '" . $_CFG['lang'] . "' " .
        "WHERE g.is_delete = 0 AND g.is_hidden = 0 AND g.is_on_sale = 1 AND g.is_alone_sale = 1 $wherep $attr_in " . //判断总数  $where
        "AND (( 1 " . $goods_id_localized_sql . $categories . $min_price . $max_price . $intro . $outstock . " ) " . $tag_where . " ) " .
        "AND b.is_show = 1 " .
        "GROUP BY b.brand_id ORDER BY b.sort_order ASC, b.brand_id ASC";
    $brands = $db->getAll($brand_sql);
    foreach ($brands AS $key => $val)
    {
        $brands[$key]['brand_name'] = $val['brand_name'];
        $brands[$key]['brand_id']   = $val['brand_id'];
        $brands[$key]['brand_logo'] = empty($val['brand_logo']) ? null: '/' . DATA_DIR . '/brandlogo/' . $val['brand_logo'];
        $brands[$key]['url'] = '/search.php?' . http_build_query(array_merge($search_parameters, array('brand' => $val['brand_id'])));

        /* 判断品牌是否被选中 */
        if ($_REQUEST['brand'] == $val['brand_id'])
        {
            $brands[$key]['selected'] = 1;
            $brands[$key]['url'] = '/search.php?' . http_build_query(array_merge($search_parameters, array('brand' => 0)));
        }
        else
        {
            $brands[$key]['selected'] = 0;
        }
    }
    
    $tagController = new Yoho\cms\Controller\TagController();
    $keyword = stripslashes(htmlspecialchars_decode($_REQUEST['keywords']));
    $i18nKeyword = $tagController->getTagI18nByKeyword($keyword);
    $keywords_desc = $i18nKeyword[$_CFG['lang']] . ( $i18nKeyword[$_CFG['lang']] == $i18nKeyword['en_us'] ? '' :  (!empty($i18nKeyword['en_us'])? ' ' . $i18nKeyword['en_us'] :'' ) ) ;
    $description =  sprintf(_L('keyword_description'), $keywords_desc );
    $smarty->assign('brands',           $brands);
    $smarty->assign('description',      $description);

    // Sort order
    $sort_names = array(
        'last_update' => _L('productlist_sort_last_update', '預設排序'),
        'shownum' => _L('productlist_sort_shownum', '銷量排序'),
        'goods_id' => _L('productlist_sort_goods_id', '上架時間'),
        'shop_price' => _L('productlist_sort_price', '價格排序')
    );
    $sort_name = isset($sort_names[$sort]) ? $sort_names[$sort] : _L('productlist_sort_by', '排序方式');
    $smarty->assign('sort_name', $sort_name);
    $new_order = $order == "DESC" ? "ASC" : "DESC";
    $sort_links = array(
        'last_update' => ['url'=>'/search.php?' . http_build_query(array_merge($search_parameters, array('sort' => 'last_update', 'order' => 'DESC')))],
        'shownum' => ['url'=>'/search.php?' . http_build_query(array_merge($search_parameters, array('sort' => 'shownum', 'order' => 'DESC')))],
        'goods_id' => ['url'=>'/search.php?' . http_build_query(array_merge($search_parameters, array('sort' => 'goods_id', 'order' => 'DESC')))],
        'shop_price' => ['url'=>'/search.php?' . http_build_query(array_merge($search_parameters, array('sort' => 'shop_price', 'order' => 'DESC')))],
    );
    foreach ($sort_links as $key => $value) {
        if($sort == $key){
            $sort_links[$key]['order'] = $order;
            $sort_links[$key]['url']   = '/search.php?' . http_build_query(array_merge($search_parameters, array('sort' => $key, 'order' => $new_order)));
        }
    }
    $smarty->assign('sort_links', $sort_links);
    hw_assign_price_range("g.is_delete = 0 AND g.is_hidden = 0 AND g.is_on_sale = 1 AND g.is_alone_sale = 1 $wherep $attr_in AND (( 1 " . $categories . $goods_id_localized_sql . $brand . $intro . $outstock . " ) " . $tag_where . " )", $_SESSION['user_rank']);
    $pager['search'] = $search_parameters;
    $pager = get_pager('search.php', $pager['search'], $count, $page, $size);
    $pager['display'] = $display;

    $smarty->assign('pager', $pager);

    assign_template();
    assign_dynamic('search');
    $position = assign_ur_here(0, $ur_here . ($_REQUEST['keywords'] ? ' - ' . $_REQUEST['keywords'] : ''));
    $smarty->assign('page_title', $position['title']);    // 页面标题
    $smarty->assign('ur_here',    $position['ur_here']);  // 当前位置
    $smarty->assign('intromode',      $intromode);
    // $smarty->assign('categories', get_categories_tree()); // 分类树
    $smarty->assign('helps',       get_shop_help());      // 网店帮助
    // $smarty->assign('top_goods',  get_top10());           // 销售排行
    // $smarty->assign('promotion_info', get_promotion_info());
    $smarty->assign('sort', $sort);//ly sbsn 2013 0308
    $smarty->assign('order', $order );//ly sbsn 2013 0308
    $smarty->assign('display', $display);//ly sbsn 2013 0308
    $smarty->assign('script_name', 'search');//ly sbsn 2013 0308
    $smarty->assign('is_promote', $is_promote);//ly sbsn 2013 0308

    $smarty->assign('brand_id',        $_REQUEST['brand']);
    $smarty->assign('category_id',     $category);
    $smarty->assign('price_min',       $_REQUEST['price_min']);
    $smarty->assign('price_max',       $_REQUEST['price_max']);
      
    $smarty->display('search.html');
}

/*------------------------------------------------------ */
//-- PRIVATE FUNCTION
/*------------------------------------------------------ */
/**
 *
 *
 * @access public
 * @param
 *
 * @return void
 */
function is_not_null($value)
{
    if (is_array($value))
    {
        return (!empty($value['from'])) || (!empty($value['to']));
    }
    else
    {
        return !empty($value);
    }
}

/**
 * 获得可以检索的属性
 *
 * @access  public
 * @params  integer $cat_id
 * @return  void
 */
function get_seachable_attributes($cat_id = 0)
{
    $attributes = array(
        'cate' => array(),
        'attr' => array()
    );

    /* 获得可用的商品类型 */
    $sql = "SELECT t.cat_id, cat_name FROM " .$GLOBALS['ecs']->table('goods_type'). " AS t, ".
           $GLOBALS['ecs']->table('attribute') ." AS a".
           " WHERE t.cat_id = a.cat_id AND t.enabled = 1 AND a.attr_index > 0 ";
    $cat = $GLOBALS['db']->getAll($sql);

    /* 获取可以检索的属性 */
    if (!empty($cat))
    {
        foreach ($cat AS $val)
        {
            $attributes['cate'][$val['cat_id']] = $val['cat_name'];
        }
        $where = $cat_id > 0 ? ' AND a.cat_id = ' . $cat_id : " AND a.cat_id = " . $cat[0]['cat_id'];

        $sql = 'SELECT attr_id, attr_name, attr_input_type, attr_type, attr_values, attr_index, sort_order ' .
               ' FROM ' . $GLOBALS['ecs']->table('attribute') . ' AS a ' .
               ' WHERE a.attr_index > 0 ' .$where.
               ' ORDER BY cat_id, sort_order ASC';
        $res = $GLOBALS['db']->query($sql);

        while ($row = $GLOBALS['db']->FetchRow($res))
        {
            if ($row['attr_index'] == 1 && $row['attr_input_type'] == 1)
            {
                $row['attr_values'] = str_replace("\r", '', $row['attr_values']);
                $options = explode("\n", $row['attr_values']);

                $attr_value = array();
                foreach ($options AS $opt)
                {
                    $attr_value[$opt] = $opt;
                }
                $attributes['attr'][] = array(
                    'id'      => $row['attr_id'],
                    'attr'    => $row['attr_name'],
                    'options' => $attr_value,
                    'type'    => 3
                );
            }
            else
            {
                $attributes['attr'][] = array(
                    'id'   => $row['attr_id'],
                    'attr' => $row['attr_name'],
                    'type' => $row['attr_index']
                );
            }
        }
    }

    return $attributes;
}
/**
 * 获得分类的信息
 *
 * @param   integer $cat_id
 *
 * @return  void
 */
 function get_cat_info($cat_id)
 {
     return $GLOBALS['db']->getRow("SELECT " .
         "IFNULL(cl.cat_name, c.cat_name) as cat_name, " .
         "IFNULL(cl.keywords, c.keywords) as keywords, " .
         "IFNULL(cl.cat_desc, c.cat_desc) as cat_desc, " .
         "style, grade, filter_attr, parent_id " .
         "FROM " . $GLOBALS['ecs']->table('category') . " as c " .
         "LEFT JOIN" . $GLOBALS['ecs']->table('category_lang') . " as cl ON c.cat_id = cl.cat_id AND cl.lang = '" . $GLOBALS['_CFG']['lang']  . "' " .
         " WHERE c.cat_id = '$cat_id'");
 }

?>
