<?php
define('IN_ECS', true);

require_once dirname(__FILE__) . '/includes/init.php';
require_once ROOT_PATH . '/includes/lib_order.php';

if ((DEBUG_MODE & 2) != 2) {
    $smarty->caching = true;
}

// Online free sms receive
$invalid_phones = ["64522664", "64522656", "64522329", "64522937"];

$user_id = $_SESSION['user_id'];

if (empty($user_id)) {
    $logged_in = 0; 
    $_SESSION["back_act_from_event"] = 'phonenumber';
} else {
    $logged_in = 1;
    unset($_SESSION["back_act_from_event"]);
}

$now = gmtime();
$event_config = json_decode($_CFG['phonenumber_config'], true);
$valid_coupons = $event_config['coupons'];
$verify_start = local_strtotime($event_config['start_time']);
$verify_end = local_strtotime($event_config['end_time']);

$event_status = 0;
$is_gift_enough = 1;
if ($now < $verify_start || empty($_CFG['send_verify_sms'])) {
} elseif ($now < $verify_end) {
    $event_status = 1;
} else {
    $event_status = 2;
}

$verified = 0;
if (!empty($_REQUEST['act'])) {
    $event_sql = "FROM " . $ecs->table('phone_number_log') . " WHERE user_id = $user_id AND status = 1 AND create_time BETWEEN $verify_start AND $verify_end";
    $affiliate_sql = "FROM " . $ecs->table('verify_code') . " WHERE user_id = $user_id AND status = " . VERIFY_CODE_FINISH . " AND verify_type = '" . Yoho\cms\Controller\YohoBaseController::VERIFY_TYPE_SMS .  "' AND create_at BETWEEN $verify_start AND $verify_end AND verify_target REGEXP '^\\\+852-([0-9]{8})' AND verify_target LIKE '%6%'";
    if ($db->getOne("SELECT COUNT(*) $event_sql")) {
        $verify_logged = 1;
        $verified = 1;
        $log = $db->getRow("SELECT * $event_sql ORDER BY create_time DESC LIMIT 1");
    } elseif ($db->getOne("SELECT COUNT(*) $affiliate_sql")) {
        $verified = 1;
        $log = $db->getRow("SELECT * $affiliate_sql ORDER BY create_at DESC LIMIT 1");
        $affiliate_coupon_key = substr_count(explode("-", $log['verify_target'])[1], '6');
        if ($db->getOne("SELECT COUNT(*) FROM " . $ecs->table('phone_number_log') . " WHERE phone = '$log[verify_target]' AND status = 1 AND create_time BETWEEN $verify_start AND $verify_end") || empty($affiliate_coupon_key)) {
            $verified = 0;
        }
    }
}

$remains = 0;
$verified_coupon_key = -1;
foreach ($valid_coupons as $key => $coupon) {
    $users = json_decode($db->getOne("SELECT users_list FROM " . $ecs->table('coupons') . " WHERE coupon_id = $coupon[coupon_id]", true));

    if (in_array($user_id, $users)) {
        if (!$verified) {
            $verified = 1;
        }
        $verified_coupon_key = $key;
    }

    $remain = intval($coupon['limit']) - count($users) + 1;
    $valid_coupons[$key]['remain'] = max($remain, 0);
    $remains += $remain;
}

if ($verified && !$verify_logged && !empty($affiliate_coupon_key)) {
    while ($valid_coupons[$affiliate_coupon_key]['remain'] <= 0 && $affiliate_coupon_key > 0) {
        $affiliate_coupon_key--;
    }
    if (empty($affiliate_coupon_key)) {
        $verified = 0;
    }
}

if ($remains <= 0) {
    $is_gift_enough = 0;
}

if ($_REQUEST['act'] == 'verify') {
    if (!$logged_in || $event_status != 1) {
        header("Location: /phonenumber");
        exit;
    } elseif ($verified) {
        header("Location: /phonenumber-confirm");
        exit;
    } elseif (!$is_gift_enough) {
        header("Location: /phonenumber");
        exit;
    } else {
        $sent_phone = $db->getRow("SELECT phone, create_time FROM " . $ecs->table('phone_number_log') . " WHERE user_id = $user_id AND status = 0 AND create_time + 180 > $now ORDER BY create_time DESC LIMIT 1");
        if (empty($sent_phone)) {
            preg_match('/\+852-([0-9]{8})/', $_SESSION['user_name'], $phone);
            if (!empty($phone)) {
                $smarty->assign('phone', $phone[1]);
            }
        } else {
            preg_match('/\+852-([0-9]{8})/', $sent_phone['phone'], $phone);
            $smarty->assign('sent_phone', $phone[1]);
            $smarty->assign('sent_remain', $sent_phone['create_time'] + 180 - $now);
        }
    }
} elseif ($_REQUEST['act'] == 'send' || $_REQUEST['act'] == 'validate') {
    $phone = $_REQUEST['act'] == 'send' ? trim($_REQUEST['phone']) : $_SESSION['phone_number'];
    $code = trim($_REQUEST['code']);
    $error = 0;
    $msg = '';

    $origin_coupon_key = substr_count($phone, '6');
    $coupon_key = $origin_coupon_key;
    while ($valid_coupons[$coupon_key]['remain'] <= 0 && $coupon_key > 0) {
        $coupon_key--;
    }
    if (!$event_status) {
        $error = 1;
        $msg = _L('event_not_start', '活動尚未開始');
    } elseif ($event_status != 1) {
        $error = 1;
        $msg = _L('event_ended', '活動已經結束');
    } elseif (!$logged_in) {
        $error = 1;
        $msg = _L('event_not_login', '請先登入');
    } elseif ($verified) {
        $error = 2;
        $msg = _L('event_verified', '你已完成驗證');
    } elseif (empty($phone) || strlen($phone) != 8 || strpos($phone, '6') === false || in_array($phone, $invalid_phones)) {
        $error = 3;
        $msg = _L('event_invalid_phone', '未有輸入正確電話號碼');
    } elseif (!$is_gift_enough) {
        $error = 3;
        $msg = _L('event_no_gift', '所有優惠券經已全部送出，多謝各位踴躍支持');
    } elseif (empty($coupon_key)) {
        $error = 3;
        $msg = _L('event_no_gift_phone', '此電話號碼可獲得的優惠券名額已滿，多謝各位踴躍支持');
    } elseif (empty($code) && $_REQUEST['act'] == 'validate') {
        $error = 3;
        $msg = _L('event_empty_code', '未有輸入驗證碼');
    } elseif ($db->getOne("SELECT COUNT(*) FROM " . $ecs->table('phone_number_log') . " WHERE phone LIKE '%$phone%' AND create_time BETWEEN $verify_start AND $verify_end AND status = 1")) {
        $error = 4;
        $msg = _L('user_sms_already_verified', '此電話號碼已被其他人驗證');
    } else {
        $sent_times = $db->getAll("SELECT create_time, status FROM " . $ecs->table('phone_number_log') . " WHERE (user_id = $user_id OR ip = '" . real_ip() . "' OR phone LIKE '%$phone%') AND create_time BETWEEN $verify_start AND $verify_end");

        if (count($sent_times) >= 20) {
            $error = 5;
            $msg = _L('event_verify_limit_exceed', '超出驗證次數上限');
        } else {
            foreach ($sent_times as $time) {
                if ($_REQUEST['act'] == 'send' && $time['create_time'] + 180 >= $now && $time['status'] != '9') {
                    $error = 6;
                    $msg = _L('event_wait_sms_verify', '每次發送需等候最少3分鐘');
                    break;
                } elseif ($_REQUEST['act'] == 'validate' && $time['create_time'] + 10 >= $now) {
                    $error = 7;
                    $msg = _L('event_wait_sms_validate', '每次驗證需等候最少10秒');
                }
            }

            if (!$error) {
                include_once ROOT_PATH . '/includes/twilio/authy/php/Authy.php';
                $twilio_authy_api = $_CFG['twilio_authy_api'];
                
                if ($_REQUEST['act'] == 'send') {
                    if ($origin_coupon_key != $coupon_key) {
                        $msg = _L('event_gift_is_fallback', '此電話號碼可獲得的優惠券名額已滿，我們將會送上其他優惠券');
                    }
                    
                    $authy_api = new Authy\AuthyApi($twilio_authy_api);
                    $api_response = $authy_api->phoneVerificationStart($phone, '852', 'sms');
                    if($api_response->ok()) {
                        $_SESSION['phone_number'] = $phone;
                        $db->query("UPDATE " . $ecs->table('phone_number_log') . " SET status = 2 WHERE phone = '+852-$phone' AND status = 0");
                        $db->query("INSERT INTO " . $ecs->table('phone_number_log') . " (user_id, status, phone, ip, create_time) VALUES ($user_id, 0, '+852-$phone', '" . real_ip() . "', $now)");
                    } else {
                        $error = 101;
                        $msg = $api_response->message();
                    }
                } else {
                    if ($log_id = $db->getOne("SELECT MAX(log_id) FROM " . $ecs->table('phone_number_log') . " WHERE user_id = $user_id AND phone = '+852-$phone' AND status = 0")) {
                        $db->query("INSERT INTO " . $ecs->table('phone_number_log') . " (user_id, status, phone, ip, create_time) VALUES ($user_id, 9, '+852-$phone', '" . real_ip() . "', $now)");
                        $authy_api = new Authy\AuthyApi($twilio_authy_api);
                        $api_response = $authy_api->PhoneVerificationCheck($phone, '852', $code);
                        if ($api_response->ok()) {
                            $coupon_config = $valid_coupons[$coupon_key];
                            $coupon_id = $coupon_config['coupon_id'];
                            $users = json_decode($db->getOne("SELECT users_list FROM " . $ecs->table('coupons') . " WHERE coupon_id = '$coupon_id'"), true);

                            if (is_array($users) && !in_array($user_id, $users) && count($users) <= intval($coupon_config['limit'])) {
                                $users[] = intval($user_id);
                                $db->query("UPDATE " . $ecs->table('coupons') . " SET users_list = '" . json_encode($users) . "' WHERE coupon_id = '$coupon_id'");
                            }

                            $db->query("UPDATE " . $ecs->table('phone_number_log') . " SET status = 2 WHERE (user_id = $user_id OR phone = '+852-$phone') AND status = 0 AND log_id != $log_id");
                            $db->query("UPDATE " . $ecs->table('phone_number_log') . " SET status = 1, ip = '" . real_ip() . "' WHERE log_id = $log_id");
                            unset($_SESSION['phone_number']);
                        } else {
                            $error = 9;
                            $msg = _L('user_sms_incorrect_code', '驗證碼錯誤');
                        }
                    } else {
                        $error = 8;
                        $msg = _L('user_sms_no_record', '沒有此電話之紀錄');
                    }
                }
            }
        }
    }

    echo json_encode([
        'error' => $error,
        'msg' => $msg,
        'phone' => $phone
    ]);
    exit;
} elseif ($_REQUEST['act'] == 'confirm') {
    if (!$logged_in || !$event_status) {
        header("Location: /phonenumber");
        exit;
    } elseif (!$verified) {
        header("Location: /phonenumber-verify");
        exit;
    } else {
        // Vervify through 友福同享 but no gift left
        if (!$verify_logged && !$is_gift_enough) {
            header("Location: /phonenumber");
            exit;
        }
        // Vervify through 友福同享
        if (!$verify_logged) {
            $phone = empty($log['verify_target']) ? $_SESSION['user_name'] : $log['verify_target'];
            $create_time = empty($log['create_at']) ? $now : $log['create_at'];

            $origin_coupon_key = substr_count($phone, '6');
            $coupon_key = $origin_coupon_key;
            while ($valid_coupons[$coupon_key]['remain'] <= 0 && $coupon_key > 0) {
                $coupon_key--;
            }
            // no gift left
            if (empty($coupon_key)) {
                header("Location: /phonenumber-verify");
                exit;
            }

            $db->query("INSERT INTO " . $ecs->table('phone_number_log') . " (user_id, status, phone, ip, create_time) VALUES ($user_id, 1, '$phone', '" . real_ip() . "', $create_time)");
        } else {
            $phone = empty($log['phone']) ? $_SESSION['user_name'] : $log['phone'];
            $coupon_key = substr_count($phone, '6');
        }
        $coupon_config = $valid_coupons[$verified_coupon_key == -1 ? $coupon_key : $verified_coupon_key];
        $coupon_id = $coupon_config['coupon_id'];
        $coupon = coupon_info($coupon_id);
        if (!in_array($user_id, $coupon['users_list']) && count($coupon['users_list']) <= $coupon_config['limit']) {
            $coupon['users_list'][] = intval($user_id);
            $db->query("UPDATE " . $ecs->table('coupons') . " SET users_list = '" . json_encode($coupon['users_list']) . "' WHERE coupon_id = '$coupon_id'");
        }
        $coupon_amount = intval($coupon['coupon_amount']);
        $min_goods_amount = intval($coupon['min_goods_amount']);
        $smarty->assign('coupon_code', $coupon['coupon_code']);
        $smarty->assign('coupon_amount', $coupon_amount);
        $smarty->assign('min_goods_amount', $min_goods_amount);
    }
}

/* 赋值固定内容 */
assign_template();
$position = assign_ur_here(0);
$smarty->assign('page_title', $position['title']);    // 页面标题
$smarty->assign('ur_here',    $position['ur_here']);  // 当前位置
$smarty->assign('helps', get_shop_help());              // 网店帮助

$smarty->assign('logged_in', $logged_in);
$smarty->assign('verified', $verified);
$smarty->assign('event_status', $event_status);
$smarty->assign('is_gift_enough', $is_gift_enough);
$smarty->assign('act', trim($_REQUEST['act']));
 
$smarty->display('phonenumber.html');
