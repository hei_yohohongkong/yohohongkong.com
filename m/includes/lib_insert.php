<?php

if (!defined('IN_ECS'))
{
	die('Hacking attempt');
}

function insert_mobile_page_header_user_menu($arr)
{
	global $smarty;
	
	$mode = isset($arr['mode']) ? $arr['mode'] : 'navbar';
	$smarty->assign('header_user_menu_mode', $mode);
	
	if ((!empty($_SESSION['user_id'])) && (!$smarty->get_template_vars('user_display_name')))
	{
		$smarty->assign('user_display_name', insert_user_name());
	}
	
	$need_cache = $smarty->caching;
	$smarty->caching = false;
	
	$output = $smarty->fetch('library/page_header_user_menu.lbi');
	$output = $smarty->process_inserts($output);
	
	$smarty->caching = $need_cache;
	
	return $output;
}

function insert_date_select_options($arr)
{
	$pre = $arr['prefix'];
	if (isset($arr['time']))
	{
		if (intval($arr['time']) > 10000)
		{
			$arr['time'] = gmdate('Y-m-d', $arr['time'] + 8*3600);
		}
		$t     = explode('-', $arr['time']);
		$year  = strval($t[0]);
		$month = strval($t[1]);
		$day   = strval($t[2]);
	}
	$now = gmdate('Y', time());
	if ($arr['mode'] == 'year')
	{
		if (isset($arr['start_year']))
		{
			if (abs($arr['start_year']) == $arr['start_year'])
			{
				$startyear = $arr['start_year'];
			}
			else
			{
				$startyear = $arr['start_year'] + $now;
			}
		}
		else
		{
			$startyear = $now - 3;
		}
	
		if (isset($arr['end_year']))
		{
			if (strlen(abs($arr['end_year'])) == strlen($arr['end_year']))
			{
				$endyear = $arr['end_year'];
			}
			else
			{
				$endyear = $arr['end_year'] + $now;
			}
		}
		else
		{
			$endyear = $now + 3;
		}
		
		$out = '';
		if ($arr['incl_zero'])
		{
			$out .= "<option value=\"0000\">{$arr['incl_zero']}</option>";
		}
		for ($i = $startyear; $i <= $endyear; $i++)
		{
			$out .= $i == $year ? "<option value=\"$i\" selected>$i</option>" : "<option value=\"$i\">$i</option>";
		}
		return $out;
	}
	else if ($arr['mode'] == 'month')
	{
		$out = '';
		if ($arr['incl_zero'])
		{
			$out .= "<option value=\"00\">{$arr['incl_zero']}</option>";
		}
		for ($i = 1; $i <= 12; $i++)
		{
			$padded_i = str_pad($i, 2, '0', STR_PAD_LEFT);
			$out .= "<option value=\"{$padded_i}\" " . ($i == $month ? "selected" : "") . ">{$padded_i}</option>";
		}
		return $out;
	}
	else if ($arr['mode'] == 'day')
	{
		$out = '';
		if ($arr['incl_zero'])
		{
			$out .= "<option value=\"00\">{$arr['incl_zero']}</option>";
		}
		for ($i = 1; $i <= 31; $i++)
		{
			$padded_i = str_pad($i, 2, '0', STR_PAD_LEFT);
			$out .= "<option value=\"{$padded_i}\" " . ($i == $day ? "selected" : "") . ">{$padded_i}</option>";
		}
		return $out;
	}
	return '';
}

function insert_area_select_options($atts)
{
	$value = !empty($atts['value']) ? $atts['value'] : '';
	
	$please_select = _L('area_select_please_select', '請選擇...');
	
	// Test if current language is sortable (there's no good way to sort Chinese)
	$sortable = mb_check_encoding($please_select, 'ASCII');
	
	$areas = array(
		'香港島' => array('中西區','灣仔區','東區','南區'),
		'九龍' => array('油尖旺區','深水埗區','九龍城區','黃大仙區','觀塘區'),
		'新界' => array('葵青區','荃灣區','屯門區','元朗區','北區','大埔區','沙田區','西貢區','離島區'),
		'其他' => array('中國大陸','澳門','台灣','其他')
	);
	foreach ($areas as $area => $subareas)
	{
		foreach ($subareas as $key => $subarea)
		{
			$subareas[$key] = array(
				'name' => _L('area_select_' . $subarea, $subarea),
				'value' => $subarea
			);
		}
		if ($sortable)
		{
			uasort($subareas, function ($subarea1, $subarea2) {
				// 其他 must be at bottom
				if ($subarea1['value'] == '其他')
				{
					return 1;
				}
				else if ($subarea2['value'] == '其他')
				{
					return -1;
				}
				else
				{
					return strcasecmp($subarea1['name'], $subarea2['name']);
				}
			});
		}
		$areas[$area] = array(
			'name' => _L('area_select_' . $area, $area),
			'value' => $area,
			'subareas' => $subareas
		);
	}
	
	$output = '<option value=""' . ($value == '' ? ' selected="selected"' : '') . '>' . $please_select . '</option>';
	foreach ($areas as $area)
	{
		$output .= '<optgroup label="' . $area['name'] . '">';
		foreach ($area['subareas'] as $subarea)
		{
			$output .= '<option value="' . $subarea['value'] . '"' . ($value == $subarea['value'] ? ' selected="selected"' : '') . '>' . $subarea['name'] . '</option>';
		}
		$output .= '</optgroup>';
	}
	
	return $output;
}

function insert_m_index_review()
{
    global $smarty, $ecs, $db;

    if ($user_area != 'CN') {
        $google_review = [
            // [
            //     'img_src'  => 'https://lh4.googleusercontent.com/-2c9p9fl1rzw/AAAAAAAAAAI/AAAAAAAAAAA/YzoNpr7qUZs/w60-h60-p-rp-mo-br100/photo.jpg',
            //     'name'     => 'annaannakwok Kwok',
            //     'review'   => '職員好好人服務態度一流，回覆快又有禮貌，令我可是以放心購物～門市同網上購物時電器選擇多元化，夠新夠齊全，同一種產品有齊多種牌子俾我哋選擇，輕鬆搜尋到心水產品省是時快捷，而且價錢夠實惠，更會不時推出優惠，合符經濟原則，購物有信心保證令人滿意，友和Yoho係一間好有心嘅鋪，為我帶嚟網上購物新體驗，一定會向朋友大力推介！',
            //     'rating'   => '5'
            // ],
            [
                'img_src' => 'https://lh3.googleusercontent.com/-DyU_AgJY5-U/AAAAAAAAAAI/AAAAAAAAAAA/QQ1NoM35nsI/w60-h60-p-rp-mo-ba3-br100/photo.jpg',
                'name'    => 'chu tony',
                'review'  => '最新電子潮流產品，家用電器亦幾多。',
                'rating'  => '5'
            ],
            [
                'img_src' => 'https://lh6.googleusercontent.com/-5DLDIebX3PA/AAAAAAAAAAI/AAAAAAAAAAA/CAfzunVUlI4/w60-h60-p-rp-mo-br100/photo.jpg',
                'name'    => 'Cathy Ng',
                'review'  => '職員很有禮貌，送貨速度超快！',
                'rating'  => '5'
            ],
            [
                'img_src' => 'https://lh3.googleusercontent.com/-Z9jjH7A2nSU/AAAAAAAAAAI/AAAAAAAAAAA/TaEvDLF3DOA/w60-h60-p-rp-mo-br100/photo.jpg',
                'name'    => 'tang mamching',
                'review'  => '真心推薦🙏🏻服務態度良好！電器又平又抵！良心企業！',
                'rating'  => '5'
            ],
            [
                'img_src' => 'https://lh6.googleusercontent.com/-ysPD4RMC20w/AAAAAAAAAAI/AAAAAAAAAAA/1JZ7t8iT97M/w60-h60-p-rp-mo-br100/photo.jpg',
                'name'    => 'Eve Ng',
                'review'  => '朋友介紹，買左3件貨品，其中正負零吸塵機價格平過香港電視商場同格價網站好多；客服回應查詢、改單、發貨都很迅速，讚！',
                'rating'  => '5'
            ],
            [
                'img_src' => 'https://lh5.googleusercontent.com/-riyT2aJW3RY/AAAAAAAAAAI/AAAAAAAAAAA/YU10g9hiEas/w60-h60-p-rp-mo-br100/photo.jpg',
                'name'    => 'Chris Yip',
                'review'  => 'Good services, just try their best to help you. Goods arrive and same as the picture what I had ordered from Websites. Trustable online seller.',
                'rating'  => '5'
            ],
            [
                'img_src' => 'https://lh3.googleusercontent.com/-qgEIkVsKSEA/AAAAAAAAAAI/AAAAAAAAAAA/H8xUc5ToFYY/w60-h60-p-rp-mo-br100/photo.jpg',
                'name'    => 'Toby Lau',
                'review'  => '呢度購物平過出面，兼係行貨有保養，貨物種類同大型百記有得比:P 仲買到日本24 beauty bar 😀 多得朋友介紹 😌 服務態度良好，讚！',
                'rating'  => '5'
            ],
            [
                'img_src' => 'https://lh3.googleusercontent.com/-QDymuFXILl0/AAAAAAAAAAI/AAAAAAAAAAA/mcKT-BUR8jE/w60-h60-p-rp-mo-br100/photo.jpg',
                'name'    => 'Kay Lee',
                'review'  => '店員耐心解答客人問題,售後服務能即時回覆以及當日解決客人疑難.第一次購買友和,會再其次光顧.',
                'rating'  => '5'
            ],
            // [
            //     'img_src' => 'https://lh4.googleusercontent.com/-iXCaPqddsvM/AAAAAAAAAAI/AAAAAAAAAAA/o5x5_tnEX2E/w60-h60-p-rp-mo-br100/photo.jpg',
            //     'name'    => 'Alan Tse',
            //     'review'  => 'A very thorough and comprehensive range of merchandises selling by YOHO Hong Kong online. It shows a good business model of O2O that enable me to spend during festival time.',
            //     'rating'  => '5'
            // ],
            // [
            //     'img_src' => 'https://lh4.googleusercontent.com/-aa9z53na7Nc/AAAAAAAAAAI/AAAAAAAAAAA/wn89NKlr1rk/w60-h60-p-rp-mo-ba3-br100/photo.jpg',
            //     'name'    => 'Janet Lam',
            //     'review'  => 'Very good shop with lots of new technology stuff on discounted price.
            //     Staff is quick and responds within a couple of minutes if you send them a message on the phone. Worth making the trouble to pay at the ATM before going to pick up the product as you can save a bit of money by doing so.',
            //     'rating'  => '5'
            // ],
            [
                'img_src'   => 'https://lh3.googleusercontent.com/-tUYOLuCf9OM/AAAAAAAAAAI/AAAAAAAAAAA/8Xj3pG7xMU0/w60-h60-p-rp-mo-br100/photo.jpg',
                'name'      => 'Frankie Pang',
                'review'    => '價錢實惠 ，員工態度十分友善， 讚！',
                'rating'    => '5',
            ],
            [
                'img_src'   => 'https://lh3.googleusercontent.com/-81CqpobK-hA/AAAAAAAAAAI/AAAAAAAAAAA/MGpaK-dLZcA/w60-h60-p-rp-mo-br100/photo.jpg',
                'name'      => 'Yp Chan',
                'review'    => '服務態度超級好,貨品又靚質素又好',
                'rating'    => '5',
            ],
            [
                'img_src'   => 'https://lh6.googleusercontent.com/-yre4FjV4nmU/AAAAAAAAAAI/AAAAAAAAAAA/R0s-RnITZhM/w60-h60-p-rp-mo-br100/photo.jpg',
                'name'      => 'Vivian Chan',
                'review'    => '職員服務態度有禮 ! 主動與客人溝通 !',
                'rating'    => '5',
            ],
            [
                'img_src'   => 'https://lh5.googleusercontent.com/-kiW1hqYv3XA/AAAAAAAAAAI/AAAAAAAAAAA/2NZD--JnkcY/w60-h60-p-rp-mo-br100/photo.jpg',
                'name'      => 'Dove Wong Ka Ho',
                'review'    => '最新最多野賣',
                'rating'    => '5',
            ],
            [
                'img_src'   => 'https://lh4.googleusercontent.com/-Jd5iH4G0LHg/AAAAAAAAAAI/AAAAAAAAAAA/OwrycmUElmk/w60-h60-p-rp-mo-br100/photo.jpg',
                'name'      => 'Dave Chau',
                'review'    => '店長殷勤親切，貨品也很新很齊，價錢如Apple也比其他鋪頭平，非其他一般電子店可比。',
                'rating'    => '5',
            ],
            [
                'img_src'   => 'https://lh6.googleusercontent.com/-ysPD4RMC20w/AAAAAAAAAAI/AAAAAAAAAAA/1JZ7t8iT97M/w60-h60-p-rp-mo-br100/photo.jpg',
                'name'      => 'Eve Ng',
                'review'    => '朋友介紹，買左3件貨品，其中正負零吸塵機價格平過香港電視商場同格價網站好多；客服回應查詢、改單、發貨都很迅速，讚！',
                'rating'    => '5',
            ],
        ];
    
        $facebook_review = [
            [
                'img_src'   => 'https://graph.facebook.com/1510570167/picture?width=50&height=50',
                'name'      => 'Katrina Wong',
                'review'    => '服務態度好，職員能解決我對產品的疑惑，是一次很愉快的購物經歷！very good！會再來',
                'rating'    => '5',
            ],
            [
                'img_src'   => 'https://graph.facebook.com/529587559/picture?width=50&height=50',
                'name'      => 'Cody Kam',
                'review'    => '服務好！員工態度好！種類多！價錢平！值得再回購！',
                'rating'    => '5',
            ],
            [
                'img_src'   => 'https://graph.facebook.com/100013158673345/picture?width=50&height=50',
                'name'      => '&nbsp;周文欣&nbsp;',
                'review'    => '購物很方便也很有保障！',
                'rating'    => '5',
            ],
            [
                'img_src'   => 'https://graph.facebook.com/1373067427/picture?width=50&height=50',
                'name'      => 'Sau Mei Ip',
                'review'    => '很滿意服務，價錢非常合理',
                'rating'    => '5',
            ],
            [
                'img_src'   => 'https://graph.facebook.com/573202068/picture?width=50&height=50',
                'name'      => 'Tina Tso',
                'review'    => '產品齊全，員工又友善',
                'rating'    => '5',
            ],
            [
                'img_src'   => 'https://graph.facebook.com/1517220786/picture?width=50&height=50',
                'name'      => 'Kwan Kwan Tam',
                'review'    => '尋日落單，今早就送到，昨天還在擔心，客服有問有答，非常專業，很滿意的一次購物',
                'rating'    => '5',
            ],
            [
                'img_src'   => 'https://graph.facebook.com/711824547/picture?width=50&height=50',
                'name'      => 'Candy Loi',
                'review'    => '產品多，齊全，經濟實惠',
                'rating'    => '5',
            ],
            [
                'img_src'   => 'https://graph.facebook.com/672215959/picture?width=50&height=50',
                'name'      => 'Fai Wong',
                'review'    => '真係要讚下，尋日先登記做用戶，三點幾落ORDER落銀行過數，四點幾已經出咗貨，今朝食晏前就收貨，又快又方便。',
                'rating'    => '5',
            ],
            [
                'img_src'   => 'https://graph.facebook.com/1731987383/picture?width=50&height=50',
                'name'      => 'Tong Wang',
                'review'    => 'Staff always reacts fast  Shopped online several times already; perfect service!',
                'rating'    => '5',
            ],
            /* [
                 'img_src'   => 'https://graph.facebook.com/1336182160/picture?width=50&height=50',
                 'name'      => 'Tracy Wan',
                 'review'    => 'Fast reply and delivery, good product with nice price. I have already bought 2 dehumidifiers, 2 vacuum cleaners and a shower head from yoho. I highly recommend yoho to my frds.',
                 'rating'    => '5',
             ],*/
            [
                'img_src'   => 'https://graph.facebook.com/533516694/picture?width=50&height=50',
                'name'      => 'Rainbow HK',
                'review'    => 'Fast delivery, excellent product with good price!',
                'rating'    => '5',
            ],
            [
                'img_src'   => 'https://graph.facebook.com/705702598/picture?width=50&height=50',
                'name'      => 'Christy Li',
                'review'    => 'Very good customer service. Nice shopping experience!',
                'rating'    => '5',
            ],
        ];
    } else {
        $google_review = [
            // [
            //     'img_src'  => 'https://lh4.googleusercontent.com/-2c9p9fl1rzw/AAAAAAAAAAI/AAAAAAAAAAA/YzoNpr7qUZs/w60-h60-p-rp-mo-br100/photo.jpg',
            //     'name'     => 'annaannakwok Kwok',
            //     'review'   => '職員好好人服務態度一流，回覆快又有禮貌，令我可是以放心購物～門市同網上購物時電器選擇多元化，夠新夠齊全，同一種產品有齊多種牌子俾我哋選擇，輕鬆搜尋到心水產品省是時快捷，而且價錢夠實惠，更會不時推出優惠，合符經濟原則，購物有信心保證令人滿意，友和Yoho係一間好有心嘅鋪，為我帶嚟網上購物新體驗，一定會向朋友大力推介！',
            //     'rating'   => '5'
            // ],
            [
                'img_src' => '/data/review_img/g01.jpg',
                'name'    => 'chu tony',
                'review'  => '最新電子潮流產品，家用電器亦幾多。',
                'rating'  => '5'
            ],
            [
                'img_src' => '/data/review_img/g02.jpg',
                'name'    => 'Cathy Ng',
                'review'  => '職員很有禮貌，送貨速度超快！',
                'rating'  => '5'
            ],
            [
                'img_src' => '/data/review_img/g03.jpg',
                'name'    => 'tang mamching',
                'review'  => '真心推薦🙏🏻服務態度良好！電器又平又抵！良心企業！',
                'rating'  => '5'
            ],
            [
               'img_src' => '/data/review_img/g04.jpg',
                'name'    => 'Eve Ng',
                'review'  => '朋友介紹，買左3件貨品，其中正負零吸塵機價格平過香港電視商場同格價網站好多；客服回應查詢、改單、發貨都很迅速，讚！',
                'rating'  => '5'
            ],
            [
                'img_src' => '/data/review_img/g05.jpg',
                'name'    => 'Chris Yip',
                'review'  => 'Good services, just try their best to help you. Goods arrive and same as the picture what I had ordered from Websites. Trustable online seller.',
                'rating'  => '5'
            ],
            [
                'img_src' => '/data/review_img/g06.jpg',
                'name'    => 'Toby Lau',
                'review'  => '呢度購物平過出面，兼係行貨有保養，貨物種類同大型百記有得比:P 仲買到日本24 beauty bar 😀 多得朋友介紹 😌 服務態度良好，讚！',
                'rating'  => '5'
            ],
            [
                'img_src' => '/data/review_img/g07.jpg',
                'name'    => 'Kay Lee',
                'review'  => '店員耐心解答客人問題,售後服務能即時回覆以及當日解決客人疑難.第一次購買友和,會再其次光顧.',
                'rating'  => '5'
            ],
            // [
            //     'img_src' => 'https://lh4.googleusercontent.com/-iXCaPqddsvM/AAAAAAAAAAI/AAAAAAAAAAA/o5x5_tnEX2E/w60-h60-p-rp-mo-br100/photo.jpg',
            //     'name'    => 'Alan Tse',
            //     'review'  => 'A very thorough and comprehensive range of merchandises selling by YOHO Hong Kong online. It shows a good business model of O2O that enable me to spend during festival time.',
            //     'rating'  => '5'
            // ],
            // [
            //     'img_src' => 'https://lh4.googleusercontent.com/-aa9z53na7Nc/AAAAAAAAAAI/AAAAAAAAAAA/wn89NKlr1rk/w60-h60-p-rp-mo-ba3-br100/photo.jpg',
            //     'name'    => 'Janet Lam',
            //     'review'  => 'Very good shop with lots of new technology stuff on discounted price.
            //     Staff is quick and responds within a couple of minutes if you send them a message on the phone. Worth making the trouble to pay at the ATM before going to pick up the product as you can save a bit of money by doing so.',
            //     'rating'  => '5'
            // ],
            [
                'img_src' => '/data/review_img/g08.jpg',
                'name'      => 'Frankie Pang',
                'review'    => '價錢實惠 ，員工態度十分友善， 讚！',
                'rating'    => '5',
            ],
            [
                'img_src' => '/data/review_img/g09.jpg',
                'name'      => 'Yp Chan',
                'review'    => '服務態度超級好,貨品又靚質素又好',
                'rating'    => '5',
            ],
            [
                'img_src' => '/data/review_img/g10.jpg',
                'name'      => 'Vivian Chan',
                'review'    => '職員服務態度有禮 ! 主動與客人溝通 !',
                'rating'    => '5',
            ],
            [
                'img_src' => '/data/review_img/g11.jpg',
                'name'      => 'Dove Wong Ka Ho',
                'review'    => '最新最多野賣',
                'rating'    => '5',
            ],
            [
                'img_src' => '/data/review_img/g12.jpg',
                'name'      => 'Dave Chau',
                'review'    => '店長殷勤親切，貨品也很新很齊，價錢如Apple也比其他鋪頭平，非其他一般電子店可比。',
                'rating'    => '5',
            ],
            [
                'img_src' => '/data/review_img/g13.jpg',
                'name'      => 'Eve Ng',
                'review'    => '朋友介紹，買左3件貨品，其中正負零吸塵機價格平過香港電視商場同格價網站好多；客服回應查詢、改單、發貨都很迅速，讚！',
                'rating'    => '5',
            ],
        ];
    
        $facebook_review = [
            [
                'img_src' => '/data/review_img/f01.jpg',
                'name'      => 'Katrina Wong',
                'review'    => '服務態度好，職員能解決我對產品的疑惑，是一次很愉快的購物經歷！very good！會再來',
                'rating'    => '5',
            ],
            [
                'img_src' => '/data/review_img/f02.jpg',
                'name'      => 'Cody Kam',
                'review'    => '服務好！員工態度好！種類多！價錢平！值得再回購！',
                'rating'    => '5',
            ],
            [
                'img_src' => '/data/review_img/f03.jpg',
                'name'      => '&nbsp;周文欣&nbsp;',
                'review'    => '購物很方便也很有保障！',
                'rating'    => '5',
            ],
            [
                'img_src' => '/data/review_img/f04.jpg',
                'name'      => 'Sau Mei Ip',
                'review'    => '很滿意服務，價錢非常合理',
                'rating'    => '5',
            ],
            [
                'img_src' => '/data/review_img/f05.jpg',
                'name'      => 'Tina Tso',
                'review'    => '產品齊全，員工又友善',
                'rating'    => '5',
            ],
            [
                'img_src' => '/data/review_img/f06.jpg',
                'name'      => 'Kwan Kwan Tam',
                'review'    => '尋日落單，今早就送到，昨天還在擔心，客服有問有答，非常專業，很滿意的一次購物',
                'rating'    => '5',
            ],
            [
                'img_src' => '/data/review_img/f07.jpg',
                'name'      => 'Candy Loi',
                'review'    => '產品多，齊全，經濟實惠',
                'rating'    => '5',
            ],
            [
                'img_src' => '/data/review_img/f08.jpg',
                'name'      => 'Fai Wong',
                'review'    => '真係要讚下，尋日先登記做用戶，三點幾落ORDER落銀行過數，四點幾已經出咗貨，今朝食晏前就收貨，又快又方便。',
                'rating'    => '5',
            ],
            [
                'img_src' => '/data/review_img/f09.jpg',
                'name'      => 'Tong Wang',
                'review'    => 'Staff always reacts fast  Shopped online several times already; perfect service!',
                'rating'    => '5',
            ],
            /* [
                 'img_src'   => 'https://graph.facebook.com/1336182160/picture?width=50&height=50',
                 'name'      => 'Tracy Wan',
                 'review'    => 'Fast reply and delivery, good product with nice price. I have already bought 2 dehumidifiers, 2 vacuum cleaners and a shower head from yoho. I highly recommend yoho to my frds.',
                 'rating'    => '5',
             ],*/
            [
                'img_src' => '/data/review_img/f10.jpg',
                'name'      => 'Rainbow HK',
                'review'    => 'Fast delivery, excellent product with good price!',
                'rating'    => '5',
            ],
            [
                'img_src' => '/data/review_img/f11.jpg',
                'name'      => 'Christy Li',
                'review'    => 'Very good customer service. Nice shopping experience!',
                'rating'    => '5',
            ],
        ];
    }

    $g_random_keys = array_rand($google_review, 5);
    $fb_random_keys = array_rand($facebook_review, 10);
    $rand_id = array_merge($g_random_keys, $fb_random_keys);
    $display_review = [];

    foreach($g_random_keys as $g_k) {
        $google_review[$g_k]['platform'] = 'google';
        $display_review[] = $google_review[$g_k];
    }

    foreach($fb_random_keys as $f_k) {
        $facebook_review[$f_k]['platform'] = 'facebook';
        $display_review[] = $facebook_review[$f_k];
    }
    shuffle($display_review);
    $smarty->assign('index_review', $display_review);

    // Obtain smarty output
    $need_cache = $smarty->caching;
	$smarty->caching = false;
	
	$output = $smarty->fetch('library/index_review.lbi');
	$output = $smarty->process_inserts($output);
	
	$smarty->caching = $need_cache;
	
	return $output;
}
?>