<?php

/**
 * ECSHOP mobile前台公共函数
 * ============================================================================
 * * 版权所有 2005-2012 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: testyang $
 * $Id: lib_main.php 15013 2008-10-23 09:31:42Z testyang $
*/

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

/**
 * 对输出编码
 *
 * @access  public
 * @param   string   $str
 * @return  string
 */
function encode_output($str)
{
//    if (EC_CHARSET != 'utf-8')
//    {
//        $str = ecs_iconv(EC_CHARSET, 'utf-8', $str);
//    }
    return htmlspecialchars($str);
}

/**
 * wap分页函数
 *
 * @access      public
 * @param       int     $num        总记录数
 * @param       int     $perpage    每页记录数
 * @param       int     $curr_page  当前页数
 * @param       string  $mpurl      传入的连接地址
 * @param       string  $pvar       分页变量
 */
function get_wap_pager($num, $perpage, $curr_page, $mpurl,$pvar)
{
    $multipage = '';
    if($num > $perpage)
    {
        $page = 2;
        $offset = 1;
        $pages = ceil($num / $perpage);
        $all_pages = $pages;
        $tmp_page = $curr_page;
        $setp = strpos($mpurl, '?') === false ? "?" : '&amp;';
        if($curr_page > 1)
        {
            $multipage .= "<a href=\"$mpurl${setp}${pvar}=".($curr_page-1)."\">上一页</a>";
        }
        $multipage .= $curr_page."/".$pages;
        if(($curr_page++) < $pages)
        {
            $multipage .= "<a href=\"$mpurl${setp}${pvar}=".$curr_page++."\">下一页</a><br/>";
        }
        //$multipage .= $pages > $page ? " ... <a href=\"$mpurl&amp;$pvar=$pages\"> [$pages] &gt;&gt;</a>" : " 页/".$all_pages."页";
        //$url_array = explode("?" , $mpurl);
       // $field_str = "";
       // if (isset($url_array[1]))
       // {
          //  $filed_array = explode("&amp;" , $url_array[1]);
           // if (count($filed_array) > 0)
            //{
             //   foreach ($filed_array AS $data)
              //  {
               //     $value_array = explode("=" , $data);
                //    $field_str .= "<postfield name='".$value_array[0]."' value='".encode_output($value_array[1])."'/>\n";
               // }
           // }
      //  }
        //$multipage .= "跳转到第<input type='text' name='pageno' format='*N' size='4' value='' maxlength='2' emptyok='true' />页<anchor>[GO]<go href='{$url_array[0]}' method='get'>{$field_str}<postfield name='".$pvar."' value='$(pageno)'/></go></anchor>";
        //<postfield name='snid' value='".session_id()."'/>
    }
    return $multipage;
}

function get_mobile_pager($record_count, $size, $page, $url, $pvar)
{
    $size = intval($size);
    if ($size < 1)
    {
        $size = 10;
    }
    
    $page = intval($page);
    if ($page < 1)
    {
        $page = 1;
    }
    
    $record_count = intval($record_count);
    
    $page_count = $record_count > 0 ? intval(ceil($record_count / $size)) : 1;
    if ($page > $page_count)
    {
        $page = $page_count;
    }
    /* 分页样式 */
    $pager['styleid'] = isset($GLOBALS['_CFG']['page_style'])? intval($GLOBALS['_CFG']['page_style']) : 0;
    
    $page_prev  = ($page > 1) ? $page - 1 : 1;
    $page_next  = ($page < $page_count) ? $page + 1 : $page_count;
    
    $param_url = strpos($url, '?') === false ? "?" : '&amp;';
    
    $pager['url']          = $url;
    $pager['start']        = ($page -1) * $size;
    $pager['page']         = $page;
    $pager['size']         = $size;
    $pager['record_count'] = $record_count;
    $pager['page_count']   = $page_count;
    
    if ($pager['styleid'] == 0)
    {
        $pager['page_first']   = $url . $param_url . $pvar . '=1';
        $pager['page_prev']    = $url . $param_url . $pvar . '=' . $page_prev;
        $pager['page_next']    = $url . $param_url . $pvar . '=' . $page_next;
        $pager['page_last']    = $url . $param_url . $pvar . '=' . $page_count;
        $pager['array']  = array();
        for ($i = 1; $i <= $page_count; $i++)
        {
            $pager['array'][$i] = $i;
        }
    }
    else
    {
        $_pagenum = 5;     // 显示的页码
        $_offset = 2;       // 当前页偏移值
        $_from = $_to = 0;  // 开始页, 结束页
        if($_pagenum > $page_count)
        {
            $_from = 1;
            $_to = $page_count;
        }
        else
        {
            $_from = $page - $_offset;
            $_to = $_from + $_pagenum - 1;
            if($_from < 1)
            {
                $_to = $page + 1 - $_from;
                $_from = 1;
                if($_to - $_from < $_pagenum)
                {
                    $_to = $_pagenum;
                }
            }
            elseif($_to > $page_count)
            {
                $_from = $page_count - $_pagenum + 1;
                $_to = $page_count;
            }
        }
        $url_format = $url . $param_url . $pvar . '=';
        $pager['page_first'] = ($page - $_offset > 1 && $_pagenum < $page_count) ? $url_format . 1 : '';
        $pager['page_prev']  = ($page > 1) ? $url_format . $page_prev : '';
        $pager['page_next']  = ($page < $page_count) ? $url_format . $page_next : '';
        $pager['page_last']  = ($_to < $page_count) ? $url_format . $page_count : '';
        $pager['page_kbd']  = ($_pagenum < $page_count) ? true : false;
        $pager['page_number'] = array();
        for ($i=$_from;$i<=$_to;++$i)
        {
            $pager['page_number'][$i] = $url_format . $i;
        }
    }
    $pager['search'] = $param;
    
    return $pager;
}

/**
 * 返回尾文件
 *
 * @return  string
 */
function get_footer()
{
    return sprintf(_L('global_footer_copyright', '&copy; %d 友和 YOHO, 保留所有權利'), local_date('Y'));
    
    // if ($_SESSION['user_id'] > 0)
    // {
    //     $footer = "<br/><a href='user.php?act=user_center'>用户中心</a>|<a href='user.php?act=logout'>退出</a>|<a href='javascript:scroll(0,0)' hidefocus='true'>回到顶部</a><br/>Copyright 2009<br/>Powered by ECShop v2.7.2";
    // }
    // else
    // {
    //     $footer = "<br/><a href='user.php?act=login'>登陆</a>|<a href='user.php?act=register'>免费注册</a>|<a href='javascript:scroll(0,0)' hidefocus='true'>回到顶部</a><br/>Copyright 2009<br/>Powered by ECShop v2.7.2";
    // }
    // 
    // return $footer;
}

function assign_mobile_ur_here($cat = 0, $str = '')
{
    /* 判断是否重写，取得文件名 */

    $cur_url = basename(PHP_SELF);
    if (intval($GLOBALS['_CFG']['rewrite']))
    {
        $filename = strpos($cur_url,'-') ? substr($cur_url, 0, strpos($cur_url,'-')) : substr($cur_url, 0, -4);
    }
    else
    {
        $filename = substr($cur_url, 0, -4);
    }

    /* 初始化“页面标题”和“当前位置” */
    $cur_lang = $GLOBALS['_CFG']['lang']; 
    if($cur_lang == 'zh_tw'){
        $shop_title = $GLOBALS['_CFG']['shop_title'];
    } else {
        $sql = "SELECT `code` FROM " . $GLOBALS['ecs']->table('shop_config_lang') . " WHERE `code` LIKE 'shop_title' AND `lang` LIKE '" .$cur_lang. "'";
        $shop_title = $GLOBALS['db']->getOne($sql);
    }
    $page_title = $shop_title;
    $ur_here    = '<li><a href="/">' . _L('global_home', '首頁') . '</a></li>';

    /* 根据文件名分别处理中间的部分 */
    if ($filename != 'index')
    {
        /* 处理有分类的 */
        if (in_array($filename, array('category', 'goods', 'article_cat', 'article', 'brand', 'topic')))
        {
            /* 商品分类或商品 */
            if ('category' == $filename || 'goods' == $filename || 'brand' == $filename)
            {
                if ($cat > 0)
                {
                    $cat_arr = get_parent_cats($cat);

                    $key     = 'cid';
                    $namekey = 'cname';
                    $type    = 'category';
                    $permakey = 'cperma';
                }
                else
                {
                    $cat_arr = array();
                }
            }
            /* 文章分类或文章 */
            elseif ('article_cat' == $filename || 'article' == $filename)
            {
                if ($cat > 0)
                {
                    $cat_arr = get_article_parent_cats($cat);
                    $first = reset($cat_arr);
                    $end = end($cat_arr);
                    //If the parent is UPHELP_CAT , don't show in nav bar
                    if($end['cat_type'] == UPHELP_CAT ) array_pop($cat_arr);
                    if($first['cat_name'] == $str) array_shift($cat_arr);
                    $key  = 'acid';
                    $namekey = 'acname';
                    $type = 'article_cat';
                    $permakey = 'acperma';
                }
                else
                {
                    $cat_arr = array();
                }
            }
            /* 推廣優惠 */
            elseif ('topic' == $filename)
            {
                $key = 'tcid';
                $namekey = 'tcname';
                $type = 'topic_cat';
                $permakey = 'tcperma';
                
                $cat_arr = array(array('cat_id' => 0, 'cat_name' => _L('topic_root', '推廣優惠')));
                if ($cat > 0)
                {
                    $sql = "SELECT IFNULL(tcl.topic_cat_name, tc.topic_cat_name) as topic_cat_name, $permakey " .
                            "FROM " . $GLOBALS['ecs']->table('topic_category') . " as tc " .
                            "LEFT JOIN " . $GLOBALS['ecs']->table('topic_category_lang') . " as tcl " .
                                "ON tcl.topic_cat_id = tc.topic_cat_id AND tcl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
                            "LEFT JOIN (SELECT id, table_name, lang, perma_link as $permakey FROM " . $GLOBALS['ecs']->table('perma_link') . ") tcpl ON tcpl.id = tc.topic_cat_id  AND tcpl.table_name = 'topic_category' AND tcpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
                            "WHERE tc.topic_cat_id = '" . $cat . "'";
                    $info = $GLOBALS['db']->getRow($sql);               
                    array_unshift($cat_arr, array('cat_id' => $cat, 'cat_name' => $info['topic_cat_name'], 'tcperma' => $info['tcperma']   ));
                }
            }
            /* brand */
            elseif ('brand' == $filename)
            {
                $ur_here = '<li><a href="/">' . _L('global_home', '首頁') . '</a></li>' .
                                '<li><a href="/brand/">' . _L('brand_list', '所有品牌') . '</a></li>';
                if ($cat > 0)
				{
                    $cat_arr = get_parent_cats($cat);

                    $key = 'cid';
                    $namekey = 'cname';
                    $type = 'category';
                    $permakey = 'cperma';
                }
				else
				{
                    $cat_arr = array();
                }
            }

            /* 循环分类 */
            if (!empty($cat_arr))
            {
                krsort($cat_arr);
                $last_cat_name = '';
                foreach ($cat_arr AS $val)
                {
                    if ($last_cat_name == $val['cat_name']) continue; // Added by howang
                    $page_title = htmlspecialchars($val['cat_name']) . ' - ' . $page_title;
                    $args       = array($key => $val['cat_id'], $namekey => $val['cat_name'], $permakey => $val[$permakey]);
                    $ur_here   .= '<li><a href="'. build_uri($type, $args, $val['cat_name']) . '">' .
                                    htmlspecialchars($val['cat_name']) . '</a></li>';
                    $last_cat_name = $val['cat_name']; // Added by howang
                }
            }
        }
        /* 处理无分类的 */
        else
        {
            /* 团购 */
            if ('group_buy' == $filename)
            {
                $page_title = $GLOBALS['_LANG']['group_buy_goods'] . ' - ' . $page_title;
                $args       = array('gbid' => '0');
                $ur_here   .= '<li><a href="group_buy.php">' .
                                $GLOBALS['_LANG']['group_buy_goods'] . '</a></li>';
            }
            /* 拍卖 */
            elseif ('auction' == $filename)
            {
                $page_title = $GLOBALS['_LANG']['auction'] . ' - ' . $page_title;
                $args       = array('auid' => '0');
                $ur_here   .= '<li><a href="auction.php">' .
                                $GLOBALS['_LANG']['auction'] . '</a></li>';
            }
            /* 夺宝 */
            elseif ('snatch' == $filename)
            {
                $page_title = $GLOBALS['_LANG']['snatch'] . ' - ' . $page_title;
                $args       = array('id' => '0');
                $ur_here   .= '<li><a href="snatch.php">' . 
                                $GLOBALS['_LANG']['snatch_list'] . '</a></li>';
            }
            /* 批发 */
            elseif ('wholesale' == $filename)
            {
                $page_title = $GLOBALS['_LANG']['wholesale'] . ' - ' . $page_title;
                $args       = array('wsid' => '0');
                $ur_here   .= '<li><a href="wholesale.php">' .
                                $GLOBALS['_LANG']['wholesale'] . '</a></li>';
            }
            /* 积分兑换 */
            elseif ('exchange' == $filename)
            {
                $page_title = $GLOBALS['_LANG']['exchange'] . ' - ' . $page_title;
                $args       = array('wsid' => '0');
                $ur_here   .= '<li><a href="exchange.php">' .
                                $GLOBALS['_LANG']['exchange'] . '</a></li>';
            }
            /* 其他的在这里补充 */
        }
    }

    /* 处理最后一部分 */
    if (!empty($str))
    {
        $page_title  = $str . ' - ' . $page_title;
        $ur_here    .= '<li class="active">' . $str . '</li>';
    }

    /* 返回值 */
    return array('title' => $page_title, 'ur_here' => $ur_here);
}

/**
 * 显示一个提示信息
 *
 * @access  public
 * @param   string  $content
 * @param   string  $link
 * @param   string  $href
 * @param   string  $type               信息类型：warning, error, info
 * @param   string  $auto_redirect      是否自动跳转
 * @return  void
 */
function show_mobile_message($content, $links = '', $hrefs = '', $type = 'info', $auto_redirect = true)
{
    assign_template();

    $msg['content'] = $content;
    if (is_array($links) && is_array($hrefs))
    {
        if (!empty($links) && count($links) == count($hrefs))
        {
            foreach($links as $key =>$val)
            {
                $msg['url_info'][$val] = $hrefs[$key];
            }
            $msg['back_url'] = $hrefs['0'];
        }
    }
    else
    {
        $link = empty($links) ? _L('global_go_back', '返回上一頁') : $links;
        $href = empty($hrefs) ? 'javascript:history.back()' : $hrefs;
        $msg['url_info'][$link] = $href;
        $msg['back_url'] = $href;
    }

    $msg['type'] = $type;
    $position = assign_mobile_ur_here(0, _L('global_system_message', '系統訊息'));
    $GLOBALS['smarty']->assign('page_title', $position['title']);   // 页面标题
    $GLOBALS['smarty']->assign('ur_here',    $position['ur_here']); // 当前位置

    if (is_null($GLOBALS['smarty']->get_template_vars('helps')))
    {
        $GLOBALS['smarty']->assign('helps', get_shop_help()); // 网店帮助
    }

    $GLOBALS['smarty']->assign('auto_redirect', $auto_redirect);
    $GLOBALS['smarty']->assign('message', $msg);
    $GLOBALS['smarty']->display('message.html');

    exit;
}

?>