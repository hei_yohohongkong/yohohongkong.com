<?php


define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

$promote_kind = in_array($_REQUEST['promote_kind'], array("featured", "gift", "redeem", "package", "info")) ? $_REQUEST['promote_kind'] : "featured";
$smarty->assign('cat_id', $cat_id);
$smarty->assign('cat_name', $cat_name);


$cache_id = sprintf('%X', crc32($_SESSION['user_rank'] . '-' . $_CFG['lang'] . '-' . $promote_kind));
if (!$smarty->is_cached('promotion_info.html', $cache_id))
{
    assign_template();
    $position = assign_ur_here(0, $cat_name);
    $smarty->assign('page_title',       $position['title']);                    // 页面标题
    $smarty->assign('ur_here',          $position['ur_here']);                  // 当前位置
    
    $CategoryController = new Yoho\cms\Controller\CategoryController();
    $cat_list = cat_list(0, 0, false, 0, false, true);
    $category = $CategoryController->groupCat($cat_list);
    $smarty->assign('categories',              $category);
    $smarty->assign('helps',            get_shop_help());
    $smarty->assign('promote_kind',     $promote_kind);
}

$smarty->display('promotion_info.html', $cache_id);
exit;


/* 显示模板 */
$smarty->display($templates, $cache_id);

exit;

?>