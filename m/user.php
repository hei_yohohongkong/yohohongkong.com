<?php

/**
 * ECSHOP 用户中心
 * ============================================================================
 * * 版权所有 2005-2012 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liubo $
 * $Id: user.php 16643 2009-09-08 07:02:13Z liubo $
*/

define('IN_ECS', true);
define('FORCE_HTTPS', true);
require(dirname(__FILE__) . '/includes/init.php');
require(dirname(dirname(__FILE__)) . '/includes/lib_order.php');
/* 载入语言文件 */
require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/user.php');
// Re-load yoho language pack to avoid languages overrided by user pack
require(ROOT_PATH . 'languages/' .$_CFG['lang']. '/yoho.php');
$smarty->assign('lang', $_LANG);
$userController    = new Yoho\cms\Controller\UserController();
$orderController   = new Yoho\cms\Controller\OrderController();
$paymentController = new Yoho\cms\Controller\PaymentController();
$feedController    = new Yoho\cms\Controller\FeedController();
$userRankController = new Yoho\cms\Controller\UserRankController();
$actg = $paymentController->actg;
$actgHooks = $paymentController->actgHooks;

if ((DEBUG_MODE & 2) != 2)
{
	$smarty->caching = true;
}

$act = isset($_REQUEST['act']) ? $_REQUEST['act'] : '';

// Enforce HTTPS connection on important pages
if (in_array($act, array('register', 'login', 'get_password', 'profile', 'affiliate')))
{
	if (!$usingSSL)
	{
		redirect_to_https();
	}
}

if (!function_exists('validate_date'))
{
	function validate_date($date)
	{
		$d = DateTime::createFromFormat('Y-m-d', $date);
		return $d && $d->format('Y-m-d') == $date && intval($d->format('Y')) != 0;
	}
}

/* 用户登陆 */
$login_faild = 0;
$user_id = $_SESSION['user_id'];
// $affiliate = unserialize($GLOBALS['_CFG']['affiliate']);
// $smarty->assign('affiliate', $affiliate);
$is_affiliate = empty($user_id) ? '' : $db->getOne("SELECT is_affiliate FROM " . $ecs->table('users') . " WHERE user_id = '" . $user_id . "'");
$smarty->assign('is_affiliate', $is_affiliate);

if ($act == 'do_login')
{
	$ccc = !empty($_POST['ccc']) ? $_POST['ccc'] : '';
	$user_name = !empty($_POST['username']) ? $_POST['username'] : '';
	$pwd = !empty($_POST['pwd']) ? $_POST['pwd'] : '';
	$remember_me = !empty($_POST['remember_me']) ? 1 : 0;
	
	if (!empty($ccc))
	{
		$user_name = '+' . $ccc . '-' . $user_name;
	}
	
	require_once(ROOT_PATH . 'includes/lib_passport.php');
	
	$user->login($user_name, $pwd, $remember_me);
	
	if($_SESSION['user_id'] > 0)
	{
		restore_user_saved_language();
		update_user_info();
		recalculate_price();//重新计算购物车中的商品价格

		if (!empty($_SESSION['redirect_to'])) {
			$redirect_to = $_SESSION['redirect_to'];
			unset($_SESSION['redirect_to']);
			header('Location: '.$redirect_to);
			exit;
		}

		if($_SESSION['user_rank_program_special']) {
			header('Location: '.$_SESSION['user_rank_program_special']['url']);
		} else {
			header('Location: /user.php');
		}
		
	}
	else
	{
		$_SESSION['login_fail']++;
	
		$login_faild = 1;
		show_login_page();
	}
}

elseif ($act == 'order_list')
{
	//Blocking Wholesale Customer
	if(in_array(@$_SESSION['user_id'],$userController->getWholesaleUserIds()))
	    show_message('批發客戶請直接聯絡營業員訂購');
	if ($_SESSION['user_id'] <= 0)
	{
		header('Location: /user.php?act=login');
		exit;
	}
	include_once(ROOT_PATH . 'includes/lib_transaction.php');
	
	assign_template();
	
	$position = assign_ur_here(0,  _L('user_my_orders', '我的訂單'));
	$smarty->assign('page_title',  $position['title']);	// 页面标题
	$smarty->assign('ur_here',     $position['ur_here']);  // 当前位置
	$CategoryController = new Yoho\cms\Controller\CategoryController();
	$t1 = $CategoryController->getTopCategories(0);
	$smarty->assign('t1',              $t1);
	$smarty->assign('helps',       get_shop_help());
	$smarty->assign('keywords',    htmlspecialchars($_CFG['shop_keywords']));
	$smarty->assign('description', htmlspecialchars($_CFG['shop_desc']));
	
	$record_count = $db->getOne("SELECT COUNT(*) FROM " .$ecs->table('order_info'). " WHERE user_id = {$_SESSION['user_id']} AND order_status != " . OS_CANCELED);
	if ($record_count > 0)
	{
		$page_num = '10';
		$page = !empty($_GET['page']) ? intval($_GET['page']) : 1;
		$pages = ceil($record_count / $page_num);

		if ($page <= 0)
		{
			$page = 1;
		}
		if ($pages == 0)
		{
			$pages = 1;
		}
		if ($page > $pages)
		{
			$page = $pages;
		}
		//$pagebar = get_wap_pager($record_count, $page_num, $page, 'user.php?act=order_list', 'page');
		//$smarty->assign('pagebar' , $pagebar);
		$pager = get_mobile_pager($record_count, $page_num, $page, 'user.php?act=order_list', 'page');
		$smarty->assign('pager', $pager);
		
		$orders = get_mobile_user_orders($_SESSION['user_id'], $page_num, $page_num * ($page - 1));
		
		$smarty->assign('orders', $orders);
	}
	$has_remind = get_order_not_upload_receipt($user_id);
	if ($has_remind) {
		$remind = "<div id='upload_remind'>" . sprintf(_L('user_require_upload', "訂單 <b>%s</b> 尚未上傳訂單收據。 <a href='/order_receipts/order/%s'><b>按此</b></a> 立即上傳。"), $has_remind['order_id'], $has_remind['order_sn']) . "</div>";
		$smarty->assign('remind', $remind);
	}
	
	$smarty->display('order_list.html');
	exit;
}

/* 查看订单详情 */
elseif ($act == 'order_detail')
{
	//Blocking Wholesale Customer
	if(in_array(@$_SESSION['user_id'],$userController->getWholesaleUserIds()))
	    show_message('批發客戶請直接聯絡營業員訂購');

	if ($_SESSION['user_id'] <= 0)
	{
		header('Location: /user.php?act=login');
		exit;
	}

	assign_template();
	
	$position = assign_ur_here(0,  _L('user_order_detail', '訂單詳情'));
	$smarty->assign('page_title',  $position['title']);	// 页面标题
	$smarty->assign('ur_here',     $position['ur_here']);  // 当前位置
	$CategoryController = new Yoho\cms\Controller\CategoryController();
	$t1 = $CategoryController->getTopCategories(0);
	$smarty->assign('t1',              $t1);
	$smarty->assign('helps',       get_shop_help());
	$smarty->assign('keywords',    htmlspecialchars($_CFG['shop_keywords']));
	$smarty->assign('description', htmlspecialchars($_CFG['shop_desc']));
	
	$user_id = $_SESSION['user_id'];
	$payl = isset($_REQUEST['payl']) ? trim($_REQUEST['payl']) : '0';
	
	include_once(ROOT_PATH . 'includes/lib_transaction.php');
	include_once(ROOT_PATH . 'includes/lib_payment.php');
	include_once(ROOT_PATH . 'includes/lib_order.php');
	include_once(ROOT_PATH . 'includes/lib_clips.php');
	
	$order_id = isset($_GET['order_id']) ? intval($_GET['order_id']) : 0;
	
	/* 订单详情 */
	$order = get_order_detail($order_id, $user_id);

	$order_goods = order_goods($order_id, false);
	//check stock if it is enough
	if ($order['money_paid'] == 0 && $order['is_online_pay'] == 1) {
		$need_to_cancel_order = false;
		foreach ($order_goods as $goods_item) {
			// check current stock 
			//dd($erpController);
			if ($goods_item['is_gift'] != 0 || $goods_item['insurance_of'] != 0  || $goods_item['flashdeal_id'] != 0) {
				continue;
			}

			$goods_info = get_goods_info($goods_item['goods_id']);
			$current_stock = $goods_info['goods_number'];
			//$current_stock = $erpController->getSellableProductQty($goods_item['goods_id']);
			if ($current_stock <= 0 || $current_stock < ($goods_item['goods_number'] - $goods_item['send_number'])) {
				// check if per sales
				if ($goods_info['is_pre_sale'] != 1 && $goods_info['pre_sale_days'] > 0) {
					$need_to_cancel_order = false;
					break;
				} else {
					$need_to_cancel_order = true;
				}
			} else {
				$need_to_cancel_order = false;
				break;
			}
		}
		
		if ($need_to_cancel_order == true) {
			cancel_order($order_id);
			show_message('“尊敬的客戶，由於您在購買 ['.$goods_info['goods_name'].'] 時只有1件庫存，同一時間有另外顧客下單，但比您先完成付款，因此此單系統會自動幫您取消，請重新下單，給您帶來不便，敬請諒解！”');
			exit;
		}
	}

	// get coupon promote
	$can_change_order_payment_type = ($orderController->canChangeOrderPaymentType($order_id) && $orderController->canChangeOrderPaymentTypeWithFlashDeal($order_id));
	$smarty->assign('can_change_order_payment_type', $can_change_order_payment_type);

	if ($order === false)
	{
		$err->show(_L('global_go_home', '返回首頁'), '/');
		exit;
	}
	
	/* 是否显示添加到购物车 */
	if ($order['extension_code'] != 'group_buy' && $order['extension_code'] != 'exchange_goods')
	{
		$smarty->assign('allow_to_cart', 1);
	}
	if($payl == 1 && ($order['order_status'] == OS_CANCELED || $order['pay_status'] != PS_UNPAYED)){
		show_message(_L('global_can_not_paid', '無法支付此訂單'));
		exit;
	}
	
	/* 订单商品 */
	$goods_detail = order_goods($order_id, true);
	$goods_list = $goods_detail['goods'];
	foreach ($goods_list AS $key => $value)
	{
		$goods_list[$key]['market_price'] = price_format($value['market_price'], false);
		$goods_list[$key]['goods_price']  = price_format($value['goods_price'], false);
		$goods_list[$key]['subtotal']     = price_format($value['subtotal'], false);
		
		$sql = "SELECT goods_thumb FROM " . $ecs->table('goods') . " WHERE `goods_id` = '" . $value['goods_id'] . "'";
		$row = $db->getRow($sql);
		$goods_list[$key]['goods_thumb']  = $row['goods_thumb'];
		$goods_list[$key]['url']          = build_uri('goods', array('gid' => $value['goods_id'], 'gname' => $value['goods_name']));
	}
	
	/* 设置能否修改使用余额数 */
	if ($order['order_amount'] > 0)
	{
		if ($order['order_status'] == OS_UNCONFIRMED || $order['order_status'] == OS_CONFIRMED)
		{
			$user = user_info($order['user_id']);
			if ($user['user_money'] + $user['credit_line'] > 0)
			{
				$smarty->assign('allow_edit_surplus', 1);
				$smarty->assign('max_surplus', sprintf($_LANG['max_surplus'], $user['user_money']));
			}
		}
	}
	
	// /* 未发货，未付款时允许更换支付方式 */
	// if ($order['order_amount'] > 0 && $order['pay_status'] == PS_UNPAYED && $order['shipping_status'] == SS_UNSHIPPED)
	// howang: only need to populate payment methods on payment page
	if (($payl == 1) && ($order['order_amount'] > 0))
	{
        $order['current_platform'] = $paymentController::PLATFORM_MOBILE;
        $user = user_info($order['user_id']);
		$online_only = false;
		$order_goods = order_goods($order_id);
		foreach ($order_goods as $og_k => $og) {
			// Have flashdeal goods, only can pay with online payment
			$sql = "SELECT is_online_pay FROM ".$ecs->table('flashdeals').' WHERE flashdeal_id = '.$og['flashdeal_id'];
			if($db->getOne($sql)) {
				$online_only = true;
				break;
			}

		}
		$filter_data['country']          = $order['country'];
		$filter_data['pay_id']           = $order['pay_id'];
		$filter_data['goods_amount']     = $order['goods_amount'];
		$filter_data['rank_points']      = $user['rank_points'];
		$filter_data['step']             = 'checkout';
		$filter_data['current_platform'] = $paymentController::PLATFORM_MOBILE;
		$filter_data['is_online_pay']    = $online_only;
		$payment_list = $paymentController->available_payment_list(false, 0, true, $filter_data);

		$smarty->assign('payment_list', $payment_list);
	}
	
	// Order Status, YOHO's way
	if (in_array($order['order_status'], array(OS_CANCELED, OS_INVALID, OS_RETURNED, OS_WAITING_REFUND)))
	{
		$order['yoho_order_status'] = $_LANG['os'][$order['order_status']];
	}
	else if (in_array($order['pay_status'], array(PS_UNPAYED, PS_PAYING)))
	{
		$order['yoho_order_status'] = $_LANG['ps'][$order['pay_status']];
	}
	else if ($order['shipping_status'] != SS_RECEIVED)
	{
		//Get invoice no Carrier
        $sql = "SELECT l.logistics_url, l.logistics_code, l.logistics_name, do.invoice_no as trackingNumber FROM ".$ecs->table("delivery_order")." as do ".
		"LEFT JOIN ".$ecs->table("logistics")." as l ON do.logistics_id = l.logistics_id ".
		"WHERE do.order_id = '".$order['order_id']."' AND do.logistics_id > 0";
		$delivery_orders = $db->getAll($sql);
		// Check $delivery_orders
		$empty_delivery = false;
		foreach ($delivery_orders as $key => $value) {
			if(array_filter($value) && !empty($value['trackingNumber']) && $value['logistics_code'] != 'remark'){
				$empty_delivery = true;
			} else {
				unset($delivery_orders[$key]);
			}
		}
		if($empty_delivery){
			$smarty->assign('show_tracking_info', 1);
			$smarty->assign('delivery_orders',      $delivery_orders);
		}

		$order['yoho_order_status'] = $order['shipping_status'] == SS_RECEIVED ? $_LANG['ss_received'] : $_LANG['ss'][$order['shipping_status']];
	}
	else
	{
		$order['yoho_order_status'] = $order['shipping_status'] == SS_RECEIVED ? $_LANG['ss_received'] : $_LANG['ss'][$order['shipping_status']];
	}
	$display_pay = true;
	if($order['order_status'] == OS_CANCELED)$display_pay = false;
	
	if ($order['order_amount'] > 0)
	{
		require_once(ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_accounting.php');
		$smarty->assign('payment_amount', $actgHooks->getOrderPaymentAmount($order));
	}
	
	// If is 線上付款 及 付款成功, add purchase Event.
	if($_REQUEST['paysuccess'] && $_REQUEST['paysuccess']=='1' && $order['pay_status'] == PS_PAYED){
		$feed = $feedController->purchase_insert_code($order, $goods_list);
        $smarty->assign('stats_code_insert', $feed['stats_code_insert']);
        $smarty->assign('extra_insert_code', $feed['extra_insert_code']);
        $smarty->assign('just_online_paid', 1);
	}

	/* 订单 支付 配送 状态语言项 */
	$order['order_status'] = $_LANG['os'][$order['order_status']];
	$order['pay_status'] = $_LANG['ps'][$order['pay_status']];
	$order['shipping_status'] = $_LANG['ss'][$order['shipping_status']];
	
	$smarty->assign('order',	  $order);
	$smarty->assign('display_pay',	  $display_pay);
	$smarty->assign('goods_list', $goods_list);
	$smarty->assign('packages', $goods_detail['packages']);
	$smarty->assign('payl', $payl);
	

	if ((!empty($_REQUEST['payment_processing'])) && ($_REQUEST['payment_processing'] == 1))
	{
		$smarty->display('order_paying.html');
	}
	else if (($payl == 1) && ($order['order_amount'] > 0))
	{
		$payment = payment_info($order['pay_id']);
		
		include_once('includes/modules/payment/' . $payment['pay_code'] . '.php');
		
		$pay_obj	= new $payment['pay_code'];

		$pay_online = $pay_obj->get_code($order, $payment['pay_config_list']);
		
		$order['pay_desc'] = $payment['pay_desc'];
		
		//$smarty->assign('norder',	 order_info($order['order_sn']));
        $smarty->assign('payment', $payment);
		$smarty->assign('pay_online', $pay_online);
		$smarty->assign('is_online_pay', $payment['is_online_pay']);
		
		$smarty->display('order_pay.html');
	}
	else
	{
		$smarty->display('order_detail.html');
	}
	exit;
}

/* 修改訂單付款方式 */
elseif ($act == 'act_edit_payment')
{
	/* 检查是否登录 */
	if ($_SESSION['user_id'] <= 0)
	{
		header('Location: /user.php?act=login');
		exit;
	}
	
	$pay_id = empty($_REQUEST['pay_id']) ? -1 : $_REQUEST['pay_id'];
	
	if ($pay_id <= 0)
	{
		show_mobile_message(
			_L('payment_change_method_error_missing', '請選擇付款方式'),
			_L('global_go_back', '返回上一頁'), 'javascript:history.back(-1)', 'warning');
		exit;
	}
	
	include_once(ROOT_PATH . 'includes/lib_order.php');
	
	$payment_info = payment_info($pay_id);
	
	if (empty($payment_info))
	{
		show_mobile_message(
			_L('payment_change_method_error_invalid', '錯誤的付款方式'),
			_L('global_go_back', '返回上一頁'), 'javascript:history.back(-1)', 'error');
		exit;
	}
	
	/* 检查订单号 */
	$order_id = intval($_REQUEST['order_id']);
	if ($order_id <= 0)
	{
		show_mobile_message(
			_L('user_order_not_exists', '訂單並不存在'),
			_L('global_go_back', '返回上一頁'), 'javascript:history.back(-1)', 'error');
		exit;
	}
	
	/* 取得订单 */
	$order = order_info($order_id);
	
	if (empty($order))
	{
		show_mobile_message(
			_L('user_order_not_exists', '訂單並不存在'),
			_L('global_go_back', '返回上一頁'), 'javascript:history.back(-1)', 'error');
		exit;
	}
	
	/* 检查订单用户跟当前用户是否一致 */
	if ($_SESSION['user_id'] != $order['user_id'])
	{
		show_mobile_message(
			_L('user_order_not_exists', '訂單並不存在'),
			_L('global_go_back', '返回上一頁'), 'javascript:history.back(-1)', 'error');
		exit;
	}
	
	/* 未发货，未付款时允许更换支付方式 */
	if (!($order['order_amount'] > 0 && $order['pay_status'] == PS_UNPAYED && $order['shipping_status'] == SS_UNSHIPPED))
	{
		show_mobile_message(
			_L('payment_change_method_error_order_status', '此訂單不允許修改付款方式。'),
			_L('global_go_back', '返回上一頁'), 'javascript:history.back(-1)', 'error');
		exit;
	}
	
	// Update order_amount according to new pay_fee
	$order_amount = $order['order_amount'] - $order['pay_fee'];
	$pay_fee = pay_fee($pay_id, $order_amount);
	$order_amount += $pay_fee;
	
	$sql = "UPDATE " . $ecs->table('order_info') .
		   " SET pay_id='$pay_id', pay_name='$payment_info[display_name]', pay_fee='$pay_fee', order_amount='$order_amount'".
		   " WHERE order_id = '$order_id' LIMIT 1";
	$db->query($sql);
	
	//如果支付费用改变了，也要相应的更改pay_log表的order_amount
	$pay_log_amount = $db->getOne("SELECT order_amount FROM " .$ecs->table('pay_log')." WHERE order_id = '$order_id' AND order_type = 'PAY_SURPLUS' AND is_paid = 0");
	
	if ($pay_log_amount != $order_amount)
	{
		$db->query("UPDATE " .$ecs->table('pay_log').
				" SET order_amount = '$order_amount' WHERE order_id = '$order_id' AND order_type = 'PAY_SURPLUS' AND is_paid = 0");
	}
	
	if ($payment_info['is_online_pay'] == '0') {
		$crmController = new Yoho\cms\Controller\CrmController();
		$crmController->init_offline_payment_flow($order_id);
	} else {
		$db->query("UPDATE " . $ecs->table("offline_payment") . " SET status = " . OFFLINE_PAYTMENT_CLOSED . " WHERE order_id = $order_id AND status < " . OFFLINE_PAYTMENT_FINISHED);
	}

	/* 跳转 */
	header('Location: /user.php?act=order_detail&order_id=' . $order_id . '&payl=1');
	exit;
}

/* 取消订单 */
elseif ($act == 'cancel_order')
{
	if ($_SESSION['user_id'] <= 0)
	{
		header('Location: /user.php?act=login');
		exit;
	}
	
	include_once(ROOT_PATH . 'includes/lib_transaction.php');
	include_once(ROOT_PATH . 'includes/lib_order.php');
	
	$order_id = isset($_GET['order_id']) ? intval($_GET['order_id']) : 0;
	if (cancel_order($order_id, $_SESSION['user_id']))
	{
		$_SESSION['order_action_msg_type'] = 'info';
		$_SESSION['order_action_msg'] = _L('user_order_cancel_success', '您已成功取消訂單。');
		$db->query("UPDATE " . $ecs->table("offline_payment") . " SET status = " . OFFLINE_PAYTMENT_CLOSED . " WHERE order_id = $order_id AND status < " . OFFLINE_PAYTMENT_FINISHED);
	}
	else
	{
		$_SESSION['order_action_msg_type'] = 'warning';
		$_SESSION['order_action_msg'] = _L('user_order_cancel_failed', '取消訂單失敗！');
	}
	
	header("Location: /user.php?act=order_list");
	exit;
}

/* 确认收货 */
elseif ($act == 'affirm_received')
{
	if ($_SESSION['user_id'] <= 0)
	{
		header('Location: /user.php?act=login');
		exit;
	}
	
	include_once(ROOT_PATH . 'includes/lib_transaction.php');
	
	$order_id = isset($_GET['order_id']) ? intval($_GET['order_id']) : 0;
	$_LANG['buyer'] = '買家';
	if (affirm_received($order_id, $_SESSION['user_id']))
	{
		$_SESSION['order_action_msg_type'] = 'info';
		$_SESSION['order_action_msg'] = _L('user_confirm_delivery_success', '確認收貨成功！');
		
	}
	else
	{
		$_SESSION['order_action_msg_type'] = 'warning';
		$_SESSION['order_action_msg'] = _L('user_confirm_delivery_failed', '確認收貨失敗！');
	}
	
	header("Location: /user.php?act=order_list");
	exit;
}

/* 退出会员中心 */
elseif ($act == 'logout')
{
	if (!isset($back_act) && isset($GLOBALS['_SERVER']['HTTP_REFERER']))
	{
		$back_act = strpos($GLOBALS['_SERVER']['HTTP_REFERER'], 'user.php') ? '/' : $GLOBALS['_SERVER']['HTTP_REFERER'];
	}
	else
	{
		$back_act = '/';
	}
	
	$user->logout();
	header("Location: $back_act");
	exit;
}

/* 显示会员注册界面 */
elseif ($act == 'register')
{
	// 如果已經登入，轉到會員中心
	if ($_SESSION['user_id'] > 0)
	{
		header('Location: /user.php');
		exit;
	}
	$reference = get_affiliate();
	$cache_id = sprintf('%X', crc32($_CFG['lang'].(($reference)?'-'.$reference : '')));
	
	if (!$smarty->is_cached('register.html', $cache_id))
	{
		assign_template();
		
		$position = assign_ur_here(0,  _L('account_register', '會員註冊'));
		$smarty->assign('page_title',  $position['title']);	// 页面标题
		$smarty->assign('ur_here',     $position['ur_here']);  // 当前位置
		$smarty->assign('keywords',    htmlspecialchars($_CFG['shop_keywords']));
		$smarty->assign('description', htmlspecialchars($_CFG['shop_desc']));
		$CategoryController = new Yoho\cms\Controller\CategoryController();
		$t1 = $CategoryController->getTopCategories(0);
		$smarty->assign('t1',              $t1);
		$smarty->assign('helps',       get_shop_help());
		// If have Reference
		if($reference) {
			$smarty->assign('reference', $reference);
		}
		
		// 從文章取得使用條款
		$smarty->assign('tos_content', get_tos_article_content());
		// 從文章取得隱私條款
		$smarty->assign('privacy_content', get_privacy_article_content());
		$smarty->assign('user_area',   user_area());
		$smarty->assign('user_countries', implode(',', available_register_countries()));
	}
	$smarty->display('register.html', $cache_id);
	exit;
}

/* 用户中心 */
elseif ($act == 'default' || empty($act))
{
	//Blocking Wholesale Customer
	if(in_array(@$_SESSION['user_id'],$userController->getWholesaleUserIds()))
	    show_message('批發客戶請直接聯絡營業員訂購');
	if (!$_SESSION['user_id']) // 用戶未登入，顯示登入畫面
	{
		header('Location: /user.php?act=login');
		exit;
	}
	
	// howang: If user login / register from checkout page, send them back to checkout
	if ($_SESSION['from_checkout'])
	{
		$_SESSION['from_checkout'] = false;
		unset($_SESSION['from_checkout']);
		
		header('Location: /checkout');
		exit;
	}
	
	// Temporary disable user center, auto redirect to profile page
	header('Location: /user.php?act=profile');
	exit;
	
	assign_template();
	
	$position = assign_ur_here(0,  _L('user_dashboard', '帳戶概覽'));
	$smarty->assign('page_title',  $position['title']);	// 页面标题
	$smarty->assign('ur_here',     $position['ur_here']);  // 当前位置
	$smarty->assign('keywords',    htmlspecialchars($_CFG['shop_keywords']));
	$smarty->assign('description', htmlspecialchars($_CFG['shop_desc']));
	$CategoryController = new Yoho\cms\Controller\CategoryController();
	$t1 = $CategoryController->getTopCategories(0);
	$smarty->assign('t1',              $t1);
	$smarty->assign('helps',       get_shop_help());
	
	$user_id = $_SESSION['user_id'];
	
	// 取得基本用戶資料
	$sql = "SELECT pay_points, user_money, credit_line, last_login, is_validated, info_completed FROM " . $ecs->table('users') . " WHERE user_id = '$user_id'";
	$row = $GLOBALS['db']->getRow($sql);
	
	//補充資料送積分
	$user_info_completed = $row['info_completed'];
	if ($user_info_completed == 0)
	{
		show_mobile_message(_L('user_incomplete_login_prompt', '您是首次登入，補填個人資料並驗證電郵，送積分哦！'),
							_L('user_edit_profile', '修改資料'), 'user.php?act=profile', 'info');
	}
	
	// 取得用戶紅包
	$sql = "SELECT SUM(bt.type_money) AS bonus_value, COUNT(*) AS bonus_count ".
			"FROM " .$GLOBALS['ecs']->table('user_bonus'). " AS ub, ".
				$GLOBALS['ecs']->table('bonus_type') . " AS bt ".
			"WHERE ub.user_id = '$user_id' AND ub.bonus_type_id = bt.type_id AND ub.order_id = 0";
	$user_bonus = $GLOBALS['db']->getRow($sql);
	
	include_once(ROOT_PATH .'includes/lib_clips.php');
	
	// 取得用戶資料
	$info = get_user_default($user_id);
	$info['username']	= stripslashes($_SESSION['user_name']);
	$info['pay_points']  = $row['pay_points'];
	$info['is_validate'] = ($_CFG['member_email_validate'] && !$row['is_validated']) ? 0 : 1;
	$info['credit_line'] = $row['credit_line'];
	$info['formated_credit_line'] = price_format($info['credit_line'], false);
	$info['surplus']	 = price_format($row['user_money'], false);
	$info['bonus']	   = $user_bonus['bonus_count'];
	$info['bonus_value'] = price_format($user_bonus['bonus_value'], false);
	
	// 取得訂單數目
	$sql = "SELECT COUNT(*) FROM " . $ecs->table('order_info') . " WHERE user_id = '" .$user_id. "'";
	$info['order_count'] = $GLOBALS['db']->getOne($sql);
	
	$smarty->assign('info', $info);
	
	if ($rank = get_rank_info())
	{
		$smarty->assign('rank_name', sprintf($_LANG['your_level'], $rank['rank_name']));
		if (!empty($rank['next_rank_name']))
		{
			$smarty->assign('next_rank_name', sprintf($_LANG['next_level'], $rank['next_rank'] ,$rank['next_rank_name']));
		}
	}
	
	$orders = get_mobile_user_orders($user_id, 5);
	$smarty->assign('orders', $orders);
	
	$smarty->assign('user_notice', $_CFG['user_notice']);
	$smarty->assign('prompt',	  get_user_prompt($user_id));
	
	$smarty->display('user.html');
	exit;
}

/* 登入畫面 */
elseif ($act == 'login')
{
	show_login_page();
}

/* SMS驗證畫面 */
elseif ($act == 'sms_verification')
{
	show_sms_verification_page();
}

/* 個人資料 */
elseif ($act == 'profile')
{
	//Blocking Wholesale Customer
	if(in_array(@$_SESSION['user_id'],$userController->getWholesaleUserIds()))
	    show_message('批發客戶請直接聯絡營業員訂購');
	if ($_SESSION['user_id'] <= 0)
	{
		header('Location: /user.php?act=login');
		exit;
	}
	
	// if isset back_act_from_event session
	if (!empty($_SESSION["back_act_from_event"])) {
		$back_act_from_event = $_SESSION["back_act_from_event"];
		unset($_SESSION["back_act_from_event"]);
		header('Location: /'.$back_act_from_event);
		exit;
	}

	assign_template();
	
	$position = assign_ur_here(0,  _L('user_my_profile', '個人資料'));
	$smarty->assign('page_title',  $position['title']);	// 页面标题
	$smarty->assign('ur_here',     $position['ur_here']);  // 当前位置
	$smarty->assign('keywords',    htmlspecialchars($_CFG['shop_keywords']));
	$smarty->assign('description', htmlspecialchars($_CFG['shop_desc']));
	$CategoryController = new Yoho\cms\Controller\CategoryController();
	$t1 = $CategoryController->getTopCategories(0);
	$smarty->assign('t1',              $t1);
	$smarty->assign('helps',       get_shop_help());
	
	include_once(ROOT_PATH . 'includes/lib_transaction.php');
	
	$user_id = $_SESSION['user_id'];
	
	$user_info = get_profile($user_id);

	if($_CFG['send_verify_sms'] == 1 && $user_info['is_sms_validated'] == 0) {
		$smarty->assign('display_sms_verify', 1);
	}
	
	if ($user_info['email'] == $_SESSION['user_name'] . '@yohohongkong.com') // Prevent user keep using default system generated email
	{
		$user_info['email'] = '';
	}
	
	/* 取出注册扩展字段 */
	$sql = 'SELECT * FROM ' . $ecs->table('reg_fields') . ' WHERE type < 2 AND display = 1 ORDER BY dis_order, id';
	$extend_info_list = $db->getAll($sql);
	
	$sql = 'SELECT reg_field_id, content ' .
		   'FROM ' . $ecs->table('reg_extend_info') .
		   " WHERE user_id = $user_id";
	$extend_info_arr = $db->getAll($sql);
	
	$temp_arr = array();
	foreach ($extend_info_arr AS $val)
	{
		$temp_arr[$val['reg_field_id']] = $val['content'];
	}
	
	foreach ($extend_info_list AS $key => $val)
	{
		switch ($val['id'])
		{
			case 1:	 $extend_info_list[$key]['content'] = $user_info['msn']; break;
			case 2:	 $extend_info_list[$key]['content'] = $user_info['qq']; break;
			case 3:	 $extend_info_list[$key]['content'] = $user_info['office_phone']; break;
			case 4:	 $extend_info_list[$key]['content'] = $user_info['home_phone']; break;
			case 5:	 $extend_info_list[$key]['content'] = $user_info['mobile_phone']; break;
			default:	$extend_info_list[$key]['content'] = empty($temp_arr[$val['id']]) ? '' : $temp_arr[$val['id']] ;
		}
		if (in_array($val['id'], array(11,12))) // 常住地區/工作地區
		{
			// Display localized area name if available
			$extend_info_list[$key]['content_display'] = _L('area_select_' . $extend_info_list[$key]['content'], $extend_info_list[$key]['content']);
		}
		// Display localized field name if available
		$extend_info_list[$key]['reg_field_name'] = _L('user_profile_extend_field'.$val['id'], $val['reg_field_name']);
		$extend_info_list[$key]['field'] = 'extend_field'.$val['id'];
	}
	
	$smarty->assign('extend_info_list', $extend_info_list);
	
	/* 密码提示问题 */
	$smarty->assign('passwd_questions', $_LANG['passwd_questions']);
	
	// 是否已補填資料
	$sql = 'SELECT info_completed FROM ' . $ecs->table('users') . "  WHERE user_id = '$user_id'";
	$smarty->assign('user_info_completed', $db->getOne($sql));
	
	$smarty->assign('profile', $user_info);
	
	$smarty->display('user_profile.html');
	exit;
}

/* 修改个人资料的处理 */
elseif ($act == 'act_edit_profile')
{
	if ($_SESSION['user_id'] <= 0)
	{
		header('Location: /user.php?act=login');
		exit;
	}
	
	$user_id = $_SESSION['user_id'];
	
	include_once(ROOT_PATH . 'includes/lib_transaction.php');
	
	$sql = 'SELECT info_completed, phpbb_userid FROM ' . $ecs->table('users') . "  WHERE user_id = '$user_id'";
	$row = $db->getRow($sql);
	$user_info_completed = $row['info_completed'];
	$phpbb_id = $row['phpbb_userid'];
	
	$birthdayYear = empty($_POST['birthdayYear']) ? '' : trim($_POST['birthdayYear']);
	$birthdayMonth = empty($_POST['birthdayMonth']) ? '' : trim($_POST['birthdayMonth']);
	$birthdayDay = empty($_POST['birthdayDay']) ? '' : trim($_POST['birthdayDay']);
	$birthday = $birthdayYear .'-'. $birthdayMonth .'-'. $birthdayDay;
	$email = empty($_POST['email']) ? '' : trim($_POST['email']);
	$other['msn'] = $msn = isset($_POST['extend_field1']) ? trim($_POST['extend_field1']) : '';
	$other['qq'] = $qq = isset($_POST['extend_field2']) ? trim($_POST['extend_field2']) : '';
	$other['office_phone'] = $office_phone = isset($_POST['extend_field3']) ? trim($_POST['extend_field3']) : '';
	$other['home_phone'] = $home_phone = isset($_POST['extend_field4']) ? trim($_POST['extend_field4']) : '';
	$other['mobile_phone'] = $mobile_phone = isset($_POST['extend_field5']) ? trim($_POST['extend_field5']) : '';
	$sel_question = empty($_POST['sel_question']) ? '' : compile_str($_POST['sel_question']);
	$passwd_answer = isset($_POST['passwd_answer']) ? compile_str(trim($_POST['passwd_answer'])) : '';
	
	if (!empty($office_phone) && !preg_match( '/^[\d|\_|\-|\s]+$/', $office_phone ) )
	{
		show_mobile_message($_LANG['passport_js']['office_phone_invalid']);
	}
	if (!empty($home_phone) && !preg_match( '/^[\d|\_|\-|\s]+$/', $home_phone) )
	{
		show_mobile_message($_LANG['passport_js']['home_phone_invalid']);
	}
	
	if (empty($email))
	{
		show_message(sprintf(_L('user_profile_error_missing', '請輸入您的%s'), _L('user_profile_email', '電子郵件地址')));
	}
	
	if ((!is_email($email)) ||
		($email == $_SESSION['user_name'] . '@yohohongkong.com')) // Prevent user keep using default system generated email
	{
		show_mobile_message(sprintf(_L('user_profile_error_invalid', '請輸入正確的%s'), _L('user_profile_email', '電子郵件地址')));
	}
	
	$user_info = get_profile($user_id);
	
	if (!$user_info_completed)
	{
		if (($birthday == '--') || ($birthday == '0000-00-00'))
		{
			show_mobile_message(sprintf(_L('user_profile_error_missing', '請輸入您的%s'), _L('user_profile_birthday', '生日日期')));
		}

		if (!validate_date($birthday))
		{
			show_mobile_message(sprintf(_L('user_profile_error_invalid', '請輸入正確的%s'), _L('user_profile_birthday', '生日日期')));
		}

		if (intval($_POST['sex']) <= 0)
		{
			show_mobile_message(sprintf(_L('user_profile_error_missing', '請輸入您的%s'), _L('user_profile_gender', '性別')));
		}

		if (intval($_POST['sex']) > 2) // 0 = 保密, 1 = 男, 2 = 女, >2 = invalid
		{
			show_mobile_message(sprintf(_L('user_profile_error_invalid', '請輸入正確的%s'), _L('user_profile_gender', '性別')));
		}
	}
	else
	{
		$birthday = $user_info['birthday'];
		$_POST['sex'] = $user_info['sex'];
	}
	
	// 公開顯示名稱
	$display_name = $_POST['extend_field10'];
	if (!empty($display_name))
	{
		if (mb_strlen($display_name,'utf-8') > 20)
		{
			show_mobile_message(sprintf(_L('user_profile_error_too_long', '%s太長了'), _L('user_profile_extend_field10', '公開顯示名稱')));
		}
		$display_name = htmlspecialchars($display_name);
		$sql = "SELECT 1 FROM " . $ecs->table('reg_extend_info') . "  WHERE reg_field_id = '10' AND user_id != '$user_id' AND content = '$display_name'";
		$res = $db->getOne($sql);
		if (!empty($res))
		{
			show_mobile_message(sprintf(_L('user_profile_error_already_taken', '您的%s（%s）已被使用'), _L('user_profile_extend_field10', '公開顯示名稱'), $display_name));
		}
		$sql = "SELECT content FROM " . $ecs->table('reg_extend_info') . "  WHERE reg_field_id = '10' AND user_id = '$user_id'";
		$old_display_name = $db->getOne($sql);
	}
	
	if (!empty($msn) && !is_email($msn))
	{
		show_mobile_message($_LANG['passport_js']['msn_invalid']);
	}
	if (!empty($qq) && !preg_match('/^\d+$/', $qq))
	{
		show_mobile_message($_LANG['passport_js']['qq_invalid']);
	}
	if (!empty($mobile_phone) && !preg_match('/^[\d-\s]+$/', $mobile_phone))
	{
		show_mobile_message($_LANG['passport_js']['mobile_phone_invalid']);
	}
	
	/* 更新用户扩展字段的数据 */
	$sql = 'SELECT id,reg_field_name,is_need FROM ' . $ecs->table('reg_fields') . ' WHERE type = 0 AND display = 1 ORDER BY dis_order, id';   //读出所有扩展字段的id
	$fields_arr = $db->getAll($sql);
	
	foreach ($fields_arr AS $val)	   //循环更新扩展用户信息
	{
		$extend_field_index = 'extend_field' . $val['id'];
		$temp_field_content = isset($_POST[$extend_field_index]) ? $_POST[$extend_field_index] : '';
		$temp_field_content = strlen($temp_field_content) > 100 ? mb_substr(htmlspecialchars($temp_field_content), 0, 99) : htmlspecialchars($temp_field_content);
		
		if ((empty($temp_field_content)) && ($val['is_need'] == '1'))
		{
			show_mobile_message(sprintf(_L('user_profile_error_missing', '請輸入您的%s'), _L('user_profile_extend_field' . $val['id'], $val['reg_field_name'])));
		}
		
		$sql = "SELECT * FROM " . $ecs->table('reg_extend_info') . "  WHERE reg_field_id = '" . $val['id'] . "' AND user_id = '" . $user_id . "'";
		if ($db->getOne($sql))	  //如果之前没有记录，则插入
		{
			$sql = "UPDATE " . $ecs->table('reg_extend_info') .
					" SET content = '" . $temp_field_content . "'" .
					" WHERE reg_field_id = '" . $val['id'] . "' AND user_id = '" . $user_id . "'";
		}
		else
		{
			$sql = "INSERT INTO " . $ecs->table('reg_extend_info') .
					" (`user_id`, `reg_field_id`, `content`)" .
					" VALUES ('" . $user_id . "', '" . $val['id'] . "', '" . $temp_field_content . "')";
		}
		$db->query($sql);
	}
	
	/* 写入密码提示问题和答案 */
	if (!empty($passwd_answer) && !empty($sel_question))
	{
		$sql = 'UPDATE ' . $ecs->table('users') . " SET `passwd_question`='$sel_question', `passwd_answer`='$passwd_answer'  WHERE `user_id`='" . $_SESSION['user_id'] . "'";
		$db->query($sql);
	}
	
	$profile  = array(
		'user_id'  => $user_id,
		'email'	=> $email,
		'sex'	  => isset($_POST['sex'])   ? intval($_POST['sex']) : 0,
		'birthday' => $birthday,
		'other'	=> isset($other) ? $other : array()
	);
	
	if (edit_profile($profile))
	{
		if ($user_info['email'] != $profile['email'])
		{
			// Email changed, mark as not validated
			$sql="UPDATE ".$ecs->table('users'). "SET `is_validated` = '0' WHERE `user_id` = '".$user_id."' LIMIT 1";
			$db->query($sql);
			
			// send verify email
			include_once(ROOT_PATH .'includes/lib_passport.php');
			send_regiter_hash_async($user_id);
			
			if (!empty($phpbb_id))
			{
				require_once ROOT_PATH . 'core/lib_phpbb.php';
				phpbb_call(array(
					'act' => 'update_email',
					'phpbb_userid' => $phpbb_id,
					'new_email' => $profile['email'],
				));
			}
		}
		
		if ($old_display_name != $display_name)
		{
			if (!empty($phpbb_id))
			{
				require_once ROOT_PATH . 'core/lib_phpbb.php';
				phpbb_call(array(
					'act' => 'update_user_name',
					'phpbb_userid' => $phpbb_id,
					'new_name' => $display_name,
				));
			}
		}

		if ($user_info_completed == 0)
		{
			// Mark as user info completed
			$sql="UPDATE ".$ecs->table('users'). "SET `info_completed` = '1' WHERE `user_id` = '".$user_id."' LIMIT 1";
			$db->query($sql);
		}

		$sql = 'SELECT is_validated, first_validated FROM ' . $ecs->table('users') . "  WHERE user_id = '$user_id'";
		$row = $db->getRow($sql);
		$is_validated = $row['is_validated'];
		$first_validated = $row['first_validated'];

		if (($user_info_completed == 0) && ($is_validated == 1))
		{
			$sql="UPDATE ".$ecs->table('users'). "SET `info_completed` = '1' WHERE `user_id` = '".$user_id."' LIMIT 1";
			$db->query($sql);
			
			$profile_count_point = 2000; // As requested by Franz, only give away 2000 points
			log_account_change($user_id, 0, 0, 0, $profile_count_point, "首次修改資料並驗證電郵獲得積分", ACT_OTHER);
			show_mobile_message(sprintf(_L('user_complete_reward', '首次修改資料並驗證電郵成功，系統贈送給您%s分'), $profile_count_point),
								_L('user_my_profile', '個人資料'), 'user.php?act=profile', 'info');
		}
		else
		{
			show_mobile_message(sprintf(_L('user_profile_updated', '您的個人資料已經成功修改%s！'),
								($first_validated == 0 ? _L('user_profile_update_validate_email', '，驗證電郵可獲贈積分哦') : '')),
								_L('user_my_profile', '個人資料'), 'user.php?act=profile', 'info');
		}
	}
	else
	{
		if ($user->error == ERR_EMAIL_EXISTS)
		{
			$msg = sprintf(_L('user_profile_error_already_taken', '您的%s（%s）已被使用。'),
							_L('user_profile_email' , '電子郵件地址'), $profile['email']);
		}
		else
		{
			$msg = _L('user_profile_error_other', '修改個人資料失敗，請稍後再試。');
		}
		show_mobile_message($msg, '', '', 'info');
	}
}
elseif ($act == 'act_validate_display_name')
{
	if ($_SESSION['user_id'] <= 0)
	{
		$error_msg = _L('user_email_validate_send_error_not_logged_in', '請先登入');
		header('Content-Type: application/json;charset=utf-8');
		echo json_encode(array('valid' => false, 'message' => $error_msg));
		exit;
	}
	
	$user_id = $_SESSION['user_id'];
	
	$display_name = empty($_POST['extend_field10']) ? '' : $_POST['extend_field10'];
	if (mb_strlen($display_name,'utf-8') > 20)
	{
		$error_msg = sprintf(_L('user_profile_error_too_long', '%s太長了'), _L('user_profile_extend_field10', '公開顯示名稱'));
		header('Content-Type: application/json;charset=utf-8');
		echo json_encode(array('valid' => false, 'message' => $error_msg));
		exit;
	}
	$display_name = htmlspecialchars($display_name);
	$sql = "SELECT 1 FROM " . $ecs->table('reg_extend_info') . "  WHERE reg_field_id = '10' AND user_id != '$user_id' AND content = '$display_name'";
	$res = $db->getOne($sql);
	if (!empty($res))
	{
		$error_msg = sprintf(_L('user_profile_error_already_taken', '您的%s（%s）已被使用'), _L('user_profile_extend_field10', '公開顯示名稱'), $display_name);
		header('Content-Type: application/json;charset=utf-8');
		echo json_encode(array('valid' => false, 'message' => $error_msg));
		exit;
	}
	
	header('Content-Type: application/json');
	echo json_encode(array('valid' => true));
	exit;
}

/* 修改会员密码 */
elseif ($act == 'act_edit_password')
{
	if ($_SESSION['user_id'] <= 0)
	{
		header('Location: /user.php?act=login');
		exit;
	}
	
	$user_id = $_SESSION['user_id'];
	
	include_once(ROOT_PATH . 'includes/lib_passport.php');
	
	$old_password = isset($_POST['old_password']) ? trim($_POST['old_password']) : null;
	$new_password = isset($_POST['new_password']) ? trim($_POST['new_password']) : '';
	
	if (strlen($new_password) < 6)
	{
		show_mobile_message(_L('account_password_too_short', '密碼長度最少6個字哦！'));
	}
	
	$user_info = $user->get_profile_by_id($user_id); //论坛记录
	
	if ($user_id > 0 && $user->check_user($_SESSION['user_name'], $old_password))
	{
		if ($user->edit_user(array('username'=>$_SESSION['user_name'], 'old_password'=>$old_password, 'password'=>$new_password), 0))
		{
			// howang: added password change time
			$sql="UPDATE ".$ecs->table('users'). "SET `ec_salt`='',`salt`='0',`passchg_time`='".gmtime()."' WHERE user_id= '".$user_id."'";
			$db->query($sql);
			$user->logout();
			show_mobile_message(_L('account_reset_password_success', '修改密碼成功，請重新登入！'), _L('account_reset_password_back_to_login', '返回登入'), '/user.php?act=login', 'info');
		}
		else
		{
			show_mobile_message(_L('account_reset_password_failed', '修改密碼失敗，請稍後再試。'), _L('global_go_back', '返回上一頁'), '', 'info');
		}
	}
	else
	{
		show_mobile_message(_L('user_old_password_incorrect', '您輸入的現用密碼不正確！'), _L('global_go_back', '返回上一頁'), '', 'info');
	}
}
elseif ($act == 'act_validate_password')
{
	$password = empty($_POST['password']) ? (empty($_POST['old_password']) ? '' : $_POST['old_password']) : $_POST['password'];
	$valid = (($_SESSION['user_id'] > 0) && ($user->check_user($_SESSION['user_name'], $password)));
	
	header('Content-Type: application/json');
	echo json_encode(array('valid' => $valid));
	exit;
}

/* 重設密碼 */
elseif ($act == 'get_password')
{
	assign_template();
	
	$position = assign_ur_here(0,  _L('account_reset_password_title', '重設密碼'));
	$smarty->assign('page_title',  $position['title']);	// 页面标题
	$smarty->assign('ur_here',     $position['ur_here']);  // 当前位置
	$smarty->assign('keywords',    htmlspecialchars($_CFG['shop_keywords']));
	$smarty->assign('description', htmlspecialchars($_CFG['shop_desc']));
	$CategoryController = new Yoho\cms\Controller\CategoryController();
	$t1 = $CategoryController->getTopCategories(0);
	$smarty->assign('t1',              $t1);
	$smarty->assign('helps',       get_shop_help());
	
	$smarty->assign('user_area',   user_area());
	$smarty->assign('user_countries', implode(',', available_login_countries()));
	
	include_once(ROOT_PATH . 'includes/lib_passport.php');
	
	if (isset($_GET['code']) && isset($_GET['uid'])) //从邮件处获得的act
	{
		$code = trim($_GET['code']);
		$uid  = intval($_GET['uid']);
		
		/* 判断链接的合法性 */
		$user_info = $user->get_profile_by_id($uid);
		if ((empty($user_info)) || (!verify_password_reset_token($uid, $code)))
		{
			show_mobile_message(
				_L('account_reset_password_token_invalid', '你所使用的重設密碼鏈結並不正確或已經失效。'),
				_L('global_go_home', '返回首頁'), '/', 'info');
		}
		
		$smarty->assign('uid', $uid);
		$smarty->assign('code', $code);
		$smarty->assign('user_name', $user_info['user_name']);
	}
	
	$smarty->display('reset_password.html');
	exit;
}

/* 積分 */
elseif ($act == 'transform_points')
{
	header('Location: /user.php?act=points');
	exit;
}
elseif ($act == 'points')
{
	//Blocking Wholesale Customer
	if(in_array(@$_SESSION['user_id'],$userController->getWholesaleUserIds()))
	    show_message('批發客戶請直接聯絡營業員訂購');
	if ($_SESSION['user_id'] <= 0)
	{
		header('Location: /user.php?act=login');
		exit;
	}
	
	assign_template();
	
	$position = assign_ur_here(0,  _L('user_my_points', '我的積分'));
	$smarty->assign('page_title',  $position['title']);	// 页面标题
	$smarty->assign('ur_here',     $position['ur_here']);  // 当前位置
	$smarty->assign('keywords',    htmlspecialchars($_CFG['shop_keywords']));
	$smarty->assign('description', htmlspecialchars($_CFG['shop_desc']));
	$CategoryController = new Yoho\cms\Controller\CategoryController();
	$t1 = $CategoryController->getTopCategories(0);
	$smarty->assign('t1',              $t1);
	$smarty->assign('helps',       get_shop_help());
	
	$user_id = $_SESSION['user_id'];
	
	$sql = "SELECT pay_points, rank_points FROM " . $ecs->table('users')  . " WHERE user_id='$user_id'";
	$row = $db->getRow($sql);
	
	$smarty->assign('pay_points', $row['pay_points']);
	$smarty->assign('rank_points', $row['rank_points']);
	
	$smarty->display('user_points.html');
	exit;
}

/* ajax 发送验证邮件 */
elseif ($act == 'send_hash_mail')
{
	include_once(ROOT_PATH .'includes/cls_json.php');
	include_once(ROOT_PATH .'includes/lib_passport.php');
	$json = new JSON();
	
	$result = array('error' => 0, 'message' => '', 'content' => '');
	
	$user_id = $_SESSION['user_id'];
	
	if ($user_id == 0)
	{
		/* 用户没有登录 */
		$result['error']   = 1;
		$result['message'] = _L('user_email_validate_send_error_not_logged_in', '請先登入');
		die($json->encode($result));
	}
	
	if (send_regiter_hash_async($user_id))
	{
		$result['message'] = _L('user_email_validate_send_success', '驗證郵件發送成功');
		die($json->encode($result));
	}
	else
	{
		$result['error'] = 1;
		$result['message'] = $GLOBALS['err']->last_message();
	}
	
	die($json->encode($result));
}

/* 验证用户注册邮件 */
elseif ($act == 'validate_email')
{
	$hash = empty($_GET['hash']) ? '' : trim($_GET['hash']);
	if ($hash)
	{
		include_once(ROOT_PATH . 'includes/lib_passport.php');
		$id = register_hash('decode', $hash);
		if ($id > 0)
		{
			$sql = 'SELECT first_validated, info_completed FROM ' . $ecs->table('users') . "  WHERE user_id = '$id'";
			$row = $db->getRow($sql);
			$first_validated = $row['first_validated'];
			$info_completed = $row['info_completed'];
			
			$sql = "UPDATE " . $ecs->table('users') . " SET is_validated = 1, first_validated = 1 WHERE user_id='$id'";
			$db->query($sql);
			
			if (($first_validated == 0) && ($info_completed == 1))
			{
				$profile_count_point = 2000; // As requested by Franz, only give away 2000 points
				log_account_change($id, 0, 0, 0, $profile_count_point, "首次修改資料並驗證電郵獲得積分", ACT_OTHER);
				show_mobile_message(sprintf(_L('user_complete_reward', '首次修改資料並驗證電郵成功，系統贈送給您%s分'), $profile_count_point),
									_L('user_my_profile', '個人資料'), 'user.php?act=profile', 'info');
			}
			else
			{
				$sql = 'SELECT user_name, email FROM ' . $ecs->table('users') . " WHERE user_id = '$id'";
				$row = $db->getRow($sql);
				$incomplete = (($first_validated == 0) && ($info_completed == 0));
				$msg = sprintf(_L('user_email_validate_success', '%s 您好，您的電郵地址 %s 已通過驗證%s'),
							$row['user_name'], $row['email'],
							$incomplete ? _L('user_email_validate_complete_profile', '，補填個人資料，送積分哦！') : '');
				show_mobile_message($msg, _L('user_my_profile', '個人資料'), 'user.php?act=profile');
			}
		}
	}
	show_mobile_message(_L('user_email_validate_failed', '驗證失敗，請確認你的驗證連結是否正確'));
}

/* 上傳用戶頭像 */
elseif ($act == 'act_upload_avatar')
{
	if ($_SESSION['user_id'] <= 0)
	{
		header('Location: /user.php?act=login');
		exit;
	}
	
	$save_path = ROOT_PATH . 'uploads/avatars/';
	$save_path .= $_SESSION['user_id'];

	if (!isset($_FILES['avatar_image']))
	{
		show_mobile_message(_L('user_avatar_upload_error_empty', '若要更改頭像，請上傳圖片。'));
	}

	$error = $_FILES['avatar_image']['error'];
	if (($error == UPLOAD_ERR_PARTIAL) || ($error == UPLOAD_ERR_NO_FILE))
	{
		show_mobile_message(_L('user_avatar_upload_error_empty', '若要更改頭像，請上傳圖片。'));
	}
	else if (($error == UPLOAD_ERR_INI_SIZE) || ($error == UPLOAD_ERR_FORM_SIZE))
	{
		show_mobile_message(_L('user_avatar_upload_error_filesize', '圖片過大，請先把圖片縮小至 1 MB 以內。'));
	}
	else if ($error != UPLOAD_ERR_OK)
	{
		show_mobile_message(_L('user_avatar_upload_error_other', '圖片上傳錯誤，請稍後再試。'));
	}

	$tmp_name = $_FILES['avatar_image']['tmp_name'];

	$size = getimagesize($tmp_name);

	$width = $size[0];
	$height = $size[1];
	$type = $size['mime'];

	if ($width > 2048 || $height > 2048)
	{
		show_mobile_message(_L('user_avatar_upload_error_resolution', '圖片尺寸過大，請先把圖片縮小至 512x512 以內。'));
	}

	$animated_gif_fix = '';
	if ($type == 'image/jpeg')
	{
		$tmp_path = $save_path . '_tmp.jpg';
	}
	else if ($type == 'image/gif')
	{
		$tmp_path = $save_path . '_tmp.gif';
		$animated_gif_fix = '[0]'; // Get first frame only
	}
	else if ($type == 'image/png')
	{
		$tmp_path = $save_path . '_tmp.png';
	}
	else
	{
		show_mobile_message(_L('user_avatar_upload_error_image_format', '圖片格式錯誤，請確認圖片格式是 jpeg 、 gif 或 png。'));
	}

	if (!move_uploaded_file($tmp_name, $tmp_path))
	{
		show_mobile_message(_L('user_avatar_upload_error_other', '圖片上傳錯誤，請稍後再試。'));
	}

	exec('/usr/bin/convert ' . escapeshellarg($tmp_path . $animated_gif_fix) . ' -auto-orient -resize 200x200^ -gravity center -extent 200x200 ' . escapeshellarg($save_path . '.png'));
	exec('/usr/bin/convert ' . escapeshellarg($save_path . '.png') . ' -resize 100x100 ' . escapeshellarg($save_path . '_100x100.png'));
	exec('/usr/bin/convert ' . escapeshellarg($save_path . '.png') . ' -resize 60x60 ' . escapeshellarg($save_path . '_60x60.png'));

	unlink($tmp_path);

	show_mobile_message(_L('user_avatar_upload_success', '上傳頭像成功，更新頭像可能需要數分鐘時間。'), _L('user_my_profile', '個人資料'), '/user.php?act=profile', 'info');
}

/* Redirect function */
elseif ($act == 'redirect')
{
	$url = empty($_REQUEST['direct']) ? '' : $_REQUEST['direct'];
	if($_SESSION['user_id'] > 0){
		ecs_header("Location: /$url");
		exit();
	} else {
		ecs_header("Location: /user.php?act=login");
		exit();
	}


}
elseif ($act == "load_tracking_info")
{
	include_once(ROOT_PATH .'includes/cls_json.php');
	include_once(ROOT_PATH .'includes/lib_passport.php');
	$json = new JSON();

	$result = array('error' => 0, 'message' => '', 'content' => '');
	$res = $orderController->get_order_tracking_info($_REQUEST['order_sn']);
	$result['message'] = $res;
	die($json->encode($res));
}
/* affiliate */
elseif ($act == "affiliate")
{
	//Blocking Wholesale Customer
	if(in_array(@$_SESSION['user_id'],$userController->getWholesaleUserIds()))
	    show_message('批發客戶請直接聯絡營業員訂購');
	if ($_SESSION['user_id'] <= 0)
	{
		header('Location: /user.php?act=login');
		exit;
	}
	
	assign_template();
	
	$position = assign_ur_here(0,  _L('user_affiliate', '推廣大使'));
	$smarty->assign('page_title',  $position['title']);	// 页面标题
	$smarty->assign('ur_here',     $position['ur_here']);  // 当前位置
	$smarty->assign('keywords',    htmlspecialchars($_CFG['shop_keywords']));
	$smarty->assign('description', htmlspecialchars($_CFG['shop_desc']));
	$CategoryController = new Yoho\cms\Controller\CategoryController();
	$t1 = $CategoryController->getTopCategories(0);
	$smarty->assign('t1',              $t1);
	$smarty->assign('helps',       get_shop_help());

	// New Member Referral Program
	$sql = "SELECT oi.user_id, oi.order_sn, al.money, al.point, al.time FROM " . $ecs->table('affiliate_log') . " al " .
			"LEFT JOIN " . $ecs->table('order_info') . " oi ON oi.order_id = al.order_id " .
			"WHERE al.user_id = " . $user_id . " ORDER BY al.time DESC LIMIT 30";
	$affLog = $db->getAll($sql);
	foreach($affLog as $key => $log)
	{
		$order_sn = substr($log['order_sn'], 0, strlen($log['order_sn']) - 5) . "***" . substr($log['order_sn'], -2, 2);
		$affLog[$key]['info'] = sprintf($_LANG['user_affiliate_separate_info'], $order_sn, $log['point']);
		$affLog[$key]['date'] = local_date($_CFG['date_format'], $log['time']);
	}

	$smarty->assign('aff_log', $affLog);

	$cfg_config = unserialize($GLOBALS['_CFG']['affiliate']);
	$config = $cfg_config['config'];
	$content = '';
	if($config['level_point_all'] || $config['instant_money_discount'])
	{
		$level_order_num = $config['level_order_num'] > 0 ? sprintf($_LANG['user_affiliate_order_time'], ($config['level_order_num'] > 1 ? $config['level_order_num'] > 0 : '')) : '';
		$level_order_amount = $config['level_order_amount'] > 0 ? sprintf($_LANG['user_affiliate_order_amount'], $config['level_order_amount']) : '';
		$level_point_all = !empty($config['level_point_all']) ? (strpos($config['level_point_all'], '%') === false ? '$' . value_of_integral($config['level_point_all']) : sprintf($_LANG['user_affiliate_whole_order'], $config['level_point_all'])) : '';
		$instant_money_discount = !empty($config['instant_money_discount']) ? (strpos($config['instant_money_discount'], '%') === false ? '$' . $config['instant_money_discount'] : sprintf($_LANG['user_affiliate_whole_order'], $config['instant_money_discount'])) : '';
		$content = sprintf($_LANG['user_affiliate_type_content_0'], $level_order_num, $level_order_amount) . (!empty($instant_money_discount) ? sprintf($_LANG['user_affiliate_type_content_1'], $instant_money_discount) : '') . (!empty($level_point_all) ? sprintf($_LANG['user_affiliate_type_content_2'], $level_point_all) : '');
		if($_CFG['lang'] == 'en_us') {
			$content = (!empty($instant_money_discount) ? sprintf($_LANG['user_affiliate_type_content_0'], $instant_money_discount) : '') .
			sprintf($_LANG['user_affiliate_type_content_1'], $level_order_amount, $level_order_num) .
			(!empty($level_point_all) ? sprintf($_LANG['user_affiliate_type_content_2'], $level_point_all) : '');
		}
	}
	$smarty->assign('user_affiliate_type_intro_content', sprintf($_LANG['user_affiliate_type_intro_content_pre'], $level_point_all) . sprintf($_LANG['user_affiliate_type_intro_content'], $content));

	// Referal Program
	$cookie_expire = $affiliate['config']['expire'] . $_LANG['expire_unit'][$affiliate['config']['expire_unit']];
	$smarty->assign('cookie_expire', $cookie_expire);

	$sql = "SELECT ar.* " .
            "FROM " . $GLOBALS['ecs']->table('affiliate_rewards') . " as ar " .
            "WHERE ar.user_id = '" . $user_id . "' " .
            "ORDER BY ar.reward_id DESC";
    $data = $GLOBALS['db']->getAll($sql);
	$rewards_list = array();
    foreach ($data as $row)
    {
        $row['start_date'] = local_date($_CFG['date_format'], $row['start_time']);
        $row['end_date'] = local_date($_CFG['date_format'], $row['end_time']);
        
        $rewards_list[$row['reward_id']] = $row;
    }
    
    $reward_ids = array_map(function ($row) { return $row['reward_id']; }, $data);
    
    $sql = "SELECT ard.*, g.goods_id, g.goods_name, c.cat_id, c.cat_name " .
            "FROM " . $GLOBALS['ecs']->table('affiliate_rewards_detail') . " as ard " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('goods') . " as g ON g.goods_id = ard.related_id AND ard.type = 'goods' " .
            "LEFT JOIN " . $GLOBALS['ecs']->table('category') . " as c ON c.cat_id = ard.related_id AND ard.type = 'category' " .
            "WHERE ard.reward_id " . db_create_in($reward_ids) . " " .
            "ORDER BY ard.reward_id, ard.type";
    $details = $GLOBALS['db']->getAll($sql);
    
    foreach ($details as $detail)
    {
		if ($detail['type'] == 'goods')
		{
			$detail['link'] = build_uri('goods', array('gid' => $detail['goods_id'], 'gname' => $detail['goods_name']));
		}
		elseif ($detail['type'] == 'category')
		{
			$detail['link'] = build_uri('category', array('cid' => $detail['cat_id'], 'cname' => $detail['cat_name']));
		}

        if (!isset($rewards_list[$detail['reward_id']]['details']))
        {
            $rewards_list[$detail['reward_id']]['details'] = array($detail);
        }
        else
        {
            $rewards_list[$detail['reward_id']]['details'][] = $detail;
        }
	}
	$sql = "SELECT arl.*, oi.order_sn, oi.add_time, g.goods_name " .
			"FROM " . $ecs->table('affiliate_rewards_log') . " as arl " .
			"LEFT JOIN " . $ecs->table('order_info') . " as oi ON oi.order_id = arl.order_id " .
			"LEFT JOIN " . $ecs->table('goods') . " as g ON g.goods_id = arl.goods_id " .
			"WHERE arl.`user_id` = '" . $user_id . "' " .
			"AND arl.`reward_amount` > 0 " .
			"ORDER BY arl.order_id DESC LIMIT 30";
	$logdb = $db->getAll($sql);
	foreach ($logdb as $key => $rt)
	{
		$rt['order_sn'] = substr($rt['order_sn'], 0, strlen($rt['order_sn']) - 5) . "***" . substr($rt['order_sn'], -2, 2);
		$rt['add_date_formatted'] = local_date($_CFG['date_format'], $rt['add_time']);
		$rt['reward_amount_formatted'] = price_format($rt['reward_amount'], false);
		$rt['goods_url'] = build_uri('goods', array('gid' => $rt['goods_id']));
		
		$logdb[$key] = $rt;
	}
	$smarty->assign('logdb', $logdb);
	$smarty->assign('shopname', $_CFG['shop_name']);
	$smarty->assign('userid', $user_id);
	$smarty->assign('shop_logo', 'yohohk/img/logo.gif');
	$smarty->assign('shopurl', $ecs->url());
	$smarty->assign('rewards_list', $rewards_list);
	$smarty->display('user_affiliate.html');

} elseif ($act == 'programs') { /* 特殊會員 */
    if (!$_REQUEST['program']) {
        header('Location: user.php');
        exit;
	}
	if (empty($_SESSION['user_id']))
	{
		header('Location: /user.php?act=login');
		exit;
	}

	assign_template();

    // 特殊會員
    $now = strtotime(local_date('Y-m-d H:i:s'));
    $sql = "SELECT urp.program_id, urp.program_code, IFNULL(urpl.program_name, urp.program_name) as program_name, IFNULL(urpl.description, urp.description) as description, urp.theme_color, urp.status, urp.verifiy_type, urp.special_code, urp.require_proof " .
        " FROM " . $ecs->table("user_rank_program") . " as urp " .
        "LEFT JOIN " . $ecs->table('user_rank_program_lang') . " as urpl ON urpl.program_id = urp.program_id AND urpl.lang = '" . $_CFG['lang'] . "' " .
        " WHERE urp.program_code = '" . $_REQUEST['program'] . "' ";
    $program = $db->getRow($sql);
    $program_id = $program['program_id'];

    $record = $db->getRow("SELECT * FROM " . $ecs->table("user_rank_program_application") . " WHERE program_id = $program_id AND user_id = $_SESSION[user_id]");
	if (!empty($record)) {
		$stage = $record['stage'];
		$now = strtotime(local_date('Y-m-d H:i:s'));
		$expiry = strtotime($record['expiry']);
		if ($expiry < $now) {
			$stage = RANK_APPLICATION_FURTHER;
			if ($program['status'] == RANK_PROGRAM_END) {
				$stage = 5;
			}
		}
	} else {
		$stage = -1;
		if(!in_array($program['status'], [RANK_PROGRAM_HIDE, RANK_PROGRAM_SHOW])) {
			header('Location: user.php');
			exit;
		}
	}
	$record['expiry'] = date("Y-m-d", strtotime($record['expiry']));
    $smarty->assign('program', $program);
    $smarty->assign('stage', $stage);
    $smarty->assign('record', $record);
    $smarty->assign('program_code', $_REQUEST['program']);
    $smarty->assign('shopname', $_CFG['shop_name']);
    $smarty->assign('userid', $user_id);
    $smarty->assign('shop_logo', 'yohohk/img/logo.gif');
	$smarty->assign('helps', get_shop_help());
    $smarty->assign('shopurl', $ecs->url());
    $smarty->display('user_programs.html');
} elseif ($act == 'validate_program') { /* 验证特殊會員 */
	if (empty($_SESSION['user_id']))
	{
		$_SESSION['back_act_from_event'] = 'user.php?' . strip_tags($_SERVER['QUERY_STRING']);
		header('Location: /user.php?act=login');
		exit;
	}
    $verify = empty($_REQUEST['verify']) ? '' : trim($_REQUEST['verify']);
    if ($verify) {
        $success = $userRankController->validateProgram($_REQUEST);
        if ($success['error'] == 0) {
            $program = $success['program'];
            $msg = sprintf(_L('user_email_validate_success', '%s 您好，您的電郵地址 %s 已通過驗證%s'),
                '', $success['email'],
                $incomplete ? _L('user_email_validate_complete_profile', '，補填個人資料，送積分哦！') : '');
            show_mobile_message($msg, $program['program_name'], '/programs/' . $program['program_code']);
        }
    }
    show_mobile_message(_L('user_email_validate_failed', '驗證失敗，請確認你的驗證連結是否正確'));
}
function show_login_page()
{
	global $smarty, $login_faild, $_CFG;
	
	$cache_id = sprintf('%X', crc32($login_faild . '-' . $_CFG['lang']));
	
	if (!$smarty->is_cached('login.html', $cache_id))
	{
		assign_template();
		
		$position = assign_ur_here(0,  _L('account_login', '會員登入'));
		$smarty->assign('page_title',  $position['title']);	// 页面标题
		$smarty->assign('ur_here',     $position['ur_here']);  // 当前位置
		$smarty->assign('keywords',    htmlspecialchars($_CFG['shop_keywords']));
		$smarty->assign('description', htmlspecialchars($_CFG['shop_desc']));
		$CategoryController = new Yoho\cms\Controller\CategoryController();
		$t1 = $CategoryController->getTopCategories(0);
		$smarty->assign('t1',              $t1);
		$smarty->assign('helps',       get_shop_help());
		
		$smarty->assign('login_faild', $login_faild);
		$smarty->assign('user_area',   user_area());
		$smarty->assign('user_countries', implode(',', available_login_countries()));
	}
	$smarty->display('login.html', $cache_id);
	exit;
}
function show_sms_verification_page()
{
	global $smarty, $login_faild, $_CFG;
	$username = $_SESSION['user_name'];
	$cache_id = sprintf('%X', crc32($_CFG['lang'].(($username)?'-'.$username : '')));
	
	if (!$smarty->is_cached('sms_verification.html', $cache_id))
	{
		assign_template();
		
		$position = assign_ur_here(0,  _L('user_sms_verification', 'SMS驗證'));
		$smarty->assign('page_title',  $position['title']);	// 页面标题
		$smarty->assign('ur_here',     $position['ur_here']);  // 当前位置
		$smarty->assign('keywords',    htmlspecialchars($_CFG['shop_keywords']));
		$smarty->assign('description', htmlspecialchars($_CFG['shop_desc']));
		$CategoryController = new Yoho\cms\Controller\CategoryController();
		$t1 = $CategoryController->getTopCategories(0);
		$smarty->assign('t1',              $t1);
		$smarty->assign('helps',       get_shop_help());
		
		$smarty->assign('username', $username);
		$smarty->assign('user_area',   user_area());
		$smarty->assign('user_countries', implode(',', available_login_countries()));
	}
	$smarty->display('sms_verification.html', $cache_id);
	exit;
}
function get_mobile_user_orders($user_id, $number = 10, $start = 0)
{
	require_once(ROOT_PATH . '/includes/lib_clips.php');

	// Get PayMe Payment Code
	$paymeController = new Yoho\cms\Controller\PaymeController();
	$payme_id_sql = "SELECT pay_id FROM " . $GLOBALS['ecs']->table('payment') . " WHERE pay_code = 'payme'";
	$payme_id = $GLOBALS['db']->getOne($payme_id_sql);

	$arr = array();
	$sql = "SELECT order_id, order_sn, order_status, shipping_status, pay_status, add_time, reviewed, pay_id, " .
		"(goods_amount + shipping_fee + insure_fee + pay_fee + pack_fee + card_fee + tax - discount) AS total_fee, ".
		"(money_paid + order_amount) as total_amount ". // howang: money_paid(已付金額) + order_amount(應付金額) = 訂單總金額
		" FROM " .$GLOBALS['ecs']->table('order_info') .
		" WHERE user_id = '$user_id' AND order_status != " . OS_CANCELED . " ORDER BY add_time DESC";
	$res = $GLOBALS['db']->SelectLimit($sql, $number, $start);
	
	while ($row = $GLOBALS['db']->fetchRow($res))
	{
		// Update existing payment status in case of incomplete PayMe payments
		if ($row['pay_id'] == $payme_id && $row['pay_status'] == PS_UNPAYED) {
			// Update status
			$logId = get_paylog_id($row['order_id'], $pay_type = PAY_ORDER);
			if ($paymeController->updateOrderPaymeStatus($row['order_id'], $logId) === true) {
				$row['order_status'] = OS_CONFIRMED;
				$row['pay_status'] = PS_PAYED;
			}
		}

		$cancel_btn = '<a href="/user.php?act=cancel_order&order_id=' . $row['order_id'] . '" '.
			'onclick="if (!confirm(\'' . _L('user_order_cancel_prompt', '您確認要取消該訂單嗎？') . '\')) return false;" '.
			'class="btn btn-danger btn-xs"><span>' . _L('user_order_cancel', '取消') . '</span></a>';
		
		$receive_btn = '<a href="/user.php?act=affirm_received&order_id=' . $row['order_id'] . '" '.
			'onclick="if (!confirm(\'' . _L('user_confirm_delivery_prompt', '你確認已經收到貨物了嗎？') .'\')) return false;" '.
			'class="btn btn-primary btn-xs"><span>' . _L('user_confirm_delivery', '確認收貨') . '</span></a>';
		
		$pay_btn = '<a href="/user.php?act=order_detail&order_id=' . $row['order_id'] . '&payl=1" '.
			'class="btn btn-primary btn-xs"><span>' . _L('user_order_pay', '付款') . '</span></a>';
		
		$view_btn = '<a href="/user.php?act=order_detail&order_id=' . $row['order_id'] . '" '.
			'class="btn btn-default btn-xs"><span>' . _L('user_order_view', '查看訂單') . '</span></a>';
		
		$review_btn  = '<a href="/review.php?order_id=' .$row['order_id']. '" '.
			'class="btn btn-success btn-xs"><span>' . _L('user_order_review', '評價') . '</span></a>';
		
		$can_review = ((!in_array($row['order_status'], array(OS_UNCONFIRMED, OS_CANCELED, OS_INVALID, OS_RETURNED, OS_WAITING_REFUND))) &&
					   ($row['pay_status'] == PS_PAYED) &&
					   (in_array($row['shipping_status'], array(SS_SHIPPED, SS_RECEIVED))) &&
					   (!$row['reviewed']));
		
		if ($row['order_status'] == OS_UNCONFIRMED)
		{
			$row['handler'] = $cancel_btn . ' ' . $pay_btn;
		}
		else if ($row['order_status'] == OS_SPLITED)
		{
			if ($row['shipping_status'] == SS_SHIPPED)
			{
				$row['handler'] = $receive_btn;
				if ($can_review)
				{
					$row['handler'] .= ' ' . $review_btn;
				}
			}
			else if ($row['pay_status'] == PS_UNPAYED)
			{
				$row['handler'] = $pay_btn;
			}
			else if ($can_review)
			{
				$row['handler'] = $review_btn;
			}
			else
			{
				$row['handler'] = $view_btn;
			}
		}
		else
		{
			if($row['order_status'] == OS_CONFIRMED && $row['pay_status'] != PS_PAYED && $row['shipping_status'] == SS_UNSHIPPED)
			{
				$row['handler'] = $pay_btn;
			}
			else if ($can_review)
			{
				$row['handler'] = $review_btn;
			}
			else
			{
				$row['handler'] = $view_btn;
			}
		}
		// Order step
		if (in_array($row['order_status'], array(OS_CANCELED, OS_INVALID, OS_RETURNED, OS_WAITING_REFUND)))
		{
			$row['step'] = 0;
		}
		else if (in_array($row['pay_status'], array(PS_UNPAYED, PS_PAYING)))
		{
			$row['step'] = 1;
		}
		else if (!in_array($row['shipping_status'], array(SS_SHIPPED, SS_RECEIVED)))
		{
			$row['step'] = 2;
		}
		else if ($row['shipping_status'] != SS_RECEIVED)
		{
			$row['step'] = 3;
		}
		else if (!$row['reviewed'])
		{
			$row['step'] = 4;
		}
		else
		{
			$row['step'] = 5;
		}
		
		$row['shipping_status'] = ($row['shipping_status'] == SS_SHIPPED_ING) ? SS_PREPARING : $row['shipping_status'];
		
		// Order Status, YOHO's way
		if (in_array($row['order_status'], array(OS_CANCELED, OS_INVALID, OS_RETURNED, OS_WAITING_REFUND)))
		{
			$row['order_status'] = '<em class="order-status order_status_'.$row['order_status'].' order_step os'.$row['step'].'">'.$GLOBALS['_LANG']['os'][$row['order_status']].'</em>';
		}
		else if (in_array($row['pay_status'], array(PS_UNPAYED, PS_PAYING)))
		{
			$row['order_status'] = '<em class="order-status pay_status_'.$row['pay_status'].' order_step os'.$row['step'].'">'.$GLOBALS['_LANG']['ps'][$row['pay_status']].'</em>';
		}
		else if ($row['order_status'] == OS_SPLITED && $row['reviewed'] == 0)
		{
			$row['order_status'] = '<em class="shipping_status_'.$row['shipping_status'].' order_step os'.$row['step'].'">'.$GLOBALS['_LANG']['wait_review'].'</em>';
		}
		else
		{
			$row['order_status'] = '<em class="order-status shipping_status_'.$row['shipping_status'].' order_step os'.$row['step'].'">'.($row['shipping_status'] == SS_RECEIVED ? $GLOBALS['_LANG']['ss_received'] : $GLOBALS['_LANG']['ss'][$row['shipping_status']]).'</em>';
		}
		
		$arr[] = array(
			'order_id'	   => $row['order_id'],
			'order_sn'	   => $row['order_sn'],
			'order_time'	 => local_date($GLOBALS['_CFG']['time_format'], $row['add_time']),
			'order_status'   => $row['order_status'],
			'total_fee'	  => price_format($row['total_fee'], false),
			'total_amount'   => price_format($row['total_amount'], false),
			'handler'		=> $row['handler']
		);
	}
	
	return $arr;
}

?>
