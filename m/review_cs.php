<?php
define('IN_ECS', true);

require dirname(__FILE__) . '/includes/init.php';
$commentController = new Yoho\cms\Controller\CommentController();

if (empty($_SESSION['user_id'])) {
    show_message(_L('review_error_not_logged_in', '若要參與用戶滿意度調查，請先登入。'), _L('review_login_link', '會員登入'), '/user.php?act=login');
}

$review_id = isset($_REQUEST['review_id']) ? intval($_REQUEST['review_id']) : 0;

if (empty($review_id) || empty($_REQUEST['act'])) {
    show_message(_L('review_error_review_not_found', '你輸入了無效的連結'));
}

if ($_REQUEST['act'] == 'fill_review') {
    $review = $db->getRow("SELECT * FROM " . $ecs->table("review_crm") . " WHERE review_id = $review_id");
    if (empty($review) || $review['user_id'] != $_SESSION['user_id']) {
        show_message(_L('review_error_review_not_found', '你輸入了無效的連結'));
    }
    $admin = $db->getRow(
        "SELECT au.user_id, au.user_name, au.avatar, SUM(ctc.duration) as answer FROM " . $ecs->table("crm_twilio_call") . "ctc " .
        "LEFT JOIN " . $ecs->table("admin_user") . " au ON au.user_id = ctc.admin_id " .
        "WHERE sent_evaluate = $review_id GROUP BY ctc.admin_id"
    );
    $time = intval($admin['answer']);
    if ($time >= 3600) {
        $total .= floor($time / 3600) . _L('review_hour', '小時');
        $time -= floor($time / 3600) * 3600;
    }
    if ($time >= 60) {
        $total .= floor($time / 60) .  _L('review_minute', '分');
        $time -= floor($time / 60) * 60;
    }
    if ($time > 0) {
        $total .= $time . _L('review_second', '秒');
    }
    $admin['answer'] = $total;
    $smarty->assign('admin',   $admin);
    assign_template();
    $position = assign_ur_here(0, _L('review_survey_title', '用戶滿意度調查'));
    $smarty->assign('page_title',   $position['title']);
    $smarty->assign('ur_here',      $position['ur_here']);
    $smarty->assign('keywords',     htmlspecialchars($_CFG['shop_keywords']));
    $smarty->assign('description',  htmlspecialchars($_CFG['shop_desc']));
    $smarty->assign('helps',        get_shop_help());
    $CategoryController = new Yoho\cms\Controller\CategoryController();
    $t1 = $CategoryController->getTopCategories(0);
    $smarty->assign('t1',           $t1);

    $smarty->assign('review',       $review);
    assign_dynamic('index');
    $smarty->display('review_cs.html');
} elseif ($_REQUEST['act'] == 'submit_review') {
    $review = $db->getRow("SELECT * FROM " . $ecs->table("review_crm") . " WHERE review_id = $review_id");
    if (empty($review) || $review['user_id'] != $_SESSION['user_id']) {
        show_message(_L('review_error_review_not_found', '你輸入了無效的連結'));
    }
    if (!empty($review['complete'])) {
        show_message(_L('review_error_order_reviewed', '你已經參與過用戶滿意度調查了。'));
    }

    $score1 = empty($_REQUEST['cs_rating1']) ? 0 : intval(trim($_REQUEST['cs_rating1']));
    $score2 = empty($_REQUEST['cs_rating2']) ? 0 : intval(trim($_REQUEST['cs_rating2']));
    $comment = trim($_REQUEST['comment']);

    if (empty($score1) || empty($score2)) {
        show_message(_L('review_error_rating_missing', '提交前請先評分哦！'));
    }

    $db->query("UPDATE " . $ecs->table("review_crm") . " SET score1 = $score1, score2 = $score2, comment = '$comment', complete = CURRENT_TIMESTAMP WHERE review_id = $review_id");
    show_message(_L('review_success', '感謝您參與用戶滿意度調查，我們已收到您填寫的問卷。'), _L('global_go_home', '返回首頁'), '/');
}