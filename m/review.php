<?php

/***
 * review.php
 * by howang 2014-08-08
 *
 * Customer satisfaction survey
 ***/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require(dirname(dirname(__FILE__)) . '/includes/lib_order.php');
$commentController = new Yoho\cms\Controller\CommentController();
if (empty($_SESSION['user_id']))
{
	show_mobile_message('若要參與用戶滿意度調查，請先登入。','會員登入','/user.php?act=login');
}

$act = isset($_REQUEST['act']) ? $_REQUEST['act'] : '';

$order_id = isset($_REQUEST['order_id']) ? intval($_REQUEST['order_id']) : 0;

if (!empty($order_id))
{
	$sql = "SELECT order_id, order_sn, user_id, order_status, shipping_status, pay_status, salesperson, reviewed FROM " . $ecs->table('order_info') . " WHERE order_id = '" . $order_id . "'";
	$order = $db->getRow($sql);
}

if (empty($order) || ($order['user_id'] != $_SESSION['user_id']))
{
	show_mobile_message('若要參與用戶滿意度調查，請先購物。');
}
/*
if ($order['reviewed'])
{
	show_mobile_message('你已經參與過用戶滿意度調查了。');
}
*/
if (in_array($order['order_status'], array(OS_UNCONFIRMED, OS_CANCELED, OS_INVALID, OS_RETURNED, OS_WAITING_REFUND)) || !in_array($order['shipping_status'], array(SS_SHIPPED, SS_RECEIVED)) || $order['pay_status'] != PS_PAYED)
{
	show_mobile_message('交易完成後才可以參與用戶滿意度調查。');
}

$sql = "SELECT s.*, au.avatar FROM " . $ecs->table('salesperson') . " s left join ".  $ecs->table('admin_user') ." au on (s.admin_id = au.user_id) WHERE s.`available` = 1 AND s.`sales_name` = '" . $order['salesperson'] . "'";
$staff = $db->getRow($sql);

$is_online = ($order['salesperson'] == 'online');

if ($act == 'submit_review')
{
	if (!$order['reviewed']) {
		$required_fields = array('shop_rating3');
		if ($is_online)
		{
			array_unshift($required_fields, 'shop_rating1', 'shop_rating2');
		}
		foreach ($required_fields as $required_field)
		{
			if (empty($_POST[$required_field]))
			{
				show_mobile_message('提交前請先評分哦！');
			}
		}
		
		if (!empty($staff))
		{
			$required_fields = array('sales_rating1','sales_rating2','sales_rating3');
			foreach ($required_fields as $required_field)
			{
				if (empty($_POST[$required_field]))
				{
					show_mobile_message('提交前請先評分哦！');
				}
			}
			
			$score1 = max(min(intval($_POST['sales_rating1']), 10), 1);
			$score2 = max(min(intval($_POST['sales_rating2']), 10), 1);
			$score3 = max(min(intval($_POST['sales_rating3']), 10), 1);
			$comment = empty($_POST['sales_comment']) ? '' : $_POST['sales_comment'];
			
			$sales_review = array(
				'sales_id' => $staff['sales_id'],
				'user_id' => $_SESSION['user_id'],
				'order_id' => $order_id,
				'score1' => $score1,
				'score2' => $score2,
				'score3' => $score3,
				'comment' => $comment,
				'time' => gmtime()
			);
			
			$db->autoExecute($ecs->table('review_sales'), $sales_review, 'INSERT');
			
			// Update the cached scores for the salesperson
			$sql = "SELECT AVG(`score1`) as score1, AVG(`score2`) as score2, AVG(`score3`) as score3 FROM " . $ecs->table('review_sales') . " WHERE `sales_id` = '" . $staff['sales_id'] . "'";
			$scores = $db->getRow($sql);
			$sql = "UPDATE " . $ecs->table('salesperson') . " SET `score1` = '" . $scores['score1'] . "', `score2` = '" . $scores['score2'] . "', `score3` = '" . $scores['score3'] . "' WHERE `sales_id` = '" . $staff['sales_id'] . "'";
			$db->query($sql);
		}
		
		$shop_score1 = isset($_POST['shop_rating1']) ? max(min(intval($_POST['shop_rating1']), 10), 1) : 0;
		$shop_score2 = isset($_POST['shop_rating2']) ? max(min(intval($_POST['shop_rating2']), 10), 1) : 0;
		$shop_score3 = max(min(intval($_POST['shop_rating3']), 10), 1);
		$shop_comment = empty($_POST['shop_comment']) ? '' : $_POST['shop_comment'];
		$shop_comment = empty($shop_comment) ? (!empty($_POST['share_attempted']) ? 'Shared on Facebook' : '') : $shop_comment;
		
		$shop_review = array(
			'user_id' => $_SESSION['user_id'],
			'order_id' => $order_id,
			'score1' => $shop_score1,
			'score2' => $shop_score2,
			'score3' => $shop_score3,
			'comment' => $shop_comment,
			'time' => gmtime()
		);
		
		$db->autoExecute($ecs->table('review_shop'), $shop_review, 'INSERT');
		
		$sql = "UPDATE " . $ecs->table('order_info') . " SET `reviewed` = 1 WHERE `order_id` = '" . $order_id . "'";
		$db->query($sql);
	}
	if($_CFG['comment_open'] == 1) {
		$goods_list = [];
		foreach ($_REQUEST as $key => $value) {
			if(substr($key, 0, strlen('rating_')) === 'rating_') {
				$goods_id = str_replace('rating_', '', $key);
				$goods_list[$goods_id]['rank'] = $value;
				unset($_REQUEST[$key]);
			} elseif(substr($key, 0, strlen('comment_')) === 'comment_') {
				$goods_id = str_replace('comment_', '', $key);
				$goods_list[$goods_id]['content'] = $value;
				unset($_REQUEST[$key]);
			}

		}
		foreach ($_FILES as $key => $value) {
			if(substr($key, 0, strlen('img_')) === 'img_') {
				$goods_id = str_replace('img_', '', $key);
				$goods_list[$goods_id]['image']['file'] = $value;
				unset($_FILES[$key]);
			}
		}
		$error_list = [];
		foreach($goods_list as $goods_id => $goods) {
			$goods['type'] = $commentController::COMMENT_TYPE_GOODS;
			$goods['lei']  = 1;
			$goods['id']   = $goods_id;
			$can = $commentController->user_can_comment($goods['type'], $goods_id);
			if(!$can['can_comment']) $error_list[$goods_id] = $can['message'];
			if($goods['rank'] > 0 && $can['can_comment']) {
				$res = $commentController->add_comment($goods);
				if($res['error']) $error_list[$goods_id] = $res['error'];
			}
		}
    }
	show_mobile_message('感謝您參與用戶滿意度調查，我們已收到您填寫的問卷。','返回訂單列表','/user.php?act=order_list');
}
else
{
	assign_template();
	$position = assign_ur_here(0, '用戶滿意度調查');
	$type = ($_REQUEST['type'] == 'goods') ? $_REQUEST['type'] : 'order';
	if($order['reviewed'])$type = 'goods';
	$smarty->assign('type',	$type);
	$smarty->assign('page_title',	$position['title']);	 // 页面标题
	$smarty->assign('ur_here',		$position['ur_here']); // 当前位置
	/* meta information */
	$smarty->assign('keywords',		htmlspecialchars($_CFG['shop_keywords']));
	$smarty->assign('description',	htmlspecialchars($_CFG['shop_desc']));
	
	$smarty->assign('helps',         get_shop_help()); // 网店帮助
	
	$CategoryController = new Yoho\cms\Controller\CategoryController();
    $t1 = $CategoryController->getTopCategories(0);
    $smarty->assign('t1',              $t1);
	
	$smarty->assign('order',         $order);
	$smarty->assign('staff',         $staff);
	$smarty->assign('is_online',     $is_online);
	
	$smarty->assign('stars',         range(1,10));
	
	$sql = "SELECT goods_id, goods_price FROM " . $ecs->table('order_goods') . " WHERE order_id = '" . $order_id . "' HAVING goods_price = MAX(goods_price) LIMIT 1";
	$row = $db->getRow($sql);
	$share_url = desktop_url(build_uri('goods', array('gid'=>$row['goods_id'])));
	$smarty->assign('share_url',     $share_url);
	if($_CFG['comment_open'] == 1) {
		/* 订单商品 */
		$goods_list = order_goods($order_id);
		foreach ($goods_list AS $key => $value)
		{
			$goods_list[$key]['market_price']     = price_format($value['market_price'], false);
			$goods_list[$key]['goods_price']      = price_format($value['goods_price'], false);
			$goods_list[$key]['subtotal']         = price_format($value['subtotal'], false);
			$goods_list[$key]['user_can_comment'] = $commentController->user_can_comment($commentController::COMMENT_TYPE_GOODS, $value['goods_id']);
			$goods_thumb = $db->getOne("SELECT `goods_thumb` FROM " . $ecs->table('goods') . " WHERE `goods_id` = '" . $value['goods_id'] . "'");
			$goods_list[$key]['goods_thumb'] = get_image_path($value['goods_id'], $goods_thumb, true);
			$goods_list[$key]['goods_url'] = build_uri('goods', array('gid' => $value['goods_id'], 'gname' => $value['goods_name']));
		}

		$smarty->assign('goods_list',     $goods_list);
    }
	/* 页面中的动态内容 */
	assign_dynamic('index');
	
	$smarty->display('review.html');
}

exit;

?>