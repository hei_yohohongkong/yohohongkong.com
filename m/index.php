<?php

/**
 * ECSHOP mobile首页
 * ============================================================================
 * * 版权所有 2005-2012 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liuhui $
 * $Id: index.php 15013 2010-03-25 09:31:42Z liuhui $
*/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

if ((DEBUG_MODE & 2) != 2)
{
    $smarty->caching = true;
}

/*------------------------------------------------------ */
//-- 判断是否存在缓存，如果存在则调用缓存，反之读取相应内容
/*------------------------------------------------------ */
/* 缓存编号 */
$cache_id = sprintf('%X', crc32($_SESSION['user_rank'] . '-' . user_area() . '-' . $_CFG['lang']));
$CategoryController = new Yoho\cms\Controller\CategoryController();
$topicController = new Yoho\cms\Controller\TopicController();
$index_promotion = $topicController->get_index_promotion();

$luckyDrawController = new Yoho\cms\Controller\LuckyDrawController();



$lucky_draw_id = $lucky_draw_active[0];

if (!$smarty->is_cached('index.html', $cache_id))
{
    assign_template('',array(),'home');

    $position = assign_ur_here();
    $smarty->assign('page_title',      $position['title']);    // 页面标题
    $smarty->assign('ur_here',         $position['ur_here']);  // 当前位置
    $cat_list = cat_list(0, 0, false, 0, false);
//    $rank = $CategoryController->getRankItem(12);
    $indexCatSession = $CategoryController->getIndexCatSession();

    $smarty->assign('cat_session',     $indexCatSession);
    $smarty->assign('t1',              $menu_cat);
    $smarty->assign('helps',           get_shop_help());
    $index_hot_cat = $CategoryController->getHotCategory();
    $smarty->assign('hot_cat',         $index_hot_cat);
//    $smarty->assign('ranks',           $rank);
    
    $smarty->assign('promotion',         $index_promotion['promotion']);
    $smarty->assign('featured',           $index_promotion['featured']);
    /* meta information */
    $smarty->assign('keywords',        htmlspecialchars($_CFG['shop_keywords']));
    $smarty->assign('description',     htmlspecialchars($_CFG['shop_desc']));

    // 手機版首頁鏈結
    $smarty->assign('mobile_links',    get_mobile_index_links());

    // 廣告輪播
    $smarty->assign('carousels',       get_homepage_carousels());

    // 店長推薦 - 最新「精品」
//	$smarty->assign('bestgoods',       category_get_goods_all(0,0,0,0,'rand()','desc',0,6,0,"AND g.is_best=1"));

	// 熱賣產品 - 最新「熱銷」
//	$smarty->assign('hotgoods',        category_get_goods_all(0,0,0,0,'rand()','desc',0,6,0,"AND g.is_hot=1"));

    // 新產品 - 最新「新品」
//    $smarty->assign('newgoods',        category_get_goods_all(0,0,0,0,'rand()','desc',0,6,0,"AND g.is_new=1"));

    // // 達人推薦 Powered by howang http://www.howang.hk/
    // $tatsujin = array(
    //     array(
    //         'title' => '美女裝備',
    //         'desc' => '俗語說“沒有醜女人，只有懶女人”，所以美麗是努力的成果！快來看看各款最新最熱賣的美容儀器：洗臉、清潔、保濕、彩光美肌、激光脫毛、去黑眼圈、去暗瘡、美唇、減淡皺紋等等針對妳不同的需要！',
    //         'list' => category_get_goods_all(0,0,0,0,'shownum','desc',0,6,0,"AND g.goods_id IN (77, 620, 87, 99, 695, 268)")
    //     ),
    //     array(
    //         'title' => '流動娛樂',
    //         'desc' => '為你提供無時無刻暢快的流動娛樂！',
    //         'list' => category_get_goods_all(0,0,0,0,'shownum','desc',0,6,0,"AND g.goods_id IN (167, 669, 326, 1368, 731, 1364)")
    //     ),
    //     array(
    //         'title' => '休閒閱讀',
    //         'desc' => '在這裡你會找到各大品牌推出的電子書閱讀器，既護眼又輕巧，而且輕輕鬆鬆查字典、做筆記，配件亦一應俱全，為你帶來愉悅的閱讀體驗！',
    //         'list' => category_get_goods_all(0,0,0,0,'shownum','desc',0,6,0,"AND g.goods_id IN (167, 43, 421, 296, 1334, 1336)")
    //     ),
    //     array(
    //         'title' => '工作狂人',
    //         'desc' => '工作至上的你一定要看看各款工作上的好伴侶！',
    //         'list' => category_get_goods_all(0,0,0,0,'shownum','desc',0,6,0,"AND g.goods_id IN (301, 1368, 489, 343, 691, 344)")
    //     ),
    //     array(
    //         'title' => '潮人潮物',
    //         'desc' => '世界千變萬化，科技日新月異，作為潮人一族的你怎麼會錯過全球最新最快的潮物？',
    //         'list' => category_get_goods_all(0,0,0,0,'shownum','desc',0,6,0,"AND g.goods_id IN (705, 557, 739, 684, 629, 100)")
    //     ),
    //     array(
    //         'title' => '送禮佳品',
    //         'desc' => '無論你想送禮物給父母、伴侶、朋友，友和為你捜羅多款價廉物美的送禮佳品！',
    //         'list' => category_get_goods_all(0,0,0,0,'shownum','desc',0,6,0,"AND g.goods_id IN (712, 171, 167, 325, 615, 246)")
    //     ),
    // );
    // $smarty->assign('tatsujin', $tatsujin);

    assign_dynamic('index');
}

$smarty->display('index.html',$cache_id);

function get_mobile_index_links()
{
    global $_CFG, $db, $ecs;

    $sql = "SELECT mil.rec_id, " .
                "IFNULL(mill.title, mil.title) as title, " .
                "IFNULL(mill.url, mil.url) as url, " .
                "IFNULL(mill.img, mil.img) as img, " .
                "mil.bg_color " .
            "FROM " . $ecs->table('mobile_index_links') . " as mil " .
            "LEFT JOIN " . $ecs->table('mobile_index_links_lang') . " as mill " .
                "ON mill.rec_id = mil.rec_id AND mill.lang = '" . $_CFG['lang'] . "' " .
            "WHERE mil.enabled = 1 " .
            "ORDER BY mil.sort_order ASC";
    return $db->getAll($sql);
}

?>