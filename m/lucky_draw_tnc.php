<?php

/***
* vip.php
* by Anthony@YOHO 20170721
*
* YOHO Hong Kong VIP page
***/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

if ((DEBUG_MODE & 2) != 2)
{
    $smarty->caching = true;
}

$user_id = $_SESSION['user_id'];

$luckyDrawController = new Yoho\cms\Controller\LuckyDrawController();
$lucky_draw_active = $luckyDrawController->getActiveLuckyDrawEvent();

if (empty($lucky_draw_active)) {
	header('Location: /luckydraw');
	exit; 
}

$lucky_draw_id = $lucky_draw_active[0];
$lucky_draw_info = $luckyDrawController->getLuckyDrawEventInfo($lucky_draw_id);

setcookie('m_display', $display, gmtime() + 86400 * 7);

/* 赋值固定内容 */
assign_template();
$smarty->assign('page_title', $_LANG['lucky_draw']);  // 页面标题
$smarty->assign('ur_here',    $_LANG['lucky_draw']);  // 当前位置
$smarty->assign('helps', get_shop_help());              // 网店帮助
$smarty->assign('lucky_draw_info',    $lucky_draw_info);  // 当前位置

$smarty->display('lucky_draw_tnc.html');
