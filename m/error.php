<?php

/**
 * ECSHOP 首页文件
 * ============================================================================
 * * 版权所有 2005-2012 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liubo $
 * $Id: index.php 17217 2011-01-19 06:29:08Z liubo $
*/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

if ((DEBUG_MODE & 2) != 2)
{
    $smarty->caching = true;
}

$cache_id = sprintf('%X', crc32($_SESSION['user_rank'] . '-' . user_area() . '-' . $_CFG['lang'].'404'));
$CategoryController = new Yoho\cms\Controller\CategoryController();
$fleashdealController = new Yoho\cms\Controller\FlashdealController();

if (!$smarty->is_cached('error.html', $cache_id))
{
    assign_template('',array(),'home');
    
    $position = assign_ur_here();
    $smarty->assign('page_title',      $position['title']);    // 页面标题
    $smarty->assign('ur_here',         $position['ur_here']);  // 当前位置

    $t1 = $CategoryController->getTopCategories(0);
    $smarty->assign('t1',              $t1);
    /* meta information */
    $smarty->assign('keywords',        htmlspecialchars($_CFG['shop_keywords']));
    $smarty->assign('description',     htmlspecialchars($_CFG['shop_desc']));
    $smarty->assign('helps',           get_shop_help());       // 网店帮助
    
    /* 页面中的动态内容 */
    assign_dynamic('error');
}

$smarty->display('error.html', $cache_id);

?>