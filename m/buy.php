<?php

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

$goods_id = empty($_REQUEST['goods_id']) ? (empty($_REQUEST['id']) ? 0 : $_REQUEST['id']) : $_REQUEST['goods_id'];

if (!empty($goods_id))
{
	header('Location: /cart?step=link_buy&goods_id=' . $goods_id);
	exit;
}
else
{
	header('Location: /cart');
	exit;
}

?>