var checkPaymePaymentTime = function() {
    function checkPaymePaymentTimeAction() {
        if (moment().isSameOrAfter(payme_request_expiry_date_time)) {
        disablePaymeButton(payme_error_message.payCodeExpired);
        return;
        }
        setTimeout(checkPaymePaymentTimeAction, 5000);
    };

    checkPaymePaymentTimeAction();
};

var checkPayStatus = function() {
    var order_id = YOHO.mobile.data.orderInfo.order_id;
    if (!order_id) return;

    var initialCheckInterval = 5000;
    var checkInterval = initialCheckInterval;
    var checkPayStatus = function () {
        $.ajax({
        url: '/ajax/order_status.php',
        type: 'post',
        dataType: 'json',
        data: {
            'act': 'pay_status',
            'order_id': order_id
        },
        success: function(data) {
            if (data.status == 1) {
            var pay_status = data.pay_status;
            if (pay_status == 2) // PS_PAYED
            {
                // Order paid, redirect to order detail page
                if ($('.payment_processing').length || data.payment_success == 1)
                {
                window.location = '/user.php?act=order_detail&order_id=' + order_id + '&paysuccess=1';
                }
                else
                {
                window.location = '/user.php?act=order_detail&order_id=' + order_id;
                }
                return;
            }
            }
            else if (data.status == 0) {
            if (data.hasOwnProperty('error') && data.error.length > 0)
            {
                YOHO.dialog.warn(data.error);
                setTimeout(function () {
                window.location = '/respond.php?code=' + $('#pay_code').val();
                }, 3000)
            }
            }

            checkInterval = initialCheckInterval;
            setTimeout(checkPayStatus, checkInterval);
        },
        error: function(xhr) {

            checkInterval = checkInterval * 2;
            setTimeout(checkPayStatus, checkInterval);
        }
        });
    };
    setTimeout(checkPayStatus, checkInterval);
};

    var checkPaymePaymentStatus = function() {
        if ( typeof webhook === 'undefined' || !webhook || typeof payme_request_id === 'undefined' || !payme_request_id) return;
        if ( typeof moment === 'undefined') return; // Since Moment.js is loaded only when payme payment is selected

        var initialCheckInterval = ($('.payment_processing').length) ? 3000 : 5000;
        var checkInterval = initialCheckInterval;
        var checkPayStatus = function () {
        var normal_response = true;
        $.ajax({
        url: '/ajax/payme_order_status.php',
        type: 'post',
        dataType: 'json',
        data: {
            'payment_request_id': payme_request_id
        },
        success: function(data) {
            if (data.pay_status == 'FAILURE') {
                if (moment().isSameOrAfter(payme_request_expiry_date_time)) {
                    disablePaymeButton(payme_error_message.payCodeExpiredNotConfirmed);
                }
            } else if (data.pay_status == 'EXPIRED') {
                disablePaymeButton(payme_error_message.payCodeExpired);
                return;
            }

            checkInterval = initialCheckInterval;
            setTimeout(checkPayStatus, checkInterval);
        },
        error: function(xhr) {
            normal_response = false;
            checkInterval = checkInterval * 2;

            if (moment().isSameOrAfter(payme_request_expiry_date_time) && !normal_response) {
                disablePaymeButton(payme_error_message.payCodeExpiredNotConfirmed);
                return;
            }

            setTimeout(checkPayStatus, checkInterval);
        }
    });
};

setTimeout(checkPayStatus, checkInterval);
};

function disablePaymeButton(message) {
    $('.payme_mobile_payment_button_container').hide();
    $('.payme-error-message').html(message);
    $('.payme-error-container').show();
    setTimeout(function(){
        window.location.reload();
    }, 1000);
}