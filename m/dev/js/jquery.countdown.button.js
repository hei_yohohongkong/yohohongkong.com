(function(factory) {
    'use strict';

    if (typeof define === 'function' && define.amd) {
      define(['jquery'], factory);
    } else {
      factory(jQuery);
    }

})(function($) {
    'use strict';

    $.fn.countdownButton = function(options) {
      var settings = $.extend({
        seconds: 60,
        beforeClick: $.noop,
        onClick: $.noop,
        afterClick: $.noop,
      }, options);

      return this.click(function() {
        if (settings.beforeClick() !== false) {
          var btn = $(this);
          var btnLabel = btn.html();

          var timeoutSeconds = settings.seconds;
          var remainingSeconds = timeoutSeconds;

          btn.prop('disabled', true);

          var interval = setInterval(function() {
            btn.html(btnLabel + '<span style="color:red">(' + remainingSeconds-- + ')</span>');
          }, 1000);

          setTimeout(function() {
            clearInterval(interval);
            btn.html(btnLabel);
            btn.prop('disabled', false);
            settings.afterClick();
          }, (timeoutSeconds * 1000) + 1000);

          settings.onClick();
        }
      });
    }

});