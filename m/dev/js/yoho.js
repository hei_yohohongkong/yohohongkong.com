/* jshint browser: true, unused:true, undef:true */
/* globals jQuery, Bloodhound, FB, console, alert, confirm, sprintf */
/* jshint -W041 */

var YOHO = YOHO || {};

(function ($) {

YOHO.languages = YOHO.languages || {};

// Obtain localized string
YOHO._ = function(key, fallback)
{
	return (YOHO.languages && YOHO.languages.hasOwnProperty(key)) ? YOHO.languages[key] : (fallback || '');
};

YOHO.check = {
	// 判斷IE6
	isIE6: window.VBArray && !window.XMLHttpRequest,
	// 判斷是否為中英文，數字，下划線，減號
	isNick: function(str) {
		var nickReg = /^[\u4e00-\u9fa5A-Za-z0-9_-]+$/;
		return nickReg.test(str);
	},
	// 判斷是否為中英文，空格，減號，點
	isValidName: function(str) {
		var nickReg = /^[\u4e00-\u9fa5A-Za-z .-]+$/;
		return nickReg.test(str);
	},
	// 判斷郵箱
	isEmail: function(str) {
		var emailReg = /^([a-zA-Z0-9_.+-])+\@([a-z0-9][a-z0-9-]*\.)+[a-z]{2,5}$/i;
		return emailReg.test(str);
	},
	//判斷手機
	isMobile: function(str, ccc) {
		// 香港手提電話號碼: 5,6或9字頭，共8位數字
		var mobileReg = /^[456789][0-9]{7}$/;
		// 澳門手提電話號碼: 6字頭，共8位數字
		var macauReg = /^[6][0-9]{7}$/;
		// 台灣手機號碼: 09開頭，共10位數字 (開頭0字可省略)
		var taiwanReg = /^0?9[0-9]{8}$/;
		// 大陸手機號: 1字頭，第二位是345678其中一個，共11位數字
		var mainlandReg = /^1[345678][0-9]{9}$/;
		// 新加坡手機號碼: 8或9字頭，共8位數字
		var singaporeReg = /^[89][0-9]{7}$/;
		// 其他: 最少8位數字
		var otherReg = /^[0-9]{8,}$/;
		// 禁止輸入緊急號碼 (99字頭,11字頭,911)
		var invalidReg = /^(99|11|911)/;
		if (invalidReg.test(str)) return false;
		// Check depends on country calling code
		if ($.type(ccc) === 'string')
		{
			if (ccc == '852') // Hong Kong +852
				return mobileReg.test(str);
			else if (ccc == '853') // Macau +853
				return macauReg.test(str);
			else if (ccc == '886') // Taiwan +886
				return taiwanReg.test(str);
			else if (ccc == '86') // Mainland China +86
				return mainlandReg.test(str);
			else if (ccc == '65') // Singapore +65
				return singaporeReg.test(str);
			else // Other countries, accept all
				return otherReg.test(str);
		}
		// Fallback: accept all
		return (mobileReg.test(str) || macauReg.test(str) || taiwanReg.test(str) || mainlandReg.test(str) || singaporeReg.test(str));
	},
	// 判斷電話號碼
	isTelephone: function(str) {
		// 寬鬆地接受可有可無的+字頭，之後任意數字括號橫線空格最少8位
		var phoneReg = /^(\+\d+)?[0-9\(\)\- ]{8,}$/;
		return phoneReg.test(str);
	},
	// 判斷URL
	isUrl: function(str) {
		var urlReg = /^http:\/\/([\w-]+\.)+[\w-]+(\/[\w-.\/?%&=]*)?$/;
		return urlReg.test(str);
	},
	// 判斷數字
	isNum: function(str) {
		var numReg = /^[0-9]\d*$/;
		return numReg.test(str);
	}
};

YOHO.mobile = {
	data: {},
	flashdealCartTimer: {},

	init: function () {

		var _this = this;

		this.fixConsoleLog();
		this.sidebar.init();
		this.sidebar_category.init();
		this.topBarCat();
		this.searchBar();
		this.popover();
		this.show404Error();
		this.reviewOrderPrompt();
		this.carousel();
		this.showAlertMessage();
		this.languageSelector();
		this.splashAd();
		this.bottomBar();
		this.footerHelpDropDown();
		this.back2top();
		this.indexRank();
		this.filterMenu();
		this.checkoutProcess();
		this.quotationProcess();
		this.affiliateLink();
		this.affiliateLinkTrigger();
		this.affiliateCopy();
		this.flashdealCartCountDown();
		// this.goodsColumns();
		this.ulSelect();

		$("#goods_list_container").click(function(){
			_this.checkMissingPart();
		})

		$(".package_info_popup_close").click(function(){
			$(".package_info_popup").fadeOut();
		})
	},
	showAlertMessage: function(){
		$(document).on("click",'.page_alert_fake',function() {
			var alert_title = $('.page_alert.slide .item.active .title').html();
			var alert_content = $('.page_alert.slide .item.active .content').html();
			YOHO.mobile.modal.showInfo('<span class="red">'+ alert_title +'</span>',alert_content);
		});
	},
	bottomBar: function(){
		var url = window.location.pathname;
		// console.log(url.pathname);
		$('ul.yoho-bottom-bar li a[href="'+ url +'"]').parent().addClass('active');
		$('ul.yoho-bottom-bar li a').filter(function() {
			return this.href == url;
		}).parent().addClass('active');
	},
	footerHelpDropDown: function(){
		$('div.footer-help-menu a.help-menu-title').click(function () {
			var id = $(this).data('id');
			var p  = $(this).parent;
			if(!$('#'+id).hasClass('display'))
			{
				var last_div = $('div.footer-help-menu a.help-menu-title.active');
				var last_id = last_div.data('id');
				$('#'+last_id).hide();
				setTimeout(function(){
					$('#'+last_id).removeClass('display');
				},100);
				last_div.children("span").removeClass('caret-up');
				last_div.children("span").addClass('caret');
				last_div.removeClass('active');

				$('#'+id).show();
				setTimeout(function(){
					$('#'+id).addClass('display');
				},100);
				$(this).addClass('active');
				$(this).children("span").removeClass('caret');
				$(this).children("span").addClass('caret-up');

			} else {
				$('#'+id).hide();
				setTimeout(function(){
					$('#'+id).removeClass('display');
				},100);
				$(this).removeClass('active');
				$(this).children("span").removeClass('caret-up');
				$(this).children("span").addClass('caret');
			}
		});
	},
	fixConsoleLog: function() {
		// Workaround for browsers without console.log
		if (typeof window.console == 'undefined')
		{
			window.console = { log: function () {} };
		}
	},

	modal: {
		create: function (title, content, footer) {
			var modalHtml = ''+
			'<div class="modal fade" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true" style="display: none;"><div class="modal-dialog"><div class="modal-content">' +
				'<div class="modal-header">' +
					'<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>' +
					'<h4 class="modal-title" id="my-modal-title">' + (typeof title === 'string' ? title : '') + '</h4>' +
				'</div>' +
				'<div class="modal-body"><p>' + (typeof content === 'string' ? content : '') + '</p></div>' +
				'<div class="modal-footer">' + (typeof footer === 'string' ? footer : '') + '</div>' +
			'</div></div></div>';
			var modal = $(modalHtml);
			modal.appendTo('body');
			if (typeof title === 'object')
			{
				modal.find('.modal-title').append(title);
			}
			if (typeof content === 'object')
			{
				modal.find('.modal-body').append(content);
			}
			if (typeof footer === 'object')
			{
				modal.find('.modal-footer').append(footer);
			}
			return modal;
		},

		createAddCart: function (content) {
			var modalHtml = ''+
			'<div class="modal fade" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true" style="display: none;"><div class="modal-dialog addcart-dialog"><div class="modal-content">' +
				'<div class="modal-body"><p>' + (typeof content === 'string' ? content : '') + '</p></div>' +
			'</div></div></div>';
			var modal = $(modalHtml);
			modal.appendTo('body');
			return modal;
		},

		show: function (modal, options) {
			modal.show().modal(options);

			modal.on('hidden.bs.modal', function(){
				modal.remove();
			});
		},

		hide: function (modal) {
			modal.modal('hide');
		},

		createButton: function (title, css_class, dismiss) {
			var buttonHtml = '<button type="button" class="btn ' + css_class + '"' + (dismiss ? ' data-dismiss="modal"' : '') + '>' + title + '</button>';
			var button = $(buttonHtml);
			return button;
		},

		showInfo: function(title, body) {
			if (body === undefined)
			{
				body = title;
				title = YOHO._('global_modal_title_info', '資訊');
			}
			var content = '<div class="row"><div class="col-xs-2"><span class="glyphicon glyphicon-info-sign text-info" style="font-size: 36px; padding: 2px;"></span></div><div class="col-xs-10">' + body + '</div></div>';
			var closeBtn = this.createButton(YOHO._('global_OK_only', 'Okay'), 'btn-primary', true);
			var modal = this.create(title, content, closeBtn);
			this.show(modal);
			return modal;
		},

		showQuestion: function(title, body, ok_action, cancel_action) {
			if (body === undefined)
			{
				body = title;
				title = YOHO._('global_modal_title_question', '問題');
			}
			var content = '<div class="row"><div class="col-xs-2"><span class="glyphicon glyphicon-question-sign text-info" style="font-size: 36px; padding: 2px;"></span></div><div class="col-xs-10">' + body + '</div></div>';
			var cancelBtn = this.createButton(YOHO._('global_Cancel', 'Cancel'), 'btn-default', true);
			var okBtn = this.createButton(YOHO._('global_OK', 'OK'), 'btn-primary', false);
			var btns = $('<div>');
			cancelBtn.appendTo(btns);
			okBtn.appendTo(btns);
			var modal = this.create(title, content, btns);
			this.show(modal);
			okBtn.click(ok_action);
			cancelBtn.click(cancel_action);
			return modal;
		},

		showSuccess: function(title, body) {
			if (body === undefined)
			{
				body = title;
				title = YOHO._('global_modal_title_success', '成功');
			}
			var content = '<div class="row"><div class="col-xs-2"><span class="glyphicon glyphicon-ok-circle" style="font-size: 36px; padding: 2px; color: #40c040;"></span></div><div class="col-xs-10">' + body + '</div></div>';
			var closeBtn = this.createButton(YOHO._('global_OK_only', 'Okay'), 'btn-primary', true);
			var modal = this.create(title, content, closeBtn);
			this.show(modal);
			return modal;
		},
		showAddCartSuccess: function(body) {
			var content = '<div class="row"><div class="col-xs-2"><svg class="dialog-icon"><use xlink:href="/yohohk/img/yoho_svg.svg#dialog-ok"></use></svg></div><div class="col-xs-10">' + body + '</div></div>';
			var modal = this.createAddCart(content);
			this.show(modal);
			return modal;
		},

		showWarning: function(title, body) {
			if (body === undefined)
			{
				body = title;
				title = YOHO._('global_modal_title_warning', '警告');
			}
			var content = '<div class="row"><div class="col-xs-2"><span class="glyphicon glyphicon-warning-sign" style="font-size: 36px; padding: 2px; color: #fed403;"></span></div><div class="col-xs-10">' + body + '</div></div>';
			var closeBtn = this.createButton(YOHO._('global_OK_only', 'Okay'), 'btn-primary', true);
			var modal = this.create(title, content, closeBtn);
			this.show(modal);
			return modal;
		},

		showError: function(title, body) {
			if (body === undefined)
			{
				body = title;
				title = YOHO._('global_modal_title_error', '錯誤');
			}
			var content = '<div class="row"><div class="col-xs-2"><span class="glyphicon glyphicon-exclamation-sign text-danger" style="font-size: 36px; padding: 2px; color: #f00;"></span></div><div class="col-xs-10">' + body + '</div></div>';
			var closeBtn = this.createButton(YOHO._('global_OK_only', 'Okay'), 'btn-primary', true);
			var modal = this.create(title, content, closeBtn);
			this.show(modal);
			return modal;
		}
	},

	createAlert: function (message, type) {
		if (type === undefined)
		{
			type = 'danger';
		}
		var alertHtml = '' +
		'<div class="alert alert-' + type + ' alert-dismissible" role="alert">' +
			'<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>' +
			message +
		'</div>';
		return $(alertHtml);
	},

	sidebar: {
		init: function () {
			var _this = this;

			$('button.sidebar-toggle').click(function () {
				_this.toggleSidebar();
			});
			$('.sidebar-bg').click(function () {
				_this.toggleSidebar(false);
			});
			$('.sidebar a[data-toggle!="collapse"]').click(function () {
				_this.toggleSidebar(false);
			});

			// Rotate expand indicator in collapse toggle
			var rotateIndicator = function (collapseID, startAngle, endAngle) {
				var $indicator = $('a[data-toggle="collapse"][data-target="#' + collapseID + '"] span.expand-indicator');
				if ($indicator.length)
				{
					// jQuery hack to animate CSS3 transform
					// ref: http://stackoverflow.com/a/15191130
					$({deg: startAngle}).animate({deg: endAngle}, {
						duration: 300,//350,
						step: function(now) {
							$indicator.css({
								'transform': 'rotate(' + now + 'deg)'
							});
						}
					});
				}
			};
			$('.sidebar .collapse').on('show.bs.collapse', function (event) {
				if ($(this).get(0) != $(event.target).get(0)) return;
				rotateIndicator($(this).attr('id'), 0, 90);
			});
			$('.sidebar .collapse').on('hide.bs.collapse', function (event) {
				if ($(this).get(0) != $(event.target).get(0)) return;
				rotateIndicator($(this).attr('id'), 90, 0);
			});

			// Attempt to cater iOS Safari bug that make the sidebar not scrollable initially
			if (navigator.userAgent.match(/(iPad|iPhone|iPod)/g))
			{
				$('.sidebar .collapse').on('shown.bs.collapse', function () {
					var $dummy = $('<div style="opacity: 0;">.</div>');
					$dummy.appendTo($('.sidebar'));
					setTimeout(function() {
						$dummy.remove();
					}, 100);
				});
			}

			// Swipe to close sidebar
			// also need to remove <a> from excludedElements list
			$('.sidebar').swipe({
				swipeLeft: function() {
					_this.toggleSidebar(false);
				},
				tap: function(event, target) {
					// Native HTMLElement click
					target.click();
				},
				threshold: 0,
				allowPageScroll: 'vertical',
				excludedElements: $.fn.swipe.defaults.excludedElements.replace('a,','')
			});
			$('.sidebar-bg').swipe({
				swipeLeft: function() {
					_this.toggleSidebar(false);
				},
				tap: function() {
					_this.toggleSidebar(false);
				},
				threshold: 0
			});
		},

		toggleSidebar: function (show) {
			var speed = 300;
			var $page = $('.page-content');
			var visible = $page.hasClass('sidebar-visible');
			if (show === undefined)
			{
				show = !visible;
			}
			else
			{
				if (show == visible) return;
			}

			if (show)
			{
				// Show sidebar
				$page.addClass('sidebar-visible');

				// To cater iOS 8 Safari bug
				if (navigator.userAgent.match(/(iPad|iPhone|iPod)/g))
				{
					$(document).scrollTop(0);
				}

				$page.css('width', $(window).width());
				$page.css('position', 'absolute');
				$page.animate({
					left: '280px'
				}, {
					duration: speed,
					queue: false
				});

				$('.sidebar').animate({
					left: 0
				}, {
					duration: speed,
					queue: false
				});

				$('.sidebar-bg').css('display','block');
				$('.sidebar-bg').animate({
					'opacity': 1
				}, {
					duration: speed,
					queue: false
				});
			}
			else
			{
				// Hide sidebar
				$page.removeClass('sidebar-visible');

				$page.animate({
					left: '0'
				}, {
					duration: speed,
					queue: false,
					done: function () {
						$page.css('width', '');
						$page.css('position', '');
					}
				});

				$('.sidebar').animate({
					left: '-305px'
				}, {
					duration: speed,
					queue: false
				});

				$('.sidebar-bg').animate({
					'opacity': 0
				}, {
					duration: speed,
					queue: false,
					done: function () {
						$('.sidebar-bg').css('display','none');
					}
				});
			}
		}
	},

	sidebar_category: {
		init: function () {
			var _this = this;

			$('a.sidebar-category-close').click(function () {
				_this.closeSidebarCategory();
			});
			$('a.sidebar-category-toggle').click(function () {
				$cid = $(this).data('cid');
				$cname = $(this).data('cname');
				_this.openSidebarCategory(undefined, $cid,$cname);
			});
			$('a.sidebar-category-hot-toggle').click(function () {
				_this.openSidebarCategory();
			});
			$('.sidebar-category-bg').click(function () {
				_this.openSidebarCategory(false);
			});
			$('.sidebar-category a[data-toggle!="collapse"]').click(function () {
				//_this.openSidebarCategorggle(false);
			});

			// Rotate expand indicator in collapse toggle
			var rotateIndicator = function (collapseID, startAngle, endAngle) {
				var $indicator = $('a[data-toggle="collapse"][data-target="#' + collapseID + '"] span.expand-indicator');
				if ($indicator.length)
				{
					// jQuery hack to animate CSS3 transform
					// ref: http://stackoverflow.com/a/15191130
					$({deg: startAngle}).animate({deg: endAngle}, {
						//duration: 300,//350,
						step: function(now) {
							$indicator.css({
								'transform': 'rotate(' + now + 'deg)'
							});
						}
					});
				}
			};
			$('.sidebar-category .collapse').on('show.bs.collapse', function (event) {
				if ($(this).get(0) != $(event.target).get(0)) return;
				rotateIndicator($(this).attr('id'), 0, 90);
			});
			$('.sidebar-category .collapse').on('hide.bs.collapse', function (event) {
				if ($(this).get(0) != $(event.target).get(0)) return;
				rotateIndicator($(this).attr('id'), 90, 0);
			});

			// Attempt to cater iOS Safari bug that make the sidebar-category not scrollable initially
			if (navigator.userAgent.match(/(iPad|iPhone|iPod)/g))
			{
				$('.sidebar-category .collapse').on('shown.bs.collapse', function () {
					var $dummy = $('<div style="opacity: 0;">.</div>');
					$dummy.appendTo($('.sidebar-category'));
					setTimeout(function() {
						$dummy.remove();
					}, 100);
				});
			}

			// Swipe to close sidebar-category
			// also need to remove <a> from excludedElements list
			/*
			$('.sidebar-category').swipe({
				swipeLeft: function() {
					_this.toggleSidebarCategory(false);
				},
				tap: function(event, target) {
					// Native HTMLElement click
					target.click();
				},
				threshold: 0,
				allowPageScroll: 'vertical',
				excludedElements: $.fn.swipe.defaults.excludedElements.replace('a,','')
			});
			$('.sidebar-category-bg').swipe({
				swipeLeft: function() {
					_this.toggleSidebarCategory(false);
				},
				tap: function() {
					_this.toggleSidebarCategory(false);
				},
				threshold: 0
			});
			*/
			
			var $content_height = ($(window).height() - $('.back-to-sidebar').outerHeight()) - 30;
			//$('.list-main-category').css('height', ($(window).height()));
			//$('.list-sub-category').css('height', ($(window).height()));
			
			$(".list-main-category a").click(function(n){
				if (!$(this).hasClass('special-cat')) {
					$('.category-tab-pane.active').removeClass('active');
					$('.silder-main-cat.active').removeClass('active');
					
					var $id = $(this).data('cid');
					var $name = $(this).data('cname');
					var insertPos = $(".list-sub-category");
					if($("#silder-cat-"+$id).length){
						$('#silder-cat-'+$id).addClass('active');
					}else{
						YOHO.mobile.ajaxGetCat($id,insertPos,$name);
					}
					$('#silder-main-cat-'+$id).addClass('active');
					$('#silder-cat-'+$id).addClass('active');
					$('#silder-cat-'+$id).scrollTop(0);
					$('#silder-cat-'+$id).animate({ height: $content_height+'px' }, 200);
				}
			});

			/*
			$(window).resize(function(){
				var $content_height = ($(window).height() - $('.back-to-sidebar').outerHeight());
				$('.list-main-category').css('height', ($content_height+100));
				$('.list-sub-category').css('height', ($content_height+100));
			});
			*/

		},

		toggleSidebarCategory: function (show, cid) {
			var speed = 300;
			var $page = $('.page-content');
			var visible = $page.hasClass('sidebar-category-visible');
			if (show === undefined)
			{
				show = !visible;
			}
			else
			{
				if (show == visible) return;
			}

			if (show)
			{
				// Show sidebar
				$page.addClass('sidebar-category-visible');

				// To cater iOS 8 Safari bug
				if (navigator.userAgent.match(/(iPad|iPhone|iPod)/g))
				{
					$(document).scrollTop(0);
				}

				$page.css('width', $(window).width());
				$page.css('position', 'absolute');
				$page.animate({
					left: '280px'
				}, {
					duration: speed,
					queue: false
				});

				$('.sidebar-category').animate({
					left: 0
				}, {
					duration: speed,
					queue: false
				});

				$('.sidebar-category-bg').css('display','block');
				$('.sidebar-category-bg').animate({
					'opacity': 1
				}, {
					duration: speed,
					queue: false
				});

				if(cid == "" || cid === undefined) cid = "hot-cat";
				$('.category-tab-pane.active').removeClass('active');
				$('.silder-main-cat.active').removeClass('active');
				$('#silder-cat-'+cid).addClass('active');
				$('#silder-main-cat-'+cid).addClass('active');
			}
			else
			{
				// Hide sidebar-category
				$page.removeClass('sidebar-category-visible');
				
				$page.animate({
					left: '0'
				}, {
					duration: speed,
					queue: false,
					done: function () {
						$page.css('width', '');
						$page.css('position', '');
					}
				});
				$('.sidebar-category').animate({
					left: '-'+$('.sidebar-category').outerWidth()+'px'
				}, {
					duration: speed,
					queue: false
				});

				$('.sidebar-category-bg').animate({
					'opacity': 0
				}, {
					duration: speed,
					queue: false,
					done: function () {
						$('.sidebar-category-bg').css('display','none');
					}
				});
			}
		},
		openSidebarCategory: function (show, cid,cname) {
			var speed = 300;
			var $page = $('.page-content');
			var visible = $page.hasClass('sidebar-category-visible');

			// Show sidebar
			$page.addClass('sidebar-category-visible');

			// To cater iOS 8 Safari bug
			if (navigator.userAgent.match(/(iPad|iPhone|iPod)/g))
			{
				$(document).scrollTop(0);
			}

			$page.css('width', $(window).width());
			$page.css('position', 'absolute');
			$page.animate({
				left: '280px'
			}, {
				duration: speed,
				queue: false
			});

			$('.sidebar-category').animate({
				left: 0
			}, {
				duration: speed,
				queue: false
			});

			$('.sidebar-category-bg').css('display','block');
			$('.sidebar-category-bg').animate({
				'opacity': 1
			}, {
				duration: speed,
				queue: false
			});

			if(cid == "" || cid === undefined) cid = "hot-cat";
			$('.category-tab-pane.active').removeClass('active');
			$('.silder-main-cat.active').removeClass('active');
			$('#silder-cat-'+cid).addClass('active');
			$('#silder-main-cat-'+cid).addClass('active');
			if($("#silder-cat-"+cid).length){
				$('#silder-cat-'+cid).addClass('active');
			}else{
				var insertPos = $(".list-sub-category");
				YOHO.mobile.ajaxGetCat(cid,insertPos,cname);
			}
		},
		closeSidebarCategory: function () {
			var speed = 300;
			var $page = $('.page-content');
			
			// Hide sidebar-category
			$page.removeClass('sidebar-category-visible');
			
			$page.animate({
				left: '0'
			}, {
				duration: speed,
				queue: false,
				done: function () {
					$page.css('width', '');
					$page.css('position', '');
				}
			});
			$('.sidebar-category').animate({
				left: '-'+$('.sidebar-category').outerWidth()+'px'
			}, {
				duration: speed,
				queue: false
			});

			$('.sidebar-category-bg').animate({
				'opacity': 0
			}, {
				duration: speed,
				queue: false,
				done: function () {
					$('.sidebar-category-bg').css('display','none');
				}
			});
		},
	},
	//ipad top bar 全部產品分類
	topBarCat: function() {
		$('.dropdown-submenu').hover(
			function() {
				var cur_cat_id = $(this).data('cid');
				if ($('#dropdown-menu-'+cur_cat_id+' li').length) {
					return;
				}
				$.ajax({
					url: '/ajax/getCategory.php',
					type: 'post',
					data: {
						cat_id: cur_cat_id,
						brand:	0
					},
					dataType:'json',
					success: function(data) {
						var bottom_cat = data.bottom_cat;
						var level2_length = bottom_cat.length;
						var html = '';
						for (var i=0; i<level2_length; i++) {
							var cat = bottom_cat[i];	
							html += '<li role="presentation" class="dropdown-header hidden-xs">'+cat.cat_name+'</li>';
							var child_length = cat.children.length;
							for (var j=0; j<child_length; j++) {
								html += '<li><a href="'+cat.children[j].url+'">'+cat.children[j].cat_name+'</a></li>';
							}
							if (i!=level2_length-1) {
								html += '<li role="presentation" class="divider"></li>';
							}	
						}
						$('#dropdown-menu-'+cur_cat_id).append(html);
					},
					error: function(xhr) {
						YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
					}
				});
			}
		);
	},

	//sidebar 產品分類 
	ajaxGetCat: function(cur_cat_id,$subCata,$name) {
		if(typeof cur_cat_id ==='undefined'){
			return;
		}
		$.ajax({
			url: '/ajax/getCategory.php',
			type: 'post',
			data: {
				cat_id: cur_cat_id,
				brand:	0
			},
			dataType:'json',
			success: function(data) {
				var bottom_cat = data.bottom_cat;
				var level2_length = bottom_cat.length;
				var html = '';
				html += '<div class="category-tab-pane active" id="silder-cat-'+cur_cat_id+'">';
				html += '<div class="category-tab_area">' +
							'<div class="category-tab">' +
								'<img src="/data/afficheimg/category/'+cur_cat_id+'_mobile_icon.png" alt="'+$name+'">' +
								'<span class="category-tab-text">'+$name+'</span>' +
							'</div>' +
						'</div>';
				for (var i=0; i<level2_length; i++) {
					var cat = bottom_cat[i];
					var children_no = cat["children"].length;		
					html += '<div class="subcategories-list">';	
					if (children_no == 0) {
						html += '<div class="subcategories-list-top">' +
									'<span class="subcategories-list-tittle">'+cat.cat_name+'</span>' +
								'</div>' +
								'<div class="subcategories-list-bottom">' +
									'<a class="subcategories-list-item" href="'+cat.url+'">' +
										'<div class="subcategories-list-img">' +
											'<img class="mini-product-image lazyload" alt="'+cat.cat_name+'" title="'+cat.cat_name+'" data-src="/'+cat.img+'" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="  />' +
										'</div>' +
										'<div class="title">'+cat.cat_name+'</div>' +
									'</a>' +
								'</div>';
					} else {
						html += '<div class="subcategories-list-top">' +
									'<span class="subcategories-list-tittle">'+cat.cat_name+'</span>' +
								'</div>' +
								'<div class="subcategories-list-bottom">';
						var child_length = cat.children.length;
						for (var j=0; j<child_length; j++) {
							html += '<a class="subcategories-list-item" href="'+cat.children[j].url+'">'+
										'<div class="subcategories-list-img">' +
											'<img class="mini-product-image lazyload" alt="'+cat.children[j].cat_name+'" title="'+cat.children[j].cat_name+'" data-src="/'+cat.children[j].img+'" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="  />' +
										'</div>' +

										'<div class="title">'+cat.children[j].cat_name+'</div>' +
									'</a>';
						}
						html += '</div>';
					}
					html += '</div>';
				}
				html += '</div>';
				$subCata.append(html);
			},
			error: function(xhr) {
				YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
			}
		});
	},

	searchBar: function () {
		var searchSuggestions = new Bloodhound({
			datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			sorter: function (a,b) {return a.sort-b.sort;},
			remote: '/ajax/search_suggestions.php?q=%QUERY&m=1',
			limit: 10
		});
		$(window).scroll(function() {
			if($(window).scrollTop() >= ($('.navbar-fixed-top').height()+150))
			{
				$('.top-menu').addClass('hidden');
				$('.top-menu-search').removeClass('hidden');
			} else {
				if($(document.activeElement).attr('type') == "text" && document.activeElement.className.match('scroll-search')){
					$('.top-menu').addClass('hidden');
					$('.top-menu-search').removeClass('hidden');
				} else {
					$('.top-menu').removeClass('hidden');
					$('.top-menu-search').addClass('hidden');
				}
			}

		});
		searchSuggestions.initialize();
		$("form[action='/search.php']").submit(function() {
			var input_val = $(this).find('input[name="keywords"]')[0].value;
			if(input_val == '' || typeof  input_val === 'undefined') {
				YOHO.mobile.modal.showWarning(YOHO._('global_search_alert', '請輸入關鍵字'));
				return false;
			}
		});
		$('.header-search-input').keypress(function (e) {
			if (e.which == 13)
			{
				var myForm = $(this).closest('form');
				myForm.submit();
				return true;
			}
		}).typeahead({hint: false}, {
			name: 'search-suggestions',
			displayKey: 'name',
			source: searchSuggestions.ttAdapter()
		});

		$('.searchbar input[name="keywords"]').keypress(function (e) {
			if (e.which == 13)
			{
				var myForm = $(this).closest('form');
				myForm.submit();
				return true;
			}
		}).typeahead({hint: false}, {
			name: 'searchbar-suggestions',
			displayKey: 'name',
			source: searchSuggestions.ttAdapter()
		});

		$('.navbar-toggle.search').click(function() {
			$(document).scrollTop(0);
			$('.searchbar').collapse('show');
			$('.searchbar input[name="keywords"]').focus();
		});

		// If both the input and submit button lost focus, hide search bar
		$('.searchbar input[name="keywords"]').blur(function() {
			setTimeout(function() {
				if (!$('.searchbar button[type="submit"]').is(':focus'))
				{
					$('.searchbar').collapse('hide');
				}
			}, 0);
		});
		$('.searchbar button[type="submit"]').blur(function() {
			setTimeout(function() {
				if (!$('.searchbar input[name="keywords"]').is(':focus'))
				{
					$('.searchbar').collapse('hide');
				}
			}, 0);
		});

		$('.navbar-toggle.shopping-cart').click(function() {
			window.location = '/cart';
		});
	},

	popover: function () {
		YOHO.mobile.data.activePopover = null;

		$('.yoho-popover').popover().click(function() {
			var $this = $(this);
			if (YOHO.mobile.data.activePopover !== null)
			{
				YOHO.mobile.data.activePopover.popover('toggle');
				if (YOHO.mobile.data.activePopover.get(0) == $this.get(0))
				{
					YOHO.mobile.data.activePopover = null;
					return;
				}
			}

			$this.popover('toggle');
			YOHO.mobile.data.activePopover = $this;
		});

		$('.yoho-popover').on('shown.bs.popover', function () {
			var $closebtn = $('<button type="button" class="close">&times;</button>');
			$('.popover-title').append($closebtn);
			$closebtn.click(function () {
				YOHO.mobile.data.activePopover.trigger('click');
			});
		});
	},

	show404Error: function () {
		if ($.cookie('error_request'))
		{
			alert(sprintf(YOHO._('global_404_error', '您所輸入的網址「%s」並不正確！'), $.cookie('error_request')));
			$.removeCookie('error_request');
		}
	},

	reviewOrderPrompt: function () {
		$('.review-order-prompt').on('closed.bs.alert', function () {
			// Set a cookie that will expire in 5 minutes
			var expire_date = new Date();
			expire_date.setMinutes(expire_date.getMinutes() + 5);
			$.cookie('hide_review_order_prompt', 1, { expires: expire_date, path: '/', domain: '.yohohongkong.com' });
		});
	},

	carousel: function () {
		console.log('kkk');
		// Add swipe support to Bootstrap Carousel with TouchSwipe
		// ref: http://lazcreative.com/blog/adding-swipe-support-to-bootstrap-carousel-3-0/
		// also need to remove <a> from excludedElements list for our index carousel to work
		$('.carousel-inner').swipe({
			swipeLeft: function() {
				$(this).parent().carousel('next');
			},
			swipeRight: function() {
				$(this).parent().carousel('prev');
			},
			tap: function(event, target) {
				// Native HTMLElement click
				target.click();
			},
			threshold: 75,
			allowPageScroll: 'vertical',
			excludedElements: $.fn.swipe.defaults.excludedElements.replace('a,','')
		});
		$('.review-carousel').carousel({
			interval: 4000
		});
		$('.page_alert').carousel({
			interval: 6000
		});

		$('.page_alert_slide').click(function(){
			//	var alert_title = $(this).find('.title').html();
			//	var alert_content = $(this).find('.content').html();
			//	var modal = YOHO.mobile.modal.showInfo(alert_title,alert_content);
			console.log('bbbbbbbbbbbbb');
		});	
	},

	goodsDetail: function () {
		var goods_id = $('.product-detail').data('goods-id');
		//get related articles
		var getRelatedArticles = function(goods_id) {	
			$.ajax({
				url: '/ajax/getRelatedArticles.php',
				type: 'post',
				data: {
					goods_id: goods_id
				},
				dataType:'json',
				success: function(data) {
					var promo_links_list = data.promo_links_list;
					var promo_links_list_len = promo_links_list.length;
					var topic_list = data.topic_list;
					var topic_list_len = topic_list.length;
					var article_list = data.article_list;
					var article_list_len = article_list.length;
					var html = '';
					if (promo_links_list_len!=0 || topic_list_len != 0 || article_list_len !=0) {
						html += '<table class="related-articles" style="border-collapse: collapse; border: 0px none transparent; margin: 8px 0;background-color: #fff;font-size: 16px;width: 100%;">' +
									'<tr class="goods_article_header" style="padding: 5px 0px;">' +
										'<td colspan="2" style="font-size: 18px;">' + YOHO._('related_articles', '相關文章') + '</td>'
									'</tr>';
						for (var i=0; i<promo_links_list_len; i++) {
							var link_img = promo_links_list[i].link_img;
							var link_url = promo_links_list[i].link_url;
							var link_newtab = promo_links_list[i].link_newtab;
							var link_title = promo_links_list[i].link_title;
							var link_desc = promo_links_list[i].link_desc;
							html += '<tr><td style="padding: 10px 4px;">';
							if (link_img) {
								if (link_url) {
									if (link_newtab) {
										html += '<a style="color: #4d4d4d;font-weight: bold;" href="' + link_url + '"target="_blank">'
									} else {
										html += '<a style="color: #4d4d4d;font-weight: bold;" href="' + link_url + '">';
									}		
								} 
								html += '<img src="' + link_img + '" title="' + link_title + '" width="25" height="25">';	
								if (link_url) {
									html += '</a>'
								}
							}
							html += '</td><td style="padding: 10px 4px; vertical-align: middle;">';
							if (link_url) {
								if (link_newtab) {
									html += '<a style="color: #4d4d4d;font-weight: bold;" href="' + link_url + '" target="_blank">';
								} else {
									html += '<a style="color: #4d4d4d;font-weight: bold;" href="' + link_url + '">';
								}	
							}
							html += link_title;
							if (link_url) {
								html += '</a>';
							}
							if (link_desc) {
								html += '<br>' +link_desc;
							}
							html += '</td></tr>';
						}
	
						if (promo_links_list_len!=0 && (topic_list_len != 0 || article_list_len !=0)) {
							html += '<tr class="hr" style="border-bottom:1px solid #f5f5f5;"><td colspan="2"></td></tr>';
						}
	
						for (var i=0; i<topic_list_len; i++) {
							var url = topic_list[i].url;
							var title = topic_list[i].title;
							var topic_id = topic_list[i].topic_id;
							html += '<tr><td style="padding: 10px 4px;">';
							html += '<a href="' + url + '" title="' + title + '"><img src="/data/afficheimg/topic/' + topic_id + '_icon.png" width="25" height="25"></a>';
							html += '</td><td style="padding: 10px 4px; vertical-align: middle;">';
							html += '<a href="' + url + '" style="color: #4d4d4d;font-weight: bold;">' + title + '</a>';
							html += '</td></tr>';
							
						}
						if (topic_list_len != 0 && article_list_len !=0) {
							html += '<tr class="hr" style="border-bottom:1px solid #f5f5f5;"><td colspan="2"></td></tr>';
						}
						for (var i=0; i<article_list_len; i++) {
							var url = article_list[i].url;
							var title = article_list[i].title;
							html += '<tr><td style="padding: 10px 4px;">';
							html += '<a href="' + url + '" title="' + title + '"><img src="/yohohk/img/product-article.png" width="25" height="25"></a>';
							html += '</td><td style="padding: 10px 4px; vertical-align: middle;">';
							html += '<a href="' + url + '" style="color: #4d4d4d;font-weight: bold;">' + title + '</a>';
							html += '</td></tr>';	
						}
						html += '</table>';	
						$(html).insertAfter(".goods_content");
					}
					
				},
				error: function(xhr) {
					YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
				}
			});
		};
		var main = function(dfd){
			var dfd = $.Deferred();
			var main_img = $('.carousel img, .gift-set img');
			var total_main_img = main_img.length;
			var imagesLoaded = 0;
			if (total_main_img) {
				main_img.each(function () {
					var img_url = $(this).data("src");
					$(this).attr('src',img_url)
				});
				main_img.on('load',function(){
					imagesLoaded++;
					if (imagesLoaded == total_main_img) {
						dfd.resolve();
					}
				});	
			} else {
				dfd.resolve();
			}	
			return dfd.promise();
		}
		$.when(main()).always(
			function() {
				var goods_list = $('.associated-accessories .horizontal-goods-list').eq(0);
				var goodsListImg = goods_list.find('img');
				goodsListImg.each(function () {
					var img_url = $(this).data("src");
					$(this).attr('src',img_url)
				});
				getRelatedArticles(goods_id);
				$('.also-bought-items img').each(function () {
					var img_url = $(this).data("src");
					$(this).attr('src',img_url)
				});
			}
		);
	
		//if have accessories and related goods in the same time, click second tab then render images of second tab
		$(".associated-accessories .tab").eq(1).one("click",function(){
			var goods_list = $('.associated-accessories .horizontal-goods-list').eq(1);
			var goodsListImg = goods_list.find('img');
			goodsListImg.each(function(){
				var img_url = $(this).data("src");
				$(this).attr('src',img_url)
			});
		});
		// Collapse and CSS max-width + width:auto didn't play well together
		// Reapply the HTML so the browser's rendering engine will re-calculate the image width
		$('#goods-details-desc').on('shown.bs.collapse', function () {
			var html = $(this).html();
			$(this).html(html);
		});

		$('.horizontal-goods-list-control a.btn').click(function () {
			var $ul = $(this).closest('.horizontal-goods-list').find('ul.row');
			var direction = $(this).closest('.horizontal-goods-list-control').hasClass('left') ? '-' : '+';
			$ul.animate({ 'scrollLeft': direction + '=' + $ul.width() });
			var $cur_pos = $ul.siblings('.bottom').find('.cur_pos');
			if(direction=="-"){
				if($cur_pos.data('pos')!=0){
					$cur_pos.prev().addClass('cur_pos');
					$cur_pos.removeClass('cur_pos');
				}
			}else{
				if($cur_pos.index()!=$cur_pos.siblings().size()){
					$cur_pos.next().addClass('cur_pos');
					$cur_pos.removeClass('cur_pos');
				}
			}

		});

		$('.associated-accessories .tabs span.tab').click(function () {
			var $this = $(this);
			var idx = $this.closest('.tabs').find('span.tab').index($this);
			
			$this.closest('.tabs').find('span.tab').removeClass('on');
			$this.addClass('on');
			
			var $contents = $this.closest('.associated-accessories').find('.horizontal-goods-list');
			$contents.hide();
			var $content = $contents.eq(idx);
			$content.show();
		});
		$('.product-modal .tabs span.tab').click(function () {
			var $this = $(this);
			var idx = $this.closest('.tabs').find('span.tab').index($this);
			
			$this.closest('.tabs').find('span.tab').removeClass('on');
			$this.addClass('on');
			
			var $contents = $this.closest('.product-modal').find('.products');
			$contents.hide();
			var $content = $contents.eq(idx);
			$content.show();
			if($this.find('#offerSetModal')){
				var $offer_info_contents = $this.closest('.modal').find('div.offer-set-info');
				$offer_info_contents.hide();
				var $offer_info_content = $offer_info_contents.eq(idx);
				$offer_info_content.show();
			}
		});

		// $('.product-modal .redeem-btn').not('.added').click(function () {
		// 	$(this).addClass('added');				
		// });

		$('.product-modal').on('show.bs.modal', function () {
			var modal = $(this);
			if(modal.find('.modal-dialog').length){
				return;
			}
			var modal_id = $(this).attr('id');
			var goods_id = $(this).data('goods-id');
			var act_id = $(this).data('act-id');
			var act_name = $(this).data('act-name');
			var act_type_ext = $(this).data('act-type-ext');			
			var modal_type = '';
			if(modal_id.indexOf('gift') != "-1") modal_type = 'gift';
			if(modal_id.indexOf('redeem') != "-1") modal_type = 'redeem';
			if(modal_id.indexOf('offerSet') != "-1") modal_type = 'offerSet';
			$.ajax({
				url: '/ajax/getGoodsOffer.php',
				type: 'post',
				data: {
					modal_type: modal_type,
					goods_id: goods_id,
					act_id: act_id,
				},
				dataType:'json',
				success: function(data) {
					var res = data;
					var html = '<div class="modal-dialog modal-lg">' +
									'<div class="modal-content">' +
										'<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true" class="times">&times;</span><span class="sr-only">Close</span></button>' +
										'<div class="top">' +
											'<div class="header">' + act_name + '</div>' +
										'</div>';
					if (modal_type == 'gift' || modal_type == 'redeem') {
						if (res.act_kind ==	"1") {
							//贈品modal
							html += '<div class="label red"><span>'+YOHO._('gifts ', '贈品')+'</span></div>' +
									'<div class="description"><span>';
							var need_to_select = 0;
							if (act_type_ext > 0) {
								need_to_select = 1;
								html += sprintf(YOHO._('gifts_add_to_cart_select', '請選擇下列贈品任意 %s 件'), act_type_ext);
							} else {
								html += YOHO._('gifts_add_to_cart_auto ', '指定產品及贈品將同時加入購物車');
							}
							html +=	'</span></div>' +
									'<div class="tabs">' +
										'<span class="tab on">'+YOHO._('gifts ', '贈品')+'</span>' +
									'</div>' +
									'<div class="products">';
							var gift_no = res.gift.length;
							if (typeof gift_no !== "undefined") {
								//請選擇下列贈品任意 1 件 or 指定產品及贈品將同時加入購物車
								for (var i=0; i< gift_no; i++ ) {
									var gift = res.gift[i];
									html += '<div class="product-container">' +
												'<div class="product-info-left">' +
													'<img src="/' + gift.goods_img + '" width="60" height="60" alt="' + gift.goods_name + '"/>' +
												'</div>' +
												'<div class="product-info-right">' +
													'<div class="product-name">' + gift.goods_name; 
									if (gift.qty > 1) html += '<span class="package_items_qty">&#215; ' + gift.qty + '</span>';
									html += '</div>' +
											'<div class="product-value">' +
												'<span class="red">' + gift.final_price_formatted + '<del>' + gift.market_price_formatted + '</del></span>' +
											'</div>';
									if (need_to_select) {
										html += '<form method="post">' +
													'<input type="hidden" name="act_id" value="' + act_id + '">' +
													'<input type="hidden" name="gift_id" value="' + gift.id + '">' +
													'<a onclick="javascript:addtocart('+"'addFavourable', this)"+'">' +
													'<div class="redeem-btn">' +
														YOHO._('goods_add ', '加入')+
														'</div>' +
													'</a>' +
												'</form>';
									}				
									html +=	'</div>' +
										'</div>';
								}	
							}else {
								//請選擇下列贈品任意 2/3/4 件
								var gifts = res.gift;
								for (var key in gifts) {
									var gift = gifts[key]
									html += '<div class="product-container">' +
												'<div class="product-info-left">' +
													'<img src="/' + gift.goods_img + '" width="60" height="60" alt="' + gift.goods_name + '"/>' +
												'</div>' +
												'<div class="product-info-right">' +
													'<div class="product-name">' + gift.goods_name; 
									if (gift.qty > 1) html += '<span class="package_items_qty">&#215; ' + gift.qty + '</span>';
									html += '</div>' +
											'<div class="product-value">' +
												'<span class="red">' + gift.final_price_formatted + '<del>' + gift.market_price_formatted + '</del></span>' +
											'</div>';
									if (need_to_select) {
										html += '<form method="post">' +
													'<input type="hidden" name="act_id" value="' + act_id + '">' +
													'<input type="hidden" name="gift_id" value="' + gift.id + '">' +
													'<a onclick="javascript:addtocart('+"'addFavourable', this)"+'">' +
													'<div class="redeem-btn">' +
														YOHO._('goods_add ', '加入')+
														'</div>' +
													'</a>' +
												'</form>';
									}				
									html +=	'</div>' +
										'</div>';
								}
							}
							html += '</div>';
							if (!need_to_select) {
								html += '<div class="product-action-form">' +
											'<form method="post">' +
											'<input type="hidden" name="act_id" value="' + act_id + '">' +
											'<a class="action-button" onclick="javascript:addtocart('+"'addFavourable', this)"+'">' +
												'<i class="fa fa-fw fa-shopping-cart"></i>'+YOHO._('goods_add_to_cart ', '加入購物車') +
											'</a>' +
											'</form>' +
										'</div>';
							}
						} else if (res.act_kind == "2") {
							//換購modal
							html += '<div class="label"><span>'+YOHO._('Redeem ', '換購')+'</span></div>' +
									'<div class="description"><span>'+YOHO._('redeem_add_to_cart_auto ', '指定產品及換購產品將同時加入購物車')+'</span></div>' +
									'<div class="products redeem">';

							var price_goods = res.gift_group_by_price.price_goods;
							for (var key in price_goods) {
								var gifts_list = price_goods[key].gift;
								if (typeof gifts_list.length === "undefined") {
									for (var key in gifts_list) {
										var gifts = gifts_list[key]
										html += ''+
										'<div class="product-container">' +
											'<div class="product-info-left">' +
											'<img src="/' + gifts.goods_img + '" width="60" height="60" alt="' + gifts.goods_name + '"/>' +
											'</div>' +
											'<div class="product-info-right">' +
												'<div class="product-name">' +
												gifts.goods_name +
												'</div>' +
												'<div class="product-value">' +
													'<span class="red">' + gifts.formated_price + '<del>' + gifts.final_price_formatted + '</del></span>' +
												'</div>' +
												'<form method="post">' +
													'<input type="hidden" name="act_id" value="' + act_id + '">' +
													'<input type="hidden" name="gift_id" value="' + gifts.id + '">' +
													'<a onclick="javascript:addtocart('+"'addFavourable', this)"+'">' +
													'<div class="redeem-btn">' +
														YOHO._('goods_add ', '加入')+
														'</div>' +
													'</a>' +
												'</form>' +
											'</div>' +
										'</div>';
									}
								} else {
									for (var i=0; i< gifts_list.length; i++ ) {
										var gifts = gifts_list[i]
										html += ''+
										'<div class="product-container">' +
											'<div class="product-info-left">' +
											'<img src="/' + gifts.goods_img + '" width="60" height="60" alt="' + gifts.goods_name + '"/>' +
											'</div>' +
											'<div class="product-info-right">' +
												'<div class="product-name">' +
												gifts.goods_name +
												'</div>' +
												'<div class="product-value">' +
													'<span class="red">' + gifts.formated_price + '<del>' + gifts.final_price_formatted + '</del></span>' +
												'</div>' +
												'<form method="post">' +
													'<input type="hidden" name="act_id" value="' + act_id + '">' +
													'<input type="hidden" name="gift_id" value="' + gifts.id + '">' +
													'<a onclick="javascript:addtocart('+"'addFavourable', this)"+'">' +
													'<div class="redeem-btn">' +
														YOHO._('goods_add ', '加入')+
														'</div>' +
													'</a>' +
												'</form>' +
											'</div>' +
										'</div>';
									}
								}
							}
							html += '</div>';
						}
					} else if (modal_type == 'offerSet') {
						html += '<div class="label"><span>'+YOHO._('offer_set', '套裝優惠')+'</span></div>'+
								'<div class="tabs">' +
									'<span class="tab on">'+YOHO._('offer_set', '套裝優惠')+'</span>' +
								'</div>' +
								'<div class="product_bindle_list">';
						$.each(res, function(count, package) {
							var act_id = package.act_id;
							var discount = package.discount;
							var total_price = package.total_price;
							var subtotal = package.subtotal;
							var gift_no = package.goods_list.length;
							html +=	'<div class="product_bindle_container">';
							for (var i=0; i< gift_no; i++ ) {
								var goods = package.goods_list[i];
								html += '<div class="product-container">' +
											'<div class="product-info-left">' +
												'<img src="/' + goods.goods_img + '" width="40" height="40" alt="' + goods.goods_name + '"/>' +
											'</div>' +
											'<div class="product-info-right">' +
												'<div class="product-name">' + goods.goods_name;
								if (goods.goods_number > 1) {
									html += ' <span class="package_items_qty">&#215; ' + goods.goods_number + '</span>';
								}					
								html +=	'</div>' +
									'</div>' +
								'</div>';
							}
							html += '<div class="bindle_info">' +
										'<div class="bindle_desc">'+
											YOHO._('offer_price', '組合價')+
											'<span class="red">'+
												discount+
											'</span>'+	
											'折 '+
											'<span class="red">'+
												total_price+
											'</span>'+
											'<del>'+
												subtotal+
											'</del>'+
										'</div>'+
										'<div class="bindle_action_form">'+
										'<form method="post" style="text-align: right;padding-right: 5px;">' +
											'<input type="hidden" name="package_id" value="' + act_id + '">' +
											'<a class="bindle-action-button" onclick="javascript:addtocart('+"'addPackage', this)"+'">' +
												YOHO._('goods_add_to_cart ', '加入購物車') +
											'</a>' +
										'</form>' +
										'</div>'+
									'</div>' +
								'</div>';
						});
						html += '</div>';
					}

					html += ' </div>' +
						'</div>' +
					'</div>';	
					modal.append(html);
				},
				error: function(xhr) {
					YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
				}
			});
		})
		
		var ul_width = $('.horizontal-goods-list ul.row').width();
		var li_width = $('.horizontal-goods-list ul.row li:not(.per-offer)').width();
		var lis_per = Math.floor(ul_width/li_width);
		$('.horizontal-goods-list').each(function () {	
			//優惠組合		
			if($(this).hasClass('offer')){
				var count = $(this).find('ul.row li').length;
			}else{
			//配件，相關產品，顧客購買的還有
				var total_lists = 0;
				$(this).find('ul.row li').each(function () {	
					if($(this).html()){
						total_lists += 1;
					}
				});			
				var count = total_lists/lis_per;
			}	
			var insert_html = '';
			for(var i = 0; i < count; i++){
				if(i==0){
					insert_html = '<ul class="bottom" data-goods-no="'+ lis_per +'">';
					insert_html += '<li class="cur_pos" data-pos=' + i + '></li>';
				}else{
					insert_html += '<li data-pos=' + i + '></li>';
				if(i==count-1){
					insert_html += '</ul>';
				}
				}			
			}
			$(this).append(insert_html);
			$(this).find('.bottom li').click(function () {
				var $ul = $(this).closest('.horizontal-goods-list').find('ul');
				var $cur_pos = $(this).parent().find('.cur_pos');
				var $from_pos_data = $cur_pos.data('pos');
				var $to_pos_data = $(this).data('pos');
				var $gap = $to_pos_data - $from_pos_data;
				if($(this).hasClass('cur_pos')){
					return;
				}else{
					$cur_pos.removeClass('cur_pos');
					$(this).addClass('cur_pos');
				}
				var direction = $gap<0 ? '-' : '+';
				$ul.animate({ 'scrollLeft': direction + '=' + $ul.width() * Math.abs($gap) });
			});

		});

		// Wrap YouTube / Vimeo videos to work with responsive CSS rules
		$('iframe[src*="www.youtube.com"],iframe[src*="player.vimeo.com"]').wrap('<div class="video-container"></div>');

		// Bind event to wish list button
		YOHO.mobile.wishList();

		// Bind event to pre-order form (if available)
		if ($('.goods-yuding').length)
		{
			YOHO.mobile.yuding();
		}

		// Scroll down a bit on mobile device
		if (($(window).width() < 768) && ($('.breadcrumb').length))
		{
			var breadcrumbTop = $('.breadcrumb').offset().top || 0;
			var breadcrumbHeight = $('.breadcrumb').innerHeight() || 0;
			var navBarHeight = $('.navbar-fixed-top').height() || 0;
			$(document).scrollTop(breadcrumbTop + breadcrumbHeight - navBarHeight);
		}

		$('.single-btn').click(function(){
			var $id = $(this).children('a').data('id');
			if($($id).hasClass('collapsed')){
				$($id).trigger('click');
				setTimeout(function() {
					$("html, body").animate({ scrollTop: ($($id).offset().top - $('.navbar-fixed-top').height() - 5) }, 300);
				}, 500);
			} else {
				$("html, body").animate({ scrollTop: ($($id).offset().top - $('.navbar-fixed-top').height() - 5) }, 300);
			}
		});
		

		$(document).on('click', '.double-btn.buy.flashdeal', function () {
			var $buybtn = $(this);
			var country = (YOHO.userArea) ? YOHO.userArea : 'hk';
			var $d = $('#goods_price + div').text();
			var $f = $('.double-btn.buy.flashdeal>a').data('fid');
			var $r = $('.double-btn.buy.flashdeal>a').data('rl');
			var $k = $('.double-btn.buy.flashdeal>a').data('key');
			var $id = $('.double-btn.buy.flashdeal>a').data('id');
			$r = 50 + $r;
			if ($buybtn.data('clicked'))
			{
				return;
			}
			// $buybtn.data('clicked', 1);
			var modalTitle = '';
			var captchaResponse = "";
			var recaptchaWidgId = null;
			var content = '<form id="buyForm" style="position:relateive">' +
				(country == 'CN' ? '<script src="https://recaptcha.net/recaptcha/api.js" async defer></script>' : '<script src="https://www.google.com/recaptcha/api.js" async defer></script>')+
				'<div style="transform:scale(0.90);-webkit-transform:scale(0.90);transform-origin:0 0;-webkit-transform-origin:0 0;" class="g-recaptcha" data-size="normal" id="g-recaptcha" data-callback="recaptchaCallback" data-sitekey="6LetXnYUAAAAAGG6czaEj9RicDJvovFd6A5jYhAp"></div>' +
				'</form>';
			var modal = YOHO.mobile.modal.create(modalTitle, content, '');
			$(modal).attr('id', 'flashdeal-cart-recaptcha');
			YOHO.mobile.modal.show(modal);
			
			$ftime = $('.flashdeal-countdown').data('nowTs');
			if($('input[id="goods_ap"]').val() == 1 || $ftime >= 1541836800) {
				$('div#flashdeal-cart-recaptcha>.modal-dialog>.modal-content').height($(window).height() - 20);
				$dialogdiv = $('#buyForm');
				$height = $('div#flashdeal-cart-recaptcha>.modal-dialog>.modal-content').height();
				$width = $('div#flashdeal-cart-recaptcha>.modal-dialog>.modal-content').width();
				var posx = (Math.random() * ($width - 305)).toFixed();
				var posy = (Math.random() * ($height - 80)).toFixed();
				$dialogdiv.css({
					'position':'fixed',
					'left':posx+'px',
					'top':posy+'px'
				})
			}
			recaptchaCallback = function () { // initializing callback function
				captchaResponse = grecaptcha.getResponse();
				$buybtn.data('clicked', 1);
				//Our AJAX POST
				$.ajax({
					url: '/ajax/cartbuy.php',
					type: 'post',
					data: {
						'ac': 'got',
						'pd': $d,
						'g': grecaptcha.getResponse(),
						'f': $f,
						'k': $k,
						'id': $id,
						'pp': $('input[name="goods_price"]').val()
					},
					dataType: "json",
					success: function(result) {
						if (result.status == 1)
						{
							YOHO.mobile.modal.hide(modal);
							YOHO.mobile.modal.showInfo(YOHO._('global_cart_add_success', '加入購物車成功！'));
							var qty = $buybtn.parent().find('.flashdeal-qty >num').text() - 1;
							$buybtn.parent().find('.flashdeal-qty >num').text(qty);

							if(Object.keys(YOHO.mobile.flashdealCartTimer).length == 0 || YOHO.mobile.flashdealCartTimer.expired == '') {
								YOHO.mobile.flashdealCartTimer.now_time = result.fg_info.now_time;
								YOHO.mobile.flashdealCartTimer.expired = result.fg_info.expired;
								YOHO.mobile.flashdealCartCountDown();
							}
						} else {
							if(result.type == 'captcha') {
								$('.g-recaptcha div div iframe').css('padding', '1px');
								$('.g-recaptcha div div iframe').css('border', '1px solid #dc3545');
								$buybtn.data('clicked',0);
							} else {
								YOHO.mobile.modal.showWarning(result.text);
								$buybtn.data('clicked',0);
							}
							setTimeout(function() {
								window.location.reload();
							}, 3000);
						}
						$buybtn.removeData('clicked');
					},
					error: function(xhr) {
						$buybtn.data('clicked',0);
						YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
					}
				})			
			};
		});
	},

	yuding: function () {
		$('.pre_im-menu').on('click', '.dropdown-menu li a', function(){
			$(".pre_im-menu-btn").html($(this).text() + ' <span class="caret"></span>');
			$(".pre_im-menu-btn").data('val', $(this).data('val'));
		});

		$('.goods-yuding .yuding-btn').click(function() {
			var _this = $(this);
			var goods_id = $('.goods-yuding').data('goodsId');
			var pre_type = $('.goods-yuding').data('yudingType');
			var pre_name = $('.goods-yuding input[name="pre_name"]').val();
			var pre_im = $('.goods-yuding .pre_im-menu-btn').data('val');
			var pre_value = $('.goods-yuding input[name="pre_im_value"]').val();

			if (!pre_name)
			{
				YOHO.mobile.modal.showWarning(YOHO._('goods_stay_tuned_error_name_empty', '請輸入您的稱呼'));
				return false;
			}
			if (!pre_im || !pre_value)
			{
				YOHO.mobile.modal.showWarning(YOHO._('goods_stay_tuned_error_value_empty', '請提供您的聯絡方法'));
				return false;
			}

			if ((pre_im == 'tel') && !YOHO.check.isMobile(pre_value) && !YOHO.check.isTelephone(pre_value))
			{
				YOHO.mobile.modal.showWarning(YOHO._('goods_stay_tuned_error_invalid_tel', '請輸入正確的電話號碼'));
				return false;
			}

			if ((pre_im == 'email') && !YOHO.check.isEmail(pre_value))
			{
				YOHO.mobile.modal.showWarning(YOHO._('goods_stay_tuned_error_invalid_email', '請輸入正確的電郵地址'));
				return false;
			}

			if (((pre_im == 'whatsapp') || (pre_im == 'weisms')) && !YOHO.check.isMobile(pre_value))
			{
				YOHO.mobile.modal.showWarning(YOHO._('goods_stay_tuned_error_invalid_whatsapp', '請輸入正確的手提電話號碼'));
				return false;
			}

			if(_this.hasClass('clicked'))
			{
				return false;
			}
			_this.addClass('clicked');
			_this.html('<img src="/images/loading.gif"> ' + YOHO._('goods_stay_tuned_submitting', '發送中'));

			$.ajax({
				url: '/ajax/yuding.php',
				type: 'POST',
				data: {
					'act': 'pre_submit',
					'pre_type': pre_type,
					'goods_id': goods_id,
					'pre_name': pre_name,
					'pre_im': pre_im,
					'pre_value': pre_value
				},
				dataType: 'json',
				success: function(res) {
					if (res.status == 1)
					{
						// YOHO.mobile.modal.showSuccess("您的聯絡資料已成功送出，到貨時我們會聯絡您。");

						$('.goods-yuding form').remove();
						$('<br><div class="alert alert-success" role="alert">' + YOHO._('goods_stay_tuned_submitted_prompt', '您的資料已成功送出，到貨時我們會聯絡您。') + '</div>').appendTo($('.goods-yuding'));
					}
					else
					{
						if (res.hasOwnProperty('error') && res.error.length)
						{
							YOHO.mobile.modal.showWarning(res.error);
						}
						_this.removeClass('clicked');
						_this.html(YOHO._('goods_stay_tuned_submit', '確定'));
					}
				},
				error: function(xhr) {
					YOHO.mobile.modal.showError(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
					_this.removeClass('clicked');
					_this.html(YOHO._('goods_stay_tuned_submit', '確定'));

					console.log(xhr);
				}
			});
		});
	},

	wishList: function() {
		var modalTitle = YOHO._('wishlist_title', '願望清單');
		$('.wishlist-btn,.goods-wishlist-btn').click(function() {
			var $btn = $(this);
			var act = $btn.hasClass('on') ? 'remove' : 'add';
			$.ajax({
				url: '/ajax/wish_list.php',
				type: 'post',
				dataType: 'json',
				data: {
					'act': act,
					'goods_id': $(this).data('goodsId')
				},
				success: function(data) {
					if (data.status == 1)
					{
						$btn[act == 'add' ? 'addClass' : 'removeClass']('on');
						var newText = (act == 'add') ?
							YOHO._('wishlist_remove', '從願望清單中移除') :
							YOHO._('wishlist_add', '加入願望清單');
						$btn.attr('title', newText);
						$btn.find('span:last').text(newText);

						setTimeout(function () {
							YOHO.mobile.modal.showInfo(modalTitle, (act == 'add' ?
								YOHO._('wishlist_add_success', '已加入願望清單') :
								YOHO._('wishlist_remove_success', '已從願望清單中移除')
							));
						}, 100);
					}
					else if (data.status == 0)
					{
						var errMsg = (data.hasOwnProperty('error') && data.error.length) ? data.error :
							(act == 'add' ?
								YOHO._('wishlist_add_failed', '加入願望清單失敗，請稍後再試。') :
								YOHO._('wishlist_remove_failed', '從願望清單中移除失敗，請稍後再試。')
							);
						var modal = YOHO.mobile.modal.showWarning(modalTitle, errMsg);
						if (data.hasOwnProperty('showlogin') && data.showlogin == 1)
						{
							modal.find('button.btn-primary').click(function () {
								window.location = '/user.php?act=login';
							});
						}
					}
				},
				error: function(xhr) {
					YOHO.mobile.modal.showError(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
				}
			});
		});
	},

	flashdeal: function() {
		// Flashdeal buy button event
		$(document).on('click', '.flashdeal-addtocart-btn', function () {
			var $buybtn = $(this);

			var flashdeal_id = $buybtn.data('flashdealId');

			if ($buybtn.data('clicked'))
			{
				return;
			}
			$buybtn.data('clicked', 1);

			$.ajax({
				url: '/ajax/flashdeal.php',
				type: 'post',
				data: {
					'act': 'addtocart',
					'flashdeal_id': flashdeal_id
				},
				dataType: 'json',
				success: function (result) {
					if (result.status == 1)
					{
						YOHO.mobile.modal.showInfo(YOHO._('global_cart_add_success', '加入購物車成功！'));
						var qty = $buybtn.parent().find('.remaining_qty>num').text() - 1;
						$buybtn.parent().find('.remaining_qty>num').text(qty);

						if(Object.keys(YOHO.mobile.flashdealCartTimer).length == 0 || YOHO.mobile.flashdealCartTimer.expired == '') {
							YOHO.mobile.flashdealCartTimer.now_time = result.fg_info.now_time;
							YOHO.mobile.flashdealCartTimer.expired = result.fg_info.expired;
							YOHO.mobile.flashdealCartCountDown();
						}
					}
					else
					{
						YOHO.mobile.modal.showWarning(result.text);
					}
					$buybtn.removeData('clicked');
				},
				error: function(xhr) {
					YOHO.mobile.modal.showError(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
					$buybtn.removeData('clicked');
				}
			});
		});

		// Flashdeal notify button event
		$(document).on('click', '.flashdeal-notify-btn', function (){
			var $this = $(this);

			var flashdeal_id = $this.data('flashdealId');

			var modalTitle = YOHO._('flashdeal_notifyme_title', '開賣通知');
			var content = '<h4>' + YOHO._('flashdeal_notifyme_instruction', '請輸入電郵地址，開賣立即通知您！') + '</h4>' +
						'<div class="input-group">' +
							'<span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>' +
							'<input type="text" class="form-control" name="email" placeholder="' + YOHO._('flashdeal_notifyme_email', '電子郵件地址') + '">' +
						'</div>';
			var cancelBtn = YOHO.mobile.modal.createButton(YOHO._('global_Cancel', '取消'), 'btn-default', true);
			var okBtn = YOHO.mobile.modal.createButton(YOHO._('global_OK', '確定'), 'btn-primary', false);
			var btns = $('<div>');
			cancelBtn.appendTo(btns);
			okBtn.appendTo(btns);
			var modal = YOHO.mobile.modal.create(modalTitle, content, btns);
			YOHO.mobile.modal.show(modal);
			modal.on('shown.bs.modal', function () {
				modal.find('input[name="email"]').focus();
			});
			okBtn.click(function () {
				var email = modal.find('input[name="email"]').val();

				var errMsg = '';
				if (!email)
				{
					errMsg = YOHO._('flashdeal_notifyme_error_missing_email', '請填寫電郵地址');
				}
				else if (!YOHO.check.isEmail(email))
				{
					errMsg = YOHO._('flashdeal_notifyme_error_invalid_email', '電郵地址格式錯誤');
				}

				if (errMsg.length)
				{
					YOHO.mobile.modal.showWarning(modalTitle, errMsg);
					return;
				}

				$.ajax({
					url: '/ajax/flashdeal.php',
					type: 'post',
					data: {
						'act': 'notifyme',
						'flashdeal_id': flashdeal_id,
						'email': email
					},
					dataType: 'json',
					success: function (result) {
						var msg = (result.hasOwnProperty('text') && ($.type(result.text) ==='string')) ? result.text : '';
						if (result.status == 1)
						{
							YOHO.mobile.modal.hide(modal);
							YOHO.mobile.modal.showInfo(modalTitle, (msg === '' ? YOHO._('flashdeal_notifyme_success', '您已成功登記，開賣時會以電郵通知您。') : msg));
						}
						else
						{
							YOHO.mobile.modal.showWarning(modalTitle, (msg === '' ? YOHO._('flashdeal_notifyme_failed', '提交失敗，請稍後再試。') : msg));
						}
					},
					error: function(xhr) {
						YOHO.mobile.modal.showError(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
					}
				});
			});
			modal.find('input[name="email"]').keypress(function (e) {
				if (e.which == 13)
				{
					okBtn.trigger('click');
					e.preventDefault();
				}
			}).val($('.flashdeal-notify-default-email').text());
		});

		$('.share-btn').click(function () {
			var $parent = $(this).closest('.flashdeal-goods-row');
			var myURL = window.location.href;
			var myImage = $parent.find('.goods-img-container img')[0].src || window.location.protocol + '//' + window.location.hostname + '/images/logo_square.png';
			var myTitle = $parent.find('.flashdeal-goods-name h3').text();
			myTitle = myTitle ? YOHO._('flashdeal_title', '友和閃購') + ' - ' + myTitle : YOHO._('flashdeal_title', '友和閃購');
			var myDescription = $.trim($parent.find('.goods-brief .flashdeal-text').contents().get(0).nodeValue) || $('meta[name="Description"]').attr('content');

			if ($(this).hasClass('facebook'))
			{
				// Share on Facebook
				FB.ui({
					method: 'feed',
					link: myURL,
					picture: myImage,
					name: myTitle,
					description: myDescription
				}, function(response) {
					var shared = (response && !response.error_code) ? 1 : 0;
					console.log(shared);
				});
			}
			else if ($(this).hasClass('twitter') ||
					$(this).hasClass('pinterest') ||
					$(this).hasClass('google-plus') ||
					$(this).hasClass('weibo'))
			{
				var share_url;
				if ($(this).hasClass('twitter'))
				{
					share_url = 'https://twitter.com/share?url=' + encodeURIComponent(myURL);
				}
				else if ($(this).hasClass('pinterest'))
				{
					share_url = 'https://www.pinterest.com/pin/create/button/?url=' + encodeURIComponent(myURL) + '&media=' + encodeURIComponent(myImage) + '&description=' + encodeURIComponent(myTitle);
				}
				else if ($(this).hasClass('google-plus'))
				{
					share_url = 'https://plus.google.com/share?url=' + encodeURIComponent(myURL);
				}
				else if ($(this).hasClass('weibo'))
				{
					share_url = 'http://service.weibo.com/share/share.php?url=' + encodeURIComponent(myURL) + '&type=button&language=zh_tw&pic=' + encodeURIComponent(myImage) + '&searchPic=true&style=simple';
				}

				window.open(share_url);
			}
		});

		var getTs = function () { return Math.floor((new Date()).getTime() / 1000); };
		$('.flashdeal-countdown').each(function () {
			var $countdown = $(this);
			var serverTs = parseInt($countdown.data('nowTs')) || getTs();
			var tsDiff = serverTs - getTs() + 1; // add 1 seconds for network delay
			var endTs = parseInt($countdown.data('endTs'));
			var $parent = $countdown.parent();
			var updateCountdown = function () {
				var remaining_time = endTs - (getTs() + tsDiff);
				// var remaining_time = 0;
				if (remaining_time <= 0)
				{
					// Terminating condition met
					$parent.text(YOHO._('flashdeal_started', '閃購已經開始'));
					setTimeout(function(){location.href=$(location).attr('href');},2000);
				}
				// else if (remaining_time < 720)
				// {
				// 	$parent.html('' +
				// 		'<span style="color:#ff0000;">' +
				// 		'<i class="fa fa-exclamation-circle"></i>' +
				// 		YOHO._('goods_promote_ending_soon', '進入最後倒數！') +
				// 		'</span>'
				// 	);
				//
				// 	setTimeout(updateCountdown, 1000);
				// }
				else
				{
					var percent02d = function (input) {
						return (String(input).length < 2 ? '0' : '') + input;
					};
					var date_str = '';
					if (remaining_time > 86400)
					{
						date_str = date_str + '<span class="goods_promote_day">'+ percent02d(Math.floor(remaining_time / 86400)) + YOHO._('goods_promote_day', '天') + '</span>';
						remaining_time = remaining_time % 86400;
					}
					if (remaining_time > 3600)
					{
						date_str = date_str + '<span class="goods_promote_hour">'+  percent02d(Math.floor(remaining_time / 3600))　+ '</span>:';
						remaining_time = remaining_time % 3600;
					}
					if (remaining_time > 60)
					{
						date_str = date_str + '<span class="goods_promote_miniute">'+ percent02d(Math.floor(remaining_time / 60)) + '</span>:';
						remaining_time = remaining_time % 60;
					} else {
						date_str = date_str + '<span class="goods_promote_miniute">00</span>:';
					}
					date_str = date_str + '<span class="goods_promote_second">'+ percent02d(remaining_time) + '</span>';

					$countdown.html(date_str);

					setTimeout(updateCountdown, 1000);
				}
			};
			updateCountdown();
		});

		$(document).on('click', '.flashdeal-learn-more', function (e) {
			e.preventDefault();

			var $parent = $(this).closest('.flashdeal-goods-row');
			var $detail = $parent.find('.flashdeal-product-detail');
			var $ship = $parent.find('.flashdeal-goods-shipping-content');

			if ($detail.length && $detail.css('display') === 'none')
			{
				$detail.show();
				$detail.children('.sectionbox').scrollTop(0);
				$('body').addClass('lock-position');
				var detailTop = $detail.offset().top || 0;
				var navBarHeight = $('.navbar-fixed-top').height() || 0;
				$(document).scrollTop(detailTop - navBarHeight);
				if ($ship.css('display') === ('block')) {
					$ship.css('display', 'none');
				}
			} else {
				$('body').removeClass('lock-position');
				$detail.css('display', 'none');
			}
		});

		$(document).on('click', '.flashdeal-product-detail', function (e) {
			$('body').removeClass('lock-position');
			$(this).css('display', 'none');
			var $parent = $(this).closest('.flashdeal-goods-row');
			var detailTop = $parent.offset().top || 0;
			var navBarHeight = $('.navbar-fixed-top').height() || 0;
			$(document).scrollTop(detailTop - navBarHeight);
		});
		$(document).on('click', '.flashdeal-ship', function (e) {
			e.preventDefault();

			var $parent = $(this).closest('.flashdeal-goods-row');

			var $ship = $parent.find('.flashdeal-goods-shipping-content');
			var $detail = $parent.find('.flashdeal-goods-detail');
			if ($ship.length && $ship.css('display') === 'none')
			{
				$ship.show();
				if ($detail.css('display') === ('block')) {
					$detail.css('display', 'none');
				}
			} else {
				$ship.css('display', 'none');
			}
		});

		$(document).on('click','.flashdeal-goods-shipping-content', function (e) {
			$(this).css('display', 'none');
			var $parent = $(this).closest('.flashdeal-goods-row');
			var detailTop = $parent.offset().top || 0;
			var navBarHeight = $('.navbar-fixed-top').height() || 0;
			$(document).scrollTop(detailTop - navBarHeight);
		});

		$( window ).load(function() {
			$('.upcoming-deals-title').each(function(){
				var $this = $(this);
				var $originTop = $this.offset().top - 150;
				$(window).scroll(function(){
					if(($(window).scrollTop() >= $originTop))
					{
						$this.addClass('fixed');
					} else {
						$this.removeClass('fixed');
					}
				});
			});
		});
	},

	indexflashdeal: function(){
		var getTs = function () { return Math.floor((new Date()).getTime() / 1000); };
		$('.flashdeal-countdown').each(function () {
			var $countdown = $(this);
			var serverTs = parseInt($countdown.data('nowTs')) || getTs();
			var tsDiff = serverTs - getTs() + 2; // add 2 seconds for network delay
			var endTs = parseInt($countdown.data('endTs'));
			var $parent = $countdown.parent();
			var updateCountdown = function () {
				var remaining_time = endTs - (getTs() + tsDiff);
				if (remaining_time <= 0)
				{
					// Terminating condition met
					$parent.text(YOHO._('flashdeal_started', '閃購已經開始'));
					setTimeout(function(){window.location = '/flashdeal?start=1'},500);
				}
				else if (remaining_time < 720)
				{
					$parent.html('' +
						'<span style="color:#ff0000;">' +
						'<i class="fa fa-exclamation-circle"></i>' +
						YOHO._('goods_promote_ending_soon', '進入最後倒數！') +
						'</span>'
					);

					setTimeout(updateCountdown, 1000);
				}
				else
				{
					var percent02d = function (input) {
						return (String(input).length < 2 ? '0' : '') + input;
					};
					var date_str = '';
					if (remaining_time > 86400)
					{
						date_str = date_str + '<span class="goods_promote_day">'+ percent02d(Math.floor(remaining_time / 86400)) + YOHO._('goods_promote_day', '天') + '</span>';
						remaining_time = remaining_time % 86400;
					}
					if (remaining_time > 3600)
					{
						date_str = date_str + '<span class="goods_promote_hour">'+  percent02d(Math.floor(remaining_time / 3600))　+ '</span>:';
						remaining_time = remaining_time % 3600;
					}
					if (remaining_time > 60)
					{
						date_str = date_str + '<span class="goods_promote_miniute">'+ percent02d(Math.floor(remaining_time / 60)) + '</span>:';
						remaining_time = remaining_time % 60;
					} else {
						date_str = date_str + '<span class="goods_promote_miniute">00</span>:';
					}
					date_str = date_str + '<span class="goods_promote_second">'+ percent02d(remaining_time) + '</span>';

					$countdown.html(date_str);

					setTimeout(updateCountdown, 1000);
				}
			};
			updateCountdown();
		});
	},
	setupTelInput: function ($input) {
		var opts = {};
		opts.defaultCountry = ($input.data('country')) ? $input.data('country').toLowerCase() : 'hk';
		opts.preferredCountries = ['hk','mo','tw','cn'];
		opts.onlyCountries = ($input.data('countries')) ? $input.data('countries').toLowerCase().split(',') : opts.preferredCountries;
		opts.defaultCountry = (opts.onlyCountries.indexOf(opts.defaultCountry) !== -1) ? opts.defaultCountry : 'hk';

		var containsAll = function(arr1, arr2) {
			return $.grep(arr2, function(v) { return $.inArray(v, arr1) !== -1; }).length === arr2.length;
		};
		if (containsAll(opts.preferredCountries, opts.onlyCountries))
		{
			// If preferredCountries includes all onlyCountries, we don't need preferredCountries at all
			delete opts.preferredCountries;
		}

		$input.intlTelInput(opts);
	},

	login: function () {
		var $form = $('.form-signin');
		var $ccc = $form.find('input[name="ccc"]');
		var $name = $form.find('input[name="username"]');

		this.setupTelInput($name);

		$form.submit(function () {
			$ccc.val($name.intlTelInput('getSelectedCountryData').dialCode);
		});
	},

	register: function () {
		var $form = $('.form-register'),
		$mobile = $form.find('input[name=mobile]'),
		$email = $form.find('input[name=email]'),
		$pwd = $form.find('input[name=password]'),
		$pwd2 = $form.find('input[name=password2]'),
		$agree_tos = $form.find('input[type=checkbox][name=agree_tos]');
		var $reference = $form.find('input[name=reference]');

		this.setupTelInput($mobile);

		$form.submit(function(event) {

			event.preventDefault();

			var _btn = $form.find('button.btn-primary');

			if(_btn.find('img').length > 0)
			{
				return false;
			}

			var _ccc = $mobile.intlTelInput('getSelectedCountryData').dialCode;
			var _mobile = $.trim($mobile.val());
			var _email = $.trim($email.val());
			var _pwd = $.trim($pwd.val());
			var _pwd2 = $.trim($pwd2.val());
			var _agree_tos = $agree_tos.prop('checked');
			var _ref = $.trim($reference.val());

			$('#register-error-msg').html('');

			var errs = [];

			if(!_mobile || !_email || !_pwd || !_pwd2)
			{
				var list = [];
				if (!_mobile) { list.push(YOHO._('account_username', '手提電話號碼')); }
				if (!_email)  { list.push(YOHO._('account_email', '電郵地址')); }
				if (!_pwd)    { list.push(YOHO._('account_password', '密碼')); }
				if (!_pwd2)   { list.push(YOHO._('account_password2', '確認密碼')); }

				errs.push(sprintf(YOHO._('account_please_fill_in', '請輸入%s'), list.join(YOHO._('account_list_delimiter', '、'))));
			}

			var mobile_ok = YOHO.check.isMobile(_mobile, _ccc);
			var email_ok = YOHO.check.isEmail(_email);
			if(!mobile_ok || !email_ok)
			{
				errs.push(sprintf(
					YOHO._('account_format_invalid', '%s格式有誤，再修改一下哦！'),
					((!mobile_ok) ? YOHO._('account_username', '手提電話號碼') : YOHO._('account_email', '電郵地址'))
				));
			}

			if(_pwd.length && _pwd.length < 6)
			{
				errs.push(YOHO._('account_password_too_short', '密碼長度最少6個字哦！'));
			}

			if(_pwd.length && _pwd2.length && _pwd != _pwd2)
			{
				errs.push(YOHO._('account_password_mismatch', '您兩次輸入的密碼不一致！'));
			}

			if (!_agree_tos)
			{
				errs.push(YOHO._('pls_agree_tos_and_privacy', '請同意《免責聲明》和《私隱條款》！'));
			}

			if (errs.length > 0)
			{
				$.each(errs, function (idx, err) {
					$('#register-error-msg').append(YOHO.mobile.createAlert(err, 'warning'));
				});
				return false;
			}

			_btn.html('<img src="/images/loading.gif" /> ' + YOHO._('account_submitting', '提交中'));

			$.ajax({
				url: '/ajax/ajaxuser_reg.php',
				type: 'post',
				data: {
					'ccc': _ccc,
					'mobile': _mobile,
					'email': _email,
					'password': _pwd,
					'password_': _pwd2,
					'reference': _ref
				},
				dataType: 'json',
				success: function(data) {
					if(data.status == 1)
					{
						if(data.redirect) {
							location.href = data.redirect;
						}else if(data.send_verify_sms == 1) { // Pop up SMS verify BOX
							var $send_phone = $('#sms_verify').find('input[name=send_phone]');
							$send_phone.val('+'+_ccc+'-'+_mobile);
							location.href = '/user.php?act=sms_verification';
							// YOHO.mobile.smsverify(_mobile, _ccc);
						} else {
							location.href = '/user.php';
						}
					}
					else
					{
						_btn.html(YOHO._('account_register_btn', '註冊成為會員'));
						var errorMsg = YOHO._('account_register_error_unknown', '註冊失敗，請稍後再試。');
						if (data.hasOwnProperty('error') && data.error.length > 0)
						{
							errorMsg = data.error;
						}
						$('#register-error-msg').html('').append(YOHO.mobile.createAlert(errorMsg, 'warning'));
					}
				},
				error: function(xhr) {
					_btn.html('註冊成為會員');
					YOHO.mobile.modal.showError(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
				}
			});
		});

		// $('#tosModal button.btn-default[data-dismiss="modal"], #privacyModal button.btn-default[data-dismiss="modal"]').click(function () {
		// 	$agree_tos.prop('checked',false);
		// });
		// $('#tosModal button.btn-primary[data-dismiss="modal"], #privacyModal button.btn-primary[data-dismiss="modal"]').click(function () {
		// 	$agree_tos.prop('checked',true);
		// });
	},

	resetPassword: function () {
		var req_frm = $('.form-request-reset');

		var $mobile = req_frm.find('input[name="mobile"]');
		this.setupTelInput($mobile);

		req_frm.submit(function(event) {

			event.preventDefault();

			var _btn = req_frm.find('button.btn-primary');

			if(_btn.find('img').length > 0)
			{
				return false;
			}

			var data = {
				'act': 'checkUser'
			};
			if (req_frm.find('input[name="email"]').val() !== '')
			{
				data.type = 'email';
				data.val = req_frm.find('input[name="email"]').val();
			}
			else
			{
				data.type = 'mobile';
				data.ccc = $mobile.intlTelInput('getSelectedCountryData').dialCode;
				data.val = req_frm.find('input[name="mobile"]').val();
			}

			if (!data.val)
			{
				$('#reset-password-error-msg').html(YOHO._('account_reset_password_needs_input', '請先輸入手提電話號碼或電郵地址'));
				return false;
			}

			_btn.html('<img src="/images/loading.gif" /> ' + YOHO._('account_submitting', '提交中'));

			$.ajax({
				url: '/ajax/findpwd.php',
				type: 'post',
				dataType: 'json',
				data: data,
				success: function(data) {
					_btn.html(YOHO._('button_next', '下一步'));
					var email = data.email;
					var pos1 = email.indexOf("@");
					var pos2 = email.lastIndexOf(".");
					var sec1 = email.substring(1,pos1);
					var sec2 = email.substring(pos1+2,pos2);
					var sec1_length = sec1.length;
					var sec2_length = sec2.length;
					var new_sec1 = "";
					var new_sec2 = "";
					for (var i=0; i<sec1_length;i++) {
						new_sec1 += "*"; 
					}
					for (var i=0; i<sec2_length;i++) {
						new_sec2 += "*"; 
					}
					var processed_email = email.replace(sec1,new_sec1);
					processed_email = processed_email.replace(sec2,new_sec2);
					if(data.status == 1)
					{
						$('.form-request-reset .step1').hide();
						$('.form-request-reset .find-step span').removeClass('on').eq(3).addClass('on');
						var step2_html = ''+
										'<div class="find-info">' +
											sprintf(YOHO._('account_reset_password_email_sent_to', '系統已發送驗證電郵到 %s'),"" ) +
											'<div>' + processed_email + '</div>' +
										'</div>' +
										'<div class="last">' +
											'<a href="http://'+email.replace(/.*@/,'')+'" target="_blank" class="btn-css3">' +
												YOHO._('account_reset_password_check_mailbox', '去查看郵件') +
											'</a>' +
											'<a href="javascript:;" class="cancel">' +
												YOHO._('account_reset_password_back_to_edit', '返回修改信息') +
											'</a>' +
										'</div>';
						$('.form-request-reset .step2').html(step2_html);
						$('.form-request-reset .step2').show();
						$('.form-request-reset .step2 a.cancel').click(function() {
							$('.form-request-reset .step2').hide();
							$('.form-request-reset .step1').show();
						});
					}
					else if (data.status == 2)
					{
						var modal = YOHO.mobile.modal.showInfo(YOHO._('account_reset_password_use_default', '您從未登入過，請使用預設密碼「yohohongkong」登入'));
						modal.find('button.btn-primary').click(function () {
							window.location = '/user.php?act=login';
						});
					}
					else if (data.status == 3)
					{
						$('#reset-password-error-msg').html(YOHO._('account_reset_password_requires_email', '帳戶已認證，請輸入電子郵件地址'));
						req_frm.find('input[name="email"]').focus();
					}
					else
					{
						$('#reset-password-error-msg').html(data.error);
					}
				},
				error: function(xhr) {
					_btn.html(YOHO._('button_next', '下一步'));
					YOHO.mobile.modal.showError(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
				}
			});
		});

		var res_frm = $('.form-reset-password');
		res_frm.submit(function(event) {

			event.preventDefault();

			var _btn = res_frm.find('button.btn-primary');

			if(_btn.find('img').length > 0)
			{
				return false;
			}

			var _pwd = $.trim(res_frm.find('input[name="new_password"]').val());
			var _pwd2 = $.trim(res_frm.find('input[name="confirm_password"]').val());

			if(_pwd.length < 6)
			{
				$('#reset-password-error-msg').html('').append(YOHO.mobile.createAlert(YOHO._('account_password_too_short', '密碼長度最少6個字哦！'), 'warning'));
				return false;
			}

			if(_pwd != _pwd2)
			{
				$('#reset-password-error-msg').html('').append(YOHO.mobile.createAlert(YOHO._('account_password_mismatch', '您兩次輸入的密碼不一致！'), 'warning'));
				return false;
			}

			_btn.html('<img src="/images/loading.gif" /> ' + YOHO._('account_submitting', '提交中'));

			$.ajax({
				url: '/ajax/findpwd.php',
				type: 'post',
				data: {
					'act': 'resetPassword',
					'uid': $('input[name="uid"]').val(),
					'code': $('input[name="code"]').val(),
					'new_password': _pwd,
					'confirm_password': _pwd2
				},
				dataType: 'json',
				success: function(data) {
					if(data.status == 1)
					{
						var modal = YOHO.mobile.modal.showInfo(YOHO._('account_reset_password_title', '重設密碼'), YOHO._('account_reset_password_success', '修改密碼成功，請重新登入！'));
						modal.find('button.btn-primary').click(function () {
							window.location = '/user.php?act=login';
						});
					}
					else
					{
						_btn.html(YOHO._('account_reset_password_btn', '重設密碼'));
						var msg = data.error || YOHO._('account_reset_password_failed', '修改密碼失敗，請稍後再試。');
						$('#reset-password-error-msg').html('').append(YOHO.mobile.createAlert(msg, 'warning'));
					}
				},
				error: function(xhr) {
					_btn.html(YOHO._('account_reset_password_btn', '重設密碼'));
					YOHO.mobile.modal.showError(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
				}
			});
		});
	},

	profileEditor: function () {
		var frm = $('form#edit-profile');

		frm.bootstrapValidator({
			feedbackIcons: {
					valid: 'glyphicon glyphicon-ok',
					invalid: 'glyphicon glyphicon-remove',
					validating: 'glyphicon glyphicon-refresh'
			},
			excluded: ':disabled'
		});

		frm.on('click', 'a.editdata', function () {
			$(this).closest('.form-control-static').hide();
			$(this).closest('div').find('input,select').show().focus();

			// Show save button
			frm.find('button[type="submit"]').show();
		});
	},

	passwordEditor: function () {
		var frm = $('form#edit-password');

		frm.bootstrapValidator({
			feedbackIcons: {
					// valid: 'glyphicon glyphicon-ok',
					invalid: 'glyphicon glyphicon-remove',
					validating: 'glyphicon glyphicon-refresh'
			},
			live: 'disabled',
			excluded: ':disabled',
			fields: {
				'old_password': {
					validators: {
						remote: {
							message: frm.find('input[name="old_password"]').data('bvRemoteMessage'),
							url: '/user.php?act=act_validate_password'
						}
					}
				},
				'new_password': {
					validators: {
						stringLength: {
							message: frm.find('input[name="new_password"]').data('bvStringlengthMessage'),
							min: 6
						}
					}
				},
				'confirm_password': {
					validators: {
						callback: {
							message: YOHO._('account_password_mismatch', '您兩次輸入的密碼不一致！'),
							callback: function(value, validator) {
								var new_password = validator.getFieldElements('new_password').val();
								return (value == new_password);
							}
						}
					}
				}
			}
		});

		frm.on('click', 'a.editdata', function (event) {
			frm.find('.form-group').show();
			frm.find('#old-password-group').show();

			$(this).closest('.form-control-static').hide();
			$(this).closest('div').find('input').show().focus();

			event.preventDefault();
		});

		frm.on('click', 'a.canceledit', function (event) {
			frm.find('.form-group').hide();

			var $oldpw_div = frm.find('#old-password-group');
			$oldpw_div.show();
			$oldpw_div.find('.form-control-static').show();
			$oldpw_div.find('input').hide();

			event.preventDefault();
		});
	},

	sendHashMail: function() {
		$.ajax({
			url: 'user.php',
			data: { 'act':'send_hash_mail' },
			dataType: 'json',
			success: function(data) {
				if (data.hasOwnProperty('error') && data.error !== 0)
				{
					YOHO.mobile.modal.showWarning(data.message);
				}
				else
				{
					YOHO.mobile.modal.showSuccess(data.message);
				}
			},
			error: function(xhr) {
				YOHO.mobile.modal.showError(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
				console.log(xhr);
			}
		});
	},

	cart: function () {
		var removeGoodsFromCart = function (rec_id)
		{
			window.location = '/cart?step=drop_goods&id=' + rec_id;
		};

		var updateGoodsNum = function(id, num) {
			$.ajax({
				url: '/cart',
				type: 'post',
				data: {
					'step': 'update_cart',
					'lineItemId': id,
					'goods_number': num
				},
				success: function(result) {
					if (result.error)
					{
						var msg = YOHO._('cart_update_failed', '更新購買數量失敗');
						if (result.hasOwnProperty('message'))
						{
							msg = result.message;
						}
						var modal = YOHO.mobile.modal.showWarning(msg);
						modal.find('button.btn-primary').click(function () {
							window.location.reload();
						});
					}
					else
					{
						setTimeout(function () {
							window.location.reload();
						}, 1000);
					}
				},
				error: function(xhr) {
					YOHO.mobile.modal.showError(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
					console.log(xhr);
				}
			});
		};

		var calculateTotal = function() {
			var total = 0;
			$('.goods-number-group input').each(function (){
				var count = parseInt($(this).val()) || 1;
				var priceText = $(this).closest('.cart-goods-item').find('.cart-goods-price').text();
				var price = YOHO.mobile.parsePrice(priceText) || 0;
				total = total + price * count;
			});

			$('.total-goods-price').text('HK$ ' + total.toFixed(2));
		};

		$('.goods-number-group').click('button', function (event) {
			var $btn = $(event.target).closest('button');
			var $input = $(this).find('input[type="number"]');
			var op = $btn.data('op');
			var recId = $input.data('recId');
			var value = parseInt($input.val());
			if (isNaN(value) || value <= 0)
			{
				value = 1;
				$input.val(value);
				return;
			}
			else if (op == '+')
			{
				value = value + 1;
			}
			else if (op == '-')
			{
				value = value - 1;
				if (value <= 0)
				{
					if (confirm(YOHO._('cart_remove_prompt', '您確定要從購物車中移除該產品嗎？')))
					{
						removeGoodsFromCart(recId);
					}
					return;
				}
			}
			//$input.val(value);
			updateGoodsNum(recId, value);
			calculateTotal();
		});

		$('.goods-number-group input').change(function () {
			var $input = $(this);
			var recId = $input.data('recId');
			var num = $input.val();
			updateGoodsNum(recId, num);
			calculateTotal();
		});

		$.each($('.favourable-goods-table'), function( key, value ){
			$this = $(this);
			$goods_count = $(this).find('tbody tr.goods-tr').length;
			if($goods_count >= 5) {
				for (var i = 4; i<= $goods_count; i++) {
					$($(this).find('tbody tr.goods-tr')[i]).hide();
				}
			} else {
				$($(this).find('tbody tr.more-tr')).hide();
			}
		});
		$('tbody tr.more-tr').on('click', function(){
			$table = $(this).closest('table');
			for (var i = 4; i<= $goods_count; i++) {
				$($table.find('tbody tr.goods-tr')[i]).slideDown();
			}
			$(this).hide();
		});

		YOHO.mobile.flow();
	},
	quotation: function () {
		// Choose shipping method
		YOHO.mobile.checkoutOption('shipping', 'glyphicon-globe');
		$('.shipping-option a.btn').click(function () {
			// Shipping fee to be paid on delivery?
			if (($(this).hasClass("fod")) && (YOHO.mobile.parsePrice($(this).find('em.postage_fee').text()) > 0))
			{
				$('div.shipping_fee_on_delivery').show();
				$('input[name="shipping_fod"]').prop('disabled', false);
			} else
			{
				$('div.shipping_fee_on_delivery').hide();
				$('input[name="shipping_fod"]').prop('disabled', true);
				$('input[name="shipping_fod"]').prop('checked', false);
			}

			YOHO.mobile.recalculateQuotationTotalPrice();
		});
		$('.shipping-option a.btn').first().trigger('click');
		$('input[name="shipping_fod"]').change(function () {
			YOHO.mobile.recalculateQuotationTotalPrice();
		});

		// Choose payment method
		YOHO.mobile.checkoutOption('payment', 'glyphicon-credit-card');
		$('.payment-option a.btn').click(function () {
			YOHO.mobile.recalculateQuotationTotalPrice();
		});
		$('.payment-option a.btn').first().trigger('click');

		// Validate before submitting form
		$('#receiverForm_btn').click(function () {

			if (!$('input[name=company_name]').val() || $('input[name=company_name]').val() == "")
			{
				YOHO.mobile.modal.showWarning(YOHO._('quotation_input_company_name', '請輸入公司名稱！'));
				return false;
			}

			if (!$('input[name=contact_email]').val() || $('input[name=contact_email]').val() == "")
			{
				YOHO.mobile.modal.showWarning(YOHO._('quotation_input_contact_email', '請輸入聯絡電郵！'));
				return false;
			} else if (!YOHO.check.isEmail($('input[name=contact_email]').val()))
			{
				YOHO.mobile.modal.showWarning(YOHO._('email_info', '請正確填寫此電郵地址，我們會將報價單寄到此電郵。'));
				return false;
			}

			if (!$('input[name=contact_tel]').val() || $('input[name=contact_tel]').val() == "")
			{
				YOHO.mobile.modal.showWarning(YOHO._('quotation_input_contact_tel', '請輸入聯絡電話！'));
				return false;
			}

			if (!$('input[name=contact_name]').val() || $('input[name=contact_name]').val() == "")
			{
				YOHO.mobile.modal.showWarning(YOHO._('quotation_input_contact_name', '請輸入聯絡人名稱！'));
				return false;
			}

			if (!$('input[name=shipping]:checked').val())
			{
				YOHO.mobile.modal.showWarning(YOHO._('checkout_error_missing_shipping', '請選擇送貨方式！'));
				event.preventDefault();
				return false;
			}
			if (!$('input[name=payment]:checked').val())
			{
				YOHO.mobile.modal.showWarning(YOHO._('checkout_error_missing_payment', '請選擇付款方式！'));
				event.preventDefault();
				return false;
			}
			// Disable submit button to prevent double submitting
			$(this).html('<img src="/images/loading.gif"> ' + YOHO._('checkout_submitting', '提交中')).prop('disabled', true);
			$('#receiverForm').submit();

		});

		// Calculate price
		YOHO.mobile.recalculateQuotationTotalPrice();
	},

	recalculateQuotationTotalPrice: function () {
		var goods_total = YOHO.mobile.parsePrice($('#goods-total').data('price')) || 0;
		var postage_fee = YOHO.mobile.parsePrice($('div.shipping-option a.btn-info em.postage_fee').text()) || 0;
		var payment = YOHO.mobile.parsePrice($('div.payment-option a.btn-info em.payment_fee').text()) || 0;
		var integral = YOHO.mobile.parsePrice($('div.integral_offset_value span').text()) || 0;
		var coupon = YOHO.mobile.parsePrice($('div.coupon_offset_value span').text()) || 0;

		if ($('input[name="shipping_fod"]').prop('checked'))
		{
			postage_fee = 0;
		}

		var price = goods_total;

		if (integral > 0)
		{
			price -= integral;
		}

		if (coupon > 0)
		{
			price -= coupon;
		}

		// Handle payment fee (especially when it is a percentage)
		var paymentFee = $("div.payment-option a.btn-info em.payment_fee").text();
		if (paymentFee.length > 0 && paymentFee.indexOf('%') === paymentFee.length - 1)
		{
			// paymentFee is a percentage of the total payable price
			var paymentFeeRate = parseFloat(paymentFee) / 100.0;
			payment = price * (paymentFeeRate > 0 ? paymentFeeRate / (1 - paymentFeeRate) : paymentFeeRate);
		}

		payment = Math.ceil(payment);
		price += payment;
		price += postage_fee;

		if (price < 0)
		{
			price = 0;
		}

		$("div.shipping_fee_value span").text(postage_fee.toFixed(2));
		if (postage_fee > 0)
		{
			$("div.shipping_fee_value").show();
		} else
		{
			$("div.shipping_fee_value").hide();
		}

		$("div.payment_fee_value span").text(payment.toFixed(2));
		$("div.payment_discount_value span").text((payment * -1).toFixed(2));
		if (payment > 0)
		{
			$("div.payment_fee_value").show();
			$("div.payment_discount_value").hide();
		} else if (payment < 0)
		{
			$("div.payment_fee_value").hide();
			$("div.payment_discount_value").show();
		} else
		{
			$("div.payment_fee_value").hide();
			$("div.payment_discount_value").hide();
		}

		var $currency = $("div.payment-option a.btn-info em.payment_currency");
		if ($currency.length)
		{
			var currencyRate = $currency.data('currencyRate');
			var currencyFormat = $currency.data('currencyFormat');
			var amount = (price / currencyRate).toFixed(2);
			var amount_formatted = currencyFormat.replace('%s', amount);
			$("#quotation-total").text(amount_formatted);
		} else
		{
			$("#quotation-total").text('HK$ ' + price.toFixed(2));
		}
	},

	//處理價錢
	parsePrice: function(formatted_price) {
		if (formatted_price === null || formatted_price === undefined)
		{
			return 0;
		}
		if (formatted_price.toFixed)
		{
			return formatted_price; // it is a number already
		}
		if ($.type(formatted_price) !== 'string')
		{
			return 0;
		}
		if (formatted_price.length === 0)
		{
			return 0;
		}
		if (formatted_price.indexOf('%') === formatted_price.length - 1)
		{
			return 0; // don't process percentage here
		}
		var price = parseFloat(formatted_price);
		if (isNaN(price))
		{
			var matches = (formatted_price.match(/\-?\d+[,\.]?\d*/));
			if ($.isArray(matches))
			{
				price = parseFloat(matches[0]);
			}
			else
			{
				console.log('Warning: "' + formatted_price + '" cannot be parsed as price');
				price = 0;
			}
		}
		return price;
	},

	checkout: function(init) {
		var _this = this;
		var lock_list = {};
		/* Record user edit address time */
		_this.data.seconds = 0;
		var order_staying_time = $.cookie("order_staying_time");
		if(typeof order_staying_time !== "undefined" && order_staying_time != null){
			_this.data.seconds = parseInt(order_staying_time);
		}

		/* Checkout page global change step function */
		function flowStepAjax($step, $data, $success_function) {
			$data = $data || {};
			$success_function = $success_function || function () {
			};
			$shipping_type_code = $('input[name="shipping_type"]').val();
			$('#goods_list').addClass('disabled');
			$('input[name="payment"]').prop('checked', false);
			$.ajax({
				url: '/ajax/checkout.php',
				type: 'POST',
				cache: false,
				data: {code: $shipping_type_code, act: 'select_step', step: $step, data: $data},
				dataType: 'json',
				success: function (result) {
					if (result.status == 1) {
						$nextStep = result.nextStep;
						$stepName = $nextStep.stepName;
						YOHO.step = $nextStep.step;
						if ($stepName == 'address') {
							createAddressBox($nextStep, $shipping_type_code);
						} else if ($stepName == 'shipping') {
							$('.checkout_step#shipping_method').nextAll().fadeOut().promise().done(function () {
								createShippingBox($nextStep);
							});
						} else if ($stepName == 'payment') {
							$('.checkout_step#payment_method').nextAll().fadeOut().promise().done(function () {
								createPaymentBox($nextStep);
							});
						}
						$success_function(result);
					}
				},
				error: function (xhr) {
					YOHO.mobile.modal.showError(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '(' + xhr.status + ')');
				}
			});
		};

		/*
		* Create step box function
		* */
		function createAddressBox($data, $shipping_type_code) {
			if ($shipping_type_code == 'express') {
				$address_list = $data.list;
				$default_address = $data.default;
				/* Clear old address list */
				$('.checkout_step.address ul').empty();
				$.each($address_list, function (key, address) {
					/* Init li */
					$li = $('<li class="col-xs-12 col-md-12 col-sm-12"></li>');
					/* Create edit div */
					$edit_div = $('<div class="address_edit"></div>');
					$edit_link =
						'<a href="javascript:void(0)" data-value="' + address.id + '" id="edit-address" class="pay_bg edit_btn" title="' + YOHO._('checkout_edit_address', '修改收貨人信息') + '">' +
						'<svg class="">' +
						'<use xlink:href="/yohohk/img/yoho_svg.svg#edit_pen"></use>' +
						'</svg>' +
						'</a>'
					;
					$edit_div.append($edit_link);
					/* Create hidden radio box */
					$label = $('<label class="address_radio"></label>');
					$input = $('<input type="radio" name="address_radio" class="radio" />');
					$input.val(address.id);
					$input.data('country', address.country);
					$label.append($input);
					$label.append($('<span class="checkmark"></span>'));
					/* Create address info */
					$address_info =
						'<label class="consignee title">' + address.consignee + '</label>' +
						'<div class="address_info">' +
						'<svg class="">' +
						'<use xlink:href="/yohohk/img/yoho_svg.svg#location"></use>\n' +
						'</svg>' +
						address.address +
						'</div>' +
						'<div class="tel">' +
						'<svg class="">' +
						'<use xlink:href="/yohohk/img/yoho_svg.svg#phone"></use>' +
						'</svg>' +
						address.mobile +
						'</div>';
					$li.append($label).append($edit_div).append($address_info);
					/* Add into html */
					$('.checkout_step.address ul').append($li);
				});
				/* Show and add event */
				$('.checkout_step.address').fadeIn();
			} else if ($shipping_type_code == 'pickuppoint') {
				$('#locker_m').hide();
				$('#payment_method').fadeOut();
				if ($data.over_max_size == 1 ||  Object.keys($data.list).length == 0) {
					$('.over_max_size_box').show();
					$('#locker_table').empty();
					$('#locker_box').hide();
					$('.locker_method_loading').fadeOut();
					$('#locker').fadeIn();
					$('#selected_locker').html('');
					$('.re_sel_locker').hide();
				} else {
					$('.over_max_size_box').hide();
					$('#locker_box').fadeIn();
					$("select[name='locker_area']").prop('disabled', true);
					$("select[name='locker_region']").prop('disabled', true);
					$("select[name='locker_area']").empty().append('<option value="0">' + YOHO._('checkout_address_please_select', '--請選擇--') + '</option>');
					$("select[name='locker_region']").empty().append('<option value="0">' + YOHO._('checkout_address_please_select', '--請選擇--') + '</option>');
					$('#locker_table').html('');
					$('#selected_locker').html('');
					$('.locker_method_loading').fadeOut();
					$('#locker').fadeIn();
					lock_list = $data.list;
					$.each($data.list, function ($key, $value) {
						$("select[name='locker_area']").append($("<option></option>").attr("value", $key).text($key));
					});
					$("select[name='locker_area']").prop('disabled', false);

					$locker = $data.default;
					if ($locker !== null && Object.keys($locker).length > 0 && $locker.city && $locker.district && $locker.edCode) {
						$("select[name='locker_area']").val($locker.city).trigger('change');
						$("select[name='locker_region']").val($locker.district).trigger('change');
						if (!$("input[name=lockerCode][value='" + $locker.edCode + "']").is(':disabled')) {
							$("input[name=lockerCode][value='" + $locker.edCode + "']").attr('checked', 'checked').trigger('change');
							$('#selected_locker').html($locker.twThrowaddress);
							$('input[name="allotRouter"]').val($locker.allotRouter);
							$('input[name="locker_consignee"]').val($locker.consignee);
							$('input[name="locker_tel"]').val($locker.tel);
							$('a.re_sel_locker').unbind('click');
							$('a.re_sel_locker').show();
							$('a.re_sel_locker').click(function(){
								$(this).hide();
								//$('#selected_locker').html('');
								$('#locker_m').fadeIn();
							});
						} else {
							$('#locker_m').fadeIn();
						}
						if ($locker.arCode) {
							$('input[name="allotRouterCode"]').val($locker.arCode);
						}
					} else {
						$('#locker_m').fadeIn();
					}
				}
			}
		};

		function createShippingBox($data) {
			$('#shipyyy').empty();
			$('.cal_free_shipping').hide();
			_this = this;
			$result = $data.data;
			$shipping_list = $result.shipping_list;
			if($result.restricted_to_hk_goods) {
				$html =
					'<div style="padding: 10px; border: 1px solid #ff6969; background-color: #fff6bf;">' +
					'   <div style="font-size: 14px;">' +
					'       <i class="fa fa-fw fa-warning"></i>' +
					YOHO._('checkout_shipping_restricted_to_hk', '您的購物車中包含了以下產品只能送貨到香港：') +
					'   </div>';
				$.each($result.restricted_to_hk_goods, function( key, goods ) {
					$html += '   <div style="padding: 2px 2px 2px 24px;">' + goods + '</div>';
				});
				$html += '</div>';
				$('#shipyyy').append($html);

			} else if($result.shipping_list) {
				$ul = $('<ul class="cle_float"></ul>');
				$shipping_option = $result.shipping_list.length;
				$.each($result.shipping_list, function( key, shipping ) {
					$li = $('<li class="online col-xs-12 col-md-4 col-sm-4 shipping-option"></li>');
					if(shipping.support_fod == true) $li.addClass('fod');
					$shipping_box = $('<div class="shipping_box"></div>');
					$title = '<div class="shipping_title title">' +
						(shipping.shipping_logo_svg ? shipping.shipping_logo_svg : '') +
						'<input name="source_shipping" type="radio" value="'+shipping.shipping_id+'" style="display:none;"/></span>'+
						'<input name="source_shipping_area" type="radio" value="'+shipping.shipping_area_id+'" style="display:none;"/></span>'+
						'<input name="shipping" type="radio" value="'+shipping.shipping_id+'" style="display:none;"/></span>'+
						'<input name="shipping_area" type="radio" value="'+shipping.shipping_area_id+'" style="display:none;"/></span>'+
						'<input name="cal_free_shipping" type="hidden" value="'+shipping.cal_free_shipping+'" style="display:none;"/></span>'+
						'<div class="name">'+shipping.shipping_name+'</div>'+
						'<em class="postage_fee">'+shipping.format_shipping_fee+'</em>' +
						'</div>';
					$shipping_info = '<div class="single-shipping_info"><span>'+shipping.shipping_desc+'</span></div>';
					$shipping_box.append($title).append($shipping_info);
					$li.append($shipping_box);
					$ul.append($li);
				});
				$fod = '<div class="cle"></div>'+
					'<div class="shipping_fee_on_delivery" style="display: none; margin-top: 8px;">'+
					'<input type="checkbox" id="shipping_fod" name="shipping_fod"> <label for="shipping_fod">'+ YOHO._('checkout_shipping_fee_on_delivery' , '運費到付')+'</label><a class="btn btn-sm btn-link inline-btn yoho-popover" data-toggle="popover" data-trigger="focus" data-placement="bottom" title="'+ YOHO._('checkout_shipping_fee_on_delivery', '運費到付')+'" data-content="'+ YOHO._('checkout_shipping_fee_on_delivery_notice', '速遞收取的運費可能會高於以上金額')+'"><span class="glyphicon glyphicon-question-sign"></span></a></div>';
				$('#shipyyy').append($ul).append('<div class="shipping_info">' + '<span></span>' + '</div>').append($fod);
			}
			$('#shipping_method').fadeIn();
			if ($shipping_option == 1){
				$('#shipyyy').children("ul").children("li").trigger("click");
			}
		};

		function createPaymentBox($data) {
			var methodRequiredTooltip = [
				'stripegooglepay', 'stripeapplepay'
			];
			$('#paymentbox').empty();
			_this = this;
			$payment_list = $data.data;
			if($payment_list) {
				$.each($payment_list, function( key, payment ) {
					if(payment.is_online != 1 ) return;
					$li = $('<li class="online col-xs-12 col-md-4 col-sm-4"></li>');
					$li.attr('id', 'pay'+payment.pay_id);
					$payment_box = $('<div class="payment_box"></div>');
					$title = '<div class="payment_title title">' +
						'<input name="payment" type="radio" value="'+payment.pay_id+'" style="display:none;"/></span>'+
						(payment.payment_logo_svg ? payment.payment_logo_svg : '') +
						'<div class="name">'+payment.pay_name+'</div>'+
						((methodRequiredTooltip.includes(payment.pay_code)) ? "<div style='position:absolute;right:20px;top:35%'><span data-html='true' title=\""+getBrowserPaymentTooltip(payment.pay_code)+"\" class='browser-payment-tooltip "+payment.pay_code+"'>?</span></div>" : '') +
						'<em class="postage_fee"></em>' +
						'</div>';
					$payment_info = '<div class="single-payment_info">'+
						'<em class="payment_fee" style="display:none;">'+payment.format_pay_fee+'</em>'+
						'<em class="payment_currency" style="display:none;" data-currency-rate="'+payment.currency.currency_rate+'" data-currency-format="'+payment.currency.currency_format+'">'+payment.currency.currency_name+'</em>'+
						payment.pay_desc+'</div>';
					$payment_box.append($title).append($payment_info);
					$li.append($payment_box);
					$('#paymentbox').append($li);
				});
			}
			if ($payment_list.length == 0) {
				YOHO.mobile.modal.showWarning(YOHO._('flashdeal_addtocart_error_only_single_payment_method_allowed', '您所選擇的閃購無法使用其他支付方法優惠'));
			}
			YOHO.mobile.popover();
			$('#payment_method').fadeIn();
			$.each(methodRequiredTooltip, function(key, pay_code) {
				initPaymentMethodTooltip(pay_code);
			});
		};

		if (typeof init !== "undefined" && init == true) {
		$('.shipping_type').on('click', function(){
			if($(this).hasClass('current')) return;

			$shipping_type_code = $(this).data('value');
			$(this).addClass('current').siblings().removeClass('current');
			$('input[name="shipping_type"]').val($shipping_type_code);
			$('#shipping_type_info').empty();
			$('#shipping_type_info').html($(this).children('.single_shipping_type_info').html());
			$('.checkout_step.ship_type').nextAll().fadeOut().promise().done(function() {
				/* Call ajax to get shipping */
				flowStepAjax('shipping_type', {}, function(){
						$("html, body").animate({ scrollTop: ($('.checkout_step.ship_type').next().offset().top - $('.navbar-fixed-top').height() - 5) }, 300);
					});
			});
		});
		// Removal
		$('input[name="need_removal"]').change(function(){
			if (this.checked) {
				// the checkbox is now checked
				$('.need_removal_list').fadeIn();
				var $shipping_type = $('.shipping_type.current').data('value');
				if($shipping_type == 'express' && $('.addr_queue li.current input:radio').data('country') == 3409) {
					$li = $('.addr_queue li.current');
					$('input[name="nr_address"]').val($.trim($li.find('.address_info').text()));
					$('input[name="nr_consignee"]').val($.trim($li.find('.consignee.title').text()));
					$('input[name="nr_phone"]').val($.trim($li.find('.tel').text()));
				}
			} else {
				// the checkbox is now no longer checked
				$('.nr_consignee input').val('');
				$('.need_removal_list').hide();
			}
		});
		// Address
		$('body').on('click','a#edit-address', function () {
			var selected_address = $(this).data('value');
			YOHO.mobile.editAddress(selected_address);
		});
		$('body').on('click','a#add-address', function () {
			YOHO.mobile.showAddressDialog();
		});
		// Choose from address list
		$('body').on('click','.addr_queue li', function () {
			if($(this).hasClass('current')) return;
			$addr_queue_li = $(this);
			var $radio = $addr_queue_li.find('input:radio');
			flowStepAjax('address', {id: $radio.val()}, function(){
				$('.addr_queue>li.current').removeClass('current');
				$addr_queue_li.addClass('current');
				$radio.trigger('click');
				$("html, body").animate({ scrollTop: ($('.checkout_step#address').next().offset().top - $('.navbar-fixed-top').height() - 5) }, 300);
			});
		});
		// Choose payment method
		$('body').on('click', 'div.payment_method li', function () {
			if($(this).hasClass('current')) return;
			// Will do nothong in case of tooltip event is triggered
			if($('.tooltip.in').length > 0) return;
			$(this).addClass('current').siblings().removeClass('current');
			$('.payment_info').empty();
			$('.payment_info').html($(this).find('.single-payment_info').html());
			var radio = $(this).find('input[type="radio"]');
			radio.prop('checked', true);
			var code = $("input#coupon_value").data("promote");
			if (code != "") {
				$("input#coupon_value").data("promote", "");
				$("#use-coupon-btn").click();
				YOHO.mobile.checkout().validateCoupon(code);
			} else {
				YOHO.mobile.recalculateCheckoutTotalPrice();
			}
			$('#goods_list').removeClass('disabled');
			$("html, body").animate({ scrollTop: ($('#goods_list').offset().top - $('.navbar-fixed-top').height() - 5) }, 300);
		});
		// Choose shipping method
		var $ship = $('div.shipping_method');
		$("#shipping_type_info").html('');
		$('body').on('click', 'div.shipping_method li', function () {
			if($(this).hasClass('current')) return;
			$(this).addClass('current').siblings().removeClass('current');
			$('.shipping_info').empty();
			$('.shipping_info').html($(this).find('.single-shipping_info').html());
			var radio = $(this).find('input[type="radio"]');
			radio.prop('checked', true);
			// Shipping fee to be paid on delivery?
			if (($(this).hasClass("fod")) && (YOHO.mobile.parsePrice($(this).find('em.postage_fee').text()) > 0))
			{
				$('div.shipping_fee_on_delivery').show();
				$('input[name="shipping_fod"]').prop('disabled', false);
			}
			else
			{
				$('div.shipping_fee_on_delivery').hide();
				$('input[name="shipping_fod"]').prop('disabled', true);
				$('input[name="shipping_fod"]').prop('checked', false);
			}
			var $shipping_type = $('.shipping_type.current').data('value');
			$shipping_id = $('input[name=shipping]:checked').val();
			$address_id = $('input[name="address_radio"]:checked').val();
			YOHO.mobile.recalculateCheckoutTotalPrice();
			flowStepAjax('shipping', {
				shipping_id: $(this).find('input[name=source_shipping]').val(),
				shipping_area_id: $(this).find('input[name=source_shipping_area]').val()
			}, function () {
				$("html, body").animate({ scrollTop: ($('.checkout_step#shipping_method').nextAll('div:visible').first().offset().top - $('.navbar-fixed-top').height() - 5) }, 300);
			});
		});
		$('body').on('change', 'input[name="shipping_fod"]', function () {
			YOHO.mobile.recalculateCheckoutTotalPrice();
		});
		// Choose from locker list
		$('body').on('change', 'input[name="lockerCode"]', function(){
			$('#selected_locker').html('');
			$('input[name="allotRouter"]').val($(this).data('ar'));
			$('input[name="allotRouterCode"]').val('');
			$('.locked_time').hide();
			$(this).closest('tr').find('.locked_time').show();
			if($(this).data('hot') == 1) {
				var dialog = null;
				$content = $('<div></div>');
				$ar = $(this).data('ar');
				$area = $("select[name='locker_area']").val();
				$region =$("select[name='locker_region']").val();
				$list = lock_list[$area][$region][$ar];
				$select = $("<select name='ar_select' class='form-control'></select>");
				$select.empty();
				$.each($list, function($key, $value) {
					if($value.isHot == 0) {
						$select.append($("<option></option>").attr("value", $value.edCode).text($value.twThrowaddress));
					}
				});
				$content.append($select);
				var cancelBtn = YOHO.mobile.modal.createButton(YOHO._('global_Cancel', 'Cancel'), 'btn-default', true);
				var okBtn = YOHO.mobile.modal.createButton(YOHO._('global_OK', 'OK'), 'btn-primary', true);
				var btns = $('<div></div>');
				cancelBtn.appendTo(btns);
				okBtn.appendTo(btns);

				var ok_action = function() {
					//$("#locker select, #locker input").prop('disabled', false);
					$("input[name='allotRouterCode']").val($("select[name='ar_select']").val());
				};
				var cancel_action = function() {
					$('.locked_time').hide();
					$('input[name="lockerCode"]').prop('checked', false);
					//$("#locker select, #locker input").prop('disabled', false);
					$('#payment_method').fadeOut();
				};
				okBtn.click(ok_action);
				cancelBtn.click(cancel_action);

				var modal = YOHO.mobile.modal.create(YOHO._('checkout_locker_second_select', '為確保您的訂單能順利送達，請選擇一個後備智能櫃'), $content, btns);
				YOHO.mobile.modal.show(modal);
				//return modal;
			}
			$('#selected_locker').html($(this).parent().text());
			if($(this).data('shipping_id') > 0  && $(this).data('shipping_area_id') > 0) {
				$('input[name=shipping]:checked').val($(this).data('shipping_id'));
				$('input[name=shipping_area]:checked').val($(this).data('shipping_area_id'));
			}
			flowStepAjax('address', {shipping_id: $('input[name=shipping]:checked').val(), shipping_area_id: $('input[name=shipping_area]:checked').val()}, function(){
				setTimeout(function(){$("html, body").animate({ scrollTop: ($('.checkout_step#locker').next().offset().top - $('.navbar-fixed-top').height() - 5) }, 300);
			})}, 600);
		});
		$('body').on('change', "select[name='locker_area']", function () {
			$this = $(this);
			$("select[name='locker_region']").empty().append('<option value="0">' + YOHO._('checkout_address_please_select', '--請選擇--') + '</option>');
			$("select[name='locker_region']").prop('disabled', true);
			if($this.val() == 0 ) {
				var table = $('#locker_table');
				table.html('');
				return;
			}
			$region_list = lock_list[$this.val()];
			$.each($region_list, function($key, $value) {
				$("select[name='locker_region']").append($("<option></option>").attr("value", $key).text($key));
			});
			$("select[name='locker_region']").prop('disabled', false);
		});
		$('body').on('change', "select[name='locker_region']", function () {
			$this = $(this);
			var table = $('#locker_table');
			table.html('');
			if($this.val() == 0 )return;
			$chose_list = lock_list[$('select[name="locker_area"]').val()][$this.val()];
			$lockers = [];
			$.each($chose_list, function($k, $v) {
				$.each($v, function($key, $value) {
					var row = $('<tr>');
					var remarks = '';
					if($value.shipping_desc){
						remarks = '&nbsp;<span class="pickup_remarks">'+$value.shipping_desc+'</span>';
					}
					$icon = ($value.shop_icon) ? "<img class='pickup_shop_icon' src='"+$value.shop_icon+"' />" : "";
					$row_html = '<div class="locker_label" data-over_size_lang="'+YOHO._('checkout_pickuppoint_over_size', '此提貨點不支援你所選購產品的尺寸')+'"><label><input data-hot="'+$value.isHot+'" data-shipping_area_id="'+$value.shipping_area_id+'" data-shipping_id="'+$value.shipping_id+'" data-ar="'+$value.allotRouter+'" type="radio" name="lockerCode" class="lockerCode" value="'+$value.edCode+'">'+$icon+$value.twThrowaddress+remarks+'</label>';
					if($value.isHot == 1) {
						$row_html += '<br><span class="isHot">'+YOHO._('checkout_locker_is_hot', '此櫃為熱門選擇或需要等候較長時間')+'</span>';
					}
					$row_html += '</div>';
					$time_html = '';
					if($value.workday) $time_html  += YOHO._('checkout_locker_workday', '星期一至星期五') +' : '+ $value.workday + '</br>';
					if($value.saturday) $time_html += YOHO._('checkout_locker_saturday', '星期六') +' : '+ $value.saturday + '</br>';
					if($value.sunday) $time_html   += YOHO._('checkout_locker_sunday', '星期日') +' : '+ $value.sunday + '</br>';
					if($value.holiday) $time_html  += YOHO._('checkout_locker_holiday', '公眾假期') +' : '+ $value.holiday + '</br>';
					var td3 = $('<div class="locked_time">').append($time_html);
					var td1 = $('<td>').html($row_html);

					if($value.isBlackList == 1) {
						td1.find('input[name="lockerCode"]').attr('disabled', 'disabled');
						td1.find('.locker_label').append('<br><span class="isFull">'+YOHO._('checkout_locker_is_black_list', '此櫃暫時無法選取')+'</span>');
					}
					if($value.over_max_size == 1) {
						td1.find('input[name="lockerCode"]').attr('disabled', 'disabled');
						td1.find('.locker_label').addClass('over_size');
					}
					td1.find('.locker_label').append(td3);
					row.append(td1);
					table.append(row);
				});
			});
			//$("#locker_m").append(table);
		});

		// Use Integral
		$('#use-integral-btn').click(function () {
			var user_integral = parseInt($.trim($('#canuse-integral-user').text())) || 0;
			var order_integral = parseInt($.trim($('#canuse-integral-order').text())) || 0;
			var can_use_integral = Math.min(user_integral, order_integral);
			var integral_scale = parseFloat($.trim($('#integral_scale').text())) || 0.5;
			var one_dollar_integral = (100 / integral_scale) || 100;
			var optimum_integral = parseInt(can_use_integral / one_dollar_integral) * one_dollar_integral;

			$('#integral_value').val(optimum_integral);
			$('input[name="integral"]').val(optimum_integral);
			$(this).hide();
			$('div.checkout-integral').show('fast');
			$('#integral_value').trigger('change');
		});
		$('#integral_value').change(function () {
			var user_integral = parseInt($.trim($('#canuse-integral-user').text())) || 0;
			var order_integral = parseInt($.trim($('#canuse-integral-order').text())) || 0;
			var can_use_integral = Math.min(user_integral, order_integral);
			var integral_scale = parseFloat($.trim($('#integral_scale').text())) || 0.5;
			var use_integral = parseInt($('#integral_value').val()) || 0;
			if (use_integral < 0) { use_integral = 0; }
			if (use_integral > can_use_integral) { use_integral = can_use_integral; }
			var cash_value = (use_integral / 100 * integral_scale) || 0;

			$('#integral_value').val(use_integral);
			$('input[name="integral"]').val(use_integral);
			$('div.integral_offset_value span').text(cash_value.toFixed(2));
			if (cash_value > 0)
			{
				$('div.integral_offset_value').show();
			}
			else
			{
				$('div.integral_offset_value').hide();
			}
			YOHO.mobile.recalculateCheckoutTotalPrice();
		}).keydown(function(event) {
			// Prevent enter submit the form
			if(event.keyCode == 13)
			{
				event.preventDefault();
				$(this).blur();
			}
		});
		}
		var validateCoupon = function (code) {
			$('input#coupon_value').hide();
			$('#coupon-result').hide();
			$('#coupon-loading').show();

			var finishLoading = function () {
				$('input#coupon_value').show();
				$('#coupon-result').show();
				$('#coupon-loading').hide();
			};

			var codeList = [];
			$('#coupon-result input[name="coupon[]"]').each(function (){
				codeList.push($(this).val());
			});

			if (code)
			{
				codeList.push(code);
			}

			// No coupon used
			if (codeList.length === 0)
			{
				finishLoading();

				$('#coupon-result').empty();

				$('div.coupon_offset_value span').text('0.00');
				$('div.coupon_offset_value').hide();

				$(".coupon_promote").get().map(function(v) {
					if (codeList.indexOf($(v).data("code").toUpperCase()) === -1) {
						$(v).remove();
					}
				})

				YOHO.mobile.recalculateCheckoutTotalPrice();

				return;
			}

			$.ajax({
				type: 'post',
				url: '/ajax/coupon_use.php',
				data: {'code': codeList},
				dataType: 'json',
				success: function(data) {
					if (!data.status)
					{
						if (data.hasOwnProperty('error'))
						{
							YOHO.mobile.modal.showWarning(data.error);
						}
					}
					else
					{
						$('input#coupon_value').val('');

						$('#coupon-result').empty();
						$.each(data.coupon_list, function (coupon_id, coupon_code) {
							var coupon_label = $('<span class="label label-info">' + coupon_code +
												'<input name="coupon[]" type="hidden" value="' + coupon_code + '">' +
												'<a class="glyphicon glyphicon-remove label-remove"></a></span>');
							$('#coupon-result').append(coupon_label);
						});

						$('#coupon-result .label .label-remove').click(function () {
							$(this).parent('.label').remove();
							$(".coupon_extra_msg").html('');
							validateCoupon();
						});

						var cash_value = YOHO.mobile.parsePrice(data.coupon_value);
						$('div.coupon_offset_value span').text(cash_value.toFixed(2));
						$('div.coupon_offset_value').show();

						YOHO.mobile.recalculateCheckoutTotalPrice();
						$.each(data.promote_list, function (promote_code, promote_id) {
							if (promote_id == 1 && $("div.coupon_promote[data-code=" + promote_code + "]").length == 0) {
								$html = $("<div class='coupon_promote' data-code='" + promote_code + "'>" +
												"<span class='promote_title'>" + YOHO._("checkout_insert_the_club_id", "請輸入你的 The Club 會員帳號") + "</span>" +
												"<input class='form-control promote_txt' name='theClubId' type='text' maxlength='10'>" +
												"<div id='theClubHint'>" + YOHO._("checkout_hint_the_club_id", "會員帳號為8字開頭的10位數字") + "</div>" +
												"<ul id='theClubTerms'>" + YOHO._("checkout_terms_the_club_id", "The Club Terms") + "</ul>" +
											"</div>");
								$html.find("input[name=theClubId]").on("input", function(e) {
									var val = $(e.target).val();
									var error = false;
									$("#theClubHint").removeClass("error");
									$(e.target).removeClass("error");
									if (val.length != 0) {
										if (!/^8[0-9]{9}$/.test(val)) {
											error = true;
										}
									}
									if (error) {
										$(e.target).addClass("error");
										$("#theClubHint").addClass("error");
									}
								})
								$("#goods_list .can-use-integral").append($html);
							}
						});

						if ( typeof data.extra_message !== 'undefined' && data.extra_message != '') {
							$(".coupon_extra_msg").html(data.extra_message);
						}
					}

					finishLoading();
				},
				error: function(xhr) {
					finishLoading();

					YOHO.mobile.modal.showError(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
					console.log(xhr);
				}
			});
		};

		if (typeof init !== "undefined" && init == true) {
		// Use Coupon
		$('#use-coupon-btn').click(function () {
			$(this).hide();
			$('.enter-coupon').show();
		});
		$('input#coupon_value').change(function () {
			var code = $('input#coupon_value').val();

			code = code.toUpperCase();
			$('input#coupon_value').val(code);

			validateCoupon(code);
		}).keydown(function(event) {
			// Prevent enter submit the form
			if(event.keyCode == 13)
			{
				event.preventDefault();
				$(this).blur();
			}
		});

		// Validate before submitting form   # checkout-form
		$('#checkout-submit-btn').click(function (event) {

			var couponcodes = [];
			$.each(document.querySelectorAll('input[name="coupon[]"]'), function (idx, couponcode) {
				couponcodes.push(couponcode.value);
			});

			var _event = event;

			var _this = this;
			if (couponcodes.length > 0) {
				$.ajax({
					url : '/ajax/checkout.php',
					type: 'post',
					cache: false,
					data: {
						'act'           : 'check_coupon_promote',
						'coupons'       : couponcodes,
						'payment_type'  : $('input[name=payment]:checked').val(),
					},
					dataType: 'json',
					success: function(result) {
						if (result.success == 0)
						{
							YOHO.mobile.modal.showWarning(result.error_msg);
							event.preventDefault();
							return false;
						} else {
							_submitOrderBeforeCheck(_this);
						} 
					},
					error: function(xhr) {
							YOHO.mobile.modal.showError(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
							console.log(xhr);
					}
				});
			} else {
				_submitOrderBeforeCheck(_this);
			}
			
			function _submitOrderBeforeCheck(_this) {
					if (!$('input[name=shipping]:checked').val())
					{
						YOHO.mobile.modal.showWarning(YOHO._('checkout_error_missing_shipping', '請選擇送貨方式！'));
						event.preventDefault();
						return false;
					}
					if (!$('input[name=payment]:checked').val())
					{
						YOHO.mobile.modal.showWarning(YOHO._('checkout_error_missing_payment', '請選擇付款方式！'));
						event.preventDefault();
						return false;
					}
					if ($('input[name="shipping_type"]').val() == 'express' && $('.addr_queue li').length == 0 )
					{
						YOHO.mobile.modal.showWarning(YOHO._('checkout_error_missing_address', '請填寫收貨地址！'));
						event.preventDefault();
						return false;
					}
					if($('input[name="shipping_type"]').val() == 'pickuppoint' && (!$('input[name="lockerCode"]:checked').val() || typeof $('input[name="lockerCode"]:checked').val() == "undefined")) {
						YOHO.mobile.modal.showWarning(YOHO._('checkout_error_missing_address', '請填寫收貨地址！'));
						event.preventDefault();
						return false;
					}
					if ($('input[name="shipping_type"]').val() == 'pickuppoint' && !YOHO.mobile.checkTextFieldName($("input[name='locker_consignee']")) )
					{
						event.preventDefault();
						return false;
					}
					if ($('input[name="shipping_type"]').val() == 'pickuppoint' && !YOHO.mobile.checkTextFieldInt($("input[name='locker_tel']")) )
					{
						event.preventDefault();
						return false;
					}
					if($('input[name="need_removal"]').length > 0 && $('input[name="need_removal"]:checked').val())
					{
						if(!$('input[name=nr_consignee]').val() || !$('input[name=nr_address]').val() || !$('input[name=nr_phone]').val()) {
							YOHO.mobile.modal.showWarning(YOHO._('checkout_address_error_nr_info', '請填寫除舊資料！'));
							event.preventDefault();
							return false;
						}
					}
					if($('input[name="theClubId"]').length > 0)
					{
						if($('input[name="theClubId"]').val() == "" || !/^8[0-9]{9}$/.test($('input[name="theClubId"]').val())) {
							YOHO.mobile.modal.showWarning(YOHO._("checkout_insert_correct_the_club_id", "請輸入正確的 The Club 會員帳號"));
							event.preventDefault();
							return false;
						}
					}
					// Disable submit button to prevent double submitting
					$(_this).html('<img src="/images/loading.gif"> ' + YOHO._('checkout_submitting', '提交中')).prop('disabled', true);
					$('#checkout-form').submit();
					return true;
			}

			// event.preventDefault();
			// return false;
		});
		}
		// Calculate price
		//YOHO.mobile.recalculateCheckoutTotalPrice();

		YOHO.mobile.flow();
		return {
			flowStepAjax: function ($step, $data, $success_function) {
				return flowStepAjax($step, $data, $success_function);
			},
			validateCoupon: function (code) {
				return validateCoupon(code);
			}
		};
	},

	checkoutOption: function (option_name, default_icon, selected_icon) {
		if (default_icon === undefined)
		{
			default_icon = 'glyphicon-asterisk';
		}
		if (selected_icon === undefined)
		{
			selected_icon = 'glyphicon-ok';
		}
		$('.' + option_name + '-option a.btn').click(function () {
			var radio = $(this).parent().find('input[type="radio"]');
			radio.prop('checked', true);
			radio.trigger('change');
		});
		$('.' + option_name + '-option input[type="radio"]').change(function () {
			$('.' + option_name + '-option input[type="radio"]').each(function () {
				if($(this).prop('checked'))
				{
					$(this).parent().find('a.btn').addClass('btn-info').removeClass('btn-default');
					$(this).parent().find('a.btn .glyphicon').removeClass(default_icon).addClass(selected_icon);
				}
				else
				{
					$(this).parent().find('a.btn').removeClass('btn-info').addClass('btn-default');
					$(this).parent().find('a.btn .glyphicon').addClass(default_icon).removeClass(selected_icon);
				}
			});
		});
	},

	// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
	// lat: 22.311620, lng: 114.224104 yoho address
	// This function is using google map API to handle user address
	googleInitAutocomplete: function() {
		var _this = this;
		// Create google map
		var map = new google.maps.Map(document.getElementById("map"), {
			center: {lat: 22.311620, lng: 114.224104},
			zoom: 17
		});
		var center = map.getCenter();
		var input = document.getElementById("pac-input");
		var v = input.value;
		input.addEventListener('change', function() {
			// setTimeout(function(){
			// 	google.maps.event.trigger(input, 'focus');
			// 	google.maps.event.trigger(input, 'keydown', {
			// 		keyCode: 13
			// 	});
			// },300);
		});

		var searchBox = new google.maps.places.SearchBox(input);
		var geocoder  = new google.maps.Geocoder;

		// Bias the SearchBox results towards current map s viewport.
		map.addListener("bounds_changed", function() {
			searchBox.setBounds(map.getBounds());
		});
		var service = new google.maps.places.PlacesService(map);
		var marker = [];
		service.getDetails({
			placeId: "ChIJq6q6gUUBBDQRz3RxKaAlw4w"
			}, function(place, status) {
				if (status === google.maps.places.PlacesServiceStatus.OK) {
					marker = new google.maps.Marker({
					map: map,
					position: place.geometry.location
					});
				}else {
					// Google server error , try to call
					// _this.hkaddresshandle();
				}
			});

		var markers = [];
		// Listen for the event fired when the user selects a prediction and retrieve
		// more details for that place.
		searchBox.addListener("places_changed", function() {

			var places = searchBox.getPlaces();
			if (places.length == 0) {
				// can't find customer address, so the edit address button
				$('.formatted_address').show();
				$('.address_edit_btn').hide();
				$('.address_not_found_btn').show();
				$('.address_not_on_list_btn').hide();
				$("#format_address_display").html('');
				$('.full_address_info:not(.formatted_address,#search_address),#next_info').hide();

				return;
			} else {
				$(".auto_address").removeClass('show');
				$('.address_edit_btn').hide();
				$('.address_not_found_btn').hide();
				$('.address_not_on_list_btn').show();
			}
			// Clear out the old markers.
			marker.setMap(null);
			markers.forEach(function(marker) {
				marker.setMap(null);
			});
			markers = [];

			// For each place, get the icon, name and location.
			var bounds = new google.maps.LatLngBounds();

			var place = places[0];

			if (!place.geometry) {
				return;
			}
			var icon = {
				url: place.icon,
				size: new google.maps.Size(71, 71),
				origin: new google.maps.Point(0, 0),
				anchor: new google.maps.Point(17, 34),
				scaledSize: new google.maps.Size(25, 25)
			};

			// Create a marker for each place.
			markers.push(new google.maps.Marker({
				map: map,
				title: place.name,
				position: place.geometry.location
			}));
			var latlng = place.geometry.location;
			var full_address_view = false;
			geocoder.geocode({'location': latlng}, function(results, status) {
				if (status === 'OK') {

					if (results[0]) {
						var html = "";
						var address_format = [];
						var political = [];
						for (var i = 0; i < results[0].address_components.length; i++) {
							var addressType = results[0].address_components[i].types[0];
							var name = results[0].address_components[i].long_name;
							address_format[addressType] = name;
							if(addressType === 'country'){
								var country_code = results[0].address_components[i].short_name;
							}
							if(addressType === 'political'){
								political.push(name);
							}
						}

						if(political.length > 0){
							political = political.reverse();
							address_format['street_number'] = '';
							address_format['route'] = (typeof address_format['route'] == 'undefined') ? '' : address_format['route'];
							for (var index = 0; index < political.length; index++) {
								var element = political[index];
								if(index == 0){
									address_format['route'] += (address_format['route'] != "" ? " " : "") + element;
								} else {
									address_format['street_number'] += element;
									if(index != (political.length - 1)){
										address_format['street_number'] += "-";
									}
								}
							}
						}

						// Check address from order record
						var google_return_country = '';
						if(country_code){
							country_code = country_code.toLocaleLowerCase();
							//_this.countries_code[country_code]
							google_return_country = country_code;
						}

						var addr_break_building = address_format['premise']?address_format['premise']:place.name;
						var addr_break_google_place = place.name;
						var addr_break_street_name = address_format['route'];
						var addr_break_street_num = address_format['street_number'];
						var addr_break_locality = address_format['neighborhood']?address_format['neighborhood']:address_format['locality'];
						var addr_break_administrative_area = address_format['administrative_area_level_1'];
						var addr_break_phase = address_format['phase'];
						var addr_break_block = address_format['block'];
						var addr_break_google_place = place.name;

						if (country_code == 'hk'){
							var google_return_building = address_format['premise'];
							var google_return_place = place.name;
							var google_return_street = address_format['route'];
							var google_return_street_num = address_format['street_number'];
							var google_return_admin_area = address_format['administrative_area_level_1'];

								$.ajax({
									url : '/ajax/address.php',
									type: 'post',
									cache: false,
									data: {
										'act'           : 'get_verified_addr_break',
										'building'       : google_return_building,
										'place'       : google_return_place,
										'street'       : google_return_street,
										'street_num'       : google_return_street_num,
										'admin_area'       : google_return_admin_area
									},
									dataType: 'json',
									success: function(result) {
										if (result.status == 1){
											if (result.list.phase !=''){
												addr_break_phase = result.list.phase;
											}
											if (result.list.sign_building !=''){
												if (result.list.phase ==''){
													addr_break_building = result.list.sign_building;
												} else if (result.list.phase !=''){
													var temp_building_name = (address_format['premise'])?address_format['premise']:place.name;
													if (temp_building_name.indexOf(result.list.sign_building) >= 0 && result.list.sign_building !=""){
														addr_break_building = result.list.sign_building;
													} else {
														temp_building_name = temp_building_name.replace(result.list.phase, "");
														addr_break_building = temp_building_name;
													}
												}
												
												addr_break_google_place = '';
											}
											if (result.list.block !=''){
												addr_break_block = result.list.block;
											}
											if (result.list.street_name !=''){
												addr_break_street_name = result.list.street_name;
											}
											if (result.list.street_num !=''){
												addr_break_street_num = result.list.street_num;
											}
											if (result.list.locality !=''){
												addr_break_locality = result.list.locality;
											}
											if (result.list.administrative_area !=''){
												addr_break_administrative_area = result.list.administrative_area;
											}
											$('input#building').val(addr_break_building);
											$('input#google_place').val(addr_break_google_place);
											$('input#street_name').val(addr_break_street_name);
											$('input#street_num').val(addr_break_street_num);
											$('input#locality').val(addr_break_locality);
											$('input#administrative_area').val(addr_break_administrative_area);
											$('input#phase').val(addr_break_phase);
											$('input#block').val(addr_break_block);
										} else {
											$('input[name=format_address]').val(results[0]['formatted_address']);
											$('input#building').val((address_format['premise'])?address_format['premise']:place.name);
											$('input#google_place').val(place.name);
											$('input#street_name').val(address_format['route']);
											$('input#street_num').val(address_format['street_number']);
											$('input#locality').val((address_format['neighborhood']?address_format['neighborhood']:address_format['locality']));
											$('input#administrative_area').val(address_format['administrative_area_level_1']);
										} 
									},
									error: function(xhr) {
										//YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
									}
								});
						}

						// Handle hide address form
						$('input#addressStreet').val(results[0]['formatted_address']);
						$('input#building').val((address_format['premise'])?address_format['premise']:place.name);
						$('input#google_place').val(place.name);
						$('input#street_name').val(address_format['route']);
						$('input#street_num').val(address_format['street_number']);
						$('input#locality').val((address_format['neighborhood']?address_format['neighborhood']:address_format['locality']));
						$('input#administrative_area').val(address_format['administrative_area_level_1']);

						if(country_code){
							country_code=country_code.toLocaleLowerCase();
							_this.data.countries_code[country_code]
							$('select#addressCountry').val(_this.data.countries_code[country_code]);
						} else {
							$('select#addressCountry').val(0);
						}
						// $('select[name=country]').val(address_format['country']);
						$('input#addressZipcode').val(address_format['postal_code']);
						$("#format_address_display").html($('input#addressStreet').val());
						$(".auto_address").hide();
						$(".formatted_address").show();

						// Hong Kong Gov API: 如果地址是香港地址, 提交到香港政府地方搜尋提高精準
						if(country_code == "hk"){
							// _this.hkaddresshandle();
						}
						if (country_code == 'cn'){
							full_address_view = true;
						}
						$('.full_address_info:not(.auto_address),#next_info').show();
					}
				} else {
					full_address_view = true;
				}
				if (full_address_view){
					YOHO.mobile.showFullAddressView();
				}
			});
			if (place.geometry.viewport) {
				// Only geocodes have viewport.
				bounds.union(place.geometry.viewport);
			} else {
				bounds.extend(place.geometry.location);
			}
			v = input.value;
			map.fitBounds(bounds);
			// center = map.getCenter();
		});
	},

	recalculateCheckoutTotalPrice: function () {
		var shipping_fod = 0;
		if($('input[name=shipping_fod]:checked').val()){
			shipping_fod = 1;
		}

		var couponcodes = [];
		$.each(document.querySelectorAll('input[name="coupon[]"]'), function (idx, couponcode) {
			couponcodes.push(couponcode.value);
		});

		$.ajax({
			url : '/ajax/checkout.php',
			type: 'post',
			cache: false,
			data: {
				'act'          : 'calculate_amount',
				'total'        : $('#total_price').attr('totalPrice'),
				'shipping'     : $('input[name=shipping]:checked').val(),
				'payment'      : $('input[name=payment]:checked').val(),
				'coupon'       : couponcodes,
				'integral'     : $('input[name="integral"]').val(),
				'address'      : $('input[name="address_radio"]').val(),
				'shipping_fod' : shipping_fod,
				'shipping_type' : $('.shipping_type.current').data('value')
			},
			dataType: 'json',
			success: function(result) {
				if (result.status == 1)
				{

					var total = result.total;

					// Handle shipping fee value
					$("div.shipping_fee_value span").text(total.shipping_fee);
					if (total.shipping_fee > 0) {
						$("div.shipping_fee_value").show();
					}
					else {
						$("div.shipping_fee_value").hide();

					}

					// Handle payment fee value
					var payment = total.pay_fee;
					$("div.payment_fee_value span").text(payment.toFixed(2));
					$("div.payment_discount_value span").text((payment * -1).toFixed(2));
					if (payment > 0)
					{
						$("div.payment_fee_value").show();
						$("div.payment_discount_value").hide();
					}
					else if (payment < 0)
					{
						$("div.payment_fee_value").hide();
						$("div.payment_discount_value").show();
					}
					else
					{
						$("div.payment_fee_value").hide();
						$("div.payment_discount_value").hide();
					}

					// Update max integral
					$('#canuse-integral-order').text(total.max_integral);
					// Update coupon price
					$('div.coupon_offset_value span').text(total.coupon);
					if(total.coupon > 0)$('div.coupon_offset_value').show();
					else $('div.coupon_offset_value').hide();
					// Update China Cross-Border E-Commerce Tax
					$('div.shipping_fee_cn_ecom span').text(total.cn_ship_tax);
					if(total.cn_ship_tax > 0)$('div.shipping_fee_cn_ecom').show();
					else $('div.shipping_fee_cn_ecom').hide();
					// Update integral price
					$('div.integral_offset_value span').text(total.integral_money);
					// Update used integral
					$('input[id=integral_value]').val(total.integral);
					// Update tital price
					$('#order-total').text(total.amount_formated);
				}
			},
			error: function(xhr) {
				YOHO.mobile.modal.showError(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
			}
		});
	},

	editAddress: function (address_id) {
		// Edit address
		$.ajax({
			url : '/ajax/address.php',
			cache: false,
			dataType: 'json',
			success: function(result) {
				if (result.status == 1)
				{
					var ary = result.addr;
					$.each(ary, function(i,v) {
						if(v.id == address_id)
						{
							YOHO.mobile.showAddressDialog(v);
							return false;
						}
					});
				}
			},
			error: function(xhr) {
				YOHO.mobile.modal.showError(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
				console.log(xhr);
			}
		});
	},

	showAddressDialog: function (data) {
		var country_opts = '';
		$.each(this.data.countries, function (i, country) {
			country_opts += '<option value="' + country.id + '"' + (country.default ? ' selected="selected"' : '') + '>' + country.name + '</option>';
		});

		var address_types_html = "<select id='sel_address_type' name='sel_address_type' class='form-control'>" + "<option value='-1'>--" + YOHO._('checkout_address_select_type', '選擇地址類型') + "--</option>";
		var cat_index = 0;
		arr_address_types.forEach(function(item, index, array){
				if (cat_index != 0 && cat_index != item.type_cat){
						address_types_html += "</optgroup>";
				}
				if (cat_index != item.type_cat){
						address_types_html += "<optgroup label='" + arr_address_types_groups[item.type_cat] + "'>";
				}
				address_types_html += "<option value='" + item.type_id + "' data-type='" + item.type_cat +"'>" + item.type_name + "</option>";
				if (cat_index != item.type_cat){
						cat_index = item.type_cat;
				}
		});
		//address_types_html += "<optgroup label='" + YOHO._('checkout_address_type_other', '其他') + "'><option value='99'>" + YOHO._('checkout_address_type_other', '其他') + "</option></optgroup></select>";
		address_types_html += "</select>";

		var lift_types_html = "<select id='sel_address_lift_type' name='sel_address_lift_type' class='form-control'>" +
												"<option value='0'>--" + YOHO._('checkout_address_type_lift_sel', '是否設有升降機') + "--</option>" +
												"<option value='1'>" + YOHO._('checkout_address_type_lift_with', '設有升降機') + "</option>" +
												"<option id='sel_lift_option_3' value='3'>" + YOHO._('checkout_address_type_lift_with_no_ground', '設有升降機(不到達地面)') + "</option>" +
												"<option value='2'>" + YOHO._('checkout_address_type_lift_wo', '沒有升降機') + "</option>" +
												"</select>";

		var addressDialogHtml =
		'<div class="checkout-address-dialog">'+
			'<div class="error-area" style="display: none; margin-bottom: 10px;"></div>'+
			'<!--<div class="form-group">'+
				'<label for="addressEmail">' + YOHO._('checkout_address_email', '電郵地址') + '</label>'+
				'<input type="email" class="form-control" id="addressEmail" placeholder="' + YOHO._('checkout_address_email', '電郵地址') + '">'+
			'</div>-->'+
			'<div class="form-group">'+
				'<h4><b><u>' + YOHO._('checkout_address_head', '送貨地址') + '</u></b></h4>'+
			'</div>'+
			'<div class="form-group consignee_info">'+
				'<label for="addressStreet">' + YOHO._('checkout_address_street', '詳細地址') + '</label>'+
				'<input type="hidden" name="type_address" id="type_address" /><input type="hidden" name="type_lift" id="type_lift" />'+
				'<div id="final_address"></div>'+
				'<div id="sel_address_info">'+ address_types_html + lift_types_html + '</div>' +
			'</div>'+
			'<div id="search_address" class="form-group full_address_info">'+
				'<label for="addressStreet">' + YOHO._('checkout_search_address', '搜尋地址') + '</label>'+
				'<input type="text" class="form-control"  id="pac-input" placeholder="' + YOHO._('checkout_address_search_placeholder', '輸入您的街名或大廈名稱') + '">'+
			'</div>'+
			'<div class="form-group formatted_address full_address_info">'+
				'<label for="addressStreet">' + YOHO._('checkout_address_street', '詳細地址') + '</label>'+
				'<div class="">'+
					'<div id="format_address_display"></div>'+
					'<div><a class="edit_address address_edit_btn" style="text-align: center;">' + YOHO._('checkout_address_edit_address','編輯地址') + '</a></div>'+
					'<div><a class="edit_address address_not_found_btn" style="text-align: center;">' + YOHO._('checkout_address_search_not_found','請重新搜尋或按這裡自行編輯地址') + '</a></div>'+
					'<div><a class="edit_address address_not_on_list_btn" style="text-align: center;">' + YOHO._('checkout_address_not_on_list','按這裡自行編輯') + '</a></div>'+
					'<div><a class="edit_address address_not_search_btn" style="text-align: center;">' + YOHO._('checkout_address_not_search','手動填寫地址') + '</a></div>'+
				'</div>'+
				'<input type="hidden" class="form-control" id="addressStreet" placeholder="' + YOHO._('checkout_address_street', '詳細地址') + '">'+
				'<input type="hidden" class="form-control" id="google_place">'+
			'</div>'+
			'<div class="form-group full_address_info">'+
				'<div class="checkout-building">'+
					'<label for="addressStreet">' + YOHO._('checkout_address_building', '大廈') + '</label>'+
					'<input type="text" class="form-control" id="building" placeholder="' + YOHO._('checkout_address_building', '大廈') + '">'+
				'</div>'+
				'<div class="checkout-floor">'+
					'<label for="addressStreet">' + YOHO._('checkout_address_floor', '樓層') + '</label>'+
					'<input type="text" class="form-control" id="floor" placeholder="' + YOHO._('checkout_address_floor', '樓層') + '">'+
				'</div>'+
				'<div class="checkout-room">'+
					'<label for="addressStreet">' + YOHO._('checkout_address_room', '室') + '</label>'+
					'<input type="text" class="form-control" id="room" placeholder="' + YOHO._('checkout_address_room', '室') + '">'+
				'</div>'+
			'</div>'+
			'<div class="form-group full_address_info">'+
				'<div class="checkout-streetName">'+
					'<label for="addressPhase">' + YOHO._('checkout_address_phase', '屋苑') + '</label>'+
					'<input type="text" class="form-control" id="phase" placeholder="' + YOHO._('checkout_address_phase', '屋苑') + '">'+
				'</div>'+
				'<div class="checkout-streetNum">'+
					'<label for="addressBlock">' + YOHO._('checkout_address_block', '座數') + '</label>'+
					'<input type="text" class="form-control" id="block" placeholder="' + YOHO._('checkout_address_block', '座數') + '">'+
				'</div>'+
			'</div>'+
			'<div class="form-group auto_address full_address_info">'+
				'<div class="checkout-streetName">'+
					'<label for="addressStreet">' + YOHO._('checkout_address_street_name', '街道') + '</label>'+
					'<input type="text" class="form-control" id="street_name" placeholder="' + YOHO._('checkout_address_street_name', '街道') + '">'+
				'</div>'+
				'<div class="checkout-streetNum">'+
					'<label for="addressStreet">' + YOHO._('checkout_address_street_num', '街號') + '</label>'+
					'<input type="text" class="form-control" id="street_num" placeholder="' + YOHO._('checkout_address_street_num', '街號') + '">'+
				'</div>'+
			'</div>'+
			'<div class="form-group auto_address full_address_info">'+
				'<div class="checkout-locality">'+
					'<label for="addressStreet">' + YOHO._('checkout_address_locality', '地方/市鎮') + '</label>'+
					'<input type="text" class="form-control" id="locality" placeholder="' + YOHO._('checkout_address_locality', '地方/市鎮') + '">'+
				'</div>'+
				'<div class="checkout-area">'+
					'<label for="addressStreet">' + YOHO._('checkout_address_area', '地區') + '</label>'+
					'<input type="text" class="form-control" id="administrative_area" placeholder="' + YOHO._('checkout_address_area', '地區') + '">'+
					'</div>'+
			'</div>'+
			'<div class="form-group auto_address full_address_info">'+
				'<label for="addressCountry">' + YOHO._('checkout_address_country', '所在國家') + '</label>'+
				'<select id="addressCountry" class="form-control">'+
					'<option value="0">' + YOHO._('checkout_address_please_select', '--請選擇--') + '</option>'+
					country_opts+
				'</select>'+
			'</div>'+
			'<div class="form-group auto_address full_address_info">'+
				'<label for="addressZipcode">' + YOHO._('checkout_address_zipcode', '郵遞區號') + '</label>'+
				'<input type="text" class="form-control" id="addressZipcode" placeholder="' + YOHO._('checkout_address_zipcode', '郵遞區號') + '">'+
			'</div>'+
			'<div class="form-group consignee_info">'+
				'<h4><b><u>' + YOHO._('checkout_contact_head', '聯絡資料') + '</u></b></h4>'+
			'</div>'+
			'<div class="form-group consignee_info">'+
				'<label for="addressName">' + YOHO._('checkout_address_consignee', '收貨人姓名') + '</label><span class="checkout_must_fill">*</span>'+
				'<input type="text" class="form-control" id="addressName" placeholder="' + sprintf(YOHO._('checkout_address_error_empty_wo_exclamation', '請輸入%s'), YOHO._('checkout_address_consignee', '收貨人姓名')) + '">'+
			'</div>'+
			'<div class="form-group consignee_info">'+
				'<label for="addressPhone">' + YOHO._('checkout_address_phone', '聯絡電話') + '</label><span class="checkout_must_fill">*</span>'+
				'<input type="text" class="form-control" id="addressPhone" placeholder="' + sprintf(YOHO._('checkout_address_error_empty_wo_exclamation', '請輸入%s'), YOHO._('checkout_address_phone', '聯絡電話')) + '">'+
			'</div>'+
			'<div class="form-group consignee_info" id="id_doc">'+
				'<label for="addressIdDoc">' + YOHO._('checkout_address_id_doc', '身份证ID') + '</label><span class="checkout_must_fill">*</span>'+
				'<input type="text" class="form-control" id="addressIdDoc" placeholder="' + sprintf(YOHO._('checkout_address_error_empty_wo_exclamation', '請輸入%s'), YOHO._('checkout_address_id_doc', '身份证ID')) + '">'+
				'<div class="id_remark">'+
					'<p>' + YOHO._('checkout_address_id_doc_remark_1', '因商品涉及入境清關，根據海關規定，需要您完善當前收貨人身份證信息。')+
						'<br><span>' + YOHO._('checkout_address_id_doc_remark_2', '若信息不正确/不真实，会导致订单清关失败，无法发货。') + '</span>'+
					'</p>'+
					'<p>' + YOHO._('checkout_address_id_doc_remark_3', '友和承諾嚴格保密您的個人信息。') + '</p>'+
            	'</div>'+
			'</div>'+
		'</div>';
		var scripts = $("script[src^='https://maps.googleapis.com/']");
		if(scripts.length > 0){
			for(var i =0; i< scripts.length;i++){
				scripts[i].remove();
			}
			window.google = {};
			$('#map').html("");
			addressDialogHtml +='<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAFTMXnxVhxw1b67sbuvmaKyTjeJs7RYzM&libraries=places&language='+this.data.lang+'&callback=YOHO.mobile.googleInitAutocomplete" async defer></script>';
		} else {
			addressDialogHtml +='<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAFTMXnxVhxw1b67sbuvmaKyTjeJs7RYzM&libraries=places&language='+this.data.lang+'&callback=YOHO.mobile.googleInitAutocomplete" async defer></script>';
		}

		var $cancelBtn = YOHO.mobile.modal.createButton(YOHO._('global_Cancel', '取消'), 'btn-default', true);
		var $saveBtn = YOHO.mobile.modal.createButton(YOHO._('checkout_address_save_btn', '確認並儲存'), 'btn-primary', false);
		var $nextBtn = YOHO.mobile.modal.createButton(YOHO._('checkout_address_next_btn', '下一步'), 'btn-primary', false);
		var $prevBtn = YOHO.mobile.modal.createButton(YOHO._('checkout_address_prev_btn', '上一步'), 'btn-primary', false);
		var btns = $('<div>').append($cancelBtn, $prevBtn, $nextBtn, $saveBtn);
		var dialogTitle = (data !== undefined) ? YOHO._('checkout_edit_address', '修改收貨人信息') : YOHO._('checkout_add_address', '新增收貨人信息');
		var modal = YOHO.mobile.modal.create(dialogTitle, addressDialogHtml, btns);
		var options = (data !== undefined) ? undefined : {backdrop: 'static'};
		YOHO.mobile.modal.show(modal, options);
		// Count User staying time
		var staying_time = setInterval(function(){
			YOHO.mobile.data.seconds += 1;
		}, 1000);

		$('.full_address_info:not(#search_address),.consignee_info').hide();
		$('input[id=building]').on('change', function(){
			$('input[id=addressStreet]').val('');
		});

		$('.edit_address').on('click', function(){
			$('.auto_address').show();
			$('.formatted_address').hide();
			$('#format_address_display').html("");
			$('.full_address_info:not(.formatted_address),#next_info').show();
		});

		modal.find('#addressCountry').change(function () {
			var country_id = $(this).val();
			// HK / Macau don't need zipcode
			if ((country_id == 3409) || (country_id == 3410))
			{
				modal.find('#addressZipcode').closest('div.form-group').hide();
			}
			else if($('.formatted_address').css('display') == 'none')
			{
				modal.find('#addressZipcode').closest('div.form-group').show();
			}
		});

		$saveBtn.attr('id','save_info');
		$nextBtn.attr('id','next_info');
		$prevBtn.attr('id','prev_info');
		$saveBtn.hide();
		$nextBtn.hide();
		$prevBtn.hide();

		if(data !== undefined)
		{
			$('.address_not_search_btn').hide();
			// Apply default values
			if (data.hasOwnProperty('email') && YOHO.check.isEmail(data.email))
			{
				modal.find('#addressEmail').val(data.email);
			}
			modal.find('#addressName').val(data.name);
			modal.find('#addressCountry').val(data.country_id);
			modal.find('#addressStreet').val('');
			modal.find('#addressZipcode').val(data.zipcode);
			modal.find('#addressPhone').val(data.phone);
			modal.find('#addressIdDoc').val(data.id_doc);
			$('#format_address_display').html(data.format_address);
			$.each(data, function(key,value) {
				modal.find('#'+key).val(value);
			});
			if(data.id > 0){
				$(".auto_address").hide();
				$(".formatted_address,.full_address_info:not(.auto_address)").show();
				$nextBtn.show();
			} else {
				$(".auto_address").hide();
				$(".formatted_address").hide();
			}
		} else { // New address
			$('.address_edit_btn').hide();
			$('.address_not_search_btn').show();
			$(".formatted_address,.formatted_address").show();
		}

		modal.find('#addressCountry').trigger('change');

		if ((!modal.find('#addressPhone').val()) && (YOHO.mobile.data.hasOwnProperty('user_name')))
		{
			var index_tel_code_end = YOHO.mobile.data.user_name.indexOf('-');
			var user_phone_fill_in = YOHO.mobile.data.user_name;
			if (index_tel_code_end > 0){
				user_phone_fill_in = YOHO.mobile.data.user_name.substr(index_tel_code_end + 1);
			}
			modal.find('#addressPhone').val(user_phone_fill_in);
		}
		if ((!modal.find('#addressEmail').val()) &&
			((YOHO.mobile.data.hasOwnProperty('user_email_address')) && (YOHO.check.isEmail(YOHO.mobile.data.user_email_address))))
		{
			modal.find('#addressEmail').val(YOHO.mobile.data.user_email_address);
		}
		$cancelBtn.on('click', function(){
			clearInterval(staying_time);
		});
		$saveBtn.on('click',function() {

			var _administrative_area = modal.find('#administrative_area').val(),
				_zipcode             = modal.find('#addressZipcode').val(),
				_country             = modal.find('#addressCountry').val(),
				_address             = modal.find('#addressStreet').val(),
				_google_place        = modal.find('#google_place').val(),
				_email               = modal.find('#addressEmail').val(),
				_phone               = modal.find('#addressPhone').val(),
				_id_doc              = modal.find('#addressIdDoc').val(),
				_street              = modal.find('#street_name').val(),
				_name                = modal.find('#addressName').val(),
				_street_num          = modal.find('#street_num').val(),
				_building            = modal.find('#building').val(),
				_phase               = modal.find('#phase').val(),
				_block               = modal.find('#block').val(),
				_locality            = modal.find('#locality').val(),
				_floor               = modal.find('#floor').val(),
				_room                = modal.find('#room').val();

			if (_country == 3409){
				var _address_type 		 = $('#sel_address_type').val(),
					_lift                = $('#sel_address_lift_type').val();
			} else {
				var _address_type 		 = 0,
					_lift                = 0;
			}
			if ($saveBtn.hasClass('btn-loading'))
			{
				return false;
			}

			modal.find('.error-area').html('').hide();

			var errs = [];

			//if(/*!_email || */!_name || (!_phase && !_building || !_street) || !_phone || (_country == 3409 && (_address_type == "" || _address_type == -1 || _lift == "" || _lift == 0)) || (_country == 3436 && (_address_type == "" || _address_type == -1 || _lift == "" || _lift == 0)))
			//{
				var list = [];
				// if (!_email)  { list.push(YOHO._('checkout_address_email', '電郵地址')); }
				if (!YOHO.check.isValidName(_name)){
					console.log(1);
					//list.push(YOHO._('checkout_address_consignee', '收貨人姓名'));
					errs.push(sprintf(YOHO._('checkout_address_error_invalid', '請輸入正確的%s！'), YOHO._('checkout_address_consignee', '收貨人姓名')));
				}

				if (!_country || _country == 0){
					console.log(2);
					//list.push(YOHO._('checkout_address_country', '所在國家'));
					errs.push(sprintf(YOHO._('checkout_address_error_invalid', '請輸入正確的%s！'), YOHO._('checkout_address_country', '所在國家')));
				}
				if (!_phase && !_building && !_street){
					console.log(3);
					//list.push(YOHO._('checkout_address_building_street_phase_block', '街道 或 屋苑 或 大廈'));
					errs.push(sprintf(YOHO._('checkout_address_error_invalid', '請輸入正確的%s！'), YOHO._('checkout_address_building_street_phase_block', '街道 或 屋苑 或 大廈')));
				}

				// Require zipcode if not HK / Macau
				if (_country != 3409 && _country != 3410 && !_zipcode)
				{
					console.log(4);
					//list.push(YOHO._('checkout_address_zipcode', '郵遞區號'));
					errs.push(sprintf(YOHO._('checkout_address_error_invalid', '請輸入正確的%s！'), YOHO._('checkout_address_zipcode', '郵遞區號')));
				}

				if (!checkPhoneNo(_phone, _country))
				{
					console.log(5);
					if (_country == 3436){
						//list.push(YOHO._('checkout_address_cn_phone_11', '11位手機號碼'));
						errs.push(sprintf(YOHO._('checkout_address_error_invalid', '請輸入正確的%s！'), YOHO._('checkout_address_cn_phone_11', '11位手機號碼')));
					} else {
						//list.push(YOHO._('checkout_address_phone', '聯絡電話'));
						errs.push(sprintf(YOHO._('checkout_address_error_invalid', '請輸入正確的%s！'), YOHO._('checkout_address_phone', '聯絡電話')));
					}
				}
				
				if (!checkidNo(_id_doc, _country))
				{
					console.log(6);
					//list.push(YOHO._('checkout_address_cn_id_doc_18', '18位內地身份證號碼'));
					errs.push(sprintf(YOHO._('checkout_address_error_invalid', '請輸入正確的%s！'), YOHO._('checkout_address_cn_id_doc_18', '18位內地身份證號碼')));
				}

				if (_country == 3409){
					console.log(7);
					if (_address_type == "" || _address_type == -1)
					{
						//list.push(YOHO._('checkout_address_type', '地址類型'));
						errs.push(sprintf(YOHO._('checkout_address_error_invalid', '請輸入正確的%s！'), YOHO._('checkout_address_type', '地址類型')));
					}
		
					if (_lift == "" || _lift == 0)
					{
						//list.push(YOHO._('checkout_address_type_lift_sel', '是否設有升降機'));
						errs.push(sprintf(YOHO._('checkout_address_error_invalid', '請輸入正確的%s！'), YOHO._('checkout_address_type_lift_sel', '是否設有升降機')));
					}
				}

				//errs.push(sprintf(YOHO._('checkout_address_error_invalid', '請輸入正確的%s！'), list.join(YOHO._('account_list_delimiter', '、'))));
			//}

			//var invalid_list = [];
			//if(_phone.length > 0 && !YOHO.check.isMobile(_phone) && !YOHO.check.isTelephone(_phone))
			//{
				//invalid_list.push(YOHO._('checkout_address_phone', '聯絡電話'));
			//}

			// if(_email.length > 0 && !YOHO.check.isEmail(_email))
			// {
			// 	invalid_list.push(YOHO._('checkout_address_email', '電郵地址'));
			// }


			//if (invalid_list.length > 0)
			//{
			//	errs.push(sprintf(YOHO._('checkout_address_error_invalid', '請輸入正確的%s！'), invalid_list.join(YOHO._('account_list_delimiter', '、'))));
			//}

			if (errs.length > 0)
			{
				console.log(errs.length);
				modal.find('.error-area').show();
				$.each(errs, function (idx, err) {
					modal.find('.error-area').append(YOHO.mobile.createAlert(err, 'warning'));
				});
				return false;
			}

			if (_country != 3409){
				_address_type = 0;
				_lift = 0;
			}

			$saveBtn.addClass('btn-loading');
			clearInterval(staying_time);
			var params = {
				'id':'',
				'email': _email,
				'receiver': _name,
				'country': _country,
				'administrative_area': _administrative_area,
				'zipcode':_zipcode,
				'street': _street,
				'street_num': _street_num,
				'building': _building,
				'phase': _phase,
				'block': _block,
				'address': _address,
				'google_place': _google_place,
				'locality': _locality,
				'floor': _floor,
				'room': _room,
				'phone': _phone,
				'id_doc': _id_doc,
				'address_type': _address_type,
				'lift': _lift,
				'staying_time': YOHO.mobile.data.seconds
			};

			if (data !== undefined && data.hasOwnProperty('id'))
			{
				params.id = data.id;
			}

			$.ajax({
				url: '/ajax/updateaddress.php',
				type: 'post',
				data: params,
				dataType: 'json',
				success: function(data) {
					if(data.status == 1)
					{
						// 沒有地址的用戶剛填寫完地址
						if ($('#user_status').val() == 2)
						{
							YOHO.mobile.checkout().flowStepAjax('shipping_type',[], function(){
								YOHO.mobile.modal.showSuccess(YOHO._('checkout_address_updated', '地址修改成功！'));
								YOHO.mobile.modal.hide(modal);
								clearInterval(staying_time);
								if($('input[name="address_radio"][value="'+data.obj.id+'"]').find()) {
									$('input[name="address_radio"][value="'+data.obj.id+'"]').closest('li').trigger('click');
								}
							});
						}
						else
						{
							YOHO.mobile.checkout().flowStepAjax('shipping_type',[], function(){
								YOHO.mobile.modal.showSuccess(YOHO._('checkout_address_updated', '地址修改成功！'));
								YOHO.mobile.modal.hide(modal);
								clearInterval(staying_time);
								if($('input[name="address_radio"][value="'+data.obj.id+'"]').find()) {
									$('input[name="address_radio"][value="'+data.obj.id+'"]').closest('li').trigger('click');
								}
							});
						}
					}
					else if(data.status == 0 || data.status == 2)
					{
						YOHO.mobile.modal.showError(YOHO._('checkout_address_error_too_long', '儲存失敗：收貨人信息過長'));
					}
					else if(data.status == 4)
					{
						YOHO.mobile.modal.showError(YOHO._('checkout_address_error_exceed', '儲存失敗：地址數量超過上限'));
					}
					else if(data.status == 7)
					{
						YOHO.mobile.modal.showError(YOHO._('checkout_address_error_invalid_format', '地址填寫錯誤'));
					}
					else if(data.status == 8)
					{
						YOHO.mobile.modal.showError(YOHO._('checkout_address_error_register_failed', '儲存失敗：電郵地址已經存在!<br>如果您已經註冊，請直接登入。'));
					}
					else
					{
						YOHO.mobile.modal.showError(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。'));
					}
				},
				error: function(xhr) {
					$saveBtn.removeClass('btn-loading');
					YOHO.mobile.modal.showError(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
					console.log(xhr);
				}
			});
		});
		$nextBtn.on('click',function() {
			if ($saveBtn.hasClass('btn-loading'))
			{
				return false;
			}
			var _administrative_area = modal.find('#administrative_area').val(),
				_zipcode             = modal.find('#addressZipcode').val(),
				_country             = modal.find('#addressCountry').val(),
				_address             = modal.find('#addressStreet').val(),
				_google_place        = modal.find('#google_place').val(),
				_street              = modal.find('#street_name').val(),
				_name                = modal.find('#addressName').val(),
				_street_num          = modal.find('#street_num').val(),
				_building            = modal.find('#building').val(),
				_phase               = modal.find('#phase').val(),
				_block               = modal.find('#block').val(),
				_locality            = modal.find('#locality').val(),
				_floor               = modal.find('#floor').val(),
				_room                = modal.find('#room').val();

			var errs = [];
			var full_address_view = false;
			if((!_phase && !_building && !_street) || !_administrative_area || !_locality || (_country != 3409 && _country != 3410 && !_zipcode))
			{
				var list = [];
				//if (!_name)   { list.push(YOHO._('checkout_address_consignee', '收貨人姓名')); }
				if (!_country){ list.push(YOHO._('checkout_address_country', '所在國家')); full_address_view = true;}
				if (!_administrative_area){ list.push(YOHO._('checkout_address_area', '地區')); full_address_view = true;}
				if (!_locality){ list.push(YOHO._('checkout_address_locality', '地方/市鎮')); full_address_view = true;}
				if (!_phase && !_building && !_street) { list.push(YOHO._('checkout_address_building_street_phase_block', '街道 或 屋苑 或 大廈')); full_address_view = true;}

				// Require zipcode if not HK / Macau
				if (_country != 3409 && _country != 3410 && !_zipcode)
				{
					list.push(YOHO._('checkout_address_zipcode', '郵遞區號'));
					full_address_view = true;
				}

				errs.push(sprintf(YOHO._('checkout_address_error_empty', '請輸入%s！'), list.join(YOHO._('account_list_delimiter', '、'))));
			}

			if (errs.length > 0)
			{
				modal.find('.error-area').show();
				$.each(errs, function (idx, err) {
					modal.find('.error-area').append(YOHO.mobile.createAlert(err, 'warning'));
				});
				if (full_address_view){
					YOHO.mobile.showFullAddressView();
				}
				return false;
			}

			$.ajax({
				url : '/ajax/address.php',
				type: 'get',
				dataType: 'json',
				data:{
					act: 'get_format_address',
					country: _country,
					sign_building: _building,
					phase: _phase,
					block: _block,
					floor: _floor,
					room: _room,
					street_name: _street,
					street_num: _street_num,
					locality: _locality,
					administrative_area: _administrative_area,
					address: _address
				},
				success:function(res){
					if(res.status == 1){
						$('#final_address').html(res.address.address);
						if (_country == 3409){
							if (res.address.type && res.address.lift){
								$('#sel_address_type').val(res.address.type);
								$('#sel_address_lift_type').val(res.address.lift);
								$('#sel_address_info').addClass('hide');
								$('#type_address').val(res.address.type);
								$('#type_lift').val(res.address.lift);
							} else {
								$('#sel_address_info').removeClass('hide');
								$('#type_address').val("");
								$('#type_lift').val("");
								$('#sel_address_type').val(-1);
								$('#sel_address_lift_type').val(0);
							}
						} else {
							$('#sel_address_info').addClass('hide');
						}
						$('.consignee_info,#save_info,#prev_info').show();
						$('#next_info,.full_address_info').hide();
						
						if (_country != 3436){
							$('#id_doc').hide()
						} else {
							$('#id_doc').show();
						}
					}
				}
			})

		});
		$prevBtn.on('click',function() {
			if ($saveBtn.hasClass('btn-loading'))
			{
				return false;
			}

			$('#final_address').html("");
			$('#next_info,.full_address_info:not(.auto_address)').show();
			if($('#format_address_display').text() == ""){
				$('.auto_address').show();
				$('.formatted_address').hide();
			}
			$('.consignee_info,#save_info,#prev_info').hide();
		});

		$('#addressIdDoc').focusout(function() {
			var _country = modal.find('#addressCountry').val();
			var _id_doc = modal.find('#addressIdDoc').val();
			if (!checkidNo(_id_doc, _country))
			{
				if (_country == 3436){
					$('#addressIdDoc').siblings("span.checkout_must_fill").html("* " + sprintf(
						YOHO._('checkout_address_error_invalid', '請輸入正確的%s！'),
						YOHO._('checkout_address_cn_id_doc_18', '18位內地身份證號碼')
					));
				}
			} else {
				$('#addressIdDoc').siblings("span.checkout_must_fill").html("*");
			}
		});

		$('#addressPhone').focusout(function() {
			var _country = modal.find('#addressCountry').val();
			var _phone = modal.find('#addressPhone').val();
			if (!checkPhoneNo(_phone, _country))
			{
				if (_country == 3436){
					$('#addressPhone').siblings("span.checkout_must_fill").html("* " + sprintf(
						YOHO._('checkout_address_error_invalid', '請輸入正確的%s！'),
						YOHO._('checkout_address_cn_phone_11', '11位手機號碼')
					));
				} else {
					$('#addressPhone').siblings("span.checkout_must_fill").html("* " + sprintf(
						YOHO._('checkout_address_error_invalid', '請輸入正確的%s！'),
						YOHO._('checkout_address_phone', '聯絡電話')
					));
				}
			} else {
				$('#addressPhone').siblings("span.checkout_must_fill").html("*");
			}
		});

		$("#sel_address_type").change(function() {
			$("#type_address").val($(this).val());
			if ($(this).val() == 2 || $(this).val() == 0){
				$("#sel_lift_option_3").show();
			} else {
				if ($("#sel_address_lift_type").val() == 3){
					$("#sel_address_lift_type").val(0);
					$("#type_lift").val(0);
				}
				$("#sel_lift_option_3").hide();
			}
		});

		$("#sel_address_lift_type").change(function() {
			$("#type_lift").val($(this).val());
		});

		function checkPhoneNo($phoneNo, $countryId) {
			var num_pattern = new RegExp('^[0-9]+$');
			$phoneNo = $.trim($phoneNo).replace(/-/g, "");
			if (num_pattern.test($phoneNo)){
				if (!YOHO.check.isMobile($phoneNo) && !YOHO.check.isTelephone($phoneNo) || ($countryId == 3436 && $phoneNo.length < 11)){
					return false;
				} else {
					return true;
				}
			} else {
				return false;
			}
		}

		function checkidNo($idNo, $countryId) {
			//var num_pattern = new RegExp('^[0-9]+$');
			$idNo = $.trim($idNo);
			if ($countryId == 3436){
				//if (!num_pattern.test($idNo) || $idNo.length < 18 || $idNo.length > 18){ // check number only and length 18
				if ($idNo.length < 18 || $idNo.length > 18){ // check length 18
					return false;
				} else {
					return true;
				}
			} else {
				return true;
			}
		}

	},

	payment: function () {

		if (!$('.other_payment').length)
		{
			// Auto submit PayPal form
			var $submitBtn = $('form[action="https://www.paypal.com/cgi-bin/webscr"]').find('input[type="submit"]');
			if ($submitBtn.length)
			{
				$submitBtn.trigger('click');
				$('<img src="/images/loading_yoho.gif" style="display: block; margin-left: auto; margin-right: auto;">').insertAfter($submitBtn);
				$submitBtn.remove();
			}

			// Auto redirect to Alipay
			var $alipayBtn = $('a[href^="https://www.alipay.com/cooperate/gateway.do"]');
			if ($alipayBtn.length)
			{
				$('<img src="/images/loading_yoho.gif" style="display: block; margin-left: auto; margin-right: auto;">').insertAfter($alipayBtn);
				$alipayBtn.hide();
				window.location.href = $alipayBtn.attr('href');
			}
		}

		// Bank transfer dialog
		var bankTransferHtml = '', banks_html = '';
		var order_id = YOHO.mobile.data.orderInfo.order_id || 0;
		$.ajax({
			url: '/ajax/bank_accounts.php?act=bankings&order_id=' + order_id,
			dataType: 'json',
			success:function(e){
				var user = e.bank_user;
				var banks = e.banks;
				banks_html = '<p><span class="gray">' + YOHO._('payment_bank_account_name', '帳戶名稱: ') + ' </span>' + user + '</p>';
				banks.map(function(v,i){banks_html += '<p><span class="gray">' + v.name + ': </span>' +v.account + '</p>'});
				bankTransferHtml = '<div class="huikuan-intro">'+
						'<h4><b>1</b> ' + YOHO._('payment_bank_transfer', 'ATM轉帳 | 網上銀行轉帳') + '</h4>'+
						'<div id="banks">' + banks_html + '</div>'+
						'<h4><b>2</b> ' + YOHO._('payment_tell_yoho', '完成後請將轉帳收據發送給友和以便核實您的付款') + '</h4>'+
						'<p><span class="gray">' + YOHO._('payment_tell_yoho_whatsapp', 'WhatsApp：') + '</span>53356996</p>'+
						'<p><span class="gray">' + YOHO._('payment_tell_yoho_email', '電郵：') + '</span><a href="mailto:info@yohohongkong.com">info@yohohongkong.com</a></p>'+
						'<p><span class="gray">' + YOHO._('payment_tell_yoho_facebook_messenger', 'Facebook Messenger：') + '</span><a href="https://m.me/yohohongkong">'+ YOHO._('payment_tell_yoho_facebook_messenger_link', '查詢') +'</a></p>'+
						'<h4><b>3</b> ' + YOHO._('payment_tell_yoho_confirm', '確認收妥款項後會馬上處理') + '</h4>'+
					'</div>';
				$('#huikuan-btn').click();
			}
		});
		$('#huikuan-btn').click(function () {
			var modal = YOHO.mobile.modal.create(
				YOHO._('payment_bank_transfer_title', '銀行轉帳'), bankTransferHtml,
				YOHO.mobile.modal.createButton(YOHO._('payment_dialog_ok', '知道了'), 'btn-primary', true)
			);
			YOHO.mobile.modal.show(modal);
		});

		// Alipay transfer dialog
		var alipayTransferHtml = ''+
			'<div class="huikuan-intro">'+
				'<h4><b>1</b> ' + YOHO._('payment_alipay', '轉帳到本公司的支付寶帳戶') + '</h4>'+
				'<p><span class="gray">' + YOHO._('payment_alipay_account', '帳戶：') + '</span>tb_lhsale@yahoo.com</p>'+
				'<p><span class="gray">' + YOHO._('payment_alipay_name', '姓名：') + '</span>胡柱</p>'+
				'<h4><b>2</b> ' + YOHO._('payment_tell_yoho', '完成後請將轉帳收據發送給友和以便核實您的付款') + '</h4>'+
				'<p><span class="gray">' + YOHO._('payment_tell_yoho_whatsapp', 'WhatsApp：') + '</span>53356996</p>'+
				'<p><span class="gray">' + YOHO._('payment_tell_yoho_email', '電郵：') + '</span><a href="mailto:info@yohohongkong.com">info@yohohongkong.com</a></p>'+
				'<p><span class="gray">' + YOHO._('payment_tell_yoho_facebook_messenger', 'Facebook Messenger：') + '</span><a href="https://m.me/yohohongkong">'+ YOHO._('payment_tell_yoho_facebook_messenger_link', '查詢') +'</a></p>'+
				'<h4><b>3</b> ' + YOHO._('payment_tell_yoho_confirm', '確認收妥款項後會馬上處理') + '</h4>'+
			'</div>';
		$('#alipay-btn').click(function () {
			var modal = YOHO.mobile.modal.create(
				YOHO._('payment_alipay_title', '支付寶轉帳'), alipayTransferHtml,
				YOHO.mobile.modal.createButton(YOHO._('payment_dialog_ok', '知道了'), 'btn-primary', true)
			);
			YOHO.mobile.modal.show(modal);
		}).trigger('click');

		// Change payment method dialog
		$('.other_payment').find('#change_payment_btn').on('click', function() {
			var paymentHtml = $('.other_payment').find('#other_payment_form_container').html();
			var submitBtn = YOHO.mobile.modal.createButton(YOHO._('payment_change_method_submit', '確定'), 'btn-primary', false);
			var modal = YOHO.mobile.modal.create(YOHO._('payment_change_method', '改用其他付款方式'), paymentHtml, submitBtn);
			YOHO.mobile.modal.show(modal);
			submitBtn.click(function () {
				modal.find('form').submit();
			});
		});

		// Check payment status
		if ($('.payment_processing').length &&
			YOHO.mobile.data.hasOwnProperty('orderInfo') && YOHO.mobile.data.orderInfo.hasOwnProperty('order_id'))
		{
			var initialCheckInterval = 3000;
			var checkInterval = initialCheckInterval;
			var checkPayStatus = function () {
				var order_id = YOHO.mobile.data.orderInfo.order_id || 0;
				if (!order_id) return;
				$.ajax({
					url: '/ajax/order_status.php',
					type: 'post',
					dataType: 'json',
					data: {
						'act': 'pay_status',
						'order_id': order_id
					},
					success: function(data) {
						if (data.status == 1)
						{
							var pay_status = data.pay_status;
							if (pay_status == 2) // PS_PAYED
							{
								// Order paid, redirect to order detail page
								window.location = '/user.php?act=order_detail&order_id=' + order_id + '&paysuccess=1';
								return;
							}
						} else if (data.status == 0) {
							if (data.hasOwnProperty('error') && data.error.length > 0)
							{
								YOHO.mobile.modal.showError(data.error);
								setTimeout(function () {
									window.location = '/respond.php?code=' + $("#pay_code").val();
								}, 3000)
							}
						// } else if (data.status == 2) {
						// 	if (data.hasOwnProperty('error') && data.error.length > 0)
						// 	{
						// 		YOHO.mobile.modal.showError(data.error);
						// 		setTimeout(function () {
						// 			window.location.reload();
						// 		}, 3000)
						// 	}
						}

						checkInterval = initialCheckInterval;
						setTimeout(checkPayStatus, checkInterval);
					},
					error: function(xhr) {
						checkInterval = checkInterval * 2;
						setTimeout(checkPayStatus, checkInterval);
						console.log(xhr);
					}
				});
			};
			setTimeout(checkPayStatus, checkInterval);
		}

		if($('#stripe-payment-form').length){

			// Stripe V3 js config
			var publishableKey = document.getElementById('publishableKey').value;
			var stripe = Stripe(publishableKey);
			// From button
			var btn        = document.getElementById('stripeBtn');
			// Processing model
			var model      = document.getElementById('processing-model');
			// Hidden input field
			var amount     = document.getElementById('amount');
			var currency   = document.getElementById('currency');
			var returnUrl = document.getElementById('return-url');
			var source = document.getElementById('source');
			var order_id = document.getElementById('order_id').value;
			var log_id = document.getElementById('log_id').value;
			var pay_code = document.getElementById('pay_code').value;
			// Error message div
			var displayError = $('#stripe-error');
			// Default vaild
			var numberComplete = false;
			var cvcComplete = false;
			var expiryComplete = false;
			// Default disable from button.
			disableBtn();

			// Setup stripe elements
			var elements = stripe.elements({locale:'en'});
			var style = {
				base: {
				'::placeholder': {
					color: '#ccc',
				},
				},
				invalid: {
				color: '#e5424d',
				':focus': {
					color: '#303238',
				},
				}
			};
			// Card number
			var cardNumber = elements.create('cardNumber',{placeholder:'‎4111 1111 1111 1111', style: style});
			cardNumber.mount('#card-number');
			// Card cvc
			var cardCvc = elements.create('cardCvc',{style: style});
			cardCvc.mount('#card-cvc');
			// Card expiry date
			var cardExpiry = elements.create('cardExpiry',{style: style});
			cardExpiry.mount('#card-expiry');
			// Event : cardNumber
			cardNumber.addEventListener('change', function(event) {
				if (event.error) {
					displayError.text(event.error.message);
					displayError.show();
				} else {
					displayError.text('');
					displayError.hide();
					//check type
					var type = event.brand.toLowerCase();
					type = type.replace(" ", "-");
					card_type = type;
					$('#card-image').removeClass().addClass(type);
				}
				if(event.complete == true){
					$('#card-number').addClass('StripeElement--success');
					numberComplete = true;
					checkFromComplete();
				} else {
					$('#card-number').removeClass('StripeElement--success');
					numberComplete = false;
					disableBtn();
				}
			});
			// Event : cardCvc
			cardCvc.addEventListener('change', function(event) {
				if (event.error) {
					displayError.text(event.error.message);
					displayError.show();
				} else {
					displayError.text('');
					displayError.hide();
				}
				if(event.complete == true){
					$('#card-cvc').addClass('StripeElement--success');
					cvcComplete = true;
					checkFromComplete();
				} else {
					$('#card-cvc').removeClass('StripeElement--success');
					cvcComplete = false;
					disableBtn();
				}
			});
			// Event : cardExpiry
			cardExpiry.addEventListener('change', function(event) {
				if (event.error) {
					displayError.text(event.error.message);
					displayError.show();
				} else {
					displayError.text('');
					displayError.hide();
				}
				if(event.complete == true){
					$('#card-expiry').addClass('StripeElement--success');
					expiryComplete = true;
					checkFromComplete();
				} else {
					$('#card-expiry').removeClass('StripeElement--success');
					expiryComplete = false;
					disableBtn();
				}
			});

			document.querySelector('#stripe-payment-form').addEventListener('submit', function(e) {

				// Set view
				disableBtn();
				btn.innerHTML = YOHO._('goods_feedback_submiting', '提交中...');
				model.classList.remove('hidden');

				e.preventDefault();
				var form = document.querySelector('#stripe-payment-form');
				var extraDetails = {

				};
				stripe.createSource(cardNumber).then(function(result) {

					if (result.error) {
						// Inform the user if there was an error
						var errorElement = document.querySelector('.stripe-error');
						errorElement.textContent = result.error.message;
						errorElement.classList.add('visible');
						enableBtn();
						model.classList.add('hidden');
					} else {
						if(card_type != "amex"){
							stripe3DSecure(result.source);
						} else {
							// AE card is not support 3DS.
							// Success, get the source ID:
							// Insert the source into the form so it gets submitted to the server:
							source.value = result.source.id;
							// Submit the form:
							form.submit();
						}
					}
				});
			});

			/* Function */
			function checkFromComplete(){
				if(expiryComplete && cvcComplete && numberComplete){
					enableBtn();
				} else {
					disableBtn();
				}
			}

			function disableBtn(){
				btn.setAttribute('disabled', 'disabled');
				btn.classList.add('graybtn');
				btn.innerHTML = YOHO._('sumbit', '提交');
			}
			function enableBtn(){
				btn.removeAttribute('disabled', 'disabled');
				btn.classList.remove('graybtn');
				btn.innerHTML = YOHO._('sumbit', '提交');
			}

			function stripe3DSecure(token){
				stripe.createSource({
				type: 'three_d_secure',
				amount: amount.value,
				currency: currency.value,
				three_d_secure: {
					card: token.id
				},
				redirect: {
					return_url: returnUrl.value
				},
				metadata: {
					'order_id': order_id,
					'pay_code': pay_code,
					'log_id':   log_id
				}
				}).then(function(result) {
					if (result.error) {
						// Inform the user if there was an error
						var errorElement = document.querySelector('.stripe-error');
						errorElement.textContent = result.error.message;
						errorElement.classList.add('visible');
						enableBtn();
						model.classList.add('hidden');
					} else {
						// var url = result.source.redirect.url;
						// location.href = url;
						var height = ($(window).height() * 0.7 );
						$('.p-modal-frame').css('width','90%');
						$('#model-body').html('<iframe style="width:100%; height: '+height+'px;" frameborder="0" src="' + result.source.redirect.url + '"></iframe>');
					}
				});
			}
		}
		//Stripe WeChat
		if($('#stripe-wc-payment-form').length){
			var publishableKey = document.getElementById('publishableKey').value;
			var stripe = Stripe(publishableKey);
			// Hidden input field
			var amount     = document.getElementById('amount');
			var currency   = document.getElementById('currency');
			var source = document.getElementById('source');
			var order_id = document.getElementById('order_id').value;
			var log_id = document.getElementById('log_id').value;
			var pay_code = document.getElementById('pay_code').value;
			var qrCode = $('#qrCode');


			stripe.createSource({
				type: 'wechat',
				amount: amount.value,
				currency: currency.value,
				metadata: {
					'order_id': order_id,
					'pay_code': pay_code,
					'log_id':   log_id
				}
			}).then(function(result) {
				var qr_code_url = result.source.wechat.qr_code_url;
				qrCode.attr("href", qr_code_url);
				$.ajax({
					url: '/ajax/qrcode.php',
					type: 'post',
					dataType: 'json',
					data: {
						'qr_code_url': qr_code_url,
						'order_id':    order_id
					},
					success: function(data) {

						if(data.status == 1){ //success
							qrCode.html('<img src="'+data.qr_code+'" />');
						} else {
							YOHO.mobile.modal.showError('伺服器繁忙，請稍後再試。('+xhr.status+')');
						}
					}
				});
				// qrCode.html('<img src="https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl='+qr_code_url+'" />');
			});
		}

		if($('#braintree-creadit-card').length){

			var clientToken = document.querySelector('#clientToken').value;
			var form = document.querySelector('#brain-cardForm');
			var payBtn = document.getElementById('button-pay');
			var nonceGroup = document.querySelector('.nonce-group');
			var nonceInput = document.querySelector('.nonce-group input');
			var payGroup = document.querySelector('.pay-group');
			var modal = document.getElementById('modal');
			var bankFrame = document.querySelector('.bt-modal-body');
			var closeFrame = document.getElementById('text-close');
			var components = {
				client: null,
				dataCollector: null,
				threeDSecure: null,
				hostedFields: null
			};


			braintree.client.create({
				authorization: clientToken
			},onClientCreate);


			function onClientCreate(err,client){

				if(err){
					console.log('client error:', err);
					return;
				}

				components.client = client;

				// Braintree Hosted Fields
				braintree.hostedFields.create(
				{
				  	client: client,
				  	styles: {
				    	'input': {
				      		'font-size': '14px',
				      	    'line-height': '35px'
				    	},
					    'input.invalid': {
					      	'color': 'red'
					    },
					    'input.valid': {
					      	'color': 'green'
					    }
				  	},
				  	fields: {
					    number: {
					      	selector: '#card-number',
						  	placeholder: 'xxxx xxxx xxxx xxxx'
					    },
					    cvv: {
					      	selector: '#cvv',
						  	placeholder: '123'
					    },
					    expirationDate: {
					      	selector: '#expiration-date',
						  	placeholder: 'MM/YYYY'
					    }
				  	}
			 	}, onComponent('hostedFields'));

				//3D Secure
				braintree.threeDSecure.create({
				    client: client
				  }, onComponent('threeDSecure'));

				//Data collector (kount)
				braintree.dataCollector.create({
				    client: client,
				    kount: true
				  }, onComponent('dataCollector'));
			}

			function onComponent(name){

				return function(err, component) {
				    if (err) {
				    	console.log('component error:', err);
				    	return;
				    }

				    components[name] = component;

				    if (components.threeDSecure && components.hostedFields && components.dataCollector) {
				    	setupForm();
				    }
				}
			}

			function setupForm() {
			  	enablePayNow();
			}

			function addFrame(err, iframe) {
				bankFrame.appendChild(iframe);
				modal.classList.remove('hidden');
			}

			function removeFrame() {
				var iframe = bankFrame.querySelector('iframe');
				modal.classList.add('hidden');
				iframe.parentNode.removeChild(iframe);
			}

			function enablePayNow() {
				payBtn.removeAttribute('disabled', 'disabled');
				payBtn.classList.remove('graybtn');
				components.hostedFields.on('validityChange', function (event) {

			      	// Check if all fields are valid, then show submit button
			      	var formValid = Object.keys(event.fields).every(function (key) {
			        	return event.fields[key].isValid;
			      	});

			      	if (formValid) {
			      		payBtn.classList.add('show-button');
			      	} else {
			      		payBtn.classList.remove('show-button');
			      	}
		    	});
				components.hostedFields.on('empty', function (event) {
						$('#card-image').removeClass();
						$(form).removeClass();
			    });
				components.hostedFields.on('cardTypeChange', function (event) {
						// Change card bg depending on card type
					if (event.cards.length === 1) {
				        $(form).removeClass().addClass(event.cards[0].type);
				        $('#card-image').removeClass().addClass(event.cards[0].type);
				        $('header').addClass('header-slide');

						// Change the CVV length for AmericanExpress cards
				        if (event.cards[0].code.size === 4) {
				        	components.hostedFields.setAttribute({
				            	field: 'cvv',
				            	attribute: 'placeholder',
				            	value: '1234'
				          	});
				        } else {
				      		components.hostedFields.setAttribute({
				          		field: 'cvv',
				          		attribute: 'placeholder',
				          		value: '123'
							});
						}
			      	} else {
			      		components.hostedFields.setAttribute({
			          		field: 'cvv',
			          		attribute: 'placeholder',
			          		value: '123'
						});
					}
				});
			}

			closeFrame.addEventListener('click', function () {
				components.threeDSecure.cancelVerifyCard(removeFrame());
				enablePayNow();
			});

			payBtn.addEventListener('click', function(event) {
				payBtn.setAttribute('disabled', 'disabled');
				payBtn.classList.add('graybtn');
				var amount = document.getElementById('amount').value;
				components.hostedFields.tokenize(function(err, payload) {
				    if (err) {
				    	enablePayNow();
				    	return;
				    }

				    components.threeDSecure.verifyCard({
				    	amount: amount,
				    	nonce: payload.nonce,
				    	addFrame: addFrame,
				    	removeFrame: removeFrame
				    }, function(err, verification) {
				    	if (err) {
				    		enablePayNow();
				    		return;
				    	}

						var deviceData = components.dataCollector.deviceData;
				    	document.querySelector('input[name=\"device_data\"]').value = deviceData;
				    	document.querySelector('input[name=\"payment_method_nonce\"]').value = verification.nonce;
	    				form.submit();
				    });
				});
			});
		}
		YOHO.mobile.flow();
	},

	regEmail: function() {
		var subscribed = $.cookie('email_subscribed');
		if (subscribed == '3')
		{
			YOHO.mobile.modal.showSuccess(
				YOHO._('index_email_title', '登記友和電子報'),
				YOHO._('index_email_result_confirmed', '謝謝，您已完成了電子報的訂閱確認。')
			);
			$.cookie('email_subscribed', '1', { expires: new Date(2038,1,1), path: '/', domain: '.yohohongkong.com' });
		}
		else if (subscribed == '2')
		{
			YOHO.mobile.modal.showSuccess(
				YOHO._('index_email_title', '登記友和電子報'),
				YOHO._('index_email_result_registered', '謝謝，您已成功登記電郵地址。')
			);
			$.cookie('email_subscribed', '1', { expires: new Date(2038,1,1), path: '/', domain: '.yohohongkong.com' });
		}
		// TODO: Let people signup for newsletter on mobile
	},

	languageSelector: function () {
		if (navigator.cookieEnabled == true && !$.cookie('is_spider') && !$.cookie('yoho_language')) {
			showlanguageSelector()
		}
		$('a.language-toggle').click(function () {
			var new_lang = $(this).data('langCode');
			if (new_lang != YOHO.lang)
			{
				// Set a cookie that will expire in 1 year
				var expire_date = new Date();
				expire_date.setFullYear(expire_date.getFullYear() + 1);
				$.cookie('yoho_language', new_lang, { expires: expire_date, path: '/', domain: '.yohohongkong.com' });
				var windown_href = window.location.href;
				var windown_href = windown_href.replace(/en_us\/|zh_tw\/|zh_cn\/|\?lang=tc/g, '');
				window.location.href = windown_href;
				//window.location.reload();
			}
		});
		//language selector pop up
		function showlanguageSelector() {
			var html = ''+
					'<div class="lang_dialog cle">' +
						'<img src="/images/welcome_msg.png">' +
						'<div class="language-btn">' +
							'<span data-lang-code="zh_tw">繁體中文</span>' +
							'<span data-lang-code="zh_cn">简体中文</span>' +
							'<span data-lang-code="en_us">English</span>' +
						'</div>' +
						'<div class="lang_shadow">' +
						'</div>' +
					'</div>';
			$(html).appendTo("body");
			var win_height = $(window).height(); 
			var win_width = $(window).width(); 
			var doc_height = $(document).height(); 
			$('.lang_dialog img').on('load',function(){
				var img_height = $('.lang_dialog img').height();
				var top = (win_height-img_height) /2 - 50;
				$(".lang_dialog").css({"top":top});
				$(".lang_dialog .lang_shadow").css({"width":win_width, "height":doc_height});
			});		
			$('.lang_dialog span').click(function () {
				var new_lang = $(this).data('langCode');
				// Set a cookie that will expire in 1 year
				var expire_date = new Date();
				expire_date.setFullYear(expire_date.getFullYear() + 1);
				$.cookie('yoho_language', new_lang, { expires: expire_date, path: '/', domain: '.yohohongkong.com' });
				var windown_href = window.location.href;
				var windown_href = windown_href.replace(/en_us\/|zh_tw\/|zh_cn\/|\?lang=tc/g, '');
				window.location.href = windown_href;
			});
			$('.lang_shadow').click(function(){
				$('.lang_dialog').remove();
			});
		}
	},

	brandsIndex: function () {
		$('a.brands-index-letter').click(function () {
			var $letter = $(this).data('letter');
			var $brands = document.getElementById('brands-collapse-' + $letter);
			var $collapsed = $brands.closest('.brands-col');
				$brands.className += " in";
				var top = $collapsed.offsetTop || 0;
				var navBarHeight = $('.navbar-fixed-top').height() || 0;
				$(document).scrollTop(top - navBarHeight);
		});
	},

	/* For splash Ads */
	splashAd: function(){

		/* Close btn */
		$('i.btnCloseSplashAd').click(function () {
			$('.SplashAd').hide();
		});
	},

	footUser: function(){
		$('.navbar-toggle.sidebar-toggle').trigger("click");
		setTimeout(function() {
			var left_manu = $('#sidebar-user-menu');
			if(!left_manu.hasClass('in'))
			{
				$('*[data-target="#sidebar-user-menu"]').trigger("click");
			}
		}, 200);
	},
	//返回頂部
	back2top: function () {
		var $win = $(window),
			_wHeight = $win.height();

		var $back2top = $('<div class="back2top"><a class="back2btn" hidefocus=true href="javascript:;"><em></em></a></div>').appendTo("body");

		if($win.scrollTop() < 50){
			$back2top.hide();
		}else{
			$back2top.show();
		}

		$win.scroll(function () {
			var _sTop = 0;
			if (YOHO.check.isIE6) {
				_sTop = $win.scrollTop();
				$back2top.css({
					'top' : _sTop + _wHeight - 89
				});
			}
			if($win.scrollTop() < 50){
				$back2top.hide();
			}else{
				$back2top.show();
			}

			// setTimeout(function() {
			// 	$back2top.hide();
			// },5000);
		});

		$back2top.find(".back2btn").click(function () {
			$('body,html').animate({ scrollTop : 0 }, 500);
			return false;
		});
	},
	indexRank: function () {
		var first = $('.rank-header>div>a.active');
		var color = first.data('theme');

		first.css('background',color);

		$('.rank-header>div>a').click(function () {

			var last = $('.rank-header>div>a.active');
			var last_id = last.data('rid');
			last.removeClass('active');
			last.css('background','transparent');
			$(last_id).hide();

			$(this).addClass('active');
			var id = $(this).data('rid');
			var color = $(this).data('theme');
			$(this).css('background',color);
			$(id).show();
		});
		var glBrowserWidth = $(window).innerWidth();
		//if we're on a large desktop, organize the slides in 6 columns
		if (glBrowserWidth > 991) {
			resizeSlider(6);
			//Large Tablet - 4 columns
		} else if (glBrowserWidth > 735) {
			resizeSlider(4);
			//Small Tablet - 2 column
		} else {
			resizeSlider(2);
		}
		$(window).resize(function() {

			var browserWidth = $(window).innerWidth();
			if(glBrowserWidth != browserWidth){
				glBrowserWidth = browserWidth;
				//if we're on a large desktop, organize the slides in 6 columns
				if (browserWidth > 991) {
					resizeSlider(6);
					//Large Tablet - 4 columns
				} else if (browserWidth > 735) {
					resizeSlider(4);
					//Small Tablet - 2 column
				} else {
					resizeSlider(2);
				}
			}
		}).resize();

		$(window).on("load", function() {
			$('.carousel.slide').each(function(n) {
				var slide_height = $(this).height();
				$(this).find('a.single-item').css({"height":slide_height})
			});
		});

		function resizeSlider(numColumns) {
			//Translate number of bootstrap columns
			if (numColumns == 6) {
				var bsColumn = "col-xs-2";
			} else if (numColumns == 4) {
				var bsColumn = "col-xs-3";
			} else if (numColumns == 3) {
				var bsColumn = "col-xs-4";
			} else if (numColumns == 2) {
				var bsColumn = "col-xs-6";
			} else if (numColumns == 1) {
				var bsColumn = "col-xs-12";
			}
			$('.carousel.slide').each(function(n) {
				var id = $(this).attr('id');
				//Upwrap the slide images from their containing divs
				$("#"+id+" .single-item").unwrap().unwrap();
				//Wrap every card in the appropriate bootstrap column
				$("#"+id+" .single-item").wrap("<div class='" + bsColumn + " col-item'></div>");
				var slideWrappers = $("#"+id+" .col-item");
				//Wrap every 3 cards in an item class, forming 1 whole slide
				for (var i = 0; i < slideWrappers.length; i += numColumns) {
					if (i == 0) {
						var activeItem = ' active';
					} else {
						var activeItem = '';
					}
					slideWrappers.slice(i, i + numColumns).wrapAll("<div class='item" + activeItem + "'></div>");
				}
			});
		}
	},
	categoryList: function () {
		var $content_height = ($(window).height() - $('.navbar-header').outerHeight());
		$(".cat-left>li").each(function(n) {
			var $color  = $(this).children('a').data('color');
			var $id     = $(this).attr('id');
			var $height = $(this).children('a').outerHeight();
			var $cid    = $(this).children('a').data('cid');
			$('body').append('<style>.cat-left>li#'+$id+'.active:after{background: ' + $color + ';height: '+$height+'px;}</style>');
			$('#'+$cid).css('min-height', ($content_height-5)+'px');
		});
		var $height = $(window).height();
		var $top_height = $('.navbar-header').height();
		var $foot_height = $('footer').height();

		$('.cat-menu-left').css('height', $content_height);
		$('.cat-menu-right').css('height', $content_height);

		$(".cat-left>li>a").click(function(n){
			$('.tab-pane.active').removeClass('active');
			var $id = $(this).data('cid');
			$('#'+$id).addClass('active');
			$('.cat-menu-right').css('height', 0);
			$('.cat-menu-right').scrollTop(0);
			$('.cat-menu-right').animate({ height: $content_height+'px' }, 200);
			$('#'+$id).css('min-height', ($content_height-5)+'px');
			// $('.cat-menu-right').scrollTop(0);
		});

	},
	filterMenu: function() {
		var $filterBtn = $('#filter-list-btn');
		var $filterNav = $('.filter-navigation');
		var height = window.innerHeight;
		var resize = function() {
			if(window.innerHeight != height) {
				height = window.innerHeight;
				$(".filter-content").css('height', height + 'px');
			}
		};
		if($('.fix-filter-box').length > 0){
			$('.page-content>.container').css('margin-top', ($('.fix-filter-box').outerHeight() + 5)+'px');
		}
		$filterBtn.click(function(n){
			var $target = $(this).data('target');
			$(".filter-content").css('height', height + 'px');
			$($target).css('left',0);
			$(window).resize(function() {
				resize();
			});
			$('body').addClass('lock-position');
		});
		$('.cover-floor').click(function(n){
			$(this).parent().css('left','100%');
			$('body').removeClass('lock-position');
		});
		$('.more-filters').each(function() {
			var $this = $(this);
			var orgHeight = $this.height();
			var displayHeight = $this.children('a').outerHeight(true);
			if (orgHeight > displayHeight)
			{
				$this.css({
					'overflow': 'hidden',
					'height': displayHeight+'px'
				});

				var $more = $('<a class="filter-more-btn"><span>' + YOHO._('productlist_brand_filter_more', '更多') + '</span> <i class="fa fa-lg fa-angle-down"></i></a>');
				$more.insertAfter($this);
				$more.click(function () {
					if ($more.find('i.fa-angle-down').length)
					{
						$this.animate({ 'height': orgHeight + 'px' }, 500, function () {
							$this.css({ 'overflow': '' });
							$more.find('span').text(YOHO._('productlist_brand_filter_less', '收起'));
							$more.find('i.fa').addClass('fa-angle-up').removeClass('fa-angle-down');
						});
					}
					else
					{
						$this.animate({ 'height': displayHeight+'px' }, 500, function () {
							$this.css({ 'overflow': 'hidden' });
							$more.find('span').text(YOHO._('productlist_brand_filter_more', '更多'));
							$more.find('i.fa').addClass('fa-angle-down').removeClass('fa-angle-up');
						});
					}
				});
			}
		});
		$('.price-range-slider').each(function () {
			var $slider = $(this);
			var $form = $slider.closest('form');
			var $input_min = $form.find('input[name="price_min"]');
			var $input_max = $form.find('input[name="price_max"]');
			var rangeFrom = $slider.data('rangeFrom');
			if (rangeFrom === null || rangeFrom === '')
			{
				rangeFrom = $input_min.val();
			}
			if (rangeFrom === null || rangeFrom === '')
			{
				rangeFrom = 0;
			}
			var rangeTo = $slider.data('rangeTo') || $input_max.val() || 20000;
			var rangeScale = $slider.data('rangeScale');
			if (!$.isArray(rangeScale) || rangeScale[rangeScale.length-1] == 0)
			{
				rangeScale = [rangeFrom, rangeTo];
			}
			var rangeStep = $slider.data('rangeStep') || 1;
			var width = $slider.data('width') || 180;
			if(width.indexOf("%") >= 0){
				var $content = $slider.parent().parent('.content');
				width = $content.width() * (parseInt(width)/100);
			}
			$slider.jRange({
				from: rangeFrom,
				to: rangeTo,
				step: rangeStep,
				scale: rangeScale,
				format: '$%s',
				width: width,
				showLabels: true,
				theme: 'theme-blue',
				isRange: true,
				onstatechange: function (value) {
					var tmp = value.split(',');
					var price_min = parseFloat(tmp[0]);
					var price_max = parseFloat(tmp[1]);
					// Use 0 for price_max if it is at the max. value
					if (price_max == rangeTo)
					{
						price_max = 0;
					}
					$input_min.val(price_min);
					$input_max.val(price_max);
					setTimeout(function() {
						$form.submit();
					}, 500);
				}
			});
		});
		$('.product-list-mode-toggle').click(function () {
			var mode = $(this).data('mode');
			// Set a cookie that will expire in a week
			var expire_date = new Date();
			expire_date.setDate(expire_date.getDate() + 7);
			$.cookie('m_display', mode, { expires: expire_date });
			window.location.reload();
		});
	},
	resizeImgMap: function() {
		$('img[usemap]').rwdImageMaps();
	},
	showRelated: function(){
		var html = document.getElementById('suggest-items').innerHTML;
		var modal = YOHO.mobile.modal.create(
			YOHO._('goods_discontinued', '此產品已停售'), html
		);
		YOHO.mobile.modal.show(modal);
		//var redirect = $('#showRelated li a')[0].href;
		//setTimeout(function(){location.href = redirect}, 10000);
		//setInterval(function(){$('span#countdown').html(parseInt($('span#countdown').html()) > 0 ? parseInt($('span#countdown').html()) - 1 : 0)}, 1000);
	},
	addToCartSuccess: function(type, addQty, product){
		var body = "";
		if(type == "gift"){
			body = sprintf(YOHO._('cart_add_success_favourable', '%1s<br><br>及%2s已加入購物車'), "<span class='reminder_bold'>" + product + "</span>", "<span class='reminder_highlight'>"+ YOHO._('gifts', '贈品') +"</span>")
		}
		else if(type == "redeem"){
			body = sprintf(YOHO._('cart_add_success_favourable', '%1s<br><br>及%2s已加入購物車'), "<span class='reminder_bold'>" + product + "</span>", "<span class='reminder_highlight'>"+ YOHO._('redeem_goods', '換購產品') +"</span>")
		}
		else if(type == "package"){
			body = "<span class='reminder_bold'>" + product + "</span><br><br>" + sprintf(YOHO._('cart_add_success_package', '%s 件組合產品已加入購物車'), "<span class='reminder_highlight'>" + addQty + "</span>")
		}
		else{
			body = "<span class='reminder_bold'>" + product + "</span><br><br>" + YOHO._('add_goods_success', '已成功加入購物車');
		}

		var modal = YOHO.mobile.modal.showAddCartSuccess(body);
		setTimeout(function (){
			YOHO.mobile.modal.hide(modal);
		}, 700);
		
		//var redirect = $('#showRelated li a')[0].href;
		//setTimeout(function(){location.href = redirect}, 10000);
		//setInterval(function(){$('span#countdown').html(parseInt($('span#countdown').html()) > 0 ? parseInt($('span#countdown').html()) - 1 : 0)}, 1000);
	},
	checkoutProcess: function(){
		$('#checkout').click(function(){
			var success_callback = function(){
				$(this).html('<img src="/images/loading.gif" /> ' + YOHO._('cart_submitting', '提交中'));
				setTimeout(function () {
					location.href = '/checkout';
				}, 100);
			}
			$preorder = $('.preorder_detail').get();
			$require_goods = $('.require_goods_detail').get();
			var confirm_dialog = [];
			if ($preorder.length > 0 || $require_goods.length > 0) {
				$(".glyphicon-question-sign.text-info").hide()
				if($preorder.length > 0) {
					var info = $preorder.reduce(function(p,c,i){
						return p += (i + 1) + ". <b>"+$(c).attr('id')+"</b> - " + $(c).val() + "<br><br>"
					}, "<br><br>");
					confirm_dialog.push(YOHO._('checkout_has_preorder_goods', '以下產品需要訂貨：') + info);
					// YOHO.mobile.modal.showQuestion(YOHO._('checkout_preorder_goods', '需訂貨產品'), YOHO._('checkout_has_preorder_goods', '以下產品需要訂貨：') + info, success_callback);
				}
				if ($require_goods.length > 0) {
					confirm_dialog.push(YOHO._('checkout_has_missing_require_goods', '以下產品需額外購買變壓器：') + "<br>" +
					$require_goods.reduce( function(p,c,i) {
						p += "<div style='margin-top: 10px;'>" +
								"<div class='row'>" +
									"<div class='col-xs-12'>" + $(c).attr('id') + "</div>" +
								"</div>" +
								"<div class='row'>" +
									"<div class='col-xs-8'>" +
										"<div class='col-xs-12'>" +
											"<a href='product/" + $(c).data('tid') + "'>" + $(c).val() + "</a> " +
										"</div>" +
										"<div class='col-xs-12'>" +
											"<span style='color: #d4062e;'>" + $(c).data('price') + "</span>" +
										"</div>" +
									"</div>" +
									"<div class='col-xs-4'>" +
										"<button class='require_addtocart btn btn-sm btn-primary' data-rid='" + $(c).data('tid') + "' onclick='addToCart(" + $(c).data('tid') + ")' data-rid='" + $(c).data('tid') + "'>" +
											"<i class='fa fa-fw fa-shopping-cart'></i> " + YOHO._('productlist_add_to_cart_mobile', '加入購物車') +
										"</button>" +
									"</div>" +
								"</div>" +
							"</div>";
						return p;
					}, ""));
				}
				if ($(".cart-goods-list").data("type") == "mix") {
					confirm_dialog.push(YOHO._('checkout_has_multiple_delivery_party', '由於你購買的貨品在分別在不同倉庫，訂單會分開寄出，所以你將會分開收到你的包裹。'));
				}
				YOHO.mobile.modal.showQuestion(YOHO._('cart_checkout_notice', '下單需知'), confirm_dialog.join("<hr class='checkout_notice'>"), success_callback);
			}
			else
			{
				if ($(".cart-goods-list").data("type") == "mix") {
					var confirm_dialog = [];
					confirm_dialog.push(YOHO._('checkout_has_multiple_delivery_party', '由於你購買的貨品在分別在不同倉庫，訂單會分開寄出，所以你將會分開收到你的包裹。'));
					YOHO.mobile.modal.showQuestion(YOHO._('cart_checkout_notice', '下單需知'), confirm_dialog.join("<hr class='checkout_notice'>"), success_callback);
				} else {
					success_callback();
				}
			}
		})
	},
	quotationProcess: function(){
		$('#quotation').click(function(){
			var success_callback = function(){
				location.href = '/quotation';
			}
			$preorder = $('.preorder_detail').get();
			if($preorder.length > 0) {
				var info = $preorder.reduce(function(p,c,i){
					//return p += (i + 1) + ". <b>"+$(c).attr('id')+"</b> - " + sprintf(YOHO._('goods_delivery_timing_preorder', '一般於 %s 個工作天內派送。'), $(c).val() + "</span>") + "<br><br>"
					return p+=(i+1)+". <b>"+$(c).attr('id')+"</b> - "+$(c).val()+"<br><br>";
				}, "<br><br>");
				YOHO.mobile.modal.showQuestion(YOHO._('checkout_preorder_goods', '需訂貨產品'), YOHO._('checkout_has_preorder_goods', '以下產品需要訂貨：') + info, success_callback);
			}
			else
			{
				success_callback();
			}
		})
	},
	checkSMSValiated: function() {
		$.ajax({
			url: "",
			data: {

			},
			success: function(res) {
				// Disable SMS validation if already verified
				if(false) {
					$('#sms_validation').html('<div class="col-xs-9" style="vertical-align: middle;"><label class="control-label profile-form-label"><i class="fa fa-check" aria-hidden="true"></i> ' + YOHO._('user_sms_already', '你已經成功驗證此電話號碼') + '</label></div>')
				}
			}
		})
	},
	sendSMS: function() {
        $('#send_sms').click(function(e){
			$.ajax({
				url: '/ajax/verify_sms.php',
				type: 'POST',
				data: {
					'send_phone': $(e.target).data('mobile'),
					'act': 'send_sms'
				},
				async: false,
				dataType:'json',
				success: function(data) {
					if(data.status == 3) //Not open SMS Verify
					{
						YOHO.mobile.modal.showError(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。'));
						setTimeout(function(){ location.href = '/user.php'; }, 3000);
					}
					if(data.status == 1)
					{
						$('#verify_sms').show();
						$('#send_sms').hide();
						$('#sms_verify').prop('disabled', false);
						$('#sms_verify').val("");
					}
					else
					{
						YOHO.mobile.modal.showError(data.error);
						res = false;
					}
				},
				error: function(xhr) {
					YOHO.mobile.modal.showError(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
				}
			});
        })
	},
	verifySMS: function() {
			$('#verify_sms').click(function(e) {
		  // Verify SMS code
		  $.ajax({
			url: '/ajax/verify_sms.php',
			type: 'POST',
			data: {
				'send_phone': $(e.target).data('mobile'),
				'verify_code': $('#sms_verify').val(),
				'act': 'verify_sms'
			},
			dataType:'json',
			success: function(data) {
				if(data.status == 3) //Not open SMS Verify
				{
					YOHO.mobile.modal.showError(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。'));
				}
				if(data.status == 1)
				{
					YOHO.mobile.modal.showSuccess(YOHO._('user_sms_already', '你的手機已經驗證成功。'));
					setTimeout(function(){ location.href = '/user.php?act=profile'; }, 3000);
				}
				else
				{
					YOHO.mobile.modal.showError(data.error);
				}
			},
			error: function(xhr) {
				YOHO.mobile.modal.showError(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
			}
		});
		})
	},
	// SMS Verify
	smsverify: function() {
		var _ccc_mobile = $('#sms-verify input[name="send_phone"]').val();
		var str1 = _ccc_mobile.indexOf("+");
		var str2 = _ccc_mobile.indexOf("-");
		var _ccc = _ccc_mobile.substring(str1+1,str2);
		var _mobile = _ccc_mobile.substring(str2+1);
		var $msg = $('#sms-error-msg');
		$('.send_btn').click(function() {
			if($('.send_btn').hasClass('disabled')){
				return;
			}
			//sms verify
			var countdown = 180;
			var timer;
			$('.send_btn').addClass('disabled');
			setTime($(this),countdown);
			$.ajax({
				url: '/ajax/verify_sms.php',
				type: 'POST',
				data: {
					'ccc': _ccc,
					'mobile': _mobile,
					'send_phone': _ccc_mobile,
					'act': 'send_sms'
				},
				async: false,
				dataType:'json',
				success: function(data) {
					if(data.status == 3) //Not open SMS Verify
					{
						YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。'));
						setTimeout(function(){ location.href = '/user.php'; }, 3000);
					}
					if(data.status == 1)
					{
						clearTimeout(timer);
						countdown = 0;
					}
					else
					{
						YOHO.dialog.warn(sprintf(
							data.error
						));
					}
				},
				error: function(xhr) {
					console.log(xhr)
					YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
				}
			});


		

			
		});		

		function setTime(obj,countdown){
			if(countdown ==0) {
				obj.removeClass('disabled');
				obj.html(YOHO._('user_send_verify', '發送驗證碼'));
				countdown = 180;
				return;
			}else {
				obj.addClass('disabled');
				obj.html(countdown + "秒後" + YOHO._('send_again', '重新發送'));
				countdown--;
			}
			timer = setTimeout(function(){
				setTime(obj,countdown)
			},1000)

		}

		$('#check_verify_code').click(function(){
			$btn = $(this);
			if($btn.find('img').length > 0)
			{
				return false;
			}
			// $btn.html('<img src="/images/loading.gif" /> ' + YOHO._('account_submitting', '提交中'));
			$btn.html(YOHO._('account_submitting', '提交中'));	
			$.ajax({
				url: '/ajax/verify_sms.php',
				type: 'POST',
				data: {
					'ccc': _ccc,
					'mobile': _mobile,
					'send_phone': _ccc_mobile,
					'verify_code': $('.verify_code').val(),
					'act': 'verify_sms'
				},
				dataType:'json',
				success: function(data) {
					if(data.status == 3) //Not open SMS Verify
					{
						$btn.html(YOHO._('user_verify', '驗證'));
						$msg.html('').append(YOHO.mobile.createAlert(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。'), 'warning'));
						setTimeout(function(){ location.href = '/user.php'; }, 3000);
					}
					if(data.status == 1)
					{
						$msg.html('').append(YOHO.mobile.createAlert(YOHO._('user_sms_already', '你的手機已經驗證成功。'), 'warning'));
						setTimeout(function(){ location.href = '/user.php'; }, 3000);
					}
					else
					{
						$msg.html('').append(YOHO.mobile.createAlert(data.error, 'warning'));
						$btn.html(YOHO._('user_verify', '驗證'));
					}
				},
				error: function(xhr) {
					$btn.html(YOHO._('user_verify', '驗證'));
					$msg.html('').append(YOHO.mobile.createAlert(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')', 'warning'));
				}
			});
		});
	},
	// affiliateLink
	affiliateLink: function() {
		$('#affiliatelink').click(function (evt) {
			evt.preventDefault();
			$url = $(this).data('share');
			$is_login = $(this).data('is_login');
			$banner = $(this).data('banner');
			$title = $(this).data('title');
			$text = $(this).data('text') || $url;
			$content = '';
			if($banner) $content += '<div style="margin-bottom:10px;"><img src="'+$banner+'" width="100%"></div>';
			if($is_login == 'y') {
				$content += '<div style="text-align:center;">' +
				'<a href="https://api.whatsapp.com/send?text='+encodeURIComponent($text)+'" target="_blank"><img src="/yohohk/img/whatsapp_icon.png" width="40px" height="40px" style="margin-right: 10px;"></a>'+
				'<a href="https://lineit.line.me/share/ui?text='+encodeURIComponent($text)+'&url='+encodeURIComponent($text)+'" target="_blank"><img src="/yohohk/img/line_icon.png" width="40px" height="40px" style="margin-right: 10px;"></a>'+
				'<a href="https://www.facebook.com/sharer.php?quote='+encodeURIComponent($text)+'&u='+encodeURIComponent($url)+'" target="_blank"><img src="/yohohk/img/fb_icon.png" width="40px" height="40px" style="margin-right: 10px; border-radius: 5px;"></a>'+
				'<a href="https://telegram.me/share/url?url='+encodeURIComponent($url)+'" target="_blank"><img src="/yohohk/img/telegram_icon.png" width="40px" height="40px" style="margin-right: 10px; padding: 3px; border: 1px solid #eeeeee; border-radius: 5px;"></a>' +
				'<a href="https://twitter.com/share?text='+encodeURIComponent($text)+'&url='+encodeURIComponent($url)+'" target="_blank"><img src="/yohohk/img/twitter_icon.png" width="40px" height="40px" style="margin-right: 10px;"></a>'+
				'<input class="form-control copy-input" style="margin-top: 10px;" type="text" value="'+$url+'"></div>';
			} else {
				$content += '<div style="text-align:center;"><a href="/user.php?act=login&redirect_to='+ encodeURIComponent($url) +'">'+YOHO._('global_login_popup_please_login', '請登入')+'</a></div>';
			}
			var dialog = YOHO.mobile.modal.create($title,$content);
			YOHO.mobile.modal.show(dialog)
		});
	},
	affiliateLinkTrigger: function() {
		$('.affiliatelink').click(function (evt) {
			evt.preventDefault();
			$('#affiliatelink').trigger('click');
		});
	},
	mobileCopy: function (el, msg) {
		var editable = el.contentEditable;
		var readOnly = el.readOnly;
		el.contentEditable = true;
		el.readOnly = false;
		if (navigator.userAgent.match(/ipad|ipod|iphone/i)) {
			range = document.createRange();
			range.selectNodeContents(el);
			selection = window.getSelection();
			selection.removeAllRanges();
			selection.addRange(range);
			el.setSelectionRange(0, 999999);
			el.contentEditable = editable;
			el.readOnly = readOnly;
			document.execCommand("Copy");
			selection.removeAllRanges();
		} else {
			el.select();
			document.execCommand("Copy");
			document.getSelection().removeAllRanges();
		}
		if (msg != "") {
			YOHO.mobile.modal.showSuccess(msg);
		}
	},
	affiliateCopy: function(){
		$(document).on('click', '.copy-input', function(e){
			YOHO.mobile.mobileCopy($(e.target).get(0), YOHO._('affiliate_copy', '已複製專用鏈結'));
		})
	},
	/* Products page sub title */
	topTag: function (){
		$('.goods-tab-box>ul>li>a').click(function() {
			$('.goods-tab-box a').removeClass('active');
			$(this).addClass('active');
			var $tag = $(this).data('tag');	
			$('#goods-details > .panel-default').hide();
			$('#goods-details > .'+$tag).show();
			if($tag == 'goods_all') {
				var content_height = $('#goods-details').height();
				$('.comment').css({display:"block"});
				$('.related-articles').css({display:"table"});	
				if(content_height<=2000){
					$('.goods-content-more-btn').css({display:"none"});
					$('#goods-details').css({"margin-bottom":"0"});
				}else{
					if(!$('div#goods-details').hasClass('show-part')) {
						$('div#goods-details').addClass('show-part');															
					}
					$('.goods_content div#goods-details.show-part').css({height:content_height/2});
					$('.goods-content-more-btn').show();
						
				}					
			}else if($tag == 'guarantee') {
				$('.comment').css({display:"none"});
				$('.related-articles').css({display:"none"});			
				if($('div#goods-details').hasClass('show-part')) {
					$('div#goods-details').removeClass('show-part');
					$('.goods-content-more-btn').hide();
					$('.goods_content div#goods-details').css({height:""});	
				}	
			}else {
				$('.comment').css({display:"block"});
				$('.related-articles').css({display:"none"});		
				if($('div#goods-details').hasClass('show-part')) {
					$('div#goods-details').removeClass('show-part');
					$('.goods-content-more-btn').hide();	
					$('.goods_content div#goods-details').css({height:""});							
				}
			}
			$("html, body").animate({scrollTop: $('.goods_content').offset().top - $('.navbar-fixed-top').height() - 5 }, 500);
			$('.goods-tab-box').animate({scrollLeft: $(this).offset().left}, 500);
		});
		$('.goods-content-more-btn').click(function(){
			$(this).hide();
			$('.goods_content div#goods-details').css({height:""});
			$('div#goods-details').removeClass('show-part');
		});
		if($('.scroll-content').length > 0 ){
			$('.scroll-content').each(function( index, value ) {
				var $parent = $(this).parent();
				var $originTop = $(this).offset().top;
				var $this = $(this);
				$(window).scroll(function() {
					if($this.data("target")){
						$originTop = $("."+$this.data("target")).offset().top;
					}
					if(($(window).scrollTop()+50 >=($parent.offset().top)))
					{
						$this.addClass('fixed');
						$this.css('top', ($(window).scrollTop() - $originTop + $('.navbar-fixed-top').height()));
					} else {
						$this.removeClass('fixed');
					}
					if(($this.offset().top + $this.outerHeight()) >= $('.yoho_footer').offset().top) {
						$this.css('top', ($('.yoho_footer').offset().top - ($this.outerHeight() + $originTop) - 10));
					}
				});
			});
		}
	},

	// 評價
	pinglun: function($goods_id){
		$("#onpinglun").click(function(){
			var $img = $('.carousel-inner .item.active>img').attr('src');
			var $title = $('.goods-head>h3').html();
			var _pinglun_box = null,

			html =  '<form action="comment.php" id="commentdrop" enctype="multipart/form-data">'+
						'<div class="writeComment"  id="comment-form">'+
							' <div class="goods_img"><img src="'+$img+'" width="100" height="100" alt="'+$title+'"></div>'+
							' <div class="goods_title"><h3>'+$title+'</h3></div>'+
							' <div class="star"><div id="comment_rating"></div></div>'+
							' <textarea id="rv-content" name="content"></textarea>'+
							' <input type="hidden" name="type" value="0" />'+
							' <input type="hidden" name="id" value="'+$goods_id+'" />'+
							' <input type="hidden" name="enabled_captcha" value="0" />'+
							' <input type="hidden" name="rank" value="5" />'+
							' <input type="hidden" name="lei" value="1" />'+
							' <input type="hidden" name="act" value="send_comment" />'+
							' <div class="uploads">'+
								'<input type="file" name="file[]" accept="image/*"/>'+
								'<div class="preview"></div>'+
								'<i class="fa fa-plus fa-2x"></i>'+
							'</div>'+
							'<div style="font-size: 12px;text-align: left;color: #afafaf;">'+YOHO._('comment_disclaimer', '')+'</div>'+
						'</div>'+
					'</form>';
			var modal = YOHO.mobile.modal.create('', html, YOHO.mobile.modal.createButton(YOHO._('comment_submit', '發表評價'), 'btn-primary comment_submit', false));
			YOHO.mobile.modal.show(modal);
			$("#comment_rating").rateYo({
				rating: 5,
				numStars: 5,
				precision: 2,
				minValue: 1,
				maxValue: 5,
				fullStar: true,
				"starSvg": '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" style="enable-background:new 0 0 40 40;" xml:space="preserve"><style type="text/css"></style>'+//
						'<path class="st0" d="M22.8,4.6l4.1,8.4c0.3,0.6,0.8,1,1.5,1.1l9.2,1.3c1.6,0.2,2.3,2.2,1.1,3.4L32,25.3c-0.5,0.5-0.7,1.1-0.6,1.7 l1.6,9.2c0.3,1.6-1.4,2.8-2.9,2.1l-8.3-4.3c-0.6-0.3-1.3-0.3-1.8,0l-8.3,4.3C10.4,39,8.7,37.8,9,36.2l1.6-9.2 c0.1-0.6-0.1-1.3-0.6-1.7l-6.7-6.5c-1.2-1.1-0.5-3.1,1.1-3.4l9.2-1.3c0.6-0.1,1.2-0.5,1.5-1.1l4.1-8.4C20,3.1,22,3.1,22.8,4.6z"/>'+
						'<path class="st1" d="M10.9,40c-0.7,0-1.4-0.2-2-0.7c-1.1-0.8-1.6-2.1-1.4-3.4l1.6-9.2c0-0.2,0-0.3-0.1-0.4l-6.7-6.5 c-1-0.9-1.3-2.3-0.9-3.6c0.4-1.3,1.5-2.2,2.8-2.4l9.2-1.3c0.2,0,0.3-0.1,0.4-0.3l4.1-8.4C18.5,2.7,19.7,2,21,2 c1.3,0,2.5,0.7,3.1,1.9l4.1,8.4c0.1,0.1,0.2,0.2,0.4,0.3l9.2,1.3c1.3,0.2,2.4,1.1,2.8,2.4c0.4,1.3,0.1,2.6-0.9,3.6l-6.7,6.5 c-0.1,0.1-0.2,0.3-0.1,0.4l1.6,9.2c0.2,1.3-0.3,2.6-1.4,3.4c-1.1,0.8-2.5,0.9-3.7,0.3l-8.3-4.3c-0.1-0.1-0.3-0.1-0.4,0l-8.3,4.3 C12,39.9,11.5,40,10.9,40z M21,32.2c0.6,0,1.1,0.1,1.6,0.4l8.3,4.3c0.2,0.1,0.4,0,0.5,0c0.1-0.1,0.2-0.2,0.2-0.5L30,27.3 c-0.2-1.1,0.2-2.3,1-3.1l6.7-6.5c0.2-0.2,0.2-0.4,0.1-0.5c0-0.1-0.1-0.3-0.4-0.3l-9.2-1.3c-1.1-0.2-2.1-0.9-2.6-1.9l-4.1-8.4 C21.3,5,21.1,5,21,5c-0.1,0-0.3,0-0.4,0.3l-4.1,8.4c-0.5,1-1.5,1.7-2.6,1.9l-9.2,1.3c-0.3,0-0.3,0.2-0.4,0.3c0,0.1-0.1,0.3,0.1,0.5 l6.7,6.5c0.8,0.8,1.2,1.9,1,3.1l-1.6,9.2c0,0.3,0.1,0.4,0.2,0.5c0.1,0.1,0.3,0.2,0.5,0l8.3-4.3C19.9,32.3,20.4,32.2,21,32.2z"/>'+
					"</svg>",
				starWidth: "30px",
				spacing: "15px",
				ratedFill: "#FEDA00",
				normalFill: "#E7E3E1",
				onInit: function () {
				},
				onSet: function (rating, rateYoInstance) {
				  $('input[name="rank"]').val(rating);
				}
			});
			$('body').on('change', 'input[type="file"]', function() {
				var input = this;
				var reader = new FileReader();
				if (input.files && input.files[0]) {
					var reader = new FileReader();
					var parent = $(input).closest('.uploads');
					if(parent.closest('.comment_content').children('.uploads').length < 5) {
						parent.after(parent.clone());
						parent.next().find('input[type="file"]').val('');
					}

					reader.onload = function(e) {
						$(input).next('.preview').html('<img src="'+e.target.result+'">');
					}

					reader.readAsDataURL(input.files[0]);
				}
			});
			$(document).ready(function() {

				// Detect ios 11_x_x affected
				// NEED TO BE UPDATED if new versions are affected
				var ua = navigator.userAgent,
				iOS = /iPad|iPhone|iPod/.test(ua),
				iOS11 = /OS 11_0|OS 11_1|OS 11_2/.test(ua);
				// ios 11 bug caret position
				if ( iOS && iOS11 ) {
					// Add CSS class to body
					$("body").addClass("iosBugFixCaret");
				}
			});
			// find form parent add id:
			$('#comment-form').closest('.modal-content').attr('id', 'popupcomment');
			$('.comment_submit').click(function(e){
				// e.preventDefault();
				var formData = new FormData();
				$.each($('#commentdrop').find("input[type='file']"), function(i, tag) {
					$.each($(tag)[0].files, function(i, file) {
						formData.append(tag.name, file);
					});
				});
				var params = $('#commentdrop').serializeArray();
				$.each(params, function (i, val) {
					formData.append(val.name, val.value);
				});
				$(this).prop("disabled", true);
				$.ajax({
					type: "POST",
					url: '/comment.php',
					dataType: 'JSON',
					processData: false,
					contentType: false,
					data: formData, // serializes the form's elements.
					success: function(data)
					{
						comment_success(data);
					}
				});
			  });
		});
		$("#goods_to_review").click(function(){
			$('.goods-tab-box a.comment_header').trigger('click');
		});

		function comment_success(response) {
			if (response.status == 1)
			{
				YOHO.mobile.modal.showSuccess(YOHO._('comment_success', '評價已提交成功，感謝您的大力支持。'));
				setTimeout(function(){ location.reload(); }, 3000);
			}
			// else if (response.status == 2)
			// {
			// 	YOHO.mobile.modal.showError('驗證碼輸入錯誤或者沒有輸入驗證碼！');
			// }
			else if (response.status == 3)
			{
				YOHO.mobile.modal.showError(YOHO._('comment_error_frequently', '評價過於頻繁，請等待30秒之後重新提交！'));
			}
			else
			{
				YOHO.mobile.modal.showError(response.message);
			}
		};

		if($("#goods_total_rating").length > 0) {
			var $rating = $("#goods_total_rating").data('rate');
			$("#goods_total_rating").rateYo({
				rating: $rating,
				readOnly: true,
				numStars: 5,
				precision: 2,
				minValue: 1,
				maxValue: 5,
				"starSvg":
					'<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" style="enable-background:new 0 0 40 40;" xml:space="preserve"><style type="text/css"></style>'+//
						'<path class="st0" d="M22.8,4.6l4.1,8.4c0.3,0.6,0.8,1,1.5,1.1l9.2,1.3c1.6,0.2,2.3,2.2,1.1,3.4L32,25.3c-0.5,0.5-0.7,1.1-0.6,1.7 l1.6,9.2c0.3,1.6-1.4,2.8-2.9,2.1l-8.3-4.3c-0.6-0.3-1.3-0.3-1.8,0l-8.3,4.3C10.4,39,8.7,37.8,9,36.2l1.6-9.2 c0.1-0.6-0.1-1.3-0.6-1.7l-6.7-6.5c-1.2-1.1-0.5-3.1,1.1-3.4l9.2-1.3c0.6-0.1,1.2-0.5,1.5-1.1l4.1-8.4C20,3.1,22,3.1,22.8,4.6z"/>'+
						'<path class="st1" d="M10.9,40c-0.7,0-1.4-0.2-2-0.7c-1.1-0.8-1.6-2.1-1.4-3.4l1.6-9.2c0-0.2,0-0.3-0.1-0.4l-6.7-6.5 c-1-0.9-1.3-2.3-0.9-3.6c0.4-1.3,1.5-2.2,2.8-2.4l9.2-1.3c0.2,0,0.3-0.1,0.4-0.3l4.1-8.4C18.5,2.7,19.7,2,21,2 c1.3,0,2.5,0.7,3.1,1.9l4.1,8.4c0.1,0.1,0.2,0.2,0.4,0.3l9.2,1.3c1.3,0.2,2.4,1.1,2.8,2.4c0.4,1.3,0.1,2.6-0.9,3.6l-6.7,6.5 c-0.1,0.1-0.2,0.3-0.1,0.4l1.6,9.2c0.2,1.3-0.3,2.6-1.4,3.4c-1.1,0.8-2.5,0.9-3.7,0.3l-8.3-4.3c-0.1-0.1-0.3-0.1-0.4,0l-8.3,4.3 C12,39.9,11.5,40,10.9,40z M21,32.2c0.6,0,1.1,0.1,1.6,0.4l8.3,4.3c0.2,0.1,0.4,0,0.5,0c0.1-0.1,0.2-0.2,0.2-0.5L30,27.3 c-0.2-1.1,0.2-2.3,1-3.1l6.7-6.5c0.2-0.2,0.2-0.4,0.1-0.5c0-0.1-0.1-0.3-0.4-0.3l-9.2-1.3c-1.1-0.2-2.1-0.9-2.6-1.9l-4.1-8.4 C21.3,5,21.1,5,21,5c-0.1,0-0.3,0-0.4,0.3l-4.1,8.4c-0.5,1-1.5,1.7-2.6,1.9l-9.2,1.3c-0.3,0-0.3,0.2-0.4,0.3c0,0.1-0.1,0.3,0.1,0.5 l6.7,6.5c0.8,0.8,1.2,1.9,1,3.1l-1.6,9.2c0,0.3,0.1,0.4,0.2,0.5c0.1,0.1,0.3,0.2,0.5,0l8.3-4.3C19.9,32.3,20.4,32.2,21,32.2z"/>'+
					"</svg>",
				starWidth: "20px",
				spacing: "8px",
				ratedFill: "#FEDA00",
				normalFill: "#E7E3E1",
			});
		}
	},

	// 評價列表圖片展示
	ping_list: function(top) {
		var $list = $('#ping-list');
		loadPageRating();
		$list.on('click','li',function() {
			var _this = $(this),
			_src = _this.data('src'),
			$view = _this.parent('ul').next('.ping-photo-viewer');
			if(_src == undefined)
			{
				return false;
			}
			if(_this.hasClass('current'))
			{
				_this.removeClass('current');
				$view.hide().off();
			}
			else
			{
				_this.addClass('current').siblings('li').removeClass('current');
				$view.html('<img src="'+_src+'" />').show().off().on('click', function() {
					_this.trigger('click');
				});;
			}
		});

		// 分页浏览
		$list.on('click','a.step',function() {
			var $goods_id = $('#comment_page').data('id');
			var _page = $(this).data('page');
			window.scrollTo(0,$('#pjxqitem').offset().top);
			$list.html('<div style="padding:40px; text-align:center;"><p class="nala-load" style="height:30px;">&nbsp;</p></div>');
			$.ajax({
				url: '/comment.php?act=gotopage',
				data: {'id': $goods_id,'page':_page,'lei':1},
				success: function(data) {
					$list.html($(data).find('#ping-list').html());
					loadPageRating();
				},
				error: function(xhr) {
					YOHO.mobile.modal.showError(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
					//$list.html('<div style="padding:40px; text-align:center;">評價載入失敗，請 <a class="step red" href="javascript:;">重試</a></div>');
				}
			});
		});

		 function loadPageRating() {
			$(".displayrating").each( function() {
				var rating = $(this).data('rate');
				$(this).rateYo(
					{
					  rating: rating,
					  numStars: 5,
					  precision: 2,
					  minValue: 1,
					  maxValue: 5,
					  readOnly: true,
					  "starSvg": '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" style="enable-background:new 0 0 40 40;" xml:space="preserve"><style type="text/css"></style>'+//
							  '<path class="st0" d="M22.8,4.6l4.1,8.4c0.3,0.6,0.8,1,1.5,1.1l9.2,1.3c1.6,0.2,2.3,2.2,1.1,3.4L32,25.3c-0.5,0.5-0.7,1.1-0.6,1.7 l1.6,9.2c0.3,1.6-1.4,2.8-2.9,2.1l-8.3-4.3c-0.6-0.3-1.3-0.3-1.8,0l-8.3,4.3C10.4,39,8.7,37.8,9,36.2l1.6-9.2 c0.1-0.6-0.1-1.3-0.6-1.7l-6.7-6.5c-1.2-1.1-0.5-3.1,1.1-3.4l9.2-1.3c0.6-0.1,1.2-0.5,1.5-1.1l4.1-8.4C20,3.1,22,3.1,22.8,4.6z"/>'+
							  '<path class="st1" d="M10.9,40c-0.7,0-1.4-0.2-2-0.7c-1.1-0.8-1.6-2.1-1.4-3.4l1.6-9.2c0-0.2,0-0.3-0.1-0.4l-6.7-6.5 c-1-0.9-1.3-2.3-0.9-3.6c0.4-1.3,1.5-2.2,2.8-2.4l9.2-1.3c0.2,0,0.3-0.1,0.4-0.3l4.1-8.4C18.5,2.7,19.7,2,21,2 c1.3,0,2.5,0.7,3.1,1.9l4.1,8.4c0.1,0.1,0.2,0.2,0.4,0.3l9.2,1.3c1.3,0.2,2.4,1.1,2.8,2.4c0.4,1.3,0.1,2.6-0.9,3.6l-6.7,6.5 c-0.1,0.1-0.2,0.3-0.1,0.4l1.6,9.2c0.2,1.3-0.3,2.6-1.4,3.4c-1.1,0.8-2.5,0.9-3.7,0.3l-8.3-4.3c-0.1-0.1-0.3-0.1-0.4,0l-8.3,4.3 C12,39.9,11.5,40,10.9,40z M21,32.2c0.6,0,1.1,0.1,1.6,0.4l8.3,4.3c0.2,0.1,0.4,0,0.5,0c0.1-0.1,0.2-0.2,0.2-0.5L30,27.3 c-0.2-1.1,0.2-2.3,1-3.1l6.7-6.5c0.2-0.2,0.2-0.4,0.1-0.5c0-0.1-0.1-0.3-0.4-0.3l-9.2-1.3c-1.1-0.2-2.1-0.9-2.6-1.9l-4.1-8.4 C21.3,5,21.1,5,21,5c-0.1,0-0.3,0-0.4,0.3l-4.1,8.4c-0.5,1-1.5,1.7-2.6,1.9l-9.2,1.3c-0.3,0-0.3,0.2-0.4,0.3c0,0.1-0.1,0.3,0.1,0.5 l6.7,6.5c0.8,0.8,1.2,1.9,1,3.1l-1.6,9.2c0,0.3,0.1,0.4,0.2,0.5c0.1,0.1,0.3,0.2,0.5,0l8.3-4.3C19.9,32.3,20.4,32.2,21,32.2z"/>'+
						  "</svg>",
					  starWidth: "20px",
					  spacing: "8px",
					  ratedFill: "#FEDA00",
					  normalFill: "#E7E3E1"
					}
				);
			});
		};
	},

	flashdealCartCountDown: function() {
		if(Object.keys(YOHO.mobile.flashdealCartTimer).length > 0 && YOHO.mobile.flashdealCartTimer.expired != '') {
			var getTs = function () { return Math.floor((new Date()).getTime() / 1000); };
			$div = $('<div class="flashdeal-cart-countdown"></div>');
			var $countdown = $div;
			var serverTs = parseInt(YOHO.mobile.flashdealCartTimer.now_time) || getTs();
			var tsDiff = serverTs - getTs() + 2; // add 2 seconds for network delay
			var endTs = parseInt(YOHO.mobile.flashdealCartTimer.expired);
			var $parent = $countdown.parent();
			var updateCountdown = function () {
				if(YOHO.mobile.flashdealCartTimer.expired == '') {
					clearTimeout(updateCountdown);
					$('.flashdeal-cart-countdown').remove();
					return;
				}
				var remaining_time = endTs - (getTs() + tsDiff);
				//var remaining_time = 7100;
				if (remaining_time <= 0) {
					YOHO.mobile.modal.showInfo(YOHO._('flashdeal_cart_timeout', '您的閃購超過時間, 如有需要請重新加入購物車！'));
					setTimeout(function(){location.reload()}, 5000);
				}
				else
				{
					var percent02d = function (input) {
						return (String(input).length < 2 ? '0' : '') + input;
					};
					var date_str = '';
					if (remaining_time > 86400)
					{
						date_str = date_str + '<span class="flashdeal_cart_day">'+ percent02d(Math.floor(remaining_time / 86400)) + YOHO._('goods_promote_day', '天') + '</span>';
						remaining_time = remaining_time % 86400;
					}
					if (remaining_time > 3600)
					{
						date_str = date_str + '<span class="flashdeal_cart_hour">'+  percent02d(Math.floor(remaining_time / 3600))　+ '</span>：';
						remaining_time = remaining_time % 3600;
					}
					if (remaining_time > 60)
					{
						date_str = date_str + '<span class="flashdeal_cart_miniute">'+ percent02d(Math.floor(remaining_time / 60)) + '</span>：';
						remaining_time = remaining_time % 60;
					} else {
						date_str = date_str + '<span class="flashdeal_cart_miniute">00</span>：';
					}
					date_str = date_str + '<span class="flashdeal_cart_second">'+ percent02d(remaining_time) + '</span>';
					//
					$countdown.html(sprintf(YOHO._('global_flashdeal_bottombar_timeout', '您購物車中的閃購將於<span class="flashdeal_cart_timer"> %s </span>過期'), date_str));
					if($('body .navbar-fixed-bottom').length > 0) {
						$($countdown).css('bottom', '50px');
					}
					$(".yoho_footer").append($countdown);
					setTimeout(updateCountdown, 1000);
				}
			};
			updateCountdown();

		} else {
			$('.flashdeal-cart-countdown').remove();
		}
	},

	applyRankProgram: function (require_proof) {
		$(document).on("click", ".apply_program", function (e) {
			$target = $(e.target).is('a') ? $(e.target) : $(e.target).parents('a.apply_program');
			var pid = $target.data("pid");
			if (typeof pid !== "undefined") {
				$.ajax({
					url: '/ajax/user_rank_program.php',
					dataType: 'json',
					data: {
						pid: pid,
						act: "get_program"
					},
					success: function (res) {
						if (res.error == 1) {
							YOHO.mobile.modal.showError(YOHO._(res.msg, res.msg));
						} else {
							var data = res.data;

							$(document).off('change', 'input[type=file][name=rank_program_file]');
							$(document).off('click', '#apply_rank_submit');

							var secret = CryptoJS.lib.WordArray.random(128 / 8).toString(CryptoJS.enc.Hex);
							$(document).on('change', 'input[type=file][name=rank_program_file]', function () {
								$("#apply_rank_submit").addClass("disabled");
								var input = this;
								var reader = new FileReader();
								if (input.files && input.files[0]) {
									var reader = new FileReader();

									reader.onload = function (e) {
										$(input).next('.preview').html('<img src="' + e.target.result + '">');
										var cryptKey = CryptoJS.enc.Hex.parse(data.secret);
										var cipherText = CryptoJS.AES.encrypt(e.target.result, cryptKey, {
											iv: CryptoJS.enc.Hex.parse(secret),
											mode: CryptoJS.mode.CBC,
											padding: CryptoJS.pad.Pkcs7
										})

										$.ajax({
											url: '/ajax/user_rank_program.php',
											method: 'post',
											dataType: 'json',
											data: {
												pid: pid,
												act: "secret_file",
												secret: CryptoJS.enc.Base64.stringify(cipherText.ciphertext)
											},
											success: function (res) {
												if (res.error == 1) {
													YOHO.mobile.modal.showError(YOHO._(res.msg, res.msg));
												} else {
													//reader.readAsDataURL(input.files[0]);
													$("#apply_rank_submit").removeClass("disabled");
												}
											},
											error: function (xhr) {
												YOHO.mobile.modal.showError(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '(' + xhr.status + ')');
											}
										});
									}
									reader.readAsDataURL(input.files[0]);
								}
							})
							var id = "apply_program_" + data.program.program_id;
							$(document).on('click', '#apply_rank_submit', function () {
								var input = $('input[type=file][name=rank_program_file]');
								var _email = $.trim($('input[name=verify_email]').val());
								if (!_email) {
									YOHO.mobile.modal.showError(sprintf(YOHO._('account_please_fill_in', '請輸入%s'), YOHO._('account_email', '電郵地址')));
									return;
								}
								var email_ok = YOHO.check.isEmail(_email);
								if (!email_ok) {
									YOHO.mobile.modal.showError(sprintf(
										YOHO._('account_format_invalid', '%s格式有誤，再修改一下哦！'),
										YOHO._('account_email', '電郵地址')
									));
									return;
								}
								if (require_proof && (!input[0].files || !input[0].files[0])) {
									YOHO.mobile.modal.showError(YOHO._("user_rank_program_proof", "上傳證明文件"));
									return;
								} else
								{
									$(this).addClass("disabled");
									$(this).val(YOHO._('account_submitting', '提交中'));
									$.ajax({
										url: '/ajax/user_rank_program.php',
										method: 'post',
										dataType: 'json',
										data: {
											pid: $("input[name=rank_program_id]").val(),
											uid: $("input[name=rank_program_user]").val(),
											secret: secret,
											verify_email: _email,
											act: "apply_program"
										},
										success: function (res) {
											if (res.error == 1) {
												YOHO.mobile.modal.showError(YOHO._(res.msg, res.msg));
											} else {
												YOHO.mobile.modal.showInfo(YOHO._(res.msg, res.msg));
												setTimeout(function () {
													location.reload();
												}, 2000)
											}
										}
									})
								}
							})
							var html = "<div class='rank_program_title'>" + YOHO._("user_rank_program_name", "計劃名稱") + "</div>" +
								"<div style='margin-bottom: 15px;'>" + data.program.program_name + "</div>" +
								"<div class='rank_program_title'>" + YOHO._("user_rank_program_username", "申請帳號") + "</div>" +
								"<div style='margin-bottom: 15px;'>" + data.user_name + "</div>" +
								"<div class='rank_program_title'><label>" + YOHO._("user_rank_program_email", "驗證電郵") + "</label></div>" +
								"<div style='margin-bottom: 15px;'><input type='email' name='verify_email' class='form-control verify_email'></div>" +
								(require_proof ? "<div class='rank_program_title'>" + YOHO._("user_rank_program_proof", "上傳證明文件") + "</div>" +
								"<div style='display: inline-block; width: 100%; margin-bottom: 20px;'>" +
								"<div class='uploads'>" +
								"<input type='file' name='rank_program_file' accept='image/*'>" +
								"<div class='preview'></div>" +
								"<i class='fa fa-plus fa-2x'></i>" +
								"</div>" +
								"</div>" : "") +
								"<input type='hidden' name='rank_program_file_data' value=''>" +
								"<input type='hidden' name='rank_program_id' value='" + data.program.program_id + "'>" +
								// "<input type='hidden' name='rank_program_user' value='" + YOHO.userInfo.uid + "'>" +
								"<input type='button' value='" + YOHO._("user_rank_program_submit", "提交") + "' class='btn-primary btn action-button' id='apply_rank_submit' style='line-height: 25px;'>";
							var modal = YOHO.mobile.modal.create(YOHO._("user_rank_program_apply", "申請") + " " + data.program.program_name, html);
							YOHO.mobile.modal.show(modal);
						}
					}
				})
			}
		})
	},
	flow: function () {
		$(window).scroll(function() {
			if($(window).scrollTop() > 50)
			{
				$('.flow-header').css('top', '0px');
				$('.navbar-fixed-top').hide();
			} else {
				$('.flow-header').css('top', "");
				$('.navbar-fixed-top').show();
			}

		});
	},

	goodsColumns: function () {
        $(".goodsColRating").show();
	},

	ulSelect: function () {
		$.fn.ulSelect = function () {
			var ul = $(this);

			if (!ul.hasClass('zg-ul-select')) {
				ul.addClass('zg-ul-select');
			}
			// SVG arrow
			var arrow = '<svg id="ul-arrow" xmlns="http://www.w3.org/2000/svg" version="1.1" width="20" height="20" viewBox="0 0 32 32"><line stroke-width="1" stroke="#449FDB" opacity=""/><polygon class="st0" points="15,27.163 27.89,4.837 2.11,4.837 "/></svg>';
			$('li:first-of-type', this).addClass('active').append(arrow);
			$(this).on('click', 'li', function (event) {
				// Remove div#selected if it exists
				if ($('#selected--zg-ul-select').length) {
					$('#selected--zg-ul-select').remove();
				}
				ul.before('<div id="selected--zg-ul-select">');
				var selected = $('#selected--zg-ul-select');
				$('li #ul-arrow', ul).remove();
				ul.toggleClass('active');
				// Remove active class from any <li> that has it...
				ul.children().removeClass('active');
				// And add the class to the <li> that gets clicked
				$(this).toggleClass('active');
				var selectedText = $(this).text();
				if (ul.hasClass('active')) {
					selected.text(selectedText).addClass('active').append(arrow);
				} else {
					selected.text('').removeClass('active');
					$('li.active', ul).append(arrow);
				}
			});
			// Close the faux select menu when clicking outside it
			$(document).on('click', function (event) {
				if ($('ul.zg-ul-select').length) {
					if (!$('ul.zg-ul-select').has(event.target).length == 0) {
						return;
					} else {
						$('ul.zg-ul-select').removeClass('active');
						$('#selected--zg-ul-select').removeClass('active').text('');
						$('#ul-arrow').remove();
						$('ul.zg-ul-select li.active').append(arrow);
					}
				}
			});
			ul.show();
		}
	},

	treasureNotify: function(init, info) {
		var percent02d = function (input) {
			return (String(input).length < 2 ? '0' : '') + input;
		};
		$.ajax({
			url: '/ajax/treasure.php?treasure_action=get_info',
			dataType: 'json',
			success: function(info) {
				if (info.step != 'fail') {
					$notify2 = $("<div class='notify2 " + info.step + "'></div>");
					$base_icon = $("<img class='treasure_base_icon' src='/yohohk/img/treasure/treasure.gif'>");
					$base_icon.click(function() {
						$(".notify2").removeClass("hidden")
					})
					var head_html = "<div class='notify_head'>" +
										"<div class='notify_title'>提示！</div>" +
										"<div class='notify_step' data-s='found'>1</div>" +
										"<div class='notify_step' data-s='unlock'>2</div>" +
										"<div class='notify_step' data-s='prize'>3</div>" +
										"<div class='notify_step' data-s='open'>4</div>" +
									"</div>"
					$notify_head = $(head_html);
					var body_html = "<div class='notify_body'>" +
										"<div class='notify_image'>" +
											"<img src='/yohohk/img/treasure/" + info.step + ".png'>" +
										"</div>" +
									"</div>";
					$notify_body = $(body_html);
					$notify_message = $("<div class='notify_message'></div>");
					if (info.step == "found") {
						$notify_unlock = $("<div class='notify_btn red notify_unlock'>" + YOHO._("treasure_action_unlock", "開始解鎖") + "</div>");
						$notify_treasure = $("<div class='notify_btn'>" + YOHO._("treasure_action_detail", "活動詳情") + "</div>");
						$notify_treasure.click(function() {
							window.location = "https://www.yohohongkong.com/article.php?id=349"
						})
						$notify_unlock.click(function() {
							$.ajax({
								url: '/ajax/treasure.php?treasure_action=unlock_treasure',
								dataType: 'json',
								success: function(result) {
									if (result.status == 0) {
										alert(result.message);
										location.reload();
									} else {
										YOHO.mobile.treasureNotify();
									}
								}
							})
						})
						$notify_message.append("<h4>" + YOHO._("treasure_step_found", "成功捕捉神秘獎賞！") + "</h4>");
						$notify_message.append($notify_unlock);
						$notify_message.append($notify_treasure);
					} else if (info.step == "unlock") {
						var html = "";
						if (parseInt(info.remain_time) > 0) {
							var remaining_time = "";
							if (info.remain_time >= 60) {
								remaining_time = percent02d(Math.floor(info.remain_time / 60));
							} else {
								remaining_time = '00';
							}
							remaining_time += ":" + percent02d(info.remain_time % 60);
							html = "<h1 id='treasureCountDown' data-t='" + info.remain_time + "'>" + remaining_time + "</h1>";
							setTimeout(YOHO.mobile.treasureCountDown, 1000);
						}
						$notify_message.append(html);
						$notify_message.append("<h4 style='text-align: left'>" + YOHO._("treasure_step_unlock", "更多神秘獎賞，於完成解鎖後等你捕捉！") + "</h4>");
					} else if (info.step == "no_prize") {
						$notify_sorry = $("<div class='notify_btn orange notify_sorry'>" + YOHO._("treasure_action_fail_" + (Math.floor(Math.random() * 3) + 1) , "再次展開追捕行動！") + "</div>");
						$notify_sorry.click(function() {
							$.ajax({
								url: '/ajax/treasure.php?treasure_action=confirm',
								dataType: 'json',
								success: function(result) {
									if (result.status == 1) {
										$(".notify2").remove();
									} else {
										location.reload();
									}
								}
							})
						})
						$notify_message.append("<h4>" + YOHO._("treasure_step_prize_none", "未能解鎖成功！") + "</h4>");
						$notify_message.append($notify_sorry);
					} else if (info.step == "prize") {
						$notify_open = $("<div class='notify_btn orange notify_open'>" + YOHO._("treasure_action_open", "確認領獎") + "</div>");
						$notify_open.click(function(){
							$.ajax({
								url: '/ajax/treasure.php?treasure_action=open_treasure',
								dataType: 'json',
								success: function(result) {
									if (result.status == 0) {
										alert(result.message);
										location.reload();
									} else {
										var html = "";
										if (result.step != 'fail') {
											if (result.reward != 0) {
												html += "<div class='treasure_type'><div class='treasure_type_" + (result.reward == 1 ? ("coupon'>" + YOHO._("treasure_coupon", "優惠券")) : ("prize'>" + YOHO._("treasure_prize", "免費"))) + "</div><div class='treasure_type_name'>" + result.treasure_name + "</div></div>" +
														"<div style='margin-top: 10px;'>" +
															"<div class='treasure_tnc' data-msg='" +
																sprintf(YOHO._("treasure_step_detail_prize", "<li>請立即將優惠碼紀錄，優惠碼於任何情況不獲補發。</li>")) +
																(result.treasure_tnc == "" ? "" : result.treasure_tnc) +
																sprintf(YOHO._("treasure_step_detail_validate", "<li>優惠券有效期： %s</li>"), result.coupon_date) +
																"'><u>" + YOHO._("treasure_tnc", "條款") +
															"</u></div>" +
															"<span class='treasure_tnc_remark'>請立即紀錄！</span>" +
															"<input title='點擊複製' class='treasure_coupon' value='" + result.coupon_code + "' readonly>" +
														"</div>";
											}
											$(".notify_step").removeClass("active");
											$(".notify_step[data-s=open]").addClass("active");
											$notify_message.html(html);
										} else {
											if (result.status == 1) {
												alert("你已經領獎了！");
											}
											$(".notify2").remove();
										}
									}
								}
							})
						})
						$notify_message.append("<h5>" + YOHO._("treasure_step_prize", "成功捕捉") + "</h5><h4>" + info.treasure.treasure_name + "</h4>");
						if (info.treasure.treasure_desc != "") {
							$notify_message.append("<p>" + info.treasure.treasure_desc + "</p>");
						}
						$notify_message.append($notify_open);
					}
					$notify_body.append($notify_message);
					$notify_close = $("<span class='fa fa-chevron-down notify_close'></span>");
					$notify_close.click(function(){
						$notify2.addClass("hidden");
					})
					$notify_head.append($notify_close);
					$notify2.append($notify_head);
					$notify2.append($notify_body);
					$(".notify2").remove();
					$('body').append($notify2);
					$('body').append($base_icon);
					$(".notify_step[data-s=" + info.step + "]").addClass("active");
					if (info.step == "no_prize") {
						$(".notify_step[data-s=open]").addClass("active");
					}
					$notify2.fadeIn();
					if (typeof init !== "undefined" && init) {
						$(document).on("click", ".treasure_coupon", function(e) {
							YOHO.mobile.mobileCopy($(e.target).get(0), YOHO._("global_copied", "已複製"));
						})
						$(document).on("click", ".treasure_tnc", function(e) {
							$target = $(e.target).hasClass("treasure_tnc") ? $(e.target) : $(e.target).parents(".treasure_tnc");
							var html = "<div class='treasure_tnc_detail'>" +
											"<ul>" +
												$target.data("msg") +
											"</ul>" +
										"</div>";
							YOHO.mobile.modal.showInfo(YOHO._("treasure_tnc", "條款"), html);
							$(".modal").find(".col-xs-2").remove();
							$(".modal").find(".col-xs-10").addClass("col-xs-12").removeClass("col-xs-10");
						})
						if (info.step == "found" || info.step == "unlock") {
							$notify_close.click();
						}
					}
					setTimeout(function() {
						$notify2.remove()
					}, info.close_time * 1000)
				} else {
					$(".notify2").remove();
				}
			}
		})
	},

	treasureCountDown: function() {
		var percent02d = function (input) {
			return (String(input).length < 2 ? '0' : '') + input;
		};
		if ($("#treasureCountDown").length > 0 && parseInt($("#treasureCountDown").data('t')) > 1) {
			var remain_time = parseInt($("#treasureCountDown").data('t')) - 1;
			$("#treasureCountDown").data('t', remain_time);
			var remaining_time = "";
			if (remain_time >= 60) {
				remaining_time = percent02d(Math.floor(remain_time / 60));
			} else {
				remaining_time = '00';
			}
			remaining_time += ":" + percent02d(remain_time % 60);
			$("#treasureCountDown").text(remaining_time);
			if (remain_time > 0) {
				setTimeout(YOHO.mobile.treasureCountDown, 1000);
			}
		} else {
			YOHO.mobile.treasureNotify();
		}
	},

	treasureOverallNotify: function(notice) {
		if (typeof notice !== "undefined" && notice !== "") {
			$notify = $("<div class='notify'><div class='notify_head'><div class='notify_title'>聖誕勁賞要</div><div class='notify_message'><div>" + notice + "</div></div></div></div>");
			$notify.click(function() {
				window.location = "https://www.yohohongkong.com/article.php?id=349"
			})
			$('body').append($notify);
			setTimeout(function(){
				$notify.fadeIn('slow');
			}, 3000)
			setTimeout(function(){
				$notify.fadeOut('fast');
			}, 10000)
		}
	},

	orderReceiptUpload: function() {
		$("form#pay_proof_form").find('input[type=file][name=pay_proof]').on('change', function () {
			var input = this;
			var reader = new FileReader();
			$("div#pay_submit").addClass("disabled or_btn_gray");
			if (input.files && input.files[0]) {
				$("div#pay_submit").removeClass("disabled or_btn_gray");
				$('select[name=pay_account]').trigger("change");
				var reader = new FileReader();
		
				reader.onload = function (e) {
					if (e.target.result.indexOf("image/") !== -1) {
						$(input).next('.preview').html('<img src="' + e.target.result + '">');
					} else if (e.target.result.indexOf("application/pdf") !== -1) {
						$(input).next('.preview').html('<div class="unable_preview">' + input.files[0].name + '</div>');
					} else {
						$(input).next('.preview').html('');
						$("div#pay_submit").addClass("disabled or_btn_gray");
						$('input[type=file][name=pay_proof]').val('');
						alert(YOHO._('order_receipt_proof_accept', '只接受相片或pdf'))
					}
				}
				reader.readAsDataURL(input.files[0]);
			} else {
				$(input).next('.preview').html('');
			}
		})
		
		$("form#pay_proof_form").find('select[name=pay_account]').on('change', function() {
			if ($(this).val() == "" || $('input[type=file][name=pay_proof]').get(0).files.length == 0) {
				$("div#pay_submit").addClass("disabled or_btn_gray");
			} else {
				$("div#pay_submit").removeClass("disabled or_btn_gray");
			}
		})
		
		$("form#pay_proof_form").find('div#pay_submit').on('click', function() {
			if (!$(this).hasClass('disabled')) {
				$("#pay_proof_form").submit();
			}
		})
	},

	phonenumber: function() {
		return {
			init: function() {
				$(document).on('click', '#play_now', function() {
					if (!YOHO.mobile.logged_in) {
						window.location = '/user.php?act=login';
					} else {
						window.location = "/phonenumber-verify";
					}
				})
				.on('click', '.success#verify_btn', function() {
					if ($('#number').val().length == 8 && $('#number').val().indexOf('6') !== -1) {
						var modal = YOHO.mobile.modal.showQuestion(sprintf(YOHO._('user_send_verify_mobile', '發送驗證碼至 %s'), '+852-' + $('#number').val()), undefined, function() {
							$('#number').attr('disabled', true)
							$.ajax({
								url: 'phonenumber',
								dataType: 'json',
								data: {
									act: 'send',
									phone: $('#number').val(),
								},
								success: function (res) {
									YOHO.mobile.modal.hide(modal);
									if (res.error == '0') {
										if (res.msg != "") {
											YOHO.mobile.modal.showInfo(res.msg)
										}
										YOHO.mobile.phonenumber().blockResend();
										YOHO.mobile.phonenumber().blockReverify();
									} else {
										if (res.error == '2') {
											window.location = "/phonenumber-confirm";
										}
										YOHO.mobile.modal.showError(res.msg)
										$('#number').attr('disabled', false)
									}
								},
								error: function (xhr) {
									YOHO.mobile.modal.showError(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
								}
							})
						})
					}
				})
				.on('click', '.success#validate_btn', function() {
					if ($('#verify_code').val() != "") {
						$('#verify_code').attr('disabled', true)
						$('#verify_btn, #validate_btn').removeClass('success');
						$.ajax({
							url: 'phonenumber',
							dataType: 'json',
							data: {
								act: 'validate',
								code: $('#verify_code').val(),
							},
							success: function (res) {
								if (res.error == '0') {
									window.location = "/phonenumber-confirm";
								} else {
									if (res.error == '2') {
										window.location = "/phonenumber-confirm";
									}
									YOHO.mobile.modal.showError(res.msg)
									blockReverify();
									$('#verify_code').attr('disabled', false)
									if ($("#verify_countdown").length == 0) {
										$('#verify_btn').addClass('success');
									}
								}
							},
							error: function (xhr) {
								YOHO.mobile.modal.showError(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
							}
						})
					}
				})
				.on('click', '#shop_btn', function() {
					window.location = "/"
				})
				.on('click', '#tnc_btn', function() {
					window.location = '/yohohk/img/phonenumber_tnc.png'
				})
				.on('click', '#coupon_code input.code', function(e) {
					YOHO.mobile.mobileCopy($(e.target).get(0), '已複製優惠碼')
				})
				$(document).on('keyup', '#number', function(e) {
					$('#number').val($('#number').val().substring(0, 8).replace(/\D/g, ''));
					
					$('#verify_btn').removeClass('success');
					$('#error_message').html("")
					if ($('#number').val().length == 8) {
						if ($('#number').val().indexOf('6') !== -1) {
							$('#verify_btn').addClass('success');
						} else {
							$('#error_message').html("- " + sprintf(YOHO._('event_require_text', '必須包含 %s 字'), 6))
						}
					} else {
						$('#error_message').html("- " + sprintf(YOHO._('event_require_length', '請輸入 %s 位數字'), 8))
					}
				})
			},
			blockResend: function(time) {
				time = typeof time === "undefined" ? 180 : time;
				$("#verify_code").show();
				$("#verify_btn").removeClass('success');
				$("#verify_btn").append("<span id='verify_countdown' class='countdown' data-t='" + time + "'>(" + time + ")</span>");
				var verify_timer = setInterval(function(){
					var remain = parseInt($("#verify_countdown").data('t'));
					if (remain > 0) {
						remain--;
						$("#verify_countdown").data('t', remain);
						$("#verify_countdown").html("(" + remain + ")");
					} else {
						$("#verify_btn").addClass('success');
						$("#verify_countdown").remove();
						clearInterval(verify_timer);
					}
				}, 1000)
			},
			blockReverify: function() {
				$("#validate_btn").removeClass('success').show();
				$("#validate_btn").append("<span id='validate_countdown' class='countdown' data-t='10'>(10)</span>");
				var validate_timer = setInterval(function(){
					var remain = parseInt($("#validate_countdown").data('t'));
					if (remain > 0) {
						remain--;
						$("#validate_countdown").data('t', remain);
						$("#validate_countdown").html("(" + remain + ")");
					} else {
						$("#validate_btn").addClass('success');
						$("#validate_countdown").remove();
						clearInterval(validate_timer);
					}
				}, 1000)
			}
		}
	},
	recruitArticle: function(){

		$(document).ready(function(){
			$(".job-detail").addClass("hidden");

			$(".job-box .job-title").click(function(){
					$(this).next(".mobile-btn").click();
			})

			$(".job-box .mobile-btn").click(function(){
					if ($(this).html() == String.fromCharCode(43)){
							showJob($(this));
					} else if ($(this).html() == String.fromCharCode(8211)){
							hideJob($(this));
					}
			})
		});

		function showJob(target){
			if (target.length) {
					$(".job-detail").addClass("hidden");
					$(".mobile-btn").html("&#43;");
					target.html("&#8211;");
					target.siblings('.job-info').children('.job-detail').removeClass("hidden");
					var offset = target.parent('.job-box').offset().top - 50;
					$(window).scrollTop(offset);
				}
		}
		
		function hideJob(target){
				if (target.length) {
					target.html("&#43;");
					target.siblings('.job-info').children('.job-detail').addClass("hidden");
					var offset = target.parent('.job-box').offset().top - 50;
					$(window).scrollTop(offset);
				}
		}
	},

	checkMissingPart: function() {
		if ($("#goods_list").hasClass("disabled")){
			if (!$('input[name="shipping_type"]').val()){ //if Shipping Type chosen
				$('html, body').animate({scrollTop: $(".ship_type").offset().top - 100}, 1000);
				YOHO.mobile.modal.showWarning(sprintf(
					YOHO._('checkout_address_error_not_selected', '請選擇%s！'),
					YOHO._('checkout_shipping_type', '送貨類型').toLowerCase()
				));
			} else if ($('input[name="shipping_type"]').val() == "pickuppoint"){ //if Shipping Type is pick up point
				if ((this.loopFindSelected())){ //if Shipping Method chosen
					return;
				} else if (!$('#selected_locker').html()){ //if Shipping Address chosen
					YOHO.mobile.modal.showWarning(sprintf(
						YOHO._('checkout_address_error_not_selected', '請選擇%s！'),
						YOHO._('checkout_consignee', '收貨地址').toLowerCase()
					));
					$('html, body').animate({scrollTop: $("#locker").offset().top - 100}, 1000);
				} else if ($("#payment_method").is(":visible")) { //if Payment Method chosen
					YOHO.mobile.modal.showWarning(YOHO._('checkout_error_missing_payment', '請選擇付款方式！'));
					$('html, body').animate({scrollTop: $("#payment_method").offset().top - 100}, 1000);
				}
			} else if ($('input[name="shipping_type"]').val() == "store"){  //if Shipping Type is shop pick up
				if (this.loopFindSelected()){ //if Shipping Method chosen
					return;
				} else if ($("#payment_method").is(":visible")) { //if Payment Method chosen
					YOHO.mobile.modal.showWarning(YOHO._('checkout_error_missing_payment', '請選擇付款方式！'));
					$('html, body').animate({scrollTop: $("#payment_method").offset().top - 100}, 1000);
				}
			} else if ($('input[name="shipping_type"]').val() == "express"){  //if Shipping Type is express
				if (!$("input[name='address_radio']:checked").val()){
					YOHO.mobile.modal.showWarning(sprintf(
						YOHO._('checkout_address_error_not_selected', '請選擇%s！'), 
						YOHO._('checkout_consignee', '收貨地址').toLowerCase()
					));
					$('html, body').animate({scrollTop: $("#address").offset().top - 100}, 1000);
				} else if ($("#payment_method").is(":visible")) { //if Payment Method chosen
					YOHO.mobile.modal.showWarning(YOHO._('checkout_error_missing_payment', '請選擇付款方式！'));
					$('html, body').animate({scrollTop: $("#payment_method").offset().top - 100}, 1000);
				}
			}
		}
	},

	loopFindSelected: function() {
		$selected = false;
		$items = $("#shipyyy li");
		$items.each(function(idx, li) {
			if ($(li).hasClass('current')){
				$selected = true;
			}
		});
		if (!$selected){
			YOHO.mobile.modal.showWarning(sprintf(
				YOHO._('checkout_address_error_not_selected', '請選擇%s！'),
				YOHO._('checkout_shipping', '送貨方式').toLowerCase()
			));
			$('html, body').animate({scrollTop: $("#shipping_method").offset().top - 100}, 1000);
			return true;
		} else {
			return false;
		}
	},

	checkTextFieldInt: function(element) {
		var input_str = element.val();
			if ((YOHO.check.isMobile(input_str) && YOHO.check.isTelephone(input_str)) || input_str.match(/^\+\d+-\d+$/) || input_str.match(/^\d+-\d+$/)) {
				element.css("border-color", "");
				return true;
			} else {
				element.css("border-color", "#ff5c5c");
				YOHO.mobile.modal.showWarning(sprintf(
					YOHO._('checkout_contact_no_error_invalid_tel_format', '請輸入正確的%s格式！'),
					YOHO._('checkout_address_phone', '聯絡電話').toLowerCase()
				));
				$('html, body').animate({scrollTop: $(element).offset().top - 100}, 1000);
				return false;
			}
	},

	checkTextFieldName: function(element) {
		var input_str = element.val();
			if (YOHO.check.isValidName(input_str)) {
				element.css("border-color", "");
				return true;
			} else {
				element.css("border-color", "#ff5c5c");
				YOHO.mobile.modal.showWarning(sprintf(
					YOHO._('checkout_address_error_invalid', '請輸入正確的%s！'),
					YOHO._('checkout_address_consignee', '收貨人姓名').toLowerCase()
				));
				$('html, body').animate({scrollTop: $(element).offset().top - 100}, 1000);
				return false;
			}
	},

	showPackageInfo: function(package_id) {
		$("div.package_info_popup[data-id=" + package_id + "]").fadeIn();
	},

	showFullAddressView: function() {
		$('.auto_address').show();
		$('.formatted_address').hide();
		$('#format_address_display').html("");
		$('.full_address_info:not(.formatted_address),#next_info').show();
	},

};

$(function (){
	YOHO.mobile.init();
});

})(jQuery);

function addToCart(goods_id)
{
	$.ajax({
		url: '/ajax/cartbuy.php',
		type: 'post',
		data: {
			'ac': 'ot',
			'id': goods_id,
			'gnum': 1
		},
		dataType: "json",
		success: function(result) {
			if(result.status == 1) {
				$require_addtocart = $("button.require_addtocart[data-rid=" + goods_id + "]");
				$require_addtocart.html(YOHO._('cart_require_goods_added', '已加入'));
				$require_addtocart.addClass("disabled");
				$require_addtocart.prop("disabled", true);
				$.ajax({
					url : '/cart',
					cache: false,
					success: function(result) {
						$('.cart-goods-list').html($(result).find('.cart-goods-list').html());
						YOHO.mobile.cart();
					},
					error: function(xhr) {
						YOHO.mobile.modal.showWarning(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
					}
				});
			} else {
				YOHO.mobile.modal.showWarning(result.text);
			}
		},
		error: function(xhr) {
			YOHO.mobile.modal.showWarning(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
		}
	});
}

function getBrowserPaymentTooltip($pay_code){
	return ("<ul class='payment-method-tooltip'><li>"+YOHO._(($pay_code+'_accept_payment_through'), '支援經瀏覽器Payment API 付款')+"</li><li>"+YOHO._(($pay_code+'_support_platform'), '注意: 瀏覽器的付款方法權限必須打開')+"</li><li>"+YOHO._(($pay_code+'_credit_card_data_remain_confidential'), '支付資料均會保密, 本公司不會儲存任何信用卡數據')+"</li><li>"+YOHO._(($pay_code+'_exchange_rate'), '所有項款皆以港幣 (HKD) 結算，結算匯率將由銀行/金融機構/ Stripe 提供')+"</li></ul>");
}

function initPaymentMethodTooltip($pay_code) {
	var selector = '.browser-payment-tooltip.'+$pay_code;
	$(selector).tooltip({
		container: 'body',
		template: "<div class='tooltip browser-payment-tooltip-container'><div class='tooltip-arrow'></div><div class='tooltip-inner'></div></div>"
	});
}
