<?php

/**
 * ECSHOP 商品页
 * ============================================================================
 * * 版权所有 2005-2012 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: testyang $
 * $Id: goods.php 15013 2008-10-23 09:31:42Z testyang $
*/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require(ROOT_PATH . 'includes/lib_order.php');
$fleashdealController = new Yoho\cms\Controller\FlashdealController();
$feedController       = new Yoho\cms\Controller\FeedController();
$packageController = new Yoho\cms\Controller\PackageController();
$orderController   = new Yoho\cms\Controller\OrderController();
$order_fee_platform = $orderController::ORDER_FEE_PLATFORM_WEB;
if ((DEBUG_MODE & 2) != 2)
{
    $smarty->caching = true;
}

$goods_id = !empty($_GET['id']) ? intval($_GET['id']) : '';

// goods id 19445 is the first product create on 2018-07-01, after 19445 products will redirect with https.
if (!empty($goods_id) && $goods_id >= 19445) {
    if (!isSecure()) {
        $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        header('HTTP/1.1 301 Moved Permanently');
        header('Location: ' . $redirect);
        exit;
    }
}

// Redirect user to new URL without the php filename
if (strpos($_SERVER['REQUEST_URI'], 'goods.php') !== false)
{
    $new_url = build_uri('goods', array('gid'=>$goods_id));

    // Append with original query string parameters, except the id which is already included in $new_url
    parse_str($_SERVER["QUERY_STRING"], $get);
    if (isset($get['id'])) unset($get['id']);
    if (!empty($get))
    {
        $new_url .= (strpos($new_url, '?') === false ? '?' : '&') . http_build_query($get, '', '&');
    }

    header('Location: ' . $new_url);
    exit;
}

// If goods is on flashdeal today
$flashdeal_id = $_REQUEST['fid'] ? intval($_REQUEST['fid']) : $fleashdealController->check_goods_is_flashdeal($goods_id);
$is_flashdeal_today = $fleashdealController->is_flashdeal_today();
//$is_flashdeal_today = false;
$VipController = new Yoho\cms\Controller\VipController();

/*------------------------------------------------------ */
//-- 判断是否存在缓存，如果存在则调用缓存，反之读取相应内容
/*------------------------------------------------------ */
$now = gmtime();
$todaystarttime = local_strtotime(local_date('Y-m-d 00:00:00'));
$is_now = $db->getOne(
    "SELECT 1 ".
    "FROM ".$ecs->table('flashdeals')." AS fd ".
    "LEFT JOIN ".$ecs->table('flashdeal_event')." AS fde ON fde.fd_event_id = fd.fd_event_id ".
    "WHERE fde.status = 1 AND fd.deal_date BETWEEN '" . $todaystarttime . "' AND '" . $now . "' "
);

//$goods_last_update = $db->getOne("SELECT last_update FROM " . $ecs->table('goods') . " WHERE goods_id = $goods_id LIMIT 1");

if (isset($_REQUEST['c_ur']) && !empty($_REQUEST['c_lang']) && !empty($_REQUEST['c_fdid'])) {
    $_SESSION['user_rank'] = $_REQUEST['c_ur'];
    $_CFG['lang'] = $_REQUEST['c_lang'];
    $flashdeal_id = $_REQUEST['c_fdid'];
    $_REQUEST['fid'] = $_REQUEST['c_fdid'];
    $is_now = 1;
    $is_testing = '';
}

/* 缓存编号 */
//$cache_id = sprintf('%X', crc32($goods_id . '-' . $_SESSION['user_rank'] . '-' . $goods_last_update . '-' . $_CFG['lang'] . '-' . user_area().'-'.$is_now.'-'.$is_testing)); // old cache id
$cache_id = sprintf('%X', crc32($goods_id . '-' . $_SESSION['user_rank'] . '-' . $_CFG['lang'] . '-' .  $flashdeal_id  .'-'. $is_now .'-'.$is_testing));
$cache_id =  $goods_id . '-' . $cache_id ;
$smarty->assign('is_flashdeal_today', $is_flashdeal_today);

//获取关联礼包
if ($is_flashdeal_today && $flashdeal_id > 0) {
    $package_goods_list = [];
    $package = [];
} else {
    $package_goods_list = $packageController->get_package_goods_list($goods_id);
    $package = $package_goods_list[0];
}

//print_r($package_goods_list);
$smarty->assign('package_goods_list',$package_goods_list);    // 获取关联礼包
$smarty->assign('package',$package);    // 获取关联礼包
assign_dynamic('goods');
$volume_price_list = get_volume_price_list($goods_id, '1');
$smarty->assign('volume_price_list',$volume_price_list);    // 商品优惠价格区间

//获取優惠活動
if ($is_flashdeal_today && $flashdeal_id > 0) {
    $favourable_list_by_goods = [];
} else {
    $favourable_list_by_goods = $orderController->favourable_list_by_goods($_SESSION['user_rank'], $_SESSION['user_id'], $goods_id, $order_fee_platform);
}

//print_r($favourable_list_by_goods);echo "33864549";
$smarty->assign('favourable_list_by_goods',$favourable_list_by_goods);    // 获取優惠活動

//insurance

$insurance_plan_list = $orderController->getInsurancePlanByGoodsID($goods_id); // insurance plan list of this good
$is_insurance = FALSE; // if good got insurance plan
$type_insurance = array();

if (!empty($insurance_plan_list)){
    $is_insurance = TRUE;
    foreach ($insurance_plan_list as $plan_name => $val){
        array_push($type_insurance, $plan_name);
    }
}

$smarty->assign('is_insurance',$is_insurance); 

$smarty->assign('type_insurance',$type_insurance);

$smarty->assign('insurance_plan_list',$insurance_plan_list); 

if (!$smarty->is_cached('goods.html', $cache_id))
//if (true)
{
    assign_template();

    $smarty->assign('goods_id', $goods_id);
    
    /* 获得商品的信息 */
    if ($is_testing){
        $goods = get_goods_info($goods_id,true);
    } else {
        $goods = get_goods_info($goods_id);
    }

    if ($goods === false)
    {
       /* 如果没有找到任何记录则跳回到首页 */
       ecs_header("Location: /\n");
       exit;
    }
    $linked_goods = get_linked_goods($goods_id);
    foreach(get_parent_cats($goods['cat_id']) as $k=>$v)
    {
        $catlist[] = $v['cat_id'];
    }

    $smarty->assign('catlist',  $catlist);           // set parent categorey list

    $position = assign_mobile_ur_here($goods['cat_id'], $goods['goods_name']);
    $smarty->assign('page_title',      $position['title']);    // 页面标题
    $smarty->assign('ur_here',         $position['ur_here']);  // 当前位置
    $CategoryController = new Yoho\cms\Controller\CategoryController();
    $t1 = $CategoryController->getTopCategories(0);
    $smarty->assign('t1',              $t1);
    $smarty->assign('helps',           get_shop_help());


    /* meta information */
    $smarty->assign('keywords',        htmlspecialchars($_CFG['shop_keywords']));
    $smarty->assign('description',     htmlspecialchars($_CFG['shop_desc']));

    // YOHO: Don't show accurate stocks
    $goods['goods_number'] = $goods['goods_number'] < 20 ? $goods['goods_number'] : 20;

    // YOHO: If 代購，pretend to have stocks (set to 20 here)
    // $goods['goods_number'] = $goods['pre_sale_days'] > 0 ? 20 : $goods['goods_number'];

    $smarty->assign('goods', $goods);
    $shop_price = $goods['shop_price'];
    $smarty->assign('rank_prices', get_user_rank_prices($goods_id, $shop_price));    // 会员等级价格

    //brand-logo
    if (isset($goods['brand_id']) && $goods['brand_id'] > 0)
    {
        $sql = "SELECT brand_logo, brand_id, brand_name, brand_mobile_banner, bperma FROM " . $ecs->table('brand') . " b " .
                "LEFT JOIN (SELECT id, table_name, lang, perma_link as bperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") bpl ON bpl.id = b.brand_id  AND bpl.table_name = 'brand' AND bpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
                " WHERE brand_id = '" . $goods['brand_id'] . "' " .
                "LIMIT 1";
        $brand = $db->getRow($sql);
        if ($brand['brand_logo'])
        {
            $brand_logo = '/' . DATA_DIR . '/brandlogo/' . $brand['brand_logo'];
            $brand_url = build_uri('brand', array('cid' => 0, 'bid' => $brand['brand_id'], 'bperma' => $brand['bperma']), $brand['brand_name']);
            $smarty->assign('brand_logo', $brand_logo);
            $smarty->assign('brand_url', $brand_url);
        }

        if($brand['brand_mobile_banner']) {
            $brand_banner = '/' . DATA_DIR . '/brandbanner/' . $brand['brand_mobile_banner'];
            $smarty->assign('brand_banner', $brand_banner);
        }
    }

    $properties = get_goods_properties($goods_id);  // 获得商品的规格和属性

    //获取tag
    $tagController = new Yoho\cms\Controller\TagController();
    $tag_array = $tagController->get_tags_by_goods($goods_id);
    // $tag_array = get_tags($goods_id);
    $smarty->assign('tags',                $tag_array);

    // $smarty->assign('specification',   $properties['spe']);                   // 商品规格
    $smarty->assign('pictures',        get_goods_gallery($goods_id));         // 商品相册
    $smarty->assign('fittings',        get_goods_fittings(array($goods_id))); // 配件
    $smarty->assign('related_goods',       $linked_goods);                                   // 关联商品
    /* disabled by Anthony: For 11/11 flashdeal. */
    // $smarty->assign('bought_goods',    get_also_bought($goods_id));           // 购买了该商品的用户还购买了哪些商品
    $smarty->assign('today',           get_today_info());                     // 假期判定，用於顯示不同送貨時間
    $smarty->assign('user_area',       user_area());                          // 地區判定，用於顯示海外送貨提示
    // $smarty->assign('goods_article_list',  get_linked_articles($goods_id));                  // 关联文章
	$feed = $feedController->goods_insert_code($goods);
    $smarty->assign('stats_code_insert', $feed['stats_code_insert']);
    $smarty->assign('extra_insert_code', $feed['extra_insert_code']);
    assign_dynamic('goods');

    if ($is_flashdeal_today) {
        $comment_arr['name']  = 'comments';
        $comment_arr['id']  = $goods_id;
        $comment_arr['type']  = 0 ;
        $comment_arr['lei'] = 1;
        
        $smarty->assign('insert_comments_goods', insert_comments($comment_arr));

        $goodspromote_arr['name'] = 'goodspromote';
        $goodspromote_arr['goods_id'] = $goods_id ;
        $goodspromote_arr['return_output'] = false ;
        insert_goodspromote($goodspromote_arr);

        $flash_widget_arr['name'] = 'flashdeal_widget';
        $flash_widget_arr['id'] = $goods_id ;
        $flash_widget_arr['type'] = 'goods' ;
        $flash_widget_arr['return_output'] = false ;
        insert_flashdeal_widget($flash_widget_arr);
    }
}
    $smarty->assign('page_lang',  $GLOBALS['_CFG']['lang']);
    $smarty->assign('cart_num', insert_cart_info());
    $smarty->assign('vip',       $VipController->get_goods_page_vip_info());
    $smarty->display('goods.html', $cache_id);
/**
 * 获得指定商品的各会员等级对应的价格
 *
 * @access  public
 * @param   integer     $goods_id
 * @return  array
 */
function get_user_rank_prices($goods_id, $shop_price)
{
    $sql = "SELECT rank_id, IFNULL(mp.user_price, r.discount * $shop_price / 100) AS price, r.rank_name, r.discount " .
            'FROM ' . $GLOBALS['ecs']->table('user_rank') . ' AS r ' .
            'LEFT JOIN ' . $GLOBALS['ecs']->table('member_price') . " AS mp ".
                "ON mp.goods_id = '$goods_id' AND mp.user_rank = r.rank_id " .
            "WHERE r.show_price = 1 OR r.rank_id = '$_SESSION[user_rank]'";
    $res = $GLOBALS['db']->query($sql);

    $arr = array();
    while ($row = $GLOBALS['db']->fetchRow($res))
    {

        $arr[$row['rank_id']] = array(
            'rank_name' => htmlspecialchars($row['rank_name']),
            'price'     => price_format($row['price'])
        );
    }

    return $arr;
}


?>
