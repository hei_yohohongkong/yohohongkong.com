<?php

/**
 * ECSHOP 专题前台
 * ============================================================================
 * * 版权所有 2005-2012 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * @author:     webboy <laupeng@163.com>
 * @version:    v2.1
 * ---------------------------------------------
 */

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

if (!isSecure()) {
    $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: ' . $redirect);
    exit;
}

if ((DEBUG_MODE & 2) != 2)
{
    $smarty->caching = true;
}

$topic_id = empty($_REQUEST['topic_id']) ? (empty($_REQUEST['id']) ? 0 : intval($_REQUEST['id'])) : intval($_REQUEST['topic_id']);
$redirection = $db->getOne("SELECT redirection FROM ".$ecs->table('topic')." WHERE topic_id = $topic_id ");
if($redirection > 0) {
    header("HTTP/1.1 301 Moved Permanently");
    header("Location: /post/".$redirection);
    exit();
}

// Hardcode redirection for 313 page
if ($topic_id == 98)
{
    header('Location: /313');
    exit;
}

if (empty($topic_id))
{
    $cat_id = empty($_REQUEST['cat_id']) ? 0 : intval($_REQUEST['cat_id']);
    if ($cat_id > 0)
    {
        $sql = "SELECT IFNULL(tcl.topic_cat_name, tc.topic_cat_name) " .
                "FROM " . $ecs->table('topic_category') . " as tc " .
                "LEFT JOIN " . $ecs->table('topic_category_lang') . " as tcl " .
                    "ON tcl.topic_cat_id = tc.topic_cat_id AND tcl.lang = '" . $_CFG['lang'] . "' " .
                "WHERE tc.topic_cat_id = '" . $cat_id . "'";
        $cat_name = $db->getOne($sql);
    }
    if (empty($cat_name))
    {
        $cat_id = 0;
        $cat_name = '';
    }
    $smarty->assign('cat_id', $cat_id);
    $smarty->assign('cat_name', $cat_name);
    
    $cache_id = sprintf('%X', crc32($_SESSION['user_rank'] . '-' . $_CFG['lang'] . '-' . $cat_id));
    
    if (!$smarty->is_cached('topics.html', $cache_id))
    {
        assign_template();
        $position = assign_mobile_ur_here(0, $cat_name);
        $smarty->assign('page_title',       $position['title']);                    // 页面标题
        $smarty->assign('ur_here',          $position['ur_here']);                  // 当前位置
        
        $CategoryController = new Yoho\cms\Controller\CategoryController();
        $t1 = $CategoryController->getTopCategories(0);
	    $smarty->assign('t1',              $t1);
        $smarty->assign('helps',            get_shop_help());
        
        $sql = "SELECT t.topic_id, IFNULL(tl.title, t.title) as title, tperma " .
               "FROM " . $ecs->table('topic') . " as t " .
               "LEFT JOIN " . $ecs->table('topic_lang') . " as tl ON tl.topic_id = t.topic_id AND tl.lang = '" . $_CFG['lang'] . "' " .
               "LEFT JOIN (SELECT id, table_name, lang, perma_link as tperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") bpl ON bpl.id = t.topic_id  AND bpl.table_name = 'topic' AND bpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
               "WHERE " . gmtime() . " BETWEEN t.start_time AND t.end_time + 86399 " .
               "AND FIND_IN_SET('" . $cat_id . "', t.topic_cat_id) != 0 " .
               "ORDER BY t.sort_order ASC, t.topic_id DESC";
        $topics = $GLOBALS['db']->getAll($sql);
        
        foreach ($topics as $key => $row)
        {
            $topics[$key]['url'] = build_uri('topic', array('tid' => $row['topic_id'], 'tname' => $row['title'],'tperma' => $row['tperma'] ), $row['title']);
        }
        
        $smarty->assign('topic', $topics);
    }
    
    $smarty->display('topics.html', $cache_id);
    exit;
}

$sql = "SELECT t.*, " .
            "IFNULL(tl.title, t.title) as title, " .
            "IFNULL(tl.intro, t.intro) as intro, " .
            "IFNULL(tl.intro_mobile, t.intro_mobile) as intro_mobile, " .
            "IFNULL(tl.template, t.template) as template, " .
            "IFNULL(tl.htmls, t.htmls) as htmls, " .
            "IFNULL(tl.keywords, t.keywords) as keywords, " .
            "IFNULL(tl.description, t.description) as description " .
        "FROM " . $ecs->table('topic') . " as t " .
        "LEFT JOIN " . $ecs->table('topic_lang') . " as tl ON tl.topic_id = t.topic_id AND tl.lang = '" . $_CFG['lang'] . "' " .
        "WHERE t.topic_id = '$topic_id' " .
        "AND " . gmtime() . " BETWEEN start_time AND end_time + 86399";

$topic = $db->getRow($sql);

if (empty($topic))
{
    /* 如果没有找到任何记录则跳回到专题列表 */
    header("Location: " . build_uri('topic', array()));
    exit;
}

$templates = empty($topic['template']) ? 'topic.html' : $topic['template'];

$cache_id = sprintf('%X', crc32($_SESSION['user_rank'] . '-' . $_CFG['lang'] . '-' . $topic_id));

if (!$smarty->is_cached($templates, $cache_id))
{
    if (empty($topic['topic_img']))
    {
        $topic['header_type'] = 'html';
    }
    else
    {
        if ((strpos($topic['topic_img'], '://')  === false) && (substr($topic['topic_img'], 0, 1) != '/'))
        {
            $topic['topic_img'] = '/' . $topic['topic_img'];
        }
        
        if (strpos($topic['topic_img'], '.swf')  !== false)
        {
            $topic['header_type'] = 'flash';
        }
        else
        {
            $topic['header_type'] = 'image';
        }
    }
    
    $tmp = @unserialize($topic['data']);
    if (!$tmp) // Load old malformed data
    {
        $topic['data'] = addcslashes($topic['data'], "'");
        $tmp = @unserialize($topic['data']);
    }
    $data_arr = (array)$tmp;

    $goods_id = array();
    $sort_goods_arr = array();

    foreach ($data_arr AS $key=>$value)
    {
        foreach($value AS $k => $val)
        {
            $opt = explode('|', $val);
            $data_arr[$key][$k] = $opt[1];
            $goods_id[] = $opt[1];
            $sort_goods_arr[($key == 'default' ? _L('topic_all_goods', '全部相關產品') : $key)] = array();
        }
    }

    // check if it only show top 50
    if ($topic['show_top_50'] == 1) {
        $sql = "select goods_id from ". $ecs->table('goods') ." g where ".db_create_in($goods_id, 'g.goods_id')." order by sort_order desc limit 50 ";
        $filtered_goods = $db->getCol($sql);
        if (sizeof($filtered_goods) > 0) {
            $goods_id = $filtered_goods;
        }
    }

    $sql = hw_goods_list_sql(db_create_in($goods_id, 'g.goods_id'), 'goods_id', 'DESC');
    $res = $GLOBALS['db']->getAll($sql);
    $res = hw_process_goods_rows($res);
    
    foreach ($res as $row)
    {
        foreach ($data_arr AS $key => $value)
        {
            foreach ($value AS $val)
            {
                if ($val == $row['goods_id'])
                {
                    $key = $key == 'default' ? _L('topic_all_goods', '全部相關產品') : $key;
                    $sort_goods_arr[$key][] = $row;
                }
            }
        }
    }

    /* 模板赋值 */
    assign_template();
    // handle multi-category
    $tcid = explode(",", $topic['topic_cat_id'])[0];
    $position = assign_mobile_ur_here($tcid, $topic['title']);
    $smarty->assign('page_title',       $position['title']);       // 页面标题
    $smarty->assign('ur_here',          $position['ur_here']);     // 当前位置
    $smarty->assign('sort_goods_arr',   $sort_goods_arr);          // 商品列表
    $smarty->assign('topic',            $topic);                   // 专题信息
    $smarty->assign('keywords',         $topic['keywords']);       // 专题信息
    $smarty->assign('description',      $topic['description']);    // 专题信息
    $smarty->assign('title_pic',        $topic['title_pic']);      // 分类标题图片地址
    $smarty->assign('base_style',       '#' . $topic['base_style']);     // 基本风格样式颜色
    
    $CategoryController = new Yoho\cms\Controller\CategoryController();
    $t1 = $CategoryController->getTopCategories(0);
    $smarty->assign('t1',              $t1);
    $smarty->assign('helps',            get_shop_help());
}
/* 显示模板 */
$smarty->display($templates, $cache_id);

?>
