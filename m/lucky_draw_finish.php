<?php

/***
* luckydraw.php
* by eric
*
* YOHO Hong Kong Lucky Draw page
***/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

if ((DEBUG_MODE & 2) != 2)
{
    $smarty->caching = true;
}

$user_id = $_SESSION['user_id'];
$user_rank = $_SESSION['user_rank'];
$rank_points = $_SESSION['rank_points'];

if (empty($_SESSION['user_id'])) {
	header('Location: /luckydraw');
	exit;
}

$luckyDrawController = new Yoho\cms\Controller\LuckyDrawController();
$lucky_draw_active = $luckyDrawController->getActiveLuckyDrawEvent();

if (empty($lucky_draw_active)) {
	header('Location: /luckydraw');
	exit; 
}

$lucky_draw_id = $lucky_draw_active[0];
$lucky_draw_info = $luckyDrawController->getLuckyDrawEventInfo($lucky_draw_id);

if (empty($lucky_draw_info['finished_mobile'])) {
    $lucky_draw_info['finished_mobile'] = $lucky_draw_info['finished'];
}


$affiliate = $affiliate = unserialize($_CFG['affiliate']);

$splitted_link = explode("#", $lucky_draw_info['share_link_1']);
$splitted_link[0] .= '?u='.$user_id;
$url = implode('#', $splitted_link);

//$url = $lucky_draw_info['share_link_1'].'?u='.$user_id;
// if(isset($affiliate['on']) && $affiliate['on'] == 1) { //友福同享
    if($affiliate['config']['affiliate_text']) {
        $text = $affiliate['config']['affiliate_text'];
        $text = str_replace("{%url}", $url, $text);
        $text = str_replace("{%code}", intval($_SESSION['user_id']), $text);
    }
    $title = _L('user_affiliate_type');
    $img = substr($_SERVER['HTTP_HOST'], 0, 2) === "m." ? "../yohohk/img/affiliate.png" :$_CFG['affiliate_btn_img'];
    $tmp_img = dirname(__FILE__).'/'.$img;
    $banner_img = $_CFG['affiliate_banner_img'];
//}

$share_url = $url;

setcookie('m_display', $display, gmtime() + 86400 * 7);

/* 赋值固定内容 */
assign_template();
$smarty->assign('page_title', $_LANG['lucky_draw']);  // 页面标题
$smarty->assign('ur_here',    $_LANG['lucky_draw']);  // 当前位置
$smarty->assign('helps', get_shop_help());              // 网店帮助
$smarty->assign('lucky_draw_info',    $lucky_draw_info);  // 当前位置
$smarty->assign('user_id',    $user_id); 
$smarty->assign('share_url',  $share_url);
$smarty->assign('text',  $text);
$smarty->assign('title',  $title);
$smarty->assign('banner_img',  $banner_img);
$smarty->assign('is_login',  ($_SESSION['user_id']) ? "y":"n");

$smarty->display('lucky_draw_finish.html');
