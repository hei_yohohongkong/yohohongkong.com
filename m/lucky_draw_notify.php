<?php
/**
* YOHO Lucky Draw page
* 20180731
*/
define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

if ((DEBUG_MODE & 2) != 2) {
    $smarty->caching = true;
}

// check login 
// echo 'info confirm page<br>';
// echo 'user_id:'.$_SESSION['user_id'];

if (empty($_SESSION['user_id'])) {
    // redirect to login page;
	header('Location: /user.php?act=login');
	exit;
}

$user_id = $_SESSION['user_id'];
$user_rank = $_SESSION['user_rank'];
$rank_points = $_SESSION['rank_points'];
$luckyDrawController = new Yoho\cms\Controller\LuckyDrawController();

$is_lucky_draw_active = $luckyDrawController->isLuckyDrawEventActive();
if (!$is_lucky_draw_active) {
    // redirect
	header('Location: /luckydraw');
	exit; 
}

if (!empty($_REQUEST['action']) && $_REQUEST['action'] == 'lucky_draw_notify') {
    $ticket_id = $_REQUEST['play_id'];
    // get the prize (gift id)
    $result = $luckyDrawController->sendLuckyDrawResultNotice($ticket_id);

    if (isset($result['error'])) {
        echo json_encode($result);
    } else {
        echo json_encode(array('data' => $result) );
    }
    exit;
}
