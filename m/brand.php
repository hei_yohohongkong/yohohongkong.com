<?php

/**
 * ECSHOP 品牌专区
 * ============================================================================
 * * 版权所有 2005-2012 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: testyang $
 * $Id: brands.php 15013 2008-10-23 09:31:42Z testyang $
*/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

// howang: make the parameter consistent with the non-mobile version
if (!empty($_GET['id']))
{
    $_GET['b_id'] = $_GET['id'];
}

$b_id = !empty($_GET['b_id']) ? intval($_GET['b_id']) : 0;
$attrbuteController = new Yoho\cms\Controller\AttributeController();
$categoryController = new Yoho\cms\Controller\CategoryController();
// Redirect user to new URL without the php filename
if (strpos($_SERVER['REQUEST_URI'], 'brand.php') !== false)
{
    $new_url = build_uri('brand', array('bid' => $b_id));

    // Append with original query string parameters, except the id which is already included in $new_url
    parse_str($_SERVER["QUERY_STRING"], $get);
    if (isset($get['id'])) unset($get['id']);
    if (isset($get['b_id'])) unset($get['b_id']);
    if (!empty($get))
    {
        $new_url .= (strpos($new_url, '?') === false ? '?' : '&') . http_build_query($get, '', '&');
    }

    header('Location: ' . $new_url);
    exit;
}

/* 初始化分页信息 */
$page = !empty($_REQUEST['page']) && intval($_REQUEST['page']) > 0 ? intval($_REQUEST['page']) : 1;
$size = !empty($_CFG['page_size']) && intval($_CFG['page_size']) > 0 ? intval($_CFG['page_size']) : 10;
$cat_id = !empty($_REQUEST['cat']) && intval($_REQUEST['cat']) > 0 ? intval($_REQUEST['cat']) : 0;
$price_min = isset($_REQUEST['price_min']) && intval($_REQUEST['price_min']) > 0 ? intval($_REQUEST['price_min']) : 0;
$price_max = isset($_REQUEST['price_max']) && intval($_REQUEST['price_max']) > 0 ? intval($_REQUEST['price_max']) : 0;

/* 排序、显示方式以及类型 */
$default_display_type = $_CFG['show_order_type'] == '0' ? 'list' : ($_CFG['show_order_type'] == '1' ? 'grid' : 'text');
$default_sort_order_method = $_CFG['sort_order_method'] == '0' ? 'DESC' : 'ASC';
$default_sort_order_type   = $_CFG['sort_order_type'] == '0' ? 'last_update' : ($_CFG['sort_order_type'] == '1' ? 'shop_price' : 'goods_id');

$sort = (isset($_REQUEST['sort']) && in_array(trim(strtolower($_REQUEST['sort'])), array('goods_id', 'shop_price', 'last_update', 'shownum'))) ? trim($_REQUEST['sort']) : $default_sort_order_type;
$order = (isset($_REQUEST['order']) && in_array(trim(strtoupper($_REQUEST['order'])), array('ASC', 'DESC'))) ? trim($_REQUEST['order']) : $default_sort_order_method;
$display = (isset($_REQUEST['display']) && in_array(trim(strtolower($_REQUEST['display'])), array('list', 'grid', 'text'))) ? trim($_REQUEST['display']) : (isset($_COOKIE['m_display']) ? $_COOKIE['m_display'] : $default_display_type);
//$display  = in_array($display, array('list', 'grid', 'text')) ? $display : 'text';
$display = in_array($display, array('list', 'grid')) ? $display : 'grid';
$filter_attr_str = isset($_REQUEST['filter_attr']) ? htmlspecialchars(trim($_REQUEST['filter_attr'])) : '0';

$filter_attr_str = trim(urldecode($filter_attr_str));
$filter_attr_str = preg_match('/^[\d\.]+$/', $filter_attr_str) ? $filter_attr_str : '';
$filter_attr = empty($filter_attr_str) ? '' : explode('.', $filter_attr_str);

/* 页面的缓存ID */
$cache_id = sprintf('%X', crc32($b_id . '-' . $sort . '-' . $order . '-' . $page . '-' . $_SESSION['user_rank'] . '-' . $_CFG['lang']));

if (!$smarty->is_cached('brand.html', $cache_id))
{
    assign_template();
    
    if ($b_id > 0)
    {
        $brand_info = get_brand_info($b_id);
        $smarty->assign('brand_info', $brand_info);
        $sql = "SELECT count(*) FROM " . $ecs->table('goods') . " as g WHERE g.is_on_sale = 1 AND g.is_alone_sale = 1 AND g.is_delete = 0 AND g.brand_id = '$b_id'";
        $num = $db->getOne($sql);
        if ($num > 0)
        {
            $page_num = 20;
            $pages = ceil($num / $page_num);
            if ($page <= 0)
            {
                $page = 1;
            }
            if ($pages == 0)
            {
                $pages = 1;
            }
            if ($page > $pages)
            {
                $page = $pages;
            }
            $i = 1;
            $pager = get_mobile_pager($num, $page_num, $page, build_uri('brand', array('bid' => $b_id, 'bperma' => $brand_info['bperma'], 'sort' => $sort, 'order' => $order)), 'page');
            $smarty->assign('pager', $pager);
        }
        $children = get_children($cat_id);
        
        /* 属性筛选 */
        $attr_data = $attrbuteController->get_attr_data($cat_id, $filter_attr, 'brand', array('cid'=>$cat_id, 'bid'=>$b_id, 'price_min'=>$price_min, 'price_max'=>$price_max, 'filter_attr'=>$filter_attr));
        $ext = $attr_data['ext']; //商品查询条件扩展
        $all_attr_list = $attr_data['all_attr_list'];
    
        $count = hw_get_cagtegory_goods_count($children, $b_id, $price_min, $price_max, $ext);
        $goods_list = hw_category_get_goods($children, $b_id, $price_min, $price_max, $ext, $size, $page, $sort, $order);
        $smarty->assign('goods_list', $goods_list);

        $goods_id_arr = array();
        foreach($goods_list as $val){
            $goods_id_arr[] = $val['goods_id'];
        }
        $goodsController = new Yoho\cms\Controller\GoodsController();
        //get favourable list
        $favourable_arr = $goodsController->product_list_get_favourable_info($_SESSION['user_rank'], $goods_id_arr);

        //get package list
        $package_arr = $goodsController->product_list_get_package_info($goods_id_arr);

        //get topic list
        $topic_arr = $goodsController->product_list_get_topic_info($goods_id_arr);

        $smarty->assign('favourable_list', $favourable_arr);
        $smarty->assign('package_list',    $package_arr);
        $smarty->assign('topic_list',      $topic_arr);

        
        $sort_names = array(
            'last_update' => _L('productlist_sort_last_update', '預設排序'),
            'shownum' => _L('productlist_sort_shownum', '銷量排序'),
            'goods_id' => _L('productlist_sort_goods_id', '上架時間'),
            'shop_price' => _L('productlist_sort_price', '價格排序')
        );
        $sort_name = isset($sort_names[$sort]) ? $sort_names[$sort] : _L('productlist_sort_by', '排序方式');
        $smarty->assign('sort_name', $sort_name);

        $new_order = $order == "DESC" ? "ASC" : "DESC";
        $sort_links = array(
            'last_update' => ['url'=>build_uri('brand', array('cid' => $cat_id, 'sort' => 'last_update', 'order' => 'DESC', 'page' => $page,'bid'=>$b_id, 'bperma' => $brand_info['bperma'], 'price_min'=>$price_min, 'price_max'=>$price_max, 'filter_attr'=>$filter_attr_str))],
            'shownum' => ['url'=>build_uri('brand', array('cid' => $cat_id, 'sort' => 'shownum', 'order' => 'DESC', 'page' => $page,'bid'=>$b_id, 'bperma' => $brand_info['bperma'], 'price_min'=>$price_min, 'price_max'=>$price_max, 'filter_attr'=>$filter_attr_str))],
            'goods_id' => ['url'=>build_uri('brand', array('cid' => $cat_id, 'sort' => 'goods_id', 'order' => 'DESC', 'page' => $page,'bid'=>$b_id, 'bperma' => $brand_info['bperma'], 'price_min'=>$price_min, 'price_max'=>$price_max, 'filter_attr'=>$filter_attr_str))],
            'shop_price' => ['url'=>build_uri('brand', array('cid' => $cat_id, 'sort' => 'shop_price', 'order' => 'DESC', 'page' => $page,'bid'=>$b_id, 'bperma' => $brand_info['bperma'], 'price_min'=>$price_min, 'price_max'=>$price_max, 'filter_attr'=>$filter_attr_str))],
        );
        foreach ($sort_links as $key => $value) {
            if($sort == $key){
                $sort_links[$key]['order'] = $order;
                $sort_links[$key]['url']   = build_uri('brand', array('cid' => $cat_id, 'sort' => $key, 'order' => $new_order, 'page' => $page,'bid'=>$b_id, 'bperma' => $brand_info['bperma'], 'price_min'=>$price_min, 'price_max'=>$price_max, 'filter_attr'=>$filter_attr_str));
            }
        }
        $smarty->assign('sort_links', $sort_links);
        // Price Filter
        $smarty->assign('price_min', $price_min);
        $smarty->assign('price_max', $price_max);
        $where = "g.brand_id = '" . $b_id . "' AND g.is_on_sale = 1 AND g.is_alone_sale = 1 AND g.is_delete = 0 ";
        $left_menu = $categoryController->getLeftCatMenu($cat_id, "brand", $where, $b_id);
        if($category > 0){
            $first_parent = array_reverse($categoryController->getParentLevel($category));
            $smarty->assign('first_parent_cat',$first_parent[0]['cat_id']);
        }
        unset($left_menu['count']);
        $smarty->assign('filter_cat_menu',$left_menu);
        $smarty->assign('script_name', 'brand');
        hw_assign_price_range("g.is_delete = 0 AND g.is_on_sale = 1 AND g.is_alone_sale = 1 AND g.brand_id = '$b_id'",$_SESSION['user_rank']);
    }
    $position = assign_mobile_ur_here(0,  $brand_info['brand_name']);
    if(!empty($_GET['id']))
	{
        $position = assign_mobile_ur_here(0, (empty($brand_info['brand_name'])) ? _L('brand_list', '所有品牌') : $brand_info['brand_name']);
    }
    $smarty->assign('page_title', $position['title']);    // 页面标题
    $smarty->assign('ur_here', $position['ur_here']);  // 当前位置
    $CategoryController = new Yoho\cms\Controller\CategoryController();
    $t1 = $CategoryController->getTopCategories(0);
    $smarty->assign('t1',              $t1);
    $smarty->assign('helps', get_shop_help());
    $smarty->assign('display',      $display);


    /* meta information */
    $smarty->assign('keywords', htmlspecialchars($_CFG['shop_keywords']));

    $where = "g.brand_id = '" . $b_id . "' AND g.is_on_sale = 1 AND g.is_alone_sale = 1 AND g.is_delete = 0 ";
    $left_menu = $categoryController->getLeftCatMenu($cat_id, "brand", $where, $b_id);
    $l3_cat_name = "";
    foreach($left_menu as $val){
        $level2 = $val["children"];
        foreach($level2 as  $val2){
            $level3 = $val2["children"];

            foreach($level3 as $key => $val3){
                if($lang == 'en_us') {
                        $l3_cat_name .= $val3["cat_name"] ."，";   
                } else {
                        $l3_cat_name .= $val3["cat_name"] ."、";
                }
            }
        }      
    }
    $l3_cat_name_len = strlen($l3_cat_name);
    $l3_cat_name = substr($l3_cat_name,0,$l3_cat_name_len-3);
    $brand_desc = $brand_info['brand_desc'];   
    $insert_pos = strpos($brand_desc,"-");
    $brand_info['brand_desc'] = substr_replace($brand_desc, $l3_cat_name , $insert_pos-1,0);
    $smarty->assign('description', htmlspecialchars($brand_info['brand_desc']));
    $brands_array = get_brands();

    if (count($brands_array) > 1)
	{
        $smarty->assign('brand_id', $b_id);
        $smarty->assign('other_brands', $brands_array);
    }
}

$smarty->display('brand.html', $cache_id);
?>