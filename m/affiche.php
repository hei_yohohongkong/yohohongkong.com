<?php

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

$desktop_url = PROTOCOL_PREFIX . DESKTOP_HOST . $_SERVER['REQUEST_URI'];

header('Location: ' . $desktop_url);
exit;

?>