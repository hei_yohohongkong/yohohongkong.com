<?php

/**
 * ECSHOP 商品分类页
 * ============================================================================
 * * 版权所有 2005-2012 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: testyang $
 * $Id: category.php 15013 2008-10-23 09:31:42Z testyang $
*/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
$CategoryController = new Yoho\cms\Controller\CategoryController();
$AttrbuteController = new Yoho\cms\Controller\AttributeController();
$goodsController = new Yoho\cms\Controller\GoodsController();
// howang: make the parameter consistent with the non-mobile version
if (!empty($_GET['id']))
{
    $_GET['c_id'] = $_GET['id'];
}
else if (!empty($_GET['category']))
{
    $_GET['c_id'] = $_GET['category'];
}
if ((DEBUG_MODE & 2) != 2)
{
    $smarty->caching = true;
}
$cat_id = !empty($_GET['c_id']) ? intval($_GET['c_id']) : 0;
$cat_id = ($cat_id < 0) ? 0 : $cat_id;

// Redirect user to new URL without the php filename
if (strpos($_SERVER['REQUEST_URI'], 'category.php') !== false)
{
    $new_url = build_uri('category', array('cid' => $cat_id));
    
    // Append with original query string parameters, except the id which is already included in $new_url
    parse_str($_SERVER["QUERY_STRING"], $get);
    if (isset($get['id'])) unset($get['id']);
    if (isset($get['category'])) unset($get['category']);
    if (isset($get['c_id'])) unset($get['c_id']);
    if (!empty($get))
    {
        $new_url .= (strpos($new_url, '?') === false ? '?' : '&') . http_build_query($get, '', '&');
    }
    
    header('Location: ' . $new_url);
    exit;
}

/*------------------------------------------------------ */
//-- INPUT
/*------------------------------------------------------ */

/* 初始化分页信息 */
$page = isset($_REQUEST['page'])   && intval($_REQUEST['page'])  > 0 ? intval($_REQUEST['page'])  : 1;
$size = isset($_CFG['page_size'])  && intval($_CFG['page_size']) > 0 ? intval($_CFG['page_size']) : 10;
$brand = isset($_REQUEST['brand']) && intval($_REQUEST['brand']) > 0 ? intval($_REQUEST['brand']) : 0;
$price_max = isset($_REQUEST['price_max']) && intval($_REQUEST['price_max']) > 0 ? intval($_REQUEST['price_max']) : 0;
$price_min = isset($_REQUEST['price_min']) && intval($_REQUEST['price_min']) > 0 ? intval($_REQUEST['price_min']) : 0;
$filter_attr_str = isset($_REQUEST['filter_attr']) ? htmlspecialchars(trim($_REQUEST['filter_attr'])) : '0';

$filter_attr_str = trim(urldecode($filter_attr_str));
$filter_attr_str = preg_match('/^[\d\.]+$/',$filter_attr_str) ? $filter_attr_str : '';
$filter_attr = empty($filter_attr_str) ? '' : explode('.', $filter_attr_str);

/* 排序、显示方式以及类型 */
$default_display_type = $_CFG['show_order_type'] == '1' ? 'list' : ($_CFG['show_order_type'] == '0' ? 'grid' : 'text');
$default_sort_order_method = $_CFG['sort_order_method'] == '0' ? 'DESC' : 'ASC';
$default_sort_order_type   = $_CFG['sort_order_type'] == '0' ? 'last_update' : ($_CFG['sort_order_type'] == '1' ? 'shop_price' : 'goods_id');

$sort  = (isset($_REQUEST['sort'])  && in_array(trim(strtolower($_REQUEST['sort'])), array('goods_id', 'shop_price', 'last_update','shownum'))) ? trim($_REQUEST['sort'])  : $default_sort_order_type;
$order = (isset($_REQUEST['order']) && in_array(trim(strtoupper($_REQUEST['order'])), array('ASC', 'DESC'))) ? trim($_REQUEST['order']) : $default_sort_order_method;
$display  = (isset($_REQUEST['display']) && in_array(trim(strtolower($_REQUEST['display'])), array('list', 'grid', 'text'))) ? trim($_REQUEST['display'])  : (isset($_COOKIE['m_display']) ? $_COOKIE['m_display'] : $default_display_type);
$display  = in_array($display, array('list', 'grid', 'text')) ? $display : 'text';
/*------------------------------------------------------ */
//-- PROCESSOR
/*------------------------------------------------------ */

/* 页面的缓存ID */
$cache_id = sprintf('%X', crc32(
    $cat_id . '-' .
    $display . '-' .
    $sort  .'-' .
    $order  .'-' .
    $page . '-' .
    $size . '-' .
    $_SESSION['user_rank'] . '-' .
    $_CFG['lang'] . '-' .
    $brand. '-' .
    $price_max . '-' .
    $price_min . '-' .
    $filter_attr_str
));
if(!empty($_REQUEST['is_menu']))
{
    $t1 = $CategoryController->getTopCategories(0);
    $smarty->assign('t1',              $t1);
    $smarty->assign('helps',            get_shop_help());
    assign_template();
    $smarty->display('category_list.html');
    exit;
}
if (!$smarty->is_cached('category.html', $cache_id))
{
    /* 如果页面没有被缓存则重新获取页面的内容 */
    
    $children = get_children($cat_id);
    
    $cat = get_cat_info($cat_id);   // 获得分类的相关信息
    $cat_desc = $cat['cat_desc'];
    if (!empty($cat))
    {
        $smarty->assign('keywords',    htmlspecialchars($cat['keywords']));
        $brandNames = $CategoryController->getDescBrands($cat_id);
        $len = count($brandNames);
        $brand_names = '';
        $insert_pos_zh = strpos($cat_desc,"買") ? strpos($cat_desc,"買") : strpos($cat_desc,"买");
        if($insert_pos_zh) {
            for ($x = 0; $x < $len; $x++) {
                $brandName = $brandNames[$x];
                if ($x < $len-1) {
                    if(strlen($brandName["b_brand_name"])!=0){
                        $brand_names .= $brandName["b_brand_name"] ."、";
                    } 
                    if(strlen($brandName["bl_brand_name"])!=0 && $brandName["bl_brand_name"] != $brandName["b_brand_name"]){
                        $brand_names .= $brandName["bl_brand_name"] ."、";
                    } 
                } else {
                    if(strlen($brandName["bl_brand_name"])!=0 && strlen($brandName["b_brand_name"])!=0){
                        if($brandName["bl_brand_name"] != $brandName["b_brand_name"]){
                            $brand_names .= $brandName["b_brand_name"] ."、";
                            $brand_names .= $brandName["bl_brand_name"] ." ";   
                        } else {
                            $brand_names .= $brandName["b_brand_name"] ." ";
                        } 
                    } else if (strlen($brandName["bl_brand_name"])!=0 && strlen($brandName["b_brand_name"])==0){
                        $brand_names .= $brandName["bl_brand_name"] ." ";
                    } else if (strlen($brandName["bl_brand_name"])==0 && strlen($brandName["b_brand_name"])!=0){
                        $brand_names .= $brandName["b_brand_name"] ." ";
                    }
                }
            }
            $insert_pos = $insert_pos_zh +4;
            $cat['cat_desc'] = substr_replace($cat_desc, $brand_names , $insert_pos,0);
        } else {
            for ($x = 0; $x < $len; $x++) {
                $brandName = $brandNames[$x];
                if ($x < $len-1) {
                    if(strlen($brandName["bl_brand_name"])!=0){
                        $brand_names .= $brandName["bl_brand_name"] .", ";
                    } 
                } else {
                    if(strlen($brandName["bl_brand_name"])!=0){
                        $brand_names_len = strlen($brand_names);
                        $brand_names = substr($brand_names,0,$brand_names_len-2);
                        $brand_names .= " and " . $brandName["bl_brand_name"] ." ";
                    } 
                }
            }
            $cat['cat_desc'] = $brand_names . $cat_desc;
        }
        $smarty->assign('description', htmlspecialchars($cat['cat_desc']));
        $smarty->assign('cat_style',   htmlspecialchars($cat['style']));
    }
    else
    {
        /* 如果分类不存在则返回首页 */
        header('Location: /');
        exit;
    }
    
    /* 赋值固定内容 */
    if ($brand > 0)
    {
        $brand_name = hw_get_brand_name($brand);
    }
    else
    {
        $brand_name = '';
    }
    $search_parameters = array('cid'=>$cat_id, 'bid'=>$brand, 'price_min'=>$price_min, 'price_max'=>$price_max, 'filter_attr'=>$filter_attr, 'filter_attr_str'=>$filter_attr_str,'sort'=>$sort,'order'=>$order);
    // Price Filter
    hw_assign_price_range($CategoryController->getRelatedWhere($cat_id), $_SESSION['user_rank']);

    // Brands
    $brands = $CategoryController->getRelatedBrands($cat_id, $search_parameters);
    $smarty->assign('brands', $brands);
    // Attrbute
    $attr_data = $AttrbuteController->get_attr_data($cat_id, $filter_attr, 'category',  $search_parameters);
    $ext = $attr_data['ext']; //商品查询条件扩展
    $all_attr_list = $attr_data['all_attr_list'];
    $selected_value = $attr_data['selected_value'];
    if(!empty($brand_name)) array_unshift($selected_value, $brand_name);
    $smarty->assign('filter_attr_list',  $all_attr_list);
    $smarty->assign('selected_value',  $selected_value);

    assign_template('c', array($cat_id), 'category');
    
    $position = assign_mobile_ur_here($cat_id, $brand_name);
    $smarty->assign('page_title',       $position['title']);    // 页面标题
    $smarty->assign('ur_here',          $position['ur_here']);  // 当前位置
    $cat_list = cat_list(0, 0, false, 0, false);
    $group_cat_list = $CategoryController->groupCat($cat_list);
    $t1 = $CategoryController->getTopCategories(0);
    $smarty->assign('t1',              $t1);
    
    //$smarty->assign('categories',       get_categories_tree($cat_id)); // 分类树
    $smarty->assign('helps',            get_shop_help());              // 网店帮助
    //$smarty->assign('top_goods',        get_top10());                  // 销售排行
    //$smarty->assign('show_marketprice', $_CFG['show_marketprice']);
    $smarty->assign('category',         $cat);
    $smarty->assign('category_id',      $cat_id);
    $smarty->assign('category_name',    $cat['cat_name']);
    
    $smarty->assign('price_max',        $price_max);
    $smarty->assign('price_min',        $price_min);
    $smarty->assign('filter_attr',      $filter_attr_str);
    $smarty->assign('feed_url',         ($_CFG['rewrite'] == 1) ? "feed-c$cat_id.xml" : 'feed.php?cat=' . $cat_id); // RSS URL
    
    $smarty->assign('brand_id',         $brand);
    $smarty->assign('brandname',        $brand_name);
    
    $smarty->assign('sort',             $sort);
    $smarty->assign('order',            $order);
    
    $smarty->assign('data_dir',         DATA_DIR);
    
    // $smarty->assign('promotion_info',   get_promotion_info());
    
    // /* 调查 */
    // $vote = get_vote();
    // if (!empty($vote))
    // {
    //     $smarty->assign('vote_id',      $vote['id']);
    //     $smarty->assign('vote',         $vote['content']);
    // }
    // $smarty->assign('new_goods',        get_category_recommend_goods('new', $children, $brand, $price_min, $price_max, $ext));
    // $smarty->assign('best_goods',       get_category_recommend_goods('best', $children, $brand, $price_min, $price_max, $ext));
    // $smarty->assign('promotion_goods',  get_category_recommend_goods('promote', $children, $brand, $price_min, $price_max, $ext));
    // $smarty->assign('hot_goods',        get_category_recommend_goods('hot', $children, $brand, $price_min, $price_max, $ext));
    
    $smarty->assign('subcategories',    hw_get_subcategories($cat_id));
    
    $count = hw_get_cagtegory_goods_count($children, $brand, $price_min, $price_max, $ext, 0);
    $max_page = ($count> 0) ? ceil($count / $size) : 1;
    if ($page > $max_page)
    {
        $page = $max_page;
    }
    $goodslist = hw_category_get_goods($children, $brand, $price_min, $price_max, $ext, $size, $page, $sort, $order, 0, $display, [], 0);
    $goods_id_arr = array();
    foreach($goodslist as $val){
        $goods_id_arr[] = $val['goods_id'];
    }

    //get favourable list
    $favourable_arr = $goodsController->product_list_get_favourable_info($_SESSION['user_rank'], $goods_id_arr);

    //get package list
    $package_arr = $goodsController->product_list_get_package_info($goods_id_arr);

    //get topic list
    $topic_arr = $goodsController->product_list_get_topic_info($goods_id_arr);

    $smarty->assign('goods_list',       $goodslist);
    $smarty->assign('favourable_list',  $favourable_arr);
    $smarty->assign('package_list',     $package_arr);
    $smarty->assign('topic_list',      $topic_arr);
    $smarty->assign('script_name',      'category');
    $smarty->assign('display',      $display);
    foreach ($brands as $row) {
        if ($row['brand_id'] = $brand) {
            $bperma = $row['bperma'];
        }
    }
    assign_pager('category',            $cat_id, $count, $size, $sort, $order, $page, '', $brand, $price_min, $price_max, $display, $filter_attr_str, '', '', ['cperma' => $cat['cperma'], 'bperma' => $bperma]); // 分页
    
    $sort_names = array(
        'last_update' => _L('productlist_sort_last_update', '預設排序'),
        'shownum' => _L('productlist_sort_shownum', '銷量排序'),
        'goods_id' => _L('productlist_sort_goods_id', '上架時間'),
        'shop_price' => _L('productlist_sort_price', '價格排序')
    );
    $sort_name = isset($sort_names[$sort]) ? $sort_names[$sort] : _L('productlist_sort_by', '排序方式');
    $smarty->assign('sort_name', $sort_name);
    $new_order = $order == "DESC" ? "ASC" : "DESC";
    $sort_links = array(
        'last_update' => ['url'=>build_uri('category', array('cid' => $cat_id, 'cperma' => $cat['cperma'], 'sort' => 'last_update', 'order' => 'DESC', 'page' => $page,'bid'=>$brand, 'bperma' => $bperma, 'price_min'=>$price_min, 'price_max'=>$price_max, 'filter_attr'=>$filter_attr_str, 'cname'=>$cat['cat_name']))],
        'shownum' => ['url'=>build_uri('category', array('cid' => $cat_id, 'cperma' => $cat['cperma'], 'sort' => 'shownum', 'order' => 'DESC', 'page' => $page,'bid'=>$brand, 'bperma' => $bperma, 'price_min'=>$price_min, 'price_max'=>$price_max, 'filter_attr'=>$filter_attr_str, 'cname'=>$cat['cat_name']))],
        'goods_id' => ['url'=>build_uri('category', array('cid' => $cat_id, 'cperma' => $cat['cperma'], 'sort' => 'goods_id', 'order' => 'DESC', 'page' => $page,'bid'=>$brand, 'bperma' => $bperma, 'price_min'=>$price_min, 'price_max'=>$price_max, 'filter_attr'=>$filter_attr_str, 'cname'=>$cat['cat_name']))],
        'shop_price' => ['url'=>build_uri('category', array('cid' => $cat_id, 'cperma' => $cat['cperma'], 'sort' => 'shop_price', 'order' => 'DESC', 'page' => $page,'bid'=>$brand, 'bperma' => $bperma, 'price_min'=>$price_min, 'price_max'=>$price_max, 'filter_attr'=>$filter_attr_str, 'cname'=>$cat['cat_name']))],
    );
    foreach ($sort_links as $key => $value) {
        if($sort == $key){
            $sort_links[$key]['order'] = $order;
            $sort_links[$key]['url']   = build_uri('category', array('cid' => $cat_id, 'cperma' => $cat['cperma'], 'sort' => $key, 'order' => $new_order, 'page' => $page,'bid'=>$brand, 'bperma' => $bperma, 'price_min'=>$price_min, 'price_max'=>$price_max, 'filter_attr'=>$filter_attr_str, 'cname'=>$cat['cat_name']));
        }
    }
    $smarty->assign('sort_links', $sort_links);
    $level = intval($cat_list[$cat_id]['level']) + 1;
    $smarty->assign('level',           $level);
    switch($level){
        case 1:
            // Hot Category
	        $smarty->assign('hot_category',          $CategoryController->getHotCategory(7, $cat_id));
            // Cat session
	        $smarty->assign('cat_session',           $group_cat_list[$cat_id]);
            break;
        case 2:
            // Cat session
	        $smarty->assign('cat_session',           $group_cat_list[$cat['parent_id']]['children'][$cat_id]);
            break;
    };

    assign_dynamic('category'); // 动态内容
}

$catlist = array();
foreach(get_parent_cats($cat_id) as $k=>$v)
{
    $catlist[] = $v['cat_id'];
}
$smarty->assign('catlist',  $catlist);           // set parent categorey list

$smarty->display('category.html', $cache_id);

/*------------------------------------------------------ */
//-- PRIVATE FUNCTION
/*------------------------------------------------------ */

/**
 * 获得分类的信息
 *
 * @param   integer $cat_id
 *
 * @return  void
 */
function get_cat_info($cat_id)
{
    return $GLOBALS['db']->getRow("SELECT " .
        "IFNULL(cl.cat_name, c.cat_name) as cat_name, " .
        "IFNULL(cl.keywords, c.keywords) as keywords, " .
        "IFNULL(cl.cat_desc, c.cat_desc) as cat_desc, " .
        "style, grade, filter_attr, parent_id, cperma " .
        "FROM " . $GLOBALS['ecs']->table('category') . " as c " .
        "LEFT JOIN" . $GLOBALS['ecs']->table('category_lang') . " as cl ON c.cat_id = cl.cat_id AND cl.lang = '" . $GLOBALS['_CFG']['lang']  . "' " .
        "LEFT JOIN (SELECT id, table_name, lang, perma_link as cperma FROM " . $GLOBALS['ecs']->table('perma_link') . ") cpl ON cpl.id = c.cat_id  AND cpl.table_name = 'category' AND cpl.lang = '" . $GLOBALS['_CFG']['lang'] . "' " .
        " WHERE c.cat_id = '$cat_id'");
}

?>
