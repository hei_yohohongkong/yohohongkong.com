<?php

/**
 * ECSHOP 提交用户评论
 * ============================================================================
 * * 版权所有 2005-2012 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liubo $
 * $Id: comment.php 17217 2011-01-19 06:29:08Z liubo $
*/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require(ROOT_PATH . 'includes/cls_json.php');
$commentController = new Yoho\cms\Controller\CommentController();
if (!isset($_REQUEST['cmt']) && !isset($_REQUEST['act']))
{
    /* 只有在没有提交评论内容以及没有act的情况下才跳转 */
    ecs_header("Location: ./\n");
    exit;
}
$_REQUEST['cmt'] = isset($_REQUEST['cmt']) ? json_str_iconv($_REQUEST['cmt']) : '';

$json   = new JSON;
$result = array('error' => '', 'message' => '', 'content' => '');

if (empty($_REQUEST['act']) || $_REQUEST['act'] == 'send_comment')
{
    /*
     * act 参数为空
     * 默认为添加评论内容
     */
	 
	
	
    $cmt  = $_REQUEST;
	
    $cmt['page'] = 1;
    $cmt['id']   = !empty($cmt['id'])   ? intval($cmt['id']) : 0;
    $cmt['type'] = !empty($cmt['type']) ? intval($cmt['type']) : 0;
    $cmt['lei'] = !empty($cmt['lei']) ? intval($cmt['lei']) : 1;

    if (empty($cmt) || !isset($cmt['type']) || !isset($cmt['id']))
    { 
	//echo '{"status":0,"message":$_LANG[invalid_comments}';
        
    }
    
    else
    {
        if ((intval($_CFG['captcha']) & CAPTCHA_COMMENT) && gd_version() > 0)
        {
            /* 检查验证码 */
            include_once('includes/cls_captcha.php');

            $validator = new captcha();
            if (!$validator->check_word($cmt['captcha']))
            { 
                //$result['message'] = $_LANG['invalid_captcha'];
					echo '{"status":2}'; // 2验证码问题
            }
            else
            {
                $factor = intval($_CFG['comment_factor']);
                if ($cmt['type'] == 0 && $factor > 0)
                {
                    /* 只有商品才检查评论条件 */
                    switch ($factor)
                    {
                        case COMMENT_LOGIN :
                            if ($_SESSION['user_id'] == 0)
                            { 
                                $result['message'] = $_LANG['comment_login'];
                            }
                            break;

                        case COMMENT_CUSTOM :
                            if ($_SESSION['user_id'] > 0)
                            {
                                $sql = "SELECT o.order_id FROM " . $ecs->table('order_info') . " AS o ".
                                       " WHERE user_id = '" . $_SESSION['user_id'] . "'".
                                       " AND (o.order_status = '" . OS_CONFIRMED . "' or o.order_status = '" . OS_SPLITED . "') ".
                                       " AND (o.pay_status = '" . PS_PAYED . "' OR o.pay_status = '" . PS_PAYING . "') ".
                                       " AND (o.shipping_status = '" . SS_SHIPPED . "' OR o.shipping_status = '" . SS_RECEIVED . "') ".
                                       " LIMIT 1";


                                 $tmp = $db->getOne($sql);
                                 if (empty($tmp))
                                 { 
                                    $result['message'] = $_LANG['comment_custom'];
                                 }
                            }
                            else
                            { 
                                $result['message'] = $_LANG['comment_custom'];
                            }
                            break;
                        case COMMENT_BOUGHT :
                        if ($_SESSION['user_id'] > 0)
                        {
                            $sql = "SELECT COUNT(o.order_id)".
                                " FROM " . $ecs->table('order_info'). " AS o, ".
                                $ecs->table('order_goods') . " AS og ".
                                " WHERE o.order_id = og.order_id".
                                " AND o.user_id = '" . $_SESSION['user_id'] . "'".
                                " AND og.goods_id = '" . $cmt['id'] . "'".
                                " AND og.delivery_qty > 0 ".
                                " AND (o.order_status = '" . OS_CONFIRMED . "' or o.order_status = '" . OS_SPLITED . "') ".
                                " AND (o.pay_status = '" . PS_PAYED . "' OR o.pay_status = '" . PS_PAYING . "') ".
                                " AND (o.shipping_status = '" . SS_SHIPPED . "' OR o.shipping_status = '" . SS_RECEIVED . "' OR o.shipping_status = '" . SS_SHIPPED_PART . "') ".
                                " LIMIT 1";
                            $bought_count = $db->getOne($sql);
                            if (!$bought_count)
                            {
                                $result['error'] = 1;
                                $result['message'] = $_LANG['comment_brought'];
                            }else{
                                $sql = "SELECT COUNT(comment_id) FROM " . $ecs->table('comment') .
                                    " WHERE user_id = '" . $_SESSION['user_id'] . "'".
                                    " AND id_value= '" . $cmt['id'] . "'".
                                    " LIMIT 1";
                                $comment_count = $db->getOne($sql);
                                if($comment_count >= $bought_count){
                                    $result['error'] = 1;
                                    $result['message'] = $_LANG['comment_repeat'];
                                }
                            }
                        } else {
                            $result['error'] = 1;
                            $result['message'] = $_LANG['comment_login'];
                        }
                    }
                }

                /* 无错误就保存留言 */
                if (empty($result['error']))
                {
                    $cmt['image'] = $_FILES;
					$result = $commentController->add_comment($cmt);
                    $_SESSION['send_time'] = $cur_time;
                    if($result['error']) echo json_encode($result);
                    else echo json_encode(['status'=>1, 'cmt'=>$cmt]);
                    exit();
                } else {
                    echo json_encode($result);
                }
            }
        }
        else
        {
            /* 没有验证码时，用时间来限制机器人发帖或恶意发评论 */
            if (!isset($_SESSION['send_time']))
            {
                $_SESSION['send_time'] = 0;
            }

            $cur_time = gmtime();
            if (($cur_time - $_SESSION['send_time']) < 30) // 小于30秒禁止发评论
            {
				echo '{"status":3}';  //3问答间隔小于30秒
				
               // $result['message'] = $_LANG['cmt_spam_warning'];
            }
            else
            {
                $factor = intval($_CFG['comment_factor']);
                if ($cmt['type'] == 0 && $factor > 0)
                {
                    /* 只有商品才检查评论条件 */
                    switch ($factor)
                    {
                        case COMMENT_LOGIN :
                            if ($_SESSION['user_id'] == 0)
                            { 
                                $result['message'] = $_LANG['comment_login'];
                            }
                            break;

                        case COMMENT_CUSTOM :
                            if ($_SESSION['user_id'] > 0)
                            {
                                $sql = "SELECT o.order_id FROM " . $ecs->table('order_info') . " AS o ".
                                       " WHERE user_id = '" . $_SESSION['user_id'] . "'".
                                       " AND (o.order_status = '" . OS_CONFIRMED . "' or o.order_status = '" . OS_SPLITED . "') ".
                                       " AND (o.pay_status = '" . PS_PAYED . "' OR o.pay_status = '" . PS_PAYING . "') ".
                                       " AND (o.shipping_status = '" . SS_SHIPPED . "' OR o.shipping_status = '" . SS_RECEIVED . "') ".
                                       " LIMIT 1";


                                 $tmp = $db->getOne($sql);
                                 if (empty($tmp))
                                 { 
                                    $result['message'] = $_LANG['comment_custom'];
                                 }
                            }
                            else
                            { 
                                $result['message'] = $_LANG['comment_custom'];
                            }
                            break;

                        case COMMENT_BOUGHT :
                            if ($_SESSION['user_id'] > 0)
                            {
                                $sql = "SELECT COUNT(o.order_id)".
                                    " FROM " . $ecs->table('order_info'). " AS o, ".
                                    $ecs->table('order_goods') . " AS og ".
                                    " WHERE o.order_id = og.order_id".
                                    " AND o.user_id = '" . $_SESSION['user_id'] . "'".
                                    " AND og.goods_id = '" . $cmt['id'] . "'".
                                    " AND og.delivery_qty > 0 ".
                                    " AND (o.order_status = '" . OS_CONFIRMED . "' or o.order_status = '" . OS_SPLITED . "') ".
                                    " AND (o.pay_status = '" . PS_PAYED . "' OR o.pay_status = '" . PS_PAYING . "') ".
                                    " AND (o.shipping_status = '" . SS_SHIPPED . "' OR o.shipping_status = '" . SS_RECEIVED . "') ".
                                    " LIMIT 1";
                                $bought_count = $db->getOne($sql);
                                if (!$bought_count)
                                {
                                    $result['error'] = 1;
                                    $result['message'] = $_LANG['comment_brought'];
                                }else{
                                    $sql = "SELECT COUNT(comment_id) FROM " . $ecs->table('comment') .
                                        " WHERE user_id = '" . $_SESSION['user_id'] . "'".
                                        " AND id_value= '" . $cmt['id'] . "'".
                                        " LIMIT 1";
                                    $comment_count = $db->getOne($sql);
                                    if($comment_count >= $bought_count){
                                        $result['error'] = 1;
                                        $result['message'] = $_LANG['comment_repeat'];
                                    }
                                }
                            } else {
                                $result['error'] = 1;
                                $result['message'] = $_LANG['comment_login'];
                            }
                    }
                }
                /* 无错误就保存留言 */
                if (empty($result['error']))
                {
                    $cmt['image'] = $_FILES;
                    $result = $commentController->add_comment($cmt);
                    $_SESSION['send_time'] = $cur_time;
                    if($result['error']) echo json_encode($result);
                    else echo json_encode(['status'=>1, 'cmt'=>$cmt]);
                    exit();
                } else {
                    echo json_encode($result);
                    exit();
                }
            }
        }
    }
}
else
{
    /*
     * act 参数不为空
     * 默认为评论内容列表
     * 根据 _GET 创建一个静态对象
	 新增L 判断是评论还是问答还是其他
     */
    $cmt['id']   = !empty($_GET['id'])   ? intval($_GET['id'])   : 0;
    $cmt['type'] = !empty($_GET['type']) ? intval($_GET['type']) : 0;
    $cmt['page'] = isset($_GET['page'])   && intval($_GET['page'])  > 0 ? intval($_GET['page'])  : 1;
    $cmt['lei'] = !empty($_GET['lei']) ? intval($_GET['lei']) : 1;
    $comments = $commentController->assign_comment($cmt['id'], $cmt['type'], $cmt['page'], $cmt['lei']);

    if ($comments['error'] == 0)
    {
        $can_comment = $commentController->user_can_comment($commentController::COMMENT_TYPE_GOODS, $cmt['id']);
        /* 验证码相关设置 */
        if ((intval($GLOBALS['_CFG']['captcha']) & CAPTCHA_COMMENT) && gd_version() > 0)
        {
            $smarty->assign('enabled_captcha', 1);
            $smarty->assign('rand', mt_rand());
        }
        $smarty->assign('username',     stripslashes($_SESSION['user_name']));
        $smarty->assign('email',        $_SESSION['email']);
        $smarty->assign('comment_type', $cmt['type']);
        $smarty->assign('id',           $cmt['id']);
        // comment
        $smarty->assign('can_comment', $can_comment['can_comment']);
        $smarty->assign('comments',     $comments['comments']);
        $smarty->assign('count',     $comments['total']);
        $smarty->assign('pager',        $comments['pager']);
        $smarty->assign('lei',        $cmt['lei']);

        /* 验证码相关设置 */
        if ((intval($_CFG['captcha']) & CAPTCHA_COMMENT) && gd_version() > 0)
        {
            $smarty->assign('enabled_captcha', 1);
            $smarty->assign('rand', mt_rand());
        }

    // $result['message'] = $_CFG['comment_check'] ? $_LANG['cmt_submit_wait'] : $_LANG['cmt_submit_done'];
        

    
        $result['content'] = $smarty->fetch("library/comments_list.lbi");

        if (!empty($_REQUEST['act']))
        {
            foreach($result as $color)
            {
                echo $color;
            }
        }

    }
}




//echo $json->encode($result); //sbsn 直接输出HTML

?>