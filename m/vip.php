<?php

/***
* vip.php
* by Anthony@YOHO 20170721
*
* YOHO Hong Kong VIP page
***/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

if ((DEBUG_MODE & 2) != 2)
{
    $smarty->caching = true;
}


$user_id = $_SESSION['user_id'];
$user_rank = $_SESSION['user_rank'];
$rank_points = $_SESSION['rank_points'];
$vipController = new Yoho\cms\Controller\VipController();
/* 初始化分页信息 */
$page = !empty($_REQUEST['page']) && intval($_REQUEST['page']) > 0 ? intval($_REQUEST['page']) : 1;
$size = !empty($_CFG['page_size']) && intval($_CFG['page_size']) > 0 ? intval($_CFG['page_size']) : 10;
$cat_id = !empty($_REQUEST['cid']) && intval($_REQUEST['cid']) > 0 ? intval($_REQUEST['cid']) : 0;
$brand_id = !empty($_REQUEST['bid']) && intval($_REQUEST['bid']) > 0 ? intval($_REQUEST['bid']) : 0;
$price_min = isset($_REQUEST['price_min']) && intval($_REQUEST['price_min']) > 0 ? intval($_REQUEST['price_min']) : 0;
$price_max = isset($_REQUEST['price_max']) && intval($_REQUEST['price_max']) > 0 ? intval($_REQUEST['price_max']) : 0;

/* 排序、显示方式以及类型 */
$default_display_type = $_CFG['show_order_type'] == '0' ? 'list' : ($_CFG['show_order_type'] == '1' ? 'grid' : 'text');
$default_sort_order_method = $_CFG['sort_order_method'] == '0' ? 'DESC' : 'ASC';
$default_sort_order_type   = $_CFG['sort_order_type'] == '0' ? 'last_update' : ($_CFG['sort_order_type'] == '1' ? 'shop_price' : 'goods_id');

$sort = (isset($_REQUEST['sort']) && in_array(trim(strtolower($_REQUEST['sort'])), array('goods_id', 'shop_price', 'last_update', 'shownum'))) ? trim($_REQUEST['sort']) : $default_sort_order_type;
$order = (isset($_REQUEST['order']) && in_array(trim(strtoupper($_REQUEST['order'])), array('ASC', 'DESC'))) ? trim($_REQUEST['order']) : $default_sort_order_method;
$display = (isset($_REQUEST['display']) && in_array(trim(strtolower($_REQUEST['display'])), array('list', 'grid', 'text'))) ? trim($_REQUEST['display']) : (isset($_COOKIE['m_display']) ? $_COOKIE['m_display'] : $default_display_type);
//$display  = in_array($display, array('list', 'grid', 'text')) ? $display : 'text';
$display = in_array($display, array('list', 'grid')) ? $display : 'grid';
$vip = $vipController->get_vip_info_with_user($user_id, $user_rank, $rank_points);
setcookie('m_display', $display, gmtime() + 86400 * 7);

/* ------------------------------------------------------ */
//-- PROCESSOR
/* ------------------------------------------------------ */

if (empty($vip)) {
    header('Location: /vip');
    exit;
}

$smarty->assign('data_dir', DATA_DIR);

/* 赋值固定内容 */
assign_template();
$smarty->assign('page_title', $_LANG['vip_offer']);  // 页面标题
$smarty->assign('ur_here',    $_LANG['vip_offer']);  // 当前位置
$CategoryController = new Yoho\cms\Controller\CategoryController();
$t1 = $CategoryController->getTopCategories(0);
$smarty->assign('t1',              $t1);
$smarty->assign('helps', get_shop_help());              // 网店帮助

$goods_list_info = $vipController->get_vip_goods($cat_id, $brand_id, $price_min, $price_max, $ext, $size, $page, $sort, $order);
$sort_names = array(
    'last_update' => _L('productlist_sort_last_update', '預設排序'),
    'shownum' => _L('productlist_sort_shownum', '銷量排序'),
    'goods_id' => _L('productlist_sort_goods_id', '上架時間'),
    'shop_price' => _L('productlist_sort_price', '價格排序')
);
$sort_name = isset($sort_names[$sort]) ? $sort_names[$sort] : _L('productlist_sort_by', '排序方式');
$smarty->assign('sort_name', $sort_name);

$new_order = $order == "DESC" ? "ASC" : "DESC";
$sort_links = array(
    'last_update' => ['url'=>build_uri('vip', array('cid' => $cat_id, 'sort' => 'last_update', 'order' => 'DESC', 'page' => $page,'bid'=>$brand_id, 'price_min'=>$price_min, 'price_max'=>$price_max, 'filter_attr'=>$filter_attr_str))],
    'shownum' => ['url'=>build_uri('vip', array('cid' => $cat_id, 'sort' => 'shownum', 'order' => 'DESC', 'page' => $page,'bid'=>$brand_id, 'price_min'=>$price_min, 'price_max'=>$price_max, 'filter_attr'=>$filter_attr_str))],
    'goods_id' => ['url'=>build_uri('vip', array('cid' => $cat_id, 'sort' => 'goods_id', 'order' => 'DESC', 'page' => $page,'bid'=>$brand_id, 'price_min'=>$price_min, 'price_max'=>$price_max, 'filter_attr'=>$filter_attr_str))],
    'shop_price' => ['url'=>build_uri('vip', array('cid' => $cat_id, 'sort' => 'shop_price', 'order' => 'DESC', 'page' => $page,'bid'=>$brand_id, 'price_min'=>$price_min, 'price_max'=>$price_max, 'filter_attr'=>$filter_attr_str))],
);
foreach ($sort_links as $key => $value) {
    if($sort == $key){
        $sort_links[$key]['order'] = $order;
        $sort_links[$key]['url']   = build_uri('vip', array('cid' => $cat_id, 'sort' => $key, 'order' => $new_order, 'page' => $page,'bid'=>$brand_id, 'price_min'=>$price_min, 'price_max'=>$price_max, 'filter_attr'=>$filter_attr_str));
    }
}
$smarty->assign('sort_links', $sort_links);
$smarty->assign('goods_list', $goods_list_info['goodslist']);
$smarty->assign('script_name', 'vip');
$smarty->assign('display', $display);
$smarty->assign('sort', $sort);
$smarty->assign('order', $order);
$smarty->assign('vip', $vip);
$smarty->assign('rank_points', $rank_points);
$smarty->assign('vip_page', true);
unset($goods_list_info['left_cat_menu']['count']);
$smarty->assign('filter_cat_menu', $goods_list_info['left_cat_menu']);
$smarty->assign('left_cat_menu_count',$goods_list_info['left_cat_menu_count']);
$smarty->assign('brands',           $goods_list_info['brands']);
$smarty->assign('brand_id',        $goods_list_info['brand_id']);
// Price Filter
$smarty->assign('price_min', $price_min);
$smarty->assign('price_max', $price_max);

assign_pager('vip', $cat_id, $goods_list_info['count'], $size, $sort, $order, $page, $ext, $brand_id, $price_min, $price_max, $display, $is_promote); // 分页
assign_dynamic('vip'); // 动态内容


$smarty->display('vip.html');
