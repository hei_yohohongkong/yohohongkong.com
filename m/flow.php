<?php

/**
 * ECSHOP 购物流程
 * ============================================================================
 * 版权所有 2005-2010 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: douqinghua $
 * $Id: flow.php 17218 2011-01-24 04:10:41Z douqinghua $
 */

define('IN_ECS', true);
define('FORCE_HTTPS', true);
require(dirname(__FILE__) . '/includes/init.php');
require(ROOT_PATH . 'includes/lib_order.php');
require_once(ROOT_PATH . ADMIN_PATH . '/includes/lib_accounting.php');
$userController = new Yoho\cms\Controller\UserController();
$paymentController = new Yoho\cms\Controller\PaymentController();
$orderController = new Yoho\cms\Controller\OrderController();
$feedController       = new Yoho\cms\Controller\FeedController();
$addressController = new Yoho\cms\Controller\AddressController();
$rspController = new Yoho\cms\Controller\RspController();
$userRankController = new Yoho\cms\Controller\UserRankController();
$flashdealController = new \Yoho\cms\Controller\FlashdealController();
$order_fee_platform = $orderController::ORDER_FEE_PLATFORM_WEB;

$actg = $paymentController->actg;
$actgHooks = $paymentController->actgHooks;

//Blocking Wholesale Customer
if(in_array(@$_SESSION['user_id'],$userController->getWholesaleUserIds()))
    show_message('批發客戶請直接聯絡營業員訂購');
// Updated to use yoho language pack, shouldn't need these old language files
// /* 载入语言文件 */
// require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/user.php');
// require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/shopping_flow.php');

/*------------------------------------------------------ */
//-- INPUT
/*------------------------------------------------------ */

if (!isset($_REQUEST['step']))
{
    $_REQUEST['step'] = "cart";
}

/*------------------------------------------------------ */
//-- PROCESSOR
/*------------------------------------------------------ */

switch ($_REQUEST['step'])
{
    case 'checkout':
    case 'consignee':
        $page_title = _L('flow_title_checkout', '結帳');
        break;
    case 'done':
        $page_title = _L('flow_title_done', '付款');
    case 'cart':
    default:
        $page_title = _L('flow_title_cart', '購物車');
}

assign_template();
assign_dynamic('flow');
$position = assign_ur_here(0, $page_title);
$smarty->assign('page_title',       $position['title']);    // 页面标题
$smarty->assign('ur_here',          $position['ur_here']);  // 当前位置

//$smarty->assign('categories',       get_categories_tree()); // 分类树
$smarty->assign('helps',            get_shop_help());       // 网店帮助
$smarty->assign('lang',             $_LANG);
$smarty->assign('show_marketprice', $_CFG['show_marketprice']);
$smarty->assign('data_dir',         DATA_DIR);       // 数据目录
$CategoryController = new Yoho\cms\Controller\CategoryController();
$t1 = $CategoryController->getTopCategories(0);
$smarty->assign('t1',              $t1);

/*------------------------------------------------------ */
//-- 添加商品到购物车
/*------------------------------------------------------ */
if ($_REQUEST['step'] == 'add_to_cart')
{
    include_once('includes/cls_json.php');
    $_POST['goods']=strip_tags(urldecode($_POST['goods']));
    $_POST['goods'] = json_str_iconv($_POST['goods']);

    if (!empty($_REQUEST['goods_id']) && empty($_POST['goods']))
    {
        if (!is_numeric($_REQUEST['goods_id']) || intval($_REQUEST['goods_id']) <= 0)
        {
            header('Location: /cart');
        }
        $goods_id = intval($_REQUEST['goods_id']);
        exit;
    }

    $result = array('error' => 0, 'message' => '', 'content' => '', 'goods_id' => '');
    $json  = new JSON;

    if (empty($_POST['goods']))
    {
        $result['error'] = 1;
        die($json->encode($result));
    }

    $goods = $json->decode($_POST['goods']);

    /* 检查：如果商品有规格，而post的数据没有规格，把商品的规格属性通过JSON传到前台 */
    if (empty($goods->spec) AND empty($goods->quick))
    {
        $sql = "SELECT a.attr_id, a.attr_name, a.attr_type, ".
            "g.goods_attr_id, g.attr_value, g.attr_price " .
        'FROM ' . $GLOBALS['ecs']->table('goods_attr') . ' AS g ' .
        'LEFT JOIN ' . $GLOBALS['ecs']->table('attribute') . ' AS a ON a.attr_id = g.attr_id ' .
        "WHERE a.attr_type != 0 AND g.goods_id = '" . $goods->goods_id . "' " .
        'ORDER BY a.sort_order, g.attr_price, g.goods_attr_id';

        $res = $GLOBALS['db']->getAll($sql);

        if (!empty($res))
        {
            $spe_arr = array();
            foreach ($res AS $row)
            {
                $spe_arr[$row['attr_id']]['attr_type'] = $row['attr_type'];
                $spe_arr[$row['attr_id']]['name']     = $row['attr_name'];
                $spe_arr[$row['attr_id']]['attr_id']     = $row['attr_id'];
                $spe_arr[$row['attr_id']]['values'][] = array(
                                                            'label'        => $row['attr_value'],
                                                            'price'        => $row['attr_price'],
                                                            'format_price' => price_format($row['attr_price'], false),
                                                            'id'           => $row['goods_attr_id']);
            }
            $i = 0;
            $spe_array = array();
            foreach ($spe_arr AS $row)
            {
                $spe_array[]=$row;
            }
            $result['error']   = ERR_NEED_SELECT_ATTR;
            $result['goods_id'] = $goods->goods_id;
            $result['parent'] = $goods->parent;
            $result['message'] = $spe_array;

            die($json->encode($result));
        }
    }

    /* 更新：如果是一步购物，先清空购物车 */
    if ($_CFG['one_step_buy'] == '1')
    {
        clear_cart();
    }

    /* 检查：商品数量是否合法 */
    if (!is_numeric($goods->number) || intval($goods->number) <= 0)
    {
        $result['error']   = 1;
        $result['message'] = $_LANG['invalid_number'];
    }
    /* 更新：购物车 */
    else
    {
        // 更新：添加到购物车
        if (addto_cart($goods->goods_id, $goods->number, $goods->spec, $goods->parent))
        {
            if ($_CFG['cart_confirm'] > 2)
            {
                $result['message'] = '';
            }
            else
            {
                $result['message'] = $_CFG['cart_confirm'] == 1 ? $_LANG['addto_cart_success_1'] : $_LANG['addto_cart_success_2'];
            }

            $result['content'] = insert_cart_info();
            $result['one_step_buy'] = $_CFG['one_step_buy'];
        }
        else
        {
            $result['message']  = $err->last_message();
            $result['error']    = $err->error_no;
            $result['goods_id'] = stripslashes($goods->goods_id);
            if (is_array($goods->spec))
            {
                $result['product_spec'] = implode(',', $goods->spec);
            }
            else
            {
                $result['product_spec'] = $goods->spec;
            }
        }
    }

    $result['confirm_type'] = !empty($_CFG['cart_confirm']) ? $_CFG['cart_confirm'] : 2;
    die($json->encode($result));
}
elseif ($_REQUEST['step'] == 'link_buy')
{
    $goods_id = intval($_GET['goods_id']);
    $flashdealController = new \Yoho\cms\Controller\FlashdealController();
    $flashdeal_id = $flashdealController->check_goods_is_flashdeal($goods_id);
    if($flashdeal_id > 0) return false;
    else if (!cart_goods_exists($goods_id,array()))
    {
        addto_cart($goods_id);
    }
    header('Location: /cart');
    exit;
}
elseif ($_REQUEST['step'] == 'login')
{
	if($_SESSION['user_id'] > 0  )  //在登录页面加一个判断是否已经登录 如果登录了直接跳转到提交订单的页面 LY
	{
		header('Location: /checkout');
		exit;
	}
	// howang: redirect to login page and we're done here
	else
	{
		$_SESSION['from_checkout'] = true;
		//header('Location: user.php?act=login');
		//exit;
	}

    include_once('languages/'. $_CFG['lang']. '/user.php');

    /*
     * 用户登录注册
     */
    if ($_SERVER['REQUEST_METHOD'] == 'GET')
    {
        $smarty->assign('anonymous_buy', $_CFG['anonymous_buy']);

        /* 检查是否有赠品，如果有提示登录后重新选择赠品 */
        $sql = "SELECT COUNT(*) FROM " . $ecs->table('cart') .
                " WHERE session_id = '" . SESS_ID . "' AND is_gift > 0";
        if ($db->getOne($sql) > 0)
        {
            $smarty->assign('need_rechoose_gift', 1);
        }

        /* 检查是否需要注册码 */
        $captcha = intval($_CFG['captcha']);
        if (($captcha & CAPTCHA_LOGIN) && (!($captcha & CAPTCHA_LOGIN_FAIL) || (($captcha & CAPTCHA_LOGIN_FAIL) && $_SESSION['login_fail'] > 2)) && gd_version() > 0)
        {
            $smarty->assign('enabled_login_captcha', 1);
            $smarty->assign('rand', mt_rand());
        }
        if ($captcha & CAPTCHA_REGISTER)
        {
            $smarty->assign('enabled_register_captcha', 1);
            $smarty->assign('rand', mt_rand());
        }
    }
    else
    {
        include_once('includes/lib_passport.php');
        if (!empty($_POST['act']) && $_POST['act'] == 'signin')
        {
            $captcha = intval($_CFG['captcha']);
            if (($captcha & CAPTCHA_LOGIN) && (!($captcha & CAPTCHA_LOGIN_FAIL) || (($captcha & CAPTCHA_LOGIN_FAIL) && $_SESSION['login_fail'] > 2)) && gd_version() > 0)
            {
                if (empty($_POST['captcha']))
                {
                    show_mobile_message($_LANG['invalid_captcha']);
                }

                /* 检查验证码 */
                include_once('includes/cls_captcha.php');

                $validator = new captcha();
                $validator->session_word = 'captcha_login';
                if (!$validator->check_word($_POST['captcha']))
                {
                    show_mobile_message($_LANG['invalid_captcha']);
                }
            }

            if ($user->login($_POST['username'], $_POST['password'],isset($_POST['remember'])))
            {
                update_user_info();  //更新用户信息
                recalculate_price(); // 重新计算购物车中的商品价格

                /* 检查购物车中是否有商品 没有商品则跳转到首页 */
                $sql = "SELECT COUNT(*) FROM " . $ecs->table('cart') . " WHERE session_id = '" . SESS_ID . "' ";
                if ($db->getOne($sql) > 0)
                {
                    header('Location: /checkout');
                }
                else
                {
                    header('Location: /');
                }

                exit;
            }
            else
            {
                $_SESSION['login_fail']++;
                show_mobile_message($_LANG['signin_failed'], '', 'flow.php?step=login');
            }
        }
        elseif (!empty($_POST['act']) && $_POST['act'] == 'signup')
        {
            if ((intval($_CFG['captcha']) & CAPTCHA_REGISTER) && gd_version() > 0)
            {
                if (empty($_POST['captcha']))
                {
                    show_mobile_message($_LANG['invalid_captcha']);
                }

                /* 检查验证码 */
                include_once('includes/cls_captcha.php');

                $validator = new captcha();
                if (!$validator->check_word($_POST['captcha']))
                {
                    show_mobile_message($_LANG['invalid_captcha']);
                }
            }

            if (register(trim($_POST['username']), trim($_POST['password']), trim($_POST['email'])))
            {
                /* 用户注册成功 */
                ecs_header("Location: flow.php?step=consignee\n");
                exit;
            }
            else
            {
                $err->show();
            }
        }
        else
        {
            // TODO: 非法访问的处理
        }
    }

	/* 对商品信息赋值 */
    $cart_goods = cart_goods($flow_type); // 取得商品列表，计算合计
    $smarty->assign('goods_list', $cart_goods);
    
    /*
     * 取得订单信息
     */
    $order = flow_order_info();
    $smarty->assign('order', $order);
    /* 计算折扣 */
    if ($flow_type != CART_EXCHANGE_GOODS && $flow_type != CART_GROUP_BUY_GOODS)
    {
        $discount = compute_discount();
        $smarty->assign('discount', $discount['discount']);
        $favour_name = empty($discount['name']) ? '' : join(',', $discount['name']);
        $smarty->assign('your_discount', sprintf($_LANG['your_discount'], $favour_name, price_format($discount['discount'])));
    }

    /*
     * 计算订单的费用
     */
    $total = $orderController->order_fee_flow($order, $cart_goods, $consignee, $order_fee_platform);

	 $smarty->assign('cartnumber',cart_ly_number());//sbsn ly  商品个数
	 $smarty->assign('jifen',get_give_integral());//sbsn 显示获赠积分
    $smarty->assign('total', $total);
    $smarty->assign('shopping_money', sprintf($_LANG['shopping_money'], $total['formated_goods_price']));
    $smarty->assign('market_price_desc', sprintf($_LANG['than_market_price'], $total['formated_market_price'], $total['formated_saving'], $total['save_rate']));






}
elseif ($_REQUEST['step'] == 'consignee')
{
    /*------------------------------------------------------ */
    //-- 收货人信息
    /*------------------------------------------------------ */
    include_once('includes/lib_transaction.php');

    if ($_SERVER['REQUEST_METHOD'] == 'GET')
    {
        /* 取得购物类型 */
        $flow_type = isset($_SESSION['flow_type']) ? intval($_SESSION['flow_type']) : CART_GENERAL_GOODS;

        /*
         * 收货人信息填写界面
         */

        if (isset($_REQUEST['direct_shopping']))
        {
            $_SESSION['direct_shopping'] = 1;
        }

        /* 取得国家列表、商店所在国家、商店所在国家的省列表 */
        $smarty->assign('country_list',       get_regions());
        $smarty->assign('shop_country',       $_CFG['shop_country']);
        $smarty->assign('shop_province_list', get_regions(1, $_CFG['shop_country']));

        /* 获得用户所有的收货人信息 */
        if ($_SESSION['user_id'] > 0)
        {
            $consignee_list = get_consignee_list($_SESSION['user_id']);

            if (count($consignee_list) < 5)
            {
                /* 如果用户收货人信息的总数小于 5 则增加一个新的收货人信息 */
                $consignee_list[] = array('country' => $_CFG['shop_country'], 'email' => isset($_SESSION['email']) ? $_SESSION['email'] : '');
            }
        }
        else
        {
            if (isset($_SESSION['flow_consignee'])){
                $consignee_list = array($_SESSION['flow_consignee']);
            }
            else
            {
                $consignee_list[] = array('country' => $_CFG['shop_country']);
            }
        }
        $smarty->assign('name_of_region',   array($_CFG['name_of_region_1'], $_CFG['name_of_region_2'], $_CFG['name_of_region_3'], $_CFG['name_of_region_4']));
        $smarty->assign('consignee_list', $consignee_list);

        /* 取得每个收货地址的省市区列表 */
        $province_list = array();
        $city_list = array();
        $district_list = array();
        foreach ($consignee_list as $region_id => $consignee)
        {
            $consignee['country']  = isset($consignee['country'])  ? intval($consignee['country'])  : 0;
            $consignee['province'] = isset($consignee['province']) ? intval($consignee['province']) : 0;
            $consignee['city']     = isset($consignee['city'])     ? intval($consignee['city'])     : 0;

            $province_list[$region_id] = get_regions(1, $consignee['country']);
            $city_list[$region_id]     = get_regions(2, $consignee['province']);
            $district_list[$region_id] = get_regions(3, $consignee['city']);
        }
        $smarty->assign('province_list', $province_list);
        $smarty->assign('city_list',     $city_list);
        $smarty->assign('district_list', $district_list);

        /* 返回收货人页面代码 */
        $smarty->assign('real_goods_count', exist_real_goods(0, $flow_type) ? 1 : 0);
    }
    else
    {
        /*
         * 保存收货人信息
         */
        $consignee = array(
            'address_id'    => empty($_POST['address_id']) ? 0  :   intval($_POST['address_id']),
            'consignee'     => empty($_POST['consignee'])  ? '' :   compile_str(trim($_POST['consignee'])),
            'country'       => empty($_POST['country'])    ? '' :   intval($_POST['country']),
            'province'      => empty($_POST['province'])   ? '' :   intval($_POST['province']),
            'city'          => empty($_POST['city'])       ? '' :   intval($_POST['city']),
            'district'      => empty($_POST['district'])   ? '' :   intval($_POST['district']),
            'email'         => empty($_POST['email'])      ? '' :   compile_str($_POST['email']),
            'address'       => empty($_POST['address'])    ? '' :   compile_str($_POST['address']),
            'zipcode'       => empty($_POST['zipcode'])    ? '' :   compile_str(make_semiangle(trim($_POST['zipcode']))),
            'tel'           => empty($_POST['tel'])        ? '' :   compile_str(make_semiangle(trim($_POST['tel']))),
            'mobile'        => empty($_POST['mobile'])     ? '' :   compile_str(make_semiangle(trim($_POST['mobile']))),
            'sign_building' => empty($_POST['sign_building']) ? '' :compile_str($_POST['sign_building']),
            'best_time'     => empty($_POST['best_time'])  ? '' :   compile_str($_POST['best_time']),
        );

        if ($_SESSION['user_id'] > 0)
        {
            include_once(ROOT_PATH . 'includes/lib_transaction.php');

            /* 如果用户已经登录，则保存收货人信息 */
            $consignee['user_id'] = $_SESSION['user_id'];

            save_consignee($consignee, true);
        }

        /* 保存到session */
        $_SESSION['flow_consignee'] = stripslashes_deep($consignee);

        header('Location: /checkout');
        exit;
    }
}
elseif ($_REQUEST['step'] == 'drop_consignee')
{
    /*------------------------------------------------------ */
    //-- 删除收货人信息
    /*------------------------------------------------------ */
    include_once('includes/lib_transaction.php');

    $consignee_id = intval($_GET['id']);

    if (drop_consignee($consignee_id))
    {
        ecs_header("Location: flow.php?step=consignee\n");
        exit;
    }
    else
    {
        show_mobile_message($_LANG['not_fount_consignee']);
    }
}
elseif ($_REQUEST['step'] == 'checkout')
{
    /*------------------------------------------------------ */
    //-- New checkout flow: 20180912 by Anthony
    /*------------------------------------------------------ */

    /* Step 1. Check cart have goods and user is login */

    /* 检查购物车中是否有商品 */
    $sql = "SELECT COUNT(*) FROM " . $ecs->table('cart') .
        " WHERE session_id = '" . SESS_ID . "' " .
        "AND parent_id = 0 AND is_gift = 0 AND rec_type = '$flow_type'";

    if ($db->getOne($sql) == 0)
    {
        show_mobile_message(_L('checkout_error_empty_cart', '您的購物車中沒有產品！'), '', '', 'warning');
    }

    /*
     * 检查用户是否已经登录
     * 如果用户已经登录了则检查是否有默认的收货地址
     * 如果没有登录则跳转到登录和注册页面
     */
    if (empty($_SESSION['direct_shopping']) && $_SESSION['user_id'] == 0)
    {
        /* 用户没有登录且没有选定匿名购物，转向到登录页面 */
        //sbsn ly   如果没有登录 则返回值 让.DWT文件自动判断为匿名购买  并弹出对应的窗口填写地址。

        header('Location: flow.php?step=login');
        exit;
    }

    /* Step 2. Get ECShop init data */
    $flow_type = isset($_SESSION['flow_type']) ? intval($_SESSION['flow_type']) : CART_GENERAL_GOODS;

    /* 团购标志 */
    if ($flow_type == CART_GROUP_BUY_GOODS)
    {
        $smarty->assign('is_group_buy', 1);
    }
    /* 积分兑换商品 */
    elseif ($flow_type == CART_EXCHANGE_GOODS)
    {
        $smarty->assign('is_exchange_goods', 1);
    }
    else
    {
        //正常购物流程  清空其他购物流程情况
        $_SESSION['flow_order']['extension_code'] = '';
    }

    /* 对是否允许修改购物车赋值 */
    if ($flow_type != CART_GENERAL_GOODS || $_CFG['one_step_buy'] == '1')
    {
        $smarty->assign('allow_edit_cart', 0);
    }
    else
    {
        $smarty->assign('allow_edit_cart', 1);
    }

    /* Step 3. Get order total, cart goods, user data(余额, 积分) */

    $cart = cart_goods($flow_type, true);
    $cart_goods = $cart['goods']; // 取得商品列表，计算合计
    $user_info = user_info($_SESSION['user_id']);
    $order = flow_order_info();

    $need_removal_list = $rspController->needRemovalGoods($cart_goods);
    if (!empty($need_removal_list)) {
        $smarty->assign('need_removal', $need_removal_list);
    }
    $smarty->assign('goods_list', $cart_goods);
    $smarty->assign('packages', $cart['packages']);

    /* 取得包装与贺卡(Not use) */
    if ($total['real_goods_count'] > 0) {
        /* 只有有实体商品,才要判断包装和贺卡 */
        if (!isset($_CFG['use_package']) || $_CFG['use_package'] == '1') {
            /* 如果使用包装，取得包装列表及用户选择的包装 */
            $smarty->assign('pack_list', pack_list());
        }

        /* 如果使用贺卡，取得贺卡列表及用户选择的贺卡 */
        if (!isset($_CFG['use_card']) || $_CFG['use_card'] == '1') {
            $smarty->assign('card_list', card_list());
        }
    }

    /* 如果使用余额，取得用户余额 */
    if ((!isset($_CFG['use_surplus']) || $_CFG['use_surplus'] == '1') && $_SESSION['user_id'] > 0 && $user_info['user_money'] > 0) {
        // 能使用余额
        $smarty->assign('allow_use_surplus', 1);
        $smarty->assign('your_surplus', $user_info['user_money']);
    }

    /* 如果使用积分，取得用户可用积分及本订单最多可以使用的积分 */
    if ((!isset($_CFG['use_integral']) || $_CFG['use_integral'] == '1')
        && $_SESSION['user_id'] > 0
        && $user_info['pay_points'] > 0
        && ($flow_type != CART_GROUP_BUY_GOODS && $flow_type != CART_EXCHANGE_GOODS)) {
        // 能使用积分
        $smarty->assign('allow_use_integral', 1);
        $smarty->assign('order_max_integral', flow_available_points());  // 可用积分
        $smarty->assign('your_integral', $user_info['pay_points']); // 用户积分
    }

    /* 如果使用红包，取得用户可以使用的红包及用户选择的红包 */
    if ((!isset($_CFG['use_bonus']) || $_CFG['use_bonus'] == '1')
        && ($flow_type != CART_GROUP_BUY_GOODS && $flow_type != CART_EXCHANGE_GOODS)) {
        // 取得用户可用红包
        $user_bonus = user_bonus($_SESSION['user_id'], $total['goods_price']);
        if (!empty($user_bonus)) {
            foreach ($user_bonus AS $key => $val) {
                $user_bonus[$key]['bonus_money_formated'] = price_format($val['type_money'], false);
            }
            $smarty->assign('bonus_list', $user_bonus);
        }

        // 能使用红包
        $smarty->assign('allow_use_bonus', 1);
    }

    /* 如果使用缺货处理，取得缺货处理列表 */
    if (!isset($_CFG['use_how_oos']) || $_CFG['use_how_oos'] == '1') {
        if (is_array($GLOBALS['_LANG']['oos']) && !empty($GLOBALS['_LANG']['oos'])) {
            $smarty->assign('how_oos_list', $GLOBALS['_LANG']['oos']);
        }
    }

    /* 如果能开发票，取得发票内容列表 */
    if ((!isset($_CFG['can_invoice']) || $_CFG['can_invoice'] == '1')
        && isset($_CFG['invoice_content'])
        && trim($_CFG['invoice_content']) != '' && $flow_type != CART_EXCHANGE_GOODS) {
        $inv_content_list = explode("\n", str_replace("\r", '', $_CFG['invoice_content']));
        $smarty->assign('inv_content_list', $inv_content_list);

        $inv_type_list = array();
        foreach ($_CFG['invoice_type']['type'] as $key => $type) {
            if (!empty($type)) {
                $inv_type_list[$type] = $type . ' [' . floatval($_CFG['invoice_type']['rate'][$key]) . '%]';
            }
        }
        $smarty->assign('inv_type_list', $inv_type_list);
    }

    /* Feed data to GA/FB */
    $feed = $feedController->checkout_insert_code($cart_goods);
    $smarty->assign('stats_code_insert', $feed['stats_code_insert']);
    $smarty->assign('extra_insert_code', $feed['extra_insert_code']);

    /*
     * 取得购物流程设置
     */
    $smarty->assign('config', $_CFG);

    /* 计算折扣 */
    if ($flow_type != CART_EXCHANGE_GOODS && $flow_type != CART_GROUP_BUY_GOODS)
    {
        $discount = compute_discount();
        $smarty->assign('discount', $discount['discount']);
        $favour_name = empty($discount['name']) ? '' : join(',', $discount['name']);
        $smarty->assign('your_discount', sprintf($_LANG['your_discount'], $favour_name, price_format($discount['discount'])));
    }

    /*
     * 计算订单的费用
     */
    $total = $orderController->order_fee_flow($order, $cart_goods, $consignee, $order_fee_platform);

    $smarty->assign('cartnumber',cart_ly_number());//sbsn ly  商品个数
    $smarty->assign('jifen',get_give_integral());//sbsn 显示获赠积分
    $smarty->assign('total', $total);
    $smarty->assign('shopping_money', sprintf($_LANG['shopping_money'], $total['formated_goods_price']));
    $smarty->assign('market_price_desc', sprintf($_LANG['than_market_price'], $total['formated_market_price'], $total['formated_saving'], $total['save_rate']));
    /* 保存 session */
    $_SESSION['flow_order'] = $order;
    $smarty->assign('order', $order);
    $smarty->assign('lang', $_CFG['lang']);

    /* Step 4. assign shipping_type first. , shipping, address, payment will on the next step using ajax */
    $shippingController = new \Yoho\cms\Controller\ShippingController();
    $shipping_type_list = $shippingController->available_shipping_type_list();
    $smarty->assign('shipping_type_list', $shipping_type_list);
    // 取得國家名單
    $smarty->assign('available_countries_code', available_countries_code());
    $smarty->assign('available_countries', available_countries());
    $smarty->assign("promote_code", get_promote());
}
elseif ($_REQUEST['step'] == 'select_shipping')
{
    /*------------------------------------------------------ */
    //-- 改变配送方式
    /*------------------------------------------------------ */
    include_once('includes/cls_json.php');
    $json = new JSON;
    $result = array('error' => '', 'content' => '', 'need_insure' => 0);

    /* 取得购物类型 */
    $flow_type = isset($_SESSION['flow_type']) ? intval($_SESSION['flow_type']) : CART_GENERAL_GOODS;

    /* 获得收货人信息 */
    $consignee = get_consignee($_SESSION['user_id']);

    /* 对商品信息赋值 */
    $cart_goods = cart_goods($flow_type); // 取得商品列表，计算合计

    if (empty($cart_goods) || !check_consignee_info($consignee, $flow_type))
    {
        $result['error'] = _L('checkout_error_empty_cart', '您的購物車中沒有產品！');
    }
    else
    {
        /* 取得购物流程设置 */
        $smarty->assign('config', $_CFG);

        /* 取得订单信息 */
        $order = flow_order_info();

        $order['shipping_id'] = intval($_REQUEST['shipping']);
        $regions = array($consignee['country'], $consignee['province'], $consignee['city'], $consignee['district']);
        $shipping_info = shipping_area_info($order['shipping_id'], $regions);

        /* 计算订单的费用 */
        $total = $orderController->order_fee_flow($order, $cart_goods, $consignee, $order_fee_platform);
        $smarty->assign('total', $total);

        /* 取得可以得到的积分和红包 */
        $smarty->assign('total_integral', cart_amount(false, $flow_type) - $total['bonus'] - $total['coupon'] - $total['integral_money']);
        $smarty->assign('total_bonus',    price_format(get_total_bonus(), false));

        /* 团购标志 */
        if ($flow_type == CART_GROUP_BUY_GOODS)
        {
            $smarty->assign('is_group_buy', 1);
        }

        $result['cod_fee']     = $shipping_info['pay_fee'];
        if (strpos($result['cod_fee'], '%') === false)
        {
            $result['cod_fee'] = price_format($result['cod_fee'], false);
        }
        $result['need_insure'] = ($shipping_info['insure'] > 0 && !empty($order['need_insure'])) ? 1 : 0;
        $result['content']     = $smarty->fetch('library/order_total.lbi');
    }

    echo $json->encode($result);
    exit;
}
elseif ($_REQUEST['step'] == 'select_insure')
{
    /*------------------------------------------------------ */
    //-- 选定/取消配送的保价
    /*------------------------------------------------------ */

    include_once('includes/cls_json.php');
    $json = new JSON;
    $result = array('error' => '', 'content' => '', 'need_insure' => 0);

    /* 取得购物类型 */
    $flow_type = isset($_SESSION['flow_type']) ? intval($_SESSION['flow_type']) : CART_GENERAL_GOODS;

    /* 获得收货人信息 */
    $consignee = get_consignee($_SESSION['user_id']);

    /* 对商品信息赋值 */
    $cart_goods = cart_goods($flow_type); // 取得商品列表，计算合计

    if (empty($cart_goods) || !check_consignee_info($consignee, $flow_type))
    {
        $result['error'] = _L('checkout_error_empty_cart', '您的購物車中沒有產品！');
    }
    else
    {
        /* 取得购物流程设置 */
        $smarty->assign('config', $_CFG);

        /* 取得订单信息 */
        $order = flow_order_info();

        $order['need_insure'] = intval($_REQUEST['insure']);

        /* 保存 session */
        $_SESSION['flow_order'] = $order;

        /* 计算订单的费用 */
        $total = $orderController->order_fee_flow($order, $cart_goods, $consignee, $order_fee_platform);
        $smarty->assign('total', $total);

        /* 取得可以得到的积分和红包 */
        $smarty->assign('total_integral', cart_amount(false, $flow_type) - $total['bonus'] - $total['coupon'] - $total['integral_money']);
        $smarty->assign('total_bonus',    price_format(get_total_bonus(), false));

        /* 团购标志 */
        if ($flow_type == CART_GROUP_BUY_GOODS)
        {
            $smarty->assign('is_group_buy', 1);
        }

        $result['content'] = $smarty->fetch('library/order_total.lbi');
    }

    echo $json->encode($result);
    exit;
}
elseif ($_REQUEST['step'] == 'select_payment')
{
    /*------------------------------------------------------ */
    //-- 改变支付方式
    /*------------------------------------------------------ */

    include_once('includes/cls_json.php');
    $json = new JSON;
    $result = array('error' => '', 'content' => '', 'need_insure' => 0, 'payment' => 1);

    /* 取得购物类型 */
    $flow_type = isset($_SESSION['flow_type']) ? intval($_SESSION['flow_type']) : CART_GENERAL_GOODS;

    /* 获得收货人信息 */
    $consignee = get_consignee($_SESSION['user_id']);

    /* 对商品信息赋值 */
    $cart_goods = cart_goods($flow_type); // 取得商品列表，计算合计

    if (empty($cart_goods) || !check_consignee_info($consignee, $flow_type))
    {
        $result['error'] = _L('checkout_error_empty_cart', '您的購物車中沒有產品！');
    }
    else
    {
        /* 取得购物流程设置 */
        $smarty->assign('config', $_CFG);

        /* 取得订单信息 */
        $order = flow_order_info();

        $order['pay_id'] = intval($_REQUEST['payment']);
        $payment_info = payment_info($order['pay_id']);
        $result['pay_code'] = $payment_info['pay_code'];

        /* 保存 session */
        $_SESSION['flow_order'] = $order;

        /* 计算订单的费用 */
        $total = $orderController->order_fee_flow($order, $cart_goods, $consignee, $order_fee_platform);
        $smarty->assign('total', $total);

        /* 取得可以得到的积分和红包 */
        $smarty->assign('total_integral', cart_amount(false, $flow_type) - $total['bonus'] - $total['coupon'] - $total['integral_money']);
        $smarty->assign('total_bonus',    price_format(get_total_bonus(), false));

        /* 团购标志 */
        if ($flow_type == CART_GROUP_BUY_GOODS)
        {
            $smarty->assign('is_group_buy', 1);
        }

        $result['content'] = $smarty->fetch('library/order_total.lbi');
    }

    echo $json->encode($result);
    exit;
}
elseif ($_REQUEST['step'] == 'select_pack')
{
    /*------------------------------------------------------ */
    //-- 改变商品包装
    /*------------------------------------------------------ */

    include_once('includes/cls_json.php');
    $json = new JSON;
    $result = array('error' => '', 'content' => '', 'need_insure' => 0);

    /* 取得购物类型 */
    $flow_type = isset($_SESSION['flow_type']) ? intval($_SESSION['flow_type']) : CART_GENERAL_GOODS;

    /* 获得收货人信息 */
    $consignee = get_consignee($_SESSION['user_id']);

    /* 对商品信息赋值 */
    $cart_goods = cart_goods($flow_type); // 取得商品列表，计算合计

    if (empty($cart_goods) || !check_consignee_info($consignee, $flow_type))
    {
        $result['error'] = _L('checkout_error_empty_cart', '您的購物車中沒有產品！');
    }
    else
    {
        /* 取得购物流程设置 */
        $smarty->assign('config', $_CFG);

        /* 取得订单信息 */
        $order = flow_order_info();

        $order['pack_id'] = intval($_REQUEST['pack']);

        /* 保存 session */
        $_SESSION['flow_order'] = $order;

        /* 计算订单的费用 */
        $total = $orderController->order_fee_flow($order, $cart_goods, $consignee, $order_fee_platform);
        $smarty->assign('total', $total);

        /* 取得可以得到的积分和红包 */
        $smarty->assign('total_integral', cart_amount(false, $flow_type) - $total['bonus'] - $total['coupon'] - $total['integral_money']);
        $smarty->assign('total_bonus',    price_format(get_total_bonus(), false));

        /* 团购标志 */
        if ($flow_type == CART_GROUP_BUY_GOODS)
        {
            $smarty->assign('is_group_buy', 1);
        }

        $result['content'] = $smarty->fetch('library/order_total.lbi');
    }

    echo $json->encode($result);
    exit;
}
elseif ($_REQUEST['step'] == 'select_card')
{
    /*------------------------------------------------------ */
    //-- 改变贺卡
    /*------------------------------------------------------ */

    include_once('includes/cls_json.php');
    $json = new JSON;
    $result = array('error' => '', 'content' => '', 'need_insure' => 0);

    /* 取得购物类型 */
    $flow_type = isset($_SESSION['flow_type']) ? intval($_SESSION['flow_type']) : CART_GENERAL_GOODS;

    /* 获得收货人信息 */
    $consignee = get_consignee($_SESSION['user_id']);

    /* 对商品信息赋值 */
    $cart_goods = cart_goods($flow_type); // 取得商品列表，计算合计

    if (empty($cart_goods) || !check_consignee_info($consignee, $flow_type))
    {
        $result['error'] = _L('checkout_error_empty_cart', '您的購物車中沒有產品！');
    }
    else
    {
        /* 取得购物流程设置 */
        $smarty->assign('config', $_CFG);

        /* 取得订单信息 */
        $order = flow_order_info();

        $order['card_id'] = intval($_REQUEST['card']);

        /* 保存 session */
        $_SESSION['flow_order'] = $order;

        /* 计算订单的费用 */
        $total = $orderController->order_fee_flow($order, $cart_goods, $consignee, $order_fee_platform);
        $smarty->assign('total', $total);

        /* 取得可以得到的积分和红包 */
        $smarty->assign('total_integral', cart_amount(false, $flow_type) - $order['bonus'] - $total['coupon'] - $total['integral_money']);
        $smarty->assign('total_bonus',    price_format(get_total_bonus(), false));

        /* 团购标志 */
        if ($flow_type == CART_GROUP_BUY_GOODS)
        {
            $smarty->assign('is_group_buy', 1);
        }

        $result['content'] = $smarty->fetch('library/order_total.lbi');
    }

    echo $json->encode($result);
    exit;
}
elseif ($_REQUEST['step'] == 'change_surplus')
{
    /*------------------------------------------------------ */
    //-- 改变余额
    /*------------------------------------------------------ */
    include_once('includes/cls_json.php');

    $surplus   = floatval($_GET['surplus']);
    $user_info = user_info($_SESSION['user_id']);

    if ($user_info['user_money'] + $user_info['credit_line'] < $surplus)
    {
        $result['error'] = $_LANG['surplus_not_enough'];
    }
    else
    {
        /* 取得购物类型 */
        $flow_type = isset($_SESSION['flow_type']) ? intval($_SESSION['flow_type']) : CART_GENERAL_GOODS;

        /* 取得购物流程设置 */
        $smarty->assign('config', $_CFG);

        /* 获得收货人信息 */
        $consignee = get_consignee($_SESSION['user_id']);

        /* 对商品信息赋值 */
        $cart_goods = cart_goods($flow_type); // 取得商品列表，计算合计

        if (empty($cart_goods) || !check_consignee_info($consignee, $flow_type))
        {
            $result['error'] = _L('checkout_error_empty_cart', '您的購物車中沒有產品！');
        }
        else
        {
            /* 取得订单信息 */
            $order = flow_order_info();
            $order['surplus'] = $surplus;

            /* 计算订单的费用 */
            $total = $orderController->order_fee_flow($order, $cart_goods, $consignee, $order_fee_platform);
            $smarty->assign('total', $total);

            /* 团购标志 */
            if ($flow_type == CART_GROUP_BUY_GOODS)
            {
                $smarty->assign('is_group_buy', 1);
            }

            $result['content'] = $smarty->fetch('library/order_total.lbi');
        }
    }

    $json = new JSON();
    die($json->encode($result));
}
elseif ($_REQUEST['step'] == 'change_integral')
{
    /*------------------------------------------------------ */
    //-- 改变积分
    /*------------------------------------------------------ */
    include_once('includes/cls_json.php');

    $points    = floatval($_GET['points']);
    $user_info = user_info($_SESSION['user_id']);

    /* 取得订单信息 */
    $order = flow_order_info();

    $flow_points = flow_available_points();  // 该订单允许使用的积分
    $user_points = $user_info['pay_points']; // 用户的积分总数

    if ($points > $user_points)
    {
        $result['error'] = $_LANG['integral_not_enough'];
    }
    elseif ($points > $flow_points)
    {
        $result['error'] = sprintf($_LANG['integral_too_much'], $flow_points);
    }
    else
    {
        /* 取得购物类型 */
        $flow_type = isset($_SESSION['flow_type']) ? intval($_SESSION['flow_type']) : CART_GENERAL_GOODS;

        $order['integral'] = $points;

        /* 获得收货人信息 */
        $consignee = get_consignee($_SESSION['user_id']);

        /* 对商品信息赋值 */
        $cart_goods = cart_goods($flow_type); // 取得商品列表，计算合计

        if (empty($cart_goods) || !check_consignee_info($consignee, $flow_type))
        {
            $result['error'] = _L('checkout_error_empty_cart', '您的購物車中沒有產品！');
        }
        else
        {
            /* 计算订单的费用 */
            $total = $orderController->order_fee_flow($order, $cart_goods, $consignee, $order_fee_platform);
            $smarty->assign('total',  $total);
            $smarty->assign('config', $_CFG);

            /* 团购标志 */
            if ($flow_type == CART_GROUP_BUY_GOODS)
            {
                $smarty->assign('is_group_buy', 1);
            }

            $result['content'] = $smarty->fetch('library/order_total.lbi');
            $result['error'] = '';
        }
    }

    $json = new JSON();
    die($json->encode($result));
}


    /*------------------------------------------------------ */
    //-- sbsn LY  AJAX  address
    /*------------------------------------------------------ */




elseif ($_REQUEST['step'] == 'change_bonus')
{
    /*------------------------------------------------------ */
    //-- 改变红包
    /*------------------------------------------------------ */
    include_once('includes/cls_json.php');
    $result = array('error' => '', 'content' => '');

    /* 取得购物类型 */
    $flow_type = isset($_SESSION['flow_type']) ? intval($_SESSION['flow_type']) : CART_GENERAL_GOODS;

    /* 获得收货人信息 */
    $consignee = get_consignee($_SESSION['user_id']);

    /* 对商品信息赋值 */
    $cart_goods = cart_goods($flow_type); // 取得商品列表，计算合计

    if (empty($cart_goods) || !check_consignee_info($consignee, $flow_type))
    {
        $result['error'] = _L('checkout_error_empty_cart', '您的購物車中沒有產品！');
    }
    else
    {
        /* 取得购物流程设置 */
        $smarty->assign('config', $_CFG);

        /* 取得订单信息 */
        $order = flow_order_info();

        $bonus = bonus_info(intval($_GET['bonus']));

        if ((!empty($bonus) && $bonus['user_id'] == $_SESSION['user_id']) || $_GET['bonus'] == 0)
        {
            $order['bonus_id'] = intval($_GET['bonus']);
        }
        else
        {
            $order['bonus_id'] = 0;
            $result['error'] = $_LANG['invalid_bonus'];
        }

        /* 计算订单的费用 */
        $total = $orderController->order_fee_flow($order, $cart_goods, $consignee, $order_fee_platform);
        $smarty->assign('total', $total);

        /* 团购标志 */
        if ($flow_type == CART_GROUP_BUY_GOODS)
        {
            $smarty->assign('is_group_buy', 1);
        }

        $result['content'] = $smarty->fetch('library/order_total.lbi');
    }

    $json = new JSON();
    die($json->encode($result));
}
elseif ($_REQUEST['step'] == 'change_needinv')
{
    /*------------------------------------------------------ */
    //-- 改变发票的设置
    /*------------------------------------------------------ */
    include_once('includes/cls_json.php');
    $result = array('error' => '', 'content' => '');
    $json = new JSON();
    $_GET['inv_type'] = !empty($_GET['inv_type']) ? json_str_iconv(urldecode($_GET['inv_type'])) : '';
    $_GET['invPayee'] = !empty($_GET['invPayee']) ? json_str_iconv(urldecode($_GET['invPayee'])) : '';
    $_GET['inv_content'] = !empty($_GET['inv_content']) ? json_str_iconv(urldecode($_GET['inv_content'])) : '';

    /* 取得购物类型 */
    $flow_type = isset($_SESSION['flow_type']) ? intval($_SESSION['flow_type']) : CART_GENERAL_GOODS;

    /* 获得收货人信息 */
    $consignee = get_consignee($_SESSION['user_id']);

    /* 对商品信息赋值 */
    $cart_goods = cart_goods($flow_type); // 取得商品列表，计算合计

    if (empty($cart_goods) || !check_consignee_info($consignee, $flow_type))
    {
        $result['error'] = _L('checkout_error_empty_cart', '您的購物車中沒有產品！');
        die($json->encode($result));
    }
    else
    {
        /* 取得购物流程设置 */
        $smarty->assign('config', $_CFG);

        /* 取得订单信息 */
        $order = flow_order_info();

        if (isset($_GET['need_inv']) && intval($_GET['need_inv']) == 1)
        {
            $order['need_inv']    = 1;
            $order['inv_type']    = trim(stripslashes($_GET['inv_type']));
            $order['inv_payee']   = trim(stripslashes($_GET['inv_payee']));
            $order['inv_content'] = trim(stripslashes($_GET['inv_content']));
        }
        else
        {
            $order['need_inv']    = 0;
            $order['inv_type']    = '';
            $order['inv_payee']   = '';
            $order['inv_content'] = '';
        }

        /* 计算订单的费用 */
        $total = $orderController->order_fee_flow($order, $cart_goods, $consignee, $order_fee_platform);
        $smarty->assign('total', $total);

        /* 团购标志 */
        if ($flow_type == CART_GROUP_BUY_GOODS)
        {
            $smarty->assign('is_group_buy', 1);
        }

        die($smarty->fetch('library/order_total.lbi'));
    }
}
elseif ($_REQUEST['step'] == 'change_oos')
{
    /*------------------------------------------------------ */
    //-- 改变缺货处理时的方式
    /*------------------------------------------------------ */

    /* 取得订单信息 */
    $order = flow_order_info();

    $order['how_oos'] = intval($_GET['oos']);

    /* 保存 session */
    $_SESSION['flow_order'] = $order;
}
elseif ($_REQUEST['step'] == 'check_surplus')
{
    /*------------------------------------------------------ */
    //-- 检查用户输入的余额
    /*------------------------------------------------------ */
    $surplus   = floatval($_GET['surplus']);
    $user_info = user_info($_SESSION['user_id']);

    if (($user_info['user_money'] + $user_info['credit_line'] < $surplus))
    {
        die($_LANG['surplus_not_enough']);
    }

    exit;
}
elseif ($_REQUEST['step'] == 'check_integral')
{
    /*------------------------------------------------------ */
    //-- 检查用户输入的余额
    /*------------------------------------------------------ */
    $points      = floatval($_GET['integral']);
    $user_info   = user_info($_SESSION['user_id']);
    $flow_points = flow_available_points();  // 该订单允许使用的积分
    $user_points = $user_info['pay_points']; // 用户的积分总数

    if ($points > $user_points)
    {
        die($_LANG['integral_not_enough']);
    }

    if ($points > $flow_points)
    {
        die(sprintf($_LANG['integral_too_much'], $flow_points));
    }

    exit;
}
/*------------------------------------------------------ */
//-- 完成所有订单操作，提交到数据库
/*------------------------------------------------------ */
elseif ($_REQUEST['step'] == 'done')
{
    include_once('includes/lib_clips.php');
    include_once('includes/lib_payment.php');

    if (!empty($_POST['coupon'])) {
        $checkPromoteCouponResult = $orderController->checkCouponPromoteCreditCardChangePayment($_POST['coupon'],$_POST['payment']);
        if ($checkPromoteCouponResult['need_to_change']) {
            if ($checkPromoteCouponResult['to_change_payment_id'] > 0) {
                $_POST['payment'] = $checkPromoteCouponResult['to_change_payment_id'];
            }
        }
    }

    /*
     * 检查用户是否已经登录
     * 如果用户已经登录了则检查是否有默认的收货地址
     * 如果没有登录则跳转到登录和注册页面
     */
    if (empty($_SESSION['direct_shopping']) && $_SESSION['user_id'] == 0)
    {
        /* 用户没有登录且没有选定匿名购物，转向到登录页面 */
        ecs_header("Location: flow.php?step=login\n");
        exit;
    }

    /* 取得购物类型 */
    $flow_type = isset($_SESSION['flow_type']) ? intval($_SESSION['flow_type']) : CART_GENERAL_GOODS;

    /* 订单中的商品 */
    $cart_goods = cart_goods($flow_type);
    
    /* 检查购物车中是否有商品 */
    if (empty($cart_goods))
    {
        show_mobile_message(_L('checkout_error_empty_cart', '您的購物車中沒有產品！'), _L('global_go_home', '返回首頁'), './', 'warning');
    }

    /* 检查商品总额是否达到最低限购金额 */
    if ($flow_type == CART_GENERAL_GOODS && cart_amount(true, CART_GENERAL_GOODS) < $_CFG['min_goods_amount'])
    {
        show_mobile_message(sprintf(
            _L('checkout_error_below_min_amount', '您購買的產品沒有達到本店的最低限購金額 %s ，不能提交訂單。'),
            price_format($_CFG['min_goods_amount'], false)
        ));
    }

    // howang: 根據庫存自動調整購物車中的產品數量
    $auto_adjust_cart_stock = array();
    foreach ($cart_goods as $value)
    {
        $auto_adjust_cart_stock[$value['rec_id']] = $value['goods_number'];
    }
    auto_adjust_cart_stock($auto_adjust_cart_stock);
    unset($auto_adjust_cart_stock);

    /* 检查商品库存 */
    /* 如果使用库存，且下订单时减库存，则减少库存 */
    if ($_CFG['use_storage'] == '1' && $_CFG['stock_dec_time'] == SDT_PLACE)
    {
        $_cart_goods_stock = array();
        foreach ($cart_goods as $value)
        {
            $_cart_goods_stock[$value['rec_id']] = $value['goods_number'];
        }
        flow_cart_stock($_cart_goods_stock);
        unset($_cart_goods_stock);
    }

    // TODO: shipping_type
    if($_REQUEST['address_type'] == 'pickuppoint' || $_REQUEST['shipping_type'] == 'pickuppoint') {
        $consignee = $addressController->locker_consignee($_SESSION['user_id'], $_REQUEST);
    } elseif ($_REQUEST['shipping_type'] == 'store') {
        $user_info   = user_info($_SESSION['user_id']);
        $consignee = [];
        $consignee['consignee']  = null;
        $consignee['email']      = $user_info['email'];
        $consignee['zipcode']    = '';
        $consignee['tel']        = $user_info['user_name'];
        $consignee['mobile']     = $user_info['user_name'];
        $consignee['best_time']  = '';
        $consignee['city']       = 0;
        $consignee['country']    = 3409;
        $consignee['district']   = 0;
    } else {
        $consignee = get_consignee($_SESSION['user_id']);
    }

    /* 检查收货人信息是否完整
    if (!check_consignee_info($consignee, $flow_type))
    {
        // 如果不完整则转向到收货人信息填写界面
        ecs_header("Location: flow.php?step=checkout\n");  //注释掉 收货人不完整返回一个值给check页 面  sbsn ly
        exit;
    } */

    $_POST['how_oos'] = isset($_POST['how_oos']) ? intval($_POST['how_oos']) : 0;
    $_POST['card_message'] = isset($_POST['card_message']) ? compile_str($_POST['card_message']) : '';
    $_POST['inv_type'] = !empty($_POST['inv_type']) ? compile_str($_POST['inv_type']) : '';
    $_POST['inv_payee'] = isset($_POST['inv_payee']) ? compile_str($_POST['inv_payee']) : '';
    $_POST['inv_content'] = isset($_POST['inv_content']) ? compile_str($_POST['inv_content']) : '';
    $_POST['postscript'] = isset($_POST['postscript']) ? compile_str($_POST['postscript']) : '';

    $order = array(
        'shipping_id'     => isset($_POST['shipping']) ? intval($_POST['shipping']) : 0,
        'pay_id'          => isset($_POST['payment']) ? intval($_POST['payment']) : 0,
        'pack_id'         => isset($_POST['pack']) ? intval($_POST['pack']) : 0,
        'card_id'         => isset($_POST['card']) ? intval($_POST['card']) : 0,
        'card_message'    => trim($_POST['card_message']),
        'surplus'         => isset($_POST['surplus']) ? floatval($_POST['surplus']) : 0.00,
        'integral'        => isset($_POST['integral']) ? intval($_POST['integral']) : 0,
        'bonus_id'        => isset($_POST['bonus']) ? intval($_POST['bonus']) : 0,
        'coupon_code'     => is_array($_POST['coupon']) ? array_map('trim', $_POST['coupon']) : array(trim($_POST['coupon'])),
        'need_inv'        => empty($_POST['need_inv']) ? 0 : 1,
        'shipping_fod'    => empty($_POST['shipping_fod']) ? 0 : 1,
        'inv_type'        => $_POST['inv_type'],
        'inv_payee'       => trim($_POST['inv_payee']),
        'inv_content'     => $_POST['inv_content'],
        'postscript'      => trim($_POST['postscript']),
        'how_oos'         => isset($_LANG['oos'][$_POST['how_oos']]) ? addslashes($_LANG['oos'][$_POST['how_oos']]) : '',
        'need_insure'     => isset($_POST['need_insure']) ? intval($_POST['need_insure']) : 0,
        'user_id'         => $_SESSION['user_id'],
        'add_time'        => gmtime(),
        'order_status'    => OS_UNCONFIRMED,
        'shipping_status' => SS_UNSHIPPED,
        'pay_status'      => PS_UNPAYED,
        'agency_id'       => get_agency_by_regions(array($consignee['country'], $consignee['province'], $consignee['city'], $consignee['district'])),
        'add_time_format' => local_date($GLOBALS['_CFG']['time_format'], gmtime())
        );

    if ($order['pay_id'] <= 0)
    {
		show_mobile_message(_L('checkout_error_missing_payment', '請選擇付款方式！'));
		exit;
	}

    /* 扩展信息 */
    if (isset($_SESSION['flow_type']) && intval($_SESSION['flow_type']) != CART_GENERAL_GOODS)
    {
        $order['extension_code'] = $_SESSION['extension_code'];
        $order['extension_id'] = $_SESSION['extension_id'];
    }
    else
    {
        $order['extension_code'] = '';
        $order['extension_id'] = 0;
    }

    /* 检查积分余额是否合法 */
    $user_id = $_SESSION['user_id'];
    if ($user_id > 0)
    {
        $user_info = user_info($user_id);

        $order['surplus'] = min($order['surplus'], $user_info['user_money'] + $user_info['credit_line']);
        if ($order['surplus'] < 0)
        {
            $order['surplus'] = 0;
        }

        // 查询用户有多少积分
        $flow_points = flow_available_points();  // 该订单允许使用的积分
        $user_points = $user_info['pay_points']; // 用户的积分总数

        $order['integral'] = min($order['integral'], $user_points, $flow_points);
        if ($order['integral'] < 0)
        {
            $order['integral'] = 0;
        }
    }
    else
    {
        $order['surplus']  = 0;
        $order['integral'] = 0;
    }

    /* 检查红包是否存在 */
    if ($order['bonus_id'] > 0)
    {
        $bonus = bonus_info($order['bonus_id']);

        if (empty($bonus) || $bonus['user_id'] != $user_id || $bonus['order_id'] > 0 || $bonus['min_goods_amount'] > cart_amount(true, $flow_type))
        {
            $order['bonus_id'] = 0;
        }
    }
    elseif (isset($_POST['bonus_sn']))
    {
        $bonus_sn = trim($_POST['bonus_sn']);
        $bonus = bonus_info(0, $bonus_sn);
        $now = gmtime();
        if (empty($bonus) || $bonus['user_id'] > 0 || $bonus['order_id'] > 0 || $bonus['min_goods_amount'] > cart_amount(true, $flow_type) || $now > $bonus['use_end_date'])
        {
        }
        else
        {
            if ($user_id > 0)
            {
                $sql = "UPDATE " . $ecs->table('user_bonus') . " SET user_id = '$user_id' WHERE bonus_id = '$bonus[bonus_id]' LIMIT 1";
                $db->query($sql);
            }
            $order['bonus_id'] = $bonus['bonus_id'];
            $order['bonus_sn'] = $bonus_sn;
        }
    }

    // Check coupon
    $coupon_promote = [];
    if (!empty($order['coupon_code']))
    {
        $order['coupon_code'] = array_unique(array_map('strtoupper', $order['coupon_code']));

        $cart_amount = cart_amount(true, $flow_type);

        foreach ($order['coupon_code'] as $coupon_code)
        {
            $coupon = coupon_info(0, $coupon_code);

            if (verify_coupon($coupon, $order, $cart_amount, $cart_goods, $user_id))
            {
                // Assign coupon_id to order
                $order['coupon_id'] = isset($order['coupon_id']) ? $order['coupon_id'] : array();
                $order['coupon_id'][] = $coupon['coupon_id'];
                $order['postscript'] = $coupon['is_remark'] ? $order['postscript']."\n".$coupon['coupon_name'] : $order['postscript'];
                if (!empty($coupon['promote_id'])) {
                    $promote = [
                        "promote_id"    => $coupon['promote_id'],
                        "coupon_id"     => $coupon['coupon_id'],
                        "data"          => [],
                    ];
                    if ($coupon['promote_id'] == 1) {
                        $promote['data']['theClubId'] = trim($_POST['theClubId']);
                    }
                    $coupon_promote[] = $promote;
                }
            }
        }
    }

    /* 收货人信息 */
    // Will need drop address info in order_info in future.
    foreach ($consignee as $key => $value)
    {
        $order[$key] = addslashes($value);
    }

    if (empty($order['email']) && ($user_id > 0))
    {
        $user_info = user_info($user_id);
        $order['email'] = $user_info['email'];
    }

   /* 判断是不是实体商品 */
    foreach ($cart_goods AS $val)
    {
        /* 统计实体商品的个数 */
        if ($val['is_real'])
        {
            $is_real_good = 1;
        }
    }
    if(isset($is_real_good))
    {
        $sql = "SELECT shipping_id FROM " . $ecs->table('shipping') . " WHERE shipping_id=".$order['shipping_id'] ." AND enabled = 1";
        if (!$db->getOne($sql))
        {
           show_mobile_message(_L('checkout_error_missing_shipping', '請選擇送貨方式！'));
        }
    }

    if ($order['shipping_id'] > 0)
    {
        $shipping = shipping_info($order['shipping_id']);

        // check if 運費到付 is available
        if ($order['shipping_fod'])
        {
            if (!$shipping['support_fod'])
            {
                show_mobile_message(_L('checkout_error_shipping_fod_unsupported', '該送貨方式不支援運費到付！'));
            }
        }
    }
    
    // howang: 檢查是否包括需訂貨產品
    $goods_preorder = check_goods_preorder($cart_goods);
    if (!empty($goods_preorder))
    {
        $preorder_msg = '';
        $order_remarks_msg = '';
        foreach ($goods_preorder as $preorder)
        {
            $preorder_msg .= $preorder['goods_name'] . ' ' . $preorder['preorder_mode'] . ' ' . $preorder['pre_sale_days'] . '天';
            //$preorder_msg .= ' (' . $preorder['pre_sale_date_formatted'] . ')';
            $preorder_msg .= "\n";

            $order_remarks_msg .= $preorder['goods_name'] . ' 訂貨大約' . $preorder['pre_sale_days'] . "工作天\n";
        }
        // 商家備註
        $order['to_buyer'] = empty($order['to_buyer']) ? '' : $order['to_buyer'];
        // 客戶備註
        $order['postscript'] = empty($order['postscript']) ? '' : $order['postscript'];
        // 訂貨備註
        $order['order_remarks'] = addslashes($order_remarks_msg);
    }

    $order['cn_ship_tax'] = 0;

    /* 订单中的总额 */
    $total = $orderController->order_fee_flow($order, $cart_goods, $consignee, $order_fee_platform);
    $order['bonus']        = $total['bonus'];
    $order['coupon']       = $total['coupon'];
    $order['goods_amount'] = $total['goods_price'];
    $order['discount']     = $total['discount'];
    $order['surplus']      = $total['surplus'];
    $order['tax']          = $total['tax'];
    $order['cp_price']     = !empty($total['cp_price']) ? $total['cp_price'] : '0';
    $order['cn_ship_tax']  = $total['cn_ship_tax'];

    // 购物车中的商品能享受红包支付的总额
    $discount_amout = compute_discount_amount();
    // 红包和积分最多能支付的金额为商品总额
    $temp_amout = $order['goods_amount'] - $discount_amout;
    if ($temp_amout <= 0)
    {
        $order['bonus_id'] = 0;
        $order['coupon_id'] = array();
    }

    /* 配送方式 */
    if ($order['shipping_id'] > 0)
    {
        $order['shipping_name'] = addslashes($shipping['shipping_name']);
    }
    $order['shipping_fee'] = $total['shipping_fee'];
    $order['insure_fee']   = $total['shipping_insure'];

    /* 支付方式 */
    if ($order['pay_id'] > 0)
    {

        $payment = payment_info($order['pay_id']);//sbsn 货到付款
        // $order['pay_name'] = addslashes($payment['pay_name']);
        $order['pay_name'] = addslashes($payment['display_name']);
    }
    $order['pay_fee'] = $total['pay_fee'];
    $order['cod_fee'] = $total['cod_fee'];

    /* 商品包装 */
    if ($order['pack_id'] > 0)
    {
        $pack               = pack_info($order['pack_id']);
        $order['pack_name'] = addslashes($pack['pack_name']);
    }
    $order['pack_fee'] = $total['pack_fee'];

    /* 祝福贺卡 */
    if ($order['card_id'] > 0)
    {
        $card               = card_info($order['card_id']);
        $order['card_name'] = addslashes($card['card_name']);
    }
    $order['card_fee']      = $total['card_fee'];

    $order['order_amount']  = number_format($total['amount'], 2, '.', '');

    /* 如果全部使用余额支付，检查余额是否足够 */
    if ($payment['pay_code'] == 'balance' && $order['order_amount'] > 0)
    {
        if($order['surplus'] >0) //余额支付里如果输入了一个金额
        {
            $order['order_amount'] = $order['order_amount'] + $order['surplus'];
            $order['surplus'] = 0;
        }
        if ($order['order_amount'] > ($user_info['user_money'] + $user_info['credit_line']))
        {
            show_mobile_message($_LANG['balance_not_enough']);
        }
        else
        {
            $order['surplus'] = $order['order_amount'];
            $order['order_amount'] = 0;
        }
    }

    /* 如果订单金额为0（使用余额或积分或红包支付），修改订单状态为已确认、已付款 */
    if ($order['order_amount'] <= 0)
    {
        $order['order_status'] = OS_CONFIRMED;
        $order['confirm_time'] = gmtime();
        $order['pay_status']   = PS_PAYED;
        $order['pay_time']     = gmtime();
        $order['order_amount'] = 0;
    }

    $order['integral_money']   = $total['integral_money'];
    $order['integral']         = $total['integral'];

    if ($order['extension_code'] == 'exchange_goods')
    {
        $order['integral_money']   = 0;
        $order['integral']         = $total['exchange_integral'];
    }

    $order['from_ad']          = !empty($_SESSION['from_ad']) ? $_SESSION['from_ad'] : '0';
    $order['referer']          = !empty($_SESSION['referer']) ? addslashes($_SESSION['referer']) : '';

    /* 记录扩展信息 */
    if ($flow_type != CART_GENERAL_GOODS)
    {
        $order['extension_code'] = $_SESSION['extension_code'];
        $order['extension_id'] = $_SESSION['extension_id'];
    }

    /* Affiliate handle */
    $affiliate = unserialize($_CFG['affiliate']);
    $parent_id = $user_info['parent_id'];

   // Flow : Handle order affiliate
    // Normal affiliate
    if(isset($affiliate['on']) && $affiliate['on'] == 1)
    {
        if($user_id == $parent_id || $affiliate['config']['separate_by'] == 0) // Can't user == affiliate OR 層層疊推薦回贈
        {
            if($user_id == $parent_id) $order['is_separate'] = $orderController::ORDER_SEPARATE_IGNORE; //We set this order is can't do affiliate
            $parent_id = 0;
        }
        // Check cart_goods amount and order is meets the affiliate
        $goods_price = 0;
        foreach ($cart_goods AS $aff_goods) {
            $goods_price  += $aff_goods['goods_price'] * $aff_goods['goods_number'];
        }
        $can_affiliate = $orderController->check_order_can_separate($user_id, $goods_price, null);
        if(!$can_affiliate) {
            $parent_id = 0;
            $order['is_separate'] = $orderController::ORDER_SEPARATE_IGNORE; //We set this order is can't do affiliate
        }
    }
    else //分成功能关闭
    {
        $parent_id = 0;
        $order['is_separate'] = $orderController::ORDER_SEPARATE_IGNORE; //We set this order is can't do affiliate
    }

    // YOHO affiliate (推廣大使)
    // If normal affiliate is OFF or separate_by = 0 but YOHO affiliate is ON: Check the affiliate_eligible_for_order
    if(isset($affiliate['affiliate_yoho_on']) && $affiliate['affiliate_yoho_on'] == 1 && $parent_id == 0) {

        $parent_id = get_affiliate(true);
        if($user_id == $parent_id) $parent_id = 0;
        if ($parent_id && affiliate_eligible_for_order($parent_id, $order, $cart_goods))
        {
            // Affiliate is eligible for one or more goods in this order
            // If normal affiliate is OFF but YOHO affiliate is ON: We set order is_separate = 3 (只有推廣大使訂單)
            if(empty($affiliate['on']))$order['is_separate'] = $orderController::ORDER_SEPARATE_IGNORE;
        }
        else
        {
            // Affiliate is not eligible for this order
            $parent_id = 0;
        }
    }

    // 「友」福同享 / YOHO affiliate (推廣大使) will using order[parent_id], but 層層疊推薦分成 is not using.
    $order['parent_id'] = $parent_id;

    // Online order default salesperson to 'online', and platform to 'mobile'
    $order['salesperson'] = 'online';
    $order['platform'] = 'mobile';
    
    // Also save the language
    $order['language'] = $_CFG['lang'];

    // Clear address_id
    $order['address_id'] = 0;

    /* 插入订单表 */
    $error_no = 0;
    do
    {
        $order['order_sn'] = get_order_sn(); //获取新订单号
        $GLOBALS['db']->autoExecute($GLOBALS['ecs']->table('order_info'), $order, 'INSERT', '', 'SILENT');

        $error_no = $GLOBALS['db']->errno();

        if ($error_no > 0 && $error_no != 1062)
        {
            die($GLOBALS['db']->errorMsg());
        }
    }
    while ($error_no == 1062); //如果是订单号重复则重新提交数据

    $new_order_id = $db->insert_id();
    $order['order_id'] = $new_order_id;

    // Hold order if this order is created by fraud user
    $orderController->markFraudUserOrder($new_order_id);

    /* 插入订单商品 */
    $sql = "INSERT INTO " . $ecs->table('order_goods') . "( " .
                "order_id, goods_id, goods_name, goods_sn, product_id, goods_number, market_price, ".
                "goods_price, goods_attr, is_real, extension_code, parent_id, insurance_of, is_gift, is_package, is_insurance, goods_attr_id, cost, flashdeal_id, user_rank) ".
            " SELECT '$new_order_id', c.goods_id, c.goods_name, c.goods_sn, c.product_id, c.goods_number, c.market_price, ".
                "c.goods_price, c.goods_attr, c.is_real, c.extension_code, c.parent_id, c.insurance_of, c.is_gift, c.is_package, c.is_insurance, c.goods_attr_id, g.cost, c.flashdeal_id, c.user_rank  ".
            " FROM " . $ecs->table('cart') . "as c ".
            " LEFT JOIN " . $ecs->table('goods') . " as g ON c.goods_id = g.goods_id ".
            " WHERE session_id = '".SESS_ID."' AND rec_type = '$flow_type'";
    $db->query($sql);
    /* 標記VIP價錢  */
    $sql = "SELECT og.rec_id, og.goods_id, og.goods_price, u.user_id, og.user_rank as goods_user_rank FROM " . $ecs->table('order_goods') . ' og' .
            " LEFT JOIN " . $ecs->table('order_info') . " oi ON og.order_id = oi.order_id" .
            " LEFT JOIN " . $ecs->table('users') . " u ON oi.user_id = u.user_id" .
            " WHERE og.order_id = " . $new_order_id;
    $goods_list = $db->getAll($sql);
    foreach($goods_list as $goods){
        $is_vip = intval($orderController->is_vip_price($goods['goods_id'],$goods['user_id'],$goods['goods_price']));

        if($is_vip) { // Anthony: 如果購物車價格為特殊價格, 但等同VIP價, 預設為VIP價.
            $sql = "UPDATE " . $ecs->table('order_goods') . " SET user_rank = 2 " .
                " WHERE is_gift = 0 AND order_id = " . $new_order_id . " AND goods_id = " . $goods['goods_id'] . " AND rec_id = " . $goods['rec_id'];
            $db->query($sql);
        }
    }

    if (!empty($coupon_promote)) {
        foreach ($coupon_promote as $promote) {
            $db->query("INSERT INTO " . $ecs->table("coupon_promote_info") . " (order_id, promote_id, user_id, coupon_id, data) VALUES ('$new_order_id', '$promote[promote_id]', '$order[user_id]', '$promote[coupon_id]', '" . \json_encode($promote['data']) . "')");
        }
    }

    $flashdealController = new \Yoho\cms\Controller\FlashdealController();
    $flashdealController->updateFlashdealCartToOrder($new_order_id);
    /* Insert Order address */
    $order_address_id = $orderController->update_order_address($consignee, $new_order_id);
    $order['address_id'] = $order_address_id;

    /* 修改拍卖活动状态 */
    if ($order['extension_code']=='auction')
    {
        $sql = "UPDATE ". $ecs->table('goods_activity') ." SET is_finished='2' WHERE act_id=".$order['extension_id'];
        $db->query($sql);
    }

    /* 处理余额、积分、红包 */
    if ($order['user_id'] > 0 && $order['surplus'] > 0)
    {
        log_account_change($order['user_id'], $order['surplus'] * (-1), 0, 0, 0, sprintf($_LANG['pay_order'], $order['order_sn']));
    }
    if ($order['user_id'] > 0 && $order['integral'] > 0)
    {
        log_account_change($order['user_id'], 0, 0, 0, $order['integral'] * (-1), sprintf($_LANG['pay_order'], $order['order_sn']), ACT_OTHER, 0, ['type' => 'sales', 'sales_order_id' => $new_order_id]);
    }


    if ($order['bonus_id'] > 0 && $temp_amout > 0)
    {
        use_bonus($order['bonus_id'], $new_order_id);
    }

    if (!empty($order['coupon_id']))
    {
        foreach ($order['coupon_id'] as $coupon_id)
        {
            use_coupon($coupon_id, $new_order_id);
        }
    }

    /* 如果使用库存，且下订单时减库存，则减少库存 */
    if ($_CFG['use_storage'] == '1' && $_CFG['stock_dec_time'] == SDT_PLACE)
    {
        change_order_goods_storage($order['order_id'], true, SDT_PLACE);
    }

    /* 给商家发邮件 */
    /* 增加是否给客服发送邮件选项 */
    if ($_CFG['send_service_email'] && $_CFG['service_email'] != '')
    {
        $tpl = get_mail_template('remind_of_new_order');
		$smarty->assign('norder',     order_info(0, $order['order_sn']));
        $smarty->assign('order', $order);
        $smarty->assign('goods_list', $cart_goods);
        $smarty->assign('shop_name', $_CFG['shop_name']);
        $smarty->assign('send_date', date($_CFG['time_format']));
        $content = $smarty->fetch('str:' . $tpl['template_content']);
        send_mail($_CFG['shop_name'], $_CFG['service_email'], $tpl['template_subject'], $content, $tpl['is_html']);
    }

    /* 如果需要，发短信 */
    if ($_CFG['sms_order_placed'] == '1' && $_CFG['sms_shop_mobile'] != '')
    {
        include_once('includes/cls_sms.php');
        $sms = new sms();
        $msg = $order['pay_status'] == PS_UNPAYED ?
            $_LANG['order_placed_sms'] : $_LANG['order_placed_sms'] . '[' . $_LANG['sms_paid'] . ']';
        $sms->send($_CFG['sms_shop_mobile'], sprintf($msg, $order['consignee'], $order['tel']),'', 13,1);
    }

    /* 如果订单金额为0 处理虚拟卡 */
    if ($order['order_amount'] <= 0)
    {
        $sql = "SELECT goods_id, goods_name, goods_number AS num FROM ".
               $GLOBALS['ecs']->table('cart') .
                " WHERE is_real = 0 AND extension_code = 'virtual_card'".
                " AND session_id = '".SESS_ID."' AND rec_type = '$flow_type'";

        $res = $GLOBALS['db']->getAll($sql);

        $virtual_goods = array();
        foreach ($res AS $row)
        {
            $virtual_goods['virtual_card'][] = array('goods_id' => $row['goods_id'], 'goods_name' => $row['goods_name'], 'num' => $row['num']);
        }

        if ($virtual_goods AND $flow_type != CART_GROUP_BUY_GOODS)
        {
            /* 虚拟卡发货 */
            if (virtual_goods_ship($virtual_goods,$msg, $order['order_sn'], true))
            {
                /* 如果没有实体商品，修改发货状态，送积分和红包 */
                $sql = "SELECT COUNT(*)" .
                        " FROM " . $ecs->table('order_goods') .
                        " WHERE order_id = '$order[order_id]' " .
                        " AND is_real = 1";
                if ($db->getOne($sql) <= 0)
                {
                    /* 修改订单状态 */
                    update_order($order['order_id'], array('shipping_status' => SS_SHIPPED, 'shipping_time' => gmtime()));

                    /* 如果订单用户不为空，计算积分，并发给用户；发红包 */
                    if ($order['user_id'] > 0)
                    {
                        /* 取得用户信息 */
                        $user = user_info($order['user_id']);

                        /* 计算并发放积分 */
                        $integral = integral_to_give($order);
                        log_account_change($order['user_id'], 0, 0, intval($integral['rank_points']), intval($integral['custom_points']), sprintf($_LANG['order_gift_integral'], $order['order_sn']), ACT_OTHER, 0, ['sales_order_id' => $new_order_id]);

                        /* 发放红包 */
                        send_order_bonus($order['order_id']);
                    }
                }
            }
        }
    }

    /* 清空购物车 */
    clear_cart($flow_type);
    /* 清除缓存，否则买了商品，但是前台页面读取缓存，商品数量不减少 */
    //    clear_all_files();
    // clear_cache_files('flashdeal');
    // clear_cache_files('goods');

    /* 插入支付日志 */
    $order['log_id'] = insert_pay_log($new_order_id, $order['order_amount'], PAY_ORDER);

    /* 取得支付信息，生成支付代码 */
    if ($order['order_amount'] > 0)
    {
        $payment = payment_info($order['pay_id']);

        include_once('includes/modules/payment/' . $payment['pay_code'] . '.php');

        $pay_obj    = new $payment['pay_code'];

        $pay_online = $pay_obj->get_code($order, $payment['pay_config_list']);

        $order['pay_desc'] = $payment['pay_desc'];
        $online_only = false;
        foreach ($cart_goods as $og_k => $og) {
            // Have flashdeal goods, only can pay with online payment
            $sql = "SELECT is_online_pay FROM ".$ecs->table('flashdeals').' WHERE flashdeal_id = '.$og['flashdeal_id'];
            if($db->getOne($sql)) {
                $online_only = true;
                break;
            }
    
        }
        $order['is_online_pay']      = $online_only;
        $payment_list = $paymentController->available_payment_list(false, 0, true, $order);

        $smarty->assign('payment_list', $payment_list);
        $smarty->assign('payment', $payment);
        $smarty->assign('pay_online', $pay_online);

        $smarty->assign('payment_amount', $actgHooks->getOrderPaymentAmount($order));
    }
    if(!empty($order['shipping_name']))
    {
        $order['shipping_name']=trim(stripcslashes($order['shipping_name']));
    }

    // Create need removal form.
    $need_removal_list = $rspController->needRemovalGoods($cart_goods);
    if(!empty($need_removal_list)) {
        $rspController->handle_insert_record($_REQUEST, $rspController::RSP_FROM_INTERNET, $order['order_id']);
    }

    // Add user rank program remark
    $userRankController->addProgramRemark($order['order_id']);

    /* 订单信息 */
    $smarty->assign('norder',     order_info(0,$order['order_sn']));
    $smarty->assign('order',      $order);
    $smarty->assign('total',      $total);
    $smarty->assign('goods_list', $cart_goods);
    $smarty->assign('order_submit_back', sprintf($_LANG['order_submit_back'], $_LANG['back_home'], $_LANG['goto_user_center'])); // 返回提示

    user_uc_call('add_feed', array($order['order_id'], BUY_GOODS)); //推送feed到uc
    unset($_SESSION['flow_consignee']); // 清除session中保存的收货人信息
    unset($_SESSION['flow_order']);
    unset($_SESSION['direct_shopping']);

    // YOHO: Send payment reminder email
    if ($order['order_amount'] > 0)
    {
        if (!$payment['is_online_pay']) {
            $crmController = new Yoho\cms\Controller\CrmController();
            $receipt_link = $crmController->init_offline_payment_flow($order);
        }
        if (can_send_mail($order['email'], true))
        {
            $tpl = get_mail_template('payment_reminder');
            $smarty->assign('pay_code', $payment['pay_code']);
            $smarty->assign('shop_url', $ecs->url());
            $smarty->assign('shop_name', $GLOBALS['_CFG']['shop_name']);
            $smarty->assign('send_date', local_date($GLOBALS['_CFG']['date_format']));
            $smarty->assign('sent_date', local_date($GLOBALS['_CFG']['date_format']));

            // Send upload payment receipt link
            if (!$payment['is_online_pay']) {
                $smarty->assign('receipt_link', $receipt_link);
            }

            $content = $smarty->fetch('str:' . $tpl['template_content']);
            send_mail($order['consignee'], $order['email'], $tpl['template_subject'] . ' - ' . $order['order_sn'], $content, $tpl['is_html']);
        }
    }
    // If is 線下付款, add purchase Event, (如果是信用卡/其他線上付款方式, 將付款完成後再CALL purchase_insert_code)
    if (!$payment['is_online_pay']) {
        $feed = $feedController->purchase_insert_code($order, $cart_goods);
        $smarty->assign('stats_code_insert', $feed['stats_code_insert']);
        $smarty->assign('extra_insert_code', $feed['extra_insert_code']);
    
    }
    $smarty->assign('is_online_pay', $payment['is_online_pay']);
}

/*------------------------------------------------------ */
//-- 更新购物车
/*------------------------------------------------------ */

elseif ($_REQUEST['step'] == 'update_cart')
{
    if (isset($_REQUEST['lineItemId']) && isset($_REQUEST['goods_number']))
    {
        $arr = array();
        $arr[$_REQUEST['lineItemId']] = $_REQUEST['goods_number'];
        $res = flow_update_cart($arr);
        header('Content-Type: application/json');
        echo json_encode($res);
    }
    else
    {
        header('Content-Type: application/json');
        echo json_encode(array('error' => 1));
    }

    //show_mobile_message($_LANG['update_cart_notice'], $_LANG['back_to_cart'], 'flow.php');
    exit;
}

/*------------------------------------------------------ */
//-- 删除购物车中的商品
/*------------------------------------------------------ */

elseif ($_REQUEST['step'] == 'drop_goods')
{
    $rec_id = intval($_GET['id']);
    flow_drop_cart_goods($rec_id);
    if(!empty($_GET['ajax']))
    {
        header('Content-Type: application/json');
        echo json_encode(array(
            'totalCount' => insert_cart_info(),
            'totalPrice' => insert_cart_infoamount()
        ));
        exit;
    }
    
    header('Location: /cart');
    exit;
}

/* 把优惠活动加入购物车 */
elseif ($_REQUEST['step'] == 'add_favourable')
{
    /* 取得优惠活动信息 */
    $act_id = intval($_POST['act_id']);
     
    $res = $orderController->add_favourable_goods(0, 1, $act_id, $_SESSION['user_rank'], $_SESSION['user_id'], $order_fee_platform, $_POST['gift']);

    /* 刷新购物车 */
    header('Location: /cart');
    exit;
}
elseif ($_REQUEST['step'] == 'clear')
{
    $sql = "DELETE FROM " . $ecs->table('cart') . " WHERE session_id='" . SESS_ID . "'";
    $db->query($sql);
    $flashdealController = new \Yoho\cms\Controller\FlashdealController();
    $flashdealController->updateFlashdealCartByECSCart();
    header('Location: /cart');
    exit;
}
elseif ($_REQUEST['step'] == 'drop_to_collect')
{
    if ($_SESSION['user_id'] > 0)
    {
        $rec_id = intval($_GET['id']);
        $goods_id = $db->getOne("SELECT  goods_id FROM " .$ecs->table('cart'). " WHERE rec_id = '$rec_id' AND session_id = '" . SESS_ID . "' ");
        $count = $db->getOne("SELECT goods_id FROM " . $ecs->table('collect_goods') . " WHERE user_id = '$_SESSION[user_id]' AND goods_id = '$goods_id'");
        if (empty($count))
        {
            $time = gmtime();
            $sql = "INSERT INTO " .$GLOBALS['ecs']->table('collect_goods'). " (user_id, goods_id, add_time)" .
                    "VALUES ('$_SESSION[user_id]', '$goods_id', '$time')";
            $db->query($sql);
        }
        flow_drop_cart_goods($rec_id);
    }
    header('Location: /cart');
    exit;
}

/* 验证红包序列号 */
elseif ($_REQUEST['step'] == 'validate_bonus')
{
    $bonus_sn = trim($_REQUEST['bonus_sn']);
    if (is_numeric($bonus_sn))
    {
        $bonus = bonus_info(0, $bonus_sn);
    }
    else
    {
        $bonus = array();
    }

//    if (empty($bonus) || $bonus['user_id'] > 0 || $bonus['order_id'] > 0)
//    {
//        die($_LANG['bonus_sn_error']);
//    }
//    if ($bonus['min_goods_amount'] > cart_amount())
//    {
//        die(sprintf($_LANG['bonus_min_amount_error'], price_format($bonus['min_goods_amount'], false)));
//    }
//    die(sprintf($_LANG['bonus_is_ok'], price_format($bonus['type_money'], false)));
    $bonus_kill = price_format($bonus['type_money'], false);

    include_once('includes/cls_json.php');
    $result = array('error' => '', 'content' => '');

    /* 取得购物类型 */
    $flow_type = isset($_SESSION['flow_type']) ? intval($_SESSION['flow_type']) : CART_GENERAL_GOODS;

    /* 获得收货人信息 */
    $consignee = get_consignee($_SESSION['user_id']);

    /* 对商品信息赋值 */
    $cart_goods = cart_goods($flow_type); // 取得商品列表，计算合计

    if (empty($cart_goods) || !check_consignee_info($consignee, $flow_type))
    {
        $result['error'] = _L('checkout_error_empty_cart', '您的購物車中沒有產品！');
    }
    else
    {
        /* 取得购物流程设置 */
        $smarty->assign('config', $_CFG);

        /* 取得订单信息 */
        $order = flow_order_info();


        if (((!empty($bonus) && $bonus['user_id'] == $_SESSION['user_id']) || ($bonus['type_money'] > 0 && empty($bonus['user_id']))) && $bonus['order_id'] <= 0)
        {
            //$order['bonus_kill'] = $bonus['type_money'];
            $now = gmtime();
            if ($now > $bonus['use_end_date'])
            {
                $order['bonus_id'] = '';
                $result['error']=$_LANG['bonus_use_expire'];
            }
            else
            {
                $order['bonus_id'] = $bonus['bonus_id'];
                $order['bonus_sn'] = $bonus_sn;
            }
        }
        else
        {
            //$order['bonus_kill'] = 0;
            $order['bonus_id'] = '';
            $result['error'] = $_LANG['invalid_bonus'];
        }

        /* 计算订单的费用 */
        $total = $orderController->order_fee_flow($order, $cart_goods, $consignee, $order_fee_platform);

        if($total['goods_price']<$bonus['min_goods_amount'])
        {
         $order['bonus_id'] = '';
         /* 重新计算订单 */
         $total = $orderController->order_fee_flow($order, $cart_goods, $consignee, $order_fee_platform);
         $result['error'] = sprintf($_LANG['bonus_min_amount_error'], price_format($bonus['min_goods_amount'], false));
        }

        $smarty->assign('total', $total);

        /* 团购标志 */
        if ($flow_type == CART_GROUP_BUY_GOODS)
        {
            $smarty->assign('is_group_buy', 1);
        }

        $result['content'] = $smarty->fetch('library/order_total.lbi');
    }
    $json = new JSON();
    die($json->encode($result));
}
/*------------------------------------------------------ */
//-- 添加礼包到购物车
/*------------------------------------------------------ */
elseif ($_REQUEST['step'] == 'add_package_to_cart')
{
    include_once('includes/cls_json.php');
    $package_id = ($_POST['package_id']);
    $package_number=1;


    if (empty($_POST['package_id']))
    {
        $result['error'] = 1;
        die($json->encode($result));
    }



    /* 如果是一步购物，先清空购物车 */
    if ($_CFG['one_step_buy'] == '1')
    {
        clear_cart();
    }

    /* 商品数量是否合法 */
    if (!is_numeric($package_number) || intval($package_number) <= 0)
    {
        $result['error']   = 1;
        $result['message'] = $_LANG['invalid_number'];
    }
    else
    {
        /* 添加到购物车 */
        if (add_package_to_cart($package_id,$package_number))
        {
            if ($_CFG['cart_confirm'] > 2)
            {
                $result['message'] = '';
            }
            else
            {
                $result['message'] = $_CFG['cart_confirm'] == 1 ? $_LANG['addto_cart_success_1'] : $_LANG['addto_cart_success_2'];
            }

            $result['content'] = insert_cart_info();
            $result['one_step_buy'] = $_CFG['one_step_buy'];
        }
        else
        {
            $result['message']    = $err->last_message();
            $result['error']      = $err->error_no;
            $result['package_id'] = stripslashes($package_id);
        }
    }
    $result['confirm_type'] = !empty($_CFG['cart_confirm']) ? $_CFG['cart_confirm'] : 2;

    header('Location: /cart');
    exit;
}
else//if ($_REQUEST['step'] == 'cart')
{
    /* 标记购物流程为普通商品 */
    $_SESSION['flow_type'] = CART_GENERAL_GOODS;

    /* 如果是一步购物，跳到结算中心 */
    if ($_CFG['one_step_buy'] == '1')
    {
        header('Location: /checkout');
        exit;
    }

    //ungroup package not available
    $orderController->auto_ungroup_package($order_fee_platform);

    // Auto group product to package
	$orderController->auto_group_package($order_fee_platform);

    /* delete all Favourable / Package product which not available */
    $orderController->check_cart_favourable_goods($_SESSION['user_rank'], $_SESSION['user_id'], $order_fee_platform);

    /* 取得优惠活动 */
    $favourable_list = $orderController->favourable_list($_SESSION['user_rank'], $_SESSION['user_id'], $order_fee_platform,null);
    /*
    if(!$flashdealController->checkFlashdealEventIsGoingToStart()){
        $favourable_list = $orderController->favourable_list($_SESSION['user_rank'], $_SESSION['user_id'], $order_fee_platform,null);
    }
    else{
        $favourable_list = array();
    }
    */
    $smarty->assign('favourable_list', $favourable_list);

    /* 取得商品列表，计算合计 */
    $cart_goods = get_cart_goods();
    $smarty->assign('goods_list', $cart_goods['goods_list']);
    $smarty->assign('total', $cart_goods['total']);
    $smarty->assign('ship_type', $cart_goods['ship_type']);
    $smarty->assign('packages', $cart_goods['packages']);

    //购物车的描述的格式化
    $smarty->assign('shopping_money',         sprintf($_LANG['shopping_money'], $cart_goods['total']['goods_price']));
    $smarty->assign('market_price_desc',      sprintf($_LANG['than_market_price'],
        $cart_goods['total']['market_price'], $cart_goods['total']['saving'], $cart_goods['total']['save_rate']));

    // 显示收藏夹内的商品
    if ($_SESSION['user_id'] > 0)
    {
        require_once(ROOT_PATH . 'includes/lib_clips.php');

        if (!$flashdealController->checkFlashdealEventIsGoingToStart()) {
            $collection_goods = get_collection_goods($_SESSION['user_id']);
        }
        $smarty->assign('collection_goods', $collection_goods);
    }

    /* 计算折扣 */
    $discount = compute_discount();
    $smarty->assign('discount', $discount['discount']);
    $favour_name = empty($discount['name']) ? '' : join(',', $discount['name']);
    $smarty->assign('your_discount', sprintf($_LANG['your_discount'], $favour_name, price_format($discount['discount'])));

    /* 增加是否在购物车里显示商品图 */
    $smarty->assign('show_goods_thumb', $GLOBALS['_CFG']['show_goods_in_cart']);

    /* 增加是否在购物车里显示商品属性 */
    $smarty->assign('show_goods_attribute', $GLOBALS['_CFG']['show_attr_in_cart']);

    /* 购物车中商品配件列表 */
    //取得购物车中基本件ID
    $sql = "SELECT goods_id " .
            "FROM " . $GLOBALS['ecs']->table('cart') .
            " WHERE session_id = '" . SESS_ID . "' " .
            "AND rec_type = '" . CART_GENERAL_GOODS . "' " .
            "AND is_gift = 0 " .
            "AND extension_code <> 'package' " .
            "AND parent_id = 0 ";
    $parent_list = $GLOBALS['db']->getCol($sql);

    // howang: we don't use 配件
    // $fittings_list = get_goods_fittings($parent_list);
    // $smarty->assign('fittings_list', $fittings_list);
    if (!$flashdealController->checkFlashdealEventIsGoingToStart()) {
        $promote_goods_list = get_goods_promote_cart($parent_list);
    }
    $smarty->assign('promote_goods_list', $promote_goods_list);
    // Get can cal free region
    $free_region = $addressController->get_free_calculate_region();
    if(!empty($free_region['country'])) {
        foreach ($free_region['country'] as $country) {
            $country_name[] = $_LANG['country_'.$country];
        }
        $free_shipping_country_name = implode(', ', $country_name);
    }
    $smarty->assign('fs_country_name', $free_shipping_country_name);
    // if cart is being adjusted by system...
    if (!empty($_SESSION['adjusted_goods']))
    {
        $adjusted_goods = $_SESSION['adjusted_goods'];
        unset($_SESSION['adjusted_goods']);
        foreach ($adjusted_goods as $key => $goods)
        {
            $adjusted_goods[$key]['url'] = build_uri('goods', array('gid' => $goods['goods_id']), $goods['goods_name']);
        }
        $smarty->assign('adjusted_goods', $adjusted_goods);
    }
}

if (in_array($_REQUEST['step'], array('checkout', 'consignee')))
{
    $sql = "SELECT address_id ".
            "FROM " . $GLOBALS['ecs']->table('users') .
            "WHERE user_id = '" . $_SESSION['user_id'] . "'";
    $smarty->assign('m', $GLOBALS['db']->getOne($sql));
    $smarty->assign('lyadd', $userController->get_user_address_list($_SESSION['user_id']));
    $smarty->assign('address_types', json_encode($addressController->get_address_types()));
    $types_groups = $addressController->get_address_types_groups();
    $address_types_groups = array();
    if (sizeof($types_groups) > 0){
        foreach ($types_groups as $group){
            $address_types_groups[$group['cat']] = $_LANG['checkout_address_type_'.$group['cat']];
        }
    }
    $smarty->assign('address_types_groups', json_encode($address_types_groups));
}

if (isset($order['order_id'])) {
    $can_change_order_payment_type = ($orderController->canChangeOrderPaymentType($order['order_id']) && $orderController->canChangeOrderPaymentTypeWithFlashDeal($order['order_id']));
    $smarty->assign('can_change_order_payment_type', $can_change_order_payment_type);
}

$smarty->assign('currency_format', $_CFG['currency_format']);
$smarty->assign('integral_scale',  $_CFG['integral_scale']);
$smarty->assign('step',            $_REQUEST['step']);
assign_dynamic('shopping_flow');

$smarty->display('flow.html');

/*------------------------------------------------------ */
//-- PRIVATE FUNCTION
/*------------------------------------------------------ */

/**
 * 获得用户的可用积分
 *
 * @access  private
 * @return  integral
 */
function flow_available_points()
{
    $sql = "SELECT SUM(g.integral * c.goods_number) ".
            "FROM " . $GLOBALS['ecs']->table('cart') . " AS c, " . $GLOBALS['ecs']->table('goods') . " AS g " .
            "WHERE c.session_id = '" . SESS_ID . "' AND c.goods_id = g.goods_id AND c.is_gift = 0 AND g.integral > 0 " .
            "AND c.rec_type = '" . CART_GENERAL_GOODS . "'";

    $val = intval($GLOBALS['db']->getOne($sql));

    return integral_of_value($val);
}

/**
 * 更新购物车中的商品数量
 *
 * @access  public
 * @param   array   $arr
 * @return array
 */
function flow_update_cart($arr)
{
    
    global $orderController;
    global $order_fee_platform;
    /* 处理 */
    foreach ($arr AS $key => $val)
    {
        
        $val = intval(make_semiangle($val));
        if (!is_numeric($key))
        {
            continue;
        }
        
        if ($val <= 0)
        {
            flow_drop_cart_goods($key);
        }

        //查询：
        $sql = "SELECT `goods_id`, `goods_attr_id`, `product_id`, `extension_code`, `flashdeal_id`, `is_gift`, `is_package`, `is_insurance`, `insurance_of` FROM" .$GLOBALS['ecs']->table('cart').
               " WHERE rec_id='$key' AND session_id='" . SESS_ID . "'";
        $goods = $GLOBALS['db']->getRow($sql);

        $sql = "SELECT goods_name, " . hw_goods_number_sql() . " FROM (".
                "SELECT c.goods_name, g.is_pre_sale, g.pre_sale_date, g.pre_sale_days, ".
                hw_goods_number_subquery('g.') . ", " .
    			hw_reserved_number_subquery('g.') . " " .
                "FROM " .$GLOBALS['ecs']->table('goods'). " AS g, ".
                    $GLOBALS['ecs']->table('cart'). " AS c ".
                "WHERE g.goods_id = c.goods_id AND c.rec_id = '$key'".
                ") as tmp";
        $row = $GLOBALS['db']->getRow($sql);

        // YOHO: limit quantity to 20 a time
        $row['goods_number'] = min(intval($row['goods_number']), 20);

        // Handle flashdeal
        if ($goods['flashdeal_id'] > 0)
        {
            /* Update by Anthony: flashdeal only can buy 1 */
//            $sql = "SELECT fd.*, " .
//                        "(SELECT IFNULL(SUM(og.`goods_number`),0) " .
//                        "FROM " . $GLOBALS['ecs']->table('order_goods') . " as og " .
//                        "LEFT JOIN " . $GLOBALS['ecs']->table('order_info') . " as oi ON og.order_id = oi.order_id " .
//                        "WHERE og.goods_id = fd.goods_id " .
//                        "AND og.flashdeal_id = fd.flashdeal_id " .
//                        "AND oi.order_status " . db_create_in(array(OS_CONFIRMED, OS_SPLITED, OS_SPLITING_PART)) . " " .
//                        "AND oi.pay_status " . db_create_in(array(PS_PAYED, PS_PAYING)) . " " .
//                        ") as dealt_quantity " .
//                    "FROM " . $GLOBALS['ecs']->table('flashdeals') . " as fd " .
//                    "WHERE flashdeal_id = '" . $goods['flashdeal_id'] . "'";
//            $flashdeal = $GLOBALS['db']->getRow($sql);
//
//            $available_quantity = $flashdeal['deal_quantity'] - $flashdeal['dealt_quantity'];

            if (1 < $val)
            {
                return array(
                    'error' => 1,
                    'message' => sprintf(_L('global_cart_quantity_exceeded2', '非常抱歉，您選購的「%s」數量超出購買上限 %d 個。'),
                        $row['goods_name'], 1)
                );
            }

            continue;
        }

        if ($goods['is_insurance'] == 1 && $goods['insurance_of'] > 0){
            $sql = "SELECT c.goods_number
            FROM " .$GLOBALS['ecs']->table('cart') . " AS c 
            WHERE c.session_id = '" . SESS_ID . "'
            AND c.goods_id = '" . $goods['insurance_of'] . "'
            AND c.is_package = 0 
            AND c.is_insurance = 0 ";

            $res = $GLOBALS['db']->getOne($sql);
            
            if ($res < $val){
                $val = $res;
            }
        }
        
        //查询：系统启用了库存，检查输入的商品数量是否有效
        if (intval($GLOBALS['_CFG']['use_storage']) > 0 && $goods['extension_code'] != 'package' && $goods['is_insurance'] == 0)
        {
            if ($row['goods_number'] < $val)
            {
                // show_message(sprintf($GLOBALS['_LANG']['stock_insufficiency'], $row['goods_name'],
                // $row['goods_number'], $row['goods_number']));
                // exit;
                return array(
                    'error' => 1,
                    'message' => sprintf(_L('global_cart_quantity_exceeded2', '非常抱歉，您選購的「%s」數量超出購買上限 %d 個。'),
                                        $row['goods_name'], $row['goods_number'])
                );
            }
            /* 是货品 */
            $goods['product_id'] = trim($goods['product_id']);
            if (!empty($goods['product_id']))
            {
                $sql = "SELECT product_number FROM " .$GLOBALS['ecs']->table('products'). " WHERE goods_id = '" . $goods['goods_id'] . "' AND product_id = '" . $goods['product_id'] . "'";

                $product_number = $GLOBALS['db']->getOne($sql);
                if ($product_number < $val)
                {
                    // show_message(sprintf($GLOBALS['_LANG']['stock_insufficiency'], $row['goods_name'],
                    // $product_number['product_number'], $product_number['product_number']));
                    // exit;
                    return array(
                        'error' => 1,
                        'message' => sprintf(_L('global_cart_quantity_exceeded2', '非常抱歉，您選購的「%s」數量超出購買上限 %d 個。'),
                                            $row['goods_name'], $product_number['product_number'])
                    );
                }
            }
        }
        elseif (intval($GLOBALS['_CFG']['use_storage']) > 0 && $goods['extension_code'] == 'package')
        {
            if (judge_package_stock($goods['goods_id'], $val))
            {
                // show_message($GLOBALS['_LANG']['package_stock_insufficiency']);
                // exit;
                return array(
                    'error' => 1,
                    'message' => _L('global_cart_out_of_stock', '您選購的產品已經售罄。')
                );
            }
        }
       
        /* 查询：检查该项是否为基本件 以及是否存在配件 */
        /* 此处配件是指添加商品时附加的并且是设置了优惠价格的配件 此类配件都有parent_id goods_number为1 */
        $sql = "SELECT b.goods_number, b.rec_id
                FROM " .$GLOBALS['ecs']->table('cart') . " a, " .$GLOBALS['ecs']->table('cart') . " b
                WHERE a.rec_id = '$key'
                AND a.session_id = '" . SESS_ID . "'
                AND a.extension_code <> 'package'
                AND b.parent_id = a.goods_id
                AND b.session_id = '" . SESS_ID . "'";
        $offers_accessories_res = $GLOBALS['db']->query($sql);

        //订货数量大于0
        if ($val > 0)
        {
            /* 判断是否为超出数量的优惠价格的配件 删除*/
            $row_num = 1;
            while ($offers_accessories_row = $GLOBALS['db']->fetchRow($offers_accessories_res))
            {
                if ($row_num > $val)
                {
                    $sql = "DELETE FROM " . $GLOBALS['ecs']->table('cart') .
                            " WHERE session_id = '" . SESS_ID . "' " .
                            "AND rec_id = '" . $offers_accessories_row['rec_id'] ."' LIMIT 1";
                    $GLOBALS['db']->query($sql);
                }

                $row_num ++;
            }

            $sql = "UPDATE " . $GLOBALS['ecs']->table('cart') . " " . 
                    "SET goods_number = " . $val . " " . 
                    "WHERE insurance_of = '" . $goods['goods_id'] . "' " . 
                    "AND is_insurance = 1 " . 
                    "AND session_id = '" . SESS_ID . "' " . 
                    "AND goods_number > " . $val . " ";
            $GLOBALS['db']->query($sql);

            /* 处理超值礼包 */
            if ($goods['extension_code'] == 'package')
            {
                //更新购物车中的商品数量
                $sql = "UPDATE " .$GLOBALS['ecs']->table('cart').
                        " SET goods_number = '$val' WHERE rec_id='$key' AND session_id='" . SESS_ID . "'";
            }
            /* 处理普通商品或非优惠的配件 */
            else
            {
                if ($goods['is_insurance'] == 1 && $goods['insurance_of'] > 0){
                    $ins_price = get_insurance_price($goods['goods_id'], $goods['insurance_of']);
                    if ($ins_price){
                        $goods_price = $ins_price;
                        $goods_user_rank = 0;
                    } else {
                        return array(
                            'error' => 1,
                            'message' => _L('global_cart_insurance_not_exists', '您選購的保險服務不存在')
                        );
                    }
                } else {
                    $attr_id         = empty($goods['goods_attr_id']) ? array() : explode(',', $goods['goods_attr_id']);
                    $final_price     = get_final_price($goods['goods_id'], $val, true, $attr_id, $_SESSION['user_rank'], $goods['is_gift'], $goods['is_package']);
                    $goods_price     = $final_price['final_price'];
                    $goods_user_rank =  $final_price['user_rank_id'];
                }

                //更新购物车中的商品数量
                $sql = "UPDATE " .$GLOBALS['ecs']->table('cart').
                        " SET goods_number = '$val', goods_price = '$goods_price', user_rank = '$goods_user_rank' WHERE rec_id='$key' AND session_id='" . SESS_ID . "'";
            }
        }
        //订货数量等于0
        else
        {
            /* 如果是基本件并且有优惠价格的配件则删除优惠价格的配件 */
            while ($offers_accessories_row = $GLOBALS['db']->fetchRow($offers_accessories_res))
            {
                $sql = "DELETE FROM " . $GLOBALS['ecs']->table('cart') .
                        " WHERE session_id = '" . SESS_ID . "' " .
                        "AND rec_id = '" . $offers_accessories_row['rec_id'] ."' LIMIT 1";
                $GLOBALS['db']->query($sql);
            }

            $sql = "DELETE FROM " .$GLOBALS['ecs']->table('cart').
                " WHERE rec_id='$key' AND session_id='" .SESS_ID. "'";
        }

        $GLOBALS['db']->query($sql);

        // clean goods cache
        clear_cache_files("goods_".$goods['goods_id']);
        
    }

    /* Delete all gift dones't meet the requirement */
    $sql = 'SELECT SUM(goods_number) AS number, max(goods_number) AS max_number, is_gift AS act_id' .
           ' FROM ' . $GLOBALS['ecs']->table('cart') .
           " WHERE session_id = '" . SESS_ID . "' AND rec_type = '" . CART_GENERAL_GOODS . "' AND is_gift <> 0 group by is_gift";
    $gifts_res = $GLOBALS['db']->query($sql);
    while ($gifts = $GLOBALS['db']->fetchRow($gifts_res)){
        $drop_flag = false;

        $gift_sum = $gifts["number"];
        $max_gift_num = $gifts["max_number"];
        $act_id = $gifts["act_id"];

        $favourable = favourable_info($act_id);
        $message = '';
        /* Step 1: Get favourable info */
        if (empty($favourable))
        {
            $message = $_LANG['favourable_not_exist'];
            //echo $message;
            $drop_flag = true;
        }
        
        
        /* Step 4: Handle favourable type action */
        if ($favourable['act_type'] == FAT_GOODS)
        {            
            // Check goods limit
            $goods_quantity = $orderController->cart_favourable_quantity($favourable, $order_fee_platform);
            if($favourable["act_kind"]==1 || $favourable["act_kind"]==3){
                $total_act_type_ext = intval(count($favourable['gift']) * floor($goods_quantity / $favourable['min_quantity']));
                if($max_gift_num > floor($goods_quantity / $favourable['min_quantity'])){
                    //$message = "多了禮物";
                    //echo $message;
                    $drop_flag = true;
                }
            }
            else{
                $total_act_type_ext = intval($favourable['act_type_ext'] * floor($goods_quantity / $favourable['min_quantity']));
            }

            if ($total_act_type_ext > 0 && $gift_sum > $total_act_type_ext)
            {
                //$message = "多了禮物";
                //echo $message;
                $drop_flag = true;
            }

        }

        if($drop_flag){
            $sql = "DELETE FROM " .$GLOBALS['ecs']->table('cart').
                " WHERE session_id = '" . SESS_ID . "' AND rec_type = '" . CART_GENERAL_GOODS . "' AND is_gift = '".$act_id."'";
            $GLOBALS['db']->query($sql);
        }
    }

    return array(
        'error' => 0
    );
}

/**
 * 检查订单中商品库存
 *
 * @access  public
 * @param   array   $arr
 *
 * @return  void
 */
function flow_cart_stock($arr)
{
    foreach ($arr AS $key => $val)
    {
        $val = intval(make_semiangle($val));
        if ($val <= 0 || !is_numeric($key))
        {
            continue;
        }

        $sql = "SELECT `goods_id`, `goods_attr_id`, `extension_code`, `is_insurance`, `insurance_of` FROM" .$GLOBALS['ecs']->table('cart').
               " WHERE rec_id='$key' AND session_id='" . SESS_ID . "'";
        $goods = $GLOBALS['db']->getRow($sql);

        if ($goods['is_insurance'] == 0 && $goods['insurance_of'] == 0){
            $sql = "SELECT c.goods_name, g.goods_number, c.product_id ".
                    "FROM " .$GLOBALS['ecs']->table('goods'). " AS g, ".
                        $GLOBALS['ecs']->table('cart'). " AS c ".
                    "WHERE g.goods_id = c.goods_id AND c.rec_id = '$key'";
            $row = $GLOBALS['db']->getRow($sql);

            //系统启用了库存，检查输入的商品数量是否有效
            if (intval($GLOBALS['_CFG']['use_storage']) > 0 && $goods['extension_code'] != 'package')
            {
                if ($row['goods_number'] < $val)
                {
                    show_message(sprintf($GLOBALS['_LANG']['stock_insufficiency'], $row['goods_name'],
                    $row['goods_number'], $row['goods_number']));
                    exit;
                }

                /* 是货品 */
                $row['product_id'] = trim($row['product_id']);
                if (!empty($row['product_id']))
                {
                    $sql = "SELECT product_number FROM " .$GLOBALS['ecs']->table('products'). " WHERE goods_id = '" . $goods['goods_id'] . "' AND product_id = '" . $row['product_id'] . "'";
                    $product_number = $GLOBALS['db']->getOne($sql);
                    if ($product_number < $val)
                    {
                        show_message(sprintf($GLOBALS['_LANG']['stock_insufficiency'], $row['goods_name'],
                        $row['goods_number'], $row['goods_number']));
                        exit;
                    }
                }
            }
            elseif (intval($GLOBALS['_CFG']['use_storage']) > 0 && $goods['extension_code'] == 'package')
            {
                if (judge_package_stock($goods['goods_id'], $val))
                {
                    show_message($GLOBALS['_LANG']['package_stock_insufficiency']);
                    exit;
                }
            }
        }
    }

}

// New function by howang: Auto adjust quantity in cart to fit stock
function auto_adjust_cart_stock($arr)
{
    $arr2 = array();
    $adjusted_goods = array();
    foreach ($arr AS $key => $val)
    {
        $val = intval(make_semiangle($val));
        if ($val <= 0 || !is_numeric($key))
        {
            continue;
        }

        $sql = "SELECT `goods_id`, `goods_name`, `product_id`, `goods_attr_id`, `extension_code`, `flashdeal_id`, `is_insurance`, `insurance_of` FROM" .$GLOBALS['ecs']->table('cart').
               " WHERE rec_id='$key' AND session_id='" . SESS_ID . "'";
        $goods = $GLOBALS['db']->getRow($sql);

        //系统启用了库存，检查输入的商品数量是否有效
        if (intval($GLOBALS['_CFG']['use_storage']) > 0 && $goods['extension_code'] != 'package' && ($goods['is_insurance'] == 0 && $goods['insurance_of'] == 0))
        {
            $sql = "SELECT goods_name, " . hw_goods_number_sql() . ", " . hw_goods_can_overorder_sql() . " FROM (".
                    "SELECT g.goods_name, g.is_pre_sale, g.pre_sale_date, g.pre_sale_days, ".
                    hw_goods_number_subquery('g.') . ", " .
        			hw_reserved_number_subquery('g.') . " " .
                    "FROM " .$GLOBALS['ecs']->table('goods'). " AS g ".
                    "WHERE g.goods_id = '" . $goods['goods_id'] . "'".
                    ") as tmp";
            $row = $GLOBALS['db']->getRow($sql);

            // YOHO: limit quantity to 20 a time
            $row['goods_number'] = min(intval($row['goods_number']), 20);

            $old_val = $val;

            // Handle flashdeal
            if ($goods['flashdeal_id'] > 0)
            {

                if (1 < $val)
                {
                    $val = 1;
                }
            }
            // Normal goods
            else
            {
                $goods['product_id'] = trim($goods['product_id']);

                if (empty($goods['product_id']))
                {
                    if (($row['goods_number'] < $val) && (!$row['can_overorder']))
                    {
                        $val = $row['goods_number'];
                    }
                }
                else
                {
                    /* 是货品 */
                    $sql = "SELECT product_number FROM " .$GLOBALS['ecs']->table('products'). " WHERE goods_id = '" . $goods['goods_id'] . "' AND product_id = '" . $goods['product_id'] . "'";
                    $product_number = $GLOBALS['db']->getOne($sql);
                    if (($product_number < $val) && (!$row['can_overorder']))
                    {
                        $val = $product_number;
                    }
                }
            }

            // $val updated...
            if ($val != $old_val)
            {
                $arr2[$key] = $val;
                $adjusted_goods[] = array(
                    'goods_id' => $goods['goods_id'],
                    'goods_name' => $goods['goods_name'],
                    'old_number' => $old_val,
                    'new_number' => $val,
                    'is_flashdeal' => ($goods['flashdeal_id'] > 0)
                );
            }
        }
        // TODO: Handle 套裝優惠 (for now judge_package_stock() does not work with 庫存系統, so it's incorrect)
        // elseif (intval($GLOBALS['_CFG']['use_storage']) > 0 && $goods['extension_code'] == 'package')
        // {
        //     if (judge_package_stock($goods['goods_id'], $val))
        //     {
        //         $arr2[$key] = 0;
        //     }
        // }
    }

    if (count($arr2) > 0)
    {
        flow_update_cart($arr2);

        //show_message('由於某些產品存貨量已經改變，系統已自動調整您的購物車，請仔細確認後重新下單。', '返回購物車', '/cart', 'info');

        // Save the adjusted goods to session and re-display on cart page
        $_SESSION['adjusted_goods'] = $adjusted_goods;
        header('Location: /cart');
        exit;
    }
}

/**
 * 删除购物车中的商品
 *
 * @access  public
 * @param   integer $id
 * @return  void
 */
function flow_drop_cart_goods($id)
{
    /* 取得商品id */
    $sql = "SELECT * FROM " .$GLOBALS['ecs']->table('cart'). " WHERE rec_id = '$id'";
    $row = $GLOBALS['db']->getRow($sql);
    if ($row)
    {
        //如果是超值礼包
        if ($row['extension_code'] == 'package')
        {
            $sql = "DELETE FROM " . $GLOBALS['ecs']->table('cart') .
                " WHERE session_id = '" . SESS_ID . "' " .
                " AND is_package = '".$row['is_package']."'";
        }

        //如果是普通商品，同时删除所有赠品及其配件
        elseif ($row['parent_id'] == 0 && $row['is_gift'] == 0 && $row['is_insurance'] == 0)
        {
            
            /* 检查购物车中该普通商品的不可单独销售的配件并删除 */
            $sql = "SELECT c.rec_id
                    FROM " . $GLOBALS['ecs']->table('cart') . " AS c, " . $GLOBALS['ecs']->table('group_goods') . " AS gg, " . $GLOBALS['ecs']->table('goods'). " AS g
                    WHERE gg.parent_id = '" . $row['goods_id'] . "'
                    AND c.goods_id = gg.goods_id
                    AND c.parent_id = '" . $row['goods_id'] . "'
                    AND c.extension_code <> 'package'
                    AND gg.goods_id = g.goods_id
                    AND g.is_alone_sale = 0";

                    $res = $GLOBALS['db']->query($sql);
            $_del_str = $id . ',';
            while ($id_alone_sale_goods = $GLOBALS['db']->fetchRow($res))
            {
                $_del_str .= $id_alone_sale_goods['rec_id'] . ',';
            }
            $_del_str = trim($_del_str, ',');

            $sql = "DELETE FROM " . $GLOBALS['ecs']->table('cart') .
                    " WHERE session_id = '" . SESS_ID . "' " .
                    "AND (rec_id IN ($_del_str) OR parent_id = '$row[goods_id]' OR is_gift <> 0 OR is_package <> 0)";


            $insuranceController = new Yoho\cms\Controller\InsuranceController();
            $insurance_plan = $insuranceController->getInsByGoodID($row['goods_id']);

            if ($insurance_plan){
                $arr_insurance_list = array();
                foreach ($insurance_plan as $ins){
                    if (!in_array($ins['target_goods_id'], $arr_insurance_list)){
                        array_push($arr_insurance_list, $ins['target_goods_id']);
                    }
                }

                $str_insurance_list =  implode(',', $arr_insurance_list);

                $sql = "SELECT *
                        FROM " . $GLOBALS['ecs']->table('cart') . " AS c 
                        WHERE c.goods_id IN (" . $str_insurance_list . ") 
                        AND c.insurance_of = '" . $row['goods_id'] . "' 
                        AND c.is_insurance = 1 
                        AND c.is_package = 0 ";

                $res = $GLOBALS['db']->query($sql);

                $_del_str = $id . ',';
                while ($id_alone_sale_goods = $GLOBALS['db']->fetchRow($res))
                {
                    $_del_str .= $id_alone_sale_goods['rec_id'] . ',';
                }
                $_del_str = trim($_del_str, ',');

                $sql = "DELETE FROM " . $GLOBALS['ecs']->table('cart') .
                        " WHERE session_id = '" . SESS_ID . "' " .
                        "AND (rec_id IN ($_del_str) OR (parent_id = '$row[goods_id]' and is_insurance <> 0))";
            }
        }

        //如果不是普通商品，只删除该商品即可
        else
        {
            $sql = "DELETE FROM " . $GLOBALS['ecs']->table('cart') .
                    " WHERE session_id = '" . SESS_ID . "' " .
                    "AND rec_id = '$id' LIMIT 1";
        }
        $GLOBALS['db']->query($sql);
        // Flashdeal product
        if($row['flashdeal_id'] > 0) {
            $flashdealController = new \Yoho\cms\Controller\FlashdealController();
            $flashdealController->updateFlashdealCartByECSCart();
        }

        clear_cache_files("goods_".$row['goods_id']);
    }

    flow_clear_cart_alone();
}

/**
 * 删除购物车中不能单独销售的商品
 *
 * @access  public
 * @return  void
 */
function flow_clear_cart_alone()
{
    /* 查询：购物车中所有不可以单独销售的配件 */
    $sql = "SELECT c.rec_id, gg.parent_id
            FROM " . $GLOBALS['ecs']->table('cart') . " AS c
                LEFT JOIN " . $GLOBALS['ecs']->table('group_goods') . " AS gg ON c.goods_id = gg.goods_id
                LEFT JOIN" . $GLOBALS['ecs']->table('goods') . " AS g ON c.goods_id = g.goods_id
            WHERE c.session_id = '" . SESS_ID . "'
            AND c.extension_code <> 'package'
            AND gg.parent_id > 0
            AND g.is_alone_sale = 0";
    $res = $GLOBALS['db']->query($sql);
    $rec_id = array();
    while ($row = $GLOBALS['db']->fetchRow($res))
    {
        $rec_id[$row['rec_id']][] = $row['parent_id'];
    }

    if (empty($rec_id))
    {
        return;
    }

    /* 查询：购物车中所有商品 */
    $sql = "SELECT DISTINCT goods_id
            FROM " . $GLOBALS['ecs']->table('cart') . "
            WHERE session_id = '" . SESS_ID . "'
            AND extension_code <> 'package'";
    $res = $GLOBALS['db']->query($sql);
    $cart_good = array();
    while ($row = $GLOBALS['db']->fetchRow($res))
    {
        $cart_good[] = $row['goods_id'];
    }

    if (empty($cart_good))
    {
        return;
    }

    /* 如果购物车中不可以单独销售配件的基本件不存在则删除该配件 */
    $del_rec_id = '';
    foreach ($rec_id as $key => $value)
    {
        foreach ($value as $v)
        {
            if (in_array($v, $cart_good))
            {
                continue 2;
            }
        }

        $del_rec_id = $key . ',';
    }
    $del_rec_id = trim($del_rec_id, ',');

    if ($del_rec_id == '')
    {
        return;
    }

    /* 删除 */
    $sql = "DELETE FROM " . $GLOBALS['ecs']->table('cart') ."
            WHERE session_id = '" . SESS_ID . "'
            AND rec_id IN ($del_rec_id)";
    $GLOBALS['db']->query($sql);
}

?>
