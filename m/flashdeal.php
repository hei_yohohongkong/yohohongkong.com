<?php

/***
 * flashdeal.php
 * by howang 2015-02-09
 *
 * Flash deals
 ***/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

if ((DEBUG_MODE & 2) != 2) {
    $smarty->caching = true;
}

$page = empty($_REQUEST['page']) ? 0 : intval($_REQUEST['page']);
$page = ($page < 1) ? 1 : $page;
$fleashdealController = new Yoho\cms\Controller\FlashdealController();
$flashdeal_id = empty($_REQUEST['flashdeal_id']) ? 0 : intval($_REQUEST['flashdeal_id']);
$date = empty($_REQUEST['date']) ? local_date('Ymd') : $_REQUEST['date'];
if (strlen((string)$date) < 8) {
    $date = local_date('Y') . (string)$date;
}
$time = empty($_REQUEST['time']) ? local_date('Hi') : $_REQUEST['time'];
$cid = empty($_REQUEST['cid']) ? 0 : intval($_REQUEST['cid']);
//Clear cache when flashdeal start
$now = local_strtotime('now');
$todaystarttime = local_strtotime(local_date('Y-m-d 00:00:00'));
$is_now = $db->getOne(
    "SELECT 1 ".
    "FROM ".$ecs->table('flashdeals')." AS fd ".
    "LEFT JOIN ".$ecs->table('flashdeal_event')." AS fde ON fde.fd_event_id = fd.fd_event_id ".
    "WHERE fde.status = 1 AND fd.deal_date BETWEEN '" . $todaystarttime . "' AND '" . $now . "' LIMIT 1 "
);
$clean = $db->getOne(
    "SELECT 1 ".
    "FROM ".$ecs->table('flashdeals')." AS fd ".
    "LEFT JOIN ".$ecs->table('flashdeal_event')." AS fde ON fde.fd_event_id = fd.fd_event_id ".
    "WHERE fde.status = 1 AND fd.deal_date = '" . $now . "' LIMIT 1 "
);
if($clean) {
    clear_cache_files('flashdeal');
}
$upcoming = $_REQUEST['upcoming'] ? true : false;
/*------------------------------------------------------ */
//-- 判断是否存在缓存，如果存在则调用缓存，反之读取相应内容
/*------------------------------------------------------ */
/* 缓存编号 */
$cache_id = $flashdeal_id . '-' . $_SESSION['user_rank'] . '-' . $_CFG['lang'] . '-' . $date . $time . '-' . $page . '-' . $upcoming . '-' . $cid.'-'.$is_now;
$cache_id = sprintf('%X', crc32($cache_id));
if (!$smarty->is_cached('flashdeal.html', $cache_id)) {
    assign_template();
    $position = assign_ur_here(0, _L('flashdeal_title', '友和閃購'));
    $smarty->assign('page_title', $position['title']);// 页面标题
    $smarty->assign('ur_here', $position['ur_here']); // 当前位置
    /* meta information */
    $smarty->assign('keywords', htmlspecialchars($_CFG['shop_keywords']));
    $smarty->assign('description', htmlspecialchars($_CFG['shop_desc']));
    $smarty->assign('helps', get_shop_help());       // 网店帮助
    $CategoryController = new Yoho\cms\Controller\CategoryController();
    $t1 = $CategoryController->getTopCategories(0);
    $smarty->assign('t1', $t1);
    $flashdeals = $fleashdealController->get_flashdeals_list(20, false, $page, $date, $time, [], $upcoming, $cid);
    if ($flashdeals == false) { // this day don't have flashdeal
        $flashdeals = $fleashdealController->get_flashdeals_list(20, false, $page, $date, $time, [], true, $cid);
        $upcoming = 1;
    }
    $smarty->assign('flashdeals', $flashdeals);
    if ($flashdeals['next_page']) {
        $smarty->assign('next_page', $flashdeals['next_page']);
    }
    $smarty->assign('upcoming', $upcoming);
    $smarty->assign('cid', $cid);

    $smarty->assign('config', $fleashdealController->get_flashdeal_config());

}

$smarty->display('flashdeal.html', $cache_id); //
exit;

?>