<?php

/**
 * ECSHOP 文章
 * ============================================================================
 * 版权所有 2005-2010 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liubo $
 * $Id: article.php 16455 2009-07-13 09:57:19Z liubo $
*/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
$fleashdealController = new Yoho\cms\Controller\FlashdealController();
// howang: make the parameter consistent with the non-mobile version
if (!empty($_GET['id']))
{
    $_GET['a_id'] = $_GET['id'];
}

$a_id = !empty($_GET['a_id']) ? intval($_GET['a_id']) : '';

$cache_id = sprintf('%X', crc32($a_id . '-' . $_CFG['lang']));

if (!$smarty->is_cached('article.html', $cache_id))
{
    assign_template();

    if ($a_id > 0)
    {
        $article = get_article_info($a_id);
    }
    else
    {
        header('Location: /posts');
        exit;
    }

    if (empty($article))
    {
        header('Location: /');
        exit;
    }

    if (!empty($article['link']) && $article['link'] != 'http://' && $article['link'] != 'https://')
    {
        header('Location: ' . $article['link']);
        exit;
    }

    $position = assign_mobile_ur_here($article['cat_id']);
    $smarty->assign('page_title',      $position['title']);    // 页面标题
    $smarty->assign('ur_here',         $position['ur_here']);  // 当前位置
    $CategoryController = new Yoho\cms\Controller\CategoryController();
    $t1 = $CategoryController->getTopCategories(0);
    $smarty->assign('t1',              $t1);
    $smarty->assign('helps',           get_shop_help());
    $hot_list =  get_hot_article($article['cat_id'], $a_id);
    /* Mobile: Only get the first. */
    $hot = reset($hot_list);
    $smarty->assign('hot_next', $hot);
//    $smarty->assign('hot_lists',            get_hot_article($article['cat_id'])); // Hot articles list
    /*
    //flashdeal
    $flashdeals = $fleashdealController->get_index_flashdeals(20, 'today');
    $smarty->assign('flashdeals', $flashdeals);
    if($flashdeals){
        $time_list = $fleashdealController->get_countdown();
        $smarty->assign('today_gmt_now_time', $time_list['gmt_now_time']);
        $smarty->assign('today_gmt_end_time', $time_list['gmt_end_time']);
        $smarty->assign('today_countdown',    $time_list['countdown']);
    }
    $smarty->assign('upcoming_flashdeals', get_upcoming_flashdeals(6, true));
    */
    /* meta information */
    $smarty->assign('keywords',        htmlspecialchars(empty($article['keywords']) ? $_CFG['shop_keywords'] : $article['keywords']));
    $smarty->assign('description',     htmlspecialchars(empty($article['description']) ? $_CFG['shop_desc'] : $article['description']));

    $smarty->assign('article', $article);

    // /* 相关商品 */
    $sql = "SELECT a.goods_id " .
             "FROM " . $ecs->table('goods_article') . " AS a, " . $ecs->table('goods') . " AS g " .
             "WHERE a.goods_id = g.goods_id " .
             "AND a.article_id = '$_REQUEST[id]' ";
    $goods_id = $db->getCol($sql);

    $sort_goods_arr = array();
    $sql = hw_goods_list_sql(db_create_in($goods_id, 'g.goods_id'), 'goods_id', 'DESC');
    $res = $db->getAll($sql);
    $goods_list = hw_process_goods_rows($res);

    $smarty->assign('goods_list',   $goods_list);          // 商品列表

    assign_dynamic('article');
}

$smarty->display('article.html', $cache_id);

?>