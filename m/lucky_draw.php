<?php

/***
* vip.php
* by Anthony@YOHO 20170721
*
* YOHO Hong Kong VIP page
***/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

if ((DEBUG_MODE & 2) != 2)
{
    $smarty->caching = true;
}

$user_id = $_SESSION['user_id'];
$user_rank = $_SESSION['user_rank'];
$rank_points = $_SESSION['rank_points'];

$luckyDrawController = new Yoho\cms\Controller\LuckyDrawController();
$is_lucky_draw_active = $luckyDrawController->isLuckyDrawEventActive();


if (!$is_lucky_draw_active) {
    // get the last event id
    $lucky_draw_id = $luckyDrawController->getTheLastFinishedLuckyDrawEvent();
    //echo 'error - event not ready';
} else {
    $lucky_draw_id = $luckyDrawController->getTheFirstActiveLuckyDrawEvent();
}

$lucky_draw_info = $luckyDrawController->getLuckyDrawEventInfo($lucky_draw_id);
$is_gift_enough = $luckyDrawController->checkGiftEnough($lucky_draw_id);
$user_ticket_count = $luckyDrawController->getDrawTicketCount($user_id,$lucky_draw_id);
$is_hold_lucky_draw = $luckyDrawController->isHoldLuckyDraw($lucky_draw_id);

if (empty($lucky_draw_info['intro_mobile'])) {
    $lucky_draw_info['intro_mobile'] = $lucky_draw_info['intro'];
}

setcookie('m_display', $display, gmtime() + 86400 * 7);

/* 赋值固定内容 */
assign_template();
$smarty->assign('page_title', $_LANG['lucky_draw']);  // 页面标题
$smarty->assign('ur_here',    $_LANG['lucky_draw']);  // 当前位置
$smarty->assign('helps', get_shop_help());              // 网店帮助
$smarty->assign('lucky_draw_info', $lucky_draw_info); 
$smarty->assign('is_lucky_draw_active', $is_lucky_draw_active);
$smarty->assign('is_gift_enough', $is_gift_enough);
$smarty->assign('user_ticket_count', $user_ticket_count);
$smarty->assign('user_id', $user_id);
$smarty->assign('is_hold_lucky_draw', $is_hold_lucky_draw);
$smarty->display('lucky_draw.html');
