<?php

/**
 * ECSHOP 資訊分類
 * ============================================================================
 * 版权所有 2005-2010 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liubo $
 * $Id: article.php 16455 2009-07-13 09:57:19Z liubo $
*/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
$fleashdealController = new Yoho\cms\Controller\FlashdealController();
// howang: make the parameter consistent with the non-mobile version
if (!empty($_GET['id']))
{
    $_GET['ac_id'] = $_GET['id'];
}

$cat_id = !empty($_GET['ac_id']) ? intval($_GET['ac_id']) : 0;
$cat_id = $cat_id <= 0 ? '-1' : $cat_id;

$cache_id = sprintf('%X', $id . '-' . crc32($_CFG['lang']));
$cat_type = $db->getOne("SELECT cat_type FROM " . $ecs->table('article_cat') . " WHERE cat_id = '$cat_id' ");
if($cat_type == COMMON_CAT) $dwt = 'article_list.html';
else $dwt = 'statics_page_cat.html';
/* TODO: statics_page_cat is a hard code page(short url is config in .htaccess):
 * about/ =  post/52
 * supplier/ =  post/27
 * investors/ =  post/53
 * contact/ =  post/4
 * join-us/ =  post/29
*/
if (!$smarty->is_cached($dwt, $cache_id))
{
    assign_template();

    if ($cat_id == '-1')
    {
        $article_cat = array(
            'cat_name' => _L('article_list', '所有資訊'),
            'keywords' => $_CFG['shop_keywords'],
            'cat_desc' => $_CFG['shop_desc']
        );
    }
    else
    {
        $sql = "SELECT IFNULL(acl.cat_name, ac.cat_name) as cat_name, " .
                "IFNULL(acl.keywords, ac.keywords) as keywords, " .
                "IFNULL(acl.cat_desc, ac.cat_desc) as cat_desc, acperma " . 
                "FROM " . $ecs->table('article_cat') . " as ac " .
                "LEFT JOIN " . $ecs->table('article_cat_lang') . " as acl ON acl.cat_id = ac.cat_id AND acl.lang = '" . $_CFG['lang'] . "' " .
                'LEFT JOIN (SELECT id, table_name, lang, perma_link as acperma FROM ' . $GLOBALS['ecs']->table('perma_link') . ') cpl ON cpl.id = ac.cat_id  AND cpl.table_name = "article_cat" AND cpl.lang = "' . $GLOBALS['_CFG']['lang'] . '"' .
                "WHERE ac.cat_id = '" . $cat_id . "' AND ac.cat_type NOT IN(".SUPPORT_CAT.")";
        $article_cat = $db->getRow($sql);
    }
    $smarty->assign('cat_title', $article_cat['cat_name']);

    $position = assign_mobile_ur_here($cat_id, $article_cat['cat_name']);
    $smarty->assign('page_title',      $position['title']);    // 页面标题
    $smarty->assign('ur_here',         $position['ur_here']);  // 当前位置
    $CategoryController = new Yoho\cms\Controller\CategoryController();
    $t1 = $CategoryController->getTopCategories(0);
    $smarty->assign('t1',              $t1);
    $smarty->assign('helps',           get_shop_help());
    $smarty->assign('hot_lists',            get_hot_article($cat_id,0,1)); // Hot articles list

    /* meta information */
    $smarty->assign('keywords',        htmlspecialchars($article_cat['keywords']));
    $smarty->assign('description',     htmlspecialchars($article_cat['cat_desc']));
/*
    //flashdeal
    $flashdeals = $fleashdealController->get_index_flashdeals(20, 'today');
    $smarty->assign('flashdeals', $flashdeals);
    if($flashdeals){
        $time_list = $fleashdealController->get_countdown();
        $smarty->assign('today_gmt_now_time', $time_list['gmt_now_time']);
        $smarty->assign('today_gmt_end_time', $time_list['gmt_end_time']);
        $smarty->assign('today_countdown',    $time_list['countdown']);
    }
    $smarty->assign('upcoming_flashdeals', get_upcoming_flashdeals(6, true));*/
    $tree = article_categories_tree($cat_id);
    $smarty->assign('article_categories', $tree); //資訊分类树
    $article_num = get_article_count($cat_id);
    $count = $db->getOne("SELECT COUNT(*) FROM " . $ecs->table('article') . " WHERE is_open = 1 AND cat_id = " .$cat_id);
    /* If count > 0: have article in this category, we using article list layout with select */
    if (!empty($tree[$cat_id]['children']) && $count == 0) {
        foreach ($tree[$cat_id]['children'] as $key => $value) {
            $article_array = get_cat_articles($key, 1, 7, '', 'order');
            if (count($article_array) >= 4 && count($article_array) < 7) {
                $add_fake_total = 7-count($article_array);
                $last = end($article_array);
                for ($i =0;$i < $add_fake_total;$i++) {
                    array_push($article_array, $last);
                }
            }
            $tree[$cat_id]['children'][$key]['article'] = array_values($article_array);
        }
        $smarty->assign('on_sale_goods_list', get_promote_goods());
        $smarty->assign('article_children', $tree[$cat_id]['children']); //資訊分类树
    } else if ($article_num > 0) {
        if(!empty($tree[$cat_id]['children'])) {
            /* We get sub cat list to options */
            $opt = [];
            foreach ($tree[$cat_id]['children'] as $key => $value) {
               $opt[] = ['value'=>$value['cat_id'], 'name'=>$value['cat_name']];
            }
            $smarty->assign('sub_cat_options', $opt);
        }
        $page_num = 10;
        $page = !empty($_GET['page']) ? intval($_GET['page']) : 1;
        $pages = ceil($article_num / $page_num);
        if ($page <= 0)
        {
            $page = 1;
        }
        if ($pages == 0)
        {
            $pages = 1;
        }
        if ($page > $pages)
        {
            $page = $pages;
        }
        $pager = get_mobile_pager($article_num, $page_num, $page, build_uri('article_cat', array('acid' => $cat_id, 'acperma' => $article_cat['acperma']), $cat_title), 'page');
        $smarty->assign('pager', $pager);
        $article_array = get_cat_articles($cat_id, $page, $page_num, '', 'order');
        $i = 1;
        foreach ($article_array as $key => $article_data)
        {
            $article_array[$key]['i'] = $i;
            $article_array[$key]['title'] = encode_output($article_data['title']);
            $i++;
        }
        $smarty->assign('article_array', $article_array);
    }
}

$smarty->display($dwt, $cache_id);

?>