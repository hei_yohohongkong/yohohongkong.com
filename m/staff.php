<?php

/***
 * staff.php
 * by howang 2014-08-08
 *
 * YOHO Hong Kong staff list
 ***/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

if ((DEBUG_MODE & 2) != 2)
{
	$smarty->caching = true;
}

/*------------------------------------------------------ */
//-- 判断是否存在缓存，如果存在则调用缓存，反之读取相应内容
/*------------------------------------------------------ */
/* 缓存编号 */
$cache_id = sprintf('%X', crc32($_SESSION['user_rank'] . '-' . $_CFG['lang']));
if (!$smarty->is_cached('staff.html', $cache_id))
{
	assign_template();
	$position = assign_ur_here(0, '友和團隊');
	$smarty->assign('page_title',	$position['title']);	 // 页面标题
	$smarty->assign('ur_here',		$position['ur_here']); // 当前位置
	/* meta information */
	$smarty->assign('keywords',		htmlspecialchars($_CFG['shop_keywords']));
	$smarty->assign('description',	htmlspecialchars($_CFG['shop_desc']));
	
	$smarty->assign('helps',         get_shop_help()); // 网店帮助
	
	$CategoryController = new Yoho\cms\Controller\CategoryController();
    $t1 = $CategoryController->getTopCategories(0);
    $smarty->assign('t1',              $t1);
	
	$sql = "SELECT s.*, au.avatar FROM " . $ecs->table('salesperson') . " s left join ".  $ecs->table('admin_user') ." au on (s.admin_id = au.user_id) WHERE s.`available` = 1";
	$staffs = $db->getAll($sql);
	$smarty->assign('staffs',        $staffs);
	
	$smarty->assign('stars',         range(1,10));
	
	/* 页面中的动态内容 */
	assign_dynamic('index');
}
$smarty->display('staff.html', $cache_id);

exit;

?>