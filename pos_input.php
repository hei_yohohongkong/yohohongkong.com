<?php

/***
 * pos_input.php
 * by howang 2014-08-26
 *
 * User input page, POS System for YOHO Hong Kong
 ***/

define('IN_ECS', true);

define('DESKTOP_ONLY', true); // This page don't have mobile version

define('INIT_NO_USERS', true); // Skip session for faster loading

require(dirname(__FILE__) . '/includes/init.php');

if ((DEBUG_MODE & 2) != 2)
{
	$smarty->caching = true;
}

// Check admin login
$admin = check_admin_login();

$action = empty($_REQUEST['act']) ? '' : $_REQUEST['act'];

$saved = 0;

if ($action == 'save_user_input')
{
	if (!empty($_POST['mobile']))
	{
		$data = array(
			'ccc' => trim($_POST['ccc']),
			'mobile' => trim($_POST['mobile']),
			'user_name' => '+' . trim($_POST['ccc']) . '-' . trim($_POST['mobile']),
			'email' => empty($_POST['email']) ? '' : trim($_POST['email']),
			'add_time' => gmtime()
		);
		
		if (!empty($admin['user_id']))
		{
			$data['admin_id'] = $admin['user_id'];
		}
		
		$db->autoExecute($ecs->table('pos_user_input'), $data, 'INSERT');
		
		$saved = 1;
	}
	else
	{
		$saved = 2;
	}
}

/*------------------------------------------------------ */
//-- 判断是否存在缓存，如果存在则调用缓存，反之读取相应内容
/*------------------------------------------------------ */
/* 缓存编号 */
$cache_id = sprintf('%X', crc32($saved  . '-' . $_SESSION['user_rank'] . '-' . $_CFG['lang']));
if (!$smarty->is_cached('pos_input.dwt', $cache_id))
{
	assign_template();
	$position = assign_ur_here(0, 'POS 系統');
	$smarty->assign('page_title',	 $position['title']);	// 页面标题
	$smarty->assign('ur_here',		 $position['ur_here']);  // 当前位置
	/* meta information */
	$smarty->assign('keywords',		 htmlspecialchars($_CFG['shop_keywords']));
	$smarty->assign('description',	 htmlspecialchars($_CFG['shop_desc']));
	
	$smarty->assign('saved', $saved);
	
	$smarty->assign('available_register_countries', available_register_countries());
}
$smarty->display('pos_input.dwt', $cache_id);
exit;

?>