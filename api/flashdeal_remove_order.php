<?php

use function GuzzleHttp\json_decode;

define('IN_ECS', true);

require dirname(dirname(__FILE__)) . '/includes/init.php';
require dirname(dirname(__FILE__)) . '/includes/lib_order.php';
require dirname(dirname(__FILE__)) . '/includes/lib_transaction.php';

$headers = apache_request_headers();
if (isset($headers['X-Auth-Token'])) {
    if ($headers['X-Auth-Token'] == "NueU0nlhh4911uqnE1WlJuofRVOxRs5oqrVAI33gA") {
        if (!empty($_REQUEST['order_ids'])) {
            $orders = explode(",", $_REQUEST['order_ids']);
            foreach ($orders as $order_id) {
                cancel_order($order_id);
            }
            die(json_encode(["error" => 0, "msg" => "success"]));
        }
        die(json_encode(["error" => 1, "msg"=>"empty order"]));
    }
    die(json_encode(["error" => 1, "msg"=>"auth fail"]));
}
die(json_encode(["error" => 1, "msg"=>"sth went wrong"]));
?>