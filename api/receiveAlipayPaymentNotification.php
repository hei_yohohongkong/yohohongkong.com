<?php

define('IN_ECS', true);

// Skip update user info for faster loading
define('INIT_NO_UPDATE_USER_INFO', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');
require(dirname(dirname(__FILE__)) . '/includes/lib_order.php');
require_once(dirname(dirname(__FILE__)) . '/includes/lib_payment.php');
$alipayController = new Yoho\cms\Controller\AlipayController();
$orderController = new Yoho\cms\Controller\OrderController();
/*------------------------------------------------------ */
//-- INPUT
/*------------------------------------------------------ */
$payment_info = null;

// Verify notification

// Get Payment Wallet
$order_sn = substr(explode("_", trim($_POST['out_trade_no']))[0], 0, 13);
$order_id = $orderController->getOrderIdBySn($order_sn);
$order_info = order_info($order_id);
$wallet = (!empty($order_info['origin_pay_code']) && in_array(strtolower($order_info['origin_pay_code']), ['alipayhk', 'alipaycn'])) ? strtolower($order_info['origin_pay_code']) : 'alipayhk';
$payment_info = get_payment($wallet); // Set default is alipayhk by default

if ($wallet == 'alipayhk') {
    $host_name = $payment_info['alipayhk_api_base_gateway'];
    $sign_key = $payment_info['alipayhk_md5_signature_key'];
    $pid = $payment_info['alipayhk_merchant_pid'];
}
elseif ($wallet == 'alipaycn') {
    // AliapyCN is not implemented yet
    $host_name = $payment_info['alipaycn_api_base_gateway'];
    $sign_key = $payment_info['alipaycn_md5_signature_key'];
    $pid = $payment_info['alipaycn_merchant_pid'];
}

$parameter = [
    'service' => 'notify_verify',
    'partner' => $pid,
    'notify_id' => $_POST['notify_id']
];

$sign  = '';
ksort($parameter);
reset($parameter);
foreach ($parameter AS $key => $val)
{
    $sign  .= ecs_iconv(EC_CHARSET, 'utf-8', $key) . "=" . ecs_iconv(EC_CHARSET, 'utf-8', $val). "&";
}

$sign  = substr($sign, 0, -1). ecs_iconv(EC_CHARSET, 'utf-8', $sign_key);

$parameter['sign_type'] = 'MD5';
$parameter['sign'] = md5($sign);

$ch = curl_init($host_name);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($parameter));
curl_setopt($ch, CURLOPT_HTTPHEADER, [
    "Content-Type: application/x-www-form-urlencoded",
    "Content-Length: " . strlen(http_build_query($parameter)),
]);

$verification_result = curl_exec($ch);
if (curl_errno($ch) != 0) // cURL error
{
    hw_error_log('Cannot connect to Alipay: ' . curl_error($ch));
    curl_close($ch);
    echo json_encode([
        'status' => 0,
        'error' => 'Notification Verification failed: Unable to connect to Payment Service'
    ]);
    exit;
}
else
{
    curl_close($ch);
    if ($verification_result == "False") {
        hw_error_log('Notification Verification Query failed: Notification Source/ Timing error.');
        echo json_encode([
            'status' => 0,
            'error' => 'Notification Verification Query failed: Notification Source/ Timing error.'
        ]);
        exit;
    }
}

// Verify signature and data
$sign = $_POST['sign'];
$data = null;
$data_string = '';

$data = $_POST;
unset($data['sign']);
unset($data['sign_type']);

ksort($data);
reset($data);
foreach ($data AS $key => $val)
{
    $data_string  .= ecs_iconv(EC_CHARSET, 'utf-8', $key) . "=" . ecs_iconv(EC_CHARSET, 'utf-8', $val). "&";
}

$data_string  = substr($data_string, 0, -1). ecs_iconv(EC_CHARSET, 'utf-8', $sign_key);

if (!$sign)
{
    echo 'FAILED';
    exit;
}

if ($sign != md5($data_string)) {
    echo 'FAILED';
    exit;
}

// Echo success to server, then continue execution
echo "SUCCESS";

$insert_result = $alipayController->insertAsyncNotification($data, $wallet);
// Mark order is paid only when order status is "TRADE_FINISHED"
if($insert_result && $data['trade_status'] == "TRADE_FINISHED"){
    $log_id = substr(explode('_', trim($data['out_trade_no']))[0], 13);

    if (!empty($log_id)) {
        $log = $alipayController->getPaymentLogByLogId($log_id);

        $orderController->order_payment_transaction_log($log_id, $payment_info['pay_id'], $data['trade_no'], 'HKD', $log['order_amount'], 0);

        $action_note = $payment_info['pay_code'] . ':' . $data['trade_no'] . "（HKD $log[order_amount]）";
        order_paid($log_id,PS_PAYED,$action_note);
    }
    

}

?>
