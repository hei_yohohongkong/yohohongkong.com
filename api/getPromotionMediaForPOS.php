<?php
/***
* getPromotionMediaForPOS.php
*
* AJAX get ad media, display on pos machine
***/
use Yoho\cms\Model;

define('IN_ECS', true);

// Skip template engine for faster loading
define('INIT_NO_SMARTY', true);

// If the user is already logged in, we will replace the session with new login session, 
// so updating user info for logged in user here is meaningless
define('INIT_NO_UPDATE_USER_INFO', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');

// https://www.apple.com/105/media/hk/iphone-11-pro/2019/3bd902e4-0752-4ac1-95f8-6225c32aec6d/films/product/iphone-11-pro-product-tpl-hk-2019_1280x720h.mp4

$ad_pos_model = new Model\AdPos();
$ad = $ad_pos_model->get_one_valid_ad();

if($ad) {
    $result = ['status' => true,'data' => $ad];
} else {
    $result = ['status' => false];
}

header('Content-Type: application/json');
echo json_encode($result);
exit;
?>