<?php
define('IN_ECS', true);

require dirname(dirname(__FILE__)) . '/includes/init.php';

if (!empty($_REQUEST['ParentCallSid']) && !empty($_REQUEST['CallSid'])) {
    $crmController = new Yoho\cms\Controller\CrmController();
    $ticket_id = $crmController->createTicket($_REQUEST['CallSid'], $_REQUEST['From'], CRM_PLATFORM_TWILIO, $_REQUEST['From'], date('Y-m-d H:i:s'));
    $list_id = $db->getOne("SELECT list_id FROM " . $ecs->table("crm_list") . " WHERE unique_id = '$_REQUEST[CallSid]'");
    if ($_REQUEST['CallStatus'] == "initiated") {
        if (!empty($list_id)) {
            $db->query("INSERT INTO " . $ecs->table("crm_twilio_call") . " (list_id, duration, levels) VALUES ($list_id, -1, '$_REQUEST[level]')");
        }
    } else {
        $db->query("INSERT INTO " . $ecs->table("sse") . " (time, target, event, data) VALUES (" . gmtime() . ", 'crm', 'twilio', '$ticket_id-$_REQUEST[CallStatus]')");
        if ($_REQUEST['CallStatus'] == "completed") {
            if (!empty($list_id)) {
                $db->query("UPDATE " . $ecs->table("crm_twilio_call") . " SET duration = $_REQUEST[CallDuration] WHERE list_id = $list_id");
            }
        }
    }
}

if ($_REQUEST['DequeueResult'] == "failed") {
    $message = "系統出錯了";
} elseif ($_REQUEST['DequeueResult'] == "bridged" && !empty($_REQUEST['DequeuedCallSid'])) {
    $call = $db->getRow("SELECT * FROM " . $ecs->table("crm_list") . " WHERE unique_id = '$_REQUEST[DequeuedCallSid]'");
    if (!empty($call)) {
        $caller = explode("TwilioClient", $_REQUEST['Caller'])[1];
        $admin_id = $db->getOne("SELECT user_id FROM " . $ecs->table("admin_user") . " WHERE LOWER(user_name) = LOWER('$caller')");
        if (empty($admin_id)) {
            $admin_id = 0;
        }
        $db->query("INSERT INTO " . $ecs->table("sse") . " (time, target, event, data) VALUES (" . gmtime() . ", 'crm', 'twilio', '$call[ticket_id]-completed')");
        $db->query("UPDATE " . $ecs->table("crm_twilio_call") . " SET duration = $_REQUEST[DequeuedCallDuration], admin_id = '$admin_id' WHERE list_id = $call[list_id]");
        $db->query("UPDATE " . $ecs->table('crm_ticket_info') . " SET status = " . CRM_STATUS_REPLIED . ", last_update = CURRENT_TIMESTAMP WHERE ticket_id = $call[ticket_id]");
        $db->query("INSERT INTO " . $ecs->table('crm_log') . " (ticket_id, admin_id, type) VALUES ($call[ticket_id], 1, " . CRM_LOG_TYPE_UPDATE . ")");
        $db->query("DELETE FROM " . $ecs->table("crm_twilio_call") . " WHERE admin_id = '0' AND waited = '0' AND duration = '-1' AND list_id < $call[list_id]");
    } else {
        $db->query("INSERT INTO dem_test_abnormal (logs) VALUES ('" . json_encode($_REQUEST) . "')");
    }
    $message = "通話終結";
} elseif ($_REQUEST['DequeueResult'] == "queue-empty") {
    $message = "目前未有客人等待中";
}

if (!empty($_REQUEST['DequeueResult'])) {
    header('Content-Type: text/xml');
    echo '<?xml version="1.0" encoding="UTF-8"?><Response><Say voice="alice" language="zh-HK">' . $message . '</Say><Hangup/></Response>';
    exit;
}
?>