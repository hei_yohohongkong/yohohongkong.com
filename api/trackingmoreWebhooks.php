<?php

/**
 * TrackingMore Webhooks: handle track status update.
 * https://www.trackingmore.com/webhook-tw.html
 * 2017-11-08 Anthony@YOHO
 */

define('IN_ECS', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');
require(dirname(dirname(__FILE__)) . '/includes/lib_order.php');
require_once(ROOT_PATH  . '/includes/lib_trackingmore.php');
require_once(ROOT_PATH . 'includes/cls_json.php');

// Retrieve the request's body and parse it as JSON
$input   = @file_get_contents("php://input");
$request = json_decode($input, true);
// Our trackingmore user is using franz email:
$useremail  = "franzwu@yohohongkong.com";
$timeStr    = $request['verifyInfo']['timeStr'];
$signature  = $request['verifyInfo']['signature'];
$verify     = verify($timeStr,$useremail,$signature);

if($verify){
    /** data Format JSON:
     * "data": {
        "id": "",
        "tracking_number": "",
        "carrier_code": "",
        "status": "",
        "created_at": "",
        "updated_at": "",
        "title": "",
        "order_id": "",
        "customer_name": ,
        "original_country": "",
        "destination_country": "",
        "itemTimeLength": 00,
        "origin_info": {
        "weblink": "",
        "phone": null,
        "carrier_code": "",
        "trackinfo": [
            {
            "Date": "2017-09-18 10:29",
            "StatusDescription": "",
            "Details": ""
            },
        ]
        },
        "destination_info": {
        "weblink": "",
        "phone": null,
        "carrier_code": "",
        "trackinfo": [
            {
            "Date": "2017-09-16 16:41:00",
            "StatusDescription": "",
            "Details": ""
            },

        ]
        }
        }
    */
    $data = $request['data'];
    extract($data);
    // If track have destination_info, get by destination_info(收貨地送貨資料), else get origin_info(送貨地送貨資料);
    $trackinfo = !empty($destination_info['trackinfo']) ? $destination_info['trackinfo'] : $origin_info['trackinfo'];
    $last_status_date = local_strtotime($trackinfo[0]['Date']); // Only get the first date.
    $first_info = end($trackinfo);
    $start_status_date = local_strtotime($first_info['Date']);
    // Update the delivery order:
    $GLOBALS['db']->query('START TRANSACTION');

    $sql = "UPDATE ".$GLOBALS['ecs']->table("delivery_order").
    " SET track_status = '$status' , track_status_date = '$last_status_date', track_start_date = '$start_status_date' ".
    "WHERE order_id = '$order_id' AND invoice_no = '$tracking_number'";
    if(!$GLOBALS['db']->query($sql)){
        $GLOBALS['db']->query('ROLLBACK');
        exit();
    }
    $order = order_info($order_id);
    // Order log
    if(!order_action($order['order_sn'], $order['order_status'], $order['shipping_status'], $order['pay_status'], '物流狀態更新:'.$status, 'Trackingmore Webhooks')){
        $GLOBALS['db']->query('ROLLBACK');
        exit();
    }
    $GLOBALS['db']->query('COMMIT');
} else {
    die('Hacking attempt');
}


/** Signature Verification:
 * You can verify safely about the source of POST data to make sure it is from Trackingmore. You can also analyze the POST data directly without verification.
 * Below are steps about safety verification.
 * 1. Analyze the two parameters ("timeStr" and "signature") in the POST data.
 * 2. Use your registered email address on Trackingmore and the parameter "timeStr" to generate "signature", and then verify the generated "signature" with the signature from POST data. Note: Signature Algorithm is SHA256.
 */
function verify($timeStr,$useremail,$signature){
    $hash="sha256";
    $result=hash_hmac($hash,$timeStr,$useremail);
    return strcmp($result,$signature)==0?1:0;
}