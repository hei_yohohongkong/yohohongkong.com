<?php

/***
 * admin_stock_alert.php
 * by howang 2015-09-01
 *
 * PHP Script for sending email receipt in background invoked by cron
 ***/

//  Disabled by Dem at 11-01-2019
exit;

if (extension_loaded('newrelic'))
{
	newrelic_background_job(true);
}

define('IN_ECS', true);

require(dirname(dirname(dirname(__FILE__))) . '/includes/init.php');
require(dirname(dirname(dirname(__FILE__))) . '/includes/lib_order.php');
$erpController = new Yoho\cms\Controller\ErpController();
if (!empty($_GET['cron']) && ($_SERVER['REMOTE_ADDR'] == $_SERVER['SERVER_ADDR']))
{
	set_time_limit(900); // 15 minutes
	
	// Get goods meet 呆壞貨 criteria
	$time_yesterday = local_strtotime('today'); // 23:59:59 yesterday
	$time_30days_before = local_strtotime('-30 days midnight'); // 00:00:00 30 days ago
	$sql = "SELECT g.goods_id, g.shop_price, g.cost, " . 
				"IFNULL((SELECT sum(ega.goods_qty) FROM " . $ecs->table('erp_goods_attr_stock') . " as ega WHERE ega.goods_id = g.goods_id AND ega.warehouse_id IN (".$erpController->getSellableWarehouseIds(true).")), 0) as goods_number, " .
				"IFNULL((SELECT SUM(og.goods_number) FROM " . $ecs->table('order_goods') . " as og LEFT JOIN " . $ecs->table('order_info') . " as oi ON oi.order_id = og.order_id WHERE og.goods_id = g.goods_id " . order_query_sql('finished', 'oi.') . " AND (oi.shipping_time BETWEEN " . $time_30days_before . " AND " . $time_yesterday . ")), 0) as thirtydayssales " .
			"FROM " . $ecs->table('goods'). " as g " .
			"WHERE g.is_delete = 0 AND g.is_on_sale = 1 AND g.is_real = 1 AND (g.is_promote = 0 OR g.promote_end_date < " . gmtime() . ") " .
			"HAVING goods_number > 0 AND (thirtydayssales = 0 OR (thirtydayssales > 0 AND goods_number * 3 >= thirtydayssales * 5))";
	$res = $db->getAll($sql);
	
	// // Randomly pick 25% of 呆壞貨
	// shuffle($res);
	// $count = count($res);
	// $res = array_slice($res, 0, round($count / 4));
	
	// Update: Randomly pick up to 500 呆壞貨
	shuffle($res);
	$count = count($res);
	if ($count > 500)
	{
		$res = array_slice($res, 0, 500);
	}
	
	// Insert them to the suggestion table
	$current_time = gmtime();
	$sql = "INSERT INTO " . $ecs->table('suggested_promotions') . " (`suggest_time`, `goods_id`, `goods_price`, `cost`, `suggested_price`, `start_time`, `end_time`, `status`) VALUES " .
		implode(',', array_map(function ($goods) use ($current_time) {
			$suggested_price = round(((double)$goods['shop_price'] - (double)$goods['cost']) * 0.75 + (double)$goods['cost']);
			return "('" . $current_time  . "', '" . $goods['goods_id'] . "', '" . $goods['shop_price'] . "', '" . $goods['cost'] . "', '" . $suggested_price . "', '" . local_strtotime('monday') . "', '" . local_strtotime('monday + 6 day') . "', 'pending')";
		}, $res));
	$db->query($sql);
	exit;
}

function get_secure_site_url()
{
	$old_https = isset($_SERVER['HTTPS']) ? $_SERVER['HTTPS'] : null;
	$_SERVER['HTTPS'] = 'on';
	$secure_site_url = $GLOBALS['ecs']->url();
	if (is_null($old_https))
	{
		unset($_SERVER['HTTPS']);
	}
	else
	{
		$_SERVER['HTTPS'] = $old_https;
	}
	return $secure_site_url;
}

?>