<?php

/***
 * proddb_sync.php
 * by howang 2015-09-29
 *
 * PHP Script for sync'ing data from Product Database
 ***/

if (extension_loaded('newrelic'))
{
	newrelic_background_job(true);
}

define('IN_ECS', true);

require(dirname(dirname(dirname(__FILE__))) . '/includes/init.php');

require ROOT_PATH . ADMIN_PATH . '/includes/lib_proddb.php';

set_time_limit(0);

if (!empty($_GET['cron']) && ($_SERVER['REMOTE_ADDR'] == $_SERVER['SERVER_ADDR']))
{
	$lockfile = ROOT_PATH . 'data/proddb_sync.lock';
	if (file_exists($lockfile) && (filemtime($lockfile) > time() - 1800))
	{
		error_log('Lockfile "' . $lockfile . '" exists, aborting...');
		exit(1);
	}
	touch($lockfile);
	register_shutdown_function(function ($lockfile) {
    	unlink($lockfile);
	}, $lockfile);
	
	// Get all proddb_id associated with goods, and their sync time
	$sql = "SELECT proddb_id as product_id, min(proddb_sync_time) as last_update " .
			"FROM " . $ecs->table('goods') .
			"WHERE proddb_id > 0 " .
			"GROUP BY proddb_id";
	$res = $db->getAll($sql);
	
	// Call API to retrieve updates
	$updates = $proddb->get_product_updates($res);
	
	foreach ($updates as $update)
	{
		// Save the updated descriptions and update sync time
		$param = array(
			'goods_brief' => $update['product']['product_brief'],
			'goods_desc' => $update['product']['product_desc'],
			'proddb_sync_time' => $update['product']['last_update'],
		);
		$param = addslashes_deep($param);
		$db->autoExecute($ecs->table('goods'), $param, 'UPDATE', " `proddb_id` = '" . $update['product']['product_id'] . "'");
		
		// Get goods_id associated with this proddb_id
		$sql = "SELECT goods_id " .
				"FROM " . $ecs->table('goods') .
				"WHERE proddb_id = '" . $update['product']['product_id'] . "'";
		$goods_ids = $db->getCol($sql);
		
		// Also save localized versions
		if (empty($update['localized_versions']))
		{
			// No localized version, remove existing data
			$sql = "UPDATE " . $ecs->table('goods_lang') .
					"SET goods_brief = NULL, goods_desc = NULL " .
					"WHERE goods_id " . db_create_in($goods_ids);
			$db->query($sql);
		}
		else
		{
			$localized_versions = array();
			foreach ($update['localized_versions'] as $key => $version)
			{
				$localized_versions[$key] = array(
					'goods_brief' => $version['product_brief'],
					'goods_desc' => $version['product_desc'],
				);
			}
			$localized_versions = json_encode($localized_versions);
			foreach ($goods_ids as $goods_id)
			{
				save_localized_versions('goods', $goods_id, $localized_versions);
			}
		}
		
		foreach ($goods_ids as $goods_id)
		{
			error_log(basename(__FILE__) . ': Updated goods #' . $goods_id . ' with anyspecs product #' . $update['product']['product_id']);
		}
	}
	
	// If updated, clear cache
	if (!empty($updates))
	{
		/* 清空缓存 */
	    clear_cache_files();
	}
	
	exit;
}

?>