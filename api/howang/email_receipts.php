<?php

/***
 * email_receipts.php
 * by howang 2014-09-01
 *
 * PHP Script for sending email receipt in background invoked by cron
 ***/

if (extension_loaded('newrelic'))
{
	newrelic_background_job(true);
}

define('IN_ECS', true);

require(dirname(dirname(dirname(__FILE__))) . '/includes/init.php');
try{
	$orderController = new Yoho\cms\Controller\OrderController();
	$userController = new Yoho\cms\Controller\UserController();
}catch(Exception $e){
	$orderController = false;
	$userController = false;
}


require ROOT_PATH . 'core/lib_payload.php';
require ROOT_PATH . 'includes/lib_order.php';

if(!empty($_GET['cron']))
{
	//TODO: adding back salesagent constant after merge ,'".$orderController::DB_ORDER_TYPE_SALESAGENT."'
	$exclusion = (!$orderController||!$userController?' ':" AND (order_type NOT IN ('".$orderController::DB_ORDER_TYPE_WHOLESALE."') OR order_type is null) AND user_id NOT IN ('".implode("','", $userController->getWholesaleUserIds())."') ");
	$sql = "SELECT `order_id` " .
		   "FROM " . $ecs->table('order_info') . " " .
		   "WHERE `platform` = 'pos' AND `receipt_sent` = 0 AND `add_time` < " . (gmtime() - 900) . " " . order_query_sql('finished') .
		   $exclusion.
		   "ORDER BY `order_id` DESC LIMIT 5";
	$orders = $db->getCol($sql);
	
	if (is_array($orders))
	{
		// Get our own URL
		$api_url = 'http' . ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ? 's' : '') . '://' . (empty($_SERVER['HTTP_HOST']) ? $_SERVER['SERVER_NAME'] : $_SERVER['HTTP_HOST']) . $_SERVER['PHP_SELF'];
		
		foreach ($orders as $order_id)
		{
			$obj = array('act' => 'send_receipt', 'order_id' => $order_id);
			$payload = base64_encode(encode_payload($obj));
			//echo 'curl -d "payload=' . rawurlencode($payload) . '" ' . $api_url . ' > /dev/null 2>&1 &' . "\n";
			exec('curl -d "payload=' . rawurlencode($payload) . '" ' . $api_url . ' > /dev/null 2>&1 &');
		}
		
		// Set "sent" flag, so it won't be send over and over
		$sql = "UPDATE " . $ecs->table('order_info') . " " .
			   "SET `receipt_sent` = 1 " .
			   "WHERE `order_id` " . db_create_in($orders);
		$db->query($sql);
	}
	exit;
}

$payload = isset($_REQUEST['payload']) ? $_REQUEST['payload'] : '';
if (empty($payload)) exit;
$obj = decode_payload(base64_decode($payload));
if (!is_array($obj)) exit;
$act = isset($obj['act']) ? $obj['act'] : '';

if ($act == 'send_receipt')
{
	$order_id = intval($obj['order_id']);
	
	$order = order_info($order_id);
	
	if (empty($order)) return;
	
	if (empty($order['email'])) return;
	
	// howang: don't send if user is using default system-generated email address
	if (is_placeholder_email($order['email'])) return;
	
	// check bounce list
	if (!can_send_mail($order['email'], true) || in_array($order['order_type'], [Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE])) return;
	
	require_once ROOT_PATH . 'includes/lib_erp.php';
	
	// Load additional language file
	require_once(ROOT_PATH . 'languages/' . $_CFG['lang'] . '/order_print.php');
	// Re-assign the language array to template engine
	$smarty->assign('lang', $_LANG);
	
	if ($order['user_id'] > 0)
	{
		$sql = "SELECT `password`, `ec_salt` FROM " . $ecs->table('users') . " WHERE `user_id` = '" . $order['user_id'] . "'";
		$row = $db->getRow($sql);
		
		if (($row['password'] == md5('yohohongkong')) && ($row['ec_salt'] === null))
		{
			$smarty->assign('default_password', true);
		}
	}
	
	$tpl = get_mail_template('order_receipt');
	$smarty->assign('order', $order);
	$smarty->assign('review_link', $ecs->url() . 'review.php?order_id=' . $order['order_id']);
	$smarty->assign('shop_name', $_CFG['shop_name']);
	$smarty->assign('send_date', local_date($_CFG['date_format']));
	$smarty->assign('sent_date', local_date($_CFG['date_format']));
	$content = $smarty->fetch('str:' . $tpl['template_content']);
	
	$pdfname = 'receipt_' . $order['order_sn'] . '.pdf';
	$receipt_path = ROOT_PATH . DATA_DIR . '/receiptpdf/' . $pdfname;
	if (!file_exists($receipt_path))
	{
		assign_order_print($order['order_id']);
		$smarty->assign('receipt_background', true);
		
		$old_template_dir = $smarty->template_dir;
		$smarty->template_dir = ROOT_PATH . DATA_DIR;
		$old_direct_output = $smarty->direct_output;
		$smarty->direct_output = true;
		$template = 'order_receipt.html';
		if ($order['only_market_invoice'])
		{
			$template = 'order_receipt_market.html';
		}
		$html = $smarty->fetch($template);
		$smarty->template_dir = $old_template_dir;
		$smarty->direct_output = $old_direct_output;
		
		$pdf = generate_receipt_pdf($html);
		file_put_contents($receipt_path, $pdf);
	}
	
	send_mail_with_attachment($order['consignee'], $order['email'], $tpl['template_subject'] . ' - ' . $order['order_sn'], $content, $tpl['is_html'], false, $receipt_path);
}

?>