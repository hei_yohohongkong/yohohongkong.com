<?php

/***
 * email_verify.php
 * by howang 2014-09-16
 *
 * PHP Script for sending verify email in background
 ***/

if (extension_loaded('newrelic'))
{
	newrelic_background_job(true);
}

define('IN_ECS', true);

require(dirname(dirname(dirname(__FILE__))) . '/includes/init.php');

require ROOT_PATH . 'core/lib_payload.php';

$payload = isset($_REQUEST['payload']) ? $_REQUEST['payload'] : '';
if (empty($payload)) exit;
$obj = decode_payload(base64_decode($payload));
if (!is_array($obj)) exit;
$act = isset($obj['act']) ? $obj['act'] : '';

if ($act == 'send_verify_email')
{
	$user_id = intval($obj['user_id']);
	
	// send verify email
	require ROOT_PATH . 'includes/lib_passport.php';
	$ok = send_regiter_hash($user_id);
	if (!$ok)
	{
		hw_error_log('send_regiter_hash fail for user_id: ' . $user_id);
	}
}

?>