<?php

/***
 * admin_stock_alert.php
 * by howang 2015-09-01
 *
 * PHP Script for sending email receipt in background invoked by cron
 ***/

if (extension_loaded('newrelic'))
{
	newrelic_background_job(true);
}

define('IN_ECS', true);

require(dirname(dirname(dirname(__FILE__))) . '/includes/init.php');
$erpController = new Yoho\cms\Controller\ErpController();

if (!empty($_GET['cron']) && ($_SERVER['REMOTE_ADDR'] == $_SERVER['SERVER_ADDR']))
{
	set_time_limit(900); // 15 minutes
	
	// Get goods meet 庫存警告 criteria
	$sql = "SELECT g.goods_id, g.goods_name, sum(ega.goods_qty) as real_goods_number, g.warn_number " .
			"FROM " . $ecs->table('goods'). " as g " .
			"LEFT JOIN " . $ecs->table('erp_goods_attr_stock') . " as ega ON ega.goods_id = g.goods_id AND ega.warehouse_id IN (".$erpController->getSellableWarehouseIds(true).")" .
			"WHERE g.is_delete = 0 AND g.is_on_sale = 1 AND g.is_real = 1 " .
			"GROUP BY g.goods_id " .
			"HAVING real_goods_number <= g.warn_number ";
	$res = $db->getAll($sql);
	
	$goods_ids = array();
	$warn_goods = array();
	foreach ($res as $row)
	{
		$goods_ids[] = $row['goods_id'];
		$row['sales'] = '0';
		$warn_goods[$row['goods_id']] = $row;
	}
	
	// Get goods sold quantity of the past month
	$time_yesterday = local_strtotime('today') - 1; // 23:59:59 yesterday
	$time_last_month = local_strtotime('-1 month midnight'); // 00:00:00 1 month ago
	$sql = "SELECT og.goods_id, SUM(og.goods_number) AS sales " .
			"FROM " . $ecs->table('order_goods') . " as og " .
			"LEFT JOIN " . $ecs->table('order_info') . " as oi ON oi.order_id = og.order_id " .
			"WHERE oi.order_status IN ('1','5') AND oi.shipping_status IN ('1','2') AND oi.pay_status IN ('2','1') " .
			"AND (oi.shipping_time BETWEEN " . $time_last_month . " AND " . $time_yesterday . ") " .
			"AND og.goods_id " . db_create_in($goods_ids) .
			"GROUP BY og.goods_id";
	$res = $db->getAll($sql);
	foreach ($res as $row)
	{
		$warn_goods[$row['goods_id']]['sales'] = $row['sales'];
	}
	
	// Get goods - supplier relations
	$sql = "SELECT goods_id, supplier_id " .
			"FROM " . $ecs->table('erp_goods_supplier') .
			"WHERE goods_id " . db_create_in($goods_ids);
	$res = $db->getAll($sql);
	
	// Build a list of goods group by supplier
	$supplier_ids = array();
	$goods_by_supplier = array();
	$goods_ids_with_supplier = array();
	foreach ($res as $row)
	{
		$supplier_ids[] = $row['supplier_id'];
		$goods_ids_with_supplier[] = $row['goods_id'];
		if (!isset($goods_by_supplier[$row['supplier_id']]))
		{
			$goods_by_supplier[$row['supplier_id']] = array();
		}
		$goods_by_supplier[$row['supplier_id']][] = $warn_goods[$row['goods_id']];
	}
	
	// Assign goods without supplier to supplier_id 0
	foreach ($warn_goods as $goods)
	{
		if (!in_array($goods['goods_id'], $goods_ids_with_supplier))
		{
			if (!isset($goods_by_supplier[0]))
			{
				$goods_by_supplier[0] = array();
			}
			$goods_by_supplier[0][] = $goods;
		}
	}
	
	// Get supplier info
	$sql = "SELECT supplier_id, name " .
			"FROM " . $ecs->table('erp_supplier') .
			"WHERE supplier_id " . db_create_in($supplier_ids);
	$res = $db->getAll($sql);
	
	$suppliers = array();
	foreach ($res as $row)
	{
		$suppliers[$row['supplier_id']] = $row;
	}
	
	// Add virtual supplier for those goods without supplier
	if (isset($goods_by_supplier[0]))
	{
		$suppliers[0] = array('supplier_id' => '0', 'name' => '沒有供應商的產品');
	}
	
	// Loop though the goods list, generate emails and send them
	$secure_site_url = get_secure_site_url();
	foreach ($goods_by_supplier as $supplier_id => $goods_list)
	{
		// Get supplier name, or set name to a meaningful string if supplier info not found
		if (isset($suppliers[$supplier_id]))
		{
			$supplier = $suppliers[$supplier_id];
		}
		else
		{
			$supplier = array('supplier_id' => $supplier_id, 'name' => '無效供應商 ID: ' . $supplier_id);
		}
		
		// Process only if the goods list is not empty (just to be save, this should be always true)
		if (!empty($goods_list))
		{
			// Build the email subject and content
			$subject = local_date('n月j日') . ' ' . $supplier['name'] . ' 補貨提醒';
			$content = '<h1>' . $subject . '</h1>';
			$content .= '<table border="1" cellpadding="4" style="border-collapse: collapse;">';
			$content .= '<tr><th>產品名稱</th><th>警告數目</th><th>存貨數目</th><th>最近一個月內銷量</th></tr>';
			foreach ($goods_list as $goods)
			{
				$content .= '<tr><td>' . htmlspecialchars($goods['goods_name']) . '</td><td>' . $goods['warn_number'] . '</td><td>' . $goods['real_goods_number'] . '</td><td>'. $goods['sales'] . '</td></tr>';
			}
			$content .= '</table>';
			$content .= '* 最近一個月內銷量為 ' . local_date('Y-m-d', $time_last_month) . ' 至 ' . local_date('Y-m-d', $time_yesterday) . '之銷量<br>';
			$content .= '<br>';
			$content .= '<a href="' . $secure_site_url . 'x/goods.php?act=list&stock_warning=1&is_on_sale=1&suppliers_id=' . $supplier_id . '">按此在管理系統中查看</a>';
			$is_html = true;

			// Get admins responsible for the supplier
			$sql = "SELECT user_name, email " .
				"FROM " . $ecs->table('admin_user') . " as au " .
				"LEFT JOIN " . $ecs->table('erp_supplier_admin') . " as esa ON esa.admin_id = au.user_id " .
				"WHERE " . ($supplier_id == 0 ? "au.user_name = 'admin' " : // If supplier is missing, send to admin
					"esa.supplier_id = '" . $supplier_id . "' AND au.user_name != 'admin' ") . // otherwise exclude admin
				"GROUP BY au.user_id ".
				" UNION ALL ".
				"SELECT user_name, email " .
				"FROM " . $ecs->table('admin_user') . " as au " .
				"LEFT JOIN " . $ecs->table('erp_supplier_secondary_admin') . " as esa ON esa.admin_id = au.user_id " .
				"WHERE " . ($supplier_id == 0 ? "au.user_name = 'admin' " : // If supplier is missing, send to admin
					"esa.supplier_id = '" . $supplier_id . "' AND au.user_name != 'admin' ") . // otherwise exclude admin
				"GROUP BY au.user_id ";
			$res = $db->getAll($sql);
			// If no admin assigned to the supplier, send to a default admin
			if (empty($res))
			{
				$res = array(array('user_name' => 'franz', 'email' => 'franz@yohohongkong.com'));
			}
			
			// Build recipients list and send the email
			$names = array();
			$emails = array();
			foreach($res as $row)
			{
				$names[] = $row['user_name'];
				$emails[] = $row['email'];
			}
			send_mail_with_attachment($names, $emails, $subject, $content, $is_html);
		}
	}
	exit;
}

function get_secure_site_url()
{
	$old_https = isset($_SERVER['HTTPS']) ? $_SERVER['HTTPS'] : null;
	$_SERVER['HTTPS'] = 'on';
	$secure_site_url = $GLOBALS['ecs']->url();
	if (is_null($old_https))
	{
		unset($_SERVER['HTTPS']);
	}
	else
	{
		$_SERVER['HTTPS'] = $old_https;
	}
	return $secure_site_url;
}

?>