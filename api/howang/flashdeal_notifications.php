<?php

/***
 * flashdeal_notifications.php
 * by howang 2015-02-13
 *
 * PHP Script for sending email notification for today's flashdeal in background invoked by cron
 ***/

if (extension_loaded('newrelic'))
{
	newrelic_background_job(true);
}

define('IN_ECS', true);

require(dirname(dirname(dirname(__FILE__))) . '/includes/init.php');

require ROOT_PATH . 'core/lib_payload.php';

set_time_limit(0);

if (!empty($_GET['cron']) && ($_SERVER['REMOTE_ADDR'] == $_SERVER['SERVER_ADDR']))
{
    $start_time = local_strtotime(local_date('Y-m-d 00:00:00'));
    $end_time   = local_strtotime(local_date('Y-m-d 23:59:59'));
	$sql = "SELECT fdn.`notify_id` " .
		   "FROM " . $ecs->table('flashdeal_notify') . " as fdn " .
		   "LEFT JOIN " . $ecs->table('flashdeals') . " as fd ON fd.flashdeal_id = fdn.flashdeal_id " .
		   "LEFT JOIN " . $ecs->table('flashdeal_event') . " AS fde ON fde.fd_event_id = fd.fd_event_id " .
		   "WHERE fde.status = 1 AND fd.deal_date BETWEEN '" . $start_time . "' AND '" . $end_time . "' AND fdn.is_send = 0 " .
		   "ORDER BY `notify_id`";
	$notify_ids = $db->getCol($sql);
	
	error_log(basename(__FILE__) . ':' . __LINE__ . ' Notify IDs: ' . implode(',', $notify_ids));
	
	if ((!empty($notify_ids)) && (is_array($notify_ids)))
	{
		// Get our own URL
		$api_url = 'http' . ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ? 's' : '') . '://' . (empty($_SERVER['HTTP_HOST']) ? $_SERVER['SERVER_NAME'] : $_SERVER['HTTP_HOST']) . $_SERVER['PHP_SELF'];
		
		// Config
		$max_process = 5;
		$max_nofitication_per_process = 5;
		
		$pids = array();
		do
		{
			// Clean up finished process
			foreach ($pids as $key => $pid)
			{
				$status = proc_get_status($pid);
				if (empty($status['running']))
				{
					//error_log('PID ' . $status['pid'] . ' exited, clean up...');
					// Close process handle
					proc_close($pid);
					// Remove from $pids array
					unset($pids[$key]);
				}
			}
			
			// Spawn new process to send notifications
			$spawned = false;
			while (($max_process - count($pids) > 0) && (!empty($notify_ids)))
			{
				$num = 0;
				$cnt = count($notify_ids);
				if ($cnt > $max_nofitication_per_process)
				{
					$num = $max_nofitication_per_process;
				}
				else
				{
					$num = $cnt;
				}
				
				// Get array of notify_id to send
				$ids = array_slice($notify_ids, 0, $num);
				$notify_ids = array_slice($notify_ids, $num);
				
				// Spawn new process to send notification
				$obj = array('act' => 'notify', 'notify_ids' => $ids);
				$payload = base64_encode(encode_payload($obj));
				$cmd = 'exec curl -d "payload=' . rawurlencode($payload) . '" ' . $api_url . ' > /dev/null 2>&1';
				//error_log(basename(__FILE__) . ':' . __LINE__ . ' Notifying IDs: ' . implode(',', $ids));
				$pid = proc_open($cmd, array(), $pipes);
				if ($pid !== false)
				{
					$status = proc_get_status($pid);
					//error_log('Spawned PID ' . $status['pid'] . ' for notifying IDs: ' . implode(',', $ids));
					
					$pids[] = $pid;
					$spawned = true;
				}
				else
				{
					error_log(basename(__FILE__) . ':' . __LINE__ . ' Error running proc_open(' . $cmd . ')');
				}
				
				// If all notifications are scheduled, exit this loop
				if (empty($notify_ids))
				{
					break;
				}
			}
			
			if ($spawned) sleep(1);
		}
		while (!empty($pids));
	}
	exit;
}

$payload = isset($_REQUEST['payload']) ? $_REQUEST['payload'] : '';
if (empty($payload)) exit;
$obj = decode_payload(base64_decode($payload));
if (!is_array($obj)) exit;
$act = isset($obj['act']) ? $obj['act'] : '';

if ($act == 'notify')
{
	$notify_ids = array_filter($obj['notify_ids']);
	
	$sql = "SELECT fdn.`email`, fd.*, g.*, IFNULL(rei.`content`, u.`user_name`) as `display_name` " .
			"FROM " . $ecs->table('flashdeal_notify') . " as fdn " .
			"LEFT JOIN " . $ecs->table('flashdeals') . " as fd ON fd.flashdeal_id = fdn.flashdeal_id " .
			"LEFT JOIN " . $ecs->table('goods') . " as g ON g.goods_id = fd.goods_id " .
			"LEFT JOIN " . $ecs->table('users') . " as u ON u.user_id = fdn.user_id " .
			"LEFT JOIN " . $ecs->table('reg_extend_info') . " as rei ON rei.`user_id` = fdn.user_id AND rei.`reg_field_id` = 10 " .
			"WHERE notify_id " . db_create_in($notify_ids) . "";
	$res = $db->getAll($sql);
	
	foreach($res as $notify)
	{
		if (empty($notify)) return;
		
		if (empty($notify['email'])) return;
		
		// howang: don't send if user is using default system-generated email address
		if (is_placeholder_email($notify['email'])) return;
		
		// check bounce list
		if (!can_send_mail($notify['email'])) return;
		
		$tpl = get_mail_template('flashdeal_notice');
		$smarty->assign('data', $notify);
		$smarty->assign('shop_url', $ecs->url());
		$smarty->assign('shop_name', $_CFG['shop_name']);
		$smarty->assign('send_date', local_date($_CFG['date_format']));
		$smarty->assign('sent_date', local_date($_CFG['date_format']));
		$content = $smarty->fetch('str:' . $tpl['template_content']);
		
		//error_log(basename(__FILE__) . ':' . __LINE__ . ' Sending email to ' . $notify['email']);
		send_mail_with_attachment($notify['display_name'], $notify['email'], $tpl['template_subject'], $content, $tpl['is_html']);
	}
    $sql = "UPDATE ".$ecs->table('flashdeal_notify')." SET is_send = 1 " .
        "WHERE notify_id " . db_create_in($notify_ids) . "";
    $res = $db->query($sql, 'SILENT');
}

?>