<?php

/***
 * admin_notifications.php
 * by howang 2014-07-09
 *
 * PHP Script for sending admin notification emails asynchronously
 ***/

if (extension_loaded('newrelic'))
{
	newrelic_background_job(true);
}

define('IN_ECS', true);

require(dirname(dirname(dirname(__FILE__))) . '/includes/init.php');

require(ROOT_PATH . '/' . ADMIN_PATH . '/includes/lib_notifications.php');

$func = empty($_REQUEST['func']) ? '' : $_REQUEST['func'];

$params = empty($_REQUEST['params']) ? '' : $_REQUEST['params'];

$sig = empty($_REQUEST['sig']) ? '' : $_REQUEST['sig'];

if (empty($func)) exit;
if (empty($params)) exit;
if (empty($sig)) exit;

$sig = json_decode(base64_decode($sig), true);

// Signature only valid for 10 seconds
if (gmtime() - $sig['t'] > 10)
{
	error_log(__FILE__ . ' gmtime() = ' . gmtime() . ', t = ' . $sig['t'] . ', diff = ' . (gmtime() - $sig['t']) . ' > 10');
	exit;
}

$hash = substr(sha1($func . $params . $GLOBALS['_CFG']['hash_code'] . $sig['t']),34);
if ($hash !== $sig['c'])
{
	error_log(__FILE__ . ' func = ' . $func);
	error_log(__FILE__ . ' params = ' . $params);
	error_log(__FILE__ . ' hash_code = ' . $GLOBALS['_CFG']['hash_code']);
	error_log(__FILE__ . ' hash = ' . $hash . ', c = ' . $sig['c']);
	exit;
}

$params = json_decode(base64_decode($params), true);
call_user_func_array($func, $params);

exit;

?>