<?php
define('IN_ECS', true);

require dirname(dirname(__FILE__)) . '/includes/init.php';

$input   = file_get_contents("php://input");
$request = json_decode($input, true);
$event = $request['event'];
$data = $request['data'];
$create_time = date('Y-m-d H:i:s', $request['timestamp']);

if ($event == "call.created") {
    if (empty($data['raw_digits'])) {
        $phone = "Anonymous$data[id]";
    } else {
        $phone = implode("", explode(" ", $data['raw_digits']));
    }
    if (empty($data["user"])) {
        $user = "No User";
    } else {
        $user = $data["user"]["name"];
    }
    createTicket($data["id"], $phone, CRM_PLATFORM_AIRCALL, $data["direction"] == "outbound" ? $user : $phone, $create_time);
} elseif ($event == "call.answered") {
    if (!empty($data["user"])) {
        $user = $data["user"]["name"];
        $admin_id = $db->getOne("SELECT admin_id FROM " . $ecs->table("crm_platform_agent") . " WHERE agent_id = '" . $data['user']['id'] . "' AND platform = " . CRM_PLATFORM_AIRCALL);
        udpateAircallQueue($data['id'], CRM_AIRCALL_ANSWER, $admin_id);
        $db->query("INSERT INTO " . $ecs->table("sse") . " (time, target, event, data) VALUES (" . gmtime() . ", 'crm', 'aircall', 'answered')");
    }
} elseif ($event == "call.ended") {
    if (!empty($data['missed_call_reason']) || empty($data['answered_at'])) {
        udpateAircallQueue($data['id'], CRM_AIRCALL_MISSED, -1);
    } else {
        $sql = "DELETE FROM " . $ecs->table("crm_aircall_queue") . " WHERE call_id = $data[id]";
        $db->query($sql);
    }
    $db->query("INSERT INTO " . $ecs->table("sse") . " (time, target, event, data) VALUES (" . gmtime() . ", 'crm', 'aircall', 'ended')");
}

http_response_code(200);

function createTicket($unique_id, $ref_id, $platform, $sender, $create_time)
{
    global $ecs, $db;
    $ticket_id = $db->getOne("SELECT ticket_id FROM " . $ecs->table('crm_list') . " WHERE unique_id = '$unique_id'");
    if (empty($ticket_id)) {
        $sql = "SELECT ticket_id FROM " . $ecs->table('crm_ticket_info') . " WHERE platform = $platform AND ref_id = '$ref_id'";
        $ticket_id = $db->getOne($sql);
        $log_type = CRM_LOG_TYPE_INSERT;
        if (empty($ticket_id)) {
            $db->query("INSERT INTO " . $ecs->table('crm_ticket_info') . " (ref_id, platform) VALUES ('$ref_id', $platform)");
            $ticket_id = $db->insert_id();
        } else {
            $db->query("UPDATE " . $ecs->table('crm_ticket_info') . " SET status = " . CRM_STATUS_PENDING . ", last_update = CURRENT_TIMESTAMP WHERE ticket_id = $ticket_id");
            $log_type = CRM_LOG_TYPE_UPDATE;
        }
        $user_id = $db->getOne("SELECT user_id FROM " . $ecs->table('users') . " WHERE REPLACE(user_name, '-', '') LIKE '%$ref_id%'");
        if (!empty($user_id)) {
            $db->query("UPDATE " . $ecs->table('crm_ticket_info') . " SET user_id = $user_id WHERE ticket_id = $ticket_id");
        }
        $sql = "INSERT INTO " . $ecs->table('crm_list') . " (ticket_id, unique_id, sender, create_at) VALUES ($ticket_id, '$unique_id', '$sender', '$create_time')";
        $db->query($sql);
        $list_id = $db->insert_id();
        $db->query("INSERT INTO " . $ecs->table('crm_log') . " (ticket_id, list_id, admin_id, type) VALUES ($ticket_id, $list_id, 0, " . $log_type . ")");
    }
    return $ticket_id;
}

function udpateAircallQueue($call_id, $event, $admin_id = 0)
{
    global $ecs, $db;
    // if record count > 1000, remove old record
    $old_queue_id = $db->getOne("SELECT IFNULL(MAX(queue_id), 0) FROM (SELECT queue_id FROM " . $ecs->table("crm_aircall_queue") . " ORDER BY queue_id DESC LIMIT 1000, 1000) t");
    if ($old_queue_id) {
        $db->query("DELETE FROM " . $ecs->table("crm_aircall_queue") . " WHERE queue_id <= $old_queue_id");
    }
    if (empty($admin_id)) {
        $admins = $db->getCol("SELECT user_id FROM " . $ecs->table("admin_user") . " WHERE (role_id IN (3, 4, 10) AND is_disable != 1) OR user_id = 1");
        foreach ($admins as $admin) {
            udpateAircallQueue($call_id, $event, $admin);
        }
    } else {
        $ticket_id = $db->getOne("SELECT ticket_id FROM " . $ecs->table("crm_list") . " WHERE unique_id = '$call_id'");
        if ($event == CRM_AIRCALL_ANSWER) {
            $db->query("DELETE FROM " . $ecs->table("crm_aircall_queue") . " WHERE ticket_id = $ticket_id AND event = " . CRM_AIRCALL_MISSED);
            $db->query("UPDATE " . $ecs->table('crm_ticket_info') . " SET status = " . CRM_STATUS_REPLIED . ", last_update = CURRENT_TIMESTAMP WHERE ticket_id = $ticket_id");
            $db->query("INSERT INTO " . $ecs->table('crm_log') . " (ticket_id, admin_id, type) VALUES ($ticket_id, $admin_id, " . CRM_LOG_TYPE_REPLY . ")");
            $db->query("DELETE FROM " . $ecs->table("crm_aircall_queue") . " WHERE admin_id = $admin_id AND event = " . CRM_AIRCALL_ANSWER);
        }
        $sql = "INSERT INTO " . $ecs->table("crm_aircall_queue") . " (ticket_id, call_id, admin_id, event) VALUES ($ticket_id, $call_id, $admin_id, $event)";
    }
    if (!empty($sql)) {
        $db->query($sql);
    }
}
?>