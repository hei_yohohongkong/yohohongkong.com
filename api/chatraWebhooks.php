<?php
define('IN_ECS', true);

require dirname(dirname(__FILE__)) . '/includes/init.php';

$input   = file_get_contents("php://input");
$request = json_decode($input, true);
$eventName = $request['eventName'];
$client = $request['client'];

if ($eventName == "chatFragment") {
    foreach ($request['messages'] as $message) {
        if (empty($message['text']) && empty($message['file'])) {
            continue;
        }
        if ($message['type'] == "agent") {
            $sender = $message['agentId'];
        } else {
            $sender = $client['id'];
        }
        $ref_id = $client['chatId'];
        $unique_id = $message['id'];
        $create_time = date('Y-m-d H:i:s', floor($message['createdAt'] / 1000));
        $ticket_id = createTicket($unique_id, $ref_id, CRM_PLATFORM_CHATRA, $sender, $create_time);
        $db->query("INSERT INTO " . $ecs->table("sse") . " (time, target, event, data) VALUES (" . gmtime() . ", 'crm', 'chatra', '$ticket_id')");
        if (!empty($message['file'])) {
            $file = $message['file'];
            $list_id = $db->getOne("SELECT list_id FROM " . $ecs->table('crm_list') . " WHERE unique_id = '$unique_id'");
            $type = explode(".", $file['fileName'])[1];
            if (empty($file['size'])) {
                $head = array_change_key_case(get_headers($file_url, 1));
                $file['size'] = isset($head['content-length']) ? intval($head['content-length']) : 0;
            }
            $is_image = $file['isImage'] ? 1 : 0;
            $db->query("INSERT INTO " . $ecs->table("crm_chatra_attachment") . " (list_id, name, url, size, is_image) VALUES ($list_id, '$file[fileName]', '$file[url]', $file[size], $is_image)");
        }
    }
}

function createTicket($unique_id, $ref_id, $platform, $sender, $create_time)
{
    global $ecs, $db;
    $ticket_id = $db->getOne("SELECT ticket_id FROM " . $ecs->table('crm_list') . " WHERE unique_id = '$unique_id'");
    if (empty($ticket_id)) {
        $sql = "SELECT ticket_id FROM " . $ecs->table('crm_ticket_info') . " WHERE platform = $platform AND ref_id = '$ref_id'";
        $ticket_id = $db->getOne($sql);
        if (empty($ticket_id)) {
            $db->query("INSERT INTO " . $ecs->table('crm_ticket_info') . " (ref_id, platform) VALUES ('$ref_id', $platform)");
            $ticket_id = $db->insert_id();
        }
        $user_id = $db->getOne("SELECT user_id FROM " . $ecs->table('crm_platform_user') . " WHERE platform = $platform AND client_id = '$sender'");
        if (!empty($user_id)) {
            $db->query("UPDATE " . $ecs->table('crm_ticket_info') . " SET user_id = $user_id WHERE ticket_id = $ticket_id");
        }
        $sql = "INSERT INTO " . $ecs->table('crm_list') . " (ticket_id, unique_id, sender, create_at) VALUES ($ticket_id, '$unique_id', '$sender', '$create_time')";
        $db->query($sql);
        $list_id = $db->insert_id();
        $db->query("INSERT INTO " . $ecs->table('crm_log') . " (ticket_id, list_id, admin_id, type) VALUES ($ticket_id, $list_id, 0, " . CRM_LOG_TYPE_INSERT . ")");
    }
    return $ticket_id;
}
?>