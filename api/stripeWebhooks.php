<?php

define('IN_ECS', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');
require(ROOT_PATH . 'includes/lib_payment.php');
require_once(ROOT_PATH . 'includes/lib_order.php');
require_once(ROOT_PATH . 'includes/cls_json.php');
require_once(ROOT_PATH . '/includes/stripe/init.php');
include_once('includes/lib_clips.php');

$is_sandbox = false;
echo "Please Wait....";
// Retrieve the request's body and parse it as JSON
$input   = @file_get_contents("php://input");
$request = json_decode($input, true);
$object  = $request['data']['object'];
$type    = $object['type'];
$status  = $object['status'];
$docheckout = true;
$card_log = '(已經過短訊安全認證)';
// If pending_webhooks != 1 false
//Check Event , Only 3ds and wechat will do
if(!$object || $request['pending_webhooks'] != 1 || !in_array($type, ["three_d_secure","wechat"]) || $status != 'chargeable')$docheckout = false;

if($type == "three_d_secure"){
    $authenticated    = $object['three_d_secure']['authenticated'];
    $redirect_status  = $object['redirect']['status'];

    // check 3ds
    if($redirect_status != "succeeded" || $authenticated != true) {
        // Special case: User is using a support 3DS card, but not yet been enrolled in 3D Secure. ref: https://stripe.com/docs/sources/three-d-secure
        if ($redirect_status == "succeeded" && $authenticated == false && $status == 'chargeable') {
            echo " Card is support 3ds, but user not yet been enrolled. ";
            $card_log = '(信用卡支援短訊安全認證-但用戶沒有開通)';
            $docheckout = true;
        } else {
            $docheckout = false;
        }
    }
}
if($docheckout == true){
    echo " Starting transaction";
    $metadata  = $object['metadata'];
    $order_id  = $metadata['order_id'];
    $log_id  = $metadata['log_id'];
    $pay_code  = isset($metadata['pay_code']) ? $metadata['pay_code'] : 'stripe';
    $amount    = number_format(($object['amount']/100),2,'.','');
    $source_id = $object['id'];
    if(empty($order_id)){
        echo "NULL order Id";
        $_SESSION['webhooks_error'] = 1;
        return false;
    }
    if(empty($log_id)) {
        $sql = 'SELECT log_id FROM ' . $GLOBALS['ecs']->table('pay_log').
            " WHERE order_id = '$order_id' AND is_paid = '0' order by log_id desc limit 1";
        $log_id = $GLOBALS['db']->getOne($sql);
    }

    //Get Payment Info
    $sql = 'SELECT * FROM ' . $GLOBALS['ecs']->table('payment').
        " WHERE pay_code = '$pay_code' AND enabled = '1'";
    $payment = $GLOBALS['db']->getRow($sql);

    if ($payment)
    {
        $config_list = unserialize($payment['pay_config']);

        foreach ($config_list AS $config)
        {
            $payment[$config['name']] = $config['value'];
        }
    }

    // Set up Stripe APi
    if($is_sandbox){
        $publishable_key = 'pk_test_E0Stl002F54obSM0TUWwAgix';
        $secret_key      = 'sk_test_fB3Ene1AWkvnEc0IdfHLTgEH';
    } else {
        $publishable_key = $payment['stripe_publishable_key'];
        $secret_key      = $payment['stripe_secret_key'];
    }
    $stripe = array(
        'secret_key'      => $secret_key,
        'publishable_key' => $publishable_key
    );
    \Stripe\Stripe::setApiKey($stripe['secret_key']);

    // Get Order Info
    $oSql = "SELECT o.order_sn, o.consignee, o.email, o.tel, o.mobile, o.pay_id, o.address, o.order_status, o.pay_status, o.shipping_status, r.region_name, u.reg_time, u.last_login, u.last_ip, u.user_name, u.new_user_name, u.rank_points, u.pay_points, u.user_id ".
    " FROM " . $GLOBALS['ecs']->table('order_info') ." as o ".
    " LEFT JOIN " . $GLOBALS['ecs']->table('region') ." as r ON r.region_id = o.country ".
    " LEFT JOIN " . $GLOBALS['ecs']->table('users') ." as u ON o.user_id = u.user_id ".
    " WHERE order_id = '" . $order_id . "'";

    $order_info = $GLOBALS['db']->getRow($oSql);
    $order_info['reg_time_formatted'] = local_date($GLOBALS['_CFG']['date_format'], $order_info['reg_time']);
    $order_info['last_login_formatted'] = local_date($GLOBALS['_CFG']['date_format'], $order_info['last_login']);
    if($order_info['rank_points'] > 1000 ){
        $order_info['rank_points_op'] = true;
    } else {
        $order_info['rank_points_op'] = false;
    }

    // If order is cancelled / refunded
    if (in_array($order_info['order_status'], [OS_CANCELED, OS_INVALID, OS_WAITING_REFUND])) {
        order_action($order_info['order_sn'], $order_info['order_status'], $order_info['shipping_status'], $order_info['pay_status'], 'Stripe Error: 此訂單已取消, 無法付款.');
        unset($_SESSION['processing']['stripe']);
        $_SESSION['webhooks_error'] = 1;
        return false;
    }

    $flashdeal_info = $GLOBALS['db']->getRow(
        "SELECT count(og.flashdeal_id) as flashdeal_num, oi.add_time as add_time, oi.order_status FROM ".$GLOBALS['ecs']->table('order_goods') . " as og ".
        " LEFT JOIN ".$GLOBALS['ecs']->table('order_info')."as oi ON oi.order_id = og.order_id ".
        " WHERE og.flashdeal_id > 0 and og.order_id = $order_id GROUP BY og.order_id"
    );
    // If order is payed, Don't pay again
    if($order_info['pay_status'] == PS_PAYED)
    {
        /* 记录订单操作记录 */
        order_action($order_info['order_sn'], $order_info['order_status'], $order_info['shipping_status'], $order_info['pay_status'], 'Stripe Error: 此訂單已付款成功, 無需再付款.');
        unset($_SESSION['processing']['stripe']);
        $_SESSION['webhooks_error'] = 1;
        return false;
    }
    $flashdealController = new \Yoho\cms\Controller\FlashdealController();
    $expired = local_strtotime($flashdealController::FLASH_PAY_ORDER_EXPIRED, $flashdeal_info['add_time']);
    $expired2 = $flashdealController->getOrderFisrtExpired($order_id);
    if ($flashdeal_info['flashdeal_num'] > 0 && gmtime() > $expired && gmtime() > $expired2) {
        include_once(ROOT_PATH . 'includes/lib_transaction.php');
        cancel_order($order_id, $order_info['user_id']);
        order_action($order_info['order_sn'], OS_CANCELED, $order_info['shipping_status'], $order_info['pay_status'], 'Stripe Error: 此閃購訂單已到期, 無法付款.');
        unset($_SESSION['processing']['stripe']);
        $_SESSION['webhooks_error'] = 1;
        return false;
    }

    //Check amount
    $sql = "SELECT * FROM " . $GLOBALS['ecs']->table('pay_log') . " WHERE log_id = '" . $log_id . "'";
    $pay_log = $GLOBALS['db']->getRow($sql);
    $fraud = FRAUD_CLEAR;
    if ($pay_log['order_amount'] != $amount)
    {
        echo "pay_log(".$pay_log['order_amount'].") order_amount(".$amount.") not match";
        $_SESSION['webhooks_error'] = 1;
        return false;
    } else {
        try {

            $currency = !empty($pay_log['currency_code']) ? $pay_log['currency_code'] : $payment['stripe_currency'];
            $charge = \Stripe\Charge::create(array(
                'source'   => $source_id,
                'amount'   => $object['amount'],
                'metadata' => array(
                    'order_id'  	 => $order_info['order_sn'],
                    'consignee' 	 => $order_info['consignee'],
                    'email'     	 => $order_info['email'],
                    'tel'       	 => $order_info['tel'],
                    'mobile'    	 => $order_info['mobile'],
                    'log_id'    	 => $log_id,
                    'address'   	 => $order_info['address'],
                    'register_time'  => $order_info['reg_time_formatted'],
                    'last_login'     => $order_info['last_login_formatted'],
                    'country'        => $order_info['region_name'],
                    'ip'			 => $order_info['last_ip'],
                    'register_tel'   => $order_info['user_name'],
                    'rank_points'    => $order_info['rank_points'],
                    'rank_points_op' => $order_info['rank_points_op']

                ),
                'currency' => $currency
            ));

        } catch (Exception $e) {
            $body = $e->getJsonBody();
            $error = $body['error']['message'];
            echo $error;

            if ($body['error']['decline_code'] == "fraudulent") {
                $fraud = FRAUD_CONFIRM;
            }
            if (in_array($body['error']['decline_code'], ["incorrect_cvc", "invalid_cvc", "incorrect_pin", "invalid_pin"])) {
                $fraud = FRAUD_MEDIUM;
            }
            if (in_array($body['error']['decline_code'], ["do_not_honor", "lost_card", "pickup_card", "pin_try_exceeded", "restricted_card", "stolen_card"])) {
                $fraud = FRAUD_HIGH;
            }
            payment_risk_level_log($body['error']['charge'], $order_id, $order_info['pay_id'], $fraud);

            /* 记录订单操作记录 */
            order_action($order_info['order_sn'], $order_info['order_status'], $order_info['shipping_status'], $order_info['pay_status'], '付款失敗(Stripe):'.$error, $GLOBALS['_LANG']['buyer']);
            $_SESSION['webhooks_error'] = 1;
            return false;
        }
        if (!$error) {

            $txn_id = $charge->id;
            $action_note = $pay_code. $card_log . ':' . $txn_id . '（' . $pay_log['currency_code'] . $pay_log['order_amount'] . '）';

            if ($charge->outcome->reason == "incorrect_cvc") {
                $fraud = FRAUD_HIGH;
            } elseif ($charge->outcome->risk_level == "elevated") {
                $fraud = FRAUD_MEDIUM;
            }
            payment_risk_level_log($charge->id, $order_id, $order_info['pay_id'], $fraud, 1);

            /* 改变订单状态 */
            order_paid($log_id, PS_PAYED, $action_note);
            $fraudController = new Yoho\cms\Controller\FraudController();
            $fraudController->fraudOrderControl($order_id, $fraudController::PAYMENT_CODE_STRIPE, $charge);
            // If QR code img is exists, delete it
            $img = "/images/tmpqrcode/".$order_id.".png";
            if (file_exists($img)) unlink($img);
            return true;

        } else {
            return false;
        }
    }
} else {
    return false;
}
return false;