<?php

use function GuzzleHttp\json_decode;

define('IN_ECS', true);

require dirname(dirname(__FILE__)) . '/includes/init.php';
require dirname(dirname(__FILE__)) . '/includes/lib_order.php';
require dirname(dirname(__FILE__)) . '/includes/lib_transaction.php';

$current_mins = date('i');
if ($current_mins == 18 || $current_mins == 38) {
    clear_cache_files('flashdeal');
    clear_cache_files('goods');
}

$flashdealController = new Yoho\cms\Controller\FlashdealController();

//$flashdealController->is_flashdeal_today();


//if ($current_mins == 15 || $current_mins == 30 || $current_mins == 45  || $current_mins == 00 || $_REQUEST['force'] == 1) {
if ($_REQUEST['force'] == 1) {    
    // create flashdeal cache
    $flashdealController->preCreateFlashDealCache();
}

// flashdeal cache callnack function
function flashdeal_request_callback($response, $info, $request) {
    
    if ($info['http_code']!='200' && $_REQUEST['force'] == 1) {
        echo '<pre>';
        print_r($info);
        echo "<hr>";
    } else if ($info['http_code']=='200' && $_REQUEST['force'] == 1) {
        echo '<pre>';
        print_r($info['url']);
        echo "<hr>";
    }
}
die (json_encode(["error" => 0, "msg" => "success"]));
?>