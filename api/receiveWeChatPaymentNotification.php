<?php

define('IN_ECS', true);

// Skip update user info for faster loading
define('INIT_NO_UPDATE_USER_INFO', true);

require(dirname(dirname(__FILE__)) . '/includes/init.php');
require(dirname(dirname(__FILE__)) . '/includes/lib_order.php');
require_once(dirname(dirname(__FILE__)) . '/includes/lib_payment.php');
$wechat_pay_controller = new Yoho\cms\Controller\WeChatPayLogController();
$orderController = new Yoho\cms\Controller\OrderController();
/*------------------------------------------------------ */
//-- INPUT
/*------------------------------------------------------ */
$sign = $_SERVER['HTTP_X_QF_SIGN'];
$payment_info = get_payment('qfwechatpay');
$data = file_get_contents('php://input');

// Verify signature and data
if (!$sign)
{
    echo 'FAILED';
    exit;
}

if ($sign != strtoupper(md5($data.$payment_info['qfwechatpay_key']))) {
    echo 'FAILED';
    exit;
}

// Echo success to server, then continue execution
echo "SUCCESS";

$data = json_decode($data);
$insert_result = $wechat_pay_controller->insert_wechat_pay_payment_notification($data);
// Mark order is paid only when order type is payment and respcd is 0000
if($insert_result && ($data->order_type == 'payment' || $data->notify_type == 'payment') && $data->respcd == '0000'){
    $log_id = substr(explode('_', trim($data->out_trade_no))[0], 13);

    if (!empty($log_id)) {
        $orderController->order_payment_transaction_log($log_id, $payment_info['pay_id'], $data->syssn, $data->txcurrcd,($data->txamt/100),0);
    }
    
    $action_note = $payment_info['pay_code'] . ':' . $data->syssn . '（' . $data->txcurrcd . ($data->txamt / 100) . '）';
    order_paid($log_id,PS_PAYED,$action_note);
}

?>
