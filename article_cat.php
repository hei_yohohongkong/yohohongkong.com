<?php

/**
 * ECSHOP 資訊分类
 * ============================================================================
 * * 版权所有 2005-2012 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liubo $
 * $Id: article_cat.php 17217 2011-01-19 06:29:08Z liubo $
*/


define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

if ((DEBUG_MODE & 2) != 2)
{
    $smarty->caching = true;
}

if (!isSecure()) {
    $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: ' . $redirect);
    exit;
}

/*------------------------------------------------------ */
//-- INPUT
/*------------------------------------------------------ */

/* 获得指定的分类ID */
if (!empty($_GET['id']))
{
    $cat_id = intval($_GET['id']);
}
elseif (!empty($_GET['category']))
{
    $cat_id = intval($_GET['category']);
}
else
{
    header('Location: /');
    exit;
}

/* 获得当前页码 */
$page   = !empty($_REQUEST['page'])  && intval($_REQUEST['page'])  > 0 ? intval($_REQUEST['page'])  : 1;

/*------------------------------------------------------ */
//-- PROCESSOR
/*------------------------------------------------------ */

/* 获得页面的缓存ID */
$cache_id = sprintf('%X', crc32($cat_id . '-' . $page . '-' . $_CFG['lang']));
$cat_type = $db->getOne("SELECT cat_type FROM " . $ecs->table('article_cat') . " WHERE cat_id = '$cat_id' ");
if($cat_type == COMMON_CAT) $dwt = 'article_cat.dwt';
else $dwt = 'statics_page_cat.dwt';
/* TODO: statics_page_cat is a hard code page(short url is config in .htaccess):
 * about/ =  post/52
 * supplier/ =  post/27
 * investors/ =  post/53
 * contact/ =  post/4
 * join-us/ =  post/29
*/
/* 如果页面没有被缓存则重新获得页面的内容 */
if (!$smarty->is_cached($dwt, $cache_id))
{
    if ($cat_id == '-1')
    {
        $article_cat = array(
            'cat_name' => _L('article_list', '所有資訊'),
            'keywords' => $_CFG['shop_keywords'],
            'cat_desc' => $_CFG['shop_desc']
        );
    }
    else
    {
        $where = "ac.cat_id = '" . $cat_id . "'";
        $sql = "SELECT IFNULL(acl.cat_name, ac.cat_name) as cat_name, " .
                "IFNULL(acl.keywords, ac.keywords) as keywords, " .
                "IFNULL(acl.cat_desc, ac.cat_desc) as cat_desc " .
                "FROM " . $ecs->table('article_cat') . " as ac " .
                "LEFT JOIN " . $ecs->table('article_cat_lang') . " as acl ON acl.cat_id = ac.cat_id AND acl.lang = '" . $_CFG['lang'] . "' " .
                "WHERE $where AND cat_type NOT IN(".SUPPORT_CAT.")";
        $article_cat = $db->getRow($sql);
    }
    if ($article_cat === false || empty($article_cat))
    {
        /* 如果没有找到任何记录则返回首页 */
        header('Location: /');
        exit;
    }

    assign_template('a', array($cat_id));
    $position = assign_ur_here($cat_id);
    $ur_here_list = $smarty->get_template_vars('ur_here_list');
    $parent_title = false;
    if($ur_here_list) {
        $parent_title = !($ur_here_list[0]['name'] == $article_cat['cat_name']);
    }
    $smarty->assign('parent_title',           $parent_title);
    $smarty->assign('page_title',           $position['title']);     // 页面标题
    $smarty->assign('ur_here',              $position['ur_here']);   // 当前位置
    $smarty->assign('goods_list',              $position['ur_here']);   // 当前位置
    $smarty->assign('helps',                get_shop_help());        // 网店帮助
    $CategoryController = new Yoho\cms\Controller\CategoryController();
    $t1 = $CategoryController->getTopCategories(0);
    $smarty->assign('t1',              $t1);
    $tree = article_categories_tree($cat_id);
    $smarty->assign('article_categories',   $tree); //資訊分类树
    $smarty->assign('cat_id',               $cat_id);
    $smarty->assign('cat_name',             $article_cat['cat_name']);
    $smarty->assign('keywords',             htmlspecialchars($article_cat['keywords']));
    $smarty->assign('description',          htmlspecialchars($article_cat['cat_desc']));
    $smarty->assign('hot_lists',            get_hot_article($cat_id)); // Hot articles list
    // $smarty->assign('feed_url',             ($_CFG['rewrite'] == 1) ? "feed-typearticle_cat" . $cat_id . ".xml" : 'feed.php?type=article_cat' . $cat_id); // RSS URL

    /* 获得資訊总数 */
    $size   = isset($_CFG['article_page_size']) && intval($_CFG['article_page_size']) > 0 ? intval($_CFG['article_page_size']) : 20;
    $article_num =  $count  = get_article_count($cat_id);
    $parent_id = $db->getOne("SELECT parent_id FROM " . $ecs->table('article_cat') . " WHERE cat_id = " .$cat_id);
    if ($parent_id == 0) {/* Top category */
        foreach ($tree[$cat_id]['children'] as $key => $value) {
            $article_array = get_cat_articles($key, 1, 7, '', 'order');
            if (count($article_array) >= 4 && count($article_array) < 7) {
                $add_fake_total = 7-count($article_array);
                $last = end($article_array);
                for ($i =0;$i < $add_fake_total;$i++) {
                    array_push($article_array, $last);
                }
            }
            $tree[$cat_id]['children'][$key]['article'] = array_values($article_array);
        }
        $smarty->assign('article_children', $tree[$cat_id]['children']); //資訊分类树
    } else {
        if(!empty($tree[$cat_id]['children'])) {
            /* We get sub cat list to options */
            $opt = [];
            foreach ($tree[$cat_id]['children'] as $key => $value) {
                $opt[] = ['value'=>$value['cat_id'], 'name'=>$value['cat_name']];
            }
            $smarty->assign('sub_cat_options', $opt);
        }
        $pages  = ($count > 0) ? ceil($count / $size) : 1;

        if ($page > $pages)
        {
            $page = $pages;
        }
        $pager['search']['id'] = $cat_id;
        $keywords = '';
        $goon_keywords = ''; //继续传递的搜索关键词

        /* 获得資訊列表 */
        if (isset($_REQUEST['keywords']))
        {
            $keywords = addslashes(htmlspecialchars(urldecode(trim($_REQUEST['keywords']))));
            $pager['search']['keywords'] = $keywords;
            $search_url = substr(strrchr($_POST['cur_url'], '/'), 1);

            $smarty->assign('search_value',    stripslashes(stripslashes($keywords)));
            $smarty->assign('search_url',       $search_url);
            $count  = get_article_count($cat_id, $keywords);
            $pages  = ($count > 0) ? ceil($count / $size) : 1;
            if ($page > $pages)
            {
                $page = $pages;
            }

            $goon_keywords = urlencode($_REQUEST['keywords']);
        }
        $article_array = get_cat_articles($cat_id, $page, $size, '', 'order');
        $i = 1;
        foreach ($article_array as $key => $article_data)
        {
            $article_array[$key]['i'] = $i;
            $article_array[$key]['title'] = htmlspecialchars($article_data['title']);
            $i++;
        }
        $smarty->assign('article_array', $article_array);
    }
    $smarty->assign('article_list',    get_cat_articles($cat_id, $page, $size ,$keywords));
    $smarty->assign('goods_list', get_promote_goods());
    /* 分页 */
    assign_pager('article_cat', $cat_id, $count, $size, '', '', $page, $goon_keywords);
    assign_dynamic('article_cat');
}

$smarty->display($dwt, $cache_id);

?>