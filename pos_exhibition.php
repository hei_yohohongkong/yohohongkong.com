<?php

/***
 * pos_exhibition.php
 * by Anthony 2017-11-17
 *
 * POS(EXHIBITION) System for YOHO Hong Kong
 ***/

define('IN_ECS', true);

define('FORCE_HTTPS', true); // Require HTTPS for security

define('DESKTOP_ONLY', true); // This page don't have mobile version

define('POS_EXHIBITION', true); // POS EXHIBITION

require(dirname(__FILE__) . '/includes/init.php');

require(ROOT_PATH . 'includes/lib_order.php');
require(ROOT_PATH . 'includes/lib_order_1.php');

/* 载入语言文件 */
require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/user.php');
require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/shopping_flow.php');
require_once(ROOT_PATH . '/includes/stripe/init.php');
include_once('includes/lib_clips.php');
include_once('includes/lib_payment.php');

$paymentController = new Yoho\cms\Controller\PaymentController();
$orderController = new Yoho\cms\Controller\OrderController();
$erpController   = new Yoho\cms\Controller\ErpController();
$order_fee_platform = $orderController::ORDER_FEE_PLATFORM_POS;
$actg = $paymentController->actg;
$actgHooks = $paymentController->actgHooks;

$smarty->assign('lang',	$_LANG);

if ((DEBUG_MODE & 2) != 2)
{
	$smarty->caching = true;
}

// Check admin login
$admin = check_admin_login();
if (!$admin)
{
	if (!empty($_REQUEST['from_app']))
	{
		setcookie('login_from_pos', '1', 0, '/', false, true, true);
		header('Location: /x/privilege.php?act=login');
		exit;
	}
	
	show_message($_LANG['not_login'], $_LANG['back_home'], $ecs->url(), 'error');
	exit;
}
$smarty->assign('admin', $admin);

$action = empty($_REQUEST['act']) ? '' : $_REQUEST['act'];

if($action == 'place_order'){

	if(!$_REQUEST['order_id']){
		show_message('沒有訂單資料', '返回 POS 系統', '/pos_exhibition.php', 'warning');
		exit;
	}
	$order_id = $_REQUEST['order_id'];
	$order    = order_info($order_id);
	$order['log_id'] = $_REQUEST['l_id'];
	if($order['pay_status'] == PS_PAYED){
		show_message('此訂單已付款', '返回 POS 系統', '/pos_exhibition.php', 'warning');
		exit;
	}
	if($order['platform'] != 'pos_exhibition'){
		show_message('此訂單不是展場訂單', '返回 POS 系統', '/pos_exhibition.php', 'warning');
		exit;
	}

	/* 取得支付信息，生成支付代码 */
	if ($order['order_amount'] > 0)
	{
		$payment = payment_info($order['pay_id']);

		include_once('includes/modules/payment/' . $payment['pay_code'] . '.php');

		$pay_obj    = new $payment['pay_code'];

		$pay_online = $pay_obj->get_code($order, unserialize_config($payment['pay_config']));

		$order['pay_desc'] = $payment['pay_desc'];
        $smarty->assign('payment', $payment);
		$smarty->assign('pay_online', $pay_online);

		$smarty->assign('payment_amount', $actgHooks->getOrderPaymentAmount($order));
	}

	if ($order['user_id'] > 0)
	{
		/* 取得用户信息 */
		$user = user_info($order['user_id']);
		$smarty->assign('user',      $user);
	}
	$smarty->assign('step', 'done');
	$smarty->display('pos_pay.dwt');
	exit;
}
elseif ($action == 'order_pay'){

	// $payment = get_payment($_REQUEST['pay_code']);
	$pay_code = $_REQUEST['pay_code'];
	$plugin_file = 'includes/modules/payment/' . $pay_code . '.php';

	if (file_exists($plugin_file))
	{
		include_once($plugin_file);
		$payment = new $pay_code();
		$success = @$payment->respond();
	}
	else
	{
		$success = false;
	}

	// If pay success, handle order
	if($success){
		$order_id = $_REQUEST['order_id'];
		if(empty($order_id)){
			show_message('操作失敗', '返回 POS 系統', '/pos_exhibition.php', 'warning');
			exit;
		}
		$arr['shipping_status'] = SS_RECEIVED;
		$arr['shipping_time']   = gmtime();
		if(update_order($order_id, $arr)){
			$erpController->pos_erp_delivery($order_id, $admin, 'ex_normal');

			$smarty->assign('step', 'done');
			$smarty->assign('order', order_info($order_id));
			$smarty->assign('back_href', '/pos_exhibition.php');
			assign_dynamic('shopping_flow');
			$smarty->display('flow_quick.dwt');
			exit;
		}
		
	} else{
		show_message('操作失敗', '返回 POS 系統', '/pos_exhibition.php', 'warning');
		exit;
	}
}
/*------------------------------------------------------ */
//-- 判断是否存在缓存，如果存在则调用缓存，反之读取相应内容
/*------------------------------------------------------ */
/* 缓存编号 */
$cache_id = sprintf('%X', crc32($_SESSION['user_rank'] . '-' . $_CFG['lang']));
if (!$smarty->is_cached('pos_exhibition.dwt', $cache_id))
{
	assign_template();
	//$cat = get_cat_info($cat_id);   // 获得分类的相关信息
	$position = assign_ur_here(0, '展場 - POS 系統');
	$smarty->assign('page_title',	  $position['title']);	// 页面标题
	$smarty->assign('ur_here',		 $position['ur_here']);  // 当前位置
	/* meta information */
	$smarty->assign('keywords',		htmlspecialchars($_CFG['shop_keywords']));
	$smarty->assign('description',	 htmlspecialchars($_CFG['shop_desc']));
	
	// Shipping methods
	$shipping_list = shipping_list(1);
	if(count($shipping_list) <= 0)$shipping_list = available_shipping_list(array(1,3409)); // array(3409)
	$shipping_list_output = array();
	foreach ($shipping_list as $shipping)
	{
		if(in_array($shipping['shipping_id'], [2, 3])) // TODO: hard code shipping: 門市購買, 速遞
		$shipping_list_output[] = array(
			'shipping_id' => $shipping['shipping_id'],
			'shipping_name' => $shipping['shipping_name']
		);
	}
	$smarty->assign('shipping_list', $shipping_list_output);
	
	// Warehouselist
	$erpController = new Yoho\cms\Controller\ErpController();
	$smarty->assign('warehouse_list', $erpController->getWarehouseList());

	// Payment methods
	$filter_data['is_ex_pos'] = true;
	$payment_list = $paymentController->available_payment_list(true, 0, false, $filter_data);

	$smarty->assign('payment_list', $payment_list);
	
	// Integral scale
	$smarty->assign('integral_scale', $_CFG['integral_scale']);
	
	// List of salesperson available in POS
	$smarty->assign('salespeople', $db->getAll('SELECT sales_id, sales_name FROM ' . $GLOBALS['ecs']->table('salesperson') . ' AS sp WHERE available = 1 AND allow_pos = 1'));

	// List of sales agent with rate
	$smarty->assign('sales_agents', get_pos_sales_agent());
    
  	// List country **( 3 is hard code shipping id : 速遞)
	$smarty->assign('country_list',      get_shipping_available_countries(3));

	// Unpay list
	$unpaySql =  " SELECT o.order_id, o.order_sn, o.order_amount, pl.log_id FROM ".$ecs->table('order_info')." as o ".
	" LEFT JOIN ".$ecs->table('pay_log')." as pl ON pl.log_id = (SELECT log_id FROM ".$ecs->table('pay_log')." WHERE order_id = o.order_id order by log_id DESC LIMIT 1 )".
	" WHERE o.platform = 'pos_exhibition' AND o.pay_status = '".PS_UNPAYED."' ";

	$o_list = $db->getAll($unpaySql);
	$unpay_list = [];
	foreach ($o_list as $key => $order) {
		$order['href'] = '/pos_exhibition.php?act=place_order&order_id='.$order['order_id'].'&l_id='.$order['log_id'];
		$unpay_list[] = $order;
	}
	$smarty->assign('unpay_list', $unpay_list);
}

$smarty->display('pos_exhibition.dwt', $cache_id);

exit;

function output_json($obj)
{
	header('Content-Type: application/json');
	echo json_encode($obj);
	exit;
}
?>

