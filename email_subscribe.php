<?php
	$action = (isset($_GET['act'])) ? $_GET['act'] : '';
	
	if ($action == 'subscribed')
	{
		// Set a cookie that will expire in 2038-1-1 (nearly the end of 32bit unix timestamp)
		setcookie('email_subscribed', '2', mktime(0,0,0,1,1,2038), '/', '.yohohongkong.com');
		header('Location: /');
		exit;
	}
	elseif ($action == 'confirmed')
	{
		// Set a cookie that will expire in 2038-1-1 (nearly the end of 32bit unix timestamp)
		setcookie('email_subscribed', '3', mktime(0,0,0,1,1,2038), '/', '.yohohongkong.com');
		header('Location: /');
		exit;
	}
	else
	{
		// Reset the "prompted" cookie by expiring it on 1990, so the website will display the subscribe from
		setcookie('email_subscribe_prompted', '0', mktime(0,0,0,1,1,1990), '/', '.yohohongkong.com');
		header('Location: /');
		exit;
	}
?>