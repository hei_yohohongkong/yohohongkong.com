/***
 * pos.js
 * by howang 2014-06-05
 *
 * POS System for YOHO Hong Kong
 ***/

/* jshint -W041 */

var YOHO = YOHO || {};

YOHO.config = YOHO.config || {};

YOHO.config.disableCommonInit = true;
YOHO.config.posMode = 'normal';
YOHO.config.currentUserWholesale = false;

function updateCurrentUserWholesale(newVal){
	YOHO.config.currentUserWholesale=newVal;
	//Trigger DOM
	if(newVal==true){
		if ($('.order_modes a.active').data('mode') != "wholesale") {
			$('.order_modes a[data-mode=wholesale]').click();
		}
		$('input[name="is_wholesale"]').prop("checked", true);
		//TODO: alert, hard coded here
		$('input[name="shipping"][value=4]').prop("checked", true).trigger("click");
		$('input[name="payment"][value=11]').prop("checked", true).trigger("click");
		$('.retail_only').hide();
		$('.wholesale_only').show();
		if($(".ws_price_th").length == 0) {
			var newPriceTh = document.createElement('th');
			newPriceTh.innerHTML = "批發銷售價格";
			newPriceTh.className = "ws_price_th";
			$('#cart_table thead').children().append(newPriceTh);
		}
		$('input[name="already_member"][value="0"]').prop('disabled', true);
		$('input[name="already_member"][value="1"]').trigger('click');

	}else{
		$('input[name="is_wholesale"]').prop("checked", false);
		$('input[name="invoice_no"]').val('');
		$('.retail_only').show();
		$('.wholesale_only').hide();
		var wsRriceTh = $('#cart_table thead').children().find(".ws_price_th");
		wsRriceTh.remove();
		$('input[name="already_member"][value="0"]').prop('disabled', false);
		$('input[name="ws_customer_id"]').val('');
	}

}

(function($) {

$(".wholesale_only").hide();


$('input[name="is_wholesale"]').on("change",function(e){
	if($(this).prop('checked')==true){
		updateCurrentUserWholesale(1);
	}else{
		updateCurrentUserWholesale(0);
	}

});

YOHO.pos = {
	data: {
		goods_list: [],
		have_preorder_goods: false,
		force_reserve_mode: false,
		sa_rates: [],
		goods_replace_price: new Object
	},
	
	init: function() {
		this.readBarcode();
		this.addToCart(0, 0);
		this.orderModes();
		this.autosizeTextareas();
		this.shippingPayment();
		this.countryCodePrefix();
		this.lookupMember();
		this.calculateDiscount();
		this.verifyCoupon();
		this.handleDeposit();
		this.handleReserve();
		this.checkMobilePhoneNumber();
		this.checkEmailAddress();
		this.assignSalesperson();
		this.loadUserInput();
		this.changeCash();
		this.confirmOrder();
		this.saPriceInput();
		this.displayZip();
		this.userRankChange();
		this.isPayPayment();
		this.wsPriceInput();
		this.need_removal();
	},
	
	initRepair: function() {
		this.orderModes();
		this.autosizeTextareas();
		this.countryCodePrefix();
		this.checkMobilePhoneNumber();
		this.checkEmailAddress();
		this.assignSalesperson();
		this.repairHaveOrder();
		this.lookupRepairOrder();
		this.lookupRepairGoods();
		this.handleRepairFee();
		this.confirmRepair();
	},
	
	saPriceInput: function(){
		var _this = this;
		$(document).on('change', 'input[class="sa_price_input"]', function () {
			var goods_id = $(this).data("goods_id");
			// Add replace price to data
			if(this.value !== "" && this.value !== null){
				_this.data.goods_replace_price[""+goods_id] = this.value;
			} else {
				delete  _this.data.goods_replace_price[""+goods_id];
			}
			// Update order price
			_this.updateOrderPrice();
		});
	},

	updateOrderPrice: function(){
		var _this = this;
		var order_mode = $('.order_modes a.active').data('mode');
		/*
		if (order_mode == 'sales-agent'){
			var sa = $('#sa_select').val();
		} else {
			var sa = -1;
		}
		*/

		$.ajax({
			url: '/pos.php',
			type: 'post',
			cache: false,
			data: {
				'act':'update_order_price',
				'goods_replace_price': _this.data.goods_replace_price,
				'shipping':$('input[name="shipping"]:checked').val(),
				'payment':$('input[name="payment"]:checked').val(),
				'country':$('#country').val(),
				//'order_mode':order_mode,
				//'sa':sa
			},
			dataType: 'json',
			success: function(result) {
				// Update goods price
				$.each(result.cart_list, function (idx, goods) {
					if (goods.custom_price && goods.custom_subtotal){
						$('#goods_price_'+goods.goods_id).html(goods.custom_price);
						$('#goods_subtotal_'+goods.goods_id).html(goods.custom_subtotal);
						/*
						if (order_mode == 'sales-agent' && sa > 0){
							$("input[name=sa_price_" + goods.goods_id + "]").val(goods.custom_subtotal.replace('HK$ ',''));
						}
						*/
					} else {
						$('#goods_price_'+goods.goods_id).html(goods.goods_price);
						$('#goods_subtotal_'+goods.goods_id).html(goods.subtotal);
					}
				});
				// Update subtotal and order total
				$('#goods_amount').html(result.goods_amount);
				$('#order_total').html(result.order_total);
				_this.calculateCashChange();
			},
			error: function(xhr) {
				YOHO.dialog.warn("伺服器繁忙，請稍後再試。("+xhr.status+")");
				console.log('error: ' + JSON.stringify(xhr));
			}
		});
	},

	//set sales agent layout
	salesagentLayout: function(){
		var _this = this;

		if ($('input[name="payment"]:checked').val() == 99){
			return;
		}
		// If non-select sales agent, remove all layout
		var sel = $('#sa_select');
		if(sel.val() === '0') {
			_this.clearSaLayout();
			return;
		}

		if (_this.data.goods_list.length > 0 ){
			// Call ajax to query salesagent rate
			$.ajax({
				url: '/pos.php',
				type: 'post',
				cache: false,
				data: {
					'act':'get_sales_agent',
					'sa_id':sel.val()
				},
				dataType: 'json',
				success: function(data) {
					YOHO.pos.data.sa_rates = data.rates;

					var goods_rates = [];
					$.each(_this.data.goods_list, function (idx, goods) {
						//check if have selected value
						goods_rates[goods.goods_id] = $('select[name="goods_rates_'+goods.goods_id+'"]').val();
					});

					var cart_thead = $('#cart_table thead');
					if(cart_thead.is(":visible")){
						// If no th, create it.
						if (!cart_thead.children().find(".sa_rate_th").length >0 ){
							createSaThead();
						}
					}
					//empty cart table
					$('#cart_table tbody').empty();

					//create cart goods
					$.each(_this.data.goods_list, function (idx, goods) {
						selectSingleSaRow(idx, goods, goods_rates);
					});

				},
				error: function(xhr) {
					YOHO.dialog.warn("伺服器繁忙，請稍後再試。("+xhr.status+")");
					console.log('error: ' + JSON.stringify(xhr));
				}
			}); //end call ajax
		}

		// Create sales agent td row
		function selectSingleSaRow(idx, goods, goods_rates){

			/* Default cart table layout */
			var tbody = '<tr>'+
				'<td style="text-align: left;">' + goods.goods_name + '</td>' +
				'<td nowrap="nowrap" id="goods_price_'+goods.goods_id+'">' + goods.goods_price + '</td>' +
				'<td><a class="stock_breakdown" href="javascript:void(0)" data-gid="' + goods.goods_id + '" data-pos="1"><span class="font200">' + goods.goods_number + '</span></a></td>' +
				'<td nowrap="nowrap" id="goods_subtotal_'+goods.goods_id+'">' + goods.subtotal + '</td>' +
				'<td><a href="javascript:void(0)" title="取消訂購該產品" class="delete" data-goods-id="' + goods.goods_id + '"></a></td>'+
				'</tr>';

			$('#cart_table tbody').append(tbody);

			// Bind removeFromCart event
			$('#cart_table a.delete').click(function (event) {
				_this.removeFromCart($(event.target).data('goodsId'));
			});
			/*  End efault cart table layout */

			/* Create select options input */
			var select = '';
			var select_head = '<select name="goods_rates_' + goods.goods_id + '">';
			var select_body = '';
			$.each(YOHO.pos.data.sa_rates, function (key, rate) {
				// If have old same good rate, selected it.
				if (goods_rates[goods.goods_id] && (goods_rates[goods.goods_id] == rate.sa_rate_id)) {
					select_body += '<option value="' + rate.sa_rate_id + '" selected="selected">' + rate.sa_rate + '%</option>';
				// If no old same good rate, selected default rate.
				} else if(!goods_rates[goods.goods_id] && rate.is_default == '1') {
					select_body += '<option value="' + rate.sa_rate_id + '" selected="selected">' + rate.sa_rate + '%</option>';
				} else {
					select_body += '<option value="' + rate.sa_rate_id + '">' + rate.sa_rate + '%</option>';
				}
			});
			var select_foot = '</select>';
			select = select_head + select_body + select_foot;
			var td = document.createElement('td');
			td.className = "sa_rate_td";
			td.innerHTML = select;
			$('#cart_table tbody').children().eq(idx).append(td);
			/*  End create select options input */

			/* Create custom price input */
			var td2 = document.createElement('td');
			td2.className = "sa_price_td";
			var price_input = document.createElement('input');
			price_input.type = 'text';
			price_input.size = '5';
			price_input.className = 'sa_price_input';
			price_input.name = 'sa_price_' + goods.goods_id;
			price_input.dataset.goods_id = goods.goods_id;
			// If have old custom price, replace it.
			if(_this.data.goods_replace_price[goods.goods_id]){
				price_input.value = _this.data.goods_replace_price[goods.goods_id];
			}
			td2.append(price_input);
			$('#cart_table tbody').children().eq(idx).append(td2);
			/* End create custom price input */

		}

		// Create sales agent thead on cart table
		function createSaThead() {
			var th = document.createElement('th');
			th.innerHTML = "手續費";
			th.className = "sa_rate_th";
			$('#cart_table thead').children().append(th);
			var newPriceTh = document.createElement('th');
			newPriceTh.innerHTML = "代理銷售價格";
			newPriceTh.className = "sa_price_th";
			$('#cart_table thead').children().append(newPriceTh);
		}
	},

	// Clear sales agent layout
	clearSaLayout: function() {

		// Clear cart table sa column
		var saRateTh = $('#cart_table thead').children().find(".sa_rate_th");
		var saRriceTh = $('#cart_table thead').children().find(".sa_price_th");
		var saRateTd = $('#cart_table tbody').children().find('.sa_rate_td');
		var saPriceTd = $('#cart_table tbody').children().find('.sa_price_td');
		$('#sa_shipping').remove();
		if(!$('input[name="shipping"]:checked').val()){
			$('input[name="shipping"][value=2]').prop("checked", true);
		}
		saRateTh.remove();
		saRriceTh.remove();
		saRateTd.remove();
		saPriceTd.remove();

		//  Clear sales agent select box, sales agent sn input
		$('#sa_select').val(0);
		$('input[name="sa_sn"]').val("");

		// Clear data
		this.data.sa_rates = [];

		if(Object.keys(this.data.goods_replace_price).length > 0) {
			this.data.goods_replace_price = new Object;
			// Update order price
			this.updateOrderPrice();
		}
	},
	
	wsPriceInput: function(){
		var _this = this;
		$(document).on('change', 'input[class="ws_price_input"]', function () {
			var goods_id = $(this).data("goods_id");
			// Add replace price to data
			if(this.value !== "" && this.value !== null){
				_this.data.goods_replace_price[""+goods_id] = this.value;
			} else {
				delete  _this.data.goods_replace_price[""+goods_id];
			}
			// Update order price
			_this.updateOrderPrice();
		});
	},
	readBarcode: function() {
		var _this = this;
		$('.search_goods_sn').keypress(function(event) {
			// ASCII cod 13 = Enter
			if (event.which == 13)
			{
				event.preventDefault();
				
				var barcode = $(this).val();
				_this.searchGoodsBySN(barcode);
			}
		}).focus();
	},
	
	searchGoodsBySN: function (goods_sn) {
		var _this = this;
		var user_rank = $('#user_rank').val();
		var mode = $('.order_modes a.active').data('mode');
		if (mode == 'retail'){
			mode = $('.order_modes #retail_menu a.active').data('mode');
		}
		$.ajax({
			url: '/pos.php',
			type: 'post',
			cache: false,
			data: {
				'act':'get_goods_by_sn',
				'goods_sn':goods_sn,
				'user_rank':user_rank,
				'order_mode':mode,
			},
			dataType: 'json',
			success: function(data) {
				// Clear Barcode field
				$('.search_goods_sn').val('');
				
				if (YOHO.config.posMode == 'normal')
				{
					_this.loadSearchResults(data);
				}
				else if (YOHO.config.posMode == 'repair')
				{
					_this.loadRepairGoods(data);
				}
			},
			error: function(xhr) {
				YOHO.dialog.warn("伺服器繁忙，請稍後再試。("+xhr.status+")");
				console.log('error: ' + JSON.stringify(xhr));
			}
		});
	},
	
	loadSearchResults: function (data) {
		var _this = this;
		if (data.status == 1)
		{
			// Clear goods list
			$('#results_table').find('tbody').empty();
			
			// Populate goods info
			$.each(data.goods_list, function (idx, goods) {
				var goods_id = goods.goods_id;
				
				$('#results_table').find('tbody').append($('<tr data-goods-id="' + goods_id + '">'+
				'<td style="text-align: left;">'+
					'<img src="'+goods.goods_img+'" width="65" height="65">'+
					'<a href="goods.php?id=' + goods_id + '" target="_blank" class="goods_link">'+
						goods.goods_name+
					'</a>'+
				'</td>'+
				'<td>'+
					'<a href="x/goods.php?act=list&amp;keyword=' + goods.goods_sn + '" target="_blank">' +
						goods.goods_sn+
					'</a>'+
				'</td>'+
				'<td><a class="stock_breakdown" href="javascript:void(0)" data-gid="' + goods_id + '" data-pos="1">' + goods.goods_number + '</a></td>'+
				'<td>' + goods.shop_price + '</td>'+
				'<td>'+
					'<input type="text" size="5" value="1" class="addtocart_input"> '+
					'<a href="javascript:void(0)" class="addtocart_btn"><i class="iconfont">ŭ</i></a>'+
				'</td>'+
				'</tr>'));
			});
			
			_this.bindGoodsLinks();
			
			// Add to cart when user press ENTER (ascii code = 13) or TAB (trigger blur)
			$('.addtocart_input').keypress(function(event) {
				if (event.which == 13)
				{
					event.preventDefault();
					$(this).blur(); // trigger the blur event
				}
			}).blur(function(event) {
				var goods_id = $(this).closest('tr').data('goodsId');
				var qty = $(this).val();
				_this.addToCart(goods_id, qty);
			});
			
			// Alternatively user can click the add to cart button
			$('.addtocart_btn').click(function(event) {
				var $goods_input = $(this).closest('tr').find('.addtocart_input');
				$goods_input.blur(); // trigger the blur event binded above
			});
			
			if (data.goods_list.length == 1)
			{
				// As suggested by kwong, auto select the textbox
				$('.addtocart_input:first').focus();
			}
		}
		else
		{
			$('#results_table').find('tbody').html('<tr id="goods_list_td">'+
				'<td colspan="5" style="text-align: left;"><span style="color:red">' + data.error + '</span></td>'+
			'</tr>');
		}
	},
	
	bindGoodsLinks: function () {
		var _this = this;
		// Show product info upon mouseover
		$('tr[data-goods-id] .goods_link').off('mouseover').on('mouseover', function(event) {
			_this.showProductInfo($(this).closest('tr').data('goodsId'), $(this));
		});
		
		// if user keep his mouse over when we update the goods list, existing productInfo box
		// can no longer be closed as the original element is replaced as we populate new data
		// therefore we need to hide all existing productInfo here
		$('.productInfo').hide();
	},
	
	showProductInfo: function (goods_id, element) {
		var $productInfo;
		// If the productInfo div is present, show it immediately 
		if ($('#productInfo' + goods_id).length > 0)
		{
			$productInfo = $('#productInfo' + goods_id);
			$productInfo.css({ 'top': element.offset().top + element.height() / 2 });
			$productInfo.show();
			element.off('mouseout');
			element.on('mouseout', function (event) {
				$productInfo.hide();
			});
		}
		// Otherwise we add the div to body, and query product info from server with ajax
		else
		{
			// Append new div to body first, to prevent rapid calls to showProductInfo end up with multiple server request
			$productInfo = $('<div class="productInfo" id="productInfo' + goods_id + '"></div>');
			$productInfo.html('<div style="text-align: center;"><img src="/images/loading.gif"> 載入產品資料中</div>');
			$productInfo.appendTo('body');
			$productInfo.css({ 'top': element.offset().top + element.height() / 2 });
			$productInfo.show();
			$productInfo.mouseout(function (event) {
				$productInfo.hide();
			});
			element.off('mouseout');
			element.on('mouseout', function (event) {
				$productInfo.hide();
			});
			
			$.ajax({
				url: '/pos.php',
				type: 'post',
				cache: false,
				data: {
					'act': 'get_goods_preview',
					'goods_id': goods_id
				},
				dataType: 'json',
				success: function(data) {
					// Populate the html response from server into our div
					if (data.status == 1)
					{
						$productInfo.html(data.result);
					}
					else
					{
						$productInfo.html('<span style="color: red;">' + data.error + '</span>');
					}
					
					// If user's mouse is still on the element, show it now
					if (element.is(':hover'))
					{
						$productInfo.show();
					}
					element.off('mouseout');
					element.on('mouseout', function (event) {
						$productInfo.hide();
					});
				},
				error: function(xhr) {
					// If we encounter an error, remove the $productInfo so user can trigger the ajax again
					$productInfo.remove();
					console.log('error: ' + JSON.stringify(xhr));
				}
			});
		}
	},
	
	addToCart: function(goods_id, qty, is_gift, clean_integral, is_package) {
		is_gift = is_gift || 0;
		is_package = is_package || 0;
		clean_integral = clean_integral || 1;
		var _this = this;
		var user_rank = $('#user_rank').val();
		var mode = $('.order_modes a.active').data('mode');
		if (mode == 'retail'){
			mode = $('.order_modes #retail_menu a.active').data('mode');
		}

		$.ajax({
			url: '/pos.php',
			type: 'post',
			cache: false,
			data: {
				'act':'add_to_cart',
				'goods_id':goods_id,
				'quantity':qty,
				'shipping':$('input[name="shipping"]:checked').val(),
				'payment':$('input[name="payment"]:checked').val(),
				'country':$('#country').val(),
				'order_mode':mode,
				'user_rank':user_rank,
				'is_gift': is_gift,
				'is_package': is_package,
				'clean_integral':clean_integral
			},
			dataType: 'json',
			success: function(data) {
				if (data.status == 1)
				{
					// Save the goods list into our data object
					_this.data.goods_list = data.cart_list;

					// Update the cart list
					if (data.cart_list.length > 0)
					{
						$('#cart_table thead').show();

						// If in sales-agent
						if ($('.order_modes a.active').data('mode') == 'sales-agent' && $('#sa_select').val() != '0' )
						{
							_this.salesagentLayout();
						} else {
							$('#cart_table tbody').empty();
							$.each(data.cart_list, function (idx, goods) {
								console.log(goods);
								var is_vip = '';
								var has_require_goods = '';
								if(goods.is_vip == 1) is_vip = "<span style='color:red;padding-left:10px;'>&nbsp;VIP售價</span>";
								if(goods.has_require_goods == 1) has_require_goods = "<a href='#goto_requiregoods_" + goods.goods_id + "'><span style='color:blue;padding-left:10px;' href='#goto_requiregoods_" + goods.goods_id + "'>&nbsp;變壓器</span></a>";
								var is_gift = '';
								if(goods.is_gift > 0){
									if(goods.goods_price=="HK$ 0.00"){
										is_gift = "<span style='color:red;'>&nbsp;贈品</span>";
									}
									else{
										is_gift = "<span style='color:red;'>&nbsp;優惠品</span>";
									}
								}
								var is_package = '';
								if(goods.is_package > 0) is_package = "<span style='color:red;'>&nbsp;套裝優惠</span>";
								$('#cart_table tbody').append('<tr>'+
									'<td style="text-align: left;">' + '<img src="'+goods.goods_img+'" width="65" height="65">'+goods.goods_name + is_vip + is_gift + is_package + has_require_goods + '</td>' +
										'<td nowrap="nowrap" id="goods_price_'+goods.goods_id+'">' + goods.goods_price + '</td>' +
									'<td><a class="stock_breakdown" href="javascript:void(0)" data-gid="' + goods.goods_id + '" data-pos="1"><span class="font200">' + goods.goods_number + '</span></a></td>' +
										'<td nowrap="nowrap" id="goods_subtotal_'+goods.goods_id+'">' + goods.subtotal + '</td>' +
									'<td><a href="javascript:void(0)" title="取消訂購該產品" class="delete '+(goods.is_auto_add ? 'is_auto_add':'')+'" data-goods-id="' + goods.goods_id + '" data-is-gift="'+goods.is_gift+'" data-is-package="'+goods.is_package+'"></a></td>'+
									'</tr>');
								if ($('.order_modes a.active').data('mode') == 'wholesale')
								{
									/* Create custom price input */
									var td2 = document.createElement('td');
									td2.className = "ws_price_td";
									var price_input = document.createElement('input');
									price_input.type = 'text';
									price_input.size = '5';
									price_input.className = 'ws_price_input';
									price_input.name = 'ws_price_' + goods.goods_id;
									price_input.dataset.goods_id = goods.goods_id;
									// If have old custom price, replace it.
									if(_this.data.goods_replace_price[goods.goods_id]){
										price_input.value = _this.data.goods_replace_price[goods.goods_id];
									}
									td2.append(price_input);
									$('#cart_table tbody').children().eq(idx).append(td2);
									/* End create custom price input */
								}
							});

							// Bind removeFromCart event
							$('#cart_table a.delete').click(function (event) {
								//console.log($(event.target));
								_this.removeFromCart($(event.target).data('goodsId'), $(event.target).data('isGift'), $(event.target).data('isPackage'));
							});
							var $need_removal = data.need_removal;
							$('#collection_total').html("");
							if(Object.keys($need_removal).length > 0) {
								$html = '';
								$.each($need_removal, function ($cat_id, $nr) {
									$html += '<div class="nr_item">';
									$html += '	'+$nr.cat_name+' <input type="number" name="nr_cat['+$nr.cat_id+']" max="'+$nr.total+'" min="0" value="'+$nr.total+'" class="nr_input">部';
									$html += '</div>';
								});
								$('#collection_total').html($html);
								$('.need_removal').show();
							}

						}
					}
					else
					{
						if ($('input[name="need_removal"]').prop('checked')) {
							$('input[name="need_removal"]').prop('checked', false).trigger('change');
						}
						$('#collection_total').html("");
						$('.need_removal').hide();
						$('#cart_table thead').hide();
						$('#cart_table tbody').html('<tr><td colspan="5">訂單中暫時沒有產品</td></tr>');

					}
					
					if(Object.keys(_this.data.goods_replace_price).length > 0) {
						// Update order price
						_this.updateOrderPrice();
					} else {
					_this.handleFavourable();
					_this.handleRequireGoods();
					// Update subtotal and order total
					$('#goods_amount').html(data.goods_amount);
					$('#order_total').html(data.order_total);
					_this.calculateCashChange();
					}
					// Update postscript with preorder_msg
					var postscript = $('textarea[name="postscript"]').val();
					// postscript = postscript.replace(/(?:^|\r?\n)(.+) 訂貨 \d+天(\r?\n)/g, ''); // remove lines with "訂貨"
					$('textarea[name="order_remarks"]').val(data.preorder_msg).trigger('keyup');
					YOHO.pos.data.have_preorder_goods = (data.preorder_msg.length > 0);

					/*
					if ($('.order_modes a.active').data('mode') == 'sales-agent' && $('#sa_select').val() > 0){
						_this.updateOrderPrice();
					}
					*/

				}
				else
				{
					YOHO.dialog.warn(data.error);
				}
			},
			error: function(xhr) {
				YOHO.dialog.warn("伺服器繁忙，請稍後再試。("+xhr.status+")");
				console.log('error: ' + JSON.stringify(xhr));
			}
		});
	},
	
	removeFromCart: function(goods_id, is_gift, is_package) {
		delete  this.data.goods_replace_price[""+goods_id];
		this.addToCart(goods_id, 0, is_gift, 0, is_package);
	},
	
	orderModes: function() {
		var _this = this;
		$('.order_modes div a').click(function (){
			var $btn = $(this);
			var mode = $btn.data('mode');
			$('input[name="reserve"]').closest('tr').hide();
			$('input[name="deposit"]').closest('tr').hide();

			if (mode == 'retail')
			{
				$btn.addClass('active');
				$btn.closest('.order_modes').find('a').not($btn).removeClass('active');
				$(".order_modes #retail_menu").show();
				$('a[data-mode="normal"]').trigger('click');
				return;
			}
			
			if (YOHO.config.posMode == 'repair')
			{
				if (mode != 'repair')
				{
					window.open('/pos.php');
					return;
				}
				return;
			}
			
			// 維修
			if (mode == 'repair')
			{
				window.open('/pos_repair.php');
				return;
			}
			
			$btn.addClass('active');
			$btn.closest('.order_modes').find('a').not($btn).removeClass('active');

			if (mode == 'normal')
			{
				$('#menu_retail').addClass('active');
				$('input[name="reserve"]').closest('tr').show();
				$("#reserve_0").prop("checked", true);
			}
			
			// 訂貨
			/*
			if (mode == 'reserve')
			{
				$('input[name="deposit"]').closest('tr').show();
			}
			else
			{
				$('input[name="deposit"]').closest('tr').hide();
				YOHO.pos.data.force_reserve_mode = false;
			}
			*/

			YOHO.pos.data.force_reserve_mode = false;
			
			// 換貨
			if (mode == 'replace')
			{
				$('#menu_retail').addClass('active');
				$('.replace_box').show();
			}
			else
			{
				$('.replace_box').hide();
			}

			// 代理銷售
			if (mode == 'sales-agent')
			{
				$(".order_modes #retail_menu").hide();
				updateCurrentUserWholesale(0);
				$('.sales_agent_only').show();
				$('.salesagent_hide').hide();
				$('#shipping_list').append('<label id="sa_shipping">'+
					'<input name="shipping" type="radio" value="99">代理銷售速遞'+
				'</label>');
				$('input[name="shipping"][value=99]').click(function(event) {
					_this.displayExpress();
				});
				if(_this.data.goods_list.length > 0){
					$('input[name="shipping"][value=99]').prop("checked", true).trigger("click");
					$('input[name="payment"][value=11]').prop("checked", true).trigger("click");
				} else {
					$('input[name="shipping"][value=99]').prop("checked", true);
					$('input[name="payment"][value=11]').prop("checked", true);
					_this.displayExpress();
				}
				$('#sa_select').val(0);

			}
			// 批發銷售
			else if( mode=='wholesale')
			{
				$(".order_modes #retail_menu").hide();
				$('.sales_agent_only').hide();
				$('.salesagent_hide').show();
				_this.clearSaLayout();
				updateCurrentUserWholesale(1);
			} else {
				$('.sales_agent_only').hide();
				$('.salesagent_hide').show();
				_this.clearSaLayout();
				updateCurrentUserWholesale(0);
				_this.displayExpress();
				$('input[name="shipping"]:eq(0)').prop("checked", true).trigger("click");
				$('input[name="payment"]:eq(0)').prop("checked", true).trigger("click");
			}
			_this.addToCart(0, 0);
			if (mode == 'un_pay')
			{
				$(".p9").hide();
				$(".unpay").show();
			} else {
				$(".p9").show();
				$(".unpay").hide();
			}
			
			$('input[name="replace_order_sn"]').keypress(function(event) {
				// ASCII cod 13 = Enter
				if (event.which == 13)
				{
					event.preventDefault();
					
					var order_sn = $(this).val();
					_this.loadExistingOrder(order_sn);
				}
			});
		});

		var requestMode = YOHO.common.getUrlParameter('mode') || 'normal';
		$('.order_modes div a[data-mode="'+requestMode+'"]').trigger('click');
		console.log(requestMode);
	},
	
	loadExistingOrder: function (order_sn) {
		var _this = this;
		$.ajax({
			url: '/pos.php',
			type: 'post',
			cache: false,
			data: {
				'act':'load_existing_order',
				'order_sn': order_sn
			},
			dataType: 'json',
			success: function(data) {
				if (YOHO.config.posMode == 'normal')
				{
					_this.loadReplaceOrder(data);
				}
				else if (YOHO.config.posMode == 'repair')
				{
					_this.loadRepairOrder(data);
				}
			},
			error: function(xhr) {
				YOHO.dialog.warn("伺服器繁忙，請稍後再試。("+xhr.status+")");
				console.log('error: ' + JSON.stringify(xhr));
			}
		});
	},
	
	loadReplaceOrder: function (data) {
		var _this = this;
		if (data.status == 1)
		{
			if ((data.order.shipping_status != 1) && // SS_SHIPPED
				(data.order.shipping_status != 2) && // SS_RECEIVED
				(data.order.shipping_status != 4))   // SS_SHIPPED_PART
			{
				$('#replace_order_goods_table').removeData('orderId');
				$('#replace_order_summary').hide();
				$('#replace_order_goods_table').hide();
				$('#replace_order_goods_table tbody').empty();
				
				YOHO.dialog.warn('此訂單並未出貨');
				
				return;
			}
			
			if (data.order)
			{
				$('#replace_order_summary').html(_this.orderSummaryTable(data.order));
				$('#replace_order_summary').show();
			}
			else
			{
				$('#replace_order_summary').hide();
			}
			
			// Save order_id as the table's data
			$('#replace_order_goods_table').data('orderId', data.order_id);
			
			// Add order sn to postscript
			var postscript = $('textarea[name="postscript"]').val();
			if (postscript.indexOf(data.order.order_sn) == -1)
			{
				$('textarea[name="postscript"]').val('換貨，舊單編號: ' + data.order.order_sn + "\n" + postscript);
			}
			
			// Populate the order goods list
			if (data.order_goods.length > 0)
			{
				$('#replace_order_goods_table').show();
				$('#replace_order_goods_table tbody').empty();
				var warehouse_list='';

				
				$('.warehouse_list').each(function () {
					var selected = '';
					if($(this).data('id') == 2) selected = 'checked="checked" ';
					warehouse_list+='<label><input type="radio" name="replace_warehousing_" value="'+$(this).data('id')+'" '+selected+'/> '+$(this).data('name')+($(this).data('sellable')?'(可銷售)':'(需檢查/維修)')+'</label><br>';
				});

				$.each(data.order_goods, function (idx, goods) {
					var current_warehouse_list = warehouse_list;
					current_warehouse_list = current_warehouse_list.replace(/replace_warehousing_/g,'replace_warehousing_'+goods.rec_id);

					$('#replace_order_goods_table tbody').append('<tr data-goods-id="' + goods.goods_id + '">'+
						'<td style="text-align: left;"><a href="x/goods.php?act=list&amp;keyword=' + goods.goods_sn + '" target="_blank" class="goods_link">' + goods.goods_name + '</a>' + (goods.replaced_qty > 0 ? '<span style="color: red;"> (已換貨<b>' + goods.replaced_qty + '</b>次)</span>' : '') + '</td>' +
						'<td nowrap="nowrap">' + goods.goods_price + '</td>' +
						'<td><a class="stock_breakdown" href="javascript:void(0)" data-gid="' + goods.goods_id + '" data-pos="1"><span class="font200">' + goods.goods_number + '</span></a></td>' +
						'<td><input type="text" size="5" value="0" class="replace_qty" data-rec-id="' + goods.rec_id + '" data-goods-price="' + goods.goods_price + '" data-goods-number="' + goods.goods_number + '"></td>'+
						'<td>'+current_warehouse_list+'</td>'+
						'</tr>');
				});
				
				_this.bindGoodsLinks();
				
				$('.replace_qty').blur(function () {
					// Auto apply discount for replaced goods
					var replaced_total = 0;
					$('.replace_qty').each(function () {
						var $input = $(this);
						var maxQty = parseInt($input.data('goodsNumber')) || 0;
						var qty = parseInt($input.val()) || 0;
						if (qty > maxQty)
						{
							qty = maxQty;
							$input.val(qty);
						}
						var price = parseFloat($input.data('goodsPrice'));
						replaced_total += qty * price;
					});
					if (replaced_total > 0)
					{
						$('input[name="discount"][value="1"]').trigger('click');
						$('input[name="vardiscount"]').val(replaced_total * -1);
						_this.calculateOrderAmount();
					}
					else
					{
						$('input[name="discount"][value="0"]').trigger('click');
					}
				});
			}
			else
			{
				$('#replace_order_goods_table').hide();
				$('#replace_order_goods_table tbody').empty();
			}
			
			// If old order have assoicated user, populate it too
			if (data.user_name)
			{
				$('input[name="already_member"][value="1"]').trigger('click');
				// $('input[name="member_user_name"]').val(data.user_name);
				_this.validateAccount(data.user_name);
			}
		}
		else
		{
			$('#replace_order_goods_table').removeData('orderId');
			$('#replace_order_goods_table').hide();
			$('#replace_order_goods_table tbody').empty();
			
			YOHO.dialog.warn(data.error);
		}
	},
	
	orderSummaryTable: function(order) {
		return '<table border="0" width="100%">' +
			'<tr><th>訂單編號：</th><td><a href="x/order.php?act=info&amp;order_id=' + order.order_id + '" target="_blank">' + order.order_sn + ' <i class="fa fa-external-link"></i></a></td>' +
				'<th>銷售人員：</th><td>' + order.salesperson + '</td>' +
				'<th>訂單來源：</th><td>' + order.platform_display + '</td></tr>' +
			'<tr><th>下單時間：</th><td>' + order.formated_add_time + '</td>' +
				'<th>付款時間：</th><td>' + order.formated_pay_time + '</td>' +
				'<th>出貨時間：</th><td>' + order.formated_shipping_time + '</td></tr>' +
			'<tr><th>產品序號：</th><td colspan="5">' + order.serial_numbers.replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1<br>$2') + '</td></tr>' +
			'<tr><th>客戶備註：</th><td colspan="5">' + order.postscript.replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1<br>$2') + '</td></tr>' +
			'<tr><th>商家備註：</th><td colspan="5">' + order.to_buyer.replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1<br>$2') + '</td></tr>' +
		'</table>';
	},
	
	autosizeTextareas: function() {
		$('textarea').keyup(function() {
			var text = $(this).val();
			var rows = text.replace("\r",'').split("\n").length + 1;
			rows = rows < 2 ? 2 : rows;
			$(this).attr('rows', rows);
		});
	},
	
	shippingPayment: function() {
		var _this = this;
		$('input[name="shipping"],input[name="payment"]').click(function(event) {
			_this.displayExpress();
		});
		$('input[name="payment"]').click(function(event) {
			if($('.order_modes a.active').data('mode') == 'retail' && $('#retail_menu a.active').data('mode') == 'normal'){
				if ($('input[name="payment"]:checked').val() == 99){
					$('.redeem_only').show();
					$('#reserve_0').prop("checked", true);
					$('.pre_order_only').hide();
				} else {
					$('.redeem_only').hide();
					$('.pre_order_only').show();
				}
			}
			_this.isPayPayment();
		});
	},

	isPayPayment: function(){
		var _this = this;
		if ($('#is_pay').length <= 0) {
			return;
		}
		if(	$('input[name="payment"]:checked').val() == 3 || $('input[name="payment"]:checked').val() == 12 ){
			$('input[name="is_pay"]').val('1');
		} else {
			$('input[name="is_pay"]').val('0');
		}
	},
	
	displayExpress: function(){
		var _this = this;
		// If shipping != 速遞
		if(	$('input[name="shipping"]:checked').val() != 2 && $('input[name="shipping"]:checked').val() != 4 && $('input[name="shipping"]:checked').val() != 5 ){
			$('.express_info').show();
			$.ajax({
				url: '/pos.php',
				type: 'post',
				cache: false,
				data: {
					'act':'update_shipping_country',
					'shipping': $('input[name="shipping"]:checked').val()
				},
				dataType: 'json',
				success: function(data) {
					var $sel = $('#country');
					$sel.empty();
					$.each(data.country_list, function(key, value){
						if(value.id != 1){
							$sel.append($("<option></option>").attr("value", value.id).text(value.name));
						} else if(value.default == true){
							$sel.append($("<option></option>").attr("value", value.id).attr("selected","selected").text(value.name));
						}
					})
				},
				error: function(xhr) {
					YOHO.dialog.warn("伺服器繁忙，請稍後再試。("+xhr.status+")");
					console.log('error: ' + JSON.stringify(xhr));
				}
			});
			if(_this.data.goods_list.length > 0){
				_this.calculateOrderAmount();
			}
		} else {
			$('.express_info').hide();
			// Clean Input data
			$('input[name="consignee_name"]').val('');
			$('select[id="country"]').val(3409).trigger("change");
			$('input[name="zipcode"]').val('');;
			$('input[name="address"]').val('');
			$('input[name="consignee_mobile"]').val('');
		}
	},
	
	displayZip: function(){
		var _this = this;
		var countrySelect = $('#country');
		countrySelect.change(function(){
			if(countrySelect.val() == 3409){ // 3409 == Hong Kong
				$('input[name="zipcode"]').val('');
				$('.zipcode_display').hide();
			} else{
				$('.zipcode_display').show();
			}
			if(_this.data.goods_list.length > 0){
				// Revalidate integral
				_this.calculateOrderAmount();
			}
		});
	},
	
	countryCodePrefix: function() {
		var opts = {
			defaultCountry: 'hk',
			preferredCountries: ['hk','mo','tw','cn'],
			utilsScript: '/yohohk/js/intlTelInput.utils.min.js',
			autoFormat: false,
			autoPlaceholder: false
		};
		
		if (YOHO.config.posMode == 'normal')
		{
			$('input[name="member_user_name"]').intlTelInput(opts);
			$('input[name="mobile"]').intlTelInput(opts);
			$('input[name="consignee_mobile"]').intlTelInput(opts);
		}
		else if (YOHO.config.posMode == 'repair')
		{
			$('input[name="mobile"]').intlTelInput(opts);
			$('input[name="consignee_mobile"]').intlTelInput(opts);
		}
	},
	
	cccToCountryCode: function(ccc) {
		var code = 'hk';
		$.each($.fn.intlTelInput.getCountryData(), function(i, country) {
			if (country.dialCode == ccc)
			{
				code = country.iso2;
				return false; // breaks $.each
			}
		});
		return code;
	},
	
	lookupMember: function() {
		var _this = this;
		$('input[name="already_member"]').click(function(event) {
			if ($(event.target).val() == 1)
			{
				$('#already_member_span').show();
			}
			else
			{
				$('#already_member_span').hide();
				// Reset to no user_name
				$('input[name="member_user_name"]').val('');
				// Using integral?
				if ($('input[name="integral"]').val() > 0)
				{
					// Reset to not use integral
					$('input[name="integral"]').val('');
					// Revalidate integral
					_this.calculateOrderAmount();
				}
			}
		});
		
		$('input[name="member_user_name"]').keypress(function(event) {
			if (event.which == 13)
			{
				event.preventDefault();
				
				$(this).trigger('blur');
			}
		}).blur(function(event) {
			var ccc = $(this).intlTelInput('getSelectedCountryData').dialCode;
			var mobile = $(this).val();
			var user_name = ccc ? '+' + ccc + '-' + mobile : mobile;
			_this.validateAccount(user_name);
		});
		$('#validate_account').click(function(event) {
			$('input[name="member_user_name"]').trigger('blur');
		});
		$('#mobile_').keypress(function(event) {
			if (event.which == 13)
			{
				event.preventDefault();
				
				$(this).trigger('blur');
			}
		}).blur(function(event) {
			var ccc = $(this).intlTelInput('getSelectedCountryData').dialCode;
			var mobile = $(this).val();
			var user_name = ccc ? '+' + ccc + '-' + mobile : mobile;
			_this.validateAccount(user_name);
		});
	},
	
	validateAccount: function(user_name) {
		var _this = this;
		$.ajax({
			url: '/pos.php',
			type: 'post',
			cache: false,
			data: {
				'act':'validate_account',
				'user_name': user_name
			},
			dataType: 'json',
			success: function(data) {
				if (YOHO.config.posMode == 'normal')
				{
					if (data.status == 1)
					{
						if ($('.order_modes a.active').data('mode') == 'wholesale' && data.ws_customer_id == '0'){
							YOHO.dialog.warn("此會員並未連結批發客戶資料");
							return false;
						}
						if ($('.order_modes a.active').data('mode') != "wholesale" && data.is_wholesale != 0) {
							$('.order_modes a[data-mode=wholesale]').click();
						} else {
							updateCurrentUserWholesale(data.is_wholesale);
						}
						if(YOHO.config.currentUserWholesale==true && data.is_wholesale==0){
							YOHO.dialog.warn("此會員並不是批發會員!請手動建立新批發帳戶");
							return false;
						} else if (YOHO.config.currentUserWholesale == true) {
							if (data.ws_customer_id == '0') {
								YOHO.dialog.warn("此會員並未連結批發客戶資料");
								return false;
							} else {
								if ($('.order_modes a.active').data('mode') != "wholesale") {
									$('.order_modes a[data-mode=wholesale]').trigger('click');
								}
								$('input[name=invoice_no]').val(data.ws_company)
								$('input[name="ws_customer_id"]').val(data.ws_customer_id);
							}
						}

						$('#user_surplus').html(''+
						//'<input name="payment" type="radio" value="4">'+
						//' 餘額:' + data.user_money +
						' 積分:<span id="pay_points">' + data.pay_points + '</span>' +
						' 使用 <input type="text" name="integral" value="0" style="width:40px;text-align:center;">' +
						' <input id="validate_integral" type="button" class="bnt_yellow" value="確定">');
						
						$('span#pay_points').click(function(event) {
							var user_integral = parseInt($.trim($(this).text())) || 0;
							var integral_scale = parseFloat($.trim($('#integral_scale').text())) || 0.5;
							var one_dollar_integral = (100 / integral_scale) || 100;
							var optimum_integral = parseInt(user_integral / one_dollar_integral) * one_dollar_integral;
							$('input[name="integral"]').val(optimum_integral);
						});
						$('input[name="integral"]').keypress(function(event) {
							if (event.which == 13)
							{
								event.preventDefault();
								
								_this.calculateOrderAmount();
							}
						}).blur(function(event) {
							_this.calculateOrderAmount();
						});
						$('#validate_integral').click(function(event) {
							_this.calculateOrderAmount();
						});
						
						$('#user_rank').val(data.user_rank).trigger('change');
						$('#user_id').val(data.user_id);
						$('input[name="already_member"][value="1"]').trigger('click');
						if(data.rank_name)
						{
							$('#is_vip').html(data.rank_name);
						} else {
							$('#is_vip').html('&nbsp;');
						}

						if($('.order_modes a.active').data('mode') == 'sales-agent') {
							$('.sales_agent_only').show();
							$('.salesagent_hide').hide();
						}
					}
					else
					{
						$('input[name=invoice_no]').val('')
						$('#user_surplus').html('<span style="color:red">' + data.error + '</span>');
						$('#user_rank').val(0).trigger('change');
						$('#user_id').val(0);
						$('input[name="member_user_name"]').val('');
						$('#is_vip').html('&nbsp;');
						
					}
				}
				
				// Set phone number and email if available
				if (data.status == 1)
				{
					if (data.user_name)
					{
						var matches = data.user_name.match(/^\+([0-9]+)-([0-9]+)$/);
						if (matches)
						{
							var ccc = matches[1];
							var mobile = matches[2];
							$('input[name="mobile"]').intlTelInput('selectCountry', YOHO.pos.cccToCountryCode(ccc));
							$('input[name="mobile"]').val(mobile).trigger('keyup');
							$('input[name="member_user_name"]').intlTelInput('selectCountry', YOHO.pos.cccToCountryCode(ccc));
							$('input[name="member_user_name"]').val(mobile).trigger('keyup');
						}
						else
						{
							$('input[name="mobile"]').val(data.user_name).trigger('keyup');
							$('input[name="member_user_name"]').val(data.user_name).trigger('keyup');
						}
					}
					if (data.email) $('input[name="email"]').val(data.email).trigger('keyup');
				}
			},
			error: function(xhr) {
				YOHO.dialog.warn("伺服器繁忙，請稍後再試。("+xhr.status+")");
				console.log('error: ' + JSON.stringify(xhr));
			}
		});
	},

	userRankChange: function(){
		var _this = this;
		$('#user_rank').change(function(event) {
			_this.addToCart(0, 0);
		});

	},
	
	calculateDiscount: function() {
		var _this = this;
		$('input[name="discount"]').click(function(event) {
			if ($(event.target).val() == 1)
			{
				$('#discount_span').show();
			}
			else
			{
				$('#discount_span').hide();
				// Reset to no discount
				$('input[name="vardiscount"]').val('1');
				// Revalidate discount
				_this.calculateOrderAmount();
			}
		});
		
		$('input[name="vardiscount"]').keypress(function(event) {
			if (event.which == 13)
			{
				event.preventDefault();
				
				_this.calculateOrderAmount();
			}
		}).blur(function(event) {
			_this.calculateOrderAmount();
		});
		$('#validate_discount').click(function(event) {
			_this.calculateOrderAmount();
		});
	},
	
	verifyCoupon: function() {
		var _this = this;
		$('input[name="coupon"]').click(function(event) {
			if ($(event.target).val() == 1)
			{
				$('#coupon_span').show();
			}
			else
			{
				$('#coupon_span').hide();
				// Reset to no coupon
				$('input[name="coupon_code"]').val('');
				// Revalidate discount
				_this.calculateOrderAmount();
			}
		});
		
		$('input[name="coupon_code"]').keypress(function(event) {
			if (event.which == 13)
			{
				event.preventDefault();
				
				$(this).trigger('blur');
			}
		}).blur(function(event) {
			var coupon_codes = $(this).val().toUpperCase().split(',');
			// Remove whitespaces
			coupon_codes = $.map(coupon_codes, function(code) {
				return $.trim(code);
			});
			// Remove empty strings and duplicates
			coupon_codes = $.grep(coupon_codes, function(code, i) {
				return (code != '') && ($.inArray(code, coupon_codes) === i);
			});
			
			$(this).val(coupon_codes.join(','));
			
			_this.calculateOrderAmount();
		});
		$('#validate_coupon').click(function(event) {
			$('input[name="coupon_code"]').trigger('blur');
		});
	},
	
	handleReserve: function () {
		var _this = this;
		$('input[name="reserve"]').click(function(event) {
			if ($(event.target).val() == 1)
			{
				$('input[name="deposit"]').closest('tr').show();
			}
			else
			{
				$('input[name="deposit"]').closest('tr').hide();
				YOHO.pos.data.force_reserve_mode = false;
			}
		});
	},
	
	handleDeposit: function () {
		var _this = this;
		$('input[name="deposit"]').click(function(event) {
			if ($(event.target).val() == 1)
			{
				$('#deposit_span').show();
			}
			else
			{
				$('#deposit_span').hide();
				// Reset to no deposit
				$('input[name="deposit_amount"]').val('0');
				// Calculate amount
				_this.calculateOrderAmount();
			}
		});
		
		$('input[name="deposit_amount"]').keypress(function(event) {
			if (event.which == 13)
			{
				event.preventDefault();
				
				_this.calculateOrderAmount();
			}
		}).blur(function(event) {
			_this.calculateOrderAmount();
		});
		$('#validate_deposit').click(function(event) {
			_this.calculateOrderAmount();
		});
	},
	
	calculateOrderAmount: function () {
		var _this = this;
		var user_name = '';
		if ($('input[name="member_user_name"]').val())
		{
			user_name = '+' + $('input[name="member_user_name"]').intlTelInput('getSelectedCountryData').dialCode + '-' + $('input[name="member_user_name"]').val();
		}
		var mode = $('.order_modes a.active').data('mode');
		if (mode == 'retail'){
			mode = $('.order_modes #retail_menu a.active').data('mode');
		}
		$.ajax({
			url: '/pos.php',
			type: 'post',
			cache: false,
			data: {
				'act':'calculate_amount',
				'user_name':user_name,
				'integral':$('input[name="integral"]').val(),
				'discount':$('input[name="vardiscount"]').val(),
				'coupon':$('input[name="coupon_code"]').val(),
				'deposit':$('input[name="deposit_amount"]').val(),
				'shipping':$('input[name="shipping"]:checked').val(),
				'payment':$('input[name="payment"]:checked').val(),
				'country':$('#country').val(),
				'order_mode':mode
			},
			dataType: 'json',
			success: function(data) {
				if (data.status == 1)
				{
					$('#order_total').html(data.order_total);
					_this.calculateCashChange();
					var codeList = [];
					$.each(data.promote_list, function (promote_code, promote_id) {
						codeList.push(promote_code);
					})
					$(".promotion_only").get().map(function(v) {
						if (codeList.indexOf($(v).data("code")) === -1) {
							$(v).remove();
						}
					})
					$.each(data.promote_list, function (promote_code, promote_id) {
						if (promote_id == 1 && $(".promotion_only[data-code=" + promote_code + "]").length == 0) {
							$html = $("<tr class='promotion_only' data-code='" + promote_code + "'>" +
											"<th id='theClubIdTitle'>The Club 會員帳號</th>" +
											"<td><input class='inputtext promote_input' name='theClubId' type='text' maxlength='10' pattern='^8[0-9]{9}$' autocomplete='off' required>" +
											"<span id='theClubIdHint' style='color:red;padding-left: 20px;display:none'>The Club 會員帳號為8字開頭的10位數字</span></td>" +
										"</tr>");
							$(".cart_info .address_content_inner table").append($html);
						}
					});
				}
				else
				{
					YOHO.dialog.warn(data.error);
					if (data.hasOwnProperty('pay_points'))
					{
						$('input[name="integral"]').val(data.pay_points);
					}
					else if (data.hasOwnProperty('flow_points'))
					{
						$('input[name="integral"]').val(data.flow_points);
					}
					if (data.hasOwnProperty('vardiscount'))
					{
						$('input[name="vardiscount"]').val(data.vardiscount);
					}
					if (data.hasOwnProperty('coupon_code'))
					{
						$('input[name="coupon_code"]').val(data.coupon_code);
					}
				}
			},
			error: function(xhr) {
				YOHO.dialog.warn("伺服器繁忙，請稍後再試。("+xhr.status+")");
				console.log('error: ' + JSON.stringify(xhr));
			}
		});
	},
	
	checkMobilePhoneNumber: function() {
		var $mobile = $('input[name="mobile"]');
		$mobile.keyup(function (event) {
			var ccc = $mobile.intlTelInput('getSelectedCountryData').dialCode;
			var mobile = $mobile.val();
			if ((mobile != '') && (!YOHO.check.isMobile(mobile, ccc)))
			{
				$('#mobile_hint').html('手提電話號碼不正確');
				$('#is_vip').html('&nbsp;');
			}
			else
			{
				$('#mobile_hint').html('&nbsp;');
			}
			if ((mobile == '11111111') || (mobile == '88888888') || (mobile == '99999999'))
			{
				$('input[name="consignee"]').closest('tr').show();
			}
			else
			{
				$('input[name="consignee"]').closest('tr').hide();
			}
		});
		// Also monitor change from intlTelInput
		$mobile.change(function () {
			$mobile.trigger('keyup');
		});
	},
	
	checkEmailAddress: function() {
		var _this = this;
		var $email = $('input[name="email"]');
		$email.keyup(function (event) {
			var email = $email.val() || '';
			if (email != '')
			{
				var typo = _this.containsCommonEmailTypos(email);
				if (typo)
				{
					$('#email_hint').html('<span style="font-size:18px;">偵測到常見錯誤“' + typo + '”<span>');
				}
				else if (!YOHO.check.isEmail(email))
				{
					$('#email_hint').html('電郵地址不正確');
				}
				else
				{
					$('#email_hint').html('&nbsp;');
				}
			}
			else
			{
				$('#email_hint').html('&nbsp;');
			}
		});
	},
	
	containsCommonEmailTypos: function(email) {
		var commonTypos = ['gamil.', 'gnail.', 'ganil.', 'gmaiil.', 'gmail.com.hk', 'yhaoo.', 'yaho.', 'yhao.', 'yayoo.', 'yshoo.', 'yaoo.', 'homtail.', 'hotnail.', 'hotamil.', '.con', '.co.hk', '.om', ',com', 'natvigator.'];
		for (var i = 0; i < commonTypos.length; i++)
		{
			if (email.indexOf(commonTypos[i]) > -1)
			{
				return commonTypos[i];
			}
		}
		var foundTypo = '';
		var regexTypos = {'gmail.co$':'gmail.co', 'hotmail.co$':'hotmail.co'};
		$.each(regexTypos, function(regex, typo) {
			if (email.match(new RegExp(regex)))
			{
				foundTypo = typo;
				return false; // breaks out of $.each
			}
		});
		if (foundTypo != '')
		{
			return foundTypo;
		}
		return false;
	},
	
	assignSalesperson: function() {
		var $salesperson = $('select[name="salesperson"]');
		
		var previousSalesperson = YOHO.common.cookie('POS[salesperson]');
		if (previousSalesperson)
		{
			$salesperson.find('option').each(function () {
				var option = $(this);
				if (option.val() == previousSalesperson)
				{
					$salesperson.val(previousSalesperson);
				}
			});
		}
		$salesperson.change(function (event) {
			YOHO.common.cookie('POS[salesperson]', $salesperson.val());
		});
	},
	
	loadUserInput: function() {
		var _this = this;
		var $select = $('select#user_input_select');
		
		var refresh_user_input_list = function () {
			$.ajax({
				url: '/pos.php',
				type: 'post',
				cache: false,
				data: {
					'act':'load_user_input'
				},
				dataType: 'json',
				success: function(data) {
					if (data.status == 1)
					{
						$select.empty();
						$select.append($('<option selected="selected">請選擇</option>'));
						$.each(data.input_list, function (idx, item) {
							var $opt = $('<option></option>');
							$opt.data('rec_id',item.rec_id);
							$opt.data('ccc',item.ccc);
							$opt.data('mobile',item.mobile);
							$opt.data('email',item.email);
							$opt.data('user_id',item.user_id);
							$opt.text('手提：' + item.mobile + ' 電郵：' + item.email);
							$select.append($opt);
						});
						if ($select.find('option').length == 1)
						{
							$select.empty();
							$select.append($('<option>暫沒資料</option>'));
						}
					}
					else
					{
						YOHO.dialog.warn(data.error);
					}
				},
				error: function(xhr) {
					YOHO.dialog.warn("伺服器繁忙，請稍後再試。("+xhr.status+")");
					console.log('error: ' + JSON.stringify(xhr));
				}
			});
		};
		
		$('#refresh_user_input').click(function () {
			$select.empty();
			$select.append($('<option>載入中...</option>'));
			refresh_user_input_list();
		});
		
		$select.change(function () {
			var $opt = $select.find(':selected');
			
			var rec_id = $opt.data('rec_id');
			var ccc = $opt.data('ccc');
			var mobile = $opt.data('mobile');
			var email = $opt.data('email');
			var user_id = $opt.data('user_id');
			
			if (rec_id)
			{
				if (ccc) $('input[name="mobile"]').intlTelInput('selectCountry', YOHO.pos.cccToCountryCode(ccc));
				if (mobile) $('input[name="mobile"]').val(mobile).trigger('keyup');
				if (email) $('input[name="email"]').val(email).trigger('keyup');
				
				if (user_id)
				{
					$('input[name="already_member"][value="1"]').trigger('click');
					// $('input[name="member_user_name"]').intlTelInput('selectCountry', YOHO.pos.cccToCountryCode(ccc));
					// $('input[name="member_user_name"]').val(mobile);
					var user_name = ccc ? '+' + ccc + '-' + mobile : mobile;
					_this.validateAccount(user_name);
				}
				else
				{
					$('input[name="already_member"][value="0"]').trigger('click');
				}
			}
		});
		
		// Load the list once on page load
		refresh_user_input_list();
	},
	
	changeCash: function() {
		var _this = this;
		$('#cash_received').keyup(function(event) {
			_this.calculateCashChange();
			if (event.which == 13)
			{
				event.preventDefault();
			}
		});
	},
	
	calculateCashChange: function () {
		if ($('#cash_received').val() == '')
		{
			$('#cash_change').val('');
			return;
		}
		var amount = parseFloat($('#total_amount').text()) || 0;
		var received = parseFloat($('#cash_received').val()) || 0;
		var change = (received - amount).toFixed(2);
		$('#cash_change').val(change);
		$('#cash_change').css('font-weight','bold');
		$('#cash_change').css('color', (change == 0) ? 'green' : (change > 0) ? 'black' : 'red');
	},
	
	confirmOrder: function() {
		var _this = this;
		$('.confirm_order_btn').click(function(event) {
			if ((!$.isArray(YOHO.pos.data.goods_list)) || (YOHO.pos.data.goods_list.length === 0))
			{
				YOHO.dialog.warn('您的訂單中沒有任何產品！');
				return;
			}
			
			var option_reserve = $('input[name="reserve"]:checked').val();
			var order_mode = $('.order_modes a.active').data('mode');
			//if (order_mode == 'reserve')
			if (option_reserve == 1)
			{
				if (!YOHO.pos.data.have_preorder_goods && !YOHO.pos.data.force_reserve_mode)
				{
					var dialog = YOHO.dialog.creat({
						id:'confirm',
						content:'<div class="warn-tip"><i class="iconfont">&#227;</i>你選擇了訂貨模式，但訂單中沒有需要訂貨的產品。</div>'
					});
					dialog.button({
						name: '改為「正常銷售」',
						focus: true,
						callback: function () {
							$("#reserve_0").prop("checked", true);
							//$('.order_modes a[data-mode="normal"]').trigger('click');
							setTimeout(function () {
								$('.confirm_order_btn').trigger('click');
							}, 100);
						}
					},{
						name: '我確定這是「訂貨」單',
						callback: function () {
							YOHO.pos.data.force_reserve_mode = true;
							setTimeout(function () {
								$('.confirm_order_btn').trigger('click');
							}, 100);
						}
					});
					
					return;
				}
			}
			if($('input[name="need_removal"]').length > 0 && !$('input[name="need_removal"]:checked').val() && $('.need_removal').is(':visible') )
			{
				YOHO.dialog.warn('請查詢客人是否需要WEEE回收服務！');
				return;
			}
			if($('input[name="need_removal"]').length > 0 && $('input[name="need_removal"]:checked').val() == 1)
			{
				if(!$('input[name=nr_consignee]').val() || !$('input[name=nr_address]').val() || !$('input[name=nr_phone]').val()) {
					YOHO.dialog.warn('請填寫WEEE回收資料！');
					return;
				}
			}
			// Transformer check
			if($('input[name="need_requiregoods"]').length > 0 && !$('input[name="need_requiregoods"]:checked').val() && $('.need_requiregoods').is(':visible') )
			{
				YOHO.dialog.warn('請查詢客人是否需要變壓器');
				return;
			}
			// Prmotion only
			var promote_error = "";
			if ($('.promotion_only').length > 0) {
				$('.promotion_only').get().map(function(promo) {
					$(promo).find("input").get().map(function(input) {
						var name = $(input).prop("name");
						if ($(input).prop("required") && $(input).val() == "") {
							promote_error = '請填寫' + $("#" + name + "Title").text();
						} else if ($(input).prop("pattern") != "") {
							var regex = new RegExp($(input).prop("pattern"));
							if (!regex.test($(input).val()))  {
								promote_error = $("#" + name + "Hint").text();
							}
						}
					})
				})
			}
			if (promote_error != "") {
				YOHO.dialog.warn(promote_error);
				return;
			}
			//wholesale check
			if(order_mode == 'wholesale' && $('input[name=invoice_no]').val()==''){
				YOHO.dialog.warn('請輸入批發單Invoice Number');
				return;
			}
			
			// check sales agent
			if(order_mode =='sales-agent' &&( $('input[name="sa_sn"]').val() =='' ||  $('#sa_select').val() == 0) ){
				YOHO.dialog.warn('請選擇代理及填寫代理訂單號碼！');
				return;
			}

			var goods_list_str = '';
			$.each(YOHO.pos.data.goods_list, function (idx, goods) {
				goods_list_str += goods.goods_name + ' x' + goods.goods_number + '<br>';
			});
			
			var mobile_with_prefix = '';
			if ($('input[name="mobile"]').val())
			{
				mobile_with_prefix = '+' + $('input[name="mobile"]').intlTelInput('getSelectedCountryData').dialCode + '-' + $('input[name="mobile"]').val();
			}
			
			function getNextNode($e)
			{
				var contents = $e.parent().contents();
				return $(contents.get(contents.index($e)+1));
			}
			
			var report = '' +
				(order_mode != 'normal' ? '<div style="color: red; font-weight: bold; text-align: center;">' + $.trim($('.order_modes a.active').text()) + '</div>' : '') +
				'出單列表：<br><div style="max-height: 150px; overflow-y: scroll;">' + 
				goods_list_str +
				'</div><hr>' +
				'產品序號：<br><div style="max-height: 150px; overflow-y: scroll;">' + 
				$('textarea[name="serial_numbers"]').val().replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1<br>$2') +
				'</div><hr>' +
				'備註：<br><div style="max-height: 150px; overflow-y: scroll;">' + 
				$('textarea[name="postscript"]').val().replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1<br>$2') +
				'</div><hr>' +
				'訂貨備註：<br><div style="max-height: 150px; overflow-y: scroll;">' + 
				$('textarea[name="order_remarks"]').val().replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1<br>$2') +
				'</div><hr>' +
				'出貨方式：' + getNextNode($('input[name="shipping"]:checked')).text() + '<br>' +
				'付款方式：' + getNextNode($('input[name="payment"]:checked')).text() + '<br>' +
				'會員：' + getNextNode($('input[name="already_member"]:checked')).text() + ($('input[name="already_member"]:checked').val() == 1 ? $('input[name="member_user_name"]').val() : '') + '<br>' +
				($('input[name="integral"]').val() > 0 ? '使用積分：' + $('input[name="integral"]').val() + '<br>' : '') +
				'折扣：' + ($('input[name="discount"]:checked').val() == 1 ? $('input[name="vardiscount"]').val() : getNextNode($('input[name="discount"]:checked')).text()) + '<br>' +
				($('input[name="coupon"]:checked').val() > 0 ? '優惠券：' + $('input[name="coupon_code"]').val() + '<br>' : '') +
				'<hr>' +
				($('input[name="consignee"]').is(':visible') ? '客戶名稱：' + $('input[name="consignee"]').val() + '<br>' : '') +
				'手提電話號碼：' + mobile_with_prefix + '<br>' +
				'Email：' + $('input[name="email"]').val() + '<br>' +
				'<hr>' +
				'銷售人員：' + $('select[name="salesperson"]').val() + '<br>';
			YOHO.dialog.confirm('確定所有資料正確無誤？<br><br>' + report, _this.submitOrder);
		});
	},
	
	submitOrder: function() {
		var $form = $('<form action="/quick_buy.php?step=done" method="post"></form>');
		
		var fieldlist = [];
		
		fieldlist.push({'name': 'step', 'value': 'done' });
		
		var mode = $('.order_modes a.active').data('mode');
		if (mode == 'retail'){
			mode = $('.order_modes #retail_menu a.active').data('mode');
		}

		var option_reserve = $('input[name="reserve"]:checked').val();
		if (mode == 'normal' && option_reserve == 1){
			mode = 'reserve';
		}

		fieldlist.push({'name': 'mode', 'value': mode });
		
		if (mode == 'replace')
		{
			fieldlist.push({'name': 'replace_order_id', 'value': $('#replace_order_goods_table').data('orderId') });
			
			$('#replace_order_goods_table .replace_qty').each(function () {
				var $input = $(this);
				var rec_id = $input.data('recId');
				var qty = parseInt($input.val()) || 0;
				var warehousing = $('input[name=replace_warehousing_' + rec_id+']:checked').val();
				
				if (qty > 0)
				{
					fieldlist.push({'name': 'replace[' + rec_id + ']', 'value': warehousing});
					fieldlist.push({'name': 'replace_qty[' + rec_id + ']', 'value': qty});
				}
			});
		}
		if (mode == 'sales-agent')
		{
			$.each(YOHO.pos.data.goods_list, function (idx, goods) {
		
				var goods_rate = $('select[name="goods_rates_'+goods.goods_id+'"]').val();
				var sa_id = $('#sa_select').val();
				var sa_sn = $('input[name="sa_sn"]').val();

				fieldlist.push({'name': 'sa_id',        'value': sa_id });
				fieldlist.push({'name': 'sa_sn',        'value': sa_sn });
				fieldlist.push({'name': 'goods_number[' + goods.goods_id + ']', 'value': goods.goods_number});
				fieldlist.push({'name': 'goods_rate[' + goods.goods_id + ']', 'value': goods_rate});
			});
		} else {
		$.each(YOHO.pos.data.goods_list, function (idx, goods) {
			fieldlist.push({'name': 'goods_number[' + goods.goods_id + ']', 'value': goods.goods_number});
		});
		}
		if (mode == 'ex_normal')
		{
			fieldlist.push({'name': 'is_pay', 'value': $('input[name="is_pay"]').val()});
		}
		
		$.each(YOHO.pos.data.goods_replace_price, function (id, price) {
			fieldlist.push({'name': 'custom_price['+id+']', 'value': price});
		});
		
		var mobile_with_prefix = '';
		if ($('input[name="mobile"]').val())
		{
			mobile_with_prefix = '+' + $('input[name="mobile"]').intlTelInput('getSelectedCountryData').dialCode + '-' + $('input[name="mobile"]').val();
		}

		var user_with_prefix = '';
		if ($('input[name="member_user_name"]').val())
		{
			user_with_prefix = '+' + $('input[name="member_user_name"]').intlTelInput('getSelectedCountryData').dialCode + '-' + $('input[name="member_user_name"]').val();
		}
		
		if ($('input[name="consignee_mobile"]').val())
		{
			consignee_mobile_with_prefix = '+' + $('input[name="consignee_mobile"]').intlTelInput('getSelectedCountryData').dialCode + '-' + $('input[name="consignee_mobile"]').val();
		} else {
			consignee_mobile_with_prefix = mobile_with_prefix;
		}

		if ($('input[name="need_removal"]').length > 0 && $('input[name="need_requiregoods"]:checked').val() == 1) {
			var postscript = $("textarea[name=postscript]").val();
			if (postscript.indexOf("不需要變壓器") == -1) {
				$("textarea[name=postscript]").val(postscript + (postscript.length > 0 ? " " : "") + "不需要變壓器")
			}
		}
		if (mode == 'normal')
		{
			var sa_id = $('#sa_select').val();
			var sa_sn = $('input[name="sa_sn"]').val();
			fieldlist.push({'name': 'sa_id',        'value': sa_id });
			fieldlist.push({'name': 'sa_sn',        'value': sa_sn });
		}
		fieldlist.push({'name': 'serial_numbers', 'value': $('textarea[name="serial_numbers"]').val() });
		fieldlist.push({'name': 'postscript',     'value': $('textarea[name="postscript"]').val() });
		fieldlist.push({'name': 'order_remarks',  'value': $('textarea[name="order_remarks"]').val() });
		fieldlist.push({'name': 'country',        'value': $('select[id="country"]').val() });
		fieldlist.push({'name': 'province',       'value': 0 });
		fieldlist.push({'name': 'city',           'value': 0 });
		fieldlist.push({'name': 'district',       'value': 0 });
		fieldlist.push({'name': 'shipping',       'value': $('input[name="shipping"]:checked').val() });
		fieldlist.push({'name': 'payment',        'value': $('input[name="payment"]:checked').val() });
		fieldlist.push({'name': 'user_id',        'value': $('input[name="member_user_name"]').val() });
		fieldlist.push({'name': 'user_name',      'value': user_with_prefix });
		fieldlist.push({'name': 'integral',       'value': $('input[name="integral"]').val() });
		fieldlist.push({'name': 'vardiscount',    'value': $('input[name="vardiscount"]').val() });
		fieldlist.push({'name': 'coupon',         'value': $('input[name="coupon_code"]').val() });
		fieldlist.push({'name': 'deposit',        'value': $('input[name="deposit_amount"]').val() });
		if ($('input[name="address"]').val())
		{
			fieldlist.push({'name': 'address',        'value': $('input[name="address"]').val() });
		} else {
		fieldlist.push({'name': 'address',        'value': 'NULL' });
		}
		if ($('input[name="zipcode"]').val())
		{
			fieldlist.push({'name': 'zipcode',        'value': $('input[name="zipcode"]').val() });
		} else {
		fieldlist.push({'name': 'zipcode',        'value': 'NULL' });
		}
		if ($('input[name="consignee_name"]').val() && (	$('input[name="shipping"]:checked').val() != 2 && $('input[name="shipping"]:checked').val() != 4 && $('input[name="shipping"]:checked').val() != 5 ))
		{
			fieldlist.push({'name': 'consignee',      'value': $('input[name="consignee_name"]:visible').val() });
		} else {
		fieldlist.push({'name': 'consignee',      'value': $('input[name="consignee"]:visible').val() });
		}
		fieldlist.push({'name': 'mobile',         'value': mobile_with_prefix });
		fieldlist.push({'name': 'email',          'value': $('input[name="email"]').val() });
		fieldlist.push({'name': 'salesperson',    'value': $('select[name="salesperson"]').val() });
		fieldlist.push({'name': 'tel',            'value': consignee_mobile_with_prefix });
		fieldlist.push({'name': 'is_wholesale',    'value': $('input[name="is_wholesale"]:checked').val()==1?1:0 });
		fieldlist.push({'name': 'type_sn',    'value': $('input[name="invoice_no"]').val() });
		fieldlist.push({'name': 'ws_customer_id',    'value': $('input[name="ws_customer_id"]').val() });
		if($('input[name="need_removal"]').length > 0 && $('input[name="need_removal"]:checked').val() == 1)
		{
			fieldlist.push({'name': 'need_removal',    'value': $('input[name="need_removal"]').val() });
			fieldlist.push({'name': 'nr_consignee',    'value': $('input[name="nr_consignee"]').val() });
			fieldlist.push({'name': 'nr_address',      'value': $('input[name="nr_address"]').val() });
			fieldlist.push({'name': 'nr_phone',        'value': $('input[name="nr_phone"]').val() });
			sel = $('input[name^="nr_cat"]');
			sel.each(function() {
				fieldlist.push({'name': $(this).attr('name'),        'value': $(this).val() });
			});
		}
		if ($('.promotion_only').length > 0) {
			$('.promotion_only').get().map(function(promo) {
				$(promo).find("input").get().map(function(input) {
					var name = $(input).prop("name");
					var val = $(input).val();
					fieldlist.push({'name': name, 'value': val });
				})
			})
		}
		$.each(fieldlist, function (idx, field) {
			var $input = $('<input type="hidden">');
			$input.prop('name', field.name);
			$input.val(field.value);
			$form.append($input);
		});
		
		$form.appendTo('body');
		$form.submit();
	},
	
	repairHaveOrder: function() {
		$('input[name="have_order"]').click(function() {
			if ($(this).val() == 1)
			{
				$('#repair_by_order').show();
				$('#repair_by_goods').hide();
			}
			else
			{
				$('#repair_by_order').hide();
				$('#repair_by_goods').show();
			}
		});
	},
	
	lookupRepairOrder: function() {
		var _this = this;
		$('input[name="repair_order_sn"]').keypress(function(event) {
			// ASCII cod 13 = Enter
			if (event.which == 13)
			{
				event.preventDefault();
				
				var order_sn = $(this).val();
				_this.loadExistingOrder(order_sn);
			}
		});
	},
	
	loadRepairOrder: function(data) {
		if (data.status == 1)
		{
			if ((data.order.shipping_status != 1) && // SS_SHIPPED
				(data.order.shipping_status != 2) && // SS_RECEIVED
				(data.order.shipping_status != 4))   // SS_SHIPPED_PART
			{
				$('#repair_order_goods_table').removeData('orderId');
				$('#repair_order_goods_table').removeData('orderSn');
				$('#repair_order_summary').hide();
				$('#repair_order_goods_table').hide();
				$('#repair_order_goods_table tbody').empty();
				
				YOHO.dialog.warn('此訂單並未出貨');
				
				return;
			}
			
			// Save order_id as the table's data
			$('#repair_order_goods_table').data('orderId', data.order_id);
			$('#repair_order_goods_table').data('orderSn', data.order.order_sn);
			
			if (data.order)
			{
				$('#repair_order_summary').html(this.orderSummaryTable(data.order));
				$('#repair_order_summary').show();
			}
			else
			{
				$('#repair_order_summary').hide();
			}
			
			// Populate the order goods list
			if (data.order_goods.length > 0)
			{
				$('#repair_order_goods_table').show();
				this.populateRepairGoodsTable(data.order_goods);
			}
			else
			{
				$('#repair_order_goods_table').hide();
				$('#repair_order_goods_table tbody').empty();
			}
			
			// If old order have consignee name, use it as contact name
			if (data.order.consignee)
			{
				$('input[name="contact_name"]').val(data.order.consignee);
			}
			
			// If old order have assoicated user, populate it too
			if (data.user_name)
			{
				this.validateAccount(data.user_name);
			}
		}
		else
		{
			$('#repair_order_goods_table').removeData('orderId');
			$('#repair_order_goods_table').removeData('orderSn');
			$('#repair_order_summary').hide();
			$('#repair_order_goods_table').hide();
			$('#repair_order_goods_table tbody').empty();
			
			YOHO.dialog.warn(data.error);
		}
	},
	
	lookupRepairGoods: function() {
		var _this = this;
		$('input[name="repair_goods_sn"]').keypress(function(event) {
			// ASCII cod 13 = Enter
			if (event.which == 13)
			{
				event.preventDefault();
				
				var barcode = $(this).val();
				_this.searchGoodsBySN(barcode);
			}
		});
	},
	
	loadRepairGoods: function(data) {
		// If search by goods, remove cached order ID
		$('#repair_order_goods_table').removeData('orderId');
		$('#repair_order_goods_table').removeData('orderSn');
		// and hide order summary
		$('#repair_order_summary').hide();
		
		if (data.status == 1)
		{
			if (data.goods_list.length > 0)
			{
				$('#repair_order_goods_table').show();
				this.populateRepairGoodsTable(data.goods_list);
			}
			else
			{
				$('#repair_order_goods_table').hide();
				$('#repair_order_goods_table tbody').empty();
			}
		}
		else
		{
			$('#repair_order_goods_table').hide();
			$('#repair_order_goods_table tbody').empty();
			
			YOHO.dialog.warn(data.error);
		}
	},
	
	populateRepairGoodsTable: function (goods_list) {
		// Clear the table body
		$('#repair_order_goods_table tbody').empty();
		
		// Appead rows to the table
		$.each(goods_list, function (idx, goods) {
			$('#repair_order_goods_table tbody').append('<tr data-goods-id="' + goods.goods_id + '">' +
				'<td style="text-align: left;">' +
					'<a href="goods.php?id=' + goods.goods_id + '" class="goods_link" target="_blank">' +
						goods.goods_name +
					'</a>' +
				'</td>' +
				'<td style="text-align: center;">' +
					'<a href="/x/goods.php?act=list&amp;keyword=' + goods.goods_sn + '" target="_blank">' +
						goods.goods_sn +
					'</a>' +
				'</td>' +
				'<td><input type="radio" name="repair_goods_id" value="' + goods.goods_id + '" class="repair_radio" data-goods-id="' + goods.goods_id + '"></td>'+
				'</tr>');
		});
		
		this.bindGoodsLinks();
	},
	
	handleRepairFee: function() {
		$('input[name="repair_fee"]').click(function(event) {
			if ($(event.target).val() == 1)
			{
				$('#custom_repair_fee').show();
			}
			else
			{
				$('#custom_repair_fee').hide();
			}
		});
	},

	handleFavourable: function(){
		var _this = this;
		var user_rank = $('#user_rank').val();
		var user_id = $('#user_id').val();
		var order_mode = $('.order_modes a.active').data('mode');
		if (order_mode == "retail"){
			order_mode = $('.order_modes #retail_menu a.active').data('mode');
		}
		var option_reserve = $('input[name="reserve"]:checked').val();
		var payment_type = $('input[name="payment"]:checked').val();
		if (payment_type == 99){
			var is_redeem = 1;
		} else {
			var is_redeem = 0;
		}
		if((order_mode == "normal" && payment_type != 99) || option_reserve == 1){
			$.ajax({
				url: '/pos.php',
				type: 'post',
				cache: false,
				data: {
					'act':'load_favourable',
					'user_rank':user_rank,
					'user_id':user_id,
					'order_mode':order_mode,
					'is_redeem':is_redeem
				},
				dataType: 'json',
				success: function(data) {
					if (data.status == 1)
					{
						// Clear Favourable table
						$('#favourable_table').find('tbody').empty();
						// Populate goods info
						$.each(data.favourable_list, function (idx, favourable) {
	
							if(!favourable.available) return;
							$('#favourable_table').find('tbody').append($(
								'<tr>'+'<td colspan="6">'+favourable.act_name+'</td>'+'</tr>'+
								'<tr style="height: 50px;"><th><h4>產品名稱</h4></th><th width="100"><h4>條碼</h4></th><th width="70"><h4>優惠及存貨</h4></th><th width="100"><h4>價格</h4></th><th width=""><h4></h4></th></tr>'
							));
							$.each(favourable.gift, function (idx, goods) {
								var goods_id = goods.goods_id;
								var in_cart = false;
								if($.inArray(parseInt(goods_id), favourable.in_cart_goods) !== -1) { // In cart
									in_cart = true;
								} else {
									in_cart = false;
								}
								$('#favourable_table').find('tbody').append($('<tr data-goods-id="' + goods_id + '" data-act-id="'+favourable.act_id+'" >'+
								'<td style="text-align: left;">'+
									'<img src="'+goods.goods_img+'" width="65" height="65">'+
									'<a href="goods.php?id=' + goods_id + '" target="_blank" class="goods_link">'+
										goods.goods_name+
									'</a>'+
								'</td>'+
								'<td>'+
									'<a href="x/goods.php?act=list&amp;keyword=' + goods.goods_sn + '" target="_blank">' +
										goods.goods_sn+
									'</a>'+
								'</td>'+
								'<td>'+goods.goods_number+'</td>'+
								'<td>' + (goods.price >0 ? goods.formated_price : "<span style='color: #cc0000;'>免費</span>") + '</td>'+
								'<td>'+
									'<input type="hidden" size="5" disabled="disabled" value="1" class="fa_addtocart_input">'+
									((in_cart) ? '': '<a href="javascript:void(0)" class="fa_addtocart_btn"><i class="iconfont">ŭ</i></a>')+
								'</td>'+
								'</tr>'));
							});
						});
						if($('#favourable_table').find('tbody').find('tr').length > 0)$('.FavourableList').show();
						else $('.FavourableList').hide();
						_this.bindGoodsLinks();
	
						// Add to cart when user press ENTER (ascii code = 13) or TAB (trigger blur)
						$('.fa_addtocart_input').keypress(function(event) {
							if (event.which == 13)
							{
								event.preventDefault();
								$(this).blur(); // trigger the blur event
							}
						}).blur(function(event) {
							var goods_id = $(this).closest('tr').data('goodsId');
							var qty = $(this).val(); // 1
							var act_id = $(this).closest('tr').data('actId');
							_this.addFavourable(goods_id, qty, act_id);
						});
	
						// Alternatively user can click the add to cart button
						$('.fa_addtocart_btn').click(function(event) {
							var $goods_input = $(this).closest('tr').find('.fa_addtocart_input');
							$goods_input.blur(); // trigger the blur event binded above
						});
	
						// Need update cart?
						if (data.need_update == true){
							_this.addToCart(0, 0, 0, 2, 0);
						}
					}
					else
					{
						YOHO.dialog.warn(data.error);
					}
				},
				error: function(xhr) {
					YOHO.dialog.warn("伺服器繁忙，請稍後再試。("+xhr.status+")");
					console.log('error: ' + JSON.stringify(xhr));
				}
			});
		} else {
			$('.FavourableList').hide();
		}
	},
	
	addFavourable: function(goods_id, qty, act_id){
		var _this = this;
		var user_rank = $('#user_rank').val();
		var user_id = $('#user_id').val();
		var mode = $('.order_modes a.active').data('mode');
		if (mode == 'retail'){
			mode = $('.order_modes #retail_menu a.active').data('mode');
		}
		
		$.ajax({
			url: '/pos.php',
			type: 'post',
			cache: false,
			data: {
				'act':'add_favourable',
				'goods_id':goods_id,
				'quantity':qty,
				'shipping':$('input[name="shipping"]:checked').val(),
				'payment':$('input[name="payment"]:checked').val(),
				'country':$('#country').val(),
				'order_mode':mode,
				'user_rank':user_rank,
				'user_id':user_id,
				'act_id':act_id
			},
			dataType: 'json',
			success: function(data) {

				if (data.status == 1)
				{
					_this.addToCart(0, 0, 0, 2, 0);
				}
				else
				{
					YOHO.dialog.warn(data.error);
				}
			},
			error: function(xhr) {
				YOHO.dialog.warn("伺服器繁忙，請稍後再試。("+xhr.status+")");
				console.log('error: ' + JSON.stringify(xhr));
			}
		});
	},

	confirmRepair: function() {
		var _this = this;
		$('.confirm_order_btn').click(function(event) {
			if (!$('input[name="repair_goods_id"]:checked').val())
			{
				YOHO.dialog.warn('您沒有選擇要維修的產品！');
				return;
			}
			
			if (!$('input[name="mobile"]').val() && !$('input[name="email"]').val())
			{
				YOHO.dialog.warn('至少要填個聯絡電話吧！');
				return;
			}
			
			var mobile_with_prefix = '+' + $('input[name="mobile"]').intlTelInput('getSelectedCountryData').dialCode + '-' + $('input[name="mobile"]').val();
			
			function getNextNode($e)
			{
				var contents = $e.parent().contents();
				return $(contents.get(contents.index($e)+1));
			}
			
			var report = '' + 
				($('#repair_order_goods_table').data('orderId') ? '舊單編號：' + $('#repair_order_goods_table').data('orderSn') + '<br><hr>' : '') +
				'要維修的產品：<br>' + 
				$('input[name="repair_goods_id"]:checked').closest('tr').find('.goods_link').text() +
				'<hr>' +
				'維修費：' + ($('input[name="repair_fee"]:checked').val() == 1 ? $('input[name="repair_fee_amount"]').val() : getNextNode($('input[name="repair_fee"]:checked')).text()) + '<br>' +
				'<hr>' +
				'顧客姓名：' + $('input[name="contact_name"]').val() + '<br>' +
				'聯絡電話：' + mobile_with_prefix + '<br>' +
				'Email：' + $('input[name="email"]').val() + '<br>' +
				'<hr>' +
				'備註：<br><div style="max-height: 150px; overflow-y: scroll;">' + 
				$('textarea[name="remark"]').val().replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1<br>$2') +
				'</div><hr>' +
				'員工：' + $('select[name="salesperson"]').val() + '<br>';
			YOHO.dialog.confirm('確定所有資料正確無誤？<br><br>' + report, _this.submitRepair);
		});
	},
	
	submitRepair: function() {
		var $form = $('<form action="/pos_repair.php" method="post"></form>');
		
		var fieldlist = [];
		
		fieldlist.push({'name': 'act', 'value': 'confirm_repair' });
		
		var mobile_with_prefix = '+' + $('input[name="mobile"]').intlTelInput('getSelectedCountryData').dialCode + '-' + $('input[name="mobile"]').val();
		
		var repair_fee = $('input[name="repair_fee"]:checked').val() == '1' ? $('input[name="repair_fee_amount"]').val() : 0;
		
		fieldlist.push({'name': 'order_id',       'value': $('#repair_order_goods_table').data('orderId') });
		fieldlist.push({'name': 'goods_id',       'value': $('input[name="repair_goods_id"]:checked').val() });
		fieldlist.push({'name': 'repair_fee',     'value': repair_fee });
		fieldlist.push({'name': 'contact_name',   'value': $('input[name="contact_name"]').val() });
		fieldlist.push({'name': 'mobile',         'value': mobile_with_prefix });
		fieldlist.push({'name': 'email',          'value': $('input[name="email"]').val() });
		fieldlist.push({'name': 'remark',         'value': $('textarea[name="remark"]').val() });
		fieldlist.push({'name': 'operator',       'value': $('select[name="salesperson"]').val() });
		
		$.each(fieldlist, function (idx, field) {
			var $input = $('<input type="hidden">');
			$input.prop('name', field.name);
			$input.val(field.value);
			$form.append($input);
		});
		
		$form.appendTo('body');
		$form.submit();
	},

	//Stripe
	stripe: function(){
		
		// Stripe V3 js config
		var publishableKey = document.getElementById('publishableKey').value;
		var stripe = Stripe(publishableKey);
		// From button
		var btn        = document.getElementById('stripeBtn');
		// Processing model
		var model      = document.getElementById('processing-model');
		// Hidden input field
		var amount     = document.getElementById('amount');
		var currency   = document.getElementById('currency');
		var returnUrl = document.getElementById('return-url');
		var source = document.getElementById('source');
		// Error message div
		var displayError = $('#stripe-error');
		// Default vaild
		var numberComplete = false;
		var cvcComplete = false;
		var expiryComplete = false;
		var order_id = document.getElementById('order_id').value;
		var log_id = document.getElementById('log_id').value;
		var pay_code = document.getElementById('pay_code').value;
		// Default disable from button.
		disableBtn();

		// Setup stripe elements
		var elements = stripe.elements({locale:'en'});
		var style = {
			base: {
				'::placeholder': {
				color: '#ccc',
				},
			},
			invalid: {
				color: '#e5424d',
				':focus': {
				color: '#303238',
				},
			}
			};
		// Card number
		var cardNumber = elements.create('cardNumber',{placeholder:'‎4111 1111 1111 1111', style: style});
		cardNumber.mount('#card-number');
		// Card cvc
		var cardCvc = elements.create('cardCvc',{style: style});
		cardCvc.mount('#card-cvc');
		// Card expiry date
		var cardExpiry = elements.create('cardExpiry',{style: style});
		cardExpiry.mount('#card-expiry');
		// Event : cardNumber
		cardNumber.addEventListener('change', function(event) {
			if (event.error) {
				displayError.text(event.error.message);
				displayError.show();
			} else {
				displayError.text('');
				displayError.hide();
				//check type
				var type = event.brand.toLowerCase();
				type = type.replace(" ", "-");
				card_type = type;
				$('#card-image').removeClass().addClass(type);
			}
			if(event.complete == true){
				$('#card-number').addClass('StripeElement--success');
				numberComplete = true;
				checkFromComplete();
			} else {
				$('#card-number').removeClass('StripeElement--success');
				numberComplete = false;
				disableBtn();
			}
		});
		// Event : cardCvc
		cardCvc.addEventListener('change', function(event) {
			if (event.error) {
				displayError.text(event.error.message);
				displayError.show();
			} else {
				displayError.text('');
				displayError.hide();
			}
			if(event.complete == true){
				$('#card-cvc').addClass('StripeElement--success');
				cvcComplete = true;
				checkFromComplete();
			} else {
				$('#card-cvc').removeClass('StripeElement--success');
				cvcComplete = false;
				disableBtn();
			}
		});
		// Event : cardExpiry
		cardExpiry.addEventListener('change', function(event) {
			if (event.error) {
				displayError.text(event.error.message);
				displayError.show();
			} else {
				displayError.text('');
				displayError.hide();
			}
			if(event.complete == true){
				$('#card-expiry').addClass('StripeElement--success');
				expiryComplete = true;
				checkFromComplete();
			} else {
				$('#card-expiry').removeClass('StripeElement--success');
				expiryComplete = false;
				disableBtn();
			}
		});

		document.querySelector('#stripe-payment-form').addEventListener('submit', function(e) {

			// Set view
			disableBtn();
			btn.innerHTML = YOHO._('goods_feedback_submiting', '提交中...');
			model.classList.remove('hidden');

				e.preventDefault();
				var form = document.querySelector('#stripe-payment-form');
				var extraDetails = {

				};
				stripe.createSource(cardNumber).then(function(result) {

				if (result.error) {
						// Inform the user if there was an error
					var errorElement = document.querySelector('.stripe-error');
					errorElement.textContent = result.error.message;
					errorElement.classList.add('visible');
					enableBtn();
					model.classList.add('hidden');
				} else {
					// POS is not use 3DS.
					source.value = result.source.id;
					// Submit the form:
					form.submit();
				}
			});
		});

		/* Function */
		function checkFromComplete(){
			if(expiryComplete && cvcComplete && numberComplete){
				enableBtn();
			} else {
				disableBtn();
			}
		}

		function disableBtn(){
			btn.setAttribute('disabled', 'disabled');
			btn.classList.add('graybtn');
			btn.innerHTML = YOHO._('sumbit', '提交');
		}
		function enableBtn(){
			btn.removeAttribute('disabled', 'disabled');
			btn.classList.remove('graybtn');
			btn.innerHTML = YOHO._('sumbit', '提交');
		}

		function stripe3DSecure(token){
			stripe.createSource({
				type: 'three_d_secure',
				amount: amount.value,
				currency: currency.value,
				three_d_secure: {
					card: token.id
				},
				redirect: {
					return_url: returnUrl.value
				},
				metadata: {
					'order_id': order_id,
					'pay_code': pay_code,
					'log_id':   log_id
				}
			}).then(function(result) {
				if (result.error) {
						// Inform the user if there was an error
					var errorElement = document.querySelector('.stripe-error');
					errorElement.textContent = result.error.message;
					errorElement.classList.add('visible');
					enableBtn();
					model.classList.add('hidden');
				} else {
					$('.p-modal-frame').css('width', '40%');
					$('#model-body').html('<iframe style="width:100%; height: '+($(window).height() * 0.7 )+'px;" frameborder="0" src="' + result.source.redirect.url + '"></iframe>');
				}
			});
		}

	},
	need_removal: function() {
		$('input[name="need_removal"]').change(function(){
			if ($(this).val() == 1) {
				// the checkbox is now checked
				$('.need_removal_list').fadeIn();
				if($('input[name="shipping"]:checked').val() == 3 || $('input[name="shipping"]:checked').val() == 99) {
					$('input[name="nr_address"]').val($('input[name="nr_address"]').val());
					$('input[name="nr_consignee"]').val($('input[name="address"]').val());
					$('input[name="nr_phone"]').val($('input[name="consignee_mobile"]').val());
				}
			} else {
				// the checkbox is now no longer checked
				$('.nr_consignee input').val('');
				$('.need_removal_list').hide();
			}
		});
	},

	// Require goods
	handleRequireGoods: function (){
		var _this = this;
		$.ajax({
			url: '/pos.php',
			type: 'post',
			cache: false,
			data: {
				'act':'load_requiregoods',
				'user_rank': $('#user_rank').val()
			},
			dataType: 'json',
			success: function(data) {
				if (data.status == 1)
				{
					// Clear Require Goods table
					$('#requiregoods_table').find('tbody').empty();
					// Populate goods info
					$.each(data.require_goods_list, function (idx, require_goods) {
						$('#requiregoods_table').find('tbody').append($(
							'<tr id="goto_requiregoods_' + require_goods.goods_id + '">'+'<td colspan="6">' + require_goods.goods_name + ' - 變壓器</td>'+'</tr>'+
							'<tr style="height: 50px;"><th><h4>產品名稱</h4></th><th width="100"><h4>條碼</h4></th><th width="70"><h4>優惠及存貨</h4></th><th width="100"><h4>價格</h4></th><th width=""><h4></h4></th></tr>'
						));
						$.each(require_goods.goods_list, function (idx, goods) {
							var goods_id = goods.goods_id;
							var in_cart = false;
							if($.inArray(parseInt(goods_id), data.in_cart_goods) === -1) { // Not in cart
								$('#requiregoods_table').find('tbody').append($('<tr data-goods-id="' + goods_id + '" >'+
								'<td style="text-align: left;">'+
									'<img src="'+goods.goods_img+'" width="65" height="65">'+
									'<a href="goods.php?id=' + goods_id + '" target="_blank" class="goods_link">'+
										goods.goods_name+
									'</a>'+
								'</td>'+
								'<td>'+
									'<a href="x/goods.php?act=list&amp;keyword=' + goods.goods_sn + '" target="_blank">' +
										goods.goods_sn+
									'</a>'+
								'</td>'+
								'<td>'+goods.goods_number+'</td>'+
								'<td>' + (goods.shop_price >0 ? goods.formatted_shop_price : "<span style='color: #cc0000;'>免費</span>") + '</td>'+
								'<td>'+
									'<input type="hidden" size="5" disabled="disabled" value="1" class="rg_addtocart_input">'+
									'<a href="javascript:void(0)" class="rg_addtocart_btn"><i class="iconfont">ŭ</i></a>' +
								'</td>'+
								'</tr>'));
							}
						});
					});
					if ($('#requiregoods_table').find('tbody').find('tr').length > 0) {
						$('.RequireGoodsList, .need_requiregoods').show();
					} else {
						$('.RequireGoodsList, .need_requiregoods').hide();
						$('input[name=need_requiregoods]').prop("checked", false)
					}
					_this.bindGoodsLinks();

					// Add to cart when user press ENTER (ascii code = 13) or TAB (trigger blur)
					$('.rg_addtocart_input').keypress(function(event) {
						if (event.which == 13)
						{
							event.preventDefault();
							$(this).blur(); // trigger the blur event
						}
					}).blur(function(event) {
						var goods_id = $(this).closest('tr').data('goodsId');
						var qty = $(this).val();
						_this.addToCart(goods_id, qty);
					});

					// Alternatively user can click the add to cart button
					$('.rg_addtocart_btn').click(function(event) {
						var $goods_input = $(this).closest('tr').find('.rg_addtocart_input');
						$goods_input.blur(); // trigger the blur event binded above
					});

					// Need update cart?
					if (data.need_update == true){
						_this.addToCart(0, 0, 0, 2);
					}
				}
				else
				{
					YOHO.dialog.warn(data.error);
				}
			},
			error: function(xhr) {
				YOHO.dialog.warn("伺服器繁忙，請稍後再試。("+xhr.status+")");
				console.log('error: ' + JSON.stringify(xhr));
			}
		});
	},
};

$(function() {
	if (YOHO.config.posMode == 'normal')
	{
		YOHO.pos.init();
	}
	else if (YOHO.config.posMode == 'repair')
	{
		YOHO.pos.initRepair();
	}
});

})(jQuery);
