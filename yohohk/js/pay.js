/***
* pay.js
*
* Javascript file for the payment page
***/

var YOHO = YOHO || {};

YOHO.config = YOHO.config || {};

YOHO.config.disableCommonInit = true;

(function($) {

YOHO.payment = {

	init: function() {
		YOHO.orderInfo = YOHO.orderInfo || {};
		this.showTrade();
		this.paypalAutoSubmit();
		this.alipayAutoRedirect();
		// this.huikuanDialog();
		this.alipayDialog();
		this.changePaymentDialog();
		this.checkPayStatus();
		this.checkWeChatPayStatus();
		this.checkPaymePaymentStatus();
		this.checkAlipayStatus();
	},

	// 訂單詳情事件
	showTrade: function() {
		var $trabtn = $("#trade-showbtn"),
		$traDetail = $trabtn.next("div.trade-detail");

		$trabtn.on('click', function(e) {
			e.preventDefault();
			var $this = $(this);
			if ($this.hasClass('clicked'))
			{
				return;
			}
			$this.addClass('clicked');
			$traDetail.fadeIn();
		});

		$(document).on('click', function(e) {
			if ($(e.target).parents('.trade-detail').length > 0)
			{
				if (e.target.nodeName.toLowerCase() == 'a')
				{
					return;
				}
				e.preventDefault();
			}
			else
			{
				$trabtn.removeClass('clicked');
				$traDetail.fadeOut();
			}
		});
	},

	// PayPal 付款
	paypalAutoSubmit: function() {
		if (!$('.other_payment').length)
		{
			var $submitBtn = $('form[action="https://www.paypal.com/cgi-bin/webscr"]').find('input[type="submit"]');
			if ($submitBtn.length)
			{
				$submitBtn.trigger('click');
				$('<img src="/images/loading_yoho.gif" style="display: block; margin-left: auto; margin-right: auto;">').insertAfter($submitBtn);
				$submitBtn.remove();
			}
		}
	},

	// 支付寶
	alipayAutoRedirect: function() {
		if (!$('.other_payment').length)
		{
			// Auto redirect to Alipay
			var $alipayBtn = $('a[href^="https://www.alipay.com/cooperate/gateway.do"]');
			if ($alipayBtn.length)
			{
				$('<img src="/images/loading_yoho.gif" style="display: block; margin-left: auto; margin-right: auto;">').insertAfter($alipayBtn);
				$alipayBtn.hide();
				window.location.href = $alipayBtn.attr('href');
			}
		}
	},

	// 銀行匯款
	huikuanDialog: function() {
		var $btn = $('#huikuan-btn');

		if (!$btn.length) return;
		var order_id = YOHO.orderInfo.order_id || 0;
		var banks_html = '',html = '';
		$.ajax({
			url: '/ajax/bank_accounts.php?act=bankings&order_id=' + order_id,
			dataType: 'json',
			success:function(e){
				var user = e.bank_user;
				var banks = e.banks;
				banks_html = '<p><span class="gray">' + YOHO._('payment_bank_account_name', '帳戶名稱: ') + ' </span>' + user + '</p>';
				banks.map(function(v,i){ banks_html += '<p><span class="gray">' + v.name + ': </span>' +v.account + '</p>';});
				html = '<div class="bank-intro">'+
						'<h4><b>1</b> ' + YOHO._('payment_bank_transfer', 'ATM轉帳 | 網上銀行轉帳') + '</h4>'+
						'<div id="banks">' + banks_html + '</div>'+
						'<h4><b>2</b> ' + YOHO._('payment_tell_yoho', '完成後請將轉帳收據發送給友和以便核實您的付款') + '</h4>'+
						'<p><span class="gray">' + YOHO._('payment_tell_yoho_whatsapp', 'WhatsApp：') + '</span>53356996</p>'+
						'<p><span class="gray">' + YOHO._('payment_tell_yoho_email', '電郵：') + '</span><a href="mailto:info@yohohongkong.com">info@yohohongkong.com</a></p>'+
						'<p><span class="gray">' + YOHO._('payment_tell_yoho_facebook_messenger', 'Facebook Messenger：') + '</span><a href="https://m.me/yohohongkong">'+ YOHO._('payment_tell_yoho_facebook_messenger_link', '查詢') +'</a></p>'+
						'<h4><b>3</b> ' + YOHO._('payment_tell_yoho_confirm', '確認收妥款項後會馬上處理') + '</h4>'+
					'</div>';
				$btn.click();
			}
		});
		$btn.on('click', function() {
			var _this = this,
				dialog = $.dialog({
					title: YOHO._('payment_bank_transfer_title', '銀行轉帳'),
					id: 'huikuan',
					padding: '0 20px',
					okVal: YOHO._('payment_dialog_ok', '知道了'),
					follow: _this,
					ok: function() {
						return;
					},
					content: html
				});
		});//.trigger('click');
	},

	// 支付寶
	alipayDialog: function() {
		var $btn = $('#alipay-btn');

		if ($btn.length === 0) return;

		var html = '<div class="huikuan-intro">'+
				'<h4><b>1</b> ' + YOHO._('payment_alipay', '轉帳到本公司的支付寶帳戶') + '</h4>'+
				'<p><span class="gray">' + YOHO._('payment_alipay_account', '帳戶：') + '</span>tb_lhsale@yahoo.com</p>'+
				'<p><span class="gray">' + YOHO._('payment_alipay_name', '姓名：') + '</span>胡柱</p>'+
				'<h4><b>2</b> ' + YOHO._('payment_tell_yoho', '完成後請將轉帳收據發送給友和以便核實您的付款') + '</h4>'+
				'<p><span class="gray">' + YOHO._('payment_tell_yoho_whatsapp', 'WhatsApp：') + '</span>53356996</p>'+
				'<p><span class="gray">' + YOHO._('payment_tell_yoho_email', '電郵：') + '</span><a href="mailto:info@yohohongkong.com">info@yohohongkong.com</a></p>'+
				'<p><span class="gray">' + YOHO._('payment_tell_yoho_facebook_messenger', 'Facebook Messenger：') + '</span><a href="https://m.me/yohohongkong">'+ YOHO._('payment_tell_yoho_facebook_messenger_link', '查詢') +'</a></p>'+
				'<h4><b>3</b> ' + YOHO._('payment_tell_yoho_confirm', '確認收妥款項後會馬上處理') + '</h4>'+
			'</div>';
		$btn.on('click', function() {
			var _this = this,
				dialog = $.dialog({
					title: YOHO._('payment_alipay_title', '支付寶轉帳'),
					id: 'alipay',
					padding: '0 20px',
					okVal: YOHO._('payment_dialog_ok', '知道了'),
					follow: _this,
					ok: function() {
						return;
					},
					content: html
				});
		}).trigger('click');
	},

	changePaymentDialog: function() {
		var $other_pay = $('.other_payment');
		$other_pay.find('#change_payment_btn').on('click', function() {
			var _this = this;
			var dialog = $.dialog({
				title: YOHO._('payment_change_method', '改用其他付款方式'),
				id: 'changePayment',
				padding: '20px',
				follow: _this,
				content: $other_pay.find('#other_payment_form_container').html()
			});
		});
	},

	checkPayStatus: function() {
		var order_id = YOHO.orderInfo.order_id || 0;
		if (!order_id) return;

		var initialCheckInterval = ($('.payment_processing').length) ? 3000 : 5000;
		var checkInterval = initialCheckInterval;
		var checkPayStatus = function () {
			$.ajax({
				url: '/ajax/order_status.php',
				type: 'post',
				dataType: 'json',
				data: {
					'act': 'pay_status',
					'order_id': order_id
				},
				success: function(data) {
					if (data.status == 1) {
						var pay_status = data.pay_status;
						if (pay_status == 2) // PS_PAYED
						{
							// Order paid, redirect to order detail page
							if ($('.payment_processing').length || data.payment_success == 1)
							{
								window.location = '/user.php?act=order_detail&order_id=' + order_id + '&paysuccess=1';
							}
							else
							{
								window.location = '/user.php?act=order_detail&order_id=' + order_id;
							}
							return;
						}
					}
					else if (data.status == 0) {
						if (data.hasOwnProperty('error') && data.error.length > 0)
						{
							YOHO.dialog.warn(data.error);
							setTimeout(function () {
								window.location = '/respond.php?code=' + $("#pay_code").val();
							}, 3000)
						}
					// } else if (data.status == 2) {
					// 	if (data.hasOwnProperty('error') && data.error.length > 0)
					// 	{
					// 		YOHO.dialog.warn(data.error);
					// 		setTimeout(function () {
					// 			window.location.reload();
					// 		}, 3000)
					// 	}
					}

					checkInterval = initialCheckInterval;
					setTimeout(checkPayStatus, checkInterval);
				},
				error: function(xhr) {
					//YOHO.dialog.warn('伺服器繁忙，請稍後再試。('+xhr.status+')');

					checkInterval = checkInterval * 2;
					setTimeout(checkPayStatus, checkInterval);
				}
			});
		};
		setTimeout(checkPayStatus, checkInterval);
	},

	//Stripe
	stripe: function(){

		// Stripe V3 js config
		var publishableKey = document.getElementById('publishableKey').value;
		var stripe = Stripe(publishableKey);
		// From button
		var btn        = document.getElementById('stripeBtn');
		// Processing model
		var model      = document.getElementById('processing-model');
		// Hidden input field
		var amount     = document.getElementById('amount');
		var currency   = document.getElementById('currency');
		var returnUrl = document.getElementById('return-url');
		var source = document.getElementById('source');
		// Error message div
		var displayError = $('#stripe-error');
		// Default vaild
		var numberComplete = false;
		var cvcComplete = false;
		var expiryComplete = false;
		var order_id = document.getElementById('order_id').value;
		var log_id = document.getElementById('log_id').value;
		var pay_code = document.getElementById('pay_code').value;
		// Default disable from button.
		disableBtn();

		// Setup stripe elements
		var elements = stripe.elements({locale:'en'});
		var style = {
			base: {
			  '::placeholder': {
				color: '#ccc',
			  },
			},
			invalid: {
			  color: '#e5424d',
			  ':focus': {
				color: '#303238',
			  },
			}
		  };
		// Card number
		var cardNumber = elements.create('cardNumber',{placeholder:'‎4111 1111 1111 1111', style: style});
		cardNumber.mount('#card-number');
		// Card cvc
		var cardCvc = elements.create('cardCvc',{style: style});
		cardCvc.mount('#card-cvc');
		// Card expiry date
		var cardExpiry = elements.create('cardExpiry',{style: style});
		cardExpiry.mount('#card-expiry');
		// Event : cardNumber
		cardNumber.addEventListener('change', function(event) {
			if (event.error) {
				displayError.text(event.error.message);
				displayError.show();
			} else {
				displayError.text('');
				displayError.hide();
				//check type
				var type = event.brand.toLowerCase();
				type = type.replace(" ", "-");
				card_type = type;
				$('#card-image').removeClass().addClass(type);
			}
			if(event.complete == true){
				$('#card-number').addClass('StripeElement--success');
				numberComplete = true;
				checkFromComplete();
			} else {
				$('#card-number').removeClass('StripeElement--success');
				numberComplete = false;
				disableBtn();
			}
		});
		// Event : cardCvc
		cardCvc.addEventListener('change', function(event) {
			if (event.error) {
				displayError.text(event.error.message);
				displayError.show();
			} else {
				displayError.text('');
				displayError.hide();
			}
			if(event.complete == true){
				$('#card-cvc').addClass('StripeElement--success');
				cvcComplete = true;
				checkFromComplete();
			} else {
				$('#card-cvc').removeClass('StripeElement--success');
				cvcComplete = false;
				disableBtn();
			}
		});
		// Event : cardExpiry
		cardExpiry.addEventListener('change', function(event) {
			if (event.error) {
				displayError.text(event.error.message);
				displayError.show();
			} else {
				displayError.text('');
				displayError.hide();
			}
			if(event.complete == true){
				$('#card-expiry').addClass('StripeElement--success');
				expiryComplete = true;
				checkFromComplete();
			} else {
				$('#card-expiry').removeClass('StripeElement--success');
				expiryComplete = false;
				disableBtn();
			}
		});

		document.querySelector('#stripe-payment-form').addEventListener('submit', function(e) {

			// Set view
			disableBtn();
			btn.innerHTML = YOHO._('goods_feedback_submiting', '提交中...');
			model.classList.remove('hidden');

		  	e.preventDefault();
		  	var form = document.querySelector('#stripe-payment-form');
		  	var extraDetails = {

		  	};
		  	stripe.createSource(cardNumber).then(function(result) {

		    	if (result.error) {
		      		// Inform the user if there was an error
		    		var errorElement = document.querySelector('.stripe-error');
					errorElement.textContent = result.error.message;
					errorElement.classList.add('visible');
					enableBtn();
					model.classList.add('hidden');
		    	} else {
					if(card_type != "amex"){
						stripe3DSecure(result.source);
					} else {
						// AE card is not support 3DS.
						// Success, get the source ID:
						// Insert the source into the form so it gets submitted to the server:
						source.value = result.source.id;
						// Submit the form:
						form.submit();
					}
		    	}
			});
		});

		/* Function */
		function checkFromComplete(){
			if(expiryComplete && cvcComplete && numberComplete){
				enableBtn();
			} else {
				disableBtn();
			}
		}

		function disableBtn(){
			btn.setAttribute('disabled', 'disabled');
			btn.classList.add('graybtn');
			btn.innerHTML = YOHO._('sumbit', '提交');
		}
		function enableBtn(){
			btn.removeAttribute('disabled', 'disabled');
			btn.classList.remove('graybtn');
			btn.innerHTML = YOHO._('sumbit', '提交');
		}

		function stripe3DSecure(token){
			stripe.createSource({
			  type: 'three_d_secure',
			  amount: amount.value,
			  currency: currency.value,
			  three_d_secure: {
			    card: token.id
			    },
			  redirect: {
				return_url: returnUrl.value
				},
				metadata: {
					'order_id': order_id,
					'pay_code': pay_code,
					'log_id':   log_id
			    }
			}).then(function(result) {
		    	if (result.error) {
		      		// Inform the user if there was an error
		    		var errorElement = document.querySelector('.stripe-error');
					errorElement.textContent = result.error.message;
					errorElement.classList.add('visible');
					enableBtn();
					model.classList.add('hidden');
		    	} else {
					$('.p-modal-frame').css('width', '40%');
					$('#model-body').html('<iframe style="width:100%; height: '+($(window).height() * 0.7 )+'px;" frameborder="0" src="' + result.source.redirect.url + '"></iframe>');
		    	}
			});
		}

	},

	//Stripe WeChat
	stripeWechat: function(){
		var publishableKey = document.getElementById('publishableKey').value;
		var stripe = Stripe(publishableKey);
		// Hidden input field
		var amount     = document.getElementById('amount');
		var currency   = document.getElementById('currency');
		var source = document.getElementById('source');
		var order_id = document.getElementById('order_id').value;
		var log_id = document.getElementById('log_id').value;
		var pay_code = document.getElementById('pay_code').value;
		var qrCode = $('#qrCode');


		stripe.createSource({
			type: 'wechat',
			amount: amount.value,
			currency: currency.value,
			metadata: {
				'order_id': order_id,
				'pay_code': pay_code,
				'log_id':   log_id
			}
		}).then(function(result) {
			var qr_code_url = result.source.wechat.qr_code_url;
			qrCode.attr("href", qr_code_url);
			$.ajax({
				url: '/ajax/qrcode.php',
				type: 'post',
				dataType: 'json',
				data: {
					'qr_code_url': qr_code_url,
					'order_id':    order_id
				},
				success: function(data) {

					if(data.status == 1){ //success
						qrCode.html('<img src="'+data.qr_code+'" />');
					} else {
						YOHO.dialog.warn('伺服器繁忙，請稍後再試。('+xhr.status+')');
					}
				}
			});
			// qrCode.html('<img src="https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl='+qr_code_url+'" />');
		});
	},


	//BrainTree Credit card
	braintree: function(){

		var clientToken = document.querySelector('#clientToken').value;
		var form = document.querySelector('#brain-cardForm');
		var payBtn = document.getElementById('button-pay');
		var nonceGroup = document.querySelector('.nonce-group');
		var nonceInput = document.querySelector('.nonce-group input');
		var payGroup = document.querySelector('.pay-group');
		var modal = document.getElementById('modal');
		var bankFrame = document.querySelector('.bt-modal-body');
		var closeFrame = document.getElementById('text-close');
		var components = {
			client: null,
			dataCollector: null,
			threeDSecure: null,
			hostedFields: null
		};


		braintree.client.create({
			authorization: clientToken
		},onClientCreate);


		function onClientCreate(err,client){

			if(err){
				console.log('client error:', err);
				return;
			}

			components.client = client;

			// Braintree Hosted Fields
			braintree.hostedFields.create(
			{
			  	client: client,
			  	styles: {
			    	'input': {
			      		'font-size': '14px'
			    	},
				    'input.invalid': {
				      	'color': 'red'
				    },
				    'input.valid': {
				      	'color': 'green'
				    }
			  	},
			  	fields: {
				    number: {
				      	selector: '#card-number',
					  	placeholder: 'xxxx xxxx xxxx xxxx'
				    },
				    cvv: {
				      	selector: '#cvv',
					  	placeholder: '123'
				    },
				    expirationDate: {
				      	selector: '#expiration-date',
					  	placeholder: 'MM/YYYY'
				    }
			  	}
		 	}, onComponent('hostedFields'));

			//3D Secure
			braintree.threeDSecure.create({
			    client: client
			  }, onComponent('threeDSecure'));

			//Data collector (kount)
			braintree.dataCollector.create({
			    client: client,
			    kount: true
			  }, onComponent('dataCollector'));
		}

		function onComponent(name){

			return function(err, component) {
			    if (err) {
			    	console.log('component error:', err);
			    	return;
			    }

			    components[name] = component;

			    if (components.threeDSecure && components.hostedFields && components.dataCollector) {
			    	setupForm();
			    }
			}
		}
		
		function setupForm() {
		  	enablePayNow();
		}

		function addFrame(err, iframe) {
			bankFrame.appendChild(iframe);
			modal.classList.remove('hidden');
		}

		function removeFrame() {
			var iframe = bankFrame.querySelector('iframe');
			modal.classList.add('hidden');
			iframe.parentNode.removeChild(iframe);
		}

		function enablePayNow() {
			payBtn.removeAttribute('disabled', 'disabled');
			payBtn.classList.remove('graybtn');
			components.hostedFields.on('validityChange', function (event) {

		      	// Check if all fields are valid, then show submit button
		      	var formValid = Object.keys(event.fields).every(function (key) {
		        	return event.fields[key].isValid;
		      	});

		      	if (formValid) {
		      		payBtn.classList.add('show-button');
		      	} else {
		      		payBtn.classList.remove('show-button');
		      	}
	    	});
			components.hostedFields.on('empty', function (event) {
					$('#card-image').removeClass();
					$(form).removeClass();
		    });
			components.hostedFields.on('cardTypeChange', function (event) {
					// Change card bg depending on card type
				if (event.cards.length === 1) {
			        $(form).removeClass().addClass(event.cards[0].type);
			        $('#card-image').removeClass().addClass(event.cards[0].type);
			        $('header').addClass('header-slide');

					// Change the CVV length for AmericanExpress cards
			        if (event.cards[0].code.size === 4) {
			        	components.hostedFields.setAttribute({
			            	field: 'cvv',
			            	attribute: 'placeholder',
			            	value: '1234'
			          	});
			        } else {
			      		components.hostedFields.setAttribute({
			          		field: 'cvv',
			          		attribute: 'placeholder',
			          		value: '123'
						});
					}
		      	} else {
		      		components.hostedFields.setAttribute({
		          		field: 'cvv',
		          		attribute: 'placeholder',
		          		value: '123'
					});
				}
			});
		}

		closeFrame.addEventListener('click', function () {
			components.threeDSecure.cancelVerifyCard(removeFrame());
			enablePayNow();
		});

		payBtn.addEventListener('click', function(event) {
			payBtn.setAttribute('disabled', 'disabled');
			payBtn.classList.add('graybtn');
			var amount = document.getElementById('amount').value;
			components.hostedFields.tokenize(function(err, payload) {
			    if (err) {
			    	//console.log('tokenization error:', err);
			    	enablePayNow();
			    	return;
			    }

			    components.threeDSecure.verifyCard({
			    	amount: amount,
			    	nonce: payload.nonce,
			    	addFrame: addFrame,
			    	removeFrame: removeFrame
			    }, function(err, verification) {
			    	if (err) {
			    		enablePayNow();
			    		return;
			    	}

					var deviceData = components.dataCollector.deviceData;
					//console.log('deviceData:', deviceData);
			    	document.querySelector('input[name=\"device_data\"]').value = deviceData;
			    	document.querySelector('input[name=\"payment_method_nonce\"]').value = verification.nonce;
    				form.submit();
			    });
			});
		});
	},

	// Check WeChat Pay
	checkWeChatPayStatus: function() {
		if (typeof webhook === 'undefined' || !webhook || typeof qfwechatpay_order_payment_id === 'undefined' || !qfwechatpay_order_payment_id) return;
		var initialCheckInterval = ($('.payment_processing').length) ? 3000 : 5000;
		var checkInterval = initialCheckInterval;
		var checkPayStatus = function () {
			$.ajax({
				url: '/ajax/qfwechatpay_order_status.php',
				type: 'post',
				dataType: 'json',
				data: {
					'order_payment_id': qfwechatpay_order_payment_id
				},
				success: function(data) {
					checkInterval = initialCheckInterval;
					setTimeout(checkPayStatus, checkInterval);
				},
				error: function(xhr) {
					checkInterval = checkInterval * 2;
					setTimeout(checkPayStatus, checkInterval);
				}
			});
		};
		setTimeout(checkPayStatus, checkInterval);
	},

	// Check Alipay
	checkAlipayStatus: function() {
		if (typeof webhook === 'undefined' || !webhook) return;
		if (typeof alipay_order_payment_id === 'undefined') return;
		if (typeof wallet === 'undefined') return;

		var initialCheckInterval = ($('.payment_processing').length) ? 3000 : 5000;
		var checkInterval = initialCheckInterval;
		var checkPayStatus = function () {
			$.ajax({
				url: '/ajax/alipay_order_status.php',
				type: 'post',
				data: {
					'order_payment_id': alipay_order_payment_id,
					'wallet': wallet
				},
				success: function(data) {
					checkInterval = initialCheckInterval;
					setTimeout(checkPayStatus, checkInterval);
				},
				error: function(xhr) {
					checkInterval = checkInterval * 2;
					setTimeout(checkPayStatus, checkInterval);
				}
			});
		};
		setTimeout(checkPayStatus, checkInterval);
	},

	// Check Payme Payment Status
	checkPaymePaymentStatus: function() {
		if ( typeof webhook === 'undefined' || !webhook || typeof payme_request_id === 'undefined' || !payme_request_id) return;
		if ( typeof moment === 'undefined') return; // Since Moment.js is loaded only when payme payment is selected

		var initialCheckInterval = ($('.payment_processing').length) ? 3000 : 5000;
		var checkInterval = initialCheckInterval;
		var checkPayStatus = function () {
			var normal_response = true;
			$.ajax({
				url: '/ajax/payme_order_status.php',
				type: 'post',
				dataType: 'json',
				data: {
					'payment_request_id': payme_request_id
				},
				success: function(data) {
					
					if (data.pay_status == "FAILURE") {
						normal_response = false;
					} else {
						normal_response = true;
					}
					if (data.pay_status == "EXPIRED") {
						showErrorMessage(payme_error_message.payCodeExpired);
						return;
					}

					if (moment().isSameOrAfter(payme_request_expiry_date_time) && !normal_response) {
						showErrorMessage(payme_error_message.payCodeExpiredNotConfirmed);
					}

					checkInterval = initialCheckInterval;
					setTimeout(checkPayStatus, checkInterval);
				},
				error: function(xhr) {
					normal_response = false;
					checkInterval = checkInterval * 2;

					if (moment().isSameOrAfter(payme_request_expiry_date_time) && !normal_response) {
						showErrorMessage(payme_error_message.payCodeExpiredNotConfirmed);
						return;
					}

					setTimeout(checkPayStatus, checkInterval);
				}
			});
		};

		setTimeout(checkPayStatus, checkInterval);

		function showErrorMessage(message) {
			$("#payCodeCanvas").hide();
			$(".payme-error-message").html(message);
			$(".payme-error-container").show();

			setTimeout(function(){
				window.location.reload();
			}, 1000);
		}
	},

};

$(function() {
	YOHO.payment.init();
});

})(jQuery);
