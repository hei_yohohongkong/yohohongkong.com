/***
 * deal.js
 *
 * Javascript file for Deal
 ***/

/* jshint -W041 */

var YOHO = YOHO || {};

YOHO.config = YOHO.config || {};

YOHO.config.disableCommonInit = true;

(function($) {

YOHO.deal = {
	
	init: function() {
		this.dealForm();
	},
	
	validateRequiredField: function($e) {
		if ($e.val() == '')
		{
			this.makeTextFieldError($e);
			return false;
		}
		return true;
	},
	
	makeTextFieldError: function($e) {
		$e.addClass('error');
		$e.on('keyup.removeError', function (event) {
			$e.removeClass('error');
			$e.off('keyup.removeError');
		});
	},
	
	dealForm: function() {
		var frm = $('form#deal_form');
		var _this = this;
		
		frm.submit(function (event) {
			frm.find('input.required').each(function (idx) {
				_this.validateRequiredField($(this));
			});
			
			frm.find('select.required').each(function (idx) {
				if ($(this).val() == '')
				{
					$(this).addClass('error');
					$(this).on('change.removeError', function (event) {
						$(this).removeClass('error');
						$(this).off('change.removeError');
					});
				}
			});
			
			var modelSelect = frm.find('select[name="model"]');
			var mobileField = frm.find('input[name="mobile"]');
			var emailField = frm.find('input[name="email"]');
			
			if (!YOHO.check.isMobile(mobileField.val()))
			{
				_this.makeTextFieldError(mobileField);
			}
			if (!YOHO.check.isEmail(emailField.val()))
			{
				_this.makeTextFieldError(emailField);
			}
			
			if ((frm.find('input.error').length > 0) || (frm.find('select.error').length > 0))
			{
				// Have error
			}
			else
			{
				$.ajax({
					url: '/deal',
					type: 'post',
					cache: false,
					data: {
						'act':'aregister',
						'model': modelSelect.val(),
						'mobile': mobileField.val(),
						'email': emailField.val()
					},
					dataType: 'json',
					success: function(data) {
						if (data.status == 1)
						{
							YOHO.dialog.ok(data.message);
						}
						else
						{
							YOHO.dialog.warn(data.error);
						}
					},
					error: function(xhr) {
						YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
						console.log('error: ' + JSON.stringify(xhr));
					}
				});
			}
			
			event.preventDefault(); // Stop the from from submitting
		});
	}
};

$(function() {
	YOHO.deal.init();
});

})(jQuery);
