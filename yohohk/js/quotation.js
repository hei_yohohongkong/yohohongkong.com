/***
 * quotation.js
 *
 * Javascript file for the quotation page
 ***/

var YOHO = YOHO || {};

YOHO.config = YOHO.config || {};

YOHO.config.disableCommonInit = true;

(function ($) {

    YOHO.quotation = {

        init: function ()
        {
            // 確認送貨方式模塊
            this.shipping_method();
            // 確認付款方式模塊
            this.payment_method();
            this.submit();
        },

        // 確認送貨方式模塊 函數
        shipping_method: function ()
        {
            //付款方式切換選擇事件
            var _this = this;
            var $ship = $('div.shipping_method');

            $ship.on('click', 'li', function () {
                var $this = $(this);
                if (!$this.hasClass('current') && !$this.hasClass('disable'))
                {
                    $ship.find('input[name=shipping]').prop('checked', false); //選中之前先清空所有按鈕選擇狀
                    $this.find('input[name=shipping]').prop('checked', true); // 選中radio

                    $this.addClass('current').siblings().removeClass('current');

                    // 如果支援運費到付而且需要付運費的話，顯示運費到付選項
                    if (($(this).hasClass('fod')) && (_this.parsePrice($(this).find('em.postage_fee').text()) > 0))
                    {
                        $('div.shipping_fee_on_delivery').show();
                        $('input[name="shipping_fod"]').prop('disabled', false);
                    } else
                    {
                        $('div.shipping_fee_on_delivery').hide();
                        $('input[name="shipping_fod"]').prop('disabled', true);
                        $('input[name="shipping_fod"]').prop('checked', false);
                    }

                    _this.updatePrice();
                }
            });

            $('input[name="shipping_fod"]').change(function () {
                _this.updatePrice();
            });

            // If there is shipping method already checked, trigger the click event to select it
            if ($ship.find('input[name="shipping"]:checked').length > 0)
            {
                $ship.find('input[name="shipping"]:checked').closest('li').trigger('click');
            }
            // Otherwise auto select first shipping method
            else
            {
                $ship.find('li').first().trigger('click');
            }
        },

        // 確認付款方式模塊 函數
        payment_method: function ()
        {
            //付款方式切換選擇事件
            var _this = this;
            var $pay = $('div.payment_method');

            $pay.on('click', 'li', function () {
                var $this = $(this);
                if (!$this.hasClass('current') && !$this.hasClass('disable'))
                {
                    $pay.find('input[name=payment]').prop('checked', false); // deselect all
                    $this.find('input[name=payment]').prop('checked', true); // select this

                    $this.addClass('current').siblings().removeClass('current');

                    _this.updatePrice();
                }
            });

            // If there is payment method already checked, trigger the click event to select it
            if ($pay.find('input[name="payment"]:checked').length > 0)
            {
                $pay.find('input[name="payment"]:checked').closest('li').trigger('click');
            }
            // Otherwise auto select first shipping method
            else
            {
                $pay.find('li').first().trigger('click');
            }
        },

        //處理價錢
        parsePrice: function (formatted_price)
        {
            if (formatted_price == null || formatted_price == undefined)
            {
                return 0;
            }
            if (formatted_price.toFixed)
            {
                return formatted_price; // it is a number already
            }
            if (formatted_price.length == 0)
            {
                return 0;
            }
            if (formatted_price.indexOf('%') === formatted_price.length - 1)
            {
                return 0; // don't process percentage here
            }
            var price = parseFloat(formatted_price);
            if (isNaN(price))
            {
                var matches = (formatted_price.match(/\-?\d+[,\.]?\d*/));
                if ($.isArray(matches))
                {
                    price = parseFloat(matches[0]);
                } else
                {
                    if (console && typeof console.log == 'function')
                    {
                        console.log('Warning: "' + formatted_price + '" cannot be parsed as price');
                    }
                    price = 0;
                }
            }
            return price;
        },

		//計算/更新價格
		updatePrice: function()
		{
			var shipping_fod = 0;

			var couponcodes = [];
			//7: payapl//3:cash
			var payment_id = $('div.payment_method li.current input[name="payment"]').val();
			if(payment_id == '1')payment_id = '7';
			else if(payment_id == '2') payment_id = '3';

			$.ajax({
				url : '/ajax/checkout.php',
				type: 'post',
				cache: false,
				data: {
					'act'          : 'calculate_amount',
					'total'        : $('#total_price').attr('totalPrice'),
					'shipping'     : $('div.shipping_method li.current input[name="shipping"]').val(),
					'payment'      : payment_id,
					'coupon'       : couponcodes,
					'integral'     : $('input.integral_txt').val(),
					'address'      : $('input[name="address_radio"]').val(),
                    'shipping_fod' : shipping_fod,
                    'is_quotation' : 1
				},
				dataType: 'json',
				success: function(result) {
					if (result.status == 1)
					{
                        console.log(result);
						var total = result.total;
						var incl_shipping_text = YOHO._('checkout_shipping_fee_included', '已包含運費');
						if ($('input[name="shipping_fod"]').prop('checked'))
						{
							incl_shipping_text = YOHO._('checkout_shipping_fee_excluded', '不包含運費');
						}
						$('.include_shipping').text('(' + incl_shipping_text + ')');

						payment = total.pay_fee;

						$('p.payment_fee_value span').text(payment.toFixed(2));
						$('p.payment_discount_value span').text((payment * -1).toFixed(2));
						if (payment > 0)
						{
							$('p.payment_fee_value').show();
							$('p.payment_discount_value').hide();
						}
						else if (payment < 0)
						{
							$('p.payment_fee_value').hide();
							$('p.payment_discount_value').show();
						}
						else
						{
							$('p.payment_fee_value').hide();
							$('p.payment_discount_value').hide();
						}
						// Update total price
						$('div.total_price em').text(total.amount_formated);
					}
				},
				error: function(xhr) {
					YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
				}
			});
		},

        /*  Validate before submitting form */
        submit: function ()
        {
            $('#receiverForm_btn').click(function () {

                if (!$('input[name=company_name]').val() || $('input[name=company_name]').val() == "")
                {
                    YOHO.dialog.warn(YOHO._('quotation_input_company_name', '請輸入公司名稱！'));
                    return false;
                }

                if (!$('input[name=contact_email]').val() || $('input[name=contact_email]').val() == "")
                {
                    YOHO.dialog.warn(YOHO._('quotation_input_contact_email', '請輸入聯絡電郵！'));
                    return false;
                }
                else if (!YOHO.check.isEmail($('input[name=contact_email]').val()))
                {
                    YOHO.dialog.warn(YOHO._('email_info', '請正確填寫此電郵地址，我們會將報價單寄到此電郵。'));
                    return false;
                }

                if (!$('input[name=contact_tel]').val() || $('input[name=contact_tel]').val() == "")
                {
                    YOHO.dialog.warn(YOHO._('quotation_input_contact_tel', '請輸入聯絡電話！'));
                    return false;
                }

                if (!$('input[name=contact_name]').val() || $('input[name=contact_name]').val() == "")
                {
                    YOHO.dialog.warn(YOHO._('quotation_input_contact_name', '請輸入聯絡人名稱！'));
                    return false;
                }

                if (!$('input[name=shipping]:checked').val())
                {
                    YOHO.dialog.warn(YOHO._('checkout_error_missing_shipping', '請選擇送貨方式！'));
                    return false;
                }
                if (!$('input[name=payment]:checked').val())
                {
                    YOHO.dialog.warn(YOHO._('checkout_error_missing_payment', '請選擇付款方式！'));
                    return false;
                }
                $(this).html('<img src="/images/loading.gif" /> ' + YOHO._('cart_submitting', '提交中'));
                $('#receiverForm').submit();
                return true;
            });
        }
    };

    $(function () {
        YOHO.quotation.init();
    });

})(jQuery);
