/***
 * login.js
 *
 * Login / Register page
 ***/

/* jshint -W041 */

var YOHO = YOHO || {};

YOHO.config = YOHO.config || {};

YOHO.config.disableCommonInit = true;

(function($){

YOHO.account = {
	setupTelInput: function ($input, extra_opts) {
		var opts = {};
		opts.defaultCountry = (YOHO.userInfo.hasOwnProperty('area') && YOHO.userInfo.area) ? YOHO.userInfo.area.toLowerCase() : 'hk';
		opts.preferredCountries = ['hk','mo','tw','cn'];
		opts.onlyCountries = !!window.available_country_codes ? window.available_country_codes : opts.preferredCountries;
		opts.onlyCountries = ($input.data('countries')) ? $input.data('countries').toLowerCase().split(',') : opts.onlyCountries;
		opts.defaultCountry = (opts.onlyCountries.indexOf(opts.defaultCountry) !== -1) ? opts.defaultCountry : 'hk';
		
		var containsAll = function(arr1, arr2) {
			return $.grep(arr2, function(v) { return $.inArray(v, arr1) !== -1; }).length === arr2.length;
		};
		if (containsAll(opts.preferredCountries, opts.onlyCountries))
		{
			// If preferredCountries includes all onlyCountries, we don't need preferredCountries at all
			delete opts.preferredCountries;
		}
		
		if ($.type(extra_opts) === 'object')
		{
			opts = $.extend(opts, extra_opts);
		}
		
		$input.intlTelInput(opts);
	},
	
	login: function() {
		var $form = $('#login-form');
		var $name = $form.find('input[name=username]');
		var $pwd = $form.find('input[name=password]');
		var $msg = $form.find('em.msg');
		var $btn = $form.find('a.btn-css3');
		
		YOHO.account.setupTelInput($name, {dropdownContainer: 'body'});
		
		$pwd.keyup(function(event) {
			if(event.keyCode == 13)
			{
				$btn.trigger('click');
			}
		});
		
		$btn.click(function() {
			if ($btn.find('img').length)
			{
				return false;
			}
			
			var _ccc = $name.intlTelInput('getSelectedCountryData').dialCode;
			var _name = $.trim($name.val());
			var _pwd = $.trim($pwd.val());
			
			if(!_name || !_pwd)
			{
				$msg.text(sprintf(
					YOHO._('account_please_fill_in', '請輸入%s'),
					((!_name) ? YOHO._('account_username', '手提電話號碼') : YOHO._('account_password', '密碼'))
				)).show().delay(2000).fadeOut();
				return false;
			}
			
			$btn.html('<img src="/images/loading.gif" /> ' + YOHO._('account_logging_in', '登入中'));
			
			$.ajax({
				url: '/ajax/ajaxuser.php',
				type: 'post',
				data: {
					'ccc': _ccc,
					'username': _name,
					'password': _pwd
				},
				dataType: 'json',
				success: function(data) {
					if (data.status == 1)
					{
						if(data.redirect) {
							location.href = data.redirect;
						} else {
							location.href = '/user.php';
						}
					}
					else if (data.status == 0)
					{
						$btn.html(YOHO._('account_login_btn', '登 入'));
						$msg.text(YOHO._('account_login_failed', '您輸入的手提電話號碼和密碼不相符！')).show().delay(2000).fadeOut();
					}
				},
				error: function(xhr) {
					$btn.html(YOHO._('account_login_btn', '登 入'));
					YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
				}
			});
		});
	},
	
	register: function() {
		var $form = $('#reg-email');
		var $mobile = $form.find('input[name=mobile]');
		var $email = $form.find('input[name=email]');
		var $pwd = $form.find('input[name=password]');
		var $pwd2 = $form.find('input[name=password2]');
		var $reference = $form.find('input[name=reference]');
		var $msg = $form.find('li.msg em');

		YOHO.account.setupTelInput($mobile);
		$form.on('click', 'a.btn-css3', function() {
			var $btn = $(this);
			
			if($btn.find('img').length > 0)
			{
				return false;
			}
			
			var _ccc = $mobile.intlTelInput('getSelectedCountryData').dialCode;
			var _mobile = $.trim($mobile.val());
			var _email = $.trim($email.val());
			var _pwd = $.trim($pwd.val());
			var _pwd2 = $.trim($pwd2.val());
			var _ref = $.trim($reference.val());
			
			if(!_mobile || !_email || !_pwd || !_pwd2)
			{
				$msg.text(sprintf(
					YOHO._('account_please_fill_in', '請輸入%s'),
					((!_mobile) ? YOHO._('account_username', '手提電話號碼') : 
					 (!_email) ? YOHO._('account_email', '電郵地址') : YOHO._('account_password', '密碼'))
				)).show().delay(2000).fadeOut();
				return false;
			}
			
			var mobile_ok = YOHO.check.isMobile(_mobile, _ccc);
			var email_ok = YOHO.check.isEmail(_email);
			if(!mobile_ok || !email_ok)
			{
				$msg.text(sprintf(
					YOHO._('account_format_invalid', '%s格式有誤，再修改一下哦！'),
					((!mobile_ok) ? YOHO._('account_username', '手提電話號碼') : YOHO._('account_email', '電郵地址'))
				)).show().delay(2000).fadeOut();
				return false;
			}
			
			if(_pwd.length < 6)
			{
				$msg.text(YOHO._('account_password_too_short', '密碼長度最少6個字哦！')).show().delay(2000).fadeOut();
				return false;
			}
			
			if(_pwd != _pwd2)
			{
				$msg.text(YOHO._('account_password_mismatch', '您兩次輸入的密碼不一致！')).show().delay(2000).fadeOut();
				return false;
			}
			
			$btn.html('<img src="/images/loading.gif" /> ' + YOHO._('account_submitting', '提交中'));
			
			$.ajax({
				url: '/ajax/ajaxuser_reg.php',
				type: 'post',
				data: {
					'ccc': _ccc,
					'mobile': _mobile,
					'email': _email,
					'password': _pwd,
					'password_': _pwd2,
					'reference': _ref
				},
				dataType:'json',
				success: function(data) {
					if(data.status == 1)
					{
						if(data.redirect) {
							location.href = data.redirect;
						}else if(data.send_verify_sms == 1) { // Pop up SMS verify BOX
							$('#sms_verify').css('right', 0);
							var $send_phone = $('#sms_verify').find('input[name=send_phone]');
							$send_phone.val('+'+_ccc+'-'+_mobile);
							YOHO.account.smsverify(_mobile, _ccc);
						} else {
							location.href = '/user.php';
						}
					}
					else
					{
						$btn.html(YOHO._('account_agree_tos_and_register_btn', '同意使用條款並註冊'));
						$msg.text(data.error).show().delay(3000).fadeOut();
					}
				},
				error: function(xhr) {
					$btn.html(YOHO._('account_agree_tos_and_register_btn', '同意使用條款並註冊'));
					YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
				}
			});
		});
		
		$form.find('input[name=mobile],input[name=email],input[name=password],input[name=password2]').keyup(function (evt) {
			if (evt.which == 13)
			{
				$form.find('a.btn-css3').trigger('click');
			}
		}).keydown(function(evt) {
			if (evt.which == 13)
			{
				evt.preventDefault();
			}
		});
	},
	
	// 選擇找回密碼方式
	forgetPassword: function() {
		var _this = this;
		var $list = $('#find-list');
		var $msg = $list.find('li.msg em');
		
		YOHO.account.setupTelInput($list.find('input[name="mobile"]'));
		
		$list.find('.both-forget').click(function(e) {
			$('#zixun-box').show();
		});
		$list.find('.btn-next').click(function(e) {
			var mobile = $('#find-list input[name="mobile"]').val();
			var email = $('#find-list input[name="email"]').val();
			var $input = (mobile != "" || (mobile == "" && email == "")) ? $('#find-list input[name="mobile"]') : $('#find-list input[name="email"]');
			var type = (mobile != "" || (mobile == "" && email == "")) ? "mobile" : "email";
			var value = (mobile != "" || (mobile == "" && email == "")) ? mobile : email;
			var ccc = (type == 'mobile') ? $input.intlTelInput('getSelectedCountryData').dialCode : '';
			
			e.stopPropagation();
			
			
			if (((type == 'mobile') && (!YOHO.check.isMobile(value, ccc))) ||
				((type == 'email') && (!YOHO.check.isEmail(value))))
			{
				
				$msg.text(sprintf(
					YOHO._('account_format_invalid', '%s格式有誤，再修改一下哦！'),
					((type == 'mobile') ? YOHO._('account_username', '手提電話號碼') : YOHO._('account_email', '電郵地址'))
				)).show().delay(2000).fadeOut();
				return false;
			}			
			
			var postParam = {
				'act': 'checkUser',
				'type': type,
				'val': value
			};
			
			if (type == 'mobile')
			{
				postParam.ccc = ccc;
			}

			$.ajax({
				url: '/ajax/findpwd.php',
				type: 'post',
				dataType: 'json',
				data: postParam,
				success: function(data) {
					if(data.status == 1)
					{
						_this.findByEmail(data.email);
					}
					else if (data.status == 2)
					{
						var dialog = YOHO.dialog.creat({
							'content': '<div style="font-size: 14px;">' +
											YOHO._('account_reset_password_use_default', '您從未登入過，請使用預設密碼「yohohongkong」登入') +
										'</div>'
						});
						dialog.button({
							name: YOHO._('account_reset_password_back_to_login', '返回登入'),
							focus: true,
							callback: function () {
								$('#password-box').find('a.closed').trigger('click');
							}
						});
					}
					else if (data.status == 3)
					{
						$msg.text(YOHO._('account_reset_password_requires_email', '帳戶已認證，請輸入電子郵件地址')).show().delay(3000).fadeOut();
					}
					else
					{
						$msg.text(data.error).show().delay(3000).fadeOut();
					}
				},
				error: function(xhr) {
					YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
				}
			});
		});
		
		$list.on('keypress','input',function(event) {
			// ASCII cod 13 = Enter
			if (event.which == 13)
			{
				event.preventDefault();
				
				$(event.target).closest('li').find('a.graybtn').trigger('click');
			}
		});
	},
	
	// 第2步
	findByEmail: function(email) {
		var $list = $('#find-list');
		var $box = $('#find-step2');
		var $step = $('#password-box').find('ul.find-step li');
		var pos1 = email.indexOf("@");
		var pos2 = email.lastIndexOf(".");
		var sec1 = email.substring(1,pos1);
		var sec2 = email.substring(pos1+2,pos2);
		var sec1_length = sec1.length;
		var sec2_length = sec2.length;
		var new_sec1 = "";
		var new_sec2 = "";
		for (var i=0; i<sec1_length;i++) {
			new_sec1 += "*"; 
		}
		for (var i=0; i<sec2_length;i++) {
			new_sec2 += "*"; 
		}
		var processed_email = email.replace(sec1,new_sec1);
		processed_email = processed_email.replace(sec2,new_sec2);
		var html = '<li class="msg"><em></em></li>' +
					'<li class="find-info">' +
						sprintf(YOHO._('account_reset_password_email_sent_to', '系統已發送驗證電郵到 %s'),"" ) +
						'<div>' + processed_email + '</div>' +
					'</li>' +
					'<li class="last">' +
						'<a href="http://'+email.replace(/.*@/,'')+'" target="_blank" class="btn-css3">' +
							YOHO._('account_reset_password_check_mailbox', '去查看郵件') +
						'</a>' +
						'<a href="javascript:;" class="cancel">' +
							YOHO._('account_reset_password_back_to_edit', '返回修改信息') +
						'</a>' +
					'</li>';
		
		$list.animate({'left': -510}, 300, 'easeOutExpo');
		$box.html(html).animate({'left':0}, 300, 'easeOutExpo');
		$step.removeClass('on').eq(2).addClass('on');
		
		$box.find('a.cancel').click(function() {
			$box.animate({'left': 510}, 600, 'easeOutExpo');
			$list.animate({'left': 0}, 600, 'easeOutExpo');
			$step.removeClass('on').eq(0).addClass('on');
		});
	},
	
	// 第三步
	resetPassword: function() {
		var $box = $('#find-step3');
		var $pwd = $box.find('input[name="new_password"]');
		var $pwd2 = $box.find('input[name="comfirm_password"]');
		var $msg = $box.find('li.msg em');
		
		$box.find('a.btn-css3').click(function() {
			var $btn = $(this);
			
			if($btn.find('img').length > 0)
			{
				return false;
			}
			
			var _pwd = $.trim($pwd.val());
			var _pwd2 = $.trim($pwd2.val());
			
			if(_pwd.length < 6)
			{
				$msg.text(YOHO._('account_password_too_short', '密碼長度最少6個字哦！')).show().delay(2000).fadeOut();
				return false;
			}
			
			if(_pwd != _pwd2)
			{
				$msg.text(YOHO._('account_password_mismatch', '您兩次輸入的密碼不一致！')).show().delay(2000).fadeOut();
				return false;
			}
			
			$btn.html('<img src="/images/loading.gif" /> ' + YOHO._('account_submitting', '提交中'));
			
			$.ajax({
				url: '/ajax/findpwd.php',
				type: 'post',
				data: {
					'act': 'resetPassword',
					'uid': $('input[name="uid"]').val(),
					'code': $('input[name="code"]').val(),
					'new_password': _pwd,
					'confirm_password': _pwd2
				},
				dataType: 'json',
				success: function(data) {
					if(data.status == 1)
					{
						YOHO.dialog.success(YOHO._('account_reset_password_success', '修改密碼成功，請重新登入！'));
						setTimeout(function() {
							// location.href = '/user.php?act=login';
							location.href = '/';
						}, 2000);
					}
					else
					{
						$btn.html(YOHO._('account_reset_password_btn', '重設密碼'));
						var msg = data.error || YOHO._('account_reset_password_failed', '修改密碼失敗，請稍後再試。');
						YOHO.dialog.warn(msg);
					}
				},
				error: function(xhr) {
					$btn.html(YOHO._('account_reset_password_btn', '重設密碼'));
					YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
				}
			});
		});
		$box.on('keypress','input',function(event) {
			// ASCII cod 13 = Enter
			if (event.which == 13)
			{
				event.preventDefault();
				
				$box.find('a.btn-css3').trigger('click');
			}
		});
	},

	// SMS Verify
	smsverify: function(_mobile, _ccc) {
		var $box = $('#sms_verify');
		var $msg         = $box.find('li.msg em');
		var $send_phone  = $box.find('input[name="send_phone"]');
		var $verify_code = $box.find('input[name="verify_code"]');
		var $btn_send    = $box.find('button[name="send_verify_code"]');
		var $btn_check   = $box.find('#check_verify_code');
		$btn_send.countdownButton({
			seconds:180,
			beforeClick: btnSendbeforeClick,
			afterClick: btnSendafterClick
		});
		function btnSendbeforeClick() {
			$btn_send.addClass('graybtn');
			var res = true;
			$.ajax({
				url: '/ajax/verify_sms.php',
				type: 'POST',
				data: {
					'ccc': _ccc,
					'mobile': _mobile,
					'send_phone': $send_phone.val(),
					'act': 'send_sms'
				},
				async: false,
				dataType:'json',
				success: function(data) {
					if(data.status == 3) //Not open SMS Verify
					{
						$btn_send.html(YOHO._('user_send_verify', '發送驗證碼'));
						$btn_send.removeClass('graybtn');
						YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。'));
						res = false;
						setTimeout(function(){ location.href = '/user.php'; }, 3000);
					}
					if(data.status == 1)
					{
						// console.log(data);
					}
					else
					{
						$btn_send.html(YOHO._('user_send_verify', '發送驗證碼'));
						$btn_send.removeClass('graybtn');
						$msg.text(data.error).show().delay(3000).fadeOut();
						res = false;
					}
				},
				error: function(xhr) {
					$btn_send.html(YOHO._('user_send_verify', '發送驗證碼'));
					$btn_send.removeClass('graybtn');
					YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
					console.log(xhr);
					res = false;
				}
			});
			return res;
		};

		function btnSendafterClick() {
			$btn_send.removeClass('graybtn');
		}

		$('#check_verify_code').click(function(){
			$btn = $(this);
			if($btn.find('img').length > 0)
			{
				return false;
			}
			$btn.html('<img src="/images/loading.gif" /> ' + YOHO._('account_submitting', '提交中'));
			$.ajax({
				url: '/ajax/verify_sms.php',
				type: 'POST',
				data: {
					'ccc': _ccc,
					'mobile': _mobile,
					'send_phone': $send_phone.val(),
					'verify_code': $verify_code.val(),
					'act': 'verify_sms'
				},
				dataType:'json',
				success: function(data) {
					if(data.status == 3) //Not open SMS Verify
					{
						$btn.html(YOHO._('user_verify', '確定'));
						YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。'));
						setTimeout(function(){ location.href = '/user.php'; }, 3000);
					}
					if(data.status == 1)
					{
						YOHO.dialog.warn(YOHO._('user_sms_already', '你的手機已經驗證成功。'), function(){location.href = '/user.php';});
						setTimeout(function(){ location.href = '/user.php'; }, 3000);
					}
					else
					{
						$btn.html(YOHO._('user_verify', '確定'));
						$msg.text(data.error).show().delay(3000).fadeOut();
					}
				},
				error: function(xhr) {
					$btn.html(YOHO._('user_verify', '確定'));
					YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
				}
			});
		});

	}
};

$(function(){
	var $loginPic = $('#login-pic'),
	$regPic = $('#reg-pic'),
	$loginBox = $('#login-box'),
	$regBox = $('#reg-box'),
	$reg_a = $regBox.find('a.trigger-box'),
	$login_a = $loginBox.find('a.trigger-box'),
	$xieyibox = $('#xieyi-box'),
	$privacybox = $('#privacy-box'),
	$pwdbox = $('#password-box');
	
	var replaceHistory = function (act) {
		if (typeof window.history.replaceState !== 'function') return;
		try { window.history.replaceState({'act': act}, window.title, '?act=' + act); } catch (e) { }
	};

	$reg_a.click(function() {
		if($xieyibox.hasClass('show')){
			$xieyibox.find('a.iconfont').trigger('click');
		}
		// 登入
		$regBox.animate({'left':-450},300,'easeOutExpo');
		$regPic.animate({'right':-500},300,'easeOutExpo',function() {
			$loginBox.animate({'right':15},300,'easeOutExpo');
			$loginPic.animate({'left':15},300,'easeOutExpo');
		});
		
		replaceHistory('login');
		return false;
	});

	$login_a.click(function() {
		// 註冊
		$loginBox.animate({'right':-450},300,'easeOutExpo');
		$loginPic.animate({'left':-500},300,'easeOutExpo',function() {
			$regBox.animate({'left':15},300,'easeOutExpo');
			$regPic.animate({'right':0},300,'easeOutExpo');
		});
		
		replaceHistory('register');
		return false;
	});
	
	// 登錄模塊事件初始化
	YOHO.account.login();
	// 註冊模塊事件初始化
	YOHO.account.register();
	// 忘記密碼模塊事件初始化
	YOHO.account.forgetPassword();
	
	// 顯示使用條款
	$regBox.on('click','a.xieyi-trg',function() {
		if ($xieyibox.hasClass('show'))
		{
			$xieyibox.find('a.iconfont').trigger('click');
		}
		else
		{
			$xieyibox.animate({ 'right': 0 }, 300, 'easeOutExpo', function(){
				$xieyibox.addClass('show');
			});
			if ($privacybox.hasClass('show'))
			{
				$privacybox.find('a.iconfont').trigger('click');
			}
		}
	});

	$xieyibox.on('click', 'a.closed', function() {
		$xieyibox.animate({ 'right': -450 }, 300, 'easeOutExpo', function(){
			$xieyibox.removeClass('show');
		});
	});

	// 顯示隱私條款
	$regBox.on('click','a.privacy-trg',function() {
		if ($privacybox.hasClass('show'))
		{
			$privacybox.find('a.iconfont').trigger('click');
		}
		else
		{
			$privacybox.animate({ 'right': 0 }, 300, 'easeOutExpo', function(){
				$privacybox.addClass('show');
			});
			if ($xieyibox.hasClass('show'))
			{
				$xieyibox.find('a.iconfont').trigger('click');
			}
		}
	});

	$privacybox.on('click', 'a.closed', function() {
		$privacybox.animate({ 'right': -450 }, 300, 'easeOutExpo', function(){
			$privacybox.removeClass('show');
		});
	});

	// 顯示找回密碼模塊
	function showFindPwd(callback) {
		$regBox.animate({ 'left': -450 }, 300, 'easeOutExpo');
		$regPic.animate({ 'right': -500 }, 300, 'easeOutExpo');
		$loginBox.animate({ 'right': -450 }, 300, 'easeOutExpo');
		$loginPic.animate({ 'left': 0 }, 300, 'easeOutExpo', function() {
			$pwdbox.animate({ 'right': 15 }, 300, 'easeOutExpo', callback);
		});
		
		replaceHistory('get_password');
	}
	$('#set-pwd-btn').click(function(e) {
		e.preventDefault();
		showFindPwd();
	});

	// 關閉找回密碼模塊
	$pwdbox.on('click','a.closed',function(e) {
		e.preventDefault();
		$pwdbox.animate({ 'right': -550 }, 300, 'easeOutExpo', function() {
			$loginBox.animate({ 'right': 15 }, 300, 'easeOutExpo');
			$loginPic.animate({ 'left': 15 }, 300, 'easeOutExpo');
		});
		
		replaceHistory('login');
	});

	// 在線咨詢彈窗
	var $zixunBox = $('#zixun-box');
	$('#zixun-btn').click(function() {
		$zixunBox.show();
	});
	$zixunBox.on('click','a.closed',function() {
	   $zixunBox.hide(); 
	});
	
	// Switch to target page according to act in parameter
	if(location.href.indexOf('act=register') != -1)
	{
		//$login_a.trigger('click');
		$loginBox.css('right', -450);
		$loginPic.css('left', -500);
		$regBox.css('left', 15);
		$regPic.css('right', 0);
	}
	else if(location.href.indexOf('act=get_password') != -1)
	{
		if ($('#find-step3').find('input[name="uid"]').length > 0)
		{
			$('#find-list').css('left', -510);
			$('#find-step3').css('left', 0);
			$pwdbox.find('ul.find-step li').removeClass('on').eq(4).addClass('on');
			$pwdbox.find('a.closed').hide();
			YOHO.account.resetPassword();
		}
		//showFindPwd();
		$regBox.css('left', -450);
		$regPic.css('right', -500);
		$loginBox.css('right', -450);
		$loginPic.css('left', 0);
		$pwdbox.css('right', 15);
		
		replaceHistory('get_password');
	}
	else //if(location.href.indexOf('act=login') != -1)
	{
		//$reg_a.trigger('click');
		$regBox.css('left', -450);
		$regPic.css('right', -500);
		$loginBox.css('right', 15);
		$loginPic.css('left', 15);
	}
});

})(jQuery);
