/***
 * checkout.js
 *
 * Javascript file for the checkout page
 ***/

/* jshint browser: true, unused:true, undef:true */
/* globals jQuery, sprintf, console */
/* jshint -W041 */

var YOHO = YOHO || {};

YOHO.config = YOHO.config || {};

YOHO.config.disableCommonInit = true;

(function($){

YOHO.checkout = {
	countries: [],
	countries_code: [],
	address_list: [],
	address_format: [],
	seconds : 0,
	locker_list : {},
	init: function() {
		var country_opts = '';
		var order_staying_time = YOHO.common.cookie("order_staying_time");

		if(typeof order_staying_time !== "undefined" && order_staying_time != null){
			this.seconds = parseInt(order_staying_time);
		}

		this.addr_dialog_status = "usual";

		var _this = this;
		var status = $("#user_status").val();


		// 選擇送貨類型模塊
		this.shipping_type();
		// 確認送貨方式模塊
		this.shipping_method();
		// 確認付款方式模塊
		this.payment_method();
		// 優惠券，禮品券事件
		this.coupon_method();
		this.pickuppoint_method();
		this.need_removal();
		// 提交訂單
		this.submitOrder();

		/*
		$("input[name='locker_tel']").focusout(function(){
			_this.checkTextFieldInt($(this));
		})
		*/
		$("#goods_list").parent().click(function(){
			_this.checkMissingPart();
		})

		$("div.payment_method").on('click', 'li', function(){
			$selected = false;
			$items = $("#paymentbox li");
			$items.each(function(idx, li) {
				if ($(li).hasClass('current')){
					$selected = true;
				}
			});
			if ($selected){
				$('html, body').animate({scrollTop: $("#goods_list").offset().top - 100}, 1000);
			}
		})

	},

	flowStepAjax: function($step, $data, $success_function)
	{
		$data = $data || {};
		$success_function = $success_function || function(){};
		$shipping_type_code = $('input[name="shipping_type"]').val();
		$('#goods_list').addClass('disabled');
		$('input[name="payment"]').val(0);
		$.ajax({
			url : '/ajax/checkout.php',
			type: 'POST',
			cache: false,
			data: {code: $shipping_type_code, act: 'select_step', step: $step, data: $data},
			dataType: 'json',
			success: function(result) {
				if (result.status == 1)
				{
					if (result.nextStep != 'null'){
						$nextStep = result.nextStep;
					} else {
						$nextStep = 'null';
					}
					$stepName = $nextStep.stepName;
					YOHO.step = $nextStep.step;
					if($stepName == 'address') {
						_this.createAddressBox($nextStep, $shipping_type_code);
					} else if($stepName == 'shipping') {
						$('.checkout_step#shipping_method').nextAll().fadeOut().promise().done(function() {
							_this.createShippingBox($nextStep);
						});
					} else if($stepName == 'payment') {
						$('.checkout_step#payment_method').nextAll().fadeOut().promise().done(function() {
							_this.createPaymentBox($nextStep);
						});
					}
					$success_function(result);
				}
			},
			error: function(xhr) {
				YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
			}
		});
	},

	shipping_type: function()
	{
		_this = this;
		$('.shipping_type').on('click', function() {
			if($(this).hasClass('current')) return;

				$shipping_type_code = $(this).data('value');
				$(this).addClass('current').siblings().removeClass('current');
				$('input[name="shipping_type"]').val($shipping_type_code);
				$('#shipping_type_info').empty();
				$('#shipping_type_info').html($(this).children('.shipping_desc').html());
			$('.checkout_step.ship_type').nextAll().fadeOut().promise().done(function() {
				/* Call ajax to get shipping */
				_this.flowStepAjax('shipping_type');
			});
		});
	},

	createAddressBox: function($data, $shipping_type_code) {
		if($shipping_type_code == 'express') {
			$address_list = $data.list;
			$default_address = $data.default;
			/* Clear old address list */
			$('.checkout_step.address ul').empty();
			$.each($address_list, function( key, address ) {
				/* Init li */
				$li = $('<li></li>');
				/* Create hidden radio box */
				$label = $('<label class="address_radio"></label>');
				$input = $('<input type="radio" name="address_radio" class="radio" />');
				$input.val(address.id);
				$input.data('country', address.country);
				$label.append($input);
				$label.append($('<span class="checkmark"></span>'));
				/* Create address info */
				$address_info =
					'<label class="consignee title">'+ address.consignee +'</label>' +
					'<div class="address_info">' +
					'   <i class="fa fa-map-marker"></i>' + address.address
				'</div>' +
				'<div class="tel">' +
				address.mobile +
				'</div>';
				/* Create edit button */
				$edit_div = $('<div class="address_edit"></div>');
				$edit_a = $('<a href="javascript:void(0)" class="pay_bg edit_btn"></a>');
				$edit_a.prop('title', YOHO._('checkout_edit_address', '修改收貨人信息'));
				$edit_div.append($edit_a);
				$li.append($label).append($address_info).append($edit_div);
				/* Add li event */
				_this.addressLiEvent($li, address);
				/* Add into html */
				$('.checkout_step.address ul').append($li);
			});
			/* Show and add event */
			$('.checkout_step.address').fadeIn();
			$('a.new_addr').unbind('click');
			$('a.new_addr').on('click',function() {
				// 新增收貨人信息
				if($address_list.length >= 5)
				{
					YOHO.dialog.warn(YOHO._('checkout_address_error_exceed', '收貨地址數已達上限！<br>可選擇其中一個進行修改!'));
					return false;
				}
				_this.newAddrDialog();
			});
		} else if ($shipping_type_code == 'pickuppoint') {
			$('#payment_method').fadeOut();
			if($data.over_max_size == 1 ||  Object.keys($data.list).length == 0) {
				$('.over_max_size_box').show();
				$('#locker_table').empty();
				$('#locker_box').hide();
				$('#locker').fadeIn();
			} else {
				$('.over_max_size_box').hide();
				$('#locker_box').fadeIn();
				$("select[name='locker_area']").prop('disabled', true);
				$("select[name='locker_region']").prop('disabled', true);
				$("select[name='locker_area']").empty().append('<option value="0">' + YOHO._('checkout_address_please_select', '--請選擇--') + '</option>');
				$("select[name='locker_region']").empty().append('<option value="0">' + YOHO._('checkout_address_please_select', '--請選擇--') + '</option>');
				$('#locker_table').html('');
				$('#selected_locker').html('');
				$('#locker').fadeIn();
				YOHO.checkout.locker_list = $data.list;
				$.each($data.list, function($key, $value) {
					$("select[name='locker_area']").append($("<option></option>").attr("value", $key).text($key));
				});
				$("select[name='locker_area']").prop('disabled', false);

				$locker = $data.default;
				if($locker !== null && Object.keys($locker).length > 0 && $locker.city && $locker.district && $locker.edCode) {
					$("select[name='locker_area']").val($locker.city).trigger('change');
					$("select[name='locker_region']").val($locker.district).trigger('change');
					if(!$("input[name=lockerCode][value='" + $locker.edCode + "']").is(':disabled')) {
						$("input[name=lockerCode][value='" + $locker.edCode + "']").attr('checked', 'checked').trigger('change');
						$('#selected_locker').html($locker.twThrowaddress);
						$('input[name="allotRouter"]').val($locker.allotRouter);
						$('input[name="locker_consignee"]').val($locker.consignee);
						$('input[name="locker_tel"]').val($locker.tel.substring($locker.tel.indexOf('-') + 1));
					}
					if($locker.arCode) {
						$('input[name="allotRouterCode"]').val($locker.arCode);
					}
				}
			}
		}
	},

	createShippingBox: function($data) {
		$('#shipyyy').empty();
		$('#global_shipping_info').empty();
		$('.cal_free_shipping').hide();
		_this = this;
		$result = $data.data;
		//console.log($result);
		$shipping_list = $result.shipping_list;
		if($result.restricted_to_hk_goods) {
			$html =
				'<div style="padding: 10px; border: 1px solid #ff6969; background-color: #fff6bf;">' +
				'   <div style="font-size: 14px;">' +
				'       <i class="fa fa-fw fa-warning"></i>' +
				YOHO._('checkout_shipping_restricted_to_hk', '您的購物車中包含了以下產品只能送貨到香港：') +
				'   </div>';
			$.each($result.restricted_to_hk_goods, function( key, goods ) {
				$html += '   <div style="padding: 2px 2px 2px 24px;">' + goods + '</div>';
			});
			$html += '</div>';
			$('#shipyyy').append($html);

		} else if($result.shipping_list) {
			$ul = $('<ul></ul>');
			$shipping_option = $result.shipping_list.length;
			$.each($result.shipping_list, function( key, shipping ) {
				$li = $('<li class="online"></li>');
				if(shipping.support_fod == true) $li.addClass('fod');
				$shipping_box = $('<div class="shipping_box"></div>');
				$title = '<div class="shipping_title title">' +
					'<input name="source_shipping" type="radio" value="'+shipping.shipping_id+'" style="display:none;"/></span>'+
					'<input name="source_shipping_area" type="radio" value="'+shipping.shipping_area_id+'" style="display:none;"/></span>'+
					'<input name="shipping" type="radio" value="'+shipping.shipping_id+'" style="display:none;"/></span>'+
					'<input name="shipping_area" type="radio" value="'+shipping.shipping_area_id+'" style="display:none;"/></span>'+
					'<input name="cal_free_shipping" type="hidden" value="'+shipping.cal_free_shipping+'" style="display:none;"/></span>'+
					'   <div class="shipping_logo">' +
					(shipping.shipping_logo ? '<img src="'+shipping.logo+'"/>' : '') +
					'   </div>'+
					'<div class="name">'+shipping.shipping_name+'</div>'+
					'<em class="postage_fee">'+shipping.format_shipping_fee+'</em>' +
					'</div>';
				$shipping_info = '<div class="shipping_info"><span>'+shipping.shipping_desc+'</span></div>';
				$shipping_box.append($title).append($shipping_info);
				$li.append($shipping_box);
				$ul.append($li);
			});
			$fod = '<div class="cle"></div>'+
				'<div class="shipping_fee_on_delivery" style="display: none; margin-top: 8px;">'+
				'<input type="checkbox" id="shipping_fod" name="shipping_fod"> <label for="shipping_fod">'+ YOHO._('checkout_shipping_fee_on_delivery' , '運費到付')+'<span style="padding-left: 6px; color: #777;">'+
				YOHO._('checkout_shipping_fee_on_delivery_notice' , '速遞收取的運費可能會高於以上金額')+
				'</span></label></div>';
			$('#shipyyy').append($ul).append($fod);
		}
		$('#shipping_method').fadeIn();
		if ($shipping_option == 1){
			$('#shipyyy').children("ul").children("li").trigger("click");
		}
		
	},

	createPaymentBox: function($data) {
		var methodRequiredTooltip = [
			'stripegooglepay', 'stripeapplepay'
		];
		$('#paymentbox').empty();
		$('#global_payment_info').empty();
		_this = this;
		$payment_list = $data.data;
		if($payment_list) {
			$ul = $('<ul></ul>');
			$.each($payment_list, function( key, payment ) {
				if(payment.is_online != 1 ) return;
				$li = $('<li class="online"></li>');
				$li.attr('id', 'pay'+payment.pay_id);
				$payment_box = $('<div class="payment_box"></div>');
				$title = '<div class="payment_title title">' +
					'<input name="payment" type="radio" value="'+payment.pay_id+'" style="display:none;"/></span>'+
					'   <div class="payment_logo">' +
					(payment.payment_logo ? '<img src="'+payment.payment_logo+'" ' + (payment.pay_code == "payme" ? 'style="width:100px;"' : '') + '/>' : '') +
					'   </div>'+
					((methodRequiredTooltip.includes(payment.pay_code)) ? "<div><span title='"+payment.pay_name+"' class='browser-payment-tooltip "+payment.pay_code+"'>?</span></div>" : '') +
					'<div class="name">'+payment.pay_name+'</div>'+
					'<em class="postage_fee"></em>' +
					'</div>';
				$payment_info = '<div class="payment_info">'+
					'<em class="payment_fee" style="display:none;">'+payment.format_pay_fee+'</em>'+
					'<em class="payment_currency" style="display:none;" data-currency-rate="'+payment.currency.currency_rate+'" data-currency-format="'+payment.currency.currency_format+'">'+payment.currency.currency_name+'</em>'+
					payment.pay_desc+'</div>';
				$payment_box.append($title).append($payment_info);
				$li.append($payment_box);
				$ul.append($li);
			});
			$('#paymentbox').append($ul);
		} 
		if ($payment_list.length == 0) {
			YOHO.dialog.warn(YOHO._('flashdeal_addtocart_error_only_single_payment_method_allowed', '您所選擇的閃購無法使用其他支付方法優惠'));
		}
		$('#payment_method').fadeIn();
		$.each(methodRequiredTooltip, function(key, pay_code) {
			initTooltip(pay_code);
		});
	},

	need_removal: function() {
		$('input[name="need_removal"]').change(function(){
			if (this.checked) {
				// the checkbox is now checked
				$('.need_removal_list').fadeIn();
				var $shipping_type = $('.shipping_type.current').data('value');
				if($shipping_type == 'express' && $('.addr_queue li.current input:radio').data('country') == 3409) {
					$li = $('.addr_queue li.current');
					$('input[name="nr_address"]').val($.trim($li.find('.address_info').text()));
					$('input[name="nr_consignee"]').val($.trim($li.find('.consignee.title').text()));
					$('input[name="nr_phone"]').val($.trim($li.find('.tel').text()));
				}
			} else {
				// the checkbox is now no longer checked
				$('.nr_consignee input').val('');
				$('.need_removal_list').hide();
			}
		});
	},

	addressLiEvent: function($li, $address)
	{
		var _this = this;
		var $radio = $li.find('input:radio');
		var $ebtn = $li.find('a.edit_btn');

		$li.on('mouseenter',function() {
			if(!$li.hasClass('current')){
				$li.addClass('line_hover');
				if($radio.is(':checked')){
					// $ebtn.css('display', 'inline-block');
				}
			}
		}).on('mouseleave',function() {
			if(!$li.hasClass('current')){
				$li.removeClass('line_hover');
				if($radio.is(':checked')){
					// $ebtn.css('display', 'inline');
				}
			}
		}).on('click','a.edit_btn',function() {
			$(".auto_address").removeClass('show');
			$("#map").hide();
			$("#detail_address").show();
			$('.address_not_found_btn').hide();
			$('.address_not_on_list_btn').hide();

			// 編輯收貨地址
			var id = $radio.val();
			_this.editAddrDialog($address);
			$('.building_info,#next_info').addClass('show');
			if (_this.addr_dialog_status == "contact"){
				_this.addr_dialog_status = "usual";
				$('a.btn#next_info').trigger('click');
			}
		}).on('click','label',function() {
		}).on('click','input:radio',function() {
			//$(this).siblings('label').trigger('click');
			// 選擇收貨地址
			var $oli = $li.siblings();
			if($li.hasClass('current'))
			{
				return false;
			}
			if (_this.addressInfoFilled($radio.val()) == false){
				var editAddrBtn = $li.find("div.address_edit").find("a.edit_btn");
				YOHO.dialog.warn(sprintf(
					YOHO._('checkout_address_error_empty', '請輸入%s！'),
					YOHO._('checkout_address_receiver_info', '收貨人資料')
				));
				_this.addr_dialog_status = "contact";
				editAddrBtn.trigger("click");
				return false;
			}

			$radio.eq(0).prop('checked', true);
			$oli.find('input:radio').eq(0).prop('checked', false);
			_this.flowStepAjax('address', {id: $radio.val()}, function(){
				YOHO.dialog.success(YOHO._('checkout_address_updated', '地址修改成功！'));
				$('.addr_queue>li.current').removeClass('current');
				$li.addClass('current');
			});
		});
	},
	
	// 修改收貨人信息
	editAddrDialog: function(json)
	{
		var _this = this;
		
		_this.submitAddress(json);
	},
	
	// 新增收貨人信息
	newAddrDialog: function(flag)
	{
		var _this = this;
		var _id = 'address_edit';
		if (flag == 2)
		{
			_id = 'address';
			// If user don't have any address, hide cancel btn.
			$("#addr_edit_win a.graybtn").hide();
		}
		var defaultJson = {'id':'','country_id':0};
		_this.submitAddress(defaultJson);
	},

	// 收貨人信息提交驗證並提交
	submitAddress: function(editJson)
	{
		// Clear search input
		$('#pac-input').val('');
		$(".iconfont.arrow.edit_address.edit_address").show();
		$('#prev_info').removeClass('show');

		// edit_address click event
		$(".edit_address").on('click',function(){
			$(".auto_address").addClass('show');
			$("#format_address_display").html('');
			$("#map").hide();
			$("#detail_address").hide();
			$('input[name=format_address]').val('');
			$(".iconfont.arrow.edit_address.edit_address").hide();
			$('.building_info,#next_info').addClass('show');
		});
		$('input[name=building]').on('change', function(){
			$('input[name=format_address]').val('');
		});

		var _this = this;
		// Form elements
		var $box    = $('#addr_edit_win');
		var $btn    = $box.find('a.btn#save_info');
		var $btn2    = $box.find('a.btn#next_info');
		var $btn3    = $box.find('a.btn#prev_info');
		var $cancel = $box.find('a.graybtn');
		var $sel_address = $box.find('select[name=address_type]');
		var $sel_lift = $box.find('select[name=address_lift_type]');
		// Consignee elements
		var $name  = $box.find('input[name=name]');
		var $phone = $box.find('input[name=phone]');
		var $id_doc = $box.find('input[name=id_doc]');
		// Address elements
		var $address  = $box.find('input[name=format_address]'); // Complete Address
		var $floor    = $box.find('input[name=floor]'); 
		var $room     = $box.find('input[name=room]');
		var $building = $box.find('input[name=building]');
		var $google_place = $box.find('input[name=google_place]'); // Google place name
		var $phase  = $box.find('input[name=phase]');
		var $block  = $box.find('input[name=block]');
		var $street_name  = $box.find('input[name=street_name]');
		var $street_num   = $box.find('input[name=street_num]');
		var $locality     = $box.find('input[name=locality]');
		var $administrative_area  = $box.find('input[name=administrative_area]');
		var $country = $box.find('select[name=country]');
		var $zipcode = $box.find('input[name=zipcode]');
		var $address_type = $box.find('input[name=type_address]');
		var $lift_type = $box.find('input[name=type_lift]');
		// Count User staying time
		var staying_time = setInterval(function(){
			_this.seconds += 1;
		}, 1000);

		editJson = editJson || {};
		if(editJson.id != undefined && editJson.id != '') // Edit address
		{
			$('.address_not_search_btn').show();
			// 有Json信息傳入,則彈窗後賦值
			changeCountry(editJson.country);
			$.each(editJson, function(index,value){ 
				if(value && value.length > 0){
					$box.find('input[name='+index+']').val(value);
				}
			});
			
			$("#format_address_display").html($('input[name=format_address]').val());
			$('input[name=format_address]').val('');
		}
		else if(editJson.id == ''){ // New address
			$.each($("#addr_edit_win input"), function(index,input){
				input.value = '';
			});
			$(".auto_address").removeClass('show');
			$("#format_address_display").html('');
			$("#map").hide();
			$('.address_not_search_btn').show();
			$('#detail_address').show();
			$('input[name=format_address]').val('');
			// $country.mySelect();
		} else
		{
			$country.mySelect();
		}
		
		// Cancel btn event
		$cancel.on('click',function() {
			$("#address_dialog").hide();
			changeCountry(0);
			clearInterval(staying_time);
		});

		$sel_address.change(function() {
			$address_type.val($(this).val());
			if ($(this).val() == 2 || $(this).val() == 0){
				$("#sel_lift_option_3").show();
			} else {
				if ($sel_lift.val() == 3){
					$lift_type.val(0);
					$sel_lift.val(0);
				}
				$("#sel_lift_option_3").hide();
			}
		});

		$sel_lift.change(function() {
			$lift_type.val($(this).val());
		});

		// Address box next step
		$btn2.on('click',function(){
			if ($btn.hasClass('btn-loading'))
			{
				return false;
			}
			$room.val($room.val().toUpperCase());
			var _country = $country.val(),
			_administrative_area = $administrative_area.val(),
			_building = $.trim($building.val()),
			_phase = $.trim($phase.val()),
			_block = $.trim($block.val()),
			_address = $.trim($address.val()),
			_google_place = $.trim($google_place.val()),
			_street = $.trim($street_name.val()),
			_street_num = $.trim($street_num.val()),
			_locality = $.trim($locality.val()),
			_floor = $.trim($floor.val()),
			_room = $.trim($room.val()),
			_zipcode = $.trim($zipcode.val());
			// Update: only require country
			if (!_country || _country == 0)
			{
				YOHO.dialog.warn(sprintf(
					YOHO._('checkout_address_error_not_selected', '請選擇%s！'),
					YOHO._('checkout_address_country', '所在國家')
				));
				showFullAddressView();
				return false;
			}
			_province = 0;

			if (_administrative_area == '')
			{
				YOHO.dialog.warn(sprintf(
					YOHO._('checkout_address_error_empty', '請輸入%s！'),
					YOHO._('checkout_address_area', '地區')
				));
				showFullAddressView();
				return false;
			}

			if (_locality == '')
			{
				YOHO.dialog.warn(sprintf(
					YOHO._('checkout_address_error_empty', '請輸入%s！'),
					YOHO._('checkout_address_locality', '地方/市鎮')
				));
				showFullAddressView();
				return false;
			}

			if (_building == '' && _street == '' && _phase == '')
			{
				YOHO.dialog.warn(sprintf(
					YOHO._('checkout_address_error_empty', '請輸入%s！'),
					YOHO._('checkout_address_building_street_phase_block', '街道 或 屋苑 或 大廈')
				));
				showFullAddressView();
				return false;
			}

			// Require zipcode if not HK or Macau
			if (_country != 3409 && _country != 3410 && _zipcode == '')
			{
				YOHO.dialog.warn(sprintf(
					YOHO._('checkout_address_error_empty', '請輸入%s！'),
					YOHO._('checkout_address_zipcode', '郵遞區號')
				));
				showFullAddressView();
				return false;
			}
			
			$.ajax({
				url : '/ajax/address.php',
				type: 'get',
				dataType: 'json',
				data:{
					act: 'get_format_address',
					country: _country,
					sign_building: _building,
					phase: _phase,
					block: _block,
					floor: _floor,
					room: _room,
					street_name: _street,
					street_num: _street_num,
					locality: _locality,
					administrative_area: _administrative_area,
					address: _address
				},
				success:function(res){
					if(res.status == 1){
						$('#formated_address').html(res.address.address);
						//$address.val(res.address.address);
						if (_country == 3409){
							if (res.address.type && res.address.lift){
								$sel_address.val(res.address.type);
								$sel_lift.val(res.address.lift);
								$('#select_address_type').addClass('hide');
								$address_type.val(res.address.type);
								$lift_type.val(res.address.lift);
							} else {
								$('#select_address_type').removeClass('hide');
								$address_type.val("");
								$lift_type.val("");
								$sel_address.val(-1);
								$sel_lift.val(0);
							}
						} else {
							$('#select_address_type').addClass('hide');
						}
						$('#next_info,.building_info,.full_address_info').removeClass('show');
						$('.full_address_info').addClass('hide');
						$('.consignee_info,#save_info,#prev_info').addClass('show');

						if (_country != 3436){
							$('#id_doc').removeClass('show');
							$('#id_doc').addClass('hide');
						} else {
							$('#id_doc').addClass('show');
							$('#id_doc').removeClass('hide');
						}
					}
				}
			})
		})
		$btn3.on('click',function(){
			if ($btn.hasClass('btn-loading'))
			{
				return false;
			}
			$('#formated_address').html("");
			$('#next_info,.building_info').addClass('show');
			$('.full_address_info').removeClass('hide');
			if($('#format_address_display').text() == "")
				$('.auto_address').addClass('show');
			$('.consignee_info,#save_info,#prev_info').removeClass('show');
		})

		// Address box submit event
		$btn.on('click',function() {
			var _name = $.trim($name.val()),
			_country = $country.val(),
			_administrative_area = $administrative_area.val(),
			_building = $.trim($building.val()),
			_address = $.trim($address.val()),
			_google_place = $.trim($google_place.val()),
			_phase = $.trim($phase.val()),
			_block = $.trim($block.val()),
			_street = $.trim($street_name.val()),
			_street_num = $.trim($street_num.val()),
			_locality = $.trim($locality.val()),
			_floor = $.trim($floor.val()),
			_room = $.trim($room.val()),
			_zipcode = $.trim($zipcode.val()),
			_phone = $.trim($phone.val()),
			_id_doc = $.trim($id_doc.val()),
			_address_type = $.trim($address_type.val()),
			_lift = $.trim($lift_type.val()),
			params = {};
			if ($btn.hasClass('btn-loading'))
			{
				return false;
			}
			
			if (!YOHO.check.isValidName(_name))
			{
				YOHO.dialog.warn(sprintf(
					YOHO._('checkout_address_error_invalid', '請輸入正確的%s！'),
					YOHO._('checkout_address_consignee', '收貨人姓名')
				));
				return false;
			}

			// Update: only require country
			if (_country == 0)
			{
				YOHO.dialog.warn(sprintf(
					YOHO._('checkout_address_error_not_selected', '請選擇%s！'),
					YOHO._('checkout_address_country', '所在國家')
				));
				return false;
			}
			_province = 0;
			
			if (_address == '' && (_building == '' && _street == '' && _phase))
			{
				YOHO.dialog.warn(sprintf(
					YOHO._('checkout_address_error_empty', '請輸入%s！'),
					YOHO._('checkout_address_street', '詳細地址')
				));
				return false;
			}

			// Require zipcode if not HK or Macau
			if (_country != 3409 && _country != 3410 && _zipcode == '')
			{
				YOHO.dialog.warn(sprintf(
					YOHO._('checkout_address_error_empty', '請輸入%s！'),
					YOHO._('checkout_address_zipcode', '郵遞區號')
				));
				return false;
			}

			if (!checkPhoneNo(_phone, _country))
			{
				if (_country == 3436){
					YOHO.dialog.warn(sprintf(
						YOHO._('checkout_address_error_invalid', '請輸入正確的%s！'),
						YOHO._('checkout_address_cn_phone_11', '11位手機號碼')
					));
				} else {
					YOHO.dialog.warn(sprintf(
						YOHO._('checkout_address_error_invalid', '請輸入正確的%s！'),
						YOHO._('checkout_address_phone', '聯絡電話')
					));
				}
				return false;
			}
			
			if (!checkidNo(_id_doc, _country))
			{
				YOHO.dialog.warn(sprintf(
					YOHO._('checkout_address_error_invalid', '請輸入正確的%s！'),
					YOHO._('checkout_address_cn_id_doc_18', '18位內地身份證號碼')
				));
				return false;
			}

			if (_country == 3409){
				if (_address_type == "" || _address_type == -1)
				{
					YOHO.dialog.warn(sprintf(
						YOHO._('checkout_address_error_not_selected', '請選擇%s！'),
						YOHO._('checkout_address_type', '地址類型')
					));
					return false;
				}

				if (_lift == "" || _lift == 0)
				{
					YOHO.dialog.warn(sprintf(
						YOHO._('checkout_address_error_not_selected', '請選擇%s！'),
						YOHO._('checkout_address_type_lift_sel', '是否設有升降機')
					));
					return false;
				}
			}
			$btn.addClass('btn-loading');
			clearInterval(staying_time);

			if (_country != 3409){
				_address_type = 0;
				_lift = 0;
			}

			params = {
				'id':'',
				'receiver': _name,
				'phone': _phone,
				'id_doc': _id_doc,
				'street': _street,
				'street_num': _street_num,
				'zipcode':_zipcode,
				'country': _country,
				'administrative_area': _administrative_area,
				'building': _building,
				'address': _address,
				'google_place': _google_place,
				'phase': _phase,
				'block': _block,
				'locality': _locality,
				'floor': _floor,
				'room': _room,
				'address_type': _address_type,
				'lift': _lift,
				'staying_time': _this.seconds
			};
			
			if(editJson.id != undefined)
			{
				params.id = editJson.id;
			}
			
			$.ajax({
				url: '/ajax/updateaddress.php',
				type: 'post',
				data: params,
				dataType: 'json',
				success: function(data) {
					$btn.removeClass('btn-loading');
					if(data.status == 1)
					{
						// 沒有地址的用戶剛填寫完地址
						if ($('#user_status').val() == 2)
						{
							_this.flowStepAjax('shipping_type',[], function(){
								YOHO.dialog.success(YOHO._('checkout_address_updated', '地址修改成功！'));
								$("#address_dialog").hide();
								if($('input[name="address_radio"][value="'+data.obj.id+'"]').find()) {
									$('input[name="address_radio"][value="'+data.obj.id+'"]').trigger('click');
								}
							});
						}
						else
						{
							_this.flowStepAjax('shipping_type',[], function(){
								//YOHO.dialog.success(YOHO._('checkout_address_updated', '地址修改成功！'));
								$("#address_dialog").hide();
								if($('input[name="address_radio"][value="'+data.obj.id+'"]').find()) {
									$('input[name="address_radio"][value="'+data.obj.id+'"]').trigger('click');
								}
							});
						}
					}
					else if(data.status == 0 || data.status == 2)
					{
						YOHO.dialog.warn(YOHO._('checkout_address_error_too_long', '儲存失敗：收貨人信息過長'));
					}
					else if(data.status == 4)
					{
						YOHO.dialog.warn(YOHO._('checkout_address_error_exceed', '儲存失敗：地址數量超過上限'));
					}
					else if(data.status == 7)
					{
						YOHO.dialog.warn(YOHO._('checkout_address_error_invalid_format', '地址填寫錯誤'));
					}
					else if(data.status == 8)
					{
						YOHO.dialog.warn(YOHO._('checkout_address_error_register_failed', '儲存失敗：電郵地址已經存在!<br>如果您已經註冊，請直接登入。'));
					}
					else
					{
						window.location.reload();
					}
				},
				error: function(xhr) {
					$btn.removeClass('btn-loading');
					YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
				}
			});
		});

		$id_doc.focusout(function() {
			var _country = $country.val(), _id_doc = $.trim($id_doc.val());
			if (!checkidNo(_id_doc, _country))
			{
				if (_country == 3436){
					$id_doc.parent("li").find("span.red").html("* " + sprintf(
						YOHO._('checkout_address_error_invalid', '請輸入正確的%s！'),
						YOHO._('checkout_address_cn_id_doc_18', '18位內地身份證號碼')
					));
				}
			} else {
				$id_doc.parent("li").find("span.red").html("*");
			}
		});

		$phone.focusout(function() {
			var _country = $country.val(), _phone = $.trim($phone.val());
			if (!checkPhoneNo(_phone, _country))
			{
				if (_country == 3436){
					$phone.parent("li").find("span.red").html("* " + sprintf(
						YOHO._('checkout_address_error_invalid', '請輸入正確的%s！'),
						YOHO._('checkout_address_cn_phone_11', '11位手機號碼')
					));
				} else {
					$phone.parent("li").find("span.red").html("* " + sprintf(
						YOHO._('checkout_address_error_invalid', '請輸入正確的%s！'),
						YOHO._('checkout_address_phone', '聯絡電話')
					));
				}
			} else {
				$phone.parent("li").find("span.red").html("*");
			}
		});

		$("#address_dialog").show();
		$('.building_info,.consignee_info,#save_info,#next_info').removeClass('show');
		$('.full_address_info').removeClass('hide');

		function checkPhoneNo($phoneNo, $countryId) {
			var num_pattern = new RegExp('^[0-9]+$');
			$phoneNo = $.trim($phoneNo).replace(/-/g, "");
			if (num_pattern.test($phoneNo)){
				if (!YOHO.check.isMobile($phoneNo) && !YOHO.check.isTelephone($phoneNo) || ($countryId == 3436 && $phoneNo.length < 11)){
					return false;
				} else {
					return true;
				}
			} else {
				return false;
			}
		}

		function checkidNo($idNo, $countryId) {
			//var num_pattern = new RegExp('^[0-9]+$');
			$idNo = $.trim($idNo);
			if ($countryId == 3436){
				//if (!num_pattern.test($idNo) || $idNo.length < 18 || $idNo.length > 18){ // check number only and length 18
				if ($idNo.length < 18 || $idNo.length > 18){ // check length 18
					return false;
				} else {
					return true;
				}
			} else {
				return true;
			}
		}

	},

	// 省市級聯事件-觸發
	switchAreaStart: function($country, $province, $city, $area)
	{
		var _this = this;
		$country.change(function() {
			_this.switchArea($country, $province, null, function(){
				_this.switchArea($province, $city, function(){
					_this.switchArea($city, $area, null);
				});
			});
		});
		
		$province.change(function() {
			_this.switchArea($province, $city, null, function(){
				_this.switchArea($city, $area, null);
			});
		});
		
		$city.change(function() {
			_this.switchArea($city, $area, null);
		});
	},

	// 省市區聯動事件
	switchArea: function($parent, $child, selected_id, fn)
	{
		var pid = $parent.val();
		if(pid == 0)
		{
			$child.empty().append('<option value="0">' + YOHO._('checkout_address_please_select', '--請選擇--') + '</option>').mySelect();
			if (fn)
			{
				fn();
			}
		}
		else
		{
			$.ajax({
				url:'/ajax/region.php',
				type:'post',
				data: {id : pid},
				dataType: 'json',
				success:function(data) {
					var options = '<option value="0">' + YOHO._('checkout_address_please_select', '--請選擇--') + '</option>';
					$.each(data,function(i) {
						if (data[i].id == selected_id)
						{
							options += '<option value="'+this.id+'" selected="selected">'+this.name+'</option>';  
						}
						else
						{
							options += '<option value="'+this.id+'" >'+this.name+'</option>'; 
						}
					});
					$child.empty().append(options).mySelect();
					if (fn)
					{
						fn();
					}
				}
			});
		}
	},

	// 確認送貨方式模塊 函數
	shipping_method: function()
	{
		//付款方式切換選擇事件
		var _this = this;
		var $ship = $('div.shipping_method');

		$ship.on('click', 'li', function() {
			var $this = $(this);
			if(!$this.hasClass('current') && !$this.hasClass('disable'))
			{
				$ship.find('input[name=shipping]').prop('checked', false); //選中之前先清空所有按鈕選擇狀
				$this.find('input[name=shipping]').prop('checked', true); // 選中radio
				$ship.find('input[name=shipping_area]').prop('checked', false); //選中之前先清空所有按鈕選擇狀
				$this.find('input[name=shipping_area]').prop('checked', true); // 選中radio
				$this.addClass('current').siblings().removeClass('current');
				$('#global_shipping_info').html($this.find('.shipping_info').html());
				if($this.find('input[name=cal_free_shipping]').val() == 'true') $('.cal_free_shipping').show();
				else $('.cal_free_shipping').hide();
				// 如果支援運費到付而且需要付運費的話，顯示運費到付選項
				if (($(this).hasClass('fod')) && (_this.parsePrice($(this).find('em.postage_fee').text()) > 0))
				{
					$('div.shipping_fee_on_delivery').show();
					$('input[name="shipping_fod"]').prop('disabled', false);
				}
				else
				{
					$('div.shipping_fee_on_delivery').hide();
					$('input[name="shipping_fod"]').prop('disabled', true);
					$('input[name="shipping_fod"]').prop('checked', false);
				}
				_this.updatePrice();
				_this.flowStepAjax('shipping', {
					shipping_id: $this.find('input[name=source_shipping]').val(),
					shipping_area_id: $this.find('input[name=source_shipping_area]').val()
				}, function () {

				});
			}
		});
		
		$('input[name="shipping_fod"]').change(function () {
			_this.updatePrice();
		});
	},

	// 確認付款方式模塊 函數
	payment_method: function()
	{
		//付款方式切換選擇事件
		var _this = this;
		var $pay = $('div.payment_method');

		$pay.on('click', 'li', function() {
			var $this = $(this);
			if(!$this.hasClass('current') && !$this.hasClass('disable'))
			{
				$pay.find('input[name=payment]').prop('checked', false); // deselect all
				$this.find('input[name=payment]').prop('checked', true); // select this
				$('#global_payment_info').html($this.find('.payment_info').html());
				
				$this.addClass('current').siblings().removeClass('current');
				
				var code = $("input.coupon_txt").data("promote");
				if (code != "") {
					$("input.coupon_txt").data("promote", "");
					YOHO.checkout.coupon_method().useCoupon(code);
				} else {
					_this.updatePrice();
				}
				$('#goods_list').removeClass('disabled');
			}
		});
	},

	pickuppoint_method: function() {
		var _this = this;

		$('body').on('change', 'input[name="lockerCode"]',  function(){
			$('#selected_locker').html('');
			$('input[name="allotRouter"]').val($(this).data('ar'));
			$('input[name="allotRouterCode"]').val('');
			$('.locked_time').hide();
			$(this).closest('tr').find('.locked_time').show();
			$area = $("select[name='locker_area']").val();
			$region =$("select[name='locker_region']").val();
			$ar = $(this).data('ar');
			$list = YOHO.checkout.locker_list[$area][$region][$ar];
			if($(this).data('hot') == 1) {
				// $("#locker select, #locker input").prop('disabled', true);
				var dialog = null;
				$content = $('<div></div>');
				$content.append('<h1>'+YOHO._('checkout_locker_second_select', '為確保您的訂單能順利送達，請選擇一個後備智能櫃')+'</h1>');
				$select = $("<select name='ar_select'></select>");
				$select.empty();
				$.each($list, function($key, $value) {
					if($value.isHot == 0 && $value.isBlackList == 0) {
						$select.append($("<option></option>").attr("value", $value.edCode).text($value.twThrowaddress));
					}
				});
				$content.append($select);
				dialog = YOHO.dialog.creat({id:'confirm',content:$content.html()});
				dialog.button({
					name: YOHO._('global_OK', '確定'),
					focus: true,
					callback: function() {
						// $("#locker select, #locker input").prop('disabled', false);
						$("input[name='allotRouterCode']").val($("select[name='ar_select']").val());
					}
				},{
					name: YOHO._('global_Cancel', '取消'),
					callback: function() {
						$('.locked_time').hide();
						$('input[name="lockerCode"]').prop('checked', false);
						// $("#locker select, #locker input").prop('disabled', false);
						$('#payment_method').fadeOut();
					}
				});
			}
			$('#selected_locker').html($(this).parent().text());
			if($(this).data('shipping_id') > 0  && $(this).data('shipping_area_id') > 0) {
				$('input[name=shipping]:checked').val($(this).data('shipping_id'));
				$('input[name=shipping_area]:checked').val($(this).data('shipping_area_id'));
			}
			_this.flowStepAjax('address', {shipping_id: $('input[name=shipping]:checked').val(), shipping_area_id: $('input[name=shipping_area]:checked').val()});

		});

		$('body').on('change', "select[name='locker_area']", function () {
			$this = $(this);
			$("select[name='locker_region']").empty().append('<option value="0">' + YOHO._('checkout_address_please_select', '--請選擇--') + '</option>');
			$("select[name='locker_region']").prop('disabled', true);
			if($this.val() == 0 ) {
				var table = $('#locker_table');
				table.html('');
				return;
			}
			$region_list = YOHO.checkout.locker_list[$this.val()];
			$.each($region_list, function($key, $value) {
				$("select[name='locker_region']").append($("<option></option>").attr("value", $key).text($key));
			});
			$("select[name='locker_region']").prop('disabled', false);
		});

		$('body').on('change', "select[name='locker_region']", function () {
			$this = $(this);
			var table = $('#locker_table');
			table.html('');
			if($this.val() == 0 )return;
			$chose_list = $region_list[$this.val()];
			$lockers = [];
			$.each($chose_list, function($k, $v) {
				$.each($v, function($key, $value) {
					var row = $('<tr>');
					var remarks = '';
					if($value.shipping_desc){
						remarks = '&nbsp;<span class="pickup_remarks">'+$value.shipping_desc+'</span>';
					}
					$icon = ($value.shop_icon) ? "<img class='pickup_shop_icon' src='"+$value.shop_icon+"' />" : "";
					
					$row_html = '<div class="locker_label" data-over_size_lang="'+YOHO._('checkout_pickuppoint_over_size', '此提貨點不支援你所選購產品的尺寸')+'"><label><input data-hot="'+$value.isHot+'" data-shipping_area_id="'+$value.shipping_area_id+'" data-shipping_id="'+$value.shipping_id+'" data-ar="'+$value.allotRouter+'" type="radio" name="lockerCode" class="lockerCode" value="'+$value.edCode+'">'+$icon+$value.twThrowaddress+remarks+'</label>';
					if($value.isHot == 1) {
						$row_html += '<br><span class="isHot">'+YOHO._('checkout_locker_is_hot', '此櫃為熱門選擇或需要等候較長時間')+'</span>';
					}
					$row_html += '</div>';
					$time_html = '';
					if($value.workday) $time_html  += YOHO._('checkout_locker_workday', '星期一至星期五') +' : '+ $value.workday + '</br>';
					if($value.saturday) $time_html += YOHO._('checkout_locker_saturday', '星期六') +' : '+ $value.saturday + '</br>';
					if($value.sunday) $time_html   += YOHO._('checkout_locker_sunday', '星期日') +' : '+ $value.sunday + '</br>';
					if($value.holiday) $time_html  += YOHO._('checkout_locker_holiday', '公眾假期') +' : '+ $value.holiday + '</br>';
					var td3 = $('<div class="locked_time"></div>').append($time_html);
					var td1 = $('<td>').html($row_html);

					if($value.isBlackList == 1) {
						td1.find('input[name="lockerCode"]').attr('disabled', 'disabled');
						td1.find('.locker_label').append('<br><span class="isFull">'+YOHO._('checkout_locker_is_black_list', '此櫃暫時無法選取')+'</span>');
					}
					if($value.over_max_size == 1) {
						td1.find('input[name="lockerCode"]').attr('disabled', 'disabled');
						td1.find('.locker_label').addClass('over_size');
					}
					td1.find('.locker_label').append(td3);
					row.append(td1);
					table.append(row);
				});
			});
			//$("#locker_m").append(table);
		});
	},

	// 優惠券/禮品券模塊 函數
	coupon_method: function()
	{
		var _this = this;
		var $hd = $('.coupon_integral');
		var $hd_li = $('.coupon_integral');
		var $bd_li = $('.coupon_integral');
		var $integ = $bd_li.find('input[name=integral]');
		var $coupon_txt = $bd_li.find('input.coupon_txt');
		var $integ_txt = $integ.next('input.integral_txt');
		var $totalPrice = $('#total_price');
		var $coupon_used = $totalPrice.siblings('p.coupon_offset_value');
		var $integ_used = $totalPrice.siblings('p.integral_offset_value');

		// 選擇優惠類型信息切換觸發事件
		$hd.on('click','li',function() {
			var $this = $(this);
			var i = $this.index();
			if (!$this.hasClass('use'))
			{
				var visible = $bd_li.filter(':visible');
				if (visible.attr('status'))
				{
					visible.find('.coupon_info').text('您所修改的內容還沒有使用，放棄本次修改請將此修改面板關閉，將保留已使用優惠信息').show();
				}
				else
				{
					$this.addClass('click').siblings().removeClass('click').filter('.use').addClass('no_bevel');
					$bd_li.eq(i).show().siblings(":not('.use')").hide();
				}
			}
		}).on('click','a.modify_btn',function() {
			// 修改使用
			var $li = $(this).parents('li');
			var i = $li.index();
			$li.addClass('click').siblings('.use').addClass('no_bevel');
			$bd_li.eq(i).attr('status','editing').show().find('.coupon_info').hide();
			return false;
		}).on('click','a.del_btn',function() {
			$(".goods_list .content .coupon .coupon_extra_msg").html('');
			// 取消使用
			var $this = $(this);
			$this.parents('li').removeClass('use');
			if ($this.hasClass('coupon_cancel'))
			{
				$this.parent('div.using_coupon').remove();
				useCoupon();
			}
			else
			{
				$this.parent('.use_success').hide();
				$integ.val(0);
				$integ_txt.val('');
				$integ_used.hide().find('span').text(0);
				_this.updatePrice();
			}
			return false;
		});
		
		// 優惠信息面板關閉事件
		$bd_li.on('click','a.close_btn',function() {
			var $parent_li = $(this).parents('li');
			$parent_li.hide();
			$hd_li.each(function() {
				var $li = $(this);
				if ($li.hasClass('use'))
				{
					$li.removeClass().addClass('use');
				}
				else
				{
					$li.removeClass();
				}
			});
			//status標識再次編輯狀態,如關閉信息面板,取消編輯狀態標識
			if($parent_li.attr('status'))
			{
				$parent_li.removeAttr('status');
			}
		}).on('click','a.coupon_use_btn',function(){
			// 取得用戶輸入的優惠券號碼
			var code = $coupon_txt.val();
			if (code.length < 1)
			{
				$coupon_txt.nextAll('.coupon_info').text(
					YOHO._('coupon_error_empty', '請輸入優惠券號碼')
				).show().delay(2600).fadeOut();
				return;
			}
			// 必須是大寫
			code = code.toUpperCase();
			// 使用優惠券
			useCoupon(code);
		}).on('click','a.integral_btn',function(){
			// 使用積分
			useIntegral();
		});
		
		$coupon_txt.keypress(function(event) {
			if (event.which == 13)
			{
				event.preventDefault();
				$bd_li.find('a.coupon_use_btn').trigger('click');
			}
		});
		
		$integ_txt.keypress(function(event) {
			if (event.which == 13)
			{
				event.preventDefault();
				useIntegral();
			}
		});
		
		// 優惠券使用方法
		function useCoupon(code)
		{
			var codeList = [];
			$('.applied_coupon input[name="coupon[]"]').each(function (){
				codeList.push($(this).val());
			});
			
			if (code)
			{
				codeList.push(code);
			}
			
			// No coupon used
			if (codeList.length === 0)
			{
				$coupon_txt.parent('.use_success').hide();
				$coupon_txt.val('');
				$(".coupon_promote").remove();
				//$coupon_used.hide();
				_this.updatePrice();
				return;
			}

			$(".coupon_promote").get().map(function(v) {
				if (codeList.indexOf($(v).data("code").toUpperCase()) === -1) {
					$(v).remove();
				}
			})
			
			$.ajax({
				type: 'post',
				url: '/ajax/coupon_use.php',
				data: {'code': codeList},
				dataType: 'json',
				success: function(data) {
					if (data.status == 0)
					{
						// 此優惠劵無效
						//$coupon_txt.nextAll('.coupon_info').text(data.error).show().delay(2600).fadeOut();
						$coupon_txt.nextAll('.coupon_info').html(data.error).show();
					}
					else
					{
						$coupon_txt.nextAll('.coupon_info').html('');
						$('.applied_coupon').empty();
						$.each(data.coupon_list, function (coupon_id, coupon_code) {
							var coupon_label = $('<div class="using_coupon"><span class="number" style="width:147px;">' + coupon_code +
												'<input name="coupon[]" type="hidden" value="' + coupon_code + '">' +
												'</span><a href="javascript:void(0)" class="pay_bg del_btn coupon_cancel"><i class="fa fa-remove"></i></a></div>');
							$('.applied_coupon').append(coupon_label);
						});
						
						var cash_value = _this.parsePrice(data.coupon_value);
						var total = _this.parsePrice($totalPrice.attr('totalPrice'))||0,
							postage = _this.parsePrice($('div.shipping_method li.current em.postage_fee').text())||0,
							integral = _this.parsePrice($integ_used.find('span').text())||0;
						var price = total+postage;
						if (integral > 0)
						{
							price -= integral;
						}

						$coupon_txt.val('');
						//$coupon_txt.parent('li').hide();
						var $coupon_hdli = $hd_li.eq($coupon_txt.parent('li').index());
						$coupon_hdli.removeClass();
						$coupon_hdli.find('div.use_success').show();
						$coupon_used.show();
						_this.updatePrice();
						$.each(data.promote_list, function (promote_code, promote_id) {
							if (promote_id == 1 && $("div.coupon_promote[data-code=" + promote_code + "]").length == 0) {
								$html = $("<div class='coupon_promote' data-code='" + promote_code + "'>" +
												"<span class='promote_title'>" + YOHO._("checkout_insert_the_club_id", "請輸入你的 The Club 會員帳號") + "</span>" +
												"<input class='txt txt_long promote_txt' name='theClubId' type='text' maxlength='10'>" +
												"<div id='theClubHint'>" + YOHO._("checkout_hint_the_club_id", "會員帳號為8字開頭的10位數字") + "</div>" +
												"<ul id='theClubTerms'>" + YOHO._("checkout_terms_the_club_id", "The Club Terms") + "</ul>" +
											"</div>");
								$html.find("input[name=theClubId]").on("input", function(e) {
									var val = $(e.target).val();
									var error = false;
									$("#theClubHint").removeClass("error");
									$(e.target).removeClass("error");
									if (val.length != 0) {
										if (!/^8[0-9]{9}$/.test(val)) {
											error = true;
										}
									}
									if (error) {
										$(e.target).addClass("error");
										$("#theClubHint").addClass("error");
									}
								})
								$(".goods_list .content .coupon").append($html);
							}
						});
						if ( typeof data.extra_message !== 'undefined' && data.extra_message != '') {
							$(".goods_list .content .coupon .coupon_extra_msg").html(data.extra_message);
						}
					}
				}
			});
		}

		//積分使用方法
		function useIntegral()
		{
			var $tip = $integ_txt.next('.coupon_info');
			var integral = $integ_txt.val();
			var total = _this.parsePrice($totalPrice.attr('totalPrice'))||0;
			var shipping = _this.parsePrice($('div.shipping_method li.current em.postage_fee').text())||0;
			var codeList = [];
			$('.applied_coupon input[name="coupon[]"]').each(function (){
				codeList.push($(this).val());
			});
			
			var callback = function() {
				var index = $integ.parent('li').index();
				$integ_used.show();
				successfullyUsed(index, integral);
				_this.updatePrice();
			};
			$.ajax({
				url : '/ajax/integral.php',
				type: 'post',
				cache: false,
				data: {
					'integral' : integral,
					'total'    : total,
					'shipping' : shipping,
					'coupon'   : codeList
				},
				dataType: 'json',
				success: function(result) {
					if (result.status == 1) {
						callback();
					} else if (result.status == 2) {
						$tip.text(YOHO._('checkout_use_points_error_not_numeric', '請輸入正確的積分數值')).show().delay(2600).fadeOut();
	   				 	return false;
					} else if (result.status == 3) {
						$tip.text(YOHO._('checkout_use_points_error_out_of_range', '您輸入的積分超出有效範圍')).show().delay(2600).fadeOut();
						return false;
					} else if (result.status == 4) {
						YOHO.dialog.confirm(YOHO._('checkout_use_points_over_order_amount', '您使用的積分抵消面額大於訂單總價，確定使用嗎？'), callback);
					}
				},
				error: function(xhr) {
					YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
				}
			});
		}

		//使用成功後狀態
		function successfullyUsed(index, val)
		{
			//$bd_li.eq(index).hide();
			//$hd_li.eq(index).removeClass().addClass('use').find('div.use_success').show().find('span.number').text(val);
		}

		return {
			useCoupon: function(code) {
				useCoupon(code);
			}
		}
	},

	//處理價錢
	parsePrice: function(formatted_price)
	{
		if (formatted_price == null || formatted_price == undefined)
		{
			return 0;
		}
		if (formatted_price.toFixed)
		{
			return formatted_price; // it is a number already
		}
		if (formatted_price.length == 0)
		{
			return 0;
		}
		if (formatted_price.indexOf('%') === formatted_price.length - 1)
		{
			return 0; // don't process percentage here
		}
		var price = parseFloat(formatted_price);
		if (isNaN(price))
		{
			var matches = (formatted_price.match(/\-?\d+[,\.]?\d*/));
			if ($.isArray(matches))
			{
				price = parseFloat(matches[0]);
			}
			else
			{
				if (console && typeof console.log == 'function')
				{
					console.log('Warning: "' + formatted_price + '" cannot be parsed as price');
				}
				price = 0;
			}
		}
		return price;
	},

	//計算/更新價格
	updatePrice: function()
	{
		var shipping_fod = 0;
		if(document.getElementById("shipping_fod") && document.getElementById("shipping_fod").checked){
			shipping_fod = 1;
		}
		
		var couponcodes = [];
		$.each(document.querySelectorAll('input[name="coupon[]"]'), function (idx, couponcode) {
			couponcodes.push(couponcode.value);
		});
		$.ajax({
			url : '/ajax/checkout.php',
			type: 'post',
			cache: false,
			data: {
				'act'           : 'calculate_amount',
				'total'         : $('#total_price').attr('totalPrice'),
				'shipping'      : $('div.shipping_method li.current input[name="shipping"]').val(),
				'shipping_area' : $('div.shipping_method li.current input[name="shipping_area"]').val(),
				'payment'       : $('div.payment_method li.current input[name="payment"]').val(),
				'coupon'        : couponcodes,
				'integral'      : $('input.integral_txt').val(),
				'address'       : $('input[name="address_radio"]').val(),
				'shipping_fod'  : shipping_fod,
				'shipping_type' : $('.shipping_type.current').data('value')
			},
			dataType: 'json',
			success: function(result) {
				if (result.status == 1)
				{
					var total = result.total;
					var incl_shipping_text = YOHO._('checkout_shipping_fee_included', '已包含運費');
					if ($('input[name="shipping_fod"]').prop('checked'))
					{
						incl_shipping_text = YOHO._('checkout_shipping_fee_excluded', '不包含運費');
					}
					$('.include_shipping').text('(' + incl_shipping_text + ')');
					
					payment = total.pay_fee;

					$('p.payment_fee_value span').text(payment.toFixed(2));
					$('p.payment_discount_value span').text((payment * -1).toFixed(2));
					if (payment > 0)
					{
						$('p.payment_fee_value').show();
						$('p.payment_discount_value').hide();
					}
					else if (payment < 0)
					{
						$('p.payment_fee_value').hide();
						$('p.payment_discount_value').show();
					}
					else
					{
						$('p.payment_fee_value').hide();
						$('p.payment_discount_value').hide();
					}
					// Update coupon price
					$('p.coupon_offset_value span').text(total.coupon);
					if(total.coupon > 0)$('p.coupon_offset_value').show();
					else $('p.coupon_offset_value').hide();
					// Update integral price
					$('p.integral_offset_value span').text(total.integral_money);
					// Update max integral
					$('em.total_integral').text(total.cart_max_integral);
					$('em[id="order-canuse-coupon"]').text(total.max_integral);
					// Update used integral
					$('div.use_success>span.number').text(total.integral);
					$('input[name=integral]').val(total.integral);
					$('input.integral_txt').val(total.integral);
					// Update tital price
					$('div.total_price em').text(total.amount_formated);
					// Handle shipping fee value
					$("p.shipping_fee_value span").text(total.shipping_fee);
					if (total.shipping_fee > 0) {
						$("p.shipping_fee_value").show();
					}
					else {
						$("p.shipping_fee_value").hide();

					}
					// Update China Cross-Border E-Commerce Tax
					$('p.shipping_fee_cn_ecom span').text(total.cn_ship_tax);
					if(total.cn_ship_tax  > 0)$('p.shipping_fee_cn_ecom').show();
					else $('p.shipping_fee_cn_ecom').hide();
				}
			},
			error: function(xhr) {
				YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
			}
		});
	},
	
	submitOrder: function()
	{
		var _this = this;
		$('#receiverForm_btn').click(function() {
			var btn_this = this;
			if ($(this).data('clicked'))
			{
				return;
			}

			var couponcodes = [];
			$.each(document.querySelectorAll('input[name="coupon[]"]'), function (idx, couponcode) {
				couponcodes.push(couponcode.value);
			});

			if (couponcodes.length > 0) {
				$.ajax({
					url : '/ajax/checkout.php',
					type: 'post',
					cache: false,
					data: {
						'act'           : 'check_coupon_promote',
						'coupons'       : couponcodes,
						'payment_type'  : $('div.payment_method li.current input[name="payment"]').val(),
					},
					dataType: 'json',
					success: function(result) {
						if (result.success == 0)
						{
							YOHO.dialog.warn(result.error_msg);
							return false;
						} else {
							_submitOrderBeforeCheck(btn_this);
						} 
					},
					error: function(xhr) {
						YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
					}
				});
			} else {
				_submitOrderBeforeCheck(btn_this);
			}

			return false;
			function _submitOrderBeforeCheck(btn_this) {
				if ($('input[name="shipping_type"]').val() == 'express' && $('.addr_queue li').length == 0 )
				{
					YOHO.dialog.warn(YOHO._('checkout_error_missing_address', '請填寫收貨地址！'));
					$(btn_this).html(YOHO._('checkout_submit_btn', '去付款'));
					$(btn_this).data('clicked', 0);
					return false;
				}
				if ($('input[name="shipping_type"]').val() == 'pickuppoint' && !_this.checkTextFieldInt($("input[name='locker_tel']")) )
				{
					return false;
				}
				if (!$('input[name=shipping]:checked').val())
				{
					YOHO.dialog.warn(YOHO._('checkout_error_missing_shipping', '請選擇送貨方式！'));
					$(btn_this).html(YOHO._('checkout_submit_btn', '去付款'));
					$(btn_this).data('clicked', 0);
					return false;
				}
				if (!$('input[name=payment]:checked').val())
				{
					YOHO.dialog.warn(YOHO._('checkout_error_missing_payment', '請選擇付款方式！'));
					$(btn_this).html(YOHO._('checkout_submit_btn', '去付款'));
					$(btn_this).data('clicked', 0);
					return false;
				}
				if ($('ul.switchable_content li:visible').attr('status'))
				{
					YOHO.dialog.warn('您有優惠項目修改內容沒有儲存！');
					$(btn_this).html(YOHO._('checkout_submit_btn', '去付款'));
					$(btn_this).data('clicked', 0);
					return false;
				}

				if($('input[name="shipping_type"]').val() == 'pickuppoint' && !$('input[name="lockerCode"]:checked').val()) {
					YOHO.dialog.warn(YOHO._('checkout_error_missing_address', '請填寫收貨地址！'));
					$(btn_this).html(YOHO._('checkout_submit_btn', '去付款'));
					$(btn_this).data('clicked', 0);
					return false;
				}
				if($('input[name="shipping_type"]').val() == 'pickuppoint' && !YOHO.check.isValidName($('input[name="locker_consignee"]').val()))
				{
					YOHO.dialog.warn(sprintf(
						YOHO._('checkout_address_error_invalid', '請輸入正確的%s！'),
						YOHO._('checkout_address_consignee', '收貨人姓名')
					));
					return false;
				}
				if($('input[name="need_removal"]').length > 0 && $('input[name="need_removal"]:checked').val())
				{
					if(!$('input[name=nr_consignee]').val() || !$('input[name=nr_address]').val() || !$('input[name=nr_phone]').val()) {
						YOHO.dialog.warn(YOHO._('checkout_address_error_nr_info', '請填寫除舊資料！'));
						return false;
					}
				}
				if($('input[name="theClubId"]').length > 0)
				{
					if($('input[name="theClubId"]').val() == "" || !/^8[0-9]{9}$/.test($('input[name="theClubId"]').val())) {
						YOHO.dialog.warn(YOHO._("checkout_insert_correct_the_club_id", "請輸入正確的 The Club 會員帳號"));
						return false;
					}
				}
				if(_this.seconds > 0){
					YOHO.common.cookie('order_staying_time', _this.seconds);
				}
				$(btn_this).html('<img src="/images/loading.gif" /> ' + YOHO._('checkout_submitting', '提交中'));
				$(btn_this).data('clicked', 1);
				$('#receiverForm').submit();
			}
			
		});
	},

	// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
	// lat: 22.311620, lng: 114.224104 yoho address
	// This function is using google map API to handle user address
	initAutocomplete: function() {
		var _this = this;
		// Create google map
		var map = new google.maps.Map(document.getElementById("map"), {
			center: {lat: 22.311620, lng: 114.224104},
			zoom: 17
		});
		var center = map.getCenter();
		var input = document.getElementById("pac-input");
		var v = input.value;
		input.addEventListener('change', function() {
			// setTimeout(function(){
			// 	console.log('trigger');
			// 	google.maps.event.trigger(input, 'focus');
			// 	google.maps.event.trigger(input, 'keydown', {
			// 		keyCode: 13
			// 	});
			// },300);
		});

		// $("#map_show").on('click',function(){
			// if($("#map").css('display')=='none'){
			// 	if($(".auto_address").hasClass('show')){
			// 		$(".auto_address").removeClass('show');
			// 		setTimeout(function(){
			// 			$("#map").show();
			// 			google.maps.event.trigger(map, 'resize');
			// 			map.setCenter(center); 
			// 		},300);
			// 	} else {
			// 		$("#map").show();
			// 		google.maps.event.trigger(map, 'resize');
			// 		map.setCenter(center); 
			// 	}
			// } else {
			// 	$("#map").hide();
			// 	$(".auto_address").addClass('show');
			// }
		// });

		var searchBox = new google.maps.places.SearchBox(input);
		var geocoder  = new google.maps.Geocoder;
	
	 $("#map_show").on('click',function(){
				$(input).trigger('focus');
				google.maps.event.trigger(input, 'keydown',{keyCode:13} );
		});

		// Bias the SearchBox results towards current map s viewport.
		map.addListener("bounds_changed", function() {
			searchBox.setBounds(map.getBounds());
		});
		var service = new google.maps.places.PlacesService(map);
		var marker = [];
		service.getDetails({
			placeId: "ChIJq6q6gUUBBDQRz3RxKaAlw4w"
			}, function(place, status) {
				if (status === google.maps.places.PlacesServiceStatus.OK) {
					marker = new google.maps.Marker({
					map: map,
					position: place.geometry.location
					});
				}else {
					// Google server error , try to call 
					_this.hkaddresshandle();
				}
			});

		var markers = [];
		// Listen for the event fired when the user selects a prediction and retrieve
		// more details for that place.
		searchBox.addListener("places_changed", function() {

			var places = searchBox.getPlaces();
			if (places.length == 0) {
				// can't find customer address, so the edit address button
				$('#detail_address').show();
				$('.address_not_found_btn').show();
				$('.address_not_on_list_btn').hide();
				$('.address_not_search_btn').hide();
				$("#format_address_display").html('');
				$('.building_info').removeClass('show');
				$('#next_info,.building_info,.full_address_info').removeClass('show');
				return;
			} else {
				$(".auto_address").removeClass('show');
				$('.address_not_found_btn').hide();
				$('.address_not_search_btn').hide();
				$('.address_not_on_list_btn').show();
			}
			// Clear out the old markers.
			marker.setMap(null);
			markers.forEach(function(marker) {
				marker.setMap(null);
			});
			markers = [];
			// For each place, get the icon, name and location.
			var bounds = new google.maps.LatLngBounds();

			var place = places[0];

			if (!place.geometry) {
				return;
			}
			var icon = {
				url: place.icon,
				size: new google.maps.Size(71, 71),
				origin: new google.maps.Point(0, 0),
				anchor: new google.maps.Point(17, 34),
				scaledSize: new google.maps.Size(25, 25)
			};

			// Create a marker for each place.
			markers.push(new google.maps.Marker({
				map: map,
				title: place.name,
				position: place.geometry.location
			}));
			var latlng = place.geometry.location;
			var full_address_view = false;
			geocoder.geocode({'location': latlng}, function(results, status) {
				if (status === 'OK') {
					if (results[0]) {
						var html = "";
						var address_format = [];
						var political = [];
						for (var i = 0; i < results[0].address_components.length; i++) {
							var addressType = results[0].address_components[i].types[0];
							var name = results[0].address_components[i].long_name;
							address_format[addressType] = name;
							if(addressType === 'country'){
								var country_code = results[0].address_components[i].short_name;
							}
							if(addressType === 'political'){
								political.push(name);
							}
						}

						if(political.length > 0){
							political = political.reverse();
							address_format['street_number'] = '';
							address_format['route'] = (typeof address_format['route'] == 'undefined') ? '' : address_format['route'];
							for (var index = 0; index < political.length; index++) {
								var element = political[index];
								if(index == 0){
									address_format['route'] += (address_format['route'] != "" ? " " : "") + element;
								} else {
									address_format['street_number'] += element;
									if(index != (political.length - 1)){
										address_format['street_number'] += "-";
									}
								}
							}
						}

						// Check address from order record
						var google_return_country = '';
						if(country_code){
							country_code = country_code.toLocaleLowerCase();
							//_this.countries_code[country_code]
							google_return_country = country_code;
						}

						var addr_break_building = address_format['premise']?address_format['premise']:place.name;
						var addr_break_google_place = place.name;
						var addr_break_street_name = address_format['route'];
						var addr_break_street_num = address_format['street_number'];
						var addr_break_locality = address_format['neighborhood']?address_format['neighborhood']:address_format['locality'];
						var addr_break_administrative_area = address_format['administrative_area_level_1'];
						var addr_break_phase = address_format['phase'];
						var addr_break_block = address_format['block'];
						var addr_break_google_place = place.name;

						if (country_code == 'hk'){
							var google_return_building = address_format['premise'];
							var google_return_place = place.name;
							var google_return_street = address_format['route'];
							var google_return_street_num = address_format['street_number'];
							var google_return_admin_area = address_format['administrative_area_level_1'];

								$.ajax({
									url : '/ajax/address.php',
									type: 'post',
									cache: false,
									data: {
										'act'           : 'get_verified_addr_break',
										'building'       : google_return_building,
										'place'       : google_return_place,
										'street'       : google_return_street,
										'street_num'       : google_return_street_num,
										'admin_area'       : google_return_admin_area
									},
									dataType: 'json',
									success: function(result) {
										if (result.status == 1){
											if (result.list.phase !=''){
												addr_break_phase = result.list.phase;
											}
											if (result.list.sign_building !=''){
												if (result.list.phase ==''){
													addr_break_building = result.list.sign_building;
												} else if (result.list.phase !=''){
													var temp_building_name = addr_break_building;
													if (temp_building_name.indexOf(result.list.sign_building) >= 0 && result.list.sign_building !=""){
														addr_break_building = result.list.sign_building;
													} else {
														temp_building_name = temp_building_name.replace(result.list.phase, "");
														addr_break_building = temp_building_name;
													}
												}
												
												addr_break_google_place = '';
											}
											if (result.list.block !=''){
												addr_break_block = result.list.block;
											}
											if (result.list.street_name !=''){
												addr_break_street_name = result.list.street_name;
											}
											if (result.list.street_num !=''){
												addr_break_street_num = result.list.street_num;
											}
											if (result.list.locality !=''){
												addr_break_locality = result.list.locality;
											}
											if (result.list.administrative_area !=''){
												addr_break_administrative_area = result.list.administrative_area;
											}
											$('input[name=building]').val(addr_break_building);
											$('input[name=google_place]').val(addr_break_google_place);
											$('input[name=street_name]').val(addr_break_street_name);
											$('input[name=street_num]').val(addr_break_street_num);
											$('input[name=locality]').val(addr_break_locality);
											$('input[name=administrative_area]').val(addr_break_administrative_area);
											$('input[name=phase]').val(addr_break_phase);
											$('input[name=block]').val(addr_break_block);
										} else {
											$('input[name=format_address]').val(results[0]['formatted_address']);
											$('input[name=building]').val((address_format['premise'])?address_format['premise']:place.name);
											$('input[name=google_place]').val(place.name);
											$('input[name=street_name]').val(address_format['route']);
											$('input[name=street_num]').val(address_format['street_number']);
											$('input[name=locality]').val((address_format['neighborhood']?address_format['neighborhood']:address_format['locality']));
											$('input[name=administrative_area]').val(address_format['administrative_area_level_1']);
										} 
									},
									error: function(xhr) {
										//YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
									}
								});
						}
						// Handle hide address form
						$('input[name=format_address]').val(results[0]['formatted_address']);
						$('input[name=building]').val((address_format['premise'])?address_format['premise']:place.name);
						$('input[name=google_place]').val(place.name);
						$('input[name=street_name]').val(address_format['route']);
						$('input[name=street_num]').val(address_format['street_number']);
						$('input[name=locality]').val((address_format['neighborhood']?address_format['neighborhood']:address_format['locality']));
						$('input[name=administrative_area]').val(address_format['administrative_area_level_1']);

						if(country_code){
							//$('select[name=country]').val(_this.countries_code[country_code]);
							changeCountry(_this.countries_code[country_code]);
						} else {
							//$('select[name=country]').val(0);
							changeCountry(_this.countries_code[0]);
						}
						// $('select[name=country]').val(address_format['country']);
						$('input[name=zipcode]').val(address_format['postal_code']);
						$("#format_address_display").html($('input[name=format_address]').val());
						$("#detail_address").show();
						$(".iconfont.arrow.edit_address.edit_address").show();
					
						// Hong Kong Gov API: 如果地址是香港地址, 提交到香港政府地方搜尋提高精準
						if(country_code == "hk"){
							_this.hkaddresshandle();
						}
						$('.building_info,#next_info').addClass('show');
						$('.auto_address').removeClass('show');
						if (country_code == 'cn'){
							full_address_view = true;
						}
					} 
				} else {
					full_address_view = true;
				}
				if (full_address_view){
					showFullAddressView();
				}
			});
			if (place.geometry.viewport) {
				// Only geocodes have viewport.
				bounds.union(place.geometry.viewport);
			} else {
				bounds.extend(place.geometry.location);
			}
			v = input.value;
			map.fitBounds(bounds);
			// center = map.getCenter();
		});
	},
	hkaddresshandle: function(){

		// Get search address
		var value = $("#pac-input").val();
		var _this = this;

		// Call hkgov API
		$.ajax({
			url : '/ajax/address.php',
			type: 'post',
			cache: false,
			data: {
				'act' : 'hkgov',
				'kw'  : value
			},
			dataType: 'json',
			success: function(result) {
				if (result.status == 1) // sucesss
				{
					_this.address_list = [];
					_this.address_format = [];
					// Step 1: Save google result to array 0.
					var google_address = {
						building: $('input[name=building]').val(),
						street_name: $('input[name=street_name]').val(),
						street_num: $('input[name=street_num]').val(),
						locality: $('input[name=locality]').val(),
						administrative_area: $('input[name=administrative_area]').val(),
						google_place: $('input[name=google_place]').val()
					};
					_this.address_list.push(google_address);
					_this.address_format.push($('input[name=format_address]').val());

					// Step 2: Save HK gov result to array. We only get 2 result (n=2) in api url.
					var format_address_list = result.format_address_list;
					var consignee_list      = result.consignee_list;

					for (var index = 0; index < result.format_address_list.length; index++) {
						var format_address = format_address_list[index];
						var consignee      = consignee_list[index];
						_this.address_list.push(consignee);
						_this.address_format.push(format_address);
					}

					// Step 3: Change format_address_display to redio box, and add event in this.
					$('#format_address_display').html('');
					_this.address_format.forEach(function(value, key) { 
						var radio = $('<input>').attr({
							type: 'radio', name: 'address_other_radio', value: key, id: 'address_radio_'+key
						});
						if(key == 0) radio.prop('checked',true);
						var label = $("<label>").text(value);
						var html  = $("<div>").append(radio).append(label);
						$('#format_address_display').append(html);
					});

					// Add event
					$('input[type=radio][name=address_other_radio]').change(function() {
						var address = _this.address_list[this.value];
						var address_format = _this.address_format[this.value];
						// Handle hide address form
						$('input[name=format_address]').val(address_format);
						$('input[name=building]').val(address['building']);
						$('input[name=google_place]').val(address['google_place']);
						$('input[name=street_name]').val(address['street_name']);
						$('input[name=street_num]').val(address['street_num']);
						$('input[name=locality]').val(address['locality']);
						$('input[name=administrative_area]').val(address['administrative_area']);
					});

				}
			},
			error: function(xhr) {
				return ;
			}
		});
	},

	checkTextFieldInt: function(element) {
		var input_str = element.val();
		if (!element.is(":hidden")){
			if ((YOHO.check.isMobile(input_str) && YOHO.check.isTelephone(input_str)) || input_str.match(/^\+\d+-\d+$/) || input_str.match(/^\d+-\d+$/)) {
				element.css("border-color", "");
				return true;
			} else {
				element.css("border-color", "#ff5c5c");
				YOHO.dialog.warn(sprintf(
					YOHO._('checkout_contact_no_error_invalid_tel_format', '請輸入正確的%s格式！'),
					YOHO._('checkout_address_phone', '聯絡電話').toLowerCase()
				));
				$('html, body').animate({scrollTop: $(element).offset().top - 100}, 1000);
				return false;
			}
		}
	},

	checkMissingPart: function() {
		if ($("#goods_list").hasClass("disabled")){
			if (!$('input[name="shipping_type"]').val()){ //if Shipping Type not chosen
				$('html, body').animate({scrollTop: $(".ship_type").offset().top - 100}, 1000);
				YOHO.dialog.warn(sprintf(
					YOHO._('checkout_address_error_not_selected', '請選擇%s！'),
					YOHO._('checkout_shipping_type', '送貨類型').toLowerCase()
				));
			} else if ($('input[name="shipping_type"]').val() == "pickuppoint"){ //if Shipping Type is pick up point
				if ((_this.loopFindShipSelected())){ //if Shipping Method chosen
					return;
				} else if (!$('#selected_locker').html()){ //if Shipping Address chosen
					YOHO.dialog.warn(sprintf(
						YOHO._('checkout_address_error_not_selected', '請選擇%s！'),
						YOHO._('checkout_consignee', '收貨地址').toLowerCase()
					));
					$('html, body').animate({scrollTop: $("#locker").offset().top - 100}, 1000);
				} else if ($("#payment_method").is(":visible")) { //if Payment Method chosen
					YOHO.dialog.warn(YOHO._('checkout_error_missing_payment', '請選擇付款方式！'));
					$('html, body').animate({scrollTop: $("#payment_method").offset().top - 100}, 1000);
				}
			} else if ($('input[name="shipping_type"]').val() == "store"){  //if Shipping Type is shop pick up
				if (_this.loopFindShipSelected()){ //if Shipping Method chosen
					return;
				} else if ($("#payment_method").is(":visible")) { //if Payment Method chosen
					YOHO.dialog.warn(YOHO._('checkout_error_missing_payment', '請選擇付款方式！'));
					$('html, body').animate({scrollTop: $("#payment_method").offset().top - 100}, 1000);
				}
			} else if ($('input[name="shipping_type"]').val() == "express"){  //if Shipping Type is express
				if (!$("input[name='address_radio']:checked").val()){
					YOHO.dialog.warn(sprintf(
						YOHO._('checkout_address_error_not_selected', '請選擇%s！'), 
						YOHO._('checkout_consignee', '收貨地址').toLowerCase()
					));
					$('html, body').animate({scrollTop: $("#address").offset().top - 100}, 1000);
				} else if ($("#payment_method").is(":visible")) { //if Payment Method chosen
					YOHO.dialog.warn(YOHO._('checkout_error_missing_payment', '請選擇付款方式！'));
					$('html, body').animate({scrollTop: $("#payment_method").offset().top - 100}, 1000);
				}
			}
		}
	},

	// check if Shipping Method chosen
	loopFindShipSelected: function() {
		$selected = false;
		$items = $("#shipyyy li");
		$items.each(function(idx, li) {
			if ($(li).hasClass('current')){
				$selected = true;
			}
		});
		if (!$selected){
			YOHO.dialog.warn(sprintf(
				YOHO._('checkout_address_error_not_selected', '請選擇%s！'),
				YOHO._('checkout_shipping', '送貨方式').toLowerCase()
			));
			$('html, body').animate({scrollTop: $("#shipping_method").offset().top - 100}, 1000);
			return true;
		} else {
			return false;
		}
	},

	addressInfoFilled: function($user_addr_id)
	{
		if (!$user_addr_id) return false;
		$.ajax({
			url : '/ajax/checkout.php',
			type: 'POST',
			cache: false,
			async: false,
			data: {act: 'check_address_info_filled', user_addr_id: $user_addr_id},
			dataType: 'json',
			success: function(result) {
				if (result.status == 1)
				{
					if (result.res == false){
						$res = false;
					} else {
						$res = true;
					}
				} else {
					$res = false;
				}
			},
			error: function(xhr) {
				YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
			}
		});
		return $res;
	},

};

$(function () {
	YOHO.checkout.init();
});

$('select[name="country"]').change(function(){
	checkCountry();
});
    
function changeCountry(new_country) {
	//console.log('change->'+new_country);
	$('select[name="country"]').val(new_country);
	checkCountry();
}

function checkCountry() {
	if ($('select[name="country"]').val() == '3409' || $('select[name="country"]').val() == '3410'){
		$('input[name="zipcode"]').val("");
		$('input[name="zipcode"]').prop("disabled", true);
	} else {
		$('input[name="zipcode"]').val("");
		$('input[name="zipcode"]').prop("disabled", false);
	}
}

function showFullAddressView() {
	$(".auto_address").addClass('show');
	$("#format_address_display").html('');
	$("#map").hide();
	$("#detail_address").hide();
	$('input[name=format_address]').val('');
	$(".iconfont.arrow.edit_address.edit_address").hide();
	$('.building_info,#next_info').addClass('show');
}

function initTooltip($pay_code) {
	var selector = '.browser-payment-tooltip.'+$pay_code;
	$(selector).tooltip({
		position: {
			my: 'center bottom-20',
			at: 'center left',
			using: function( position, feedback ) {
				$( this ).css( position );
				$( '<div>' )
					.addClass( 'arrow' )
					.addClass( feedback.vertical )
					.addClass( feedback.horizontal )
					.appendTo( this );
			}
		},
		content: "<div><ul class='payment-method-tooltip'><li>"+YOHO._(($pay_code+'_accept_payment_through'), '支援經瀏覽器Payment API 付款')+"</li><li>"+YOHO._(($pay_code+'_support_platform'), '注意: 瀏覽器的付款方法權限必須打開')+"</li><li>"+YOHO._(($pay_code+'_credit_card_data_remain_confidential'), '支付資料均會保密, 本公司不會儲存任何信用卡數據')+"</li><li>"+YOHO._(($pay_code+'_exchange_rate'), '所有項款皆以港幣 (HKD) 結算，結算匯率將由銀行/金融機構/ Stripe 提供')+"</li></ul></div>"
	});
}

})(jQuery);
