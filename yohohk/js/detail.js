/***
 * detail.js
 *
 * Javascript file for product page
 ***/

/* jshint -W041 */
/* jshint ignore:start */
/* Bézier curve from https://github.com/weepy/jquery.path */
;(function($){$.path={};var V={rotate:function(p,degrees){var radians=degrees*Math.PI/180,c=Math.cos(radians),s=Math.sin(radians);return[c*p[0]-s*p[1],s*p[0]+c*p[1]];},scale:function(p,n){return[n*p[0],n*p[1]];},add:function(a,b){return[a[0]+b[0],a[1]+b[1]];},minus:function(a,b){return[a[0]-b[0],a[1]-b[1]];}};$.path.bezier=function(params,rotate){params.start=$.extend({angle:0,length:0.3333},params.start);params.end=$.extend({angle:0,length:0.3333},params.end);this.p1=[params.start.x,params.start.y];this.p4=[params.end.x,params.end.y];var v14=V.minus(this.p4,this.p1),v12=V.scale(v14,params.start.length),v41=V.scale(v14,-1),v43=V.scale(v41,params.end.length);v12=V.rotate(v12,params.start.angle);this.p2=V.add(this.p1,v12);v43=V.rotate(v43,params.end.angle);this.p3=V.add(this.p4,v43);this.f1=function(t){return(t*t*t);};this.f2=function(t){return(3*t*t*(1-t));};this.f3=function(t){return(3*t*(1-t)*(1-t));};this.f4=function(t){return((1-t)*(1-t)*(1-t));};this.css=function(p){var f1=this.f1(p),f2=this.f2(p),f3=this.f3(p),f4=this.f4(p),css={};if(rotate){css.prevX=this.x;css.prevY=this.y;}
css.x=this.x=(this.p1[0]*f1+this.p2[0]*f2+this.p3[0]*f3+this.p4[0]*f4+0.5)|0;css.y=this.y=(this.p1[1]*f1+this.p2[1]*f2+this.p3[1]*f3+this.p4[1]*f4+0.5)|0;css.left=css.x+"px";css.top=css.y+"px";return css;};};$.fx.step.path=function(fx){var css=fx.end.css(1-fx.pos);if(css.prevX!=null){$.cssHooks.transform.set(fx.elem,"rotate("+Math.atan2(css.prevY-css.y,css.prevX-css.x)+")");}
fx.elem.style.top=css.top;fx.elem.style.left=css.left;};})(jQuery);
/* jshint ignore:end */

var YOHO = YOHO || {};

(function($) {

YOHO.detail = {
	
	init: function() {
		this.goodsPicSlide();
		this.updateQuantity();
		this.addCart();
		this.share();
		// If 限時特價, start the countdown timer
		if ($('#promote_countdown').length)
		{
			this.promoteCountdown();
		}
		this.responsiveVideo();
		this.sectionBoxTab();
		this.stayTuned();
		this.suggestAPrice();
		this.reportAProblem();
		this.topTag();
		this.pinglun();
		this.ping_list();
	},

	// 加入購物車
	addCart: function() {
		var $buybtn = $("#buy_btn"),
			$buypackagebtn = $(".buypackagebtn"), //package button
			$buyfavourablebtn = $(".buyfavourablebtn"), //favourable button
			$input = $('#skunum'),
			$id = $("#id"),
			$act_id = $("#act_id"), //package id
			$pd = $('#skunum').next('div'),
			$sku_box = $("#sku_box"),
			$goods_price = $('#goods_price');
			$limit = $('#storage');

		var $insuranceCheckBox = $("div.insurance-info-container div.insurance-info div.ins-option");

		// 加入購物車按鈕事件
		$buybtn.on('click',function() {
			var _buyNum = $.trim($input.val());
			var _limitNum = parseInt($.trim($limit.text()));
			var _checkedInsurance = $insuranceCheckBox.find("input[type='radio']:checked");
			var _buyInsurance = new Array();
			_checkedInsurance.each(function() {
				if ($(this).val() > 0){
					_buyInsurance.push($(this).val());
				}
			});

			if ($buybtn.data('clicked') == 1)
			{
				return false;
			}
			$buybtn.data('clicked',1);
			
			if(!YOHO.check.isNum(_buyNum))
			{
				YOHO.dialog.warn(YOHO._('global_cart_invalid_quantity', '請輸入正確的數量！'));
				$input.val('1');
				$buybtn.data('clicked',0);
				return false;
			}
			
			_buyNum = parseInt(_buyNum);
			
			if (_buyNum > _limitNum)
			{
				YOHO.dialog.warn(sprintf(YOHO._('global_cart_quantity_exceeded', '您購買了%d個，超過購買上限<b>%d</b>個，加入購物車失敗。'), _buyNum, _limitNum));
				$input.val('1');
				$buybtn.data('clicked',0);
				return false;
			}

			//異步加入購物車
			$.ajax({
				url: '/ajax/cartbuy.php',
				type: 'post',
				data: {
					'ac': 'ot',
					'id': $id.val(),
					'gnum': _buyNum,
					'ins_id': _buyInsurance
				},
				dataType: "json",
				success: function(result) {
			
					if(result.status == 1)
					{
						var product_name = $(".product_name h1").text()

						if ($('div.more-menu[data-type="cart"]').length)
						{
							$limit.text(parseInt($limit.text()) - _buyNum);
							show_success(result);
							YOHO.dialog.showDialogCartBuy(null, null, product_name);
						}
						else
						{
							YOHO.dialog.success(YOHO._('global_cart_add_success', '加入購物車成功！'));
							$buybtn.data('clicked',0);
						}
						var country = (YOHO.userInfo.hasOwnProperty('area') && YOHO.userInfo.area) ? YOHO.userInfo.area.toLowerCase() : 'hk';
						if(country != "cn") {
							//GA - ecommerce
							ga('require','ec');
							ga('ec:addProduct', {
							'id': $id.val(),
							'name': $('.product_name > h1').text(),
							'quantity': _buyNum
							});
							ga('ec:setAction', 'add');
							ga('send', 'event', 'UX', 'click', 'add to cart');
							// Facebook Pixel - ecommerce - add to cart
							fbq('track', 'AddToCart', {
								content_ids: [ ""+$id.val()+"" ],
								content_type: 'product',
								value: $goods_price.val(),
								currency: 'HKD'
							});
						}
					}
					else
					{
						YOHO.dialog.warn(result.text);
						$buybtn.data('clicked',0);
					}
				},
				error: function(xhr) {
					$buybtn.data('clicked',0);
					YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
				}
			});
		});

		//優惠活動加入購物車
		$buyfavourablebtn.on('click',function() {
			
			$thisBtn = $("#" + $(this).attr("id"));
			if ($thisBtn.hasClass("added"))
			{
				return false;
			}

			var _buyNum = 1;
			/*
			if ($thisBtn.data('clicked') == 1)
			{
				return false;
			}
			
			if(!YOHO.check.isNum(_buyNum))
			{
				YOHO.dialog.warn(YOHO._('global_cart_invalid_quantity', '請輸入正確的數量！'));
				$input.val('1');
				$buybtn.data('clicked',0);
				return false;
			}
			*/
			
			_buyNum = parseInt(_buyNum);
			var act_form = $(this).closest('form');
			var act_id = act_form.find('input[name="act_id"]').val();
			var gift_id = act_form.find('input[name="gift_id"]').val();

			//異步加入購物車
			$.ajax({

				url: '/ajax/cartbuy.php',
				type: 'post',
				data: {
					'ac': 'favourable_ot',
					'id': $id.val(),
					'gnum': _buyNum,
					'act_id': act_id,
					'gift_id': gift_id
				},
				dataType: "json",
				success: function(result) {
					if(result.status == 1)
					{
						//console.log(result);
						var favourable_kind;
						var product_name = $(".product_name h1").text()
						if(result.favourable_kind == 1){
							favourable_kind = "gift";
						}
						else if(result.favourable_kind == 2){
							favourable_kind = "redeem";
						}

						if ($('div.more-menu[data-type="cart"]').length)
						{
							$limit.text(parseInt($limit.text()) - _buyNum);
							show_success(result);
							YOHO.dialog.showDialogCartBuy(favourable_kind, result.addedQty, product_name);

						}
						else
						{
							YOHO.dialog.success(YOHO._('global_cart_add_success', '加入購物車成功！'));
							$buybtn.data('clicked',0);
						}
						var country = (YOHO.userInfo.hasOwnProperty('area') && YOHO.userInfo.area) ? YOHO.userInfo.area.toLowerCase() : 'hk';
						if(country != "cn") {
							//GA - ecommerce
							ga('require','ec');
							ga('ec:addProduct', {
							'id': $id.val(),
							'name': $('.product_name > h1').text(),
							'quantity': _buyNum
							});
							ga('ec:setAction', 'add');
							ga('send', 'event', 'UX', 'click', 'add to cart');
							// Facebook Pixel - ecommerce - add to cart
							fbq('track', 'AddToCart', {
								content_ids: [ ""+$id.val()+"" ],
								content_type: 'product',
								value: $goods_price.val(),
								currency: 'HKD'
							});
						}

						if($thisBtn.hasClass("add-to-cart")){
							$thisBtn.addClass('added');
							$thisBtn.removeClass('add-to-cart');
							$thisBtn.html(YOHO._('added', '成功加入'));
						}
					}
					else
					{
						YOHO.dialog.warn(result.text);
						$buybtn.data('clicked',0);
					}
				},
				error: function(xhr) {
					$buybtn.data('clicked',0);
					YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
				}
			});
		});

		//套裝加入購物車
		$buypackagebtn.on('click',function() {
			$thisBtn = $("#" + $(this).attr("id"));

			if ($thisBtn.hasClass("added"))
			{
				return false;
			}

			var _buyNum = 1;
			/*
			if ($buypackagebtn.data('clicked') == 1)
			{
				return false;
			}
			$buypackagebtn.data('clicked',1);
			
			if(!YOHO.check.isNum(_buyNum))
			{
				YOHO.dialog.warn(YOHO._('global_cart_invalid_quantity', '請輸入正確的數量！'));
				$input.val('1');
				$buybtn.data('clicked',0);
				return false;
			}
			
			
			_buyNum = parseInt(_buyNum);
			*/
			var act_form = $(this).closest('form');
			var package_id = act_form.find('input[name="package_id"]').val();
			
			//異步加入購物車
			$.ajax({
				url: '/ajax/cartbuy.php',
				type: 'post',
				data: {
					'ac': 'package_ot',
					'package_id': package_id
				},
				dataType: "json",
				success: function(result) {
					if(result.status == 1)
					{
						var product_name = $(".top .header").text();

						if ($('div.more-menu[data-type="cart"]').length)
						{
							$limit.text(parseInt($limit.text()) - _buyNum);
							show_success(result);
							YOHO.dialog.showDialogCartBuy("package", result.addedQty, product_name);
						}
						else
						{
							YOHO.dialog.success(YOHO._('global_cart_add_success', '加入購物車成功！'));
							$buybtn.data('clicked',0);
						}
						var country = (YOHO.userInfo.hasOwnProperty('area') && YOHO.userInfo.area) ? YOHO.userInfo.area.toLowerCase() : 'hk';
						if(country != "cn") {
							//GA - ecommerce
							ga('require','ec');
							ga('ec:addProduct', {
							'id': $id.val(),
							'name': $('.product_name > h1').text(),
							'quantity': _buyNum
							});
							ga('ec:setAction', 'add');
							ga('send', 'event', 'UX', 'click', 'add to cart');
							// Facebook Pixel - ecommerce - add to cart
							fbq('track', 'AddToCart', {
								content_ids: [ ""+$id.val()+"" ],
								content_type: 'product',
								value: $goods_price.val(),
								currency: 'HKD'
							});
						}
					}
					else
					{
						YOHO.dialog.warn(result.text);
						$buybtn.data('clicked',0);
					}

					if($thisBtn.hasClass("add-to-cart")){
						$thisBtn.addClass('added');
						$thisBtn.removeClass('add-to-cart');
						$thisBtn.html(YOHO._('added', '成功加入'));
					}

				},
				error: function(xhr) {
					$buybtn.data('clicked',0);
					YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
				}
			});
		});

		var buyForm = $("#buyForm");
		var fbuybtn = $("#goods_buy_btn");

		var country = (YOHO.userInfo.hasOwnProperty('area') && YOHO.userInfo.area) ? YOHO.userInfo.area.toLowerCase() : 'hk';
		$content = '<div style="text-align:center;">' +
				'<form id="buyForm">'+
				(country == "cn" ? '<script src="https://recaptcha.net/recaptcha/api.js" async defer></script>' : '<script src="https://www.google.com/recaptcha/api.js" async defer></script>')+
                '	<div class="g-recaptcha" data-sitekey="6LetXnYUAAAAAGG6czaEj9RicDJvovFd6A5jYhAp" data-callback="recaptchaCallback" ></div>'+
                '</form>'+
				'</div>';
		//Set our own custom submit function
		fbuybtn.on("click", function(e) {
			//Prevent the default behavior of a form
			e.preventDefault();
			flashdealdialog = YOHO.dialog.creat({
				title: 'Recaptcha',
				okVal: YOHO._('goods_feedback_cancel', '取消'),
				cancel: true,
				cancelVal: YOHO._('goods_feedback_cancel', '取消'),
				ok: function() {
					return false;
				},
				content: $content
			});
			$ftime = $('.flashdeal-countdown').data('nowTs');
			if($('input[id="goods_ap"]').val() == 1 || $ftime >= 1541836800) {
				$dialogdiv = $(flashdealdialog.DOM.inner);
				$height = $(window).height();
				if($height < 400) $height = 1000;
				$width = $(window).width();
				if($width < 600) $width = 1000;
				var posx = (Math.random() * ($width - 400)).toFixed();
				var posy = (Math.random() * ($height - 250)).toFixed();
				$dialogdiv.css({
					'position':'fixed',
					'left':posx+'px',
					'top':posy+'px'
				})
			}
		});

		recaptchaCallback = function() {
			if (grecaptcha.getResponse() == ""){
				$('.g-recaptcha div div iframe').css('padding', '1px');
				$('.g-recaptcha div div iframe').css('border', '1px solid #dc3545');
				return;
			} else {
				$('.g-recaptcha div div iframe').css('border', '');
			}
			if ($buybtn.data('clicked') == 1)
			{
				return false;
			}
			$buybtn.data('clicked',1);
			$dialogdiv.addClass("loading");
			//Our AJAX POST
			$.ajax({
				url: '/ajax/cartbuy.php',
				type: 'post',
				data: {
					'ac': 'got',
					'pd': $pd.text(),
					'g': grecaptcha.getResponse(),
					'id': $("#id").val(),
					'pp': $goods_price.val()

				},
				dataType: "json",
				success: function(result) {
					$dialogdiv.removeClass("loading");
					if(result.status == 1)
					{
						if ($('div.more-menu[data-type="cart"]').length)
						{
							$limit.text(parseInt($limit.text()) - 1);
							flytop_success(result);
							if(Object.keys(YOHO.flashdealCartTimer).length == 0 || YOHO.flashdealCartTimer.expired == '') {
								YOHO.flashdealCartTimer.now_time = result.fg_info.now_time;
								YOHO.flashdealCartTimer.expired = result.fg_info.expired;
								YOHO.common.flashdealCartCountDown();
							}
							buyForm.fadeOut();
							$pd.html('');
							flashdealdialog.close();
						}
						else
						{
							YOHO.dialog.success(YOHO._('global_cart_add_success', '加入購物車成功！'));
							$buybtn.data('clicked',0);
							flashdealdialog.close();
						}
						var country = (YOHO.userInfo.hasOwnProperty('area') && YOHO.userInfo.area) ? YOHO.userInfo.area.toLowerCase() : 'hk';
						if(country != "cn") {
							//GA - ecommerce
							ga('require','ec');
							ga('ec:addProduct', {
								'id': $id.val(),
								'name': $('.product_name > h1').text(),
								'quantity': 1
							});
							ga('ec:setAction', 'add');
							ga('send', 'event', 'UX', 'click', 'add to cart');
							// Facebook Pixel - ecommerce - add to cart
							fbq('track', 'AddToCart', {
								content_ids: [ ""+$id.val()+"" ],
								content_type: 'product',
								value: $goods_price.val(),
								currency: 'HKD'
							});
						}
					}
					else
					{
						if(result.type == 'captcha') {
							$('.g-recaptcha div div iframe').css('padding', '1px');
							$('.g-recaptcha div div iframe').css('border', '1px solid #dc3545');
							$buybtn.data('clicked',0);
							flashdealdialog.close();
						} else {
							YOHO.dialog.warn(result.text);
							$buybtn.data('clicked',0);
							flashdealdialog.close();
						}

					}
				},
				error: function(xhr) {
					$dialogdiv.removeClass("loading");
					$buybtn.data('clicked',0);
					YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
					flashdealdialog.close();
				}
			});
		}

		// 產品飛入購物車動畫
		function flytop_success(data)
		{
			var btn_pos = $buybtn.offset() || fbuybtn.offset();
			var $cart_btn = $('div.more-menu[data-type="cart"]');
			var cart_pos = $cart_btn.offset();
			var $item = $('#item-thumbs').find('li').eq(0).find('img').clone();
			
			$item.css({
				position: "absolute",
				zIndex: "9801",
				width: "45px",
				height: "45px",
				top: btn_pos.top,
				left: btn_pos.left
			}).appendTo("body");
			
			// 拋物線運動
			fly_params = {
				start: {
					x: btn_pos.left + $buybtn.width() / 2,
					y: btn_pos.top + $buybtn.height() / 2,
					angle: -60
				},
				end: {
					x: cart_pos.left + $cart_btn.width() / 2 - $item.width() / 2,
					y: cart_pos.top + $cart_btn.height() / 2 - $item.height() / 2,
					angle: 10,
					length: 0.3
				}
			};
			$item.animate({path : new $.path.bezier(fly_params)}, 600, function(){
				$cart_btn.css({'top':'-20px'}).animate({'top':'0'},400,'easeOutBounce',function() {
					show_success(data);
				});
				$item.fadeOut(function(){
					$item.remove();
				});
			});
		}
		
		// 購買成功彈窗
		function show_success(data)
		{
			var num = data.count;
			
			$('.hd-cartnum').text(num);
			$('.top_fix_bar .red').text(num);
			
			var addCart_txt = YOHO._('global_cart_added_text', [
				"快D帶我番屋企啦 :)",
				"您真系識貨喎 XD"
			]);
			var ok_txt = addCart_txt[parseInt(Math.random() * addCart_txt.length)];
			
			var html = '<div class="panel" id="addcart-success">' +
				'<div class="panel_tit">' + YOHO._('global_cart_added_popup_title', '成功加入購物車') + '</div>'+
				'<div class="panel_con addcart-success-info">'+
				'<div class="addcart-img">'+
				'<svg class="addcart-icon" alt="' + YOHO._('global_cart_popup_title', 'global_cart_popup_title') + '" title="' + YOHO._('global_cart_popup_title', 'global_cart_popup_title') + '"><use xlink:href="/yohohk/img/yoho_svg.svg#shopping_bag"></use></svg>'+
				'</div>'+
				'<p class="ok-txt">'+ok_txt+'</p>'+
				'<p class="ok-item-count-txt">' + sprintf(YOHO._('global_cart_added_popup_item_count', '購物車共有 %s 件產品'), '<span class="red">'+data.count+'</span>') + '</p>'+
				//(data.totalPrice < 1000 ? '<p class="gray">購物滿$1000免運費</p>' : '')+
				'</div>'+
				'<div class="cart-bottom-amount-gocart">'+
				'<div class="cart-amount">'+
				'<p>' + sprintf(YOHO._('global_cart_added_popup_total_amount', '合計：%s'), 'HK$ '+(data.totalPrice).toFixed(2)) + '</p>'+
				'</div>'+
				'<div class="cart-gocart">'+
				'<div class="gocart"><a href="/cart" class="btn">' + YOHO._('global_cart_added_popup_go_cart_btn', '去購物車結算') + '</a></div>'+
				'</div>'+
				'</div>'+
				'</div>';
			
			var $cart_btn = $('div.more-menu[data-type="cart"]');
			$cart_btn.addClass('hover').data('addedToCart',1);
			
			if ($cart_btn.find('.more-bd').length === 0)
			{
				$cart_btn.append('<div class="more-bd cart-bd" style="display: none;" data-loaded="1"></div>');
			}
			
			$cart_btn.find('.more-bd').html(html).slideDown('fast', function (){
				$buybtn.data('clicked',0);
			});
			
			var _time = $cart_btn.data('_time');
			if (_time)
			{
				clearTimeout(_time);
				$cart_btn.removeData('_time');
			}
			
			_time = setTimeout(function() {
				clearTimeout(_time);
				$cart_btn.removeData('_time');
				
				$cart_btn.find('.more-bd').slideUp('fast', function() {
					$cart_btn.removeClass('hover');
					$(this).remove();
				});
			}, 5000);
			$cart_btn.data('_time', _time);
		}
	},
	
	// 更新購買數量
	updateQuantity: function() {
		var $input = $('#skunum');
		var updateNum = function(delta)
		{
			var num = $input.val() || 1;
			var max = parseInt($.trim($('#storage').text())) || 0;
			max = Math.max(0, Math.min(20, max));
			
			if(!YOHO.check.isNum(num))
			{
				$input.val('1');
				return;
			}
			
			num = parseInt(num) + delta;
			
			if (num <= 0)
			{
				num = 1;
				YOHO.dialog.warn(YOHO._('global_cart_add_quantity_min', '您至少得購買1件產品！'));
			}
			else if (num > max)
			{
				num = max;
				YOHO.dialog.warn(sprintf(YOHO._('global_cart_add_quantity_max', '您最多只能購買%s件產品。'), max));
			}
			
			$input.val(num);
		};
		
		// plus / minus button
		$('#sku_box .count_box i').click(function () {
			updateNum(($(this).hasClass('spinner-arrow-up')) ? 1 : -1);
		});
		
		// Validate user input
		$input.change(function () {
			updateNum(0);
		});
	},
	
	// 產品輪播圖
	goodsPicSlide: function() {
		var $view = $("#pic-view"),
			$thumb = $("#item-thumbs"),
			$arrow = $thumb.find('div.arrow'),
			$clone = $thumb.find('img').eq(0).clone(),
			show = null;
		
		$clone.insertAfter($view);
		
		$thumb.on('mouseenter','li',function(){
			var _li = $(this);
			if(_li.hasClass('current'))
			{
				return false;
			}
			show = setTimeout(function() {
				var _i = _li.index(),
				_img = _li.find('img'),
				_ssrc = _img.attr('src');
				_bsrc = _img.data('view');
				
				$arrow.css({'left': _i * 60});
				_li.addClass('current').siblings('li').removeClass('current');
				$clone.attr('src',_ssrc);
				$view.attr('src',_bsrc);
			}, 200);
		}).on('mouseleave','li',function(){
			if(show != null)
			{
				clearTimeout(show);
			}
		});
	},
	
	responsiveVideo: function () {
		// Wrap YouTube / Vimeo videos to work with responsive CSS rules
		$('iframe[src*="www.youtube.com"],iframe[src*="player.vimeo.com"]').wrap('<div class="video-container"></div>');
	},
	
	promoteCountdown: function () {
		var getTs = function () { return Math.floor((new Date()).getTime() / 1000); };
		var serverTs = parseInt($('#promote_countdown').data('nowTs')) || getTs();
		var tsDiff = serverTs - getTs() + 2; // add 2 seconds for network delay
		var endTs = parseInt($('#promote_countdown').data('endTs'));
		var $countdown = $('#promote_countdown');
		var $parent = $countdown.parent();
		function updateCountdown()
		{
			var remaining_time = endTs - (getTs() + tsDiff);
			if (remaining_time <= 0)
			{
				// Terminating condition met
				$parent.text(YOHO._('goods_promote_ended', '限時特價已經結束'));
				window.location.reload();
			}
			else if (remaining_time < 720)
			{
				$parent.html('' +
					'<span style="color:#ff7700;">' +
					'<i class="iconfont">&#359;</i> ' +
					YOHO._('goods_promote_ending_soon', '進入最後倒數！') +
					'</span>'
				);
				
				setTimeout(updateCountdown, 1000);
			}
			else
			{
				var percent02d = function (input) {
					return (String(input).length < 2 ? '0' : '') + input;
				};
				var date_str = '';
				
				if (remaining_time > 86400)
				{
					date_str = date_str + Math.floor(remaining_time / 86400) + YOHO._('goods_promote_day', '天');
					remaining_time = remaining_time % 86400;
				}
				if (remaining_time > 3600)
				{
					date_str = date_str + Math.floor(remaining_time / 3600) + YOHO._('goods_promote_hour', '小時');
					remaining_time = remaining_time % 3600;
				}
				if (remaining_time > 60)
				{
					date_str = date_str + percent02d(Math.floor(remaining_time / 60)) + YOHO._('goods_promote_miniute', '分');
					remaining_time = remaining_time % 60;
				}
				date_str = date_str + percent02d(remaining_time) + YOHO._('goods_promote_second', '秒');
				
				$countdown.text(date_str);
				
				setTimeout(updateCountdown, 1000);
			}
		}
		updateCountdown();
	},
	
	share: function () {
		$('.share-btn').click(function () {
			var myURL = $('link[rel="canonical"]').attr('href') || window.location.href;
			var myImage = $('meta[property="og:image"]').attr('content') || window.location.protocol + '//' + window.location.hostname + '/images/logo_square.png';
			var myTitle = $('meta[property="og:title"]').attr('content') || '友和 YOHO';
			var myDescription = $('meta[property="og:description"]').attr('content') || $('meta[name="Description"]').attr('content') || '';
			
			if ($(this).hasClass('facebook'))
			{
				// Share on Facebook
				FB.ui({
					method: 'feed',
					link: myURL,
					picture: myImage,
					name: myTitle,
					description: myDescription
				}, function(response) {
					var shared = (response && !response.error_code) ? 1 : 0;
				});
			}
			else if ($(this).hasClass('twitter') || 
					$(this).hasClass('pinterest') || 
					$(this).hasClass('google-plus') || 
					$(this).hasClass('weibo'))
			{
				var win_width, win_height, win_settings, share_url;
				if ($(this).hasClass('twitter'))
				{
					win_width = 550;
					win_height = 450;
					win_settings = 'personalbar=0,toolbar=0,scrollbars=1,resizable=1';
					share_url = 'https://twitter.com/share?url=' + encodeURIComponent(myURL);
				}
				else if ($(this).hasClass('pinterest'))
				{
					win_width = 750;
					win_height = 320;
					win_settings = 'status=no,resizable=yes,scrollbars=yes,personalbar=no,directories=no,location=no,toolbar=no,menubar=no';
					share_url = 'https://www.pinterest.com/pin/create/button/?url=' + encodeURIComponent(myURL) + '&media=' + encodeURIComponent(myImage) + '&description=' + encodeURIComponent(myTitle);
				}
				else if ($(this).hasClass('google-plus'))
				{
					win_width = 600;
					win_height = 600;
					win_settings = 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes';
					share_url = 'https://plus.google.com/share?url=' + encodeURIComponent(myURL);
				}
				else if ($(this).hasClass('weibo'))
				{
					win_width = 650;
					win_height = 520;
					win_settings = 'toolbar=no,menubar=no,resizable=no,status=no';
					share_url = 'http://service.weibo.com/share/share.php?url=' + encodeURIComponent(myURL) + '&type=button&language=zh_tw&pic=' + encodeURIComponent(myImage) + '&searchPic=true&style=simple';
				}
				
				var scr_width = screen.width;
				var scr_height = screen.height;
				var win_left = Math.round((scr_width / 2) - (win_width / 2));
				var win_top = (scr_height > win_height) ? Math.round((scr_height / 2) - (win_height / 2)) : 0;
				
				window.open(share_url, '', win_settings +
					',width=' + win_width +
					',height=' + win_height +
					',left=' + win_left +
					',top=' + win_top);
			}
		});
	},
	
	sectionBoxTab: function() {
		$('.sectionbox h4.tabs span.tab').click(function () {
			var $this = $(this);
			var idx = $this.closest('h4.tabs').find('span.tab').index($this);
			
			$this.closest('h4.tabs').find('span.tab').removeClass('on');
			$this.addClass('on');
			
			var $contents = $this.closest('.sectionbox').find('div.content');
			$contents.hide();
			var $content = $contents.eq(idx);
			$content.show();
			
			if ($content.find('.mini-product-list').length)
			{
				var $outer = $content.find('table');
				var $inner = $outer.find('ul');
				YOHO.common.miniProductList.updateBtnState($outer, $inner);
			}
		});
	},
	
	stayTuned: function () {
		$('.goods-stay-tuned-method').click(function() {
			var $btn = $(this);
			var $gst = $btn.closest('.goods-stay-tuned');
			var $gstm = $btn.closest('.goods-stay-tuned-methods');
			
			$('.goods-stay-tuned-method').not($btn).removeClass('on');
			$btn.addClass('on');
			
			var gstmOffset = $gstm.offset();
			var btnOffset = $btn.offset();
			$gst.find('.goods-stay-tuned-indicator').animate({'top': btnOffset.top - gstmOffset.top});
			
			$gst.find('input[name="pre_im_value"]').attr('placeholder', $btn.data('fieldname'));
		});
		$('.goods-stay-tuned button').click(function() {
			var $btn = $(this);
			var $gst = $btn.closest('.goods-stay-tuned');
			var pre_type = $gst.data('type');
			var pre_im = $gst.find('.goods-stay-tuned-method.on').data('im');
			var pre_name = $gst.find('input[name="pre_name"]').val();
			var pre_value = $gst.find('input[name="pre_im_value"]').val();
			
			if (!pre_name)
			{
				YOHO.dialog.warn(YOHO._('goods_stay_tuned_error_name_empty', '請輸入您的稱呼'));
				return false;
			}
			if (!pre_im || !pre_value)
			{
				YOHO.dialog.warn(YOHO._('goods_stay_tuned_error_value_empty', '請提供您的聯絡方法'));
				return false;
			}
			
			if ((pre_im == 'tel') && !YOHO.check.isMobile(pre_value) && !YOHO.check.isTelephone(pre_value))
			{
				YOHO.dialog.warn(YOHO._('goods_stay_tuned_error_invalid_tel', '請輸入正確的電話號碼'));
				return false;
			}
			
			if ((pre_im == 'email') && !YOHO.check.isEmail(pre_value))
			{
				YOHO.dialog.warn(YOHO._('goods_stay_tuned_error_invalid_email', '請輸入正確的電郵地址'));
				return false;
			}
			
			if ((pre_im == 'whatsapp') && !YOHO.check.isMobile(pre_value))
			{
				YOHO.dialog.warn(YOHO._('goods_stay_tuned_error_invalid_whatsapp', '請輸入正確的手提電話號碼'));
				return false;
			}
			
			if($btn.hasClass('clicked'))
			{
				return false;
			}
			$btn.addClass('clicked');
			$btn.html('<img src="/images/loading.gif"> ' + YOHO._('goods_stay_tuned_submitting', '發送中'));
			
			$.ajax({
				url: '/ajax/yuding.php',
				type: 'POST',
				data: {
					'act': 'pre_submit',
					'pre_type': pre_type,
					'goods_id': YOHO.goodsInfo.itemId,
					'pre_name': pre_name,
					'pre_im': pre_im,
					'pre_value': pre_value
				},
				dataType: 'json',
				success: function(res) {
					if (res.status == 1)
					{
						YOHO.dialog.success(YOHO._('goods_stay_tuned_submitted_prompt', '您的資料已成功送出，到貨時我們會聯絡您。'));
						
						$gst.find('.goods-stay-tuned-header div').remove();
						$gst.find('.goods-stay-tuned-table').remove();
						$gst.append('<br><div class="goods-stay-tuned-header"><i class="fa fa-check"></i> ' + YOHO._('goods_stay_tuned_submitted_text', '感謝，您的聯絡資料已送出。') + '</div>');
					}
					else
					{
						YOHO.dialog.warn(res.error);
						
						$btn.removeClass('clicked');
						$btn.html(YOHO._('goods_stay_tuned_submit', '確定'));
					}
				},
				error: function(xhr) {
					$btn.removeClass('clicked');
					$btn.html('確定');
					
					YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
				}
			});
		});
	},
	
	feedbackDialog: function (opts) {
		//	opts: {
		//		title: 'Dialog Title',
		//		id: 'dialog-id',
		//		content: 'HTML goes here',
		//		get_data_func: function ($dg) { return {'key': 'value'}; }
		//	}
		
		if (!YOHO.userInfo.login)
		{
			YOHO.dialog.showLogin();
			return;
		}
		
		var dialog = YOHO.dialog.creat({
			title: opts.title,
			id: opts.id,
			okVal: YOHO._('goods_feedback_submit', '確定'),
			cancel: true,
			cancelVal: YOHO._('goods_feedback_cancel', '取消'),
			ok: function() {
				var $dg = $(dialog.DOM.wrap);
				var data = opts.get_data_func($dg);
				
				var $btn = $dg.find('.aui_buttons button.highlight');
				$btn.text(YOHO._('goods_feedback_submiting', '提交中...')).prop('disabled', true);
				
				$.ajax({
					url: '/ajax/feedback.php',
					type: 'POST',
					data: data,
					dataType: 'json',
					success: function(res) {
						if (res.status == 1)
						{
							// YOHO.dialog.success will close other dialog before showing
							YOHO.dialog.success(YOHO._('goods_feedback_submitted', '您的意見已成功提交，謝謝。'));
						}
						else
						{
							if (res.hasOwnProperty('error') && res.error.length > 0)
							{
								YOHO.dialog.warn(res.error, function () {
									if (res.hasOwnProperty('showlogin') && res.showlogin == 1)
									{
										YOHO.dialog.showLogin();
									}
								});
							}
							
							$btn.text(YOHO._('goods_feedback_submit', '確定')).prop('disabled', false);
						}
					},
					error: function(xhr) {
						$btn.text(YOHO._('goods_feedback_submit', '確定')).prop('disabled', false);
						
						YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
						console.log(JSON.stringify(xhr));
					}
				});
				
				return false;
			},
			content: opts.content
		});
	},
	
	suggestAPrice: function () {
		$('.goods-suggest-a-price').click(function () {
			YOHO.detail.feedbackDialog({
				title: YOHO._('goods_suggest_a_price', '提議更好的售價'),
				id: 'suggest-price',
				content: '<table class="suggest-a-price-table">' +
						'<tr><th align="right">' + YOHO._('goods_feedback_product', '產品：') + '</th>' +
						'<td><span style="width: 440px; display: inline-block;">' + $('dt.product_name h1').text() + '</td></tr>' +
						'<tr><th align="right">' + YOHO._('goods_feedback_price', '售價：') + '</th>' +
						'<td><input style="width: 440px;" type="text" name="price" placeholder="' + sprintf(YOHO._('goods_feedback_our_price', '(本店售價：%s)'), $.trim($('strong.goods-price').text())) + '"></td></tr>' +
						'<tr><th align="right">' + YOHO._('goods_feedback_price_source', '來源：') + '</th>' +
						'<td><input style="width: 440px;" type="text" name="source" placeholder="' + YOHO._('goods_feedback_price_source_placeholder', '請提供報價來源，例如網頁鏈結、店鋪名稱') + '"></td></tr>' +
						'<tr><th align="right">' + YOHO._('goods_feedback_other_comment', '其他意見：') + '</th>' +
						'<td><textarea style="width: 440px;" name="comment" rows="7"></textarea></td></tr>' +
						'</table>',
				get_data_func: function ($dg) {
					var $price = $dg.find('input[name="price"]');
					var $source = $dg.find('input[name="source"]');
					var $comment = $dg.find('textarea[name="comment"]');
					return {
						'act': 'submit_price_suggestion',
						'goods_id': YOHO.goodsInfo.itemId,
						'price': $price.val(),
						'source': $source.val(),
						'comment': $comment.val()
					};
				}
			});
		});
	},

	reportAProblem: function () {
		$('.goods-report-a-problem').html('<i class="fa fa-bug"></i> ' + YOHO._('goods_report_a_problem', '回報問題 / 提供意見'));
		$('.goods-report-a-problem').click(function () {
			// If goods detail is loaded within a frame, call YOHO in parent instead
			var myYOHO = (window.parent && window.parent.hasOwnProperty('YOHO')) ? window.parent.YOHO : YOHO;
			myYOHO.detail.reportAProblemClicked();
		});
	},

	reportAProblemClicked: function () {
		YOHO.detail.feedbackDialog({
			title: YOHO._('goods_report_a_problem', '回報問題 / 提供意見'),
			id: 'report-problem',
			content: '<div><b>' + YOHO._('goods_feedback_product', '產品：') + '</b> ' + $('dt.product_name h1').text() + '</div>' +
					'<div><textarea name="problem" cols="100" rows="7"></textarea></div>',
			get_data_func: function ($dg) {
				var $problem = $dg.find('textarea[name="problem"]');
				return {
					'act': 'submit_problem',
					'goods_id': YOHO.goodsInfo.itemId,
					'problem': $problem.val()
				};
			}
		});
	},

	/* Products page sub title */
	topTag: function (){
		$('.goods-tab-box>a').click(function(){
			if($(this).data('tag') == $('.goods-tab-box a.active').data('tag') ) return;
			$('.goods-tab-box a').removeClass('active');
			$(this).addClass('active');
			var $tag = $(this).data('tag');
			$('.goods-tab-content').fadeOut(function(){
				$('.goods-tab-content > .sectionbox').hide();
				$('.'+$tag).show();
				$('.goods-tab-content').fadeIn(function(){
				});
			});
		});
	},

	// 評價
	pinglun: function(){
		$("#onpinglun").click(function(){
			var $img = $('#pic-view').attr('src');
			var $title = $('.product_name>h1').html();
			var _pinglun_box = null,

			html =  '<form action="comment.php" id="commentdrop">'+
						'<div class="writeComment"  id="comment-form">'+
							' <div class="goods_img"><img src="'+$img+'" width="100" height="100" alt="'+$title+'"></div>'+
							' <div class="goods_title"><h1>'+$title+'</h1></div>'+
							' <div class="star"><div id="comment_rating"></div></div>'+
							' <textarea id="rv-content" name="content"></textarea>'+
							' <input type="hidden" name="type" value="0" />'+
							' <input type="hidden" name="id" value="'+YOHO.goodsInfo.itemId+'" />'+
							' <input type="hidden" name="enabled_captcha" value="0" />'+
							' <input type="hidden" name="rank" value="5" />'+
							' <input type="hidden" name="lei" value="1" />'+
							' <input type="hidden" name="act" value="send_comment" />'+
							' <div id="dz-container" class="dropzone dz-clickable"><div class="dz-default dz-message"><span>'+YOHO._('dropzone_upload_files', '按此選擇檔案 或 拖放相片到這裡')+'</span></div></div>'+
							' <input name="file" type="file" multiple style="visibility: hidden; height: 0;"/>'+
							' <div class=""><a href="javascript:;" class="action-button" id="send_comment">'+YOHO._('comment_submit', '發表評價')+'</a></div>'+
							' <div class="comment_intro">'+YOHO._('comment_disclaimer', 'comment_disclaimer')+'</div>'+
						'</div>'+
					'</form>';

			_pinglun_box  = $.dialog({
				title: '',
				lock: true,
				fixed: true,
				id: 'pinglun',
				width: 550,
				padding: '15px',
				content:html,
				init: function() {
					$("#comment_rating").rateYo({
						rating: 5,
						numStars: 5,
						precision: 2,
						minValue: 1,
						maxValue: 5,
						fullStar: true,
						"starSvg": '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" style="enable-background:new 0 0 40 40;" xml:space="preserve"><style type="text/css"></style>'+//
								'<path class="st0" d="M22.8,4.6l4.1,8.4c0.3,0.6,0.8,1,1.5,1.1l9.2,1.3c1.6,0.2,2.3,2.2,1.1,3.4L32,25.3c-0.5,0.5-0.7,1.1-0.6,1.7 l1.6,9.2c0.3,1.6-1.4,2.8-2.9,2.1l-8.3-4.3c-0.6-0.3-1.3-0.3-1.8,0l-8.3,4.3C10.4,39,8.7,37.8,9,36.2l1.6-9.2 c0.1-0.6-0.1-1.3-0.6-1.7l-6.7-6.5c-1.2-1.1-0.5-3.1,1.1-3.4l9.2-1.3c0.6-0.1,1.2-0.5,1.5-1.1l4.1-8.4C20,3.1,22,3.1,22.8,4.6z"/>'+
								'<path class="st1" d="M10.9,40c-0.7,0-1.4-0.2-2-0.7c-1.1-0.8-1.6-2.1-1.4-3.4l1.6-9.2c0-0.2,0-0.3-0.1-0.4l-6.7-6.5 c-1-0.9-1.3-2.3-0.9-3.6c0.4-1.3,1.5-2.2,2.8-2.4l9.2-1.3c0.2,0,0.3-0.1,0.4-0.3l4.1-8.4C18.5,2.7,19.7,2,21,2 c1.3,0,2.5,0.7,3.1,1.9l4.1,8.4c0.1,0.1,0.2,0.2,0.4,0.3l9.2,1.3c1.3,0.2,2.4,1.1,2.8,2.4c0.4,1.3,0.1,2.6-0.9,3.6l-6.7,6.5 c-0.1,0.1-0.2,0.3-0.1,0.4l1.6,9.2c0.2,1.3-0.3,2.6-1.4,3.4c-1.1,0.8-2.5,0.9-3.7,0.3l-8.3-4.3c-0.1-0.1-0.3-0.1-0.4,0l-8.3,4.3 C12,39.9,11.5,40,10.9,40z M21,32.2c0.6,0,1.1,0.1,1.6,0.4l8.3,4.3c0.2,0.1,0.4,0,0.5,0c0.1-0.1,0.2-0.2,0.2-0.5L30,27.3 c-0.2-1.1,0.2-2.3,1-3.1l6.7-6.5c0.2-0.2,0.2-0.4,0.1-0.5c0-0.1-0.1-0.3-0.4-0.3l-9.2-1.3c-1.1-0.2-2.1-0.9-2.6-1.9l-4.1-8.4 C21.3,5,21.1,5,21,5c-0.1,0-0.3,0-0.4,0.3l-4.1,8.4c-0.5,1-1.5,1.7-2.6,1.9l-9.2,1.3c-0.3,0-0.3,0.2-0.4,0.3c0,0.1-0.1,0.3,0.1,0.5 l6.7,6.5c0.8,0.8,1.2,1.9,1,3.1l-1.6,9.2c0,0.3,0.1,0.4,0.2,0.5c0.1,0.1,0.3,0.2,0.5,0l8.3-4.3C19.9,32.3,20.4,32.2,21,32.2z"/>'+
							"</svg>",
						starWidth: "25px",
						spacing: "8px",
						ratedFill: "#FEDA00",
						normalFill: "#E7E3E1",
						onInit: function () {
						},
						onSet: function (rating, rateYoInstance) {
						  $('input[name="rank"]').val(rating);
						}
					});
					// Add dropzone file upload:
					var dropzone = new Dropzone('#dz-container', {
						// The configuration we've talked about above
						autoProcessQueue: false,
						uploadMultiple: true,
						parallelUploads: 100,
						acceptedFiles: 'image/*',
						url: '/comment.php',
						maxFiles: 5,
						addRemoveLinks: true,
						dictRemoveFile :'<i class="fa fa-trash-o fa-2x" aria-hidden="true"></i>',
						// The setting up of the dropzone
						init: function() {
							var myDropzone = this;
							// First change the button to actually tell Dropzone to process the queue.
							$btn = document.querySelector("#send_comment");
							if($btn) {
								$btn.addEventListener("click", function(e) {
								// Make sure that the form isn't actually being sent.
								e.preventDefault();
								e.stopPropagation();
								var count = myDropzone.files.length;
								if(count == 0) {
									$.ajax({
										type: "POST",
										url: '/comment.php',
										dataType: "json",
										data: $("#commentdrop").serialize(), // serializes the form's elements.
										success: function(data)
										{
											comment_success(data);
										}
									});
								} else {
									myDropzone.processQueue();
								}
							});
							}
							// Listen to the sendingmultiple event. In this case, it's the sendingmultiple event instead
							// of the sending event because uploadMultiple is set to true.
							this.on("sendingmultiple", function(files, xhr, formData) {
							// Gets triggered when the form is actually being sent.
							// Hide the success button or the complete form.
							//merge serialize into formData
							var result = {};
							$.each($("#commentdrop").serializeArray(), function() {
								result[this.name] = this.value;
							});
							for (var key in result) {
								formData.append(key, result[key])
							}
							});
							this.on("successmultiple", function(files, response) {
							// Gets triggered when the files have successfully been sent.
							// Redirect user or notify of success.
							// console.log("successmultiple", response);

							comment_success($.parseJSON(response));

							});
							this.on("errormultiple", function(files, response) {
							// Gets triggered when there was an error sending the files.
							// Maybe show form again, and notify user of error
							console.log("errormultiple");
							});
							this.on("maxfilesexceeded", function(file){
								YOHO.dialog.warn(YOHO._('comment_upload_limit', '最多只能上傳5張圖片'));
								myDropzone.removeFile(file);
							});
							this.on("maxfilesreached", function(file){
							});
							this.on("addedfile", function(file) {
								$(file).on('click', function(){
								});
							});
						}
					});
				}
			});

			// find form parent add id:
			$('#comment-form').closest('.dialog_pinglun').attr('id', 'popupcomment');
		});
		$("#goods_to_review").click(function(){
			$("html, body").scrollTop($('.goods-tab-box').offset().top - $('.top_fix_bar ').height());
			$('.goods-tab-box>a.comment_header').trigger('click');
		});

		function comment_success(response) {
			if (response.status == 1)
			{
				YOHO.dialog.success(YOHO._('comment_success', '評價已提交成功，感謝您的大力支持。'));
				setTimeout(function(){ location.reload(); }, 3000);
			}
			// else if (response.status == 2)
			// {
			// 	YOHO.dialog.warn('驗證碼輸入錯誤或者沒有輸入驗證碼！');
			// }
			else if (response.status == 3)
			{
				YOHO.dialog.warn(YOHO._('comment_error_frequently', '評價過於頻繁，請等待30秒之後重新提交！'));
			}
			else
			{
				YOHO.dialog.warn(response.message);
			}
		};
	},

	// 評價列表圖片展示
	ping_list: function(top) {
		var $list = $('#ping-list');
		loadPageRating();
		
		$list.on('click','li',function() {
			var _this = $(this),
			_src = _this.data('src'),
			$view = _this.parent('ul').next('.ping-photo-viewer');
			if(_src == undefined)
			{
				return false;
			}
			
			if(_this.hasClass('current'))
			{
				_this.removeClass('current');
				$view.hide().off();
			}
			else
			{
				_this.addClass('current').siblings('li').removeClass('current');
				$view.html('<img src="'+_src+'" />').show().off().on('click', function() {
					_this.trigger('click');
				});;
			}
		});

		// 分页浏览
		$list.on('click','a.step',function() {
			var _page = $(this).data('page');
			window.scrollTo(0,$('#pjxqitem').offset().top);
			$list.html('<div style="padding:40px; text-align:center;"><p class="nala-load" style="height:30px;">&nbsp;</p></div>');
			$.ajax({
				url: '/comment.php?act=gotopage',
				data: {'id':YOHO.goodsInfo.itemId,'page':_page,'lei':1},
				success: function(data) {
					$list.html($(data).find('#ping-list').html());
					loadPageRating();
				},
				error: function(xhr) {
					YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
					//$list.html('<div style="padding:40px; text-align:center;">評價載入失敗，請 <a class="step red" href="javascript:;">重試</a></div>');
				}
			});
		});

		 function loadPageRating() {
			$(".displayrating").each( function() {
				var rating = $(this).data('rate');
				$(this).rateYo(
					{
					  rating: rating,
					  numStars: 5,
					  precision: 2,
					  minValue: 1,
					  maxValue: 5,
					  readOnly: true,
					  "starSvg": '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 40" style="enable-background:new 0 0 40 40;" xml:space="preserve"><style type="text/css"></style>'+//
							  '<path class="st0" d="M22.8,4.6l4.1,8.4c0.3,0.6,0.8,1,1.5,1.1l9.2,1.3c1.6,0.2,2.3,2.2,1.1,3.4L32,25.3c-0.5,0.5-0.7,1.1-0.6,1.7 l1.6,9.2c0.3,1.6-1.4,2.8-2.9,2.1l-8.3-4.3c-0.6-0.3-1.3-0.3-1.8,0l-8.3,4.3C10.4,39,8.7,37.8,9,36.2l1.6-9.2 c0.1-0.6-0.1-1.3-0.6-1.7l-6.7-6.5c-1.2-1.1-0.5-3.1,1.1-3.4l9.2-1.3c0.6-0.1,1.2-0.5,1.5-1.1l4.1-8.4C20,3.1,22,3.1,22.8,4.6z"/>'+
							  '<path class="st1" d="M10.9,40c-0.7,0-1.4-0.2-2-0.7c-1.1-0.8-1.6-2.1-1.4-3.4l1.6-9.2c0-0.2,0-0.3-0.1-0.4l-6.7-6.5 c-1-0.9-1.3-2.3-0.9-3.6c0.4-1.3,1.5-2.2,2.8-2.4l9.2-1.3c0.2,0,0.3-0.1,0.4-0.3l4.1-8.4C18.5,2.7,19.7,2,21,2 c1.3,0,2.5,0.7,3.1,1.9l4.1,8.4c0.1,0.1,0.2,0.2,0.4,0.3l9.2,1.3c1.3,0.2,2.4,1.1,2.8,2.4c0.4,1.3,0.1,2.6-0.9,3.6l-6.7,6.5 c-0.1,0.1-0.2,0.3-0.1,0.4l1.6,9.2c0.2,1.3-0.3,2.6-1.4,3.4c-1.1,0.8-2.5,0.9-3.7,0.3l-8.3-4.3c-0.1-0.1-0.3-0.1-0.4,0l-8.3,4.3 C12,39.9,11.5,40,10.9,40z M21,32.2c0.6,0,1.1,0.1,1.6,0.4l8.3,4.3c0.2,0.1,0.4,0,0.5,0c0.1-0.1,0.2-0.2,0.2-0.5L30,27.3 c-0.2-1.1,0.2-2.3,1-3.1l6.7-6.5c0.2-0.2,0.2-0.4,0.1-0.5c0-0.1-0.1-0.3-0.4-0.3l-9.2-1.3c-1.1-0.2-2.1-0.9-2.6-1.9l-4.1-8.4 C21.3,5,21.1,5,21,5c-0.1,0-0.3,0-0.4,0.3l-4.1,8.4c-0.5,1-1.5,1.7-2.6,1.9l-9.2,1.3c-0.3,0-0.3,0.2-0.4,0.3c0,0.1-0.1,0.3,0.1,0.5 l6.7,6.5c0.8,0.8,1.2,1.9,1,3.1l-1.6,9.2c0,0.3,0.1,0.4,0.2,0.5c0.1,0.1,0.3,0.2,0.5,0l8.3-4.3C19.9,32.3,20.4,32.2,21,32.2z"/>'+
						  "</svg>",
					  starWidth: "20px",
					  spacing: "8px",
					  ratedFill: "#FEDA00",
					  normalFill: "#E7E3E1"
					}
				);
			});
		};
	},

	//贈品、換購、優惠組合了解更多popup
	showMore: function(params) {
		var _more_box = null;
		var attached_products_container = params.attached_products_container;
		var favourable_id = params.favourable_id;
		var max_select = params.favourable_max_select;
		var promotion_title = params.promotion_title;
		var package_id = params.package_id;
		var label_text = params.label_text;
		var label_id = params.label_id;
		var tag_prefix = params.tag_prefix;
		var offer_set_info = params.offer_set_info;
		var promotion = params.promotion;
		var top_html = "";
		var label_html = "";
		var tab_html = "";
		var cart_html = "";
		var add_to_cart_html = "";
		var offer_set_info_html = "";
		var need_to_select = false;
		var dialog_width = "1000px";
		var dialog_height = "auto";

		if (promotion_title != '') {
			top_html = '<div class="top">'+
							'<div class="header">' + promotion_title + '</div>'+
							'<div class="hint">' + '' + '</div>' +
						'</div>';
		}
		if(label_id=="gift") {
			gift_text = YOHO._('gifts_add_to_cart_auto', '指定產品及贈品將同時加入購物車');
			if(max_select > 0){
				need_to_select = true;
				gift_text = sprintf(YOHO._('gifts_add_to_cart_select', '請選擇下列贈品任意 %s 件'), '<span class="reminder_highlight">'+parseInt(max_select)+'</span>');
			}

			label_html = ""+
			'<span class="">'+
				label_text +
			'</span>';
			label_html += '<span class="remarks">'+ gift_text +'</span>';
			for(var i=0; i<attached_products_container.length; i++) {
				if(i==0) {
					tab_html += ""+
					'<span class="tab on">'+
						label_text +
					'</span>';
				}else {
					tab_html += ""+
					'<span class="tab">'+
						label_text +
					'</span>';
				}
			}

			if(!need_to_select){
				cart_html = ""+
				'<div class="cart">'+
					'<form method="post">'+
						'<input type="hidden" name="act_id" value="' + favourable_id + '">'+
						'<a href="javascript:void(0);" class="action-button buyfavourablebtn">'+
							'<i class="fa fa-fw fa-shopping-cart"></i>' + YOHO._('goods_add_to_cart', '加入購物車') +
						'</a>'+	
					'</form>'
				'</div>';
			}

		}
		if(label_id=="redeem") {
			label_html = ""+
			'<span class="bg-blue">'+
				label_text +
			'</span>';
			label_html += '<span class="remarks">'+ YOHO._('redeem_add_to_cart_auto', '指定產品及換購產品將同時加入購物車') +'</span>';
		}
		if(label_id=="offer") {
			dialog_width = "964px";
			dialog_height = "630px";

			label_html = ""+
			'<span class="bg-blue">'+
				label_text +
			'</span>';

			tab_html += ""+
			'<span class="tab on">'+
				label_text +
			'</span>';

		}

		var attached_products_html = "";
		if(label_id=="offer"){
			for(var i = 0; i < attached_products_container.length; i++){
				cart_html = ""+
				'<div class="cart">'+
					'<form method="post">'+
						'<input type="hidden" name="package_id" value="'+offer_set_info[i].offer_set_id+'">'+
						'<a href="javascript:void(0);" class="action-button buypackagebtn" id="buy_btn">'+
							'<i class="fa fa-fw fa-shopping-cart"></i>' + YOHO._('goods_add_to_cart', '加入購物車') +
						'</a>'+	
					'</form>'+
				'</div>';
				
				promote_desc_html = '' +
				'<div class="offer-set-info">'+
					offer_set_info[i].offer_set_item_count+
					YOHO._('offer_items', '件')+
					' '+	
					YOHO._('offer_price', '組合價')+
					'<span class="red">'+
						offer_set_info[i].offer_set_discount+
					'</span>'+	
					'折 '+
					'<span class="red">'+
						offer_set_info[i].offer_set_promote_price+
					'</span>'+	
					'元 '+YOHO._('list_price', '建議零售價')+
					offer_set_info[i].offer_set_original_price+
					'元 '+
				'</div>';

				attached_products_html += "<li class='bundle_container'>";
				attached_products_html += '<div class="attached_container"><ul class="attached-product-lists">';	
				
				var attached_products = attached_products_container[i];		
				var attached_products_length = attached_products.length; 			
				for(var j = 0; j <= attached_products_length; j++){
					if(j == attached_products_length) {
						var remainder = attached_products_length % 3;
						var pad = 3 - remainder;
						if(remainder!=0){
							var width = 450 * 0.33 * pad -5;
							attached_products_html += ''+
							'<li class="pad" data-pad="'+pad +'" style="width:'+ width +'px;min-height:242px;margin-right:15px">'+
							'</li>';	
						}													
						break;
					}
	
					var attached_products_number_html = "";
					if(attached_products[j].number > 1){
						attached_products_number_html +=
						'<div class="product-img-qty">' +
							'<span class="product-img-qty-small-text">x</span> '+ attached_products[j].number +
						'</div>';
					}
					
	
					attached_products_html += ''+
					'<li>'+
							'<div class="attached_product">'+
								'<a target="_blank" href="' + attached_products[j].page_link + '" title="' + attached_products[j].name + '">'+
									'<div class="product-img-container">'+
										'<div class="product-img">'+
											'<img src="' + attached_products[j].img_url + '" alt="gift-image" />'+
										'</div>' +
										attached_products_number_html +
									'</div>'+			
									'<div class="original-price">'+
										attached_products[j].original_price +
									'</div>'+
									'<div class="attached-product-name">'+
										attached_products[j].name +
									'</div>'+
								'</a>'+
							'</div>'+
							add_to_cart_html+
					'</li>';
				}
				attached_products_html += ''+
				'</ul>'+
				promote_desc_html+
				'<div class="dialog-pagination">'+
				'</div>'+
				cart_html+
				'</div>'+
				'</li>'
			}
	
			var left_col = '';
			var right_col_class = 'full'
	
			var html = ''+
			'<div class="more_dialog cle" style="width: '+dialog_width+'; height: '+dialog_height+'; padding:15px 15px 0px 15px; box-sizing:initial;">'+
				top_html+
				'<div class="content">'+
					'<div class="middle">'+
						left_col+
						'<div class="' +right_col_class+ '">'+
							'<div class="label">'+
								label_html+
							'</div>'+
							offer_set_info_html+
							'<div class="tabs">'+
								tab_html+
							'</div>'+
							'<div class="attached-products">'+
								'<ul class="bundle_list">'+
									attached_products_html+
								'</ul>'+
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>'+
			'</div>';
			
			_more_box = $.dialog({
				title: null,
				lock: true,
				fixed: true,
				padding: '0',
				id: 'more_info',
				content: html,
				init: function() {
					$('.more_dialog').nextAll().last().click(function(){
						$('.aui_close').trigger('click');
					});
					$('.more_dialog span.tab').click(function () {
						var $this = $(this);
						var idx = $this.closest('.tabs').find('span.tab').index($this);
						
						$this.closest('.tabs').find('span.tab').removeClass('on');
						$this.addClass('on');
						
						var $contents = $this.closest('.middle').find('div.attached_container');
						$contents.hide();
						var $content = $contents.eq(idx);
						$content.show();
						var $offer_info_contents = $this.closest('.middle').find('div.offer-set-info');
						$offer_info_contents.hide();
						var $offer_info_content = $offer_info_contents.eq(idx);
						$offer_info_content.show();
					});
					
				}
			});
			YOHO.common.miniProductList.promote_detail_init();

			display_per_page = 2;
			var promote_length = $('.more_dialog').find('.attached_container').length;
			var count = Math.ceil(promote_length / display_per_page);
			var count_page_html = "";
			for(var i = 0; i < count; i++){
				var page_num = i + 1;
				if(i==0){
					count_page_html = ''+
					'<div class="bottom">'+
					'<span class="dialog-btn left-btn"><</span>'+
					'<ul>';
					count_page_html += '<li class="cur_pos" data-pos=' + i + '>'+ page_num +'</li>';
				}else{
					count_page_html += '<li data-pos=' + i + '>'+ page_num +'</li>';							
				}
				if(i==count-1){
					count_page_html += '</ul>'+
					'<span class="dialog-btn right-btn">></span>'+
					'</div>';
				}			
			}
			$('.more_dialog').append(count_page_html);
			
			$('.more_dialog').find('.dialog-btn').click(function () {
				var $outer = $('.more_dialog').find('.attached-products');
				var $inner = $outer.find('ul.bundle_list');
				var $cur_pos = $(this).parent().find('.cur_pos');
				if ($(this).hasClass('left-btn'))
				{	
					if($cur_pos.data('pos')!=0){
						$cur_pos.prev().addClass('cur_pos');
						$cur_pos.removeClass('cur_pos');
					}
					$inner.animate({
						'scrollLeft': '-=' + ($inner.width())
					}, function () {
						YOHO.common.miniProductList.updateBtnState($outer, $inner);
					});
				}
				else if ($(this).hasClass('right-btn'))
				{	
					if($cur_pos.data('pos')!= count-1){
						$cur_pos.next().addClass('cur_pos');
						$cur_pos.removeClass('cur_pos');
					}
					$inner.animate({
						'scrollLeft': '+=' + ($inner.width())
					}, function () {
						YOHO.common.miniProductList.updateBtnState($outer, $inner);
					});
				}
			});

			$('.more_dialog').find('.bottom li').click(function () {
				var $outer = $('.more_dialog').find('.attached-products');
				var $inner = $outer.find('ul.bundle_list');
				var $cur_pos = $(this).parent().find('.cur_pos');
				var $from_pos_data = $cur_pos.data('pos');
				var $to_pos_data = $(this).data('pos');
				var $gap = Math.abs($to_pos_data - $from_pos_data)
				if($(this).hasClass('cur_pos')){
					return;
				}else{
					$cur_pos.removeClass('cur_pos');
					$(this).addClass('cur_pos');
					if($from_pos_data < $to_pos_data){
						$inner.animate({
							'scrollLeft': '+=' + ($inner.width() * $gap)
						}, function () {
							YOHO.common.miniProductList.updateBtnState($outer, $inner);
						});
					}else{
						$inner.animate({
							'scrollLeft': '-=' + ($inner.width() * $gap)
						}, function () {
							YOHO.common.miniProductList.updateBtnState($outer, $inner);
						});
					}
				}
			});

		}
		else{
			for(var i = 0; i < attached_products_container.length; i++){
				if(offer_set_info[i]) {
					cart_html = ""+
					'<div class="cart">'+
						'<form method="post">'+
							'<input type="hidden" name="package_id" value="'+offer_set_info[i].offer_set_id+'">'+
							'<a href="javascript:void(0);" class="action-button buypackagebtn" id="buy_btn">'+
								'<i class="fa fa-fw fa-shopping-cart"></i>' + YOHO._('goods_add_to_cart', '加入購物車') +
							'</a>'+	
						'</form>'+
					'</div>';
				}
	
				if(i==0){
					attached_products_html += ''+
					'<div class="attached_container"><ul class="attached-product-lists">';	
				}else{
					attached_products_html += ''+
					'<div class="attached_container" style="display:none"><ul class="attached-product-lists">';	
				}
				
				var attached_products = attached_products_container[i];		
				var attached_products_length = attached_products.length; 			
				for(var j = 0; j <= attached_products_length; j++){
					if(j == attached_products_length) {
						var remainder = attached_products_length % 4;
						var pad = 4 - remainder;
						if(remainder!=0){
							var width = 737 * 0.25 * pad -5;
							attached_products_html += ''+
							'<li class="pad" data-pad="'+pad +'" style="width:'+ width +'px;min-height:200px;margin-right:0">'+
							'</li>';	
						}													
						break;
					}
	
					if(label_id=="redeem" || (label_id=="gift" && need_to_select)) {
						add_to_cart_html = '' +
						'<div class="cart">'+
							'<form method="post">'+
								'<input type="hidden" name="act_id" value="' + favourable_id + '">'+
								'<input type="hidden" name="gift_id" value="' + attached_products[j].id + '">'+
								'<a href="javascript:void(0);" class="add-to-cart buyfavourablebtn" id="addGift_'+favourable_id+'_'+attached_products[j].id+'">'+
									'<i class="fa fa-fw fa-shopping-cart"></i>' + YOHO._('goods_add_to_cart', '加入購物車') +
								'</a>'+	
							'</form>'
						'</div>';
					}
	
					var attached_products_number_html = "";
					if(attached_products[j].number > 1){
						attached_products_number_html +=
						'<div class="product-img-qty">' +
							'<span class="product-img-qty-small-text">x</span> '+ attached_products[j].number +
						'</div>';
	
					}
	
	
					
					
					redeem_price_html = '';
					if(label_id=="redeem") {
						redeem_price_html += 
						'<div class="promote-price">'+
							attached_products[j].redeem_price +
						'</div>';
					}
	
					attached_products_html += ''+
					'<li>'+
						
							'<div class="attached_product">'+
								'<a target="_blank" href="' + attached_products[j].page_link + '" title="' + attached_products[j].name + '">'+
									'<div class="product-img-container">'+
										'<div class="product-img">'+
											'<img src="' + attached_products[j].img_url + '" alt="gift-image" />'+
										'</div>' +
										attached_products_number_html +
									'</div>'+			
									redeem_price_html +
									'<div class="original-price">'+
										attached_products[j].original_price +
									'</div>'+
									'<div class="attached-product-name">'+
										attached_products[j].name +
									'</div>'+
								'</a>'+
							'</div>'+
							add_to_cart_html+
					'</li>';
				}
				attached_products_html += ''+
				'</ul>'+
				cart_html+
				'</div>';
			}
	
			var left_col = '';
			var right_col_class = 'full'
			left_col = ''+
			'<div class="left">'+
				'<img src="' + params.left_pic + '" alt="' + params.product_name + '" />'+
				'<div class="product-name">'+
					params.product_name +
				'</div>'+
				'<div class="goods-price">'+
					params.goods_price +
				'</div>'+
				'<del class="goods-market-price">'+
					params.goods_market_price +
				'</del>'+
			'</div>';
	
			right_col_class = 'right';
	
			var html = ''+
			'<div class="more_dialog cle" style="width: 1000px; padding:15px 15px 0px 15px; box-sizing:initial;">'+
				top_html+
				'<div class="content">'+
					'<div class="middle">'+
						left_col+
						'<div class="' +right_col_class+ '">'+
							'<div class="label">'+
								label_html+
							'</div>'+
							offer_set_info_html+
							'<div class="tabs">'+
								tab_html+
							'</div>'+
							'<div class="attached-products">'+
								attached_products_html+
							'</div>'+					
						'</div>'+
					'</div>'+
				'</div>'+
			'</div>';
	
			_more_box = $.dialog({
				title: null,
				lock: true,
				fixed: true,
				padding: '0',
				id: 'more_info',
				content: html,
				init: function() {
					$('.dialog_more_info').nextAll().last().click(function(){
						$('.aui_close').trigger('click');
					});
					$('.more_dialog span.tab').click(function () {
						var $this = $(this);
						var idx = $this.closest('.tabs').find('span.tab').index($this);
						
						$this.closest('.tabs').find('span.tab').removeClass('on');
						$this.addClass('on');
						
						var $contents = $this.closest('.middle').find('div.attached_container');
						$contents.hide();
						var $content = $contents.eq(idx);
						$content.show();				
					});
					
					$('.attached_container').each(function () {	
						display_per_page = 4;
	
						var li_length = $(this).find('li').length;
						var count = Math.ceil(li_length / display_per_page);
						var count_page_html = "";
						for(var i = 0; i < count; i++){
							var page_num = i + 1;
							if(i==0){
								count_page_html = ''+
								'<div class="bottom">'+
								'<span class="dialog-btn left-btn"><</span>'+
								'<ul>';
								count_page_html += '<li class="cur_pos" data-pos=' + i + '>'+ page_num +'</li>';
							}else{
								count_page_html += '<li data-pos=' + i + '>'+ page_num +'</li>';							
							}
							if(i==count-1){
								count_page_html += '</ul>'+
								'<span class="dialog-btn right-btn">></span>'+
								'</div>';
							}			
						}
						$(this).append(count_page_html);
						$(this).find('.dialog-btn').click(function () {
							var $outer = $(this).closest('.attached_container');
							var $inner = $outer.find('ul.attached-product-lists');
							var $cur_pos = $inner.next().next().find('.cur_pos');
							if ($(this).hasClass('left-btn'))
							{	
								if($cur_pos.data('pos')!=0){
									$cur_pos.prev().addClass('cur_pos');
									$cur_pos.removeClass('cur_pos');
								}
								$inner.animate({
									'scrollLeft': '-=' + ($inner.width())
								}, function () {
									// _this.updateBtnState($outer, $inner);
								});
							}
							else if ($(this).hasClass('right-btn'))
							{	
								if($cur_pos.data('pos')!= count-1){
									$cur_pos.next().addClass('cur_pos');
									$cur_pos.removeClass('cur_pos');
								}
								$inner.animate({
									'scrollLeft': '+=' + ($inner.width())
								}, function () {
									// _this.updateBtnState($outer, $inner);
								});
							}
						});
						$(this).find('.bottom li').click(function () {
							var $outer = $(this).closest('.attached_container');
							var $inner = $outer.find('ul.attached-product-lists');
							var $cur_pos = $(this).parent().find('.cur_pos');
							var $from_pos_data = $cur_pos.data('pos');
							var $to_pos_data = $(this).data('pos');
							var $gap = Math.abs($to_pos_data - $from_pos_data)
							if($(this).hasClass('cur_pos')){
								return;
							}else{
								$cur_pos.removeClass('cur_pos');
								$(this).addClass('cur_pos');
								if($from_pos_data < $to_pos_data){
									$inner.animate({
										'scrollLeft': '+=' + ($inner.width() * $gap)
									}, function () {
										// _this.updateBtnState($outer, $inner);
									});
								}else{
									$inner.animate({
										'scrollLeft': '-=' + ($inner.width() * $gap)
									}, function () {
										// _this.updateBtnState($outer, $inner);
									});
								}
							}
			
						});
					});
				}
			});
		}
		
		this.addCart();
	},
	//贈品、換購、優惠、優惠資訊 set width
	calWidth: function() {
		var gift_container_width = $('.gift-container').width();
		
		$('.gift-header').each(function () {
			var gift_set = $(this).closest(".gift-set");
			var gift_label_width = gift_set.find('.gift-label').width();
			var gift_info_left_width = gift_set.find('.gift-info-left').width();
			var gift_info_container_width = gift_container_width - gift_label_width -16;
			gift_set.find('.gift-info-container').css({"width":gift_info_container_width});
			var gift_header_width = gift_info_container_width - gift_info_left_width -9;			
			gift_set.find('.gift-header').css({"width":gift_header_width});
		});
		

		$('.exchange-header').each(function () {
			var gift_set = $(this).closest(".gift-set");
			var redeem_label_width = gift_set.find('.redeem-label').width();
			var exchange_info_left_width = gift_set.find('.exchange-info-left').width();    
			var redeem_info_container_width = gift_container_width - redeem_label_width -16;
			gift_set.find('.redeem-info-container').css({"width":redeem_info_container_width});
			var exchange_header_width = redeem_info_container_width - exchange_info_left_width -20;
			gift_set.find('.exchange-header').css({"width":exchange_header_width});
		});
		
		$('.offer-set-header').each(function () {
			var gift_set = $(this).closest(".gift-set");
			var offer_set_label_width = gift_set.find('.offer-set-label').width();
			var offer_set_left_width = gift_set.find('.offer-set-left').width();
			var offer_set_container_width = gift_container_width - offer_set_label_width -16;
			gift_set.find('.offer-set-container').css({"width":offer_set_container_width});
			var offer_set_header_width = offer_set_container_width - offer_set_left_width -9;
			gift_set.find('.offer-set-header').css({"width":offer_set_header_width});
		});
		
		$('.offer-info-header').each(function () {
			var gift_set = $(this).closest(".gift-set");
			var offer_info_label_width = gift_set.find('.offer-info-label').width();
			var offer_info_left_width = gift_set.find('.offer-info-left').width();  
			var offer_info_container_width = gift_container_width - offer_info_label_width -16;
			gift_set.find('.offer-info-container').css({"width":offer_info_container_width});		
			var offer_info_header_width = offer_info_container_width - offer_info_left_width -6;
			gift_set.find('.offer-info-header').css({"width":offer_info_header_width});    
		});
 
	
    },
};

$(function() {
	YOHO.detail.init();
});

})(jQuery);