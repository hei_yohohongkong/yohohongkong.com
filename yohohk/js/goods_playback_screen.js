let goods_info = [];

let fetchGoodsInfo = function(mode) {
    let url ='/goods-playback-screen/' + mode + '?act=query';
    let fetchGoodsInfoAction = function () {
        $.ajax({
            url: url,
            type: 'get',
            success: function(data) {
                let parsedData = JSON.parse(data);
                goods_info = parsedData.content;
                if (mode === 'top-100') {
                    goods_info.sort(function(a, b) {
                        return b.goods_num - a.goods_num;
                    });
                }
                setTimeout(fetchGoodsInfoAction, 600000);
            },
            error: function(xhr) {
                setTimeout(fetchGoodsInfoAction, 600000);
            }
        });
    };
    fetchGoodsInfoAction();
};

let displaySavedGoodsInfo = function(mode) {

    let updateSavedGoodsInfoDisplay = function(mode) {
        let timeout = 0;
        if (goods_info.length > 0) {
            // Random Pick a product
            let index = Math.floor(Math.random() * goods_info.length);
            let item = goods_info[index];

            if (item.goods_img.length > 0) {
                // Change generic item
                let brand_name = (item.brand_name === null) ? '' : item.brand_name;
                $('.goods_image').attr('src', '/' + item.goods_img);
                $('.goods_brand').text(brand_name);
                $('.goods_name').text(item.goods_name);
                // Change specific item
                if (mode === 'top-100') {
                    $('.goods_ranking').text(index + 1);
                }
                if (mode === 'new') {
                    $('.goods_shelf_date_day').text(item.add_date_day);
                    $('.goods_shelf_date_month').text(item.add_date_month);
                }
                if (mode === 'five-star-review') {
                    $('.goods_additional_info').html(item.additional_info);
                    changeRatingStarSize();
                }
                if (mode === 'seven-day-hot') {
                    $('.goods_sold_num').text(item.additional_info);
                }
            }
            timeout = (item.goods_img.length > 0) ? 10000 : 0;
        }

        // Set Timeout
        setTimeout(updateSavedGoodsInfoDisplay, timeout, mode);
    };

    updateSavedGoodsInfoDisplay(mode);

};

function changeRatingStarSize()
{
    let width = $(window).width();
    let svgElement = $('svg');
    svgElement.attr('height', (width*0.02)+'px');
    svgElement.attr('width', (width*0.02)+'px');
}

$(document).ready(function () {
   fetchGoodsInfo(mode);
   setTimeout(displaySavedGoodsInfo, 500, mode);
});