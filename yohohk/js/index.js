/***
 * index.js
 *
 * Home Page javascript file
 ***/

var YOHO = YOHO || {};

(function($){

YOHO.homePage = {
	init: function () {
		this.swichable();
		this.regEmail();
		this.blogPost();
		this.indexAnimate();
	},

	swichable: function () {
		// 首頁輪播圖
		var $slide = $("#index-slide");
		$slide.switchable({
			triggers: true,
			//putTriggers: 'append',
			panels: 'li',
			effect: 'fade',
			interval: 5,
			autoplay: true
		});

		/* slider pager responsive  */
		var slider_pager = $('.index-banner .switchable-triggers');
		var img = $('.index-slide img');
		// slider_pager.css({"top":(img.height() - 40)});
		$(window).resize(function() {
			slider_pager.css({"top":(img.height() - 40)});
		});
		//$('.quote-card').show();
		//$('.review-right').show();
	},

	regEmail: function () {
		// If user was redirected back from email registration...
		var subscribed = YOHO.common.cookie('email_subscribed');
		if (subscribed == '3')
		{
			YOHO.dialog.ok(YOHO._('index_email_result_confirmed', '謝謝，您已完成了電子報的訂閱確認。'));
			YOHO.common.cookie('email_subscribed', '1', { expires: new Date(2038,1,1), path: '/', domain: '.yohohongkong.com' });
		}
		else if (subscribed == '2')
		{
			YOHO.dialog.ok(YOHO._('index_email_result_registered', '謝謝，您已成功登記電郵地址。'));
			YOHO.common.cookie('email_subscribed', '1', { expires: new Date(2038,1,1), path: '/', domain: '.yohohongkong.com' });
		}
		
		// Bind events for email registration form
		$('.reg-email-block form input[name="from_website"]').val(document.location.host);
		$('.reg-email-block form').submit(function () {
			var email = $(this).find('input[name="email"]').val();
			if (!email)
			{
				YOHO.dialog.warn(YOHO._('index_email_error_empty', '請輸入您的電郵地址'));
				return false;
			}
			if (!YOHO.check.isEmail(email))
			{
				YOHO.dialog.warn(YOHO._('index_email_error_invalid', '請輸入正確的電郵地址'));
				return false;
			}
			$(this).find('input[name="emailconfirm"]').val(email);
			$(this).submit();
		});
	},
	
	blogPost: function () {
		$outer = $('div.blog-post-block .scrollable-outer');
		$outer.css('height', $outer.height());
		$outer.switchable({
			triggers: true,
			triggersWrapCls: 'scrollable-triggers',
			triggerType: 'click',
			panels: '.scrollable-item',
			easing: 'ease-in-out',
			effect: 'scrollLeft',
			end2end: true,
			autoplay: true
		});
	},
	indexAnimate: function () {

		// Rank session animate
		var $height = $(".rank-left").height() - 2 ;
		// $('.rank-left-bar').css({'height':$height,"background-color": $('.rank-left div.active').data('theme')});
		$('.rank-left div.active').css("background-color",$('.rank-left div.active').data('theme'));
		$('.rank-right').css('border-color', $('.rank-left div.active').data('theme'));
		$rankBtn = $('.rank-left div');
		$rankBtn.click(function() {
			$lastBtn = $('.rank-left div.active');
			// $lastBtn.css("background-color","white");
			$lastBtn.removeClass('active');
			$lastid  = $lastBtn.data('rid');
			$($lastid).hide();
			$($lastid).css('opacity', 0);

			$theme = $(this).data('theme');
			$rid   = $(this).data('rid');
			// $(this).css("background-color",$theme);
			$('.rank-right').css('border-color', $theme);
			$($rid).show();
			setTimeout(function() {
				$($rid).css('opacity', 1);
			}, 200);
			$(this).addClass('active');
			var $outer = $($rid +' div').closest('table');
			var $inner = $outer.find('ul');
			$inner.scrollLeft(0);
			YOHO.common.miniProductList.updateBtnState($outer, $inner);
		});

		// Category Session animate
		$hotCatBtn = $('.hot-subcat a');
		$hotCatBtn.click(function() {
			$lastBtn = $(this).parent().children('a.active');
			$lastBtn.removeClass('active');
			$lastid  = $lastBtn.data('cid');
			$($lastid).show();
			$($lastid).hide();

			// $theme = $(this).data('theme');
			$id   = $(this).data('cid');
			// $(this).css("background-color",$theme);
			$($id).show();
			$(this).addClass('active');
			var $outer = $($id).find('table');
			var $inner = $outer.find('ul');
			$inner.scrollLeft(0);
			YOHO.common.miniProductList.updateBtnState($outer, $inner);
		});
	}
};

$(function(){
	YOHO.homePage.init();
});

})(jQuery);
