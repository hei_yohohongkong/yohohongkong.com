var create_qrcode = function(text, typeNumber, errorCorrectionLevel, mode, mb) {
    qrcode.stringToBytes = qrcode.stringToBytesFuncs[mb];
    if (typeNumber == 0) {
        typeNumber = suggestTypeNumber(text);
      }
    
    var qr = qrcode(typeNumber || 4, errorCorrectionLevel || 'M');
    qr.addData(text, mode);
    qr.make();

    return qr;
};

var suggestTypeNumber = function(text) {
    var length = text.length;
    if (length <= 32) { return 3; }
    else if (length <= 46) { return 4; }
    else if (length <= 60) { return 5; }
    else if (length <= 74) { return 6; }
    else if (length <= 86) { return 7; }
    else if (length <= 108) { return 8; }
    else if (length <= 130) { return 9; }
    else if (length <= 151) { return 10; }
    else if (length <= 177) { return 11; }
    else if (length <= 203) { return 12; }
    else if (length <= 241) { return 13; }
    else if (length <= 258) { return 14; }
    else if (length <= 292) { return 15; }
    else { return 40; }
}