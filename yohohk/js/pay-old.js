/***
* pay.js
*
* Javascript file for the payment page
***/

var YOHO = YOHO || {};

YOHO.config = YOHO.config || {};

YOHO.config.disableCommonInit = true;

YOHO.checkPay = {
	common: {
		hasBalance: false
	},

	init: function() {
		// var objCom = this.common;
		// 
		// objCom.total = parseFloat($('#pay-total').text());
		// objCom.$payBox = $('#pay-box');
		// objCom.$payBtn = $('#pay-btn');
		// objCom.$payIntro = $('#pay-intro');
		// objCom.$otherBanks = $('#other-banks');
		// objCom.$fenqiNum = $('#fenqi_num');
		// objCom.$fenqiBank = $('#fenqi-bank');
		// objCom.remain = 0;
		// 
		// if ($('#balance').length > 0)
		// {
		// 	objCom.$balance = $('#balance');
		// 	objCom.balance = parseFloat(objCom.$balance.data('num'));
		// 	objCom.hasBalance = true;
		// 	if (objCom.balance >= objCom.total)
		// 	{
		// 		objCom.$balance.next('span').html('<span>使用餘額支付<em class="green"><i></i>'+objCom.total.toFixed(2)+'</em></span>');
		// 	}
		// 	this.balancePay();
		// }
		// 
		// // 为了兼容IE下诡异的记录上次checked,首次进入页面重置选中支付宝付款
		// objCom.$payBox.find('input[name=channel]').each(function(i) {
		// 	this.checked = false;
		// 	if (i == 0)
		// 	{
		// 		this.checked = true;
		// 	}
		// });
		// 
		// this.updatePayInfo();
		// this.submitPay();
		// this.channelClick();
		// this.showOtherBanks();
		this.showTrade();
		this.huikuanDialog();
		this.alipayDialog();
		this.changePaymentDialog();
	},

	// // 更新支付价格信息
	// updatePayInfo: function() {
	// 	var objCom = this.common,
	// 	$channel = objCom.$payBox.find('input[name=channel]:checked'),
	// 	_channel = $channel.next('label').attr('class'),
	// 	$fenqiBank = objCom.$fenqiBank,
	// 	_fenNum = objCom.$fenqiNum.val(),
	// 	_fenStr = _str = '', _remain = 0;
	// 	
	// 	$fenqiBank.hide();
	// 	
	// 	if (objCom.hasBalance)
	// 	{
	// 		// 含余额支付
	// 		_remain = objCom.total - objCom.balance;
	// 		
	// 		if (_remain > 0)
	// 		{
	// 			if (_remain >= 300)
	// 			{
	// 				$fenqiBank.show();
	// 			}
	// 			objCom.remain = _remain;
	// 			_str = '使用餘額支付<span class="red"><em>'+objCom.balance+'</em></span>，另需使用<label class="'+_channel+'"></label>'+_fenStr+'支付<span class="red"><em>'+_remain.toFixed(2)+'</em></span>';
	// 		}
	// 		else
	// 		{
	// 			_str = '使用餘額支付<span class="red">￥<em>'+objCom.total.toFixed(2)+'</em></span>';
	// 		}
	// 	}
	// 	else
	// 	{
	// 		_remain = objCom.total;
	// 		if (_remain >= 300)
	// 		{
	// 			$fenqiBank.show();
	// 		}
	// 		objCom.remain = _remain;
	// 		
	// 		if(chongzhi == "cz")
	// 		{
	// 			_str = '選擇<label class="'+_channel+'"></label>'+_fenStr+'進行充值';
	// 		}
	// 		else
	// 		{
	// 			_str = '使用<label class="'+_channel+'"></label>'+_fenStr+'支付<span class="red">￥<em>'+_remain.toFixed(2)+'</em></span>';
	// 		}
	// 	}
	// 	objCom.$payIntro.html(_str).css('display','inline-block');
	// },
	// 
	// // 餘額付款
	// balancePay: function() {
	// 	var that = this,
	// 		objCom = that.common,
	// 		$balance = objCom.$balance,
	// 		$bal;
	// 	
	// 	$bal = $balance.parent('div.balance');
	// 	
	// 	$balance.click(function() {
	// 		if($(this).is(":checked"))
	// 		{
	// 			$bal.removeClass('bal-disabled');
	// 			objCom.hasBalance = true;
	// 		}
	// 		else
	// 		{
	// 			$bal.addClass('bal-disabled');
	// 			objCom.hasBalance = false;
	// 		}
	// 		that.updatePayInfo();
	// 	});
	// },
	// 
	// // 選擇支付方式事件2
	// channelClick: function(){
	// 	var that = this,
	// 	objCom = that.common,
	// 	payBox = objCom.$payBox;
	// 
	// 	payBox.on('click','div.banks-bd',function(){
	// 		var _this = $(this),
	// 		$old = payBox.find('div.selected'),
	// 		_channel = _this.find('input[name=channel]')[0];
	// 		if(_channel.value == 'CMBCD'){
	// 			_channel.checked = false;
	// 			$old.find('input[name=channel]')[0].checked = true;
	// 			fenqiDialog($old, _this);
	// 		}else{
	// 			$old.find('input[name=channel]')[0].checked = false;
	// 			$old.removeClass('selected');
	// 			_this.addClass('selected');
	// 			_channel.checked = true;
	// 			objCom.$fenqiNum.val('');
	// 			that.updatePayInfo();
	// 		}
	// 	});
	// 
	// 	// 顯示分期彈窗
	// 	function fenqiDialog($old, _this){
	// 		var _check3 = _check6 = _check12 = html = '',
	// 		_remain = objCom.remain,
	// 		_total3 = _remain*3/100+_remain;
	// 		_total6 = _remain*6/100+_remain;
	// 		_total12 = _remain*12/100+_remain;
	// 		switch (objCom.$fenqiNum.val()) {
	// 			case '3':
	// 				_check3 = 'checked="checked"';
	// 				break;
	// 			case '6':
	// 				_check6 = 'checked="checked"';
	// 				break;
	// 			case '12':
	// 				_check12 = 'checked="checked"';
	// 				break;
	// 			default:
	// 				break;
	// 		};
	// 
	// 		html = '<div class="fenqi-qishu" id="fenqi-qishu">'
	// 			+ '<h4 class="methods_info">銀行：<label class="zhaoshang"></label></h4>'
	// 			+ '<p><input '+_check3+' type="radio" name="fq_qishu" value="3" />3期，收取3.0%手續費，總還款金額'+_total3.toFixed(2)+'元，每期'+(_total3/3).toFixed(2)+'元</p>'
	// 			+ '<p><input '+_check6+' type="radio" name="fq_qishu" value="6" />6期，收取4.2%手續費，總還款金額'+_total6.toFixed(2)+'元，每期'+(_total6/3).toFixed(2)+'元</p>'
	// 			+ '<p><input '+_check12+' type="radio" name="fq_qishu" value="12" />12期，收取6.0%手續費，總還款金額'+_total12.toFixed(2)+'元，每期'+(_total12/3).toFixed(2)+'元</p>'
	// 			+ '</div>';
	// 		$.dialog({
	// 			lock: true,
	// 			id: 'fenqi',
	// 			title: '分期付款',
	// 			content: html,
	// 			ok: function(){
	// 				var _qishu = 0,
	// 				$qishu = $('#fenqi-qishu').find('input[name=fq_qishu]');
	// 
	// 				$qishu.each(function(){
	// 					if(this.checked == true){
	// 						_qishu = this.value;
	// 					}
	// 				});
	// 				$old.find('input[name=channel]')[0].checked = false;
	// 				$old.removeClass('selected');
	// 				_this.addClass('selected');
	// 				_this.find('input[name=channel]')[0].checked = true;
	// 				objCom.$fenqiNum.val(_qishu);
	// 				that.updatePayInfo();
	// 			},
	// 			cancel: true
	// 		});
	// 	}
	// },
	// 
	// // 提交付款
	// submitPay: function(){
	//   
	//    
	// 
	// if(chongzhi=="cz")
	// 	{  var id = $('#trade-id').val(),
	// 	html = '<div class="pay-tipwin">'+
	// 			'<h3>請您在新開頁面中完成付款。</h3>'+
	// 			'<p>付款完成前請不要關閉此窗口。</p>'+
	// 			'<p>完成付款後請點擊下面的按鈕。</p>'+
	// 			'<p class="btn-p">'+
	// 				'<a class="btn" href="/user.php?act=account_log">完成付款</a>'+
	// 				'<a class="btn" href="http://wpa.qq.com/msgrd?v=3&uin=443393653&site=qq&menu=yes">付款遇到問題</a>'+
	// 			'</p>'+
	// 			'<p><a class="ud" onclick="YOHO.dialog.close(\'pay_tip\');" href="javascript:;">選擇其他支付方式重新提交充值申請</a></p>'+
	// 		'</div>';}
	// 	else
	// 	{  var id = $('#trade-id').val(),
	// 	html = '<div class="pay-tipwin">'+
	// 			'<h3>請您在新開頁面中完成付款。</h3>'+
	// 			'<p>付款完成前請不要關閉此窗口。</p>'+
	// 			'<p>完成付款後請點擊下面的按鈕。</p>'+
	// 			'<p class="btn-p">'+
	// 				'<a class="btn" href="/user.php?act=order_detail&order_id='+ tradeNum +'&payl=ok">完成付款</a>'+
	// 				'<a class="btn" href="http://wpa.qq.com/msgrd?v=3&uin=443393653&site=qq&menu=yes">付款遇到問題</a>'+
	// 			'</p>'+
	// 			'<p><a class="ud" onclick="YOHO.dialog.close(\'pay_tip\');" href="javascript:;">返回選擇其他支付方式</a></p>'+
	// 		'</div>';}
	// 	
	// 	
	// 	$('#pay-btn').click(function(){
	// 		YOHO.dialog.creat({id:'pay_tip', content:html});
	// 		$('#payform').submit();
	// 	});
	// },
	// 
	// // 顯示其他銀行
	// showOtherBanks: function(){
	// 	var that = this,
	// 	$banks = that.common.$otherBanks,
	// 	html = '<ul class="methods_info cle" id="other-banks-dg">',
	// 
	// 	banksAry = [
	// 		{"val":"9_ABC", "name":"nongye"},
	// 		{"val":"9_CITIC", "name":"zhongxin"},
	// 		{"val":"9_BOC", "name":"zhongguo"},
	// 		{"val":"9_PSBC", "name":"youzheng"},
	// 		{"val":"9_CMBC", "name":"minsheng"},
	// 		{"val":"9_CIB", "name":"xingye"},
	// 		{"val":"9_CEB", "name":"guangda"},
	// 		{"val":"9_GDB", "name":"guangfa"},
	// 		{"val":"9_SPDB", "name":"pufa"},
	// 		{"val":"9_BOS", "name":"shanghai"},
	// 		{"val":"9_SDB", "name":"shenzhenfz"},
	// 		{"val":"9_BCCB", "name":"beijing"},
	// 		{"val":"9_HXB", "name":"huaxia"},
	// 		// {"val":"CFT_GDB", "name":"shnong"},
	// 		{"val":"9_NJCB", "name":"nanjing"},
	// 		 {"val":"9_HCCB", "name":"hangzhou"},
	// 		{"val":"9_NBCB", "name":"ningbo"}
	// 		// {"val":"CFT_GDB", "name":"huarun"},
	// 		// {"val":"CFT_GDB", "name":"bohai"}
	// 	];
	// 
	// 	$.each(banksAry, function(i, v){
	// 		html += '<li><div data-index="'+i+'" class="banks-bd"><input type="radio" name="channel" value="'+v.val+'"><label class="'+v.name+'"></label></div></li>';
	// 	});
	// 	
	// 	html += '</ul>';
	// 	$banks.on('click', 'a.other-bank-btn', function(){
	// 		YOHO.dialog.creat({
	// 			id: 'otherBanks',
	// 			title: '選擇其他銀行',
	// 			content: html,
	// 			init: function(){
	// 				bindEvent($('#other-banks-dg'));
	// 			}
	// 		});
	// 	});
	// 
	// 	function bindEvent($elem){
	// 		$elem.on('click','div.banks-bd',function(){
	// 			var _this = $(this),
	// 			_index = _this.data('index');
	// 			$old = that.common.$payBox.find('div.selected');
	// 			$old.find('input[name=channel]')[0].checked = false;
	// 			$old.removeClass('selected');
	// 			$banks.find('li').eq(1).html('<div class="banks-bd selected"><input type="radio" name="channel" checked="checked" value="'+banksAry[_index].val+'"><label class="'+banksAry[_index].name+'"></label></div>').show();
	// 			YOHO.dialog.close('otherBanks');
	// 			that.common.$fenqiNum.val('');
	// 			that.updatePayInfo();
	// 		});
	// 	}
	// },
	
	// 訂單詳情事件
	showTrade: function(){
		var $trabtn = $("#trade-showbtn"),
		$traDetail = $trabtn.next("div.trade-detail");
	
		$trabtn.on('click', function(){
			var _this = $(this);
			if(_this.hasClass('clicked')){
				return false;
			}
			$traDetail.fadeIn();
			return false;
		});
	
		$(document).on("click",function(e){
			if($(e.target).parents('.trade-detail').length>0){
				if(e.target.nodeName.toLowerCase() == 'a'){
					return;
				}
				return false;
			}else{
				$trabtn.removeClass("clicked");
				$traDetail.fadeOut();
			}
		});
	},

	// 銀行匯款
	huikuanDialog: function(){
		var $btn = $('#huikuan-btn'),
		html = '',
		offset;
		
		html = '<div class="huikuan-intro">'+
				'<h4><b>1</b> ATM轉帳 | 網上銀行轉帳</h4>'+
				'<p><span class="gray">帳戶名稱：</span>Yoho Hong Kong Limited</p>'+
				'<p><span class="gray">滙豐銀行：</span>456-348739-838 </p>'+
				'<p><span class="gray">恒生銀行：</span>799-041793-883 </p>'+
				'<p><span class="gray">中國銀行：</span>012-887-0-019977-9 </p>'+
				'<h4><b>2</b> 完成轉帳後透過以下方式通知友和</h4>'+
				'<p><span class="gray">WhatsApp：</span>53356996</p>'+
				'<p><span class="gray">電郵：</span><a href="mailto:info@yohohongkong.com">info@yohohongkong.com</a></p>'+
				'<p><span class="gray">Line：</span>yohohongkong</p>'+
				'<p><span class="gray">微信：</span>yohohongkong</p>'+
				'<h4><b>3</b> 確認收妥款項後會馬上處理</h4>'+
			'</div>';
		$btn.on('click', function(){
			var _this = this,
				dialog = $.dialog({
					title: '銀行轉帳',
					id: 'huikuan',
					padding: '0 20px',
					okVal: '知道了',
					follow: _this,
					ok: function(){
						return;
					},
					content: html
				});
		});
	},
	
	// 支付寶
	alipayDialog: function(){
		var $btn = $('#alipay-btn'),
		html = '',
		offset;
		
		html = '<div class="huikuan-intro">'+
				'<h4><b>1</b> 轉帳到本公司的支付寶帳戶</h4>'+
				'<p><span class="gray">帳戶：</span>tb_lhsale@yahoo.com</p>'+
				'<p><span class="gray">姓名：</span>胡柱</p>'+
				'<h4><b>2</b> 完成轉帳後透過以下方式通知友和</h4>'+
				'<p><span class="gray">WhatsApp：</span>53356996</p>'+
				'<p><span class="gray">電郵：</span><a href="mailto:info@yohohongkong.com">info@yohohongkong.com</a></p>'+
				'<p><span class="gray">Line：</span>yohohongkong</p>'+
				'<p><span class="gray">微信：</span>yohohongkong</p>'+
				'<h4><b>3</b> 確認收妥款項後會馬上處理</h4>'+
			'</div>';
		$btn.on('click', function(){
			var _this = this,
				dialog = $.dialog({
					title: '支付寶轉帳',
					id: 'alipay',
					padding: '0 20px',
					okVal: '知道了',
					follow: _this,
					ok: function(){
						return;
					},
					content: html
				});
		});
	},
	
	changePaymentDialog: function(){
		var $other_pay = $('.other_payment');
		$other_pay.find('#change_payment_btn').on('click', function(){
			var _this = this;
			var dialog = $.dialog({
				title: '改用其他付款方式',
				id: 'changePayment',
				padding: '20px',
				follow: _this,
				content: $other_pay.find('#other_payment_form_container').html()
			});
		});
	}
}

$(function(){
	YOHO.checkPay.init();
});

// temp bugfix
var chongzhi = chongzhi || "";
var tradeNum = tradeNum || "";
