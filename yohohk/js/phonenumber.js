$(document).on('click', '#play_now', function() {
    if (!logged_in) {
        back_url = '/phonenumber-verify';
        YOHO.dialog.showLogin();
    } else {
        window.location = "/phonenumber-verify";
    }
})
.on('click', '.success#verify_btn', function() {
    if ($('#number').val().length == 8 && $('#number').val().indexOf('6') !== -1) {
        YOHO.dialog.confirm(sprintf(YOHO._('user_send_verify_mobile', '發送驗證碼至 %s'), '+852-' + $('#number').val()), function() {
            $('#number').attr('disabled', true)
            $.ajax({
                url: 'phonenumber',
                dataType: 'json',
                data: {
                    act: 'send',
                    phone: $('#number').val(),
                },
                success: function (res) {
                    if (res.error == '0') {
                        if (res.msg != "") {
                            YOHO.dialog.ok(res.msg)
                        }
                        blockResend();
                        blockReverify();
                    } else {
                        if (res.error == '2') {
                            window.location = "/phonenumber-confirm";
                        }
                        YOHO.dialog.warn(res.msg)
                        $('#number').attr('disabled', false)
                    }
                },
                error: function (xhr) {
                    YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
                }
            })
        })
    }
})
.on('click', '.success#validate_btn', function() {
    if ($('#verify_code').val() != "") {
        $('#verify_code').attr('disabled', true)
        $('#verify_btn, #validate_btn').removeClass('success');
        $.ajax({
            url: 'phonenumber',
            dataType: 'json',
            data: {
                act: 'validate',
                code: $('#verify_code').val(),
            },
            success: function (res) {
                if (res.error == '0') {
                    window.location = "/phonenumber-confirm";
                } else {
                    if (res.error == '2') {
                        window.location = "/phonenumber-confirm";
                    }
                    YOHO.dialog.warn(res.msg)
                    blockReverify();
                    $('#verify_code').attr('disabled', false)
                    if ($("#verify_countdown").length == 0) {
                        $('#verify_btn').addClass('success');
                    }
                }
            },
            error: function (xhr) {
                YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
            }
        })
    }
})
.on('click', '#shop_btn', function() {
    window.location = "/"
})
.on('click', '#tnc_btn', function() {
    $('.hover-tnc').remove();
    var html = "<div class='hover-tnc'>" +
                    '<div class="hover-tnc-container">' +
                        "<i class='btnCloseSplashAd'></i>" +
                        '<div class="tnc-body">' +
                            "<img src='yohohk/img/phonenumber_tnc.png'>" +
                        '</div>' +
                    '</div>' +
                '</div>';
    $('body').append(html);
})
.on('click', '.hover-tnc-container i.btnCloseSplashAd', function() {
    $('.hover-tnc').remove();
})
.on('click', '#coupon_code input.code', function(e) {
    $(e.target).select();
    YOHO.dialog.success('已複製優惠碼');
    document.execCommand('copy');
    document.getSelection().removeAllRanges();
})

$('#number').on('keydown keyup', function(e) {
    if ((e.which >= 37 && e.which <= 40) || e.which == 8 || e.which == 46) {

    } else if ((e.which >= 48 && e.which <= 57) || (e.which >= 96 && e.which <= 105)) {
        var selectedText = "";
        if (typeof window.getSelection != "undefined") {
            selectedText = window.getSelection().toString();
        } else if (typeof document.selection != "undefined" && document.selection.type == "Text") {
            selectedText = document.selection.createRange().text;
        }
        if ($('#number').val().length >= 8 && selectedText == "") {
            e.preventDefault();
        }
    } else {
        e.preventDefault();
    }
    $('#verify_btn').removeClass('success');
    $('#error_message').html("")
    if ($('#number').val().length == 8) {
        if ($('#number').val().indexOf('6') !== -1) {
            $('#verify_btn').addClass('success');
        } else {
            $('#error_message').html("- " + sprintf(YOHO._('event_require_text', '必須包含 %s 字'), 6))
        }
    } else {
        $('#error_message').html("- " + sprintf(YOHO._('event_require_length', '請輸入 %s 位數字'), 8))
    }
})

function blockResend(time) {
    time = typeof time === "undefined" ? 180 : time;
    $("#verify_code").show();
    $("#verify_btn").removeClass('success');
    $("#verify_btn").append("<span id='verify_countdown' class='countdown' data-t='" + time + "'>(" + time + ")</span>");
    var verify_timer = setInterval(function(){
        var remain = parseInt($("#verify_countdown").data('t'));
        if (remain > 0) {
            remain--;
            $("#verify_countdown").data('t', remain);
            $("#verify_countdown").html("(" + remain + ")");
        } else {
            $("#verify_btn").addClass('success');
            $("#verify_countdown").remove();
            clearInterval(verify_timer);
        }
    }, 1000)
}
function blockReverify() {
    $("#validate_btn").removeClass('success').show();
    $("#validate_btn").append("<span id='validate_countdown' class='countdown' data-t='10'>(10)</span>");
    var validate_timer = setInterval(function(){
        var remain = parseInt($("#validate_countdown").data('t'));
        if (remain > 0) {
            remain--;
            $("#validate_countdown").data('t', remain);
            $("#validate_countdown").html("(" + remain + ")");
        } else {
            $("#validate_btn").addClass('success');
            $("#validate_countdown").remove();
            clearInterval(validate_timer);
        }
    }, 1000)
}