$(document).on('change', 'input[type=file][name=pay_proof]', function () {
    var input = this;
    var reader = new FileReader();
    $("div#pay_submit").addClass("disabled or_btn_gray");
    if (input.files && input.files[0]) {
        $("div#pay_submit").removeClass("disabled or_btn_gray");
        $('select[name=pay_account]').trigger("change");
        var reader = new FileReader();

        reader.onload = function (e) {
            if (e.target.result.indexOf("image/") !== -1) {
                $(input).next('.preview').html('<img src="' + e.target.result + '">');
            } else if (e.target.result.indexOf("application/pdf") !== -1) {
                $(input).next('.preview').html('<div class="unable_preview">' + input.files[0].name + '</div>');
            } else {
                $(input).next('.preview').html('');
                $("div#pay_submit").addClass("disabled or_btn_gray");
                $('input[type=file][name=pay_proof]').val('');
                alert(YOHO._('order_receipt_proof_accept', '只接受相片或pdf'))
            }
        }
        reader.readAsDataURL(input.files[0]);
    } else {
        $(input).next('.preview').html('');
    }
})

$(document).on('change', 'select[name=pay_account]', function() {
    if ($(this).val() == "" || $('input[type=file][name=pay_proof]').get(0).files.length == 0) {
        $("div#pay_submit").addClass("disabled or_btn_gray");
    } else {
        $("div#pay_submit").removeClass("disabled or_btn_gray");
    }
})

$(document).on('click', 'div#pay_submit:not(.disabled)', function() {
    $("#pay_proof_form").submit();
})