/***
 * cart.js
 *
 * Javascript file for the shopping cart page
 ***/
/* jshint browser: true, unused:true, undef:true */
/* globals jQuery, sprintf, confirm */
/* jshint -W041 */

var YOHO = YOHO || {};

YOHO.config = YOHO.config || {};

YOHO.config.disableCommonInit = true;

YOHO.common = YOHO.common || {};

(function($){

YOHO.common.userNav = function() {
	var $bar = $('#userinfo-bar').find('.hd_lbar');
	
	var _handleLogin = function () {
		if(YOHO.userInfo.login)
		{
			bindUserNav();
		}
		else
		{
			$bar.show();
			
			$('.login-link').click(function(e) {
				var aff = $('.login-link').data('affiliate');
				YOHO.dialog.showLogin(aff);
				e.preventDefault();
			});
		}
	};
	if (YOHO.userInfo.initial)
	{
		$.ajax({
			url: '/ajax/loginAuth.php',
			cache: false,
			xhrFields: { withCredentials: true },
			dataType:'json',
			success: function(result) {
				YOHO.userInfo = result;
				_handleLogin();
			},
			error: _handleLogin
		});
	}
	else
	{
		_handleLogin();
	}
	
	function bindUserNav()
	{
		var $nav = null,
			_time = null,
			level = '',
			_tmpName = '',
			_data = YOHO.userInfo,
			_left = 0;
		
		if(_data.level == 1)
		{
			level = 'vip-ico';
		}
		else if(_data.level == 2)
		{
			level = 'svip-ico';
		}
		_tmpName = _data.name;
		
		var $tmpName = $('<span>' + _tmpName + '</span>').appendTo('body');

		_tmpName = '<span>' + _tmpName + '</span>';

		$tmpName.remove();
		
		$bar.html('<a href="/"><i class="fa fa-home fa-lg"></i> ' + YOHO._('global_home', '首頁') + '</a>' +
			'<a href="/user.php" class="usernav-link" rel="nofollow">' +
				'<img src="/images/kong.gif" class="'+level+'" />' + _tmpName +
				'<i class="iconfont arrow">&#8193;</i>' +
			'</a>' +
			'<a href="/user.php?act=logout" class="gray"><i class="fa fa-power-off fa-lg"></i> ' + YOHO._('global_header_menu_user_logout', '登出') + '</a>').show();
		
		_left = $bar.find('a.usernav-link').offset().left;
		
		$nav = $('<div class="usernav-bd" style="left:' + _left + 'px;">' +
			'<a >' + YOHO._('global_header_menu_user_points', '我的積分：') + _data.jifen + '</a>' +
			'<a href="/user.php?act=order_list">' + YOHO._('global_header_menu_user_orders', '我的訂單') + '</a>' +
			'<a href="/user.php?act=profile">' + YOHO._('global_header_menu_user_profile', '修改個人資料') + '</a>' +
			'<a href="/user.php?act=logout" class="last">' + YOHO._('global_header_menu_user_logout', '登出') + '</a>' +
			'</div>').appendTo('body');
		$(window).resize(function() {
			$nav.css('left', $bar.find('a.usernav-link').offset().left);
		});
		$bar.on('mouseenter','a.usernav-link',function() {
			clearTimeout(_time);
			$(this).addClass('hover');
			$nav.addClass('usernav-show');
		}).on('mouseleave','a.usernav-link',function() {
			var _this = $(this);
			_time = setTimeout(function() {
				$nav.removeClass('usernav-show');
				_this.removeClass('hover');
			}, 300);
		});
		
		$nav.on('mouseenter',function() {
			if(_time !== null)
			{
				clearTimeout(_time);
			}
		}).on('mouseleave',function() {
			_time = setTimeout(function() {
				$nav.removeClass('usernav-show');
				$bar.find('a.usernav-link').removeClass('hover');
			}, 300);
		});
	}
};

YOHO.cart = {
	common: {
		$priceTotal: null,
		$box: null,
		$itemNumTop: null
	},

	init: function() {
		var that = this;
		var $priceTotal = $('#price-total');
		
		// Is our cart empty?
		if ($priceTotal.length == 0)
		{
			// Initialize search bar as we have one on our empty cart page
			YOHO.common.searchBar();
			// Skip rest of the initialization steps
			return;
		}
		
		var objCom = that.common;
		objCom.$itemNumTop = $('#itemsnum-top');
		objCom.$priceTotal = $priceTotal;
		objCom.$box = $('#cart-box');
		
		// Update once on init
		that.updateCart();
		
		var $list = objCom.$box.find('li');
		
		// Hover support
		$list.on('mouseenter', function() {
			$(this).addClass('hover');
		}).on('mouseleave', function() {
			$(this).removeClass('hover');
		});
		
		// Init quantity fields
		$list.find('input:text').each(function() {
			var $input = $(this);
			
			var limit = $input.data('limit') ? parseInt($input.data('limit')) : 20;
			if ($input.val() == limit)
			{
				$input.next().addClass('disabled');
			}
			$input.data('oval', $input.val());
		});
		
		objCom.$box.find('ul').each(function() {
			$(this).find('li:last').css('border-bottom','none');
		});
		
		// Clear cart
		$('#del-all').click(function() {
			if (confirm(YOHO._('cart_clear_cart_prompt', '您確定要清空購物車嗎？')))
			{
				window.location = '/cart?step=clear';
			}
		});
		
		that.bindCheckoutBtn();
		
		that.listEvent($list);
		
		that.warnTipEvent();

		$(".package_info_popup").each(function() {
			var _this = $(this);
			_this.mouseover(function(){
				_this.addClass('hover')
			}).mouseout(function() {
				_this.removeClass('hover')
				var package_id = _this.data('id');
				$(".package_info_popup_name[data-id=" + package_id + "]").trigger('mouseout');
			})
		})
		$('.package_info_popup_name').each(function() {
			var _this = $(this);
			_this.mouseover(function() {
				var package_id = _this.data('id');
				clearTimeout(_this.data('clear'));
				_this.data('clear', 0);
				$popup = $(".package_info_popup[data-id=" + package_id + "]");
				var css = _this.offset();
				css.top += 25 - $(document).scrollTop();
				css.left -= $(document).scrollLeft();
				$popup.css(css);
				$popup.addClass('show');
			}).mouseout(function() {
				if ($(".package_info_popup.show:not(.hover):not([data-clear=0])").length > 0) {
					_this.data('clear', setTimeout(function() {
						$(".package_info_popup.show:not(.hover)").removeClass('show');
						_this.data('clear', 0);
					}, 500))
				}
			})
		})
		$('em.counttime').each(function() {
			var _this = $(this),
				_time = _this.data("time");
				// todayTimes = parseInt(new Date().setHours(23,59,59,0).valueOf()),
				// to24Hours = todayTimes - parseInt(new Date().getTime());
			that.countDown(_time, _this);
		});
		
		$('div.warn').each(function() {
			var _this = $(this);
			_this.on('click', 'a.close', function() {
				_this.fadeOut(function() {
					_this.remove();
				});
			});
		});
	},
	
	listEvent: function ($list) {
		var that = this,
		setTime = null;
		
		$list.on('click','a.delete',function(e) {
			if (confirm(YOHO._('cart_remove_prompt', '您確定要從購物車中移除該產品嗎？')))
			{
				var $li = $(this).parents('li');
				var id = $li.data('id');
				that.deleteGoods(id, $li);
				
				e.preventDefault();
			}
		}).on('click','span',function() {
			var $this = $(this);
			var $input = $this.siblings('input:text');
			var limit = $input.data('limit') ? parseInt($input.data('limit')) : 20;
			var num = $input.val();
			
			if($this.hasClass('disabled'))
			{
				return false;
			}
			
			clearTimeout(setTime);
			
			if (!YOHO.check.isNum(num))
			{
				$input.val($input.data('oval'));
				return false;
			}
			
			num = parseInt(num);
			
			if ($this.hasClass('add'))
			{
				num++;
			}
			else
			{
				num--;
			}
			if (num > limit)
			{
				$this.addClass('disabled');
				num = limit;
				showNumLimit($input, limit);
			}
			else if (num <= 0)
			{
				// num = 1;
				// $this.addClass('disabled');
				if (confirm(YOHO._('cart_remove_prompt', '您確定要從購物車中移除該產品嗎？')))
				{
					var $li = $(this).parents('li');
					var id = $li.data('id');
					that.deleteGoods(id, $li);
					
					$this.preventDefault();
				}
				else
				{
					num++;
				}
			}
			
			$this.siblings('span').removeClass('disabled');
			
			$input.val(num).data('oval', num);
			setTime = setTimeout(function() {
				var id = $this.parents('li').data('id');
				that.updateGoodsNum(id, num);
				that.updateCart();
			}, 500);
			
		}).on('keyup','input:text',function() {
			var $input = $(this);
			var oval = $input.data('oval');
			var limit = $input.data('limit') ? parseInt($input.data('limit')) : 20;
			var val = $input.val();
			
			clearTimeout(setTime);
		 	
			if (val == "" || $input.parents('li').hasClass('disabled'))
			{
				setTime = setTimeout(function() {
					$input.val(oval).focus();
				}, 1000);
			}
			else if(val <= 0)
			{
				if (confirm(YOHO._('cart_remove_prompt', '您確定要從購物車中移除該產品嗎？')))
					{
						var $li = $(this).parents('li');
						var id = $li.data('id');
						that.deleteGoods(id, $li);
						
						$input.preventDefault();
					}
			}
			else
			{
				setTime = setTimeout(function() {
					if(YOHO.check.isNum(val))
					{
						if (val >= limit)
						{
							val = limit;
							$input.val(val).siblings('span').removeClass('disabled').eq(1).addClass('disabled');
							showNumLimit($input, limit);
						}
						else if(val == 1)
						{
							$input.siblings('span').removeClass('disabled').eq(0).addClass('disabled');
						}
						else
						{
							$input.siblings('span').removeClass('disabled');
						}
						$input.data('oval',val).trigger('blur');
						
						var id = $input.parents('li').data('id');
						that.updateGoodsNum(id, val);
						that.updateCart();
					}
					else
					{
						$input.val(oval).focus();
					}
				}, 500);
			}
		});
		
		function showNumLimit($input, _limit)
		{
			$('<div class="numlimit-tip"><i>&diams;</i><i class="btm">&diams;</i><p>' + sprintf(YOHO._('cart_update_limit', '最多只能購買%s件'), _limit) + '</p></div>').insertAfter($input).delay(2000).fadeOut(function() { $(this).remove(); });
		}
	},

	// 更新購物車價格
	updateCart: function() {
		var objCom = this.common;
		var $group = objCom.$box.find('.goods-list');
		var _totalItems = 0;
		var _totalPrice = 0;
		
		// 商品被刪光，則刷新頁面
		if($group.length == 0)
		{
			window.location.reload();
			return;
		}
		
		// 循環各組內的商品價格
		$group.each(function() {
			$(this).find('li:not(.package-item)').each(function() {
				var $li = $(this);
				var $input = $li.find('input:text');
				var $unitprice = $li.find('.price-one em');
				var $subtotal = $li.find('.price-xj em');
				var item_num = $li.find('div.pic').length;

				var qty = ($input.length) ? parseInt($input.val()) : parseFloat($li.find('em.only1').text());
				var unitprice = parseFloat($unitprice.text());				

				if (isNaN(unitprice))
				{
					var formatted_price = $unitprice.text();
					var price = (formatted_price.match(/\d+[,\.]?\d*/))[0];
					unitprice = parseFloat(price);
				}
				
				var subtotal = parseFloat((unitprice * qty).toFixed(2));
				$subtotal.text('HK$ ' + subtotal);
				
				_totalItems += item_num * qty;
				_totalPrice += subtotal;
			});
		});

		//handle package price
		$group.each(function() {
			$(this).find('div[class="package_unit"]').each(function() {
				var $div = $(this);
				var $unitprice = $div.find('input[name="package_unit_price[]"]');
				var $unitqty = $div.find('input[name="package_unit_qty[]"]');
				var $unititem = $div.find('input[name="package_unit_item[]"]');

				var unitprice = parseFloat(($unitprice.val().match(/\d+[,\.]?\d*/))[0]);
				var unitqty = parseInt($unitqty.val());	
				var unititem = parseInt($unititem.val());
				var subtotal = parseFloat((unitprice * unitqty).toFixed(2));
				_totalItems += unititem * unitqty;
				_totalPrice += subtotal;
			});
		});
		
		objCom.$priceTotal.html('<p>' + sprintf(YOHO._('cart_summary', '%s 件產品，總價：%s'), _totalItems, '<span class="red">HK$<strong>' + _totalPrice.toFixed(2) + '</strong></span>') + '</p><p>' + '<a href="javascript:;" class="btn">' + YOHO._('cart_checkout_now', '去下單') + '<i class="iconfont">&#402;</i></a>' + '</p>');
		this.bindCheckoutBtn();
		this.bindQuotationBtn();
		objCom.$itemNumTop.html(sprintf(YOHO._('cart_item_count', '%s 件產品'), _totalItems));
	},
	
	bindCheckoutBtn: function () {
		// Proceed to checkout
		var $checkoutBtn = this.common.$priceTotal.find('a.btn');
		var success_callback = function(){
			$(this).html('<img src="/images/loading.gif" /> ' + YOHO._('cart_submitting', '提交中'));
			if(YOHO.userInfo.login){
				setTimeout(function () {
					location.href = '/checkout';
				}, 100);
			}else{
				YOHO.dialog.showLogin();
			}
			$('#checkout-top').off('click').click(function() {
				$checkoutBtn.trigger('click');
			});
		};

		$checkoutBtn.click(function() {
			var $preorder = $('.preorder_detail').get();
			var $require_goods = $('.require_goods_detail').get();
			var confirm_dialog = [];
			if ($preorder.length > 0 || $require_goods.length > 0) {
				if($preorder.length > 0) {
					confirm_dialog.push(YOHO._('checkout_has_preorder_goods', '以下產品需要訂貨：') + "<br>" + $preorder.reduce( function(p,c,i) {
						p+=(i+1)+". <b>"+$(c).attr('id')+"</b> - "+$(c).val()+"<br>";
						return p;
					}, ""));
				}
				if ($require_goods.length > 0) {
					confirm_dialog.push(YOHO._('checkout_has_missing_require_goods', '以下產品需額外購買變壓器：') + "<br>" +
					$require_goods.reduce( function(p,c,i) {
						p += "<tr>" +
								"<td style='padding: 8px 0'>" + (i + 1) + ". <b>" + $(c).attr('id') + "</b> - <b>" +
									"<a target='_blank' href='product/" + $(c).data('tid') + "'>" + $(c).val() + "</a> " +
								"</b></td>" +
							"<td style='text-align: center'><b><span class='price-xj'>" + $(c).data('price') + "</span></b></td>" +
								"<td><button class='require_addtocart highlight pull-right' data-rid='" + $(c).data('tid') + "' onclick='YOHO.cart.buyRequireGoods(" + $(c).data('tid') + ")' data-rid='" + $(c).data('tid') + "'>" +
									"<i class='fa fa-fw fa-shopping-cart'></i> " + YOHO._('productlist_add_to_cart', '加入購物車') +
								"</button></td>" +
							"</tr>";
						return p;
					}, "<table>") + "</table>");
				}
				if ($("#cart-box").data("type") == "mix") {
					confirm_dialog.push(YOHO._('checkout_has_multiple_delivery_party', '由於你購買的貨品在分別在不同倉庫，訂單會分開寄出，所以你將會分開收到你的包裹。'));
				}
				YOHO.dialog.confirm("<span style='font-size: 16px;'><b>" + YOHO._('cart_checkout_notice', '下單需知') + "</span></b><br><br>" + confirm_dialog.join("<hr class='checkout_notice'>"), success_callback);
			} else {
				if ($("#cart-box").data("type") == "mix") {
					var confirm_dialog = [];
					confirm_dialog.push(YOHO._('checkout_has_multiple_delivery_party', '由於你購買的貨品在分別在不同倉庫，訂單會分開寄出，所以你將會分開收到你的包裹。'));
					YOHO.dialog.confirm("<span style='font-size: 16px;'><b>" + YOHO._('cart_checkout_notice', '下單需知') + "</span></b><br><br>" + confirm_dialog.join("<hr class='checkout_notice'>"), success_callback);
				} else {
					success_callback();
				}
			}
		});
	},
	
	bindQuotationBtn: function() {
		var success_callback = function(){
			location.href = '/quotation';
		}
		$('#quotation').click(function() {
			var $preorder = $('.preorder_detail').get();
			if($preorder.length > 0) {
				var info = $preorder.reduce(function(p,c,i) {
					//p+=(i+1)+". <b>"+$(c).attr('id')+"</b> - "+sprintf(YOHO._('goods_delivery_timing_preorder', '一般於 %s 個工作天內派送。'), $(c).val()+"</span>")+"<br>";console.log(YOHO._('goods_delivery_timing_preorder', '一般於 %s 個工作天內派送。'));
					p+=(i+1)+". <b>"+$(c).attr('id')+"</b> - "+$(c).val()+"<br>";
					return p;
				}, "");
				YOHO.dialog.confirm(YOHO._('checkout_has_preorder_goods', '以下產品需要訂貨：') + "<br><br>" + info, success_callback);
			} else {
				success_callback();
			}
		});
	},

	ajaxUpdateCart: function(id, num, error_msg, success_callback) {
		$.ajax({
			url: '/cart',
			type: 'post',
			data: {
				'step': 'update_cart',
				'lineItemId': id,
				'goods_number': num
			},
			success: function(result) {
				if (result.error)
				{
					if (result.hasOwnProperty('message'))
					{
						error_msg = result.message;
					}
					YOHO.dialog.warn(error_msg, function () {
						window.location.reload();
					});
				}
				else
				{
					success_callback();
					window.location.reload();
				}
			},
			error: function(xhr) {
				YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
			}
		});
	},
	
	updateGoodsNum: function(id, num) {
		this.ajaxUpdateCart(id, num, YOHO._('cart_update_failed', '更新購買數量失敗'), $.noop);
	},

	deleteGoods: function(id, $li) {
		var that = this;
		that.ajaxUpdateCart(id, 0, YOHO._('cart_remove_failed', '從購物車中移除產品失敗'), function () {
			var $ele = ($li.siblings('li').length == 0) ? $li.parents('.goods-list') : $li;
			$ele.animate({'opacity':0}, 300, function() {
				$(this).remove();
				that.updateCart();
			});
		});
	},

	// 顯示警示話術
	warnTipEvent: function() {
		var that = this;
		that.common.$box.find('i.infotip').each(function() {
			var _this = $(this),
			$tip = null;
			_this.on('mouseenter',function() {
				var _time = _this.data('time'),
				_str = _this.data('str'),
				_offset = _this.offset();
				
				if(_time != undefined)
				{
					// 限時折扣
					$tip = $('<div style="top:'+(_offset.top+25)+'px; left:'+_offset.left+'px;" class="numlimit-tip hover-warn"><i>&diams;</i><i class="btm">&diams;</i><p><em class="red"></em><span class="red">後過期</span><br>'+_str+'</p></div>').appendTo('body');
					that.countDown(_time,$tip.find('em'));
			   }
			   else
			   {
					// 滿減
					$tip = $('<div style="top:'+(_offset.top+25)+'px; left:'+_offset.left+'px;"  class="numlimit-tip hover-warn"><i></i><i class="btm"></i><p>'+_str+'</p></div>').appendTo('body');
			   }
		   }).on('mouseleave',function() {
			   $tip.remove();
		   });
		});
	},
	
	// 返回時間JSON，參數為秒
	timeJson: function(times) {
		var oTime = {};
		oTime.secs = Math.floor(times % 60);
		oTime.mins = Math.floor(times / 60 % 60);
		oTime.hours = Math.floor(times / 60 / 60 % 24);
		oTime.days = Math.floor(times / 60 / 60 / 24);
		if (oTime.secs < 10)
		{
			oTime.secs = '0' + oTime.secs;
		}
		if (oTime.mins < 10)
		{
			oTime.mins = '0' + oTime.mins;
		}
		if (oTime.hours < 10)
		{
			oTime.hours = '0' + oTime.hours;
		}
		return oTime;
	},
	
	// 倒計時
	countDown: function(millisecond, $elem) {
		var _this = this,
		_time = Math.floor(millisecond/1000);

		var $time = $elem;
		var timeDown = function (times) {
			var temp = _this.timeJson(times);
			$time.html(
				temp.days + YOHO._('goods_promote_day', '天') + 
				temp.hours+ YOHO._('goods_promote_hour', '小時') +
				temp.mins + YOHO._('goods_promote_miniute', '分') +
				temp.secs + YOHO._('goods_promote_second', '秒')
			);
		};

		timeDown(_time);
		setInterval(function() {
			if(_time > 0)
			{
				timeDown(--_time);
			}
			else
			{
				window.location.reload();
			}
		}, 1000);
	},

	// buy require goods
	buyRequireGoods: function (goods_id) {
		var that = this;
		$.ajax({
			url: '/ajax/cartbuy.php',
			type: 'post',
			data: {
				'ac': 'ot',
				'id': goods_id,
				'gnum': 1
			},
			dataType: "json",
			success: function(result) {
				if(result.status == 1) {
					$require_addtocart = $("button.require_addtocart[data-rid=" + goods_id + "]");
					$require_addtocart.html(YOHO._('cart_require_goods_added', '已加入'));
					$require_addtocart.removeClass("highlight");
					$require_addtocart.prop("disabled", true);
					$.ajax({
						url : '/cart',
						cache: false,
						success: function(result) {
							$('#cart-box').html($(result).find('#cart-box').html());
							that.init();
						},
						error: function(xhr) {
							YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
						}
					});
					var country = (YOHO.userInfo.hasOwnProperty('area') && YOHO.userInfo.area) ? YOHO.userInfo.area.toLowerCase() : 'hk';
					if(country != "cn") {
						//GA - ecommerce
						ga('require','ec');
						ga('ec:addProduct', {
						'id': goods_id,
						'name': $('.product_name > h1').text(),
						'quantity': 1
						});
						ga('ec:setAction', 'add');
						ga('send', 'event', 'UX', 'click', 'add to cart');
						// Facebook Pixel - ecommerce - add to cart
						fbq('track', 'AddToCart', {
							content_ids: [ ""+goods_id+"" ],
							content_type: 'product',
							value: result.addedAmount,
							currency: 'HKD'
						});
					}
				} else {
					YOHO.dialog.warn(result.text);
				}
			},
			error: function(xhr) {
				YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
			}
		});
	}
};

$(function() {
	YOHO.common.userNav();
	YOHO.cart.init();
	YOHO.common.splashAd();
});

})(jQuery);
