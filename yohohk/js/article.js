/***
 * article.js
 *
 * Javascript file for article page
 ***/

/* jshint -W041 */

var YOHO = YOHO || {};

(function($) {

YOHO.article = {
	
	init: function() {
		this.share();
	},

	share: function () {
		$('.share-btn').click(function () {
			var myURL = $('link[rel="canonical"]').attr('href') || window.location.href;
			var myImage = $('meta[property="og:image"]').attr('content') || window.location.protocol + '//' + window.location.hostname + '/images/logo_square.png';
			var myTitle = $('meta[property="og:title"]').attr('content') || '友和 YOHO';
			var myDescription = $('meta[property="og:description"]').attr('content') || $('meta[name="Description"]').attr('content') || '';
			
			if ($(this).hasClass('facebook'))
			{
				// Share on Facebook
				FB.ui({
					method: 'feed',
					link: myURL,
					picture: myImage,
					name: myTitle,
					description: myDescription
				}, function(response) {
					var shared = (response && !response.error_code) ? 1 : 0;
				});
			}
			else if ($(this).hasClass('twitter') || 
					$(this).hasClass('pinterest') || 
					$(this).hasClass('google-plus') || 
					$(this).hasClass('weibo'))
			{
				var win_width, win_height, win_settings, share_url;
				if ($(this).hasClass('twitter'))
				{
					win_width = 550;
					win_height = 450;
					win_settings = 'personalbar=0,toolbar=0,scrollbars=1,resizable=1';
					share_url = 'https://twitter.com/share?url=' + encodeURIComponent(myURL);
				}
				else if ($(this).hasClass('pinterest'))
				{
					win_width = 750;
					win_height = 320;
					win_settings = 'status=no,resizable=yes,scrollbars=yes,personalbar=no,directories=no,location=no,toolbar=no,menubar=no';
					share_url = 'https://www.pinterest.com/pin/create/button/?url=' + encodeURIComponent(myURL) + '&media=' + encodeURIComponent(myImage) + '&description=' + encodeURIComponent(myTitle);
				}
				else if ($(this).hasClass('google-plus'))
				{
					win_width = 600;
					win_height = 600;
					win_settings = 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes';
					share_url = 'https://plus.google.com/share?url=' + encodeURIComponent(myURL);
				}
				else if ($(this).hasClass('weibo'))
				{
					win_width = 650;
					win_height = 520;
					win_settings = 'toolbar=no,menubar=no,resizable=no,status=no';
					share_url = 'http://service.weibo.com/share/share.php?url=' + encodeURIComponent(myURL) + '&type=button&language=zh_tw&pic=' + encodeURIComponent(myImage) + '&searchPic=true&style=simple';
				}
				
				var scr_width = screen.width;
				var scr_height = screen.height;
				var win_left = Math.round((scr_width / 2) - (win_width / 2));
				var win_top = (scr_height > win_height) ? Math.round((scr_height / 2) - (win_height / 2)) : 0;
				
				window.open(share_url, '', win_settings +
					',width=' + win_width +
					',height=' + win_height +
					',left=' + win_left +
					',top=' + win_top);
			}
		});
	}
	
};

$(function() {
	YOHO.article.init();
});

})(jQuery);

$(document).ready(function(){
	$(".job-detail").addClass("hidden");

	$(".job-box .btn-view-less").click(function(){
        hideJob($(this));
	})

	$(".job-box .btn-view-more").click(function(){
        showJob($(this));
    })
});

function showJob(target){
    if (target.length) {
		$(".job-detail").addClass("hidden");
		$(".btn-view-more").removeClass("hidden");
		$(".btn-view-less").addClass("hidden");
		target.addClass("hidden");
		target.prev('.btn-view-less').removeClass("hidden");
        target.siblings('.job-info').children('.job-detail').removeClass("hidden");
        var offset = target.parent('.job-box').offset().top - 60;
        $(window).scrollTop(offset);
    }
}

function hideJob(target){
    if (target.length) {
		target.addClass("hidden");
		target.next('.btn-view-more').removeClass("hidden");
        target.siblings('.job-info').children('.job-detail').addClass("hidden");
        var offset = target.parent('.job-box').offset().top - 60;
        $(window).scrollTop(offset);
    }
}
