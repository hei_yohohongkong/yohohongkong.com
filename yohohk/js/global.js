/***
 * global.js
 *
 * Global javascript file
 ***/

/* jshint -W041 */
/* jshint ignore:start */
// jQuery.easing animation plugin
jQuery.extend(jQuery.easing,{def:'easeOutQuad',swing:function(x,t,b,c,d){return jQuery.easing[jQuery.easing.def](x,t,b,c,d);},easeInQuad:function(x,t,b,c,d){return c*(t/=d)*t+b;},easeOutQuad:function(x,t,b,c,d){return-c*(t/=d)*(t-2)+b;},easeInExpo:function(x,t,b,c,d){return(t==0)?b:c*Math.pow(2,10*(t/d-1))+b;},easeOutExpo:function(x,t,b,c,d){return(t==d)?b+c:c*(-Math.pow(2,-10*t/d)+1)+b;},easeInBack:function(x,t,b,c,d,s){if(s==undefined)s=1.70158;return c*(t/=d)*t*((s+1)*t-s)+b;},easeOutBack:function(x,t,b,c,d,s){if(s==undefined)s=1.70158;return c*((t=t/d-1)*t*((s+1)*t+s)+1)+b;},easeInOutBack:function(x,t,b,c,d,s){if(s==undefined)s=1.70158;if((t/=d/2)<1)return c/2*(t*t*(((s*=(1.525))+1)*t-s))+b;return c/2*((t-=2)*t*(((s*=(1.525))+1)*t+s)+2)+b;},easeOutBounce:function(e,f,a,h,g){if((f/=g)<(1/2.75)){return h*(7.5625*f*f)+a}else{if(f<(2/2.75)){return h*(7.5625*(f-=(1.5/2.75))*f+0.75)+a}else{if(f<(2.5/2.75)){return h*(7.5625*(f-=(2.25/2.75))*f+0.9375)+a}else{return h*(7.5625*(f-=(2.625/2.75))*f+0.984375)+a}}}}});

// Amazon-like menu jQuery plugin
(function($){$.fn.menuAim=function(opts){this.each(function(){init.call(this,opts)});return this};function init(opts){var $menu=$(this),activeRow=null,mouseLocs=[],lastDelayLoc=null,timeoutId=null,options=$.extend({rowSelector:"> li",submenuSelector:"*",submenuDirection:"right",tolerance:75,enter:$.noop,exit:$.noop,activate:$.noop,deactivate:$.noop,exitMenu:$.noop},opts);var MOUSE_LOCS_TRACKED=3,DELAY=300;var mousemoveDocument=function(e){mouseLocs.push({x:e.pageX,y:e.pageY});if(mouseLocs.length>MOUSE_LOCS_TRACKED){mouseLocs.shift()}};var mouseleaveMenu=function(){if(timeoutId){clearTimeout(timeoutId)}if(options.exitMenu(this)){if(activeRow){options.deactivate(activeRow)}activeRow=null}};var mouseenterRow=function(){if(timeoutId){clearTimeout(timeoutId)}options.enter(this);possiblyActivate(this)},mouseleaveRow=function(){options.exit(this)};var clickRow=function(){activate(this)};var activate=function(row){if(row.className.toLowerCase()=="current"){return}if(activeRow){options.deactivate(activeRow)}options.activate(row);activeRow=row};var possiblyActivate=function(row){var delay=activationDelay();if(delay){timeoutId=setTimeout(function(){possiblyActivate(row)},delay)}else{activate(row)}};var activationDelay=function(){if(!activeRow||!$(activeRow).is(options.submenuSelector)){return 0}var offset=$menu.offset(),upperLeft={x:offset.left,y:offset.top-options.tolerance},upperRight={x:offset.left+$menu.outerWidth(),y:upperLeft.y},lowerLeft={x:offset.left,y:offset.top+$menu.outerHeight()+options.tolerance},lowerRight={x:offset.left+$menu.outerWidth(),y:lowerLeft.y},loc=mouseLocs[mouseLocs.length-1],prevLoc=mouseLocs[0];if(!loc){return 0}if(!prevLoc){prevLoc=loc}if(prevLoc.x<offset.left||prevLoc.x>lowerRight.x||prevLoc.y<offset.top||prevLoc.y>lowerRight.y){return 0}if(lastDelayLoc&&loc.x==lastDelayLoc.x&&loc.y==lastDelayLoc.y){return 0}function slope(a,b){return(b.y-a.y)/(b.x-a.x)}var decreasingCorner=upperRight,increasingCorner=lowerRight;if(options.submenuDirection=="left"){decreasingCorner=lowerLeft;increasingCorner=upperLeft}else{if(options.submenuDirection=="below"){decreasingCorner=lowerRight;increasingCorner=lowerLeft}else{if(options.submenuDirection=="above"){decreasingCorner=upperLeft;increasingCorner=upperRight}}}var decreasingSlope=slope(loc,decreasingCorner),increasingSlope=slope(loc,increasingCorner),prevDecreasingSlope=slope(prevLoc,decreasingCorner),prevIncreasingSlope=slope(prevLoc,increasingCorner);if(decreasingSlope<prevDecreasingSlope&&increasingSlope>prevIncreasingSlope){lastDelayLoc=loc;return DELAY}lastDelayLoc=null;return 0};$menu.mouseleave(mouseleaveMenu).find(options.rowSelector).mouseenter(mouseenterRow).mouseleave(mouseleaveRow).click(clickRow);$(document).mousemove(mousemoveDocument)}})(jQuery);

// ref: https://github.com/dreamerslab/jquery.actual
(function(a){a.fn.addBack=a.fn.addBack||a.fn.andSelf;
a.fn.extend({actual:function(b,l){if(!this[b]){throw'$.actual => The jQuery method "'+b+'" you called does not exist';}var f={absolute:false,clone:false,includeMargin:false};
var i=a.extend(f,l);var e=this.eq(0);var h,j;if(i.clone===true){h=function(){var m="position: absolute !important; top: -1000 !important; ";e=e.clone().attr("style",m).appendTo("body");
};j=function(){e.remove();};}else{var g=[];var d="";var c;h=function(){c=e.parents().addBack().filter(":hidden");d+="visibility: hidden !important; display: block !important; ";
if(i.absolute===true){d+="position: absolute !important; ";}c.each(function(){var m=a(this);var n=m.attr("style");g.push(n);m.attr("style",n?n+";"+d:d);
});};j=function(){c.each(function(m){var o=a(this);var n=g[m];if(n===undefined){o.removeAttr("style");}else{o.attr("style",n);}});};}h();var k=/(outer)/.test(b)?e[b](i.includeMargin):e[b]();
j();return k;}});})(jQuery);

// ref: https://github.com/alexei/sprintf.js
!function(a){function b(){var a=arguments[0],c=b.cache;return c[a]&&c.hasOwnProperty(a)||(c[a]=b.parse(a)),b.format.call(null,c[a],arguments)}function c(a){return Object.prototype.toString.call(a).slice(8,-1).toLowerCase()}function d(a,b){return Array(b+1).join(a)}var e={not_string:/[^s]/,number:/[diefg]/,json:/[j]/,not_json:/[^j]/,text:/^[^\x25]+/,modulo:/^\x25{2}/,placeholder:/^\x25(?:([1-9]\d*)\$|\(([^\)]+)\))?(\+)?(0|'[^$])?(-)?(\d+)?(?:\.(\d+))?([b-gijosuxX])/,key:/^([a-z_][a-z_\d]*)/i,key_access:/^\.([a-z_][a-z_\d]*)/i,index_access:/^\[(\d+)\]/,sign:/^[\+\-]/};b.format=function(a,f){var g,h,i,j,k,l,m,n=1,o=a.length,p="",q=[],r=!0,s="";for(h=0;o>h;h++)if(p=c(a[h]),"string"===p)q[q.length]=a[h];else if("array"===p){if(j=a[h],j[2])for(g=f[n],i=0;i<j[2].length;i++){if(!g.hasOwnProperty(j[2][i]))throw new Error(b("[sprintf] property '%s' does not exist",j[2][i]));g=g[j[2][i]]}else g=j[1]?f[j[1]]:f[n++];if("function"==c(g)&&(g=g()),e.not_string.test(j[8])&&e.not_json.test(j[8])&&"number"!=c(g)&&isNaN(g))throw new TypeError(b("[sprintf] expecting number but found %s",c(g)));switch(e.number.test(j[8])&&(r=g>=0),j[8]){case"b":g=g.toString(2);break;case"c":g=String.fromCharCode(g);break;case"d":case"i":g=parseInt(g,10);break;case"j":g=JSON.stringify(g,null,j[6]?parseInt(j[6]):0);break;case"e":g=j[7]?g.toExponential(j[7]):g.toExponential();break;case"f":g=j[7]?parseFloat(g).toFixed(j[7]):parseFloat(g);break;case"g":g=j[7]?parseFloat(g).toPrecision(j[7]):parseFloat(g);break;case"o":g=g.toString(8);break;case"s":g=(g=String(g))&&j[7]?g.substring(0,j[7]):g;break;case"u":g>>>=0;break;case"x":g=g.toString(16);break;case"X":g=g.toString(16).toUpperCase()}e.json.test(j[8])?q[q.length]=g:(!e.number.test(j[8])||r&&!j[3]?s="":(s=r?"+":"-",g=g.toString().replace(e.sign,"")),l=j[4]?"0"===j[4]?"0":j[4].charAt(1):" ",m=j[6]-(s+g).length,k=j[6]&&m>0?d(l,m):"",q[q.length]=j[5]?s+g+k:"0"===l?s+k+g:k+s+g)}return q.join("")},b.cache={},b.parse=function(a){for(var b=a,c=[],d=[],f=0;b;){if(null!==(c=e.text.exec(b)))d[d.length]=c[0];else if(null!==(c=e.modulo.exec(b)))d[d.length]="%";else{if(null===(c=e.placeholder.exec(b)))throw new SyntaxError("[sprintf] unexpected placeholder");if(c[2]){f|=1;var g=[],h=c[2],i=[];if(null===(i=e.key.exec(h)))throw new SyntaxError("[sprintf] failed to parse named argument key");for(g[g.length]=i[1];""!==(h=h.substring(i[0].length));)if(null!==(i=e.key_access.exec(h)))g[g.length]=i[1];else{if(null===(i=e.index_access.exec(h)))throw new SyntaxError("[sprintf] failed to parse named argument key");g[g.length]=i[1]}c[2]=g}else f|=2;if(3===f)throw new Error("[sprintf] mixing positional and named placeholders is not (yet) supported");d[d.length]=c}b=b.substring(c[0].length)}return d};var f=function(a,c,d){return d=(c||[]).slice(0),d.splice(0,0,a),b.apply(null,d)};"undefined"!=typeof exports?(exports.sprintf=b,exports.vsprintf=f):(a.sprintf=b,a.vsprintf=f,"function"==typeof define&&define.amd&&define(function(){return{sprintf:b,vsprintf:f}}))}("undefined"==typeof window?this:window);

// ref: http://stackoverflow.com/a/1533945/613541
(function($){$.fn.shuffleElements=function(n){return this.each(function(){var t=$(this),r=t.children(n);r.sort(function(){return Math.round(Math.random())-.5}),t.detach(n);for(var e=0;e<r.length;e++)t.append(r[e])})};})(jQuery);
/* jshint ignore:end */

// Workaround for browsers without console
if (typeof window.console == "undefined")
{
	window.console = {
		log: function () {}
	};
}

var YOHO = YOHO || {};

(function($){

YOHO.config = YOHO.config || {
	disableCommonInit: false
};

YOHO.userInfo = YOHO.userInfo || {
	initial: 1,
	login: 0
};

YOHO.languages = YOHO.languages || {};
YOHO.flashdealCartTimer = YOHO.flashdealCartTimer || {};

// Obtain localized string
YOHO._ = function(key, fallback)
{
	return (YOHO.languages && YOHO.languages.hasOwnProperty(key)) ? YOHO.languages[key] : (fallback || '');
};

YOHO.check = {
	// 判斷IE6
	isIE6: window.VBArray && !window.XMLHttpRequest,
	// 判斷是否為中英文，數字，下划線，減號
	isNick: function(str) {
		var nickReg = /^[\u4e00-\u9fa5A-Za-z0-9_-]+$/;
		return nickReg.test(str);
	},
	// 判斷是否為中英文，空格，減號，點
	isValidName: function(str) {
		var nickReg = /^[\u4e00-\u9fa5A-Za-z .-]+$/;
		return nickReg.test(str);
	},
	// 判斷郵箱
	isEmail: function(str) {
		var emailReg = /^([a-zA-Z0-9_.+-])+\@([a-z0-9][a-z0-9-]*\.)+[a-z]{2,5}$/i;
		return emailReg.test(str);
	},
	//判斷手機
	isMobile: function(str, ccc) {
		// 香港手提電話號碼: 5,6或9字頭，共8位數字
		var mobileReg = /^[456789][0-9]{7}$/;
		// 澳門手提電話號碼: 6字頭，共8位數字
		var macauReg = /^[6][0-9]{7}$/;
		// 台灣手機號碼: 09開頭，共10位數字 (開頭0字可省略)
		var taiwanReg = /^0?9[0-9]{8}$/;
		// 大陸手機號: 1字頭，第二位是345678其中一個，共11位數字
		var mainlandReg = /^1[345678][0-9]{9}$/;
		// 新加坡手機號碼: 8或9字頭，共8位數字
		var singaporeReg = /^[89][0-9]{7}$/;
		// 其他: 最少8位數字
		var otherReg = /^[0-9]{8,}$/;
		// 禁止輸入緊急號碼 (99字頭,11字頭,911)
		var invalidReg = /^(99|11|911)/;
		if (invalidReg.test(str)) return false;
		// Check depends on country calling code
		if ($.type(ccc) === 'string')
		{
			if (ccc == '852') // Hong Kong +852
				return mobileReg.test(str);
			else if (ccc == '853') // Macau +853
				return macauReg.test(str);
			else if (ccc == '886') // Taiwan +886
				return taiwanReg.test(str);
			else if (ccc == '86') // Mainland China +86
				return mainlandReg.test(str);
			else if (ccc == '65') // Singapore +65
				return singaporeReg.test(str);
			else // Other countries, accept all
				return otherReg.test(str);
		}
		// Fallback: accept all
		return (mobileReg.test(str) || macauReg.test(str) || taiwanReg.test(str) || mainlandReg.test(str) || singaporeReg.test(str));
	},
	// 判斷電話號碼
	isTelephone: function(str) {
		// 寬鬆地接受可有可無的+字頭，之後任意數字括號橫線空格最少8位
		var phoneReg = /^(\+\d+)?[0-9\(\)\- ]{8,}$/;
		return phoneReg.test(str);
	},
	// 判斷URL
	isUrl: function(str) {
		var urlReg = /^http:\/\/([\w-]+\.)+[\w-]+(\/[\w-.\/?%&=]*)?$/;
		return urlReg.test(str);
	},
	// 判斷數字
	isNum: function(str) {
		var numReg = /^[0-9]\d*$/;
		return numReg.test(str);
	}
};

//全局彈窗
YOHO.dialog = {
	close: function (id) {
		var list = $.dialog.list;
		list = id ? {id: list[id]} : list;
		$.each(list, function (i, d) {
			d.close();
		});
	},
	creat: function (options) {
		var dialog = null;
		options = $.extend({
			fixed:true,
			title:false,
			lock:true,
			padding:'20px 40px',
			id: '',
			content: ''
		},options);
		dialog = $.dialog(options);
		return dialog;
	},
	success: function (html,time) {
		var dialog = null;
		time = time || 2;
		this.close();
		html = '<div class="success-tip"><i class="iconfont">&#126;</i> '+html+'</div>';
		dialog = this.creat({id:'success',content:html,lock:false});
		setTimeout(function(){
			var $dg = $(dialog.DOM.wrap);
			$dg.animate({'top':'-=50px','opacity':0},300,'easeInBack',function(){
				dialog.close();
			});
		}, time*1000);
	},
	warn: function (html,callback){
		var dialog = null;
		html = '<div class="warn-tip"><i class="iconfont">&#227;</i>'+html+'</div>';
		dialog = this.creat({id:'warn',content:html});
		dialog.button({
			name: YOHO._('global_OK_only', 'Okay'),
			focus: true,
			callback: callback
		});
	},
	ok: function (html,callback){
		var dialog = null;
		html = '<div class="ok-tip"><i class="iconfont">&#379;</i>'+html+'</div>';
		dialog = this.creat({id:'ok',content:html});
		dialog.button({
			name: YOHO._('global_OK_only', 'Okay'),
			focus: true,
			callback: callback
		});
	},
	confirm: function (html,callback) {
		var dialog = null;
		html = '<div class="confirm-tip"><i class="iconfont">&#228;</i>'+html+'</div>';
		dialog = this.creat({id:'confirm',content:html});
		dialog.button({
			name: YOHO._('global_OK', '確定'),
			focus: true,
			callback: callback
		},{
			name: YOHO._('global_Cancel', '取消')
		});
	},
	confirm_for_review: function (html,callbackfun1,callbackfun2) {
		var dialog = null;
		html = '<div class="confirm-tip"><i class="iconfont">&#228;</i>'+html+'</div>';
		dialog = this.creat({id:'confirm',content:html});
		dialog.button({
			name: YOHO._('review_for_goods', '為商品評分'),
			focus: true,
			callback: callbackfun1
		},{
			name: YOHO._('review_servey_submit', '繼續提交問卷'),
			callback: callbackfun2
		});
	},
	showDialogCartBuy: function(type, addQty, product){
		var _dialog_box = null;
		var body = "";

		if(type == "gift"){
			body = sprintf(YOHO._('cart_add_success_favourable', '%1s<br><br>及%2s已加入購物車'), "<span class='reminder_bold'>" + product + "</span>", "<span class='reminder_highlight'>"+ YOHO._('gifts', '贈品') +"</span>")
		}
		else if(type == "redeem"){
			body = sprintf(YOHO._('cart_add_success_favourable', '%1s<br><br>及%2s已加入購物車'), "<span class='reminder_bold'>" + product + "</span>", "<span class='reminder_highlight'>"+ YOHO._('redeem_goods', '換購產品') +"</span>")
		}
		else if(type == "package"){
			body = "<span class='reminder_bold'>" + product + "</span><br><br>" + sprintf(YOHO._('cart_add_success_package', '%s 件組合產品已加入購物車'), "<span class='reminder_highlight'>" + addQty + "</span>")
		}
		else{
			body = "<span class='reminder_bold'>" + product + "</span><br><br>" + YOHO._('add_goods_success', '已成功加入購物車');
		}

		html = ''+
		'<div class="msg_dialog cle" style="width: 400px; padding-bottom:20px; box-sizing:initial;">'+
			'<div class="msg-content">'+
				'<div class="msg-body">'+
					'<p></p>'+
					'<div class="row">'+
						'<div class="left">'+
							//'<span class="glyphicon glyphicon-ok-circle" style="font-size: 36px; padding: 2px; color: #00acee;"></span>'+
							'<svg class="dialog-icon">'+
								'<use xlink:href="/yohohk/img/yoho_svg.svg#dialog-ok"></use>'+
							'</svg>'+
						'</div>'+
						'<div class="right">'+
							body+
						'</div>'+
					'</div>'+
			'</div>'+
		'</div>';
		if(typeof affiliate_on === "undefined"){
			if(YOHO.affiliateon==1){
				affiliate_on = 1;
			}
		}

		_dialog_box = $.dialog({
			title: null,
			lock: true,
			fixed: true,
			padding: '0',
			id: 'login',
			content: html,
			init: function() {
				setTimeout(function() {
					$('.dialog_login .aui_close').trigger('click');
				}, 700);

				/*
				setTimeout(function() {
					$('.aui_close').trigger('click');
				}, 3000);
				*/
			}
		});
	},

	showLogin: function(affiliate_on) {
		YOHO.common.showLogin(affiliate_on);
	},

	showRegister: function(affiliate_on) {
		YOHO.common.showRegister(affiliate_on);
	},

	showRelated: function(){
		YOHO.common.showRelated();
	}
};

YOHO.common = {

	// 懸浮定位方法
	fixed: function($elem) {
		var stopFoot = true,
		$footer = $('#footer');

		if(arguments[1] === false)
		{
			stopFoot = false;
		}

		$elem.each(function() {
			var $this = $(this),
			_height = $this.height(),
			_oTop = $this.offset().top;

			$(window).on("scroll", function () {
				var _sTop = $(this).scrollTop(),
				_ftop = $footer.offset().top-_height-50;
				if (_sTop > _oTop)
				{
					if (_sTop > _ftop && stopFoot)
					{
						$this.css({
							'top': _ftop,
							'position': 'absolute'
						});
						return;
					}
					$this.addClass('fixed');
					if (YOHO.check.isIE6)
					{
						$this.css({
							'top': _sTop,
							'position': 'absolute'
						});
					}
					else
					{
						$this.css({
							'top': 0,
							'position': 'fixed'
						});
					}
				}
				else
				{
					$this.removeClass('fixed');
					$this[0].style.top = '';
					$this[0].style.position = '';
				}
			});
		});
	},

	//延遲加載圖片
	lazyload: function() {
		var $win = $(window),
			winheight = $win.height(),
			$images = null,
			_timeout = null,
			length = 0;

		$images = $("img").filter(function() {
			return $(this).attr("original") !== undefined;
		});

		length = $images.length;

		if(length === 0)
		{
			return;
		}

		function showImg()
		{
			$images.each(function () {
				var _this = $(this),
					_src = _this.attr("original");
				if (_src === '')
				{
					return;
				}
				if (_this.offset().top <= (winheight + $win.scrollTop() + 50))
				{
					_this.hide().attr('src', _src).fadeIn();
					_this.attr("original", "");
					length--;
				}
			});
		}

		showImg();
		$win.on("scroll", function () {
			if(length > 0){
				if (_timeout !== null)
				{
					clearTimeout(_timeout);
				}
				_timeout = setTimeout(function(){
					showImg();
				}, 50);
			}
		});
	},
	deferImage : function() {
		var $win = $(window);
		$win.load(function() {
			var imgDefer = $('.defer-image');
			for (var i=0; i<imgDefer.length; i++) {
				//console.log('.defer-image....');
				if(imgDefer[i].getAttribute('data-src')) {
				  imgDefer[i].setAttribute('src',imgDefer[i].getAttribute('data-src'));
				}
			}
		});
	},
	//操作cookie
	cookie : function(name, value, options) {
		if (typeof value != 'undefined') {
			// 有value值, 设置cookie
			options = options || {};
			if (value === null) {
				value = '';
				options.expires = -1;
			}
			var expires = '';
			if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
				var date;
				if (typeof options.expires == 'number') {
					//options.expires以小时为单位
					date = new Date();
					date.setTime(date.getTime() + (options.expires * 60 * 60 * 1000));
				} else {
					date = options.expires;
				}
				expires = '; expires=' + date.toUTCString();
			}
			var path = options.path ? '; path=' + options.path : '; path=/';
			var domain = options.domain ? '; domain=' + options.domain : '';
			var secure = options.secure ? '; secure' : '';
			document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
		} else {
			// 只有name值, 获取cookie
			var cookieValue = null;
			if (document.cookie && document.cookie !== '') {
				var cookies = document.cookie.split(';');
				for (var i = 0; i < cookies.length; i++) {
					var cookie = jQuery.trim(cookies[i]);
					if (cookie.substring(0, name.length + 1) == (name + '=')) {
						cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
						break;
					}
				}
			}
			return cookieValue;
		}
	},
	getRelatedArticles: function(goods_id) {	
		$.ajax({
			url: '/ajax/getRelatedArticles.php',
			type: 'post',
			data: {
				goods_id: goods_id
			},
			dataType:'json',
			success: function(data) {
				var promo_links_list = data.promo_links_list;
				var promo_links_list_len = promo_links_list.length;
				var topic_list = data.topic_list;
				var topic_list_len = topic_list.length;
				var article_list = data.article_list;
				var article_list_len = article_list.length;
				var html = '';
				if (promo_links_list_len!=0 || topic_list_len != 0 || article_list_len !=0) {
					html += '<div class="goods_article_header">' + YOHO._('related_articles', '相關文章') + '</div>' +
							'<table class="goods_article_table" style="border-collapse: collapse; margin: 0;width: 100%">';
					for (var i=0; i<promo_links_list_len; i++) {
						var link_img = promo_links_list[i].link_img;
						var link_url = promo_links_list[i].link_url;
						var link_newtab = promo_links_list[i].link_newtab;
						var link_title = promo_links_list[i].link_title;
						var link_desc = promo_links_list[i].link_desc;
						html += '<tr><td>';
						if (link_img) {
							if (link_url) {
								if (link_newtab) {
									html += '<a href="' + link_url + '" target="_blank">';
								} else {
									html += '<a href="' + link_url + '">';
								}		
							} 
							html += '<img src="' + link_img + '" title="' + link_title + '" >';	
							if (link_url) {
								html += '</a>'
							}
						}
						html += '</td><td>';
						if (link_url) {
							if (link_newtab) {
								html += '<a href="' + link_url + '" target="_blank">';
							} else {
								html += '<a href="' + link_url + '">';
							}	
						}
						html += link_title;
						if (link_url) {
							html += '</a>';
						}
						if (link_desc) {
							html += '<br>' +link_desc;
						}
						html += '</td></tr>';
					}

					if (promo_links_list_len!=0 && (topic_list_len != 0 || article_list_len !=0)) {
						html += '<tr class="hr" style="border-bottom:1px solid #f5f5f5;"><td colspan="2"></td></tr>';
					}

					for (var i=0; i<topic_list_len; i++) {
						var url = topic_list[i].url;
						var title = topic_list[i].title;
						var topic_id = topic_list[i].topic_id;
						html += '<tr><td>';
						html += '<a href="' + url + '" title="' + title + '"><img src="/data/afficheimg/topic/' + topic_id + '_icon.png" ></a>';
						html += '</td><td>';
						html += '<a href="' + url + '">' + title + '</a>';
						html += '</td></tr>';
						
					}
					if (topic_list_len != 0 && article_list_len !=0) {
						html += '<tr class="hr" style="border-bottom:1px solid #f5f5f5;"><td colspan="2"></td></tr>';
					}
					for (var i=0; i<article_list_len; i++) {
						var url = article_list[i].url;
						var title = article_list[i].title;
						html += '<tr><td>';
						html += '<a href="' + url + '" title="' + title + '"><img src="/yohohk/img/product-article.png" ></a>';
						html += '</td><td>';
						html += '<a href="' + url + '">' + title + '</a>';
						html += '</td></tr>';	
					}
					html += '</table>';	
					$(html).insertBefore(".social-btn");
				}
				
			},
			error: function(xhr) {
				YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
			}
		});
	},
	//根據level1 cat_id，得到該分類下所有的level2, level3 cat and brand
	ajaxGetCat: function(cur_cat_id,$subCata) {
		if(typeof cur_cat_id ==='undefined'){
			return;
		}
		$.ajax({
			url: '/ajax/getCategory.php',
			type: 'post',
			data: {
				cat_id: cur_cat_id
			},
			dataType:'json',
			success: function(data) {
				var bottom_cat = data.bottom_cat;
				var level2_length = bottom_cat.length;
				var brands = data.brands;
				var brands_length = brands.length;
				var html = '<div class="J_subView cle" id="subView'+cur_cat_id+'" style="display:block;">';
				html += '<img class="category-feature-image" src="/data/afficheimg/category/'+cur_cat_id+'_menu.png" onerror="this.style.display='+"'none'"+';">';
				html += '<div class="subcategories-lists cle">';
				for (var i=0; i<level2_length; i++) {
					var cat = bottom_cat[i];
					var children_no = cat["children"].length;
					html += '<div class="subcategories-list">';
					if (children_no == 0) {
						html += '<div class="subcategories-list-left">' +
									'<a href="'+cat.url+'"><span class="subcategories-list-tittle">'+cat.cat_name+'</span></a>' +
								'</div>' +
								'<div class="subcategories-list-right">' +
									'<a class="subcategories-list-item" href="'+cat.url+'">'+cat.cat_name+'</a>' +
								'</div>';

					} else {
						html += '<div class="subcategories-list-left">' +
									'<a href="'+cat.url+'"><span class="subcategories-list-tittle">'+cat.cat_name+'</span></a>' +
								'</div>' +
								'<div class="subcategories-list-right">';
						var child_length = cat.children.length;
						for (var j=0; j<child_length; j++) {
							html += '<a class="subcategories-list-item" href="'+cat.children[j].url+'">'+cat.children[j].cat_name+'</a>'
						}
						html += '</div>';
					}
					html += '</div>';
				}
				html += '</div>';
				html += '<div class="category-brands-list">';
				for (var k=0; k<brands_length && k <7; k++) {
					var brand = brands[k];
					html += '<a href="' + brand.url + '">' +
								'<img src="/data/brandlogo/'+brand.brand_logo+'" alt="'+brand.brand_name+'" title="'+brand.brand_name+'">' +
							'</a>';
				}
				html += '</div></div>';
				$subCata.html(html);
			},
			error: function(xhr) {
				YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
			}
		});
	},

	// 主導航欄滑動效果
	mainNav_animate: function() {
		var $mainCata = $("#J_mainCata"),
			$subCata = $("#J_subCata"),
			$mainNav = $("#main_nav"),
			_time = null,
			_hover = null,
			isHover = false,
			isIndex = false,
			indexHeight = 0;

		//if($('#index-slide').length > 0)
		if (YOHO.hasOwnProperty('homePage'))
		{
			isIndex = true;
			indexHeight = $mainCata.height();
		}
		$mainCata.find('ul').menuAim({
			activate: function (row) {
				var $this = $(row),
					_index = $this.index();
				var cur_cat_id = $this.data("cat-id");
				$subView = $subCata.find("#subView"+cur_cat_id);
				$subCata.find(".J_subView").hide();
				// if (!$subView.length) {
					YOHO.common.ajaxGetCat(cur_cat_id,$subCata);
				// } else {
					// $subView.show();
				// }
				if (isHover)
				{
					$this.addClass("current").siblings('li').removeClass("current");
				}
				else
				{
					// if (_hover !== null)
					// {
					// 	clearTimeout(_hover);
					// }
					// _hover = setTimeout(function(){
						isHover = true;
						$mainNav.addClass('main_nav_hover');
						$this.addClass("current").siblings('li').removeClass("current");
						$subCata.css({"display":"block",'height':$mainCata.find('ul').height()}).animate({left:200},200);
					// }, 200);
				}
			},
			exitMenu: function() {
				// if (_hover !== null)
				// {
				// 	clearTimeout(_hover);
				// }
			}
		});

		$mainNav.on("mouseenter",function() {
			if (_time !== null)
			{
				clearTimeout(_time);
			}
			cur_cat_id = $(this).data("cat-id");
			var mouseenterFunc = function() {
				$mainNav.addClass('main_nav_hover');
				$mainCata.stop().show().animate({'opacity':1,'height':$mainCata.find('ul').height()},360);
				
			};
			if (isIndex)
			{
				mouseenterFunc();
			}
			else
			{
				_time = setTimeout(mouseenterFunc, 200);
			}
		}).on("mouseleave",function(){
			if (_time !== null)
			{
				$subCata.find(".J_subView").hide();
				clearTimeout(_time);
			}
			//_time = setTimeout(function(){
				$subCata.css({"left":0,"display":"none"});
				isHover = false;
				if (!isIndex)
				{
					$mainCata.stop().delay(200).animate({'opacity':0,'height':0},360, function(){
						$mainNav.removeClass('main_nav_hover');
						$mainCata.hide().find("li").removeClass('current');
					});
				}
				else
				{
					$mainNav.removeClass('main_nav_hover');
					$mainCata.find("li").removeClass('current');
					$mainCata.stop().delay(200).animate({'height':indexHeight}, 360);
				}
				$subCata.hide();
			//}, 200);
		});
	},

	// Search bar in page header
	searchBar: function() {
		var $s_form = $("#search_fm");
		if (!$s_form.length)
		{
			return;
		}

		var $hotkey = $('<dl class="search-hotkey">' +
		'<dt>' + YOHO._('global_search_popular', '熱門') + ' : </dt><dd>' +
		'<a href="/keyword/Paperwhite">Paperwhite</a>' +
		'<a href="/keyword/Kindle">Kindle</a>' +
		'<a href="/keyword/Nexus">Nexus</a>' +
		'<a href="/keyword/Hitachi">Hitachi</a>' +
		'<a href="/keyword/Fuelband">Fuelband</a>' +
		'<a href="/keyword/iPad">iPad</a>' +
		'<a href="/keyword/Macbook">Macbook</a>' +
		'<a href="/keyword/Fitbit">Fitbit</a>' +
		'<a href="/keyword/Refa">Refa</a>' +
		'<a href="/keyword/Kobo">Kobo</a>' +
		'</dd></dl>');
		var populateKeywords = function (data) {
			YOHO.searchkeywords = data;

			var keyword_list = '';
			$.each(data,function (idx, val) {
				var keywords = val.split(" ");
				var keywords_len = keywords.length;
				var keysords_string = "";
				for (var i = 0; i<keywords_len; i++) {
					var keyword = keywords[i];
					if (i < keywords_len-1) {
						if (keyword != "") {
							keysords_string += keyword + "+";
						} else {
							keysords_string += "+";
						}
					} else {
						keysords_string += keyword;
					}
				}
				keyword_list += '<a href="/keyword/' + keysords_string + '" onClick="ga(\'send\', \'pageview\', \'keyword\/' + keysords_string + '\' );"  >' + val + '</a>';
			});
			$hotkey = $('<dl class="search-hotkey"><dt>' + YOHO._('global_search_popular', '熱門') + ' : </dt><dd>' + keyword_list + '</dd></dl>');

			// For cart page
			if ($('.key-hot'))
			{
				$('.key-hot p a').remove();
				var cart_keyword_list = '';
				var i = 0, c = data.length;
				c = c > 3 ? 3 : c;
				for (i = 0; i < c; i++)
				{
					cart_keyword_list += '<a href="/keyword/' + encodeURIComponent(data[i]) + '" onClick="ga(\'send\', \'pageview\', \'keyword\/' + encodeURIComponent(data[i]) + '\' );" >' + data[i] + '</a><br>';
				}
				$('.key-hot p').append($(cart_keyword_list));
			}
		};

		if (YOHO.searchkeywords && $.isArray(YOHO.searchkeywords) && YOHO.searchkeywords.length)
		{
			// Populate the list of popular keywords if provided
			populateKeywords(YOHO.searchkeywords);
		}
		else
		{
			// Ask server for the list of popular keywords, instead of just using the hard-coded list above
			$.ajax({
				url: '/ajax/search_popular.php',
				dataType: 'json',
				success: populateKeywords
			});
		}

		var $sInput = $s_form.find('input.sea_input');
		var $result = $('<div class="search_result"></div>').appendTo('body');
		var placeholder_text = YOHO._('global_search_placeholder', '搜尋您喜愛的電子產品 (e.g. Kindle)');
		var cache = {}, t = null, li_Index = -1, _offset = null;

		$s_form.submit(function() {
			var input = $.trim($sInput.val());
			if (!input || input == placeholder_text)
			{
				// location.href = "/search.php";
				YOHO.dialog.warn(YOHO._('global_search_alert', '請輸入關鍵字'));
				return false;
			}
		});
		var $mini_s_form = $("#mini_search_fm");
		$mini_s_form.submit(function() {
			var input = $.trim($mini_s_form.find('input.sea_input').val());
			if (!input || input == placeholder_text)
			{
				// location.href = "/search.php";
				YOHO.dialog.warn(YOHO._('global_search_alert', '請輸入關鍵字'));
				return false;
			}
		});
		$sInput.attr('autocomplete','off').on('focus',function () {
			var key = $.trim($sInput.val());
			_offset = $sInput.offset();
			bind_clickEvent();
			if (key === '')
			{
				return false;
			}
			if (key == placeholder_text)
			{
				$sInput.val('');
				$hotkey.css({'left':_offset.left,'top':_offset.top}).appendTo('body');
			}
			else
			{
				if (cache[key] !== undefined)
				{
					showResult(key);
				}
				else
				{
					ajaxSearch(key);
				}
			}
			return false;
		}).on('keyup',function(e) {
			if (e.keyCode != 40 && e.keyCode != 38 && e.keyCode != 13)
			{
				clearTimeout(t);
				t = setTimeout(function() {
					var key = $.trim($sInput.val());
					$hotkey.remove();
					if(cache[key] !== undefined)
					{
						showResult(key);
					}
					else
					{
						ajaxSearch(key);
					}
				}, 300);
			}
		}).blur(function() {
			if(!$.trim($sInput.val()))
			{
				$sInput.val(placeholder_text);
			}
		}).on('keydown',function (e) {
			var li = $result.find("li"),
				_len = li.length;
			if (_len > 0)
			{
				switch (e.keyCode)
				{
					case 40: // 下
						li_Index++;
						if(li_Index > _len-1)
						{
							li_Index = 0;
						}
						li.removeClass("on").eq(li_Index).addClass("on");
						$sInput.val(li.eq(li_Index).text());
						break;
					case 38: // 上
						li_Index--;
						if(li_Index < 0)
						{
							li_Index = _len - 1;
						}
						li.removeClass("on").eq(li_Index).addClass("on");
						$sInput.val(li.eq(li_Index).text());
					break;
				}
			}
		});

		function ajaxSearch(key)
		{
			$.ajax({
				url: '/ajax/search_suggestions.php',
				data: {'q':key},
				success: function(result) {
					cache[key] = result;
					showResult(key);
				}
			});
		}

		function showResult(key)
		{
			var html='<ul>',
			data = cache[key];
			if (data[0] !== '')
			{
				$.each(data,function (i,v) {
					html += '<li>'+v+'</li>';
				});
				$result.html(html).css({'left':_offset.left, 'top':_offset.top, 'display':'block'});
				li_Index = -1;
			}
			else
			{
				$result.hide();
			}
		}

		function bind_clickEvent()
		{
			$(document).on("click",function(e) {
				if (($(e.target).parents(".search-hotkey").length === 0) && e.target.className != 'search-hotkey' && e.target.id != 'textfield' &&  e.target.className != 'sea_input')
				{
					$hotkey.remove();
					// console.log('rm');
				}
				if (e.target.id != 'textfield')
				{
					$result.hide();
				}
			});
			$result.on('mouseenter','li',function() {
				var _li = $(this);
				li_Index = _li.index();
				_li.addClass('on').siblings('li').removeClass('on');
				return false;
			}).on('click','li',function() {
				$sInput.val($(this).text());
				setTimeout(function() {
					$s_form.submit();
				}, 300);
			});
		}
	},

	// 頁頭顯示用戶信息
	showLoginInfo: function() {
		var _showLoginInfo = function(result) {
			YOHO.userInfo = result;
			var $userlink = $('div.more-menu[data-type="userinfo"] a.menu-link');
			var $userSpan = $userlink.find('span');
			if (result.login)
			{
				$userSpan.text(YOHO._('global_header_menu_user_name_message', '歡迎，') + result.username);
				$userlink.attr('href','/user.php');
			}
			else
			{
				// Handle register newsletter events (on homepage only)
				if (YOHO.hasOwnProperty('homePage'))
				{
					YOHO.homePage.regEmail();
				}
			}

			// Make font size smaller if there's not enough space to display the username
			var $tmpName = $('<span style="visibility: hidden;">'+$userSpan.text()+'</span>').appendTo('body');
			if (($tmpName.width() > 78) && ($tmpName.width() < 100))
			{
				$userSpan.css('font-size', (78 / $tmpName.width()) + 'em');
			}
			$tmpName.remove();

			YOHO.common.headerMenu.init();
		};

		if (YOHO.userInfo.initial)
		{
			$.ajax({
				url: '/ajax/loginAuth.php',
				cache: false,
				xhrFields: { withCredentials: true },
				dataType:'json',
				success: _showLoginInfo
			});
		}
		else
		{
			_showLoginInfo(YOHO.userInfo);
		}
	},

	headerMenu: {
		init: function () {
			var headerMenu = this;
			$('#userinfo-bar,#userinfo-bar-fix,#fbmsg-bar').on('mouseenter','div.more-menu',function() {
				var _this = $(this),
					type = _this.data('type');
				var _time = _this.data('_time');
				if(_time)
				{
					clearTimeout(_time);
					_this.removeData('_time');
				}
				else
				{
					_time = setTimeout(function(){
						_this.removeData('_time');
						if(type == 'userinfo')
						{
							if (!YOHO.userInfo.login) return;
							headerMenu.show_userinfo(_this);
						}
						else if(type == 'facebook')
						{
							headerMenu.show_facebook(_this);
						}
						else if(type == 'weixin')
						{
							headerMenu.show_weixin(_this);
						}
						else if(type == 'social')
						{
							headerMenu.show_social(_this);
						}
						else if(type == 'language')
						{
							headerMenu.show_language(_this);
						}
						else if(type == 'cart')
						{
							headerMenu.show_cart(_this);
						}
						_this.addClass('hover');
					}, 300);
					_this.data('_time', _time);
				}
			}).on('mouseleave','div.more-menu',function() {
				var _this = $(this);
				var _time = _this.data('_time');
				if(_time)
				{
					clearTimeout(_time);
					_this.removeData('_time');
				}
				else
				{
					_time = setTimeout(function(){
						_this.removeClass('hover');
						_this.find('.more-bd').slideUp('fast', function (){
							if (_this.data('addedToCart'))
							{
								_this.removeData('addedToCart');
								$(this).remove();
							}
						});
						_this.removeData('_time');
					}, 300);
					_this.data('_time', _time);
				}
			});
		},

		show_userinfo: function($div) {
			if (!YOHO.userInfo.login) return;

			if($div.find('.more-bd').length === 0)
			{
				$div.append('<div class="more-bd usernav-bd" style="display: none;"><div class="list">' +
				'<a>' + YOHO._('global_header_menu_user_points', '我的積分：') + YOHO.userInfo.jifen + '</a>' +
				'<a href="/user.php?act=order_list">' + YOHO._('global_header_menu_user_orders', '我的訂單') + '</a>' +
				'<a href="/user.php?act=profile">' + YOHO._('global_header_menu_user_profile', '修改個人資料') + '</a>' +
				'<a href="/user.php?act=logout" class="last">' + YOHO._('global_header_menu_user_logout', '登出') + '</a></div></div>');
			}

			$div.find('.more-bd').slideDown('fast');
		},

		show_facebook: function($div) {
			if($div.find('.more-bd').length === 0)
			{
				$div.append('<div class="more-bd newmsg-menu-bd" style="display: none;" data-loaded="0"><div class="yoho-load"></div></div>');
				$.ajax({
					url: '/ajax/fb_popup_box.php',
					cache: false,
					dataType: 'html',
					success: function(data){
						$div.find('.more-bd').html(data).data('loaded',1).slideDown('fast');
					}
				});
			}
			else if ($div.find('.more-bd').data('loaded'))
			{
				$div.find('.more-bd').slideDown('fast');
			}
		},

		show_weixin: function($div) {
			$div.find('.more-bd').slideDown('fast');
		},

		show_social: function($div) {
			$div.find('.more-bd').slideDown('fast');
		},

		show_language: function($div) {
			$div.find('.more-bd').slideDown('fast');
		},

		show_cart: function($div) {
			// Bind event for the cart menu
			var bindEvents = function ($morebd) {
				$morebd.find('a.del_btn').click(function(e) {
					deleteGood($(this));
					e.preventDefault();
				});
				$morebd.find('.adjust-item').find('span').click(function(e) {
					var $this = $(this);
					var $input = $this.siblings('input:text');
					var limit = $input.data('limit') ? parseInt($input.data('limit')) : 20;
					var num = $input.val();
					var $delBtn = $this.closest('.good-action').find('a.del_btn');
					var $unitCount = $this.closest('.good-row').find('.good-unit-count');

					if ($this.hasClass('add')){
						num++;
					} else {
						num--;
					}
					
					if (num >= limit) {
						$this.addClass('disabled');
						num = limit;
						showNumLimit($input, limit);
					}

					else if (num <= 0)
					{
						if (confirm(YOHO._('cart_remove_prompt', '您確定要從購物車中移除該產品嗎？')))
						{
							deleteGood($delBtn);
							e.preventDefault();
						}
						else
						{
							num++;
						}
					}
					updateGood($delBtn, num);
					$input.val(num).data('oval', num);
					$unitCount.text(num);
				});
				$morebd.find("dl").hover(function() {
					$(this).css({"backgroundColor":"#f1f1f1"});
				},function(){
					$(this).css({"backgroundColor":"#ffffff"});
				});
				$morebd.data('binded',1);
			};

			var loadFromServer = function ($div) {
				$.ajax({
					url: '/ajax/cart_popup_box.php',
					cache: false,
					dataType: 'html',
					success: function(data) {
						$morebd = $div.find('.more-bd');
						$morebd.html(data).data('loaded',1).data('binded',0);
						bindEvents($morebd);
						$morebd.slideDown('fast');
					}
				});
			};

			//update amount of good
			function updateGood($elem, $no)
			{
				var id = $elem.data('id');
				var no = $no;
				$.ajax({
					url: '/cart?step=update_cart&ajax=y&lineItemId='+id+'&goods_number='+no+'&countItem==y',
					type: 'post',
					dataType: 'json',
					success: function(result){
						if (result.error){
							//if (result.hasOwnProperty('message')){
							//	error_msg = result.message;
							//	console.log('error return:' + error_msg);
							//}
						} else {
							getCount();
							loadFromServer($div);
						}
					},
					error: function (xhr){
						console.log('updateGood error:' + JSON.stringify(xhr));
					}
				});
			}

			//get total count & total price of pop up cart
			function getCount()
			{
				$.ajax({
					url: '/ajax/cart_popup_box.php?total_update=y',
					type: 'post',
					cache: false,
					dataType: 'json',
					success: function(result){
						if (result.error){
							//if (result.hasOwnProperty('message')){
							//	error_msg = result.message;
							//	console.log('error return:' + error_msg);
							//}
						} else {
							$('#panel-cartnum').text(result.totalCount);
							$('.top-bar-cart-num').text(result.totalCount);
							$('#panel-price').text(result.totalPrice);
						}
					},
					error: function (xhr){
						console.log('updateGood error:' + JSON.stringify(xhr));
					}
				});
			}


			//刪除產品
			function deleteGood($elem)
			{
				var id = $elem.data('id');
				$.ajax({
					url: '/cart?step=drop_goods&ajax=y&id='+id+'',
					type: 'post',
					dataType: 'json',
					success: function(result){
						var $dl = $elem.parents('.good-row');
						if($elem.data('taocan'))
						{
							location.reload();
						}
						else
						{
							$dl.animate({'height':0, 'padding-top':0, 'padding-bottom':0}, 300, function() {
								$dl.remove();

								$('.hd-cartnum').text(result.totalCount);
								$('#panel-cartnum').text(result.totalCount);
								$('#panel-price').text((result.totalPrice).toFixed(2));
								$('.top_fix_bar .red').text(result.totalCount);
								YOHO.flashdealCartTimer = result.expired;
								if(Object.keys(result.expired).length > 0) {
									YOHO.common.flashdealCartCountDown();
								}

								if (result.totalCount == 0)
								{
									// load again to display empty page
									loadFromServer($div);
									$('.flashdeal-cart-countdown').remove();
								}
							});
						}
					},
					error: function (xhr){
						console.log('deleteGood error:' + JSON.stringify(xhr));
					}
				});
			}

			var $morebd = $div.find('.more-bd');
			if($morebd.length === 0)
			{
				$div.append('<div class="more-bd cart-bd" style="display: none;" data-loaded="0"><div class="yoho-load"></div></div>');
				loadFromServer($div);
			}
			else if ($morebd.data('loaded'))
			{
				if (!$morebd.data('binded'))
				{
					bindEvents($morebd);
				}
				$div.find('.more-bd').slideDown('fast');
			}

			function showNumLimit($input, _limit){
				$('<div class="numlimit-tip"><i>&diams;</i><i class="btm">&diams;</i><p>' + sprintf(YOHO._('cart_update_limit', '最多只能購買%s件'), _limit) + '</p></div>').insertAfter($input).delay(2000).fadeOut(function() { $(this).remove(); });
			}
		}
	},

	// 頁頭廣告語輪播
	hdIntro_Slide: function() {
		$("#hd-intro").switchable({
			panels: 'li',
			effect: 'scrollUp',
			triggers: false,
			interval: 3,
			autoplay: true,
			end2end: true
		});
	},

	//返回頂部
	back2top: function () {
		var $win = $(window),
			_wHeight = $win.height();

		var $back2top = $('<div class="back2top"><a class="back2btn" hidefocus=true href="javascript:;"><img alt="Back to Top" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><em></em></a></div>').appendTo("body");

		$win.scroll(function () {
			var _sTop = 0;
			if (YOHO.check.isIE6) {
				_sTop = $win.scrollTop();
				$back2top.css({
					'top' : _sTop + _wHeight - 89
				});
			}
		});
		$back2top.find(".back2btn").click(function () {
			$('body,html').animate({ scrollTop : 0 }, 500);
			return false;
		});
	},

//	bookmarkYoho: function () {
//		// Bookmark YOHO Hong Kong
//		var $favorite_yoho = $('#favorite_yoho');
//		$favorite_yoho.attr({'rel':'sidebar'});
//		$favorite_yoho.on('click',function() {
//			var _url = "http://www.yohohongkong.com/";
//			var _title = "友和 YOHO";
//			var browser = {
//				IE : /MSIE/.test(window.navigator.userAgent) && !window.opera,
//				FF : /Firefox/.test(window.navigator.userAgent),
//				OP : !!window.opera
//			};
//
//			if(browser.IE)
//			{
//				// IE Add to Favourite
//				window.external.AddFavorite( _url, _title);
//			}
//			else if (browser.FF || window.sidebar)
//			{
//				// Mozilla Firefox Bookmark
//				$('#favorite_yoho').attr({'title': _title, 'href': _url});
//				window.sidebar.addPanel(title, url, "");
//			}
//			//else if(browser.OP) // Opera 7+
//			else
//			{
//				YOHO.dialog.warn("請使用 Ctrl+D (Windows) / Cmd+D (Mac) 快捷鍵把本網站加入書籤");
//			}
//		});
//	},

	// 返回時間JSON，參數為秒
	timeJson: function(times) {
		var oTime = {};
		oTime.secs = Math.floor(times % 60);
		oTime.mins = Math.floor(times / 60 % 60);
		oTime.hours = Math.floor(times / 60 / 60);
		oTime.days = 0;

		if(oTime.hours > 23)
		{
			oTime.days = Math.floor(oTime.hours/24);
			oTime.hours = oTime.hours - oTime.days*24;
		}
		if(oTime.secs<10)
		{
			oTime.secs = '0' + oTime.secs;
		}
		if(oTime.mins<10)
		{
			oTime.mins = '0' + oTime.mins;
		}
		if(oTime.hours<10)
		{
			oTime.hours = '0' + oTime.hours;
		}
		return oTime;
	},

	buyGoods: function() {

		// 批發模式等加入購物車
		$('.productlist').on('change','input[name=pifa]',function(){
			var _input = $(this),
			//_input = _this.find('input[name=pifa]'),
			_ac =_input.data('ac'),
			_sid = _input.data('skuid'),
			_kc  = _input.data('kc'),
			_num = _input.val(),
			_name=_input.data('name');

			$.ajax({
				url: '/ajax/cartbuy.php',
				type: 'post',
				dataType: 'json',
				data: {'id':_sid, 'gnum':_num , 'ac':_ac},
				success: function(data) {
					if(data.status == 1)
					{
						YOHO.dialog.success(YOHO._('global_cart_add_success', '加入購物車成功！') + '<br/> '+_name+'*'+_num+'');
					}
					else if (data.status == 999)
					{
						YOHO.dialog.warn(sprintf(YOHO._('global_cart_quantity_exceeded', '您購買了%d個，超過購買上限<b>%d</b>個，加入購物車失敗。'), _num, _kc));
					}
					else
					{
						YOHO.dialog.warn(YOHO._('global_cart_add_failed', '加入失敗，請稍後再試。'));
					}
				},
				error: function(xhr){
					YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
				}
			});
		});

		// 零售模式等加入購物車
		//$('.productlist').on('click','.addcart',function() {
		$('.productlist .addcart, .product-addtocart').click(function () {
			var _input = $(this),
			_ac ='ot',
			_sid = _input.data('skuid'),
			_num = 1,
			_parent = _input.data('parentGid') || 0;
			_price =  _input.data('price') || 0;
			_prodname = _input.parent().parent().parent().find("img:first-child()").prop('alt');
			$.ajax({
				url: '/ajax/cartbuy.php',
				type: 'post',
				dataType: 'json',
				data: {'id':_sid, 'gnum':_num, 'ac':_ac, 'parent':_parent},
				success: function(data) {
					if(data.status == 1)
					{
						YOHO.dialog.success(YOHO._('global_cart_add_success', '加入購物車成功！'));

						var addCart_txt = YOHO._('global_cart_added_text', [
			        		"快D帶我番屋企啦 :)",
			        		"您真系識貨喎 XD"
						]);

						var num = data.count,
							_top = 50,
							html = '',
							ok_txt = '';

						$('.hd-cartnum').text(num);
						$('.top_fix_bar .red').text(num);

						ok_txt = addCart_txt[parseInt(Math.random() * addCart_txt.length)];

						html = '<div class="panel" style="top:'+_top+'px;" id="addcart-success">' +
								'<div class="panel_tit">' + YOHO._('global_cart_added_popup_title', '成功加入購物車') + '</div>'+
								'<div class="panel_con addcart-success-info">'+
								'<p class="ok-txt">'+ok_txt+'</p>'+
								'<p>' + sprintf(YOHO._('global_cart_added_popup_item_count', '購物車共有 %s 件產品'), '<b class="red">'+data.count+'</b>') + '</p>'+
								//(data.totalPrice < 1000 ? '<p class="gray">購物滿$1000免運費</p>' : '')+
								'<p>' + sprintf(YOHO._('global_cart_added_popup_total_amount', '合計：%s'), '<b class="red">HK$'+(data.totalPrice).toFixed(2)+'</b>') + '</p>'+
								'<div class="gocart"><a href="/cart" class="btn">' + YOHO._('global_cart_added_popup_go_cart_btn', '去購物車結算') + '</a></div>'+
								'</div></div>';

						var $cart_btn = $('div.more-menu[data-type="cart"]');
						var $fix_cart_btn = $('div.more-menu[data-type="fix-cart"]');
						$cart_btn.addClass('hover').data('addedToCart',1);

						if ($cart_btn.find('.more-bd').length === 0)
						{
							$cart_btn.append('<div class="more-bd cart-bd" style="display: none;" data-loaded="1"></div>');
						}

						$cart_btn.find('.more-bd').html(html).slideDown('fast');
						$fix_cart_btn.find('.more-bd').html(html);
						var _time = $cart_btn.data('_time');
						if (_time)
						{
							clearTimeout(_time);
							$cart_btn.removeData('_time');
						}

						_time = setTimeout(function() {
							clearTimeout(_time);
							$cart_btn.removeData('_time');

							$cart_btn.find('.more-bd').slideUp('fast', function() {
								$cart_btn.removeClass('hover');
								$(this).remove();
							});
						}, 5000);
						$cart_btn.data('_time', _time);
						var country = (YOHO.userInfo.hasOwnProperty('area') && YOHO.userInfo.area) ? YOHO.userInfo.area.toLowerCase() : 'hk';
						if(country != "cn") {
							//GA - ecommerce
							ga('require','ec');
							ga('ec:addProduct', {
								'id': _sid,
								'name': _prodname,
								'quantity': 1
							});
							ga('ec:setAction', 'add');
							ga('send', 'event', 'UX', 'click', 'add to cart');
							// Facebook Pixel - ecommerce - add to cart
							fbq('track', 'AddToCart', {
								content_ids: [""+_sid+""],
								content_type: 'product',
								value: _price,
								currency: 'HKD'
							});
						}
					}
					else if(data.status == 999)
					{
						YOHO.dialog.warn(sprintf(YOHO._('global_cart_quantity_exceeded', '您購買了%d個，超過購買上限<b>%d</b>個，加入購物車失敗。'), _num, _kc));
					}
					else if (data.hasOwnProperty('text') && data.text.length > 0)
					{
						YOHO.dialog.warn(data.text);
					}
					else
					{
						YOHO.dialog.warn(YOHO._('global_cart_add_failed', '加入失敗，請稍後再試。'));
					}
				},
				error: function(xhr) {
					YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
				}
			});
		});
	},

	// 倒數計時，包括顯示毫秒跳動
	countDown: function(millisecond, options) {
		var _this = this, _dot = 9, mytime = null,
		_time = Math.floor(millisecond/1000);
		options = $.extend({
			timeDiv: $('#countdown em'), //倒計時顯示區域
			dotTime: false  //是否顯示毫秒
		},options);

		var $time = options.timeDiv;
		var timeDown = function (times) {
			var temp = _this.timeJson(times);
			$time.html(temp.hours+':'+temp.mins+':'+temp.secs);
		};

		timeDown(_time);
		mytime = setInterval(function() {
			if(_time>0){
				timeDown(--_time);
			}else{
				clearInterval(mytime);
				location.reload();
			}
		}, 1000);

		if(options.dotTime) {
			var mydot = null,
				$dot = $('<i class="dottime">.9</i>').insertAfter($time);
			mydot = setInterval(function(){
				if(_dot < 0){
					_dot=9;
				}
				$dot.text('.'+_dot);
				_dot--;
			}, 100);
		}
	},

	// 會員登入
	showLogin: function(affiliate_on) {
		var _login_box = null,
		html = ''+
		'<div class="login_dialog cle" style="width: 400px; padding-bottom:20px; box-sizing:initial;">'+
			'<div class="logo">'+
				'<img src="/images/logo.png?version=20181228" alt="友和 YOHO">'+
			'</div>'+
			'<ul id="login-form" class="login-form">'+
				'<li>'+
					'<label>'+
						'<input type="text" name="username" class="text" placeholder="' + YOHO._('global_login_popup_username', '手提電話號碼') + '">'+
					'</label>'+
				'</li>'+
				'<li>'+
					'<label>'+
						'<input type="password" name="password" class="text" placeholder="' + YOHO._('global_login_popup_password', '密碼') + '">'+
					'</label>'+
				'</li>'+
				'<li class="last">'+
					'<a href="javascript:;" hidefocus="true" class="btn" id="login_btn">' + 
						'<svg class="st0">' + 
							'<use xlink:href="/images/login_icon.svg#Layer_1"></use>' + 
						'</svg>' + 
						YOHO._('global_login_popup_login_btn', '登 入') + 
					'</a>'+
				'</li>'+
				'<li class="last_one login_form_register">'+
					YOHO._('global_login_popup_no_account_register_btn', '沒有賬戶？按此註冊') + 
				'</li>'+
					'<a href="/user.php?act=get_password" class="forget_password">' + YOHO._('global_login_popup_forget_password_btn', '忘記密碼?') + '</a>'+
			'</ul>'+
		'</div>';
		if(typeof affiliate_on === "undefined"){
			if(YOHO.affiliateon==1){
				affiliate_on = 1;
			}
		}

		_login_box = $.dialog({
			title: null,
			lock: true,
			fixed: true,
			padding: '0',
			id: 'login',
			content: html,
			init: function() {
				var $form = $('#login-form'),
				$name = $form.find('input[name="username"]'),
				$pwd = $form.find('input[name="password"]');

				$pwd.keyup(function (event) {
					if(event.keyCode == 13)
					{
						$form.find('a.btn').trigger("click");
					}
				});
				$form.on('click','.login_form_register', function() {
					$('.aui_close').trigger('click');
					YOHO.dialog.showRegister(affiliate_on);
				});
				$('.dialog_login').nextAll().last().click(function(){
					$('.aui_close').trigger('click');
				});
				$form.on('click','span.forget_password', function() {
					location.href = '/user.php?act=get_password';
				});
				function name_keyup() {
					$name.one("keyup",function () {
						$(this).removeClass("empty");
						$(this).css({'border':'1px #eee solid'});
					});
				}
				function pwd_keyup() {
					$pwd.one("keyup",function () {
						$(this).removeClass("empty");
						$(this).css({'border':'1px #eee solid'});
					});
				}	
				$form.on('click','a#login_btn', function() {
					var _this = $(this);
					var _name = $.trim($name.val());
					var _pwd = $.trim($pwd.val());

					if(_this.find('img').length > 0)
					{
						return false;
					}

					if(!_name)
					{
						YOHO.dialog.warn(sprintf(
							YOHO._('global_login_popup_please_fill_in', '請輸入%s'),
							(YOHO._('global_login_popup_username', '手提電話號碼'))
						));
						$name.css({'border':'1px #fd495c solid'});
						$name.addClass("empty");
						name_keyup();
						return false;
					}
					if(!_pwd)
					{
						YOHO.dialog.warn(sprintf(
							YOHO._('global_login_popup_please_fill_in', '請輸入%s'),
							( YOHO._('global_login_popup_password', '密碼'))
						));
						$pwd.css({'border':'1px #fd495c solid'});
						$pwd.addClass("empty");
						pwd_keyup();
						return false;
					}

					// _this.html('<img src="/images/loading.gif" /> ' + YOHO._('global_login_popup_logging_in', '登入中'));
					_this.html(YOHO._('account_submitting', '提交中'));	
					var hostname = '';
					try
					{
						hostname = document.baseURI.split('//')[1].split('/')[0];
					} catch (e) { }
					if (!hostname)
					{
						hostname = window.location.hostname;
					}

					var ajaxParams = {
						url:'https://' + hostname + '/ajax/ajaxuser.php',
						data: {
							'username': _name,
							'password': _pwd
						},
						success: function (data) {
							if (data.status == 1)
							{
								if(data.redirect) {
									window.location = data.redirect;
								} else if (data.first_login)
								{
									// Send user to user.php so they can complete their info to get reward
									window.location = '/user.php';
								}
								else
								{
									if (typeof back_url != 'undefined') {
										window.location = back_url;
									}else{
										window.location.reload();
									}
									
								}
							}
							else if (data.status == 0)
							{
								_this.html(YOHO._('global_login_popup_login_btn', '登 入'));
								YOHO.dialog.warn(YOHO._('global_login_popup_login_failed', '您輸入的手提電話號碼和密碼不相符！'));
							}
						},
						error: function (xhr) {
							_this.html(YOHO._('global_login_popup_login_btn', '登 入'));
							YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
							// console.log('Error: ' + JSON.stringify(xhr));
						}
					};

					if (!!$.fn.intlTelInput)
					{
						// Add country calling code to post data
						ajaxParams.data.ccc = $name.intlTelInput('getSelectedCountryData').dialCode;
					}

					if ($.support.cors)
					{
						$.extend(ajaxParams, {
							type:'post',
							dataType:'json',
							xhrFields: { withCredentials: true }
						});
					}
					else
					{
						$.extend(ajaxParams, {
							type:'get',
							async: false,
							dataType:'jsonp',
							jsonpCallback: 'jsonCallback'
						});
					}

					$.ajax(ajaxParams);
				});

				if (($.fn.placeHolder) && (document.createElement("input").placeholder == undefined))
				{
					$form.find('input[placeholder]').placeHolder();
				}

				if (!$.fn.intlTelInput)
				{
					$('<link rel="stylesheet" href="/yohohk/css/intlTelInput.css">'+
					'<script src="/yohohk/js/intlTelInput.min.js"></script>').appendTo('head');
				}
				var country = (YOHO.userInfo.hasOwnProperty('area') && YOHO.userInfo.area) ? YOHO.userInfo.area.toLowerCase() : 'hk';
				var only_countries = ($.type(YOHO.userInfo.countryList) === 'array') ? YOHO.userInfo.countryList : ['hk','mo','tw','cn','us','ca','gb','fr','au','nz'];
				country = (only_countries.indexOf(country) !== -1) ? country : 'hk';
				$name.intlTelInput({
					defaultCountry: country,
					preferredCountries: ['hk','mo','tw','cn'],
					onlyCountries: only_countries
				});
			}
		});

		
	},

	showRegister: function(affiliate_on) {
		var _register_box = null,
		register_html = ''+
		'<div class="login_dialog cle" style="width: 400px; padding-bottom:30px; box-sizing:initial;">'+
			'<div class="logo">'+
				'<img src="/images/logo.png?version=20181228" alt="友和 YOHO">'+
			'</div>'+
			'<ul id="register-form" class="login-form">'+
				'<li>'+
					'<label>'+
						'<input type="text" name="mobile" class="text" placeholder="' + YOHO._('global_login_popup_username', '手提電話號碼') + '">'+
					'</label>'+
				'</li>'+
				'<li>'+
					'<label>'+
						'<input type="text" name="email" class="text" placeholder="' + YOHO._('account_email', '電郵地址') + '">'+
					'</label>'+
				'</li>'+
				'<li>'+
					'<label>'+
						'<input type="password" name="password" class="text" placeholder="' + YOHO._('global_login_popup_password', '密碼') + '">'+
					'</label>'+
				'</li>'+
				'<li>'+
					'<label>'+
						'<input type="password" name="password2" class="text" placeholder="' + YOHO._('account_password2', '確認密碼') + '">'+
					'</label>'+
				'</li>'+
				'<li class="reference_li">'+
						'<input class="text reference" name="reference" type="text" placeholder="' + YOHO._('account_reference', '推薦人') + '">'+
				'</li>'+
				'<li class="checkbox_li">'+
					'<input type="checkbox" name="agree" id="agree_checkbox">'+
					'<label for="agree_checkbox"></label>'+
					'<span class="read_agree_text">' + 
						YOHO._( 'account_read_agree', '我已詳細閱讀並同意')+ 
					'</span>' +
					'<span class="tos_text">' + 
						YOHO._( 'account_tos_link', '《使用條款》')+ 
					'</span>' +
					'<span class="read_agree_text">' + 
						YOHO._( 'account_and', '和')+ 
					'</span>' +
					'<span class="privacy_text">' + 
						YOHO._( 'account_privacy_link', '《隱私條款》')+ 
					'</span>' +
				'</li>'+
				'<li class="last">'+
					'<a href="javascript:;" hidefocus="true" class="btn" id="register_btn">' + 
						'<svg class="st0">' + 
							'<use xlink:href="/images/login_icon.svg#Layer_1"></use>' + 
						'</svg>' + 
						YOHO._('global_register', '註冊') + 
					'</a>'+
				'</li>'+
				'<li class="last_one register_form_login">'+
					YOHO._('account_already_member_login_here', '已經是會員？按此登入') +
				'</li>'+
			'</ul>'+
		'</div>'+
		'<div class="box" id="xieyi-box">'+
	'<div class="trig"><a href="javascript:;" hidefocus="true" class="iconfont closed">&#xdf;</a></div>'+
	'<h2 class="title"></h2>'+
	'<div class="line"></div>'+
	'<div class="xieyi-bd">'+
	   '</div>'
		'</div>';
		_register_box = $.dialog({
			title: null,
			lock: true,
			fixed: true,
			padding: '0',
			id: 'register',
			content: register_html,
			init: function() {
				if(affiliate_on == 1) {
					$('.reference_li').css({'display':"block"});
					// try to get yoho_affiliate
					$.ajax({
						url: '/ajax/ajaxuser_affiliate.php',
						type: 'post',
						data: {},
						dataType:'json',
						success: function(data) {
							if (data.reference != '') {
								$('.reference_li .text.reference').val(data.reference);
							}
						},
						error: function(xhr) {
							$btn.html(YOHO._('global_register', '註冊'));
							YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
						}
					});
				}
				var $form = $('#register-form'),
				$mobile = $form.find('input[name="mobile"]');
				$email = $form.find('input[name="email"]');
				$pwd = $form.find('input[name="password"]');
				$pwd2 = $form.find('input[name="password2"]');
				$reference = $form.find('input[name="reference"]');
				$agree_checkbox = $form.find('input[name="agree"]');
				$form.on('click','.register_form_login', function() {
					$('.aui_close').trigger('click');
					YOHO.dialog.showLogin(affiliate_on);
				});
				$('.dialog_register').nextAll().last().click(function(){
					$('.aui_close').trigger('click');
				});
				if (!$.fn.intlTelInput)
				{
					$('<link rel="stylesheet" href="/yohohk/css/intlTelInput.css">'+
					'<script src="/yohohk/js/intlTelInput.min.js"></script>').appendTo('head');
				}
				var country = (YOHO.userInfo.hasOwnProperty('area') && YOHO.userInfo.area) ? YOHO.userInfo.area.toLowerCase() : 'hk';
				var only_countries = ($.type(YOHO.userInfo.countryList) === 'array') ? YOHO.userInfo.countryList : ['hk','mo','tw','cn','us','ca','gb','fr','au','nz'];
				country = (only_countries.indexOf(country) !== -1) ? country : 'hk';
				$mobile.intlTelInput({
					defaultCountry: country,
					preferredCountries: ['hk','mo','tw','cn'],
					onlyCountries: only_countries
				});
				function mobile_keyup() {
					$mobile.one("keyup",function () {
						$(this).removeClass("empty");
						$(this).css({'border':'1px #eee solid'});
					});
				}		
				function email_keyup() {
					$email.one("keyup",function () {
						$(this).removeClass("empty");
						$(this).css({'border':'1px #eee solid'});
					});
				}	
				function pwd_keyup() {
					$pwd.one("keyup",function () {
						$(this).removeClass("empty");
						$(this).css({'border':'1px #eee solid'});
					});
				}
				function pwd2_keyup() {
					$pwd2.one("keyup",function () {
						$(this).removeClass("empty");
						$(this).css({'border':'1px #eee solid'});
					});
				}	
				function pwd_or_pw2_keyup() {
					$('input[type="password"]').one("keyup",function () {
						$pwd.removeClass("empty");
						$pwd.css({'border':'1px #eee solid'});
						$pwd2.removeClass("empty");
						$pwd2.css({'border':'1px #eee solid'});
					});
				}
				function checkbox_click(){
					$('#agree_checkbox + label').one("click",function(){
						$(this).css({'border':'1px #eee solid'});
					});
				}		
				function tosbox_close(){
					$('.box .closed').click(function(){
						$('.box').css({'display':'none'});
					});
				}					
				$form.on('click','a#register_btn', function() {
					var $btn = $(this);
					var _ccc = $mobile.intlTelInput('getSelectedCountryData').dialCode;
					var _mobile = $.trim($mobile.val());
					var _email = $.trim($email.val());
					var _pwd = $.trim($pwd.val());
					var _pwd2 = $.trim($pwd2.val());	
					var _agree_ischecked = $agree_checkbox.is(':checked');	
					var _ref = $.trim($reference.val());			
					if($btn.find('img').length > 0)
					{
						return false;
					}

					if(!_mobile)
					{
						YOHO.dialog.warn(sprintf(
							YOHO._('global_login_popup_please_fill_in', '請輸入%s'),
							( YOHO._('global_login_popup_username', '手提電話號碼'))
						));
						$mobile.css({'border':'1px #fd495c solid'});
						$mobile.addClass("empty");
						mobile_keyup();
						return false;
					}
					if(!_email)
					{
						YOHO.dialog.warn(sprintf(
							YOHO._('global_login_popup_please_fill_in', '請輸入%s'),
							( YOHO._('account_email', '電郵地址'))
						));
						$email.css({'border':'1px #fd495c solid'});
						$email.addClass("empty");
						email_keyup();
						return false;
					}
					if(!_pwd)
					{
						YOHO.dialog.warn(sprintf(
							YOHO._('global_login_popup_please_fill_in', '請輸入%s'),
							( YOHO._('global_login_popup_password', '密碼'))
						));
						$pwd.css({'border':'1px #fd495c solid'});
						$pwd.addClass("empty");
						pwd_keyup();
						return false;
					}
					if(!_pwd2)
					{
						YOHO.dialog.warn(sprintf(
							YOHO._('global_login_popup_please_fill_in', '請輸入%s'),
							( YOHO._('account_password2', '確認密碼'))
						));
						$pwd2.css({'border':'1px #fd495c solid'});
						$pwd2.addClass("empty");
						pwd2_keyup();
						return false;
					}
					var mobile_ok = YOHO.check.isMobile(_mobile, _ccc);
					var email_ok = YOHO.check.isEmail(_email);
					if(!mobile_ok)
					{	
						YOHO.dialog.warn(sprintf(
							YOHO._('account_format_invalid', '%s格式有誤，再修改一下哦！'),
							YOHO._('account_username', '手提電話號碼')
						));
						$mobile.css({'border':'1px #fd495c solid'});
						$mobile.addClass("empty");
						mobile_keyup();
						return false;
					}
					if(!email_ok)
					{	
						YOHO.dialog.warn(sprintf(
							YOHO._('account_format_invalid', '%s格式有誤，再修改一下哦！'),
							YOHO._('account_email', '電郵地址')
						));
						$email.css({'border':'1px #fd495c solid'});
						$email.addClass("empty");
						email_keyup();
						return false;
					}
					
					if(_pwd.length < 6)
					{
						YOHO.dialog.warn(sprintf(
							YOHO._('account_password_too_short', '密碼長度最少6個字哦！')
						));
						$pwd.css({'border':'1px #fd495c solid'});
						$pwd.addClass("empty");
						pwd_keyup();
						return false;
					}
					
					if(_pwd != _pwd2)
					{
						YOHO.dialog.warn(sprintf(
							YOHO._('account_password_mismatch', '您兩次輸入的密碼不一致！')
						));
						$pwd.css({'border':'1px #fd495c solid'});
						$pwd.addClass("empty");
						$pwd2.css({'border':'1px #fd495c solid'});
						$pwd2.addClass("empty");
						pwd_or_pw2_keyup();
						return false;
					}

					if(!_agree_ischecked)
					{
						$('#agree_checkbox + label').css({'border':'1px #fd495c solid'});
						YOHO.dialog.warn(sprintf(
							YOHO._('pls_agree_tos_and_privacy', '請同意《免責聲明》和《私隱條款》')
						));
						checkbox_click();
						return false;
					}											
					// $btn.html('<img src="/images/loading.gif" /> ' + YOHO._('account_submitting', '提交中'));				
					$btn.html(YOHO._('account_submitting', '提交中'));				
					$.ajax({
						url: '/ajax/ajaxuser_reg.php',
						type: 'post',
						data: {
							'ccc': _ccc,
							'mobile': _mobile,
							'email': _email,
							'password': _pwd,
							'password_': _pwd2,
							'reference': _ref
						},
						dataType:'json',
						success: function(data) {
							if(data.status == 1)
							{
								if(data.redirect) {
									location.href = data.redirect;
								}else if(data.send_verify_sms == 1) { // Pop up SMS verify BOX
									$('.aui_close').trigger('click');
									showSMSVerify(_mobile,_ccc);
								} else {
									location.href = '/user.php';
								}
							}else if(data.status == 0){
								YOHO.dialog.warn(sprintf(
									data.error
								));
								$btn.html(YOHO._('global_register', '註冊'));
							}
							else
							{
								YOHO.dialog.warn(sprintf(
									data.error
								));
								$btn.html(YOHO._('global_register', '註冊'));
							}
						},
						error: function(xhr) {
							$btn.html(YOHO._('global_register', '註冊'));
							YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
						}
					});
							
				});	
				$form.on('click','.tos_text', function() {
					$.ajax({
						url: '/ajax/get_tos_article_content.php',
						type: 'post',
						data: {
							title: "tos"
						},
						dataType:'json',
						success: function(data) {
							$('.xieyi-bd').html(data);
							$('.title').html(YOHO._('account_tos_link', '《使用條款》'));
							$('.box').css('display','block');
							tosbox_close();
						},
						error: function(xhr) {
							YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
						}
					});
				});
				$form.on('click','.privacy_text', function() {
					$.ajax({
						url: '/ajax/get_tos_article_content.php',
						type: 'post',
						data: {
							title: "privacy"
						},
						dataType:'json',
						success: function(data) {
							$('.xieyi-bd').html(data);
							$('.title').html(YOHO._('account_privacy_link', '《隱私條款》'));
							$('.box').css('display','block');
							tosbox_close();
						},
						error: function(xhr) {
							YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
						}
					});
				});
			}
		});

		function showSMSVerify(_mobile,_ccc) {
			var _verify_box = null,
			_ccc_mobile = "+" + _ccc + "-" + _mobile;
			register_html = ''+
			'<div class="login_dialog cle" style="width: 400px; padding-bottom:0px; box-sizing:initial;">'+
				'<div class="logo">'+
					'<img src="/images/logo.png?version=20181228" alt="友和 YOHO">'+
				'</div>'+
				'<ul id="sms-verify" class="login-form">'+
					'<li>'+
						'<label>'+
							'<input type="text" name="send_phone" class="text" disabled="disabled" value="'+_ccc_mobile+'">'+
						'</label>'+
					'</li>'+
					'<li>'+
						'<label>'+
							'<input type="text" class="verify_code text" name="verify_code" class="text" placeholder="' + YOHO._('user_verify_code', '驗證碼') + '">'+
							'<a class="send_btn" name="send_verify_code" value="" type="button">' + YOHO._('user_send_verify', '發送驗證碼') + '</a>'+
						'</label>'+
					'</li>'+
					'<li>'+
						'<a id="check_verify_code" href="javascript:;" hidefocus="true" class="btn verify_btn">'+ YOHO._('user_verify', '驗證') + '</a>'+
						'<a href="/user.php" hidefocus="true" class="verify_later_btn">'+ YOHO._('user_cancel_sms_verify', '稍後再驗證') + '</a>'+						
					'</li>'+
				'</ul>'
			'</div>';
			_verify_box = $.dialog({
				title: null,
				lock: true,
				fixed: true,
				padding: '0',
				id: 'verify',
				content: register_html,
				init: function() {
					var $box = $('#sms_verify');
					$('.dialog_verify').nextAll().last().click(function(){
						$('.aui_close').trigger('click');
					});		
					$('.send_btn').click(function() {
						if($('.send_btn').hasClass('disabled')){
							return;
						}
						//sms verify
						var countdown = 180;
						var timer;
						$('.send_btn').addClass('disabled');
						setTime($(this),countdown);
						$.ajax({
							url: '/ajax/verify_sms.php',
							type: 'POST',
							data: {
								'ccc': _ccc,
								'mobile': _mobile,
								'send_phone': _ccc_mobile,
								'act': 'send_sms'
							},
							async: false,
							dataType:'json',
							success: function(data) {
								if(data.status == 3) //Not open SMS Verify
								{
									YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。'));
									// res = false;
									setTimeout(function(){ location.href = '/user.php'; }, 3000);
								}
								if(data.status == 1)
								{
									clearTimeout(timer);
									countdown = 0;
								}
								else
								{
									YOHO.dialog.warn(sprintf(
										data.error
									));
								}
							},
							error: function(xhr) {
								YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
							}
						});
			

					
			
						
					});		

					function setTime(obj,countdown){
						if(countdown ==0) {
							obj.removeClass('disabled');
							obj.html(YOHO._('user_send_verify', '發送驗證碼'));
							countdown = 180;
							return;
						}else {
							obj.addClass('disabled');
							obj.html(countdown + "秒後" + YOHO._('send_again', '重新發送'));
							countdown--;
						}
					  timer = setTimeout(function(){
							setTime(obj,countdown)
						},1000)

					}

					$('#check_verify_code').click(function(){
						$btn = $(this);
						if($btn.find('img').length > 0)
						{
							return false;
						}
						// $btn.html('<img src="/images/loading.gif" /> ' + YOHO._('account_submitting', '提交中'));
						$btn.html(YOHO._('account_submitting', '提交中'));	
						$.ajax({
							url: '/ajax/verify_sms.php',
							type: 'POST',
							data: {
								'ccc': _ccc,
								'mobile': _mobile,
								'send_phone': _ccc_mobile,
								'verify_code': $('.verify_code').val(),
								'act': 'verify_sms'
							},
							dataType:'json',
							success: function(data) {
								if(data.status == 3) //Not open SMS Verify
								{
									$btn.html(YOHO._('user_verify', '驗證'));
									YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。'));
									setTimeout(function(){ location.href = '/user.php'; }, 3000);
								}
								if(data.status == 1)
								{
									YOHO.dialog.warn(YOHO._('user_sms_already', '你的手機已經驗證成功。'), function(){location.href = '/user.php';});
									setTimeout(function(){ location.href = '/user.php'; }, 3000);
								}
								else
								{
									YOHO.dialog.warn(sprintf(
										data.error
									));
									$btn.html(YOHO._('user_verify', '驗證'));
								}
							},
							error: function(xhr) {
								$btn.html(YOHO._('user_verify', '驗證'));
								YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
							}
						});
					});
				}
			});
		}
	},

	showRelated: function(){
		var html = '' +
			'<div style="margin:30px">' +
				'<div style="margin-bottom:10px">' +
					'<h1>' + YOHO._('goods_show_related', '為你推介正在售賣的相關產品') + '</h1>' +
				'</div><div id="relatedGoods" style="width:600px"></div>' +
				//'<br><p><span id="countdown">10</span>' + YOHO._('goods_discontinued_redirect', ' 秒後將自動轉到第一件產品的頁面') + '</p>' +
			'</div>'
		var _relate_goods = $.dialog({
			title: YOHO._('goods_discontinued', '此產品已停售'),
			lock: true,
			fixed: true,
			padding: '0',
			content: html,
			init: function(){
				$('#showRelated').appendTo('#relatedGoods');
				//var redirect = $('#showRelated li a')[0].href;
				//setTimeout(function(){location.href = redirect}, 10000);
				//setInterval(function(){$('#countdown').html(parseInt($('#countdown').html()) > 0 ? parseInt($('#countdown').html()) - 1 : 0)}, 1000);
			}
		});
	},

	show404Error: function() {
		if (YOHO.common.cookie('error_request'))
		{
			YOHO.dialog.warn(sprintf(YOHO._('global_404_error', '您所輸入的網址「%s」並不正確！'), YOHO.common.cookie('error_request')));
			YOHO.common.cookie('error_request', null);
		}
	},

	reviewOrderPrompt: function() {
		$('.review-order-prompt button').on('click', function () {
			$(this).closest('.review-order-prompt').hide();

			// Set a cookie that will expire in 5 minutes
			var expire_date = new Date();
			expire_date.setMinutes(expire_date.getMinutes() + 5);
			YOHO.common.cookie('hide_review_order_prompt', '1', { expires: expire_date, path: '/', domain: '.yohohongkong.com' });
		});
	},

	wishList: function() {
		var wishListAjax = function (act, goods_id, callback) {
			$.ajax({
				url: '/ajax/wish_list.php',
				type: 'post',
				dataType: 'json',
				data: {
					'act': act,
					'goods_id': goods_id
				},
				success: function(data) {
					if (data.status == 1)
					{
						if (act == 'add')
						{
							YOHO.dialog.success(YOHO._('wishlist_add_success', '已加入願望清單'));
						}
						else
						{
							YOHO.dialog.success(YOHO._('wishlist_remove_success', '已從願望清單中移除'));
						}
					}
					else if (data.status == 0)
					{
						if (data.hasOwnProperty('error') && data.error.length > 0)
						{
							YOHO.dialog.warn(data.error, function () {
								if (data.hasOwnProperty('showlogin') && data.showlogin == 1)
								{
									YOHO.dialog.showLogin();
								}
							});
						}
						else
						{
							YOHO.dialog.warn((act == 'add') ? YOHO._('wishlist_add_failed', '加入願望清單失敗，請稍後再試。') : YOHO._('wishlist_remove_failed', '從願望清單中移除失敗，請稍後再試。'));
						}
					}
					callback(data);
				},
				error: function(xhr) {
					YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
				}
			});
		};

		// Old productlist small wishlist button
		$('.productlist').find('.addwishlist, .removewishlist').click(function() {
			var $btn = $(this);
			var act = $btn.hasClass('addwishlist') ? 'add' : 'remove';
			var goods_id = $(this).data('goodsId');
			wishListAjax(act, goods_id, function (data) {
				if (data.status == 1)
				{
					if (act == 'add')
					{
						$btn.removeClass('addwishlist');
						$btn.addClass('removewishlist');
						$btn.attr('title', YOHO._('wishlist_remove', '從願望清單中移除'));
					}
					else
					{
						$btn.addClass('addwishlist');
						$btn.removeClass('removewishlist');
						$btn.attr('title', YOHO._('wishlist_add', '加入願望清單'));
					}
				}
			});
		});

		// New product-list small wishlist button
		$('.product-list').find('.product-wishlist').click(function() {
			var $btn = $(this);
			var act = $btn.hasClass('on') ? 'remove' : 'add';
			var goods_id = $(this).data('goodsId');
			wishListAjax(act, goods_id, function (data) {
				if (data.status == 1)
				{
					if (act == 'add')
					{
						$btn.addClass('on');
						$btn.attr('title', YOHO._('wishlist_remove', '從願望清單中移除'));
					}
					else
					{
						$btn.removeClass('on');
						$btn.attr('title', YOHO._('wishlist_add', '加入願望清單'));
					}
				}
			});
		});

		// Product page big wishlist button
		$('.wishlist-btn').click(function() {
			var $btn = $(this);
			var act = $btn.hasClass('on') ? 'remove' : 'add';
			var goods_id = $(this).data('goodsId');
			wishListAjax(act, goods_id, function (data) {
				if (data.status == 1)
				{
					if (act == 'add')
					{
						$btn.addClass('on');
						$btn.attr('title', YOHO._('wishlist_remove', '從願望清單中移除'));
						$btn.find('i.fa').removeClass('fa-plus').addClass('fa-heart');
						$btn.find('span:last').text(YOHO._('wishlist_on_wishlist', '在願望清單上'));
					}
					else
					{
						$btn.removeClass('on');
						$btn.attr('title', YOHO._('wishlist_add', '加入願望清單'));
						$btn.find('i.fa').addClass('fa-plus').removeClass('fa-heart');
						$btn.find('span:last').text(YOHO._('wishlist_add', '加入願望清單'));
					}
				}
			});
		});
	},

	followUser: function() {
		$('.followuser, .unfollowuser').click(function() {
			var $btn = $(this);
			var act = $btn.hasClass('followuser') ? 'follow' : 'unfollow';
			$.ajax({
				url: '/ajax/user_follow.php',
				type: 'post',
				dataType: 'json',
				data: {
					'act': act,
					'follow_id': $(this).data('userId')
				},
				success: function(data) {
					if (data.status == 1)
					{
						if (act == 'follow')
						{
							$btn.removeClass('followuser');
							$btn.addClass('unfollowuser');
							$btn.text('取消跟隨');

							YOHO.dialog.success('已跟隨該會員');
						}
						else
						{
							$btn.addClass('followuser');
							$btn.removeClass('unfollowuser');
							$btn.text('跟隨');

							YOHO.dialog.success('已取消跟隨該會員');
						}
					}
					else if (data.status == 0)
					{
						if (data.hasOwnProperty('error') && data.error.length > 0)
						{
							YOHO.dialog.warn(data.error);
						}
						else
						{
							YOHO.dialog.warn((act == 'unfollow' ? '取消' : '') + '跟隨該會員失敗，請稍後再試。');
						}
					}
				},
				error: function(xhr) {
					YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
				}
			});
		});
	},

	miniProductList: {
		init: function () {
			var _this = this;
			// Shuffle elements initially
			$('ul.mini-product-list.shuffle').shuffleElements('li');
			// Update once initially
			$('.lr-btn-thin').each(function () {
				var $outer = $(this).closest('table');
				var $inner = $outer.find('ul');

				_this.updateBtnState($outer, $inner);
			});
			$('.lr-btn-thin').click(function () {
				var $outer = $(this).closest('table');
				var $inner = $outer.find('ul');
				var $li    = $inner.children('li');
				var $col   = $inner.data('cols');
				var $cur_pos = $('.sectionbox .bottom .cur_pos');
				if ($(this).hasClass('left-btn'))
				{	
					$inner.animate({
						'scrollLeft': '-=' + (208 * $col)
					}, function () {
						_this.updateBtnState($outer, $inner);
					});
				}
				else if ($(this).hasClass('right-btn'))
				{	
					$inner.animate({
						'scrollLeft': '+=' + (208 * $col)
					}, function () {
						_this.updateBtnState($outer, $inner);
					});
				}
			});
			$('.sectionbox .bottom li').click(function () {
				var $outer = $('.sectionbox table');
				var $inner = $outer.find('ul');
				var $li    = $inner.children('li');
				var $col   = $inner.data('cols');
				var $cur_pos = $('.sectionbox .bottom .cur_pos');
				var $from_pos_data = $cur_pos.data('pos');
				var $to_pos_data = $(this).data('pos');
				var $gap = Math.abs($to_pos_data - $from_pos_data)
				if($(this).hasClass('cur_pos')){
					return;
				}else{
					$cur_pos.removeClass('cur_pos');
					$(this).addClass('cur_pos');
					if($from_pos_data < $to_pos_data){
						$inner.animate({
							'scrollLeft': '+=' + ($li.outerWidth() * $col * $gap)
						}, function () {
							_this.updateBtnState($outer, $inner);
						});
					}else{
						$inner.animate({
							'scrollLeft': '-=' + ($li.outerWidth() * $col * $gap)
						}, function () {
							_this.updateBtnState($outer, $inner);
						});
					}
				}

			});
		},

		product_detail_init: function () {
			var _this = this;
			// Shuffle elements initially
			$('ul.mini-product-list.shuffle').shuffleElements('li');
			$('.product_info .sectionbox .goods_slide ').each(function () {	
				//優惠組合		
				if($(this).hasClass('offer')){
					var count = $(this).find('table li').length;
				}else{
				//配件，相關產品，顧客購買的還有
					var total_lists = $(this).find('table li').length;
					var per_lists = $(this).find('ul.mini-product-list').data('cols');
					var count = Math.ceil(total_lists/per_lists);
					var insert_html = '';
				}
				for(var i = 0; i < count; i++){
					if(i==0){
						insert_html = '<ul class="bottom">';
						insert_html += '<li class="cur_pos" data-pos=' + i + '></li>';
					}else{
						insert_html += '<li data-pos=' + i + '></li>';
						if(i==count-1){
							insert_html += '</ul>';
						}
					}			
				}
				$(this).append(insert_html);
				// Update once initially
				$(this).find('.product-detail-lr-btn-thin').each(function () {
					var $outer = $(this).closest('table');
					var $inner = $outer.find('ul');
					_this.updateBtnState($outer, $inner);
				});
				$(this).find('.product-detail-lr-btn-thin').click(function () {
					var $outer = $(this).closest('table');
					var $inner = $outer.find('ul');
					var $li    = $inner.children('li');
					var $col   = $inner.data('cols');
					var $cur_pos = $outer.next().find('.cur_pos');
					if ($(this).hasClass('left-btn'))
					{	
						
						if($cur_pos.data('pos')!=0){
							$cur_pos.prev().addClass('cur_pos');
							$cur_pos.removeClass('cur_pos');
						}
						$inner.animate({
							'scrollLeft': '-=' + ($inner.width())
						}, function () {
							_this.updateBtnState($outer, $inner);
						});
					}
					else if ($(this).hasClass('right-btn'))
					{	
						if($cur_pos.data('pos')!= count-1){
							$cur_pos.next().addClass('cur_pos');
							$cur_pos.removeClass('cur_pos');
						}
						$inner.animate({
							'scrollLeft': '+=' + ($inner.width())
						}, function () {
							_this.updateBtnState($outer, $inner);
						});
					}
				});
				$(this).find('.bottom li').click(function () {
					var $outer = $(this).parent().prev();
					var $inner = $outer.find('ul');
					var $li    = $inner.children('li');
					var $col   = $inner.data('cols');
					var $cur_pos = $(this).parent().find('.cur_pos');
					var $from_pos_data = $cur_pos.data('pos');
					var $to_pos_data = $(this).data('pos');
					var $gap = Math.abs($to_pos_data - $from_pos_data)
					if($(this).hasClass('cur_pos')){
						return;
					}else{
						$cur_pos.removeClass('cur_pos');
						$(this).addClass('cur_pos');
						if($from_pos_data < $to_pos_data){
							$inner.animate({
								'scrollLeft': '+=' + ($inner.width() * $gap)
							}, function () {
								_this.updateBtnState($outer, $inner);
							});
						}else{
							$inner.animate({
								'scrollLeft': '-=' + ($inner.width() * $gap)
							}, function () {
								_this.updateBtnState($outer, $inner);
							});
						}
					}
	
				});

			});
		},

		promote_detail_init: function () {
			var _this = $(this);
			// Shuffle elements initially
			$('.attached_container').each(function () {
				var $this_list = $(this);
				var total_lists = $this_list.find('li').length;
				var per_lists = 3;
				var count = Math.ceil(total_lists/per_lists);
				var insert_html = '';
				
				for(var i = 0; i < count; i++){
					if(i==0){
						insert_html = '<ul class="bottom">';
						insert_html += '<li class="cur_pos" data-pos=' + i + '></li>';
					}else{
						insert_html += '<li data-pos=' + i + '></li>';
						if(i==count-1){
							insert_html += '</ul>';
						}
					}			
				}

				$this_list.find('.dialog-pagination').append(insert_html);

				$this_list.find('.dialog-pagination li').click(function () {
					var $outer = $this_list;
					var $inner = $outer.find('ul');
					var $li    = $inner.children('li');
					var $col   = $inner.data('cols');
					var $cur_pos = $(this).parent().find('.cur_pos');
					var $from_pos_data = $cur_pos.data('pos');
					var $to_pos_data = $(this).data('pos');
					var $gap = Math.abs($to_pos_data - $from_pos_data)
					if($(this).hasClass('cur_pos')){
						return;
					}else{
						$cur_pos.removeClass('cur_pos');
						$(this).addClass('cur_pos');
						if($from_pos_data < $to_pos_data){
							$inner.animate({
								'scrollLeft': '+=' + ($inner.width() * $gap)
							}, function () {
								YOHO.common.miniProductList.updateBtnState($outer, $inner);
							});
						}else{
							$inner.animate({
								'scrollLeft': '-=' + ($inner.width() * $gap)
							}, function () {
								YOHO.common.miniProductList.updateBtnState($outer, $inner);
							});
						}
					}
	
				});
				
			});
			
		},

		updateBtnState: function ($outer, $inner) {
			if ($inner.scrollLeft() <= 0)
			{
				$outer.find('.left-btn').removeClass('enabled');
			}
			else
			{
				$outer.find('.left-btn').addClass('enabled');
			}
			if ($inner.get(0).scrollWidth - $inner.scrollLeft() == $inner.outerWidth())
			{
				$outer.find('.right-btn').removeClass('enabled');
			}
			else
			{
				$outer.find('.right-btn').addClass('enabled');
			}
		}
	},

	sectionBlockTab: function() {
		$('.section-block h4.tabs').each(function () {
			var $this = $(this);

			// Remove text nodes (empty space) between tabs
			$this.contents().filter(function() {
				return this.nodeType == 3; //Node.TEXT_NODE
			}).remove();

			// Calculate total width of all tabs
			var width = 0;
			var fixed_width = 0;
			$this.children().each(function () {
				var outer_width = $(this).outerWidth(true);
				width += outer_width;
				fixed_width += outer_width - $(this).width();
			});

			// If total width is longer than container width, make font-size smaller
			var tabs_width = $this.width() - 2; // 2px extra room
			if (width > tabs_width)
			{
				var new_font_size = (tabs_width - fixed_width) / (width - fixed_width) + 'em';
				$this.children().css('font-size', new_font_size);
			}
		});

		$('.section-block h4.tabs span.tab').click(function () {
			var $this = $(this);
			var idx = $this.closest('h4.tabs').find('span.tab').index($this);

			$this.closest('h4.tabs').find('span.tab').removeClass('on');
			$this.addClass('on');

			var $contents = $this.closest('.section-block').find('div.content');
			var $content = $contents.eq(idx);
			var $others = $contents.not($content);

			$others.hide();
			$content.show();
			if ($content.find('.mini-product-list').length)
			{
				var $outer = $content.find('table');
				var $inner = $outer.find('ul');
				YOHO.common.miniProductList.updateBtnState($outer, $inner);
			}

			// $others.animate({'opacity': 0}, 'fast', function () {
			// 	$others.hide();
			// 	$content.css('opacity',0).show().animate({'opacity': 1}, 'fast', function () {
			// 		if ($content.find('.mini-product-list').length)
			// 		{
			// 			var $outer = $content.find('table');
			// 			var $inner = $outer.find('ul');
			// 			YOHO.common.miniProductList.updateBtnState($outer, $inner);
			// 		}
			// 	});
			// });

			// $others.slideUp('fast');
			// $content.slideDown('fast');
			// if ($content.find('.mini-product-list').length)
			// {
			// 	var $outer = $content.find('table');
			// 	var $inner = $outer.find('ul');
			// 	YOHO.common.miniProductList.updateBtnState($outer, $inner);
			// }
		});
	},

	flashdealWidget: function () {
		var $scrollable = $('div.flashdeal-block .scrollable-outer');
		$scrollable.each(function () {
			var $outer = $(this);
			$outer.css('height', $outer.actual('height'));
			if ($outer.find('.scrollable-item').length > 1)
			{
				$outer.switchable({
					triggers: true,
					triggersWrapCls: 'scrollable-triggers',
					triggerType: 'click',
					panels: '.scrollable-item',
					easing: 'ease-in-out',
					effect: 'scrollLeft',
					end2end: true,
					autoplay: true,
					interval: 5
				});
			}
		});
	},
	//language selector pop up
	showlanguageSelector: function() {
		var _lang_box = null,
		html = ''+
		'<div class="lang_dialog cle">' +
			'<img src="/images/welcome_msg.png">' +
			'<div class="language-btn">' +
				'<span data-lang-code="zh_tw">繁體中文</span>' +
				'<span data-lang-code="zh_cn">简体中文</span>' +
				'<span data-lang-code="en_us">English</span>' +
			'</div>' +
		'</div>';

		_lang_box = $.dialog({
			title: null,
			lock: true,
			fixed: true,
			padding: '0',
			id: 'lang',
			content: html,
			init: function() {
				var win_height = $(window).height(); 
				var win_width = $(window).width(); 
				var left = win_width /2 - 205;
				var top = win_height /2 - 225;
				$(".dialog_lang").css({"left":left, "top":top});
				$('.dialog_lang').nextAll().last().click(function(){
					$('.aui_close').trigger('click');
				});
				$('.lang_dialog span').click(function () {
					var new_lang = $(this).data('langCode');
					// Set a cookie that will expire in 1 year
					var expire_date = new Date();
					expire_date.setFullYear(expire_date.getFullYear() + 1);
					YOHO.common.cookie('yoho_language', new_lang, { expires: expire_date, path: '/', domain: '.yohohongkong.com' });
					var windown_href = window.location.href;
					var windown_href = windown_href.replace(/en_us\/|zh_tw\/|zh_cn\/|\?lang=tc/g, '');
					window.location.href = windown_href;
				});
			}
		});

		
	},
	languageSelector: function () {
		if (navigator.cookieEnabled == true && !YOHO.common.cookie('is_spider') && !YOHO.common.cookie('yoho_language')) {
			YOHO.common.showlanguageSelector()
		}
		$('a.language-toggle').click(function () {
			$(this).closest('div.more-menu').removeClass('hover');
			$(this).closest('.more-bd').hide();

			var new_lang = $(this).data('langCode');
			if (new_lang != YOHO.lang)
			{
				// Set a cookie that will expire in 1 year
				var expire_date = new Date();
				expire_date.setFullYear(expire_date.getFullYear() + 1);
				YOHO.common.cookie('yoho_language', new_lang, { expires: expire_date, path: '/', domain: '.yohohongkong.com' });
				var windown_href = window.location.href;
				var windown_href = windown_href.replace(/en_us\/|zh_tw\/|zh_cn\/|\?lang=tc/g, '');
				window.location.href = windown_href;
				//window.location.reload();
			}
		});
	},
	catLeftMenu: function(){
		$('a.cat-menu-parent').click(function () {
			var $parent = $(this).closest('.left-cat-menu');
			var ul = $parent.find('.left-cat-sub-menu');
			if (ul.css('display') === 'none') {
				ul.css('display', 'block');
			} else {
				ul.css('display', 'none');
			}
		});
	},
	/* For splash Ads */
	splashAd: function(){

		/* Close btn */
		$('i.btnCloseSplashAd').click(function () {
			var ul = $(this).closest('.SplashAd');
			if (ul.css('display') === 'none') {
				ul.css('display', 'block');
			} else {
				ul.css('display', 'none');
			}
		});

		/* calculate ads position */
		$(window).scroll(function(){
			var ul = $('.SplashAd');
			if (ul.css('display') === 'block') {
				ul.css('left', -$(window).scrollLeft());
			}
    	});
	},
	/* Top fix search bar */
	topFixSearch: function() {
		$(window).scroll(function() {
			if($(window).scrollTop() >= ($('.hd_main').height()))
			{
				$('.top_fix_bar').addClass('show_fix_bar');
			} else {
				$('.top_fix_bar').removeClass('show_fix_bar');
				$('#J_mainCata_fix').hide();
				$('#J_subCata_fix').hide();
			}
		});
		var $mainCata = $("#J_mainCata_fix"),
			$subCata = $("#J_subCata_fix"),
			$mainNav = $("#main_nav_fix"),
			_time = null,
			_hover = null,
			isHover = false,
			isIndex = false,
			indexHeight = 0;
		$mainCata.find('ul').menuAim({
			activate: function (row) {
				var $this = $(row),
					_index = $this.index();
				var cur_cat_id = $this.data("cat-id");
				$subView = $subCata.find("#subView"+cur_cat_id);
				$subCata.find(".J_subView").hide();
				// if (!$subView.length) {
					YOHO.common.ajaxGetCat(cur_cat_id,$subCata);
				// } else {
				// 	$subView.show();
				// }		
				if (isHover)
				{
					$this.addClass("current").siblings('li').removeClass("current");
				}
				else
				{
					isHover = true;
					$mainNav.addClass('main_nav_hover');
					$this.addClass("current").siblings('li').removeClass("current");
					$subCata.css({"left":($mainCata.offset().left),"display":"none"});
					$subCata.css({"display":"block",'height':$mainCata.find('ul').height()}).animate({left:($mainCata.offset().left+$mainCata.width())},200);
				}
	
			},
			exitMenu: function() {

			}
		});

		$mainNav.on("mouseenter",function() {
			if (_time !== null)
			{
				clearTimeout(_time);
			}
			cur_cat_id = $(this).data("cat-id");
			var mouseenterFunc = function() {
				$mainNav.addClass('main_nav_hover');
				$mainCata.stop().show().animate({'opacity':1,'height':$mainCata.find('ul').height()},300);
			};
			if (isIndex)
			{
				mouseenterFunc();
			}
			else
			{
				_time = setTimeout(mouseenterFunc, 200);
			}
		}).on("mouseleave",function(){
			if (_time !== null)
			{
				$subCata.find(".J_subView").hide();
				clearTimeout(_time);
			}
			//_time = setTimeout(function(){
				$subCata.css({"left":($mainCata.offset().left),"display":"none"});
				isHover = false;
				if (!isIndex)
				{
					$mainCata.stop().delay(200).animate({'opacity':0,'height':0},300, function(){
						$mainNav.removeClass('main_nav_hover');
						$mainCata.hide().find("li").removeClass('current');
					});
				}
				else
				{
					$mainNav.removeClass('main_nav_hover');
					$mainCata.find("li").removeClass('current');
					$mainCata.stop().delay(200).animate({'height':indexHeight}, 300);
				}
			//}, 200);
		});
	},
	fixScrollMenu: function (){
		if($('.scroll-content').length > 0 ){
			$('.scroll-content').each(function( index, value ) {
				var $parent = $(this).parent();
				var $originTop = $(this).offset().top;
				var $this = $(this);
				$(window).scroll(function() {
					if($this.data("target")){
						$originTop = $("."+$this.data("target")).offset().top;
					}
					if(($(window).scrollTop() >=$parent.offset().top))
					{
						$this.addClass('fixed');
						$this.css('top', ($(window).scrollTop() - $originTop + $('.top_fix_bar').height()));
					} else {
						$this.removeClass('fixed');
					}
					if(($this.offset().top + $this.outerHeight()) >= $('.footer').offset().top) {
						$this.css('top', ($('.footer').offset().top - ($this.outerHeight() + $originTop) - 10));
					}
				});
			});
		}
	},
	flashdealCountdown: function () {
		var getTs = function () { return Math.floor((new Date()).getTime() / 1000); };
		$('.flashdeal-countdown').each(function () {
			var $countdown = $(this);
			var serverTs = parseInt($countdown.data('nowTs')) || getTs();
			var tsDiff = serverTs - getTs() + 1; // add 1 seconds for network delay
			var endTs = parseInt($countdown.data('endTs'));
			var $parent = $countdown.parent();
			var updateCountdown = function () {
				var remaining_time = endTs - (getTs() + tsDiff);
				// var remaining_time = 0;
				if (remaining_time <= 0)
				{
					// Terminating condition met
					$parent.text(YOHO._('flashdeal_started', '閃購已經開始'));
					setTimeout(function(){location.href=$(location).attr('href');},2000);
				}
				// else if (remaining_time < 720)
				// {
				// 	$parent.html('' +
				// 		'<span style="color:#ff0000;">' +
				// 		'<i class="iconfont">&#359;</i> ' +
				// 		YOHO._('goods_promote_ending_soon', '進入最後倒數！') +
				// 		'</span>'
				// 	);
				//
				// 	setTimeout(updateCountdown, 1000);
				// }
				else
				{
					var percent02d = function (input) {
						return (String(input).length < 2 ? '0' : '') + input;
					};
					var date_str = '';
					if (remaining_time > 86400)
					{
						date_str = date_str + '<span class="goods_promote_day">'+ percent02d(Math.floor(remaining_time / 86400)) + YOHO._('goods_promote_day', '天') + '</span>';
						remaining_time = remaining_time % 86400;
					}
					if (remaining_time > 3600)
					{
						date_str = date_str + '<span class="goods_promote_hour">'+  percent02d(Math.floor(remaining_time / 3600))　+ '</span>:';
						remaining_time = remaining_time % 3600;
					}
					if (remaining_time > 60)
					{
						date_str = date_str + '<span class="goods_promote_miniute">'+ percent02d(Math.floor(remaining_time / 60)) + '</span>:';
						remaining_time = remaining_time % 60;
					} else {
						date_str = date_str + '<span class="goods_promote_miniute">00</span>:';
					}
					date_str = date_str + '<span class="goods_promote_second">'+ percent02d(remaining_time) + '</span>';

					$countdown.html(date_str);

					setTimeout(updateCountdown, 1000);
				}
			};
			updateCountdown();
		});
	},

	flashdealCartCountDown: function() {
		if(Object.keys(YOHO.flashdealCartTimer).length > 0 && YOHO.flashdealCartTimer.expired != '') {
			var getTs = function () { return Math.floor((new Date()).getTime() / 1000); };
			$div = $('<div class="flashdeal-cart-countdown"></div>');
			var $countdown = $div;
			var serverTs = parseInt(YOHO.flashdealCartTimer.now_time) || getTs();
			var tsDiff = serverTs - getTs() + 2; // add 2 seconds for network delay
			var endTs = parseInt(YOHO.flashdealCartTimer.expired);
			var $parent = $countdown.parent();
			var updateCountdown = function () {
				if(YOHO.flashdealCartTimer.expired == '') {
					clearTimeout(updateCountdown);
					$('.flashdeal-cart-countdown').remove();
					return;
				}
				var remaining_time = endTs - (getTs() + tsDiff);
				//var remaining_time = 7100;
				if (remaining_time <= 0) {
					var dialog = YOHO.dialog.creat({
						okVal: YOHO._('goods_feedback_submit', '確定'),
						ok: function() {
							location.reload();
						},
						content: YOHO._('flashdeal_cart_timeout', '您的閃購超過時間, 如有需要請重新加入購物車！')
					});
					setTimeout(function(){location.reload()}, 5000);
				}
				else
				{
					var percent02d = function (input) {
						return (String(input).length < 2 ? '0' : '') + input;
					};
					var date_str = '';
					if (remaining_time > 86400)
					{
						date_str = date_str + '<span class="flashdeal_cart_day">'+ percent02d(Math.floor(remaining_time / 86400)) + YOHO._('goods_promote_day', '天') + '</span>';
						remaining_time = remaining_time % 86400;
					}
					if (remaining_time > 3600)
					{
						date_str = date_str + '<span class="flashdeal_cart_hour">'+  percent02d(Math.floor(remaining_time / 3600))　+ '</span>：';
						remaining_time = remaining_time % 3600;
					}
					if (remaining_time > 60)
					{
						date_str = date_str + '<span class="flashdeal_cart_miniute">'+ percent02d(Math.floor(remaining_time / 60)) + '</span>：';
						remaining_time = remaining_time % 60;
					} else {
						date_str = date_str + '<span class="flashdeal_cart_miniute">00</span>：';
					}
					date_str = date_str + '<span class="flashdeal_cart_second">'+ percent02d(remaining_time) + '</span>';

					$countdown.html(sprintf(YOHO._('global_flashdeal_bottombar_timeout', '您購物車中的閃購將於<span class="flashdeal_cart_timer"> %s </span>過期'), date_str));
					$(".footer").append($countdown);
					setTimeout(updateCountdown, 1000);
				}
			};
			updateCountdown();

		} else {
			$('.flashdeal-cart-countdown').remove();
		}
	},
	
	affiliateLink: function() {
		$('#affiliatelink').click(function (evt) {
			evt.preventDefault();
			$url = $(this).data('share');
			$is_login = $(this).data('is_login');
			$banner = $(this).data('banner');
			$title = $(this).data('title');
			$text = $(this).data('text') || $url;
			$content = '';
			if($banner) $content += '<div style="margin-bottom:10px;height:360px;"><img src="'+$banner+'" width="400"></div>';
			if($is_login == 'y') {
				$content += '<div style="text-align:center;">' +
				'<a href="https://api.whatsapp.com/send?text='+encodeURIComponent($text)+'" target="_blank"><img src="/yohohk/img/whatsapp_icon.png" width="40px" height="40px" style="margin-right: 10px;"></a>'+
				'<a href="https://lineit.line.me/share/ui?text='+encodeURIComponent($text)+'&url='+encodeURIComponent($url)+'" target="_blank"><img src="/yohohk/img/line_icon.png" width="40px" height="40px" style="margin-right: 10px;"></a>'+
				'<a href="https://www.facebook.com/sharer.php?quote='+encodeURIComponent($text)+'&u='+encodeURIComponent($url)+'" target="_blank"><img src="/yohohk/img/fb_icon.png" width="40px" height="40px" style="margin-right: 10px; border-radius: 5px;"></a>'+
				'<a href="https://twitter.com/share?text='+encodeURIComponent($text)+'" target="_blank"><img src="/yohohk/img/twitter_icon.png" width="40px" height="40px" style="margin-right: 10px;"></a>'+
				'<a href="https://plus.google.com/share?url='+encodeURIComponent($url)+'" target="_blank"><img src="/yohohk/img/google_plus_icon.png" width="40px" height="40px" style="margin-right: 10px;"></a>' +
				'<br><input class="copy-input" style="cursor: pointer; width:88%; margin-right: 20px; margin-top: 10px; height: 16px; padding: 8px; border-radius: 2px; border: 1px solid #ccc; box-shadow: inset 0 1px 1px rgba(0,0,0,.075);" type="text" value="'+$url+'" readonly></div>';
			} else {
				$content += '<div style="text-align:center;width:400px;"><a href="/user.php?act=login&redirect_to='+ encodeURIComponent($url) +'">'+YOHO._('global_login_popup_please_login', '請登入')+'</a></div>';
			}
			var dialog = YOHO.dialog.creat({
				title: $title,
				okVal: YOHO._('goods_feedback_submit', '確定'),
				cancel: true,
				cancelVal: YOHO._('goods_feedback_submit', '確定'),
				ok: function() {
					return false;
				},
				content: $content
			});
		});
	},
	affiliateLinkTrigger: function() {
		$('.affiliatelink').click(function (evt) {
			evt.preventDefault();
			console.log('affiliateLinkTrigger');
			$('#affiliatelink').trigger('click');
		});
	},
	affiliateCopy: function(){
		$(document).on('click', '.copy-input', function(e){
			$(e.target).select();
			document.execCommand("Copy");
			YOHO.dialog.success(YOHO._('affiliate_copy', '已複製專用鏈結'), 1)
		})
	},

	treasureNotify: function(init) {
		var percent02d = function (input) {
			return (String(input).length < 2 ? '0' : '') + input;
		};
		$.ajax({
			url: '/ajax/treasure.php?treasure_action=get_info',
			dataType: 'json',
			success: function(info) {
				if (info.step != 'fail') {
					$notify2 = $("<div class='notify2 " + info.step + "'></div>");
					$base_icon = $("<img class='treasure_base_icon' src='/yohohk/img/treasure/treasure.gif'>");
					$base_icon.click(function() {
						$(".notify2").removeClass("hidden")
					})
					var head_html = "<div class='notify_head'>" +
										"<div class='notify_title'>提示！</div>" +
										"<div class='notify_step' data-s='found'>1</div>" +
										"<div class='notify_step' data-s='unlock'>2</div>" +
										"<div class='notify_step' data-s='prize'>3</div>" +
										"<div class='notify_step' data-s='open'>4</div>" +
									"</div>"
					$notify_head = $(head_html);
					var body_html = "<div class='notify_body'>" +
										"<div class='notify_image'>" +
											"<img src='/yohohk/img/treasure/" + info.step + ".png'>" +
										"</div>" +
									"</div>";
					$notify_body = $(body_html);
					$notify_message = $("<div class='notify_message'></div>");
					if (info.step == "found") {
						$notify_unlock = $("<div class='notify_btn red notify_unlock'>" + YOHO._("treasure_action_unlock", "開始解鎖") + "</div>");
						$notify_treasure = $("<div class='notify_btn'>" + YOHO._("treasure_action_detail", "活動詳情") + "</div>");
						$notify_treasure.click(function() {
							window.location = "https://www.yohohongkong.com/article.php?id=349"
						})
						$notify_unlock.click(function() {
							$.ajax({
								url: '/ajax/treasure.php?treasure_action=unlock_treasure',
								dataType: 'json',
								success: function(result) {
									if (result.status == 0) {
										location.reload();
									} else {
										YOHO.common.treasureNotify();
									}
								}
							})
						})
						$notify_message.append("<h1>" + YOHO._("treasure_step_found", "成功捕捉神秘獎賞！") + "</h1>");
						$notify_message.append($notify_unlock);
						$notify_message.append($notify_treasure);
					} else if (info.step == "unlock") {
						var html = "";
						if (parseInt(info.remain_time) > 0) {
							var remaining_time = "";
							if (info.remain_time >= 60) {
								remaining_time = percent02d(Math.floor(info.remain_time / 60));
							} else {
								remaining_time = '00';
							}
							remaining_time += ":" + percent02d(info.remain_time % 60);
							html = "<h1 id='treasureCountDown' data-t='" + info.remain_time + "'>" + remaining_time + "</h1>";
							setTimeout(YOHO.common.treasureCountDown, 1000);
						}
						$notify_message.append(html);
						$notify_message.append("<h2 style='text-align: left'>" + YOHO._("treasure_step_unlock", "更多神秘獎賞，於完成解鎖後等你捕捉！") + "</h2>");
					} else if (info.step == "no_prize") {
						$notify_sorry = $("<div class='notify_btn orange notify_sorry'>" + YOHO._("treasure_action_fail_" + (Math.floor(Math.random() * 3) + 1) , "再次展開追捕行動！") + "</div>");
						$notify_sorry.click(function() {
							$.ajax({
								url: '/ajax/treasure.php?treasure_action=confirm',
								dataType: 'json',
								success: function(result) {
									if (result.status == 1) {
										$(".notify2").remove();
									} else {
										location.reload();
									}
								}
							})
						})
						$notify_message.append("<h1>" + YOHO._("treasure_step_prize_none", "未能解鎖成功！") + "</h1>");
						$notify_message.append($notify_sorry);
					} else if (info.step == "prize") {
						$notify_open = $("<div class='notify_btn orange notify_open'>" + YOHO._("treasure_action_open", "確認領獎") + "</div>");
						$notify_open.click(function(){
							$.ajax({
								url: '/ajax/treasure.php?treasure_action=open_treasure',
								dataType: 'json',
								success: function(result) {
									if (result.status == 0) {
										location.reload();
									} else {
										var html = "";
										if (result.step != 'fail') {
											if (result.reward != 0) {
												html += "<div class='treasure_type'><div class='treasure_type_" + (result.reward == 1 ? ("coupon'>" + YOHO._("treasure_coupon", "優惠券")) : ("prize'>" + YOHO._("treasure_prize", "免費"))) + "</div><div class='treasure_type_name'>" + result.treasure_name + "</div></div>" +
														"<div style='margin-top: 10px;'>" +
															"<div class='treasure_tnc' data-msg='" +
																sprintf(YOHO._("treasure_step_detail_prize", "<li>請立即將優惠碼紀錄，優惠碼於任何情況不獲補發。</li>")) +
																sprintf(YOHO._("treasure_step_detail_validate", "<li>優惠券有效期： %s</li>"), result.coupon_date) +
																(result.treasure_tnc == "" ? "" : result.treasure_tnc) +
																"'><u>" + YOHO._("treasure_tnc", "條款") +
															"</u></div>" +
															"<span class='treasure_tnc_remark'>請立即紀錄！</span>" +
															"<input title='點擊複製' class='treasure_coupon' value='" + result.coupon_code + "' readonly>" +
														"</div>";
											}
											$(".notify_step").removeClass("active");
											$(".notify_step[data-s=open]").addClass("active");
											$notify_message.html(html);
										} else {
											if (result.status == 1) {
												alert("你已經領獎了！");
											}
											$(".notify2").remove();
										}
									}
								}
							})
						})
						$notify_message.append("<h2>" + YOHO._("treasure_step_prize", "成功捕捉") + "</h2><h1>" + info.treasure.treasure_name + "</h1>");
						if (info.treasure.treasure_desc != "") {
							$notify_message.append("<p>" + info.treasure.treasure_desc + "</p>");
						}
						$notify_message.append($notify_open);
					}
					$notify_body.append($notify_message);
					$notify_close = $("<span class='fa fa-chevron-down notify_close'></span>");
					$notify_close.click(function(){
						$notify2.addClass("hidden");
					})
					$notify_head.append($notify_close);
					$notify2.append($notify_head);
					$notify2.append($notify_body);
					$(".notify2").remove();
					$('body').append($notify2);
					$('body').append($base_icon);
					$(".notify_step[data-s=" + info.step + "]").addClass("active")
					if (info.step == "no_prize") {
						$(".notify_step[data-s=open]").addClass("active");
					}
					$notify2.fadeIn();
					if (info.step == "found") {
						$notify_close.click();
					}
					if (typeof init !== "undefined" && init) {
						$(document).on("click", ".treasure_coupon", function(e) {
							$(e.target).select();
							YOHO.dialog.success('已複製優惠碼');
							document.execCommand('copy');
							document.getSelection().removeAllRanges();
						})
						$(document).on("click", ".treasure_tnc", function(e) {
							$target = $(e.target).hasClass("treasure_tnc") ? $(e.target) : $(e.target).parents(".treasure_tnc");
							var html = YOHO._("treasure_tnc", "條款");
							html += "<div class='treasure_tnc_detail'>" +
										"<ul>" +
											$target.data("msg") +
										"</ul>" +
									"</div>";
							YOHO.dialog.ok(html);
							$(".ok-tip").find(".iconfont").remove();
						})
					}
					setTimeout(function() {
						$notify2.remove()
					}, info.close_time * 1000)
				} else {
					$(".notify2").remove();
				}
			}
		})
	},

	treasureCountDown: function() {
		var percent02d = function (input) {
			return (String(input).length < 2 ? '0' : '') + input;
		};
		if ($("#treasureCountDown").length > 0 && parseInt($("#treasureCountDown").data('t')) > 1) {
			var remain_time = parseInt($("#treasureCountDown").data('t')) - 1;
			$("#treasureCountDown").data('t', remain_time);
			var remaining_time = "";
			if (remain_time >= 60) {
				remaining_time = percent02d(Math.floor(remain_time / 60));
			} else {
				remaining_time = '00';
			}
			remaining_time += ":" + percent02d(remain_time % 60);
			$("#treasureCountDown").text(remaining_time);
			if (remain_time > 0) {
				setTimeout(YOHO.common.treasureCountDown, 1000);
			}
		} else {
			YOHO.common.treasureNotify();
		}
	},

	treasureOverallNotify: function(notice) {
		if (typeof notice !== "undefined" && notice !== "") {
			$notify = $("<div class='notify'><div class='notify_head'><div class='notify_title'>聖誕勁賞要</div>" + notice + "</div></div>");
			$notify.click(function() {
				window.location = "https://www.yohohongkong.com/article.php?id=349"
			})
			$('body').append($notify);
			setTimeout(function(){
				$notify.fadeIn('slow').animate({
					bottom: '18pt'
				}, {duration: 'fast', queue: false});
			}, 3000)
			setTimeout(function(){
				$notify.fadeOut('fast');
			}, 10000)
		}
	},
	goodsColumns: function () {
		$(".goodsColRating").show();
	},
	getUrlParameter: function getUrlParameter(sParam) {
		var sPageURL = window.location.search.substring(1),
			sURLVariables = sPageURL.split('&'),
			sParameterName,
			i;
	
		for (i = 0; i < sURLVariables.length; i++) {
			sParameterName = sURLVariables[i].split('=');
	
			if (sParameterName[0] === sParam) {
				return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
			}
		}
	},
	searchSubmit: function () {
		
		$(document).on("click", ".search_submit_js", function(e) {
			var $s_form = $("#search_fm");
			$s_form.submit();
		});
	},
};

$(function(){
	if (!YOHO.config.disableCommonInit)
	{
		YOHO.common.searchSubmit();
		YOHO.common.showLoginInfo();
		YOHO.common.hdIntro_Slide();
		YOHO.common.buyGoods();
		YOHO.common.mainNav_animate();
		YOHO.common.back2top();
//		YOHO.common.bookmarkYoho();
		YOHO.common.searchBar();
		YOHO.common.lazyload();
		YOHO.common.deferImage();
		YOHO.common.show404Error();
		YOHO.common.reviewOrderPrompt();
		YOHO.common.wishList();
		YOHO.common.followUser();
		YOHO.common.miniProductList.init();
		YOHO.common.miniProductList.product_detail_init();
		YOHO.common.miniProductList.promote_detail_init();
		YOHO.common.sectionBlockTab();
		YOHO.common.flashdealWidget();
		YOHO.common.languageSelector();
		// YOHO.common.catLeftMenu();
		YOHO.common.splashAd();
		YOHO.common.topFixSearch();
		YOHO.common.fixScrollMenu();
		YOHO.common.flashdealCountdown();
		YOHO.common.affiliateLink();
		YOHO.common.affiliateLinkTrigger();
		YOHO.common.affiliateCopy();
		
		// YOHO.common.goodsColumns();
	}
	YOHO.common.flashdealCartCountDown();
});

})(jQuery);
