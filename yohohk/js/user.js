/***
 * user.js
 *
 * Javascript file for User Center
 ***/
 
 /* jshint -W041 */

var YOHO = YOHO || {};

(function($) {

YOHO.userCenter = {
	
	init: function() {
		// Order list
		this.confirmDelivery();
		
		// Personal profile page
		this.profileEditor();
		this.passwordEditor();
		this.avatarEditor();
		this.sendSMS();
		this.verifySMS();
		this.checkSMSValiated();
	},
	
	sendHashMail: function() {
		$.ajax({
			url: 'user.php',
			data: { 'act':'send_hash_mail' },
			dataType: 'json',
			success: function(data) {
				if (data.hasOwnProperty('error') && data.error != 0)
				{
					YOHO.dialog.warn(data.message);
				}
				else
				{
					YOHO.dialog.ok(data.message);
				}
			},
			error: function(xhr) {
				YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
				console.log('Error: ' + JSON.stringify(xhr));
			}
		});
	},
	
	validateRequiredField: function($e) {
		if ($e.val() == '')
		{
			this.makeTextFieldError($e);
			return false;
		}
		return true;
	},
	
	makeTextFieldError: function($e) {
		$e.addClass('error');
		$e.on('keyup.removeError', function (event) {
			$e.removeClass('error');
			$e.off('keyup.removeError');
		});
	},
	
	profileEditor: function() {
		var frm = $('form#edit_profile');
		frm.on('click', 'span.editdata', function (event) {
			var $class = $(this).data("class");
			$(this).hide();
			if($class) {
				$('.'+$class +' span.data').hide();
				$('.'+$class +' .input_field').show();
				$('.'+$class).find('.info_completed').removeClass('info_completed');
			} else {
				$(this).parent().find('span.data').hide();
				$(this).parent().find('div.input_field').show();
				$(this).parent().parent().find('.profile_form_label').removeClass('info_completed');
				$(this).parent().parent().find('.profile_form_input').removeClass('info_completed');
			}
			$(this).parent().find('select').show();
			
			// Show save button
			frm.find('input[type="submit"]').show();
		});
		
		frm.submit(function (event) {
			frm.find('input.required').each(function (idx) {
				if (!YOHO.userCenter.validateRequiredField($(this)))
				{
					event.preventDefault(); // Stop the from from submitting
				}
			});
		});
	},
	
	passwordEditor: function() {
		var frm = $('form[name="formPassword"]');
		
		frm.on('click', 'span.editdata', function (event) {
			$(this).hide();
			$(this).parent().find('span.data').hide();
			$(this).parent().find('input').show();
			frm.find('label[for="new_password"]').show();
			frm.find('label[for="comfirm_password"]').show();
			frm.find('div.submit_wrap').show();
		});
		
		frm.on('click', 'span.canceledit', function (event) {
			frm.find('span.editdata').show();
			frm.find('span.editdata').parent().find('span.data').show();
			frm.find('span.editdata').parent().find('input').hide();
			frm.find('label[for="new_password"]').hide();
			frm.find('label[for="comfirm_password"]').hide();
			frm.find('div.submit_wrap').hide();
		});
		
		frm.submit(function (event) {
			var frm = $('form[name="formPassword"]');
			var old_password     = frm.find('input[name="old_password"]');
			var new_password     = frm.find('input[name="new_password"]');
			var confirm_password = frm.find('input[name="comfirm_password"]');
			
			var msg = '';
			var reg = null;
			
			if (!YOHO.userCenter.validateRequiredField(old_password))
			{
				msg += YOHO._('user_old_password_empty', '請輸入您的原密碼！') + '<br/>';
			}
			
			if (!YOHO.userCenter.validateRequiredField(new_password))
			{
				msg += YOHO._('user_new_password_empty', '請輸入您的新密碼！') + '<br/>';
			}
			
			if (!YOHO.userCenter.validateRequiredField(confirm_password))
			{
				msg += YOHO._('user_confirm_password_empty', '請輸入您的確認密碼！') + '<br/>';
			}
			
			if (msg.length == 0)
			{
				if (new_password.val() != confirm_password.val())
				{
					msg += YOHO._('user_confirm_password_mismatch', '您現兩次輸入的密碼不一致！') + '<br/>';
					YOHO.userCenter.makeTextFieldError(confirm_password);
				}
			}
			
			if (msg.length > 0)
			{
				YOHO.dialog.warn(msg);
				event.preventDefault();
				return false;
			}
			else
			{
				return true;
			}
		});
	},
	
	avatarEditor: function() {
		var frm = $('form#edit_avatar');
		frm.on('click', 'span.editdata', function (event) {
			$(this).hide();
			frm.find('div[for="avatar_image"]').show();
			frm.find('div#avatar_hint').show();
			frm.find('div.submit_wrap').show();
		});
		
		frm.on('click', 'span.canceledit', function (event) {
			frm.find('span.editdata').show();
			frm.find('div[for="avatar_image"]').hide();
			frm.find('div#avatar_hint').hide();
			frm.find('div.submit_wrap').hide();
		});
	},
	
	confirmDelivery: function () {
		$('.affirm_received_btn').click(function (event) {
			var order_id = $(this).data('orderId');
			YOHO.dialog.confirm(YOHO._('user_confirm_delivery_prompt', '你確認已經收到貨物了嗎？'), function () {
				window.location = 'user.php?act=affirm_received&order_id=' + order_id;
			});
		});
	},
	loadTrackingInfo: function ($order_sn) {
		$.ajax({
			url: 'user.php',
			data: { 'act':'load_tracking_info', 'order_sn': $order_sn },
			dataType: 'json',
			success: function(data) {
				$.each(data, function( key, value ) {
					$("#tracking_status_"+key).html(
						(value.status_img ? "<img width='50px' src='yohohk/img/tracking_icon/icon_"+value.status_img+".png'>" : "")+
						"<div>"+value.status+"</div>"
					);
					var $trackinginfo = value.trackinfo;
					var $length = $trackinginfo !== null ? Object.keys($trackinginfo).length : 0;
					if($length > 0){
						var $html = "";
						$.each($trackinginfo, function(k, v){
							var $active = "";
							if(k == 0)$active = " active";
							$html += '<dd class="post_message '+$active+'">';
							$html += '<b></b>'+
							'<time class="time"><strong>'+v.Date+'</strong></time>'+
							'<div class="message"><span>'+v.StatusDescription+'</span></div>'+
							'<div class="address">'+v.Details+'</span></div>';
							$html += '</dd>';
						});
					} else {
						var $html = '<dd class="post_message no_info">'+YOHO._('not_tracking_info', '未有相關資訊')+'</dd>';
					}
					$("#tracking_session_"+key).html($html);
				  });
			},
			error: function(xhr) {
				// YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
				// console.log('Error: ' + JSON.stringify(xhr));
			}
		});
	},
	checkSMSValiated: function () {
		$.ajax({
			url: "",
			data: {

			},
			success: function(res) {
				// Disable SMS validation if already verified
				if(false) {
					$('#sms_validation').html('<span><i class="fa fa-check" aria-hidden="true"></i> ' + YOHO._('user_sms_already', '你已經成功驗證此電話號碼') + '</span>')
				}
			}
		})
	},
	sendSMS: function () {
		var success_callback = function() {
			$.ajax({
				url: '/ajax/verify_sms.php',
				type: 'POST',
				data: {
					'send_phone': YOHO.userInfo.name,
					'act': 'send_sms'
				},
				async: false,
				dataType:'json',
				success: function(data) {
					if(data.status == 3) //Not open SMS Verify
					{
						YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。'));
						setTimeout(function(){ location.href = '/user.php'; }, 3000);
					}
					if(data.status == 1)
					{
						$('#verify_sms').show();
						$('#send_sms').hide();
						$('#sms_verify').prop('disabled', false);
						$('#sms_verify').val("")
					}
					else
					{
						YOHO.dialog.warn(data.error);
						res = false;
					}
				},
				error: function(xhr) {
					YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
				}
			});
		}
		$('#send_sms').click(function(){
			YOHO.dialog.confirm(sprintf(YOHO._('user_send_verify_mobile', '傳送驗證碼至 %s?'), YOHO.userInfo.name), success_callback);
		})
	},
	verifySMS: function () {
		$('#verify_sms').click(function() {
			$.ajax({
				url: '/ajax/verify_sms.php',
				type: 'POST',
				data: {
					'send_phone': YOHO.userInfo.name,
					'verify_code': $('#sms_verify').val(),
					'act': 'verify_sms'
				},
				dataType:'json',
				success: function(data) {
					if(data.status == 3) //Not open SMS Verify
					{
						YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。'));
					}
					if(data.status == 1)
					{
						YOHO.dialog.warn(YOHO._('user_sms_already', '你的手機已經驗證成功。'), function(){location.href = '/user.php?act=profile';});
						setTimeout(function(){ location.href = '/user.php?act=profile'; }, 3000);
					}
					else
					{
						YOHO.dialog.warn(data.error);
					}
				},
				error: function(xhr) {
					YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
				}
			});
		})
	},
	applyRankProgram: function (require_proof) {
		$(document).on("click", ".apply_program", function (e) {
			$target = $(e.target).is('a') ? $(e.target) : $(e.target).parents('a.apply_program');
			var pid = $target.data("pid");
			if (typeof pid !== "undefined") {
				$.ajax({
					url: '/ajax/user_rank_program.php',
					dataType: 'json',
					data: {
						pid: pid,
						act: "get_program"
					},
					success: function (res) {
						if (res.error == 1) {
							YOHO.dialog.warn(YOHO._(res.msg, res.msg));
						} else {
							var data = res.data;

							$(document).off('change', 'input[type=file][name=rank_program_file]');
							$(document).off('click', '#apply_rank_submit');

							var secret = CryptoJS.lib.WordArray.random(128 / 8).toString(CryptoJS.enc.Hex);
							$(document).on('change', 'input[type=file][name=rank_program_file]', function () {
								$("#apply_rank_submit").prop("disabled", true);
								var input = this;
								var reader = new FileReader();
								if (input.files && input.files[0]) {
									var reader = new FileReader();

									reader.onload = function (e) {
										$(input).next('.preview').html('<img src="' + e.target.result + '">');
										var cryptKey = CryptoJS.enc.Hex.parse(data.secret);
										var cipherText = CryptoJS.AES.encrypt(e.target.result, cryptKey, {
											iv: CryptoJS.enc.Hex.parse(secret),
											mode: CryptoJS.mode.CBC,
											padding: CryptoJS.pad.Pkcs7
										})

										$.ajax({
											url: '/ajax/user_rank_program.php',
											method: 'post',
											dataType: 'json',
											data: {
												pid: pid,
												act: "secret_file",
												secret: CryptoJS.enc.Base64.stringify(cipherText.ciphertext)
											},
											success: function (res) {
												if (res.error == 1) {
													$("#apply_rank_submit").css("cursor", "not-allowed");
													$("#apply_rank_submit").addClass("action-button-gray");
													YOHO.dialog.warn(YOHO._(res.msg, res.msg));
												} else {
													//reader.readAsDataURL(input.files[0]);
													$("#apply_rank_submit").prop("disabled", false);
													$("#apply_rank_submit").css("cursor", "pointer");
													$("#apply_rank_submit").removeClass("action-button-gray");
												}
											},
											error: function (xhr) {
												YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '(' + xhr.status + ')');
											}
										});
									}
									reader.readAsDataURL(input.files[0]);
								}
							})
							var id = "apply_program_" + data.program.program_id;
							$(document).on('click', '#apply_rank_submit', function () {
								var input = $('input[type=file][name=rank_program_file]');
								if (!$('input[name=verify_email]').val()) {
									YOHO.dialog.warn(sprintf(YOHO._('account_please_fill_in', '請輸入%s'), YOHO._('account_email', '電郵地址')));
									return;
								}
								var _email = $.trim($('input[name=verify_email]').val());
								var email_ok = YOHO.check.isEmail(_email);
								if (!email_ok) {
									YOHO.dialog.warn(sprintf(YOHO._('account_format_invalid', '%s格式有誤，再修改一下哦！'), YOHO._('account_email', '電郵地址')));
									return;
								}
								if (require_proof && (!input[0].files || !input[0].files[0])) {
									YOHO.dialog.warn(YOHO._("user_rank_program_proof", "上傳證明文件"));
									return;
								} else
								{
									$(this).prop("disabled", true);
									$(this).css("cursor", "not-allowed");
									$(this).addClass("action-button-gray");
									$(this).val(YOHO._('account_submitting', '提交中'));
									$.ajax({
							 			url: '/ajax/user_rank_program.php',
										method: 'post',
										dataType: 'json',
										data: {
											pid: $("input[name=rank_program_id]").val(),
											uid: $("input[name=rank_program_user]").val(),
											secret: secret,
											verify_email: _email,
											act: "apply_program"
										},
										success: function (res) {
											if (res.error == 1) {
												YOHO.dialog.warn(YOHO._(res.msg, res.msg));
											} else {
												dialog.close();
												$(".dialog_" + id).remove();
												YOHO.dialog.success(YOHO._(res.msg, res.msg));
												setTimeout(function () {
													location.reload();
												}, 2000);
											}
										}
									})
								}
							})
							var html = "<div class='rank_program_title'>" + YOHO._("user_rank_program_name", "計劃名稱") + "</div>" +
								"<div style='margin-bottom: 15px;'>" + data.program.program_name + "</div>" +
								"<div class='rank_program_title'>" + YOHO._("user_rank_program_username", "申請帳號") + "</div>" +
								"<div style='margin-bottom: 15px;'>" + YOHO.userInfo.username + "</div>" +
								"<div class='rank_program_title'><label>" + YOHO._("user_rank_program_email", "驗證電郵") + "</label></div>" +
								"<div style='margin-bottom: 15px;position: relative;'><input type='email' name='verify_email' class='verify_email'><span class='icon1'><i class='fa fa-envelope' aria-hidden='true'></i></span></div>" +
								(require_proof ? "<div class='rank_program_title'>" + YOHO._("user_rank_program_proof", "上傳證明文件") + "</div>" +
								"<div style='display: inline-block; width: 100%; margin-bottom: 20px;'>" +
								"<div class='uploads'>" +
								"<input type='file' name='rank_program_file' accept='image/*'>" +
								"<div class='preview'></div>" +
								"<i class='fa fa-plus fa-2x'></i>" +
								"</div>" +
								"</div>" : "") +
								"<input type='hidden' name='rank_program_file_data' value=''>" +
								"<input type='hidden' name='rank_program_id' value='" + data.program.program_id + "'>" +
								"<input type='hidden' name='rank_program_user' value='" + YOHO.userInfo.uid + "'>" +
								"<input type='button' value='" + YOHO._("user_rank_program_submit", "提交") + "' class='action-button action-button' id='apply_rank_submit' style='line-height: 25px;'>";
							var dialog = YOHO.dialog.creat({
								title: YOHO._("user_rank_program_apply", "申請") + " " + data.program.program_name,
								content: html,
								id: id,
								width: 400,
							});
							$(dialog.DOM.inner).find(".aui_content").css("overflow", "auto");
						}
					}
				})
			}
		})
	}
};

$(function() {
	YOHO.userCenter.init();
});

})(jQuery);
