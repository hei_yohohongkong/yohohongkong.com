/***
 * flashdeal.js
 *
 * Javascript file for Flash Deal
 ***/

/* jshint -W041 */
/* jshint ignore:start */
/* Bézier curve from https://github.com/weepy/jquery.path */
;(function($){$.path={};var V={rotate:function(p,degrees){var radians=degrees*Math.PI/180,c=Math.cos(radians),s=Math.sin(radians);return[c*p[0]-s*p[1],s*p[0]+c*p[1]];},scale:function(p,n){return[n*p[0],n*p[1]];},add:function(a,b){return[a[0]+b[0],a[1]+b[1]];},minus:function(a,b){return[a[0]-b[0],a[1]-b[1]];}};$.path.bezier=function(params,rotate){params.start=$.extend({angle:0,length:0.3333},params.start);params.end=$.extend({angle:0,length:0.3333},params.end);this.p1=[params.start.x,params.start.y];this.p4=[params.end.x,params.end.y];var v14=V.minus(this.p4,this.p1),v12=V.scale(v14,params.start.length),v41=V.scale(v14,-1),v43=V.scale(v41,params.end.length);v12=V.rotate(v12,params.start.angle);this.p2=V.add(this.p1,v12);v43=V.rotate(v43,params.end.angle);this.p3=V.add(this.p4,v43);this.f1=function(t){return(t*t*t);};this.f2=function(t){return(3*t*t*(1-t));};this.f3=function(t){return(3*t*(1-t)*(1-t));};this.f4=function(t){return((1-t)*(1-t)*(1-t));};this.css=function(p){var f1=this.f1(p),f2=this.f2(p),f3=this.f3(p),f4=this.f4(p),css={};if(rotate){css.prevX=this.x;css.prevY=this.y;}
css.x=this.x=(this.p1[0]*f1+this.p2[0]*f2+this.p3[0]*f3+this.p4[0]*f4+0.5)|0;css.y=this.y=(this.p1[1]*f1+this.p2[1]*f2+this.p3[1]*f3+this.p4[1]*f4+0.5)|0;css.left=css.x+"px";css.top=css.y+"px";return css;};};$.fx.step.path=function(fx){var css=fx.end.css(1-fx.pos);if(css.prevX!=null){$.cssHooks.transform.set(fx.elem,"rotate("+Math.atan2(css.prevY-css.y,css.prevX-css.x)+")");}
fx.elem.style.top=css.top;fx.elem.style.left=css.left;};})(jQuery);
/* jshint ignore:end */

var YOHO = YOHO || {};

(function($) {

YOHO.flashdeal = {

	init: function() {
		this.notifyMe();
		this.share();
		this.productCell();
		this.learnMore();
		this.closeLearnMore();
		this.reverseDisplay();
		this.reverseDisplayInfo();
		this.jumpToGoods();
		if($('.pagination__next').length > 0) {
			$('.flashdeals-list').infiniteScroll({
				path: '.pagination__next',
				append: '.flashdeals-item',
				status: '.flashdeals-status',
				hideNav: '.pagination',
				history: false
			});
		}
	},

	notifyMe: function() {
		$(document).on('click', '.flashdeal-notify-btn', function (){
			var $this = $(this);

			var flashdeal_id = $this.data('flashdealId');

			var dialog = YOHO.dialog.creat({
				id: 'notifyemail',
				content: '<div class="confirm-tip">' +
							'<i class="iconfont">&#228;</i>' +
							YOHO._('flashdeal_notifyme_instruction', '請輸入電郵地址，開賣立即通知您！') +
						'</div>' +
						'<div style="padding: 8px 4px 0 4px;">' +
							'<span class="iconfont" style="display:inline-block; width:14px; padding-right:8px; color:#aaa;">&#349;</span>' +
							'<input type="text" class="text" name="email" placeholder="' + YOHO._('flashdeal_notifyme_email', '電子郵件地址') + '" value="' + $('.flashdeal-notify-default-email').text() + '" size="40"/>' +
						'</div>'
			});
			dialog.button({
				name: YOHO._('global_OK', '確定'),
				focus: true,
				callback: function () {
					var $dg = $(dialog.DOM.wrap);

					var email = $dg.find('input[name="email"]').val();

					var errMsg = '';
					if (!email)
					{
						errMsg = YOHO._('flashdeal_notifyme_error_missing_email', '請填寫電郵地址');
					}
					else if (!YOHO.check.isEmail(email))
					{
						errMsg = YOHO._('flashdeal_notifyme_error_invalid_email', '電郵地址格式錯誤');
					}
					if (errMsg.length)
					{
						YOHO.dialog.warn(errMsg, function () {
							// Reopen the enter email dialog
							$this.trigger('click');
							return;
						});
					}

					$.ajax({
						url: '/ajax/flashdeal.php',
						type: 'post',
						data: {
							'act': 'notifyme',
							'flashdeal_id': flashdeal_id,
							'email': email
						},
						dataType: 'json',
						success: function (result) {
							var msg = (result.hasOwnProperty('text') && ($.type(result.text) ==='string')) ? result.text : '';
							if (result.status == 1)
							{
								YOHO.dialog.ok(msg === '' ? YOHO._('flashdeal_notifyme_success', '您已成功登記，開賣時會以電郵通知您。') : msg);
							}
							else
							{
								YOHO.dialog.warn(msg === '' ? YOHO._('flashdeal_notifyme_failed', '提交失敗，請稍後再試。') : msg);
							}
						},
						error: function(xhr) {
							YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
						}
					});
				}
			},{
				name: YOHO._('global_Cancel', '取消'),
				callback: function () {
				}
			});
		});
	},

	share: function () {
		$(document).on('click', '.share-btn', function () {
			var $parent = $(this).closest('.flashdeal-product-cell');
			if (!$parent.length)
			{
				$parent = $(this).closest('.flashdeal-product-row');
			}
			var myURL = window.location.href;
			var myImage = $parent.find('.flashdeal-product-image-container img')[0].src || window.location.protocol + '//' + window.location.hostname + '/images/logo_square.png';
			var myTitle = $parent.find('.flashdeal-product-name').text();
			myTitle = myTitle ? YOHO._('flashdeal_title', '友和閃購') + ' - ' + myTitle : YOHO._('flashdeal_title', '友和閃購');
			var myDescription = $.trim($parent.find('.flashdeal-product-features').contents().get(0).nodeValue) || $('meta[name="Description"]').attr('content');

			if ($(this).hasClass('facebook'))
			{
				// Share on Facebook
				FB.ui({
					method: 'feed',
					link: myURL,
					picture: myImage,
					name: myTitle,
					description: myDescription
				}, function(response) {
					var shared = (response && !response.error_code) ? 1 : 0;
				});
			}
			else if ($(this).hasClass('twitter') ||
					$(this).hasClass('pinterest') ||
					$(this).hasClass('google-plus') ||
					$(this).hasClass('weibo'))
			{
				var win_width, win_height, win_settings, share_url;
				if ($(this).hasClass('twitter'))
				{
					win_width = 550;
					win_height = 450;
					win_settings = 'personalbar=0,toolbar=0,scrollbars=1,resizable=1';
					share_url = 'https://twitter.com/share?url=' + encodeURIComponent(myURL);
				}
				else if ($(this).hasClass('pinterest'))
				{
					win_width = 750;
					win_height = 320;
					win_settings = 'status=no,resizable=yes,scrollbars=yes,personalbar=no,directories=no,location=no,toolbar=no,menubar=no';
					share_url = 'https://www.pinterest.com/pin/create/button/?url=' + encodeURIComponent(myURL) + '&media=' + encodeURIComponent(myImage) + '&description=' + encodeURIComponent(myTitle);
				}
				else if ($(this).hasClass('google-plus'))
				{
					win_width = 600;
					win_height = 600;
					win_settings = 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes';
					share_url = 'https://plus.google.com/share?url=' + encodeURIComponent(myURL);
				}
				else if ($(this).hasClass('weibo'))
				{
					win_width = 650;
					win_height = 520;
					win_settings = 'toolbar=no,menubar=no,resizable=no,status=no';
					share_url = 'http://service.weibo.com/share/share.php?url=' + encodeURIComponent(myURL) + '&type=button&language=zh_tw&pic=' + encodeURIComponent(myImage) + '&searchPic=true&style=simple';
				}

				var scr_width = screen.width;
				var scr_height = screen.height;
				var win_left = Math.round((scr_width / 2) - (win_width / 2));
				var win_top = (scr_height > win_height) ? Math.round((scr_height / 2) - (win_height / 2)) : 0;

				window.open(share_url, '', win_settings +
					',width=' + win_width +
					',height=' + win_height +
					',left=' + win_left +
					',top=' + win_top);
			}
		});
	},

	productCell: function () {
		$('.flashdeal-products-row').each(function () {
			var $cell = $(this).find('.flashdeal-product-cell');
			$cell.height($(this).closest('tr').height()).css({'position': 'relative'});
			$cell.find('.flashdeal-product-buy-and-share').css({
				'left': '0',
				'right': '0',
				'bottom': '0'
			});
		});
	},

	learnMore: function () {
		$(document).on('click', '.single_flashdeal_item', function (e) {
			e.preventDefault();
			var $flashdealId = $(this).data('flashdealId');
			var $detail = $('#detail-' + $flashdealId);
			if ($(e.target).hasClass('flashdeal-add-to-cart-btn')) {
				return;
			} else {
				$detail.show();
			}

		});
	},
	closeLearnMore: function () {
		$(document).on('click', 'a.close-btn', function (e) {
			e.preventDefault();
			$(this).parent().hide();
		});
	},
	reverseDisplay: function () {
		$(document).on('click', '.flashdeal-read-more-btn', function (e) {
			var $btn = $(this);
			var flashdeal_id = $btn.data('flashdealId');
			var x = document.getElementById('flashdeal-read-more-' + flashdeal_id);
			if (x.style.visibility === 'hidden') {
				x.style.visibility = 'visible';
			} else {
				x.style.visibility = 'hidden';
			}
		});
	},
	reverseDisplayInfo: function () {

		$(document).on('click', '.flashdeal-read-more-icon', function (e) {
			e.preventDefault();

			var $parent = $(this).closest('.flashdeal-product-cell');
			if (!$parent.length)
			{
				$parent = $(this).closest('.flashdeal-product-row');
			}

			var $btn = $(this);
			var flashdeal_id = $btn.data('flashdealId');
			var html = $parent.find('.flashdeal-read-more-ship').html();
			var width = 810;
			var height = $(window).height() - 600;
			var dialog = YOHO.dialog.creat({
				content: html,
				// Reset CSS on close, so it won't affect other dialogs (such as the login dialog)
				close: function () {
					$(dialog.DOM.content).css({
						'width': '',
						'height': '',
						'overflow-y': ''
					});
					$(dialog.DOM.wrap).css({
						'top': ''
					});
				}
			});
			$(dialog.DOM.content).css({
				'width': width + 'px',
				'height': height + 'px',
				'overflow-y': 'scroll'
			});
			$(dialog.DOM.wrap).css({
				'top': '200px'
			});
		});
	},
	jumpToGoods: function () {
		var $goods = $('.flashdeal-selected-goods').text();

		if($goods > 0 ){
			$(window).scrollTop(($('#single_'+$goods).offset().top - 70));
			$('#single_'+$goods).addClass("active");
			$('#single_'+$goods).addClass("active");
			setTimeout(function(){$('#single_'+$goods).removeClass("active")}, 2000);
		}
	}
};

$(function() {

	YOHO.flashdeal.init();

});

})(jQuery);
