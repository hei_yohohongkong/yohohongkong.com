/***
 * list.js
 *
 * Product List javascript file
 ***/

/* jshint -W041 */

var YOHO = YOHO || {};

(function($){

YOHO.list = {
	init: function() {
		this.collapsibleBrands();
		this.priceSlider();
		this.productListMode();
		this.productListSorting();
		this.leftMenuDisplay();
	},
	
	collapsibleBrands: function() {
		$('.brands-filter-block').show();
		$('.brands-filters').each(function() {
			var $this = $(this);
			var orgHeight = $this.height();
			var displayHeight = 52;
			if (orgHeight > displayHeight)
			{
				$this.css({
					'overflow': 'hidden',
					'height': displayHeight+'px'
				});
				
				var $more = $('<a class="brands-filter-more"><span>' + YOHO._('productlist_brand_filter_more', '更多') + '</span> <i class="fa fa-lg fa-angle-down"></i></a>');
				$more.insertAfter($this);
				$more.click(function () {
					if ($more.find('i.fa-angle-down').length)
					{
						$this.animate({ 'height': orgHeight + 'px' }, 500, function () {
							$this.css({ 'overflow': '' });
							$more.find('span').text(YOHO._('productlist_brand_filter_less', '收起'));
							$more.find('i.fa').addClass('fa-angle-up').removeClass('fa-angle-down');
						});
					}
					else
					{
						$this.animate({ 'height': displayHeight+'px' }, 500, function () {
							$this.css({ 'overflow': 'hidden' });
							$more.find('span').text(YOHO._('productlist_brand_filter_more', '更多'));
							$more.find('i.fa').addClass('fa-angle-down').removeClass('fa-angle-up');
						});
					}
				});
			}
		});
	},
	
	priceSlider: function() {
		$('.price-range-slider').each(function () {
			var $slider = $(this);
			var $form = $slider.closest('form');
			var $input_min = $form.find('input[name="price_min"]');
			var $input_max = $form.find('input[name="price_max"]');
			var rangeFrom = $slider.data('rangeFrom');
			if (rangeFrom === null || rangeFrom === '')
			{
				rangeFrom = $input_min.val();
			}
			if (rangeFrom === null || rangeFrom === '')
			{
				rangeFrom = 0;
			}
			var rangeTo = $slider.data('rangeTo') || $input_max.val() || 20000;
			var rangeScale = $slider.data('rangeScale');
			if (!$.isArray(rangeScale) || rangeScale[rangeScale.length-1] == 0)
			{
				rangeScale = [rangeFrom, rangeTo];
			}
			var rangeStep = $slider.data('rangeStep') || 1;
			var width = $slider.data('width') || 180;
			$slider.jRange({
				from: rangeFrom,
				to: rangeTo,
				step: rangeStep,
				scale: rangeScale,
				format: '$%s',
				width: width,
				showLabels: true,
				theme: 'theme-blue',
				isRange: true,
				onstatechange: function (value) {
					var tmp = value.split(',');
					var price_min = parseFloat(tmp[0]);
					var price_max = parseFloat(tmp[1]);
					// Use 0 for price_max if it is at the max. value
					if (price_max == rangeTo)
					{
						price_max = 0;
					}
					$input_min.val(price_min);
					$input_max.val(price_max);
					$form.submit();
				}
			});
		});
		$('.price-range-reset-btn').click(function () {
			var $btn = $(this);
			var $form = $btn.closest('form');
			var $slider = $form.find('.price-range-slider');
			var rangeFrom = $slider.data('rangeFrom') || 0;
			var rangeTo = $slider.data('rangeTo') || 50000;
			$slider.jRange('setValue', rangeFrom + ',' + rangeTo);
			$form.find('input[name="price_min"]').val(0);
			$form.find('input[name="price_max"]').val(0);
			$form.submit();
		});
	},
	
	productListMode: function() {
		$('.product-list-mode-toggle').click(function () {
			var mode = $(this).data('mode');
			if(mode == "list"){
				$('.product-list').removeClass("grid").addClass(mode);
			}else{
				$('.product-list').removeClass("list").addClass(mode);
			}
			$(this).addClass('on');
			$('.product-list-mode-toggle').not($(this)).removeClass('on');
			
			// Set a cookie that will expire in a week
			var expire_date = new Date();
			expire_date.setDate(expire_date.getDate() + 7);
			YOHO.common.cookie('ECS[display]', mode, { expires: expire_date });
		});
	},
	
	productListSorting: function() {
		if($('input[name="top"]').length && $('input[name="top"]').val() == 1){
			$form =$('input[name="top"]').closest('form');
			$(window).scrollTop(($('.product-list-header').offset().top - 70));
		}
		if ($('.product-list-sorting').length)
		{
			$('.product-list-sorting button').click(function () {
				$opt = $(this).data('sort');
				$opt2 = $('#order').find('option:selected');
				$form = $(this).closest('form');

				$form.find('input[name="top"]').val(1);
				$form.find('input[name="sort"]').val($opt);
				$form.find('input[name="order"]').val($opt2.data('order'));
				$form.submit();
			});
			$('.product-list-sorting select').simpleselect().change(function () {
				$opt = $('.product-list-sorting button.selected').data('sort');
				$opt2 = $('#order').find('option:selected');
				$form = $(this).closest('form');

				$form.find('input[name="sort"]').val($opt);
				$form.find('input[name="top"]').val(1);
				$form.find('input[name="order"]').val($opt2.data('order'));
				$form.submit();
			});
		}
	},
	leftMenuDisplay: function() {
		$('.display-cat').click(function(){
			$this = $(this);
			$target = $(this).data('target');
			$subCatMenu = $($target);
			if($subCatMenu.css('display') == 'none' ){
				$(this).css('pointer-events','none');
				$targetHeight = $subCatMenu.height();
				$subCatMenu.height(0);
				$subCatMenu.show();
				$subCatMenu.animate({ 'height': $targetHeight+'px' }, 500, function(){
					$this.css('pointer-events','auto');
				});
				$(this).children('.display-text').text(YOHO._('productlist_brand_filter_less', '收起'));
				$(this).children('.caret').addClass('up');

			} else {
				$(this).css('pointer-events','none');
				$targetHeight = $subCatMenu.height();
				$subCatMenu.animate({ 'height': '0px' }, 500, function(){
					$subCatMenu.hide();
					$this.css('pointer-events','auto');
					$subCatMenu.css('height', $targetHeight+'px');
				});
				$(this).children('.display-text').text(YOHO._('productlist_brand_filter_more', '更多'));
				$(this).children('.caret').removeClass('up');
			}
		});
	}
};

$(function(){
	YOHO.list.init();
});

})(jQuery);
