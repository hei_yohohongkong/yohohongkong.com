var err_code = {
    'SUB_Some fields are missing.': '11',
    'SUB_Invalid email address.': '12',
    'SUB_Invalid list ID.': '13',
    'SUB_Already subscribed.': '14',
    'UN_Some fields are missing.': '21',
    'UN_Invalid email address.': '22',
    'No data passed': '31',
    'API key not passed': '32',
    'Invalid API key': '33',
    'Email not passed': '34',
    'List ID not passed': '35',
    'Email does not exist in list': '36',
    'UN_Email does not exist in list': '362'
}
var err_msg = {
    'Email does not exist in list': YOHO._('subscription_message_no_mail', '沒有此電郵紀錄，請重新檢查。'),
    'Invalid email address.':YOHO._('subscription_message_invalid_mail','電郵地址格式不正確，請重新輸入。')
}
var returnVal = "", subscribeVal = "", newSub = false;
function sub_submit(){
    if($('#email').val() != ""){
        newSub = true;
        $('form.subscribe input').attr('disabled','');
        $('#submit').val(YOHO._('subscription_button_load', '處理中...'));
        getSubscriberStatus(subscribe)
        var t = setInterval(function(){
            if(subscribeVal != "" && subscribeVal){
                console.log(subscribeVal);
                switch(subscribeVal){
                    case 'success': location.href += '&success';break;
                    case 'already': alert(YOHO._('subscription_message_subscribe_already', '此電郵地址已經訂閱了友和電子報，請檢查您輸入的電郵地址是否正確')); $('form.subscribe input').removeAttr('disabled',''); $('#submit').val(YOHO._('subscription_button_submit', '提交'));break;
                    case 'verify': alert(YOHO._('subscription_message_subscribe_verify', '請前往電郵點選確認鏈結以完成訂閱程序'));location.reload();break;
                    case 'cancelled': alert(YOHO._('subscription_message_subscribe_cancel', '您已放棄訂閱電子報！')); location.reload();break;
                    default:alert(err_msg[subscribeVal.replace('UN_','').replace('SUB_','')] || 'Error' + err_code[subscribeVal] + YOHO._('subscription_message_subscribe_fail', ': 訂閱失敗，請檢查您輸入的資料並重試')); $('form.subscribe input').removeAttr('disabled',''); $('#submit').val(YOHO._('subscription_button_submit', '提交'));break;
                }
                clearInterval(t)
            }
        },500)
    } else {
        alert(YOHO._('subscription_message_subscribe_missing', '請輸入欲登記的電郵地址'));
    }
}
function un_submit(){
    if($('#email').val() != ""){
        $('form.subscribe input').attr('disabled','');
        $('#submit').val(YOHO._('subscription_button_load', '處理中...'));
        getSubscriberStatus(unsubscribe);
        var t = setInterval(function(){
            if(returnVal != "" && returnVal){
                console.log(returnVal);
                switch(returnVal){
                    case 'success': case 'unsubscribe':location.href += '&success';break;
                    default:alert(err_msg[returnVal.replace('UN_','').replace('SUB_','')] || 'Error' + err_code[returnVal] + YOHO._('subscription_message_unsubscribe_fail',': 取消訂閱失敗，請檢查您輸入的資料並重試'));location.reload();break;
                }
                clearInterval(t)
            }
        },500)
    } else {
        alert(YOHO._('subscription_message_unsubscribe_missing', '請輸入已登記的電郵地址'));
    }
}
function up_submit(){
    if($('#email').val() != ""){
        if($('#name').val() != ""){
            $('form.subscribe input').attr('disabled','');
            $('#submit').val(YOHO._('subscription_button_load', '處理中...'));
            getSubscriberStatus($('#new_email').val() != "" ? unsubscribe : subscribe);
            var t = setInterval(function(){
                if(returnVal != "" && returnVal){
                    if($('#new_email').val() != ""){
                console.log(returnVal);
                        switch(returnVal){
                            case 'success':case 'unsubscribe': $('#email').val($('#new_email').val()); subscribe(returnVal);break;
                            default: alert(err_msg[returnVal.replace('UN_','').replace('SUB_','')] || 'Error' + err_code[returnVal] + YOHO._('subscription_message_update_fail', ': 更新資料失敗，請檢查您輸入的資料並重試')); $('form.subscribe input').removeAttr('disabled',''); $('#submit').val(YOHO._('subscription_button_submit', '提交'));break;
                        }
                    }
                    clearInterval(t)
                }
            },500);
            var t2 = setInterval(function(){
                if(subscribeVal != "" && subscribeVal){
                console.log(subscribeVal);
                    switch(subscribeVal){
                        case 'success': case 'SUB_Already subscribed.': location.href += '&success';break;
                        case 'cancelled': alert(YOHO._('subscription_message_update_cancel', '你已放棄更新資料！')); location.reload();break;
                        default: alert(err_msg[subscribeVal.replace('UN_','').replace('SUB_','')] || 'Error' + err_code[subscribeVal] + YOHO._('subscription_message_update_fail', ': 更新資料失敗，請檢查您輸入的資料並重試')); $('form.subscribe input').removeAttr('disabled','');$('#submit').val(YOHO._('subscription_button_submit', '提交'));break;
                    }
                    clearInterval(t2)
                }
            },500);
        } else {
            alert(YOHO._('subscription_message_update_missing', '請輸入您的名字'));
        }
    } else {
        alert(YOHO._('subscription_message_unsubscribe_missing', '請輸入已登記的電郵地址'));
    }
}
function subscribe(respond){
    subscribeVal = "";
    var sub = false;console.log('sub_res:' + respond)
    switch(respond){
        case 'Unsubscribed': case 'unsubscribe': sub = confirm(YOHO._('subscription_message_subscribe_confirm', '您已取消訂閱友和電子報！\n更新資料會令您重新訂閱友和電子報，您是否仍要繼續？'));break;
        case 'Subscribed': case 'Bounced': case 'Soft bounced': case 'Complained': newSub == true ? subscribeVal = "already" : sub = true;break;
        case 'Unconfirmed': newSub == true ? subscribeVal = "verify" : sub = true;break;
        case 'Email does not exist in list': newSub == true ? sub = true : subscribeVal = respond;break;
        case 'success': sub = true; break;
        default: subscribeVal = respond;
    }
    if(sub){
        $.ajax({
            url: base_url + 'subscribe',
            type: 'post',
            data: {
                name: $('#name').val(),
                email: $('#email').val(),
                list: list_id,
                hp: $('#hp').val(),
                boolean: true
            },
            success: function(res){
                switch(res){
                    case '1': subscribeVal = 'success';break;
                    default: subscribeVal = "SUB_" + res;break;
                }
            }
        })
    }else if(subscribeVal == ""){
        subscribeVal = "cancelled";
    }
}
function unsubscribe(respond){
    returnVal = "";
    switch(respond){
        case 'Subscribed': case 'Unconfirmed': case 'Bounced': case 'Soft bounced': case 'Complained': 
            $.ajax({
                url: base_url + 'unsubscribe',
                type: 'post',
                data: {
                    email: $('#email').val(),
                    list: list_id,
                    boolean: true
                },
                success: function(res){
                    switch(res){
                        case '1': returnVal = 'success';break;
                        default: returnVal = res;break;
                    }
                }
            });
            break;
        case 'Unsubscribed': returnVal = 'unsubscribe';break;
        default: returnVal = "UN_" + respond;break;
    }
}
function getSubscriberStatus(callback){
    $.ajax({
        url: base_url + 'api/subscribers/subscription-status.php',
        type: 'post',
        data: {
            api_key: api_key,
            email: $('#email').val(),
            list_id: list_id
        },
        success: function(respond){
            callback(respond)
        }
    })
}