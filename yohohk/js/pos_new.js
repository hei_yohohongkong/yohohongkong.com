/***
 * pos_new.js
 * modified from pos.js by yan 2019-07-25
 *
 * POS System for YOHO Hong Kong
 ***/


var YOHO = YOHO || {};

YOHO.config = YOHO.config || {};

YOHO.config.disableCommonInit = true;
// YOHO.config.currentUserWholesale = false;
YOHO.config.isDebug = false;

(function($) {
YOHO.pos = {
	const: {
		HK_COUNTRY_CODE_ID: 	3409,
		MACAU_COUNTRY_CODE_ID: 	3410,
		CHINA_COUNTRY_CODE_ID: 	3436,

		EXPRESS_SHIPPING_TYPE: 		'express',
		STORE_SHIPPING_TYPE: 		'retail_store',
		PICKUP_POINT_SHIPPING_TYPE: 'pickuppoint',

		REDEEM_PAYMENT: 99,

		KEYCODE_ENTER: 	13,
		KEYCODE_ESC:	27,

		AVATARS: '/uploads/avatars/default_60x60.png',
	},
	data: {
		mode: 'normal',
		countries_code: [],
		warehouseList: [],

		goodsList: [],
		user_rank: 0,
		user_id: 0,
		user_name: '',
		shipping: 2,
		shipping_area_id: '',
		shippingType: 'retail_store',
		payment: 3,
		country: 3409,
		discount: 1,
		integral: 0,
		coupon_codes: [],
		saId: 0,
		saOrder: '',
		saError: null,

		serialNumber: '',
		postscript: '',
		internalRemarks: '',
		orderRemarks: '',
		currentAddress: new Object,

		pickupPointList: [],
		currentRegionList: [],
		allotRouter: '',
		allotRouterCode: '',
		selectedPickupPointArea: 0,
		selectedPickupPointRegion: 0,

		have_preorder_goods: false,
		force_reserve_mode: false,

		sa_rates: [],
		goods_replace_price: new Object, // is using for sa and wholesale

		// repair
		repairGoodsList: [],
		repairFee: 0,
		repairRemark: '',
	},

	render: { // render UI
		goodsTableRow: function(goods, status) {
			var vipLabel = '';
			var requireGoodsLabel = '';
			var giftLabel = '';

			var goodsNumber = 1;
			var goodsNumberInputClass = '';
			var goodsNumberInputReadonly = '';
			var buttonIconClass = '';
			var buttonIcon = '<i class="fa fa-shopping-cart"></i>';
			var priceDisplay = null;
			var actIdDisplay = '';
			var isGiftDisplay = '';

			switch (status) {
				case 'search':
					goodsNumberInputClass = 'addtocart-input';
					buttonIconClass = 'addtocart-button';
					priceDisplay = goods.shop_price;
					break;
				case 'favourable':
					goodsNumberInputClass = 'fa_addtocart_input';
					buttonIconClass = 'fa_addtocart_btn';
					priceDisplay =  (goods.act_kind == '1') ? '<span class="required">免費</span>' : goods.formated_price;
					actIdDisplay = 'data-act-id="' + goods.act_id + '"';
					goodsNumberInputReadonly = 'readonly';
					break;
				case 'require-goods':
					goodsNumberInputClass = 'rg_addtocart_input';
					buttonIconClass = 'rg_addtocart_btn';
					priceDisplay =  goods.formatted_shop_price;
					goodsNumberInputReadonly = 'readonly';
					break;
				case 'cart':
					goodsNumber = goods.goods_number;
					buttonIconClass = goods.is_auto_add ? 'display_none' : 'remove-button';
					buttonIcon = goods.is_auto_add ? '' : '<i class="fa fa-trash"></i>';
					priceDisplay = goods.goods_price;
					isGiftDisplay = 'data-is-gift="' + goods.is_gift + '"';

					if(goods.is_gift > 0) {
						giftLabel = (goods.goods_price_normal=== '0.00') ? this.tagLabel('贈品') : this.tagLabel('換購');
						goodsNumberInputReadonly = 'readonly';
					} else {
						goodsNumberInputClass = 'updatecart-input';
					}

					if(goods.is_vip == 1) vipLabel =  this.tagLabel('VIP售價');
					if(goods.has_require_goods == 1) requireGoodsLabel = "<a href='#goto_requiregoods_" + goods.goods_id + "'>"+ this.tagLabel('變壓器', 'has-require-goods') +"</a>";
					break;
				default:
					break;
			}

			var row = '<tr data-goods-id="' + goods.goods_id + '"'+ actIdDisplay + isGiftDisplay +'>'+
						'<td class="goods-name">'+
							'<img src="'+goods.goods_img+'">'+
							'<div>'+
								'<a href="goods.php?id=' + goods.goods_id + '" target="_blank" class="goods_link">' + goods.goods_name + '</a>' + 
								vipLabel + giftLabel + requireGoodsLabel +
							'</div>'+
						'</td>'+
						'<td><a href="x/goods.php?act=list&amp;keyword='+goods.goods_sn+'" target="_blank">' + goods.goods_sn + '</a></td>'+
						'<td>' + priceDisplay + '</td>'+
						'<td>'+
							'<input type="number" min="1" pattern="[\d]*" value="' + goodsNumber + '" class="' + goodsNumberInputClass + '"' + goodsNumberInputReadonly + '> '+
						'</td>'+
						'<td><div class="icon ' + buttonIconClass + '">' + buttonIcon + '</div></td>'+
					'</tr>';


			var stocksInquiryRow = this.stocksInquiryTableRow(goods.stocks_inquiry, 5);

			return row + stocksInquiryRow;
		},

		packageTableRows: function(package, status) {
			var _this = this;
			var goodsTableRows = '';
			var rowSpan = package.goods_list.length * 2;
			var packageLabel = '';
			var requireGoodsLabel = '';

			switch(status) {
				case 'cart':
					packageLabel =  _this.tagLabel('套裝優惠');
					var packageInfoCells =  '<td rowspan="' + rowSpan + '">' + package.total_price + '</td>' +
											'<td rowspan="' + rowSpan + '">' +
												'<input type="number" pattern="[\d.]*" value="' + package.qty + '" class="package-updatecart-input" readonly>'+
											'</td>' +
											'<td rowspan="' + rowSpan + '">' +
												'<div class="icon package-remove-button"><i class="fa fa-trash"></i></div>' + 
											'</td>';
					break;
				default:
					var packageInfoCells =  '<td rowspan="' + rowSpan + '">' + package.total_price + '</td>' +
											'<td rowspan="' + rowSpan + '">' +
												'<input type="number" min="1" pattern="[\d.]*" value="1" class="package-addtocart-input">'+
											'</td>' +
											'<td rowspan="' + rowSpan + '">' +
												'<div class="icon package-addtocart-button"><i class="fa fa-shopping-cart"></i></div>' + 
											'</td>';
					break;
			}

			$.each(package.goods_list, function(index, goods) {
				packageInfoCells = (index == 0 ) ? packageInfoCells : '';
				if(goods.has_require_goods && goods.has_require_goods == 1) 
					requireGoodsLabel = "<a href='#goto_requiregoods_" + goods.goods_id + "'>"+ _this.tagLabel('變壓器', 'has-require-goods') +"</a>";

				var requireGoodsNum = (goods.goods_number > 1) ? ' x' + goods.goods_number : '';
				var infoIds = 'data-act-id=' + package.act_id + ' data-goods-id=' + goods.goods_id;
				var row = 	'<tr '+ infoIds +'>' +
								'<td class="goods-name">' +
									'<img src="'+goods.goods_img+'">'+
									'<a href="goods.php?id=' + goods.goods_id + '" target="_blank" class="goods_link"><div>'+
										goods.goods_name + requireGoodsNum + packageLabel + requireGoodsLabel +
									'</div></a>'+
								'</td>' +
								'<td><a href="x/goods.php?act=list&amp;keyword='+goods.goods_sn+'" target="_blank">' + goods.goods_sn + '</a></td>'+
								packageInfoCells +
							'</tr>';
				goodsTableRows += row;

				var stocksInquiryRow = _this.stocksInquiryTableRow(goods.stocks_inquiry, 2);
				goodsTableRows += stocksInquiryRow;
			});

			return goodsTableRows;
		},

		goodsTable: function(goods, rows, status) {
			var goodsGroupName = '';
			var tableId = '';
			var infoIds = '';
			switch (status) {
				case 'favourable':
					goodsGroupName = goods.act_name;
					break;
				case 'packages':
					infoIds = 'data-act-id=' + goods.act_id + ' data-goods-id=' + goods.goods_id;
					goodsGroupName = goods.act_name;
					break;
				case 'require-goods':
					goodsGroupName = goods.goods_name + ' - 變壓器';
					tableId = 'goto_requiregoods_' + goods.goods_id;
					break;
				default:
					break;
			}

			return '<div class="goods-table table-responsive"><table class="table" id="' + tableId + '" ' + infoIds + '>' +
						'<caption>' + goodsGroupName + '</caption>' +
						'<thead><tr><th>產品名稱</th><th>條碼</th><th>價格</th><th>數量</th><th></th></tr></thead>' +
						'<tbody>' + rows + '</tbody>'+
					'</table></div>';
		},

		addressListTableRow: function(address) {
			var editButton = (address.id == 0) ? 
								'<div class="icon address-edit-button">'+
									'<svg class="edit-icon" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M14.8345 4.34951L19.4947 9.0097L7.61119 20.8932L2.951 16.233L14.8345 4.34951ZM22.9898 3.26213L20.8927 1.16504C20.116 0.388345 18.7957 0.388345 17.9413 1.16504L15.9219 3.18446L20.5821 7.84466L22.9122 5.51456C23.6112 4.8932 23.6112 3.88349 22.9898 3.26213ZM0.543231 22.7573C0.465562 23.1456 0.776241 23.4563 1.16459 23.3786L6.36847 22.1359L1.70828 17.4757L0.543231 22.7573Z" fill="white"></path></svg>'+
								'</div>' :
								'';

			return '<tr><td>'+
						'<div class="select-radio select-address">'	+
							'<input type="radio" id="addressRadio'+ address.id +'" data-country="' + address.country + '" name="address_radio" class="select-radio-input" value="' + address.id + '">'+
							'<label for="addressRadio'+ address.id +'" class="address-list-radio select-radio-style"></label>' +
						'</div>'+
					'</td>'+
					'<td><div class="shipper-name">' + address.consignee + '</div></td>' +
					'<td><div class="shipper-address">' + address.address + '</div></td>'+
					'<td>'+ editButton +
					'</td></tr>';
		},

		stocksInquiryTableRow: function(stocksInquiry, colSpan) {
			var stocks = stocksInquiry.stocks;
			var warehouses = stocksInquiry.warehouses;
			var warehousesStocksHtml = '';
			var totalStocks = 0;

			$.each(stocks, function(goodsId, warehousesStocks) {
				var warehouseStocksLength = Object.keys(warehousesStocks).length;
				$.each(warehousesStocks, function(warehouseId, warehouseStocks) {
					warehouseStocksLength--;
					totalStocks += parseInt(warehouseStocks);
					var warehouseNameHtml = '<span class="stock-title">'+ warehouses[warehouseId] +': </span>';
					var warehouseStocksHtml = '<span class="stocks-number">' + warehouseStocks + '</span>';
					warehousesStocksHtml += '<div>' + warehouseNameHtml + warehouseStocksHtml + '</div>';
					if(warehouseStocksLength != 0) warehousesStocksHtml += '<span class="line">|</span>';
				});
			});
			return '<tr class="stocks-inquiry">' +
						'<td colspan="' + colSpan + '">' +
							'<div class="total-stocks">' +
								'<span class="stocks-title">總庫存</span>' +
								'<span class="stocks-number">' + totalStocks + '</span>' +
							'</div>' +
							'<div class="warehouse-stocks">' + warehousesStocksHtml + '</div>' +
						'</td>' +
					'</tr>';
		},

		tagLabel: function(text, classList, hasRemoveIcon) {
			classList = classList || '';
			var removeIcon = (hasRemoveIcon) ? '<i class="remove-tag fa fa-remove"></i>' : '';
			return '<div class="tag-label ' + classList + '">' + 
						'<span>' + text + '</span>' + 
						removeIcon +
					'</div>';
		},

		needRemovalItem: function(item) {
			return '<div class="nr_item">'+
						item.cat_name+' <input type="number" pattern="[\d.]*" name="nr_cat['+item.cat_id+']" max="'+item.total+'" min="0" value="'+item.total+'" class="nr_input">部'+
					'</div>';
		},

		selectOption: function(value, content, isSelected) {
			var selectedOption = (isSelected) ? 'selected' : '';
			return '<option value="' + value + '" ' + selectedOption + '>' + content + '</option>';
		},

		customerInfoSelectOption: function(item) {
			var data = $.map(item, function(info, key) {
				return 'data-'+key+'="'+info+'"';
			})

			var content = '手提：' + item.mobile + ' 電郵：' + item.email;
			return '<option '+data.join(' ')+'">' + content + '</option>';
		},

		restrictedHKGoodsWarning: function(restrictedHKGoods) {
			var goodsList = '';
			$.each(restrictedHKGoods, function(i, goods) {
				goodsList += '<div>'+ goods +'</div>';
			});

			return '<div class="restricted-to-hk">' +
						'<div class="restricted-to-hk-warning">' +
							'<i class="fa fa-fw fa-warning"></i>' +
							'<div>您的購物車中包含了以下產品只能送貨到香港：</div>' +
						'</div>' + 
						'<div class="goods-list">'+ goodsList + '</div>' +
					'</div>';
		},

		shippingList: function(shippingList) {
			var shippingMethodButtons = '';
			$.each(shippingList, function(i, shipping) {
				shippingMethodButtons += '<div class="tab-button">' +
											'<input data-area-id="' + shipping.shipping_area_id + '" id="shipping' + shipping.shipping_id + '" class="tab-button-rb" type="radio" name="shippingMethod" value="' + shipping.shipping_id + '"/>' +
											'<label for="shipping' + shipping.shipping_id + '" class="tab-button-label">'+
												'<span class="shipping-name">' + shipping.shipping_name + '</span>' +
												'<em class="postage_fee">' + shipping.format_shipping_fee + '</em>' +
											'</label>' +
										'</div>';
			});
			return shippingMethodButtons;
		},

		promotionTheClubList: function(promote_code) {
			return '<div data-promotion-code="' + promote_code + '">' +
						'<div class="promotion-title cart-info-label">The Club 會員帳號</div>' +
						'<div class="promotion-form input-inside-btn">' +
							'<input name="theClubId" type="number" class="promotion-input" value="" placeholder="輸入The Club 會員帳號" maxlength="10" pattern="^8[0-9]{9}$" autocomplete="off" required>' +
						'</div>' +
						'<div class="promotion-hints cart-info-hints">The Club 會員帳號為8字開頭的10位數字</div>' +
					'</div>';
		},

		previewOrder: function(info) {
			var title = $('.menu li.active').text();
			var sectionHtml = '<div class="preview-title">'+title+'</div>';
			$.each(info, function(infoKey, infoObj){
				var infoHtml = '';
				$.each(infoObj, function(name, content) {
					var title = '';
					switch (name) {
						case 'goodsList': 		title = '出單列表';break;
						case 'serialNumber': 	title = '產品序號'; break;
						case 'postscript': 		title = '客戶備註'; break;
						case 'internalRemarks': title = '內部備註'; break;
						case 'orderRemarks': 	title = '訂貨備註'; break;
						case 'coupon': 			title = '優惠券';break;
						case 'discount': 		title = '折扣'; break;
						case 'payment': 		title = '付款方式'; break;
						case 'member': 			title = '會員'; break;
						case 'shipping': 		title = '出貨方式'; break;
						case 'integral': 		title = '使用積分'; break;
						case 'email': 			title = 'Email'; break;
						case 'mobile': 			title = '手提電話'; break;
						case 'salesperson': 	title = '銷售人員'; break;
						case 'repairGoods': 	title = '要維修的產品';break;
						case 'orderNumber': 	title = '舊單編號';break;
						case 'repairFee': 		title = '維修費';break;
						case 'contactNumber': 	title = '聯絡電話';break;
						case 'contactName': 	title = '顧客姓名';break;
						case 'repairRemark': 	title = '維修備註'; break;
					}

					var infotableRowClass = '';
					if(infoKey == 'goodsInfo') {
						var contentText = '';
						if($.isArray(content)) {
							$.each(content, function(i, text) {
								contentText += '<div>' + text + '</div>';
							});
						} else {
							contentText = '<div>' + content + '</div>';
						}
						content = contentText;
					} else {
						infotableRowClass = 'info-table-row';
					}

					infoHtml += '<div class="preview-section-info '+infotableRowClass+'">'+
									'<div class="info-title title">' + title + '</div>' +
									'<div class="info-content">' + content + '</div>' +
								'</div>';
				});

				var infoClass = (infoKey != 'goodsInfo') ? 'info-table' : 'preview-goods-info';
				sectionHtml += '<section class="preview-section '+infoClass+'">' + infoHtml + '</section>';
			});

			return sectionHtml;
		},

		orderSummaryTable: function(order) {
			return '<table>' +
				'<tr><th>訂單編號：</th><td><a href="x/order.php?act=info&amp;order_id=' + order.order_id + '" target="_blank">' + order.order_sn + '</a></td>' +
					'<th>銷售人員：</th><td>' + order.salesperson + '</td>' +
					'<th>訂單來源：</th><td>' + order.platform_display + '</td></tr>' +
				'<tr><th>下單時間：</th><td>' + order.formated_add_time + '</td>' +
					'<th>付款時間：</th><td>' + order.formated_pay_time + '</td>' +
					'<th>出貨時間：</th><td>' + order.formated_shipping_time + '</td></tr>' +
				'<tr><th>產品序號：</th><td colspan="5" class="pre-line">' + order.serial_numbers + '</td></tr>' +
				'<tr><th>客戶備註：</th><td colspan="5" class="pre-line">' + order.postscript + '</td></tr>' +
				'<tr><th>商家備註：</th><td colspan="5" class="pre-line">' + order.to_buyer + '</td></tr>' +
			'</table>';
		},

		replaceGoodsRows: function(goodsList, warehouseList) {
			var _this = this;
			var replaceGoodsRows = '';
			$.each(goodsList, function(goodsIndex, goods) {
				var warehouseTagButtons = '';
				$.each(warehouseList, function(warehouseIndex, warehouse) {
					var selected = (warehouse.warehouse_id == 2) ? 'checked="checked" ' : '';
					warehouseTagButtons += 	'<div class="tab-button">' +
												'<input type="radio" class="tab-button-rb" id="replace_warehousing_'+goods.rec_id+'_'+warehouse.warehouse_id+'" name="replace_warehousing_'+goods.rec_id+'" value="'+warehouse.warehouse_id+'" '+ selected +'>' +
												'<label for="replace_warehousing_'+goods.rec_id+'_'+warehouse.warehouse_id+'" class="tab-button-label">'+warehouse.name+((warehouse.is_on_sale != 0)?'(可銷售)':'(需檢查/維修)')+'</label>' +
											'</div>';
				});

				var replaceGoodsRow = 	'<tr>' + 
											'<td>' +
												'<a href="x/goods.php?act=list&amp;keyword=' + goods.goods_sn + '" target="_blank" class="goods_link">' + goods.goods_name + '</a>' +
												(goods.replaced_qty > 0 ? '<span class="replaced-goods-hints"> (已換貨<strong>' + goods.replaced_qty + '</strong>次)</span>' : '') +
											'</td>' +
											'<td>'+goods.goods_price+'</td>' +
											'<td>'+goods.goods_number+'</td>' +
											'<td><input type="number" min="0" pattern="[\d.]*" value="0" class="replace_qty" data-goods-id="' + goods.rec_id + '" data-goods-price="' + goods.goods_price + '" data-goods-number="' + goods.goods_number + '"></td>' + 
											'<td>'+warehouseTagButtons+'</td>' +
										'</tr>';
				var stocksInquiryRow = _this.stocksInquiryTableRow(goods.stocks_inquiry, 5);
				replaceGoodsRows += replaceGoodsRow + stocksInquiryRow;
			});

			return replaceGoodsRows;
		},

		repairGoodsRows: function(goodsList) {
			var _this = this;
			var repairGoodsRows = '';

			$.each(goodsList, function(goodsIndex, goods) {
				var repairGoodsRow = 	'<tr>' + 
											'<td>' +
												'<img src="'+goods.goods_img+'">'+
												'<a href="goods.php?id=' + goods.goods_id + '" class="goods_link" target="_blank">' +
													goods.goods_name +
												'</a>' +
											'</td>' +
											'<td>' +
												'<a href="/x/goods.php?act=list&amp;keyword=' + goods.goods_sn + '" target="_blank">' +
													goods.goods_sn +
												'</a>' +
											'</td>' +
											'<td>' +
												'<div class="select-radio select-repair-goods">'	+
													'<input type="radio" id="repairGoodsRadio'+ goods.goods_id +'" class="select-radio-input" data-goods-id="' + goods.goods_id + '" name="repair_goods" class="select-radio-input" value="' + goods.goods_id + '">'+
													'<label for="repairGoodsRadio'+ goods.goods_id +'" class="repair-radio select-radio-style"></label>' +
												'</div>'+
											'</td>'+
										'</tr>';
				var stocksInquiryRow = _this.stocksInquiryTableRow(goods.stocks_inquiry, 3);
				repairGoodsRows += repairGoodsRow + stocksInquiryRow;
			});
			return repairGoodsRows;
		},

		pickupPointAddressListTableRow: function(addressInfo) {
			var remarks = (addressInfo.shipping_desc) ? '<span class="required">' +addressInfo.shipping_desc+ '</span>' : '';
			var icon = (addressInfo.shop_icon) ? '<img class="pickup-point-icon" src="' + addressInfo.shop_icon + '" />' : '';
			
			var pickupPointTimeslot = '';
				pickupPointTimeslot += (addressInfo.workday) ? '<div>星期一至星期五 : '+ addressInfo.workday +'</div>' : '';
				pickupPointTimeslot += (addressInfo.saturday) ? '<div>星期六 : '+ addressInfo.saturday +'</div>' : '';
				pickupPointTimeslot += (addressInfo.sunday) ? '<div>星期日 : '+ addressInfo.sunday +'</div>' : '';
				pickupPointTimeslot += (addressInfo.holiday) ? '<div>公眾假期 : '+ addressInfo.holiday +'</div>' : '';

			// handle over size goods
			var inputDisabled = (addressInfo.over_max_size == 1 || addressInfo.isBlackList) ? 'disabled=disabled' : '';
			var isOverSize = (addressInfo.over_max_size == 1) ? 'over_size' : '';

			// handle hot pickup point
			// addressInfo.isHot = 1;
			var hotText = (addressInfo.isHot == 1) ? '<div class="hot-pickup-point">此櫃為熱門選擇或需要等候較長時間</div>' : '';

			// handle full pickup point
			var fullText = (addressInfo.isBlackList == 1) ? '<div class="full-pickup-point">此櫃暫時無法選取</div>' : '';

			return 	'<tr>' +
						'<td data-over_size_lang="此提貨點不支援你所選購產品的尺寸" class="'+isOverSize+'">' +
							'<div class="select-radio select-address">' + 
								'<input data-hot="'+ addressInfo.isHot +'" data-area-id="'+addressInfo.shipping_area_id+'" data-shipping-id="'+addressInfo.shipping_id+'" data-ar="'+addressInfo.allotRouter+'" type="radio" id="addressRadio'+ addressInfo.edCode +'" name="lockerCode" class="select-radio-input" value="'+addressInfo.edCode+'" '+inputDisabled+'>' +
								'<label for="addressRadio'+ addressInfo.edCode +'" class="address-list-radio select-radio-style"></label>' +
							'</div>' +
							'<div class="pickup-point-info">'+
								'<div class="pickup-point-address">' +
									icon + '<span class="name">'+ addressInfo.twThrowaddress +'</span>' + remarks +
								'</div>' +
								hotText + fullText +
								'<div class="pickup-point-timeslot display_none">' + pickupPointTimeslot + '</div>' +
							'</div>' +
						'</td>' +
					'</tr>';
		}
	},
	
	init: function() {
		this.getInitDataApi();

		this.readBarcode();
		this.addToCartApi(0, 0);
		this.getCustomerInfoApi();

		this.countryCodePrefix();
		this.checkMobilePhoneNumber();
		this.checkEmailAddress();
		this.changeMemberStatus();
		this.showRemarksDialog();
		this.changeNeedRemovalStatus();

		this.handleShippingCountryOnChange();
		this.handleShippingType();
		this.changePaymentMethod();

		this.handleSalesAgentInfo();
		this.handleDiscount();
		this.handleCoupon();
		this.handleReserve();

		this.assignSalesperson();
		this.changeCash();

		this.confirmOrder();

		// read order
		this.readOrderNumber();
	}, 

	initRepair: function() {
		this.changeRepairSearching();
		this.readBarcode();
		this.readOrderNumber();
		this.handleRepairFee();

		this.countryCodePrefix();
		this.checkMobilePhoneNumber();
		this.checkEmailAddress();	
		this.blurContactFormInput();
		this.handleAccountValidation();

		this.changeCash();
		this.assignSalesperson();
		this.showRepairRemarksDialog();
		this.confirmRepair();

	},

	containsCommonEmailTypos: function(email) {
		var commonTypos = ['gamil.', 'gnail.', 'ganil.', 'gmaiil.', 'gmail.com.hk', 'yhaoo.', 'yaho.', 'yhao.', 'yayoo.', 'yshoo.', 'yaoo.', 'homtail.', 'hotnail.', 'hotamil.', '.con', '.co.hk', '.om', ',com', 'natvigator.'];
		for (var i = 0; i < commonTypos.length; i++)
		{
			if (email.indexOf(commonTypos[i]) > -1)
			{
				return commonTypos[i];
			}
		}
		var foundTypo = '';
		var regexTypos = {'gmail.co$':'gmail.co', 'hotmail.co$':'hotmail.co'};
		$.each(regexTypos, function(regex, typo) {
			if (email.match(new RegExp(regex)))
			{
				foundTypo = typo;
				return false; // breaks out of $.each
			}
		});
		if (foundTypo != '')
		{
			return foundTypo;
		}
		return false;
	},

	cccToCountryCode: function(ccc) {
		var code = 'hk';
		$.each($.fn.intlTelInput.getCountryData(), function(i, country) {
			if (country.dialCode == ccc)
			{
				code = country.iso2;
				return false; // breaks $.each
			}
		});
		return code;
	},

	countryCodePrefix: function() {
		var opts = {
			defaultCountry: 'hk',
			preferredCountries: ['hk','mo','tw','cn'],
			utilsScript: '/yohohk/js/intlTelInput.utils.min.js',
			autoFormat: false,
			autoPlaceholder: false
		};
		
		$('input.mobile-input').intlTelInput(opts);
	},

	checkPhoneNo: function($phoneNo, $countryId) {
		var num_pattern = new RegExp('^[0-9]+$');
		$phoneNo = $.trim($phoneNo).replace(/-/g, "");
		if (num_pattern.test($phoneNo)){
			if (!YOHO.check.isMobile($phoneNo) && !YOHO.check.isTelephone($phoneNo) || ($countryId == 3436 && $phoneNo.length < 11)){
				return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
	},

	getMobileNumberWithPrefix: function($elem) {
		var countryDialCode = $elem.intlTelInput('getSelectedCountryData').dialCode;
		return '+' + countryDialCode + '-' + $elem.val();
	},

	// search goods
	readBarcode: function() {
		var _this = this;
		$('.search_goods_sn').on('keyup', function(event) {
			event.preventDefault();
			if(event.which === _this.const.KEYCODE_ENTER) {
				$(this).blur();
				_this.getGoodsBySnApi($(this).val());
			}

			if(event.which === _this.const.KEYCODE_ESC) {
				$(this).blur();
			}
		}).focus();
	},
	
	loadSearchResults: function(data) {
		var _this = this;
		var goodsList = data.goods_list;
		var $resultTable = $('#resultTable');
		var $resultTableBody = $resultTable.find('tbody');

		// Clear goods list
		$resultTableBody.empty();

		var $error = $('.search .error');
		if(data.status == 1) 
		{
			$error.html('');

			// render goods list
			if(goodsList.length == 1)
			{
				$resultTable.hide();
				// if only one goods in the list, auto add goods to the cart table 
				var goodsId = goodsList[0].goods_id;
				_this.addToCartApi(goodsId, 1);
			}
			else if(goodsList.length > 1)  // if more than one goods in the list, render the goods to result table 
			{
				$resultTable.show();
				// Populate goods info
				$.each(goodsList, function (idx, goods) {
					var status = 'search';
					var tableRow = _this.render.goodsTableRow(goods, status);
					$resultTableBody.append(tableRow);
				});

				_this.handleGoodsAddToCart();
			}

			if($('input[name=mobile]').val() == '') {
				$('input[name=mobile]').focus();
			} else {
				$('input[name=cash_received]').focus();
			}
		}
		else
		{
			$resultTable.hide();
			$error.html(data.error);

			$('.search_goods_sn').focus();
		}

	},

	handleGoodsAddToCart: function() {
		var _this = this;
		// Add to cart when user press ENTER (ascii code = 13) or TAB (trigger blur)
		$('.addtocart-input').on('keyup', function(event) {
			if (event.which == _this.const.KEYCODE_ENTER || event.which === _this.const.KEYCODE_ESC)
			{
				event.preventDefault();
				$(this).trigger('blur'); // trigger the blur event
			}
		}).on('blur', function(event) {
			event.preventDefault();
			var goods_id = $(this).closest('tr').data('goodsId');
			var qty = $(this).val();
			_this.addToCartApi(goods_id, qty);
		});
		
		// Alternatively user can click the add to cart button
		$('.addtocart-button').on('click', function(event) {
			var $goods_input = $(this).closest('tr').find('.addtocart-input');
				$goods_input.trigger('blur'); // trigger the blur event binded above
		});
	},

	// cart table
	handleGoodsUpdateToCart: function() {
		var _this = this;
		$('.updatecart-input').on('keyup', function(event) {
			if (event.which == _this.const.KEYCODE_ENTER || event.which === _this.const.KEYCODE_ESC)
			{
				event.preventDefault();
				$(this).trigger('blur'); // trigger the blur event
			}
		}).on('blur', function(event) {
			event.preventDefault();
			var goods_id = $(this).closest('tr').data('goodsId');
			var qty = $(this).val();
			_this.addToCartApi(goods_id, qty);
		});
	},

	handleGoodsRemoveFromCart: function() {
		var _this = this;
		$('.remove-button').on('click', function(event) {
			event.preventDefault();

			var goodsId =  $(this).closest('tr').data('goodsId');
			var isGift =  $(this).closest('tr').data('isGift');
	
			_this.addToCartApi(goodsId, 0, isGift, 0, 0);
		})
	},

	handleCartList: function(cartList, mode) {
		var _this = this;
		// Save the cart list into our data object
		_this.data.goodsList = cartList.goods_list;

		var $cartTable = $('#cartTable');
		var $cartTableBody = $cartTable.find('tbody');

		// Clear cart Table
		$cartTableBody.empty();

		if(cartList.goods_list.length > 0) 
		{
			$.each(cartList.goods_list, function (idx, goods) {
				if(goods.is_package == 0) {
					var status = 'cart';
					var tableRow = _this.render.goodsTableRow(goods, status);
					$cartTableBody.append(tableRow);
				}
			});

			_this.handleGoodsUpdateToCart();
			_this.handleGoodsRemoveFromCart();

			// show package on cart table
			if(Object.keys(cartList.package_list).length > 0) {
				$.each(cartList.package_list, function (idx, package) {
					var status = 'cart';
					var tableRows = _this.render.packageTableRows(package, status);
					$cartTableBody.append(tableRows);
				});

				_this.handlePackageRemoveToCart();
			}

			$cartTable.show();
		}
		else
		{
			// hide cart table
			$cartTable.hide();
		}
	},

	// WEEE
	handleWEEEform: function(needRemoval) {
		var _this = this;
		if(Object.keys(needRemoval).length > 0) {
			var itemsForm = '';

			$.each(needRemoval, function(id, item) {
				itemsForm += _this.render.needRemovalItem(item)
			})
			$('#collection_total').html(itemsForm);
			$('.need_removal').show();
		} else {
			$('input[name="need_removal"]').prop("checked", false).trigger('change');
			$('.need_removal').hide();
		}
	},

	changeNeedRemovalStatus: function() {
		var _this = this;
		$('input[name="need_removal"]').change(function(){
			if ($(this).val() == 1) {
				// the checkbox is now checked
				$('.need_removal_form').fadeIn();

				if($('input[name="shippingType"]:checked').val() == _this.const.EXPRESS_SHIPPING_TYPE) {
					var _currentAddress = _this.data.currentAddress;
					if(Object.keys(_currentAddress).length > 0) {
						$('input[name="nr_address"]').val(_currentAddress.format_address);
						$('input[name="nr_consignee"]').val(_currentAddress.name);
						$('input[name="nr_phone"]').val(_currentAddress.phone);
					}
				}
			} else {
				// the checkbox is now no longer checked
				$('.nr_consignee input').val('');
				$('.need_removal_form').hide();
			}
		});
	},

	// favourable table
	handleFavourableTable: function(data) {
		var _this = this;
		var $favourableTable = $('#favourableTable');
			$favourableTable.empty();

		if(data.favourable_list.length > 0) {
			$.each(data.favourable_list, function (idx, favourable) {
				if(!favourable.available) return;
				var tableRows = '';
				var status = 'favourable';

				$.each(favourable.gift, function (idx, goods) {
					goods.act_id = favourable.act_id;
					goods.act_kind = favourable.act_kind;
					var tableRow = _this.render.goodsTableRow(goods, status);
					tableRows += tableRow;
				});

				var renderFavourableTable = _this.render.goodsTable(favourable, tableRows, status);
				$favourableTable.append(renderFavourableTable);
			});

			$favourableTable.show();
		} else {
			$favourableTable.hide();
		}

		_this.handleFavourableAddToCart();

		if(data.need_update) {
			_this.addToCartApi(0, 0, 0, 2, 0);
		}
	},

	handleFavourableAddToCart: function() {
		var _this = this;

		$('.fa_addtocart_btn').on('click', function(event) {
			var goods_id = $(this).closest('tr').data('goodsId');
			var qty = $('.fa_addtocart_input').val(); // 1
			var act_id = $(this).closest('tr').data('actId');
			_this.addFavourableApi(goods_id, qty, act_id);
		});
	},

	// package table
	handlePackageTable: function(data) {
		var _this = this;
		var $packagesTable = $('#packagesTable');
			$packagesTable.empty();

		if(data.packages_list.length > 0) {
			$.each(data.packages_list, function (idx, package) {
				var tableRows = '';
				var status = 'packages';

				var tableRows = _this.render.packageTableRows(package, status);
				var renderPackagesTable = _this.render.goodsTable(package, tableRows, status);
				$packagesTable.append(renderPackagesTable);
			});


			$packagesTable.show();
		} else {
			$packagesTable.hide();
		}

		_this.handlePackageAddToCart();
	},

	handlePackageAddToCart: function() {
		var _this = this;
		// Add to cart when user press ENTER (ascii code = 13) or TAB (trigger blur)
		$('.package-addtocart-input').on('keyup', function(event) {
			if (event.which == _this.const.KEYCODE_ENTER || event.which == _this.const.KEYCODE_ESC)
			{
				event.preventDefault();
				$(this).trigger('blur'); // trigger the blur event
			}
		}).on('blur', function(event) {
			event.preventDefault();
			var _packageTable = $(this).parents('table');
			var packageId = _packageTable.data('actId');
			var fromThisGoodsId = _packageTable.data('goodsId')
			var qty = $(this).val();

			_this.addPackageApi(packageId, fromThisGoodsId, qty);
		});
	
		$('.package-addtocart-button').on('click', function(event) {
			var $package_input = $(this).closest('tr').find('.package-addtocart-input');
				$package_input.trigger('blur');
		});

	},

	handlePackageRemoveToCart: function() {
		var _this = this;
		$('.package-remove-button').on('click', function(event) {
			event.preventDefault();
			var goodsId =  $(this).closest('tr').data('goodsId');
			var packageId =  $(this).closest('tr').data('actId');

			_this.addToCartApi(goodsId, 0, 0, 0, packageId);
		})
	},

	// require goods table
	handleRequireGoodsTable: function(data) {
		var _this = this;
		var $requireGoodsTable = $('#requireGoodsTable');
		var $need_requiregoods = $('.need_requiregoods');

		// Clear require goods table
		$requireGoodsTable.html('');

		if(data.require_goods_list.length > 0) {
			$.each(data.require_goods_list, function (idx, require_goods) {
				var tableRows = '';
				var status = 'require-goods';

				$.each(require_goods.goods_list, function (idx, goods) {
					var tableRow = _this.render.goodsTableRow(goods, status);
					tableRows += tableRow;
				});

				var renderRequireGoodsTable = _this.render.goodsTable(require_goods, tableRows, status);
				$requireGoodsTable.append(renderRequireGoodsTable);
			});

			$requireGoodsTable.show();
			$need_requiregoods.show();
		} else {
			$requireGoodsTable.hide();
			$need_requiregoods.hide();
			$('input[name=need_requiregoods]').prop("checked", false);
		}

		_this.handleRequireGoodsAddToCart();
	},

	handleRequireGoodsAddToCart: function() {
		var _this = this;

		$('.rg_addtocart_btn').on('click', function(event) {
			var goods_id = $(this).closest('tr').data('goodsId');
			var qty = $('.rg_addtocart_input').val(); // 1
			_this.addToCartApi(goods_id, qty);
		});
	},

	// user account
	checkMobilePhoneNumber: function() {
		var _this = this;
		var $mobile = $('input[name="mobile"]');
		$mobile.on('keyup', function (event) {
			var ccc = $mobile.intlTelInput('getSelectedCountryData').dialCode;
			var mobile = $mobile.val() || '';
			var $mobileError = $('#mobile_hint');

			var errorMsg = '';
			if ((mobile != '') && (!YOHO.check.isMobile(mobile, ccc)))
			{
				errorMsg = '手提電話號碼不正確';
			}
			$mobileError.html(errorMsg);

			// TODO: test account?? but will create new one
			/** if ((mobile == '11111111') || (mobile == '88888888') || (mobile == '99999999'))
			{
				$('input[name="consignee"]').closest('tr').show();
			}
			else
			{
				$('input[name="consignee"]').closest('tr').hide();
			} **/
		});

		// Also monitor change from intlTelInput
		$mobile.on('change input', function () {
			_this.showIntegral(false);
			$('.pay_points').html('');
			$mobile.removeClass('has-error');
			$mobile.trigger('keyup');
		});
	},

	checkEmailAddress: function() {
		var _this = this;
		var $email = $('input[name="email"]');
		$email.on('keyup', function (event) {

			if(event.which === _this.const.KEYCODE_ESC) $(this).blur();

			var email = $email.val() || '';
			var $emailError = $('#email_hint');
			var errorMsg = '';
			if (email != '')
			{
				var typo = _this.containsCommonEmailTypos(email);
				if (typo)
				{
					errorMsg = '偵測到常見錯誤“' + typo + '”';
				}
				else if (!YOHO.check.isEmail(email))
				{
					errorMsg ='電郵地址不正確';
				}
			}
			$emailError.html(errorMsg);
		});
	},

	changeMemberStatus: function() {
		var _this = this;
		$('input[name=isMember]').on('change', function(event) {
			var target = event.target;
			var $checkMemberbtn = $('.check-member-status-btn');
			var isMember = $(target).val();
			if(isMember == 1) 
			{
				$checkMemberbtn.show();
			}
			else
			{
				$checkMemberbtn.hide();
				$('input[name="mobile"]').removeClass('has-error');
				$('#mobile_hint').html('');
				$('#email_hint').html('');

				// calculate the order amount if integral is using
				_this.showIntegral(false);
				$('input[name=integral]').val('').trigger('blur');
			}
		});

		// validate user account
		_this.handleAccountValidation();

		_this.handleIntegral();
	},

	handleIntegral: function() {
		var _this = this;
		$('input[name="integral"]').on('keyup', function(event) {
			if (event.which == _this.const.KEYCODE_ENTER || event.which === _this.const.KEYCODE_ESC)
			{
				event.preventDefault();
				$(this).trigger('blur');
			}
		}).blur(function(event) {
			event.preventDefault();
			var integral = $('input[name=integral]').val();
			_this.data.integral = (integral == '') ? 0 : integral;

			_this.calculateAmountApi();
		});
		$('.integral-btn').on('click', function(event) {
			$('input[name="integral"]').trigger('blur');
		});
	},

	handleIntegralTagLabel: function() {
		var _this = this;
		var _data = this.data;

		var $integralInput = $('input[name=integral]');
			$integralInput.val('');
		var $integralButton = $('.integral-btn');
		var $integralTag = $('.integral-tag');
			$integralTag.empty();
		
		if(_data.integral != 0) {
			$integralInput.prop('disabled', true);
			$integralButton.prop('disabled', true);

			var integralTag = _this.render.tagLabel(_data.integral, '', true);
				$integralTag.html(integralTag);

			// remove tag
			$integralTag.find('.remove-tag').on('click', function(e) {
				e.preventDefault();
				$integralInput.val('').trigger('blur');
			});
		} else {
			$integralInput.prop('disabled', false);
			$integralButton.prop('disabled', false);
		}
	},

	handlePayPoints: function() {
		var _this = this;
		$('span.pay_points').off('click').on('click', function(event) {
			if(_this.data.integral == 0) {
				var $interalInput = $('input[name="integral"]');
				var user_integral = parseInt($.trim($(this).text())) || 0;
				var integral_scale = parseFloat($.trim($('#integral_scale').text())) || 0.5;
				var one_dollar_integral = (100 / integral_scale) || 100;
				var optimum_integral = parseInt(user_integral / one_dollar_integral) * one_dollar_integral;
				$interalInput.val(optimum_integral);
				$interalInput.prop('disabled', false);
				$('.integral-btn').prop('disabled', false);
			}

			$('.cart-info').animate({
				scrollTop: ($('.member-info-integral').first().offset().top)
			},500);
		});
	},

	handleAccountValidation: function() {
		var _this = this;
		var $mobile = $('input[name="mobile"]');
		$mobile.on('keyup', function(event) {
			if(event.which == _this.const.KEYCODE_ENTER || event.which === _this.const.KEYCODE_ESC) 
			{
				event.preventDefault();
				$(this).trigger('blur');
			}
		}).on('blur', function(event) {
			var ccc = $(this).intlTelInput('getSelectedCountryData').dialCode;
			var mobile = $(this).val();
			var userName = (ccc && mobile != '') ? '+' + ccc + '-' + mobile : mobile;
			if(userName != _this.data.user_name) {
				_this.validateAccountApi(userName);
			}

			if(_this.data.mode != 'repair') {
				$('.cash_received').focus();
			} else {
				$('input[name=repair_fee_amount]').focus().select();
			}
		});

		$('.check-member-status-btn').on('click', function(event) {
			$mobile.trigger('blur');
		});
	},

	handleMemberInfoAutoFill: function(userName, email) {
		var _this = this;
		if (userName)
		{
			var matches = userName.match(/^\+([0-9]+)-([0-9]+)$/);
			var $mobile = $('input[name="mobile"]');
			if (matches)
			{
				var ccc = matches[1];
				var mobile = matches[2];
				$mobile.intlTelInput('selectCountry', _this.cccToCountryCode(ccc));
				$mobile.val(mobile).trigger('keyup');
			}
			else
			{
				$mobile.val(userName).trigger('keyup');
			}
		}
		if (email) $('input[name="email"]').val(email).trigger('keyup');
	},

	handleCustomerInfo: function(customerInfoList) {
		var _this = this;

		// render select
		$select = $('.customer-select');
		$select.empty();

		if(customerInfoList.length > 0) {
			var customerInfoSelectOption = _this.render.selectOption(0, '請選擇');
			$.each(customerInfoList, function(index, item) {
				customerInfoSelectOption += _this.render.customerInfoSelectOption(item);
			});
			$select.append(customerInfoSelectOption);
			

			$select.off('change').on('change', function(event){
				event.preventDefault();

				var selectedItem = $(this).find(':selected');
				var rec_id = selectedItem.data('rec_id');
				var ccc = selectedItem.data('ccc');
				var mobile = selectedItem.data('mobile');
				var email = selectedItem.data('email');
				var user_id = selectedItem.data('user_id');

				if(rec_id) {
					var user_name = ccc ? '+' + ccc + '-' + mobile : mobile;
					if(user_id) {
						_this.validateAccountApi(user_name);
					} else {
						_this.data.user_id = 0;
						_this.handleMemberInfoAutoFill(user_name, email);
						_this.showIntegral(false);
						$('input[name=isMember][value=0]').trigger('click');
						$('input[name="mobile"]').trigger('keyup').trigger('blur');
					}
				}
			});
		} else {
			$select.append(_this.render.selectOption(0, '暫沒客戶資料'));
		}

		$('.refresh-customer-info').off('click')
								.on('click', function () {
									$select.append(_this.render.selectOption(0, '載入中...'));
									_this.getCustomerInfoApi();
								});
	},

	showIntegral: function(isShow) {
		$('.member-info-integral-display').toggle(isShow);
		$('.member-info-integral').toggle(isShow);
	},

	// discount
	handleDiscount: function() {
		var _this = this;
		var $discountInput = $('input[name=discount]');

		$discountInput.on('keyup', function(event) {
			if(event.which == _this.const.KEYCODE_ENTER || event.which === _this.const.KEYCODE_ESC) 
			{
				event.preventDefault();
				$(this).trigger('blur');
			}
		}).on('blur', function(event) {
			event.preventDefault();
			var discountVal =  $('input[name=discount]').val();
			_this.data.discount = (discountVal != '') ? discountVal : 1;

			_this.calculateAmountApi();
		});

		$('.discount-btn').on('click', function(event) {
			$discountInput.trigger('blur');
		});
	},

	handleDiscountTagLabel: function() {
		var _this = this;
		var _data = this.data;

		var $discountInput = $('input[name=discount]');
			$discountInput.val('');
		var $discountButton = $('.discount-btn');
		var $discountTag = $('.discount-tag');
			$discountTag.empty();
		
		if(_data.discount != 1) {
			$discountInput.prop('disabled', true);
			$discountButton.prop('disabled', true);

			var discountTag = _this.render.tagLabel(_data.discount, '', true);
				$discountTag.html(discountTag);

			// remove tag
			$discountTag.find('.remove-tag').on('click', function(e) {
				e.preventDefault();
				$discountInput.val('').trigger('blur');
			});
		} else {
			$discountInput.prop('disabled', false);
			$discountButton.prop('disabled', false);
		}
	},

	// coupon
	handleCoupon: function() {
		var _this = this;
		var $couponInput = $('input[name=coupon]');

		$couponInput.on('keyup', function(event) {
			if(event.which == _this.const.KEYCODE_ENTER || event.which === _this.const.KEYCODE_ESC) 
			{
				event.preventDefault();
				$(this).trigger('blur');
			}
		}).on('blur', function(event) {
			event.preventDefault();
			if($(this).val() == '') return;

			var _data = _this.data;
			var coupon_codes = $(this).val().toUpperCase().split(',');
			$.merge(_data.coupon_codes, coupon_codes);

			_this.useCoupon(_data.coupon_codes);

			$(this).val('');
		});

		$('.coupon-btn').on('click', function(event) {
			$couponInput.trigger('blur');
		});
	},

	useCoupon: function(coupon_codes) {
		var _this = this;
		
		// Remove whitespaces
		coupon_codes = $.map(coupon_codes, function(code) {
			return $.trim(code);
		});
		// Remove empty strings and duplicates
		coupon_codes = $.grep(coupon_codes, function(code, i) {
			return (code != '') && ($.inArray(code, coupon_codes) === i);
		});

		_this.data.coupon_codes = coupon_codes;
		_this.calculateAmountApi();
	},

	handleCouponTagLabel: function(coupon_code) {
		var _this = this;
		var _data = _this.data;
		var $couponCodesTag = $('.coupon-codes-tag');
			$couponCodesTag.empty();

		var couponList = (coupon_code == '') ? [] : coupon_code.split(',');
		_data.coupon_codes = couponList;
		if(couponList.length > 0) {
			var couponCodesTag = '';
			$.each(couponList, function(i, coupon) {
				couponCodesTag += _this.render.tagLabel(coupon, '', true);
			})
			$couponCodesTag.append(couponCodesTag);

			_this.removeCouponTag();
		}
	},

	removeCouponTag: function() {
		var _this = this;
		var _data = this.data;
		$('.coupon-codes-tag .remove-tag').on('click', function(e) {
			e.preventDefault();
			var $this = $(this);
			var coupon = $this.parent().find('span').html();
			_data.coupon_codes = $.grep(_data.coupon_codes, function(value) {
				return value != coupon;
			});

			_this.useCoupon(_data.coupon_codes);
		});
	},

	// sales agent
	handleSalesAgentInfo: function() {
		var _this = this;
		var _data = _this.data;
		var $saOrderInput = $('.sales-agent-order-input');

		$saOrderInput.on('keyup', function(event) {
			if(event.which == _this.const.KEYCODE_ENTER || event.which === _this.const.KEYCODE_ESC) 
			{
				event.preventDefault();
				$(this).trigger('blur');
			}
		}).on('blur', function(event) {
			event.preventDefault();
			var saOrder = $(this).val();
			
			if(_data.payment == _this.const.REDEEM_PAYMENT){
				if(_data.saId == 0) {
					YOHO.dialog.warn('請選擇代理！');
				} else {
					if(saOrder != '') {
						// validate the goods can sales agent
						var goodsIds = $.map(_data.goodsList, function(goods){
							return goods.goods_id;
						});
						
						_this.validateSaGoodsApi(goodsIds);
					} else {
						// reset sales agent tag label
						_this.handleSalesAgentTagLabel();
					}
				}
			}
		});

		$('.sales-agent-order-btn').on('mouseup', function(event) {
			event.preventDefault();
			var tagLabelLength = $('.sales-agent-tag').children().length;
			if(tagLabelLength.length == 0) {
				$saOrderInput.trigger('blur');
			}
		});

		$('.sa_select').on('change', function(event) {
			event.preventDefault();
			_data.saId = $(this).val();
			$('.sales-agent-order-input').blur();
		});
	},

	handleSalesAgentTagLabel: function() {
		var _this = this;
		var _data = this.data;
			_data.saOrder = $('.sales-agent-order-input').val();


		var $saOrderInput = $('.sales-agent-order-input');
			$saOrderInput.val('');
		var $saOrderButton = $('.sales-agent-order-btn');
		var $saOrderTag = $('.sales-agent-tag');
			$saOrderTag.empty();
		
		if(_data.saOrder != '') {
			$saOrderInput.prop('disabled', true);
			$saOrderButton.prop('disabled', true);

			var saOrderTag = _this.render.tagLabel(_data.saOrder, '', true);
				$saOrderTag.html(saOrderTag);

			// remove tag
			$saOrderTag.find('.remove-tag').on('click', function(e) {
				e.preventDefault();
				$saOrderInput.val('').trigger('blur');
			});
		} else {
			$saOrderInput.prop('disabled', false);
			$saOrderButton.prop('disabled', false);
		}
	},

	// remark
	showRemarksDialog: function() {
		var _this = this;
		var $remarksDialog = $('.remarks-dialog');
		$('.remarks-button, .remarks-edit-button').on('click', function(event) {
			event.preventDefault();

			$remarksDialog.show();
		});

		$('textarea').on('keyup', function(event) {
			event.preventDefault();
			if(event.which === _this.const.KEYCODE_ESC) $(this).blur();
		})

		// close dialog
		$('.remarks-dialog .dialog-close').on('click', function(event) {
			event.preventDefault();

			// storage remarks info
			$('textarea[name="serial_numbers"]').val(_this.data.serialNumber);
			$('textarea[name="postscript"]').val(_this.data.postscript);
			$('textarea[name="to_buyer"]').val(_this.data.internalRemarks);

			$remarksDialog.hide();
		});

		// confirm remarks info
		_this.confirmRemarksInfo();
	},

	confirmRemarksInfo: function() {
		var _this = this;
		$('.remarks-dialog .confirm-button').on('click', function(event) {
			event.preventDefault();
			var $textareSerialNumbers = $('textarea[name="serial_numbers"]');
			var serialNumbersVal = $.trim( $textareSerialNumbers.val() );
			var $textarePostscript = $('textarea[name="postscript"]');
			var postscriptVal = $.trim( $textarePostscript.val() );
			var $textareInternalRemark = $('textarea[name="to_buyer"]');
			var internalRemarksVal = $.trim( $textareInternalRemark.val() );
			var orderRemarksVal = $.trim( _this.data.orderRemarks );

			var $serialNumber = $('.serial-numbers');
			var $postscript = $('.postscript');
			var $internalRemarks = $('.internal-remarks');
			var $orderRemarks = $('.order-remarks');

			// display remarks to remark info
			$serialNumber.toggle(serialNumbersVal !== '');
			$serialNumber.find('.content').html(serialNumbersVal);

			$postscript.toggle(postscriptVal !== '');
			$postscript.find('.content').html(postscriptVal);

			$internalRemarks.toggle(internalRemarksVal !== '');
			$internalRemarks.find('.content').html(internalRemarksVal);

			$orderRemarks.toggle(orderRemarksVal !== '');
			$orderRemarks.find('.content').html(orderRemarksVal);

			// storage remarks info
			_this.data.serialNumber = serialNumbersVal;
			_this.data.postscript = postscriptVal;
			_this.data.internalRemarks = internalRemarksVal;

			// handle remarks info display
			var $remarksContent = $('.remarks-content');
				$remarksContent.find('.remarks-button').toggle(serialNumbersVal == '' && postscriptVal == '' && orderRemarksVal == '' && internalRemarksVal == '');
				$remarksContent.find('.remarks-info').toggle(serialNumbersVal !== '' || postscriptVal !== '' || orderRemarksVal !== '' || internalRemarksVal !== '');

			$('.remarks-dialog').hide();
		});
	},

	// payment
	changePaymentMethod: function() {
		var _this = this;
		$('input[name=paymentMethod]').on('change', function(e) {
			e.preventDefault();
			var previousPayment = _this.data.payment;
			var paymentMethodVal = $(this).val();
			_this.data.payment = paymentMethodVal;

			if(paymentMethodVal == _this.const.REDEEM_PAYMENT){ // redeem
				$('input[name="isReserve"][value=0]').trigger('click');
				$('.sales-agent-wrapper').show();
				$('.reserve-buttons').hide();
			} else {
				$('.sales-agent-wrapper').hide();
				$('.reserve-buttons').show();

				$('input[name="sa-order-input"]').val('').trigger('blur');
				$('.sa_select').val(0).trigger('change');
			}

			if(	(previousPayment != 99 && paymentMethodVal == 99) || 
				(previousPayment == 99 && paymentMethodVal != 99)) {
				// 換領時, 沒有贈品
				_this.addToCartApi(0, 0);
			} else {
				_this.calculateAmountApi();
			}
		});
	},

	// shipping
	handleShippingType: function() {
		var _this = this;
		var _data = _this.data;
		$('input[name="shippingType"]').on('click', function(event) {
			var shippingType = $(this).val();
			if(_data.shippingType != shippingType) {
				_data.shippingType = shippingType;
				_data.currentAddress = {};
				_this.showFormatedAddress('');
			}

			if(shippingType == _this.const.EXPRESS_SHIPPING_TYPE) {
				handleMemberAddress();
				_this.isReserveOnly(true);
			} else if(shippingType == _this.const.PICKUP_POINT_SHIPPING_TYPE) {
				_this.ajaxShippingTypeFlowRequest('shipping_type');
				_this.isReserveOnly(true);
			} else {
				_data.country = _this.const.HK_COUNTRY_CODE_ID;
				_data.shipping = 2;
				_data.shipping_area_id = '';

				$('.express-shipping-method-wrapper').hide();
				$('.express-shipping-list').empty();

				_this.isReserveOnly();

				if(_data.goodsList.length > 0) {
					_this.calculateAmountApi();
				}
			}
		});

		$('.shipping-address-edit-button').on('click', function(event) {
			if(_data.shippingType == _this.const.EXPRESS_SHIPPING_TYPE) {
				handleMemberAddress();
			} else if(_data.shippingType == _this.const.PICKUP_POINT_SHIPPING_TYPE) {
				_this.ajaxShippingTypeFlowRequest('shipping_type');
			}
		});

		function handleMemberAddress() {
			var _data = _this.data
			var userId = _data.user_id;
			if(userId > 0) {
				// is member, get user address
				_this.ajaxShippingTypeFlowRequest('shipping_type');
			} else {
				$('input[name=isMember][value=0]').trigger('click');
				// is not a member, open the shipper address dialog
				_this.showShipperAddressDialog(_data.currentAddress);
			}
		}
	},

	showFormatedAddress: function(address) {
		$('.shipping-address').toggle(!!address);
		$('.shipping-address-display').html(address);
	},

	// shipper address list dialog
	showAddressListDialog: function(data, shipping_type_code) {
		var _this = this;
		if(shipping_type_code == _this.const.EXPRESS_SHIPPING_TYPE) {
			var selectedAddress = null;
			var $shipperAddressListDialog = $('.shipper-address-list-dialog');
				$shipperAddressListDialog.show();
			
			// render address list to table
			var $shipperAddressListTableBody = $('.shipper-address-list-table').find('tbody');
				$shipperAddressListTableBody.empty();

			var address_list = data.list;
			$.each(address_list, function(key, address) {
				var $addressListRow = $(_this.render.addressListTableRow(address));
				$shipperAddressListTableBody.append($addressListRow);

				$addressListRow.on('click', '.address-edit-button', function(event) {
					// _this.showShipperAddressDialog(address);
				}).on('click', 'input[name="address_radio"]', function(event){
					selectedAddress = address;
				});
			});

			// handle add new address button
			var $addNewAddressBtn = $('.add-new-address-button');
			var $editNewAddressBtn = $('.edit-new-address-button');
			// if(address_list.length >= 5) {
			// 	$addNewAddressBtn.hide();
			// 	$editNewAddressBtn.hide();
			// } else {
			// 	$addNewAddressBtn.show();
				// add new address
				if(Object.keys(_this.data.currentAddress).length == 0 || _this.data.currentAddress.id != 0) {
					$addNewAddressBtn.show();
					$editNewAddressBtn.hide();

					$addNewAddressBtn.off('click').on('click', function(event) {
						_this.showShipperAddressDialog();
					});
				} else {
					$addNewAddressBtn.hide();
					$editNewAddressBtn.show();

					$editNewAddressBtn.off('click').on('click', function(event) {
						_this.showShipperAddressDialog(_this.data.currentAddress);
					});
				}
			// }

			// close dialog
			$shipperAddressListDialog.find('.dialog-close')
									.off('click').on('click', function(e) {
										$shipperAddressListDialog.hide();
									});
			
			$shipperAddressListDialog.find('.confirm-button').off('click').on('click', function(event) {
				event.preventDefault();
				if(selectedAddress == null) {
					if(Object.keys(_this.data.currentAddress).length == 0) {
						YOHO.dialog.warn('請選擇收貨人地址');
					} else {
						$shipperAddressListDialog.hide();
					}
				} else {
					_this.confirmAddressListDialog(selectedAddress);
				}
			});
		}
	},

	confirmAddressListDialog: function(selectedAddress) {
		var _this = this;

		_this.data.currentAddress = selectedAddress;
		_this.data.country = selectedAddress.country;

		var $shipperAddressListDialog = $('.shipper-address-list-dialog');
		var $selectedAddress = $shipperAddressListDialog.find('input[name="address_radio"]:checked');
		var $selectedAddressRow = $selectedAddress.parents('tr');
		var $selectedAddressId = selectedAddress.id;

		$shipperAddressListDialog.hide();
		if( !_this.checkAddressInfoApi($selectedAddressId) ) {
			YOHO.dialog.warn('請輸入收貨人資料');
			var editeAddressBtn = $selectedAddressRow.find('.address-edit-button');
				editeAddressBtn.trigger("click");
		} else {
			var addressInfo = _this.data.currentAddress;
			var $formatedAddress = addressInfo.address;
			_this.showFormatedAddress($formatedAddress);
			_this.ajaxShippingTypeFlowRequest('address',
				{
					address_id: $selectedAddressId,
					consignee: {
						country: addressInfo.country,
						province: addressInfo.province,
						city: addressInfo.city,
						district: addressInfo.district,
					}
				}
			);
		}
	},

	// shipper address dialog
	showShipperAddressDialog: function(address) {
		var _this = this;
		$('#searchAddressInput').val('');
		$('.shipper-address-list-dialog').find('.dialog-close').trigger('click');


		var $shipperAddressDialog = $('.shipper-address-dialog');
			$shipperAddressDialog.show();

		// reset error message
		$shipperAddressDialog.find('.error').each(function(i, errorElem) {
			$(errorElem).html('');
		});

		// Set input blur when press 'esc'
		$shipperAddressDialog.off('keyup')
							.on('keyup', 'input', function(event) {
								event.preventDefault();
								if(event.which === _this.const.KEYCODE_ESC) $(this).blur();
							});


		// handle address info to dialog form
		address = address || {};
		_this.fillAddressInfo($shipperAddressDialog, address);

		// show detail shipper address
		_this.showDetailAddressInput();

		// select building info
		_this.handleBuildingInfoSelect();

		// confirm shipper address
		_this.confirmShipperAddressDialog($shipperAddressDialog, address);

		// close dialog
		$shipperAddressDialog.find('.dialog-close')
							.off('click').on('click', function(e) {
								_this.closeShipperAddressDialog(e);
							});
	},

	confirmShipperAddressDialog: function($shipperAddressDialog, address) {
		var _this = this;
		var _data = _this.data;
		$shipperAddressDialog.find('.confirm-button').off('click').on('click', function(event) {
			event.preventDefault();

			if(validateShipperAddressForm()) {
				var addressFields = $shipperAddressDialog.find( ":input" ).serializeArray();
				var params = parseAddressInfo(addressFields);

				_data.currentAddress = params;
				_data.country = params.country;

				_this.showFormatedAddress(_data.currentAddress.format_address);
				_this.triggerShipperAddressCloseButton();

				_this.ajaxShippingTypeFlowRequest(
					'address', 
					{consignee: {
						country: _data.currentAddress.country,
						province: _data.currentAddress.province,
						city: 0,
						district: 0,
					}}
				);
			}
		});
		

		function parseAddressInfo(addressFields) {
			var params = {}; 
			$.each(addressFields, function(index, field) {
				var fieldName = field.name;
				params[fieldName] = $.trim(field.value);
			});

			params.id = 0;

			if(params.country != _this.const.HK_COUNTRY_CODE_ID) {
				params.dest_type = 0;
				params.lift = 0;
			}

			if(params.country != _this.const.CHINA_COUNTRY_CODE_ID) {
				params.id_doc = '';
			}

			if(params.country == _this.const.HK_COUNTRY_CODE_ID || params.country == _this.const.MACAU_COUNTRY_CODE_ID) {
				params.zipcode = '';
			}
			return params;
		}

		function validateShipperAddressForm() {
			var isError = false;
			var country = _this.data.country;
	
			// consigee name
			var name = $('.consignee-info').find('input[name=name]').val();
			if (!YOHO.check.isValidName(name)) {
				$('#nameError').html('請輸入正確的收貨人姓名');
				isError = true;
			} else {
				$('#nameError').html('');
			}
	
			// consigee mobile number
			var $mobile = $('input[name="phone"]');
			var mobile = $.trim($mobile.val()) || '';
			var $mobileError = $('#consigeeMobileError');
			if (!_this.checkPhoneNo(mobile, country)) {
				if (country == 3436){
					$mobileError.html('請輸入正確的11位手機號碼');
				} else {
					$mobileError.html('請輸入正確的電話號碼');
				}
				isError = true;
			} else {
				$mobileError.html('')
			}
	
			// consigee id doc
			var idDocVal = $('input[name=id_doc]').val()
			if (!checkidNo(idDocVal, country)) {
				$('#idDocError').html('輸入18位內地身份證號碼');
				isError = true;
			} else {
				$('#idDocError').html('');
			}

			// address
			if($('#consigneeAddressInput').val() == '') {
				isError = true;
				$('#addressError').html('請輸入地址');
			}
	
			// lift info
			if (country == 3409){
				var liftErrorMsg = '';
				var destTypeVal = $('select[name=dest_type]').val();
				var liftVal = $('select[name=lift]').val();
	
				if (destTypeVal == "" || destTypeVal == -1) {
					liftErrorMsg = '請選擇地址類型';
					isError = true;
				} else if (liftVal == "" || liftVal == 0) {
					liftErrorMsg = '請選擇是否設有升降機';
					isError = true;
				}
				$('#liftInfoError').html(liftErrorMsg);
			} else {
				// zipcode
				if($('input[name=zipcode]').val() == '') {
					isError = true;
					$('#zipcodeError').html('請輸入郵遞區號');
				}
			}
	
			return !isError;
	
			function checkidNo($idNo, $countryId) {
				//var num_pattern = new RegExp('^[0-9]+$');
				$idNo = $.trim($idNo);
				if ($countryId == 3436){
					//if (!num_pattern.test($idNo) || $idNo.length < 18 || $idNo.length > 18){ // check number only and length 18
					if ($idNo.length < 18 || $idNo.length > 18){ // check length 18
						return false;
					} else {
						return true;
					}
				} else {
					return true;
				}
			}
		}
	},

	

	closeShipperAddressDialog: function(e) {
		$('#detail_address').show();
		$('.auto-address').hide();
		$('select[name=country]').val(this.const.HK_COUNTRY_CODE_ID);
		$(e.target).parents('.pos-dialog').hide();
	},

	triggerShipperAddressCloseButton: function() {
		$('.shipper-address-dialog').find('.dialog-close').trigger('click');
	},

	showDetailAddressInput() {
		$(".edit_address").off('click').on('click',function(event){
			event.preventDefault();
			$('#detail_address').hide();
			$('.auto-address').show();
			$('.confirm-button').show();
		});
	},

	fillAddressInfo(_shippingDialog, address) {
		var _this = this;

		if(Object.keys(address).length > 0) {
			$('.address_not_search_btn').hide();
			$('.address_not_on_list_btn').show();

			_this.changeShippingCountry(address.country);

			$.each(address, function(index,value){ 
				if(value && value.length > 0){
					if(index == 'dest_type' && value == 0) value = -1;

					var $inputElement = _shippingDialog.find('input[name='+index+']');
					var $selectElement = _shippingDialog.find('select[name='+index+']')
					if($inputElement) $inputElement.val(value);
					if($selectElement) $selectElement.val(value).trigger('change');
				}
			});
			// $("#format_address_display").html($('input[name=format_address]').val());
			// $('input[name=format_address]').val('');
		} else {
			$('.address_not_search_btn').show();
			$('.address_not_on_list_btn').hide();

			// reset address display
			var addressFields = _shippingDialog.find( ":input" ).serializeArray();
			$.each(addressFields, function(idx, fields){
				var fieldsName = fields.name;

				var $inputElement = _shippingDialog.find('input[name='+fieldsName+']');
				var $selectElement = _shippingDialog.find('select[name='+fieldsName+']')
				if($inputElement) $inputElement.val('');
				if($selectElement) {
					var fieldsValue = 0;
					if(fieldsName == 'dest_type') fieldsValue = -1;
					if(fieldsName == 'lift') fieldsValue = 0;
					if(fieldsName == 'country') fieldsValue = _this.const.HK_COUNTRY_CODE_ID;

					$selectElement.val(fieldsValue).trigger('change');
				}
			});

			$("#format_address_display").html('');
		}
	},

	handleBuildingInfoSelect() {
		var _this = this;
		var $liftSelect = $('select[name=lift]');
		$('select[name="dest_type"]').off('change').on('change', function(event) {
			event.preventDefault();
			var $this = $(this);
			$("#sel_lift_option_3").toggle($this.val() == 2 || $this.val() == 0);
			if( $liftSelect.val() == 3) $liftSelect.val(0);
		});
	},

	updateShippingCountryList: function(data) {
		var _this = this;
			_this.data.countries_code = data.available_countries_code;
		var $shippingCountrySelect = $('#shippingCountry');
			$shippingCountrySelect.empty();

		$.each(data.country_list, function(key, country){
			var countryOption = _this.render.selectOption(country.id, country.name, country.default);
			$shippingCountrySelect.append(countryOption);
		});
	},

	handleShippingCountryOnChange() {
		var _this = this;
		$('select[name="country"]').off('change').on('change', function(event) {
			event.preventDefault();
			var _value = parseInt($(this).val());
			var $zipcodeInput = $('input[name="zipcode"]');
				$zipcodeInput.val('');
				$zipcodeInput.prop("disabled", _value == _this.const.HK_COUNTRY_CODE_ID || _value == _this.const.MACAU_COUNTRY_CODE_ID);
			$('#select_address_type').toggle( _value == _this.const.HK_COUNTRY_CODE_ID);
			$('#id_doc').toggle( _value == _this.const.CHINA_COUNTRY_CODE_ID);
			_this.data.country = _value;
		});
	},

	changeShippingCountry(val) {
		$('select[name=country]').val(val).trigger('change');
	},

	showFullAddressView: function() {
		$(".edit_address").trigger('click');
	},

	// shipping method
	showExpressShippingMethod: function(data) {
		var _this = this;
		var $expressShippingMethodWrapper = $('.express-shipping-method-wrapper');
			$expressShippingMethodWrapper.show();
		var $expressShippingList = $expressShippingMethodWrapper.find('.express-shipping-list');
			$expressShippingList.empty();
		var result = data.data;

		var shippingList = result.shipping_list;
		if(shippingList.length > 0) {
			var renderExpressShippingList = _this.render.shippingList(shippingList);
			$expressShippingList.append(renderExpressShippingList);

			var $shippingMethodRb = $expressShippingList.find('input[name=shippingMethod]');
				$shippingMethodRb.on('change', function(e) {
					e.preventDefault();
					_this.data.shipping = $(this).val();
					_this.data.shipping_area_id = $(this).data('areaId');
					_this.calculateAmountApi();
				});


				$shippingMethodRb.first().trigger('click');
		} else {
			if(result.restricted_to_hk_goods) {
				_this.data.shipping = 0;
				var restrictedToHKGoods = result.restricted_to_hk_goods;
				var warning = _this.render.restrictedHKGoodsWarning(restrictedToHKGoods);

				$expressShippingList.append(warning);
			}
		}
		
	},

	showPickupPointDialog: function(data) {
		var _this = this;
		var _data = _this.data;
		var $pickupPointDialog = $('.pickup-point-dialog');
			$pickupPointDialog.show();

		// reset error message
		$pickupPointDialog.find('.error').each(function(i, errorElem) {
			$(errorElem).html('');
		});

		// Set input blur when press 'esc'
		$pickupPointDialog.off('keyup')
							.on('keyup', 'input', function(event) {
								event.preventDefault();
								if(event.which === _this.const.KEYCODE_ESC) $(this).blur();
							});
		
		// clear dialog content
		var $shippingList = $pickupPointDialog.find('.pickup-point-list');
			$shippingList.empty();
		var result = data.data;

		// render
		var shippingList = result.shipping_list;
		if(shippingList.length > 0) {
			var renderPickupPointShippingList = _this.render.shippingList(shippingList);
			$shippingList.append(renderPickupPointShippingList);

			// select pickup point
			var $shippingMethodRb = $shippingList.find('input[name=shippingMethod]');
				$shippingMethodRb.on('change', function(e) {
					e.preventDefault();

					// reset pickup point area and region
					_this.resetPickupPointAddressSelectOption();
					$('.loading').show();
					
					var requestData = {
						'shipping_id': $(this).val(),
						'shipping_area_id': $(this).data('areaId')
					}

					// get pickup point area address list
					_this.ajaxShippingTypeFlowRequest('shipping', requestData);
				});
		}

		// handle address info to dialog form
		fillLockerAddressInfo();

		// confirm pickup point data
		_this.confirmPickupPointDialog();
		

		// close dialog
		$pickupPointDialog.find('.dialog-close')
							.off('click').on('click', function(e) {
								$pickupPointDialog.hide();

								if(Object.keys(_data.currentAddress).length == 0) {
									_data.allotRouterCode = '';
								}
							});


		function fillLockerAddressInfo() {
			var address = _data.currentAddress;

			if(Object.keys(address).length > 0) {
				// auto fill in address to the input
				$.each(address, function(name,value){ 
					if(name == 'shippingMethod' || name == 'lockerCode') {
						var selectedRadioElement = $pickupPointDialog.find('input[name='+name+'][value="'+value+'"]').prop('checked', true);
						if(name == 'lockerCode') {
							selectedRadioElement.trigger('change');
						}
					} else {
						var $inputElement = $pickupPointDialog.find('input[name='+name+']');
						var $selectElement = $pickupPointDialog.find('select[name='+name+']');
						if($inputElement) $inputElement.val(value);
						if($selectElement) $selectElement.val(value).trigger('change');
					}
				});
			} else {
				// reset all input
				var addressFields = $pickupPointDialog.find( ":input" ).serializeArray();

				$.each(addressFields, function(idx, fields){
					var fieldsName = fields.name;

					var $inputElement = $pickupPointDialog.find('input[name='+fieldsName+']');
					var $selectElement = $pickupPointDialog.find('select[name='+fieldsName+']');
					if($inputElement) $inputElement.val('');
					if($selectElement) {
						$selectElement.val(0).trigger('change');
						$selectElement.prop('disabled', true);
					}
				});

				_this.resetPickupPointTable();
			}
		}
	},

	confirmPickupPointDialog: function() {
		var _this = this;
		var _data = _this.data;
		var $pickupPointDialog = $('.pickup-point-dialog');
		$pickupPointDialog.find('.confirm-button').off('click').on('click', function(event) {
			event.preventDefault();

			var lockerInfoFields = $pickupPointDialog.find( ":input" ).serializeArray();
			if(validateLockerInfo()) {
				var $selectedPickupPoint = $pickupPointDialog.find('input[name=lockerCode]:checked');
				_data.shipping = $selectedPickupPoint.data('shippingId');
				_data.shipping_area_id = $selectedPickupPoint.data('areaId');
				_this.calculateAmountApi();

				var params = parsePickupPointInfo(lockerInfoFields);
				_data.currentAddress = params;

				_this.showFormatedAddress($('.selected-pickup-point-address').text());
				$pickupPointDialog.find('.dialog-close').trigger('click');
			}

		});
		
		function parsePickupPointInfo(lockerInfoFields) {
			var params = {};
			$.each(lockerInfoFields, function(index, field) {
				params[field.name] = $.trim(field.value);
			});

			params['allotRouter'] = _this.data.allotRouter;
			params['allotRouterCode'] = _this.data.allotRouterCode;
			params['shipping'] = _this.data.shipping;
			params['shipping_area'] =  _this.data.shipping_area_id;

			return params;
		}

		function validateLockerInfo() {
			var isError = false;
			var country = _this.data.country;
			
			// pickup point
			var pickupPointShippingVal = $pickupPointDialog.find('input[name=shippingMethod]:checked').val();
			if(pickupPointShippingVal == undefined) {
				$('#pickupPointError').html('請選擇提貨點');
				isError = true;
			}

			// consigee name
			var name = $pickupPointDialog.find('.consignee-info').find('input[name=locker_consignee]').val();
			if (!YOHO.check.isValidName(name)) {
				$('#lockerNameError').html('請輸入正確的收貨人姓名');
				isError = true;
			} else {
				$('#lockerNameError').html('');
			}
	
			// consigee mobile number
			var $mobile = $pickupPointDialog.find('input[name="locker_tel"]');
			var mobile = $.trim($mobile.val()) || '';
			var $mobileError = $('#lockerConsigeeMobileError');
			if (!_this.checkPhoneNo(mobile, country)) {
				$mobileError.html('請輸入正確的電話號碼');
				isError = true;
			} else {
				$mobileError.html('')
			}

			var addressErrorMsg = '';
			if($('select[name=locker_area]').val() == 0 || $('select[name=locker_region]').val() == 0) {
				addressErrorMsg = '請選擇地區';
				isError = true;
			} else if($('input[name=lockerCode]:checked').val() == undefined) {
				addressErrorMsg = '請選擇提貨點地址';
				isError = true;
			} 
			$('#lockerAddressError').html(addressErrorMsg);

			return !isError;
		}
	},

	fillDefaultlocker: function(locker) {
		var _this = this;
		if(locker !== null && Object.keys(locker).length > 0 && locker.city && locker.district && locker.edCode) {
			$("select[name='locker_area']").val(locker.city).trigger('change');
			$("select[name='locker_region']").val(locker.district).trigger('change');
			if(!$("input[name=lockerCode][value='" + locker.edCode + "']").is(':disabled')) {
				$("input[name=lockerCode][value='" + locker.edCode + "']").attr('checked', 'checked').trigger('change');
				$('#selected_locker').html(locker.twThrowaddress);
				$('input[name="allotRouter"]').val(locker.allotRouter);
				$('input[name="locker_consignee"]').val(locker.consignee);
				$('input[name="locker_tel"]').val(locker.tel.substring(locker.tel.indexOf('-') + 1));
			}
			if(locker.arCode) {
				$('select[name=ar_select]').val(locker.arCode);
				_this.data.allotRouterCode = locker.arCode;
			}
		}
	},

	handlePickupPointArea: function(data) {
		var _this = this;
		var _data = _this.data;
		var $selectPickupPointArea = $('select[name=locker_area]');
			$selectPickupPointArea.prop('disabled', false);

		// store pickup point address list
		_data.pickupPointList = data.list;
		// render area list 
		var areaListSelectOption = '';
		$.each(data.list, function(area, object){
			areaListSelectOption += _this.render.selectOption(area, area);
		});
		$selectPickupPointArea.append(areaListSelectOption);


		$selectPickupPointArea.on('change', function(event) {
			event.preventDefault();
			var $this = $(this);
			var areaVal = $this.val();

			_this.handlePickupPointRegion(areaVal);
		});
	},

	handlePickupPointRegion: function(pickupPointArea) {
		var _this = this;
		var _data = _this.data;
		// reset pickup point region select option
		_this.resetPickupPointAddressSelectOption('region');

		if(pickupPointArea != 0) {
			var regionList = _data.pickupPointList[pickupPointArea];
			_data.currentRegionList = regionList;
		
			// render region list 
			var regionListSelectOption = '';
			$.each(regionList, function(region, object){
				regionListSelectOption += _this.render.selectOption(region, region);
			});
			var $selectPickupPointRegion = $('select[name=locker_region]');
				$selectPickupPointRegion.append(regionListSelectOption);
				$selectPickupPointRegion.prop('disabled', false);

			// handle pickup point region select option on change
			$selectPickupPointRegion.on('change', function(event){
				event.preventDefault();
				var $this = $(this);
				_this.resetPickupPointTable();

				var pickupPointRegion = $this.val();
				if(pickupPointRegion == 0) return;

				var addressList =  _data.currentRegionList[pickupPointRegion];
				_this.handlePickupPointAddressList(addressList);
			});
		}
	},

	handlePickupPointAddressList: function(addressList) {
		var _this = this;
		var _data = _this.data;
		var $table = $('.pickup-point-address-list-table');
		var $tableBody = $table.find('tbody');

		// render pickup point address list
		var tRow = '';
		$.each(addressList, function(index, list) {
			$.each(list, function(code, addressInfo) {
				tRow += _this.render.pickupPointAddressListTableRow(addressInfo);
			});
		})
		$tableBody.append(tRow);
		$('.pickup-point-address-info').show();


		$('input[name=lockerCode]').on('change', function(event) {
			event.preventDefault();
			$('.selected-pickup-point-address').html('');

			_data.allotRouter = $(this).data('ar');
			_data.allotRouterCode = '';

			$('.pickup-point-timeslot').hide();
			$(this).closest('tr').find('.pickup-point-timeslot').show();


			var selectedPickupPointArea = $('select[name=locker_area]').val();
			var selectedPickupPointRegion = $('select[name=locker_region]').val();
			
			var currentAddressList = _data.pickupPointList[selectedPickupPointArea][selectedPickupPointRegion][_data.allotRouter];

			// show second pickup point dialog
			if($(this).data('hot') == 1 && $(this).val() != _data.currentAddress.lockerCode) {
				_this.showSecondPickupPointDialog(currentAddressList);
			}
			
			var lockerCode = $(this).val();
			var currentAddressText = currentAddressList[lockerCode]['twThrowaddress'];
			$('.selected-pickup-point-address').html(currentAddressText);
		});
	},

	showSecondPickupPointDialog: function(addressList) {
		var _this = this;
		var $secondPickupPointDialog = $('.second-pickup-point-dialog');
			$secondPickupPointDialog.show();
		var $secondPickupPointSelectOption = $('select[name=ar_select]');
			$secondPickupPointSelectOption.empty();

		secondPickupPointSelectOptions = '';
		$.each(addressList, function(code, addressInfo) {
			// addressInfo.isHot = 0; addressInfo.isBlackList = 0;
			if(addressInfo.isHot == 0 && addressInfo.isBlackList == 0) {
				secondPickupPointSelectOptions += _this.render.selectOption(addressInfo.edCode, addressInfo.twThrowaddress);
			}
		});	
		$secondPickupPointSelectOption.append(secondPickupPointSelectOptions);

		// confirm the second pickup point selected
		$secondPickupPointDialog.find('.confirm-button')
								.off('click').on('click', function() {
									_this.data.allotRouterCode = $secondPickupPointSelectOption.val();
									$secondPickupPointDialog.hide();
								});


		// close second pickup point dialog
		$secondPickupPointDialog.find('.dialog-close')
								.off('click').on('click', function(event) {
									event.preventDefault();

									$('.pickup-point-timeslot').hide();
									$('input[name=lockerCode]').prop('checked', false);
									$secondPickupPointDialog.hide();
								})
	},

	resetPickupPointTable: function() {
		$('.pickup-point-address-info').hide();
		var $table = $('.pickup-point-address-list-table');
		var $tableBody = $table.find('tbody');
			$tableBody.empty();
		$('.selected-pickup-point-address').html('');
	},

	resetPickupPointAddressSelectOption: function(selectType) {
		selectType = selectType || null; // null is reset all

		var _this = this;
		var firstSelectOption = _this.render.selectOption(0, '-- 請選擇 --');

		if(selectType === 'area' || selectType == null) {
			$selectPickupPointArea = $('select[name=locker_area]');
			$selectPickupPointArea.prop('disabled', true);
			$selectPickupPointArea.empty().append(firstSelectOption);
		}
		
		if(selectType === 'region' || selectType == null) {
			$selectPickupPointRegion = $('select[name=locker_region]');
			$selectPickupPointRegion.prop('disabled', true);
			$selectPickupPointRegion.empty().append(firstSelectOption);
		}
		_this.resetPickupPointTable();
	},

	// reserve
	handleReserve: function() {
		var _this = this;
		$('input[name="isReserve"]').on('change', function(event) {
			event.preventDefault();
			var $redeembBtn = $('#payment99').closest('.tab-button');
			if($(this).val() == 0) {
				_this.data.force_reserve_mode = false;
				$redeembBtn.show();
			} else {
				$redeembBtn.hide();
			}
		}); 
	},

	// 速遞和提貨點只能是訂貨
	isReserveOnly: function(isReserveOnly) {
		var _this = this;
		isReserveOnly = isReserveOnly || false;

		if(isReserveOnly) {
			$('input[name="isReserve"][value=1]').trigger('click');
			$('input[name="isReserve"][value=0]').prop('disabled', true);
		} else {
			$('input[name="isReserve"][value=0]').prop('disabled', false).trigger('click');
		}
	},

	// sales info
	assignSalesperson: function() {
		var $salesperson = $('select[name="salesperson"]');
		
		var previousSalesperson = YOHO.common.cookie('POS[salesperson]');
		if (previousSalesperson)
		{
			$salesperson.find('option').each(function () {
				var option = $(this);
				if (option.val() == previousSalesperson)
				{
					$salesperson.val(previousSalesperson);
				}
			});
		}
		$salesperson.change(function (event) {
			YOHO.common.cookie('POS[salesperson]', $salesperson.val());
		});
	},

	changeCash: function() {
		var _this = this;
		$('.cash_received').on('input', function(event) {
			event.preventDefault();
			_this.calculateCashChange();
		});

		$('.cash_received').on('keyup', function(event) {
			event.preventDefault();
			if(event.which === _this.const.KEYCODE_ESC || event.which === _this.const.KEYCODE_ENTER) {
				$(this).blur();
			}
		});
	},

	calculateCashChange: function() {
		if ($('.cash_received').val() == '') {
			$('.cash-change').text(0);
		} else {
			var amount = parseFloat($('.total-amount').text()) || 0;
			var received = parseFloat($('.cash_received').val()) || 0;
			var change = (received - amount).toFixed(2);
			$('.cash-change').text(change);
		}
	},

	// search order
	readOrderNumber: function() {
		var _this = this;
		$('.search_order_sn').on('keyup', function(event) {
			event.preventDefault();
			if(event.which === _this.const.KEYCODE_ENTER) {
				$(this).blur();
				_this.getOrderBySnApi( $(this).val() );
			}

			if(event.which === _this.const.KEYCODE_ESC) {
				$(this).blur();
			}
		});
	},

	loadOrder: function(data) {
		var _this = this;
		var _data = _this.data;

		if (_data.mode != 'repair') {
			$('#replaceTable').removeData('orderId').hide();
			$('#replaceTable').find('tbody').empty();
		} else {
			$('#repairTable').removeData('orderId').removeData('orderSn').hide();
			$('#repairTable').find('tbody').empty();
		}

		_this.handleOrderSummaryTable();

		var $error = $('.search-order .error');
		if(data.status == 1) {
			$error.html('');

			if ((data.order.shipping_status != 1) && // SS_SHIPPED
				(data.order.shipping_status != 2) && // SS_RECEIVED
				(data.order.shipping_status != 4))   // SS_SHIPPED_PART
			{
				$error.html('此訂單並未出貨');
			}
			else {
				if(data.order) {
					_this.handleOrderSummaryTable(_this.render.orderSummaryTable(data.order), true);
				}

				if (_data.mode != 'repair') {
					_this.loadReplaceOrder(data);
				} else {
					_this.loadRepairOrder(data);
				}

				// If old order have assoicated user, populate it
				if(data.user_name) {
					_this.validateAccountApi(data.user_name);
				}
			}
		} else {
			$error.html(data.error);

			$('.search_order_sn').focus();
		}
	},

	loadReplaceOrder: function(data) {
		var _this = this;
		var _data = _this.data;

		var $replaceTable = $('#replaceTable');

		// Add order sn to postscript
		var $postscript = $('textarea[name="postscript"]');
		var postscriptArray = $postscript.val().split("\n");
		var orderPostScript = '換貨，舊單編號: ' + data.order.order_sn;
		if(postscriptArray[0].indexOf('換貨，舊單編號') == -1) {
			postscriptArray.unshift(orderPostScript);
		} else {
			postscriptArray[0] = orderPostScript;
		}
		var newPostscript = postscriptArray.join('\n');
		$('textarea[name="postscript"]').val(newPostscript).trigger('keyup');
		$('.remarks-dialog .confirm-button').trigger('click');

		// render order goods list
		if(data.order_goods.length > 0) {
			$replaceTable.show();
			$replaceTable.data('orderId', data.order_id);

			var replaceGoodsRows = _this.render.replaceGoodsRows(data.order_goods, _data.warehouseList);
			$replaceTable.find('tbody').append(replaceGoodsRows);

			if(_data.goodsList.length == 0) {
				$('.search_goods_sn').focus();
			} else {
				if(data.order_goods.length == 1) {
					$('.replace_qty').focus().select();
				}else {
					$('.cash_received').focus();
				}
			}

			// handle the quantity of goods
			$('.replace_qty').on('keyup', function(event) {
				if(event.which === _this.const.KEYCODE_ENTER || event.which === _this.const.KEYCODE_ESC) {
					event.preventDefault();
					$(this).trigger('blur');
				}
			}).on('blur', function(event) {
				event.preventDefault();

				// Auto apply discount for replaced goods
				var isMaxQtyError = false;
				var replaced_total = 0;
				$('.replace_qty').each(function () {
					var $input = $(this);
					var maxQty = 1;
					var qty = parseInt($input.val()) || 0;
					if(qty > maxQty) {
						isMaxQtyError = true;
						qty = maxQty;
					} 
					$input.val(qty);
					var price = parseFloat($input.data('goodsPrice'));
					replaced_total += qty * price;
				});
				var discount =  (replaced_total > 0) ? replaced_total * -1 : '';
				$('input[name="discount"]').val(discount).trigger('blur');

				if(isMaxQtyError) {
					YOHO.dialog.warn('訂單每件貨品只能換貨一次');
				}
			});
		}
	},

	loadRepairOrder: function(data) {
		var _this = this;
		var orderGoods = data.order_goods;
		var $repairTable = $('#repairTable');

		// Save order_id as the table's data
		$repairTable.data('orderId', data.order_id);
		$repairTable.data('orderSn', data.order.order_sn);

		// render order goods list
		if(orderGoods.length > 0) {
			$repairTable.show();
			
			_this.storeRepairGoods(orderGoods);

			var repairGoodsRows = _this.render.repairGoodsRows(orderGoods);
			$repairTable.find('tbody').append(repairGoodsRows);

			if(orderGoods.length == 1) {
				$('input[name=repair_goods]').prop('checked', true);
			}
		}
		
		// If old order have consignee name, use it as contact name
		if (data.order.consignee) {
			$('input[name="contact_name"]').val(data.order.consignee);
		}

		if(data.user_name) {
			$('input[name=repair_fee_amount]').focus().select();
		} else {
			$('input[name="mobile"]').focus();
		}
	},

	loadRepairGoods: function(data) {
		var _this = this;
		// If search by goods, remove cached order ID
		var $repairTable = $('#repairTable');
			$repairTable.removeData('orderId').removeData('orderSn').hide();
			$repairTable.find('tbody').empty();

		var $error = $('.search .error');
		if(data.status == 1) {
			$error.html('');
			
			var goodsList = data.goods_list;
			if (goodsList.length > 0) {
				$repairTable.show();

				_this.storeRepairGoods(goodsList);

				var repairGoodsRows = _this.render.repairGoodsRows(goodsList);
				$repairTable.find('tbody').append(repairGoodsRows);

				if(goodsList.length == 1) {
					$('input[name=repair_goods]').prop('checked', true);
				}
			}

			$('input[name=mobile]').focus();
		} else {
			$error.html(data.error);

			$('.search_goods_sn').focus();
		}


	},

	storeRepairGoods: function(repairGoods) {
		var repairGoodsList = {};
		$.each(repairGoods, function(goodsIndex, goods) {
			repairGoodsList[goods.goods_id] = {
				goods_name: goods.goods_name,
				goods_number: goods.goods_sn
			}
		});
		this.data.repairGoodsList = repairGoodsList;
	},

	handleOrderSummaryTable: function(orderSummaryTable, isShow) {
		orderSummaryTable = orderSummaryTable || '';
		isShow = isShow || false;

		var $orderSummary = $('.order-summary');
			$orderSummary.html(orderSummaryTable);
			$orderSummary.toggle(isShow);
	},

	// preview order
	confirmOrder: function() {
		var _this = this;
		var _data = _this.data;
		var $previewOrderDialog = $('.preview-order-dialog');

		$('.confirm-order-button').on('click', function(event) {
			event.preventDefault();
			
			setTimeout(function () {
				// validation
				var errorMsg = validateOrder();
				if(errorMsg != '') {
					YOHO.dialog.warn(errorMsg);
				} else {
					var isReserve = $('input[name="isReserve"]:checked').val();
					if (isReserve == 1)
					{
						if(_data.shippingType == _this.const.STORE_SHIPPING_TYPE) {
							if (!_data.have_preorder_goods && !_data.force_reserve_mode)
							{
								var dialog = YOHO.dialog.creat({
									id:'confirm',
									content:'<div class="warn-tip"><i class="iconfont">&#227;</i>你選擇了訂貨模式，但訂單中沒有需要訂貨的產品。</div>'
								});
								dialog.button({
									name: '改為「正常銷售」',
									focus: true,
									callback: function () {
										$('input[name="isReserve"][value=0]').trigger('click');
										setTimeout(function () {
											$(this).trigger('click');
										}, 100);
									}
								},{
									name: '我確定這是「訂貨」單',
									callback: function () {
										_data.force_reserve_mode = true;
										setTimeout(function () {
											$(this).trigger('click');
										}, 100);
									}
								});
								
								return;
							}
						}
					}
					$previewOrderDialog.find('.preview-content').empty();

					// parse preview order info
					var previewOrderInfo = parsePreviewOrder();
					var previewHtml = _this.render.previewOrder(previewOrderInfo);
					$previewOrderDialog.find('.preview-content').append(previewHtml);
					$previewOrderDialog.show();
				}
			}, 200);
		});

		function validateOrder() {
			// if(!YOHO.config.isDebug) {
			// 	if($('#mobile_hint').text() != '') {
			// 		return $('#mobile_hint').text();
			// 	}
	
			// 	if($('#email_hint').text() != '') {
			// 		return $('#email_hint').text();
			// 	}
			// }

			if ((!$.isArray(_data.goodsList)) || (_data.goodsList.length === 0)) {
				return '您的訂單中沒有任何產品！';
			}
	
			if($('input[name="need_removal"]').length > 0 && !$('input[name="need_removal"]:checked').val() && $('.need_removal').is(':visible') )
			{
				return '請查詢客人是否需要WEEE回收服務！';
			}
	
			if($('input[name="need_removal"]').length > 0 && $('input[name="need_removal"]:checked').val() == 1)
			{
				if(!$('input[name=nr_consignee]').val() || !$('input[name=nr_address]').val() || !$('input[name=nr_phone]').val()) {
					return '請填寫WEEE回收資料！';
				}
			}
	
			if($('input[name="need_requiregoods"]').length > 0 && !$('input[name="need_requiregoods"]:checked').val() && $('.need_requiregoods').is(':visible') )
			{
				return '請查詢客人是否需要變壓器';
			}

			// address
			if(
				(_data.shippingType == _this.const.EXPRESS_SHIPPING_TYPE || _data.shippingType == _this.const.PICKUP_POINT_SHIPPING_TYPE) && 
				Object.keys(_data.currentAddress).length == 0
			) {
				return '請查詢客人收貨地址';
			}

			if(_data.shipping == 0 && _data.shippingType == _this.const.EXPRESS_SHIPPING_TYPE) {
				return '訂單中某些產品只能送貨到香港';
			}
	
			// Prmotion only
			var promote_error = "";
			if ($('.promotion-wrapper').children('div').length > 0) {
				$('.promotion-wrapper').children('div').get().map(function(promo) {
					$(promo).find("input").get().map(function(input) {
						if ($(input).prop("required") && $(input).val() == "") {
							promote_error = '請填寫' + $(promo).find('.promotion-title').text();
						} else if ($(input).prop("pattern") != "") {
							var regex = new RegExp($(input).prop("pattern"));
							if (!regex.test($(input).val()))  {
								promote_error = $(promo).find('.promotion-hints').text();
							}
						}
					})
				})
			}
			if (promote_error != '') {
				return promote_error;
			}
	
			// check sales agent
			if( _data.payment == _this.const.REDEEM_PAYMENT && ( _data.saOrder == '' ||  _data.saId == 0) ){
				return (_data.saError != null) ? _data.saError : '請選擇代理及填寫代理訂單號碼！';
			}
	
			// check sales person
			if($('select[name=salesperson]').val() == 0) {
				return '請選擇銷售人員！';
			}


			if(_data.mode == 'replace') {
				var replacedGoodsNumber = 0;

				$('#replaceTable .replace_qty').each(function() {
					var $input = $(this);
					var qty = parseInt($input.val()) || 0;
					
					if (qty > 0) {
						replacedGoodsNumber++;
					}
				});

				if(replacedGoodsNumber == 0) {
					return '請選擇需要換貨的數量！';
				}
			}

			return '';
		}

		function parsePreviewOrder() {
			var previewOrderInfo = {
				goodsInfo: {},
				paymentInfo: {},
				memberInfo: {},
				salesPersonInfo: {},
			};

			// goods list
			var goodsList = $.map(_data.goodsList, function(goods) {
				return goods.goods_name + ' x' + goods.goods_number;
			});
			previewOrderInfo.goodsInfo.goodsList = goodsList;

			// remarks
			if(_data.serialNumber != '') 		previewOrderInfo.goodsInfo.serialNumber 	= _data.serialNumber;
			if(_data.postscript != '')  		previewOrderInfo.goodsInfo.postscript 		= _data.postscript;
			if(_data.internalRemarks != '')  	previewOrderInfo.goodsInfo.internalRemarks 	= _data.internalRemarks;
			if(_data.orderRemarks != '') 		previewOrderInfo.goodsInfo.orderRemarks 	= _data.orderRemarks;

			// shipping
			var shippingElemName = (_data.shipping == 2) ? 'shippingType' : 'shippingMethod';
			var shippingName = $('input[name="' + shippingElemName + '"]:checked').next('label').find('span').text();
			previewOrderInfo.paymentInfo.shipping = shippingName;

			// payment
			var paymentName = $('input[name="paymentMethod"]:checked').next('label').text();
			previewOrderInfo.paymentInfo.payment = paymentName;

			// member status, mobile number and interal
			var memberMobileVal = $('input[name=mobile]').val();
			var isMember = (_data.user_id != 0 || memberMobileVal != '');
			var memberName = (isMember) ? '已有' + memberMobileVal : '新';
			previewOrderInfo.paymentInfo.member = memberName;
			if(isMember && _data.integral > 0) previewOrderInfo.paymentInfo.integral = _data.integral;

			// discount and coupon
			previewOrderInfo.paymentInfo.discount = (_data.discount == 1) ? '沒有折扣' : _data.discount;
			if(_data.coupon_codes.length > 0) previewOrderInfo.paymentInfo.coupon = _data.coupon_codes.join(',')

			// member info - mobile number and email
			var mobileNumber = (memberMobileVal) ? _this.getMobileNumberWithPrefix($('input[name=mobile]')) : '';
			previewOrderInfo.memberInfo.mobile = mobileNumber;
			previewOrderInfo.memberInfo.email = $('input[name=email]').val();

			// sales person
			previewOrderInfo.salesPersonInfo.salesperson = $('select[name="salesperson"]').val();

			return previewOrderInfo;
		}

		$previewOrderDialog.find('.dialog-close').on('click', function(event) {
			$previewOrderDialog.hide();
		});

		$previewOrderDialog.find('.confirm-button').on('click', function(event) {
			event.preventDefault();
			_this.submitOrder();
			$previewOrderDialog.hide();
		});
	},

	submitOrder: function() {
		var _this = this;
		var _data = _this.data;
		var submitData = {};

		submitData['step'] = 'done';

		var isReserve = $('input[name="isReserve"]:checked').val();
		submitData['mode'] = (_data.mode == 'normal' && isReserve == 1) ? 'reserve' : _data.mode;

		$.each(_data.goodsList, function(idx, goods) {
			submitData['goods_number['+goods.goods_id+']'] = goods.goods_number;
		});

		if(_data.mode == 'normal') {
			submitData['sa_id'] = _data.saId;
			submitData['sa_sn'] = _data.saOrder;
		}
		if(_data.mode == 'replace') {
			submitData['replace_order_id'] = $('#replaceTable').data('orderId');

			$('#replaceTable .replace_qty').each(function() {
				var $input = $(this);
				var goodsId = $input.data('goodsId');
				var qty = parseInt($input.val()) || 0;
				var warehousingId = $('input[name=replace_warehousing_' +goodsId+']:checked').val();
				
				if (qty > 0)
				{
					submitData['replace[' + goodsId + ']'] = warehousingId;
					submitData['replace_qty[' + goodsId + ']'] = qty;
				}
			});
		}

		var mobileWithPrefix = ($('input[name="mobile"]').val()) ? _this.getMobileNumberWithPrefix($('input[name=mobile]')) : '';
		var postscript = _data.postscript + (($('input[name="need_requiregoods"]:checked').val() == 1) ? ' 不需要變壓器' : '');

		submitData['serial_numbers'] = 	_data.serialNumber;
		submitData['postscript'] = 		$.trim(postscript);
		submitData['order_remarks'] =	_data.orderRemarks;
		submitData['internal_remarks'] =_data.internalRemarks;
		submitData['shipping_type'] = 	_data.shippingType;
		submitData['shipping'] = 		_data.shipping;
		submitData['payment'] = 		_data.payment;
		submitData['user_id'] = 		$('input[name="mobile"]').val();
		submitData['user_name'] = 		mobileWithPrefix;
		submitData['mobile'] = 			mobileWithPrefix;
		submitData['email'] = 			$('input[name="email"]').val();
		submitData['integral'] = 		_data.integral;
		submitData['vardiscount'] = 	_data.discount;
		submitData['coupon'] = 			_data.coupon_codes.join(',');
		submitData['salesperson'] = 	$('select[name="salesperson"]').val();

		submitData['province'] = 		0;
		submitData['city'] = 			0;
		submitData['district'] = 		0;

		// address
		if (_data.shippingType == _this.const.EXPRESS_SHIPPING_TYPE) {
			if(_data.currentAddress.id == 0) {
				// new address for new customer
				$.each(_data.currentAddress, function(key, value) {
					if(key == 'id') key = 'address_id';
					if(key == 'format_address') key = 'address';
					submitData['new_address['+ key +']'] = value;
				});
			}
		} else if (_data.shippingType == _this.const.PICKUP_POINT_SHIPPING_TYPE) {
			$.each(_data.currentAddress, function(key, value) {
				submitData['locker_info['+ key +']'] = value;
			});
		}


		// WEEE form
		if($('input[name="need_removal"]:checked').val() == 1 && $('.need_removal').is(':visible') ) {
			submitData['need_removal'] = $('input[name=need_removal]').val();
			submitData['nr_consignee'] = $('input[name=nr_consignee]').val();
			submitData['nr_address'] = $('input[name=nr_address]').val();
			submitData['nr_phone'] = $('input[name=nr_phone]').val();

			sel = $('input[name^="nr_cat"]');
			sel.each(function() {
				submitData[$(this).attr('name')] = $(this).val();
			});
		}

		if ($('.promotion-wrapper').children('div').length > 0) {
			$('.promotion-wrapper').children('div').get().map(function(promo) {
				$(promo).find("input").get().map(function(input) {
					submitData[$(input).prop("name")] = $(input).val();
				})
			})
		}

		var $form = $('<form action="/pos_quick_buy.php" method="post"></form>');
		$.each(submitData, function (dataKey, dataValue) {
			var $input = $('<input type="hidden">');
			$input.prop('name', dataKey);
			$input.val(dataValue);
			$form.append($input);
		});
		
		$form.appendTo('body');
		$form.submit();
	},


	// repair
	blurContactFormInput: function() {
		var _this = this;
		$('.member-info-form').on('keyup', 'input', function(event) {
			event.preventDefault();
			if(event.which === _this.const.KEYCODE_ESC) $(this).blur();
		});
	},

	changeRepairSearching: function() {
		var _this = this;
		$('input[name=repair-status]').on('change', function(event) {
			event.preventDefault();

			var currentRepairSearchStatus = $(this).val();
			$('.search-order-section').toggle(currentRepairSearchStatus == 'order');
			$('.search-goods-section').toggle(currentRepairSearchStatus == 'barcode');

			$('#repairTable').find('body').empty();
			$('#repairTable').hide();

			_this.handleOrderSummaryTable();

			$('.search-order .error, .search .error').html('');
			$('.search_order_sn, .search_goods_sn').val('');
		});
	},

	handleRepairFee: function() {
		var _this = this;
		var _data = _this.data;
		var $repairFeeAmount =  $('.repair-fee-amount');
		var defaultFee = 300;
		// default repair fee
		showRepairAmount(defaultFee);

		$('input[name="repair-fee"]').on('change', function(event) {
			event.preventDefault();
			var $this = $(event.target);
			if($this.val() == 1) {
				$repairFeeAmount.show();
				$repairFeeAmount.val(defaultFee);

				showRepairAmount(defaultFee);
			} else {
				$repairFeeAmount.hide();
				showRepairAmount(0);
			}
		});

		$repairFeeAmount.on('keyup', function(event) {
			event.preventDefault();
			if(event.which === _this.const.KEYCODE_ESC) $(this).blur();
		});

		$repairFeeAmount.on('input',function(event) {
			event.preventDefault();
			var amount =  ($(this).val() != '') ? $(this).val() : 0;
			showRepairAmount(amount);
		});

		function showRepairAmount(fee) {
			var $repairTotalAmount = $('.repair .total-amount');
			var amount = parseFloat(fee).toFixed(2) || 0;
				amountFormatted = (amount == 0) ? '0.00' : amount;
			$repairTotalAmount.text(amountFormatted);

			_data.repairFee = amount;
			_this.calculateCashChange();
		}
	},

	showRepairRemarksDialog: function() {
		var _this = this;
		var $remarksDialog = $('.repair-remarks-dialog');
		$('.remarks-button, .remarks-edit-button').on('click', function(event) {
			event.preventDefault();

			$('textarea[name=repair-remarks-textare]').val(_this.data.repairRemark);

			$remarksDialog.show();
		});

		$('textarea').on('keyup', function(event) {
			event.preventDefault();
			if(event.which === _this.const.KEYCODE_ESC) $(this).blur();
		})

		// close dialog
		$remarksDialog.find('.dialog-close').on('click', function(event) {
			event.preventDefault();
			$remarksDialog.hide();
		});

		// confirm remarks info
		$remarksDialog.find('.confirm-button').on('click', function(event) {
			event.preventDefault();

			var $textareaRepairRemarks = $('textarea[name=repair-remarks-textare]');
			var repairRemarksVal = $.trim( $textareaRepairRemarks.val() );
			var $repairRemark = $('.repair-remarks');


			// display remarks to remark info
			$repairRemark.toggle(repairRemarksVal !== '');
			$repairRemark.find('.content').html(repairRemarksVal);
			_this.data.repairRemark = repairRemarksVal;

			var $remarksContent = $('.remarks-content');
				$remarksContent.find('.remarks-button').toggle(repairRemarksVal == '');
				$remarksContent.find('.remarks-info').toggle(repairRemarksVal !== '');

			$remarksDialog.hide();
		});

		$remarksDialog.on('keyup', function(event) {
			event.preventDefault();
			if(event.which === _this.const.KEYCODE_ESC) $(this).blur();
		});
	},

	confirmRepair: function() {
		var _this = this;
		var _data = _this.data;
		var $previewOrderDialog = $('.preview-order-dialog');

		$('.confirm-order-button').on('click', function(event) {
			event.preventDefault();

			setTimeout(function () {
				// validation
				var errorMsg = validateRepair();
				if(errorMsg != '') {
					YOHO.dialog.warn(errorMsg);
				} else {
					
					
					$previewOrderDialog.find('.preview-content').empty();

					// parse preview repair info
					var previewRepairInfo = parsePreviewRepair();

					var previewHtml = _this.render.previewOrder(previewRepairInfo);
					$previewOrderDialog.find('.preview-content').append(previewHtml);
					$previewOrderDialog.show();
				}
			}, 200);
		});

		function validateRepair() {
			// if(!YOHO.config.isDebug) {
			// 	if($('#mobile_hint').text() != '') {
			// 		return $('#mobile_hint').text();
			// 	}
	
			// 	if($('#email_hint').text() != '') {
			// 		return $('#email_hint').text();
			// 	}
			// }

			if (!$('input[name="repair_goods"]:checked').val()) {
				return '您沒有選擇要維修的產品！';
			}

			if (!$('input[name="mobile"]').val() && !$('input[name="email"]').val()) {
				return '至少要填個聯絡電話吧！';
			}

			// check sales person
			if($('select[name=salesperson]').val() == 0) {
				return '請選擇銷售人員！';
			}
			return '';
		}

		function parsePreviewRepair() {
			var previewOrderInfo = {
				goodsInfo: {},
				paymentInfo: {},
				contactInfo: {},
				salesPersonInfo: {},
			};

			// order number
			if($('#repairTable').data('orderId')) {
				previewOrderInfo.goodsInfo.orderNumber =  $('#repairTable').data('orderSn');
			}

			// repair goods
			var selectedRepairGoodsId = $('input[name="repair_goods"]:checked').val();
			previewOrderInfo.goodsInfo.repairGoods = _data.repairGoodsList[selectedRepairGoodsId].goods_name;

			// remarks
			if(_data.repairRemark != '') previewOrderInfo.goodsInfo.repairRemark = _data.repairRemark;

			// repair fee
			var repairFeeText = $('input[name=repair-fee]:checked').next().text();
			previewOrderInfo.paymentInfo.repairFee = ($('input[name=repair-fee]:checked').val() == 1) ? _data.repairFee : repairFeeText;

			// contact
			var memberMobileVal = $('input[name=mobile]').val();
			var mobileNumber = (memberMobileVal) ? _this.getMobileNumberWithPrefix($('input[name=mobile]')) : '';
			previewOrderInfo.contactInfo.contactNumber = mobileNumber;
			previewOrderInfo.contactInfo.email = $('input[name=email]').val();
			previewOrderInfo.contactInfo.contactName = $('input[name=contact_name]').val();

			// sales person
			previewOrderInfo.salesPersonInfo.salesperson = $('select[name="salesperson"]').val();

			return previewOrderInfo;
		}

		$previewOrderDialog.find('.dialog-close').on('click', function(event) {
			$previewOrderDialog.hide();
		});

		$previewOrderDialog.find('.confirm-button').on('click', function(event) {
			event.preventDefault();
			_this.submitRepair();
			$previewOrderDialog.hide();
		});
	},

	submitRepair: function() {
		var _this = this;
		var _data = _this.data;

		var mobile_with_prefix = '+' + $('input[name="mobile"]').intlTelInput('getSelectedCountryData').dialCode + '-' + $('input[name="mobile"]').val();
		
		var submitData = {
			act: 			'confirm_repair',
			order_id: 		$('#repairTable').data('orderId'),
			goods_id: 		$('input[name="repair_goods"]:checked').val(),
			repair_fee: 	_data.repairFee,
			contact_name: 	$('input[name=contact_name]').val(),
			mobile:			mobile_with_prefix,
			email:			$('input[name=email]').val(),
			remark:			_data.repairRemark,
			operator:		$('select[name="salesperson"]').val(),
		};

		var $form = $('<form action="/pos_repair_new.php" method="post"></form>');
		$.each(submitData, function (dataKey, dataValue) {
			var $input = $('<input type="hidden">');
			$input.prop('name', dataKey);
			$input.val(dataValue);
			$form.append($input);
		});
		
		$form.appendTo('body');
		$form.submit();
	},

	// call ajax
	// validate account
	getInitDataApi: function() {
		var _this = this;
		this.ajaxPOSPostRequest(
			{
				'act':'get_init_data',
			},
			function(data) {
				_this.data.warehouseList = data.warehouse_list;
				_this.updateShippingCountryList(data);
			}
		)
	},
	
	validateAccountApi: function(userName) {
		var _this = this;
		var _data = _this.data;
		this.ajaxPOSPostRequest(
			{
				'act':'validate_account',
				'user_name': userName
			},
			function(data) {
				if(_this.data.mode != 'repair')
				{
					_this.data.user_name = userName;
					_this.data.integral = 0;
					$('input[name="integral"]').val('');
					if(data.status == 1) // is member
					{
						$('.avatar-img').attr('src', data.avatar);
						// if is member, change the mode to member mode
						$('input[name=isMember][value=1]').trigger('click');
						$('.mobile-input').removeClass('has-error');

						// show integral
						_this.showIntegral(true);
						var userRankName = (data.rank_name) ? data.rank_name : '';
						$('.user-rank-name').html(userRankName);
						$('span.pay_points').html(data.pay_points);

						_this.handlePayPoints();

						// reset discount, coupon, integral, sales agent
						resetDiscountCouponIntegralAndSA();

						_this.data.user_id = data.user_id;
						_this.data.user_rank = data.user_rank;
						_this.addToCartApi(0, 0);

						_this.data.currentAddress = {};
						_this.showFormatedAddress('');
					}
					else // is not member
					{
						$('.avatar-img').attr('src', _this.const.AVATARS);

						var $checkedIsMemeberTab = $('input[name=isMember]:checked');
						if($checkedIsMemeberTab.val() == 1) 
						{
							$('.mobile-input').addClass('has-error');
							$('#mobile_hint').html( data.error );
						}

						_this.data.user_id = 0;
						_this.data.user_rank = 0;

						// reset discount, coupon, integral, sales agent
						resetDiscountCouponIntegralAndSA();
						
						// reset goods price is not vip price
						_this.addToCartApi(0, 0);

						_this.showIntegral(false);
					}
				}

				// auto fill member user name and email to member form
				if(data.status == 1) {
					_this.handleMemberInfoAutoFill(data.user_name, data.email);
				}
			}
		)

		function resetDiscountCouponIntegralAndSA() {
			if(_data.coupon_codes.length > 0) {
				var $couponCodesTag = $('.coupon-codes-tag');
					$couponCodesTag.empty();
				// $('input[name=coupon]').val(_data.coupon_codes.join(','));
				_data.coupon_codes = [];
				$('.promotion-wrapper').empty();
			}

			if(_data.discount < 1) {
				$('.discount-tag').empty();

				$('input[name=discount]').prop('disabled', false);
				// $('input[name=discount]').val(_data.discount);

				$('.discount-btn').prop('disabled', false);

				_data.discount = 1;
			}

			if(_data.integral > 0) {
				$('.integral-tag').empty();

				$('input[name=integral]').prop('disabled', false);
				// $('input[name=integral]').val(_data.integral);

				$('.integral-btn').prop('disabled', false);

				_data.integral = 0;
			}

			if($('.sales-agent-order-input').val() != '' || $('#sa_select').val() > 0) {
				$('#sa_select').val(0);
				$('.sales-agent-tag').empty();
				$('.sales-agent-order-input').val('');
				
				// $('.sales-agent-order-input').val(_data.saOrder);
				$('.sales-agent-order-input').prop('disabled', false);

				$('.sales-agent-order-btn').prop('disabled', false);

				_data.saId = 0;
				_data.saOrder = '';
			}
		}
	},

	// search goods
	getGoodsBySnApi: function(goodsSN) {
		var _this = this;
		var userRank = _this.data.user_rank;
		var mode = (!_this.data.mode) ? 'normal' : _this.data.mode;
		
		this.ajaxPOSPostRequest(
			{
				'act':'get_goods_by_sn',
				'goods_sn':goodsSN,
				'user_rank':userRank,
				'order_mode':mode,
			},
			function(data) {
				// Clear Barcode field
				$('.search_goods_sn').val('');

				if (_this.data.mode != 'repair') {
					_this.loadSearchResults(data);
				} else {
					_this.loadRepairGoods(data);
				}
			}
		)
	},

	addToCartApi: function(goods_id, qty, is_gift, clean_integral, is_package) {
		is_gift = is_gift || 0;
		is_package = is_package || 0;
		clean_integral = clean_integral || 1;
		var _this = this;
		var _data = _this.data;
		var user_rank = _data.user_rank;
		var mode = (!_data.mode) ? 'normal' : _data.mode;
		var payment = _data.payment;
		var isRedeem = (payment == _this.const.REDEEM_PAYMENT) ? '1' : '0';

		this.ajaxPOSPostRequest(
			{
				'act':'add_to_cart',
				'goods_id':goods_id,
				'quantity':qty,
				'shipping': _data.shipping,
				'payment':payment,
				'country': _data.country,
				'order_mode':mode,
				'user_rank':user_rank,
				'is_gift': is_gift,
				'is_package': is_package,
				'clean_integral':clean_integral,
				'is_redeem': isRedeem,
			},
			function(data) {
				if(data.status == 1)
				{
					_this.handleCartList(data.cart_list, mode);

					_this.handleWEEEform(data.need_removal);

					// get Favourable, require Good and package
					_this.getFavourableApi();
					_this.getRequireGoodsApi();
					_this.getPackageApi();
					
					//update amount and calculate cash change
					if(Object.keys(data.cart_list.goods_list).length != 0) {
						_this.calculateAmountApi();
					} 
					else {
						$('.total-amount').html(data.total_amount.toFixed(2));
						$('.order-amount-info').html(data.order_total);
						_this.calculateCashChange();
					}

					// order remarks
					_data.orderRemarks = data.preorder_msg;
					$('.remarks-dialog .confirm-button').trigger('click');

					_data.have_preorder_goods = (data.preorder_msg.length > 0);
					if(	_data.shippingType == _this.const.EXPRESS_SHIPPING_TYPE ) {
						_this.ajaxShippingTypeFlowRequest(
							'address', 
							{consignee: {
								country: _data.currentAddress.country,
								province: _data.currentAddress.province,
								city: 0,
								district: 0,
							}}
						);
					}

					// check the goods is for the sales agent
					if( _data.payment == _this.const.REDEEM_PAYMENT ) {
						// reset sales agent
						if(_data.saOrder != '') {
							$('.sales-agent-tag').empty();
			
							$('.sales-agent-order-input').val(_data.saOrder);
							$('.sales-agent-order-input').prop('disabled', false);
			
							$('.sales-agent-order-btn').prop('disabled', false);
			
							_data.saOrder = '';
						}

						if($('input[name=sa-order-input]').val() != '') {
							$('.sales-agent-order-input').blur();
						}
					}
				}
				else
				{
					YOHO.dialog.warn(data.error);
				}
			}
		)
	},

	getFavourableApi: function() {
		var _this = this;
		var _data = _this.data;
		var user_rank = _data.user_rank;
		var user_id = _data.user_id;
		var mode = (!_data.mode) ? 'normal' : _data.mode;
		var paymentMethodVal = _data.payment;

		if(mode == 'normal' && paymentMethodVal != _this.const.REDEEM_PAYMENT) {
			this.ajaxPOSPostRequest(
				{
					'act':'load_favourable',
					'user_rank':user_rank,
					'user_id':user_id,
				},
				function(data) {
					_this.handleFavourableTable(data);
				}
			)
		} else {
			// Clear favourable Table
			var $favourableTable = $('#favourableTable');
				$favourableTable.empty();
				$favourableTable.hide();
		}
	},

	addFavourableApi: function(goods_id, qty, act_id) {
		var _this = this;
		var _data = _this.data;
		var mode = (!_data.mode) ? 'normal' : _data.mode;

		this.ajaxPOSPostRequest(
			{
				'act':'add_favourable',
				'goods_id':goods_id,
				'quantity':qty,
				'shipping':_data.shipping,
				'payment':_data.payment,
				'country': _data.country,
				'order_mode':mode,
				'user_rank':_data.user_rank,
				'user_id': _data.user_id,
				'act_id':act_id
			},
			function(data) {
				if (data.status == 1)
				{
					_this.addToCartApi(0, 0, 0, 2, 0);
				}
				else
				{
					YOHO.dialog.warn(data.error);
				}
			}
		)
	},

	getRequireGoodsApi: function() {
		var _this = this;
		var user_rank =  _this.data.user_rank;
		this.ajaxPOSPostRequest(
			{
				'act':'load_requiregoods',
				'user_rank':user_rank,
			},
			function(data) {
				if (data.status == 1) {
					_this.handleRequireGoodsTable(data);
				} else {
					YOHO.dialog.warn(data.error);
				}
			}
		)
	},

	getPackageApi: function() {
		var _this = this;
		if(_this.data.payment != _this.const.REDEEM_PAYMENT) {
			this.ajaxPOSPostRequest(
				{
					'act':'load_packages',
				},
				function(data) {
					_this.handlePackageTable(data);
				}
			)
		} else {
			var $packagesTable = $('#packagesTable');
				$packagesTable.empty();
				$packagesTable.hide();
		}
	},

	addPackageApi: function(package_id, goods_id, qty) {
		var _this = this;
		var _data = _this.data;
		var user_rank = _data.user_rank;
		
		this.ajaxPOSPostRequest(
			{
				'act':'add_package_to_cart',
				'package_id': package_id,
				'goods_id': goods_id,
				'quantity': qty,
				'user_rank':user_rank,
			},
			function(data) {
				if (data.status == 1) {
					_this.addToCartApi(0, 0);
				} else {
					YOHO.dialog.warn(data.error);
				}
			}
		)
	},

	updateAddressApi: function(params) {
		var _this = this;
		params.user_id = _this.data.user_id;
		_this.ajaxPostRequest(
			'/ajax/updateaddress.php', 
			params, 
			function(data) {
				if(data.status == 1)
				{
					_this.ajaxShippingTypeFlowRequest('shipping_type', [], function() {
						YOHO.dialog.success(YOHO._('checkout_address_updated', '地址修改成功！'));
						_this.triggerShipperAddressCloseButton();
						var selectAddress = $('input[name="address_radio"][value="'+data.obj.id+'"]');
						if(selectAddress.length > 0) {
							selectAddress.prop('checked', true).trigger('click');
						}
					});

				}
				else if(data.status == 0 || data.status == 2)
				{
					YOHO.dialog.warn(YOHO._('checkout_address_error_too_long', '儲存失敗：收貨人信息過長'));
				}
				else if(data.status == 4)
				{
					YOHO.dialog.warn(YOHO._('checkout_address_error_exceed', '儲存失敗：地址數量超過上限'));
				}
				else if(data.status == 7)
				{
					YOHO.dialog.warn(YOHO._('checkout_address_error_invalid_format', '地址填寫錯誤'));
				}
				else if(data.status == 8)
				{
					YOHO.dialog.warn(YOHO._('checkout_address_error_register_failed', '儲存失敗：電郵地址已經存在!<br>如果您已經註冊，請直接登入。'));
				}
				else
				{
					window.location.reload();
				}
			}
		)
	},

	checkAddressInfoApi: function(userAddresId) {
		var _this = this;
		var res = false;
		if(!userAddresId) return res;
		var data = {act: 'check_address_info_filled', user_addr_id: userAddresId};
		_this.ajaxPostRequest(
			'/ajax/checkout.php', 
			data, 
			function(result) {res = (result.status == 1) ? result.res : false;},
			false
		)
		return res;
	},

	getFormatAddressApi: function(params) {
		var data = {act: 'get_format_address'}
		$.extend( data, params );
		var _this = this;
		_this.ajaxGetRequest(
			'/ajax/address.php', 
			data, 
			function(result) {
				var addressInfo = result.address;

				_this.data.currentAddress.format_address = addressInfo.address;
				_this.showFormatedAddress(addressInfo.address);
				_this.triggerShipperAddressCloseButton();
				_this.ajaxShippingTypeFlowRequest(
					'address', 
					{consignee: {
						country: addressInfo.country,
						province: addressInfo.province,
						city: 0,
						district: 0,
					}}
				);
			},
		)
	},

	calculateAmountApi: function() {
		var _this = this;
		var _data = _this.data;
		var $mobile = $('input[name="mobile"]');
		var userName = (_data.user_id != 0) ? '+' + $mobile.intlTelInput('getSelectedCountryData').dialCode + '-' +$mobile.val() : '';
		var mode = (!_data.mode) ? 'normal' : _data.mode;

		this.ajaxPOSPostRequest(
			{
				act: 'calculate_amount',
				user_name: userName,
				coupon: _data.coupon_codes.join(','), 
				integral: _data.integral,
				discount: _data.discount,
				shipping: _data.shipping,
				shipping_area: _data.shipping_area_id,
				payment: _data.payment,
				country: _data.country,
				order_mode: mode,
			},
			function(data) {
				if(data.status == 1) {
					$('.total-amount').html(data.total_amount.toFixed(2));
					$('.order-amount-info').html(data.order_total);
					_this.calculateCashChange();

					// render tag label with delete
					_this.handleDiscountTagLabel();

					_this.handleCouponTagLabel(data.coupon_code);

					_this.handleIntegralTagLabel();
					
					// promotion
					var $promotion = $('.promotion-wrapper');
						$promotion.empty();
					$.each(data.promote_list, function(promote_code, promote_id) {
						if (promote_id == 1) {
							var promotionElem = _this.render.promotionTheClubList(promote_code);
							$promotion.append(promotionElem);
						} 
					})

					$('.promotion-input').on('keyup', function(event) {
						event.preventDefault();
						if(event.which === _this.const.KEYCODE_ESC) $(this).blur();
					});
				} else {
					YOHO.dialog.warn(data.error);
					if (data.hasOwnProperty('pay_points'))
					{
						$('input[name="integral"]').val(data.pay_points);
					}
					else if (data.hasOwnProperty('flow_points'))
					{
						$('input[name="integral"]').val(data.flow_points);
					}
					if (data.hasOwnProperty('vardiscount'))
					{
						$('input[name="discount"]').val('').trigger('blur');
					}
					if (data.hasOwnProperty('coupon_code'))
					{
						_data.coupon_codes = [];
						$('input[name="coupon"]').val(data.coupon_code).trigger('blur');
					}
					if(_data.goodsList.length == 0) {
						// reset coupon code, discount and integral data
						_data.coupon_codes = [];

						_data.discount = 1;
						$('input[name=discount]').val('');

						_data.integral = 0;
						$('input[name=integral]').val('');
					}
				}
			}
		)
	},

	validateSaGoodsApi: function(goodsIds) {
		var _this = this;
		this.ajaxPOSPostRequest(
			{
				act: 'validate_sa_goods',
				goods_ids: goodsIds,
				sa_id: _this.data.saId,
			},
			function(data) {
				if(data.status == 1) {
					_this.data.saError = null;
					_this.handleSalesAgentTagLabel();
				} else {
					_this.data.saError = data.error;
					YOHO.dialog.warn(data.error);
				}
			}
		);
	},

	getOrderBySnApi: function(orderSn) {
		var _this = this;
		
		this.ajaxPOSPostRequest(
			{
				'act':'get_order_by_sn',
				'order_sn': orderSn
			},
			function(data) {
				_this.loadOrder(data);
			}
		)
	},

	getCustomerInfoApi: function() {
		var _this = this;
		this.ajaxPOSPostRequest(
			{
				act: 'load_user_input',
			},
			function(data) {
				if(data.status == 1) {
					_this.handleCustomerInfo(data.input_list);
				} else {
					YOHO.dialog.warn(data.error);
				}
			}
		);
	},

	// ajax
	ajaxPOSPostRequest: function(data, successCallback) {
		$.ajax({
			url: '/pos_new.php',
			type: 'post',
			cache: false,
			data: data,
			dataType: 'json',
			success: function(data) {
				successCallback(data);
			},
			error: function(xhr) {
				YOHO.dialog.warn("伺服器繁忙，請稍後再試。("+xhr.status+")");
				console.log('error: ' + JSON.stringify(xhr));
			}
		});
	},

	ajaxShippingTypeFlowRequest: function(step, data, successCallback) {
		var _this = this;
		data = data || {};
		successCallback = successCallback || function(){};
		shipping_type_code = _this.data.shippingType;
		var userId = _this.data.user_id;

		$.ajax({
			url : '/pos_new.php',
			type: 'POST',
			cache: false,
			data: {user_id: userId, code: shipping_type_code, act: 'select_shipping_type', step: step, data: data},
			dataType: 'json',
			success: function(data) {
				if(data.status == 1) {
					var nextStep = data.nextStep;
					if(nextStep !== null) {
						var stepName = nextStep.stepName
						if(shipping_type_code == _this.const.EXPRESS_SHIPPING_TYPE) {
							if(stepName == 'address') {
								_this.showAddressListDialog(nextStep, shipping_type_code);
							} else if(stepName == 'shipping') {
								// render shipping box
								_this.showExpressShippingMethod(nextStep);
							}
						} else if(shipping_type_code == _this.const.PICKUP_POINT_SHIPPING_TYPE) {
							if(stepName == 'shipping') {
								_this.showPickupPointDialog(nextStep);
							} else if(stepName == 'address') {
								$('input[name=locker_consignee]').focus();
								$('.loading').hide();
								_this.handlePickupPointArea(nextStep);
								_this.fillDefaultlocker(nextStep.default);
							}
						}
					}
					successCallback(data);
				}
			},
			error: function(xhr) {
				YOHO.dialog.warn("伺服器繁忙，請稍後再試。("+xhr.status+")");
				console.log('error: ' + JSON.stringify(xhr));
			}
		});
	},

	ajaxPostRequest: function(url, data, successCallback, isAsync) {
		isAsync = (isAsync === undefined) ? true : isAsync;
		$.ajax({
			url : url,
			type: 'post',
			dataType: 'json',
			async: isAsync,
			data: data,
			success:function(result){
				successCallback(result);
			},
			error: function(xhr) {
				YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
			}
		})
	},

	ajaxGetRequest: function(url, data, successCallback, isAsync) {
		isAsync = (isAsync === undefined) ? true : isAsync;
		$.ajax({
			url : url,
			type: 'get',
			dataType: 'json',
			async: isAsync,
			data: data,
			success:function(result){
				successCallback(result);
			},
			error: function(xhr) {
				YOHO.dialog.warn(YOHO._('global_ajax_error', '伺服器繁忙，請稍後再試。') + '('+xhr.status+')');
			}
		})
	},
};

$(function() {
	YOHO.pos.data.mode = YOHO.common.getUrlParameter('mode') || 'normal';

	if (YOHO.pos.data.mode != 'repair') {
		YOHO.pos.init();
	} else {
		YOHO.pos.initRepair();
	}
});

})(jQuery);
