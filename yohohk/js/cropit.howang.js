(function (_win, $, undef) {
	_win.buildCropit = function(fieldName, imageWidth, imageHeight) {
		var $container = $('<div class="cropit-image-container"></div>');
		$container.data('fieldName', fieldName);
		$container.data('imageWidth', imageWidth);
		$container.data('imageHeight', imageHeight);
		$container.html(
			'<div class="cropit-image-preview-container">' +
				'<div class="cropit-image-preview"></div>' +
			'</div>' +
			'<div class="cropit-image-zoom-control" style="display:none;">' +
				'<span class="cropit-image-zoom-small"></span>' +
				'<input type="range" class="cropit-image-zoom-input" />' +
				'<span class="cropit-image-zoom-large"></span>' +
			'</div>' +
			'<input type="file" class="cropit-image-input" />' // name="' + fieldName + '"
		);
		return $container;
	};
	
	_win.initCropit = function($ele, default_img, extra_opts) {
		var opt = {
			width: $ele.data('imageWidth'),
			height: $ele.data('imageHeight'),
			exportZoom: 1,
			imageBackground: true,
			imageBackgroundBorderWidth: 20,
			onImageLoaded: function () {
				// If there is default_img, init imageLoadedCount to 0, otherwise init to 1
				var imageLoadedCount = $ele.data('imageLoadedCount') || (default_img ? 0 : 1);
				if (imageLoadedCount++ > 0)
				{
					this.$zoomSlider.closest('.cropit-image-zoom-control').show();
					this.$preview.css('cursor', 'move');
				}
				$ele.data('imageLoadedCount', imageLoadedCount);
			}
		};
		if (extra_opts !== undef)
		{
			opt = $.extend({}, opt, extra_opts);
		}
		$ele.cropit(opt);
		$ele.width(Math.max(opt.width + (opt.imageBackground ? opt.imageBackgroundBorderWidth * 2 : 0), 250));
		if (default_img)
		{
			if (extra_opts !== undef && extra_opts.existanceCheck)
			{
                // Test image existance
                $.ajax({
                  url: default_img,
                  type: 'head',
                  cache: false,
                  success: function () {
					$ele.cropit('imageSrc', default_img);
                  },
                  error: function () {
					default_img = '';
                  }
                });
			}
			else
			{
				// Assume default_img exists
				$ele.cropit('imageSrc', default_img);
			}
		}
	};
	
	var dataURItoBlob = function(dataURI) {
		// convert base64/URLEncoded data component to raw binary data held in a string
		var byteString;
		if (dataURI.split(',')[0].indexOf('base64') >= 0)
		{
			byteString = atob(dataURI.split(',')[1]);
		}
		else
		{
			byteString = unescape(dataURI.split(',')[1]);
		}
		// separate out the mime component
		var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
		// write the bytes of the string to a typed array
		var ia = new Uint8Array(byteString.length);
		for (var i = 0; i < byteString.length; i++)
		{
			ia[i] = byteString.charCodeAt(i);
		}
		return new Blob([ia], {type:mimeString});
	};
	
	var resizeImage = function (dataURL, targetWidth, targetHeight, callback) {
		var img = new Image();
		img.onload = function() {
			var canvas = document.createElement('canvas');
			canvas.width = targetWidth;
			canvas.height = targetHeight;
			
			var ctx = canvas.getContext('2d');
			ctx.drawImage(img, 0, 0, img.width, img.height, 0, 0, targetWidth, targetHeight);
			
			callback(canvas.toDataURL('image/png'));
		};
		img.onerror = function() {
			callback(null);
		};
		img.src = dataURL;
	};
	
	var resizeImageProgressively = function (dataURL, targetWidth, targetHeight, callback) {
		var targetSize = {width: targetWidth, height: targetHeight};
		var step = 0.5;
		var img = new Image();
		img.onload = function() {
			var currentSize = {width: img.width, height: img.height};
			
			var canvas = document.createElement('canvas');
			canvas.width = currentSize.width;
			canvas.height = currentSize.height;
			
			var ctx = canvas.getContext('2d');
			ctx.drawImage(img, 0, 0, img.width, img.height, 0, 0, currentSize.width, currentSize.height);
			
			var ratioW, ratioH, ratio;
			do {
				ratioW = targetSize.width / currentSize.width;
				ratioH = targetSize.height / currentSize.height;
				ratio = ratioW < ratioH ? ratioW : ratioH;
				if (ratio <= step)
				{
					var newSize = {width: Math.round(currentSize.width * step), height: Math.round(currentSize.height * step)};
					if (window.console)
					{
						console.log('resizeImageProgressively: {' + currentSize.width + ', ' + currentSize.height + '} resize to {' + newSize.width + ', ' + newSize.height + '}');
					}
					ctx.drawImage(canvas, 0, 0, currentSize.width, currentSize.height, 0, 0, newSize.width, newSize.height);
					currentSize = newSize;
				}
			} while (ratio <= step);
			
			var targetCanvas = document.createElement('canvas');
			targetCanvas.width = targetSize.width;
			targetCanvas.height = targetSize.height;
			
			var targetContext = targetCanvas.getContext('2d');
			targetContext.drawImage(canvas, 0, 0, currentSize.width, currentSize.height, 0, 0, targetSize.width, targetSize.height);
			
			callback(targetCanvas.toDataURL('image/png'));
		};
		img.onerror = function() {
			callback(null);
		};
		img.src = dataURL;
	};
	
	_win.cropitExport = function ($ele, callback) {
		var dataURL = $ele.cropit('export', {
			// type: 'image/jpeg',
			// quality: 0.9,
			// fillBg: '#fff'
			originalSize: true,
			type: 'image/png'
		});
		
		if (!dataURL)
		{
			callback(null);
			return;
		}
		
		resizeImageProgressively(dataURL, $ele.data('imageWidth'), $ele.data('imageHeight'), function (resized) {
			var blob = dataURItoBlob(resized);
			callback(blob);
		});
	};
	
	// Code borrowed from caolan/async
	var asyncRoot = this;
	function asyncOnlyOnce(fn) {
		var called = false;
		return function() {
			if (called)
			{
				throw new Error("Callback was already called.");
			}
			called = true;
			fn.apply(asyncRoot, arguments);
		};
	}
	var asyncEach = function (arr, iterator, callback) {
		callback = callback || function () {};
		if (!arr.length)
		{
			return callback();
		}
		var completed = 0;
		for (var i = 0; i < arr.length; i += 1)
		{
			iterator(arr[i], asyncOnlyOnce(done));
		}
		function done(err)
		{
			if (err)
			{
				callback(err);
				callback = function () {};
			}
			else
			{
				completed += 1;
				if (completed >= arr.length)
				{
					callback();
				}
			}
		}
	};
	
	_win.cropitFormHook = function($form, imgs) {
		var form = $form.get(0);
		// Due to the different in onsubmit and submit event, we disable the onsubmit, and handle it ourselves
		var onsubmit = null;
		$(function () {
			if (typeof(form.onsubmit) == 'function')
			{
				onsubmit = form.onsubmit;
				form.onsubmit = 'return true;';
			}
		});
		$form.submit(function (event) {
			// If from have onsubmit, run it
			if (typeof(onsubmit) == 'function')
			{
				if (onsubmit() === false)
				{
					event.preventDefault();
					event.stopPropagation();
					event.stopImmediatePropagation();
					return false;
				}
			}
			
			// If FCKEditor is used, force them to update the underlaying text input
			if (window.FCKeditorAPI && FCKeditorAPI.Instances)
			{
				$.each(FCKeditorAPI.Instances, function (i, oEditor) {
					oEditor.UpdateLinkedField();
				});
			}
			
			// If user didn't load new image, append empty file input field to the form
			var fileInputs = [];
			$.each(imgs, function(i, $ele) {
				var imageLoadedCount = $ele.data('imageLoadedCount') || 0;
				if (imageLoadedCount <= 1)
				{
					var $fileInput = $('<input type="file" name="' + $ele.data('fieldName') + '">');
					$fileInput.appendTo($form);
					fileInputs.push($fileInput);
				}
			});
			
			// Obtain FormData from the form
			var formdata = new FormData(form);
			
			// Remove the previously appended empty file input fields
			$.each(fileInputs, function(i, $fileInput) {
				$fileInput.remove();
			});
			
			// Append the exported image as Blob to our FormData
			asyncEach(imgs, function($ele, callback) {
				var imageLoadedCount = $ele.data('imageLoadedCount') || 0;
				if (imageLoadedCount > 1)
				{
					_win.cropitExport($ele, function (blob) {
						formdata.append($ele.data('fieldName'), blob, 'image.png');
						callback(null);
					});
				}
				else
				{
					callback(null);
				}
			}, function (err) {
				// Finally submit the FormData to server via AJAX
				$.ajax({
					url: form.action || window.location.href,
					type: form.method,
					data: formdata,
					processData: false,
					contentType: false,
					dataType: 'html',
					success: function (res) {
						// Replace body with HTML response from server
						// also hack to make window.onload work
						var old_onload = window.onload;
						$('body').html(res);
						if (window.onload != old_onload)
						{
							window.onload();
						}
					}
				});
			});
			
			event.preventDefault();
		});
	};
	// Somehow I made a typo and didn't realize it before it's everywhere in the code
	// This is the temporary measure to keep misspelled code work (espcially for those code cached by browsers)
	_win.cropitFromHook = function($form, imgs) {
		return _win.cropitFormHook($form, imgs);
	};
})(window, jQuery);
