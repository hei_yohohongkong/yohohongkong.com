<?php
/**
* YOHO VIP page
* Anthony@YOHO 20170720
*/
define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

if ((DEBUG_MODE & 2) != 2) {
    $smarty->caching = true;
}

$user_id = $_SESSION['user_id'];
$user_rank = $_SESSION['user_rank'];
$rank_points = $_SESSION['rank_points'];
$luckyDrawController = new Yoho\cms\Controller\LuckyDrawController();
$lucky_draw_active = $luckyDrawController->getActiveLuckyDrawEvent();

if (empty($lucky_draw_active)) {
	header('Location: /luckydraw');
	exit; 
}

$lucky_draw_id = $lucky_draw_active[0];
// $user_ticket_count = $luckyDrawController->getDrawTicketCount($user_id,$lucky_draw_id);
// 
// if ($user_ticket_count == 0){
//     // redirect
//    	header('Location: /luckydraw');
//    	exit; 
// }

if (!empty($_REQUEST['action']) && $_REQUEST['action'] == 'lucky_draw_notify') {
    $ticket_id = $_REQUEST['play_id'];
    // get the prize (gift id)
    $result = $luckyDrawController->sendLuckyDrawResultNotice($ticket_id);

    if (isset($result['error'])) {
        echo json_encode($result);
    } else {
        echo json_encode(array('data' => $result) );
    }
    exit;

}


