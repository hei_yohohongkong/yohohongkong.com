/* 商品列表页
 * update 2013.04.05
*/
var NALA = NALA || {};
NALA.list={
	init:function(){
		this.priceSearch();
		this.addCart();
	},
	cataTab:function(){
		$("#J_cataTab .cc_tit").switchable("#J_cataTab .sitem2",{speed:0.7,delay:0.12,triggerType:"mouse",vertical:false,triggers:"li"});
	},
	priceSearch:function(){
		function doSearchByPrice(){
			var pricemin = $("[name=pricemin]").val();
			var pricemax = $("[name=pricemax]").val();
			var flag = true;

			if(isNaN(pricemin) || isNaN(pricemax) || (pricemin==""&&pricemax=="")){
				flag = false;
				alert("请输入正确的数字！！！");
				return flag;
			}
			else {
				$("#hidden_price").val($("[name=pricemin]").val()+"~"+$("[name=pricemax]").val());
				$("#form2").submit();
			}		
		}
		$("#J_fmPrice input").focus(function(){
			$("#J_fmPrice").height("60px").css({"border":"1px solid #e6e6e6","borderTop":"none","background":"#fefefe"});
		});
		$("#J_fmPrice button").click(function(e){
			e.preventDefault();
			doSearchByPrice();
		});
		$(document).click(function(){
			$("#J_fmPrice").height("22px").css({"border":"none","background":"none"});
		});
		$('#J_fmPrice').click(function(event) {
			event.stopPropagation();
		});
	},
	addCart:function(){
		$('a.addcart').click(function(){
			var id = $(this).next('input').val();
			NALA.dialog.addCart(id,1);
		});
	},
	brandImg:function(){
		$(".brands_list img").each(function(){
			var $this =$(this);
			var _image = new Image();
			_image.src= $(this).attr("src");
			var _h = _image.height;	
			var _w = _image.width;
			var _mT = Math.floor(20 -(50*_h/_w));
			if(_h/_w > 2/5){
				$(this).css({"height":40,"width":Math.floor(40*_w/_h)});
			}
			else{
				$(this).css({
					"width":100,"height":Math.floor(100*_h/_w),
					"paddingTop":_mT,
					"paddingBottom":40-Math.floor(100*_h/_w)-_mT
				});
			}
		});
	}
};
$(function(){
	NALA.list.init();

	// 天气预报
	if($("#weather_city").length>0){
		$.ajax({
			url: '/item/ajaxWeather',
			success: function (data) {
			   $("#weather_city").text(data.city);
			   $("#url2").attr('href',data.url);
			   $("#small1").attr('src',data.img);
			   $("#small1").attr('title',data.title);
			   $("#temp1").html(data.temp);
			   $("#wind1").html(data.wind);
			}
		});
	}

	if($("#J_cataTab").length ==1){
		$("#J_cataTab .cc_tit").switchable("#J_cataTab .sitem2",{speed:0.7,delay:0.12,triggerType:"mouse",vertical:false,triggers:"li"});
	}
	if($(".brands_list").length ==1){
		NALA.list.brandImg();
	}

	if($('#ulgx').length > 0){

		if (parseInt($('#ulgx').height()) > 78){
			toggle_box("#ulgxmore");
		}else{
			$('#ulgx').parent('.sitemcon').css('height','auto');
		}
		if (parseInt($('#ulpp').height()) > 78){
			toggle_box("#ulppmore");
		}else{
			$('#ulpp').parent('.sitemcon').css('height','auto');
		}

	}
	
});


function toggle_box(_btn) {
	var btn = $(_btn).css("visibility",'visible'),
		box = btn.prev('.sitemcon');
	btn.click(function(){
		if(btn.hasClass('top_brand')){
			box.css({'height':'78px','overflow':'hidden'});
			btn.removeClass('top_brand').addClass('more_brand');
		}else{
			box.css({'height':'auto','overflow':'visible'});
			btn.removeClass('more_brand').addClass('top_brand');
		}
		return false;
	});
}