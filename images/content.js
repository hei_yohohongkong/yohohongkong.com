// JavaScript Document

$(document).ready(function(){
	// remove topinfoline
	if ($('#topinfoline').length > 0) $('body').css('background-position','0 24px');
	
	if ($('#header').length > 0)
	{
		//$('#header .header_bottom li').mouseover(
		//	function(){$('#header .header_bottom li').removeClass('hover'); $(this).prev('li').addClass('hover');}
		//);
		
		$('#header .header_bottom li.current').prev('li').addClass('fix');
		$('#header .header_bottom li').hover(
			function(){
				$('#header .header_bottom li').removeClass('hover');
				$(this).prev('li').addClass('hover');
			},
			function(){
				$(this).prev('li').removeClass('hover');
			}
		);
		
		$('#home .mainbox td').mouseover(function(){
			$('#home .mainbox td').removeClass('hover');
			$(this).addClass('hover');
		});
		
		$('#header .links .item2').live('mouseover mouseout', function(event) {
			if (event.type == 'mouseover')
			{$('#header .mynala_popup').show(); $('#header .mynala_link').addClass('mynala_link_on');}
			else
			{$('#header .mynala_popup').hide(); $('#header .mynala_link').removeClass('mynala_link_on');}
		});
	}
	
	// header search input
	if ($('#header input.text').length > 0)
	{
		$('#header input.text').focus(function(){
			$(this).addClass('focus');
			if ($.trim($(this).val()) == '输入关键字') $(this).val('');
		});
		$('#header input.text').blur(function(){
			if ($.trim($(this).val()) == '')
			{
				$(this).removeClass('focus');
				$(this).val('输入关键字');
			}
		});
		
		$('#search_fm').submit(function(){
			if ($.trim($('#search_fm input.text').val()) == '输入关键字') return false;
		});
	}
	
	// brand hover event
	if ($('#popupitem').length > 0)
	{
		//旧页面二级导航菜单popup
		//$('#topnav li.firstlevel').hover(
		//	function(){
		//		if ($(this).find('.con').length > 0)
		//		{
		//			$(this).find('.fix').width($(this).width()-4).show();
		//			$(this).children('a:eq(0)').addClass('hover');
		//			$(this).find('.con').show();
		//		}
		//	},
		//	function(){
		//		if ($(this).find('.con').length > 0)
		//		{
		//			$(this).find('.fix').hide();
		//			$(this).children('a:eq(0)').removeClass('hover');
		//			$(this).find('.con').hide();
		//		}
		//	}
		//);
		
		$('#popupitem').hoverIntent(
			function(){
				//if ($.browser.msie && $.browser.version == '6.0')
				//{$('body').css('overflow','hidden');}
				//else
				//{$('#wrapper').css('padding-right','16px');if ($.browser.mozilla) $('#wrapper').css('padding-right','18px');$('html').css('overflow','hidden');}
				
				$('#popupitem a.allbrand').addClass('brandactive');
				$('#popupitem .popupitem_con').hide();
				$('#popupitem .popupitem_con').eq(0).show();
				$('#popupitem .point_brand, #popupitem .propitem, #popupitem .fix').show();
			},
			function(){
				//$('#wrapper').css('padding-right','0');
				
				//if ($.browser.msie && $.browser.version == '6.0')
				//{$('body').css('overflow','hidden');}
				//else
				//{$('html').css('overflow','auto');}
				
				$('#popupitem ul.category a').removeClass('current');
				$('#popupitem ul.category a').eq(0).addClass('current');
				$('#popupitem .allbrand').removeClass('brandactive');
				$('#popupitem .point_brand, #popupitem .propitem, #popupitem .fix').hide();
			}
		);
		
		$('#popupitem ul.category a').click(function(){
			var index = $('#popupitem ul.category a').index(this);
			$('#popupitem ul.category a').removeClass('current');
			$(this).addClass('current');
			$('#popupitem .popupitem_con').hide();
			$('#popupitem .popupitem_con').eq(index).show();
			return false;
		});
	}
	
	// homepage slideshow
	if ($('#homeslideshow').length > 0)
	{
		setTimeout(function(){
			$('#homeslideshow img:first').fadeIn(1500);
			$('#homeslideshow').cycle({timeout:5000, pause:1, pager:'#homeslideshownav'});
		}, 1500);
	}
	
	// prodcut list page - view more
	if ($('#screening').length > 0)
	{
		if (parseInt($('#ulgx').height()) > 24) $('#ulgxmore').show();
		if (parseInt($('#ulpp').height()) > 24) $('#ulppmore').show();
		
		$('#screening div.sitem:lt(3)').find('li:last a').css('border','none');
		
		$('#ulgxmore').toggle(
			function(){
				$('#ulgx').parents('.sitemcon').css({'height':'auto','overflow':'visible'});
				$(this).removeClass('more_brand').addClass('top_brand').text('精简');
			},
			function(){
				$('#ulgx').parents('.sitemcon').css({'height':'24px','overflow':'hidden'});
				$(this).removeClass('top_brand').addClass('more_brand').text('更多');
			}
		);
		
		$('#ulppmore').toggle(
			function(){
				$('#ulpp').parents('.sitemcon').css({'height':'auto','overflow':'visible'});
				$(this).removeClass('more_brand').addClass('top_brand').text('精简');
			},
			function(){
				$('#ulpp').parents('.sitemcon').css({'height':'24px','overflow':'hidden'});
				$(this).removeClass('top_brand').addClass('more_brand').text('更多');
			}
		);

	}

	// image load event 
	if ($('img[original]').length > 0)
	{$('img[original]').lazyload({effect:"fadeIn"});}
	
	// product detail page items
	if ($('#productdetail').length > 0)
	{
		$('#specificationitem a').click(function(){
			var storage = $(this).attr('storage');
			$('#specificationitem a').removeClass('current');
			$(this).addClass('current');
			$('#specification').val($(this).attr('skuid'));
			if(storage>0){
				var kucunText = "件(库存"+storage+"件)";
				$("#quantity").next('span').html(kucunText);
			}else{
				var wukuText = "件当前无库存";
				$("#quantity").next('span').html(wukuText);
			}
		});
		$('#specificationitem a:eq(0)').trigger('click');

		$('#thumbnails .imgdvs a').click(function(){
			$('#productdetailimg img').attr('src',$(this).children('img').attr('src').replace('tiny_',''));
		});
				
		$("#productdetail .scrollable").scrollable();
		
		$('#productdetailimg a').jqzoom({
			preloadText:'图片加载中...',
			lens:true,
			preloadImages:false,
			alwaysOn:false,
			zoomWidth: 475,
			zoomHeight: 288,
			xOffset: 19
		});
		
		if ($.browser.msie)
		{
			$('#productdetailimg a').hover(
				function(){$('#productdetail .overlaydv').show();},
				function(){$('#productdetail .overlaydv').hide();}
			);
		}
		
		//$('#addcart_button').click(function(){
		//	//setTimeout(function(){$('#popupbox_addcart').show()},100);
		//	setTimeout(function(){$('#popupbox_addcart_errormsg').show()},100);
		//});
		
		$('.popupbox a.close').click(function(){
			$(this).parents('.popupbox').fadeOut('500');
		});
		
		
		// 收藏按钮popupbox
		$('#socialbuttons a.collect').click(function(){
			$.post('/shoucang/', {productid:'1111'}, function(data){
				
			});
			alert('需通过ajax收藏该产品，并返回成功的标识，而后显示下面的收藏成功信息。');
			$('#popupbox_collect').show();
			setTimeout(function(){$('#popupbox_collect').fadeOut(500);},2000);
		});
	}
	
	if ($('#producttabs').length > 0)
	{
		$('#producttabs a').click(function(){
			var index = $('#producttabs a').index(this);
			var id = $(this).attr('class');
			$('#producttabs li').removeClass('current');
			$(this).parents('li').addClass('current');
			
			if (index == 0)
			{$('div.sectionbox').show();$('div.sectionbox .title').show();
			     $('#bzxzitem').css('margin-top','10px');
				 $('#shfwitem').css('margin-top','10px');
			}
			else
			{$('div.sectionbox').hide(); $('div.sectionbox .title:gt(1)').hide(); $('div.sectionbox[id*="'+id+'"]').show();
			     $('#bzxzitem').css('margin-top','0');
				 $('#shfwitem').css('margin-top','0');
			}
			
			return false;
		});
	}
	
	// satisfied
	if ($.browser.msie && $.browser.version == '6.0')
	{$('#satisfiedcon').scrollFollow({'relativeTo':'bottom'});}
	
	// scrollable function
	if ($("#home").length > 0)
	{
		$("#home .scrollable").scrollable();
		$("#home .sidebar_list li").mouseover(
			function(){
				$(this).parents(".sidebar_list").find("a.current").removeClass("current");
				$(this).children('a:eq(0)').addClass("current");
				$(this).parents(".sidebar_list").find(".imgdv").hide();
				$(this).find('.imgdv').show();
			}
		);
	}
	
	
	//
	if ($('#hotbrandtabs').length > 0)
	{
		$('#hotbrandtabs a').mouseover(function(){
			var index = $('#hotbrandtabs a').index(this);
			$('#hotbrandtabs a').removeClass('fix');
			$('#hotbrandtabs a').removeClass('current');
			$(this).addClass('current');
			$(this).prev('a').addClass('fix');
			$('#hotbrand_con table').hide();
			$('#hotbrand_con table').eq(index).show();
		});
	}
	
});

Date.prototype.dateDiff = function(interval,objDate){
	if(arguments.length<2||objDate.constructor!=Date) return undefined;
	switch (interval) {
		case "s":return parseInt((objDate-this)/1000);
		case "n":return parseInt((objDate-this)/60000);
		case "h":return parseInt((objDate-this)/3600000);
		case "d":return parseInt((objDate-this)/86400000);
		case "w":return parseInt((objDate-this)/(86400000*7));
		case "m":return (objDate.getMonth()+1)+((objDate.getFullYear()-this.getFullYear())*12)-(this.getMonth()+1);
		case "y":return objDate.getFullYear()-this.getFullYear();
		default:return undefined;
	}
}

//禁止mouseover冒泡事件开始  #邵盼 2011-08-08
function checkHover(e,target){
if (getEvent(e).type=="mouseover") {
return !contains(target,getEvent(e).relatedTarget||getEvent(e).fromElement) && !((getEvent(e).relatedTarget||getEvent(e).fromElement)===target);
} else {
return !contains(target,getEvent(e).relatedTarget||getEvent(e).toElement) && !((getEvent(e).relatedTarget||getEvent(e).toElement)===target);
}
}
function getEvent(e){
return e||window.event;
}
function contains(parentNode, childNode) {
if(parentNode.contains) {
return parentNode != childNode && parentNode.contains(childNode);
}else{
return !!(parentNode.compareDocumentPosition(childNode) & 16);
}
}
//禁止冒泡事件结束