<?php
	$act = (isset($_GET['act'])) ? $_GET['act'] : '';
	if ($act == 'pi')
	{
		phpinfo();
		exit;
	}
	else if ($act == 'ua')
	{
		echo $_SERVER['HTTP_USER_AGENT'];
		exit;
	}
	else if ($act == 'curl')
	{
		header('Content-Type: text/plain');
		echo 'Start: ' . time() . "\n";
		exec('curl http://www.yohohongkong.com/api/howang/slow.php > /dev/null 2>&1 &');
		echo 'End: ' . time() . "\n";
		exit;
	}
	else if ($act == 'fl')
	{
		define('IN_ECS',true);
		require dirname(__FILE__) . '/includes/init.php';
		
		header('Content-Type: text/plain;charset=utf-8');
		$arr = array_map('hw_get_all_categories', array_map('hw_get_cat_parent', array_unique(array(155,81,6))));
		print_r(
		//$arr
		array_reduce($arr, 'array_merge', array())
		);
		exit;
	}
	else if ($act == 'gen_key')
	{
		define('IN_ECS',true);
		require dirname(__FILE__) . '/includes/init.php';
		require ROOT_PATH . 'core/lib_payload.php';
		
		header('Content-Type: text/plain;charset=utf-8');
		
		echo base64_encode(generate_key());
		
		exit;
	}
	else if ($act == 'change_name')
	{
		define('IN_ECS',true);
		require dirname(__FILE__) . '/includes/init.php';
		require ROOT_PATH . 'core/lib_phpbb.php';
		
		header('Content-Type: text/plain;charset=utf-8');
		
		$obj = array(
			'act' => 'update_user_name',
			'phpbb_userid' => '53',
			'new_name' => 'The Sad Mummy',
		);
		
		// $obj = array(
		// 	'act' => 'remove_users',
		// 	'phpbb_ids' => array(53)
		// );
		
		echo rawurlencode(base64_encode(encode_payload($obj)));
		
		//phpbb_call($obj);
		
		exit;
	}
	else if ($act == 'geoip2')
	{
		define('IN_ECS',true);
		require dirname(__FILE__) . '/includes/init.php';
		
		$ip = real_ip();
		
		$record = geoip2($ip);
		try
		{
			$country_code = $record->country->isoCode;
		}
		catch (Exception $e)
		{
			$country_code = 'NA';
		}
		
		header('Content-Type: text/plain');
		echo $country_code;
		exit;
	}
	else if ($act == 'send_receipt')
	{
		define('IN_ECS',true);
		require dirname(__FILE__) . '/includes/init.php';
		require ROOT_PATH . 'includes/lib_order.php';
		require ROOT_PATH . 'includes/lib_erp.php';
		// Load additional language file
		require_once(ROOT_PATH . 'languages/' . $_CFG['lang'] . '/order_print.php');
		// Re-assign the language array to template engine
		$smarty->assign('lang', $_LANG);
		
		$order = order_info(0, '201404028738');
		if ($order['user_id'] > 0)
		{
			$sql = "SELECT `password`, `ec_salt` FROM " . $ecs->table('users') . " WHERE `user_id` = '" . $order['user_id'] . "'";
			$row = $db->getAll($sql);
			
			if (($row['password'] == md5('yohohongkong')) && ($row['ec_salt'] === null))
			{
				$smarty->assign('default_password', true);
			}
		}
		
		$tpl = get_mail_template('order_receipt');
		$smarty->assign('order', $order);
		$smarty->assign('review_link', $ecs->url() . 'review.php?order_id=' . $order['order_id']);
		$smarty->assign('shop_name', $_CFG['shop_name']);
		$smarty->assign('send_date', local_date($_CFG['date_format']));
		$smarty->assign('sent_date', local_date($_CFG['date_format']));
		$content = $smarty->fetch('str:' . $tpl['template_content']);
		$pdfname = 'receipt_' . $order['order_sn'] . '.pdf';
		$receipt_path = ROOT_PATH . DATA_DIR . '/receiptpdf/' . $pdfname;
		if (!file_exists($receipt_path))
		{
			assign_order_print($order['order_id']);
			$smarty->assign('receipt_background', true);
			
			$old_template_dir = $smarty->template_dir;
			$smarty->template_dir = DATA_DIR;
			$template = 'order_receipt.html';
			if ($order['only_market_invoice'])
			{
				$template = 'order_receipt_market.html';
			}
			$html = $smarty->fetch($template);
			$smarty->template_dir = $old_template_dir;
			
			$pdf = generate_receipt_pdf($html);
			file_put_contents($receipt_path, $pdf);
		}
		send_mail_with_attachment($order['consignee'], $order['email'], $tpl['template_subject'], $content, $tpl['is_html'], false, $receipt_path);
	}
	else if ($act == 'send_verify_email')
	{
		define('IN_ECS',true);
		require dirname(__FILE__) . '/includes/init.php';
		require dirname(__FILE__) . '/includes/lib_passport.php';
		
		$user_id = 4484;
		send_regiter_hash($user_id);
	}
	else if ($act == 'cat_test')
	{
		define('IN_ECS',true);
		require dirname(__FILE__) . '/includes/init.php';
		$goods_list = hw_category_get_goods(get_children(39), 0, 0, 0, '', 12, 1, 'goods_id', 'desc');
		
		header('Content-Type: text/plain;charset=utf-8');
		print_r($goods_list);
		exit;
	}
	else if ($act == 'error_log')
	{
		define('IN_ECS',true);
		require dirname(__FILE__) . '/includes/init.php';
		
		hw_error_log('testing');
		exit;
	}
	else if ($act == 'log_request')
	{
		define('IN_ECS',true);
		require dirname(__FILE__) . '/includes/init.php';
		
		ob_start();
		echo '$_SERVER[QUERY_STRING] = ' . $_SERVER['QUERY_STRING'] . "\n";
		echo '$_GET = ' . print_r($_GET, true) . "\n";
		echo '$_POST = ' . print_r($_POST, true) . "\n";
		echo 'Raw POST = ' . var_dump(file_get_contents('php://input')) . "\n";
		$log = ob_get_clean();
		
		hw_error_log($log);//, 1, 'howang@yohohongkong.com');
		exit;
	}
	else if ($act == 'referral_coupon')
	{
		define('IN_ECS',true);
		require dirname(__FILE__) . '/includes/init.php';
		require dirname(__FILE__) . '/includes/lib_order.php';
		
		// howang's user_id on beta = 11347, on production = 4484
		$user_id = (strpos($_SERVER['SERVER_NAME'], 'beta.yohohongkong.com') !== false) ? 11347 : 4484;
		$user = user_info($user_id);
		
		// Generate coupon_code
		$allchars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$coupon_code = '';
		for ($i = 0; $i < 8; $i++)
		{
			$coupon_code .= $allchars[mt_rand(0, strlen($allchars) - 1)];
		}
		
		$coupon = array(
			'coupon_name' => 'Referral Coupon - ' . $user['user_name'],
			'coupon_code' => $coupon_code,
			'coupon_amount' => 10,
			'coupon_discount' => 0,
			'goods_list' => '',
			'users_list' => '',
			'min_goods_amount' => 0,
			'reusable' => 0,
			'start_time' => gmtime(),
			'end_time' => 2147483647,
			'creator' => 'howang',
			'referer_id' => $user_id
		);
		$db->autoExecute($ecs->table('coupons'), $coupon, 'INSERT');
		
		echo 'Coupon Code: ' . $coupon_code;
		exit;
	}
	else if ($act == 'clear_cache')
	{
		define('IN_ECS',true);
		require dirname(__FILE__) . '/includes/init.php';
		clear_all_files();
		echo 'All cache cleared';
		exit;
	}
	else if ($act == 'stream_transports')
	{
		header('Content-Type: text/plain;charset=utf-8');
		$xportlist = stream_get_transports();
		print_r($xportlist);
		exit;
	}
	echo 'Hello, howang!';
?>