<?php
/**
* YOHO Lucky Draw page
* 20180731
*/
define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

if ((DEBUG_MODE & 2) != 2) {
    $smarty->caching = true;
}

// check login
 
if (!empty($_SESSION['user_id'])) {
    // redirect to confirm info page;
	//header('Location: /luckydraw-info-confirm');
	//exit;
}else{
    // header('Location: /luckydraw-logged_in');
	// exit;
}

$user_id = $_SESSION['user_id'];

if (empty($user_id)) {
    $logged_in = 0; 
} else {
    $logged_in = 1;
}

$user_rank = $_SESSION['user_rank'];
$rank_points = $_SESSION['rank_points'];

$luckyDrawController = new Yoho\cms\Controller\LuckyDrawController();
$is_lucky_draw_active = $luckyDrawController->isLuckyDrawEventActive();

if (!$is_lucky_draw_active) {
    // get the last event id
    $lucky_draw_id = $luckyDrawController->getTheLastFinishedLuckyDrawEvent();
    //echo 'error - event not ready';
} else {
    $lucky_draw_id = $luckyDrawController->getTheFirstActiveLuckyDrawEvent();
}

if (empty($lucky_draw_id)) {
	header('Location: /');
	exit;
}

$user_ticket_count = $luckyDrawController->getDrawTicketCount($user_id,$lucky_draw_id);
$lucky_draw_info = $luckyDrawController->getLuckyDrawEventInfo($lucky_draw_id);
$is_gift_enough = $luckyDrawController->checkGiftEnough($lucky_draw_id);
$is_hold_lucky_draw = $luckyDrawController->isHoldLuckyDraw($lucky_draw_id);

// echo '<pre>';
// print_r($lucky_draw);
// exit;
/* 赋值固定内容 */
assign_template();
$position = assign_ur_here(0);

$smarty->assign('logged_in', $logged_in);
$smarty->assign('page_title', $position['title']);    // 页面标题
$smarty->assign('ur_here',    $position['ur_here']);  // 当前位置

$smarty->assign('helps', get_shop_help());              // 网店帮助
$smarty->assign('lucky_draw_info', $lucky_draw_info); 
$smarty->assign('is_lucky_draw_active', $is_lucky_draw_active);
$smarty->assign('is_gift_enough', $is_gift_enough);
$smarty->assign('is_hold_lucky_draw', $is_hold_lucky_draw);

$smarty->assign('user_ticket_count', $user_ticket_count);
$smarty->assign('user_id', $user_id);
$smarty->display('lucky_draw.dwt');
