<?php

/***
 * pos_new.php
 * modified from pos.php by yan 2019-07-25
 *
 * POS System for YOHO Hong Kong
 ***/

define('IN_ECS', true);

define('DESKTOP_ONLY', true); // This page don't have mobile version

define('FORCE_HTTPS', true); // Require HTTPS for security

define('IS_POS', true);

require(dirname(__FILE__) . '/includes/init.php');

require(ROOT_PATH . 'includes/lib_order.php');
require(ROOT_PATH . 'includes/lib_order_1.php');

$_CFG['lang'] = 'zh_tw'; // only TC version for pos

/* 载入语言文件 */
require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/user.php');
require_once(ROOT_PATH . 'languages/' .$_CFG['lang']. '/shopping_flow.php');

$paymentController = new Yoho\cms\Controller\PaymentController();
$orderController = new Yoho\cms\Controller\OrderController();
$addressController = new Yoho\cms\Controller\AddressController();
$userController = new Yoho\cms\Controller\UserController();
$rspController = new Yoho\cms\Controller\RspController();
$shippingController = new \Yoho\cms\Controller\ShippingController();
$erpController = new Yoho\cms\Controller\ErpController();
$packageController = new Yoho\cms\Controller\PackageController();
$platform = $orderController::ORDER_FEE_PLATFORM_POS;
$actg = $paymentController->actg;
$actgHooks = $paymentController->actgHooks;

$smarty->assign('lang',	$_LANG);

if ((DEBUG_MODE & 2) != 2)
{
	$smarty->caching = true;
}

$_SESSION['user_id'] = 0;

// Check admin login
$admin = check_admin_login();
if (!$admin)
{
	if (!empty($_REQUEST['from_app']))
	{
		setcookie('login_from_pos', '1', 0, '/', false, true, true);
		header('Location: /x/privilege.php?act=login');
		exit;
	}
	
	show_message($_LANG['not_login'], $_LANG['back_home'], $ecs->url(), 'error');
	exit;
}
$smarty->assign('admin', $admin);

$action = empty($_REQUEST['act']) ? '' : $_REQUEST['act'];
$mode = empty($_REQUEST['mode']) ? 'normal' : $_REQUEST['mode'];
$smarty->assign('mode', $mode);

/*------------------------------------------------------ */
//-- ajax api
/*------------------------------------------------------ */
switch ($action) {
	case 'get_init_data':

		$shop_warehouse_id = $erpController->getShopWarehouseId();
		$warehouse_list = $erpController->getWarehouseListByShopId($shop_warehouse_id);

		output_json(array(
			'status' => 1,
			'available_countries_code' => available_countries_code(),
			'country_list' => available_countries(),
			'warehouse_list' => $warehouse_list
		));
		break;
	case 'validate_account': // Check if account exists, and return account information
		$user_name = empty($_REQUEST['user_name']) ? '' : trim($_REQUEST['user_name']);
		
		if (empty($user_name))
		{
			output_json(array(
				'status' => 0,
				'user_name' => $user_name,
				'error' => '請輸入會員ID'
			));
		}
		
		$sql = "SELECT user_id FROM ". $ecs->table('users')."  WHERE user_name = '$user_name' LIMIT 1";
		$user_id = $db->getOne($sql);
		
		$_SESSION['user_id'] = ($user_id) ? $user_id : 0;

		if (!$user_id)
		{
			output_json(array(
				'status' => 0,
				'user_name' => $user_name,
				'error' => '查無此會員ID'
			));
		}
		
		$user = user_info($user_id);
		
		$ws_company = "";
		if (!empty($user['wsid'])) {
			$ws_company = $db->getOne("SELECT company FROM " . $ecs->table('wholesale_customer') . " WHERE wsid = $user[wsid]");
		}

		output_json(array(
			'status' 			=> 1,
			'user_id' 			=> $user_id,
			'user_name' 		=> $user_name,
			'email' 			=> $user['email'],
			'user_money' 		=> ($user['user_money'] + $user['credit_line']), // 餘額 + 信用額
			'pay_points' 		=> $user['pay_points'], // 積分
			'is_wholesale' 		=> $user['is_wholesale'],
			'ws_customer_id' 	=> $user['wsid'], // 批發客ID
			'ws_company' 		=> $ws_company, // 批發客公司
			'user_rank'    		=> $user['user_rank'],
			'rank_name'    		=> $user['rank_name'],
			'avatar'			=> insert_avatar_url(['id' => $user_id, 'size' => 's']),
		));
		break;

	case 'get_goods_by_sn': // Retrieve Goods Information with Barcode

		$goods_sn = empty($_REQUEST['goods_sn']) ? '' : trim($_REQUEST['goods_sn']);
		$user_rank = empty($_REQUEST['user_rank']) ? 0 : intval($_REQUEST['user_rank']);

		if (empty($goods_sn))
		{
			output_json(array(
				'status' => 0,
				'goods_sn' => $goods_sn,
				'error' => '請輸入 Barcode'
			));
		}
		
		$sql = "SELECT goods_id FROM " . $ecs->table("goods") . " WHERE goods_sn ='$goods_sn' AND is_on_sale = 1 AND is_alone_sale = 1 AND is_delete = 0 LIMIT 1";
		$goods_ids = $db->getCol($sql);
		
		if (empty($goods_ids))
		{
			// Try searching by name
			$search = '(' . implode(' AND ', array_map(function ($keyword) { return "goods_name LIKE '%" . mysql_like_quote($keyword) . "%'"; } , preg_split('/\s+/', $goods_sn))) . ')';
			$sql = "SELECT goods_id FROM " . $ecs->table("goods") . " WHERE " . $search . " AND is_on_sale = 1 AND is_alone_sale = 1 AND is_delete = 0";
			if($_REQUEST['order_mode'] == 'normal') $sql .= " AND (goods_sn = goods_id OR LOWER(goods_sn) = CONCAT('no', goods_id)) "; //  We only can search by goods_name if goods is no barcode.(No barcode : goods_id = goods_sn)
			$goods_ids = $db->getCol($sql);
			
			if (empty($goods_ids))
			{
				output_json(array(
					'status' => 0,
					'goods_sn' => $goods_sn,
					'error' => '沒有產品符合您輸入的 Barcode: ' . $goods_sn
				));
			}
		}
		
		$goods_list = array();
		foreach ($goods_ids as $goods_id)
		{
			// get_goods_info() declared in lib_goods.php, which is loaded by lib_order.php already
			$goods = get_goods_info($goods_id, true);

			// get the stock of the goods
			$stocks_inquiry = stocks_inquiry($goods['goods_id']);

			// Get price
			$final_price     = get_final_price($goods_id, 1, true, array(), $user_rank);
			$shop_price      = $final_price['final_price'];
			$goods_user_rank = $final_price['user_rank_id'];
			$youhui_price = $row['shop_price'] > $shop_price ? ($row['shop_price'] - $shop_price) : 0;
			
			// Create response object
			$goodsOutput = array(
				'goods_id' => $goods['goods_id'],
				'goods_name' => $goods['goods_name'],
				'goods_sn' => $goods['goods_sn'],
				'goods_img' => $goods['goods_thumb'],
				'shop_price' => $shop_price,
				'youhui_price' => $youhui_price,
				//'goods_number' => $goods['goods_number'],
				'goods_number' => $goods['goods_number_shop'][1],  // we dont count on FLS stock
				'goods_number_str' => $goods['goods_number_sn'],
				'goods_user_rank' => $goods_user_rank,
				'stocks_inquiry' => $stocks_inquiry
			);

			$goods_list[] = $goodsOutput;
		}
		
		output_json(array(
			'status' => 1,
			'goods_sn' => $goods_sn,
			'goods_list' => $goods_list
		));
		
		break;
	
	case 'add_to_cart': // Add Goods to Cart
		/* 取得購物類型 */
		$flow_type = isset($_SESSION['flow_type']) ? intval($_SESSION['flow_type']) : CART_GENERAL_GOODS;
		$smarty->assign('config', $_CFG);
		// $user_info = user_info($_SESSION['user_id']);
		$user_rank = empty($_REQUEST['user_rank']) ? 0 : intval($_REQUEST['user_rank']);
		$shipping = empty($_REQUEST['shipping']) ? 0 : floatval($_REQUEST['shipping']);
		$payment = empty($_REQUEST['payment']) ? 0 : floatval($_REQUEST['payment']);
		$is_gift = empty($_REQUEST['is_gift']) ? 0 : intval($_REQUEST['is_gift']);
		$is_package = empty($_REQUEST['is_package']) ? 0 : intval($_REQUEST['is_package']);
		$clean_integral = isset($_REQUEST['clean_integral']) ? intval($_REQUEST['clean_integral']) : 1;
		
		$goods_id = empty($_REQUEST['goods_id']) ? 0 : $_REQUEST['goods_id']; /*商品ID*/
		$goods_number = isset($_REQUEST['quantity']) ? intval($_REQUEST['quantity']) : 0;
		$spec = array();

		$is_redeem = isset($_REQUEST['is_redeem']) ? $_REQUEST['is_redeem'] : '0';

		if( $is_redeem != 1) {
			//ungroup package not available
			$orderController->auto_ungroup_package($platform);
			
			// Auto group product to package
			$orderController->auto_group_package($platform);
			
			/* delete all Favourable / Package product which not available */
			$orderController->check_cart_favourable_goods($user_rank, $_SESSION['user_id'], $platform);
		}


		if(!empty($goods_id))
		{
			/* 檢查：商品數量是否合法 */
			if (!is_numeric($goods_number))
			{
				output_json(array(
					'status' => 0,
					'goods_id' => $goods_id,
					'goods_number' => $goods_number,
					'error' => $_LANG['invalid_number']
				));
			}
			/* 更新：購物車 */
			else
			{
				if ($goods_number > 0) // This step only handle normal goods, not include gift goods.
				{
					/* 取得商品信息 */
					$sql = "SELECT g.goods_id, g.goods_name, g.goods_sn, g.give_integral, g.is_on_sale, g.is_real,g.integral, ".
						"g.market_price, g.shop_price AS org_price, g.promote_price, g.promote_start_date, ".
						"g.promote_end_date, g.goods_weight, g.integral, g.extension_code, ".
						"g.goods_number, g.is_alone_sale, g.is_shipping,".
						" g.shop_price * '$_SESSION[discount]' AS shop_price ".
					" FROM " .$GLOBALS['ecs']->table('goods'). " AS g ".
					" WHERE g.goods_id = '$goods_id'" .
					" AND g.is_delete = 0";

					// $sql = "SELECT g.goods_id, g.goods_name, g.goods_sn, g.give_integral, g.is_on_sale, g.is_real,g.integral, ".
					// 	"g.market_price, g.shop_price AS org_price, g.promote_price, g.promote_start_date, ".
					// 	"g.promote_end_date, g.goods_weight, g.integral, g.extension_code, ".
					// 	"g.goods_number, g.is_alone_sale, g.is_shipping,".
					// 	"IFNULL(mp.user_price, g.shop_price * '$_SESSION[discount]') AS shop_price ".
					// " FROM " .$GLOBALS['ecs']->table('goods'). " AS g ".
					// " LEFT JOIN " . $GLOBALS['ecs']->table('member_price') . " AS mp ".
					// 		"ON mp.goods_id = g.goods_id AND mp.user_rank = '$user_rank' ".
					// " WHERE g.goods_id = '$goods_id'" .
					// " AND g.is_delete = 0";
					
					$goods = $GLOBALS['db']->getRow($sql);

					/* 計算商品的促銷價格 */
					$spec_price             = spec_price($spec);
					$final_price     = get_final_price($goods_id, $goods_number, true, $spec, 0);
					$goods_price     = $final_price['final_price'];
					$goods_user_rank = $final_price['user_rank_id'];
					$goods['market_price'] += $spec_price;
					$goods_attr             = get_goods_attr_info($spec);
					$goods_attr_id          = join(',', $spec);
					
					$goods['user_id']       = $_SESSION['user_id'];
					$goods['goods_price']   = $goods_price;
					$goods['goods_attr']    = $goods_attr;
					$goods['parent_id']     = 0;
					$goods['is_gift']       = 0;
					$goods['is_package']    = 0;
					$goods['is_shipping']   = 0;
					$goods['goods_number']  = $goods_number;
					$goods['subtotal']      = $goods_price * $goods_number;
					$goods['session_id']    = SESS_ID;
					$goods['user_rank']     = $goods_user_rank;
					//echo "1";die("DELETE FROM ".$GLOBALS['ecs']->table('cart1'). " WHERE goods_id=$goods_id AND is_gift=0 AND is_package=$is_package AND session_id='".SESS_ID."'");
					$GLOBALS['db']->query("DELETE FROM ".$GLOBALS['ecs']->table('cart1'). " WHERE goods_id=$goods_id AND is_gift=0 AND is_package=$is_package AND session_id='".SESS_ID."'");
					$GLOBALS['db']->autoExecute($GLOBALS['ecs']->table('cart1'), addslashes_deep($goods), 'INSERT');
					/* 保存購物信息 */
				}
				else
				{
					// goods_number = 0: remove from cart
					//echo "2";die("DELETE FROM ".$GLOBALS['ecs']->table('cart1'). " WHERE goods_id=$goods_id AND is_gift=$is_gift AND is_package=$is_package AND session_id='".SESS_ID."'");
					$GLOBALS['db']->query("DELETE FROM ".$GLOBALS['ecs']->table('cart1'). " WHERE goods_id=$goods_id AND is_gift=$is_gift AND is_package=$is_package AND session_id='".SESS_ID."'" );
					if($is_package){
						$GLOBALS['db']->query("DELETE FROM ".$GLOBALS['ecs']->table('cart1'). " WHERE is_package=$is_package AND session_id='".SESS_ID."'" );
					}
				}
			}
		}
		// Update order type
		$order_type = isset($_REQUEST['order_mode']) ? $_REQUEST['order_mode'] : 'normal';
		if($order_type == 'sales-agent' || $order_type == 'wholesale' || $is_redeem == 1 || $order_type == 'replace') { 
			/* 把赠品删除 */
			$sql = "DELETE FROM " . $GLOBALS['ecs']->table('cart1') . " WHERE session_id = '" . SESS_ID . "' AND is_gift > 0";
			$GLOBALS['db']->query($sql);
		}

		if( $is_redeem != 1) {
			// Auto group product to package
			$orderController->auto_group_package($platform);
		} else {
			$sql = "DELETE FROM " . $GLOBALS['ecs']->table('cart1') . " WHERE session_id = '" . SESS_ID . "' AND is_package > 0";
			$GLOBALS['db']->query($sql);
		}
		
		$cart_goods = get_cart_goods_1($user_rank, -1, false);

		usort($cart_goods['goods_list'], function ($a, $b) { return intval($a['rec_id']) - intval($b['rec_id']); });
		$goods_list = array();
		$packages = array();
		$in_cart_goods_ids = [];
		foreach ($cart_goods['goods_list'] as $goods) {
			$in_cart_goods_ids[] = $goods['goods_id'];
		}
		foreach ($cart_goods['goods_list'] as $goods)
		{
			// get the stock of the goods
			$stocks_inquiry = stocks_inquiry($goods['goods_id']);

			if ($goods['extension_code'] == 'package')
			{
				$sql = "SELECT  goods_number, goods_price FROM " . $ecs->table('package_goods') .
						"WHERE package_id = " . $goods['is_package'] . 
						" AND goods_id = " . $goods['goods_id'];
				$package_goods = $GLOBALS['db']->getRow($sql);


				if (!in_array($goods['is_package'], array_keys($packages))) {
					$now = gmtime();
					$sql = "SELECT ga.act_id, ga.act_name, ga.act_desc, ga.start_time, ga.end_time,
							 ga.is_finished, ga.ext_info, gal.act_name as act_name_i18n 
							FROM " . $ecs->table('goods_activity') . " AS ga left join ".$ecs->table('goods_activity_lang')." gal on (ga.act_id = gal.act_id) and gal.lang = '" .$_CFG['lang'] . "' 
							WHERE ga.act_id = " . $goods['is_package'] . " 
							AND ga.start_time <= '" . $now . "' 
							AND ga.end_time >= '" . $now ."'";

					$package = $GLOBALS['db']->GetRow($sql);
					$packages[$goods['is_package']] = $package;

					$packages[$goods['is_package']]['qty'] = $goods['goods_number'] / $package_goods['goods_number'];
					$packages[$goods['is_package']]['base_price'] = $packageController->get_package_total_price($goods['is_package']);
					$packages[$goods['is_package']]['total_price'] = price_format(bcmul($packages[$goods['is_package']]['qty'], $packages[$goods['is_package']]['base_price'], 2), false);
				}

				$goods_info = array(
					'goods_id' => $goods['goods_id'],
					'goods_name' => $goods['goods_name'],
					'goods_sn' => $goods['goods_sn'],
					'goods_img' => $goods['goods_thumb'],
					'package_id' => $goods['is_package'],
					'has_require_goods' => empty($goods['require_goods_id']) || in_array($goods['require_goods_id'], $in_cart_goods_ids) ? 0 : 1,
					'stocks_inquiry' => $stocks_inquiry
				);

				$package_goods = array_merge($goods_info, $package_goods);
				$packages[$goods['is_package']]['goods_list'][] = $package_goods;
			}


			$goods_list[] = array(
				'goods_id' => $goods['goods_id'],
				'goods_name' => $goods['goods_name'],
				'goods_number' => $goods['goods_number'],
				'goods_price' => $goods['goods_price'],
				'goods_price_normal' => $goods['goods_price_normal'],
				'goods_sn' => $goods['goods_sn'],
				'goods_img' => $goods['goods_thumb'],
				'subtotal' => $goods['subtotal'],
				'is_vip' => $goods['is_vip'],
				'is_gift' => $goods['is_gift'],
				'is_package' => $goods['is_package'],
				'is_auto_add' => $goods['is_auto_add'],
				'has_require_goods' => empty($goods['require_goods_id']) || in_array($goods['require_goods_id'], $in_cart_goods_ids) ? 0 : 1,
				'stocks_inquiry' => $stocks_inquiry
			);
		}
		
		/*
		* 取得訂單信息
		*/
		$order = flow_order_info();
		
		// pos will trigger add_to_cart with goods_id = 0 and goods_number = 0 on page load
		// If integral is set but didn't checkout, we have to clear it so it won't carry over
		if (empty($goods_id) && empty($goods_number) && $clean_integral != 2)
		{
			$order['integral'] = 0;
			$order['coupon_id'] = array();
			if (isset($order['exclusive_coupon'])) unset($order['exclusive_coupon']);
		}
		
		// Update shipping / payment method
		$order['shipping_id'] = $shipping;
		$order['pay_id'] = $payment;
		
		// Update order type
		$order_type = isset($_REQUEST['order_mode']) ? $_REQUEST['order_mode'] : 'normal';
		if($order_type == 'sales-agent') { 
			$order['order_type']   = Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT;
		} elseif($order_type == 'wholesale') { 
			$order['order_type'] = Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE;
		}
		else {
			$order['order_type'] = NULL;
		}
		
		$smarty->assign('order', $order);
		
		$consignee['country'] = isset($_REQUEST['country']) ? intval($_REQUEST['country']) : 0;
		$consignee['province'] = isset($_REQUEST['province']) ? intval($_REQUEST['province']) : 0;
		$consignee['city'] = isset($_REQUEST['city']) ? intval($_REQUEST['city']) : 0;
		$consignee['district'] = isset($_REQUEST['district']) ? intval($_REQUEST['district']) : 0;

		/*
		* 計算訂單的費用
		*/
		$cart_goods = cart_goods_1($flow_type, null, null, $user_rank, -1, false); // 取得商品列表，計算合計
		$total = $orderController->order_fee_flow($order, $cart_goods, $consignee, $platform);
		$smarty->assign('total', $total);
		
		/* 如果訂單中有固定運費產品，只送香港 */
		$restricted_to_hk_goods_ids = [];
		if($shipping != 2) {
			$free_region = $addressController->get_free_calculate_region();
			$cal_free_shipping = false;
			if (!empty($free_region['country'] && in_array($consignee['country'], $free_region['country']))) $cal_free_shipping = true;
			$cart_weight_price = cart_weight_price_1(CART_GENERAL_GOODS, $cal_free_shipping);
	
			if (($cart_weight_price['price'] > 0) && ($consignee['country'] != 3409)) {
				// Clear shipping_list
				$shipping_list = array();
	
				$sql = "SELECT c.goods_id " .
						"FROM " . $ecs->table('cart1') . " as c " .
						"LEFT JOIN " . $ecs->table('goods') . " as g ON g.goods_id = c.goods_id " .
						"WHERE c.session_id = '" . SESS_ID . "' " .
						"AND rec_type = '$flow_type' AND g.is_shipping = 0 AND g.shipping_price > 0 AND c.extension_code != 'package_buy'";
				$restricted_to_hk_goods_ids = $db->getCol($sql);
			}
		}
		
		$_SESSION['flow_order'] = $order;
		
		// 檢查是否包括需訂貨產品
		$preorder_msg = '';
		if (!empty($cart_goods))
		{
			$goods_preorder = check_goods_preorder($cart_goods);
			if (!empty($goods_preorder))
			{
				foreach ($goods_preorder as $preorder)
				{
					$preorder_msg .= $preorder['goods_name'] . ' 訂貨大約' . $preorder['pre_sale_days'] . '工作天' . "\n";
				}
			}
		}
		$need_removal_list = $rspController->needRemovalGoods($cart_goods);

		output_json(array(
			'status' => 1,
			'cart_list' => array(
				'goods_list' => $goods_list,
				'package_list' => $packages,
			),
			'total_amount' => $total['amount'],
			'order_total' => $smarty->fetch('library/pos_order_total_new.lbi'),
			'preorder_msg' => $preorder_msg,
			'need_removal' => $need_removal_list,
			'restricted_to_hk_goods_ids' => $restricted_to_hk_goods_ids,
		));

		break;

	case 'load_favourable': // ajax: handle favourable goods
		extract($_REQUEST);
		$favourable_list = $orderController->favourable_list($user_rank, $user_id, $platform);

		foreach($favourable_list as $favourable_index => $favourable) {
			foreach($favourable['gift'] as $index => $goods) {
				// get the stock of the goods
				$stocks_inquiry = stocks_inquiry($goods['goods_id']);
				$favourable_list[$favourable_index]['gift'][$index]['stocks_inquiry'] = $stocks_inquiry;
			}
		}

		if (isset($_SESSION['pos_update_cart'])) {
			$need_to_update_cart = true;
			unset($_SESSION['pos_update_cart']);
		} else {
			$need_to_update_cart = !$orderController->check_cart_favourable_goods($user_rank, $user_id, $platform);
		}
		output_json(array(
			'status' => 1,
			'favourable_list' => $favourable_list,
			'need_update' => $need_to_update_cart
		));
		break;

	case 'add_favourable': // ajax: add favourable goods
		extract($_REQUEST);

		$res = $orderController->add_favourable_goods($goods_id, $quantity, $act_id, $user_rank, $user_id, $platform);
		if($res){
			$info = $orderController->get_pos_order_info($user_rank);
			$goods_list = $info['goods_list'];
			$total = $info['total'];
			$smarty->assign('total', $total);
			output_json(array(
				'status' => 1,
				'cart_list' => $goods_list,
				'goods_amount' => $total['goods_price_formated'],
				'order_total' => $smarty->fetch('library/pos_order_total_new.lbi')
			));
		} else {
			output_json(array(
				'status' => 0,
				'error' => '添加優惠失敗!'
			));
		}
		break;
	
	case 'load_requiregoods': // ajax: handle require goods
		extract($_REQUEST);
		$require_goods_list = [];

		// get all goods_id in cart
		$in_cart_goods =  $db->getAll("SELECT goods_id, goods_name FROM " . $ecs->table("cart1") . " WHERE session_id = '" . SESS_ID . "'");

		foreach ($in_cart_goods as $cart_goods) {
			$in_cart_goods_id[] = $cart_goods['goods_id'];
		}
		foreach ($in_cart_goods as $cart_goods) {
			$require_goods_id = $db->getOne("SELECT require_goods_id FROM " . $ecs->table("goods") . " WHERE goods_id = $cart_goods[goods_id]");
			$goods_list = [];

			if (!empty($require_goods_id) && !in_array($require_goods_id, $in_cart_goods_id)) {
				// get_goods_info() declared in lib_goods.php, which is loaded by lib_order.php already
				$goods = get_goods_info($require_goods_id, true);

				// Get price
				$shop_price  = get_final_price($require_goods_id, 1, true, array(), $user_rank)['final_price'];
				$youhui_price = $row['shop_price'] > $shop_price ? ($row['shop_price'] - $shop_price) : 0;

				// Create response object
				$goodsOutput = array(
					'goods_id' => $goods['goods_id'],
					'goods_name' => $goods['goods_name'],
					'goods_sn' => $goods['goods_sn'],
					'goods_img' => $goods['goods_thumb'],
					'shop_price' => $shop_price,
					'youhui_price' => $youhui_price,
					'goods_number' => $goods['goods_number'],
					'goods_number_str' => $goods['goods_number_sn'],
					'formatted_shop_price' => price_format($shop_price),
					'stocks_inquiry' => stocks_inquiry($goods['goods_id']),
				);

				$goods_list[] = $goodsOutput;
			}
			if (!empty($goods_list)) {
				$require_goods_list[] = [
					'goods_id' => $cart_goods['goods_id'],
					'goods_name' => $cart_goods['goods_name'],
					'goods_list' => $goods_list,
				];
			}
		}

		output_json(array(
			'status' => 1,
			'require_goods_list' => $require_goods_list,
			'in_cart_goods' => $in_cart_goods_id
		));
		break;

	case 'load_packages':
		extract($_REQUEST);
		$goods_list = $orderController->get_normal_goods_from_cart($platform);

        $package_arr = array();
        foreach($goods_list as $goods_id => $goods){
			$package_goods_list = $packageController->get_package_goods_list($goods_id);
			foreach($package_goods_list as $package_goods) {
				if(!in_array($package_goods['act_id'], array_column($package_arr, 'act_id'))) {
					foreach($package_goods['goods_list'] as $goods_index => $goods){
                        // get the stock of the goods
						$stocks_inquiry = stocks_inquiry($goods['goods_id']);
						$package_goods['goods_list'][$goods_index]['stocks_inquiry'] = $stocks_inquiry;
                    }

					array_push($package_arr, $package_goods);
				}
			}
		}
		
		output_json(array(
			'status' => 1,
			'packages_list' => $package_arr,
		));

		break;
	case 'add_package_to_cart':
		extract($_REQUEST);

		if(!empty($goods_id)) {
			/* 取得商品信息 */
			$sql = "SELECT c.goods_number FROM `ecs_cart1` as c WHERE `goods_id`=$goods_id AND `session_id` = '".SESS_ID."' AND `is_package` = 0";
			$goods_number = $GLOBALS['db']->getOne($sql);
			if($goods_number > $quantity) {
				$goods_number -= $quantity;
				$goods = array('goods_number' => $goods_number);
				$res = $GLOBALS['db']->autoExecute($GLOBALS['ecs']->table('cart1'), $goods, 'UPDATE', 'goods_id = ' . $goods_id .' AND session_id = "'.SESS_ID.'"');

			} else {
				$GLOBALS['db']->query("DELETE FROM ".$GLOBALS['ecs']->table('cart1'). " WHERE goods_id=$goods_id AND is_gift=0 AND is_package=0 AND session_id='".SESS_ID."'");
			}
		}

		$res = add_package_to_cart($package_id, [], $quantity, 'cart1');
		if($res){
			$info = $orderController->get_pos_order_info($user_rank);
			$goods_list = $info['goods_list'];
			$total = $info['total'];
			$smarty->assign('total', $total);
			output_json(array(
				'status' => 1,
				'cart_list' => $goods_list,
				'goods_amount' => $total['goods_price_formated'],
				'order_total' => $smarty->fetch('library/pos_order_total_new.lbi')
			));
		} else {
			output_json(array(
				'status' => 0,
				'error' => '添加套裝優惠失敗!'
			));
		}
		break;
	case 'update_shipping_country':
		$shipping_id = empty($_REQUEST['shipping']) ? 3 : $_REQUEST['shipping'];
		if($shipping_id == 99) $shipping_id = 0;

		$country_list = get_shipping_available_countries($shipping_id);
		$available_coutries_code = available_countries_code();

		if($shipping_id == 3) {
			$countries_id = array_column($country_list, 'id');
			$available_coutries_code = array_filter($available_coutries_code, function($coutries_code) use ($countries_id) {
				return in_array($coutries_code, $countries_id);
			});
		}

		output_json(array(
			'status' => 1,
			'shipping_id' => $shipping_id,
			'available_countries_code' => $available_coutries_code,
			'country_list' => $country_list
		));
		break;
	case 'select_shipping_type': 
		$code				= $_REQUEST['code'];
		$step               = empty($_REQUEST['step'])? 'shipping_type' : $_REQUEST['step'];
		$data               = $_REQUEST['data'];
		$user_id            = $_REQUEST['user_id'];
		$nextStep 			= false;

		if($code == 'express') {
			/* 1. Get user address */
			/* 2. Base on user address to display shipping modules */
			/* 3. Base on shipping modules to display payment modules */
			switch($step) {
				case 'shipping_type':
					$sql = "SELECT address_id ".
						"FROM " . $GLOBALS['ecs']->table('users') .
						"WHERE user_id = '" . $user_id . "'";
					$selected_address = $GLOBALS['db']->getOne($sql);
					$address = $userController->get_user_address_list($user_id);
					$nextStep = ['step' => $step, 'stepName' => 'address', 'list' => $address, 'default' => $selected_address];
					break;
				case 'address':
					if($user_id > 0 && isset($data['address_id'])) {
						$res = $userController->set_user_address_list($user_id, $data['address_id']);
					}
					$consignee = $data['consignee'];
					$shipping = $shippingController->getFlowShippingList($code, $consignee, $platform);
					$nextStep = ['step' => $step, 'stepName' => 'shipping', 'data' => $shipping];
					break;
			}
		}
		else if($code == 'pickuppoint') {
			/* 1. Get this type shipping modules */
            /* 2. Base on shipping modules to display address list (module->pickuplist) */
            /* 3. Base on shipping modules to display payment modules */
            switch($step) {
				case 'shipping_type':
					$shipping = $shippingController->getFlowShippingList($code, [], $platform);
                    $nextStep =  ['step' => $step, 'stepName' => 'shipping', 'data' => $shipping];
                    break;
                case 'address':
                    $res = $paymentController->getFlowPaymentList($user_id, $data);
                    if(!is_null($res)) {
                        $nextStep =  ['step' => $step, 'stepName' => 'payment', 'data' => $res];
                    }
                    break;
				case 'shipping':
					$res = $shippingController->getShippingAreaAddressList($data, $user_id);
                    $nextStep = ['step' => $step, 'stepName' => 'address', 'list' => $res['group_list'], 'default' => $res['locker']];
                    break;
            }
		}
		output_json(array('status' => 1, 'nextStep' => $nextStep));
		break;
	case 'calculate_amount':
		$user_name = empty($_REQUEST['user_name']) ? '' : trim($_REQUEST['user_name']);
		$shipping = empty($_REQUEST['shipping']) ? 0 : intval($_REQUEST['shipping']);
		$shipping_area = empty($_REQUEST['shipping_area']) ? 0 : intval($_REQUEST['shipping_area']);
		$payment = empty($_REQUEST['payment']) ? 0 : intval($_REQUEST['payment']);
		
		$integral = empty($_REQUEST['integral']) ? 0 : intval($_REQUEST['integral']);
		$discount = empty($_REQUEST['discount']) ? 0 : floatval($_REQUEST['discount']);
		$coupon_code = empty($_REQUEST['coupon']) ? array() : explode(',',trim($_REQUEST['coupon']));
		$deposit = empty($_REQUEST['deposit']) ? 0 : floatval($_REQUEST['deposit']);
		$promote_list = array();
		
		/* 對商品信息賦值 */
		$cart_goods = cart_goods_1(null, null, $user_name, 0, -1, false); // 取得商品列表，計算合計

		if (empty($cart_goods))
		{
			output_json(array(
				'status' => 0,
				'error' => '您的訂單中沒有任何產品'
			));
		}


		if ($integral < 0)
		{
			output_json(array(
				'status' => 0,
				'integral' => $integral,
				'error' => '使用積分不能少於 0！'
			));
		}

		
		if (!empty($user_name))
		{
			$sql = "SELECT user_id FROM ". $ecs->table('users')."  WHERE user_name = '$user_name' LIMIT 1";
			$user_id = $db->getOne($sql);
			
			if (!$user_id)
			{
				output_json(array(
					'status' => 0,
					'user_name' => $user_name,
					'error' => '查無此會員ID'
				));
			}
			
			$user = user_info($user_id);
		}
		
		// Integral is used, validate it
		if ($integral > 0)
		{
			// Must provide user name to use integral
			if (empty($user_name))
			{
				output_json(array(
					'status' => 0,
					'user_name' => $user_name,
					'error' => '請輸入會員ID'
				));
			}
			
			if ($integral > $user['pay_points'])
			{
				output_json(array(
					'status' => 0,
					'integral' => $integral,
					'pay_points' => $user['pay_points'],
					'error' => '使用積分('.$integral.')大於會員擁有的積分('.$user['pay_points'].')！'
				));
			}
			
			// 该订单允许使用的积分
			$flow_points = flow_available_points_1();
			if ($integral > $flow_points)
			{
				output_json(array(
					'status' => 0,
					'integral' => $integral,
					'flow_points' => $flow_points,
					'error' => '使用積分('.$integral.')大於訂單允許使用的積分('.$flow_points.')！'
				));
			}
		}
		
		if ($discount == 0)
		{
			output_json(array(
				'status' => 0,
				'discount' => $discount,
				'vardiscount' => 1,
				'error' => '折扣不能設為 0！'
			));
		}
		
		/* 取得購物類型 */
		$flow_type = isset($_SESSION['flow_type']) ? intval($_SESSION['flow_type']) : CART_GENERAL_GOODS;

		/* 獲得收貨人信息 */
		$consignee['country'] = isset($_REQUEST['country']) ? intval($_REQUEST['country']) : 0;
		$consignee['province'] = isset($_REQUEST['province']) ? intval($_REQUEST['province']) : 0;
		$consignee['city'] = isset($_REQUEST['city']) ? intval($_REQUEST['city']) : 0;
		$consignee['district'] = isset($_REQUEST['district']) ? intval($_REQUEST['district']) : 0;
		
		/* 取得購物流程設置 */
		$smarty->assign('config', $_CFG);
		
		/* 取得訂單信息 */
		$order = flow_order_info();
		
		// Apply Shipping / Payment method if provided
		if (!empty($shipping))
		{
			$order['shipping_id'] = $shipping;
		}
		$order['shipping_area_id'] = $shipping_area;
		if (!empty($payment))
		{
			$order['pay_id'] = $payment;
		}
		
		// Reset coupons before calculation
		$order['coupon_id'] = array();
		if (isset($order['exclusive_coupon'])) unset($order['exclusive_coupon']);
		
		// Update order type
		$order_type = isset($_REQUEST['order_mode']) ? $_REQUEST['order_mode'] : 'normal';
		if($order_type == 'sales-agent'){ $order['order_type']   = Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_SALESAGENT; }
		elseif($order_type == 'wholesale'){ $order['order_type'] = Yoho\cms\Controller\OrderController::DB_ORDER_TYPE_WHOLESALE; }
		else {
			$order['order_type'] = NULL;
		}
		
		// Apply coupon if provided
		if (!empty($coupon_code))
		{
			// trim whitspace -> filter empty string -> upper case -> unique
			$coupon_code = array_unique(array_map('strtoupper', array_filter(array_map('trim', $coupon_code))));
			
			$invalid_codes = array();
			$error_msg = '';
			
			foreach ($coupon_code as $code)
			{
				$coupon = coupon_info(0, $code);
				
				$error = 0;
				if (verify_coupon($coupon, $order, cart_amount_1(true, $flow_type), $cart_goods, $user_id, $error))
				{
					// Assign coupon_id to order
					$order['coupon_id'][] = $coupon['coupon_id'];
					if (!empty($coupon['promote_id'])) {
						$promote_list[$code] = intval($coupon['promote_id']);
					}
				}
				else
				{
					$invalid_codes[] = $code;
					$error_msg .= (empty($error_msg) ? '' : '<br>') . '優惠券「' . $code . '」驗證失敗： ' . coupon_error_message($error);
				}
			}
			
			if (!empty($invalid_codes))
			{
				$valid_codes = array_filter($coupon_code, function ($c) use ($invalid_codes) { return !in_array($c, $invalid_codes); });
				output_json(array(
					'status' => 0,
					'coupon_code' => implode(',', $valid_codes),
					'error' => $error_msg
				));
			}
		}
		
		// Apply integral to use
		$order['integral'] = $integral;

		$total = $orderController->order_fee_flow($order, $cart_goods, $consignee, $platform, $discount);
		$smarty->assign('total', $total);
		
		// Handle deposit if provided
		$smarty->assign('deposit', $deposit);
		if ($deposit > 0)
		{
			$smarty->assign('deposit_formated', price_format($deposit));
			$due_amount = $total['amount'] - $deposit;
			$smarty->assign('due_amount', $due_amount);
			$smarty->assign('due_amount_formated', price_format($due_amount));
		}
		
		output_json(array(
			'status' => 1,
			'total_amount' => $total['amount'],
			'order_total' => $smarty->fetch('library/pos_order_total_new.lbi'),
			'promote_list' => $promote_list,
			'coupon_code' => implode(',', $coupon_code),
		));
		break;
	
	case 'validate_sa_goods':
		extract($_REQUEST);
		
		$goods_got_price = TRUE;
		$normal_goods_ids = [];

		if (empty($goods_ids))
		{
			output_json(array(
				'status' => 0,
				'error' => '您的訂單中沒有任何產品'
			));
		}

		foreach ($goods_ids as $goods_id){
			$good_ga_price = $orderController->getGoodGaPrice($sa_id, $goods_id);
			if (empty($good_ga_price)){
				$goods_got_price = FALSE;
				$normal_goods_ids[] = $goods_id;
			}
		}

		if(!$goods_got_price){
			output_json(array(
				'status' => 0,
				// 'error' => '未能找到訂單貨品的代理銷售價錢',
				'error' => '訂單貨品並不包含於此代理銷售中',
				'normal_goods_ids' => $normal_goods_ids,
			));
		} else {
			output_json(array(
				'status' => 1,
				'goods_ids' => $goods_ids,
			));
		}

		break;
	
	case 'get_order_by_sn':
		$order_sn = empty($_REQUEST['order_sn']) ? '' : trim($_REQUEST['order_sn']);
	
		if (empty($order_sn))
		{
			output_json(array(
				'status' => 0,
				'order_sn' => $order_sn,
				'error' => '請先輸入訂單號'
			));
		}
		
		$order = order_info(0, $order_sn); //$db->getRow("SELECT * FROM " . $ecs->table('order_info') . " WHERE order_sn = '" . $order_sn . "'");
		
		$order_id = empty($order['order_id']) ? 0 : intval($order['order_id']);
		if (!$order_id)
		{
			output_json(array(
				'status' => 0,
				'order_sn' => $order_sn,
				'error' => '查無此訂單號'
			));
		}
		
		$platforms = Yoho\cms\Controller\OrderController::DB_SELLING_PLATFORMS;
		$order['platform_display'] = isset($platforms[$order['platform']]) ? $platforms[$order['platform']] : $order['platform'];
		
		$user_id = empty($order['user_id']) ? 0 : intval($order['user_id']);
		$user_name = $user_id ? $db->getOne("SELECT user_name FROM " . $ecs->table('users') . " WHERE user_id = '" . $user_id . "'") : '';
		$sql = "SELECT og.*, g.goods_thumb FROM " . $ecs->table('order_goods') . " as og LEFT JOIN " . $ecs->table('goods') . " as g ON og.goods_id = g.goods_id WHERE order_id = '" . $order_id . "'";
		$order_goods = $db->getAll($sql);
		
		foreach ($order_goods as $index => $goods) {
			$order_goods[$index]['goods_img'] = get_image_path($goods['goods_id'], $goods['goods_img']);

			$stocks_inquiry = stocks_inquiry($goods['goods_id']);
			$order_goods[$index]['stocks_inquiry'] = $stocks_inquiry;
		}


		output_json(array(
			'status' => 1,
			'order_id' => $order_id,
			'order' => $order,
			'user_name' => $user_name,
			'order_goods' => $order_goods
		));
		
		break;
	case 'load_user_input':
		$sql = "SELECT ui.`rec_id`, ui.`ccc`, ui.`mobile`, ui.`user_name`, ui.`email`, u.`user_id` " .
				" FROM " . $ecs->table('pos_user_input') . " as ui " .
				" LEFT JOIN " . $ecs->table('users') . " as u ON ui.`user_name` = u.`user_name` " .
				" WHERE ui.`add_time` > " . (gmtime() - 300) . // Only show input from last 5 minutes
				" ORDER BY ui.`add_time` DESC LIMIT 10";
		$list = $db->getAll($sql);
		
		output_json(array(
			'status' => 1,
			'input_list' => $list
		));
		break;
}
/*------------------------------------------------------ */
//-- Display pages
/*------------------------------------------------------ */
$pages = array(
	'normal' => 'pos/pos_new.dwt',
	'replace' => 'pos/pos_replace.dwt', 
	'repair' => 'pos/pos_repair_new.dwt', 
	'wholesale' => 'pos/pos_wholesale.dwt', 
	'sales-agent' => 'pos/pos_sales_agent.dwt', 
);
$cache_id = sprintf('%X', crc32($_SESSION['user_rank'] . '-' . $_CFG['lang'] . '-' . $mode));

// TODO: combine all mode page
/*------------------------------------------------------ */
//-- 零售 / 換貨
/*------------------------------------------------------ */
if($mode == 'normal' || $mode == 'replace')
{
	if (!$smarty->is_cached($pages[$mode], $cache_id)) {
		$position = assign_ur_here(0, 'POS 系統');
		$smarty->assign('page_title',	  $position['title']);	// 页面标题
		$smarty->assign('ur_here',		 $position['ur_here']);  // 当前位置

		// Payment methods
		$filter_data['is_pos'] = true;
		$payment_list = $paymentController->available_payment_list(true, 0, false, $filter_data);
		$smarty->assign('payment_list', $payment_list);

		// hand code shipping methods
		$shipping_list_output = [
			['shipping_type_code' => 'retail_store','shipping_type_name' => '門市購買'],
			['shipping_type_code' => 'express','shipping_type_name' => '速遞'],
			['shipping_type_code' => 'pickuppoint','shipping_type_name' => '提貨點'],
		];
		$smarty->assign('shipping_type_list', $shipping_list_output);

		// List address types
		$smarty->assign('address_types', $addressController->get_address_types());
		$types_groups = $addressController->get_address_types_groups();
		$address_types_groups = array();
		if (sizeof($types_groups) > 0){
			foreach ($types_groups as $group){
				$address_types_groups[$group['cat']] = $_LANG['checkout_address_type_'.$group['cat']];
			}
		}
		$smarty->assign('address_types_groups', $address_types_groups);

		// List of sales agent with rate
		$smarty->assign('sales_agents', get_pos_sales_agent());

		// Integral scale
		$smarty->assign('integral_scale', $_CFG['integral_scale']);

		// List of salesperson available in POS
		$smarty->assign('salespeople', $db->getAll('SELECT sales_id, sales_name FROM ' . $GLOBALS['ecs']->table('salesperson') . ' AS sp WHERE available = 1 AND allow_pos = 1'));

	}
	$smarty->display($pages[$mode], $cache_id);
}

/*------------------------------------------------------ */
//-- 維修
/*------------------------------------------------------ */
if($mode == 'repair')
{
	if (!$smarty->is_cached($pages[$mode], $cache_id)) {
		$position = assign_ur_here(0, '維修 － POS 系統');
		$smarty->assign('page_title',  $position['title']);	// 页面标题
		$smarty->assign('ur_here',     $position['ur_here']);  // 当前位置

		// List of salesperson available in POS
		$smarty->assign('salespeople', $db->getAll('SELECT sales_id, sales_name FROM ' . $GLOBALS['ecs']->table('salesperson') . ' AS sp WHERE available = 1 AND allow_pos = 1'));
	}
	$smarty->display($pages[$mode], $cache_id);
}


/*------------------------------------------------------ */
//-- 批發
/*------------------------------------------------------ */
else if($mode == 'wholesale')
{
	if (!$smarty->is_cached($pages[$mode], $cache_id)) {
	}
	$smarty->display($pages[$mode], $cache_id);
}

/*------------------------------------------------------ */
//-- 代理銷售
/*------------------------------------------------------ */
else if($mode == 'sales-agent')
{
	if (!$smarty->is_cached($pages[$mode], $cache_id)) {
	}
	$smarty->display($pages[$mode], $cache_id);
}


exit;

function output_json($obj)
{
	header('Content-Type: application/json');
	echo json_encode($obj);
	exit;
}

function stocks_inquiry($goods_ids) {
	$erpController = new Yoho\cms\Controller\ErpController();
	$goods_ids = explode(",", $goods_ids);
	$temp_warehouses = $erpController->getSellableWarehouseInfo();
	foreach ($temp_warehouses as $row) {
		$warehouses[$row['warehouse_id']] = $row['name'];
	}
	$stocks = $erpController->getSellableProductsQtyByWarehouses($goods_ids, "");
	return array(
		'stocks' => $stocks,
		'warehouses' => $warehouses,
	);
}

?>
