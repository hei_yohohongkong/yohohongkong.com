<?php

/***
 * member.php
 * by howang 2014-08-20
 *
 * Member public profile page
 ***/

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

if ((DEBUG_MODE & 2) != 2)
{
	$smarty->caching = true;
}

$user_id = empty($_GET['id']) ? (empty($_SESSION['user_id']) ? 0 : $_SESSION['user_id']) : $_GET['id'];

if (empty($user_id))
{
	header('Location: /');
	exit;
}

// 會員資料
$sql = "SELECT * " .
	   "FROM " . $ecs->table('users') . " " .
	   "WHERE `user_id` = '" . $user_id . "'";
$userinfo = $db->getRow($sql);

// 找不到會員或會員未填寫資料則返回首頁
if ((!$userinfo) || (!$userinfo['info_completed']))
{
	header('Location: /');
	exit;
}

// 會員擴展資料
$sql = "SELECT `reg_field_id`, `content` " .
	   "FROM " . $ecs->table('reg_extend_info') . " " .
	   "WHERE `user_id` = '" . $user_id . "'";
$arr = $db->getAll($sql);
foreach ($arr as $info)
{
	if ($info['reg_field_id'] == 8) $userinfo['first_name'] = $info['content']; // 名字
	if ($info['reg_field_id'] == 9) $userinfo['last_name'] = $info['content']; // 姓氏
	if ($info['reg_field_id'] == 10) $userinfo['display_name'] = $info['content']; // 公開顯示名稱
	if ($info['reg_field_id'] == 11) $userinfo['live_area'] = $info['content']; // 常住地區
	if ($info['reg_field_id'] == 12) $userinfo['work_area'] = $info['content']; // 工作地區
}

assign_template();
$position = assign_ur_here(0, '會員專頁');
$smarty->assign('page_title',	$position['title']);	 // 页面标题
$smarty->assign('ur_here',		$position['ur_here']); // 当前位置
/* meta information */
$smarty->assign('keywords',		htmlspecialchars($_CFG['shop_keywords']));
$smarty->assign('description',	htmlspecialchars($_CFG['shop_desc']));

$smarty->assign('helps',         get_shop_help()); // 网店帮助

$CategoryController = new Yoho\cms\Controller\CategoryController();
$t1 = $CategoryController->getTopCategories(0);
$smarty->assign('t1',              $t1);

$smarty->assign('user_id',       $user_id);

// 會員等級
if ($userinfo['user_rank'] > 0)
{
	$sql = "SELECT rank_name FROM " . $ecs->table('user_rank') . " WHERE rank_id = '" . $userinfo['user_rank'] . "'";
}
else
{
	$sql = "SELECT rank_name FROM " . $ecs->table('user_rank') . " WHERE min_points <= " . intval($userinfo['rank_points']) . " ORDER BY min_points DESC";
}
$rank_name = $db->getOne($sql);
if ($rank_name)
{
	$userinfo['rank_name'] = $rank_name;
}
else
{
	$userinfo['rank_name'] = $_LANG['undifine_rank'];
}

$smarty->assign('userinfo',      $userinfo);

// 已擁有的產品
$sql = "SELECT DISTINCT og.`goods_id`, g.`goods_name`, g.`goods_thumb` " .
	   "FROM " . $ecs->table('order_goods') . " as og " .
	   "LEFT JOIN " . $ecs->table('order_info') . " as oi ON oi.`order_id` = og.`order_id` " . 
	   "LEFT JOIN " . $ecs->table('goods') . " as g ON g.`goods_id` = og.`goods_id` " . 
	   "WHERE oi.`user_id` = '" . $user_id . "'" .
	   "ORDER BY oi.`shipping_time` DESC";
$ownlist = $db->getAll($sql);
foreach ($ownlist as $key => $row)
{
	$ownlist[$key]['url'] = build_uri('goods', array('gid'=>$row['goods_id']), $row['goods_name']);
}
$smarty->assign('ownlist',       $ownlist);

// 很想擁有的產品
$sql = "SELECT wl.`goods_id`, g.`goods_name`, g.`goods_thumb` " .
	   "FROM " . $ecs->table('wish_list') . " as wl " .
	   "LEFT JOIN " . $ecs->table('goods') . " as g ON g.`goods_id` = wl.`goods_id` " . 
	   "WHERE wl.`user_id` = '" . $user_id . "'" . 
	   "ORDER BY wl.`add_time` DESC";
$wishlist = $db->getAll($sql);
foreach ($wishlist as $key => $row)
{
	$wishlist[$key]['url'] = build_uri('goods', array('gid'=>$row['goods_id']), $row['goods_name']);
}
$smarty->assign('wishlist',      $wishlist);

// 跟隨了誰
$sql = "SELECT uf.`follow_id` as user_id, rei.`content` as display_name " .
	   "FROM " . $ecs->table('user_follows') . " as uf " .
	   "LEFT JOIN " . $ecs->table('reg_extend_info') . " as rei ON rei.`user_id` = uf.`follow_id` AND rei.`reg_field_id` = 10 " .
	   "WHERE uf.`user_id` = '" . $user_id . "'" .
	   "ORDER BY uf.`add_time` DESC";
$following = $db->getAll($sql);
foreach ($following as $key => $row)
{
	$following[$key]['user_url'] = build_uri('user', array('uid'=>$row['user_id']), $row['display_name']);
}
$smarty->assign('following',     $following);

// 被誰跟隨
$sql = "SELECT uf.`user_id`, rei.`content` as display_name " .
	   "FROM " . $ecs->table('user_follows') . " as uf " .
	   "LEFT JOIN " . $ecs->table('reg_extend_info') . " as rei ON rei.`user_id` = uf.`user_id` AND rei.`reg_field_id` = 10 " .
	   "WHERE uf.`follow_id` = '" . $user_id . "'" .
	   "ORDER BY uf.`add_time` DESC";
$followedby = $db->getAll($sql);
foreach ($followedby as $key => $row)
{
	$followedby[$key]['user_url'] = build_uri('user', array('uid'=>$row['user_id']), $row['display_name']);
}
$smarty->assign('followedby',    $followedby);

// 當前登入了的會員是否已 follow 此會員
if (!empty($_SESSION['user_id']))
{
	$sql = "SELECT 1 FROM " . $ecs->table('user_follows') . 
		   "WHERE `user_id` = '" . $_SESSION['user_id'] . "' AND `follow_id` = '" . $user_id . "'";
	$is_following = (boolean) $db->getOne($sql);
	$smarty->assign('is_following',  $is_following);
}

$smarty->display('member.dwt');

exit;

?>