<?php
/**
* YOHO Lucky Draw page
* 20180731
*/
define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

if ((DEBUG_MODE & 2) != 2) {
    $smarty->caching = true;
}

// check login 
// echo 'info confirm page<br>';
// echo 'user_id:'.$_SESSION['user_id'];


if (empty($_SESSION['user_id'])) {
    // redirect to login page;
    $_SESSION["back_act_from_event"] = 'luckydraw-info-confirm';
	header('Location: /user.php?act=login');
	exit;
}

$user_id = $_SESSION['user_id'];
$user_rank = $_SESSION['user_rank'];
$rank_points = $_SESSION['rank_points'];
$luckyDrawController = new Yoho\cms\Controller\LuckyDrawController();

$lucky_draw_active = $luckyDrawController->getActiveLuckyDrawEvent();
$user_info = $luckyDrawController->getUserInfoConfirm($user_id);

if (empty($lucky_draw_active)) {
    // redirect
	header('Location: /luckydraw');
	exit; 
}

$lucky_draw_id = $lucky_draw_active[0];
$user_ticket_count = $luckyDrawController->getDrawTicketCount($user_id,$lucky_draw_id);
$lucky_draw_info = $luckyDrawController->getLuckyDrawEventInfo($lucky_draw_id);

if ($user_ticket_count == 0){
    // redirect
   	header('Location: /luckydraw');
   	exit; 
}

if (!empty($_REQUEST['action']) && $_REQUEST['action'] == 'lucky_draw_info_save') {
    $email = $_REQUEST['email'];
    
    if (trim($_REQUEST['last_name']) == '') {
        $result['error'] = $_LANG['lucky_draw_error_last_empty']; 
    }

    if (trim($_REQUEST['first_name']) == '') {
        $result['error'] = $_LANG['lucky_draw_error_first_empty']; 
    }

    if (trim($email) == '') {
        $result['error'] = $_LANG['lucky_draw_error_email_empty']; 
    }

    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $result['error'] = $_LANG['lucky_draw_error_email']; 
    }else{

        // check exist
        $sql = "select count(*) from " . $ecs->table('users'). " where user_id != ".$user_id." and email = '".$email."' ";
        $count = $db->getOne($sql);

        if ($count > 0) {
            $result['error'] = $email. $_LANG['lucky_draw_error_email_used']; 
        } else {
            $luckyDrawController->updateUserInfo($user_id,$email,$_REQUEST['first_name'],$_REQUEST['last_name']);
        }
    }
    
    if (isset($result['error'])) {
        echo json_encode($result);
    } else {
        echo json_encode(array('data' => $result) );
    }
    exit;
}

/* ------------------------------------------------------ */
//-- PROCESSOR
/* ------------------------------------------------------ */
// 
// if (empty($vip)) {
//     header('Location: /vip');
//     exit;
// }

$smarty->assign('data_dir', DATA_DIR);

/* 赋值固定内容 */
assign_template();
$position = assign_ur_here(0);
$smarty->assign('page_title', $position['title']);    // 页面标题
$smarty->assign('ur_here',    $position['ur_here']);  // 当前位置
$categoryController = new Yoho\cms\Controller\CategoryController();
$t1 = $categoryController->getTopCategories(0);
$smarty->assign('t1',              $t1);
$smarty->assign('helps', get_shop_help());              // 网店帮助

//$goods_list_info = $vipController->get_vip_goods($cat_id, $brand_id, $price_min, $price_max, $ext, $size, $page, $sort, $order);

$smarty->assign('user_ticket_count', $user_ticket_count);
$smarty->assign('user_info', $user_info);
$smarty->assign('lucky_draw_info', $lucky_draw_info);
$smarty->assign('script_name', 'vip');
$smarty->assign('display', $display);
$smarty->assign('sort', $sort);
$smarty->assign('order', $order);
$smarty->assign('vip', $vip);
$smarty->assign('rank_points', $rank_points);


$smarty->display('lucky_draw_info_confirm.dwt');
