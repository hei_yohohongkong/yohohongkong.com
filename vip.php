<?php
/**
* YOHO VIP page
* Anthony@YOHO 20170720
*/
define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');

if ((DEBUG_MODE & 2) != 2) {
    $smarty->caching = true;
}

$user_id = $_SESSION['user_id'];
$user_rank = $_SESSION['user_rank'];
$rank_points = $_SESSION['rank_points'];
$vipController = new Yoho\cms\Controller\VipController();
/* 初始化分页信息 */
$page = !empty($_REQUEST['page']) && intval($_REQUEST['page']) > 0 ? intval($_REQUEST['page']) : 1;
$size = !empty($_CFG['page_size']) && intval($_CFG['page_size']) > 0 ? intval($_CFG['page_size']) : 10;
$cat_id = !empty($_REQUEST['cid']) && intval($_REQUEST['cid']) > 0 ? intval($_REQUEST['cid']) : 0;
$brand_id = !empty($_REQUEST['bid']) && intval($_REQUEST['bid']) > 0 ? intval($_REQUEST['bid']) : 0;
$price_min = isset($_REQUEST['price_min']) && intval($_REQUEST['price_min']) > 0 ? intval($_REQUEST['price_min']) : 0;
$price_max = isset($_REQUEST['price_max']) && intval($_REQUEST['price_max']) > 0 ? intval($_REQUEST['price_max']) : 0;

/* 排序、显示方式以及类型 */
$default_display_type = $_CFG['show_order_type'] == '0' ? 'list' : ($_CFG['show_order_type'] == '1' ? 'grid' : 'text');
$default_sort_order_method = $_CFG['sort_order_method'] == '0' ? 'DESC' : 'ASC';
$default_sort_order_type   = $_CFG['sort_order_type'] == '0' ? 'last_update' : ($_CFG['sort_order_type'] == '1' ? 'shop_price' : 'goods_id');

$sort = (isset($_REQUEST['sort']) && in_array(trim(strtolower($_REQUEST['sort'])), array('goods_id', 'shop_price', 'last_update', 'shownum'))) ? trim($_REQUEST['sort']) : $default_sort_order_type;
$order = (isset($_REQUEST['order']) && in_array(trim(strtoupper($_REQUEST['order'])), array('ASC', 'DESC'))) ? trim($_REQUEST['order']) : $default_sort_order_method;
$display = (isset($_REQUEST['display']) && in_array(trim(strtolower($_REQUEST['display'])), array('list', 'grid', 'text'))) ? trim($_REQUEST['display']) : (isset($_COOKIE['ECS']['display']) ? $_COOKIE['ECS']['display'] : $default_display_type);
//$display  = in_array($display, array('list', 'grid', 'text')) ? $display : 'text';
$display = in_array($display, array('list', 'grid')) ? $display : 'grid';
$vip = $vipController->get_vip_info_with_user($user_id, $user_rank, $rank_points);
setcookie('ECS[display]', $display, gmtime() + 86400 * 7);

/* ------------------------------------------------------ */
//-- PROCESSOR
/* ------------------------------------------------------ */

if (empty($vip)) {
    header('Location: /vip');
    exit;
}

$smarty->assign('data_dir', DATA_DIR);

/* 赋值固定内容 */
assign_template();
$position = assign_ur_here(0);
$smarty->assign('page_title', $position['title']);    // 页面标题
$smarty->assign('ur_here',    $position['ur_here']);  // 当前位置
$categoryController = new Yoho\cms\Controller\CategoryController();
$t1 = $categoryController->getTopCategories(0);
$smarty->assign('t1',              $t1);
$smarty->assign('helps', get_shop_help());              // 网店帮助

$goods_list_info = $vipController->get_vip_goods($cat_id, $brand_id, $price_min, $price_max, $ext, $size, $page, $sort, $order);

$cperma = $categoryController->getCategoryPermaLink($cat_id, $goods_list_info['left_cat_menu']);
if (!empty($brand_id)) {
    foreach ($goods_list_info['brands'] as $b) {
        if (!empty($bperma)) continue;
        if ($b['brand_id'] == $brand_id) {
            $bperma = $b['bperma'];
        }
    }
}

$smarty->assign('goods_list', $goods_list_info['goodslist']);
$smarty->assign('script_name', 'vip');
$smarty->assign('display', $display);
$smarty->assign('sort', $sort);
$smarty->assign('order', $order);
$smarty->assign('vip', $vip);
$smarty->assign('rank_points', $rank_points);
$smarty->assign('vip_page', true);
if($category > 0){
    $first_parent = array_reverse($categoryController->getParentLevel($category));
    $smarty->assign('first_parent_cat',$first_parent[0]['cat_id']);
}
$smarty->assign('left_cat_menu', $goods_list_info['left_cat_menu']);
$smarty->assign('left_cat_menu_count',$goods_list_info['left_cat_menu_count']);
$smarty->assign('brands',           $goods_list_info['brands']);
$smarty->assign('brand_id',        $goods_list_info['brand_id']);
$smarty->assign('category_id',        $cat_id);
// Price Filter
$smarty->assign('price_min', $price_min);
$smarty->assign('price_max', $price_max);

assign_pager('vip', $cat_id, $goods_list_info['count'], $size, $sort, $order, $page, $ext, $brand_id, $price_min, $price_max, $display, $is_promote, '', '', ['cperma' => $cperma, 'bperma' => $bperma]); // 分页
assign_dynamic('vip'); // 动态内容


$smarty->display('vip.dwt');
