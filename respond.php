<?php

/**
 * ECSHOP 支付响应页面
 * ============================================================================
 * * 版权所有 2005-2012 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: liubo $
 * $Id: respond.php 17217 2011-01-19 06:29:08Z liubo $
 */

define('IN_ECS', true);

define('DESKTOP_ONLY', true);

require(dirname(__FILE__) . '/includes/init.php');
require(ROOT_PATH . 'includes/lib_payment.php');
require(ROOT_PATH . 'includes/lib_order.php');
/* 支付方式代码 */
$pay_code = !empty($_REQUEST['code']) ? trim($_REQUEST['code']) : '';

//获取首信支付方式
if (empty($pay_code) && !empty($_REQUEST['v_pmode']) && !empty($_REQUEST['v_pstring']))
{
    $pay_code = 'cappay';
}

//获取快钱神州行支付方式
if (empty($pay_code) && ($_REQUEST['ext1'] == 'shenzhou') && ($_REQUEST['ext2'] == 'ecshop'))
{
    $pay_code = 'shenzhou';
}

/* 参数是否为空 */
if (empty($pay_code))
{
    $msg = _L('payment_failed', '付款失敗。如需協助請聯絡我們。'); //$_LANG['pay_not_exist'];
}
else
{
    /* 检查code里面有没有问号 */
    if (strpos($pay_code, '?') !== false)
    {
        $arr1 = explode('?', $pay_code);
        $arr2 = explode('=', $arr1[1]);

        $_REQUEST['code']   = $arr1[0];
        $_REQUEST[$arr2[0]] = $arr2[1];
        $_GET['code']       = $arr1[0];
        $_GET[$arr2[0]]     = $arr2[1];
        $pay_code           = $arr1[0];
    }

    $success = false;

    /* 判断是否启用 */
    $sql = "SELECT COUNT(*) FROM " . $ecs->table('payment') . " WHERE pay_code = '$pay_code' AND enabled = 1";
    if ($db->getOne($sql) == 0)
    {
        $msg = _L('payment_failed', '付款失敗。如需協助請聯絡我們。'); //$_LANG['pay_disabled'];
    }
    else
    {
        $plugin_file = 'includes/modules/payment/' . $pay_code . '.php';

        /* 检查插件文件是否存在，如果存在则验证支付是否成功，否则则返回失败信息 */
        if (file_exists($plugin_file))
        {
            $flashdealController = new \Yoho\cms\Controller\FlashdealController();
            if($_SESSION['order_id']) { // If session have order_id (braintree, stripe, braintreepaypal), check have flashdeal goods: flashdeal order only have 5mins to pay order.
                $flashdeal_info = $db->getRow("SELECT count(og.flashdeal_id) as flashdeal_num, oi.add_time as add_time, oi.order_status FROM ".$ecs->table('order_goods') . " as og ".
                    " LEFT JOIN ".$ecs->table('order_info')."as oi ON oi.order_id = og.order_id ".
                    " WHERE og.flashdeal_id > 0 and og.order_id = ".$_SESSION['order_id']." GROUP BY og.order_id");
                $expired = local_strtotime($flashdealController::FLASH_PAY_ORDER_EXPIRED, $flashdeal_info['add_time']);
                $a = local_date("Y-m-d H:i:s", $expired);
                $expired2 = $flashdealController->getOrderFisrtExpired($_SESSION['order_id']);
                $b = local_date("Y-m-d H:i:s", $expired2);
                /* Check if order have flashdeal products, and expired 5 min and exired flashd eal 10min. */
                if (in_array($flashdeal_info['order_status'], [OS_CANCELED, OS_INVALID, OS_WAITING_REFUND])) {
                    $msg = _L('payment_failed_flashdeal', '付款失敗。此閃購訂單已到期，如需協助請聯絡我們。');
                } elseif($flashdeal_info['flashdeal_num'] > 0 && gmtime() > $expired && gmtime() > $expired2) {
                    include_once(ROOT_PATH . 'includes/lib_transaction.php');
                    include_once(ROOT_PATH . 'includes/lib_order.php');
                    cancel_order($_SESSION['order_id'], $_SESSION['user_id']);
                    $msg = _L('payment_failed_flashdeal', '付款失敗。此閃購訂單已到期，如需協助請聯絡我們。');
                } else {
                    /* 根据支付方式代码创建支付类的对象并调用其响应操作方法 */
                    include_once($plugin_file);

                    $payment = new $pay_code();
                    $success = @$payment->respond();
                    
                    if (gettype($success) == 'array') {
                        if (isset($success['error_msg'])) {
                            $msg = $success['error_msg'];
                        }
                    } else {
                        $msg = $success ? _L('payment_success', '付款成功，我們將盡快為您出貨。') : _L('payment_failed', '付款失敗。如需協助請聯絡我們。');
                    }  
                }
            } else {
                /* 根据支付方式代码创建支付类的对象并调用其响应操作方法 */
                include_once($plugin_file);

                $payment = new $pay_code();
                $success = @$payment->respond();
                $msg     = $success ? _L('payment_success', '付款成功，我們將盡快為您出貨。') : _L('payment_failed', '付款失敗。如需協助請聯絡我們。');
            }
        }
        else
        {
            $msg = _L('payment_failed', '付款失敗。如需協助請聯絡我們。'); //$_LANG['pay_not_exist'];
        }
    }
}

// 如果付款成功、是訂單付款、有登入，自動轉到訂單詳情頁面
if ($success === true && !empty($paid_order) && !empty($_SESSION['user_id']))
{
    header('Location: /user.php?act=order_detail&order_id=' . $paid_order['order_id'] . '&paysuccess=1');
    exit;
}

assign_template();
$position = assign_ur_here();
$smarty->assign('page_title', $position['title']);   // 页面标题
$smarty->assign('ur_here',    $position['ur_here']); // 当前位置
$smarty->assign('helps',      get_shop_help());      // 网店帮助

$CategoryController = new Yoho\cms\Controller\CategoryController();
$t1 = $CategoryController->getTopCategories(0);
$smarty->assign('t1',              $t1);
$smarty->assign('keywords',   htmlspecialchars($_CFG['shop_keywords']));
$smarty->assign('description',htmlspecialchars($_CFG['shop_desc']));

$smarty->assign('message',    $msg);
$smarty->assign('shop_url',   $ecs->url());

$smarty->display('respond.dwt');

?>