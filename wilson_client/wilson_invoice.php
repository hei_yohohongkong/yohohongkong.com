<?php
/**
 * ECSHOP OPEN API统一接口
 * ============================================================================
 * * 版权所有 2005-2012 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: sxc_shop $
 * $Id: goods.php 15921 2009-05-07 05:35:58Z sxc_shop $
*/

define('IN_ECS', true);
require_once(dirname(__FILE__) . '/../includes/init.php');
require_once(ROOT_PATH . 'includes/lib_license.php');
require_once('includes/cls_json.php');
require_once(ROOT_PATH . 'includes/lib_order.php');
require_once(ROOT_PATH . 'includes/lib_goods.php');
require_once(ROOT_PATH . 'includes/lib_erp.php');
define('RETURN_TYPE', empty($_POST['return_data']) ? 1 : ($_POST['return_data'] == 'json' ? 2 : 1));
require_once(ROOT_PATH . 'languages/zh_tw/order_print.php');
$smarty->assign('lang', $_LANG);

ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);

$mass_key = $_GET['mass_key'];
$wilson_token = $_GET['wilson_token'];

if (empty($_GET['mass_key']) || empty($_GET['wilson_token']))
{
    header("Location: /"); /* Redirect browser */
    exit();
}

$deliveryorderController = new Yoho\cms\Controller\DeliveryOrderController();

$deliveryorderController->validate_wilson_print_token($mass_key,$wilson_token);

if (!$deliveryorderController->validate_wilson_print_token($mass_key,$wilson_token)) {
    header("Location: /"); /* Redirect browser */
    exit();
}

// check the mass_key if exist
$deliveryorderController->isWilsonOrderDelivery($mass_key);

if (!$deliveryorderController->isWilsonOrderDelivery($mass_key)) {
    header("Location: /"); /* Redirect browser */
    exit();
}

$token = $deliveryorderController->getWilsonPrintToken($mass_key);

$html = '';
$order_sn_list = $deliveryorderController->get_order_sn();
//$order_sn_list = array_column($order_sn, 'order_sn');
foreach ($order_sn_list as $order_sn_key => $order)
{
    assign_order_print(0,$order['order_sn'], $order['delivery_id'], 1);
    $smarty->template_dir = '../' . DATA_DIR;
    $template_order = $smarty->get_template_vars('order');
    $template = ($template_order['only_market_invoice']) ? 'order_print3.html' : 'order_print2.html';
    $smarty->fetch($template);

    $html .= 
        '<div style="PAGE-BREAK-AFTER:always"></div>';
    assign_order_print(0,$order['order_sn'], $order['delivery_id'], 2);
    $smarty->template_dir = '../' . DATA_DIR;
    $template_order = $smarty->get_template_vars('order');
    $template = ($template_order['only_market_invoice']) ? 'order_print3.html' : 'order_print2.html';
    $html .= $smarty->fetch($template) .
        '<div style="PAGE-BREAK-AFTER:always"></div>';

    //$sql = "UPDATE " . $ecs->table('mass_delivery') . " SET process = ".MK_FINISH." WHERE mass_key = '" . $_REQUEST['mass_key'] . "' AND order_id = (SELECT order_id FROM " . $ecs->table('order_info') . " WHERE order_sn = '" . $order_sn . "')";
    //$db->query($sql);
}
echo $html;
exit;

print_r($order_sn);

exit;

?>