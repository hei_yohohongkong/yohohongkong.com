<?php

/* * *
 * quotation.php
 * by Anthony 2017-03-29
 *
 * Yoho quotation front-end function
 * * */

define('IN_ECS', true);

require(dirname(__FILE__) . '/includes/init.php');
require(ROOT_PATH . 'includes/lib_quotation.php');
$addressController = new Yoho\cms\Controller\AddressController();
/* ------------------------------------------------------ */
//-- PROCESSOR
/* ------------------------------------------------------ */

$page_title = _L('quotation', '報價單');
$position = assign_ur_here(0, $page_title);
$smarty->assign('page_title', $position['title']);		// 页面标题
$smarty->assign('ur_here', $position['ur_here']);	// 当前位置
//$smarty->assign('categories',       get_categories_tree()); // 分类树
$smarty->assign('helps', get_shop_help());			 // 网店帮助
$smarty->assign('lang', $_LANG);
$smarty->assign('data_dir', DATA_DIR);			 // 数据目录
assign_template();

/* Add new quotation (default) */
if ($_REQUEST['act'] == 'add_quotation')
{
	$flow_type = CART_GENERAL_GOODS;
	$quotation = array();

	// Check cart item
	$sql = "SELECT COUNT(*) FROM " . $ecs->table('cart') .
					" WHERE session_id = '" . SESS_ID . "' " .
					"AND parent_id = 0 AND is_gift = 0 AND rec_type = '$flow_type'";

	if ($db->getOne($sql) == 0)
	{
		show_message(_L('checkout_error_empty_cart', '您的購物車中沒有產品！'), '', '', 'warning');
	}

	// Get cart Items
	$cart_goods = cart_goods($flow_type);

	// Hard code address (Only count shipping address : HK)
	$consignee = array();
	$consignee['country'] = '3409';
	$consignee['province'] = '0';
	$consignee['city'] = '0';
	$consignee['district'] = '0';

	// Get weight info
	$free_region = $addressController->get_free_calculate_region();
    $cal_free_shipping = false;
    if (!empty($free_region['country'] && in_array($consignee['country'], $free_region['country']))) $cal_free_shipping = true;
    $smarty->assign('cal_free_shipping', $cal_free_shipping);
	$cart_weight_price = cart_weight_price($flow_type, $cal_free_shipping);
	$smarty->assign('cart_weight_price', $cart_weight_price);

	// Get shiiping info
	$shipping_list = get_quotation_shipping_list($consignee, $cart_weight_price, $quotation, $cal_free_shipping);

	// Count quotation fee
	$total = quotation_fee($quotation, $cart_goods, $pay_id, $consignee);

	// Assign view

	$smarty->assign('goods_list', $cart_goods);
	$smarty->assign('total', $total);
	$smarty->assign('shipping_list', $shipping_list);
	$smarty->assign('cartnumber', cart_ly_number());
	$smarty->assign('is_quotation', true);
	$smarty->display('quotation.dwt');
} elseif ($_REQUEST['act'] == 'submit_quotation')
{
	$flow_type = CART_GENERAL_GOODS;
	$pay_id = $_POST['payment']; // Only for quotation payment
	$shipping = $_POST['shipping'];
	/*
	$quotation = array(
			'shipping_id' => isset($_POST['shipping']) ? intval($_POST['shipping']) : 0,
	);
	*/
	$quotation = flow_order_info();
    // Apply Shipping / Payment method if provided
    if (isset($shipping))
    {
        $quotation['shipping_id'] = $shipping;
    }
    if (isset($pay_id))
    {
		$quotation['pay_id'] = $pay_id;
		if($quotation['pay_id'] == '1')$quotation['pay_id'] = '7';
		else if($quotation['pay_id'] == '2') $quotation['pay_id'] = '3';
	}
	
	// Contact info
	$contact_info = array(
			'company_name' => $_POST['company_name'],
			'contact_name' => $_POST['contact_name'],
			'contact_email' => $_POST['contact_email'],
			'contact_tel' => $_POST['contact_tel']
	);

	// Check cart item
	$sql = "SELECT COUNT(*) FROM " . $ecs->table('cart') .
					" WHERE session_id = '" . SESS_ID . "' " .
					"AND parent_id = 0 AND is_gift = 0 AND rec_type = '$flow_type'";

	if ($db->getOne($sql) == 0)
	{
		show_message(_L('checkout_error_empty_cart', '您的購物車中沒有產品！'), '', '', 'warning');
	}

	// Get cart Items
	$cart_goods = cart_goods($flow_type);

	// Hard code address (Only count shipping address : HK)
	$consignee = array();
	$consignee['country'] = '3409';
	$consignee['province'] = '0';
	$consignee['city'] = '0';
	$consignee['district'] = '0';

	// Get weight info
	$free_region = $addressController->get_free_calculate_region();
    $cal_free_shipping = false;
    if (!empty($free_region['country'] && in_array($consignee['country'], $free_region['country']))) $cal_free_shipping = true;
    $smarty->assign('cal_free_shipping', $cal_free_shipping);
	$cart_weight_price = cart_weight_price($flow_type, $cal_free_shipping);
	$smarty->assign('cart_weight_price', $cart_weight_price);

	// Get shiiping info
	$shipping_list = get_quotation_shipping_list($consignee, $cart_weight_price, $quotation, $cal_free_shipping);

	// Count quotation fee
	$total = quotation_fee($quotation, $cart_goods, $pay_id, $consignee);

	//Save data to table
	$quotation_id = add_quotation($contact_info, $shipping, $pay_id, $total, $cart_goods);
	$result = add_quotation_goods($quotation_id, $cart_goods);

	if ($result && can_send_mail($contact_info['contact_email'], true))
	{
		$quotation = get_quotation($quotation_id);
		$quotation['goods'] = $cart_goods;
		$now = date("Y-m-d H:i:s");

		$tpl = get_mail_template('send_quotation');
		$smarty->assign('quotation', $quotation);
		$smarty->assign('total', $total);
		$smarty->assign('date', $now);
		$smarty->assign('shop_name', $GLOBALS['_CFG']['shop_name']);
		$smarty->assign('send_date', local_date($GLOBALS['_CFG']['date_format']));
		$smarty->assign('sent_date', local_date($GLOBALS['_CFG']['date_format']));
		$content = $smarty->fetch('str:' . $tpl['template_content']);

		//pdf Setting
		if (!make_dir(ROOT_PATH . DATA_DIR . '/quotationpdf/'))
		{
			return false;
		}
		$pdfname = 'quotation_' . $quotation['quotation_sn'] . '.pdf';
		$pdf_path = ROOT_PATH . DATA_DIR . '/quotationpdf/' . $pdfname;
		if (!file_exists($pdf_path))
		{
			$old_template_dir = $smarty->template_dir;
			$smarty->template_dir = DATA_DIR;
			$html = $smarty->fetch('quotation_pdf.html');
			$smarty->template_dir = $old_template_dir;
			$pdf = generate_pdf($html);
			file_put_contents($pdf_path, $pdf);
		}
		send_mail_with_attachment($contact_info['contact_name'], $contact_info['contact_email'], $tpl['template_subject'] . ' - ' . $quotation['quotation_sn'], $content, $tpl['is_html'], false, $pdf_path);
		$smarty->assign('is_quotation', true);
		$smarty->display('send_quotation.dwt');
	}
}
?>
